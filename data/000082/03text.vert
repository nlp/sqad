<s>
Paříž	Paříž	k1gFnSc1	Paříž
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
i	i	k9	i
správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
regionu	region	k1gInSc2	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
<g/>
,	,	kIx,	,
zahrnujícího	zahrnující	k2eAgInSc2d1	zahrnující
Paříž	Paříž	k1gFnSc4	Paříž
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
předměstí	předměstí	k1gNnSc4	předměstí
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
sama	sám	k3xTgFnSc1	sám
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
departementů	departement	k1gInPc2	departement
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
<g/>
:	:	kIx,	:
département	département	k1gMnSc1	département
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
centrem	centrum	k1gNnSc7	centrum
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
552,7	[number]	k4	552,7
miliardami	miliarda	k4xCgFnPc7	miliarda
€	€	k?	€
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejaktivnější	aktivní	k2eAgFnSc7d3	nejaktivnější
oblastí	oblast	k1gFnSc7	oblast
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
sídlí	sídlet	k5eAaImIp3nS	sídlet
vedení	vedení	k1gNnSc1	vedení
téměř	téměř	k6eAd1	téměř
poloviny	polovina	k1gFnSc2	polovina
všech	všecek	k3xTgFnPc2	všecek
francouzských	francouzský	k2eAgFnPc2d1	francouzská
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
kanceláře	kancelář	k1gFnSc2	kancelář
hlavních	hlavní	k2eAgFnPc2d1	hlavní
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
ředitelství	ředitelství	k1gNnSc2	ředitelství
mnoha	mnoho	k4c2	mnoho
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
,	,	kIx,	,
OECD	OECD	kA	OECD
nebo	nebo	k8xC	nebo
ICC	ICC	kA	ICC
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
významným	významný	k2eAgNnSc7d1	významné
světovým	světový	k2eAgNnSc7d1	světové
kulturním	kulturní	k2eAgNnSc7d1	kulturní
<g/>
,	,	kIx,	,
obchodním	obchodní	k2eAgNnSc7d1	obchodní
i	i	k8xC	i
politickým	politický	k2eAgNnSc7d1	politické
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
zhruba	zhruba	k6eAd1	zhruba
2,2	[number]	k4	2,2
miliónu	milión	k4xCgInSc2	milión
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
aglomeraci	aglomerace	k1gFnSc6	aglomerace
pak	pak	k6eAd1	pak
asi	asi	k9	asi
10,3	[number]	k4	10,3
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
Řádu	řád	k1gInSc2	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
Válečného	válečný	k2eAgInSc2d1	válečný
kříže	kříž	k1gInSc2	kříž
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
a	a	k8xC	a
Řádu	řád	k1gInSc3	řád
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Paris	Paris	k1gMnSc1	Paris
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
se	se	k3xPyFc4	se
[	[	kIx(	[
<g/>
paʀ	paʀ	k?	paʀ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
latinské	latinský	k2eAgNnSc1d1	latinské
jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
Lutetia	Lutetia	k1gFnSc1	Lutetia
[	[	kIx(	[
<g/>
lutecia	lutecium	k1gNnSc2	lutecium
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
Lutetia	Lutetia	k1gFnSc1	Lutetia
Parisiorum	Parisiorum	k1gInSc1	Parisiorum
(	(	kIx(	(
<g/>
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
Lutè	Lutè	k1gFnSc2	Lutè
[	[	kIx(	[
<g/>
lytɛ	lytɛ	k?	lytɛ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
Lutécie	Lutécie	k1gFnSc2	Lutécie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
později	pozdě	k6eAd2	pozdě
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
jména	jméno	k1gNnSc2	jméno
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
galského	galský	k2eAgInSc2d1	galský
kmene	kmen	k1gInSc2	kmen
Parisiů	Parisi	k1gInPc2	Parisi
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
jméno	jméno	k1gNnSc1	jméno
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
keltsko-galského	keltskoalský	k2eAgNnSc2d1	keltsko-galský
slova	slovo	k1gNnSc2	slovo
parios	parios	k1gInSc1	parios
<g/>
,	,	kIx,	,
znamenajícího	znamenající	k2eAgMnSc4d1	znamenající
"	"	kIx"	"
<g/>
kotel	kotel	k1gInSc4	kotel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
tato	tento	k3xDgFnSc1	tento
teze	teze	k1gFnSc1	teze
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jistá	jistý	k2eAgFnSc1d1	jistá
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
označení	označení	k1gNnSc1	označení
kmene	kmen	k1gInSc2	kmen
Parisiů	Parisi	k1gMnPc2	Parisi
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
keltsko-galského	keltskoalský	k2eAgNnSc2d1	keltsko-galský
slova	slovo	k1gNnSc2	slovo
parisio	parisio	k6eAd1	parisio
<g/>
,	,	kIx,	,
znamenajícího	znamenající	k2eAgMnSc4d1	znamenající
"	"	kIx"	"
<g/>
pracující	pracující	k2eAgMnPc1d1	pracující
muži	muž	k1gMnPc1	muž
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
řemeslníci	řemeslník	k1gMnPc1	řemeslník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Paříž	Paříž	k1gFnSc1	Paříž
známá	známý	k2eAgFnSc1d1	známá
i	i	k9	i
pod	pod	k7c7	pod
slangovým	slangový	k2eAgNnSc7d1	slangové
jménem	jméno	k1gNnSc7	jméno
Paname	Panam	k1gInSc5	Panam
[	[	kIx(	[
<g/>
panam	panama	k1gFnPc2	panama
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
lidové	lidový	k2eAgNnSc1d1	lidové
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
především	především	k9	především
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dokládá	dokládat	k5eAaImIp3nS	dokládat
např.	např.	kA	např.
skladba	skladba	k1gFnSc1	skladba
Amoureux	Amoureux	k1gInSc1	Amoureux
de	de	k?	de
Paname	Panam	k1gInSc5	Panam
(	(	kIx(	(
<g/>
Zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
do	do	k7c2	do
Panamy	Panama	k1gFnSc2	Panama
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
Renauda	Renaud	k1gMnSc2	Renaud
Séchana	Séchan	k1gMnSc2	Séchan
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
"	"	kIx"	"
<g/>
Moi	Moi	k1gMnSc1	Moi
<g/>
,	,	kIx,	,
j	j	k?	j
<g/>
'	'	kIx"	'
<g/>
suis	suisit	k5eAaPmRp2nS	suisit
amoureux	amoureux	k1gInSc1	amoureux
de	de	k?	de
Paname	Panam	k1gInSc5	Panam
<g/>
,	,	kIx,	,
du	du	k?	du
béton	béton	k1gInSc1	béton
<g />
.	.	kIx.	.
</s>
<s>
et	et	k?	et
du	du	k?	du
macadam	macadam	k1gInSc1	macadam
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
zamilovaný	zamilovaný	k1gMnSc1	zamilovaný
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
betonové	betonový	k2eAgFnSc2d1	betonová
a	a	k8xC	a
makadamové	makadamový	k2eAgFnSc2d1	makadamová
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
píseň	píseň	k1gFnSc4	píseň
Loin	Loina	k1gFnPc2	Loina
de	de	k?	de
Paname	Panam	k1gInSc5	Panam
(	(	kIx(	(
<g/>
Daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
Panamy	Panama	k1gFnSc2	Panama
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
použitá	použitý	k2eAgFnSc1d1	použitá
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Paříž	Paříž	k1gFnSc1	Paříž
36	[number]	k4	36
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
Oscar	Oscar	k1gMnSc1	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
Pars	Parsa	k1gFnPc2	Parsa
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
kmenem	kmen	k1gInSc7	kmen
Parsi	Parse	k1gFnSc3	Parse
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
Pars	Parsa	k1gFnPc2	Parsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xC	jako
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důvod	důvod	k1gInSc4	důvod
některých	některý	k3yIgNnPc2	některý
francouzských	francouzský	k2eAgNnPc2d1	francouzské
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
slova	slovo	k1gNnPc1	slovo
v	v	k7c6	v
tamním	tamní	k2eAgInSc6d1	tamní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Paříže	Paříž	k1gFnSc2	Paříž
jsou	být	k5eAaImIp3nP	být
označování	označování	k1gNnSc4	označování
jako	jako	k8xC	jako
Pařížané	Pařížan	k1gMnPc1	Pařížan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
jako	jako	k8xC	jako
Parisiens	Parisiens	k1gInSc4	Parisiens
[	[	kIx(	[
<g/>
paʀ	paʀ	k?	paʀ
<g/>
̃	̃	k?	̃
<g/>
]	]	kIx)	]
a	a	k8xC	a
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
jako	jako	k8xC	jako
Parisians	Parisians	k1gInSc4	Parisians
[	[	kIx(	[
<g/>
pə	pə	k?	pə
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
pə	pə	k?	pə
<g/>
.	.	kIx.	.
<g/>
ʒ	ʒ	k?	ʒ
<g/>
̩	̩	k?	̩
<g/>
z	z	k7c2	z
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Parigot	Parigot	k1gMnSc1	Parigot
(	(	kIx(	(
<g/>
znamenající	znamenající	k2eAgMnSc1d1	znamenající
"	"	kIx"	"
<g/>
Pařížan	Pařížan	k1gMnSc1	Pařížan
<g/>
"	"	kIx"	"
a	a	k8xC	a
vyslovující	vyslovující	k2eAgMnSc1d1	vyslovující
se	se	k3xPyFc4	se
[	[	kIx(	[
<g/>
paʀ	paʀ	k?	paʀ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
používáno	používat	k5eAaImNgNnS	používat
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
slangu	slang	k1gInSc6	slang
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xS	jako
hanlivé	hanlivý	k2eAgFnPc1d1	hanlivá
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
Parigot	Parigot	k1gMnSc1	Parigot
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
mít	mít	k5eAaImF	mít
i	i	k9	i
kladný	kladný	k2eAgInSc4d1	kladný
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Mimile	Mimila	k1gFnSc6	Mimila
(	(	kIx(	(
<g/>
un	un	k?	un
gars	gars	k1gInSc1	gars
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ménilmontant	Ménilmontant	k1gMnSc1	Ménilmontant
<g/>
)	)	kIx)	)
od	od	k7c2	od
Maurice	Maurika	k1gFnSc6	Maurika
Chevaliera	chevalier	k1gMnSc4	chevalier
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
est	est	k?	est
un	un	k?	un
gars	gars	k1gInSc1	gars
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ménilmontant	Ménilmontant	k1gMnSc1	Ménilmontant
<g/>
,	,	kIx,	,
un	un	k?	un
vrai	vra	k1gFnSc2	vra
p	p	k?	p
<g/>
'	'	kIx"	'
<g/>
tit	tit	k?	tit
Parigot	Parigot	k1gInSc1	Parigot
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
chlapík	chlapík	k1gMnSc1	chlapík
z	z	k7c2	z
Ménilmontant	Ménilmontant	k1gMnSc1	Ménilmontant
<g/>
,	,	kIx,	,
opravdový	opravdový	k2eAgMnSc1d1	opravdový
malý	malý	k2eAgMnSc1d1	malý
Pařížan	Pařížan	k1gMnSc1	Pařížan
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lokálně	lokálně	k6eAd1	lokálně
jsou	být	k5eAaImIp3nP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
pařížských	pařížský	k2eAgNnPc2d1	pařížské
předměstí	předměstí	k1gNnPc2	předměstí
hovorově	hovorově	k6eAd1	hovorově
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
jako	jako	k9	jako
banlieusards	banlieusards	k6eAd1	banlieusards
[	[	kIx(	[
<g/>
bɑ	bɑ	k?	bɑ
<g/>
̃	̃	k?	̃
<g/>
ljø	ljø	k?	ljø
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obyvatelé	obyvatel	k1gMnPc1	obyvatel
banlieue	banlieue	k1gNnPc7	banlieue
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
předměstí	předměstí	k1gNnPc1	předměstí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
regionu	region	k1gInSc2	region
Île-de-France	Îlee-France	k1gFnSc1	Île-de-France
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
označováni	označován	k2eAgMnPc1d1	označován
jako	jako	k8xS	jako
Franciliens	Franciliensa	k1gFnPc2	Franciliensa
[	[	kIx(	[
<g/>
fʀ	fʀ	k?	fʀ
<g/>
̃	̃	k?	̃
<g/>
siljɛ	siljɛ	k?	siljɛ
<g/>
̃	̃	k?	̃
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Pařížané	Pařížan	k1gMnPc1	Pařížan
označují	označovat	k5eAaImIp3nP	označovat
obyvatele	obyvatel	k1gMnPc4	obyvatel
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
mimo	mimo	k7c4	mimo
Île-de-France	Îlee-France	k1gFnPc4	Île-de-France
jako	jako	k8xS	jako
provinciaux	provinciaux	k1gInSc4	provinciaux
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
při	při	k7c6	při
odkazování	odkazování	k1gNnSc6	odkazování
na	na	k7c4	na
zbytek	zbytek	k1gInSc4	zbytek
Francie	Francie	k1gFnSc2	Francie
jako	jako	k8xS	jako
na	na	k7c6	na
la	la	k1gNnSc6	la
province	province	k1gFnSc2	province
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
hanlivý	hanlivý	k2eAgInSc4d1	hanlivý
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
nálezy	nález	k1gInPc1	nález
osídlení	osídlení	k1gNnSc2	osídlení
oblasti	oblast	k1gFnSc2	oblast
Paříže	Paříž	k1gFnSc2	Paříž
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
zhruba	zhruba	k6eAd1	zhruba
4500	[number]	k4	4500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
osídlili	osídlit	k5eAaPmAgMnP	osídlit
kraj	kraj	k1gInSc4	kraj
Galové	Galová	k1gFnSc2	Galová
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Parisiů	Parisi	k1gInPc2	Parisi
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
52	[number]	k4	52
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
kraj	kraj	k1gInSc4	kraj
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
jej	on	k3xPp3gMnSc4	on
Lutetia	Lutetius	k1gMnSc4	Lutetius
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Lutè	Lutè	k1gMnSc2	Lutè
<g/>
;	;	kIx,	;
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
bažinaté	bažinatý	k2eAgNnSc1d1	bažinaté
místo	místo	k1gNnSc1	místo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Lutetia	Lutetia	k1gFnSc1	Lutetia
Parisiorum	Parisiorum	k1gInSc1	Parisiorum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
osídlen	osídlen	k2eAgInSc1d1	osídlen
pouze	pouze	k6eAd1	pouze
dnešní	dnešní	k2eAgInSc1d1	dnešní
ostrov	ostrov	k1gInSc1	ostrov
Île	Île	k1gFnSc1	Île
de	de	k?	de
la	la	k1gNnSc7	la
Cité	Citá	k1gFnSc2	Citá
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
stojí	stát	k5eAaImIp3nS	stát
katedrála	katedrála	k1gFnSc1	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgNnSc1d1	římské
osídlení	osídlení	k1gNnSc1	osídlení
rychle	rychle	k6eAd1	rychle
rostlo	růst	k5eAaImAgNnS	růst
a	a	k8xC	a
během	během	k7c2	během
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
šířit	šířit	k5eAaImF	šířit
i	i	k9	i
na	na	k7c4	na
levý	levý	k2eAgInSc4d1	levý
břeh	břeh	k1gInSc4	břeh
Seiny	Seina	k1gFnSc2	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
římské	římský	k2eAgFnSc2d1	římská
nadvlády	nadvláda	k1gFnSc2	nadvláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
508	[number]	k4	508
Chlodvík	Chlodvík	k1gInSc1	Chlodvík
I.	I.	kA	I.
učinil	učinit	k5eAaImAgInS	učinit
z	z	k7c2	z
města	město	k1gNnSc2	město
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Chlodvík	Chlodvík	k1gMnSc1	Chlodvík
I.	I.	kA	I.
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
511	[number]	k4	511
výstavbu	výstavba	k1gFnSc4	výstavba
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Štěpána	Štěpána	k1gFnSc1	Štěpána
(	(	kIx(	(
<g/>
St-	St-	k1gMnSc5	St-
Étienne	Étienn	k1gMnSc5	Étienn
<g/>
)	)	kIx)	)
na	na	k7c6	na
Île	Île	k1gFnSc6	Île
de	de	k?	de
la	la	k1gNnSc2	la
Cité	Citá	k1gFnSc2	Citá
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
také	také	k9	také
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
pevnost	pevnost	k1gFnSc4	pevnost
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
před	před	k7c7	před
Vikingy	Viking	k1gMnPc7	Viking
po	po	k7c6	po
roce	rok	k1gInSc6	rok
800	[number]	k4	800
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
845	[number]	k4	845
byla	být	k5eAaImAgFnS	být
Paříž	Paříž	k1gFnSc1	Paříž
vikinskými	vikinský	k2eAgMnPc7d1	vikinský
nájezdníky	nájezdník	k1gMnPc7	nájezdník
dobyta	dobyt	k2eAgFnSc1d1	dobyta
<g/>
.	.	kIx.	.
</s>
<s>
Slabost	slabost	k1gFnSc1	slabost
posledních	poslední	k2eAgMnPc2d1	poslední
karolínských	karolínský	k2eAgMnPc2d1	karolínský
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
postupnému	postupný	k2eAgInSc3d1	postupný
nárůstu	nárůst	k1gInSc3	nárůst
moci	moc	k1gFnSc2	moc
pařížských	pařížský	k2eAgNnPc2d1	pařížské
hrabat	hrabě	k1gNnPc2	hrabě
<g/>
.	.	kIx.	.
</s>
<s>
Odo	Odo	k?	Odo
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
pařížský	pařížský	k2eAgMnSc1d1	pařížský
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
králem	král	k1gMnSc7	král
Francie	Francie	k1gFnSc1	Francie
navzdory	navzdory	k7c3	navzdory
nárokům	nárok	k1gInPc3	nárok
Karla	Karel	k1gMnSc2	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Dynastický	dynastický	k2eAgInSc1d1	dynastický
spor	spor	k1gInSc1	spor
byl	být	k5eAaImAgInS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
987	[number]	k4	987
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Hugo	Hugo	k1gMnSc1	Hugo
Kapet	Kapet	k1gMnSc1	Kapet
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
pařížský	pařížský	k2eAgMnSc1d1	pařížský
<g/>
,	,	kIx,	,
zvolen	zvolen	k2eAgMnSc1d1	zvolen
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
posledního	poslední	k2eAgMnSc2d1	poslední
krále	král	k1gMnSc2	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Karlovců	Karlovac	k1gInPc2	Karlovac
za	za	k7c4	za
francouzského	francouzský	k2eAgMnSc4d1	francouzský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
také	také	k9	také
na	na	k7c4	na
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Augusta	August	k1gMnSc2	August
město	město	k1gNnSc1	město
dále	daleko	k6eAd2	daleko
mohutně	mohutně	k6eAd1	mohutně
rostlo	růst	k5eAaImAgNnS	růst
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souviselo	souviset	k5eAaImAgNnS	souviset
hlavně	hlavně	k9	hlavně
s	s	k7c7	s
vysušením	vysušení	k1gNnSc7	vysušení
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
močálů	močál	k1gInPc2	močál
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
cesty	cesta	k1gFnPc1	cesta
byly	být	k5eAaImAgFnP	být
dlážděny	dláždit	k5eAaImNgFnP	dláždit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
první	první	k4xOgInSc1	první
Louvre	Louvre	k1gInSc1	Louvre
jako	jako	k8xC	jako
mocná	mocný	k2eAgFnSc1d1	mocná
pevnost	pevnost	k1gFnSc1	pevnost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
i	i	k8xC	i
výstavba	výstavba	k1gFnSc1	výstavba
několika	několik	k4yIc2	několik
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
katedrály	katedrála	k1gFnPc4	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
škol	škola	k1gFnPc2	škola
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
bylo	být	k5eAaImAgNnS	být
sjednoceno	sjednotit	k5eAaPmNgNnS	sjednotit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
studovali	studovat	k5eAaImAgMnP	studovat
mj.	mj.	kA	mj.
Albert	Albert	k1gMnSc1	Albert
Veliký	veliký	k2eAgMnSc1d1	veliký
a	a	k8xC	a
sv.	sv.	kA	sv.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
Paříž	Paříž	k1gFnSc1	Paříž
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
jako	jako	k9	jako
obchodní	obchodní	k2eAgNnSc4d1	obchodní
i	i	k8xC	i
intelektuální	intelektuální	k2eAgNnSc4d1	intelektuální
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
rozvoj	rozvoj	k1gInSc4	rozvoj
dočasně	dočasně	k6eAd1	dočasně
přerušovaly	přerušovat	k5eAaImAgFnP	přerušovat
morové	morový	k2eAgFnPc1d1	morová
epidemie	epidemie	k1gFnPc1	epidemie
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
pak	pak	k6eAd1	pak
zejména	zejména	k9	zejména
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvůr	dvůr	k1gInSc1	dvůr
město	město	k1gNnSc1	město
přechodně	přechodně	k6eAd1	přechodně
dokonce	dokonce	k9	dokonce
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Krále	Král	k1gMnSc2	Král
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
1643	[number]	k4	1643
<g/>
-	-	kIx~	-
<g/>
1715	[number]	k4	1715
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
královská	královský	k2eAgFnSc1d1	královská
rezidence	rezidence	k1gFnSc1	rezidence
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
do	do	k7c2	do
blízkých	blízký	k2eAgFnPc2d1	blízká
Versailles	Versailles	k1gFnPc2	Versailles
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnSc3d1	označovaná
jako	jako	k8xS	jako
Le	Le	k1gMnSc3	Le
Grand	grand	k1gMnSc1	grand
Siè	Siè	k1gMnSc1	Siè
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Velké	velký	k2eAgNnSc1d1	velké
Století	století	k1gNnSc1	století
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
velkolepých	velkolepý	k2eAgInPc2d1	velkolepý
paláců	palác	k1gInPc2	palác
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
náměstí	náměstí	k1gNnSc4	náměstí
Place	plac	k1gInSc6	plac
des	des	k1gNnSc4	des
Victoires	Victoiresa	k1gFnPc2	Victoiresa
a	a	k8xC	a
Place	plac	k1gInSc5	plac
Vendôme	Vendôm	k1gInSc5	Vendôm
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc4	Paříž
jich	on	k3xPp3gInPc2	on
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
měla	mít	k5eAaImAgFnS	mít
pouze	pouze	k6eAd1	pouze
pár	pár	k4xCyI	pár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
také	také	k9	také
zbourány	zbourat	k5eAaPmNgFnP	zbourat
středověké	středověký	k2eAgFnPc1d1	středověká
hradby	hradba	k1gFnPc1	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
začala	začít	k5eAaPmAgFnS	začít
dobytím	dobytí	k1gNnSc7	dobytí
Bastilly	Bastilla	k1gFnSc2	Bastilla
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1789	[number]	k4	1789
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
nestabilitě	nestabilita	k1gFnSc3	nestabilita
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
konzul	konzul	k1gMnSc1	konzul
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
papeže	papež	k1gMnSc2	papež
Pia	Pius	k1gMnSc2	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
korunoval	korunovat	k5eAaBmAgInS	korunovat
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
francouzským	francouzský	k2eAgMnSc7d1	francouzský
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
vytvořit	vytvořit	k5eAaPmF	vytvořit
nejkrásnější	krásný	k2eAgNnSc4d3	nejkrásnější
město	město	k1gNnSc4	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zejména	zejména	k9	zejména
množství	množství	k1gNnSc1	množství
velkolepých	velkolepý	k2eAgInPc2d1	velkolepý
pomníků	pomník	k1gInPc2	pomník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
však	však	k9	však
byla	být	k5eAaImAgFnS	být
Paříž	Paříž	k1gFnSc1	Paříž
dobyta	dobyt	k2eAgFnSc1d1	dobyta
anglickými	anglický	k2eAgMnPc7d1	anglický
<g/>
,	,	kIx,	,
pruskými	pruský	k2eAgMnPc7d1	pruský
<g/>
,	,	kIx,	,
rakouskými	rakouský	k2eAgMnPc7d1	rakouský
a	a	k8xC	a
ruskými	ruský	k2eAgNnPc7d1	ruské
vojsky	vojsko	k1gNnPc7	vojsko
a	a	k8xC	a
Napoleon	napoleon	k1gInSc1	napoleon
byl	být	k5eAaImAgInS	být
vypovězen	vypovědět	k5eAaPmNgInS	vypovědět
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Elbu	Elbus	k1gInSc2	Elbus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
po	po	k7c6	po
útěku	útěk	k1gInSc6	útěk
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
byl	být	k5eAaImAgMnS	být
definitivně	definitivně	k6eAd1	definitivně
sesazen	sesadit	k5eAaPmNgMnS	sesadit
a	a	k8xC	a
deportován	deportovat	k5eAaBmNgMnS	deportovat
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Svaté	svatý	k2eAgFnSc2d1	svatá
Heleny	Helena	k1gFnSc2	Helena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
opět	opět	k6eAd1	opět
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nejistoty	nejistota	k1gFnSc2	nejistota
chopil	chopit	k5eAaPmAgInS	chopit
moci	moct	k5eAaImF	moct
státním	státní	k2eAgInSc7d1	státní
převratem	převrat	k1gInSc7	převrat
Napoleonův	Napoleonův	k2eAgMnSc1d1	Napoleonův
synovec	synovec	k1gMnSc1	synovec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
Napoleon	Napoleon	k1gMnSc1	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
Paříž	Paříž	k1gFnSc1	Paříž
rozkvetla	rozkvetnout	k5eAaPmAgFnS	rozkvetnout
v	v	k7c4	v
nejkrásnější	krásný	k2eAgNnSc4d3	nejkrásnější
město	město	k1gNnSc4	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
vděčí	vděčit	k5eAaImIp3nS	vděčit
především	především	k9	především
baronu	baron	k1gMnSc3	baron
Hausmannovi	Hausmann	k1gMnSc3	Hausmann
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
realizoval	realizovat	k5eAaBmAgMnS	realizovat
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
přeměnu	přeměna	k1gFnSc4	přeměna
středověkého	středověký	k2eAgNnSc2d1	středověké
města	město	k1gNnSc2	město
na	na	k7c4	na
město	město	k1gNnSc4	město
s	s	k7c7	s
vzdušnými	vzdušný	k2eAgInPc7d1	vzdušný
bulváry	bulvár	k1gInPc7	bulvár
a	a	k8xC	a
třídami	třída	k1gFnPc7	třída
<g/>
.	.	kIx.	.
</s>
<s>
Císařství	císařství	k1gNnSc4	císařství
ukončila	ukončit	k5eAaPmAgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
prohraná	prohraný	k2eAgFnSc1d1	prohraná
Prusko-francouzská	pruskorancouzský	k2eAgFnSc1d1	prusko-francouzská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
končící	končící	k2eAgFnSc1d1	končící
obléháním	obléhání	k1gNnSc7	obléhání
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
následovaném	následovaný	k2eAgInSc6d1	následovaný
Pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
komunou	komuna	k1gFnSc7	komuna
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ale	ale	k8xC	ale
představuje	představovat	k5eAaImIp3nS	představovat
opět	opět	k6eAd1	opět
období	období	k1gNnSc1	období
velkého	velký	k2eAgInSc2d1	velký
rozmachu	rozmach	k1gInSc2	rozmach
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
také	také	k9	také
La	la	k1gNnPc1	la
Belle	bell	k1gInSc5	bell
Époque	Époque	k1gNnPc2	Époque
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
Krásné	krásný	k2eAgNnSc1d1	krásné
období	období	k1gNnSc1	období
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
pořádání	pořádání	k1gNnSc4	pořádání
Světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgInSc1d1	dnešní
symbol	symbol	k1gInSc1	symbol
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
výstavným	výstavný	k2eAgNnSc7d1	výstavné
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
plným	plný	k2eAgNnSc7d1	plné
nově	nova	k1gFnSc3	nova
budovaných	budovaný	k2eAgFnPc2d1	budovaná
budov	budova	k1gFnPc2	budova
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
a	a	k8xC	a
1924	[number]	k4	1924
Paříž	Paříž	k1gFnSc1	Paříž
pořádala	pořádat	k5eAaImAgFnS	pořádat
Letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Paříž	Paříž	k1gFnSc1	Paříž
opět	opět	k6eAd1	opět
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
symbolizováno	symbolizovat	k5eAaImNgNnS	symbolizovat
především	především	k9	především
výstavbou	výstavba	k1gFnSc7	výstavba
obchodní	obchodní	k2eAgFnSc2d1	obchodní
čtvrti	čtvrt	k1gFnSc2	čtvrt
La	la	k1gNnSc2	la
Défense	Défense	k1gFnSc2	Défense
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
moderním	moderní	k2eAgMnSc7d1	moderní
administrativním	administrativní	k2eAgMnSc7d1	administrativní
centrem	centr	k1gMnSc7	centr
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
doplněn	doplnit	k5eAaPmNgMnS	doplnit
budovou	budova	k1gFnSc7	budova
La	la	k1gNnSc2	la
Grande	grand	k1gMnSc5	grand
Arche	Arche	k1gNnPc7	Arche
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Velký	velký	k2eAgInSc1d1	velký
Oblouk	oblouk	k1gInSc1	oblouk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ohromná	ohromný	k2eAgFnSc1d1	ohromná
krychle	krychle	k1gFnSc1	krychle
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vešla	vejít	k5eAaPmAgFnS	vejít
i	i	k9	i
katedrála	katedrála	k1gFnSc1	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
stavbou	stavba	k1gFnSc7	stavba
byl	být	k5eAaImAgInS	být
také	také	k9	také
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Tour	Toura	k1gFnPc2	Toura
Montparnasse	Montparnasse	k1gFnSc2	Montparnasse
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
stavby	stavba	k1gFnSc2	stavba
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
posledním	poslední	k2eAgInSc7d1	poslední
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
takto	takto	k6eAd1	takto
postaven	postavit	k5eAaPmNgInS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
totiž	totiž	k9	totiž
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
pro	pro	k7c4	pro
Paříž	Paříž	k1gFnSc4	Paříž
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
že	že	k8xS	že
budovy	budova	k1gFnPc4	budova
nesmí	smět	k5eNaImIp3nS	smět
převýšit	převýšit	k5eAaPmF	převýšit
37	[number]	k4	37
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
historický	historický	k2eAgInSc1d1	historický
ráz	ráz	k1gInSc1	ráz
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
aglomerace	aglomerace	k1gFnSc1	aglomerace
a	a	k8xC	a
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
pánev	pánev	k1gFnSc1	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
pánvi	pánev	k1gFnSc6	pánev
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
řeky	řeka	k1gFnSc2	řeka
Seiny	Seina	k1gFnSc2	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
samotného	samotný	k2eAgNnSc2d1	samotné
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgInPc2d1	pouhý
105,397	[number]	k4	105,397
km	km	kA	km
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
2	[number]	k4	2
211	[number]	k4	211
297	[number]	k4	297
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
21	[number]	k4	21
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
496	[number]	k4	496
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
1	[number]	k4	1
290	[number]	k4	290
211	[number]	k4	211
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
včetně	včetně	k7c2	včetně
přilehlých	přilehlý	k2eAgNnPc2d1	přilehlé
předměstí	předměstí	k1gNnPc2	předměstí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2	[number]	k4	2
723	[number]	k4	723
km	km	kA	km
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
10	[number]	k4	10
197	[number]	k4	197
687	[number]	k4	687
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
sídelní	sídelní	k2eAgFnSc1d1	sídelní
aglomerace	aglomerace	k1gFnSc1	aglomerace
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
14	[number]	k4	14
518	[number]	k4	518
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
území	území	k1gNnSc4	území
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
11	[number]	k4	11
014	[number]	k4	014
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
65	[number]	k4	65
m.	m.	k?	m.
</s>
<s>
Nejnižším	nízký	k2eAgNnSc7d3	nejnižší
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
řeky	řeka	k1gFnSc2	řeka
Seiny	Seina	k1gFnSc2	Seina
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
25	[number]	k4	25
m	m	kA	m
<g/>
,	,	kIx,	,
kolísá	kolísat	k5eAaImIp3nS	kolísat
podle	podle	k7c2	podle
stavu	stav	k1gInSc2	stav
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
u	u	k7c2	u
Pont	Pont	k1gInSc1	Pont
aval	aval	k1gInSc4	aval
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opouští	opouštět	k5eAaImIp3nS	opouštět
město	město	k1gNnSc4	město
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
přirozeným	přirozený	k2eAgInSc7d1	přirozený
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
vrcholek	vrcholek	k1gInSc1	vrcholek
kopce	kopec	k1gInSc2	kopec
Montmartre	Montmartr	k1gInSc5	Montmartr
(	(	kIx(	(
<g/>
130	[number]	k4	130
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
rovněž	rovněž	k9	rovněž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pramení	pramenit	k5eAaImIp3nS	pramenit
řeka	řeka	k1gFnSc1	řeka
Seina	Seina	k1gFnSc1	Seina
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
231	[number]	k4	231
km	km	kA	km
od	od	k7c2	od
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Historickým	historický	k2eAgInSc7d1	historický
středem	střed	k1gInSc7	střed
Paříže	Paříž	k1gFnSc2	Paříž
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
point	pointa	k1gFnPc2	pointa
zéro	zéro	k1gMnSc1	zéro
(	(	kIx(	(
<g/>
nultý	nultý	k4xOgInSc1	nultý
bod	bod	k1gInSc1	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Cité	Citá	k1gFnSc2	Citá
na	na	k7c6	na
Place	plac	k1gInSc6	plac
du	du	k?	du
Parvis-Notre-Dame	Parvis-Notre-Dam	k1gInSc5	Parvis-Notre-Dam
před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
katedrály	katedrála	k1gFnSc2	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
měří	měřit	k5eAaImIp3nS	měřit
všechny	všechen	k3xTgFnPc4	všechen
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Pařížský	pařížský	k2eAgInSc1d1	pařížský
poledník	poledník	k1gInSc1	poledník
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
vyměření	vyměření	k1gNnSc4	vyměření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1718	[number]	k4	1718
dokončil	dokončit	k5eAaPmAgMnS	dokončit
Giovanni	Giovanň	k1gMnSc3	Giovanň
Domenico	Domenico	k1gMnSc1	Domenico
Cassini	Cassin	k2eAgMnPc1d1	Cassin
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
jej	on	k3xPp3gMnSc4	on
François	François	k1gInSc1	François
Arago	Arago	k6eAd1	Arago
upřesnil	upřesnit	k5eAaPmAgInS	upřesnit
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
středem	střed	k1gInSc7	střed
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
observatoře	observatoř	k1gFnSc2	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
konference	konference	k1gFnSc1	konference
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
základním	základní	k2eAgInSc7d1	základní
poledníkem	poledník	k1gInSc7	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Poledník	poledník	k1gInSc1	poledník
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1736	[number]	k4	1736
vyznačen	vyznačit	k5eAaPmNgInS	vyznačit
dvěma	dva	k4xCgInPc7	dva
milníky	milník	k1gInPc7	milník
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Severní	severní	k2eAgInSc1d1	severní
milník	milník	k1gInSc1	milník
<g/>
)	)	kIx)	)
na	na	k7c6	na
Montmartru	Montmartrum	k1gNnSc6	Montmartrum
v	v	k7c6	v
dnes	dnes	k6eAd1	dnes
soukromé	soukromý	k2eAgFnSc6d1	soukromá
zahradě	zahrada	k1gFnSc6	zahrada
u	u	k7c2	u
Moulin	moulin	k2eAgMnSc1d1	moulin
de	de	k?	de
la	la	k0	la
Galette	Galett	k1gInSc5	Galett
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
milník	milník	k1gInSc1	milník
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
přemístěn	přemístit	k5eAaPmNgInS	přemístit
ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
observatoře	observatoř	k1gFnSc2	observatoř
do	do	k7c2	do
parku	park	k1gInSc2	park
Montsouris	Montsouris	k1gFnSc2	Montsouris
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
dvoustého	dvoustý	k2eAgNnSc2d1	dvoustý
výročí	výročí	k1gNnSc2	výročí
narození	narození	k1gNnSc2	narození
Françoise	Françoise	k1gFnSc1	Françoise
Araga	Araga	k1gFnSc1	Araga
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
pás	pás	k1gInSc1	pás
z	z	k7c2	z
135	[number]	k4	135
bronzových	bronzový	k2eAgInPc2d1	bronzový
medailonů	medailon	k1gInPc2	medailon
zasazených	zasazený	k2eAgInPc2d1	zasazený
do	do	k7c2	do
chodníků	chodník	k1gInPc2	chodník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
prochází	procházet	k5eAaImIp3nS	procházet
Pařížský	pařížský	k2eAgInSc4d1	pařížský
poledník	poledník	k1gInSc4	poledník
Paříží	Paříž	k1gFnPc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
umělec	umělec	k1gMnSc1	umělec
Jan	Jan	k1gMnSc1	Jan
Dibbets	Dibbets	k1gInSc4	Dibbets
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
plakety	plaketa	k1gFnPc4	plaketa
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Šifra	šifra	k1gFnSc1	šifra
mistra	mistr	k1gMnSc2	mistr
Leonarda	Leonardo	k1gMnSc2	Leonardo
Dan	Dan	k1gMnSc1	Dan
Brown	Brown	k1gMnSc1	Brown
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
růžovou	růžový	k2eAgFnSc4d1	růžová
linii	linie	k1gFnSc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
pánev	pánev	k1gFnSc1	pánev
se	se	k3xPyFc4	se
utvořila	utvořit	k5eAaPmAgFnS	utvořit
před	před	k7c7	před
41	[number]	k4	41
miliónem	milión	k4xCgInSc7	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
postupně	postupně	k6eAd1	postupně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
velký	velký	k2eAgInSc1d1	velký
soubor	soubor	k1gInSc1	soubor
sedimentárních	sedimentární	k2eAgFnPc2d1	sedimentární
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
šelfových	šelfový	k2eAgFnPc2d1	šelfová
pánví	pánev	k1gFnPc2	pánev
jurského	jurský	k2eAgNnSc2d1	jurské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzniku	vznik	k1gInSc2	vznik
Alp	Alpy	k1gFnPc2	Alpy
byla	být	k5eAaImAgFnS	být
pánev	pánev	k1gFnSc1	pánev
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstala	zůstat	k5eAaPmAgFnS	zůstat
otevřena	otevřít	k5eAaPmNgFnS	otevřít
do	do	k7c2	do
Lamanšského	lamanšský	k2eAgInSc2d1	lamanšský
průlivu	průliv	k1gInSc2	průliv
a	a	k8xC	a
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
předznamenalo	předznamenat	k5eAaPmAgNnS	předznamenat
budoucí	budoucí	k2eAgNnSc1d1	budoucí
povodí	povodí	k1gNnSc1	povodí
Loiry	Loira	k1gFnSc2	Loira
a	a	k8xC	a
Seiny	Seina	k1gFnSc2	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
oligocénu	oligocén	k1gInSc2	oligocén
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
pánev	pánev	k1gFnSc1	pánev
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
vápencem	vápenec	k1gInSc7	vápenec
a	a	k8xC	a
sádrovcem	sádrovec	k1gInSc7	sádrovec
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
písky	písek	k1gInPc1	písek
a	a	k8xC	a
jíly	jíl	k1gInPc1	jíl
(	(	kIx(	(
<g/>
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
zdejších	zdejší	k2eAgMnPc2d1	zdejší
fosilních	fosilní	k2eAgMnPc2d1	fosilní
měkkýšů	měkkýš	k1gMnPc2	měkkýš
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
Jeanu	Jean	k1gMnSc3	Jean
Baptistovi	baptista	k1gMnSc3	baptista
Lamarckovi	Lamarcek	k1gMnSc3	Lamarcek
formulovat	formulovat	k5eAaImF	formulovat
jeho	jeho	k3xOp3gFnSc3	jeho
evoluční	evoluční	k2eAgFnSc3d1	evoluční
teorii	teorie	k1gFnSc3	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Pařížský	pařížský	k2eAgInSc4d1	pařížský
reliéf	reliéf	k1gInSc4	reliéf
se	se	k3xPyFc4	se
formoval	formovat	k5eAaImAgMnS	formovat
erozí	eroze	k1gFnSc7	eroze
vrstev	vrstva	k1gFnPc2	vrstva
z	z	k7c2	z
druhohor	druhohory	k1gFnPc2	druhohory
a	a	k8xC	a
paleogénu	paleogén	k1gInSc2	paleogén
<g/>
.	.	kIx.	.
</s>
<s>
Nejsvrchnější	svrchní	k2eAgFnSc1d3	nejsvrchnější
vrstva	vrstva	k1gFnSc1	vrstva
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
třetihor	třetihory	k1gFnPc2	třetihory
tvoří	tvořit	k5eAaImIp3nP	tvořit
nánosy	nános	k1gInPc1	nános
řeky	řeka	k1gFnSc2	řeka
Seiny	Seina	k1gFnSc2	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
pařížským	pařížský	k2eAgInSc7d1	pařížský
kopcem	kopec	k1gInSc7	kopec
je	být	k5eAaImIp3nS	být
Montmartre	Montmartr	k1gInSc5	Montmartr
(	(	kIx(	(
<g/>
130	[number]	k4	130
m	m	kA	m
<g/>
)	)	kIx)	)
s	s	k7c7	s
dominantou	dominanta	k1gFnSc7	dominanta
Sacré-Cœ	Sacré-Cœ	k1gFnSc2	Sacré-Cœ
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Belleville	Belleville	k1gNnSc1	Belleville
(	(	kIx(	(
<g/>
128,5	[number]	k4	128,5
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ménilmontant	Ménilmontant	k1gMnSc1	Ménilmontant
(	(	kIx(	(
<g/>
108	[number]	k4	108
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Buttes-Chaumont	Buttes-Chaumont	k1gMnSc1	Buttes-Chaumont
(	(	kIx(	(
<g/>
103	[number]	k4	103
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Passy	Pass	k1gInPc1	Pass
(	(	kIx(	(
<g/>
71	[number]	k4	71
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Chaillot	Chaillot	k1gMnSc1	Chaillot
(	(	kIx(	(
<g/>
67	[number]	k4	67
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Montparnasse	Montparnasse	k1gFnPc1	Montparnasse
(	(	kIx(	(
<g/>
66	[number]	k4	66
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Butte	Butt	k1gInSc5	Butt
aux	aux	k?	aux
Cailles	Cailles	k1gInSc1	Cailles
(	(	kIx(	(
<g/>
63	[number]	k4	63
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Montagne	montagne	k1gFnSc1	montagne
Sainte-Geneviè	Sainte-Geneviè	k1gFnSc1	Sainte-Geneviè
(	(	kIx(	(
<g/>
61	[number]	k4	61
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
třemi	tři	k4xCgInPc7	tři
departementy	departement	k1gInPc7	departement
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
tzv.	tzv.	kA	tzv.
malý	malý	k2eAgInSc1d1	malý
prstenec	prstenec	k1gInSc1	prstenec
(	(	kIx(	(
<g/>
la	la	k1gNnSc1	la
petite	petit	k1gInSc5	petit
couronne	couronn	k1gMnSc5	couronn
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
Hauts-de-Seine	Hautse-Sein	k1gInSc5	Hauts-de-Sein
<g/>
,	,	kIx,	,
Seine-Saint-Denis	Seine-Saint-Denis	k1gFnPc6	Seine-Saint-Denis
a	a	k8xC	a
Val-de-Marne	Vale-Marn	k1gInSc5	Val-de-Marn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
123	[number]	k4	123
samostatných	samostatný	k2eAgFnPc2d1	samostatná
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
těchto	tento	k3xDgFnPc2	tento
obcí	obec	k1gFnPc2	obec
nastal	nastat	k5eAaPmAgInS	nastat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
9	[number]	k4	9
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
má	mít	k5eAaImIp3nS	mít
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
i	i	k9	i
pro	pro	k7c4	pro
Paříž	Paříž	k1gFnSc4	Paříž
velký	velký	k2eAgInSc4d1	velký
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
obchodní	obchodní	k2eAgFnSc1d1	obchodní
čtvrť	čtvrť	k1gFnSc1	čtvrť
La	la	k1gNnSc2	la
Défense	Défense	k1gFnSc2	Défense
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Cité	Citá	k1gFnSc2	Citá
byl	být	k5eAaImAgInS	být
osídlen	osídlit	k5eAaPmNgInS	osídlit
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc2	starověk
keltským	keltský	k2eAgInSc7d1	keltský
kmenem	kmen	k1gInSc7	kmen
Parisiů	Parisi	k1gInPc2	Parisi
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c6	na
Seině	Seina	k1gFnSc6	Seina
už	už	k6eAd1	už
jen	jen	k9	jen
dva	dva	k4xCgInPc1	dva
přirozené	přirozený	k2eAgInPc1d1	přirozený
ostrovy	ostrov	k1gInPc1	ostrov
-	-	kIx~	-
Cité	Cité	k1gNnSc1	Cité
a	a	k8xC	a
ostrov	ostrov	k1gInSc1	ostrov
sv.	sv.	kA	sv.
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
ostrov	ostrov	k1gInSc1	ostrov
Cygnes	Cygnes	k1gInSc1	Cygnes
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
přehradní	přehradní	k2eAgFnSc7d1	přehradní
hrází	hráz	k1gFnSc7	hráz
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
mostu	most	k1gInSc2	most
Grenelle	Grenelle	k1gFnSc2	Grenelle
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Cité	Citá	k1gFnSc2	Citá
získal	získat	k5eAaPmAgInS	získat
svou	svůj	k3xOyFgFnSc4	svůj
dnešní	dnešní	k2eAgFnSc4d1	dnešní
rozlohu	rozloha	k1gFnSc4	rozloha
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
mostu	most	k1gInSc2	most
Pont	Pont	k1gInSc1	Pont
Neuf	Neuf	k1gInSc1	Neuf
<g/>
,	,	kIx,	,
když	když	k8xS	když
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
připojeny	připojit	k5eAaPmNgInP	připojit
menší	malý	k2eAgInPc1d2	menší
ostrovy	ostrov	k1gInPc1	ostrov
Juifs	Juifsa	k1gFnPc2	Juifsa
a	a	k8xC	a
Gourdaine	Gourdain	k1gMnSc5	Gourdain
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Square	square	k1gInSc1	square
du	du	k?	du
Vert-Galant	Vert-Galant	k1gMnSc1	Vert-Galant
a	a	k8xC	a
Place	plac	k1gInSc5	plac
Dauphine	dauphin	k1gMnSc5	dauphin
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
sv.	sv.	kA	sv.
Ludvíka	Ludvík	k1gMnSc2	Ludvík
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
a	a	k8xC	a
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
rozdělen	rozdělit	k5eAaPmNgMnS	rozdělit
kanálem	kanál	k1gInSc7	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
île	île	k?	île
aux	aux	k?	aux
Vaches	Vaches	k1gInSc4	Vaches
a	a	k8xC	a
ostrov	ostrov	k1gInSc4	ostrov
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
si	se	k3xPyFc3	se
podržel	podržet	k5eAaPmAgMnS	podržet
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgInP	být
ostrovy	ostrov	k1gInPc1	ostrov
opět	opět	k6eAd1	opět
spojeny	spojen	k2eAgInPc1d1	spojen
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
vybudována	vybudován	k2eAgFnSc1d1	vybudována
rezidenční	rezidenční	k2eAgFnSc1d1	rezidenční
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
dnešní	dnešní	k2eAgInSc1d1	dnešní
název	název	k1gInSc1	název
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1725	[number]	k4	1725
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Bratrství	bratrství	k1gNnSc2	bratrství
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
ostrov	ostrov	k1gInSc1	ostrov
Louviers	Louviersa	k1gFnPc2	Louviersa
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
mezi	mezi	k7c7	mezi
dnešním	dnešní	k2eAgMnSc7d1	dnešní
Boulevardem	Boulevard	k1gMnSc7	Boulevard
Morland	Morlanda	k1gFnPc2	Morlanda
a	a	k8xC	a
Quai	quai	k1gNnSc2	quai
Henri-IV	Henri-IV	k1gFnSc2	Henri-IV
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
pravému	pravý	k2eAgInSc3d1	pravý
břehu	břeh	k1gInSc3	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Maquerelle	Maquerelle	k1gFnSc2	Maquerelle
ležel	ležet	k5eAaImAgInS	ležet
mezi	mezi	k7c7	mezi
Rue	Rue	k1gFnSc7	Rue
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Université	Universitý	k2eAgFnSc2d1	Universitá
a	a	k8xC	a
dnešní	dnešní	k2eAgFnSc7d1	dnešní
Seinou	Seina	k1gFnSc7	Seina
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
pravému	pravý	k2eAgInSc3d1	pravý
břehu	břeh	k1gInSc3	břeh
během	během	k7c2	během
Prvního	první	k4xOgNnSc2	první
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
rovněž	rovněž	k9	rovněž
ostrov	ostrov	k1gInSc4	ostrov
Merdeuse	Merdeuse	k1gFnSc2	Merdeuse
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
ostrov	ostrov	k1gInSc1	ostrov
Cygnes	Cygnes	k1gInSc1	Cygnes
(	(	kIx(	(
<g/>
Labutí	labutí	k2eAgInSc1d1	labutí
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
Champ-de-Mars	Champe-Marsa	k1gFnPc2	Champ-de-Marsa
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
umělý	umělý	k2eAgInSc4d1	umělý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kopie	kopie	k1gFnSc1	kopie
sochy	socha	k1gFnSc2	socha
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
umělé	umělý	k2eAgInPc1d1	umělý
ostrovy	ostrov	k1gInPc1	ostrov
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
jezerech	jezero	k1gNnPc6	jezero
v	v	k7c6	v
parcích	park	k1gInPc6	park
-	-	kIx~	-
Belvédè	Belvédè	k1gMnSc1	Belvédè
v	v	k7c6	v
parku	park	k1gInSc6	park
Buttes-Chaumont	Buttes-Chaumonta	k1gFnPc2	Buttes-Chaumonta
<g/>
,	,	kIx,	,
Bercy	Berca	k1gFnSc2	Berca
a	a	k8xC	a
Reuilly	Reuilla	k1gFnSc2	Reuilla
ve	v	k7c6	v
Vincenneském	Vincenneský	k2eAgInSc6d1	Vincenneský
lesíku	lesík	k1gInSc6	lesík
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seina	Seina	k1gFnSc1	Seina
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Biè	Biè	k1gFnSc6	Biè
a	a	k8xC	a
Pařížské	pařížský	k2eAgInPc1d1	pařížský
vodní	vodní	k2eAgInPc1d1	vodní
kanály	kanál	k1gInPc1	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Seina	Seina	k1gFnSc1	Seina
významně	významně	k6eAd1	významně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
život	život	k1gInSc4	život
města	město	k1gNnSc2	město
už	už	k9	už
od	od	k7c2	od
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrála	hrát	k5eAaImAgFnS	hrát
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
dopravě	doprava	k1gFnSc6	doprava
zboží	zboží	k1gNnSc2	zboží
do	do	k7c2	do
a	a	k8xC	a
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
síť	síť	k1gFnSc1	síť
vodních	vodní	k2eAgInPc2d1	vodní
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
Seinu	Seina	k1gFnSc4	Seina
propojily	propojit	k5eAaPmAgInP	propojit
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Ourcq	Ourcq	k1gFnPc2	Ourcq
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
význam	význam	k1gInSc4	význam
řeky	řeka	k1gFnSc2	řeka
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
města	město	k1gNnSc2	město
nižší	nízký	k2eAgFnSc2d2	nižší
než	než	k8xS	než
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
patří	patřit	k5eAaImIp3nS	patřit
společnost	společnost	k1gFnSc1	společnost
Ports	Portsa	k1gFnPc2	Portsa
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
Pařížské	pařížský	k2eAgInPc1d1	pařížský
přístavy	přístav	k1gInPc1	přístav
<g/>
)	)	kIx)	)
k	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
pařížským	pařížský	k2eAgMnPc3d1	pařížský
podnikům	podnik	k1gInPc3	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
město	město	k1gNnSc4	město
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
nestejné	stejný	k2eNgFnPc4d1	nestejná
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
levý	levý	k2eAgInSc4d1	levý
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
činí	činit	k5eAaImIp3nS	činit
téměř	téměř	k6eAd1	téměř
13	[number]	k4	13
km	km	kA	km
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
mezi	mezi	k7c7	mezi
3,40	[number]	k4	3,40
a	a	k8xC	a
5,70	[number]	k4	5,70
m.	m.	k?	m.
Její	její	k3xOp3gFnSc1	její
šířka	šířka	k1gFnSc1	šířka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
30	[number]	k4	30
m	m	kA	m
(	(	kIx(	(
<g/>
u	u	k7c2	u
Quai	quai	k1gNnSc2	quai
de	de	k?	de
Montebello	Montebello	k1gNnSc1	Montebello
<g/>
)	)	kIx)	)
do	do	k7c2	do
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
u	u	k7c2	u
mostu	most	k1gInSc2	most
Mirabeau	Mirabeaus	k1gInSc2	Mirabeaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
na	na	k7c6	na
Seině	Seina	k1gFnSc6	Seina
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
říční	říční	k2eAgFnSc1d1	říční
linka	linka	k1gFnSc1	linka
Voguéo	Voguéo	k6eAd1	Voguéo
<g/>
.	.	kIx.	.
</s>
<s>
Turistické	turistický	k2eAgFnPc1d1	turistická
lodě	loď	k1gFnPc1	loď
provozují	provozovat	k5eAaImIp3nP	provozovat
společnosti	společnost	k1gFnPc4	společnost
Bateau-mouche	Bateauouche	k1gNnPc2	Bateau-mouche
a	a	k8xC	a
Vedettes	Vedettesa	k1gFnPc2	Vedettesa
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
na	na	k7c6	na
vlnách	vlna	k1gFnPc6	vlna
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
do	do	k7c2	do
Seiny	Seina	k1gFnSc2	Seina
vlévají	vlévat	k5eAaImIp3nP	vlévat
dva	dva	k4xCgInPc1	dva
menší	malý	k2eAgInPc1d2	menší
toky	tok	k1gInPc1	tok
<g/>
.	.	kIx.	.
</s>
<s>
Biè	Biè	k?	Biè
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
pramenící	pramenící	k2eAgFnSc1d1	pramenící
u	u	k7c2	u
města	město	k1gNnSc2	město
Guyancourt	Guyancourta	k1gFnPc2	Guyancourta
a	a	k8xC	a
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
podzemním	podzemní	k2eAgInSc6d1	podzemní
kanálem	kanál	k1gInSc7	kanál
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Seiny	Seina	k1gFnSc2	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Paříže	Paříž	k1gFnSc2	Paříž
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
osídlením	osídlení	k1gNnSc7	osídlení
se	se	k3xPyFc4	se
na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
březích	břeh	k1gInPc6	břeh
stavěla	stavět	k5eAaImAgFnS	stavět
jatka	jatka	k1gFnSc1	jatka
<g/>
,	,	kIx,	,
koželužny	koželužna	k1gFnPc1	koželužna
a	a	k8xC	a
barvírny	barvírna	k1gFnPc1	barvírna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
řeku	řeka	k1gFnSc4	řeka
používaly	používat	k5eAaImAgFnP	používat
jako	jako	k8xS	jako
odpadní	odpadní	k2eAgFnSc4d1	odpadní
stoku	stoka	k1gFnSc4	stoka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
byly	být	k5eAaImAgInP	být
svedeny	svést	k5eAaPmNgInP	svést
i	i	k9	i
další	další	k2eAgFnPc1d1	další
odpadní	odpadní	k2eAgFnPc1d1	odpadní
strouhy	strouha	k1gFnPc1	strouha
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
řeka	řeka	k1gFnSc1	řeka
z	z	k7c2	z
hygienických	hygienický	k2eAgInPc2d1	hygienický
důvodů	důvod	k1gInPc2	důvod
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
svedena	svést	k5eAaPmNgFnS	svést
do	do	k7c2	do
podzemního	podzemní	k2eAgInSc2d1	podzemní
kanálu	kanál	k1gInSc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
před	před	k7c7	před
jejím	její	k3xOp3gInSc7	její
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
odváděla	odvádět	k5eAaImAgFnS	odvádět
voda	voda	k1gFnSc1	voda
do	do	k7c2	do
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
získával	získávat	k5eAaImAgInS	získávat
led	led	k1gInSc1	led
pro	pro	k7c4	pro
pařížské	pařížský	k2eAgFnPc4d1	Pařížská
ledárny	ledárna	k1gFnPc4	ledárna
<g/>
.	.	kIx.	.
</s>
<s>
Obdobný	obdobný	k2eAgInSc1d1	obdobný
osud	osud	k1gInSc1	osud
postihl	postihnout	k5eAaPmAgInS	postihnout
říčku	říčka	k1gFnSc4	říčka
Grange	Grang	k1gMnSc2	Grang
Bateliè	Bateliè	k1gMnSc2	Bateliè
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
z	z	k7c2	z
potoků	potok	k1gInPc2	potok
na	na	k7c4	na
návrší	návrší	k1gNnSc4	návrší
Ménilmontant	Ménilmontant	k1gMnSc1	Ménilmontant
<g/>
,	,	kIx,	,
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
odpadní	odpadní	k2eAgFnSc1d1	odpadní
strouha	strouha	k1gFnSc1	strouha
svedena	sveden	k2eAgFnSc1d1	svedena
do	do	k7c2	do
systému	systém	k1gInSc2	systém
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
stok	stoka	k1gFnPc2	stoka
<g/>
.	.	kIx.	.
</s>
<s>
Vincenneským	Vincenneský	k2eAgInSc7d1	Vincenneský
lesíkem	lesík	k1gInSc7	lesík
protéká	protékat	k5eAaImIp3nS	protékat
uměle	uměle	k6eAd1	uměle
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
potok	potok	k1gInSc1	potok
Gravelle	Gravelle	k1gInSc1	Gravelle
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
propojuje	propojovat	k5eAaImIp3nS	propojovat
tamní	tamní	k2eAgFnPc4d1	tamní
jezera	jezero	k1gNnPc4	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
potoka	potok	k1gInSc2	potok
čerpána	čerpat	k5eAaImNgFnS	čerpat
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Marny	marno	k1gNnPc7	marno
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
protékají	protékat	k5eAaImIp3nP	protékat
Boulogneským	boulogneský	k2eAgInSc7d1	boulogneský
lesíkem	lesík	k1gInSc7	lesík
tři	tři	k4xCgInPc4	tři
potoky	potok	k1gInPc4	potok
Armenonville	Armenonville	k1gFnSc2	Armenonville
<g/>
,	,	kIx,	,
Longchamp	Longchamp	k1gInSc4	Longchamp
a	a	k8xC	a
Sablons	Sablons	k1gInSc4	Sablons
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
vlastníkem	vlastník	k1gMnSc7	vlastník
sítě	síť	k1gFnSc2	síť
vodních	vodní	k2eAgInPc2d1	vodní
průplavů	průplav	k1gInPc2	průplav
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
130	[number]	k4	130
km	km	kA	km
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
řeky	řeka	k1gFnPc4	řeka
Seinu	Seina	k1gFnSc4	Seina
a	a	k8xC	a
Ourcq	Ourcq	k1gFnSc4	Ourcq
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Bassin	Bassin	k1gMnSc1	Bassin
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Arsenal	Arsenal	k1gMnSc1	Arsenal
<g/>
,	,	kIx,	,
Canal	Canal	k1gMnSc1	Canal
Saint-Martin	Saint-Martin	k1gMnSc1	Saint-Martin
<g/>
,	,	kIx,	,
Bassin	Bassin	k2eAgMnSc1d1	Bassin
de	de	k?	de
la	la	k0	la
Villette	Villett	k1gInSc5	Villett
a	a	k8xC	a
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
města	město	k1gNnSc2	město
směřují	směřovat	k5eAaImIp3nP	směřovat
Canal	Canal	k1gInSc4	Canal
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Ourcq	Ourcq	k1gMnSc1	Ourcq
a	a	k8xC	a
Canal	Canal	k1gMnSc1	Canal
Saint-Denis	Saint-Denis	k1gFnSc2	Saint-Denis
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
řadu	řada	k1gFnSc4	řada
technických	technický	k2eAgNnPc2d1	technické
děl	dělo	k1gNnPc2	dělo
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
plavební	plavební	k2eAgFnPc1d1	plavební
komory	komora	k1gFnPc1	komora
nebo	nebo	k8xC	nebo
čerpadla	čerpadlo	k1gNnPc1	čerpadlo
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyhlášky	vyhláška	k1gFnSc2	vyhláška
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1802	[number]	k4	1802
vydané	vydaný	k2eAgNnSc4d1	vydané
Napoleonem	napoleon	k1gInSc7	napoleon
kvůli	kvůli	k7c3	kvůli
zásobování	zásobování	k1gNnSc3	zásobování
Paříže	Paříž	k1gFnSc2	Paříž
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
usnadnění	usnadnění	k1gNnSc1	usnadnění
přepravy	přeprava	k1gFnSc2	přeprava
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slouží	sloužit	k5eAaImIp3nS	sloužit
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
i	i	k9	i
malý	malý	k2eAgInSc1d1	malý
kanál	kanál	k1gInSc1	kanál
Darse	Darse	k1gFnSc2	Darse
du	du	k?	du
fond	fond	k1gInSc1	fond
de	de	k?	de
Rouvray	Rouvraa	k1gFnSc2	Rouvraa
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jsou	být	k5eAaImIp3nP	být
antropogenní	antropogenní	k2eAgNnPc1d1	antropogenní
jezera	jezero	k1gNnPc1	jezero
ve	v	k7c6	v
Vincenneském	Vincenneský	k2eAgInSc6d1	Vincenneský
a	a	k8xC	a
Boulogneském	boulogneský	k2eAgInSc6d1	boulogneský
lesíku	lesík	k1gInSc6	lesík
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Vincennes	Vincennes	k1gInSc1	Vincennes
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
jezera	jezero	k1gNnSc2	jezero
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
Saint-Mandé	Saint-Mandý	k2eAgFnSc2d1	Saint-Mandý
(	(	kIx(	(
<g/>
40	[number]	k4	40
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Daumesnil	Daumesnil	k1gMnSc1	Daumesnil
(	(	kIx(	(
<g/>
12	[number]	k4	12
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Minimes	Minimes	k1gMnSc1	Minimes
(	(	kIx(	(
<g/>
6	[number]	k4	6
ha	ha	kA	ha
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gravelle	Gravelle	k1gFnSc1	Gravelle
(	(	kIx(	(
<g/>
1	[number]	k4	1
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
jezer	jezero	k1gNnPc2	jezero
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
i	i	k8xC	i
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
lesíku	lesík	k1gInSc6	lesík
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
Lac	Lac	k1gFnPc1	Lac
inférieur	inférieura	k1gFnPc2	inférieura
(	(	kIx(	(
<g/>
Dolní	dolní	k2eAgNnSc1d1	dolní
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
11	[number]	k4	11
ha	ha	kA	ha
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lac	Lac	k1gMnSc1	Lac
supérieur	supérieur	k1gMnSc1	supérieur
(	(	kIx(	(
<g/>
Horní	horní	k2eAgNnSc1d1	horní
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
3	[number]	k4	3
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
pět	pět	k4xCc4	pět
rezervoárů	rezervoár	k1gInPc2	rezervoár
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
nádrž	nádrž	k1gFnSc1	nádrž
Montsouris	Montsouris	k1gFnSc1	Montsouris
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
20	[number]	k4	20
%	%	kIx~	%
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
rezervoárem	rezervoár	k1gInSc7	rezervoár
je	být	k5eAaImIp3nS	být
nádrž	nádrž	k1gFnSc1	nádrž
u	u	k7c2	u
nemocnice	nemocnice	k1gFnSc2	nemocnice
sv.	sv.	kA	sv.
Ludvíka	Ludvík	k1gMnSc4	Ludvík
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hydrogeologie	hydrogeologie	k1gFnSc1	hydrogeologie
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
urbanizací	urbanizace	k1gFnSc7	urbanizace
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgInPc1d1	podzemní
toky	tok	k1gInPc1	tok
Biè	Biè	k1gMnSc2	Biè
a	a	k8xC	a
Grange	Grang	k1gMnSc2	Grang
Bateliè	Bateliè	k1gMnSc2	Bateliè
tvoří	tvořit	k5eAaImIp3nP	tvořit
malé	malý	k2eAgInPc4d1	malý
přítoky	přítok	k1gInPc4	přítok
Seiny	Seina	k1gFnSc2	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Biè	Biè	k?	Biè
na	na	k7c6	na
levém	levý	k2eAgNnSc6d1	levé
a	a	k8xC	a
Grange	Grange	k1gNnSc6	Grange
Bateliè	Bateliè	k1gFnSc2	Bateliè
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
hygienických	hygienický	k2eAgInPc2d1	hygienický
důvodů	důvod	k1gInPc2	důvod
zakryty	zakrýt	k5eAaPmNgFnP	zakrýt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtích	čtvrt	k1gFnPc6	čtvrt
Grenelle	Grenelle	k1gNnPc2	Grenelle
<g/>
,	,	kIx,	,
Auteuil	Auteuila	k1gFnPc2	Auteuila
a	a	k8xC	a
Butte	Butt	k1gInSc5	Butt
aux	aux	k?	aux
Cailles	Caillesa	k1gFnPc2	Caillesa
již	již	k6eAd1	již
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
fungují	fungovat	k5eAaImIp3nP	fungovat
artéské	artéský	k2eAgFnPc1d1	artéská
studny	studna	k1gFnPc1	studna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
kašnou	kašna	k1gFnSc7	kašna
napájenou	napájený	k2eAgFnSc7d1	napájená
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
fontána	fontána	k1gFnSc1	fontána
Puits	Puitsa	k1gFnPc2	Puitsa
de	de	k?	de
Grenelle	Grenelle	k1gNnSc4	Grenelle
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
galo-římské	galo-římský	k2eAgFnSc2d1	galo-římský
Lutetie	Lutetie	k1gFnSc2	Lutetie
byla	být	k5eAaImAgFnS	být
zdrojem	zdroj	k1gInSc7	zdroj
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
řeka	řeka	k1gFnSc1	řeka
Seina	Seina	k1gFnSc1	Seina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
kvalita	kvalita	k1gFnSc1	kvalita
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
řeka	řeka	k1gFnSc1	řeka
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
bažiny	bažina	k1gFnPc4	bažina
se	s	k7c7	s
stojatou	stojatý	k2eAgFnSc7d1	stojatá
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Romanizovaná	romanizovaný	k2eAgFnSc1d1	romanizovaná
populace	populace	k1gFnSc1	populace
obývající	obývající	k2eAgFnSc1d1	obývající
pahorek	pahorek	k1gInSc4	pahorek
Sainte-Geneviè	Sainte-Geneviè	k1gFnSc7	Sainte-Geneviè
proto	proto	k8xC	proto
během	běh	k1gInSc7	běh
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vystavěla	vystavět	k5eAaPmAgFnS	vystavět
vlastní	vlastní	k2eAgInSc4d1	vlastní
akvadukt	akvadukt	k1gInSc4	akvadukt
přivádějící	přivádějící	k2eAgInSc4d1	přivádějící
čistou	čistý	k2eAgFnSc4d1	čistá
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Rungis	Rungis	k1gFnSc2	Rungis
a	a	k8xC	a
napájející	napájející	k2eAgFnSc2d1	napájející
mj.	mj.	kA	mj.
veřejné	veřejný	k2eAgFnSc2d1	veřejná
lázně	lázeň	k1gFnSc2	lázeň
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
Cluny	Cluna	k1gFnSc2	Cluna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
akvadukt	akvadukt	k1gInSc1	akvadukt
i	i	k8xC	i
lázně	lázeň	k1gFnPc1	lázeň
zničeny	zničen	k2eAgFnPc4d1	zničena
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Merovejců	Merovejec	k1gMnPc2	Merovejec
a	a	k8xC	a
Karlovců	Karlovac	k1gInPc2	Karlovac
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
opět	opět	k6eAd1	opět
čerpala	čerpat	k5eAaImAgFnS	čerpat
ze	z	k7c2	z
Seiny	Seina	k1gFnSc2	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zdrojem	zdroj	k1gInSc7	zdroj
byly	být	k5eAaImAgInP	být
prameny	pramen	k1gInPc1	pramen
z	z	k7c2	z
pahorků	pahorek	k1gInPc2	pahorek
v	v	k7c6	v
Belleville	Bellevilla	k1gFnSc6	Bellevilla
a	a	k8xC	a
Pré-Saint-Gervais	Pré-Saint-Gervais	k1gFnSc6	Pré-Saint-Gervais
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
využívali	využívat	k5eAaImAgMnP	využívat
mniši	mnich	k1gMnPc1	mnich
v	v	k7c6	v
opatstvích	opatství	k1gNnPc6	opatství
Saint-Laurent	Saint-Laurent	k1gInSc1	Saint-Laurent
a	a	k8xC	a
Saint-Martin-des-Champs	Saint-Martines-Champs	k1gInSc1	Saint-Martin-des-Champs
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
u	u	k7c2	u
svých	svůj	k3xOyFgInPc2	svůj
klášterů	klášter	k1gInPc2	klášter
kašny	kašna	k1gFnSc2	kašna
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Filip	Filip	k1gMnSc1	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
když	když	k8xS	když
založil	založit	k5eAaPmAgMnS	založit
pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
tržnici	tržnice	k1gFnSc4	tržnice
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
zde	zde	k6eAd1	zde
vybudovat	vybudovat	k5eAaPmF	vybudovat
dvě	dva	k4xCgFnPc1	dva
kašny	kašna	k1gFnPc1	kašna
<g/>
,	,	kIx,	,
napájené	napájený	k2eAgFnPc1d1	napájená
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
Prés	Présa	k1gFnPc2	Présa
Saint-Gervais	Saint-Gervais	k1gFnSc2	Saint-Gervais
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
fontána	fontána	k1gFnSc1	fontána
Neviňátek	neviňátko	k1gNnPc2	neviňátko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1605	[number]	k4	1605
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Seiny	Seina	k1gFnSc2	Seina
u	u	k7c2	u
mostu	most	k1gInSc2	most
Pont	Pont	k1gInSc1	Pont
Neuf	Neuf	k1gMnSc1	Neuf
postaveno	postaven	k2eAgNnSc4d1	postaveno
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
zvané	zvaný	k2eAgNnSc4d1	zvané
Samaritánka	samaritánka	k1gFnSc1	samaritánka
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1813	[number]	k4	1813
<g/>
.	.	kIx.	.
</s>
<s>
Pumpa	pumpa	k1gFnSc1	pumpa
vháněla	vhánět	k5eAaImAgFnS	vhánět
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
městských	městský	k2eAgFnPc2d1	městská
kašen	kašna	k1gFnPc2	kašna
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
fungovalo	fungovat	k5eAaImAgNnS	fungovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1673	[number]	k4	1673
<g/>
-	-	kIx~	-
<g/>
1858	[number]	k4	1858
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
u	u	k7c2	u
Pont	Pont	k1gInSc1	Pont
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1623	[number]	k4	1623
byl	být	k5eAaImAgInS	být
zprovozněn	zprovozněn	k2eAgInSc1d1	zprovozněn
tzv.	tzv.	kA	tzv.
Medicejský	Medicejský	k2eAgInSc1d1	Medicejský
akvadukt	akvadukt	k1gInSc1	akvadukt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
nových	nový	k2eAgFnPc2d1	nová
fontán	fontána	k1gFnPc2	fontána
nechal	nechat	k5eAaPmAgMnS	nechat
zřídit	zřídit	k5eAaPmF	zřídit
i	i	k9	i
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
dodnes	dodnes	k6eAd1	dodnes
fungují	fungovat	k5eAaImIp3nP	fungovat
Boucheratova	Boucheratův	k2eAgFnSc1d1	Boucheratův
fontána	fontána	k1gFnSc1	fontána
a	a	k8xC	a
Gaillon	Gaillon	k1gInSc1	Gaillon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
rostl	růst	k5eAaImAgInS	růst
tak	tak	k6eAd1	tak
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Pierre-Paul	Pierre-Paul	k1gInSc1	Pierre-Paul
Riquet	Riquet	k1gInSc1	Riquet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
projektoval	projektovat	k5eAaBmAgInS	projektovat
Canal	Canal	k1gInSc4	Canal
du	du	k?	du
Midi	Mid	k1gFnSc2	Mid
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
výstavbu	výstavba	k1gFnSc4	výstavba
kanálu	kanál	k1gInSc2	kanál
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Ourcq	Ourcq	k1gFnSc2	Ourcq
až	až	k9	až
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
dnešního	dnešní	k2eAgInSc2d1	dnešní
Place	plac	k1gInSc6	plac
de	de	k?	de
la	la	k1gNnSc2	la
Nation	Nation	k1gInSc1	Nation
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1680	[number]	k4	1680
a	a	k8xC	a
diskreditace	diskreditace	k1gFnSc1	diskreditace
jeho	jeho	k3xOp3gMnSc2	jeho
ochránce	ochránce	k1gMnSc2	ochránce
ministra	ministr	k1gMnSc2	ministr
Colberta	Colbert	k1gMnSc2	Colbert
přerušily	přerušit	k5eAaPmAgFnP	přerušit
celý	celý	k2eAgInSc4d1	celý
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
velmi	velmi	k6eAd1	velmi
nákladný	nákladný	k2eAgInSc1d1	nákladný
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
Antoine	Antoin	k1gInSc5	Antoin
Deparcieux	Deparcieux	k1gInSc1	Deparcieux
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1762	[number]	k4	1762
projekt	projekt	k1gInSc1	projekt
k	k	k7c3	k
přivedení	přivedení	k1gNnSc3	přivedení
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Yvette	Yvett	k1gInSc5	Yvett
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
sice	sice	k8xC	sice
plány	plán	k1gInPc4	plán
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
projekt	projekt	k1gInSc1	projekt
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
návrh	návrh	k1gInSc1	návrh
odvést	odvést	k5eAaPmF	odvést
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Biè	Biè	k1gFnSc2	Biè
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
do	do	k7c2	do
Seiny	Seina	k1gFnSc2	Seina
přiváděla	přivádět	k5eAaImAgFnS	přivádět
vodu	voda	k1gFnSc4	voda
silně	silně	k6eAd1	silně
znečištěnou	znečištěný	k2eAgFnSc4d1	znečištěná
při	při	k7c6	při
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
kůží	kůže	k1gFnPc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
projekt	projekt	k1gInSc1	projekt
podpořil	podpořit	k5eAaPmAgInS	podpořit
<g/>
,	,	kIx,	,
práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
záhy	záhy	k6eAd1	záhy
přerušeny	přerušit	k5eAaPmNgFnP	přerušit
tlakem	tlak	k1gInSc7	tlak
silných	silný	k2eAgInPc2d1	silný
cechů	cech	k1gInPc2	cech
koželuhů	koželuh	k1gMnPc2	koželuh
<g/>
,	,	kIx,	,
jirchářů	jirchář	k1gMnPc2	jirchář
a	a	k8xC	a
barvířů	barvíř	k1gMnPc2	barvíř
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
využívali	využívat	k5eAaPmAgMnP	využívat
(	(	kIx(	(
<g/>
a	a	k8xC	a
znečišťovali	znečišťovat	k5eAaImAgMnP	znečišťovat
<g/>
)	)	kIx)	)
řeku	řeka	k1gFnSc4	řeka
a	a	k8xC	a
obávali	obávat	k5eAaImAgMnP	obávat
se	se	k3xPyFc4	se
ohrožení	ohrožení	k1gNnSc3	ohrožení
svých	svůj	k3xOyFgFnPc2	svůj
živností	živnost	k1gFnPc2	živnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
vybudovat	vybudovat	k5eAaPmF	vybudovat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
15	[number]	k4	15
fontán	fontána	k1gFnPc2	fontána
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
např.	např.	kA	např.
fontány	fontána	k1gFnPc1	fontána
Feláhova	feláhův	k2eAgFnSc1d1	feláhův
<g/>
,	,	kIx,	,
Censier	Censier	k1gInSc1	Censier
<g/>
,	,	kIx,	,
Lédina	Lédino	k1gNnPc1	Lédino
<g/>
,	,	kIx,	,
Martova	Martův	k2eAgNnPc1d1	Martovo
<g/>
,	,	kIx,	,
Palmová	palmový	k2eAgNnPc1d1	palmové
nebo	nebo	k8xC	nebo
svatého	svatý	k2eAgMnSc2d1	svatý
Eustacha	Eustach	k1gMnSc2	Eustach
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
kanálu	kanál	k1gInSc2	kanál
Ourcq	Ourcq	k1gFnSc2	Ourcq
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
kanálu	kanál	k1gInSc2	kanál
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
kašen	kašna	k1gFnPc2	kašna
rozvedena	rozvést	k5eAaPmNgFnS	rozvést
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
<g/>
,	,	kIx,	,
kanál	kanál	k1gInSc4	kanál
sám	sám	k3xTgInSc4	sám
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
anglický	anglický	k2eAgMnSc1d1	anglický
filantrop	filantrop	k1gMnSc1	filantrop
Richard	Richard	k1gMnSc1	Richard
Wallace	Wallace	k1gFnSc1	Wallace
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
financoval	financovat	k5eAaBmAgInS	financovat
výstavbu	výstavba	k1gFnSc4	výstavba
tzv.	tzv.	kA	tzv.
Wallaceových	Wallaceův	k2eAgFnPc2d1	Wallaceova
fontán	fontána	k1gFnPc2	fontána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
spravuje	spravovat	k5eAaImIp3nS	spravovat
pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
vodovodní	vodovodní	k2eAgFnSc4d1	vodovodní
síť	síť	k1gFnSc4	síť
městská	městský	k2eAgFnSc1d1	městská
společnost	společnost	k1gFnSc1	společnost
Eau	Eau	k1gFnSc2	Eau
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
zásobárnou	zásobárna	k1gFnSc7	zásobárna
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
nádrž	nádrž	k1gFnSc1	nádrž
Montsouris	Montsouris	k1gFnSc2	Montsouris
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
202	[number]	k4	202
000	[number]	k4	000
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
přiváděna	přivádět	k5eAaImNgFnS	přivádět
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Évry	Évr	k1gMnPc4	Évr
a	a	k8xC	a
Orly	Orel	k1gMnPc4	Orel
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Podnebí	podnebí	k1gNnSc2	podnebí
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
podnebném	podnebný	k2eAgInSc6d1	podnebný
pásu	pás	k1gInSc6	pás
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
oceánickým	oceánický	k2eAgNnSc7d1	oceánické
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
relativně	relativně	k6eAd1	relativně
chladná	chladný	k2eAgNnPc1d1	chladné
léta	léto	k1gNnPc1	léto
(	(	kIx(	(
<g/>
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
)	)	kIx)	)
a	a	k8xC	a
mírné	mírný	k2eAgFnPc1d1	mírná
zimy	zima	k1gFnPc1	zima
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Častý	častý	k2eAgMnSc1d1	častý
je	být	k5eAaImIp3nS	být
déšť	déšť	k1gInSc4	déšť
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
ročních	roční	k2eAgNnPc6d1	roční
obdobích	období	k1gNnPc6	období
a	a	k8xC	a
měnící	měnící	k2eAgNnSc1d1	měnící
se	se	k3xPyFc4	se
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Urbanistický	urbanistický	k2eAgInSc1d1	urbanistický
vývoj	vývoj	k1gInSc1	vývoj
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zvýšení	zvýšení	k1gNnSc4	zvýšení
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
pokles	pokles	k1gInSc1	pokles
počtu	počet	k1gInSc2	počet
dnů	den	k1gInPc2	den
s	s	k7c7	s
mlhou	mlha	k1gFnSc7	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
1630	[number]	k4	1630
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hodin	hodina	k1gFnPc2	hodina
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
francouzský	francouzský	k2eAgInSc4d1	francouzský
průměr	průměr	k1gInSc4	průměr
činí	činit	k5eAaImIp3nP	činit
1973	[number]	k4	1973
hodiny	hodina	k1gFnPc1	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
činí	činit	k5eAaImIp3nP	činit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
641	[number]	k4	641
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
a	a	k8xC	a
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
měsících	měsíc	k1gInPc6	měsíc
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
vyrovnané	vyrovnaný	k2eAgFnPc1d1	vyrovnaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
je	být	k5eAaImIp3nS	být
111	[number]	k4	111
deštivých	deštivý	k2eAgInPc2d1	deštivý
dnů	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
deště	dešť	k1gInPc1	dešť
nejsou	být	k5eNaImIp3nP	být
prudké	prudký	k2eAgInPc1d1	prudký
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vzácný	vzácný	k2eAgMnSc1d1	vzácný
(	(	kIx(	(
<g/>
padá	padat	k5eAaImIp3nS	padat
průměrně	průměrně	k6eAd1	průměrně
jen	jen	k9	jen
15	[number]	k4	15
dní	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
)	)	kIx)	)
a	a	k8xC	a
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
vydrží	vydržet	k5eAaPmIp3nS	vydržet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
překročí	překročit	k5eAaPmIp3nS	překročit
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
43	[number]	k4	43
dny	den	k1gInPc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
a	a	k8xC	a
devětkrát	devětkrát	k6eAd1	devětkrát
ročně	ročně	k6eAd1	ročně
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Vítr	vítr	k1gInSc1	vítr
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
mírný	mírný	k2eAgInSc1d1	mírný
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
dnů	den	k1gInPc2	den
s	s	k7c7	s
nárazy	náraz	k1gInPc7	náraz
přesahujícími	přesahující	k2eAgInPc7d1	přesahující
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vane	vanout	k5eAaImIp3nS	vanout
převážně	převážně	k6eAd1	převážně
západním	západní	k2eAgInSc7d1	západní
a	a	k8xC	a
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
během	během	k7c2	během
vichřice	vichřice	k1gFnSc2	vichřice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
meteorologické	meteorologický	k2eAgFnSc6d1	meteorologická
stanici	stanice	k1gFnSc6	stanice
v	v	k7c6	v
parku	park	k1gInSc6	park
Montsouris	Montsouris	k1gFnPc2	Montsouris
zaznamenány	zaznamenán	k2eAgInPc1d1	zaznamenán
poryvy	poryv	k1gInPc1	poryv
větru	vítr	k1gInSc2	vítr
až	až	k9	až
169	[number]	k4	169
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
překročily	překročit	k5eAaPmAgFnP	překročit
dokonce	dokonce	k9	dokonce
220	[number]	k4	220
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgInSc1d1	absolutní
rekord	rekord	k1gInSc1	rekord
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
rychlosti	rychlost	k1gFnSc2	rychlost
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
meteorologických	meteorologický	k2eAgNnPc2d1	meteorologické
měření	měření	k1gNnPc2	měření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgNnSc1d1	životní
prostředí	prostředí	k1gNnSc1	prostředí
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
koncentrací	koncentrace	k1gFnSc7	koncentrace
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
malý	malý	k2eAgInSc4d1	malý
podíl	podíl	k1gInSc4	podíl
veřejné	veřejný	k2eAgFnSc2d1	veřejná
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
činí	činit	k5eAaImIp3nS	činit
jen	jen	k9	jen
5,8	[number]	k4	5,8
m	m	kA	m
<g/>
2	[number]	k4	2
zeleně	zeleně	k6eAd1	zeleně
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
započtení	započtení	k1gNnSc2	započtení
Boulogneského	boulogneský	k2eAgInSc2d1	boulogneský
a	a	k8xC	a
Vincenneského	Vincenneský	k2eAgInSc2d1	Vincenneský
lesíka	lesík	k1gInSc2	lesík
je	být	k5eAaImIp3nS	být
poměr	poměr	k1gInSc1	poměr
14,5	[number]	k4	14,5
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
městského	městský	k2eAgNnSc2d1	Městské
plánování	plánování	k1gNnSc2	plánování
zavedla	zavést	k5eAaPmAgFnS	zavést
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
koeficient	koeficient	k1gInSc1	koeficient
biotopu	biotop	k1gInSc2	biotop
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stavitel	stavitel	k1gMnSc1	stavitel
musí	muset	k5eAaImIp3nS	muset
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
stavbě	stavba	k1gFnSc6	stavba
vyčlenit	vyčlenit	k5eAaPmF	vyčlenit
část	část	k1gFnSc4	část
pozemku	pozemek	k1gInSc2	pozemek
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
plochu	plocha	k1gFnSc4	plocha
přiléhající	přiléhající	k2eAgFnSc4d1	přiléhající
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Znečištění	znečištění	k1gNnSc1	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
regionu	region	k1gInSc6	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
monitoruje	monitorovat	k5eAaImIp3nS	monitorovat
od	od	k7c2	od
1979	[number]	k4	1979
společnost	společnost	k1gFnSc1	společnost
Airparif	Airparif	k1gInSc1	Airparif
<g/>
.	.	kIx.	.
</s>
<s>
Znečištění	znečištění	k1gNnSc1	znečištění
souvisí	souviset	k5eAaImIp3nS	souviset
především	především	k9	především
s	s	k7c7	s
dopravou	doprava	k1gFnSc7	doprava
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
málo	málo	k6eAd1	málo
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
oceánické	oceánický	k2eAgNnSc1d1	oceánické
klima	klima	k1gNnSc1	klima
obecně	obecně	k6eAd1	obecně
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
rozptylu	rozptyl	k1gInSc3	rozptyl
znečišťujících	znečišťující	k2eAgFnPc2d1	znečišťující
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
při	při	k7c6	při
anticyklonálním	anticyklonální	k2eAgNnSc6d1	anticyklonální
počasí	počasí	k1gNnSc6	počasí
se	se	k3xPyFc4	se
škodliviny	škodlivina	k1gFnPc1	škodlivina
hromadí	hromadit	k5eAaImIp3nP	hromadit
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
hladina	hladina	k1gFnSc1	hladina
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnPc1d1	letní
podmínky	podmínka	k1gFnPc1	podmínka
(	(	kIx(	(
<g/>
vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
a	a	k8xC	a
sluneční	sluneční	k2eAgInSc1d1	sluneční
svit	svit	k1gInSc1	svit
<g/>
)	)	kIx)	)
podporují	podporovat	k5eAaImIp3nP	podporovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
koncentraci	koncentrace	k1gFnSc4	koncentrace
ozonu	ozon	k1gInSc2	ozon
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hladina	hladina	k1gFnSc1	hladina
hluku	hluk	k1gInSc2	hluk
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
radnice	radnice	k1gFnSc2	radnice
sice	sice	k8xC	sice
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
užívání	užívání	k1gNnSc2	užívání
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
provozu	provoz	k1gInSc2	provoz
motorek	motorka	k1gFnPc2	motorka
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
distribuovaná	distribuovaný	k2eAgFnSc1d1	distribuovaná
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
50	[number]	k4	50
%	%	kIx~	%
z	z	k7c2	z
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
a	a	k8xC	a
z	z	k7c2	z
50	[number]	k4	50
%	%	kIx~	%
z	z	k7c2	z
upravených	upravený	k2eAgFnPc2d1	upravená
vod	voda	k1gFnPc2	voda
Seiny	Seina	k1gFnSc2	Seina
a	a	k8xC	a
Marny	marno	k1gNnPc7	marno
<g/>
.	.	kIx.	.
</s>
<s>
Odpadní	odpadní	k2eAgFnSc1d1	odpadní
a	a	k8xC	a
dešťová	dešťový	k2eAgFnSc1d1	dešťová
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
smíšená	smíšený	k2eAgFnSc1d1	smíšená
a	a	k8xC	a
míří	mířit	k5eAaImIp3nS	mířit
do	do	k7c2	do
čistírny	čistírna	k1gFnSc2	čistírna
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
Achè	Achè	k1gFnSc6	Achè
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
INSEE	INSEE	kA	INSEE
tvořila	tvořit	k5eAaImAgFnS	tvořit
populace	populace	k1gFnSc1	populace
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
211	[number]	k4	211
297	[number]	k4	297
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2007	[number]	k4	2007
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
2	[number]	k4	2
193	[number]	k4	193
030	[number]	k4	030
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
relativně	relativně	k6eAd1	relativně
malému	malý	k2eAgNnSc3d1	malé
území	území	k1gNnSc3	území
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
pouhých	pouhý	k2eAgInPc2d1	pouhý
10	[number]	k4	10
540	[number]	k4	540
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
činila	činit	k5eAaImAgFnS	činit
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnPc2	zalidnění
20	[number]	k4	20
980	[number]	k4	980
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km	km	kA	km
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
20	[number]	k4	20
870	[number]	k4	870
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
aglomerace	aglomerace	k1gFnSc1	aglomerace
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
INSEE	INSEE	kA	INSEE
<g/>
)	)	kIx)	)
396	[number]	k4	396
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
počtem	počet	k1gInSc7	počet
10	[number]	k4	10
197	[number]	k4	197
678	[number]	k4	678
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
území	území	k1gNnSc1	území
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2008	[number]	k4	2008
11	[number]	k4	11
899	[number]	k4	899
544	[number]	k4	544
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nS	činit
20	[number]	k4	20
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc1d3	veliký
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
činil	činit	k5eAaImAgInS	činit
podle	podle	k7c2	podle
INSEE	INSEE	kA	INSEE
podíl	podíl	k1gInSc1	podíl
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
do	do	k7c2	do
35	[number]	k4	35
let	léto	k1gNnPc2	léto
46	[number]	k4	46
%	%	kIx~	%
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
celorepublikový	celorepublikový	k2eAgInSc1d1	celorepublikový
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
41,85	[number]	k4	41,85
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Demografie	demografie	k1gFnSc1	demografie
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
netvoří	tvořit	k5eNaImIp3nP	tvořit
samostatný	samostatný	k2eAgInSc4d1	samostatný
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
odvislá	odvislý	k2eAgFnSc1d1	odvislá
od	od	k7c2	od
předměstí	předměstí	k1gNnSc2	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
relativně	relativně	k6eAd1	relativně
malého	malý	k2eAgNnSc2d1	malé
území	území	k1gNnSc2	území
vlastního	vlastní	k2eAgNnSc2d1	vlastní
města	město	k1gNnSc2	město
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
regionu	region	k1gInSc2	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
permanentnímu	permanentní	k2eAgInSc3d1	permanentní
pohybu	pohyb	k1gInSc3	pohyb
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
výrazně	výrazně	k6eAd1	výrazně
klesal	klesat	k5eAaImAgInS	klesat
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
pokles	pokles	k1gInSc1	pokles
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
o	o	k7c4	o
86	[number]	k4	86
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
mezi	mezi	k7c7	mezi
přirozeným	přirozený	k2eAgInSc7d1	přirozený
přírůstkem	přírůstek	k1gInSc7	přírůstek
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
porodností	porodnost	k1gFnSc7	porodnost
a	a	k8xC	a
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
migrací	migrace	k1gFnSc7	migrace
(	(	kIx(	(
<g/>
rozdíl	rozdíl	k1gInSc1	rozdíl
přistěhovanými	přistěhovaný	k2eAgInPc7d1	přistěhovaný
a	a	k8xC	a
odstěhovanými	odstěhovaný	k2eAgInPc7d1	odstěhovaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
relativně	relativně	k6eAd1	relativně
nízký	nízký	k2eAgInSc1d1	nízký
a	a	k8xC	a
nemohl	moct	k5eNaImAgInS	moct
ani	ani	k9	ani
zdaleka	zdaleka	k6eAd1	zdaleka
vyvážit	vyvážit	k5eAaPmF	vyvážit
čistou	čistý	k2eAgFnSc4d1	čistá
migraci	migrace	k1gFnSc4	migrace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
negativní	negativní	k2eAgInSc4d1	negativní
(	(	kIx(	(
<g/>
počet	počet	k1gInSc4	počet
vystěhovaných	vystěhovaný	k2eAgInPc2d1	vystěhovaný
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
bilancemi	bilance	k1gFnPc7	bilance
pak	pak	k6eAd1	pak
tvořil	tvořit	k5eAaImAgInS	tvořit
celkové	celkový	k2eAgNnSc4d1	celkové
záporné	záporný	k2eAgNnSc4d1	záporné
saldo	saldo	k1gNnSc4	saldo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
úbytek	úbytek	k1gInSc1	úbytek
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
počet	počet	k1gInSc1	počet
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
porodnost	porodnost	k1gFnSc1	porodnost
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
se	s	k7c7	s
14,6	[number]	k4	14,6
‰	‰	k?	‰
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
až	až	k9	až
2008	[number]	k4	2008
nad	nad	k7c7	nad
celostátním	celostátní	k2eAgInSc7d1	celostátní
průměrem	průměr	k1gInSc7	průměr
<g/>
)	)	kIx)	)
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
migrační	migrační	k2eAgInSc1d1	migrační
deficit	deficit	k1gInSc1	deficit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
0,3	[number]	k4	0,3
%	%	kIx~	%
v	v	k7c6	v
období	období	k1gNnSc6	období
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
oproti	oproti	k7c3	oproti
-0,7	-0,7	k4	-0,7
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
-	-	kIx~	-
%	%	kIx~	%
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
-	-	kIx~	-
%	%	kIx~	%
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
a	a	k8xC	a
-	-	kIx~	-
%	%	kIx~	%
v	v	k7c6	v
období	období	k1gNnSc6	období
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Statisticky	statisticky	k6eAd1	statisticky
se	se	k3xPyFc4	se
tak	tak	k9	tak
populace	populace	k1gFnPc1	populace
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
mírně	mírně	k6eAd1	mírně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
a	a	k8xC	a
mládne	mládnout	k5eAaImIp3nS	mládnout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
snižoval	snižovat	k5eAaImAgInS	snižovat
počet	počet	k1gInSc1	počet
bytů	byt	k1gInPc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
obrátil	obrátit	k5eAaPmAgInS	obrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
1	[number]	k4	1
095	[number]	k4	095
090	[number]	k4	090
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
1	[number]	k4	1
110	[number]	k4	110
912	[number]	k4	912
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
1	[number]	k4	1
148	[number]	k4	148
145	[number]	k4	145
bytů	byt	k1gInPc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohyb	pohyb	k1gInSc1	pohyb
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
obecného	obecný	k2eAgInSc2d1	obecný
trendu	trend	k1gInSc2	trend
růstu	růst	k1gInSc2	růst
městského	městský	k2eAgNnSc2d1	Městské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
center	centrum	k1gNnPc2	centrum
aglomerací	aglomerace	k1gFnSc7	aglomerace
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
i	i	k8xC	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
rodin	rodina	k1gFnPc2	rodina
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
výrazně	výrazně	k6eAd1	výrazně
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
soužití	soužití	k1gNnSc2	soužití
více	hodně	k6eAd2	hodně
generací	generace	k1gFnPc2	generace
dospělých	dospělí	k1gMnPc2	dospělí
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
počtu	počet	k1gInSc2	počet
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
celkový	celkový	k2eAgInSc4d1	celkový
nárůst	nárůst	k1gInSc4	nárůst
porodnosti	porodnost	k1gFnSc2	porodnost
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
mnoho	mnoho	k4c4	mnoho
mladých	mladý	k1gMnPc2	mladý
bez	bez	k7c2	bez
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
především	především	k9	především
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
finanční	finanční	k2eAgFnSc3d1	finanční
nedostupnosti	nedostupnost	k1gFnSc3	nedostupnost
větších	veliký	k2eAgInPc2d2	veliký
bytů	byt	k1gInPc2	byt
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
páry	pára	k1gFnPc1	pára
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
je	on	k3xPp3gNnSc4	on
bydlení	bydlení	k1gNnSc4	bydlení
příjemnější	příjemný	k2eAgMnPc1d2	příjemnější
a	a	k8xC	a
levnější	levný	k2eAgMnPc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
58	[number]	k4	58
%	%	kIx~	%
pařížských	pařížský	k2eAgInPc2d1	pařížský
bytů	byt	k1gInPc2	byt
má	mít	k5eAaImIp3nS	mít
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
pokoje	pokoj	k1gInPc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
žije	žít	k5eAaImIp3nS	žít
oproti	oproti	k7c3	oproti
zbytku	zbytek	k1gInSc3	zbytek
země	zem	k1gFnSc2	zem
nadprůměrný	nadprůměrný	k2eAgInSc1d1	nadprůměrný
počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
mladých	mladý	k2eAgMnPc2d1	mladý
pracujících	pracující	k1gMnPc2	pracující
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
starších	starý	k2eAgFnPc2d2	starší
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rodiny	rodina	k1gFnPc1	rodina
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
zde	zde	k6eAd1	zde
na	na	k7c4	na
1	[number]	k4	1
148	[number]	k4	148
720	[number]	k4	720
domácností	domácnost	k1gFnPc2	domácnost
připadalo	připadat	k5eAaPmAgNnS	připadat
501	[number]	k4	501
836	[number]	k4	836
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tvořilo	tvořit	k5eAaImAgNnS	tvořit
1	[number]	k4	1
433	[number]	k4	433
376	[number]	k4	376
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
68	[number]	k4	68
%	%	kIx~	%
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
51,4	[number]	k4	51,4
%	%	kIx~	%
domácností	domácnost	k1gFnPc2	domácnost
tvořila	tvořit	k5eAaImAgFnS	tvořit
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
590	[number]	k4	590
122	[number]	k4	122
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
asi	asi	k9	asi
28	[number]	k4	28
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
Pařížanů	Pařížan	k1gMnPc2	Pařížan
<g/>
.	.	kIx.	.
43	[number]	k4	43
%	%	kIx~	%
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
rodin	rodina	k1gFnPc2	rodina
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bezdětného	bezdětný	k2eAgInSc2d1	bezdětný
páru	pár	k1gInSc2	pár
do	do	k7c2	do
25	[number]	k4	25
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
433	[number]	k4	433
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
39,3	[number]	k4	39,3
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
párů	pár	k1gInPc2	pár
má	mít	k5eAaImIp3nS	mít
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc4	jeden
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
17,6	[number]	k4	17,6
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
jsou	být	k5eAaImIp3nP	být
samoživitelé	samoživitel	k1gMnPc1	samoživitel
(	(	kIx(	(
<g/>
francouzský	francouzský	k2eAgInSc4d1	francouzský
průměr	průměr	k1gInSc4	průměr
je	být	k5eAaImIp3nS	být
13,5	[number]	k4	13,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
70,2	[number]	k4	70,2
%	%	kIx~	%
párů	pár	k1gInPc2	pár
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
27,5	[number]	k4	27,5
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
sezdaných	sezdaný	k2eAgInPc2d1	sezdaný
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
76,9	[number]	k4	76,9
%	%	kIx~	%
párů	pár	k1gInPc2	pár
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
21,5	[number]	k4	21,5
%	%	kIx~	%
párů	pár	k1gInPc2	pár
tvoří	tvořit	k5eAaImIp3nP	tvořit
svobodní	svobodný	k2eAgMnPc1d1	svobodný
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
postižena	postihnout	k5eAaPmNgFnS	postihnout
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rozvodovostí	rozvodovost	k1gFnSc7	rozvodovost
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
připadalo	připadat	k5eAaPmAgNnS	připadat
20,5	[number]	k4	20,5
rozvodu	rozvod	k1gInSc2	rozvod
na	na	k7c4	na
1000	[number]	k4	1000
sňatků	sňatek	k1gInPc2	sňatek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
z	z	k7c2	z
francouzských	francouzský	k2eAgInPc2d1	francouzský
departementů	departement	k1gInPc2	departement
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
většina	většina	k1gFnSc1	většina
registrovaných	registrovaný	k2eAgNnPc2d1	registrované
partnerství	partnerství	k1gNnPc2	partnerství
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
plodnost	plodnost	k1gFnSc1	plodnost
činila	činit	k5eAaImAgFnS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
1,57	[number]	k4	1,57
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
regionálním	regionální	k2eAgInSc7d1	regionální
(	(	kIx(	(
<g/>
2,01	[number]	k4	2,01
<g/>
)	)	kIx)	)
i	i	k8xC	i
celostátním	celostátní	k2eAgInSc6d1	celostátní
(	(	kIx(	(
<g/>
2,0	[number]	k4	2,0
<g/>
)	)	kIx)	)
průměrem	průměr	k1gInSc7	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
dětí	dítě	k1gFnPc2	dítě
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
domácnost	domácnost	k1gFnSc4	domácnost
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
nízký	nízký	k2eAgMnSc1d1	nízký
<g/>
:	:	kIx,	:
43	[number]	k4	43
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgMnPc4	žádný
potomky	potomek	k1gMnPc4	potomek
mladší	mladý	k2eAgMnPc1d2	mladší
25	[number]	k4	25
let	léto	k1gNnPc2	léto
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
25	[number]	k4	25
%	%	kIx~	%
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
8,9	[number]	k4	8,9
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
regionální	regionální	k2eAgFnSc1d1	regionální
(	(	kIx(	(
<g/>
11,8	[number]	k4	11,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
i	i	k9	i
celostátní	celostátní	k2eAgFnSc1d1	celostátní
(	(	kIx(	(
<g/>
9,6	[number]	k4	9,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
průměr	průměr	k1gInSc1	průměr
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
malému	malý	k2eAgNnSc3d1	malé
množství	množství	k1gNnSc3	množství
bydlení	bydlení	k1gNnSc2	bydlení
a	a	k8xC	a
vysokým	vysoký	k2eAgFnPc3d1	vysoká
cenám	cena	k1gFnPc3	cena
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
27	[number]	k4	27
400	[number]	k4	400
€	€	k?	€
průměrného	průměrný	k2eAgInSc2d1	průměrný
příjmu	příjem	k1gInSc2	příjem
na	na	k7c4	na
domácnost	domácnost	k1gFnSc4	domácnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
jsou	být	k5eAaImIp3nP	být
zdejší	zdejší	k2eAgFnPc1d1	zdejší
domácnosti	domácnost	k1gFnPc1	domácnost
nejbohatší	bohatý	k2eAgFnPc1d3	nejbohatší
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
okolní	okolní	k2eAgInPc4d1	okolní
čtyři	čtyři	k4xCgInPc4	čtyři
departementy	departement	k1gInPc4	departement
Hauts-de-Seine	Hautse-Sein	k1gInSc5	Hauts-de-Sein
<g/>
,	,	kIx,	,
Yvelines	Yvelinesa	k1gFnPc2	Yvelinesa
<g/>
,	,	kIx,	,
Essonne	Essonn	k1gInSc5	Essonn
a	a	k8xC	a
Val-de-Marne	Vale-Marn	k1gInSc5	Val-de-Marn
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
příčkách	příčka	k1gFnPc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
koncentrací	koncentrace	k1gFnSc7	koncentrace
vysoce	vysoce	k6eAd1	vysoce
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1	kvalifikovaný
profesionálů	profesionál	k1gMnPc2	profesionál
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
příjmy	příjem	k1gInPc7	příjem
v	v	k7c6	v
regionu	region	k1gInSc6	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nelze	lze	k6eNd1	lze
Paříž	Paříž	k1gFnSc1	Paříž
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
město	město	k1gNnSc4	město
bohatých	bohatý	k2eAgInPc2d1	bohatý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
movitých	movitý	k2eAgMnPc2d1	movitý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zdejší	zdejší	k2eAgFnSc1d1	zdejší
struktura	struktura	k1gFnSc1	struktura
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
smíšená	smíšený	k2eAgFnSc1d1	smíšená
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
indexu	index	k1gInSc2	index
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
jsou	být	k5eAaImIp3nP	být
skutečné	skutečný	k2eAgInPc1d1	skutečný
příjmy	příjem	k1gInPc1	příjem
Pařížanů	Pařížan	k1gMnPc2	Pařížan
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgMnPc1d2	nižší
oproti	oproti	k7c3	oproti
nominálním	nominální	k2eAgMnPc3d1	nominální
<g/>
:	:	kIx,	:
životní	životní	k2eAgInPc1d1	životní
náklady	náklad	k1gInPc1	náklad
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
nájemné	nájemný	k2eAgFnPc1d1	nájemná
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k6eAd1	zvláště
vysoké	vysoký	k2eAgInPc1d1	vysoký
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
potravin	potravina	k1gFnPc2	potravina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
dražší	drahý	k2eAgMnSc1d2	dražší
než	než	k8xS	než
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
skrývá	skrývat	k5eAaImIp3nS	skrývat
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgInPc2d1	vysoký
příjmů	příjem	k1gInPc2	příjem
může	moct	k5eAaImIp3nS	moct
zastínit	zastínit	k5eAaPmF	zastínit
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgInPc2d1	nízký
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Paříže	Paříž	k1gFnSc2	Paříž
10	[number]	k4	10
%	%	kIx~	%
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
příjmů	příjem	k1gInPc2	příjem
představuje	představovat	k5eAaImIp3nS	představovat
50	[number]	k4	50
961	[number]	k4	961
€	€	k?	€
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgInPc1d1	sociální
rozdíly	rozdíl	k1gInPc1	rozdíl
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
Paříže	Paříž	k1gFnSc2	Paříž
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
bohatí	bohatý	k2eAgMnPc1d1	bohatý
<g/>
)	)	kIx)	)
a	a	k8xC	a
obyvateli	obyvatel	k1gMnPc7	obyvatel
východních	východní	k2eAgInPc2d1	východní
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
31	[number]	k4	31
521	[number]	k4	521
€	€	k?	€
na	na	k7c4	na
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
oproti	oproti	k7c3	oproti
19	[number]	k4	19
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvořil	tvořit	k5eAaImAgInS	tvořit
13	[number]	k4	13
759	[number]	k4	759
€	€	k?	€
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgInSc4d3	nejbohatší
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc4	obvod
mají	mít	k5eAaImIp3nP	mít
nejnižší	nízký	k2eAgInSc4d3	nejnižší
průměrný	průměrný	k2eAgInSc4d1	průměrný
příjem	příjem	k1gInSc4	příjem
v	v	k7c4	v
Île-de-France	Îlee-Franec	k1gMnPc4	Île-de-Franec
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c4	mezi
příjmy	příjem	k1gInPc4	příjem
desetiny	desetina	k1gFnSc2	desetina
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
a	a	k8xC	a
desetiny	desetina	k1gFnPc4	desetina
nejchudších	chudý	k2eAgMnPc2d3	nejchudší
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nS	tvořit
poměr	poměr	k1gInSc1	poměr
6,7	[number]	k4	6,7
oproti	oproti	k7c3	oproti
2	[number]	k4	2
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
skupina	skupina	k1gFnSc1	skupina
13	[number]	k4	13
<g/>
×	×	k?	×
bohatší	bohatý	k2eAgFnSc1d2	bohatší
než	než	k8xS	než
skupina	skupina	k1gFnSc1	skupina
nejchudších	chudý	k2eAgMnPc2d3	nejchudší
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
departementem	departement	k1gInSc7	departement
s	s	k7c7	s
největšími	veliký	k2eAgInPc7d3	veliký
sociálními	sociální	k2eAgInPc7d1	sociální
rozdíly	rozdíl	k1gInPc7	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
i	i	k9	i
vytváření	vytváření	k1gNnSc1	vytváření
etnických	etnický	k2eAgNnPc2d1	etnické
a	a	k8xC	a
sociálních	sociální	k2eAgNnPc2d1	sociální
ghett	ghetto	k1gNnPc2	ghetto
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
čtvrtích	čtvrt	k1gFnPc6	čtvrt
jako	jako	k8xS	jako
např.	např.	kA	např.
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Rochechouart	Rochechouarta	k1gFnPc2	Rochechouarta
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgNnSc1d1	sociální
složení	složení	k1gNnSc1	složení
okrajových	okrajový	k2eAgInPc2d1	okrajový
obvodů	obvod	k1gInPc2	obvod
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
složení	složení	k1gNnSc1	složení
sousedících	sousedící	k2eAgNnPc2d1	sousedící
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
západní	západní	k2eAgFnSc1d1	západní
16	[number]	k4	16
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
bohatých	bohatý	k2eAgInPc2d1	bohatý
předměstí	předměstí	k1gNnSc4	předměstí
Boulogne-Billancourt	Boulogne-Billancourta	k1gFnPc2	Boulogne-Billancourta
<g/>
,	,	kIx,	,
Neuilly-sur-Seine	Neuillyur-Sein	k1gInSc5	Neuilly-sur-Sein
nebo	nebo	k8xC	nebo
Levallois-Perret	Levallois-Perreta	k1gFnPc2	Levallois-Perreta
<g/>
,	,	kIx,	,
severní	severní	k2eAgInPc1d1	severní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgInPc1d1	severovýchodní
obvody	obvod	k1gInPc1	obvod
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
chudších	chudý	k2eAgNnPc2d2	chudší
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
třech	tři	k4xCgInPc6	tři
obchodech	obchod	k1gInPc6	obchod
žije	žít	k5eAaImIp3nS	žít
40	[number]	k4	40
%	%	kIx~	%
chudých	chudý	k2eAgInPc2d1	chudý
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zdejších	zdejší	k2eAgFnPc6d1	zdejší
čtvrtích	čtvrt	k1gFnPc6	čtvrt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Goutte	Goutt	k1gInSc5	Goutt
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Or	Or	k1gFnPc1	Or
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
mnohé	mnohý	k2eAgInPc1d1	mnohý
sociální	sociální	k2eAgInPc1d1	sociální
problémy	problém	k1gInPc1	problém
<g/>
:	:	kIx,	:
nízké	nízký	k2eAgNnSc1d1	nízké
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
nebo	nebo	k8xC	nebo
špatný	špatný	k2eAgInSc1d1	špatný
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
32,6	[number]	k4	32,6
%	%	kIx~	%
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
rodin	rodina	k1gFnPc2	rodina
cizinců	cizinec	k1gMnPc2	cizinec
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
francouzského	francouzský	k2eAgInSc2d1	francouzský
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
9,7	[number]	k4	9,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
čtvrti	čtvrt	k1gFnPc1	čtvrt
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
vznikem	vznik	k1gInSc7	vznik
specifických	specifický	k2eAgFnPc2d1	specifická
komunitních	komunitní	k2eAgFnPc2d1	komunitní
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xC	jako
např.	např.	kA	např.
Marais	Marais	k1gInSc1	Marais
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
velká	velká	k1gFnSc1	velká
gay	gay	k1gMnSc1	gay
komunita	komunita	k1gFnSc1	komunita
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
společenství	společenství	k1gNnSc2	společenství
aškenázských	aškenázský	k2eAgMnPc2d1	aškenázský
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
ulice	ulice	k1gFnSc2	ulice
Rue	Rue	k1gMnPc2	Rue
des	des	k1gNnSc2	des
Rosiers	Rosiersa	k1gFnPc2	Rosiersa
již	již	k6eAd1	již
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
zase	zase	k9	zase
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
velká	velký	k2eAgFnSc1d1	velká
asijská	asijský	k2eAgFnSc1d1	asijská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
nezjišťuje	zjišťovat	k5eNaImIp3nS	zjišťovat
etnický	etnický	k2eAgInSc4d1	etnický
původ	původ	k1gInSc4	původ
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
nebo	nebo	k8xC	nebo
náboženské	náboženský	k2eAgNnSc4d1	náboženské
vyznání	vyznání	k1gNnSc4	vyznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získávají	získávat	k5eAaImIp3nP	získávat
se	se	k3xPyFc4	se
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
rodné	rodný	k2eAgFnSc6d1	rodná
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
údajů	údaj	k1gInPc2	údaj
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
multikulturních	multikulturní	k2eAgFnPc2d1	multikulturní
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
19,4	[number]	k4	19,4
%	%	kIx~	%
narozených	narozený	k2eAgFnPc2d1	narozená
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgNnSc2	tento
sčítání	sčítání	k1gNnSc2	sčítání
4,2	[number]	k4	4,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Paříže	Paříž	k1gFnSc2	Paříž
tvořili	tvořit	k5eAaImAgMnP	tvořit
nedávní	dávný	k2eNgMnPc1d1	nedávný
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
(	(	kIx(	(
<g/>
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
přišli	přijít	k5eAaPmAgMnP	přijít
mezi	mezi	k7c7	mezi
sčítáním	sčítání	k1gNnSc7	sčítání
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1990	[number]	k4	1990
a	a	k8xC	a
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
15	[number]	k4	15
%	%	kIx~	%
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
imigrační	imigrační	k2eAgFnSc1d1	imigrační
vlna	vlna	k1gFnSc1	vlna
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
začala	začít	k5eAaPmAgFnS	začít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
německých	německý	k2eAgMnPc2d1	německý
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odcházeli	odcházet	k5eAaImAgMnP	odcházet
kvůli	kvůli	k7c3	kvůli
zemědělské	zemědělský	k2eAgFnSc3d1	zemědělská
krizi	krize	k1gFnSc3	krize
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
migračních	migrační	k2eAgFnPc2d1	migrační
vln	vlna	k1gFnPc2	vlna
pak	pak	k6eAd1	pak
následovalo	následovat	k5eAaImAgNnS	následovat
nepřetržitě	přetržitě	k6eNd1	přetržitě
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
:	:	kIx,	:
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
židé	žid	k1gMnPc1	žid
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
Rusové	Rusové	k2eAgNnSc6d1	Rusové
po	po	k7c6	po
Říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
francouzských	francouzský	k2eAgFnPc2d1	francouzská
kolonií	kolonie	k1gFnPc2	kolonie
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Portugalci	Portugalec	k1gMnPc1	Portugalec
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
sefardští	sefardský	k2eAgMnPc1d1	sefardský
židé	žid	k1gMnPc1	žid
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
zemí	zem	k1gFnPc2	zem
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
také	také	k9	také
Asiati	Asiat	k1gMnPc1	Asiat
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
skupinu	skupina	k1gFnSc4	skupina
imigrantů	imigrant	k1gMnPc2	imigrant
tvořili	tvořit	k5eAaImAgMnP	tvořit
rovněž	rovněž	k9	rovněž
Američané	Američan	k1gMnPc1	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Usídlení	usídlení	k1gNnSc1	usídlení
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
pocitu	pocit	k1gInSc6	pocit
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
ze	z	k7c2	z
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Château-Rouge	Château-Roug	k1gFnSc2	Château-Roug
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Belleville	Bellevilla	k1gFnSc6	Bellevilla
žijí	žít	k5eAaImIp3nP	žít
silné	silný	k2eAgFnPc1d1	silná
komunity	komunita	k1gFnPc1	komunita
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
asijská	asijský	k2eAgFnSc1d1	asijská
čtvrť	čtvrť	k1gFnSc1	čtvrť
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
,	,	kIx,	,
Laosu	Laos	k1gInSc2	Laos
<g/>
,	,	kIx,	,
Thajska	Thajsko	k1gNnSc2	Thajsko
nebo	nebo	k8xC	nebo
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Prévôt	Prévôta	k1gFnPc2	Prévôta
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
Prévôt	Prévôt	k1gMnSc1	Prévôt
des	des	k1gNnSc2	des
marchands	marchandsa	k1gFnPc2	marchandsa
<g/>
,	,	kIx,	,
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
komuna	komuna	k1gFnSc1	komuna
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hôtel	Hôtel	k1gMnSc1	Hôtel
de	de	k?	de
ville	ville	k1gFnSc2	ville
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
dva	dva	k4xCgInPc1	dva
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
existovaly	existovat	k5eAaImAgInP	existovat
až	až	k6eAd1	až
do	do	k7c2	do
období	období	k1gNnSc2	období
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
-	-	kIx~	-
prévôt	prévôt	k1gMnSc1	prévôt
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
představený	představený	k1gMnSc1	představený
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
a	a	k8xC	a
prévôt	prévôt	k5eAaPmF	prévôt
des	des	k1gNnSc4	des
marchands	marchandsa	k1gFnPc2	marchandsa
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
doslova	doslova	k6eAd1	doslova
představený	představený	k1gMnSc1	představený
cechu	cech	k1gInSc2	cech
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
)	)	kIx)	)
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
na	na	k7c6	na
správě	správa	k1gFnSc6	správa
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Prévôt	Prévôt	k1gMnSc1	Prévôt
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
zhruba	zhruba	k6eAd1	zhruba
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
postavení	postavení	k1gNnSc2	postavení
rychtáře	rychtář	k1gMnSc2	rychtář
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
zájmy	zájem	k1gInPc1	zájem
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Pařížské	pařížský	k2eAgNnSc1d1	pařížské
hrabství	hrabství	k1gNnSc1	hrabství
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Huga	Hugo	k1gMnSc2	Hugo
Kapeta	Kapeto	k1gNnSc2	Kapeto
připojeno	připojit	k5eAaPmNgNnS	připojit
ke	k	k7c3	k
královské	královský	k2eAgFnSc3d1	královská
koruně	koruna	k1gFnSc3	koruna
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
zde	zde	k6eAd1	zde
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
správce	správce	k1gMnSc1	správce
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
datum	datum	k1gNnSc1	datum
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
titulu	titul	k1gInSc6	titul
prévôta	prévôt	k1gInSc2	prévôt
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1060	[number]	k4	1060
<g/>
.	.	kIx.	.
</s>
<s>
Pařížský	pařížský	k2eAgInSc1d1	pařížský
prévôt	prévôt	k1gInSc1	prévôt
byl	být	k5eAaImAgInS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
byl	být	k5eAaImAgMnS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
městským	městský	k2eAgMnSc7d1	městský
soudcem	soudce	k1gMnSc7	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
prévôta	prévôt	k1gInSc2	prévôt
postupně	postupně	k6eAd1	postupně
získal	získat	k5eAaPmAgMnS	získat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
pravomocí	pravomoc	k1gFnPc2	pravomoc
jako	jako	k8xS	jako
zástupce	zástupce	k1gMnSc2	zástupce
panovníka	panovník	k1gMnSc2	panovník
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
do	do	k7c2	do
jednání	jednání	k1gNnSc2	jednání
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dotýkala	dotýkat	k5eAaImAgFnS	dotýkat
zájmů	zájem	k1gInPc2	zájem
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
správu	správa	k1gFnSc4	správa
justice	justice	k1gFnSc2	justice
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
zasedání	zasedání	k1gNnSc2	zasedání
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
soudce	soudce	k1gMnSc1	soudce
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
též	též	k9	též
zasedání	zasedání	k1gNnSc1	zasedání
královské	královský	k2eAgFnSc2d1	královská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzoval	potvrzovat	k5eAaImAgMnS	potvrzovat
výnosy	výnos	k1gInPc1	výnos
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
podřízena	podřízen	k2eAgFnSc1d1	podřízena
i	i	k8xC	i
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
pořádek	pořádek	k1gInSc4	pořádek
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
správu	správa	k1gFnSc4	správa
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
městské	městský	k2eAgFnSc2d1	městská
hlídky	hlídka	k1gFnSc2	hlídka
apod.	apod.	kA	apod.
Sídlil	sídlit	k5eAaImAgMnS	sídlit
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Grand	grand	k1gMnSc1	grand
Châtelet	Châtelet	k1gInSc1	Châtelet
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
sloužil	sloužit	k5eAaImAgInS	sloužit
i	i	k9	i
jako	jako	k9	jako
vězení	vězení	k1gNnSc1	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
za	za	k7c2	za
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgMnSc7d1	poslední
pařížským	pařížský	k2eAgMnSc7d1	pařížský
prévôtem	prévôt	k1gMnSc7	prévôt
byl	být	k5eAaImAgMnS	být
Anne	Anne	k1gFnSc4	Anne
Gabriel	Gabriel	k1gMnSc1	Gabriel
Henri	Henr	k1gFnSc2	Henr
Bernard	Bernard	k1gMnSc1	Bernard
de	de	k?	de
Boulainvilliers	Boulainvilliers	k1gInSc1	Boulainvilliers
<g/>
.	.	kIx.	.
</s>
<s>
Prévôt	Prévôt	k1gInSc1	Prévôt
des	des	k1gNnSc2	des
marchands	marchands	k6eAd1	marchands
naopak	naopak	k6eAd1	naopak
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
purkmistrovi	purkmistr	k1gMnSc3	purkmistr
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
městech	město	k1gNnPc6	město
a	a	k8xC	a
vykonával	vykonávat	k5eAaImAgInS	vykonávat
správu	správa	k1gFnSc4	správa
jménem	jméno	k1gNnSc7	jméno
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
funkce	funkce	k1gFnSc2	funkce
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
pařížského	pařížský	k2eAgInSc2d1	pařížský
cechu	cech	k1gInSc2	cech
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Cech	cech	k1gInSc1	cech
měl	mít	k5eAaImAgInS	mít
právo	právo	k1gNnSc4	právo
volit	volit	k5eAaImF	volit
si	se	k3xPyFc3	se
svého	svůj	k3xOyFgMnSc4	svůj
představitele	představitel	k1gMnSc4	představitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
jejich	jejich	k3xOp3gInPc4	jejich
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Mocná	mocný	k2eAgFnSc1d1	mocná
korporace	korporace	k1gFnSc1	korporace
měla	mít	k5eAaImAgFnS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1170	[number]	k4	1170
monopol	monopol	k1gInSc1	monopol
na	na	k7c4	na
dodávky	dodávka	k1gFnPc4	dodávka
zboží	zboží	k1gNnSc2	zboží
do	do	k7c2	do
města	město	k1gNnSc2	město
po	po	k7c6	po
Seině	Seina	k1gFnSc6	Seina
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
upravit	upravit	k5eAaPmF	upravit
správu	správa	k1gFnSc4	správa
města	město	k1gNnSc2	město
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přísežní	přísežní	k2eAgFnSc1d1	přísežní
cechu	cech	k1gInSc6	cech
obchodníků	obchodník	k1gMnPc2	obchodník
stali	stát	k5eAaPmAgMnP	stát
radními	radní	k1gFnPc7	radní
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc1	jejich
vůdce	vůdce	k1gMnSc1	vůdce
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
prévôt	prévôt	k5eAaPmF	prévôt
des	des	k1gNnSc4	des
marchands	marchandsa	k1gFnPc2	marchandsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1263	[number]	k4	1263
cech	cech	k1gInSc4	cech
obchodníků	obchodník	k1gMnPc2	obchodník
poprvé	poprvé	k6eAd1	poprvé
zvolil	zvolit	k5eAaPmAgInS	zvolit
představitele	představitel	k1gMnSc4	představitel
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
byl	být	k5eAaImAgMnS	být
Évrard	Évrard	k1gMnSc1	Évrard
Valenciennes	Valenciennes	k1gMnSc1	Valenciennes
a	a	k8xC	a
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnPc1	jeho
pomocníci	pomocník	k1gMnPc1	pomocník
byli	být	k5eAaImAgMnP	být
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
čtyři	čtyři	k4xCgMnPc1	čtyři
konšelé	konšel	k1gMnPc1	konšel
<g/>
.	.	kIx.	.
</s>
<s>
Kompetence	kompetence	k1gFnSc1	kompetence
prévôta	prévôt	k1gInSc2	prévôt
des	des	k1gNnSc2	des
marchands	marchands	k6eAd1	marchands
byla	být	k5eAaImAgFnS	být
teoreticky	teoreticky	k6eAd1	teoreticky
omezena	omezit	k5eAaPmNgFnS	omezit
jen	jen	k9	jen
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
obchodem	obchod	k1gInSc7	obchod
se	s	k7c7	s
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
získával	získávat	k5eAaImAgMnS	získávat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
zásobování	zásobování	k1gNnSc4	zásobování
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgFnSc2d1	veřejná
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
městské	městský	k2eAgFnSc2d1	městská
daně	daň	k1gFnSc2	daň
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
nad	nad	k7c7	nad
říčním	říční	k2eAgInSc7d1	říční
obchodem	obchod	k1gInSc7	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
moci	moc	k1gFnSc2	moc
tohoto	tento	k3xDgInSc2	tento
úřadu	úřad	k1gInSc2	úřad
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Étienna	Étienn	k1gMnSc2	Étienn
Marcela	Marcel	k1gMnSc2	Marcel
(	(	kIx(	(
<g/>
1354	[number]	k4	1354
<g/>
-	-	kIx~	-
<g/>
1358	[number]	k4	1358
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vedl	vést	k5eAaImAgInS	vést
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
králi	král	k1gMnSc3	král
a	a	k8xC	a
donutil	donutit	k5eAaPmAgInS	donutit
dauphina	dauphin	k1gMnSc4	dauphin
<g/>
,	,	kIx,	,
budoucího	budoucí	k2eAgMnSc4d1	budoucí
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
k	k	k7c3	k
politickým	politický	k2eAgMnPc3d1	politický
ústupkům	ústupek	k1gInPc3	ústupek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Marcelově	Marcelův	k2eAgFnSc6d1	Marcelova
porážce	porážka	k1gFnSc6	porážka
byly	být	k5eAaImAgFnP	být
pravomoci	pravomoc	k1gFnPc1	pravomoc
prévôta	prévôt	k1gInSc2	prévôt
omezeny	omezit	k5eAaPmNgFnP	omezit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1383	[number]	k4	1383
po	po	k7c6	po
povstání	povstání	k1gNnSc6	povstání
maillotinů	maillotin	k1gInPc2	maillotin
proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
dani	daň	k1gFnSc3	daň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
podporoval	podporovat	k5eAaImAgMnS	podporovat
i	i	k9	i
pařížský	pařížský	k2eAgInSc4d1	pařížský
prévôt	prévôt	k1gInSc4	prévôt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
úřad	úřad	k1gInSc1	úřad
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1388	[number]	k4	1388
byl	být	k5eAaImAgInS	být
úřad	úřad	k1gInSc1	úřad
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1412	[number]	k4	1412
byl	být	k5eAaImAgMnS	být
prévôt	prévôt	k1gMnSc1	prévôt
podřízen	podřídit	k5eAaPmNgMnS	podřídit
králi	král	k1gMnPc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Étienne	Étiennout	k5eAaPmIp3nS	Étiennout
Marcel	Marcel	k1gMnSc1	Marcel
rovněž	rovněž	k9	rovněž
umístil	umístit	k5eAaPmAgMnS	umístit
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1357	[number]	k4	1357
do	do	k7c2	do
domu	dům	k1gInSc2	dům
s	s	k7c7	s
podloubím	podloubí	k1gNnSc7	podloubí
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Place	plac	k1gInSc6	plac
de	de	k?	de
Grè	Grè	k1gFnSc2	Grè
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
centrum	centrum	k1gNnSc1	centrum
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
městských	městský	k2eAgFnPc2d1	městská
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
sloužila	sloužit	k5eAaImAgFnS	sloužit
i	i	k9	i
jako	jako	k9	jako
městská	městský	k2eAgFnSc1d1	městská
tržnice	tržnice	k1gFnSc1	tržnice
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
městská	městský	k2eAgFnSc1d1	městská
obec	obec	k1gFnSc1	obec
scházela	scházet	k5eAaImAgFnS	scházet
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
poblíž	poblíž	k7c2	poblíž
pevnosti	pevnost	k1gFnSc2	pevnost
Petit	petit	k1gInSc1	petit
Châtelet	Châtelet	k1gInSc1	Châtelet
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
ulice	ulice	k1gFnSc2	ulice
Rue	Rue	k1gMnSc1	Rue
Soufflot	Soufflot	k1gMnSc1	Soufflot
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
Montagne	montagne	k1gFnSc4	montagne
Sainte-Geneviè	Sainte-Geneviè	k1gFnSc2	Sainte-Geneviè
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
středověký	středověký	k2eAgInSc1d1	středověký
dům	dům	k1gInSc1	dům
nahradil	nahradit	k5eAaPmAgInS	nahradit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
renesanční	renesanční	k2eAgInSc4d1	renesanční
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1533	[number]	k4	1533
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
až	až	k9	až
roku	rok	k1gInSc2	rok
1628	[number]	k4	1628
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1850	[number]	k4	1850
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
rozšíření	rozšíření	k1gNnSc1	rozšíření
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
byla	být	k5eAaImAgFnS	být
zachována	zachován	k2eAgFnSc1d1	zachována
renesanční	renesanční	k2eAgFnSc1d1	renesanční
fasáda	fasáda	k1gFnSc1	fasáda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bojích	boj	k1gInPc6	boj
za	za	k7c2	za
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
komuny	komuna	k1gFnSc2	komuna
radnici	radnice	k1gFnSc4	radnice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
podpálili	podpálit	k5eAaPmAgMnP	podpálit
komunardi	komunard	k1gMnPc1	komunard
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
i	i	k9	i
s	s	k7c7	s
městským	městský	k2eAgInSc7d1	městský
archivem	archiv	k1gInSc7	archiv
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
obnovena	obnovit	k5eAaPmNgNnP	obnovit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
prévôta	prévôta	k1gFnSc1	prévôta
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
obsazována	obsazován	k2eAgFnSc1d1	obsazována
královskými	královský	k2eAgMnPc7d1	královský
úředníky	úředník	k1gMnPc7	úředník
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
práva	právo	k1gNnSc2	právo
či	či	k8xC	či
financí	finance	k1gFnPc2	finance
namísto	namísto	k7c2	namísto
pařížských	pařížský	k2eAgMnPc2d1	pařížský
měšťanů	měšťan	k1gMnPc2	měšťan
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
radnice	radnice	k1gFnSc2	radnice
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
osob	osoba	k1gFnPc2	osoba
<g/>
:	:	kIx,	:
prévôt	prévôt	k1gInSc1	prévôt
des	des	k1gNnSc1	des
marchands	marchands	k1gInSc1	marchands
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgMnPc1	čtyři
konšelé	konšel	k1gMnPc1	konšel
<g/>
,	,	kIx,	,
36	[number]	k4	36
poradců	poradce	k1gMnPc2	poradce
<g/>
,	,	kIx,	,
16	[number]	k4	16
zástupců	zástupce	k1gMnPc2	zástupce
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
quartenier	quartenier	k1gInSc1	quartenier
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
prévôtem	prévôt	k1gInSc7	prévôt
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
byl	být	k5eAaImAgMnS	být
Jacques	Jacques	k1gMnSc1	Jacques
de	de	k?	de
Flesselles	Flesselles	k1gMnSc1	Flesselles
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1789	[number]	k4	1789
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1789	[number]	k4	1789
zástupci	zástupce	k1gMnPc1	zástupce
třetího	třetí	k4xOgInSc2	třetí
stavu	stav	k1gInSc2	stav
žádali	žádat	k5eAaImAgMnP	žádat
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
správě	správa	k1gFnSc6	správa
a	a	k8xC	a
vedení	vedení	k1gNnSc6	vedení
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
de	de	k?	de
Flesselles	Flesselles	k1gMnSc1	Flesselles
jejich	jejich	k3xOp3gInPc4	jejich
požadavky	požadavek	k1gInPc4	požadavek
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
jako	jako	k9	jako
nelegální	legální	k2eNgMnSc1d1	nelegální
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1789	[number]	k4	1789
byl	být	k5eAaImAgInS	být
návrh	návrh	k1gInSc1	návrh
opět	opět	k6eAd1	opět
předložen	předložit	k5eAaPmNgInS	předložit
a	a	k8xC	a
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
veřejnosti	veřejnost	k1gFnSc2	veřejnost
přiznal	přiznat	k5eAaPmAgMnS	přiznat
Jacques	Jacques	k1gMnSc1	Jacques
de	de	k?	de
Flesselles	Flesselles	k1gInSc1	Flesselles
účast	účast	k1gFnSc1	účast
12	[number]	k4	12
volených	volený	k2eAgMnPc2d1	volený
zástupců	zástupce	k1gMnPc2	zástupce
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
zasedání	zasedání	k1gNnSc1	zasedání
tohoto	tento	k3xDgNnSc2	tento
shromáždění	shromáždění	k1gNnSc2	shromáždění
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1789	[number]	k4	1789
a	a	k8xC	a
Jacques	Jacques	k1gMnSc1	Jacques
de	de	k?	de
Flesselles	Flesselles	k1gMnSc1	Flesselles
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jeho	jeho	k3xOp3gMnSc7	jeho
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Bastillu	Bastilla	k1gFnSc4	Bastilla
<g/>
.	.	kIx.	.
</s>
<s>
Vzbouřenci	vzbouřenec	k1gMnPc1	vzbouřenec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
hledali	hledat	k5eAaImAgMnP	hledat
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgNnSc4	žádný
nenašli	najít	k5eNaPmAgMnP	najít
a	a	k8xC	a
Flessellese	Flessellese	k1gFnPc4	Flessellese
obvinili	obvinit	k5eAaPmAgMnP	obvinit
ze	z	k7c2	z
spojení	spojení	k1gNnSc2	spojení
s	s	k7c7	s
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
tedy	tedy	k9	tedy
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
před	před	k7c7	před
radnicí	radnice	k1gFnSc7	radnice
popraven	popraven	k2eAgMnSc1d1	popraven
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
hlavu	hlava	k1gFnSc4	hlava
nosil	nosit	k5eAaImAgInS	nosit
dav	dav	k1gInSc1	dav
po	po	k7c6	po
ulicích	ulice	k1gFnPc6	ulice
nabodnutou	nabodnutý	k2eAgFnSc4d1	nabodnutá
na	na	k7c4	na
kopí	kopí	k1gNnSc4	kopí
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
funkce	funkce	k1gFnSc1	funkce
prévôta	prévôt	k1gInSc2	prévôt
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
den	den	k1gInSc4	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
první	první	k4xOgMnSc1	první
pařížský	pařížský	k2eAgMnSc1d1	pařížský
starosta	starosta	k1gMnSc1	starosta
Jean	Jean	k1gMnSc1	Jean
Sylvain	Sylvain	k1gMnSc1	Sylvain
Bailly	Bailla	k1gFnSc2	Bailla
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
tzv.	tzv.	kA	tzv.
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
komuny	komuna	k1gFnSc2	komuna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1789	[number]	k4	1789
<g/>
-	-	kIx~	-
<g/>
1795	[number]	k4	1795
představovala	představovat	k5eAaImAgFnS	představovat
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
správní	správní	k2eAgInSc4d1	správní
orgán	orgán	k1gInSc4	orgán
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1795	[number]	k4	1795
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Paříž	Paříž	k1gFnSc1	Paříž
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
12	[number]	k4	12
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
fungovaly	fungovat	k5eAaImAgFnP	fungovat
jako	jako	k9	jako
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
s	s	k7c7	s
vlastními	vlastní	k2eAgFnPc7d1	vlastní
radnicemi	radnice	k1gFnPc7	radnice
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1800	[number]	k4	1800
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
však	však	k9	však
těchto	tento	k3xDgInPc2	tento
12	[number]	k4	12
radnic	radnice	k1gFnPc2	radnice
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
a	a	k8xC	a
obvody	obvod	k1gInPc1	obvod
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
pouhými	pouhý	k2eAgFnPc7d1	pouhá
územně-správními	územněprávní	k2eAgFnPc7d1	územně-správní
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
opět	opět	k6eAd1	opět
jednu	jeden	k4xCgFnSc4	jeden
společnou	společný	k2eAgFnSc4d1	společná
radnici	radnice	k1gFnSc4	radnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
prefekt	prefekt	k1gMnSc1	prefekt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
správu	správa	k1gFnSc4	správa
města	město	k1gNnSc2	město
řídil	řídit	k5eAaImAgMnS	řídit
z	z	k7c2	z
Hôtel	Hôtela	k1gFnPc2	Hôtela
de	de	k?	de
ville	ville	k1gNnSc4	ville
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
funkce	funkce	k1gFnSc1	funkce
starosty	starosta	k1gMnSc2	starosta
byla	být	k5eAaImAgNnP	být
zrušena	zrušit	k5eAaPmNgNnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
rady	rada	k1gFnSc2	rada
zřízené	zřízený	k2eAgFnSc2d1	zřízená
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1884	[number]	k4	1884
stál	stát	k5eAaImAgMnS	stát
prezident	prezident	k1gMnSc1	prezident
volený	volený	k2eAgMnSc1d1	volený
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
jen	jen	k6eAd1	jen
čestná	čestný	k2eAgFnSc1d1	čestná
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
starosty	starosta	k1gMnSc2	starosta
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obnovena	obnovit	k5eAaPmNgFnS	obnovit
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
pokaždé	pokaždé	k6eAd1	pokaždé
však	však	k9	však
jen	jen	k9	jen
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
června	červen	k1gInSc2	červen
1848	[number]	k4	1848
během	během	k7c2	během
únorové	únorový	k2eAgFnSc2d1	únorová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
od	od	k7c2	od
září	září	k1gNnSc2	září
1870	[number]	k4	1870
do	do	k7c2	do
června	červen	k1gInSc2	červen
1871	[number]	k4	1871
za	za	k7c2	za
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
komuny	komuna	k1gFnSc2	komuna
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1859	[number]	k4	1859
upravil	upravit	k5eAaPmAgMnS	upravit
územní	územní	k2eAgNnSc4d1	územní
rozdělení	rozdělení	k1gNnSc4	rozdělení
Paříže	Paříž	k1gFnSc2	Paříž
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
20	[number]	k4	20
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
existují	existovat	k5eAaImIp3nP	existovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
každého	každý	k3xTgInSc2	každý
obvodu	obvod	k1gInSc2	obvod
stál	stát	k5eAaImAgInS	stát
starosta	starosta	k1gMnSc1	starosta
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
přidělenci	přidělenec	k1gMnPc1	přidělenec
(	(	kIx(	(
<g/>
adjoints	adjoints	k1gInSc1	adjoints
<g/>
)	)	kIx)	)
volení	volení	k1gNnSc1	volení
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1871	[number]	k4	1871
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
přidělenců	přidělenec	k1gMnPc2	přidělenec
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
a	a	k8xC	a
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
mandátu	mandát	k1gInSc2	mandát
časově	časově	k6eAd1	časově
neomezil	omezit	k5eNaPmAgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
radnice	radnice	k1gFnSc1	radnice
<g/>
,	,	kIx,	,
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
starostů	starosta	k1gMnPc2	starosta
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
Policejní	policejní	k2eAgFnSc1d1	policejní
prefektura	prefektura	k1gFnSc1	prefektura
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Správu	správa	k1gFnSc4	správa
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
departementu	departement	k1gInSc2	departement
Paříže	Paříž	k1gFnSc2	Paříž
přestavuje	přestavovat	k5eAaImIp3nS	přestavovat
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Mairie	Mairie	k1gFnSc2	Mairie
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
výkonným	výkonný	k2eAgInSc7d1	výkonný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
volená	volený	k2eAgFnSc1d1	volená
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
Conseil	Conseil	k1gMnSc1	Conseil
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
pařížský	pařížský	k2eAgMnSc1d1	pařížský
starosta	starosta	k1gMnSc1	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
sídlem	sídlo	k1gNnSc7	sídlo
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
budova	budova	k1gFnSc1	budova
radnice	radnice	k1gFnSc2	radnice
Hôtel	Hôtel	k1gInSc1	Hôtel
de	de	k?	de
ville	ville	k1gFnSc2	ville
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1964	[number]	k4	1964
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
departement	departement	k1gInSc1	departement
Paříž	Paříž	k1gFnSc4	Paříž
(	(	kIx(	(
<g/>
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
bývalého	bývalý	k2eAgInSc2d1	bývalý
departementu	departement	k1gInSc2	departement
Seine	Sein	k1gInSc5	Sein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
dělí	dělit	k5eAaImIp3nS	dělit
Paříž	Paříž	k1gFnSc4	Paříž
na	na	k7c4	na
obec	obec	k1gFnSc4	obec
a	a	k8xC	a
na	na	k7c4	na
departement	departement	k1gInSc4	departement
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
Pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pravomoci	pravomoc	k1gFnSc2	pravomoc
jak	jak	k8xS	jak
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
samospráva	samospráva	k1gFnSc1	samospráva
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
i	i	k9	i
generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
departement	departement	k1gInSc4	departement
(	(	kIx(	(
<g/>
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc4d1	jediné
město	město	k1gNnSc4	město
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
sobě	se	k3xPyFc3	se
takto	takto	k6eAd1	takto
spojuje	spojovat	k5eAaImIp3nS	spojovat
obec	obec	k1gFnSc1	obec
i	i	k8xC	i
departement	departement	k1gInSc1	departement
<g/>
.	.	kIx.	.
</s>
<s>
Radu	rada	k1gFnSc4	rada
tvořili	tvořit	k5eAaImAgMnP	tvořit
volení	volený	k2eAgMnPc1d1	volený
zástupci	zástupce	k1gMnPc1	zástupce
každého	každý	k3xTgInSc2	každý
obvodu	obvod	k1gInSc2	obvod
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
čestná	čestný	k2eAgFnSc1d1	čestná
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
metropole	metropole	k1gFnSc1	metropole
však	však	k9	však
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jejích	její	k3xOp3gInPc2	její
obvodů	obvod	k1gInPc2	obvod
<g/>
)	)	kIx)	)
stále	stále	k6eAd1	stále
funkci	funkce	k1gFnSc3	funkce
starosty	starosta	k1gMnPc7	starosta
neměla	mít	k5eNaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
stál	stát	k5eAaImAgInS	stát
i	i	k9	i
nadále	nadále	k6eAd1	nadále
prefekt	prefekt	k1gMnSc1	prefekt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
osoba	osoba	k1gFnSc1	osoba
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rada	rada	k1gFnSc1	rada
měla	mít	k5eAaImAgFnS	mít
jen	jen	k9	jen
omezené	omezený	k2eAgFnPc4d1	omezená
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
zákon	zákon	k1gInSc1	zákon
ze	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1975	[number]	k4	1975
obnovil	obnovit	k5eAaPmAgInS	obnovit
funkci	funkce	k1gFnSc4	funkce
starosty	starosta	k1gMnSc2	starosta
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
představitelem	představitel	k1gMnSc7	představitel
města	město	k1gNnSc2	město
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
předsedá	předsedat	k5eAaImIp3nS	předsedat
Pařížské	pařížský	k2eAgFnSc3d1	Pařížská
radě	rada	k1gFnSc3	rada
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
starosta	starosta	k1gMnSc1	starosta
jak	jak	k6eAd1	jak
představitelem	představitel	k1gMnSc7	představitel
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zároveň	zároveň	k6eAd1	zároveň
departementu	departement	k1gInSc2	departement
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
VÚSC	VÚSC	kA	VÚSC
a	a	k8xC	a
pražský	pražský	k2eAgMnSc1d1	pražský
primátor	primátor	k1gMnSc1	primátor
představitelem	představitel	k1gMnSc7	představitel
obce	obec	k1gFnSc2	obec
i	i	k8xC	i
kraje	kraj	k1gInSc2	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
jsou	být	k5eAaImIp3nP	být
pravomoci	pravomoc	k1gFnPc1	pravomoc
pařížského	pařížský	k2eAgMnSc2d1	pařížský
starosty	starosta	k1gMnSc2	starosta
mnohem	mnohem	k6eAd1	mnohem
rozsáhlejší	rozsáhlý	k2eAgFnSc1d2	rozsáhlejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
starostů	starosta	k1gMnPc2	starosta
ostatních	ostatní	k2eAgFnPc2d1	ostatní
francouzských	francouzský	k2eAgFnPc2d1	francouzská
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
obecní	obecní	k2eAgFnPc1d1	obecní
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
a	a	k8xC	a
starostou	starosta	k1gMnSc7	starosta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jacques	Jacques	k1gMnSc1	Jacques
Chirac	Chirac	k1gMnSc1	Chirac
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1982	[number]	k4	1982
vyšel	vyjít	k5eAaPmAgInS	vyjít
tzv.	tzv.	kA	tzv.
zákon	zákon	k1gInSc1	zákon
PLM	PLM	kA	PLM
(	(	kIx(	(
<g/>
Paříž-Lyon-Marseille	Paříž-Lyon-Marseill	k1gMnSc2	Paříž-Lyon-Marseill
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
těmto	tento	k3xDgMnPc3	tento
třem	tři	k4xCgFnPc3	tři
velkým	velký	k2eAgNnPc3d1	velké
městům	město	k1gNnPc3	město
přiznal	přiznat	k5eAaPmAgInS	přiznat
zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
současná	současný	k2eAgFnSc1d1	současná
funkce	funkce	k1gFnSc1	funkce
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
předsedá	předsedat	k5eAaImIp3nS	předsedat
radě	rada	k1gFnSc3	rada
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zároveň	zároveň	k6eAd1	zároveň
nese	nést	k5eAaImIp3nS	nést
povinnosti	povinnost	k1gFnPc4	povinnost
prezidenta	prezident	k1gMnSc2	prezident
generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
departement	departement	k1gInSc4	departement
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
rada	rada	k1gFnSc1	rada
má	mít	k5eAaImIp3nS	mít
163	[number]	k4	163
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
zůstala	zůstat	k5eAaPmAgFnS	zůstat
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
radnici	radnice	k1gFnSc4	radnice
s	s	k7c7	s
voleným	volený	k2eAgMnSc7d1	volený
starostou	starosta	k1gMnSc7	starosta
a	a	k8xC	a
obvodní	obvodní	k2eAgFnSc7d1	obvodní
radou	rada	k1gFnSc7	rada
(	(	kIx(	(
<g/>
conseil	conseil	k1gInSc1	conseil
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
arrondissement	arrondissement	k1gMnSc1	arrondissement
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvodní	obvodní	k2eAgFnPc1d1	obvodní
rady	rada	k1gFnPc1	rada
spravují	spravovat	k5eAaImIp3nP	spravovat
místní	místní	k2eAgFnSc4d1	místní
občanskou	občanský	k2eAgFnSc4d1	občanská
vybavenost	vybavenost	k1gFnSc4	vybavenost
(	(	kIx(	(
<g/>
kulturní	kulturní	k2eAgInSc4d1	kulturní
<g/>
,	,	kIx,	,
společenské	společenský	k2eAgInPc4d1	společenský
a	a	k8xC	a
sportovní	sportovní	k2eAgInPc4d1	sportovní
podniky	podnik	k1gInPc4	podnik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
druh	druh	k1gInSc1	druh
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
komunální	komunální	k2eAgFnSc2d1	komunální
decentralizace	decentralizace	k1gFnSc2	decentralizace
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
prostředníky	prostředník	k1gInPc1	prostředník
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
a	a	k8xC	a
ústředním	ústřední	k2eAgInSc7d1	ústřední
orgánem	orgán	k1gInSc7	orgán
-	-	kIx~	-
pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
radnicí	radnice	k1gFnSc7	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
kompetence	kompetence	k1gFnPc1	kompetence
jsou	být	k5eAaImIp3nP	být
poradní	poradní	k2eAgFnPc1d1	poradní
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
omezeny	omezit	k5eAaPmNgFnP	omezit
na	na	k7c4	na
správu	správa	k1gFnSc4	správa
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnPc1	radnice
obvodů	obvod	k1gInPc2	obvod
nemají	mít	k5eNaImIp3nP	mít
vlastní	vlastní	k2eAgInPc1d1	vlastní
prostředky	prostředek	k1gInPc1	prostředek
ani	ani	k8xC	ani
rozpočet	rozpočet	k1gInSc1	rozpočet
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
starostové	starosta	k1gMnPc1	starosta
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
omezené	omezený	k2eAgFnPc1d1	omezená
pravomoci	pravomoc	k1gFnPc1	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1986	[number]	k4	1986
upravil	upravit	k5eAaPmAgMnS	upravit
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
financování	financování	k1gNnSc4	financování
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Převedl	převést	k5eAaPmAgMnS	převést
rovněž	rovněž	k9	rovněž
na	na	k7c4	na
starostu	starosta	k1gMnSc4	starosta
Paříže	Paříž	k1gFnSc2	Paříž
některé	některý	k3yIgFnPc4	některý
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zákon	zákon	k1gInSc4	zákon
PLM	PLM	kA	PLM
dal	dát	k5eAaPmAgMnS	dát
původně	původně	k6eAd1	původně
policejnímu	policejní	k2eAgMnSc3d1	policejní
prefektovi	prefekt	k1gMnSc3	prefekt
<g/>
:	:	kIx,	:
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
,	,	kIx,	,
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
tržnicemi	tržnice	k1gFnPc7	tržnice
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
veřejného	veřejný	k2eAgInSc2d1	veřejný
městského	městský	k2eAgInSc2d1	městský
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
rada	rada	k1gFnSc1	rada
rovněž	rovněž	k9	rovněž
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
působení	působení	k1gNnSc4	působení
společností	společnost	k1gFnPc2	společnost
Sociétés	Sociétésa	k1gFnPc2	Sociétésa
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
économie	économie	k1gFnPc1	économie
mixte	mixte	k5eAaPmIp2nP	mixte
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
poslední	poslední	k2eAgFnSc4d1	poslední
změnu	změna	k1gFnSc4	změna
přinesl	přinést	k5eAaPmAgInS	přinést
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
posílení	posílení	k1gNnSc1	posílení
pravomocí	pravomoc	k1gFnPc2	pravomoc
obvodních	obvodní	k2eAgFnPc2d1	obvodní
radnic	radnice	k1gFnPc2	radnice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
investic	investice	k1gFnPc2	investice
a	a	k8xC	a
přijímání	přijímání	k1gNnPc2	přijímání
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
přebírá	přebírat	k5eAaImIp3nS	přebírat
starosta	starosta	k1gMnSc1	starosta
Paříže	Paříž	k1gFnSc2	Paříž
rovněž	rovněž	k9	rovněž
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
místní	místní	k2eAgFnSc4d1	místní
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
parkování	parkování	k1gNnSc4	parkování
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
omezily	omezit	k5eAaPmAgFnP	omezit
pravomoci	pravomoc	k1gFnPc1	pravomoc
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
policejní	policejní	k2eAgFnSc2d1	policejní
prefektury	prefektura	k1gFnSc2	prefektura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
tzv.	tzv.	kA	tzv.
rada	rada	k1gFnSc1	rada
čtvrtě	čtvrt	k1gFnSc2	čtvrt
(	(	kIx(	(
<g/>
conseil	conseil	k1gInSc1	conseil
de	de	k?	de
quartier	quartier	k1gInSc1	quartier
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
mohou	moct	k5eAaImIp3nP	moct
ustanovit	ustanovit	k5eAaPmF	ustanovit
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
prosazování	prosazování	k1gNnSc4	prosazování
návrhů	návrh	k1gInPc2	návrh
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
života	život	k1gInSc2	život
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rady	rada	k1gFnPc1	rada
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
připojeny	připojit	k5eAaPmNgInP	připojit
k	k	k7c3	k
radnici	radnice	k1gFnSc3	radnice
příslušného	příslušný	k2eAgInSc2d1	příslušný
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Územní	územní	k2eAgInSc1d1	územní
rozsah	rozsah	k1gInSc1	rozsah
těchto	tento	k3xDgFnPc2	tento
rad	rada	k1gFnPc2	rada
ovšem	ovšem	k9	ovšem
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
odpovídat	odpovídat	k5eAaImF	odpovídat
hranicím	hranice	k1gFnPc3	hranice
administrativních	administrativní	k2eAgFnPc2d1	administrativní
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
Paříž	Paříž	k1gFnSc1	Paříž
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
121	[number]	k4	121
těchto	tento	k3xDgFnPc2	tento
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
rad	rada	k1gFnPc2	rada
určují	určovat	k5eAaImIp3nP	určovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
radnice	radnice	k1gFnPc4	radnice
obvodů	obvod	k1gInPc2	obvod
většinou	většinou	k6eAd1	většinou
podle	podle	k7c2	podle
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
tak	tak	k9	tak
množství	množství	k1gNnSc1	množství
rad	rada	k1gFnPc2	rada
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
obvodu	obvod	k1gInSc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
zatímco	zatímco	k8xS	zatímco
3	[number]	k4	3
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
3	[number]	k4	3
rady	rada	k1gFnSc2	rada
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
jich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
deset	deset	k4xCc1	deset
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
velkých	velký	k2eAgFnPc2d1	velká
městských	městský	k2eAgFnPc2d1	městská
aglomerací	aglomerace	k1gFnPc2	aglomerace
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
jako	jako	k8xC	jako
Lille	Lille	k1gInSc4	Lille
nebo	nebo	k8xC	nebo
Lyon	Lyon	k1gInSc4	Lyon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
světových	světový	k2eAgFnPc2d1	světová
metropolí	metropol	k1gFnPc2	metropol
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
žádný	žádný	k3yNgInSc1	žádný
zastřešující	zastřešující	k2eAgInSc1d1	zastřešující
orgán	orgán	k1gInSc1	orgán
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
Paříž	Paříž	k1gFnSc4	Paříž
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
koordinoval	koordinovat	k5eAaBmAgInS	koordinovat
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
obcemi	obec	k1gFnPc7	obec
na	na	k7c6	na
hustě	hustě	k6eAd1	hustě
osídleném	osídlený	k2eAgNnSc6d1	osídlené
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nedostatek	nedostatek	k1gInSc1	nedostatek
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
především	především	k9	především
při	při	k7c6	při
nepokojích	nepokoj	k1gInPc6	nepokoj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
diskusi	diskuse	k1gFnSc4	diskuse
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
administrativního	administrativní	k2eAgInSc2d1	administrativní
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Velké	velký	k2eAgFnSc2d1	velká
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
správní	správní	k2eAgFnSc1d1	správní
jednotka	jednotka	k1gFnSc1	jednotka
Métropole	Métropole	k1gFnSc2	Métropole
du	du	k?	du
Grand	grand	k1gMnSc1	grand
Paris	Paris	k1gMnSc1	Paris
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
Paříž	Paříž	k1gFnSc4	Paříž
a	a	k8xC	a
obce	obec	k1gFnPc1	obec
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
departementech	departement	k1gInPc6	departement
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zástupců	zástupce	k1gMnPc2	zástupce
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
metropolní	metropolní	k2eAgFnSc6d1	metropolní
radě	rada	k1gFnSc6	rada
(	(	kIx(	(
<g/>
conseil	conseil	k1gInSc1	conseil
métropole	métropole	k1gFnSc2	métropole
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c4	na
90	[number]	k4	90
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
čestných	čestný	k2eAgMnPc2d1	čestný
občanů	občan	k1gMnPc2	občan
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
rada	rada	k1gFnSc1	rada
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
může	moct	k5eAaImIp3nS	moct
udělit	udělit	k5eAaPmF	udělit
čestné	čestný	k2eAgNnSc4d1	čestné
občanství	občanství	k1gNnSc4	občanství
osobě	osoba	k1gFnSc3	osoba
z	z	k7c2	z
kterékoliv	kterýkoliv	k3yIgFnSc2	kterýkoliv
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
o	o	k7c4	o
svobodu	svoboda	k1gFnSc4	svoboda
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
,	,	kIx,	,
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
nebo	nebo	k8xC	nebo
svobodu	svoboda	k1gFnSc4	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
získalo	získat	k5eAaPmAgNnS	získat
toto	tento	k3xDgNnSc1	tento
ocenění	ocenění	k1gNnSc1	ocenění
12	[number]	k4	12
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Ingrid	Ingrid	k1gFnSc1	Ingrid
Betancourtová	Betancourtová	k1gFnSc1	Betancourtová
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aun	Aun	k1gMnSc1	Aun
Schan	Schan	k1gMnSc1	Schan
Su	Su	k?	Su
Ťij	Ťij	k1gMnSc1	Ťij
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dalajláma	dalajláma	k1gMnSc1	dalajláma
Tändzin	Tändzin	k1gMnSc1	Tändzin
Gjamccho	Gjamccha	k1gFnSc5	Gjamccha
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Džafar	Džafar	k1gMnSc1	Džafar
Panahí	Panahí	k1gMnSc1	Panahí
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
starostů	starosta	k1gMnPc2	starosta
pařížských	pařížský	k2eAgInPc2d1	pařížský
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Komunální	komunální	k2eAgFnPc1d1	komunální
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
podle	podle	k7c2	podle
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
obvod	obvod	k1gInSc1	obvod
volí	volit	k5eAaImIp3nS	volit
své	svůj	k3xOyFgFnPc4	svůj
obvodní	obvodní	k2eAgFnPc4d1	obvodní
rady	rada	k1gFnPc4	rada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
celkem	celkem	k6eAd1	celkem
517	[number]	k4	517
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Obvodní	obvodní	k2eAgFnPc1d1	obvodní
rady	rada	k1gFnPc1	rada
volí	volit	k5eAaImIp3nP	volit
své	svůj	k3xOyFgMnPc4	svůj
starosty	starosta	k1gMnPc4	starosta
týden	týden	k1gInSc1	týden
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
francouzských	francouzský	k2eAgFnPc6d1	francouzská
obcích	obec	k1gFnPc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
obvodu	obvod	k1gInSc2	obvod
se	se	k3xPyFc4	se
také	také	k9	také
volí	volit	k5eAaImIp3nP	volit
zástupci	zástupce	k1gMnPc1	zástupce
do	do	k7c2	do
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pak	pak	k6eAd1	pak
volí	volit	k5eAaImIp3nS	volit
starostu	starosta	k1gMnSc4	starosta
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
šestileté	šestiletý	k2eAgNnSc1d1	šestileté
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
rady	rada	k1gFnSc2	rada
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
městský	městský	k2eAgInSc4d1	městský
obvod	obvod	k1gInSc4	obvod
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
a	a	k8xC	a
přímých	přímý	k2eAgFnPc6d1	přímá
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
kolech	kolo	k1gNnPc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zástupců	zástupce	k1gMnPc2	zástupce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
obvodů	obvod	k1gInPc2	obvod
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
17	[number]	k4	17
osob	osoba	k1gFnPc2	osoba
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
obvod	obvod	k1gInSc4	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvním	první	k4xOgInSc6	první
zasedání	zasedání	k1gNnSc6	zasedání
je	být	k5eAaImIp3nS	být
tajným	tajný	k2eAgNnSc7d1	tajné
hlasováním	hlasování	k1gNnSc7	hlasování
zvolen	zvolen	k2eAgMnSc1d1	zvolen
starosta	starosta	k1gMnSc1	starosta
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
musí	muset	k5eAaImIp3nS	muset
získat	získat	k5eAaPmF	získat
absolutní	absolutní	k2eAgFnSc4d1	absolutní
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
kolech	kolo	k1gNnPc6	kolo
nebo	nebo	k8xC	nebo
relativní	relativní	k2eAgFnSc4d1	relativní
většinu	většina	k1gFnSc4	většina
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
starostovi	starosta	k1gMnSc3	starosta
přidělenci	přidělenec	k1gMnSc3	přidělenec
(	(	kIx(	(
<g/>
adjoints	adjoints	k6eAd1	adjoints
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
nesmí	smět	k5eNaImIp3nS	smět
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
48	[number]	k4	48
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
radou	rada	k1gFnSc7	rada
podle	podle	k7c2	podle
stranických	stranický	k2eAgFnPc2d1	stranická
kandidátek	kandidátka	k1gFnPc2	kandidátka
s	s	k7c7	s
absolutní	absolutní	k2eAgFnSc7d1	absolutní
většinou	většina	k1gFnSc7	většina
<g/>
.	.	kIx.	.
</s>
<s>
Departement	departement	k1gInSc1	departement
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
21	[number]	k4	21
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
obvod	obvod	k1gInSc4	obvod
volí	volit	k5eAaImIp3nP	volit
jednoho	jeden	k4xCgMnSc4	jeden
poslance	poslanec	k1gMnSc4	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
těchto	tento	k3xDgInPc2	tento
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
neshodují	shodovat	k5eNaImIp3nP	shodovat
s	s	k7c7	s
hranicemi	hranice	k1gFnPc7	hranice
městských	městský	k2eAgMnPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
při	při	k7c6	při
obecních	obecní	k2eAgFnPc6d1	obecní
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Obvody	obvod	k1gInPc1	obvod
mají	mít	k5eAaImIp3nP	mít
totiž	totiž	k9	totiž
výrazně	výrazně	k6eAd1	výrazně
odlišný	odlišný	k2eAgInSc1d1	odlišný
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
spravedlivější	spravedlivý	k2eAgNnSc4d2	spravedlivější
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
málo	málo	k6eAd1	málo
obydlené	obydlený	k2eAgInPc1d1	obydlený
centrální	centrální	k2eAgInPc1d1	centrální
obvody	obvod	k1gInPc1	obvod
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
spojeny	spojit	k5eAaPmNgInP	spojit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
lidnaté	lidnatý	k2eAgInPc1d1	lidnatý
obvody	obvod	k1gInPc1	obvod
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
každý	každý	k3xTgMnSc1	každý
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
celky	celek	k1gInPc4	celek
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
Paříž	Paříž	k1gFnSc4	Paříž
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
11	[number]	k4	11
socialistických	socialistický	k2eAgMnPc2d1	socialistický
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
2	[number]	k4	2
zelení	zelení	k1gNnSc2	zelení
a	a	k8xC	a
8	[number]	k4	8
za	za	k7c4	za
UMP	UMP	kA	UMP
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Čtvrtě	čtvrt	k1gFnSc2	čtvrt
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
Bývalé	bývalý	k2eAgInPc1d1	bývalý
pařížské	pařížský	k2eAgInPc1d1	pařížský
městské	městský	k2eAgInPc1d1	městský
obvody	obvod	k1gInPc1	obvod
<g/>
,	,	kIx,	,
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
sekce	sekce	k1gFnSc1	sekce
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Bývalé	bývalý	k2eAgFnPc1d1	bývalá
obce	obec	k1gFnPc1	obec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Pařížské	pařížský	k2eAgInPc1d1	pařížský
městské	městský	k2eAgInPc1d1	městský
obvody	obvod	k1gInPc1	obvod
a	a	k8xC	a
Administrativní	administrativní	k2eAgFnPc1d1	administrativní
čtvrtě	čtvrt	k1gFnPc1	čtvrt
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
Paříž	Paříž	k1gFnSc1	Paříž
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
postavením	postavení	k1gNnSc7	postavení
hradeb	hradba	k1gFnPc2	hradba
Filipem	Filip	k1gMnSc7	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Augustem	August	k1gMnSc7	August
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
ostrov	ostrov	k1gInSc4	ostrov
Cité	Citá	k1gFnSc2	Citá
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
čtvrti	čtvrt	k1gFnPc4	čtvrt
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
postavení	postavení	k1gNnSc6	postavení
hradeb	hradba	k1gFnPc2	hradba
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
čtvrtí	čtvrt	k1gFnPc2	čtvrt
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
na	na	k7c4	na
osm	osm	k4xCc4	osm
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
další	další	k2eAgNnSc1d1	další
území	území	k1gNnSc1	území
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgFnPc2d1	další
osm	osm	k4xCc1	osm
čtvrtí	čtvrt	k1gFnPc2	čtvrt
pak	pak	k6eAd1	pak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
po	po	k7c6	po
dostavění	dostavění	k1gNnSc6	dostavění
hradeb	hradba	k1gFnPc2	hradba
za	za	k7c4	za
Karla	Karel	k1gMnSc4	Karel
V.	V.	kA	V.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
Paříž	Paříž	k1gFnSc1	Paříž
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
šestnácti	šestnáct	k4xCc2	šestnáct
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
čtvrti	čtvrt	k1gFnSc6	čtvrt
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
správce	správce	k1gMnSc1	správce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
quartinier	quartinier	k1gInSc1	quartinier
<g/>
)	)	kIx)	)
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
administrativní	administrativní	k2eAgInPc4d1	administrativní
úkoly	úkol	k1gInPc4	úkol
a	a	k8xC	a
plukovník	plukovník	k1gMnSc1	plukovník
za	za	k7c4	za
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
pořádek	pořádek	k1gInSc4	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1702	[number]	k4	1702
byl	být	k5eAaImAgInS	být
zvýšen	zvýšit	k5eAaPmNgInS	zvýšit
počet	počet	k1gInSc1	počet
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
na	na	k7c4	na
dvacet	dvacet	k4xCc4	dvacet
<g/>
:	:	kIx,	:
pět	pět	k4xCc4	pět
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc4	ostrov
Cité	Citá	k1gFnSc2	Citá
a	a	k8xC	a
čtrnáct	čtrnáct	k4xCc4	čtrnáct
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
<g/>
:	:	kIx,	:
kde	kde	k6eAd1	kde
žilo	žít	k5eAaImAgNnS	žít
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtě	čtvrt	k1gFnPc1	čtvrt
byly	být	k5eAaImAgFnP	být
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
policejních	policejní	k2eAgMnPc2d1	policejní
poručíků	poručík	k1gMnPc2	poručík
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
dva	dva	k4xCgInPc4	dva
inspektory	inspektor	k1gMnPc7	inspektor
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
čtvrti	čtvrt	k1gFnSc6	čtvrt
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1708	[number]	k4	1708
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
quartierů	quartier	k1gMnPc2	quartier
se	se	k3xPyFc4	se
omezovala	omezovat	k5eAaImAgFnS	omezovat
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
některých	některý	k3yIgFnPc2	některý
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
desátků	desátek	k1gInPc2	desátek
apod.	apod.	kA	apod.
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1789	[number]	k4	1789
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
60	[number]	k4	60
okrsků	okrsek	k1gInPc2	okrsek
kvůli	kvůli	k7c3	kvůli
volbě	volba	k1gFnSc3	volba
delegátů	delegát	k1gMnPc2	delegát
do	do	k7c2	do
generálních	generální	k2eAgInPc2d1	generální
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
bývalá	bývalý	k2eAgFnSc1d1	bývalá
čtvrť	čtvrť	k1gFnSc1	čtvrť
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
nebo	nebo	k8xC	nebo
čtyř	čtyři	k4xCgInPc2	čtyři
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
během	během	k7c2	během
roku	rok	k1gInSc2	rok
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
sestavil	sestavit	k5eAaPmAgInS	sestavit
oddíl	oddíl	k1gInSc1	oddíl
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1790	[number]	k4	1790
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
okrsků	okrsek	k1gInPc2	okrsek
snížen	snížen	k2eAgMnSc1d1	snížen
na	na	k7c4	na
48	[number]	k4	48
tzv.	tzv.	kA	tzv.
revolučních	revoluční	k2eAgFnPc2d1	revoluční
sekcí	sekce	k1gFnPc2	sekce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
vymezeny	vymezit	k5eAaPmNgFnP	vymezit
hranicemi	hranice	k1gFnPc7	hranice
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1785	[number]	k4	1785
<g/>
-	-	kIx~	-
<g/>
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1795	[number]	k4	1795
bylo	být	k5eAaImAgNnS	být
správní	správní	k2eAgNnSc1d1	správní
členění	členění	k1gNnSc1	členění
Paříže	Paříž	k1gFnSc2	Paříž
opět	opět	k6eAd1	opět
upraveno	upravit	k5eAaPmNgNnS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Sekce	sekce	k1gFnPc1	sekce
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgFnP	změnit
na	na	k7c4	na
čtvrtě	čtvrt	k1gFnPc4	čtvrt
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
čtyři	čtyři	k4xCgFnPc1	čtyři
čtvrtě	čtvrt	k1gFnPc1	čtvrt
tvořily	tvořit	k5eAaImAgFnP	tvořit
jeden	jeden	k4xCgMnSc1	jeden
městský	městský	k2eAgInSc1d1	městský
obvod	obvod	k1gInSc1	obvod
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
bylo	být	k5eAaImAgNnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1840	[number]	k4	1840
<g/>
-	-	kIx~	-
<g/>
1845	[number]	k4	1845
byly	být	k5eAaImAgFnP	být
městské	městský	k2eAgFnPc1d1	městská
hradby	hradba	k1gFnPc1	hradba
posunuty	posunout	k5eAaPmNgFnP	posunout
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1859	[number]	k4	1859
vyšel	vyjít	k5eAaPmAgInS	vyjít
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
organizaci	organizace	k1gFnSc6	organizace
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	Hranice	k1gFnPc1	Hranice
města	město	k1gNnSc2	město
byly	být	k5eAaImAgFnP	být
posunuty	posunut	k2eAgFnPc1d1	posunuta
až	až	k9	až
k	k	k7c3	k
novým	nový	k2eAgFnPc3d1	nová
hradbám	hradba	k1gFnPc3	hradba
a	a	k8xC	a
město	město	k1gNnSc1	město
pohltilo	pohltit	k5eAaPmAgNnS	pohltit
některé	některý	k3yIgFnPc4	některý
bývalé	bývalý	k2eAgFnPc4d1	bývalá
obce	obec	k1gFnPc4	obec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
mezi	mezi	k7c7	mezi
starými	starý	k2eAgFnPc7d1	stará
a	a	k8xC	a
novými	nový	k2eAgFnPc7d1	nová
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
ze	z	k7c2	z
4365	[number]	k4	4365
ha	ha	kA	ha
o	o	k7c4	o
dalších	další	k2eAgFnPc2d1	další
3438	[number]	k4	3438
ha	ha	kA	ha
na	na	k7c4	na
7802	[number]	k4	7802
ha	ha	kA	ha
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
ze	z	k7c2	z
3228	[number]	k4	3228
ha	ha	kA	ha
na	na	k7c4	na
7088	[number]	k4	7088
ha	ha	kA	ha
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nezapočítává	započítávat	k5eNaImIp3nS	započítávat
plocha	plocha	k1gFnSc1	plocha
řeky	řeka	k1gFnSc2	řeka
Seiny	Seina	k1gFnSc2	Seina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozšíření	rozšíření	k1gNnSc1	rozšíření
se	se	k3xPyFc4	se
dotklo	dotknout	k5eAaPmAgNnS	dotknout
celkem	celkem	k6eAd1	celkem
24	[number]	k4	24
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
obce	obec	k1gFnPc1	obec
byly	být	k5eAaImAgFnP	být
zahrnuty	zahrnout	k5eAaPmNgFnP	zahrnout
kompletně	kompletně	k6eAd1	kompletně
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
jména	jméno	k1gNnSc2	jméno
převzaly	převzít	k5eAaPmAgInP	převzít
nově	nově	k6eAd1	nově
vytvořené	vytvořený	k2eAgInPc1d1	vytvořený
administrativní	administrativní	k2eAgInPc1d1	administrativní
městské	městský	k2eAgInPc1d1	městský
celky	celek	k1gInPc1	celek
(	(	kIx(	(
<g/>
La	la	k0	la
Villette	Villett	k1gMnSc5	Villett
-	-	kIx~	-
Quartier	Quartier	k1gInSc1	Quartier
de	de	k?	de
la	la	k0	la
Villette	Villett	k1gMnSc5	Villett
<g/>
,	,	kIx,	,
Belleville	Bellevill	k1gMnSc5	Bellevill
-	-	kIx~	-
Quartier	Quartier	k1gInSc1	Quartier
de	de	k?	de
Belleville	Belleville	k1gInSc1	Belleville
<g/>
,	,	kIx,	,
Vaugirard	Vaugirard	k1gInSc1	Vaugirard
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
Vaugirard	Vaugirarda	k1gFnPc2	Vaugirarda
a	a	k8xC	a
Grenelle	Grenelle	k1gNnPc2	Grenelle
-	-	kIx~	-
Quartier	Quartier	k1gInSc1	Quartier
de	de	k?	de
Grenelle	Grenelle	k1gInSc1	Grenelle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
sedmi	sedm	k4xCc2	sedm
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
Thiersových	Thiersův	k2eAgFnPc2d1	Thiersova
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
Paříž	Paříž	k1gFnSc4	Paříž
(	(	kIx(	(
<g/>
uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
<g/>
)	)	kIx)	)
a	a	k8xC	a
sousedící	sousedící	k2eAgNnPc1d1	sousedící
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
vně	vně	k6eAd1	vně
hradeb	hradba	k1gFnPc2	hradba
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Passy	Pass	k1gInPc1	Pass
a	a	k8xC	a
Auteuil	Auteuil	k1gInSc1	Auteuil
mezi	mezi	k7c7	mezi
16	[number]	k4	16
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
a	a	k8xC	a
město	město	k1gNnSc1	město
Boulogne	Boulogn	k1gInSc5	Boulogn
<g/>
,	,	kIx,	,
Batignolles-Monceaux	Batignolles-Monceaux	k1gInSc1	Batignolles-Monceaux
mezi	mezi	k7c7	mezi
<g />
.	.	kIx.	.
</s>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
a	a	k8xC	a
město	město	k1gNnSc1	město
Clichy	Clicha	k1gFnSc2	Clicha
<g/>
,	,	kIx,	,
Montmartre	Montmartr	k1gInSc5	Montmartr
mezi	mezi	k7c7	mezi
18	[number]	k4	18
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
a	a	k8xC	a
město	město	k1gNnSc1	město
Saint-Ouen	Saint-Ouna	k1gFnPc2	Saint-Ouna
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Chapelle	Chapelle	k1gFnSc2	Chapelle
mezi	mezi	k7c7	mezi
18	[number]	k4	18
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
a	a	k8xC	a
města	město	k1gNnSc2	město
Saint-Ouen	Saint-Ouna	k1gFnPc2	Saint-Ouna
<g/>
,	,	kIx,	,
Saint-Denis	Saint-Denis	k1gFnPc2	Saint-Denis
a	a	k8xC	a
Aubervilliers	Aubervilliersa	k1gFnPc2	Aubervilliersa
<g/>
,	,	kIx,	,
Charonne	Charonn	k1gInSc5	Charonn
mezi	mezi	k7c7	mezi
20	[number]	k4	20
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
a	a	k8xC	a
města	město	k1gNnSc2	město
Montreuil	Montreuila	k1gFnPc2	Montreuila
a	a	k8xC	a
Bagnolet	Bagnolet	k1gInSc1	Bagnolet
<g/>
,	,	kIx,	,
Bercy	Berca	k1gFnPc1	Berca
mezi	mezi	k7c7	mezi
12	[number]	k4	12
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
a	a	k8xC	a
město	město	k1gNnSc1	město
Charenton-le-Pont	Charentone-Ponta	k1gFnPc2	Charenton-le-Ponta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
13	[number]	k4	13
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nacházely	nacházet	k5eAaImAgFnP	nacházet
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
odpojeno	odpojit	k5eAaPmNgNnS	odpojit
a	a	k8xC	a
zbývající	zbývající	k2eAgFnPc1d1	zbývající
části	část	k1gFnPc1	část
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jako	jako	k9	jako
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
tvořící	tvořící	k2eAgFnPc4d1	tvořící
pařížská	pařížský	k2eAgNnPc4d1	pařížské
předměstí	předměstí	k1gNnPc4	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
Neuilly-sur-Seine	Neuillyur-Sein	k1gMnSc5	Neuilly-sur-Sein
<g/>
,	,	kIx,	,
Clichy	Clicha	k1gFnPc1	Clicha
<g/>
,	,	kIx,	,
Saint-Ouen	Saint-Ouen	k1gInSc1	Saint-Ouen
<g/>
,	,	kIx,	,
Aubervilliers	Aubervilliers	k1gInSc1	Aubervilliers
<g/>
,	,	kIx,	,
Pantin	Pantin	k1gMnSc1	Pantin
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Pré-Saint-Gervais	Pré-Saint-Gervais	k1gFnSc2	Pré-Saint-Gervais
<g/>
,	,	kIx,	,
Saint-Mandé	Saint-Mandý	k2eAgFnSc2d1	Saint-Mandý
<g/>
,	,	kIx,	,
Bagnolet	Bagnolet	k1gInSc1	Bagnolet
<g/>
,	,	kIx,	,
Ivry-sur-Seine	Ivryur-Sein	k1gMnSc5	Ivry-sur-Sein
<g/>
,	,	kIx,	,
Gentilly	Gentill	k1gInPc1	Gentill
<g/>
,	,	kIx,	,
Montrouge	Montrouge	k1gNnPc1	Montrouge
<g/>
,	,	kIx,	,
Vanves	Vanves	k1gInSc1	Vanves
<g/>
,	,	kIx,	,
Issy-les-Moulineaux	Issyes-Moulineaux	k1gInSc1	Issy-les-Moulineaux
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
1860	[number]	k4	1860
nově	nově	k6eAd1	nově
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
20	[number]	k4	20
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
a	a	k8xC	a
80	[number]	k4	80
administrativních	administrativní	k2eAgFnPc2d1	administrativní
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
každý	každý	k3xTgInSc1	každý
obvod	obvod	k1gInSc1	obvod
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
<g/>
)	)	kIx)	)
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
územní	územní	k2eAgNnSc4d1	územní
rozdělení	rozdělení	k1gNnSc4	rozdělení
platí	platit	k5eAaImIp3nS	platit
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
hranice	hranice	k1gFnSc1	hranice
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
obvodů	obvod	k1gInPc2	obvod
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nezměnily	změnit	k5eNaPmAgFnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnPc1d1	vnější
hranice	hranice	k1gFnPc1	hranice
některých	některý	k3yIgInPc2	některý
obvodů	obvod	k1gInPc2	obvod
se	se	k3xPyFc4	se
upravovaly	upravovat	k5eAaImAgInP	upravovat
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
o	o	k7c6	o
zrušení	zrušení	k1gNnSc6	zrušení
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
:	:	kIx,	:
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
dekret	dekret	k1gInSc4	dekret
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
připojení	připojení	k1gNnSc2	připojení
vojenského	vojenský	k2eAgNnSc2d1	vojenské
cvičiště	cvičiště	k1gNnSc2	cvičiště
v	v	k7c4	v
Issy-les-Moulineaux	Issyes-Moulineaux	k1gInSc4	Issy-les-Moulineaux
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
<g/>
)	)	kIx)	)
1929	[number]	k4	1929
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dekret	dekret	k1gInSc4	dekret
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
připojení	připojení	k1gNnSc4	připojení
Boulogneský	boulogneský	k2eAgInSc1d1	boulogneský
lesík	lesík	k1gInSc1	lesík
(	(	kIx(	(
<g/>
846	[number]	k4	846
ha	ha	kA	ha
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vincenneský	Vincenneský	k2eAgInSc1d1	Vincenneský
lesík	lesík	k1gInSc1	lesík
(	(	kIx(	(
<g/>
995	[number]	k4	995
ha	ha	kA	ha
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
<g/>
)	)	kIx)	)
1930	[number]	k4	1930
připojení	připojení	k1gNnSc2	připojení
některých	některý	k3yIgNnPc2	některý
hraničních	hraniční	k2eAgNnPc2d1	hraniční
území	území	k1gNnPc2	území
z	z	k7c2	z
Levallois-Perret	Levallois-Perreta	k1gFnPc2	Levallois-Perreta
<g/>
,	,	kIx,	,
Clichy	Clicha	k1gFnPc1	Clicha
<g/>
,	,	kIx,	,
Saint-Ouen	Saint-Ouen	k1gInSc1	Saint-Ouen
<g/>
,	,	kIx,	,
Saint-Denis	Saint-Denis	k1gInSc1	Saint-Denis
<g/>
,	,	kIx,	,
Aubervilliers	Aubervilliers	k1gInSc1	Aubervilliers
<g/>
,	,	kIx,	,
Pantin	Pantin	k1gInSc1	Pantin
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Pré-Saint-Gervais	Pré-Saint-Gervais	k1gFnSc1	Pré-Saint-Gervais
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Lilas	Lilas	k1gInSc4	Lilas
<g/>
,	,	kIx,	,
Bagnolet	Bagnolet	k1gInSc4	Bagnolet
a	a	k8xC	a
Montreuil	Montreuil	k1gInSc4	Montreuil
Paříž	Paříž	k1gFnSc1	Paříž
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
své	svůj	k3xOyFgFnPc4	svůj
současné	současný	k2eAgFnPc4d1	současná
hranice	hranice	k1gFnPc4	hranice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
10	[number]	k4	10
540	[number]	k4	540
ha	ha	kA	ha
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
7	[number]	k4	7
802	[number]	k4	802
ha	ha	kA	ha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
a	a	k8xC	a
3438	[number]	k4	3438
ha	ha	kA	ha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
administrativní	administrativní	k2eAgFnSc7d1	administrativní
a	a	k8xC	a
územní	územní	k2eAgFnSc7d1	územní
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
pařížský	pařížský	k2eAgInSc1d1	pařížský
obvod	obvod	k1gInSc1	obvod
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
arrondissement	arrondissement	k1gMnSc1	arrondissement
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
administrativní	administrativní	k2eAgFnPc4d1	administrativní
čtvrti	čtvrt	k1gFnPc4	čtvrt
(	(	kIx(	(
<g/>
quartier	quartier	k1gMnSc1	quartier
administratif	administratif	k1gMnSc1	administratif
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poštovních	poštovní	k2eAgNnPc6d1	poštovní
směrovacích	směrovací	k2eAgNnPc6d1	směrovací
číslech	číslo	k1gNnPc6	číslo
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
obvody	obvod	k1gInPc1	obvod
označeny	označit	k5eAaPmNgInP	označit
posledním	poslední	k2eAgNnSc7d1	poslední
dvojčíslím	dvojčíslí	k1gNnSc7	dvojčíslí
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
obvod	obvod	k1gInSc1	obvod
i	i	k8xC	i
čtvrť	čtvrť	k1gFnSc1	čtvrť
má	mít	k5eAaImIp3nS	mít
pořadové	pořadový	k2eAgNnSc4d1	pořadové
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
názvy	název	k1gInPc1	název
jsou	být	k5eAaImIp3nP	být
odvozeny	odvozen	k2eAgInPc1d1	odvozen
jednak	jednak	k8xC	jednak
od	od	k7c2	od
jmen	jméno	k1gNnPc2	jméno
připojených	připojený	k2eAgFnPc2d1	připojená
bývalých	bývalý	k2eAgFnPc2d1	bývalá
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
významných	významný	k2eAgFnPc2d1	významná
architektonických	architektonický	k2eAgFnPc2d1	architektonická
dominant	dominanta	k1gFnPc2	dominanta
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
čtvrti	čtvrt	k1gFnSc6	čtvrt
(	(	kIx(	(
<g/>
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
paláce	palác	k1gInPc1	palác
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnPc1	nemocnice
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
vnější	vnější	k2eAgFnSc6d1	vnější
prezentaci	prezentace	k1gFnSc6	prezentace
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
historický	historický	k2eAgInSc4d1	historický
městský	městský	k2eAgInSc4d1	městský
znak	znak	k1gInSc4	znak
a	a	k8xC	a
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
též	též	k9	též
logo	logo	k1gNnSc4	logo
<g/>
.	.	kIx.	.
</s>
<s>
Tradičními	tradiční	k2eAgFnPc7d1	tradiční
barvami	barva	k1gFnPc7	barva
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
užití	užití	k1gNnSc1	užití
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1358	[number]	k4	1358
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Étienne	Étienn	k1gInSc5	Étienn
Marcel	Marcel	k1gMnSc1	Marcel
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prévôt	prévôt	k1gMnSc1	prévôt
des	des	k1gNnSc2	des
marchands	marchandsa	k1gFnPc2	marchandsa
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
stoupenci	stoupenec	k1gMnPc1	stoupenec
nosili	nosit	k5eAaImAgMnP	nosit
během	během	k7c2	během
povstání	povstání	k1gNnPc2	povstání
proti	proti	k7c3	proti
Karlu	Karel	k1gMnSc3	Karel
V.	V.	kA	V.
čapky	čapka	k1gFnSc2	čapka
napůl	napůl	k6eAd1	napůl
červené	červený	k2eAgFnPc1d1	červená
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Heslem	heslo	k1gNnSc7	heslo
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
latinské	latinský	k2eAgNnSc1d1	latinské
rčení	rčení	k1gNnSc1	rčení
Fluctuat	Fluctuat	k1gInSc1	Fluctuat
nec	nec	k?	nec
mergitur	mergitura	k1gFnPc2	mergitura
(	(	kIx(	(
<g/>
Zmítá	zmítat	k5eAaImIp3nS	zmítat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepotápí	potápět	k5eNaImIp3nS	potápět
nebo	nebo	k8xC	nebo
též	též	k9	též
Pluje	plout	k5eAaImIp3nS	plout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepotopí	potopit	k5eNaPmIp3nP	potopit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
městském	městský	k2eAgInSc6d1	městský
znaku	znak	k1gInSc6	znak
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
heslem	heslo	k1gNnSc7	heslo
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vyhláškou	vyhláška	k1gFnSc7	vyhláška
pařížského	pařížský	k2eAgMnSc2d1	pařížský
prefekta	prefekt	k1gMnSc2	prefekt
barona	baron	k1gMnSc2	baron
Hausmanna	Hausmann	k1gMnSc2	Hausmann
ze	z	k7c2	z
dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1853	[number]	k4	1853
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
symbolické	symbolický	k2eAgFnSc6d1	symbolická
rovině	rovina	k1gFnSc6	rovina
má	mít	k5eAaImIp3nS	mít
Paříž	Paříž	k1gFnSc4	Paříž
rovněž	rovněž	k9	rovněž
čtyři	čtyři	k4xCgNnPc4	čtyři
ochránce	ochránce	k1gMnSc2	ochránce
mezi	mezi	k7c7	mezi
světci	světec	k1gMnPc7	světec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
patronem	patron	k1gMnSc7	patron
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celé	celý	k2eAgFnSc2d1	celá
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
svatý	svatý	k2eAgMnSc1d1	svatý
Diviš	Diviš	k1gMnSc1	Diviš
(	(	kIx(	(
<g/>
Saint	Saint	k1gInSc1	Saint
Denis	Denisa	k1gFnPc2	Denisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgInSc7	první
pařížským	pařížský	k2eAgInSc7d1	pařížský
biskupem	biskup	k1gInSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Jenovéfa	Jenovéfa	k1gFnSc1	Jenovéfa
(	(	kIx(	(
<g/>
Sainte	Saint	k1gMnSc5	Saint
Geneviè	Geneviè	k1gMnSc5	Geneviè
<g/>
,	,	kIx,	,
421	[number]	k4	421
<g/>
-	-	kIx~	-
<g/>
512	[number]	k4	512
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uctívána	uctíván	k2eAgFnSc1d1	uctívána
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svými	svůj	k3xOyFgFnPc7	svůj
modlitbami	modlitba	k1gFnPc7	modlitba
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
útok	útok	k1gInSc4	útok
Attily	Attila	k1gMnSc2	Attila
na	na	k7c4	na
město	město	k1gNnSc4	město
a	a	k8xC	a
zachránila	zachránit	k5eAaPmAgFnS	zachránit
tak	tak	k9	tak
Paříž	Paříž	k1gFnSc1	Paříž
před	před	k7c7	před
vypleněním	vyplenění	k1gNnSc7	vyplenění
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Marcel	Marcel	k1gMnSc1	Marcel
(	(	kIx(	(
<g/>
†	†	k?	†
asi	asi	k9	asi
436	[number]	k4	436
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
pařížským	pařížský	k2eAgMnSc7d1	pařížský
biskupem	biskup	k1gMnSc7	biskup
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
draka	drak	k1gMnSc2	drak
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
hrobu	hrob	k1gInSc2	hrob
cizoložnice	cizoložnice	k1gFnPc1	cizoložnice
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Aurea	Aurea	k1gFnSc1	Aurea
(	(	kIx(	(
<g/>
†	†	k?	†
666	[number]	k4	666
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
633	[number]	k4	633
abatyší	abatyše	k1gFnPc2	abatyše
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Dotykem	dotyk	k1gInSc7	dotyk
s	s	k7c7	s
jejími	její	k3xOp3gInPc7	její
ostatky	ostatek	k1gInPc7	ostatek
a	a	k8xC	a
rakví	rakev	k1gFnPc2	rakev
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zažehnána	zažehnán	k2eAgFnSc1d1	zažehnána
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
malý	malý	k2eAgInSc1d1	malý
znak	znak	k1gInSc1	znak
se	s	k7c7	s
znamením	znamení	k1gNnSc7	znamení
na	na	k7c6	na
štítu	štít	k1gInSc6	štít
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
štít	štít	k1gInSc1	štít
doplněn	doplnit	k5eAaPmNgInS	doplnit
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
vojenskými	vojenský	k2eAgInPc7d1	vojenský
řády	řád	k1gInPc7	řád
<g/>
,	,	kIx,	,
mottem	motto	k1gNnSc7	motto
a	a	k8xC	a
rostlinnými	rostlinný	k2eAgFnPc7d1	rostlinná
ratolestmi	ratolest	k1gFnPc7	ratolest
<g/>
.	.	kIx.	.
</s>
<s>
Blasonování	Blasonování	k1gNnSc1	Blasonování
malého	malý	k2eAgInSc2d1	malý
znaku	znak	k1gInSc2	znak
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
V	v	k7c6	v
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
stříbrná	stříbrnat	k5eAaImIp3nS	stříbrnat
loď	loď	k1gFnSc1	loď
se	s	k7c7	s
vzdutou	vzdutý	k2eAgFnSc7d1	vzdutá
plachtou	plachta	k1gFnSc7	plachta
na	na	k7c6	na
vlnách	vlna	k1gFnPc6	vlna
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
hlava	hlava	k1gFnSc1	hlava
štítu	štít	k1gInSc2	štít
posázená	posázený	k2eAgFnSc1d1	posázená
zlatými	zlatý	k2eAgFnPc7d1	zlatá
liliemi	lilie	k1gFnPc7	lilie
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
čestnými	čestný	k2eAgInPc7d1	čestný
řády	řád	k1gInPc7	řád
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
město	město	k1gNnSc1	město
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Řád	řád	k1gInSc1	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
(	(	kIx(	(
<g/>
dekret	dekret	k1gInSc4	dekret
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
dekret	dekret	k1gInSc4	dekret
z	z	k7c2	z
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
napravo	napravo	k6eAd1	napravo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Řád	řád	k1gInSc1	řád
osvobození	osvobození	k1gNnSc2	osvobození
(	(	kIx(	(
<g/>
dekret	dekret	k1gInSc4	dekret
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
nalevo	nalevo	k6eAd1	nalevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
štítu	štít	k1gInSc6	štít
spočívá	spočívat	k5eAaImIp3nS	spočívat
zlatá	zlatý	k2eAgFnSc1d1	zlatá
zděná	zděný	k2eAgFnSc1d1	zděná
koruna	koruna	k1gFnSc1	koruna
s	s	k7c7	s
pěti	pět	k4xCc7	pět
viditelnými	viditelný	k2eAgInPc7d1	viditelný
listy	list	k1gInPc7	list
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
čtyřhranných	čtyřhranný	k2eAgFnPc2d1	čtyřhranná
věží	věž	k1gFnPc2	věž
s	s	k7c7	s
bránou	brána	k1gFnSc7	brána
a	a	k8xC	a
cimbuřím	cimbuří	k1gNnSc7	cimbuří
<g/>
.	.	kIx.	.
</s>
<s>
Štít	štít	k1gInSc1	štít
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
na	na	k7c6	na
heraldicky	heraldicky	k6eAd1	heraldicky
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
dubovou	dubový	k2eAgFnSc7d1	dubová
ratolestí	ratolest	k1gFnSc7	ratolest
a	a	k8xC	a
zleva	zleva	k6eAd1	zleva
vavřínem	vavřín	k1gInSc7	vavřín
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
štítem	štít	k1gInSc7	štít
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
deviza	deviza	k1gFnSc1	deviza
FLUCTUAT	FLUCTUAT	kA	FLUCTUAT
NEC	NEC	kA	NEC
MERGITUR	MERGITUR	kA	MERGITUR
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
tradičním	tradiční	k2eAgInSc7d1	tradiční
symbolem	symbol	k1gInSc7	symbol
mocného	mocný	k2eAgInSc2d1	mocný
a	a	k8xC	a
bohatého	bohatý	k2eAgInSc2d1	bohatý
cechu	cech	k1gInSc2	cech
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
na	na	k7c4	na
chod	chod	k1gInSc4	chod
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Plavidlo	plavidlo	k1gNnSc1	plavidlo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jejich	jejich	k3xOp3gInSc7	jejich
symbolem	symbol	k1gInSc7	symbol
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
dováželi	dovážet	k5eAaImAgMnP	dovážet
do	do	k7c2	do
města	město	k1gNnSc2	město
po	po	k7c6	po
Seině	Seina	k1gFnSc6	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Latinská	latinský	k2eAgFnSc1d1	Latinská
deviza	deviza	k1gFnSc1	deviza
města	město	k1gNnSc2	město
Fluctuat	Fluctuat	k2eAgInSc4d1	Fluctuat
nec	nec	k?	nec
mergitur	mergitura	k1gFnPc2	mergitura
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
ve	v	k7c6	v
štítu	štít	k1gInSc6	štít
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
též	též	k9	též
historické	historický	k2eAgInPc4d1	historický
zvraty	zvrat	k1gInPc4	zvrat
a	a	k8xC	a
revoluce	revoluce	k1gFnPc4	revoluce
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
město	město	k1gNnSc1	město
muselo	muset	k5eAaImAgNnS	muset
projít	projít	k5eAaPmF	projít
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nezlomeno	zlomit	k5eNaPmNgNnS	zlomit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vlajka	vlajka	k1gFnSc1	vlajka
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
svislé	svislý	k2eAgFnSc2d1	svislá
bikolóry	bikolóra	k1gFnSc2	bikolóra
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
modro-červeně	modro-červeně	k6eAd1	modro-červeně
polceným	polcený	k2eAgInSc7d1	polcený
listem	list	k1gInSc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
během	během	k7c2	během
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
i	i	k9	i
na	na	k7c4	na
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
radnice	radnice	k1gFnSc1	radnice
používá	používat	k5eAaImIp3nS	používat
rovněž	rovněž	k9	rovněž
logo	logo	k1gNnSc4	logo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
modrý	modrý	k2eAgInSc1d1	modrý
pruh	pruh	k1gInSc1	pruh
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
MAIRIE	MAIRIE	kA	MAIRIE
DE	DE	k?	DE
PARIS	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
radnice	radnice	k1gFnSc1	radnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
siluetou	silueta	k1gFnSc7	silueta
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Architektura	architektura	k1gFnSc1	architektura
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
francouzských	francouzský	k2eAgMnPc2d1	francouzský
panovníků	panovník	k1gMnPc2	panovník
již	již	k6eAd1	již
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
zanechalo	zanechat	k5eAaPmAgNnS	zanechat
své	svůj	k3xOyFgFnPc4	svůj
stopy	stopa	k1gFnPc4	stopa
ve	v	k7c6	v
vzhledu	vzhled	k1gInSc6	vzhled
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
dějinách	dějiny	k1gFnPc6	dějiny
neprošlo	projít	k5eNaPmAgNnS	projít
nikdy	nikdy	k6eAd1	nikdy
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
zničením	zničení	k1gNnSc7	zničení
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
např.	např.	kA	např.
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1666	[number]	k4	1666
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lisabonu	Lisabon	k1gInSc2	Lisabon
(	(	kIx(	(
<g/>
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
roku	rok	k1gInSc2	rok
1755	[number]	k4	1755
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Berlína	Berlín	k1gInSc2	Berlín
(	(	kIx(	(
<g/>
bombardování	bombardování	k1gNnSc1	bombardování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dispoziční	dispoziční	k2eAgNnSc4d1	dispoziční
řešení	řešení	k1gNnSc4	řešení
mnoha	mnoho	k4c2	mnoho
základních	základní	k2eAgFnPc2d1	základní
ulic	ulice	k1gFnPc2	ulice
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
zachováno	zachovat	k5eAaPmNgNnS	zachovat
a	a	k8xC	a
doplněno	doplnit	k5eAaPmNgNnS	doplnit
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
i	i	k9	i
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
během	během	k7c2	během
modernizace	modernizace	k1gFnSc2	modernizace
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
po	po	k7c6	po
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Seiny	Seina	k1gFnSc2	Seina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
asi	asi	k9	asi
deset	deset	k4xCc1	deset
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
písčin	písčina	k1gFnPc2	písčina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
jen	jen	k9	jen
dva	dva	k4xCgMnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
vzhled	vzhled	k1gInSc1	vzhled
města	město	k1gNnSc2	město
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
z	z	k7c2	z
přestavby	přestavba	k1gFnSc2	přestavba
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
prefekt	prefekt	k1gMnSc1	prefekt
Haussmann	Haussmann	k1gMnSc1	Haussmann
v	v	k7c6	v
období	období	k1gNnSc2	období
Druhého	druhý	k4xOgNnSc2	druhý
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
proraženy	proražen	k2eAgFnPc1d1	proražena
dnešní	dnešní	k2eAgFnPc1d1	dnešní
nejrušnější	rušný	k2eAgFnPc1d3	nejrušnější
trasy	trasa	k1gFnPc1	trasa
(	(	kIx(	(
<g/>
Boulevard	Boulevard	k1gMnSc1	Boulevard
Saint-Germain	Saint-Germain	k1gMnSc1	Saint-Germain
<g/>
,	,	kIx,	,
Boulevard	Boulevard	k1gMnSc1	Boulevard
de	de	k?	de
Sébastopol	Sébastopol	k1gInSc1	Sébastopol
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Tehdy	tehdy	k6eAd1	tehdy
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Paříže	Paříž	k1gFnSc2	Paříž
zmizely	zmizet	k5eAaPmAgInP	zmizet
labyrinty	labyrint	k1gInPc1	labyrint
úzkých	úzký	k2eAgFnPc2d1	úzká
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
domy	dům	k1gInPc4	dům
postavené	postavený	k2eAgInPc4d1	postavený
v	v	k7c6	v
horních	horní	k2eAgNnPc6d1	horní
patrech	patro	k1gNnPc6	patro
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
nebo	nebo	k8xC	nebo
z	z	k7c2	z
hrázděného	hrázděný	k2eAgNnSc2d1	hrázděné
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
zástavba	zástavba	k1gFnSc1	zástavba
bulvárů	bulvár	k1gInPc2	bulvár
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
stejně	stejně	k6eAd1	stejně
vysokých	vysoký	k2eAgInPc2d1	vysoký
domů	dům	k1gInPc2	dům
s	s	k7c7	s
novoklasicistní	novoklasicistní	k2eAgFnSc7d1	novoklasicistní
fasádou	fasáda	k1gFnSc7	fasáda
<g/>
,	,	kIx,	,
francouzskými	francouzský	k2eAgNnPc7d1	francouzské
okny	okno	k1gNnPc7	okno
a	a	k8xC	a
balkóny	balkón	k1gInPc7	balkón
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
patře	patro	k1gNnSc6	patro
lemovanými	lemovaný	k2eAgFnPc7d1	lemovaná
stromořadím	stromořadí	k1gNnSc7	stromořadí
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
přísným	přísný	k2eAgFnPc3d1	přísná
regulačním	regulační	k2eAgFnPc3d1	regulační
vyhláškám	vyhláška	k1gFnPc3	vyhláška
o	o	k7c6	o
vzhledu	vzhled	k1gInSc6	vzhled
a	a	k8xC	a
velikosti	velikost	k1gFnSc3	velikost
domů	dům	k1gInPc2	dům
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tak	tak	k9	tak
dnes	dnes	k6eAd1	dnes
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
zástavba	zástavba	k1gFnSc1	zástavba
působí	působit	k5eAaImIp3nS	působit
jednolitě	jednolitě	k6eAd1	jednolitě
a	a	k8xC	a
vyrovnaně	vyrovnaně	k6eAd1	vyrovnaně
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
nařízení	nařízení	k1gNnPc1	nařízení
o	o	k7c6	o
omezené	omezený	k2eAgFnSc6d1	omezená
výšce	výška	k1gFnSc6	výška
domů	dům	k1gInPc2	dům
platí	platit	k5eAaImIp3nS	platit
doposud	doposud	k6eAd1	doposud
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
vyšších	vysoký	k2eAgFnPc2d2	vyšší
než	než	k8xS	než
37	[number]	k4	37
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
povolována	povolován	k2eAgFnSc1d1	povolována
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
obvodech	obvod	k1gInPc6	obvod
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
hranice	hranice	k1gFnSc1	hranice
ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
i	i	k9	i
každoročních	každoroční	k2eAgInPc2d1	každoroční
26	[number]	k4	26
miliónů	milión	k4xCgInPc2	milión
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
ciziny	cizina	k1gFnSc2	cizina
najdou	najít	k5eAaPmIp3nP	najít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
světských	světský	k2eAgFnPc2d1	světská
a	a	k8xC	a
církevních	církevní	k2eAgFnPc2d1	církevní
stavebních	stavební	k2eAgFnPc2d1	stavební
památek	památka	k1gFnPc2	památka
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
římské	římský	k2eAgFnSc2d1	římská
Galie	Galie	k1gFnSc2	Galie
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
zastupující	zastupující	k2eAgFnPc1d1	zastupující
všechny	všechen	k3xTgFnPc1	všechen
architektonické	architektonický	k2eAgFnPc1d1	architektonická
slohy	sloha	k1gFnPc1	sloha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historické	historický	k2eAgFnSc2d1	historická
Paříže	Paříž	k1gFnSc2	Paříž
však	však	k9	však
po	po	k7c6	po
rekonstrukcích	rekonstrukce	k1gFnPc6	rekonstrukce
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jen	jen	k9	jen
málo	málo	k1gNnSc4	málo
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Památky	památka	k1gFnPc1	památka
jako	jako	k8xC	jako
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
Sacré-Cœ	Sacré-Cœ	k1gFnSc1	Sacré-Cœ
<g/>
,	,	kIx,	,
Louvre	Louvre	k1gInSc1	Louvre
<g/>
,	,	kIx,	,
katedrála	katedrála	k1gFnSc1	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
,	,	kIx,	,
Invalidovna	invalidovna	k1gFnSc1	invalidovna
nebo	nebo	k8xC	nebo
Vítězný	vítězný	k2eAgInSc1d1	vítězný
oblouk	oblouk	k1gInSc1	oblouk
a	a	k8xC	a
Champs-Élysées	Champs-Élysées	k1gInSc1	Champs-Élysées
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
symboly	symbol	k1gInPc4	symbol
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Nábřeží	nábřeží	k1gNnSc1	nábřeží
Seiny	Seina	k1gFnSc2	Seina
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
UNESCO	UNESCO	kA	UNESCO
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
památky	památka	k1gFnPc1	památka
snad	snad	k9	snad
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
architektonických	architektonický	k2eAgInPc2d1	architektonický
slohů	sloh	k1gInPc2	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
Louvre	Louvre	k1gInSc1	Louvre
se	s	k7c7	s
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
pyramidami	pyramida	k1gFnPc7	pyramida
před	před	k7c7	před
vchodem	vchod	k1gInSc7	vchod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
supermoderní	supermoderní	k2eAgFnSc1d1	supermoderní
čtvrť	čtvrť	k1gFnSc1	čtvrť
La	la	k1gNnSc2	la
Défense	Défense	k1gFnSc2	Défense
<g/>
,	,	kIx,	,
plná	plný	k2eAgFnSc1d1	plná
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
a	a	k8xC	a
supermoderních	supermoderní	k2eAgFnPc2d1	supermoderní
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
nejznámější	známý	k2eAgFnSc1d3	nejznámější
moderní	moderní	k2eAgFnSc1d1	moderní
stavba	stavba	k1gFnSc1	stavba
Paříže	Paříž	k1gFnSc2	Paříž
-	-	kIx~	-
Grande	grand	k1gMnSc5	grand
Arche	Archus	k1gMnSc5	Archus
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
k	k	k7c3	k
200	[number]	k4	200
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jen	jen	k9	jen
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
architekti	architekt	k1gMnPc1	architekt
postavili	postavit	k5eAaPmAgMnP	postavit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	on	k3xPp3gNnSc4	on
napadne	napadnout	k5eAaPmIp3nS	napadnout
<g/>
"	"	kIx"	"
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
získala	získat	k5eAaPmAgFnS	získat
Paříž	Paříž	k1gFnSc1	Paříž
po	po	k7c6	po
výstavbě	výstavba	k1gFnSc6	výstavba
Pompidouova	Pompidouův	k2eAgNnSc2d1	Pompidouův
centra	centrum	k1gNnSc2	centrum
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
regionu	region	k1gInSc2	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
impulsů	impuls	k1gInPc2	impuls
světového	světový	k2eAgNnSc2d1	světové
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
činil	činit	k5eAaImAgInS	činit
HDP	HDP	kA	HDP
v	v	k7c6	v
regionu	region	k1gInSc6	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
podle	podle	k7c2	podle
INSEE	INSEE	kA	INSEE
552,664	[number]	k4	552,664
miliardy	miliarda	k4xCgFnSc2	miliarda
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
500	[number]	k4	500
největších	veliký	k2eAgFnPc2d3	veliký
nadnárodních	nadnárodní	k2eAgFnPc2d1	nadnárodní
společností	společnost	k1gFnPc2	společnost
jich	on	k3xPp3gFnPc2	on
má	mít	k5eAaImIp3nS	mít
25	[number]	k4	25
své	svůj	k3xOyFgInPc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Île-de-France	Îlee-France	k1gFnSc1	Île-de-France
představuje	představovat	k5eAaImIp3nS	představovat
29	[number]	k4	29
%	%	kIx~	%
francouzského	francouzský	k2eAgInSc2d1	francouzský
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
představuje	představovat	k5eAaImIp3nS	představovat
pouze	pouze	k6eAd1	pouze
18,7	[number]	k4	18,7
%	%	kIx~	%
francouzského	francouzský	k2eAgNnSc2d1	francouzské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
83	[number]	k4	83
%	%	kIx~	%
přidané	přidaný	k2eAgFnSc2d1	přidaná
hodnoty	hodnota	k1gFnSc2	hodnota
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zdejší	zdejší	k2eAgFnSc1d1	zdejší
ekonomika	ekonomika	k1gFnSc1	ekonomika
i	i	k9	i
nadále	nadále	k6eAd1	nadále
nesmírně	smírně	k6eNd1	smírně
různorodá	různorodý	k2eAgFnSc1d1	různorodá
při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
městy	město	k1gNnPc7	město
stejné	stejná	k1gFnSc2	stejná
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
region	region	k1gInSc4	region
zažil	zažít	k5eAaPmAgMnS	zažít
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
prudkou	prudký	k2eAgFnSc4d1	prudká
deindustrializaci	deindustrializace	k1gFnSc4	deindustrializace
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
první	první	k4xOgInSc4	první
oblastí	oblast	k1gFnPc2	oblast
francouzského	francouzský	k2eAgInSc2d1	francouzský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
v	v	k7c6	v
regionu	region	k1gInSc6	region
650	[number]	k4	650
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
tvoří	tvořit	k5eAaImIp3nS	tvořit
jen	jen	k9	jen
14	[number]	k4	14
%	%	kIx~	%
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
20	[number]	k4	20
let	léto	k1gNnPc2	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
silné	silný	k2eAgFnSc3d1	silná
deindustrializaci	deindustrializace	k1gFnSc3	deindustrializace
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
automobilový	automobilový	k2eAgInSc1d1	automobilový
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
156	[number]	k4	156
000	[number]	k4	000
pracovníků	pracovník	k1gMnPc2	pracovník
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
60	[number]	k4	60
000	[number]	k4	000
přímých	přímý	k2eAgNnPc2d1	přímé
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgInPc1d1	výrobní
závody	závod	k1gInPc1	závod
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
oba	dva	k4xCgMnPc1	dva
národní	národní	k2eAgMnPc1d1	národní
výrobci	výrobce	k1gMnPc1	výrobce
<g/>
:	:	kIx,	:
Renault	renault	k1gInSc1	renault
ve	v	k7c6	v
Flins-sur-Seine	Flinsur-Sein	k1gInSc5	Flins-sur-Sein
a	a	k8xC	a
PSA	pes	k1gMnSc4	pes
Peugeot	peugeot	k1gInSc4	peugeot
Citroën	Citroëno	k1gNnPc2	Citroëno
v	v	k7c4	v
Poissy	Poiss	k1gInPc4	Poiss
a	a	k8xC	a
Aulnay-sous-Bois	Aulnayous-Bois	k1gFnPc4	Aulnay-sous-Bois
<g/>
.	.	kIx.	.
</s>
<s>
Letecký	letecký	k2eAgInSc1d1	letecký
a	a	k8xC	a
zbrojní	zbrojní	k2eAgInSc1d1	zbrojní
průmysl	průmysl	k1gInSc1	průmysl
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
72	[number]	k4	72
000	[number]	k4	000
pracovníků	pracovník	k1gMnPc2	pracovník
(	(	kIx(	(
<g/>
z	z	k7c2	z
čehož	což	k3yRnSc2	což
je	být	k5eAaImIp3nS	být
36	[number]	k4	36
000	[number]	k4	000
přímých	přímý	k2eAgNnPc2d1	přímé
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnPc1	zastoupení
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
energetické	energetický	k2eAgInPc1d1	energetický
koncerny	koncern	k1gInPc1	koncern
jako	jako	k9	jako
skupina	skupina	k1gFnSc1	skupina
Areva	Arev	k1gMnSc4	Arev
i	i	k9	i
Total	totat	k5eAaImAgInS	totat
a	a	k8xC	a
Électricité	Électricita	k1gMnPc1	Électricita
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Paříži	Paříž	k1gFnSc6	Paříž
převažují	převažovat	k5eAaImIp3nP	převažovat
menší	malý	k2eAgInPc1d2	menší
podniky	podnik	k1gInPc1	podnik
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
oděvní	oděvní	k2eAgInSc1d1	oděvní
a	a	k8xC	a
obuvnický	obuvnický	k2eAgInSc1d1	obuvnický
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
textilií	textilie	k1gFnPc2	textilie
a	a	k8xC	a
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
tvoří	tvořit	k5eAaImIp3nP	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
část	část	k1gFnSc4	část
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
INSEE	INSEE	kA	INSEE
uvádí	uvádět	k5eAaImIp3nS	uvádět
3,8	[number]	k4	3,8
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
pracujících	pracující	k2eAgMnPc2d1	pracující
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
(	(	kIx(	(
<g/>
71	[number]	k4	71
%	%	kIx~	%
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
700	[number]	k4	700
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
obchodu	obchod	k1gInSc6	obchod
(	(	kIx(	(
<g/>
13	[number]	k4	13
%	%	kIx~	%
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnPc1d1	finanční
činnosti	činnost	k1gFnPc1	činnost
představují	představovat	k5eAaImIp3nP	představovat
270	[number]	k4	270
000	[number]	k4	000
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Île-de-France	Îlee-France	k1gFnPc4	Île-de-France
jsou	být	k5eAaImIp3nP	být
ředitelství	ředitelství	k1gNnSc3	ředitelství
velkých	velký	k2eAgFnPc2d1	velká
globálních	globální	k2eAgFnPc2d1	globální
bank	banka	k1gFnPc2	banka
(	(	kIx(	(
<g/>
BNP	BNP	kA	BNP
Paribas	Paribas	k1gInSc1	Paribas
<g/>
,	,	kIx,	,
Société	Sociétý	k2eAgNnSc1d1	Société
Générale	Général	k1gMnSc5	Général
<g/>
,	,	kIx,	,
Crédit	Crédit	k1gFnSc3	Crédit
Agricole	Agricole	k1gFnSc2	Agricole
<g/>
)	)	kIx)	)
a	a	k8xC	a
sídlo	sídlo	k1gNnSc4	sídlo
Euronextu	Euronext	k1gInSc2	Euronext
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
burza	burza	k1gFnSc1	burza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jsou	být	k5eAaImIp3nP	být
kanceláře	kancelář	k1gFnPc1	kancelář
velkých	velký	k2eAgFnPc2d1	velká
finančních	finanční	k2eAgFnPc2d1	finanční
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Lazard	Lazard	k1gMnSc1	Lazard
nebo	nebo	k8xC	nebo
Goldman	Goldman	k1gMnSc1	Goldman
Sachs	Sachsa	k1gFnPc2	Sachsa
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
významnou	významný	k2eAgFnSc7d1	významná
destinací	destinace	k1gFnSc7	destinace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
45	[number]	k4	45
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
regionu	region	k1gInSc2	region
(	(	kIx(	(
<g/>
48	[number]	k4	48
%	%	kIx~	%
bez	bez	k7c2	bez
započtení	započtení	k1gNnSc2	započtení
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
určeny	určit	k5eAaPmNgFnP	určit
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
obilovin	obilovina	k1gFnPc2	obilovina
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
však	však	k9	však
pouhých	pouhý	k2eAgInPc2d1	pouhý
7	[number]	k4	7
600	[number]	k4	600
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
0,5	[number]	k4	0,5
%	%	kIx~	%
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Blízkost	blízkost	k1gFnSc1	blízkost
trhu	trh	k1gInSc2	trh
s	s	k7c7	s
11	[number]	k4	11
miliony	milion	k4xCgInPc1	milion
spotřebitelů	spotřebitel	k1gMnPc2	spotřebitel
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úrodnost	úrodnost	k1gFnSc1	úrodnost
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
technologický	technologický	k2eAgInSc4d1	technologický
rozvoj	rozvoj	k1gInSc4	rozvoj
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Île-de-France	Îlee-France	k1gFnSc1	Île-de-France
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
významnou	významný	k2eAgFnSc7d1	významná
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
oblastí	oblast	k1gFnSc7	oblast
i	i	k9	i
přes	přes	k7c4	přes
stále	stále	k6eAd1	stále
probíhající	probíhající	k2eAgFnSc4d1	probíhající
urbanizaci	urbanizace	k1gFnSc4	urbanizace
<g/>
.	.	kIx.	.
</s>
<s>
Mzdy	mzda	k1gFnPc1	mzda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
regionální	regionální	k2eAgInSc4d1	regionální
průměr	průměr	k1gInSc4	průměr
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
19	[number]	k4	19
euro	euro	k1gNnSc1	euro
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
oproti	oproti	k7c3	oproti
18,2	[number]	k4	18,2
€	€	k?	€
v	v	k7c4	v
Île-de-France	Îlee-Franec	k1gMnSc4	Île-de-Franec
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
13,1	[number]	k4	13,1
€	€	k?	€
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
důsledkem	důsledek	k1gInSc7	důsledek
silného	silný	k2eAgNnSc2d1	silné
zastoupení	zastoupení	k1gNnSc2	zastoupení
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
25	[number]	k4	25
%	%	kIx~	%
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mzdovou	mzdový	k2eAgFnSc7d1	mzdová
nerovností	nerovnost	k1gFnSc7	nerovnost
<g/>
:	:	kIx,	:
10	[number]	k4	10
%	%	kIx~	%
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
s	s	k7c7	s
nejnižším	nízký	k2eAgNnSc7d3	nejnižší
platem	plato	k1gNnSc7	plato
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
<g/>
×	×	k?	×
nižší	nízký	k2eAgInSc4d2	nižší
plat	plat	k1gInSc4	plat
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
s	s	k7c7	s
nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
platy	plat	k1gInPc7	plat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
o	o	k7c4	o
málo	málo	k6eAd1	málo
regionální	regionální	k2eAgInSc4d1	regionální
průměr	průměr	k1gInSc4	průměr
(	(	kIx(	(
<g/>
3,7	[number]	k4	3,7
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
2,6	[number]	k4	2,6
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc4d1	velký
i	i	k8xC	i
geografické	geografický	k2eAgInPc4d1	geografický
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
městě	město	k1gNnSc6	město
<g/>
:	:	kIx,	:
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hodinová	hodinový	k2eAgFnSc1d1	hodinová
mzda	mzda	k1gFnSc1	mzda
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
byla	být	k5eAaImAgFnS	být
24,2	[number]	k4	24,2
€	€	k?	€
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
82	[number]	k4	82
%	%	kIx~	%
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
(	(	kIx(	(
<g/>
13,3	[number]	k4	13,3
€	€	k?	€
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
platech	plat	k1gInPc6	plat
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
a	a	k8xC	a
ženami	žena	k1gFnPc7	žena
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
obdobné	obdobný	k2eAgFnSc6d1	obdobná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
rozdíl	rozdíl	k1gInSc1	rozdíl
činí	činit	k5eAaImIp3nS	činit
jen	jen	k9	jen
6	[number]	k4	6
%	%	kIx~	%
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
10	[number]	k4	10
%	%	kIx~	%
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
tvořil	tvořit	k5eAaImAgInS	tvořit
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
domácnosti	domácnost	k1gFnSc2	domácnost
22	[number]	k4	22
535	[number]	k4	535
€	€	k?	€
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
Paříž	Paříž	k1gFnSc1	Paříž
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
1147	[number]	k4	1147
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c4	mezi
30	[number]	k4	30
687	[number]	k4	687
obcemi	obec	k1gFnPc7	obec
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
domácnostmi	domácnost	k1gFnPc7	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
ekonomiky	ekonomika	k1gFnSc2	ekonomika
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgFnSc3d1	rozmanitá
turistické	turistický	k2eAgFnSc3d1	turistická
nabídce	nabídka	k1gFnSc3	nabídka
(	(	kIx(	(
<g/>
197	[number]	k4	197
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
136	[number]	k4	136
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
91	[number]	k4	91
koncertních	koncertní	k2eAgInPc2d1	koncertní
sálů	sál	k1gInPc2	sál
<g/>
,	,	kIx,	,
110	[number]	k4	110
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
73	[number]	k4	73
památníků	památník	k1gInPc2	památník
apod.	apod.	kA	apod.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
turistickým	turistický	k2eAgFnPc3d1	turistická
destinacím	destinace	k1gFnPc3	destinace
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Paříž	Paříž	k1gFnSc4	Paříž
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
15	[number]	k4	15
039	[number]	k4	039
572	[number]	k4	572
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
8	[number]	k4	8
374	[number]	k4	374
977	[number]	k4	977
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
55,7	[number]	k4	55,7
%	%	kIx~	%
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pokles	pokles	k1gInSc4	pokles
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2007	[number]	k4	2007
o	o	k7c4	o
2,4	[number]	k4	2,4
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
15	[number]	k4	15
414	[number]	k4	414
532	[number]	k4	532
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
představovalo	představovat	k5eAaImAgNnS	představovat
toto	tento	k3xDgNnSc1	tento
ekonomické	ekonomický	k2eAgNnSc1d1	ekonomické
odvětví	odvětví	k1gNnSc1	odvětví
12,5	[number]	k4	12,5
%	%	kIx~	%
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
156	[number]	k4	156
250	[number]	k4	250
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
připadá	připadat	k5eAaPmIp3nS	připadat
36	[number]	k4	36
040	[number]	k4	040
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
v	v	k7c6	v
ubytovacích	ubytovací	k2eAgNnPc6d1	ubytovací
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
87	[number]	k4	87
529	[number]	k4	529
v	v	k7c6	v
restauracích	restaurace	k1gFnPc6	restaurace
<g/>
,	,	kIx,	,
19	[number]	k4	19
216	[number]	k4	216
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
a	a	k8xC	a
13	[number]	k4	13
465	[number]	k4	465
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
15	[number]	k4	15
100	[number]	k4	100
podniků	podnik	k1gInPc2	podnik
zaměřených	zaměřený	k2eAgInPc2d1	zaměřený
na	na	k7c4	na
turismus	turismus	k1gInSc4	turismus
(	(	kIx(	(
<g/>
2	[number]	k4	2
000	[number]	k4	000
ubytovacích	ubytovací	k2eAgNnPc2d1	ubytovací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
10	[number]	k4	10
669	[number]	k4	669
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
1520	[number]	k4	1520
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
a	a	k8xC	a
911	[number]	k4	911
ostatních	ostatní	k2eAgFnPc2d1	ostatní
služeb	služba	k1gFnPc2	služba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Turisté	turist	k1gMnPc1	turist
tvoří	tvořit	k5eAaImIp3nP	tvořit
50	[number]	k4	50
%	%	kIx~	%
návštěvníků	návštěvník	k1gMnPc2	návštěvník
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
8	[number]	k4	8
%	%	kIx~	%
tržeb	tržba	k1gFnPc2	tržba
dopravních	dopravní	k2eAgInPc2d1	dopravní
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
%	%	kIx~	%
nákupů	nákup	k1gInPc2	nákup
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
%	%	kIx~	%
ze	z	k7c2	z
16	[number]	k4	16
miliónů	milión	k4xCgInPc2	milión
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
ubytují	ubytovat	k5eAaPmIp3nP	ubytovat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
9,7	[number]	k4	9,7
miliónu	milión	k4xCgInSc2	milión
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Turistika	turistika	k1gFnSc1	turistika
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
každoročně	každoročně	k6eAd1	každoročně
příjem	příjem	k1gInSc1	příjem
osmi	osm	k4xCc2	osm
miliard	miliarda	k4xCgFnPc2	miliarda
€	€	k?	€
do	do	k7c2	do
zdejší	zdejší	k2eAgFnSc2d1	zdejší
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
30	[number]	k4	30
miliónů	milión	k4xCgInPc2	milión
daňových	daňový	k2eAgInPc2d1	daňový
příjmů	příjem	k1gInPc2	příjem
pro	pro	k7c4	pro
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
padesát	padesát	k4xCc1	padesát
nejnavštěvovanějších	navštěvovaný	k2eAgFnPc2d3	nejnavštěvovanější
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
69,1	[number]	k4	69,1
miliónu	milión	k4xCgInSc2	milión
návštěv	návštěva	k1gFnPc2	návštěva
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nárůst	nárůst	k1gInSc4	nárůst
o	o	k7c4	o
11,3	[number]	k4	11,3
%	%	kIx~	%
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
stojí	stát	k5eAaImIp3nS	stát
katedrála	katedrála	k1gFnSc1	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
13,5	[number]	k4	13,5
milióny	milión	k4xCgInPc4	milión
návštěvníků	návštěvník	k1gMnPc2	návštěvník
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činí	činit	k5eAaImIp3nP	činit
zdaleka	zdaleka	k6eAd1	zdaleka
nejnavštěvovanější	navštěvovaný	k2eAgFnSc4d3	nejnavštěvovanější
historickou	historický	k2eAgFnSc4d1	historická
památku	památka	k1gFnSc4	památka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Baziliku	bazilika	k1gFnSc4	bazilika
Sacré-Cœ	Sacré-Cœ	k1gFnSc2	Sacré-Cœ
na	na	k7c6	na
Montmartru	Montmartr	k1gInSc6	Montmartr
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
10,5	[number]	k4	10,5
miliónu	milión	k4xCgInSc2	milión
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
Louvre	Louvre	k1gInSc4	Louvre
8,3	[number]	k4	8,3
miliónu	milión	k4xCgInSc2	milión
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
nejnavštěvovanějšího	navštěvovaný	k2eAgNnSc2d3	nejnavštěvovanější
muzea	muzeum	k1gNnSc2	muzeum
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
Eiffelovu	Eiffelův	k2eAgFnSc4d1	Eiffelova
věž	věž	k1gFnSc4	věž
6,7	[number]	k4	6,7
miliónu	milión	k4xCgInSc2	milión
a	a	k8xC	a
Centre	centr	k1gInSc5	centr
Georges	Georges	k1gMnSc1	Georges
Pompidou	Pompida	k1gFnSc7	Pompida
5,1	[number]	k4	5,1
miliónu	milión	k4xCgInSc2	milión
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Cité	Citý	k2eAgNnSc1d1	Citý
des	des	k1gNnSc1	des
sciences	sciencesa	k1gFnPc2	sciencesa
et	et	k?	et
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
industrie	industrie	k1gFnSc1	industrie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
Musée	Musée	k1gFnSc1	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsay	k1gInPc1	Orsay
měly	mít	k5eAaImAgInP	mít
tři	tři	k4xCgInPc1	tři
milióny	milión	k4xCgInPc1	milión
ročních	roční	k2eAgMnPc2d1	roční
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Hotely	hotel	k1gInPc1	hotel
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
regionu	region	k1gInSc6	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
představují	představovat	k5eAaImIp3nP	představovat
téměř	téměř	k6eAd1	téměř
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
všech	všecek	k3xTgInPc2	všecek
francouzských	francouzský	k2eAgInPc2d1	francouzský
hotelů	hotel	k1gInPc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
1465	[number]	k4	1465
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
66	[number]	k4	66
turistických	turistický	k2eAgInPc2d1	turistický
bytových	bytový	k2eAgInPc2d1	bytový
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
31	[number]	k4	31
ubytoven	ubytovna	k1gFnPc2	ubytovna
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
vysokoškolských	vysokoškolský	k2eAgFnPc2d1	vysokoškolská
kolejí	kolej	k1gFnPc2	kolej
a	a	k8xC	a
kampusů	kampus	k1gInPc2	kampus
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
kemp	kemp	k1gInSc4	kemp
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
171	[number]	k4	171
600	[number]	k4	600
postelí	postel	k1gFnPc2	postel
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
podniků	podnik	k1gInPc2	podnik
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgFnPc4	tři
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
19,4	[number]	k4	19,4
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
klasifikovány	klasifikován	k2eAgInPc4d1	klasifikován
jako	jako	k8xS	jako
čtyřhvězdičkové	čtyřhvězdičkový	k2eAgInPc4d1	čtyřhvězdičkový
<g/>
.	.	kIx.	.
61	[number]	k4	61
%	%	kIx~	%
hotelů	hotel	k1gInPc2	hotel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
16	[number]	k4	16
%	%	kIx~	%
na	na	k7c6	na
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
předměstích	předměstí	k1gNnPc6	předměstí
a	a	k8xC	a
23	[number]	k4	23
%	%	kIx~	%
na	na	k7c6	na
vnějších	vnější	k2eAgNnPc6d1	vnější
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
obsazenost	obsazenost	k1gFnSc1	obsazenost
hotelů	hotel	k1gInPc2	hotel
činila	činit	k5eAaImAgFnS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
71,3	[number]	k4	71,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Paříž	Paříž	k1gFnSc1	Paříž
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
za	za	k7c4	za
Barcelonu	Barcelona	k1gFnSc4	Barcelona
se	s	k7c7	s
79	[number]	k4	79
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
francouzský	francouzský	k2eAgInSc1d1	francouzský
průměr	průměr	k1gInSc1	průměr
59	[number]	k4	59
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
obsazenosti	obsazenost	k1gFnSc2	obsazenost
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
městských	městský	k2eAgInPc6d1	městský
obvodech	obvod	k1gInPc6	obvod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
rozsahem	rozsah	k1gInSc7	rozsah
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
:	:	kIx,	:
nejlevnější	levný	k2eAgFnPc1d3	nejlevnější
kategorie	kategorie	k1gFnPc1	kategorie
jsou	být	k5eAaImIp3nP	být
nejrychleji	rychle	k6eAd3	rychle
zaplněny	zaplněn	k2eAgFnPc1d1	zaplněna
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
turistika	turistika	k1gFnSc1	turistika
směřuje	směřovat	k5eAaImIp3nS	směřovat
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
67	[number]	k4	67
%	%	kIx~	%
strávených	strávený	k2eAgFnPc2d1	strávená
nocí	noc	k1gFnPc2	noc
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
cizince	cizinec	k1gMnPc4	cizinec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
33	[number]	k4	33
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
tuzemští	tuzemský	k2eAgMnPc1d1	tuzemský
zákazníci	zákazník	k1gMnPc1	zákazník
<g/>
.	.	kIx.	.
65	[number]	k4	65
%	%	kIx~	%
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
návštěvníků	návštěvník	k1gMnPc2	návštěvník
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
ubytování	ubytování	k1gNnSc2	ubytování
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
41	[number]	k4	41
%	%	kIx~	%
francouzských	francouzský	k2eAgMnPc2d1	francouzský
zákazníků	zákazník	k1gMnPc2	zákazník
se	se	k3xPyFc4	se
ubytuje	ubytovat	k5eAaPmIp3nS	ubytovat
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
klientela	klientela	k1gFnSc1	klientela
pochází	pocházet	k5eAaImIp3nS	pocházet
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
osmi	osm	k4xCc2	osm
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgFnSc2d1	Evropská
země	zem	k1gFnSc2	zem
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
a	a	k8xC	a
Belgie	Belgie	k1gFnSc1	Belgie
celkem	celkem	k6eAd1	celkem
tvoří	tvořit	k5eAaImIp3nS	tvořit
42	[number]	k4	42
%	%	kIx~	%
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
klientů	klient	k1gMnPc2	klient
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	s	k7c7	s
17,7	[number]	k4	17,7
%	%	kIx~	%
a	a	k8xC	a
poté	poté	k6eAd1	poté
Japonsko	Japonsko	k1gNnSc1	Japonsko
s	s	k7c7	s
6,5	[number]	k4	6,5
%	%	kIx~	%
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
začala	začít	k5eAaPmAgFnS	začít
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napadlo	napadnout	k5eAaPmAgNnS	napadnout
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
Paříž	Paříž	k1gFnSc1	Paříž
pověst	pověst	k1gFnSc1	pověst
drahého	drahý	k2eAgNnSc2d1	drahé
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
hotelovém	hotelový	k2eAgInSc6d1	hotelový
průmyslu	průmysl	k1gInSc6	průmysl
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
konkurenceschopná	konkurenceschopný	k2eAgFnSc1d1	konkurenceschopná
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
světovými	světový	k2eAgNnPc7d1	světové
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
nadprůměrná	nadprůměrný	k2eAgFnSc1d1	nadprůměrná
v	v	k7c6	v
cenách	cena	k1gFnPc6	cena
dvou	dva	k4xCgInPc2	dva
a	a	k8xC	a
tříhvězdičkových	tříhvězdičkový	k2eAgInPc2d1	tříhvězdičkový
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejdražší	drahý	k2eAgFnSc1d3	nejdražší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
luxusních	luxusní	k2eAgInPc2d1	luxusní
hotelů	hotel	k1gInPc2	hotel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
turistickým	turistický	k2eAgInSc7d1	turistický
ruchem	ruch	k1gInSc7	ruch
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
zdejší	zdejší	k2eAgFnSc4d1	zdejší
zvláštnost	zvláštnost	k1gFnSc4	zvláštnost
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pařížský	pařížský	k2eAgInSc1d1	pařížský
syndrom	syndrom	k1gInSc1	syndrom
<g/>
,	,	kIx,	,
přechodná	přechodný	k2eAgFnSc1d1	přechodná
psychická	psychický	k2eAgFnSc1d1	psychická
porucha	porucha	k1gFnSc1	porucha
postihující	postihující	k2eAgFnSc1d1	postihující
zejména	zejména	k9	zejména
japonské	japonský	k2eAgMnPc4d1	japonský
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
světové	světový	k2eAgFnSc2d1	světová
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházelo	nacházet	k5eAaImAgNnS	nacházet
106	[number]	k4	106
francouzských	francouzský	k2eAgInPc2d1	francouzský
podniků	podnik	k1gInPc2	podnik
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
haute	haut	k1gMnSc5	haut
couture	coutur	k1gMnSc5	coutur
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejstarším	starý	k2eAgMnPc3d3	nejstarší
patří	patřit	k5eAaImIp3nS	patřit
Givenchy	Givencha	k1gFnPc4	Givencha
<g/>
,	,	kIx,	,
Dior	Dior	k1gInSc1	Dior
<g/>
,	,	kIx,	,
Jean-Louis	Jean-Louis	k1gFnSc1	Jean-Louis
Scherrer	Scherrer	k1gMnSc1	Scherrer
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
Ungaro	Ungara	k1gFnSc5	Ungara
<g/>
,	,	kIx,	,
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
,	,	kIx,	,
Yves	Yves	k1gInSc1	Yves
Saint-Laurent	Saint-Laurent	k1gInSc1	Saint-Laurent
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
Pierre	Pierr	k1gInSc5	Pierr
Cardin	Cardin	k2eAgMnSc1d1	Cardin
a	a	k8xC	a
André	André	k1gMnSc1	André
Courrè	Courrè	k1gMnSc1	Courrè
nebo	nebo	k8xC	nebo
menší	malý	k2eAgFnSc1d2	menší
Dominique	Dominique	k1gFnSc1	Dominique
Sirop	Sirop	k1gInSc1	Sirop
<g/>
,	,	kIx,	,
Adeline	Adelin	k1gInSc5	Adelin
André	André	k1gMnSc1	André
a	a	k8xC	a
Franck	Franck	k1gMnSc1	Franck
Sorbier	Sorbier	k1gMnSc1	Sorbier
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
módní	módní	k2eAgInPc1d1	módní
domy	dům	k1gInPc1	dům
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgMnPc4d1	známý
nejenom	nejenom	k6eAd1	nejenom
v	v	k7c6	v
módě	móda	k1gFnSc6	móda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kosmetiky	kosmetika	k1gFnSc2	kosmetika
<g/>
,	,	kIx,	,
k	k	k7c3	k
letitým	letitý	k2eAgFnPc3d1	letitá
značkám	značka	k1gFnPc3	značka
patří	patřit	k5eAaImIp3nS	patřit
Chanel	Chanel	k1gMnSc1	Chanel
č.	č.	k?	č.
5	[number]	k4	5
či	či	k8xC	či
Arpè	Arpè	k1gFnPc1	Arpè
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Miss	miss	k1gFnSc1	miss
Dior	Diora	k1gFnPc2	Diora
ze	z	k7c2	z
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
parfémy	parfém	k1gInPc7	parfém
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
nabídka	nabídka	k1gFnSc1	nabídka
koženého	kožený	k2eAgNnSc2d1	kožené
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
firmy	firma	k1gFnPc1	firma
Louis	louis	k1gInSc2	louis
Vuitton	Vuitton	k1gInSc1	Vuitton
a	a	k8xC	a
Hermè	Hermè	k1gFnSc1	Hermè
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
firmy	firma	k1gFnPc1	firma
mají	mít	k5eAaImIp3nP	mít
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
módními	módní	k2eAgInPc7d1	módní
doplňky	doplněk	k1gInPc7	doplněk
<g/>
:	:	kIx,	:
Guy	Guy	k1gFnSc1	Guy
Laroche	Laroche	k1gFnSc1	Laroche
<g/>
,	,	kIx,	,
Nina	Nina	k1gFnSc1	Nina
Ricci	Ricce	k1gFnSc4	Ricce
<g/>
,	,	kIx,	,
Marcel	Marcel	k1gMnSc1	Marcel
Rochas	Rochas	k1gMnSc1	Rochas
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
Balmain	Balmain	k1gInSc1	Balmain
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
módním	módní	k2eAgMnPc3d1	módní
návrhářům	návrhář	k1gMnPc3	návrhář
žijícím	žijící	k2eAgFnPc3d1	žijící
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Gaultier	Gaultier	k1gInSc1	Gaultier
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
módy	móda	k1gFnSc2	móda
korzety	korzet	k1gInPc1	korzet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Claude	Claud	k1gInSc5	Claud
Montana	Montana	k1gFnSc1	Montana
<g/>
,	,	kIx,	,
Christian	Christian	k1gMnSc1	Christian
Lacroix	Lacroix	k1gInSc1	Lacroix
nebo	nebo	k8xC	nebo
Chantal	Chantal	k1gMnSc1	Chantal
Thomass	Thomass	k1gInSc1	Thomass
(	(	kIx(	(
<g/>
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
prê	prê	k?	prê
tvoří	tvořit	k5eAaImIp3nS	tvořit
Jean-Charles	Jean-Charles	k1gInSc4	Jean-Charles
de	de	k?	de
Castelbajac	Castelbajac	k1gInSc1	Castelbajac
nebo	nebo	k8xC	nebo
Vanessa	Vanessa	k1gFnSc1	Vanessa
Bruno	Bruno	k1gMnSc1	Bruno
a	a	k8xC	a
Isabel	Isabela	k1gFnPc2	Isabela
Marant	Maranta	k1gFnPc2	Maranta
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
Paříž	Paříž	k1gFnSc1	Paříž
čelí	čelit	k5eAaImIp3nS	čelit
konkurenci	konkurence	k1gFnSc4	konkurence
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
Milána	Milán	k1gInSc2	Milán
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
asijských	asijský	k2eAgFnPc2d1	asijská
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
drží	držet	k5eAaImIp3nP	držet
významnou	významný	k2eAgFnSc4d1	významná
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
šperků	šperk	k1gInPc2	šperk
(	(	kIx(	(
<g/>
koncentrované	koncentrovaný	k2eAgFnPc4d1	koncentrovaná
na	na	k7c6	na
Place	plac	k1gInSc6	plac
Vendôme	Vendôm	k1gInSc5	Vendôm
a	a	k8xC	a
Rue	Rue	k1gMnPc2	Rue
de	de	k?	de
la	la	k1gNnPc2	la
Paix	Paix	k1gInSc1	Paix
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
haute	haut	k1gMnSc5	haut
couture	coutur	k1gMnSc5	coutur
<g/>
.	.	kIx.	.
</s>
<s>
Luxusní	luxusní	k2eAgInPc1d1	luxusní
oděvy	oděv	k1gInPc1	oděv
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
především	především	k9	především
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
na	na	k7c6	na
Avenue	avenue	k1gFnSc6	avenue
Montaigne	Montaign	k1gMnSc5	Montaign
a	a	k8xC	a
zejména	zejména	k9	zejména
Rue	Rue	k1gMnSc1	Rue
du	du	k?	du
Faubourg-Saint-Honoré	Faubourg-Saint-Honorý	k2eAgNnSc1d1	Faubourg-Saint-Honorý
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
ředitelství	ředitelství	k1gNnSc1	ředitelství
firem	firma	k1gFnPc2	firma
LVMH	LVMH	kA	LVMH
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnPc4d3	veliký
holdingové	holdingový	k2eAgFnPc4d1	holdingová
společnosti	společnost	k1gFnPc4	společnost
v	v	k7c6	v
sektoru	sektor	k1gInSc6	sektor
luxusního	luxusní	k2eAgNnSc2d1	luxusní
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
Hermè	Hermè	k1gFnPc2	Hermè
<g/>
,	,	kIx,	,
Cartier	Cartira	k1gFnPc2	Cartira
<g/>
,	,	kIx,	,
Dior	Diora	k1gFnPc2	Diora
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
butiků	butik	k1gInPc2	butik
slavných	slavný	k2eAgInPc2d1	slavný
módních	módní	k2eAgInPc2d1	módní
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
"	"	kIx"	"
<g/>
shoppingu	shopping	k1gInSc3	shopping
<g/>
"	"	kIx"	"
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
významné	významný	k2eAgInPc4d1	významný
obchodní	obchodní	k2eAgInPc4d1	obchodní
domy	dům	k1gInPc4	dům
s	s	k7c7	s
pobočkami	pobočka	k1gFnPc7	pobočka
i	i	k8xC	i
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Galeries	Galeries	k1gInSc1	Galeries
Lafayette	Lafayett	k1gInSc5	Lafayett
a	a	k8xC	a
Printemps	Printempsa	k1gFnPc2	Printempsa
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
na	na	k7c6	na
Boulevardu	Boulevard	k1gInSc6	Boulevard
Haussmann	Haussmanna	k1gFnPc2	Haussmanna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nabízel	nabízet	k5eAaImAgInS	nabízet
kompletní	kompletní	k2eAgInSc4d1	kompletní
sortiment	sortiment	k1gInSc4	sortiment
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Le	Le	k1gFnSc2	Le
Bon	bon	k1gInSc4	bon
Marché	Marchý	k2eAgNnSc1d1	Marché
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
pařížský	pařížský	k2eAgInSc1d1	pařížský
obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
La	la	k1gNnSc2	la
Samaritaine	Samaritain	k1gInSc5	Samaritain
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
s	s	k7c7	s
nákupní	nákupní	k2eAgFnSc7d1	nákupní
plochou	plocha	k1gFnSc7	plocha
48	[number]	k4	48
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
jeho	jeho	k3xOp3gFnSc1	jeho
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
přestavba	přestavba	k1gFnSc1	přestavba
na	na	k7c4	na
kanceláře	kancelář	k1gFnPc4	kancelář
a	a	k8xC	a
byty	byt	k1gInPc4	byt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
různá	různý	k2eAgNnPc4d1	různé
nákupní	nákupní	k2eAgNnPc4d1	nákupní
centra	centrum	k1gNnPc4	centrum
(	(	kIx(	(
<g/>
největší	veliký	k2eAgNnSc4d3	veliký
je	být	k5eAaImIp3nS	být
Forum	forum	k1gNnSc1	forum
des	des	k1gNnSc2	des
Halles	Hallesa	k1gFnPc2	Hallesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgNnPc3d3	nejvýznamnější
tržištím	tržiště	k1gNnPc3	tržiště
patří	patřit	k5eAaImIp3nS	patřit
Aligre	Aligr	k1gInSc5	Aligr
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Opery	opera	k1gFnSc2	opera
Bastilla	Bastillo	k1gNnSc2	Bastillo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
keramika	keramika	k1gFnSc1	keramika
a	a	k8xC	a
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc4	potravina
a	a	k8xC	a
květiny	květina	k1gFnPc4	květina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
denní	denní	k2eAgFnPc1d1	denní
či	či	k8xC	či
týdenní	týdenní	k2eAgInPc1d1	týdenní
trhy	trh	k1gInPc1	trh
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
bleší	bleší	k2eAgInSc1d1	bleší
trh	trh	k1gInSc1	trh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
poblíž	poblíž	k6eAd1	poblíž
Porte	port	k1gInSc5	port
de	de	k?	de
Clignancourt	Clignancourt	k1gInSc1	Clignancourt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
vzájemně	vzájemně	k6eAd1	vzájemně
propojených	propojený	k2eAgFnPc2d1	propojená
tržnic	tržnice	k1gFnPc2	tržnice
<g/>
.	.	kIx.	.
</s>
<s>
Trh	trh	k1gInSc1	trh
s	s	k7c7	s
květinami	květina	k1gFnPc7	květina
a	a	k8xC	a
s	s	k7c7	s
ptáky	pták	k1gMnPc7	pták
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Place	plac	k1gInSc6	plac
Louis-Lépine	Louis-Lépin	k1gInSc5	Louis-Lépin
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Cité	Citá	k1gFnSc2	Citá
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
Paříže	Paříž	k1gFnSc2	Paříž
jsou	být	k5eAaImIp3nP	být
bukinisté	bukinista	k1gMnPc1	bukinista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prodávají	prodávat	k5eAaImIp3nP	prodávat
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
pohlednice	pohlednice	k1gFnPc4	pohlednice
a	a	k8xC	a
obrazy	obraz	k1gInPc4	obraz
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
Seiny	Seina	k1gFnSc2	Seina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
významu	význam	k1gInSc3	význam
města	město	k1gNnSc2	město
různorodá	různorodý	k2eAgFnSc1d1	různorodá
a	a	k8xC	a
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
ji	on	k3xPp3gFnSc4	on
různé	různý	k2eAgFnPc1d1	různá
dopravní	dopravní	k2eAgFnPc1d1	dopravní
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
provozovateli	provozovatel	k1gMnPc7	provozovatel
jsou	být	k5eAaImIp3nP	být
společnosti	společnost	k1gFnPc1	společnost
RATP	RATP	kA	RATP
(	(	kIx(	(
<g/>
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
větve	větev	k1gFnSc2	větev
linek	linka	k1gFnPc2	linka
RER	RER	kA	RER
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
<g/>
)	)	kIx)	)
a	a	k8xC	a
SNCF	SNCF	kA	SNCF
(	(	kIx(	(
<g/>
Transilien	Transilien	k1gInSc1	Transilien
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
větve	větev	k1gFnSc2	větev
linek	linka	k1gFnPc2	linka
RER	RER	kA	RER
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
a	a	k8xC	a
linky	linka	k1gFnSc2	linka
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
a	a	k8xC	a
E	E	kA	E
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
Optile	Optila	k1gFnSc3	Optila
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
téměř	téměř	k6eAd1	téměř
sto	sto	k4xCgNnSc1	sto
soukromých	soukromý	k2eAgFnPc2d1	soukromá
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
provozují	provozovat	k5eAaImIp3nP	provozovat
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
dopravci	dopravce	k1gMnPc1	dopravce
jsou	být	k5eAaImIp3nP	být
zastřešeni	zastřešit	k5eAaPmNgMnP	zastřešit
asociací	asociace	k1gFnPc2	asociace
STIF	STIF	kA	STIF
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
veškerou	veškerý	k3xTgFnSc4	veškerý
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
území	území	k1gNnSc6	území
regionu	region	k1gInSc2	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
<g/>
.	.	kIx.	.
</s>
<s>
Samostatně	samostatně	k6eAd1	samostatně
působí	působit	k5eAaImIp3nS	působit
společnost	společnost	k1gFnSc1	společnost
Aéroports	Aéroportsa	k1gFnPc2	Aéroportsa
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
všechna	všechen	k3xTgNnPc4	všechen
pařížská	pařížský	k2eAgNnPc4d1	pařížské
letiště	letiště	k1gNnPc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Metro	metro	k1gNnSc4	metro
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Pařížské	pařížský	k2eAgNnSc1d1	pařížské
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
páteřní	páteřní	k2eAgInPc4d1	páteřní
dopravní	dopravní	k2eAgInPc4d1	dopravní
stí	stý	k4xOgMnPc1	stý
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
místy	místy	k6eAd1	místy
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
sousedních	sousední	k2eAgNnPc2d1	sousední
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
stanic	stanice	k1gFnPc2	stanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
linek	linka	k1gFnPc2	linka
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
okrajových	okrajový	k2eAgInPc2d1	okrajový
úseků	úsek	k1gInPc2	úsek
jiných	jiný	k2eAgFnPc2d1	jiná
linek	linka	k1gFnPc2	linka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
linka	linka	k1gFnSc1	linka
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1900	[number]	k4	1900
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
(	(	kIx(	(
<g/>
výstavba	výstavba	k1gFnSc1	výstavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
metro	metro	k1gNnSc1	metro
velmi	velmi	k6eAd1	velmi
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
šestnáct	šestnáct	k4xCc4	šestnáct
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
dvě	dva	k4xCgFnPc1	dva
linky	linka	k1gFnPc1	linka
(	(	kIx(	(
<g/>
7	[number]	k4	7
a	a	k8xC	a
13	[number]	k4	13
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
rozvětvené	rozvětvený	k2eAgInPc1d1	rozvětvený
<g/>
,	,	kIx,	,
a	a	k8xC	a
301	[number]	k4	301
stanici	stanice	k1gFnSc4	stanice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
242	[number]	k4	242
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
59	[number]	k4	59
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
linka	linka	k1gFnSc1	linka
14	[number]	k4	14
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
jako	jako	k8xC	jako
plně	plně	k6eAd1	plně
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
linky	linka	k1gFnPc1	linka
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
síť	síť	k1gFnSc1	síť
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1400	[number]	k4	1400
linek	linka	k1gFnPc2	linka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
především	především	k9	především
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Paříži	Paříž	k1gFnSc6	Paříž
již	již	k6eAd1	již
méně	málo	k6eAd2	málo
a	a	k8xC	a
zde	zde	k6eAd1	zde
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
tramvaje	tramvaj	k1gFnPc1	tramvaj
tažené	tažený	k2eAgFnPc1d1	tažená
koňmi	kůň	k1gMnPc7	kůň
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
parní	parní	k2eAgFnPc1d1	parní
tramvaje	tramvaj	k1gFnPc1	tramvaj
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
byly	být	k5eAaImAgFnP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
tramvajemi	tramvaj	k1gFnPc7	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
aglomerace	aglomerace	k1gFnSc1	aglomerace
měla	mít	k5eAaImAgFnS	mít
kdysi	kdysi	k6eAd1	kdysi
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
síť	síť	k1gFnSc4	síť
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
960	[number]	k4	960
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c7	za
zastaralou	zastaralý	k2eAgFnSc7d1	zastaralá
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
nahrazována	nahrazován	k2eAgFnSc1d1	nahrazována
autobusy	autobus	k1gInPc7	autobus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zmizely	zmizet	k5eAaPmAgFnP	zmizet
tramvaje	tramvaj	k1gFnPc1	tramvaj
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
jsou	být	k5eAaImIp3nP	být
tramvaje	tramvaj	k1gFnPc1	tramvaj
opět	opět	k6eAd1	opět
postupně	postupně	k6eAd1	postupně
zaváděny	zaváděn	k2eAgInPc1d1	zaváděn
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
T2	T2	k1gFnSc1	T2
a	a	k8xC	a
T	T	kA	T
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
linka	linka	k1gFnSc1	linka
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
na	na	k7c6	na
území	území	k1gNnSc6	území
Paříže	Paříž	k1gFnSc2	Paříž
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
její	její	k3xOp3gNnSc1	její
prodloužení	prodloužení	k1gNnSc1	prodloužení
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
příprav	příprava	k1gFnPc2	příprava
či	či	k8xC	či
výstavby	výstavba	k1gFnSc2	výstavba
i	i	k8xC	i
další	další	k2eAgFnPc4d1	další
linky	linka	k1gFnPc4	linka
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
disponuje	disponovat	k5eAaBmIp3nS	disponovat
ještě	ještě	k6eAd1	ještě
dalšími	další	k2eAgFnPc7d1	další
možnostmi	možnost	k1gFnPc7	možnost
<g/>
:	:	kIx,	:
Lanovka	lanovka	k1gFnSc1	lanovka
na	na	k7c4	na
Montmartre	Montmartr	k1gInSc5	Montmartr
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vystoupat	vystoupat	k5eAaPmF	vystoupat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Montmartru	Montmartr	k1gInSc2	Montmartr
<g/>
.	.	kIx.	.
</s>
<s>
Orlyval	Orlyvat	k5eAaBmAgMnS	Orlyvat
je	být	k5eAaImIp3nS	být
lehké	lehký	k2eAgNnSc4d1	lehké
metro	metro	k1gNnSc4	metro
spojující	spojující	k2eAgFnSc4d1	spojující
stanici	stanice	k1gFnSc4	stanice
Antony	anton	k1gInPc1	anton
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
RER	RER	kA	RER
B	B	kA	B
a	a	k8xC	a
Letiště	letiště	k1gNnSc2	letiště
Orly	Orel	k1gMnPc4	Orel
<g/>
.	.	kIx.	.
</s>
<s>
CDGVAL	CDGVAL	kA	CDGVAL
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
kyvadlové	kyvadlový	k2eAgFnSc2d1	kyvadlová
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
Letišti	letiště	k1gNnSc6	letiště
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Gaulla	Gaull	k1gMnSc2	Gaull
<g/>
.	.	kIx.	.
</s>
<s>
Voguéo	Voguéo	k6eAd1	Voguéo
je	být	k5eAaImIp3nS	být
říční	říční	k2eAgFnSc1d1	říční
linka	linka	k1gFnSc1	linka
na	na	k7c6	na
Seině	Seina	k1gFnSc6	Seina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
CDG	CDG	kA	CDG
Express	express	k1gInSc1	express
je	být	k5eAaImIp3nS	být
plánované	plánovaný	k2eAgNnSc1d1	plánované
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
nádražím	nádraží	k1gNnSc7	nádraží
Gare	Gar	k1gFnSc2	Gar
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Est	Est	k1gMnSc1	Est
a	a	k8xC	a
letištěm	letiště	k1gNnSc7	letiště
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Gaulla	Gaull	k1gMnSc2	Gaull
<g/>
.	.	kIx.	.
</s>
<s>
Vélib	Vélib	k1gInSc1	Vélib
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
veřejné	veřejný	k2eAgFnSc2d1	veřejná
půjčovny	půjčovna	k1gFnSc2	půjčovna
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
cyklostezek	cyklostezka	k1gFnPc2	cyklostezka
budovaná	budovaný	k2eAgFnSc1d1	budovaná
od	od	k7c2	od
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
měřila	měřit	k5eAaImAgFnS	měřit
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
371	[number]	k4	371
km	km	kA	km
včetně	včetně	k7c2	včetně
pruhů	pruh	k1gInPc2	pruh
pro	pro	k7c4	pro
autobusy	autobus	k1gInPc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
železnice	železnice	k1gFnSc1	železnice
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
mezi	mezi	k7c7	mezi
Paříží	Paříž	k1gFnSc7	Paříž
(	(	kIx(	(
<g/>
nádraží	nádraží	k1gNnSc4	nádraží
Saint-Lazare	Saint-Lazar	k1gMnSc5	Saint-Lazar
<g/>
)	)	kIx)	)
a	a	k8xC	a
městem	město	k1gNnSc7	město
Le	Le	k1gMnSc2	Le
Pecq	Pecq	k1gMnSc2	Pecq
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
mnoho	mnoho	k4c1	mnoho
železničních	železniční	k2eAgFnPc2d1	železniční
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
jako	jako	k9	jako
konečné	konečná	k1gFnSc3	konečná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Petite	petit	k1gInSc5	petit
Ceinture	Ceintur	k1gMnSc5	Ceintur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
je	být	k5eAaImIp3nS	být
šest	šest	k4xCc1	šest
velkých	velký	k2eAgNnPc2d1	velké
nádraží	nádraží	k1gNnPc2	nádraží
<g/>
:	:	kIx,	:
Gare	Gare	k1gInSc1	Gare
de	de	k?	de
Paris-Nord	Paris-Nord	k1gInSc1	Paris-Nord
<g/>
,	,	kIx,	,
Gare	Garus	k1gMnSc5	Garus
Saint-Lazare	Saint-Lazar	k1gMnSc5	Saint-Lazar
<g/>
,	,	kIx,	,
Gare	Gar	k1gInPc1	Gar
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Est	Est	k1gMnSc1	Est
<g/>
,	,	kIx,	,
Gare	Gare	k1gFnSc1	Gare
Montparnasse	Montparnasse	k1gFnSc1	Montparnasse
<g/>
,	,	kIx,	,
Gare	Gare	k1gInSc1	Gare
de	de	k?	de
Lyon	Lyon	k1gInSc1	Lyon
a	a	k8xC	a
Gare	Gare	k1gInSc1	Gare
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Austerlitz	Austerlitza	k1gFnPc2	Austerlitza
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgFnPc4d1	hlavní
tratě	trať	k1gFnPc4	trať
i	i	k8xC	i
příměstskou	příměstský	k2eAgFnSc4d1	příměstská
železnici	železnice	k1gFnSc4	železnice
a	a	k8xC	a
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
Paříže	Paříž	k1gFnSc2	Paříž
s	s	k7c7	s
předměstími	předměstí	k1gNnPc7	předměstí
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
linky	linka	k1gFnPc1	linka
RER	RER	kA	RER
a	a	k8xC	a
Transilien	Transilien	k1gInSc1	Transilien
<g/>
.	.	kIx.	.
</s>
<s>
RER	RER	kA	RER
je	být	k5eAaImIp3nS	být
příměstská	příměstský	k2eAgFnSc1d1	příměstská
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zajíždí	zajíždět	k5eAaImIp3nS	zajíždět
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
propojeny	propojit	k5eAaPmNgFnP	propojit
stávající	stávající	k2eAgFnPc1d1	stávající
příměstské	příměstský	k2eAgFnPc1d1	příměstská
železniční	železniční	k2eAgFnPc1d1	železniční
linky	linka	k1gFnPc1	linka
do	do	k7c2	do
jednotného	jednotný	k2eAgInSc2d1	jednotný
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
je	být	k5eAaImIp3nS	být
RER	RER	kA	RER
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k8xS	jako
expresní	expresní	k2eAgFnSc1d1	expresní
síť	síť	k1gFnSc1	síť
s	s	k7c7	s
přestupy	přestup	k1gInPc7	přestup
na	na	k7c4	na
metro	metro	k1gNnSc4	metro
a	a	k8xC	a
jako	jako	k9	jako
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
předměstími	předměstí	k1gNnPc7	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
měl	mít	k5eAaImAgMnS	mít
RER	RER	kA	RER
pět	pět	k4xCc4	pět
linek	linka	k1gFnPc2	linka
(	(	kIx(	(
<g/>
nejnovější	nový	k2eAgInPc4d3	nejnovější
RER	RER	kA	RER
E	E	kA	E
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
257	[number]	k4	257
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
33	[number]	k4	33
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
a	a	k8xC	a
587	[number]	k4	587
km	km	kA	km
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Transilien	Transilien	k1gInSc1	Transilien
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
sítě	síť	k1gFnSc2	síť
příměstské	příměstský	k2eAgFnSc2d1	příměstská
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
SNCF	SNCF	kA	SNCF
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
linek	linka	k1gFnPc2	linka
RER	RER	kA	RER
linky	linka	k1gFnPc1	linka
Transilien	Transilino	k1gNnPc2	Transilino
neprotínají	protínat	k5eNaImIp3nP	protínat
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
konečnou	konečný	k2eAgFnSc4d1	konečná
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
železničních	železniční	k2eAgFnPc2d1	železniční
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
poskytovanou	poskytovaný	k2eAgFnSc7d1	poskytovaná
RER	RER	kA	RER
<g/>
.	.	kIx.	.
</s>
<s>
Umožňují	umožňovat	k5eAaImIp3nP	umožňovat
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
vzdálenějšími	vzdálený	k2eAgFnPc7d2	vzdálenější
částmi	část	k1gFnPc7	část
regionu	region	k1gInSc2	region
Île-de-France	Îlee-France	k1gFnPc1	Île-de-France
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
měl	mít	k5eAaImAgInS	mít
Transilien	Transilien	k1gInSc1	Transilien
15	[number]	k4	15
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
sedm	sedm	k4xCc1	sedm
končilo	končit	k5eAaImAgNnS	končit
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
železničních	železniční	k2eAgFnPc6d1	železniční
stanicích	stanice	k1gFnPc6	stanice
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
linky	linka	k1gFnPc1	linka
mají	mít	k5eAaImIp3nP	mít
konečnou	konečná	k1gFnSc4	konečná
i	i	k9	i
mimo	mimo	k7c4	mimo
region	region	k1gInSc4	region
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
široké	široký	k2eAgInPc4d1	široký
bulváry	bulvár	k1gInPc4	bulvár
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
automobilové	automobilový	k2eAgFnSc3d1	automobilová
dopravě	doprava	k1gFnSc3	doprava
přesto	přesto	k8xC	přesto
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Automobilový	automobilový	k2eAgInSc1d1	automobilový
provoz	provoz	k1gInSc1	provoz
a	a	k8xC	a
parkování	parkování	k1gNnSc1	parkování
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
problematické	problematický	k2eAgFnPc1d1	problematická
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
světových	světový	k2eAgFnPc2d1	světová
metropolí	metropol	k1gFnPc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Elektronické	elektronický	k2eAgInPc4d1	elektronický
senzory	senzor	k1gInPc4	senzor
měřící	měřící	k2eAgInPc4d1	měřící
provoz	provoz	k1gInSc4	provoz
jsou	být	k5eAaImIp3nP	být
umístěné	umístěný	k2eAgInPc1d1	umístěný
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
hlavních	hlavní	k2eAgInPc6d1	hlavní
pařížských	pařížský	k2eAgInPc6d1	pařížský
silničních	silniční	k2eAgInPc6d1	silniční
tazích	tag	k1gInPc6	tag
a	a	k8xC	a
na	na	k7c6	na
dálnicích	dálnice	k1gFnPc6	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Vyhodnocené	vyhodnocený	k2eAgInPc1d1	vyhodnocený
údaje	údaj	k1gInPc1	údaj
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
stanovení	stanovení	k1gNnSc3	stanovení
hustoty	hustota	k1gFnSc2	hustota
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
oznámení	oznámení	k1gNnSc1	oznámení
dopravní	dopravní	k2eAgFnSc2d1	dopravní
zácpy	zácpa	k1gFnSc2	zácpa
a	a	k8xC	a
odhadu	odhad	k1gInSc2	odhad
doby	doba	k1gFnSc2	doba
jízdy	jízda	k1gFnSc2	jízda
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
stanovišti	stanoviště	k1gNnPc7	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgFnPc1d1	přístupná
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
a	a	k8xC	a
na	na	k7c6	na
světelných	světelný	k2eAgInPc6d1	světelný
silničních	silniční	k2eAgInPc6d1	silniční
ukazatelích	ukazatel	k1gInPc6	ukazatel
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnPc1	dálnice
spojují	spojovat	k5eAaImIp3nP	spojovat
Paříž	Paříž	k1gFnSc4	Paříž
s	s	k7c7	s
jihem	jih	k1gInSc7	jih
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
Dijon	Dijon	k1gInSc1	Dijon
<g/>
,	,	kIx,	,
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severem	sever	k1gInSc7	sever
(	(	kIx(	(
<g/>
Lille	Lille	k1gInSc1	Lille
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Normandií	Normandie	k1gFnSc7	Normandie
(	(	kIx(	(
<g/>
Rouen	Rouen	k1gInSc1	Rouen
<g/>
,	,	kIx,	,
Caen	Caen	k1gInSc1	Caen
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc5	Le
Havre	Havr	k1gMnSc5	Havr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
východem	východ	k1gInSc7	východ
(	(	kIx(	(
<g/>
Remeš	Remeš	k1gFnSc1	Remeš
<g/>
,	,	kIx,	,
Mety	Mety	k1gFnPc1	Mety
<g/>
,	,	kIx,	,
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Akvitánií	Akvitánie	k1gFnSc7	Akvitánie
(	(	kIx(	(
<g/>
Orléans	Orléans	k1gInSc1	Orléans
<g/>
,	,	kIx,	,
Bordeaux	Bordeaux	k1gNnSc1	Bordeaux
<g/>
)	)	kIx)	)
a	a	k8xC	a
pobřežím	pobřeží	k1gNnSc7	pobřeží
Atlantiku	Atlantik	k1gInSc2	Atlantik
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Mans	Mans	k1gInSc1	Mans
<g/>
,	,	kIx,	,
Nantes	Nantes	k1gInSc1	Nantes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
se	se	k3xPyFc4	se
napojují	napojovat	k5eAaImIp3nP	napojovat
na	na	k7c4	na
městský	městský	k2eAgInSc4d1	městský
okruh	okruh	k1gInSc4	okruh
boulevard	boulevarda	k1gFnPc2	boulevarda
périphérique	périphériqu	k1gInSc2	périphériqu
<g/>
.	.	kIx.	.
</s>
<s>
Letecké	letecký	k2eAgNnSc1d1	letecké
spojení	spojení	k1gNnSc1	spojení
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
existuje	existovat	k5eAaImIp3nS	existovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zprovozněno	zprovoznit	k5eAaPmNgNnS	zprovoznit
Letiště	letiště	k1gNnSc1	letiště
Le	Le	k1gFnSc2	Le
Bourget	Bourgeta	k1gFnPc2	Bourgeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Letiště	letiště	k1gNnSc1	letiště
Orly	Orel	k1gMnPc4	Orel
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
pařížské	pařížský	k2eAgNnSc1d1	pařížské
Letiště	letiště	k1gNnSc1	letiště
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Gaulla	Gaull	k1gMnSc2	Gaull
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Heliport	heliport	k1gInSc1	heliport
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
Issy-les-Moulineaux	Issyes-Moulineaux	k1gInSc1	Issy-les-Moulineaux
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
provozovatelem	provozovatel	k1gMnSc7	provozovatel
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Aéroports	Aéroportsa	k1gFnPc2	Aéroportsa
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Le	Le	k1gFnSc6	Le
Bourget	Bourgeta	k1gFnPc2	Bourgeta
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k6eAd1	jen
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
letectví	letectví	k1gNnSc3	letectví
a	a	k8xC	a
vládním	vládní	k2eAgInPc3d1	vládní
letům	let	k1gInPc3	let
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
známého	známý	k1gMnSc2	známý
aerosalónu	aerosalón	k1gInSc2	aerosalón
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejznámější	známý	k2eAgNnSc4d3	nejznámější
letecké	letecký	k2eAgNnSc4d1	letecké
muzeum	muzeum	k1gNnSc4	muzeum
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
množství	množství	k1gNnSc1	množství
nemocnic	nemocnice	k1gFnPc2	nemocnice
-	-	kIx~	-
všeobecných	všeobecný	k2eAgInPc2d1	všeobecný
a	a	k8xC	a
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
<g/>
,	,	kIx,	,
fakultních	fakultní	k2eAgInPc2d1	fakultní
i	i	k8xC	i
vojenských	vojenský	k2eAgInPc2d1	vojenský
aj.	aj.	kA	aj.
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
starou	starý	k2eAgFnSc4d1	stará
historii	historie	k1gFnSc4	historie
sahající	sahající	k2eAgFnSc4d1	sahající
až	až	k9	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Špitál	špitál	k1gInSc1	špitál
Hôtel-Dieu	Hôtel-Dieus	k1gInSc2	Hôtel-Dieus
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Cité	Citá	k1gFnSc2	Citá
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
651	[number]	k4	651
pařížský	pařížský	k2eAgInSc4d1	pařížský
biskup	biskup	k1gInSc4	biskup
svatý	svatý	k2eAgMnSc1d1	svatý
Landerik	Landerik	k1gMnSc1	Landerik
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
nemocnicí	nemocnice	k1gFnSc7	nemocnice
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1260	[number]	k4	1260
založil	založit	k5eAaPmAgMnS	založit
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
léčebný	léčebný	k2eAgInSc4d1	léčebný
ústav	ústav	k1gInSc4	ústav
pro	pro	k7c4	pro
slepce	slepec	k1gMnPc4	slepec
Quinze-Vingts	Quinze-Vingtsa	k1gFnPc2	Quinze-Vingtsa
(	(	kIx(	(
<g/>
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
oftalmologii	oftalmologie	k1gFnSc4	oftalmologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemocnici	nemocnice	k1gFnSc4	nemocnice
sv.	sv.	kA	sv.
Ludvíka	Ludvík	k1gMnSc2	Ludvík
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1607	[number]	k4	1607
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ulevila	ulevit	k5eAaPmAgFnS	ulevit
špitálu	špitál	k1gInSc3	špitál
Hôtel-Dieu	Hôtel-Dieus	k1gInSc2	Hôtel-Dieus
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tato	tento	k3xDgFnSc1	tento
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
významné	významný	k2eAgFnPc1d1	významná
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
vojenské	vojenský	k2eAgFnPc1d1	vojenská
nemocnice	nemocnice	k1gFnPc1	nemocnice
Invalidovna	invalidovna	k1gFnSc1	invalidovna
a	a	k8xC	a
Val-de-Grâce	Vale-Grâce	k1gFnSc1	Val-de-Grâce
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
zařízení	zařízení	k1gNnPc2	zařízení
je	být	k5eAaImIp3nS	být
spravována	spravovat	k5eAaImNgFnS	spravovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
organizace	organizace	k1gFnSc2	organizace
Assistance	Assistance	k1gFnSc1	Assistance
publique	publique	k1gFnSc1	publique
-	-	kIx~	-
Hôpitaux	Hôpitaux	k1gInSc1	Hôpitaux
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
pomoc	pomoc	k1gFnSc1	pomoc
-	-	kIx~	-
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
90	[number]	k4	90
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
patří	patřit	k5eAaImIp3nS	patřit
mj.	mj.	kA	mj.
nemocnice	nemocnice	k1gFnSc1	nemocnice
Necker	Necker	k1gMnSc1	Necker
<g/>
,	,	kIx,	,
Cochin	Cochin	k1gMnSc1	Cochin
<g/>
,	,	kIx,	,
Salpê	Salpê	k1gMnSc1	Salpê
<g/>
,	,	kIx,	,
Saint-Antoine	Saint-Antoin	k1gMnSc5	Saint-Antoin
<g/>
,	,	kIx,	,
Bichat-Claude-Bernard	Bichat-Claude-Bernarda	k1gFnPc2	Bichat-Claude-Bernarda
nebo	nebo	k8xC	nebo
Evropská	evropský	k2eAgFnSc1d1	Evropská
nemocnice	nemocnice	k1gFnSc1	nemocnice
Georgese	Georgese	k1gFnSc1	Georgese
Pompidoua	Pompidoua	k1gFnSc1	Pompidoua
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
leží	ležet	k5eAaImIp3nP	ležet
významné	významný	k2eAgFnPc1d1	významná
nemocnice	nemocnice	k1gFnPc1	nemocnice
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Neuilly-sur-Seine	Neuillyur-Sein	k1gInSc5	Neuilly-sur-Sein
(	(	kIx(	(
<g/>
Americká	americký	k2eAgFnSc1d1	americká
nemocnice	nemocnice	k1gFnSc1	nemocnice
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Créteil	Créteil	k1gMnSc1	Créteil
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Kremlin-Bicê	Kremlin-Bicê	k1gMnSc1	Kremlin-Bicê
<g/>
,	,	kIx,	,
Montfermeil	Montfermeil	k1gMnSc1	Montfermeil
<g/>
,	,	kIx,	,
Clichy	Clicha	k1gFnPc1	Clicha
<g/>
,	,	kIx,	,
Argenteuil	Argenteuil	k1gInSc1	Argenteuil
nebo	nebo	k8xC	nebo
Versailles	Versailles	k1gFnSc1	Versailles
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejhustších	hustý	k2eAgFnPc2d3	nejhustší
sítí	síť	k1gFnPc2	síť
lékařů	lékař	k1gMnPc2	lékař
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
všeobecných	všeobecný	k2eAgInPc2d1	všeobecný
nebo	nebo	k8xC	nebo
specialistů	specialista	k1gMnPc2	specialista
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
připadlo	připadnout	k5eAaPmAgNnS	připadnout
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
226,9	[number]	k4	226,9
lékařů	lékař	k1gMnPc2	lékař
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
specialistů	specialista	k1gMnPc2	specialista
činili	činit	k5eAaImAgMnP	činit
průměr	průměr	k1gInSc4	průměr
dokonce	dokonce	k9	dokonce
501,3	[number]	k4	501,3
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnSc2d1	významná
lékařské	lékařský	k2eAgFnSc2d1	lékařská
fakulty	fakulta	k1gFnSc2	fakulta
mají	mít	k5eAaImIp3nP	mít
univerzity	univerzita	k1gFnPc1	univerzita
Paříž	Paříž	k1gFnSc1	Paříž
V	V	kA	V
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
VII	VII	kA	VII
a	a	k8xC	a
Paříž	Paříž	k1gFnSc1	Paříž
XI	XI	kA	XI
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
univerzitám	univerzita	k1gFnPc3	univerzita
slouží	sloužit	k5eAaImIp3nS	sloužit
Meziuniverzitní	Meziuniverzitní	k2eAgFnSc1d1	Meziuniverzitní
knihovna	knihovna	k1gFnSc1	knihovna
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
soudního	soudní	k2eAgNnSc2d1	soudní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
řídí	řídit	k5eAaImIp3nS	řídit
Policejní	policejní	k2eAgFnSc1d1	policejní
prefektura	prefektura	k1gFnSc1	prefektura
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
Quai	quai	k1gNnSc2	quai
de	de	k?	de
la	la	k1gNnSc2	la
Rapée	Rapée	k1gFnPc2	Rapée
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
dějin	dějiny	k1gFnPc2	dějiny
lékařství	lékařství	k1gNnSc2	lékařství
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Univerzity	univerzita	k1gFnSc2	univerzita
Paříž	Paříž	k1gFnSc1	Paříž
V	V	kA	V
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
vojenského	vojenský	k2eAgNnSc2d1	vojenské
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
nemocnici	nemocnice	k1gFnSc6	nemocnice
Val-de-Grâce	Vale-Grâec	k1gInSc2	Val-de-Grâec
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
muzeum	muzeum	k1gNnSc1	muzeum
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
nemocnice	nemocnice	k1gFnPc1	nemocnice
sv.	sv.	kA	sv.
Ludvíka	Ludvík	k1gMnSc4	Ludvík
a	a	k8xC	a
organizace	organizace	k1gFnPc4	organizace
Assistance	Assistanec	k1gMnSc2	Assistanec
publique	publiqu	k1gMnSc2	publiqu
-	-	kIx~	-
Hôpitaux	Hôpitaux	k1gInSc1	Hôpitaux
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
sídlící	sídlící	k2eAgMnSc1d1	sídlící
v	v	k7c4	v
Hôtel	Hôtel	k1gInSc4	Hôtel
de	de	k?	de
Miramion	Miramion	k1gInSc1	Miramion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
sídlí	sídlet	k5eAaImIp3nS	sídlet
tradičně	tradičně	k6eAd1	tradičně
množství	množství	k1gNnSc1	množství
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejprestižnějším	prestižní	k2eAgFnPc3d3	nejprestižnější
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
decentralizaci	decentralizace	k1gFnSc3	decentralizace
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Štrasburku	Štrasburk	k1gInSc2	Štrasburk
přeložena	přeložen	k2eAgMnSc4d1	přeložen
École	Écol	k1gMnSc4	Écol
nationale	nationale	k6eAd1	nationale
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
administration	administration	k1gInSc1	administration
a	a	k8xC	a
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
École	École	k1gFnSc1	École
normale	normal	k1gMnSc5	normal
supérieure	supérieur	k1gMnSc5	supérieur
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
Paříž	Paříž	k1gFnSc1	Paříž
hlavním	hlavní	k2eAgMnSc7d1	hlavní
vysokoškolským	vysokoškolský	k2eAgMnSc7d1	vysokoškolský
centrem	centr	k1gMnSc7	centr
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
univerzit	univerzita	k1gFnPc2	univerzita
podléhá	podléhat	k5eAaImIp3nS	podléhat
Pařížské	pařížský	k2eAgFnSc3d1	Pařížská
akademii	akademie	k1gFnSc3	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c6	na
státních	státní	k2eAgFnPc6d1	státní
školách	škola	k1gFnPc6	škola
263	[number]	k4	263
812	[number]	k4	812
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
135	[number]	k4	135
570	[number]	k4	570
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
stupni	stupeň	k1gInSc6	stupeň
a	a	k8xC	a
128	[number]	k4	128
242	[number]	k4	242
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
<g/>
)	)	kIx)	)
a	a	k8xC	a
138	[number]	k4	138
527	[number]	k4	527
na	na	k7c6	na
soukromých	soukromý	k2eAgFnPc6d1	soukromá
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vysokoškoláků	vysokoškolák	k1gMnPc2	vysokoškolák
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
přibližně	přibližně	k6eAd1	přibližně
585	[number]	k4	585
000	[number]	k4	000
studentů	student	k1gMnPc2	student
v	v	k7c4	v
Île-de-France	Îlee-France	k1gFnPc4	Île-de-France
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
celkem	celkem	k6eAd1	celkem
881	[number]	k4	881
veřejných	veřejný	k2eAgNnPc2d1	veřejné
vzdělávacích	vzdělávací	k2eAgNnPc2d1	vzdělávací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
bylo	být	k5eAaImAgNnS	být
323	[number]	k4	323
mateřských	mateřský	k2eAgFnPc2d1	mateřská
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
334	[number]	k4	334
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
6	[number]	k4	6
speciálních	speciální	k2eAgFnPc2d1	speciální
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
školy	škola	k1gFnPc1	škola
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
110	[number]	k4	110
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
72	[number]	k4	72
všeobecných	všeobecný	k2eAgNnPc2d1	všeobecné
a	a	k8xC	a
technických	technický	k2eAgNnPc2d1	technické
lyceí	lyceum	k1gNnPc2	lyceum
<g/>
,	,	kIx,	,
34	[number]	k4	34
odborných	odborný	k2eAgNnPc2d1	odborné
lyceí	lyceum	k1gNnPc2	lyceum
a	a	k8xC	a
2	[number]	k4	2
experimentální	experimentální	k2eAgNnPc4d1	experimentální
lycea	lyceum	k1gNnPc4	lyceum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
dalších	další	k2eAgFnPc2d1	další
256	[number]	k4	256
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
<g/>
:	:	kIx,	:
110	[number]	k4	110
mateřských	mateřský	k2eAgFnPc2d1	mateřská
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
speciální	speciální	k2eAgFnSc1d1	speciální
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
67	[number]	k4	67
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
73	[number]	k4	73
všeobecných	všeobecný	k2eAgNnPc2d1	všeobecné
a	a	k8xC	a
technických	technický	k2eAgNnPc2d1	technické
lyceí	lyceum	k1gNnPc2	lyceum
a	a	k8xC	a
5	[number]	k4	5
odborných	odborný	k2eAgNnPc2d1	odborné
lyceí	lyceum	k1gNnPc2	lyceum
<g/>
.	.	kIx.	.
</s>
<s>
Lycea	lyceum	k1gNnPc1	lyceum
Louis-le-Grand	Louise-Grando	k1gNnPc2	Louis-le-Grando
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc4	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
mají	mít	k5eAaImIp3nP	mít
celostátní	celostátní	k2eAgFnSc4d1	celostátní
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
působnost	působnost	k1gFnSc4	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Paříž	Paříž	k1gFnSc1	Paříž
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
center	centrum	k1gNnPc2	centrum
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
v	v	k7c4	v
teologii	teologie	k1gFnSc4	teologie
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
teologická	teologický	k2eAgFnSc1d1	teologická
škola	škola	k1gFnSc1	škola
při	při	k7c6	při
katedrále	katedrála	k1gFnSc6	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
získala	získat	k5eAaPmAgFnS	získat
od	od	k7c2	od
Filipa	Filip	k1gMnSc2	Filip
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Augusta	Augusta	k1gMnSc1	Augusta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1200	[number]	k4	1200
privilegium	privilegium	k1gNnSc4	privilegium
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
pod	pod	k7c7	pod
jurisdikcí	jurisdikce	k1gFnSc7	jurisdikce
práva	právo	k1gNnSc2	právo
církevního	církevní	k2eAgNnSc2d1	církevní
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
světského	světský	k2eAgNnSc2d1	světské
<g/>
.	.	kIx.	.
</s>
<s>
Univerzitu	univerzita	k1gFnSc4	univerzita
uznal	uznat	k5eAaPmAgMnS	uznat
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
bule	bula	k1gFnSc6	bula
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1215	[number]	k4	1215
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Řehoř	Řehoř	k1gMnSc1	Řehoř
IX	IX	kA	IX
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1231	[number]	k4	1231
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
univerzita	univerzita	k1gFnSc1	univerzita
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
fakulty	fakulta	k1gFnPc4	fakulta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výuka	výuka	k1gFnSc1	výuka
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
kolejích	kolej	k1gFnPc6	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1257	[number]	k4	1257
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
kolem	kolem	k7c2	kolem
Montagne	montagne	k1gFnSc2	montagne
Sainte-Geneviè	Sainte-Geneviè	k1gFnSc2	Sainte-Geneviè
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Latinská	latinský	k2eAgFnSc1d1	Latinská
čtvrť	čtvrť	k1gFnSc1	čtvrť
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
významným	významný	k2eAgInSc7d1	významný
školským	školský	k2eAgInSc7d1	školský
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1530	[number]	k4	1530
zde	zde	k6eAd1	zde
František	František	k1gMnSc1	František
I.	I.	kA	I.
založil	založit	k5eAaPmAgMnS	založit
Collè	Collè	k1gMnSc1	Collè
de	de	k?	de
France	Franc	k1gMnSc4	Franc
jako	jako	k8xC	jako
konkurenci	konkurence	k1gFnSc3	konkurence
univerzitě	univerzita	k1gFnSc3	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikají	vznikat	k5eAaImIp3nP	vznikat
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
(	(	kIx(	(
<g/>
grandes	grandes	k1gMnSc1	grandes
écoles	écoles	k1gMnSc1	écoles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
rovněž	rovněž	k6eAd1	rovněž
našly	najít	k5eAaPmAgFnP	najít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c4	v
Latinské	latinský	k2eAgFnPc4d1	Latinská
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
École	École	k1gFnSc1	École
Polytechnique	Polytechnique	k1gFnSc1	Polytechnique
nebo	nebo	k8xC	nebo
École	École	k1gFnSc1	École
normale	normale	k6eAd1	normale
supérieure	supérieur	k1gMnSc5	supérieur
založené	založený	k2eAgInPc1d1	založený
během	během	k7c2	během
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
naopak	naopak	k6eAd1	naopak
zrušena	zrušen	k2eAgFnSc1d1	zrušena
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
obnovena	obnoven	k2eAgFnSc1d1	obnovena
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
fakult	fakulta	k1gFnPc2	fakulta
<g/>
:	:	kIx,	:
právnické	právnický	k2eAgFnSc2d1	právnická
<g/>
,	,	kIx,	,
lékařské	lékařský	k2eAgFnSc2d1	lékařská
<g/>
,	,	kIx,	,
farmaceutické	farmaceutický	k2eAgFnSc2d1	farmaceutická
<g/>
,	,	kIx,	,
filozofické	filozofický	k2eAgFnSc2d1	filozofická
<g/>
,	,	kIx,	,
teologické	teologický	k2eAgFnSc2d1	teologická
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
počet	počet	k1gInSc4	počet
studentů	student	k1gMnPc2	student
výrazně	výrazně	k6eAd1	výrazně
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studentské	studentský	k2eAgFnSc6d1	studentská
revoltě	revolta	k1gFnSc6	revolta
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
epicentrem	epicentrum	k1gNnSc7	epicentrum
byla	být	k5eAaImAgFnS	být
Sorbonna	Sorbonna	k1gFnSc1	Sorbonna
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
univerzita	univerzita	k1gFnSc1	univerzita
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
třinácti	třináct	k4xCc2	třináct
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
institucí	instituce	k1gFnPc2	instituce
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
I	i	k9	i
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
II	II	kA	II
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
III	III	kA	III
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
V	V	kA	V
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
VII	VII	kA	VII
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
VIII	VIII	kA	VIII
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
IX	IX	kA	IX
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
X	X	kA	X
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
XI	XI	kA	XI
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
XII	XII	kA	XII
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
XIII	XIII	kA	XIII
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
určité	určitý	k2eAgInPc4d1	určitý
obory	obor	k1gInPc4	obor
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
centrem	centrum	k1gNnSc7	centrum
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
života	život	k1gInSc2	život
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
Latinská	latinský	k2eAgFnSc1d1	Latinská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nP	ležet
Univerzity	univerzita	k1gFnPc1	univerzita
Paříž	Paříž	k1gFnSc4	Paříž
I	i	k9	i
až	až	k9	až
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Collè	Collè	k1gFnSc1	Collè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
Katolický	katolický	k2eAgInSc1d1	katolický
institut	institut	k1gInSc1	institut
<g/>
,	,	kIx,	,
École	École	k1gFnSc1	École
nationale	nationale	k6eAd1	nationale
des	des	k1gNnSc1	des
chartes	chartes	k1gInSc1	chartes
<g/>
,	,	kIx,	,
Sciences	Sciences	k1gInSc1	Sciences
Po	po	k7c6	po
aj.	aj.	kA	aj.
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
Kampus	kampus	k1gInSc4	kampus
Jussieu	Jussieus	k1gInSc2	Jussieus
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
centrem	centrum	k1gNnSc7	centrum
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
stává	stávat	k5eAaImIp3nS	stávat
13	[number]	k4	13
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Paris	Paris	k1gMnSc1	Paris
Rive	Riv	k1gMnSc2	Riv
Gauche	Gauche	k1gNnSc2	Gauche
vzniká	vznikat	k5eAaImIp3nS	vznikat
Kampus	kampus	k1gInSc4	kampus
Paris	Paris	k1gMnSc1	Paris
Rive	Riv	k1gInSc2	Riv
Gauche	Gauch	k1gFnSc2	Gauch
pro	pro	k7c4	pro
Univerzitu	univerzita	k1gFnSc4	univerzita
Paříž	Paříž	k1gFnSc4	Paříž
VII	VII	kA	VII
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
nové	nový	k2eAgNnSc4d1	nové
sídlo	sídlo	k1gNnSc4	sídlo
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
Paříž	Paříž	k1gFnSc1	Paříž
provozuje	provozovat	k5eAaImIp3nS	provozovat
sedm	sedm	k4xCc4	sedm
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
jsou	být	k5eAaImIp3nP	být
věnovány	věnovat	k5eAaImNgInP	věnovat
užitému	užitý	k2eAgNnSc3d1	užité
umění	umění	k1gNnSc3	umění
(	(	kIx(	(
<g/>
École	Écol	k1gMnSc5	Écol
Boulle	Boull	k1gMnSc5	Boull
<g/>
,	,	kIx,	,
École	Écol	k1gMnSc5	Écol
Estienne	Estienn	k1gMnSc5	Estienn
<g/>
,	,	kIx,	,
École	Écol	k1gMnSc5	Écol
supérieure	supérieur	k1gMnSc5	supérieur
des	des	k1gNnPc3	des
arts	arts	k6eAd1	arts
appliqués	appliqués	k6eAd1	appliqués
Duperré	Duperrý	k2eAgNnSc1d1	Duperrý
<g/>
,	,	kIx,	,
École	Écol	k1gMnSc5	Écol
professionnelle	professionnell	k1gMnSc5	professionnell
supérieure	supérieur	k1gMnSc5	supérieur
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
arts	artsit	k5eAaPmRp2nS	artsit
graphiques	graphiques	k1gInSc1	graphiques
et	et	k?	et
d	d	k?	d
<g/>
</s>
<s>
'	'	kIx"	'
<g/>
architecture	architectur	k1gMnSc5	architectur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
technického	technický	k2eAgNnSc2d1	technické
zaměření	zaměření	k1gNnSc2	zaměření
(	(	kIx(	(
<g/>
École	École	k1gInSc1	École
des	des	k1gNnSc1	des
ingénieurs	ingénieurs	k1gInSc1	ingénieurs
de	de	k?	de
la	la	k1gNnSc7	la
ville	ville	k1gFnSc2	ville
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
a	a	k8xC	a
École	École	k1gFnSc1	École
supérieure	supérieur	k1gMnSc5	supérieur
de	de	k?	de
physique	physique	k1gNnSc6	physique
et	et	k?	et
de	de	k?	de
chimie	chimie	k1gFnSc1	chimie
industrielle	industrielle	k1gFnSc1	industrielle
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
na	na	k7c4	na
zahradnictví	zahradnictví	k1gNnSc4	zahradnictví
(	(	kIx(	(
<g/>
École	École	k1gInSc1	École
du	du	k?	du
Breuil	Breuil	k1gInSc1	Breuil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
akademie	akademie	k1gFnSc1	akademie
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
nejprestižnější	prestižní	k2eAgFnSc7d3	nejprestižnější
institucí	instituce	k1gFnSc7	instituce
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
duševního	duševní	k2eAgInSc2d1	duševní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1634	[number]	k4	1634
kardinál	kardinál	k1gMnSc1	kardinál
Richelieu	Richelieu	k1gMnSc1	Richelieu
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
francouzského	francouzský	k2eAgInSc2d1	francouzský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Collè	Collè	k1gFnSc6	Collè
des	des	k1gNnSc2	des
Quatre-Nations	Quatre-Nations	k1gInSc4	Quatre-Nations
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
naproti	naproti	k7c3	naproti
Louvru	Louvre	k1gInSc3	Louvre
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Francouzského	francouzský	k2eAgInSc2d1	francouzský
institutu	institut	k1gInSc2	institut
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1666	[number]	k4	1666
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
Colbertem	Colbert	k1gInSc7	Colbert
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
sídlem	sídlo	k1gNnSc7	sídlo
mnoha	mnoho	k4c2	mnoho
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
ústavů	ústav	k1gInPc2	ústav
a	a	k8xC	a
laboratoří	laboratoř	k1gFnPc2	laboratoř
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
již	již	k6eAd1	již
francouzských	francouzský	k2eAgInPc2d1	francouzský
nebo	nebo	k8xC	nebo
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnější	významný	k2eAgFnSc3d3	nejvýznamnější
patří	patřit	k5eAaImIp3nS	patřit
Národní	národní	k2eAgNnSc1d1	národní
výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
Institut	institut	k1gInSc1	institut
Henri	Henr	k1gFnSc2	Henr
Poincaré	Poincarý	k2eAgInPc1d1	Poincarý
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgInSc1d1	centrální
francouzský	francouzský	k2eAgInSc1d1	francouzský
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pasteurův	Pasteurův	k2eAgInSc1d1	Pasteurův
ústav	ústav	k1gInSc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
Francouzský	francouzský	k2eAgInSc1d1	francouzský
národní	národní	k2eAgInSc1d1	národní
archiv	archiv	k1gInSc1	archiv
<g/>
,	,	kIx,	,
Institut	institut	k1gInSc1	institut
français	français	k1gFnSc2	français
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
architecture	architectur	k1gMnSc5	architectur
<g/>
,	,	kIx,	,
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
observatoř	observatoř	k1gFnSc1	observatoř
<g/>
,	,	kIx,	,
IRCAM	IRCAM	kA	IRCAM
nebo	nebo	k8xC	nebo
INSEE	INSEE	kA	INSEE
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
institucí	instituce	k1gFnPc2	instituce
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
své	své	k1gNnSc4	své
ředitelství	ředitelství	k1gNnSc2	ředitelství
např.	např.	kA	např.
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Prvořadou	prvořadý	k2eAgFnSc7d1	prvořadá
institucí	instituce	k1gFnSc7	instituce
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
základy	základ	k1gInPc1	základ
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
založil	založit	k5eAaPmAgMnS	založit
královskou	královský	k2eAgFnSc4d1	královská
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
917	[number]	k4	917
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
I.	I.	kA	I.
ji	on	k3xPp3gFnSc4	on
přeložil	přeložit	k5eAaPmAgMnS	přeložit
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
Fontainebleau	Fontainebleaus	k1gInSc2	Fontainebleaus
<g/>
.	.	kIx.	.
</s>
<s>
Radikální	radikální	k2eAgFnSc4d1	radikální
změnu	změna	k1gFnSc4	změna
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
francouzské	francouzský	k2eAgFnPc4d1	francouzská
knihovny	knihovna	k1gFnPc4	knihovna
Velká	velký	k2eAgFnSc1d1	velká
francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
celé	celý	k2eAgFnPc1d1	celá
klášterní	klášterní	k2eAgFnPc1d1	klášterní
knihovny	knihovna	k1gFnPc1	knihovna
a	a	k8xC	a
knižní	knižní	k2eAgFnPc1d1	knižní
sbírky	sbírka	k1gFnPc1	sbírka
sekularizovány	sekularizován	k2eAgFnPc1d1	sekularizována
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
znárodňovány	znárodňován	k2eAgFnPc4d1	znárodňován
šlechtické	šlechtický	k2eAgFnPc4d1	šlechtická
knihovny	knihovna	k1gFnPc4	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
knihy	kniha	k1gFnPc4	kniha
poté	poté	k6eAd1	poté
obohatily	obohatit	k5eAaPmAgInP	obohatit
fondy	fond	k1gInPc1	fond
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
budově	budova	k1gFnSc6	budova
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
původní	původní	k2eAgNnSc4d1	původní
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Rue	Rue	k1gFnSc6	Rue
de	de	k?	de
Richelieu	richelieu	k1gNnSc1	richelieu
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
uložení	uložení	k1gNnSc3	uložení
neknihovních	knihovní	k2eNgFnPc2d1	knihovní
sbírek	sbírka	k1gFnPc2	sbírka
(	(	kIx(	(
<g/>
mince	mince	k1gFnPc1	mince
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
plakáty	plakát	k1gInPc1	plakát
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Knihovna	knihovna	k1gFnSc1	knihovna
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
další	další	k2eAgNnSc1d1	další
oddělení	oddělení	k1gNnSc1	oddělení
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Arsenal	Arsenal	k1gMnSc3	Arsenal
nebo	nebo	k8xC	nebo
Bibliothè	Bibliothè	k1gMnSc3	Bibliothè
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Opéra	Opéra	k1gMnSc1	Opéra
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
jsou	být	k5eAaImIp3nP	být
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
fondy	fond	k1gInPc1	fond
erotické	erotický	k2eAgFnSc2d1	erotická
literatury	literatura	k1gFnSc2	literatura
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Enfer	Enfer	k1gInSc4	Enfer
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
knihovnou	knihovna	k1gFnSc7	knihovna
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
Mazarine	Mazarin	k1gInSc5	Mazarin
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
Quai	quai	k1gNnSc2	quai
de	de	k?	de
Conti	Conti	k1gNnSc2	Conti
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
osobní	osobní	k2eAgFnSc2d1	osobní
knihovny	knihovna	k1gFnSc2	knihovna
kardinála	kardinál	k1gMnSc2	kardinál
Mazarina	Mazarin	k1gMnSc2	Mazarin
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
je	být	k5eAaImIp3nS	být
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
Francouzskému	francouzský	k2eAgInSc3d1	francouzský
institutu	institut	k1gInSc3	institut
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
knihovnou	knihovna	k1gFnSc7	knihovna
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
publique	publiqu	k1gInSc2	publiqu
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
information	information	k1gInSc1	information
přičleněná	přičleněný	k2eAgFnSc1d1	přičleněná
k	k	k7c3	k
Centre	centr	k1gInSc5	centr
Georges	Georges	k1gMnSc1	Georges
Pompidou	Pompida	k1gFnSc7	Pompida
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
tvoří	tvořit	k5eAaImIp3nS	tvořit
síť	síť	k1gFnSc4	síť
69	[number]	k4	69
veřejných	veřejný	k2eAgFnPc2d1	veřejná
knihoven	knihovna	k1gFnPc2	knihovna
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
městských	městský	k2eAgInPc6d1	městský
obvodech	obvod	k1gInPc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
specializované	specializovaný	k2eAgInPc1d1	specializovaný
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
jsou	být	k5eAaImIp3nP	být
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
historique	historique	k1gFnSc1	historique
de	de	k?	de
la	la	k1gNnPc2	la
ville	ville	k1gNnPc2	ville
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
Historická	historický	k2eAgFnSc1d1	historická
knihovna	knihovna	k1gFnSc1	knihovna
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bibliothè	Bibliothè	k1gMnSc1	Bibliothè
administrative	administrativ	k1gInSc5	administrativ
de	de	k?	de
la	la	k1gNnPc2	la
ville	ville	k1gNnPc2	ville
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
Správní	správní	k2eAgFnSc1d1	správní
knihovna	knihovna	k1gFnSc1	knihovna
města	město	k1gNnSc2	město
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bibliothè	Bibliothè	k1gMnSc1	Bibliothè
des	des	k1gNnSc2	des
littératures	littératures	k1gMnSc1	littératures
policiè	policiè	k?	policiè
(	(	kIx(	(
<g/>
Knihovna	knihovna	k1gFnSc1	knihovna
detektivní	detektivní	k2eAgFnSc2d1	detektivní
literatury	literatura	k1gFnSc2	literatura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Médiathè	Médiathè	k1gMnSc1	Médiathè
<g />
.	.	kIx.	.
</s>
<s>
musicale	musical	k1gInSc5	musical
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
(	(	kIx(	(
<g/>
Hudební	hudební	k2eAgFnSc1d1	hudební
mediotéka	mediotéka	k1gFnSc1	mediotéka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
knihovny	knihovna	k1gFnPc1	knihovna
se	se	k3xPyFc4	se
specializují	specializovat	k5eAaBmIp3nP	specializovat
na	na	k7c4	na
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
a	a	k8xC	a
užité	užitý	k2eAgNnSc4d1	užité
umění	umění	k1gNnSc4	umění
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
Forney	Fornea	k1gFnSc2	Fornea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
feminismus	feminismus	k1gInSc1	feminismus
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
Marguerite	Marguerit	k1gInSc5	Marguerit
Durand	Durando	k1gNnPc2	Durando
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Heure	Heur	k1gMnSc5	Heur
Joyeuse	Joyeus	k1gMnSc5	Joyeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kinematografii	kinematografie	k1gFnSc4	kinematografie
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
du	du	k?	du
cinéma	cinéma	k1gNnSc1	cinéma
François-Truffaut	François-Truffaut	k1gMnSc1	François-Truffaut
<g/>
,	,	kIx,	,
Cinémathè	Cinémathè	k1gMnSc1	Cinémathè
Robert-Lynen	Robert-Lynen	k1gInSc1	Robert-Lynen
<g/>
,	,	kIx,	,
Forum	forum	k1gNnSc1	forum
des	des	k1gNnSc2	des
images	imagesa	k1gFnPc2	imagesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
turismus	turismus	k1gInSc1	turismus
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
du	du	k?	du
tourisme	tourismus	k1gInSc5	tourismus
et	et	k?	et
des	des	k1gNnPc2	des
voyages	voyages	k1gInSc1	voyages
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
množství	množství	k1gNnSc1	množství
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
knihoven	knihovna	k1gFnPc2	knihovna
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
univerzitám	univerzita	k1gFnPc3	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
je	být	k5eAaImIp3nS	být
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
Sainte-Geneviè	Sainte-Geneviè	k1gFnSc1	Sainte-Geneviè
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
univerzity	univerzita	k1gFnPc1	univerzita
Paříž	Paříž	k1gFnSc4	Paříž
I	i	k9	i
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
II	II	kA	II
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
III	III	kA	III
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
IV	Iva	k1gFnPc2	Iva
a	a	k8xC	a
Paříž	Paříž	k1gFnSc1	Paříž
VII	VII	kA	VII
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
zaměření	zaměření	k1gNnSc4	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
i	i	k9	i
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
de	de	k?	de
la	la	k1gNnSc1	la
Sorbonne	Sorbonn	k1gInSc5	Sorbonn
(	(	kIx(	(
<g/>
univerzity	univerzita	k1gFnPc1	univerzita
Paříž	Paříž	k1gFnSc4	Paříž
I	i	k9	i
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
III	III	kA	III
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
V	V	kA	V
a	a	k8xC	a
Paříž	Paříž	k1gFnSc1	Paříž
VII	VII	kA	VII
<g/>
)	)	kIx)	)
nemá	mít	k5eNaImIp3nS	mít
úzkou	úzký	k2eAgFnSc4d1	úzká
profilaci	profilace	k1gFnSc4	profilace
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
knihovny	knihovna	k1gFnPc1	knihovna
jsou	být	k5eAaImIp3nP	být
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
na	na	k7c4	na
dané	daný	k2eAgFnPc4d1	daná
obory	obora	k1gFnPc4	obora
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lékařství	lékařství	k1gNnSc1	lékařství
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gMnSc5	Bibliothè
interuniversitaire	interuniversitair	k1gMnSc5	interuniversitair
santé	santý	k2eAgInPc4d1	santý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnSc2d1	přírodní
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gMnSc4	Bibliothè
Jussieu	Jussiea	k1gMnSc4	Jussiea
<g/>
,	,	kIx,	,
Bibliothè	Bibliothè	k1gMnSc1	Bibliothè
Pierre-et-Marie-Curie	Pierret-Marie-Curie	k1gFnSc2	Pierre-et-Marie-Curie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
ekonomii	ekonomie	k1gFnSc4	ekonomie
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gMnSc1	Bibliothè
Cujas	Cujas	k1gMnSc1	Cujas
<g/>
,	,	kIx,	,
Bibliothè	Bibliothè	k1gMnSc5	Bibliothè
Sainte-Barbe	Sainte-Barb	k1gMnSc5	Sainte-Barb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společenské	společenský	k2eAgFnSc2d1	společenská
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gMnSc1	Bibliothè
de	de	k?	de
sciences	sciences	k1gMnSc1	sciences
humaines	humaines	k1gMnSc1	humaines
et	et	k?	et
sociales	sociales	k1gMnSc1	sociales
Paris	Paris	k1gMnSc1	Paris
Descartes-CNRS	Descartes-CNRS	k1gMnSc1	Descartes-CNRS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lingvistiku	lingvistika	k1gFnSc4	lingvistika
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gMnSc5	Bibliothè
interuniversitaire	interuniversitair	k1gMnSc5	interuniversitair
des	des	k1gNnSc2	des
langues	langues	k1gMnSc1	langues
orientales	orientales	k1gMnSc1	orientales
<g/>
,	,	kIx,	,
Bibliothè	Bibliothè	k1gMnSc5	Bibliothè
universitaire	universitair	k1gMnSc5	universitair
des	des	k1gNnSc7	des
langues	langues	k1gInSc1	langues
et	et	k?	et
civilisations	civilisations	k1gInSc1	civilisations
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
literaturu	literatura	k1gFnSc4	literatura
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gMnSc5	Bibliothè
littéraire	littérair	k1gMnSc5	littérair
Jacques	Jacques	k1gMnSc1	Jacques
Doucet	Doucet	k1gFnSc2	Doucet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
knihovny	knihovna	k1gFnPc1	knihovna
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
veřejné	veřejný	k2eAgFnPc1d1	veřejná
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
ať	ať	k9	ať
už	už	k6eAd1	už
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Assemblée	Assemblée	k1gInSc1	Assemblée
nationale	nationale	k6eAd1	nationale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francouzský	francouzský	k2eAgInSc1d1	francouzský
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gMnSc1	Bibliothè
centrale	centrale	k6eAd1	centrale
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Institut	institut	k1gInSc1	institut
national	nationat	k5eAaPmAgMnS	nationat
de	de	k?	de
la	la	k1gNnPc6	la
statistique	statistiqu	k1gInSc2	statistiqu
et	et	k?	et
des	des	k1gNnSc1	des
études	études	k1gMnSc1	études
économiques	économiques	k1gMnSc1	économiques
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
nebo	nebo	k8xC	nebo
Francouzský	francouzský	k2eAgInSc1d1	francouzský
institut	institut	k1gInSc1	institut
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
de	de	k?	de
la	la	k1gNnSc7	la
Cité	Citá	k1gFnSc2	Citá
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
architecture	architectur	k1gMnSc5	architectur
et	et	k?	et
du	du	k?	du
patrimoine	patrimoinout	k5eAaPmIp3nS	patrimoinout
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Ke	k	k7c3	k
specializovaným	specializovaný	k2eAgFnPc3d1	specializovaná
knihovnám	knihovna	k1gFnPc3	knihovna
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
du	du	k?	du
Film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
kinematografie	kinematografie	k1gFnSc1	kinematografie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Médiathè	Médiathè	k1gMnSc1	Médiathè
Musicale	musical	k1gInSc5	musical
Mahler	Mahler	k1gMnSc1	Mahler
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polská	polský	k2eAgFnSc1d1	polská
knihovna	knihovna	k1gFnSc1	knihovna
(	(	kIx(	(
<g/>
polské	polský	k2eAgFnPc1d1	polská
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
knihovna	knihovna	k1gFnSc1	knihovna
Saulchoir	Saulchoira	k1gFnPc2	Saulchoira
(	(	kIx(	(
<g/>
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tisk	tisk	k1gInSc1	tisk
Regionální	regionální	k2eAgInSc1d1	regionální
deník	deník	k1gInSc1	deník
Le	Le	k1gFnSc2	Le
Parisien	Parisina	k1gFnPc2	Parisina
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
deseti	deset	k4xCc6	deset
departementálních	departementálních	k?	departementálních
mutacích	mutace	k1gFnPc6	mutace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Vycházejí	vycházet	k5eAaImIp3nP	vycházet
zde	zde	k6eAd1	zde
čtyři	čtyři	k4xCgInPc1	čtyři
bezplatné	bezplatný	k2eAgInPc1d1	bezplatný
deníky	deník	k1gInPc1	deník
-	-	kIx~	-
tři	tři	k4xCgNnPc4	tři
vycházejí	vycházet	k5eAaImIp3nP	vycházet
ráno	ráno	k1gNnSc1	ráno
(	(	kIx(	(
<g/>
20	[number]	k4	20
minutes	minutesa	k1gFnPc2	minutesa
<g/>
,	,	kIx,	,
Direct	Direct	k2eAgInSc1d1	Direct
Matin	Matin	k1gInSc1	Matin
a	a	k8xC	a
Métro	Métro	k1gNnSc1	Métro
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
je	být	k5eAaImIp3nS	být
večerník	večerník	k1gInSc1	večerník
(	(	kIx(	(
<g/>
Direct	Direct	k2eAgInSc1d1	Direct
Soir	Soir	k1gInSc1	Soir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
distribuovány	distribuovat	k5eAaBmNgInP	distribuovat
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
o	o	k7c6	o
kulturním	kulturní	k2eAgInSc6d1	kulturní
programu	program	k1gInSc6	program
v	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
přinášejí	přinášet	k5eAaImIp3nP	přinášet
týdeníky	týdeník	k1gInPc7	týdeník
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Officiel	Officiel	k1gInSc1	Officiel
des	des	k1gNnSc1	des
spectacles	spectaclesa	k1gFnPc2	spectaclesa
a	a	k8xC	a
Pariscope	Pariscop	k1gMnSc5	Pariscop
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
Kromě	kromě	k7c2	kromě
celostátního	celostátní	k2eAgInSc2d1	celostátní
kanálu	kanál	k1gInSc2	kanál
France	Franc	k1gMnSc4	Franc
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
regionální	regionální	k2eAgInSc4d1	regionální
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
několik	několik	k4yIc1	několik
místních	místní	k2eAgFnPc2d1	místní
televizních	televizní	k2eAgFnPc2d1	televizní
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Télif	Télif	k1gInSc1	Télif
(	(	kIx(	(
<g/>
Télévision	Télévision	k1gInSc1	Télévision
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
televizní	televizní	k2eAgInSc4d1	televizní
kanál	kanál	k1gInSc4	kanál
šířený	šířený	k2eAgInSc4d1	šířený
kabelem	kabel	k1gInSc7	kabel
<g/>
,	,	kIx,	,
satelitem	satelit	k1gMnSc7	satelit
a	a	k8xC	a
ADSL	ADSL	kA	ADSL
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
několik	několik	k4yIc4	několik
místních	místní	k2eAgFnPc2d1	místní
televizních	televizní	k2eAgFnPc2d1	televizní
společností	společnost	k1gFnPc2	společnost
<g/>
:	:	kIx,	:
VOTV	VOTV	kA	VOTV
(	(	kIx(	(
<g/>
Val-d	Val	k1gMnSc1	Val-d
<g/>
'	'	kIx"	'
<g/>
Oise	Oise	k1gInSc1	Oise
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Télessonne	Télessonn	k1gMnSc5	Télessonn
(	(	kIx(	(
<g/>
Essonne	Essonn	k1gMnSc5	Essonn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TVM	TVM	kA	TVM
Est	Est	k1gMnSc1	Est
parisien	parisien	k2eAgMnSc1d1	parisien
(	(	kIx(	(
<g/>
Seine-Saint-Denis	Seine-Saint-Denis	k1gInSc1	Seine-Saint-Denis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TVFil	TVFil	k1gInSc1	TVFil
<g/>
78	[number]	k4	78
(	(	kIx(	(
<g/>
Yvelines	Yvelines	k1gInSc1	Yvelines
<g/>
)	)	kIx)	)
a	a	k8xC	a
RTV	RTV	kA	RTV
(	(	kIx(	(
<g/>
Rosny-sous-Bois	Rosnyous-Bois	k1gInSc1	Rosny-sous-Bois
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgFnPc2d1	další
sedm	sedm	k4xCc1	sedm
televizí	televize	k1gFnPc2	televize
sdílí	sdílet	k5eAaImIp3nS	sdílet
jeden	jeden	k4xCgInSc1	jeden
kanál	kanál	k1gInSc1	kanál
<g/>
:	:	kIx,	:
NRJ	NRJ	kA	NRJ
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
IDF	IDF	kA	IDF
1	[number]	k4	1
<g/>
,	,	kIx,	,
Cap	cap	k1gMnSc1	cap
24	[number]	k4	24
<g/>
,	,	kIx,	,
Demain	Demain	k1gInSc1	Demain
IDF	IDF	kA	IDF
<g/>
,	,	kIx,	,
BDM	BDM	kA	BDM
TV	TV	kA	TV
<g/>
,	,	kIx,	,
Cinaps	Cinaps	k1gInSc1	Cinaps
TV	TV	kA	TV
a	a	k8xC	a
Télé	Télé	k1gNnSc7	Télé
Bocal	Bocal	k1gInSc1	Bocal
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
vysílaly	vysílat	k5eAaImAgFnP	vysílat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
též	též	k6eAd1	též
dvě	dva	k4xCgFnPc4	dva
televize	televize	k1gFnPc4	televize
nezákonně	zákonně	k6eNd1	zákonně
<g/>
.	.	kIx.	.
</s>
<s>
Zaléa	Zaléa	k1gFnSc1	Zaléa
TV	TV	kA	TV
a	a	k8xC	a
Teleplaisance	Teleplaisance	k1gFnSc1	Teleplaisance
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
pouze	pouze	k6eAd1	pouze
amatéry	amatér	k1gMnPc4	amatér
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
kanály	kanál	k1gInPc1	kanál
vysílají	vysílat	k5eAaImIp3nP	vysílat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlas	rozhlas	k1gInSc1	rozhlas
První	první	k4xOgFnSc1	první
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc1	stanice
Radio	radio	k1gNnSc1	radio
Tour	Toura	k1gFnPc2	Toura
Eiffel	Eifflo	k1gNnPc2	Eifflo
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vysílat	vysílat	k5eAaImF	vysílat
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
umožněno	umožnit	k5eAaPmNgNnS	umožnit
i	i	k8xC	i
soukromé	soukromý	k2eAgNnSc1d1	soukromé
vysílání	vysílání	k1gNnSc1	vysílání
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
soukromá	soukromý	k2eAgFnSc1d1	soukromá
stanice	stanice	k1gFnSc1	stanice
Radiola	Radiola	k1gFnSc1	Radiola
získala	získat	k5eAaPmAgFnS	získat
licenci	licence	k1gFnSc4	licence
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
regionálních	regionální	k2eAgNnPc2d1	regionální
rádií	rádio	k1gNnPc2	rádio
různého	různý	k2eAgNnSc2d1	různé
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
Město	město	k1gNnSc1	město
Paříž	Paříž	k1gFnSc1	Paříž
provozuje	provozovat	k5eAaImIp3nS	provozovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
internetové	internetový	k2eAgFnPc4d1	internetová
stránky	stránka	k1gFnPc4	stránka
www.paris.fr	www.paris.fra	k1gFnPc2	www.paris.fra
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
síť	síť	k1gFnSc4	síť
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
pro	pro	k7c4	pro
bezplatné	bezplatný	k2eAgNnSc4d1	bezplatné
internetové	internetový	k2eAgNnSc4d1	internetové
připojení	připojení	k1gNnSc4	připojení
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
knihovnách	knihovna	k1gFnPc6	knihovna
<g/>
,	,	kIx,	,
muzeích	muzeum	k1gNnPc6	muzeum
a	a	k8xC	a
radnicích	radnice	k1gFnPc6	radnice
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gNnSc2	jejich
otevření	otevření	k1gNnSc2	otevření
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
připojení	připojení	k1gNnSc1	připojení
trvá	trvat	k5eAaImIp3nS	trvat
maximálně	maximálně	k6eAd1	maximálně
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
elektronickým	elektronický	k2eAgInSc7d1	elektronický
kanálem	kanál	k1gInSc7	kanál
jsou	být	k5eAaImIp3nP	být
světelné	světelný	k2eAgInPc1d1	světelný
informační	informační	k2eAgInPc1d1	informační
panely	panel	k1gInPc1	panel
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přinášejí	přinášet	k5eAaImIp3nP	přinášet
nejen	nejen	k6eAd1	nejen
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
aktuální	aktuální	k2eAgFnSc6d1	aktuální
dopravní	dopravní	k2eAgFnSc6d1	dopravní
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
o	o	k7c6	o
kulturních	kulturní	k2eAgFnPc6d1	kulturní
akcích	akce	k1gFnPc6	akce
nebo	nebo	k8xC	nebo
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
umožnil	umožnit	k5eAaPmAgMnS	umožnit
starosta	starosta	k1gMnSc1	starosta
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
Bertrand	Bertrand	k1gInSc4	Bertrand
Delanoë	Delanoë	k1gFnSc7	Delanoë
využití	využití	k1gNnSc2	využití
těchto	tento	k3xDgFnPc2	tento
tabulí	tabule	k1gFnPc2	tabule
ke	k	k7c3	k
zveřejnění	zveřejnění	k1gNnSc3	zveřejnění
asi	asi	k9	asi
3600	[number]	k4	3600
milostných	milostný	k2eAgInPc2d1	milostný
vzkazů	vzkaz	k1gInPc2	vzkaz
ke	k	k7c3	k
Dni	den	k1gInSc3	den
svatého	svatý	k2eAgMnSc2d1	svatý
Valentýna	Valentýn	k1gMnSc2	Valentýn
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
lidé	člověk	k1gMnPc1	člověk
poslali	poslat	k5eAaPmAgMnP	poslat
na	na	k7c4	na
internetovou	internetový	k2eAgFnSc4d1	internetová
adresu	adresa	k1gFnSc4	adresa
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kultura	kultura	k1gFnSc1	kultura
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
kulturním	kulturní	k2eAgNnSc7d1	kulturní
městem	město	k1gNnSc7	město
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
i	i	k8xC	i
turisty	turist	k1gMnPc4	turist
nabízí	nabízet	k5eAaImIp3nS	nabízet
nepřeberné	přeberný	k2eNgNnSc1d1	nepřeberné
množství	množství	k1gNnSc1	množství
kulturních	kulturní	k2eAgFnPc2d1	kulturní
institucí	instituce	k1gFnPc2	instituce
-	-	kIx~	-
přes	přes	k7c4	přes
150	[number]	k4	150
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
200	[number]	k4	200
galerií	galerie	k1gFnPc2	galerie
<g/>
,	,	kIx,	,
asi	asi	k9	asi
100	[number]	k4	100
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
650	[number]	k4	650
kin	kino	k1gNnPc2	kino
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Nabídka	nabídka	k1gFnSc1	nabídka
kulturních	kulturní	k2eAgFnPc2d1	kulturní
událostí	událost	k1gFnPc2	událost
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
četná	četný	k2eAgNnPc4d1	četné
divadelní	divadelní	k2eAgNnPc4d1	divadelní
a	a	k8xC	a
operní	operní	k2eAgNnPc4d1	operní
představení	představení	k1gNnPc4	představení
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc4	výstava
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgInPc4d1	hudební
a	a	k8xC	a
filmové	filmový	k2eAgInPc4d1	filmový
festivaly	festival	k1gInPc4	festival
<g/>
,	,	kIx,	,
módní	módní	k2eAgFnPc4d1	módní
přehlídky	přehlídka	k1gFnPc4	přehlídka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sportovní	sportovní	k2eAgFnPc4d1	sportovní
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
hmotným	hmotný	k2eAgInSc7d1	hmotný
dokladem	doklad	k1gInSc7	doklad
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
je	být	k5eAaImIp3nS	být
Arè	Arè	k1gMnSc1	Arè
de	de	k?	de
Lutè	Lutè	k1gMnSc1	Lutè
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Rue	Rue	k1gFnSc2	Rue
Monge	Mong	k1gFnSc2	Mong
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k8xC	jako
římský	římský	k2eAgInSc1d1	římský
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
i	i	k8xC	i
aréna	aréna	k1gFnSc1	aréna
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
využívala	využívat	k5eAaImAgFnS	využívat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kapacita	kapacita	k1gFnSc1	kapacita
byla	být	k5eAaImAgFnS	být
zhruba	zhruba	k6eAd1	zhruba
17	[number]	k4	17
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
centralistické	centralistický	k2eAgFnSc3d1	centralistická
tradici	tradice	k1gFnSc3	tradice
sídlí	sídlet	k5eAaImIp3nS	sídlet
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
divadelní	divadelní	k2eAgInPc4d1	divadelní
<g/>
,	,	kIx,	,
operní	operní	k2eAgInPc4d1	operní
a	a	k8xC	a
baletní	baletní	k2eAgInPc4d1	baletní
soubory	soubor	k1gInPc4	soubor
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
národní	národní	k2eAgFnSc1d1	národní
opera	opera	k1gFnSc1	opera
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1669	[number]	k4	1669
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
opery	opera	k1gFnSc2	opera
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dva	dva	k4xCgInPc4	dva
operní	operní	k2eAgInPc4d1	operní
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Opéra	Opéra	k1gFnSc1	Opéra
Garnier	Garnira	k1gFnPc2	Garnira
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1875	[number]	k4	1875
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc2	svůj
architekta	architekt	k1gMnSc2	architekt
Charlese	Charles	k1gMnSc2	Charles
Garniera	Garnier	k1gMnSc2	Garnier
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
pro	pro	k7c4	pro
představení	představení	k1gNnSc4	představení
baletu	balet	k1gInSc2	balet
a	a	k8xC	a
klasických	klasický	k2eAgFnPc2d1	klasická
oper	opera	k1gFnPc2	opera
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozlohou	rozloha	k1gFnSc7	rozloha
největším	veliký	k2eAgNnSc7d3	veliký
divadlem	divadlo	k1gNnSc7	divadlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
tradice	tradice	k1gFnSc1	tradice
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
na	na	k7c6	na
konci	konec	k1gInSc6	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
škola	škola	k1gFnSc1	škola
při	při	k7c6	při
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
koncertních	koncertní	k2eAgInPc2d1	koncertní
sálů	sál	k1gInPc2	sál
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
je	být	k5eAaImIp3nS	být
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
sály	sál	k1gInPc1	sál
Bataclan	Bataclana	k1gFnPc2	Bataclana
<g/>
,	,	kIx,	,
Folies	Folies	k1gMnSc1	Folies
Bergè	Bergè	k1gMnSc1	Bergè
nebo	nebo	k8xC	nebo
Zénith	Zénith	k1gMnSc1	Zénith
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
je	být	k5eAaImIp3nS	být
Philharmonie	philharmonie	k1gFnSc1	philharmonie
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Cité	Cité	k1gNnSc2	Cité
de	de	k?	de
la	la	k1gNnSc2	la
musique	musique	k1gFnPc2	musique
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
často	často	k6eAd1	často
konají	konat	k5eAaImIp3nP	konat
v	v	k7c4	v
AccorHotels	AccorHotels	k1gInSc4	AccorHotels
Arena	Aren	k1gInSc2	Aren
<g/>
.	.	kIx.	.
</s>
<s>
Mazi	Maz	k1gFnPc1	Maz
významné	významný	k2eAgMnPc4d1	významný
autory	autor	k1gMnPc4	autor
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Frédéric	Frédéric	k1gMnSc1	Frédéric
Chopin	Chopin	k1gMnSc1	Chopin
Hector	Hector	k1gMnSc1	Hector
Berlioz	Berlioz	k1gMnSc1	Berlioz
a	a	k8xC	a
Johann	Johann	k1gMnSc1	Johann
Strauss	Straussa	k1gFnPc2	Straussa
st.	st.	kA	st.
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
např.	např.	kA	např.
Édith	Édith	k1gInSc1	Édith
Piaf	piaf	k1gInSc1	piaf
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
filmové	filmový	k2eAgFnPc1d1	filmová
projekce	projekce	k1gFnPc1	projekce
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1895	[number]	k4	1895
bratři	bratr	k1gMnPc1	bratr
Lumiè	Lumiè	k1gFnPc2	Lumiè
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
v	v	k7c4	v
Salon	salon	k1gInSc4	salon
indien	indina	k1gFnPc2	indina
du	du	k?	du
Grand	grand	k1gMnSc1	grand
Café	café	k1gNnSc2	café
první	první	k4xOgFnSc2	první
filmové	filmový	k2eAgFnSc2d1	filmová
promítání	promítání	k1gNnSc4	promítání
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgInP	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
celkem	celkem	k6eAd1	celkem
376	[number]	k4	376
sálů	sál	k1gInPc2	sál
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
150	[number]	k4	150
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
a	a	k8xC	a
89	[number]	k4	89
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
<g/>
.	.	kIx.	.
</s>
<s>
Poskytují	poskytovat	k5eAaImIp3nP	poskytovat
velkou	velký	k2eAgFnSc4d1	velká
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
přibližně	přibližně	k6eAd1	přibližně
450	[number]	k4	450
až	až	k9	až
500	[number]	k4	500
různých	různý	k2eAgInPc2d1	různý
filmů	film	k1gInPc2	film
a	a	k8xC	a
roční	roční	k2eAgFnSc1d1	roční
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
27	[number]	k4	27
miliónů	milión	k4xCgInPc2	milión
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
regionem	region	k1gInSc7	region
Île-de-France	Îlee-France	k1gFnSc2	Île-de-France
disponuje	disponovat	k5eAaBmIp3nS	disponovat
největší	veliký	k2eAgFnSc7d3	veliký
muzejní	muzejní	k2eAgFnSc7d1	muzejní
nabídkou	nabídka	k1gFnSc7	nabídka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
jak	jak	k8xC	jak
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
do	do	k7c2	do
rozsahu	rozsah	k1gInSc2	rozsah
a	a	k8xC	a
různorodosti	různorodost	k1gFnSc2	různorodost
jejich	jejich	k3xOp3gFnPc2	jejich
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
největším	veliký	k2eAgNnSc7d3	veliký
co	co	k8xS	co
do	do	k7c2	do
rozlohy	rozloha	k1gFnSc2	rozloha
a	a	k8xC	a
sbírek	sbírka	k1gFnPc2	sbírka
je	být	k5eAaImIp3nS	být
Louvre	Louvre	k1gInSc1	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
návštěvností	návštěvnost	k1gFnSc7	návštěvnost
8,3	[number]	k4	8,3
miliónu	milión	k4xCgInSc2	milión
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
Louvre	Louvre	k1gInSc4	Louvre
zdaleka	zdaleka	k6eAd1	zdaleka
nejnavštěvovanějším	navštěvovaný	k2eAgNnSc7d3	nejnavštěvovanější
muzeem	muzeum	k1gNnSc7	muzeum
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
exponáty	exponát	k1gInPc1	exponát
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
období	období	k1gNnSc4	období
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jiná	jiný	k2eAgNnPc1d1	jiné
muzea	muzeum	k1gNnPc1	muzeum
mají	mít	k5eAaImIp3nP	mít
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
reputaci	reputace	k1gFnSc4	reputace
jako	jako	k8xS	jako
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
Musée	Musée	k1gNnSc2	Musée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orsay	Orsaa	k1gMnSc2	Orsaa
představující	představující	k2eAgNnSc1d1	představující
umění	umění	k1gNnSc1	umění
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
tři	tři	k4xCgNnPc1	tři
muzea	muzeum	k1gNnPc1	muzeum
mají	mít	k5eAaImIp3nP	mít
statut	statut	k1gInSc4	statut
národních	národní	k2eAgFnPc2d1	národní
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zřizována	zřizován	k2eAgNnPc4d1	zřizováno
francouzským	francouzský	k2eAgInSc7d1	francouzský
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
např.	např.	kA	např.
Musée	Musée	k1gInSc1	Musée
national	nationat	k5eAaImAgInS	nationat
du	du	k?	du
Moyen	Moyen	k1gInSc1	Moyen
Âge	Âge	k1gFnSc1	Âge
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
středověku	středověk	k1gInSc2	středověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Musée	Musée	k1gInSc1	Musée
du	du	k?	du
quai	quai	k1gNnSc7	quai
Branly	Branla	k1gFnSc2	Branla
(	(	kIx(	(
<g/>
mimoevropské	mimoevropský	k2eAgNnSc1d1	mimoevropské
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Další	další	k2eAgNnPc1d1	další
jsou	být	k5eAaImIp3nP	být
řízena	řízen	k2eAgNnPc1d1	řízeno
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
ministerstvy	ministerstvo	k1gNnPc7	ministerstvo
jako	jako	k8xS	jako
např.	např.	kA	např.
Musée	Musée	k1gInSc1	Musée
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Armée	Armé	k1gMnSc2	Armé
(	(	kIx(	(
<g/>
v	v	k7c6	v
Invalidovně	invalidovna	k1gFnSc6	invalidovna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
provozuje	provozovat	k5eAaImIp3nS	provozovat
několik	několik	k4yIc4	několik
vlastních	vlastní	k2eAgNnPc2d1	vlastní
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
Musée	Musée	k1gInSc1	Musée
Carnavalet	Carnavalet	k1gInSc1	Carnavalet
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
dějinám	dějiny	k1gFnPc3	dějiny
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pařížské	pařížský	k2eAgFnPc1d1	Pařížská
katakomby	katakomby	k1gFnPc1	katakomby
<g/>
,	,	kIx,	,
Petit	petit	k1gInSc1	petit
Palais	Palais	k1gFnSc1	Palais
<g/>
,	,	kIx,	,
Palais	Palais	k1gFnSc1	Palais
de	de	k?	de
Tokyo	Tokyo	k1gNnSc1	Tokyo
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgNnSc1d1	moderní
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
Cernuschi	Cernusch	k1gFnSc2	Cernusch
(	(	kIx(	(
<g/>
asijské	asijský	k2eAgNnSc1d1	asijské
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnSc1	Musée
des	des	k1gNnSc2	des
arts	arts	k6eAd1	arts
décoratifs	décoratifs	k6eAd1	décoratifs
(	(	kIx(	(
<g/>
dekorativní	dekorativní	k2eAgNnSc1d1	dekorativní
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
restaurace	restaurace	k1gFnSc1	restaurace
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
smyslu	smysl	k1gInSc6	smysl
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
restaurace	restaurace	k1gFnSc2	restaurace
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1765	[number]	k4	1765
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeden	jeden	k4xCgMnSc1	jeden
provozovatel	provozovatel	k1gMnSc1	provozovatel
vývařovny	vývařovna	k1gFnSc2	vývařovna
polévek	polévka	k1gFnPc2	polévka
získal	získat	k5eAaPmAgMnS	získat
povolení	povolení	k1gNnSc4	povolení
prodávat	prodávat	k5eAaImF	prodávat
též	též	k9	též
jehněčí	jehněčí	k2eAgFnSc1d1	jehněčí
s	s	k7c7	s
omáčkou	omáčka	k1gFnSc7	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
restaurací	restaurace	k1gFnPc2	restaurace
nazývaly	nazývat	k5eAaImAgInP	nazývat
podniky	podnik	k1gInPc1	podnik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nabízely	nabízet	k5eAaImAgInP	nabízet
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
jídel	jídlo	k1gNnPc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Velkou	velký	k2eAgFnSc7d1	velká
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
revolucí	revoluce	k1gFnSc7	revoluce
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
ani	ani	k8xC	ani
100	[number]	k4	100
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgNnP	být
omezení	omezení	k1gNnPc1	omezení
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
cechovních	cechovní	k2eAgNnPc2d1	cechovní
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
prodávat	prodávat	k5eAaImF	prodávat
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
jídel	jídlo	k1gNnPc2	jídlo
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
podniku	podnik	k1gInSc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgNnP	být
omezení	omezení	k1gNnPc1	omezení
zrušena	zrušit	k5eAaPmNgNnP	zrušit
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
restaurací	restaurace	k1gFnPc2	restaurace
narůstal	narůstat	k5eAaImAgInS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Pařížské	pařížský	k2eAgFnPc4d1	Pařížská
restaurace	restaurace	k1gFnPc4	restaurace
provozovali	provozovat	k5eAaImAgMnP	provozovat
většinou	většinou	k6eAd1	většinou
kuchaři	kuchař	k1gMnPc1	kuchař
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
původně	původně	k6eAd1	původně
pracovali	pracovat	k5eAaImAgMnP	pracovat
ve	v	k7c6	v
šlechtických	šlechtický	k2eAgFnPc6d1	šlechtická
rodinách	rodina	k1gFnPc6	rodina
a	a	k8xC	a
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
emigraci	emigrace	k1gFnSc6	emigrace
začali	začít	k5eAaPmAgMnP	začít
podnikat	podnikat	k5eAaImF	podnikat
v	v	k7c6	v
pohostinství	pohostinství	k1gNnSc6	pohostinství
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgMnSc7	ten
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
mezi	mezi	k7c4	mezi
měšťanské	měšťanský	k2eAgFnPc4d1	měšťanská
vrstvy	vrstva	k1gFnPc4	vrstva
styl	styl	k1gInSc4	styl
vaření	vaření	k1gNnSc1	vaření
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neznali	znát	k5eNaImAgMnP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
kavárny	kavárna	k1gFnPc1	kavárna
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
obliba	obliba	k1gFnSc1	obliba
rostla	růst	k5eAaImAgFnS	růst
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgNnP	být
otevřena	otevřít	k5eAaPmNgNnP	otevřít
Café	café	k1gNnPc1	café
de	de	k?	de
la	la	k1gNnPc6	la
Régence	Régenec	k1gInSc2	Régenec
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
kavárnou	kavárna	k1gFnSc7	kavárna
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Café	café	k1gNnSc2	café
Procope	Procop	k1gInSc5	Procop
zprovozněná	zprovozněný	k2eAgNnPc1d1	zprovozněné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1686	[number]	k4	1686
<g/>
.	.	kIx.	.
</s>
<s>
Kavárny	kavárna	k1gFnPc1	kavárna
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
i	i	k9	i
na	na	k7c4	na
chodníky	chodník	k1gInPc4	chodník
a	a	k8xC	a
bulváry	bulvár	k1gInPc4	bulvár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Paříž	Paříž	k1gFnSc1	Paříž
napojila	napojit	k5eAaPmAgFnS	napojit
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
a	a	k8xC	a
během	během	k7c2	během
následné	následný	k2eAgFnSc2d1	následná
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
začali	začít	k5eAaPmAgMnP	začít
stěhovat	stěhovat	k5eAaImF	stěhovat
obyvatelé	obyvatel	k1gMnPc1	obyvatel
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinášeli	přinášet	k5eAaImAgMnP	přinášet
gastronomické	gastronomický	k2eAgFnSc3d1	gastronomická
rozmanitosti	rozmanitost	k1gFnSc3	rozmanitost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tak	tak	k9	tak
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
rybí	rybí	k2eAgFnPc4d1	rybí
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnPc4	restaurace
s	s	k7c7	s
alsaskou	alsaský	k2eAgFnSc7d1	alsaská
nebo	nebo	k8xC	nebo
provensálskou	provensálský	k2eAgFnSc7d1	provensálská
kuchyní	kuchyně	k1gFnSc7	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
příchod	příchod	k1gInSc1	příchod
imigrantů	imigrant	k1gMnPc2	imigrant
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
nebo	nebo	k8xC	nebo
z	z	k7c2	z
Maghrebu	Maghreb	k1gInSc2	Maghreb
či	či	k8xC	či
Asie	Asie	k1gFnSc2	Asie
přinesl	přinést	k5eAaPmAgInS	přinést
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc4d2	veliký
kulinářskou	kulinářský	k2eAgFnSc4d1	kulinářská
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dnes	dnes	k6eAd1	dnes
Paříž	Paříž	k1gFnSc1	Paříž
nabízí	nabízet	k5eAaImIp3nS	nabízet
kuchyně	kuchyně	k1gFnSc1	kuchyně
pěti	pět	k4xCc2	pět
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejproslulejším	proslulý	k2eAgInPc3d3	nejproslulejší
podnikům	podnik	k1gInPc3	podnik
patří	patřit	k5eAaImIp3nS	patřit
restaurace	restaurace	k1gFnPc4	restaurace
Maxim	maximum	k1gNnPc2	maximum
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
kavárny	kavárna	k1gFnSc2	kavárna
Café	café	k1gNnSc2	café
de	de	k?	de
Flore	Flor	k1gMnSc5	Flor
<g/>
,	,	kIx,	,
La	la	k1gNnPc7	la
Coupole	Coupole	k1gFnSc2	Coupole
<g/>
,	,	kIx,	,
La	la	k0	la
Rotonde	Rotond	k1gMnSc5	Rotond
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Deux	Deux	k1gInSc1	Deux
Magots	Magotsa	k1gFnPc2	Magotsa
nebo	nebo	k8xC	nebo
Café	café	k1gNnSc2	café
de	de	k?	de
la	la	k1gNnSc2	la
Paix	Paix	k1gInSc1	Paix
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sport	sport	k1gInSc1	sport
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Sport	sport	k1gInSc1	sport
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
tradičně	tradičně	k6eAd1	tradičně
k	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
událostem	událost	k1gFnPc3	událost
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hrála	hrát	k5eAaImAgFnS	hrát
Paříž	Paříž	k1gFnSc1	Paříž
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
šíření	šíření	k1gNnSc6	šíření
a	a	k8xC	a
organizaci	organizace	k1gFnSc6	organizace
moderního	moderní	k2eAgInSc2d1	moderní
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
udělen	udělen	k2eAgInSc1d1	udělen
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
jeu	jeu	k?	jeu
de	de	k?	de
paume	paumat	k5eAaPmIp3nS	paumat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
použit	použit	k2eAgInSc1d1	použit
metrický	metrický	k2eAgInSc1d1	metrický
systém	systém	k1gInSc1	systém
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
zde	zde	k6eAd1	zde
první	první	k4xOgNnSc4	první
soutěžní	soutěžní	k2eAgNnSc4d1	soutěžní
parkurové	parkurový	k2eAgNnSc4d1	parkurové
skákání	skákání	k1gNnSc4	skákání
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
cyklistický	cyklistický	k2eAgInSc1d1	cyklistický
závod	závod	k1gInSc1	závod
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
turnaj	turnaj	k1gInSc4	turnaj
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
šermu	šerm	k1gInSc6	šerm
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
mohly	moct	k5eAaImAgFnP	moct
poprvé	poprvé	k6eAd1	poprvé
soutěžit	soutěžit	k5eAaImF	soutěžit
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
obliba	obliba	k1gFnSc1	obliba
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
šampionát	šampionát	k1gInSc1	šampionát
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
založena	založen	k2eAgNnPc4d1	založeno
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
později	pozdě	k6eAd2	pozdě
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Fotbal	fotbal	k1gInSc1	fotbal
se	se	k3xPyFc4	se
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
měřítku	měřítko	k1gNnSc6	měřítko
prosadil	prosadit	k5eAaPmAgInS	prosadit
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
množství	množství	k1gNnSc1	množství
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
Red	Red	k1gMnPc2	Red
Star	Star	kA	Star
<g/>
,	,	kIx,	,
Racing	Racing	k1gInSc1	Racing
<g/>
,	,	kIx,	,
Olympique	Olympique	k1gInSc1	Olympique
<g/>
,	,	kIx,	,
CASG	CASG	kA	CASG
<g/>
,	,	kIx,	,
Stade	Stad	k1gInSc5	Stad
français	français	k1gFnSc3	français
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Také	také	k9	také
první	první	k4xOgNnSc4	první
mistrovství	mistrovství	k1gNnSc4	mistrovství
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
a	a	k8xC	a
odehrávalo	odehrávat	k5eAaImAgNnS	odehrávat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
mistrovství	mistrovství	k1gNnSc1	mistrovství
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
a	a	k8xC	a
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
pařížském	pařížský	k2eAgNnSc6d1	pařížské
předměstí	předměstí	k1gNnSc6	předměstí
Saint-Denis	Saint-Denis	k1gFnPc2	Saint-Denis
postaven	postavit	k5eAaPmNgMnS	postavit
Stade	Stad	k1gInSc5	Stad
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
sportovním	sportovní	k2eAgFnPc3d1	sportovní
organizacím	organizace	k1gFnPc3	organizace
patří	patřit	k5eAaImIp3nS	patřit
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Paris	Paris	k1gMnSc1	Paris
Saint-Germain	Saint-Germain	k1gMnSc1	Saint-Germain
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
federací	federace	k1gFnPc2	federace
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
mnohé	mnohé	k1gNnSc1	mnohé
přesídlily	přesídlit	k5eAaPmAgInP	přesídlit
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
sídlit	sídlit	k5eAaImF	sídlit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
přednesl	přednést	k5eAaPmAgMnS	přednést
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1892	[number]	k4	1892
baron	baron	k1gMnSc1	baron
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Coubertin	Coubertin	k1gInSc4	Coubertin
výzvu	výzev	k1gInSc2	výzev
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1894	[number]	k4	1894
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc6	příležitost
prvního	první	k4xOgInSc2	první
olympijského	olympijský	k2eAgInSc2d1	olympijský
kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
hned	hned	k6eAd1	hned
druhé	druhý	k4xOgFnPc4	druhý
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
osmé	osmý	k4xOgNnSc1	osmý
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
však	však	k9	však
Paříž	Paříž	k1gFnSc1	Paříž
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Londýnem	Londýn	k1gInSc7	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavním	hlavní	k2eAgNnPc3d1	hlavní
pařížským	pařížský	k2eAgNnPc3d1	pařížské
sportovištím	sportoviště	k1gNnPc3	sportoviště
patří	patřit	k5eAaImIp3nS	patřit
Parc	Parc	k1gInSc1	Parc
des	des	k1gNnSc2	des
Princes	princesa	k1gFnPc2	princesa
<g/>
,	,	kIx,	,
Stade	Stad	k1gInSc5	Stad
Roland-Garros	Roland-Garrosa	k1gFnPc2	Roland-Garrosa
<g/>
,	,	kIx,	,
Stade	Stad	k1gInSc5	Stad
Sébastien-Charléty	Sébastien-Charlét	k1gInPc1	Sébastien-Charlét
<g/>
,	,	kIx,	,
Stade	Stad	k1gInSc5	Stad
Jean-Bouin	Jean-Bouina	k1gFnPc2	Jean-Bouina
nebo	nebo	k8xC	nebo
Stade	Stad	k1gInSc5	Stad
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Coubertin	Coubertina	k1gFnPc2	Coubertina
<g/>
.	.	kIx.	.
</s>
<s>
Palais	Palais	k1gInSc1	Palais
omnisports	omnisports	k1gInSc1	omnisports
de	de	k?	de
Paris-Bercy	Paris-Berca	k1gFnSc2	Paris-Berca
a	a	k8xC	a
Palais	Palais	k1gFnSc2	Palais
des	des	k1gNnSc2	des
Sports	Sportsa	k1gFnPc2	Sportsa
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
slouží	sloužit	k5eAaImIp3nS	sloužit
vedle	vedle	k7c2	vedle
sportovních	sportovní	k2eAgNnPc2d1	sportovní
utkání	utkání	k1gNnPc2	utkání
také	také	k9	také
ke	k	k7c3	k
kulturním	kulturní	k2eAgInPc3d1	kulturní
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
koncertům	koncert	k1gInPc3	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
35	[number]	k4	35
plaveckých	plavecký	k2eAgInPc2d1	plavecký
bazénů	bazén	k1gInPc2	bazén
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgMnPc2	dva
50	[number]	k4	50
<g/>
metrových	metrový	k2eAgFnPc2d1	metrová
<g/>
.	.	kIx.	.
</s>
