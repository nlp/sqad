<p>
<s>
Národní	národní	k2eAgMnPc1d1	národní
buditelé	buditel	k1gMnPc1	buditel
je	on	k3xPp3gNnSc4	on
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
osobnosti	osobnost	k1gFnPc4	osobnost
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
určitého	určitý	k2eAgInSc2d1	určitý
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
usilovaly	usilovat	k5eAaImAgFnP	usilovat
o	o	k7c6	o
probuzení	probuzení	k1gNnSc6	probuzení
národního	národní	k2eAgNnSc2d1	národní
sebevědomí	sebevědomí	k1gNnSc2	sebevědomí
v	v	k7c6	v
obdobích	období	k1gNnPc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
dotyčný	dotyčný	k2eAgInSc4d1	dotyčný
národ	národ	k1gInSc4	národ
jazykově	jazykově	k6eAd1	jazykově
<g/>
,	,	kIx,	,
kulturně	kulturně	k6eAd1	kulturně
nebo	nebo	k8xC	nebo
politicky	politicky	k6eAd1	politicky
utlačován	utlačován	k2eAgInSc1d1	utlačován
<g/>
,	,	kIx,	,
omezován	omezovat	k5eAaImNgInS	omezovat
nebo	nebo	k8xC	nebo
znevýhodňován	znevýhodňován	k2eAgInSc1d1	znevýhodňován
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
jazykovou	jazykový	k2eAgFnSc7d1	jazyková
či	či	k8xC	či
kulturní	kulturní	k2eAgFnSc7d1	kulturní
skupinou	skupina	k1gFnSc7	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čeští	český	k2eAgMnPc1d1	český
národní	národní	k2eAgMnPc1d1	národní
buditelé	buditel	k1gMnPc1	buditel
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
české	český	k2eAgMnPc4d1	český
národní	národní	k2eAgMnPc4d1	národní
buditele	buditel	k1gMnPc4	buditel
jsou	být	k5eAaImIp3nP	být
řazeni	řazen	k2eAgMnPc1d1	řazen
např.	např.	kA	např.
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
a	a	k8xC	a
desítky	desítka	k1gFnPc1	desítka
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnPc2	osobnost
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Národní	národní	k2eAgMnPc1d1	národní
buditelé	buditel	k1gMnPc1	buditel
jiných	jiný	k2eAgInPc2d1	jiný
národů	národ	k1gInPc2	národ
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
národní	národní	k2eAgMnPc4d1	národní
buditele	buditel	k1gMnPc4	buditel
považována	považován	k2eAgFnSc1d1	považována
skupina	skupina	k1gFnSc1	skupina
osobností	osobnost	k1gFnPc2	osobnost
soustředěná	soustředěný	k2eAgFnSc1d1	soustředěná
kolem	kolem	k7c2	kolem
Ludovíta	Ludovít	k1gInSc2	Ludovít
Štúra	Štúr	k1gInSc2	Štúr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litevským	litevský	k2eAgMnSc7d1	litevský
předním	přední	k2eAgMnSc7d1	přední
národním	národní	k2eAgMnSc7d1	národní
buditelem	buditel	k1gMnSc7	buditel
je	být	k5eAaImIp3nS	být
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jonas	Jonas	k1gMnSc1	Jonas
Basanavičius	Basanavičius	k1gMnSc1	Basanavičius
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
-	-	kIx~	-
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Za	za	k7c4	za
národní	národní	k2eAgMnPc4d1	národní
buditele	buditel	k1gMnPc4	buditel
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
osobnosti	osobnost	k1gFnPc1	osobnost
více	hodně	k6eAd2	hodně
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
žily	žít	k5eAaImAgFnP	žít
pod	pod	k7c7	pod
tureckou	turecký	k2eAgFnSc7d1	turecká
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
bulharské	bulharský	k2eAgMnPc4d1	bulharský
národní	národní	k2eAgMnPc4d1	národní
buditele	buditel	k1gMnPc4	buditel
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
básníci	básník	k1gMnPc1	básník
Ivan	Ivan	k1gMnSc1	Ivan
Vazov	Vazov	k1gInSc1	Vazov
a	a	k8xC	a
Christo	Christa	k1gMnSc5	Christa
Botev	Botva	k1gFnPc2	Botva
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
</s>
</p>
