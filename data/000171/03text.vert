<s>
Dušan	Dušan	k1gMnSc1	Dušan
Vitázek	Vitázka	k1gFnPc2	Vitázka
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1980	[number]	k4	1980
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
JAMU	jam	k1gInSc3	jam
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Hair	Hair	k1gInSc1	Hair
obdržel	obdržet	k5eAaPmAgInS	obdržet
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
Cenu	cena	k1gFnSc4	cena
Thálie	Thálie	k1gFnSc2	Thálie
<g/>
,	,	kIx,	,
totéž	týž	k3xTgNnSc1	týž
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Cenu	cena	k1gFnSc4	cena
Thálie	Thálie	k1gFnSc1	Thálie
2011	[number]	k4	2011
obdržel	obdržet	k5eAaPmAgInS	obdržet
za	za	k7c4	za
dvojroli	dvojrole	k1gFnSc4	dvojrole
Jekylla	Jekyllo	k1gNnSc2	Jekyllo
&	&	k?	&
Hydea	Hydea	k1gMnSc1	Hydea
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
představení	představení	k1gNnSc6	představení
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejoblíbenějšího	oblíbený	k2eAgMnSc4d3	nejoblíbenější
herce	herec	k1gMnSc4	herec
sezóny	sezóna	k1gFnSc2	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
je	být	k5eAaImIp3nS	být
herečka	herečka	k1gFnSc1	herečka
Pavla	Pavla	k1gFnSc1	Pavla
Vitázková	Vitázkový	k2eAgFnSc1d1	Vitázková
rozená	rozený	k2eAgFnSc1d1	rozená
Ptáčková	Ptáčková	k1gFnSc1	Ptáčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1978	[number]	k4	1978
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
Barboru	Barbora	k1gFnSc4	Barbora
Anastázii	Anastázie	k1gFnSc4	Anastázie
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rozálii	Rozálie	k1gFnSc4	Rozálie
Agnes	Agnesa	k1gFnPc2	Agnesa
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Henry	Henry	k1gMnSc1	Henry
Jekyll	Jekyll	k1gMnSc1	Jekyll
/	/	kIx~	/
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
Hyde	Hyd	k1gFnSc2	Hyd
-	-	kIx~	-
Jekyll	Jekyll	k1gMnSc1	Jekyll
&	&	k?	&
Hyde	Hyde	k1gInSc1	Hyde
Bert	Berta	k1gFnPc2	Berta
-	-	kIx~	-
Mary	Mary	k1gFnSc1	Mary
Poppins	Poppins	k1gInSc1	Poppins
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
<g/>
)	)	kIx)	)
Buckingham	Buckingham	k1gInSc1	Buckingham
-	-	kIx~	-
Tři	tři	k4xCgMnPc1	tři
mušketýři	mušketýr	k1gMnPc1	mušketýr
(	(	kIx(	(
<g/>
činohra	činohra	k1gFnSc1	činohra
<g/>
)	)	kIx)	)
Aralambi	Aralambi	k1gNnSc1	Aralambi
-	-	kIx~	-
Cikáni	cikán	k1gMnPc1	cikán
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
Bourák	Bourák	k?	Bourák
-	-	kIx~	-
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
Story	story	k1gFnSc1	story
Josef	Josef	k1gMnSc1	Josef
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
úžasný	úžasný	k2eAgInSc1d1	úžasný
pestrobarevný	pestrobarevný	k2eAgInSc1d1	pestrobarevný
plášť	plášť	k1gInSc1	plášť
Rejpal	rejpat	k5eAaImAgInS	rejpat
-	-	kIx~	-
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
Ježíš	ježit	k5eAaImIp2nS	ježit
<g />
.	.	kIx.	.
</s>
<s>
Nazaretský	nazaretský	k2eAgInSc1d1	nazaretský
-	-	kIx~	-
Jesus	Jesus	k1gInSc1	Jesus
Christ	Christ	k1gMnSc1	Christ
Superstar	superstar	k1gFnPc2	superstar
Marius	Marius	k1gMnSc1	Marius
-	-	kIx~	-
Les	les	k1gInSc1	les
Misérables	Misérables	k1gInSc1	Misérables
(	(	kIx(	(
<g/>
Bídníci	bídník	k1gMnPc1	bídník
<g/>
)	)	kIx)	)
Erland	Erland	k1gInSc1	Erland
-	-	kIx~	-
Balada	balada	k1gFnSc1	balada
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
(	(	kIx(	(
<g/>
Singoalla	Singoalla	k1gFnSc1	Singoalla
<g/>
)	)	kIx)	)
Che	che	k0	che
-	-	kIx~	-
Evita	Evita	k1gMnSc1	Evita
Michael	Michael	k1gMnSc1	Michael
-	-	kIx~	-
Čarodějky	čarodějka	k1gFnPc1	čarodějka
z	z	k7c2	z
Eastwicku	Eastwick	k1gInSc2	Eastwick
Audrey	Audrea	k1gFnSc2	Audrea
2	[number]	k4	2
-	-	kIx~	-
kytka	kytka	k1gFnSc1	kytka
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kvítek	kvítek	k1gInSc1	kvítek
z	z	k7c2	z
horrroru	horrror	k1gInSc2	horrror
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Mozart	Mozart	k1gMnSc1	Mozart
-	-	kIx~	-
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
!	!	kIx.	!
</s>
<s>
Mickey	Mickea	k1gFnPc4	Mickea
-	-	kIx~	-
Pokrevní	pokrevní	k2eAgMnPc1d1	pokrevní
bratři	bratr	k1gMnPc1	bratr
Ramón	Ramón	k1gMnSc1	Ramón
-	-	kIx~	-
Zorro	Zorro	k1gNnSc1	Zorro
Rambajz-tágo	Rambajzágo	k1gNnSc1	Rambajz-tágo
-	-	kIx~	-
Cats	Catsa	k1gFnPc2	Catsa
Nick	Nicko	k1gNnPc2	Nicko
Hurley	Hurle	k2eAgFnPc1d1	Hurle
-	-	kIx~	-
Flashdance	Flashdance	k1gFnPc1	Flashdance
(	(	kIx(	(
<g/>
muzikál	muzikál	k1gInSc1	muzikál
<g/>
)	)	kIx)	)
Jerry	Jerra	k1gFnSc2	Jerra
-	-	kIx~	-
Očistec	očistec	k1gInSc4	očistec
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dušan	Dušan	k1gMnSc1	Dušan
Vitázek	Vitázka	k1gFnPc2	Vitázka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Dušan	Dušan	k1gMnSc1	Dušan
Vitázek	Vitázka	k1gFnPc2	Vitázka
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Dušanem	Dušan	k1gMnSc7	Dušan
Vitázkem	Vitázek	k1gInSc7	Vitázek
na	na	k7c4	na
www.kafe.cz	www.kafe.cz	k1gInSc4	www.kafe.cz
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Dušana	Dušan	k1gMnSc2	Dušan
Vitázka	Vitázka	k1gFnSc1	Vitázka
</s>
