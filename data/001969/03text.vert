<s>
Moroni	Moron	k1gMnPc1	Moron
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
م	م	k?	م
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
souostrovního	souostrovní	k2eAgInSc2d1	souostrovní
státu	stát	k1gInSc2	stát
Komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
žilo	žít	k5eAaImAgNnS	žít
60	[number]	k4	60
200	[number]	k4	200
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
Grande	grand	k1gMnSc5	grand
Comore	Comor	k1gMnSc5	Comor
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
Komor	komora	k1gFnPc2	komora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
ze	z	k7c2	z
souostroví	souostroví	k1gNnSc2	souostroví
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
sopka	sopka	k1gFnSc1	sopka
Karthala	Karthal	k1gMnSc2	Karthal
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
20	[number]	k4	20
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Prince	princ	k1gMnSc2	princ
Said	Said	k1gMnSc1	Said
Ibrahim	Ibrahim	k1gMnSc1	Ibrahim
Airport	Airport	k1gInSc4	Airport
<g/>
.	.	kIx.	.
</s>
