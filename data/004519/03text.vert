<s>
Marvel	Marvelit	k5eAaPmRp2nS	Marvelit
Worldwide	Worldwid	k1gMnSc5	Worldwid
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnSc5	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgFnPc1d1	známá
také	také	k9	také
jako	jako	k9	jako
Marvel	Marvel	k1gMnSc1	Marvel
Comics	comics	k1gInSc1	comics
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc1d1	americké
komiksové	komiksový	k2eAgNnSc1d1	komiksové
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
spadající	spadající	k2eAgNnSc1d1	spadající
pod	pod	k7c4	pod
společnost	společnost	k1gFnSc4	společnost
Marvel	Marvel	k1gMnSc1	Marvel
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
kolos	kolos	k1gMnSc1	kolos
The	The	k1gMnSc1	The
Walt	Walt	k1gMnSc1	Walt
Disney	Disne	k2eAgFnPc4d1	Disne
Company	Compana	k1gFnPc4	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
jako	jako	k8xC	jako
Timely	Timel	k1gMnPc4	Timel
Comics	comics	k1gInSc4	comics
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Atlas	Atlas	k1gInSc4	Atlas
Comics	comics	k1gInSc1	comics
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
na	na	k7c4	na
Marvel	Marvel	k1gInSc4	Marvel
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostali	dostat	k5eAaPmAgMnP	dostat
prostor	prostor	k1gInSc4	prostor
autoři	autor	k1gMnPc1	autor
jako	jako	k8xC	jako
Stan	stan	k1gInSc1	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Buscema	Buscemum	k1gNnSc2	Buscemum
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
Kirby	Kirba	k1gFnSc2	Kirba
a	a	k8xC	a
Steve	Steve	k1gMnSc1	Steve
Ditko	Ditko	k1gNnSc4	Ditko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
doba	doba	k1gFnSc1	doba
vzniku	vznik	k1gInSc2	vznik
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
Marvel	Marvela	k1gFnPc2	Marvela
komiksů	komiks	k1gInPc2	komiks
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Foura	k1gFnPc2	Foura
<g/>
,	,	kIx,	,
Avengers	Avengersa	k1gFnPc2	Avengersa
<g/>
,	,	kIx,	,
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
<g/>
,	,	kIx,	,
X-Men	X-Mna	k1gFnPc2	X-Mna
<g/>
,	,	kIx,	,
Daredevil	Daredevil	k1gFnPc2	Daredevil
<g/>
,	,	kIx,	,
Hulk	Hulka	k1gFnPc2	Hulka
<g/>
,	,	kIx,	,
Thor	Thora	k1gFnPc2	Thora
<g/>
,	,	kIx,	,
Punisher	Punishra	k1gFnPc2	Punishra
nebo	nebo	k8xC	nebo
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1	vydavatel
Martin	Martin	k1gMnSc1	Martin
Goodman	Goodman	k1gMnSc1	Goodman
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
založil	založit	k5eAaPmAgMnS	založit
budoucí	budoucí	k2eAgMnSc1d1	budoucí
Marvel	Marvel	k1gMnSc1	Marvel
jako	jako	k8xS	jako
Timely	Timela	k1gFnPc1	Timela
Publications	Publicationsa	k1gFnPc2	Publicationsa
s	s	k7c7	s
imprintem	imprint	k1gInSc7	imprint
Timely	Timela	k1gFnSc2	Timela
Comics	comics	k1gInSc1	comics
pro	pro	k7c4	pro
vydávání	vydávání	k1gNnSc4	vydávání
komiksů	komiks	k1gInPc2	komiks
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
komiks	komiks	k1gInSc1	komiks
Marvel	Marvel	k1gInSc1	Marvel
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
1	[number]	k4	1
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
komiksu	komiks	k1gInSc6	komiks
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgFnP	objevit
postavy	postava	k1gFnPc1	postava
Human	Humana	k1gFnPc2	Humana
Torch	Torcha	k1gFnPc2	Torcha
a	a	k8xC	a
Namor	Namora	k1gFnPc2	Namora
the	the	k?	the
Sub-Mariner	Sub-Marinra	k1gFnPc2	Sub-Marinra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Carl	Carl	k1gMnSc1	Carl
Burgos	Burgos	k1gMnSc1	Burgos
a	a	k8xC	a
Bill	Bill	k1gMnSc1	Bill
Everett	Everett	k1gMnSc1	Everett
<g/>
.	.	kIx.	.
</s>
<s>
Komiks	komiks	k1gInSc1	komiks
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
měl	mít	k5eAaImAgInS	mít
náklad	náklad	k1gInSc1	náklad
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
900	[number]	k4	900
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
autoři	autor	k1gMnPc1	autor
Joe	Joe	k1gMnSc1	Joe
Simon	Simon	k1gMnSc1	Simon
a	a	k8xC	a
Jack	Jack	k1gMnSc1	Jack
Kirby	Kirba	k1gFnSc2	Kirba
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
vlasteneckého	vlastenecký	k2eAgMnSc4d1	vlastenecký
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byl	být	k5eAaImAgMnS	být
Captain	Captain	k1gMnSc1	Captain
America	America	k1gMnSc1	America
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
první	první	k4xOgInSc4	první
komiks	komiks	k1gInSc4	komiks
Captain	Captain	k1gMnSc1	Captain
America	Americ	k1gInSc2	Americ
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
1	[number]	k4	1
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
její	její	k3xOp3gInSc1	její
náklad	náklad	k1gInSc1	náklad
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
hrdiny	hrdina	k1gMnPc7	hrdina
byli	být	k5eAaImAgMnP	být
Whizzer	Whizzer	k1gMnSc1	Whizzer
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
America	America	k1gFnSc1	America
<g/>
,	,	kIx,	,
Destroyer	Destroyer	k1gInSc1	Destroyer
<g/>
,	,	kIx,	,
Vision	vision	k1gInSc1	vision
<g/>
,	,	kIx,	,
a	a	k8xC	a
Angel	angel	k1gMnSc1	angel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ti	ten	k3xDgMnPc1	ten
již	již	k6eAd1	již
takového	takový	k3xDgInSc2	takový
úspěchu	úspěch	k1gInSc2	úspěch
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
také	také	k9	také
Joe	Joe	k1gMnSc1	Joe
Simon	Simon	k1gMnSc1	Simon
opustil	opustit	k5eAaPmAgMnS	opustit
post	post	k1gInSc4	post
editora	editor	k1gMnSc2	editor
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
zabral	zabrat	k5eAaPmAgMnS	zabrat
bratranec	bratranec	k1gMnSc1	bratranec
Goodmanovy	Goodmanův	k2eAgFnSc2d1	Goodmanova
ženy	žena	k1gFnSc2	žena
Stanley	Stanlea	k1gFnSc2	Stanlea
Lieber	Liebra	k1gFnPc2	Liebra
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Stan	stan	k1gInSc1	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
éře	éra	k1gFnSc6	éra
superhrdinské	superhrdinský	k2eAgInPc4d1	superhrdinský
komiksy	komiks	k1gInPc4	komiks
všeobecně	všeobecně	k6eAd1	všeobecně
upadaly	upadat	k5eAaImAgFnP	upadat
a	a	k8xC	a
dostávaly	dostávat	k5eAaImAgFnP	dostávat
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
zájem	zájem	k1gInSc4	zájem
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Goodman	Goodman	k1gMnSc1	Goodman
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
přeorientovat	přeorientovat	k5eAaPmF	přeorientovat
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
horrorů	horror	k1gInPc2	horror
<g/>
,	,	kIx,	,
westernů	western	k1gInPc2	western
<g/>
,	,	kIx,	,
krimi	krimi	k1gFnPc2	krimi
<g/>
,	,	kIx,	,
válečných	válečný	k2eAgInPc2d1	válečný
a	a	k8xC	a
dobrodružných	dobrodružný	k2eAgInPc2d1	dobrodružný
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
i	i	k8xC	i
proto	proto	k8xC	proto
změnil	změnit	k5eAaPmAgInS	změnit
název	název	k1gInSc1	název
na	na	k7c4	na
Atlas	Atlas	k1gInSc4	Atlas
Comics	comics	k1gInSc4	comics
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
svých	svůj	k3xOyFgFnPc2	svůj
novin	novina	k1gFnPc2	novina
Atlas	Atlas	k1gInSc1	Atlas
News	News	k1gInSc4	News
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
oživení	oživení	k1gNnSc4	oživení
superhrdinské	superhrdinský	k2eAgFnSc2d1	superhrdinská
tematiky	tematika	k1gFnSc2	tematika
u	u	k7c2	u
Atlasu	Atlas	k1gInSc2	Atlas
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
znovu	znovu	k6eAd1	znovu
vydávány	vydáván	k2eAgInPc4d1	vydáván
komiksy	komiks	k1gInPc4	komiks
Human	Human	k1gMnSc1	Human
Torch	Torch	k1gMnSc1	Torch
<g/>
,	,	kIx,	,
Namor	Namor	k1gMnSc1	Namor
a	a	k8xC	a
Captain	Captain	k1gMnSc1	Captain
America	America	k1gMnSc1	America
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
Syd	Syd	k1gMnSc1	Syd
Shores	Shores	k1gMnSc1	Shores
<g/>
,	,	kIx,	,
Dick	Dick	k1gMnSc1	Dick
Ayer	Ayer	k1gMnSc1	Ayer
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Everett	Everett	k1gMnSc1	Everett
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Romita	Romita	k1gMnSc1	Romita
Sr	Sr	k1gMnSc1	Sr
<g/>
.	.	kIx.	.
a	a	k8xC	a
Stan	stan	k1gInSc1	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
éra	éra	k1gFnSc1	éra
započala	započnout	k5eAaPmAgFnS	započnout
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zásluha	zásluha	k1gFnSc1	zásluha
za	za	k7c4	za
oživení	oživení	k1gNnSc4	oživení
superhrdinských	superhrdinský	k2eAgInPc2d1	superhrdinský
komiksů	komiks	k1gInPc2	komiks
patří	patřit	k5eAaImIp3nS	patřit
spíše	spíše	k9	spíše
konkurenčním	konkurenční	k2eAgInSc7d1	konkurenční
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Marvel	Marvel	k1gInSc1	Marvel
se	se	k3xPyFc4	se
dokázal	dokázat	k5eAaPmAgInS	dokázat
plně	plně	k6eAd1	plně
adaptovat	adaptovat	k5eAaBmF	adaptovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1961	[number]	k4	1961
Stan	stan	k1gInSc4	stan
Lee	Lea	k1gFnSc3	Lea
a	a	k8xC	a
Jack	Jack	k1gInSc4	Jack
Kirby	Kirba	k1gFnSc2	Kirba
představili	představit	k5eAaPmAgMnP	představit
nový	nový	k2eAgInSc4d1	nový
tým	tým	k1gInSc4	tým
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Foura	k1gFnPc2	Foura
v	v	k7c6	v
The	The	k1gFnSc6	The
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Foura	k1gFnPc2	Foura
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Marvel	Marvela	k1gFnPc2	Marvela
dále	daleko	k6eAd2	daleko
vydávalo	vydávat	k5eAaPmAgNnS	vydávat
westerny	western	k1gInPc4	western
<g/>
,	,	kIx,	,
romance	romance	k1gFnPc4	romance
a	a	k8xC	a
válečné	válečný	k2eAgInPc4d1	válečný
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
například	například	k6eAd1	například
komiks	komiks	k1gInSc4	komiks
Sgt	Sgt	k1gFnSc2	Sgt
<g/>
.	.	kIx.	.
</s>
<s>
Fury	Fura	k1gFnPc1	Fura
and	and	k?	and
his	his	k1gNnPc1	his
Howling	Howling	k1gInSc1	Howling
Commandos	Commandos	k1gInSc4	Commandos
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Nick	Nick	k1gInSc4	Nick
Fury	Fura	k1gFnSc2	Fura
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
vydávány	vydávat	k5eAaPmNgInP	vydávat
i	i	k9	i
další	další	k2eAgInPc1d1	další
komiksy	komiks	k1gInPc1	komiks
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Hulk	Hulk	k1gMnSc1	Hulk
<g/>
,	,	kIx,	,
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
<g/>
,	,	kIx,	,
Thor	Thor	k1gMnSc1	Thor
<g/>
,	,	kIx,	,
Ant-Man	Ant-Man	k1gMnSc1	Ant-Man
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
X-Men	X-Mna	k1gFnPc2	X-Mna
<g/>
,	,	kIx,	,
Daredevil	Daredevil	k1gFnPc2	Daredevil
i	i	k8xC	i
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
nemesis	nemesis	k1gFnSc7	nemesis
Doctor	Doctor	k1gMnSc1	Doctor
Doom	Doom	k1gMnSc1	Doom
<g/>
,	,	kIx,	,
Magneto	magneto	k1gNnSc1	magneto
<g/>
,	,	kIx,	,
Galactus	Galactus	k1gInSc1	Galactus
<g/>
,	,	kIx,	,
Loki	Loki	k1gNnPc1	Loki
<g/>
,	,	kIx,	,
Green	Green	k2eAgInSc1d1	Green
Goblin	Goblin	k1gInSc1	Goblin
a	a	k8xC	a
Doctor	Doctor	k1gMnSc1	Doctor
Octopus	Octopus	k1gMnSc1	Octopus
<g/>
.	.	kIx.	.
</s>
<s>
Stan	stan	k1gInSc4	stan
Lee	Lea	k1gFnSc3	Lea
a	a	k8xC	a
Steve	Steve	k1gMnSc1	Steve
Ditko	Ditko	k1gNnSc4	Ditko
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
nejúspěšnější	úspěšný	k2eAgFnSc4d3	nejúspěšnější
komiksovou	komiksový	k2eAgFnSc4d1	komiksová
sérii	série	k1gFnSc4	série
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vydali	vydat	k5eAaPmAgMnP	vydat
první	první	k4xOgNnSc4	první
číslo	číslo	k1gNnSc4	číslo
Spider-Mana	Spider-Mana	k1gFnSc1	Spider-Mana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
Marvel	Marvel	k1gInSc1	Marvel
ročně	ročně	k6eAd1	ročně
prodával	prodávat	k5eAaImAgInS	prodávat
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
komiksových	komiksový	k2eAgFnPc2d1	komiksová
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
Goodman	Goodman	k1gMnSc1	Goodman
prodal	prodat	k5eAaPmAgMnS	prodat
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
společnosti	společnost	k1gFnSc2	společnost
Perfect	Perfecta	k1gFnPc2	Perfecta
Film	film	k1gInSc1	film
and	and	k?	and
Chemical	Chemical	k1gFnSc1	Chemical
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spadala	spadat	k5eAaImAgFnS	spadat
pod	pod	k7c7	pod
Magazine	Magazin	k1gInSc5	Magazin
Management	management	k1gInSc1	management
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
Goodman	Goodman	k1gMnSc1	Goodman
po	po	k7c6	po
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
od	od	k7c2	od
Marvelu	Marvel	k1gInSc2	Marvel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
americké	americký	k2eAgNnSc1d1	americké
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
požádalo	požádat	k5eAaPmAgNnS	požádat
Stana	Stan	k1gMnSc4	Stan
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
komiksů	komiks	k1gInPc2	komiks
zapracoval	zapracovat	k5eAaPmAgMnS	zapracovat
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
braní	braní	k1gNnSc3	braní
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
takový	takový	k3xDgInSc1	takový
příběh	příběh	k1gInSc1	příběh
v	v	k7c6	v
komiksu	komiks	k1gInSc6	komiks
Spider-Mana	Spider-Mana	k1gFnSc1	Spider-Mana
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vydavatelská	vydavatelský	k2eAgFnSc1d1	vydavatelská
autocenzura	autocenzura	k1gFnSc1	autocenzura
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Comics	comics	k1gInSc4	comics
Code	Code	k1gFnSc4	Code
Authority	Authorita	k1gFnSc2	Authorita
příběh	příběh	k1gInSc1	příběh
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
pro	pro	k7c4	pro
propagaci	propagace	k1gFnSc4	propagace
drog	droga	k1gFnPc2	droga
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
kontext	kontext	k1gInSc4	kontext
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
ho	on	k3xPp3gMnSc4	on
Lee	Lea	k1gFnSc6	Lea
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
číslech	číslo	k1gNnPc6	číslo
The	The	k1gMnPc2	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
#	#	kIx~	#
<g/>
96	[number]	k4	96
<g/>
–	–	k?	–
<g/>
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
<s>
Komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
byl	být	k5eAaImAgInS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
pravidel	pravidlo	k1gNnPc2	pravidlo
vydávání	vydávání	k1gNnSc2	vydávání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
se	se	k3xPyFc4	se
Stan	stan	k1gInSc1	stan
Lee	Lea	k1gFnSc3	Lea
stal	stát	k5eAaPmAgInS	stát
šéfvydavatelem	šéfvydavatel	k1gMnSc7	šéfvydavatel
Marvelu	Marvel	k1gInSc2	Marvel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaPmF	vydávat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
otevřeně	otevřeně	k6eAd1	otevřeně
horrorové	horrorové	k2eAgFnSc1d1	horrorové
a	a	k8xC	a
vcelku	vcelku	k6eAd1	vcelku
násilné	násilný	k2eAgInPc4d1	násilný
komiksy	komiks	k1gInPc4	komiks
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
The	The	k1gMnSc1	The
Tomb	Tomb	k1gMnSc1	Tomb
of	of	k?	of
Dracula	Dracula	k1gFnSc1	Dracula
<g/>
,	,	kIx,	,
Shang-Chi	Shang-Chi	k1gNnSc1	Shang-Chi
<g/>
,	,	kIx,	,
Conan	Conan	k1gInSc1	Conan
<g/>
,	,	kIx,	,
Red	Red	k1gFnSc1	Red
Sonja	Sonja	k1gFnSc1	Sonja
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
adaptace	adaptace	k1gFnSc1	adaptace
filmů	film	k1gInPc2	film
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
odysea	odysea	k1gFnSc1	odysea
a	a	k8xC	a
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
první	první	k4xOgInSc4	první
marvel	marvel	k1gInSc4	marvel
komiksový	komiksový	k2eAgInSc4d1	komiksový
con	con	k?	con
MarvelCon	MarvelCon	k1gInSc4	MarvelCon
75	[number]	k4	75
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
Marvel	Marvel	k1gInSc1	Marvel
pronikl	proniknout	k5eAaPmAgInS	proniknout
na	na	k7c4	na
britský	britský	k2eAgInSc4d1	britský
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Imprint	Imprint	k1gMnSc1	Imprint
Marvel	Marvel	k1gMnSc1	Marvel
UK	UK	kA	UK
tehdy	tehdy	k6eAd1	tehdy
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaPmF	vydávat
příběhy	příběh	k1gInPc4	příběh
superhrdiny	superhrdina	k1gFnSc2	superhrdina
Captain	Captaina	k1gFnPc2	Captaina
Britain	Britain	k1gMnSc1	Britain
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
se	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
editorem	editor	k1gInSc7	editor
stal	stát	k5eAaPmAgInS	stát
Jim	on	k3xPp3gMnPc3	on
Shooter	Shooter	k1gInSc4	Shooter
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
byly	být	k5eAaImAgFnP	být
vydávány	vydáván	k2eAgFnPc1d1	vydávána
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
série	série	k1gFnPc1	série
jako	jako	k8xS	jako
Uncanny	Uncanna	k1gFnPc1	Uncanna
X-Men	X-Mna	k1gFnPc2	X-Mna
od	od	k7c2	od
Chrise	Chrise	k1gFnSc2	Chrise
Claremonta	Claremont	k1gMnSc2	Claremont
a	a	k8xC	a
Johna	John	k1gMnSc2	John
Byrnea	Byrneus	k1gMnSc2	Byrneus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Daredevil	Daredevil	k1gMnSc1	Daredevil
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
prospěla	prospět	k5eAaPmAgFnS	prospět
pomoc	pomoc	k1gFnSc4	pomoc
od	od	k7c2	od
Franka	Frank	k1gMnSc2	Frank
Millera	Miller	k1gMnSc2	Miller
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
první	první	k4xOgFnPc1	první
superhrdinské	superhrdinský	k2eAgFnPc1d1	superhrdinská
crossovery	crossovera	k1gFnPc1	crossovera
Contest	Contest	k1gInSc1	Contest
of	of	k?	of
Champions	Champions	k1gInSc1	Champions
a	a	k8xC	a
Tajné	tajný	k2eAgFnPc1d1	tajná
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
vydávány	vydáván	k2eAgInPc1d1	vydáván
příběhy	příběh	k1gInPc1	příběh
z	z	k7c2	z
alternativní	alternativní	k2eAgFnSc2d1	alternativní
reality	realita	k1gFnSc2	realita
New	New	k1gFnSc2	New
Universe	Universe	k1gFnSc2	Universe
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
nesetkaly	setkat	k5eNaPmAgFnP	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
také	také	k9	také
Marvel	Marvel	k1gInSc1	Marvel
založí	založit	k5eAaPmIp3nS	založit
dva	dva	k4xCgInPc4	dva
imprinty	imprint	k1gInPc4	imprint
Star	Star	kA	Star
Comics	comics	k1gInSc4	comics
a	a	k8xC	a
Epic	Epic	k1gInSc4	Epic
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Marvel	Marvel	k1gMnSc1	Marvel
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
zaostával	zaostávat	k5eAaImAgInS	zaostávat
za	za	k7c4	za
DC	DC	kA	DC
Comics	comics	k1gInSc4	comics
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
komerčně	komerčně	k6eAd1	komerčně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
při	při	k7c6	při
udílení	udílení	k1gNnSc6	udílení
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
byl	být	k5eAaImAgInS	být
Marvel	Marvel	k1gInSc1	Marvel
prodán	prodán	k2eAgInSc1d1	prodán
společnosti	společnost	k1gFnSc3	společnost
New	New	k1gFnSc2	New
World	Worlda	k1gFnPc2	Worlda
Entertainment	Entertainment	k1gInSc1	Entertainment
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gInSc4	on
však	však	k9	však
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
prodala	prodat	k5eAaPmAgFnS	prodat
společnosti	společnost	k1gFnSc3	společnost
MacAndrews	MacAndrewsa	k1gFnPc2	MacAndrewsa
and	and	k?	and
Forbes	forbes	k1gInSc1	forbes
<g/>
.	.	kIx.	.
</s>
<s>
Devadesátá	devadesátý	k4xOgNnPc1	devadesátý
léta	léto	k1gNnPc1	léto
byla	být	k5eAaImAgNnP	být
opět	opět	k6eAd1	opět
érou	éra	k1gFnSc7	éra
boomu	boom	k1gInSc2	boom
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
komiksy	komiks	k1gInPc4	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Marvel	Marvet	k5eAaImAgMnS	Marvet
i	i	k9	i
proto	proto	k8xC	proto
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaPmF	vydávat
alternativní	alternativní	k2eAgInPc4d1	alternativní
příběhy	příběh	k1gInPc4	příběh
svých	svůj	k3xOyFgFnPc2	svůj
postav	postava	k1gFnPc2	postava
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
2099	[number]	k4	2099
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
příběhy	příběh	k1gInPc1	příběh
byly	být	k5eAaImAgInP	být
zřízeny	zřízen	k2eAgInPc1d1	zřízen
k	k	k7c3	k
vyvrcholení	vyvrcholení	k1gNnSc3	vyvrcholení
v	v	k7c4	v
Onslaught	Onslaught	k1gInSc4	Onslaught
ságu	sága	k1gFnSc4	sága
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
Marvel	Marvel	k1gMnSc1	Marvel
zažil	zažít	k5eAaPmAgMnS	zažít
vyprázdnění	vyprázdnění	k1gNnSc4	vyprázdnění
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gNnPc2	jeho
sedm	sedm	k4xCc1	sedm
významných	významný	k2eAgMnPc2d1	významný
umělců	umělec	k1gMnPc2	umělec
odešlo	odejít	k5eAaPmAgNnS	odejít
ke	k	k7c3	k
konkurenci	konkurence	k1gFnSc3	konkurence
do	do	k7c2	do
Image	image	k1gFnSc2	image
Comics	comics	k1gInSc1	comics
(	(	kIx(	(
<g/>
Todd	Todd	k1gInSc1	Todd
McFarlane	McFarlan	k1gMnSc5	McFarlan
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Rob	roba	k1gFnPc2	roba
Liefeld	Liefelda	k1gFnPc2	Liefelda
<g/>
,	,	kIx,	,
Marc	Marc	k1gFnSc1	Marc
Silvestri	Silvestri	k1gNnSc2	Silvestri
<g/>
,	,	kIx,	,
Erik	Erik	k1gMnSc1	Erik
Larsen	larsena	k1gFnPc2	larsena
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
Valentino	Valentina	k1gFnSc5	Valentina
a	a	k8xC	a
Whilce	Whilec	k1gMnPc4	Whilec
Portacio	Portacio	k6eAd1	Portacio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
imprint	imprint	k1gInSc1	imprint
Marvel	Marvela	k1gFnPc2	Marvela
Knights	Knightsa	k1gFnPc2	Knightsa
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
byly	být	k5eAaImAgFnP	být
devadesátá	devadesátý	k4xOgNnPc4	devadesátý
léta	léto	k1gNnPc4	léto
pro	pro	k7c4	pro
Marvel	Marvel	k1gInSc4	Marvel
zklamáním	zklamání	k1gNnSc7	zklamání
<g/>
,	,	kIx,	,
vrcholem	vrchol	k1gInSc7	vrchol
bylo	být	k5eAaImAgNnS	být
vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
bankrotu	bankrot	k1gInSc2	bankrot
pro	pro	k7c4	pro
Marvel	Marvel	k1gInSc4	Marvel
Entertainment	Entertainment	k1gMnSc1	Entertainment
Group	Group	k1gMnSc1	Group
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
založení	založení	k1gNnSc1	založení
Marvel	Marvela	k1gFnPc2	Marvela
Enterprises	Enterprisesa	k1gFnPc2	Enterprisesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
letech	let	k1gInPc6	let
Marvel	Marvel	k1gInSc1	Marvel
znovu	znovu	k6eAd1	znovu
překročil	překročit	k5eAaPmAgInS	překročit
vydavatelské	vydavatelský	k2eAgFnPc4d1	vydavatelská
zvyklosti	zvyklost	k1gFnPc4	zvyklost
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaImF	vydávat
násilné	násilný	k2eAgInPc4d1	násilný
příběhy	příběh	k1gInPc4	příběh
pod	pod	k7c7	pod
imprintem	imprint	k1gInSc7	imprint
MAX	Max	k1gMnSc1	Max
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
vzniklými	vzniklý	k2eAgInPc7d1	vzniklý
imprinty	imprint	k1gInPc7	imprint
byly	být	k5eAaImAgFnP	být
i	i	k9	i
Marvel	Marvlo	k1gNnPc2	Marvlo
Adventures	Adventuresa	k1gFnPc2	Adventuresa
a	a	k8xC	a
především	především	k6eAd1	především
Ultimate	Ultimat	k1gInSc5	Ultimat
Marvel	Marvel	k1gMnSc1	Marvel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaImF	vydávat
alternativní	alternativní	k2eAgInPc4d1	alternativní
restartované	restartovaný	k2eAgInPc4d1	restartovaný
a	a	k8xC	a
moderní	moderní	k2eAgInPc4d1	moderní
příběhy	příběh	k1gInPc4	příběh
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
v	v	k7c6	v
crossoveru	crossover	k1gInSc6	crossover
Ultimatum	ultimatum	k1gNnSc1	ultimatum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
Marvel	Marvel	k1gMnSc1	Marvel
dočkal	dočkat	k5eAaPmAgMnS	dočkat
úspěchu	úspěch	k1gInSc2	úspěch
na	na	k7c6	na
filmovém	filmový	k2eAgNnSc6d1	filmové
plátně	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
natočeny	natočit	k5eAaBmNgInP	natočit
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
filmy	film	k1gInPc1	film
X-Men	X-Mna	k1gFnPc2	X-Mna
a	a	k8xC	a
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
proměnily	proměnit	k5eAaPmAgFnP	proměnit
do	do	k7c2	do
filmových	filmový	k2eAgFnPc2d1	filmová
sérií	série	k1gFnPc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
The	The	k1gFnSc2	The
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
Marvel	Marvel	k1gInSc4	Marvel
za	za	k7c4	za
čtyři	čtyři	k4xCgFnPc4	čtyři
miliardy	miliarda	k4xCgFnPc4	miliarda
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Disney	Disney	k1gInPc7	Disney
<g/>
,	,	kIx,	,
Marvel	Marvel	k1gMnSc1	Marvel
oživil	oživit	k5eAaPmAgMnS	oživit
imprint	imprint	k1gInSc4	imprint
CrossGen	CrossGen	k2eAgInSc4d1	CrossGen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaImF	vydávat
komiksy	komiks	k1gInPc4	komiks
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
filmu	film	k1gInSc2	film
Iron	iron	k1gInSc4	iron
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
Marvel	Marvel	k1gMnSc1	Marvel
založil	založit	k5eAaPmAgMnS	založit
Marvel	Marvel	k1gInSc4	Marvel
Cinematic	Cinematice	k1gFnPc2	Cinematice
Universe	Universe	k1gFnSc2	Universe
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
na	na	k7c6	na
stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
plátně	plátno	k1gNnSc6	plátno
propojuje	propojovat	k5eAaImIp3nS	propojovat
příběhy	příběh	k1gInPc4	příběh
i	i	k8xC	i
dalších	další	k2eAgFnPc2d1	další
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětové	celosvětový	k2eAgFnPc1d1	celosvětová
tržby	tržba	k1gFnPc1	tržba
z	z	k7c2	z
marvel	marvela	k1gFnPc2	marvela
filmů	film	k1gInPc2	film
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
činily	činit	k5eAaImAgInP	činit
12	[number]	k4	12
miliard	miliarda	k4xCgFnPc2	miliarda
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Marvel	Marvel	k1gInSc1	Marvel
také	také	k9	také
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
televizní	televizní	k2eAgInPc4d1	televizní
seriály	seriál	k1gInPc4	seriál
či	či	k8xC	či
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
u	u	k7c2	u
Marvel	Marvela	k1gFnPc2	Marvela
celkový	celkový	k2eAgInSc4d1	celkový
relaunch	relaunch	k1gInSc4	relaunch
vydávaných	vydávaný	k2eAgFnPc2d1	vydávaná
sérií	série	k1gFnPc2	série
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
série	série	k1gFnSc2	série
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Marvel	Marvela	k1gFnPc2	Marvela
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Zcela	zcela	k6eAd1	zcela
novými	nový	k2eAgFnPc7d1	nová
sériemi	série	k1gFnPc7	série
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
Uncanny	Uncanen	k2eAgFnPc1d1	Uncanen
Avengers	Avengers	k1gInSc4	Avengers
a	a	k8xC	a
All-New	All-New	k1gFnSc4	All-New
X-Men	X-Mna	k1gFnPc2	X-Mna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2014	[number]	k4	2014
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
restartování	restartování	k1gNnSc4	restartování
sérií	série	k1gFnPc2	série
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
All-New	All-New	k1gFnSc1	All-New
Marvel	Marvel	k1gMnSc1	Marvel
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
2014	[number]	k4	2014
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
třetí	třetí	k4xOgFnSc1	třetí
vlna	vlna	k1gFnSc1	vlna
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Avengers	Avengersa	k1gFnPc2	Avengersa
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Série	série	k1gFnPc1	série
vydávané	vydávaný	k2eAgFnPc1d1	vydávaná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vln	vlna	k1gFnPc2	vlna
Marvel	Marvela	k1gFnPc2	Marvela
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
Marvel	Marvel	k1gInSc4	Marvel
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
All-New	All-New	k1gFnSc1	All-New
X-Men	X-Men	k1gInSc1	X-Men
<g/>
,	,	kIx,	,
Avengers	Avengers	k1gInSc1	Avengers
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
Avengers	Avengers	k1gInSc1	Avengers
Arena	Areno	k1gNnSc2	Areno
<g/>
,	,	kIx,	,
Cable	Cabla	k1gFnSc3	Cabla
and	and	k?	and
X-Force	X-Forka	k1gFnSc3	X-Forka
<g/>
,	,	kIx,	,
Captain	Captain	k2eAgMnSc1d1	Captain
America	America	k1gMnSc1	America
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
,	,	kIx,	,
Deadpool	Deadpool	k1gInSc1	Deadpool
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
FF	ff	kA	ff
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Foura	k1gFnPc2	Foura
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
pokračování	pokračování	k1gNnSc1	pokračování
<g/>
,	,	kIx,	,
Fearless	Fearless	k1gInSc1	Fearless
Defenders	Defenders	k1gInSc1	Defenders
<g/>
,	,	kIx,	,
Guardians	Guardians	k1gInSc1	Guardians
of	of	k?	of
the	the	k?	the
Galaxy	Galax	k1gInPc7	Galax
<g/>
,	,	kIx,	,
Indestructible	Indestructible	k1gMnSc1	Indestructible
Hulk	Hulk	k1gMnSc1	Hulk
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
Morbius	Morbius	k1gInSc1	Morbius
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Living	Living	k1gInSc1	Living
Vampire	Vampir	k1gInSc5	Vampir
<g/>
,	,	kIx,	,
New	New	k1gFnPc3	New
Avengers	Avengersa	k1gFnPc2	Avengersa
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
vol	vol	k6eAd1	vol
<g />
.	.	kIx.	.
</s>
<s>
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
Savage	Savage	k1gFnSc1	Savage
Wolverine	Wolverin	k1gInSc5	Wolverin
<g/>
,	,	kIx,	,
Secret	Secret	k1gInSc4	Secret
Avengers	Avengersa	k1gFnPc2	Avengersa
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Superior	superior	k1gMnSc1	superior
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
<g/>
,	,	kIx,	,
Thor	Thor	k1gMnSc1	Thor
<g/>
:	:	kIx,	:
God	God	k1gMnSc1	God
of	of	k?	of
Thunder	Thunder	k1gInSc1	Thunder
<g/>
,	,	kIx,	,
Thunderbolts	Thunderbolts	k1gInSc1	Thunderbolts
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Uncanny	Uncanna	k1gFnPc1	Uncanna
Avengers	Avengers	k1gInSc1	Avengers
<g/>
,	,	kIx,	,
Uncanny	Uncanna	k1gFnPc1	Uncanna
X-Force	X-Forka	k1gFnSc3	X-Forka
<g/>
,	,	kIx,	,
Uncanny	Uncanna	k1gFnPc1	Uncanna
X-Men	X-Mno	k1gNnPc2	X-Mno
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
Wolverine	Wolverin	k1gInSc5	Wolverin
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
X-Men	X-Men	k2eAgInSc1d1	X-Men
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
X-Men	X-Men	k1gInSc1	X-Men
Legacy	Legaca	k1gFnSc2	Legaca
<g/>
,	,	kIx,	,
Young	Young	k1gInSc4	Young
Avengers	Avengersa	k1gFnPc2	Avengersa
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
All-New	All-New	k?	All-New
Marvel	Marvlo	k1gNnPc2	Marvlo
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
All-New	All-New	k1gMnSc1	All-New
Ghost	Ghost	k1gMnSc1	Ghost
Rider	Rider	k1gMnSc1	Rider
<g/>
,	,	kIx,	,
All-New	All-New	k1gMnSc1	All-New
Invaders	Invaders	k1gInSc1	Invaders
<g/>
,	,	kIx,	,
All-New	All-New	k1gMnSc1	All-New
X-Factor	X-Factor	k1gMnSc1	X-Factor
<g/>
,	,	kIx,	,
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
Avengers	Avengers	k1gInSc1	Avengers
Undercover	Undercover	k1gInSc1	Undercover
<g/>
,	,	kIx,	,
Avengers	Avengers	k1gInSc1	Avengers
World	World	k1gInSc1	World
<g/>
,	,	kIx,	,
Black	Black	k1gInSc1	Black
Widow	Widow	k1gFnPc2	Widow
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
Captain	Captain	k2eAgInSc1d1	Captain
Marvel	Marvel	k1gInSc1	Marvel
<g/>
,	,	kIx,	,
Cyclops	Cyclops	k1gInSc1	Cyclops
<g/>
,	,	kIx,	,
Daredevil	Daredevil	k1gFnSc1	Daredevil
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Elektra	Elektra	k1gFnSc1	Elektra
<g/>
,	,	kIx,	,
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Foura	k1gFnPc2	Foura
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Hulk	Hulk	k1gMnSc1	Hulk
<g/>
,	,	kIx,	,
Inhuman	Inhuman	k1gMnSc1	Inhuman
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc1	iron
Fist	Fist	k1gInSc1	Fist
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Living	Living	k1gInSc1	Living
Weapon	Weapon	k1gInSc1	Weapon
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc1	iron
Patriot	patriot	k1gMnSc1	patriot
<g/>
,	,	kIx,	,
Legendary	Legendara	k1gFnPc1	Legendara
Star-Lord	Star-Lordo	k1gNnPc2	Star-Lordo
<g/>
,	,	kIx,	,
Loki	Lok	k1gMnPc1	Lok
<g/>
:	:	kIx,	:
Agent	agent	k1gMnSc1	agent
of	of	k?	of
Asgard	Asgard	k1gInSc1	Asgard
<g/>
,	,	kIx,	,
Magneto	magneto	k1gNnSc1	magneto
<g/>
,	,	kIx,	,
Moon	Moon	k1gInSc1	Moon
<g />
.	.	kIx.	.
</s>
<s>
Knight	Knight	k1gInSc1	Knight
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
Ms.	Ms.	k1gFnSc1	Ms.
Marvel	Marvela	k1gFnPc2	Marvela
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Warriors	Warriorsa	k1gFnPc2	Warriorsa
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
Nightcrawler	Nightcrawler	k1gInSc1	Nightcrawler
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Punisher	Punishra	k1gFnPc2	Punishra
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
,	,	kIx,	,
Rocket	Rocket	k1gMnSc1	Rocket
Raccoon	Raccoon	k1gMnSc1	Raccoon
<g/>
,	,	kIx,	,
Savage	Savage	k1gFnSc1	Savage
Hulk	Hulk	k1gInSc1	Hulk
<g/>
,	,	kIx,	,
Secret	Secret	k1gInSc1	Secret
Avengers	Avengersa	k1gFnPc2	Avengersa
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
She-Hulk	She-Hulk	k1gInSc1	She-Hulk
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Silver	Silver	k1gInSc1	Silver
Surfer	Surfra	k1gFnPc2	Surfra
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
,	,	kIx,	,
Spider-Man	Spider-Man	k1gInSc1	Spider-Man
2099	[number]	k4	2099
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Storm	Storm	k1gInSc1	Storm
<g/>
,	,	kIx,	,
Wolverine	Wolverin	k1gInSc5	Wolverin
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
Wolverine	Wolverin	k1gInSc5	Wolverin
and	and	k?	and
the	the	k?	the
X-Men	X-Mno	k1gNnPc2	X-Mno
<g/>
,	,	kIx,	,
X-Force	X-Forka	k1gFnSc3	X-Forka
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Avengers	Avengers	k1gInSc1	Avengers
NOW	NOW	kA	NOW
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
Bucky	bucek	k1gMnPc4	bucek
Barnes	Barnesa	k1gFnPc2	Barnesa
<g/>
:	:	kIx,	:
Winter	Winter	k1gMnSc1	Winter
Soldier	Soldier	k1gMnSc1	Soldier
<g/>
,	,	kIx,	,
Deathlok	Deathlok	k1gInSc1	Deathlok
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
Guardians	Guardians	k1gInSc1	Guardians
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
Thor	Thor	k1gInSc1	Thor
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
All-New	All-New	k1gMnSc1	All-New
Captain	Captain	k1gMnSc1	Captain
America	America	k1gMnSc1	America
<g/>
,	,	kIx,	,
Angela	Angela	k1gFnSc1	Angela
<g/>
:	:	kIx,	:
Asgard	Asgard	k1gInSc1	Asgard
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Assassin	Assassin	k1gInSc1	Assassin
<g/>
,	,	kIx,	,
Superior	superior	k1gMnSc1	superior
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
v	v	k7c6	v
dějové	dějový	k2eAgFnSc6d1	dějová
linii	linie	k1gFnSc6	linie
"	"	kIx"	"
<g/>
Time	Time	k1gNnSc1	Time
Runs	Runsa	k1gFnPc2	Runsa
Out	Out	k1gFnPc2	Out
<g/>
"	"	kIx"	"
z	z	k7c2	z
komiksů	komiks	k1gInPc2	komiks
Avengers	Avengersa	k1gFnPc2	Avengersa
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
a	a	k8xC	a
New	New	k1gMnPc1	New
Avengers	Avengersa	k1gFnPc2	Avengersa
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
multivesmíru	multivesmír	k1gMnSc3	multivesmír
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
v	v	k7c6	v
crossoveru	crossover	k1gInSc6	crossover
Secret	Secret	k1gMnSc1	Secret
Wars	Wars	k1gInSc1	Wars
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
eventu	event	k1gInSc2	event
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
restartu	restart	k1gInSc3	restart
šedesáti	šedesát	k4xCc2	šedesát
sérií	série	k1gFnPc2	série
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
začnou	začít	k5eAaPmIp3nP	začít
být	být	k5eAaImF	být
vydávány	vydávat	k5eAaPmNgInP	vydávat
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
čísla	číslo	k1gNnSc2	číslo
pod	pod	k7c7	pod
novou	nový	k2eAgFnSc7d1	nová
hlavičkou	hlavička	k1gFnSc7	hlavička
All-New	All-New	k1gMnSc1	All-New
<g/>
,	,	kIx,	,
All-Different	All-Different	k1gMnSc1	All-Different
Marvel	Marvel	k1gMnSc1	Marvel
<g/>
.	.	kIx.	.
</s>
<s>
Spider-man	Spideran	k1gMnSc1	Spider-man
01	[number]	k4	01
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
#	#	kIx~	#
<g/>
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
58	[number]	k4	58
<g/>
)	)	kIx)	)
Spider-man	Spideran	k1gMnSc1	Spider-man
02	[number]	k4	02
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
#	#	kIx~	#
<g/>
59	[number]	k4	59
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
Spider-man	Spideran	k1gMnSc1	Spider-man
03	[number]	k4	03
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
#	#	kIx~	#
<g/>
68	[number]	k4	68
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
<g/>
)	)	kIx)	)
Spider-man	Spideran	k1gMnSc1	Spider-man
04	[number]	k4	04
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
#	#	kIx~	#
<g/>
76	[number]	k4	76
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
)	)	kIx)	)
Spider-man	Spideran	k1gMnSc1	Spider-man
05	[number]	k4	05
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
#	#	kIx~	#
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
89	[number]	k4	89
<g/>
)	)	kIx)	)
Spider-man	Spideran	k1gMnSc1	Spider-man
06	[number]	k4	06
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
#	#	kIx~	#
<g/>
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
98	[number]	k4	98
<g/>
)	)	kIx)	)
Spider-man	Spideran	k1gMnSc1	Spider-man
07	[number]	k4	07
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Amazing	Amazing	k1gInSc1	Amazing
Spider	Spider	k1gMnSc1	Spider
-	-	kIx~	-
Man	Man	k1gMnSc1	Man
99	[number]	k4	99
<g/>
-	-	kIx~	-
<g/>
109	[number]	k4	109
<g/>
)	)	kIx)	)
X-men	Xen	k1gInSc1	X-men
01	[number]	k4	01
(	(	kIx(	(
<g/>
Giant	Giant	k1gInSc1	Giant
Size	Size	k1gNnSc1	Size
X-men	Xen	k1gInSc1	X-men
#	#	kIx~	#
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
X-Men	X-Men	k1gInSc1	X-Men
#	#	kIx~	#
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
103	[number]	k4	103
<g/>
)	)	kIx)	)
X-men	Xen	k1gInSc1	X-men
02	[number]	k4	02
(	(	kIx(	(
<g/>
X-Men	X-Mna	k1gFnPc2	X-Mna
#	#	kIx~	#
<g/>
104	[number]	k4	104
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
113	[number]	k4	113
<g/>
)	)	kIx)	)
X-men	Xen	k1gInSc1	X-men
03	[number]	k4	03
(	(	kIx(	(
<g/>
X-Men	X-Mna	k1gFnPc2	X-Mna
#	#	kIx~	#
<g/>
114	[number]	k4	114
<g/>
-	-	kIx~	-
<g/>
124	[number]	k4	124
<g/>
)	)	kIx)	)
X-men	Xen	k1gInSc1	X-men
04	[number]	k4	04
(	(	kIx(	(
<g/>
X-Men	X-Mna	k1gFnPc2	X-Mna
#	#	kIx~	#
<g/>
125	[number]	k4	125
<g/>
-	-	kIx~	-
<g/>
138	[number]	k4	138
<g/>
)	)	kIx)	)
Wolverine	Wolverin	k1gInSc5	Wolverin
01	[number]	k4	01
(	(	kIx(	(
<g/>
Wolverine	Wolverin	k1gInSc5	Wolverin
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
Wolverine	Wolverin	k1gInSc5	Wolverin
<g />
.	.	kIx.	.
</s>
<s>
02	[number]	k4	02
(	(	kIx(	(
<g/>
Wolverine	Wolverin	k1gInSc5	Wolverin
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
Wolverine	Wolverin	k1gInSc5	Wolverin
03	[number]	k4	03
(	(	kIx(	(
<g/>
Wolverine	Wolverin	k1gInSc5	Wolverin
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
Wolverine	Wolverin	k1gInSc5	Wolverin
04	[number]	k4	04
(	(	kIx(	(
<g/>
Wolverine	Wolverin	k1gInSc5	Wolverin
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
30	[number]	k4	30
<g/>
)	)	kIx)	)
Wolverine	Wolverin	k1gInSc5	Wolverin
05	[number]	k4	05
(	(	kIx(	(
<g/>
Wolverine	Wolverin	k1gInSc5	Wolverin
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
)	)	kIx)	)
Amazing	Amazing	k1gInSc1	Amazing
Spider-man	Spideran	k1gMnSc1	Spider-man
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Man	k1gInSc1	Spider-Man
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
Amazing	Amazing	k1gInSc1	Amazing
Spider-man	Spideran	k1gMnSc1	Spider-man
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Odhalení	odhalení	k1gNnSc1	odhalení
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
)	)	kIx)	)
Amazing	Amazing	k1gInSc1	Amazing
Spider-man	Spideran	k1gMnSc1	Spider-man
<g/>
:	:	kIx,	:
Dokud	dokud	k8xS	dokud
hvězdy	hvězda	k1gFnPc1	hvězda
nezhasnou	zhasnout	k5eNaPmIp3nP	zhasnout
(	(	kIx(	(
<g/>
The	The	k1gFnSc7	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Many	k1gInPc2	Spider-Many
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
43	[number]	k4	43
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
)	)	kIx)	)
Amazing	Amazing	k1gInSc1	Amazing
Spider-man	Spideran	k1gMnSc1	Spider-man
<g/>
:	:	kIx,	:
Příčiny	příčina	k1gFnPc1	příčina
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
následky	následek	k1gInPc1	následek
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
49	[number]	k4	49
<g/>
-	-	kIx~	-
<g/>
54	[number]	k4	54
<g/>
)	)	kIx)	)
Amazing	Amazing	k1gInSc1	Amazing
Spider-man	Spideran	k1gMnSc1	Spider-man
<g/>
:	:	kIx,	:
Šťastné	Šťastné	k2eAgFnPc4d1	Šťastné
narozeniny	narozeniny	k1gFnPc4	narozeniny
(	(	kIx(	(
<g/>
The	The	k1gMnPc2	The
Amazing	Amazing	k1gInSc4	Amazing
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
58	[number]	k4	58
<g/>
,	,	kIx,	,
The	The	k1gMnPc2	The
Amazing	Amazing	k1gInSc4	Amazing
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
#	#	kIx~	#
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
501	[number]	k4	501
<g/>
)	)	kIx)	)
Amazing	Amazing	k1gInSc1	Amazing
Spider-man	Spideran	k1gMnSc1	Spider-man
<g/>
:	:	kIx,	:
Ezekielův	Ezekielův	k2eAgInSc1d1	Ezekielův
návrat	návrat	k1gInSc1	návrat
(	(	kIx(	(
<g/>
The	The	k1gMnPc2	The
Amazing	Amazing	k1gInSc4	Amazing
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
#	#	kIx~	#
<g/>
502	[number]	k4	502
<g/>
-	-	kIx~	-
<g/>
508	[number]	k4	508
<g/>
)	)	kIx)	)
Amazing	Amazing	k1gInSc1	Amazing
Spider-man	Spideran	k1gMnSc1	Spider-man
<g/>
:	:	kIx,	:
Hříchy	hřích	k1gInPc1	hřích
minulosti	minulost	k1gFnSc2	minulost
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Mana	k1gFnPc2	Spider-Mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
#	#	kIx~	#
<g/>
<g />
.	.	kIx.	.
</s>
<s>
509	[number]	k4	509
<g/>
-	-	kIx~	-
<g/>
514	[number]	k4	514
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
1	[number]	k4	1
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Four	k1gInSc1	Four
#	#	kIx~	#
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Four	k1gInSc1	Four
#	#	kIx~	#
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
3	[number]	k4	3
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Four	k1gInSc1	Four
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
<g />
.	.	kIx.	.
</s>
<s>
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
4	[number]	k4	4
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
5	[number]	k4	5
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xen	k2eAgInSc4d1	X-men
#	#	kIx~	#
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
6	[number]	k4	6
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
7	[number]	k4	7
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g />
.	.	kIx.	.
</s>
<s>
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
8	[number]	k4	8
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
9	[number]	k4	9
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
10	[number]	k4	10
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
11	[number]	k4	11
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
22	[number]	k4	22
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
13	[number]	k4	13
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
14	[number]	k4	14
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
15	[number]	k4	15
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Spider-man	Spideran	k1gMnSc1	Spider-man
#	#	kIx~	#
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Ultimate	Ultimat	k1gInSc5	Ultimat
X-men	Xna	k1gFnPc2	X-mna
#	#	kIx~	#
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
Amazing	Amazing	k1gInSc1	Amazing
Spider-man	Spideran	k1gMnSc1	Spider-man
<g/>
:	:	kIx,	:
Kravenův	Kravenův	k2eAgInSc1d1	Kravenův
poslední	poslední	k2eAgInSc1d1	poslední
lov	lov	k1gInSc1	lov
(	(	kIx(	(
<g/>
Web	web	k1gInSc1	web
of	of	k?	of
Spider-Man	Spider-Man	k1gInSc1	Spider-Man
#	#	kIx~	#
<g/>
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
32	[number]	k4	32
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Amazing	Amazing	k1gInSc1	Amazing
Spider-Man	Spider-Man	k1gInSc1	Spider-Man
#	#	kIx~	#
<g/>
293	[number]	k4	293
<g/>
-	-	kIx~	-
<g/>
294	[number]	k4	294
a	a	k8xC	a
Spectacular	Spectacular	k1gMnSc1	Spectacular
Spider-Man	Spider-Man	k1gMnSc1	Spider-Man
#	#	kIx~	#
<g/>
131	[number]	k4	131
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
132	[number]	k4	132
<g/>
)	)	kIx)	)
Spider-man	Spideran	k1gMnSc1	Spider-man
<g/>
:	:	kIx,	:
Utrpení	utrpení	k1gNnSc1	utrpení
(	(	kIx(	(
<g/>
Spider-Man	Spider-Man	k1gInSc1	Spider-Man
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Torment	Torment	k1gInSc1	Torment
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
X-men	Xen	k1gInSc1	X-men
-	-	kIx~	-
První	první	k4xOgFnSc1	první
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Nejsvětlejší	světlý	k2eAgInSc1d3	nejsvětlejší
zítřek	zítřek	k1gInSc1	zítřek
(	(	kIx(	(
<g/>
X-Men	X-Men	k1gInSc1	X-Men
<g/>
:	:	kIx,	:
First	First	k1gInSc1	First
Class	Classa	k1gFnPc2	Classa
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
#	#	kIx~	#
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
New	New	k1gFnSc1	New
X-men	Xen	k1gInSc1	X-men
<g/>
:	:	kIx,	:
G	G	kA	G
jako	jako	k8xC	jako
genocida	genocida	k1gFnSc1	genocida
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
X-Men	X-Mna	k1gFnPc2	X-Mna
#	#	kIx~	#
<g/>
114	[number]	k4	114
<g/>
-	-	kIx~	-
<g/>
120	[number]	k4	120
<g/>
)	)	kIx)	)
New	New	k1gFnSc1	New
X-men	Xen	k1gInSc1	X-men
<g/>
:	:	kIx,	:
Impérium	impérium	k1gNnSc1	impérium
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
X-Men	X-Mna	k1gFnPc2	X-Mna
#	#	kIx~	#
<g/>
121	[number]	k4	121
<g/>
-	-	kIx~	-
<g/>
126	[number]	k4	126
<g/>
)	)	kIx)	)
Wolverine	Wolverin	k1gInSc5	Wolverin
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
žiju	žít	k5eAaImIp1nS	žít
(	(	kIx(	(
<g/>
Wolverine	Wolverin	k1gInSc5	Wolverin
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
119	[number]	k4	119
<g/>
-	-	kIx~	-
<g/>
122	[number]	k4	122
<g/>
)	)	kIx)	)
Wolverine	Wolverin	k1gInSc5	Wolverin
a	a	k8xC	a
Hulk	Hulko	k1gNnPc2	Hulko
(	(	kIx(	(
<g/>
Wolverine	Wolverin	k1gInSc5	Wolverin
Hulk	Hulk	k1gMnSc1	Hulk
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Thor	Thor	k1gInSc1	Thor
<g/>
:	:	kIx,	:
Vikingové	Viking	k1gMnPc1	Viking
(	(	kIx(	(
<g/>
Thor	Thor	k1gMnSc1	Thor
<g/>
:	:	kIx,	:
Vikings	Vikings	k1gInSc1	Vikings
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
<g/>
:	:	kIx,	:
Extremis	Extremis	k1gFnSc1	Extremis
(	(	kIx(	(
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
4	[number]	k4	4
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Punisher	Punishra	k1gFnPc2	Punishra
Max	max	kA	max
0	[number]	k4	0
<g/>
:	:	kIx,	:
Od	od	k7c2	od
kolébky	kolébka	k1gFnSc2	kolébka
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
Punisher	Punishra	k1gFnPc2	Punishra
Max	max	kA	max
1	[number]	k4	1
<g/>
:	:	kIx,	:
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Punisher	Punishra	k1gFnPc2	Punishra
Max	max	kA	max
2	[number]	k4	2
<g/>
:	:	kIx,	:
Irská	irský	k2eAgFnSc1d1	irská
kuchyně	kuchyně	k1gFnSc1	kuchyně
Punisher	Punishra	k1gFnPc2	Punishra
Max	max	kA	max
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Matička	matička	k1gFnSc1	matička
Rus	Rus	k1gMnSc1	Rus
Punisher	Punishra	k1gFnPc2	Punishra
Max	max	kA	max
4	[number]	k4	4
<g/>
:	:	kIx,	:
Dole	dole	k6eAd1	dole
je	být	k5eAaImIp3nS	být
nahoře	nahoře	k6eAd1	nahoře
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
Punisher	Punishra	k1gFnPc2	Punishra
Max	max	kA	max
5	[number]	k4	5
<g/>
:	:	kIx,	:
Otrokáři	otrokář	k1gMnPc1	otrokář
Punisher	Punishra	k1gFnPc2	Punishra
Max	max	kA	max
6	[number]	k4	6
<g/>
:	:	kIx,	:
Barracuda	Barracuda	k1gFnSc1	Barracuda
Punisher	Punishra	k1gFnPc2	Punishra
Max	max	kA	max
7	[number]	k4	7
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
Punisher	Punishra	k1gFnPc2	Punishra
Max	max	kA	max
8	[number]	k4	8
<g/>
:	:	kIx,	:
Vdovy	vdova	k1gFnSc2	vdova
Punisher	Punishra	k1gFnPc2	Punishra
I.	I.	kA	I.
Punisher	Punishra	k1gFnPc2	Punishra
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Punisher	Punishra	k1gFnPc2	Punishra
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Punisher	Punishra	k1gFnPc2	Punishra
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Ultimate	Ultimat	k1gInSc5	Ultimat
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Four	k1gInSc1	Four
<g/>
:	:	kIx,	:
Zrod	zrod	k1gInSc1	zrod
(	(	kIx(	(
<g/>
Ultimate	Ultimat	k1gInSc5	Ultimat
Fantastic	Fantastice	k1gFnPc2	Fantastice
Four	Four	k1gInSc1	Four
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Captain	Captain	k1gInSc1	Captain
America	Americ	k1gInSc2	Americ
1	[number]	k4	1
(	(	kIx(	(
<g/>
Captain	Captain	k1gInSc1	Captain
America	Americ	k1gInSc2	Americ
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
Captain	Captain	k1gInSc1	Captain
<g />
.	.	kIx.	.
</s>
<s>
America	America	k6eAd1	America
2	[number]	k4	2
(	(	kIx(	(
<g/>
Captain	Captain	k1gInSc1	Captain
America	Americ	k1gInSc2	Americ
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
Winter	Winter	k1gMnSc1	Winter
Soldier	Soldier	k1gMnSc1	Soldier
<g/>
:	:	kIx,	:
Winter	Winter	k1gMnSc1	Winter
Kills	Killsa	k1gFnPc2	Killsa
<g/>
,	,	kIx,	,
Captain	Captain	k1gMnSc1	Captain
America	America	k1gMnSc1	America
<g/>
:	:	kIx,	:
65	[number]	k4	65
<g/>
th	th	k?	th
Anniversary	Anniversara	k1gFnSc2	Anniversara
Special	Special	k1gMnSc1	Special
<g/>
)	)	kIx)	)
Captain	Captain	k1gMnSc1	Captain
America	America	k1gMnSc1	America
<g/>
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Captain	Captain	k1gMnSc1	Captain
America	America	k1gMnSc1	America
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
)	)	kIx)	)
Daredevil	Daredevil	k1gMnSc1	Daredevil
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
beze	beze	k7c2	beze
strachu	strach	k1gInSc2	strach
1	[number]	k4	1
(	(	kIx(	(
<g/>
Daredevil	Daredevil	k1gFnPc4	Daredevil
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
a	a	k8xC	a
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
Daredevil	Daredevil	k1gMnSc1	Daredevil
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
<g />
.	.	kIx.	.
</s>
<s>
beze	beze	k7c2	beze
strachu	strach	k1gInSc2	strach
2	[number]	k4	2
(	(	kIx(	(
<g/>
Daredevil	Daredevil	k1gFnPc4	Daredevil
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
a	a	k8xC	a
56	[number]	k4	56
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
Daredevil	Daredevil	k1gMnSc1	Daredevil
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
beze	beze	k7c2	beze
strachu	strach	k1gInSc2	strach
3	[number]	k4	3
(	(	kIx(	(
<g/>
Daredevil	Daredevil	k1gFnPc4	Daredevil
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
Daredevil	Daredevil	k1gFnSc1	Daredevil
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
beze	beze	k7c2	beze
strachu	strach	k1gInSc2	strach
4	[number]	k4	4
(	(	kIx(	(
<g/>
Daredevil	Daredevil	k1gFnPc4	Daredevil
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
81	[number]	k4	81
<g/>
)	)	kIx)	)
Daredevil	Daredevil	k1gFnSc1	Daredevil
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc1	rok
jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
Daredevil	Daredevil	k1gMnPc1	Daredevil
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
Without	Without	k1gMnSc1	Without
Fear	Fear	k1gMnSc1	Fear
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Ghost	Ghost	k1gMnSc1	Ghost
Rider	Rider	k1gMnSc1	Rider
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
slz	slza	k1gFnPc2	slza
Ghost	Ghost	k1gMnSc1	Ghost
Rider	Rider	k1gMnSc1	Rider
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
zatracení	zatracení	k1gNnSc2	zatracení
Avengers	Avengersa	k1gFnPc2	Avengersa
<g/>
:	:	kIx,	:
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Avengers	Avengersa	k1gFnPc2	Avengersa
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
Annual	Annual	k1gInSc1	Annual
'	'	kIx"	'
<g/>
98	[number]	k4	98
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
Captain	Captain	k2eAgMnSc1d1	Captain
America	America	k1gMnSc1	America
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
Quicksilver	Quicksilver	k1gInSc1	Quicksilver
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
#	#	kIx~	#
<g />
.	.	kIx.	.
</s>
<s>
<g/>
10	[number]	k4	10
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
Ultimates	Ultimates	k1gInSc1	Ultimates
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
-	-	kIx~	-
Nadčlověk	nadčlověk	k1gMnSc1	nadčlověk
(	(	kIx(	(
<g/>
Ultimates	Ultimates	k1gMnSc1	Ultimates
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Ultimates	Ultimates	k1gInSc1	Ultimates
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
-	-	kIx~	-
Národní	národní	k2eAgFnSc1d1	národní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
(	(	kIx(	(
<g/>
Ultimates	Ultimates	k1gInSc1	Ultimates
#	#	kIx~	#
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
Ultimates	Ultimates	k1gInSc1	Ultimates
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
1	[number]	k4	1
-	-	kIx~	-
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
monstra	monstrum	k1gNnPc1	monstrum
(	(	kIx(	(
<g/>
Ultimates	Ultimates	k1gInSc1	Ultimates
2	[number]	k4	2
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Ultimates	Ultimates	k1gInSc1	Ultimates
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
-	-	kIx~	-
Jak	jak	k8xS	jak
ukrást	ukrást	k5eAaPmF	ukrást
Ameriku	Amerika	k1gFnSc4	Amerika
(	(	kIx(	(
<g/>
Ultimates	Ultimates	k1gInSc1	Ultimates
2	[number]	k4	2
#	#	kIx~	#
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
Tajné	tajný	k2eAgFnSc2d1	tajná
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
Marvel	Marvel	k1gInSc1	Marvel
Super	super	k2eAgInSc1d1	super
Heroes	Heroes	k1gInSc1	Heroes
Secret	Secreta	k1gFnPc2	Secreta
Wars	Warsa	k1gFnPc2	Warsa
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
Thor	Thor	k1gInSc1	Thor
#	#	kIx~	#
<g/>
383	[number]	k4	383
<g/>
,	,	kIx,	,
She-Hulk	She-Hulk	k1gInSc1	She-Hulk
#	#	kIx~	#
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
What	What	k1gMnSc1	What
If	If	k1gMnSc1	If
<g/>
?	?	kIx.	?
</s>
<s>
#	#	kIx~	#
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
114	[number]	k4	114
<g/>
)	)	kIx)	)
Tajná	tajný	k2eAgFnSc1d1	tajná
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
Secret	Secret	k1gMnSc1	Secret
War	War	k1gMnSc1	War
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Marvels	Marvelsa	k1gFnPc2	Marvelsa
–	–	k?	–
Zázraky	zázrak	k1gInPc1	zázrak
(	(	kIx(	(
<g/>
Marvels	Marvels	k1gInSc1	Marvels
#	#	kIx~	#
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ultimátní	Ultimátní	k2eAgInSc4d1	Ultimátní
komiksový	komiksový	k2eAgInSc4d1	komiksový
komplet	komplet	k1gInSc4	komplet
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marvel	Marvel	k1gInSc1	Marvel
Comics	comics	k1gInSc4	comics
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Česká	český	k2eAgFnSc1d1	Česká
databáze	databáze	k1gFnSc1	databáze
komiksových	komiksový	k2eAgFnPc2d1	komiksová
postav	postava	k1gFnPc2	postava
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Marvel	Marvel	k1gMnSc1	Marvel
</s>
