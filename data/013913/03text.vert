<s>
Zemarchos	Zemarchos	k1gMnSc1
</s>
<s>
Zemarchos	Zemarchos	k1gInSc1
(	(	kIx(
<g/>
starořecky	starořecky	k6eAd1
Ζ	Ζ	k?
–	–	k?
Zémarchos	Zémarchos	k1gMnSc1
<g/>
,	,	kIx,
Zimarchos	Zimarchos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	kA
n.	n.	kA
l.	l.	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
byzantský	byzantský	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
<g/>
,	,	kIx,
diplomat	diplomat	k1gMnSc1
a	a	k8xC
cestovatel	cestovatel	k1gMnSc1
za	za	k7c2
vlády	vláda	k1gFnSc2
císaře	císař	k1gMnSc2
Justina	Justin	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
569	#num#	k4
byl	být	k5eAaImAgInS
vyslán	vyslat	k5eAaPmNgInS
jako	jako	k8xS,k8xC
posel	posít	k5eAaPmAgInS
k	k	k7c3
vládci	vládce	k1gMnPc7
západních	západní	k2eAgMnPc2d1
Turků	Turek	k1gMnPc2
<g/>
,	,	kIx,
dospěl	dochvít	k5eAaPmAgMnS
do	do	k7c2
Ektagu	Ektag	k1gInSc2
severně	severně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Kuldže	Kuldž	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
přijat	přijmout	k5eAaPmNgMnS
chánem	chán	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výměna	výměna	k1gFnSc1
poselstva	poselstvo	k1gNnSc2
mezi	mezi	k7c7
Turky	Turek	k1gMnPc7
a	a	k8xC
Byzantinci	Byzantinec	k1gMnPc7
měla	mít	k5eAaImAgFnS
za	za	k7c4
cíl	cíl	k1gInSc4
společný	společný	k2eAgInSc4d1
postup	postup	k1gInSc4
proti	proti	k7c3
sásánovské	sásánovský	k2eAgFnSc3d1
Persii	Persie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
okolo	okolo	k7c2
Aralského	aralský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
k	k	k7c3
Volze	Volha	k1gFnSc3
a	a	k8xC
pak	pak	k6eAd1
po	po	k7c6
Donu	Don	k1gInSc6
k	k	k7c3
Černému	černý	k2eAgNnSc3d1
moři	moře	k1gNnSc3
a	a	k8xC
do	do	k7c2
Cařihradu	Cařihrad	k1gInSc2
(	(	kIx(
<g/>
roku	rok	k1gInSc2
571	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
Evropa	Evropa	k1gFnSc1
poprvé	poprvé	k6eAd1
dozvěděla	dozvědět	k5eAaPmAgFnS
o	o	k7c4
existenci	existence	k1gFnSc4
Aralského	aralský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
cesta	cesta	k1gFnSc1
rovněž	rovněž	k9
potvrdila	potvrdit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Kaspické	kaspický	k2eAgNnSc1d1
moře	moře	k1gNnSc1
je	být	k5eAaImIp3nS
uzavřené	uzavřený	k2eAgNnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
teorie	teorie	k1gFnSc1
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
spojení	spojení	k1gNnSc4
s	s	k7c7
oceánem	oceán	k1gInSc7
se	se	k3xPyFc4
udržela	udržet	k5eAaPmAgFnS
ještě	ještě	k9
dlouho	dlouho	k6eAd1
ve	v	k7c6
středověkých	středověký	k2eAgFnPc6d1
kosmografiích	kosmografie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Zemarchosova	Zemarchosův	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
je	být	k5eAaImIp3nS
zachována	zachovat	k5eAaPmNgFnS
u	u	k7c2
Menandra	Menandr	k1gMnSc4
Protektora	protektor	k1gMnSc2
"	"	kIx"
<g/>
De	De	k?
legationibus	legationibus	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Zemarchus	Zemarchus	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ABC	ABC	kA
cestovatelů	cestovatel	k1gMnPc2
<g/>
,	,	kIx,
mořeplavců	mořeplavec	k1gMnPc2
<g/>
,	,	kIx,
objevitelů	objevitel	k1gMnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
.	.	kIx.
288	#num#	k4
s.	s.	k?
S.	S.	kA
283	#num#	k4
<g/>
.	.	kIx.
</s>
