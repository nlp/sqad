<s>
Umocňování	umocňování	k1gNnSc1	umocňování
je	být	k5eAaImIp3nS	být
matematická	matematický	k2eAgFnSc1d1	matematická
operace	operace	k1gFnSc1	operace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
opakované	opakovaný	k2eAgNnSc4d1	opakované
násobení	násobení	k1gNnSc4	násobení
<g/>
.	.	kIx.	.
</s>
<s>
Umocňování	umocňování	k1gNnSc1	umocňování
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
násobení	násobení	k1gNnSc3	násobení
v	v	k7c6	v
podobném	podobný	k2eAgInSc6d1	podobný
vztahu	vztah	k1gInSc6	vztah
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
je	být	k5eAaImIp3nS	být
samo	sám	k3xTgNnSc4	sám
násobení	násobení	k1gNnSc4	násobení
ke	k	k7c3	k
sčítání	sčítání	k1gNnSc3	sčítání
<g/>
.	.	kIx.	.
</s>
<s>
Umocňování	umocňování	k1gNnSc1	umocňování
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
zkrácenému	zkrácený	k2eAgInSc3d1	zkrácený
zápisu	zápis	k1gInSc3	zápis
vícenásobného	vícenásobný	k2eAgNnSc2d1	vícenásobné
násobení	násobení	k1gNnSc2	násobení
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
⋅	⋅	k?	⋅
z	z	k7c2	z
⋅	⋅	k?	⋅
z	z	k7c2	z
⋯	⋯	k?	⋯
z	z	k7c2	z
:	:	kIx,	:
⏟	⏟	k?	⏟
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
-	-	kIx~	-
k	k	k7c3	k
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
́	́	k?	́
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
underbrace	underbrace	k1gFnPc4	underbrace
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
cdots	cdots	k1gInSc1	cdots
z	z	k7c2	z
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
-kr	r	k?	-kr
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
acute	acute	k5eAaPmIp2nP	acute
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
:	:	kIx,	:
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vzorci	vzorec	k1gInSc6	vzorec
se	se	k3xPyFc4	se
z	z	k7c2	z
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
základ	základ	k1gInSc1	základ
mocniny	mocnina	k1gFnSc2	mocnina
(	(	kIx(	(
<g/>
mocněnec	mocněnec	k1gInSc1	mocněnec
<g/>
)	)	kIx)	)
a	a	k8xC	a
n	n	k0	n
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
exponent	exponent	k1gInSc1	exponent
(	(	kIx(	(
<g/>
mocnitel	mocnitel	k1gInSc1	mocnitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
n-tá	nat	k5eAaBmIp3nS	n-tat
mocnina	mocnina	k1gFnSc1	mocnina
čísla	číslo	k1gNnSc2	číslo
z	z	k7c2	z
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
z	z	k7c2	z
na	na	k7c6	na
n-tou	nou	k6eAd1	n-tou
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
3	[number]	k4	3
·	·	k?	·
3	[number]	k4	3
·	·	k?	·
3	[number]	k4	3
·	·	k?	·
3	[number]	k4	3
=	=	kIx~	=
81	[number]	k4	81
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
tři	tři	k4xCgFnPc4	tři
na	na	k7c6	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zapisujeme	zapisovat	k5eAaImIp1nP	zapisovat
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
<s>
Exponent	exponent	k1gMnSc1	exponent
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obecně	obecně	k6eAd1	obecně
reálné	reálný	k2eAgNnSc1d1	reálné
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
komplexní	komplexní	k2eAgNnSc1d1	komplexní
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
#	#	kIx~	#
<g/>
Definice	definice	k1gFnSc2	definice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
prázdného	prázdný	k2eAgInSc2d1	prázdný
součinu	součin	k1gInSc2	součin
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
<g/>
0	[number]	k4	0
=	=	kIx~	=
1	[number]	k4	1
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
z	z	k7c2	z
≠	≠	k?	≠
0	[number]	k4	0
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
viz	vidět	k5eAaImRp2nS	vidět
#	#	kIx~	#
<g/>
Nula	nula	k1gFnSc1	nula
na	na	k7c4	na
nultou	nultý	k4xOgFnSc4	nultý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nulový	nulový	k2eAgInSc4d1	nulový
základ	základ	k1gInSc4	základ
a	a	k8xC	a
kladný	kladný	k2eAgInSc4d1	kladný
exponent	exponent	k1gInSc4	exponent
(	(	kIx(	(
<g/>
n	n	k0	n
>	>	kIx)	>
0	[number]	k4	0
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
0	[number]	k4	0
<g/>
n	n	k0	n
=	=	kIx~	=
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
z	z	k7c2	z
technických	technický	k2eAgInPc2d1	technický
důvodů	důvod	k1gInPc2	důvod
nelze	lze	k6eNd1	lze
psát	psát	k5eAaImF	psát
exponent	exponent	k1gInSc4	exponent
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zápis	zápis	k1gInSc1	zápis
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
z	z	k7c2	z
<g/>
**	**	k?	**
<g/>
n.	n.	k?	n.
Pomocí	pomocí	k7c2	pomocí
umocňování	umocňování	k1gNnSc2	umocňování
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
několik	několik	k4yIc1	několik
základních	základní	k2eAgFnPc2d1	základní
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
posloupností	posloupnost	k1gFnPc2	posloupnost
<g/>
:	:	kIx,	:
Mocninná	mocninný	k2eAgFnSc1d1	mocninná
funkce	funkce	k1gFnSc1	funkce
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
a	a	k8xC	a
·	·	k?	·
xn	xn	k?	xn
<g/>
,	,	kIx,	,
exponenciální	exponenciální	k2eAgFnSc1d1	exponenciální
funkce	funkce	k1gFnSc1	funkce
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
zx	zx	k?	zx
<g/>
,	,	kIx,	,
geometrická	geometrický	k2eAgFnSc1d1	geometrická
posloupnost	posloupnost	k1gFnSc1	posloupnost
an	an	k?	an
=	=	kIx~	=
zn	zn	kA	zn
a	a	k8xC	a
funkce	funkce	k1gFnSc2	funkce
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
=	=	kIx~	=
xx	xx	k?	xx
<g/>
.	.	kIx.	.
</s>
<s>
Inverzní	inverzní	k2eAgFnSc1d1	inverzní
operace	operace	k1gFnSc1	operace
k	k	k7c3	k
umocňování	umocňování	k1gNnSc3	umocňování
je	být	k5eAaImIp3nS	být
odmocňování	odmocňování	k1gNnSc4	odmocňování
<g/>
.	.	kIx.	.
</s>
<s>
Mocnina	mocnina	k1gFnSc1	mocnina
s	s	k7c7	s
přirozeným	přirozený	k2eAgInSc7d1	přirozený
exponentem	exponent	k1gInSc7	exponent
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
∈	∈	k?	∈
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
}	}	kIx)	}
)	)	kIx)	)
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
opakované	opakovaný	k2eAgNnSc1d1	opakované
násobení	násobení	k1gNnSc1	násobení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
rekurentně	rekurentně	k6eAd1	rekurentně
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
z	z	k7c2	z
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
⋅	⋅	k?	⋅
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
z	z	k7c2	z
<g/>
}	}	kIx)	}
:	:	kIx,	:
Rekurentní	rekurentní	k2eAgInSc1d1	rekurentní
vzorec	vzorec	k1gInSc1	vzorec
lze	lze	k6eAd1	lze
obrátit	obrátit	k5eAaPmF	obrátit
a	a	k8xC	a
tak	tak	k6eAd1	tak
při	při	k7c6	při
<g />
.	.	kIx.	.
</s>
<s hack="1">
nenulovém	nulový	k2eNgInSc6d1	nenulový
základu	základ	k1gInSc6	základ
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
)	)	kIx)	)
tuto	tento	k3xDgFnSc4	tento
definici	definice	k1gFnSc4	definice
použít	použít	k5eAaPmF	použít
i	i	k9	i
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
celé	celý	k2eAgInPc4d1	celý
exponenty	exponent	k1gInPc4	exponent
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
∈	∈	k?	∈
:	:	kIx,	:
Z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Z	z	k7c2	z
<g/>
}	}	kIx)	}
}	}	kIx)	}
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
z	z	k7c2	z
<g/>
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
−	−	k?	−
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-n	-n	k?	-n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Definici	definice	k1gFnSc4	definice
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
zobecnit	zobecnit	k5eAaPmF	zobecnit
pro	pro	k7c4	pro
racionální	racionální	k2eAgInSc4d1	racionální
exponent	exponent	k1gInSc4	exponent
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
odmocňování	odmocňování	k1gNnSc2	odmocňování
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
\	\	kIx~	\
<g/>
over	over	k1gInSc4	over
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Zobecnění	zobecnění	k1gNnSc1	zobecnění
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
obor	obor	k1gInSc4	obor
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
rozšíření	rozšíření	k1gNnSc4	rozšíření
definice	definice	k1gFnSc2	definice
o	o	k7c4	o
mocniny	mocnina	k1gFnPc4	mocnina
s	s	k7c7	s
iracionálními	iracionální	k2eAgInPc7d1	iracionální
exponenty	exponent	k1gInPc7	exponent
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
dodefinováním	dodefinování	k1gNnSc7	dodefinování
pomocí	pomoc	k1gFnPc2	pomoc
<g />
.	.	kIx.	.
</s>
<s hack="1">
limity	limit	k1gInPc4	limit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
n	n	k0	n
\	\	kIx~	\
<g/>
atop	atopit	k5eAaPmRp2nS	atopit
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
Q	Q	kA	Q
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Pro	pro	k7c4	pro
mocniny	mocnina	k1gFnPc4	mocnina
<g />
.	.	kIx.	.
</s>
<s hack="1">
s	s	k7c7	s
komplexním	komplexní	k2eAgInSc7d1	komplexní
základem	základ	k1gInSc7	základ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
i	i	k9	i
=	=	kIx~	=
r	r	kA	r
⋅	⋅	k?	⋅
(	(	kIx(	(
cos	cos	kA	cos
:	:	kIx,	:
φ	φ	k?	φ
+	+	kIx~	+
i	i	k8xC	i
sin	sin	kA	sin
:	:	kIx,	:
φ	φ	k?	φ
)	)	kIx)	)
=	=	kIx~	=
r	r	kA	r
⋅	⋅	k?	⋅
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
i	i	k9	i
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
bi	bi	k?	bi
<g/>
=	=	kIx~	=
<g/>
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
+	+	kIx~	+
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
varphi	varph	k1gMnSc6	varph
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
φ	φ	k?	φ
∈	∈	k?	∈
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
∈	∈	k?	∈
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
pak	pak	k6eAd1	pak
<g />
.	.	kIx.	.
</s>
<s hack="1">
platí	platit	k5eAaImIp3nS	platit
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Moivrovu	Moivrův	k2eAgFnSc4d1	Moivrův
větu	věta	k1gFnSc4	věta
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
≡	≡	k?	≡
(	(	kIx(	(
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
i	i	k9	i
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
r	r	kA	r
⋅	⋅	k?	⋅
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
i	i	k9	i
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
i	i	k8xC	i
:	:	kIx,	:
n	n	k0	n
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
[	[	kIx(	[
cos	cos	kA	cos
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
φ	φ	k?	φ
)	)	kIx)	)
+	+	kIx~	+
i	i	k9	i
sin	sin	kA	sin
:	:	kIx,	:
(	(	kIx(	(
n	n	k0	n
φ	φ	k?	φ
)	)	kIx)	)
]	]	kIx)	]
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
bi	bi	k?	bi
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnPc4	varphi
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
varphi	varph	k1gFnSc6	varph
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
varphi	varph	k1gFnSc6	varph
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Argument	argument	k1gInSc1	argument
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
=	=	kIx~	=
Arg	Arg	k1gMnSc1	Arg
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varphi	varph	k1gFnSc6	varph
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
Arg	Arg	k1gMnPc2	Arg
<g/>
}	}	kIx)	}
z	z	k7c2	z
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
nutně	nutně	k6eAd1	nutně
skok	skok	k1gInSc4	skok
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
polohu	poloha	k1gFnSc4	poloha
však	však	k9	však
lze	lze	k6eAd1	lze
zvolit	zvolit	k5eAaPmF	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Volí	volit	k5eAaImIp3nS	volit
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnPc6	varphi
}	}	kIx)	}
z	z	k7c2	z
intervalu	interval	k1gInSc2	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
0	[number]	k4	0
;	;	kIx,	;
2	[number]	k4	2
π	π	k?	π
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
0	[number]	k4	0
<g/>
;	;	kIx,	;
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
)	)	kIx)	)
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
π	π	k?	π
;	;	kIx,	;
π	π	k?	π
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
rangle	rangle	k1gNnPc6	rangle
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Komplexní	komplexní	k2eAgFnSc1d1	komplexní
mocnina	mocnina	k1gFnSc1	mocnina
s	s	k7c7	s
neceločíselným	celočíselný	k2eNgInSc7d1	celočíselný
exponentem	exponent	k1gInSc7	exponent
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
obecně	obecně	k6eAd1	obecně
mnohoznačná	mnohoznačný	k2eAgFnSc1d1	mnohoznačná
funkce	funkce	k1gFnSc1	funkce
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
holomorfní	holomorfní	k2eAgFnSc6d1	holomorfní
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
komplexním	komplexní	k2eAgNnSc7d1	komplexní
číslem	číslo	k1gNnSc7	číslo
i	i	k9	i
exponent	exponent	k1gInSc4	exponent
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
mocnina	mocnina	k1gFnSc1	mocnina
dána	dát	k5eAaPmNgFnS	dát
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
n	n	k0	n
ln	ln	k?	ln
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
i	i	k8xC	i
φ	φ	k?	φ
+	+	kIx~	+
ln	ln	k?	ln
:	:	kIx,	:
r	r	kA	r
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
z	z	k7c2	z
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Užitečná	užitečný	k2eAgFnSc1d1	užitečná
definice	definice	k1gFnSc1	definice
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
množiny	množina	k1gFnPc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
,	,	kIx,	,
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
{	{	kIx(	{
f	f	k?	f
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
B	B	kA	B
→	→	k?	→
A	A	kA	A
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
:	:	kIx,	:
<g/>
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
A	A	kA	A
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
čili	čili	k8xC	čili
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgNnPc2	všecek
zobrazení	zobrazení	k1gNnPc2	zobrazení
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
}	}	kIx)	}
do	do	k7c2	do
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
,	,	kIx,	,
tedy	tedy	k8xC	tedy
takových	takový	k3xDgNnPc2	takový
zobrazení	zobrazení	k1gNnPc2	zobrazení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
každému	každý	k3xTgMnSc3	každý
prvku	prvek	k1gInSc2	prvek
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
}	}	kIx)	}
přiřazují	přiřazovat	k5eAaImIp3nP	přiřazovat
právě	právě	k6eAd1	právě
jeden	jeden	k4xCgInSc4	jeden
prvek	prvek	k1gInSc4	prvek
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
obě	dva	k4xCgFnPc1	dva
množiny	množina	k1gFnPc1	množina
konečné	konečná	k1gFnSc2	konečná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
počet	počet	k1gInSc1	počet
takových	takový	k3xDgNnPc2	takový
zobrazení	zobrazení	k1gNnPc2	zobrazení
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
|	|	kIx~	|
<g/>
A	A	kA	A
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
A	a	k9	a
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
B	B	kA	B
<g/>
|	|	kIx~	|
<g/>
}}	}}	k?	}}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
klademe	klást	k5eAaImIp1nP	klást
00	[number]	k4	00
=	=	kIx~	=
1	[number]	k4	1
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
#	#	kIx~	#
<g/>
Nula	nula	k1gFnSc1	nula
na	na	k7c4	na
nultou	nultý	k4xOgFnSc4	nultý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
{	{	kIx(	{
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
a	a	k8xC	a
↦	↦	k?	↦
0	[number]	k4	0
;	;	kIx,	;
b	b	k?	b
↦	↦	k?	↦
0	[number]	k4	0
}	}	kIx)	}
,	,	kIx,	,
{	{	kIx(	{
a	a	k8xC	a
↦	↦	k?	↦
0	[number]	k4	0
;	;	kIx,	;
b	b	k?	b
↦	↦	k?	↦
1	[number]	k4	1
}	}	kIx)	}
,	,	kIx,	,
{	{	kIx(	{
a	a	k8xC	a
↦	↦	k?	↦
1	[number]	k4	1
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s hack="1">
b	b	k?	b
↦	↦	k?	↦
0	[number]	k4	0
}	}	kIx)	}
,	,	kIx,	,
{	{	kIx(	{
a	a	k8xC	a
↦	↦	k?	↦
1	[number]	k4	1
;	;	kIx,	;
b	b	k?	b
↦	↦	k?	↦
1	[number]	k4	1
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
mapsto	mapsto	k6eAd1	mapsto
0	[number]	k4	0
<g/>
;	;	kIx,	;
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
mapsto	mapsto	k6eAd1	mapsto
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
mapsto	mapsto	k6eAd1	mapsto
0	[number]	k4	0
<g/>
;	;	kIx,	;
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
mapsto	mapsto	k6eAd1	mapsto
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
mapsto	mapsto	k6eAd1	mapsto
1	[number]	k4	1
<g/>
;	;	kIx,	;
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
mapsto	mapsto	k6eAd1	mapsto
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
mapsto	mapsto	k6eAd1	mapsto
1	[number]	k4	1
<g/>
;	;	kIx,	;
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
mapsto	mapsto	k6eAd1	mapsto
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
{	{	kIx(	{
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
{	{	kIx(	{
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
{	{	kIx(	{
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
{	{	kIx(	{
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
}	}	kIx)	}
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
:	:	kIx,	:
Mocninu	mocnina	k1gFnSc4	mocnina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
s	s	k7c7	s
nezáporným	záporný	k2eNgInSc7d1	nezáporný
celým	celý	k2eAgInSc7d1	celý
základem	základ	k1gInSc7	základ
i	i	k9	i
exponentem	exponent	k1gInSc7	exponent
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
,	,	kIx,	,
n	n	k0	n
∈	∈	k?	∈
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
,	,	kIx,	,
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
)	)	kIx)	)
lze	lze	k6eAd1	lze
také	také	k9	také
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
počet	počet	k1gInSc1	počet
všech	všecek	k3xTgFnPc2	všecek
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tic	ic	k1gFnSc4	-tic
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
}	}	kIx)	}
-prvkové	rvkový	k2eAgFnSc2d1	-prvkový
množiny	množina	k1gFnSc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyjádření	vyjádření	k1gNnSc1	vyjádření
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
předchozí	předchozí	k2eAgFnSc4d1	předchozí
definici	definice	k1gFnSc4	definice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zobrazení	zobrazení	k1gNnSc1	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
-prvkové	rvkový	k2eAgFnSc2d1	-prvkový
množiny	množina	k1gFnSc2	množina
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k9	jako
uspořádanou	uspořádaný	k2eAgFnSc4d1	uspořádaná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tici	iec	k1gInSc3	-tiec
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
{	{	kIx(	{
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
0	[number]	k4	0
,	,	kIx,	,
0	[number]	k4	0
,	,	kIx,	,
0	[number]	k4	0
)	)	kIx)	)
,	,	kIx,	,
(	(	kIx(	(
0	[number]	k4	0
,	,	kIx,	,
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
0	[number]	k4	0
)	)	kIx)	)
,	,	kIx,	,
(	(	kIx(	(
1	[number]	k4	1
,	,	kIx,	,
0	[number]	k4	0
,	,	kIx,	,
0	[number]	k4	0
)	)	kIx)	)
,	,	kIx,	,
(	(	kIx(	(
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
,	,	kIx,	,
(	(	kIx(	(
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
0	[number]	k4	0
)	)	kIx)	)
,	,	kIx,	,
(	(	kIx(	(
1	[number]	k4	1
,	,	kIx,	,
0	[number]	k4	0
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
,	,	kIx,	,
(	(	kIx(	(
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
=	=	kIx~	=
8	[number]	k4	8
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
0,1	[number]	k4	0,1
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
left	left	k1gInSc1	left
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
0,0	[number]	k4	0,0
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
0,0	[number]	k4	0,0
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
1,0	[number]	k4	1,0
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
1,1	[number]	k4	1,1
<g/>
,0	,0	k4	,0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
1,0	[number]	k4	1,0
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
1,1	[number]	k4	1,1
<g/>
,1	,1	k4	,1
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
8	[number]	k4	8
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pro	pro	k7c4	pro
reálná	reálný	k2eAgNnPc4d1	reálné
nebo	nebo	k8xC	nebo
komplexní	komplexní	k2eAgNnPc4d1	komplexní
čísla	číslo	k1gNnPc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
následující	následující	k2eAgInPc4d1	následující
vztahy	vztah	k1gInPc4	vztah
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
výrazy	výraz	k1gInPc1	výraz
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
definované	definovaný	k2eAgNnSc1d1	definované
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
b	b	k?	b
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
(	(	kIx(	(
<g/>
ab	ab	k?	ab
<g/>
\	\	kIx~	\
<g/>
right	right	k1gMnSc1	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s hack="1">
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Arg	Arg	k1gFnSc1	Arg
:	:	kIx,	:
a	a	k8xC	a
+	+	kIx~	+
Arg	Arg	k1gFnSc1	Arg
:	:	kIx,	:
b	b	k?	b
∈	∈	k?	∈
(	(	kIx(	(
−	−	k?	−
π	π	k?	π
;	;	kIx,	;
π	π	k?	π
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
Arg	Arg	k1gFnSc1	Arg
<g/>
}	}	kIx)	}
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
Arg	Arg	k1gFnSc6	Arg
<g/>
}	}	kIx)	}
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
rangle	rangle	k1gNnPc3	rangle
}	}	kIx)	}
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
se	se	k3xPyFc4	se
neprojeví	projevit	k5eNaPmIp3nS	projevit
skok	skok	k1gInSc1	skok
argumentu	argument	k1gInSc2	argument
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
}}}}	}}}}	k?	}}}}
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Arg	Arg	k1gFnSc1	Arg
:	:	kIx,	:
a	a	k8xC	a
−	−	k?	−
Arg	Arg	k1gFnSc1	Arg
:	:	kIx,	:
b	b	k?	b
∈	∈	k?	∈
(	(	kIx(	(
−	−	k?	−
π	π	k?	π
;	;	kIx,	;
π	π	k?	π
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
Arg	Arg	k1gFnSc6	Arg
<g/>
}	}	kIx)	}
a-	a-	k?	a-
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
Arg	Arg	k1gFnSc6	Arg
<g/>
}	}	kIx)	}
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
rangle	rangla	k1gFnSc6	rangla
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
−	−	k?	−
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
-x	-x	k?	-x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
−	−	k?	−
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x-y	x	k?	x-y
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
x	x	k?	x
⋅	⋅	k?	⋅
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	leftum	k1gNnPc2	leftum
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
y	y	k?	y
<g/>
}}	}}	k?	}}
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Im	Im	k1gFnSc1	Im
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
ln	ln	k?	ln
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
)	)	kIx)	)
∈	∈	k?	∈
(	(	kIx(	(
−	−	k?	−
π	π	k?	π
;	;	kIx,	;
π	π	k?	π
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
Im	Im	k1gFnSc6	Im
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
ln	ln	k?	ln
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
pi	pi	k0	pi
\	\	kIx~	\
<g/>
rangle	rangla	k1gFnSc6	rangla
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
00	[number]	k4	00
viz	vidět	k5eAaImRp2nS	vidět
níže	níže	k1gFnSc1	níže
<g/>
)	)	kIx)	)
Umocňování	umocňování	k1gNnSc1	umocňování
není	být	k5eNaImIp3nS	být
obecně	obecně	k6eAd1	obecně
komutativní	komutativní	k2eAgFnSc1d1	komutativní
(	(	kIx(	(
<g/>
23	[number]	k4	23
≠	≠	k?	≠
32	[number]	k4	32
<g/>
)	)	kIx)	)
ani	ani	k8xC	ani
asociativní	asociativní	k2eAgMnSc1d1	asociativní
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
≠	≠	k?	≠
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nula	nula	k1gFnSc1	nula
umocněná	umocněný	k2eAgFnSc1d1	umocněná
na	na	k7c4	na
kladné	kladný	k2eAgNnSc4d1	kladné
číslo	číslo	k1gNnSc4	číslo
je	být	k5eAaImIp3nS	být
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
x	x	k?	x
>	>	kIx)	>
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
0	[number]	k4	0
<g/>
x	x	k?	x
=	=	kIx~	=
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
nula	nula	k1gFnSc1	nula
umocněná	umocněný	k2eAgFnSc1d1	umocněná
na	na	k7c4	na
záporné	záporný	k2eAgNnSc4d1	záporné
číslo	číslo	k1gNnSc4	číslo
není	být	k5eNaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
takový	takový	k3xDgInSc1	takový
výraz	výraz	k1gInSc1	výraz
vede	vést	k5eAaImIp3nS	vést
na	na	k7c6	na
dělení	dělení	k1gNnSc6	dělení
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
reálných	reálný	k2eAgNnPc2d1	reálné
ani	ani	k8xC	ani
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
definováno	definovat	k5eAaBmNgNnS	definovat
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
x	x	k?	x
>	>	kIx)	>
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
−	−	k?	−
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
0	[number]	k4	0
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-x	-x	k?	-x
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
0	[number]	k4	0
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Zcela	zcela	k6eAd1	zcela
obecně	obecně	k6eAd1	obecně
není	být	k5eNaImIp3nS	být
výraz	výraz	k1gInSc4	výraz
00	[number]	k4	00
definován	definovat	k5eAaBmNgInS	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Limita	limita	k1gFnSc1	limita
mocniny	mocnina	k1gFnSc2	mocnina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
základ	základ	k1gInSc4	základ
i	i	k8xC	i
exponent	exponent	k1gInSc4	exponent
konvergují	konvergovat	k5eAaImIp3nP	konvergovat
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
tzv.	tzv.	kA	tzv.
neurčitý	určitý	k2eNgInSc4d1	neurčitý
výraz	výraz	k1gInSc4	výraz
a	a	k8xC	a
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
vyčíslení	vyčíslení	k1gNnSc4	vyčíslení
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
znát	znát	k5eAaImF	znát
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
základem	základ	k1gInSc7	základ
a	a	k8xC	a
exponentem	exponent	k1gInSc7	exponent
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výraz	výraz	k1gInSc4	výraz
00	[number]	k4	00
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
lze	lze	k6eAd1	lze
dívat	dívat	k5eAaImF	dívat
dvěma	dva	k4xCgInPc7	dva
základními	základní	k2eAgInPc7d1	základní
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
hledí	hledět	k5eAaImIp3nS	hledět
jako	jako	k9	jako
na	na	k7c6	na
limitu	limit	k1gInSc6	limit
funkce	funkce	k1gFnSc2	funkce
x	x	k?	x
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
všude	všude	k6eAd1	všude
kromě	kromě	k7c2	kromě
nuly	nula	k1gFnSc2	nula
rovna	roven	k2eAgFnSc1d1	rovna
jedné	jeden	k4xCgFnSc6	jeden
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
nule	nula	k1gFnSc6	nula
dodefinovat	dodefinovat	k5eAaPmF	dodefinovat
stejně	stejně	k6eAd1	stejně
a	a	k8xC	a
klade	klást	k5eAaImIp3nS	klást
se	s	k7c7	s
00	[number]	k4	00
=	=	kIx~	=
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
druhý	druhý	k4xOgInSc4	druhý
pohled	pohled	k1gInSc4	pohled
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
0	[number]	k4	0
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
kladná	kladný	k2eAgNnPc4d1	kladné
x	x	k?	x
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
nule	nula	k1gFnSc6	nula
dodefinuje	dodefinovat	k5eAaPmIp3nS	dodefinovat
00	[number]	k4	00
=	=	kIx~	=
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
situacích	situace	k1gFnPc6	situace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
hlavně	hlavně	k9	hlavně
první	první	k4xOgFnSc1	první
definice	definice	k1gFnSc1	definice
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
=	=	kIx~	=
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vyžadována	vyžadovat	k5eAaImNgFnS	vyžadovat
pro	pro	k7c4	pro
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
zápis	zápis	k1gInSc4	zápis
mnoha	mnoho	k4c2	mnoho
vzorců	vzorec	k1gInPc2	vzorec
<g/>
:	:	kIx,	:
Aby	aby	kYmCp3nS	aby
při	při	k7c6	při
zápisu	zápis	k1gInSc6	zápis
polynomu	polynom	k1gInSc2	polynom
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}}	}}}	k?	}}}
platilo	platit	k5eAaImAgNnS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
(	(	kIx(	(
0	[number]	k4	0
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
00	[number]	k4	00
=	=	kIx~	=
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
zápis	zápis	k1gInSc1	zápis
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
pro	pro	k7c4	pro
mocninnou	mocninný	k2eAgFnSc4d1	mocninná
řadu	řada	k1gFnSc4	řada
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
platnost	platnost	k1gFnSc1	platnost
binomické	binomický	k2eAgFnSc2d1	binomická
věty	věta	k1gFnSc2	věta
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
00	[number]	k4	00
=	=	kIx~	=
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Existuje	existovat	k5eAaImIp3nS	existovat
právě	právě	k9	právě
jedno	jeden	k4xCgNnSc1	jeden
zobrazení	zobrazení	k1gNnSc1	zobrazení
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
do	do	k7c2	do
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prázdné	prázdný	k2eAgNnSc1d1	prázdné
zobrazení	zobrazení	k1gNnSc1	zobrazení
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
#	#	kIx~	#
<g/>
Alternativní	alternativní	k2eAgFnPc1d1	alternativní
definice	definice	k1gFnPc1	definice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidlo	pravidlo	k1gNnSc1	pravidlo
pro	pro	k7c4	pro
derivování	derivování	k1gNnSc4	derivování
mocninné	mocninný	k2eAgFnSc2d1	mocninná
funkce	funkce	k1gFnSc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
n	n	k0	n
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tfrac	tfrac	k1gFnSc1	tfrac
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
nx	nx	k?	nx
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
x	x	k?	x
=	=	kIx~	=
0	[number]	k4	0
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
00	[number]	k4	00
=	=	kIx~	=
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
00	[number]	k4	00
ponecháno	ponechat	k5eAaPmNgNnS	ponechat
nedefinované	definovaný	k2eNgFnSc2d1	nedefinovaná
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
výjimečně	výjimečně	k6eAd1	výjimečně
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
druhé	druhý	k4xOgFnSc2	druhý
definice	definice	k1gFnSc2	definice
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
=	=	kIx~	=
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
každodenním	každodenní	k2eAgInSc6d1	každodenní
životě	život	k1gInSc6	život
často	často	k6eAd1	často
používáme	používat	k5eAaImIp1nP	používat
mocniny	mocnina	k1gFnPc1	mocnina
o	o	k7c6	o
základu	základ	k1gInSc6	základ
deset	deset	k4xCc4	deset
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
1	[number]	k4	1
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
mocniny	mocnina	k1gFnPc1	mocnina
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
naší	náš	k3xOp1gFnSc2	náš
desítkové	desítkový	k2eAgFnSc2d1	desítková
číselné	číselný	k2eAgFnSc2d1	číselná
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
předpony	předpona	k1gFnPc4	předpona
násobků	násobek	k1gInPc2	násobek
jednotek	jednotka	k1gFnPc2	jednotka
označením	označení	k1gNnSc7	označení
mocnin	mocnina	k1gFnPc2	mocnina
deseti	deset	k4xCc7	deset
–	–	k?	–
1	[number]	k4	1
kg	kg	kA	kg
=	=	kIx~	=
10	[number]	k4	10
<g/>
3	[number]	k4	3
g	g	kA	g
apod.	apod.	kA	apod.
Velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
využití	využití	k1gNnSc4	využití
druhé	druhý	k4xOgFnSc2	druhý
mocniny	mocnina	k1gFnSc2	mocnina
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vynásobení	vynásobení	k1gNnSc4	vynásobení
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
mocnina	mocnina	k1gFnSc1	mocnina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
čtverec	čtverec	k1gInSc1	čtverec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsah	obsah	k1gInSc1	obsah
čtverce	čtverec	k1gInSc2	čtverec
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
druhé	druhý	k4xOgFnSc6	druhý
mocnině	mocnina	k1gFnSc6	mocnina
délky	délka	k1gFnSc2	délka
jeho	jeho	k3xOp3gFnSc2	jeho
hrany	hrana	k1gFnSc2	hrana
(	(	kIx(	(
<g/>
S	s	k7c7	s
=	=	kIx~	=
a	a	k8xC	a
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počítače	počítač	k1gInPc1	počítač
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
dat	datum	k1gNnPc2	datum
používají	používat	k5eAaImIp3nP	používat
dvojkovou	dvojkový	k2eAgFnSc4d1	dvojková
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
mocninách	mocnina	k1gFnPc6	mocnina
čísla	číslo	k1gNnSc2	číslo
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
používají	používat	k5eAaImIp3nP	používat
násobky	násobek	k1gInPc4	násobek
jednotek	jednotka	k1gFnPc2	jednotka
jako	jako	k8xS	jako
mocniny	mocnina	k1gFnSc2	mocnina
o	o	k7c6	o
základu	základ	k1gInSc6	základ
2	[number]	k4	2
–	–	k?	–
1	[number]	k4	1
KiB	KiB	k1gFnPc2	KiB
=	=	kIx~	=
210	[number]	k4	210
B	B	kA	B
=	=	kIx~	=
1024	[number]	k4	1024
B.	B.	kA	B.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
binární	binární	k2eAgFnPc1d1	binární
předpony	předpona	k1gFnPc1	předpona
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
jsou	být	k5eAaImIp3nP	být
zvlášť	zvlášť	k6eAd1	zvlášť
důležité	důležitý	k2eAgFnPc1d1	důležitá
mocniny	mocnina	k1gFnPc1	mocnina
o	o	k7c6	o
základu	základ	k1gInSc6	základ
e	e	k0	e
≅	≅	k?	≅
2,718	[number]	k4	2,718
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
Eulerova	Eulerův	k2eAgNnSc2d1	Eulerovo
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Odmocnina	odmocnina	k1gFnSc1	odmocnina
Logaritmus	logaritmus	k1gInSc1	logaritmus
Mocninná	mocninný	k2eAgFnSc1d1	mocninná
funkce	funkce	k1gFnSc1	funkce
Exponenciální	exponenciální	k2eAgFnSc2d1	exponenciální
funkce	funkce	k1gFnSc2	funkce
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
řada	řada	k1gFnSc1	řada
Kořen	kořen	k1gInSc1	kořen
(	(	kIx(	(
<g/>
matematika	matematika	k1gFnSc1	matematika
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
umocňování	umocňování	k1gNnSc2	umocňování
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mocnina	mocnina	k1gFnSc1	mocnina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
