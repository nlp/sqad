<s>
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Cristoforo	Cristofora	k1gFnSc5	Cristofora
Colombo	Colomba	k1gFnSc5	Colomba
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Christophorus	Christophorus	k1gMnSc1	Christophorus
Columbus	Columbus	k1gMnSc1	Columbus
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
Cristóbal	Cristóbal	k1gInSc1	Cristóbal
Colón	colón	k1gInSc1	colón
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
Cristóvã	Cristóvã	k1gMnSc5	Cristóvã
Colombo	Colomba	k1gMnSc5	Colomba
<g/>
,	,	kIx,	,
asi	asi	k9	asi
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1451	[number]	k4	1451
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Janov	Janov	k1gInSc1	Janov
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1506	[number]	k4	1506
<g/>
,	,	kIx,	,
Valladolid	Valladolid	k1gInSc1	Valladolid
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
janovský	janovský	k2eAgMnSc1d1	janovský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
a	a	k8xC	a
kolonizátor	kolonizátor	k1gMnSc1	kolonizátor
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
Janovské	Janovské	k2eAgFnSc6d1	Janovské
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Katolických	katolický	k2eAgMnPc2d1	katolický
králů	král	k1gMnPc2	král
Španělska	Španělsko	k1gNnSc2	Španělsko
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
čtyři	čtyři	k4xCgFnPc4	čtyři
plavby	plavba	k1gFnPc1	plavba
přes	přes	k7c4	přes
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
probudily	probudit	k5eAaPmAgFnP	probudit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
kontinent	kontinent	k1gInSc4	kontinent
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
výpravy	výprava	k1gFnPc1	výprava
a	a	k8xC	a
Kolumbovy	Kolumbův	k2eAgFnPc1d1	Kolumbova
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
trvalých	trvalý	k2eAgNnPc2d1	trvalé
sídel	sídlo	k1gNnPc2	sídlo
na	na	k7c6	na
Hispaniole	Hispaniola	k1gFnSc6	Hispaniola
zahájily	zahájit	k5eAaPmAgInP	zahájit
španělskou	španělský	k2eAgFnSc4d1	španělská
kolonizaci	kolonizace	k1gFnSc4	kolonizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
soutěž	soutěž	k1gFnSc4	soutěž
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
o	o	k7c6	o
kolonizaci	kolonizace	k1gFnSc6	kolonizace
Jižní	jižní	k2eAgFnSc2d1	jižní
i	i	k8xC	i
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začínala	začínat	k5eAaImAgFnS	začínat
evropská	evropský	k2eAgFnSc1d1	Evropská
světovláda	světovláda	k1gFnSc1	světovláda
i	i	k8xC	i
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
soutěž	soutěž	k1gFnSc1	soutěž
mezi	mezi	k7c7	mezi
evropskými	evropský	k2eAgNnPc7d1	Evropské
královstvími	království	k1gNnPc7	království
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
zbohatnout	zbohatnout	k5eAaPmF	zbohatnout
zakládáním	zakládání	k1gNnSc7	zakládání
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
Kolumbův	Kolumbův	k2eAgInSc4d1	Kolumbův
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
dosažení	dosažení	k1gNnSc4	dosažení
Asie	Asie	k1gFnSc2	Asie
obeplutím	obeplutí	k1gNnSc7	obeplutí
Země	zem	k1gFnSc2	zem
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
podporu	podpor	k1gInSc2	podpor
u	u	k7c2	u
španělského	španělský	k2eAgInSc2d1	španělský
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
viděl	vidět	k5eAaImAgMnS	vidět
naději	naděje	k1gFnSc4	naděje
pro	pro	k7c4	pro
obchody	obchod	k1gInPc4	obchod
s	s	k7c7	s
kořením	koření	k1gNnSc7	koření
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
místo	místo	k7c2	místo
plánovaného	plánovaný	k2eAgNnSc2d1	plánované
Japonska	Japonsko	k1gNnSc2	Japonsko
břehů	břeh	k1gInPc2	břeh
Bahamských	bahamský	k2eAgInPc2d1	bahamský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
San	San	k1gMnSc1	San
Salvador	Salvador	k1gMnSc1	Salvador
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tří	tři	k4xCgFnPc2	tři
dalších	další	k2eAgFnPc2d1	další
výprav	výprava	k1gFnPc2	výprava
navštívil	navštívit	k5eAaPmAgInS	navštívit
Velké	velká	k1gFnPc4	velká
i	i	k8xC	i
Malé	Malé	k2eAgFnPc1d1	Malé
Antily	Antily	k1gFnPc1	Antily
<g/>
,	,	kIx,	,
pobřeží	pobřeží	k1gNnSc1	pobřeží
Venezuely	Venezuela	k1gFnSc2	Venezuela
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
zabral	zabrat	k5eAaPmAgMnS	zabrat
pro	pro	k7c4	pro
Španělské	španělský	k2eAgNnSc4d1	španělské
impérium	impérium	k1gNnSc4	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
nebyl	být	k5eNaImAgMnS	být
prvním	první	k4xOgMnSc7	první
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Ameriku	Amerika	k1gFnSc4	Amerika
–	–	k?	–
o	o	k7c4	o
pět	pět	k4xCc4	pět
století	století	k1gNnPc2	století
dříve	dříve	k6eAd2	dříve
jejích	její	k3xOp3gInPc2	její
břehů	břeh	k1gInPc2	břeh
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
norská	norský	k2eAgFnSc1d1	norská
expedice	expedice	k1gFnSc1	expedice
vedená	vedený	k2eAgFnSc1d1	vedená
Leifem	Leif	k1gInSc7	Leif
Erikssonem	Eriksson	k1gInSc7	Eriksson
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
založila	založit	k5eAaPmAgFnS	založit
kolonii	kolonie	k1gFnSc4	kolonie
na	na	k7c6	na
dnešním	dnešní	k2eAgInSc6d1	dnešní
Newfoundlandu	Newfoundland	k1gInSc6	Newfoundland
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
teprve	teprve	k6eAd1	teprve
Kolumbovy	Kolumbův	k2eAgFnPc1d1	Kolumbova
cesty	cesta	k1gFnPc1	cesta
zahájily	zahájit	k5eAaPmAgFnP	zahájit
trvalé	trvalý	k2eAgInPc4d1	trvalý
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Amerikou	Amerika	k1gFnSc7	Amerika
a	a	k8xC	a
podnítily	podnítit	k5eAaPmAgFnP	podnítit
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
evropské	evropský	k2eAgFnSc2d1	Evropská
kolonizace	kolonizace	k1gFnSc2	kolonizace
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
moderního	moderní	k2eAgInSc2d1	moderní
Západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
viděl	vidět	k5eAaImAgMnS	vidět
svůj	svůj	k3xOyFgInSc4	svůj
úspěch	úspěch	k1gInSc4	úspěch
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
šíření	šíření	k1gNnSc6	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgMnS	objevit
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
nový	nový	k2eAgInSc4d1	nový
kontinent	kontinent	k1gInSc4	kontinent
a	a	k8xC	a
obyvatele	obyvatel	k1gMnSc2	obyvatel
objevených	objevený	k2eAgFnPc2d1	objevená
zemí	zem	k1gFnPc2	zem
nazval	nazvat	k5eAaPmAgInS	nazvat
Indios	Indios	k1gInSc1	Indios
(	(	kIx(	(
<g/>
španělský	španělský	k2eAgInSc1d1	španělský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
Indy	Ind	k1gMnPc4	Ind
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
název	název	k1gInSc1	název
mohl	moct	k5eAaImAgInS	moct
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
spiritualitu	spiritualita	k1gFnSc4	spiritualita
a	a	k8xC	a
duchovní	duchovní	k2eAgNnSc4d1	duchovní
založení	založení	k1gNnSc4	založení
obyvatel	obyvatel	k1gMnSc1	obyvatel
nového	nový	k2eAgInSc2d1	nový
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tak	tak	k9	tak
podle	podle	k7c2	podle
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
a	a	k8xC	a
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
vírou	víra	k1gFnSc7	víra
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
in	in	k?	in
Deo	Deo	k1gMnSc1	Deo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zhoršení	zhoršení	k1gNnSc1	zhoršení
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
španělským	španělský	k2eAgInSc7d1	španělský
královským	královský	k2eAgInSc7d1	královský
dvorem	dvůr	k1gInSc7	dvůr
a	a	k8xC	a
koloniálními	koloniální	k2eAgMnPc7d1	koloniální
správci	správce	k1gMnPc7	správce
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
Kolumbovu	Kolumbův	k2eAgNnSc3d1	Kolumbovo
zatčení	zatčení	k1gNnSc3	zatčení
a	a	k8xC	a
propuštění	propuštění	k1gNnSc4	propuštění
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
guvernéra	guvernér	k1gMnSc4	guvernér
sídel	sídlo	k1gNnPc2	sídlo
v	v	k7c6	v
Hispaniole	Hispaniol	k1gInSc6	Hispaniol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
vleklým	vleklý	k2eAgInPc3d1	vleklý
sporům	spor	k1gInPc3	spor
o	o	k7c4	o
výhody	výhod	k1gInPc4	výhod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
chtěl	chtít	k5eAaImAgMnS	chtít
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
dědice	dědic	k1gMnPc4	dědic
od	od	k7c2	od
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
jen	jen	k9	jen
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Valladolidu	Valladolid	k1gInSc6	Valladolid
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
ostatky	ostatek	k1gInPc7	ostatek
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
umístěny	umístit	k5eAaPmNgInP	umístit
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
říjnem	říjen	k1gInSc7	říjen
1451	[number]	k4	1451
v	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
<g/>
,	,	kIx,	,
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
historiků	historik	k1gMnPc2	historik
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
v	v	k7c6	v
závěti	závěť	k1gFnSc6	závěť
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
Janovana	Janovan	k1gMnSc4	Janovan
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
Žid	Žid	k1gMnSc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
tkadlec	tkadlec	k1gMnSc1	tkadlec
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
drobný	drobný	k2eAgMnSc1d1	drobný
obchodník	obchodník	k1gMnSc1	obchodník
se	s	k7c7	s
sýry	sýr	k1gInPc7	sýr
v	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1470	[number]	k4	1470
v	v	k7c6	v
Savoně	Savona	k1gFnSc6	Savona
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbova	Kolumbův	k2eAgFnSc1d1	Kolumbova
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
Zuzana	Zuzana	k1gFnSc1	Zuzana
Fontanarossová	Fontanarossová	k1gFnSc1	Fontanarossová
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
,	,	kIx,	,
Biografie	biografie	k1gFnSc1	biografie
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
<g/>
,	,	kIx,	,
Už	už	k6eAd1	už
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
si	se	k3xPyFc3	se
hledal	hledat	k5eAaImAgMnS	hledat
obživu	obživa	k1gFnSc4	obživa
jako	jako	k8xS	jako
námořník	námořník	k1gMnSc1	námořník
a	a	k8xC	a
obchodník	obchodník	k1gMnSc1	obchodník
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
plujících	plující	k2eAgFnPc6d1	plující
přes	přes	k7c4	přes
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
také	také	k9	také
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
žoldnéř	žoldnéř	k1gMnSc1	žoldnéř
pro	pro	k7c4	pro
Reného	René	k1gMnSc4	René
I.	I.	kA	I.
z	z	k7c2	z
Anjou	Anja	k1gFnSc7	Anja
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1476	[number]	k4	1476
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
Centurionů	centurio	k1gMnPc2	centurio
z	z	k7c2	z
Janova	Janov	k1gInSc2	Janov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
např.	např.	kA	např.
na	na	k7c4	na
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
do	do	k7c2	do
portugalských	portugalský	k2eAgFnPc2d1	portugalská
osad	osada	k1gFnPc2	osada
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
do	do	k7c2	do
Guinejského	guinejský	k2eAgInSc2d1	guinejský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Porto	porto	k1gNnSc1	porto
Santo	Sant	k2eAgNnSc1d1	Santo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pro	pro	k7c4	pro
Centuriony	centurio	k1gMnPc4	centurio
nakupoval	nakupovat	k5eAaBmAgMnS	nakupovat
třtinový	třtinový	k2eAgInSc4d1	třtinový
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
potkal	potkat	k5eAaPmAgInS	potkat
svou	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
manželku	manželka	k1gFnSc4	manželka
Filipínu	Filipín	k1gInSc2	Filipín
Moniz	Moniza	k1gFnPc2	Moniza
de	de	k?	de
Perestrello	Perestrello	k1gNnSc4	Perestrello
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
bývalého	bývalý	k2eAgMnSc2d1	bývalý
kapitána	kapitán	k1gMnSc2	kapitán
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1478	[number]	k4	1478
nebo	nebo	k8xC	nebo
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
následujícího	následující	k2eAgInSc2d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
syna	syn	k1gMnSc2	syn
Diega	Dieg	k1gMnSc2	Dieg
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
knihy	kniha	k1gFnPc1	kniha
a	a	k8xC	a
mapy	mapa	k1gFnPc1	mapa
o	o	k7c6	o
oceánských	oceánský	k2eAgFnPc6d1	oceánská
cestách	cesta	k1gFnPc6	cesta
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zemřelý	zemřelý	k2eAgMnSc1d1	zemřelý
kapitán	kapitán	k1gMnSc1	kapitán
zanechal	zanechat	k5eAaPmAgMnS	zanechat
<g/>
,	,	kIx,	,
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
vlastní	vlastní	k2eAgFnSc3d1	vlastní
výpravě	výprava	k1gFnSc3	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnPc4	manželství
trávil	trávit	k5eAaImAgMnS	trávit
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c4	v
Porto	porto	k1gNnSc4	porto
Santu	Sant	k1gInSc2	Sant
a	a	k8xC	a
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1479	[number]	k4	1479
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Janova	Janov	k1gInSc2	Janov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
ze	z	k7c2	z
služeb	služba	k1gFnPc2	služba
Centurionů	centurio	k1gMnPc2	centurio
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
jako	jako	k8xS	jako
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
kreslič	kreslič	k1gMnSc1	kreslič
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbovy	Kolumbův	k2eAgInPc1d1	Kolumbův
podněty	podnět	k1gInPc1	podnět
k	k	k7c3	k
plavbě	plavba	k1gFnSc3	plavba
na	na	k7c4	na
východ	východ	k1gInSc4	východ
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
západ	západ	k1gInSc4	západ
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
poznatky	poznatek	k1gInPc7	poznatek
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
autorita	autorita	k1gFnSc1	autorita
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
údaje	údaj	k1gInPc1	údaj
mořeplavců	mořeplavec	k1gMnPc2	mořeplavec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
plavbě	plavba	k1gFnSc6	plavba
začal	začít	k5eAaPmAgInS	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
nejspíše	nejspíše	k9	nejspíše
až	až	k9	až
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1476	[number]	k4	1476
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
svých	svůj	k3xOyFgNnPc2	svůj
studií	studio	k1gNnPc2	studio
četl	číst	k5eAaImAgMnS	číst
např.	např.	kA	např.
Dějiny	dějiny	k1gFnPc1	dějiny
papeže	papež	k1gMnSc2	papež
Pia	Pius	k1gMnSc2	Pius
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Traktáty	traktát	k1gInPc1	traktát
Petra	Petr	k1gMnSc2	Petr
z	z	k7c2	z
Ailly	Ailla	k1gFnSc2	Ailla
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milionu	milion	k4xCgInSc2	milion
Marca	Marc	k1gInSc2	Marc
Pola	pola	k1gFnSc1	pola
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
díla	dílo	k1gNnSc2	dílo
Plinia	Plinium	k1gNnSc2	Plinium
staršího	starší	k1gMnSc2	starší
a	a	k8xC	a
Plútarcha	Plútarch	k1gMnSc2	Plútarch
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
kulatá	kulatý	k2eAgFnSc1d1	kulatá
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
Kolumbově	Kolumbův	k2eAgFnSc6d1	Kolumbova
době	doba	k1gFnSc6	doba
všeobecně	všeobecně	k6eAd1	všeobecně
uznávalo	uznávat	k5eAaImAgNnS	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výpočtech	výpočet	k1gInPc6	výpočet
jejího	její	k3xOp3gInSc2	její
obvodu	obvod	k1gInSc2	obvod
se	se	k3xPyFc4	se
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
výpočty	výpočet	k1gInPc4	výpočet
Klaudia	Klaudium	k1gNnSc2	Klaudium
Ptolemaia	Ptolemaios	k1gMnSc2	Ptolemaios
a	a	k8xC	a
arabského	arabský	k2eAgMnSc2d1	arabský
astronoma	astronom	k1gMnSc2	astronom
Alfragana	Alfragan	k1gMnSc2	Alfragan
<g/>
;	;	kIx,	;
arabské	arabský	k2eAgFnSc2d1	arabská
a	a	k8xC	a
italské	italský	k2eAgFnSc2d1	italská
míle	míle	k1gFnSc2	míle
ale	ale	k8xC	ale
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
stejně	stejně	k6eAd1	stejně
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dopočítal	dopočítat	k5eAaPmAgMnS	dopočítat
menší	malý	k2eAgFnSc3d2	menší
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
spisovateli	spisovatel	k1gMnPc7	spisovatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yRgInPc2	který
čerpal	čerpat	k5eAaImAgInS	čerpat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
<g/>
,	,	kIx,	,
Strabón	Strabón	k1gMnSc1	Strabón
<g/>
,	,	kIx,	,
Plinius	Plinius	k1gMnSc1	Plinius
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
Seneca	Seneca	k1gMnSc1	Seneca
a	a	k8xC	a
Paolo	Paolo	k1gNnSc4	Paolo
dal	dát	k5eAaPmAgMnS	dát
Pozzo	Pozza	k1gFnSc5	Pozza
Toscanelli	Toscanelle	k1gFnSc3	Toscanelle
<g/>
.	.	kIx.	.
</s>
<s>
Toscanelli	Toscanell	k1gMnPc1	Toscanell
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
přišel	přijít	k5eAaPmAgMnS	přijít
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
financování	financování	k1gNnSc4	financování
výpravy	výprava	k1gFnSc2	výprava
portugalského	portugalský	k2eAgMnSc2d1	portugalský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Kolumbovy	Kolumbův	k2eAgFnSc2d1	Kolumbova
korespondence	korespondence	k1gFnSc2	korespondence
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
dva	dva	k4xCgInPc1	dva
dopisy	dopis	k1gInPc1	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
Toscanelli	Toscanell	k1gMnPc1	Toscanell
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
plánech	plán	k1gInPc6	plán
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byla	být	k5eAaImAgFnS	být
i	i	k9	i
mapa	mapa	k1gFnSc1	mapa
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sám	sám	k3xTgMnSc1	sám
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Paolo	Paolo	k1gNnSc1	Paolo
dal	dát	k5eAaPmAgInS	dát
Pozzo	Pozza	k1gFnSc5	Pozza
Toscanelli	Toscanelle	k1gFnSc4	Toscanelle
<g/>
,	,	kIx,	,
dopis	dopis	k1gInSc4	dopis
Kryštofu	Kryštof	k1gMnSc3	Kryštof
Kolumbovi	Kolumbus	k1gMnSc3	Kolumbus
<g/>
,	,	kIx,	,
Od	od	k7c2	od
různých	různý	k2eAgMnPc2d1	různý
námořníků	námořník	k1gMnPc2	námořník
také	také	k9	také
slyšel	slyšet	k5eAaImAgMnS	slyšet
o	o	k7c6	o
opracovaných	opracovaný	k2eAgInPc6d1	opracovaný
kusech	kus	k1gInPc6	kus
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
donesly	donést	k5eAaPmAgFnP	donést
západní	západní	k2eAgInPc4d1	západní
větry	vítr	k1gInPc4	vítr
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c6	o
lidských	lidský	k2eAgFnPc6d1	lidská
mrtvolách	mrtvola	k1gFnPc6	mrtvola
s	s	k7c7	s
nápadně	nápadně	k6eAd1	nápadně
širokým	široký	k2eAgInSc7d1	široký
obličejem	obličej	k1gInSc7	obličej
<g/>
,	,	kIx,	,
objevených	objevený	k2eAgInPc2d1	objevený
na	na	k7c6	na
Azorách	Azory	k1gFnPc6	Azory
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
údajům	údaj	k1gInPc3	údaj
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
věřil	věřit	k5eAaImAgMnS	věřit
a	a	k8xC	a
považoval	považovat	k5eAaImAgMnS	považovat
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
významné	významný	k2eAgFnPc4d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1484	[number]	k4	1484
předložil	předložit	k5eAaPmAgMnS	předložit
své	svůj	k3xOyFgInPc4	svůj
návrhy	návrh	k1gInPc4	návrh
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
portugalskému	portugalský	k2eAgMnSc3d1	portugalský
králi	král	k1gMnSc3	král
Janu	Jana	k1gFnSc4	Jana
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Portugalskému	portugalský	k2eAgMnSc3d1	portugalský
<g/>
,	,	kIx,	,
synovci	synovec	k1gMnPc1	synovec
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
dal	dát	k5eAaPmAgInS	dát
návrh	návrh	k1gInSc4	návrh
posoudit	posoudit	k5eAaPmF	posoudit
třem	tři	k4xCgFnPc3	tři
svým	svůj	k3xOyFgMnPc3	svůj
poradcům	poradce	k1gMnPc3	poradce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
jako	jako	k8xC	jako
neproveditelný	proveditelný	k2eNgInSc1d1	neproveditelný
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
nechtěl	chtít	k5eNaImAgMnS	chtít
Kolumba	Kolumbus	k1gMnSc4	Kolumbus
patrně	patrně	k6eAd1	patrně
podpořit	podpořit	k5eAaPmF	podpořit
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
na	na	k7c6	na
území	území	k1gNnSc6	území
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
;	;	kIx,	;
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
odklonem	odklon	k1gInSc7	odklon
k	k	k7c3	k
nejistému	jistý	k2eNgInSc3d1	nejistý
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1484	[number]	k4	1484
nebo	nebo	k8xC	nebo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1485	[number]	k4	1485
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
proniknout	proniknout	k5eAaPmF	proniknout
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vládl	vládnout	k5eAaImAgMnS	vládnout
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Aragonský	aragonský	k2eAgMnSc1d1	aragonský
a	a	k8xC	a
Isabela	Isabela	k1gFnSc1	Isabela
Kastilská	kastilský	k2eAgFnSc1d1	Kastilská
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
předložit	předložit	k5eAaPmF	předložit
svůj	svůj	k3xOyFgInSc4	svůj
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
západní	západní	k2eAgFnSc2d1	západní
cestou	cesta	k1gFnSc7	cesta
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
povedlo	povést	k5eAaPmAgNnS	povést
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1486	[number]	k4	1486
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
musel	muset	k5eAaImAgMnS	muset
čekat	čekat	k5eAaImF	čekat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1488	[number]	k4	1488
<g/>
,	,	kIx,	,
než	než	k8xS	než
jeho	jeho	k3xOp3gInSc1	jeho
projekt	projekt	k1gInSc1	projekt
přezkoumá	přezkoumat	k5eAaPmIp3nS	přezkoumat
královská	královský	k2eAgFnSc1d1	královská
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
vyslal	vyslat	k5eAaPmAgMnS	vyslat
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Bartoloměje	Bartoloměj	k1gMnSc4	Bartoloměj
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
jeho	on	k3xPp3gInSc4	on
návrh	návrh	k1gInSc4	návrh
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
Jindřichu	Jindřich	k1gMnSc3	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudorovi	Tudorův	k2eAgMnPc1d1	Tudorův
<g/>
;	;	kIx,	;
žádnou	žádný	k3yNgFnSc4	žádný
zprávu	zpráva	k1gFnSc4	zpráva
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
ale	ale	k9	ale
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1486	[number]	k4	1486
v	v	k7c6	v
královských	královský	k2eAgFnPc6d1	královská
službách	služba	k1gFnPc6	služba
španělských	španělský	k2eAgFnPc6d1	španělská
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1487	[number]	k4	1487
potkal	potkat	k5eAaPmAgMnS	potkat
Beatriz	Beatriz	k1gMnSc1	Beatriz
Enríquez	Enríquez	k1gMnSc1	Enríquez
de	de	k?	de
Harana	Harana	k1gFnSc1	Harana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
družkou	družka	k1gFnSc7	družka
<g/>
;	;	kIx,	;
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
však	však	k9	však
neoženil	oženit	k5eNaPmAgInS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1488	[number]	k4	1488
mu	on	k3xPp3gMnSc3	on
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
Fernanda	Fernanda	k1gFnSc1	Fernanda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1488	[number]	k4	1488
královská	královský	k2eAgFnSc1d1	královská
rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
geografické	geografický	k2eAgFnPc4d1	geografická
záležitosti	záležitost	k1gFnPc4	záležitost
Kolumbův	Kolumbův	k2eAgInSc4d1	Kolumbův
plán	plán	k1gInSc4	plán
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
kvůli	kvůli	k7c3	kvůli
rozporu	rozpor	k1gInSc2	rozpor
jeho	jeho	k3xOp3gInPc2	jeho
výpočtů	výpočet	k1gInPc2	výpočet
s	s	k7c7	s
všeobecně	všeobecně	k6eAd1	všeobecně
uznávanými	uznávaný	k2eAgInPc7d1	uznávaný
poznatky	poznatek	k1gInPc7	poznatek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
zařídil	zařídit	k5eAaPmAgMnS	zařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
jednalo	jednat	k5eAaImAgNnS	jednat
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
psal	psát	k5eAaImAgMnS	psát
Janu	Jan	k1gMnSc3	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
u	u	k7c2	u
něho	on	k3xPp3gNnSc2	on
získat	získat	k5eAaPmF	získat
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
čekání	čekání	k1gNnSc2	čekání
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1491	[number]	k4	1491
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
svěřil	svěřit	k5eAaPmAgMnS	svěřit
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
problémy	problém	k1gInPc7	problém
převoru	převor	k1gMnSc3	převor
Juanu	Juan	k1gMnSc3	Juan
Pérezovi	Pérez	k1gMnSc3	Pérez
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
půjde	jít	k5eAaImIp3nS	jít
hledat	hledat	k5eAaImF	hledat
štěstí	štěstí	k1gNnSc4	štěstí
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
královnu	královna	k1gFnSc4	královna
Isabelu	Isabela	k1gFnSc4	Isabela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slíbila	slíbit	k5eAaPmAgFnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Granady	Granada	k1gFnSc2	Granada
Kolumbovi	Kolumbus	k1gMnSc6	Kolumbus
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
prostředky	prostředek	k1gInPc1	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Granada	Granada	k1gFnSc1	Granada
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Španělů	Španěl	k1gMnPc2	Španěl
padla	padnout	k5eAaImAgFnS	padnout
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1492	[number]	k4	1492
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
mezi	mezi	k7c7	mezi
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
a	a	k8xC	a
královským	královský	k2eAgInSc7d1	královský
dvorem	dvůr	k1gInSc7	dvůr
nejprve	nejprve	k6eAd1	nejprve
ztroskotalo	ztroskotat	k5eAaPmAgNnS	ztroskotat
na	na	k7c6	na
Kolumbových	Kolumbův	k2eAgInPc6d1	Kolumbův
požadavcích	požadavek	k1gInPc6	požadavek
na	na	k7c4	na
odměnu	odměna	k1gFnSc4	odměna
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
dvora	dvůr	k1gInSc2	dvůr
přemrštěných	přemrštěný	k2eAgFnPc2d1	přemrštěná
<g/>
.	.	kIx.	.
</s>
<s>
Žádal	žádat	k5eAaImAgInS	žádat
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgInS	jmenovat
admirálem	admirál	k1gMnSc7	admirál
a	a	k8xC	a
místodržitelem	místodržitel	k1gMnSc7	místodržitel
všech	všecek	k3xTgFnPc2	všecek
objevených	objevený	k2eAgFnPc2d1	objevená
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gNnSc3	on
královský	královský	k2eAgInSc4d1	královský
dvůr	dvůr	k1gInSc4	dvůr
zajistil	zajistit	k5eAaPmAgMnS	zajistit
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odměnu	odměna	k1gFnSc4	odměna
a	a	k8xC	a
privilegium	privilegium	k1gNnSc4	privilegium
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
objevné	objevný	k2eAgFnPc4d1	objevná
plavby	plavba	k1gFnPc4	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Kolumbovým	Kolumbův	k2eAgMnPc3d1	Kolumbův
přátelům	přítel	k1gMnPc3	přítel
ale	ale	k8xC	ale
královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
nabídku	nabídka	k1gFnSc4	nabídka
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1492	[number]	k4	1492
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
První	první	k4xOgFnSc1	první
výprava	výprava	k1gFnSc1	výprava
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1492	[number]	k4	1492
vyplul	vyplout	k5eAaPmAgMnS	vyplout
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
karavelami	karavela	k1gFnPc7	karavela
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
Palos	Palos	k1gInSc1	Palos
de	de	k?	de
la	la	k1gNnSc2	la
Frontera	Fronter	k1gMnSc2	Fronter
blízko	blízko	k7c2	blízko
Huelvy	Huelva	k1gFnSc2	Huelva
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
loď	loď	k1gFnSc1	loď
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
velel	velet	k5eAaImAgMnS	velet
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
la	la	k1gNnSc4	la
Cosa	Cos	k1gInSc2	Cos
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
39,1	[number]	k4	39,1
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
Pinta	pinta	k1gFnSc1	pinta
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
17,8	[number]	k4	17,8
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
velel	velet	k5eAaImAgMnS	velet
jí	on	k3xPp3gFnSc3	on
Martín	Martín	k1gMnSc1	Martín
Alonso	Alonsa	k1gFnSc5	Alonsa
Pinzón	Pinzón	k1gInSc4	Pinzón
a	a	k8xC	a
Niñ	Niñ	k1gFnPc4	Niñ
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
velel	velet	k5eAaImAgInS	velet
Vicente	Vicent	k1gInSc5	Vicent
Yáñ	Yáñ	k1gMnPc4	Yáñ
Pinzón	Pinzón	k1gInSc4	Pinzón
<g/>
,	,	kIx,	,
měřila	měřit	k5eAaImAgFnS	měřit
17,1	[number]	k4	17,1
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Flotila	flotila	k1gFnSc1	flotila
celkem	celkem	k6eAd1	celkem
čítala	čítat	k5eAaImAgFnS	čítat
asi	asi	k9	asi
sto	sto	k4xCgNnSc1	sto
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celé	celý	k2eAgFnSc2d1	celá
plavby	plavba	k1gFnSc2	plavba
si	se	k3xPyFc3	se
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
psal	psát	k5eAaImAgMnS	psát
lodní	lodní	k2eAgInSc4d1	lodní
deník	deník	k1gInSc4	deník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
v	v	k7c6	v
upravených	upravený	k2eAgInPc6d1	upravený
opisech	opis	k1gInPc6	opis
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Cesare	Cesar	k1gMnSc5	Cesar
de	de	k?	de
Lollis	Lollis	k1gInSc4	Lollis
<g/>
,	,	kIx,	,
Život	život	k1gInSc4	život
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
76	[number]	k4	76
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
problém	problém	k1gInSc1	problém
nastal	nastat	k5eAaPmAgInS	nastat
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
po	po	k7c6	po
vyplutí	vyplutí	k1gNnSc6	vyplutí
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prasklo	prasknout	k5eAaPmAgNnS	prasknout
kormidlo	kormidlo	k1gNnSc1	kormidlo
Pinty	pinta	k1gFnSc2	pinta
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
opravy	oprava	k1gFnSc2	oprava
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Las	laso	k1gNnPc2	laso
Palmas	Palmasa	k1gFnPc2	Palmasa
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
zbývající	zbývající	k2eAgFnPc1d1	zbývající
dvě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
zastavily	zastavit	k5eAaPmAgFnP	zastavit
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
La	la	k1gNnSc2	la
Gomera	Gomero	k1gNnSc2	Gomero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zdržely	zdržet	k5eAaPmAgFnP	zdržet
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
dorazily	dorazit	k5eAaPmAgInP	dorazit
na	na	k7c4	na
Las	laso	k1gNnPc2	laso
Palmas	Palmasa	k1gFnPc2	Palmasa
<g/>
,	,	kIx,	,
během	během	k7c2	během
sedmi	sedm	k4xCc2	sedm
dní	den	k1gInPc2	den
byla	být	k5eAaImAgFnS	být
Pinta	pinta	k1gFnSc1	pinta
opravena	opravit	k5eAaPmNgFnS	opravit
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
flotila	flotila	k1gFnSc1	flotila
vyplula	vyplout	k5eAaPmAgFnS	vyplout
na	na	k7c4	na
Gomeru	Gomera	k1gFnSc4	Gomera
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
pak	pak	k6eAd1	pak
opustila	opustit	k5eAaPmAgFnS	opustit
poslední	poslední	k2eAgInSc4d1	poslední
známý	známý	k2eAgInSc4d1	známý
přístav	přístav	k1gInSc4	přístav
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
se	se	k3xPyFc4	se
posádka	posádka	k1gFnSc1	posádka
občas	občas	k6eAd1	občas
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dáli	dál	k1gFnSc6	dál
vidí	vidět	k5eAaImIp3nS	vidět
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
tráva	tráva	k1gFnSc1	tráva
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
si	se	k3xPyFc3	se
posádka	posádka	k1gFnSc1	posádka
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
je	být	k5eAaImIp3nS	být
povzbudil	povzbudit	k5eAaPmAgMnS	povzbudit
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
jim	on	k3xPp3gMnPc3	on
část	část	k1gFnSc4	část
zisku	zisk	k1gInSc2	zisk
z	z	k7c2	z
obchodní	obchodní	k2eAgFnSc2d1	obchodní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
nalezena	naleznout	k5eAaPmNgFnS	naleznout
větévka	větévka	k1gFnSc1	větévka
šípkové	šípkový	k2eAgFnSc2d1	šípková
růže	růž	k1gFnSc2	růž
<g/>
,	,	kIx,	,
rákos	rákos	k1gInSc1	rákos
a	a	k8xC	a
kousky	kousek	k1gInPc1	kousek
opracovaného	opracovaný	k2eAgNnSc2d1	opracované
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
deset	deset	k4xCc4	deset
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
sám	sám	k3xTgMnSc1	sám
uviděl	uvidět	k5eAaPmAgMnS	uvidět
světlo	světlo	k1gNnSc4	světlo
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
třepotalo	třepotat	k5eAaImAgNnS	třepotat
<g/>
.	.	kIx.	.
</s>
<s>
Ukázal	ukázat	k5eAaPmAgMnS	ukázat
ho	on	k3xPp3gMnSc4	on
Pedrovi	Pedr	k1gMnSc3	Pedr
Gutiérrezovi	Gutiérrez	k1gMnSc3	Gutiérrez
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
utvrdil	utvrdit	k5eAaPmAgMnS	utvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
Rodrigo	Rodrigo	k6eAd1	Rodrigo
z	z	k7c2	z
Triany	Triana	k1gFnSc2	Triana
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
plul	plout	k5eAaImAgInS	plout
na	na	k7c6	na
Pintě	pinta	k1gFnSc6	pinta
<g/>
,	,	kIx,	,
že	že	k8xS	že
vidí	vidět	k5eAaImIp3nS	vidět
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
z	z	k7c2	z
flotily	flotila	k1gFnSc2	flotila
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uviděl	uvidět	k5eAaPmAgInS	uvidět
Nový	nový	k2eAgInSc4d1	nový
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
ale	ale	k9	ale
toto	tento	k3xDgNnSc4	tento
prvenství	prvenství	k1gNnSc4	prvenství
a	a	k8xC	a
odměnu	odměna	k1gFnSc4	odměna
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
maravedí	maravedit	k5eAaPmIp3nS	maravedit
stálé	stálý	k2eAgFnSc2d1	stálá
penze	penze	k1gFnSc2	penze
přiřkl	přiřknout	k5eAaPmAgInS	přiřknout
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
tím	ten	k3xDgInSc7	ten
prvním	první	k4xOgMnSc6	první
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
den	den	k1gInSc4	den
dopoledne	dopoledne	k6eAd1	dopoledne
se	se	k3xPyFc4	se
flotila	flotila	k1gFnSc1	flotila
vylodila	vylodit	k5eAaPmAgFnS	vylodit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Guanahaní	Guanahaný	k2eAgMnPc1d1	Guanahaný
v	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
Bahamách	Bahamy	k1gFnPc6	Bahamy
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
San	San	k1gFnSc4	San
Salvador	Salvador	k1gMnSc1	Salvador
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Martínem	Martín	k1gMnSc7	Martín
Alonsem	Alons	k1gMnSc7	Alons
Pinzónem	Pinzón	k1gMnSc7	Pinzón
<g/>
,	,	kIx,	,
Vicencem	Vicenec	k1gMnSc7	Vicenec
Yaňezem	Yaňez	k1gInSc7	Yaňez
<g/>
,	,	kIx,	,
notářem	notář	k1gMnSc7	notář
Escobedou	Escobeda	k1gMnSc7	Escobeda
<g/>
,	,	kIx,	,
Rodrigo	Rodrigo	k6eAd1	Rodrigo
Sanchézem	Sanchéz	k1gInSc7	Sanchéz
ze	z	k7c2	z
Segovie	Segovie	k1gFnSc2	Segovie
zabrali	zabrat	k5eAaPmAgMnP	zabrat
tento	tento	k3xDgInSc4	tento
ostrov	ostrov	k1gInSc4	ostrov
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
krále	král	k1gMnSc4	král
a	a	k8xC	a
královnu	královna	k1gFnSc4	královna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
byli	být	k5eAaImAgMnP	být
domorodci	domorodec	k1gMnPc1	domorodec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
přátelské	přátelský	k2eAgNnSc4d1	přátelské
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
tělesně	tělesně	k6eAd1	tělesně
stavěné	stavěný	k2eAgFnPc1d1	stavěná
<g/>
,	,	kIx,	,
nahé	nahý	k2eAgFnPc1d1	nahá
a	a	k8xC	a
beze	beze	k7c2	beze
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
nedověděl	dovědět	k5eNaPmAgInS	dovědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
vyplul	vyplout	k5eAaPmAgInS	vyplout
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
nezdařených	zdařený	k2eNgFnPc6d1	nezdařená
zastávkách	zastávka	k1gFnPc6	zastávka
připlul	připlout	k5eAaPmAgInS	připlout
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
přistál	přistát	k5eAaImAgMnS	přistát
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Hispaniola	Hispaniola	k1gFnSc1	Hispaniola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
ostrova	ostrov	k1gInSc2	ostrov
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
byl	být	k5eAaImAgMnS	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
místním	místní	k2eAgMnSc7d1	místní
kasikem	kasik	k1gMnSc7	kasik
Guacanarim	Guacanarima	k1gFnPc2	Guacanarima
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
nechal	nechat	k5eAaPmAgMnS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
pevnost	pevnost	k1gFnSc4	pevnost
Navidad	Navidad	k1gInSc4	Navidad
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgMnS	zanechat
tam	tam	k6eAd1	tam
43	[number]	k4	43
lidí	člověk	k1gMnPc2	člověk
jako	jako	k8xC	jako
posádku	posádka	k1gFnSc4	posádka
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1493	[number]	k4	1493
flotila	flotila	k1gFnSc1	flotila
–	–	k?	–
teď	teď	k6eAd1	teď
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
lodích	loď	k1gFnPc6	loď
–	–	k?	–
naposledy	naposledy	k6eAd1	naposledy
přistála	přistát	k5eAaPmAgFnS	přistát
na	na	k7c6	na
Hispaniole	Hispaniola	k1gFnSc6	Hispaniola
a	a	k8xC	a
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
zastávce	zastávka	k1gFnSc6	zastávka
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
nepřátelským	přátelský	k2eNgNnSc7d1	nepřátelské
chováním	chování	k1gNnSc7	chování
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
potom	potom	k6eAd1	potom
vyplul	vyplout	k5eAaPmAgMnS	vyplout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
;	;	kIx,	;
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vezl	vézt	k5eAaImAgMnS	vézt
10	[number]	k4	10
Indiánů	Indián	k1gMnPc2	Indián
a	a	k8xC	a
vzorky	vzorek	k1gInPc1	vzorek
různých	různý	k2eAgFnPc2d1	různá
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bavlny	bavlna	k1gFnSc2	bavlna
nebo	nebo	k8xC	nebo
aloe	aloe	k1gFnSc2	aloe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
chtěl	chtít	k5eAaImAgMnS	chtít
dokázat	dokázat	k5eAaPmF	dokázat
bohatství	bohatství	k1gNnSc4	bohatství
objevených	objevený	k2eAgInPc2d1	objevený
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
doplul	doplout	k5eAaPmAgMnS	doplout
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
v	v	k7c6	v
Azorském	azorský	k2eAgNnSc6d1	Azorské
souostroví	souostroví	k1gNnSc6	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
loďstvo	loďstvo	k1gNnSc1	loďstvo
zdrželo	zdržet	k5eAaPmAgNnS	zdržet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Portugalci	Portugalec	k1gMnPc1	Portugalec
zajali	zajmout	k5eAaPmAgMnP	zajmout
několik	několik	k4yIc4	několik
Kolumbových	Kolumbův	k2eAgMnPc2d1	Kolumbův
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
je	on	k3xPp3gNnPc4	on
nechtěli	chtít	k5eNaImAgMnP	chtít
propustit	propustit	k5eAaPmF	propustit
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
zakotvil	zakotvit	k5eAaPmAgMnS	zakotvit
v	v	k7c6	v
Cascais	Cascais	k1gFnSc6	Cascais
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
Lisabonu	Lisabon	k1gInSc2	Lisabon
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Palosu	Palos	k1gInSc2	Palos
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zakotvil	zakotvit	k5eAaPmAgInS	zakotvit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
,	,	kIx,	,
Lodní	lodní	k2eAgInSc1d1	lodní
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
přijat	přijmout	k5eAaPmNgInS	přijmout
a	a	k8xC	a
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
plavbě	plavba	k1gFnSc6	plavba
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
šířila	šířit	k5eAaImAgFnS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sídlil	sídlit	k5eAaImAgInS	sídlit
královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
a	a	k8xC	a
tam	tam	k6eAd1	tam
strávil	strávit	k5eAaPmAgMnS	strávit
skoro	skoro	k6eAd1	skoro
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Dostávalo	dostávat	k5eAaImAgNnS	dostávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
projevů	projev	k1gInPc2	projev
náklonnosti	náklonnost	k1gFnSc2	náklonnost
od	od	k7c2	od
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgFnPc2d1	postavená
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zajistil	zajistit	k5eAaPmAgMnS	zajistit
povýšení	povýšení	k1gNnSc4	povýšení
svých	svůj	k3xOyFgMnPc2	svůj
bratrů	bratr	k1gMnPc2	bratr
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
a	a	k8xC	a
Diega	Dieg	k1gMnSc2	Dieg
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
už	už	k6eAd1	už
začala	začít	k5eAaPmAgFnS	začít
plánovat	plánovat	k5eAaImF	plánovat
druhá	druhý	k4xOgFnSc1	druhý
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
té	ten	k3xDgFnSc3	ten
první	první	k4xOgFnSc1	první
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
;	;	kIx,	;
během	během	k7c2	během
pěti	pět	k4xCc2	pět
měsíců	měsíc	k1gInPc2	měsíc
bylo	být	k5eAaImAgNnS	být
připraveno	připravit	k5eAaPmNgNnS	připravit
sedmnáct	sedmnáct	k4xCc1	sedmnáct
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
chystalo	chystat	k5eAaImAgNnS	chystat
nalodit	nalodit	k5eAaPmF	nalodit
přes	přes	k7c4	přes
tisíc	tisíc	k4xCgInSc4	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
účastníky	účastník	k1gMnPc7	účastník
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Alonso	Alonsa	k1gFnSc5	Alonsa
de	de	k?	de
Hojeda	Hojeda	k1gMnSc1	Hojeda
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
Ponce	Ponce	k1gMnSc1	Ponce
de	de	k?	de
León	León	k1gMnSc1	León
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
de	de	k?	de
la	la	k1gNnSc4	la
Cosa	Cos	k1gInSc2	Cos
a	a	k8xC	a
Kolumbův	Kolumbův	k2eAgMnSc1d1	Kolumbův
bratr	bratr	k1gMnSc1	bratr
Diego	Diego	k1gMnSc1	Diego
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úkolům	úkol	k1gInPc3	úkol
mise	mise	k1gFnSc2	mise
patřilo	patřit	k5eAaImAgNnS	patřit
obrátit	obrátit	k5eAaPmF	obrátit
domorodce	domorodka	k1gFnSc3	domorodka
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
na	na	k7c4	na
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
založit	založit	k5eAaPmF	založit
nové	nový	k2eAgFnPc4d1	nová
osady	osada	k1gFnPc4	osada
a	a	k8xC	a
přístavy	přístav	k1gInPc4	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
zakázal	zakázat	k5eAaPmAgMnS	zakázat
účastníkům	účastník	k1gMnPc3	účastník
výpravy	výprava	k1gFnSc2	výprava
vézt	vézt	k5eAaImF	vézt
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
zboží	zboží	k1gNnSc4	zboží
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
obchodu	obchod	k1gInSc2	obchod
<g/>
;	;	kIx,	;
vše	všechen	k3xTgNnSc4	všechen
řídil	řídit	k5eAaImAgMnS	řídit
on	on	k3xPp3gMnSc1	on
jako	jako	k8xC	jako
místokrál	místokrál	k1gMnSc1	místokrál
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
expedice	expedice	k1gFnSc1	expedice
vyplula	vyplout	k5eAaPmAgFnS	vyplout
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1493	[number]	k4	1493
z	z	k7c2	z
Cádizu	Cádiz	k1gInSc2	Cádiz
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
se	se	k3xPyFc4	se
ke	k	k7c3	k
Kanárským	kanárský	k2eAgInPc3d1	kanárský
ostrovům	ostrov	k1gInPc3	ostrov
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
odtamtud	odtamtud	k6eAd1	odtamtud
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
v	v	k7c6	v
jižnějším	jižní	k2eAgInSc6d2	jižnější
kurzu	kurz	k1gInSc6	kurz
než	než	k8xS	než
při	při	k7c6	při
předchozí	předchozí	k2eAgFnSc6d1	předchozí
výpravě	výprava	k1gFnSc6	výprava
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
přistála	přistát	k5eAaPmAgFnS	přistát
u	u	k7c2	u
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Dominika	Dominik	k1gMnSc4	Dominik
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
nebyl	být	k5eNaImAgInS	být
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc1	loď
se	se	k3xPyFc4	se
vydaly	vydat	k5eAaPmAgFnP	vydat
dále	daleko	k6eAd2	daleko
<g/>
;	;	kIx,	;
další	další	k2eAgInPc4d1	další
ostrovy	ostrov	k1gInPc4	ostrov
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Marie-Galante	Marie-Galant	k1gMnSc5	Marie-Galant
a	a	k8xC	a
Guadeloupe	Guadeloupe	k1gMnSc5	Guadeloupe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Guadeloupe	Guadeloupe	k1gFnSc6	Guadeloupe
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
o	o	k7c6	o
kanibalismu	kanibalismus	k1gInSc6	kanibalismus
Karibů	Karib	k1gMnPc2	Karib
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
výprava	výprava	k1gFnSc1	výprava
doplula	doplout	k5eAaPmAgFnS	doplout
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
Montserrat	Montserrat	k1gMnSc1	Montserrat
<g/>
,	,	kIx,	,
Redonda	Redonda	k1gFnSc1	Redonda
<g/>
,	,	kIx,	,
Antigua	Antigua	k1gFnSc1	Antigua
a	a	k8xC	a
St.	st.	kA	st.
Croix	Croix	k1gInSc1	Croix
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
přes	přes	k7c4	přes
dnešní	dnešní	k2eAgInPc4d1	dnešní
Panenské	panenský	k2eAgInPc4d1	panenský
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
Portoriko	Portoriko	k1gNnSc4	Portoriko
na	na	k7c6	na
Hispaniolu	Hispaniol	k1gInSc6	Hispaniol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rok	rok	k1gInSc4	rok
předtím	předtím	k6eAd1	předtím
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
pevnosti	pevnost	k1gFnSc2	pevnost
Navidad	Navidad	k1gInSc4	Navidad
zbyly	zbýt	k5eAaPmAgInP	zbýt
jen	jen	k9	jen
trosky	troska	k1gFnSc2	troska
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Guacanariho	Guacanari	k1gMnSc4	Guacanari
(	(	kIx(	(
<g/>
kasika	kasika	k1gFnSc1	kasika
z	z	k7c2	z
první	první	k4xOgFnSc2	první
výpravy	výprava	k1gFnSc2	výprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
pevnosti	pevnost	k1gFnSc2	pevnost
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
kvůli	kvůli	k7c3	kvůli
nemoci	nemoc	k1gFnSc3	nemoc
a	a	k8xC	a
některé	některý	k3yIgNnSc1	některý
pobili	pobít	k5eAaPmAgMnP	pobít
poddaní	poddaný	k1gMnPc1	poddaný
sousedního	sousední	k2eAgNnSc2d1	sousední
kasika	kasikum	k1gNnSc2	kasikum
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
na	na	k7c6	na
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Hispanioly	Hispaniola	k1gFnSc2	Hispaniola
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
vybudovat	vybudovat	k5eAaPmF	vybudovat
novou	nový	k2eAgFnSc4d1	nová
osadu	osada	k1gFnSc4	osada
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
Isabela	Isabela	k1gFnSc1	Isabela
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
kastilské	kastilský	k2eAgFnSc2d1	Kastilská
královny	královna	k1gFnSc2	královna
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
sbíral	sbírat	k5eAaImAgMnS	sbírat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
nalézt	nalézt	k5eAaPmF	nalézt
naleziště	naleziště	k1gNnSc4	naleziště
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
bohatých	bohatý	k2eAgNnPc6d1	bohaté
nalezištích	naleziště	k1gNnPc6	naleziště
tohoto	tento	k3xDgInSc2	tento
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vyslat	vyslat	k5eAaPmF	vyslat
12	[number]	k4	12
lodí	loď	k1gFnPc2	loď
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podaly	podat	k5eAaPmAgFnP	podat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
dosavadním	dosavadní	k2eAgInSc6d1	dosavadní
průběhu	průběh	k1gInSc6	průběh
plavby	plavba	k1gFnSc2	plavba
a	a	k8xC	a
předvedly	předvést	k5eAaPmAgInP	předvést
výsledky	výsledek	k1gInPc1	výsledek
obchodní	obchodní	k2eAgFnSc2d1	obchodní
kořisti	kořist	k1gFnSc2	kořist
<g/>
;	;	kIx,	;
z	z	k7c2	z
Hispanioly	Hispaniola	k1gFnSc2	Hispaniola
vypluly	vyplout	k5eAaPmAgInP	vyplout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
února	únor	k1gInSc2	únor
1494	[number]	k4	1494
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
další	další	k2eAgInPc4d1	další
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
;	;	kIx,	;
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
z	z	k7c2	z
Hispanioly	Hispaniola	k1gFnSc2	Hispaniola
vyplul	vyplout	k5eAaPmAgMnS	vyplout
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
karavelami	karavela	k1gFnPc7	karavela
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vedení	vedení	k1gNnSc1	vedení
nad	nad	k7c7	nad
Isabelou	Isabela	k1gFnSc7	Isabela
svěřil	svěřit	k5eAaPmAgInS	svěřit
pětičlennému	pětičlenný	k2eAgInSc3d1	pětičlenný
výboru	výbor	k1gInSc3	výbor
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
Kolumbův	Kolumbův	k2eAgMnSc1d1	Kolumbův
bratr	bratr	k1gMnSc1	bratr
Diego	Diego	k1gMnSc1	Diego
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
zastávkou	zastávka	k1gFnSc7	zastávka
byla	být	k5eAaImAgFnS	být
Kuba	Kuba	k1gFnSc1	Kuba
a	a	k8xC	a
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
Jamajku	Jamajka	k1gFnSc4	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
zlato	zlato	k1gNnSc1	zlato
tam	tam	k6eAd1	tam
ale	ale	k8xC	ale
nenašel	najít	k5eNaPmAgMnS	najít
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Plavil	plavit	k5eAaImAgInS	plavit
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
jejího	její	k3xOp3gInSc2	její
jižního	jižní	k2eAgInSc2d1	jižní
břehu	břeh	k1gInSc2	břeh
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
obeplul	obeplout	k5eAaPmAgMnS	obeplout
Jamajku	Jamajka	k1gFnSc4	Jamajka
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Hispaniolu	Hispaniola	k1gFnSc4	Hispaniola
podél	podél	k7c2	podél
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
až	až	k9	až
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1494	[number]	k4	1494
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Isabely	Isabela	k1gFnSc2	Isabela
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Isabely	Isabela	k1gFnSc2	Isabela
mezitím	mezitím	k6eAd1	mezitím
doplul	doplout	k5eAaPmAgMnS	doplout
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kryštof	Kryštof	k1gMnSc1	Kryštof
mu	on	k3xPp3gMnSc3	on
udělil	udělit	k5eAaPmAgMnS	udělit
titul	titul	k1gInSc4	titul
guvernéra	guvernér	k1gMnSc2	guvernér
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Kolumbovy	Kolumbův	k2eAgFnSc2d1	Kolumbova
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
se	se	k3xPyFc4	se
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Španěly	Španěl	k1gMnPc7	Španěl
a	a	k8xC	a
Indiány	Indián	k1gMnPc7	Indián
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yQgFnPc2	který
si	se	k3xPyFc3	se
Španělé	Španěl	k1gMnPc1	Španěl
dělali	dělat	k5eAaImAgMnP	dělat
otroky	otrok	k1gMnPc4	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
chtěl	chtít	k5eAaImAgMnS	chtít
potlačit	potlačit	k5eAaPmF	potlačit
vzpoury	vzpoura	k1gFnPc4	vzpoura
a	a	k8xC	a
tak	tak	k6eAd1	tak
nechal	nechat	k5eAaPmAgMnS	nechat
několik	několik	k4yIc4	několik
domorodců	domorodec	k1gMnPc2	domorodec
zavraždit	zavraždit	k5eAaPmF	zavraždit
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k1gNnPc2	další
poslal	poslat	k5eAaPmAgInS	poslat
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
jako	jako	k8xC	jako
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1495	[number]	k4	1495
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
velké	velký	k2eAgNnSc1d1	velké
povstání	povstání	k1gNnSc1	povstání
Indiánů	Indián	k1gMnPc2	Indián
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
krvavě	krvavě	k6eAd1	krvavě
potlačil	potlačit	k5eAaPmAgMnS	potlačit
a	a	k8xC	a
obnovil	obnovit	k5eAaPmAgMnS	obnovit
tak	tak	k6eAd1	tak
autoritu	autorita	k1gFnSc4	autorita
Španělska	Španělsko	k1gNnSc2	Španělsko
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
přenechal	přenechat	k5eAaPmAgMnS	přenechat
Isabelu	Isabela	k1gFnSc4	Isabela
Bartolomějovi	Bartoloměj	k1gMnSc3	Bartoloměj
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1496	[number]	k4	1496
odplul	odplout	k5eAaPmAgMnS	odplout
z	z	k7c2	z
Hispanioly	Hispaniola	k1gFnSc2	Hispaniola
<g/>
;	;	kIx,	;
do	do	k7c2	do
Cádizu	Cádiz	k1gInSc2	Cádiz
dorazil	dorazit	k5eAaPmAgInS	dorazit
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
ho	on	k3xPp3gMnSc4	on
nečekalo	čekat	k5eNaImAgNnS	čekat
bouřlivé	bouřlivý	k2eAgNnSc1d1	bouřlivé
přivítání	přivítání	k1gNnSc1	přivítání
–	–	k?	–
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
opadla	opadnout	k5eAaPmAgFnS	opadnout
počáteční	počáteční	k2eAgFnSc1d1	počáteční
euforie	euforie	k1gFnSc1	euforie
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
objevu	objev	k1gInSc2	objev
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
převládat	převládat	k5eAaImF	převládat
skupina	skupina	k1gFnSc1	skupina
jeho	jeho	k3xOp3gMnPc2	jeho
protivníků	protivník	k1gMnPc2	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
začali	začít	k5eAaPmAgMnP	začít
obávat	obávat	k5eAaImF	obávat
nemoci	nemoc	k1gFnPc4	nemoc
pocházející	pocházející	k2eAgFnPc4d1	pocházející
z	z	k7c2	z
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
nazvána	nazvat	k5eAaPmNgFnS	nazvat
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
třetí	třetí	k4xOgFnSc4	třetí
výpravu	výprava	k1gFnSc4	výprava
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
Kolumbovi	Kolumbův	k2eAgMnPc1d1	Kolumbův
katoličtí	katolický	k2eAgMnPc1d1	katolický
králové	král	k1gMnPc1	král
osm	osm	k4xCc4	osm
karavel	karavela	k1gFnPc2	karavela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1498	[number]	k4	1498
poslal	poslat	k5eAaPmAgMnS	poslat
dvě	dva	k4xCgFnPc4	dva
napřed	napřed	k6eAd1	napřed
a	a	k8xC	a
sám	sám	k3xTgInSc1	sám
se	s	k7c7	s
šesti	šest	k4xCc2	šest
zbývajícími	zbývající	k2eAgFnPc7d1	zbývající
karavelami	karavela	k1gFnPc7	karavela
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
sty	sto	k4xCgNnPc7	sto
muži	muž	k1gMnSc3	muž
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
vyplul	vyplout	k5eAaPmAgInS	vyplout
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1498	[number]	k4	1498
z	z	k7c2	z
města	město	k1gNnSc2	město
Sanlúcar	Sanlúcar	k1gMnSc1	Sanlúcar
de	de	k?	de
Barrameda	Barrameda	k1gMnSc1	Barrameda
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
loďstvo	loďstvo	k1gNnSc4	loďstvo
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
tři	tři	k4xCgFnPc1	tři
karavely	karavela	k1gFnPc1	karavela
měly	mít	k5eAaImAgFnP	mít
plout	plout	k5eAaImF	plout
k	k	k7c3	k
Hispaniole	Hispaniola	k1gFnSc3	Hispaniola
za	za	k7c7	za
Kolumbovým	Kolumbův	k2eAgMnSc7d1	Kolumbův
bratrem	bratr	k1gMnSc7	bratr
Bartolomějem	Bartoloměj	k1gMnSc7	Bartoloměj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tam	tam	k6eAd1	tam
mezitím	mezitím	k6eAd1	mezitím
založil	založit	k5eAaPmAgMnS	založit
Santo	Sant	k2eAgNnSc4d1	Santo
Domingo	Domingo	k1gNnSc4	Domingo
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
zbývajícími	zbývající	k2eAgFnPc7d1	zbývající
karavelami	karavela	k1gFnPc7	karavela
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
plout	plout	k5eAaImF	plout
nejdříve	dříve	k6eAd3	dříve
na	na	k7c4	na
Kapverdy	Kapverd	k1gMnPc4	Kapverd
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
směr	směr	k1gInSc4	směr
ale	ale	k8xC	ale
stočil	stočit	k5eAaPmAgMnS	stočit
více	hodně	k6eAd2	hodně
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
objevilo	objevit	k5eAaPmAgNnS	objevit
Kolumbovo	Kolumbův	k2eAgNnSc1d1	Kolumbovo
loďstvo	loďstvo	k1gNnSc1	loďstvo
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Trinidad	Trinidad	k1gInSc4	Trinidad
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
posádka	posádka	k1gFnSc1	posádka
viděla	vidět	k5eAaImAgFnS	vidět
první	první	k4xOgFnSc4	první
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
výběžek	výběžek	k1gInSc1	výběžek
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
si	se	k3xPyFc3	se
nejdříve	dříve	k6eAd3	dříve
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
si	se	k3xPyFc3	se
do	do	k7c2	do
deníku	deník	k1gInSc2	deník
nicméně	nicméně	k8xC	nicméně
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pevnina	pevnina	k1gFnSc1	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tam	tam	k6eAd1	tam
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
srpna	srpen	k1gInSc2	srpen
přistál	přistát	k5eAaImAgInS	přistát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
nejspíše	nejspíše	k9	nejspíše
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Španělé	Španěl	k1gMnPc1	Španěl
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
na	na	k7c4	na
americkou	americký	k2eAgFnSc4d1	americká
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
doplout	doplout	k5eAaPmF	doplout
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
(	(	kIx(	(
<g/>
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
objevil	objevit	k5eAaPmAgInS	objevit
ostrov	ostrov	k1gInSc1	ostrov
Margarita	Margarita	k1gFnSc1	Margarita
<g/>
)	)	kIx)	)
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1498	[number]	k4	1498
vplul	vplout	k5eAaPmAgMnS	vplout
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Hispaniole	Hispaniola	k1gFnSc6	Hispaniola
mezitím	mezitím	k6eAd1	mezitím
Francisco	Francisco	k6eAd1	Francisco
Roldán	Roldán	k2eAgMnSc1d1	Roldán
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
soudce	soudce	k1gMnSc1	soudce
v	v	k7c6	v
Isabele	Isabela	k1gFnSc6	Isabela
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
vzpouru	vzpoura	k1gFnSc4	vzpoura
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
přidalo	přidat	k5eAaPmAgNnS	přidat
mnoho	mnoho	k4c1	mnoho
osadníků	osadník	k1gMnPc2	osadník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tam	tam	k6eAd1	tam
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
přijel	přijet	k5eAaPmAgMnS	přijet
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
s	s	k7c7	s
Roldánem	Roldán	k1gInSc7	Roldán
dohodnout	dohodnout	k5eAaPmF	dohodnout
a	a	k8xC	a
situaci	situace	k1gFnSc4	situace
uklidnit	uklidnit	k5eAaPmF	uklidnit
<g/>
;	;	kIx,	;
Roldán	Roldán	k2eAgMnSc1d1	Roldán
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
žádal	žádat	k5eAaImAgMnS	žádat
ústupky	ústupek	k1gInPc4	ústupek
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
nakonec	nakonec	k6eAd1	nakonec
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Roldán	Roldán	k2eAgMnSc1d1	Roldán
začal	začít	k5eAaPmAgMnS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
své	svůj	k3xOyFgFnPc4	svůj
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Kolumbovy	Kolumbův	k2eAgMnPc4d1	Kolumbův
úředníky	úředník	k1gMnPc4	úředník
a	a	k8xC	a
rozděloval	rozdělovat	k5eAaImAgMnS	rozdělovat
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
si	se	k3xPyFc3	se
u	u	k7c2	u
kastilských	kastilský	k2eAgMnPc2d1	kastilský
králů	král	k1gMnPc2	král
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
snažil	snažit	k5eAaImAgMnS	snažit
nahradit	nahradit	k5eAaPmF	nahradit
slíbené	slíbený	k2eAgNnSc4d1	slíbené
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1500	[number]	k4	1500
v	v	k7c4	v
Santo	Santo	k1gNnSc4	Santo
Domingu	Doming	k1gInSc2	Doming
přistály	přistát	k5eAaImAgFnP	přistát
dvě	dva	k4xCgFnPc1	dva
karavely	karavela	k1gFnPc1	karavela
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
vezly	vézt	k5eAaImAgFnP	vézt
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Francisca	Francisc	k1gInSc2	Francisc
de	de	k?	de
Bobadilla	Bobadilla	k1gMnSc1	Bobadilla
<g/>
.	.	kIx.	.
</s>
<s>
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Bobadilla	Bobadilla	k1gMnSc1	Bobadilla
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
kastilských	kastilský	k2eAgMnPc2d1	kastilský
králů	král	k1gMnPc2	král
pověřen	pověřit	k5eAaPmNgMnS	pověřit
úřadem	úřad	k1gInSc7	úřad
dočasného	dočasný	k2eAgMnSc4d1	dočasný
soudce	soudce	k1gMnSc4	soudce
a	a	k8xC	a
guvernéra	guvernér	k1gMnSc4	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Zatkl	zatknout	k5eAaPmAgMnS	zatknout
všechny	všechen	k3xTgMnPc4	všechen
tři	tři	k4xCgMnPc4	tři
bratry	bratr	k1gMnPc4	bratr
Kolumby	Kolumbus	k1gMnPc4	Kolumbus
přítomné	přítomný	k1gMnPc4	přítomný
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
(	(	kIx(	(
<g/>
Kryštofa	Kryštof	k1gMnSc2	Kryštof
<g/>
,	,	kIx,	,
Diega	Dieg	k1gMnSc2	Dieg
a	a	k8xC	a
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
)	)	kIx)	)
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
je	být	k5eAaImIp3nS	být
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
připluli	připlout	k5eAaPmAgMnP	připlout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Královští	královský	k2eAgMnPc1d1	královský
manželé	manžel	k1gMnPc1	manžel
je	on	k3xPp3gMnPc4	on
propustili	propustit	k5eAaPmAgMnP	propustit
a	a	k8xC	a
pozvali	pozvat	k5eAaPmAgMnP	pozvat
Kolumba	Kolumbus	k1gMnSc4	Kolumbus
do	do	k7c2	do
Granady	Granada	k1gFnSc2	Granada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zrovna	zrovna	k6eAd1	zrovna
sídlili	sídlit	k5eAaImAgMnP	sídlit
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Bobadillovou	Bobadillův	k2eAgFnSc7d1	Bobadillův
tvrdostí	tvrdost	k1gFnSc7	tvrdost
nemají	mít	k5eNaImIp3nP	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
Kolumbovi	Kolumbus	k1gMnSc6	Kolumbus
financoval	financovat	k5eAaBmAgInS	financovat
i	i	k9	i
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
výpravu	výprava	k1gFnSc4	výprava
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepřistane	přistat	k5eNaPmIp3nS	přistat
na	na	k7c6	na
Hispaniole	Hispaniola	k1gFnSc6	Hispaniola
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
vyplul	vyplout	k5eAaPmAgMnS	vyplout
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1502	[number]	k4	1502
opět	opět	k6eAd1	opět
z	z	k7c2	z
Cádizu	Cádiz	k1gInSc2	Cádiz
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
karavelami	karavela	k1gFnPc7	karavela
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
140	[number]	k4	140
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Fernando	Fernanda	k1gFnSc5	Fernanda
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Arcily	Arcila	k1gFnSc2	Arcila
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
na	na	k7c4	na
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
doplul	doplout	k5eAaPmAgInS	doplout
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
narazil	narazit	k5eAaPmAgMnS	narazit
při	při	k7c6	při
třetí	třetí	k4xOgFnSc6	třetí
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
najít	najít	k5eAaPmF	najít
úžinu	úžina	k1gFnSc4	úžina
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
by	by	kYmCp3nS	by
proplul	proplout	k5eAaPmAgMnS	proplout
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
doplul	doplout	k5eAaPmAgMnS	doplout
k	k	k7c3	k
jižnímu	jižní	k2eAgInSc3d1	jižní
cípu	cíp	k1gInSc3	cíp
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
doplul	doplout	k5eAaPmAgMnS	doplout
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
Hondurasu	Honduras	k1gInSc2	Honduras
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
muži	muž	k1gMnPc7	muž
plujícími	plující	k2eAgMnPc7d1	plující
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
kánoi	kánoe	k1gFnSc6	kánoe
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
Kolumbovi	Kolumbův	k2eAgMnPc1d1	Kolumbův
připadali	připadat	k5eAaImAgMnP	připadat
inteligentnější	inteligentní	k2eAgMnPc1d2	inteligentnější
než	než	k8xS	než
domorodci	domorodec	k1gMnPc1	domorodec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
viděl	vidět	k5eAaImAgInS	vidět
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
v	v	k7c6	v
září	září	k1gNnSc6	září
přistál	přistát	k5eAaPmAgInS	přistát
v	v	k7c6	v
Kostarice	Kostarika	k1gFnSc6	Kostarika
a	a	k8xC	a
pak	pak	k6eAd1	pak
plul	plout	k5eAaImAgInS	plout
stále	stále	k6eAd1	stále
jižně	jižně	k6eAd1	jižně
k	k	k7c3	k
Panamě	Panama	k1gFnSc3	Panama
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
získával	získávat	k5eAaImAgInS	získávat
od	od	k7c2	od
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
cestou	cesta	k1gFnSc7	cesta
potkával	potkávat	k5eAaImAgInS	potkávat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
větší	veliký	k2eAgNnSc1d2	veliký
než	než	k8xS	než
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
panamské	panamský	k2eAgFnSc2d1	Panamská
provincie	provincie	k1gFnSc2	provincie
Veraguas	Veraguas	k1gMnSc1	Veraguas
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
založit	založit	k5eAaPmF	založit
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
agresivitě	agresivita	k1gFnSc3	agresivita
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1503	[number]	k4	1503
doplul	doplout	k5eAaPmAgMnS	doplout
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
už	už	k6eAd1	už
jen	jen	k9	jen
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
loďmi	loď	k1gFnPc7	loď
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
zbývající	zbývající	k2eAgFnPc1d1	zbývající
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ztratil	ztratit	k5eAaPmAgInS	ztratit
<g/>
)	)	kIx)	)
na	na	k7c4	na
Jamajku	Jamajka	k1gFnSc4	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
tak	tak	k9	tak
katastrofálním	katastrofální	k2eAgInSc6d1	katastrofální
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
odplout	odplout	k5eAaPmF	odplout
<g/>
.	.	kIx.	.
</s>
<s>
Diego	Diego	k1gMnSc1	Diego
Méndez	Méndez	k1gMnSc1	Méndez
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
Kolumbovy	Kolumbův	k2eAgFnSc2d1	Kolumbova
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
na	na	k7c6	na
kánoi	kánoe	k1gFnSc6	kánoe
doplavit	doplavit	k5eAaPmF	doplavit
na	na	k7c4	na
Hispaniolu	Hispaniola	k1gFnSc4	Hispaniola
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
posádky	posádka	k1gFnSc2	posádka
poslat	poslat	k5eAaPmF	poslat
novou	nový	k2eAgFnSc4d1	nová
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Záchrana	záchrana	k1gFnSc1	záchrana
pro	pro	k7c4	pro
Kolumba	Kolumbus	k1gMnSc4	Kolumbus
přijela	přijet	k5eAaPmAgFnS	přijet
až	až	k6eAd1	až
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1504	[number]	k4	1504
<g/>
;	;	kIx,	;
mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
vzbouřilo	vzbouřit	k5eAaPmAgNnS	vzbouřit
a	a	k8xC	a
Indiáni	Indián	k1gMnPc1	Indián
se	se	k3xPyFc4	se
po	po	k7c6	po
osmi	osm	k4xCc6	osm
měsících	měsíc	k1gInPc6	měsíc
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
Španěly	Španěly	k1gInPc4	Španěly
starat	starat	k5eAaImF	starat
již	již	k6eAd1	již
nebudou	být	k5eNaImBp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
si	se	k3xPyFc3	se
jejich	jejich	k3xOp3gFnSc4	jejich
loajalitu	loajalita	k1gFnSc4	loajalita
opět	opět	k6eAd1	opět
získal	získat	k5eAaPmAgMnS	získat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
zatmění	zatmění	k1gNnSc4	zatmění
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
Santo	Santo	k1gNnSc1	Santo
Domingo	Domingo	k1gMnSc1	Domingo
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1504	[number]	k4	1504
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
poslední	poslední	k2eAgFnSc2d1	poslední
výpravy	výprava	k1gFnSc2	výprava
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
výpravy	výprava	k1gFnSc2	výprava
se	se	k3xPyFc4	se
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zajistit	zajistit	k5eAaPmF	zajistit
svým	svůj	k3xOyFgMnPc3	svůj
synům	syn	k1gMnPc3	syn
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
pocty	pocta	k1gFnPc4	pocta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
slíbeny	slíbit	k5eAaPmNgInP	slíbit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gFnSc2	jeho
ochránkyně	ochránkyně	k1gFnSc2	ochránkyně
Isabely	Isabela	k1gFnSc2	Isabela
Kastilské	kastilský	k2eAgFnSc2d1	Kastilská
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1504	[number]	k4	1504
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
ale	ale	k8xC	ale
moc	moc	k6eAd1	moc
nedařilo	dařit	k5eNaImAgNnS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gMnPc4	jeho
dědice	dědic	k1gMnPc4	dědic
byl	být	k5eAaImAgMnS	být
uznán	uznán	k2eAgMnSc1d1	uznán
pouze	pouze	k6eAd1	pouze
titul	titul	k1gInSc4	titul
admirál	admirál	k1gMnSc1	admirál
<g/>
;	;	kIx,	;
tituly	titul	k1gInPc7	titul
místokrál	místokrál	k1gMnSc1	místokrál
a	a	k8xC	a
guvernér	guvernér	k1gMnSc1	guvernér
Západní	západní	k2eAgFnSc2d1	západní
Indie	Indie	k1gFnSc2	Indie
byly	být	k5eAaImAgInP	být
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
jen	jen	k9	jen
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
usadil	usadit	k5eAaPmAgInS	usadit
ve	v	k7c6	v
Valladolidu	Valladolid	k1gInSc6	Valladolid
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřel	zemřít	k5eAaPmAgInS	zemřít
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1506	[number]	k4	1506
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
54	[number]	k4	54
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
uchovány	uchovat	k5eAaPmNgInP	uchovat
nejdříve	dříve	k6eAd3	dříve
ve	v	k7c6	v
Valladolidu	Valladolid	k1gInSc6	Valladolid
a	a	k8xC	a
poté	poté	k6eAd1	poté
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1509	[number]	k4	1509
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
de	de	k?	de
las	laso	k1gNnPc2	laso
Cuevas	Cuevasa	k1gFnPc2	Cuevasa
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1542	[number]	k4	1542
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c4	v
Santo	Santo	k1gNnSc4	Santo
Domingu	Doming	k1gInSc2	Doming
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1795	[number]	k4	1795
<g/>
,	,	kIx,	,
když	když	k8xS	když
Hispaniolu	Hispaniola	k1gFnSc4	Hispaniola
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
přemístěny	přemístit	k5eAaPmNgFnP	přemístit
do	do	k7c2	do
Havany	Havana	k1gFnSc2	Havana
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Kuba	Kuba	k1gFnSc1	Kuba
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Španělsko-americké	španělskomerický	k2eAgFnSc2d1	španělsko-americká
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
Kolumbovy	Kolumbův	k2eAgInPc1d1	Kolumbův
ostatky	ostatek	k1gInPc1	ostatek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
ke	k	k7c3	k
čtyřstému	čtyřstý	k2eAgNnSc3d1	čtyřstý
výročí	výročí	k1gNnSc3	výročí
jeho	jeho	k3xOp3gInSc2	jeho
objevu	objev	k1gInSc2	objev
<g/>
,	,	kIx,	,
převezeny	převezen	k2eAgFnPc1d1	převezena
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Sevilly	Sevilla	k1gFnSc2	Sevilla
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
náhodné	náhodný	k2eAgFnSc3d1	náhodná
záměně	záměna	k1gFnSc3	záměna
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c4	v
Santo	Santo	k1gNnSc4	Santo
Domingu	Doming	k1gInSc2	Doming
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
moderní	moderní	k2eAgInPc1d1	moderní
testy	test	k1gInPc1	test
DNA	DNA	kA	DNA
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kosti	kost	k1gFnPc1	kost
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
jsou	být	k5eAaImIp3nP	být
opravdu	opravdu	k6eAd1	opravdu
Kolumbovy	Kolumbův	k2eAgFnPc1d1	Kolumbova
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbův	Kolumbův	k2eAgMnSc1d1	Kolumbův
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
dědic	dědic	k1gMnSc1	dědic
Diego	Diego	k1gMnSc1	Diego
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zdědil	zdědit	k5eAaPmAgMnS	zdědit
titul	titul	k1gInSc4	titul
admirála	admirál	k1gMnSc2	admirál
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1526	[number]	k4	1526
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
<g/>
,	,	kIx,	,
také	také	k9	také
Diego	Diego	k6eAd1	Diego
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1578	[number]	k4	1578
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vymřelo	vymřít	k5eAaPmAgNnS	vymřít
Kolumbovo	Kolumbův	k2eAgNnSc4d1	Kolumbovo
mužské	mužský	k2eAgNnSc4d1	mužské
legitimní	legitimní	k2eAgNnSc4d1	legitimní
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1499	[number]	k4	1499
se	se	k3xPyFc4	se
Alonso	Alonsa	k1gFnSc5	Alonsa
de	de	k?	de
Hojeda	Hojeda	k1gMnSc1	Hojeda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Juanem	Juan	k1gMnSc7	Juan
de	de	k?	de
la	la	k1gNnSc6	la
Cosa	Cos	k1gInSc2	Cos
a	a	k8xC	a
Amerigem	Amerig	k1gInSc7	Amerig
Vespuccim	Vespuccima	k1gFnPc2	Vespuccima
vypravili	vypravit	k5eAaPmAgMnP	vypravit
na	na	k7c4	na
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
připluli	připlout	k5eAaPmAgMnP	připlout
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
Surinamu	Surinam	k1gInSc2	Surinam
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pluli	plout	k5eAaImAgMnP	plout
severně	severně	k6eAd1	severně
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Guyany	Guyana	k1gFnSc2	Guyana
a	a	k8xC	a
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
k	k	k7c3	k
Americe	Amerika	k1gFnSc3	Amerika
vypravil	vypravit	k5eAaPmAgInS	vypravit
i	i	k9	i
Vicente	Vicent	k1gInSc5	Vicent
Yáñ	Yáñ	k1gMnPc4	Yáñ
Pinzón	Pinzón	k1gInSc4	Pinzón
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
jižně	jižně	k6eAd1	jižně
a	a	k8xC	a
doplul	doplout	k5eAaPmAgMnS	doplout
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
řeky	řeka	k1gFnSc2	řeka
Amazonky	Amazonka	k1gFnSc2	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
doplul	doplout	k5eAaPmAgMnS	doplout
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
Brazílie	Brazílie	k1gFnSc2	Brazílie
portugalský	portugalský	k2eAgMnSc1d1	portugalský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
Pedro	Pedro	k1gNnSc1	Pedro
Álvares	Álvares	k1gMnSc1	Álvares
Cabral	Cabral	k1gMnSc1	Cabral
a	a	k8xC	a
přiřkl	přiřknout	k5eAaPmAgMnS	přiřknout
ji	on	k3xPp3gFnSc4	on
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Vespucci	Vespucce	k1gFnSc4	Vespucce
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
zastánců	zastánce	k1gMnPc2	zastánce
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
objevil	objevit	k5eAaPmAgMnS	objevit
nový	nový	k2eAgInSc4d1	nový
kontinent	kontinent	k1gInSc4	kontinent
<g/>
,	,	kIx,	,
a	a	k8xC	a
německý	německý	k2eAgMnSc1d1	německý
kartograf	kartograf	k1gMnSc1	kartograf
Martin	Martin	k1gMnSc1	Martin
Waldseemüller	Waldseemüller	k1gMnSc1	Waldseemüller
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
mapě	mapa	k1gFnSc6	mapa
světa	svět	k1gInSc2	svět
označil	označit	k5eAaPmAgInS	označit
nový	nový	k2eAgInSc1d1	nový
kontinent	kontinent	k1gInSc1	kontinent
(	(	kIx(	(
<g/>
identifikována	identifikován	k2eAgFnSc1d1	identifikována
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
America	America	k1gMnSc1	America
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1538	[number]	k4	1538
nazval	nazvat	k5eAaBmAgMnS	nazvat
Gerhard	Gerhard	k1gMnSc1	Gerhard
Mercator	Mercator	k1gMnSc1	Mercator
Amerikou	Amerika	k1gFnSc7	Amerika
celý	celý	k2eAgInSc4d1	celý
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Kolumbův	Kolumbův	k2eAgInSc1d1	Kolumbův
objev	objev	k1gInSc1	objev
znamenal	znamenat	k5eAaImAgInS	znamenat
počátek	počátek	k1gInSc4	počátek
španělské	španělský	k2eAgFnSc2d1	španělská
conquisty	conquista	k1gFnSc2	conquista
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
kolonizace	kolonizace	k1gFnSc1	kolonizace
Jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
Portugalskem	Portugalsko	k1gNnSc7	Portugalsko
<g/>
,	,	kIx,	,
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1492	[number]	k4	1492
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
symbolický	symbolický	k2eAgInSc4d1	symbolický
počátek	počátek	k1gInSc4	počátek
novověku	novověk	k1gInSc2	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
geografických	geografický	k2eAgInPc2d1	geografický
útvarů	útvar	k1gInPc2	útvar
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
nese	nést	k5eAaImIp3nS	nést
po	po	k7c6	po
Kolumbovi	Kolumbus	k1gMnSc6	Kolumbus
jméno	jméno	k1gNnSc4	jméno
Columbus	Columbus	k1gInSc1	Columbus
nebo	nebo	k8xC	nebo
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
např.	např.	kA	např.
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Ohia	Ohio	k1gNnSc2	Ohio
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
USA	USA	kA	USA
nese	nést	k5eAaImIp3nS	nést
Kolumbovo	Kolumbův	k2eAgNnSc1d1	Kolumbovo
jméno	jméno	k1gNnSc1	jméno
např.	např.	kA	např.
stát	stát	k5eAaPmF	stát
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
druhé	druhý	k4xOgNnSc4	druhý
pondělí	pondělí	k1gNnSc4	pondělí
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
)	)	kIx)	)
slaví	slavit	k5eAaImIp3nP	slavit
Kolumbův	Kolumbův	k2eAgInSc4d1	Kolumbův
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
měna	měna	k1gFnSc1	měna
Kostariky	Kostarika	k1gFnSc2	Kostarika
–	–	k?	–
colón	colón	k1gInSc1	colón
–	–	k?	–
převzala	převzít	k5eAaPmAgFnS	převzít
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
formy	forma	k1gFnSc2	forma
mořeplavcova	mořeplavcův	k2eAgNnSc2d1	mořeplavcův
příjmení	příjmení	k1gNnSc2	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
město	město	k1gNnSc1	město
Panamy	Panama	k1gFnSc2	Panama
se	se	k3xPyFc4	se
též	též	k9	též
nazývá	nazývat	k5eAaImIp3nS	nazývat
Colón	colón	k1gInSc1	colón
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
Kolumbova	Kolumbův	k2eAgFnSc1d1	Kolumbova
světová	světový	k2eAgFnSc1d1	světová
výstava	výstava	k1gFnSc1	výstava
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
400	[number]	k4	400
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
Kolumbova	Kolumbův	k2eAgNnSc2d1	Kolumbovo
připlutí	připlutí	k1gNnSc2	připlutí
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
<g/>
.	.	kIx.	.
</s>
<s>
Expo	Expo	k1gNnSc4	Expo
během	během	k7c2	během
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
trvání	trvání	k1gNnSc2	trvání
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
27	[number]	k4	27
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pošta	pošta	k1gFnSc1	pošta
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
do	do	k7c2	do
oslav	oslava	k1gFnPc2	oslava
zapojila	zapojit	k5eAaPmAgFnS	zapojit
vydáním	vydání	k1gNnSc7	vydání
16	[number]	k4	16
typů	typ	k1gInPc2	typ
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Columbian	Columbiany	k1gInPc2	Columbiany
Issue	Issu	k1gFnSc2	Issu
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
byl	být	k5eAaImAgMnS	být
zobrazen	zobrazen	k2eAgMnSc1d1	zobrazen
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
Isabela	Isabela	k1gFnSc1	Isabela
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Expa	Expo	k1gNnSc2	Expo
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
těchto	tento	k3xDgFnPc2	tento
známek	známka	k1gFnPc2	známka
za	za	k7c7	za
celkem	celek	k1gInSc7	celek
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
