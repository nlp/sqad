<s>
Zdeněk	Zdeněk	k1gMnSc1
Dopita	dopit	k2eAgFnSc1d1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Dopita	dopit	k2eAgFnSc1d1
Narození	narození	k1gNnSc6
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1912	#num#	k4
<g/>
Brno	Brno	k1gNnSc1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1977	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
červen	červen	k1gInSc1
1977	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Brno	Brno	k1gNnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
režisér	režisér	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zdeněk	Zdeňka	k1gFnPc2
Dopita	dopit	k2eAgFnSc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1912	#num#	k4
Brno	Brno	k1gNnSc4
–	–	k?
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1977	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
divadelní	divadelní	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celou	celý	k2eAgFnSc4d1
svou	svůj	k3xOyFgFnSc4
profesní	profesní	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
zejména	zejména	k9
ochotnickému	ochotnický	k2eAgNnSc3d1
divadlu	divadlo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
byl	být	k5eAaImAgMnS
ředitelem	ředitel	k1gMnSc7
Divadla	divadlo	k1gNnSc2
bratří	bratr	k1gMnPc2
Mrštíků	Mrštík	k1gInPc2
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgNnSc1d1
Městské	městský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vzorem	vzor	k1gInSc7
pro	pro	k7c4
mnoho	mnoho	k4c1
jeho	jeho	k3xOp3gMnPc2
následovníků	následovník	k1gMnPc2
jako	jako	k8xC,k8xS
L.	L.	kA
Walletzkého	Walletzký	k1gMnSc2
<g/>
,	,	kIx,
dr	dr	kA
<g/>
.	.	kIx.
Víta	Vít	k1gMnSc2
Závodského	Závodský	k2eAgMnSc2d1
<g/>
,	,	kIx,
dr	dr	kA
<g/>
.	.	kIx.
Miroslava	Miroslav	k1gMnSc4
Plešáka	plešák	k1gMnSc4
<g/>
,	,	kIx,
Alenu	Alena	k1gFnSc4
Kalábovou	Kalábová	k1gFnSc4
<g/>
,	,	kIx,
dr	dr	kA
<g/>
.	.	kIx.
Libuši	Libuše	k1gFnSc4
Zbořilovou	Zbořilová	k1gFnSc4
aj.	aj.	kA
V	v	k7c6
letech	let	k1gInPc6
1960	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
byl	být	k5eAaImAgMnS
předsedou	předseda	k1gMnSc7
krajského	krajský	k2eAgInSc2d1
poradního	poradní	k2eAgInSc2d1
sboru	sbor	k1gInSc2
pro	pro	k7c4
divadlo	divadlo	k1gNnSc4
jihomoravského	jihomoravský	k2eAgInSc2d1
KKS	KKS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
profesorem	profesor	k1gMnSc7
na	na	k7c6
JAMU	jam	k1gInSc6
<g/>
,	,	kIx,
mezi	mezi	k7c7
jeho	jeho	k3xOp3gMnPc7
žáky	žák	k1gMnPc7
patří	patřit	k5eAaImIp3nP
např.	např.	kA
i	i	k8xC
Libuše	Libuše	k1gFnSc1
Šafránková	Šafránková	k1gFnSc1
nebo	nebo	k8xC
Oldřich	Oldřich	k1gMnSc1
Kaiser	Kaiser	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ZBOŘILOVÁ	Zbořilová	k1gFnSc1
<g/>
,	,	kIx,
Libuše	Libuše	k1gFnSc1
<g/>
:	:	kIx,
Blahopřání	blahopřání	k1gNnSc1
příteli	přítel	k1gMnSc3
ochotníků	ochotník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
AS	as	k1gInSc1
1972	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
7	#num#	k4
</s>
<s>
AS	as	k1gInSc1
1977	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
–	–	k?
studie	studie	k1gFnSc2
</s>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1
<g/>
,	,	kIx,
Pavla	Pavla	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
:	:	kIx,
13	#num#	k4
<g/>
.	.	kIx.
sešit	sešit	k1gInSc1
:	:	kIx,
Dig-Doš	Dig-Doš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
216-338	216-338	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
416	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
314	#num#	k4
<g/>
-	-	kIx~
<g/>
315	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zdeněk	Zdeňka	k1gFnPc2
Dopita	dopít	k5eAaPmNgFnS
na	na	k7c4
www.amaterskedivadlo.cz	www.amaterskedivadlo.cz	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1022731	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5749	#num#	k4
8170	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83695210	#num#	k4
</s>
