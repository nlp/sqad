<s>
Helvíkov	Helvíkov	k1gInSc1	Helvíkov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Klein	Klein	k1gMnSc1	Klein
Hermigsdorf	Hermigsdorf	k1gMnSc1	Hermigsdorf
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Anenská	anenský	k2eAgFnSc1d1	Anenská
Studánka	studánka	k1gFnSc1	studánka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
2,5	[number]	k4	2,5
km	km	kA	km
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Anenské	anenský	k2eAgFnSc2d1	Anenská
Studánky	studánka	k1gFnSc2	studánka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
19	[number]	k4	19
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
žilo	žít	k5eAaImAgNnS	žít
10	[number]	k4	10
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Helvíkov	Helvíkov	k1gInSc1	Helvíkov
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
3,39	[number]	k4	3,39
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Helvíkově	Helvíkův	k2eAgFnSc6d1	Helvíkův
stojí	stát	k5eAaImIp3nS	stát
Kaple	kaple	k1gFnSc1	kaple
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konají	konat	k5eAaImIp3nP	konat
pravidelně	pravidelně	k6eAd1	pravidelně
poutní	poutní	k2eAgFnPc4d1	poutní
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Helvíkov	Helvíkov	k1gInSc4	Helvíkov
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc1	Commons
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Helvíkov	Helvíkov	k1gInSc4	Helvíkov
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
