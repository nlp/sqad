<s>
Louis	Louis	k1gMnSc1	Louis
Germain	Germain	k1gMnSc1	Germain
David	David	k1gMnSc1	David
de	de	k?	de
Funè	Funè	k1gMnSc1	Funè
de	de	k?	de
Galarza	Galarza	k1gFnSc1	Galarza
[	[	kIx(	[
<g/>
lwi	lwi	k?	lwi
də	də	k?	də
fy	fy	kA	fy
<g/>
.	.	kIx.	.
<g/>
nɛ	nɛ	k?	nɛ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Courbevoie	Courbevoie	k1gFnSc1	Courbevoie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1983	[number]	k4	1983
v	v	k7c6	v
Nantes	Nantesa	k1gFnPc2	Nantesa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slavný	slavný	k2eAgMnSc1d1	slavný
francouzský	francouzský	k2eAgMnSc1d1	francouzský
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
komik	komik	k1gMnSc1	komik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
se	se	k3xPyFc4	se
jmenují	jmenovat	k5eAaBmIp3nP	jmenovat
Daniel	Daniel	k1gMnSc1	Daniel
Charles	Charles	k1gMnSc1	Charles
Louis	Louis	k1gMnSc1	Louis
de	de	k?	de
Funè	Funè	k1gMnSc1	Funè
de	de	k?	de
Galarza	Galarza	k1gFnSc1	Galarza
<g/>
,	,	kIx,	,
Olivier	Olivier	k1gMnSc1	Olivier
de	de	k?	de
Funè	Funè	k1gMnSc1	Funè
a	a	k8xC	a
Patrick	Patrick	k1gMnSc1	Patrick
de	de	k?	de
Funè	Funè	k1gMnSc1	Funè
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
dětí	dítě	k1gFnPc2	dítě
nedaleko	nedaleko	k7c2	nedaleko
Paříže	Paříž	k1gFnSc2	Paříž
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
Sevilly	Sevilla	k1gFnSc2	Sevilla
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Carlos	Carlos	k1gMnSc1	Carlos
Luis	Luisa	k1gFnPc2	Luisa
de	de	k?	de
Funè	Funè	k1gMnSc2	Funè
de	de	k?	de
Galarza	Galarz	k1gMnSc2	Galarz
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
Málagy	Málaga	k1gFnSc2	Málaga
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
obchodoval	obchodovat	k5eAaImAgMnS	obchodovat
s	s	k7c7	s
klenoty	klenot	k1gInPc7	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Leonor	Leonora	k1gFnPc2	Leonora
Soto	Soto	k1gMnSc1	Soto
y	y	k?	y
Réguéra	Réguéra	k1gFnSc1	Réguéra
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Ortigueira	Ortigueir	k1gInSc2	Ortigueir
v	v	k7c6	v
Galicii	Galicie	k1gFnSc6	Galicie
<g/>
.	.	kIx.	.
</s>
<s>
Louis	Louis	k1gMnSc1	Louis
měl	mít	k5eAaImAgMnS	mít
sestru	sestra	k1gFnSc4	sestra
Marii	Maria	k1gFnSc4	Maria
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
režiséra	režisér	k1gMnSc2	režisér
Françoise	Françoise	k1gFnSc2	Françoise
Gira	Girus	k1gMnSc2	Girus
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
Charles	Charles	k1gMnSc1	Charles
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zastřelen	zastřelit	k5eAaPmNgInS	zastřelit
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
kreslení	kreslení	k1gNnSc4	kreslení
a	a	k8xC	a
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
francouzštiny	francouzština	k1gFnSc2	francouzština
dobře	dobře	k6eAd1	dobře
ovládal	ovládat	k5eAaImAgInS	ovládat
i	i	k9	i
španělštinu	španělština	k1gFnSc4	španělština
a	a	k8xC	a
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hercem	herec	k1gMnSc7	herec
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
neúspěšně	úspěšně	k6eNd1	úspěšně
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
řadu	řada	k1gFnSc4	řada
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
:	:	kIx,	:
byl	být	k5eAaImAgMnS	být
kožešník	kožešník	k1gMnSc1	kožešník
<g/>
,	,	kIx,	,
dekoratér	dekoratér	k1gMnSc1	dekoratér
<g/>
,	,	kIx,	,
aranžér	aranžér	k1gMnSc1	aranžér
<g/>
,	,	kIx,	,
účetní	účetní	k1gMnSc1	účetní
<g/>
,	,	kIx,	,
prodavač	prodavač	k1gMnSc1	prodavač
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
pianista	pianista	k1gMnSc1	pianista
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Pigalle	Pigalle	k1gFnSc2	Pigalle
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
Simonovy	Simonův	k2eAgInPc4d1	Simonův
herecké	herecký	k2eAgInPc4d1	herecký
kurzy	kurz	k1gInPc4	kurz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Germaine	Germain	k1gInSc5	Germain
Carroyer	Carroyero	k1gNnPc2	Carroyero
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc4	syn
Daniela	Daniel	k1gMnSc4	Daniel
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Jeanne	Jeann	k1gInSc5	Jeann
Barthelémy	Barthelém	k1gInPc1	Barthelém
de	de	k?	de
Maupassant	Maupassant	k1gInSc1	Maupassant
(	(	kIx(	(
<g/>
praneteří	praneteř	k1gFnSc7	praneteř
Guy	Guy	k1gMnSc2	Guy
de	de	k?	de
Maupassanta	Maupassant	k1gMnSc2	Maupassant
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
:	:	kIx,	:
Patricka	Patricka	k1gFnSc1	Patricka
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oliviera	Oliviera	k1gFnSc1	Oliviera
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patrick	Patrick	k1gMnSc1	Patrick
je	být	k5eAaImIp3nS	být
lékařem	lékař	k1gMnSc7	lékař
<g/>
,	,	kIx,	,
Olivier	Olivier	k1gMnSc1	Olivier
se	se	k3xPyFc4	se
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
věnoval	věnovat	k5eAaPmAgMnS	věnovat
herectví	herectví	k1gNnSc4	herectví
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
společně	společně	k6eAd1	společně
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
pilot	pilot	k1gMnSc1	pilot
Concorde	Concord	k1gInSc5	Concord
pro	pro	k7c4	pro
Air	Air	k1gMnSc4	Air
France	Franc	k1gMnSc4	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Louis	Louis	k1gMnSc1	Louis
de	de	k?	de
Funè	Funè	k1gMnSc1	Funè
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Slaměný	slaměný	k2eAgMnSc1d1	slaměný
milenec	milenec	k1gMnSc1	milenec
<g/>
,	,	kIx,	,
před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Barbizonovo	Barbizonův	k2eAgNnSc1d1	Barbizonův
pokušení	pokušení	k1gNnSc1	pokušení
<g/>
.	.	kIx.	.
</s>
<s>
Různými	různý	k2eAgFnPc7d1	různá
vedlejšími	vedlejší	k2eAgFnPc7d1	vedlejší
rolemi	role	k1gFnPc7	role
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
prokousával	prokousávat	k5eAaImAgMnS	prokousávat
<g/>
"	"	kIx"	"
celých	celý	k2eAgNnPc2d1	celé
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gMnSc4	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
na	na	k7c6	na
výsluní	výsluní	k1gNnSc6	výsluní
díky	díky	k7c3	díky
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
Oskar	Oskar	k1gMnSc1	Oskar
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
i	i	k9	i
řadu	řada	k1gFnSc4	řada
epizodních	epizodní	k2eAgFnPc2d1	epizodní
rolí	role	k1gFnPc2	role
<g/>
,	,	kIx,	,
než	než	k8xS	než
získal	získat	k5eAaPmAgMnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
komedii	komedie	k1gFnSc6	komedie
Četník	četník	k1gMnSc1	četník
ze	z	k7c2	z
Saint	Sainta	k1gFnPc2	Sainta
Tropez	Tropeza	k1gFnPc2	Tropeza
-	-	kIx~	-
získal	získat	k5eAaPmAgMnS	získat
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
coby	coby	k?	coby
strážmistr	strážmistr	k1gMnSc1	strážmistr
Ludovic	Ludovice	k1gFnPc2	Ludovice
Cruchot	Cruchot	k1gMnSc1	Cruchot
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
už	už	k6eAd1	už
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nabídky	nabídka	k1gFnPc1	nabídka
jen	jen	k9	jen
hrnuly	hrnout	k5eAaImAgFnP	hrnout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Fantomas	Fantomas	k1gMnSc1	Fantomas
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
komisaře	komisař	k1gMnPc4	komisař
Juva	Juvum	k1gNnSc2	Juvum
<g/>
,	,	kIx,	,
zahrál	zahrát	k5eAaPmAgInS	zahrát
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
Jeanem	Jean	k1gMnSc7	Jean
Maraisem	Marais	k1gInSc7	Marais
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Gérardem	Gérard	k1gMnSc7	Gérard
Ourym	Ourym	k1gInSc4	Ourym
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
herce	herec	k1gMnSc2	herec
André	André	k1gMnSc2	André
Bourvila	Bourvila	k1gMnSc2	Bourvila
komedii	komedie	k1gFnSc4	komedie
Smolař	smolař	k1gMnSc1	smolař
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
11	[number]	k4	11
752	[number]	k4	752
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgInS	zapsat
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
filmové	filmový	k2eAgFnSc2d1	filmová
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
pozdější	pozdní	k2eAgInSc1d2	pozdější
film	film	k1gInSc1	film
Velký	velký	k2eAgInSc1d1	velký
flám	flám	k1gInSc4	flám
(	(	kIx(	(
<g/>
natočený	natočený	k2eAgInSc4d1	natočený
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
17	[number]	k4	17
000	[number]	k4	000
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
(	(	kIx(	(
<g/>
ve	v	k7c6	v
francouzských	francouzský	k2eAgNnPc6d1	francouzské
kinech	kino	k1gNnPc6	kino
tento	tento	k3xDgInSc1	tento
rekord	rekord	k1gInSc1	rekord
překonal	překonat	k5eAaPmAgInS	překonat
až	až	k9	až
Titanik	Titanik	k1gInSc1	Titanik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgInS	být
řadu	řada	k1gFnSc4	řada
let	let	k1gInSc1	let
enormně	enormně	k6eAd1	enormně
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgMnS	být
téměř	téměř	k6eAd1	téměř
neznámý	známý	k2eNgMnSc1d1	neznámý
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
komedie	komedie	k1gFnSc2	komedie
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
rabína	rabín	k1gMnSc2	rabín
Jákoba	Jákob	k1gMnSc2	Jákob
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
jeho	jeho	k3xOp3gFnSc2	jeho
popularitě	popularita	k1gFnSc3	popularita
napomohl	napomoct	k5eAaPmAgInS	napomoct
výrazný	výrazný	k2eAgInSc1d1	výrazný
výkon	výkon	k1gInSc1	výkon
dabingu	dabing	k1gInSc2	dabing
Františka	František	k1gMnSc2	František
Filipovského	Filipovský	k1gMnSc2	Filipovský
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
herecký	herecký	k2eAgInSc1d1	herecký
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
opíral	opírat	k5eAaImAgInS	opírat
o	o	k7c4	o
živelnou	živelný	k2eAgFnSc4d1	živelná
komiku	komika	k1gFnSc4	komika
a	a	k8xC	a
neuvěřitelné	uvěřitelný	k2eNgFnPc4d1	neuvěřitelná
mimické	mimický	k2eAgFnPc4d1	mimická
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
zdědil	zdědit	k5eAaPmAgInS	zdědit
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
vynesly	vynést	k5eAaPmAgInP	vynést
přezdívku	přezdívka	k1gFnSc4	přezdívka
Muž	muž	k1gMnSc1	muž
tisíce	tisíc	k4xCgInPc1	tisíc
tváří	tvář	k1gFnPc2	tvář
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
tváří	tvář	k1gFnSc7	tvář
z	z	k7c2	z
gumy	guma	k1gFnSc2	guma
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
typické	typický	k2eAgFnPc4d1	typická
grimasy	grimasa	k1gFnPc4	grimasa
okoukal	okoukat	k5eAaPmAgMnS	okoukat
z	z	k7c2	z
grotesek	groteska	k1gFnPc2	groteska
s	s	k7c7	s
Charlie	Charlie	k1gMnSc1	Charlie
Chaplinem	Chaplin	k1gInSc7	Chaplin
<g/>
,	,	kIx,	,
kačerem	kačer	k1gMnSc7	kačer
Donaldem	Donald	k1gMnSc7	Donald
i	i	k8xC	i
od	od	k7c2	od
Laurela	Laurel	k1gMnSc2	Laurel
a	a	k8xC	a
Hardyho	Hardy	k1gMnSc2	Hardy
<g/>
.	.	kIx.	.
</s>
<s>
Louis	Louis	k1gMnSc1	Louis
de	de	k?	de
Funè	Funè	k1gMnSc1	Funè
miloval	milovat	k5eAaImAgMnS	milovat
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
zahradu	zahrada	k1gFnSc4	zahrada
a	a	k8xC	a
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Clermont	Clermonta	k1gFnPc2	Clermonta
na	na	k7c6	na
Loiře	Loira	k1gFnSc6	Loira
u	u	k7c2	u
Nantes	Nantesa	k1gFnPc2	Nantesa
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
trávil	trávit	k5eAaImAgMnS	trávit
čas	čas	k1gInSc4	čas
ve	v	k7c6	v
skleníku	skleník	k1gInSc6	skleník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
růžím	růž	k1gFnPc3	růž
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vyšlechtit	vyšlechtit	k5eAaPmF	vyšlechtit
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
La	la	k1gNnSc4	la
rose	rosa	k1gFnSc3	rosa
de	de	k?	de
Louis	Louis	k1gMnSc1	Louis
de	de	k?	de
Funè	Funè	k1gFnSc1	Funè
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
uznaným	uznaný	k2eAgInSc7d1	uznaný
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
Funè	Funè	k1gMnSc1	Funè
měl	mít	k5eAaImAgMnS	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
srdcem	srdce	k1gNnSc7	srdce
a	a	k8xC	a
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
svojí	svůj	k3xOyFgFnSc7	svůj
rolí	role	k1gFnSc7	role
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
představoval	představovat	k5eAaImAgInS	představovat
různé	různý	k2eAgFnPc4d1	různá
cholerické	cholerický	k2eAgFnPc4d1	cholerická
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
riskoval	riskovat	k5eAaBmAgMnS	riskovat
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
prodělal	prodělat	k5eAaPmAgMnS	prodělat
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc2	film
Četník	četník	k1gMnSc1	četník
a	a	k8xC	a
četnice	četnice	k1gFnSc1	četnice
a	a	k8xC	a
na	na	k7c4	na
který	který	k3yIgInSc4	který
nakonec	nakonec	k6eAd1	nakonec
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1983	[number]	k4	1983
také	také	k9	také
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
<g />
.	.	kIx.	.
</s>
<s>
Lakomec	lakomec	k1gMnSc1	lakomec
1981	[number]	k4	1981
Zelňačka	zelňačka	k1gFnSc1	zelňačka
Slaměný	slaměný	k2eAgMnSc1d1	slaměný
milenec	milenec	k1gMnSc1	milenec
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
́	́	k?	́
<g/>
amant	amant	k1gMnSc1	amant
de	de	k?	de
paille	paille	k1gFnSc1	paille
<g/>
)	)	kIx)	)
-	-	kIx~	-
statista	statista	k1gMnSc1	statista
Dům	dům	k1gInSc4	dům
doni	doňa	k1gFnSc2	doňa
Bernardy	Bernard	k1gMnPc4	Bernard
-	-	kIx~	-
plačka	plačka	k1gFnSc1	plačka
Winterset	Winterset	k1gMnSc1	Winterset
-	-	kIx~	-
Hanriot	Hanriot	k1gInSc1	Hanriot
Thermidor	thermidor	k1gInSc1	thermidor
-	-	kIx~	-
tulák	tulák	k1gMnSc1	tulák
Tramvaj	tramvaj	k1gFnSc1	tramvaj
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Touha	touha	k1gFnSc1	touha
-	-	kIx~	-
Pablo	Pablo	k1gNnSc1	Pablo
Dominika	Dominik	k1gMnSc2	Dominik
a	a	k8xC	a
Dominika	Dominik	k1gMnSc2	Dominik
(	(	kIx(	(
<g/>
Dominique	Dominiqu	k1gMnSc2	Dominiqu
et	et	k?	et
Dominique	Dominiqu	k1gMnSc2	Dominiqu
<g/>
)	)	kIx)	)
-	-	kIx~	-
pan	pan	k1gMnSc1	pan
Ernest	Ernest	k1gMnSc1	Ernest
Brouk	brouk	k1gMnSc1	brouk
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
puce	puce	k?	puce
à	à	k?	à
l	l	kA	l
<g/>
́	́	k?	́
<g/>
oreille	oreille	k1gInSc1	oreille
<g/>
)	)	kIx)	)
-	-	kIx~	-
Augustin	Augustin	k1gMnSc1	Augustin
Feraillon	Feraillon	k1gInSc4	Feraillon
Ach	ach	k0	ach
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc4	ten
krásné	krásný	k2eAgFnPc4d1	krásná
bakchantky	bakchantka	k1gFnPc4	bakchantka
(	(	kIx(	(
<g/>
Ah	ah	k0	ah
<g/>
!	!	kIx.	!
</s>
<s>
Les	les	k1gInSc1	les
belles	belles	k1gMnSc1	belles
Bacchantes	Bacchantes	k1gMnSc1	Bacchantes
<g/>
)	)	kIx)	)
-	-	kIx~	-
komisař	komisař	k1gMnSc1	komisař
Leboeuf	Leboeuf	k1gMnSc1	Leboeuf
Poppi	Popp	k1gFnSc2	Popp
-	-	kIx~	-
Poppi	Popp	k1gFnSc2	Popp
Miláček	miláček	k1gMnSc1	miláček
Ornifle	Ornifle	k1gFnSc1	Ornifle
-	-	kIx~	-
Machetu	Machet	k1gInSc2	Machet
Zasněme	zasnout	k5eAaPmRp1nP	zasnout
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
Faisons	Faisons	k1gInSc1	Faisons
un	un	k?	un
rê	rê	k?	rê
<g/>
)	)	kIx)	)
-	-	kIx~	-
manžel	manžel	k1gMnSc1	manžel
Oskar	Oskar	k1gMnSc1	Oskar
(	(	kIx(	(
<g/>
Oscar	Oscar	k1gMnSc1	Oscar
<g/>
)	)	kIx)	)
-	-	kIx~	-
Barnier	Barnier	k1gInSc1	Barnier
Velký	velký	k2eAgInSc1d1	velký
valčík	valčík	k1gInSc1	valčík
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
grosse	grosse	k1gFnSc2	grosse
valse	valse	k1gFnSc2	valse
<g/>
)	)	kIx)	)
-	-	kIx~	-
celník	celník	k1gInSc1	celník
Roussel	Roussel	k1gInSc1	Roussel
Valčík	valčík	k1gInSc1	valčík
toreadorů	toreador	k1gMnPc2	toreador
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
valse	valse	k1gFnSc2	valse
des	des	k1gNnSc2	des
toreadors	toreadorsa	k1gFnPc2	toreadorsa
<g/>
)	)	kIx)	)
-	-	kIx~	-
generál	generál	k1gMnSc1	generál
</s>
