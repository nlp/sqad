<s>
Ivan	Ivan	k1gMnSc1	Ivan
Král	Král	k1gMnSc1	Král
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k8xC	jako
baskytarista	baskytarista	k1gMnSc1	baskytarista
skupiny	skupina	k1gFnSc2	skupina
Patti	Patť	k1gFnSc2	Patť
Smith	Smith	k1gMnSc1	Smith
Group	Group	k1gMnSc1	Group
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
režisérů	režisér	k1gMnPc2	režisér
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
The	The	k1gMnSc1	The
Blank	Blank	k1gMnSc1	Blank
Generation	Generation	k1gInSc4	Generation
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Českého	český	k2eAgMnSc4d1	český
lva	lev	k1gMnSc4	lev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Kabriolet	kabriolet	k1gInSc1	kabriolet
<g/>
.	.	kIx.	.
</s>

