<s>
Původní	původní	k2eAgFnSc1d1	původní
DNA	dna	k1gFnSc1	dna
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
matrice	matrice	k1gFnSc1	matrice
či	či	k8xC	či
templát	templát	k1gInSc1	templát
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
DNA	dna	k1gFnSc1	dna
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
replika	replika	k1gFnSc1	replika
<g/>
.	.	kIx.	.
</s>
