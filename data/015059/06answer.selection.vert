<s desamb="1">
Medaile	medaile	k1gFnSc1
je	být	k5eAaImIp3nS
udílena	udílet	k5eAaImNgFnS
občanům	občan	k1gMnPc3
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
i	i	k9
cizím	cizí	k2eAgMnPc3d1
státním	státní	k2eAgMnPc3d1
příslušníkům	příslušník	k1gMnPc3
za	za	k7c2
jejich	jejich	k3xOp3gFnSc2
přínos	přínos	k1gInSc4
k	k	k7c3
sociálně-ekonomickému	sociálně-ekonomický	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
<g/>
,	,	kIx,
formování	formování	k1gNnSc4
státu	stát	k1gInSc2
a	a	k8xC
posílení	posílení	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
suverenity	suverenita	k1gFnSc2
<g/>
.	.	kIx.
</s>