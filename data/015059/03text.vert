<s>
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
nezávislosti	nezávislost	k1gFnSc2
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
nezávislosti	nezávislost	k1gFnSc2
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Қ	Қ	k?
Р	Р	k?
т	т	k?
20	#num#	k4
ж	ж	k?
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
pamětní	pamětní	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
</s>
<s>
Založeno	založen	k2eAgNnSc1d1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Způsobilost	způsobilost	k1gFnSc1
</s>
<s>
občané	občan	k1gMnPc1
Kazachstánu	Kazachstán	k1gInSc2
i	i	k9
cizí	cizí	k2eAgMnPc1d1
státní	státní	k2eAgMnPc1d1
příslušníci	příslušník	k1gMnPc1
</s>
<s>
Uděluje	udělovat	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
</s>
<s>
přínos	přínos	k1gInSc1
při	při	k7c6
formování	formování	k1gNnSc6
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
posílení	posílení	k1gNnSc4
její	její	k3xOp3gFnSc2
suverenity	suverenita	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gInSc4
sociálně-ekonomický	sociálně-ekonomický	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Nursultan	Nursultan	k1gInSc1
Nazarbajev	Nazarbajev	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnSc1d1
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
10	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
nezávislosti	nezávislost	k1gFnSc2
Kazašské	kazašský	k2eAgFnSc2d1
republikyPamětní	republikyPamětný	k2eAgMnPc1d1
medaile	medaile	k1gFnPc4
25	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
nezávislosti	nezávislost	k1gFnSc2
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
nezávislosti	nezávislost	k1gFnSc2
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
kazašsky	kazašsky	k6eAd1
Қ	Қ	k?
Р	Р	k?
т	т	k?
20	#num#	k4
ж	ж	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
státní	státní	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
založené	založený	k2eAgFnSc2d1
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medaile	medaile	k1gFnSc1
je	být	k5eAaImIp3nS
udílena	udílet	k5eAaImNgFnS
občanům	občan	k1gMnPc3
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
i	i	k9
cizím	cizí	k2eAgMnPc3d1
státním	státní	k2eAgMnPc3d1
příslušníkům	příslušník	k1gMnPc3
za	za	k7c2
jejich	jejich	k3xOp3gFnSc2
přínos	přínos	k1gInSc4
k	k	k7c3
sociálně-ekonomickému	sociálně-ekonomický	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
<g/>
,	,	kIx,
formování	formování	k1gNnSc4
státu	stát	k1gInSc2
a	a	k8xC
posílení	posílení	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
suverenity	suverenita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
Nursultanem	Nursultan	k1gInSc7
Nazarbajevem	Nazarbajev	k1gInSc7
na	na	k7c6
základě	základ	k1gInSc6
dekretu	dekret	k1gInSc2
prezidenta	prezident	k1gMnSc2
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
č.	č.	k?
83	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pravidla	pravidlo	k1gNnPc1
udílení	udílení	k1gNnPc2
</s>
<s>
Medaile	medaile	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
udělena	udělit	k5eAaPmNgFnS
jak	jak	k9
občanům	občan	k1gMnPc3
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
cizím	cizí	k2eAgMnPc3d1
státním	státní	k2eAgMnPc3d1
příslušníkům	příslušník	k1gMnPc3
za	za	k7c2
jejich	jejich	k3xOp3gFnSc2
přínos	přínos	k1gInSc4
k	k	k7c3
formování	formování	k1gNnSc3
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
posílení	posílení	k1gNnSc4
její	její	k3xOp3gFnSc2
suverenity	suverenita	k1gFnSc2
a	a	k8xC
za	za	k7c4
její	její	k3xOp3gInSc4
sociálně-ekonomický	sociálně-ekonomický	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrhy	návrh	k1gInPc1
na	na	k7c4
udělení	udělení	k1gNnSc4
medaile	medaile	k1gFnSc2
jsou	být	k5eAaImIp3nP
předkládány	předkládán	k2eAgFnPc1d1
prezidentu	prezident	k1gMnSc3
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
například	například	k6eAd1
parlamentem	parlament	k1gInSc7
<g/>
,	,	kIx,
vládou	vláda	k1gFnSc7
<g/>
,	,	kIx,
veřejnými	veřejný	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
a	a	k8xC
dalšími	další	k2eAgInPc7d1
subjekty	subjekt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medaile	medaile	k1gFnSc1
je	být	k5eAaImIp3nS
udílena	udílet	k5eAaImNgFnS
jménem	jméno	k1gNnSc7
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ji	on	k3xPp3gFnSc4
také	také	k6eAd1
vyznamenaným	vyznamenaný	k2eAgMnPc3d1
předává	předávat	k5eAaImIp3nS
během	během	k7c2
slavnostního	slavnostní	k2eAgInSc2d1
ceremoniálu	ceremoniál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
v	v	k7c6
této	tento	k3xDgFnSc6
povinnosti	povinnost	k1gFnSc6
zastoupen	zastoupit	k5eAaPmNgMnS
některými	některý	k3yIgMnPc7
politiky	politik	k1gMnPc7
či	či	k8xC
státními	státní	k2eAgMnPc7d1
úředníky	úředník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
medailí	medaile	k1gFnSc7
je	být	k5eAaImIp3nS
příjemci	příjemce	k1gMnSc3
předáno	předán	k2eAgNnSc4d1
také	také	k9
osvědčení	osvědčení	k1gNnSc4
o	o	k7c4
udělení	udělení	k1gNnSc4
vyznamenání	vyznamenání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Seznam	seznam	k1gInSc1
vyznamenaných	vyznamenaný	k2eAgMnPc2d1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
tuto	tento	k3xDgFnSc4
medaili	medaile	k1gFnSc4
obdrželi	obdržet	k5eAaPmAgMnP
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
zveřejněn	zveřejnit	k5eAaPmNgInS
ve	v	k7c6
výnosu	výnos	k1gInSc6
prezidenta	prezident	k1gMnSc4
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
č.	č.	k?
172	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
O	o	k7c4
odměňování	odměňování	k1gNnSc4
Pamětní	pamětní	k2eAgFnSc7d1
medailí	medaile	k1gFnSc7
20	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
nezávislosti	nezávislost	k1gFnSc2
Kazašské	kazašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
medaile	medaile	k1gFnSc2
</s>
<s>
Medaile	medaile	k1gFnSc1
pravidelného	pravidelný	k2eAgInSc2d1
kulatého	kulatý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
o	o	k7c6
průměru	průměr	k1gInSc6
34	#num#	k4
mm	mm	kA
je	být	k5eAaImIp3nS
vyrobena	vyroben	k2eAgFnSc1d1
z	z	k7c2
mosazné	mosazný	k2eAgFnSc2d1
slitiny	slitina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přední	přední	k2eAgFnSc6d1
straně	strana	k1gFnSc6
medaile	medaile	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
vyobrazeno	vyobrazen	k2eAgNnSc4d1
logo	logo	k1gNnSc4
výročí	výročí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc1
reliéfního	reliéfní	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
číslo	číslo	k1gNnSc1
nula	nula	k1gFnSc1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
stylizovaného	stylizovaný	k2eAgNnSc2d1
slunce	slunce	k1gNnSc2
s	s	k7c7
paprsky	paprsek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pozadí	pozadí	k1gNnSc6
je	být	k5eAaImIp3nS
vlající	vlající	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
spodní	spodní	k2eAgFnSc6d1
části	část	k1gFnSc6
medaile	medaile	k1gFnSc2
je	být	k5eAaImIp3nS
reliéfní	reliéfní	k2eAgNnSc1d1
vyobrazení	vyobrazení	k1gNnSc1
prezidentského	prezidentský	k2eAgInSc2d1
paláce	palác	k1gInSc2
Akorda	Akordo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zadní	zadní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
medaile	medaile	k1gFnSc2
je	být	k5eAaImIp3nS
uprostřed	uprostřed	k6eAd1
reliéfní	reliéfní	k2eAgInSc1d1
nápis	nápis	k1gInSc1
Қ	Қ	k?
Р	Р	k?
Т	Т	k?
20	#num#	k4
ж	ж	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
kovové	kovový	k2eAgFnSc3d1
destičce	destička	k1gFnSc3
potažené	potažený	k2eAgFnSc2d1
stuhou	stuha	k1gFnSc7
je	být	k5eAaImIp3nS
medaile	medaile	k1gFnSc1
připojena	připojen	k2eAgFnSc1d1
pomocí	pomocí	k7c2
jednoduchého	jednoduchý	k2eAgInSc2d1
kroužku	kroužek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stuhou	stuha	k1gFnSc7
z	z	k7c2
hedvábného	hedvábný	k2eAgNnSc2d1
moaré	moaré	k1gNnSc2
je	být	k5eAaImIp3nS
potažena	potažen	k2eAgFnSc1d1
kovová	kovový	k2eAgFnSc1d1
destička	destička	k1gFnSc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
šestiúhelníku	šestiúhelník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Destička	destička	k1gFnSc1
je	být	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
50	#num#	k4
mm	mm	kA
a	a	k8xC
široká	široký	k2eAgFnSc1d1
32	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stuha	stuha	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
pruhem	pruh	k1gInSc7
červené	červený	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
širokým	široký	k2eAgNnSc7d1
16	#num#	k4
mm	mm	kA
a	a	k8xC
stejně	stejně	k6eAd1
širokým	široký	k2eAgInSc7d1
modrým	modrý	k2eAgInSc7d1
pruhem	pruh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
je	být	k5eAaImIp3nS
mosazná	mosazný	k2eAgFnSc1d1
8	#num#	k4
mm	mm	kA
široká	široký	k2eAgFnSc1d1
spona	spona	k1gFnSc1
zdobená	zdobený	k2eAgFnSc1d1
vavřínovými	vavřínový	k2eAgFnPc7d1
ratolestmi	ratolest	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
zadní	zadní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
stuhy	stuha	k1gFnSc2
je	být	k5eAaImIp3nS
špendlík	špendlík	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
připevnění	připevnění	k1gNnSc3
medaile	medaile	k1gFnSc2
k	k	k7c3
oděvu	oděv	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Medaile	medaile	k1gFnSc1
se	se	k3xPyFc4
nosí	nosit	k5eAaImIp3nS
na	na	k7c6
stužce	stužka	k1gFnSc6
nalevo	nalevo	k6eAd1
na	na	k7c6
hrudi	hruď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
dalších	další	k2eAgInPc2d1
kazašských	kazašský	k2eAgInPc2d1
řádů	řád	k1gInPc2
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
za	za	k7c7
nimi	on	k3xPp3gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Medaile	medaile	k1gFnPc1
byly	být	k5eAaImAgFnP
vyráběny	vyrábět	k5eAaImNgFnP
v	v	k7c6
kazašském	kazašský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Öskemen	Öskemen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
М	М	k?
«	«	k?
<g/>
20	#num#	k4
л	л	k?
н	н	k?
Р	Р	k?
К	К	k?
<g/>
»	»	k?
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
О	О	k?
ю	ю	k?
м	м	k?
в	в	k?
о	о	k?
20	#num#	k4
<g/>
-л	-л	k?
Н	Н	k?
Р	Р	k?
К	К	k?
-	-	kIx~
И	И	k?
"	"	kIx"
<g/>
Ә	Ә	k?
<g/>
"	"	kIx"
<g/>
.	.	kIx.
adilet	adilet	k1gInSc1
<g/>
.	.	kIx.
<g/>
zan	zan	k?
<g/>
.	.	kIx.
<g/>
kz	kz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Н	Н	k?
К	К	k?
<g/>
.	.	kIx.
М	М	k?
20	#num#	k4
Л	Л	k?
Н	Н	k?
<g/>
..	..	k?
wawards	wawards	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
