<s>
SMS	SMS	kA
</s>
<s>
Přijatá	přijatý	k2eAgFnSc1d1
SMS	SMS	kA
na	na	k7c6
mobilu	mobil	k1gInSc6
Motorola	Motorola	kA
RAZR	RAZR	kA
<g/>
.	.	kIx.
</s>
<s>
Nejběžnější	běžný	k2eAgNnSc1d3
rozložení	rozložení	k1gNnSc1
klávesnice	klávesnice	k1gFnSc2
tlačítkových	tlačítkový	k2eAgInPc2d1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
SMS	SMS	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Služba	služba	k1gFnSc1
krátkých	krátká	k2eAgFnPc2d1
textových	textový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
SMS	SMS	kA
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Short	Short	k2eAgFnPc2d1
message	message	k2eAgFnPc2d1
service	service	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
název	název	k1gInSc4
pro	pro	k7c4
službu	služba	k1gFnSc4
dostupnou	dostupný	k2eAgFnSc4d1
na	na	k7c6
většině	většina	k1gFnSc6
digitálních	digitální	k2eAgInPc2d1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprávu	zpráva	k1gFnSc4
lze	lze	k6eAd1
posílat	posílat	k5eAaImF
mezi	mezi	k7c7
mobilními	mobilní	k2eAgInPc7d1
telefony	telefon	k1gInPc7
<g/>
,	,	kIx,
jinými	jiný	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
<g/>
,	,	kIx,
na	na	k7c4
pevné	pevný	k2eAgInPc4d1
telefony	telefon	k1gInPc4
nebo	nebo	k8xC
přes	přes	k7c4
internet	internet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
zprávy	zpráva	k1gFnSc2
je	být	k5eAaImIp3nS
omezena	omezit	k5eAaPmNgFnS
na	na	k7c4
160	#num#	k4
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
často	často	k6eAd1
jako	jako	k9
SMS	SMS	kA
označuje	označovat	k5eAaImIp3nS
i	i	k9
samotná	samotný	k2eAgFnSc1d1
krátká	krátký	k2eAgFnSc1d1
textová	textový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Technologickým	technologický	k2eAgMnSc7d1
nástupcem	nástupce	k1gMnSc7
SMS	SMS	kA
jsou	být	k5eAaImIp3nP
zprávy	zpráva	k1gFnPc1
MMS	MMS	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
umožňují	umožňovat	k5eAaImIp3nP
posílat	posílat	k5eAaImF
i	i	k9
multimediální	multimediální	k2eAgInSc4d1
obsah	obsah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
SMS	SMS	kA
je	být	k5eAaImIp3nS
také	také	k9
možné	možný	k2eAgNnSc1d1
podpořit	podpořit	k5eAaPmF
charitu	charita	k1gFnSc4
či	či	k8xC
jinou	jiný	k2eAgFnSc4d1
veřejně	veřejně	k6eAd1
prospěšnou	prospěšný	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
typ	typ	k1gInSc4
SMS	SMS	kA
je	být	k5eAaImIp3nS
v	v	k7c6
ČR	ČR	kA
používán	používán	k2eAgInSc1d1
termín	termín	k1gInSc1
Dárcovská	dárcovský	k2eAgFnSc1d1
SMS	SMS	kA
–	–	k?
DMS	DMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozovatel	provozovatel	k1gMnSc1
DMS	DMS	kA
garantuje	garantovat	k5eAaBmIp3nS
minimální	minimální	k2eAgFnSc4d1
částku	částka	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
odvádí	odvádět	k5eAaImIp3nS
na	na	k7c4
konto	konto	k1gNnSc4
obecně	obecně	k6eAd1
prospěšné	prospěšný	k2eAgFnSc2d1
nadace	nadace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylá	zbylý	k2eAgFnSc1d1
část	část	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
úhradu	úhrada	k1gFnSc4
nákladů	náklad	k1gInPc2
operátora	operátor	k1gMnSc2
s	s	k7c7
provozem	provoz	k1gInSc7
služby	služba	k1gFnSc2
DMS	DMS	kA
<g/>
.	.	kIx.
</s>
<s>
Velikost	velikost	k1gFnSc1
zprávy	zpráva	k1gFnSc2
SMS	SMS	kA
</s>
<s>
SMS	SMS	kA
má	mít	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
160	#num#	k4
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
velikost	velikost	k1gFnSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
standardu	standard	k1gInSc2
GSM	GSM	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
stanovuje	stanovovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
text	text	k1gInSc4
SMS	SMS	kA
je	být	k5eAaImIp3nS
povoleno	povolit	k5eAaPmNgNnS
celkem	celkem	k6eAd1
1120	#num#	k4
bitů	bit	k1gInPc2
na	na	k7c4
jednu	jeden	k4xCgFnSc4
SMS	SMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
standardně	standardně	k6eAd1
používá	používat	k5eAaImIp3nS
7	#num#	k4
<g/>
bitové	bitový	k2eAgInPc1d1
kódování	kódování	k1gNnSc4
ve	v	k7c6
znakové	znakový	k2eAgFnSc6d1
sadě	sada	k1gFnSc6
GSM	GSM	kA
0	#num#	k4
<g/>
3.38	3.38	k4
<g/>
,	,	kIx,
odpovídá	odpovídat	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
160	#num#	k4
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
zpráva	zpráva	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
znaky	znak	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
v	v	k7c6
této	tento	k3xDgFnSc6
znakové	znakový	k2eAgFnSc6d1
sadě	sada	k1gFnSc6
obsaženy	obsáhnout	k5eAaPmNgFnP
nejsou	být	k5eNaImIp3nP
(	(	kIx(
<g/>
například	například	k6eAd1
všechny	všechen	k3xTgInPc4
české	český	k2eAgInPc4d1
znaky	znak	k1gInPc4
s	s	k7c7
diakritikou	diakritika	k1gFnSc7
kromě	kromě	k7c2
é	é	k0
<g/>
)	)	kIx)
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
16	#num#	k4
<g/>
bitové	bitový	k2eAgNnSc1d1
kódování	kódování	k1gNnSc4
UCS-	UCS-	k1gFnPc2
<g/>
2	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
do	do	k7c2
jedné	jeden	k4xCgFnSc2
SMS	SMS	kA
zprávy	zpráva	k1gFnSc2
vejde	vejít	k5eAaPmIp3nS
pouze	pouze	k6eAd1
70	#num#	k4
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moderní	moderní	k2eAgInPc1d1
mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
podporují	podporovat	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
dlouhých	dlouhý	k2eAgInPc2d1
SMS	SMS	kA
zpráv	zpráva	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
vlastně	vlastně	k9
několik	několik	k4yIc4
„	„	k?
<g/>
normálních	normální	k2eAgFnPc2d1
<g/>
“	“	k?
zpráv	zpráva	k1gFnPc2
tvářící	tvářící	k2eAgFnSc2d1
se	se	k3xPyFc4
jako	jako	k9
jedna	jeden	k4xCgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
informace	informace	k1gFnPc1
o	o	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
fragmentech	fragment	k1gInPc6
dlouhé	dlouhý	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
zapsat	zapsat	k5eAaPmF
do	do	k7c2
hlavičky	hlavička	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
délka	délka	k1gFnSc1
textu	text	k1gInSc2
jednoho	jeden	k4xCgInSc2
fragmentu	fragment	k1gInSc2
spojovatelné	spojovatelný	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
omezena	omezit	k5eAaPmNgFnS
na	na	k7c4
153	#num#	k4
znaků	znak	k1gInPc2
(	(	kIx(
<g/>
při	při	k7c6
7	#num#	k4
<g/>
bitovém	bitový	k2eAgNnSc6d1
kódování	kódování	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
resp.	resp.	kA
67	#num#	k4
znaků	znak	k1gInPc2
(	(	kIx(
<g/>
při	při	k7c6
16	#num#	k4
<g/>
bitovém	bitový	k2eAgNnSc6d1
kódování	kódování	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhé	Dlouhá	k1gFnSc2
SMS	SMS	kA
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
skládat	skládat	k5eAaImF
v	v	k7c6
principu	princip	k1gInSc6
z	z	k7c2
maximálně	maximálně	k6eAd1
255	#num#	k4
fragmentů	fragment	k1gInPc2
<g/>
,	,	kIx,
mobilní	mobilní	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
v	v	k7c6
praxi	praxe	k1gFnSc6
však	však	k9
podporují	podporovat	k5eAaImIp3nP
zhruba	zhruba	k6eAd1
6	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
fragmentů	fragment	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
přístroje	přístroj	k1gInPc4
od	od	k7c2
určitého	určitý	k2eAgInSc2d1
počtu	počet	k1gInSc2
znaků	znak	k1gInPc2
automaticky	automaticky	k6eAd1
odesílají	odesílat	k5eAaImIp3nP
zprávu	zpráva	k1gFnSc4
jako	jako	k8xS,k8xC
MMS	MMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
pro	pro	k7c4
odesílatele	odesílatel	k1gMnSc4
i	i	k9
cenově	cenově	k6eAd1
výhodnější	výhodný	k2eAgMnSc1d2
<g/>
,	,	kIx,
neboť	neboť	k8xC
operátoři	operátor	k1gMnPc1
obvykle	obvykle	k6eAd1
účtují	účtovat	k5eAaImIp3nP
poplatek	poplatek	k1gInSc4
za	za	k7c4
každý	každý	k3xTgInSc4
odeslaný	odeslaný	k2eAgInSc4d1
fragment	fragment	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
však	však	k9
dobré	dobrý	k2eAgNnSc1d1
se	se	k3xPyFc4
ujistit	ujistit	k5eAaPmF
<g/>
,	,	kIx,
jestli	jestli	k8xS
příjemce	příjemce	k1gMnPc4
může	moct	k5eAaImIp3nS
MMS	MMS	kA
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
zařízení	zařízení	k1gNnSc6
přijmout	přijmout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
SMS	SMS	kA
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Krátké	Krátké	k2eAgFnPc1d1
textové	textový	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
se	se	k3xPyFc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
pro	pro	k7c4
zasílání	zasílání	k1gNnSc4
informací	informace	k1gFnPc2
a	a	k8xC
vzájemné	vzájemný	k2eAgNnSc1d1
dorozumívání	dorozumívání	k1gNnSc1
mezi	mezi	k7c7
uživateli	uživatel	k1gMnPc7
mobilních	mobilní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
GSM	GSM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telekomunikační	telekomunikační	k2eAgInSc4d1
operátor	operátor	k1gInSc4
O2	O2	k1gFnSc2
také	také	k9
umožňuje	umožňovat	k5eAaImIp3nS
zasílání	zasílání	k1gNnSc4
SMS	SMS	kA
z	z	k7c2
pevné	pevný	k2eAgFnSc2d1
linky	linka	k1gFnSc2
na	na	k7c4
mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
rovněž	rovněž	k9
možné	možný	k2eAgNnSc1d1
zaslat	zaslat	k5eAaPmF
SMS	SMS	kA
na	na	k7c4
pevnou	pevný	k2eAgFnSc4d1
linku	linka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
SMS	SMS	kA
strojovým	strojový	k2eAgInSc7d1
hlasem	hlas	k1gInSc7
dvakrát	dvakrát	k6eAd1
přečtena	přečten	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
má	mít	k5eAaImIp3nS
uživatel	uživatel	k1gMnSc1
klasickou	klasický	k2eAgFnSc4d1
pevnou	pevný	k2eAgFnSc4d1
linku	linka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nových	nový	k2eAgInPc6d1
telefonech	telefon	k1gInPc6
s	s	k7c7
displejem	displej	k1gInSc7
se	se	k3xPyFc4
SMS	SMS	kA
zobrazuje	zobrazovat	k5eAaImIp3nS
klasicky	klasicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
SMS	SMS	kA
zprávy	zpráva	k1gFnPc1
jsou	být	k5eAaImIp3nP
využívány	využíván	k2eAgMnPc4d1
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
mobilního	mobilní	k2eAgInSc2d1
marketingu	marketing	k1gInSc2
na	na	k7c4
podporu	podpora	k1gFnSc4
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
služeb	služba	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
komerčních	komerční	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
(	(	kIx(
<g/>
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Premium	Premium	k1gNnSc4
Rate	Rate	k1gNnSc2
SMS	SMS	kA
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
PR	pr	k0
SMS	SMS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
krátkých	krátká	k1gFnPc2
textových	textový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
hlasovat	hlasovat	k5eAaImF
v	v	k7c6
soutěžích	soutěž	k1gFnPc6
či	či	k8xC
dostávat	dostávat	k5eAaImF
informace	informace	k1gFnPc4
a	a	k8xC
zpravodajství	zpravodajství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interaktivní	interaktivní	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
v	v	k7c6
těchto	tento	k3xDgInPc6
případech	případ	k1gInPc6
umožňují	umožňovat	k5eAaImIp3nP
komunikaci	komunikace	k1gFnSc4
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc4
hlasování	hlasování	k1gNnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zobrazit	zobrazit	k5eAaPmF
okamžitě	okamžitě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěžní	soutěžní	k2eAgInPc1d1
SMS	SMS	kA
ze	z	k7c2
strany	strana	k1gFnSc2
zákazníků	zákazník	k1gMnPc2
se	se	k3xPyFc4
zasílají	zasílat	k5eAaImIp3nP
na	na	k7c4
klasické	klasický	k2eAgNnSc4d1
mobilní	mobilní	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
s	s	k7c7
přijímací	přijímací	k2eAgFnSc7d1
GSM	GSM	kA
bránou	brána	k1gFnSc7
anebo	anebo	k8xC
na	na	k7c4
číslo	číslo	k1gNnSc4
společné	společný	k2eAgNnSc4d1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
operátory	operátor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
tvaru	tvar	k1gInSc6
90Y	90Y	k4
YY	YY	kA
XX	XX	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
XX	XX	kA
určuje	určovat	k5eAaImIp3nS
cenu	cena	k1gFnSc4
SMS	SMS	kA
v	v	k7c6
korunách	koruna	k1gFnPc6
včetně	včetně	k7c2
DPH	DPH	kA
a	a	k8xC
YYY	YYY	kA
jednoznačně	jednoznačně	k6eAd1
určuje	určovat	k5eAaImIp3nS
poskytovatele	poskytovatel	k1gMnPc4
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
za	za	k7c4
odeslání	odeslání	k1gNnSc4
takových	takový	k3xDgInPc2
SMS	SMS	kA
zpráv	zpráva	k1gFnPc2
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
mezi	mezi	k7c7
poskytovatelem	poskytovatel	k1gMnSc7
obsahu	obsah	k1gInSc2
(	(	kIx(
<g/>
například	například	k6eAd1
televizí	televize	k1gFnSc7
či	či	k8xC
jiným	jiný	k2eAgNnSc7d1
médiem	médium	k1gNnSc7
<g/>
,	,	kIx,
pořadatelem	pořadatel	k1gMnSc7
soutěže	soutěž	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poskytovatelem	poskytovatel	k1gMnSc7
propojení	propojení	k1gNnSc2
a	a	k8xC
mobilním	mobilní	k2eAgInSc7d1
operátorem	operátor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Jestliže	jestliže	k8xS
je	být	k5eAaImIp3nS
soutěž	soutěž	k1gFnSc1
či	či	k8xC
marketingová	marketingový	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
většího	veliký	k2eAgInSc2d2
rozsahu	rozsah	k1gInSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
pořádající	pořádající	k2eAgFnSc1d1
firma	firma	k1gFnSc1
propojit	propojit	k5eAaPmF
přímo	přímo	k6eAd1
s	s	k7c7
ústřednou	ústředna	k1gFnSc7
mobilního	mobilní	k2eAgMnSc2d1
operátora	operátor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
garantují	garantovat	k5eAaBmIp3nP
smlouvy	smlouva	k1gFnPc4
určitý	určitý	k2eAgInSc4d1
minimální	minimální	k2eAgInSc4d1
objem	objem	k1gInSc4
za	za	k7c4
zaslané	zaslaný	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
propojení	propojení	k1gNnSc1
s	s	k7c7
operátorem	operátor	k1gInSc7
je	být	k5eAaImIp3nS
však	však	k9
velmi	velmi	k6eAd1
drahé	drahý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
existují	existovat	k5eAaImIp3nP
firmy	firma	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
mají	mít	k5eAaImIp3nP
s	s	k7c7
operátorem	operátor	k1gInSc7
zařízeno	zařízen	k2eAgNnSc1d1
přímé	přímý	k2eAgNnSc1d1
propojení	propojení	k1gNnSc1
a	a	k8xC
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP
své	svůj	k3xOyFgFnPc4
služby	služba	k1gFnPc4
ostatním	ostatní	k2eAgMnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgFnSc4d3
firmy	firma	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
poskytují	poskytovat	k5eAaImIp3nP
třetím	třetí	k4xOgMnSc6
subjektům	subjekt	k1gInPc3
komerční	komerční	k2eAgNnPc4d1
SMS	SMS	kA
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
patří	patřit	k5eAaImIp3nS
Comgate	Comgat	k1gInSc5
a	a	k8xC
Axima	Aximum	k1gNnPc1
nebo	nebo	k8xC
firmy	firma	k1gFnPc1
specializující	specializující	k2eAgFnPc1d1
se	se	k3xPyFc4
na	na	k7c4
mobilní	mobilní	k2eAgInSc4d1
marketing	marketing	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
PR	pr	k0
SMS	SMS	kA
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
marketingového	marketingový	k2eAgInSc2d1
mixu	mix	k1gInSc2
připraveného	připravený	k2eAgInSc2d1
kampani	kampaň	k1gFnSc3
na	na	k7c4
míru	míra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provize	provize	k1gFnSc1
pro	pro	k7c4
zadavatele	zadavatel	k1gMnPc4
<g/>
,	,	kIx,
v	v	k7c6
řádu	řád	k1gInSc6
několika	několik	k4yIc2
desítek	desítka	k1gFnPc2
procent	procento	k1gNnPc2
z	z	k7c2
ceny	cena	k1gFnSc2
SMS	SMS	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
smluv	smlouva	k1gFnPc2
s	s	k7c7
realizátorem	realizátor	k1gMnSc7
kampaně	kampaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příkladem	příklad	k1gInSc7
komerčního	komerční	k2eAgInSc2d1
využít	využít	k5eAaPmF
SMS	SMS	kA
zprávy	zpráva	k1gFnPc1
bylo	být	k5eAaImAgNnS
například	například	k6eAd1
zvláštní	zvláštní	k2eAgNnSc4d1
předplatné	předplatné	k1gNnSc4
erotické	erotický	k2eAgFnSc2d1
linky	linka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odesláním	odeslání	k1gNnSc7
zpoplatněné	zpoplatněný	k2eAgFnSc2d1
textové	textový	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
s	s	k7c7
klíčovým	klíčový	k2eAgNnSc7d1
slovem	slovo	k1gNnSc7
došlo	dojít	k5eAaPmAgNnS
automaticky	automaticky	k6eAd1
k	k	k7c3
aktivaci	aktivace	k1gFnSc3
přístupu	přístup	k1gInSc2
na	na	k7c4
místní	místní	k2eAgNnSc4d1
telefonní	telefonní	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
ve	v	k7c6
tvaru	tvar	k1gInSc6
2	#num#	k4
<g/>
xx	xx	k?
xxx	xxx	k?
xxx	xxx	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
uživatel	uživatel	k1gMnSc1
své	svůj	k3xOyFgNnSc4
předplatné	předplatné	k1gNnSc4
nezrušil	zrušit	k5eNaPmAgInS
<g/>
,	,	kIx,
odečítala	odečítat	k5eAaImAgFnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
z	z	k7c2
kreditu	kredit	k1gInSc2
či	či	k8xC
na	na	k7c4
fakturu	faktura	k1gFnSc4
operátora	operátor	k1gMnSc2
částka	částka	k1gFnSc1
19,50	19,50	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služba	služba	k1gFnSc1
byla	být	k5eAaImAgFnS
zřejmě	zřejmě	k6eAd1
ojedinělá	ojedinělý	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
většina	většina	k1gFnSc1
erotických	erotický	k2eAgFnPc2d1
linek	linka	k1gFnPc2
či	či	k8xC
audiotextových	audiotextův	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
je	být	k5eAaImIp3nS
poskytována	poskytovat	k5eAaImNgFnS
na	na	k7c6
číslech	číslo	k1gNnPc6
se	se	k3xPyFc4
zvláštním	zvláštní	k2eAgInSc7d1
tarifem	tarif	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc4
druh	druh	k1gInSc4
SMS	SMS	kA
předplatného	předplatný	k2eAgInSc2d1
provozovatelem	provozovatel	k1gMnSc7
erotické	erotický	k2eAgFnSc2d1
linky	linka	k1gFnSc2
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
použití	použití	k1gNnSc4
Premium	Premium	k1gNnSc1
Rate	Rat	k1gInSc2
SMS	SMS	kA
existují	existovat	k5eAaImIp3nP
přísná	přísný	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
SMS	SMS	kA
je	být	k5eAaImIp3nS
například	například	k6eAd1
možné	možný	k2eAgNnSc1d1
používat	používat	k5eAaImF
pouze	pouze	k6eAd1
pro	pro	k7c4
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
mobilním	mobilní	k2eAgInSc7d1
telefonem	telefon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidla	pravidlo	k1gNnPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
podle	podle	k7c2
typu	typ	k1gInSc2
poskytovatele	poskytovatel	k1gMnSc2
Premium	Premium	k1gNnSc1
SMS	SMS	kA
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Rekordy	rekord	k1gInPc1
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
jsou	být	k5eAaImIp3nP
v	v	k7c4
užívání	užívání	k1gNnSc4
SMS	SMS	kA
velmi	velmi	k6eAd1
zběhlí	zběhlý	k2eAgMnPc1d1
a	a	k8xC
stále	stále	k6eAd1
je	být	k5eAaImIp3nS
trend	trend	k1gInSc1
takový	takový	k3xDgInSc1
<g/>
,	,	kIx,
že	že	k8xS
každým	každý	k3xTgInSc7
rokem	rok	k1gInSc7
vzroste	vzrůst	k5eAaPmIp3nS
objem	objem	k1gInSc1
poslaných	poslaný	k2eAgMnPc2d1
SMS	SMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
za	za	k7c4
to	ten	k3xDgNnSc4
především	především	k6eAd1
přijatelně	přijatelně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
jednu	jeden	k4xCgFnSc4
odeslanou	odeslaný	k2eAgFnSc4d1
SMS	SMS	kA
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
160	#num#	k4
znaků	znak	k1gInPc2
bývá	bývat	k5eAaImIp3nS
dostačující	dostačující	k2eAgInSc1d1
objem	objem	k1gInSc1
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Klasickým	klasický	k2eAgMnSc7d1
„	„	k?
<g/>
rekordním	rekordní	k2eAgInSc7d1
<g/>
“	“	k?
obdobím	období	k1gNnSc7
bývají	bývat	k5eAaImIp3nP
Vánoce	Vánoce	k1gFnPc1
a	a	k8xC
Nový	nový	k2eAgInSc1d1
rok	rok	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zatím	zatím	k6eAd1
v	v	k7c6
těchto	tento	k3xDgNnPc6
obdobích	období	k1gNnPc6
každým	každý	k3xTgInSc7
rokem	rok	k1gInSc7
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
rekord	rekord	k1gInSc4
za	za	k7c4
předchozí	předchozí	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
prošlo	projít	k5eAaPmAgNnS
všemi	všecek	k3xTgFnPc7
českými	český	k2eAgFnPc7d1
sítěmi	síť	k1gFnPc7
na	na	k7c4
Vánoce	Vánoce	k1gFnPc4
58,5	58,5	k4
milionů	milion	k4xCgInPc2
SMS	SMS	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
každého	každý	k3xTgMnSc4
občana	občan	k1gMnSc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
kojenců	kojenec	k1gMnPc2
a	a	k8xC
starých	starý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
)	)	kIx)
připadlo	připadnout	k5eAaPmAgNnS
6	#num#	k4
odeslaných	odeslaný	k2eAgInPc2d1
SMS	SMS	kA
za	za	k7c4
jeden	jeden	k4xCgInSc4
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Silvestra	Silvestr	k1gMnSc4
bylo	být	k5eAaImAgNnS
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
odesláno	odeslat	k5eAaPmNgNnS
33	#num#	k4
milionů	milion	k4xCgInPc2
textových	textový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
posláno	poslat	k5eAaPmNgNnS
přes	přes	k7c4
70	#num#	k4
milionů	milion	k4xCgInPc2
SMS	SMS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
marketingové	marketingový	k2eAgFnSc2d1
akce	akce	k1gFnSc2
„	„	k?
<g/>
Děkujeme	děkovat	k5eAaImIp1nP
<g/>
,	,	kIx,
jste	být	k5eAaImIp2nP
jedničky	jednička	k1gFnPc4
<g/>
“	“	k?
firmy	firma	k1gFnSc2
T-Mobile	T-Mobila	k1gFnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2005	#num#	k4
byly	být	k5eAaImAgFnP
SMS	SMS	kA
ze	z	k7c2
sítě	síť	k1gFnSc2
T-Mobile	T-Mobila	k1gFnSc6
naprosto	naprosto	k6eAd1
zdarma	zdarma	k6eAd1
prošlo	projít	k5eAaPmAgNnS
jenom	jenom	k9
z	z	k7c2
této	tento	k3xDgFnSc2
sítě	síť	k1gFnSc2
50,6	50,6	k4
milionů	milion	k4xCgInPc2
SMS	SMS	kA
a	a	k8xC
v	v	k7c6
neděli	neděle	k1gFnSc6
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
54	#num#	k4
milionů	milion	k4xCgInPc2
SMS	SMS	kA
<g/>
.	.	kIx.
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
úspěšnou	úspěšný	k2eAgFnSc7d1
službou	služba	k1gFnSc7
Premium	Premium	k1gNnSc1
SMS	SMS	kA
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
hlasování	hlasování	k1gNnSc1
do	do	k7c2
první	první	k4xOgFnSc2
řady	řada	k1gFnSc2
soutěže	soutěž	k1gFnSc2
Česko	Česko	k1gNnSc1
hledá	hledat	k5eAaImIp3nS
SuperStar	superstar	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
provoz	provoz	k1gInSc1
srovnatelný	srovnatelný	k2eAgInSc1d1
se	s	k7c7
silvestrovským	silvestrovský	k2eAgNnSc7d1
zatížením	zatížení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
jedna	jeden	k4xCgFnSc1
odeslaná	odeslaný	k2eAgFnSc1d1
SMS	SMS	kA
stála	stát	k5eAaImAgFnS
6	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s>
SMS	SMS	kA
brány	brána	k1gFnPc1
</s>
<s>
Mobilní	mobilní	k2eAgMnPc1d1
operátoři	operátor	k1gMnPc1
poskytují	poskytovat	k5eAaImIp3nP
svým	svůj	k3xOyFgMnPc3
zákazníkům	zákazník	k1gMnPc3
či	či	k8xC
veřejnosti	veřejnost	k1gFnSc2
možnost	možnost	k1gFnSc4
zaslat	zaslat	k5eAaPmF
(	(	kIx(
<g/>
bezplatnou	bezplatný	k2eAgFnSc4d1
či	či	k8xC
zpoplatněnou	zpoplatněný	k2eAgFnSc4d1
<g/>
)	)	kIx)
textovou	textový	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
do	do	k7c2
své	svůj	k3xOyFgFnSc2
vlastní	vlastní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k9
do	do	k7c2
sítě	síť	k1gFnSc2
jiných	jiný	k2eAgMnPc2d1
operátorů	operátor	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
rozesílání	rozesílání	k1gNnSc2
hromadných	hromadný	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
služba	služba	k1gFnSc1
je	být	k5eAaImIp3nS
přístupná	přístupný	k2eAgFnSc1d1
ze	z	k7c2
stránek	stránka	k1gFnPc2
mobilního	mobilní	k2eAgMnSc2d1
operátora	operátor	k1gMnSc2
anebo	anebo	k8xC
přes	přes	k7c4
jiné	jiný	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Veřejné	veřejný	k2eAgFnPc4d1
SMS	SMS	kA
brány	brána	k1gFnPc4
</s>
<s>
Dříve	dříve	k6eAd2
bylo	být	k5eAaImAgNnS
v	v	k7c6
ČR	ČR	kA
zasílání	zasílání	k1gNnPc2
SMS	SMS	kA
zpráv	zpráva	k1gFnPc2
z	z	k7c2
bran	brána	k1gFnPc2
mobilních	mobilní	k2eAgInPc2d1
operátorů	operátor	k1gInPc2
omezené	omezený	k2eAgInPc1d1
jenom	jenom	k6eAd1
počtem	počet	k1gInSc7
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
svojí	svojit	k5eAaImIp3nS
SMS	SMS	kA
bránu	brána	k1gFnSc4
omezil	omezit	k5eAaPmAgInS
operátor	operátor	k1gMnSc1
Eurotel	Eurotel	kA
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
O	o	k7c4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožňuje	umožňovat	k5eAaImIp3nS
zaslání	zaslání	k1gNnSc4
zprávy	zpráva	k1gFnSc2
zdarma	zdarma	k6eAd1
o	o	k7c6
délce	délka	k1gFnSc6
60	#num#	k4
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
U	u	k7c2
mobilního	mobilní	k2eAgMnSc2d1
operátora	operátor	k1gMnSc2
T-Mobile	T-Mobila	k1gFnSc6
je	být	k5eAaImIp3nS
SMS	SMS	kA
brána	brát	k5eAaImNgFnS
veřejně	veřejně	k6eAd1
dostupná	dostupný	k2eAgFnSc1d1
<g/>
,	,	kIx,
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
internetovém	internetový	k2eAgInSc6d1
portálu	portál	k1gInSc6
t-mobile	t-mobile	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s>
Mobilní	mobilní	k2eAgInSc4d1
operátor	operátor	k1gInSc4
Vodafone	Vodafon	k1gInSc5
nabízí	nabízet	k5eAaImIp3nS
zaslání	zaslání	k1gNnSc4
SMS	SMS	kA
do	do	k7c2
své	svůj	k3xOyFgFnSc2
sítě	síť	k1gFnSc2
zdarma	zdarma	k6eAd1
všem	všecek	k3xTgMnPc3
bez	bez	k7c2
omezení	omezení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Lze	lze	k6eAd1
využít	využít	k5eAaPmF
služeb	služba	k1gFnPc2
dalších	další	k2eAgNnPc2d1
internetových	internetový	k2eAgNnPc2d1
SMS	SMS	kA
bran	brána	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
odesílají	odesílat	k5eAaImIp3nP
SMS	SMS	kA
zdarma	zdarma	k6eAd1
do	do	k7c2
všech	všecek	k3xTgFnPc2
sítí	síť	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
nutná	nutný	k2eAgFnSc1d1
registrace	registrace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poskytovatelé	poskytovatel	k1gMnPc1
telekomunikačních	telekomunikační	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
ale	ale	k8xC
tuto	tento	k3xDgFnSc4
službu	služba	k1gFnSc4
omezují	omezovat	k5eAaImIp3nP
<g/>
,	,	kIx,
operátor	operátor	k1gInSc4
Vodafone	Vodafon	k1gInSc5
službu	služba	k1gFnSc4
k	k	k7c3
30	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
ukončil	ukončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházela	nacházet	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c6
stránkách	stránka	k1gFnPc6
vodafone	vodafon	k1gInSc5
<g/>
.	.	kIx.
<g/>
park	park	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operátor	operátor	k1gInSc1
O	o	k7c4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
také	také	k9
vlastní	vlastní	k2eAgFnSc4d1
bránu	brána	k1gFnSc4
ji	on	k3xPp3gFnSc4
o	o	k7c4
rok	rok	k1gInSc4
dříve	dříve	k6eAd2
zrušil	zrušit	k5eAaPmAgMnS
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
zákazníky	zákazník	k1gMnPc4
v	v	k7c4
mojeo	mojeo	k1gNnSc4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
také	také	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
trend	trend	k1gInSc1
nastal	nastat	k5eAaPmAgInS
i	i	k9
s	s	k7c7
nástupem	nástup	k1gInSc7
tzv.	tzv.	kA
virtuálních	virtuální	k2eAgInPc2d1
operátorů	operátor	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ji	on	k3xPp3gFnSc4
provozuje	provozovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
jediný	jediný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1
spamu	spamat	k5eAaPmIp1nS
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
SMS	SMS	kA
branami	brána	k1gFnPc7
i	i	k8xC
komerčním	komerční	k2eAgNnSc7d1
zasíláním	zasílání	k1gNnSc7
SMS	SMS	kA
existuje	existovat	k5eAaImIp3nS
riziko	riziko	k1gNnSc4
spamu	spam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
řeší	řešit	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
problematiku	problematika	k1gFnSc4
zákon	zákon	k1gInSc1
o	o	k7c6
některých	některý	k3yIgFnPc6
službách	služba	k1gFnPc6
informační	informační	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zaslání	zaslání	k1gNnSc4
obchodního	obchodní	k2eAgNnSc2d1
sdělení	sdělení	k1gNnSc2
pomocí	pomocí	k7c2
SMS	SMS	kA
je	být	k5eAaImIp3nS
vyžadován	vyžadován	k2eAgInSc1d1
prokazatelný	prokazatelný	k2eAgInSc1d1
souhlas	souhlas	k1gInSc1
příjemce	příjemce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
subjekt	subjekt	k1gInSc4
prokazatelný	prokazatelný	k2eAgInSc1d1
souhlas	souhlas	k1gInSc1
nevlastní	vlastnit	k5eNaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zasílání	zasílání	k1gNnSc1
obchodních	obchodní	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
zakázáno	zakázat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Podvržení	podvržení	k1gNnSc1
SMS	SMS	kA
zpráv	zpráva	k1gFnPc2
</s>
<s>
Během	během	k7c2
různého	různý	k2eAgNnSc2d1
období	období	k1gNnSc2
se	se	k3xPyFc4
na	na	k7c6
Internetu	Internet	k1gInSc6
objevovaly	objevovat	k5eAaImAgFnP
služby	služba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nabízely	nabízet	k5eAaImAgFnP
možnost	možnost	k1gFnSc4
zneužít	zneužít	k5eAaPmF
a	a	k8xC
zasílat	zasílat	k5eAaImF
falešné	falešný	k2eAgFnPc4d1
SMS	SMS	kA
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
zprávy	zpráva	k1gFnPc1
vypadaly	vypadat	k5eAaPmAgFnP,k5eAaImAgFnP
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
kdyby	kdyby	kYmCp3nP
byly	být	k5eAaImAgFnP
odeslány	odeslán	k2eAgFnPc1d1
někým	někdo	k3yInSc7
jiným	jiný	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
posledních	poslední	k2eAgFnPc2d1
kauz	kauza	k1gFnPc2
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
koncem	koncem	k7c2
srpna	srpen	k1gInSc2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
on-line	on-lin	k1gInSc5
službu	služba	k1gFnSc4
serveru	server	k1gInSc2
SMSspoofing	SMSspoofing	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nabízel	nabízet	k5eAaImAgInS
po	po	k7c4
registraci	registrace	k1gFnSc4
možnost	možnost	k1gFnSc4
simulovat	simulovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
SMS	SMS	kA
odeslala	odeslat	k5eAaPmAgFnS
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
mobilního	mobilní	k2eAgInSc2d1
přístroje	přístroj	k1gInSc2
jiná	jiný	k2eAgFnSc1d1
osoba	osoba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služba	služba	k1gFnSc1
fungovala	fungovat	k5eAaImAgFnS
také	také	k9
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
a	a	k8xC
všechny	všechen	k3xTgInPc1
tři	tři	k4xCgInPc1
mobilní	mobilní	k2eAgInPc1d1
operátory	operátor	k1gInPc1
(	(	kIx(
<g/>
Eurotel	Eurotel	kA
<g/>
,	,	kIx,
T-Mobile	T-Mobila	k1gFnSc6
<g/>
,	,	kIx,
Oskar	Oskar	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Doručování	doručování	k1gNnSc1
takových	takový	k3xDgFnPc2
podvržených	podvržený	k2eAgFnPc2d1
SMS	SMS	kA
zpráv	zpráva	k1gFnPc2
bylo	být	k5eAaImAgNnS
po	po	k7c6
spuštění	spuštění	k1gNnSc6
služby	služba	k1gFnSc2
zastaveno	zastavit	k5eAaPmNgNnS
například	například	k6eAd1
mobilním	mobilní	k2eAgInSc7d1
operátorem	operátor	k1gInSc7
Eurotel	Eurotel	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
odborného	odborný	k2eAgInSc2d1
serveru	server	k1gInSc2
Mobil	mobil	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
existoval	existovat	k5eAaImAgMnS
pro	pro	k7c4
rozpoznání	rozpoznání	k1gNnSc4
falešných	falešný	k2eAgMnPc2d1
SMS	SMS	kA
zaslaných	zaslaný	k2eAgMnPc2d1
touto	tento	k3xDgFnSc7
službou	služba	k1gFnSc7
následující	následující	k2eAgInSc1d1
postup	postup	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Pokud	pokud	k8xS
si	se	k3xPyFc3
pravost	pravost	k1gFnSc4
zprávy	zpráva	k1gFnSc2
neověříte	ověřit	k5eNaPmIp2nP
přímo	přímo	k6eAd1
u	u	k7c2
odesílatele	odesílatel	k1gMnSc2
<g/>
,	,	kIx,
můžete	moct	k5eAaImIp2nP
se	se	k3xPyFc4
podívat	podívat	k5eAaPmF,k5eAaImF
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
z	z	k7c2
jakého	jaký	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
SMS	SMS	kA
centra	centrum	k1gNnSc2
byla	být	k5eAaImAgFnS
zpráva	zpráva	k1gFnSc1
odeslána	odeslat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
telefony	telefon	k1gInPc1
toto	tento	k3xDgNnSc4
číslo	číslo	k1gNnSc4
uvádějí	uvádět	k5eAaImIp3nP
v	v	k7c6
detailních	detailní	k2eAgFnPc6d1
informacích	informace	k1gFnPc6
u	u	k7c2
každé	každý	k3xTgFnSc2
SMS	SMS	kA
<g/>
.	.	kIx.
</s>
<s>
SMSspoofing	SMSspoofing	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
+447797706025	+447797706025	k4
<g/>
.	.	kIx.
</s>
<s>
Eurotel	Eurotel	kA
<g/>
/	/	kIx~
<g/>
O	o	k7c4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
+420602909909	+420602909909	k4
</s>
<s>
Oskar	Oskar	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Vodafone	Vodafon	k1gInSc5
<g/>
:	:	kIx,
+420608005681	+420608005681	k4
</s>
<s>
T-Mobile	T-Mobile	k6eAd1
<g/>
:	:	kIx,
+420603052000	+420603052000	k4
</s>
<s>
Dosud	dosud	k6eAd1
(	(	kIx(
<g/>
rok	rok	k1gInSc1
2009	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
také	také	k9
možné	možný	k2eAgNnSc1d1
uvést	uvést	k5eAaPmF
nesprávné	správný	k2eNgNnSc4d1
odesílací	odesílací	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
u	u	k7c2
brány	brána	k1gFnSc2
operátora	operátor	k1gMnSc2
Vodafone	Vodafon	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takové	takový	k3xDgFnSc6
zprávě	zpráva	k1gFnSc6
je	být	k5eAaImIp3nS
však	však	k9
uveden	uveden	k2eAgInSc1d1
příznak	příznak	k1gInSc1
odesílatele	odesílatel	k1gMnSc2
„	„	k?
<g/>
WWW	WWW	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
může	moct	k5eAaImIp3nS
adresáta	adresát	k1gMnSc4
v	v	k7c6
případě	případ	k1gInSc6
pochybností	pochybnost	k1gFnPc2
upozornit	upozornit	k5eAaPmF
na	na	k7c4
nutnost	nutnost	k1gFnSc4
určité	určitý	k2eAgFnSc2d1
obezřetnosti	obezřetnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1
službu	služba	k1gFnSc4
lze	lze	k6eAd1
využít	využít	k5eAaPmF
i	i	k9
v	v	k7c6
rámci	rámec	k1gInSc6
aplikace	aplikace	k1gFnSc2
Oskárek	Oskárka	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
odesílat	odesílat	k5eAaImF
tzv.	tzv.	kA
aprílové	aprílový	k2eAgFnSc6d1
SMS	SMS	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
lze	lze	k6eAd1
určit	určit	k5eAaPmF
odesílatele	odesílatel	k1gMnPc4
jako	jako	k8xC,k8xS
text	text	k1gInSc4
o	o	k7c6
délce	délka	k1gFnSc6
až	až	k9
11	#num#	k4
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doručená	doručený	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
zobrazit	zobrazit	k5eAaPmF
např.	např.	kA
s	s	k7c7
odesílatelem	odesílatel	k1gMnSc7
"	"	kIx"
<g/>
Fantomas	Fantomas	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Jezisek	Jezisek	k1gInSc1
<g/>
"	"	kIx"
nebo	nebo	k8xC
"	"	kIx"
<g/>
Operator	Operator	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
umělecká	umělecký	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Ztohoven	Ztohovna	k1gFnPc2
posílala	posílat	k5eAaImAgFnS
falešné	falešný	k2eAgFnSc2d1
sms	sms	kA
vzájemně	vzájemně	k6eAd1
mezi	mezi	k7c7
různými	různý	k2eAgMnPc7d1
poslanci	poslanec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Dle	dle	k7c2
statistik	statistika	k1gFnPc2
agentury	agentura	k1gFnSc2
Nielsen	Nielsen	k1gInSc1
poslal	poslat	k5eAaPmAgMnS
průměrný	průměrný	k2eAgMnSc1d1
Američan	Američan	k1gMnSc1
ve	v	k7c6
druhém	druhý	k4xOgInSc6
čtvrtletí	čtvrtletí	k1gNnSc6
roku	rok	k1gInSc2
2008	#num#	k4
357	#num#	k4
textových	textový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
měsíčně	měsíčně	k6eAd1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
hovorů	hovora	k1gMnPc2
uskutečnil	uskutečnit	k5eAaPmAgInS
jen	jen	k9
204	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
se	se	k3xPyFc4
tak	tak	k6eAd1
změnila	změnit	k5eAaPmAgFnS
dlouho	dlouho	k6eAd1
zažitá	zažitý	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
-	-	kIx~
nepopulární	populární	k2eNgFnPc1d1
textové	textový	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
předčily	předčit	k5eAaBmAgFnP,k5eAaPmAgFnP
klasické	klasický	k2eAgNnSc4d1
telefonování	telefonování	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Některé	některý	k3yIgInPc1
přístroje	přístroj	k1gInPc1
dokáží	dokázat	k5eAaPmIp3nP
před	před	k7c7
odesláním	odeslání	k1gNnSc7
z	z	k7c2
SMS	SMS	kA
diakritiku	diakritika	k1gFnSc4
odstranit	odstranit	k5eAaPmF
(	(	kIx(
<g/>
tzv.	tzv.	kA
"	"	kIx"
<g/>
oříznout	oříznout	k5eAaPmF
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
automaticky	automaticky	k6eAd1
nahradit	nahradit	k5eAaPmF
znaky	znak	k1gInPc4
s	s	k7c7
diakritikou	diakritika	k1gFnSc7
nediakritizovanými	diakritizovaný	k2eNgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
omezení	omezení	k1gNnSc3
délky	délka	k1gFnSc2
ze	z	k7c2
160	#num#	k4
na	na	k7c4
70	#num#	k4
znaků	znak	k1gInPc2
tedy	tedy	k9
nedojde	dojít	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
funkci	funkce	k1gFnSc4
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
potřeba	potřeba	k6eAd1
aktivovat	aktivovat	k5eAaBmF
v	v	k7c4
nastavení	nastavení	k1gNnSc4
odesílání	odesílání	k1gNnSc2
SMS	SMS	kA
<g/>
,	,	kIx,
jiná	jiná	k1gFnSc1
zařízení	zařízení	k1gNnSc2
ji	on	k3xPp3gFnSc4
naopak	naopak	k6eAd1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
jako	jako	k9
jedinou	jediný	k2eAgFnSc4d1
možnou	možný	k2eAgFnSc4d1
a	a	k8xC
SMS	SMS	kA
odešlou	odeslat	k5eAaPmIp3nP
vždy	vždy	k6eAd1
bez	bez	k7c2
diakritiky	diakritika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://mobil.idnes.cz/sms-zpravy-jsou-skutecnym-luxusem-i-za-korunu-jsou-predrazene-pvh-/mob_tech.asp?c=A090304_165203_mob_tech_hro	http://mobil.idnes.cz/sms-zpravy-jsou-skutecnym-luxusem-i-za-korunu-jsou-predrazene-pvh-/mob_tech.asp?c=A090304_165203_mob_tech_hra	k1gFnSc5
<g/>
↑	↑	k?
Obecná	obecný	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
poskytování	poskytování	k1gNnSc4
Prémiových	prémiový	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
(	(	kIx(
<g/>
Premium	Premium	k1gNnSc1
SMS	SMS	kA
<g/>
,	,	kIx,
Premium	Premium	k1gNnSc1
MMS	MMS	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracovní	pracovní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Premium	Premium	k1gNnSc1
Services	Services	k1gMnSc1
<g/>
,	,	kIx,
2008-08-01	2008-08-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
Češi	Čech	k1gMnPc1
poslali	poslat	k5eAaPmAgMnP
70	#num#	k4
milionů	milion	k4xCgInPc2
sms	sms	kA
<g/>
.	.	kIx.
iHned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2007-12-25	2007-12-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
7693	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VOLDŘICH	VOLDŘICH	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
-	-	kIx~
70	#num#	k4
milionů	milion	k4xCgInPc2
SMS	SMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dumfinanci	Dumfinance	k1gFnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-12-27	2007-12-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
5153	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Lidé	člověk	k1gMnPc1
letos	letos	k6eAd1
poslali	poslat	k5eAaPmAgMnP
na	na	k7c4
Vánoce	Vánoce	k1gFnPc4
70	#num#	k4
milionů	milion	k4xCgInPc2
SMS	SMS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-12-25	2007-12-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HRON	Hron	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vánoční	vánoční	k2eAgInSc1d1
přání	přání	k1gNnSc2
trhla	trhnout	k5eAaPmAgFnS
rekord	rekord	k1gInSc4
<g/>
,	,	kIx,
zákazníci	zákazník	k1gMnPc1
poslali	poslat	k5eAaPmAgMnP
70	#num#	k4
milionů	milion	k4xCgInPc2
textovek	textovka	k1gFnPc2
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-12-27	2007-12-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
jsme	být	k5eAaImIp1nP
poslali	poslat	k5eAaPmAgMnP
70	#num#	k4
milionů	milion	k4xCgInPc2
SMS	SMS	kA
a	a	k8xC
290	#num#	k4
tisíc	tisíc	k4xCgInSc4
MMS	MMS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FinExpert	FinExpert	k1gMnSc1
<g/>
,	,	kIx,
2007-12-27	2007-12-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
poslali	poslat	k5eAaPmAgMnP
Češi	Čech	k1gMnPc1
přes	přes	k7c4
70	#num#	k4
miliónů	milión	k4xCgInPc2
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-12-27	2007-12-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
POSPÍŠIL	Pospíšil	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operátoři	operátor	k1gMnPc1
na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
:	:	kIx,
padaly	padat	k5eAaImAgInP
rekordy	rekord	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MobilMania	MobilManium	k1gNnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-12-26	2007-12-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1214	#num#	k4
<g/>
-	-	kIx~
<g/>
1887	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠPULÁK	ŠPULÁK	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češi	Čech	k1gMnPc1
poslali	poslat	k5eAaPmAgMnP
na	na	k7c4
Štědrý	štědrý	k2eAgInSc4d1
den	den	k1gInSc4
přes	přes	k7c4
70	#num#	k4
miliónů	milión	k4xCgInPc2
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slunečnice	slunečnice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-12-25	2007-12-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
pad	padnout	k5eAaPmDgInS
<g/>
,	,	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Češi	Čech	k1gMnPc1
si	se	k3xPyFc3
poslali	poslat	k5eAaPmAgMnP
70	#num#	k4
milionů	milion	k4xCgInPc2
vánočních	vánoční	k2eAgFnPc2d1
SMS	SMS	kA
přání	přání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-12-25	2007-12-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
8991	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
O2	O2	k4
SMS	SMS	kA
brána	brát	k5eAaImNgFnS
</s>
<s>
SMS	SMS	kA
a	a	k8xC
MMS	MMS	kA
brána	brána	k1gFnSc1
T-mobile	T-mobil	k1gMnSc5
</s>
<s>
Hromadné	hromadný	k2eAgNnSc1d1
rozesílání	rozesílání	k1gNnSc1
SMS	SMS	kA
–	–	k?
Vodafone	Vodafon	k1gInSc5
<g/>
↑	↑	k?
O2	O2	k1gMnSc1
SMSender	SMSender	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
o	o	k7c4
<g/>
2	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Američané	Američan	k1gMnPc1
přestávají	přestávat	k5eAaImIp3nP
telefonovat	telefonovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propadli	propadnout	k5eAaPmAgMnP
textovkám	textovka	k1gFnPc3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
PR	pr	k0
SMS	SMS	kA
(	(	kIx(
<g/>
Premium	Premium	k1gNnSc1
Rate	Rate	k1gFnPc2
SMS	SMS	kA
<g/>
)	)	kIx)
–	–	k?
využití	využití	k1gNnSc1
v	v	k7c6
marketingu	marketing	k1gInSc6
</s>
<s>
T9	T9	k4
–	–	k?
prediktivní	prediktivní	k2eAgInSc4d1
algoritmus	algoritmus	k1gInSc4
pro	pro	k7c4
psaní	psaní	k1gNnSc4
</s>
<s>
Short	Short	k1gInSc1
date	dat	k1gFnSc2
service	service	k1gFnSc2
(	(	kIx(
<g/>
SDS	SDS	kA
<g/>
)	)	kIx)
</s>
<s>
Rich	Rich	k1gInSc1
Communication	Communication	k1gInSc1
Services	Services	k1gInSc1
(	(	kIx(
<g/>
RCS	RCS	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
SMS	SMS	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
SMS	SMS	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Jak	jak	k6eAd1
poslat	poslat	k5eAaPmF
SMS	SMS	kA
jménem	jméno	k1gNnSc7
jiného	jiný	k2eAgInSc2d1
mobilu	mobil	k1gInSc2
(	(	kIx(
<g/>
článek	článek	k1gInSc1
Vlastimila	Vlastimil	k1gMnSc2
Klímy	Klíma	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
SMS	SMS	kA
nejsou	být	k5eNaImIp3nP
bezpečné	bezpečný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falšovat	falšovat	k5eAaImF
je	on	k3xPp3gNnSc4
může	moct	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
článek	článek	k1gInSc1
na	na	k7c4
iDNES	iDNES	k?
<g/>
)	)	kIx)
</s>
<s>
Už	už	k6eAd1
vím	vědět	k5eAaImIp1nS
proč	proč	k6eAd1
má	mít	k5eAaImIp3nS
SMS	SMS	kA
jen	jen	k6eAd1
160	#num#	k4
znaků	znak	k1gInPc2
(	(	kIx(
<g/>
článek	článek	k1gInSc1
serveru	server	k1gInSc2
MobilMania	MobilManium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
13836	#num#	k4
</s>
