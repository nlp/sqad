<s>
Služba	služba	k1gFnSc1
krátkých	krátká	k2eAgFnPc2d1
textových	textový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
SMS	SMS	kA
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Short	Short	k2eAgFnPc2d1
message	message	k2eAgFnPc2d1
service	service	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
název	název	k1gInSc4
pro	pro	k7c4
službu	služba	k1gFnSc4
dostupnou	dostupný	k2eAgFnSc4d1
na	na	k7c6
většině	většina	k1gFnSc6
digitálních	digitální	k2eAgInPc2d1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
.	.	kIx.
</s>