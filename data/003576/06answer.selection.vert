<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
této	tento	k3xDgFnSc2	tento
pouště	poušť	k1gFnSc2	poušť
leží	ležet	k5eAaImIp3nS	ležet
Údolí	údolí	k1gNnSc1	údolí
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejníže	nízce	k6eAd3	nízce
ležící	ležící	k2eAgInSc4d1	ležící
bod	bod	k1gInSc4	bod
(	(	kIx(	(
<g/>
Badwater	Badwater	k1gMnSc1	Badwater
<g/>
,	,	kIx,	,
85	[number]	k4	85
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
hladiny	hladina	k1gFnSc2	hladina
oceánu	oceán	k1gInSc2	oceán
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejteplejší	teplý	k2eAgNnSc4d3	nejteplejší
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
