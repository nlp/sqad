<p>
<s>
Přezdívka	přezdívka	k1gFnSc1	přezdívka
je	být	k5eAaImIp3nS	být
neoficiální	oficiální	k2eNgNnSc4d1	neoficiální
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
či	či	k8xC	či
jeho	jeho	k3xOp3gFnSc1	jeho
podoba	podoba	k1gFnSc1	podoba
<g/>
)	)	kIx)	)
nějaké	nějaký	k3yIgFnPc4	nějaký
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
osoby	osoba	k1gFnPc4	osoba
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
organizace	organizace	k1gFnPc4	organizace
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
jméno	jméno	k1gNnSc4	jméno
domácké	domácký	k2eAgFnSc2d1	domácká
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
o	o	k7c4	o
nějakou	nějaký	k3yIgFnSc4	nějaký
formu	forma	k1gFnSc4	forma
zkomoleniny	zkomolenina	k1gFnSc2	zkomolenina
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
jména	jméno	k1gNnSc2	jméno
občanského	občanský	k2eAgNnSc2d1	občanské
(	(	kIx(	(
<g/>
hypokoristikon	hypokoristikon	k1gNnSc1	hypokoristikon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
oslovení	oslovení	k1gNnSc6	oslovení
pocházející	pocházející	k2eAgFnSc4d1	pocházející
z	z	k7c2	z
nějaké	nějaký	k3yIgFnSc2	nějaký
větší	veliký	k2eAgFnSc2d2	veliký
sociální	sociální	k2eAgFnSc2d1	sociální
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgFnSc2d1	pracovní
či	či	k8xC	či
společenské	společenský	k2eAgFnSc2d1	společenská
skupiny	skupina	k1gFnSc2	skupina
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dotyčného	dotyčný	k2eAgMnSc2d1	dotyčný
zná	znát	k5eAaImIp3nS	znát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
skautské	skautský	k2eAgFnSc6d1	skautská
organizaci	organizace	k1gFnSc6	organizace
<g/>
,	,	kIx,	,
sportovním	sportovní	k2eAgInSc6d1	sportovní
oddíle	oddíl	k1gInSc6	oddíl
<g/>
,	,	kIx,	,
divadelním	divadelní	k2eAgInSc6d1	divadelní
souboru	soubor	k1gInSc6	soubor
<g/>
,	,	kIx,	,
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívka	přezdívka	k1gFnSc1	přezdívka
často	často	k6eAd1	často
popisuje	popisovat	k5eAaImIp3nS	popisovat
nějaký	nějaký	k3yIgInSc1	nějaký
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
znak	znak	k1gInSc1	znak
příslušné	příslušný	k2eAgFnSc2d1	příslušná
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
zkomoleninu	zkomolenina	k1gFnSc4	zkomolenina
jména	jméno	k1gNnSc2	jméno
nebo	nebo	k8xC	nebo
vtipnou	vtipný	k2eAgFnSc4d1	vtipná
kombinaci	kombinace	k1gFnSc4	kombinace
části	část	k1gFnSc2	část
jejího	její	k3xOp3gNnSc2	její
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
povahového	povahový	k2eAgInSc2d1	povahový
rysu	rys	k1gInSc2	rys
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nelze	lze	k6eNd1	lze
zapomínat	zapomínat	k5eAaImF	zapomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přezdívka	přezdívka	k1gFnSc1	přezdívka
představuje	představovat	k5eAaImIp3nS	představovat
jistý	jistý	k2eAgInSc4d1	jistý
předstupeň	předstupeň	k1gInSc4	předstupeň
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	on	k3xPp3gMnPc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
u	u	k7c2	u
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
mají	mít	k5eAaImIp3nP	mít
dodnes	dodnes	k6eAd1	dodnes
charakter	charakter	k1gInSc4	charakter
přezdívky	přezdívka	k1gFnSc2	přezdívka
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
od	od	k7c2	od
severoamerických	severoamerický	k2eAgMnPc2d1	severoamerický
indiánů	indián	k1gMnPc2	indián
<g/>
,	,	kIx,	,
totéž	týž	k3xTgNnSc1	týž
však	však	k9	však
platí	platit	k5eAaImIp3nS	platit
také	také	k9	také
pro	pro	k7c4	pro
domorodé	domorodý	k2eAgMnPc4d1	domorodý
Afričany	Afričan	k1gMnPc4	Afričan
<g/>
,	,	kIx,	,
australské	australský	k2eAgMnPc4d1	australský
Aborigince	Aboriginec	k1gMnPc4	Aboriginec
<g/>
,	,	kIx,	,
obyvatele	obyvatel	k1gMnSc4	obyvatel
Oceánie	Oceánie	k1gFnSc2	Oceánie
nebo	nebo	k8xC	nebo
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
arabská	arabský	k2eAgFnSc1d1	arabská
<g/>
,	,	kIx,	,
indická	indický	k2eAgFnSc1d1	indická
<g/>
,	,	kIx,	,
čínská	čínský	k2eAgFnSc1d1	čínská
nebo	nebo	k8xC	nebo
japonská	japonský	k2eAgNnPc1d1	Japonské
jména	jméno	k1gNnPc1	jméno
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
podobu	podoba	k1gFnSc4	podoba
stručné	stručný	k2eAgFnSc2d1	stručná
charakteristiky	charakteristika	k1gFnSc2	charakteristika
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
omen	omen	k1gNnSc1	omen
nomen	nomen	k1gInSc1	nomen
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
zapomínat	zapomínat	k5eAaImF	zapomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohá	mnohý	k2eAgNnPc4d1	mnohé
současná	současný	k2eAgNnPc4d1	současné
jména	jméno	k1gNnPc4	jméno
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
přezdívka	přezdívka	k1gFnSc1	přezdívka
určité	určitý	k2eAgFnSc2d1	určitá
osoby	osoba	k1gFnSc2	osoba
-	-	kIx~	-
např.	např.	kA	např.
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
pouze	pouze	k6eAd1	pouze
přezdívka	přezdívka	k1gFnSc1	přezdívka
apoštola	apoštol	k1gMnSc2	apoštol
Šimona	Šimon	k1gMnSc2	Šimon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
neochvějnost	neochvějnost	k1gFnSc4	neochvějnost
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
valná	valný	k2eAgFnSc1d1	valná
většina	většina	k1gFnSc1	většina
příjmení	příjmení	k1gNnSc2	příjmení
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
přezdívky	přezdívka	k1gFnPc1	přezdívka
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
byli	být	k5eAaImAgMnP	být
odlišováni	odlišován	k2eAgMnPc1d1	odlišován
např.	např.	kA	např.
nositelé	nositel	k1gMnPc1	nositel
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Příjmení-přezdívky	Příjmenířezdívka	k1gFnPc1	Příjmení-přezdívka
vznikaly	vznikat	k5eAaImAgFnP	vznikat
jednak	jednak	k8xC	jednak
podle	podle	k7c2	podle
fyzického	fyzický	k2eAgInSc2d1	fyzický
vzhledu	vzhled	k1gInSc2	vzhled
podle	podle	k7c2	podle
vzhledu	vzhled	k1gInSc2	vzhled
-	-	kIx~	-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Malý	Malý	k1gMnSc1	Malý
<g/>
,	,	kIx,	,
Tučný	tučný	k2eAgMnSc1d1	tučný
<g/>
,	,	kIx,	,
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Ryšavý	Ryšavý	k1gMnSc1	Ryšavý
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
charakterových	charakterový	k2eAgFnPc2d1	charakterová
vlastností	vlastnost	k1gFnPc2	vlastnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Líný	líný	k2eAgMnSc1d1	líný
<g/>
,	,	kIx,	,
Kluge	Kluge	k1gFnSc1	Kluge
<g/>
,	,	kIx,	,
Liška	Liška	k1gMnSc1	Liška
<g/>
,	,	kIx,	,
Mazák	mazák	k1gMnSc1	mazák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
podle	podle	k7c2	podle
povolání	povolání	k1gNnSc2	povolání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kovář	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Řezníček	Řezníček	k1gMnSc1	Řezníček
<g/>
,	,	kIx,	,
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
,	,	kIx,	,
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žili	žít	k5eAaImAgMnP	žít
nebo	nebo	k8xC	nebo
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Doležal	Doležal	k1gMnSc1	Doležal
<g/>
,	,	kIx,	,
Zápotocký	zápotocký	k2eAgMnSc1d1	zápotocký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
podobnosti	podobnost	k1gFnSc2	podobnost
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc7	strom
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
uměleckého	umělecký	k2eAgInSc2d1	umělecký
pseudonymu	pseudonym	k1gInSc2	pseudonym
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
si	se	k3xPyFc3	se
každý	každý	k3xTgMnSc1	každý
tvůrce	tvůrce	k1gMnSc1	tvůrce
obvykle	obvykle	k6eAd1	obvykle
vymýšlí	vymýšlet	k5eAaImIp3nS	vymýšlet
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
svobodné	svobodný	k2eAgFnSc6d1	svobodná
vůli	vůle	k1gFnSc6	vůle
<g/>
,	,	kIx,	,
přezdívky	přezdívka	k1gFnPc1	přezdívka
obvykle	obvykle	k6eAd1	obvykle
vymýšlejí	vymýšlet	k5eAaImIp3nP	vymýšlet
a	a	k8xC	a
zavádějí	zavádět	k5eAaImIp3nP	zavádět
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
jsou	být	k5eAaImIp3nP	být
dotčené	dotčený	k2eAgFnSc3d1	dotčená
osobě	osoba	k1gFnSc3	osoba
dokonce	dokonce	k9	dokonce
i	i	k9	i
vnuceny	vnucen	k2eAgInPc4d1	vnucen
proti	proti	k7c3	proti
její	její	k3xOp3gFnSc3	její
vůli	vůle	k1gFnSc3	vůle
nebo	nebo	k8xC	nebo
používány	používat	k5eAaImNgFnP	používat
bez	bez	k7c2	bez
jejího	její	k3xOp3gNnSc2	její
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starších	starý	k2eAgInPc6d2	starší
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tato	tento	k3xDgFnSc1	tento
přezdívka	přezdívka	k1gFnSc1	přezdívka
příslotek	příslotek	k1gInSc1	příslotek
(	(	kIx(	(
<g/>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
přezdívky	přezdívka	k1gFnPc4	přezdívka
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
děti	dítě	k1gFnPc1	dítě
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
označují	označovat	k5eAaImIp3nP	označovat
své	svůj	k3xOyFgMnPc4	svůj
učitele	učitel	k1gMnPc4	učitel
nebo	nebo	k8xC	nebo
vězni	vězeň	k1gMnPc1	vězeň
své	svůj	k3xOyFgMnPc4	svůj
dozorce	dozorce	k1gMnPc4	dozorce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
kategorií	kategorie	k1gFnSc7	kategorie
přezdívek	přezdívka	k1gFnPc2	přezdívka
či	či	k8xC	či
pseudonymů	pseudonym	k1gInPc2	pseudonym
jsou	být	k5eAaImIp3nP	být
krycí	krycí	k2eAgNnPc1d1	krycí
jména	jméno	k1gNnPc1	jméno
používaná	používaný	k2eAgNnPc1d1	používané
při	při	k7c6	při
špionáži	špionáž	k1gFnSc6	špionáž
a	a	k8xC	a
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
tajných	tajný	k2eAgFnPc2d1	tajná
policií	policie	k1gFnPc2	policie
a	a	k8xC	a
tajných	tajný	k2eAgFnPc2d1	tajná
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
všech	všecek	k3xTgNnPc2	všecek
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
křestních	křestní	k2eAgNnPc2d1	křestní
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
příjmení	příjmení	k1gNnPc2	příjmení
osob	osoba	k1gFnPc2	osoba
bývají	bývat	k5eAaImIp3nP	bývat
povětšinou	povětšina	k1gFnSc7	povětšina
přezdívky	přezdívka	k1gFnSc2	přezdívka
(	(	kIx(	(
<g/>
jakož	jakož	k8xC	jakož
i	i	k9	i
pseudonymy	pseudonym	k1gInPc4	pseudonym
a	a	k8xC	a
krycí	krycí	k2eAgNnPc4d1	krycí
jména	jméno	k1gNnPc4	jméno
<g/>
)	)	kIx)	)
známy	znám	k2eAgFnPc1d1	známa
pouze	pouze	k6eAd1	pouze
omezenému	omezený	k2eAgInSc3d1	omezený
okruhu	okruh	k1gInSc3	okruh
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
přezdívky	přezdívka	k1gFnPc4	přezdívka
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
tzv.	tzv.	kA	tzv.
indiánská	indiánský	k2eAgNnPc1d1	indiánské
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
bojová	bojový	k2eAgNnPc1d1	bojové
jména	jméno	k1gNnPc1	jméno
<g/>
)	)	kIx)	)
užívaná	užívaný	k2eAgFnSc1d1	užívaná
v	v	k7c6	v
hnutí	hnutí	k1gNnSc6	hnutí
woodcraft	woodcraftum	k1gNnPc2	woodcraftum
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
spolcích	spolek	k1gInPc6	spolek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přezdívky	přezdívka	k1gFnSc2	přezdívka
používané	používaný	k2eAgFnSc2d1	používaná
v	v	k7c6	v
internetových	internetový	k2eAgFnPc6d1	internetová
diskusích	diskuse	k1gFnPc6	diskuse
<g/>
,	,	kIx,	,
chatech	chat	k1gInPc6	chat
a	a	k8xC	a
komunikačních	komunikační	k2eAgInPc6d1	komunikační
programech	program	k1gInPc6	program
(	(	kIx(	(
<g/>
ICQ	ICQ	kA	ICQ
<g/>
,	,	kIx,	,
Skype	Skyp	k1gInSc5	Skyp
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
nick	nick	k6eAd1	nick
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
nickname	nicknam	k1gInSc5	nicknam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
však	však	k9	však
zpravidla	zpravidla	k6eAd1	zpravidla
svůj	svůj	k3xOyFgInSc4	svůj
nick	nick	k6eAd1	nick
uživatelé	uživatel	k1gMnPc1	uživatel
Internetu	Internet	k1gInSc2	Internet
určuji	určovat	k5eAaImIp1nS	určovat
sami	sám	k3xTgMnPc1	sám
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
tzv.	tzv.	kA	tzv.
nicky	nicka	k1gFnPc4	nicka
obecnou	obecný	k2eAgFnSc4d1	obecná
podobu	podoba	k1gFnSc4	podoba
tzv.	tzv.	kA	tzv.
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
větším	veliký	k2eAgInSc6d2	veliký
informačním	informační	k2eAgInSc6d1	informační
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roli	role	k1gFnSc4	role
zčásti	zčásti	k6eAd1	zčásti
podobnou	podobný	k2eAgFnSc4d1	podobná
přezdívce	přezdívka	k1gFnSc6	přezdívka
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
řeholní	řeholní	k2eAgNnPc1d1	řeholní
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
lidem	lid	k1gInSc7	lid
vstupujícím	vstupující	k2eAgInSc7d1	vstupující
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
zpravidla	zpravidla	k6eAd1	zpravidla
vybírá	vybírat	k5eAaImIp3nS	vybírat
jejich	jejich	k3xOp3gNnSc1	jejich
představený	představený	k1gMnSc1	představený
a	a	k8xC	a
která	který	k3yRgNnPc4	který
mají	mít	k5eAaImIp3nP	mít
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
začátek	začátek	k1gInSc4	začátek
nové	nový	k2eAgFnSc2d1	nová
životní	životní	k2eAgFnSc2d1	životní
etapy	etapa	k1gFnSc2	etapa
a	a	k8xC	a
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
křestní	křestní	k2eAgNnPc4d1	křestní
a	a	k8xC	a
biřmovací	biřmovací	k2eAgNnPc4d1	biřmovací
jména	jméno	k1gNnPc4	jméno
<g/>
)	)	kIx)	)
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
patronát	patronát	k1gInSc4	patronát
určitého	určitý	k2eAgMnSc2d1	určitý
světce	světec	k1gMnSc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
praxe	praxe	k1gFnSc1	praxe
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
dalších	další	k2eAgFnPc6d1	další
náboženských	náboženský	k2eAgFnPc6d1	náboženská
<g/>
,	,	kIx,	,
zájmových	zájmový	k2eAgFnPc6d1	zájmová
i	i	k8xC	i
jiných	jiný	k2eAgFnPc6d1	jiná
komunitách	komunita	k1gFnPc6	komunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
přezdívek	přezdívka	k1gFnPc2	přezdívka
==	==	k?	==
</s>
</p>
<p>
<s>
Stupor	stupor	k1gInSc4	stupor
mundi	mund	k1gMnPc1	mund
-	-	kIx~	-
římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Sicilský	sicilský	k2eAgInSc1d1	sicilský
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
rytíř	rytíř	k1gMnSc1	rytíř
-	-	kIx~	-
římskoněmecký	římskoněmecký	k2eAgMnSc1d1	římskoněmecký
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc5d1	habsburský
</s>
</p>
<p>
<s>
Sissi	Sisse	k1gFnSc4	Sisse
–	–	k?	–
rakouská	rakouský	k2eAgFnSc1d1	rakouská
císařovna	císařovna	k1gFnSc1	císařovna
a	a	k8xC	a
předposlední	předposlední	k2eAgFnSc1d1	předposlední
česká	český	k2eAgFnSc1d1	Česká
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
</s>
</p>
<p>
<s>
Starý	Starý	k1gMnSc1	Starý
či	či	k8xC	či
Starej	starat	k5eAaImRp2nS	starat
Procházka	Procházka	k1gMnSc1	Procházka
–	–	k?	–
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Protentokrát	protentokrát	k6eAd1	protentokrát
–	–	k?	–
dobová	dobový	k2eAgFnSc1d1	dobová
přezdívka	přezdívka	k1gFnSc1	přezdívka
pro	pro	k7c4	pro
Protektorát	protektorát	k1gInSc4	protektorát
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
</s>
</p>
<p>
<s>
Fronta	fronta	k1gFnSc1	fronta
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
–	–	k?	–
hanlivá	hanlivý	k2eAgFnSc1d1	hanlivá
přezdívka	přezdívka	k1gFnSc1	přezdívka
Stalinova	Stalinův	k2eAgInSc2d1	Stalinův
pomníku	pomník	k1gInSc2	pomník
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
</s>
</p>
<p>
<s>
Jestřáb	jestřáb	k1gMnSc1	jestřáb
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
</s>
</p>
<p>
<s>
Lanďák	Lanďák	k1gMnSc1	Lanďák
–	–	k?	–
Pavel	Pavel	k1gMnSc1	Pavel
Landovský	Landovský	k2eAgMnSc1d1	Landovský
(	(	kIx(	(
<g/>
pěkný	pěkný	k2eAgInSc1d1	pěkný
příklad	příklad	k1gInSc1	příklad
na	na	k7c4	na
hypokoristikon	hypokoristikon	k1gNnSc4	hypokoristikon
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
přezdívka	přezdívka	k1gFnSc1	přezdívka
odvozená	odvozený	k2eAgFnSc1d1	odvozená
z	z	k7c2	z
příjmení	příjmení	k1gNnSc2	příjmení
osoby	osoba	k1gFnSc2	osoba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dominátor	Dominátor	k1gMnSc1	Dominátor
–	–	k?	–
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
přezdívka	přezdívka	k1gFnSc1	přezdívka
českého	český	k2eAgMnSc2d1	český
hokejového	hokejový	k2eAgMnSc2d1	hokejový
brankáře	brankář	k1gMnSc2	brankář
<g/>
,	,	kIx,	,
hypokoristikonPřezdívky	hypokoristikonPřezdívka	k1gFnSc2	hypokoristikonPřezdívka
se	se	k3xPyFc4	se
nemusejí	muset	k5eNaImIp3nP	muset
týkat	týkat	k5eAaImF	týkat
jen	jen	k9	jen
jména	jméno	k1gNnPc4	jméno
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgNnPc4d1	podobné
označení	označení	k1gNnPc4	označení
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
pro	pro	k7c4	pro
skupiny	skupina	k1gFnPc4	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
geografické	geografický	k2eAgInPc4d1	geografický
útvary	útvar	k1gInPc4	útvar
apod.	apod.	kA	apod.
-	-	kIx~	-
Perníkáři	perníkář	k1gMnSc3	perníkář
–	–	k?	–
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
,	,	kIx,	,
Taliáni	talián	k1gMnPc1	talián
-	-	kIx~	-
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
Země	zem	k1gFnPc1	zem
vycházejícího	vycházející	k2eAgNnSc2d1	vycházející
slunce	slunce	k1gNnSc2	slunce
-	-	kIx~	-
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Večernice	večernice	k1gFnSc1	večernice
-	-	kIx~	-
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
pseudonym	pseudonym	k1gInSc1	pseudonym
</s>
</p>
<p>
<s>
hypokoristikon	hypokoristikon	k1gNnSc1	hypokoristikon
</s>
</p>
<p>
<s>
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
</s>
</p>
<p>
<s>
příjmení	příjmení	k1gNnSc1	příjmení
</s>
</p>
<p>
<s>
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
jméno	jméno	k1gNnSc4	jméno
</s>
</p>
<p>
<s>
řeholní	řeholní	k2eAgNnSc1d1	řeholní
jméno	jméno	k1gNnSc1	jméno
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
přezdívka	přezdívka	k1gFnSc1	přezdívka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
