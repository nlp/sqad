<p>
<s>
Félix	Félix	k1gInSc1	Félix
Sánchez	Sánchez	k1gInSc1	Sánchez
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
též	též	k9	též
Super	super	k6eAd1	super
Felix	Felix	k1gMnSc1	Felix
či	či	k8xC	či
Dictator	Dictator	k1gMnSc1	Dictator
je	být	k5eAaImIp3nS	být
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
z	z	k7c2	z
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
věnující	věnující	k2eAgFnSc2d1	věnující
se	se	k3xPyFc4	se
běhu	běh	k1gInSc3	běh
na	na	k7c4	na
400	[number]	k4	400
m	m	kA	m
přes	přes	k7c4	přes
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dvojnásobného	dvojnásobný	k2eAgMnSc4d1	dvojnásobný
olympijského	olympijský	k2eAgMnSc4d1	olympijský
vítěze	vítěz	k1gMnSc4	vítěz
a	a	k8xC	a
dvojnásobného	dvojnásobný	k2eAgMnSc4d1	dvojnásobný
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světovým	světový	k2eAgFnPc3d1	světová
tabulkám	tabulka	k1gFnPc3	tabulka
dominoval	dominovat	k5eAaImAgInS	dominovat
především	především	k9	především
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
minulého	minulý	k2eAgNnSc2d1	Minulé
desetiletí	desetiletí	k1gNnSc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
a	a	k8xC	a
2004	[number]	k4	2004
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
43	[number]	k4	43
závodů	závod	k1gInPc2	závod
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
sérii	série	k1gFnSc6	série
korunoval	korunovat	k5eAaBmAgInS	korunovat
ziskem	zisk	k1gInSc7	zisk
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
také	také	k9	také
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Edmontonu	Edmonton	k1gInSc6	Edmonton
2001	[number]	k4	2001
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
2007	[number]	k4	2007
skončil	skončit	k5eAaPmAgMnS	skončit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
časem	časem	k6eAd1	časem
48,01	[number]	k4	48,01
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc4	jeho
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výkon	výkon	k1gInSc4	výkon
sezóny	sezóna	k1gFnSc2	sezóna
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
čtyřiatřiceti	čtyřiatřicet	k4xCc6	čtyřiatřicet
letech	let	k1gInPc6	let
slavil	slavit	k5eAaImAgInS	slavit
nečekaný	čekaný	k2eNgInSc4d1	nečekaný
comeback	comeback	k1gInSc4	comeback
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
finále	finále	k1gNnSc6	finále
stejným	stejný	k2eAgInSc7d1	stejný
časem	čas	k1gInSc7	čas
47,63	[number]	k4	47,63
jako	jako	k8xC	jako
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnSc7	svůj
nejrychlejším	rychlý	k2eAgMnSc7d3	nejrychlejší
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
ocenění	ocenění	k1gNnSc4	ocenění
Laureus	Laureus	k1gInSc4	Laureus
pro	pro	k7c4	pro
návrat	návrat	k1gInSc4	návrat
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
ukončil	ukončit	k5eAaPmAgInS	ukončit
sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInPc4d1	osobní
rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
400	[number]	k4	400
m	m	kA	m
přek	překa	k1gFnPc2	překa
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
–	–	k?	–
47,25	[number]	k4	47,25
s	s	k7c7	s
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
</s>
</p>
<p>
<s>
400	[number]	k4	400
m	m	kA	m
(	(	kIx(	(
<g/>
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
–	–	k?	–
44,90	[number]	k4	44,90
s	s	k7c7	s
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Gateshead	Gateshead	k1gInSc1	Gateshead
</s>
</p>
<p>
<s>
400	[number]	k4	400
m	m	kA	m
(	(	kIx(	(
<g/>
hala	hala	k1gFnSc1	hala
<g/>
)	)	kIx)	)
–	–	k?	–
46,68	[number]	k4	46,68
s	s	k7c7	s
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Glasgow	Glasgow	k1gInSc1	Glasgow
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Félix	Félix	k1gInSc1	Félix
Sánchez	Sánchez	k1gInSc4	Sánchez
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Félix	Félix	k1gInSc1	Félix
Sánchez	Sánchez	k1gInSc1	Sánchez
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
sports-reference	sportseferenka	k1gFnSc6	sports-referenka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
