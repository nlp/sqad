<s>
Seznam	seznam	k1gInSc1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Brna	Brno	k1gNnSc2
</s>
<s>
Statutární	statutární	k2eAgNnSc1d1
město	město	k1gNnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Brno	Brno	k1gNnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
29	#num#	k4
samosprávných	samosprávný	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
s	s	k7c7
vlastním	vlastní	k2eAgNnSc7d1
zastupitelstvem	zastupitelstvo	k1gNnSc7
<g/>
,	,	kIx,
starostou	starosta	k1gMnSc7
<g/>
,	,	kIx,
radou	rada	k1gMnSc7
a	a	k8xC
také	také	k9
vlastním	vlastní	k2eAgInSc7d1
znakem	znak	k1gInSc7
a	a	k8xC
vlajkou	vlajka	k1gFnSc7
<g/>
,	,	kIx,
město	město	k1gNnSc1
se	se	k3xPyFc4
celkově	celkově	k6eAd1
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
48	#num#	k4
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
přehled	přehled	k1gInSc4
obsahuje	obsahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
základní	základní	k2eAgInPc4d1
souhrnné	souhrnný	k2eAgInPc4d1
údaje	údaj	k1gInPc4
o	o	k7c6
každé	každý	k3xTgFnSc6
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
,	,	kIx,
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
řazeny	řadit	k5eAaImNgFnP
sestupně	sestupně	k6eAd1
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
k	k	k7c3
26	#num#	k4
<g/>
.	.	kIx.
březnu	březen	k1gInSc6
2011	#num#	k4
podle	podle	k7c2
Sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
domů	dům	k1gInPc2
a	a	k8xC
bytů	byt	k1gInPc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mapa	mapa	k1gFnSc1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
</s>
<s>
Brno-Bohunice	Brno-Bohunice	k1gFnSc1
</s>
<s>
Brno-Bosonohy	Brno-Bosonoh	k1gInPc1
</s>
<s>
Brno-Bystrc	Brno-Bystrc	k6eAd1
</s>
<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
</s>
<s>
Brno-Chrlice	Brno-Chrlice	k1gFnSc1
</s>
<s>
Brno-Ivanovice	Brno-Ivanovice	k1gFnSc1
</s>
<s>
Brno-Jehnice	Brno-Jehnice	k1gFnSc1
</s>
<s>
Brno-jih	Brno-jih	k1gMnSc1
</s>
<s>
Brno-Jundrov	Brno-Jundrov	k1gInSc1
</s>
<s>
Brno-Kníničky	Brno-Knínička	k1gFnPc1
</s>
<s>
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc1
</s>
<s>
Brno-Komín	Brno-Komín	k1gMnSc1
</s>
<s>
Brno-Královo	Brno-Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
Brno-Líšeň	Brno-Líšeň	k1gFnSc1
</s>
<s>
Brno-Maloměřice	Brno-Maloměřice	k1gFnPc1
a	a	k8xC
Obřany	Obřana	k1gFnPc1
</s>
<s>
Brno-Medlánky	Brno-Medlánka	k1gFnPc1
</s>
<s>
Brno-Nový	Brno-Nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Brno-Ořešín	Brno-Ořešín	k1gMnSc1
</s>
<s>
Brno-Řečkovice	Brno-Řečkovice	k1gFnSc1
a	a	k8xC
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Brno-sever	Brno-sever	k1gMnSc1
</s>
<s>
Brno-Slatina	Brno-Slatina	k1gFnSc1
</s>
<s>
Brno-Starý	Brno-Starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Brno-střed	Brno-střed	k1gMnSc1
</s>
<s>
Brno-Tuřany	Brno-Tuřan	k1gMnPc4
</s>
<s>
Brno-Útěchov	Brno-Útěchov	k1gInSc1
</s>
<s>
Brno-Vinohrady	Brno-Vinohrada	k1gFnPc1
</s>
<s>
Brno-Žabovřesky	Brno-Žabovřesky	k6eAd1
</s>
<s>
Brno-Žebětín	Brno-Žebětín	k1gMnSc1
</s>
<s>
Brno-Židenice	Brno-Židenice	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
</s>
<s>
Nad	nad	k7c4
20	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Brno-střed	Brno-střed	k1gMnSc1
</s>
<s>
ČástiBrno-město	ČástiBrno-město	k6eAd1
<g/>
,	,	kIx,
Staré	Staré	k2eAgNnSc1d1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Štýřice	Štýřice	k1gFnSc1
<g/>
,	,	kIx,
Veveří	veveří	k2eAgFnSc1d1
<g/>
,	,	kIx,
Stránice	Stránice	k1gFnSc1
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
<g/>
:	:	kIx,
Černá	černý	k2eAgFnSc1d1
Pole	pole	k1gFnSc1
<g/>
,	,	kIx,
Pisárky	Pisárek	k1gInPc1
<g/>
,	,	kIx,
Trnitá	trnitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Zábrdovice	Zábrdovice	k1gFnPc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
64	#num#	k4
316	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
15	#num#	k4
km²	km²	k?
</s>
<s>
Brno-střed	Brno-střed	k1gInSc1
je	být	k5eAaImIp3nS
nejvýznamnější	významný	k2eAgFnSc7d3
městskou	městský	k2eAgFnSc7d1
částí	část	k1gFnSc7
Brna	Brno	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc7
administrativním	administrativní	k2eAgNnSc7d1
<g/>
,	,	kIx,
hospodářským	hospodářský	k2eAgNnSc7d1
a	a	k8xC
kulturním	kulturní	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
tradičně	tradičně	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
řada	řada	k1gFnSc1
úřadů	úřad	k1gInPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
řada	řada	k1gFnSc1
základních	základní	k2eAgFnPc2d1
<g/>
,	,	kIx,
středních	střední	k2eAgFnPc2d1
i	i	k8xC
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
mnoho	mnoho	k4c4
firem	firma	k1gFnPc2
i	i	k8xC
obchodů	obchod	k1gInPc2
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
také	také	k9
důležitý	důležitý	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
uzel	uzel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
zhruba	zhruba	k6eAd1
uprostřed	uprostřed	k7c2
města	město	k1gNnSc2
po	po	k7c6
obou	dva	k4xCgInPc6
březích	břeh	k1gInPc6
řeky	řeka	k1gFnSc2
Svratky	Svratka	k1gFnSc2
západně	západně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Svitavy	Svitava	k1gFnSc2
<g/>
,	,	kIx,
svým	svůj	k3xOyFgNnSc7
územím	území	k1gNnSc7
se	se	k3xPyFc4
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
kryje	krýt	k5eAaImIp3nS
s	s	k7c7
územím	území	k1gNnSc7
historického	historický	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nesourodým	sourodý	k2eNgInSc7d1
celkem	celek	k1gInSc7
řady	řada	k1gFnSc2
čtvrtí	čtvrtit	k5eAaImIp3nS
s	s	k7c7
hustou	hustý	k2eAgFnSc7d1
<g/>
,	,	kIx,
ale	ale	k8xC
různě	různě	k6eAd1
starou	starý	k2eAgFnSc7d1
zástavbou	zástavba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
nejstarší	starý	k2eAgFnSc2d3
brněnské	brněnský	k2eAgFnSc2d1
zástavby	zástavba	k1gFnSc2
v	v	k7c6
historickém	historický	k2eAgNnSc6d1
jádru	jádro	k1gNnSc6
Brna	Brno	k1gNnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
také	také	k9
moderní	moderní	k2eAgFnPc4d1
výškové	výškový	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
i	i	k8xC
panelová	panelový	k2eAgNnPc4d1
sídliště	sídliště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominanty	dominanta	k1gFnPc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
představují	představovat	k5eAaImIp3nP
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
na	na	k7c6
kopci	kopec	k1gInSc6
Petrov	Petrov	k1gInSc1
a	a	k8xC
hrad	hrad	k1gInSc1
Špilberk	Špilberk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
také	také	k9
nejvýznamnější	významný	k2eAgInSc1d3
brněnský	brněnský	k2eAgInSc1d1
park	park	k1gInSc1
Lužánky	Lužánka	k1gFnSc2
i	i	k8xC
ústřední	ústřední	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Brno-sever	Brno-sever	k1gMnSc1
</s>
<s>
ČástiHusovice	ČástiHusovice	k1gFnSc1
<g/>
,	,	kIx,
Lesná	lesný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Soběšice	Soběšice	k1gFnSc1
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
<g/>
:	:	kIx,
Černá	černý	k2eAgFnSc1d1
Pole	pole	k1gFnSc1
(	(	kIx(
<g/>
většina	většina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Zábrdovice	Zábrdovice	k1gFnPc4
(	(	kIx(
<g/>
asi	asi	k9
třetina	třetina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
47	#num#	k4
643	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
12,24	12,24	k4
km²	km²	k?
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
okraji	okraj	k1gInSc6
brněnské	brněnský	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
její	její	k3xOp3gFnSc2
níže	nízce	k6eAd2
položené	položený	k2eAgFnSc2d1
části	část	k1gFnSc2
území	území	k1gNnSc2
stoupá	stoupat	k5eAaImIp3nS
přes	přes	k7c4
terasu	terasa	k1gFnSc4
Lesné	lesný	k2eAgFnSc2d1
k	k	k7c3
zalesněným	zalesněný	k2eAgFnPc3d1
náhorním	náhorní	k2eAgFnPc3d1
planinám	planina	k1gFnPc3
Soběšic	Soběšice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
Soběšic	Soběšice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
mají	mít	k5eAaImIp3nP
charakter	charakter	k1gInSc4
velké	velký	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
zbytek	zbytek	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
výrazně	výrazně	k6eAd1
městský	městský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
má	mít	k5eAaImIp3nS
velice	velice	k6eAd1
nesourodou	sourodý	k2eNgFnSc4d1
strukturu	struktura	k1gFnSc4
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
svého	svůj	k3xOyFgNnSc2
vymezení	vymezení	k1gNnSc2
i	i	k9
dost	dost	k6eAd1
atypický	atypický	k2eAgInSc4d1
tvar	tvar	k1gInSc4
připomínající	připomínající	k2eAgFnSc2d1
přesýpací	přesýpací	k2eAgFnSc2d1
hodiny	hodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
například	například	k6eAd1
budovy	budova	k1gFnPc4
Mendelovy	Mendelův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
Muzeum	muzeum	k1gNnSc1
romské	romský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
dětská	dětský	k2eAgFnSc1d1
nemocnice	nemocnice	k1gFnSc1
<g/>
,	,	kIx,
Taneční	taneční	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
MIMI	Mimi	k1gFnSc2
FORTUNAE	FORTUNAE	kA
<g/>
,	,	kIx,
vila	vila	k1gFnSc1
Tugendhat	Tugendhat	k1gFnSc1
nebo	nebo	k8xC
soběšický	soběšický	k2eAgInSc1d1
klášter	klášter	k1gInSc1
klarisek	klariska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
i	i	k9
park	park	k1gInSc1
Schreberovy	Schreberův	k2eAgFnSc2d1
zahrádky	zahrádka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
jediným	jediný	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
na	na	k7c6
území	území	k1gNnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
je	být	k5eAaImIp3nS
husovický	husovický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Nejsvětějšího	nejsvětější	k2eAgNnSc2d1
srdce	srdce	k1gNnSc2
Páně	páně	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Brno-Královo	Brno-Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
ČástiKrálovo	ČástiKrálův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
<g/>
,	,	kIx,
Ponava	Ponava	k1gFnSc1
<g/>
,	,	kIx,
Sadová	sadový	k2eAgFnSc1d1
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
<g/>
:	:	kIx,
Černá	černý	k2eAgFnSc1d1
Pole	pole	k1gFnSc1
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
28	#num#	k4
674	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
9,91	9,91	k4
km²	km²	k?
</s>
<s>
Brno-Královo	Brno-Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
(	(	kIx(
<g/>
v	v	k7c6
hantecu	hantecus	k1gInSc6
Krpole	Krpole	k1gFnSc2
nebo	nebo	k8xC
Kénig	Kénig	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
severně	severně	k6eAd1
od	od	k7c2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-střed	Brno-střed	k1gMnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
městský	městský	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Sadová	sadový	k2eAgFnSc1d1
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgInPc1d1
lesy	les	k1gInPc1
a	a	k8xC
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
významný	významný	k2eAgInSc1d1
krajinný	krajinný	k2eAgInSc1d1
prvek	prvek	k1gInSc1
Údolí	údolí	k1gNnSc2
Zaječího	zaječí	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
severozápad	severozápad	k1gInSc4
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
zasahuje	zasahovat	k5eAaImIp3nS
významný	významný	k2eAgInSc4d1
krajinný	krajinný	k2eAgInSc4d1
prvek	prvek	k1gInSc4
Medlánecké	Medlánecký	k2eAgInPc4d1
kopce	kopec	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
brněnské	brněnský	k2eAgNnSc1d1
nákupní	nákupní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Královo	Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
s	s	k7c7
non-stop	non-stop	k1gInSc1
hypermarketem	hypermarket	k1gInSc7
Tesco	Tesco	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Brno-Líšeň	Brno-Líšeň	k1gFnSc1
</s>
<s>
ČástiLíšeň	ČástiLíšeň	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
26	#num#	k4
781	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
15,71	15,71	k4
km²	km²	k?
</s>
<s>
Líšeň	Líšeň	k1gFnSc1
je	být	k5eAaImIp3nS
historický	historický	k2eAgInSc4d1
městys	městys	k1gInSc4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
na	na	k7c6
východě	východ	k1gInSc6
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Líšeň	Líšeň	k1gFnSc4
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
starou	stará	k1gFnSc4
(	(	kIx(
<g/>
původní	původní	k2eAgFnSc1d1
zástavba	zástavba	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
novou	nový	k2eAgFnSc4d1
Líšeň	Líšeň	k1gFnSc4
(	(	kIx(
<g/>
sídliště	sídliště	k1gNnSc4
vybudované	vybudovaný	k2eAgFnSc2d1
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
nové	nový	k2eAgFnSc2d1
Líšně	Líšeň	k1gFnSc2
má	mít	k5eAaImIp3nS
stará	starý	k2eAgFnSc1d1
Líšeň	Líšeň	k1gFnSc1
dodnes	dodnes	k6eAd1
charakter	charakter	k1gInSc4
spíše	spíše	k9
velké	velký	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
či	či	k8xC
městečka	městečko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
katastru	katastr	k1gInSc2
Líšně	Líšeň	k1gFnSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
rozsáhlé	rozsáhlý	k2eAgInPc4d1
lesy	les	k1gInPc4
a	a	k8xC
zasahuje	zasahovat	k5eAaImIp3nS
sem	sem	k6eAd1
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
CHKO	CHKO	kA
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východní	východní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
katastru	katastr	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Mariánské	mariánský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
využíváno	využívat	k5eAaPmNgNnS,k5eAaImNgNnS
ke	k	k7c3
kulturním	kulturní	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
,	,	kIx,
a	a	k8xC
Brňany	Brňan	k1gMnPc4
využíváno	využívat	k5eAaImNgNnS,k5eAaPmNgNnS
k	k	k7c3
rekreaci	rekreace	k1gFnSc3
a	a	k8xC
relaxaci	relaxace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celá	k1gFnSc2
údolí	údolí	k1gNnSc2
je	být	k5eAaImIp3nS
projízdné	projízdný	k2eAgNnSc1d1
jak	jak	k8xS,k8xC
na	na	k7c6
kole	kolo	k1gNnSc6
tak	tak	k6eAd1
autem	auto	k1gNnSc7
s	s	k7c7
povolením	povolení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Brno-Bystrc	Brno-Bystrc	k6eAd1
</s>
<s>
ČástiBystrc	ČástiBystrc	k6eAd1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
24	#num#	k4
218	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
27,24	27,24	k4
km²	km²	k?
</s>
<s>
Bystrc	Bystrc	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
hantecu	hantecus	k1gInSc6
zvaná	zvaný	k2eAgFnSc1d1
Bástr	Bástr	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
městská	městský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
a	a	k8xC
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
severozápadě	severozápad	k1gInSc6
Brna	Brno	k1gNnSc2
a	a	k8xC
svojí	svůj	k3xOyFgFnSc7
rozlohou	rozloha	k1gFnSc7
je	být	k5eAaImIp3nS
vůbec	vůbec	k9
největší	veliký	k2eAgFnSc4d3
městskou	městský	k2eAgFnSc4d1
část	část	k1gFnSc4
i	i	k8xC
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
této	tento	k3xDgFnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
řada	řada	k1gFnSc1
brněnských	brněnský	k2eAgInPc2d1
rekreačních	rekreační	k2eAgInPc2d1
objektů	objekt	k1gInPc2
–	–	k?
Brněnská	brněnský	k2eAgFnSc1d1
přehrada	přehrada	k1gFnSc1
<g/>
,	,	kIx,
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Údolí	údolí	k1gNnSc1
oddechu	oddech	k1gInSc2
<g/>
,	,	kIx,
rozsáhlé	rozsáhlý	k2eAgInPc1d1
Podkomorské	podkomorský	k2eAgInPc1d1
lesy	les	k1gInPc1
s	s	k7c7
areálem	areál	k1gInSc7
Pohádky	pohádka	k1gFnSc2
máje	máj	k1gInSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Helenčiny	Helenčin	k2eAgFnSc2d1
a	a	k8xC
Ríšovy	Ríšův	k2eAgFnSc2d1
studánky	studánka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hrad	hrad	k1gInSc4
Veveří	veveří	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Brno-Židenice	Brno-Židenice	k1gFnSc1
</s>
<s>
Částizčásti	Částizčást	k1gFnPc1
<g/>
:	:	kIx,
Židenice	Židenice	k1gFnPc1
(	(	kIx(
<g/>
jihozápadní	jihozápadní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Zábrdovice	Zábrdovice	k1gFnPc4
(	(	kIx(
<g/>
východní	východní	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
22	#num#	k4
000	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
3,03	3,03	k4
km²	km²	k?
</s>
<s>
Zástavba	zástavba	k1gFnSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
levém	levý	k2eAgInSc6d1
(	(	kIx(
<g/>
východním	východní	k2eAgInSc6d1
<g/>
)	)	kIx)
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Svitavy	Svitava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
Židenic	Židenice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
má	mít	k5eAaImIp3nS
městský	městský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
srpkovitě	srpkovitě	k6eAd1
ohraničuje	ohraničovat	k5eAaImIp3nS
sousední	sousední	k2eAgFnSc4d1
městskou	městský	k2eAgFnSc4d1
část	část	k1gFnSc4
Brno-Vinohrady	Brno-Vinohrada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západní	západní	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
téměř	téměř	k6eAd1
úplná	úplný	k2eAgFnSc1d1
rovina	rovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východně	východně	k6eAd1
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
zvedat	zvedat	k5eAaImF
Bílá	bílý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
severně	severně	k6eAd1
od	od	k7c2
ní	on	k3xPp3gFnSc2
nedaleko	nedaleko	k7c2
budovy	budova	k1gFnSc2
místní	místní	k2eAgFnSc2d1
polikliniky	poliklinika	k1gFnSc2
se	se	k3xPyFc4
zase	zase	k9
zvedá	zvedat	k5eAaImIp3nS
výše	vysoce	k6eAd2
Židenický	Židenický	k2eAgInSc1d1
kopec	kopec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severozápadním	severozápadní	k2eAgInSc6d1
svahu	svah	k1gInSc6
Židenického	Židenický	k2eAgInSc2d1
kopce	kopec	k1gInSc2
při	při	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
Vinohrady	Vinohrady	k1gInPc7
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
i	i	k9
zdejší	zdejší	k2eAgInSc1d1
lesopark	lesopark	k1gInSc1
Akátky	akátek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
lesopark	lesopark	k1gInSc1
<g/>
,	,	kIx,
rozkládající	rozkládající	k2eAgFnSc2d1
se	se	k3xPyFc4
Juliánovem	Juliánovo	k1gNnSc7
na	na	k7c6
Bílé	bílý	k2eAgFnSc6d1
Hoře	hora	k1gFnSc6
<g/>
,	,	kIx,
nabízí	nabízet	k5eAaImIp3nS
vyhlídku	vyhlídka	k1gFnSc4
na	na	k7c4
Brno	Brno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc4
lesoparky	lesopark	k1gInPc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
zdejší	zdejší	k2eAgMnPc1d1
občané	občan	k1gMnPc1
k	k	k7c3
procházkám	procházka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
samém	samý	k3xTgInSc6
jihu	jih	k1gInSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
jižně	jižně	k6eAd1
od	od	k7c2
původního	původní	k2eAgInSc2d1
Juliánova	Juliánův	k2eAgInSc2d1
v	v	k7c6
těsné	těsný	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
tramvajové	tramvajový	k2eAgFnSc2d1
trati	trať	k1gFnSc2
linky	linka	k1gFnPc1
číslo	číslo	k1gNnSc1
8	#num#	k4
rozkládá	rozkládat	k5eAaImIp3nS
areál	areál	k1gInSc4
zdejšího	zdejší	k2eAgInSc2d1
židovského	židovský	k2eAgInSc2d1
hřbitova	hřbitov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
probíhá	probíhat	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
intenzivní	intenzivní	k2eAgFnSc2d1
rekonstrukce	rekonstrukce	k1gFnSc2
bytových	bytový	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Žabovřesky	Brno-Žabovřesky	k6eAd1
</s>
<s>
ČástiŽabovřesky	ČástiŽabovřesky	k6eAd1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
21	#num#	k4
047	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
4,35	4,35	k4
km²	km²	k?
</s>
<s>
Žabovřesky	Žabovřesky	k1gFnPc1
(	(	kIx(
<g/>
v	v	k7c6
hantecu	hantecus	k1gInSc6
Žabiny	Žabiny	k?
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
bývalá	bývalý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
městská	městský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
<g/>
,	,	kIx,
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žabovřesky	Žabovřesky	k1gFnPc1
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Svratky	Svratka	k1gFnSc2
severozápadně	severozápadně	k6eAd1
od	od	k7c2
historického	historický	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
mají	mít	k5eAaImIp3nP
relativně	relativně	k6eAd1
blízko	blízko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žabovřesky	Žabovřesky	k1gFnPc1
mají	mít	k5eAaImIp3nP
městský	městský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
lesoparky	lesopark	k1gInPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
Wilsonův	Wilsonův	k2eAgInSc1d1
les	les	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
nejjižnější	jižní	k2eAgFnSc4d3
část	část	k1gFnSc4
Žabovřesk	Žabovřesky	k1gFnPc2
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
lesopark	lesopark	k1gInSc1
na	na	k7c6
Palackého	Palackého	k2eAgInSc6d1
vrchu	vrch	k1gInSc6
tvoří	tvořit	k5eAaImIp3nS
nejsevernější	severní	k2eAgFnSc1d3
část	část	k1gFnSc1
Žabovřesk	Žabovřesky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
až	až	k6eAd1
20	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Brno-Řečkovice	Brno-Řečkovice	k1gFnSc1
a	a	k8xC
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
ČástiŘečkovice	ČástiŘečkovice	k1gFnSc1
<g/>
,	,	kIx,
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
15	#num#	k4
486	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
7,57	7,57	k4
km²	km²	k?
</s>
<s>
Rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
severu	sever	k1gInSc6
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
kolísá	kolísat	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
226	#num#	k4
<g/>
–	–	k?
<g/>
398	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Jižněji	jižně	k6eAd2
položená	položený	k2eAgFnSc1d1
podstatně	podstatně	k6eAd1
větší	veliký	k2eAgFnSc1d2
čtvrť	čtvrť	k1gFnSc1
Řečkovice	Řečkovice	k1gFnSc2
má	mít	k5eAaImIp3nS
městský	městský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
podstatně	podstatně	k6eAd1
menší	malý	k2eAgFnSc1d2
čtvrť	čtvrť	k1gFnSc1
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
je	být	k5eAaImIp3nS
spíše	spíše	k9
vesnicí	vesnice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východ	východ	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
pokrývají	pokrývat	k5eAaImIp3nP
relativně	relativně	k6eAd1
rozsáhlé	rozsáhlý	k2eAgInPc1d1
lesy	les	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Brno-Bohunice	Brno-Bohunice	k1gFnSc1
</s>
<s>
ČástiBohunice	ČástiBohunice	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
14	#num#	k4
683	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
3,02	3,02	k4
km²	km²	k?
</s>
<s>
Bohunice	Bohunice	k1gFnPc1
mají	mít	k5eAaImIp3nP
spíše	spíše	k9
městský	městský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
a	a	k8xC
zvlněnou	zvlněný	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
se	s	k7c7
znatelnými	znatelný	k2eAgInPc7d1
výškovými	výškový	k2eAgInPc7d1
rozdíly	rozdíl	k1gInPc7
v	v	k7c6
zástavbě	zástavba	k1gFnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
rozsáhlé	rozsáhlý	k2eAgNnSc4d1
panelové	panelový	k2eAgNnSc4d1
sídliště	sídliště	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
středu	střed	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
zástavba	zástavba	k1gFnSc1
zbytku	zbytek	k1gInSc2
původní	původní	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižně	jižně	k6eAd1
od	od	k7c2
původní	původní	k2eAgFnSc2d1
zástavby	zástavba	k1gFnSc2
protéká	protékat	k5eAaImIp3nS
říčka	říčka	k1gFnSc1
Leskava	Leskava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
od	od	k7c2
sídliště	sídliště	k1gNnSc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
přilehlý	přilehlý	k2eAgInSc1d1
rozsáhlý	rozsáhlý	k2eAgInSc1d1
areál	areál	k1gInSc1
zdejší	zdejší	k2eAgFnSc2d1
fakultní	fakultní	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zasahuje	zasahovat	k5eAaImIp3nS
i	i	k9
na	na	k7c6
území	území	k1gNnSc6
sousední	sousední	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Starý	Brno-Starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
sousední	sousední	k2eAgFnSc7d1
vysokou	vysoký	k2eAgFnSc7d1
zdí	zeď	k1gFnSc7
obehnaný	obehnaný	k2eAgInSc1d1
areál	areál	k1gInSc1
zdejší	zdejší	k2eAgFnSc2d1
vazební	vazební	k2eAgFnSc2d1
věznice	věznice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejzápadnější	západní	k2eAgFnSc6d3
části	část	k1gFnSc6
katastru	katastr	k1gInSc2
Bohunic	Bohunice	k1gFnPc2
je	být	k5eAaImIp3nS
vybudována	vybudován	k2eAgFnSc1d1
severní	severní	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
obchodního	obchodní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
Campus	Campus	k1gMnSc1
Square	square	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
jižní	jižní	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
již	již	k6eAd1
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
sousední	sousední	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Starý	Brno-Starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Brno-Vinohrady	Brno-Vinohrada	k1gFnPc1
</s>
<s>
Částizčásti	Částizčást	k1gFnPc1
<g/>
:	:	kIx,
Židenice	Židenice	k1gFnPc1
<g/>
,	,	kIx,
Maloměřice	Maloměřice	k1gFnPc1
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
13	#num#	k4
361	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
2,28	2,28	k4
km²	km²	k?
</s>
<s>
Drtivou	drtivý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
zástavby	zástavba	k1gFnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
asi	asi	k9
150	#num#	k4
barevných	barevný	k2eAgInPc2d1
panelových	panelový	k2eAgInPc2d1
domů	dům	k1gInPc2
sídliště	sídliště	k1gNnSc2
Vinohrady	Vinohrady	k1gInPc4
<g/>
,	,	kIx,
rozkládajícího	rozkládající	k2eAgMnSc2d1
se	se	k3xPyFc4
na	na	k7c6
protáhlé	protáhlý	k2eAgFnSc6d1
vyvýšenině	vyvýšenina	k1gFnSc6
na	na	k7c6
jihu	jih	k1gInSc6
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silueta	silueta	k1gFnSc1
sídliště	sídliště	k1gNnSc2
je	být	k5eAaImIp3nS
dobře	dobře	k6eAd1
viditelná	viditelný	k2eAgFnSc1d1
z	z	k7c2
mnoha	mnoho	k4c2
stran	strana	k1gFnPc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgFnPc4
ulice	ulice	k1gFnPc4
v	v	k7c6
této	tento	k3xDgFnSc6
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
jsou	být	k5eAaImIp3nP
pojmenované	pojmenovaný	k2eAgFnPc1d1
podle	podle	k7c2
obcí	obec	k1gFnPc2
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
střed	střed	k1gInSc1
sídliště	sídliště	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nS
Pálavské	pálavský	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsevernějším	severní	k2eAgInPc3d3
a	a	k8xC
zároveň	zároveň	k6eAd1
i	i	k9
nejvýchodnějším	východní	k2eAgNnSc7d3
místem	místo	k1gNnSc7
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
jsou	být	k5eAaImIp3nP
pozemky	pozemek	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgFnPc6
stojí	stát	k5eAaImIp3nS
hotel	hotel	k1gInSc1
Velká	velký	k2eAgFnSc1d1
Klajdovka	Klajdovka	k1gFnSc1
a	a	k8xC
přilehlá	přilehlý	k2eAgFnSc1d1
hájenka	hájenka	k1gFnSc1
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
poblíž	poblíž	k6eAd1
Hádů	hádes	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Starý	Brno-Starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Částizčásti	Částizčást	k1gFnPc1
<g/>
:	:	kIx,
Starý	starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
(	(	kIx(
<g/>
většina	většina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
12	#num#	k4
931	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
3,28	3,28	k4
km²	km²	k?
</s>
<s>
Zástavbu	zástavba	k1gFnSc4
Starého	Starého	k2eAgInSc2d1
Lískovce	Lískovec	k1gInSc2
dnes	dnes	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
převážně	převážně	k6eAd1
panelové	panelový	k2eAgInPc1d1
domy	dům	k1gInPc1
postavené	postavený	k2eAgInPc1d1
na	na	k7c6
konci	konec	k1gInSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
severně	severně	k6eAd1
a	a	k8xC
západně	západně	k6eAd1
od	od	k7c2
vesnické	vesnický	k2eAgFnSc2d1
rodinné	rodinný	k2eAgFnSc2d1
zástavby	zástavba	k1gFnSc2
původní	původní	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejsevernější	severní	k2eAgFnSc6d3
části	část	k1gFnSc6
katastru	katastr	k1gInSc2
Starého	Starého	k2eAgInSc2d1
Lískovce	Lískovec	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
nejnovější	nový	k2eAgFnSc1d3
vícepatrová	vícepatrový	k2eAgFnSc1d1
budova-poliklinika	budova-poliklinik	k1gMnSc2
<g/>
,	,	kIx,
starší	starý	k2eAgFnSc1d2
nejvyšší	vysoký	k2eAgFnSc1d3
budova	budova	k1gFnSc1
v	v	k7c6
areálu	areál	k1gInSc6
s	s	k7c7
nápisem	nápis	k1gInSc7
FAKULTNÍ	fakultní	k2eAgFnSc2d1
NEMOCNICE	nemocnice	k1gFnSc2
BRNO	Brno	k1gNnSc1
a	a	k8xC
porodnice	porodnice	k1gFnSc1
fakultní	fakultní	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2007	#num#	k4
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Moravského	moravský	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
archivu	archiv	k1gInSc2
<g/>
,	,	kIx,
koncem	koncem	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
zde	zde	k6eAd1
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
i	i	k8xC
nové	nový	k2eAgNnSc1d1
nákupní	nákupní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Campus	Campus	k1gInSc1
Square	square	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
však	však	k9
zasahuje	zasahovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
sousední	sousední	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Bohunice	Brno-Bohunice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zámeček	zámeček	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
budova	budova	k1gFnSc1
v	v	k7c6
Lískovci	Lískovec	k1gInSc6
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgMnS
nic	nic	k3yNnSc1
jiného	jiný	k2eAgNnSc2d1
než	než	k8xS
starý	starý	k2eAgInSc1d1
panský	panský	k2eAgInSc1d1
hospodářský	hospodářský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
si	se	k3xPyFc3
v	v	k7c6
Lískovci	Lískovec	k1gInSc6
zřídily	zřídit	k5eAaPmAgFnP
starobrněnské	starobrněnský	k2eAgFnPc1d1
cisterciačky	cisterciačka	k1gFnPc1
a	a	k8xC
měly	mít	k5eAaImAgFnP
na	na	k7c6
něm	on	k3xPp3gInSc6
vždy	vždy	k6eAd1
své	svůj	k3xOyFgNnSc4
správce	správce	k1gMnSc1
panství	panství	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
Starý	starý	k2eAgInSc4d1
Lískovec	Lískovec	k1gInSc4
patřil	patřit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyl	být	k5eNaImAgMnS
to	ten	k3xDgNnSc4
tedy	tedy	k8xC
soukromý	soukromý	k2eAgInSc1d1
majetek	majetek	k1gInSc1
<g/>
,	,	kIx,
tím	ten	k3xDgMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
až	až	k9
po	po	k7c6
zrušení	zrušení	k1gNnSc6
cisterciáckého	cisterciácký	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1782	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
náboženský	náboženský	k2eAgInSc1d1
fond	fond	k1gInSc1
i	i	k8xC
následní	následní	k2eAgMnPc1d1
majitelé	majitel	k1gMnPc1
v	v	k7c6
něm	on	k3xPp3gNnSc6
vždycky	vždycky	k6eAd1
viděli	vidět	k5eAaImAgMnP
především	především	k9
hospodářské	hospodářský	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc1
</s>
<s>
ČástiKohoutovice	ČástiKohoutovice	k1gFnSc1
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
Pisárky	Pisárka	k1gFnSc2
(	(	kIx(
<g/>
západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jundrov	Jundrov	k1gInSc1
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
12	#num#	k4
621	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
4,09	4,09	k4
km²	km²	k?
</s>
<s>
Povrch	povrch	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
ze	z	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
zalesněn	zalesnit	k5eAaPmNgInS
<g/>
,	,	kIx,
se	se	k3xPyFc4
od	od	k7c2
východu	východ	k1gInSc2
pozvolna	pozvolna	k6eAd1
zvedá	zvedat	k5eAaImIp3nS
<g/>
,	,	kIx,
městskou	městský	k2eAgFnSc7d1
částí	část	k1gFnSc7
protéká	protékat	k5eAaImIp3nS
Kohoutovický	kohoutovický	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
čtvrť	čtvrť	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
níže	nízce	k6eAd2
položená	položený	k2eAgFnSc1d1
relativně	relativně	k6eAd1
nevelká	velký	k2eNgFnSc1d1
zástavba	zástavba	k1gFnSc1
původní	původní	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
důvodu	důvod	k1gInSc2
značně	značně	k6eAd1
členitého	členitý	k2eAgInSc2d1
terénu	terén	k1gInSc2
obklopená	obklopený	k2eAgFnSc1d1
nesouvislou	souvislý	k2eNgFnSc7d1
zástavbou	zástavba	k1gFnSc7
zdejšího	zdejší	k2eAgNnSc2d1
panelového	panelový	k2eAgNnSc2d1
sídliště	sídliště	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
převážně	převážně	k6eAd1
na	na	k7c6
kopcích	kopec	k1gInPc6
nad	nad	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pisárecká	pisárecký	k2eAgFnSc1d1
část	část	k1gFnSc1
zástavby	zástavba	k1gFnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
domy	dům	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
až	až	k6eAd1
na	na	k7c4
jedinou	jediný	k2eAgFnSc4d1
výjimku	výjimka	k1gFnSc4
nacházejí	nacházet	k5eAaImIp3nP
na	na	k7c6
severním	severní	k2eAgInSc6d1
svahu	svah	k1gInSc6
ulic	ulice	k1gFnPc2
Antonína	Antonín	k1gMnSc2
Procházky	Procházka	k1gMnSc2
a	a	k8xC
Libušino	Libušin	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
několika	několik	k4yIc7
panelovými	panelový	k2eAgInPc7d1
domy	dům	k1gInPc7
přiléhajícími	přiléhající	k2eAgInPc7d1
k	k	k7c3
severovýchodní	severovýchodní	k2eAgFnSc3d1
části	část	k1gFnSc3
katastru	katastr	k1gInSc6
Kohoutovic	Kohoutovice	k1gFnPc2
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Libušina	Libušin	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
izolovanou	izolovaný	k2eAgFnSc7d1
jižněji	jižně	k6eAd2
položenou	položený	k2eAgFnSc7d1
budovou	budova	k1gFnSc7
zdejší	zdejší	k2eAgFnSc2d1
myslivny	myslivna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástavba	zástavba	k1gFnSc1
jundrovské	jundrovský	k2eAgFnSc2d1
části	část	k1gFnSc2
katastru	katastr	k1gInSc2
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
chatami	chata	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominantou	dominanta	k1gFnSc7
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
dobře	dobře	k6eAd1
viditelnou	viditelný	k2eAgFnSc4d1
z	z	k7c2
mnoha	mnoho	k4c2
částí	část	k1gFnPc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
objekt	objekt	k1gInSc4
zdejšího	zdejší	k2eAgInSc2d1
kohoutovického	kohoutovický	k2eAgInSc2d1
vodojemu	vodojem	k1gInSc2
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Kohoutovice	Kohoutovice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Brno-Nový	Brno-Nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
Částizčásti	Částizčást	k1gFnPc1
<g/>
:	:	kIx,
Nový	nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
(	(	kIx(
<g/>
většina	většina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Starý	starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
11	#num#	k4
349	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
1,66	1,66	k4
km²	km²	k?
</s>
<s>
Nový	nový	k2eAgInSc4d1
Lískovec	Lískovec	k1gInSc4
rozkládající	rozkládající	k2eAgInSc4d1
se	se	k3xPyFc4
na	na	k7c6
úpatí	úpatí	k1gNnSc6
Kamenného	kamenný	k2eAgInSc2d1
vrchu	vrch	k1gInSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
rozdílných	rozdílný	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
je	být	k5eAaImIp3nS
severněji	severně	k6eAd2
položený	položený	k2eAgInSc4d1
původní	původní	k2eAgInSc4d1
Nový	nový	k2eAgInSc4d1
Lískovec	Lískovec	k1gInSc4
<g/>
,	,	kIx,
skládající	skládající	k2eAgInPc4d1
se	se	k3xPyFc4
převážně	převážně	k6eAd1
z	z	k7c2
rodinných	rodinný	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
,	,	kIx,
jižně	jižně	k6eAd1
položené	položený	k2eAgNnSc4d1
panelové	panelový	k2eAgNnSc4d1
sídliště	sídliště	k1gNnSc4
Nový	nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
a	a	k8xC
jihozápadně	jihozápadně	k6eAd1
položené	položený	k2eAgNnSc4d1
panelové	panelový	k2eAgNnSc4d1
sídliště	sídliště	k1gNnSc4
Kamenný	kamenný	k2eAgInSc1d1
Vrch	vrch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
obecně	obecně	k6eAd1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
největším	veliký	k2eAgNnPc3d3
brněnským	brněnský	k2eAgNnPc3d1
panelovým	panelový	k2eAgNnPc3d1
sídlištím	sídliště	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sever	sever	k1gInSc4
Nového	Nového	k2eAgInSc2d1
Lískovce	Lískovec	k1gInSc2
zasahuje	zasahovat	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
Kamenný	kamenný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
až	až	k6eAd1
10	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Brno-jih	Brno-jih	k1gMnSc1
</s>
<s>
ČástiKomárov	ČástiKomárov	k1gInSc1
<g/>
,	,	kIx,
Horní	horní	k2eAgFnPc1d1
Heršpice	Heršpice	k1gFnPc1
<g/>
,	,	kIx,
Dolní	dolní	k2eAgFnPc1d1
Heršpice	Heršpice	k1gFnPc1
<g/>
,	,	kIx,
Přízřenice	Přízřenice	k1gFnPc1
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
<g/>
:	:	kIx,
Trnitá	trnitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
jihovýchodní	jihovýchodní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
9	#num#	k4
690	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
12,77	12,77	k4
km²	km²	k?
</s>
<s>
Původní	původní	k2eAgFnPc1d1
části	část	k1gFnPc1
Dolních	dolní	k2eAgFnPc2d1
Heršpic	Heršpice	k1gFnPc2
a	a	k8xC
Přízřenic	Přízřenice	k1gFnPc2
si	se	k3xPyFc3
dosud	dosud	k6eAd1
zachovávají	zachovávat	k5eAaImIp3nP
vesnický	vesnický	k2eAgInSc4d1
charakter	charakter	k1gInSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
představují	představovat	k5eAaImIp3nP
jedny	jeden	k4xCgInPc1
z	z	k7c2
nejzachovalejších	zachovalý	k2eAgInPc2d3
vesnických	vesnický	k2eAgInPc2d1
celků	celek	k1gInPc2
na	na	k7c6
území	území	k1gNnSc6
moderního	moderní	k2eAgNnSc2d1
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnPc4d1
Heršpice	Heršpice	k1gFnPc4
<g/>
,	,	kIx,
Komárov	Komárov	k1gInSc1
a	a	k8xC
Trnitá	trnitý	k2eAgNnPc1d1
mají	mít	k5eAaImIp3nP
pro	pro	k7c4
změnu	změna	k1gFnSc4
spíše	spíše	k9
městský	městský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
podél	podél	k7c2
Vídeňské	vídeňský	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
nachází	nacházet	k5eAaImIp3nS
významná	významný	k2eAgFnSc1d1
obchodně	obchodně	k6eAd1
průmyslová	průmyslový	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
různých	různý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
také	také	k9
významná	významný	k2eAgNnPc1d1
obchodní	obchodní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
Futurum	futurum	k1gNnSc1
a	a	k8xC
Avion	avion	k1gInSc1
Shopping	shopping	k1gInSc1
Park	park	k1gInSc1
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městskou	městský	k2eAgFnSc7d1
částí	část	k1gFnSc7
prochází	procházet	k5eAaImIp3nS
i	i	k9
několik	několik	k4yIc1
důležitých	důležitý	k2eAgFnPc2d1
dopravních	dopravní	k2eAgFnPc2d1
tepen	tepna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
o	o	k7c4
dálnice	dálnice	k1gFnPc4
D1	D1	k1gFnSc2
a	a	k8xC
D	D	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
rychlostní	rychlostní	k2eAgFnSc4d1
komunikaci	komunikace	k1gFnSc4
E	E	kA
<g/>
461	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Brno-Slatina	Brno-Slatina	k1gFnSc1
</s>
<s>
ČástiSlatina	ČástiSlatina	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
9	#num#	k4
360	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
5,83	5,83	k4
km²	km²	k?
</s>
<s>
Přestože	přestože	k8xS
má	mít	k5eAaImIp3nS
Slatina	slatina	k1gFnSc1
městský	městský	k2eAgInSc1d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
dosud	dosud	k6eAd1
stále	stále	k6eAd1
velice	velice	k6eAd1
dobře	dobře	k6eAd1
zachované	zachovaný	k2eAgNnSc4d1
původní	původní	k2eAgNnSc4d1
vesnické	vesnický	k2eAgNnSc4d1
jádro	jádro	k1gNnSc4
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
přibližně	přibližně	k6eAd1
uprostřed	uprostřed	k7c2
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihem	jih	k1gInSc7
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
prochází	procházet	k5eAaImIp3nS
od	od	k7c2
jihovýchodu	jihovýchod	k1gInSc2
k	k	k7c3
západu	západ	k1gInSc3
Vlárská	vlárský	k2eAgFnSc1d1
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
nádraží	nádraží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podél	podél	k7c2
trati	trať	k1gFnSc2
a	a	k8xC
Řípské	Řípský	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
sousedící	sousedící	k2eAgFnSc1d1
se	s	k7c7
zdejšími	zdejší	k2eAgFnPc7d1
kasárnami	kasárny	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
samém	samý	k3xTgInSc6
jihozápadě	jihozápad	k1gInSc6
katastru	katastr	k1gInSc2
Slatiny	slatina	k1gFnSc2
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
Švédské	švédský	k2eAgInPc1d1
valy	val	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihem	jih	k1gInSc7
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
prochází	procházet	k5eAaImIp3nS
dálnice	dálnice	k1gFnSc1
D	D	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsevernější	severní	k2eAgFnSc1d3
část	část	k1gFnSc1
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
je	být	k5eAaImIp3nS
archeologicky	archeologicky	k6eAd1
významná	významný	k2eAgFnSc1d1
Stránská	Stránská	k1gFnSc1
skála	skála	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
</s>
<s>
ČástiČernovice	ČástiČernovice	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
8	#num#	k4
024	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
6,29	6,29	k4
km²	km²	k?
</s>
<s>
Černovice	Černovice	k1gFnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
menších	malý	k2eAgFnPc2d2
Starých	Stará	k1gFnPc2
a	a	k8xC
větších	veliký	k2eAgFnPc2d2
Nových	Nových	k2eAgFnPc2d1
Černovic	Černovice	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterými	který	k3yQgFnPc7,k3yRgFnPc7,k3yIgFnPc7
se	se	k3xPyFc4
nedaleko	nedaleko	k7c2
budovy	budova	k1gFnSc2
zdejšího	zdejší	k2eAgInSc2d1
textilního	textilní	k2eAgInSc2d1
kombinátu	kombinát	k1gInSc2
Nová	nový	k2eAgFnSc1d1
Mosilana	Mosilana	k1gFnSc1
nachází	nacházet	k5eAaImIp3nS
areál	areál	k1gInSc4
zdejší	zdejší	k2eAgFnSc2d1
psychiatrické	psychiatrický	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Nové	Nové	k2eAgFnPc1d1
Černovice	Černovice	k1gFnPc1
mají	mít	k5eAaImIp3nP
městský	městský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
s	s	k7c7
převážně	převážně	k6eAd1
prvorepublikovými	prvorepublikový	k2eAgInPc7d1
domy	dům	k1gInPc7
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
Staré	Staré	k2eAgFnPc4d1
Černovice	Černovice	k1gFnPc4
vesnický	vesnický	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
převážně	převážně	k6eAd1
zemědělsky	zemědělsky	k6eAd1
a	a	k8xC
zahrádkářsky	zahrádkářsky	k6eAd1
zaměřené	zaměřený	k2eAgFnPc4d1
Černovice	Černovice	k1gFnPc4
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
okrajových	okrajový	k2eAgFnPc6d1
částech	část	k1gFnPc6
mění	měnit	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c4
průmyslovou	průmyslový	k2eAgFnSc4d1
čtvrť	čtvrť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
změna	změna	k1gFnSc1
bývalého	bývalý	k2eAgNnSc2d1
černovického	černovický	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
v	v	k7c4
mediálně	mediálně	k6eAd1
známou	známý	k2eAgFnSc4d1
průmyslovou	průmyslový	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
Černovické	Černovický	k2eAgFnSc2d1
terasy	terasa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozporuplnost	rozporuplnost	k1gFnSc1
tohoto	tento	k3xDgNnSc2
území	území	k1gNnSc2
dokládá	dokládat	k5eAaImIp3nS
vcelku	vcelku	k6eAd1
blízké	blízký	k2eAgNnSc4d1
sousedství	sousedství	k1gNnSc4
brněnské	brněnský	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
skládky	skládka	k1gFnSc2
Černovice	Černovice	k1gFnSc2
a	a	k8xC
malého	malý	k2eAgInSc2d1
kousku	kousek	k1gInSc2
původní	původní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
Černovický	Černovický	k2eAgInSc1d1
hájek	hájek	k1gInSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
však	však	k9
již	již	k6eAd1
k	k	k7c3
Černovicím	Černovice	k1gFnPc3
nenáleží	náležet	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Brno-Komín	Brno-Komín	k1gMnSc1
</s>
<s>
ČástiKomín	ČástiKomín	k1gMnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
7	#num#	k4
457	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
7,60	7,60	k4
km²	km²	k?
</s>
<s>
Komín	Komín	k1gInSc1
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
připojená	připojený	k2eAgFnSc1d1
k	k	k7c3
Brnu	Brno	k1gNnSc3
<g/>
,	,	kIx,
městská	městský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
a	a	k8xC
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
také	také	k9
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
Brna	Brno	k1gNnSc2
rozkládající	rozkládající	k2eAgFnSc1d1
se	se	k3xPyFc4
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
řeky	řeka	k1gFnSc2
Svratky	Svratka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
evidováno	evidovat	k5eAaImNgNnS
43	#num#	k4
ulic	ulice	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
225	#num#	k4
adres	adresa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Medlánky	Brno-Medlánka	k1gFnPc1
</s>
<s>
ČástiMedlánky	ČástiMedlánka	k1gFnPc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
5	#num#	k4
898	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
3,51	3,51	k4
km²	km²	k?
</s>
<s>
Medlánky	Medlánka	k1gFnPc1
jsou	být	k5eAaImIp3nP
bývalá	bývalý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
(	(	kIx(
<g/>
v	v	k7c6
poněkud	poněkud	k6eAd1
odlišných	odlišný	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
<g/>
)	)	kIx)
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
,	,	kIx,
městská	městský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
a	a	k8xC
také	také	k9
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
rozkládající	rozkládající	k2eAgFnSc1d1
se	se	k3xPyFc4
na	na	k7c6
severu	sever	k1gInSc6
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
v	v	k7c6
těsném	těsný	k2eAgNnSc6d1
sousedství	sousedství	k1gNnSc6
s	s	k7c7
Řečkovicemi	Řečkovice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
katastru	katastr	k1gInSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
aeroklubové	aeroklubový	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
s	s	k7c7
provozem	provoz	k1gInSc7
kluzáků	kluzák	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Tuřany	Brno-Tuřan	k1gMnPc4
</s>
<s>
ČástiTuřany	ČástiTuřan	k1gMnPc4
<g/>
,	,	kIx,
Brněnské	brněnský	k2eAgFnPc1d1
Ivanovice	Ivanovice	k1gFnPc1
<g/>
,	,	kIx,
Dvorska	Dvorska	k1gFnSc1
<g/>
,	,	kIx,
Holásky	Holásek	k1gMnPc4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
5	#num#	k4
674	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
17,84	17,84	k4
km²	km²	k?
</s>
<s>
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
města	město	k1gNnSc2
na	na	k7c6
levém	levý	k2eAgInSc6d1
(	(	kIx(
<g/>
východním	východní	k2eAgInSc6d1
<g/>
)	)	kIx)
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Svitavy	Svitava	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
západní	západní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
této	tento	k3xDgFnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
prochází	procházet	k5eAaImIp3nS
naopak	naopak	k6eAd1
po	po	k7c6
pravém	pravý	k2eAgInSc6d1
(	(	kIx(
<g/>
západním	západní	k2eAgInSc6d1
<g/>
)	)	kIx)
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
památek	památka	k1gFnPc2
je	být	k5eAaImIp3nS
známý	známý	k2eAgInSc1d1
kostel	kostel	k1gInSc1
zvěstování	zvěstování	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
historickém	historický	k2eAgNnSc6d1
jádru	jádro	k1gNnSc6
Tuřan	Tuřana	k1gFnPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
z	z	k7c2
některých	některý	k3yIgFnPc2
ulic	ulice	k1gFnPc2
dobře	dobře	k6eAd1
viditelnou	viditelný	k2eAgFnSc4d1
dominantu	dominanta	k1gFnSc4
této	tento	k3xDgFnSc2
čtvrti	čtvrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Tuřany	Brno-Tuřan	k1gMnPc7
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
také	také	k9
tři	tři	k4xCgNnPc1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednak	jednak	k8xC
relativně	relativně	k6eAd1
velká	velká	k1gFnSc1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Černovický	Černovický	k2eAgInSc1d1
hájek	hájek	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Holásecká	Holásecký	k2eAgNnPc1d1
jezera	jezero	k1gNnPc1
a	a	k8xC
Rájecká	Rájecký	k2eAgFnSc1d1
tůň	tůň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východě	východ	k1gInSc6
území	území	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
brněnské	brněnský	k2eAgNnSc1d1
mezinárodní	mezinárodní	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
Airport	Airport	k1gInSc1
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Brno-Maloměřice	Brno-Maloměřice	k1gFnPc1
a	a	k8xC
Obřany	Obřana	k1gFnPc1
</s>
<s>
ČástiObřany	ČástiObřan	k1gMnPc4
<g/>
,	,	kIx,
zčásti	zčásti	k6eAd1
<g/>
:	:	kIx,
Maloměřice	Maloměřice	k1gFnPc1
(	(	kIx(
<g/>
většina	většina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
5	#num#	k4
621	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
9,36	9,36	k4
km²	km²	k?
</s>
<s>
Rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
po	po	k7c6
obou	dva	k4xCgInPc6
březích	břeh	k1gInPc6
řeky	řeka	k1gFnSc2
Svitavy	Svitava	k1gFnSc2
nedaleko	nedaleko	k7c2
Hádů	hádes	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
a	a	k8xC
skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
celého	celý	k2eAgNnSc2d1
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Obřany	Obřana	k1gFnSc2
a	a	k8xC
téměř	téměř	k6eAd1
celého	celý	k2eAgNnSc2d1
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
Maloměřice	Maloměřice	k1gFnPc4
(	(	kIx(
<g/>
k	k	k7c3
této	tento	k3xDgFnSc3
části	část	k1gFnSc3
Maloměřic	Maloměřice	k1gFnPc2
náleží	náležet	k5eAaImIp3nS
od	od	k7c2
konce	konec	k1gInSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
také	také	k6eAd1
někdejší	někdejší	k2eAgFnSc1d1
severní	severní	k2eAgFnSc1d1
exkláva	exkláva	k1gFnSc1
bývalé	bývalý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Juliánova	Juliánův	k2eAgFnSc1d1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
je	být	k5eAaImIp3nS
dnešní	dnešní	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
Baarovo	Baarův	k2eAgNnSc4d1
nábřeží	nábřeží	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katastrální	katastrální	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
čtvrtěmi	čtvrt	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
do	do	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
tvořily	tvořit	k5eAaImAgFnP
samostatné	samostatný	k2eAgFnPc1d1
obce	obec	k1gFnPc1
probíhá	probíhat	k5eAaImIp3nS
středem	střed	k1gInSc7
řeky	řeka	k1gFnSc2
Svitavy	Svitava	k1gFnSc2
a	a	k8xC
Mlýnského	mlýnský	k2eAgInSc2d1
náhonu	náhon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Obřan	Obřana	k1gFnPc2
se	se	k3xPyFc4
<g/>
,	,	kIx,
již	již	k9
mimo	mimo	k7c4
území	území	k1gNnSc4
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
nachází	nacházet	k5eAaImIp3nS
také	také	k9
hrad	hrad	k1gInSc4
Obřany	Obřana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
účely	účel	k1gInPc4
senátních	senátní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
je	být	k5eAaImIp3nS
území	území	k1gNnSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Maloměřice	Brno-Maloměřice	k1gFnSc2
a	a	k8xC
Obřany	Obřana	k1gFnSc2
zařazeno	zařadit	k5eAaPmNgNnS
do	do	k7c2
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc1
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1
městské	městský	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
,	,	kIx,
pod	pod	k7c4
5	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Brno-Jundrov	Brno-Jundrov	k1gInSc1
</s>
<s>
Částizčásti	Částizčást	k1gFnPc1
<g/>
:	:	kIx,
Jundrov	Jundrov	k1gInSc1
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
celý	celý	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pisárky	Pisárek	k1gInPc1
(	(	kIx(
<g/>
jen	jen	k9
ZSJ	ZSJ	kA
Výšina	výšina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
4	#num#	k4
132	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
4,15	4,15	k4
km²	km²	k?
</s>
<s>
Povrch	povrch	k1gInSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejklidnějším	klidný	k2eAgFnPc3d3
částem	část	k1gFnPc3
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
pozvolna	pozvolna	k6eAd1
zvedá	zvedat	k5eAaImIp3nS
od	od	k7c2
hladiny	hladina	k1gFnSc2
řeky	řeka	k1gFnSc2
Svratky	Svratka	k1gFnSc2
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
severu	sever	k1gInSc6
až	až	k8xS
k	k	k7c3
západněji	západně	k6eAd2
položeným	položený	k2eAgInPc3d1
zalesněným	zalesněný	k2eAgInPc3d1
kopcům	kopec	k1gInPc3
nad	nad	k7c7
jundrovským	jundrovský	k2eAgNnSc7d1
sídlištěm	sídliště	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
městské	městský	k2eAgFnPc1d1
části	část	k1gFnPc1
dominují	dominovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
ulic	ulice	k1gFnPc2
je	být	k5eAaImIp3nS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
stromech	strom	k1gInPc6
a	a	k8xC
keřích	keř	k1gInPc6
(	(	kIx(
<g/>
Dubová	dubový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Březová	březový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jasanová	jasanový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Sosnová	sosnový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Šeříková	šeříkový	k2eAgFnSc1d1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
menšina	menšina	k1gFnSc1
pak	pak	k6eAd1
po	po	k7c6
osobnostech	osobnost	k1gFnPc6
moravských	moravský	k2eAgInPc2d1
–	–	k?
Optátova	Optátův	k2eAgMnSc2d1
(	(	kIx(
<g/>
Václav	Václav	k1gMnSc1
Beneš	Beneš	k1gMnSc1
Optát	Optát	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pivoňkova	Pivoňkův	k2eAgFnSc1d1
(	(	kIx(
<g/>
Alois	Alois	k1gMnSc1
Pivoňka	Pivoňka	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
slovenských	slovenský	k2eAgFnPc2d1
–	–	k?
Nálepkova	Nálepkův	k2eAgMnSc2d1
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
Ján	Ján	k1gMnSc1
Nálepka	nálepka	k1gFnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
českých	český	k2eAgInPc2d1
–	–	k?
Gellnerova	Gellnerův	k2eAgMnSc2d1
(	(	kIx(
<g/>
František	František	k1gMnSc1
Gellner	Gellner	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tyršovo	Tyršův	k2eAgNnSc1d1
návrší	návrší	k1gNnSc1
(	(	kIx(
<g/>
Miroslav	Miroslav	k1gMnSc1
Tyrš	Tyrš	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Jundrově	Jundrov	k1gInSc6
sídlí	sídlet	k5eAaImIp3nS
obecní	obecní	k2eAgFnSc1d1
Mateřská	mateřský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Dubová	dubový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Jasanová	jasanový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Soukromá	soukromý	k2eAgFnSc1d1
mateřská	mateřský	k2eAgFnSc1d1
a	a	k8xC
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Rozmarýnová	rozmarýnový	k2eAgFnSc1d1
a	a	k8xC
soukromá	soukromý	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Akademie	akademie	k1gFnSc2
STING	STING	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
jundrovským	jundrovský	k2eAgMnPc3d1
občanům	občan	k1gMnPc3
slouží	sloužit	k5eAaImIp3nS
kaple	kaple	k1gFnSc1
Pána	pán	k1gMnSc2
Ježíše	Ježíš	k1gMnSc2
v	v	k7c6
Getsemanech	Getsemany	k1gInPc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
stojí	stát	k5eAaImIp3nS
za	za	k7c7
řekou	řeka	k1gFnSc7
Svratkou	Svratka	k1gFnSc7
na	na	k7c6
katastru	katastr	k1gInSc6
Žabovřesk	Žabovřesky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Chrlice	Brno-Chrlice	k1gFnSc1
</s>
<s>
ČástiChrlice	ČástiChrlice	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
3	#num#	k4
722	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
9,5	9,5	k4
km²	km²	k?
</s>
<s>
Chrlice	Chrlice	k1gFnPc1
leží	ležet	k5eAaImIp3nP
na	na	k7c6
levém	levý	k2eAgInSc6d1
(	(	kIx(
<g/>
východním	východní	k2eAgInSc6d1
<g/>
)	)	kIx)
břehu	břeh	k1gInSc6
Svratky	Svratka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrlice	Chrlice	k1gFnPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
podstatě	podstata	k1gFnSc6
charakter	charakter	k1gInSc4
velké	velký	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
však	však	k9
narušen	narušen	k2eAgInSc1d1
menším	malý	k2eAgNnSc7d2
panelovým	panelový	k2eAgNnSc7d1
sídlištěm	sídliště	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
především	především	k9
z	z	k7c2
jihu	jih	k1gInSc2
a	a	k8xC
západu	západ	k1gInSc2
dobře	dobře	k6eAd1
viditelnými	viditelný	k2eAgInPc7d1
vysokými	vysoký	k2eAgInPc7d1
zařízeními	zařízení	k1gNnPc7
zdejšího	zdejší	k2eAgInSc2d1
průmyslového	průmyslový	k2eAgInSc2d1
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
chrlický	chrlický	k2eAgInSc4d1
intravilán	intravilán	k1gInSc4
se	se	k3xPyFc4
rozkládají	rozkládat	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgFnPc1d1
plochy	plocha	k1gFnPc1
orné	orný	k2eAgFnSc2d1
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
vyplňují	vyplňovat	k5eAaImIp3nP
většinu	většina	k1gFnSc4
chrlického	chrlický	k2eAgInSc2d1
katastru	katastr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrlice	Chrlice	k1gFnPc1
patří	patřit	k5eAaImIp3nP
do	do	k7c2
oblasti	oblast	k1gFnSc2
T2	T2	k1gFnSc2
–	–	k?
nejsušší	suchý	k2eAgFnSc4d3
a	a	k8xC
nejteplejší	teplý	k2eAgFnSc4d3
oblast	oblast	k1gFnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Chrlicích	Chrlice	k1gFnPc6
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
mnoho	mnoho	k4c1
různých	různý	k2eAgFnPc2d1
společenských	společenský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
Sbor	sbor	k1gInSc1
dobrovolných	dobrovolný	k2eAgMnPc2d1
hasičů	hasič	k1gMnPc2
<g/>
,	,	kIx,
TJ	tj	kA
Sokol	Sokol	k1gInSc1
<g/>
,	,	kIx,
Vinařský	vinařský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
Spolek	spolek	k1gInSc1
pro	pro	k7c4
zachování	zachování	k1gNnSc4
kulturních	kulturní	k2eAgFnPc2d1
tradic	tradice	k1gFnPc2
<g/>
,	,	kIx,
Klub	klub	k1gInSc1
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
Fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
SK	Sk	kA
Chrlice	Chrlice	k1gFnPc1
<g/>
,	,	kIx,
Automotoklub	automotoklub	k1gInSc1
<g/>
,	,	kIx,
Tenisový	tenisový	k2eAgInSc1d1
klub	klub	k1gInSc1
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
centru	centrum	k1gNnSc6
Chrlic	Chrlice	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
bezprostřední	bezprostřední	k2eAgNnSc4d1
okolí	okolí	k1gNnSc4
tvoří	tvořit	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
část	část	k1gFnSc1
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
budova	budova	k1gFnSc1
bývalého	bývalý	k2eAgInSc2d1
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Žebětín	Brno-Žebětín	k1gMnSc1
</s>
<s>
ČástiŽebětín	ČástiŽebětín	k1gMnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
3	#num#	k4
577	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
13,6	13,6	k4
km²	km²	k?
</s>
<s>
Žebětín	Žebětín	k1gInSc1
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
městská	městský	k2eAgFnSc1d1
čtvrť	čtvrť	k1gFnSc1
<g/>
,	,	kIx,
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
a	a	k8xC
městaská	městaský	k2eAgFnSc1d1
část	část	k1gFnSc1
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	s	k7c7
9	#num#	k4
km	km	kA
západně	západně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Masarykova	Masarykův	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
charakter	charakter	k1gInSc4
velké	velký	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
účely	účel	k1gInPc4
senátních	senátní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
je	být	k5eAaImIp3nS
území	území	k1gNnSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Brno-Žebětín	Brno-Žebětína	k1gFnPc2
zařazeno	zařadit	k5eAaPmNgNnS
do	do	k7c2
volebního	volební	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
číslo	číslo	k1gNnSc1
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Brno-Bosonohy	Brno-Bosonoh	k1gInPc1
</s>
<s>
ČástiBosonohy	ČástiBosonoh	k1gInPc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
2	#num#	k4
457	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
7,15	7,15	k4
km²	km²	k?
</s>
<s>
Bosonohy	Bosonohy	k?
jsou	být	k5eAaImIp3nP
bývalá	bývalý	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
a	a	k8xC
také	také	k9
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
Bosonohy	Bosonohy	k?
si	se	k3xPyFc3
dodnes	dodnes	k6eAd1
zachovávají	zachovávat	k5eAaImIp3nP
charakter	charakter	k1gInSc4
větší	veliký	k2eAgFnSc2d2
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Ivanovice	Brno-Ivanovice	k1gFnSc1
</s>
<s>
ČástiIvanovice	ČástiIvanovice	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
1	#num#	k4
746	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
2,45	2,45	k4
km²	km²	k?
</s>
<s>
Rozkládají	rozkládat	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c6
severním	severní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
západně	západně	k6eAd1
od	od	k7c2
železniční	železniční	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
z	z	k7c2
Brna	Brno	k1gNnSc2
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
východním	východní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
zalesněných	zalesněný	k2eAgInPc2d1
kopců	kopec	k1gInPc2
Přírodního	přírodní	k2eAgInSc2d1
parku	park	k1gInSc2
Baba	baba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
Brnu	Brno	k1gNnSc3
byla	být	k5eAaImAgFnS
obec	obec	k1gFnSc1
Ivanovice	Ivanovice	k1gFnPc1
u	u	k7c2
Brna	Brno	k1gNnSc2
připojena	připojen	k2eAgFnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1971	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ivanovice	Ivanovice	k1gFnPc1
mají	mít	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
vesnický	vesnický	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
Ivanovicích	Ivanovice	k1gFnPc6
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
rodinné	rodinný	k2eAgFnSc2d1
zástavby	zástavba	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
na	na	k7c6
přelomu	přelom	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
dosáhla	dosáhnout	k5eAaPmAgFnS
k	k	k7c3
severní	severní	k2eAgFnSc3d1
hranici	hranice	k1gFnSc3
katastru	katastr	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c4
ni	on	k3xPp3gFnSc4
plynule	plynule	k6eAd1
navazuje	navazovat	k5eAaImIp3nS
rodinná	rodinný	k2eAgFnSc1d1
zástavba	zástavba	k1gFnSc1
sousední	sousední	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
na	na	k7c6
severu	sever	k1gInSc6
Ivanovic	Ivanovice	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
(	(	kIx(
<g/>
květen	květen	k1gInSc1
2009	#num#	k4
<g/>
)	)	kIx)
nacházejí	nacházet	k5eAaImIp3nP
již	již	k6eAd1
tři	tři	k4xCgFnPc1
ulice	ulice	k1gFnPc1
(	(	kIx(
<g/>
Fedrova	Fedrův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Ivanovických	ivanovický	k2eAgMnPc2d1
legionářů	legionář	k1gMnPc2
a	a	k8xC
Lysická	Lysická	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
od	od	k7c2
ostatních	ostatní	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
oddělené	oddělený	k2eAgInPc4d1
uzamykatelnými	uzamykatelný	k2eAgInPc7d1
vraty	vrat	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
obývány	obýván	k2eAgInPc1d1
sociálně	sociálně	k6eAd1
silnými	silný	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
jejich	jejich	k3xOp3gFnSc1
segregace	segregace	k1gFnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
nevraživosti	nevraživost	k1gFnSc3
mezi	mezi	k7c7
jimi	on	k3xPp3gInPc7
a	a	k8xC
starousedlíky	starousedlík	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
způsobuje	způsobovat	k5eAaImIp3nS
kritické	kritický	k2eAgInPc4d1
problémy	problém	k1gInPc4
například	například	k6eAd1
při	při	k7c6
příjezdu	příjezd	k1gInSc6
sanitek	sanitka	k1gFnPc2
či	či	k8xC
hasičů	hasič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Brno-Jehnice	Brno-Jehnice	k1gFnSc1
</s>
<s>
ČástiJehnice	ČástiJehnice	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
1	#num#	k4
102	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
4,07	4,07	k4
km²	km²	k?
</s>
<s>
Jehnice	Jehnice	k1gFnPc1
mají	mít	k5eAaImIp3nP
charakter	charakter	k1gInSc4
velké	velký	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
protéká	protékat	k5eAaImIp3nS
Jehnický	Jehnický	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
vlévající	vlévající	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
západě	západ	k1gInSc6
katastru	katastr	k1gInSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
do	do	k7c2
potoka	potok	k1gInSc2
Ponávky	Ponávka	k1gFnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
západním	západní	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
katastru	katastr	k1gInSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
protéká	protékat	k5eAaImIp3nS
od	od	k7c2
severu	sever	k1gInSc2
k	k	k7c3
jihu	jih	k1gInSc3
a	a	k8xC
napájí	napájet	k5eAaImIp3nS
zde	zde	k6eAd1
svojí	svůj	k3xOyFgFnSc7
vodou	voda	k1gFnSc7
tři	tři	k4xCgInPc4
rybníky	rybník	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
a	a	k8xC
jihovýchodním	jihovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
jehnického	jehnický	k2eAgInSc2d1
intravilánu	intravilán	k1gInSc2
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
výstavby	výstavba	k1gFnSc2
a	a	k8xC
vzniklo	vzniknout	k5eAaPmAgNnS
zde	zde	k6eAd1
několik	několik	k4yIc4
nových	nový	k2eAgFnPc2d1
ulic	ulice	k1gFnPc2
s	s	k7c7
řadou	řada	k1gFnSc7
moderních	moderní	k2eAgInPc2d1
úhledných	úhledný	k2eAgInPc2d1
rodinných	rodinný	k2eAgInPc2d1
domků	domek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Kníničky	Brno-Knínička	k1gFnPc1
</s>
<s>
ČástiKníničky	ČástiKnínička	k1gFnPc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
1	#num#	k4
006	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
10,92	10,92	k4
km²	km²	k?
</s>
<s>
Kníničky	Knínička	k1gFnPc1
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
u	u	k7c2
Brněnské	brněnský	k2eAgFnSc2d1
přehrady	přehrada	k1gFnSc2
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Svratky	Svratka	k1gFnSc2
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
si	se	k3xPyFc3
zachovávají	zachovávat	k5eAaImIp3nP
vesnický	vesnický	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
rozšiřování	rozšiřování	k1gNnSc3
zástavby	zástavba	k1gFnSc2
Kníniček	Knínička	k1gFnPc2
severním	severní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástavba	zástavba	k1gFnSc1
samotných	samotný	k2eAgFnPc2d1
Kníniček	Knínička	k1gFnPc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
při	při	k7c6
východní	východní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
pod	pod	k7c7
úpatím	úpatí	k1gNnSc7
Mniší	mniší	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
katastru	katastr	k1gInSc2
Kníniček	Knínička	k1gFnPc2
je	být	k5eAaImIp3nS
i	i	k9
část	část	k1gFnSc1
Brněnské	brněnský	k2eAgFnSc2d1
přehrady	přehrada	k1gFnSc2
včetně	včetně	k7c2
poloviny	polovina	k1gFnSc2
přehradní	přehradní	k2eAgFnSc2d1
hráze	hráz	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
dvě	dva	k4xCgFnPc1
třetiny	třetina	k1gFnPc1
katastru	katastr	k1gInSc2
Kníniček	Knínička	k1gFnPc2
pokrývají	pokrývat	k5eAaImIp3nP
Podkomorské	podkomorský	k2eAgInPc1d1
lesy	les	k1gInPc1
<g/>
,	,	kIx,
rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
západně	západně	k6eAd1
od	od	k7c2
kníničské	kníničský	k2eAgFnSc2d1
zástavby	zástavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katastrem	katastr	k1gInSc7
Kníniček	Knínička	k1gFnPc2
prochází	procházet	k5eAaImIp3nS
trasa	trasa	k1gFnSc1
nedokončené	dokončený	k2eNgFnSc2d1
exteritoriální	exteritoriální	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
Vídeň	Vídeň	k1gFnSc1
<g/>
–	–	k?
<g/>
Vratislav	Vratislav	k1gMnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Hitlerovy	Hitlerův	k2eAgFnPc4d1
dálnice	dálnice	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Brno-Útěchov	Brno-Útěchov	k1gInSc1
</s>
<s>
ČástiÚtěchov	ČástiÚtěchov	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
760	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
1,18	1,18	k4
km²	km²	k?
</s>
<s>
Útěchov	Útěchov	k1gInSc1
je	být	k5eAaImIp3nS
nejsevernější	severní	k2eAgFnSc7d3
a	a	k8xC
rozlohou	rozloha	k1gFnSc7
také	také	k9
nejmenší	malý	k2eAgFnSc1d3
městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
druhá	druhý	k4xOgFnSc1
nejmenší	malý	k2eAgFnSc1d3
–	–	k?
před	před	k7c7
Ořešínem	Ořešín	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útěchov	Útěchov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
obklopen	obklopen	k2eAgInSc4d1
lesy	les	k1gInPc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
relativně	relativně	k6eAd1
čisté	čistý	k2eAgNnSc4d1
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
za	za	k7c7
účelem	účel	k1gInSc7
relaxace	relaxace	k1gFnSc2
vyhledáván	vyhledávat	k5eAaImNgInS
obyvateli	obyvatel	k1gMnPc7
ostatních	ostatní	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autobusové	autobusový	k2eAgNnSc1d1
spojení	spojení	k1gNnPc1
zajišťují	zajišťovat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
linky	linka	k1gFnPc4
provozované	provozovaný	k2eAgNnSc1d1
Dopravním	dopravní	k2eAgInSc7d1
podnikem	podnik	k1gInSc7
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Brno-Ořešín	Brno-Ořešín	k1gMnSc1
</s>
<s>
ČástiOřešín	ČástiOřešín	k1gMnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
577	#num#	k4
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
<g/>
3,07	3,07	k4
km²	km²	k?
</s>
<s>
Ořešín	Ořešín	k1gMnSc1
si	se	k3xPyFc3
zachovává	zachovávat	k5eAaImIp3nS
vesnický	vesnický	k2eAgInSc4d1
charakter	charakter	k1gInSc4
s	s	k7c7
rodinnou	rodinný	k2eAgFnSc7d1
zástavbou	zástavba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
k	k	k7c3
nejmenším	malý	k2eAgFnPc3d3
brněnským	brněnský	k2eAgFnPc3d1
městským	městský	k2eAgFnPc3d1
částem	část	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ořešín	Ořešín	k1gInSc1
je	být	k5eAaImIp3nS
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
,	,	kIx,
východu	východ	k1gInSc2
a	a	k8xC
jihu	jih	k1gInSc2
obklopen	obklopen	k2eAgInSc4d1
lesy	les	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
zaujímají	zaujímat	k5eAaImIp3nP
téměř	téměř	k6eAd1
dvě	dva	k4xCgFnPc1
třetiny	třetina	k1gFnPc1
ořešínského	ořešínský	k2eAgInSc2d1
katastru	katastr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lese	les	k1gInSc6
na	na	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
obcí	obec	k1gFnSc7
Vranov	Vranov	k1gInSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
Babí	babit	k5eAaImIp3nP
doly	dol	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1
výměra	výměra	k1gFnSc1
[	[	kIx(
<g/>
km²	km²	k?
<g/>
]	]	kIx)
</s>
<s>
PSČ	PSČ	kA
</s>
<s>
Zákl	Zákla	k1gFnPc2
<g/>
.	.	kIx.
sídelních	sídelní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Brno-střed	Brno-střed	k1gMnSc1
</s>
<s>
64	#num#	k4
316	#num#	k4
</s>
<s>
15,00	15,00	k4
</s>
<s>
602	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
603	#num#	k4
00	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
Brno-sever	Brno-sever	k1gMnSc1
</s>
<s>
47	#num#	k4
643	#num#	k4
</s>
<s>
12,24	12,24	k4
</s>
<s>
602	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
613	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
614	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
638	#num#	k4
00	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
Brno-Královo	Brno-Králův	k2eAgNnSc1d1
Pole	pole	k1gNnSc1
</s>
<s>
28	#num#	k4
674	#num#	k4
</s>
<s>
9,91	9,91	k4
</s>
<s>
602	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
612	#num#	k4
00	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
Brno-Líšeň	Brno-Líšeň	k1gFnSc1
</s>
<s>
26	#num#	k4
781	#num#	k4
</s>
<s>
15,71	15,71	k4
</s>
<s>
628	#num#	k4
00	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Brno-Bystrc	Brno-Bystrc	k6eAd1
</s>
<s>
24	#num#	k4
218	#num#	k4
</s>
<s>
27,24	27,24	k4
</s>
<s>
635	#num#	k4
00	#num#	k4
až	až	k9
641	#num#	k4
00	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Brno-Židenice	Brno-Židenice	k1gFnSc1
</s>
<s>
22	#num#	k4
000	#num#	k4
</s>
<s>
3,03	3,03	k4
</s>
<s>
615	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
636	#num#	k4
00	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Brno-Žabovřesky	Brno-Žabovřesky	k6eAd1
</s>
<s>
21	#num#	k4
047	#num#	k4
</s>
<s>
4,35	4,35	k4
</s>
<s>
616	#num#	k4
00	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
Brno-Řečkovice	Brno-Řečkovice	k1gFnSc1
a	a	k8xC
Mokrá	mokrý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
15	#num#	k4
486	#num#	k4
</s>
<s>
7,57	7,57	k4
</s>
<s>
621	#num#	k4
00	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Brno-Bohunice	Brno-Bohunice	k1gFnSc1
</s>
<s>
14	#num#	k4
683	#num#	k4
</s>
<s>
3,02	3,02	k4
</s>
<s>
625	#num#	k4
00	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Brno-Vinohrady	Brno-Vinohrada	k1gFnPc1
</s>
<s>
13	#num#	k4
361	#num#	k4
</s>
<s>
2,28	2,28	k4
</s>
<s>
628	#num#	k4
00	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Brno-Starý	Brno-Starý	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
12	#num#	k4
931	#num#	k4
</s>
<s>
3,28	3,28	k4
</s>
<s>
625	#num#	k4
00	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Brno-Kohoutovice	Brno-Kohoutovice	k1gFnSc1
</s>
<s>
12	#num#	k4
621	#num#	k4
</s>
<s>
4,09	4,09	k4
</s>
<s>
623	#num#	k4
00	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Brno-Nový	Brno-Nový	k2eAgInSc1d1
Lískovec	Lískovec	k1gInSc1
</s>
<s>
11	#num#	k4
349	#num#	k4
</s>
<s>
1,66	1,66	k4
</s>
<s>
625	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
634	#num#	k4
00	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Brno-jih	Brno-jih	k1gMnSc1
</s>
<s>
9	#num#	k4
690	#num#	k4
</s>
<s>
12,77	12,77	k4
</s>
<s>
602	#num#	k4
00	#num#	k4
až	až	k9
619	#num#	k4
00	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
Brno-Slatina	Brno-Slatina	k1gFnSc1
</s>
<s>
9	#num#	k4
360	#num#	k4
</s>
<s>
5,83	5,83	k4
</s>
<s>
627	#num#	k4
00	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Brno-Černovice	Brno-Černovice	k1gFnSc1
</s>
<s>
8	#num#	k4
024	#num#	k4
</s>
<s>
6,29	6,29	k4
</s>
<s>
618	#num#	k4
00	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Brno-Komín	Brno-Komín	k1gMnSc1
</s>
<s>
7	#num#	k4
457	#num#	k4
</s>
<s>
7,60	7,60	k4
</s>
<s>
624	#num#	k4
00	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Brno-Medlánky	Brno-Medlánka	k1gFnPc1
</s>
<s>
5	#num#	k4
898	#num#	k4
</s>
<s>
3,51	3,51	k4
</s>
<s>
612	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
621	#num#	k4
00	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Brno-Tuřany	Brno-Tuřan	k1gMnPc4
</s>
<s>
5	#num#	k4
674	#num#	k4
</s>
<s>
17,84	17,84	k4
</s>
<s>
620	#num#	k4
00	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Brno-Maloměřice	Brno-Maloměřice	k1gFnPc1
a	a	k8xC
Obřany	Obřana	k1gFnPc1
</s>
<s>
5	#num#	k4
621	#num#	k4
</s>
<s>
9,36	9,36	k4
</s>
<s>
614	#num#	k4
00	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Brno-Jundrov	Brno-Jundrov	k1gInSc1
</s>
<s>
4	#num#	k4
132	#num#	k4
</s>
<s>
4,15	4,15	k4
</s>
<s>
637	#num#	k4
00	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Brno-Chrlice	Brno-Chrlice	k1gFnSc1
</s>
<s>
3	#num#	k4
722	#num#	k4
</s>
<s>
9,50	9,50	k4
</s>
<s>
643	#num#	k4
00	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Brno-Žebětín	Brno-Žebětín	k1gMnSc1
</s>
<s>
3	#num#	k4
577	#num#	k4
</s>
<s>
13,60	13,60	k4
</s>
<s>
641	#num#	k4
00	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Brno-Bosonohy	Brno-Bosonoh	k1gInPc1
</s>
<s>
2	#num#	k4
457	#num#	k4
</s>
<s>
7,15	7,15	k4
</s>
<s>
642	#num#	k4
00	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Brno-Ivanovice	Brno-Ivanovice	k1gFnSc1
</s>
<s>
1	#num#	k4
746	#num#	k4
</s>
<s>
2,45	2,45	k4
</s>
<s>
621	#num#	k4
00	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Brno-Jehnice	Brno-Jehnice	k1gFnSc1
</s>
<s>
1	#num#	k4
102	#num#	k4
</s>
<s>
4,07	4,07	k4
</s>
<s>
621	#num#	k4
00	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Brno-Kníničky	Brno-Knínička	k1gFnPc1
</s>
<s>
1	#num#	k4
006	#num#	k4
</s>
<s>
10,92	10,92	k4
</s>
<s>
635	#num#	k4
00	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Brno-Útěchov	Brno-Útěchov	k1gInSc1
</s>
<s>
760	#num#	k4
</s>
<s>
1,18	1,18	k4
</s>
<s>
644	#num#	k4
00	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Brno-Ořešín	Brno-Ořešín	k1gMnSc1
</s>
<s>
577	#num#	k4
</s>
<s>
3,07	3,07	k4
</s>
<s>
621	#num#	k4
00	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Definitivní	definitivní	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
Sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
domů	dům	k1gInPc2
a	a	k8xC
bytů	byt	k1gInPc2
2011	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Členění	členění	k1gNnSc1
Brna	Brno	k1gNnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Brno-město	Brno-města	k1gFnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Územní	územní	k2eAgInSc1d1
plán	plán	k1gInSc1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
s	s	k7c7
vyznačením	vyznačení	k1gNnSc7
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
</s>
