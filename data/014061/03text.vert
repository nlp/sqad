<s>
Chrom	chrom	k1gInSc1
</s>
<s>
Chrom	chrom	k1gInSc1
</s>
<s>
[	[	kIx(
<g/>
Ar	ar	k1gInSc1
<g/>
]	]	kIx)
3	#num#	k4
<g/>
d	d	k?
<g/>
5	#num#	k4
4	#num#	k4
<g/>
s	s	k7c7
<g/>
1	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
Cr	cr	kA
</s>
<s>
24	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Chrom	chrom	k1gInSc1
<g/>
,	,	kIx,
Cr	cr	k0
<g/>
,	,	kIx,
24	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Chromium	Chromium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
100	#num#	k4
až	až	k6eAd1
200	#num#	k4
ppm	ppm	k?
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
</s>
<s>
0,00005	0,00005	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Světle	světle	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
lesklý	lesklý	k2eAgInSc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
tvrdý	tvrdý	k2eAgMnSc1d1
a	a	k8xC
zároveň	zároveň	k6eAd1
křehký	křehký	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-47-3	7440-47-3	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
51,996	51,996	k4
<g/>
1	#num#	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
140	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
118	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
63	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Ar	ar	k1gInSc1
<g/>
]	]	kIx)
3	#num#	k4
<g/>
d	d	k?
<g/>
5	#num#	k4
4	#num#	k4
<g/>
s	s	k7c7
<g/>
1	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
−	−	k?
<g/>
II	II	kA
<g/>
,	,	kIx,
-I	-I	k?
<g/>
,	,	kIx,
I	I	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
V	V	kA
<g/>
,	,	kIx,
VI	VI	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,66	1,66	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
652,9	652,9	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
1590,6	1590,6	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
2987	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Krychlová	krychlový	k2eAgFnSc1d1
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
7,23	7,23	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
7,15	7,15	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
8,5	8,5	k4
</s>
<s>
Tlak	tlak	k1gInSc1
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
</s>
<s>
100	#num#	k4
Pa	Pa	kA
při	při	k7c6
1991K	1991K	k4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
5940	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
93,9	93,9	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1906,85	1906,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
180	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
2670,85	2670,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
944	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
339,5	339,5	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
21,0	21,0	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
449	#num#	k4
Jkg	Jkg	k1gFnSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
7,74	7,74	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
125	#num#	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
−	−	k?
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Antiferomagnetický	Antiferomagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
R	R	kA
<g/>
11	#num#	k4
<g/>
,	,	kIx,
R40	R40	k1gFnSc1
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
S	s	k7c7
<g/>
7	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
33	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
,	,	kIx,
S60	S60	k1gFnSc1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
50	#num#	k4
<g/>
Cr	cr	k0
</s>
<s>
4,345	4,345	k4
<g/>
%	%	kIx~
</s>
<s>
1,3	1,3	k4
<g/>
×	×	k?
<g/>
1018	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2	#num#	k4
×	×	k?
ε	ε	k?
</s>
<s>
-	-	kIx~
</s>
<s>
50	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
</s>
<s>
51	#num#	k4
<g/>
Cr	cr	k0
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
27,704	27,704	k4
dne	den	k1gInSc2
</s>
<s>
ε	ε	k?
</s>
<s>
0,752	0,752	k4
4	#num#	k4
</s>
<s>
50V	50V	k4
</s>
<s>
γ	γ	k?
</s>
<s>
0,320	0,320	k4
</s>
<s>
50V	50V	k4
</s>
<s>
52	#num#	k4
<g/>
Cr	cr	k0
</s>
<s>
83,789	83,789	k4
<g/>
%	%	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
28	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
53	#num#	k4
<g/>
Cr	cr	k0
</s>
<s>
9,501	9,501	k4
<g/>
%	%	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
29	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
54	#num#	k4
<g/>
Cr	cr	k0
</s>
<s>
2,365	2,365	k4
<g/>
%	%	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
30	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vanad	vanad	k1gInSc1
≺	≺	k?
<g/>
Cr	cr	k0
<g/>
≻	≻	k?
Mangan	mangan	k1gInSc1
</s>
<s>
⋎	⋎	k?
<g/>
Mo	Mo	k1gFnSc1
</s>
<s>
Chrom	chrom	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
chróm	chróm	k1gInSc1
<g/>
;	;	kIx,
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Cr	cr	k0
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Chromium	Chromium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
šedý	šedý	k2eAgInSc1d1
<g/>
,	,	kIx,
lesklý	lesklý	k2eAgInSc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
tvrdý	tvrdý	k2eAgMnSc1d1
a	a	k8xC
zároveň	zároveň	k6eAd1
křehký	křehký	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
metalurgii	metalurgie	k1gFnSc6
při	při	k7c6
výrobě	výroba	k1gFnSc6
legovaných	legovaný	k2eAgFnPc2d1
ocelí	ocel	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
slitin	slitina	k1gFnPc2
<g/>
,	,	kIx,
tenká	tenký	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
chromu	chrom	k1gInSc2
chrání	chránit	k5eAaImIp3nS
povrch	povrch	k1gInSc4
kovových	kovový	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
před	před	k7c7
korozí	koroze	k1gFnSc7
a	a	k8xC
zvyšuje	zvyšovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
tvrdost	tvrdost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
χ	χ	k?
/	/	kIx~
chrō	chrō	k1gFnSc1
„	„	k?
<g/>
barva	barva	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
neboť	neboť	k8xC
mnohé	mnohý	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
zbarvené	zbarvený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
českého	český	k2eAgNnSc2d1
obrození	obrození	k1gNnSc2
razili	razit	k5eAaImAgMnP
puristé	purista	k1gMnPc1
pro	pro	k7c4
chróm	chróm	k1gInSc4
pojmenování	pojmenování	k1gNnSc2
barvík	barvík	k1gInSc1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
nakonec	nakonec	k6eAd1
neujalo	ujmout	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Čístý	Čístý	k2eAgInSc1d1
chrom	chrom	k1gInSc1
</s>
<s>
Chrom	chrom	k1gInSc1
je	být	k5eAaImIp3nS
nejtvrdším	tvrdý	k2eAgInSc7d3
elementárním	elementární	k2eAgInSc7d1
kovem	kov	k1gInSc7
a	a	k8xC
vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
mimořádně	mimořádně	k6eAd1
nízkou	nízký	k2eAgFnSc7d1
reaktivitou	reaktivita	k1gFnSc7
a	a	k8xC
vysokou	vysoký	k2eAgFnSc7d1
chemickou	chemický	k2eAgFnSc7d1
odolností	odolnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
objeven	objeven	k2eAgInSc1d1
roku	rok	k1gInSc2
1797	#num#	k4
Louisem	Louis	k1gMnSc7
Nicolasem	Nicolas	k1gMnSc7
Vauquelinem	Vauquelin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
přechodné	přechodný	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
mají	mít	k5eAaImIp3nP
valenční	valenční	k2eAgInPc1d1
elektrony	elektron	k1gInPc1
v	v	k7c6
d-sféře	d-sféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Cr	cr	k0
<g/>
3	#num#	k4
<g/>
+	+	kIx~
a	a	k8xC
Cr	cr	k0
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
sloučeniny	sloučenina	k1gFnPc1
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
+	+	kIx~
jsou	být	k5eAaImIp3nP
silnými	silný	k2eAgInPc7d1
redukčními	redukční	k2eAgInPc7d1
činidly	činidlo	k1gNnPc7
a	a	k8xC
za	za	k7c2
normálních	normální	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
jsou	být	k5eAaImIp3nP
oxidovány	oxidován	k2eAgMnPc4d1
vzdušným	vzdušný	k2eAgInSc7d1
kyslíkem	kyslík	k1gInSc7
na	na	k7c4
trojmocné	trojmocný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
jeho	jeho	k3xOp3gFnPc1
sloučeniny	sloučenina	k1gFnPc1
mají	mít	k5eAaImIp3nP
oxidační	oxidační	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
Cr	cr	k0
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Přes	přes	k7c4
svoji	svůj	k3xOyFgFnSc4
značnou	značný	k2eAgFnSc4d1
chemickou	chemický	k2eAgFnSc4d1
stálost	stálost	k1gFnSc4
se	se	k3xPyFc4
chrom	chrom	k1gInSc1
pomalu	pomalu	k6eAd1
rozpouští	rozpouštět	k5eAaImIp3nS
v	v	k7c6
neoxidujících	oxidující	k2eNgFnPc6d1
kyselinách	kyselina	k1gFnPc6
(	(	kIx(
<g/>
kyselina	kyselina	k1gFnSc1
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
kyseliny	kyselina	k1gFnPc1
s	s	k7c7
oxidačním	oxidační	k2eAgNnSc7d1
působením	působení	k1gNnSc7
povrch	povrch	k7c2wR
kovu	kov	k1gInSc2
pasivují	pasivovat	k5eAaBmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chrom	chrom	k1gInSc1
se	se	k3xPyFc4
oxiduje	oxidovat	k5eAaBmIp3nS
při	při	k7c6
zahřívání	zahřívání	k1gNnSc6
v	v	k7c6
kyslíkovém	kyslíkový	k2eAgInSc6d1
plameni	plamen	k1gInSc6
nebo	nebo	k8xC
s	s	k7c7
oxidačními	oxidační	k2eAgNnPc7d1
činidly	činidlo	k1gNnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
dusičnany	dusičnan	k1gInPc4
nebo	nebo	k8xC
chlorečnany	chlorečnan	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímo	přímo	k6eAd1
se	se	k3xPyFc4
také	také	k9
slučuje	slučovat	k5eAaImIp3nS
s	s	k7c7
halogeny	halogen	k1gInPc7
<g/>
,	,	kIx,
se	s	k7c7
sírou	síra	k1gFnSc7
<g/>
,	,	kIx,
dusíkem	dusík	k1gInSc7
<g/>
,	,	kIx,
uhlíkem	uhlík	k1gInSc7
<g/>
,	,	kIx,
křemíkem	křemík	k1gInSc7
<g/>
,	,	kIx,
borem	bor	k1gInSc7
a	a	k8xC
některými	některý	k3yIgInPc7
kovy	kov	k1gInPc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
teprve	teprve	k6eAd1
za	za	k7c2
žáru	žár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
výroba	výroba	k1gFnSc1
</s>
<s>
Rubín	rubín	k1gInSc1
zabarvený	zabarvený	k2eAgInSc1d1
červeně	červeně	k6eAd1
malým	malý	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
chromu	chrom	k1gInSc2
</s>
<s>
Chrom	chrom	k1gInSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
prvky	prvek	k1gInPc4
s	s	k7c7
poměrně	poměrně	k6eAd1
značným	značný	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
na	na	k7c6
Zemi	zem	k1gFnSc6
i	i	k8xC
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
činí	činit	k5eAaImIp3nS
průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
chromu	chromat	k5eAaImIp1nS
kolem	kolem	k7c2
0,1	0,1	k4
<g/>
–	–	k?
<g/>
0,2	0,2	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
koncentrace	koncentrace	k1gFnSc1
pohybuje	pohybovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
0,05	0,05	k4
mikrogramů	mikrogram	k1gInPc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
litru	litr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadají	připadat	k5eAaImIp3nP,k5eAaPmIp3nP
na	na	k7c4
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
chromu	chromat	k5eAaImIp1nS
přibližně	přibližně	k6eAd1
3	#num#	k4
miliony	milion	k4xCgInPc4
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
chrom	chrom	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
často	často	k6eAd1
současně	současně	k6eAd1
s	s	k7c7
rudami	ruda	k1gFnPc7
železa	železo	k1gNnSc2
například	například	k6eAd1
jako	jako	k9
ruda	ruda	k1gFnSc1
chromit	chromit	k5eAaImF
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
podvojný	podvojný	k2eAgInSc4d1
oxid	oxid	k1gInSc4
železnato-chromitý	železnato-chromitý	k2eAgInSc4d1
FeO	FeO	k1gFnSc7
.	.	kIx.
</s>
<s desamb="1">
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
důležitým	důležitý	k2eAgInSc7d1
minerálem	minerál	k1gInSc7
chromu	chrom	k1gInSc2
je	být	k5eAaImIp3nS
krokoit	krokoit	k5eAaImF,k5eAaBmF,k5eAaPmF
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
chroman	chroman	k1gInSc4
olovnatý	olovnatý	k2eAgInSc4d1
PbCrO	PbCrO	k1gFnSc7
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	Malá	k1gFnSc1
množství	množství	k1gNnSc2
chromu	chrom	k1gInSc2
přispívají	přispívat	k5eAaImIp3nP
k	k	k7c3
zabarvení	zabarvení	k1gNnSc3
drahokamů	drahokam	k1gInPc2
smaragdu	smaragd	k1gInSc2
a	a	k8xC
rubínu	rubín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3
světové	světový	k2eAgFnPc1d1
zásoby	zásoba	k1gFnPc1
chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vyrábí	vyrábět	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
polovinu	polovina	k1gFnSc4
veškeré	veškerý	k3xTgFnSc2
světové	světový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
tohoto	tento	k3xDgInSc2
kovu	kov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
význačnými	význačný	k2eAgMnPc7d1
producenty	producent	k1gMnPc7
chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
Kazachstán	Kazachstán	k1gInSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
a	a	k8xC
Turecko	Turecko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
postupem	postup	k1gInSc7
metalurgického	metalurgický	k2eAgNnSc2d1
získávání	získávání	k1gNnSc2
chromu	chromat	k5eAaImIp1nS
je	on	k3xPp3gFnPc4
redukce	redukce	k1gFnPc4
chromitu	chromit	k1gInSc2
uhlíkem	uhlík	k1gInSc7
(	(	kIx(
<g/>
koksem	koks	k1gInSc7
<g/>
)	)	kIx)
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
peci	pec	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
FeCr	FeCr	k1gInSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
4	#num#	k4
+	+	kIx~
4	#num#	k4
C	C	kA
→	→	k?
Fe	Fe	k1gMnSc1
+	+	kIx~
2	#num#	k4
Cr	cr	k0
+	+	kIx~
4	#num#	k4
CO	co	k8xS
</s>
<s>
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
slitina	slitina	k1gFnSc1
chromu	chrom	k1gInSc2
se	s	k7c7
železem	železo	k1gNnSc7
–	–	k?
ferrochrom	ferrochrom	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
lze	lze	k6eAd1
dále	daleko	k6eAd2
přímo	přímo	k6eAd1
používat	používat	k5eAaImF
při	při	k7c6
legování	legování	k1gNnSc6
speciálních	speciální	k2eAgFnPc2d1
ocelí	ocel	k1gFnPc2
a	a	k8xC
slitin	slitina	k1gFnPc2
s	s	k7c7
obsahem	obsah	k1gInSc7
Fe	Fe	k1gFnSc2
a	a	k8xC
Cr	cr	k0
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
čistého	čistý	k2eAgInSc2d1
chromu	chrom	k1gInSc2
je	být	k5eAaImIp3nS
poněkud	poněkud	k6eAd1
komplikovanější	komplikovaný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
je	být	k5eAaImIp3nS
z	z	k7c2
chromové	chromový	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
působením	působení	k1gNnSc7
roztaveného	roztavený	k2eAgInSc2d1
hydroxidu	hydroxid	k1gInSc2
sodného	sodný	k2eAgInSc2d1
(	(	kIx(
<g/>
NaOH	NaOH	k1gFnSc1
<g/>
)	)	kIx)
připraven	připraven	k2eAgMnSc1d1
dichroman	dichroman	k1gMnSc1
sodný	sodný	k2eAgInSc4d1
Na	na	k7c4
<g/>
2	#num#	k4
<g/>
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
uhlíkem	uhlík	k1gInSc7
redukován	redukovat	k5eAaBmNgMnS
za	za	k7c2
vzniku	vznik	k1gInSc2
oxidu	oxid	k1gInSc2
chromitého	chromitý	k2eAgInSc2d1
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
krokem	krok	k1gInSc7
je	být	k5eAaImIp3nS
redukce	redukce	k1gFnSc1
oxidu	oxid	k1gInSc2
hliníkem	hliník	k1gInSc7
nebo	nebo	k8xC
křemíkem	křemík	k1gInSc7
za	za	k7c2
vzniku	vznik	k1gInSc2
elementárního	elementární	k2eAgInSc2d1
chromu	chrom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
+	+	kIx~
2	#num#	k4
Al	ala	k1gFnPc2
→	→	k?
2	#num#	k4
Cr	cr	k0
+	+	kIx~
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Socha	socha	k1gFnSc1
Počátek	počátek	k1gInSc1
z	z	k7c2
nerezové	rezový	k2eNgFnSc2d1
oceli	ocel	k1gFnSc2
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Největší	veliký	k2eAgInSc1d3
podíl	podíl	k1gInSc1
světové	světový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
chromu	chrom	k1gInSc2
najde	najít	k5eAaPmIp3nS
jednoznačně	jednoznačně	k6eAd1
využití	využití	k1gNnSc1
v	v	k7c6
metalurgickém	metalurgický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
především	především	k6eAd1
při	při	k7c6
výrobě	výroba	k1gFnSc6
legovaných	legovaný	k2eAgFnPc2d1
ocelí	ocel	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
chromu	chrom	k1gInSc2
ve	v	k7c6
slitině	slitina	k1gFnSc6
určuje	určovat	k5eAaImIp3nS
především	především	k9
její	její	k3xOp3gFnSc4
tvrdost	tvrdost	k1gFnSc4
a	a	k8xC
mechanickou	mechanický	k2eAgFnSc4d1
odolnost	odolnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
obsahu	obsah	k1gInSc2
cca	cca	kA
12	#num#	k4
%	%	kIx~
Cr	cr	k0
rozpuštěného	rozpuštěný	k2eAgNnSc2d1
v	v	k7c6
tuhém	tuhý	k2eAgInSc6d1
roztoku	roztok	k1gInSc6
je	být	k5eAaImIp3nS
ocel	ocel	k1gFnSc1
korozivzdorná	korozivzdorný	k2eAgFnSc1d1
(	(	kIx(
<g/>
pasivace	pasivace	k1gFnSc1
povrchu	povrch	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cr	cr	k0
zlepšuje	zlepšovat	k5eAaImIp3nS
také	také	k9
její	její	k3xOp3gFnSc4
žáruvzdornost	žáruvzdornost	k1gFnSc4
a	a	k8xC
žárupevnost	žárupevnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
nástrojových	nástrojový	k2eAgFnPc2d1
ocelí	ocel	k1gFnPc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
legura	legura	k1gFnSc1
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
prokalitelnosti	prokalitelnost	k1gFnSc2
a	a	k8xC
tvrdosti	tvrdost	k1gFnSc2
(	(	kIx(
<g/>
tvorba	tvorba	k1gFnSc1
speciálních	speciální	k2eAgInPc2d1
karbidů	karbid	k1gInPc2
chromu	chrom	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgInPc1d1
druhy	druh	k1gInPc1
ocelí	ocelit	k5eAaImIp3nP
s	s	k7c7
nižším	nízký	k2eAgNnSc7d2
zastoupením	zastoupení	k1gNnSc7
chromu	chrom	k1gInSc2
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
výrobě	výroba	k1gFnSc3
geologických	geologický	k2eAgInPc2d1
vrtných	vrtný	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
vysoce	vysoce	k6eAd1
výkonných	výkonný	k2eAgInPc2d1
nožů	nůž	k1gInPc2
pro	pro	k7c4
stříhání	stříhání	k1gNnSc4
kovů	kov	k1gInPc2
<g/>
,	,	kIx,
frézovacích	frézovací	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
pro	pro	k7c4
opracování	opracování	k1gNnSc4
dřeva	dřevo	k1gNnSc2
a	a	k8xC
v	v	k7c6
řadě	řada	k1gFnSc6
podobných	podobný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnost	možnost	k1gFnSc1
kalitelnosti	kalitelnost	k1gFnSc2
a	a	k8xC
korozivzdornosti	korozivzdornost	k1gFnSc2
ocelí	ocel	k1gFnPc2
legovaných	legovaný	k2eAgFnPc2d1
Cr	cr	k0
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
u	u	k7c2
chirurgických	chirurgický	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
potravinářském	potravinářský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
vodních	vodní	k2eAgInPc6d1
strojích	stroj	k1gInPc6
(	(	kIx(
<g/>
odlitky	odlitek	k1gInPc1
vodních	vodní	k2eAgFnPc2d1
turbín	turbína	k1gFnPc2
<g/>
)	)	kIx)
atd.	atd.	kA
Chrom	chrom	k1gInSc1
se	se	k3xPyFc4
také	také	k9
přidává	přidávat	k5eAaImIp3nS
do	do	k7c2
mosazi	mosaz	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
zvětšila	zvětšit	k5eAaPmAgFnS
její	její	k3xOp3gFnSc4
tvrdost	tvrdost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
životě	život	k1gInSc6
se	se	k3xPyFc4
s	s	k7c7
chromem	chrom	k1gInSc7
setkáme	setkat	k5eAaPmIp1nP
spíše	spíše	k9
jako	jako	k9
s	s	k7c7
materiálem	materiál	k1gInSc7
<g/>
,	,	kIx,
chránícím	chránící	k2eAgMnSc7d1
kovové	kovový	k2eAgInPc4d1
povrchy	povrch	k1gInPc4
před	před	k7c7
korozí	koroze	k1gFnSc7
za	za	k7c4
současné	současný	k2eAgNnSc4d1
zvýšení	zvýšení	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
estetického	estetický	k2eAgInSc2d1
vzhledu	vzhled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasickým	klasický	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
chromování	chromování	k1gNnSc1
chirurgických	chirurgický	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
i	i	k8xC
jiných	jiný	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
používaných	používaný	k2eAgNnPc2d1
v	v	k7c6
medicíně	medicína	k1gFnSc6
(	(	kIx(
<g/>
sterilizátory	sterilizátor	k1gInPc1
<g/>
,	,	kIx,
zubařské	zubařský	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
a	a	k8xC
podobné	podobný	k2eAgInPc1d1
předměty	předmět	k1gInPc1
sloužící	sloužící	k2eAgInPc1d1
k	k	k7c3
vyšetření	vyšetření	k1gNnSc3
pacienta	pacient	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
civilním	civilní	k2eAgInSc6d1
životě	život	k1gInSc6
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
chromované	chromovaný	k2eAgInPc4d1
předměty	předmět	k1gInPc4
často	často	k6eAd1
ve	v	k7c4
vybavení	vybavení	k1gNnSc4
koupelen	koupelna	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
luxusních	luxusní	k2eAgInPc2d1
automobilových	automobilový	k2eAgInPc2d1
doplňků	doplněk	k1gInPc2
a	a	k8xC
v	v	k7c6
řadě	řada	k1gFnSc6
dalších	další	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Oxid	oxid	k1gInSc1
chromitý	chromitý	k2eAgInSc1d1
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
chrom	chrom	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
Cr	cr	k0
<g/>
3	#num#	k4
<g/>
+	+	kIx~
a	a	k8xC
Cr	cr	k0
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
se	se	k3xPyFc4
setkáme	setkat	k5eAaPmIp1nP
i	i	k9
se	s	k7c7
sloučeninami	sloučenina	k1gFnPc7
Cr	cr	k0
<g/>
4	#num#	k4
<g/>
+	+	kIx~
a	a	k8xC
Cr	cr	k0
<g/>
5	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
dvojmocného	dvojmocný	k2eAgInSc2d1
chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
silná	silný	k2eAgNnPc1d1
redukční	redukční	k2eAgNnPc1d1
činidla	činidlo	k1gNnPc1
<g/>
,	,	kIx,
působením	působení	k1gNnSc7
vzdušného	vzdušný	k2eAgInSc2d1
kyslíku	kyslík	k1gInSc2
se	se	k3xPyFc4
samovolně	samovolně	k6eAd1
oxidují	oxidovat	k5eAaBmIp3nP
za	za	k7c2
vzniku	vznik	k1gInSc2
Cr	cr	k0
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prakticky	prakticky	k6eAd1
se	se	k3xPyFc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
v	v	k7c6
analytické	analytický	k2eAgFnSc6d1
chemii	chemie	k1gFnSc6
při	při	k7c6
reduktometrických	reduktometrický	k2eAgFnPc6d1
titracích	titrace	k1gFnPc6
jako	jako	k9
jedny	jeden	k4xCgInPc4
z	z	k7c2
nejsilnějších	silný	k2eAgNnPc2d3
redukčních	redukční	k2eAgNnPc2d1
činidel	činidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
přitom	přitom	k6eAd1
připravují	připravovat	k5eAaImIp3nP
až	až	k9
v	v	k7c6
roztoku	roztok	k1gInSc6
redukcí	redukce	k1gFnPc2
chromitých	chromitý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
v	v	k7c6
kyselém	kyselý	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
zinkovým	zinkový	k2eAgInSc7d1
amalgámem	amalgám	k1gInSc7
<g/>
,	,	kIx,
nad	nad	k7c7
nímž	jenž	k3xRgMnSc7
jsou	být	k5eAaImIp3nP
také	také	k9
dlouhodobě	dlouhodobě	k6eAd1
uchovávány	uchovávat	k5eAaImNgInP
bez	bez	k7c2
přístupu	přístup	k1gInSc2
vzduchu	vzduch	k1gInSc2
nad	nad	k7c7
zinkovým	zinkový	k2eAgInSc7d1
amalgámem	amalgám	k1gInSc7
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožní	umožnit	k5eAaPmIp3nS
uchovat	uchovat	k5eAaPmF
kyselý	kyselý	k2eAgInSc4d1
roztok	roztok	k1gInSc4
chromnaté	chromnatý	k2eAgFnSc2d1
soli	sůl	k1gFnSc2
i	i	k9
po	po	k7c4
dobu	doba	k1gFnSc4
několika	několik	k4yIc2
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnější	významný	k2eAgFnSc1d2
a	a	k8xC
stálejší	stálý	k2eAgFnPc1d2
chromnaté	chromnatý	k2eAgFnPc1d1
soli	sůl	k1gFnPc1
jsou	být	k5eAaImIp3nP
chlorid	chlorid	k1gInSc4
chromnatý	chromnatý	k2eAgInSc4d1
CrCl	CrCl	k1gInSc4
<g/>
2	#num#	k4
lépe	dobře	k6eAd2
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
Cl	Cl	k1gFnSc1
<g/>
4	#num#	k4
a	a	k8xC
síran	síran	k1gInSc4
chromnatý	chromnatý	k2eAgInSc4d1
CrSO	CrSO	k1gFnSc7
<g/>
4	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gFnPc4
podvojné	podvojný	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
trojmocného	trojmocný	k2eAgInSc2d1
chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
neomezeně	omezeně	k6eNd1
stálé	stálý	k2eAgFnPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
zelenou	zelený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soli	sůl	k1gFnPc4
trojmocného	trojmocný	k2eAgInSc2d1
chromu	chrom	k1gInSc2
slouží	sloužit	k5eAaImIp3nS
také	také	k9
ve	v	k7c6
sklářském	sklářský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
k	k	k7c3
barvení	barvení	k1gNnSc3
skla	sklo	k1gNnSc2
a	a	k8xC
kožedělném	kožedělný	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
při	při	k7c6
činění	činění	k1gNnSc6
kůží	kůže	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
chromitý	chromitý	k2eAgInSc1d1
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
barevný	barevný	k2eAgInSc4d1
pigment	pigment	k1gInSc4
pod	pod	k7c7
označením	označení	k1gNnSc7
chromová	chromový	k2eAgFnSc1d1
zeleň	zeleň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxid	oxid	k1gInSc1
chromitý	chromitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
inertní	inertní	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
nerozpouští	rozpouštět	k5eNaImIp3nS
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
kyselinách	kyselina	k1gFnPc6
ani	ani	k8xC
v	v	k7c6
zásadách	zásada	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
laboratoři	laboratoř	k1gFnSc6
se	se	k3xPyFc4
velmi	velmi	k6eAd1
často	často	k6eAd1
připravuje	připravovat	k5eAaImIp3nS
efektní	efektní	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
<g/>
,	,	kIx,
tepelným	tepelný	k2eAgInSc7d1
rozkladem	rozklad	k1gInSc7
dichromanu	dichroman	k1gMnSc3
amonného	amonný	k2eAgInSc2d1
<g/>
,	,	kIx,
známou	známý	k2eAgFnSc4d1
jako	jako	k9
sopka	sopka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
NH	NH	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
→	→	k?
N2	N2	k1gMnSc1
+	+	kIx~
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
+	+	kIx~
4	#num#	k4
H2O	H2O	k1gFnPc2
</s>
<s>
Hydroxid	hydroxid	k1gInSc1
chromitý	chromitý	k2eAgInSc1d1
Cr	cr	k0
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
šedozelená	šedozelený	k2eAgFnSc1d1
sraženina	sraženina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vzniká	vznikat	k5eAaImIp3nS
reakcí	reakce	k1gFnSc7
chromitých	chromitý	k2eAgInPc2d1
kationů	kation	k1gInPc2
s	s	k7c7
hydroxidovými	hydroxidový	k2eAgInPc7d1
aniony	anion	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kyselém	kyselý	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
na	na	k7c4
chromité	chromitý	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Cr	cr	k0
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
+	+	kIx~
3	#num#	k4
HCl	HCl	k1gFnSc2
→	→	k?
CrCl	CrCl	k1gInSc1
<g/>
3	#num#	k4
+	+	kIx~
3	#num#	k4
H2O	H2O	k1gFnPc2
</s>
<s>
V	v	k7c6
zásaditém	zásaditý	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
na	na	k7c4
chromitany	chromitan	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Cr	cr	k0
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
OH	OH	kA
<g/>
−	−	k?
→	→	k?
[	[	kIx(
<g/>
Cr	cr	k0
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
−	−	k?
</s>
<s>
Z	z	k7c2
chromitanů	chromitan	k1gInPc2
se	se	k3xPyFc4
reakcí	reakce	k1gFnPc2
s	s	k7c7
oxidačními	oxidační	k2eAgNnPc7d1
činidly	činidlo	k1gNnPc7
připravují	připravovat	k5eAaImIp3nP
chromany	chroman	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
Cr	cr	k0
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
−	−	k?
<g/>
+	+	kIx~
2	#num#	k4
H2O2	H2O2	k1gMnSc1
→	→	k?
CrO	CrO	k1gMnSc1
<g/>
42	#num#	k4
<g/>
−	−	k?
+	+	kIx~
4	#num#	k4
H2O	H2O	k1gFnPc2
</s>
<s>
Chlorid	chlorid	k1gInSc1
chromitý	chromitý	k2eAgInSc4d1
CrCl	CrCl	k1gInSc4
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
bezvodém	bezvodý	k2eAgInSc6d1
stavu	stav	k1gInSc6
červenofialová	červenofialový	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vodného	vodný	k2eAgInSc2d1
roztoku	roztok	k1gInSc2
lze	lze	k6eAd1
získat	získat	k5eAaPmF
chlorid	chlorid	k1gInSc4
chromitý	chromitý	k2eAgInSc4d1
jako	jako	k9
hexahydrát	hexahydrát	k1gInSc4
CrCl	CrCl	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
smaragdově	smaragdově	k6eAd1
zelenou	zelený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlorid	chlorid	k1gInSc1
chromitý	chromitý	k2eAgInSc1d1
tvoří	tvořit	k5eAaImIp3nS
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
chloridy	chlorid	k1gInPc7
komplexy	komplex	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
mají	mít	k5eAaImIp3nP
nejčastěji	často	k6eAd3
složení	složení	k1gNnSc2
M	M	kA
<g/>
2	#num#	k4
<g/>
CrCl	CrCl	k1gInSc1
<g/>
5	#num#	k4
pentachlorochromitanový	pentachlorochromitanový	k2eAgInSc1d1
anion	anion	k1gInSc1
<g/>
(	(	kIx(
<g/>
M	M	kA
-	-	kIx~
alkalický	alkalický	k2eAgInSc1d1
kov	kov	k1gInSc1
nebo	nebo	k8xC
amonný	amonný	k2eAgInSc1d1
kation	kation	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
látky	látka	k1gFnPc1
mají	mít	k5eAaImIp3nP
nejčastěji	často	k6eAd3
červenou	červený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
stálé	stálý	k2eAgMnPc4d1
pouze	pouze	k6eAd1
v	v	k7c6
koncentrovaných	koncentrovaný	k2eAgInPc6d1
roztocích	roztok	k1gInPc6
v	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
chlorovodíku	chlorovodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
zředěných	zředěný	k2eAgInPc6d1
roztocích	roztok	k1gInPc6
se	se	k3xPyFc4
komplex	komplex	k1gInSc1
rozpadá	rozpadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
původní	původní	k2eAgInPc4d1
chloridy	chlorid	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Síran	síran	k1gInSc1
chromitý	chromitý	k2eAgInSc1d1
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
bezvodém	bezvodý	k2eAgInSc6d1
stavu	stav	k1gInSc6
bílý	bílý	k2eAgInSc1d1
prášek	prášek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vodného	vodný	k2eAgInSc2d1
roztoku	roztok	k1gInSc2
lze	lze	k6eAd1
získat	získat	k5eAaPmF
síran	síran	k1gInSc4
chromitý	chromitý	k2eAgInSc4d1
jako	jako	k8xC,k8xS
oktadekahydrát	oktadekahydrát	k1gInSc4
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
<g/>
.	.	kIx.
18	#num#	k4
H	H	kA
<g/>
2	#num#	k4
<g/>
O.	O.	kA
Při	při	k7c6
zahřívání	zahřívání	k1gNnSc6
se	se	k3xPyFc4
z	z	k7c2
krystalů	krystal	k1gInPc2
síranu	síran	k1gInSc2
chromitého	chromitý	k2eAgInSc2d1
odštěpuje	odštěpovat	k5eAaImIp3nS
voda	voda	k1gFnSc1
a	a	k8xC
vznikají	vznikat	k5eAaImIp3nP
zeleně	zeleně	k6eAd1
zbarvené	zbarvený	k2eAgInPc1d1
sírany	síran	k1gInPc1
chromité	chromitý	k2eAgInPc1d1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
mají	mít	k5eAaImIp3nP
proměnné	proměnný	k2eAgNnSc4d1
složení	složení	k1gNnSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
méně	málo	k6eAd2
rozpustné	rozpustný	k2eAgFnPc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kamenec	Kamenec	k1gInSc1
chromitý	chromitý	k2eAgInSc1d1
KCr	KCr	k1gMnSc7
<g/>
(	(	kIx(
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2.12	2.12	k4
H2O	H2O	k1gFnPc2
neboli	neboli	k8xC
dodekahydrát	dodekahydrát	k1gInSc1
síranu	síran	k1gInSc2
draselnochromitého	draselnochromitý	k2eAgInSc2d1
je	být	k5eAaImIp3nS
tmavěfialová	tmavěfialový	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
barvířství	barvířství	k1gNnSc6
a	a	k8xC
koželužství	koželužství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
čtyřmocného	čtyřmocný	k2eAgInSc2d1
chromu	chrom	k1gInSc2
</s>
<s>
Oxid	oxid	k1gInSc4
chromičitý	chromičitý	k2eAgInSc4d1
CrO	CrO	k1gFnSc7
<g/>
2	#num#	k4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
záznamový	záznamový	k2eAgInSc1d1
materiál	materiál	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
má	mít	k5eAaImIp3nS
feromagnetické	feromagnetický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Krystalický	krystalický	k2eAgMnSc1d1
dichroman	dichroman	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
šestimocného	šestimocný	k2eAgInSc2d1
chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
středně	středně	k6eAd1
silnými	silný	k2eAgInPc7d1
oxidačními	oxidační	k2eAgNnPc7d1
činidly	činidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prakticky	prakticky	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gNnPc7
setkáme	setkat	k5eAaPmIp1nP
jako	jako	k9
se	s	k7c7
solemi	sůl	k1gFnPc7
kyseliny	kyselina	k1gFnSc2
chromové	chromový	k2eAgFnPc4d1
<g/>
,	,	kIx,
chromany	chroman	k1gInPc4
<g/>
(	(	kIx(
<g/>
CrO	CrO	k1gFnPc2
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
−	−	k?
nebo	nebo	k8xC
kyseliny	kyselina	k1gFnSc2
dichromové	dichromový	k2eAgFnSc2d1
<g/>
,	,	kIx,
dichromany	dichroman	k1gMnPc7
(	(	kIx(
<g/>
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
−	−	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chromany	chroman	k1gInPc7
a	a	k8xC
dichromany	dichroman	k1gMnPc7
v	v	k7c6
roztocích	roztok	k1gInPc6
mohou	moct	k5eAaImIp3nP
navzájem	navzájem	k6eAd1
přecházet	přecházet	k5eAaImF
mezi	mezi	k7c7
sebou	se	k3xPyFc7
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c4
pH	ph	kA
prostředí	prostředí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
chromany	chroman	k1gInPc1
jsou	být	k5eAaImIp3nP
stálé	stálý	k2eAgMnPc4d1
v	v	k7c6
alkalickém	alkalický	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
a	a	k8xC
mají	mít	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
žlutou	žlutý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dichromany	Dichroman	k1gMnPc4
jsou	být	k5eAaImIp3nP
oranžové	oranžový	k2eAgFnPc1d1
a	a	k8xC
jsou	být	k5eAaImIp3nP
stabilní	stabilní	k2eAgInPc1d1
v	v	k7c6
kyselém	kyselé	k1gNnSc6
pH	ph	kA
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
CrO	CrO	k1gFnSc1
<g/>
42	#num#	k4
<g/>
−	−	k?
<g/>
+	+	kIx~
<g/>
2	#num#	k4
H	H	kA
<g/>
+	+	kIx~
→	→	k?
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
72	#num#	k4
<g/>
−	−	k?
<g/>
+	+	kIx~
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c6
</s>
<s>
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
72	#num#	k4
<g/>
−	−	k?
<g/>
+	+	kIx~
<g/>
2	#num#	k4
OH	OH	kA
<g/>
−	−	k?
→	→	k?
2	#num#	k4
CrO	CrO	k1gFnSc1
<g/>
42	#num#	k4
<g/>
−	−	k?
<g/>
+	+	kIx~
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c6
</s>
<s>
Oxid	oxid	k1gInSc1
chromový	chromový	k2eAgInSc1d1
CrO	CrO	k1gFnSc2
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
tmavěčervená	tmavěčervený	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
silně	silně	k6eAd1
hygroskopická	hygroskopický	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vzniká	vznikat	k5eAaImIp3nS
reakcí	reakce	k1gFnSc7
dichromanu	dichroman	k1gMnSc3
s	s	k7c7
koncentrovanou	koncentrovaný	k2eAgFnSc7d1
kyselinou	kyselina	k1gFnSc7
sírovou	sírový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
hořkokyselou	hořkokyselý	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
jedovatý	jedovatý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
poutání	poutání	k1gNnSc6
vzdušné	vzdušný	k2eAgFnSc2d1
vlhkosti	vlhkost	k1gFnSc2
se	se	k3xPyFc4
oxid	oxid	k1gInSc1
chromový	chromový	k2eAgInSc1d1
postupně	postupně	k6eAd1
mění	měnit	k5eAaImIp3nS
v	v	k7c6
zlatožlutou	zlatožlutý	k2eAgFnSc7d1
kyselinou	kyselina	k1gFnSc7
chromovou	chromový	k2eAgFnSc7d1
H	H	kA
<g/>
2	#num#	k4
<g/>
CrO	CrO	k1gFnSc1
<g/>
4	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
známa	znám	k2eAgFnSc1d1
pouze	pouze	k6eAd1
v	v	k7c6
roztoku	roztok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Dichroman	Dichroman	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
K	k	k7c3
<g/>
2	#num#	k4
<g/>
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
analytické	analytický	k2eAgFnSc6d1
chemii	chemie	k1gFnSc6
jako	jako	k9
primární	primární	k2eAgInSc1d1
oxidimetrický	oxidimetrický	k2eAgInSc1d1
standard	standard	k1gInSc1
pro	pro	k7c4
titrace	titrace	k1gFnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
jej	on	k3xPp3gMnSc4
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
ve	v	k7c6
velmi	velmi	k6eAd1
vysoké	vysoký	k2eAgFnSc6d1
čistotě	čistota	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
neomezeně	omezeně	k6eNd1
stálý	stálý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dichroman	Dichroman	k1gMnSc1
draselný	draselný	k2eAgMnSc1d1
se	se	k3xPyFc4
často	často	k6eAd1
používá	používat	k5eAaImIp3nS
jako	jako	k8xS,k8xC
oxidační	oxidační	k2eAgNnSc1d1
činidlo	činidlo	k1gNnSc1
v	v	k7c6
reakcích	reakce	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Chroman	chroman	k1gInSc4
olovnatý	olovnatý	k2eAgInSc4d1
PbCrO	PbCrO	k1gFnSc7
<g/>
4	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
jako	jako	k9
nerost	nerost	k1gInSc4
krokoit	krokoit	k5eAaPmF,k5eAaBmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
žlutá	žlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ve	v	k7c6
vodě	voda	k1gFnSc6
nerozpustná	rozpustný	k2eNgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chroman	chroman	k1gInSc1
olovnatý	olovnatý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
rozpustný	rozpustný	k2eAgInSc1d1
v	v	k7c6
roztocích	roztok	k1gInPc6
hydroxidů	hydroxid	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
hydroxoolovnatanů	hydroxoolovnatan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
působení	působení	k1gNnSc6
malého	malý	k2eAgNnSc2d1
množství	množství	k1gNnSc2
hydroxidu	hydroxid	k1gInSc2
<g/>
,	,	kIx,
vzniká	vznikat	k5eAaImIp3nS
z	z	k7c2
chromanu	chroman	k1gInSc2
olovnatého	olovnatý	k2eAgMnSc2d1
zásaditý	zásaditý	k2eAgInSc4d1
chroman	chroman	k1gInSc4
olovnatý	olovnatý	k2eAgInSc4d1
PbCrO	PbCrO	k1gFnSc7
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pb	Pb	k1gFnSc1
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
červenou	červený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
a	a	k8xC
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
barva	barva	k1gFnSc1
chromová	chromový	k2eAgFnSc1d1
červeň	červeň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
výrobě	výroba	k1gFnSc6
barev	barva	k1gFnPc2
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc4d1
hlavně	hlavně	k9
chroman	chroman	k1gInSc4
barnatý	barnatý	k2eAgInSc4d1
BaCrO	BaCrO	k1gFnSc7
<g/>
4	#num#	k4
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
pod	pod	k7c7
označením	označení	k1gNnSc7
žlutý	žlutý	k2eAgInSc4d1
ultramarín	ultramarín	k1gInSc4
a	a	k8xC
chroman	chroman	k1gInSc4
olovnatý	olovnatý	k2eAgInSc4d1
PbCrO	PbCrO	k1gFnSc7
<g/>
4	#num#	k4
-	-	kIx~
chromová	chromový	k2eAgFnSc1d1
žluť	žluť	k1gFnSc1
a	a	k8xC
dnes	dnes	k6eAd1
už	už	k6eAd1
méně	málo	k6eAd2
známá	známý	k2eAgFnSc1d1
podvojná	podvojný	k2eAgFnSc1d1
sůl	sůl	k1gFnSc1
chromanu	chroman	k1gInSc2
zinečnatého	zinečnatý	k2eAgInSc2d1
a	a	k8xC
dichromanu	dichroman	k1gMnSc3
draselného	draselný	k2eAgInSc2d1
3	#num#	k4
ZnCrO	ZnCrO	k1gFnPc7
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
<g/>
2	#num#	k4
<g/>
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
známá	známý	k2eAgFnSc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
zinková	zinkový	k2eAgFnSc1d1
žluť	žluť	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Inhalace	inhalace	k1gFnSc1
sloučenin	sloučenina	k1gFnPc2
šestimocného	šestimocný	k2eAgInSc2d1
chromu	chrom	k1gInSc2
vede	vést	k5eAaImIp3nS
k	k	k7c3
poškození	poškození	k1gNnSc3
dýchacích	dýchací	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
perforace	perforace	k1gFnPc1
nosní	nosní	k2eAgFnSc2d1
přepážky	přepážka	k1gFnSc2
a	a	k8xC
bronchitidy	bronchitida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
dlouhodobé	dlouhodobý	k2eAgFnSc2d1
expozice	expozice	k1gFnSc2
na	na	k7c6
kůži	kůže	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
vzniku	vznik	k1gInSc3
dermatitidy	dermatitida	k1gFnSc2
<g/>
,	,	kIx,
ekzémů	ekzém	k1gInPc2
a	a	k8xC
tzv.	tzv.	kA
chromových	chromový	k2eAgInPc2d1
vředů	vřed	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chromové	chromový	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
vyvolávají	vyvolávat	k5eAaImIp3nP
také	také	k9
rakovinu	rakovina	k1gFnSc4
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biologický	biologický	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Biologické	biologický	k2eAgInPc1d1
účinky	účinek	k1gInPc1
chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
silně	silně	k6eAd1
závislé	závislý	k2eAgInPc1d1
na	na	k7c4
mocenství	mocenství	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
do	do	k7c2
organismu	organismus	k1gInSc2
dostává	dostávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trojmocný	trojmocný	k2eAgInSc1d1
chrom	chrom	k1gInSc1
je	být	k5eAaImIp3nS
pokládán	pokládat	k5eAaImNgInS
v	v	k7c6
malém	malý	k2eAgNnSc6d1
množství	množství	k1gNnSc6
za	za	k7c4
nezbytnou	nezbytný	k2eAgFnSc4d1,k2eNgFnSc4d1
součást	součást	k1gFnSc4
každodenní	každodenní	k2eAgFnSc2d1
potravy	potrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
šestimocný	šestimocný	k2eAgInSc1d1
chrom	chrom	k1gInSc1
působí	působit	k5eAaImIp3nS
negativně	negativně	k6eAd1
a	a	k8xC
Mezinárodní	mezinárodní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
rakoviny	rakovina	k1gFnSc2
ho	on	k3xPp3gMnSc4
klasifikovala	klasifikovat	k5eAaImAgFnS
jako	jako	k8xC,k8xS
lidský	lidský	k2eAgInSc1d1
karcinogen	karcinogen	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
těchto	tento	k3xDgInPc2
důvodů	důvod	k1gInPc2
je	být	k5eAaImIp3nS
při	při	k7c6
provádění	provádění	k1gNnSc6
zdravotních	zdravotní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
nutno	nutno	k6eAd1
důsledně	důsledně	k6eAd1
zkoumat	zkoumat	k5eAaImF
ne	ne	k9
pouze	pouze	k6eAd1
obsah	obsah	k1gInSc1
chromu	chromat	k5eAaImIp1nS
v	v	k7c6
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
v	v	k7c6
jaké	jaký	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
formě	forma	k1gFnSc6
(	(	kIx(
<g/>
mocenství	mocenství	k1gNnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
prvek	prvek	k1gInSc1
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
živými	živý	k2eAgInPc7d1
organizmy	organizmus	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
potravin	potravina	k1gFnPc2
bohatých	bohatý	k2eAgInPc2d1
na	na	k7c4
trojmocný	trojmocný	k2eAgInSc4d1
chrom	chrom	k1gInSc4
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
především	především	k9
melasu	melasa	k1gFnSc4
a	a	k8xC
přírodní	přírodní	k2eAgInSc4d1
hnědý	hnědý	k2eAgInSc4d1
cukr	cukr	k1gInSc4
<g/>
,	,	kIx,
červenou	červený	k2eAgFnSc4d1
řepu	řepa	k1gFnSc4
<g/>
,	,	kIx,
lesní	lesní	k2eAgFnPc4d1
plodiny	plodina	k1gFnPc4
<g/>
,	,	kIx,
kvasnice	kvasnice	k1gFnPc4
a	a	k8xC
pivo	pivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prodávaných	prodávaný	k2eAgInPc6d1
potravinových	potravinový	k2eAgInPc6d1
doplňcích	doplněk	k1gInPc6
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
používá	používat	k5eAaImIp3nS
organická	organický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
pikolinát	pikolinát	k1gInSc4
chromitý	chromitý	k2eAgInSc1d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
však	však	k9
u	u	k7c2
pokusných	pokusný	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
způsoboval	způsobovat	k5eAaImAgInS
poškození	poškození	k1gNnSc4
chromozomů	chromozom	k1gInPc2
ovariálních	ovariální	k2eAgFnPc2d1
buněk	buňka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Doporučená	doporučený	k2eAgFnSc1d1
denní	denní	k2eAgFnSc1d1
dávka	dávka	k1gFnSc1
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
0,1	0,1	k4
mg	mg	kA
chromu	chrom	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Dostatečný	dostatečný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
chromu	chromat	k5eAaImIp1nS
v	v	k7c6
organizmu	organizmus	k1gInSc6
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc1d1
pro	pro	k7c4
správný	správný	k2eAgInSc4d1
metabolismus	metabolismus	k1gInSc4
cukrů	cukr	k1gInPc2
a	a	k8xC
tuků	tuk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Pomáhá	pomáhat	k5eAaImIp3nS
stabilizovat	stabilizovat	k5eAaBmF
hladinu	hladina	k1gFnSc4
krevního	krevní	k2eAgInSc2d1
tuku	tuk	k1gInSc2
a	a	k8xC
tlumí	tlumit	k5eAaImIp3nS
chuť	chuť	k1gFnSc4
na	na	k7c4
sladké	sladký	k2eAgFnPc4d1
potraviny	potravina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Farmaceutické	farmaceutický	k2eAgInPc1d1
přípravky	přípravek	k1gInPc1
s	s	k7c7
obsahem	obsah	k1gInSc7
chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
vhodné	vhodný	k2eAgFnPc1d1
ke	k	k7c3
kontrole	kontrola	k1gFnSc3
tělesné	tělesný	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
a	a	k8xC
také	také	k9
jako	jako	k9
doplněk	doplněk	k1gInSc4
sportovní	sportovní	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
pro	pro	k7c4
údajný	údajný	k2eAgInSc4d1
<g/>
,	,	kIx,
ovšem	ovšem	k9
neprokázaný	prokázaný	k2eNgInSc4d1
růst	růst	k1gInSc4
svalové	svalový	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
–	–	k?
viz	vidět	k5eAaImRp2nS
kulturistika	kulturistika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Kontaminace	kontaminace	k1gFnSc1
</s>
<s>
Ke	k	k7c3
kontaminaci	kontaminace	k1gFnSc3
chromem	chrom	k1gInSc7
dochází	docházet	k5eAaImIp3nS
zejména	zejména	k9
na	na	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
provozován	provozovat	k5eAaImNgInS
některý	některý	k3yIgInSc1
typ	typ	k1gInSc1
průmyslových	průmyslový	k2eAgFnPc2d1
činností	činnost	k1gFnPc2
jako	jako	k9
jsou	být	k5eAaImIp3nP
</s>
<s>
koželužny	koželužna	k1gFnPc1
<g/>
,	,	kIx,
strojírny	strojírna	k1gFnPc1
<g/>
,	,	kIx,
slévárny	slévárna	k1gFnPc1
<g/>
,	,	kIx,
textilky	textilka	k1gFnPc1
<g/>
,	,	kIx,
chemické	chemický	k2eAgFnPc1d1
výroby	výroba	k1gFnPc1
nátěrových	nátěrový	k2eAgFnPc2d1
hmot	hmota	k1gFnPc2
<g/>
,	,	kIx,
pigmentů	pigment	k1gInPc2
a	a	k8xC
smaltů	smalt	k1gInPc2
nebo	nebo	k8xC
sklárny	sklárna	k1gFnSc2
a	a	k8xC
keramické	keramický	k2eAgInPc1d1
závody	závod	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Regulace	regulace	k1gFnSc1
</s>
<s>
Vzhledem	vzhledem	k7c3
ke	k	k7c3
škodlivosti	škodlivost	k1gFnSc3
chromu	chrom	k1gInSc2
jsou	být	k5eAaImIp3nP
zavedeny	zavést	k5eAaPmNgFnP
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
limity	limita	k1gFnSc2
pro	pro	k7c4
koncentrace	koncentrace	k1gFnPc4
jeho	jeho	k3xOp3gFnPc2
sloučenin	sloučenina	k1gFnPc2
v	v	k7c6
pracovním	pracovní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pro	pro	k7c4
sloučeniny	sloučenina	k1gFnPc4
šestimocného	šestimocný	k2eAgInSc2d1
chromu	chrom	k1gInSc2
na	na	k7c4
0,1	0,1	k4
mg	mg	kA
<g/>
.	.	kIx.
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
a	a	k8xC
pro	pro	k7c4
ostatní	ostatní	k2eAgFnPc4d1
sloučeniny	sloučenina	k1gFnPc4
chromu	chromat	k5eAaImIp1nS
na	na	k7c4
1,5	1,5	k4
mg	mg	kA
<g/>
.	.	kIx.
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Směrnice	směrnice	k1gFnSc1
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
/	/	kIx~
<g/>
ES	es	k1gNnPc2
–	–	k?
RoHS	RoHS	k1gFnSc2
–	–	k?
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2006	#num#	k4
omezuje	omezovat	k5eAaImIp3nS
použití	použití	k1gNnSc4
šestimocného	šestimocný	k2eAgInSc2d1
chromu	chrom	k1gInSc2
v	v	k7c6
z	z	k7c2
elektrických	elektrický	k2eAgNnPc2d1
a	a	k8xC
elektronických	elektronický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vypouštění	vypouštění	k1gNnSc1
chromu	chrom	k1gInSc2
do	do	k7c2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
povinné	povinný	k2eAgNnSc4d1
ohlašovat	ohlašovat	k5eAaImF
do	do	k7c2
integrovaného	integrovaný	k2eAgInSc2d1
registru	registr	k1gInSc2
znečišťování	znečišťování	k1gNnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
podnik	podnik	k1gInSc1
vypouští	vypouštět	k5eAaImIp3nS
do	do	k7c2
ovzduší	ovzduší	k1gNnSc2
<g/>
,	,	kIx,
vody	voda	k1gFnSc2
nebo	nebo	k8xC
v	v	k7c6
odpadech	odpad	k1gInPc6
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
kilogramů	kilogram	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
pro	pro	k7c4
chemické	chemický	k2eAgFnPc4d1
látky	látka	k1gFnPc4
zařadila	zařadit	k5eAaPmAgFnS
některé	některý	k3yIgNnSc4
ze	z	k7c2
sloučenin	sloučenina	k1gFnPc2
chromu	chromat	k5eAaImIp1nS
mezi	mezi	k7c4
látky	látka	k1gFnPc4
vzbuzující	vzbuzující	k2eAgFnSc2d1
mimořádné	mimořádný	k2eAgFnSc2d1
obavy	obava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ASHBY	ASHBY	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
F.	F.	kA
<g/>
,	,	kIx,
&	&	k?
David	David	k1gMnSc1
R.	R.	kA
H.	H.	kA
Jones	Jones	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Engineering	Engineering	k1gInSc1
Materials	Materials	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
with	with	k1gInSc1
corrections	corrections	k1gInSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Pergamon	pergamon	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
32532	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Chapter	Chaptra	k1gFnPc2
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
119	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Bardoděj	Bardoděj	k1gMnSc1
<g/>
,	,	kIx,
Z.	Z.	kA
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
v	v	k7c6
hygieně	hygiena	k1gFnSc6
a	a	k8xC
toxikologii	toxikologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
LFH	LFH	kA
UK	UK	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1988	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chrom	chrom	k1gInSc1
v	v	k7c6
půdě	půda	k1gFnSc6
nepodceňujte	podceňovat	k5eNaImRp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
jen	jen	k9
zabijákem	zabiják	k1gInSc7
v	v	k7c6
hollywoodském	hollywoodský	k2eAgInSc6d1
trháku	trhák	k1gInSc6
<g/>
↑	↑	k?
ToxFAQs	ToxFAQs	k1gInSc1
<g/>
:	:	kIx,
Chromium	Chromium	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agency	Agenca	k1gFnSc2
for	forum	k1gNnPc2
Toxic	Toxic	k1gMnSc1
Substances	Substances	k1gMnSc1
&	&	k?
Disease	Diseasa	k1gFnSc6
Registry	registr	k1gInPc4
<g/>
,	,	kIx,
Centers	Centers	k1gInSc4
for	forum	k1gNnPc2
Disease	Diseasa	k1gFnSc3
Control	Control	k1gInSc1
and	and	k?
Prevention	Prevention	k1gInSc1
<g/>
,	,	kIx,
February	Februar	k1gInPc1
2001	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STEARNS	STEARNS	kA
<g/>
,	,	kIx,
D.	D.	kA
M.	M.	kA
<g/>
;	;	kIx,
W	W	kA
<g/>
;	;	kIx,
P	P	kA
<g/>
;	;	kIx,
W.	W.	kA
Chromium	Chromium	k1gNnSc4
<g/>
(	(	kIx(
<g/>
III	III	kA
<g/>
)	)	kIx)
picolinate	picolinat	k1gInSc5
produces	produces	k1gMnSc1
chromosome	chromosom	k1gInSc5
damage	damag	k1gInPc4
in	in	k?
Chinese	Chinese	k1gFnSc2
hamster	hamster	k1gMnSc1
ovary	ovar	k1gInPc4
cells	cells	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federation	Federation	k1gInSc1
of	of	k?
American	American	k1gInSc1
Societies	Societiesa	k1gFnPc2
for	forum	k1gNnPc2
Experimental	Experimental	k1gMnSc1
Biology	biolog	k1gMnPc7
<g/>
.	.	kIx.
1	#num#	k4
December	December	k1gInSc1
1995	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
15	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1643	#num#	k4
<g/>
–	–	k?
<g/>
1648	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
8529845	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Chrom	chrom	k1gInSc1
v	v	k7c6
půdě	půda	k1gFnSc6
nepodceňujte	podceňovat	k5eNaImRp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
jen	jen	k9
zabijákem	zabiják	k1gInSc7
v	v	k7c6
hollywoodském	hollywoodský	k2eAgInSc6d1
trháku	trhák	k1gInSc6
<g/>
1	#num#	k4
2	#num#	k4
IRZ	IRZ	kA
<g/>
:	:	kIx,
Látka	látka	k1gFnSc1
<g/>
:	:	kIx,
Chrom	chrom	k1gInSc1
a	a	k8xC
sloučeniny	sloučenina	k1gFnPc1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
Cr	cr	k0
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Miroslav	Miroslav	k1gMnSc1
Šuta	šuta	k1gFnSc1
<g/>
:	:	kIx,
Zákaz	zákaz	k1gInSc1
některých	některý	k3yIgFnPc2
chemikálií	chemikálie	k1gFnPc2
v	v	k7c6
nových	nový	k2eAgInPc6d1
spotřebičích	spotřebič	k1gInPc6
Archivováno	archivován	k2eAgNnSc4d1
24	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Odpady	odpad	k1gInPc1
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2006	#num#	k4
<g/>
↑	↑	k?
Chrom	chrom	k1gInSc1
v	v	k7c6
půdě	půda	k1gFnSc6
nepodceňujte	podceňovat	k5eNaImRp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
jen	jen	k9
zabijákem	zabiják	k1gInSc7
v	v	k7c6
hollywoodském	hollywoodský	k2eAgInSc6d1
trháku	trhák	k1gInSc6
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
chrom	chrom	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
chrom	chrom	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Chemický	chemický	k2eAgInSc1d1
vzdělávací	vzdělávací	k2eAgInSc1d1
portál	portál	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
ATSDR	ATSDR	kA
Case	Case	k1gNnSc1
Studies	Studies	k1gMnSc1
in	in	k?
Environmental	Environmental	k1gMnSc1
Medicine	Medicin	k1gInSc5
<g/>
:	:	kIx,
Chromium	Chromium	k1gNnSc4
Toxicity	toxicita	k1gFnSc2
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
Health	Health	k1gMnSc1
and	and	k?
Human	Human	k1gMnSc1
Services	Services	k1gMnSc1
</s>
<s>
It	It	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Elemental	Elemental	k1gMnSc1
–	–	k?
The	The	k1gMnSc4
Element	element	k1gInSc1
Chromium	Chromium	k1gNnSc1
</s>
<s>
The	The	k?
Merck	Merck	k1gMnSc1
Manual	Manual	k1gMnSc1
–	–	k?
Mineral	Mineral	k1gMnSc1
Deficiency	Deficienca	k1gFnSc2
and	and	k?
Toxicity	toxicita	k1gFnSc2
</s>
<s>
National	Nationat	k5eAaImAgMnS,k5eAaPmAgMnS
Institute	institut	k1gInSc5
for	forum	k1gNnPc2
Occupational	Occupational	k1gFnPc7
Safety	Safeta	k1gFnSc2
and	and	k?
Health	Health	k1gMnSc1
–	–	k?
Chromium	Chromium	k1gNnSc4
Page	Pag	k1gFnSc2
</s>
<s>
The	The	k?
periodic	periodic	k1gMnSc1
table	tablo	k1gNnSc6
of	of	k?
videos	videos	k1gInSc1
<g/>
:	:	kIx,
Chromium	Chromium	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4147944-0	4147944-0	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5768	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85025350	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85025350	#num#	k4
</s>
