<s desamb="1">
Hranice	hranice	k1gFnSc1
regionu	region	k1gInSc2
byly	být	k5eAaImAgInP
stanoveny	stanovit	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
znemožnily	znemožnit	k5eAaPmAgFnP
růst	růst	k5eAaImF
chorvatského	chorvatský	k2eAgMnSc4d1
<g/>
,	,	kIx,
resp.	resp.	kA
albánského	albánský	k2eAgInSc2d1
nacionalismu	nacionalismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
byly	být	k5eAaImAgFnP
západní	západní	k2eAgFnPc1d1
části	část	k1gFnPc1
připojeny	připojit	k5eAaPmNgFnP
k	k	k7c3
Chorvatské	chorvatský	k2eAgFnSc3d1
bánovině	bánovina	k1gFnSc3
<g/>
.	.	kIx.
</s>