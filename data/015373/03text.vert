<s>
Zetská	Zetský	k2eAgFnSc1d1
bánovina	bánovina	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Zetské	Zetský	k2eAgFnSc2d1
bánoviny	bánovina	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
jugoslávského	jugoslávský	k2eAgNnSc2d1
království	království	k1gNnSc2
</s>
<s>
Zetská	Zetský	k2eAgFnSc1d1
bánovina	bánovina	k1gFnSc1
(	(	kIx(
<g/>
srbochorvatsky	srbochorvatsky	k6eAd1
З	З	k?
б	б	k?
<g/>
/	/	kIx~
<g/>
Zetska	Zetska	k1gFnSc1
banovina	banovina	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
administrativní	administrativní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
Království	království	k1gNnSc1
Jugoslávie	Jugoslávie	k1gFnSc2
mezi	mezi	k7c7
lety	let	k1gInPc7
1929	#num#	k4
až	až	k9
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgInSc7
názvem	název	k1gInSc7
odkazovala	odkazovat	k5eAaImAgFnS
na	na	k7c4
středověké	středověký	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
Zeta	Zet	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Zahrnovala	zahrnovat	k5eAaImAgFnS
území	území	k1gNnSc4
dnešní	dnešní	k2eAgFnSc2d1
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
<g/>
,	,	kIx,
jihozápadní	jihozápadní	k2eAgFnSc1d1
část	část	k1gFnSc1
současné	současný	k2eAgFnSc2d1
Bosny	Bosna	k1gFnSc2
a	a	k8xC
Hercegoviny	Hercegovina	k1gFnSc2
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
současného	současný	k2eAgNnSc2d1
Chorvatska	Chorvatsko	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
města	město	k1gNnSc2
Dubrovník	Dubrovník	k1gInSc4
až	až	k9
po	po	k7c6
Prevlaku	Prevlak	k1gInSc6
<g/>
,	,	kIx,
západ	západ	k1gInSc1
Kosova	Kosův	k2eAgNnSc2d1
a	a	k8xC
část	část	k1gFnSc1
regionu	region	k1gInSc2
Sandžak	sandžak	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správní	správní	k2eAgInSc1d1
centrum	centrum	k1gNnSc4
regionu	region	k1gInSc2
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
ve	v	k7c6
městě	město	k1gNnSc6
Cetinje	Cetinj	k1gMnSc2
<g/>
,	,	kIx,
historickém	historický	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
černohorského	černohorský	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
bánoviny	bánovina	k1gFnSc2
činila	činit	k5eAaImAgFnS
30	#num#	k4
997	#num#	k4
km²	km²	k?
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
zde	zde	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenat	k5eAaPmNgNnS
925	#num#	k4
516	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
regionu	region	k1gInSc2
byly	být	k5eAaImAgInP
stanoveny	stanovit	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
znemožnily	znemožnit	k5eAaPmAgFnP
růst	růst	k5eAaImF
chorvatského	chorvatský	k2eAgMnSc4d1
<g/>
,	,	kIx,
resp.	resp.	kA
albánského	albánský	k2eAgInSc2d1
nacionalismu	nacionalismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
byly	být	k5eAaImAgFnP
západní	západní	k2eAgFnPc1d1
části	část	k1gFnPc1
připojeny	připojit	k5eAaPmNgFnP
k	k	k7c3
Chorvatské	chorvatský	k2eAgFnSc3d1
bánovině	bánovina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Představitelé	představitel	k1gMnPc1
Zetské	Zetský	k2eAgFnSc2d1
bánoviny	bánovina	k1gFnSc2
</s>
<s>
1929	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
<g/>
:	:	kIx,
Krsta	Krst	k1gInSc2
Smiljanić	Smiljanić	k1gFnSc2
</s>
<s>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
:	:	kIx,
Uroš	Uroš	k1gMnSc1
Krulj	Krulj	k1gMnSc1
</s>
<s>
1932	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
:	:	kIx,
Aleksa	Aleksa	k1gFnSc1
Stanišić	Stanišić	k1gFnSc2
</s>
<s>
1934	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
:	:	kIx,
Mujo	Mujo	k1gMnSc1
Sočica	Sočica	k1gMnSc1
</s>
<s>
1936	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
:	:	kIx,
Petar	Petar	k1gMnSc1
Ivanišević	Ivanišević	k1gMnSc1
</s>
<s>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
:	:	kIx,
Božidar	Božidar	k1gMnSc1
Krstić	Krstić	k1gMnSc1
</s>
<s>
1941	#num#	k4
<g/>
:	:	kIx,
Blažo	Blažo	k1gMnSc1
Đukanović	Đukanović	k1gMnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ŠÍSTEK	ŠÍSTEK	k?
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
614	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
498	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
348	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
