<s>
Kopřiva	Kopřiva	k1gMnSc1
</s>
<s>
Kopřiva	kopřiva	k1gFnSc1
Kopřiva	kopřiva	k1gFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Urtica	Urtic	k2eAgFnSc1d1
dioica	dioica	k1gFnSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
růžotvaré	růžotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Rosales	Rosales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
kopřivovité	kopřivovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Urticaceae	Urticacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
kopřiva	kopřiva	k1gFnSc1
(	(	kIx(
<g/>
Urtica	Urtica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
L.	L.	kA
<g/>
,	,	kIx,
1753	#num#	k4
Druhy	druh	k1gInPc7
v	v	k7c6
ČR	ČR	kA
</s>
<s>
kopřiva	kopřiva	k1gFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
(	(	kIx(
<g/>
U.	U.	kA
dioica	dioica	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
kopřiva	kopřiva	k1gFnSc1
žahavka	žahavka	k1gFnSc1
(	(	kIx(
<g/>
U.	U.	kA
urens	urens	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
kopřiva	kopřiva	k1gFnSc1
lužní	lužní	k2eAgFnSc1d1
(	(	kIx(
<g/>
U.	U.	kA
kioviensis	kioviensis	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
další	další	k2eAgMnPc1d1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
text	text	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
rodu	rod	k1gInSc6
rostlin	rostlina	k1gFnPc2
Urtica	Urtic	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Kopřiva	Kopřiva	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kopřiva	Kopřiva	k1gMnSc1
(	(	kIx(
<g/>
Urtica	Urtica	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rod	rod	k1gInSc4
rostlin	rostlina	k1gFnPc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
kopřivovitých	kopřivovitý	k2eAgFnPc2d1
(	(	kIx(
<g/>
Urticaceae	Urticaceae	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
žahavé	žahavý	k2eAgInPc1d1
chlupy	chlup	k1gInPc1
<g/>
,	,	kIx,
palisty	palist	k1gInPc1
a	a	k8xC
vstřícné	vstřícný	k2eAgInPc1d1
listy	list	k1gInPc1
s	s	k7c7
pilovitými	pilovitý	k2eAgInPc7d1
<g/>
,	,	kIx,
vzácně	vzácně	k6eAd1
celokrajnými	celokrajný	k2eAgInPc7d1
<g/>
,	,	kIx,
okraji	okraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
dotyku	dotyk	k1gInSc6
se	se	k3xPyFc4
trichomy	trichom	k1gInPc7
zabodnou	zabodnout	k5eAaPmIp3nP
do	do	k7c2
kůže	kůže	k1gFnSc2
a	a	k8xC
při	při	k7c6
následném	následný	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
způsobí	způsobit	k5eAaPmIp3nS
její	její	k3xOp3gNnSc1
malé	malý	k2eAgNnSc1d1
a	a	k8xC
nepatrné	patrný	k2eNgNnSc1d1,k2eAgNnSc1d1
protrhnutí	protrhnutí	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
bolesti	bolest	k1gFnSc3
a	a	k8xC
svědění	svědění	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nejznámějším	známý	k2eAgInSc7d3
druhem	druh	k1gInSc7
tohoto	tento	k3xDgInSc2
rodu	rod	k1gInSc2
je	být	k5eAaImIp3nS
kopřiva	kopřiva	k1gFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Urtica	Urtic	k2eAgFnSc1d1
dioica	dioica	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
původem	původ	k1gInSc7
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc2
a	a	k8xC
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
kopřiv	kopřiva	k1gFnPc2
</s>
<s>
Rod	rod	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
mnoho	mnoho	k4c4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velká	k1gFnSc2
množství	množství	k1gNnSc2
druhových	druhový	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
počítaných	počítaný	k2eAgNnPc2d1
k	k	k7c3
němu	on	k3xPp3gInSc3
ve	v	k7c6
starší	starý	k2eAgFnSc6d2
literatuře	literatura	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
nyní	nyní	k6eAd1
považuje	považovat	k5eAaImIp3nS
za	za	k7c2
synonyma	synonymum	k1gNnSc2
kopřivy	kopřiva	k1gFnSc2
dvoudomé	dvoudomý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
z	z	k7c2
těchto	tento	k3xDgNnPc2
jmen	jméno	k1gNnPc2
přežívají	přežívat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
jména	jméno	k1gNnPc1
poddruhů	poddruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
tři	tři	k4xCgInPc1
druhy	druh	k1gInPc1
rodu	rod	k1gInSc2
kopřiva	kopřiva	k1gFnSc1
<g/>
:	:	kIx,
kopřiva	kopřiva	k1gFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
(	(	kIx(
<g/>
hojná	hojný	k2eAgFnSc1d1
po	po	k7c6
celé	celá	k1gFnSc6
ČR	ČR	kA
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
nejvyšších	vysoký	k2eAgFnPc2d3
poloh	poloha	k1gFnPc2
Krkonoš	Krkonoše	k1gFnPc2
<g/>
,	,	kIx,
Jeseníků	Jeseník	k1gInPc2
a	a	k8xC
Králického	králický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kopřiva	kopřiva	k1gFnSc1
žahavka	žahavka	k1gFnSc1
(	(	kIx(
<g/>
běžná	běžný	k2eAgFnSc1d1
v	v	k7c6
teplejších	teplý	k2eAgInPc6d2
krajích	kraj	k1gInPc6
<g/>
,	,	kIx,
jinde	jinde	k6eAd1
vzácná	vzácný	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
kopřiva	kopřiva	k1gFnSc1
lužní	lužní	k2eAgFnSc1d1
(	(	kIx(
<g/>
vzácně	vzácně	k6eAd1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
považována	považován	k2eAgFnSc1d1
za	za	k7c7
vyhynulou	vyhynulý	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nepůvodních	původní	k2eNgInPc2d1
druhů	druh	k1gInPc2
se	se	k3xPyFc4
u	u	k7c2
nás	my	k3xPp1nPc2
mohou	moct	k5eAaImIp3nP
vyskytnout	vyskytnout	k5eAaPmF
zplanělé	zplanělý	k2eAgFnPc4d1
středomořské	středomořský	k2eAgFnPc4d1
kopřivy	kopřiva	k1gFnPc4
kopřiva	kopřiva	k1gFnSc1
Dodartova	Dodartův	k2eAgFnSc1d1
a	a	k8xC
kopřiva	kopřiva	k1gFnSc1
kulkonosná	kulkonosný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
angustifolia	angustifolia	k1gFnSc1
–	–	k?
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Korea	Korea	k1gFnSc1
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
cannabina	cannabina	k1gFnSc1
–	–	k?
záp	záp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asie	Asie	k1gFnSc1
od	od	k7c2
Sibiře	Sibiř	k1gFnSc2
po	po	k7c4
Írán	Írán	k1gInSc4
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
dioica	dioica	k1gFnSc1
L.	L.	kA
–	–	k?
kopřiva	kopřiva	k1gFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
-	-	kIx~
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc1
<g/>
,	,	kIx,
Sev	Sev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amerika	Amerika	k1gFnSc1
</s>
<s>
Urtica	Urtica	k1gMnSc1
dodartii	dodartie	k1gFnSc3
L.	L.	kA
–	–	k?
kopřiva	kopřiva	k1gFnSc1
Dodartova	Dodartův	k2eAgFnSc1d1
</s>
<s>
Urtica	Urtica	k6eAd1
ferox	ferox	k1gInSc1
–	–	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
hyperborea	hyperborea	k1gFnSc1
–	–	k?
Himálaj	Himálaj	k1gFnSc1
od	od	k7c2
Pákistánu	Pákistán	k1gInSc2
po	po	k7c4
Bhútán	Bhútán	k1gInSc4
<g/>
,	,	kIx,
Mongolsko	Mongolsko	k1gNnSc1
a	a	k8xC
Tibet	Tibet	k1gInSc1
<g/>
,	,	kIx,
vysoké	vysoký	k2eAgFnSc2d1
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
incisa	incisa	k1gFnSc1
–	–	k?
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
kioviensis	kioviensis	k1gFnSc1
Rogow	Rogow	k1gFnSc2
<g/>
.	.	kIx.
–	–	k?
kopřiva	kopřiva	k1gFnSc1
lužní	lužní	k2eAgFnSc1d1
</s>
<s>
Urtica	Urtica	k1gFnSc1
laetivirens	laetivirensa	k1gFnPc2
–	–	k?
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Mandžusko	Mandžusko	k1gNnSc1
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
parviflora	parviflora	k1gFnSc1
–	–	k?
Himálaj	Himálaj	k1gFnSc1
(	(	kIx(
<g/>
nižší	nízký	k2eAgFnSc2d2
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
pilulifera	pilulifera	k1gFnSc1
L.	L.	kA
–	–	k?
kopřiva	kopřiva	k1gFnSc1
kulkonosná	kulkonosný	k2eAgFnSc1d1
-	-	kIx~
Evropa	Evropa	k1gFnSc1
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
platyphylla	platyphylla	k1gFnSc1
–	–	k?
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Urtica	Urtic	k2eAgFnSc1d1
thunbergiana	thunbergiana	k1gFnSc1
–	–	k?
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Urtica	Urtica	k6eAd1
urens	urens	k1gInSc1
L.	L.	kA
–	–	k?
kopřiva	kopřiva	k1gFnSc1
žahavka	žahavka	k1gFnSc1
-	-	kIx~
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Sev	Sev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amerika	Amerika	k1gFnSc1
</s>
<s>
Léčivé	léčivý	k2eAgInPc1d1
účinky	účinek	k1gInPc1
</s>
<s>
Kopřiva	Kopřiva	k1gMnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
při	při	k7c6
nemocech	nemoc	k1gFnPc6
kloubů	kloub	k1gInPc2
<g/>
,	,	kIx,
při	při	k7c6
krvácení	krvácení	k1gNnSc6
z	z	k7c2
dásní	dáseň	k1gFnPc2
<g/>
,	,	kIx,
chudokrevnosti	chudokrevnost	k1gFnSc2
(	(	kIx(
<g/>
anémie	anémie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
také	také	k9
používat	používat	k5eAaImF
v	v	k7c6
kuchyni	kuchyně	k1gFnSc6
jako	jako	k8xC,k8xS
špenát	špenát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kopřiva	Kopřiva	k1gMnSc1
neobsahuje	obsahovat	k5eNaImIp3nS
kyselinu	kyselina	k1gFnSc4
šťavelovou	šťavelový	k2eAgFnSc4d1
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
je	být	k5eAaImIp3nS
špenát	špenát	k1gInSc4
při	při	k7c6
některých	některý	k3yIgNnPc6
onemocněních	onemocnění	k1gNnPc6
a	a	k8xC
poruchách	porucha	k1gFnPc6
nevhodný	vhodný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvhodnější	vhodný	k2eAgFnSc1d3
pro	pro	k7c4
využití	využití	k1gNnSc4
v	v	k7c6
kuchyni	kuchyně	k1gFnSc6
jsou	být	k5eAaImIp3nP
mladé	mladý	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
sbírané	sbíraný	k2eAgFnPc4d1
brzy	brzy	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
se	se	k3xPyFc4
Kopřiva	kopřiva	k1gFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
může	moct	k5eAaImIp3nS
používat	používat	k5eAaImF
při	při	k7c6
onemocnění	onemocnění	k1gNnSc6
bronchitidou	bronchitida	k1gFnSc7
<g/>
,	,	kIx,
cukrovkou	cukrovka	k1gFnSc7
<g/>
,	,	kIx,
akné	akné	k1gFnSc7
<g/>
,	,	kIx,
nežidech	nežid	k1gMnPc6
a	a	k8xC
vyrážce	vyrážka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kopřiva	kopřiva	k1gFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
též	též	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
tvorbu	tvorba	k1gFnSc4
mateřského	mateřský	k2eAgNnSc2d1
mléka	mléko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Využívány	využíván	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
hlavně	hlavně	k9
listy	list	k1gInPc1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
uplatnění	uplatnění	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
i	i	k9
nať	nať	k1gFnSc4
či	či	k8xC
oddenek	oddenek	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgNnPc6,k3yQgNnPc6,k3yIgNnPc6
jsou	být	k5eAaImIp3nP
obsaženy	obsažen	k2eAgInPc1d1
flavonoidy	flavonoid	k1gInPc1
<g/>
,	,	kIx,
fenolkarbonové	fenolkarbonový	k2eAgFnPc1d1
kyseliny	kyselina	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
minerálních	minerální	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideální	ideální	k2eAgInSc4d1
období	období	k1gNnSc1
jejich	jejich	k3xOp3gInSc2
sběru	sběr	k1gInSc2
představuje	představovat	k5eAaImIp3nS
jaro	jaro	k1gNnSc4
do	do	k7c2
konce	konec	k1gInSc2
května	květen	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
kopřivy	kopřiva	k1gFnSc2
v	v	k7c6
agroprůmyslovém	agroprůmyslový	k2eAgInSc6d1
komplexu	komplex	k1gInSc6
</s>
<s>
Rod	rod	k1gInSc1
kopřiva	kopřiva	k1gFnSc1
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
průmyslu	průmysl	k1gInSc6
několikeré	několikerý	k4xRyIgFnPc4
využití	využití	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
např.	např.	kA
několikaletá	několikaletý	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
ke	k	k7c3
zpracování	zpracování	k1gNnSc3
v	v	k7c6
krmivářství	krmivářství	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
produkci	produkce	k1gFnSc6
biomasy	biomasa	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
textilním	textilní	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
potravinářství	potravinářství	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
kuchyni	kuchyně	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
farmacii	farmacie	k1gFnSc6
a	a	k8xC
kosmetice	kosmetika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informace	informace	k1gFnSc1
pro	pro	k7c4
laickou	laický	k2eAgFnSc4d1
i	i	k8xC
odbornou	odborný	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
propojuje	propojovat	k5eAaImIp3nS
a	a	k8xC
zveřejňuje	zveřejňovat	k5eAaImIp3nS
na	na	k7c6
svých	svůj	k3xOyFgInPc6
kanálech	kanál	k1gInPc6
v	v	k7c6
několika	několik	k4yIc6
jazycích	jazyk	k1gInPc6
neformální	formální	k2eNgMnPc1d1
hub	houba	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kubát	Kubát	k1gMnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Klíč	klíč	k1gInSc1
ke	k	k7c3
květeně	květena	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
↑	↑	k?
HERBACEA	HERBACEA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kopřiva	kopřiva	k1gFnSc1
dvoudomá	dvoudomý	k2eAgFnSc1d1
a	a	k8xC
léčebné	léčebný	k2eAgInPc4d1
účinky	účinek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylinkovo	Bylinkův	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Nettlenet	Nettlenet	k1gInSc1
<g/>
.	.	kIx.
nettlenet	ttlenet	k5eNaPmF,k5eNaImF,k5eNaBmF
<g/>
.	.	kIx.
<g/>
webnode	webnod	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NNI	NNI	kA
<g/>
.	.	kIx.
psiinstitute	psiinstitut	k1gInSc5
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kopřiva	kopřiva	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kopřiva	kopřiva	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4136380-2	4136380-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85091055	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85091055	#num#	k4
</s>
