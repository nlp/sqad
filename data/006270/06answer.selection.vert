<s>
Na	na	k7c6	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
schůzi	schůze	k1gFnSc6	schůze
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
byl	být	k5eAaImAgInS	být
předložen	předložit	k5eAaPmNgInS	předložit
společný	společný	k2eAgInSc1d1	společný
návrh	návrh	k1gInSc1	návrh
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ČNR	ČNR	kA	ČNR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
SNR	SNR	kA	SNR
<g/>
)	)	kIx)	)
a	a	k8xC	a
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
<g/>
.	.	kIx.	.
</s>
