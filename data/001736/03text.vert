<s>
Red	Red	k?	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
zkráceně	zkráceně	k6eAd1	zkráceně
RHCP	RHCP	kA	RHCP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
(	(	kIx(	(
<g/>
kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
<g/>
)	)	kIx)	)
funk-rocková	funkockový	k2eAgFnSc1d1	funk-rockový
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
s	s	k7c7	s
částečně	částečně	k6eAd1	částečně
rapovými	rapový	k2eAgInPc7d1	rapový
prvky	prvek	k1gInPc7	prvek
a	a	k8xC	a
prvky	prvek	k1gInPc7	prvek
punku	punk	k1gInSc2	punk
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
vzniku	vznik	k1gInSc2	vznik
Red	Red	k1gFnSc2	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc4	Chile
Peppers	Peppers	k1gInSc4	Peppers
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
Fairfax	Fairfax	k1gInSc1	Fairfax
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc4	School
potkali	potkat	k5eAaPmAgMnP	potkat
dva	dva	k4xCgMnPc1	dva
pozdější	pozdní	k2eAgMnPc1d2	pozdější
dlouholetí	dlouholetý	k2eAgMnPc1d1	dlouholetý
kamarádi	kamarád	k1gMnPc1	kamarád
Anthony	Anthona	k1gFnSc2	Anthona
Kiedis	Kiedis	k1gFnSc2	Kiedis
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
narozen	narodit	k5eAaPmNgInS	narodit
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Rapids	Rapids	k1gInSc1	Rapids
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
)	)	kIx)	)
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Balzary	Balzara	k1gFnPc4	Balzara
alias	alias	k9	alias
Flea	Flea	k1gMnSc1	Flea
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgInSc1d1	narozen
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
neměli	mít	k5eNaImAgMnP	mít
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
dětství	dětství	k1gNnSc4	dětství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
rozvrácených	rozvrácený	k2eAgFnPc2d1	rozvrácená
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Anthony	Anthon	k1gMnPc4	Anthon
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gNnSc4	on
velmi	velmi	k6eAd1	velmi
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
k	k	k7c3	k
horšímu	horší	k1gNnSc3	horší
-	-	kIx~	-
Anthony	Anthona	k1gFnPc4	Anthona
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgInS	moct
dělat	dělat	k5eAaImF	dělat
takřka	takřka	k6eAd1	takřka
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
chtěl	chtít	k5eAaImAgMnS	chtít
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
vůbec	vůbec	k9	vůbec
nedbal	dbát	k5eNaImAgMnS	dbát
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
výchovu	výchova	k1gFnSc4	výchova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Blackie	Blackie	k1gFnSc2	Blackie
Dammet	Dammet	k1gMnSc1	Dammet
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
u	u	k7c2	u
béčkových	béčkový	k2eAgInPc2d1	béčkový
filmů	film	k1gInPc2	film
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
otec	otec	k1gMnSc1	otec
si	se	k3xPyFc3	se
i	i	k9	i
Anthony	Anthona	k1gFnPc1	Anthona
zahrál	zahrát	k5eAaPmAgInS	zahrát
v	v	k7c6	v
pár	pár	k4xCyI	pár
filmech	film	k1gInPc6	film
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Cole	cola	k1gFnSc3	cola
Dammet	Dammet	k1gInSc1	Dammet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
odborového	odborový	k2eAgMnSc2d1	odborový
předáka	předák	k1gMnSc2	předák
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
F.	F.	kA	F.
<g/>
I.	I.	kA	I.
<g/>
S.	S.	kA	S.
<g/>
T.	T.	kA	T.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Sylvestera	Sylvester	k1gMnSc2	Sylvester
Stallone	Stallon	k1gInSc5	Stallon
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Anthony	Anthon	k1gMnPc4	Anthon
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
Fairfax	Fairfax	k1gInSc4	Fairfax
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
chodila	chodit	k5eAaImAgFnS	chodit
na	na	k7c4	na
Fairfax	Fairfax	k1gInSc4	Fairfax
i	i	k8xC	i
nerozlučná	rozlučný	k2eNgFnSc1d1	nerozlučná
dvojice	dvojice	k1gFnSc1	dvojice
přicházející	přicházející	k2eAgFnSc1d1	přicházející
ze	z	k7c2	z
židovské	židovský	k2eAgFnSc2d1	židovská
komunity	komunita	k1gFnSc2	komunita
v	v	k7c6	v
L.A.	L.A.	k1gFnSc6	L.A.
-	-	kIx~	-
Hillel	Hillel	k1gFnSc1	Hillel
Slovak	Slovak	k1gInSc1	Slovak
(	(	kIx(	(
<g/>
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Izraele	Izrael	k1gInSc2	Izrael
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jack	Jack	k1gMnSc1	Jack
Irons	Ironsa	k1gFnPc2	Ironsa
(	(	kIx(	(
<g/>
narozen	narodit	k5eAaPmNgInS	narodit
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dvojice	dvojice	k1gFnSc1	dvojice
založila	založit	k5eAaPmAgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
studenty	student	k1gMnPc7	student
kapelu	kapela	k1gFnSc4	kapela
Another	Anothra	k1gFnPc2	Anothra
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovanou	přejmenovaný	k2eAgFnSc4d1	přejmenovaná
na	na	k7c4	na
Anthem	anthem	k1gInSc4	anthem
<g/>
.	.	kIx.	.
</s>
<s>
Kiedis	Kiedis	k1gFnSc1	Kiedis
a	a	k8xC	a
Flea	Flea	k1gFnSc1	Flea
se	se	k3xPyFc4	se
s	s	k7c7	s
Anthem	anthem	k1gInSc4	anthem
dobře	dobře	k6eAd1	dobře
znali	znát	k5eAaImAgMnP	znát
a	a	k8xC	a
již	již	k6eAd1	již
před	před	k7c7	před
maturitou	maturita	k1gFnSc7	maturita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
občas	občas	k6eAd1	občas
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Anthem	anthem	k1gInSc1	anthem
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dvě	dva	k4xCgFnPc1	dva
kapely	kapela	k1gFnPc1	kapela
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
totožným	totožný	k2eAgNnSc7d1	totožné
složením	složení	k1gNnSc7	složení
-	-	kIx~	-
What	What	k2eAgMnSc1d1	What
Is	Is	k1gMnSc1	Is
This	Thisa	k1gFnPc2	Thisa
<g/>
?	?	kIx.	?
</s>
<s>
a	a	k8xC	a
zárodek	zárodek	k1gInSc1	zárodek
příštích	příští	k2eAgFnPc2d1	příští
RHCP	RHCP	kA	RHCP
často	často	k6eAd1	často
měnící	měnící	k2eAgInPc4d1	měnící
názvy	název	k1gInPc4	název
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1983	[number]	k4	1983
se	se	k3xPyFc4	se
Red	Red	k1gFnSc1	Red
Hot	hot	k0	hot
Chili	Chil	k1gMnPc1	Chil
Peppers	Peppers	k1gInSc4	Peppers
zrodili	zrodit	k5eAaPmAgMnP	zrodit
definitivně	definitivně	k6eAd1	definitivně
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
názvem	název	k1gInSc7	název
podle	podle	k7c2	podle
kvintetu	kvintet	k1gInSc2	kvintet
Louise	Louis	k1gMnSc2	Louis
Armstronga	Armstrong	k1gMnSc2	Armstrong
přišel	přijít	k5eAaPmAgMnS	přijít
Kiedis	Kiedis	k1gInSc4	Kiedis
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
pobočkou	pobočka	k1gFnSc7	pobočka
společnosti	společnost	k1gFnSc2	společnost
EMI	EMI	kA	EMI
zněla	znět	k5eAaImAgFnS	znět
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Slovak	Slovak	k1gMnSc1	Slovak
s	s	k7c7	s
Ironsem	Irons	k1gMnSc7	Irons
začali	začít	k5eAaPmAgMnP	začít
váhat	váhat	k5eAaImF	váhat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
podepsat	podepsat	k5eAaPmF	podepsat
druhou	druhý	k4xOgFnSc4	druhý
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k9	už
jednu	jeden	k4xCgFnSc4	jeden
měli	mít	k5eAaImAgMnP	mít
jako	jako	k8xS	jako
What	What	k1gInSc4	What
is	is	k?	is
this	this	k1gInSc1	this
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
nepodepsali	podepsat	k5eNaPmAgMnP	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
s	s	k7c7	s
prostým	prostý	k2eAgInSc7d1	prostý
názvem	název	k1gInSc7	název
The	The	k1gMnSc2	The
Red	Red	k1gMnSc2	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
natáčelo	natáčet	k5eAaImAgNnS	natáčet
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
kytarista	kytarista	k1gMnSc1	kytarista
byl	být	k5eAaImAgMnS	být
přizván	přizván	k2eAgMnSc1d1	přizván
studiový	studiový	k2eAgMnSc1d1	studiový
hráč	hráč	k1gMnSc1	hráč
Jack	Jack	k1gMnSc1	Jack
Sherman	Sherman	k1gMnSc1	Sherman
a	a	k8xC	a
za	za	k7c2	za
bicí	bicí	k2eAgFnSc2d1	bicí
povolal	povolat	k5eAaPmAgMnS	povolat
Flea	Fleus	k1gMnSc4	Fleus
kamaráda	kamarád	k1gMnSc4	kamarád
Cliffa	Cliff	k1gMnSc4	Cliff
Martineze	Martineze	k1gFnSc2	Martineze
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
byl	být	k5eAaImAgMnS	být
vyhozen	vyhozen	k2eAgMnSc1d1	vyhozen
Sherman	Sherman	k1gMnSc1	Sherman
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
Hillel	Hillel	k1gMnSc1	Hillel
Slovak	Slovak	k1gMnSc1	Slovak
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
se	s	k7c7	s
Slovakem	Slovak	k1gInSc7	Slovak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
další	další	k2eAgFnSc1d1	další
deska	deska	k1gFnSc1	deska
Freaky	Freak	k1gInPc4	Freak
Styley	Stylea	k1gFnSc2	Stylea
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
opět	opět	k6eAd1	opět
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bubeníka	bubeník	k1gMnSc4	bubeník
Cliffa	Cliff	k1gMnSc2	Cliff
Martineze	Martineze	k1gFnSc2	Martineze
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
poslední	poslední	k2eAgInPc4d1	poslední
měsíce	měsíc	k1gInPc4	měsíc
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vzápětí	vzápětí	k6eAd1	vzápětí
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Jackem	Jacek	k1gMnSc7	Jacek
Ironsem	Irons	k1gMnSc7	Irons
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
parta	parta	k1gFnSc1	parta
kamarádů	kamarád	k1gMnPc2	kamarád
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
byla	být	k5eAaImAgFnS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Uplift	Uplift	k2eAgMnSc1d1	Uplift
Mofo	Mofo	k1gMnSc1	Mofo
Party	parta	k1gFnSc2	parta
Plan	plan	k1gInSc1	plan
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
TOP	topit	k5eAaImRp2nS	topit
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
nahrávky	nahrávka	k1gFnPc1	nahrávka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
předělávka	předělávka	k1gFnSc1	předělávka
Hendrixova	Hendrixův	k2eAgNnSc2d1	Hendrixův
Fire	Fire	k1gNnSc2	Fire
<g/>
)	)	kIx)	)
vyšly	vyjít	k5eAaPmAgFnP	vyjít
až	až	k9	až
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
na	na	k7c4	na
The	The	k1gFnSc4	The
Abbey	Abbea	k1gFnSc2	Abbea
Road	Road	k1gInSc1	Road
E.	E.	kA	E.
<g/>
P.	P.	kA	P.
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
první	první	k4xOgNnSc1	první
evropské	evropský	k2eAgNnSc1d1	Evropské
turné	turné	k1gNnSc1	turné
<g/>
.	.	kIx.	.
</s>
<s>
Vyčerpáni	vyčerpán	k2eAgMnPc1d1	vyčerpán
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
individuálně	individuálně	k6eAd1	individuálně
odpočívali	odpočívat	k5eAaImAgMnP	odpočívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
27	[number]	k4	27
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1988	[number]	k4	1988
večer	večer	k6eAd1	večer
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
předávkování	předávkování	k1gNnSc6	předávkování
heroinem	heroin	k1gInSc7	heroin
a	a	k8xC	a
kokainem	kokain	k1gInSc7	kokain
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
Hillel	Hillel	k1gMnSc1	Hillel
Slovak	Slovak	k1gMnSc1	Slovak
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
tragédie	tragédie	k1gFnSc2	tragédie
soubor	soubor	k1gInSc1	soubor
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
přestal	přestat	k5eAaPmAgMnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gInSc1	Jack
Irons	Irons	k1gInSc1	Irons
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgMnS	dát
rodiči	rodič	k1gMnPc7	rodič
na	na	k7c6	na
krátké	krátká	k1gFnSc6	krátká
psychiatrické	psychiatrický	k2eAgNnSc4d1	psychiatrické
léčení	léčení	k1gNnSc4	léčení
a	a	k8xC	a
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
přece	přece	k9	přece
jenom	jenom	k9	jenom
znovu	znovu	k6eAd1	znovu
dala	dát	k5eAaPmAgFnS	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
definitivního	definitivní	k2eAgNnSc2d1	definitivní
obsazení	obsazení	k1gNnSc2	obsazení
byl	být	k5eAaImAgMnS	být
přijat	přijat	k2eAgMnSc1d1	přijat
mladičký	mladičký	k2eAgMnSc1d1	mladičký
fanoušek	fanoušek	k1gMnSc1	fanoušek
kapely	kapela	k1gFnSc2	kapela
John	John	k1gMnSc1	John
Frusciante	Frusciant	k1gMnSc5	Frusciant
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jim	on	k3xPp3gMnPc3	on
svými	svůj	k3xOyFgInPc7	svůj
způsoby	způsob	k1gInPc7	způsob
a	a	k8xC	a
hrou	hra	k1gFnSc7	hra
připomínal	připomínat	k5eAaImAgMnS	připomínat
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
Slovaka	Slovak	k1gMnSc4	Slovak
<g/>
.	.	kIx.	.
</s>
<s>
Lákalo	lákat	k5eAaImAgNnS	lákat
je	on	k3xPp3gNnSc4	on
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nevystoupil	vystoupit	k5eNaPmAgMnS	vystoupit
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
kapelou	kapela	k1gFnSc7	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
post	post	k1gInSc4	post
bubeníka	bubeník	k1gMnSc2	bubeník
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
přijat	přijat	k2eAgMnSc1d1	přijat
Chad	Chad	k1gMnSc1	Chad
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
Minnesota	Minnesota	k1gFnSc1	Minnesota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
složení	složení	k1gNnSc6	složení
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
názvem	název	k1gInSc7	název
Mother	Mothra	k1gFnPc2	Mothra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Milk	Milka	k1gFnPc2	Milka
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgFnSc4	první
zlatou	zlatá	k1gFnSc4	zlatá
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
vzbudily	vzbudit	k5eAaPmAgInP	vzbudit
i	i	k8xC	i
oba	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
-	-	kIx~	-
památce	památka	k1gFnSc6	památka
Hillela	Hillela	k1gFnPc2	Hillela
Slovaka	Slovak	k1gMnSc2	Slovak
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
inspirované	inspirovaný	k2eAgNnSc1d1	inspirované
Knock	Knock	k1gInSc4	Knock
Me	Me	k1gMnSc1	Me
Down	Down	k1gMnSc1	Down
a	a	k8xC	a
předělávka	předělávka	k1gFnSc1	předělávka
písně	píseň	k1gFnSc2	píseň
Stevieho	Stevie	k1gMnSc2	Stevie
Wondera	Wonder	k1gMnSc2	Wonder
Higher	Highra	k1gFnPc2	Highra
Ground	Ground	k1gMnSc1	Ground
<g/>
.	.	kIx.	.
</s>
<s>
RHCP	RHCP	kA	RHCP
prorazili	prorazit	k5eAaPmAgMnP	prorazit
i	i	k9	i
komerčně	komerčně	k6eAd1	komerčně
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
úctu	úcta	k1gFnSc4	úcta
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
trvalo	trvat	k5eAaImAgNnS	trvat
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
světové	světový	k2eAgNnSc1d1	světové
turné	turné	k1gNnSc1	turné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
personální	personální	k2eAgFnSc6d1	personální
stabilizaci	stabilizace	k1gFnSc6	stabilizace
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
chystala	chystat	k5eAaImAgFnS	chystat
natočit	natočit	k5eAaBmF	natočit
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
vrcholné	vrcholný	k2eAgNnSc1d1	vrcholné
album	album	k1gNnSc1	album
RHCP	RHCP	kA	RHCP
Blood	Blood	k1gInSc1	Blood
Sugar	Sugar	k1gInSc1	Sugar
Sex	sex	k1gInSc1	sex
Magik	magika	k1gFnPc2	magika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
podle	podle	k7c2	podle
mnohých	mnohý	k2eAgFnPc2d1	mnohá
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dodnes	dodnes	k6eAd1	dodnes
nepřekonaným	překonaný	k2eNgNnSc7d1	nepřekonané
dílem	dílo	k1gNnSc7	dílo
této	tento	k3xDgFnSc2	tento
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
až	až	k9	až
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
albového	albový	k2eAgInSc2d1	albový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
platinová	platinový	k2eAgFnSc1d1	platinová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
pochází	pocházet	k5eAaImIp3nS	pocházet
největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
RHCP	RHCP	kA	RHCP
<g/>
:	:	kIx,	:
Give	Giv	k1gMnSc2	Giv
It	It	k1gMnSc2	It
Away	Awaa	k1gMnSc2	Awaa
<g/>
,	,	kIx,	,
Under	Under	k1gInSc1	Under
the	the	k?	the
Bridge	Bridge	k1gInSc1	Bridge
<g/>
,	,	kIx,	,
Suck	Suck	k1gInSc1	Suck
My	my	k3xPp1nPc1	my
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
na	na	k7c6	na
Hillela	Hillela	k1gFnSc6	Hillela
je	být	k5eAaImIp3nS	být
My	my	k3xPp1nPc1	my
Lovely	Lovela	k1gFnPc4	Lovela
Man	Man	k1gMnSc1	Man
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
různá	různý	k2eAgNnPc4d1	různé
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
MTV	MTV	kA	MTV
pro	pro	k7c4	pro
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hardrockovou	hardrockový	k2eAgFnSc4d1	hardrocková
zpívanou	zpívaný	k2eAgFnSc4d1	zpívaná
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1993	[number]	k4	1993
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zpívaný	zpívaný	k2eAgInSc4d1	zpívaný
hardrockový	hardrockový	k2eAgInSc4d1	hardrockový
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchy	úspěch	k1gInPc4	úspěch
podpořili	podpořit	k5eAaPmAgMnP	podpořit
dalším	další	k2eAgNnSc7d1	další
koncertováním	koncertování	k1gNnSc7	koncertování
<g/>
,	,	kIx,	,
předkapelu	předkapela	k1gFnSc4	předkapela
jim	on	k3xPp3gMnPc3	on
dělala	dělat	k5eAaImAgNnP	dělat
např.	např.	kA	např.
Nirvana	Nirvan	k1gMnSc2	Nirvan
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
album	album	k1gNnSc1	album
Nevermind	Neverminda	k1gFnPc2	Neverminda
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c4	v
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
jako	jako	k8xS	jako
Blood	Blood	k1gInSc4	Blood
Sugar	Sugar	k1gInSc1	Sugar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
oznámil	oznámit	k5eAaPmAgMnS	oznámit
své	svůj	k3xOyFgNnSc4	svůj
pevné	pevný	k2eAgNnSc4d1	pevné
<g/>
,	,	kIx,	,
nezměnitelné	změnitelný	k2eNgNnSc4d1	nezměnitelné
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
John	John	k1gMnSc1	John
Frusciante	Frusciant	k1gMnSc5	Frusciant
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
se	se	k3xPyFc4	se
přestávala	přestávat	k5eAaImAgFnS	přestávat
snášet	snášet	k5eAaImF	snášet
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
popularitou	popularita	k1gFnSc7	popularita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
natočil	natočit	k5eAaBmAgMnS	natočit
dvě	dva	k4xCgFnPc4	dva
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
komerčně	komerčně	k6eAd1	komerčně
neúspěšná	úspěšný	k2eNgNnPc1d1	neúspěšné
sólová	sólový	k2eAgNnPc1d1	sólové
alba	album	k1gNnPc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
2	[number]	k4	2
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
na	na	k7c6	na
postu	post	k1gInSc6	post
kytaristy	kytarista	k1gMnSc2	kytarista
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
málo	málo	k6eAd1	málo
známí	známý	k2eAgMnPc1d1	známý
Arik	Arik	k1gMnSc1	Arik
Marshall	Marshall	k1gMnSc1	Marshall
<g/>
,	,	kIx,	,
Zander	Zander	k1gMnSc1	Zander
Schloss	Schloss	k1gInSc1	Schloss
a	a	k8xC	a
Jesse	Jesse	k1gFnSc1	Jesse
Tobias	Tobiasa	k1gFnPc2	Tobiasa
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
vydržel	vydržet	k5eAaPmAgMnS	vydržet
pouhý	pouhý	k2eAgInSc4d1	pouhý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
RHCP	RHCP	kA	RHCP
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
jednat	jednat	k5eAaImF	jednat
i	i	k9	i
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
dřívějším	dřívější	k2eAgMnSc7d1	dřívější
kytaristou	kytarista	k1gMnSc7	kytarista
Jackem	Jacek	k1gMnSc7	Jacek
Shermanem	Sherman	k1gMnSc7	Sherman
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInPc4	jeho
požadavky	požadavek	k1gInPc4	požadavek
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
akceptovat	akceptovat	k5eAaBmF	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
přemluvili	přemluvit	k5eAaPmAgMnP	přemluvit
Dava	Davus	k1gMnSc4	Davus
Navarra	Navarra	k1gFnSc1	Navarra
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Santa	Santa	k1gFnSc1	Santa
Monica	Monica	k1gFnSc1	Monica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohokrát	mnohokrát	k6eAd1	mnohokrát
odkládané	odkládaný	k2eAgNnSc1d1	odkládané
album	album	k1gNnSc1	album
One	One	k1gFnSc2	One
Hot	hot	k0	hot
Minute	Minut	k1gInSc5	Minut
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Flea	Flea	k1gFnSc1	Flea
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
dostal	dostat	k5eAaPmAgMnS	dostat
ocenění	ocenění	k1gNnSc4	ocenění
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
baskytarista	baskytarista	k1gMnSc1	baskytarista
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
znělo	znět	k5eAaImAgNnS	znět
více	hodně	k6eAd2	hodně
metalově	metalově	k6eAd1	metalově
než	než	k8xS	než
funkově	funkově	k6eAd1	funkově
<g/>
,	,	kIx,	,
dalšího	další	k2eAgInSc2d1	další
výraznějšího	výrazný	k2eAgInSc2d2	výraznější
úspěchu	úspěch	k1gInSc2	úspěch
se	se	k3xPyFc4	se
však	však	k9	však
nedočkalo	dočkat	k5eNaPmAgNnS	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
Odřeknuté	odřeknutý	k2eAgInPc1d1	odřeknutý
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc1d2	veliký
drogová	drogový	k2eAgFnSc1d1	drogová
závislost	závislost	k1gFnSc1	závislost
<g/>
,	,	kIx,	,
motocyklové	motocyklový	k2eAgFnPc1d1	motocyklová
nehody	nehoda	k1gFnPc1	nehoda
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
odchod	odchod	k1gInSc1	odchod
Navarra	Navarra	k1gFnSc1	Navarra
přivedly	přivést	k5eAaPmAgInP	přivést
kapelu	kapela	k1gFnSc4	kapela
na	na	k7c4	na
samý	samý	k3xTgInSc4	samý
pokraj	pokraj	k1gInSc4	pokraj
definitivního	definitivní	k2eAgInSc2d1	definitivní
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Zabojovat	zabojovat	k5eAaPmF	zabojovat
musel	muset	k5eAaImAgMnS	muset
opět	opět	k6eAd1	opět
neúnavný	únavný	k2eNgMnSc1d1	neúnavný
Flea	Flea	k1gMnSc1	Flea
<g/>
.	.	kIx.	.
</s>
<s>
Dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
se	se	k3xPyFc4	se
s	s	k7c7	s
vyléčeným	vyléčený	k2eAgMnSc7d1	vyléčený
Frusciantem	Frusciant	k1gMnSc7	Frusciant
a	a	k8xC	a
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Kiedise	Kiedise	k1gFnPc4	Kiedise
o	o	k7c6	o
nezbytnosti	nezbytnost	k1gFnSc6	nezbytnost
skutečného	skutečný	k2eAgNnSc2d1	skutečné
zbavení	zbavení	k1gNnSc2	zbavení
se	se	k3xPyFc4	se
závislosti	závislost	k1gFnSc3	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Californication	Californication	k1gInSc1	Californication
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
dobrým	dobrý	k2eAgInSc7d1	dobrý
startem	start	k1gInSc7	start
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
hudební	hudební	k2eAgFnSc2d1	hudební
kariéry	kariéra	k1gFnSc2	kariéra
této	tento	k3xDgFnSc2	tento
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Kiedis	Kiedis	k1gFnSc1	Kiedis
zpívá	zpívat	k5eAaImIp3nS	zpívat
civilněji	civilně	k6eAd2	civilně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
šetřil	šetřit	k5eAaImAgMnS	šetřit
hlasivky	hlasivka	k1gFnPc4	hlasivka
<g/>
,	,	kIx,	,
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
sborů	sbor	k1gInPc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
hitů	hit	k1gInPc2	hit
<g/>
:	:	kIx,	:
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
Scar	Scar	k1gMnSc1	Scar
Tissue	Tissu	k1gFnSc2	Tissu
či	či	k8xC	či
Otherside	Othersid	k1gInSc5	Othersid
nebo	nebo	k8xC	nebo
svižnější	svižný	k2eAgMnSc1d2	svižnější
Around	Around	k1gMnSc1	Around
The	The	k1gMnSc1	The
World	World	k1gMnSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
skupiny	skupina	k1gFnSc2	skupina
byl	být	k5eAaImAgInS	být
pečlivě	pečlivě	k6eAd1	pečlivě
zajištěn	zajistit	k5eAaPmNgInS	zajistit
propagačními	propagační	k2eAgInPc7d1	propagační
koncerty	koncert	k1gInPc7	koncert
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
světa	svět	k1gInSc2	svět
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
rozhovory	rozhovor	k1gInPc7	rozhovor
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
skvělé	skvělý	k2eAgNnSc1d1	skvělé
album	album	k1gNnSc1	album
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jím	on	k3xPp3gNnSc7	on
By	by	k9	by
the	the	k?	the
Way	Way	k1gFnSc1	Way
-	-	kIx~	-
návrat	návrat	k1gInSc1	návrat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
podařil	podařit	k5eAaPmAgInS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
deskou	deska	k1gFnSc7	deska
je	být	k5eAaImIp3nS	být
Stadium	stadium	k1gNnSc1	stadium
Arcadium	Arcadium	k1gNnSc1	Arcadium
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
obrovská	obrovský	k2eAgFnSc1d1	obrovská
vyzrálost	vyzrálost	k1gFnSc1	vyzrálost
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
čtveřice	čtveřice	k1gFnSc2	čtveřice
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
hity	hit	k1gInPc7	hit
(	(	kIx(	(
<g/>
Dani	daň	k1gFnSc3	daň
California	Californium	k1gNnSc2	Californium
<g/>
,	,	kIx,	,
Snow	Snow	k1gMnSc1	Snow
(	(	kIx(	(
<g/>
Hey	Hey	k1gMnSc1	Hey
Oh	oh	k0	oh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hump	Hump	k1gMnSc1	Hump
De	De	k?	De
Bump	Bump	k1gMnSc1	Bump
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
zaplnili	zaplnit	k5eAaPmAgMnP	zaplnit
rádia	rádio	k1gNnSc2	rádio
a	a	k8xC	a
vysílání	vysílání	k1gNnSc2	vysílání
hudebních	hudební	k2eAgFnPc2d1	hudební
televizí	televize	k1gFnPc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
propagační	propagační	k2eAgFnSc2d1	propagační
šňůry	šňůra	k1gFnSc2	šňůra
evropských	evropský	k2eAgInPc2d1	evropský
koncertů	koncert	k1gInPc2	koncert
zamířili	zamířit	k5eAaPmAgMnP	zamířit
14	[number]	k4	14
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
i	i	k8xC	i
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
do	do	k7c2	do
pražské	pražský	k2eAgFnSc2d1	Pražská
Sazka	Sazka	k1gFnSc1	Sazka
Arény	aréna	k1gFnSc2	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
teprve	teprve	k6eAd1	teprve
jejich	jejich	k3xOp3gNnSc1	jejich
druhé	druhý	k4xOgNnSc1	druhý
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zavítali	zavítat	k5eAaPmAgMnP	zavítat
zde	zde	k6eAd1	zde
však	však	k9	však
poprvé	poprvé	k6eAd1	poprvé
i	i	k9	i
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Frusciantem	Frusciant	k1gMnSc7	Frusciant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
Frusciante	Frusciant	k1gMnSc5	Frusciant
z	z	k7c2	z
RHCP	RHCP	kA	RHCP
podruhé	podruhé	k6eAd1	podruhé
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
a	a	k8xC	a
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
na	na	k7c6	na
jeho	jeho	k3xOp3gNnPc6	jeho
sólových	sólový	k2eAgNnPc6d1	sólové
albech	album	k1gNnPc6	album
Josh	Josh	k1gInSc4	Josh
Klinghoffer	Klinghoffer	k1gInSc4	Klinghoffer
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
doprovodný	doprovodný	k2eAgMnSc1d1	doprovodný
kytarista	kytarista	k1gMnSc1	kytarista
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
turné	turné	k1gNnSc4	turné
Stadium	stadium	k1gNnSc1	stadium
Arcadium	Arcadium	k1gNnSc4	Arcadium
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
with	with	k1gMnSc1	with
You	You	k1gMnSc1	You
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
fanoušky	fanoušek	k1gMnPc4	fanoušek
přijato	přijmout	k5eAaPmNgNnS	přijmout
poněkud	poněkud	k6eAd1	poněkud
vlažně	vlažně	k6eAd1	vlažně
a	a	k8xC	a
nenavázalo	navázat	k5eNaPmAgNnS	navázat
tak	tak	k9	tak
na	na	k7c4	na
úspěchy	úspěch	k1gInPc4	úspěch
předchozích	předchozí	k2eAgNnPc2d1	předchozí
několika	několik	k4yIc2	několik
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc1	první
studiová	studiový	k2eAgFnSc1d1	studiová
nahrávka	nahrávka	k1gFnSc1	nahrávka
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
kytaristou	kytarista	k1gMnSc7	kytarista
nepatřila	patřit	k5eNaImAgFnS	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgNnPc3d3	nejlepší
<g/>
,	,	kIx,	,
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
skupina	skupina	k1gFnSc1	skupina
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2011	[number]	k4	2011
až	až	k8xS	až
2014	[number]	k4	2014
odehrála	odehrát	k5eAaPmAgFnS	odehrát
téměř	téměř	k6eAd1	téměř
160	[number]	k4	160
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
zároveň	zároveň	k6eAd1	zároveň
o	o	k7c4	o
nejdelší	dlouhý	k2eAgNnSc4d3	nejdelší
turné	turné	k1gNnSc4	turné
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
propagaci	propagace	k1gFnSc4	propagace
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
alb	alba	k1gFnPc2	alba
Red	Red	k1gFnSc2	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
poté	poté	k6eAd1	poté
navštivila	navštivit	k5eAaPmAgFnS	navštivit
tato	tento	k3xDgFnSc1	tento
čtveřice	čtveřice	k1gFnSc1	čtveřice
i	i	k9	i
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgNnSc4d1	úvodní
slovo	slovo	k1gNnSc4	slovo
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
přednesl	přednést	k5eAaPmAgInS	přednést
Chris	Chris	k1gFnSc2	Chris
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
začalo	začít	k5eAaPmAgNnS	začít
nahrávání	nahrávání	k1gNnSc1	nahrávání
nově	nově	k6eAd1	nově
napsaného	napsaný	k2eAgNnSc2d1	napsané
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
pozastaveno	pozastavit	k5eAaPmNgNnS	pozastavit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
baskytarista	baskytarista	k1gMnSc1	baskytarista
Flea	Flea	k1gMnSc1	Flea
nešťastně	šťastně	k6eNd1	šťastně
zlomil	zlomit	k5eAaPmAgMnS	zlomit
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
skupina	skupina	k1gFnSc1	skupina
uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Circle	Circle	k1gFnSc1	Circle
of	of	k?	of
the	the	k?	the
Noose	Noose	k1gFnSc2	Noose
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nahrála	nahrát	k5eAaPmAgFnS	nahrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
ještě	ještě	k6eAd1	ještě
s	s	k7c7	s
Davem	Dav	k1gInSc7	Dav
Navarrem	Navarrma	k1gFnPc2	Navarrma
jako	jako	k8xC	jako
poctu	pocta	k1gFnSc4	pocta
hudebníkovi	hudebníkův	k2eAgMnPc1d1	hudebníkův
Nusrat	Nusrat	k1gMnSc1	Nusrat
Fateh	Fateh	k1gMnSc1	Fateh
Ali	Ali	k1gMnSc3	Ali
Khanovi	Khan	k1gMnSc3	Khan
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
singl	singl	k1gInSc1	singl
Dark	Darka	k1gFnPc2	Darka
Necessities	Necessitiesa	k1gFnPc2	Necessitiesa
následovaný	následovaný	k2eAgInSc1d1	následovaný
dvěma	dva	k4xCgFnPc7	dva
dalšími	další	k2eAgFnPc7d1	další
písněmi	píseň	k1gFnPc7	píseň
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
pak	pak	k6eAd1	pak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Gataway	Gataway	k1gInPc4	Gataway
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
hostuje	hostovat	k5eAaImIp3nS	hostovat
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
mj.	mj.	kA	mj.
i	i	k8xC	i
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
The	The	k1gMnSc1	The
Getaway	Getawaa	k1gFnSc2	Getawaa
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
poté	poté	k6eAd1	poté
kapela	kapela	k1gFnSc1	kapela
opět	opět	k6eAd1	opět
zavítala	zavítat	k5eAaPmAgFnS	zavítat
4	[number]	k4	4
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
i	i	k8xC	i
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Anthony	Anthon	k1gInPc1	Anthon
Kiedis	Kiedis	k1gFnSc2	Kiedis
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
Balzary	Balzara	k1gFnSc2	Balzara
(	(	kIx(	(
<g/>
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
'	'	kIx"	'
<g/>
Flea	Flea	k1gFnSc1	Flea
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
-	-	kIx~	-
basa	basa	k1gFnSc1	basa
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Chad	Chad	k1gMnSc1	Chad
Smith	Smith	k1gMnSc1	Smith
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Josh	Josh	k1gMnSc1	Josh
Klinghoffer	Klinghoffer	k1gMnSc1	Klinghoffer
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Hillel	Hillel	k1gMnSc1	Hillel
Slovak	Slovak	k1gMnSc1	Slovak
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
;	;	kIx,	;
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Jack	Jack	k1gMnSc1	Jack
Sherman	Sherman	k1gMnSc1	Sherman
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Dwayne	Dwayn	k1gInSc5	Dwayn
'	'	kIx"	'
<g/>
Blackbyrd	Blackbyrd	k1gMnSc1	Blackbyrd
<g/>
'	'	kIx"	'
McKnight	McKnight	k1gMnSc1	McKnight
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Arik	Arik	k1gMnSc1	Arik
Marshall	Marshall	k1gMnSc1	Marshall
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Jesse	Jesse	k1gFnSc1	Jesse
Tobias	Tobias	k1gMnSc1	Tobias
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Navarro	Navarra	k1gFnSc5	Navarra
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Frusciante	Frusciant	k1gMnSc5	Frusciant
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
;	;	kIx,	;
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Jack	Jack	k1gInSc1	Jack
Irons	Irons	k1gInSc1	Irons
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
;	;	kIx,	;
1986	[number]	k4	1986
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Cliff	Cliff	k1gMnSc1	Cliff
Martínez	Martínez	k1gMnSc1	Martínez
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
D.H.	D.H.	k1gFnSc6	D.H.
Peligro	Peligro	k1gNnSc4	Peligro
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Red	Red	k1gFnSc1	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Freaky	Freak	k1gInPc1	Freak
Styley	Stylea	k1gFnSc2	Stylea
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Uplift	Uplift	k2eAgMnSc1d1	Uplift
Mofo	Mofo	k1gMnSc1	Mofo
Party	parta	k1gFnSc2	parta
Plan	plan	k1gInSc1	plan
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Abbey	Abbea	k1gFnSc2	Abbea
Road	Road	k1gMnSc1	Road
E.	E.	kA	E.
<g/>
P.	P.	kA	P.
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Mother	Mothra	k1gFnPc2	Mothra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Milk	Milka	k1gFnPc2	Milka
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Blood	Blood	k1gInSc1	Blood
Sugar	Sugara	k1gFnPc2	Sugara
Sex	sex	k1gInSc1	sex
Magik	magik	k1gMnSc1	magik
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
What	What	k2eAgInSc1d1	What
Hits	Hits	k1gInSc1	Hits
<g/>
!?	!?	k?	!?
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Out	Out	k1gMnSc1	Out
in	in	k?	in
L.A.	L.A.	k1gMnSc1	L.A.
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
One	One	k1gMnSc1	One
Hot	hot	k0	hot
Minute	Minut	k1gInSc5	Minut
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Under	Under	k1gInSc1	Under
The	The	k1gMnSc1	The
Covers	Covers	k1gInSc1	Covers
<g/>
:	:	kIx,	:
Essential	Essential	k1gInSc1	Essential
RHCP	RHCP	kA	RHCP
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Californication	Californication	k1gInSc1	Californication
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
By	by	k9	by
the	the	k?	the
Way	Way	k1gFnSc1	Way
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
at	at	k?	at
Slane	Slan	k1gInSc5	Slan
Castle	Castle	k1gFnPc1	Castle
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Red	Red	k1gFnSc1	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc4	Chile
Peppers	Peppers	k1gInSc1	Peppers
Greatest	Greatest	k1gMnSc1	Greatest
Hits	Hitsa	k1gFnPc2	Hitsa
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Hyde	Hyde	k1gInSc1	Hyde
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
2	[number]	k4	2
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Stadium	stadium	k1gNnSc1	stadium
Arcadium	Arcadium	k1gNnSc1	Arcadium
(	(	kIx(	(
<g/>
2	[number]	k4	2
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
With	With	k1gMnSc1	With
You	You	k1gMnSc1	You
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Rock	rock	k1gInSc1	rock
&	&	k?	&
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
Fame	Fame	k6eAd1	Fame
Covers	Covers	k1gInSc1	Covers
EP	EP	kA	EP
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
The	The	k1gFnSc2	The
Getaway	Getawaa	k1gFnSc2	Getawaa
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
"	"	kIx"	"
<g/>
Behind	Behind	k1gInSc1	Behind
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
"	"	kIx"	"
<g/>
Fight	Fight	k2eAgInSc1d1	Fight
Like	Like	k1gInSc1	Like
a	a	k8xC	a
Brave	brav	k1gInSc5	brav
<g/>
"	"	kIx"	"
1989	[number]	k4	1989
"	"	kIx"	"
<g/>
Higher	Highra	k1gFnPc2	Highra
Ground	Ground	k1gInSc1	Ground
<g/>
"	"	kIx"	"
1989	[number]	k4	1989
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Knock	Knock	k1gMnSc1	Knock
Me	Me	k1gMnSc1	Me
Down	Down	k1gMnSc1	Down
<g/>
"	"	kIx"	"
1990	[number]	k4	1990
"	"	kIx"	"
<g/>
Show	show	k1gNnSc2	show
Me	Me	k1gFnSc2	Me
Your	Your	k1gMnSc1	Your
Soul	Soul	k1gInSc1	Soul
<g/>
"	"	kIx"	"
1992	[number]	k4	1992
"	"	kIx"	"
<g/>
Give	Give	k1gInSc1	Give
It	It	k1gMnSc2	It
Away	Awaa	k1gMnSc2	Awaa
<g/>
"	"	kIx"	"
1992	[number]	k4	1992
"	"	kIx"	"
<g/>
Under	Under	k1gInSc1	Under
the	the	k?	the
Bridge	Bridge	k1gInSc1	Bridge
<g/>
"	"	kIx"	"
1992	[number]	k4	1992
"	"	kIx"	"
<g/>
Breaking	Breaking	k1gInSc1	Breaking
the	the	k?	the
Girl	girl	k1gFnSc2	girl
<g/>
"	"	kIx"	"
1993	[number]	k4	1993
"	"	kIx"	"
<g/>
Suck	Suck	k1gInSc1	Suck
My	my	k3xPp1nPc1	my
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
"	"	kIx"	"
1994	[number]	k4	1994
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Soul	Soul	k1gInSc1	Soul
to	ten	k3xDgNnSc1	ten
Squeeze	Squeeze	k1gFnPc1	Squeeze
<g/>
"	"	kIx"	"
1995	[number]	k4	1995
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Friends	Friendsa	k1gFnPc2	Friendsa
<g/>
"	"	kIx"	"
1996	[number]	k4	1996
"	"	kIx"	"
<g/>
Warped	Warped	k1gInSc1	Warped
<g/>
"	"	kIx"	"
1996	[number]	k4	1996
"	"	kIx"	"
<g/>
Aeroplane	Aeroplan	k1gMnSc5	Aeroplan
<g/>
"	"	kIx"	"
1996	[number]	k4	1996
"	"	kIx"	"
<g/>
Coffee	Coffe	k1gInSc2	Coffe
Shop	shop	k1gInSc1	shop
<g/>
"	"	kIx"	"
1999	[number]	k4	1999
"	"	kIx"	"
<g/>
Scar	Scar	k1gInSc1	Scar
Tissue	Tissu	k1gInSc2	Tissu
<g/>
"	"	kIx"	"
1999	[number]	k4	1999
"	"	kIx"	"
<g/>
Around	Around	k1gInSc1	Around
the	the	k?	the
World	World	k1gInSc1	World
<g/>
"	"	kIx"	"
2000	[number]	k4	2000
"	"	kIx"	"
<g/>
Otherside	Othersid	k1gInSc5	Othersid
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
2000	[number]	k4	2000
"	"	kIx"	"
<g/>
Californication	Californication	k1gInSc1	Californication
<g/>
"	"	kIx"	"
2001	[number]	k4	2001
"	"	kIx"	"
<g/>
Parallel	Parallel	k1gFnSc1	Parallel
Universe	Universe	k1gFnSc1	Universe
<g/>
"	"	kIx"	"
2002	[number]	k4	2002
"	"	kIx"	"
<g/>
By	by	k9	by
the	the	k?	the
Way	Way	k1gFnSc1	Way
<g/>
"	"	kIx"	"
2002	[number]	k4	2002
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Zephyr	Zephyr	k1gMnSc1	Zephyr
Song	song	k1gInSc1	song
<g/>
"	"	kIx"	"
2003	[number]	k4	2003
"	"	kIx"	"
<g/>
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Stop	stop	k1gInSc1	stop
<g/>
"	"	kIx"	"
2003	[number]	k4	2003
"	"	kIx"	"
<g/>
Dosed	dosed	k1gInSc1	dosed
<g/>
"	"	kIx"	"
2003	[number]	k4	2003
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Universally	Universalla	k1gFnPc1	Universalla
Speaking	Speaking	k1gInSc1	Speaking
<g/>
"	"	kIx"	"
2004	[number]	k4	2004
"	"	kIx"	"
<g/>
Fortune	Fortun	k1gInSc5	Fortun
Faded	Faded	k1gMnSc1	Faded
<g/>
"	"	kIx"	"
2006	[number]	k4	2006
"	"	kIx"	"
<g/>
Dani	daň	k1gFnSc3	daň
California	Californium	k1gNnSc2	Californium
<g/>
"	"	kIx"	"
2006	[number]	k4	2006
"	"	kIx"	"
<g/>
Tell	Tell	k1gInSc1	Tell
Me	Me	k1gFnSc2	Me
Baby	baba	k1gFnSc2	baba
<g/>
"	"	kIx"	"
2006	[number]	k4	2006
"	"	kIx"	"
<g/>
Snow	Snow	k1gMnSc1	Snow
(	(	kIx(	(
<g/>
Hey	Hey	k1gMnSc1	Hey
Oh	oh	k0	oh
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
2007	[number]	k4	2007
"	"	kIx"	"
<g/>
Desecration	Desecration	k1gInSc1	Desecration
Smile	smil	k1gInSc5	smil
<g/>
"	"	kIx"	"
2007	[number]	k4	2007
"	"	kIx"	"
<g/>
Hump	Hump	k1gInSc1	Hump
<g />
.	.	kIx.	.
</s>
<s>
de	de	k?	de
Bump	Bump	k1gInSc1	Bump
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Adventures	Adventures	k1gMnSc1	Adventures
Of	Of	k1gMnSc1	Of
Rain	Rain	k1gNnSc4	Rain
Dance	Danka	k1gFnSc3	Danka
Maggie	Maggie	k1gFnSc2	Maggie
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
"	"	kIx"	"
<g/>
Monarchy	monarcha	k1gMnSc2	monarcha
of	of	k?	of
roses	roses	k1gMnSc1	roses
<g/>
"	"	kIx"	"
2011	[number]	k4	2011
"	"	kIx"	"
<g/>
Look	Look	k1gMnSc1	Look
around	around	k1gMnSc1	around
"	"	kIx"	"
2011	[number]	k4	2011
"	"	kIx"	"
<g/>
Brendan	Brendan	k1gInSc1	Brendan
s	s	k7c7	s
death	death	k1gMnSc1	death
song	song	k1gInSc1	song
<g/>
"	"	kIx"	"
2016	[number]	k4	2016
"	"	kIx"	"
<g/>
Dark	Dark	k1gMnSc1	Dark
Necessities	Necessities	k1gMnSc1	Necessities
<g/>
"	"	kIx"	"
2016	[number]	k4	2016
"	"	kIx"	"
<g/>
The	The	k1gFnSc2	The
Getaway	Getawaa	k1gFnSc2	Getawaa
<g/>
"	"	kIx"	"
</s>
