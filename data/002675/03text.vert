<s>
Svratka	Svratka	k1gFnSc1	Svratka
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Schwarza	Schwarza	k?	Schwarza
<g/>
(	(	kIx(	(
<g/>
ch	ch	k0	ch
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Schwarzawa	Schwarzawa	k1gFnSc1	Schwarzawa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
přítok	přítok	k1gInSc1	přítok
Dyje	Dyje	k1gFnSc2	Dyje
a	a	k8xC	a
hlavní	hlavní	k2eAgInSc1d1	hlavní
tok	tok	k1gInSc1	tok
procházející	procházející	k2eAgFnSc2d1	procházející
městem	město	k1gNnSc7	město
Brnem	Brno	k1gNnSc7	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
postupně	postupně	k6eAd1	postupně
okresy	okres	k1gInPc1	okres
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
,	,	kIx,	,
Brno-město	Brnoěsta	k1gFnSc5	Brno-města
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
a	a	k8xC	a
Břeclav	Břeclav	k1gFnSc1	Břeclav
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
173,9	[number]	k4	173,9
km	km	kA	km
<g/>
,	,	kIx,	,
plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
činí	činit	k5eAaImIp3nS	činit
7112,79	[number]	k4	7112,79
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Vysočině	vysočina	k1gFnSc6	vysočina
mezi	mezi	k7c7	mezi
Cikhájí	Cikháje	k1gFnSc7	Cikháje
a	a	k8xC	a
Jimramovem	Jimramov	k1gInSc7	Jimramov
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
nebo	nebo	k8xC	nebo
její	její	k3xOp3gInPc1	její
břehy	břeh	k1gInPc1	břeh
části	část	k1gFnSc2	část
historické	historický	k2eAgFnSc2d1	historická
zemské	zemský	k2eAgFnSc2d1	zemská
hranice	hranice	k1gFnSc2	hranice
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
na	na	k7c6	na
řadě	řad	k1gInSc6	řad
míst	místo	k1gNnPc2	místo
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zemská	zemský	k2eAgFnSc1d1	zemská
hranice	hranice	k1gFnSc1	hranice
od	od	k7c2	od
říčního	říční	k2eAgNnSc2d1	říční
koryta	koryto	k1gNnSc2	koryto
drobně	drobně	k6eAd1	drobně
odchyluje	odchylovat	k5eAaImIp3nS	odchylovat
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Svratky	Svratka	k1gFnSc2	Svratka
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
staročeského	staročeský	k2eAgNnSc2d1	staročeské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
svorti	svort	k1gMnPc1	svort
<g/>
"	"	kIx"	"
-	-	kIx~	-
vinout	vinout	k5eAaImF	vinout
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
také	také	k9	také
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
německého	německý	k2eAgNnSc2d1	německé
pojmenování	pojmenování	k1gNnSc2	pojmenování
<g/>
,	,	kIx,	,
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Švarcava	Švarcava	k1gFnSc1	Švarcava
<g/>
.	.	kIx.	.
</s>
<s>
Svratka	Svratka	k1gFnSc1	Svratka
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c4	na
úbočí	úbočí	k1gNnPc4	úbočí
Křivého	křivý	k2eAgInSc2d1	křivý
javoru	javor	k1gInSc2	javor
a	a	k8xC	a
Žákovy	Žákův	k2eAgFnSc2d1	Žákova
hory	hora	k1gFnSc2	hora
ve	v	k7c6	v
Žďárských	Žďárských	k2eAgInPc6d1	Žďárských
vrších	vrch	k1gInPc6	vrch
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
významný	významný	k2eAgInSc1d1	významný
pramen	pramen	k1gInSc1	pramen
vzniká	vznikat	k5eAaImIp3nS	vznikat
u	u	k7c2	u
hájovny	hájovna	k1gFnSc2	hájovna
Blatky	blatka	k1gFnSc2	blatka
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
značen	značen	k2eAgInSc1d1	značen
jako	jako	k8xS	jako
Břimovka	Břimovka	k1gFnSc1	Břimovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
pak	pak	k6eAd1	pak
zhruba	zhruba	k6eAd1	zhruba
jihojihovýchodním	jihojihovýchodní	k2eAgInSc7d1	jihojihovýchodní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
Hornosvrateckou	Hornosvratecký	k2eAgFnSc4d1	Hornosvratecká
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
ní	on	k3xPp3gFnSc7	on
vybudována	vybudován	k2eAgFnSc1d1	vybudována
soustava	soustava	k1gFnSc1	soustava
přehrad	přehrada	k1gFnPc2	přehrada
Vír	Vír	k1gInSc4	Vír
I	i	k8xC	i
a	a	k8xC	a
Vir	vir	k1gInSc1	vir
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Tišnovem	Tišnov	k1gInSc7	Tišnov
přijímá	přijímat	k5eAaImIp3nS	přijímat
Loučku	louček	k1gInSc2	louček
a	a	k8xC	a
u	u	k7c2	u
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
přetíná	přetínat	k5eAaImIp3nS	přetínat
Boskovickou	boskovický	k2eAgFnSc4d1	boskovická
brázdu	brázda	k1gFnSc4	brázda
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
a	a	k8xC	a
kotlina	kotlina	k1gFnSc1	kotlina
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Svratka	Svratka	k1gFnSc1	Svratka
mění	měnit	k5eAaImIp3nS	měnit
směr	směr	k1gInSc4	směr
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
u	u	k7c2	u
Přízřenic	Přízřenice	k1gFnPc2	Přízřenice
přijímá	přijímat	k5eAaImIp3nS	přijímat
svůj	svůj	k3xOyFgInSc4	svůj
největší	veliký	k2eAgInSc4d3	veliký
levý	levý	k2eAgInSc4d1	levý
přítok	přítok	k1gInSc4	přítok
Svitavu	Svitava	k1gFnSc4	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
Soutok	soutok	k1gInSc1	soutok
Svratky	Svratka	k1gFnSc2	Svratka
se	s	k7c7	s
Svitavou	Svitava	k1gFnSc7	Svitava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
napřímeným	napřímený	k2eAgNnSc7d1	napřímené
korytem	koryto	k1gNnSc7	koryto
rovinou	rovina	k1gFnSc7	rovina
Dyjsko-svrateckého	dyjskovratecký	k2eAgInSc2d1	dyjsko-svratecký
úvalu	úval	k1gInSc2	úval
a	a	k8xC	a
v	v	k7c6	v
Židlochovicích	Židlochovice	k1gFnPc6	Židlochovice
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vlévá	vlévat	k5eAaImIp3nS	vlévat
Litava	Litava	k1gFnSc1	Litava
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
deltovým	deltový	k2eAgNnSc7d1	deltové
vyústěním	vyústění	k1gNnSc7	vyústění
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
nádrže	nádrž	k1gFnSc2	nádrž
vodního	vodní	k2eAgNnSc2d1	vodní
díla	dílo	k1gNnSc2	dílo
Nové	Nové	k2eAgInPc1d1	Nové
Mlýny	mlýn	k1gInPc1	mlýn
na	na	k7c6	na
Dyji	Dyje	k1gFnSc6	Dyje
se	se	k3xPyFc4	se
Svratka	Svratka	k1gFnSc1	Svratka
stéká	stékat	k5eAaImIp3nS	stékat
s	s	k7c7	s
Jihlavou	Jihlava	k1gFnSc7	Jihlava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
technicky	technicky	k6eAd1	technicky
vzato	vzít	k5eAaPmNgNnS	vzít
jejím	její	k3xOp3gInSc7	její
největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
za	za	k7c4	za
rovnocenný	rovnocenný	k2eAgInSc4d1	rovnocenný
přítok	přítok	k1gInSc4	přítok
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jihlavou	Jihlava	k1gFnSc7	Jihlava
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
27,24	[number]	k4	27,24
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Tok	tok	k1gInSc1	tok
Svratky	Svratka	k1gFnSc2	Svratka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
třech	tři	k4xCgNnPc6	tři
místech	místo	k1gNnPc6	místo
uměle	uměle	k6eAd1	uměle
přehrazen	přehrazen	k2eAgMnSc1d1	přehrazen
<g/>
:	:	kIx,	:
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vír	Vír	k1gInSc1	Vír
I	I	kA	I
<g/>
,	,	kIx,	,
říční	říční	k2eAgInSc1d1	říční
km	km	kA	km
114,9	[number]	k4	114,9
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vír	Vír	k1gInSc1	Vír
II	II	kA	II
<g/>
,	,	kIx,	,
říční	říční	k2eAgInSc1d1	říční
km	km	kA	km
111,6	[number]	k4	111,6
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
říční	říční	k2eAgInSc1d1	říční
km	km	kA	km
56,2	[number]	k4	56,2
Svratka	Svratka	k1gFnSc1	Svratka
Jimramov	Jimramov	k1gInSc1	Jimramov
Štěpánov	Štěpánov	k1gInSc1	Štěpánov
nad	nad	k7c7	nad
Svratkou	Svratka	k1gFnSc7	Svratka
Nedvědice	Nedvědice	k1gFnSc2	Nedvědice
Tišnov	Tišnov	k1gInSc4	Tišnov
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
Brno	Brno	k1gNnSc1	Brno
Modřice	Modřice	k1gFnSc2	Modřice
Rajhrad	Rajhrad	k1gInSc1	Rajhrad
Židlochovice	Židlochovice	k1gFnPc4	Židlochovice
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
řeku	řeka	k1gFnSc4	řeka
vodácky	vodácky	k6eAd1	vodácky
využitelnou	využitelný	k2eAgFnSc4d1	využitelná
<g/>
.	.	kIx.	.
</s>
<s>
Vodácky	vodácky	k6eAd1	vodácky
využívaný	využívaný	k2eAgInSc1d1	využívaný
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc1	úsek
řeky	řeka	k1gFnSc2	řeka
mezi	mezi	k7c7	mezi
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
Vír	Vír	k1gInSc1	Vír
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Doubravníkem	Doubravník	k1gInSc7	Doubravník
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgNnSc7	který
pak	pak	k6eAd1	pak
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
pěkným	pěkný	k2eAgNnSc7d1	pěkné
údolím	údolí	k1gNnSc7	údolí
se	s	k7c7	s
skalami	skála	k1gFnPc7	skála
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vyššího	vysoký	k2eAgInSc2d2	vyšší
stavu	stav	k1gInSc2	stav
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
řeku	řeka	k1gFnSc4	řeka
sjet	sjet	k5eAaPmF	sjet
už	už	k6eAd1	už
z	z	k7c2	z
Milov	Milovo	k1gNnPc2	Milovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
rychle	rychle	k6eAd1	rychle
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
a	a	k8xC	a
úzká	úzký	k2eAgFnSc1d1	úzká
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vodními	vodní	k2eAgFnPc7d1	vodní
nádržemi	nádrž	k1gFnPc7	nádrž
Vír	Vír	k1gInSc1	Vír
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
krátký	krátký	k2eAgInSc1d1	krátký
slalomový	slalomový	k2eAgInSc1d1	slalomový
kanál	kanál	k1gInSc1	kanál
<g/>
,	,	kIx,	,
sjízdný	sjízdný	k2eAgInSc1d1	sjízdný
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
chodu	chod	k1gInSc6	chod
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
napsal	napsat	k5eAaBmAgMnS	napsat
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
posledních	poslední	k2eAgFnPc2d1	poslední
sbírek	sbírka	k1gFnPc2	sbírka
Chrpy	chrpa	k1gFnSc2	chrpa
a	a	k8xC	a
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
