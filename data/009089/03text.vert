<p>
<s>
Stažená	stažený	k2eAgFnSc1d1	stažená
hrdla	hrdla	k1gFnSc1	hrdla
je	být	k5eAaImIp3nS	být
komická	komický	k2eAgFnSc1d1	komická
miniopera	miniopera	k1gFnSc1	miniopera
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Ilji	Ilja	k1gMnSc2	Ilja
Hurníka	Hurník	k1gMnSc2	Hurník
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
libreto	libreto	k1gNnSc4	libreto
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
dirigenta	dirigent	k1gMnSc2	dirigent
a	a	k8xC	a
dramaturga	dramaturg	k1gMnSc2	dramaturg
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Komorní	komorní	k2eAgFnSc2d1	komorní
opery	opera	k1gFnSc2	opera
Václava	Václav	k1gMnSc2	Václav
Noska	Nosek	k1gMnSc2	Nosek
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
ji	on	k3xPp3gFnSc4	on
uvedla	uvést	k5eAaPmAgFnS	uvést
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
Komorní	komorní	k2eAgFnSc1d1	komorní
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
scéna	scéna	k1gFnSc1	scéna
Hudební	hudební	k2eAgFnSc2d1	hudební
fakulty	fakulta	k1gFnSc2	fakulta
Janáčkovy	Janáčkův	k2eAgFnSc2d1	Janáčkova
akademie	akademie	k1gFnSc2	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
Bezbariérového	bezbariérový	k2eAgNnSc2d1	bezbariérové
divadla	divadlo	k1gNnSc2	divadlo
Barka	Barka	k1gFnSc1	Barka
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
minioperou	miniopera	k1gFnSc7	miniopera
Varta	varta	k1gFnSc1	varta
Jana	Jan	k1gMnSc2	Jan
Franka	Frank	k1gMnSc2	Frank
Fischera	Fischer	k1gMnSc2	Fischer
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
díly	díl	k1gInPc7	díl
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Martinů	Martinů	k1gFnSc2	Martinů
<g/>
,	,	kIx,	,
baletu	balet	k1gInSc2	balet
Kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
revue	revue	k1gFnSc2	revue
a	a	k8xC	a
opery	opera	k1gFnSc2	opera
Dvakrát	dvakrát	k6eAd1	dvakrát
Alexandr	Alexandr	k1gMnSc1	Alexandr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
první	první	k4xOgNnPc4	první
obsazení	obsazení	k1gNnPc4	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
