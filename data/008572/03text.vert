<p>
<s>
Topspin	topspin	k1gInSc1	topspin
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
úderu	úder	k1gInSc2	úder
míče	míč	k1gInPc4	míč
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
sportech	sport	k1gInPc6	sport
používajících	používající	k2eAgInPc2d1	používající
raketu	raketa	k1gFnSc4	raketa
či	či	k8xC	či
pálku	pálka	k1gFnSc4	pálka
<g/>
.	.	kIx.	.
</s>
<s>
Míč	míč	k1gInSc1	míč
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
získává	získávat	k5eAaImIp3nS	získávat
horní	horní	k2eAgFnSc4d1	horní
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
roztočen	roztočit	k5eAaPmNgMnS	roztočit
okolo	okolo	k7c2	okolo
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
osy	osa	k1gFnSc2	osa
vpřed	vpřed	k6eAd1	vpřed
dolů	dolů	k6eAd1	dolů
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
letu	let	k1gInSc2	let
a	a	k8xC	a
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
Magnusův	Magnusův	k2eAgInSc1d1	Magnusův
jev	jev	k1gInSc1	jev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
rakety	raketa	k1gFnSc2	raketa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
veden	vést	k5eAaImNgInS	vést
zdola	zdola	k6eAd1	zdola
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
letu	let	k1gInSc2	let
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
a	a	k8xC	a
vypouklejší	vypouklý	k2eAgFnSc1d2	vypouklejší
<g/>
,	,	kIx,	,
odskok	odskok	k1gInSc1	odskok
míče	míč	k1gInSc2	míč
poté	poté	k6eAd1	poté
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
,	,	kIx,	,
vysoký	vysoký	k2eAgInSc1d1	vysoký
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
úder	úder	k1gInSc4	úder
zahraný	zahraný	k2eAgInSc4d1	zahraný
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nižší	nízký	k2eAgFnSc6d2	nižší
intenzitě	intenzita	k1gFnSc6	intenzita
horní	horní	k2eAgFnSc2d1	horní
rotace	rotace	k1gFnSc2	rotace
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
liftu	lift	k1gInSc6	lift
-	-	kIx~	-
liftovaném	liftovaný	k2eAgInSc6d1	liftovaný
úderu	úder	k1gInSc6	úder
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úder	úder	k1gInSc1	úder
s	s	k7c7	s
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
rotace	rotace	k1gFnSc2	rotace
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
čop	čop	k?	čop
(	(	kIx(	(
<g/>
backspin	backspin	k1gMnSc1	backspin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
sportů	sport	k1gInPc2	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
je	být	k5eAaImIp3nS	být
topspin	topspin	k1gInSc1	topspin
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
variantu	varianta	k1gFnSc4	varianta
forhendového	forhendový	k2eAgInSc2d1	forhendový
či	či	k8xC	či
bekhendového	bekhendový	k2eAgInSc2d1	bekhendový
úderu	úder	k1gInSc2	úder
s	s	k7c7	s
horní	horní	k2eAgFnSc7d1	horní
rotací	rotace	k1gFnSc7	rotace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pohyb	pohyb	k1gInSc1	pohyb
rakety	raketa	k1gFnSc2	raketa
s	s	k7c7	s
paží	paže	k1gFnSc7	paže
jde	jít	k5eAaImIp3nS	jít
strměji	strmě	k6eAd2	strmě
vzhůru	vzhůru	k6eAd1	vzhůru
než	než	k8xS	než
při	při	k7c6	při
klasickém	klasický	k2eAgInSc6d1	klasický
úderu	úder	k1gInSc6	úder
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
účinnost	účinnost	k1gFnSc1	účinnost
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
rychlejších	rychlý	k2eAgInPc6d2	rychlejší
površích	povrch	k1gInPc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
úder	úder	k1gInSc4	úder
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgNnSc2	který
lze	lze	k6eAd1	lze
lépe	dobře	k6eAd2	dobře
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
umístění	umístění	k1gNnSc4	umístění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgInPc7d1	další
sporty	sport	k1gInPc7	sport
jsou	být	k5eAaImIp3nP	být
baseball	baseball	k1gInSc4	baseball
<g/>
,	,	kIx,	,
golf	golf	k1gInSc4	golf
<g/>
,	,	kIx,	,
kulečník	kulečník	k1gInSc4	kulečník
<g/>
,	,	kIx,	,
kriket	kriket	k1gInSc4	kriket
a	a	k8xC	a
stolní	stolní	k2eAgInSc4d1	stolní
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Topspin	topspin	k1gInSc1	topspin
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
.	.	kIx.	.
</s>
</p>
