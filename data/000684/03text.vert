<s>
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
(	(	kIx(	(
<g/>
Bohémská	bohémský	k2eAgFnSc1d1	bohémská
rapsodie	rapsodie	k1gFnSc1	rapsodie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
britské	britský	k2eAgFnSc2d1	britská
kapely	kapela	k1gFnSc2	kapela
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
albu	album	k1gNnSc6	album
A	a	k9	a
Night	Night	k1gMnSc1	Night
at	at	k?	at
the	the	k?	the
Opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
převzali	převzít	k5eAaPmAgMnP	převzít
další	další	k2eAgMnPc1d1	další
interpreti	interpret	k1gMnPc1	interpret
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
této	tento	k3xDgFnSc2	tento
jediné	jediný	k2eAgFnSc2d1	jediná
skladby	skladba	k1gFnSc2	skladba
trvalo	trvat	k5eAaImAgNnS	trvat
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
natočit	natočit	k5eAaBmF	natočit
celé	celý	k2eAgNnSc4d1	celé
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hitem	hit	k1gInSc7	hit
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
-	-	kIx~	-
téměř	téměř	k6eAd1	téměř
šest	šest	k4xCc1	šest
minut	minuta	k1gFnPc2	minuta
je	být	k5eAaImIp3nS	být
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
délky	délka	k1gFnSc2	délka
singlu	singl	k1gInSc2	singl
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
hudebně	hudebně	k6eAd1	hudebně
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
hard	hard	k6eAd1	hard
rock	rock	k1gInSc1	rock
a	a	k8xC	a
opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
Mercury	Mercura	k1gFnPc4	Mercura
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
ji	on	k3xPp3gFnSc4	on
celou	celá	k1gFnSc4	celá
sám	sám	k3xTgInSc1	sám
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
basové	basový	k2eAgFnSc2d1	basová
linky	linka	k1gFnSc2	linka
a	a	k8xC	a
bicích	bicí	k2eAgInPc2d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
kytarových	kytarový	k2eAgInPc2d1	kytarový
přechodů	přechod	k1gInPc2	přechod
i	i	k8xC	i
slavného	slavný	k2eAgNnSc2d1	slavné
kytarového	kytarový	k2eAgNnSc2d1	kytarové
sóla	sólo	k1gNnSc2	sólo
je	být	k5eAaImIp3nS	být
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
<g/>
.	.	kIx.	.
</s>
<s>
Skladbu	skladba	k1gFnSc4	skladba
měl	mít	k5eAaImAgInS	mít
Mercury	Mercura	k1gFnSc2	Mercura
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
poznámky	poznámka	k1gFnPc4	poznámka
sepsal	sepsat	k5eAaPmAgMnS	sepsat
na	na	k7c4	na
zadní	zadní	k2eAgFnSc4d1	zadní
stranu	strana	k1gFnSc4	strana
telefonního	telefonní	k2eAgInSc2d1	telefonní
seznamu	seznam	k1gInSc2	seznam
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgInPc1d1	známý
mnohohlasy	mnohohlas	k1gInPc1	mnohohlas
v	v	k7c6	v
operní	operní	k2eAgFnSc6d1	operní
části	část	k1gFnSc6	část
nazpívali	nazpívat	k5eAaPmAgMnP	nazpívat
Mercury	Mercura	k1gFnPc4	Mercura
<g/>
,	,	kIx,	,
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
a	a	k8xC	a
May	May	k1gMnSc1	May
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
písní	píseň	k1gFnPc2	píseň
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
hned	hned	k6eAd1	hned
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
uvedení	uvedení	k1gNnSc6	uvedení
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
po	po	k7c6	po
Mercuryho	Mercury	k1gMnSc2	Mercury
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Dočkala	dočkat	k5eAaPmAgFnS	dočkat
se	se	k3xPyFc4	se
mnoha	mnoho	k4c2	mnoho
coververzí	coververze	k1gFnPc2	coververze
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
filmech	film	k1gInPc6	film
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Waynnův	Waynnův	k2eAgInSc1d1	Waynnův
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
přínosem	přínos	k1gInSc7	přínos
i	i	k9	i
po	po	k7c6	po
jiné	jiný	k2eAgFnSc6d1	jiná
stránce	stránka	k1gFnSc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
Queen	Queno	k1gNnPc2	Queno
nemohla	moct	k5eNaImAgFnS	moct
dostavit	dostavit	k5eAaPmF	dostavit
na	na	k7c6	na
Top	topit	k5eAaImRp2nS	topit
of	of	k?	of
the	the	k?	the
Pops	Pops	k1gInSc4	Pops
vysílané	vysílaný	k2eAgFnSc2d1	vysílaná
britskou	britský	k2eAgFnSc7d1	britská
BBC	BBC	kA	BBC
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
promo	promo	k6eAd1	promo
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dalo	dát	k5eAaPmAgNnS	dát
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
moderní	moderní	k2eAgInPc4d1	moderní
hudební	hudební	k2eAgInPc4d1	hudební
videoklipy	videoklip	k1gInPc4	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
několik	několik	k4yIc1	několik
na	na	k7c4	na
dnešní	dnešní	k2eAgFnSc4d1	dnešní
dobu	doba	k1gFnSc4	doba
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
trikových	trikový	k2eAgFnPc2d1	triková
sekvencí	sekvence	k1gFnPc2	sekvence
a	a	k8xC	a
ústředním	ústřední	k2eAgNnSc7d1	ústřední
tématem	téma	k1gNnSc7	téma
byl	být	k5eAaImAgInS	být
námět	námět	k1gInSc1	námět
z	z	k7c2	z
obálky	obálka	k1gFnSc2	obálka
druhého	druhý	k4xOgNnSc2	druhý
alba	album	k1gNnSc2	album
skupiny	skupina	k1gFnSc2	skupina
Queen	Quena	k1gFnPc2	Quena
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
a	a	k8xC	a
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
prvky	prvek	k1gInPc4	prvek
hard	harda	k1gFnPc2	harda
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Intro	Intro	k1gNnSc1	Intro
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
48	[number]	k4	48
<g/>
)	)	kIx)	)
Baladická	baladický	k2eAgFnSc1d1	baladická
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
<g/>
)	)	kIx)	)
Kytarové	kytarový	k2eAgNnSc1d1	kytarové
sólo	sólo	k1gNnSc1	sólo
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Operní	operní	k2eAgFnSc1d1	operní
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Hard	Hard	k1gInSc1	Hard
rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
Outro	Outro	k1gNnSc4	Outro
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
<g/>
)	)	kIx)	)
The	The	k1gFnSc2	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
December	December	k1gInSc1	December
27	[number]	k4	27
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Unconventional	Unconventional	k1gFnSc6	Unconventional
Queen	Quena	k1gFnPc2	Quena
Hit	hit	k1gInSc1	hit
Still	Still	k1gMnSc1	Still
Rocks	Rocksa	k1gFnPc2	Rocksa
After	After	k1gMnSc1	After
30	[number]	k4	30
Years	Years	k1gInSc1	Years
nytimes	nytimes	k1gInSc4	nytimes
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Klip	klip	k1gInSc1	klip
na	na	k7c6	na
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
YouTube	YouTub	k1gInSc5	YouTub
kanálu	kanál	k1gInSc2	kanál
Queen	Queen	k1gInSc4	Queen
</s>
