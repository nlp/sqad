<s>
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
(	(	kIx(	(
<g/>
Bohémská	bohémský	k2eAgFnSc1d1	bohémská
rapsodie	rapsodie	k1gFnSc1	rapsodie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
britské	britský	k2eAgFnSc2d1	britská
kapely	kapela	k1gFnSc2	kapela
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
.	.	kIx.	.
</s>
