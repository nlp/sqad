<s>
Star	Star	kA
Trek	Trek	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
pojmem	pojem	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1
získal	získat	k5eAaPmAgInS
mnoho	mnoho	k4c4
ocenění	ocenění	k1gNnPc2
<g/>
,	,	kIx,
jmenovitě	jmenovitě	k6eAd1
jednoho	jeden	k4xCgMnSc4
Oscara	Oscar	k1gMnSc4
<g/>
,	,	kIx,
31	[number]	k4
cen	cena	k1gFnPc2
Emmy	Emma	k1gFnSc2
<g/>
,	,	kIx,
několik	několik	k4yIc4
cen	cena	k1gFnPc2
BAFTA	BAFTA	kA
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgMnPc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k6eAd1
dal	dát	k5eAaPmAgMnS
vzniknout	vzniknout	k5eAaPmF
široké	široký	k2eAgFnSc3d1
základně	základna	k1gFnSc3
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
označované	označovaný	k2eAgFnPc1d1
jako	jako	k8xS
trekkies	trekkies	k1gInSc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yRgInPc4
se	se	k3xPyFc4
počítá	počítat	k5eAaImIp3nS
např.	např.	kA
Bill	Bill	k1gMnSc1
Gates	Gates	k1gMnSc1
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
Schwarzenegger	Schwarzenegger	k1gMnSc1
nebo	nebo	k8xC
Whoopi	Whoopi	k1gNnPc1
Goldbergová	Goldbergový	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>