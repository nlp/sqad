<p>
<s>
Blahoslavenství	blahoslavenství	k1gNnSc1	blahoslavenství
(	(	kIx(	(
<g/>
kalk	kalk	k1gInSc1	kalk
z	z	k7c2	z
lat.	lat.	k?	lat.
beatitudo	beatitudo	k1gNnSc1	beatitudo
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
,	,	kIx,	,
blaženost	blaženost	k1gFnSc1	blaženost
a	a	k8xC	a
řec.	řec.	k?	řec.
ε	ε	k?	ε
[	[	kIx(	[
<g/>
eulogein	eulogein	k1gMnSc1	eulogein
<g/>
]	]	kIx)	]
dobrořečit	dobrořečit	k5eAaImF	dobrořečit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
ústřední	ústřední	k2eAgFnSc2d1	ústřední
části	část	k1gFnSc2	část
tzv.	tzv.	kA	tzv.
Horského	Horského	k2eAgNnSc2d1	Horského
kázání	kázání	k1gNnSc2	kázání
zaznamenaného	zaznamenaný	k2eAgNnSc2d1	zaznamenané
v	v	k7c6	v
Matoušově	Matoušův	k2eAgInSc6d1	Matoušův
a	a	k8xC	a
Lukášovu	Lukášův	k2eAgNnSc3d1	Lukášovo
evangeliu	evangelium	k1gNnSc3	evangelium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Horské	Horské	k2eAgFnSc6d1	Horské
řeči	řeč	k1gFnSc6	řeč
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
popisuje	popisovat	k5eAaImIp3nS	popisovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
božího	boží	k2eAgNnSc2d1	boží
království	království	k1gNnSc2	království
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgMnSc1	takový
člověk	člověk	k1gMnSc1	člověk
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
,	,	kIx,	,
starším	starý	k2eAgInSc7d2	starší
tvarem	tvar	k1gInSc7	tvar
blahoslavený	blahoslavený	k2eAgMnSc1d1	blahoslavený
(	(	kIx(	(
<g/>
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
textu	text	k1gInSc6	text
μ	μ	k?	μ
[	[	kIx(	[
<g/>
makarios	makarios	k1gMnSc1	makarios
<g/>
]	]	kIx)	]
požehnaný	požehnaný	k2eAgMnSc1d1	požehnaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc4	každý
z	z	k7c2	z
blahoslavenství	blahoslavenství	k1gNnSc2	blahoslavenství
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nS	by
člověk	člověk	k1gMnSc1	člověk
podle	podle	k7c2	podle
běžných	běžný	k2eAgNnPc2d1	běžné
měřítek	měřítko	k1gNnPc2	měřítko
za	za	k7c4	za
šťastnou	šťastný	k2eAgFnSc4d1	šťastná
nepovažoval	považovat	k5eNaImAgMnS	považovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
Ježíš	ježit	k5eAaImIp2nS	ježit
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
takových	takový	k3xDgFnPc6	takový
situacích	situace	k1gFnPc6	situace
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
,	,	kIx,	,
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
za	za	k7c4	za
šťastné	šťastný	k2eAgNnSc4d1	šťastné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úryvky	úryvek	k1gInPc1	úryvek
podobné	podobný	k2eAgFnSc2d1	podobná
Ježíšovým	Ježíšův	k2eAgNnSc7d1	Ježíšovo
blahoslavenstvím	blahoslavenství	k1gNnSc7	blahoslavenství
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
ve	v	k7c6	v
svitcích	svitek	k1gInPc6	svitek
od	od	k7c2	od
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
židovských	židovský	k2eAgInPc6d1	židovský
pramenech	pramen	k1gInPc6	pramen
předkřesťanské	předkřesťanský	k2eAgFnSc2d1	předkřesťanská
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
blahoslavenství	blahoslavenství	k1gNnSc1	blahoslavenství
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
byzantské	byzantský	k2eAgFnSc2d1	byzantská
liturgie	liturgie	k1gFnSc2	liturgie
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Zlatoústého	zlatoústý	k2eAgMnSc2d1	zlatoústý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Text	text	k1gInSc1	text
blahoslavenství	blahoslavenství	k1gNnSc1	blahoslavenství
==	==	k?	==
</s>
</p>
<p>
<s>
Blahoslavení	blahoslavení	k1gNnSc1	blahoslavení
chudí	chudit	k5eAaImIp3nS	chudit
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gNnSc1	jejich
je	být	k5eAaImIp3nS	být
nebeské	nebeský	k2eAgNnSc1d1	nebeské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavení	blahoslavení	k1gNnSc1	blahoslavení
plačící	plačící	k2eAgNnSc1d1	plačící
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oni	onen	k3xDgMnPc1	onen
budou	být	k5eAaImBp3nP	být
potěšeni	potěšit	k5eAaPmNgMnP	potěšit
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavení	blahoslavení	k1gNnSc1	blahoslavení
tiší	tišit	k5eAaImIp3nS	tišit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oni	onen	k3xDgMnPc1	onen
dostanou	dostat	k5eAaPmIp3nP	dostat
zemi	zem	k1gFnSc4	zem
za	za	k7c4	za
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavení	blahoslavení	k1gNnSc1	blahoslavení
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
lační	lačnit	k5eAaImIp3nS	lačnit
a	a	k8xC	a
žízní	žíznit	k5eAaImIp3nS	žíznit
po	po	k7c6	po
spravedlnosti	spravedlnost	k1gFnSc6	spravedlnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oni	onen	k3xDgMnPc1	onen
budou	být	k5eAaImBp3nP	být
nasyceni	nasytit	k5eAaPmNgMnP	nasytit
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavení	blahoslavení	k1gNnSc4	blahoslavení
milosrdní	milosrdný	k2eAgMnPc1d1	milosrdný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oni	onen	k3xDgMnPc1	onen
dojdou	dojít	k5eAaPmIp3nP	dojít
milosrdenství	milosrdenství	k1gNnSc4	milosrdenství
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavení	blahoslavení	k1gNnSc1	blahoslavení
čistého	čistý	k2eAgNnSc2d1	čisté
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oni	onen	k3xDgMnPc1	onen
uvidí	uvidět	k5eAaPmIp3nP	uvidět
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavení	blahoslavený	k2eAgMnPc1d1	blahoslavený
tvůrci	tvůrce	k1gMnPc1	tvůrce
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oni	onen	k3xDgMnPc1	onen
budou	být	k5eAaImBp3nP	být
nazváni	nazvat	k5eAaPmNgMnP	nazvat
Božími	boží	k2eAgMnPc7d1	boží
syny	syn	k1gMnPc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavení	blahoslavení	k1gNnSc1	blahoslavení
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jsou	být	k5eAaImIp3nP	být
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
pro	pro	k7c4	pro
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gNnSc1	jejich
je	být	k5eAaImIp3nS	být
nebeské	nebeský	k2eAgNnSc1d1	nebeské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
Blaze	blaze	k6eAd1	blaze
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
když	když	k8xS	když
vás	vy	k3xPp2nPc4	vy
budou	být	k5eAaImBp3nP	být
tupit	tupit	k5eAaImF	tupit
a	a	k8xC	a
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
a	a	k8xC	a
lživě	lživě	k6eAd1	lživě
vám	vy	k3xPp2nPc3	vy
připisovat	připisovat	k5eAaImF	připisovat
všechno	všechen	k3xTgNnSc1	všechen
zlé	zlé	k1gNnSc1	zlé
kvůli	kvůli	k7c3	kvůli
mně	já	k3xPp1nSc3	já
<g/>
.	.	kIx.	.
</s>
<s>
Radujte	radovat	k5eAaImRp2nP	radovat
se	se	k3xPyFc4	se
a	a	k8xC	a
jásejte	jásat	k5eAaImRp2nP	jásat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
máte	mít	k5eAaImIp2nP	mít
v	v	k7c6	v
nebi	nebe	k1gNnSc6	nebe
hojnou	hojný	k2eAgFnSc4d1	hojná
odměnu	odměna	k1gFnSc4	odměna
<g/>
,	,	kIx,	,
vždyť	vždyť	k9	vždyť
stejně	stejně	k6eAd1	stejně
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
i	i	k9	i
proroky	prorok	k1gMnPc4	prorok
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
před	před	k7c7	před
vámi	vy	k3xPp2nPc7	vy
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Mt	Mt	k1gFnSc1	Mt
5,3	[number]	k4	5,3
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Horské	Horské	k2eAgNnSc4d1	Horské
kázání	kázání	k1gNnSc4	kázání
</s>
</p>
<p>
<s>
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
</s>
</p>
<p>
<s>
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
</s>
</p>
