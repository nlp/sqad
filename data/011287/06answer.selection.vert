<s>
V	v	k7c6	v
Horské	Horské	k2eAgFnSc6d1	Horské
řeči	řeč	k1gFnSc6	řeč
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
popisuje	popisovat	k5eAaImIp3nS	popisovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
božího	boží	k2eAgNnSc2d1	boží
království	království	k1gNnSc2	království
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgMnSc1	takový
člověk	člověk	k1gMnSc1	člověk
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
,	,	kIx,	,
starším	starý	k2eAgInSc7d2	starší
tvarem	tvar	k1gInSc7	tvar
blahoslavený	blahoslavený	k2eAgMnSc1d1	blahoslavený
(	(	kIx(	(
<g/>
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
textu	text	k1gInSc6	text
μ	μ	k?	μ
[	[	kIx(	[
<g/>
makarios	makarios	k1gMnSc1	makarios
<g/>
]	]	kIx)	]
požehnaný	požehnaný	k2eAgMnSc1d1	požehnaný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
