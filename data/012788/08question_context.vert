<s>
Space	Spako	k6eAd1
Exploration	Exploration	k1gInSc1
Technologies	Technologiesa	k1gFnPc2
Corporation	Corporation	k1gInSc1
<g/>
,	,	kIx,
více	hodně	k6eAd2
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS
SpaceX	SpaceX	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
americkou	americký	k2eAgFnSc7d1
technologickou	technologický	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
působící	působící	k2eAgFnSc7d1
v	v	k7c6
aerokosmickém	aerokosmický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2002	[number]	k4
založil	založit	k5eAaPmAgMnS
Elon	Elon	k1gMnSc1
Musk	Musk	k1gMnSc1
z	z	k7c2
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4
vydělal	vydělat	k5eAaPmAgInS
na	na	k7c6
prodeji	prodej	k1gInSc6
svého	svůj	k3xOyFgInSc2
podílu	podíl	k1gInSc2
v	v	k7c6
systému	systém	k1gInSc6
PayPal	PayPal	k1gInSc1
<g/>
.	.	kIx.
</s>