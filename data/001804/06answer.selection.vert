<s>
Suomenlinna	Suomenlinna	k1gFnSc1	Suomenlinna
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Sveaborg	Sveaborg	k1gInSc1	Sveaborg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
námořní	námořní	k2eAgFnSc7d1	námořní
obrannou	obranný	k2eAgFnSc7d1	obranná
pevností	pevnost	k1gFnSc7	pevnost
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgFnSc7d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
osmi	osm	k4xCc6	osm
dodnes	dodnes	k6eAd1	dodnes
obydlených	obydlený	k2eAgInPc6d1	obydlený
ostrovech	ostrov	k1gInPc6	ostrov
před	před	k7c7	před
vjezdem	vjezd	k1gInSc7	vjezd
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
finského	finský	k2eAgNnSc2d1	finské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Helsinky	Helsinky	k1gFnPc1	Helsinky
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
současně	současně	k6eAd1	současně
i	i	k9	i
statut	statut	k1gInSc1	statut
předměstí	předměstí	k1gNnSc2	předměstí
Helsinek	Helsinky	k1gFnPc2	Helsinky
<g/>
.	.	kIx.	.
</s>
