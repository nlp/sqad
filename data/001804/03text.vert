<s>
Suomenlinna	Suomenlinna	k1gFnSc1	Suomenlinna
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Sveaborg	Sveaborg	k1gInSc1	Sveaborg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
námořní	námořní	k2eAgFnSc7d1	námořní
obrannou	obranný	k2eAgFnSc7d1	obranná
pevností	pevnost	k1gFnSc7	pevnost
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgFnSc7d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
osmi	osm	k4xCc6	osm
dodnes	dodnes	k6eAd1	dodnes
obydlených	obydlený	k2eAgInPc6d1	obydlený
ostrovech	ostrov	k1gInPc6	ostrov
před	před	k7c7	před
vjezdem	vjezd	k1gInSc7	vjezd
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
finského	finský	k2eAgNnSc2d1	finské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Helsinky	Helsinky	k1gFnPc1	Helsinky
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
současně	současně	k6eAd1	současně
i	i	k9	i
statut	statut	k1gInSc1	statut
předměstí	předměstí	k1gNnSc2	předměstí
Helsinek	Helsinky	k1gFnPc2	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
Kustaanmiekka	Kustaanmiekka	k1gMnSc1	Kustaanmiekka
<g/>
,	,	kIx,	,
Pikku	Pikek	k1gMnSc3	Pikek
Mustasaari	Mustasaar	k1gMnSc3	Mustasaar
<g/>
,	,	kIx,	,
Iso	Iso	k1gMnSc3	Iso
Mustasaari	Mustasaar	k1gMnSc3	Mustasaar
<g/>
,	,	kIx,	,
Länsi-Mustasaari	Länsi-Mustasaar	k1gMnSc3	Länsi-Mustasaar
a	a	k8xC	a
Susisaari	Susisaar	k1gFnSc3	Susisaar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
několika	několik	k4yIc7	několik
mosty	most	k1gInPc7	most
(	(	kIx(	(
<g/>
další	další	k2eAgNnPc1d1	další
tři	tři	k4xCgFnPc1	tři
jsou	být	k5eAaImIp3nP	být
Särkkä	Särkkä	k1gMnPc4	Särkkä
<g/>
,	,	kIx,	,
Lonna	Lonna	k1gFnSc1	Lonna
a	a	k8xC	a
Pormestarinluodot	Pormestarinluodot	k1gInSc1	Pormestarinluodot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
všech	všecek	k3xTgInPc2	všecek
ostrovů	ostrov	k1gInPc2	ostrov
činí	činit	k5eAaImIp3nS	činit
0,8	[number]	k4	0,8
km2	km2	k4	km2
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
kolem	kolem	k7c2	kolem
850	[number]	k4	850
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
budovat	budovat	k5eAaImF	budovat
roku	rok	k1gInSc2	rok
1748	[number]	k4	1748
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
součástí	součást	k1gFnSc7	součást
švédského	švédský	k2eAgNnSc2d1	švédské
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
Petr	Petr	k1gMnSc1	Petr
Veliký	veliký	k2eAgMnSc1d1	veliký
založil	založit	k5eAaPmAgInS	založit
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
a	a	k8xC	a
pevnost	pevnost	k1gFnSc1	pevnost
Kronštadt	Kronštadt	k1gInSc1	Kronštadt
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
nárokovat	nárokovat	k5eAaImF	nárokovat
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
Baltickém	baltický	k2eAgNnSc6d1	Baltické
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
byla	být	k5eAaImAgFnS	být
pevnost	pevnost	k1gFnSc1	pevnost
často	často	k6eAd1	často
nazývána	nazývat	k5eAaImNgFnS	nazývat
Gibraltarem	Gibraltar	k1gInSc7	Gibraltar
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obsazení	obsazení	k1gNnSc6	obsazení
Finska	Finsko	k1gNnSc2	Finsko
ruskou	ruský	k2eAgFnSc7d1	ruská
armádou	armáda	k1gFnSc7	armáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
připadla	připadnout	k5eAaPmAgFnS	připadnout
pevnost	pevnost	k1gFnSc4	pevnost
Rusku	Rusko	k1gNnSc3	Rusko
až	až	k6eAd1	až
po	po	k7c6	po
dohodnuté	dohodnutý	k2eAgFnSc6d1	dohodnutá
kapitulaci	kapitulace	k1gFnSc6	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Finska	Finsko	k1gNnSc2	Finsko
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
rok	rok	k1gInSc4	rok
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
;	;	kIx,	;
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
využívána	využívat	k5eAaImNgFnS	využívat
bělogvardějci	bělogvardějec	k1gMnSc3	bělogvardějec
jako	jako	k8xS	jako
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
snadno	snadno	k6eAd1	snadno
dosažitelné	dosažitelný	k2eAgFnPc1d1	dosažitelná
z	z	k7c2	z
Helsinek	Helsinky	k1gFnPc2	Helsinky
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
turistickým	turistický	k2eAgInSc7d1	turistický
cílem	cíl	k1gInSc7	cíl
<g/>
;	;	kIx,	;
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
muzeí	muzeum	k1gNnPc2	muzeum
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
umělců	umělec	k1gMnPc2	umělec
zde	zde	k6eAd1	zde
provozuje	provozovat	k5eAaImIp3nS	provozovat
svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
dnešní	dnešní	k2eAgNnSc4d1	dnešní
jméno	jméno	k1gNnSc4	jméno
Suomenlinna	Suomenlinn	k1gInSc2	Suomenlinn
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Finský	finský	k2eAgInSc1d1	finský
hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
dostala	dostat	k5eAaPmAgFnS	dostat
pevnost	pevnost	k1gFnSc1	pevnost
až	až	k9	až
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
však	však	k9	však
lze	lze	k6eAd1	lze
i	i	k9	i
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
švédským	švédský	k2eAgInSc7d1	švédský
názvem	název	k1gInSc7	název
Sveaborg	Sveaborg	k1gInSc1	Sveaborg
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Švédský	švédský	k2eAgInSc1d1	švédský
hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
komplex	komplex	k1gInSc1	komplex
staveb	stavba	k1gFnPc2	stavba
zařazen	zařadit	k5eAaPmNgInS	zařadit
na	na	k7c4	na
listinu	listina	k1gFnSc4	listina
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Suomenlinna	Suomenlinn	k1gInSc2	Suomenlinn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fortress	Fortress	k1gInSc1	Fortress
of	of	k?	of
Suomenlinna	Suomenlinna	k1gFnSc1	Suomenlinna
-	-	kIx~	-
UNESCO	Unesco	k1gNnSc1	Unesco
World	Worlda	k1gFnPc2	Worlda
Heritage	Heritage	k1gNnSc2	Heritage
Centre	centr	k1gInSc5	centr
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Right	Right	k1gInSc1	Right
Place	plac	k1gInSc6	plac
Right	Right	k2eAgInSc1d1	Right
Time	Time	k1gInSc1	Time
<g/>
,	,	kIx,	,
cestovatelský	cestovatelský	k2eAgMnSc1d1	cestovatelský
journal	journat	k5eAaImAgMnS	journat
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
o	o	k7c4	o
Suomenlinně	Suomenlinně	k1gFnSc4	Suomenlinně
</s>
