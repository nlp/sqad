<p>
<s>
Autotrofie	Autotrofie	k1gFnSc1	Autotrofie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
autos	autos	k1gInSc1	autos
-	-	kIx~	-
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
trophe	trophe	k6eAd1	trophe
-	-	kIx~	-
výživa	výživa	k1gFnSc1	výživa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
získávání	získávání	k1gNnSc2	získávání
uhlíku	uhlík	k1gInSc2	uhlík
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
uhlíkatých	uhlíkatý	k2eAgInPc2d1	uhlíkatý
skeletů	skelet	k1gInPc2	skelet
vlastních	vlastní	k2eAgFnPc2d1	vlastní
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
u	u	k7c2	u
autotrofních	autotrofní	k2eAgInPc2d1	autotrofní
organismů	organismus	k1gInPc2	organismus
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
producentů	producent	k1gMnPc2	producent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
organismy	organismus	k1gInPc1	organismus
získávají	získávat	k5eAaImIp3nP	získávat
uhlík	uhlík	k1gInSc4	uhlík
z	z	k7c2	z
anorganických	anorganický	k2eAgFnPc2d1	anorganická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
oxidu	oxid	k1gInSc3	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
)	)	kIx)	)
a	a	k8xC	a
syntetizují	syntetizovat	k5eAaImIp3nP	syntetizovat
si	se	k3xPyFc3	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
uhlíkaté	uhlíkatý	k2eAgInPc1d1	uhlíkatý
řetězce	řetězec	k1gInPc1	řetězec
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
heterotrofních	heterotrofní	k2eAgInPc2d1	heterotrofní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
to	ten	k3xDgNnSc4	ten
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
a	a	k8xC	a
za	za	k7c4	za
zdroj	zdroj	k1gInSc4	zdroj
uhlíku	uhlík	k1gInSc2	uhlík
mají	mít	k5eAaImIp3nP	mít
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
jinými	jiný	k2eAgInPc7d1	jiný
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
organismy	organismus	k1gInPc1	organismus
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
heterotrofní	heterotrofní	k2eAgFnSc4d1	heterotrofní
a	a	k8xC	a
autotrofní	autotrofní	k2eAgFnSc4d1	autotrofní
výživu	výživa	k1gFnSc4	výživa
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
masožravé	masožravý	k2eAgFnPc1d1	masožravá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
pak	pak	k6eAd1	pak
označujeme	označovat	k5eAaImIp1nP	označovat
za	za	k7c4	za
mixotrofní	mixotrofní	k2eAgNnSc4d1	mixotrofní
<g/>
.	.	kIx.	.
</s>
<s>
Fotoautotrofní	Fotoautotrofní	k2eAgInPc1d1	Fotoautotrofní
organismy	organismus	k1gInPc1	organismus
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
řasy	řasa	k1gFnPc1	řasa
a	a	k8xC	a
sinice	sinice	k1gFnPc1	sinice
<g/>
,	,	kIx,	,
chemoautotrofní	chemoautotrofní	k2eAgInPc1d1	chemoautotrofní
organismy	organismus	k1gInPc1	organismus
jsou	být	k5eAaImIp3nP	být
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
lze	lze	k6eAd1	lze
také	také	k9	také
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
autotrofní	autotrofní	k2eAgInPc1d1	autotrofní
organismy	organismus	k1gInPc1	organismus
dokáží	dokázat	k5eAaPmIp3nP	dokázat
přeměnit	přeměnit	k5eAaPmF	přeměnit
anorganické	anorganický	k2eAgFnPc4d1	anorganická
látky	látka	k1gFnPc4	látka
na	na	k7c4	na
organické	organický	k2eAgFnPc4d1	organická
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
heterotrofních	heterotrofní	k2eAgInPc2d1	heterotrofní
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
</p>
