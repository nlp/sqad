<s>
Trent	Trent	k1gMnSc1	Trent
Reznor	Reznor	k1gMnSc1	Reznor
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1965	[number]	k4	1965
Mercer	Mercer	k1gMnSc1	Mercer
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
multiinstrumentalista	multiinstrumentalista	k1gMnSc1	multiinstrumentalista
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
skladatel	skladatel	k1gMnSc1	skladatel
Nine	Nin	k1gFnSc2	Nin
Inch	Inch	k1gMnSc1	Inch
Nails	Nailsa	k1gFnPc2	Nailsa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
deska	deska	k1gFnSc1	deska
Pretty	Pretta	k1gFnSc2	Pretta
Hate	Hat	k1gFnSc2	Hat
Machine	Machin	k1gInSc5	Machin
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgNnPc1d1	vydáno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
komerčně	komerčně	k6eAd1	komerčně
a	a	k8xC	a
kriticky	kriticky	k6eAd1	kriticky
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vydal	vydat	k5eAaPmAgMnS	vydat
osm	osm	k4xCc4	osm
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gInSc4	Reznor
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
hrál	hrát	k5eAaImAgInS	hrát
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
jako	jako	k9	jako
Option	Option	k1gInSc4	Option
30	[number]	k4	30
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Urge	Urge	k1gFnSc1	Urge
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Innocent	Innocent	k1gMnSc1	Innocent
či	či	k8xC	či
Exotic	Exotice	k1gFnPc2	Exotice
Birds	Birdsa	k1gFnPc2	Birdsa
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Nine	Nine	k1gInSc4	Nine
Inch	Inch	k1gInSc1	Inch
Nails	Nails	k1gInSc1	Nails
se	se	k3xPyFc4	se
produkuje	produkovat	k5eAaImIp3nS	produkovat
také	také	k9	také
alba	alba	k1gFnSc1	alba
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
Marilyn	Marilyn	k1gFnSc4	Marilyn
Manson	Mansona	k1gFnPc2	Mansona
či	či	k8xC	či
Saula	Saul	k1gMnSc2	Saul
Williamse	Williams	k1gMnSc2	Williams
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gInSc1	Reznor
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Mariqueen	Mariquena	k1gFnPc2	Mariquena
Maandig	Maandiga	k1gFnPc2	Maandiga
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
post-industrialní	postndustrialný	k2eAgMnPc1d1	post-industrialný
skupiny	skupina	k1gFnSc2	skupina
How	How	k1gFnSc1	How
to	ten	k3xDgNnSc4	ten
Destroy	Destroa	k1gFnPc1	Destroa
Angels	Angelsa	k1gFnPc2	Angelsa
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
s	s	k7c7	s
Atticus	Atticus	k1gInSc1	Atticus
Rossem	Ross	k1gInSc7	Ross
a	a	k8xC	a
dlouhodobým	dlouhodobý	k2eAgInSc7d1	dlouhodobý
členem	člen	k1gInSc7	člen
Nine	Nine	k1gFnSc1	Nine
Inch	Inch	k1gMnSc1	Inch
Nails	Nails	k1gInSc1	Nails
Robem	Robem	k?	Robem
Sheridanem	Sheridan	k1gInSc7	Sheridan
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gInSc4	Reznor
a	a	k8xC	a
Ross	Ross	k1gInSc4	Ross
složili	složit	k5eAaPmAgMnP	složit
mnoho	mnoho	k4c4	mnoho
soundtracků	soundtrack	k1gInPc2	soundtrack
pro	pro	k7c4	pro
filmy	film	k1gInPc4	film
Davida	David	k1gMnSc2	David
Finchera	Fincher	k1gMnSc2	Fincher
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Social	Social	k1gInSc1	Social
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nenávidí	nenávidět	k5eAaImIp3nP	nenávidět
ženy	žena	k1gFnPc4	žena
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zmizelá	zmizelý	k2eAgFnSc1d1	Zmizelá
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
ocenění	ocenění	k1gNnSc4	ocenění
Akademie	akademie	k1gFnSc2	akademie
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
filmový	filmový	k2eAgInSc4d1	filmový
soundstrack	soundstrack	k1gInSc4	soundstrack
za	za	k7c7	za
The	The	k1gFnSc7	The
Social	Social	k1gMnSc1	Social
Network	network	k1gInPc4	network
a	a	k8xC	a
Grammy	Gramm	k1gInPc4	Gramm
za	za	k7c7	za
Muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nenávidí	nenávidět	k5eAaImIp3nP	nenávidět
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gMnSc1	Reznor
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
New	New	k1gFnSc6	New
Castelu	Castel	k1gInSc2	Castel
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
rodičům	rodič	k1gMnPc3	rodič
Nany	Nan	k2eAgFnPc4d1	Nana
Lou	Lou	k1gFnPc4	Lou
a	a	k8xC	a
Michaeli	Michael	k1gMnSc5	Michael
Reznorovi	Reznorovi	k1gRnPc1	Reznorovi
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
německý	německý	k2eAgMnSc1d1	německý
a	a	k8xC	a
irský	irský	k2eAgInSc1d1	irský
původ	původ	k1gInSc1	původ
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc7	jeho
rodiče	rodič	k1gMnPc1	rodič
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
žil	žít	k5eAaImAgMnS	žít
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
prarodiči	prarodič	k1gMnPc7	prarodič
v	v	k7c6	v
Merceru	Mercer	k1gInSc6	Mercer
v	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Tera	Ter	k2eAgFnSc1d1	Tera
žila	žít	k5eAaImAgFnS	žít
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gInSc1	Reznor
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
již	již	k6eAd1	již
vykazoval	vykazovat	k5eAaImAgMnS	vykazovat
známky	známka	k1gFnPc4	známka
nadání	nadání	k1gNnSc2	nadání
pro	pro	k7c4	pro
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
učil	učít	k5eAaPmAgMnS	učít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
Saxofon	saxofon	k1gInSc4	saxofon
a	a	k8xC	a
Tuba	tuba	k1gFnSc1	tuba
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
jazzové	jazzový	k2eAgFnSc2d1	jazzová
a	a	k8xC	a
pochodové	pochodový	k2eAgFnSc2d1	pochodová
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gMnSc1	Reznor
též	též	k9	též
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
Ježíšem	Ježíš	k1gMnSc7	Ježíš
v	v	k7c4	v
Jesus	Jesus	k1gInSc4	Jesus
Christ	Christ	k1gInSc4	Christ
Superstar	superstar	k1gFnSc2	superstar
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
Allegheny	Alleghen	k2eAgInPc4d1	Alleghen
College	Colleg	k1gInPc4	Colleg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
techniku	technika	k1gFnSc4	technika
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
studentem	student	k1gMnSc7	student
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
Reznor	Reznor	k1gMnSc1	Reznor
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
Option	Option	k1gInSc1	Option
30	[number]	k4	30
<g/>
,	,	kIx,	,
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
tři	tři	k4xCgInPc4	tři
koncerty	koncert	k1gInPc4	koncert
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
vysoké	vysoká	k1gFnSc6	vysoká
je	být	k5eAaImIp3nS	být
Reznor	Reznor	k1gInSc4	Reznor
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
Ohia	Ohio	k1gNnSc2	Ohio
v	v	k7c6	v
Clevelandu	Clevelando	k1gNnSc6	Clevelando
začít	začít	k5eAaPmF	začít
hudební	hudební	k2eAgFnSc4d1	hudební
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
Urge	Urge	k1gFnSc4	Urge
<g/>
,	,	kIx,	,
cover	cover	k1gMnSc1	cover
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
do	do	k7c2	do
The	The	k1gFnSc2	The
Innocent	Innocent	k1gMnSc1	Innocent
jako	jako	k8xC	jako
klávesista	klávesista	k1gMnSc1	klávesista
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
-	-	kIx~	-
Livin	Livin	k1gInSc1	Livin
<g/>
'	'	kIx"	'
in	in	k?	in
the	the	k?	the
Street	Street	k1gInSc1	Street
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Reznor	Reznor	k1gMnSc1	Reznor
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Exotic	Exotice	k1gFnPc2	Exotice
Birds	Birdsa	k1gFnPc2	Birdsa
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Light	Light	k1gMnSc1	Light
of	of	k?	of
Day	Day	k1gMnSc1	Day
jako	jako	k8xS	jako
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
skupina	skupina	k1gFnSc1	skupina
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
The	The	k1gFnSc1	The
Problems	Problemsa	k1gFnPc2	Problemsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
též	též	k9	též
dělal	dělat	k5eAaImAgMnS	dělat
klávesistu	klávesista	k1gMnSc4	klávesista
skupině	skupina	k1gFnSc6	skupina
Slam	sláma	k1gFnPc2	sláma
Bamboo	Bamboo	k6eAd1	Bamboo
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gMnSc1	Reznor
získal	získat	k5eAaPmAgMnS	získat
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
Cleveland	Clevelanda	k1gFnPc2	Clevelanda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Right	Right	k1gInSc1	Right
Track	Tracka	k1gFnPc2	Tracka
Studio	studio	k1gNnSc1	studio
jako	jako	k8xS	jako
technik	technik	k1gMnSc1	technik
a	a	k8xC	a
vrátný	vrátný	k1gMnSc1	vrátný
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gMnSc1	Reznor
zde	zde	k6eAd1	zde
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
své	svůj	k3xOyFgNnSc4	svůj
dema	dema	k6eAd1	dema
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
prvních	první	k4xOgFnPc2	první
nahrávek	nahrávka	k1gFnPc2	nahrávka
Nine	Nin	k1gFnSc2	Nin
Inch	Inch	k1gMnSc1	Inch
Nails	Nailsa	k1gFnPc2	Nailsa
nebyl	být	k5eNaImAgMnS	být
Reznor	Reznor	k1gMnSc1	Reznor
schopen	schopen	k2eAgMnSc1d1	schopen
sehnat	sehnat	k5eAaPmF	sehnat
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
dokázala	dokázat	k5eAaPmAgFnS	dokázat
písně	píseň	k1gFnPc4	píseň
zahrát	zahrát	k5eAaPmF	zahrát
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
Princem	princ	k1gMnSc7	princ
a	a	k8xC	a
nahrál	nahrát	k5eAaBmAgMnS	nahrát
všechny	všechen	k3xTgInPc4	všechen
nástroje	nástroj	k1gInPc4	nástroj
kromě	kromě	k7c2	kromě
bubnů	buben	k1gInPc2	buben
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
postupoval	postupovat	k5eAaImAgMnS	postupovat
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
ostatních	ostatní	k2eAgNnPc6d1	ostatní
albech	album	k1gNnPc6	album
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
pozval	pozvat	k5eAaPmAgMnS	pozvat
jiné	jiný	k2eAgMnPc4d1	jiný
hudebníky	hudebník	k1gMnPc4	hudebník
a	a	k8xC	a
asistenty	asistent	k1gMnPc4	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
studií	studio	k1gNnPc2	studio
přívětivě	přívětivě	k6eAd1	přívětivě
odpovědělo	odpovědět	k5eAaPmAgNnS	odpovědět
na	na	k7c4	na
demo	demo	k2eAgMnSc4d1	demo
a	a	k8xC	a
Reznor	Reznor	k1gInSc1	Reznor
se	se	k3xPyFc4	se
podepsal	podepsat	k5eAaPmAgInS	podepsat
po	po	k7c6	po
TVT	TVT	kA	TVT
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
dema	demum	k1gNnSc2	demum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
neoficiálně	oficiálně	k6eNd1	oficiálně
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c4	v
88	[number]	k4	88
jako	jako	k8xS	jako
Purest	Purest	k1gMnSc1	Purest
Feeling	Feeling	k1gInSc4	Feeling
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
předěláno	předělat	k5eAaPmNgNnS	předělat
a	a	k8xC	a
znovuvydáno	znovuvydat	k5eAaPmNgNnS	znovuvydat
na	na	k7c4	na
Pretty	Prett	k1gMnPc4	Prett
Hate	Hatus	k1gMnSc5	Hatus
Machine	Machin	k1gMnSc5	Machin
-	-	kIx~	-
Reznorova	Reznorův	k2eAgFnSc1d1	Reznorova
první	první	k4xOgFnSc1	první
nahrávka	nahrávka	k1gFnSc1	nahrávka
pod	pod	k7c7	pod
Nine	Nine	k1gFnSc7	Nine
Inch	Incha	k1gFnPc2	Incha
Nails	Nailsa	k1gFnPc2	Nailsa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Nine	Nine	k1gFnSc1	Nine
Inch	Inch	k1gInSc4	Inch
Nails	Nails	k1gInSc1	Nails
Většina	většina	k1gFnSc1	většina
Reznorovy	Reznorův	k2eAgFnSc2d1	Reznorova
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xC	jako
Nine	Nine	k1gFnSc1	Nine
Inch	Incha	k1gFnPc2	Incha
Nails	Nailsa	k1gFnPc2	Nailsa
<g/>
.	.	kIx.	.
</s>
<s>
Pretty	Pretta	k1gFnPc1	Pretta
Hate	Hat	k1gFnSc2	Hat
Machine	Machin	k1gInSc5	Machin
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
1989	[number]	k4	1989
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
celkově	celkově	k6eAd1	celkově
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
předešel	předejít	k5eAaPmAgMnS	předejít
zasahování	zasahování	k1gNnSc1	zasahování
studio	studio	k1gNnSc1	studio
do	do	k7c2	do
alba	album	k1gNnSc2	album
začal	začít	k5eAaPmAgInS	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
pseudonymy	pseudonym	k1gInPc7	pseudonym
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc4	výsledek
bylo	být	k5eAaImAgNnS	být
EP	EP	kA	EP
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Broken	Broken	k1gInSc4	Broken
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nine	Nine	k6eAd1	Nine
Inch	Inch	k1gInSc1	Inch
Nails	Nailsa	k1gFnPc2	Nailsa
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c4	na
Lollapalooze	Lollapalooz	k1gInSc5	Lollapalooz
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Grammy	Gramma	k1gFnPc4	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
metalovou	metalový	k2eAgFnSc4d1	metalová
píseň	píseň	k1gFnSc4	píseň
-	-	kIx~	-
Wish	Wish	k1gInSc1	Wish
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
The	The	k1gMnPc2	The
Downward	Downward	k1gInSc4	Downward
Spiral	Spiral	k1gFnPc2	Spiral
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
1994	[number]	k4	1994
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nahrávání	nahrávání	k1gNnSc3	nahrávání
se	se	k3xPyFc4	se
Reznor	Reznor	k1gInSc1	Reznor
přesunul	přesunout	k5eAaPmAgInS	přesunout
do	do	k7c2	do
10050	[number]	k4	10050
Cielo	Cielo	k1gNnSc1	Cielo
Drive	drive	k1gInSc4	drive
mansion	mansion	k1gInSc4	mansion
-	-	kIx~	-
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vraždila	vraždit	k5eAaImAgFnS	vraždit
Mansonova	Mansonův	k2eAgFnSc1d1	Mansonova
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgInS	postavit
zde	zde	k6eAd1	zde
studio	studio	k1gNnSc4	studio
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
Le	Le	k1gFnSc4	Le
Pig	Pig	k1gFnSc2	Pig
-	-	kIx~	-
po	po	k7c6	po
slovu	slovo	k1gNnSc6	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
na	na	k7c6	na
dveřích	dveře	k1gFnPc6	dveře
krví	krvit	k5eAaImIp3nS	krvit
Sharon	Sharon	k1gInSc1	Sharon
Tate	Tat	k1gFnSc2	Tat
jejími	její	k3xOp3gMnPc7	její
vrahy	vrah	k1gMnPc7	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Nine	Nine	k6eAd1	Nine
Inch	Inch	k1gInSc1	Inch
Nails	Nailsa	k1gFnPc2	Nailsa
hojně	hojně	k6eAd1	hojně
vystupovalo	vystupovat	k5eAaImAgNnS	vystupovat
několik	několik	k4yIc1	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
Woodstocku	Woodstock	k1gInSc6	Woodstock
94	[number]	k4	94
<g/>
'	'	kIx"	'
i	i	k8xC	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechtěli	chtít	k5eNaImAgMnP	chtít
vystupovat	vystupovat	k5eAaImF	vystupovat
na	na	k7c6	na
velkých	velký	k2eAgInPc6d1	velký
prostorech	prostor	k1gInPc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
hudbě	hudba	k1gFnSc6	hudba
Nine	Nin	k1gInSc2	Nin
Inch	Inch	k1gInSc4	Inch
Nails	Nails	k1gInSc4	Nails
odrážela	odrážet	k5eAaImAgFnS	odrážet
Reznorova	Reznorův	k2eAgFnSc1d1	Reznorova
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
drogách	droga	k1gFnPc6	droga
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
studijní	studijní	k2eAgInSc4d1	studijní
perfekcionismus	perfekcionismus	k1gInSc4	perfekcionismus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
vydal	vydat	k5eAaPmAgInS	vydat
double	double	k2eAgNnSc4d1	double
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Fragile	Fragila	k1gFnSc3	Fragila
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
uspěšnost	uspěšnost	k1gFnSc4	uspěšnost
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
Reznorovo	Reznorův	k2eAgNnSc1d1	Reznorův
studium	studium	k1gNnSc1	studium
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
následující	následující	k2eAgInSc1d1	následující
tour	tour	k1gInSc1	tour
musel	muset	k5eAaImAgInS	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
následovalo	následovat	k5eAaImAgNnS	následovat
album	album	k1gNnSc1	album
With	Witha	k1gFnPc2	Witha
Teeth	Teetha	k1gFnPc2	Teetha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
se	se	k3xPyFc4	se
Reznor	Reznor	k1gInSc1	Reznor
vyléčil	vyléčit	k5eAaPmAgInS	vyléčit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovalo	následovat	k5eAaImAgNnS	následovat
Year	Year	k1gInSc4	Year
Zero	Zero	k6eAd1	Zero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kritizovalo	kritizovat	k5eAaImAgNnS	kritizovat
americkou	americký	k2eAgFnSc4d1	americká
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
Year	Year	k1gInSc4	Year
Zero	Zero	k6eAd1	Zero
přišly	přijít	k5eAaPmAgFnP	přijít
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
Ghost	Ghost	k1gMnSc1	Ghost
I-IV	I-IV	k1gMnSc1	I-IV
a	a	k8xC	a
The	The	k1gMnSc1	The
Slip	slip	k1gInSc1	slip
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgNnPc4	dva
zdarma	zdarma	k6eAd1	zdarma
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
následovalo	následovat	k5eAaImAgNnS	následovat
Wave	Wave	k1gNnSc1	Wave
Goodbay	Goodbaa	k1gFnSc2	Goodbaa
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
poslední	poslední	k2eAgNnSc1d1	poslední
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Hesitation	Hesitation	k1gInSc4	Hesitation
Marks	Marksa	k1gFnPc2	Marksa
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
začalo	začít	k5eAaPmAgNnS	začít
Tension	Tension	k1gInSc4	Tension
2013	[number]	k4	2013
tour	toura	k1gFnPc2	toura
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
spoluprací	spolupráce	k1gFnPc2	spolupráce
Reznora	Reznor	k1gMnSc2	Reznor
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
Ministry	ministr	k1gMnPc7	ministr
<g/>
,	,	kIx,	,
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
projekt	projekt	k1gInSc1	projekt
s	s	k7c7	s
názvem	název	k1gInSc7	název
1000	[number]	k4	1000
Homo	Homo	k1gMnSc1	Homo
DJs	DJs	k1gMnSc1	DJs
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gMnSc1	Reznor
coveroval	coverovat	k5eAaImAgMnS	coverovat
píseň	píseň	k1gFnSc4	píseň
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
Supernaut	Supernaut	k1gMnSc1	Supernaut
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
hlas	hlas	k1gInSc1	hlas
byl	být	k5eAaImAgInS	být
zkreslen	zkreslit	k5eAaPmNgInS	zkreslit
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
Reznor	Reznor	k1gMnSc1	Reznor
zpíval	zpívat	k5eAaImAgMnS	zpívat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Pigface	Pigface	k1gFnSc1	Pigface
píseň	píseň	k1gFnSc1	píseň
Suck	Suck	k1gInSc4	Suck
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Gub	guba	k1gFnPc2	guba
<g/>
.	.	kIx.	.
</s>
<s>
Postaral	postarat	k5eAaPmAgMnS	postarat
se	se	k3xPyFc4	se
o	o	k7c4	o
produkování	produkování	k1gNnPc4	produkování
alb	alba	k1gFnPc2	alba
Marilyna	Marilyna	k1gFnSc1	Marilyna
Mansona	Mansona	k1gFnSc1	Mansona
<g/>
:	:	kIx,	:
Portrait	Portrait	k1gInSc1	Portrait
of	of	k?	of
an	an	k?	an
American	American	k1gInSc4	American
Family	Famila	k1gFnSc2	Famila
<g/>
,	,	kIx,	,
Smells	Smells	k1gInSc4	Smells
Like	Lik	k1gFnSc2	Lik
ChildrenAantichrist	ChildrenAantichrist	k1gInSc4	ChildrenAantichrist
Superstar	superstar	k1gFnSc2	superstar
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gInSc1	Reznor
a	a	k8xC	a
Manson	Manson	k1gInSc1	Manson
se	se	k3xPyFc4	se
mnohokrát	mnohokrát	k6eAd1	mnohokrát
pohádali	pohádat	k5eAaPmAgMnP	pohádat
a	a	k8xC	a
Manson	Manson	k1gInSc4	Manson
později	pozdě	k6eAd2	pozdě
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
Musel	muset	k5eAaImAgMnS	muset
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
vybrat	vybrat	k5eAaPmF	vybrat
jestli	jestli	k8xS	jestli
budu	být	k5eAaImBp1nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
vykašlat	vykašlat	k5eAaPmF	vykašlat
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
úspěchu	úspěch	k1gInSc6	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Nedokáže	dokázat	k5eNaPmIp3nS	dokázat
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
více	hodně	k6eAd2	hodně
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Reznor	Reznor	k1gMnSc1	Reznor
produkoval	produkovat	k5eAaImAgMnS	produkovat
soundtracky	soundtrack	k1gInPc4	soundtrack
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Olivera	Oliver	k1gMnSc2	Oliver
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
Takoví	takový	k3xDgMnPc1	takový
normální	normální	k2eAgMnPc1d1	normální
zabijáci	zabiják	k1gMnPc1	zabiják
a	a	k8xC	a
film	film	k1gInSc1	film
Davida	David	k1gMnSc2	David
Lynche	Lynch	k1gMnSc2	Lynch
Lost	Lost	k1gMnSc1	Lost
Highway	Highwaa	k1gMnSc2	Highwaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
odehrál	odehrát	k5eAaPmAgInS	odehrát
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
,,	,,	k?	,,
<g/>
sólo	sólo	k2eAgInSc4d1	sólo
<g/>
"	"	kIx"	"
koncert	koncert	k1gInSc4	koncert
na	na	k7c4	na
Bridge	Bridge	k1gNnSc4	Bridge
School	School	k1gInSc4	School
Benefit	Benefit	k2eAgInSc4d1	Benefit
show	show	k1gNnSc4	show
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gMnSc1	Reznor
též	též	k9	též
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
Queens	Queens	k1gInSc1	Queens
of	of	k?	of
the	the	k?	the
Stone	ston	k1gInSc5	ston
Age	Age	k1gMnPc4	Age
<g/>
.	.	kIx.	.
</s>
<s>
Nahrál	nahrát	k5eAaBmAgMnS	nahrát
píseň	píseň	k1gFnSc4	píseň
Mantra	mantra	k1gFnSc1	mantra
s	s	k7c7	s
Dave	Dav	k1gInSc5	Dav
Grohlem	Grohl	k1gInSc7	Grohl
a	a	k8xC	a
Joshem	Josh	k1gInSc7	Josh
Hommem	Hommo	k1gNnSc7	Hommo
pro	pro	k7c4	pro
soundtrack	soundtrack	k1gInSc4	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
Sound	Sounda	k1gFnPc2	Sounda
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
Reznor	Reznor	k1gMnSc1	Reznor
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Mariqueen	Mariquena	k1gFnPc2	Mariquena
Maandig	Maandiga	k1gFnPc2	Maandiga
a	a	k8xC	a
s	s	k7c7	s
Atticus	Atticus	k1gMnSc1	Atticus
Rossem	Ross	k1gMnSc7	Ross
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
How	How	k1gFnSc4	How
to	ten	k3xDgNnSc4	ten
Destroy	Destroa	k1gFnPc1	Destroa
Angels	Angelsa	k1gFnPc2	Angelsa
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
EP	EP	kA	EP
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
a	a	k8xC	a
nahrála	nahrát	k5eAaBmAgFnS	nahrát
píseň	píseň	k1gFnSc4	píseň
pro	pro	k7c4	pro
Muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nenávidí	návidět	k5eNaImIp3nP	návidět
ženy	žena	k1gFnPc4	žena
s	s	k7c7	s
názvem	název	k1gInSc7	název
Is	Is	k1gMnSc1	Is
Your	Your	k1gMnSc1	Your
Love	lov	k1gInSc5	lov
Strong	Strong	k1gMnSc1	Strong
Enough	Enough	k1gInSc1	Enough
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
An	An	k?	An
Omen	omen	k1gNnSc1	omen
EP	EP	kA	EP
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
a	a	k8xC	a
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
albu	album	k1gNnSc6	album
skupiny	skupina	k1gFnSc2	skupina
-	-	kIx~	-
Welcome	Welcom	k1gInSc5	Welcom
Oblivion	Oblivion	k1gInSc4	Oblivion
<g/>
.	.	kIx.	.
</s>
<s>
Reznor	Reznor	k1gMnSc1	Reznor
složil	složit	k5eAaPmAgMnS	složit
originální	originální	k2eAgInSc4d1	originální
soundtrack	soundtrack	k1gInSc4	soundtrack
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
Quake	Quak	k1gInSc2	Quak
od	od	k7c2	od
id	idy	k1gFnPc2	idy
Software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
nahrát	nahrát	k5eAaBmF	nahrát
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
ambient	ambient	k1gInSc4	ambient
audio	audio	k2eAgInSc4d1	audio
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
NIN	Nina	k1gFnPc2	Nina
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
bednách	bedna	k1gFnPc6	bedna
s	s	k7c7	s
municí	munice	k1gFnSc7	munice
<g/>
.	.	kIx.	.
</s>
<s>
Reznorova	Reznorův	k2eAgFnSc1d1	Reznorova
spolupráce	spolupráce	k1gFnSc1	spolupráce
začala	začít	k5eAaPmAgFnS	začít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
fanouškem	fanoušek	k1gMnSc7	fanoušek
Dooma	Doomum	k1gNnSc2	Doomum
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
chtěl	chtít	k5eAaImAgMnS	chtít
spojil	spojit	k5eAaPmAgMnS	spojit
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
hudby	hudba	k1gFnSc2	hudba
pro	pro	k7c4	pro
Doom	Doom	k1gInSc4	Doom
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
času	čas	k1gInSc2	čas
a	a	k8xC	a
peněz	peníze	k1gInPc2	peníze
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgInS	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Year	Yeara	k1gFnPc2	Yeara
Zero	Zero	k6eAd1	Zero
vydaly	vydat	k5eAaPmAgFnP	vydat
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
hru	hra	k1gFnSc4	hra
z	z	k7c2	z
alternativní	alternativní	k2eAgFnSc2d1	alternativní
reality	realita	k1gFnSc2	realita
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
složil	složit	k5eAaPmAgMnS	složit
theme	themat	k5eAaPmIp3nS	themat
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
Call	Call	k1gInSc4	Call
of	of	k?	of
Duty	Duty	k?	Duty
<g/>
:	:	kIx,	:
Black	Black	k1gMnSc1	Black
Ops	Ops	k1gFnSc2	Ops
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trent	Trenta	k1gFnPc2	Trenta
Reznor	Reznora	k1gFnPc2	Reznora
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Trent	Trent	k1gInSc1	Trent
Reznor	Reznor	k1gInSc1	Reznor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Trent	Trent	k1gInSc1	Trent
Reznor	Reznor	k1gInSc1	Reznor
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Nine	Nine	k1gNnSc7	Nine
Inch	Incha	k1gFnPc2	Incha
Nails	Nails	k1gInSc4	Nails
České	český	k2eAgFnSc2d1	Česká
fandovské	fandovský	k2eAgFnSc2d1	fandovská
stránky	stránka	k1gFnSc2	stránka
Nine	Nine	k1gFnSc1	Nine
Inch	Inch	k1gMnSc1	Inch
Nails	Nails	k1gInSc1	Nails
</s>
