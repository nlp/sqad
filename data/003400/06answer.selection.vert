<s>
Největšími	veliký	k2eAgInPc7d3	veliký
svaly	sval	k1gInPc7	sval
na	na	k7c6	na
ptačím	ptačí	k2eAgNnSc6d1	ptačí
těle	tělo	k1gNnSc6	tělo
jsou	být	k5eAaImIp3nP	být
hrudní	hrudní	k2eAgInPc1d1	hrudní
svaly	sval	k1gInPc1	sval
(	(	kIx(	(
<g/>
mm	mm	kA	mm
<g/>
.	.	kIx.	.
pectorales	pectorales	k1gMnSc1	pectorales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
upínající	upínající	k2eAgFnSc4d1	upínající
se	se	k3xPyFc4	se
na	na	k7c4	na
hřeben	hřeben	k1gInSc4	hřeben
kosti	kost	k1gFnSc2	kost
hrudní	hrudní	k2eAgFnSc2d1	hrudní
a	a	k8xC	a
pohybující	pohybující	k2eAgFnSc2d1	pohybující
křídlem	křídlo	k1gNnSc7	křídlo
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
antagonista	antagonista	k1gMnSc1	antagonista
m.	m.	k?	m.
supracoracoideus	supracoracoideus	k1gMnSc1	supracoracoideus
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
vykonávající	vykonávající	k2eAgInSc1d1	vykonávající
pohyb	pohyb	k1gInSc1	pohyb
křídel	křídlo	k1gNnPc2	křídlo
směrem	směr	k1gInSc7	směr
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
