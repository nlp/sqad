<s>
Čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
(	(	kIx(	(
<g/>
cizím	cizit	k5eAaImIp1nS	cizit
slovem	slovem	k6eAd1	slovem
tetragon	tetragon	k1gInSc4	tetragon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rovinný	rovinný	k2eAgInSc1d1	rovinný
geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
mnohoúhelník	mnohoúhelník	k1gInSc1	mnohoúhelník
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
vrcholy	vrchol	k1gInPc7	vrchol
a	a	k8xC	a
čtyřmi	čtyři	k4xCgFnPc7	čtyři
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
