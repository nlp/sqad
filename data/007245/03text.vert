<s>
Čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
(	(	kIx(	(
<g/>
cizím	cizit	k5eAaImIp1nS	cizit
slovem	slovem	k6eAd1	slovem
tetragon	tetragon	k1gInSc4	tetragon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rovinný	rovinný	k2eAgInSc1d1	rovinný
geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
mnohoúhelník	mnohoúhelník	k1gInSc1	mnohoúhelník
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
vrcholy	vrchol	k1gInPc7	vrchol
a	a	k8xC	a
čtyřmi	čtyři	k4xCgFnPc7	čtyři
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc1	součet
velikostí	velikost	k1gFnSc7	velikost
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
úhlů	úhel	k1gInPc2	úhel
čtyřúhelníka	čtyřúhelník	k1gInSc2	čtyřúhelník
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
360	[number]	k4	360
<g/>
°	°	k?	°
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
π	π	k?	π
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
úhlopříčkou	úhlopříčka	k1gFnSc7	úhlopříčka
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Obvod	obvod	k1gInSc1	obvod
čtyřúhelníku	čtyřúhelník	k1gInSc2	čtyřúhelník
o	o	k7c6	o
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	O	kA	O
=	=	kIx~	=
a	a	k8xC	a
+	+	kIx~	+
b	b	k?	b
+	+	kIx~	+
c	c	k0	c
+	+	kIx~	+
d	d	k?	d
=	=	kIx~	=
2	[number]	k4	2
s	s	k7c7	s
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
O	o	k7c4	o
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
+	+	kIx~	+
<g/>
d	d	k?	d
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
s	s	k7c7	s
je	být	k5eAaImIp3nS	být
poloviční	poloviční	k2eAgInSc1d1	poloviční
obvod	obvod	k1gInSc1	obvod
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
vzorcích	vzorec	k1gInPc6	vzorec
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
čtyřúhelníka	čtyřúhelník	k1gInSc2	čtyřúhelník
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
f	f	k?	f
sin	sin	kA	sin
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
ef	ef	k?	ef
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
e	e	k0	e
<g/>
,	,	kIx,	,
f	f	k?	f
jsou	být	k5eAaImIp3nP	být
délky	délka	k1gFnPc4	délka
úhlopříček	úhlopříčka	k1gFnPc2	úhlopříčka
a	a	k8xC	a
φ	φ	k?	φ
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
libovolný	libovolný	k2eAgInSc4d1	libovolný
<g/>
)	)	kIx)	)
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
svírají	svírat	k5eAaImIp3nP	svírat
<g/>
.	.	kIx.	.
</s>
<s>
Konvexní	konvexní	k2eAgInSc1d1	konvexní
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
má	mít	k5eAaImIp3nS	mít
všechny	všechen	k3xTgInPc4	všechen
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
úhly	úhel	k1gInPc4	úhel
konvexní	konvexní	k2eAgInPc4d1	konvexní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
obsah	obsah	k1gInSc4	obsah
platí	platit	k5eAaImIp3nS	platit
Bretschneiderův	Bretschneiderův	k2eAgInSc4d1	Bretschneiderův
vzorec	vzorec	k1gInSc4	vzorec
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
s	s	k7c7	s
−	−	k?	−
a	a	k8xC	a
)	)	kIx)	)
(	(	kIx(	(
s	s	k7c7	s
−	−	k?	−
b	b	k?	b
)	)	kIx)	)
(	(	kIx(	(
s	s	k7c7	s
−	−	k?	−
c	c	k0	c
)	)	kIx)	)
(	(	kIx(	(
s	s	k7c7	s
−	−	k?	−
d	d	k?	d
)	)	kIx)	)
−	−	k?	−
a	a	k8xC	a
b	b	k?	b
c	c	k0	c
d	d	k?	d
⋅	⋅	k?	⋅
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
+	+	kIx~	+
γ	γ	k?	γ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
s-a	s	k?	s-a
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
s-b	s	k?	s-b
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
s-c	s	k?	s-c
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
s-d	s	k?	s-d
<g/>
)	)	kIx)	)
<g/>
-abcd	bcd	k1gMnSc1	-abcd
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
cos	cos	kA	cos
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
a	a	k8xC	a
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
jsou	být	k5eAaImIp3nP	být
strany	strana	k1gFnPc4	strana
čtyřúhelníka	čtyřúhelník	k1gInSc2	čtyřúhelník
<g/>
,	,	kIx,	,
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
poloviční	poloviční	k2eAgInSc1d1	poloviční
obvod	obvod	k1gInSc1	obvod
<g/>
,	,	kIx,	,
α	α	k?	α
a	a	k8xC	a
γ	γ	k?	γ
úhly	úhel	k1gInPc1	úhel
při	při	k7c6	při
protilehlých	protilehlý	k2eAgInPc6d1	protilehlý
vrcholech	vrchol	k1gInPc6	vrchol
(	(	kIx(	(
<g/>
např.	např.	kA	např.
A	A	kA	A
a	a	k8xC	a
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
posloupností	posloupnost	k1gFnSc7	posloupnost
stran	strana	k1gFnPc2	strana
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
obsah	obsah	k1gInSc4	obsah
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tětivový	tětivový	k2eAgInSc1d1	tětivový
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
vypočíst	vypočíst	k5eAaPmF	vypočíst
pomocí	pomocí	k7c2	pomocí
rozdělení	rozdělení	k1gNnSc2	rozdělení
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
<g/>
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
*	*	kIx~	*
<g/>
b	b	k?	b
<g/>
*	*	kIx~	*
<g/>
sin	sin	kA	sin
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
+	+	kIx~	+
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
*	*	kIx~	*
<g/>
c	c	k0	c
<g/>
*	*	kIx~	*
<g/>
sinC	sinC	k?	sinC
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Ke	k	k7c3	k
konstrukci	konstrukce	k1gFnSc3	konstrukce
obecného	obecný	k2eAgInSc2d1	obecný
čtyřúhelníku	čtyřúhelník	k1gInSc2	čtyřúhelník
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
5	[number]	k4	5
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
vhodně	vhodně	k6eAd1	vhodně
rozdělíme	rozdělit	k5eAaPmIp1nP	rozdělit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
úhlopříčkou	úhlopříčka	k1gFnSc7	úhlopříčka
<g/>
)	)	kIx)	)
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
známe	znát	k5eAaImIp1nP	znát
3	[number]	k4	3
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
doplnění	doplnění	k1gNnSc3	doplnění
druhého	druhý	k4xOgInSc2	druhý
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
postačí	postačit	k5eAaPmIp3nP	postačit
2	[number]	k4	2
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
společnou	společný	k2eAgFnSc4d1	společná
stranu	strana	k1gFnSc4	strana
již	již	k6eAd1	již
známe	znát	k5eAaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
nějaké	nějaký	k3yIgFnSc2	nějaký
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
narýsování	narýsování	k1gNnSc3	narýsování
stačí	stačit	k5eAaBmIp3nS	stačit
méně	málo	k6eAd2	málo
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
u	u	k7c2	u
rovnoběžníku	rovnoběžník	k1gInSc2	rovnoběžník
jen	jen	k9	jen
3	[number]	k4	3
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
čtverac	čtverac	k1gInSc4	čtverac
1	[number]	k4	1
prvek	prvek	k1gInSc1	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Geometrický	geometrický	k2eAgInSc4d1	geometrický
útvar	útvar	k1gInSc4	útvar
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čtyrúhelník	čtyrúhelník	k1gInSc1	čtyrúhelník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Geometrie	geometrie	k1gFnSc1	geometrie
živě	živě	k6eAd1	živě
–	–	k?	–
čtyřúhelníky	čtyřúhelník	k1gInPc1	čtyřúhelník
</s>
