<s>
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
Woolridge	Woolridg	k1gInSc2	Woolridg
Grant	grant	k1gInSc1	grant
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1985	[number]	k4	1985
Lake	Lak	k1gInSc2	Lak
Placid	Placid	k1gInSc1	Placid
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známá	k1gFnSc1	známá
pod	pod	k7c7	pod
uměleckým	umělecký	k2eAgInSc7d1	umělecký
pseudonymem	pseudonym	k1gInSc7	pseudonym
jako	jako	k8xC	jako
Lana	lano	k1gNnSc2	lano
Del	Del	k1gFnSc2	Del
Rey	Rea	k1gFnSc2	Rea
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
skladatelka	skladatelka	k1gFnSc1	skladatelka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
umělecký	umělecký	k2eAgInSc1d1	umělecký
pseudonym	pseudonym	k1gInSc1	pseudonym
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kombinací	kombinace	k1gFnPc2	kombinace
jména	jméno	k1gNnSc2	jméno
hollywoodské	hollywoodský	k2eAgFnSc2d1	hollywoodská
herečky	herečka	k1gFnSc2	herečka
Lany	lano	k1gNnPc7	lano
Turner	turner	k1gMnSc1	turner
a	a	k8xC	a
auta	auto	k1gNnPc1	auto
Ford	ford	k1gInSc1	ford
Del	Del	k1gMnSc4	Del
Rey	Rea	k1gFnSc2	Rea
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
gangsterskou	gangsterský	k2eAgFnSc4d1	gangsterská
Nancy	Nancy	k1gFnSc4	Nancy
Sinatru	Sinatr	k1gInSc2	Sinatr
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
doménového	doménový	k2eAgMnSc2d1	doménový
investora	investor	k1gMnSc2	investor
a	a	k8xC	a
milionáře	milionář	k1gMnSc2	milionář
Roberta	Robert	k1gMnSc2	Robert
Granta	Grant	k1gMnSc2	Grant
a	a	k8xC	a
matky	matka	k1gFnSc2	matka
Pat	pata	k1gFnPc2	pata
Grant	grant	k1gInSc1	grant
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
14	[number]	k4	14
let	léto	k1gNnPc2	léto
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
v	v	k7c6	v
Lake	Lake	k1gFnSc6	Lake
Placid	Placid	k1gInSc4	Placid
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
dvěma	dva	k4xCgInPc7	dva
sourozenci	sourozenec	k1gMnPc7	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
začínající	začínající	k2eAgFnSc3d1	začínající
závislosti	závislost	k1gFnSc3	závislost
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
poslána	poslán	k2eAgFnSc1d1	poslána
do	do	k7c2	do
internátní	internátní	k2eAgFnSc2d1	internátní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Connecticutu	Connecticut	k1gInSc6	Connecticut
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
období	období	k1gNnSc3	období
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgNnP	vyjádřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
GQ	GQ	kA	GQ
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
jsem	být	k5eAaImIp1nS	být
velký	velký	k2eAgMnSc1d1	velký
pijan	pijan	k1gMnSc1	pijan
<g/>
,	,	kIx,	,
pila	pila	k1gFnSc1	pila
jsem	být	k5eAaImIp1nS	být
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k8xC	i
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Myslela	myslet	k5eAaImAgFnS	myslet
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
celý	celý	k2eAgInSc1d1	celý
zatraceně	zatraceně	k6eAd1	zatraceně
hustý	hustý	k2eAgInSc1d1	hustý
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
byl	být	k5eAaImAgInS	být
moje	můj	k3xOp1gFnSc1	můj
první	první	k4xOgFnSc1	první
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomhle	tenhle	k3xDgInSc6	tenhle
období	období	k1gNnSc6	období
jsem	být	k5eAaImIp1nS	být
psala	psát	k5eAaImAgFnS	psát
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Born	Born	k1gNnSc1	Born
to	ten	k3xDgNnSc1	ten
Die	Die	k1gFnPc1	Die
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Když	když	k8xS	když
se	se	k3xPyFc4	se
zbavila	zbavit	k5eAaPmAgFnS	zbavit
závislosti	závislost	k1gFnPc4	závislost
<g/>
,	,	kIx,	,
šla	jít	k5eAaImAgFnS	jít
studovat	studovat	k5eAaImF	studovat
newyorskou	newyorský	k2eAgFnSc4d1	newyorská
Fordham	Fordham	k1gInSc4	Fordham
University	universita	k1gFnPc1	universita
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
obor	obor	k1gInSc1	obor
filozofie	filozofie	k1gFnSc2	filozofie
-	-	kIx~	-
metafyzika	metafyzika	k1gFnSc1	metafyzika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
mluvila	mluvit	k5eAaImAgFnS	mluvit
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Reflex	reflex	k1gInSc4	reflex
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Filozofie	filozofie	k1gFnSc1	filozofie
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
předmět	předmět	k1gInSc1	předmět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
ke	k	k7c3	k
mně	já	k3xPp1nSc3	já
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
promlouval	promlouvat	k5eAaImAgMnS	promlouvat
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
jsem	být	k5eAaImIp1nS	být
ráda	rád	k2eAgFnSc1d1	ráda
i	i	k9	i
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tohle	tenhle	k3xDgNnSc1	tenhle
mě	já	k3xPp1nSc4	já
opravdu	opravdu	k6eAd1	opravdu
hodně	hodně	k6eAd1	hodně
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Narazila	narazit	k5eAaPmAgFnS	narazit
jsem	být	k5eAaImIp1nS	být
tam	tam	k6eAd1	tam
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
řešili	řešit	k5eAaImAgMnP	řešit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
jsme	být	k5eAaImIp1nP	být
tady	tady	k6eAd1	tady
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsme	být	k5eAaImIp1nP	být
sem	sem	k6eAd1	sem
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
vnímáme	vnímat	k5eAaImIp1nP	vnímat
jako	jako	k8xC	jako
realitu	realita	k1gFnSc4	realita
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
své	své	k1gNnSc4	své
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Vždycky	vždycky	k6eAd1	vždycky
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
tyhle	tenhle	k3xDgFnPc4	tenhle
otázky	otázka	k1gFnPc4	otázka
kladla	klást	k5eAaImAgFnS	klást
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdo	nikdo	k3yNnSc1	nikdo
mi	já	k3xPp1nSc3	já
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Když	když	k8xS	když
jí	on	k3xPp3gFnSc7	on
bylo	být	k5eAaImAgNnS	být
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
přívěsu	přívěs	k1gInSc2	přívěs
za	za	k7c4	za
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
takový	takový	k3xDgInSc1	takový
styl	styl	k1gInSc1	styl
života	život	k1gInSc2	život
líbil	líbit	k5eAaImAgInS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
let	let	k1gInSc1	let
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
rehabilitačním	rehabilitační	k2eAgNnSc6d1	rehabilitační
centru	centrum	k1gNnSc6	centrum
pro	pro	k7c4	pro
alkoholiky	alkoholik	k1gMnPc4	alkoholik
a	a	k8xC	a
drogově	drogově	k6eAd1	drogově
závislé	závislý	k2eAgNnSc1d1	závislé
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
má	mít	k5eAaImIp3nS	mít
vytetováno	vytetován	k2eAgNnSc1d1	vytetováno
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
M	M	kA	M
<g/>
"	"	kIx"	"
odkazované	odkazovaný	k2eAgFnPc1d1	odkazovaná
její	její	k3xOp3gFnSc3	její
babičce	babička	k1gFnSc3	babička
Madeleine	Madeleine	k1gFnSc2	Madeleine
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
rukách	ruka	k1gFnPc6	ruka
má	mít	k5eAaImIp3nS	mít
vytetované	vytetovaný	k2eAgNnSc1d1	vytetované
"	"	kIx"	"
<g/>
Paradise	Paradise	k1gFnSc1	Paradise
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Trust	trust	k1gInSc1	trust
no	no	k9	no
one	one	k?	one
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prsteníčku	prsteníček	k1gInSc6	prsteníček
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
má	mít	k5eAaImIp3nS	mít
frázi	fráze	k1gFnSc4	fráze
"	"	kIx"	"
<g/>
Die	Die	k1gMnSc1	Die
young	young	k1gMnSc1	young
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
tetování	tetování	k1gNnSc1	tetování
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
-	-	kIx~	-
jména	jméno	k1gNnSc2	jméno
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Whitman	Whitman	k1gMnSc1	Whitman
a	a	k8xC	a
Nabokov	Nabokov	k1gInSc1	Nabokov
-	-	kIx~	-
a	a	k8xC	a
na	na	k7c6	na
hrudníku	hrudník	k1gInSc6	hrudník
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
Nina	Nina	k1gFnSc1	Nina
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Billie	Billie	k1gFnSc1	Billie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Popírá	popírat	k5eAaImIp3nS	popírat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
plastické	plastický	k2eAgFnPc4d1	plastická
operace	operace	k1gFnPc4	operace
včetně	včetně	k7c2	včetně
botoxu	botox	k1gInSc2	botox
ve	v	k7c6	v
rtech	ret	k1gInPc6	ret
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
dva	dva	k4xCgInPc4	dva
apartmány	apartmán	k1gInPc1	apartmán
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
frontmanem	frontman	k1gInSc7	frontman
skupiny	skupina	k1gFnSc2	skupina
Kasidy	Kasida	k1gFnSc2	Kasida
Barrie-James	Barrie-James	k1gMnSc1	Barrie-James
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neillem	Neill	k1gMnSc7	Neill
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
oficiálně	oficiálně	k6eAd1	oficiálně
skončil	skončit	k5eAaPmAgInS	skončit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnPc4	lano
už	už	k6eAd1	už
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
italským	italský	k2eAgMnSc7d1	italský
modním	modní	k1gMnSc7	modní
fotografem	fotograf	k1gMnSc7	fotograf
Francescem	Francesce	k1gMnSc7	Francesce
Carrozzinim	Carrozzinima	k1gFnPc2	Carrozzinima
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
strýc	strýc	k1gMnSc1	strýc
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
napsat	napsat	k5eAaBmF	napsat
milion	milion	k4xCgInSc4	milion
písniček	písnička	k1gFnPc2	písnička
jen	jen	k6eAd1	jen
se	s	k7c7	s
šesti	šest	k4xCc7	šest
akordy	akord	k1gInPc7	akord
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
nočních	noční	k2eAgInPc6d1	noční
klubech	klub	k1gInPc6	klub
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
pseudonymy	pseudonym	k1gInPc7	pseudonym
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Jump	Jump	k1gInSc1	Jump
Rope	Rope	k1gNnSc1	Rope
Queen	Queen	k1gInSc1	Queen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lizzy	Lizz	k1gInPc4	Lizz
Grant	grant	k1gInSc1	grant
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Phenomena	Phenomen	k2eAgFnSc1d1	Phenomena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přiznala	přiznat	k5eAaPmAgFnS	přiznat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměla	mít	k5eNaImAgFnS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
pokračovat	pokračovat	k5eAaImF	pokračovat
a	a	k8xC	a
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
kariérou	kariéra	k1gFnSc7	kariéra
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
to	ten	k3xDgNnSc4	ten
nebrala	brát	k5eNaImAgFnS	brát
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
album	album	k1gNnSc1	album
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
písněmi	píseň	k1gFnPc7	píseň
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Woolridge	Woolridg	k1gInSc2	Woolridg
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nese	nést	k5eAaImIp3nS	nést
dva	dva	k4xCgInPc4	dva
názvy	název	k1gInPc4	název
-	-	kIx~	-
Rock	rock	k1gInSc1	rock
Me	Me	k1gMnSc1	Me
Stable	Stable	k1gMnSc1	Stable
<g/>
;	;	kIx,	;
Young	Young	k1gMnSc1	Young
Like	Lik	k1gFnSc2	Lik
Me	Me	k1gMnSc1	Me
-	-	kIx~	-
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
názvy	název	k1gInPc7	název
písní	píseň	k1gFnPc2	píseň
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
neznámé	známý	k2eNgFnPc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
nahrála	nahrát	k5eAaPmAgFnS	nahrát
Sirens	Sirens	k1gInSc4	Sirens
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
May	May	k1gMnSc1	May
Jailer	Jailer	k1gMnSc1	Jailer
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
5	[number]	k4	5
Point	pointa	k1gFnPc2	pointa
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vydala	vydat	k5eAaPmAgFnS	vydat
svoje	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
EP	EP	kA	EP
Kill	Kill	k1gInSc1	Kill
Kill	Kill	k1gInSc1	Kill
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
písněmi	píseň	k1gFnPc7	píseň
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
plnohodnotné	plnohodnotný	k2eAgNnSc4d1	plnohodnotné
album	album	k1gNnSc4	album
jako	jako	k8xS	jako
Lana	lano	k1gNnPc4	lano
Del	Del	k1gFnPc2	Del
Ray	Ray	k1gFnSc1	Ray
a.k.a.	a.k.a.	k?	a.k.a.
Lizzy	Lizza	k1gFnSc2	Lizza
Grant	grant	k1gInSc1	grant
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Robert	Robert	k1gMnSc1	Robert
jí	jíst	k5eAaImIp3nS	jíst
pomohl	pomoct	k5eAaPmAgMnS	pomoct
s	s	k7c7	s
marketingem	marketing	k1gInSc7	marketing
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
staženo	stáhnout	k5eAaPmNgNnS	stáhnout
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Stranger	Strangra	k1gFnPc2	Strangra
Records	Records	k1gInSc4	Records
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
vydala	vydat	k5eAaPmAgFnS	vydat
debutový	debutový	k2eAgInSc4d1	debutový
singl	singl	k1gInSc4	singl
Video	video	k1gNnSc1	video
Games	Games	k1gInSc4	Games
<g/>
,	,	kIx,	,
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
svoje	svůj	k3xOyFgFnPc4	svůj
písně	píseň	k1gFnPc4	píseň
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
YouTube	YouTub	k1gInSc5	YouTub
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
písním	píseň	k1gFnPc3	píseň
si	se	k3xPyFc3	se
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
svoje	svůj	k3xOyFgNnPc4	svůj
vlastní	vlastní	k2eAgNnPc4d1	vlastní
videa	video	k1gNnPc4	video
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
nahrávek	nahrávka	k1gFnPc2	nahrávka
byla	být	k5eAaImAgFnS	být
demo	demo	k2eAgFnSc7d1	demo
verzí	verze	k1gFnSc7	verze
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
Born	Borna	k1gFnPc2	Borna
to	ten	k3xDgNnSc1	ten
Die	Die	k1gMnSc1	Die
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
měsíci	měsíc	k1gInSc6	měsíc
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
další	další	k2eAgNnSc1d1	další
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
<g/>
,	,	kIx,	,
Interscope	Interscop	k1gInSc5	Interscop
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnPc1	lano
původně	původně	k6eAd1	původně
nechtěla	chtít	k5eNaImAgNnP	chtít
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
sólová	sólový	k2eAgFnSc1d1	sólová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyla	být	k5eNaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
sama	sám	k3xTgFnSc1	sám
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
na	na	k7c6	na
tom	ten	k3xDgMnSc6	ten
trvalo	trvat	k5eAaImAgNnS	trvat
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
Games	Gamesa	k1gFnPc2	Gamesa
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
ocenění	ocenění	k1gNnSc2	ocenění
Q	Q	kA	Q
award	award	k6eAd1	award
za	za	k7c4	za
"	"	kIx"	"
<g/>
Next	Next	k2eAgInSc4d1	Next
big	big	k?	big
thing	thing	k1gInSc4	thing
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
vydala	vydat	k5eAaPmAgFnS	vydat
druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
Born	Born	k1gInSc4	Born
to	ten	k3xDgNnSc1	ten
Die	Die	k1gFnPc1	Die
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgMnSc3	který
zároveň	zároveň	k6eAd1	zároveň
natočila	natočit	k5eAaBmAgFnS	natočit
první	první	k4xOgNnSc4	první
profesionální	profesionální	k2eAgNnSc4d1	profesionální
video	video	k1gNnSc4	video
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Woodkidem	Woodkid	k1gMnSc7	Woodkid
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
NEXT	NEXT	kA	NEXT
Model	model	k1gInSc1	model
Management	management	k1gInSc1	management
agency	agenca	k1gMnSc2	agenca
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
host	host	k1gMnSc1	host
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
SNL	SNL	kA	SNL
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
vystoupením	vystoupení	k1gNnSc7	vystoupení
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nervozní	rvozní	k2eNgNnSc1d1	nervozní
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc4	vystoupení
byl	být	k5eAaImAgInS	být
skandál	skandál	k1gInSc1	skandál
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
výkon	výkon	k1gInSc1	výkon
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
mnohé	mnohé	k1gNnSc4	mnohé
kritiky	kritika	k1gFnSc2	kritika
parodií	parodie	k1gFnPc2	parodie
na	na	k7c4	na
originál	originál	k1gInSc4	originál
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
má	mít	k5eAaImIp3nS	mít
Lana	lano	k1gNnPc1	lano
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
americkým	americký	k2eAgNnPc3d1	americké
mediím	medium	k1gNnPc3	medium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
francouzské	francouzský	k2eAgNnSc4d1	francouzské
show	show	k1gNnSc4	show
Taratata	Taratat	k1gMnSc2	Taratat
během	během	k7c2	během
rozhovoru	rozhovor	k1gInSc2	rozhovor
prozradila	prozradit	k5eAaPmAgFnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
první	první	k4xOgFnSc1	první
deska	deska	k1gFnSc1	deska
ponese	ponést	k5eAaPmIp3nS	ponést
týž	týž	k3xTgInSc4	týž
název	název	k1gInSc4	název
jako	jako	k8xC	jako
druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
a	a	k8xC	a
vyjde	vyjít	k5eAaPmIp3nS	vyjít
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Born	Born	k1gMnSc1	Born
to	ten	k3xDgNnSc4	ten
Die	Die	k1gMnSc3	Die
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
doposud	doposud	k6eAd1	doposud
přes	přes	k7c4	přes
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
pátým	pátý	k4xOgNnSc7	pátý
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
singl	singl	k1gInSc4	singl
vydala	vydat	k5eAaPmAgFnS	vydat
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
Blue	Blu	k1gInSc2	Blu
Jeans	Jeansa	k1gFnPc2	Jeansa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ukážu	ukázat	k5eAaPmIp1nS	ukázat
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
krásnějšího	krásný	k2eAgNnSc2d2	krásnější
než	než	k8xS	než
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
vztahovalo	vztahovat	k5eAaImAgNnS	vztahovat
ke	k	k7c3	k
klipu	klip	k1gInSc3	klip
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
singly	singl	k1gInPc1	singl
Summertime	Summertim	k1gInSc5	Summertim
Sadness	Sadness	k1gInSc4	Sadness
a	a	k8xC	a
National	National	k1gMnSc2	National
Anthem	anthem	k1gInSc1	anthem
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgInP	vydat
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Lana	lano	k1gNnSc2	lano
album	album	k1gNnSc1	album
propagovala	propagovat	k5eAaImAgFnS	propagovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
festivalech	festival	k1gInPc6	festival
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Born	Borna	k1gFnPc2	Borna
to	ten	k3xDgNnSc4	ten
Die	Die	k1gFnSc7	Die
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
Lana	lano	k1gNnSc2	lano
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vydá	vydat	k5eAaPmIp3nS	vydat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
EP	EP	kA	EP
s	s	k7c7	s
názvem	název	k1gInSc7	název
Paradise	Paradise	k1gFnSc2	Paradise
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fyzické	fyzický	k2eAgFnSc6d1	fyzická
verzi	verze	k1gFnSc6	verze
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
jako	jako	k8xC	jako
re-edice	redice	k1gFnSc1	re-edice
alba	album	k1gNnSc2	album
Born	Borna	k1gFnPc2	Borna
to	ten	k3xDgNnSc1	ten
Die	Die	k1gFnSc1	Die
s	s	k7c7	s
názvem	název	k1gInSc7	název
Born	Borna	k1gFnPc2	Borna
to	ten	k3xDgNnSc4	ten
Die	Die	k1gMnSc1	Die
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Paradise	Paradise	k1gFnSc2	Paradise
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
půlce	půlka	k1gFnSc6	půlka
léta	léto	k1gNnSc2	léto
oděvní	oděvní	k2eAgFnSc1d1	oděvní
značka	značka	k1gFnSc1	značka
H	H	kA	H
<g/>
&	&	k?	&
<g/>
M	M	kA	M
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
tváří	tvář	k1gFnSc7	tvář
kolekce	kolekce	k1gFnSc2	kolekce
podzim	podzim	k1gInSc1	podzim
<g/>
/	/	kIx~	/
<g/>
zima	zima	k1gFnSc1	zima
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
Blue	Blue	k1gFnSc3	Blue
Velvet	Velvet	k1gMnSc1	Velvet
a	a	k8xC	a
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
natočil	natočit	k5eAaBmAgMnS	natočit
videoklip	videoklip	k1gInSc4	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
coververze	coververze	k1gFnSc1	coververze
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
na	na	k7c4	na
Paradise	Paradise	k1gFnPc4	Paradise
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k8xC	jako
propagační	propagační	k2eAgInSc1d1	propagační
singl	singl	k1gInSc1	singl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
oficiální	oficiální	k2eAgInSc1d1	oficiální
singl	singl	k1gInSc1	singl
Paradise	Paradise	k1gFnSc2	Paradise
byl	být	k5eAaImAgInS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
představen	představit	k5eAaPmNgInS	představit
jako	jako	k9	jako
desetiminutový	desetiminutový	k2eAgInSc1d1	desetiminutový
videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
Ride	Ride	k1gFnSc3	Ride
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ona	onen	k3xDgFnSc1	onen
reedice	reedice	k1gFnSc1	reedice
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
reklamní	reklamní	k2eAgInSc1d1	reklamní
klip	klip	k1gInSc1	klip
pro	pro	k7c4	pro
Jaguara	Jaguar	k1gMnSc4	Jaguar
F-TYPE	F-TYPE	k1gFnSc4	F-TYPE
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Burning	Burning	k1gInSc4	Burning
Desire	Desir	k1gInSc5	Desir
<g/>
.	.	kIx.	.
</s>
<s>
Potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
hudbě	hudba	k1gFnSc6	hudba
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
snímek	snímek	k1gInSc4	snímek
Velký	velký	k2eAgInSc4d1	velký
Gatsby	Gatsb	k1gInPc4	Gatsb
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yRgInSc4	který
natočila	natočit	k5eAaBmAgFnS	natočit
píseň	píseň	k1gFnSc1	píseň
Young	Younga	k1gFnPc2	Younga
and	and	k?	and
Beautiful	Beautifula	k1gFnPc2	Beautifula
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
videoklip	videoklip	k1gInSc4	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
dubnu	duben	k1gInSc3	duben
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
coververze	coververze	k1gFnSc1	coververze
k	k	k7c3	k
písním	píseň	k1gFnPc3	píseň
Chelsea	Chelse	k1gInSc2	Chelse
Hotel	hotel	k1gInSc1	hotel
#	#	kIx~	#
<g/>
2	[number]	k4	2
Leonarda	Leonardo	k1gMnSc2	Leonardo
Cohena	Cohen	k1gMnSc2	Cohen
a	a	k8xC	a
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
přezpívané	přezpívaný	k2eAgInPc4d1	přezpívaný
Summer	Summer	k1gInSc4	Summer
Wine	Win	k1gFnSc2	Win
od	od	k7c2	od
Lee	Lea	k1gFnSc6	Lea
Hazlewood	Hazlewood	k1gInSc1	Hazlewood
a	a	k8xC	a
Nancy	Nancy	k1gFnSc1	Nancy
Sinatry	Sinatrum	k1gNnPc7	Sinatrum
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vyjela	vyjet	k5eAaPmAgFnS	vyjet
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
Paradise	Paradise	k1gFnSc2	Paradise
Tour	Tour	k1gInSc4	Tour
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
svého	svůj	k3xOyFgNnSc2	svůj
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
EP	EP	kA	EP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2013	[number]	k4	2013
znovu	znovu	k6eAd1	znovu
vydala	vydat	k5eAaPmAgFnS	vydat
Summertime	Summertim	k1gInSc5	Summertim
Sadness	Sadness	k1gInSc4	Sadness
-	-	kIx~	-
zremixovanou	zremixovaný	k2eAgFnSc4d1	zremixovaná
verzi	verze	k1gFnSc4	verze
o	o	k7c4	o
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
Cedric	Cedric	k1gMnSc1	Cedric
Gervais	Gervais	k1gFnSc2	Gervais
<g/>
.	.	kIx.	.
</s>
<s>
Remix	Remix	k1gInSc1	Remix
dostal	dostat	k5eAaPmAgInS	dostat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c6	na
hudebních	hudební	k2eAgFnPc6d1	hudební
cenách	cena	k1gFnPc6	cena
Grammy	Gramma	k1gFnSc2	Gramma
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
Lana	lano	k1gNnSc2	lano
dostala	dostat	k5eAaPmAgFnS	dostat
nominace	nominace	k1gFnSc1	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
popové	popový	k2eAgNnSc4d1	popové
album	album	k1gNnSc4	album
s	s	k7c7	s
Paradise	Paradis	k1gInSc6	Paradis
a	a	k8xC	a
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
napsanou	napsaný	k2eAgFnSc4d1	napsaná
pro	pro	k7c4	pro
vizuální	vizuální	k2eAgNnPc4d1	vizuální
média	médium	k1gNnPc4	médium
s	s	k7c7	s
Young	Young	k1gMnSc1	Young
and	and	k?	and
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
premiéra	premiéra	k1gFnSc1	premiéra
půlhodinového	půlhodinový	k2eAgInSc2d1	půlhodinový
filmu	film	k1gInSc2	film
Tropico	Tropico	k1gMnSc1	Tropico
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnující	zahrnující	k2eAgFnPc4d1	zahrnující
tři	tři	k4xCgFnPc4	tři
skladby	skladba	k1gFnPc4	skladba
Body	bod	k1gInPc4	bod
Electric	Electrice	k1gInPc2	Electrice
<g/>
,	,	kIx,	,
Gods	Godsa	k1gFnPc2	Godsa
And	Anda	k1gFnPc2	Anda
Monsters	Monsters	k1gInSc1	Monsters
a	a	k8xC	a
Bel	bel	k1gInSc1	bel
Air	Air	k1gFnSc2	Air
z	z	k7c2	z
re-edice	redice	k1gFnSc2	re-edice
Paradise	Paradise	k1gFnSc2	Paradise
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
EP	EP	kA	EP
Tropico	Tropico	k1gNnSc1	Tropico
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
tyto	tento	k3xDgFnPc4	tento
tři	tři	k4xCgFnPc4	tři
písně	píseň	k1gFnPc4	píseň
i	i	k8xC	i
třiceti	třicet	k4xCc7	třicet
minutový	minutový	k2eAgInSc1d1	minutový
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
Evě	Eva	k1gFnSc6	Eva
a	a	k8xC	a
Adamovi	Adamův	k2eAgMnPc1d1	Adamův
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
cestě	cesta	k1gFnSc3	cesta
k	k	k7c3	k
znovu	znovu	k6eAd1	znovu
nalezení	nalezení	k1gNnSc3	nalezení
nevinnosti	nevinnost	k1gFnSc2	nevinnost
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
rozloučení	rozloučení	k1gNnSc4	rozloučení
s	s	k7c7	s
érou	éra	k1gFnSc7	éra
Born	Borna	k1gFnPc2	Borna
to	ten	k3xDgNnSc1	ten
Die	Die	k1gMnSc1	Die
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
Lana	lano	k1gNnSc2	lano
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
jméno	jméno	k1gNnSc4	jméno
své	své	k1gNnSc4	své
třetí	třetí	k4xOgFnSc2	třetí
desky	deska	k1gFnSc2	deska
Ultraviolence	Ultraviolence	k1gFnSc2	Ultraviolence
které	který	k3yQgNnSc1	který
později	pozdě	k6eAd2	pozdě
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
novém	nový	k2eAgInSc6d1	nový
album	album	k1gNnSc4	album
už	už	k9	už
dříve	dříve	k6eAd2	dříve
řekla	říct	k5eAaPmAgFnS	říct
"	"	kIx"	"
<g/>
Pracuji	pracovat	k5eAaImIp1nS	pracovat
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
opravdu	opravdu	k6eAd1	opravdu
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
miluji	milovat	k5eAaImIp1nS	milovat
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dělám	dělat	k5eAaImIp1nS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Psala	psát	k5eAaImAgFnS	psát
jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
Santa	Sant	k1gMnSc2	Sant
Monice	Monika	k1gFnSc6	Monika
a	a	k8xC	a
už	už	k6eAd1	už
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
deska	deska	k1gFnSc1	deska
bude	být	k5eAaImBp3nS	být
znít	znít	k5eAaImF	znít
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
musím	muset	k5eAaImIp1nS	muset
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
že	že	k8xS	že
Lana	lano	k1gNnPc4	lano
přezpívá	přezpívat	k5eAaPmIp3nS	přezpívat
píseň	píseň	k1gFnSc4	píseň
Once	Onc	k1gFnSc2	Onc
Upon	Upon	k1gInSc1	Upon
a	a	k8xC	a
Dream	Dream	k1gInSc1	Dream
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Šípkové	Šípkové	k2eAgFnSc2d1	Šípkové
Růženky	Růženka	k1gFnSc2	Růženka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
film	film	k1gInSc4	film
Maleficent	Maleficent	k1gMnSc1	Maleficent
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
Disney	Disnea	k1gFnSc2	Disnea
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
vyšla	vyjít	k5eAaPmAgFnS	vyjít
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
svou	svůj	k3xOyFgFnSc4	svůj
fotografii	fotografia	k1gFnSc4	fotografia
s	s	k7c7	s
Danem	Dan	k1gMnSc7	Dan
Auerbachem	Auerbach	k1gMnSc7	Auerbach
<g/>
,	,	kIx,	,
zpěvákem	zpěvák	k1gMnSc7	zpěvák
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
The	The	k1gMnPc2	The
Black	Black	k1gInSc1	Black
Keys	Keysa	k1gFnPc2	Keysa
<g/>
,	,	kIx,	,
s	s	k7c7	s
popisem	popis	k1gInSc7	popis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
a	a	k8xC	a
Dan	Dan	k1gMnSc1	Dan
Auerbach	Auerbach	k1gMnSc1	Auerbach
jsme	být	k5eAaImIp1nP	být
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vám	vy	k3xPp2nPc3	vy
předvedeme	předvést	k5eAaPmIp1nP	předvést
Ultraviolence	Ultraviolence	k1gFnPc4	Ultraviolence
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Auerbach	Auerbach	k1gMnSc1	Auerbach
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
producentem	producent	k1gMnSc7	producent
alba	album	k1gNnSc2	album
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lanou	laný	k2eAgFnSc4d1	laná
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
studiu	studio	k1gNnSc6	studio
v	v	k7c6	v
Nashvillu	Nashvill	k1gInSc6	Nashvill
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
Lana	lano	k1gNnSc2	lano
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
z	z	k7c2	z
následující	následující	k2eAgFnSc2d1	následující
studiové	studiový	k2eAgFnSc2d1	studiová
Ultraviolence	Ultraviolence	k1gFnSc2	Ultraviolence
ponese	ponést	k5eAaPmIp3nS	ponést
název	název	k1gInSc4	název
West	West	k2eAgInSc4d1	West
Coast	Coast	k1gInSc4	Coast
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
v	v	k7c6	v
rádiu	rádius	k1gInSc6	rádius
BBC	BBC	kA	BBC
a	a	k8xC	a
videoklip	videoklip	k1gInSc1	videoklip
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
vyjela	vyjet	k5eAaPmAgFnS	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
před	před	k7c4	před
svatební	svatební	k2eAgNnSc4d1	svatební
oslavě	oslava	k1gFnSc3	oslava
Kanyeho	Kanye	k1gMnSc2	Kanye
Westa	West	k1gMnSc2	West
a	a	k8xC	a
Kim	Kim	k1gMnSc2	Kim
Kardashian	Kardashiana	k1gFnPc2	Kardashiana
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
před	před	k7c7	před
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
průběžně	průběžně	k6eAd1	průběžně
tři	tři	k4xCgInPc1	tři
propagační	propagační	k2eAgInPc1d1	propagační
singly	singl	k1gInPc1	singl
a	a	k8xC	a
těmi	ten	k3xDgMnPc7	ten
byly	být	k5eAaImAgFnP	být
Shades	Shades	k1gMnSc1	Shades
of	of	k?	of
Cool	Cool	k1gMnSc1	Cool
<g/>
,	,	kIx,	,
Ultraviolence	Ultraviolence	k1gFnSc1	Ultraviolence
a	a	k8xC	a
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Baby	baba	k1gFnSc2	baba
<g/>
.	.	kIx.	.
</s>
<s>
Shades	Shades	k1gInSc1	Shades
of	of	k?	of
Cool	Cool	k1gInSc1	Cool
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
dočkal	dočkat	k5eAaPmAgInS	dočkat
i	i	k9	i
svého	svůj	k3xOyFgInSc2	svůj
videoklipu	videoklip	k1gInSc2	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tu	tu	k6eAd1	tu
samou	samý	k3xTgFnSc4	samý
dobu	doba	k1gFnSc4	doba
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
evropských	evropský	k2eAgInPc6d1	evropský
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Ultraviolence	Ultraviolence	k1gFnSc2	Ultraviolence
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
sprvna	sprvno	k1gNnSc2	sprvno
2014	[number]	k4	2014
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xC	jako
druhý	druhý	k4xOgInSc1	druhý
oficiální	oficiální	k2eAgInSc1d1	oficiální
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
oznámila	oznámit	k5eAaPmAgFnS	oznámit
turné	turné	k1gNnSc4	turné
na	na	k7c4	na
příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
zvané	zvaný	k2eAgFnSc2d1	zvaná
The	The	k1gFnSc2	The
Endless	Endlessa	k1gFnPc2	Endlessa
Summer	Summer	k1gMnSc1	Summer
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Německo	Německo	k1gNnSc4	Německo
vyšel	vyjít	k5eAaPmAgInS	vyjít
poslední	poslední	k2eAgInSc1d1	poslední
singl	singl	k1gInSc1	singl
z	z	k7c2	z
Ultraviolence	Ultraviolence	k1gFnSc2	Ultraviolence
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bonusová	bonusový	k2eAgFnSc1d1	bonusová
píseň	píseň	k1gFnSc1	píseň
Black	Blacka	k1gFnPc2	Blacka
Beauty	Beaut	k1gMnPc4	Beaut
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
světlo	světlo	k1gNnSc1	světlo
světa	svět	k1gInSc2	svět
spatřili	spatřit	k5eAaPmAgMnP	spatřit
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
písně	píseň	k1gFnPc4	píseň
Big	Big	k1gMnPc2	Big
Eyes	Eyesa	k1gFnPc2	Eyesa
a	a	k8xC	a
I	i	k9	i
Can	Can	k1gFnSc1	Can
Fly	Fly	k1gFnSc1	Fly
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Tima	Timus	k1gMnSc4	Timus
Burtona	Burton	k1gMnSc4	Burton
<g/>
,	,	kIx,	,
Big	Big	k1gMnPc4	Big
Eyes	Eyesa	k1gFnPc2	Eyesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
vydal	vydat	k5eAaPmAgMnS	vydat
její	její	k3xOp3gMnSc1	její
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
kamarád	kamarád	k1gMnSc1	kamarád
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
jejího	její	k3xOp3gNnSc2	její
alba	album	k1gNnSc2	album
Born	Borna	k1gFnPc2	Borna
to	ten	k3xDgNnSc1	ten
Die	Die	k1gMnPc1	Die
<g/>
,	,	kIx,	,
Emile	Emil	k1gMnSc5	Emil
Haynie	Haynie	k1gFnSc2	Haynie
píseň	píseň	k1gFnSc1	píseň
Wait	Wait	k1gInSc1	Wait
For	forum	k1gNnPc2	forum
Life	Lif	k1gFnSc2	Lif
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Lana	lano	k1gNnPc1	lano
nazpívala	nazpívat	k5eAaBmAgNnP	nazpívat
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
We	We	k1gFnSc2	We
Fall	Falla	k1gFnPc2	Falla
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
obdržela	obdržet	k5eAaPmAgFnS	obdržet
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c6	na
BRIT	Brit	k1gMnSc1	Brit
Awards	Awards	k1gInSc1	Awards
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
ženská	ženský	k2eAgFnSc1d1	ženská
umělkyně	umělkyně	k1gFnSc1	umělkyně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Grazia	Grazius	k1gMnSc4	Grazius
Magazine	Magazin	k1gInSc5	Magazin
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěla	chtít	k5eAaImAgFnS	chtít
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
orchestr	orchestr	k1gInSc4	orchestr
a	a	k8xC	a
monumentální	monumentální	k2eAgInPc4d1	monumentální
refrény	refrén	k1gInPc4	refrén
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
nahrávání	nahrávání	k1gNnSc2	nahrávání
alba	album	k1gNnSc2	album
a	a	k8xC	a
chybí	chybět	k5eAaImIp3nS	chybět
ji	on	k3xPp3gFnSc4	on
už	už	k9	už
jen	jen	k9	jen
pár	pár	k4xCyI	pár
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
kompletní	kompletní	k2eAgMnSc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
podle	podle	k7c2	podle
jejích	její	k3xOp3gNnPc2	její
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
zní	znět	k5eAaImIp3nS	znět
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
název	název	k1gInSc4	název
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
Honeymoon	Honeymoon	k1gNnSc1	Honeymoon
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bude	být	k5eAaImBp3nS	být
obsahovat	obsahovat	k5eAaImF	obsahovat
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
Music	Musice	k1gFnPc2	Musice
To	to	k9	to
Watch	Watch	k1gInSc1	Watch
Boys	boy	k1gMnPc2	boy
To	to	k9	to
a	a	k8xC	a
cover	cover	k1gInSc4	cover
verzi	verze	k1gFnSc4	verze
od	od	k7c2	od
Niny	Nina	k1gFnSc2	Nina
Simone	Simon	k1gMnSc5	Simon
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Let	léto	k1gNnPc2	léto
Me	Me	k1gMnPc2	Me
Be	Be	k1gFnSc2	Be
Misunderstood	Misunderstood	k1gInSc1	Misunderstood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
upoutávka	upoutávka	k1gFnSc1	upoutávka
k	k	k7c3	k
filmu	film	k1gInSc3	film
The	The	k1gFnPc2	The
Age	Age	k1gFnSc2	Age
of	of	k?	of
Adaline	Adalin	k1gInSc5	Adalin
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
zazněla	zaznít	k5eAaPmAgFnS	zaznít
ukázka	ukázka	k1gFnSc1	ukázka
nevydané	vydaný	k2eNgFnSc2d1	nevydaná
písně	píseň	k1gFnSc2	píseň
Life	Lif	k1gFnSc2	Lif
is	is	k?	is
Beautiful	Beautiful	k1gInSc1	Beautiful
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
vyjela	vyjet	k5eAaPmAgFnS	vyjet
Del	Del	k1gFnSc1	Del
Rey	Rea	k1gFnSc2	Rea
na	na	k7c6	na
The	The	k1gFnSc6	The
Endless	Endlessa	k1gFnPc2	Endlessa
Summer	Summer	k1gMnSc1	Summer
Tour	Tour	k1gMnSc1	Tour
po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
vystoupení	vystoupení	k1gNnSc6	vystoupení
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
album	album	k1gNnSc1	album
Honeymoon	Honeymoona	k1gFnPc2	Honeymoona
vyjde	vyjít	k5eAaPmIp3nS	vyjít
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
na	na	k7c4	na
internet	internet	k1gInSc4	internet
postupně	postupně	k6eAd1	postupně
dvě	dva	k4xCgFnPc1	dva
ukázky	ukázka	k1gFnPc1	ukázka
videoklipu	videoklip	k1gInSc2	videoklip
ke	k	k7c3	k
skladbě	skladba	k1gFnSc3	skladba
Honeymoon	Honeymoona	k1gFnPc2	Honeymoona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
nahrána	nahrát	k5eAaBmNgFnS	nahrát
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
oficiální	oficiální	k2eAgFnSc4d1	oficiální
Youtube	Youtub	k1gInSc5	Youtub
účet	účet	k1gInSc4	účet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
prozradila	prozradit	k5eAaPmAgFnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
Honeymoon	Honeymoon	k1gInSc1	Honeymoon
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
High	High	k1gMnSc1	High
by	by	k9	by
the	the	k?	the
Beach	Beach	k1gInSc1	Beach
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
vyšel	vyjít	k5eAaPmAgInS	vyjít
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
a	a	k8xC	a
videoklip	videoklip	k1gInSc1	videoklip
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Del	Del	k?	Del
Rey	Rea	k1gFnSc2	Rea
prozradila	prozradit	k5eAaPmAgFnS	prozradit
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
vyjde	vyjít	k5eAaPmIp3nS	vyjít
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
Honeymoon	Honeymoona	k1gFnPc2	Honeymoona
k	k	k7c3	k
předprodeji	předprodej	k1gInSc3	předprodej
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
zároveň	zároveň	k6eAd1	zároveň
vyšel	vyjít	k5eAaPmAgInS	vyjít
první	první	k4xOgInSc1	první
propagační	propagační	k2eAgInSc1d1	propagační
singl	singl	k1gInSc1	singl
Terrence	Terrence	k1gFnSc2	Terrence
Loves	Loves	k1gMnSc1	Loves
You	You	k1gMnSc1	You
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Beauty	Beaut	k1gMnPc4	Beaut
Behind	Behinda	k1gFnPc2	Behinda
the	the	k?	the
Madness	Madnessa	k1gFnPc2	Madnessa
od	od	k7c2	od
The	The	k1gFnSc2	The
Weeknd	Weeknd	k1gInSc1	Weeknd
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Del	Del	k1gFnSc1	Del
Rey	Rea	k1gFnSc2	Rea
přispěla	přispět	k5eAaPmAgFnS	přispět
svými	svůj	k3xOyFgInPc7	svůj
vokály	vokál	k1gInPc7	vokál
na	na	k7c6	na
písni	píseň	k1gFnSc6	píseň
Prisoner	Prisonra	k1gFnPc2	Prisonra
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
vyšel	vyjít	k5eAaPmAgInS	vyjít
druhý	druhý	k4xOgInSc4	druhý
propagační	propagační	k2eAgInSc4d1	propagační
singl	singl	k1gInSc4	singl
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
titulní	titulní	k2eAgFnSc1d1	titulní
píseň	píseň	k1gFnSc1	píseň
Honeymoon	Honeymoona	k1gFnPc2	Honeymoona
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
oficiální	oficiální	k2eAgInSc1d1	oficiální
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
Music	Musice	k1gFnPc2	Musice
To	to	k9	to
Watch	Watch	k1gInSc1	Watch
Boys	boy	k1gMnPc2	boy
To	to	k9	to
vyšel	vyjít	k5eAaPmAgInS	vyjít
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
a	a	k8xC	a
videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc2	turné
k	k	k7c3	k
albu	album	k1gNnSc3	album
Born	Borna	k1gFnPc2	Borna
to	ten	k3xDgNnSc4	ten
Die	Die	k1gFnPc1	Die
probíhaly	probíhat	k5eAaImAgFnP	probíhat
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2011	[number]	k4	2011
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
Lana	lano	k1gNnPc4	lano
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
Paradise	Paradise	k1gFnSc2	Paradise
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc4	Asie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
i	i	k9	i
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
Electronic	Electronice	k1gFnPc2	Electronice
Beats	Beats	k1gInSc1	Beats
festivalu	festival	k1gInSc2	festival
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Konal	konat	k5eAaImAgInS	konat
se	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
ve	v	k7c6	v
vyprodaném	vyprodaný	k2eAgNnSc6d1	vyprodané
divadle	divadlo	k1gNnSc6	divadlo
Archa	archa	k1gFnSc1	archa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
se	se	k3xPyFc4	se
nechala	nechat	k5eAaPmAgFnS	nechat
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
nečekala	čekat	k5eNaImAgFnS	čekat
takovouto	takovýto	k3xDgFnSc4	takovýto
reakci	reakce	k1gFnSc4	reakce
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
opakovala	opakovat	k5eAaImAgFnS	opakovat
"	"	kIx"	"
<g/>
Jste	být	k5eAaImIp2nP	být
šílení	šílený	k2eAgMnPc1d1	šílený
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dav	dav	k1gInSc4	dav
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slíbila	slíbit	k5eAaPmAgFnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přijede	přijet	k5eAaPmIp3nS	přijet
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
oznámila	oznámit	k5eAaPmAgFnS	oznámit
pokračování	pokračování	k1gNnSc4	pokračování
turné	turné	k1gNnSc2	turné
pro	pro	k7c4	pro
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnPc1	lano
vyprodala	vyprodat	k5eAaPmAgFnS	vyprodat
všechny	všechen	k3xTgFnPc4	všechen
show	show	k1gFnPc4	show
během	během	k7c2	během
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přidala	přidat	k5eAaPmAgFnS	přidat
pár	pár	k4xCyI	pár
vystoupení	vystoupení	k1gNnPc2	vystoupení
na	na	k7c6	na
evropských	evropský	k2eAgInPc6d1	evropský
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
oznámila	oznámit	k5eAaPmAgFnS	oznámit
The	The	k1gFnSc1	The
Endless	Endlessa	k1gFnPc2	Endlessa
Summer	Summer	k1gMnSc1	Summer
Tour	Tour	k1gMnSc1	Tour
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
svého	svůj	k3xOyFgNnSc2	svůj
alba	album	k1gNnSc2	album
Ultraviolence	Ultraviolence	k1gFnSc2	Ultraviolence
<g/>
.	.	kIx.	.
</s>
<s>
Born	Born	k1gMnSc1	Born
to	ten	k3xDgNnSc4	ten
Die	Die	k1gFnSc7	Die
Tour	Tour	k1gMnSc1	Tour
Paradise	Paradise	k1gFnSc2	Paradise
Tour	Tour	k1gMnSc1	Tour
The	The	k1gMnSc1	The
Endless	Endlessa	k1gFnPc2	Endlessa
Summer	Summer	k1gMnSc1	Summer
Tour	Tour	k1gMnSc1	Tour
Festival	festival	k1gInSc4	festival
Tour	Tour	k1gInSc1	Tour
2016	[number]	k4	2016
</s>
