<s>
Petro	Petra	k1gFnSc5	Petra
Olexijovyč	Olexijovyč	k1gInSc1	Olexijovyč
Porošenko	Porošenka	k1gFnSc5	Porošenka
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
:	:	kIx,	:
П	П	k?	П
О	О	k?	О
П	П	k?	П
<g/>
;	;	kIx,	;
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
Bolhrad	Bolhrada	k1gFnPc2	Bolhrada
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
pátý	pátý	k4xOgMnSc1	pátý
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Porošenko	Porošenka	k1gFnSc5	Porošenka
je	on	k3xPp3gInPc4	on
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
-	-	kIx~	-
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
národní	národní	k2eAgFnSc2d1	národní
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
parlamentu	parlament	k1gInSc2	parlament
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolit	k5eAaPmNgInS	zvolit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
oznámil	oznámit	k5eAaPmAgMnS	oznámit
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
prezident	prezident	k1gMnSc1	prezident
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2019	[number]	k4	2019
===	===	k?	===
</s>
</p>
<p>
<s>
Porošenko	Porošenka	k1gFnSc5	Porošenka
byl	být	k5eAaImAgInS	být
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
své	svůj	k3xOyFgFnSc2	svůj
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
Kanál	kanál	k1gInSc1	kanál
5	[number]	k4	5
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
organizátorů	organizátor	k1gMnPc2	organizátor
Euromajdanu	Euromajdan	k1gInSc2	Euromajdan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
proruského	proruský	k2eAgMnSc2d1	proruský
prezidenta	prezident	k1gMnSc2	prezident
Viktora	Viktor	k1gMnSc2	Viktor
Janukovyče	Janukovyč	k1gFnSc2	Janukovyč
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
pak	pak	k6eAd1	pak
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgInS	získat
přes	přes	k7c4	přes
50	[number]	k4	50
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
složil	složit	k5eAaPmAgMnS	složit
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
pátým	pátý	k4xOgMnSc7	pátý
prezidentem	prezident	k1gMnSc7	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
