<p>
<s>
Petro	Petra	k1gFnSc5	Petra
Olexijovyč	Olexijovyč	k1gInSc1	Olexijovyč
Porošenko	Porošenka	k1gFnSc5	Porošenka
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
<g/>
:	:	kIx,	:
П	П	k?	П
О	О	k?	О
П	П	k?	П
<g/>
;	;	kIx,	;
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1965	[number]	k4	1965
Bolhrad	Bolhrada	k1gFnPc2	Bolhrada
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
pátý	pátý	k4xOgMnSc1	pátý
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Porošenko	Porošenka	k1gFnSc5	Porošenka
je	on	k3xPp3gInPc4	on
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
-	-	kIx~	-
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
národní	národní	k2eAgFnSc2d1	národní
bankovní	bankovní	k2eAgFnSc2d1	bankovní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
parlamentu	parlament	k1gInSc2	parlament
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolit	k5eAaPmNgInS	zvolit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
oznámil	oznámit	k5eAaPmAgMnS	oznámit
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
prezident	prezident	k1gMnSc1	prezident
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2019	[number]	k4	2019
===	===	k?	===
</s>
</p>
<p>
<s>
Porošenko	Porošenka	k1gFnSc5	Porošenka
byl	být	k5eAaImAgInS	být
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
své	svůj	k3xOyFgFnSc2	svůj
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
Kanál	kanál	k1gInSc1	kanál
5	[number]	k4	5
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
organizátorů	organizátor	k1gMnPc2	organizátor
Euromajdanu	Euromajdan	k1gInSc2	Euromajdan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
ke	k	k7c3	k
svržení	svržení	k1gNnSc3	svržení
proruského	proruský	k2eAgMnSc2d1	proruský
prezidenta	prezident	k1gMnSc2	prezident
Viktora	Viktor	k1gMnSc2	Viktor
Janukovyče	Janukovyč	k1gFnSc2	Janukovyč
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
pak	pak	k6eAd1	pak
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgInS	získat
přes	přes	k7c4	přes
50	[number]	k4	50
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
složil	složit	k5eAaPmAgMnS	složit
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
pátým	pátý	k4xOgMnSc7	pátý
prezidentem	prezident	k1gMnSc7	prezident
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
do	do	k7c2	do
konference	konference	k1gFnSc2	konference
čtyř	čtyři	k4xCgMnPc2	čtyři
státníků	státník	k1gMnPc2	státník
v	v	k7c6	v
Minsku	Minsk	k1gInSc6	Minsk
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
a	a	k8xC	a
schválení	schválení	k1gNnSc4	schválení
druhé	druhý	k4xOgFnSc2	druhý
minské	minský	k2eAgFnSc2d1	Minská
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
klidu	klid	k1gInSc6	klid
zbraní	zbraň	k1gFnPc2	zbraň
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
prezident	prezident	k1gMnSc1	prezident
Porošenko	Porošenka	k1gFnSc5	Porošenka
pokračování	pokračování	k1gNnPc4	pokračování
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
armády	armáda	k1gFnSc2	armáda
proti	proti	k7c3	proti
proruským	proruský	k2eAgMnPc3d1	proruský
separatistům	separatista	k1gMnPc3	separatista
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
začala	začít	k5eAaPmAgFnS	začít
vláda	vláda	k1gFnSc1	vláda
premiéra	premiér	k1gMnSc2	premiér
Arsenije	Arsenije	k1gMnSc2	Arsenije
Jaceňuka	Jaceňuk	k1gMnSc2	Jaceňuk
<g/>
.	.	kIx.	.
</s>
<s>
Porošenko	Porošenka	k1gFnSc5	Porošenka
označuje	označovat	k5eAaImIp3nS	označovat
východoukrajinské	východoukrajinský	k2eAgMnPc4d1	východoukrajinský
separatisty	separatista	k1gMnPc4	separatista
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Doněcké	doněcký	k2eAgFnSc2d1	Doněcká
a	a	k8xC	a
Luhanské	Luhanský	k2eAgFnSc2d1	Luhanská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
bandity	bandita	k1gMnPc7	bandita
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
ruské	ruský	k2eAgMnPc4d1	ruský
teroristy	terorista	k1gMnPc4	terorista
<g/>
"	"	kIx"	"
a	a	k8xC	a
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
krize	krize	k1gFnSc2	krize
odmítá	odmítat	k5eAaImIp3nS	odmítat
vést	vést	k5eAaImF	vést
se	s	k7c7	s
separatisty	separatista	k1gMnPc7	separatista
politický	politický	k2eAgInSc4d1	politický
dialog	dialog	k1gInSc4	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Porošenka	Porošenka	k1gFnSc1	Porošenka
nejsou	být	k5eNaImIp3nP	být
zástupci	zástupce	k1gMnPc1	zástupce
separatistických	separatistický	k2eAgFnPc2d1	separatistická
republik	republika	k1gFnPc2	republika
"	"	kIx"	"
<g/>
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
dialogu	dialog	k1gInSc2	dialog
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ozbrojenci	ozbrojenec	k1gMnPc1	ozbrojenec
a	a	k8xC	a
teroristé	terorista	k1gMnPc1	terorista
nemohou	moct	k5eNaImIp3nP	moct
zastupovat	zastupovat	k5eAaImF	zastupovat
Donbas	Donbas	k1gInSc4	Donbas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2014	[number]	k4	2014
Porošenko	Porošenka	k1gFnSc5	Porošenka
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
každého	každý	k3xTgMnSc4	každý
ukrajinského	ukrajinský	k2eAgMnSc4d1	ukrajinský
vojáka	voják	k1gMnSc4	voják
padlého	padlý	k1gMnSc4	padlý
v	v	k7c6	v
boji	boj	k1gInSc6	boj
"	"	kIx"	"
<g/>
budou	být	k5eAaImBp3nP	být
zlikvidovány	zlikvidovat	k5eAaPmNgFnP	zlikvidovat
stovky	stovka	k1gFnPc1	stovka
teroristů	terorista	k1gMnPc2	terorista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2018	[number]	k4	2018
došlo	dojít	k5eAaPmAgNnS	dojít
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Porošenka	Porošenka	k1gFnSc1	Porošenka
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
tří	tři	k4xCgFnPc2	tři
pravoslavných	pravoslavný	k2eAgFnPc2d1	pravoslavná
církví	církev	k1gFnPc2	církev
působících	působící	k2eAgFnPc2d1	působící
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
v	v	k7c4	v
jedinou	jediný	k2eAgFnSc4d1	jediná
Pravoslavnou	pravoslavný	k2eAgFnSc4d1	pravoslavná
církev	církev	k1gFnSc4	církev
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
Moskevský	moskevský	k2eAgInSc4d1	moskevský
patriarchát	patriarchát	k1gInSc4	patriarchát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
disponuje	disponovat	k5eAaBmIp3nS	disponovat
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
provedli	provést	k5eAaPmAgMnP	provést
agenti	agent	k1gMnPc1	agent
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
SBU	SBU	kA	SBU
v	v	k7c6	v
klášterech	klášter	k1gInPc6	klášter
a	a	k8xC	a
budovách	budova	k1gFnPc6	budova
patřících	patřící	k2eAgFnPc6d1	patřící
Moskevskému	moskevský	k2eAgInSc3d1	moskevský
patriarchátu	patriarchát	k1gInSc3	patriarchát
razie	razie	k1gFnSc2	razie
a	a	k8xC	a
domovní	domovní	k2eAgFnSc2d1	domovní
prohlídky	prohlídka	k1gFnSc2	prohlídka
<g/>
.	.	kIx.	.
<g/>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Porošenko	Porošenka	k1gFnSc5	Porošenka
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
někdejší	někdejší	k2eAgMnSc1d1	někdejší
příslušníci	příslušník	k1gMnPc1	příslušník
UPA	UPA	kA	UPA
a	a	k8xC	a
OUN	OUN	kA	OUN
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
banderovci	banderovec	k1gMnPc1	banderovec
<g/>
,	,	kIx,	,
získají	získat	k5eAaPmIp3nP	získat
postavení	postavení	k1gNnSc4	postavení
válečných	válečný	k2eAgMnPc2d1	válečný
veteránů	veterán	k1gMnPc2	veterán
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnPc1d1	související
sociální	sociální	k2eAgFnPc4d1	sociální
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
2019	[number]	k4	2019
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
získal	získat	k5eAaPmAgMnS	získat
Porošenko	Porošenka	k1gFnSc5	Porošenka
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
16	[number]	k4	16
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Umístil	umístit	k5eAaPmAgInS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
za	za	k7c7	za
Volodymyrem	Volodymyr	k1gMnSc7	Volodymyr
Zelenským	Zelenský	k2eAgMnSc7d1	Zelenský
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterého	který	k3yRgMnSc4	který
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
kandidáti	kandidát	k1gMnPc1	kandidát
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
kolo	kolo	k1gNnSc1	kolo
voleb	volba	k1gFnPc2	volba
přineslo	přinést	k5eAaPmAgNnS	přinést
podle	podle	k7c2	podle
očekávání	očekávání	k1gNnSc2	očekávání
vítězství	vítězství	k1gNnSc2	vítězství
Zelenského	Zelenský	k2eAgNnSc2d1	Zelenský
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volební	volební	k2eAgFnSc6d1	volební
účasti	účast	k1gFnSc6	účast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
předběžně	předběžně	k6eAd1	předběžně
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
58	[number]	k4	58
%	%	kIx~	%
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
vítězný	vítězný	k2eAgMnSc1d1	vítězný
kandidát	kandidát	k1gMnSc1	kandidát
podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
provedených	provedený	k2eAgInPc2d1	provedený
u	u	k7c2	u
volebních	volební	k2eAgFnPc2d1	volební
uren	urna	k1gFnPc2	urna
74	[number]	k4	74
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
prezidenta	prezident	k1gMnSc4	prezident
Porošenka	Porošenka	k1gFnSc1	Porošenka
připadlo	připadnout	k5eAaPmAgNnS	připadnout
pouze	pouze	k6eAd1	pouze
26	[number]	k4	26
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Porošenko	Porošenka	k1gFnSc5	Porošenka
svoji	svůj	k3xOyFgFnSc4	svůj
porážku	porážka	k1gFnSc4	porážka
již	již	k6eAd1	již
uznal	uznat	k5eAaPmAgInS	uznat
<g/>
.	.	kIx.	.
</s>
<s>
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Zelenskyj	Zelenskyj	k1gMnSc1	Zelenskyj
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgInS	stát
novým	nový	k2eAgMnSc7d1	nový
ukrajinským	ukrajinský	k2eAgMnSc7d1	ukrajinský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Petro	Petra	k1gFnSc5	Petra
Porošenko	Porošenka	k1gFnSc5	Porošenka
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Marynou	Maryna	k1gFnSc7	Maryna
Porošenkovou	Porošenkový	k2eAgFnSc7d1	Porošenkový
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnikatelské	podnikatelský	k2eAgFnPc4d1	podnikatelská
aktivity	aktivita	k1gFnPc4	aktivita
a	a	k8xC	a
majetek	majetek	k1gInSc4	majetek
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ruského	ruský	k2eAgNnSc2d1	ruské
vydání	vydání	k1gNnSc2	vydání
časopisu	časopis	k1gInSc2	časopis
Forbes	forbes	k1gInSc1	forbes
z	z	k7c2	z
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
sedmým	sedmý	k4xOgMnSc7	sedmý
nejbohatším	bohatý	k2eAgMnSc7d3	nejbohatší
Ukrajincem	Ukrajinec	k1gMnSc7	Ukrajinec
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
jmění	jmění	k1gNnSc2	jmění
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
odhadovalo	odhadovat	k5eAaImAgNnS	odhadovat
na	na	k7c4	na
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
novějšího	nový	k2eAgInSc2d2	novější
odhadu	odhad	k1gInSc2	odhad
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
vlastní	vlastní	k2eAgFnSc4d1	vlastní
Porošenko	Porošenka	k1gFnSc5	Porošenka
majetek	majetek	k1gInSc4	majetek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
již	již	k6eAd1	již
jen	jen	k9	jen
500	[number]	k4	500
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jej	on	k3xPp3gInSc4	on
časopis	časopis	k1gInSc4	časopis
Forbes	forbes	k1gInSc1	forbes
neuvádí	uvádět	k5eNaImIp3nS	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
miliardářů	miliardář	k1gMnPc2	miliardář
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
veden	vést	k5eAaImNgInS	vést
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
1284	[number]	k4	1284
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mu	on	k3xPp3gMnSc3	on
mj.	mj.	kA	mj.
firma	firma	k1gFnSc1	firma
Roshen	Roshen	k1gInSc1	Roshen
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
ukrajinským	ukrajinský	k2eAgMnSc7d1	ukrajinský
výrobcem	výrobce	k1gMnSc7	výrobce
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tato	tento	k3xDgFnSc1	tento
firma	firma	k1gFnSc1	firma
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
čokoládu	čokoláda	k1gFnSc4	čokoláda
také	také	k9	také
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
aktuální	aktuální	k2eAgInPc1d1	aktuální
údaje	údaj	k1gInPc1	údaj
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Porošenko	Porošenka	k1gFnSc5	Porošenka
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
znám	znát	k5eAaImIp1nS	znát
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
"	"	kIx"	"
<g/>
čokoládový	čokoládový	k2eAgMnSc1d1	čokoládový
král	král	k1gMnSc1	král
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Porošenko	Porošenka	k1gFnSc5	Porošenka
byl	být	k5eAaImAgInS	být
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Panamských	panamský	k2eAgInPc6d1	panamský
dokumentech	dokument	k1gInPc6	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
slíbil	slíbit	k5eAaPmAgMnS	slíbit
prodat	prodat	k5eAaPmF	prodat
svou	svůj	k3xOyFgFnSc4	svůj
firmu	firma	k1gFnSc4	firma
na	na	k7c4	na
cukrovinky	cukrovinka	k1gFnPc4	cukrovinka
Rošen	Rošna	k1gFnPc2	Rošna
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
podle	podle	k7c2	podle
Panamských	panamský	k2eAgInPc2d1	panamský
dokumentů	dokument	k1gInPc2	dokument
přesunul	přesunout	k5eAaPmAgMnS	přesunout
své	svůj	k3xOyFgNnSc4	svůj
podnikání	podnikání	k1gNnSc4	podnikání
do	do	k7c2	do
offshorového	offshorový	k2eAgNnSc2d1	offshorový
centra	centrum	k1gNnSc2	centrum
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
Panenských	panenský	k2eAgInPc6d1	panenský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ušetřil	ušetřit	k5eAaPmAgMnS	ušetřit
miliony	milion	k4xCgInPc4	milion
na	na	k7c6	na
daních	daň	k1gFnPc6	daň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
jinak	jinak	k6eAd1	jinak
musel	muset	k5eAaImAgInS	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
možného	možný	k2eAgInSc2d1	možný
daňového	daňový	k2eAgInSc2d1	daňový
úniku	únik	k1gInSc2	únik
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Oleh	Oleh	k1gMnSc1	Oleh
Ljaško	Ljaška	k1gFnSc5	Ljaška
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Radikální	radikální	k2eAgFnSc2d1	radikální
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
obžalobu	obžaloba	k1gFnSc4	obžaloba
Porošenka	Porošenka	k1gFnSc1	Porošenka
<g/>
.	.	kIx.	.
</s>
<s>
Skandál	skandál	k1gInSc1	skandál
rozdmýchal	rozdmýchat	k5eAaPmAgInS	rozdmýchat
měsíce	měsíc	k1gInSc2	měsíc
trvající	trvající	k2eAgInPc1d1	trvající
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
Porošenkovým	Porošenkový	k2eAgInSc7d1	Porošenkový
blokem	blok	k1gInSc7	blok
a	a	k8xC	a
Lidovou	lidový	k2eAgFnSc7d1	lidová
frontou	fronta	k1gFnSc7	fronta
Arsenija	Arsenijus	k1gMnSc2	Arsenijus
Jaceňuka	Jaceňuk	k1gMnSc2	Jaceňuk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
vzájemně	vzájemně	k6eAd1	vzájemně
obviňují	obviňovat	k5eAaImIp3nP	obviňovat
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Porošenko	Porošenka	k1gFnSc5	Porošenka
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
také	také	k9	také
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
Paradise	Paradise	k1gFnSc1	Paradise
Papers	Papers	k1gInSc1	Papers
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
a	a	k8xC	a
které	který	k3yIgNnSc1	který
odhalují	odhalovat	k5eAaImIp3nP	odhalovat
daňové	daňový	k2eAgInPc1d1	daňový
úniky	únik	k1gInPc1	únik
skrze	skrze	k?	skrze
daňové	daňový	k2eAgFnSc2d1	daňová
ráje	rája	k1gFnSc2	rája
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
serveru	server	k1gInSc2	server
WikiLeaks	WikiLeaksa	k1gFnPc2	WikiLeaksa
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
až	až	k9	až
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Porošenko	Porošenka	k1gFnSc5	Porošenka
vedl	vést	k5eAaImAgInS	vést
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
výbor	výbor	k1gInSc4	výbor
pro	pro	k7c4	pro
finance	finance	k1gFnPc4	finance
a	a	k8xC	a
bankovnictví	bankovnictví	k1gNnSc4	bankovnictví
<g/>
,	,	kIx,	,
popisovali	popisovat	k5eAaImAgMnP	popisovat
američtí	americký	k2eAgMnPc1d1	americký
diplomaté	diplomat	k1gMnPc1	diplomat
Porošenka	Porošenka	k1gFnSc1	Porošenka
jako	jako	k8xC	jako
oligarchu	oligarcha	k1gMnSc4	oligarcha
zdiskreditovaného	zdiskreditovaný	k2eAgInSc2d1	zdiskreditovaný
nařčeními	nařčení	k1gNnPc7	nařčení
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
Porošenko	Porošenka	k1gFnSc5	Porošenka
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
prodá	prodat	k5eAaPmIp3nS	prodat
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgInPc2	svůj
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
Pátý	pátý	k4xOgInSc1	pátý
kanál	kanál	k1gInSc1	kanál
a	a	k8xC	a
potravinářské	potravinářský	k2eAgFnSc2d1	potravinářská
firmy	firma	k1gFnSc2	firma
Rošen	Rošna	k1gFnPc2	Rošna
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
nesplnil	splnit	k5eNaPmAgMnS	splnit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
hodnotu	hodnota	k1gFnSc4	hodnota
svých	svůj	k3xOyFgNnPc2	svůj
aktiv	aktivum	k1gNnPc2	aktivum
několikanásobně	několikanásobně	k6eAd1	několikanásobně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
Rádio	rádio	k1gNnSc4	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
Porošenko	Porošenka	k1gFnSc5	Porošenka
získal	získat	k5eAaPmAgInS	získat
za	za	k7c2	za
podivných	podivný	k2eAgFnPc2d1	podivná
okolností	okolnost	k1gFnPc2	okolnost
pozemky	pozemka	k1gFnSc2	pozemka
v	v	k7c6	v
ochranné	ochranný	k2eAgFnSc6d1	ochranná
zóně	zóna	k1gFnSc6	zóna
poblíž	poblíž	k7c2	poblíž
památky	památka	k1gFnSc2	památka
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ukrajinskými	ukrajinský	k2eAgInPc7d1	ukrajinský
zákony	zákon	k1gInPc7	zákon
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
začal	začít	k5eAaPmAgMnS	začít
stavět	stavět	k5eAaImF	stavět
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
poškodil	poškodit	k5eAaPmAgInS	poškodit
část	část	k1gFnSc4	část
opevnění	opevnění	k1gNnSc2	opevnění
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
prokuratury	prokuratura	k1gFnSc2	prokuratura
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
bylo	být	k5eAaImAgNnS	být
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
pět	pět	k4xCc1	pět
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zvolení	zvolení	k1gNnSc6	zvolení
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
označil	označit	k5eAaPmAgMnS	označit
Porošenko	Porošenka	k1gFnSc5	Porošenka
bojovníky	bojovník	k1gMnPc4	bojovník
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
známé	známá	k1gFnSc2	známá
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
banderovci	banderovec	k1gMnSc6	banderovec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
bojovali	bojovat	k5eAaImAgMnP	bojovat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
ukrajinský	ukrajinský	k2eAgInSc4d1	ukrajinský
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
<g />
.	.	kIx.	.
</s>
<s>
masově	masově	k6eAd1	masově
vyvražďovali	vyvražďovat	k5eAaImAgMnP	vyvražďovat
polské	polský	k2eAgMnPc4d1	polský
civilisty	civilista	k1gMnPc4	civilista
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
za	za	k7c7	za
"	"	kIx"	"
<g/>
příklad	příklad	k1gInSc1	příklad
hrdinství	hrdinství	k1gNnSc1	hrdinství
a	a	k8xC	a
vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
jara	jaro	k1gNnSc2	jaro
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
zablokovány	zablokovat	k5eAaPmNgFnP	zablokovat
sociální	sociální	k2eAgFnPc1d1	sociální
sítě	síť	k1gFnPc1	síť
VKontakte	VKontakt	k1gInSc5	VKontakt
a	a	k8xC	a
Odnoklasniky	Odnoklasnik	k1gInPc7	Odnoklasnik
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
vysílání	vysílání	k1gNnSc1	vysílání
řady	řada	k1gFnSc2	řada
ruských	ruský	k2eAgFnPc2d1	ruská
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
zakázáno	zakázán	k2eAgNnSc4d1	zakázáno
působení	působení	k1gNnSc4	působení
ruských	ruský	k2eAgFnPc2d1	ruská
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
firem	firma	k1gFnPc2	firma
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	se	k3xPyFc4	se
vývojem	vývoj	k1gInSc7	vývoj
bezbečnostních	bezbečnostní	k2eAgInPc2d1	bezbečnostní
softwarů	software	k1gInPc2	software
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
Porošenka	Porošenka	k1gFnSc1	Porošenka
omezit	omezit	k5eAaPmF	omezit
údajnou	údajný	k2eAgFnSc4d1	údajná
ruskou	ruský	k2eAgFnSc4d1	ruská
podporu	podpora	k1gFnSc4	podpora
separatistů	separatista	k1gMnPc2	separatista
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
síť	síť	k1gFnSc1	síť
VKontakte	VKontakt	k1gInSc5	VKontakt
je	on	k3xPp3gNnPc4	on
však	však	k9	však
v	v	k7c6	v
rusky	rusky	k6eAd1	rusky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
obdobné	obdobný	k2eAgNnSc4d1	obdobné
postavení	postavení	k1gNnSc4	postavení
jako	jako	k8xS	jako
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
síť	síť	k1gFnSc1	síť
Facebook	Facebook	k1gInSc1	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Nařízení	nařízení	k1gNnSc1	nařízení
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
nepopulární	populární	k2eNgMnSc1d1	nepopulární
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
mezi	mezi	k7c7	mezi
mladými	mladý	k2eAgInPc7d1	mladý
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
řadou	řada	k1gFnSc7	řada
organizací	organizace	k1gFnPc2	organizace
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
omezování	omezování	k1gNnSc4	omezování
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2018	[number]	k4	2018
podepsal	podepsat	k5eAaPmAgInS	podepsat
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
získali	získat	k5eAaPmAgMnP	získat
někdejší	někdejší	k2eAgMnPc1d1	někdejší
příslušníci	příslušník	k1gMnPc1	příslušník
Organizace	organizace	k1gFnSc2	organizace
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
(	(	kIx(	(
<g/>
OUN	OUN	kA	OUN
<g/>
)	)	kIx)	)
i	i	k9	i
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
UPA	UPA	kA	UPA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
označováni	označován	k2eAgMnPc1d1	označován
jako	jako	k8xS	jako
Banderovci	banderovec	k1gMnPc1	banderovec
postavení	postavení	k1gNnSc2	postavení
válečných	válečný	k2eAgMnPc2d1	válečný
veteránů	veterán	k1gMnPc2	veterán
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnPc1d1	související
sociální	sociální	k2eAgFnPc4d1	sociální
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Petro	Petra	k1gFnSc5	Petra
Porošenko	Porošenka	k1gFnSc5	Porošenka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Petro	Petra	k1gFnSc5	Petra
Porošenko	Porošenka	k1gFnSc5	Porošenka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
