<s>
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
</s>
<s>
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
(	(	kIx(
<g/>
Alliaria	Alliarium	k1gNnSc2
petiolata	petiole	k1gNnPc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
brukvotvaré	brukvotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Brassicales	Brassicales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
brukvovité	brukvovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Brassicaceae	Brassicacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
česnáček	česnáček	k1gInSc1
(	(	kIx(
<g/>
Alliaria	Alliarium	k1gNnSc2
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Alliaria	Alliarium	k1gNnPc1
petiolata	petiole	k1gNnPc1
<g/>
(	(	kIx(
<g/>
M.	M.	kA
Bieb	Bieb	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Cavara	Cavara	k1gFnSc1
&	&	k?
Grande	grand	k1gMnSc5
<g/>
,	,	kIx,
1913	#num#	k4
Synonyma	synonymum	k1gNnSc2
</s>
<s>
Alliaria	Alliarium	k1gNnPc1
alliaria	alliarium	k1gNnSc2
<g/>
,	,	kIx,
Alliaria	Alliarium	k1gNnSc2
officinalis	officinalis	k1gFnSc2
</s>
<s>
česnáček	česnáček	k1gInSc1
obecný	obecný	k2eAgInSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
(	(	kIx(
<g/>
Alliaria	Alliarium	k1gNnSc2
petiolata	petiole	k1gNnPc1
<g/>
)	)	kIx)
je	on	k3xPp3gFnPc4
vyšší	vysoký	k2eAgFnPc4d2
<g/>
,	,	kIx,
volně	volně	k6eAd1
rostoucí	rostoucí	k2eAgFnSc1d1
plevelná	plevelný	k2eAgFnSc1d1
bylina	bylina	k1gFnSc1
charakteristické	charakteristický	k2eAgFnSc2d1
vůně	vůně	k1gFnSc2
po	po	k7c6
česneku	česnek	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
dvou	dva	k4xCgInPc2
druhů	druh	k1gInPc2
rodu	rod	k1gInSc2
česnáček	česnáček	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
téměř	téměř	k6eAd1
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
nevyrůstá	vyrůstat	k5eNaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Skandinávie	Skandinávie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
je	být	k5eAaImIp3nS
domovem	domov	k1gInSc7
v	v	k7c6
západní	západní	k2eAgFnSc6d1
a	a	k8xC
střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
až	až	k9
k	k	k7c3
himálajskému	himálajský	k2eAgNnSc3d1
podhůří	podhůří	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšířil	rozšířit	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
do	do	k7c2
středomořských	středomořský	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Afriky	Afrika	k1gFnSc2
a	a	k8xC
novodobě	novodobě	k6eAd1
i	i	k9
do	do	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
je	být	k5eAaImIp3nS
k	k	k7c3
nalezení	nalezení	k1gNnSc3
na	na	k7c6
půdách	půda	k1gFnPc6
vlhkých	vlhký	k2eAgFnPc6d1
a	a	k8xC
zároveň	zároveň	k6eAd1
dobře	dobře	k6eAd1
zásobených	zásobený	k2eAgInPc2d1
živinami	živina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
ve	v	k7c6
světlých	světlý	k2eAgInPc6d1
lesích	les	k1gInPc6
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gInPc6
okrajích	okraj	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
křovinách	křovina	k1gFnPc6
a	a	k8xC
neobdělávaných	obdělávaný	k2eNgInPc6d1
úhorech	úhor	k1gInPc6
<g/>
,	,	kIx,
stěhuje	stěhovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
do	do	k7c2
blízkosti	blízkost	k1gFnSc2
lidských	lidský	k2eAgNnPc2d1
sídel	sídlo	k1gNnPc2
<g/>
,	,	kIx,
do	do	k7c2
parků	park	k1gInPc2
<g/>
,	,	kIx,
zahrad	zahrada	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
rumiště	rumiště	k1gNnSc4
i	i	k8xC
komposty	kompost	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nitrofilní	nitrofilní	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
vyhovuje	vyhovovat	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
vyšší	vysoký	k2eAgFnSc1d2
vzdušná	vzdušný	k2eAgFnSc1d1
vlhkost	vlhkost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
je	být	k5eAaImIp3nS
rostlina	rostlina	k1gFnSc1
poněkud	poněkud	k6eAd1
teplomilná	teplomilný	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
proto	proto	k8xC
vyskytuje	vyskytovat	k5eAaImIp3nS
nejčastěji	často	k6eAd3
v	v	k7c6
nížinných	nížinný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
do	do	k7c2
vyšších	vysoký	k2eAgFnPc2d2
poloh	poloha	k1gFnPc2
vystupuje	vystupovat	k5eAaImIp3nS
zřídka	zřídka	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
termofytiku	termofytikum	k1gNnSc6
a	a	k8xC
v	v	k7c6
teplejším	teplý	k2eAgNnSc6d2
mezofytiku	mezofytikum	k1gNnSc6
je	být	k5eAaImIp3nS
na	na	k7c6
celém	celý	k2eAgNnSc6d1
území	území	k1gNnSc6
poměrně	poměrně	k6eAd1
hojným	hojný	k2eAgNnSc7d1
<g/>
,	,	kIx,
v	v	k7c6
chladnějším	chladný	k2eAgNnSc6d2
mezofytiku	mezofytikum	k1gNnSc6
jen	jen	k9
roztroušeně	roztroušeně	k6eAd1
a	a	k8xC
převážně	převážně	k6eAd1
poblíž	poblíž	k6eAd1
lidských	lidský	k2eAgNnPc2d1
sídel	sídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
oreofytika	oreofytikum	k1gNnSc2
proniká	pronikat	k5eAaImIp3nS
pouze	pouze	k6eAd1
zřídka	zřídka	k6eAd1
<g/>
,	,	kIx,
nejvýše	nejvýše	k6eAd1,k6eAd3
do	do	k7c2
900	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Lodyha	lodyha	k1gFnSc1
této	tento	k3xDgFnSc2
dvouleté	dvouletý	k2eAgFnSc2d1
nebo	nebo	k8xC
krátce	krátce	k6eAd1
vytrvalé	vytrvalý	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
dorůstá	dorůstat	k5eAaImIp3nS
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
20	#num#	k4
až	až	k9
80	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
v	v	k7c6
dobrých	dobrý	k2eAgFnPc6d1
přírodních	přírodní	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
je	být	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
až	až	k9
1	#num#	k4
m.	m.	k?
Bývá	bývat	k5eAaImIp3nS
nevětvená	větvený	k2eNgFnSc1d1
nebo	nebo	k8xC
větvená	větvený	k2eAgFnSc1d1
až	až	k9
v	v	k7c6
květenství	květenství	k1gNnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
slabě	slabě	k6eAd1
hranatě	hranatě	k6eAd1
rýhovaná	rýhovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
mívá	mívat	k5eAaImIp3nS
tenké	tenký	k2eAgInPc1d1
dlouhé	dlouhý	k2eAgInPc1d1
chlupy	chlup	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozemnutí	rozemnutí	k1gNnSc6
lodyhy	lodyha	k1gFnSc2
<g/>
,	,	kIx,
listů	list	k1gInPc2
nebo	nebo	k8xC
po	po	k7c6
poranění	poranění	k1gNnSc6
tlustého	tlustý	k2eAgInSc2d1
vřetenovitého	vřetenovitý	k2eAgInSc2d1
kořene	kořen	k1gInSc2
s	s	k7c7
jemnými	jemný	k2eAgInPc7d1
postranními	postranní	k2eAgInPc7d1
kořínky	kořínek	k1gInPc7
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
česnekové	česnekový	k2eAgNnSc1d1
aroma	aroma	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dva	dva	k4xCgInPc4
druhy	druh	k1gInPc4
listů	list	k1gInPc2
<g/>
,	,	kIx,
ty	ten	k3xDgInPc1
v	v	k7c6
přízemní	přízemní	k2eAgFnSc6d1
růžici	růžice	k1gFnSc6
s	s	k7c7
chlupatými	chlupatý	k2eAgInPc7d1
řapíky	řapík	k1gInPc7
5	#num#	k4
až	až	k9
8	#num#	k4
cm	cm	kA
dlouhými	dlouhý	k2eAgInPc7d1
mají	mít	k5eAaImIp3nP
lysou	lysý	k2eAgFnSc4d1
listovou	listový	k2eAgFnSc4d1
čepel	čepel	k1gFnSc4
ledvinovitého	ledvinovitý	k2eAgInSc2d1
až	až	k8xS
okrouhlého	okrouhlý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
dosahující	dosahující	k2eAgFnSc2d1
délky	délka	k1gFnSc2
až	až	k9
17	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
po	po	k7c6
obvodě	obvod	k1gInSc6
jsou	být	k5eAaImIp3nP
zubaté	zubatý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sytě	sytě	k6eAd1
zelené	zelený	k2eAgInPc1d1
lodyžní	lodyžní	k2eAgInPc1d1
listy	list	k1gInPc1
jsou	být	k5eAaImIp3nP
o	o	k7c6
poznání	poznání	k1gNnSc6
menší	malý	k2eAgInPc1d2
<g/>
,	,	kIx,
3	#num#	k4
až	až	k9
6	#num#	k4
cm	cm	kA
dlouhé	dlouhý	k2eAgFnSc2d1
a	a	k8xC
3	#num#	k4
až	až	k9
5	#num#	k4
cm	cm	kA
široké	široký	k2eAgFnPc1d1
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInPc1
řapíky	řapík	k1gInPc1
mají	mít	k5eAaImIp3nP
jen	jen	k9
1	#num#	k4
až	až	k9
2	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
tvaru	tvar	k1gInSc2
jsou	být	k5eAaImIp3nP
trojúhelníkovitě	trojúhelníkovitě	k6eAd1
vejčitého	vejčitý	k2eAgNnSc2d1
s	s	k7c7
uťatou	uťatý	k2eAgFnSc7d1
srdčitou	srdčitý	k2eAgFnSc7d1
bázi	báze	k1gFnSc4
<g/>
,	,	kIx,
vrchol	vrchol	k1gInSc4
mají	mít	k5eAaImIp3nP
tupě	tupě	k6eAd1
špičatý	špičatý	k2eAgInSc4d1
<g/>
,	,	kIx,
po	po	k7c6
obvodě	obvod	k1gInSc6
jsou	být	k5eAaImIp3nP
vroubkované	vroubkovaný	k2eAgInPc1d1
až	až	k9
hrubě	hrubě	k6eAd1
zubaté	zubatý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Oboupohlavné	oboupohlavný	k2eAgInPc4d1
květy	květ	k1gInPc4
na	na	k7c6
krátkých	krátký	k2eAgFnPc6d1
tenkých	tenký	k2eAgFnPc6d1
stopkách	stopka	k1gFnPc6
(	(	kIx(
<g/>
do	do	k7c2
4	#num#	k4
mm	mm	kA
délky	délka	k1gFnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
seskupeny	seskupen	k2eAgInPc1d1
do	do	k7c2
nedlouhých	dlouhý	k2eNgInPc2d1
<g/>
,	,	kIx,
jednoduchých	jednoduchý	k2eAgInPc2d1
nebo	nebo	k8xC
větvených	větvený	k2eAgInPc2d1
hroznů	hrozen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyři	čtyři	k4xCgInPc1
kališní	kališní	k2eAgInPc1d1
lístky	lístek	k1gInPc1
úzce	úzko	k6eAd1
vejčitého	vejčitý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
a	a	k8xC
délky	délka	k1gFnSc2
2	#num#	k4
až	až	k9
3,5	3,5	k4
mm	mm	kA
mají	mít	k5eAaImIp3nP
barvu	barva	k1gFnSc4
bledě	bledě	k6eAd1
zelenou	zelený	k2eAgFnSc4d1
se	s	k7c7
světlým	světlý	k2eAgInSc7d1
lemem	lem	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korunní	korunní	k2eAgInPc1d1
lístky	lístek	k1gInPc1
<g/>
,	,	kIx,
také	také	k9
čtyři	čtyři	k4xCgFnPc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
podlouhle	podlouhle	k6eAd1
vejčité	vejčitý	k2eAgInPc1d1
<g/>
,	,	kIx,
5	#num#	k4
až	až	k9
6	#num#	k4
mm	mm	kA
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
čistě	čistě	k6eAd1
bílé	bílý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc1
tyčinek	tyčinka	k1gFnPc2
s	s	k7c7
nitkami	nitka	k1gFnPc7
o	o	k7c6
dvou	dva	k4xCgFnPc6
délkách	délka	k1gFnPc6
je	být	k5eAaImIp3nS
zakončeno	zakončit	k5eAaPmNgNnS
podlouhlými	podlouhlý	k2eAgInPc7d1
prašníky	prašník	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semeník	semeník	k1gInSc1
s	s	k7c7
5	#num#	k4
až	až	k9
25	#num#	k4
vajíčky	vajíčko	k1gNnPc7
nese	nést	k5eAaImIp3nS
čnělku	čnělka	k1gFnSc4
2	#num#	k4
mm	mm	kA
dlouhou	dlouhý	k2eAgFnSc4d1
s	s	k7c7
úzkou	úzký	k2eAgFnSc7d1
bliznou	blizna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	květ	k1gInPc7
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
od	od	k7c2
dubna	duben	k1gInSc2
do	do	k7c2
června	červen	k1gInSc2
<g/>
,	,	kIx,
opylovány	opylován	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
létajícím	létající	k2eAgInSc7d1
hmyzem	hmyz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c4
opylení	opylení	k1gNnSc4
se	se	k3xPyFc4
stopky	stopka	k1gFnSc2
květů	květ	k1gInPc2
(	(	kIx(
<g/>
plodní	plodní	k2eAgFnPc4d1
stopky	stopka	k1gFnPc4
<g/>
)	)	kIx)
postupně	postupně	k6eAd1
rozšiřují	rozšiřovat	k5eAaImIp3nP
až	až	k8xS
dosahují	dosahovat	k5eAaImIp3nP
tloušťky	tloušťka	k1gFnPc4
téměř	téměř	k6eAd1
jako	jako	k8xC,k8xS
plody	plod	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jsou	být	k5eAaImIp3nP
podlouhlé	podlouhlý	k2eAgFnPc1d1
šešule	šešule	k1gFnPc1
o	o	k7c6
délce	délka	k1gFnSc6
3	#num#	k4
až	až	k9
5	#num#	k4
cm	cm	kA
a	a	k8xC
tloušťce	tloušťka	k1gFnSc3
do	do	k7c2
2,5	2,5	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
vyrůstají	vyrůstat	k5eAaImIp3nP
šikmo	šikmo	k6eAd1
až	až	k9
vzpřímeně	vzpřímeně	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
jsou	být	k5eAaImIp3nP
zúžené	zúžený	k2eAgFnPc1d1
<g/>
,	,	kIx,
zakončení	zakončení	k1gNnPc1
jsou	být	k5eAaImIp3nP
vytrvalou	vytrvalý	k2eAgFnSc7d1
čnělkou	čnělka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
šešulí	šešule	k1gFnPc2
<g/>
,	,	kIx,
otvírajících	otvírající	k2eAgInPc2d1
se	s	k7c7
chlopněmi	chlopeň	k1gFnPc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
v	v	k7c6
jedné	jeden	k4xCgFnSc6
řadě	řada	k1gFnSc6
tmavě	tmavě	k6eAd1
hnědá	hnědat	k5eAaImIp3nS
semínka	semínko	k1gNnPc4
až	až	k6eAd1
3,5	3,5	k4
mm	mm	kA
dlouhá	dlouhý	k2eAgFnSc1d1
a	a	k8xC
1	#num#	k4
mm	mm	kA
široká	široký	k2eAgFnSc1d1
s	s	k7c7
podélně	podélně	k6eAd1
jemně	jemně	k6eAd1
rýhovaným	rýhovaný	k2eAgNnSc7d1
osemením	osemení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Semena	semeno	k1gNnPc1
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
rozsévána	rozsévat	k5eAaImNgNnP
jen	jen	k6eAd1
do	do	k7c2
blízkého	blízký	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
obsahuje	obsahovat	k5eAaImIp3nS
mj.	mj.	kA
glykosidy	glykosid	k1gInPc1
sinigrin	sinigrin	k1gInSc1
<g/>
,	,	kIx,
allylsulfid	allylsulfid	k1gInSc1
<g/>
,	,	kIx,
rhodanalyl	rhodanalyl	k1gInSc1
<g/>
,	,	kIx,
alliarin	alliarin	k1gInSc1
<g/>
,	,	kIx,
etérický	etérický	k2eAgInSc1d1
olej	olej	k1gInSc1
<g/>
,	,	kIx,
pektin	pektin	k1gInSc1
<g/>
,	,	kIx,
beta	beta	k1gNnSc1
karoten	karoten	k1gInSc4
a	a	k8xC
kyselinu	kyselina	k1gFnSc4
askorbovou	askorbový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
porušení	porušení	k1gNnSc6
pletiv	pletivo	k1gNnPc2
se	se	k3xPyFc4
uvolňují	uvolňovat	k5eAaImIp3nP
štiplavé	štiplavý	k2eAgInPc1d1
a	a	k8xC
čpavé	čpavý	k2eAgInPc1d1
isothiokyanáty	isothiokyanát	k1gInPc1
o	o	k7c6
kterých	který	k3yQgInPc6,k3yIgInPc6,k3yRgInPc6
se	se	k3xPyFc4
soudí	soudit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
mají	mít	k5eAaImIp3nP
antikarcinogenní	antikarcinogenní	k2eAgInPc4d1
účinky	účinek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chromozómové	chromozómový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
n	n	k0
=	=	kIx~
42	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
je	být	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc1d1
léčivou	léčivý	k2eAgFnSc7d1
bylinou	bylina	k1gFnSc7
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
se	se	k3xPyFc4
sporadicky	sporadicky	k6eAd1
užívá	užívat	k5eAaImIp3nS
v	v	k7c6
lidovém	lidový	k2eAgNnSc6d1
léčitelství	léčitelství	k1gNnSc6
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
antiseptické	antiseptický	k2eAgInPc4d1
a	a	k8xC
hojivé	hojivý	k2eAgInPc4d1
účinky	účinek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zevně	zevně	k6eAd1
se	se	k3xPyFc4
používaly	používat	k5eAaImAgInP
čerstvé	čerstvý	k2eAgInPc1d1
listy	list	k1gInPc1
na	na	k7c6
nehojící	hojící	k2eNgFnSc6d1
se	se	k3xPyFc4
rány	rána	k1gFnSc2
a	a	k8xC
pro	pro	k7c4
vnitřní	vnitřní	k2eAgNnSc4d1
užití	užití	k1gNnSc4
se	se	k3xPyFc4
z	z	k7c2
usušené	usušený	k2eAgFnSc2d1
drogy	droga	k1gFnSc2
vařily	vařit	k5eAaImAgInP
čaje	čaj	k1gInPc1
nebo	nebo	k8xC
z	z	k7c2
čerstvé	čerstvý	k2eAgFnSc2d1
se	se	k3xPyFc4
připravovaly	připravovat	k5eAaImAgFnP
tinktury	tinktura	k1gFnPc1
<g/>
,	,	kIx,
sloužily	sloužit	k5eAaImAgInP
na	na	k7c6
desinfekci	desinfekce	k1gFnSc6
močových	močový	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
i	i	k9
proti	proti	k7c3
zánětům	zánět	k1gInPc3
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žvýkáním	žvýkání	k1gNnSc7
listů	list	k1gInPc2
se	se	k3xPyFc4
také	také	k9
léčil	léčit	k5eAaImAgInS
zánět	zánět	k1gInSc1
ústní	ústní	k2eAgFnSc2d1
dutiny	dutina	k1gFnSc2
a	a	k8xC
paradentóza	paradentóza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladé	mladý	k2eAgInPc1d1
listy	list	k1gInPc1
se	se	k3xPyFc4
konzumovaly	konzumovat	k5eAaBmAgInP
především	především	k9
čerstvé	čerstvý	k2eAgInPc1d1
v	v	k7c6
zeleninových	zeleninový	k2eAgInPc6d1
salátech	salát	k1gInPc6
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
pikantnost	pikantnost	k1gFnSc4
a	a	k8xC
vysoký	vysoký	k2eAgInSc4d1
obsah	obsah	k1gInSc4
vitamínu	vitamín	k1gInSc2
C.	C.	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
</s>
<s>
Listy	list	k1gInPc1
</s>
<s>
Květy	květ	k1gInPc1
</s>
<s>
Postupné	postupný	k2eAgNnSc1d1
zrání	zrání	k1gNnSc1
plodů	plod	k1gInPc2
</s>
<s>
Semena	semeno	k1gNnPc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ZICHA	Zich	k1gMnSc4
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ondřej	Ondřej	k1gMnSc1
Zicha	Zich	k1gMnSc2
<g/>
,	,	kIx,
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
18.10	18.10	k4
<g/>
.2004	.2004	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HOSKOVEC	HOSKOVEC	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BOTANY	BOTANY	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BOTANY	BOTANY	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
19.02	19.02	k4
<g/>
.2008	.2008	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
VINDUŠKOVÁ	VINDUŠKOVÁ	kA
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česnáček	česnáček	k1gInSc1
Lékařský	lékařský	k2eAgInSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mendelova	Mendelův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Agronomická	agronomický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
GRIN	GRIN	kA
Taxonomy	Taxonom	k1gInPc1
for	forum	k1gNnPc2
Plants	Plants	k1gInSc1
<g/>
:	:	kIx,
Alliaria	Alliarium	k1gNnSc2
petiolata	petiole	k1gNnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Department	department	k1gInSc1
of	of	k?
Agriculture	Agricultur	k1gMnSc5
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
0	#num#	k4
<g/>
9.05	9.05	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AL-SHEHBAZ	AL-SHEHBAZ	k1gMnSc1
<g/>
,	,	kIx,
Ihsan	Ihsan	k1gMnSc1
A.	A.	kA
Projects	Projects	k1gInSc1
Brassicaceae	Brassicaceae	k1gNnSc2
<g/>
:	:	kIx,
Alliaria	Alliarium	k1gNnSc2
petiolata	petiole	k1gNnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tropicos	Tropicos	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
Missouri	Missouri	k1gNnSc1
Botanic	Botanice	k1gFnPc2
Garden	Gardna	k1gFnPc2
<g/>
,	,	kIx,
St.	st.	kA
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
MO	MO	kA
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
21.07	21.07	k4
<g/>
.2009	.2009	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Flora	Flora	k1gFnSc1
of	of	k?
North	North	k1gInSc4
America	Americus	k1gMnSc4
<g/>
:	:	kIx,
Alliaria	Alliarium	k1gNnSc2
petiolata	petiole	k1gNnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Missouri	Missouri	k1gNnSc1
Botanical	Botanical	k1gFnPc2
Garden	Gardna	k1gFnPc2
<g/>
,	,	kIx,
St.	st.	kA
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
MO	MO	kA
&	&	k?
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Herbaria	Herbarium	k1gNnSc2
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc2
<g/>
,	,	kIx,
MA	MA	kA
<g/>
,	,	kIx,
USA	USA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KVASNIČKOVÁ	Kvasničková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronavigator	Astronavigator	k1gInSc1
<g/>
:	:	kIx,
Zdravotní	zdravotní	k2eAgInSc1d1
prospěch	prospěch	k1gInSc1
zeleniny	zelenina	k1gFnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
brukvovitých	brukvovitý	k2eAgFnPc2d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ÚZEI	ÚZEI	kA
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
zemědělské	zemědělský	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
a	a	k8xC
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
29.06	29.06	k4
<g/>
.2004	.2004	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Herbář	herbář	k1gInSc1
Wendys	Wendys	k1gInSc1
<g/>
:	:	kIx,
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wendys	Wendys	k1gInSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Pazdera	Pazdera	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Výukový	výukový	k2eAgInSc1d1
kurs	kurs	k1gInSc1
PKR	PKR	kA
<g/>
/	/	kIx~
<g/>
Alliaria	Alliarium	k1gNnSc2
petiolata	petiole	k1gNnPc1
ve	v	k7c6
Wikiverzitě	Wikiverzita	k1gFnSc6
</s>
<s>
Taxon	taxon	k1gInSc1
Alliaria	Alliarium	k1gNnSc2
petiolata	petiole	k1gNnPc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
