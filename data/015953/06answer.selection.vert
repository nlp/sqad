<s>
Česnáček	česnáček	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
je	být	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc1d1
léčivou	léčivý	k2eAgFnSc7d1
bylinou	bylina	k1gFnSc7
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
se	se	k3xPyFc4
sporadicky	sporadicky	k6eAd1
užívá	užívat	k5eAaImIp3nS
v	v	k7c6
lidovém	lidový	k2eAgNnSc6d1
léčitelství	léčitelství	k1gNnSc6
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
antiseptické	antiseptický	k2eAgInPc4d1
a	a	k8xC
hojivé	hojivý	k2eAgInPc4d1
účinky	účinek	k1gInPc4
<g/>
.	.	kIx.
</s>