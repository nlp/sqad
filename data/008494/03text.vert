<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
asociace	asociace	k1gFnSc2	asociace
FIFA	FIFA	kA	FIFA
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
městech	město	k1gNnPc6	město
na	na	k7c6	na
dvanácti	dvanáct	k4xCc2	dvanáct
stadionech	stadion	k1gInPc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
<g/>
:	:	kIx,	:
Moskva	Moskva	k1gFnSc1	Moskva
(	(	kIx(	(
<g/>
stadiony	stadion	k1gInPc1	stadion
Lužniki	Lužnik	k1gFnSc2	Lužnik
a	a	k8xC	a
Otkrytije	Otkrytije	k1gFnSc2	Otkrytije
Arena	Aren	k1gInSc2	Aren
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Kaliningrad	Kaliningrad	k1gInSc1	Kaliningrad
<g/>
,	,	kIx,	,
Jekatěrinburg	Jekatěrinburg	k1gInSc1	Jekatěrinburg
<g/>
,	,	kIx,	,
Nižnij	Nižnij	k1gFnSc1	Nižnij
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
,	,	kIx,	,
Kazaň	Kazaň	k1gFnSc1	Kazaň
<g/>
,	,	kIx,	,
Samara	Samara	k1gFnSc1	Samara
<g/>
,	,	kIx,	,
Saransk	Saransk	k1gInSc1	Saransk
<g/>
,	,	kIx,	,
Volgograd	Volgograd	k1gInSc1	Volgograd
<g/>
,	,	kIx,	,
Rostov	Rostov	k1gInSc1	Rostov
na	na	k7c4	na
Donu	dona	k1gFnSc4	dona
a	a	k8xC	a
Soči	Soči	k1gNnSc4	Soči
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
mistrovství	mistrovství	k1gNnSc1	mistrovství
opět	opět	k6eAd1	opět
organizováno	organizovat	k5eAaBmNgNnS	organizovat
evropskou	evropský	k2eAgFnSc7d1	Evropská
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
probíhalo	probíhat	k5eAaImAgNnS	probíhat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Očekávalo	očekávat	k5eAaImAgNnS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
dosud	dosud	k6eAd1	dosud
nejdražším	drahý	k2eAgNnSc7d3	nejdražší
fotbalovým	fotbalový	k2eAgNnSc7d1	fotbalové
mistrovstvím	mistrovství	k1gNnSc7	mistrovství
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
rozpočet	rozpočet	k1gInSc1	rozpočet
se	se	k3xPyFc4	se
odhadoval	odhadovat	k5eAaImAgInS	odhadovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
14,2	[number]	k4	14,2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
turnaje	turnaj	k1gInSc2	turnaj
zahrnujícího	zahrnující	k2eAgInSc2d1	zahrnující
32	[number]	k4	32
národních	národní	k2eAgInPc2d1	národní
týmů	tým	k1gInPc2	tým
se	se	k3xPyFc4	se
mužské	mužský	k2eAgInPc1d1	mužský
fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
národní	národní	k2eAgInPc1d1	národní
týmy	tým	k1gInPc1	tým
probojovaly	probojovat	k5eAaPmAgInP	probojovat
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
probojovaly	probojovat	k5eAaPmAgInP	probojovat
celky	celek	k1gInPc1	celek
Islandu	Island	k1gInSc2	Island
a	a	k8xC	a
Panamy	Panama	k1gFnSc2	Panama
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
chyběla	chybět	k5eAaImAgFnS	chybět
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
<g/>
Finálový	finálový	k2eAgInSc1d1	finálový
zápas	zápas	k1gInSc1	zápas
turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Lužniki	Lužnik	k1gFnSc2	Lužnik
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Francie	Francie	k1gFnSc1	Francie
porazila	porazit	k5eAaPmAgFnS	porazit
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
4	[number]	k4	4
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kandidáti	kandidát	k1gMnPc1	kandidát
na	na	k7c6	na
pořadatelství	pořadatelství	k1gNnSc6	pořadatelství
==	==	k?	==
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
již	již	k6eAd1	již
ucházelo	ucházet	k5eAaImAgNnS	ucházet
o	o	k7c6	o
uspořádání	uspořádání	k1gNnSc6	uspořádání
MS	MS	kA	MS
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
FIFA	FIFA	kA	FIFA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
neuspělo	uspět	k5eNaPmAgNnS	uspět
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
hlasování	hlasování	k1gNnSc2	hlasování
o	o	k7c6	o
pořádání	pořádání	k1gNnSc6	pořádání
MS	MS	kA	MS
2018	[number]	k4	2018
členů	člen	k1gMnPc2	člen
Výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
FIFA	FIFA	kA	FIFA
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Curychu	Curych	k1gInSc6	Curych
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
32	[number]	k4	32
národních	národní	k2eAgInPc2d1	národní
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
31	[number]	k4	31
míst	místo	k1gNnPc2	místo
se	se	k3xPyFc4	se
bojuje	bojovat	k5eAaImIp3nS	bojovat
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
Rusko	Rusko	k1gNnSc1	Rusko
má	mít	k5eAaImIp3nS	mít
účast	účast	k1gFnSc4	účast
jako	jako	k8xS	jako
pořádající	pořádající	k2eAgFnSc2d1	pořádající
země	zem	k1gFnSc2	zem
zajištěnou	zajištěný	k2eAgFnSc4d1	zajištěná
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
los	los	k1gInSc1	los
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc3	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
od	od	k7c2	od
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Seznam	seznam	k1gInSc4	seznam
kvalifikovaných	kvalifikovaný	k2eAgInPc2d1	kvalifikovaný
týmů	tým	k1gInPc2	tým
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Stadiony	stadion	k1gInPc4	stadion
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Marketing	marketing	k1gInSc1	marketing
a	a	k8xC	a
organizace	organizace	k1gFnSc1	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Logo	logo	k1gNnSc1	logo
turnaje	turnaj	k1gInSc2	turnaj
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalit	k5eAaPmNgNnS	odhalit
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
kosmonauty	kosmonaut	k1gMnPc7	kosmonaut
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
kosmické	kosmický	k2eAgFnSc3d1	kosmická
stanici	stanice	k1gFnSc3	stanice
a	a	k8xC	a
poté	poté	k6eAd1	poté
promítáno	promítán	k2eAgNnSc1d1	promítáno
na	na	k7c4	na
moskevské	moskevský	k2eAgNnSc4d1	moskevské
Velké	velký	k2eAgNnSc4d1	velké
divadlo	divadlo	k1gNnSc4	divadlo
během	během	k7c2	během
večerního	večerní	k2eAgInSc2d1	večerní
televizního	televizní	k2eAgInSc2d1	televizní
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Sepp	Sepp	k1gMnSc1	Sepp
Blatter	Blatter	k1gMnSc1	Blatter
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
odráží	odrážet	k5eAaImIp3nS	odrážet
"	"	kIx"	"
<g/>
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
duši	duše	k1gFnSc4	duše
<g/>
"	"	kIx"	"
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turnajový	turnajový	k2eAgInSc1d1	turnajový
maskot	maskot	k1gInSc1	maskot
<g/>
,	,	kIx,	,
vlk	vlk	k1gMnSc1	vlk
Zabivaka	Zabivak	k1gMnSc2	Zabivak
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
skóruje	skórovat	k5eAaBmIp3nS	skórovat
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
antropomorfního	antropomorfní	k2eAgMnSc4d1	antropomorfní
vlka	vlk	k1gMnSc4	vlk
s	s	k7c7	s
oranžovými	oranžový	k2eAgFnPc7d1	oranžová
sportovními	sportovní	k2eAgFnPc7d1	sportovní
brýlemi	brýle	k1gFnPc7	brýle
a	a	k8xC	a
tričkem	tričko	k1gNnSc7	tričko
a	a	k8xC	a
šortkami	šortky	k1gFnPc7	šortky
v	v	k7c6	v
národních	národní	k2eAgFnPc6d1	národní
barvách	barva	k1gFnPc6	barva
ruského	ruský	k2eAgInSc2d1	ruský
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Maskot	maskot	k1gInSc1	maskot
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgInS	vybrat
pomocí	pomocí	k7c2	pomocí
internetového	internetový	k2eAgNnSc2d1	internetové
hlasování	hlasování	k1gNnSc2	hlasování
(	(	kIx(	(
<g/>
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
následoval	následovat	k5eAaImAgMnS	následovat
tygr	tygr	k1gMnSc1	tygr
a	a	k8xC	a
kočka	kočka	k1gFnSc1	kočka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
míč	míč	k1gInSc1	míč
mistrovství	mistrovství	k1gNnSc2	mistrovství
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Telstar	Telstar	k1gInSc1	Telstar
18	[number]	k4	18
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
a	a	k8xC	a
design	design	k1gInSc1	design
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
mistrovského	mistrovský	k2eAgInSc2d1	mistrovský
míče	míč	k1gInSc2	míč
Adidas	Adidas	k1gInSc1	Adidas
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
<g/>
Obecná	obecný	k2eAgFnSc1d1	obecná
vízová	vízový	k2eAgFnSc1d1	vízová
politika	politika	k1gFnSc1	politika
Ruska	Rusko	k1gNnSc2	Rusko
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
na	na	k7c4	na
účastníky	účastník	k1gMnPc4	účastník
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
tedy	tedy	k9	tedy
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
navštívit	navštívit	k5eAaPmF	navštívit
Rusko	Rusko	k1gNnSc4	Rusko
bez	bez	k7c2	bez
víza	vízo	k1gNnSc2	vízo
před	před	k7c4	před
a	a	k8xC	a
během	během	k7c2	během
soutěže	soutěž	k1gFnSc2	soutěž
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
státní	státní	k2eAgNnSc4d1	státní
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
víza	vízo	k1gNnSc2	vízo
jim	on	k3xPp3gMnPc3	on
bude	být	k5eAaImBp3nS	být
udělen	udělen	k2eAgInSc1d1	udělen
tzv.	tzv.	kA	tzv.
fanouškovský	fanouškovský	k2eAgInSc1d1	fanouškovský
identifikační	identifikační	k2eAgInSc1d1	identifikační
průkaz	průkaz	k1gInSc1	průkaz
–	–	k?	–
Fan	Fana	k1gFnPc2	Fana
ID	ido	k1gNnPc2	ido
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
prodeje	prodej	k1gInSc2	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
mezi	mezi	k7c7	mezi
14	[number]	k4	14
<g/>
.	.	kIx.	.
zářím	zářit	k5eAaImIp1nS	zářit
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
říjnem	říjen	k1gInSc7	říjen
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bude	být	k5eAaImBp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgNnSc1	první
fotbalové	fotbalový	k2eAgNnSc1d1	fotbalové
mistrovství	mistrovství	k1gNnSc1	mistrovství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bude	být	k5eAaImBp3nS	být
využívat	využívat	k5eAaPmF	využívat
systém	systém	k1gInSc1	systém
video	video	k1gNnSc4	video
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
(	(	kIx(	(
<g/>
VAR	var	k1gInSc1	var
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
vydala	vydat	k5eAaPmAgFnS	vydat
FIFA	FIFA	kA	FIFA
seznam	seznam	k1gInSc4	seznam
13	[number]	k4	13
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
pro	pro	k7c4	pro
video	video	k1gNnSc4	video
asistence	asistence	k1gFnSc2	asistence
<g/>
,	,	kIx,	,
devět	devět	k4xCc1	devět
jich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
Kataru	katar	k1gInSc2	katar
<g/>
.	.	kIx.	.
<g/>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
píseň	píseň	k1gFnSc1	píseň
turnaje	turnaj	k1gInSc2	turnaj
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Live	Live	k1gFnSc1	Live
It	It	k1gFnSc1	It
Up	Up	k1gFnSc1	Up
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
vokály	vokál	k1gInPc7	vokál
od	od	k7c2	od
Willa	Willo	k1gNnSc2	Willo
Smithe	Smith	k1gFnSc2	Smith
<g/>
,	,	kIx,	,
Nicky	nicka	k1gFnSc2	nicka
Jam	jáma	k1gFnPc2	jáma
a	a	k8xC	a
Era	Era	k1gFnPc2	Era
Istrefi	Istref	k1gFnSc2	Istref
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
hudební	hudební	k2eAgNnSc1d1	hudební
video	video	k1gNnSc1	video
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
FIFA	FIFA	kA	FIFA
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2018	[number]	k4	2018
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
velké	velký	k2eAgNnSc1d1	velké
rozšíření	rozšíření	k1gNnSc1	rozšíření
pro	pro	k7c4	pro
videohru	videohra	k1gFnSc4	videohra
FIFA	FIFA	kA	FIFA
18	[number]	k4	18
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
MS	MS	kA	MS
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
expanze	expanze	k1gFnSc1	expanze
je	být	k5eAaImIp3nS	být
bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
všech	všecek	k3xTgInPc2	všecek
32	[number]	k4	32
kvalifikovaných	kvalifikovaný	k2eAgInPc2d1	kvalifikovaný
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
využito	využít	k5eAaPmNgNnS	využít
je	být	k5eAaImIp3nS	být
také	také	k9	také
všech	všecek	k3xTgInPc2	všecek
12	[number]	k4	12
stadionů	stadion	k1gInPc2	stadion
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
<g/>
Držitelé	držitel	k1gMnPc1	držitel
fanouškovského	fanouškovský	k2eAgInSc2d1	fanouškovský
průkazu	průkaz	k1gInSc2	průkaz
mohou	moct	k5eAaImIp3nP	moct
získat	získat	k5eAaPmF	získat
jízdu	jízda	k1gFnSc4	jízda
zdarma	zdarma	k6eAd1	zdarma
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
veřejné	veřejný	k2eAgFnSc6d1	veřejná
dopravě	doprava	k1gFnSc6	doprava
ve	v	k7c6	v
dnech	den	k1gInPc6	den
zápasu	zápas	k1gInSc2	zápas
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
vybraných	vybraný	k2eAgInPc6d1	vybraný
vlacích	vlak	k1gInPc6	vlak
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
francouzském	francouzský	k2eAgNnSc6d1	francouzské
vítězství	vítězství	k1gNnSc6	vítězství
poháru	pohár	k1gInSc6	pohár
přišlo	přijít	k5eAaPmAgNnS	přijít
pod	pod	k7c4	pod
Eiffelovu	Eiffelův	k2eAgFnSc4d1	Eiffelova
věž	věž	k1gFnSc4	věž
slavit	slavit	k5eAaImF	slavit
na	na	k7c4	na
90	[number]	k4	90
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
stovky	stovka	k1gFnPc1	stovka
tisíc	tisíc	k4xCgInSc1	tisíc
fanoušků	fanoušek	k1gMnPc2	fanoušek
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
Champs-Elysées	Champs-Elysées	k1gInSc4	Champs-Elysées
<g/>
.	.	kIx.	.
</s>
<s>
Pařížské	pařížský	k2eAgNnSc1d1	pařížské
metro	metro	k1gNnSc1	metro
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
vítězství	vítězství	k1gNnSc2	vítězství
Francie	Francie	k1gFnSc2	Francie
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
dočasně	dočasně	k6eAd1	dočasně
přejmenovalo	přejmenovat	k5eAaPmAgNnS	přejmenovat
šest	šest	k4xCc1	šest
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
pomocí	pomoc	k1gFnSc7	pomoc
slovních	slovní	k2eAgFnPc2d1	slovní
hříček	hříčka	k1gFnPc2	hříčka
přejmenovány	přejmenován	k2eAgFnPc4d1	přejmenována
po	po	k7c6	po
francouzském	francouzský	k2eAgInSc6d1	francouzský
trenérovi	trenér	k1gMnSc3	trenér
Didierovi	Didier	k1gMnSc3	Didier
Deschampsovi	Deschamps	k1gMnSc3	Deschamps
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Champs-Élysées	Champs-Élyséesa	k1gFnPc2	Champs-Élyséesa
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
na	na	k7c4	na
Deschamps-Élysées	Deschamps-Élysées	k1gMnSc1	Deschamps-Élysées
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
po	po	k7c6	po
brankáři	brankář	k1gMnSc3	brankář
a	a	k8xC	a
kapitánovi	kapitán	k1gMnSc3	kapitán
mužstva	mužstvo	k1gNnSc2	mužstvo
Hugovi	Hugo	k1gMnSc3	Hugo
Llorisovi	Lloris	k1gMnSc3	Lloris
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Gaulle	Gaulle	k1gInSc1	Gaulle
-	-	kIx~	-
Étoiles	Étoiles	k1gInSc1	Étoiles
má	mít	k5eAaImIp3nS	mít
dočasný	dočasný	k2eAgInSc4d1	dočasný
název	název	k1gInSc4	název
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
2	[number]	k4	2
Étoiles	Étoilesa	k1gFnPc2	Étoilesa
(	(	kIx(	(
<g/>
Máme	mít	k5eAaImIp1nP	mít
dvě	dva	k4xCgFnPc1	dva
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyzvedává	vyzvedávat	k5eAaImIp3nS	vyzvedávat
druhý	druhý	k4xOgInSc4	druhý
titul	titul	k1gInSc4	titul
mistrů	mistr	k1gMnPc2	mistr
světa	svět	k1gInSc2	svět
pro	pro	k7c4	pro
francouzský	francouzský	k2eAgInSc4d1	francouzský
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sponzoři	sponzor	k1gMnPc5	sponzor
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
sponzory	sponzor	k1gMnPc7	sponzor
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2018	[number]	k4	2018
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Skupiny	skupina	k1gFnPc1	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgFnPc1d1	základní
skupiny	skupina	k1gFnPc1	skupina
MS	MS	kA	MS
2018	[number]	k4	2018
(	(	kIx(	(
<g/>
kapitáni	kapitán	k1gMnPc1	kapitán
a	a	k8xC	a
trenéři	trenér	k1gMnPc1	trenér
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Kritéria	kritérion	k1gNnSc2	kritérion
pořadí	pořadí	k1gNnSc2	pořadí
===	===	k?	===
</s>
</p>
<p>
<s>
Kritéria	kritérion	k1gNnPc1	kritérion
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
pořadí	pořadí	k1gNnSc2	pořadí
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc1d1	následující
(	(	kIx(	(
<g/>
článek	článek	k1gInSc1	článek
32.5	[number]	k4	32.5
předpisů	předpis	k1gInPc2	předpis
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
zápasů	zápas	k1gInPc2	zápas
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
rozdíl	rozdíl	k1gInSc4	rozdíl
branek	branka	k1gFnPc2	branka
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
zápasů	zápas	k1gInPc2	zápas
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
všech	všecek	k3xTgInPc2	všecek
zápasů	zápas	k1gInPc2	zápas
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
<g/>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
si	se	k3xPyFc3	se
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
týmů	tým	k1gInPc2	tým
rovny	roven	k2eAgFnPc1d1	rovna
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgInPc2	tři
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
určují	určovat	k5eAaImIp3nP	určovat
jejich	jejich	k3xOp3gNnSc4	jejich
pořadí	pořadí	k1gNnSc4	pořadí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
ze	z	k7c2	z
zápasů	zápas	k1gInPc2	zápas
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
týmy	tým	k1gInPc7	tým
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
rozdíl	rozdíl	k1gInSc4	rozdíl
branek	branka	k1gFnPc2	branka
ze	z	k7c2	z
zápasů	zápas	k1gInPc2	zápas
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
týmy	tým	k1gInPc7	tým
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
počet	počet	k1gInSc1	počet
vstřelených	vstřelený	k2eAgFnPc2d1	vstřelená
branek	branka	k1gFnPc2	branka
ze	z	k7c2	z
zápasů	zápas	k1gInPc2	zápas
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
týmy	tým	k1gInPc7	tým
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
body	bod	k1gInPc1	bod
za	za	k7c4	za
fair	fair	k6eAd1	fair
play	play	k0	play
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
první	první	k4xOgFnSc1	první
žlutá	žlutý	k2eAgFnSc1d1	žlutá
karta	karta	k1gFnSc1	karta
<g/>
:	:	kIx,	:
minus	minus	k6eAd1	minus
1	[number]	k4	1
bod	bod	k1gInSc4	bod
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
nepřímo	přímo	k6eNd1	přímo
udělená	udělený	k2eAgFnSc1d1	udělená
červená	červený	k2eAgFnSc1d1	červená
karta	karta	k1gFnSc1	karta
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
druhá	druhý	k4xOgFnSc1	druhý
žlutá	žlutý	k2eAgFnSc1d1	žlutá
karta	karta	k1gFnSc1	karta
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
minus	minus	k6eAd1	minus
3	[number]	k4	3
body	bod	k1gInPc4	bod
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
přímo	přímo	k6eAd1	přímo
udělená	udělený	k2eAgFnSc1d1	udělená
červená	červený	k2eAgFnSc1d1	červená
karta	karta	k1gFnSc1	karta
<g/>
:	:	kIx,	:
minus	minus	k6eAd1	minus
4	[number]	k4	4
body	bod	k1gInPc4	bod
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
karta	karta	k1gFnSc1	karta
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
udělená	udělený	k2eAgFnSc1d1	udělená
červená	červený	k2eAgFnSc1d1	červená
karta	karta	k1gFnSc1	karta
<g/>
:	:	kIx,	:
minus	minus	k1gInSc1	minus
5	[number]	k4	5
bodů	bod	k1gInPc2	bod
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
zápas	zápas	k1gInSc4	zápas
bude	být	k5eAaImBp3nS	být
za	za	k7c2	za
téhož	týž	k3xTgMnSc2	týž
hráče	hráč	k1gMnSc2	hráč
proveden	provést	k5eAaPmNgInS	provést
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
odečet	odečet	k1gInSc1	odečet
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
losování	losování	k1gNnSc6	losování
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
C	C	kA	C
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
D	D	kA	D
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
E	E	kA	E
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
F	F	kA	F
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
G	G	kA	G
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
H	H	kA	H
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tabulka	tabulka	k1gFnSc1	tabulka
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zápasy	zápas	k1gInPc1	zápas
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Vyřazovací	vyřazovací	k2eAgFnSc1d1	vyřazovací
část	část	k1gFnSc1	část
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vyřazovací	vyřazovací	k2eAgInSc1d1	vyřazovací
strom	strom	k1gInSc1	strom
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Osmifinále	osmifinále	k1gNnPc3	osmifinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Semifinále	semifinále	k1gNnPc3	semifinále
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Tabulka	tabulka	k1gFnSc1	tabulka
střelců	střelec	k1gMnPc2	střelec
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Střelci	Střelec	k1gMnPc5	Střelec
===	===	k?	===
</s>
</p>
<p>
<s>
6	[number]	k4	6
gólů	gól	k1gInPc2	gól
</s>
</p>
<p>
<s>
4	[number]	k4	4
góly	gól	k1gInPc4	gól
</s>
</p>
<p>
<s>
3	[number]	k4	3
góly	gól	k1gInPc4	gól
</s>
</p>
<p>
<s>
2	[number]	k4	2
góly	gól	k1gInPc4	gól
</s>
</p>
<p>
<s>
1	[number]	k4	1
gól	gól	k1gInSc1	gól
</s>
</p>
<p>
<s>
vlastní	vlastní	k2eAgInPc4d1	vlastní
góly	gól	k1gInPc4	gól
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
kontroverze	kontroverze	k1gFnSc1	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2014	[number]	k4	2014
přišli	přijít	k5eAaPmAgMnP	přijít
někteří	některý	k3yIgMnPc1	některý
němečtí	německý	k2eAgMnPc1d1	německý
politici	politik	k1gMnPc1	politik
(	(	kIx(	(
<g/>
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1	Křesťanskodemokratická
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
Zelení	Zelený	k1gMnPc1	Zelený
<g/>
)	)	kIx)	)
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
otázka	otázka	k1gFnSc1	otázka
pořadatelské	pořadatelský	k2eAgFnSc2d1	pořadatelská
země	zem	k1gFnSc2	zem
znovu	znovu	k6eAd1	znovu
otevřena	otevřen	k2eAgFnSc1d1	otevřena
<g/>
.	.	kIx.	.
</s>
<s>
Odebrání	odebrání	k1gNnSc1	odebrání
pořadatelství	pořadatelství	k1gNnSc2	pořadatelství
Rusku	Rusko	k1gNnSc6	Rusko
mělo	mít	k5eAaImAgNnS	mít
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
být	být	k5eAaImF	být
účinnou	účinný	k2eAgFnSc7d1	účinná
sankcí	sankce	k1gFnSc7	sankce
za	za	k7c4	za
anexi	anexe	k1gFnSc4	anexe
Krymu	Krym	k1gInSc2	Krym
Ruskou	ruský	k2eAgFnSc7d1	ruská
federací	federace	k1gFnSc7	federace
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
separatistů	separatista	k1gMnPc2	separatista
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
návrh	návrh	k1gInSc1	návrh
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c6	na
sestřelení	sestřelení	k1gNnSc6	sestřelení
letu	let	k1gInSc2	let
Malaysia	Malaysium	k1gNnSc2	Malaysium
Airlines	Airlines	k1gInSc4	Airlines
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
FIFA	FIFA	kA	FIFA
Sepp	Sepp	k1gMnSc1	Sepp
Blatter	Blatter	k1gMnSc1	Blatter
nato	nato	k6eAd1	nato
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
a	a	k8xC	a
odhlasováno	odhlasovat	k5eAaPmNgNnS	odhlasovat
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
my	my	k3xPp1nPc1	my
budeme	být	k5eAaImBp1nP	být
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
práci	práce	k1gFnSc6	práce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
oficiální	oficiální	k2eAgFnSc6d1	oficiální
návštěvě	návštěva	k1gFnSc6	návštěva
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
výbor	výbor	k1gInSc4	výbor
FIFA	FIFA	kA	FIFA
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
předsedou	předseda	k1gMnSc7	předseda
Chrisem	Chris	k1gInSc7	Chris
Ungerem	Unger	k1gInSc7	Unger
města	město	k1gNnSc2	město
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Soči	Soči	k1gNnSc1	Soči
a	a	k8xC	a
Kazaň	Kazaň	k1gFnSc1	Kazaň
a	a	k8xC	a
oba	dva	k4xCgInPc4	dva
stadiony	stadion	k1gInPc4	stadion
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnSc2	jejich
vyjádření	vyjádření	k1gNnSc2	vyjádření
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
pokrokem	pokrok	k1gInSc7	pokrok
příprav	příprava	k1gFnPc2	příprava
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
(	(	kIx(	(
<g/>
a	a	k8xC	a
současný	současný	k2eAgMnSc1d1	současný
<g/>
)	)	kIx)	)
prezident	prezident	k1gMnSc1	prezident
FIFA	FIFA	kA	FIFA
Gianni	Giann	k1gMnPc1	Giann
Infantino	Infantina	k1gFnSc5	Infantina
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
spokojen	spokojen	k2eAgMnSc1d1	spokojen
s	s	k7c7	s
pokrokem	pokrok	k1gInSc7	pokrok
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	tu	k6eAd1	tu
máme	mít	k5eAaImIp1nP	mít
skvělý	skvělý	k2eAgInSc4d1	skvělý
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
-	-	kIx~	-
nejen	nejen	k6eAd1	nejen
skvělý	skvělý	k2eAgInSc1d1	skvělý
světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Bezpečnostní	bezpečnostní	k2eAgMnPc1d1	bezpečnostní
experti	expert	k1gMnPc1	expert
varovali	varovat	k5eAaImAgMnP	varovat
před	před	k7c7	před
osamělými	osamělý	k2eAgMnPc7d1	osamělý
džihádisty	džihádista	k1gMnPc7	džihádista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
zaútočit	zaútočit	k5eAaPmF	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Teroristická	teroristický	k2eAgFnSc1d1	teroristická
skupina	skupina	k1gFnSc1	skupina
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
ISIS	Isis	k1gFnSc1	Isis
<g/>
)	)	kIx)	)
povzbuzovala	povzbuzovat	k5eAaImAgFnS	povzbuzovat
potenciální	potenciální	k2eAgMnPc4d1	potenciální
útočníky	útočník	k1gMnPc4	útočník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
během	během	k7c2	během
turnaje	turnaj	k1gInSc2	turnaj
udeřili	udeřit	k5eAaPmAgMnP	udeřit
<g/>
.	.	kIx.	.
</s>
<s>
Pro-ISIS	Pro-ISIS	k?	Pro-ISIS
server	server	k1gInSc1	server
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
obrázek	obrázek	k1gInSc1	obrázek
Lionela	Lionel	k1gMnSc2	Lionel
Messiho	Messi	k1gMnSc2	Messi
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
a	a	k8xC	a
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2018	[number]	k4	2018
dostal	dostat	k5eAaPmAgMnS	dostat
saúdskoarabský	saúdskoarabský	k2eAgMnSc1d1	saúdskoarabský
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
Fahad	Fahad	k1gInSc4	Fahad
Mirdásí	Mirdásí	k1gNnSc2	Mirdásí
kvůli	kvůli	k7c3	kvůli
korupci	korupce	k1gFnSc3	korupce
doživotní	doživotní	k2eAgInSc4d1	doživotní
zákaz	zákaz	k1gInSc4	zákaz
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
měl	mít	k5eAaImAgInS	mít
odcestovat	odcestovat	k5eAaPmF	odcestovat
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
asistenti	asistent	k1gMnPc1	asistent
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
byli	být	k5eAaImAgMnP	být
také	také	k9	také
vyškrtnuti	vyškrtnut	k2eAgMnPc1d1	vyškrtnut
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
pro	pro	k7c4	pro
mistrovství	mistrovství	k1gNnSc4	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
Náhradní	náhradní	k2eAgMnSc1d1	náhradní
hlavní	hlavní	k2eAgMnSc1d1	hlavní
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
nebyl	být	k5eNaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dva	dva	k4xCgMnPc1	dva
noví	nový	k2eAgMnPc1d1	nový
asistenti	asistent	k1gMnPc1	asistent
rozhodčího	rozhodčí	k1gMnSc4	rozhodčí
byli	být	k5eAaImAgMnP	být
nominováni	nominován	k2eAgMnPc1d1	nominován
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
<g/>
Jelikož	jelikož	k8xS	jelikož
FIFA	FIFA	kA	FIFA
politické	politický	k2eAgInPc4d1	politický
projevy	projev	k1gInPc4	projev
neschvaluje	schvalovat	k5eNaImIp3nS	schvalovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dva	dva	k4xCgMnPc1	dva
švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
hráči	hráč	k1gMnPc1	hráč
potrestáni	potrestat	k5eAaPmNgMnP	potrestat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
provokovali	provokovat	k5eAaImAgMnP	provokovat
soupeře	soupeř	k1gMnSc4	soupeř
při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
svých	svůj	k3xOyFgInPc2	svůj
gólů	gól	k1gInPc2	gól
gesty	gest	k1gInPc4	gest
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Kosova	Kosův	k2eAgFnSc1d1	Kosova
a	a	k8xC	a
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Dostali	dostat	k5eAaPmAgMnP	dostat
pokuty	pokuta	k1gFnPc4	pokuta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
225	[number]	k4	225
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Ruskem	Rusko	k1gNnSc7	Rusko
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
zpívali	zpívat	k5eAaImAgMnP	zpívat
někteří	některý	k3yIgMnPc1	některý
chorvatští	chorvatský	k2eAgMnPc1d1	chorvatský
fotbalisté	fotbalista	k1gMnPc1	fotbalista
v	v	k7c6	v
šatně	šatna	k1gFnSc6	šatna
nacionalistické	nacionalistický	k2eAgFnSc2d1	nacionalistická
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
písničky	písnička	k1gFnSc2	písnička
Bojna	Bojn	k1gInSc2	Bojn
Čavoglave	Čavoglav	k1gInSc5	Čavoglav
od	od	k7c2	od
Marko	Marko	k1gMnSc1	Marko
Perkoviće	Perković	k1gMnSc2	Perković
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
ustašovským	ustašovský	k2eAgInSc7d1	ustašovský
ultranacionálním	ultranacionální	k2eAgInSc7d1	ultranacionální
pozdravem	pozdrav	k1gInSc7	pozdrav
"	"	kIx"	"
<g/>
Za	za	k7c7	za
dom	dom	k?	dom
spremni	spremnit	k5eAaPmRp2nS	spremnit
<g/>
"	"	kIx"	"
z	z	k7c2	z
období	období	k1gNnSc2	období
existence	existence	k1gFnSc2	existence
Nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
v	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
prezidentka	prezidentka	k1gFnSc1	prezidentka
Kolinda	Kolinda	k1gFnSc1	Kolinda
Grabar-Kitarović	Grabar-Kitarović	k1gFnSc1	Grabar-Kitarović
sledovala	sledovat	k5eAaImAgFnS	sledovat
hru	hra	k1gFnSc4	hra
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
přímo	přímo	k6eAd1	přímo
vedle	vedle	k7c2	vedle
Gianni	Gianeň	k1gFnSc3	Gianeň
Infantiniho	Infantini	k1gMnSc2	Infantini
a	a	k8xC	a
Dmitrije	Dmitrije	k1gMnSc2	Dmitrije
Medveděva	Medveděv	k1gMnSc2	Medveděv
a	a	k8xC	a
po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
navštívila	navštívit	k5eAaPmAgFnS	navštívit
chorvatské	chorvatský	k2eAgInPc4d1	chorvatský
fotbalisty	fotbalista	k1gMnPc7	fotbalista
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
šatně	šatna	k1gFnSc6	šatna
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
obránce	obránce	k1gMnSc1	obránce
Domagoj	Domagoj	k1gInSc1	Domagoj
Vida	Vida	k?	Vida
a	a	k8xC	a
manažer	manažer	k1gMnSc1	manažer
reprezentace	reprezentace	k1gFnSc2	reprezentace
Ognjen	Ognjna	k1gFnPc2	Ognjna
Vukojevič	Vukojevič	k1gInSc1	Vukojevič
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
Ruskem	Rusko	k1gNnSc7	Rusko
oslavovali	oslavovat	k5eAaImAgMnP	oslavovat
na	na	k7c6	na
videu	video	k1gNnSc6	video
pokřikem	pokřik	k1gInSc7	pokřik
"	"	kIx"	"
<g/>
Sláva	Sláva	k1gFnSc1	Sláva
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
"	"	kIx"	"
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
hoří	hoře	k1gNnPc2	hoře
<g/>
"	"	kIx"	"
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
Vida	Vida	k?	Vida
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
FIFA	FIFA	kA	FIFA
napomenutí	napomenutí	k1gNnSc2	napomenutí
<g/>
.	.	kIx.	.
</s>
<s>
Manažeru	manažer	k1gMnSc3	manažer
Vukojevičovi	Vukojevič	k1gMnSc3	Vukojevič
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
pokuta	pokuta	k1gFnSc1	pokuta
330	[number]	k4	330
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
a	a	k8xC	a
Chorvatský	chorvatský	k2eAgInSc4d1	chorvatský
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
svaz	svaz	k1gInSc4	svaz
ho	on	k3xPp3gInSc4	on
propustil	propustit	k5eAaPmAgInS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Vida	Vida	k?	Vida
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
jenom	jenom	k9	jenom
o	o	k7c4	o
vtip	vtip	k1gInSc4	vtip
a	a	k8xC	a
"	"	kIx"	"
<g/>
ruské	ruský	k2eAgMnPc4d1	ruský
lidi	člověk	k1gMnPc4	člověk
má	mít	k5eAaImIp3nS	mít
rád	rád	k6eAd1	rád
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Anglií	Anglie	k1gFnSc7	Anglie
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
Vida	Vida	k?	Vida
rozhovor	rozhovor	k1gInSc1	rozhovor
ruské	ruský	k2eAgInPc1d1	ruský
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
Russia	Russia	k1gFnSc1	Russia
24	[number]	k4	24
a	a	k8xC	a
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
slova	slovo	k1gNnPc4	slovo
se	se	k3xPyFc4	se
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
<g/>
Vláda	vláda	k1gFnSc1	vláda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
britská	britský	k2eAgNnPc4d1	Britské
média	médium	k1gNnPc4	médium
varovaly	varovat	k5eAaImAgFnP	varovat
anglické	anglický	k2eAgMnPc4d1	anglický
fanoušky	fanoušek	k1gMnPc4	fanoušek
před	před	k7c7	před
potenciálními	potenciální	k2eAgFnPc7d1	potenciální
protibritskými	protibritský	k2eAgFnPc7d1	protibritská
náladami	nálada	k1gFnPc7	nálada
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
anglické	anglický	k2eAgFnSc2d1	anglická
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
Volgogradu	Volgograd	k1gInSc2	Volgograd
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
Tuniskem	Tunisko	k1gNnSc7	Tunisko
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
dle	dle	k7c2	dle
svých	svůj	k3xOyFgInPc2	svůj
slov	slovo	k1gNnPc2	slovo
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
přátelským	přátelský	k2eAgNnSc7d1	přátelské
přijetím	přijetí	k1gNnSc7	přijetí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Rusů	Rus	k1gMnPc2	Rus
a	a	k8xC	a
dobrou	dobrý	k2eAgFnSc7d1	dobrá
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
panuje	panovat	k5eAaImIp3nS	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Negativní	negativní	k2eAgNnSc4d1	negativní
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
o	o	k7c6	o
Rusku	Rusko	k1gNnSc6	Rusko
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
malou	malý	k2eAgFnSc4d1	malá
účast	účast	k1gFnSc4	účast
anglických	anglický	k2eAgMnPc2d1	anglický
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
Volgogradu	Volgograd	k1gInSc2	Volgograd
přečísleni	přečíslen	k2eAgMnPc1d1	přečíslen
diváky	divák	k1gMnPc7	divák
z	z	k7c2	z
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
polská	polský	k2eAgNnPc1d1	polské
média	médium	k1gNnPc1	médium
informovala	informovat	k5eAaBmAgNnP	informovat
o	o	k7c6	o
přátelském	přátelský	k2eAgNnSc6d1	přátelské
přijetí	přijetí	k1gNnSc6	přijetí
polských	polský	k2eAgMnPc2d1	polský
fanoušků	fanoušek	k1gMnPc2	fanoušek
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
nenaplnily	naplnit	k5eNaPmAgFnP	naplnit
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
opakovat	opakovat	k5eAaImF	opakovat
násilnosti	násilnost	k1gFnPc4	násilnost
mezi	mezi	k7c7	mezi
polskými	polský	k2eAgMnPc7d1	polský
a	a	k8xC	a
ruskými	ruský	k2eAgMnPc7d1	ruský
fanoušky	fanoušek	k1gMnPc7	fanoušek
z	z	k7c2	z
fotbalového	fotbalový	k2eAgNnSc2d1	fotbalové
utkání	utkání	k1gNnSc2	utkání
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012.13	[number]	k4	2012.13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
prezident	prezident	k1gMnSc1	prezident
FIFA	FIFA	kA	FIFA
Gianni	Giann	k1gMnPc1	Giann
Infantino	Infantina	k1gFnSc5	Infantina
označil	označit	k5eAaPmAgMnS	označit
šampionát	šampionát	k1gInSc4	šampionát
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
za	za	k7c4	za
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizi	televize	k1gFnSc6	televize
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
ho	on	k3xPp3gMnSc4	on
sledovaly	sledovat	k5eAaImAgInP	sledovat
tři	tři	k4xCgNnPc4	tři
miliardy	miliarda	k4xCgFnPc1	miliarda
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
stadiony	stadion	k1gInPc1	stadion
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
98	[number]	k4	98
%	%	kIx~	%
vyprodány	vyprodat	k5eAaPmNgFnP	vyprodat
a	a	k8xC	a
hostitelská	hostitelský	k2eAgFnSc1d1	hostitelská
města	město	k1gNnSc2	město
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
sedm	sedm	k4xCc1	sedm
milionů	milion	k4xCgInPc2	milion
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zahajovací	zahajovací	k2eAgFnSc1d1	zahajovací
ceremonie	ceremonie	k1gFnSc1	ceremonie
==	==	k?	==
</s>
</p>
<p>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
zahájení	zahájení	k1gNnSc1	zahájení
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Lužniki	Lužnik	k1gFnSc2	Lužnik
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Předcházelo	předcházet	k5eAaImAgNnS	předcházet
zahajovacímu	zahajovací	k2eAgInSc3d1	zahajovací
zápasu	zápas	k1gInSc3	zápas
turnaje	turnaj	k1gInSc2	turnaj
mezi	mezi	k7c7	mezi
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
představil	představit	k5eAaPmAgInS	představit
Ronaldo	Ronaldo	k1gNnSc4	Ronaldo
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
hráč	hráč	k1gMnSc1	hráč
brazilského	brazilský	k2eAgInSc2d1	brazilský
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
doprovázený	doprovázený	k2eAgMnSc1d1	doprovázený
dítětem	dítě	k1gNnSc7	dítě
v	v	k7c6	v
tričku	tričko	k1gNnSc6	tričko
s	s	k7c7	s
logem	log	k1gInSc7	log
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
anglický	anglický	k2eAgMnSc1d1	anglický
popový	popový	k2eAgMnSc1d1	popový
zpěvák	zpěvák	k1gMnSc1	zpěvák
Robbie	Robbie	k1gFnSc2	Robbie
Williams	Williams	k1gInSc1	Williams
a	a	k8xC	a
ruská	ruský	k2eAgFnSc1d1	ruská
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
<g/>
,	,	kIx,	,
sopranistka	sopranistka	k1gFnSc1	sopranistka
Aida	Aida	k1gFnSc1	Aida
Garifullinová	Garifullinový	k2eAgFnSc1d1	Garifullinový
<g/>
,	,	kIx,	,
zazpívali	zazpívat	k5eAaPmAgMnP	zazpívat
společně	společně	k6eAd1	společně
píseň	píseň	k1gFnSc4	píseň
Angels	Angelsa	k1gFnPc2	Angelsa
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
další	další	k2eAgMnPc1d1	další
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
oblečení	oblečený	k2eAgMnPc1d1	oblečený
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
všech	všecek	k3xTgInPc2	všecek
32	[number]	k4	32
týmů	tým	k1gInPc2	tým
a	a	k8xC	a
nesoucí	nesoucí	k2eAgNnSc4d1	nesoucí
označení	označení	k1gNnSc4	označení
každého	každý	k3xTgInSc2	každý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ronaldo	Ronaldo	k1gNnSc1	Ronaldo
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vrátil	vrátit	k5eAaPmAgMnS	vrátit
s	s	k7c7	s
oficiálním	oficiální	k2eAgInSc7d1	oficiální
míčem	míč	k1gInSc7	míč
zápasu	zápas	k1gInSc2	zápas
mistrovství	mistrovství	k1gNnSc2	mistrovství
(	(	kIx(	(
<g/>
Adidas	Adidas	k1gInSc1	Adidas
Telstar	Telstar	k1gInSc1	Telstar
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
začátkem	začátek	k1gInSc7	začátek
června	červen	k1gInSc2	červen
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2018	[number]	k4	2018
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
-	-	kIx~	-
FIFA	FIFA	kA	FIFA
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
2018	[number]	k4	2018
</s>
</p>
<p>
<s>
Welcome	Welcom	k1gInSc5	Welcom
<g/>
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
–	–	k?	–
turistický	turistický	k2eAgInSc1d1	turistický
portál	portál	k1gInSc1	portál
mistrovství	mistrovství	k1gNnSc2	mistrovství
</s>
</p>
<p>
<s>
Live	Live	k6eAd1	Live
It	It	k1gMnSc1	It
Up	Up	k1gFnSc2	Up
–	–	k?	–
oficiální	oficiální	k2eAgInSc1d1	oficiální
song	song	k1gInSc1	song
od	od	k7c2	od
Nicky	nicka	k1gFnSc2	nicka
Jam	jam	k1gInSc1	jam
<g/>
,	,	kIx,	,
Will	Will	k1gMnSc1	Will
Smith	Smith	k1gMnSc1	Smith
&	&	k?	&
Era	Era	k1gMnSc1	Era
Istrefi	Istref	k1gFnSc2	Istref
</s>
</p>
