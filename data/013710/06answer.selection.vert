<s>
Polotěžká	polotěžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
(	(	kIx(
<g/>
vzorec	vzorec	k1gInSc1
HDO	HDO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
izotopolog	izotopolog	k1gInSc1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
vodíku	vodík	k1gInSc2
nahrazen	nahradit	k5eAaPmNgInS
deuteriem	deuterium	k1gNnSc7
(	(	kIx(
<g/>
tedy	tedy	k8xC
vodíkem-	vodík	k1gInSc7
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
