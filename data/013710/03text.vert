<s>
Polotěžká	polotěžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
těžká	těžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
izotopolog	izotopolog	k1gMnSc1
vody	voda	k1gFnSc2
a	a	k8xC
polotěžké	polotěžký	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Polotěžká	polotěžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
O-	O-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
voda	voda	k1gFnSc1
</s>
<s>
Triviální	triviální	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
polotěžká	polotěžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
</s>
<s>
Latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
semi-grave	semi-gravat	k5eAaPmIp3nS
aqua	aqua	k6eAd1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Deuterium	deuterium	k1gNnSc1
hydrogen	hydrogen	k1gInSc1
monoxide	monoxid	k1gInSc5
</s>
<s>
Německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
halbschweres	halbschweres	k1gMnSc1
Wasser	Wasser	k1gMnSc1
</s>
<s>
Funkční	funkční	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
HDO	HDO	kA
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
HDO	HDO	kA
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
velmi	velmi	k6eAd1
slabá	slabý	k2eAgFnSc1d1
modrá	modrý	k2eAgFnSc1d1
(	(	kIx(
<g/>
totožný	totožný	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
jako	jako	k8xS,k8xC
voda	voda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
14940-63-7	14940-63-7	k4
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
139859	#num#	k4
</s>
<s>
ChEBI	ChEBI	k?
</s>
<s>
CHEBI	CHEBI	kA
<g/>
:	:	kIx,
<g/>
33806	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
]	]	kIx)
<g/>
O	o	k7c6
</s>
<s>
InChI	InChI	k?
</s>
<s>
1	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
1	#num#	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
i	i	k8xC
<g/>
/	/	kIx~
<g/>
hD	hD	k?
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
19,016	19,016	k4
<g/>
841	#num#	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
2,04	2,04	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
100,7	100,7	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
1,054	1,054	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Viskozita	viskozita	k1gFnSc1
</s>
<s>
1,124	1,124	k4
<g/>
8	#num#	k4
mPa	mPa	k?
<g/>
·	·	k?
<g/>
s	s	k7c7
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
1,329	1,329	k4
</s>
<s>
Povrchové	povrchový	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
</s>
<s>
0,07193	0,07193	k4
N	N	kA
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
</s>
<s>
0,0003125	0,0003125	k4
%	%	kIx~
vody	voda	k1gFnSc2
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Tvar	tvar	k1gInSc1
molekuly	molekula	k1gFnSc2
</s>
<s>
lomená	lomený	k2eAgFnSc1d1
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Entalpie	entalpie	k1gFnSc1
tání	tání	k1gNnSc3
Δ	Δ	k5eAaPmF
</s>
<s>
6	#num#	k4
227	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Entalpie	entalpie	k1gFnSc1
varu	var	k1gInSc2
Δ	Δ	k1gFnPc2
</s>
<s>
40	#num#	k4
700	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Polotěžká	polotěžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
(	(	kIx(
<g/>
vzorec	vzorec	k1gInSc1
HDO	HDO	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
izotopolog	izotopolog	k1gInSc1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
vodíku	vodík	k1gInSc2
nahrazen	nahradit	k5eAaPmNgInS
deuteriem	deuterium	k1gNnSc7
(	(	kIx(
<g/>
tedy	tedy	k8xC
vodíkem-	vodík	k1gInSc7
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
sloučeninu	sloučenina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
se	se	k3xPyFc4
přirozeně	přirozeně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
přírodě	příroda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Izotopology	Izotopolog	k1gMnPc4
</s>
<s>
Základní	základní	k2eAgFnSc7d1
látkou	látka	k1gFnSc7
<g/>
,	,	kIx,
od	od	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
je	být	k5eAaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
izotopologů	izotopolog	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
voda	voda	k1gFnSc1
se	s	k7c7
vzorcem	vzorec	k1gInSc7
H2O	H2O	k1gMnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
nazývána	nazýván	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
lehká	lehký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhradou	náhrada	k1gFnSc7
jednoho	jeden	k4xCgInSc2
z	z	k7c2
vodíku	vodík	k1gInSc2
deuteriem	deuterium	k1gNnSc7
vzniká	vznikat	k5eAaImIp3nS
polotěžká	polotěžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
HDO	HDO	kA
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
náhrady	náhrada	k1gFnSc2
obou	dva	k4xCgMnPc2
vodíků	vodík	k1gInPc2
těžká	těžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
D	D	kA
<g/>
2	#num#	k4
<g/>
O.	O.	kA
Nahrazením	nahrazení	k1gNnPc3
vodíků	vodík	k1gInPc2
tritiem	tritium	k1gNnSc7
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
vzniku	vznik	k1gInSc3
tritové	tritový	k2eAgFnSc2d1
vody	voda	k1gFnSc2
T2O	T2O	k1gFnSc2
(	(	kIx(
<g/>
případně	případně	k6eAd1
HTO	HTO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
ještě	ještě	k9
sloučenina	sloučenina	k1gFnSc1
DTO	DTO	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zatím	zatím	k6eAd1
není	být	k5eNaImIp3nS
česky	česky	k6eAd1
pojmenována	pojmenován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
dalších	další	k2eAgMnPc2d1
izotopologů	izotopolog	k1gMnPc2
vzniká	vznikat	k5eAaImIp3nS
v	v	k7c6
případě	případ	k1gInSc6
náhrady	náhrada	k1gFnSc2
kyslíku-	kyslíku-	k?
<g/>
16	#num#	k4
za	za	k7c4
kyslík-	kyslík-	k?
<g/>
17	#num#	k4
nebo	nebo	k8xC
18	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
rovněž	rovněž	k9
nemají	mít	k5eNaImIp3nP
česká	český	k2eAgNnPc1d1
pojmenování	pojmenování	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výskyt	výskyt	k1gInSc1
v	v	k7c6
přírodě	příroda	k1gFnSc6
</s>
<s>
Polotěžká	polotěžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
běžně	běžně	k6eAd1
v	v	k7c6
přírodě	příroda	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
díky	díky	k7c3
přirozenému	přirozený	k2eAgInSc3d1
výskytu	výskyt	k1gInSc3
deuteria	deuterium	k1gNnSc2
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
izotopu	izotop	k1gInSc2
vodíku	vodík	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vodík	vodík	k1gInSc1
v	v	k7c6
molekule	molekula	k1gFnSc6
vody	voda	k1gFnSc2
H2O	H2O	k1gFnSc2
nahrazuje	nahrazovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
molekula	molekula	k1gFnSc1
polotěžké	polotěžký	k2eAgFnSc2d1
vody	voda	k1gFnSc2
tak	tak	k6eAd1
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
3200	#num#	k4
molekul	molekula	k1gFnPc2
lehké	lehký	k2eAgFnSc2d1
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
tedy	tedy	k9
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tropech	trop	k1gInPc6
je	být	k5eAaImIp3nS
však	však	k9
poměr	poměr	k1gInSc1
nižší	nízký	k2eAgInSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Polotěžká	polotěžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
i	i	k9
na	na	k7c6
Marsu	Mars	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Polotěžkou	polotěžký	k2eAgFnSc4d1
vodu	voda	k1gFnSc4
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k9
těžkou	těžký	k2eAgFnSc4d1
voda	voda	k1gFnSc1
<g/>
)	)	kIx)
lze	lze	k6eAd1
vyrábět	vyrábět	k5eAaImF
elektrolýzou	elektrolýza	k1gFnSc7
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
vazba	vazba	k1gFnSc1
deuterium-kyslík	deuterium-kyslík	k1gInSc1
je	být	k5eAaImIp3nS
silnější	silný	k2eAgMnSc1d2
než	než	k8xS
vodík-kyslík	vodík-kyslík	k1gInSc1
a	a	k8xC
elektrolýzou	elektrolýza	k1gFnSc7
tak	tak	k6eAd1
dochází	docházet	k5eAaImIp3nS
nejdříve	dříve	k6eAd3
k	k	k7c3
rozrušení	rozrušení	k1gNnSc3
vazeb	vazba	k1gFnPc2
v	v	k7c6
H	H	kA
<g/>
2	#num#	k4
<g/>
O.	O.	kA
Vzniká	vznikat	k5eAaImIp3nS
tak	tak	k6eAd1
stále	stále	k6eAd1
koncentrovanější	koncentrovaný	k2eAgInSc4d2
roztok	roztok	k1gInSc4
D2O	D2O	k1gFnSc2
a	a	k8xC
HDO	HDO	kA
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Polotěžká	polotěžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
analýze	analýza	k1gFnSc3
cirkulace	cirkulace	k1gFnSc2
vody	voda	k1gFnSc2
v	v	k7c6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
molekuly	molekula	k1gFnPc1
jsou	být	k5eAaImIp3nP
díky	díky	k7c3
deuteriu	deuterium	k1gNnSc3
lehce	lehko	k6eAd1
identifikovatelné	identifikovatelný	k2eAgInPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
taktéž	taktéž	k?
zkoumána	zkoumat	k5eAaImNgFnS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
potenciálního	potenciální	k2eAgNnSc2d1
využití	využití	k1gNnSc2
jako	jako	k8xC,k8xS
chladícího	chladící	k2eAgNnSc2d1
média	médium	k1gNnSc2
v	v	k7c6
jaderných	jaderný	k2eAgFnPc6d1
elektrárnách	elektrárna	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Semiheavy	Semiheava	k1gFnSc2
water	watra	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Heavy	Heava	k1gFnSc2
water	watero	k1gNnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Comparison	Comparison	k1gInSc1
of	of	k?
semi-heavy	semi-heava	k1gFnSc2
water	water	k1gMnSc1
and	and	k?
H2O	H2O	k1gMnSc1
as	as	k1gNnSc2
coolant	coolant	k1gMnSc1
for	forum	k1gNnPc2
a	a	k8xC
conceptual	conceptual	k1gMnSc1
research	research	k1gMnSc1
reactor	reactor	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
view	view	k?
point	pointa	k1gFnPc2
of	of	k?
neutronic	neutronice	k1gFnPc2
parameters	parameters	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Progress	Progress	k1gInSc1
in	in	k?
Nuclear	Nuclear	k1gInSc1
Energy	Energ	k1gInPc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
118	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
103126	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
149	#num#	k4
<g/>
-	-	kIx~
<g/>
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
pnucene	pnucen	k1gInSc5
<g/>
.2019	.2019	k4
<g/>
.103126	.103126	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PITTER	PITTER	kA
<g/>
,	,	kIx,
PAVEL	Pavel	k1gMnSc1
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
-	-	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hydrochemie	Hydrochemie	k1gFnSc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
aktualiz	aktualiz	k1gMnSc1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vydavatelství	vydavatelství	k1gNnSc1
VŠCHT	VŠCHT	kA
Praha	Praha	k1gFnSc1
viii	vii	k1gFnSc2
<g/>
,	,	kIx,
579	#num#	k4
s.	s.	k?
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7080	#num#	k4
<g/>
-	-	kIx~
<g/>
701	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7080	#num#	k4
<g/>
-	-	kIx~
<g/>
701	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
880867964	#num#	k4
↑	↑	k?
WEBMASTER@TYDEN.CZ	WEBMASTER@TYDEN.CZ	k1gMnSc1
<g/>
,	,	kIx,
TYDEN	TYDEN	k?
<g/>
,	,	kIx,
www	www	k?
tyden	tyden	k?
cz	cz	k?
<g/>
,	,	kIx,
e-mail	e-mail	k1gInSc1
<g/>
:	:	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžká	těžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
pomáhá	pomáhat	k5eAaImIp3nS
zpřesňovat	zpřesňovat	k5eAaImF
klimatické	klimatický	k2eAgInPc4d1
modely	model	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TÝDEN	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-09-11	2009-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Vesmírný	vesmírný	k2eAgInSc4d1
dalekohled	dalekohled	k1gInSc4
Jamese	Jamese	k1gFnSc2
Webba	Webba	k1gFnSc1
bude	být	k5eAaImBp3nS
zkoumat	zkoumat	k5eAaImF
i	i	k9
Mars	Mars	k1gInSc1
<g/>
.	.	kIx.
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
zahraniční	zahraniční	k2eAgFnSc4d1
zajímavost	zajímavost	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-02-26	2018-02-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WEBMASTER@TYDEN.CZ	WEBMASTER@TYDEN.CZ	k1gMnSc1
<g/>
,	,	kIx,
TYDEN	TYDEN	k?
<g/>
,	,	kIx,
www	www	k?
tyden	tyden	k?
cz	cz	k?
<g/>
,	,	kIx,
e-mail	e-mail	k1gInSc1
<g/>
:	:	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžká	těžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
pomáhá	pomáhat	k5eAaImIp3nS
zpřesňovat	zpřesňovat	k5eAaImF
klimatické	klimatický	k2eAgInPc4d1
modely	model	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TÝDEN	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-09-11	2009-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TASHAKOR	TASHAKOR	kA
<g/>
,	,	kIx,
S.	S.	kA
NEUTRONIC	NEUTRONIC	kA
INVESTIGATION	INVESTIGATION	kA
OF	OF	kA
SEMI-HEAVY	SEMI-HEAVY	k1gFnPc2
WATER	WATER	kA
APPLICATION	APPLICATION	kA
IN	IN	kA
HPLWR	HPLWR	kA
NEW	NEW	kA
FLOW	FLOW	kA
PATTERN	PATTERN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNL	CNL	kA
Nuclear	Nuclear	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.129	10.129	k4
<g/>
43	#num#	k4
<g/>
/	/	kIx~
<g/>
CNR	CNR	kA
<g/>
.2016	.2016	k4
<g/>
.00019	.00019	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Voda	voda	k1gFnSc1
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1
voda	voda	k1gFnSc1
</s>
<s>
Tritiová	Tritiový	k2eAgFnSc1d1
voda	voda	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
