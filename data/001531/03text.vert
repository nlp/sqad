<s>
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Lawrence	Lawrenec	k1gInSc2	Lawrenec
Bragg	Bragga	k1gFnPc2	Bragga
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Adelaide	Adelaid	k1gMnSc5	Adelaid
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Ipswich	Ipswich	k1gMnSc1	Ipswich
<g/>
,	,	kIx,	,
Suffolk	Suffolk	k1gMnSc1	Suffolk
County	Counta	k1gFnSc2	Counta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Williama	William	k1gMnSc2	William
Henryho	Henry	k1gMnSc2	Henry
Bragga	Bragg	k1gMnSc2	Bragg
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
strukturní	strukturní	k2eAgFnSc2d1	strukturní
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
Braggovu	Braggův	k2eAgFnSc4d1	Braggova
metodu	metoda	k1gFnSc4	metoda
(	(	kIx(	(
<g/>
metodu	metoda	k1gFnSc4	metoda
otočného	otočný	k2eAgInSc2d1	otočný
krystalu	krystal	k1gInSc2	krystal
<g/>
)	)	kIx)	)
na	na	k7c6	na
určování	určování	k1gNnSc6	určování
krystalové	krystalový	k2eAgFnSc2d1	krystalová
struktury	struktura	k1gFnSc2	struktura
pomocí	pomocí	k7c2	pomocí
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
umožnila	umožnit	k5eAaPmAgFnS	umožnit
zjistit	zjistit	k5eAaPmF	zjistit
krystalovou	krystalový	k2eAgFnSc4d1	krystalová
strukturu	struktura	k1gFnSc4	struktura
mnohých	mnohý	k2eAgFnPc2d1	mnohá
anorganických	anorganický	k2eAgFnPc2d1	anorganická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
diamantu	diamant	k1gInSc2	diamant
či	či	k8xC	či
kamenné	kamenný	k2eAgFnSc2d1	kamenná
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Lawrence	Lawrence	k1gFnSc1	Lawrence
Bragg	Bragg	k1gMnSc1	Bragg
nadto	nadto	k6eAd1	nadto
formuloval	formulovat	k5eAaImAgMnS	formulovat
Braggovu	Braggův	k2eAgFnSc4d1	Braggova
rovnici	rovnice	k1gFnSc4	rovnice
(	(	kIx(	(
<g/>
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
podmínku	podmínka	k1gFnSc4	podmínka
pro	pro	k7c4	pro
ohyb	ohyb	k1gInSc4	ohyb
monochromatického	monochromatický	k2eAgNnSc2d1	monochromatické
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
lambda	lambda	k1gNnSc1	lambda
na	na	k7c6	na
krystalech	krystal	k1gInPc6	krystal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bragg	Bragg	k1gInSc1	Bragg
takto	takto	k6eAd1	takto
umožnil	umožnit	k5eAaPmAgInS	umožnit
zjištění	zjištění	k1gNnSc4	zjištění
atomové	atomový	k2eAgFnSc2d1	atomová
struktury	struktura	k1gFnSc2	struktura
nerostů	nerost	k1gInPc2	nerost
<g/>
,	,	kIx,	,
slitin	slitina	k1gFnPc2	slitina
a	a	k8xC	a
silikátů	silikát	k1gInPc2	silikát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
objevy	objev	k1gInPc4	objev
dostal	dostat	k5eAaPmAgMnS	dostat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
například	například	k6eAd1	například
Electricity	Electricita	k1gFnSc2	Electricita
<g/>
,	,	kIx,	,
Atomic	Atomic	k1gMnSc1	Atomic
structure	structur	k1gMnSc5	structur
of	of	k?	of
minerals	minerals	k1gInSc1	minerals
<g/>
,	,	kIx,	,
X-rays	Xays	k1gInSc1	X-rays
and	and	k?	and
crystal	crystat	k5eAaPmAgInS	crystat
structure	structur	k1gMnSc5	structur
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc1	The
history	histor	k1gInPc1	histor
of	of	k?	of
X-ray	Xaa	k1gFnSc2	X-raa
analysis	analysis	k1gFnSc2	analysis
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
William	William	k1gInSc1	William
Lawrence	Lawrenec	k1gInSc2	Lawrenec
Bragg	Bragga	k1gFnPc2	Bragga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
William	William	k1gInSc1	William
Lawrence	Lawrence	k1gFnSc2	Lawrence
Bragg	Bragga	k1gFnPc2	Bragga
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
www.aldebaran.cz	www.aldebaran.cz	k1gInSc4	www.aldebaran.cz
nobelprize	nobelprize	k6eAd1	nobelprize
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
</s>
