<p>
<s>
Kanye	Kanyat	k5eAaPmIp3nS	Kanyat
Omari	Omare	k1gFnSc4	Omare
West	Westa	k1gFnPc2	Westa
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Atlanta	Atlanta	k1gFnSc1	Atlanta
<g/>
,	,	kIx,	,
Georgie	Georgie	k1gFnSc1	Georgie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Yeezy	Yeez	k1gInPc1	Yeez
<g/>
,	,	kIx,	,
či	či	k8xC	či
jen	jen	k9	jen
Ye	Ye	k1gFnSc1	Ye
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
rapper	rapper	k1gMnSc1	rapper
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
53	[number]	k4	53
ocenění	ocenění	k1gNnSc4	ocenění
ze	z	k7c2	z
161	[number]	k4	161
nominací	nominace	k1gFnPc2	nominace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
21	[number]	k4	21
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
producent	producent	k1gMnSc1	producent
pro	pro	k7c4	pro
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
společnost	společnost	k1gFnSc4	společnost
Roc-A-Fella	Roc-A-Fello	k1gNnSc2	Roc-A-Fello
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
albu	album	k1gNnSc6	album
rappera	rapper	k1gMnSc2	rapper
Jay-Zho	Jay-Z	k1gMnSc2	Jay-Z
–	–	k?	–
The	The	k1gMnSc1	The
Blueprint	Blueprint	k1gMnSc1	Blueprint
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
produkoval	produkovat	k5eAaImAgMnS	produkovat
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
písně	píseň	k1gFnPc4	píseň
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xC	jako
Alicia	Alicium	k1gNnSc2	Alicium
Keys	Keysa	k1gFnPc2	Keysa
<g/>
,	,	kIx,	,
Ludacris	Ludacris	k1gFnPc2	Ludacris
nebo	nebo	k8xC	nebo
Janet	Janet	k1gMnSc1	Janet
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
velmi	velmi	k6eAd1	velmi
originální	originální	k2eAgInSc1d1	originální
styl	styl	k1gInSc1	styl
produkce	produkce	k1gFnSc2	produkce
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
zpěvných	zpěvný	k2eAgInPc6d1	zpěvný
samplech	sampl	k1gInPc6	sampl
soulových	soulový	k2eAgFnPc2d1	soulová
písniček	písnička	k1gFnPc2	písnička
proložených	proložený	k2eAgFnPc2d1	proložená
Westem	West	k1gInSc7	West
tvořenými	tvořený	k2eAgFnPc7d1	tvořená
bicími	bicí	k2eAgFnPc7d1	bicí
smyčkami	smyčka	k1gFnPc7	smyčka
a	a	k8xC	a
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
měly	mít	k5eAaImAgFnP	mít
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
hudební	hudební	k2eAgNnSc1d1	hudební
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
vliv	vliv	k1gInSc4	vliv
žánry	žánr	k1gInPc4	žánr
jako	jako	k8xC	jako
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
baroque	baroquat	k5eAaPmIp3nS	baroquat
pop	pop	k1gMnSc1	pop
<g/>
,	,	kIx,	,
trip	trip	k1gMnSc1	trip
hop	hop	k0	hop
<g/>
,	,	kIx,	,
arena	arena	k6eAd1	arena
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
folk	folk	k1gInSc1	folk
<g/>
,	,	kIx,	,
alternative	alternativ	k1gInSc5	alternativ
<g/>
,	,	kIx,	,
electronica	electronicum	k1gNnPc1	electronicum
<g/>
,	,	kIx,	,
synth-pop	synthop	k1gInSc1	synth-pop
nebo	nebo	k8xC	nebo
vážná	vážný	k2eAgFnSc1d1	vážná
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
The	The	k1gMnSc2	The
College	Colleg	k1gMnSc2	Colleg
Dropout	Dropout	k1gMnSc1	Dropout
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
Late	lat	k1gInSc5	lat
Registration	Registration	k1gInSc4	Registration
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
album	album	k1gNnSc4	album
Graduation	Graduation	k1gInSc1	Graduation
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
808	[number]	k4	808
<g/>
s	s	k7c7	s
&	&	k?	&
Heartbreak	Heartbreak	k1gMnSc1	Heartbreak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
čtyři	čtyři	k4xCgNnPc4	čtyři
alba	album	k1gNnPc4	album
získala	získat	k5eAaPmAgFnS	získat
nespočet	nespočet	k1gInSc4	nespočet
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
18	[number]	k4	18
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
uznání	uznání	k1gNnSc2	uznání
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
alba	album	k1gNnPc1	album
byla	být	k5eAaImAgNnP	být
komerčně	komerčně	k6eAd1	komerčně
velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
808	[number]	k4	808
<g/>
s	s	k7c7	s
&	&	k?	&
Heartbreak	Heartbreak	k1gInSc1	Heartbreak
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc4	jeho
třetí	třetí	k4xOgNnSc4	třetí
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
hned	hned	k6eAd1	hned
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
GOOD	GOOD	kA	GOOD
Music	Musice	k1gFnPc2	Musice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
či	či	k8xC	či
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
umělce	umělec	k1gMnSc4	umělec
jako	jako	k8xS	jako
Pusha	Push	k1gMnSc4	Push
T	T	kA	T
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
Common	Commona	k1gFnPc2	Commona
nebo	nebo	k8xC	nebo
Kid	Kid	k1gFnPc2	Kid
Cudi	Cud	k1gFnSc2	Cud
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
maskotem	maskot	k1gInSc7	maskot
je	být	k5eAaImIp3nS	být
tzv	tzv	kA	tzv
"	"	kIx"	"
<g/>
Medvídek	medvídek	k1gMnSc1	medvídek
odpadlík	odpadlík	k1gMnSc1	odpadlík
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dropout	Dropout	k1gMnSc1	Dropout
Bear	Bear	k1gMnSc1	Bear
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plyšový	plyšový	k2eAgMnSc1d1	plyšový
medvídek	medvídek	k1gMnSc1	medvídek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
obalech	obal	k1gInPc6	obal
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
čtyř	čtyři	k4xCgInPc2	čtyři
prvních	první	k4xOgInPc2	první
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
na	na	k7c6	na
singlech	singl	k1gInPc6	singl
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
videoklipech	videoklip	k1gInPc6	videoklip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
serveru	server	k1gInSc6	server
About	About	k1gInSc1	About
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
50	[number]	k4	50
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
hip-hopových	hipopový	k2eAgMnPc2d1	hip-hopový
producentů	producent	k1gMnPc2	producent
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
hudební	hudební	k2eAgFnSc7d1	hudební
stanicí	stanice	k1gFnSc7	stanice
MTV	MTV	kA	MTV
jako	jako	k8xS	jako
MC	MC	kA	MC
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
#	#	kIx~	#
<g/>
1	[number]	k4	1
Hottest	Hottest	k1gInSc1	Hottest
MC	MC	kA	MC
in	in	k?	in
the	the	k?	the
Game	game	k1gInSc1	game
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc1	biografie
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
<g/>
,	,	kIx,	,
Georgia	Georgia	k1gFnSc1	Georgia
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k9	když
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Ray	Ray	k1gMnSc1	Ray
West	West	k1gMnSc1	West
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
revolucionářské	revolucionářský	k2eAgFnSc2d1	revolucionářská
organizace	organizace	k1gFnSc2	organizace
Black	Blacka	k1gFnPc2	Blacka
Panthers	Panthersa	k1gFnPc2	Panthersa
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
afroamerických	afroamerický	k2eAgMnPc2d1	afroamerický
fotožurnalistů	fotožurnalista	k1gMnPc2	fotožurnalista
časopisu	časopis	k1gInSc6	časopis
Atlanta	Atlanta	k1gFnSc1	Atlanta
Journal-Constitution	Journal-Constitution	k1gInSc1	Journal-Constitution
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
křesťanským	křesťanský	k2eAgMnSc7d1	křesťanský
zástupcem	zástupce	k1gMnSc7	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Donda	Donda	k1gMnSc1	Donda
West	West	k1gMnSc1	West
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
profesorka	profesorka	k1gFnSc1	profesorka
angličtiny	angličtina	k1gFnSc2	angličtina
na	na	k7c4	na
Clark	Clark	k1gInSc4	Clark
Atlanta	Atlant	k1gMnSc2	Atlant
University	universita	k1gFnSc2	universita
a	a	k8xC	a
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
oddělení	oddělení	k1gNnSc2	oddělení
Anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c6	na
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
než	než	k8xS	než
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc1	jeho
manažerka	manažerka	k1gFnSc1	manažerka
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
v	v	k7c6	v
Oak	Oak	k1gFnSc6	Oak
Lawn	Lawna	k1gFnPc2	Lawna
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnPc2	Illinois
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
z	z	k7c2	z
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Podepsala	podepsat	k5eAaPmAgFnS	podepsat
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
ryze	ryze	k6eAd1	ryze
ženská	ženský	k2eAgFnSc1d1	ženská
výchova	výchova	k1gFnSc1	výchova
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
"	"	kIx"	"
<g/>
zženštilé	zženštilý	k2eAgFnSc2d1	zženštilá
<g/>
"	"	kIx"	"
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc4	jaký
známky	známka	k1gFnPc4	známka
dostával	dostávat	k5eAaImAgMnS	dostávat
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
West	West	k1gMnSc1	West
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jedničky	jednička	k1gFnPc4	jednička
a	a	k8xC	a
dvojky	dvojka	k1gFnPc4	dvojka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc4	ten
nekecám	kecat	k5eNaImIp1nS	kecat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
hodiny	hodina	k1gFnPc4	hodina
umění	umění	k1gNnSc2	umění
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
také	také	k9	také
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
státní	státní	k2eAgFnSc6d1	státní
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
kvůli	kvůli	k7c3	kvůli
soustředění	soustředění	k1gNnSc3	soustředění
se	se	k3xPyFc4	se
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
hudební	hudební	k2eAgFnSc4d1	hudební
kariéru	kariéra	k1gFnSc4	kariéra
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
produkoval	produkovat	k5eAaImAgInS	produkovat
hudbu	hudba	k1gFnSc4	hudba
místním	místní	k2eAgMnPc3d1	místní
umělcům	umělec	k1gMnPc3	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
slavným	slavný	k2eAgInSc7d1	slavný
díky	dík	k1gInPc7	dík
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
albech	album	k1gNnPc6	album
známých	známý	k2eAgMnPc2d1	známý
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xC	jako
Jay-Z	Jay-Z	k1gMnPc2	Jay-Z
<g/>
,	,	kIx,	,
Mobb	Mobb	k1gMnSc1	Mobb
Deep	Deep	k1gMnSc1	Deep
<g/>
,	,	kIx,	,
Cam	Cam	k1gMnSc1	Cam
<g/>
'	'	kIx"	'
<g/>
ron	ron	k1gInSc1	ron
<g/>
,	,	kIx,	,
Scarface	Scarface	k1gFnSc1	Scarface
<g/>
,	,	kIx,	,
Alicia	Alicia	k1gFnSc1	Alicia
Keys	Keysa	k1gFnPc2	Keysa
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Tajně	tajně	k6eAd1	tajně
také	také	k9	také
produkoval	produkovat	k5eAaImAgMnS	produkovat
muziku	muzika	k1gFnSc4	muzika
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
mentora	mentor	k1gMnSc4	mentor
Derica	Dericus	k1gMnSc4	Dericus
"	"	kIx"	"
<g/>
D-Dot	D-Dot	k1gMnSc1	D-Dot
<g/>
"	"	kIx"	"
Angelettiea	Angelettiea	k1gMnSc1	Angelettiea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Začátky	začátek	k1gInPc1	začátek
kariéry	kariéra	k1gFnSc2	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rukopis	rukopis	k1gInSc1	rukopis
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
znát	znát	k5eAaImF	znát
na	na	k7c6	na
uznávaném	uznávaný	k2eAgNnSc6d1	uznávané
albu	album	k1gNnSc6	album
umělce	umělec	k1gMnSc2	umělec
Jay-Zho	Jay-Z	k1gMnSc2	Jay-Z
–	–	k?	–
The	The	k1gMnSc1	The
Blueprint	Blueprint	k1gMnSc1	Blueprint
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Produkoval	produkovat	k5eAaImAgMnS	produkovat
hudbu	hudba	k1gFnSc4	hudba
i	i	k9	i
k	k	k7c3	k
úvodním	úvodní	k2eAgInPc3d1	úvodní
singlům	singl	k1gInPc3	singl
"	"	kIx"	"
<g/>
Izzo	Izzo	k6eAd1	Izzo
(	(	kIx(	(
<g/>
H.O.V.A.	H.O.V.A.	k1gFnSc1	H.O.V.A.
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
the	the	k?	the
City	city	k1gNnSc1	city
(	(	kIx(	(
<g/>
Ain	Ain	k1gFnSc1	Ain
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
No	no	k9	no
Love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
výsměšné	výsměšný	k2eAgFnSc3d1	výsměšná
písničce	písnička	k1gFnSc3	písnička
proti	proti	k7c3	proti
umělcům	umělec	k1gMnPc3	umělec
Nas	Nas	k1gFnSc2	Nas
a	a	k8xC	a
Mobb	Mobb	k1gMnSc1	Mobb
Deep	Deep	k1gMnSc1	Deep
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Takeover	Takeover	k1gInSc4	Takeover
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
této	tento	k3xDgFnSc2	tento
písničky	písnička	k1gFnSc2	písnička
začal	začít	k5eAaPmAgInS	začít
se	s	k7c7	s
zmíněnými	zmíněný	k2eAgMnPc7d1	zmíněný
umělci	umělec	k1gMnPc7	umělec
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
stalo	stát	k5eAaPmAgNnS	stát
ceněnou	ceněný	k2eAgFnSc7d1	ceněná
značkou	značka	k1gFnSc7	značka
v	v	k7c6	v
hip-hopovém	hipopový	k2eAgInSc6d1	hip-hopový
žánru	žánr	k1gInSc6	žánr
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
měl	mít	k5eAaImAgMnS	mít
velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
vydala	vydat	k5eAaPmAgFnS	vydat
jeho	jeho	k3xOp3gInSc4	jeho
vlastní	vlastní	k2eAgInSc4d1	vlastní
počin	počin	k1gInSc4	počin
<g/>
.	.	kIx.	.
</s>
<s>
Jay-Z	Jay-Z	k?	Jay-Z
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Roc-A-Fella	Roc-A-Fella	k1gFnSc1	Roc-A-Fella
Records	Records	k1gInSc1	Records
jej	on	k3xPp3gMnSc4	on
vidí	vidět	k5eAaImIp3nS	vidět
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
jako	jako	k8xC	jako
producenta	producent	k1gMnSc2	producent
<g/>
,	,	kIx,	,
než	než	k8xS	než
cokoli	cokoli	k3yInSc4	cokoli
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
nahrávací	nahrávací	k2eAgFnPc1d1	nahrávací
společnosti	společnost	k1gFnPc1	společnost
byly	být	k5eAaImAgFnP	být
toho	ten	k3xDgInSc2	ten
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
představitelem	představitel	k1gMnSc7	představitel
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
pouličního	pouliční	k2eAgInSc2d1	pouliční
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
by	by	kYmCp3nS	by
marketingově	marketingově	k6eAd1	marketingově
výhodným	výhodný	k2eAgInSc7d1	výhodný
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
The	The	k1gMnSc4	The
College	Colleg	k1gMnSc4	Colleg
Dropout	Dropout	k1gMnSc1	Dropout
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
účastníkem	účastník	k1gMnSc7	účastník
fatální	fatální	k2eAgFnSc2d1	fatální
autonehody	autonehoda	k1gFnSc2	autonehoda
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
domů	domů	k6eAd1	domů
z	z	k7c2	z
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
mu	on	k3xPp3gMnSc3	on
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
inspiraci	inspirace	k1gFnSc4	inspirace
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Thourgh	Thourgh	k1gInSc1	Thourgh
the	the	k?	the
Wire	Wire	k1gInSc1	Wire
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
víra	víra	k1gFnSc1	víra
je	být	k5eAaImIp3nS	být
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jeho	jeho	k3xOp3gFnPc6	jeho
písničkách	písnička	k1gFnPc6	písnička
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Jesus	Jesus	k1gInSc1	Jesus
Walks	Walks	k1gInSc1	Walks
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gNnPc2	jeho
benefičních	benefiční	k2eAgNnPc2d1	benefiční
představení	představení	k1gNnPc2	představení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
koncert	koncert	k1gInSc1	koncert
Live	Liv	k1gInSc2	Liv
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
písně	píseň	k1gFnPc1	píseň
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
debutovém	debutový	k2eAgNnSc6d1	debutové
albu	album	k1gNnSc6	album
The	The	k1gFnSc2	The
College	College	k1gFnPc2	College
Dropout	Dropout	k1gMnSc1	Dropout
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Roc-A-Fella	Roc-A-Fell	k1gMnSc2	Roc-A-Fell
Records	Recordsa	k1gFnPc2	Recordsa
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
očekávána	očekáván	k2eAgFnSc1d1	očekávána
kritika	kritika	k1gFnSc1	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
definovalo	definovat	k5eAaBmAgNnS	definovat
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
byl	být	k5eAaImAgMnS	být
West	West	k1gMnSc1	West
proslulý	proslulý	k2eAgMnSc1d1	proslulý
<g/>
,	,	kIx,	,
propracované	propracovaný	k2eAgInPc1d1	propracovaný
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
slovní	slovní	k2eAgFnPc1d1	slovní
hříčky	hříčka	k1gFnPc1	hříčka
a	a	k8xC	a
samplování	samplování	k1gNnPc1	samplování
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
třikrát	třikrát	k6eAd1	třikrát
platinové	platinový	k2eAgNnSc1d1	platinové
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
hosté	host	k1gMnPc1	host
jako	jako	k8xS	jako
Jay-Z	Jay-Z	k1gMnPc1	Jay-Z
<g/>
,	,	kIx,	,
Ludacris	Ludacris	k1gFnPc1	Ludacris
<g/>
,	,	kIx,	,
GLC	GLC	kA	GLC
<g/>
,	,	kIx,	,
Consequence	Consequence	k1gFnSc1	Consequence
<g/>
,	,	kIx,	,
Talib	Talib	k1gInSc1	Talib
Kweli	Kwele	k1gFnSc4	Kwele
<g/>
,	,	kIx,	,
Mos	Mos	k1gMnSc1	Mos
Def	Def	k1gMnSc1	Def
<g/>
,	,	kIx,	,
Common	Common	k1gMnSc1	Common
a	a	k8xC	a
Syleena	Syleena	k1gFnSc1	Syleena
Johnson	Johnsona	k1gFnPc2	Johnsona
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
albem	album	k1gNnSc7	album
byly	být	k5eAaImAgInP	být
spojeny	spojit	k5eAaPmNgInP	spojit
i	i	k8xC	i
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
Falls	Fallsa	k1gFnPc2	Fallsa
Down	Downa	k1gFnPc2	Downa
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
New	New	k1gFnSc2	New
Workou	Worká	k1gFnSc4	Worká
Plan	plan	k1gInSc1	plan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
single	singl	k1gInSc5	singl
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Slow	Slow	k1gMnSc1	Slow
Jamz	Jamz	k1gMnSc1	Jamz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
producent	producent	k1gMnSc1	producent
na	na	k7c6	na
albu	album	k1gNnSc6	album
britského	britský	k2eAgMnSc2d1	britský
zpěváka	zpěvák	k1gMnSc2	zpěvák
Javina	Javin	k1gMnSc2	Javin
Hyltona	Hylton	k1gMnSc2	Hylton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prošel	projít	k5eAaPmAgMnS	projít
i	i	k9	i
finanční	finanční	k2eAgFnSc7d1	finanční
válkou	válka	k1gFnSc7	válka
s	s	k7c7	s
rapperem	rapper	k1gInSc7	rapper
Royce	Royec	k1gInSc2	Royec
Da	Da	k1gFnSc7	Da
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
9	[number]	k4	9
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
složil	složit	k5eAaPmAgMnS	složit
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Hearbeat	Hearbeat	k1gInSc4	Hearbeat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
albu	album	k1gNnSc6	album
Build	Buildo	k1gNnPc2	Buildo
&	&	k?	&
Destroy	Destroa	k1gMnSc2	Destroa
<g/>
:	:	kIx,	:
The	The	k1gMnSc2	The
Lost	Lost	k2eAgInSc1d1	Lost
Sessions	Sessions	k1gInSc1	Sessions
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Royce	Royce	k1gFnSc1	Royce
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
beat	beat	k1gInSc4	beat
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
písničce	písnička	k1gFnSc3	písnička
nikdy	nikdy	k6eAd1	nikdy
nezaplatil	zaplatit	k5eNaPmAgMnS	zaplatit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nahrál	nahrát	k5eAaBmAgMnS	nahrát
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
zkušenosti	zkušenost	k1gFnSc6	zkušenost
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Roycem	Royec	k1gInSc7	Royec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
singly	singl	k1gInPc1	singl
produkované	produkovaný	k2eAgInPc1d1	produkovaný
Kanyem	Kany	k1gInSc7	Kany
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tvorby	tvorba	k1gFnSc2	tvorba
The	The	k1gFnSc2	The
College	Colleg	k1gFnSc2	Colleg
Dropout	Dropout	k1gMnSc1	Dropout
byly	být	k5eAaImAgFnP	být
"	"	kIx"	"
<g/>
I	i	k9	i
Changed	Changed	k1gInSc1	Changed
My	my	k3xPp1nPc1	my
Mind	Minda	k1gFnPc2	Minda
<g/>
"	"	kIx"	"
–	–	k?	–
Keyshia	Keyshius	k1gMnSc4	Keyshius
Cole	cola	k1gFnSc6	cola
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Overnight	Overnight	k1gInSc1	Overnight
Celebrity	celebrita	k1gFnSc2	celebrita
<g/>
"	"	kIx"	"
–	–	k?	–
Twista	Twista	k1gMnSc1	Twista
a	a	k8xC	a
"	"	kIx"	"
<g/>
Talk	Talk	k1gMnSc1	Talk
About	About	k1gMnSc1	About
Our	Our	k1gMnSc1	Our
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
–	–	k?	–
Brandy	brandy	k1gFnSc1	brandy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Late	lat	k1gInSc5	lat
Registration	Registration	k1gInSc1	Registration
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Jonem	Jon	k1gMnSc7	Jon
Brionem	Brion	k1gMnSc7	Brion
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2005	[number]	k4	2005
a	a	k8xC	a
setkalo	setkat	k5eAaPmAgNnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
pozitivními	pozitivní	k2eAgFnPc7d1	pozitivní
kritikami	kritika	k1gFnPc7	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Časopisy	časopis	k1gInPc1	časopis
a	a	k8xC	a
noviny	novina	k1gFnPc1	novina
jako	jako	k8xS	jako
Time	Tim	k1gFnPc1	Tim
<g/>
,	,	kIx,	,
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
Spin	spin	k1gInSc1	spin
nebo	nebo	k8xC	nebo
USA	USA	kA	USA
Today	Today	k1gInPc1	Today
<g/>
,	,	kIx,	,
vyhlašovaly	vyhlašovat	k5eAaImAgFnP	vyhlašovat
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
860	[number]	k4	860
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
2,3	[number]	k4	2,3
milionu	milion	k4xCgInSc2	milion
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
pět	pět	k4xCc1	pět
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
tři	tři	k4xCgInPc1	tři
získal	získat	k5eAaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
"	"	kIx"	"
<g/>
number-one	numbern	k1gInSc5	number-on
<g/>
"	"	kIx"	"
hit	hit	k1gInSc4	hit
"	"	kIx"	"
<g/>
Gold	Gold	k1gMnSc1	Gold
Digger	digger	k1gMnSc1	digger
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Jamie	Jamie	k1gFnSc1	Jamie
Foxx	Foxx	k1gInSc1	Foxx
<g/>
)	)	kIx)	)
a	a	k8xC	a
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Heard	Heard	k1gMnSc1	Heard
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
Say	Say	k1gFnPc2	Say
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Adam	Adam	k1gMnSc1	Adam
Levine	Levin	k1gInSc5	Levin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
v	v	k7c6	v
USA	USA	kA	USA
3,1	[number]	k4	3,1
milionu	milion	k4xCgInSc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vyprodukoval	vyprodukovat	k5eAaPmAgMnS	vyprodukovat
hity	hit	k1gInPc4	hit
"	"	kIx"	"
<g/>
Dreams	Dreams	k1gInSc4	Dreams
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Game	game	k1gInSc1	game
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Go	Go	k1gFnSc1	Go
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Common	Common	k1gInSc1	Common
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Graduation	Graduation	k1gInSc1	Graduation
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odložil	odložit	k5eAaPmAgInS	odložit
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
svého	své	k1gNnSc2	své
třetího	třetí	k4xOgNnSc2	třetí
alba	album	k1gNnSc2	album
Graduation	Graduation	k1gInSc4	Graduation
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
jako	jako	k8xS	jako
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
vydání	vydání	k1gNnSc1	vydání
alba	album	k1gNnSc2	album
Curtis	Curtis	k1gInSc4	Curtis
rappera	rappero	k1gNnSc2	rappero
50	[number]	k4	50
Centa	Cent	k1gInSc2	Cent
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zažehla	zažehnout	k5eAaPmAgFnS	zažehnout
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
soutěž	soutěž	k1gFnSc1	soutěž
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
prodá	prodat	k5eAaPmIp3nS	prodat
více	hodně	k6eAd2	hodně
kusů	kus	k1gInPc2	kus
svého	své	k1gNnSc2	své
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
soutěž	soutěž	k1gFnSc4	soutěž
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
50	[number]	k4	50
Cent	cent	k1gInSc4	cent
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
přestat	přestat	k5eAaPmF	přestat
vydávat	vydávat	k5eAaPmF	vydávat
sólo	sólo	k2eAgNnPc4d1	sólo
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
prohraje	prohrát	k5eAaPmIp3nS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
své	svůj	k3xOyFgNnSc4	svůj
prohlášení	prohlášení	k1gNnSc4	prohlášení
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
následně	následně	k6eAd1	následně
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
Graduation	Graduation	k1gInSc1	Graduation
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
957	[number]	k4	957
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
alba	album	k1gNnPc1	album
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
2	[number]	k4	2
960	[number]	k4	960
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
mu	on	k3xPp3gMnSc3	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
dalších	další	k2eAgFnPc2d1	další
osm	osm	k4xCc1	osm
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
úspěšně	úspěšně	k6eAd1	úspěšně
získal	získat	k5eAaPmAgMnS	získat
čtyři	čtyři	k4xCgFnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
pochází	pocházet	k5eAaImIp3nS	pocházet
další	další	k2eAgNnSc1d1	další
"	"	kIx"	"
<g/>
number-one	numbern	k1gInSc5	number-on
<g/>
"	"	kIx"	"
hit	hit	k1gInSc4	hit
"	"	kIx"	"
<g/>
Stronger	Stronger	k1gInSc1	Stronger
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Good	Good	k1gInSc1	Good
Life	Lif	k1gInSc2	Lif
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
T-Pain	T-Pain	k1gInSc1	T-Pain
<g/>
)	)	kIx)	)
a	a	k8xC	a
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Flashing	Flashing	k1gInSc1	Flashing
Lights	Lights	k1gInSc1	Lights
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Dwele	Dwele	k1gInSc1	Dwele
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
808	[number]	k4	808
<g/>
s	s	k7c7	s
&	&	k?	&
Heartbreak	Heartbreak	k1gInSc1	Heartbreak
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
na	na	k7c6	na
Národním	národní	k2eAgInSc6d1	národní
sjezdu	sjezd	k1gInSc6	sjezd
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
kandidáta	kandidát	k1gMnSc2	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
USA	USA	kA	USA
Baracka	Baracka	k1gFnSc1	Baracka
Obamy	Obama	k1gFnSc2	Obama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
vydal	vydat	k5eAaPmAgInS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
nový	nový	k2eAgInSc4d1	nový
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Lockdown	Lockdowno	k1gNnPc2	Lockdowno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
překvapivě	překvapivě	k6eAd1	překvapivě
neobsahoval	obsahovat	k5eNaImAgMnS	obsahovat
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
zpěv	zpěv	k1gInSc1	zpěv
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
programu	program	k1gInSc2	program
auto-tune	autoun	k1gInSc5	auto-tun
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prvky	prvek	k1gInPc4	prvek
synthpopu	synthpop	k1gInSc2	synthpop
<g/>
,	,	kIx,	,
electroniky	electronika	k1gFnSc2	electronika
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
a	a	k8xC	a
electropopu	electropop	k1gInSc2	electropop
<g/>
.	.	kIx.	.
</s>
<s>
Alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
450	[number]	k4	450
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
1	[number]	k4	1
800	[number]	k4	800
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
Heartless	Heartless	k1gInSc4	Heartless
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Lockdown	Lockdowno	k1gNnPc2	Lockdowno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
My	my	k3xPp1nPc1	my
Beautiful	Beautiful	k1gInSc4	Beautiful
Dark	Dark	k1gInSc1	Dark
Twisted	Twisted	k1gInSc1	Twisted
Fantasy	fantas	k1gInPc1	fantas
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
jmenovat	jmenovat	k5eAaImF	jmenovat
Good	Good	k1gInSc4	Good
Ass	Ass	k1gMnSc1	Ass
Job	Job	k1gMnSc1	Job
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k1gInSc1	West
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
album	album	k1gNnSc1	album
bude	být	k5eAaImBp3nS	být
opět	opět	k6eAd1	opět
něčím	něco	k3yInSc7	něco
novým	nový	k2eAgNnSc7d1	nové
<g/>
,	,	kIx,	,
co	co	k9	co
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
ještě	ještě	k6eAd1	ještě
nedělal	dělat	k5eNaImAgMnS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
umělecké	umělecký	k2eAgNnSc4d1	umělecké
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
pochází	pocházet	k5eAaImIp3nS	pocházet
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
Power	Power	k1gInSc4	Power
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Dwele	Dwele	k1gFnSc1	Dwele
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Runaway	Runawaa	k1gFnPc4	Runawaa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Pusha	Pusha	k1gMnSc1	Pusha
T	T	kA	T
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Monster	monstrum	k1gNnPc2	monstrum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgNnPc4d1	hudební
videa	video	k1gNnPc4	video
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
písním	píseň	k1gFnPc3	píseň
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
Westovy	Westův	k2eAgFnPc4d1	Westova
umělecké	umělecký	k2eAgFnPc4d1	umělecká
snahy	snaha	k1gFnPc4	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Power	Power	k1gInSc1	Power
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
starověkým	starověký	k2eAgInSc7d1	starověký
alegorickým	alegorický	k2eAgInSc7d1	alegorický
obrazem	obraz	k1gInSc7	obraz
a	a	k8xC	a
klip	klip	k1gInSc4	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Runaway	Runaway	k1gInPc4	Runaway
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
třiceti	třicet	k4xCc7	třicet
čtyř	čtyři	k4xCgMnPc2	čtyři
minutovým	minutový	k2eAgInSc7d1	minutový
mini-filmem	miniilm	k1gInSc7	mini-film
o	o	k7c6	o
pádu	pád	k1gInSc6	pád
Fénixe	fénix	k1gMnSc2	fénix
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
krásné	krásný	k2eAgFnSc2d1	krásná
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
opětovného	opětovný	k2eAgNnSc2d1	opětovné
povstání	povstání	k1gNnSc2	povstání
z	z	k7c2	z
popele	popel	k1gInSc2	popel
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Runaway	Runaway	k1gInPc4	Runaway
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dalších	další	k2eAgFnPc2d1	další
osm	osm	k4xCc1	osm
částí	část	k1gFnPc2	část
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mini-film	miniilm	k1gInSc1	mini-film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
West	West	k1gInSc4	West
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
například	například	k6eAd1	například
řídí	řídit	k5eAaImIp3nS	řídit
český	český	k2eAgInSc1d1	český
supersport	supersport	k1gInSc1	supersport
MTX	MTX	kA	MTX
Tatra	Tatra	k1gFnSc1	Tatra
V	v	k7c4	v
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
existují	existovat	k5eAaImIp3nP	existovat
jen	jen	k9	jen
čtyři	čtyři	k4xCgInPc4	čtyři
kusy	kus	k1gInPc4	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
497	[number]	k4	497
000	[number]	k4	000
ks	ks	kA	ks
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
získalo	získat	k5eAaPmAgNnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
certifikaci	certifikace	k1gFnSc4	certifikace
společnosti	společnost	k1gFnSc2	společnost
RIAA	RIAA	kA	RIAA
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
také	také	k9	také
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
RIAA	RIAA	kA	RIAA
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
kusů	kus	k1gInPc2	kus
v	v	k7c4	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
oceněno	oceněn	k2eAgNnSc1d1	oceněno
třemi	tři	k4xCgFnPc7	tři
cenami	cena	k1gFnPc7	cena
Grammy	Gramm	k1gInPc4	Gramm
<g/>
.	.	kIx.	.
</s>
<s>
Získalo	získat	k5eAaPmAgNnS	získat
Grammy	Gramm	k1gInPc4	Gramm
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
rapové	rapový	k2eAgNnSc4d1	rapové
album	album	k1gNnSc4	album
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
Grammy	Gramma	k1gFnPc4	Gramma
získal	získat	k5eAaPmAgInS	získat
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
of	of	k?	of
the	the	k?	the
Lights	Lights	k1gInSc1	Lights
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Rihanna	Rihanna	k1gFnSc1	Rihanna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Watch	Watch	k1gInSc1	Watch
the	the	k?	the
Throne	Thron	k1gInSc5	Thron
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
nahrával	nahrávat	k5eAaImAgInS	nahrávat
společné	společný	k2eAgNnSc4d1	společné
album	album	k1gNnSc4	album
s	s	k7c7	s
rapperem	rappero	k1gNnSc7	rappero
Jay-Z	Jay-Z	k1gFnSc2	Jay-Z
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Watch	Watch	k1gInSc4	Watch
the	the	k?	the
Throne	Thron	k1gMnSc5	Thron
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
EP	EP	kA	EP
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
celé	celý	k2eAgNnSc1d1	celé
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
H.A.M	H.A.M	k1gFnSc1	H.A.M
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
23	[number]	k4	23
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc4	pozice
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
singl	singl	k1gInSc1	singl
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
Otis	Otis	k1gInSc1	Otis
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
písní	píseň	k1gFnSc7	píseň
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
třetí	třetí	k4xOgInSc1	třetí
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Niggas	Niggas	k1gMnSc1	Niggas
In	In	k1gMnSc1	In
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
příčkách	příčka	k1gFnPc6	příčka
amerických	americký	k2eAgFnPc2d1	americká
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
přísným	přísný	k2eAgNnPc3d1	přísné
bezpečnostním	bezpečnostní	k2eAgNnPc3d1	bezpečnostní
opatřením	opatření	k1gNnPc3	opatření
album	album	k1gNnSc1	album
předčasně	předčasně	k6eAd1	předčasně
neuniklo	uniknout	k5eNaPmAgNnS	uniknout
na	na	k7c4	na
internet	internet	k1gInSc4	internet
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
opatření	opatření	k1gNnPc2	opatření
bylo	být	k5eAaImAgNnS	být
i	i	k8xC	i
prvotní	prvotní	k2eAgNnSc1d1	prvotní
vydání	vydání	k1gNnSc1	vydání
alba	album	k1gNnSc2	album
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
zlomilo	zlomit	k5eAaPmAgNnS	zlomit
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
prodejnosti	prodejnost	k1gFnSc6	prodejnost
alb	alba	k1gFnPc2	alba
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
tam	tam	k6eAd1	tam
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodalo	prodat	k5eAaPmAgNnS	prodat
290	[number]	k4	290
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
v	v	k7c6	v
USA	USA	kA	USA
okolo	okolo	k7c2	okolo
1	[number]	k4	1
425	[number]	k4	425
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Obdrželo	obdržet	k5eAaPmAgNnS	obdržet
certifikaci	certifikace	k1gFnSc4	certifikace
platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
RIAA	RIAA	kA	RIAA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Otis	Otis	k1gInSc4	Otis
<g/>
"	"	kIx"	"
oceněna	ocenit	k5eAaPmNgFnS	ocenit
cenou	cena	k1gFnSc7	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
obsah	obsah	k1gInSc4	obsah
alba	album	k1gNnSc2	album
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
ceny	cena	k1gFnPc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
za	za	k7c2	za
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Niggas	Niggas	k1gMnSc1	Niggas
In	In	k1gMnSc1	In
Paris	Paris	k1gMnSc1	Paris
<g/>
"	"	kIx"	"
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
za	za	k7c2	za
"	"	kIx"	"
<g/>
No	no	k9	no
Church	Church	k1gMnSc1	Church
in	in	k?	in
the	the	k?	the
Wild	Wild	k1gInSc1	Wild
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
Ocean	Ocean	k1gMnSc1	Ocean
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cruel	Cruel	k1gMnSc1	Cruel
Summer	Summer	k1gMnSc1	Summer
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
vydal	vydat	k5eAaPmAgInS	vydat
společné	společný	k2eAgNnSc4d1	společné
kompilační	kompilační	k2eAgNnSc4d1	kompilační
album	album	k1gNnSc4	album
své	svůj	k3xOyFgFnSc2	svůj
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
GOOD	GOOD	kA	GOOD
Music	Music	k1gMnSc1	Music
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Cruel	Crulo	k1gNnPc2	Crulo
Summer	Summero	k1gNnPc2	Summero
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
vydáno	vydat	k5eAaPmNgNnS	vydat
již	již	k6eAd1	již
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
odloženo	odložit	k5eAaPmNgNnS	odložit
na	na	k7c4	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
pocházejí	pocházet	k5eAaImIp3nP	pocházet
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Mercy	Merca	k1gFnPc1	Merca
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
2	[number]	k4	2
Chainz	Chainza	k1gFnPc2	Chainza
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Clique	Clique	k1gFnSc1	Clique
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Jay-Z	Jay-Z	k?	Jay-Z
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
albu	album	k1gNnSc3	album
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vydán	vydat	k5eAaPmNgInS	vydat
i	i	k9	i
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
krátký	krátký	k2eAgInSc1d1	krátký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
publikování	publikování	k1gNnSc1	publikování
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
kolem	kolem	k7c2	kolem
340	[number]	k4	340
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Yeezus	Yeezus	k1gMnSc1	Yeezus
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
šesté	šestý	k4xOgNnSc1	šestý
solo	solo	k1gNnSc1	solo
studiové	studiový	k2eAgFnSc2d1	studiová
album	album	k1gNnSc4	album
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
Yeezus	Yeezus	k1gInSc1	Yeezus
vydal	vydat	k5eAaPmAgInS	vydat
u	u	k7c2	u
společnosti	společnost	k1gFnSc2	společnost
Def	Def	k1gFnSc2	Def
Jam	jam	k1gInSc1	jam
Recordings	Recordings	k1gInSc1	Recordings
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
West	Westum	k1gNnPc2	Westum
při	při	k7c6	při
koncertu	koncert	k1gInSc6	koncert
představil	představit	k5eAaPmAgInS	představit
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
"	"	kIx"	"
<g/>
Black	Black	k1gMnSc1	Black
Skinhead	skinhead	k1gMnSc1	skinhead
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
New	New	k1gFnSc1	New
Slaves	Slaves	k1gMnSc1	Slaves
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
také	také	k6eAd1	také
propagoval	propagovat	k5eAaImAgMnS	propagovat
jako	jako	k9	jako
video	video	k1gNnSc4	video
projekci	projekce	k1gFnSc4	projekce
na	na	k7c6	na
66	[number]	k4	66
budovách	budova	k1gFnPc6	budova
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
nedočkalo	dočkat	k5eNaPmAgNnS	dočkat
klasické	klasický	k2eAgFnPc4d1	klasická
propagace	propagace	k1gFnPc4	propagace
<g/>
,	,	kIx,	,
když	když	k8xS	když
nebyl	být	k5eNaImAgInS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc1	jeden
singl	singl	k1gInSc1	singl
pro	pro	k7c4	pro
rádia	rádio	k1gNnPc4	rádio
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
nebyl	být	k5eNaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
žádný	žádný	k3yNgInSc1	žádný
videoklip	videoklip	k1gInSc1	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
bez	bez	k7c2	bez
obalu	obal	k1gInSc2	obal
a	a	k8xC	a
bookletu	booklet	k1gInSc2	booklet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
uncredited	uncredited	k1gInSc1	uncredited
<g/>
"	"	kIx"	"
hostující	hostující	k2eAgMnPc1d1	hostující
umělci	umělec	k1gMnPc1	umělec
Kid	Kid	k1gFnPc2	Kid
Cudi	Cudi	k1gNnSc2	Cudi
<g/>
,	,	kIx,	,
King	King	k1gMnSc1	King
L	L	kA	L
<g/>
,	,	kIx,	,
Assassin	Assassin	k1gMnSc1	Assassin
<g/>
,	,	kIx,	,
Chief	Chief	k1gMnSc1	Chief
Keef	Keef	k1gMnSc1	Keef
<g/>
,	,	kIx,	,
Charlie	Charlie	k1gMnSc1	Charlie
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Ocean	Ocean	k1gMnSc1	Ocean
a	a	k8xC	a
Justin	Justin	k1gMnSc1	Justin
Vernon	Vernon	k1gMnSc1	Vernon
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Bon	bona	k1gFnPc2	bona
Iver	Iver	k1gMnSc1	Iver
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hudební	hudební	k2eAgFnSc2d1	hudební
produkce	produkce	k1gFnSc2	produkce
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
Westa	Westo	k1gNnPc4	Westo
na	na	k7c6	na
albu	album	k1gNnSc6	album
nejvíce	hodně	k6eAd3	hodně
podíleli	podílet	k5eAaImAgMnP	podílet
Daft	Daft	k2eAgInSc4d1	Daft
Punk	punk	k1gInSc4	punk
a	a	k8xC	a
Mike	Mike	k1gFnSc4	Mike
Dean	Deana	k1gFnPc2	Deana
<g/>
.	.	kIx.	.
</s>
<s>
Muzikálně	muzikálně	k6eAd1	muzikálně
je	být	k5eAaImIp3nS	být
album	album	k1gNnSc4	album
více	hodně	k6eAd2	hodně
temné	temný	k2eAgFnPc1d1	temná
a	a	k8xC	a
experimentální	experimentální	k2eAgFnPc1d1	experimentální
než	než	k8xS	než
jeho	jeho	k3xOp3gFnSc1	jeho
minulá	minulý	k2eAgFnSc1d1	minulá
alba	alba	k1gFnSc1	alba
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prvky	prvek	k1gInPc4	prvek
žánrů	žánr	k1gInPc2	žánr
chicagský	chicagský	k2eAgInSc1d1	chicagský
drill	drill	k1gInSc1	drill
<g/>
,	,	kIx,	,
dancehall	dancehall	k1gInSc1	dancehall
<g/>
,	,	kIx,	,
acid	acid	k6eAd1	acid
house	house	k1gNnSc1	house
a	a	k8xC	a
industriál	industriál	k1gInSc1	industriál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
prodeje	prodej	k1gInSc2	prodej
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
327	[number]	k4	327
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
tak	tak	k6eAd1	tak
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
v	v	k7c6	v
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
oceněno	oceněn	k2eAgNnSc1d1	oceněno
certifikací	certifikace	k1gFnSc7	certifikace
zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
RIAA	RIAA	kA	RIAA
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
přes	přes	k7c4	přes
500	[number]	k4	500
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
630	[number]	k4	630
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
certifikaci	certifikace	k1gFnSc4	certifikace
platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
milion	milion	k4xCgInSc4	milion
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
distribuci	distribuce	k1gFnSc6	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
propagoval	propagovat	k5eAaImAgMnS	propagovat
i	i	k9	i
svým	svůj	k3xOyFgInSc7	svůj
prvním	první	k4xOgNnSc6	první
sólo	sólo	k2eAgNnSc6d1	sólo
turné	turné	k1gNnSc6	turné
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
–	–	k?	–
The	The	k1gMnSc1	The
Yeezus	Yeezus	k1gMnSc1	Yeezus
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
53	[number]	k4	53
zastavení	zastavení	k1gNnPc2	zastavení
na	na	k7c6	na
území	území	k1gNnSc6	území
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hostující	hostující	k2eAgMnSc1d1	hostující
umělece	umělece	k1gMnSc1	umělece
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
i	i	k9	i
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
The	The	k1gFnSc3	The
Life	Life	k1gNnSc2	Life
of	of	k?	of
Pablo	Pablo	k1gNnSc1	Pablo
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
se	se	k3xPyFc4	se
na	na	k7c6	na
produkci	produkce	k1gFnSc6	produkce
alba	album	k1gNnSc2	album
podílejí	podílet	k5eAaImIp3nP	podílet
Rick	Rick	k1gMnSc1	Rick
Rubin	Rubin	k1gMnSc1	Rubin
a	a	k8xC	a
Q-Tip	Q-Tip	k1gMnSc1	Q-Tip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2014	[number]	k4	2014
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
album	album	k1gNnSc4	album
mohl	moct	k5eAaImAgMnS	moct
vydat	vydat	k5eAaPmF	vydat
v	v	k7c6	v
září	září	k1gNnSc6	září
až	až	k9	až
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
dodržen	dodržen	k2eAgInSc1d1	dodržen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
vydal	vydat	k5eAaPmAgInS	vydat
nový	nový	k2eAgInSc1d1	nový
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
baladu	balada	k1gFnSc4	balada
"	"	kIx"	"
<g/>
Only	Onl	k2eAgFnPc1d1	Onl
One	One	k1gFnPc1	One
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
přizval	přizvat	k5eAaPmAgMnS	přizvat
Paula	Paul	k1gMnSc4	Paul
McCartneyho	McCartney	k1gMnSc4	McCartney
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
piano	piano	k1gNnSc4	piano
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
35	[number]	k4	35
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
singlu	singl	k1gInSc6	singl
"	"	kIx"	"
<g/>
FourFiveSeconds	FourFiveSeconds	k1gInSc1	FourFiveSeconds
<g/>
"	"	kIx"	"
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Rihanny	Rihanna	k1gFnSc2	Rihanna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
písni	píseň	k1gFnSc6	píseň
se	se	k3xPyFc4	se
také	také	k9	také
znovu	znovu	k6eAd1	znovu
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
představil	představit	k5eAaPmAgInS	představit
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
speciálu	speciál	k1gInSc6	speciál
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Night	Nighta	k1gFnPc2	Nighta
Live	Live	k1gNnSc1	Live
40	[number]	k4	40
<g/>
th	th	k?	th
Anniversary	Anniversara	k1gFnSc2	Anniversara
Special	Special	k1gMnSc1	Special
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Wolves	Wolves	k1gInSc1	Wolves
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Vic	Vic	k?	Vic
Mensa	mensa	k1gFnSc1	mensa
&	&	k?	&
Sia	Sia	k1gFnSc2	Sia
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
předávání	předávání	k1gNnSc6	předávání
cen	cena	k1gFnPc2	cena
BRIT	Brit	k1gMnSc1	Brit
Awards	Awards	k1gInSc4	Awards
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
Day	Day	k1gFnSc2	Day
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Day	Day	k1gMnSc1	Day
<g/>
"	"	kIx"	"
natočil	natočit	k5eAaBmAgMnS	natočit
oscarový	oscarový	k2eAgMnSc1d1	oscarový
režisér	režisér	k1gMnSc1	režisér
Steve	Steve	k1gMnSc1	Steve
McQueen	McQueen	k1gInSc1	McQueen
<g/>
;	;	kIx,	;
na	na	k7c4	na
internet	internet	k1gInSc4	internet
unikl	uniknout	k5eAaPmAgMnS	uniknout
až	až	k9	až
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
roce	rok	k1gInSc6	rok
od	od	k7c2	od
natočení	natočení	k1gNnSc2	natočení
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
přes	přes	k7c4	přes
Twitter	Twitter	k1gInSc4	Twitter
oznámil	oznámit	k5eAaPmAgInS	oznámit
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
svého	své	k1gNnSc2	své
sedmého	sedmý	k4xOgNnSc2	sedmý
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
So	So	kA	So
Help	help	k1gInSc1	help
Me	Me	k1gMnSc2	Me
God	God	k1gMnSc2	God
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
změnil	změnit	k5eAaPmAgMnS	změnit
název	název	k1gInSc4	název
alba	album	k1gNnSc2	album
na	na	k7c4	na
SWISH	SWISH	kA	SWISH
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
může	moct	k5eAaImIp3nS	moct
ještě	ještě	k6eAd1	ještě
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
oznámil	oznámit	k5eAaPmAgMnS	oznámit
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
11	[number]	k4	11
<g/>
.	.	kIx.	.
únor	únor	k1gInSc4	únor
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc4	každý
pátek	pátek	k1gInSc4	pátek
zveřejní	zveřejnit	k5eAaPmIp3nP	zveřejnit
novou	nový	k2eAgFnSc4d1	nová
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Facts	Facts	k1gInSc4	Facts
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Real	Real	k1gInSc1	Real
Friends	Friends	k1gInSc1	Friends
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
Dolla	Dolla	k1gMnSc1	Dolla
$	$	kIx~	$
<g/>
ign	ign	k?	ign
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
No	no	k9	no
More	mor	k1gInSc5	mor
Parties	Partiesa	k1gFnPc2	Partiesa
in	in	k?	in
LA	la	k1gNnSc4	la
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
změnil	změnit	k5eAaPmAgInS	změnit
název	název	k1gInSc1	název
na	na	k7c4	na
WAVES	WAVES	kA	WAVES
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
už	už	k6eAd1	už
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
změnu	změna	k1gFnSc4	změna
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
změně	změna	k1gFnSc3	změna
názvu	název	k1gInSc2	název
alba	album	k1gNnSc2	album
si	se	k3xPyFc3	se
prošel	projít	k5eAaPmAgInS	projít
rozepří	rozepře	k1gFnSc7	rozepře
s	s	k7c7	s
rapperem	rapper	k1gMnSc7	rapper
Wizem	Wiz	k1gMnSc7	Wiz
Khalifou	Khalifa	k1gMnSc7	Khalifa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
West	West	k1gMnSc1	West
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
název	název	k1gInSc4	název
rapperovi	rapper	k1gMnSc3	rapper
Maxovi	Max	k1gMnSc3	Max
B.	B.	kA	B.
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
rappery	rappero	k1gNnPc7	rappero
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
výměna	výměna	k1gFnSc1	výměna
názorů	názor	k1gInPc2	názor
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
přes	přes	k7c4	přes
Twitter	Twitter	k1gInSc4	Twitter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
napětí	napětí	k1gNnSc4	napětí
již	již	k9	již
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k9	co
Wiz	Wiz	k1gFnSc7	Wiz
Khalifa	Khalif	k1gMnSc2	Khalif
začal	začít	k5eAaPmAgMnS	začít
žít	žít	k5eAaImF	žít
s	s	k7c7	s
Westovou	Westový	k2eAgFnSc7d1	Westová
bývalou	bývalý	k2eAgFnSc7d1	bývalá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Amber	ambra	k1gFnPc2	ambra
Rose	Ros	k1gMnSc2	Ros
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
února	únor	k1gInSc2	únor
znovu	znovu	k6eAd1	znovu
změnil	změnit	k5eAaPmAgInS	změnit
název	název	k1gInSc1	název
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
uvedl	uvést	k5eAaPmAgMnS	uvést
zkratku	zkratka	k1gFnSc4	zkratka
T.L.O.	T.L.O.	k1gFnSc2	T.L.O.
<g/>
P.	P.	kA	P.
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
význam	význam	k1gInSc4	význam
zkratky	zkratka	k1gFnSc2	zkratka
–	–	k?	–
The	The	k1gMnSc2	The
Life	Lif	k1gMnSc2	Lif
of	of	k?	of
Pablo	Pablo	k1gNnSc1	Pablo
<g/>
,	,	kIx,	,
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
finálním	finální	k2eAgInSc7d1	finální
názvem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
i	i	k9	i
finální	finální	k2eAgInSc1d1	finální
seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
však	však	k9	však
provedl	provést	k5eAaPmAgInS	provést
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
skladeb	skladba	k1gFnPc2	skladba
další	další	k2eAgFnSc2d1	další
úpravy	úprava	k1gFnSc2	úprava
a	a	k8xC	a
k	k	k7c3	k
původně	původně	k6eAd1	původně
deseti	deset	k4xCc3	deset
oznámeným	oznámený	k2eAgFnPc3d1	oznámená
písním	píseň	k1gFnPc3	píseň
přidal	přidat	k5eAaPmAgInS	přidat
dalších	další	k2eAgInPc2d1	další
sedm	sedm	k4xCc4	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Mastering	Mastering	k1gInSc1	Mastering
alba	album	k1gNnSc2	album
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
na	na	k7c4	na
poslední	poslední	k2eAgFnSc4d1	poslední
chvíli	chvíle	k1gFnSc4	chvíle
v	v	k7c4	v
poslední	poslední	k2eAgInPc4d1	poslední
dny	den	k1gInPc4	den
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
exkluzivně	exkluzivně	k6eAd1	exkluzivně
na	na	k7c6	na
streamovací	streamovací	k2eAgFnSc6d1	streamovací
službě	služba	k1gFnSc6	služba
Tidal	Tidal	k1gInSc4	Tidal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
alba	album	k1gNnSc2	album
The	The	k1gFnSc1	The
Life	Life	k1gFnSc1	Life
of	of	k?	of
Pablo	Pablo	k1gNnSc1	Pablo
byla	být	k5eAaImAgFnS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
přehrána	přehrát	k5eAaPmNgFnS	přehrát
na	na	k7c4	na
fashion	fashion	k1gInSc4	fashion
show	show	k1gFnSc2	show
Yeezy	Yeeza	k1gFnPc1	Yeeza
Season	Season	k1gNnSc1	Season
3	[number]	k4	3
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
West	West	k1gMnSc1	West
předvedl	předvést	k5eAaPmAgMnS	předvést
již	již	k6eAd1	již
svou	svůj	k3xOyFgFnSc4	svůj
třetí	třetí	k4xOgFnSc4	třetí
kolekci	kolekce	k1gFnSc4	kolekce
oblečení	oblečení	k1gNnSc2	oblečení
pro	pro	k7c4	pro
Adidas	Adidas	k1gInSc4	Adidas
Original	Original	k1gFnSc2	Original
<g/>
.	.	kIx.	.
</s>
<s>
Show	show	k1gFnSc1	show
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
,	,	kIx,	,
živé	živý	k2eAgNnSc1d1	živé
vysílání	vysílání	k1gNnSc1	vysílání
akce	akce	k1gFnSc2	akce
bylo	být	k5eAaImAgNnS	být
distribuováno	distribuován	k2eAgNnSc1d1	distribuováno
do	do	k7c2	do
vybraných	vybraný	k2eAgNnPc2d1	vybrané
kin	kino	k1gNnPc2	kino
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
akce	akce	k1gFnPc4	akce
účastnilo	účastnit	k5eAaImAgNnS	účastnit
devět	devět	k4xCc1	devět
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pražského	pražský	k2eAgNnSc2d1	Pražské
kina	kino	k1gNnSc2	kino
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Lístky	lístek	k1gInPc4	lístek
na	na	k7c4	na
show	show	k1gNnSc4	show
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardno	k1gNnPc2	Gardno
byly	být	k5eAaImAgFnP	být
vyprodány	vyprodat	k5eAaPmNgFnP	vyprodat
během	během	k7c2	během
deseti	deset	k4xCc2	deset
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
objevily	objevit	k5eAaPmAgFnP	objevit
demo	demo	k2eAgFnPc1d1	demo
verze	verze	k1gFnPc1	verze
některých	některý	k3yIgFnPc2	některý
použitých	použitý	k2eAgFnPc2d1	použitá
i	i	k8xC	i
nepoužitých	použitý	k2eNgFnPc2d1	nepoužitá
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
fází	fáze	k1gFnPc2	fáze
nahrávání	nahrávání	k1gNnSc4	nahrávání
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nepoužitými	použitý	k2eNgFnPc7d1	nepoužitá
písněmi	píseň	k1gFnPc7	píseň
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
například	například	k6eAd1	například
o	o	k7c6	o
dema	dem	k1gInSc2	dem
písní	píseň	k1gFnPc2	píseň
"	"	kIx"	"
<g/>
New	New	k1gFnSc1	New
Angels	Angelsa	k1gFnPc2	Angelsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Fall	Fall	k1gMnSc1	Fall
Out	Out	k1gMnSc1	Out
of	of	k?	of
Heaven	Heavno	k1gNnPc2	Heavno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Mind	Mind	k1gMnSc1	Mind
is	is	k?	is
Powerful	Powerful	k1gInSc1	Powerful
<g/>
"	"	kIx"	"
a	a	k8xC	a
předělanou	předělaný	k2eAgFnSc4d1	předělaná
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Only	Onl	k2eAgFnPc1d1	Onl
One	One	k1gFnPc1	One
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Caroline	Carolin	k1gInSc5	Carolin
Shaw	Shaw	k1gMnSc1	Shaw
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
a	a	k8xC	a
Ty	ty	k3xPp2nSc1	ty
Dolla	Dolla	k1gFnSc1	Dolla
Sign	signum	k1gNnPc2	signum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
The	The	k1gMnSc2	The
Life	Lif	k1gMnSc2	Lif
of	of	k?	of
Pablo	Pablo	k1gNnSc1	Pablo
<g/>
,	,	kIx,	,
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
vydá	vydat	k5eAaPmIp3nS	vydat
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
oznámil	oznámit	k5eAaPmAgInS	oznámit
i	i	k9	i
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
byl	být	k5eAaImAgMnS	být
Turbo	turba	k1gFnSc5	turba
Grafx	Grafx	k1gInSc4	Grafx
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
reflektoval	reflektovat	k5eAaImAgInS	reflektovat
jméno	jméno	k1gNnSc4	jméno
herní	herní	k2eAgFnSc2d1	herní
konzole	konzola	k1gFnSc3	konzola
z	z	k7c2	z
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
TurboGrafx-	TurboGrafx-	k1gFnSc2	TurboGrafx-
<g/>
16	[number]	k4	16
Entertainment	Entertainment	k1gMnSc1	Entertainment
SuperSystem	SuperSyst	k1gMnSc7	SuperSyst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
představil	představit	k5eAaPmAgMnS	představit
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
1OAK	[number]	k4	1OAK
novou	nový	k2eAgFnSc4d1	nová
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
prozatímním	prozatímní	k2eAgInSc7d1	prozatímní
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Closest	Closest	k1gMnSc1	Closest
Thing	Thing	k1gMnSc1	Thing
to	ten	k3xDgNnSc4	ten
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
nakonec	nakonec	k6eAd1	nakonec
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Saint	Saint	k1gInSc1	Saint
Pablo	Pablo	k1gNnSc1	Pablo
<g/>
"	"	kIx"	"
a	a	k8xC	a
West	West	k1gInSc1	West
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
albu	album	k1gNnSc3	album
The	The	k1gFnSc2	The
Life	Life	k1gFnSc3	Life
of	of	k?	of
Pablo	Pablo	k1gNnSc4	Pablo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
alba	album	k1gNnSc2	album
Turbo	turba	k1gFnSc5	turba
Grafx	Grafx	k1gInSc4	Grafx
16	[number]	k4	16
nakonec	nakonec	k6eAd1	nakonec
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
také	také	k9	také
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vydá	vydat	k5eAaPmIp3nS	vydat
nové	nový	k2eAgNnSc1d1	nové
kompilační	kompilační	k2eAgNnSc1d1	kompilační
album	album	k1gNnSc1	album
umělců	umělec	k1gMnPc2	umělec
upsaných	upsaný	k2eAgMnPc2d1	upsaný
u	u	k7c2	u
jeho	on	k3xPp3gInSc2	on
labelu	label	k1gInSc2	label
GOOD	GOOD	kA	GOOD
Music	Musice	k1gInPc2	Musice
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
má	mít	k5eAaImIp3nS	mít
nést	nést	k5eAaImF	nést
název	název	k1gInSc4	název
Cruel	Cruel	k1gMnSc1	Cruel
Winter	Winter	k1gMnSc1	Winter
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Champions	Champions	k1gInSc1	Champions
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Big	Big	k1gFnSc7	Big
Sean	Seana	k1gFnPc2	Seana
a	a	k8xC	a
Desiigner	Desiignra	k1gFnPc2	Desiignra
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Gucci	Gucc	k1gMnSc5	Gucc
Mane	Man	k1gMnSc5	Man
<g/>
,	,	kIx,	,
2	[number]	k4	2
Chainz	Chainza	k1gFnPc2	Chainza
<g/>
,	,	kIx,	,
Travis	Travis	k1gFnSc1	Travis
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
Yo	Yo	k1gMnSc1	Yo
Gotti	Gotť	k1gFnSc2	Gotť
a	a	k8xC	a
Quavo	Quavo	k1gNnSc4	Quavo
<g/>
))	))	k?	))
(	(	kIx(	(
<g/>
71	[number]	k4	71
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
žádný	žádný	k3yNgInSc1	žádný
jiný	jiný	k2eAgInSc1d1	jiný
singl	singl	k1gInSc1	singl
zveřejněný	zveřejněný	k2eAgInSc1d1	zveřejněný
nebyl	být	k5eNaImAgInS	být
a	a	k8xC	a
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
kompilačního	kompilační	k2eAgNnSc2d1	kompilační
alba	album	k1gNnSc2	album
také	také	k9	také
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
zrušil	zrušit	k5eAaPmAgMnS	zrušit
své	svůj	k3xOyFgNnSc4	svůj
turné	turné	k1gNnSc4	turné
Saint	Saint	k1gMnSc1	Saint
Pablo	Pablo	k1gNnSc1	Pablo
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
péči	péče	k1gFnSc6	péče
psychiatra	psychiatr	k1gMnSc2	psychiatr
na	na	k7c6	na
UCLA	UCLA	kA	UCLA
Medical	Medical	k1gMnSc1	Medical
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
strávil	strávit	k5eAaPmAgMnS	strávit
celý	celý	k2eAgInSc4d1	celý
víkend	víkend	k1gInSc4	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byly	být	k5eAaImAgInP	být
projevy	projev	k1gInPc1	projev
psychózy	psychóza	k1gFnSc2	psychóza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnSc1	deprivace
a	a	k8xC	a
dehydratace	dehydratace	k1gFnSc1	dehydratace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
se	se	k3xPyFc4	se
na	na	k7c4	na
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
ze	z	k7c2	z
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ye	Ye	k1gFnSc1	Ye
a	a	k8xC	a
Kids	Kids	k1gInSc1	Kids
See	See	k1gFnSc2	See
Ghosts	Ghostsa	k1gFnPc2	Ghostsa
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
oznámil	oznámit	k5eAaPmAgMnS	oznámit
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
filozofické	filozofický	k2eAgFnSc6d1	filozofická
knize	kniha	k1gFnSc6	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
Break	break	k1gInSc4	break
the	the	k?	the
Simulation	Simulation	k1gInSc1	Simulation
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
knihy	kniha	k1gFnSc2	kniha
chce	chtít	k5eAaImIp3nS	chtít
sdílet	sdílet	k5eAaImF	sdílet
přes	přes	k7c4	přes
příspěvky	příspěvek	k1gInPc4	příspěvek
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
podpořil	podpořit	k5eAaPmAgMnS	podpořit
prezidenta	prezident	k1gMnSc4	prezident
Donalda	Donald	k1gMnSc4	Donald
Trumpa	Trump	k1gMnSc4	Trump
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
vlnu	vlna	k1gFnSc4	vlna
kontrovorze	kontrovorze	k1gFnSc2	kontrovorze
a	a	k8xC	a
kritiku	kritika	k1gFnSc4	kritika
například	například	k6eAd1	například
i	i	k9	i
od	od	k7c2	od
jeho	on	k3xPp3gMnSc2	on
dlouholetého	dlouholetý	k2eAgMnSc2d1	dlouholetý
spolupracovníka	spolupracovník	k1gMnSc2	spolupracovník
<g/>
,	,	kIx,	,
zpěváka	zpěvák	k1gMnSc2	zpěvák
Johna	John	k1gMnSc2	John
Legenda	legenda	k1gFnSc1	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
vyšla	vyjít	k5eAaPmAgFnS	vyjít
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Ye	Ye	k1gFnSc1	Ye
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
People	People	k1gFnSc1	People
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
T.I.	T.I.	k?	T.I.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
oba	dva	k4xCgMnPc1	dva
rappeři	rapper	k1gMnPc1	rapper
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
vydá	vydat	k5eAaPmIp3nS	vydat
dvě	dva	k4xCgNnPc4	dva
nová	nový	k2eAgNnPc4d1	nové
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
osmé	osmý	k4xOgNnSc4	osmý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Ye	Ye	k1gFnSc2	Ye
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
následovalo	následovat	k5eAaImAgNnS	následovat
Kids	Kids	k1gInSc4	Kids
See	See	k1gMnSc2	See
Ghosts	Ghostsa	k1gFnPc2	Ghostsa
<g/>
,	,	kIx,	,
společné	společný	k2eAgNnSc4d1	společné
album	album	k1gNnSc4	album
s	s	k7c7	s
Kidem	Kidum	k1gNnSc7	Kidum
Cudim	Cudima	k1gFnPc2	Cudima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2018	[number]	k4	2018
dále	daleko	k6eAd2	daleko
produkoval	produkovat	k5eAaImAgMnS	produkovat
alba	album	k1gNnPc4	album
rappera	rappero	k1gNnSc2	rappero
Pushy	Pusha	k1gFnSc2	Pusha
T	T	kA	T
-	-	kIx~	-
Daytona	Daytona	k1gFnSc1	Daytona
(	(	kIx(	(
<g/>
vydání	vydání	k1gNnSc1	vydání
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Teyany	Teyana	k1gFnSc2	Teyana
Taylorové	Taylorová	k1gFnSc2	Taylorová
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
a	a	k8xC	a
rappera	rappera	k1gFnSc1	rappera
Nase	Nase	k1gFnSc1	Nase
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
poprvé	poprvé	k6eAd1	poprvé
představil	představit	k5eAaPmAgInS	představit
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ye	Ye	k1gFnSc2	Ye
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
přes	přes	k7c4	přes
živé	živý	k2eAgNnSc4d1	živé
streamování	streamování	k1gNnSc4	streamování
z	z	k7c2	z
přehrávky	přehrávka	k1gFnSc2	přehrávka
alba	album	k1gNnSc2	album
v	v	k7c4	v
Jackson	Jackson	k1gNnSc4	Jackson
Hole	hole	k1gFnSc2	hole
ve	v	k7c6	v
Wyomingu	Wyoming	k1gInSc6	Wyoming
skrze	skrze	k?	skrze
aplikaci	aplikace	k1gFnSc4	aplikace
WAV	WAV	kA	WAV
app	app	k?	app
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
následnému	následný	k2eAgNnSc3d1	následné
leaku	leako	k1gNnSc3	leako
záznamu	záznam	k1gInSc3	záznam
stremu	strema	k1gFnSc4	strema
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc4	album
také	také	k9	také
obratem	obratem	k6eAd1	obratem
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
streamovacích	streamovací	k2eAgFnPc6d1	streamovací
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Přebal	přebal	k1gInSc1	přebal
alba	album	k1gNnSc2	album
a	a	k8xC	a
názvy	název	k1gInPc1	název
písní	píseň	k1gFnPc2	píseň
byly	být	k5eAaImAgInP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
až	až	k9	až
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
sedm	sedm	k4xCc1	sedm
skladeb	skladba	k1gFnPc2	skladba
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
23	[number]	k4	23
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
se	s	k7c7	s
208	[number]	k4	208
000	[number]	k4	000
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
(	(	kIx(	(
<g/>
po	po	k7c6	po
započítání	započítání	k1gNnSc6	započítání
streamů	stream	k1gInPc2	stream
<g/>
)	)	kIx)	)
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k1gInSc1	West
tím	ten	k3xDgNnSc7	ten
vyrovnal	vyrovnat	k5eAaPmAgInS	vyrovnat
Eminemův	Eminemův	k2eAgInSc4d1	Eminemův
rekord	rekord	k1gInSc4	rekord
osmi	osm	k4xCc2	osm
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
jdoucích	jdoucí	k2eAgFnPc2d1	jdoucí
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
debutovala	debutovat	k5eAaBmAgNnP	debutovat
na	na	k7c6	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
streamovací	streamovací	k2eAgFnSc6d1	streamovací
službě	služba	k1gFnSc6	služba
Apple	Apple	kA	Apple
Music	Musice	k1gInPc2	Musice
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
v	v	k7c6	v
přehrávání	přehrávání	k1gNnSc6	přehrávání
v	v	k7c6	v
85	[number]	k4	85
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
v	v	k7c6	v
63	[number]	k4	63
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Singly	singl	k1gInPc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
byly	být	k5eAaImAgFnP	být
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Yikes	Yikes	k1gInSc1	Yikes
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
Mine	minout	k5eAaImIp3nS	minout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
společné	společný	k2eAgNnSc4d1	společné
album	album	k1gNnSc4	album
s	s	k7c7	s
Kidem	Kidum	k1gNnSc7	Kidum
Cudim	Cudim	k1gInSc4	Cudim
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kids	Kids	k1gInSc1	Kids
See	See	k1gFnPc2	See
Ghosts	Ghostsa	k1gFnPc2	Ghostsa
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
opět	opět	k6eAd1	opět
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
přes	přes	k7c4	přes
živé	živý	k2eAgNnSc4d1	živé
streamování	streamování	k1gNnSc4	streamování
z	z	k7c2	z
přehrávky	přehrávka	k1gFnSc2	přehrávka
alba	album	k1gNnSc2	album
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
sedm	sedm	k4xCc1	sedm
skladem	sklad	k1gInSc7	sklad
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
se	s	k7c7	s
142	[number]	k4	142
000	[number]	k4	000
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
(	(	kIx(	(
<g/>
se	s	k7c7	s
započítáním	započítání	k1gNnSc7	započítání
streamů	stream	k1gInPc2	stream
<g/>
)	)	kIx)	)
v	v	k7c4	v
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
nebyl	být	k5eNaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
žádný	žádný	k3yNgInSc1	žádný
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
všech	všecek	k3xTgFnPc2	všecek
sedm	sedm	k4xCc1	sedm
písní	píseň	k1gFnPc2	píseň
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
"	"	kIx"	"
<g/>
Reborn	Reborn	k1gInSc1	Reborn
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
4	[number]	k4	4
<g/>
th	th	k?	th
Dimension	Dimension	k1gInSc1	Dimension
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Louis	Louis	k1gMnSc1	Louis
Prima	prima	k2eAgMnSc1d1	prima
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2018	[number]	k4	2018
vydal	vydat	k5eAaPmAgInS	vydat
společný	společný	k2eAgInSc1d1	společný
singl	singl	k1gInSc1	singl
s	s	k7c7	s
rapperem	rappero	k1gNnSc7	rappero
Lil	lít	k5eAaImAgMnS	lít
Pumpem	Pump	k1gMnSc7	Pump
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
I	i	k9	i
Love	lov	k1gInSc5	lov
It	It	k1gFnSc3	It
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
videoklipem	videoklip	k1gInSc7	videoklip
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
výkonným	výkonný	k2eAgMnSc7d1	výkonný
producentem	producent	k1gMnSc7	producent
byl	být	k5eAaImAgMnS	být
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Spike	Spik	k1gFnSc2	Spik
Jonze	Jonze	k1gFnSc2	Jonze
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
debutoval	debutovat	k5eAaBmAgInS	debutovat
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
svou	svůj	k3xOyFgFnSc4	svůj
aktivitu	aktivita	k1gFnSc4	aktivita
na	na	k7c6	na
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
spekulacím	spekulace	k1gFnPc3	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
propagaci	propagace	k1gFnSc4	propagace
nových	nový	k2eAgInPc2d1	nový
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
oznámil	oznámit	k5eAaPmAgMnS	oznámit
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
albu	album	k1gNnSc6	album
Watch	Watcha	k1gFnPc2	Watcha
the	the	k?	the
Throne	Thron	k1gInSc5	Thron
2	[number]	k4	2
(	(	kIx(	(
<g/>
možné	možný	k2eAgNnSc1d1	možné
pokračování	pokračování	k1gNnSc1	pokračování
společného	společný	k2eAgNnSc2d1	společné
alba	album	k1gNnSc2	album
s	s	k7c7	s
rapperem	rapper	k1gMnSc7	rapper
Jay-Z	Jay-Z	k1gMnSc7	Jay-Z
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
pak	pak	k6eAd1	pak
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
nahrávání	nahrávání	k1gNnSc4	nahrávání
společného	společný	k2eAgNnSc2d1	společné
alba	album	k1gNnSc2	album
s	s	k7c7	s
rapperem	rapper	k1gMnSc7	rapper
Chancem	Chanec	k1gMnSc7	Chanec
the	the	k?	the
Rapperem	Rapper	k1gMnSc7	Rapper
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
nést	nést	k5eAaImF	nést
název	název	k1gInSc4	název
Good	Good	k1gMnSc1	Good
Ass	Ass	k1gMnSc1	Ass
Job	Job	k1gMnSc1	Job
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Yandhi	Yandh	k1gMnSc6	Yandh
(	(	kIx(	(
<g/>
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2018	[number]	k4	2018
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
obrázek	obrázek	k1gInSc1	obrázek
CD	CD	kA	CD
bez	bez	k7c2	bez
obalu	obal	k1gInSc2	obal
s	s	k7c7	s
popiskem	popisek	k1gInSc7	popisek
Yandhi	Yandh	k1gFnSc2	Yandh
a	a	k8xC	a
datem	datum	k1gNnSc7	datum
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
spekulacím	spekulace	k1gFnPc3	spekulace
o	o	k7c6	o
vydání	vydání	k1gNnSc6	vydání
dalšího	další	k1gNnSc2	další
solo	solo	k6eAd1	solo
alba	album	k1gNnSc2	album
(	(	kIx(	(
<g/>
a	a	k8xC	a
možného	možný	k2eAgNnSc2d1	možné
tematického	tematický	k2eAgNnSc2d1	tematické
pokračování	pokračování	k1gNnSc2	pokračování
alba	album	k1gNnSc2	album
Yeezus	Yeezus	k1gInSc1	Yeezus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
stejný	stejný	k2eAgInSc4d1	stejný
vizuální	vizuální	k2eAgInSc4d1	vizuální
koncept	koncept	k1gInSc4	koncept
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	září	k1gNnSc2	září
byl	být	k5eAaImAgInS	být
West	West	k1gInSc1	West
hudebním	hudební	k2eAgMnSc7d1	hudební
hostem	host	k1gMnSc7	host
v	v	k7c6	v
premiérovém	premiérový	k2eAgInSc6d1	premiérový
díle	díl	k1gInSc6	díl
nové	nový	k2eAgFnSc2d1	nová
sezóny	sezóna	k1gFnSc2	sezóna
pořadu	pořad	k1gInSc2	pořad
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Night	Nighta	k1gFnPc2	Nighta
Live	Liv	k1gFnSc2	Liv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Teyanou	Teyana	k1gFnSc7	Teyana
Taylor	Taylor	k1gInSc4	Taylor
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
písní	píseň	k1gFnSc7	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
během	během	k7c2	během
září	září	k1gNnSc2	září
vydáno	vydán	k2eAgNnSc1d1	vydáno
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Kim	Kim	k1gFnSc1	Kim
Kardashian	Kardashian	k1gInSc1	Kardashian
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
album	album	k1gNnSc1	album
bude	být	k5eAaImBp3nS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
až	až	k9	až
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k5eAaPmF	West
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nahrávání	nahrávání	k1gNnSc1	nahrávání
ještě	ještě	k6eAd1	ještě
nebylo	být	k5eNaImAgNnS	být
zcela	zcela	k6eAd1	zcela
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
dokončit	dokončit	k5eAaPmF	dokončit
album	album	k1gNnSc4	album
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
však	však	k9	však
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
termínu	termín	k1gInSc6	termín
nevyšlo	vyjít	k5eNaPmAgNnS	vyjít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
pořádal	pořádat	k5eAaImAgMnS	pořádat
venkovní	venkovní	k2eAgMnSc1d1	venkovní
"	"	kIx"	"
<g/>
Sunday	Sunday	k1gInPc1	Sunday
Services	Servicesa	k1gFnPc2	Servicesa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přehrávky	přehrávka	k1gFnPc4	přehrávka
svých	svůj	k3xOyFgFnPc2	svůj
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
gospelové	gospelový	k2eAgFnSc6d1	gospelová
úpravě	úprava	k1gFnSc6	úprava
se	s	k7c7	s
sborem	sbor	k1gInSc7	sbor
a	a	k8xC	a
vybranými	vybraný	k2eAgMnPc7d1	vybraný
hosty	host	k1gMnPc7	host
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
také	také	k9	také
zažaloval	zažalovat	k5eAaPmAgInS	zažalovat
hudební	hudební	k2eAgFnSc4d1	hudební
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
společnost	společnost	k1gFnSc4	společnost
EMI	EMI	kA	EMI
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
nevýhodná	výhodný	k2eNgFnSc1d1	nevýhodná
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
porušovat	porušovat	k5eAaImF	porušovat
kalifornské	kalifornský	k2eAgNnSc4d1	kalifornské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
kterého	který	k3yRgMnSc2	který
lze	lze	k6eAd1	lze
uzavřít	uzavřít	k5eAaPmF	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c6	na
vlastnění	vlastnění	k1gNnSc6	vlastnění
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k1gInSc1	West
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
EMI	EMI	kA	EMI
ale	ale	k8xC	ale
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
EMI	EMI	kA	EMI
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sedmileté	sedmiletý	k2eAgNnSc1d1	sedmileté
pravidlo	pravidlo	k1gNnSc1	pravidlo
není	být	k5eNaImIp3nS	být
právně	právně	k6eAd1	právně
ukotveno	ukotven	k2eAgNnSc1d1	ukotveno
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
mu	on	k3xPp3gMnSc3	on
také	také	k6eAd1	také
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
předčasného	předčasný	k2eAgInSc2d1	předčasný
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
ho	on	k3xPp3gInSc4	on
společnost	společnost	k1gFnSc1	společnost
EMI	EMI	kA	EMI
zažalovala	zažalovat	k5eAaPmAgFnS	zažalovat
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
kontraktu	kontrakt	k1gInSc2	kontrakt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
West	West	k1gMnSc1	West
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
vyřízení	vyřízení	k1gNnSc4	vyřízení
sporu	spor	k1gInSc2	spor
mimosoudní	mimosoudní	k2eAgFnSc7d1	mimosoudní
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ostatní	ostatní	k2eAgFnPc4d1	ostatní
aktivity	aktivita	k1gFnPc4	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Podnikání	podnikání	k1gNnSc1	podnikání
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
až	až	k9	až
2009	[number]	k4	2009
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
otevřel	otevřít	k5eAaPmAgMnS	otevřít
dvě	dva	k4xCgFnPc4	dva
fast-foodové	fastoodový	k2eAgFnPc4d1	fast-foodová
restaurace	restaurace	k1gFnPc4	restaurace
řetězce	řetězec	k1gInSc2	řetězec
Fatburger	Fatburgra	k1gFnPc2	Fatburgra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
provozní	provozní	k2eAgFnSc1d1	provozní
firma	firma	k1gFnSc1	firma
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
KW	kw	kA	kw
Foods	Foods	k1gInSc1	Foods
LLC	LLC	kA	LLC
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zakoupená	zakoupený	k2eAgNnPc4d1	zakoupené
práva	právo	k1gNnPc4	právo
pro	pro	k7c4	pro
celkem	celkem	k6eAd1	celkem
deset	deset	k4xCc4	deset
poboček	pobočka	k1gFnPc2	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
otevřených	otevřený	k2eAgFnPc2d1	otevřená
však	však	k9	však
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tedy	tedy	k9	tedy
provozuje	provozovat	k5eAaImIp3nS	provozovat
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
pobočku	pobočka	k1gFnSc4	pobočka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Design	design	k1gInSc1	design
a	a	k8xC	a
móda	móda	k1gFnSc1	móda
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
kolekci	kolekce	k1gFnSc4	kolekce
bot	bota	k1gFnPc2	bota
pro	pro	k7c4	pro
Nike	Nike	k1gFnSc4	Nike
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Air	Air	k1gFnSc4	Air
Yeezy	Yeeza	k1gFnSc2	Yeeza
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
se	se	k3xPyFc4	se
prodával	prodávat	k5eAaImAgInS	prodávat
za	za	k7c4	za
215	[number]	k4	215
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
tři	tři	k4xCgInPc1	tři
tisíce	tisíc	k4xCgInPc1	tisíc
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
první	první	k4xOgFnSc2	první
kolekce	kolekce	k1gFnSc2	kolekce
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
díky	díky	k7c3	díky
limitované	limitovaný	k2eAgFnSc3d1	limitovaná
sérii	série	k1gFnSc3	série
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
až	až	k6eAd1	až
na	na	k7c6	na
deseti	deset	k4xCc6	deset
násobek	násobek	k1gInSc4	násobek
původní	původní	k2eAgFnSc2d1	původní
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc2	vedení
firmy	firma	k1gFnSc2	firma
Nike	Nike	k1gFnSc2	Nike
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
novou	nový	k2eAgFnSc4d1	nová
sérii	série	k1gFnSc4	série
<g/>
.	.	kIx.	.
</s>
<s>
Kolekce	kolekce	k1gFnSc1	kolekce
Air	Air	k1gFnSc2	Air
Yeezy	Yeeza	k1gFnSc2	Yeeza
II	II	kA	II
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
druhá	druhý	k4xOgFnSc1	druhý
kolekce	kolekce	k1gFnSc1	kolekce
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
pět	pět	k4xCc4	pět
set	sto	k4xCgNnPc2	sto
párů	pár	k1gInPc2	pár
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
barevných	barevný	k2eAgFnPc2d1	barevná
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
omezenému	omezený	k2eAgInSc3d1	omezený
počtu	počet	k1gInSc3	počet
kusů	kus	k1gInPc2	kus
prodal	prodat	k5eAaPmAgInS	prodat
i	i	k9	i
za	za	k7c4	za
devadesát	devadesát	k4xCc4	devadesát
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
variant	varianta	k1gFnPc2	varianta
s	s	k7c7	s
názvem	název	k1gInSc7	název
Red	Red	k1gFnSc2	Red
Octobers	Octobers	k1gInSc1	Octobers
byla	být	k5eAaImAgFnS	být
nečekaně	nečekaně	k6eAd1	nečekaně
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
edice	edice	k1gFnSc1	edice
byla	být	k5eAaImAgFnS	být
vyprodána	vyprodat	k5eAaPmNgFnS	vyprodat
za	za	k7c4	za
jedenáct	jedenáct	k4xCc4	jedenáct
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
kolekci	kolekce	k1gFnSc6	kolekce
bot	bota	k1gFnPc2	bota
pro	pro	k7c4	pro
luxusní	luxusní	k2eAgFnSc4d1	luxusní
módní	módní	k2eAgFnSc4d1	módní
značku	značka	k1gFnSc4	značka
Louis	louis	k1gInSc2	louis
Vuitton	Vuitton	k1gInSc1	Vuitton
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
módní	módní	k2eAgFnSc4d1	módní
značku	značka	k1gFnSc4	značka
dámského	dámský	k2eAgNnSc2d1	dámské
oblečení	oblečení	k1gNnSc2	oblečení
DW	DW	kA	DW
Kanye	Kanye	k1gFnSc4	Kanye
West	Westa	k1gFnPc2	Westa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
kolekci	kolekce	k1gFnSc4	kolekce
představil	představit	k5eAaPmAgMnS	představit
na	na	k7c4	na
Paris	Paris	k1gMnSc1	Paris
Fashion	Fashion	k1gInSc1	Fashion
Week	Week	k1gInSc1	Week
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
negativně	negativně	k6eAd1	negativně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
představil	představit	k5eAaPmAgMnS	představit
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
kolekci	kolekce	k1gFnSc4	kolekce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
již	již	k6eAd1	již
byla	být	k5eAaImAgNnP	být
přijata	přijmout	k5eAaPmNgNnP	přijmout
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
založil	založit	k5eAaPmAgMnS	založit
designérskou	designérský	k2eAgFnSc4d1	designérská
společnost	společnost	k1gFnSc4	společnost
Donda	Dondo	k1gNnSc2	Dondo
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
profesionálů	profesionál	k1gMnPc2	profesionál
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
sféry	sféra	k1gFnPc4	sféra
designu	design	k1gInSc2	design
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Adidas	Adidasa	k1gFnPc2	Adidasa
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
celou	celá	k1gFnSc4	celá
jednu	jeden	k4xCgFnSc4	jeden
kolekci	kolekce	k1gFnSc4	kolekce
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
Adidas	Adidasa	k1gFnPc2	Adidasa
Originals	Originalsa	k1gFnPc2	Originalsa
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
představena	představit	k5eAaPmNgFnS	představit
již	již	k6eAd1	již
v	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
termín	termín	k1gInSc1	termín
nebyl	být	k5eNaImAgInS	být
dodržen	dodržet	k5eAaPmNgInS	dodržet
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k9	ani
ten	ten	k3xDgInSc1	ten
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
splněn	splnit	k5eAaPmNgInS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
výrazněji	výrazně	k6eAd2	výrazně
mluvilo	mluvit	k5eAaImAgNnS	mluvit
jen	jen	k9	jen
o	o	k7c6	o
botách	bota	k1gFnPc6	bota
s	s	k7c7	s
pracovním	pracovní	k2eAgInSc7d1	pracovní
názvem	název	k1gInSc7	název
YEEZi	YEEZ	k1gFnSc2	YEEZ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Westova	Westův	k2eAgFnSc1d1	Westova
nová	nový	k2eAgFnSc1d1	nová
kolekce	kolekce	k1gFnSc1	kolekce
s	s	k7c7	s
názvem	název	k1gInSc7	název
Yeezy	Yeeza	k1gFnSc2	Yeeza
Season	Season	k1gInSc1	Season
1	[number]	k4	1
bude	být	k5eAaImBp3nS	být
vydána	vydat	k5eAaPmNgFnS	vydat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
nové	nový	k2eAgFnPc1d1	nová
boty	bota	k1gFnPc1	bota
s	s	k7c7	s
názvem	název	k1gInSc7	název
Yeezy	Yeeza	k1gFnSc2	Yeeza
750	[number]	k4	750
Boost	Boost	k1gMnSc1	Boost
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
limitovanou	limitovaný	k2eAgFnSc4d1	limitovaná
edici	edice	k1gFnSc4	edice
o	o	k7c6	o
devíti	devět	k4xCc6	devět
tisících	tisící	k4xOgInPc6	tisící
párech	pár	k1gInPc6	pár
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
za	za	k7c2	za
350	[number]	k4	350
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
zvolila	zvolit	k5eAaPmAgFnS	zvolit
společnost	společnost	k1gFnSc1	společnost
Adidas	Adidasa	k1gFnPc2	Adidasa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
celkově	celkově	k6eAd1	celkově
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
luxusní	luxusní	k2eAgFnSc4d1	luxusní
módní	módní	k2eAgFnSc4d1	módní
kolekci	kolekce	k1gFnSc4	kolekce
s	s	k7c7	s
cenami	cena	k1gFnPc7	cena
od	od	k7c2	od
430	[number]	k4	430
do	do	k7c2	do
4	[number]	k4	4
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
značky	značka	k1gFnSc2	značka
Adidas	Adidas	k1gInSc1	Adidas
vydal	vydat	k5eAaPmAgInS	vydat
i	i	k9	i
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
modelů	model	k1gInPc2	model
tenisek	teniska	k1gFnPc2	teniska
Yeezy	Yeeza	k1gFnSc2	Yeeza
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
lukrativní	lukrativní	k2eAgFnPc4d1	lukrativní
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
rozprodané	rozprodaný	k2eAgNnSc4d1	rozprodané
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Charita	charita	k1gFnSc1	charita
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
nadaci	nadace	k1gFnSc4	nadace
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Kanye	Kanye	k1gFnSc4	Kanye
West	West	k2eAgInSc4d1	West
Foundation	Foundation	k1gInSc4	Foundation
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
zvanou	zvaný	k2eAgFnSc4d1	zvaná
"	"	kIx"	"
<g/>
Donda	Donda	k1gMnSc1	Donda
West	West	k1gMnSc1	West
Foundation	Foundation	k1gInSc1	Foundation
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
pomoci	pomoct	k5eAaPmF	pomoct
menšinám	menšina	k1gFnPc3	menšina
v	v	k7c6	v
USA	USA	kA	USA
se	s	k7c7	s
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Kanye	Kanye	k5eAaImRp2nP	Kanye
West	West	k1gInSc4	West
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vložil	vložit	k5eAaPmAgInS	vložit
500	[number]	k4	500
000	[number]	k4	000
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
ukončila	ukončit	k5eAaPmAgFnS	ukončit
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
mnoha	mnoho	k4c3	mnoho
benefičním	benefiční	k2eAgInPc3d1	benefiční
koncertům	koncert	k1gInPc3	koncert
a	a	k8xC	a
akcím	akce	k1gFnPc3	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Politika	politikum	k1gNnSc2	politikum
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
USA	USA	kA	USA
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
Učinil	učinit	k5eAaPmAgInS	učinit
tak	tak	k6eAd1	tak
během	během	k7c2	během
proslovu	proslov	k1gInSc2	proslov
na	na	k7c4	na
předávání	předávání	k1gNnSc4	předávání
cen	cena	k1gFnPc2	cena
MTV	MTV	kA	MTV
Video	video	k1gNnSc1	video
Music	Musice	k1gInPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2017	[number]	k4	2017
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
republikánským	republikánský	k2eAgMnSc7d1	republikánský
prezidentem	prezident	k1gMnSc7	prezident
Donaldem	Donald	k1gMnSc7	Donald
Trumpem	Trump	k1gMnSc7	Trump
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2018	[number]	k4	2018
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
oficiálně	oficiálně	k6eAd1	oficiálně
podpořil	podpořit	k5eAaPmAgMnS	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
tweetů	tweet	k1gMnPc2	tweet
později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Trumpovi	Trump	k1gMnSc6	Trump
vidí	vidět	k5eAaImIp3nS	vidět
mnoho	mnoho	k6eAd1	mnoho
dobrého	dobrý	k2eAgInSc2d1	dobrý
a	a	k8xC	a
přínosného	přínosný	k2eAgInSc2d1	přínosný
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vyjasnil	vyjasnit	k5eAaPmAgMnS	vyjasnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
necítí	cítit	k5eNaImIp3nS	cítit
jako	jako	k9	jako
republikán	republikán	k1gMnSc1	republikán
a	a	k8xC	a
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
Trumpa	Trumpa	k1gFnSc1	Trumpa
<g/>
,	,	kIx,	,
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
i	i	k9	i
demokratku	demokratka	k1gFnSc4	demokratka
Hillary	Hillara	k1gFnSc2	Hillara
Clintonovou	Clintonová	k1gFnSc7	Clintonová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
tweetnul	tweetnout	k5eAaPmAgMnS	tweetnout
i	i	k9	i
rok	rok	k1gInSc4	rok
"	"	kIx"	"
<g/>
2024	[number]	k4	2024
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
spekulacím	spekulace	k1gFnPc3	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
kandidovat	kandidovat	k5eAaImF	kandidovat
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
Trumpovi	Trump	k1gMnSc3	Trump
kandidovat	kandidovat	k5eAaImF	kandidovat
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
na	na	k7c6	na
několika	několik	k4yIc2	několik
veřejných	veřejný	k2eAgNnPc6d1	veřejné
vystoupeních	vystoupení	k1gNnPc6	vystoupení
nosil	nosit	k5eAaImAgMnS	nosit
červenou	červený	k2eAgFnSc4d1	červená
čepici	čepice	k1gFnSc4	čepice
se	s	k7c7	s
sloganem	slogan	k1gInSc7	slogan
z	z	k7c2	z
Trumpovy	Trumpův	k2eAgFnSc2d1	Trumpova
kampaně	kampaň	k1gFnSc2	kampaň
"	"	kIx"	"
<g/>
Make	Make	k1gInSc1	Make
America	Americ	k1gInSc2	Americ
Great	Great	k2eAgInSc1d1	Great
Again	Again	k1gInSc1	Again
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
byl	být	k5eAaImAgMnS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
do	do	k7c2	do
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
na	na	k7c4	na
pracovní	pracovní	k2eAgInSc4d1	pracovní
oběd	oběd	k1gInSc4	oběd
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rozhovoru	rozhovor	k1gInSc2	rozhovor
se	se	k3xPyFc4	se
vyznal	vyznat	k5eAaPmAgMnS	vyznat
z	z	k7c2	z
obdivu	obdiv	k1gInSc2	obdiv
k	k	k7c3	k
Trumpovi	Trump	k1gMnSc3	Trump
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
politice	politika	k1gFnSc3	politika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
staví	stavit	k5eAaPmIp3nS	stavit
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
Američany	Američan	k1gMnPc4	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nošení	nošení	k1gNnSc4	nošení
čepice	čepice	k1gFnSc2	čepice
z	z	k7c2	z
Trumpovy	Trumpův	k2eAgFnSc2d1	Trumpova
kampaně	kampaň	k1gFnSc2	kampaň
mu	on	k3xPp3gMnSc3	on
dodává	dodávat	k5eAaImIp3nS	dodávat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
prezidentově	prezidentův	k2eAgFnSc3d1	prezidentova
kritice	kritika	k1gFnSc3	kritika
liberálních	liberální	k2eAgNnPc2d1	liberální
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
poněkolikáté	poněkolikáté	k6eAd1	poněkolikáté
zkritizoval	zkritizovat	k5eAaPmAgMnS	zkritizovat
obecnou	obecný	k2eAgFnSc4d1	obecná
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
jen	jen	k9	jen
Demokraty	demokrat	k1gMnPc4	demokrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vztahy	vztah	k1gInPc1	vztah
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2002	[number]	k4	2002
a	a	k8xC	a
2008	[number]	k4	2008
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
s	s	k7c7	s
designérkou	designérka	k1gFnSc7	designérka
Alexis	Alexis	k1gFnPc2	Alexis
Phifer	Phifra	k1gFnPc2	Phifra
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
byl	být	k5eAaImAgInS	být
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
zasnouben	zasnouben	k2eAgInSc1d1	zasnouben
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2008	[number]	k4	2008
a	a	k8xC	a
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
s	s	k7c7	s
modelkou	modelka	k1gFnSc7	modelka
Amber	ambra	k1gFnPc2	ambra
Rose	Ros	k1gMnSc2	Ros
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
Kim	Kim	k1gFnSc7	Kim
Kardashian	Kardashiana	k1gFnPc2	Kardashiana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
porodila	porodit	k5eAaPmAgFnS	porodit
holčičku	holčička	k1gFnSc4	holčička
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
North	North	k1gInSc4	North
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
pět	pět	k4xCc1	pět
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
řádným	řádný	k2eAgInSc7d1	řádný
termínem	termín	k1gInSc7	termín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zasnoubili	zasnoubit	k5eAaPmAgMnP	zasnoubit
<g/>
.	.	kIx.	.
</s>
<s>
Svatební	svatební	k2eAgInSc1d1	svatební
obřad	obřad	k1gInSc1	obřad
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
ve	v	k7c6	v
Forte	forte	k1gNnSc6	forte
di	di	k?	di
Belvedere	Belveder	k1gMnSc5	Belveder
<g/>
,	,	kIx,	,
ve	v	k7c4	v
Florencii	Florencie	k1gFnSc4	Florencie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2015	[number]	k4	2015
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodilo	narodit	k5eAaPmAgNnS	narodit
druhé	druhý	k4xOgNnSc1	druhý
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
chlapec	chlapec	k1gMnSc1	chlapec
Saint	Saint	k1gMnSc1	Saint
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
se	se	k3xPyFc4	se
páru	pár	k1gInSc6	pár
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
náhradního	náhradní	k2eAgNnSc2d1	náhradní
mateřství	mateřství	k1gNnSc2	mateřství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
složení	složení	k1gNnSc6	složení
maturity	maturita	k1gFnSc2	maturita
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
stipendium	stipendium	k1gNnSc1	stipendium
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
soukromé	soukromý	k2eAgFnSc6d1	soukromá
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
American	American	k1gMnSc1	American
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Art	Art	k1gFnSc2	Art
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
hodiny	hodina	k1gFnPc4	hodina
malování	malování	k1gNnSc2	malování
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
angličtinu	angličtina	k1gFnSc4	angličtina
na	na	k7c4	na
Chicago	Chicago	k1gNnSc4	Chicago
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k1gInSc1	West
jako	jako	k8xC	jako
odůvodnění	odůvodnění	k1gNnSc1	odůvodnění
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezvládal	zvládat	k5eNaImAgMnS	zvládat
sladit	sladit	k5eAaPmF	sladit
studium	studium	k1gNnSc4	studium
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ambicí	ambice	k1gFnSc7	ambice
prosadit	prosadit	k5eAaPmF	prosadit
se	se	k3xPyFc4	se
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
získal	získat	k5eAaPmAgInS	získat
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
krásných	krásný	k2eAgNnPc2d1	krásné
umění	umění	k1gNnSc2	umění
od	od	k7c2	od
soukromé	soukromý	k2eAgFnSc2d1	soukromá
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
School	School	k1gInSc1	School
of	of	k?	of
the	the	k?	the
Art	Art	k1gFnSc2	Art
Institute	institut	k1gInSc5	institut
of	of	k?	of
Chicago	Chicago	k1gNnSc4	Chicago
(	(	kIx(	(
<g/>
SAIC	SAIC	kA	SAIC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zařadil	zařadit	k5eAaPmAgInS	zařadit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
mezi	mezi	k7c4	mezi
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gInSc4	Jeff
Koons	Koonsa	k1gFnPc2	Koonsa
<g/>
,	,	kIx,	,
Patti	Patt	k1gMnPc1	Patt
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
Ed	Ed	k1gMnSc1	Ed
Harris	Harris	k1gFnSc2	Harris
a	a	k8xC	a
Marina	Marina	k1gFnSc1	Marina
Abramović	Abramović	k1gMnPc2	Abramović
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
také	také	k9	také
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
čestný	čestný	k2eAgInSc4d1	čestný
doktorát	doktorát	k1gInSc4	doktorát
od	od	k7c2	od
SAIC	SAIC	kA	SAIC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Problémy	problém	k1gInPc1	problém
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
žalován	žalovat	k5eAaImNgInS	žalovat
Evelem	Evel	k1gMnSc7	Evel
Knievelem	Knievel	k1gMnSc7	Knievel
za	za	k7c2	za
zneužití	zneužití	k1gNnSc2	zneužití
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Touch	Touch	k1gInSc1	Touch
the	the	k?	the
Sky	Sky	k1gFnSc2	Sky
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
urovnán	urovnat	k5eAaPmNgInS	urovnat
mimosoudně	mimosoudně	k6eAd1	mimosoudně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
2008	[number]	k4	2008
byli	být	k5eAaImAgMnP	být
West	West	k1gMnSc1	West
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bodyguard	bodyguard	k1gMnSc1	bodyguard
Don	Don	k1gMnSc1	Don
Crowley	Crowlea	k1gFnSc2	Crowlea
zatčeni	zatčen	k2eAgMnPc1d1	zatčen
při	při	k7c6	při
incidentu	incident	k1gInSc6	incident
s	s	k7c7	s
paparazzi	paparazh	k1gMnPc1	paparazh
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc4	Airport
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
na	na	k7c4	na
kauci	kauce	k1gFnSc4	kauce
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
obvinění	obvinění	k1gNnSc1	obvinění
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
nedostalo	dostat	k5eNaPmAgNnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
zadržen	zadržet	k5eAaPmNgMnS	zadržet
po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
incidentu	incident	k1gInSc6	incident
s	s	k7c7	s
paparazzi	paparazze	k1gFnSc3	paparazze
fotografem	fotograf	k1gMnSc7	fotograf
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
městě	město	k1gNnSc6	město
Gateshead	Gateshead	k1gInSc1	Gateshead
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
okamžitě	okamžitě	k6eAd1	okamžitě
bez	bez	k7c2	bez
obvinění	obvinění	k1gNnSc2	obvinění
propuštěn	propuštěn	k2eAgInSc4d1	propuštěn
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
LAX	LAX	kA	LAX
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
paparazzi	paparazze	k1gFnSc3	paparazze
fotografem	fotograf	k1gMnSc7	fotograf
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vytrhnout	vytrhnout	k5eAaPmF	vytrhnout
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
krádež	krádež	k1gFnSc4	krádež
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
soudu	soud	k1gInSc2	soud
byl	být	k5eAaImAgInS	být
však	však	k9	však
viněn	vinit	k5eAaImNgInS	vinit
z	z	k7c2	z
přečinu	přečin	k1gInSc2	přečin
ublížení	ublížení	k1gNnSc2	ublížení
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
shledán	shledat	k5eAaPmNgInS	shledat
nevinným	vinný	k2eNgInSc7d1	nevinný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protistrana	protistrana	k1gFnSc1	protistrana
se	se	k3xPyFc4	se
odvolala	odvolat	k5eAaPmAgFnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
Kanye	Kanye	k1gInSc1	Kanye
West	Westa	k1gFnPc2	Westa
odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
ke	k	k7c3	k
dvouleté	dvouletý	k2eAgFnSc3d1	dvouletá
podmínce	podmínka	k1gFnSc3	podmínka
<g/>
,	,	kIx,	,
povinné	povinný	k2eAgFnSc6d1	povinná
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
kurzech	kurz	k1gInPc6	kurz
zvládání	zvládání	k1gNnSc2	zvládání
hněvu	hněv	k1gInSc2	hněv
a	a	k8xC	a
ke	k	k7c3	k
250	[number]	k4	250
hodinám	hodina	k1gFnPc3	hodina
obecně	obecně	k6eAd1	obecně
prospěšných	prospěšný	k2eAgFnPc2d1	prospěšná
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
mu	on	k3xPp3gMnSc3	on
hrozilo	hrozit	k5eAaImAgNnS	hrozit
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
napadení	napadení	k1gNnSc2	napadení
osmnáctiletého	osmnáctiletý	k2eAgMnSc2d1	osmnáctiletý
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
osobně	osobně	k6eAd1	osobně
vulgárně	vulgárně	k6eAd1	vulgárně
urazit	urazit	k5eAaPmF	urazit
jeho	jeho	k3xOp3gFnSc4	jeho
snoubenku	snoubenka	k1gFnSc4	snoubenka
Kim	Kim	k1gMnSc1	Kim
Kardashian	Kardashian	k1gMnSc1	Kardashian
<g/>
.	.	kIx.	.
</s>
<s>
Incident	incident	k1gInSc1	incident
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
čekárně	čekárna	k1gFnSc6	čekárna
chiropraktika	chiropraktik	k1gMnSc2	chiropraktik
v	v	k7c4	v
Beverly	Beverla	k1gFnPc4	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
,	,	kIx,	,
Kanye	Kanye	k1gFnSc1	Kanye
West	West	k1gInSc1	West
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
spor	spor	k1gInSc1	spor
vyřešit	vyřešit	k5eAaPmF	vyřešit
mimosoudně	mimosoudně	k6eAd1	mimosoudně
<g/>
,	,	kIx,	,
když	když	k8xS	když
muži	muž	k1gMnPc7	muž
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
odškodnění	odškodnění	k1gNnSc4	odškodnění
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
250	[number]	k4	250
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Duševní	duševní	k2eAgNnSc4d1	duševní
zdraví	zdraví	k1gNnSc4	zdraví
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
písních	píseň	k1gFnPc6	píseň
přiznal	přiznat	k5eAaPmAgMnS	přiznat
k	k	k7c3	k
užívání	užívání	k1gNnSc3	užívání
antidepresiva	antidepresivum	k1gNnSc2	antidepresivum
Escitalopramu	Escitalopram	k1gInSc2	Escitalopram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
zažil	zažít	k5eAaPmAgMnS	zažít
během	během	k7c2	během
vystoupení	vystoupení	k1gNnSc2	vystoupení
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
koncertu	koncert	k1gInSc6	koncert
psychotickou	psychotický	k2eAgFnSc4d1	psychotická
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yIgFnSc2	který
například	například	k6eAd1	například
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jay-Z	Jay-Z	k1gMnSc5	Jay-Z
<g/>
,	,	kIx,	,
zavolej	zavolat	k5eAaPmRp2nS	zavolat
mi	já	k3xPp1nSc3	já
kámo	kámo	k1gNnSc4	kámo
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
mi	já	k3xPp1nSc3	já
nevoláš	volat	k5eNaImIp2nS	volat
<g/>
?	?	kIx.	?
</s>
<s>
Jay-Z	Jay-Z	k?	Jay-Z
<g/>
,	,	kIx,	,
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
máš	mít	k5eAaImIp2nS	mít
zabijáky	zabiják	k1gInPc4	zabiják
<g/>
.	.	kIx.	.
</s>
<s>
Prosím	prosit	k5eAaImIp1nS	prosit
<g/>
,	,	kIx,	,
neposílej	posílat	k5eNaImRp2nS	posílat
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
<g/>
.	.	kIx.	.
</s>
<s>
Prosím	prosit	k5eAaImIp1nS	prosit
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
chlap	chlap	k1gMnSc1	chlap
a	a	k8xC	a
zavolej	zavolat	k5eAaPmRp2nS	zavolat
mi	já	k3xPp1nSc3	já
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
předsčasně	předsčasně	k6eAd1	předsčasně
ukončen	ukončit	k5eAaPmNgInS	ukončit
a	a	k8xC	a
West	West	k1gInSc1	West
byl	být	k5eAaImAgInS	být
druhého	druhý	k4xOgMnSc4	druhý
dne	den	k1gInSc2	den
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
v	v	k7c6	v
UCLA	UCLA	kA	UCLA
Medical	Medical	k1gFnSc1	Medical
Center	centrum	k1gNnPc2	centrum
s	s	k7c7	s
příznaky	příznak	k1gInPc7	příznak
halucinací	halucinace	k1gFnPc2	halucinace
a	a	k8xC	a
paranoie	paranoia	k1gFnSc2	paranoia
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
oficiální	oficiální	k2eAgInPc1d1	oficiální
důvody	důvod	k1gInPc1	důvod
ukončení	ukončení	k1gNnSc3	ukončení
koncertu	koncert	k1gInSc2	koncert
byly	být	k5eAaImAgFnP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnPc1	deprivace
a	a	k8xC	a
dehydratace	dehydratace	k1gFnPc1	dehydratace
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
dalších	další	k2eAgInPc2d1	další
21	[number]	k4	21
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tour	toura	k1gFnPc2	toura
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
škodu	škoda	k1gFnSc4	škoda
využita	využit	k2eAgFnSc1d1	využita
Westova	Westův	k2eAgFnSc1d1	Westova
popjistka	popjistka	k1gFnSc1	popjistka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
dohady	dohad	k1gInPc4	dohad
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvody	důvod	k1gInPc1	důvod
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
závažnější	závažný	k2eAgInPc1d2	závažnější
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
po	po	k7c6	po
deseti	deset	k4xCc6	deset
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
interview	interview	k1gNnSc6	interview
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
stal	stát	k5eAaPmAgMnS	stát
závislým	závislý	k2eAgMnSc7d1	závislý
na	na	k7c6	na
opiátech	opiát	k1gInPc6	opiát
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
předepsány	předepsat	k5eAaPmNgInP	předepsat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
liposukci	liposukce	k1gFnSc4	liposukce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
a	a	k9	a
další	další	k2eAgInPc1d1	další
zážitky	zážitek	k1gInPc1	zážitek
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
spouštěčem	spouštěč	k1gInSc7	spouštěč
pro	pro	k7c4	pro
projevy	projev	k1gInPc4	projev
duševní	duševní	k2eAgFnSc2d1	duševní
poruchy	porucha	k1gFnSc2	porucha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
impulzivně	impulzivně	k6eAd1	impulzivně
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ye	Ye	k1gFnSc2	Ye
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nP	trpět
bipolární	bipolární	k2eAgFnSc7d1	bipolární
poruchou	porucha	k1gFnSc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tuto	tento	k3xDgFnSc4	tento
diagnózu	diagnóza	k1gFnSc4	diagnóza
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
rozhovorech	rozhovor	k1gInPc6	rozhovor
zase	zase	k9	zase
popřel	popřít	k5eAaPmAgInS	popřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
College	Colleg	k1gFnSc2	Colleg
Dropout	Dropout	k1gMnSc1	Dropout
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Late	lat	k1gInSc5	lat
Registration	Registration	k1gInSc4	Registration
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Graduation	Graduation	k1gInSc1	Graduation
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
808	[number]	k4	808
<g/>
s	s	k7c7	s
&	&	k?	&
Heartbreak	Heartbreak	k1gInSc1	Heartbreak
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
My	my	k3xPp1nPc1	my
Beautiful	Beautifula	k1gFnPc2	Beautifula
Dark	Dark	k1gMnSc1	Dark
Twisted	Twisted	k1gMnSc1	Twisted
Fantasy	fantas	k1gInPc7	fantas
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Yeezus	Yeezus	k1gInSc1	Yeezus
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Life	Life	k1gFnPc2	Life
of	of	k?	of
Pablo	Pablo	k1gNnSc4	Pablo
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
:	:	kIx,	:
Ye	Ye	k1gFnSc1	Ye
</s>
</p>
<p>
<s>
===	===	k?	===
Spolupráce	spolupráce	k1gFnSc1	spolupráce
===	===	k?	===
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Watch	Watch	k1gInSc1	Watch
the	the	k?	the
Throne	Thron	k1gInSc5	Thron
(	(	kIx(	(
<g/>
s	s	k7c7	s
Jay-Z	Jay-Z	k1gFnSc7	Jay-Z
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Cruel	Cruel	k1gMnSc1	Cruel
Summer	Summer	k1gMnSc1	Summer
(	(	kIx(	(
<g/>
s	s	k7c7	s
GOOD	GOOD	kA	GOOD
Music	Musice	k1gInPc2	Musice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
<g/>
:	:	kIx,	:
Kids	Kids	k1gInSc1	Kids
See	See	k1gFnSc2	See
Ghosts	Ghostsa	k1gFnPc2	Ghostsa
(	(	kIx(	(
<g/>
s	s	k7c7	s
Kid	Kid	k1gFnSc7	Kid
Cudi	Cud	k1gFnSc2	Cud
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
duo	duo	k1gNnSc1	duo
Kids	Kidsa	k1gFnPc2	Kidsa
See	See	k1gFnPc2	See
Ghosts	Ghosts	k1gInSc1	Ghosts
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Samostatný	samostatný	k2eAgInSc1d1	samostatný
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Seznam	seznam	k1gInSc1	seznam
ocenění	ocenění	k1gNnSc1	ocenění
Kanye	Kany	k1gMnSc2	Kany
Westa	West	k1gMnSc2	West
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kanye	Kany	k1gInSc2	Kany
West	Westa	k1gFnPc2	Westa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
fanstránka	fanstránka	k1gFnSc1	fanstránka
KanyeWest	KanyeWest	k1gFnSc1	KanyeWest
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
MySpace	MySpace	k1gFnSc1	MySpace
profil	profil	k1gInSc1	profil
</s>
</p>
