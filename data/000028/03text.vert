<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
ptactva	ptactvo	k1gNnSc2	ptactvo
je	být	k5eAaImIp3nS	být
slaven	slaven	k2eAgMnSc1d1	slaven
každoročně	každoročně	k6eAd1	každoročně
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
ve	v	k7c4	v
výroční	výroční	k2eAgInSc4d1	výroční
den	den	k1gInSc4	den
podepsání	podepsání	k1gNnSc2	podepsání
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
Konvence	konvence	k1gFnSc2	konvence
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
užitečného	užitečný	k2eAgNnSc2d1	užitečné
ptactva	ptactvo	k1gNnSc2	ptactvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
slaví	slavit	k5eAaImIp3nP	slavit
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Luděk	Luděk	k1gMnSc1	Luděk
Schreib	Schreib	k1gMnSc1	Schreib
<g/>
:	:	kIx,	:
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
ptactva	ptactvo	k1gNnSc2	ptactvo
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
<g/>
,	,	kIx,	,
Informační	informační	k2eAgInSc1d1	informační
server	server	k1gInSc1	server
pražské	pražský	k2eAgFnSc2d1	Pražská
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
Významné	významný	k2eAgInPc4d1	významný
dny	den	k1gInPc4	den
pro	pro	k7c4	pro
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
kalendář	kalendář	k1gInSc4	kalendář
na	na	k7c6	na
webu	web	k1gInSc6	web
Informačního	informační	k2eAgNnSc2d1	informační
centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
Den	dna	k1gFnPc2	dna
ptactva	ptactvo	k1gNnSc2	ptactvo
<g/>
,	,	kIx,	,
Fakta	faktum	k1gNnPc1	faktum
a	a	k8xC	a
Data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
kalendář	kalendář	k1gInSc1	kalendář
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
ČR	ČR	kA	ČR
Ochránci	ochránce	k1gMnPc1	ochránce
si	se	k3xPyFc3	se
připomněli	připomnět	k5eAaPmAgMnP	připomnět
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
ptactva	ptactvo	k1gNnSc2	ptactvo
<g/>
,	,	kIx,	,
Ecomonitor	Ecomonitor	k1gInSc1	Ecomonitor
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
ZO	ZO	kA	ZO
ČSOP	ČSOP	kA	ČSOP
Rokycany	Rokycany	k1gInPc1	Rokycany
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
</s>
