<s>
Ppc	Ppc	k?
<g/>
64	#num#	k4
</s>
<s>
Procesor	procesor	k1gInSc1
PowerPC	PowerPC	k1gMnSc2
970	#num#	k4
dodávaný	dodávaný	k2eAgInSc1d1
do	do	k7c2
počítačů	počítač	k1gMnPc2
Power	Power	k1gMnSc1
Mac	Mac	kA
G5	G5	k1gMnSc1
</s>
<s>
ppc	ppc	k?
<g/>
64	#num#	k4
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
hardwarových	hardwarový	k2eAgFnPc2d1
počítačových	počítačový	k2eAgFnPc2d1
platforem	platforma	k1gFnPc2
používané	používaný	k2eAgFnPc4d1
v	v	k7c6
prostředí	prostředí	k1gNnSc6
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
překladačích	překladač	k1gInPc6
z	z	k7c2
rodin	rodina	k1gFnPc2
LLVM	LLVM	kA
a	a	k8xC
GCC	GCC	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
označuje	označovat	k5eAaImIp3nS
architektury	architektura	k1gFnSc2
64	#num#	k4
<g/>
bitových	bitový	k2eAgFnPc2d1
instrukčních	instrukční	k2eAgFnPc2d1
sad	sada	k1gFnPc2
PowerPC	PowerPC	k1gFnSc2
a	a	k8xC
Power	Power	k1gInSc4
ISA	ISA	kA
ukládajících	ukládající	k2eAgInPc2d1
nejvíce	hodně	k6eAd3,k6eAd1
významný	významný	k2eAgInSc1d1
bajt	bajt	k1gInSc1
na	na	k7c4
nejnižší	nízký	k2eAgFnSc4d3
adresu	adresa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
obdobné	obdobný	k2eAgFnPc4d1
architektury	architektura	k1gFnPc4
ukládající	ukládající	k2eAgFnPc4d1
nejvíce	nejvíce	k6eAd1,k6eAd3
významný	významný	k2eAgInSc1d1
bajt	bajt	k1gInSc1
na	na	k7c4
nejvyšší	vysoký	k2eAgFnSc4d3
adresu	adresa	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
pro	pro	k7c4
POWER	POWER	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
označení	označení	k1gNnSc1
ppc	ppc	k?
<g/>
64	#num#	k4
<g/>
le	le	k?
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
ppc	ppc	k?
<g/>
64	#num#	k4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
PPC64	PPC64	k4
v	v	k7c6
Linux	Linux	kA
Standard	standard	k1gInSc1
Base	bas	k1gInSc5
</s>
