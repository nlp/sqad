ppc	ppc	k?
<g/>
64	#num#	k4
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
hardwarových	hardwarový	k2eAgFnPc2d1
počítačových	počítačový	k2eAgFnPc2d1
platforem	platforma	k1gFnPc2
používané	používaný	k2eAgFnPc4d1
v	v	k7c6
prostředí	prostředí	k1gNnSc6
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
překladačích	překladač	k1gInPc6
z	z	k7c2
rodin	rodina	k1gFnPc2
LLVM	LLVM	kA
a	a	k8xC
GCC	GCC	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
označuje	označovat	k5eAaImIp3nS
architektury	architektura	k1gFnSc2
64	#num#	k4
<g/>
bitových	bitový	k2eAgFnPc2d1
instrukčních	instrukční	k2eAgFnPc2d1
sad	sada	k1gFnPc2
PowerPC	PowerPC	k1gFnSc2
a	a	k8xC
Power	Power	k1gInSc4
ISA	ISA	kA
ukládajících	ukládající	k2eAgInPc2d1
nejvíce	hodně	k6eAd3,k6eAd1
významný	významný	k2eAgInSc1d1
bajt	bajt	k1gInSc1
na	na	k7c4
nejnižší	nízký	k2eAgFnSc4d3
adresu	adresa	k1gFnSc4
<g/>
.	.	kIx.
