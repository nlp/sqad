<s>
Hormony	hormon	k1gInPc1	hormon
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
mnohobuněčných	mnohobuněčný	k2eAgMnPc2d1	mnohobuněčný
organismů	organismus	k1gInPc2	organismus
jako	jako	k8xC	jako
chemický	chemický	k2eAgInSc4d1	chemický
přenašeč	přenašeč	k1gInSc4	přenašeč
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnPc1	skupina
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Hormony	hormon	k1gInPc1	hormon
jsou	být	k5eAaImIp3nP	být
produkovány	produkovat	k5eAaImNgInP	produkovat
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
všech	všecek	k3xTgInPc2	všecek
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
včetně	včetně	k7c2	včetně
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
tak	tak	k6eAd1	tak
řídit	řídit	k5eAaImF	řídit
průběh	průběh	k1gInSc4	průběh
a	a	k8xC	a
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
koordinaci	koordinace	k1gFnSc4	koordinace
reakcí	reakce	k1gFnPc2	reakce
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
enzymů	enzym	k1gInPc2	enzym
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
působí	působit	k5eAaImIp3nS	působit
jen	jen	k9	jen
na	na	k7c4	na
žijící	žijící	k2eAgFnPc4d1	žijící
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Působení	působení	k1gNnSc1	působení
hormonu	hormon	k1gInSc2	hormon
je	být	k5eAaImIp3nS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
detekci	detekce	k1gFnSc4	detekce
buňkou	buňka	k1gFnSc7	buňka
<g/>
,	,	kIx,	,
hormon	hormon	k1gInSc1	hormon
musí	muset	k5eAaImIp3nS	muset
interagovat	interagovat	k5eAaBmF	interagovat
s	s	k7c7	s
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
buněčným	buněčný	k2eAgInSc7d1	buněčný
receptorem	receptor	k1gInSc7	receptor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pak	pak	k6eAd1	pak
spustí	spustit	k5eAaPmIp3nS	spustit
kaskádu	kaskáda	k1gFnSc4	kaskáda
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
až	až	k9	až
k	k	k7c3	k
pro	pro	k7c4	pro
hormon	hormon	k1gInSc4	hormon
typické	typický	k2eAgFnSc3d1	typická
odezvě	odezva	k1gFnSc3	odezva
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
různé	různý	k2eAgFnPc1d1	různá
buňky	buňka	k1gFnPc1	buňka
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
receptory	receptor	k1gInPc4	receptor
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
hormonu	hormon	k1gInSc3	hormon
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
počtu	počet	k1gInSc6	počet
nebo	nebo	k8xC	nebo
odlišně	odlišně	k6eAd1	odlišně
citlivé	citlivý	k2eAgNnSc1d1	citlivé
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
hormon	hormon	k1gInSc1	hormon
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
přednostně	přednostně	k6eAd1	přednostně
na	na	k7c4	na
skupiny	skupina	k1gFnPc4	skupina
buněk	buňka	k1gFnPc2	buňka
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
citlivostí	citlivost	k1gFnSc7	citlivost
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
fytohormon	fytohormon	k1gInSc1	fytohormon
<g/>
.	.	kIx.	.
</s>
<s>
Rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
hormony	hormon	k1gInPc1	hormon
se	se	k3xPyFc4	se
od	od	k7c2	od
živočišných	živočišný	k2eAgMnPc2d1	živočišný
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
obvykle	obvykle	k6eAd1	obvykle
menší	malý	k2eAgFnSc7d2	menší
specifičností	specifičnost	k1gFnSc7	specifičnost
-	-	kIx~	-
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
hormonů	hormon	k1gInPc2	hormon
působí	působit	k5eAaImIp3nP	působit
širší	široký	k2eAgFnSc4d2	širší
paletu	paleta	k1gFnSc4	paleta
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
účinku	účinek	k1gInSc6	účinek
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
překrývají	překrývat	k5eAaImIp3nP	překrývat
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
fyziologických	fyziologický	k2eAgInPc2d1	fyziologický
účinků	účinek	k1gInPc2	účinek
také	také	k9	také
vznikají	vznikat	k5eAaImIp3nP	vznikat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
kombinaci	kombinace	k1gFnSc6	kombinace
působení	působení	k1gNnSc2	působení
fytohormonů	fytohormon	k1gInPc2	fytohormon
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
fytohormony	fytohormon	k1gInPc7	fytohormon
řadíme	řadit	k5eAaImIp1nP	řadit
<g/>
:	:	kIx,	:
auxiny	auxin	k1gInPc4	auxin
cytokininy	cytokinin	k2eAgInPc4d1	cytokinin
ethylen	ethylen	k1gInSc4	ethylen
(	(	kIx(	(
<g/>
ethen	ethen	k1gInSc1	ethen
<g/>
)	)	kIx)	)
gibereliny	giberelina	k1gFnPc1	giberelina
kyselinu	kyselina	k1gFnSc4	kyselina
abscisovou	abscisový	k2eAgFnSc4d1	abscisová
brassinosteroidy	brassinosteroida	k1gFnPc4	brassinosteroida
Hormony	hormon	k1gInPc1	hormon
vznikají	vznikat	k5eAaImIp3nP	vznikat
ve	v	k7c6	v
specializovaných	specializovaný	k2eAgFnPc6d1	specializovaná
endokrinních	endokrinní	k2eAgFnPc6d1	endokrinní
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
endon	endon	k1gInSc1	endon
–	–	k?	–
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
,	,	kIx,	,
krinein	krinein	k2eAgInSc4d1	krinein
–	–	k?	–
vylučovat	vylučovat	k5eAaImF	vylučovat
<g/>
)	)	kIx)	)
žlázách	žláza	k1gFnPc6	žláza
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
prokrvením	prokrvení	k1gNnSc7	prokrvení
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
(	(	kIx(	(
<g/>
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
sekrece	sekrece	k1gFnSc1	sekrece
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
také	také	k9	také
i	i	k9	i
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
primárně	primárně	k6eAd1	primárně
jiné	jiný	k2eAgFnPc1d1	jiná
úlohy	úloha	k1gFnPc1	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
hormony	hormon	k1gInPc1	hormon
potom	potom	k6eAd1	potom
obvykle	obvykle	k6eAd1	obvykle
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k9	jen
uvnitř	uvnitř	k7c2	uvnitř
tohoto	tento	k3xDgInSc2	tento
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hormon	hormon	k1gInSc1	hormon
sekretin	sekretin	k1gInSc1	sekretin
působí	působit	k5eAaImIp3nS	působit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
buňky	buňka	k1gFnPc4	buňka
sliznice	sliznice	k1gFnSc2	sliznice
a	a	k8xC	a
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
<g/>
.	.	kIx.	.
</s>
<s>
Hormony	hormon	k1gInPc1	hormon
vznikající	vznikající	k2eAgInPc1d1	vznikající
v	v	k7c6	v
nervovém	nervový	k2eAgInSc6d1	nervový
systému	systém	k1gInSc6	systém
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnPc6	jeho
buňkách	buňka	k1gFnPc6	buňka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
neurohormony	neurohormona	k1gFnPc1	neurohormona
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
buňky	buňka	k1gFnPc1	buňka
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
mezimozku	mezimozek	k1gInSc6	mezimozek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řídí	řídit	k5eAaImIp3nS	řídit
příjem	příjem	k1gInSc4	příjem
a	a	k8xC	a
výdej	výdej	k1gInSc4	výdej
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
soli	sůl	k1gFnSc2	sůl
nebo	nebo	k8xC	nebo
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
napětí	napětí	k1gNnSc4	napětí
cévních	cévní	k2eAgFnPc2d1	cévní
stěn	stěna	k1gFnPc2	stěna
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slepic	slepice	k1gFnPc2	slepice
<g/>
)	)	kIx)	)
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
světla	světlo	k1gNnSc2	světlo
polohovou	polohový	k2eAgFnSc4d1	polohová
koordinaci	koordinace	k1gFnSc4	koordinace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
hormony	hormon	k1gInPc4	hormon
<g/>
.	.	kIx.	.
</s>
<s>
Hormony	hormon	k1gInPc1	hormon
jsou	být	k5eAaImIp3nP	být
produkty	produkt	k1gInPc4	produkt
žláz	žláza	k1gFnPc2	žláza
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
sekrecí	sekrece	k1gFnSc7	sekrece
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tkání	tkáň	k1gFnPc2	tkáň
produkující	produkující	k2eAgInPc1d1	produkující
hormony	hormon	k1gInPc1	hormon
<g/>
,	,	kIx,	,
vylučované	vylučovaný	k2eAgFnPc1d1	vylučovaná
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
bílkovinnou	bílkovinný	k2eAgFnSc4d1	bílkovinná
povahu	povaha	k1gFnSc4	povaha
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
aminokyselinové	aminokyselinový	k2eAgInPc4d1	aminokyselinový
hormony	hormon	k1gInPc4	hormon
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
látky	látka	k1gFnPc4	látka
odvozené	odvozený	k2eAgFnPc4d1	odvozená
od	od	k7c2	od
cholesterolu	cholesterol	k1gInSc2	cholesterol
(	(	kIx(	(
<g/>
steroidy	steroid	k1gInPc1	steroid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hormony	hormon	k1gInPc1	hormon
mají	mít	k5eAaImIp3nP	mít
specifický	specifický	k2eAgInSc4d1	specifický
účinek	účinek	k1gInSc4	účinek
–	–	k?	–
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
žádnou	žádný	k3yNgFnSc7	žádný
jinou	jiný	k2eAgFnSc7d1	jiná
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
cíleně	cíleně	k6eAd1	cíleně
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
účinek	účinek	k1gInSc4	účinek
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
ohraničené	ohraničený	k2eAgFnPc4d1	ohraničená
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
jejich	jejich	k3xOp3gNnSc2	jejich
působení	působení	k1gNnSc2	působení
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
nebo	nebo	k8xC	nebo
i	i	k9	i
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Hormony	hormon	k1gInPc1	hormon
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
označovány	označovat	k5eAaImNgInP	označovat
prvními	první	k4xOgInPc7	první
posly	posel	k1gMnPc7	posel
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
uplatnily	uplatnit	k5eAaPmAgFnP	uplatnit
svůj	svůj	k3xOyFgInSc4	svůj
účinek	účinek	k1gInSc4	účinek
na	na	k7c4	na
efektorovou	efektorový	k2eAgFnSc4d1	efektorový
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
buněčný	buněčný	k2eAgInSc4d1	buněčný
receptor	receptor	k1gInSc4	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
hormonu	hormon	k1gInSc2	hormon
může	moct	k5eAaImIp3nS	moct
nacházet	nacházet	k5eAaImF	nacházet
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
buněčných	buněčný	k2eAgFnPc6d1	buněčná
membránách	membrána	k1gFnPc6	membrána
<g/>
:	:	kIx,	:
na	na	k7c6	na
buněčné	buněčný	k2eAgFnSc6d1	buněčná
membráně	membrána	k1gFnSc6	membrána
–	–	k?	–
např.	např.	kA	např.
u	u	k7c2	u
inzulínu	inzulín	k1gInSc2	inzulín
na	na	k7c6	na
jaderné	jaderný	k2eAgFnSc6d1	jaderná
membráně	membrána	k1gFnSc6	membrána
–	–	k?	–
u	u	k7c2	u
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
(	(	kIx(	(
<g/>
glukokortikoidy	glukokortikoid	k1gInPc1	glukokortikoid
a	a	k8xC	a
i	i	k9	i
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
hormony	hormon	k1gInPc4	hormon
<g/>
)	)	kIx)	)
Endokrinní	endokrinní	k2eAgFnSc1d1	endokrinní
regulace	regulace	k1gFnSc1	regulace
probíhá	probíhat	k5eAaImIp3nS	probíhat
běžně	běžně	k6eAd1	běžně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odpověď	odpověď	k1gFnSc4	odpověď
buňky	buňka	k1gFnSc2	buňka
na	na	k7c4	na
signál	signál	k1gInSc4	signál
(	(	kIx(	(
<g/>
hormon	hormon	k1gInSc1	hormon
<g/>
)	)	kIx)	)
zpětně	zpětně	k6eAd1	zpětně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
zdroj	zdroj	k1gInSc1	zdroj
signálu	signál	k1gInSc2	signál
(	(	kIx(	(
<g/>
endokrinní	endokrinní	k2eAgFnSc4d1	endokrinní
žlázu	žláza	k1gFnSc4	žláza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
nebo	nebo	k8xC	nebo
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozitivní	pozitivní	k2eAgFnSc6d1	pozitivní
zpětné	zpětný	k2eAgFnSc6d1	zpětná
vazbě	vazba	k1gFnSc6	vazba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
působení	působení	k1gNnSc1	působení
oxytocinu	oxytocin	k1gInSc2	oxytocin
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
<g/>
)	)	kIx)	)
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
odpovědi	odpověď	k1gFnSc3	odpověď
zesilující	zesilující	k2eAgFnSc2d1	zesilující
původní	původní	k2eAgInSc4d1	původní
signál	signál	k1gInSc4	signál
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
negativní	negativní	k2eAgFnSc6d1	negativní
zpětné	zpětný	k2eAgFnSc6d1	zpětná
vazbě	vazba	k1gFnSc6	vazba
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgInSc1d1	původní
signál	signál	k1gInSc1	signál
odpovědí	odpověď	k1gFnPc2	odpověď
ztlumen	ztlumen	k2eAgMnSc1d1	ztlumen
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
účinky	účinek	k1gInPc1	účinek
hormonů	hormon	k1gInPc2	hormon
při	při	k7c6	při
regulaci	regulace	k1gFnSc6	regulace
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
konfigurační	konfigurační	k2eAgFnPc4d1	konfigurační
změny	změna	k1gFnPc4	změna
enzymů	enzym	k1gInPc2	enzym
(	(	kIx(	(
<g/>
alosterické	alosterický	k2eAgInPc4d1	alosterický
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
)	)	kIx)	)
útlum	útlum	k1gInSc1	útlum
nebo	nebo	k8xC	nebo
stimulace	stimulace	k1gFnSc1	stimulace
enzymu	enzym	k1gInSc2	enzym
změna	změna	k1gFnSc1	změna
množství	množství	k1gNnSc4	množství
substrátu	substrát	k1gInSc2	substrát
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
nervy	nerv	k1gInPc7	nerv
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
hormony	hormon	k1gInPc1	hormon
mnohem	mnohem	k6eAd1	mnohem
pomaleji	pomale	k6eAd2	pomale
a	a	k8xC	a
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
životních	životní	k2eAgFnPc6d1	životní
činnostech	činnost	k1gFnPc6	činnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
<g/>
:	:	kIx,	:
růst	růst	k1gInSc1	růst
a	a	k8xC	a
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
hormony	hormon	k1gInPc1	hormon
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
řízením	řízení	k1gNnSc7	řízení
nebo	nebo	k8xC	nebo
ovlivňováním	ovlivňování	k1gNnSc7	ovlivňování
chemických	chemický	k2eAgInPc2d1	chemický
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
cílových	cílový	k2eAgFnPc6d1	cílová
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
určováním	určování	k1gNnSc7	určování
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
využívají	využívat	k5eAaPmIp3nP	využívat
živiny	živina	k1gFnPc4	živina
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tyto	tento	k3xDgFnPc4	tento
buňky	buňka	k1gFnPc4	buňka
budou	být	k5eAaImBp3nP	být
či	či	k8xC	či
nebudou	být	k5eNaImBp3nP	být
tvořit	tvořit	k5eAaImF	tvořit
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
vlasy	vlas	k1gInPc4	vlas
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc4d1	jiný
produkty	produkt	k1gInPc4	produkt
metabolických	metabolický	k2eAgInPc2d1	metabolický
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Hormony	hormon	k1gInPc1	hormon
mají	mít	k5eAaImIp3nP	mít
rozmanité	rozmanitý	k2eAgInPc1d1	rozmanitý
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
endokrinních	endokrinní	k2eAgFnPc6d1	endokrinní
žlázách	žláza	k1gFnPc6	žláza
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
celkově	celkově	k6eAd1	celkově
působící	působící	k2eAgInPc1d1	působící
hormony	hormon	k1gInPc1	hormon
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
inzulin	inzulin	k1gInSc1	inzulin
a	a	k8xC	a
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
hormony	hormon	k1gInPc1	hormon
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
jiných	jiný	k2eAgInPc2d1	jiný
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
působí	působit	k5eAaImIp3nP	působit
mnohem	mnohem	k6eAd1	mnohem
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
lokálně	lokálně	k6eAd1	lokálně
působícího	působící	k2eAgInSc2d1	působící
hormonu	hormon	k1gInSc2	hormon
je	být	k5eAaImIp3nS	být
sekretin	sekretin	k1gInSc1	sekretin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
ve	v	k7c6	v
dvanáctníku	dvanáctník	k1gInSc6	dvanáctník
(	(	kIx(	(
<g/>
duodenum	duodenum	k1gNnSc1	duodenum
<g/>
)	)	kIx)	)
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Hormon	hormon	k1gInSc1	hormon
potom	potom	k6eAd1	potom
putuje	putovat	k5eAaImIp3nS	putovat
krví	krev	k1gFnSc7	krev
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgMnSc1d1	břišní
(	(	kIx(	(
<g/>
pankreas	pankreas	k1gInSc1	pankreas
<g/>
)	)	kIx)	)
a	a	k8xC	a
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
vodnaté	vodnatý	k2eAgFnSc2d1	vodnatá
šťávy	šťáva	k1gFnSc2	šťáva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
enzymy	enzym	k1gInPc4	enzym
(	(	kIx(	(
<g/>
chemické	chemický	k2eAgInPc4d1	chemický
transformátory	transformátor	k1gInPc4	transformátor
<g/>
)	)	kIx)	)
potřebné	potřebný	k2eAgFnPc4d1	potřebná
na	na	k7c6	na
trávení	trávení	k1gNnSc6	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
příkladem	příklad	k1gInSc7	příklad
lokálně	lokálně	k6eAd1	lokálně
působících	působící	k2eAgInPc2d1	působící
hormonů	hormon	k1gInPc2	hormon
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
přenašečů	přenašeč	k1gInPc2	přenašeč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
acetylcholin	acetylcholin	k1gInSc1	acetylcholin
<g/>
,	,	kIx,	,
vytvářený	vytvářený	k2eAgInSc1d1	vytvářený
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
odevzdává	odevzdávat	k5eAaImIp3nS	odevzdávat
nerv	nerv	k1gInSc1	nerv
informaci	informace	k1gFnSc4	informace
svalové	svalový	k2eAgFnSc3d1	svalová
buňce	buňka	k1gFnSc3	buňka
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
její	její	k3xOp3gFnSc4	její
kontrakci	kontrakce	k1gFnSc4	kontrakce
<g/>
.	.	kIx.	.
</s>
<s>
Hormony	hormon	k1gInPc4	hormon
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
různých	různý	k2eAgNnPc2d1	různé
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
chemické	chemický	k2eAgFnSc2d1	chemická
povahy	povaha	k1gFnSc2	povaha
glykoproteiny	glykoproteina	k1gFnSc2	glykoproteina
–	–	k?	–
folitropin	folitropin	k1gInSc1	folitropin
<g/>
,	,	kIx,	,
tyreotropin	tyreotropin	k1gInSc1	tyreotropin
<g/>
,	,	kIx,	,
luteinizační	luteinizační	k2eAgInSc1d1	luteinizační
hormon	hormon	k1gInSc1	hormon
aminokyselinové	aminokyselinový	k2eAgFnSc2d1	aminokyselinová
–	–	k?	–
adrenalin	adrenalin	k1gInSc1	adrenalin
<g/>
,	,	kIx,	,
tyroxin	tyroxin	k1gInSc1	tyroxin
<g/>
,	,	kIx,	,
noradrenalin	noradrenalin	k1gInSc1	noradrenalin
<g/>
,	,	kIx,	,
dopamin	dopamin	k1gInSc1	dopamin
peptidové	peptidový	k2eAgFnSc2d1	peptidová
či	či	k8xC	či
bílkovinné	bílkovinný	k2eAgFnSc2d1	bílkovinná
–	–	k?	–
adrenokortikotropní	adrenokortikotropní	k2eAgInSc1d1	adrenokortikotropní
hormon	hormon	k1gInSc1	hormon
<g/>
,	,	kIx,	,
somatotropní	somatotropní	k2eAgInSc1d1	somatotropní
hormon	hormon	k1gInSc1	hormon
<g/>
,	,	kIx,	,
prolaktin	prolaktin	k1gInSc1	prolaktin
<g/>
,	,	kIx,	,
oxytocin	oxytocin	k1gInSc1	oxytocin
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
antidiuretický	antidiuretický	k2eAgInSc1d1	antidiuretický
hormon	hormon	k1gInSc1	hormon
<g/>
,	,	kIx,	,
kalcitonin	kalcitonin	k1gInSc1	kalcitonin
<g/>
,	,	kIx,	,
parathormon	parathormon	k1gInSc1	parathormon
<g/>
,	,	kIx,	,
gastrin	gastrin	k1gInSc1	gastrin
<g/>
,	,	kIx,	,
cholecystokinin	cholecystokinin	k2eAgInSc1d1	cholecystokinin
<g/>
,	,	kIx,	,
sekretin	sekretin	k1gInSc1	sekretin
<g/>
,	,	kIx,	,
inzulín	inzulín	k1gInSc1	inzulín
<g/>
,	,	kIx,	,
glukagon	glukagon	k1gInSc1	glukagon
steroidní	steroidní	k2eAgInSc1d1	steroidní
–	–	k?	–
základ	základ	k1gInSc1	základ
tvoří	tvořit	k5eAaImIp3nS	tvořit
steranový	steranový	k2eAgInSc1d1	steranový
kruh	kruh	k1gInSc1	kruh
(	(	kIx(	(
<g/>
estrogeny	estrogen	k1gInPc1	estrogen
<g/>
,	,	kIx,	,
androgeny	androgen	k1gInPc1	androgen
<g/>
,	,	kIx,	,
kortikosteroidy	kortikosteroid	k1gInPc1	kortikosteroid
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
účinku	účinek	k1gInSc2	účinek
regulační	regulační	k2eAgFnSc2d1	regulační
–	–	k?	–
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
jiné	jiný	k2eAgFnPc1d1	jiná
endokrinní	endokrinní	k2eAgFnPc1d1	endokrinní
žlázy	žláza	k1gFnPc1	žláza
(	(	kIx(	(
<g/>
liberiny	liberin	k1gInPc1	liberin
hypotalamu	hypotalamus	k1gInSc2	hypotalamus
<g/>
,	,	kIx,	,
tropní	tropnit	k5eAaPmIp3nP	tropnit
hormony	hormon	k1gInPc1	hormon
předního	přední	k2eAgInSc2d1	přední
laloku	lalok	k1gInSc2	lalok
hypofýzy	hypofýza	k1gFnSc2	hypofýza
<g/>
)	)	kIx)	)
hormony	hormon	k1gInPc1	hormon
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
účinkem	účinek	k1gInSc7	účinek
na	na	k7c4	na
tkáně	tkáň	k1gFnPc4	tkáň
(	(	kIx(	(
<g/>
inzulin	inzulin	k1gInSc1	inzulin
<g/>
,	,	kIx,	,
tyroxin	tyroxin	k1gInSc1	tyroxin
<g/>
)	)	kIx)	)
hormony	hormon	k1gInPc1	hormon
působící	působící	k2eAgInPc1d1	působící
lokálně	lokálně	k6eAd1	lokálně
–	–	k?	–
endotelin	endotelina	k1gFnPc2	endotelina
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
sekrece	sekrece	k1gFnSc2	sekrece
</s>
