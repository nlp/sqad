<s>
Hormony	hormon	k1gInPc1	hormon
jsou	být	k5eAaImIp3nP	být
produkovány	produkovat	k5eAaImNgInP	produkovat
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
všech	všecek	k3xTgInPc2	všecek
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
včetně	včetně	k7c2	včetně
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
tak	tak	k6eAd1	tak
řídit	řídit	k5eAaImF	řídit
průběh	průběh	k1gInSc4	průběh
a	a	k8xC	a
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
koordinaci	koordinace	k1gFnSc4	koordinace
reakcí	reakce	k1gFnPc2	reakce
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
