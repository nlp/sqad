<p>
<s>
Ugrofinské	ugrofinský	k2eAgInPc1d1	ugrofinský
jazyky	jazyk	k1gInPc1	jazyk
tvoří	tvořit	k5eAaImIp3nP	tvořit
společně	společně	k6eAd1	společně
se	s	k7c7	s
samojedskými	samojedský	k2eAgInPc7d1	samojedský
jazyky	jazyk	k1gInPc7	jazyk
uralskou	uralský	k2eAgFnSc4d1	Uralská
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jimi	on	k3xPp3gInPc7	on
mluví	mluvit	k5eAaImIp3nS	mluvit
na	na	k7c6	na
rozlehlých	rozlehlý	k2eAgFnPc6d1	rozlehlá
plochách	plocha	k1gFnPc6	plocha
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
,	,	kIx,	,
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
ugrofinské	ugrofinský	k2eAgNnSc1d1	ugrofinský
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
však	však	k9	však
dnes	dnes	k6eAd1	dnes
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
sporný	sporný	k2eAgInSc4d1	sporný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
někteří	některý	k3yIgMnPc1	některý
lingvisté	lingvista	k1gMnPc1	lingvista
mají	mít	k5eAaImIp3nP	mít
zato	zato	k6eAd1	zato
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
podskupiny	podskupina	k1gFnPc1	podskupina
ugrofinských	ugrofinský	k2eAgInPc2d1	ugrofinský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
finsko-permské	finskoermský	k2eAgInPc1d1	finsko-permský
jazyky	jazyk	k1gInPc1	jazyk
a	a	k8xC	a
ugrické	ugrický	k2eAgInPc1d1	ugrický
jazyky	jazyk	k1gInPc1	jazyk
k	k	k7c3	k
sobě	se	k3xPyFc3	se
mají	mít	k5eAaImIp3nP	mít
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
má	mít	k5eAaImIp3nS	mít
jiná	jiný	k2eAgFnSc1d1	jiná
skupina	skupina	k1gFnSc1	skupina
uralských	uralský	k2eAgInPc2d1	uralský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jazyky	jazyk	k1gInPc1	jazyk
samojedské	samojedský	k2eAgInPc1d1	samojedský
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
ugrofinské	ugrofinský	k2eAgInPc1d1	ugrofinský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
"	"	kIx"	"
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
uralskou	uralský	k2eAgFnSc4d1	Uralská
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ugrofinské	ugrofinský	k2eAgInPc1d1	ugrofinský
jazyky	jazyk	k1gInPc1	jazyk
mají	mít	k5eAaImIp3nP	mít
kolem	kolem	k7c2	kolem
22	[number]	k4	22
milionů	milion	k4xCgInPc2	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Ugrické	Ugrický	k2eAgInPc1d1	Ugrický
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
</s>
</p>
<p>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
</s>
</p>
<p>
<s>
Obsko-ugrické	Obskogrický	k2eAgNnSc1d1	Obsko-ugrický
</s>
</p>
<p>
<s>
Chantyjština	Chantyjština	k1gFnSc1	Chantyjština
(	(	kIx(	(
<g/>
osťáčtina	osťáčtina	k1gFnSc1	osťáčtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mansijština	Mansijština	k1gFnSc1	Mansijština
(	(	kIx(	(
<g/>
vogulština	vogulština	k1gFnSc1	vogulština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finsko-permské	finskoermský	k2eAgInPc1d1	finsko-permský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
Permské	permský	k2eAgNnSc1d1	Permské
</s>
</p>
<p>
<s>
Komijské	Komijský	k2eAgNnSc1d1	Komijský
</s>
</p>
<p>
<s>
Komi	Komi	k1gNnSc1	Komi
(	(	kIx(	(
<g/>
zyrjanština	zyrjanština	k1gFnSc1	zyrjanština
<g/>
,	,	kIx,	,
komi-zyrjanština	komiyrjanština	k1gFnSc1	komi-zyrjanština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Komi-permjačtina	Komiermjačtina	k1gFnSc1	Komi-permjačtina
</s>
</p>
<p>
<s>
Komi-jazvanština	Komiazvanština	k1gFnSc1	Komi-jazvanština
(	(	kIx(	(
<g/>
jazvanština	jazvanština	k1gFnSc1	jazvanština
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
permjačtina	permjačtina	k1gFnSc1	permjačtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Udmurtština	Udmurtština	k1gFnSc1	Udmurtština
(	(	kIx(	(
<g/>
voťáčtina	voťáčtina	k1gFnSc1	voťáčtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finsko-volžské	finskoolžský	k2eAgInPc1d1	finsko-volžský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
finsko-marijské	finskoarijský	k2eAgFnSc3d1	finsko-marijský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marijské	Marijský	k2eAgNnSc1d1	Marijský
</s>
</p>
<p>
<s>
Marijština	Marijština	k1gFnSc1	Marijština
(	(	kIx(	(
<g/>
čeremiština	čeremiština	k1gFnSc1	čeremiština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mordvinské	mordvinský	k2eAgNnSc1d1	mordvinský
</s>
</p>
<p>
<s>
Erzja	Erzja	k6eAd1	Erzja
</s>
</p>
<p>
<s>
Mokša	Mokša	k6eAd1	Mokša
</s>
</p>
<p>
<s>
Vymřelé	vymřelý	k2eAgInPc1d1	vymřelý
finsko-volžské	finskoolžský	k2eAgInPc1d1	finsko-volžský
jazyky	jazyk	k1gInPc1	jazyk
s	s	k7c7	s
nejistým	jistý	k2eNgNnSc7d1	nejisté
zařazením	zařazení	k1gNnSc7	zařazení
</s>
</p>
<p>
<s>
†	†	k?	†
Merja	Merja	k1gFnSc1	Merja
</s>
</p>
<p>
<s>
†	†	k?	†
Muromština	Muromština	k1gFnSc1	Muromština
</s>
</p>
<p>
<s>
†	†	k?	†
Meščerština	Meščerština	k1gFnSc1	Meščerština
</s>
</p>
<p>
<s>
Finsko-laponské	finskoaponský	k2eAgInPc1d1	finsko-laponský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
finsko-sámské	finskoámský	k2eAgFnSc3d1	finsko-sámský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sámské	Sámský	k2eAgInPc1d1	Sámský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
laponské	laponský	k2eAgFnSc3d1	laponská
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Západosámské	Západosámský	k2eAgNnSc1d1	Západosámský
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
sámština	sámština	k1gFnSc1	sámština
</s>
</p>
<p>
<s>
Umejská	Umejský	k2eAgFnSc1d1	Umejský
sámština	sámština	k1gFnSc1	sámština
–	–	k?	–
téměř	téměř	k6eAd1	téměř
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
Lulejská	Lulejský	k2eAgFnSc1d1	Lulejský
sámština	sámština	k1gFnSc1	sámština
</s>
</p>
<p>
<s>
Pitejská	Pitejský	k2eAgFnSc1d1	Pitejský
sámština	sámština	k1gFnSc1	sámština
–	–	k?	–
téměř	téměř	k6eAd1	téměř
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
sámština	sámština	k1gFnSc1	sámština
</s>
</p>
<p>
<s>
Východosámské	Východosámský	k2eAgNnSc1d1	Východosámský
</s>
</p>
<p>
<s>
†	†	k?	†
Kemijská	Kemijský	k2eAgFnSc1d1	Kemijský
sámština	sámština	k1gFnSc1	sámština
–	–	k?	–
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
Inarijská	Inarijský	k2eAgFnSc1d1	Inarijský
sámština	sámština	k1gFnSc1	sámština
</s>
</p>
<p>
<s>
†	†	k?	†
Akkalská	Akkalský	k2eAgFnSc1d1	Akkalský
sámština	sámština	k1gFnSc1	sámština
(	(	kIx(	(
<g/>
babinská	babinský	k2eAgFnSc1d1	babinský
<g/>
)	)	kIx)	)
-	-	kIx~	-
poslední	poslední	k2eAgFnSc1d1	poslední
mluvčí	mluvčí	k1gFnSc1	mluvčí
zemřela	zemřít	k5eAaPmAgFnS	zemřít
29	[number]	k4	29
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Kildinská	Kildinský	k2eAgFnSc1d1	Kildinský
sámština	sámština	k1gFnSc1	sámština
</s>
</p>
<p>
<s>
Skoltská	Skoltský	k2eAgFnSc1d1	Skoltský
sámština	sámština	k1gFnSc1	sámština
</s>
</p>
<p>
<s>
Terská	Terský	k2eAgFnSc1d1	Terský
sámština	sámština	k1gFnSc1	sámština
–	–	k?	–
téměř	téměř	k6eAd1	téměř
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
Baltofinské	Baltofinský	k2eAgInPc1d1	Baltofinský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
Estonština	estonština	k1gFnSc1	estonština
</s>
</p>
<p>
<s>
Estonština	estonština	k1gFnSc1	estonština
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
estonština	estonština	k1gFnSc1	estonština
</s>
</p>
<p>
<s>
Võ	Võ	k?	Võ
</s>
</p>
<p>
<s>
Setučtina	Setučtina	k1gFnSc1	Setučtina
(	(	kIx(	(
<g/>
seto	set	k2eAgNnSc1d1	seto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finské	finský	k2eAgNnSc1d1	finské
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
</s>
</p>
<p>
<s>
Meänkieli	Meänkiet	k5eAaImAgMnP	Meänkiet
</s>
</p>
<p>
<s>
Kvenština	Kvenština	k1gFnSc1	Kvenština
</s>
</p>
<p>
<s>
Ingrijská	Ingrijský	k2eAgFnSc1d1	Ingrijský
finština	finština	k1gFnSc1	finština
</s>
</p>
<p>
<s>
Ižorština	Ižorština	k1gFnSc1	Ižorština
(	(	kIx(	(
<g/>
ingrijština	ingrijština	k1gFnSc1	ingrijština
<g/>
)	)	kIx)	)
–	–	k?	–
téměř	téměř	k6eAd1	téměř
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
Karelské	karelský	k2eAgNnSc1d1	karelský
</s>
</p>
<p>
<s>
Karelština	Karelština	k1gFnSc1	Karelština
</s>
</p>
<p>
<s>
Ludičtina	Ludičtina	k1gFnSc1	Ludičtina
(	(	kIx(	(
<g/>
lydština	lydština	k1gFnSc1	lydština
<g/>
,	,	kIx,	,
lüdština	lüdština	k1gFnSc1	lüdština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Livvi	Livev	k1gFnSc3	Livev
(	(	kIx(	(
<g/>
aunuská	aunuský	k2eAgFnSc1d1	aunuský
karelština	karelština	k1gFnSc1	karelština
<g/>
,	,	kIx,	,
oloněcká	oloněcký	k2eAgFnSc1d1	oloněcký
karelština	karelština	k1gFnSc1	karelština
<g/>
,	,	kIx,	,
oloněčtina	oloněčtina	k1gFnSc1	oloněčtina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Livonština	livonština	k1gFnSc1	livonština
</s>
</p>
<p>
<s>
Vepština	Vepština	k1gFnSc1	Vepština
</s>
</p>
<p>
<s>
Votština	Votština	k1gFnSc1	Votština
–	–	k?	–
téměř	téměř	k6eAd1	téměř
vymřelá	vymřelý	k2eAgFnSc1d1	vymřelá
</s>
</p>
<p>
<s>
===	===	k?	===
Ugrické	Ugrický	k2eAgInPc1d1	Ugrický
jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Ugrická	Ugrický	k2eAgFnSc1d1	Ugrický
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
početně	početně	k6eAd1	početně
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
a	a	k8xC	a
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
jazykem	jazyk	k1gInSc7	jazyk
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
maďarština	maďarština	k1gFnSc1	maďarština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Finské	finský	k2eAgInPc1d1	finský
jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
finských	finský	k2eAgMnPc2d1	finský
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
početná	početný	k2eAgFnSc1d1	početná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jazyky	jazyk	k1gInPc4	jazyk
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
malým	malý	k2eAgInSc7d1	malý
počtem	počet	k1gInSc7	počet
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Finština	finština	k1gFnSc1	finština
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
)	)	kIx)	)
a	a	k8xC	a
estonština	estonština	k1gFnSc1	estonština
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jediné	jediný	k2eAgInPc1d1	jediný
dva	dva	k4xCgInPc1	dva
jazyky	jazyk	k1gInPc1	jazyk
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
hovoří	hovořit	k5eAaImIp3nS	hovořit
mluvčí	mluvčí	k1gMnSc1	mluvčí
na	na	k7c6	na
vlastním	vlastní	k2eAgNnSc6d1	vlastní
státním	státní	k2eAgNnSc6d1	státní
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
karelština	karelština	k1gFnSc1	karelština
na	na	k7c6	na
území	území	k1gNnSc6	území
Karelské	karelský	k2eAgFnSc2d1	Karelská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
finsky	finsky	k6eAd1	finsky
a	a	k8xC	a
karelsky	karelsky	k6eAd1	karelsky
Karjala	Karjala	k1gFnSc1	Karjala
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gInPc7	on
laponské	laponský	k2eAgInPc1d1	laponský
jazyky	jazyk	k1gInPc1	jazyk
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
územích	území	k1gNnPc6	území
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
komijské	komijský	k2eAgInPc4d1	komijský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
užívaných	užívaný	k2eAgFnPc2d1	užívaná
zejména	zejména	k9	zejména
na	na	k7c6	na
rozsáhlých	rozsáhlý	k2eAgNnPc6d1	rozsáhlé
územích	území	k1gNnPc6	území
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
význam	význam	k1gInSc1	význam
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgMnSc1d1	velký
a	a	k8xC	a
samotné	samotný	k2eAgNnSc1d1	samotné
používání	používání	k1gNnSc1	používání
postupně	postupně	k6eAd1	postupně
zaniká	zanikat	k5eAaImIp3nS	zanikat
společně	společně	k6eAd1	společně
se	s	k7c7	s
stárnoucí	stárnoucí	k2eAgFnSc7d1	stárnoucí
populací	populace	k1gFnSc7	populace
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
</p>
