<p>
<s>
Skalná	skalný	k2eAgFnSc1d1	Skalná
ihla	ihla	k1gFnSc1	ihla
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
PIENAP	PIENAP	kA	PIENAP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Chmeľnica	Chmeľnic	k1gInSc2	Chmeľnic
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Stará	starý	k2eAgFnSc1d1	stará
Ľubovňa	Ľubovňa	k1gFnSc1	Ľubovňa
v	v	k7c6	v
Prešovském	prešovský	k2eAgInSc6d1	prešovský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
či	či	k8xC	či
novelizováno	novelizovat	k5eAaBmNgNnS	novelizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
0,1400	[number]	k4	0,1400
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
nebylo	být	k5eNaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Skalná	skalný	k2eAgFnSc1d1	Skalná
ihla	ihla	k1gFnSc1	ihla
<g/>
,	,	kIx,	,
Štátny	Štátno	k1gNnPc7	Štátno
zoznam	zoznam	k6eAd1	zoznam
osobitne	osobitnout	k5eAaPmIp3nS	osobitnout
chránených	chránená	k1gFnPc2	chránená
častí	častit	k5eAaImIp3nP	častit
prírody	prírod	k1gInPc1	prírod
SR	SR	kA	SR
</s>
</p>
<p>
<s>
Chránené	Chránená	k1gFnPc1	Chránená
územia	územia	k1gFnSc1	územia
<g/>
,	,	kIx,	,
Štátna	Štáten	k2eAgFnSc1d1	Štátna
ochrana	ochrana	k1gFnSc1	ochrana
prírody	príroda	k1gFnSc2	príroda
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnSc2	republika
</s>
</p>
