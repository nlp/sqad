<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ש	ש	k?	ש
<g/>
ְ	ְ	k?	ְ
<g/>
ׂ	ׂ	k?	ׂ
<g/>
ר	ר	k?	ר
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
<g/>
ֵ	ֵ	k?	ֵ
<g/>
ל	ל	k?	ל
<g/>
,	,	kIx,	,
Jisra	Jisra	k1gMnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
إ	إ	k?	إ
<g/>
,	,	kIx,	,
Isrá	Isrá	k1gFnSc1	Isrá
<g/>
'	'	kIx"	'
<g/>
íl	íl	k?	íl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Stát	stát	k1gInSc1	stát
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
מ	מ	k?	מ
<g/>
ְ	ְ	k?	ְ
<g/>
ד	ד	k?	ד
<g/>
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
<g/>
ַ	ַ	k?	ַ
<g/>
ת	ת	k?	ת
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ש	ש	k?	ש
<g/>
ְ	ְ	k?	ְ
<g/>
ׂ	ׂ	k?	ׂ
<g/>
ר	ר	k?	ר
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
<g/>
ֵ	ֵ	k?	ֵ
<g/>
ל	ל	k?	ל
<g/>
,	,	kIx,	,
Medinat	Medinat	k1gMnSc1	Medinat
Jisra	Jisra	k1gMnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
د	د	k?	د
إ	إ	k?	إ
<g/>
,	,	kIx,	,
Daulat	Daule	k1gNnPc2	Daule
Isrá	Isrá	k1gFnSc1	Isrá
<g/>
'	'	kIx"	'
<g/>
íl	íl	k?	íl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nS	ležet
při	při	k7c6	při
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Libanonem	Libanon	k1gInSc7	Libanon
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
se	s	k7c7	s
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Izraeli	Izrael	k1gInSc3	Izrael
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
i	i	k9	i
Západní	západní	k2eAgInSc1d1	západní
břeh	břeh	k1gInSc1	břeh
Jordánu	Jordán	k1gInSc2	Jordán
(	(	kIx(	(
<g/>
Judea	Judea	k1gFnSc1	Judea
a	a	k8xC	a
Samaří	Samaří	k1gNnSc1	Samaří
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pásmo	pásmo	k1gNnSc1	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
území	území	k1gNnSc1	území
částečně	částečně	k6eAd1	částečně
spravovaná	spravovaný	k2eAgFnSc1d1	spravovaná
Palestinskou	palestinský	k2eAgFnSc7d1	palestinská
autonomií	autonomie	k1gFnSc7	autonomie
–	–	k?	–
tato	tento	k3xDgNnPc1	tento
území	území	k1gNnPc4	území
jsou	být	k5eAaImIp3nP	být
Izraelem	Izrael	k1gInSc7	Izrael
okupována	okupovat	k5eAaBmNgNnP	okupovat
od	od	k7c2	od
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
populací	populace	k1gFnSc7	populace
8,3	[number]	k4	8,3
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
většinu	většina	k1gFnSc4	většina
(	(	kIx(	(
<g/>
75,4	[number]	k4	75,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
jediným	jediný	k2eAgInSc7d1	jediný
židovským	židovský	k2eAgInSc7d1	židovský
státem	stát	k1gInSc7	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
domovem	domov	k1gInSc7	domov
izraelských	izraelský	k2eAgMnPc2d1	izraelský
Arabů	Arab	k1gMnPc2	Arab
(	(	kIx(	(
<g/>
20,6	[number]	k4	20,6
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
Drúzů	Drúz	k1gMnPc2	Drúz
a	a	k8xC	a
Samaritánů	Samaritán	k1gMnPc2	Samaritán
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
náboženských	náboženský	k2eAgFnPc2d1	náboženská
a	a	k8xC	a
etnických	etnický	k2eAgFnPc2d1	etnická
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
<g/>
Moderní	moderní	k2eAgInSc1d1	moderní
stát	stát	k1gInSc1	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
o	o	k7c4	o
jehož	jehož	k3xOyRp3gInSc4	jehož
vznik	vznik	k1gInSc4	vznik
usilovalo	usilovat	k5eAaImAgNnS	usilovat
sionistické	sionistický	k2eAgNnSc1d1	sionistické
hnutí	hnutí	k1gNnSc1	hnutí
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
na	na	k7c4	na
biblickou	biblický	k2eAgFnSc4d1	biblická
myšlenku	myšlenka	k1gFnSc4	myšlenka
země	zem	k1gFnSc2	zem
izraelské	izraelský	k2eAgFnSc2d1	izraelská
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
zaslíbení	zaslíbení	k1gNnSc1	zaslíbení
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
ústředních	ústřední	k2eAgInPc2d1	ústřední
motivů	motiv	k1gInPc2	motiv
judaismu	judaismus	k1gInSc2	judaismus
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgInPc1	tři
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
Britský	britský	k2eAgInSc4d1	britský
mandát	mandát	k1gInSc4	mandát
Palestina	Palestina	k1gFnSc1	Palestina
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vytvořit	vytvořit	k5eAaPmF	vytvořit
"	"	kIx"	"
<g/>
domovinu	domovina	k1gFnSc4	domovina
pro	pro	k7c4	pro
židovský	židovský	k2eAgInSc4d1	židovský
lid	lid	k1gInSc4	lid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
schválila	schválit	k5eAaPmAgFnS	schválit
rozdělení	rozdělení	k1gNnSc4	rozdělení
Mandátu	mandát	k1gInSc2	mandát
Palestina	Palestina	k1gFnSc1	Palestina
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
státy	stát	k1gInPc4	stát
–	–	k?	–
židovský	židovský	k2eAgInSc4d1	židovský
a	a	k8xC	a
arabský	arabský	k2eAgInSc4d1	arabský
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Liga	liga	k1gFnSc1	liga
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
tento	tento	k3xDgInSc4	tento
plán	plán	k1gInSc4	plán
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
v	v	k7c4	v
následující	následující	k2eAgInPc4d1	následující
vítězné	vítězný	k2eAgInPc4d1	vítězný
Válce	válec	k1gInPc4	válec
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
své	svůj	k3xOyFgFnPc4	svůj
hranice	hranice	k1gFnPc4	hranice
nad	nad	k7c4	nad
rámec	rámec	k1gInSc4	rámec
plánu	plán	k1gInSc2	plán
OSN	OSN	kA	OSN
na	na	k7c6	na
rozdělení	rozdělení	k1gNnSc6	rozdělení
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
trvá	trvat	k5eAaImIp3nS	trvat
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
sousedícími	sousedící	k2eAgFnPc7d1	sousedící
arabskými	arabský	k2eAgFnPc7d1	arabská
zeměmi	zem	k1gFnPc7	zem
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
několik	několik	k4yIc4	několik
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
desetiletí	desetiletí	k1gNnSc2	desetiletí
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgMnSc1d1	trvající
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
sousedícími	sousedící	k2eAgFnPc7d1	sousedící
zeměmi	zem	k1gFnPc7	zem
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
a	a	k8xC	a
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
)	)	kIx)	)
však	však	k9	však
Izrael	Izrael	k1gInSc1	Izrael
už	už	k6eAd1	už
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
podepsal	podepsat	k5eAaPmAgInS	podepsat
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
dohod	dohoda	k1gFnPc2	dohoda
s	s	k7c7	s
Palestinci	Palestinec	k1gMnPc7	Palestinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
všeobecným	všeobecný	k2eAgNnSc7d1	všeobecné
rovným	rovný	k2eAgNnSc7d1	rovné
volebním	volební	k2eAgNnSc7d1	volební
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
vede	vést	k5eAaImIp3nS	vést
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
Kneset	Kneseta	k1gFnPc2	Kneseta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
je	být	k5eAaImIp3nS	být
izraelská	izraelský	k2eAgFnSc1d1	izraelská
ekonomika	ekonomika	k1gFnSc1	ekonomika
41	[number]	k4	41
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
patří	patřit	k5eAaImIp3nS	patřit
Izrael	Izrael	k1gInSc1	Izrael
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
indexu	index	k1gInSc6	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
svobody	svoboda	k1gFnSc2	svoboda
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Izrael	Izrael	k1gInSc1	Izrael
samotný	samotný	k2eAgInSc1d1	samotný
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
,	,	kIx,	,
interní	interní	k2eAgInSc4d1	interní
informační	informační	k2eAgInSc4d1	informační
podklad	podklad	k1gInSc4	podklad
českého	český	k2eAgInSc2d1	český
Parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
institutu	institut	k1gInSc2	institut
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
uznává	uznávat	k5eAaImIp3nS	uznávat
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Tel	tel	kA	tel
Aviv-Jaffa	Aviv-Jaff	k1gMnSc2	Aviv-Jaff
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Izrael	Izrael	k1gInSc1	Izrael
již	již	k6eAd1	již
přes	přes	k7c4	přes
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
v	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
i	i	k8xC	i
náboženském	náboženský	k2eAgNnSc6d1	náboženské
užití	užití	k1gNnSc6	užití
označuje	označovat	k5eAaImIp3nS	označovat
jak	jak	k6eAd1	jak
židovský	židovský	k2eAgInSc4d1	židovský
národ	národ	k1gInSc4	národ
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zemi	zem	k1gFnSc4	zem
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bible	bible	k1gFnSc2	bible
byl	být	k5eAaImAgMnS	být
takto	takto	k6eAd1	takto
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
Jákob	Jákob	k1gMnSc1	Jákob
po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Biblický	biblický	k2eAgInSc1d1	biblický
národ	národ	k1gInSc1	národ
patriarchy	patriarcha	k1gMnSc2	patriarcha
Jákoba	Jákob	k1gMnSc2	Jákob
pak	pak	k6eAd1	pak
vešel	vejít	k5eAaPmAgInS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
synové	syn	k1gMnPc1	syn
Izraele	Izrael	k1gInSc2	Izrael
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
Izraelité	izraelita	k1gMnPc1	izraelita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
významu	význam	k1gInSc6	význam
jména	jméno	k1gNnSc2	jméno
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
teorií	teorie	k1gFnPc2	teorie
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
základem	základ	k1gInSc7	základ
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
sloveso	sloveso	k1gNnSc1	sloveso
s	s	k7c7	s
<g/>
–	–	k?	–
<g/>
r	r	kA	r
<g/>
–	–	k?	–
<g/>
r	r	kA	r
(	(	kIx(	(
<g/>
také	také	k9	také
š	š	k?	š
<g/>
–	–	k?	–
<g/>
r	r	kA	r
<g/>
–	–	k?	–
<g/>
r	r	kA	r
<g/>
,	,	kIx,	,
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
být	být	k5eAaImF	být
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
význam	význam	k1gInSc4	význam
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
vládne	vládnout	k5eAaImIp3nS	vládnout
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiná	jiná	k1gFnSc1	jiná
vykládá	vykládat	k5eAaImIp3nS	vykládat
jméno	jméno	k1gNnSc4	jméno
jako	jako	k9	jako
"	"	kIx"	"
<g/>
kníže	kníže	k1gMnSc1	kníže
Boží	božit	k5eAaImIp3nS	božit
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
zápasí	zápasit	k5eAaImIp3nS	zápasit
<g/>
/	/	kIx~	/
<g/>
bojuje	bojovat	k5eAaImIp3nS	bojovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
historická	historický	k2eAgFnSc1d1	historická
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Izraeli	Izrael	k1gInSc6	Izrael
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Merneptahovy	Merneptahův	k2eAgFnSc2d1	Merneptahův
stély	stéla	k1gFnSc2	stéla
ze	z	k7c2	z
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
židovskému	židovský	k2eAgInSc3d1	židovský
národu	národ	k1gInSc3	národ
nebo	nebo	k8xC	nebo
k	k	k7c3	k
Palestině	Palestina	k1gFnSc3	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Stát	stát	k1gInSc1	stát
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
Medinat	Medinat	k1gMnSc1	Medinat
Jisra	Jisra	k1gMnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc4d1	ostatní
návrhy	návrh	k1gInPc4	návrh
jako	jako	k8xS	jako
např.	např.	kA	např.
Erec	Erec	k1gInSc1	Erec
Jisra	Jisro	k1gNnSc2	Jisro
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
(	(	kIx(	(
<g/>
Země	země	k1gFnSc1	země
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sijón	Sijón	k1gInSc1	Sijón
a	a	k8xC	a
Judea	Judea	k1gFnSc1	Judea
byly	být	k5eAaImAgFnP	být
odmítnuty	odmítnout	k5eAaPmNgFnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
nezávislosti	nezávislost	k1gFnSc2	nezávislost
oznámil	oznámit	k5eAaPmAgMnS	oznámit
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Moše	mocha	k1gFnSc6	mocha
Šaret	Šaret	k1gInSc4	Šaret
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
občané	občan	k1gMnPc1	občan
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
budou	být	k5eAaImBp3nP	být
nazývat	nazývat	k5eAaImF	nazývat
Izraelci	Izraelec	k1gMnPc1	Izraelec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověké	starověký	k2eAgNnSc4d1	starověké
a	a	k8xC	a
středověké	středověký	k2eAgNnSc4d1	středověké
osídlení	osídlení	k1gNnSc4	osídlení
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Izraele	Izrael	k1gInSc2	Izrael
bylo	být	k5eAaImAgNnS	být
obýváno	obývat	k5eAaImNgNnS	obývat
již	již	k9	již
v	v	k7c6	v
paleolitu	paleolit	k1gInSc6	paleolit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
neolitu	neolit	k1gInSc6	neolit
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
budována	budován	k2eAgNnPc4d1	budováno
trvalá	trvalý	k2eAgNnPc4d1	trvalé
sídla	sídlo	k1gNnPc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejstarší	starý	k2eAgNnSc4d3	nejstarší
město	město	k1gNnSc4	město
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
Jericho	Jericho	k1gNnSc1	Jericho
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
8000	[number]	k4	8000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Až	až	k6eAd1	až
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vznikaly	vznikat	k5eAaImAgFnP	vznikat
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
pouze	pouze	k6eAd1	pouze
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
některé	některý	k3yIgFnSc2	některý
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
velmoci	velmoc	k1gFnSc2	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
suverénním	suverénní	k2eAgInSc7d1	suverénní
státem	stát	k1gInSc7	stát
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
Izraelské	izraelský	k2eAgNnSc1d1	izraelské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
cca	cca	kA	cca
931	[number]	k4	931
<g/>
–	–	k?	–
<g/>
722	[number]	k4	722
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Země	země	k1gFnSc1	země
izraelská	izraelský	k2eAgFnSc1d1	izraelská
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
Tóry	tóra	k1gFnSc2	tóra
Hospodinem	Hospodin	k1gMnSc7	Hospodin
přislíbena	přislíbit	k5eAaPmNgFnS	přislíbit
Abrahámovi	Abrahám	k1gMnSc3	Abrahám
a	a	k8xC	a
darována	darovat	k5eAaPmNgFnS	darovat
jeho	jeho	k3xOp3gMnPc3	jeho
potomkům	potomek	k1gMnPc3	potomek
jako	jako	k8xS	jako
Země	zem	k1gFnSc2	zem
zaslíbená	zaslíbená	k1gFnSc1	zaslíbená
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
věřícími	věřící	k2eAgMnPc7d1	věřící
židy	žid	k1gMnPc7	žid
(	(	kIx(	(
<g/>
a	a	k8xC	a
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
i	i	k9	i
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
)	)	kIx)	)
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
Svatou	svatý	k2eAgFnSc4d1	svatá
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejsvětější	nejsvětější	k2eAgNnPc4d1	nejsvětější
místa	místo	k1gNnPc4	místo
judaismu	judaismus	k1gInSc2	judaismus
i	i	k8xC	i
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
oblastí	oblast	k1gFnSc7	oblast
však	však	k9	však
vedly	vést	k5eAaImAgFnP	vést
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
obchodní	obchodní	k2eAgFnPc1d1	obchodní
stezky	stezka	k1gFnPc1	stezka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přitahovalo	přitahovat	k5eAaImAgNnS	přitahovat
zájem	zájem	k1gInSc4	zájem
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
postupně	postupně	k6eAd1	postupně
spadalo	spadat	k5eAaPmAgNnS	spadat
pod	pod	k7c4	pod
asyrskou	asyrský	k2eAgFnSc4d1	Asyrská
<g/>
,	,	kIx,	,
babylonskou	babylonský	k2eAgFnSc4d1	Babylonská
<g/>
,	,	kIx,	,
perskou	perský	k2eAgFnSc4d1	perská
<g/>
,	,	kIx,	,
řeckou	řecký	k2eAgFnSc4d1	řecká
<g/>
,	,	kIx,	,
římskou	římský	k2eAgFnSc4d1	římská
<g/>
,	,	kIx,	,
sásánovskou	sásánovský	k2eAgFnSc4d1	sásánovská
<g/>
,	,	kIx,	,
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
a	a	k8xC	a
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pod	pod	k7c4	pod
islámskou	islámský	k2eAgFnSc4d1	islámská
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
oblast	oblast	k1gFnSc4	oblast
ovládali	ovládat	k5eAaImAgMnP	ovládat
Umajjovci	Umajjovec	k1gMnPc1	Umajjovec
<g/>
,	,	kIx,	,
Abbásovci	Abbásovec	k1gMnPc1	Abbásovec
<g/>
,	,	kIx,	,
křižáci	křižák	k1gMnPc1	křižák
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1260	[number]	k4	1260
byla	být	k5eAaImAgFnS	být
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
mamlúckému	mamlúcký	k2eAgInSc3d1	mamlúcký
sultanátu	sultanát	k1gInSc3	sultanát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
protiřímském	protiřímský	k2eAgNnSc6d1	protiřímské
povstání	povstání	k1gNnSc6	povstání
Bar	bar	k1gInSc1	bar
Kochby	Kochba	k1gFnSc2	Kochba
v	v	k7c6	v
letech	let	k1gInPc6	let
132	[number]	k4	132
<g/>
−	−	k?	−
<g/>
135	[number]	k4	135
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
následujícím	následující	k2eAgNnSc6d1	následující
vyhnání	vyhnání	k1gNnSc6	vyhnání
Židů	Žid	k1gMnPc2	Žid
se	se	k3xPyFc4	se
židovská	židovská	k1gFnSc1	židovská
populace	populace	k1gFnSc2	populace
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Izraele	Izrael	k1gInSc2	Izrael
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
úplně	úplně	k6eAd1	úplně
nikdy	nikdy	k6eAd1	nikdy
nezanikla	zaniknout	k5eNaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Reakcí	reakce	k1gFnSc7	reakce
na	na	k7c6	na
povstání	povstání	k1gNnSc6	povstání
byla	být	k5eAaImAgFnS	být
také	také	k9	také
změna	změna	k1gFnSc1	změna
názvu	název	k1gInSc2	název
území	území	k1gNnSc1	území
<g/>
:	:	kIx,	:
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
provincii	provincie	k1gFnSc4	provincie
Judeu	Judea	k1gFnSc4	Judea
na	na	k7c4	na
Syria	Syrius	k1gMnSc4	Syrius
Palaestina	Palaestin	k1gMnSc4	Palaestin
podle	podle	k7c2	podle
národa	národ	k1gInSc2	národ
Pelištejců	Pelištejec	k1gMnPc2	Pelištejec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sionismus	sionismus	k1gInSc1	sionismus
a	a	k8xC	a
britský	britský	k2eAgInSc1d1	britský
mandát	mandát	k1gInSc1	mandát
===	===	k?	===
</s>
</p>
<p>
<s>
Židé	Žid	k1gMnPc1	Žid
žijící	žijící	k2eAgMnPc1d1	žijící
mimo	mimo	k7c4	mimo
Palestinu	Palestina	k1gFnSc4	Palestina
odedávna	odedávna	k6eAd1	odedávna
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
touha	touha	k1gFnSc1	touha
byla	být	k5eAaImAgFnS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
v	v	k7c6	v
Tanachu	Tanach	k1gInSc6	Tanach
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
motivem	motiv	k1gInSc7	motiv
židovských	židovský	k2eAgFnPc2d1	židovská
modlitebních	modlitební	k2eAgFnPc2d1	modlitební
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Persekuce	persekuce	k1gFnSc1	persekuce
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
jejich	jejich	k3xOp3gNnSc7	jejich
vyhnáním	vyhnání	k1gNnSc7	vyhnání
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
stálému	stálý	k2eAgInSc3d1	stálý
proudu	proud	k1gInSc3	proud
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
usazovali	usazovat	k5eAaImAgMnP	usazovat
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
velké	velký	k2eAgFnPc1d1	velká
židovské	židovský	k2eAgFnPc1d1	židovská
komunity	komunita	k1gFnPc1	komunita
ve	v	k7c6	v
"	"	kIx"	"
<g/>
čtyřech	čtyři	k4xCgNnPc6	čtyři
svatých	svatý	k2eAgNnPc6d1	svaté
městech	město	k1gNnPc6	město
<g/>
"	"	kIx"	"
a	a	k8xC	a
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
takřka	takřka	k6eAd1	takřka
celá	celý	k2eAgFnSc1d1	celá
chasidská	chasidský	k2eAgFnSc1d1	chasidská
pospolitost	pospolitost	k1gFnSc1	pospolitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
velká	velký	k2eAgFnSc1d1	velká
vlna	vlna	k1gFnSc1	vlna
moderní	moderní	k2eAgFnSc2d1	moderní
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
alija	alija	k1gFnSc1	alija
<g/>
,	,	kIx,	,
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
po	po	k7c6	po
protižidovských	protižidovský	k2eAgInPc6d1	protižidovský
pogromech	pogrom	k1gInPc6	pogrom
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
existovala	existovat	k5eAaImAgFnS	existovat
sionistická	sionistický	k2eAgFnSc1d1	sionistická
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
až	až	k9	až
Theodor	Theodor	k1gMnSc1	Theodor
Herzl	Herzl	k1gMnSc1	Herzl
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
politický	politický	k2eAgInSc4d1	politický
sionismus	sionismus	k1gInSc4	sionismus
−	−	k?	−
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
usilovalo	usilovat	k5eAaImAgNnS	usilovat
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
židovského	židovský	k2eAgInSc2d1	židovský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
díky	díky	k7c3	díky
Herzlovi	Herzl	k1gMnSc3	Herzl
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
otázka	otázka	k1gFnSc1	otázka
začala	začít	k5eAaPmAgFnS	začít
řešit	řešit	k5eAaImF	řešit
i	i	k9	i
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
diplomatické	diplomatický	k2eAgFnSc6d1	diplomatická
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
představil	představit	k5eAaPmAgMnS	představit
Herzl	Herzl	k1gMnSc1	Herzl
své	svůj	k3xOyFgFnSc2	svůj
vize	vize	k1gFnSc2	vize
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Der	drát	k5eAaImRp2nS	drát
Judenstaat	Judenstaat	k1gInSc1	Judenstaat
(	(	kIx(	(
<g/>
Židovský	židovský	k2eAgInSc1d1	židovský
stát	stát	k1gInSc1	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
předsedal	předsedat	k5eAaImAgMnS	předsedat
prvnímu	první	k4xOgInSc3	první
Světovému	světový	k2eAgInSc3d1	světový
sionistickému	sionistický	k2eAgInSc3d1	sionistický
kongresu	kongres	k1gInSc3	kongres
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
.	.	kIx.	.
<g/>
Britský	britský	k2eAgMnSc1d1	britský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Arthur	Arthur	k1gMnSc1	Arthur
Balfour	Balfour	k1gMnSc1	Balfour
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
deklaroval	deklarovat	k5eAaBmAgInS	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
vláda	vláda	k1gFnSc1	vláda
jeho	on	k3xPp3gNnSc2	on
Veličenstva	veličenstvo	k1gNnSc2	veličenstvo
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
příznivě	příznivě	k6eAd1	příznivě
na	na	k7c6	na
zřízení	zřízení	k1gNnSc6	zřízení
národní	národní	k2eAgFnSc2d1	národní
domoviny	domovina	k1gFnSc2	domovina
židovského	židovský	k2eAgInSc2d1	židovský
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Arabský	arabský	k2eAgInSc4d1	arabský
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
židovskému	židovský	k2eAgInSc3d1	židovský
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
přerostl	přerůst	k5eAaPmAgInS	přerůst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
v	v	k7c6	v
násilnosti	násilnost	k1gFnSc6	násilnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
zformování	zformování	k1gNnSc3	zformování
palestinské	palestinský	k2eAgFnSc2d1	palestinská
židovské	židovský	k2eAgFnSc2d1	židovská
ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
organizace	organizace	k1gFnSc2	organizace
Hagana	Hagana	k1gFnSc1	Hagana
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
oddělily	oddělit	k5eAaPmAgFnP	oddělit
skupiny	skupina	k1gFnPc1	skupina
Irgun	Irguno	k1gNnPc2	Irguno
a	a	k8xC	a
Lechi	Lechi	k1gNnPc2	Lechi
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
Palestina	Palestina	k1gFnSc1	Palestina
obsazena	obsadit	k5eAaPmNgFnS	obsadit
Brity	Brit	k1gMnPc7	Brit
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Edmunda	Edmund	k1gMnSc2	Edmund
Allenbyho	Allenby	k1gMnSc2	Allenby
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
území	území	k1gNnSc1	území
Palestiny	Palestina	k1gFnSc2	Palestina
svěřeno	svěřit	k5eAaPmNgNnS	svěřit
pod	pod	k7c4	pod
britskou	britský	k2eAgFnSc4d1	britská
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
protektorát	protektorát	k1gInSc1	protektorát
Britský	britský	k2eAgInSc4d1	britský
mandát	mandát	k1gInSc4	mandát
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jehož	jehož	k3xOyRp3gInPc4	jehož
hlavní	hlavní	k2eAgInPc4d1	hlavní
cíle	cíl	k1gInPc4	cíl
patřilo	patřit	k5eAaImAgNnS	patřit
provedení	provedení	k1gNnSc4	provedení
Balfourovy	Balfourův	k2eAgFnSc2d1	Balfourova
deklarace	deklarace	k1gFnSc2	deklarace
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
protektorátu	protektorát	k1gInSc2	protektorát
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
většinou	většinou	k6eAd1	většinou
arabské	arabský	k2eAgNnSc1d1	arabské
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
převážně	převážně	k6eAd1	převážně
židovský	židovský	k2eAgInSc4d1	židovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
imigrace	imigrace	k1gFnSc1	imigrace
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
za	za	k7c4	za
třetí	třetí	k4xOgFnSc4	třetí
aliji	alije	k1gFnSc4	alije
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
−	−	k?	−
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
aliji	ali	k6eAd2	ali
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
−	−	k?	−
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yQgInPc2	který
přišlo	přijít	k5eAaPmAgNnS	přijít
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Arabské	arabský	k2eAgInPc1d1	arabský
nepokoje	nepokoj	k1gInPc1	nepokoj
a	a	k8xC	a
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Jaffě	Jaffa	k1gFnSc6	Jaffa
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Britové	Brit	k1gMnPc1	Brit
omezili	omezit	k5eAaPmAgMnP	omezit
židovskou	židovský	k2eAgFnSc4d1	židovská
imigraci	imigrace	k1gFnSc4	imigrace
a	a	k8xC	a
území	území	k1gNnSc4	území
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInSc4d1	budoucí
židovský	židovský	k2eAgInSc4d1	židovský
stát	stát	k1gInSc4	stát
stanovili	stanovit	k5eAaPmAgMnP	stanovit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Jordán	Jordán	k1gInSc1	Jordán
<g/>
.	.	kIx.	.
<g/>
Vzestup	vzestup	k1gInSc1	vzestup
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
páté	pátý	k4xOgFnSc3	pátý
aliji	alij	k1gFnSc3	alij
<g/>
,	,	kIx,	,
přílivu	příliv	k1gInSc2	příliv
téměř	téměř	k6eAd1	téměř
čtvrt	čtvrt	k1xP	čtvrt
milionu	milion	k4xCgInSc2	milion
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podnítil	podnítit	k5eAaPmAgMnS	podnítit
arabské	arabský	k2eAgFnSc2d1	arabská
násilnosti	násilnost	k1gFnSc2	násilnost
a	a	k8xC	a
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
Arabové	Arab	k1gMnPc1	Arab
zabrali	zabrat	k5eAaPmAgMnP	zabrat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
změnilo	změnit	k5eAaPmAgNnS	změnit
politiku	politika	k1gFnSc4	politika
vůči	vůči	k7c3	vůči
židovskému	židovský	k2eAgNnSc3d1	Židovské
přistěhovalectví	přistěhovalectví	k1gNnSc3	přistěhovalectví
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
v	v	k7c6	v
takzvané	takzvaný	k2eAgFnSc6d1	takzvaná
Bílé	bílý	k2eAgFnSc6d1	bílá
knize	kniha	k1gFnSc6	kniha
zavedlo	zavést	k5eAaPmAgNnS	zavést
kvóty	kvóta	k1gFnPc4	kvóta
pro	pro	k7c4	pro
židovské	židovský	k2eAgMnPc4d1	židovský
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
omezení	omezení	k1gNnSc4	omezení
však	však	k9	však
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
uprchlíci	uprchlík	k1gMnPc1	uprchlík
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ilegální	ilegální	k2eAgFnSc4d1	ilegální
aliji	alije	k1gFnSc4	alije
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
aliji	alije	k1gFnSc4	alije
Bet	Bet	k1gFnSc2	Bet
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
tvořili	tvořit	k5eAaImAgMnP	tvořit
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
33	[number]	k4	33
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
oproti	oproti	k7c3	oproti
11	[number]	k4	11
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
<g/>
I	i	k8xC	i
když	když	k8xS	když
palestinští	palestinský	k2eAgMnPc1d1	palestinský
Židé	Žid	k1gMnPc1	Žid
s	s	k7c7	s
britskými	britský	k2eAgFnPc7d1	britská
přistěhovaleckými	přistěhovalecký	k2eAgFnPc7d1	přistěhovalecká
kvótami	kvóta	k1gFnPc7	kvóta
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
<g/>
,	,	kIx,	,
připojili	připojit	k5eAaPmAgMnP	připojit
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
k	k	k7c3	k
Davidu	David	k1gMnSc3	David
Ben	Ben	k1gInSc1	Ben
Gurionovi	Gurion	k1gMnSc3	Gurion
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
Budeme	být	k5eAaImBp1nP	být
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
Brity	Brit	k1gMnPc7	Brit
proti	proti	k7c3	proti
Hitlerovi	Hitler	k1gMnSc3	Hitler
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
Bílá	bílý	k2eAgFnSc1d1	bílá
kniha	kniha	k1gFnSc1	kniha
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
;	;	kIx,	;
a	a	k8xC	a
budeme	být	k5eAaImBp1nP	být
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
Bílé	bílý	k2eAgFnSc3d1	bílá
knize	kniha	k1gFnSc3	kniha
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
nebyla	být	k5eNaImAgFnS	být
žádná	žádný	k3yNgFnSc1	žádný
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Extremisté	extremista	k1gMnPc1	extremista
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
Irgun	Irguna	k1gFnPc2	Irguna
a	a	k8xC	a
Lechi	Lech	k1gMnPc1	Lech
však	však	k9	však
zahájili	zahájit	k5eAaPmAgMnP	zahájit
teroristický	teroristický	k2eAgInSc4d1	teroristický
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
Britům	Brit	k1gMnPc3	Brit
<g/>
;	;	kIx,	;
členové	člen	k1gMnPc1	člen
Lechi	Lech	k1gFnSc2	Lech
útočili	útočit	k5eAaImAgMnP	útočit
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
Irgun	Irgun	k1gInSc4	Irgun
protibritské	protibritský	k2eAgFnSc2d1	protibritská
akce	akce	k1gFnSc2	akce
zahájil	zahájit	k5eAaPmAgMnS	zahájit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
armádě	armáda	k1gFnSc6	armáda
bojovalo	bojovat	k5eAaImAgNnS	bojovat
v	v	k7c6	v
nově	nově	k6eAd1	nově
zformované	zformovaný	k2eAgFnSc6d1	zformovaná
Židovské	židovský	k2eAgFnSc6d1	židovská
brigádě	brigáda	k1gFnSc6	brigáda
na	na	k7c4	na
26	[number]	k4	26
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
Britského	britský	k2eAgInSc2d1	britský
mandátu	mandát	k1gInSc2	mandát
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Britové	Brit	k1gMnPc1	Brit
stále	stále	k6eAd1	stále
zadržovali	zadržovat	k5eAaImAgMnP	zadržovat
lodě	loď	k1gFnPc4	loď
plné	plný	k2eAgFnSc2d1	plná
židovských	židovský	k2eAgMnPc2d1	židovský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
palestinských	palestinský	k2eAgMnPc2d1	palestinský
Židů	Žid	k1gMnPc2	Žid
jako	jako	k8xS	jako
parašutisté	parašutista	k1gMnPc1	parašutista
seskočilo	seskočit	k5eAaPmAgNnS	seskočit
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
evropského	evropský	k2eAgNnSc2d1	Evropské
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
,	,	kIx,	,
ovládaného	ovládaný	k2eAgNnSc2d1	ovládané
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
britští	britský	k2eAgMnPc1d1	britský
vyzvědači	vyzvědač	k1gMnPc1	vyzvědač
navazovali	navazovat	k5eAaImAgMnP	navazovat
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
zbylými	zbylý	k2eAgFnPc7d1	zbylá
židovskými	židovský	k2eAgFnPc7d1	židovská
komunitami	komunita	k1gFnPc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
velkému	velký	k2eAgNnSc3d1	velké
legálnímu	legální	k2eAgNnSc3d1	legální
i	i	k8xC	i
nelegálnímu	legální	k2eNgNnSc3d1	nelegální
přistěhovalectví	přistěhovalectví	k1gNnSc3	přistěhovalectví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
polarizaci	polarizace	k1gFnSc3	polarizace
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
první	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
vyostřil	vyostřit	k5eAaPmAgInS	vyostřit
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
Židy	Žid	k1gMnPc4	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Brity	Brit	k1gMnPc7	Brit
obviňovali	obviňovat	k5eAaImAgMnP	obviňovat
z	z	k7c2	z
neochoty	neochota	k1gFnSc2	neochota
plnit	plnit	k5eAaImF	plnit
své	svůj	k3xOyFgInPc4	svůj
závazky	závazek	k1gInPc4	závazek
a	a	k8xC	a
stranění	stranění	k1gNnSc4	stranění
Arabům	Arab	k1gMnPc3	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sérii	série	k1gFnSc6	série
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
britské	britský	k2eAgFnPc4d1	britská
síly	síla	k1gFnPc4	síla
(	(	kIx(	(
<g/>
např.	např.	kA	např.
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
hotel	hotel	k1gInSc4	hotel
King	King	k1gMnSc1	King
David	David	k1gMnSc1	David
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
vzdala	vzdát	k5eAaPmAgFnS	vzdát
svého	svůj	k3xOyFgInSc2	svůj
mandátu	mandát	k1gInSc2	mandát
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
přijala	přijmout	k5eAaPmAgFnS	přijmout
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1947	[number]	k4	1947
Plán	plán	k1gInSc1	plán
OSN	OSN	kA	OSN
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
území	území	k1gNnSc1	území
mělo	mít	k5eAaImAgNnS	mít
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
arabský	arabský	k2eAgInSc4d1	arabský
a	a	k8xC	a
židovský	židovský	k2eAgInSc4d1	židovský
stát	stát	k1gInSc4	stát
a	a	k8xC	a
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
měl	mít	k5eAaImAgInS	mít
zůstat	zůstat	k5eAaPmF	zůstat
pod	pod	k7c7	pod
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
správou	správa	k1gFnSc7	správa
OSN	OSN	kA	OSN
jako	jako	k8xS	jako
corpus	corpus	k1gInSc6	corpus
separatum	separatum	k1gNnSc1	separatum
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
plán	plán	k1gInSc4	plán
přijali	přijmout	k5eAaPmAgMnP	přijmout
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
75	[number]	k4	75
%	%	kIx~	%
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
přiřknuto	přiřknout	k5eAaPmNgNnS	přiřknout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
poušť	poušť	k1gFnSc1	poušť
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Liga	liga	k1gFnSc1	liga
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
a	a	k8xC	a
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
arabská	arabský	k2eAgFnSc1d1	arabská
komise	komise	k1gFnSc1	komise
plán	plán	k1gInSc4	plán
odmítly	odmítnout	k5eAaPmAgFnP	odmítnout
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
porušuje	porušovat	k5eAaImIp3nS	porušovat
"	"	kIx"	"
<g/>
ustanovení	ustanovení	k1gNnSc1	ustanovení
Charty	charta	k1gFnSc2	charta
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
národu	národ	k1gInSc3	národ
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
určení	určení	k1gNnSc4	určení
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
osudu	osud	k1gInSc2	osud
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
Britského	britský	k2eAgInSc2d1	britský
mandátu	mandát	k1gInSc2	mandát
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
Izraele	Izrael	k1gInSc2	Izrael
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
někdejšího	někdejší	k2eAgInSc2d1	někdejší
britského	britský	k2eAgInSc2d1	britský
mandátu	mandát	k1gInSc2	mandát
1,2	[number]	k4	1,2
miliónu	milión	k4xCgInSc2	milión
Arabů	Arab	k1gMnPc2	Arab
a	a	k8xC	a
650	[number]	k4	650
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
armády	armáda	k1gFnSc2	armáda
pěti	pět	k4xCc2	pět
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
Izrael	Izrael	k1gInSc4	Izrael
napadly	napadnout	k5eAaPmAgFnP	napadnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozpoutaly	rozpoutat	k5eAaPmAgInP	rozpoutat
tak	tak	k6eAd1	tak
První	první	k4xOgFnSc4	první
arabsko-izraelskou	arabskozraelský	k2eAgFnSc4d1	arabsko-izraelská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
roce	rok	k1gInSc6	rok
bojů	boj	k1gInPc2	boj
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlášen	k2eAgNnSc1d1	vyhlášeno
příměří	příměří	k1gNnSc1	příměří
a	a	k8xC	a
stanoveny	stanoven	k2eAgFnPc1d1	stanovena
dočasné	dočasný	k2eAgFnPc1d1	dočasná
hranice	hranice	k1gFnPc1	hranice
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
Zelená	zelený	k2eAgFnSc1d1	zelená
linie	linie	k1gFnSc1	linie
<g/>
.	.	kIx.	.
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
anektovalo	anektovat	k5eAaBmAgNnS	anektovat
území	území	k1gNnSc4	území
Judeje	Judea	k1gFnSc2	Judea
a	a	k8xC	a
Samaří	Samaří	k1gNnSc2	Samaří
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
Západní	západní	k2eAgInSc4d1	západní
břeh	břeh	k1gInSc4	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
,	,	kIx,	,
a	a	k8xC	a
Východní	východní	k2eAgInSc1d1	východní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Egypt	Egypt	k1gInSc1	Egypt
převzal	převzít	k5eAaPmAgInS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Pásmem	pásmo	k1gNnSc7	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
byl	být	k5eAaImAgInS	být
Izrael	Izrael	k1gInSc1	Izrael
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
OSN	OSN	kA	OSN
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
opustilo	opustit	k5eAaPmAgNnS	opustit
zemi	zem	k1gFnSc4	zem
na	na	k7c4	na
711	[number]	k4	711
tisíc	tisíc	k4xCgInPc2	tisíc
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zhruba	zhruba	k6eAd1	zhruba
80	[number]	k4	80
%	%	kIx~	%
dřívější	dřívější	k2eAgFnSc2d1	dřívější
arabské	arabský	k2eAgFnSc2d1	arabská
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
osud	osud	k1gInSc1	osud
těchto	tento	k3xDgMnPc2	tento
palestinských	palestinský	k2eAgMnPc2d1	palestinský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
dodnes	dodnes	k6eAd1	dodnes
zatěžuje	zatěžovat	k5eAaImIp3nS	zatěžovat
arabsko-izraelské	arabskozraelský	k2eAgInPc4d1	arabsko-izraelský
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnPc4	první
léta	léto	k1gNnPc4	léto
židovského	židovský	k2eAgInSc2d1	židovský
státu	stát	k1gInSc2	stát
byla	být	k5eAaImAgFnS	být
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
imigrační	imigrační	k2eAgFnSc7d1	imigrační
vlnou	vlna	k1gFnSc7	vlna
přeživších	přeživší	k2eAgInPc2d1	přeživší
holocaustu	holocaust	k1gInSc2	holocaust
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
vyhnání	vyhnání	k1gNnSc4	vyhnání
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
populace	populace	k1gFnSc1	populace
Izraele	Izrael	k1gInSc2	Izrael
z	z	k7c2	z
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
na	na	k7c4	na
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
příchozí	příchozí	k1gMnPc1	příchozí
byli	být	k5eAaImAgMnP	být
většinou	většina	k1gFnSc7	většina
nemajetní	majetný	k2eNgMnPc1d1	nemajetný
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
ubytováváni	ubytováván	k2eAgMnPc1d1	ubytováván
v	v	k7c6	v
provizorních	provizorní	k2eAgInPc6d1	provizorní
uprchlických	uprchlický	k2eAgInPc6d1	uprchlický
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
známých	známý	k2eAgInPc2d1	známý
jako	jako	k8xS	jako
ma	ma	k?	ma
<g/>
'	'	kIx"	'
<g/>
abarot	abarot	k1gMnSc1	abarot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
Suezská	suezský	k2eAgFnSc1d1	Suezská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Egypt	Egypt	k1gInSc1	Egypt
znárodnil	znárodnit	k5eAaPmAgInS	znárodnit
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
Tiranskou	tiranský	k2eAgFnSc4d1	Tiranská
úžinu	úžina	k1gFnSc4	úžina
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
Egypt	Egypt	k1gInSc4	Egypt
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
Rudému	rudý	k2eAgNnSc3d1	Rudé
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Dobyl	dobýt	k5eAaPmAgInS	dobýt
sice	sice	k8xC	sice
Sinajský	sinajský	k2eAgInSc1d1	sinajský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
diplomatickým	diplomatický	k2eAgInSc7d1	diplomatický
tlakem	tlak	k1gInSc7	tlak
USA	USA	kA	USA
a	a	k8xC	a
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
se	se	k3xPyFc4	se
však	však	k9	však
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
právo	právo	k1gNnSc4	právo
plavit	plavit	k5eAaImF	plavit
se	se	k3xPyFc4	se
v	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
průplavu	průplav	k1gInSc6	průplav
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
následující	následující	k2eAgFnSc2d1	následující
dekády	dekáda	k1gFnSc2	dekáda
se	se	k3xPyFc4	se
Izraeli	Izrael	k1gMnSc3	Izrael
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
unést	unést	k5eAaPmF	unést
Adolfa	Adolf	k1gMnSc2	Adolf
Eichmanna	Eichmann	k1gMnSc2	Eichmann
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc2	tvůrce
"	"	kIx"	"
<g/>
konečného	konečný	k2eAgNnSc2d1	konečné
řešení	řešení	k1gNnSc2	řešení
<g/>
"	"	kIx"	"
židovské	židovský	k2eAgFnPc1d1	židovská
otázky	otázka	k1gFnPc1	otázka
<g/>
,	,	kIx,	,
a	a	k8xC	a
postavit	postavit	k5eAaPmF	postavit
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
před	před	k7c4	před
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
velmi	velmi	k6eAd1	velmi
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
veřejnost	veřejnost	k1gFnSc1	veřejnost
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
rozměry	rozměra	k1gFnPc4	rozměra
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
Eichmann	Eichmann	k1gInSc1	Eichmann
jediným	jediný	k2eAgMnSc7d1	jediný
člověkem	člověk	k1gMnSc7	člověk
odsouzeným	odsouzený	k2eAgMnSc7d1	odsouzený
izraelským	izraelský	k2eAgInSc7d1	izraelský
soudem	soud	k1gInSc7	soud
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konflikty	konflikt	k1gInPc4	konflikt
a	a	k8xC	a
mírové	mírový	k2eAgFnPc4d1	mírová
dohody	dohoda	k1gFnPc4	dohoda
===	===	k?	===
</s>
</p>
<p>
<s>
Arabské	arabský	k2eAgInPc1d1	arabský
státy	stát	k1gInPc1	stát
považovaly	považovat	k5eAaImAgInP	považovat
Stát	stát	k5eAaImF	stát
Izrael	Izrael	k1gInSc4	Izrael
za	za	k7c4	za
nelegitimní	legitimní	k2eNgNnPc4d1	nelegitimní
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Palestiny	Palestina	k1gFnSc2	Palestina
(	(	kIx(	(
<g/>
OOP	OOP	kA	OOP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
cíl	cíl	k1gInSc4	cíl
"	"	kIx"	"
<g/>
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zničení	zničení	k1gNnSc3	zničení
Izraele	Izrael	k1gInSc2	Izrael
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
Národní	národní	k2eAgFnSc6d1	národní
chartě	charta	k1gFnSc6	charta
se	se	k3xPyFc4	se
až	až	k6eAd1	až
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
uvádělo	uvádět	k5eAaImAgNnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
povstání	povstání	k1gNnSc1	povstání
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc4d1	jediný
způsob	způsob	k1gInSc4	způsob
jak	jak	k8xC	jak
osvobodit	osvobodit	k5eAaPmF	osvobodit
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
a	a	k8xC	a
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
svoje	své	k1gNnSc1	své
armády	armáda	k1gFnSc2	armáda
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
vyzval	vyzvat	k5eAaPmAgInS	vyzvat
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
mírové	mírový	k2eAgFnSc2d1	mírová
jednotky	jednotka	k1gFnSc2	jednotka
OSN	OSN	kA	OSN
a	a	k8xC	a
opět	opět	k6eAd1	opět
zablokoval	zablokovat	k5eAaPmAgMnS	zablokovat
Izraeli	Izrael	k1gMnSc3	Izrael
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
Rudému	rudý	k2eAgNnSc3d1	Rudé
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kroky	krok	k1gInPc1	krok
Izrael	Izrael	k1gInSc1	Izrael
chápal	chápat	k5eAaImAgInS	chápat
jako	jako	k9	jako
přípravu	příprava	k1gFnSc4	příprava
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
udeří	udeřit	k5eAaPmIp3nS	udeřit
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Šestidenní	šestidenní	k2eAgFnSc1d1	šestidenní
válka	válka	k1gFnSc1	válka
přinesla	přinést	k5eAaPmAgFnS	přinést
Izraeli	Izrael	k1gInSc3	Izrael
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
vítězství	vítězství	k1gNnSc3	vítězství
a	a	k8xC	a
zisk	zisk	k1gInSc1	zisk
území	území	k1gNnSc2	území
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
<g/>
,	,	kIx,	,
Pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
,	,	kIx,	,
Sinajského	sinajský	k2eAgInSc2d1	sinajský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
Golanských	Golanský	k2eAgFnPc2d1	Golanský
výšin	výšina	k1gFnPc2	výšina
a	a	k8xC	a
Východního	východní	k2eAgInSc2d1	východní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
palestinští	palestinský	k2eAgMnPc1d1	palestinský
ozbrojenci	ozbrojenec	k1gMnPc1	ozbrojenec
provedli	provést	k5eAaPmAgMnP	provést
řadu	řada	k1gFnSc4	řada
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
proti	proti	k7c3	proti
izraelským	izraelský	k2eAgInPc3d1	izraelský
cílům	cíl	k1gInPc3	cíl
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
masakr	masakr	k1gInSc4	masakr
izraelských	izraelský	k2eAgMnPc2d1	izraelský
atletů	atlet	k1gMnPc2	atlet
na	na	k7c6	na
LOH	LOH	kA	LOH
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
Operací	operace	k1gFnSc7	operace
Boží	boží	k2eAgInSc1d1	boží
hněv	hněv	k1gInSc1	hněv
<g/>
,	,	kIx,	,
pokusem	pokus	k1gInSc7	pokus
izraelské	izraelský	k2eAgFnPc1d1	izraelská
tajné	tajný	k2eAgFnPc1d1	tajná
služby	služba	k1gFnPc1	služba
Mosad	Mosad	k1gInSc4	Mosad
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
strůjce	strůjce	k1gMnSc1	strůjce
mnichovského	mnichovský	k2eAgInSc2d1	mnichovský
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
židovského	židovský	k2eAgInSc2d1	židovský
svátku	svátek	k1gInSc2	svátek
Jom	Jom	k1gMnSc1	Jom
kipur	kipur	k1gMnSc1	kipur
<g/>
,	,	kIx,	,
překvapivě	překvapivě	k6eAd1	překvapivě
na	na	k7c4	na
Izrael	Izrael	k1gInSc4	Izrael
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
Egypt	Egypt	k1gInSc4	Egypt
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Jomkipurská	Jomkipurský	k2eAgFnSc1d1	Jomkipurská
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
konečným	konečný	k2eAgNnSc7d1	konečné
vítězstvím	vítězství	k1gNnSc7	vítězství
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
letech	let	k1gInPc6	let
nadvlády	nadvláda	k1gFnSc2	nadvláda
socialistů	socialist	k1gMnPc2	socialist
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
izraelského	izraelský	k2eAgInSc2d1	izraelský
parlamentu	parlament	k1gInSc2	parlament
Knesetu	Kneset	k1gInSc2	Kneset
poprvé	poprvé	k6eAd1	poprvé
pravicová	pravicový	k2eAgFnSc1d1	pravicová
strana	strana	k1gFnSc1	strana
Likud	Likuda	k1gFnPc2	Likuda
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
učinil	učinit	k5eAaImAgMnS	učinit
egyptský	egyptský	k2eAgMnSc1d1	egyptský
prezident	prezident	k1gMnSc1	prezident
Anwar	Anwar	k1gInSc4	Anwar
Sadat	Sadat	k2eAgInSc4d1	Sadat
překvapivou	překvapivý	k2eAgFnSc4d1	překvapivá
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Sadat	Sadat	k1gInSc4	Sadat
a	a	k8xC	a
Menachem	Menach	k1gInSc7	Menach
Begin	Begina	k1gFnPc2	Begina
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
Egyptsko-izraelskou	egyptskozraelský	k2eAgFnSc4d1	egyptsko-izraelská
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Izrael	Izrael	k1gInSc1	Izrael
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
ze	z	k7c2	z
Sinajského	sinajský	k2eAgInSc2d1	sinajský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Izrael	Izrael	k1gInSc4	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
palestinský	palestinský	k2eAgInSc4d1	palestinský
teroristický	teroristický	k2eAgInSc4d1	teroristický
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
Operaci	operace	k1gFnSc4	operace
Lítání	Lítání	k?	Lítání
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obsazení	obsazení	k1gNnSc3	obsazení
jižního	jižní	k2eAgInSc2d1	jižní
Libanonu	Libanon	k1gInSc2	Libanon
a	a	k8xC	a
vytlačení	vytlačení	k1gNnSc2	vytlačení
jednotek	jednotka	k1gFnPc2	jednotka
OOP	OOP	kA	OOP
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Likudu	Likud	k1gInSc2	Likud
také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
expanzi	expanze	k1gFnSc3	expanze
izraelských	izraelský	k2eAgFnPc2d1	izraelská
osad	osada	k1gFnPc2	osada
na	na	k7c6	na
území	území	k1gNnSc6	území
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přinesla	přinést	k5eAaPmAgFnS	přinést
růst	růst	k1gInSc4	růst
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
Izraelci	Izraelec	k1gMnPc7	Izraelec
a	a	k8xC	a
palestinskými	palestinský	k2eAgMnPc7d1	palestinský
Araby	Arab	k1gMnPc7	Arab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1981	[number]	k4	1981
Izrael	Izrael	k1gInSc1	Izrael
vybombardoval	vybombardovat	k5eAaPmAgInS	vybombardovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc1	operace
Opera	opera	k1gFnSc1	opera
irácký	irácký	k2eAgInSc4d1	irácký
jaderný	jaderný	k2eAgInSc4d1	jaderný
reaktor	reaktor	k1gInSc4	reaktor
v	v	k7c6	v
Osiraku	Osirak	k1gInSc6	Osirak
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ho	on	k3xPp3gInSc4	on
Irák	Irák	k1gInSc4	Irák
mohl	moct	k5eAaImAgInS	moct
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
Izrael	Izrael	k1gInSc1	Izrael
do	do	k7c2	do
Libanonské	libanonský	k2eAgFnSc2d1	libanonská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zničil	zničit	k5eAaPmAgMnS	zničit
základny	základna	k1gFnPc4	základna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
OOP	OOP	kA	OOP
ostřelovala	ostřelovat	k5eAaImAgFnS	ostřelovat
raketami	raketa	k1gFnPc7	raketa
severní	severní	k2eAgInSc4d1	severní
Izrael	Izrael	k1gInSc4	Izrael
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
první	první	k4xOgFnSc3	první
libanonské	libanonský	k2eAgFnSc3d1	libanonská
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
Libanonu	Libanon	k1gInSc2	Libanon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
nárazníkové	nárazníkový	k2eAgFnSc6d1	nárazníková
zóně	zóna	k1gFnSc6	zóna
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc1	první
intifáda	intifáda	k1gFnSc1	intifáda
<g/>
,	,	kIx,	,
arabské	arabský	k2eAgNnSc1d1	arabské
povstání	povstání	k1gNnSc1	povstání
proti	proti	k7c3	proti
izraelské	izraelský	k2eAgFnSc3d1	izraelská
přítomnosti	přítomnost	k1gFnSc3	přítomnost
v	v	k7c6	v
Pásmu	pásmo	k1gNnSc6	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
a	a	k8xC	a
území	území	k1gNnSc2	území
západně	západně	k6eAd1	západně
od	od	k7c2	od
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
podporovala	podporovat	k5eAaImAgFnS	podporovat
OOP	OOP	kA	OOP
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
palestinských	palestinský	k2eAgMnPc2d1	palestinský
Arabů	Arab	k1gMnPc2	Arab
Saddáma	Saddám	k1gMnSc2	Saddám
Husajna	Husajn	k1gMnSc2	Husajn
a	a	k8xC	a
raketové	raketový	k2eAgInPc4d1	raketový
útoky	útok	k1gInPc4	útok
Iráku	Irák	k1gInSc2	Irák
proti	proti	k7c3	proti
Izraeli	Izrael	k1gInSc3	Izrael
<g/>
.	.	kIx.	.
<g/>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jicchaka	Jicchak	k1gMnSc2	Jicchak
Šamira	Šamir	k1gMnSc2	Šamir
Madridská	madridský	k2eAgFnSc1d1	Madridská
konference	konference	k1gFnSc1	konference
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
byla	být	k5eAaImAgNnP	být
průlomová	průlomový	k2eAgNnPc1d1	průlomové
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
stolu	stol	k1gInSc2	stol
sešly	sejít	k5eAaPmAgFnP	sejít
znepřátelené	znepřátelený	k2eAgFnPc1d1	znepřátelená
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
představitelé	představitel	k1gMnPc1	představitel
zde	zde	k6eAd1	zde
spolu	spolu	k6eAd1	spolu
přímo	přímo	k6eAd1	přímo
hovořili	hovořit	k5eAaImAgMnP	hovořit
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
Války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
přímým	přímý	k2eAgFnPc3d1	přímá
schůzkám	schůzka	k1gFnPc3	schůzka
mezi	mezi	k7c7	mezi
Izraelci	Izraelec	k1gMnPc7	Izraelec
a	a	k8xC	a
Araby	Arab	k1gMnPc7	Arab
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
městech	město	k1gNnPc6	město
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc1	týž
rok	rok	k1gInSc1	rok
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
volby	volba	k1gFnSc2	volba
Jicchak	Jicchak	k1gInSc1	Jicchak
Rabin	Rabin	k2eAgInSc1d1	Rabin
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
Mírová	mírový	k2eAgFnSc1d1	mírová
dohoda	dohoda	k1gFnSc1	dohoda
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1993	[number]	k4	1993
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc3	on
vyplývalo	vyplývat	k5eAaImAgNnS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
po	po	k7c4	po
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
dobu	doba	k1gFnSc4	doba
pěti	pět	k4xCc3	pět
let	léto	k1gNnPc2	léto
na	na	k7c6	na
území	území	k1gNnSc6	území
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
a	a	k8xC	a
pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Palestinská	palestinský	k2eAgFnSc1d1	palestinská
samospráva	samospráva	k1gFnSc1	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
podepsána	podepsat	k5eAaPmNgFnS	podepsat
Izraelsko-jordánská	izraelskoordánský	k2eAgFnSc1d1	izraelsko-jordánský
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
<g/>
Podepsání	podepsání	k1gNnSc1	podepsání
dohod	dohoda	k1gFnPc2	dohoda
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
narazilo	narazit	k5eAaPmAgNnS	narazit
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
radikálů	radikál	k1gMnPc2	radikál
<g/>
,	,	kIx,	,
což	což	k9	což
mírový	mírový	k2eAgInSc4d1	mírový
proces	proces	k1gInSc4	proces
značně	značně	k6eAd1	značně
zkomplikovalo	zkomplikovat	k5eAaPmAgNnS	zkomplikovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palestinské	palestinský	k2eAgFnSc6d1	palestinská
politické	politický	k2eAgFnSc6d1	politická
scéně	scéna	k1gFnSc6	scéna
začala	začít	k5eAaPmAgFnS	začít
sílit	sílit	k5eAaImF	sílit
opozice	opozice	k1gFnSc1	opozice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
Hamás	Hamás	k1gInSc1	Hamás
a	a	k8xC	a
Palestinský	palestinský	k2eAgInSc1d1	palestinský
islámský	islámský	k2eAgInSc1d1	islámský
džihád	džihád	k1gInSc1	džihád
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Organizaci	organizace	k1gFnSc3	organizace
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Palestiny	Palestina	k1gFnSc2	Palestina
a	a	k8xC	a
Smlouvám	smlouvat	k5eAaImIp1nS	smlouvat
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
začala	začít	k5eAaPmAgFnS	začít
vymezovat	vymezovat	k5eAaImF	vymezovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
smlouvě	smlouva	k1gFnSc3	smlouva
vymezila	vymezit	k5eAaPmAgFnS	vymezit
strana	strana	k1gFnSc1	strana
Likud	Likudo	k1gNnPc2	Likudo
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
pravicová	pravicový	k2eAgNnPc4d1	pravicové
uskupení	uskupení	k1gNnPc4	uskupení
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
Jicchak	Jicchak	k1gInSc1	Jicchak
Rabin	Rabina	k1gFnPc2	Rabina
byl	být	k5eAaImAgInS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1995	[number]	k4	1995
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
pravicovým	pravicový	k2eAgInSc7d1	pravicový
židovským	židovský	k2eAgInSc7d1	židovský
radikálem	radikál	k1gInSc7	radikál
Jigalem	Jigal	k1gMnSc7	Jigal
Amirem	Amir	k1gMnSc7	Amir
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Benjamina	Benjamin	k1gMnSc2	Benjamin
Netanjahua	Netanjahuus	k1gMnSc2	Netanjahuus
byl	být	k5eAaImAgInS	být
podepsán	podepsán	k2eAgInSc1d1	podepsán
tzv.	tzv.	kA	tzv.
Hebronský	hebronský	k2eAgInSc1d1	hebronský
protokol	protokol	k1gInSc1	protokol
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
znamenající	znamenající	k2eAgNnSc4d1	znamenající
předání	předání	k1gNnSc4	předání
města	město	k1gNnSc2	město
Hebron	Hebron	k1gMnSc1	Hebron
Palestinské	palestinský	k2eAgFnSc3d1	palestinská
autonomii	autonomie	k1gFnSc3	autonomie
a	a	k8xC	a
stažení	stažení	k1gNnSc6	stažení
izraelské	izraelský	k2eAgFnSc2d1	izraelská
armády	armáda	k1gFnSc2	armáda
z	z	k7c2	z
části	část	k1gFnSc2	část
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1998	[number]	k4	1998
bylo	být	k5eAaImAgNnS	být
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
Memorandum	memorandum	k1gNnSc1	memorandum
od	od	k7c2	od
Wye	Wye	k1gFnPc2	Wye
River	Rivra	k1gFnPc2	Rivra
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
přineslo	přinést	k5eAaPmAgNnS	přinést
postoupení	postoupení	k1gNnSc1	postoupení
13	[number]	k4	13
%	%	kIx~	%
území	území	k1gNnSc6	území
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
palestinským	palestinský	k2eAgMnPc3d1	palestinský
Arabům	Arab	k1gMnPc3	Arab
a	a	k8xC	a
úpravu	úprava	k1gFnSc4	úprava
statusu	status	k1gInSc2	status
některých	některý	k3yIgFnPc2	některý
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ehuda	Ehud	k1gMnSc2	Ehud
Baraka	Barak	k1gMnSc2	Barak
se	se	k3xPyFc4	se
izraelská	izraelský	k2eAgFnSc1d1	izraelská
armáda	armáda	k1gFnSc1	armáda
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
z	z	k7c2	z
jižního	jižní	k2eAgInSc2d1	jižní
Libanonu	Libanon	k1gInSc2	Libanon
(	(	kIx(	(
<g/>
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
s	s	k7c7	s
předsedou	předseda	k1gMnSc7	předseda
Palestinské	palestinský	k2eAgFnSc2d1	palestinská
autonomie	autonomie	k1gFnSc2	autonomie
Jásirem	Jásir	k1gMnSc7	Jásir
Arafatem	Arafat	k1gMnSc7	Arafat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2000	[number]	k4	2000
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
summit	summit	k1gInSc1	summit
v	v	k7c6	v
Camp	camp	k1gInSc1	camp
Davidu	David	k1gMnSc3	David
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Barak	Barak	k1gMnSc1	Barak
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
plán	plán	k1gInSc4	plán
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
Palestinského	palestinský	k2eAgInSc2d1	palestinský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
Arafatem	Arafat	k1gInSc7	Arafat
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ztroskotání	ztroskotání	k1gNnSc6	ztroskotání
jednání	jednání	k1gNnSc2	jednání
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
29.9	[number]	k4	29.9
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
další	další	k2eAgInSc4d1	další
povstání	povstání	k1gNnSc1	povstání
palestinských	palestinský	k2eAgMnPc2d1	palestinský
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
druhá	druhý	k4xOgFnSc1	druhý
intifáda	intifáda	k1gFnSc1	intifáda
neboli	neboli	k8xC	neboli
Intifáda	intifáda	k1gFnSc1	intifáda
al-Aksá	al-Aksat	k5eAaPmIp3nS	al-Aksat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2001	[number]	k4	2001
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
premiérem	premiér	k1gMnSc7	premiér
Ariel	Ariel	k1gMnSc1	Ariel
Šaron	Šaron	k1gMnSc1	Šaron
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
prosadil	prosadit	k5eAaPmAgInS	prosadit
plán	plán	k1gInSc1	plán
na	na	k7c4	na
jednostranné	jednostranný	k2eAgNnSc4d1	jednostranné
stažení	stažení	k1gNnSc4	stažení
z	z	k7c2	z
Pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
a	a	k8xC	a
plán	plán	k1gInSc1	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
bariéry	bariéra	k1gFnSc2	bariéra
kolem	kolem	k7c2	kolem
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
kómatu	kóma	k1gNnSc2	kóma
<g/>
.	.	kIx.	.
</s>
<s>
Premiérský	premiérský	k2eAgInSc4d1	premiérský
post	post	k1gInSc4	post
pak	pak	k6eAd1	pak
převzal	převzít	k5eAaPmAgMnS	převzít
vicepremiér	vicepremiér	k1gMnSc1	vicepremiér
Ehud	Ehud	k1gMnSc1	Ehud
Olmert	Olmert	k1gMnSc1	Olmert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
začala	začít	k5eAaPmAgFnS	začít
druhá	druhý	k4xOgFnSc1	druhý
libanonská	libanonský	k2eAgFnSc1d1	libanonská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
znamenala	znamenat	k5eAaImAgFnS	znamenat
vytrvalé	vytrvalý	k2eAgNnSc4d1	vytrvalé
bombardování	bombardování	k1gNnSc4	bombardování
libanonských	libanonský	k2eAgInPc2d1	libanonský
cílů	cíl	k1gInPc2	cíl
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
ostřelování	ostřelování	k1gNnSc2	ostřelování
severu	sever	k1gInSc3	sever
Izraele	Izrael	k1gInSc2	Izrael
libanonskou	libanonský	k2eAgFnSc4d1	libanonská
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itskou	itský	k2eAgFnSc7d1	itská
milicí	milice	k1gFnSc7	milice
Hizballáh	Hizballáha	k1gFnPc2	Hizballáha
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
trval	trvat	k5eAaImAgInS	trvat
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
příměří	příměří	k1gNnSc2	příměří
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
zaznívaly	zaznívat	k5eAaImAgInP	zaznívat
silné	silný	k2eAgInPc4d1	silný
hlasy	hlas	k1gInPc4	hlas
kritizující	kritizující	k2eAgFnSc1d1	kritizující
premiéra	premiéra	k1gFnSc1	premiéra
Ehuda	Ehud	k1gMnSc2	Ehud
Olmerta	Olmert	k1gMnSc2	Olmert
a	a	k8xC	a
tehdejšího	tehdejší	k2eAgMnSc4d1	tehdejší
ministra	ministr	k1gMnSc4	ministr
obrany	obrana	k1gFnSc2	obrana
Amira	Amir	k1gMnSc4	Amir
Perece	Perece	k1gMnSc4	Perece
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
izraelský	izraelský	k2eAgMnSc1d1	izraelský
premiér	premiér	k1gMnSc1	premiér
Ehud	Ehud	k1gMnSc1	Ehud
Olmert	Olmert	k1gMnSc1	Olmert
a	a	k8xC	a
palestinský	palestinský	k2eAgMnSc1d1	palestinský
prezident	prezident	k1gMnSc1	prezident
Mahmúd	Mahmúda	k1gFnPc2	Mahmúda
Abbás	Abbás	k1gInSc4	Abbás
s	s	k7c7	s
jednáním	jednání	k1gNnSc7	jednání
nad	nad	k7c7	nad
všemi	všecek	k3xTgNnPc7	všecek
spornými	sporný	k2eAgNnPc7d1	sporné
tématy	téma	k1gNnPc7	téma
izraelsko-palestinského	izraelskoalestinský	k2eAgInSc2d1	izraelsko-palestinský
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
snahou	snaha	k1gFnSc7	snaha
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
dohody	dohoda	k1gFnPc4	dohoda
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
září	září	k1gNnSc2	září
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
podniklo	podniknout	k5eAaPmAgNnS	podniknout
Izraelské	izraelský	k2eAgNnSc1d1	izraelské
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
operaci	operace	k1gFnSc3	operace
Ovocný	ovocný	k2eAgInSc4d1	ovocný
sad	sad	k1gInSc4	sad
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
vybombardovalo	vybombardovat	k5eAaPmAgNnS	vybombardovat
syrský	syrský	k2eAgInSc4d1	syrský
jaderný	jaderný	k2eAgInSc4d1	jaderný
reaktor	reaktor	k1gInSc4	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
oznámil	oznámit	k5eAaPmAgMnS	oznámit
syrský	syrský	k2eAgMnSc1d1	syrský
prezident	prezident	k1gMnSc1	prezident
Bašár	Bašár	k1gMnSc1	Bašár
Asad	Asad	k1gMnSc1	Asad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sýrie	Sýrie	k1gFnSc1	Sýrie
a	a	k8xC	a
Izrael	Izrael	k1gInSc1	Izrael
již	již	k6eAd1	již
rok	rok	k1gInSc4	rok
jednají	jednat	k5eAaImIp3nP	jednat
za	za	k7c2	za
tureckého	turecký	k2eAgNnSc2d1	turecké
zprostředkování	zprostředkování	k1gNnSc2	zprostředkování
o	o	k7c6	o
mírové	mírový	k2eAgFnSc6d1	mírová
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
;	;	kIx,	;
Izrael	Izrael	k1gInSc1	Izrael
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
<g/>
Trvalou	trvalý	k2eAgFnSc4d1	trvalá
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
hrozbu	hrozba	k1gFnSc4	hrozba
představuje	představovat	k5eAaImIp3nS	představovat
opakující	opakující	k2eAgNnSc1d1	opakující
se	se	k3xPyFc4	se
ostřelování	ostřelování	k1gNnSc1	ostřelování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
palestinských	palestinský	k2eAgFnPc2d1	palestinská
radikálních	radikální	k2eAgFnPc2d1	radikální
skupin	skupina	k1gFnPc2	skupina
z	z	k7c2	z
Pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Izrael	Izrael	k1gInSc1	Izrael
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
jednostranně	jednostranně	k6eAd1	jednostranně
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
,	,	kIx,	,
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
Pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
k	k	k7c3	k
červenci	červenec	k1gInSc3	červenec
2014	[number]	k4	2014
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
raket	raketa	k1gFnPc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
a	a	k8xC	a
rozmístěn	rozmístěn	k2eAgInSc1d1	rozmístěn
protiraketový	protiraketový	k2eAgInSc1d1	protiraketový
systém	systém	k1gInSc1	systém
Železná	železný	k2eAgFnSc1d1	železná
kopule	kopule	k1gFnSc1	kopule
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
snižovat	snižovat	k5eAaImF	snižovat
riziko	riziko	k1gNnSc4	riziko
zasažení	zasažení	k1gNnSc2	zasažení
civilních	civilní	k2eAgFnPc2d1	civilní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
však	však	k9	však
raketovou	raketový	k2eAgFnSc4d1	raketová
a	a	k8xC	a
minometnou	minometný	k2eAgFnSc4d1	minometná
hrozbu	hrozba	k1gFnSc4	hrozba
odstranit	odstranit	k5eAaPmF	odstranit
nemůže	moct	k5eNaImIp3nS	moct
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc1	několik
vojenských	vojenský	k2eAgFnPc2d1	vojenská
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
operaci	operace	k1gFnSc6	operace
Lité	litý	k2eAgNnSc1d1	Lité
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
o	o	k7c4	o
operaci	operace	k1gFnSc4	operace
Pilíř	pilíř	k1gInSc4	pilíř
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
mezi	mezi	k7c7	mezi
červencem	červenec	k1gInSc7	červenec
a	a	k8xC	a
srpnem	srpen	k1gInSc7	srpen
2014	[number]	k4	2014
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
operace	operace	k1gFnSc1	operace
Ochranné	ochranný	k2eAgNnSc4d1	ochranné
ostří	ostří	k1gNnSc4	ostří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnPc4	geografie
a	a	k8xC	a
klima	klima	k1gNnSc4	klima
==	==	k?	==
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
jeho	jeho	k3xOp3gFnSc4	jeho
západní	západní	k2eAgFnSc4d1	západní
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
ohraničen	ohraničen	k2eAgInSc1d1	ohraničen
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Libanonem	Libanon	k1gInSc7	Libanon
<g/>
,	,	kIx,	,
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
Sýrií	Sýrie	k1gFnPc2	Sýrie
<g/>
,	,	kIx,	,
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
a	a	k8xC	a
z	z	k7c2	z
jihozápadu	jihozápad	k1gInSc2	jihozápad
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
získal	získat	k5eAaPmAgMnS	získat
během	během	k7c2	během
Šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
20	[number]	k4	20
770	[number]	k4	770
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
2	[number]	k4	2
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
spadající	spadající	k2eAgNnSc1d1	spadající
pod	pod	k7c4	pod
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
Východní	východní	k2eAgInSc4d1	východní
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
a	a	k8xC	a
Golanské	Golanský	k2eAgFnPc4d1	Golanský
výšiny	výšina	k1gFnPc4	výšina
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
22	[number]	k4	22
072	[number]	k4	072
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
území	území	k1gNnSc2	území
pod	pod	k7c7	pod
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
vládou	vláda	k1gFnSc7	vláda
včetně	včetně	k7c2	včetně
vojensky	vojensky	k6eAd1	vojensky
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
autonomních	autonomní	k2eAgNnPc2d1	autonomní
palestinských	palestinský	k2eAgNnPc2d1	palestinské
území	území	k1gNnPc2	území
na	na	k7c6	na
Západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
Jordánu	Jordán	k1gInSc2	Jordán
činí	činit	k5eAaImIp3nS	činit
27	[number]	k4	27
799	[number]	k4	799
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
malou	malý	k2eAgFnSc7d1	malá
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rozmanité	rozmanitý	k2eAgInPc4d1	rozmanitý
geografické	geografický	k2eAgInPc4d1	geografický
jevy	jev	k1gInPc4	jev
a	a	k8xC	a
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Negevská	Negevský	k2eAgFnSc1d1	Negevská
poušť	poušť	k1gFnSc1	poušť
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
či	či	k8xC	či
horské	horský	k2eAgInPc4d1	horský
hřbety	hřbet	k1gInPc4	hřbet
Galileje	Galilea	k1gFnSc2	Galilea
<g/>
,	,	kIx,	,
Karmelu	Karmela	k1gFnSc4	Karmela
a	a	k8xC	a
Golan	Golan	k1gInSc4	Golan
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
centrální	centrální	k2eAgFnSc2d1	centrální
vysočiny	vysočina	k1gFnSc2	vysočina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Jordánské	jordánský	k2eAgNnSc1d1	Jordánské
údolí	údolí	k1gNnSc1	údolí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
Velké	velký	k2eAgFnSc2d1	velká
příkopové	příkopový	k2eAgFnSc2d1	příkopová
propadliny	propadlina	k1gFnSc2	propadlina
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Jordán	Jordán	k1gInSc1	Jordán
teče	téct	k5eAaImIp3nS	téct
od	od	k7c2	od
hory	hora	k1gFnSc2	hora
Hermon	Hermon	k1gNnSc1	Hermon
přes	přes	k7c4	přes
Chulské	Chulský	k2eAgNnSc4d1	Chulský
údolí	údolí	k1gNnSc4	údolí
a	a	k8xC	a
Galilejské	galilejský	k2eAgNnSc4d1	Galilejské
jezero	jezero	k1gNnSc4	jezero
do	do	k7c2	do
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
nejníže	nízce	k6eAd3	nízce
položeným	položený	k2eAgNnSc7d1	položené
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
údolí	údolí	k1gNnSc2	údolí
Vádí	vádí	k1gNnSc2	vádí
al-Araba	al-Arab	k1gMnSc2	al-Arab
končící	končící	k2eAgInSc4d1	končící
v	v	k7c6	v
Akabském	Akabský	k2eAgInSc6d1	Akabský
zálivu	záliv	k1gInSc6	záliv
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
Sinajský	sinajský	k2eAgInSc4d1	sinajský
poloostrov	poloostrov	k1gInSc4	poloostrov
jsou	být	k5eAaImIp3nP	být
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
erodované	erodovaný	k2eAgInPc1d1	erodovaný
kráterovité	kráterovitý	k2eAgInPc1d1	kráterovitý
útvary	útvar	k1gInPc1	útvar
zvané	zvaný	k2eAgInPc1d1	zvaný
machtešim	machtešim	k6eAd1	machtešim
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
machteš	machtat	k5eAaBmIp2nS	machtat
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
Machteš	Machteš	k1gFnSc1	Machteš
Ramon	Ramona	k1gFnPc2	Ramona
v	v	k7c6	v
Negevské	Negevský	k2eAgFnSc6d1	Negevská
poušti	poušť	k1gFnSc6	poušť
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
a	a	k8xC	a
10	[number]	k4	10
km	km	kA	km
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Extrémní	extrémní	k2eAgInPc1d1	extrémní
rozdíly	rozdíl	k1gInPc1	rozdíl
</s>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
−	−	k?	−
metrů	metr	k1gInPc2	metr
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
hora	hora	k1gFnSc1	hora
Hermon	Hermona	k1gFnPc2	Hermona
na	na	k7c6	na
Golanských	Golanský	k2eAgFnPc6d1	Golanský
výšinách	výšina	k1gFnPc6	výšina
2	[number]	k4	2
814	[number]	k4	814
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
hora	hora	k1gFnSc1	hora
Meron	Meron	k1gInSc1	Meron
1208	[number]	k4	1208
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Klima	klima	k1gNnSc1	klima
===	===	k?	===
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
převládá	převládat	k5eAaImIp3nS	převládat
chladné	chladný	k2eAgNnSc1d1	chladné
a	a	k8xC	a
větrné	větrný	k2eAgNnSc1d1	větrné
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
sněžením	sněžení	k1gNnSc7	sněžení
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
hory	hora	k1gFnSc2	hora
Hermon	Hermona	k1gFnPc2	Hermona
má	mít	k5eAaImIp3nS	mít
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
sněhovou	sněhový	k2eAgFnSc4d1	sněhová
pokrývku	pokrývka	k1gFnSc4	pokrývka
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
obvykle	obvykle	k6eAd1	obvykle
sněží	sněžit	k5eAaImIp3nS	sněžit
i	i	k9	i
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k1gMnPc1	pobřežní
města	město	k1gNnSc2	město
jako	jako	k8xC	jako
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
a	a	k8xC	a
Haifa	Haifa	k1gFnSc1	Haifa
mají	mít	k5eAaImIp3nP	mít
chladné	chladný	k2eAgInPc1d1	chladný
<g/>
,	,	kIx,	,
deštivé	deštivý	k2eAgInPc1d1	deštivý
zimy	zima	k1gFnPc4	zima
a	a	k8xC	a
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
a	a	k8xC	a
teplá	teplý	k2eAgNnPc4d1	teplé
léta	léto	k1gNnPc4	léto
−	−	k?	−
tj.	tj.	kA	tj.
středozemní	středozemní	k2eAgNnSc1d1	středozemní
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Tirat	Tirat	k2eAgMnSc1d1	Tirat
Cvi	Cvi	k1gMnSc1	Cvi
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Jordánského	jordánský	k2eAgNnSc2d1	Jordánské
údolí	údolí	k1gNnSc2	údolí
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
53,7	[number]	k4	53,7
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
květnem	květno	k1gNnSc7	květno
a	a	k8xC	a
zářím	září	k1gNnSc7	září
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
déšť	déšť	k1gInSc1	déšť
vzácností	vzácnost	k1gFnPc2	vzácnost
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
malému	malý	k2eAgNnSc3d1	malé
množství	množství	k1gNnSc3	množství
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
izraelští	izraelský	k2eAgMnPc1d1	izraelský
vědci	vědec	k1gMnPc1	vědec
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
také	také	k9	také
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
technologií	technologie	k1gFnPc2	technologie
minimalizujících	minimalizující	k2eAgInPc2d1	minimalizující
spotřebu	spotřeba	k1gFnSc4	spotřeba
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
vynálezů	vynález	k1gInPc2	vynález
je	být	k5eAaImIp3nS	být
kapková	kapkový	k2eAgFnSc1d1	kapková
závlaha	závlaha	k1gFnSc1	závlaha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pěstitelům	pěstitel	k1gMnPc3	pěstitel
úsporně	úsporně	k6eAd1	úsporně
dávkovat	dávkovat	k5eAaImF	dávkovat
vodu	voda	k1gFnSc4	voda
každé	každý	k3xTgFnSc3	každý
rostlině	rostlina	k1gFnSc3	rostlina
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Izraelci	Izraelec	k1gMnPc1	Izraelec
využívají	využívat	k5eAaImIp3nP	využívat
příznivých	příznivý	k2eAgFnPc2d1	příznivá
geografických	geografický	k2eAgFnPc2d1	geografická
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
solární	solární	k2eAgFnSc2d1	solární
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
produkci	produkce	k1gFnSc6	produkce
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
pluralitním	pluralitní	k2eAgInSc7d1	pluralitní
politickým	politický	k2eAgInSc7d1	politický
systémem	systém	k1gInSc7	systém
a	a	k8xC	a
všeobecným	všeobecný	k2eAgNnSc7d1	všeobecné
rovným	rovný	k2eAgNnSc7d1	rovné
volebním	volební	k2eAgNnSc7d1	volební
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
parlamentem	parlament	k1gInSc7	parlament
na	na	k7c4	na
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Premiérem	premiér	k1gMnSc7	premiér
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
pouze	pouze	k6eAd1	pouze
člen	člen	k1gMnSc1	člen
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získá	získat	k5eAaPmIp3nS	získat
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
−	−	k?	−
obvykle	obvykle	k6eAd1	obvykle
to	ten	k3xDgNnSc4	ten
bývá	bývat	k5eAaImIp3nS	bývat
předseda	předseda	k1gMnSc1	předseda
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
Kneset	Kneset	k1gInSc1	Kneset
tvořený	tvořený	k2eAgInSc1d1	tvořený
120	[number]	k4	120
poslanci	poslanec	k1gMnPc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poměrného	poměrný	k2eAgInSc2d1	poměrný
volebního	volební	k2eAgInSc2d1	volební
systému	systém	k1gInSc2	systém
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
parlament	parlament	k1gInSc1	parlament
odvolá	odvolat	k5eAaPmIp3nS	odvolat
premiéra	premiér	k1gMnSc4	premiér
nebo	nebo	k8xC	nebo
vládě	vláda	k1gFnSc6	vláda
nevysloví	vyslovit	k5eNaPmIp3nS	vyslovit
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Kneset	Kneset	k1gMnSc1	Kneset
rozpuštěn	rozpuštěn	k2eAgMnSc1d1	rozpuštěn
předčasně	předčasně	k6eAd1	předčasně
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
nemá	mít	k5eNaImIp3nS	mít
psanou	psaný	k2eAgFnSc4d1	psaná
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
ukotven	ukotvit	k5eAaPmNgInS	ukotvit
v	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
základních	základní	k2eAgInPc6d1	základní
zákonech	zákon	k1gInPc6	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
začal	začít	k5eAaPmAgInS	začít
Kneset	Kneset	k1gInSc1	Kneset
vypracovávat	vypracovávat	k5eAaImF	vypracovávat
návrh	návrh	k1gInSc4	návrh
oficiální	oficiální	k2eAgFnSc2d1	oficiální
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
zákonech	zákon	k1gInPc6	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
má	mít	k5eAaImIp3nS	mít
trojstupňový	trojstupňový	k2eAgInSc1d1	trojstupňový
soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
úrovni	úroveň	k1gFnSc6	úroveň
jsou	být	k5eAaImIp3nP	být
městské	městský	k2eAgInPc1d1	městský
soudy	soud	k1gInPc1	soud
sídlící	sídlící	k2eAgInPc1d1	sídlící
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc7d2	vyšší
instancí	instance	k1gFnSc7	instance
jsou	být	k5eAaImIp3nP	být
distriktní	distriktní	k2eAgInPc1d1	distriktní
soudy	soud	k1gInPc1	soud
sloužící	sloužící	k2eAgInPc1d1	sloužící
jako	jako	k8xS	jako
odvolací	odvolací	k2eAgInPc1d1	odvolací
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
jako	jako	k8xS	jako
soudy	soud	k1gInPc4	soud
první	první	k4xOgFnSc2	první
instance	instance	k1gFnSc2	instance
<g/>
;	;	kIx,	;
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
pěti	pět	k4xCc6	pět
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
izraelských	izraelský	k2eAgInPc2d1	izraelský
distriktů	distrikt	k1gInPc2	distrikt
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
Jeruzalémský	jeruzalémský	k2eAgInSc4d1	jeruzalémský
distrikt	distrikt	k1gInSc4	distrikt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
stupeň	stupeň	k1gInSc4	stupeň
je	být	k5eAaImIp3nS	být
Nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
soud	soud	k1gInSc4	soud
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
odvolací	odvolací	k2eAgInSc1d1	odvolací
soud	soud	k1gInSc1	soud
a	a	k8xC	a
soud	soud	k1gInSc1	soud
první	první	k4xOgFnSc2	první
instance	instance	k1gFnSc2	instance
v	v	k7c6	v
závažných	závažný	k2eAgInPc6d1	závažný
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zpochybněna	zpochybnit	k5eAaPmNgFnS	zpochybnit
zákonnost	zákonnost	k1gFnSc1	zákonnost
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
a	a	k8xC	a
postupů	postup	k1gInPc2	postup
představitelů	představitel	k1gMnPc2	představitel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
není	být	k5eNaImIp3nS	být
členem	člen	k1gInSc7	člen
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
trestního	trestní	k2eAgInSc2d1	trestní
soudu	soud	k1gInSc2	soud
kvůli	kvůli	k7c3	kvůli
obavě	obava	k1gFnSc3	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
soud	soud	k1gInSc1	soud
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
vůči	vůči	k7c3	vůči
Izraeli	Izrael	k1gInSc3	Izrael
předpojatý	předpojatý	k2eAgMnSc1d1	předpojatý
<g/>
.	.	kIx.	.
</s>
<s>
Izraelský	izraelský	k2eAgInSc1d1	izraelský
právní	právní	k2eAgInSc1d1	právní
systém	systém	k1gInSc1	systém
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
britské	britský	k2eAgNnSc4d1	Britské
zvykové	zvykový	k2eAgNnSc4d1	zvykové
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
kontinentální	kontinentální	k2eAgNnSc4d1	kontinentální
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
židovské	židovský	k2eAgNnSc4d1	Židovské
náboženské	náboženský	k2eAgNnSc4d1	náboženské
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
stare	star	k1gInSc5	star
decisis	decisis	k1gFnSc1	decisis
(	(	kIx(	(
<g/>
precedens	precedens	k1gNnSc1	precedens
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
obžalovacím	obžalovací	k2eAgInSc6d1	obžalovací
způsobu	způsob	k1gInSc6	způsob
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
strany	strana	k1gFnPc1	strana
sporu	spor	k1gInSc2	spor
předkládají	předkládat	k5eAaImIp3nP	předkládat
soudu	soud	k1gInSc2	soud
důkazní	důkazní	k2eAgInPc1d1	důkazní
materiály	materiál	k1gInPc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Svatby	svatba	k1gFnPc1	svatba
a	a	k8xC	a
rozvody	rozvod	k1gInPc1	rozvod
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jurisdikci	jurisdikce	k1gFnSc6	jurisdikce
židovských	židovský	k2eAgInPc2d1	židovský
<g/>
,	,	kIx,	,
muslimských	muslimský	k2eAgInPc2d1	muslimský
<g/>
,	,	kIx,	,
drúzských	drúzský	k2eAgInPc2d1	drúzský
a	a	k8xC	a
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
náboženských	náboženský	k2eAgInPc2d1	náboženský
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
<g/>
Izraelský	izraelský	k2eAgInSc1d1	izraelský
základní	základní	k2eAgInSc1d1	základní
zákon	zákon	k1gInSc1	zákon
Lidská	lidský	k2eAgFnSc1d1	lidská
svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
důstojnost	důstojnost	k1gFnSc1	důstojnost
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
lidská	lidský	k2eAgFnSc1d1	lidská
právy	právo	k1gNnPc7	právo
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
země	země	k1gFnSc1	země
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
označená	označený	k2eAgFnSc1d1	označená
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
organizací	organizace	k1gFnPc2	organizace
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
za	za	k7c4	za
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
lidských	lidský	k2eAgNnPc2d1	lidské
a	a	k8xC	a
politických	politický	k2eAgNnPc2d1	politické
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
území	území	k1gNnSc1	území
Palestinské	palestinský	k2eAgFnSc2d1	palestinská
autonomie	autonomie	k1gFnSc2	autonomie
bylo	být	k5eAaImAgNnS	být
touto	tento	k3xDgFnSc7	tento
organizací	organizace	k1gFnPc2	organizace
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
jako	jako	k8xS	jako
nesvobodné	svobodný	k2eNgFnPc1d1	nesvobodná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
svobody	svoboda	k1gFnSc2	svoboda
tisku	tisk	k1gInSc2	tisk
organizace	organizace	k1gFnSc2	organizace
Reportéři	reportér	k1gMnPc1	reportér
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
se	se	k3xPyFc4	se
Izrael	Izrael	k1gInSc1	Izrael
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
96	[number]	k4	96
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
ze	z	k7c2	z
180	[number]	k4	180
hodnocených	hodnocený	k2eAgFnPc2d1	hodnocená
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
arabsko-izraelským	arabskozraelský	k2eAgInSc7d1	arabsko-izraelský
konfliktem	konflikt	k1gInSc7	konflikt
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
organizace	organizace	k1gFnPc4	organizace
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnSc2	International
a	a	k8xC	a
Human	Human	k1gInSc1	Human
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watcha	k1gFnPc2	Watcha
porušování	porušování	k1gNnSc2	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Porušování	porušování	k1gNnSc1	porušování
občanských	občanský	k2eAgFnPc2d1	občanská
svobod	svoboda	k1gFnPc2	svoboda
na	na	k7c6	na
okupovaných	okupovaný	k2eAgNnPc6d1	okupované
územích	území	k1gNnPc6	území
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
nevládní	vládní	k2eNgFnSc7d1	nevládní
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
organizací	organizace	k1gFnSc7	organizace
Be-celem	Beel	k1gInSc7	Be-cel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
uzákoněn	uzákoněn	k2eAgInSc1d1	uzákoněn
systém	systém	k1gInSc1	systém
státní	státní	k2eAgFnSc2d1	státní
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
šesti	šest	k4xCc2	šest
hlavních	hlavní	k2eAgInPc2d1	hlavní
distriktů	distrikt	k1gInPc2	distrikt
(	(	kIx(	(
<g/>
mechozot	mechozot	k1gInSc1	mechozot
<g/>
,	,	kIx,	,
singulár	singulár	k1gInSc1	singulár
machoz	machoz	k1gInSc1	machoz
<g/>
)	)	kIx)	)
–	–	k?	–
Centrální	centrální	k2eAgInSc1d1	centrální
distrikt	distrikt	k1gInSc1	distrikt
<g/>
,	,	kIx,	,
Haifský	haifský	k2eAgInSc1d1	haifský
distrikt	distrikt	k1gInSc1	distrikt
<g/>
,	,	kIx,	,
Jeruzalémský	jeruzalémský	k2eAgInSc1d1	jeruzalémský
distrikt	distrikt	k1gInSc1	distrikt
<g/>
,	,	kIx,	,
Severní	severní	k2eAgInSc1d1	severní
distrikt	distrikt	k1gInSc1	distrikt
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgInSc1d1	jižní
distrikt	distrikt	k1gInSc1	distrikt
a	a	k8xC	a
Telavivský	telavivský	k2eAgInSc1d1	telavivský
distrikt	distrikt	k1gInSc1	distrikt
<g/>
.	.	kIx.	.
</s>
<s>
Distrikty	distrikt	k1gInPc1	distrikt
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
patnácti	patnáct	k4xCc2	patnáct
subdistriktů	subdistrikt	k1gInPc2	subdistrikt
(	(	kIx(	(
<g/>
nafot	nafot	k1gInSc1	nafot
<g/>
,	,	kIx,	,
singulár	singulár	k1gInSc1	singulár
nafa	nafa	k1gFnSc1	nafa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
subdistrikt	subdistrikt	k1gInSc1	subdistrikt
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
přirozené	přirozený	k2eAgFnSc6d1	přirozená
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kterých	který	k3yIgNnPc2	který
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
padesát	padesát	k4xCc1	padesát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgFnPc4	tři
metropolitní	metropolitní	k2eAgFnPc4d1	metropolitní
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
žilo	žít	k5eAaImAgNnS	žít
celkem	celek	k1gInSc7	celek
5	[number]	k4	5
113	[number]	k4	113
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Tel	tel	kA	tel
Aviv	Aviva	k1gFnPc2	Aviva
(	(	kIx(	(
<g/>
3	[number]	k4	3
464	[number]	k4	464
100	[number]	k4	100
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Haifa	Haifa	k1gFnSc1	Haifa
(	(	kIx(	(
<g/>
1	[number]	k4	1
073	[number]	k4	073
900	[number]	k4	900
<g/>
)	)	kIx)	)
a	a	k8xC	a
Beerševa	Beerševa	k1gFnSc1	Beerševa
(	(	kIx(	(
<g/>
574	[number]	k4	574
900	[number]	k4	900
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
do	do	k7c2	do
rozlohy	rozloha	k1gFnSc2	rozloha
i	i	k8xC	i
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
témuž	týž	k3xTgInSc3	týž
roku	rok	k1gInSc3	rok
největším	veliký	k2eAgNnSc7d3	veliký
izraelským	izraelský	k2eAgNnSc7d1	izraelské
městem	město	k1gNnSc7	město
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
s	s	k7c7	s
815	[number]	k4	815
300	[number]	k4	300
obyvateli	obyvatel	k1gMnPc7	obyvatel
a	a	k8xC	a
126	[number]	k4	126
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
do	do	k7c2	do
lidnatosti	lidnatost	k1gFnSc2	lidnatost
jej	on	k3xPp3gMnSc4	on
následovali	následovat	k5eAaImAgMnP	následovat
Tel	tel	kA	tel
Aviv	Aviva	k1gFnPc2	Aviva
(	(	kIx(	(
<g/>
414	[number]	k4	414
600	[number]	k4	600
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Haifa	Haifa	k1gFnSc1	Haifa
(	(	kIx(	(
<g/>
272	[number]	k4	272
200	[number]	k4	200
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rišon	Rišon	k1gMnSc1	Rišon
le-Cijon	le-Cijon	k1gMnSc1	le-Cijon
(	(	kIx(	(
<g/>
235	[number]	k4	235
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Okupovaná	okupovaný	k2eAgNnPc1d1	okupované
území	území	k1gNnPc1	území
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
Izraelem	Izrael	k1gInSc7	Izrael
okupovaná	okupovaný	k2eAgNnPc1d1	okupované
území	území	k1gNnPc1	území
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
území	území	k1gNnSc3	území
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
,	,	kIx,	,
Východního	východní	k2eAgInSc2d1	východní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
a	a	k8xC	a
Golanských	Golanský	k2eAgFnPc2d1	Golanský
výšin	výšina	k1gFnPc2	výšina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
Šestidenní	šestidenní	k2eAgFnSc6d1	šestidenní
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
zabrána	zabrána	k1gFnSc1	zabrána
Egyptu	Egypt	k1gInSc3	Egypt
<g/>
,	,	kIx,	,
Jordánsku	Jordánsko	k1gNnSc3	Jordánsko
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc3	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
také	také	k9	také
pro	pro	k7c4	pro
Sinajský	sinajský	k2eAgInSc4d1	sinajský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
navrácený	navrácený	k2eAgInSc4d1	navrácený
Egyptu	Egypt	k1gInSc3	Egypt
podle	podle	k7c2	podle
Egyptsko-izraelské	egyptskozraelský	k2eAgFnSc2d1	egyptsko-izraelská
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
těchto	tento	k3xDgNnPc6	tento
územích	území	k1gNnPc6	území
byly	být	k5eAaImAgFnP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
izraelské	izraelský	k2eAgFnPc1d1	izraelská
osady	osada	k1gFnPc1	osada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
a	a	k8xC	a
na	na	k7c6	na
Golanských	Golanský	k2eAgFnPc6d1	Golanský
výšinách	výšina	k1gFnPc6	výšina
je	být	k5eAaImIp3nS	být
uplatňováno	uplatňovat	k5eAaImNgNnS	uplatňovat
izraelské	izraelský	k2eAgNnSc1d1	izraelské
právo	právo	k1gNnSc1	právo
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
území	území	k1gNnSc3	území
byla	být	k5eAaImAgFnS	být
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
vlastního	vlastní	k2eAgInSc2d1	vlastní
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
občanům	občan	k1gMnPc3	občan
bylo	být	k5eAaImAgNnS	být
nabídnuto	nabídnut	k2eAgNnSc1d1	nabídnuto
izraelské	izraelský	k2eAgNnSc1d1	izraelské
občanství	občanství	k1gNnSc1	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Západní	západní	k2eAgInSc1d1	západní
břeh	břeh	k1gInSc1	břeh
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pod	pod	k7c7	pod
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
tamní	tamní	k2eAgFnSc2d1	tamní
osady	osada	k1gFnSc2	osada
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
450	[number]	k4	450
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jednání	jednání	k1gNnSc2	jednání
o	o	k7c6	o
těchto	tento	k3xDgNnPc6	tento
územích	území	k1gNnPc6	území
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rezoluce	rezoluce	k1gFnSc2	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
242	[number]	k4	242
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
požaduje	požadovat	k5eAaImIp3nS	požadovat
stažení	stažení	k1gNnSc4	stažení
Izraele	Izrael	k1gInSc2	Izrael
z	z	k7c2	z
okupovaných	okupovaný	k2eAgNnPc2d1	okupované
území	území	k1gNnPc2	území
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
návrat	návrat	k1gInSc4	návrat
arabským	arabský	k2eAgFnPc3d1	arabská
státům	stát	k1gInPc3	stát
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
palestinských	palestinský	k2eAgMnPc2d1	palestinský
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
uprchlíků	uprchlík	k1gMnPc2	uprchlík
Války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
žili	žít	k5eAaImAgMnP	žít
Arabové	Arab	k1gMnPc1	Arab
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
územích	území	k1gNnPc6	území
pod	pod	k7c7	pod
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Dohod	dohoda	k1gFnPc2	dohoda
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
a	a	k8xC	a
Dopisů	dopis	k1gInPc2	dopis
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
uznání	uznání	k1gNnSc6	uznání
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
palestinské	palestinský	k2eAgFnSc2d1	palestinská
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
měst	město	k1gNnPc2	město
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
tzv.	tzv.	kA	tzv.
Palestinské	palestinský	k2eAgFnSc2d1	palestinská
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Izrael	Izrael	k1gInSc1	Izrael
nad	nad	k7c7	nad
většinou	většina	k1gFnSc7	většina
území	území	k1gNnSc2	území
stále	stále	k6eAd1	stále
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
vojenský	vojenský	k2eAgInSc4d1	vojenský
dohled	dohled	k1gInSc4	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vzrůstajícím	vzrůstající	k2eAgInPc3d1	vzrůstající
sebevraženým	sebevražený	k2eAgInPc3d1	sebevražený
útokům	útok	k1gInPc3	útok
při	při	k7c6	při
Druhé	druhý	k4xOgFnSc6	druhý
intifádě	intifáda	k1gFnSc6	intifáda
(	(	kIx(	(
<g/>
také	také	k9	také
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
Oslo	Oslo	k1gNnSc1	Oslo
válce	válec	k1gInSc2	válec
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
izraelská	izraelský	k2eAgFnSc1d1	izraelská
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vybudovat	vybudovat	k5eAaPmF	vybudovat
kolem	kolem	k7c2	kolem
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
bariéru	bariéra	k1gFnSc4	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
Izrael	Izrael	k1gInSc1	Izrael
své	svůj	k3xOyFgMnPc4	svůj
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
vojenské	vojenský	k2eAgFnPc4d1	vojenská
síly	síla	k1gFnPc4	síla
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
a	a	k8xC	a
čtyř	čtyři	k4xCgFnPc2	čtyři
osad	osada	k1gFnPc2	osada
na	na	k7c6	na
Západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
ovšem	ovšem	k9	ovšem
fakticky	fakticky	k6eAd1	fakticky
vojensky	vojensky	k6eAd1	vojensky
i	i	k8xC	i
ekonomicky	ekonomicky	k6eAd1	ekonomicky
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
udržuje	udržovat	k5eAaImIp3nS	udržovat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
161	[number]	k4	161
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
má	mít	k5eAaImIp3nS	mít
94	[number]	k4	94
diplomatických	diplomatický	k2eAgNnPc2d1	diplomatické
zastoupení	zastoupení	k1gNnPc2	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Ligy	liga	k1gFnSc2	liga
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
mají	mít	k5eAaImIp3nP	mít
s	s	k7c7	s
Izraelem	Izrael	k1gMnSc7	Izrael
plné	plný	k2eAgInPc4d1	plný
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
pouze	pouze	k6eAd1	pouze
Egypt	Egypt	k1gInSc1	Egypt
a	a	k8xC	a
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
mírové	mírový	k2eAgFnPc4d1	mírová
smlouvy	smlouva	k1gFnPc4	smlouva
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979	[number]	k4	1979
a	a	k8xC	a
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
navázala	navázat	k5eAaPmAgFnS	navázat
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
státy	stát	k1gInPc4	stát
Ligy	liga	k1gFnSc2	liga
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
–	–	k?	–
Maroko	Maroko	k1gNnSc4	Maroko
a	a	k8xC	a
Tunisko	Tunisko	k1gNnSc4	Tunisko
–	–	k?	–
ukončily	ukončit	k5eAaPmAgFnP	ukončit
své	svůj	k3xOyFgInPc4	svůj
částečné	částečný	k2eAgInPc4d1	částečný
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
počátkem	počátkem	k7c2	počátkem
Druhé	druhý	k4xOgFnSc2	druhý
intifády	intifáda	k1gFnSc2	intifáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Izraele	Izrael	k1gInSc2	Izrael
navštívil	navštívit	k5eAaPmAgInS	navštívit
Maroko	Maroko	k1gNnSc4	Maroko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
izraelsko-marocké	izraelskoarocký	k2eAgInPc1d1	izraelsko-marocký
vztahy	vztah	k1gInPc1	vztah
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
izraelského	izraelský	k2eAgInSc2d1	izraelský
zákona	zákon	k1gInSc2	zákon
jsou	být	k5eAaImIp3nP	být
Libanon	Libanon	k1gInSc1	Libanon
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
a	a	k8xC	a
Jemen	Jemen	k1gInSc1	Jemen
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
země	zem	k1gFnSc2	zem
a	a	k8xC	a
občané	občan	k1gMnPc1	občan
Izraele	Izrael	k1gInSc2	Izrael
je	on	k3xPp3gFnPc4	on
nesmí	smět	k5eNaImIp3nS	smět
navštívit	navštívit	k5eAaPmF	navštívit
bez	bez	k7c2	bez
povolení	povolení	k1gNnSc2	povolení
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
je	být	k5eAaImIp3nS	být
Izrael	Izrael	k1gInSc1	Izrael
součástí	součást	k1gFnPc2	součást
Středomořského	středomořský	k2eAgInSc2d1	středomořský
dialogu	dialog	k1gInSc2	dialog
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
sedm	sedm	k4xCc4	sedm
zemí	zem	k1gFnPc2	zem
ležících	ležící	k2eAgFnPc2d1	ležící
kolem	kolem	k7c2	kolem
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
aliance	aliance	k1gFnSc1	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejbližší	blízký	k2eAgMnPc4d3	nejbližší
izraelské	izraelský	k2eAgMnPc4d1	izraelský
spojence	spojenec	k1gMnPc4	spojenec
patří	patřit	k5eAaImIp3nP	patřit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
byly	být	k5eAaImAgFnP	být
první	první	k4xOgNnSc4	první
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uznala	uznat	k5eAaPmAgFnS	uznat
Izrael	Izrael	k1gInSc4	Izrael
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
jej	on	k3xPp3gMnSc4	on
uznal	uznat	k5eAaPmAgInS	uznat
i	i	k9	i
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
stejným	stejný	k2eAgFnPc3d1	stejná
politickým	politický	k2eAgFnPc3d1	politická
a	a	k8xC	a
náboženským	náboženský	k2eAgFnPc3d1	náboženská
hodnotám	hodnota	k1gFnPc3	hodnota
pokládají	pokládat	k5eAaImIp3nP	pokládat
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
Izrael	Izrael	k1gInSc1	Izrael
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nejbližšího	blízký	k2eAgMnSc4d3	nejbližší
spojence	spojenec	k1gMnSc4	spojenec
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgInPc1d1	silný
vztahy	vztah	k1gInPc1	vztah
Německa	Německo	k1gNnSc2	Německo
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
snahy	snaha	k1gFnSc2	snaha
odškodnit	odškodnit	k5eAaPmF	odškodnit
a	a	k8xC	a
napravit	napravit	k5eAaPmF	napravit
holokaust	holokaust	k1gInSc4	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
a	a	k8xC	a
vzdělávacích	vzdělávací	k2eAgFnPc6d1	vzdělávací
aktivitách	aktivita	k1gFnPc6	aktivita
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
silnými	silný	k2eAgInPc7d1	silný
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
a	a	k8xC	a
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
partnery	partner	k1gMnPc7	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
náklonnost	náklonnost	k1gFnSc1	náklonnost
Izraeli	Izrael	k1gInSc3	Izrael
pramení	pramenit	k5eAaImIp3nS	pramenit
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
odkazu	odkaz	k1gInSc2	odkaz
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
i	i	k9	i
poté	poté	k6eAd1	poté
podporoval	podporovat	k5eAaImAgInS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Indie	Indie	k1gFnSc1	Indie
navázala	navázat	k5eAaPmAgFnS	navázat
plné	plný	k2eAgInPc4d1	plný
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
udržuje	udržovat	k5eAaImIp3nS	udržovat
silné	silný	k2eAgNnSc4d1	silné
vojenské	vojenský	k2eAgNnSc4d1	vojenské
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc4d1	kulturní
partnerství	partnerství	k1gNnSc4	partnerství
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
vztah	vztah	k1gInSc1	vztah
již	již	k6eAd1	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Britského	britský	k2eAgInSc2d1	britský
mandátu	mandát	k1gInSc2	mandát
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
,	,	kIx,	,
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
udržuje	udržovat	k5eAaImIp3nS	udržovat
silné	silný	k2eAgFnPc4d1	silná
obchodní	obchodní	k2eAgFnPc4d1	obchodní
vazby	vazba	k1gFnPc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
měl	mít	k5eAaImAgInS	mít
Izrael	Izrael	k1gInSc1	Izrael
za	za	k7c4	za
dynastie	dynastie	k1gFnPc4	dynastie
Pahlaví	Pahlavý	k2eAgMnPc1d1	Pahlavý
i	i	k8xC	i
s	s	k7c7	s
Íránem	Írán	k1gInSc7	Írán
<g/>
,	,	kIx,	,
během	během	k7c2	během
íránské	íránský	k2eAgFnSc2d1	íránská
revoluce	revoluce	k1gFnSc2	revoluce
však	však	k9	však
Írán	Írán	k1gInSc1	Írán
uznání	uznání	k1gNnSc2	uznání
Izraele	Izrael	k1gInSc2	Izrael
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgNnSc1d1	silné
spojenectví	spojenectví	k1gNnSc1	spojenectví
pojilo	pojit	k5eAaImAgNnS	pojit
Izrael	Izrael	k1gInSc4	Izrael
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
spolu	spolu	k6eAd1	spolu
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
navázaly	navázat	k5eAaPmAgFnP	navázat
plné	plný	k2eAgInPc4d1	plný
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
probíhala	probíhat	k5eAaImAgFnS	probíhat
spolupráce	spolupráce	k1gFnSc1	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
výrazně	výrazně	k6eAd1	výrazně
utrpěly	utrpět	k5eAaPmAgInP	utrpět
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
izraelského	izraelský	k2eAgInSc2d1	izraelský
zásahu	zásah	k1gInSc2	zásah
proti	proti	k7c3	proti
konvoji	konvoj	k1gInSc3	konvoj
do	do	k7c2	do
Pásma	pásmo	k1gNnSc2	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
devět	devět	k4xCc1	devět
Turků	Turek	k1gMnPc2	Turek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
předstátní	předstátní	k2eAgFnSc6d1	předstátní
a	a	k8xC	a
v	v	k7c6	v
počátečních	počáteční	k2eAgInPc6d1	počáteční
letech	let	k1gInPc6	let
existence	existence	k1gFnSc2	existence
státu	stát	k1gInSc2	stát
měl	mít	k5eAaImAgInS	mít
Izrael	Izrael	k1gInSc1	Izrael
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
mu	on	k3xPp3gMnSc3	on
i	i	k9	i
přes	přes	k7c4	přes
embargo	embargo	k1gNnSc4	embargo
OSN	OSN	kA	OSN
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
a	a	k8xC	a
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
První	první	k4xOgFnSc2	první
arabsko-izraelské	arabskozraelský	k2eAgFnSc2d1	arabsko-izraelská
války	válka	k1gFnSc2	válka
pro	pro	k7c4	pro
mladý	mladý	k2eAgInSc4d1	mladý
stát	stát	k1gInSc4	stát
životně	životně	k6eAd1	životně
důležitou	důležitý	k2eAgFnSc4d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
změně	změna	k1gFnSc6	změna
izraelské	izraelský	k2eAgFnSc2d1	izraelská
politiky	politika	k1gFnSc2	politika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
však	však	k9	však
Československo	Československo	k1gNnSc1	Československo
začalo	začít	k5eAaPmAgNnS	začít
naopak	naopak	k6eAd1	naopak
podporovat	podporovat	k5eAaImF	podporovat
a	a	k8xC	a
vyzbrojovat	vyzbrojovat	k5eAaImF	vyzbrojovat
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
a	a	k8xC	a
Izrael	Izrael	k1gInSc1	Izrael
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
až	až	k9	až
do	do	k7c2	do
Sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
nepřátelskou	přátelský	k2eNgFnSc7d1	nepřátelská
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Československem	Československo	k1gNnSc7	Československo
byly	být	k5eAaImAgFnP	být
obnoveny	obnoven	k2eAgFnPc1d1	obnovena
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Izraelem	Izrael	k1gInSc7	Izrael
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
bilaterálních	bilaterální	k2eAgFnPc2d1	bilaterální
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
ČSFR	ČSFR	kA	ČSFR
(	(	kIx(	(
<g/>
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
letecké	letecký	k2eAgFnSc6d1	letecká
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
ty	ten	k3xDgFnPc1	ten
smlouvy	smlouva	k1gFnPc1	smlouva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
unijním	unijní	k2eAgNnSc7d1	unijní
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
důležité	důležitý	k2eAgFnPc4d1	důležitá
mezistátní	mezistátní	k2eAgFnPc4d1	mezistátní
dohody	dohoda	k1gFnPc4	dohoda
patří	patřit	k5eAaImIp3nS	patřit
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c4	o
zamezení	zamezení	k1gNnSc4	zamezení
dvojího	dvojí	k4xRgNnSc2	dvojí
zdanění	zdanění	k1gNnSc2	zdanění
a	a	k8xC	a
zabránění	zabránění	k1gNnSc2	zabránění
daňového	daňový	k2eAgInSc2d1	daňový
úniku	únik	k1gInSc2	únik
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
daní	daň	k1gFnPc2	daň
z	z	k7c2	z
příjmu	příjem	k1gInSc2	příjem
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
bezvízovém	bezvízový	k2eAgInSc6d1	bezvízový
styku	styk	k1gInSc6	styk
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
pomoci	pomoc	k1gFnSc6	pomoc
v	v	k7c6	v
celních	celní	k2eAgFnPc6d1	celní
otázkách	otázka	k1gFnPc6	otázka
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
podpoře	podpora	k1gFnSc6	podpora
a	a	k8xC	a
ochraně	ochrana	k1gFnSc6	ochrana
investic	investice	k1gFnPc2	investice
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
připravena	připravit	k5eAaPmNgFnS	připravit
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Bezvízový	bezvízový	k2eAgInSc1d1	bezvízový
styk	styk	k1gInSc1	styk
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Izraelem	Izrael	k1gInSc7	Izrael
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
turistiku	turistika	k1gFnSc4	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
není	být	k5eNaImIp3nS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
poskytování	poskytování	k1gNnSc6	poskytování
bezplatné	bezplatný	k2eAgFnSc2d1	bezplatná
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obrana	obrana	k1gFnSc1	obrana
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Izraelské	izraelský	k2eAgFnPc1d1	izraelská
obranné	obranný	k2eAgFnPc1d1	obranná
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
letectva	letectvo	k1gNnSc2	letectvo
a	a	k8xC	a
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
během	během	k7c2	během
Války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
transformací	transformace	k1gFnSc7	transformace
Hagany	Hagana	k1gFnSc2	Hagana
a	a	k8xC	a
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
polovojenských	polovojenský	k2eAgFnPc2d1	polovojenská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
existence	existence	k1gFnSc1	existence
předcházela	předcházet	k5eAaImAgFnS	předcházet
založení	založení	k1gNnSc4	založení
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
při	pře	k1gFnSc4	pře
svých	svůj	k3xOyFgFnPc2	svůj
operací	operace	k1gFnPc2	operace
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
informací	informace	k1gFnPc2	informace
vojenské	vojenský	k2eAgFnSc2d1	vojenská
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
Aman	Amana	k1gFnPc2	Amana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
zpravodajskou	zpravodajský	k2eAgFnSc7d1	zpravodajská
službou	služba	k1gFnSc7	služba
Mosad	Mosad	k1gInSc1	Mosad
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
službou	služba	k1gFnSc7	služba
Šin	Šin	k1gFnSc2	Šin
Bet	Bet	k1gFnSc2	Bet
<g/>
.	.	kIx.	.
</s>
<s>
Izraelské	izraelský	k2eAgFnPc1d1	izraelská
obranné	obranný	k2eAgFnPc1d1	obranná
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
již	již	k6eAd1	již
mnohokrát	mnohokrát	k6eAd1	mnohokrát
účastnily	účastnit	k5eAaImAgFnP	účastnit
ozbrojených	ozbrojený	k2eAgMnPc2d1	ozbrojený
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
činí	činit	k5eAaImIp3nS	činit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejzkušenějších	zkušený	k2eAgFnPc2d3	nejzkušenější
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
připravených	připravený	k2eAgFnPc2d1	připravená
armád	armáda	k1gFnPc2	armáda
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
Izraelců	Izraelec	k1gMnPc2	Izraelec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
povolána	povolán	k2eAgNnPc1d1	povoláno
k	k	k7c3	k
povinné	povinný	k2eAgFnSc3d1	povinná
vojenské	vojenský	k2eAgFnSc3d1	vojenská
službě	služba	k1gFnSc3	služba
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
slouží	sloužit	k5eAaImIp3nP	sloužit
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc4	žena
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vykonat	vykonat	k5eAaPmF	vykonat
ještě	ještě	k9	ještě
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
muži	muž	k1gMnPc1	muž
prodělávají	prodělávat	k5eAaImIp3nP	prodělávat
každoroční	každoroční	k2eAgNnSc4d1	každoroční
několikatýdenní	několikatýdenní	k2eAgNnSc4d1	několikatýdenní
vojenské	vojenský	k2eAgNnSc4d1	vojenské
cvičení	cvičení	k1gNnSc4	cvičení
až	až	k9	až
do	do	k7c2	do
svých	svůj	k3xOyFgMnPc2	svůj
zhruba	zhruba	k6eAd1	zhruba
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
většiny	většina	k1gFnSc2	většina
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Osvobozeni	osvobozen	k2eAgMnPc1d1	osvobozen
od	od	k7c2	od
povinné	povinný	k2eAgFnSc2d1	povinná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
jsou	být	k5eAaImIp3nP	být
izraelští	izraelský	k2eAgMnPc1d1	izraelský
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
charedim	charedim	k1gInSc1	charedim
<g/>
,	,	kIx,	,
osvobození	osvobození	k1gNnSc1	osvobození
studentů	student	k1gMnPc2	student
ješiv	ješiv	k6eAd1	ješiv
od	od	k7c2	od
povinné	povinný	k2eAgFnSc2d1	povinná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
však	však	k9	však
naráží	narážet	k5eAaPmIp3nS	narážet
v	v	k7c4	v
izraelské	izraelský	k2eAgFnPc4d1	izraelská
společnosti	společnost	k1gFnPc4	společnost
na	na	k7c4	na
protesty	protest	k1gInPc4	protest
<g/>
.	.	kIx.	.
</s>
<s>
Alternativou	alternativa	k1gFnSc7	alternativa
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
získají	získat	k5eAaPmIp3nP	získat
osvobození	osvobození	k1gNnSc4	osvobození
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Šerut	Šerut	k1gMnSc1	Šerut
Le	Le	k1gMnSc1	Le
<g/>
'	'	kIx"	'
<g/>
umi	umi	k?	umi
neboli	neboli	k8xC	neboli
národní	národní	k2eAgFnSc1d1	národní
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
,	,	kIx,	,
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
sociálně	sociálně	k6eAd1	sociálně
prospěšných	prospěšný	k2eAgInPc6d1	prospěšný
programech	program	k1gInPc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
armáda	armáda	k1gFnSc1	armáda
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
celkem	celek	k1gInSc7	celek
621	[number]	k4	621
500	[number]	k4	500
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
176	[number]	k4	176
500	[number]	k4	500
aktivních	aktivní	k2eAgMnPc2d1	aktivní
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
445	[number]	k4	445
000	[number]	k4	000
rezervistů	rezervista	k1gMnPc2	rezervista
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
má	mít	k5eAaImIp3nS	mít
pozemní	pozemní	k2eAgFnSc1d1	pozemní
armáda	armáda	k1gFnSc1	armáda
133	[number]	k4	133
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
9500	[number]	k4	9500
a	a	k8xC	a
letectvo	letectvo	k1gNnSc1	letectvo
34	[number]	k4	34
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Armáda	armáda	k1gFnSc1	armáda
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
vyspělými	vyspělý	k2eAgInPc7d1	vyspělý
zbraňovými	zbraňový	k2eAgInPc7d1	zbraňový
systémy	systém	k1gInPc7	systém
buď	buď	k8xC	buď
navrženými	navržený	k2eAgInPc7d1	navržený
a	a	k8xC	a
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
některými	některý	k3yIgInPc7	některý
zahraničními	zahraniční	k2eAgInPc7d1	zahraniční
produkty	produkt	k1gInPc7	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
dodavatelem	dodavatel	k1gMnSc7	dodavatel
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k6eAd1	zejména
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
letech	let	k1gInPc6	let
2008	[number]	k4	2008
<g/>
−	−	k?	−
<g/>
2017	[number]	k4	2017
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
Izraeli	Izrael	k1gInSc6	Izrael
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
30	[number]	k4	30
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
systém	systém	k1gInSc1	systém
protiraketové	protiraketový	k2eAgFnSc2d1	protiraketová
obrany	obrana	k1gFnSc2	obrana
Arrow	Arrow	k1gFnSc2	Arrow
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
rovněž	rovněž	k9	rovněž
od	od	k7c2	od
jomkipurské	jomkipurský	k2eAgFnSc2d1	jomkipurská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
vlastní	vlastní	k2eAgFnSc4d1	vlastní
síť	síť	k1gFnSc4	síť
špionážních	špionážní	k2eAgInPc2d1	špionážní
satelitů	satelit	k1gInPc2	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
programu	program	k1gInSc2	program
špionážních	špionážní	k2eAgInPc2d1	špionážní
satelitů	satelit	k1gInPc2	satelit
Ofek	Ofeka	k1gFnPc2	Ofeka
zařadil	zařadit	k5eAaPmAgInS	zařadit
Izrael	Izrael	k1gInSc1	Izrael
mezi	mezi	k7c4	mezi
sedm	sedm	k4xCc4	sedm
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
satelitní	satelitní	k2eAgFnPc1d1	satelitní
kapacity	kapacita	k1gFnPc1	kapacita
a	a	k8xC	a
technologie	technologie	k1gFnPc1	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
také	také	k9	také
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
bitevní	bitevní	k2eAgInSc4d1	bitevní
tank	tank	k1gInSc4	tank
Merkava	Merkava	k1gFnSc1	Merkava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
prodán	prodat	k5eAaPmNgInS	prodat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
Izrael	Izrael	k1gInSc1	Izrael
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
obranu	obrana	k1gFnSc4	obrana
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
činilo	činit	k5eAaImAgNnS	činit
24	[number]	k4	24
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
činily	činit	k5eAaImAgInP	činit
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
5,69	[number]	k4	5,69
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
<g/>
Izrael	Izrael	k1gMnSc1	Izrael
není	být	k5eNaImIp3nS	být
signatářem	signatář	k1gMnSc7	signatář
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
nešíření	nešíření	k1gNnSc6	nešíření
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastní	vlastní	k2eAgFnPc4d1	vlastní
jaderné	jaderný	k2eAgFnPc4d1	jaderná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
oficiálně	oficiálně	k6eAd1	oficiálně
nepotvrdil	potvrdit	k5eNaPmAgMnS	potvrdit
ani	ani	k8xC	ani
nevyvrátil	vyvrátit	k5eNaPmAgMnS	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
údajně	údajně	k6eAd1	údajně
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
republikou	republika	k1gFnSc7	republika
provedl	provést	k5eAaPmAgInS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
jaderný	jaderný	k2eAgInSc4d1	jaderný
test	test	k1gInSc4	test
na	na	k7c6	na
námořní	námořní	k2eAgFnSc6d1	námořní
plošině	plošina	k1gFnSc6	plošina
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
množství	množství	k1gNnSc2	množství
izraelských	izraelský	k2eAgFnPc2d1	izraelská
jaderných	jaderný	k2eAgFnPc2d1	jaderná
hlavic	hlavice	k1gFnPc2	hlavice
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
osmdesáti	osmdesát	k4xCc7	osmdesát
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
sty	sto	k4xCgNnPc7	sto
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
odhady	odhad	k1gInPc1	odhad
však	však	k9	však
mluví	mluvit	k5eAaImIp3nP	mluvit
až	až	k9	až
o	o	k7c6	o
čtyřech	čtyři	k4xCgNnPc6	čtyři
stech	sto	k4xCgNnPc6	sto
hlavicích	hlavice	k1gFnPc6	hlavice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
400	[number]	k4	400
hlavicích	hlavice	k1gFnPc6	hlavice
hovoří	hovořit	k5eAaImIp3nS	hovořit
i	i	k9	i
World	World	k1gInSc1	World
Tribune	tribun	k1gMnSc5	tribun
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
na	na	k7c4	na
zdroje	zdroj	k1gInPc4	zdroj
z	z	k7c2	z
letectva	letectvo	k1gNnSc2	letectvo
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvíce	hodně	k6eAd3	hodně
ekonomicky	ekonomicky	k6eAd1	ekonomicky
a	a	k8xC	a
průmyslově	průmyslově	k6eAd1	průmyslově
rozvinutým	rozvinutý	k2eAgFnPc3d1	rozvinutá
zemím	zem	k1gFnPc3	zem
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
měl	mít	k5eAaImAgInS	mít
49	[number]	k4	49
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
260,9	[number]	k4	260,9
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
a	a	k8xC	a
37	[number]	k4	37
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
33	[number]	k4	33
878	[number]	k4	878
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
indexu	index	k1gInSc6	index
snadnosti	snadnost	k1gFnSc2	snadnost
podnikání	podnikání	k1gNnSc2	podnikání
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
35	[number]	k4	35
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgMnS	být
přizván	přizvat	k5eAaPmNgMnS	přizvat
do	do	k7c2	do
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
demokratické	demokratický	k2eAgInPc1d1	demokratický
principy	princip	k1gInPc1	princip
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
ekonomiku	ekonomika	k1gFnSc4	ekonomika
svobodného	svobodný	k2eAgInSc2d1	svobodný
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
omezené	omezený	k2eAgInPc4d1	omezený
přírodní	přírodní	k2eAgInPc4d1	přírodní
zdroje	zdroj	k1gInPc4	zdroj
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Izrael	Izrael	k1gInSc1	Izrael
v	v	k7c6	v
uplynulých	uplynulý	k2eAgNnPc6d1	uplynulé
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
soběstačným	soběstačný	k2eAgInSc7d1	soběstačný
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
intenzivnímu	intenzivní	k2eAgInSc3d1	intenzivní
rozvoji	rozvoj	k1gInSc3	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
produkce	produkce	k1gFnSc2	produkce
potravin	potravina	k1gFnPc2	potravina
<g/>
;	;	kIx,	;
z	z	k7c2	z
klíčových	klíčový	k2eAgFnPc2d1	klíčová
potravinářských	potravinářský	k2eAgFnPc2d1	potravinářská
surovin	surovina	k1gFnPc2	surovina
musí	muset	k5eAaImIp3nS	muset
dovážet	dovážet	k5eAaImF	dovážet
pouze	pouze	k6eAd1	pouze
obilí	obilí	k1gNnSc1	obilí
a	a	k8xC	a
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
rovněž	rovněž	k9	rovněž
výrazně	výrazně	k6eAd1	výrazně
investuje	investovat	k5eAaBmIp3nS	investovat
do	do	k7c2	do
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
;	;	kIx,	;
tyto	tento	k3xDgFnPc4	tento
investice	investice	k1gFnPc4	investice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
činily	činit	k5eAaImAgFnP	činit
4,34	[number]	k4	4,34
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
průměr	průměr	k1gInSc1	průměr
zemí	zem	k1gFnPc2	zem
OECD	OECD	kA	OECD
činil	činit	k5eAaImAgInS	činit
2,38	[number]	k4	2,38
%	%	kIx~	%
HDP	HDP	kA	HDP
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
1,55	[number]	k4	1,55
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nosné	nosný	k2eAgFnPc4d1	nosná
obory	obora	k1gFnPc4	obora
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
informatika	informatika	k1gFnSc1	informatika
a	a	k8xC	a
komunikační	komunikační	k2eAgFnSc1d1	komunikační
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
medicínské	medicínský	k2eAgInPc4d1	medicínský
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
biotechnologie	biotechnologie	k1gFnPc4	biotechnologie
a	a	k8xC	a
nanotechnologie	nanotechnologie	k1gFnPc4	nanotechnologie
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
celkem	celkem	k6eAd1	celkem
6,6	[number]	k4	6,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
má	mít	k5eAaImIp3nS	mít
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
negativní	negativní	k2eAgFnSc4d1	negativní
obchodní	obchodní	k2eAgFnSc4d1	obchodní
bilanci	bilance	k1gFnSc4	bilance
(	(	kIx(	(
<g/>
objem	objem	k1gInSc1	objem
importu	import	k1gInSc2	import
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
exportu	export	k1gInSc2	export
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
činil	činit	k5eAaImAgInS	činit
objem	objem	k1gInSc1	objem
importu	import	k1gInSc2	import
73,1	[number]	k4	73,1
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
oproti	oproti	k7c3	oproti
63,1	[number]	k4	63,1
miliardám	miliarda	k4xCgFnPc3	miliarda
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
Dováží	dovážet	k5eAaImIp3nS	dovážet
se	se	k3xPyFc4	se
především	především	k9	především
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
fosilní	fosilní	k2eAgNnPc4d1	fosilní
paliva	palivo	k1gNnPc4	palivo
<g/>
,	,	kIx,	,
suroviny	surovina	k1gFnPc4	surovina
a	a	k8xC	a
vojenské	vojenský	k2eAgNnSc4d1	vojenské
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
Izrael	Izrael	k1gMnSc1	Izrael
importuje	importovat	k5eAaBmIp3nS	importovat
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
a	a	k8xC	a
zejména	zejména	k9	zejména
ropu	ropa	k1gFnSc4	ropa
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
na	na	k7c4	na
zemědělská	zemědělský	k2eAgNnPc4d1	zemědělské
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
,	,	kIx,	,
paliva	palivo	k1gNnPc4	palivo
a	a	k8xC	a
farmaceutika	farmaceutikum	k1gNnPc4	farmaceutikum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
exportní	exportní	k2eAgFnPc4d1	exportní
komodity	komodita	k1gFnPc4	komodita
patří	patřit	k5eAaImIp3nP	patřit
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
farmaceutika	farmaceutikum	k1gNnPc1	farmaceutikum
<g/>
,	,	kIx,	,
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnPc1d1	vojenská
technologie	technologie	k1gFnPc1	technologie
a	a	k8xC	a
diamanty	diamant	k1gInPc1	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgMnPc4d1	hlavní
obchodní	obchodní	k2eAgMnPc4d1	obchodní
partnery	partner	k1gMnPc4	partner
Izraele	Izrael	k1gInPc1	Izrael
patří	patřit	k5eAaImIp3nP	patřit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
státy	stát	k1gInPc1	stát
Beneluxu	Benelux	k1gInSc2	Benelux
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
spolupráce	spolupráce	k1gFnSc1	spolupráce
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Českem	Česko	k1gNnSc7	Česko
má	mít	k5eAaImIp3nS	mít
trvale	trvale	k6eAd1	trvale
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
tendenci	tendence	k1gFnSc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
obchodu	obchod	k1gInSc2	obchod
činil	činit	k5eAaImAgInS	činit
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
objem	objem	k1gInSc4	objem
importu	import	k1gInSc2	import
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
dohromady	dohromady	k6eAd1	dohromady
630,8	[number]	k4	630,8
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
oproti	oproti	k7c3	oproti
220,5	[number]	k4	220,5
milionům	milion	k4xCgInPc3	milion
dolarů	dolar	k1gInPc2	dolar
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
se	se	k3xPyFc4	se
dováží	dovázat	k5eAaPmIp3nP	dovázat
zejména	zejména	k9	zejména
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
též	též	k9	též
farmaceutika	farmaceutikum	k1gNnPc1	farmaceutikum
<g/>
,	,	kIx,	,
měřící	měřící	k2eAgInPc1d1	měřící
přístroje	přístroj	k1gInPc1	přístroj
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Dováží	dovázat	k5eAaPmIp3nP	dovázat
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
léčiva	léčivo	k1gNnPc1	léčivo
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
optické	optický	k2eAgFnPc1d1	optická
a	a	k8xC	a
lékařské	lékařský	k2eAgInPc1d1	lékařský
přístroje	přístroj	k1gInPc1	přístroj
<g/>
,	,	kIx,	,
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
působení	působení	k1gNnSc1	působení
izraelských	izraelský	k2eAgFnPc2d1	izraelská
společností	společnost	k1gFnPc2	společnost
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
<g/>
;	;	kIx,	;
za	za	k7c7	za
největší	veliký	k2eAgFnSc7d3	veliký
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
investicí	investice	k1gFnSc7	investice
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
výši	výše	k1gFnSc6	výše
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
stojí	stát	k5eAaImIp3nS	stát
izraelská	izraelský	k2eAgFnSc1d1	izraelská
firma	firma	k1gFnSc1	firma
Teva	Teva	k1gMnSc1	Teva
Pharmaceutical	Pharmaceutical	k1gMnSc1	Pharmaceutical
Industries	Industries	k1gMnSc1	Industries
<g/>
,	,	kIx,	,
vyrábějící	vyrábějící	k2eAgFnPc4d1	vyrábějící
generická	generický	k2eAgNnPc4d1	generické
léčiva	léčivo	k1gNnPc4	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
koupě	koupě	k1gFnSc1	koupě
české	český	k2eAgFnSc2d1	Česká
firmy	firma	k1gFnSc2	firma
NESS	NESS	kA	NESS
Logos	logos	k1gInSc1	logos
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
podnikající	podnikající	k2eAgFnSc6d1	podnikající
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
Ness	Ness	k1gInSc4	Ness
Technologies	Technologiesa	k1gFnPc2	Technologiesa
za	za	k7c4	za
68	[number]	k4	68
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc4d1	nový
izraelský	izraelský	k2eAgInSc4d1	izraelský
šekel	šekel	k1gInSc4	šekel
(	(	kIx(	(
<g/>
NIS	Nisa	k1gFnPc2	Nisa
<g/>
,	,	kIx,	,
šekel	šekel	k1gInSc1	šekel
chadaš	chadaš	k1gInSc1	chadaš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
Palestinské	palestinský	k2eAgFnSc2d1	palestinská
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nahradil	nahradit	k5eAaPmAgInS	nahradit
izraelský	izraelský	k2eAgInSc1d1	izraelský
šekel	šekel	k1gInSc1	šekel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
nahradil	nahradit	k5eAaPmAgInS	nahradit
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
liru	lira	k1gFnSc4	lira
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
síle	síla	k1gFnSc3	síla
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
17	[number]	k4	17
měn	měna	k1gFnPc2	měna
obchodovaných	obchodovaný	k2eAgFnPc2d1	obchodovaná
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Izraelský	izraelský	k2eAgInSc1d1	izraelský
průmysl	průmysl	k1gInSc1	průmysl
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
16	[number]	k4	16
%	%	kIx~	%
pracujících	pracující	k1gMnPc2	pracující
a	a	k8xC	a
na	na	k7c4	na
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
a	a	k8xC	a
nejrozvinutější	rozvinutý	k2eAgNnSc1d3	nejrozvinutější
odvětví	odvětví	k1gNnSc1	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
patří	patřit	k5eAaImIp3nS	patřit
průmysl	průmysl	k1gInSc4	průmysl
letecký	letecký	k2eAgInSc4d1	letecký
<g/>
,	,	kIx,	,
komunikační	komunikační	k2eAgInSc4d1	komunikační
<g/>
,	,	kIx,	,
softwarový	softwarový	k2eAgInSc4d1	softwarový
<g/>
,	,	kIx,	,
elektronický	elektronický	k2eAgInSc4d1	elektronický
<g/>
,	,	kIx,	,
zbrojní	zbrojní	k2eAgInSc4d1	zbrojní
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
tabákový	tabákový	k2eAgInSc4d1	tabákový
a	a	k8xC	a
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zpracování	zpracování	k1gNnSc1	zpracování
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
,	,	kIx,	,
papírenský	papírenský	k2eAgInSc1d1	papírenský
a	a	k8xC	a
polygrafický	polygrafický	k2eAgInSc1d1	polygrafický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
rafinace	rafinace	k1gFnSc1	rafinace
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
hutnictví	hutnictví	k1gNnSc2	hutnictví
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
oborů	obor	k1gInPc2	obor
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
o	o	k7c4	o
5	[number]	k4	5
%	%	kIx~	%
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Izrael	Izrael	k1gInSc1	Izrael
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
světové	světový	k2eAgFnSc3d1	světová
špičce	špička	k1gFnSc3	špička
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
ochrany	ochrana	k1gFnSc2	ochrana
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
geotermální	geotermální	k2eAgFnSc2d1	geotermální
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
úspěchy	úspěch	k1gInPc1	úspěch
ve	v	k7c6	v
špičkových	špičkový	k2eAgFnPc6d1	špičková
technologiích	technologie	k1gFnPc6	technologie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
softwaru	software	k1gInSc2	software
<g/>
,	,	kIx,	,
komunikací	komunikace	k1gFnPc2	komunikace
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
jsou	být	k5eAaImIp3nP	být
srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
se	s	k7c7	s
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gFnSc2	Vallea
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgFnSc1	svůj
první	první	k4xOgFnSc1	první
zámořská	zámořský	k2eAgFnSc1d1	zámořská
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
a	a	k8xC	a
vývojová	vývojový	k2eAgFnSc1d1	vývojová
centra	centrum	k1gNnSc2	centrum
vybudovaly	vybudovat	k5eAaPmAgFnP	vybudovat
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
společnosti	společnost	k1gFnSc2	společnost
Intel	Intel	kA	Intel
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
Cisco	Cisco	k6eAd1	Cisco
Systems	Systems	k1gInSc1	Systems
a	a	k8xC	a
Motorola	Motorola	kA	Motorola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hi-tech	hio	k1gNnPc6	hi-to
(	(	kIx(	(
<g/>
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
,	,	kIx,	,
avionika	avionika	k1gFnSc1	avionika
<g/>
,	,	kIx,	,
farmacie	farmacie	k1gFnSc1	farmacie
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
až	až	k6eAd1	až
ke	k	k7c3	k
20	[number]	k4	20
<g/>
%	%	kIx~	%
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nerostné	nerostný	k2eAgNnSc1d1	nerostné
bohatství	bohatství	k1gNnSc1	bohatství
Izraele	Izrael	k1gInSc2	Izrael
je	být	k5eAaImIp3nS	být
omezené	omezený	k2eAgNnSc1d1	omezené
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
draselná	draselný	k2eAgFnSc1d1	draselná
<g/>
,	,	kIx,	,
hořečnatá	hořečnatý	k2eAgFnSc1d1	hořečnatá
a	a	k8xC	a
bromová	bromový	k2eAgFnSc1d1	Bromová
sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
z	z	k7c2	z
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fosfáty	fosfát	k1gInPc1	fosfát
a	a	k8xC	a
stavební	stavební	k2eAgFnPc1d1	stavební
suroviny	surovina	k1gFnPc1	surovina
(	(	kIx(	(
<g/>
sádra	sádra	k1gFnSc1	sádra
a	a	k8xC	a
stavební	stavební	k2eAgInSc1d1	stavební
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
měděná	měděný	k2eAgFnSc1d1	měděná
a	a	k8xC	a
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
také	také	k9	také
geologický	geologický	k2eAgInSc1d1	geologický
průzkum	průzkum	k1gInSc1	průzkum
ložisek	ložisko	k1gNnPc2	ložisko
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
ve	v	k7c6	v
středomořském	středomořský	k2eAgInSc6d1	středomořský
šelfu	šelf	k1gInSc6	šelf
poblíž	poblíž	k7c2	poblíž
Aškelonu	Aškelon	k1gInSc2	Aškelon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
má	mít	k5eAaImIp3nS	mít
vysoce	vysoce	k6eAd1	vysoce
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
1,6	[number]	k4	1,6
%	%	kIx~	%
pracujících	pracující	k1gMnPc2	pracující
a	a	k8xC	a
na	na	k7c4	na
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
2,4	[number]	k4	2,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
<g/>
%	%	kIx~	%
růstu	růst	k1gInSc2	růst
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
finančně	finančně	k6eAd1	finančně
nákladným	nákladný	k2eAgNnSc7d1	nákladné
zúrodňováním	zúrodňování	k1gNnSc7	zúrodňování
pouště	poušť	k1gFnSc2	poušť
−	−	k?	−
zavodňováním	zavodňování	k1gNnSc7	zavodňování
<g/>
,	,	kIx,	,
odsolováním	odsolování	k1gNnSc7	odsolování
a	a	k8xC	a
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
druhovou	druhový	k2eAgFnSc7d1	druhová
skladbou	skladba	k1gFnSc7	skladba
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
zabírá	zabírat	k5eAaImIp3nS	zabírat
15,45	[number]	k4	15,45
%	%	kIx~	%
<g/>
,	,	kIx,	,
pastviny	pastvina	k1gFnPc1	pastvina
7	[number]	k4	7
%	%	kIx~	%
a	a	k8xC	a
lesy	les	k1gInPc1	les
5,3	[number]	k4	5,3
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
54	[number]	k4	54
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
výměry	výměra	k1gFnSc2	výměra
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
je	být	k5eAaImIp3nS	být
zavlažováno	zavlažovat	k5eAaImNgNnS	zavlažovat
a	a	k8xC	a
zemědělci	zemědělec	k1gMnPc1	zemědělec
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
podílí	podílet	k5eAaImIp3nS	podílet
zhruba	zhruba	k6eAd1	zhruba
60	[number]	k4	60
%	%	kIx~	%
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
izraelské	izraelský	k2eAgFnSc3d1	izraelská
spotřebě	spotřeba	k1gFnSc3	spotřeba
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
patří	patřit	k5eAaImIp3nP	patřit
kibucům	kibuc	k1gInPc3	kibuc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
osady	osada	k1gFnPc4	osada
pracující	pracující	k2eAgFnPc4d1	pracující
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kolektivního	kolektivní	k2eAgNnSc2d1	kolektivní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
výrobních	výrobní	k2eAgInPc2d1	výrobní
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
půdy	půda	k1gFnSc2	půda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
typem	typ	k1gInSc7	typ
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
osad	osada	k1gFnPc2	osada
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
jsou	být	k5eAaImIp3nP	být
mošavy	mošava	k1gFnPc1	mošava
<g/>
,	,	kIx,	,
družstva	družstvo	k1gNnPc1	družstvo
samostatných	samostatný	k2eAgMnPc2d1	samostatný
sedláků	sedlák	k1gMnPc2	sedlák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
pouze	pouze	k6eAd1	pouze
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
prodeje	prodej	k1gInSc2	prodej
úrody	úroda	k1gFnSc2	úroda
nebo	nebo	k8xC	nebo
nákupu	nákup	k1gInSc3	nákup
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
původně	původně	k6eAd1	původně
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
podniků	podnik	k1gInPc2	podnik
také	také	k9	také
zřídilo	zřídit	k5eAaPmAgNnS	zřídit
přidruženou	přidružený	k2eAgFnSc4d1	přidružená
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
pěstované	pěstovaný	k2eAgFnPc4d1	pěstovaná
plodiny	plodina	k1gFnPc4	plodina
patří	patřit	k5eAaImIp3nS	patřit
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
citrusové	citrusový	k2eAgInPc1d1	citrusový
plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
a	a	k8xC	a
rajčata	rajče	k1gNnPc4	rajče
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
také	také	k9	také
chov	chov	k1gInSc4	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
velbloudů	velbloud	k1gMnPc2	velbloud
<g/>
,	,	kIx,	,
koz	koza	k1gFnPc2	koza
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Rybolov	rybolov	k1gInSc1	rybolov
se	se	k3xPyFc4	se
provozuje	provozovat	k5eAaImIp3nS	provozovat
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc1	chov
teplomilných	teplomilný	k2eAgFnPc2d1	teplomilná
ryb	ryba	k1gFnPc2	ryba
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
chovu	chov	k1gInSc3	chov
používá	používat	k5eAaImIp3nS	používat
poloslaná	poloslaný	k2eAgFnSc1d1	poloslaná
voda	voda	k1gFnSc1	voda
čerpaná	čerpaný	k2eAgFnSc1d1	čerpaná
z	z	k7c2	z
termálních	termální	k2eAgInPc2d1	termální
vrtů	vrt	k1gInPc2	vrt
<g/>
.	.	kIx.	.
<g/>
Značným	značný	k2eAgInSc7d1	značný
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
větším	veliký	k2eAgInSc7d2	veliký
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
Galilejské	galilejský	k2eAgNnSc1d1	Galilejské
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
Jordán	Jordán	k1gInSc1	Jordán
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zavlažování	zavlažování	k1gNnSc4	zavlažování
technických	technický	k2eAgFnPc2d1	technická
plodin	plodina	k1gFnPc2	plodina
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
recyklovaná	recyklovaný	k2eAgFnSc1d1	recyklovaná
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
550	[number]	k4	550
miliónů	milión	k4xCgInPc2	milión
m3	m3	k4	m3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
i	i	k9	i
pomocí	pomocí	k7c2	pomocí
odsolování	odsolování	k1gNnSc2	odsolování
(	(	kIx(	(
<g/>
400	[number]	k4	400
miliónů	milión	k4xCgInPc2	milión
m3	m3	k4	m3
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nepříznivým	příznivý	k2eNgFnPc3d1	nepříznivá
přírodním	přírodní	k2eAgFnPc3d1	přírodní
podmínkám	podmínka	k1gFnPc3	podmínka
se	se	k3xPyFc4	se
Izrael	Izrael	k1gInSc1	Izrael
snaží	snažit	k5eAaImIp3nS	snažit
využívat	využívat	k5eAaImF	využívat
moderní	moderní	k2eAgFnSc1d1	moderní
technologie	technologie	k1gFnSc1	technologie
a	a	k8xC	a
inovace	inovace	k1gFnSc1	inovace
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
biotechnologie	biotechnologie	k1gFnSc2	biotechnologie
<g/>
,	,	kIx,	,
tkáňové	tkáňový	k2eAgFnSc2d1	tkáňová
kultury	kultura	k1gFnSc2	kultura
či	či	k8xC	či
kapková	kapkový	k2eAgFnSc1d1	kapková
závlaha	závlaha	k1gFnSc1	závlaha
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
základního	základní	k2eAgInSc2d1	základní
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
vlastníkem	vlastník	k1gMnSc7	vlastník
půdy	půda	k1gFnSc2	půda
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
93	[number]	k4	93
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
prodej	prodej	k1gInSc1	prodej
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
stát	stát	k5eAaImF	stát
půdu	půda	k1gFnSc4	půda
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
49	[number]	k4	49
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
Dalším	další	k2eAgNnSc7d1	další
odvětvím	odvětví	k1gNnSc7	odvětví
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
náboženské	náboženský	k2eAgNnSc4d1	náboženské
poutnictví	poutnictví	k1gNnSc4	poutnictví
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
turisty	turist	k1gMnPc4	turist
svým	svůj	k3xOyFgNnSc7	svůj
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
plážemi	pláž	k1gFnPc7	pláž
<g/>
,	,	kIx,	,
archeologickými	archeologický	k2eAgFnPc7d1	archeologická
a	a	k8xC	a
historickými	historický	k2eAgFnPc7d1	historická
pamětihodnostmi	pamětihodnost	k1gFnPc7	pamětihodnost
i	i	k8xC	i
jedinečnou	jedinečný	k2eAgFnSc7d1	jedinečná
geografií	geografie	k1gFnSc7	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
měly	mít	k5eAaImAgFnP	mít
značný	značný	k2eAgInSc4d1	značný
dopad	dopad	k1gInSc4	dopad
bezpečnostní	bezpečnostní	k2eAgInPc4d1	bezpečnostní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
již	již	k6eAd1	již
situace	situace	k1gFnSc1	situace
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
normálu	normál	k1gInSc3	normál
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
turisty	turist	k1gMnPc4	turist
nejčastěji	často	k6eAd3	často
navštěvovaná	navštěvovaný	k2eAgNnPc1d1	navštěvované
místa	místo	k1gNnPc1	místo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
moderního	moderní	k2eAgInSc2d1	moderní
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Nejposvátnější	posvátný	k2eAgNnSc1d3	nejposvátnější
místo	místo	k1gNnSc1	místo
judaismu	judaismus	k1gInSc2	judaismus
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stál	stát	k5eAaImAgInS	stát
Jeruzalémský	jeruzalémský	k2eAgInSc1d1	jeruzalémský
chrám	chrám	k1gInSc1	chrám
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jeho	jeho	k3xOp3gInSc1	jeho
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
tzv.	tzv.	kA	tzv.
Zeď	zeď	k1gFnSc1	zeď
nářků	nářek	k1gInPc2	nářek
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
učil	učít	k5eAaPmAgMnS	učít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ukřižován	ukřižovat	k5eAaPmNgMnS	ukřižovat
a	a	k8xC	a
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
nejsvětější	nejsvětější	k2eAgNnSc1d1	nejsvětější
místo	místo	k1gNnSc1	místo
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Skalní	skalní	k2eAgInSc1d1	skalní
dóm	dóm	k1gInSc1	dóm
a	a	k8xC	a
mešita	mešita	k1gFnSc1	mešita
al-Aksá	al-Aksat	k5eAaImIp3nS	al-Aksat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Safed	Safed	k1gMnSc1	Safed
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
svatých	svatý	k2eAgNnPc2d1	svaté
měst	město	k1gNnPc2	město
judaismu	judaismus	k1gInSc2	judaismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
významné	významný	k2eAgNnSc1d1	významné
centrum	centrum	k1gNnSc1	centrum
studia	studio	k1gNnSc2	studio
kabaly	kabala	k1gFnSc2	kabala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Masada	Masada	k1gFnSc1	Masada
<g/>
,	,	kIx,	,
během	během	k7c2	během
první	první	k4xOgFnSc2	první
židovské	židovský	k2eAgFnSc2d1	židovská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Masada	Masada	k1gFnSc1	Masada
stala	stát	k5eAaPmAgFnS	stát
posledním	poslední	k2eAgNnSc7d1	poslední
místem	místo	k1gNnSc7	místo
odporu	odpor	k1gInSc2	odpor
židovských	židovský	k2eAgMnPc2d1	židovský
povstalců	povstalec	k1gMnPc2	povstalec
proti	proti	k7c3	proti
Římanům	Říman	k1gMnPc3	Říman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hebron	Hebron	k1gInSc4	Hebron
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
nejsvětější	nejsvětější	k2eAgNnSc1d1	nejsvětější
město	město	k1gNnSc1	město
judaismu	judaismus	k1gInSc2	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Tóry	tóra	k1gFnSc2	tóra
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
patriarchů	patriarcha	k1gMnPc2	patriarcha
a	a	k8xC	a
pramatek	pramatka	k1gFnPc2	pramatka
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
David	David	k1gMnSc1	David
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Hebron	Hebron	k1gInSc1	Hebron
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Izraelského	izraelský	k2eAgNnSc2d1	izraelské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tel	tel	kA	tel
Aviv	Aviv	k1gMnSc1	Aviv
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgNnSc4d1	pobřežní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
kosmopolitní	kosmopolitní	k2eAgNnSc4d1	kosmopolitní
a	a	k8xC	a
finanční	finanční	k2eAgNnSc4d1	finanční
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Betlém	Betlém	k1gInSc1	Betlém
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
Ráchel	Ráchel	k1gFnPc2	Ráchel
<g/>
,	,	kIx,	,
rodiště	rodiště	k1gNnSc4	rodiště
krále	král	k1gMnSc2	král
Davida	David	k1gMnSc2	David
a	a	k8xC	a
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
nejníže	nízce	k6eAd3	nízce
položené	položený	k2eAgNnSc4d1	položené
odkryté	odkrytý	k2eAgNnSc4d1	odkryté
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
nejníže	nízce	k6eAd3	nízce
položené	položený	k2eAgNnSc1d1	položené
slané	slaný	k2eAgNnSc1d1	slané
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
také	také	k9	také
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejslanějších	slaný	k2eAgNnPc2d3	nejslanější
jezer	jezero	k1gNnPc2	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
školství	školství	k1gNnSc1	školství
==	==	k?	==
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
délku	délka	k1gFnSc4	délka
vzdělání	vzdělání	k1gNnSc2	vzdělání
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
se	se	k3xPyFc4	se
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
s	s	k7c7	s
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
dobou	doba	k1gFnSc7	doba
vzdělání	vzdělání	k1gNnSc2	vzdělání
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
po	po	k7c6	po
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
,	,	kIx,	,
data	datum	k1gNnPc1	datum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
má	mít	k5eAaImIp3nS	mít
Izrael	Izrael	k1gInSc1	Izrael
největší	veliký	k2eAgInSc1d3	veliký
míru	míra	k1gFnSc4	míra
gramotnosti	gramotnost	k1gFnSc2	gramotnost
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
vzdělání	vzdělání	k1gNnSc4	vzdělání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
školy	škola	k1gFnPc4	škola
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
do	do	k7c2	do
pěti	pět	k4xCc2	pět
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
státní	státní	k2eAgFnSc1d1	státní
sekulární	sekulární	k2eAgFnSc1d1	sekulární
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
náboženské	náboženský	k2eAgNnSc4d1	náboženské
<g/>
,	,	kIx,	,
ultraortodoxní	ultraortodoxní	k2eAgNnSc4d1	ultraortodoxní
<g/>
,	,	kIx,	,
osadnické	osadnický	k2eAgNnSc4d1	osadnické
a	a	k8xC	a
arabské	arabský	k2eAgNnSc4d1	arabské
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
veřejné	veřejný	k2eAgFnPc1d1	veřejná
sekulární	sekulární	k2eAgFnPc1d1	sekulární
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
většina	většina	k1gFnSc1	většina
židovských	židovská	k1gFnPc2	židovská
a	a	k8xC	a
nearabských	arabský	k2eNgFnPc2d1	nearabská
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Arabové	Arab	k1gMnPc1	Arab
obvykle	obvykle	k6eAd1	obvykle
posílají	posílat	k5eAaImIp3nP	posílat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vyučujícím	vyučující	k2eAgInSc7d1	vyučující
jazykem	jazyk	k1gInSc7	jazyk
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
<g/>
Povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
3	[number]	k4	3
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Školy	škola	k1gFnPc1	škola
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
stupňů	stupeň	k1gInPc2	stupeň
−	−	k?	−
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
prvního	první	k4xOgInSc2	první
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
−	−	k?	−
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
−	−	k?	−
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
)	)	kIx)	)
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
−	−	k?	−
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
maturitní	maturitní	k2eAgFnSc7d1	maturitní
zkouškou	zkouška	k1gFnSc7	zkouška
bagrut	bagrut	k1gMnSc1	bagrut
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
předmětů	předmět	k1gInPc2	předmět
<g/>
:	:	kIx,	:
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
hebrejština	hebrejština	k1gFnSc1	hebrejština
<g/>
,	,	kIx,	,
hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
a	a	k8xC	a
světová	světový	k2eAgFnSc1d1	světová
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
dějepis	dějepis	k1gInSc1	dějepis
a	a	k8xC	a
základy	základ	k1gInPc1	základ
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
<g/>
,	,	kIx,	,
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
a	a	k8xC	a
drúzských	drúzský	k2eAgFnPc6d1	drúzský
školách	škola	k1gFnPc6	škola
jsou	být	k5eAaImIp3nP	být
zkoušky	zkouška	k1gFnPc1	zkouška
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
bible	bible	k1gFnSc2	bible
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
zkouškami	zkouška	k1gFnPc7	zkouška
z	z	k7c2	z
dědictví	dědictví	k1gNnSc2	dědictví
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
Drúzů	Drúz	k1gInPc2	Drúz
<g/>
.	.	kIx.	.
<g/>
Všech	všecek	k3xTgFnPc2	všecek
devět	devět	k4xCc1	devět
izraelských	izraelský	k2eAgFnPc2d1	izraelská
univerzit	univerzita	k1gFnPc2	univerzita
je	být	k5eAaImIp3nS	být
subvencovaných	subvencovaný	k2eAgMnPc2d1	subvencovaný
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
univerzitou	univerzita	k1gFnSc7	univerzita
je	být	k5eAaImIp3nS	být
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
domovem	domov	k1gInSc7	domov
Národní	národní	k2eAgFnSc2d1	národní
a	a	k8xC	a
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
disponující	disponující	k2eAgFnSc7d1	disponující
největší	veliký	k2eAgFnSc7d3	veliký
sbírkou	sbírka	k1gFnSc7	sbírka
knih	kniha	k1gFnPc2	kniha
s	s	k7c7	s
židovskou	židovský	k2eAgFnSc7d1	židovská
tematikou	tematika	k1gFnSc7	tematika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
70	[number]	k4	70
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Šanghajském	šanghajský	k2eAgInSc6d1	šanghajský
žebříčku	žebříček	k1gInSc6	žebříček
pěti	pět	k4xCc3	pět
set	set	k1gInSc4	set
nejprestižnějších	prestižní	k2eAgFnPc2d3	nejprestižnější
univerzit	univerzita	k1gFnPc2	univerzita
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
izraelské	izraelský	k2eAgFnPc4d1	izraelská
univerzity	univerzita	k1gFnPc4	univerzita
patří	patřit	k5eAaImIp3nS	patřit
Technion	Technion	k1gInSc1	Technion
–	–	k?	–
Izraelský	izraelský	k2eAgInSc1d1	izraelský
technologický	technologický	k2eAgInSc1d1	technologický
institut	institut	k1gInSc1	institut
<g/>
,	,	kIx,	,
Weizmannův	Weizmannův	k2eAgInSc1d1	Weizmannův
institut	institut	k1gInSc1	institut
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
Telavivská	telavivský	k2eAgFnSc1d1	Telavivská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Bar-Ilanova	Bar-Ilanův	k2eAgFnSc1d1	Bar-Ilanova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Haifská	haifský	k2eAgFnSc1d1	Haifská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Ben	Ben	k1gInSc1	Ben
Gurionova	Gurionův	k2eAgFnSc1d1	Gurionova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Negevu	Negevo	k1gNnSc6	Negevo
<g/>
,	,	kIx,	,
Open	Open	k1gMnSc1	Open
University	universita	k1gFnSc2	universita
of	of	k?	of
Israel	Israel	k1gInSc1	Israel
a	a	k8xC	a
Arielská	Arielský	k2eAgFnSc1d1	Arielský
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
podílu	podíl	k1gInSc6	podíl
vysokoškolsky	vysokoškolsky	k6eAd1	vysokoškolsky
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
20	[number]	k4	20
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
příliv	příliv	k1gInSc1	příliv
imigrantů	imigrant	k1gMnPc2	imigrant
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
40	[number]	k4	40
%	%	kIx~	%
mělo	mít	k5eAaImAgNnS	mít
univerzitní	univerzitní	k2eAgNnSc1d1	univerzitní
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
pomohl	pomoct	k5eAaPmAgMnS	pomoct
podpořit	podpořit	k5eAaPmF	podpořit
izraelský	izraelský	k2eAgMnSc1d1	izraelský
hi-tech	hi	k1gInPc6	hi-t
sektor	sektor	k1gInSc1	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
izraelští	izraelský	k2eAgMnPc1d1	izraelský
vědci	vědec	k1gMnPc1	vědec
jsou	být	k5eAaImIp3nP	být
nositeli	nositel	k1gMnPc7	nositel
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
Izrael	Izrael	k1gInSc1	Izrael
registruje	registrovat	k5eAaBmIp3nS	registrovat
nejvíce	hodně	k6eAd3	hodně
vědeckých	vědecký	k2eAgInPc2d1	vědecký
článků	článek	k1gInPc2	článek
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
izraelským	izraelský	k2eAgMnSc7d1	izraelský
kosmonautem	kosmonaut	k1gMnSc7	kosmonaut
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
stal	stát	k5eAaPmAgMnS	stát
Ilan	Ilan	k1gMnSc1	Ilan
Ramon	Ramona	k1gFnPc2	Ramona
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
palubní	palubní	k2eAgMnSc1d1	palubní
specialista	specialista	k1gMnSc1	specialista
při	při	k7c6	při
osudovém	osudový	k2eAgInSc6d1	osudový
letu	let	k1gInSc6	let
STS-107	STS-107	k1gFnSc2	STS-107
raketoplánu	raketoplán	k1gInSc2	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
kultura	kultura	k1gFnSc1	kultura
má	mít	k5eAaImIp3nS	mít
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
tradici	tradice	k1gFnSc4	tradice
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
židovskému	židovský	k2eAgInSc3d1	židovský
národu	národ	k1gInSc3	národ
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
pojmu	pojem	k1gInSc3	pojem
vztahovali	vztahovat	k5eAaImAgMnP	vztahovat
oni	onen	k3xDgMnPc1	onen
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
počítat	počítat	k5eAaImF	počítat
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
vědců	vědec	k1gMnPc2	vědec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
von	von	k1gInSc4	von
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Born	Born	k1gMnSc1	Born
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Pauli	Paul	k1gMnSc3	Paul
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Oppenheimer	Oppenheimer	k1gMnSc1	Oppenheimer
<g/>
,	,	kIx,	,
Lev	Lev	k1gMnSc1	Lev
Landau	Landaus	k1gInSc2	Landaus
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Stern	sternum	k1gNnPc2	sternum
<g/>
,	,	kIx,	,
Gabriel	Gabriel	k1gMnSc1	Gabriel
Lippmann	Lippmann	k1gMnSc1	Lippmann
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Franck	Franck	k1gMnSc1	Franck
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Abraham	Abraham	k1gMnSc1	Abraham
Michelson	Michelson	k1gMnSc1	Michelson
<g/>
,	,	kIx,	,
Lise	Lisa	k1gFnSc6	Lisa
Meitnerová	Meitnerová	k1gFnSc1	Meitnerová
<g/>
,	,	kIx,	,
Rosalind	Rosalinda	k1gFnPc2	Rosalinda
Franklinová	Franklinový	k2eAgFnSc1d1	Franklinová
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Bethe	Bethe	k1gFnSc1	Bethe
<g/>
,	,	kIx,	,
Dennis	Dennis	k1gFnSc1	Dennis
Gabor	Gabor	k1gMnSc1	Gabor
<g/>
,	,	kIx,	,
Vitalij	Vitalij	k1gMnSc1	Vitalij
Lazarevič	Lazarevič	k1gMnSc1	Lazarevič
Ginzburg	Ginzburg	k1gMnSc1	Ginzburg
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Teller	Teller	k1gMnSc1	Teller
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
chemie	chemie	k1gFnSc2	chemie
(	(	kIx(	(
<g/>
Fritz	Fritz	k1gMnSc1	Fritz
Haber	Haber	k1gMnSc1	Haber
<g/>
,	,	kIx,	,
Henri	Henr	k1gMnPc1	Henr
Moissan	Moissan	k1gMnSc1	Moissan
<g/>
,	,	kIx,	,
Ilja	Ilja	k1gMnSc1	Ilja
Prigogine	Prigogin	k1gMnSc5	Prigogin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc1	biologie
(	(	kIx(	(
<g/>
Jonas	Jonas	k1gMnSc1	Jonas
Salk	Salk	k1gMnSc1	Salk
<g/>
,	,	kIx,	,
Rita	Rita	k1gFnSc1	Rita
Leviová-Montalciniová	Leviová-Montalciniový	k2eAgFnSc1d1	Leviová-Montalciniový
<g/>
,	,	kIx,	,
Ilja	Ilja	k1gMnSc1	Ilja
Iljič	Iljič	k1gMnSc1	Iljič
Mečnikov	Mečnikov	k1gInSc1	Mečnikov
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Ehrlich	Ehrlich	k1gMnSc1	Ehrlich
<g/>
,	,	kIx,	,
Gerty	Gert	k1gMnPc4	Gert
Coriová	Coriový	k2eAgFnSc1d1	Coriová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematiky	matematika	k1gFnSc2	matematika
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Georg	Georg	k1gMnSc1	Georg
Cantor	Cantor	k1gMnSc1	Cantor
<g/>
,	,	kIx,	,
Pál	Pál	k1gMnSc1	Pál
Erdős	Erdős	k1gInSc1	Erdős
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jacob	Jacoba	k1gFnPc2	Jacoba
Jacobi	Jacob	k1gFnSc2	Jacob
<g/>
,	,	kIx,	,
Benoit	Benoit	k1gMnSc1	Benoit
Mandelbrot	Mandelbrot	k1gMnSc1	Mandelbrot
<g/>
,	,	kIx,	,
Hermann	Hermann	k1gMnSc1	Hermann
Minkowski	Minkowsk	k1gFnSc2	Minkowsk
<g/>
,	,	kIx,	,
Grigorij	Grigorij	k1gMnSc1	Grigorij
Perelman	Perelman	k1gMnSc1	Perelman
<g/>
,	,	kIx,	,
Norbert	Norbert	k1gMnSc1	Norbert
Wiener	Wiener	k1gMnSc1	Wiener
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc1	astronomie
(	(	kIx(	(
<g/>
Carl	Carl	k1gMnSc1	Carl
Sagan	Sagan	k1gMnSc1	Sagan
<g/>
,	,	kIx,	,
Saul	Saul	k1gMnSc1	Saul
Perlmutter	Perlmutter	k1gMnSc1	Perlmutter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
informatiky	informatika	k1gFnSc2	informatika
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Marvin	Marvina	k1gFnPc2	Marvina
Minsky	minsky	k6eAd1	minsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psychologie	psychologie	k1gFnSc1	psychologie
(	(	kIx(	(
<g/>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
,	,	kIx,	,
Abraham	Abraham	k1gMnSc1	Abraham
Maslow	Maslow	k1gMnSc1	Maslow
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Adler	Adler	k1gMnSc1	Adler
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
Fromm	Fromm	k1gMnSc1	Fromm
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Frankl	Frankl	k1gMnSc1	Frankl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonomie	ekonomie	k1gFnSc2	ekonomie
(	(	kIx(	(
<g/>
Milton	Milton	k1gInSc4	Milton
Friedman	Friedman	k1gMnSc1	Friedman
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Samuelson	Samuelson	k1gMnSc1	Samuelson
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Ricardo	Ricardo	k1gNnSc1	Ricardo
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Krugman	Krugman	k1gMnSc1	Krugman
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Stiglitz	Stiglitz	k1gMnSc1	Stiglitz
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gInSc1	Ludwig
von	von	k1gInSc1	von
Mises	Mises	k1gInSc1	Mises
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc1	sociologie
(	(	kIx(	(
<g/>
Émile	Émile	k1gFnSc1	Émile
Durkheim	Durkheim	k1gMnSc1	Durkheim
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
Marcuse	Marcuse	k1gFnSc2	Marcuse
<g/>
,	,	kIx,	,
Theodor	Theodor	k1gMnSc1	Theodor
W.	W.	kA	W.
Adorno	Adorno	k1gNnSc1	Adorno
<g/>
,	,	kIx,	,
Judith	Judith	k1gInSc1	Judith
Butlerová	Butlerová	k1gFnSc1	Butlerová
<g/>
,	,	kIx,	,
Zygmunt	Zygmunt	k1gMnSc1	Zygmunt
Bauman	Bauman	k1gMnSc1	Bauman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
antropologie	antropologie	k1gFnSc1	antropologie
(	(	kIx(	(
<g/>
Claude	Claud	k1gInSc5	Claud
Lévi-Strauss	Lévi-Straussa	k1gFnPc2	Lévi-Straussa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
teologie	teologie	k1gFnSc1	teologie
(	(	kIx(	(
<g/>
Maimonides	Maimonides	k1gInSc1	Maimonides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozofie	filozofie	k1gFnSc1	filozofie
(	(	kIx(	(
<g/>
Filón	Filón	k1gInSc1	Filón
Alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
<g/>
,	,	kIx,	,
Baruch	Baruch	k1gInSc1	Baruch
Spinoza	Spinoza	k1gFnSc1	Spinoza
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gMnSc1	Ludwig
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
<g/>
,	,	kIx,	,
Henri	Henr	k1gMnPc1	Henr
Bergson	Bergson	k1gMnSc1	Bergson
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Popper	Popper	k1gMnSc1	Popper
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
Derrida	Derrida	k1gFnSc1	Derrida
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
Husserl	Husserl	k1gMnSc1	Husserl
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Kuhn	Kuhn	k1gMnSc1	Kuhn
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Buber	Buber	k1gMnSc1	Buber
<g/>
,	,	kIx,	,
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Levinas	Levinas	k1gMnSc1	Levinas
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kulturologie	kulturologie	k1gFnSc1	kulturologie
(	(	kIx(	(
<g/>
Walter	Walter	k1gMnSc1	Walter
Benjamin	Benjamin	k1gMnSc1	Benjamin
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Gombrich	Gombrich	k1gMnSc1	Gombrich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politologie	politologie	k1gFnSc1	politologie
(	(	kIx(	(
<g/>
Hannah	Hannah	k1gInSc1	Hannah
Arendtová	Arendtová	k1gFnSc1	Arendtová
<g/>
,	,	kIx,	,
Raymond	Raymond	k1gMnSc1	Raymond
Aron	Aron	k1gMnSc1	Aron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lingvistiky	lingvistika	k1gFnSc2	lingvistika
(	(	kIx(	(
<g/>
Edward	Edward	k1gMnSc1	Edward
Sapir	Sapir	k1gMnSc1	Sapir
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
Jakobson	Jakobson	k1gMnSc1	Jakobson
<g/>
,	,	kIx,	,
Noam	Noam	k1gMnSc1	Noam
Chomsky	Chomsky	k1gMnSc1	Chomsky
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Lazar	Lazar	k1gMnSc1	Lazar
Zamenhof	Zamenhof	k1gMnSc1	Zamenhof
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historiografie	historiografie	k1gFnSc1	historiografie
(	(	kIx(	(
<g/>
Flavius	Flavius	k1gMnSc1	Flavius
Iosephus	Iosephus	k1gMnSc1	Iosephus
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
Hobsbawm	Hobsbawm	k1gMnSc1	Hobsbawm
<g/>
,	,	kIx,	,
Isaiah	Isaiah	k1gMnSc1	Isaiah
Berlin	berlina	k1gFnPc2	berlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
přestože	přestože	k8xS	přestože
Židé	Žid	k1gMnPc1	Žid
tvoří	tvořit	k5eAaImIp3nP	tvořit
jen	jen	k9	jen
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvacetiprocentní	dvacetiprocentní	k2eAgInSc4d1	dvacetiprocentní
podíl	podíl	k1gInSc4	podíl
mezi	mezi	k7c7	mezi
laureáty	laureát	k1gMnPc7	laureát
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
,	,	kIx,	,
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
fyzice	fyzika	k1gFnSc3	fyzika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
tradici	tradice	k1gFnSc3	tradice
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
stát	stát	k1gInSc1	stát
Izrael	Izrael	k1gInSc1	Izrael
předurčen	předurčen	k2eAgInSc1d1	předurčen
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vsadil	vsadit	k5eAaPmAgInS	vsadit
na	na	k7c4	na
vědecký	vědecký	k2eAgInSc4d1	vědecký
a	a	k8xC	a
technologický	technologický	k2eAgInSc4d1	technologický
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
vědu	věda	k1gFnSc4	věda
zamířila	zamířit	k5eAaPmAgFnS	zamířit
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
ekonoma	ekonom	k1gMnSc2	ekonom
Daniela	Daniel	k1gMnSc2	Daniel
Kahnemana	Kahneman	k1gMnSc2	Kahneman
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
získali	získat	k5eAaPmAgMnP	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
hned	hned	k6eAd1	hned
dva	dva	k4xCgMnPc1	dva
Izraelci	Izraelec	k1gMnPc1	Izraelec
<g/>
,	,	kIx,	,
Avram	Avram	k1gInSc1	Avram
Herško	Herška	k1gFnSc5	Herška
a	a	k8xC	a
Aaron	Aaron	k1gMnSc1	Aaron
Ciechanover	Ciechanover	k1gMnSc1	Ciechanover
<g/>
,	,	kIx,	,
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
rozpadu	rozpad	k1gInSc2	rozpad
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Aumann	Aumann	k1gMnSc1	Aumann
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
konfliktu	konflikt	k1gInSc6	konflikt
a	a	k8xC	a
kooperaci	kooperace	k1gFnSc6	kooperace
v	v	k7c6	v
analýze	analýza	k1gFnSc6	analýza
teorie	teorie	k1gFnSc2	teorie
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ada	Ada	kA	Ada
Jonathová	Jonathová	k1gFnSc1	Jonathová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
získala	získat	k5eAaPmAgFnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
za	za	k7c4	za
objasnění	objasnění	k1gNnSc4	objasnění
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
funkce	funkce	k1gFnSc2	funkce
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
ženskou	ženská	k1gFnSc4	ženská
laureátkou	laureátka	k1gFnSc7	laureátka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Izraelec	Izraelec	k1gMnSc1	Izraelec
Daniel	Daniel	k1gMnSc1	Daniel
Šechtman	Šechtman	k1gMnSc1	Šechtman
získal	získat	k5eAaPmAgMnS	získat
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
za	za	k7c4	za
objev	objev	k1gInSc4	objev
kvazikrystalů	kvazikrystal	k1gMnPc2	kvazikrystal
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
vědu	věda	k1gFnSc4	věda
rok	rok	k1gInSc1	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
do	do	k7c2	do
Stockholmu	Stockholm	k1gInSc2	Stockholm
pro	pro	k7c4	pro
nejprestižnější	prestižní	k2eAgNnSc4d3	nejprestižnější
vědecké	vědecký	k2eAgNnSc4d1	vědecké
ocenění	ocenění	k1gNnSc4	ocenění
jeli	jet	k5eAaImAgMnP	jet
hned	hned	k9	hned
dva	dva	k4xCgMnPc1	dva
izraelští	izraelský	k2eAgMnPc1d1	izraelský
občané	občan	k1gMnPc1	občan
<g/>
:	:	kIx,	:
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
získal	získat	k5eAaPmAgMnS	získat
Arieh	Arieh	k1gMnSc1	Arieh
Warshel	Warshel	k1gMnSc1	Warshel
<g/>
,	,	kIx,	,
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
Michael	Michael	k1gMnSc1	Michael
Levitt	Levitt	k1gMnSc1	Levitt
<g/>
.	.	kIx.	.
</s>
<s>
Nejprestižnější	prestižní	k2eAgNnSc1d3	nejprestižnější
ocenění	ocenění	k1gNnSc1	ocenění
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
počítačových	počítačový	k2eAgFnPc2d1	počítačová
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
Turingovu	Turingův	k2eAgFnSc4d1	Turingova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
Izraelci	Izraelec	k1gMnPc1	Izraelec
Shafi	Shaf	k1gFnSc2	Shaf
Goldwasserová	Goldwasserový	k2eAgFnSc1d1	Goldwasserový
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
O.	O.	kA	O.
Rabin	Rabin	k1gMnSc1	Rabin
<g/>
,	,	kIx,	,
Amir	Amir	k1gInSc4	Amir
Pnueli	Pnuel	k1gInPc7	Pnuel
a	a	k8xC	a
Judea	Judea	k1gFnSc1	Judea
Pearl	Pearla	k1gFnPc2	Pearla
<g/>
.	.	kIx.	.
</s>
<s>
Fieldsovu	Fieldsův	k2eAgFnSc4d1	Fieldsova
medaili	medaile	k1gFnSc4	medaile
získal	získat	k5eAaPmAgInS	získat
matematik	matematik	k1gMnSc1	matematik
Elon	Elona	k1gFnPc2	Elona
Lindenstrauss	Lindenstrauss	k1gInSc1	Lindenstrauss
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
především	především	k9	především
erdogickou	erdogický	k2eAgFnSc7d1	erdogický
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
dynamickými	dynamický	k2eAgInPc7d1	dynamický
systémy	systém	k1gInPc7	systém
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
aplikacemi	aplikace	k1gFnPc7	aplikace
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgMnPc3d1	známý
izraelským	izraelský	k2eAgMnPc3d1	izraelský
vědcům	vědec	k1gMnPc3	vědec
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
informatik	informatik	k1gMnSc1	informatik
a	a	k8xC	a
spoluvynálezce	spoluvynálezce	k1gMnSc1	spoluvynálezce
šifrovacího	šifrovací	k2eAgInSc2d1	šifrovací
algoritmu	algoritmus	k1gInSc2	algoritmus
RSA	RSA	kA	RSA
Adi	Adi	k1gMnSc1	Adi
Šamir	Šamir	k1gMnSc1	Šamir
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Ja	Ja	k?	Ja
<g/>
'	'	kIx"	'
<g/>
akov	akov	k1gMnSc1	akov
Bekenstein	Bekenstein	k1gMnSc1	Bekenstein
zkoumající	zkoumající	k2eAgFnSc4d1	zkoumající
termodynamiku	termodynamika	k1gFnSc4	termodynamika
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Nathan	Nathan	k1gMnSc1	Nathan
Rosen	rosen	k2eAgMnSc1d1	rosen
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
teorií	teorie	k1gFnPc2	teorie
o	o	k7c6	o
červích	červí	k2eAgFnPc6d1	červí
dírách	díra	k1gFnPc6	díra
a	a	k8xC	a
spoluzakládal	spoluzakládat	k5eAaImAgMnS	spoluzakládat
mnoho	mnoho	k4c4	mnoho
izraelských	izraelský	k2eAgFnPc2d1	izraelská
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
průkopník	průkopník	k1gMnSc1	průkopník
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
kvantového	kvantový	k2eAgNnSc2d1	kvantové
počítání	počítání	k1gNnSc2	počítání
David	David	k1gMnSc1	David
Deutsch	Deutsch	k1gMnSc1	Deutsch
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
Amos	Amos	k1gMnSc1	Amos
Tversky	Tversko	k1gNnPc7	Tversko
<g/>
,	,	kIx,	,
lingvista	lingvista	k1gMnSc1	lingvista
Ghil	Ghil	k1gMnSc1	Ghil
<g/>
'	'	kIx"	'
<g/>
ad	ad	k7c4	ad
Zuckermann	Zuckermann	k1gNnSc4	Zuckermann
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Juval	Juval	k1gMnSc1	Juval
Noach	Noach	k1gMnSc1	Noach
Harari	Harar	k1gMnSc3	Harar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
bestsellerem	bestseller	k1gInSc7	bestseller
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
:	:	kIx,	:
Sapiens	Sapiensa	k1gFnPc2	Sapiensa
<g/>
:	:	kIx,	:
od	od	k7c2	od
zvířete	zvíře	k1gNnSc2	zvíře
k	k	k7c3	k
božskému	božský	k2eAgMnSc3d1	božský
jedinci	jedinec	k1gMnSc3	jedinec
<g/>
,	,	kIx,	,
či	či	k8xC	či
filozof	filozof	k1gMnSc1	filozof
Geršom	Geršom	k1gInSc4	Geršom
Scholem	Schol	k1gInSc7	Schol
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
proslul	proslout	k5eAaPmAgMnS	proslout
pracemi	práce	k1gFnPc7	práce
o	o	k7c6	o
židovském	židovský	k2eAgInSc6d1	židovský
mysticismu	mysticismus	k1gInSc6	mysticismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2013	[number]	k4	2013
měl	mít	k5eAaImAgInS	mít
Izrael	Izrael	k1gInSc1	Izrael
8	[number]	k4	8
002	[number]	k4	002
300	[number]	k4	300
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
pouze	pouze	k6eAd1	pouze
samotný	samotný	k2eAgInSc1d1	samotný
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
v	v	k7c6	v
hranicích	hranice	k1gFnPc6	hranice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
občany	občan	k1gMnPc4	občan
Izraele	Izrael	k1gInSc2	Izrael
v	v	k7c6	v
Judeji	Judea	k1gFnSc6	Judea
a	a	k8xC	a
Samaří	Samaří	k1gNnSc6	Samaří
<g/>
,	,	kIx,	,
Golanských	Golanský	k2eAgFnPc6d1	Golanský
výšinách	výšina	k1gFnPc6	výšina
a	a	k8xC	a
Východním	východní	k2eAgInSc6d1	východní
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
Západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
Jordánu	Jordán	k1gInSc2	Jordán
celkem	celkem	k6eAd1	celkem
341	[number]	k4	341
400	[number]	k4	400
Izraelců	Izraelec	k1gMnPc2	Izraelec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
městech	město	k1gNnPc6	město
jako	jako	k8xC	jako
Ma	Ma	k1gFnPc6	Ma
<g/>
'	'	kIx"	'
<g/>
ale	ale	k8xC	ale
Adumim	Adumim	k1gInSc1	Adumim
<g/>
,	,	kIx,	,
Modi	Modi	k1gNnSc1	Modi
<g/>
'	'	kIx"	'
<g/>
in	in	k?	in
Ilit	Ilit	k1gMnSc1	Ilit
<g/>
,	,	kIx,	,
Bejtar	Bejtar	k1gMnSc1	Bejtar
Ilit	Ilit	k1gMnSc1	Ilit
nebo	nebo	k8xC	nebo
Ariel	Ariel	k1gMnSc1	Ariel
a	a	k8xC	a
v	v	k7c6	v
osadách	osada	k1gFnPc6	osada
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
zčásti	zčásti	k6eAd1	zčásti
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
před	před	k7c7	před
založením	založení	k1gNnSc7	založení
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
po	po	k7c6	po
šestidenní	šestidenní	k2eAgFnSc6d1	šestidenní
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Hebron	Hebron	k1gMnSc1	Hebron
a	a	k8xC	a
Guš	Guš	k1gMnSc1	Guš
Ecion	Ecion	k1gMnSc1	Ecion
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Golanských	Golanský	k2eAgFnPc6d1	Golanský
výšinách	výšina	k1gFnPc6	výšina
žilo	žít	k5eAaImAgNnS	žít
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
přes	přes	k7c4	přes
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
Izraelců	Izraelec	k1gMnPc2	Izraelec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
žilo	žít	k5eAaImAgNnS	žít
198	[number]	k4	198
tisíc	tisíc	k4xCgInPc2	tisíc
Izraelců	Izraelec	k1gMnPc2	Izraelec
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
izraelských	izraelský	k2eAgMnPc2d1	izraelský
osadníků	osadník	k1gMnPc2	osadník
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
500	[number]	k4	500
tisíc	tisíc	k4xCgInPc2	tisíc
(	(	kIx(	(
<g/>
6,5	[number]	k4	6,5
%	%	kIx~	%
izraelské	izraelský	k2eAgFnSc2d1	izraelská
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
7800	[number]	k4	7800
Izraelců	Izraelec	k1gMnPc2	Izraelec
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
osadách	osada	k1gFnPc6	osada
v	v	k7c6	v
Pásmu	pásmo	k1gNnSc6	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
,	,	kIx,	,
než	než	k8xS	než
byli	být	k5eAaImAgMnP	být
evakuováni	evakuovat	k5eAaBmNgMnP	evakuovat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
na	na	k7c4	na
jednostranné	jednostranný	k2eAgNnSc4d1	jednostranné
stažení	stažení	k1gNnSc4	stažení
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
68	[number]	k4	68
%	%	kIx~	%
izraelských	izraelský	k2eAgMnPc2d1	izraelský
Židů	Žid	k1gMnPc2	Žid
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
(	(	kIx(	(
<g/>
tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
slovem	slovo	k1gNnSc7	slovo
sabra	sabro	k1gNnSc2	sabro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
22	[number]	k4	22
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Arabského	arabský	k2eAgInSc2d1	arabský
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
70	[number]	k4	70
%	%	kIx~	%
izraelské	izraelský	k2eAgFnSc2d1	izraelská
populace	populace	k1gFnSc2	populace
žije	žít	k5eAaImIp3nS	žít
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
<g/>
Úřední	úřední	k2eAgInPc1d1	úřední
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
hebrejština	hebrejština	k1gFnSc1	hebrejština
a	a	k8xC	a
arabština	arabština	k1gFnSc1	arabština
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
už	už	k6eAd1	už
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
právně	právně	k6eAd1	právně
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
arabština	arabština	k1gFnSc1	arabština
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
jen	jen	k9	jen
<g/>
"	"	kIx"	"
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
status	status	k1gInSc4	status
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
hebrejština	hebrejština	k1gFnSc1	hebrejština
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
jí	jíst	k5eAaImIp3nS	jíst
většina	většina	k1gFnSc1	většina
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Arabsky	arabsky	k6eAd1	arabsky
hovoří	hovořit	k5eAaImIp3nP	hovořit
izraelští	izraelský	k2eAgMnPc1d1	izraelský
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
imigrovali	imigrovat	k5eAaBmAgMnP	imigrovat
z	z	k7c2	z
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Izraelců	Izraelec	k1gMnPc2	Izraelec
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
i	i	k9	i
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
množství	množství	k1gNnSc3	množství
imigrantů	imigrant	k1gMnPc2	imigrant
lze	lze	k6eAd1	lze
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
slyšet	slyšet	k5eAaImF	slyšet
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
Etiopie	Etiopie	k1gFnSc2	Etiopie
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
značně	značně	k6eAd1	značně
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
ruštinu	ruština	k1gFnSc4	ruština
a	a	k8xC	a
amharštinu	amharština	k1gFnSc4	amharština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
−	−	k?	−
<g/>
1994	[number]	k4	1994
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
podíl	podíl	k1gInSc1	podíl
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
na	na	k7c4	na
12	[number]	k4	12
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
izraelské	izraelský	k2eAgFnSc2d1	izraelská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nastal	nastat	k5eAaPmAgInS	nastat
významný	významný	k2eAgInSc1d1	významný
příliv	příliv	k1gInSc1	příliv
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
z	z	k7c2	z
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
Thajska	Thajsko	k1gNnSc2	Thajsko
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
zemí	zem	k1gFnPc2	zem
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
na	na	k7c4	na
70	[number]	k4	70
tisíc	tisíc	k4xCgInPc2	tisíc
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Izrael	Izrael	k1gInSc1	Izrael
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
ekonomické	ekonomický	k2eAgMnPc4d1	ekonomický
imigranty	imigrant	k1gMnPc4	imigrant
ohrožující	ohrožující	k2eAgInSc4d1	ohrožující
židovský	židovský	k2eAgInSc4d1	židovský
charakter	charakter	k1gInSc4	charakter
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
jako	jako	k8xC	jako
domovina	domovina	k1gFnSc1	domovina
pro	pro	k7c4	pro
židovský	židovský	k2eAgInSc4d1	židovský
lid	lid	k1gInSc4	lid
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
Židovský	židovský	k2eAgInSc1d1	židovský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
všem	všecek	k3xTgMnPc3	všecek
Židům	Žid	k1gMnPc3	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
maji	maji	k6eAd1	maji
židovské	židovský	k2eAgMnPc4d1	židovský
předky	předek	k1gMnPc4	předek
<g/>
,	,	kIx,	,
i	i	k8xC	i
konvertitům	konvertita	k1gMnPc3	konvertita
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
izraelského	izraelský	k2eAgNnSc2d1	izraelské
občanství	občanství	k1gNnSc2	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
76,1	[number]	k4	76,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
40	[number]	k4	40
<g/>
−	−	k?	−
<g/>
50	[number]	k4	50
%	%	kIx~	%
Židů	Žid	k1gMnPc2	Žid
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
sekulární	sekulární	k2eAgFnSc4d1	sekulární
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ח	ח	k?	ח
<g/>
,	,	kIx,	,
chiloni	chilon	k1gMnPc1	chilon
<g/>
;	;	kIx,	;
ל	ל	k?	ל
ד	ד	k?	ד
<g/>
,	,	kIx,	,
lo	lo	k?	lo
dati	dat	k1gFnSc2	dat
−	−	k?	−
nevěřící	nevěřící	k1gMnSc1	nevěřící
či	či	k8xC	či
ח	ח	k?	ח
<g/>
,	,	kIx,	,
chofši	chofš	k1gInSc6	chofš
−	−	k?	−
volnomyšlenkáři	volnomyšlenkář	k1gMnPc1	volnomyšlenkář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
−	−	k?	−
<g/>
40	[number]	k4	40
%	%	kIx~	%
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
tradicionalisty	tradicionalista	k1gMnPc4	tradicionalista
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
hebr.	hebr.	k?	hebr.
מ	מ	k?	מ
<g/>
,	,	kIx,	,
masorti	masort	k1gMnPc1	masort
či	či	k8xC	či
ד	ד	k?	ד
<g/>
,	,	kIx,	,
dati	dati	k6eAd1	dati
−	−	k?	−
věřící	věřící	k1gFnSc1	věřící
<g/>
)	)	kIx)	)
a	a	k8xC	a
20	[number]	k4	20
%	%	kIx~	%
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
religiózní	religiózní	k2eAgFnSc4d1	religiózní
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
charedim	charedim	k1gMnSc1	charedim
a	a	k8xC	a
ortodoxní	ortodoxní	k2eAgMnPc1d1	ortodoxní
Židé	Žid	k1gMnPc1	Žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Muslimové	muslim	k1gMnPc1	muslim
představují	představovat	k5eAaImIp3nP	představovat
s	s	k7c7	s
16,2	[number]	k4	16,2
%	%	kIx~	%
izraelské	izraelský	k2eAgFnSc2d1	izraelská
populace	populace	k1gFnSc2	populace
největší	veliký	k2eAgFnSc4d3	veliký
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
menšinu	menšina	k1gFnSc4	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Izraelští	izraelský	k2eAgMnPc1d1	izraelský
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
19	[number]	k4	19
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
tomuto	tento	k3xDgNnSc3	tento
číslu	číslo	k1gNnSc3	číslo
významně	významně	k6eAd1	významně
přispívají	přispívat	k5eAaImIp3nP	přispívat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgFnPc4	čtyři
pětiny	pětina	k1gFnPc4	pětina
(	(	kIx(	(
<g/>
82,6	[number]	k4	82,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
jich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
8,8	[number]	k4	8,8
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
8,4	[number]	k4	8,4
%	%	kIx~	%
Drúzů	Drúz	k1gInPc2	Drúz
<g/>
.	.	kIx.	.
</s>
<s>
Členů	člen	k1gMnPc2	člen
jiných	jiný	k2eAgFnPc2d1	jiná
náboženských	náboženský	k2eAgFnPc2d1	náboženská
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xC	jako
buddhistů	buddhista	k1gMnPc2	buddhista
a	a	k8xC	a
hinduistů	hinduista	k1gMnPc2	hinduista
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málo	málo	k1gNnSc1	málo
<g/>
.	.	kIx.	.
<g/>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
má	mít	k5eAaImIp3nS	mít
díky	díky	k7c3	díky
Zdi	zeď	k1gFnSc3	zeď
nářků	nářek	k1gInPc2	nářek
<g/>
,	,	kIx,	,
Chrámové	chrámový	k2eAgFnSc6d1	chrámová
hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
mešitě	mešita	k1gFnSc6	mešita
Al-Aksá	Al-Aksý	k2eAgFnSc1d1	Al-Aksá
a	a	k8xC	a
bazilice	bazilika	k1gFnSc3	bazilika
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
srdcích	srdce	k1gNnPc6	srdce
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
muslimů	muslim	k1gMnPc2	muslim
a	a	k8xC	a
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
důležitá	důležitý	k2eAgNnPc1d1	důležité
náboženská	náboženský	k2eAgNnPc1d1	náboženské
místa	místo	k1gNnPc1	místo
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Judejských	Judejský	k2eAgFnPc6d1	Judejská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
místo	místo	k7c2	místo
narození	narození	k1gNnSc2	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
a	a	k8xC	a
Ráchelin	Ráchelin	k2eAgInSc1d1	Ráchelin
hrob	hrob	k1gInSc1	hrob
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
či	či	k8xC	či
Jeskyně	jeskyně	k1gFnSc2	jeskyně
patriarchů	patriarcha	k1gMnPc2	patriarcha
v	v	k7c6	v
Hebronu	Hebron	k1gInSc6	Hebron
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgNnSc7d1	ústřední
místem	místo	k1gNnSc7	místo
víry	víra	k1gFnSc2	víra
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
í	í	k0	í
je	on	k3xPp3gInPc4	on
Haifa	Haifa	k1gFnSc1	Haifa
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
zakladatel	zakladatel	k1gMnSc1	zakladatel
Bahá	Bahá	k1gFnSc1	Bahá
<g/>
'	'	kIx"	'
<g/>
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
lláh	lláh	k1gMnSc1	lláh
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c4	v
Akko	Akko	k1gNnSc4	Akko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
má	mít	k5eAaImIp3nS	mít
díky	díky	k7c3	díky
rozmanitosti	rozmanitost	k1gFnSc3	rozmanitost
své	svůj	k3xOyFgFnSc2	svůj
populace	populace	k1gFnSc2	populace
nesmírně	smírně	k6eNd1	smírně
bohatou	bohatý	k2eAgFnSc4d1	bohatá
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
příchozí	příchozí	k1gMnPc1	příchozí
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
přinesli	přinést	k5eAaPmAgMnP	přinést
své	svůj	k3xOyFgFnPc4	svůj
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc4d1	náboženská
zvyklosti	zvyklost	k1gFnPc4	zvyklost
a	a	k8xC	a
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
se	se	k3xPyFc4	se
i	i	k9	i
Izrael	Izrael	k1gInSc1	Izrael
stal	stát	k5eAaPmAgInS	stát
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
tavícím	tavící	k2eAgInSc7d1	tavící
kotlíkem	kotlík	k1gInSc7	kotlík
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
věrouk	věrouka	k1gFnPc2	věrouka
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
život	život	k1gInSc1	život
a	a	k8xC	a
události	událost	k1gFnPc1	událost
odvíjejí	odvíjet	k5eAaImIp3nP	odvíjet
podle	podle	k7c2	podle
židovského	židovský	k2eAgInSc2d1	židovský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
a	a	k8xC	a
prázdniny	prázdniny	k1gFnPc1	prázdniny
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
podle	podle	k7c2	podle
židovských	židovský	k2eAgInPc2d1	židovský
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
šabat	šabat	k1gInSc1	šabat
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
dnem	den	k1gInSc7	den
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
kulturu	kultura	k1gFnSc4	kultura
měla	mít	k5eAaImAgFnS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k8xC	i
početná	početný	k2eAgFnSc1d1	početná
arabská	arabský	k2eAgFnSc1d1	arabská
menšina	menšina	k1gFnSc1	menšina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
nebo	nebo	k8xC	nebo
kulinářství	kulinářství	k1gNnSc2	kulinářství
<g/>
.	.	kIx.	.
<g/>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
literatura	literatura	k1gFnSc1	literatura
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
tradice	tradice	k1gFnSc2	tradice
literatury	literatura	k1gFnSc2	literatura
židovské	židovská	k1gFnSc2	židovská
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
kořeny	kořen	k1gInPc1	kořen
sahají	sahat	k5eAaImIp3nP	sahat
k	k	k7c3	k
autorům	autor	k1gMnPc3	autor
Tóry	tóra	k1gFnSc2	tóra
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
píšící	píšící	k2eAgMnPc1d1	píšící
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
příslušníky	příslušník	k1gMnPc4	příslušník
světového	světový	k2eAgInSc2d1	světový
literárního	literární	k2eAgInSc2d1	literární
kánonu	kánon	k1gInSc2	kánon
(	(	kIx(	(
<g/>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
Marcel	Marcel	k1gMnSc1	Marcel
Proust	Proust	k1gMnSc1	Proust
<g/>
,	,	kIx,	,
Isaac	Isaac	k1gFnSc1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
<g/>
,	,	kIx,	,
Boris	Boris	k1gMnSc1	Boris
Pasternak	Pasternak	k1gMnSc1	Pasternak
<g/>
,	,	kIx,	,
Nadine	Nadin	k1gInSc5	Nadin
Gordimerová	Gordimerová	k1gFnSc5	Gordimerová
<g/>
,	,	kIx,	,
Josif	Josif	k1gMnSc1	Josif
Brodskij	Brodskij	k1gMnSc1	Brodskij
<g/>
,	,	kIx,	,
Harold	Harold	k1gMnSc1	Harold
Pinter	Pinter	k1gMnSc1	Pinter
<g/>
,	,	kIx,	,
Heinrich	Heinrich	k1gMnSc1	Heinrich
Heine	Hein	k1gInSc5	Hein
<g/>
,	,	kIx,	,
Saul	Saul	k1gMnSc1	Saul
Bellow	Bellow	k1gMnSc1	Bellow
<g/>
,	,	kIx,	,
Isaac	Isaac	k1gFnSc1	Isaac
Bashevis	Bashevis	k1gFnSc1	Bashevis
<g />
.	.	kIx.	.
</s>
<s>
Singer	Singer	k1gMnSc1	Singer
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
Imre	Imre	k1gFnSc1	Imre
Kertész	Kertész	k1gInSc1	Kertész
<g/>
,	,	kIx,	,
J.	J.	kA	J.
D.	D.	kA	D.
Salinger	Salingra	k1gFnPc2	Salingra
<g/>
,	,	kIx,	,
Nelly	Nella	k1gFnSc2	Nella
Sachsová	Sachsový	k2eAgFnSc1d1	Sachsový
<g/>
,	,	kIx,	,
Elias	Elias	k1gMnSc1	Elias
Canetti	Canetť	k1gFnSc2	Canetť
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
Zweig	Zweig	k1gMnSc1	Zweig
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
Roth	Roth	k1gMnSc1	Roth
<g/>
,	,	kIx,	,
Allen	Allen	k1gMnSc1	Allen
Ginsberg	Ginsberg	k1gMnSc1	Ginsberg
<g/>
,	,	kIx,	,
Primo	primo	k1gNnSc1	primo
Levi	Lev	k1gFnSc2	Lev
<g/>
,	,	kIx,	,
Tristan	Tristan	k1gInSc4	Tristan
Tzara	Tzar	k1gInSc2	Tzar
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
Mailer	Mailer	k1gMnSc1	Mailer
<g/>
,	,	kIx,	,
Osip	Osip	k1gMnSc1	Osip
Mandelštam	Mandelštam	k1gInSc1	Mandelštam
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Schnitzler	Schnitzler	k1gMnSc1	Schnitzler
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
Döblin	Döblin	k1gInSc1	Döblin
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Koestler	Koestler	k1gMnSc1	Koestler
<g/>
,	,	kIx,	,
Isaak	Isaak	k1gMnSc1	Isaak
Babel	Babel	k1gMnSc1	Babel
<g/>
,	,	kIx,	,
Lion	Lion	k1gMnSc1	Lion
Feuchtwanger	Feuchtwanger	k1gMnSc1	Feuchtwanger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
psaná	psaný	k2eAgFnSc1d1	psaná
převážně	převážně	k6eAd1	převážně
hebrejsky	hebrejsky	k6eAd1	hebrejsky
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
poezii	poezie	k1gFnSc4	poezie
a	a	k8xC	a
prózu	próza	k1gFnSc4	próza
<g/>
.	.	kIx.	.
</s>
<s>
Obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
hebrejských	hebrejský	k2eAgFnPc2d1	hebrejská
knih	kniha	k1gFnPc2	kniha
lze	lze	k6eAd1	lze
přičíst	přičíst	k5eAaPmF	přičíst
na	na	k7c4	na
vrub	vrub	k1gInSc4	vrub
renesanci	renesance	k1gFnSc4	renesance
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
však	však	k9	však
publikována	publikovat	k5eAaBmNgFnS	publikovat
i	i	k9	i
například	například	k6eAd1	například
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
nebo	nebo	k8xC	nebo
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgInPc4	dva
výtisky	výtisk	k1gInPc4	výtisk
všech	všecek	k3xTgFnPc2	všecek
knih	kniha	k1gFnPc2	kniha
vydaných	vydaný	k2eAgFnPc2d1	vydaná
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
uloženy	uložit	k5eAaPmNgFnP	uložit
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
a	a	k8xC	a
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
knihovny	knihovna	k1gFnSc2	knihovna
při	při	k7c6	při
Hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
audio	audio	k2eAgFnSc4d1	audio
a	a	k8xC	a
video	video	k1gNnSc1	video
nahrávky	nahrávka	k1gFnSc2	nahrávka
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
typy	typ	k1gInPc1	typ
netištěných	tištěný	k2eNgNnPc2d1	netištěné
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
85	[number]	k4	85
%	%	kIx~	%
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
ukládaných	ukládaný	k2eAgFnPc2d1	ukládaná
do	do	k7c2	do
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
koná	konat	k5eAaImIp3nS	konat
festival	festival	k1gInSc1	festival
Týden	týden	k1gInSc1	týden
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
festivalu	festival	k1gInSc2	festival
je	být	k5eAaImIp3nS	být
veletrh	veletrh	k1gInSc1	veletrh
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
autorská	autorský	k2eAgNnPc1d1	autorské
čtení	čtení	k1gNnPc1	čtení
a	a	k8xC	a
prezentace	prezentace	k1gFnSc1	prezentace
izraelských	izraelský	k2eAgMnPc2d1	izraelský
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
zde	zde	k6eAd1	zde
také	také	k9	také
udíleno	udílen	k2eAgNnSc4d1	udíleno
nejprestižnější	prestižní	k2eAgNnSc4d3	nejprestižnější
izraelské	izraelský	k2eAgNnSc4d1	izraelské
literární	literární	k2eAgNnSc4d1	literární
ocenění	ocenění	k1gNnSc4	ocenění
−	−	k?	−
Sapirova	Sapirov	k1gInSc2	Sapirov
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Izraelský	izraelský	k2eAgMnSc1d1	izraelský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Šmu	Šmu	k1gMnSc1	Šmu
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
Josef	Josef	k1gMnSc1	Josef
Agnon	Agnon	k1gMnSc1	Agnon
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Nejčtenějšími	čtený	k2eAgMnPc7d3	nejčtenější
izraelskými	izraelský	k2eAgMnPc7d1	izraelský
autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
Amos	Amos	k1gMnSc1	Amos
Oz	Oz	k1gMnSc1	Oz
a	a	k8xC	a
Ephraim	Ephraim	k1gMnSc1	Ephraim
Kishon	Kishon	k1gMnSc1	Kishon
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Grossman	Grossman	k1gMnSc1	Grossman
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
Man	Man	k1gMnSc1	Man
Bookerovu	Bookerův	k2eAgFnSc4d1	Bookerova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Avraham	Avraham	k1gInSc1	Avraham
B.	B.	kA	B.
Jehošua	Jehošuus	k1gMnSc2	Jehošuus
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
izraelský	izraelský	k2eAgInSc1d1	izraelský
Faulkner	Faulkner	k1gInSc1	Faulkner
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
postmoderně	postmoderna	k1gFnSc3	postmoderna
je	být	k5eAaImIp3nS	být
řazeno	řazen	k2eAgNnSc1d1	řazeno
dílo	dílo	k1gNnSc1	dílo
Etgara	Etgar	k1gMnSc2	Etgar
Kereta	Keret	k1gMnSc2	Keret
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nevyhýbá	vyhýbat	k5eNaImIp3nS	vyhýbat
ani	ani	k8xC	ani
tvorbě	tvorba	k1gFnSc3	tvorba
komiksů	komiks	k1gInPc2	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
izraelského	izraelský	k2eAgMnSc4d1	izraelský
básníka	básník	k1gMnSc4	básník
bývá	bývat	k5eAaImIp3nS	bývat
označován	označován	k2eAgMnSc1d1	označován
Jehuda	Jehuda	k1gMnSc1	Jehuda
Amichai	Amicha	k1gFnSc2	Amicha
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
nebyl	být	k5eNaImAgMnS	být
Izraelcem	Izraelec	k1gMnSc7	Izraelec
<g/>
,	,	kIx,	,
za	za	k7c2	za
národního	národní	k2eAgMnSc2d1	národní
básníka	básník	k1gMnSc2	básník
označují	označovat	k5eAaImIp3nP	označovat
Izraelci	Izraelec	k1gMnPc1	Izraelec
Chajima	Chajim	k1gMnSc2	Chajim
Nachmana	Nachman	k1gMnSc2	Nachman
Bialika	Bialik	k1gMnSc2	Bialik
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
jazyk	jazyk	k1gInSc4	jazyk
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
hudba	hudba	k1gFnSc1	hudba
může	moct	k5eAaImIp3nS	moct
čerpat	čerpat	k5eAaImF	čerpat
z	z	k7c2	z
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
tradic	tradice	k1gFnPc2	tradice
židovské	židovský	k2eAgFnSc2d1	židovská
hudby	hudba	k1gFnSc2	hudba
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
Mendelssohn-Bartholdy	Mendelssohn-Bartholda	k1gFnSc2	Mendelssohn-Bartholda
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
Offenbach	Offenbach	k1gMnSc1	Offenbach
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Gershwin	Gershwin	k1gMnSc1	Gershwin
<g/>
,	,	kIx,	,
Arnold	Arnold	k1gMnSc1	Arnold
Schoenberg	Schoenberg	k1gMnSc1	Schoenberg
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
Glass	Glass	k1gInSc1	Glass
<g/>
,	,	kIx,	,
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
<g/>
,	,	kIx,	,
Irving	Irving	k1gInSc1	Irving
Berlin	berlina	k1gFnPc2	berlina
<g/>
,	,	kIx,	,
Yehudi	Yehud	k1gMnPc1	Yehud
Menuhin	Menuhin	k1gMnSc1	Menuhin
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Oistrach	Oistrach	k1gMnSc1	Oistrach
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Dylan	Dylan	k1gMnSc1	Dylan
<g/>
,	,	kIx,	,
Leonard	Leonard	k1gMnSc1	Leonard
Cohen	Cohen	k1gInSc1	Cohen
<g/>
,	,	kIx,	,
Barbra	Barbra	k1gFnSc1	Barbra
Streisandová	Streisandový	k2eAgFnSc1d1	Streisandová
<g/>
,	,	kIx,	,
Lou	Lou	k1gMnSc1	Lou
Reed	Reed	k1gMnSc1	Reed
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Simon	Simon	k1gMnSc1	Simon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
hudba	hudba	k1gFnSc1	hudba
také	také	k9	také
odráží	odrážet	k5eAaImIp3nS	odrážet
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
izraelské	izraelský	k2eAgFnSc2d1	izraelská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
hudebními	hudební	k2eAgInPc7d1	hudební
styly	styl	k1gInPc7	styl
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jemenskou	jemenský	k2eAgFnSc7d1	Jemenská
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
chasidskými	chasidský	k2eAgFnPc7d1	chasidská
melodiemi	melodie	k1gFnPc7	melodie
<g/>
,	,	kIx,	,
klezmerem	klezmero	k1gNnSc7	klezmero
<g/>
,	,	kIx,	,
arabskou	arabský	k2eAgFnSc7d1	arabská
či	či	k8xC	či
řeckou	řecký	k2eAgFnSc7d1	řecká
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
jazzem	jazz	k1gInSc7	jazz
<g/>
,	,	kIx,	,
popem	pop	k1gInSc7	pop
nebo	nebo	k8xC	nebo
rockem	rock	k1gInSc7	rock
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
světově	světově	k6eAd1	světově
uznávaná	uznávaný	k2eAgNnPc4d1	uznávané
tělesa	těleso	k1gNnPc4	těleso
patří	patřit	k5eAaImIp3nS	patřit
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
každoročně	každoročně	k6eAd1	každoročně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc1	dva
stě	sto	k4xCgFnPc1	sto
představení	představení	k1gNnPc1	představení
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
rodištěm	rodiště	k1gNnSc7	rodiště
mnoha	mnoho	k4c3	mnoho
hudebníků	hudebník	k1gMnPc2	hudebník
−	−	k?	−
celosvětově	celosvětově	k6eAd1	celosvětově
známí	známý	k1gMnPc1	známý
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
houslisté	houslista	k1gMnPc1	houslista
Jicchak	Jicchak	k1gMnSc1	Jicchak
Perlman	Perlman	k1gMnSc1	Perlman
a	a	k8xC	a
Pinchas	Pinchas	k1gMnSc1	Pinchas
Zukerman	Zukerman	k1gMnSc1	Zukerman
<g/>
.	.	kIx.	.
</s>
<s>
Izraelské	izraelský	k2eAgNnSc1d1	izraelské
občanství	občanství	k1gNnSc1	občanství
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
klavíristé	klavírista	k1gMnPc1	klavírista
Daniel	Daniel	k1gMnSc1	Daniel
Barenboim	Barenboim	k1gMnSc1	Barenboim
a	a	k8xC	a
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Igorjevič	Igorjevič	k1gMnSc1	Igorjevič
Kissin	Kissin	k1gMnSc1	Kissin
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
hvězdám	hvězda	k1gFnPc3	hvězda
izraelského	izraelský	k2eAgInSc2d1	izraelský
popu	pop	k1gInSc2	pop
patří	patřit	k5eAaImIp3nS	patřit
Ofra	Ofr	k2eAgFnSc1d1	Ofra
Haza	Haza	k1gFnSc1	Haza
<g/>
,	,	kIx,	,
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
Gene	gen	k1gInSc5	gen
Simmons	Simmons	k1gInSc4	Simmons
známý	známý	k2eAgInSc4d1	známý
z	z	k7c2	z
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
či	či	k8xC	či
Hillel	Hillel	k1gMnSc1	Hillel
Slovak	Slovak	k1gMnSc1	Slovak
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Red	Red	k1gFnSc2	Red
Hot	hot	k0	hot
Chili	Chil	k1gMnSc3	Chil
Peppers	Peppersa	k1gFnPc2	Peppersa
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
se	se	k3xPyFc4	se
takřka	takřka	k6eAd1	takřka
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
účastní	účastnit	k5eAaImIp3nP	účastnit
soutěže	soutěž	k1gFnPc1	soutěž
Velká	velká	k1gFnSc1	velká
cena	cena	k1gFnSc1	cena
Eurovize	Eurovize	k1gFnSc1	Eurovize
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
již	již	k6eAd1	již
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
pořádal	pořádat	k5eAaImAgInS	pořádat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ejlatu	Ejlat	k1gInSc6	Ejlat
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
koná	konat	k5eAaImIp3nS	konat
každoroční	každoroční	k2eAgFnSc1d1	každoroční
letní	letní	k2eAgFnSc1d1	letní
Red	Red	k1gFnSc1	Red
Sea	Sea	k1gFnSc1	Sea
Jazz	jazz	k1gInSc4	jazz
Festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
<g/>
Židovské	židovský	k2eAgNnSc1d1	Židovské
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
limitováno	limitovat	k5eAaBmNgNnS	limitovat
biblickým	biblický	k2eAgInSc7d1	biblický
zákazem	zákaz	k1gInSc7	zákaz
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
živé	živý	k2eAgFnSc3d1	živá
bytosti	bytost	k1gFnSc3	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
našlo	najít	k5eAaPmAgNnS	najít
dost	dost	k6eAd1	dost
židovských	židovský	k2eAgInPc2d1	židovský
výtvarných	výtvarný	k2eAgInPc2d1	výtvarný
talentů	talent	k1gInPc2	talent
(	(	kIx(	(
<g/>
Marc	Marc	k1gInSc1	Marc
Chagall	Chagalla	k1gFnPc2	Chagalla
<g/>
,	,	kIx,	,
Amedeo	Amedeo	k6eAd1	Amedeo
Modigliani	Modiglian	k1gMnPc1	Modiglian
<g/>
,	,	kIx,	,
Camille	Camille	k1gFnPc1	Camille
Pissarro	Pissarro	k1gNnSc1	Pissarro
<g/>
,	,	kIx,	,
Man	Man	k1gMnSc1	Man
Ray	Ray	k1gMnSc1	Ray
<g/>
,	,	kIx,	,
Roy	Roy	k1gMnSc1	Roy
Lichtenstein	Lichtenstein	k1gMnSc1	Lichtenstein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Izraelské	izraelský	k2eAgNnSc4d1	izraelské
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
například	například	k6eAd1	například
Yaacov	Yaacov	k1gInSc4	Yaacov
Agam	Agama	k1gFnPc2	Agama
a	a	k8xC	a
Marcel	Marcel	k1gMnSc1	Marcel
Janco	Janco	k1gMnSc1	Janco
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
izraelským	izraelský	k2eAgMnSc7d1	izraelský
architektem	architekt	k1gMnSc7	architekt
je	být	k5eAaImIp3nS	být
Moše	mocha	k1gFnSc3	mocha
Safdie	Safdie	k1gFnSc2	Safdie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Marina	Marina	k1gFnSc1	Marina
Bay	Bay	k1gMnSc1	Bay
Sand	Sand	k1gMnSc1	Sand
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Židé	Žid	k1gMnPc1	Žid
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
prosadili	prosadit	k5eAaPmAgMnP	prosadit
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
i	i	k8xC	i
evropském	evropský	k2eAgInSc6d1	evropský
filmu	film	k1gInSc6	film
(	(	kIx(	(
<g/>
Woody	Wooda	k1gFnSc2	Wooda
Allen	Allen	k1gMnSc1	Allen
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Ejzenštejn	Ejzenštejn	k1gMnSc1	Ejzenštejn
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
<g/>
,	,	kIx,	,
Stanley	Stanlea	k1gFnPc1	Stanlea
Kubrick	Kubrick	k1gInSc1	Kubrick
<g/>
,	,	kIx,	,
Sidney	Sidnea	k1gFnPc1	Sidnea
Lumet	Lumet	k1gInSc1	Lumet
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Cukor	Cukor	k1gMnSc1	Cukor
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Cronenberg	Cronenberg	k1gMnSc1	Cronenberg
<g/>
,	,	kIx,	,
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
<g/>
,	,	kIx,	,
Mila	Mila	k1gFnSc1	Mila
Kunisová	Kunisový	k2eAgFnSc1d1	Kunisová
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Newman	Newman	k1gMnSc1	Newman
<g/>
,	,	kIx,	,
Dustin	Dustin	k1gMnSc1	Dustin
Hoffman	Hoffman	k1gMnSc1	Hoffman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Izraelský	izraelský	k2eAgInSc1d1	izraelský
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
skromnější	skromný	k2eAgFnSc1d2	skromnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
už	už	k6eAd1	už
jistých	jistý	k2eAgInPc2d1	jistý
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Izrelský	Izrelský	k2eAgMnSc1d1	Izrelský
režisér	režisér	k1gMnSc1	režisér
Moše	mocha	k1gFnSc6	mocha
Mizrahi	Mizrah	k1gFnSc3	Mizrah
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgMnPc3d1	známý
režisérům	režisér	k1gMnPc3	režisér
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Menachem	Menach	k1gInSc7	Menach
Golan	Golana	k1gFnPc2	Golana
<g/>
,	,	kIx,	,
Eytan	Eytan	k1gMnSc1	Eytan
Fox	fox	k1gInSc1	fox
či	či	k8xC	či
Amos	Amos	k1gMnSc1	Amos
Gitai	Gita	k1gFnSc2	Gita
<g/>
.	.	kIx.	.
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
herečka	herečka	k1gFnSc1	herečka
Natalie	Natalie	k1gFnSc1	Natalie
Portmanová	Portmanová	k1gFnSc1	Portmanová
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hollywoodskou	hollywoodský	k2eAgFnSc7d1	hollywoodská
hvězdou	hvězda	k1gFnSc7	hvězda
první	první	k4xOgFnSc2	první
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
v	v	k7c6	v
akčních	akční	k2eAgInPc6d1	akční
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
Gal	Gal	k1gMnSc1	Gal
Gadotová	Gadotový	k2eAgFnSc5d1	Gadotový
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgInPc3d1	známý
hercům	herc	k1gInPc3	herc
patří	patřit	k5eAaImIp3nS	patřit
i	i	k8xC	i
Chaim	Chaim	k1gMnSc1	Chaim
Topol	Topol	k1gMnSc1	Topol
<g/>
,	,	kIx,	,
Oded	Oded	k1gMnSc1	Oded
Fehr	Fehr	k1gMnSc1	Fehr
<g/>
,	,	kIx,	,
Ayelet	Ayelet	k1gInSc1	Ayelet
Zurerová	Zurerová	k1gFnSc1	Zurerová
nebo	nebo	k8xC	nebo
Brian	Brian	k1gMnSc1	Brian
George	George	k1gInSc4	George
proslavený	proslavený	k2eAgInSc4d1	proslavený
rolí	role	k1gFnSc7	role
otce	otec	k1gMnSc2	otec
Rajeshe	Rajesh	k1gMnSc2	Rajesh
Koothrappaliho	Koothrappali	k1gMnSc2	Koothrappali
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Teorie	teorie	k1gFnSc1	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
.	.	kIx.	.
</s>
<s>
Uri	Uri	k?	Uri
Geller	Geller	k1gMnSc1	Geller
je	být	k5eAaImIp3nS	být
známým	známý	k2eAgMnSc7d1	známý
iluzionistou	iluzionista	k1gMnSc7	iluzionista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakožto	jakožto	k8xS	jakožto
pokračovatel	pokračovatel	k1gMnSc1	pokračovatel
divadelních	divadelní	k2eAgFnPc2d1	divadelní
tradic	tradice	k1gFnPc2	tradice
východoevropského	východoevropský	k2eAgNnSc2d1	východoevropské
jidiš	jidiš	k1gNnSc2	jidiš
divadla	divadlo	k1gNnSc2	divadlo
má	mít	k5eAaImIp3nS	mít
Izrael	Izrael	k1gInSc1	Izrael
živou	živý	k2eAgFnSc4d1	živá
a	a	k8xC	a
činorodou	činorodý	k2eAgFnSc4d1	činorodá
divadelní	divadelní	k2eAgFnSc4d1	divadelní
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
divadelní	divadelní	k2eAgFnSc7d1	divadelní
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
národním	národní	k2eAgNnSc7d1	národní
divadlem	divadlo	k1gNnSc7	divadlo
je	být	k5eAaImIp3nS	být
divadlo	divadlo	k1gNnSc1	divadlo
ha-Bima	ha-Bimum	k1gNnSc2	ha-Bimum
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
množství	množství	k1gNnSc1	množství
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nP	patřit
Izraelské	izraelský	k2eAgNnSc1d1	izraelské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
a	a	k8xC	a
Svatyně	svatyně	k1gFnSc2	svatyně
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
svitky	svitek	k1gInPc7	svitek
od	od	k7c2	od
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
sbírky	sbírka	k1gFnPc1	sbírka
židovského	židovský	k2eAgNnSc2d1	Židovské
a	a	k8xC	a
evropského	evropský	k2eAgNnSc2d1	Evropské
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
památník	památník	k1gInSc1	památník
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
hrdinů	hrdina	k1gMnPc2	hrdina
holocaustu	holocaust	k1gInSc2	holocaust
Jad	Jad	k1gMnSc1	Jad
vašem	váš	k3xOp2gInSc6	váš
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
a	a	k8xC	a
nejobsáhlejší	obsáhlý	k2eAgInSc4d3	nejobsáhlejší
archiv	archiv	k1gInSc4	archiv
dokumentů	dokument	k1gInPc2	dokument
o	o	k7c6	o
holocaustu	holocaust	k1gInSc6	holocaust
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kampusu	kampus	k1gInSc6	kampus
Telavivské	telavivský	k2eAgFnSc2d1	Telavivská
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
muzeum	muzeum	k1gNnSc1	muzeum
diaspory	diaspora	k1gFnSc2	diaspora
Bejt	Bejt	k?	Bejt
ha-Tfucot	ha-Tfucot	k1gMnSc1	ha-Tfucot
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgNnPc4d1	hlavní
muzea	muzeum	k1gNnPc4	muzeum
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
menších	malý	k2eAgNnPc2d2	menší
muzeí	muzeum	k1gNnPc2	muzeum
s	s	k7c7	s
kvalitními	kvalitní	k2eAgFnPc7d1	kvalitní
uměleckými	umělecký	k2eAgFnPc7d1	umělecká
sbírkami	sbírka	k1gFnPc7	sbírka
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
a	a	k8xC	a
kibucech	kibuc	k1gInPc6	kibuc
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
oplývá	oplývat	k5eAaImIp3nS	oplývat
množstvím	množství	k1gNnSc7	množství
archeologických	archeologický	k2eAgNnPc2d1	Archeologické
nalezišť	naleziště	k1gNnPc2	naleziště
a	a	k8xC	a
vykopávek	vykopávka	k1gFnPc2	vykopávka
<g/>
,	,	kIx,	,
archeologie	archeologie	k1gFnSc1	archeologie
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
představuje	představovat	k5eAaImIp3nS	představovat
významnou	významný	k2eAgFnSc4d1	významná
disciplínu	disciplína	k1gFnSc4	disciplína
a	a	k8xC	a
mnohé	mnohý	k2eAgInPc1d1	mnohý
její	její	k3xOp3gInPc1	její
objevy	objev	k1gInPc1	objev
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
cílem	cíl	k1gInSc7	cíl
turistických	turistický	k2eAgFnPc2d1	turistická
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Sport	sport	k1gInSc1	sport
a	a	k8xC	a
fyzická	fyzický	k2eAgNnPc1d1	fyzické
cvičení	cvičení	k1gNnPc1	cvičení
nebyla	být	k5eNaImAgNnP	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
židovské	židovský	k2eAgFnSc2d1	židovská
kultury	kultura	k1gFnSc2	kultura
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
atletické	atletický	k2eAgFnPc4d1	atletická
dovednosti	dovednost	k1gFnPc4	dovednost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
oceňovány	oceňovat	k5eAaImNgFnP	oceňovat
starověkými	starověký	k2eAgMnPc7d1	starověký
Řeky	Řek	k1gMnPc7	Řek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Židé	Žid	k1gMnPc1	Žid
dívali	dívat	k5eAaImAgMnP	dívat
skrz	skrz	k7c4	skrz
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byly	být	k5eAaImAgInP	být
chápány	chápat	k5eAaImNgInP	chápat
jako	jako	k8xC	jako
nevítaný	vítaný	k2eNgInSc1d1	nevítaný
dopad	dopad	k1gInSc1	dopad
helénistické	helénistický	k2eAgFnSc2d1	helénistická
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
středověký	středověký	k2eAgMnSc1d1	středověký
rabín	rabín	k1gMnSc1	rabín
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Maimonides	Maimonides	k1gMnSc1	Maimonides
však	však	k9	však
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
důležitost	důležitost	k1gFnSc4	důležitost
fyzické	fyzický	k2eAgFnSc2d1	fyzická
aktivity	aktivita	k1gFnSc2	aktivita
a	a	k8xC	a
udržování	udržování	k1gNnSc4	udržování
se	se	k3xPyFc4	se
v	v	k7c6	v
kondici	kondice	k1gFnSc6	kondice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
názor	názor	k1gInSc1	názor
byl	být	k5eAaImAgInS	být
oživen	oživit	k5eAaPmNgInS	oživit
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Maxem	Max	k1gMnSc7	Max
Nordauem	Nordau	k1gMnSc7	Nordau
a	a	k8xC	a
počátkem	počátek	k1gInSc7	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
vrchním	vrchní	k2eAgMnSc7d1	vrchní
palestinským	palestinský	k2eAgMnSc7d1	palestinský
rabínem	rabín	k1gMnSc7	rabín
Abrahamem	Abraham	k1gMnSc7	Abraham
Issacem	Issace	k1gMnSc7	Issace
Kookem	Kooek	k1gMnSc7	Kooek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
tělo	tělo	k1gNnSc1	tělo
slouží	sloužit	k5eAaImIp3nS	sloužit
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
zdravé	zdravý	k2eAgNnSc1d1	zdravé
tělo	tělo	k1gNnSc1	tělo
může	moct	k5eAaImIp3nS	moct
zaručit	zaručit	k5eAaPmF	zaručit
zdravou	zdravý	k2eAgFnSc4d1	zdravá
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
konají	konat	k5eAaImIp3nP	konat
Makabejské	makabejský	k2eAgFnPc4d1	Makabejská
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
tamní	tamní	k2eAgMnPc4d1	tamní
atlety	atlet	k1gMnPc4	atlet
událostí	událost	k1gFnSc7	událost
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
s	s	k7c7	s
Olympijskými	olympijský	k2eAgFnPc7d1	olympijská
hrami	hra	k1gFnPc7	hra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
divácky	divácky	k6eAd1	divácky
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
sporty	sport	k1gInPc4	sport
patří	patřit	k5eAaImIp3nP	patřit
fotbal	fotbal	k1gInSc4	fotbal
a	a	k8xC	a
basketbal	basketbal	k1gInSc4	basketbal
<g/>
.	.	kIx.	.
</s>
<s>
Basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
Makabi	Makab	k1gFnSc2	Makab
Tel	tel	kA	tel
Aviv	Aviv	k1gMnSc1	Aviv
šestkrát	šestkrát	k6eAd1	šestkrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Euroligu	Euroliga	k1gFnSc4	Euroliga
<g/>
.	.	kIx.	.
</s>
<s>
Miki	Miki	k6eAd1	Miki
Berkovich	Berkovich	k1gMnSc1	Berkovich
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Beerševa	Beerševa	k1gFnSc1	Beerševa
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
mezinárodním	mezinárodní	k2eAgMnSc7d1	mezinárodní
šachovým	šachový	k2eAgMnSc7d1	šachový
centrem	centr	k1gMnSc7	centr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
šachových	šachový	k2eAgMnPc2d1	šachový
mistrů	mistr	k1gMnPc2	mistr
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konalo	konat	k5eAaImAgNnS	konat
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
družstev	družstvo	k1gNnPc2	družstvo
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
a	a	k8xC	a
zdejší	zdejší	k2eAgMnSc1d1	zdejší
velmistr	velmistr	k1gMnSc1	velmistr
Boris	Boris	k1gMnSc1	Boris
Gelfand	Gelfanda	k1gFnPc2	Gelfanda
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
získal	získat	k5eAaPmAgInS	získat
celkem	celkem	k6eAd1	celkem
devět	devět	k4xCc4	devět
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
z	z	k7c2	z
LOH	LOH	kA	LOH
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
v	v	k7c6	v
disciplíně	disciplína	k1gFnSc6	disciplína
windsurfing	windsurfing	k1gInSc1	windsurfing
(	(	kIx(	(
<g/>
Gal	Gal	k1gMnSc1	Gal
Fridman	Fridman	k1gMnSc1	Fridman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
získali	získat	k5eAaPmAgMnP	získat
Izrelci	Izrelec	k1gMnPc1	Izrelec
v	v	k7c6	v
judu	judo	k1gNnSc6	judo
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc7	první
pro	pro	k7c4	pro
Izrael	Izrael	k1gInSc4	Izrael
<g/>
,	,	kIx,	,
stříbrnou	stříbrná	k1gFnSc4	stříbrná
<g/>
,	,	kIx,	,
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
judistka	judistka	k1gFnSc1	judistka
Ja	Ja	k?	Ja
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
Aradová	Aradový	k2eAgFnSc1d1	Aradový
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
bilance	bilance	k1gFnSc1	bilance
je	být	k5eAaImIp3nS	být
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
základy	základ	k1gInPc1	základ
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
osnov	osnova	k1gFnPc2	osnova
na	na	k7c6	na
izraelských	izraelský	k2eAgFnPc6d1	izraelská
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
i	i	k9	i
zcela	zcela	k6eAd1	zcela
originální	originální	k2eAgInSc4d1	originální
systém	systém	k1gInSc4	systém
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
zvaný	zvaný	k2eAgInSc1d1	zvaný
krav	kráva	k1gFnPc2	kráva
maga	magum	k1gNnSc2	magum
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
základy	základ	k1gInPc1	základ
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
bratislavský	bratislavský	k2eAgMnSc1d1	bratislavský
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
příslušník	příslušník	k1gMnSc1	příslušník
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
Imi	Imi	k1gMnSc1	Imi
Lichtenfeld	Lichtenfeld	k1gMnSc1	Lichtenfeld
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
izraelským	izraelský	k2eAgMnSc7d1	izraelský
fotbalistou	fotbalista	k1gMnSc7	fotbalista
byl	být	k5eAaImAgMnS	být
Josi	Jose	k1gFnSc4	Jose
Benajun	Benajun	k1gMnSc1	Benajun
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
Chelsea	Chelse	k1gInSc2	Chelse
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Evropskou	evropský	k2eAgFnSc4d1	Evropská
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c6	na
lavičce	lavička	k1gFnSc6	lavička
Chelsea	Chelsea	k1gMnSc1	Chelsea
zažil	zažít	k5eAaPmAgMnS	zažít
největší	veliký	k2eAgInPc4d3	veliký
úspěchy	úspěch	k1gInPc4	úspěch
izraelských	izraelský	k2eAgInPc2d1	izraelský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
Avram	Avram	k1gInSc1	Avram
Grant	grant	k1gInSc4	grant
<g/>
,	,	kIx,	,
dovedl	dovést	k5eAaPmAgMnS	dovést
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Tenisté	tenista	k1gMnPc1	tenista
Andy	Anda	k1gFnSc2	Anda
Ram	Ram	k1gMnSc1	Ram
a	a	k8xC	a
Jonatan	Jonatan	k1gMnSc1	Jonatan
Erlich	Erlich	k1gMnSc1	Erlich
společně	společně	k6eAd1	společně
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
gymnastka	gymnastka	k1gFnSc1	gymnastka
Ágnes	Ágnesa	k1gFnPc2	Ágnesa
Keletiová	Keletiový	k2eAgFnSc1d1	Keletiový
získala	získat	k5eAaPmAgFnS	získat
pět	pět	k4xCc4	pět
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
v	v	k7c6	v
dresu	dres	k1gInSc6	dres
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
občankou	občanka	k1gFnSc7	občanka
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
neizraelským	izraelský	k2eNgMnSc7d1	izraelský
židovským	židovský	k2eAgMnSc7d1	židovský
sportovcem	sportovec	k1gMnSc7	sportovec
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
plavec	plavec	k1gMnSc1	plavec
Mark	Mark	k1gMnSc1	Mark
Spitz	Spitz	k1gMnSc1	Spitz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
izraelských	izraelský	k2eAgMnPc2d1	izraelský
sportovců	sportovec	k1gMnPc2	sportovec
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
arabskými	arabský	k2eAgMnPc7d1	arabský
teroristy	terorista	k1gMnPc7	terorista
během	během	k7c2	během
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Israel	Israela	k1gFnPc2	Israela
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BLACK	BLACK	kA	BLACK
<g/>
,	,	kIx,	,
Ian	Ian	k1gMnSc1	Ian
<g/>
;	;	kIx,	;
MORRIS	MORRIS	kA	MORRIS
<g/>
,	,	kIx,	,
Benny	Benn	k1gInPc7	Benn
<g/>
.	.	kIx.	.
</s>
<s>
Mossad	Mossad	k1gInSc1	Mossad
-	-	kIx~	-
izraelské	izraelský	k2eAgFnSc2d1	izraelská
tajné	tajný	k2eAgFnSc2d1	tajná
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
632	[number]	k4	632
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
392	[number]	k4	392
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BROŽ	Brož	k1gMnSc1	Brož
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Arabsko-izraelské	arabskozraelský	k2eAgFnPc1d1	arabsko-izraelská
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Epocha	epocha	k1gFnSc1	epocha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
360	[number]	k4	360
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86328	[number]	k4	86328
<g/>
-	-	kIx~	-
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČEJKA	Čejka	k1gMnSc1	Čejka
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
<g/>
.	.	kIx.	.
</s>
<s>
Judaismus	judaismus	k1gInSc1	judaismus
<g/>
,	,	kIx,	,
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
Stát	stát	k1gInSc1	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
252	[number]	k4	252
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
3007	[number]	k4	3007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČEJKA	Čejka	k1gMnSc1	Čejka
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Palestina	Palestina	k1gFnSc1	Palestina
-	-	kIx~	-
Minulost	minulost	k1gFnSc1	minulost
<g/>
,	,	kIx,	,
současnost	současnost	k1gFnSc1	současnost
a	a	k8xC	a
směřování	směřování	k1gNnSc1	směřování
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1	blízkovýchodní
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Barrister	Barrister	k1gMnSc1	Barrister
&	&	k?	&
Principal	Principal	k1gMnSc1	Principal
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
321	[number]	k4	321
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87029	[number]	k4	87029
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GILBERT	Gilbert	k1gMnSc1	Gilbert
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
BB	BB	kA	BB
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
668	[number]	k4	668
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7257	[number]	k4	7257
<g/>
-	-	kIx~	-
<g/>
740	[number]	k4	740
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HERZOG	HERZOG	kA	HERZOG
<g/>
,	,	kIx,	,
Chaim	Chaim	k1gMnSc1	Chaim
<g/>
.	.	kIx.	.
</s>
<s>
Arabsko-izraelské	arabskozraelský	k2eAgFnPc1d1	arabsko-izraelská
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
617	[number]	k4	617
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
954	[number]	k4	954
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHAPMAN	CHAPMAN	kA	CHAPMAN
<g/>
,	,	kIx,	,
Colin	Colin	k1gMnSc1	Colin
<g/>
.	.	kIx.	.
</s>
<s>
Čí	čí	k3xOyRgNnSc1	čí
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
zaslíbená	zaslíbený	k2eAgFnSc1d1	zaslíbená
-	-	kIx~	-
Pokračující	pokračující	k2eAgFnSc1d1	pokračující
krize	krize	k1gFnSc1	krize
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Palestinci	Palestinec	k1gMnPc1	Palestinec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
336	[number]	k4	336
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
507	[number]	k4	507
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IZRAELSKÉ	izraelský	k2eAgNnSc1d1	izraelské
INFORMAČNÍ	informační	k2eAgNnSc1d1	informační
STŘEDISKO	středisko	k1gNnSc1	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Fakta	faktum	k1gNnPc1	faktum
o	o	k7c6	o
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
:	:	kIx,	:
země	země	k1gFnSc1	země
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
:	:	kIx,	:
Ahva	Ahva	k1gFnSc1	Ahva
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
28	[number]	k4	28
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KRUPP	KRUPP	kA	KRUPP
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Sionismus	sionismus	k1gInSc1	sionismus
a	a	k8xC	a
Stát	stát	k1gInSc1	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
242	[number]	k4	242
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LIETH	LIETH	kA	LIETH
<g/>
,	,	kIx,	,
Norbert	Norbert	k1gMnSc1	Norbert
<g/>
.	.	kIx.	.
</s>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
stát	stát	k1gInSc1	stát
-	-	kIx~	-
od	od	k7c2	od
pohoršení	pohoršení	k1gNnSc2	pohoršení
k	k	k7c3	k
požehnání	požehnání	k1gNnSc3	požehnání
pro	pro	k7c4	pro
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
A-Alef	A-Alef	k1gMnSc1	A-Alef
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
92	[number]	k4	92
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85237	[number]	k4	85237
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MUSIL	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Zaslíbená	zaslíbený	k2eAgFnSc1d1	zaslíbená
země	země	k1gFnSc1	země
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
228	[number]	k4	228
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POJAR	POJAR	kA	POJAR
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
-	-	kIx~	-
stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
202	[number]	k4	202
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
435	[number]	k4	435
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RUCKER	RUCKER	kA	RUCKER
<g/>
,	,	kIx,	,
Laurent	Laurent	k1gInSc1	Laurent
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	Rybka	k1gMnSc1	Rybka
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
313	[number]	k4	313
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
53	[number]	k4	53
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SACHAR	SACHAR	kA	SACHAR
<g/>
,	,	kIx,	,
Howard	Howard	k1gMnSc1	Howard
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Dějiny	dějiny	k1gFnPc1	dějiny
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Regia	Regia	k1gFnSc1	Regia
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
767	[number]	k4	767
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902484	[number]	k4	902484
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHNEIDER	Schneider	k1gMnSc1	Schneider
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gMnSc1	Ludwig
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
-	-	kIx~	-
ohnisko	ohnisko	k1gNnSc1	ohnisko
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
A-Alef	A-Alef	k1gMnSc1	A-Alef
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
91	[number]	k4	91
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85237	[number]	k4	85237
<g/>
-	-	kIx~	-
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TERNER	TERNER	kA	TERNER
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
:	:	kIx,	:
Kora	Kora	k1gFnSc1	Kora
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
262	[number]	k4	262
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901092	[number]	k4	901092
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FALCONER	FALCONER	kA	FALCONER
<g/>
,	,	kIx,	,
Colin	Colin	k1gMnSc1	Colin
<g/>
.	.	kIx.	.
</s>
<s>
Zběsilost	zběsilost	k1gFnSc1	zběsilost
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Ludmila	Ludmila	k1gFnSc1	Ludmila
Švejdová	Švejdová	k1gFnSc1	Švejdová
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Švejda	Švejda	k1gMnSc1	Švejda
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Středoevropské	středoevropský	k2eAgNnSc1d1	středoevropské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
656	[number]	k4	656
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86004	[number]	k4	86004
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
CestopisyBRŮHA	CestopisyBRŮHA	k1gMnSc1	CestopisyBRŮHA
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
BRŮHOVÁ	Brůhová	k1gFnSc1	Brůhová
<g/>
,	,	kIx,	,
Stanislava	Stanislava	k1gFnSc1	Stanislava
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc5	paní
<g/>
,	,	kIx,	,
tady	tady	k6eAd1	tady
jste	být	k5eAaImIp2nP	být
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
...	...	k?	...
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Brůha	Brůha	k1gMnSc1	Brůha
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
U	U	kA	U
DESIGN	design	k1gInSc1	design
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
187	[number]	k4	187
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
4552	[number]	k4	4552
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HANUŠ	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
HOJKA	HOJKA	kA	HOJKA
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
téměř	téměř	k6eAd1	téměř
mystická	mystický	k2eAgFnSc1d1	mystická
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Elim	Elim	k1gMnSc1	Elim
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
259	[number]	k4	259
s.	s.	k?	s.
</s>
</p>
<p>
<s>
JAZAIRIOVÁ	JAZAIRIOVÁ	kA	JAZAIRIOVÁ
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
,	,	kIx,	,
Palestina	Palestina	k1gFnSc1	Palestina
a	a	k8xC	a
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Radioservis	Radioservis	k1gInSc1	Radioservis
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
200	[number]	k4	200
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86212	[number]	k4	86212
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
PrůvodciČEŘOVSKÝ	PrůvodciČEŘOVSKÝ	k1gMnSc1	PrůvodciČEŘOVSKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
palestinská	palestinský	k2eAgNnPc4d1	palestinské
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
192	[number]	k4	192
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
541	[number]	k4	541
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PAULÍK	PAULÍK	kA	PAULÍK
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Freytag	Freytag	k1gMnSc1	Freytag
&	&	k?	&
Berndt	Berndt	k1gMnSc1	Berndt
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7316	[number]	k4	7316
<g/>
-	-	kIx~	-
<g/>
202	[number]	k4	202
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CUDLÍN	CUDLÍN	kA	CUDLÍN
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Osmdesát	osmdesát	k4xCc4	osmdesát
sedm	sedm	k4xCc4	sedm
magických	magický	k2eAgNnPc2d1	magické
míst	místo	k1gNnPc2	místo
Svaté	svatá	k1gFnSc2	svatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
336	[number]	k4	336
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2111	[number]	k4	2111
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Izrael	Izrael	k1gInSc1	Izrael
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Izrael	Izrael	k1gInSc1	Izrael
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Izrael	Izrael	k1gInSc1	Izrael
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Izrael	Izrael	k1gInSc1	Izrael
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
====	====	k?	====
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
====	====	k?	====
</s>
</p>
<p>
<s>
Portál	portál	k1gInSc1	portál
vlády	vláda	k1gFnSc2	vláda
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kancelář	kancelář	k1gFnSc1	kancelář
premiéra	premiér	k1gMnSc2	premiér
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kneset	Kneset	k1gMnSc1	Kneset
oficiální	oficiální	k2eAgFnSc2d1	oficiální
internetové	internetový	k2eAgFnSc2d1	internetová
stránky	stránka	k1gFnSc2	stránka
izraelského	izraelský	k2eAgInSc2d1	izraelský
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgInSc4d1	centrální
statistický	statistický	k2eAgInSc4d1	statistický
úřad	úřad	k1gInSc4	úřad
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
Izraele	Izrael	k1gInSc2	Izrael
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Obecné	obecný	k2eAgFnPc4d1	obecná
informace	informace	k1gFnPc4	informace
====	====	k?	====
</s>
</p>
<p>
<s>
Čejka	Čejka	k1gMnSc1	Čejka
Marek	Marek	k1gMnSc1	Marek
–	–	k?	–
Blízký	blízký	k2eAgInSc1d1	blízký
východ	východ	k1gInSc1	východ
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
&	&	k?	&
Palestina	Palestina	k1gFnSc1	Palestina
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
historie	historie	k1gFnSc1	historie
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
okolních	okolní	k2eAgInPc2d1	okolní
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Near	Near	k1gInSc1	Near
Eastern	Eastern	k1gMnSc1	Eastern
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Israel	Israel	k1gInSc1	Israel
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2010-12-10	[number]	k4	2010-12-10
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Israel	Israel	k1gInSc1	Israel
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-19	[number]	k4	2011-07-19
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc6	Aviv
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Izrael	Izrael	k1gInSc1	Izrael
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-03-29	[number]	k4	2011-03-29
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ELATH	ELATH	kA	ELATH
<g/>
,	,	kIx,	,
Eliahu	Eliaha	k1gFnSc4	Eliaha
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Israel	Israel	k1gInSc1	Israel
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Média	médium	k1gNnPc1	médium
====	====	k?	====
</s>
</p>
<p>
<s>
Eretz	Eretz	k1gInSc1	Eretz
–	–	k?	–
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
z	z	k7c2	z
Izraele	Izrael	k1gInSc2	Izrael
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
Post	post	k1gInSc1	post
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ynetnews	Ynetnews	k1gInSc1	Ynetnews
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Israel	Israet	k5eAaPmAgInS	Israet
Broadcasting	Broadcasting	k1gInSc1	Broadcasting
Authority	Authorita	k1gFnSc2	Authorita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aruc	Aruc	k6eAd1	Aruc
ševa	ševa	k6eAd1	ševa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kol	kol	k7c2	kol
Jisra	Jisro	k1gNnSc2	Jisro
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Haarec	Haarec	k1gInSc1	Haarec
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Times	Times	k1gMnSc1	Times
of	of	k?	of
Israel	Israel	k1gMnSc1	Israel
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Globes	Globes	k1gInSc1	Globes
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Israel	Israel	k1gInSc1	Israel
Hayom	Hayom	k1gInSc1	Hayom
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
====	====	k?	====
</s>
</p>
<p>
<s>
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Atlas	Atlas	k1gInSc1	Atlas
Izraele	Izrael	k1gInSc2	Izrael
</s>
</p>
<p>
<s>
Israel	Israel	k1gInSc1	Israel
and	and	k?	and
the	the	k?	the
Occupied	Occupied	k1gMnSc1	Occupied
Palestinian	Palestinian	k1gMnSc1	Palestinian
Territories	Territories	k1gMnSc1	Territories
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Israel	Israel	k1gInSc1	Israel
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
-	-	kIx~	-
o	o	k7c6	o
českých	český	k2eAgMnPc6d1	český
krajanech	krajan	k1gMnPc6	krajan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2002-05-16	[number]	k4	2002-05-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
