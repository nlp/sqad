<s>
Elisa	Elisa	k1gFnSc1
Hohenlohe-Langenburská	Hohenlohe-Langenburský	k2eAgFnSc1d1
</s>
<s>
Elisa	Elisa	k1gFnSc1
Hohenlohe-Langenburskákněžna	Hohenlohe-Langenburskákněžn	k1gInSc2
z	z	k7c2
Reussu	Reuss	k1gInSc2
Sňatek	sňatek	k1gInSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1884	#num#	k4
Manžel	manžel	k1gMnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
XXVII	XXVII	kA
<g/>
.	.	kIx.
z	z	k7c2
Reussu	Reuss	k1gInSc2
Úplné	úplný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Elisa	Elisa	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
Feodora	Feodora	k1gFnSc1
Žofie	Žofie	k1gFnSc2
Adelheid	Adelheida	k1gFnPc2
Narození	narození	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1864	#num#	k4
Langenburg	Langenburg	k1gInSc1
<g/>
,	,	kIx,
Württemberské	Württemberský	k2eAgNnSc1d1
království	království	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1929	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Gera	Gera	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Potomci	potomek	k1gMnPc1
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Feodora	Feodora	k1gFnSc1
z	z	k7c2
ReussuLuisa	ReussuLuis	k1gMnSc2
z	z	k7c2
ReussuJindřich	ReussuJindřicha	k1gFnPc2
XL	XL	kA
<g/>
.	.	kIx.
z	z	k7c2
ReussuJindřich	ReussuJindřicha	k1gFnPc2
XLII	XLII	kA
<g/>
.	.	kIx.
z	z	k7c2
ReussuJindřich	ReussuJindřicha	k1gFnPc2
XLV	XLV	kA
<g/>
.	.	kIx.
z	z	k7c2
Reussu	Reuss	k1gInSc2
Rod	rod	k1gInSc1
</s>
<s>
Hohenlohové	Hohenloh	k1gMnPc1
Otec	otec	k1gMnSc1
</s>
<s>
Heřman	Heřman	k1gMnSc1
Hohenlohe-Langenburský	Hohenlohe-Langenburský	k2eAgMnSc1d1
Matka	matka	k1gFnSc1
</s>
<s>
Leopoldina	Leopoldin	k2eAgFnSc1d1
Bádenská	bádenský	k2eAgFnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Elisa	Elisa	k1gFnSc1
Hohenlohe-Langenburská	Hohenlohe-Langenburský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Elisa	Elisa	k1gFnSc1
Viktorie	Viktoria	k1gFnSc2
Feodora	Feodora	k1gFnSc1
Žofie	Žofie	k1gFnSc2
Adelheid	Adelheida	k1gFnPc2
<g/>
;	;	kIx,
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1864	#num#	k4
<g/>
,	,	kIx,
Langenburg	Langenburg	k1gInSc1
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1929	#num#	k4
<g/>
,	,	kIx,
Gera	Gera	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
princeznou	princezna	k1gFnSc7
z	z	k7c2
Hohenlohe-Langenburgu	Hohenlohe-Langenburg	k1gInSc2
a	a	k8xC
sňatkem	sňatek	k1gInSc7
princeznou	princezna	k1gFnSc7
reussovou	reussová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
rodina	rodina	k1gFnSc1
</s>
<s>
Elisa	Elisa	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
v	v	k7c6
Langenburgu	Langenburg	k1gInSc6
jako	jako	k8xS,k8xC
druhé	druhý	k4xOgNnSc1
dítě	dítě	k1gNnSc1
a	a	k8xC
nejstarší	starý	k2eAgFnSc1d3
dcera	dcera	k1gFnSc1
knížete	kníže	k1gMnSc2
Heřmana	Heřman	k1gMnSc2
Hohenlohe-Langenburského	Hohenlohe-Langenburský	k2eAgMnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
manželky	manželka	k1gFnPc1
Leopoldiny	Leopoldin	k2eAgFnPc1d1
<g/>
,	,	kIx,
dcery	dcera	k1gFnPc1
prince	princ	k1gMnSc2
Viléma	Vilém	k1gMnSc2
Bádenského	bádenský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
praneteří	praneteř	k1gFnSc7
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
Viktorie	Viktorie	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
královny	královna	k1gFnSc2
Viktorie	Viktoria	k1gFnSc2
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
z	z	k7c2
prvního	první	k4xOgNnSc2
manželství	manželství	k1gNnSc2
s	s	k7c7
Emichem	Emich	k1gMnSc7
Karlem	Karel	k1gMnSc7
z	z	k7c2
Leiningenu	Leiningen	k1gInSc2
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jedno	jeden	k4xCgNnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
Elisina	Elisin	k2eAgFnSc1d1
babička	babička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Elisa	Elisa	k1gFnSc1
měla	mít	k5eAaImAgFnS
staršího	starý	k2eAgMnSc4d2
bratra	bratr	k1gMnSc4
Arnošta	Arnošt	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
po	po	k7c6
otcově	otcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
7	#num#	k4
<g/>
.	.	kIx.
knížetem	kníže	k1gNnSc7wR
z	z	k7c2
Hohenlohe-Langenburgu	Hohenlohe-Langenburg	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
mladší	mladý	k2eAgFnSc4d2
sestru	sestra	k1gFnSc4
Feodoru	Feodor	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c2
Emicha	Emich	k1gMnSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
knížete	kníže	k1gNnSc4wR
z	z	k7c2
Leiningenu	Leiningen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sňatek	sňatek	k1gInSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1884	#num#	k4
se	se	k3xPyFc4
dvacetiletá	dvacetiletý	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
v	v	k7c6
Langenburgu	Langenburg	k1gInSc6
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
o	o	k7c4
šest	šest	k4xCc4
let	léto	k1gNnPc2
staršího	starý	k2eAgMnSc2d2
Jindřicha	Jindřich	k1gMnSc2
XXVII	XXVII	kA
<g/>
.	.	kIx.
z	z	k7c2
Reussu	Reuss	k1gInSc2
<g/>
,	,	kIx,
nejstaršího	starý	k2eAgMnSc4d3
potomka	potomek	k1gMnSc4
Jindřicha	Jindřich	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
z	z	k7c2
Reussu	Reuss	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Anežky	Anežka	k1gFnSc2
Württemberské	Württemberský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
spolu	spolu	k6eAd1
měli	mít	k5eAaImAgMnP
pět	pět	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Elisa	Elisa	k1gFnSc1
Hohenlohe-Langenburská	Hohenlohe-Langenburský	k2eAgFnSc1d1
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
dětmi	dítě	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Feodora	Feodora	k1gFnSc1
z	z	k7c2
Reussu	Reuss	k1gInSc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1889	#num#	k4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Luisa	Luisa	k1gFnSc1
z	z	k7c2
Reussu	Reuss	k1gInSc2
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1890	#num#	k4
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1951	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
XL	XL	kA
<g/>
.	.	kIx.
z	z	k7c2
Reussu	Reuss	k1gInSc2
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1891	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1891	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
XLII	XLII	kA
<g/>
.	.	kIx.
z	z	k7c2
Reussu	Reuss	k1gInSc2
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1893	#num#	k4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1913	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
XLV	XLV	kA
<g/>
.	.	kIx.
z	z	k7c2
Reussu	Reuss	k1gInSc2
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1895	#num#	k4
–	–	k?
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pozdější	pozdní	k2eAgNnPc1d2
léta	léto	k1gNnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
její	její	k3xOp3gMnSc1
manžel	manžel	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
vládnoucím	vládnoucí	k2eAgMnSc7d1
knížetem	kníže	k1gMnSc7
z	z	k7c2
Reussu	Reuss	k1gInSc2
a	a	k8xC
Elisa	Elisa	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
kněžnou	kněžna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kníže	kníže	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
XXVII	XXVII	kA
<g/>
.	.	kIx.
abdikoval	abdikovat	k5eAaBmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
po	po	k7c6
Listopadové	listopadový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zanikly	zaniknout	k5eAaPmAgFnP
všechny	všechen	k3xTgFnPc1
německé	německý	k2eAgFnPc1d1
monarchie	monarchie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
knížete	kníže	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
přešly	přejít	k5eAaPmAgInP
tituly	titul	k1gInPc1
na	na	k7c4
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
titulárním	titulární	k2eAgMnSc7d1
knížetem	kníže	k1gMnSc7
z	z	k7c2
Reuss	Reussa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Elisa	Elis	k1gMnSc4
přežila	přežít	k5eAaPmAgFnS
svého	svůj	k3xOyFgMnSc4
manžela	manžel	k1gMnSc4
o	o	k7c4
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
a	a	k8xC
zemřela	zemřít	k5eAaPmAgFnS
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Kristián	Kristián	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
Hohenlohe-Langenburský	Hohenlohe-Langenburský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hohenlohe-Langenburský	Hohenlohe-Langenburský	k2eAgMnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Stolbersko-Gedernská	Stolbersko-Gedernská	k1gFnSc1
</s>
<s>
Arnošt	Arnošt	k1gMnSc1
I.	I.	kA
Hohenlohe-Langenburský	Hohenlohe-Langenburský	k2eAgInSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kristián	Kristián	k1gMnSc1
ze	z	k7c2
Solms-Baruth	Solms-Barutha	k1gFnPc2
</s>
<s>
Amálie	Amálie	k1gFnSc1
Henrietta	Henrietta	k1gFnSc1
ze	z	k7c2
Solms-Baruth	Solms-Barutha	k1gFnPc2
</s>
<s>
Frederika	Frederika	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Reuss-Köstritz	Reuss-Köstritz	k1gInSc4
</s>
<s>
Heřman	Heřman	k1gMnSc1
Hohenlohe-Langenburský	Hohenlohe-Langenburský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
z	z	k7c2
Leiningenu	Leiningen	k1gInSc2
</s>
<s>
Emich	Emich	k1gMnSc1
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
kníže	kníže	k1gMnSc1
z	z	k7c2
Leiningenu	Leiningen	k1gInSc2
</s>
<s>
Kristýna	Kristýna	k1gFnSc1
ze	z	k7c2
Solms-Rödelheim	Solms-Rödelheima	k1gFnPc2
</s>
<s>
Feodora	Feodora	k1gFnSc1
Leiningenská	Leiningenský	k2eAgFnSc1d1
</s>
<s>
František	František	k1gMnSc1
Sasko-Kobursko-Saalfeldský	Sasko-Kobursko-Saalfeldský	k2eAgMnSc1d1
</s>
<s>
Viktorie	Viktorie	k1gFnSc1
Sasko-Kobursko-Saalfeldská	Sasko-Kobursko-Saalfeldský	k2eAgFnSc1d1
</s>
<s>
Augusta	Augusta	k1gMnSc1
Reuss	Reussa	k1gFnPc2
Ebersdorf	Ebersdorf	k1gMnSc1
</s>
<s>
Elisa	Elisa	k1gFnSc1
Hohenlohe-Langenburská	Hohenlohe-Langenburský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Bádensko-Durlašský	Bádensko-Durlašský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Bádenský	bádenský	k2eAgMnSc1d1
</s>
<s>
Amálie	Amálie	k1gFnSc1
Nasavsko-Dietzská	Nasavsko-Dietzský	k2eAgFnSc1d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Bádenský	bádenský	k2eAgMnSc1d1
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Geyer	Geyra	k1gFnPc2
von	von	k1gInSc1
Geyersberg	Geyersberg	k1gMnSc1
</s>
<s>
Luisa	Luisa	k1gFnSc1
Karolina	Karolinum	k1gNnSc2
z	z	k7c2
Hochbergu	Hochberg	k1gInSc2
</s>
<s>
Maximiliana	Maximiliana	k1gFnSc1
von	von	k1gInSc1
Sponeck	Sponeck	k1gInSc1
</s>
<s>
Leopoldina	Leopoldin	k2eAgFnSc1d1
Bádenská	bádenský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc5d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Bedřiška	Bedřiška	k1gFnSc1
Braniborsko-Schwedtská	Braniborsko-Schwedtský	k2eAgFnSc1d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Alexandra	Alexandra	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Kristián	Kristián	k1gMnSc1
Nasavsko-Weilburský	Nasavsko-Weilburský	k2eAgMnSc1d1
</s>
<s>
Henrietta	Henrietta	k1gFnSc1
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Oranžsko-Nasavská	Oranžsko-Nasavský	k2eAgFnSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Princess	Princessa	k1gFnPc2
Elise	elise	k1gFnSc2
of	of	k?
Hohenlohe-Langenburg	Hohenlohe-Langenburg	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Elisa	Elis	k1gMnSc2
z	z	k7c2
Hohenlohe-Langenburgu	Hohenlohe-Langenburg	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kněžna	kněžna	k1gFnSc1
z	z	k7c2
Reussu	Reuss	k1gInSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Anežka	Anežka	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
</s>
<s>
1913	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
Elisa	Elisa	k1gFnSc1
Hohenlohe-Langenburská	Hohenlohe-Langenburský	k2eAgFnSc1d1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Knížectví	knížectví	k1gNnSc1
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
3717147373413541580003	#num#	k4
</s>
