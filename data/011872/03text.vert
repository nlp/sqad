<p>
<s>
Cordell	Cordell	k1gMnSc1	Cordell
Hull	Hull	k1gMnSc1	Hull
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1871	[number]	k4	1871
Olympus	Olympus	k1gInSc1	Olympus
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1955	[number]	k4	1955
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gMnSc1	D.C.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejdéle	dlouho	k6eAd3	dlouho
sloužícím	sloužící	k2eAgMnSc7d1	sloužící
ministrem	ministr	k1gMnSc7	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
USA	USA	kA	USA
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
;	;	kIx,	;
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
11	[number]	k4	11
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Franklina	Franklin	k2eAgFnSc1d1	Franklina
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc4	Roosevelt
během	během	k7c2	během
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
OSN	OSN	kA	OSN
<g/>
;	;	kIx,	;
prezident	prezident	k1gMnSc1	prezident
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
ho	on	k3xPp3gMnSc4	on
nazýval	nazývat	k5eAaImAgMnS	nazývat
otcem	otec	k1gMnSc7	otec
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cordell	Cordell	k1gMnSc1	Cordell
Hull	Hull	k1gMnSc1	Hull
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cordell	Cordella	k1gFnPc2	Cordella
Hull	Hullum	k1gNnPc2	Hullum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Cordell	Cordell	k1gMnSc1	Cordell
Hull	Hull	k1gMnSc1	Hull
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
The	The	k1gMnSc1	The
Cordell	Cordell	k1gMnSc1	Cordell
Hull	Hull	k1gMnSc1	Hull
Foundation	Foundation	k1gInSc4	Foundation
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
The	The	k1gMnSc1	The
Cordell	Cordell	k1gMnSc1	Cordell
Hull	Hull	k1gMnSc1	Hull
Institute	institut	k1gInSc5	institut
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
The	The	k1gMnSc1	The
Cordell	Cordell	k1gMnSc1	Cordell
Hull	Hull	k1gMnSc1	Hull
Museum	museum	k1gNnSc4	museum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Cordell	Cordell	k1gMnSc1	Cordell
Hull	Hull	k1gMnSc1	Hull
State	status	k1gInSc5	status
Park	park	k1gInSc1	park
</s>
</p>
