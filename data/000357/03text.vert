<s>
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8	[number]	k4	8
(	(	kIx(	(
<g/>
kódové	kódový	k2eAgNnSc1d1	kódové
označení	označení	k1gNnSc1	označení
Apollo	Apollo	k1gMnSc1	Apollo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
druhé	druhý	k4xOgFnSc2	druhý
generace	generace	k1gFnSc2	generace
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
řady	řada	k1gFnSc2	řada
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
7	[number]	k4	7
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
rozhraní	rozhraní	k1gNnSc1	rozhraní
Metro	metro	k1gNnSc1	metro
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Modern	Moderna	k1gFnPc2	Moderna
UI	UI	kA	UI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8	[number]	k4	8
již	již	k6eAd1	již
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozí	předchozí	k2eAgFnSc2d1	předchozí
generace	generace	k1gFnSc2	generace
nepoužívá	používat	k5eNaImIp3nS	používat
architekturu	architektura	k1gFnSc4	architektura
Windows	Windows	kA	Windows
CE	CE	kA	CE
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přizpůsobené	přizpůsobený	k2eAgNnSc1d1	přizpůsobené
jádro	jádro	k1gNnSc1	jádro
ze	z	k7c2	z
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
další	další	k2eAgInPc1d1	další
Windows	Windows	kA	Windows
8	[number]	k4	8
(	(	kIx(	(
<g/>
a	a	k8xC	a
starší	starý	k2eAgInPc1d2	starší
systémy	systém	k1gInPc1	systém
stejné	stejný	k2eAgFnSc2d1	stejná
řady	řada	k1gFnSc2	řada
NT	NT	kA	NT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
s	s	k7c7	s
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
7	[number]	k4	7
nejsou	být	k5eNaImIp3nP	být
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
s	s	k7c7	s
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8	[number]	k4	8
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
existuje	existovat	k5eAaImIp3nS	existovat
aktualizace	aktualizace	k1gFnSc1	aktualizace
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
7.8	[number]	k4	7.8
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přináší	přinášet	k5eAaImIp3nS	přinášet
podobně	podobně	k6eAd1	podobně
vypadající	vypadající	k2eAgNnSc1d1	vypadající
rozhraní	rozhraní	k1gNnSc1	rozhraní
Metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgFnPc4d1	možná
aplikace	aplikace	k1gFnPc4	aplikace
z	z	k7c2	z
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8	[number]	k4	8
používat	používat	k5eAaImF	používat
na	na	k7c4	na
starší	starý	k2eAgFnSc4d2	starší
verzi	verze	k1gFnSc4	verze
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8	[number]	k4	8
zařízení	zařízení	k1gNnSc2	zařízení
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
Nokia	Nokia	kA	Nokia
<g/>
,	,	kIx,	,
HTC	HTC	kA	HTC
<g/>
,	,	kIx,	,
Samsung	Samsung	kA	Samsung
<g/>
,	,	kIx,	,
Huawei	Huawei	k1gNnSc1	Huawei
<g/>
,	,	kIx,	,
Acer	Acer	k1gInSc1	Acer
<g/>
,	,	kIx,	,
LG	LG	kA	LG
<g/>
,	,	kIx,	,
ZTE	ZTE	kA	ZTE
<g/>
,	,	kIx,	,
Fujitsu	Fujitsa	k1gFnSc4	Fujitsa
a	a	k8xC	a
Dell	Dell	kA	Dell
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
10	[number]	k4	10
Podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
MicroSD	MicroSD	k1gMnSc4	MicroSD
karty	karta	k1gFnSc2	karta
Podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
1280	[number]	k4	1280
<g/>
×	×	k?	×
<g/>
720	[number]	k4	720
a	a	k8xC	a
1280	[number]	k4	1280
<g/>
×	×	k?	×
<g/>
768	[number]	k4	768
Podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
více-jádrové	víceádrový	k2eAgInPc4d1	více-jádrový
procesory	procesor	k1gInPc4	procesor
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
do	do	k7c2	do
64	[number]	k4	64
<g/>
)	)	kIx)	)
Multitasking	multitasking	k1gInSc1	multitasking
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
Podpora	podpora	k1gFnSc1	podpora
nativního	nativní	k2eAgInSc2d1	nativní
kódu	kód	k1gInSc2	kód
(	(	kIx(	(
<g/>
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zjednodušené	zjednodušený	k2eAgNnSc4d1	zjednodušené
portování	portování	k1gNnSc4	portování
aplikací	aplikace	k1gFnPc2	aplikace
z	z	k7c2	z
platforem	platforma	k1gFnPc2	platforma
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Android	android	k1gInSc4	android
<g/>
,	,	kIx,	,
Symbian	Symbian	k1gInSc4	Symbian
a	a	k8xC	a
iOS	iOS	k?	iOS
<g/>
.	.	kIx.	.
</s>
<s>
Kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
s	s	k7c7	s
aplikacemi	aplikace	k1gFnPc7	aplikace
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
8	[number]	k4	8
Nákupy	nákup	k1gInPc7	nákup
z	z	k7c2	z
aplikací	aplikace	k1gFnPc2	aplikace
Kódové	kódový	k2eAgFnSc2d1	kódová
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
aktualizaci	aktualizace	k1gFnSc4	aktualizace
OS	OS	kA	OS
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
<g/>
.	.	kIx.	.
</s>
<s>
GDR2	GDR2	k4	GDR2
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
telefony	telefon	k1gInPc4	telefon
s	s	k7c7	s
OS	OS	kA	OS
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8	[number]	k4	8
<g/>
)	)	kIx)	)
Přidání	přidání	k1gNnSc1	přidání
softwarové	softwarový	k2eAgFnSc2d1	softwarová
možnosti	možnost	k1gFnSc2	možnost
otevřít	otevřít	k5eAaPmF	otevřít
rádio	rádio	k1gNnSc4	rádio
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
tento	tento	k3xDgInSc1	tento
prvek	prvek	k1gInSc1	prvek
chyběl	chybět	k5eAaImAgInS	chybět
<g/>
;	;	kIx,	;
Úprava	úprava	k1gFnSc1	úprava
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
automatické	automatický	k2eAgNnSc1d1	automatické
a	a	k8xC	a
častější	častý	k2eAgNnSc1d2	častější
mazání	mazání	k1gNnSc1	mazání
dočasných	dočasný	k2eAgInPc2d1	dočasný
souborů	soubor	k1gInPc2	soubor
<g/>
;	;	kIx,	;
Data	datum	k1gNnSc2	datum
Sense-	Sense-	k1gFnSc2	Sense-
aplikace	aplikace	k1gFnSc2	aplikace
od	od	k7c2	od
Microsoftu	Microsoft	k1gInSc2	Microsoft
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
přenesená	přenesený	k2eAgNnPc4d1	přenesené
data	datum	k1gNnPc4	datum
přes	přes	k7c4	přes
Wi-Fi	Wi-Fe	k1gFnSc4	Wi-Fe
a	a	k8xC	a
Mobilní	mobilní	k2eAgFnSc4d1	mobilní
síť	síť	k1gFnSc4	síť
<g/>
;	;	kIx,	;
Drobné	drobný	k2eAgFnPc4d1	drobná
úpravy	úprava	k1gFnPc4	úprava
v	v	k7c6	v
Internetu	Internet	k1gInSc6	Internet
Exploreru	Explorer	k1gInSc2	Explorer
<g/>
.	.	kIx.	.
</s>
<s>
Amber	ambra	k1gFnPc2	ambra
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
telefony	telefon	k1gInPc4	telefon
Lumia	Lumium	k1gNnSc2	Lumium
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Možnost	možnost	k1gFnSc4	možnost
probudit	probudit	k5eAaPmF	probudit
telefon	telefon	k1gInSc4	telefon
pomocí	pomocí	k7c2	pomocí
poklepání	poklepání	k1gNnSc2	poklepání
na	na	k7c4	na
obrazovku	obrazovka	k1gFnSc4	obrazovka
dvěma	dva	k4xCgInPc7	dva
prsty	prst	k1gInPc7	prst
<g/>
;	;	kIx,	;
Nové	Nové	k2eAgFnSc2d1	Nové
tapety	tapeta	k1gFnSc2	tapeta
a	a	k8xC	a
úprava	úprava	k1gFnSc1	úprava
Nokia	Nokia	kA	Nokia
Tune	Tun	k1gFnPc1	Tun
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
SMS	SMS	kA	SMS
a	a	k8xC	a
Nokia	Nokia	kA	Nokia
Clock	Clock	k1gMnSc1	Clock
<g/>
;	;	kIx,	;
Flip	Flip	k1gMnSc1	Flip
to	ten	k3xDgNnSc4	ten
silence	silenka	k1gFnSc3	silenka
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
otočení	otočení	k1gNnSc1	otočení
displeje	displej	k1gInSc2	displej
způsobí	způsobit	k5eAaPmIp3nS	způsobit
ztišení	ztišení	k1gNnSc1	ztišení
buzení	buzení	k1gNnSc2	buzení
nebo	nebo	k8xC	nebo
příchozího	příchozí	k1gMnSc2	příchozí
<g />
.	.	kIx.	.
</s>
<s>
hovoru	hovor	k1gInSc2	hovor
<g/>
;	;	kIx,	;
Možnost	možnost	k1gFnSc1	možnost
uživatelsky	uživatelsky	k6eAd1	uživatelsky
upravit	upravit	k5eAaPmF	upravit
displej	displej	k1gInSc1	displej
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
barevného	barevný	k2eAgNnSc2d1	barevné
podání	podání	k1gNnSc2	podání
<g/>
,	,	kIx,	,
saturace	saturace	k1gFnSc2	saturace
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
barev	barva	k1gFnPc2	barva
<g/>
;	;	kIx,	;
Možnost	možnost	k1gFnSc1	možnost
na	na	k7c6	na
zhasnutém	zhasnutý	k2eAgInSc6d1	zhasnutý
displeji	displej	k1gInSc6	displej
zobrazit	zobrazit	k5eAaPmF	zobrazit
pouze	pouze	k6eAd1	pouze
hodiny	hodina	k1gFnPc4	hodina
<g/>
;	;	kIx,	;
Přidání	přidání	k1gNnSc1	přidání
systémové	systémový	k2eAgFnSc2d1	systémová
aplikace	aplikace	k1gFnSc2	aplikace
Filtr	filtr	k1gInSc4	filtr
hovorů	hovor	k1gInPc2	hovor
<g/>
+	+	kIx~	+
<g/>
SMS	SMS	kA	SMS
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
lze	lze	k6eAd1	lze
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
z	z	k7c2	z
názvu	název	k1gInSc2	název
filtrovat	filtrovat	k5eAaImF	filtrovat
neboli	neboli	k8xC	neboli
blokovat	blokovat	k5eAaImF	blokovat
nechtěná	chtěný	k2eNgNnPc4d1	nechtěné
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Kódové	kódový	k2eAgNnSc4d1	kódové
označení	označení	k1gNnSc4	označení
třetí	třetí	k4xOgFnSc2	třetí
aktualizace	aktualizace	k1gFnSc2	aktualizace
pro	pro	k7c4	pro
telefony	telefon	k1gInPc4	telefon
s	s	k7c7	s
platformou	platforma	k1gFnSc7	platforma
Windows	Windows	kA	Windows
Phone	Phon	k1gMnSc5	Phon
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
aktualizace	aktualizace	k1gFnSc1	aktualizace
přinese	přinést	k5eAaPmIp3nS	přinést
<g/>
:	:	kIx,	:
Podpora	podpora	k1gFnSc1	podpora
Full	Full	k1gMnSc1	Full
HD	HD	kA	HD
displejů	displej	k1gInPc2	displej
(	(	kIx(	(
<g/>
1080	[number]	k4	1080
<g/>
×	×	k?	×
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
Podpora	podpora	k1gFnSc1	podpora
výkonnějších	výkonný	k2eAgInPc2d2	výkonnější
procesorů	procesor	k1gInPc2	procesor
(	(	kIx(	(
<g/>
čtyřjádrový	čtyřjádrový	k2eAgInSc1d1	čtyřjádrový
<g/>
,	,	kIx,	,
Snapdragon	Snapdragon	k1gInSc1	Snapdragon
800	[number]	k4	800
<g/>
)	)	kIx)	)
Podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
telefony	telefon	k1gInPc4	telefon
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
obrazovkou	obrazovka	k1gFnSc7	obrazovka
(	(	kIx(	(
<g/>
tudíž	tudíž	k8xC	tudíž
možnost	možnost	k1gFnSc4	možnost
mít	mít	k5eAaImF	mít
až	až	k9	až
3	[number]	k4	3
střední	střední	k2eAgFnSc2d1	střední
dlaždice	dlaždice	k1gFnSc2	dlaždice
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
Lepší	lepšit	k5eAaImIp3nP	lepšit
ovládání	ovládání	k1gNnSc4	ovládání
pro	pro	k7c4	pro
zrakově	zrakově	k6eAd1	zrakově
postižené	postižený	k2eAgNnSc1d1	postižené
(	(	kIx(	(
<g/>
předčítání	předčítání	k1gNnSc1	předčítání
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
Screen	Screen	k2eAgInSc4d1	Screen
Reader	Reader	k1gInSc4	Reader
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
Microsoft	Microsoft	kA	Microsoft
Zira	Zira	k1gFnSc1	Zira
<g/>
)	)	kIx)	)
Úpravu	úprava	k1gFnSc4	úprava
multitaskingu	multitasking	k1gInSc2	multitasking
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
přibude	přibýt	k5eAaPmIp3nS	přibýt
zobrazení	zobrazení	k1gNnSc1	zobrazení
ikony	ikona	k1gFnSc2	ikona
dané	daný	k2eAgFnSc2d1	daná
aplikace	aplikace	k1gFnSc2	aplikace
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
ukončení	ukončení	k1gNnSc2	ukončení
aplikace	aplikace	k1gFnSc2	aplikace
z	z	k7c2	z
multitaskingu	multitasking	k1gInSc2	multitasking
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgFnSc4d2	lepší
správu	správa	k1gFnSc4	správa
úložiště	úložiště	k1gNnSc2	úložiště
A	a	k8xC	a
další	další	k2eAgFnSc2d1	další
opravy	oprava	k1gFnSc2	oprava
Uživatelé	uživatel	k1gMnPc1	uživatel
mohou	moct	k5eAaImIp3nP	moct
hlasovat	hlasovat	k5eAaImF	hlasovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
chybějící	chybějící	k2eAgFnPc4d1	chybějící
schopnosti	schopnost	k1gFnPc4	schopnost
by	by	kYmCp3nS	by
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
systému	systém	k1gInSc2	systém
měla	mít	k5eAaImAgFnS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
8	[number]	k4	8
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
