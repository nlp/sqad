<s>
Jedenáct	jedenáct	k4xCc1	jedenáct
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
portugalsky	portugalsky	k6eAd1	portugalsky
Onze	Onze	k1gNnSc1	Onze
minutos	minutosa	k1gFnPc2	minutosa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
brazilského	brazilský	k2eAgMnSc2d1	brazilský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Paula	Paul	k1gMnSc2	Paul
Coelha	Coelh	k1gMnSc2	Coelh
vydaný	vydaný	k2eAgInSc1d1	vydaný
poprvé	poprvé	k6eAd1	poprvé
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Editora	editor	k1gMnSc2	editor
Rocco	Rocco	k6eAd1	Rocco
v	v	k7c6	v
Rio	Rio	k1gFnSc6	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
román	román	k1gInSc4	román
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Argo	Argo	k6eAd1	Argo
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Lada	Lada	k1gFnSc1	Lada
Weissová	Weissová	k1gFnSc1	Weissová
<g/>
.	.	kIx.	.
</s>
<s>
Touha	touha	k1gFnSc1	touha
autora	autor	k1gMnSc2	autor
napsat	napsat	k5eAaBmF	napsat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
sexu	sex	k1gInSc6	sex
a	a	k8xC	a
rukopis	rukopis	k1gInSc1	rukopis
životopisu	životopis	k1gInSc2	životopis
jedné	jeden	k4xCgFnSc2	jeden
prostitutky	prostitutka	k1gFnSc2	prostitutka
nechaný	nechaný	k2eAgInSc4d1	nechaný
na	na	k7c6	na
recepci	recepce	k1gFnSc6	recepce
hotelu	hotel	k1gInSc2	hotel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
Paulo	Paula	k1gFnSc5	Paula
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
impulsy	impuls	k1gInPc1	impuls
pro	pro	k7c4	pro
napsání	napsání	k1gNnSc4	napsání
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
jednou	jednou	k6eAd1	jednou
jedna	jeden	k4xCgFnSc1	jeden
prostitutka	prostitutka	k1gFnSc1	prostitutka
jménem	jméno	k1gNnSc7	jméno
Mária	Márium	k1gNnSc2	Márium
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
tak	tak	k9	tak
zní	znět	k5eAaImIp3nS	znět
první	první	k4xOgFnSc1	první
věta	věta	k1gFnSc1	věta
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrdinkou	hrdinka	k1gFnSc7	hrdinka
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
mladá	mladý	k2eAgFnSc1d1	mladá
prostitutka	prostitutka	k1gFnSc1	prostitutka
Mária	Mária	k1gFnSc1	Mária
<g/>
.	.	kIx.	.
</s>
<s>
Mária	Mária	k1gFnSc1	Mária
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
a	a	k8xC	a
švadleny	švadlena	k1gFnPc1	švadlena
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
brazilském	brazilský	k2eAgNnSc6d1	brazilské
městečku	městečko	k1gNnSc6	městečko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devatenácti	devatenáct	k4xCc6	devatenáct
letech	let	k1gInPc6	let
dostudovala	dostudovat	k5eAaPmAgFnS	dostudovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
s	s	k7c7	s
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
její	její	k3xOp3gMnSc1	její
vedoucí	vedoucí	k1gMnSc1	vedoucí
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
city	city	k1gFnSc1	city
neopětovala	opětovat	k5eNaImAgFnS	opětovat
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
roky	rok	k1gInPc1	rok
si	se	k3xPyFc3	se
vydělávala	vydělávat	k5eAaImAgFnS	vydělávat
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
splnila	splnit	k5eAaPmAgFnS	splnit
sen	sen	k1gInSc4	sen
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
města	město	k1gNnSc2	město
Rio	Rio	k1gMnSc2	Rio
de	de	k?	de
Janeira	Janeir	k1gMnSc2	Janeir
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
cizincem	cizinec	k1gMnSc7	cizinec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
tanečnicí	tanečnice	k1gFnSc7	tanečnice
samby	samba	k1gFnSc2	samba
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
jednodenním	jednodenní	k2eAgNnSc6d1	jednodenní
rozmýšlení	rozmýšlení	k1gNnSc6	rozmýšlení
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Obstarala	obstarat	k5eAaPmAgFnS	obstarat
si	se	k3xPyFc3	se
roční	roční	k2eAgFnSc1d1	roční
pracovní	pracovní	k2eAgNnSc4d1	pracovní
povolení	povolení	k1gNnSc4	povolení
a	a	k8xC	a
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
ale	ale	k8xC	ale
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
zaměstnavateli	zaměstnavatel	k1gMnSc3	zaměstnavatel
zaplatit	zaplatit	k5eAaPmF	zaplatit
všechny	všechen	k3xTgFnPc4	všechen
výlohy	výloha	k1gFnPc4	výloha
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
přijetím	přijetí	k1gNnSc7	přijetí
(	(	kIx(	(
<g/>
letenka	letenka	k1gFnSc1	letenka
<g/>
,	,	kIx,	,
nákladné	nákladný	k2eAgInPc1d1	nákladný
šaty	šat	k1gInPc1	šat
<g/>
,	,	kIx,	,
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
ubytování	ubytování	k1gNnSc1	ubytování
<g/>
,	,	kIx,	,
strava	strava	k1gFnSc1	strava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
ze	z	k7c2	z
slíbených	slíbený	k2eAgInPc2d1	slíbený
500	[number]	k4	500
franků	frank	k1gInPc2	frank
na	na	k7c4	na
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
po	po	k7c6	po
odečtení	odečtení	k1gNnSc6	odečtení
výloh	výloha	k1gFnPc2	výloha
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
50	[number]	k4	50
franků	frank	k1gInPc2	frank
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svých	svůj	k3xOyFgInPc2	svůj
výpočtů	výpočet	k1gInPc2	výpočet
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
pracovat	pracovat	k5eAaImF	pracovat
nejméně	málo	k6eAd3	málo
rok	rok	k1gInSc4	rok
aby	aby	kYmCp3nS	aby
vše	všechen	k3xTgNnSc1	všechen
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
spořit	spořit	k5eAaImF	spořit
na	na	k7c4	na
zpáteční	zpáteční	k2eAgFnSc4d1	zpáteční
letenku	letenka	k1gFnSc4	letenka
<g/>
.	.	kIx.	.
</s>
<s>
Mária	Mária	k1gFnSc1	Mária
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
učit	učit	k5eAaImF	učit
francouzštinu	francouzština	k1gFnSc4	francouzština
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dokázala	dokázat	k5eAaPmAgFnS	dokázat
obhajovat	obhajovat	k5eAaImF	obhajovat
<g/>
,	,	kIx,	,
šla	jít	k5eAaImAgFnS	jít
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
vedoucím	vedoucí	k1gMnSc7	vedoucí
a	a	k8xC	a
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
pohrozila	pohrozit	k5eAaPmAgFnS	pohrozit
policejním	policejní	k2eAgNnSc7d1	policejní
stíháním	stíhání	k1gNnSc7	stíhání
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
jí	jíst	k5eAaImIp3nS	jíst
výpověď	výpověď	k1gFnSc4	výpověď
a	a	k8xC	a
odstupné	odstupné	k1gNnSc4	odstupné
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ale	ale	k9	ale
vrátila	vrátit	k5eAaPmAgFnS	vrátit
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
dát	dát	k5eAaPmF	dát
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
modelky	modelka	k1gFnSc2	modelka
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
hledání	hledání	k1gNnSc2	hledání
práce	práce	k1gFnSc2	práce
ale	ale	k8xC	ale
žádnou	žádný	k3yNgFnSc4	žádný
nenašla	najít	k5eNaPmAgFnS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
ale	ale	k9	ale
ozvala	ozvat	k5eAaPmAgFnS	ozvat
agentura	agentura	k1gFnSc1	agentura
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
módní	módní	k2eAgFnSc6d1	módní
přehlídce	přehlídka	k1gFnSc6	přehlídka
pořádané	pořádaný	k2eAgFnSc6d1	pořádaná
arabským	arabský	k2eAgMnSc7d1	arabský
šejkem	šejk	k1gMnSc7	šejk
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
sešla	sejít	k5eAaPmAgFnS	sejít
v	v	k7c6	v
luxusní	luxusní	k2eAgFnSc6d1	luxusní
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ale	ale	k8xC	ale
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
nabídky	nabídka	k1gFnSc2	nabídka
na	na	k7c4	na
přehlídku	přehlídka	k1gFnSc4	přehlídka
se	se	k3xPyFc4	se
vyklubala	vyklubat	k5eAaPmAgFnS	vyklubat
prostituce	prostituce	k1gFnSc1	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nebudu	být	k5eNaImBp1nS	být
moct	moct	k5eAaImF	moct
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
si	se	k3xPyFc3	se
prostitucí	prostituce	k1gFnSc7	prostituce
přivydělávat	přivydělávat	k5eAaImF	přivydělávat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
ženevském	ženevský	k2eAgInSc6d1	ženevský
baru	bar	k1gInSc6	bar
Copacabana	Copacabana	k1gFnSc1	Copacabana
<g/>
.	.	kIx.	.
</s>
<s>
Devadesát	devadesát	k4xCc1	devadesát
dní	den	k1gInPc2	den
před	před	k7c7	před
návratem	návrat	k1gInSc7	návrat
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
potkala	potkat	k5eAaPmAgFnS	potkat
Mária	Mária	k1gFnSc1	Mária
v	v	k7c6	v
kavárně	kavárna	k1gFnSc6	kavárna
malíře	malíř	k1gMnSc2	malíř
portrétů	portrét	k1gInPc2	portrét
a	a	k8xC	a
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
ale	ale	k9	ale
byl	být	k5eAaImAgMnS	být
častým	častý	k2eAgMnSc7d1	častý
návštěvníkem	návštěvník	k1gMnSc7	návštěvník
baru	bar	k1gInSc2	bar
Copacabana	Copacabana	k1gFnSc1	Copacabana
a	a	k8xC	a
Máriu	Márius	k1gMnSc3	Márius
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc1	takový
vztah	vztah	k1gInSc1	vztah
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
před	před	k7c7	před
odletem	odlet	k1gInSc7	odlet
spolu	spolu	k6eAd1	spolu
strávili	strávit	k5eAaPmAgMnP	strávit
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
i	i	k8xC	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Mária	Mária	k1gFnSc1	Mária
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
odjet	odjet	k5eAaPmF	odjet
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
čekala	čekat	k5eAaImAgFnS	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
přijde	přijít	k5eAaPmIp3nS	přijít
<g/>
,	,	kIx,	,
neukázal	ukázat	k5eNaPmAgMnS	ukázat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
letadlo	letadlo	k1gNnSc1	letadlo
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
cestovala	cestovat	k5eAaImAgFnS	cestovat
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
mezipřistání	mezipřistání	k1gNnSc1	mezipřistání
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
čekal	čekat	k5eAaImAgMnS	čekat
<g/>
.	.	kIx.	.
</s>
