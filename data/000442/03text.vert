<s>
Venuše	Venuše	k1gFnSc1	Venuše
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Venus	Venus	k1gInSc1	Venus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
římská	římský	k2eAgFnSc1d1	římská
bohyně	bohyně	k1gFnSc1	bohyně
smyslnosti	smyslnost	k1gFnSc2	smyslnost
<g/>
,	,	kIx,	,
krásy	krása	k1gFnSc2	krása
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
ztotožňována	ztotožňovat	k5eAaImNgFnS	ztotožňovat
s	s	k7c7	s
řeckou	řecký	k2eAgFnSc7d1	řecká
bohyní	bohyně	k1gFnSc7	bohyně
Afroditou	Afrodita	k1gFnSc7	Afrodita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
představovala	představovat	k5eAaImAgFnS	představovat
Venuše	Venuše	k1gFnSc1	Venuše
zrozená	zrozený	k2eAgFnSc1d1	zrozená
z	z	k7c2	z
pěny	pěna	k1gFnSc2	pěna
matku	matka	k1gFnSc4	matka
Aenea	Aeneas	k1gMnSc2	Aeneas
<g/>
,	,	kIx,	,
bájného	bájný	k2eAgMnSc2d1	bájný
zakladatele	zakladatel	k1gMnSc2	zakladatel
římského	římský	k2eAgInSc2d1	římský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
římské	římský	k2eAgFnSc2d1	římská
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
<g/>
)	)	kIx)	)
mytologie	mytologie	k1gFnSc2	mytologie
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
velkou	velký	k2eAgFnSc4d1	velká
řadu	řada	k1gFnSc4	řada
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
bohyně	bohyně	k1gFnSc1	bohyně
Venuše	Venuše	k1gFnSc2	Venuše
uctívána	uctíván	k2eAgFnSc1d1	uctívána
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
římského	římský	k2eAgInSc2d1	římský
rekonstrukcionalismu	rekonstrukcionalismus	k1gInSc2	rekonstrukcionalismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Římská	římský	k2eAgFnSc1d1	římská
bohyně	bohyně	k1gFnSc1	bohyně
krásy	krása	k1gFnSc2	krása
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
Venuše	Venuše	k1gFnSc2	Venuše
-	-	kIx~	-
její	její	k3xOp3gInSc4	její
původ	původ	k1gInSc4	původ
je	být	k5eAaImIp3nS	být
odvozován	odvozovat	k5eAaImNgMnS	odvozovat
od	od	k7c2	od
staroitalské	staroitalský	k2eAgFnSc2d1	staroitalská
bohyně	bohyně	k1gFnSc2	bohyně
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
oživení	oživení	k1gNnSc2	oživení
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
proměnu	proměna	k1gFnSc4	proměna
v	v	k7c6	v
bohyni	bohyně	k1gFnSc6	bohyně
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
měl	mít	k5eAaImAgInS	mít
nejspíše	nejspíše	k9	nejspíše
vliv	vliv	k1gInSc1	vliv
řecký	řecký	k2eAgInSc4d1	řecký
kult	kult	k1gInSc4	kult
Afrodíty	Afrodíta	k1gFnSc2	Afrodíta
provozovaný	provozovaný	k2eAgInSc4d1	provozovaný
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
přesně	přesně	k6eAd1	přesně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
proměně	proměna	k1gFnSc3	proměna
z	z	k7c2	z
bohyně	bohyně	k1gFnSc2	bohyně
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
přírody	příroda	k1gFnSc2	příroda
na	na	k7c6	na
bohyni	bohyně	k1gFnSc6	bohyně
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
ani	ani	k8xC	ani
samotným	samotný	k2eAgMnPc3d1	samotný
Římanům	Říman	k1gMnPc3	Říman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
původu	původ	k1gInSc2	původ
Afrodíty	Afrodíta	k1gFnPc1	Afrodíta
nebyly	být	k5eNaImAgFnP	být
řecké	řecký	k2eAgInPc4d1	řecký
mýty	mýtus	k1gInPc4	mýtus
jednotné	jednotný	k2eAgInPc4d1	jednotný
<g/>
,	,	kIx,	,
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
původ	původ	k1gMnSc1	původ
římské	římský	k2eAgFnSc2d1	římská
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
deklarován	deklarovat	k5eAaBmNgInS	deklarovat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
:	:	kIx,	:
jejím	její	k3xOp3gMnSc7	její
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
bůh	bůh	k1gMnSc1	bůh
nebe	nebe	k1gNnSc2	nebe
Caelus	Caelus	k1gMnSc1	Caelus
<g/>
.	.	kIx.	.
</s>
<s>
Caelus	Caelus	k1gInSc1	Caelus
-	-	kIx~	-
řecký	řecký	k2eAgMnSc1d1	řecký
Úranos	Úranos	k1gMnSc1	Úranos
(	(	kIx(	(
<g/>
vládce	vládce	k1gMnSc1	vládce
nad	nad	k7c7	nad
světem	svět	k1gInSc7	svět
po	po	k7c6	po
prvotním	prvotní	k2eAgInSc6d1	prvotní
Chaosu	chaos	k1gInSc6	chaos
<g/>
)	)	kIx)	)
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgMnS	převzít
Římany	Říman	k1gMnPc7	Říman
od	od	k7c2	od
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Římanů	Říman	k1gMnPc2	Říman
byl	být	k5eAaImAgMnS	být
Caelus	Caelus	k1gMnSc1	Caelus
synem	syn	k1gMnSc7	syn
boha	bůh	k1gMnSc2	bůh
Aethera	Aether	k1gMnSc2	Aether
-	-	kIx~	-
podle	podle	k7c2	podle
Řeků	Řek	k1gMnPc2	Řek
byl	být	k5eAaImAgMnS	být
Úranos	Úranos	k1gMnSc1	Úranos
synem	syn	k1gMnSc7	syn
matky	matka	k1gFnSc2	matka
země	země	k1gFnSc1	země
Gaie	Gaie	k1gFnSc1	Gaie
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gNnSc4	on
porodila	porodit	k5eAaPmAgFnS	porodit
sama	sám	k3xTgMnSc4	sám
ze	z	k7c2	z
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
bez	bez	k7c2	bez
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
Caela	Cael	k1gMnSc4	Cael
pokládali	pokládat	k5eAaImAgMnP	pokládat
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
nejen	nejen	k6eAd1	nejen
bohyně	bohyně	k1gFnSc1	bohyně
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
boha	bůh	k1gMnSc4	bůh
Saturn	Saturn	k1gMnSc1	Saturn
(	(	kIx(	(
řeckého	řecký	k2eAgMnSc2d1	řecký
Krona	Kron	k1gMnSc2	Kron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
bohyně	bohyně	k1gFnSc1	bohyně
Afrodíta	Afrodít	k1gInSc2	Afrodít
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
Římané	Říman	k1gMnPc1	Říman
přisvojili	přisvojit	k5eAaPmAgMnP	přisvojit
a	a	k8xC	a
nazvali	nazvat	k5eAaPmAgMnP	nazvat
ji	on	k3xPp3gFnSc4	on
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
maloasijském	maloasijský	k2eAgNnSc6d1	maloasijské
náboženství	náboženství	k1gNnSc6	náboženství
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
syrskofoinické	syrskofoinický	k2eAgFnSc2d1	syrskofoinický
bohyně	bohyně	k1gFnSc2	bohyně
Astarty	Astarta	k1gFnSc2	Astarta
<g/>
.	.	kIx.	.
</s>
<s>
Astarta	Astarta	k1gFnSc1	Astarta
je	být	k5eAaImIp3nS	být
odvozována	odvozovat	k5eAaImNgFnS	odvozovat
od	od	k7c2	od
asyrsko-babylonské	asyrskoabylonský	k2eAgFnSc2d1	asyrsko-babylonský
bohyně	bohyně	k1gFnSc2	bohyně
lásky	láska	k1gFnSc2	láska
Ištar	Ištara	k1gFnPc2	Ištara
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
ji	on	k3xPp3gFnSc4	on
převzali	převzít	k5eAaPmAgMnP	převzít
už	už	k9	už
v	v	k7c6	v
nejstarších	starý	k2eAgFnPc6d3	nejstarší
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
pronikla	proniknout	k5eAaPmAgFnS	proniknout
asi	asi	k9	asi
přes	přes	k7c4	přes
ostrovy	ostrov	k1gInPc4	ostrov
Krétu	Kréta	k1gFnSc4	Kréta
a	a	k8xC	a
Kythéru	Kythéra	k1gFnSc4	Kythéra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc4	její
kult	kult	k1gInSc4	kult
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
jí	on	k3xPp3gFnSc3	on
zasvěceny	zasvěcen	k2eAgFnPc1d1	zasvěcena
myrta	myrta	k1gFnSc1	myrta
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
mák	mák	k1gInSc1	mák
<g/>
,	,	kIx,	,
holubice	holubice	k1gFnSc1	holubice
<g/>
,	,	kIx,	,
delfín	delfín	k1gMnSc1	delfín
<g/>
,	,	kIx,	,
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
a	a	k8xC	a
lípa	lípa	k1gFnSc1	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnSc3	svůj
bohyni	bohyně	k1gFnSc3	bohyně
postavili	postavit	k5eAaPmAgMnP	postavit
Řekové	Řek	k1gMnPc1	Řek
velkolepé	velkolepý	k2eAgInPc1d1	velkolepý
chrámy	chrám	k1gInPc1	chrám
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
Knidu	Knid	k1gInSc6	Knid
<g/>
,	,	kIx,	,
Pafu	Pafus	k1gInSc6	Pafus
<g/>
,	,	kIx,	,
Korinthu	Korinth	k1gInSc6	Korinth
<g/>
,	,	kIx,	,
Alabandě	Alabanda	k1gFnSc6	Alabanda
<g/>
,	,	kIx,	,
Kóu	Kóu	k1gFnSc6	Kóu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řeckých	řecký	k2eAgFnPc2d1	řecká
osad	osada	k1gFnPc2	osada
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
kult	kult	k1gInSc1	kult
Afrodíty	Afrodíta	k1gFnSc2	Afrodíta
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
ztotožněna	ztotožnit	k5eAaPmNgFnS	ztotožnit
se	s	k7c7	s
staroitalskou	staroitalský	k2eAgFnSc7d1	staroitalská
bohyní	bohyně	k1gFnSc7	bohyně
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
Venuše	Venuše	k1gFnSc1	Venuše
uctívána	uctíván	k2eAgFnSc1d1	uctívána
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
Afrodíta	Afrodít	k1gInSc2	Afrodít
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláštní	obzvláštní	k2eAgFnSc3d1	obzvláštní
úctě	úcta	k1gFnSc3	úcta
se	se	k3xPyFc4	se
Venuše	Venuše	k1gFnSc2	Venuše
těšila	těšit	k5eAaImAgFnS	těšit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Caesara	Caesar	k1gMnSc2	Caesar
a	a	k8xC	a
Augusta	August	k1gMnSc2	August
<g/>
.	.	kIx.	.
</s>
<s>
Pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Juliů	Julius	k1gMnPc2	Julius
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
odvozovali	odvozovat	k5eAaImAgMnP	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
od	od	k7c2	od
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
byl	být	k5eAaImAgInS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
Iulus	Iulus	k1gMnSc1	Iulus
neboli	neboli	k8xC	neboli
Ascanius	Ascanius	k1gMnSc1	Ascanius
<g/>
.	.	kIx.	.
</s>
<s>
Ascanius	Ascanius	k1gMnSc1	Ascanius
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
synem	syn	k1gMnSc7	syn
vůdce	vůdce	k1gMnPc4	vůdce
trojských	trojský	k2eAgInPc2d1	trojský
přistěhovalů	přistěhoval	k1gInPc2	přistěhoval
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
Aenea	Aeneas	k1gMnSc2	Aeneas
a	a	k8xC	a
vnukem	vnuk	k1gMnSc7	vnuk
bohyně	bohyně	k1gFnSc2	bohyně
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
dardanského	dardanský	k2eAgMnSc2d1	dardanský
krále	král	k1gMnSc2	král
Anchísa	Anchís	k1gMnSc2	Anchís
<g/>
.	.	kIx.	.
<g/>
Venuše	Venuše	k1gFnSc1	Venuše
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
Juliově	Juliův	k2eAgNnSc6d1	Juliovo
fóru	fórum	k1gNnSc6	fórum
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
svůj	svůj	k3xOyFgInSc4	svůj
chrám	chrám	k1gInSc4	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
dal	dát	k5eAaPmAgMnS	dát
vystavět	vystavět	k5eAaPmF	vystavět
Caesar	Caesar	k1gMnSc1	Caesar
roku	rok	k1gInSc2	rok
48	[number]	k4	48
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jako	jako	k8xS	jako
Venuši	Venuše	k1gFnSc3	Venuše
Roditelce	roditelka	k1gFnSc3	roditelka
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
sloupů	sloup	k1gInPc2	sloup
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
Kolosea	Koloseum	k1gNnSc2	Koloseum
měla	mít	k5eAaImAgFnS	mít
společný	společný	k2eAgInSc4d1	společný
chrám	chrám	k1gInSc4	chrám
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
Romou	Roma	k1gFnSc7	Roma
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
společný	společný	k2eAgInSc1d1	společný
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
dvojchrám	dvojchrat	k5eAaPmIp1nS	dvojchrat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vlastně	vlastně	k9	vlastně
dva	dva	k4xCgInPc1	dva
chrámy	chrám	k1gInPc1	chrám
pod	pod	k7c7	pod
společnou	společný	k2eAgFnSc7d1	společná
střechou	střecha	k1gFnSc7	střecha
byl	být	k5eAaImAgInS	být
největším	veliký	k2eAgInSc7d3	veliký
chrámem	chrám	k1gInSc7	chrám
na	na	k7c6	na
území	území	k1gNnSc6	území
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
míry	míra	k1gFnPc1	míra
byly	být	k5eAaImAgFnP	být
<g/>
:	:	kIx,	:
110	[number]	k4	110
m	m	kA	m
x	x	k?	x
53	[number]	k4	53
m.	m.	k?	m.
Tento	tento	k3xDgInSc4	tento
svatostánek	svatostánek	k1gInSc4	svatostánek
nechal	nechat	k5eAaPmAgMnS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
císař	císař	k1gMnSc1	císař
Hadriánus	Hadriánus	k1gMnSc1	Hadriánus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
121	[number]	k4	121
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
osobně	osobně	k6eAd1	osobně
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gInPc2	jeho
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
je	být	k5eAaImIp3nS	být
zčásti	zčásti	k6eAd1	zčásti
vestavěn	vestavěn	k2eAgInSc1d1	vestavěn
chrám	chrám	k1gInSc1	chrám
Santa	Sant	k1gMnSc2	Sant
Francesca	Francescus	k1gMnSc2	Francescus
Romana	Roman	k1gMnSc2	Roman
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
