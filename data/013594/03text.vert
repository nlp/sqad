<s>
Árpás	Árpás	k6eAd1
</s>
<s>
Árpás	Árpás	k1gInSc1
Kostel	kostel	k1gInSc1
v	v	k7c6
Árpási	Árpáse	k1gFnSc6
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
47	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+1	+1	k4
Stát	stát	k1gInSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
Župa	župa	k1gFnSc1
</s>
<s>
Győr-Moson-Sopron	Győr-Moson-Sopron	k1gInSc1
Okres	okres	k1gInSc1
</s>
<s>
Tét	Tét	k?
Administrativní	administrativní	k2eAgNnSc4d1
dělení	dělení	k1gNnSc4
</s>
<s>
Árpás	Árpás	k1gInSc1
<g/>
,	,	kIx,
Dombiföld	Dombiföld	k1gMnSc1
<g/>
,	,	kIx,
Kálmánpuszta	Kálmánpuszta	k1gMnSc1
</s>
<s>
Árpás	Árpás	k6eAd1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
14,3	14,3	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
273	#num#	k4
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
19,1	19,1	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
Starostka	starostka	k1gFnSc1
</s>
<s>
Szabóné	Szabóné	k2eAgInSc1d1
Kovács	Kovács	k1gInSc1
Alice	Alice	k1gFnSc2
Mária	Márium	k1gNnSc2
Vznik	vznik	k1gInSc1
</s>
<s>
1030	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.arpas.hu	www.arpas.ha	k1gFnSc4
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
96	#num#	k4
PSČ	PSČ	kA
</s>
<s>
9132	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Árpás	Árpás	k1gInSc1
(	(	kIx(
<g/>
vyslovováno	vyslovován	k2eAgNnSc1d1
[	[	kIx(
<g/>
árpáš	árpáš	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
malá	malý	k2eAgFnSc1d1
vesnička	vesnička	k1gFnSc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
v	v	k7c6
župě	župa	k1gFnSc6
Győr-Moson-Sopron	Győr-Moson-Sopron	k1gMnSc1
<g/>
,	,	kIx,
spadající	spadající	k2eAgMnSc1d1
pod	pod	k7c4
okres	okres	k1gInSc4
Tét	Tét	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
asi	asi	k9
8	#num#	k4
km	km	kA
západně	západně	k6eAd1
od	od	k7c2
Tétu	Tétus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
273	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
zde	zde	k6eAd1
žili	žít	k5eAaImAgMnP
96,5	96,5	k4
%	%	kIx~
Maďaři	Maďar	k1gMnPc1
<g/>
,	,	kIx,
1,9	1,9	k4
%	%	kIx~
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
0,4	0,4	k4
%	%	kIx~
Romové	Rom	k1gMnPc1
a	a	k8xC
0,4	0,4	k4
%	%	kIx~
Rumuni	Rumun	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Vesnice	vesnice	k1gFnSc1
leží	ležet	k5eAaImIp3nS
u	u	k7c2
řeky	řeka	k1gFnSc2
Ráby	Ráb	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sousedními	sousední	k2eAgFnPc7d1
vesnicemi	vesnice	k1gFnPc7
jsou	být	k5eAaImIp3nP
Egyed	Egyed	k1gInSc4
<g/>
,	,	kIx,
Mórichida	Mórichida	k1gFnSc1
a	a	k8xC
Rábapordány	Rábapordán	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Árpás	Árpása	k1gFnPc2
na	na	k7c6
maďarské	maďarský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Magyarország	Magyarország	k1gMnSc1
közigazgatási	közigazgatáse	k1gFnSc4
helynévkönyve	helynévkönyev	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
január	január	k1gInSc1
1	#num#	k4
<g/>
..	..	k?
3	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Árpás	Árpása	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okres	okres	k1gInSc1
Tét	Tét	k1gFnSc2
</s>
<s>
Árpás	Árpás	k1gInSc1
•	•	k?
Csikvánd	Csikvánd	k1gInSc1
•	•	k?
Felpéc	Felpéc	k1gInSc1
•	•	k?
Gyarmat	Gyarmat	k1gInSc1
•	•	k?
Gyömöre	Gyömör	k1gInSc5
•	•	k?
Győrszemere	Győrszemer	k1gInSc5
•	•	k?
Kisbabot	Kisbabot	k1gMnSc1
•	•	k?
Mérges	Mérges	k1gMnSc1
•	•	k?
Mórichida	Mórichida	k1gFnSc1
•	•	k?
Rábacsécsény	Rábacsécséna	k1gFnSc2
•	•	k?
Rábaszentmihály	Rábaszentmihála	k1gFnSc2
•	•	k?
Rábaszentmiklós	Rábaszentmiklós	k1gInSc1
•	•	k?
Szerecseny	Szerecsen	k2eAgMnPc4d1
•	•	k?
Tét	Tét	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Maďarsko	Maďarsko	k1gNnSc1
</s>
