<s>
Gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
"	"	kIx"	"
<g/>
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
s	s	k7c7	s
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
letopočtem	letopočet	k1gInSc7	letopočet
a	a	k8xC	a
s	s	k7c7	s
gregoriánskou	gregoriánský	k2eAgFnSc7d1	gregoriánská
korekcí	korekce	k1gFnSc7	korekce
přestupnosti	přestupnost	k1gFnSc2	přestupnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
celosvětově	celosvětově	k6eAd1	celosvětově
používaný	používaný	k2eAgInSc4d1	používaný
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
počítání	počítání	k1gNnSc4	počítání
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
kalendář	kalendář	k1gInSc1	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
solární	solární	k2eAgInPc4d1	solární
kalendáře	kalendář	k1gInPc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
úpravou	úprava	k1gFnSc7	úprava
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
s	s	k7c7	s
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
letopočtem	letopočet	k1gInSc7	letopočet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1582	[number]	k4	1582
papežem	papež	k1gMnSc7	papež
Řehořem	Řehoř	k1gMnSc7	Řehoř
XIII	XIII	kA	XIII
<g/>
..	..	k?	..
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
země	zem	k1gFnPc1	zem
jej	on	k3xPp3gNnSc2	on
zaváděly	zavádět	k5eAaImAgFnP	zavádět
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
korekce	korekce	k1gFnSc1	korekce
přijata	přijmout	k5eAaPmNgFnS	přijmout
až	až	k9	až
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
juliánskému	juliánský	k2eAgInSc3d1	juliánský
kalendáři	kalendář	k1gInSc3	kalendář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
opravil	opravit	k5eAaPmAgMnS	opravit
<g/>
,	,	kIx,	,
významně	významně	k6eAd1	významně
přesnější	přesný	k2eAgMnSc1d2	přesnější
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
délce	délka	k1gFnSc6	délka
roku	rok	k1gInSc2	rok
-	-	kIx~	-
zatímco	zatímco	k8xS	zatímco
juliánský	juliánský	k2eAgInSc1d1	juliánský
kalendář	kalendář	k1gInSc1	kalendář
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
astronomické	astronomický	k2eAgFnSc3d1	astronomická
skutečnosti	skutečnost	k1gFnSc3	skutečnost
(	(	kIx(	(
<g/>
tropickému	tropický	k2eAgInSc3d1	tropický
roku	rok	k1gInSc3	rok
<g/>
)	)	kIx)	)
odchylku	odchylka	k1gFnSc4	odchylka
1	[number]	k4	1
den	dna	k1gFnPc2	dna
každých	každý	k3xTgInPc2	každý
127	[number]	k4	127
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
tato	tento	k3xDgFnSc1	tento
odchylka	odchylka	k1gFnSc1	odchylka
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
den	den	k1gInSc1	den
přibližně	přibližně	k6eAd1	přibližně
každých	každý	k3xTgNnPc2	každý
3300	[number]	k4	3300
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Reforma	reforma	k1gFnSc1	reforma
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
vkládání	vkládání	k1gNnSc1	vkládání
a	a	k8xC	a
nevkládání	nevkládání	k1gNnSc1	nevkládání
přestupných	přestupný	k2eAgInPc2d1	přestupný
dnů	den	k1gInPc2	den
a	a	k8xC	a
srovnání	srovnání	k1gNnSc4	srovnání
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
nicejského	nicejský	k2eAgInSc2d1	nicejský
koncilu	koncil	k1gInSc2	koncil
resp.	resp.	kA	resp.
Julia	Julius	k1gMnSc2	Julius
Cesara	Cesar	k1gMnSc2	Cesar
<g/>
.	.	kIx.	.
</s>
<s>
Netýkala	týkat	k5eNaImAgFnS	týkat
se	se	k3xPyFc4	se
letopočtu	letopočet	k1gInSc2	letopočet
ani	ani	k8xC	ani
uspořádání	uspořádání	k1gNnSc2	uspořádání
dnů	den	k1gInPc2	den
během	během	k7c2	během
roku	rok	k1gInSc2	rok
a	a	k8xC	a
dělení	dělení	k1gNnSc1	dělení
roku	rok	k1gInSc2	rok
na	na	k7c4	na
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
převzala	převzít	k5eAaPmAgFnS	převzít
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
zásadní	zásadní	k2eAgFnSc4d1	zásadní
přestavbu	přestavba	k1gFnSc4	přestavba
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
o	o	k7c4	o
dílčí	dílčí	k2eAgFnSc4d1	dílčí
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Komplexní	komplexní	k2eAgFnPc4d1	komplexní
racionalizační	racionalizační	k2eAgFnPc4d1	racionalizační
přestavby	přestavba	k1gFnPc4	přestavba
jsou	být	k5eAaImIp3nP	být
tématem	téma	k1gNnSc7	téma
diskusí	diskuse	k1gFnSc7	diskuse
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zkušenosti	zkušenost	k1gFnPc1	zkušenost
s	s	k7c7	s
gregoriánskou	gregoriánský	k2eAgFnSc7d1	gregoriánská
reformou	reforma	k1gFnSc7	reforma
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
poznatku	poznatek	k1gInSc3	poznatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc7d1	hlavní
podmínkou	podmínka	k1gFnSc7	podmínka
úspěchu	úspěch	k1gInSc2	úspěch
případné	případný	k2eAgFnSc2d1	případná
komplexní	komplexní	k2eAgFnSc2d1	komplexní
racionalizace	racionalizace	k1gFnSc2	racionalizace
je	být	k5eAaImIp3nS	být
současné	současný	k2eAgNnSc4d1	současné
provedení	provedení	k1gNnSc4	provedení
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
téměř	téměř	k6eAd1	téměř
celém	celý	k2eAgInSc6d1	celý
<g/>
)	)	kIx)	)
propojeném	propojený	k2eAgInSc6d1	propojený
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
ztratila	ztratit	k5eAaPmAgFnS	ztratit
náprava	náprava	k1gFnSc1	náprava
nepravidelností	nepravidelnost	k1gFnPc2	nepravidelnost
kalendáře	kalendář	k1gInSc2	kalendář
naléhavost	naléhavost	k1gFnSc1	naléhavost
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1	předchůdce
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
fungoval	fungovat	k5eAaImAgInS	fungovat
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
století	století	k1gNnPc2	století
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
Caesarova	Caesarův	k2eAgFnSc1d1	Caesarova
reforma	reforma	k1gFnSc1	reforma
se	se	k3xPyFc4	se
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Tropický	tropický	k2eAgInSc1d1	tropický
rok	rok	k1gInSc1	rok
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
délku	délka	k1gFnSc4	délka
365,242	[number]	k4	365,242
<g/>
2	[number]	k4	2
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
o	o	k7c4	o
11	[number]	k4	11
minut	minuta	k1gFnPc2	minuta
kratší	krátký	k2eAgFnPc1d2	kratší
<g/>
,	,	kIx,	,
než	než	k8xS	než
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
(	(	kIx(	(
<g/>
365,25	[number]	k4	365,25
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
tak	tak	k9	tak
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
opožďování	opožďování	k1gNnSc3	opožďování
kalendáře	kalendář	k1gInSc2	kalendář
vůči	vůči	k7c3	vůči
okamžikům	okamžik	k1gInPc3	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastává	nastávat	k5eAaImIp3nS	nastávat
rovnodennost	rovnodennost	k1gFnSc4	rovnodennost
nebo	nebo	k8xC	nebo
slunovrat	slunovrat	k1gInSc4	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
453	[number]	k4	453
byla	být	k5eAaImAgFnS	být
diference	diference	k1gFnSc1	diference
ještě	ještě	k9	ještě
jen	jen	k9	jen
1	[number]	k4	1
den	dna	k1gFnPc2	dna
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
581	[number]	k4	581
byla	být	k5eAaImAgFnS	být
diference	diference	k1gFnSc1	diference
2	[number]	k4	2
dny	den	k1gInPc4	den
atd.	atd.	kA	atd.
Nesoulad	nesoulad	k1gInSc4	nesoulad
kalendáře	kalendář	k1gInSc2	kalendář
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
tropickým	tropický	k2eAgInSc7d1	tropický
rokem	rok	k1gInSc7	rok
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
období	období	k1gNnSc6	období
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
středověku	středověk	k1gInSc2	středověk
již	již	k6eAd1	již
výrazně	výrazně	k6eAd1	výrazně
patrný	patrný	k2eAgInSc1d1	patrný
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c6	na
zpřesňující	zpřesňující	k2eAgFnSc6d1	zpřesňující
se	se	k3xPyFc4	se
možnosti	možnost	k1gFnSc2	možnost
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
nové	nový	k2eAgFnPc4d1	nová
znalosti	znalost	k1gFnPc4	znalost
učenci	učenec	k1gMnPc1	učenec
docházeli	docházet	k5eAaImAgMnP	docházet
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
chybu	chyba	k1gFnSc4	chyba
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
opravit	opravit	k5eAaPmF	opravit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
reformy	reforma	k1gFnSc2	reforma
1582	[number]	k4	1582
už	už	k9	už
byla	být	k5eAaImAgFnS	být
diference	diference	k1gFnSc1	diference
10	[number]	k4	10
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
9,809	[number]	k4	9,809
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
svátek	svátek	k1gInSc1	svátek
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
Velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
neděle	neděle	k1gFnSc2	neděle
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
nebyla	být	k5eNaImAgFnS	být
tou	ten	k3xDgFnSc7	ten
první	první	k4xOgFnSc7	první
nedělí	dělit	k5eNaImIp3nS	dělit
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
jarním	jarní	k2eAgInSc6d1	jarní
úplňku	úplněk	k1gInSc6	úplněk
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
stanoveno	stanoven	k2eAgNnSc1d1	stanoveno
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
pro	pro	k7c4	pro
římskou	římský	k2eAgFnSc4d1	římská
církev	církev	k1gFnSc4	církev
byl	být	k5eAaImAgInS	být
stav	stav	k1gInSc1	stav
neúnosný	únosný	k2eNgInSc1d1	neúnosný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
papežové	papež	k1gMnPc1	papež
a	a	k8xC	a
koncily	koncil	k1gInPc7	koncil
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
astronomové	astronom	k1gMnPc1	astronom
včetně	včetně	k7c2	včetně
Regiomontana	Regiomontan	k1gMnSc2	Regiomontan
a	a	k8xC	a
Koperníka	Koperník	k1gMnSc2	Koperník
přípravou	příprava	k1gFnSc7	příprava
reformy	reforma	k1gFnSc2	reforma
kalendáře	kalendář	k1gInSc2	kalendář
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc7d2	menší
intenzitou	intenzita	k1gFnSc7	intenzita
zaobírali	zaobírat	k5eAaImAgMnP	zaobírat
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
návrh	návrh	k1gInSc4	návrh
Compendium	Compendium	k1gNnSc4	Compendium
novae	novae	k1gNnSc2	novae
rationis	rationis	k1gFnPc2	rationis
restituendi	restituend	k1gMnPc1	restituend
kalendarium	kalendarium	k1gNnSc1	kalendarium
předložil	předložit	k5eAaPmAgInS	předložit
roku	rok	k1gInSc2	rok
1575	[number]	k4	1575
papeži	papež	k1gMnSc3	papež
Řehoři	Řehoř	k1gMnSc3	Řehoř
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
z	z	k7c2	z
kalabrijského	kalabrijský	k2eAgMnSc2d1	kalabrijský
Cirò	Cirò	k1gMnSc2	Cirò
Luigi	Luigi	k1gNnPc2	Luigi
Giglio	Giglio	k6eAd1	Giglio
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
též	též	k9	též
Aloisius	Aloisius	k1gMnSc1	Aloisius
Lilius	Lilius	k1gMnSc1	Lilius
<g/>
,	,	kIx,	,
1510	[number]	k4	1510
<g/>
–	–	k?	–
<g/>
1576	[number]	k4	1576
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1	Papežská
komise	komise	k1gFnSc1	komise
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
církevních	církevní	k2eAgFnPc2d1	církevní
i	i	k8xC	i
laických	laický	k2eAgMnPc2d1	laický
odborníků	odborník	k1gMnPc2	odborník
po	po	k7c6	po
několikaletém	několikaletý	k2eAgNnSc6d1	několikaleté
zkoumání	zkoumání	k1gNnSc6	zkoumání
alternativních	alternativní	k2eAgNnPc2d1	alternativní
řešení	řešení	k1gNnPc2	řešení
návrh	návrh	k1gInSc1	návrh
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
úpravami	úprava	k1gFnPc7	úprava
schválila	schválit	k5eAaPmAgFnS	schválit
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1582	[number]	k4	1582
Řehoř	Řehoř	k1gMnSc1	Řehoř
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
podle	podle	k7c2	podle
jejího	její	k3xOp3gInSc2	její
návrhu	návrh	k1gInSc2	návrh
vydal	vydat	k5eAaPmAgMnS	vydat
bulu	bula	k1gFnSc4	bula
Inter	Inter	k1gMnSc1	Inter
gravissimas	gravissimas	k1gMnSc1	gravissimas
<g/>
,	,	kIx,	,
vyhlašující	vyhlašující	k2eAgFnSc4d1	vyhlašující
kalendářní	kalendářní	k2eAgFnSc4d1	kalendářní
reformu	reforma	k1gFnSc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
opravy	oprava	k1gFnSc2	oprava
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
jednorázové	jednorázový	k2eAgNnSc4d1	jednorázové
vynechání	vynechání	k1gNnSc4	vynechání
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Alternativou	alternativa	k1gFnSc7	alternativa
bylo	být	k5eAaImAgNnS	být
postupné	postupný	k2eAgNnSc1d1	postupné
vyrovnání	vyrovnání	k1gNnSc1	vyrovnání
neuplatňováním	neuplatňování	k1gNnSc7	neuplatňování
přestupných	přestupný	k2eAgInPc2d1	přestupný
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
požadavkem	požadavek	k1gInSc7	požadavek
bylo	být	k5eAaImAgNnS	být
zachování	zachování	k1gNnSc1	zachování
neporušeného	porušený	k2eNgInSc2d1	neporušený
týdenního	týdenní	k2eAgInSc2d1	týdenní
cyklu	cyklus	k1gInSc2	cyklus
podle	podle	k7c2	podle
biblického	biblický	k2eAgNnSc2d1	biblické
pravidla	pravidlo	k1gNnSc2	pravidlo
o	o	k7c6	o
světění	světění	k1gNnSc6	světění
sedmého	sedmý	k4xOgInSc2	sedmý
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
liturgického	liturgický	k2eAgInSc2d1	liturgický
roku	rok	k1gInSc2	rok
pro	pro	k7c4	pro
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
přeskočených	přeskočený	k2eAgInPc2d1	přeskočený
významných	významný	k2eAgInPc2d1	významný
svátků	svátek	k1gInPc2	svátek
nejmenší	malý	k2eAgFnSc1d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Reforma	reforma	k1gFnSc1	reforma
měla	mít	k5eAaImAgFnS	mít
proto	proto	k8xC	proto
být	být	k5eAaImF	být
provedena	provést	k5eAaPmNgFnS	provést
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
svátku	svátek	k1gInSc6	svátek
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
<g/>
,	,	kIx,	,
připadajícím	připadající	k2eAgMnSc7d1	připadající
na	na	k7c4	na
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
pátek	pátek	k1gInSc4	pátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
svátková	svátkový	k2eAgFnSc1d1	svátková
šetrnost	šetrnost	k1gFnSc1	šetrnost
se	se	k3xPyFc4	se
ale	ale	k9	ale
uplatnila	uplatnit	k5eAaPmAgFnS	uplatnit
jen	jen	k9	jen
v	v	k7c6	v
několika	několik	k4yIc6	několik
málo	málo	k6eAd1	málo
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
reformu	reforma	k1gFnSc4	reforma
přijaly	přijmout	k5eAaPmAgFnP	přijmout
přesně	přesně	k6eAd1	přesně
podle	podle	k7c2	podle
papežské	papežský	k2eAgFnSc2d1	Papežská
buly	bula	k1gFnSc2	bula
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
také	také	k9	také
stanovila	stanovit	k5eAaPmAgFnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc4	každý
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
století	století	k1gNnSc2	století
bude	být	k5eAaImBp3nS	být
přestupný	přestupný	k2eAgInSc1d1	přestupný
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
dělitelný	dělitelný	k2eAgInSc1d1	dělitelný
číslem	číslo	k1gNnSc7	číslo
400	[number]	k4	400
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
chyba	chyba	k1gFnSc1	chyba
neobnoví	obnovit	k5eNaPmIp3nS	obnovit
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
řadu	řada	k1gFnSc4	řada
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
kalendářem	kalendář	k1gInSc7	kalendář
juliánským	juliánský	k2eAgInSc7d1	juliánský
a	a	k8xC	a
gregoriánským	gregoriánský	k2eAgInSc7d1	gregoriánský
13	[number]	k4	13
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
např.	např.	kA	např.
Vánoce	Vánoce	k1gFnPc1	Vánoce
ortodoxního	ortodoxní	k2eAgNnSc2d1	ortodoxní
křesťanství	křesťanství	k1gNnSc2	křesťanství
(	(	kIx(	(
<g/>
převažujícím	převažující	k2eAgFnPc3d1	převažující
např.	např.	kA	např.
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Srbsku	Srbsko	k1gNnSc6	Srbsko
a	a	k8xC	a
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řídícího	řídící	k2eAgMnSc2d1	řídící
se	se	k3xPyFc4	se
juliánským	juliánský	k2eAgInSc7d1	juliánský
kalendářem	kalendář	k1gInSc7	kalendář
<g/>
,	,	kIx,	,
nastávají	nastávat	k5eAaImIp3nP	nastávat
až	až	k9	až
13	[number]	k4	13
dní	den	k1gInPc2	den
po	po	k7c6	po
Vánocích	Vánoce	k1gFnPc6	Vánoce
podle	podle	k7c2	podle
ostatních	ostatní	k2eAgMnPc2d1	ostatní
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
nebyl	být	k5eNaImAgInS	být
všude	všude	k6eAd1	všude
přijat	přijmout	k5eAaPmNgInS	přijmout
ihned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
</s>
<s>
Stanoveného	stanovený	k2eAgInSc2d1	stanovený
dne	den	k1gInSc2	den
byla	být	k5eAaImAgFnS	být
reforma	reforma	k1gFnSc1	reforma
provedena	provést	k5eAaPmNgFnS	provést
jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
katolických	katolický	k2eAgFnPc6d1	katolická
zemích	zem	k1gFnPc6	zem
–	–	k?	–
větší	veliký	k2eAgFnSc3d2	veliký
části	část	k1gFnSc3	část
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
a	a	k8xC	a
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
zavedení	zavedení	k1gNnSc2	zavedení
reformy	reforma	k1gFnSc2	reforma
přinášelo	přinášet	k5eAaImAgNnS	přinášet
veliké	veliký	k2eAgFnPc4d1	veliká
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
prostí	prostý	k2eAgMnPc1d1	prostý
lidé	člověk	k1gMnPc1	člověk
důvodu	důvod	k1gInSc2	důvod
reformy	reforma	k1gFnSc2	reforma
nerozuměli	rozumět	k5eNaImAgMnP	rozumět
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
vypuštěné	vypuštěný	k2eAgFnPc1d1	vypuštěná
dny	den	k1gInPc4	den
byly	být	k5eAaImAgFnP	být
ukradeny	ukraden	k2eAgMnPc4d1	ukraden
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc4	problém
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
také	také	k9	také
obtížné	obtížný	k2eAgNnSc1d1	obtížné
vypočítání	vypočítání	k1gNnSc1	vypočítání
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
mezd	mzda	k1gFnPc2	mzda
apod.	apod.	kA	apod.
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
původně	původně	k6eAd1	původně
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Martin	Martin	k1gMnSc1	Martin
Medek	Medek	k1gMnSc1	Medek
z	z	k7c2	z
Mohelnice	Mohelnice	k1gFnSc2	Mohelnice
oznámil	oznámit	k5eAaPmAgMnS	oznámit
změnu	změna	k1gFnSc4	změna
data	datum	k1gNnPc4	datum
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1582	[number]	k4	1582
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
návrh	návrh	k1gInSc1	návrh
však	však	k9	však
protestantsky	protestantsky	k6eAd1	protestantsky
smýšlející	smýšlející	k2eAgNnSc1d1	smýšlející
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
šlechta	šlechta	k1gFnSc1	šlechta
i	i	k8xC	i
astronomové	astronom	k1gMnPc1	astronom
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1583	[number]	k4	1583
přijetí	přijetí	k1gNnSc4	přijetí
nového	nový	k2eAgInSc2d1	nový
kalendáře	kalendář	k1gInSc2	kalendář
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
i	i	k9	i
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Reformu	reforma	k1gFnSc4	reforma
tak	tak	k6eAd1	tak
realizoval	realizovat	k5eAaBmAgMnS	realizovat
svým	svůj	k3xOyFgInSc7	svůj
mandátem	mandát	k1gInSc7	mandát
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1583	[number]	k4	1583
až	až	k8xS	až
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
skok	skok	k1gInSc1	skok
z	z	k7c2	z
pondělí	pondělí	k1gNnSc2	pondělí
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
na	na	k7c4	na
úterý	úterý	k1gNnSc4	úterý
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
sněm	sněm	k1gInSc4	sněm
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
provedena	provést	k5eAaPmNgFnS	provést
z	z	k7c2	z
neděle	neděle	k1gFnSc2	neděle
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
na	na	k7c6	na
pondělí	pondělí	k1gNnSc6	pondělí
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1584	[number]	k4	1584
<g/>
.	.	kIx.	.
</s>
<s>
Moravští	moravský	k2eAgMnPc1d1	moravský
stavové	stavový	k2eAgNnSc4d1	stavové
ale	ale	k8xC	ale
mandát	mandát	k1gInSc4	mandát
neuposlechli	uposlechnout	k5eNaPmAgMnP	uposlechnout
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
platil	platit	k5eAaImAgInS	platit
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
císařské	císařský	k2eAgNnSc1d1	císařské
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
již	jenž	k3xRgMnPc1	jenž
odmítnout	odmítnout	k5eAaPmF	odmítnout
nemohli	moct	k5eNaImAgMnP	moct
a	a	k8xC	a
kalendář	kalendář	k1gInSc4	kalendář
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
i	i	k9	i
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
po	po	k7c6	po
sobotě	sobota	k1gFnSc6	sobota
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
následovala	následovat	k5eAaImAgFnS	následovat
neděle	neděle	k1gFnSc1	neděle
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1584	[number]	k4	1584
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
na	na	k7c6	na
kalendáři	kalendář	k1gInSc6	kalendář
závisí	záviset	k5eAaImIp3nS	záviset
výpočet	výpočet	k1gInSc1	výpočet
data	datum	k1gNnSc2	datum
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1584	[number]	k4	1584
slavily	slavit	k5eAaImAgFnP	slavit
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
město	město	k1gNnSc1	město
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přijalo	přijmout	k5eAaPmAgNnS	přijmout
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1588	[number]	k4	1588
Kadaň	Kadaň	k1gFnSc4	Kadaň
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ostatní	ostatní	k2eAgFnPc1d1	ostatní
země	zem	k1gFnPc1	zem
s	s	k7c7	s
převládajícím	převládající	k2eAgNnSc7d1	převládající
protestantským	protestantský	k2eAgNnSc7d1	protestantské
a	a	k8xC	a
pravoslavným	pravoslavný	k2eAgNnSc7d1	pravoslavné
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
na	na	k7c4	na
reformu	reforma	k1gFnSc4	reforma
přistupovaly	přistupovat	k5eAaImAgFnP	přistupovat
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
nejednotně	jednotně	k6eNd1	jednotně
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
ročních	roční	k2eAgNnPc6d1	roční
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
byla	být	k5eAaImAgFnS	být
reforma	reforma	k1gFnSc1	reforma
přijata	přijmout	k5eAaPmNgFnS	přijmout
sice	sice	k8xC	sice
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1582	[number]	k4	1582
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
reforma	reforma	k1gFnSc1	reforma
způsobila	způsobit	k5eAaPmAgFnS	způsobit
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnSc6d1	římská
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
katolické	katolický	k2eAgFnPc1d1	katolická
země	zem	k1gFnPc1	zem
přijaly	přijmout	k5eAaPmAgFnP	přijmout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1583	[number]	k4	1583
<g/>
–	–	k?	–
<g/>
1584	[number]	k4	1584
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protestantské	protestantský	k2eAgNnSc1d1	protestantské
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1700	[number]	k4	1700
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přinášelo	přinášet	k5eAaImAgNnS	přinášet
potíže	potíž	k1gFnPc4	potíž
zejména	zejména	k6eAd1	zejména
obchodníkům	obchodník	k1gMnPc3	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Protestantské	protestantský	k2eAgFnPc1d1	protestantská
země	zem	k1gFnPc1	zem
označovaly	označovat	k5eAaImAgFnP	označovat
papeže	papež	k1gMnPc4	papež
posměšně	posměšně	k6eAd1	posměšně
jako	jako	k8xS	jako
Řehoře	Řehoř	k1gMnSc2	Řehoř
Kalendářníka	Kalendářník	k1gMnSc2	Kalendářník
a	a	k8xC	a
tvrdily	tvrdit	k5eAaImAgFnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Antikrist	Antikrist	k1gMnSc1	Antikrist
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
přimět	přimět	k5eAaPmF	přimět
věrné	věrný	k2eAgMnPc4d1	věrný
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
modlili	modlit	k5eAaImAgMnP	modlit
v	v	k7c4	v
nesprávný	správný	k2eNgInSc4d1	nesprávný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
Německo	Německo	k1gNnSc1	Německo
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
než	než	k8xS	než
jej	on	k3xPp3gInSc4	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
protestantská	protestantský	k2eAgNnPc1d1	protestantské
knížata	kníže	k1gNnPc4	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
jako	jako	k8xS	jako
celek	celek	k1gInSc1	celek
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
až	až	k9	až
1775	[number]	k4	1775
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
české	český	k2eAgFnPc1d1	Česká
obce	obec	k1gFnPc1	obec
přijaly	přijmout	k5eAaPmAgFnP	přijmout
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1700	[number]	k4	1700
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Hranice	hranice	k1gFnSc1	hranice
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Cheb	Cheb	k1gInSc1	Cheb
<g/>
))	))	k?	))
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kalendář	kalendář	k1gInSc1	kalendář
změnil	změnit	k5eAaPmAgInS	změnit
roku	rok	k1gInSc2	rok
1587	[number]	k4	1587
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
21	[number]	k4	21
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc6	říjen
psal	psát	k5eAaImAgMnS	psát
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
zmatek	zmatek	k1gInSc4	zmatek
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
reformu	reforma	k1gFnSc4	reforma
nepřijalo	přijmout	k5eNaPmAgNnS	přijmout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
aby	aby	kYmCp3nS	aby
rozdíl	rozdíl	k1gInSc1	rozdíl
astronomického	astronomický	k2eAgMnSc2d1	astronomický
a	a	k8xC	a
církevního	církevní	k2eAgInSc2d1	církevní
kalendáře	kalendář	k1gInSc2	kalendář
nenarůstal	narůstat	k5eNaImAgInS	narůstat
<g/>
,	,	kIx,	,
vypustilo	vypustit	k5eAaPmAgNnS	vypustit
přestupný	přestupný	k2eAgInSc4d1	přestupný
den	den	k1gInSc4	den
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získalo	získat	k5eAaPmAgNnS	získat
jiný	jiný	k2eAgInSc4d1	jiný
kalendář	kalendář	k1gInSc4	kalendář
než	než	k8xS	než
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
evropské	evropský	k2eAgFnPc1d1	Evropská
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
záměr	záměr	k1gInSc1	záměr
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
40	[number]	k4	40
let	léto	k1gNnPc2	léto
vypouštět	vypouštět	k5eAaImF	vypouštět
přestupný	přestupný	k2eAgInSc4d1	přestupný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
tak	tak	k6eAd1	tak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
sjednocení	sjednocení	k1gNnSc4	sjednocení
s	s	k7c7	s
gregoriánským	gregoriánský	k2eAgInSc7d1	gregoriánský
kalendářem	kalendář	k1gInSc7	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
reformy	reforma	k1gFnSc2	reforma
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
upuštěno	upustit	k5eAaPmNgNnS	upustit
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
se	se	k3xPyFc4	se
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
k	k	k7c3	k
juliánskému	juliánský	k2eAgInSc3d1	juliánský
kalendáři	kalendář	k1gInSc3	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
přijat	přijmout	k5eAaPmNgInS	přijmout
gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Specifická	specifický	k2eAgFnSc1d1	specifická
situace	situace	k1gFnSc1	situace
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
v	v	k7c6	v
době	doba	k1gFnSc6	doba
reformy	reforma	k1gFnSc2	reforma
vládla	vládnout	k5eAaImAgFnS	vládnout
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1533	[number]	k4	1533
<g/>
–	–	k?	–
<g/>
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sice	sice	k8xC	sice
byla	být	k5eAaImAgFnS	být
protestantkou	protestantka	k1gFnSc7	protestantka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
požádala	požádat	k5eAaPmAgFnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
astrolog	astrolog	k1gMnSc1	astrolog
John	John	k1gMnSc1	John
Dee	Dee	k1gMnSc1	Dee
reformu	reforma	k1gFnSc4	reforma
prostudoval	prostudovat	k5eAaPmAgMnS	prostudovat
a	a	k8xC	a
sdělil	sdělit	k5eAaPmAgMnS	sdělit
královně	královna	k1gFnSc6	královna
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Dee	Dee	k?	Dee
coby	coby	k?	coby
vědec	vědec	k1gMnSc1	vědec
reformu	reforma	k1gFnSc4	reforma
schválil	schválit	k5eAaPmAgMnS	schválit
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
tedy	tedy	k9	tedy
hodlala	hodlat	k5eAaImAgFnS	hodlat
reformu	reforma	k1gFnSc4	reforma
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zapřisáhlému	zapřisáhlý	k2eAgInSc3d1	zapřisáhlý
odporu	odpor	k1gInSc3	odpor
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
byla	být	k5eAaImAgFnS	být
neustále	neustále	k6eAd1	neustále
odkládána	odkládat	k5eAaImNgFnS	odkládat
<g/>
,	,	kIx,	,
až	až	k9	až
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
odlišnost	odlišnost	k1gFnSc4	odlišnost
vůči	vůči	k7c3	vůči
zbytku	zbytek	k1gInSc3	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
pyšná	pyšný	k2eAgFnSc1d1	pyšná
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zvedala	zvedat	k5eAaImAgFnS	zvedat
vlna	vlna	k1gFnSc1	vlna
kritiky	kritika	k1gFnSc2	kritika
stávajícího	stávající	k2eAgInSc2d1	stávající
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
odlišný	odlišný	k2eAgInSc1d1	odlišný
kalendář	kalendář	k1gInSc1	kalendář
přinášel	přinášet	k5eAaImAgInS	přinášet
problémy	problém	k1gInPc4	problém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
důrazně	důrazně	k6eAd1	důrazně
bojoval	bojovat	k5eAaImAgMnS	bojovat
za	za	k7c2	za
přistoupení	přistoupení	k1gNnSc2	přistoupení
ke	k	k7c3	k
gregoriánskému	gregoriánský	k2eAgInSc3d1	gregoriánský
systému	systém	k1gInSc3	systém
Philip	Philip	k1gMnSc1	Philip
Dormer	Dormer	k1gMnSc1	Dormer
Stanhope	Stanhop	k1gInSc5	Stanhop
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Chesterfieldu	Chesterfield	k1gInSc2	Chesterfield
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
jeho	jeho	k3xOp3gFnSc7	jeho
zásluhou	zásluha	k1gFnSc7	zásluha
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
na	na	k7c4	na
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
roku	rok	k1gInSc2	rok
1752	[number]	k4	1752
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
následovalo	následovat	k5eAaImAgNnS	následovat
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
platil	platit	k5eAaImAgInS	platit
juliánský	juliánský	k2eAgInSc4d1	juliánský
kalendář	kalendář	k1gInSc4	kalendář
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
včetně	včetně	k7c2	včetně
Říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platnost	platnost	k1gFnSc1	platnost
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
narušilo	narušit	k5eAaPmAgNnS	narušit
jen	jen	k9	jen
zhruba	zhruba	k6eAd1	zhruba
desetiletí	desetiletí	k1gNnSc4	desetiletí
revolučního	revoluční	k2eAgInSc2d1	revoluční
sovětského	sovětský	k2eAgInSc2d1	sovětský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
byl	být	k5eAaImAgInS	být
gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
přijat	přijmout	k5eAaPmNgInS	přijmout
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
přijalo	přijmout	k5eAaPmAgNnS	přijmout
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
obecně	obecně	k6eAd1	obecně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
)	)	kIx)	)
úpravu	úprava	k1gFnSc4	úprava
nepřijaly	přijmout	k5eNaPmAgFnP	přijmout
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
<s>
Mimoevropské	mimoevropský	k2eAgFnPc1d1	mimoevropská
země	zem	k1gFnPc1	zem
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
evropských	evropský	k2eAgFnPc2d1	Evropská
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
mocností	mocnost	k1gFnPc2	mocnost
nebo	nebo	k8xC	nebo
s	s	k7c7	s
převládajícím	převládající	k2eAgNnSc7d1	převládající
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
přijaly	přijmout	k5eAaPmAgFnP	přijmout
zpravidla	zpravidla	k6eAd1	zpravidla
kalendářní	kalendářní	k2eAgFnSc4d1	kalendářní
reformu	reforma	k1gFnSc4	reforma
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mateřskými	mateřský	k2eAgFnPc7d1	mateřská
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
s	s	k7c7	s
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
jako	jako	k8xS	jako
občanský	občanský	k2eAgInSc4d1	občanský
kalendář	kalendář	k1gInSc4	kalendář
používají	používat	k5eAaImIp3nP	používat
všechny	všechen	k3xTgFnPc4	všechen
země	zem	k1gFnPc4	zem
světa	svět	k1gInSc2	svět
kromě	kromě	k7c2	kromě
čtyř	čtyři	k4xCgFnPc2	čtyři
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
styku	styk	k1gInSc6	styk
náš	náš	k3xOp1gInSc4	náš
letopočet	letopočet	k1gInSc4	letopočet
a	a	k8xC	a
kalendář	kalendář	k1gInSc4	kalendář
nutně	nutně	k6eAd1	nutně
užívat	užívat	k5eAaImF	užívat
musí	muset	k5eAaImIp3nS	muset
také	také	k9	také
<g/>
:	:	kIx,	:
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
platí	platit	k5eAaImIp3nS	platit
islámský	islámský	k2eAgInSc4d1	islámský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
s	s	k7c7	s
etiopským	etiopský	k2eAgInSc7d1	etiopský
kalendářem	kalendář	k1gInSc7	kalendář
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
a	a	k8xC	a
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
perským	perský	k2eAgInSc7d1	perský
kalendářem	kalendář	k1gInSc7	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
kromě	kromě	k7c2	kromě
Gregoránského	Gregoránský	k2eAgInSc2d1	Gregoránský
kalendáře	kalendář	k1gInSc2	kalendář
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
tradiční	tradiční	k2eAgInSc4d1	tradiční
kalendář	kalendář	k1gInSc4	kalendář
a	a	k8xC	a
letopočet	letopočet	k1gInSc4	letopočet
<g/>
:	:	kIx,	:
Izrael	Izrael	k1gInSc1	Izrael
se	s	k7c7	s
židovským	židovský	k2eAgInSc7d1	židovský
kalendářem	kalendář	k1gInSc7	kalendář
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
zavedla	zavést	k5eAaPmAgFnS	zavést
indický	indický	k2eAgInSc4d1	indický
národní	národní	k2eAgInSc4d1	národní
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
používají	používat	k5eAaImIp3nP	používat
modifikovanou	modifikovaný	k2eAgFnSc4d1	modifikovaná
verzi	verze	k1gFnSc4	verze
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
:	:	kIx,	:
Thajsko	Thajsko	k1gNnSc1	Thajsko
s	s	k7c7	s
thajským	thajský	k2eAgInSc7d1	thajský
solárním	solární	k2eAgInSc7d1	solární
kalendářem	kalendář	k1gInSc7	kalendář
<g/>
:	:	kIx,	:
Thajská	thajský	k2eAgFnSc1d1	thajská
modifikace	modifikace	k1gFnSc1	modifikace
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
roky	rok	k1gInPc7	rok
počítají	počítat	k5eAaImIp3nP	počítat
nikoliv	nikoliv	k9	nikoliv
podle	podle	k7c2	podle
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
epochy	epocha	k1gFnSc2	epocha
<g/>
)	)	kIx)	)
éry	éra	k1gFnSc2	éra
Buddhasakarat	Buddhasakarat	k1gInSc1	Buddhasakarat
(	(	kIx(	(
<g/>
Buddhistické	buddhistický	k2eAgFnSc2d1	buddhistická
éry	éra	k1gFnSc2	éra
–	–	k?	–
B.E.	B.E.	k1gFnSc2	B.E.
<g/>
)	)	kIx)	)
o	o	k7c4	o
543	[number]	k4	543
let	léto	k1gNnPc2	léto
delší	dlouhý	k2eAgFnPc1d2	delší
(	(	kIx(	(
<g/>
číselně	číselně	k6eAd1	číselně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
letopočet	letopočet	k1gInSc1	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2012	[number]	k4	2012
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
rok	rok	k1gInSc4	rok
2555	[number]	k4	2555
B.E.	B.E.	k1gFnPc2	B.E.
Japonsko	Japonsko	k1gNnSc1	Japonsko
s	s	k7c7	s
japonským	japonský	k2eAgInSc7d1	japonský
kalendářem	kalendář	k1gInSc7	kalendář
<g/>
:	:	kIx,	:
Japonská	japonský	k2eAgFnSc1d1	japonská
modifikace	modifikace	k1gFnSc1	modifikace
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
dělení	dělení	k1gNnSc6	dělení
na	na	k7c4	na
éry	éra	k1gFnPc4	éra
posmrtných	posmrtný	k2eAgNnPc2d1	posmrtné
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
年	年	k?	年
<g/>
,	,	kIx,	,
nengó	nengó	k?	nengó
<g/>
)	)	kIx)	)
panujících	panující	k2eAgMnPc2d1	panující
císařů	císař	k1gMnPc2	císař
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
rokem	rok	k1gInSc7	rok
Heisei	Heise	k1gFnSc2	Heise
24	[number]	k4	24
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
24	[number]	k4	24
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Akihita	Akihit	k1gMnSc2	Akihit
(	(	kIx(	(
<g/>
posmrtné	posmrtný	k2eAgNnSc1d1	posmrtné
jméno	jméno	k1gNnSc1	jméno
Heisei	Heise	k1gFnSc2	Heise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
modifikací	modifikace	k1gFnSc7	modifikace
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
datum	datum	k1gNnSc1	datum
jarní	jarní	k2eAgFnSc2d1	jarní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
jako	jako	k8xC	jako
začátku	začátek	k1gInSc2	začátek
jara	jaro	k1gNnSc2	jaro
není	být	k5eNaImIp3nS	být
určováno	určován	k2eAgNnSc1d1	určováno
úředně	úředně	k6eAd1	úředně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
astronomicky	astronomicky	k6eAd1	astronomicky
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
namísto	namísto	k7c2	namísto
pevného	pevný	k2eAgNnSc2d1	pevné
<g/>
,	,	kIx,	,
administrativního	administrativní	k2eAgNnSc2d1	administrativní
stanovení	stanovení	k1gNnSc2	stanovení
jarní	jarní	k2eAgFnSc2d1	jarní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
kalendáři	kalendář	k1gInSc6	kalendář
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c4	mezi
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc4	náš
datování	datování	k1gNnSc4	datování
podle	podle	k7c2	podle
našeho	náš	k3xOp1gInSc2	náš
(	(	kIx(	(
<g/>
Dionysiova	Dionysiův	k2eAgInSc2d1	Dionysiův
<g/>
)	)	kIx)	)
letopočtu	letopočet	k1gInSc2	letopočet
a	a	k8xC	a
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
je	být	k5eAaImIp3nS	být
také	také	k9	také
široce	široko	k6eAd1	široko
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
tropického	tropický	k2eAgInSc2d1	tropický
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
činí	činit	k5eAaImIp3nS	činit
365,242	[number]	k4	365,242
192	[number]	k4	192
129	[number]	k4	129
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
365	[number]	k4	365
dní	den	k1gInPc2	den
5	[number]	k4	5
h	h	k?	h
48	[number]	k4	48
min	min	kA	min
45,4	[number]	k4	45,4
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
juliánského	juliánský	k2eAgInSc2d1	juliánský
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
365,25	[number]	k4	365,25
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
365	[number]	k4	365
dní	den	k1gInPc2	den
6	[number]	k4	6
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
ustanovením	ustanovení	k1gNnSc7	ustanovení
přestupných	přestupný	k2eAgNnPc2d1	přestupné
století	století	k1gNnPc2	století
délku	délka	k1gFnSc4	délka
roku	rok	k1gInSc2	rok
upravil	upravit	k5eAaPmAgInS	upravit
na	na	k7c4	na
365,242	[number]	k4	365,242
<g/>
5	[number]	k4	5
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
365	[number]	k4	365
dní	den	k1gInPc2	den
5	[number]	k4	5
h	h	k?	h
49	[number]	k4	49
min	min	kA	min
12	[number]	k4	12
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
od	od	k7c2	od
délky	délka	k1gFnSc2	délka
tropického	tropický	k2eAgInSc2d1	tropický
roku	rok	k1gInSc2	rok
činí	činit	k5eAaImIp3nS	činit
rozdíl	rozdíl	k1gInSc1	rozdíl
pouze	pouze	k6eAd1	pouze
26,6	[number]	k4	26,6
s	s	k7c7	s
(	(	kIx(	(
<g/>
zatímco	zatímco	k8xS	zatímco
rozdíl	rozdíl	k1gInSc4	rozdíl
délky	délka	k1gFnSc2	délka
tropického	tropický	k2eAgInSc2d1	tropický
roku	rok	k1gInSc2	rok
s	s	k7c7	s
juliánským	juliánský	k2eAgInSc7d1	juliánský
kalendářem	kalendář	k1gInSc7	kalendář
je	být	k5eAaImIp3nS	být
až	až	k9	až
11	[number]	k4	11
min	min	kA	min
14,6	[number]	k4	14,6
s	s	k7c7	s
<g/>
;	;	kIx,	;
rozdíl	rozdíl	k1gInSc1	rozdíl
délky	délka	k1gFnSc2	délka
obou	dva	k4xCgInPc2	dva
kalendářů	kalendář	k1gInPc2	kalendář
tedy	tedy	k9	tedy
činí	činit	k5eAaImIp3nS	činit
10	[number]	k4	10
min	min	kA	min
48	[number]	k4	48
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
kalendář	kalendář	k1gInSc1	kalendář
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
od	od	k7c2	od
tropického	tropický	k2eAgInSc2d1	tropický
roku	rok	k1gInSc2	rok
odchýlí	odchýlit	k5eAaPmIp3nS	odchýlit
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
nejdříve	dříve	k6eAd3	dříve
za	za	k7c4	za
několik	několik	k4yIc4	několik
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
a	a	k8xC	a
diskuse	diskuse	k1gFnSc2	diskuse
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
reformě	reforma	k1gFnSc6	reforma
ohledně	ohledně	k7c2	ohledně
přestupných	přestupný	k2eAgInPc2d1	přestupný
dnů	den	k1gInPc2	den
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
teoretické	teoretický	k2eAgInPc1d1	teoretický
<g/>
.	.	kIx.	.
</s>
<s>
Chybu	chyba	k1gFnSc4	chyba
pak	pak	k6eAd1	pak
opět	opět	k6eAd1	opět
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
snadno	snadno	k6eAd1	snadno
odstraní	odstranit	k5eAaPmIp3nS	odstranit
jednorázové	jednorázový	k2eAgNnSc4d1	jednorázové
vynechání	vynechání	k1gNnSc4	vynechání
přestupného	přestupný	k2eAgInSc2d1	přestupný
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ji	on	k3xPp3gFnSc4	on
budou	být	k5eAaImBp3nP	být
lidé	člověk	k1gMnPc1	člověk
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
významnou	významný	k2eAgFnSc4d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
následující	následující	k2eAgInPc4d1	následující
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
Rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
přestupný	přestupný	k2eAgInSc1d1	přestupný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dělitelný	dělitelný	k2eAgMnSc1d1	dělitelný
číslem	číslo	k1gNnSc7	číslo
4	[number]	k4	4
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
Výjimka	výjimka	k1gFnSc1	výjimka
č.	č.	k?	č.
1	[number]	k4	1
:	:	kIx,	:
Rok	rok	k1gInSc1	rok
není	být	k5eNaImIp3nS	být
přestupný	přestupný	k2eAgInSc1d1	přestupný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dělitelný	dělitelný	k2eAgInSc4d1	dělitelný
<g />
.	.	kIx.	.
</s>
<s>
číslem	číslo	k1gNnSc7	číslo
100	[number]	k4	100
(	(	kIx(	(
<g/>
1700	[number]	k4	1700
<g/>
,	,	kIx,	,
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
2100	[number]	k4	2100
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
)	)	kIx)	)
Výjimka	výjimka	k1gFnSc1	výjimka
č.	č.	k?	č.
2	[number]	k4	2
z	z	k7c2	z
výjimky	výjimka	k1gFnSc2	výjimka
č.	č.	k?	č.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
výjimka	výjimka	k1gFnSc1	výjimka
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přestupný	přestupný	k2eAgMnSc1d1	přestupný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dělitelný	dělitelný	k2eAgMnSc1d1	dělitelný
číslem	číslo	k1gNnSc7	číslo
400	[number]	k4	400
(	(	kIx(	(
<g/>
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2400	[number]	k4	2400
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
ustavil	ustavit	k5eAaPmAgInS	ustavit
čtyřsetletý	čtyřsetletý	k2eAgInSc1d1	čtyřsetletý
stálý	stálý	k2eAgInSc1d1	stálý
cyklus	cyklus	k1gInSc1	cyklus
shodného	shodný	k2eAgNnSc2d1	shodné
uspořádání	uspořádání	k1gNnSc2	uspořádání
dnů	den	k1gInPc2	den
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
létech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
naroste	narůst	k5eAaPmIp3nS	narůst
rozdíl	rozdíl	k1gInSc1	rozdíl
oproti	oproti	k7c3	oproti
juliánskému	juliánský	k2eAgInSc3d1	juliánský
kalendáři	kalendář	k1gInSc3	kalendář
přesně	přesně	k6eAd1	přesně
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Rozprostření	rozprostření	k1gNnSc1	rozprostření
dnů	den	k1gInPc2	den
do	do	k7c2	do
roků	rok	k1gInPc2	rok
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
akceptované	akceptovaný	k2eAgNnSc1d1	akceptované
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
uspořádání	uspořádání	k1gNnSc3	uspořádání
dnů	den	k1gInPc2	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
do	do	k7c2	do
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
týdnů	týden	k1gInPc2	týden
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
jistě	jistě	k6eAd1	jistě
nadále	nadále	k6eAd1	nadále
bude	být	k5eAaImBp3nS	být
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
diskusí	diskuse	k1gFnPc2	diskuse
a	a	k8xC	a
návrhů	návrh	k1gInPc2	návrh
na	na	k7c4	na
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
reformy	reforma	k1gFnPc4	reforma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
mylná	mylný	k2eAgFnSc1d1	mylná
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
rok	rok	k1gInSc1	rok
4840	[number]	k4	4840
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
nebude	být	k5eNaImBp3nS	být
přestupný	přestupný	k2eAgInSc1d1	přestupný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
omyl	omyl	k1gInSc1	omyl
zřejmě	zřejmě	k6eAd1	zřejmě
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
o	o	k7c4	o
astronomii	astronomie	k1gFnSc4	astronomie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ho	on	k3xPp3gInSc4	on
následně	následně	k6eAd1	následně
převzala	převzít	k5eAaPmAgFnS	převzít
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
autorů	autor	k1gMnPc2	autor
populárně-naučné	populárněaučný	k2eAgFnSc2d1	populárně-naučná
literatury	literatura	k1gFnSc2	literatura
i	i	k8xC	i
autorů	autor	k1gMnPc2	autor
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
slovenském	slovenský	k2eAgInSc6d1	slovenský
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
chronologie	chronologie	k1gFnSc1	chronologie
–	–	k?	–
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Libri	Libri	k6eAd1	Libri
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7277-024-1	[number]	k4	80-7277-024-1
DUNCAN	DUNCAN	kA	DUNCAN
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Ewing	Ewing	k1gMnSc1	Ewing
<g/>
.	.	kIx.	.
</s>
<s>
Kalendář	kalendář	k1gInSc1	kalendář
-	-	kIx~	-
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
přesného	přesný	k2eAgInSc2d1	přesný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
SELEŠNIKOV	SELEŠNIKOV	kA	SELEŠNIKOV
<g/>
,	,	kIx,	,
S.	S.	kA	S.
I.	I.	kA	I.
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Práce	práce	k1gFnSc1	práce
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
kalendář	kalendář	k1gInSc1	kalendář
rok	rok	k1gInSc1	rok
letopočet	letopočet	k1gInSc1	letopočet
čas	čas	k1gInSc1	čas
přestupný	přestupný	k2eAgInSc1d1	přestupný
rok	rok	k1gInSc1	rok
30	[number]	k4	30
<g/>
.	.	kIx.	.
únor	únor	k1gInSc4	únor
datování	datování	k1gNnSc2	datování
</s>
