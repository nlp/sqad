<s>
Gregoriánský	gregoriánský	k2eAgInSc1d1
kalendář	kalendář	k1gInSc1
je	být	k5eAaImIp3nS
oproti	oproti	k7c3
juliánskému	juliánský	k2eAgInSc3d1
kalendáři	kalendář	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1
opravil	opravit	k5eAaPmAgMnS
<g/>
,	,	kIx,
významně	významně	k6eAd1
přesnější	přesný	k2eAgMnSc1d2
v	v	k7c6
průměrné	průměrný	k2eAgFnSc6d1
délce	délka	k1gFnSc6
roku	rok	k1gInSc2
-	-	kIx~
zatímco	zatímco	k8xS
juliánský	juliánský	k2eAgInSc1d1
kalendář	kalendář	k1gInSc1
má	mít	k5eAaImIp3nS
oproti	oproti	k7c3
astronomické	astronomický	k2eAgFnSc3d1
skutečnosti	skutečnost	k1gFnSc3
(	(	kIx(
<g/>
tropickému	tropický	k2eAgInSc3d1
roku	rok	k1gInSc3
<g/>
)	)	kIx)
odchylku	odchylka	k1gFnSc4
1	[number]	k4
den	dna	k1gFnPc2
každých	každý	k3xTgInPc2
127	[number]	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
u	u	k7c2
gregoriánského	gregoriánský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
tato	tento	k3xDgFnSc1
odchylka	odchylka	k1gFnSc1
činí	činit	k5eAaImIp3nS
1	[number]	k4
den	den	k1gInSc1
přibližně	přibližně	k6eAd1
každých	každý	k3xTgNnPc2
3300	[number]	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>