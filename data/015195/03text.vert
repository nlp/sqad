<s>
Alexandr	Alexandr	k1gMnSc1
Lebzjak	Lebzjak	k1gMnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Lebzjak	Lebzjak	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
51	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Doněck	Doněck	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
boxer	boxer	k1gMnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Řád	řád	k1gInSc1
ctimedaile	ctimedaile	k6eAd1
Řádu	řád	k1gInSc3
za	za	k7c2
zásluhy	zásluha	k1gFnSc2
o	o	k7c4
vlast	vlast	k1gFnSc4
2	#num#	k4
<g/>
.	.	kIx.
třídyŘád	třídyŘáda	k1gFnPc2
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
o	o	k7c4
vlast	vlast	k1gFnSc4
4	#num#	k4
<g/>
.	.	kIx.
třídymedaile	třídymedaile	k6eAd1
Na	na	k7c4
památku	památka	k1gFnSc4
850	#num#	k4
let	léto	k1gNnPc2
Moskvy	Moskva	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Box	box	k1gInSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
LOH	LOH	kA
2000	#num#	k4
</s>
<s>
polotěžká	polotěžký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
1997	#num#	k4
</s>
<s>
lehká	lehký	k2eAgFnSc1d1
těžká	těžký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
1991	#num#	k4
</s>
<s>
střední	střední	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
ME	ME	kA
1998	#num#	k4
</s>
<s>
lehká	lehký	k2eAgFnSc1d1
těžká	těžký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
ME	ME	kA
2000	#num#	k4
</s>
<s>
lehká	lehký	k2eAgFnSc1d1
těžká	těžký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
ME	ME	kA
1993	#num#	k4
</s>
<s>
střední	střední	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ME	ME	kA
1991	#num#	k4
</s>
<s>
střední	střední	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ME	ME	kA
1996	#num#	k4
</s>
<s>
střední	střední	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Borisovič	Borisovič	k1gMnSc1
Lebzjak	Lebzjak	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
:	:	kIx,
А	А	k?
Б	Б	k?
Л	Л	k?
<g/>
,	,	kIx,
*	*	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1969	#num#	k4
<g/>
,	,	kIx,
Doněck	Doněck	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ruský	ruský	k2eAgMnSc1d1
boxer	boxer	k1gMnSc1
<g/>
,	,	kIx,
olympijský	olympijský	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
z	z	k7c2
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
Sydney	Sydney	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Boxem	box	k1gInSc7
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
zabývat	zabývat	k5eAaImF
jako	jako	k9
žák	žák	k1gMnSc1
v	v	k7c6
malé	malý	k2eAgFnSc6d1
vesnici	vesnice	k1gFnSc6
Burkanďje	Burkanďj	k1gInSc2
900	#num#	k4
km	km	kA
daleko	daleko	k6eAd1
od	od	k7c2
nejbližšího	blízký	k2eAgNnSc2d3
města	město	k1gNnSc2
Magadanu	Magadan	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
Magadanu	Magadan	k1gInSc2
pozvání	pozvánět	k5eAaImIp3nS
jako	jako	k9
nadaný	nadaný	k2eAgMnSc1d1
sportovec	sportovec	k1gMnSc1
do	do	k7c2
sportovní	sportovní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
na	na	k7c6
Kubě	Kuba	k1gFnSc6
juniorským	juniorský	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
do	do	k7c2
71	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
vojákem	voják	k1gMnSc7
a	a	k8xC
své	svůj	k3xOyFgNnSc4
útočiště	útočiště	k1gNnSc4
našel	najít	k5eAaPmAgMnS
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zranění	zranění	k1gNnSc1
jeho	jeho	k3xOp3gFnSc1
vývoj	vývoj	k1gInSc4
zabrzdila	zabrzdit	k5eAaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
neuspěl	uspět	k5eNaPmAgMnS
ani	ani	k8xC
při	při	k7c6
dvou	dva	k4xCgInPc6
olympijských	olympijský	k2eAgInPc6d1
startech	start	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
vypadl	vypadnout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
ve	v	k7c6
druhém	druhý	k4xOgInSc6
kole	kolo	k1gNnSc6
<g/>
,	,	kIx,
oslabený	oslabený	k2eAgMnSc1d1
po	po	k7c6
zkolabování	zkolabování	k1gNnSc6
plíce	plíce	k1gFnSc2
zaviněném	zaviněný	k2eAgInSc6d1
intenzivním	intenzivní	k2eAgNnSc7d1
shazováním	shazování	k1gNnSc7
kilogramů	kilogram	k1gInPc2
v	v	k7c6
předolympijském	předolympijský	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Atlantě	Atlanta	k1gFnSc6
se	se	k3xPyFc4
situace	situace	k1gFnSc1
zopakovala	zopakovat	k5eAaPmAgFnS
přímo	přímo	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
soutěží	soutěž	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
vypadl	vypadnout	k5eAaPmAgInS
ve	v	k7c6
třetím	třetí	k4xOgNnSc6
kole	kolo	k1gNnSc6
s	s	k7c7
úspěšným	úspěšný	k2eAgMnSc7d1
obhájcem	obhájce	k1gMnSc7
titulu	titul	k1gInSc2
Arielem	Ariel	k1gMnSc7
Hernandezem	Hernandez	k1gInSc7
z	z	k7c2
Kuby	Kuba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	kYmCp3nS
zabránil	zabránit	k5eAaPmAgInS
podobným	podobný	k2eAgFnPc3d1
potížím	potíž	k1gFnPc3
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
přestoupit	přestoupit	k5eAaPmF
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
75	#num#	k4
kg	kg	kA
<g/>
)	)	kIx)
do	do	k7c2
lehké	lehký	k2eAgFnSc2d1
těžké	těžký	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
81	#num#	k4
kg	kg	kA
<g/>
)	)	kIx)
a	a	k8xC
úspěchy	úspěch	k1gInPc1
se	se	k3xPyFc4
dostavily	dostavit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrál	vyhrát	k5eAaPmAgMnS
mistrovství	mistrovství	k1gNnSc3
světa	svět	k1gInSc2
1997	#num#	k4
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
a	a	k8xC
dva	dva	k4xCgInPc1
tituly	titul	k1gInPc1
mistra	mistr	k1gMnSc2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Sydney	Sydney	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
prokazoval	prokazovat	k5eAaImAgMnS
skvělou	skvělý	k2eAgFnSc4d1
formu	forma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestou	cesta	k1gFnSc7
do	do	k7c2
finále	finále	k1gNnSc2
porazil	porazit	k5eAaPmAgMnS
čtyři	čtyři	k4xCgMnPc4
soupeře	soupeř	k1gMnPc4
<g/>
,	,	kIx,
v	v	k7c6
semifinále	semifinále	k1gNnSc6
Uzbeka	Uzbek	k1gMnSc2
Michajlova	Michajlův	k2eAgInSc2d1
verdiktem	verdikt	k1gInSc7
rozhodčího	rozhodčí	k1gMnSc2
r.	r.	kA
<g/>
s.	s.	k?
<g/>
c.	c.	k?
už	už	k6eAd1
v	v	k7c6
prvním	první	k4xOgInSc6
kole	kolo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
jednoznačně	jednoznačně	k6eAd1
porazil	porazit	k5eAaPmAgMnS
českého	český	k2eAgMnSc4d1
reprezentanta	reprezentant	k1gMnSc4
Rudolfa	Rudolf	k1gMnSc4
Kraje	kraj	k1gInPc1
20	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
na	na	k7c4
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
amatérské	amatérský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
absolvoval	absolvovat	k5eAaPmAgInS
jediný	jediný	k2eAgInSc1d1
zápas	zápas	k1gInSc1
v	v	k7c6
profesionálním	profesionální	k2eAgInSc6d1
ringu	ring	k1gInSc6
(	(	kIx(
<g/>
v	v	k7c6
Taškentu	Taškent	k1gInSc6
porazil	porazit	k5eAaPmAgMnS
na	na	k7c4
technické	technický	k2eAgNnSc4d1
k.o.	k.o.	k?
Američana	Američan	k1gMnSc4
Stacyho	Stacy	k1gMnSc4
Goodsona	Goodson	k1gMnSc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
trenérem	trenér	k1gMnSc7
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgMnSc7d1
koučem	kouč	k1gMnSc7
ruské	ruský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SVOBODA	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
Vilém	Vilém	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červená	červené	k1gNnPc1
z	z	k7c2
nosu	nos	k1gInSc2
<g/>
,	,	kIx,
stříbrná	stříbrnat	k5eAaImIp3nS
na	na	k7c6
krku	krk	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
,	,	kIx,
2000-10-01	2000-10-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
boxer	boxer	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Alexander	Alexandra	k1gFnPc2
Lebziak	Lebziak	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BoxRec	BoxRec	k1gInSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
po	po	k7c4
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2001	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
А	А	k?
Б	Б	k?
Л	Л	k?
:	:	kIx,
Alexander	Alexandra	k1gFnPc2
Lebzyak	Lebzyak	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peoples	Peoples	k1gInSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
<g/>
,	,	kIx,
2005-01-21	2005-01-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Lebziak	Lebziak	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boxrec	Boxrec	k1gInSc1
Boxing	boxing	k1gInSc4
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2008-03-27	2008-03-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Lebzjak	Lebzjak	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Box	box	k1gInSc1
<g/>
:	:	kIx,
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
v	v	k7c6
polotěžké	polotěžký	k2eAgFnSc6d1
váze	váha	k1gFnSc6
</s>
<s>
1920	#num#	k4
Eddie	Eddie	k1gFnSc1
Eagan	Eagany	k1gInPc2
•	•	k?
1924	#num#	k4
Harry	Harra	k1gFnSc2
Mitchell	Mitchella	k1gFnPc2
•	•	k?
1928	#num#	k4
Víctor	Víctor	k1gInSc1
Avendañ	Avendañ	k6eAd1
•	•	k?
1932	#num#	k4
David	David	k1gMnSc1
Carstens	Carstens	k1gInSc1
•	•	k?
1936	#num#	k4
Roger	Roger	k1gInSc1
Michelot	Michelot	k1gInSc4
•	•	k?
1948	#num#	k4
George	George	k1gInSc1
Hunter	Hunter	k1gInSc4
•	•	k?
1952	#num#	k4
Norvel	Norvela	k1gFnPc2
Lee	Lea	k1gFnSc6
•	•	k?
1956	#num#	k4
James	James	k1gInSc1
Boyd	Boyd	k1gInSc4
•	•	k?
1960	#num#	k4
Cassius	Cassius	k1gInSc1
Clay	Claa	k1gFnSc2
•	•	k?
1964	#num#	k4
Cosimo	Cosima	k1gFnSc5
Pinto	pinta	k1gFnSc5
•	•	k?
1968	#num#	k4
Danas	Danas	k1gInSc1
Pozniakas	Pozniakas	k1gInSc4
•	•	k?
1972	#num#	k4
Mate	mást	k5eAaImIp3nS
Parlov	Parlov	k1gInSc4
•	•	k?
1976	#num#	k4
Leon	Leona	k1gFnPc2
Spinks	Spinksa	k1gFnPc2
•	•	k?
1980	#num#	k4
Slobodan	Slobodan	k1gMnSc1
Kačar	Kačar	k1gMnSc1
•	•	k?
1984	#num#	k4
Anton	anton	k1gInSc1
Josipović	Josipović	k1gFnSc2
•	•	k?
1988	#num#	k4
Andrew	Andrew	k1gFnSc2
Maynard	Maynarda	k1gFnPc2
•	•	k?
1992	#num#	k4
Torsten	Torsten	k2eAgMnSc1d1
May	May	k1gMnSc1
•	•	k?
1996	#num#	k4
Vasilij	Vasilij	k1gFnPc2
Žirov	Žirov	k1gInSc4
•	•	k?
2000	#num#	k4
Alexandr	Alexandr	k1gMnSc1
Lebzjak	Lebzjak	k1gMnSc1
•	•	k?
2004	#num#	k4
Andre	Andr	k1gInSc5
Ward	Ward	k1gInSc4
•	•	k?
2008	#num#	k4
Čang	Čang	k1gInSc1
Siao-pching	Siao-pching	k1gInSc4
•	•	k?
2012	#num#	k4
Jegor	Jegora	k1gFnPc2
Mechoncev	Mechoncva	k1gFnPc2
•	•	k?
2016	#num#	k4
Julio	Julio	k1gMnSc1
César	César	k1gMnSc1
La	la	k1gNnSc2
Cruz	Cruz	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
