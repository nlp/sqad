<p>
<s>
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gInSc1	Thymus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
aromatických	aromatický	k2eAgFnPc2d1	aromatická
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
hluchavkovitých	hluchavkovitý	k2eAgInPc2d1	hluchavkovitý
<g/>
,	,	kIx,	,
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
druhů	druh	k1gInPc2	druh
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
ke	k	k7c3	k
starým	starý	k2eAgInPc3d1	starý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
léčivým	léčivý	k2eAgFnPc3d1	léčivá
bylinám	bylina	k1gFnPc3	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
antiseptické	antiseptický	k2eAgInPc4d1	antiseptický
a	a	k8xC	a
uklidňující	uklidňující	k2eAgInPc4d1	uklidňující
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
navozuje	navozovat	k5eAaImIp3nS	navozovat
příjemný	příjemný	k2eAgInSc1d1	příjemný
spánek	spánek	k1gInSc1	spánek
<g/>
,	,	kIx,	,
pománá	pománý	k2eAgFnSc1d1	pománá
proti	proti	k7c3	proti
stresu	stres	k1gInSc3	stres
<g/>
,	,	kIx,	,
depresi	deprese	k1gFnSc6	deprese
a	a	k8xC	a
migréně	migréna	k1gFnSc6	migréna
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
kašli	kašel	k1gInSc3	kašel
<g/>
,	,	kIx,	,
zahlenění	zahlenění	k1gNnSc3	zahlenění
<g/>
,	,	kIx,	,
zažívacím	zažívací	k2eAgFnPc3d1	zažívací
a	a	k8xC	a
žaludečním	žaludeční	k2eAgFnPc3d1	žaludeční
potížím	potíž	k1gFnPc3	potíž
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
gastronomii	gastronomie	k1gFnSc6	gastronomie
jako	jako	k8xS	jako
koření	kořenit	k5eAaImIp3nS	kořenit
k	k	k7c3	k
masitým	masitý	k2eAgInPc3d1	masitý
pokrmům	pokrm	k1gInPc3	pokrm
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
suchých	suchý	k2eAgInPc6d1	suchý
trávnících	trávník	k1gInPc6	trávník
<g/>
,	,	kIx,	,
na	na	k7c6	na
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
stráních	stráň	k1gFnPc6	stráň
<g/>
,	,	kIx,	,
v	v	k7c6	v
lesních	lesní	k2eAgInPc6d1	lesní
lemech	lem	k1gInPc6	lem
a	a	k8xC	a
světlých	světlý	k2eAgInPc6d1	světlý
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
na	na	k7c6	na
vysokohorských	vysokohorský	k2eAgFnPc6d1	vysokohorská
holích	hole	k1gFnPc6	hole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
květeně	květena	k1gFnSc6	květena
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
kriticky	kriticky	k6eAd1	kriticky
ohroženým	ohrožený	k2eAgFnPc3d1	ohrožená
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
v	v	k7c6	v
zahradních	zahradní	k2eAgInPc6d1	zahradní
kultivarech	kultivar	k1gInPc6	kultivar
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
k	k	k7c3	k
medonosným	medonosný	k2eAgFnPc3d1	medonosná
rostlinám	rostlina	k1gFnPc3	rostlina
s	s	k7c7	s
dobrou	dobrý	k2eAgFnSc7d1	dobrá
produkcí	produkce	k1gFnSc7	produkce
nektaru	nektar	k1gInSc2	nektar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
obrazného	obrazný	k2eAgNnSc2d1	obrazné
pojmenování	pojmenování	k1gNnSc2	pojmenování
"	"	kIx"	"
<g/>
douška	douška	k1gFnSc1	douška
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
duše	duše	k1gFnSc1	duše
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doslovný	doslovný	k2eAgInSc4d1	doslovný
překlad	překlad	k1gInSc4	překlad
latinského	latinský	k2eAgInSc2d1	latinský
výrazu	výraz	k1gInSc2	výraz
matris	matris	k1gFnSc2	matris
animula	animulum	k1gNnSc2	animulum
zmiňovaného	zmiňovaný	k2eAgNnSc2d1	zmiňované
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Origines	Originesa	k1gFnPc2	Originesa
biskupa	biskup	k1gMnSc2	biskup
Isidora	Isidor	k1gMnSc2	Isidor
Sevillského	sevillský	k2eAgMnSc2d1	sevillský
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
zřejmě	zřejmě	k6eAd1	zřejmě
odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
k	k	k7c3	k
rozšířené	rozšířený	k2eAgFnSc3d1	rozšířená
lidové	lidový	k2eAgFnSc3d1	lidová
pověsti	pověst	k1gFnSc3	pověst
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
květině	květina	k1gFnSc6	květina
jako	jako	k8xS	jako
duši	duše	k1gFnSc6	duše
zemřelé	zemřelý	k2eAgFnSc2d1	zemřelá
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
její	její	k3xOp3gFnSc7	její
vůní	vůně	k1gFnSc7	vůně
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
lásku	láska	k1gFnSc4	láska
svým	svůj	k3xOyFgFnPc3	svůj
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
ztotožněna	ztotožnit	k5eAaPmNgFnS	ztotožnit
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
Boží	božit	k5eAaImIp3nS	božit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
např.	např.	kA	např.
ruský	ruský	k2eAgInSc1d1	ruský
lidový	lidový	k2eAgInSc1d1	lidový
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
mateřídoušku	mateřídouška	k1gFnSc4	mateřídouška
"	"	kIx"	"
<g/>
bogorodskaja	bogorodskaj	k2eAgFnSc1d1	bogorodskaj
travka	travka	k1gFnSc1	travka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vegetativní	vegetativní	k2eAgInPc1d1	vegetativní
orgány	orgán	k1gInPc1	orgán
===	===	k?	===
</s>
</p>
<p>
<s>
Mateřídoušky	mateřídouška	k1gFnPc1	mateřídouška
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
silně	silně	k6eAd1	silně
aromatické	aromatický	k2eAgInPc4d1	aromatický
nízké	nízký	k2eAgInPc4d1	nízký
keříky	keřík	k1gInPc4	keřík
<g/>
,	,	kIx,	,
polokeře	polokeř	k1gInPc4	polokeř
nebo	nebo	k8xC	nebo
vytrvalé	vytrvalý	k2eAgFnPc4d1	vytrvalá
byliny	bylina	k1gFnPc4	bylina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
typického	typický	k2eAgInSc2d1	typický
polštářovitého	polštářovitý	k2eAgInSc2d1	polštářovitý
habitu	habitus	k1gInSc2	habitus
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
tenké	tenký	k2eAgFnPc1d1	tenká
větve	větev	k1gFnPc1	větev
<g/>
,	,	kIx,	,
vyrůstající	vyrůstající	k2eAgFnPc1d1	vyrůstající
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
krátkého	krátký	k2eAgInSc2d1	krátký
dřevnatého	dřevnatý	k2eAgInSc2d1	dřevnatý
stonku	stonek	k1gInSc2	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Stonky	stonka	k1gFnPc1	stonka
jsou	být	k5eAaImIp3nP	být
plazivé	plazivý	k2eAgFnPc1d1	plazivá
<g/>
,	,	kIx,	,
vystoupavé	vystoupavý	k2eAgFnPc1d1	vystoupavá
nebo	nebo	k8xC	nebo
přímé	přímý	k2eAgFnPc1d1	přímá
lodyhy	lodyha	k1gFnPc1	lodyha
<g/>
,	,	kIx,	,
v	v	k7c6	v
uzlech	uzel	k1gInPc6	uzel
často	často	k6eAd1	často
kořenující	kořenující	k2eAgNnSc1d1	kořenující
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
sympodiálně	sympodiálně	k6eAd1	sympodiálně
nebo	nebo	k8xC	nebo
monopodiálně	monopodiálně	k6eAd1	monopodiálně
větvené	větvený	k2eAgFnPc1d1	větvená
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
druhově	druhově	k6eAd1	druhově
určovací	určovací	k2eAgInSc1d1	určovací
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Květonosné	květonosný	k2eAgFnPc1d1	květonosná
větve	větev	k1gFnPc1	větev
jsou	být	k5eAaImIp3nP	být
vystoupavé	vystoupavý	k2eAgFnPc1d1	vystoupavá
až	až	k6eAd1	až
přímé	přímý	k2eAgFnPc1d1	přímá
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
oblé	oblý	k2eAgInPc4d1	oblý
nebo	nebo	k8xC	nebo
tupě	tupě	k6eAd1	tupě
až	až	k9	až
ostře	ostro	k6eAd1	ostro
čtyřhranné	čtyřhranný	k2eAgNnSc1d1	čtyřhranné
<g/>
.	.	kIx.	.
</s>
<s>
Druhově	druhově	k6eAd1	druhově
specifické	specifický	k2eAgNnSc1d1	specifické
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
ochlupení	ochlupení	k1gNnSc1	ochlupení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovnoměrné	rovnoměrný	k2eAgInPc1d1	rovnoměrný
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
stranách	strana	k1gFnPc6	strana
stonku	stonek	k1gInSc2	stonek
(	(	kIx(	(
<g/>
odění	odění	k1gNnSc1	odění
holotrichní	holotricheň	k1gFnPc2	holotricheň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hustší	hustý	k2eAgInSc1d2	hustší
na	na	k7c6	na
2	[number]	k4	2
protilehlých	protilehlý	k2eAgFnPc6d1	protilehlá
stranách	strana	k1gFnPc6	strana
(	(	kIx(	(
<g/>
odění	odění	k1gNnSc6	odění
alelotrichní	alelotrichní	k1gNnSc2	alelotrichní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jen	jen	k9	jen
na	na	k7c6	na
hranách	hrana	k1gFnPc6	hrana
(	(	kIx(	(
<g/>
odění	odění	k1gNnSc6	odění
goniotrichní	goniotrichní	k1gNnSc2	goniotrichní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
kořen	kořen	k1gInSc1	kořen
je	být	k5eAaImIp3nS	být
dřevnatějící	dřevnatějící	k2eAgInSc1d1	dřevnatějící
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
rozvětvený	rozvětvený	k2eAgMnSc1d1	rozvětvený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
kalichů	kalich	k1gInPc2	kalich
<g/>
,	,	kIx,	,
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
nadzemních	nadzemní	k2eAgFnPc2d1	nadzemní
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
siličné	siličný	k2eAgFnPc1d1	siličná
žlázky	žlázka	k1gFnPc1	žlázka
<g/>
.	.	kIx.	.
<g/>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
vstřícné	vstřícný	k2eAgInPc1d1	vstřícný
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
nebo	nebo	k8xC	nebo
přisedlé	přisedlý	k2eAgInPc1d1	přisedlý
<g/>
,	,	kIx,	,
čárkovité	čárkovitý	k2eAgInPc1d1	čárkovitý
až	až	k9	až
okrouhle	okrouhle	k6eAd1	okrouhle
vejčité	vejčitý	k2eAgFnSc2d1	vejčitá
<g/>
,	,	kIx,	,
celokrajné	celokrajný	k2eAgFnSc2d1	celokrajná
nebo	nebo	k8xC	nebo
zoubkaté	zoubkatý	k2eAgFnSc2d1	zoubkatá
<g/>
,	,	kIx,	,
chlupaté	chlupatý	k2eAgFnSc2d1	chlupatá
nebo	nebo	k8xC	nebo
lysé	lysý	k2eAgFnSc2d1	Lysá
<g/>
,	,	kIx,	,
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
někdy	někdy	k6eAd1	někdy
mírně	mírně	k6eAd1	mírně
podvinuté	podvinutý	k2eAgNnSc1d1	podvinuté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Generativní	generativní	k2eAgInPc1d1	generativní
orgány	orgán	k1gInPc1	orgán
===	===	k?	===
</s>
</p>
<p>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgMnPc4d1	drobný
<g/>
,	,	kIx,	,
oboupohlavné	oboupohlavný	k2eAgFnPc1d1	oboupohlavná
nebo	nebo	k8xC	nebo
funkčně	funkčně	k6eAd1	funkčně
samičí	samičí	k2eAgFnSc1d1	samičí
(	(	kIx(	(
<g/>
gynodioecie	gynodioecie	k1gFnSc1	gynodioecie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
stopkaté	stopkatý	k2eAgFnPc1d1	stopkatá
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
v	v	k7c6	v
lichopřeslenech	lichopřeslen	k1gInPc6	lichopřeslen
skládajících	skládající	k2eAgInPc2d1	skládající
koncové	koncový	k2eAgInPc4d1	koncový
protáhlé	protáhlý	k2eAgInPc4d1	protáhlý
nebo	nebo	k8xC	nebo
hlávkovitě	hlávkovitě	k6eAd1	hlávkovitě
stažené	stažený	k2eAgFnPc1d1	stažená
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
často	často	k6eAd1	často
přetrhované	přetrhovaný	k2eAgInPc4d1	přetrhovaný
lichoklasy	lichoklas	k1gInPc4	lichoklas
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
trubkovitě	trubkovitě	k6eAd1	trubkovitě
zvonkovitý	zvonkovitý	k2eAgMnSc1d1	zvonkovitý
<g/>
,	,	kIx,	,
dvojpyský	dvojpyský	k2eAgMnSc1d1	dvojpyský
<g/>
,	,	kIx,	,
s	s	k7c7	s
10	[number]	k4	10
až	až	k9	až
13	[number]	k4	13
žilkami	žilka	k1gFnPc7	žilka
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
3	[number]	k4	3
cípy	cíp	k1gInPc7	cíp
jsou	být	k5eAaImIp3nP	být
trojúhelníkovité	trojúhelníkovitý	k2eAgInPc1d1	trojúhelníkovitý
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
2	[number]	k4	2
cípy	cíp	k1gInPc1	cíp
užší	úzký	k2eAgInPc1d2	užší
<g/>
,	,	kIx,	,
šídlovité	šídlovitý	k2eAgInPc1d1	šídlovitý
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
trubkovitá	trubkovitý	k2eAgFnSc1d1	trubkovitá
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
až	až	k9	až
zřetelně	zřetelně	k6eAd1	zřetelně
dvoupyská	dvoupyskat	k5eAaPmIp3nS	dvoupyskat
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc1	barva
bělavé	bělavý	k2eAgFnPc1d1	bělavá
až	až	k9	až
sytě	sytě	k6eAd1	sytě
růžovofialové	růžovofialový	k2eAgNnSc1d1	růžovofialové
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
pysk	pysk	k1gInSc1	pysk
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
<g/>
,	,	kIx,	,
mělce	mělce	k6eAd1	mělce
vykrojený	vykrojený	k2eAgInSc1d1	vykrojený
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgInSc1d1	dolní
pysk	pysk	k1gInSc1	pysk
je	být	k5eAaImIp3nS	být
trojlaločný	trojlaločný	k2eAgInSc1d1	trojlaločný
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinky	tyčinka	k1gFnPc1	tyčinka
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
<g/>
,	,	kIx,	,
vyčnívající	vyčnívající	k2eAgNnSc1d1	vyčnívající
z	z	k7c2	z
koruny	koruna	k1gFnSc2	koruna
(	(	kIx(	(
<g/>
u	u	k7c2	u
funkčně	funkčně	k6eAd1	funkčně
samičích	samičí	k2eAgInPc2d1	samičí
květů	květ	k1gInPc2	květ
ale	ale	k8xC	ale
často	často	k6eAd1	často
zakrnělé	zakrnělý	k2eAgFnPc1d1	zakrnělá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnPc1d1	spodní
dvě	dva	k4xCgFnPc1	dva
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
horní	horní	k2eAgFnSc1d1	horní
<g/>
.	.	kIx.	.
</s>
<s>
Gyneceum	Gyneceum	k1gInSc1	Gyneceum
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svrchním	svrchní	k2eAgInSc7d1	svrchní
semeníkem	semeník	k1gInSc7	semeník
<g/>
,	,	kIx,	,
čnělka	čnělka	k1gFnSc1	čnělka
pestíku	pestík	k1gInSc2	pestík
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
tyčinky	tyčinka	k1gFnPc1	tyčinka
<g/>
,	,	kIx,	,
blizna	blizna	k1gFnSc1	blizna
je	být	k5eAaImIp3nS	být
rozeklána	rozeklát	k5eAaPmNgFnS	rozeklát
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
opylovány	opylovat	k5eAaImNgInP	opylovat
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
široce	široko	k6eAd1	široko
vejcovité	vejcovitý	k2eAgFnPc1d1	vejcovitá
až	až	k8xS	až
kulovité	kulovitý	k2eAgFnPc1d1	kulovitá
tvrdky	tvrdka	k1gFnPc1	tvrdka
s	s	k7c7	s
jemnou	jemný	k2eAgFnSc7d1	jemná
zrnitou	zrnitý	k2eAgFnSc7d1	zrnitá
skulpturou	skulptura	k1gFnSc7	skulptura
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
hladké	hladký	k2eAgFnPc1d1	hladká
<g/>
.	.	kIx.	.
<g/>
Rostliny	rostlina	k1gFnPc1	rostlina
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
různých	různý	k2eAgInPc2d1	různý
stupňů	stupeň	k1gInPc2	stupeň
ploidieː	ploidieː	k?	ploidieː
2	[number]	k4	2
<g/>
x	x	k?	x
(	(	kIx(	(
<g/>
diploidie	diploidie	k1gFnSc1	diploidie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
x	x	k?	x
(	(	kIx(	(
<g/>
tetraploidie	tetraploidie	k1gFnSc2	tetraploidie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
8	[number]	k4	8
<g/>
x	x	k?	x
(	(	kIx(	(
<g/>
oktoploidie	oktoploidie	k1gFnSc2	oktoploidie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
chromozómů	chromozóm	k1gInPc2	chromozóm
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
2	[number]	k4	2
<g/>
n	n	k0	n
=	=	kIx~	=
24	[number]	k4	24
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
,	,	kIx,	,
54	[number]	k4	54
<g/>
,	,	kIx,	,
56	[number]	k4	56
nebo	nebo	k8xC	nebo
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
a	a	k8xC	a
ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
mimo	mimo	k7c4	mimo
její	její	k3xOp3gFnPc4	její
nejsevernější	severní	k2eAgFnPc4d3	nejsevernější
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
temperátní	temperátní	k2eAgFnSc2d1	temperátní
Asie	Asie	k1gFnSc2	Asie
kromě	kromě	k7c2	kromě
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
Etiopské	etiopský	k2eAgFnSc6d1	etiopská
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
,	,	kIx,	,
též	též	k9	též
na	na	k7c6	na
Azorských	azorský	k2eAgInPc6d1	azorský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
Madeiře	Madeira	k1gFnSc6	Madeira
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
druhového	druhový	k2eAgNnSc2d1	druhové
bohatství	bohatství	k1gNnSc2	bohatství
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Synantropně	Synantropně	k6eAd1	Synantropně
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
<g/>
Rostou	růst	k5eAaImIp3nP	růst
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
slunných	slunný	k2eAgNnPc6d1	slunné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nižších	nízký	k2eAgInPc6d2	nižší
xerofilních	xerofilní	k2eAgInPc6d1	xerofilní
trávnících	trávník	k1gInPc6	trávník
<g/>
,	,	kIx,	,
na	na	k7c6	na
skalnatých	skalnatý	k2eAgInPc6d1	skalnatý
svazích	svah	k1gInPc6	svah
<g/>
,	,	kIx,	,
suchých	suchý	k2eAgFnPc6d1	suchá
stráních	stráň	k1gFnPc6	stráň
a	a	k8xC	a
lesních	lesní	k2eAgInPc6d1	lesní
lemech	lem	k1gInPc6	lem
<g/>
,	,	kIx,	,
též	též	k9	též
ve	v	k7c6	v
světlých	světlý	k2eAgInPc6d1	světlý
lesích	les	k1gInPc6	les
nebo	nebo	k8xC	nebo
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
křovin	křovina	k1gFnPc2	křovina
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
nad	nad	k7c7	nad
horní	horní	k2eAgFnSc7d1	horní
hranicí	hranice	k1gFnSc7	hranice
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
preferují	preferovat	k5eAaImIp3nP	preferovat
vápencové	vápencový	k2eAgNnSc4d1	vápencové
podloží	podloží	k1gNnSc4	podloží
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
spíš	spíš	k9	spíš
málo	málo	k6eAd1	málo
úživné	úživný	k2eAgInPc4d1	úživný
<g/>
,	,	kIx,	,
kyselejší	kyselý	k2eAgInPc4d2	kyselejší
nebo	nebo	k8xC	nebo
písčité	písčitý	k2eAgInPc4d1	písčitý
substráty	substrát	k1gInPc4	substrát
<g/>
.	.	kIx.	.
</s>
<s>
Vesměs	vesměs	k6eAd1	vesměs
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
dobře	dobře	k6eAd1	dobře
propustné	propustný	k2eAgFnPc1d1	propustná
půdy	půda	k1gFnPc1	půda
<g/>
,	,	kIx,	,
nesnáší	snášet	k5eNaImIp3nS	snášet
zamokření	zamokření	k1gNnSc1	zamokření
<g/>
.	.	kIx.	.
</s>
<s>
Středomořské	středomořský	k2eAgInPc1d1	středomořský
druhy	druh	k1gInPc1	druh
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
dominantním	dominantní	k2eAgFnPc3d1	dominantní
rostlinám	rostlina	k1gFnPc3	rostlina
křovinatých	křovinatý	k2eAgFnPc2d1	křovinatá
makchií	makchie	k1gFnPc2	makchie
<g/>
,	,	kIx,	,
garrigů	garrig	k1gInPc2	garrig
a	a	k8xC	a
tomillarů	tomillar	k1gInPc2	tomillar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kořenech	kořen	k1gInPc6	kořen
mateřídoušky	mateřídouška	k1gFnSc2	mateřídouška
někdy	někdy	k6eAd1	někdy
parazituje	parazitovat	k5eAaImIp3nS	parazitovat
nezelená	zelený	k2eNgFnSc1d1	nezelená
rostlina	rostlina	k1gFnSc1	rostlina
záraza	záraza	k1gFnSc1	záraza
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
Orobanche	Orobanch	k1gInPc1	Orobanch
alba	alba	k1gFnSc1	alba
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květeně	květena	k1gFnSc6	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
7	[number]	k4	7
druhů	druh	k1gInPc2	druh
mateřídoušek	mateřídouška	k1gFnPc2	mateřídouška
<g/>
.	.	kIx.	.
</s>
<s>
Naším	náš	k3xOp1gInSc7	náš
nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
vejčitá	vejčitý	k2eAgFnSc1d1	vejčitá
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gMnSc1	Thymus
pulegioides	pulegioides	k1gMnSc1	pulegioides
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
zřídkavější	zřídkavý	k2eAgMnSc1d2	zřídkavý
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplých	teplý	k2eAgInPc6d1	teplý
krajích	kraj	k1gInPc6	kraj
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
časná	časný	k2eAgFnSc1d1	časná
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gInSc1	Thymus
praecox	praecox	k1gInSc1	praecox
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
olysalá	olysalý	k2eAgFnSc1d1	olysalá
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gInSc1	Thymus
glabrescens	glabrescens	k1gInSc1	glabrescens
<g/>
)	)	kIx)	)
a	a	k8xC	a
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
panonská	panonský	k2eAgFnSc1d1	Panonská
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gMnSc1	Thymus
pannonicus	pannonicus	k1gMnSc1	pannonicus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
úzkolistá	úzkolistý	k2eAgFnSc1d1	úzkolistá
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gInSc1	Thymus
serpyllum	serpyllum	k1gInSc1	serpyllum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
psamofyt	psamofyt	k1gInSc1	psamofyt
vázaný	vázaný	k2eAgInSc1d1	vázaný
na	na	k7c4	na
písčité	písčitý	k2eAgInPc4d1	písčitý
substráty	substrát	k1gInPc4	substrát
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInPc1d1	zbývající
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
horské	horský	k2eAgInPc1d1	horský
<g/>
:	:	kIx,	:
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
alpinská	alpinský	k2eAgFnSc1d1	alpinská
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gMnSc1	Thymus
alpestris	alpestris	k1gFnSc2	alpestris
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
,	,	kIx,	,
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
ozdobná	ozdobný	k2eAgFnSc1d1	ozdobná
sudetská	sudetský	k2eAgFnSc1d1	sudetská
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gMnSc1	Thymus
pulcherrimus	pulcherrimus	k1gMnSc1	pulcherrimus
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
sudeticus	sudeticus	k1gMnSc1	sudeticus
<g/>
)	)	kIx)	)
v	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
polohách	poloha	k1gFnPc6	poloha
Hrubého	Hrubého	k2eAgInSc2d1	Hrubého
Jeseníku	Jeseník	k1gInSc2	Jeseník
<g/>
.	.	kIx.	.
</s>
<s>
Přechodně	přechodně	k6eAd1	přechodně
zplaňuje	zplaňovat	k5eAaImIp3nS	zplaňovat
pěstovaný	pěstovaný	k2eAgInSc1d1	pěstovaný
tymián	tymián	k1gInSc1	tymián
obecný	obecný	k2eAgInSc1d1	obecný
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gInSc1	Thymus
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc1	taxonomie
a	a	k8xC	a
systematika	systematika	k1gFnSc1	systematika
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc4	rod
platně	platně	k6eAd1	platně
popsal	popsat	k5eAaPmAgMnS	popsat
Carl	Carl	k1gMnSc1	Carl
Linné	Linná	k1gFnSc2	Linná
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Species	species	k1gFnPc2	species
Plantarum	Plantarum	k1gInSc4	Plantarum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1753	[number]	k4	1753
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
zařadil	zařadit	k5eAaPmAgMnS	zařadit
osm	osm	k4xCc1	osm
druhů	druh	k1gInPc2	druh
<g/>
;	;	kIx,	;
pojetí	pojetí	k1gNnSc1	pojetí
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
jeho	jeho	k3xOp3gNnPc6	jeho
dílech	dílo	k1gNnPc6	dílo
proměňovalo	proměňovat	k5eAaImAgNnS	proměňovat
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
byly	být	k5eAaImAgInP	být
průběžně	průběžně	k6eAd1	průběžně
přeřazovány	přeřazovat	k5eAaImNgInP	přeřazovat
do	do	k7c2	do
rodů	rod	k1gInPc2	rod
Satureja	Saturejum	k1gNnSc2	Saturejum
nebo	nebo	k8xC	nebo
Acinos	Acinosa	k1gFnPc2	Acinosa
<g/>
.	.	kIx.	.
</s>
<s>
Dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
Linném	Linné	k1gNnSc6	Linné
byly	být	k5eAaImAgFnP	být
rostliny	rostlina	k1gFnPc1	rostlina
s	s	k7c7	s
názvem	název	k1gInSc7	název
Thymus	Thymus	k1gInSc4	Thymus
zmiňovány	zmiňovat	k5eAaImNgInP	zmiňovat
již	již	k9	již
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
antických	antický	k2eAgMnPc2d1	antický
učenců	učenec	k1gMnPc2	učenec
Dioskorida	Dioskorid	k1gMnSc2	Dioskorid
nebo	nebo	k8xC	nebo
Plinia	Plinium	k1gNnSc2	Plinium
staršího	starší	k1gMnSc2	starší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čeledi	čeleď	k1gFnSc2	čeleď
hluchavkovitých	hluchavkovitý	k2eAgMnPc2d1	hluchavkovitý
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
nejrozsáhlejší	rozsáhlý	k2eAgFnSc2d3	nejrozsáhlejší
podčeledi	podčeleď	k1gFnSc2	podčeleď
Nepetoideae	Nepetoidea	k1gFnSc2	Nepetoidea
<g/>
,	,	kIx,	,
tribu	tribat	k5eAaPmIp1nS	tribat
Menthae	Mentha	k1gMnSc4	Mentha
a	a	k8xC	a
subtribu	subtriba	k1gMnSc4	subtriba
Menthinae	Menthina	k1gMnSc4	Menthina
<g/>
.	.	kIx.	.
</s>
<s>
Lektotypem	Lektotyp	k1gInSc7	Lektotyp
rodu	rod	k1gInSc2	rod
je	být	k5eAaImIp3nS	být
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
tymián	tymián	k1gInSc1	tymián
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gInSc1	Thymus
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
větví	větev	k1gFnSc7	větev
ve	v	k7c6	v
fylogenetickém	fylogenetický	k2eAgInSc6d1	fylogenetický
stromu	strom	k1gInSc6	strom
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dvojice	dvojice	k1gFnSc1	dvojice
rodů	rod	k1gInPc2	rod
Origanum	Origanum	k1gInSc1	Origanum
a	a	k8xC	a
Saccocalyx	Saccocalyx	k1gInSc1	Saccocalyx
<g/>
.	.	kIx.	.
<g/>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
rodu	rod	k1gInSc2	rod
Thymus	Thymus	k1gInSc1	Thymus
je	být	k5eAaImIp3nS	být
neustálená	ustálený	k2eNgFnSc1d1	neustálená
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jen	jen	k9	jen
40	[number]	k4	40
až	až	k9	až
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgNnSc4d1	jiné
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
dokonce	dokonce	k9	dokonce
až	až	k9	až
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
patří	patřit	k5eAaImIp3nS	patřit
botanicky	botanicky	k6eAd1	botanicky
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
komplikovaným	komplikovaný	k2eAgFnPc3d1	komplikovaná
skupinám	skupina	k1gFnPc3	skupina
rostlin	rostlina	k1gFnPc2	rostlina
vinou	vinou	k7c2	vinou
značné	značný	k2eAgFnSc2d1	značná
podobnosti	podobnost	k1gFnSc2	podobnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
vnitrodruhové	vnitrodruhový	k2eAgFnSc2d1	vnitrodruhová
proměnlivosti	proměnlivost	k1gFnSc2	proměnlivost
i	i	k8xC	i
častého	častý	k2eAgNnSc2d1	časté
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
křížení	křížení	k1gNnSc2	křížení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
některých	některý	k3yIgInPc2	některý
taxonomů	taxonom	k1gInPc2	taxonom
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
vedeno	vést	k5eAaImNgNnS	vést
jako	jako	k9	jako
vnitrodruhové	vnitrodruhový	k2eAgInPc1d1	vnitrodruhový
taxony	taxon	k1gInPc1	taxon
druhu	druh	k1gInSc2	druh
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
úzkolistá	úzkolistý	k2eAgFnSc1d1	úzkolistá
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gInSc1	Thymus
serpyllum	serpyllum	k1gInSc1	serpyllum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
květeně	květena	k1gFnSc6	květena
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
obtížně	obtížně	k6eAd1	obtížně
zařaditelné	zařaditelný	k2eAgInPc1d1	zařaditelný
morfotypy	morfotyp	k1gInPc1	morfotyp
s	s	k7c7	s
přechodnými	přechodný	k2eAgInPc7d1	přechodný
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nejobtížnějším	obtížný	k2eAgInSc7d3	nejobtížnější
taxonem	taxon	k1gInSc7	taxon
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
olysalá	olysalý	k2eAgFnSc1d1	olysalá
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gInSc1	Thymus
glabrescens	glabrescens	k1gInSc1	glabrescens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překrývající	překrývající	k2eAgMnSc1d1	překrývající
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
ve	v	k7c6	v
variabilitě	variabilita	k1gFnSc6	variabilita
s	s	k7c7	s
mateřídouškou	mateřídouška	k1gFnSc7	mateřídouška
panonskou	panonský	k2eAgFnSc7d1	Panonská
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gMnSc1	Thymus
pannonicus	pannonicus	k1gMnSc1	pannonicus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Mateřídoušky	mateřídouška	k1gFnPc1	mateřídouška
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
účinných	účinný	k2eAgFnPc2d1	účinná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
silice	silice	k1gFnSc1	silice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
hořčiny	hořčina	k1gFnSc2	hořčina
(	(	kIx(	(
<g/>
serpyllin	serpyllin	k1gInSc1	serpyllin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
flavonoidy	flavonoid	k1gInPc1	flavonoid
<g/>
,	,	kIx,	,
flavony	flavon	k1gInPc1	flavon
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc1	tříslovina
<g/>
,	,	kIx,	,
saponiny	saponin	k1gInPc1	saponin
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
organické	organický	k2eAgFnPc1d1	organická
kyseliny	kyselina	k1gFnPc1	kyselina
a	a	k8xC	a
minerální	minerální	k2eAgFnPc1d1	minerální
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
silici	silice	k1gFnSc6	silice
mateřídoušky	mateřídouška	k1gFnSc2	mateřídouška
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
aromatické	aromatický	k2eAgInPc1d1	aromatický
terpenoidy	terpenoid	k1gInPc1	terpenoid
<g/>
,	,	kIx,	,
např.	např.	kA	např.
thymol	thymol	k1gInSc1	thymol
<g/>
,	,	kIx,	,
linalool	linalool	k1gInSc1	linalool
<g/>
,	,	kIx,	,
kafr	kafr	k1gInSc1	kafr
<g/>
,	,	kIx,	,
cymol	cymol	k1gInSc1	cymol
<g/>
,	,	kIx,	,
borneol	borneol	k1gInSc1	borneol
<g/>
,	,	kIx,	,
geraniol	geraniol	k1gInSc1	geraniol
<g/>
,	,	kIx,	,
alfa-	alfa-	k?	alfa-
a	a	k8xC	a
beta	beta	k1gNnSc7	beta
pinen	pinen	k1gInSc1	pinen
či	či	k8xC	či
karvakrol	karvakrol	k1gInSc1	karvakrol
<g/>
.	.	kIx.	.
</s>
<s>
Silice	silice	k1gFnSc1	silice
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
především	především	k9	především
v	v	k7c6	v
přisedlých	přisedlý	k2eAgFnPc6d1	přisedlá
siličných	siličný	k2eAgFnPc6d1	siličná
žlázkách	žlázka	k1gFnPc6	žlázka
<g/>
,	,	kIx,	,
pokrývajících	pokrývající	k2eAgFnPc6d1	pokrývající
většinu	většina	k1gFnSc4	většina
nadzemní	nadzemní	k2eAgFnPc1d1	nadzemní
části	část	k1gFnPc1	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
;	;	kIx,	;
nejvíce	hodně	k6eAd3	hodně
jich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
kalichů	kalich	k1gInPc2	kalich
a	a	k8xC	a
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
silic	silice	k1gFnPc2	silice
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
velká	velký	k2eAgFnSc1d1	velká
vnitrodruhová	vnitrodruhový	k2eAgFnSc1d1	vnitrodruhová
variabilita	variabilita	k1gFnSc1	variabilita
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc1d1	projevující
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
rozdílné	rozdílný	k2eAgFnSc6d1	rozdílná
vůni	vůně	k1gFnSc6	vůně
různých	různý	k2eAgFnPc2d1	různá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
;	;	kIx,	;
obsah	obsah	k1gInSc1	obsah
produkovaných	produkovaný	k2eAgInPc2d1	produkovaný
sekundárních	sekundární	k2eAgInPc2d1	sekundární
metabolitů	metabolit	k1gInPc2	metabolit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
flavonoidů	flavonoid	k1gInPc2	flavonoid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitým	důležitý	k2eAgInSc7d1	důležitý
rozlišovacím	rozlišovací	k2eAgInSc7d1	rozlišovací
druhovým	druhový	k2eAgInSc7d1	druhový
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Léčitelství	léčitelství	k1gNnPc4	léčitelství
a	a	k8xC	a
gastronomie	gastronomie	k1gFnPc4	gastronomie
===	===	k?	===
</s>
</p>
<p>
<s>
Mateřídoušky	mateřídouška	k1gFnPc1	mateřídouška
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
starým	starý	k2eAgFnPc3d1	stará
léčivým	léčivý	k2eAgFnPc3d1	léčivá
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
;	;	kIx,	;
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc1	dva
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Droga	droga	k1gFnSc1	droga
má	mít	k5eAaImIp3nS	mít
výrazné	výrazný	k2eAgInPc4d1	výrazný
antiseptické	antiseptický	k2eAgInPc4d1	antiseptický
<g/>
,	,	kIx,	,
dezinfekční	dezinfekční	k2eAgInPc4d1	dezinfekční
a	a	k8xC	a
uklidňující	uklidňující	k2eAgInPc4d1	uklidňující
účinky	účinek	k1gInPc4	účinek
<g/>
;	;	kIx,	;
navozuje	navozovat	k5eAaImIp3nS	navozovat
příjemný	příjemný	k2eAgInSc1d1	příjemný
spánek	spánek	k1gInSc1	spánek
<g/>
,	,	kIx,	,
pománá	pománý	k2eAgFnSc1d1	pománá
proti	proti	k7c3	proti
stresu	stres	k1gInSc3	stres
<g/>
,	,	kIx,	,
depresi	deprese	k1gFnSc6	deprese
a	a	k8xC	a
migréně	migréna	k1gFnSc6	migréna
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
proti	proti	k7c3	proti
kašli	kašel	k1gInSc3	kašel
<g/>
,	,	kIx,	,
zahlenění	zahlenění	k1gNnSc3	zahlenění
<g/>
,	,	kIx,	,
zažívacím	zažívací	k2eAgFnPc3d1	zažívací
a	a	k8xC	a
žaludečním	žaludeční	k2eAgFnPc3d1	žaludeční
potížím	potíž	k1gFnPc3	potíž
a	a	k8xC	a
nadýmání	nadýmání	k1gNnPc2	nadýmání
<g/>
,	,	kIx,	,
též	též	k9	též
k	k	k7c3	k
ošetření	ošetření	k1gNnSc3	ošetření
nehojících	hojící	k2eNgFnPc2d1	nehojící
se	se	k3xPyFc4	se
ran	rána	k1gFnPc2	rána
a	a	k8xC	a
vředů	vřed	k1gInPc2	vřed
či	či	k8xC	či
gynekologickým	gynekologický	k2eAgInPc3d1	gynekologický
výplachům	výplach	k1gInPc3	výplach
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
koření	koření	k1gNnSc2	koření
se	se	k3xPyFc4	se
hodí	hodit	k5eAaPmIp3nS	hodit
k	k	k7c3	k
masitým	masitý	k2eAgInPc3d1	masitý
pokrmům	pokrm	k1gInPc3	pokrm
z	z	k7c2	z
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ke	k	k7c3	k
sladkým	sladký	k2eAgNnPc3d1	sladké
jídlům	jídlo	k1gNnPc3	jídlo
a	a	k8xC	a
koláčů	koláč	k1gInPc2	koláč
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
bouquet	bouquet	k1gNnSc2	bouquet
garni	gareň	k1gFnSc3	gareň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
připravovat	připravovat	k5eAaImF	připravovat
bylinkové	bylinkový	k2eAgInPc1d1	bylinkový
oleje	olej	k1gInPc1	olej
<g/>
,	,	kIx,	,
octy	ocet	k1gInPc1	ocet
<g/>
,	,	kIx,	,
másla	máslo	k1gNnPc1	máslo
a	a	k8xC	a
marinády	marináda	k1gFnPc1	marináda
<g/>
.	.	kIx.	.
</s>
<s>
Destilovaný	destilovaný	k2eAgInSc1d1	destilovaný
esenciální	esenciální	k2eAgInSc1d1	esenciální
olej	olej	k1gInSc1	olej
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
aromaterapii	aromaterapie	k1gFnSc6	aromaterapie
a	a	k8xC	a
kosmetice	kosmetika	k1gFnSc3	kosmetika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
zubních	zubní	k2eAgFnPc2d1	zubní
past	pasta	k1gFnPc2	pasta
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
prostředků	prostředek	k1gInPc2	prostředek
ústní	ústní	k2eAgFnSc2d1	ústní
hygieny	hygiena	k1gFnSc2	hygiena
<g/>
,	,	kIx,	,
pot-pourri	potourr	k1gFnSc2	pot-pourr
nebo	nebo	k8xC	nebo
osvěžovačů	osvěžovač	k1gInPc2	osvěžovač
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Potvrzeny	potvrzen	k2eAgFnPc1d1	potvrzena
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gNnSc4	jeho
antibakteriální	antibakteriální	k2eAgFnPc1d1	antibakteriální
<g/>
,	,	kIx,	,
fungicidní	fungicidní	k2eAgFnPc1d1	fungicidní
<g/>
,	,	kIx,	,
spazmolytické	spazmolytický	k2eAgFnPc1d1	spazmolytický
a	a	k8xC	a
antioxidační	antioxidační	k2eAgInPc1d1	antioxidační
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
<g/>
Sbírá	sbírat	k5eAaImIp3nS	sbírat
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
nať	nať	k1gFnSc1	nať
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
zdřevnatělých	zdřevnatělý	k2eAgInPc2d1	zdřevnatělý
stonků	stonek	k1gInPc2	stonek
<g/>
)	)	kIx)	)
za	za	k7c2	za
plného	plný	k2eAgInSc2d1	plný
květu	květ	k1gInSc2	květ
<g/>
,	,	kIx,	,
ideálně	ideálně	k6eAd1	ideálně
za	za	k7c2	za
slunečného	slunečný	k2eAgNnSc2d1	slunečné
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
čajů	čaj	k1gInPc2	čaj
<g/>
,	,	kIx,	,
sirupů	sirup	k1gInPc2	sirup
nebo	nebo	k8xC	nebo
tinktur	tinktura	k1gFnPc2	tinktura
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neměla	mít	k5eNaImAgFnS	mít
užívat	užívat	k5eAaImF	užívat
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
obsahu	obsah	k1gInSc2	obsah
mírně	mírně	k6eAd1	mírně
toxického	toxický	k2eAgInSc2d1	toxický
thymolu	thymol	k1gInSc2	thymol
<g/>
;	;	kIx,	;
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
používání	používání	k1gNnSc1	používání
a	a	k8xC	a
předávkování	předávkování	k1gNnSc1	předávkování
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
funkce	funkce	k1gFnSc2	funkce
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Okrasné	okrasný	k2eAgNnSc1d1	okrasné
zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
a	a	k8xC	a
kultivary	kultivar	k1gInPc1	kultivar
mateřídoušek	mateřídouška	k1gFnPc2	mateřídouška
patří	patřit	k5eAaImIp3nP	patřit
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
půvabný	půvabný	k2eAgInSc4d1	půvabný
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
vůni	vůně	k1gFnSc4	vůně
a	a	k8xC	a
léčivé	léčivý	k2eAgInPc4d1	léčivý
účinky	účinek	k1gInPc4	účinek
k	k	k7c3	k
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
zahradním	zahradní	k2eAgFnPc3d1	zahradní
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pěstování	pěstování	k1gNnSc6	pěstování
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
lehčí	lehký	k2eAgFnSc4d2	lehčí
<g/>
,	,	kIx,	,
propustnou	propustný	k2eAgFnSc4d1	propustná
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
drenážovanou	drenážovaný	k2eAgFnSc4d1	drenážovaná
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
stanoviště	stanoviště	k1gNnPc4	stanoviště
na	na	k7c6	na
plném	plný	k2eAgNnSc6d1	plné
slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
pěstovat	pěstovat	k5eAaImF	pěstovat
jako	jako	k9	jako
skalničky	skalnička	k1gFnPc4	skalnička
<g/>
,	,	kIx,	,
na	na	k7c6	na
suchých	suchý	k2eAgFnPc6d1	suchá
zídkách	zídka	k1gFnPc6	zídka
<g/>
,	,	kIx,	,
terasách	terasa	k1gFnPc6	terasa
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kvetoucí	kvetoucí	k2eAgInPc1d1	kvetoucí
lemy	lem	k1gInPc1	lem
záhonů	záhon	k1gInPc2	záhon
a	a	k8xC	a
zahradních	zahradní	k2eAgFnPc2d1	zahradní
cestiček	cestička	k1gFnPc2	cestička
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
pokryvnou	pokryvný	k2eAgFnSc4d1	pokryvná
rostlinu	rostlina	k1gFnSc4	rostlina
kamenných	kamenný	k2eAgInPc2d1	kamenný
chodníčků	chodníček	k1gInPc2	chodníček
nebo	nebo	k8xC	nebo
k	k	k7c3	k
porůstání	porůstání	k1gNnSc3	porůstání
obrubníků	obrubník	k1gInPc2	obrubník
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
často	často	k6eAd1	často
pěstovaným	pěstovaný	k2eAgInPc3d1	pěstovaný
druhům	druh	k1gInPc3	druh
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
bělokvětá	bělokvětý	k2eAgFnSc1d1	bělokvětá
(	(	kIx(	(
<g/>
Thymus	Thymus	k1gInSc1	Thymus
drucei	drucei	k1gNnSc1	drucei
<g/>
)	)	kIx)	)
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kříženec	kříženec	k1gMnSc1	kříženec
Thymus	Thymus	k1gMnSc1	Thymus
x	x	k?	x
citriodorus	citriodorus	k1gMnSc1	citriodorus
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
barevných	barevný	k2eAgInPc6d1	barevný
kultivarech	kultivar	k1gInPc6	kultivar
<g/>
.	.	kIx.	.
</s>
<s>
Efektivně	efektivně	k6eAd1	efektivně
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
množit	množit	k5eAaImF	množit
dělením	dělení	k1gNnSc7	dělení
trsů	trs	k1gInPc2	trs
<g/>
,	,	kIx,	,
množí	množit	k5eAaImIp3nP	množit
se	se	k3xPyFc4	se
také	také	k9	také
výsevem	výsev	k1gInSc7	výsev
semen	semeno	k1gNnPc2	semeno
nebo	nebo	k8xC	nebo
řízkováním	řízkování	k1gNnSc7	řízkování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Včelařství	včelařství	k1gNnSc2	včelařství
===	===	k?	===
</s>
</p>
<p>
<s>
Mateřídoušky	mateřídouška	k1gFnPc1	mateřídouška
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
medonosným	medonosný	k2eAgFnPc3d1	medonosná
rostlinám	rostlina	k1gFnPc3	rostlina
poskytujícím	poskytující	k2eAgFnPc3d1	poskytující
dobrou	dobrý	k2eAgFnSc4d1	dobrá
pastvu	pastva	k1gFnSc4	pastva
včelám	včela	k1gFnPc3	včela
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
produkují	produkovat	k5eAaImIp3nP	produkovat
mnoho	mnoho	k6eAd1	mnoho
nektaru	nektar	k1gInSc2	nektar
v	v	k7c6	v
prstenci	prstenec	k1gInSc6	prstenec
žlázek	žlázka	k1gFnPc2	žlázka
okolo	okolo	k7c2	okolo
základny	základna	k1gFnSc2	základna
čnělky	čnělka	k1gFnSc2	čnělka
<g/>
.	.	kIx.	.
</s>
<s>
Druhový	druhový	k2eAgInSc4d1	druhový
mateřídouškový	mateřídouškový	k2eAgInSc4d1	mateřídouškový
med	med	k1gInSc4	med
je	být	k5eAaImIp3nS	být
vyráběn	vyrábět	k5eAaImNgMnS	vyrábět
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
řeckém	řecký	k2eAgInSc6d1	řecký
ostrově	ostrov	k1gInSc6	ostrov
Hyméttu	Hymétt	k1gInSc2	Hymétt
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
aromatický	aromatický	k2eAgInSc1d1	aromatický
<g/>
,	,	kIx,	,
jantarové	jantarový	k2eAgFnSc2d1	jantarová
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
fruktózy	fruktóza	k1gFnSc2	fruktóza
dlouho	dlouho	k6eAd1	dlouho
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
poezii	poezie	k1gFnSc6	poezie
například	například	k6eAd1	například
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
básni	báseň	k1gFnSc6	báseň
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Kytice	kytice	k1gFnSc2	kytice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Seiferta	Seifert	k1gMnSc2	Seifert
<g/>
;	;	kIx,	;
vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ztotožňována	ztotožňovat	k5eAaImNgFnS	ztotožňovat
se	s	k7c7	s
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
na	na	k7c4	na
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Rozkvetlý	rozkvetlý	k2eAgInSc1d1	rozkvetlý
tymián	tymián	k1gInSc1	tymián
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
objevujícím	objevující	k2eAgMnSc7d1	objevující
se	se	k3xPyFc4	se
např.	např.	kA	např.
ve	v	k7c6	v
vzpomínkové	vzpomínkový	k2eAgFnSc6d1	vzpomínková
knize	kniha	k1gFnSc6	kniha
Marcela	Marcela	k1gFnSc1	Marcela
Pagnola	Pagnola	k1gFnSc1	Pagnola
Jak	jak	k6eAd1	jak
voní	vonět	k5eAaImIp3nS	vonět
tymián	tymián	k1gInSc1	tymián
<g/>
.	.	kIx.	.
</s>
<s>
Pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
Panně	Panna	k1gFnSc6	Panna
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
lázni	lázeň	k1gFnSc6	lázeň
z	z	k7c2	z
mateřídoušky	mateřídouška	k1gFnSc2	mateřídouška
ošetřovala	ošetřovat	k5eAaImAgNnP	ošetřovat
Ježíškova	Ježíškův	k2eAgNnPc1d1	Ježíškovo
roucha	roucho	k1gNnPc1	roucho
a	a	k8xC	a
plenky	plenka	k1gFnPc1	plenka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vydávaly	vydávat	k5eAaImAgInP	vydávat
líbeznou	líbezný	k2eAgFnSc4d1	líbezná
<g/>
,	,	kIx,	,
ducha	duch	k1gMnSc4	duch
povznášející	povznášející	k2eAgFnSc4d1	povznášející
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
Legendách	legenda	k1gFnPc6	legenda
o	o	k7c6	o
květinách	květina	k1gFnPc6	květina
František	František	k1gMnSc1	František
Kubka	Kubka	k1gMnSc1	Kubka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
její	její	k3xOp3gFnSc6	její
všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
oblibě	obliba	k1gFnSc6	obliba
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
množství	množství	k1gNnSc1	množství
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
školek	školka	k1gFnPc2	školka
<g/>
,	,	kIx,	,
denních	denní	k2eAgNnPc2d1	denní
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
spolků	spolek	k1gInPc2	spolek
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nesou	nést	k5eAaImIp3nP	nést
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Hejný	Hejný	k1gMnSc1	Hejný
S.	S.	kA	S.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Květena	květena	k1gFnSc1	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
306	[number]	k4	306
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
Stahl-Biskup	Stahl-Biskup	k1gMnSc1	Stahl-Biskup
<g/>
,	,	kIx,	,
Francisco	Francisco	k1gMnSc1	Francisco
Sáez	Sáez	k1gMnSc1	Sáez
<g/>
:	:	kIx,	:
Thyme	Thym	k1gInSc5	Thym
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Genus	Genus	k1gMnSc1	Genus
Thymus	Thymus	k1gMnSc1	Thymus
(	(	kIx(	(
<g/>
=	=	kIx~	=
Medicinal	Medicinal	k1gFnSc1	Medicinal
and	and	k?	and
aromatic	aromatice	k1gFnPc2	aromatice
plants	plants	k6eAd1	plants
<g/>
:	:	kIx,	:
industrial	industriat	k5eAaImAgMnS	industriat
profiles	profiles	k1gMnSc1	profiles
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taylor	Taylor	k1gMnSc1	Taylor
&	&	k?	&
Francis	Francis	k1gInSc1	Francis
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
28488	[number]	k4	28488
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Soupis	soupis	k1gInSc1	soupis
druhů	druh	k1gInPc2	druh
na	na	k7c4	na
Biolibu	Bioliba	k1gFnSc4	Bioliba
</s>
</p>
<p>
<s>
Flora	Flora	k1gFnSc1	Flora
of	of	k?	of
China	China	k1gFnSc1	China
<g/>
:	:	kIx,	:
Thymus	Thymus	k1gInSc1	Thymus
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Kew	Kew	k1gFnSc2	Kew
Science	Science	k1gFnSc1	Science
</s>
</p>
