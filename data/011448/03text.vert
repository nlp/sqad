<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
nejstaršího	starý	k2eAgInSc2d3	nejstarší
písemného	písemný	k2eAgInSc2d1	písemný
dokladu	doklad	k1gInSc2	doklad
o	o	k7c6	o
určité	určitý	k2eAgFnSc6d1	určitá
vsi	ves	k1gFnSc6	ves
či	či	k8xC	či
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
hradu	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
objevu	objev	k1gInSc6	objev
<g/>
,	,	kIx,	,
technickém	technický	k2eAgInSc6d1	technický
postupu	postup	k1gInSc6	postup
atp.	atp.	kA	atp.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
datem	datum	k1gNnSc7	datum
ante	ante	k1gFnPc2	ante
quem	quem	k6eAd1	quem
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kromě	kromě	k7c2	kromě
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
písemném	písemný	k2eAgInSc6d1	písemný
dokladu	doklad	k1gInSc6	doklad
přímo	přímo	k6eAd1	přímo
zmíněno	zmíněn	k2eAgNnSc4d1	zmíněno
založení	založení	k1gNnSc4	založení
sledovaného	sledovaný	k2eAgInSc2d1	sledovaný
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
dokládá	dokládat	k5eAaImIp3nS	dokládat
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
objekt	objekt	k1gInSc1	objekt
již	již	k6eAd1	již
existoval	existovat	k5eAaImAgInS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
případě	případ	k1gInSc6	případ
menších	malý	k2eAgInPc2d2	menší
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
i	i	k9	i
značně	značně	k6eAd1	značně
časově	časově	k6eAd1	časově
vzdálena	vzdálit	k5eAaPmNgFnS	vzdálit
od	od	k7c2	od
data	datum	k1gNnSc2	datum
skutečného	skutečný	k2eAgInSc2d1	skutečný
vzniku	vznik	k1gInSc2	vznik
(	(	kIx(	(
<g/>
sto	sto	k4xCgNnSc4	sto
i	i	k8xC	i
více	hodně	k6eAd2	hodně
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
dochovala	dochovat	k5eAaPmAgFnS	dochovat
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
středověkých	středověký	k2eAgFnPc2d1	středověká
listin	listina	k1gFnPc2	listina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
údaje	údaj	k1gInPc4	údaj
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutné	nutný	k2eAgNnSc1d1	nutné
kombinovat	kombinovat	k5eAaImF	kombinovat
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
získanými	získaný	k2eAgNnPc7d1	získané
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
základě	základ	k1gInSc6	základ
slohového	slohový	k2eAgInSc2d1	slohový
rozboru	rozbor	k1gInSc2	rozbor
zachovaných	zachovaný	k2eAgFnPc2d1	zachovaná
stavebních	stavební	k2eAgFnPc2d1	stavební
památek	památka	k1gFnPc2	památka
nebo	nebo	k8xC	nebo
archeologicky	archeologicky	k6eAd1	archeologicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
zpravidla	zpravidla	k6eAd1	zpravidla
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
o	o	k7c6	o
sledovaném	sledovaný	k2eAgInSc6d1	sledovaný
objektu	objekt	k1gInSc6	objekt
žádné	žádný	k3yNgFnSc2	žádný
další	další	k2eAgFnSc2d1	další
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
pochází	pocházet	k5eAaImIp3nS	pocházet
například	například	k6eAd1	například
z	z	k7c2	z
listin	listina	k1gFnPc2	listina
dokládajících	dokládající	k2eAgInPc2d1	dokládající
prodej	prodej	k1gInSc4	prodej
či	či	k8xC	či
jiný	jiný	k2eAgInSc4d1	jiný
převod	převod	k1gInSc4	převod
určitého	určitý	k2eAgInSc2d1	určitý
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Sledovaný	sledovaný	k2eAgInSc1d1	sledovaný
objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
připomenut	připomenut	k2eAgMnSc1d1	připomenut
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
položka	položka	k1gFnSc1	položka
ve	v	k7c6	v
výčtu	výčet	k1gInSc6	výčet
tohoto	tento	k3xDgInSc2	tento
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jazykovědě	jazykověda	k1gFnSc6	jazykověda
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
etymologii	etymologie	k1gFnSc4	etymologie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc4	termín
první	první	k4xOgInSc4	první
doklad	doklad	k1gInSc4	doklad
nebo	nebo	k8xC	nebo
první	první	k4xOgInSc4	první
doložený	doložený	k2eAgInSc4d1	doložený
výskyt	výskyt	k1gInSc4	výskyt
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Erstbeleg	Erstbeleg	k1gInSc1	Erstbeleg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
čas	čas	k1gInSc4	čas
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
nachází	nacházet	k5eAaImIp3nS	nacházet
daný	daný	k2eAgInSc4d1	daný
výraz	výraz	k1gInSc4	výraz
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g />
.	.	kIx.	.
</s>
<s>
první	první	k4xOgInSc1	první
doložený	doložený	k2eAgInSc1d1	doložený
výskyt	výskyt	k1gInSc1	výskyt
slova	slovo	k1gNnSc2	slovo
'	'	kIx"	'
<g/>
rána	ráno	k1gNnSc2	ráno
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
'	'	kIx"	'
<g/>
zranění	zranění	k1gNnPc4	zranění
<g/>
'	'	kIx"	'
×	×	k?	×
'	'	kIx"	'
<g/>
zvuk	zvuk	k1gInSc1	zvuk
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
srov.	srov.	kA	srov.
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
robot	robot	k1gMnSc1	robot
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
'	'	kIx"	'
<g/>
segwayista	segwayista	k1gMnSc1	segwayista
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
lajk	lajk	k6eAd1	lajk
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
youtuber	youtuber	k1gMnSc1	youtuber
<g/>
'	'	kIx"	'
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
