<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
nejstaršího	starý	k2eAgInSc2d3	nejstarší
písemného	písemný	k2eAgInSc2d1	písemný
dokladu	doklad	k1gInSc2	doklad
o	o	k7c6	o
určité	určitý	k2eAgFnSc6d1	určitá
vsi	ves	k1gFnSc6	ves
či	či	k8xC	či
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
hradu	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
objevu	objev	k1gInSc6	objev
<g/>
,	,	kIx,	,
technickém	technický	k2eAgInSc6d1	technický
postupu	postup	k1gInSc6	postup
atp.	atp.	kA	atp.
</s>
