<s>
Listy	list	k1gInPc1	list
čajovníku	čajovník	k1gInSc2	čajovník
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
od	od	k7c2	od
tří	tři	k4xCgInPc2	tři
do	do	k7c2	do
pětadvaceti	pětadvacet	k4xCc2	pětadvacet
centimetrů	centimetr	k1gInPc2	centimetr
a	a	k8xC	a
široké	široký	k2eAgFnPc1d1	široká
mezi	mezi	k7c7	mezi
jedním	jeden	k4xCgNnSc7	jeden
a	a	k8xC	a
deseti	deset	k4xCc7	deset
centimetry	centimetr	k1gInPc7	centimetr
<g/>
,	,	kIx,	,
dospělé	dospělý	k2eAgInPc1d1	dospělý
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
tlusté	tlustý	k2eAgFnPc1d1	tlustá
<g/>
,	,	kIx,	,
hladké	hladký	k2eAgFnPc1d1	hladká
a	a	k8xC	a
kožnaté	kožnatý	k2eAgFnPc1d1	kožnatá
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
řapíkem	řapík	k1gInSc7	řapík
<g/>
.	.	kIx.	.
</s>
