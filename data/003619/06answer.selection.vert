<s>
Termín	termín	k1gInSc1	termín
e-mail	eail	k1gInSc1	e-mail
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
internetový	internetový	k2eAgInSc4d1	internetový
systém	systém	k1gInSc4	systém
elektronické	elektronický	k2eAgFnSc2d1	elektronická
pošty	pošta	k1gFnSc2	pošta
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
protokolu	protokol	k1gInSc6	protokol
SMTP	SMTP	kA	SMTP
(	(	kIx(	(
<g/>
Simple	Simple	k1gFnSc1	Simple
Mail	mail	k1gInSc1	mail
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
intranetové	intranetový	k2eAgInPc4d1	intranetový
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
posílat	posílat	k5eAaImF	posílat
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
zprávy	zpráva	k1gFnSc2	zpráva
uživatelům	uživatel	k1gMnPc3	uživatel
uvnitř	uvnitř	k7c2	uvnitř
jedné	jeden	k4xCgFnSc2	jeden
společnosti	společnost	k1gFnSc2	společnost
nebo	nebo	k8xC	nebo
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
tyto	tento	k3xDgInPc1	tento
systémy	systém	k1gInPc1	systém
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
nestandardní	standardní	k2eNgInPc1d1	nestandardní
protokoly	protokol	k1gInPc1	protokol
<g/>
,	,	kIx,	,
mívají	mívat	k5eAaImIp3nP	mívat
ovšem	ovšem	k9	ovšem
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jim	on	k3xPp3gMnPc3	on
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
posílat	posílat	k5eAaImF	posílat
a	a	k8xC	a
přijímat	přijímat	k5eAaImF	přijímat
e-maily	eail	k1gInPc4	e-mail
z	z	k7c2	z
internetu	internet	k1gInSc2	internet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
