<s>
XVIII	XVIII	kA
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Číny	Čína	k1gFnSc2
</s>
<s>
XVIII	XVIII	kA
<g/>
.	.	kIx.
sjezd	sjezd	k1gInSc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
pchin-jinem	pchin-jin	k1gInSc7
Zhō	Zhō	k1gFnSc2
Gò	Gò	k1gInSc1
dì	dì	k?
quánguódà	quánguódà	k?
<g/>
,	,	kIx,
znaky	znak	k1gInPc4
zjednodušené	zjednodušený	k2eAgFnSc2d1
中	中	k?
<g/>
)	)	kIx)
proběhl	proběhnout	k5eAaPmAgInS
ve	v	k7c6
Pekingu	Peking	k1gInSc6
ve	v	k7c6
dnech	den	k1gInPc6
8	#num#	k4
<g/>
.	.	kIx.
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sjezdu	sjezd	k1gInSc2
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
2270	#num#	k4
řádných	řádný	k2eAgMnPc2d1
delegátů	delegát	k1gMnPc2
a	a	k8xC
57	#num#	k4
speciálních	speciální	k2eAgMnPc2d1
delegátů	delegát	k1gMnPc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
zastupujících	zastupující	k2eAgMnPc2d1
přes	přes	k7c4
82	#num#	k4
milionů	milion	k4xCgInPc2
členů	člen	k1gMnPc2
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Politickou	politický	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
ústředního	ústřední	k2eAgInSc2d1
výboru	výbor	k1gInSc2
přednesl	přednést	k5eAaPmAgMnS
odstupující	odstupující	k2eAgMnSc1d1
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
Chu	Chu	k1gMnSc1
Ťin-tchao	Ťin-tchao	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sjezd	sjezd	k1gInSc1
přijal	přijmout	k5eAaPmAgInS
novou	nový	k2eAgFnSc4d1
reakci	reakce	k1gFnSc4
stanov	stanova	k1gFnPc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gInPc6
nově	nově	k6eAd1
objevilo	objevit	k5eAaPmAgNnS
věta	věta	k1gFnSc1
o	o	k7c6
ustavení	ustavení	k1gNnSc6
„	„	k?
<g/>
socialistického	socialistický	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
s	s	k7c7
čínskými	čínský	k2eAgInPc7d1
rysy	rys	k1gInPc7
<g/>
“	“	k?
v	v	k7c6
čínské	čínský	k2eAgFnSc6d1
lidové	lidový	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
potvrzen	potvrdit	k5eAaPmNgInS
obranný	obranný	k2eAgInSc1d1
charakter	charakter	k1gInSc1
čínské	čínský	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
doktríny	doktrína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sjezd	sjezd	k1gInSc1
zvolil	zvolit	k5eAaPmAgInS
18	#num#	k4
<g/>
.	.	kIx.
ústřední	ústřední	k2eAgInSc1d1
výbor	výbor	k1gInSc1
o	o	k7c6
205	#num#	k4
členech	člen	k1gInPc6
a	a	k8xC
171	#num#	k4
kandidátech	kandidát	k1gMnPc6
<g/>
,	,	kIx,
z	z	k7c2
nich	on	k3xPp3gInPc2
184	#num#	k4
nových	nový	k2eAgInPc2d1
<g/>
,	,	kIx,
a	a	k8xC
ústřední	ústřední	k2eAgFnSc4d1
komisi	komise	k1gFnSc4
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
disciplíny	disciplína	k1gFnSc2
o	o	k7c6
130	#num#	k4
členech	člen	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgInSc1d1
výbor	výbor	k1gInSc1
poté	poté	k6eAd1
zvolil	zvolit	k5eAaPmAgInS
18	#num#	k4
<g/>
.	.	kIx.
politbyro	politbyro	k1gNnSc1
o	o	k7c6
pětadvaceti	pětadvacet	k4xCc6
členech	člen	k1gMnPc6
a	a	k8xC
sedmičlenný	sedmičlenný	k2eAgInSc1d1
sekretariát	sekretariát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
běžnému	běžný	k2eAgNnSc3d1
řízení	řízení	k1gNnSc3
strany	strana	k1gFnSc2
vybral	vybrat	k5eAaPmAgInS
ze	z	k7c2
členů	člen	k1gMnPc2
politbyra	politbyro	k1gNnSc2
sedmičlenný	sedmičlenný	k2eAgInSc1d1
stálý	stálý	k2eAgInSc1d1
výbor	výbor	k1gInSc1
(	(	kIx(
<g/>
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
<g/>
,	,	kIx,
Li	li	k8xS
Kche-čchiang	Kche-čchiang	k1gMnSc1
<g/>
,	,	kIx,
Čang	Čang	k1gMnSc1
Te-ťiang	Te-ťiang	k1gMnSc1
<g/>
,	,	kIx,
Jü	Jü	k1gMnSc1
Čeng-šeng	Čeng-šeng	k1gMnSc1
<g/>
,	,	kIx,
Liou	Lious	k1gInSc2
Jün-šan	Jün-šana	k1gFnPc2
<g/>
,	,	kIx,
Wang	Wang	k1gMnSc1
Čchi-šan	Čchi-šan	k1gMnSc1
a	a	k8xC
Čang	Čang	k1gMnSc1
Kao	Kao	k1gFnSc2
<g/>
-li	-li	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
členy	člen	k1gInPc7
členy	člen	k1gMnPc4
politbyra	politbyro	k1gNnPc1
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
Ma	Ma	k1gMnSc1
Kchaj	Kchaj	k1gMnSc1
<g/>
,	,	kIx,
Wang	Wang	k1gMnSc1
Chu-ning	Chu-ning	k1gInSc4
<g/>
,	,	kIx,
Liou	Lioa	k1gFnSc4
Jen-tung	Jen-tung	k1gInSc4
<g/>
,	,	kIx,
Liou	Lioa	k1gFnSc4
Čchi-pao	Čchi-pao	k1gMnSc1
<g/>
,	,	kIx,
Sü	Sü	k1gMnSc1
Čchi-liang	Čchi-liang	k1gMnSc1
<g/>
,	,	kIx,
Sun	Sun	kA
Čchun-lan	Čchun-lan	k1gInSc1
<g/>
,	,	kIx,
Sun	Sun	kA
Čeng-cchaj	Čeng-cchaj	k1gFnSc4
<g/>
,	,	kIx,
Li	li	k8xS
Ťien-kuo	Ťien-kuo	k6eAd1
<g/>
,	,	kIx,
Li	li	k8xS
Jüan-čchao	Jüan-čchao	k1gMnSc1
<g/>
,	,	kIx,
Wang	Wang	k1gMnSc1
Jang	Jang	k1gMnSc1
<g/>
,	,	kIx,
Čang	Čang	k1gMnSc1
Čchun-sien	Čchun-sina	k1gFnPc2
<g/>
,	,	kIx,
Fan	Fana	k1gFnPc2
Čchang-lung	Čchang-lunga	k1gFnPc2
<g/>
,	,	kIx,
Meng	Menga	k1gFnPc2
Ťien-ču	Ťien-čus	k1gInSc2
<g/>
,	,	kIx,
Čao	čao	k0
Le-ťi	Le-ť	k1gMnSc5
<g/>
,	,	kIx,
Chu	Chu	k1gMnSc2
Čchun-chua	Čchun-chuus	k1gMnSc2
<g/>
,	,	kIx,
Li	li	k8xS
Čan-šu	Čan-šu	k1gMnSc1
<g/>
,	,	kIx,
Kuo	Kuo	k1gMnSc1
Ťin-lung	Ťin-lung	k1gMnSc1
a	a	k8xC
Chan	Chan	k1gMnSc1
Čeng	Čeng	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
tajemníkem	tajemník	k1gMnSc7
disciplinární	disciplinární	k2eAgFnSc2d1
komise	komise	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Wang	Wang	k1gInSc1
Čchi-šan	Čchi-šany	k1gInPc2
a	a	k8xC
předsedou	předseda	k1gMnSc7
ústřední	ústřední	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
proběhlo	proběhnout	k5eAaPmAgNnS
předání	předání	k1gNnSc1
moci	moc	k1gFnSc2
nové	nový	k2eAgFnSc2d1
generaci	generace	k1gFnSc4
vedení	vedení	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
novým	nový	k2eAgMnSc7d1
generálním	generální	k2eAgMnSc7d1
tajemníkem	tajemník	k1gMnSc7
(	(	kIx(
<g/>
a	a	k8xC
od	od	k7c2
jara	jaro	k1gNnSc2
2003	#num#	k4
prezidentem	prezident	k1gMnSc7
<g/>
)	)	kIx)
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
dosavadní	dosavadní	k2eAgMnSc1d1
první	první	k4xOgMnSc1
tajemník	tajemník	k1gMnSc1
sekretariátu	sekretariát	k1gInSc2
a	a	k8xC
viceprezident	viceprezident	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
a	a	k8xC
(	(	kIx(
<g/>
na	na	k7c6
jaře	jaro	k1gNnSc6
2003	#num#	k4
<g/>
)	)	kIx)
novým	nový	k2eAgMnSc7d1
premiérem	premiér	k1gMnSc7
dosavadní	dosavadní	k2eAgMnSc1d1
první	první	k4xOgMnSc1
vicepremiér	vicepremiér	k1gMnSc1
Li	li	k8xS
Kche-čchiang	Kche-čchiang	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatních	ostatní	k2eAgMnPc2d1
pět	pět	k4xCc1
členů	člen	k1gMnPc2
členů	člen	k1gMnPc2
stálého	stálý	k2eAgInSc2d1
výboru	výbor	k1gInSc2
byli	být	k5eAaImAgMnP
dosud	dosud	k6eAd1
„	„	k?
<g/>
pouze	pouze	k6eAd1
<g/>
“	“	k?
členy	člen	k1gInPc4
politbyra	politbyro	k1gNnSc2
<g/>
.	.	kIx.
kromě	kromě	k7c2
nich	on	k3xPp3gMnPc2
v	v	k7c6
radikálně	radikálně	k6eAd1
obměněném	obměněný	k2eAgNnSc6d1
politbyru	politbyro	k1gNnSc6
zůstali	zůstat	k5eAaPmAgMnP
pouze	pouze	k6eAd1
Liou	Lio	k1gMnSc3
Jen-tung	Jen-tunga	k1gFnPc2
<g/>
,	,	kIx,
Li	li	k8xS
Jüan-čchao	Jüan-čchao	k1gMnSc1
a	a	k8xC
Wang	Wang	k1gMnSc1
Jang	Jang	k1gMnSc1
<g/>
,	,	kIx,
zbylých	zbylý	k2eAgMnPc2d1
patnáct	patnáct	k4xCc1
zasedalo	zasedat	k5eAaImAgNnS
v	v	k7c6
politbyru	politbyro	k1gNnSc6
poprvé	poprvé	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Speciální	speciální	k2eAgMnPc1d1
delegáti	delegát	k1gMnPc1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
bývalí	bývalý	k2eAgMnPc1d1
vysocí	vysoký	k2eAgMnPc1d1
straničtí	stranický	k2eAgMnPc1d1
funkcionáři	funkcionář	k1gMnPc1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
plná	plný	k2eAgNnPc1d1
práva	právo	k1gNnPc1
delegátů	delegát	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
18	#num#	k4
<g/>
th	th	k?
National	National	k1gFnSc2
Congress	Congressa	k1gFnPc2
of	of	k?
the	the	k?
Communist	Communist	k1gInSc1
Party	party	k1gFnSc1
of	of	k?
China	China	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Číny	Čína	k1gFnSc2
Ideologie	ideologie	k1gFnSc1
a	a	k8xC
politika	politika	k1gFnSc1
</s>
<s>
marxismus-leninismus	marxismus-leninismus	k1gInSc1
•	•	k?
maoismus	maoismus	k1gInSc1
•	•	k?
socialismus	socialismus	k1gInSc1
s	s	k7c7
čínskými	čínský	k2eAgInPc7d1
rysy	rys	k1gInPc7
Ústřední	ústřední	k2eAgFnSc2d1
orgány	orgán	k1gInPc7
</s>
<s>
sjezd	sjezd	k1gInSc1
•	•	k?
ústřední	ústřední	k2eAgInSc1d1
výbor	výbor	k1gInSc1
•	•	k?
ústřední	ústřední	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
disciplíny	disciplína	k1gFnSc2
(	(	kIx(
<g/>
tajemník	tajemník	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
ústřední	ústřední	k2eAgFnSc2d1
poradní	poradní	k2eAgFnSc2d1
komise	komise	k1gFnSc2
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
Ústřední	ústřední	k2eAgInSc1d1
výbor	výbor	k1gInSc1
</s>
<s>
orgány	orgán	k1gInPc1
ÚV	ÚV	kA
</s>
<s>
předseda	předseda	k1gMnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
místopředsedové	místopředseda	k1gMnPc1
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
stálý	stálý	k2eAgInSc4d1
výbor	výbor	k1gInSc4
politbyra	politbyro	k1gNnSc2
•	•	k?
politbyro	politbyro	k1gNnSc4
•	•	k?
sekretariát	sekretariát	k1gInSc1
ÚV	ÚV	kA
•	•	k?
ústřední	ústřední	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
komise	komise	k1gFnSc1
(	(	kIx(
<g/>
předseda	předseda	k1gMnSc1
<g/>
)	)	kIx)
aparát	aparát	k1gInSc1
ÚV	ÚV	kA
</s>
<s>
oddělení	oddělení	k1gNnSc1
</s>
<s>
organizační	organizační	k2eAgFnPc4d1
•	•	k?
propagandy	propaganda	k1gFnSc2
•	•	k?
jednotné	jednotný	k2eAgFnSc2d1
fronty	fronta	k1gFnSc2
•	•	k?
mezinárodních	mezinárodní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
komise	komise	k1gFnSc2
</s>
<s>
duchovní	duchovní	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
finanční	finanční	k2eAgFnSc1d1
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
•	•	k?
institucionální	institucionální	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
•	•	k?
komplexního	komplexní	k2eAgNnSc2d1
prohloubení	prohloubení	k1gNnSc2
reforem	reforma	k1gFnPc2
•	•	k?
kyberprostoru	kyberprostor	k1gInSc3
•	•	k?
národní	národní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
•	•	k?
politická	politický	k2eAgFnSc1d1
a	a	k8xC
právní	právní	k2eAgFnSc1d1
•	•	k?
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
další	další	k2eAgFnSc2d1
</s>
<s>
hlavní	hlavní	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
•	•	k?
politický	politický	k2eAgInSc1d1
výzkumný	výzkumný	k2eAgInSc1d1
úřad	úřad	k1gInSc1
•	•	k?
stranická	stranický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Žen-min	Žen-min	k1gInSc1
ž	ž	k?
<g/>
’	’	k?
<g/>
-pao	-pao	k6eAd1
</s>
<s>
volební	volební	k2eAgNnSc4d1
období	období	k1gNnSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
•	•	k?
2	#num#	k4
<g/>
.	.	kIx.
•	•	k?
3	#num#	k4
<g/>
.	.	kIx.
•	•	k?
4	#num#	k4
<g/>
.	.	kIx.
•	•	k?
5	#num#	k4
<g/>
.	.	kIx.
•	•	k?
6	#num#	k4
<g/>
.	.	kIx.
•	•	k?
7	#num#	k4
<g/>
.	.	kIx.
•	•	k?
8	#num#	k4
<g/>
.	.	kIx.
•	•	k?
9	#num#	k4
<g/>
.	.	kIx.
•	•	k?
10	#num#	k4
<g/>
.	.	kIx.
•	•	k?
11	#num#	k4
<g/>
.	.	kIx.
•	•	k?
12	#num#	k4
<g/>
.	.	kIx.
•	•	k?
13	#num#	k4
<g/>
.	.	kIx.
•	•	k?
14	#num#	k4
<g/>
.	.	kIx.
•	•	k?
15	#num#	k4
<g/>
.	.	kIx.
•	•	k?
16	#num#	k4
<g/>
.	.	kIx.
•	•	k?
17	#num#	k4
<g/>
.	.	kIx.
•	•	k?
18	#num#	k4
<g/>
.	.	kIx.
•	•	k?
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sjezdy	sjezd	k1gInPc1
</s>
<s>
I.	I.	kA
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
•	•	k?
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
•	•	k?
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
)	)	kIx)
•	•	k?
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
•	•	k?
V.	V.	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
VI	VI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
•	•	k?
VII	VII	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
VIII	VIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
<g/>
)	)	kIx)
•	•	k?
IX	IX	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
•	•	k?
X.	X.	kA
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
•	•	k?
XI	XI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
•	•	k?
XII	XII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
XIII	XIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
XIV	XIV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
XV	XV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
XVI	XVI	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
XVII	XVII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
XVIII	XVIII	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
XIX	XIX	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Čína	Čína	k1gFnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
