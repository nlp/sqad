<s>
Turbocharged	Turbocharged	k1gInSc1
Direct	Direct	k2eAgInSc4d1
Injection	Injection	k1gInSc4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Motor	motor	k1gInSc1
2.0	2.0	k4
TDI-PD	TDI-PD	k1gFnSc2
103	#num#	k4
kW	kW	kA
ve	v	k7c6
Volkswagenu	volkswagen	k1gInSc6
Jetta	Jett	k1gInSc2
</s>
<s>
TDI	TDI	kA
(	(	kIx(
<g/>
Turbocharged	Turbocharged	k1gMnSc1
Direct	Direct	k1gMnSc1
Injection	Injection	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
obchodní	obchodní	k2eAgInSc4d1
název	název	k1gInSc4
přeplňovaných	přeplňovaný	k2eAgInPc2d1
vznětových	vznětový	k2eAgInPc2d1
motorů	motor	k1gInPc2
s	s	k7c7
přímým	přímý	k2eAgNnSc7d1
vstřikováním	vstřikování	k1gNnSc7
produkovaných	produkovaný	k2eAgInPc2d1
koncernem	koncern	k1gInSc7
Volkswagen	volkswagen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznětové	vznětový	k2eAgInPc1d1
motory	motor	k1gInPc1
označované	označovaný	k2eAgFnSc2d1
jako	jako	k8xS,k8xC
SDI	SDI	kA
jsou	být	k5eAaImIp3nP
motory	motor	k1gInPc1
stejné	stejný	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
turbodmychadla	turbodmychadlo	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
poskytují	poskytovat	k5eAaImIp3nP
nižší	nízký	k2eAgInSc4d2
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1989	#num#	k4
představila	představit	k5eAaPmAgFnS
automobilka	automobilka	k1gFnSc1
FIAT	fiat	k1gInSc4
jako	jako	k8xC,k8xS
první	první	k4xOgInSc4
sériově	sériově	k6eAd1
vyráběný	vyráběný	k2eAgInSc4d1
motor	motor	k1gInSc4
TDI	TDI	kA
v	v	k7c6
modelu	model	k1gInSc6
CROMA	CROMA	kA
1.9	1.9	k4
<g/>
TDI	TDI	kA
<g/>
.	.	kIx.
<g/>
D	D	kA
154	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
přeplňovaný	přeplňovaný	k2eAgInSc4d1
řadový	řadový	k2eAgInSc4d1
čtyřválec	čtyřválec	k1gInSc4
o	o	k7c6
objemu	objem	k1gInSc6
1929	#num#	k4
cm³	cm³	k?
a	a	k8xC
výkonu	výkon	k1gInSc2
66	#num#	k4
<g/>
kw	kw	kA
(	(	kIx(
<g/>
později	pozdě	k6eAd2
68	#num#	k4
<g/>
kw	kw	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motor	motor	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
původního	původní	k2eAgInSc2d1
komůrkového	komůrkový	k2eAgInSc2d1
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
komůrkovému	komůrkový	k2eAgInSc3d1
agregátu	agregát	k1gInSc3
má	mít	k5eAaImIp3nS
TDI	TDI	kA
<g/>
.	.	kIx.
<g/>
D	D	kA
zesílenou	zesílený	k2eAgFnSc4d1
klikovou	klikový	k2eAgFnSc4d1
hřídel	hřídel	k1gFnSc4
<g/>
,	,	kIx,
zesílené	zesílený	k2eAgFnPc4d1
ojnice	ojnice	k1gFnPc4
<g/>
,	,	kIx,
vylepšený	vylepšený	k2eAgInSc4d1
ostřik	ostřik	k1gInSc4
pístů	píst	k1gInPc2
<g/>
,	,	kIx,
lepší	dobrý	k2eAgNnSc1d2
chlazení	chlazení	k1gNnSc1
a	a	k8xC
zcela	zcela	k6eAd1
novou	nový	k2eAgFnSc4d1
hlavu	hlava	k1gFnSc4
válců	válec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
generace	generace	k1gFnSc1
má	mít	k5eAaImIp3nS
písty	píst	k1gInPc4
se	s	k7c7
spalovacím	spalovací	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
misky	miska	k1gFnSc2
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc7d2
hlučností	hlučnost	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
vyšší	vysoký	k2eAgFnSc4d2
tepelnou	tepelný	k2eAgFnSc4d1
účinnosti	účinnost	k1gFnPc4
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlava	hlava	k1gFnSc1
osazená	osazený	k2eAgFnSc1d1
osmi	osm	k4xCc2
ventily	ventil	k1gInPc1
je	být	k5eAaImIp3nS
z	z	k7c2
hliníkové	hliníkový	k2eAgFnSc2d1
slitiny	slitina	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
zároveň	zároveň	k6eAd1
usazená	usazený	k2eAgFnSc1d1
vačková	vačkový	k2eAgFnSc1d1
hřídel	hřídel	k1gFnSc1
(	(	kIx(
<g/>
rozvod	rozvod	k1gInSc1
OHC	OHC	kA
zubovým	zubový	k2eAgInSc7d1
řemenem	řemen	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palivový	palivový	k2eAgInSc1d1
systém	systém	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
komůrkové	komůrkový	k2eAgFnSc2d1
verze	verze	k1gFnSc2
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstřikovací	vstřikovací	k2eAgInSc4d1
čerpadlo	čerpadlo	k1gNnSc1
je	být	k5eAaImIp3nS
rotační	rotační	k2eAgInSc1d1
a	a	k8xC
trysky	tryska	k1gFnPc1
víceotvorové	víceotvorová	k1gFnSc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
označením	označení	k1gNnSc7
DLLA	DLLA	kA
<g/>
160	#num#	k4
<g/>
P	P	kA
<g/>
171	#num#	k4
<g/>
,	,	kIx,
otevírací	otevírací	k2eAgInSc1d1
tlak	tlak	k1gInSc1
trysky	tryska	k1gFnSc2
je	být	k5eAaImIp3nS
250	#num#	k4
<g/>
Bar	bar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
generace	generace	k1gFnSc1
motoru	motor	k1gInSc2
má	mít	k5eAaImIp3nS
trysky	tryska	k1gFnPc4
tzv.	tzv.	kA
jednopružinkové	jednopružinkový	k2eAgFnPc4d1
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
že	že	k8xS
je	být	k5eAaImIp3nS
do	do	k7c2
válce	válec	k1gInSc2
vstříknuto	vstříknut	k2eAgNnSc1d1
vždy	vždy	k6eAd1
v	v	k7c6
jednom	jeden	k4xCgInSc6
cyklu	cyklus	k1gInSc6
jedna	jeden	k4xCgFnSc1
celistvá	celistvý	k2eAgFnSc1d1
dávka	dávka	k1gFnSc1
paliva	palivo	k1gNnSc2
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
je	být	k5eAaImIp3nS
i	i	k9
motor	motor	k1gInSc4
hlučnější	hlučný	k2eAgInSc4d2
(	(	kIx(
<g/>
tvrdší	tvrdý	k2eAgInSc4d2
<g/>
)	)	kIx)
a	a	k8xC
náchylnější	náchylný	k2eAgFnSc1d2
na	na	k7c4
kouřivost	kouřivost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generace	generace	k1gFnSc1
druhá	druhý	k4xOgFnSc1
má	mít	k5eAaImIp3nS
vstřikovače	vstřikovač	k1gInPc1
dvoupružinkové	dvoupružinkový	k2eAgInPc1d1
a	a	k8xC
cyklus	cyklus	k1gInSc1
vstřiku	vstřik	k1gInSc2
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
předstřiku	předstřik	k1gInSc2
a	a	k8xC
hlavní	hlavní	k2eAgFnSc2d1
dávky	dávka	k1gFnSc2
(	(	kIx(
<g/>
měkčí	měkký	k2eAgNnSc1d2
spalování	spalování	k1gNnSc1
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
Přeplňování	přeplňování	k1gNnPc4
zajišťuje	zajišťovat	k5eAaImIp3nS
obtokové	obtokový	k2eAgNnSc1d1
turbodmychadlo	turbodmychadlo	k1gNnSc1
KKK	KKK	kA
K16	K16	k1gFnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
turbo	turba	k1gFnSc5
s	s	k7c7
proměnnou	proměnný	k2eAgFnSc7d1
geometrií	geometrie	k1gFnSc7
lopatek	lopatka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1989	#num#	k4
se	se	k3xPyFc4
motor	motor	k1gInSc1
TDI	TDI	kA
představil	představit	k5eAaPmAgInS
jako	jako	k9
řadový	řadový	k2eAgInSc1d1
pětiválec	pětiválec	k1gInSc1
2.5	2.5	k4
TDI	TDI	kA
s	s	k7c7
výkonem	výkon	k1gInSc7
88	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
120	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
osazený	osazený	k2eAgMnSc1d1
podélně	podélně	k6eAd1
ve	v	k7c6
voze	vůz	k1gInSc6
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
Audi	Audi	k1gNnSc1
100	#num#	k4
Avant	Avanto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motor	motor	k1gInSc1
plynule	plynule	k6eAd1
táhl	táhnout	k5eAaImAgInS
a	a	k8xC
zrychloval	zrychlovat	k5eAaImAgInS
již	již	k6eAd1
od	od	k7c2
nízkých	nízký	k2eAgFnPc2d1
otáček	otáčka	k1gFnPc2
<g/>
,	,	kIx,
vůz	vůz	k1gInSc1
s	s	k7c7
ním	on	k3xPp3gMnSc7
dokázal	dokázat	k5eAaPmAgMnS
jet	jet	k5eAaImF
téměř	téměř	k6eAd1
200	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
přitom	přitom	k6eAd1
vykazoval	vykazovat	k5eAaImAgInS
spotřebu	spotřeba	k1gFnSc4
pouhých	pouhý	k2eAgInPc2d1
5,7	5,7	k4
litrů	litr	k1gInPc2
nafty	nafta	k1gFnSc2
na	na	k7c4
100	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
způsobila	způsobit	k5eAaPmAgNnP
tato	tento	k3xDgNnPc1
tři	tři	k4xCgNnPc1
pověstná	pověstný	k2eAgNnPc1d1
písmena	písmeno	k1gNnPc1
TDI	TDI	kA
doslova	doslova	k6eAd1
revoluci	revoluce	k1gFnSc4
ve	v	k7c6
světě	svět	k1gInSc6
vznětových	vznětový	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
mezníkem	mezník	k1gInSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
„	„	k?
<g/>
lidovější	lidový	k2eAgInSc1d2
<g/>
“	“	k?
motor	motor	k1gInSc1
1.9	1.9	k4
TDI	TDI	kA
o	o	k7c6
výkonu	výkon	k1gInSc6
66	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
90	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
nižšího	nízký	k2eAgInSc2d2
modelu	model	k1gInSc2
Audi	Audi	k1gNnSc1
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
jej	on	k3xPp3gMnSc4
dostala	dostat	k5eAaPmAgFnS
také	také	k9
třetí	třetí	k4xOgFnSc1
generace	generace	k1gFnSc1
lidového	lidový	k2eAgInSc2d1
vozu	vůz	k1gInSc2
Golf	golf	k1gInSc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
verze	verze	k1gFnSc2
kabriolet	kabriolet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
se	se	k3xPyFc4
světu	svět	k1gInSc3
představila	představit	k5eAaPmAgFnS
její	její	k3xOp3gFnSc1
silnější	silný	k2eAgFnSc1d2
verze	verze	k1gFnSc1
s	s	k7c7
výkonem	výkon	k1gInSc7
81	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
110	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
a	a	k8xC
variabilní	variabilní	k2eAgInSc1d1
geometrií	geometrie	k1gFnSc7
lopatek	lopatka	k1gFnPc2
turbodmychadla	turbodmychadlo	k1gNnSc2
VGT	VGT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
varianty	varianta	k1gFnPc1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
montovaly	montovat	k5eAaImAgFnP
i	i	k9
do	do	k7c2
první	první	k4xOgFnSc2
generace	generace	k1gFnSc2
Škody	škoda	k1gFnSc2
Octavia	octavia	k1gFnSc1
nebo	nebo	k8xC
do	do	k7c2
Seatu	Seat	k1gInSc2
Toledo	Toledo	k1gNnSc4
první	první	k4xOgFnSc2
generace	generace	k1gFnSc2
postaveném	postavený	k2eAgInSc6d1
na	na	k7c6
základě	základ	k1gInSc6
Golfu	golf	k1gInSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Obliba	obliba	k1gFnSc1
TDI	TDI	kA
rychle	rychle	k6eAd1
rostla	růst	k5eAaImAgFnS
a	a	k8xC
tak	tak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
dostala	dostat	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
modelu	model	k1gInSc2
Audi	Audi	k1gNnSc2
A6	A6	k1gFnSc2
motor	motor	k1gInSc1
2.5	2.5	k4
V6	V6	k1gFnSc2
TDI	TDI	kA
o	o	k7c6
výkonu	výkon	k1gInSc6
110	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
150	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
agregát	agregát	k1gInSc1
nebyl	být	k5eNaImAgInS
dostatečně	dostatečně	k6eAd1
pružný	pružný	k2eAgInSc1d1
<g/>
,	,	kIx,
přišla	přijít	k5eAaPmAgFnS
automobilka	automobilka	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
s	s	k7c7
novou	nový	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
motoru	motor	k1gInSc2
2.5	2.5	k4
V6	V6	k1gFnSc2
TDI	TDI	kA
s	s	k7c7
výkonem	výkon	k1gInSc7
132	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
180	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
se	se	k3xPyFc4
dočkali	dočkat	k5eAaPmAgMnP
i	i	k9
nejnáročnější	náročný	k2eAgMnPc1d3
motoristé	motorista	k1gMnPc1
<g/>
,	,	kIx,
protože	protože	k8xS
přeplňovaný	přeplňovaný	k2eAgInSc4d1
přímovstřikový	přímovstřikový	k2eAgInSc4d1
diesel	diesel	k1gInSc4
dostala	dostat	k5eAaPmAgFnS
i	i	k9
největší	veliký	k2eAgFnSc1d3
limuzína	limuzína	k1gFnSc1
Audi	Audi	k1gNnSc1
A8	A8	k1gFnSc2
první	první	k4xOgFnSc2
generace	generace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
osmiválcový	osmiválcový	k2eAgInSc4d1
motor	motor	k1gInSc4
3.3	3.3	k4
TDI	TDI	kA
s	s	k7c7
výkonem	výkon	k1gInSc7
165	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
224	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
a	a	k8xC
přímým	přímý	k2eAgNnSc7d1
vstřikováním	vstřikování	k1gNnSc7
Common-Rail	Common-Raila	k1gFnPc2
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
CR	cr	k0
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
nejmenší	malý	k2eAgInSc4d3
motor	motor	k1gInSc4
s	s	k7c7
označením	označení	k1gNnSc7
TDI	TDI	kA
prodávaný	prodávaný	k2eAgInSc4d1
od	od	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
má	mít	k5eAaImIp3nS
tři	tři	k4xCgInPc4
válce	válec	k1gInPc4
<g/>
,	,	kIx,
výkon	výkon	k1gInSc4
45	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
61	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
objem	objem	k1gInSc1
1.2	1.2	k4
l	l	kA
(	(	kIx(
<g/>
přesný	přesný	k2eAgInSc4d1
objem	objem	k1gInSc4
1191	#num#	k4
cm	cm	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spotřebu	spotřeba	k1gFnSc4
3	#num#	k4
litry	litr	k1gInPc4
nafty	nafta	k1gFnSc2
na	na	k7c4
100	#num#	k4
km	km	kA
a	a	k8xC
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
se	s	k7c7
sekvenční	sekvenční	k2eAgFnSc7d1
převodovkou	převodovka	k1gFnSc7
Tiptronic	Tiptronice	k1gFnPc2
a	a	k8xC
osazován	osazován	k2eAgMnSc1d1
do	do	k7c2
vozů	vůz	k1gInPc2
VW	VW	kA
Lupo	lupa	k1gFnSc5
3L	3L	k4
a	a	k8xC
Audi	Audi	k1gNnSc1
A	a	k8xC
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roli	role	k1gFnSc4
turbodieselu	turbodiesel	k1gInSc2
„	„	k?
<g/>
pro	pro	k7c4
masy	masa	k1gFnPc4
<g/>
“	“	k?
však	však	k9
měl	mít	k5eAaImAgMnS
zastávat	zastávat	k5eAaImF
tříválec	tříválec	k1gInSc4
1.4	1.4	k4
TDI	TDI	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vznikl	vzniknout	k5eAaPmAgInS
„	„	k?
<g/>
uříznutím	uříznutí	k1gNnPc3
<g/>
“	“	k?
jednoho	jeden	k4xCgInSc2
válce	válec	k1gInSc2
z	z	k7c2
legendární	legendární	k2eAgFnSc2d1
čtyřválcové	čtyřválcový	k2eAgFnSc2d1
1.9	1.9	k4
TDI	TDI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
se	se	k3xPyFc4
také	také	k9
začalo	začít	k5eAaPmAgNnS
do	do	k7c2
všech	všecek	k3xTgMnPc2
modelů	model	k1gInPc2
v	v	k7c6
koncernu	koncern	k1gInSc6
VW	VW	kA
průběžně	průběžně	k6eAd1
montovat	montovat	k5eAaImF
vysokotlaké	vysokotlaký	k2eAgNnSc4d1
vstřikování	vstřikování	k1gNnSc4
čerpadlo-tryska	čerpadlo-trysk	k1gInSc2
označované	označovaný	k2eAgFnSc2d1
zkratkou	zkratka	k1gFnSc7
PD	PD	kA
(	(	kIx(
<g/>
z	z	k7c2
německého	německý	k2eAgInSc2d1
názvu	název	k1gInSc2
Pumpe-Düse	Pumpe-Düse	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
mělo	mít	k5eAaImAgNnS
vstřikovací	vstřikovací	k2eAgInSc4d1
tlak	tlak	k1gInSc4
až	až	k9
2050	#num#	k4
barů	bar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začalo	začít	k5eAaPmAgNnS
se	se	k3xPyFc4
pak	pak	k6eAd1
<g/>
,	,	kIx,
podle	podle	k7c2
nepsaného	nepsaný	k2eAgNnSc2d1,k2eNgNnSc2d1
pravidla	pravidlo	k1gNnSc2
<g/>
,	,	kIx,
tyto	tento	k3xDgInPc4
motory	motor	k1gInPc4
laicky	laicky	k6eAd1
označovat	označovat	k5eAaImF
TDI-PD	TDI-PD	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
2003	#num#	k4
znamenal	znamenat	k5eAaImAgInS
debut	debut	k1gInSc1
pro	pro	k7c4
motor	motor	k1gInSc4
4.0	4.0	k4
V8	V8	k1gFnSc2
TDI	TDI	kA
202	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
275	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
s	s	k7c7
řadou	řada	k1gFnSc7
inovativních	inovativní	k2eAgNnPc2d1
řešení	řešení	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
staly	stát	k5eAaPmAgFnP
základem	základ	k1gInSc7
pro	pro	k7c4
novější	nový	k2eAgInPc4d2
šestiválce	šestiválec	k1gInPc4
3.0	3.0	k4
TDI	TDI	kA
CR	cr	k0
165	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
225	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motorickou	motorický	k2eAgFnSc7d1
lahůdkou	lahůdka	k1gFnSc7
byl	být	k5eAaImAgInS
desetiválec	desetiválec	k1gInSc1
5.0	5.0	k4
V10	V10	k1gMnSc2
TDI	TDI	kA
montovaný	montovaný	k2eAgInSc1d1
například	například	k6eAd1
do	do	k7c2
VW	VW	kA
Touareg	Touareg	k1gInSc1
nebo	nebo	k8xC
VW	VW	kA
Phaeton	Phaeton	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
však	však	k9
musel	muset	k5eAaImAgInS
být	být	k5eAaImF
pro	pro	k7c4
nesplnění	nesplnění	k1gNnSc4
emisních	emisní	k2eAgFnPc2d1
norem	norma	k1gFnPc2
Euro	euro	k1gNnSc4
IV	Iva	k1gFnPc2
nahrazen	nahradit	k5eAaPmNgMnS
osmiválcem	osmiválec	k1gInSc7
4.2	4.2	k4
V8	V8	k1gFnSc2
TDI	TDI	kA
<g/>
.	.	kIx.
</s>
<s>
Závodní	závodní	k2eAgInSc1d1
vůz	vůz	k1gInSc1
Audi	Audi	k1gNnSc1
R10	R10	k1gMnSc2
TDI	TDI	kA
osazený	osazený	k2eAgInSc1d1
dvanáctiválcem	dvanáctiválec	k1gInSc7
TDI	TDI	kA
o	o	k7c6
výkonu	výkon	k1gInSc6
480	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
654	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
a	a	k8xC
2008	#num#	k4
třikrát	třikrát	k6eAd1
po	po	k7c6
sobě	se	k3xPyFc3
v	v	k7c6
závodě	závod	k1gInSc6
24	#num#	k4
hodin	hodina	k1gFnPc2
Le	Le	k1gFnPc2
Mans	Mans	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
let	léto	k1gNnPc2
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
se	se	k3xPyFc4
z	z	k7c2
nabídky	nabídka	k1gFnSc2
koncernu	koncern	k1gInSc2
VW	VW	kA
začínají	začínat	k5eAaImIp3nP
postupně	postupně	k6eAd1
vytrácet	vytrácet	k5eAaImF
všechny	všechen	k3xTgFnPc4
verze	verze	k1gFnPc4
TDI	TDI	kA
motorů	motor	k1gInPc2
se	s	k7c7
vstřikováním	vstřikování	k1gNnSc7
PD	PD	kA
a	a	k8xC
pozvolna	pozvolna	k6eAd1
je	on	k3xPp3gInPc4
začínají	začínat	k5eAaImIp3nP
nahrazovat	nahrazovat	k5eAaImF
motory	motor	k1gInPc4
se	s	k7c7
systémem	systém	k1gInSc7
CR	cr	k0
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
jsou	být	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
kultivovanější	kultivovaný	k2eAgInPc1d2
a	a	k8xC
tišší	tichý	k2eAgInPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2010	#num#	k4
nabídka	nabídka	k1gFnSc1
motorů	motor	k1gInPc2
TDI-PD	TDI-PD	k1gFnPc2
úplně	úplně	k6eAd1
zmizela	zmizet	k5eAaPmAgFnS
a	a	k8xC
nahradily	nahradit	k5eAaPmAgInP
je	on	k3xPp3gFnPc4
motory	motor	k1gInPc7
1.6	1.6	k4
TDI	TDI	kA
CR	cr	k0
ve	v	k7c6
výkonech	výkon	k1gInPc6
55	#num#	k4
<g/>
,	,	kIx,
66	#num#	k4
a	a	k8xC
77	#num#	k4
kW	kW	kA
<g/>
,	,	kIx,
tříválcová	tříválcový	k2eAgFnSc1d1
1.2	1.2	k4
TDI	TDI	kA
CR	cr	k0
55	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
přesný	přesný	k2eAgInSc4d1
objem	objem	k1gInSc4
1199	#num#	k4
cm	cm	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
nemá	mít	k5eNaImIp3nS
s	s	k7c7
původní	původní	k2eAgFnSc6d1
1.2	1.2	k4
TDI-PD	TDI-PD	k1gFnSc6
nic	nic	k3yNnSc1
společného	společný	k2eAgNnSc2d1
<g/>
,	,	kIx,
a	a	k8xC
2.0	2.0	k4
TDI	TDI	kA
CR	cr	k0
<g/>
.	.	kIx.
</s>
<s>
Třešničkou	třešnička	k1gFnSc7
na	na	k7c6
dortu	dort	k1gInSc6
sériově	sériově	k6eAd1
vyráběných	vyráběný	k2eAgInPc2d1
TDI	TDI	kA
je	být	k5eAaImIp3nS
motor	motor	k1gInSc1
6.0	6.0	k4
V12	V12	k1gFnSc2
TDI	TDI	kA
s	s	k7c7
výkonem	výkon	k1gInSc7
368	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
500	#num#	k4
k	k	k7c3
<g/>
)	)	kIx)
a	a	k8xC
točivým	točivý	k2eAgInSc7d1
momentem	moment	k1gInSc7
rovných	rovný	k2eAgFnPc2d1
1000	#num#	k4
Nm	Nm	k1gFnPc2
v	v	k7c6
širokém	široký	k2eAgNnSc6d1
spektru	spektrum	k1gNnSc6
1750	#num#	k4
-	-	kIx~
3250	#num#	k4
otáček	otáčka	k1gFnPc2
za	za	k7c4
minutu	minuta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
dodává	dodávat	k5eAaImIp3nS
do	do	k7c2
vozů	vůz	k1gInPc2
Audi	Audi	k1gNnSc2
Q7	Q7	k1gFnSc2
V12	V12	k1gFnSc2
TDI	TDI	kA
výhradně	výhradně	k6eAd1
s	s	k7c7
pohonem	pohon	k1gInSc7
všech	všecek	k3xTgFnPc2
kol	kola	k1gFnPc2
a	a	k8xC
sekvenční	sekvenční	k2eAgFnSc7d1
převodovkou	převodovka	k1gFnSc7
Tiptronic	Tiptronice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Falšování	falšování	k1gNnSc1
emisí	emise	k1gFnPc2
firmou	firma	k1gFnSc7
Volkswagen	volkswagen	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dieselgate	Dieselgat	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2015	#num#	k4
vydala	vydat	k5eAaPmAgFnS
americká	americký	k2eAgFnSc1d1
Agentura	agentura	k1gFnSc1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
EPA	EPA	kA
<g/>
)	)	kIx)
prohlášení	prohlášení	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
zmiňuje	zmiňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
firma	firma	k1gFnSc1
Volkswagen	volkswagen	k1gInSc1
manipuluje	manipulovat	k5eAaImIp3nS
s	s	k7c7
motorovým	motorový	k2eAgInSc7d1
softwarem	software	k1gInSc7
na	na	k7c6
dieselových	dieselový	k2eAgFnPc6d1
TDI	TDI	kA
motorech	motor	k1gInPc6
a	a	k8xC
porušuje	porušovat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
emisní	emisní	k2eAgFnSc1d1
limity	limita	k1gFnSc2
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
se	se	k3xPyFc4
Michael	Michael	k1gMnSc1
Horn	Horn	k1gMnSc1
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
americké	americký	k2eAgFnSc2d1
pobočky	pobočka	k1gFnSc2
Volkswagenu	volkswagen	k1gInSc2
veřejnosti	veřejnost	k1gFnSc2
omluvil	omluvit	k5eAaPmAgMnS
za	za	k7c4
své	svůj	k3xOyFgNnSc4
chování	chování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dieselové	dieselový	k2eAgInPc1d1
motory	motor	k1gInPc1
TDI	TDI	kA
se	s	k7c7
softwarem	software	k1gInSc7
pro	pro	k7c4
falšování	falšování	k1gNnSc4
emisí	emise	k1gFnPc2
údajně	údajně	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
asi	asi	k9
11	#num#	k4
miliónů	milión	k4xCgInPc2
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akcie	akcie	k1gFnSc1
firmy	firma	k1gFnSc2
během	během	k7c2
několika	několik	k4yIc2
dní	den	k1gInPc2
propadly	propadnout	k5eAaPmAgInP
zhruba	zhruba	k6eAd1
o	o	k7c4
třetinu	třetina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
agentura	agentura	k1gFnSc1
EPA	EPA	kA
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
rozšířil	rozšířit	k5eAaPmAgMnS
vyšetřování	vyšetřování	k1gNnSc4
koncernu	koncern	k1gInSc2
Volkswagen	volkswagen	k1gInSc4
též	též	k9
na	na	k7c4
automobily	automobil	k1gInPc4
z	z	k7c2
modelových	modelový	k2eAgNnPc2d1
let	léto	k1gNnPc2
2014	#num#	k4
až	až	k6eAd1
2016	#num#	k4
s	s	k7c7
šestiválcovými	šestiválcový	k2eAgInPc7d1
naftovými	naftový	k2eAgInPc7d1
motory	motor	k1gInPc7
TDI	TDI	kA
o	o	k7c6
objemu	objem	k1gInSc6
tři	tři	k4xCgInPc4
litry	litr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
týkat	týkat	k5eAaImF
asi	asi	k9
10	#num#	k4
tisíc	tisíc	k4xCgInPc2
vozů	vůz	k1gInPc2
Volkswagen	volkswagen	k1gInSc1
Touareg	Touarega	k1gFnPc2
z	z	k7c2
modelového	modelový	k2eAgInSc2d1
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
Porsche	Porsche	k1gNnSc1
Cayenne	Cayenn	k1gInSc5
z	z	k7c2
modelového	modelový	k2eAgInSc2d1
roku	rok	k1gInSc2
2015	#num#	k4
a	a	k8xC
Audi	Audi	k1gNnSc1
A6	A6	k1gFnPc2
Quattro	Quattro	k1gNnSc1
<g/>
,	,	kIx,
Audi	Audi	k1gNnSc1
A7	A7	k1gFnPc2
Quattro	Quattro	k1gNnSc1
<g/>
,	,	kIx,
Audi	Audi	k1gNnSc1
A8	A8	k1gFnPc2
a	a	k8xC
Audi	Audi	k1gNnSc1
Q5	Q5	k1gFnSc2
z	z	k7c2
modelového	modelový	k2eAgInSc2d1
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Princip	princip	k1gInSc1
vstřikování	vstřikování	k1gNnSc2
motorů	motor	k1gInPc2
TDI	TDI	kA
</s>
<s>
U	u	k7c2
běžných	běžný	k2eAgInPc2d1
vznětových	vznětový	k2eAgInPc2d1
motorů	motor	k1gInPc2
s	s	k7c7
nepřímým	přímý	k2eNgNnSc7d1
vstřikováním	vstřikování	k1gNnSc7
paliva	palivo	k1gNnSc2
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
vstřikování	vstřikování	k1gNnSc3
do	do	k7c2
oddělené	oddělený	k2eAgFnSc2d1
předkomůrky	předkomůrka	k1gFnSc2
a	a	k8xC
odtud	odtud	k6eAd1
se	se	k3xPyFc4
směs	směs	k1gFnSc1
teprve	teprve	k6eAd1
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
válce	válec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
u	u	k7c2
přeplňovaného	přeplňovaný	k2eAgInSc2d1
vznětového	vznětový	k2eAgInSc2d1
motoru	motor	k1gInSc2
s	s	k7c7
přímým	přímý	k2eAgNnSc7d1
vstřikováním	vstřikování	k1gNnSc7
paliva	palivo	k1gNnSc2
tryska	tryska	k1gFnSc1
vstřikovače	vstřikovač	k1gInPc1
rozprašuje	rozprašovat	k5eAaImIp3nS
palivo	palivo	k1gNnSc4
přímo	přímo	k6eAd1
do	do	k7c2
spalovacího	spalovací	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
vytvořeném	vytvořený	k2eAgNnSc6d1
ve	v	k7c6
dně	dno	k1gNnSc6
pístu	píst	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přísun	přísun	k1gInSc1
dostatečného	dostatečný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
vzduchu	vzduch	k1gInSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
turbodmychadlo	turbodmychadlo	k1gNnSc4
<g/>
,	,	kIx,
u	u	k7c2
výkonnějších	výkonný	k2eAgFnPc2d2
verzí	verze	k1gFnPc2
s	s	k7c7
variabilní	variabilní	k2eAgFnSc7d1
geometrií	geometrie	k1gFnSc7
lopatek	lopatka	k1gFnPc2
VGT	VGT	kA
(	(	kIx(
<g/>
např.	např.	kA
1.9	1.9	k4
TDI	TDI	kA
81	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
generace	generace	k1gFnSc1
motorů	motor	k1gInPc2
má	mít	k5eAaImIp3nS
elektronicky	elektronicky	k6eAd1
řízené	řízený	k2eAgNnSc1d1
rotační	rotační	k2eAgNnSc1d1
vstřikovací	vstřikovací	k2eAgNnSc1d1
čerpadlo	čerpadlo	k1gNnSc1
a	a	k8xC
dvoupružinkové	dvoupružinkový	k2eAgInPc1d1
vstřikovače	vstřikovač	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
generace	generace	k1gFnSc1
už	už	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
se	s	k7c7
systémem	systém	k1gInSc7
PD	PD	kA
(	(	kIx(
<g/>
čerpadlo-tryska	čerpadlo-trysek	k1gMnSc2
<g/>
)	)	kIx)
kdy	kdy	k6eAd1
čerpadlo	čerpadlo	k1gNnSc1
a	a	k8xC
tryska	tryska	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
komponent	komponent	k1gInSc4
umístěný	umístěný	k2eAgInSc4d1
v	v	k7c6
hlavě	hlava	k1gFnSc6
válců	válec	k1gInPc2
poháněný	poháněný	k2eAgInSc4d1
od	od	k7c2
vačkové	vačkový	k2eAgFnSc2d1
hřídele	hřídel	k1gFnSc2
a	a	k8xC
ovládaný	ovládaný	k2eAgMnSc1d1
elektronicky	elektronicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Aféra	aféra	k1gFnSc1
s	s	k7c7
emisemi	emise	k1gFnPc7
se	se	k3xPyFc4
podle	podle	k7c2
Volkswagenu	volkswagen	k1gInSc2
celosvětově	celosvětově	k6eAd1
týká	týkat	k5eAaImIp3nS
11	#num#	k4
milionů	milion	k4xCgInPc2
aut	auto	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
USA	USA	kA
rozšířily	rozšířit	k5eAaPmAgFnP
vyšetřování	vyšetřování	k1gNnSc4
Volkswagenu	volkswagen	k1gInSc2
kvůli	kvůli	k7c3
skandálu	skandál	k1gInSc3
s	s	k7c7
měřením	měření	k1gNnSc7
emisí	emise	k1gFnPc2
</s>
