<p>
<s>
Doba	doba	k1gFnSc1	doba
pobělohorská	pobělohorský	k2eAgFnSc1d1	pobělohorská
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
důsledky	důsledek	k1gInPc1	důsledek
měly	mít	k5eAaImAgInP	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
politické	politický	k2eAgInPc4d1	politický
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc4d1	náboženský
<g/>
,	,	kIx,	,
společenské	společenský	k2eAgInPc4d1	společenský
i	i	k8xC	i
kulturní	kulturní	k2eAgInPc4d1	kulturní
poměry	poměr	k1gInPc4	poměr
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
pobělohorská	pobělohorský	k2eAgFnSc1d1	pobělohorská
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
dobu	doba	k1gFnSc4	doba
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
období	období	k1gNnSc4	období
až	až	k6eAd1	až
do	do	k7c2	do
r.	r.	kA	r.
1740	[number]	k4	1740
–	–	k?	–
nástupu	nástup	k1gInSc2	nástup
osvícenství	osvícenství	k1gNnPc2	osvícenství
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
až	až	k9	až
do	do	k7c2	do
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
předbělohorská	předbělohorský	k2eAgFnSc1d1	předbělohorská
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
relativní	relativní	k2eAgFnSc7d1	relativní
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
tolerancí	tolerance	k1gFnSc7	tolerance
<g/>
,	,	kIx,	,
významnou	významný	k2eAgFnSc7d1	významná
politickou	politický	k2eAgFnSc7d1	politická
rolí	role	k1gFnSc7	role
stavů	stav	k1gInPc2	stav
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc7d1	bohatá
českou	český	k2eAgFnSc7d1	Česká
literární	literární	k2eAgFnSc7d1	literární
činností	činnost	k1gFnSc7	činnost
(	(	kIx(	(
<g/>
doba	doba	k1gFnSc1	doba
veleslavínská	veleslavínský	k2eAgFnSc1d1	Veleslavínská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
znamenala	znamenat	k5eAaImAgFnS	znamenat
zásadní	zásadní	k2eAgFnSc4d1	zásadní
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
židovského	židovský	k2eAgMnSc2d1	židovský
<g/>
)	)	kIx)	)
jen	jen	k9	jen
katolické	katolický	k2eAgNnSc1d1	katolické
náboženství	náboženství	k1gNnSc1	náboženství
(	(	kIx(	(
<g/>
doba	doba	k1gFnSc1	doba
rekatolizace	rekatolizace	k1gFnSc2	rekatolizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obnoveným	obnovený	k2eAgNnSc7d1	obnovené
zřízením	zřízení	k1gNnSc7	zřízení
zemským	zemský	k2eAgMnSc7d1	zemský
přešel	přejít	k5eAaPmAgMnS	přejít
stát	stát	k5eAaPmF	stát
od	od	k7c2	od
stavovství	stavovství	k1gNnSc2	stavovství
k	k	k7c3	k
absolutismu	absolutismus	k1gInSc3	absolutismus
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spojený	spojený	k2eAgInSc1d1	spojený
centralismus	centralismus	k1gInSc1	centralismus
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
dlouhodobějším	dlouhodobý	k2eAgInSc6d2	dlouhodobější
horizontu	horizont	k1gInSc6	horizont
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
úpadek	úpadek	k1gInSc4	úpadek
užívání	užívání	k1gNnSc2	užívání
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
ničení	ničení	k1gNnSc3	ničení
nekatolických	katolický	k2eNgFnPc2d1	nekatolická
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
napsal	napsat	k5eAaPmAgMnS	napsat
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
<g/>
:	:	kIx,	:
Za	za	k7c2	za
tohoto	tento	k3xDgInSc2	tento
našeho	náš	k3xOp1gInSc2	náš
věku	věk	k1gInSc2	věk
knihy	kniha	k1gFnSc2	kniha
české	český	k2eAgFnPc4d1	Česká
tak	tak	k6eAd1	tak
vzácné	vzácný	k2eAgFnPc1d1	vzácná
a	a	k8xC	a
řídké	řídký	k2eAgFnPc1d1	řídká
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
doba	doba	k1gFnSc1	doba
za	za	k7c2	za
mého	můj	k3xOp1gNnSc2	můj
chlapectví	chlapectví	k1gNnSc2	chlapectví
<g/>
,	,	kIx,	,
brzo	brzo	k6eAd1	brzo
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
pražském	pražský	k2eAgNnSc6d1	Pražské
[	[	kIx(	[
<g/>
1620	[number]	k4	1620
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všechny	všechen	k3xTgFnPc4	všechen
knihy	kniha	k1gFnPc4	kniha
psané	psaný	k2eAgNnSc1d1	psané
českým	český	k2eAgInSc7d1	český
jazykem	jazyk	k1gInSc7	jazyk
pokládány	pokládán	k2eAgFnPc4d1	pokládána
právě	právě	k6eAd1	právě
proto	proto	k8xC	proto
za	za	k7c4	za
kacířské	kacířský	k2eAgNnSc4d1	kacířské
nevědomostí	nevědomost	k1gFnSc7	nevědomost
jistých	jistý	k2eAgMnPc2d1	jistý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
byly	být	k5eAaImAgInP	být
dobré	dobrý	k2eAgInPc1d1	dobrý
či	či	k8xC	či
špatné	špatný	k2eAgInPc1d1	špatný
<g/>
,	,	kIx,	,
odsuzovány	odsuzován	k2eAgInPc1d1	odsuzován
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
koutu	kout	k1gInSc2	kout
domů	dům	k1gInPc2	dům
byvše	být	k5eAaPmDgFnP	být
vyvlečeny	vyvléct	k5eAaPmNgInP	vyvléct
nebo	nebo	k8xC	nebo
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
vyrvány	vyrván	k2eAgFnPc1d1	vyrvána
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
nic	nic	k6eAd1	nic
netýkaly	týkat	k5eNaImAgFnP	týkat
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
trhány	trhat	k5eAaImNgInP	trhat
a	a	k8xC	a
na	na	k7c6	na
zapálených	zapálený	k2eAgFnPc6d1	zapálená
hranicích	hranice	k1gFnPc6	hranice
páleny	pálen	k2eAgInPc1d1	pálen
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
uprostřed	uprostřed	k7c2	uprostřed
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odmítly	odmítnout	k5eAaPmAgInP	odmítnout
přestoupit	přestoupit	k5eAaPmF	přestoupit
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
nejslavnějšího	slavný	k2eAgMnSc2d3	nejslavnější
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
např.	např.	kA	např.
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Pavel	Pavel	k1gMnSc1	Pavel
Stránský	Stránský	k1gMnSc1	Stránský
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Skála	Skála	k1gMnSc1	Skála
ze	z	k7c2	z
Zhoře	Zhoř	k1gFnSc2	Zhoř
nebo	nebo	k8xC	nebo
grafik	grafik	k1gMnSc1	grafik
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc1	zbytek
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
vyplněn	vyplnit	k5eAaPmNgInS	vyplnit
pokračující	pokračující	k2eAgFnSc7d1	pokračující
třicetiletou	třicetiletý	k2eAgFnSc7d1	třicetiletá
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
obdobím	období	k1gNnSc7	období
stabilizace	stabilizace	k1gFnSc2	stabilizace
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
zotavováním	zotavování	k1gNnSc7	zotavování
z	z	k7c2	z
následků	následek	k1gInPc2	následek
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
polovina	polovina	k1gFnSc1	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
dobou	doba	k1gFnSc7	doba
nového	nový	k2eAgInSc2d1	nový
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
rozkvětu	rozkvět	k1gInSc2	rozkvět
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
i	i	k9	i
děj	děj	k1gInSc1	děj
Jiráskova	Jiráskův	k2eAgInSc2d1	Jiráskův
románu	román	k1gInSc2	román
Temno	temno	k6eAd1	temno
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
názvem	název	k1gInSc7	název
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
celé	celý	k2eAgNnSc1d1	celé
pobělohorské	pobělohorský	k2eAgNnSc1d1	pobělohorské
období	období	k1gNnSc1	období
jako	jako	k8xC	jako
doba	doba	k1gFnSc1	doba
temna	temno	k1gNnSc2	temno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tradičním	tradiční	k2eAgNnSc6d1	tradiční
pojetí	pojetí	k1gNnSc6	pojetí
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
odvozeném	odvozený	k2eAgMnSc6d1	odvozený
od	od	k7c2	od
Palackého	Palacký	k1gMnSc2	Palacký
a	a	k8xC	a
Masaryka	Masaryk	k1gMnSc2	Masaryk
je	být	k5eAaImIp3nS	být
pobělohorské	pobělohorský	k2eAgNnSc1d1	pobělohorské
období	období	k1gNnSc1	období
chápáno	chápat	k5eAaImNgNnS	chápat
negativně	negativně	k6eAd1	negativně
jako	jako	k8xS	jako
doba	doba	k1gFnSc1	doba
úpadku	úpadek	k1gInSc2	úpadek
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
i	i	k8xC	i
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
také	také	k9	také
období	období	k1gNnSc1	období
rozvoje	rozvoj	k1gInSc2	rozvoj
specifické	specifický	k2eAgFnSc2d1	specifická
kultury	kultura	k1gFnSc2	kultura
českého	český	k2eAgNnSc2d1	české
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
osobnostem	osobnost	k1gFnPc3	osobnost
pobělohorské	pobělohorský	k2eAgFnSc2d1	pobělohorská
doby	doba	k1gFnSc2	doba
patřil	patřit	k5eAaImAgMnS	patřit
člen	člen	k1gMnSc1	člen
hlavní	hlavní	k2eAgFnSc2d1	hlavní
rekatolizační	rekatolizační	k2eAgFnSc2d1	rekatolizační
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
<g/>
,	,	kIx,	,
vzdělanec	vzdělanec	k1gMnSc1	vzdělanec
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
vlastenec	vlastenec	k1gMnSc1	vlastenec
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
315	[number]	k4	315
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
59	[number]	k4	59
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DENIS	Denisa	k1gFnPc2	Denisa
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Centralism	Centralism	k6eAd1	Centralism
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jindřich	Jindřich	k1gMnSc1	Jindřich
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Bursík	Bursík	k1gMnSc1	Bursík
&	&	k?	&
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
633	[number]	k4	633
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Od	od	k7c2	od
Bílé	bílý	k2eAgFnSc2d1	bílá
hory	hora	k1gFnSc2	hora
do	do	k7c2	do
konce	konec	k1gInSc2	konec
panování	panování	k1gNnSc2	panování
Leopolda	Leopold	k1gMnSc2	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
baroko	baroko	k1gNnSc1	baroko
</s>
</p>
<p>
<s>
Pobělohorská	pobělohorský	k2eAgFnSc1d1	pobělohorská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
Protireformace	protireformace	k1gFnSc1	protireformace
</s>
</p>
<p>
<s>
Reformace	reformace	k1gFnSc1	reformace
a	a	k8xC	a
protireformace	protireformace	k1gFnSc1	protireformace
(	(	kIx(	(
<g/>
1550	[number]	k4	1550
<g/>
–	–	k?	–
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
</s>
</p>
