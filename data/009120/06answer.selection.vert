<s>
Doba	doba	k1gFnSc1	doba
pobělohorská	pobělohorský	k2eAgFnSc1d1	pobělohorská
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
důsledky	důsledek	k1gInPc1	důsledek
měly	mít	k5eAaImAgInP	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
politické	politický	k2eAgInPc4d1	politický
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgInPc4d1	náboženský
<g/>
,	,	kIx,	,
společenské	společenský	k2eAgInPc4d1	společenský
i	i	k8xC	i
kulturní	kulturní	k2eAgInPc4d1	kulturní
poměry	poměr	k1gInPc4	poměr
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
