<s>
Asie	Asie	k1gFnSc1	Asie
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
44	[number]	k4	44
603	[number]	k4	603
853	[number]	k4	853
km2	km2	k4	km2
největší	veliký	k2eAgInPc1d3	veliký
<g/>
,	,	kIx,	,
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
miliardami	miliarda	k4xCgFnPc7	miliarda
obyvatel	obyvatel	k1gMnPc2	obyvatel
nejlidnatější	lidnatý	k2eAgInPc1d3	nejlidnatější
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
rovněž	rovněž	k9	rovněž
nejhustěji	husto	k6eAd3	husto
osídlený	osídlený	k2eAgInSc1d1	osídlený
světadíl	světadíl	k1gInSc1	světadíl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
součást	součást	k1gFnSc4	součást
kontinentu	kontinent	k1gInSc2	kontinent
zvaného	zvaný	k2eAgInSc2d1	zvaný
Eurasie	Eurasie	k1gFnPc4	Eurasie
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
Eurafrasie	Eurafrasie	k1gFnSc1	Eurafrasie
<g/>
.	.	kIx.	.
</s>
