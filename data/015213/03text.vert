<s>
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
</s>
<s>
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gMnSc2
Osobní	osobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1994	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Plymouth	Plymouth	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Občanství	občanství	k1gNnSc2
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Choť	choť	k1gFnSc1
</s>
<s>
Dustin	Dustin	k1gInSc1
Lance	lance	k1gNnSc2
Black	Blacka	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
2017	#num#	k4
<g/>
)	)	kIx)
Výška	výška	k1gFnSc1
</s>
<s>
177	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
74	#num#	k4
kg	kg	kA
Sportovní	sportovní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Sport	sport	k1gInSc1
</s>
<s>
skoky	skok	k1gInPc1
do	do	k7c2
vody	voda	k1gFnSc2
Účast	účast	k1gFnSc1
na	na	k7c6
LOH	LOH	kA
</s>
<s>
2008	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Skoky	skok	k1gInPc1
do	do	k7c2
vody	voda	k1gFnSc2
na	na	k7c4
LOH	LOH	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
LOH	LOH	kA
2012	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
LOH	LOH	kA
2016	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
synchro	synchra	k1gFnSc5
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
skocích	skok	k1gInPc6
do	do	k7c2
vody	voda	k1gFnSc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
2009	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
2015	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
2015	#num#	k4
</s>
<s>
týmová	týmový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
2017	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
skocích	skok	k1gInPc6
do	do	k7c2
vody	voda	k1gFnSc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
ME	ME	kA
2008	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
ME	ME	kA
2012	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
ME	ME	kA
2014	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
ME	ME	kA
2016	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
ME	ME	kA
2016	#num#	k4
</s>
<s>
věž	věž	k1gFnSc1
10	#num#	k4
m	m	kA
synchro	synchra	k1gFnSc5
</s>
<s>
Thomas	Thomas	k1gMnSc1
Robert	Robert	k1gMnSc1
„	„	k?
<g/>
Tom	Tom	k1gMnSc1
<g/>
“	“	k?
Daley	Dalea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1994	#num#	k4
Plymouth	Plymoutha	k1gFnPc2
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
anglický	anglický	k2eAgMnSc1d1
skokan	skokan	k1gMnSc1
do	do	k7c2
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
ve	v	k7c6
skoku	skok	k1gInSc6
z	z	k7c2
10	#num#	k4
<g/>
metrové	metrový	k2eAgFnSc2d1
věže	věž	k1gFnSc2
a	a	k8xC
bronzový	bronzový	k2eAgMnSc1d1
olympionik	olympionik	k1gMnSc1
z	z	k7c2
Letních	letní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
2012	#num#	k4
v	v	k7c6
Londýně	Londýn	k1gInSc6
i	i	k9
z	z	k7c2
Letních	letní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
2016	#num#	k4
v	v	k7c6
Rio	Rio	k1gFnSc6
de	de	k?
Janeiru	Janeir	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Plymouthu	Plymouth	k1gInSc6
rodičům	rodič	k1gMnPc3
Robertovi	Robert	k1gMnSc6
a	a	k8xC
Debře	Debře	k?
Daleyovým	Daleyův	k2eAgMnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Otec	otec	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
věku	věk	k1gInSc6
40	#num#	k4
let	léto	k1gNnPc2
na	na	k7c4
rakovinu	rakovina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
svých	svůj	k3xOyFgInPc6
sportovních	sportovní	k2eAgInPc6d1
úspěších	úspěch	k1gInPc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
od	od	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
2013	#num#	k4
i	i	k9
tváří	tvář	k1gFnSc7
televizní	televizní	k2eAgFnSc2d1
obrazovky	obrazovka	k1gFnSc2
v	v	k7c6
pořadu	pořad	k1gInSc6
Splash	Splash	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
televize	televize	k1gFnSc1
ITV	ITV	kA
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
mentoroval	mentorovat	k5eAaImAgMnS
celebrity	celebrita	k1gFnSc2
v	v	k7c6
jejich	jejich	k3xOp3gInPc6
skocích	skok	k1gInPc6
do	do	k7c2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Dalším	další	k2eAgInSc7d1
jeho	jeho	k3xOp3gInSc7
pořadem	pořad	k1gInSc7
byla	být	k5eAaImAgFnS
šestidílná	šestidílný	k2eAgFnSc1d1
minisérie	minisérie	k1gFnSc1
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
Goes	Goes	k1gInSc1
Global	globat	k5eAaImAgInS
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
s	s	k7c7
kamarádkou	kamarádka	k1gFnSc7
Sophie	Sophie	k1gFnSc2
cestoval	cestovat	k5eAaImAgMnS
s	s	k7c7
batohem	batoh	k1gInSc7
po	po	k7c6
Thajsku	Thajsko	k1gNnSc6
<g/>
,	,	kIx,
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
Maroku	Maroko	k1gNnSc6
či	či	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
Austrálii	Austrálie	k1gFnSc4
<g/>
,	,	kIx,
vysílaná	vysílaný	k2eAgFnSc1d1
na	na	k7c6
ITV2	ITV2	k1gFnSc6
na	na	k7c6
jaře	jaro	k1gNnSc6
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
natáčení	natáčení	k1gNnSc6
pořadu	pořad	k1gInSc2
překonal	překonat	k5eAaPmAgMnS
rekord	rekord	k1gInSc4
ve	v	k7c6
skoku	skok	k1gInSc6
z	z	k7c2
357	#num#	k4
stop	stopa	k1gFnPc2
(	(	kIx(
<g/>
asi	asi	k9
109	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
vysokého	vysoký	k2eAgInSc2d1
útesu	útes	k1gInSc2
s	s	k7c7
12	#num#	k4
salty	salto	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
otevřený	otevřený	k2eAgMnSc1d1
gay	gay	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
prosinci	prosinec	k1gInSc6
2013	#num#	k4
provedl	provést	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
veřejný	veřejný	k2eAgInSc4d1
coming	coming	k1gInSc4
out	out	k?
prostřednictvím	prostřednictvím	k7c2
videa	video	k1gNnSc2
na	na	k7c4
YouTube	YouTub	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
ve	v	k7c6
vztahu	vztah	k1gInSc6
s	s	k7c7
mužem	muž	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnSc7
manželem	manžel	k1gMnSc7
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
scenárista	scenárista	k1gMnSc1
Dustin	Dustin	k1gMnSc1
Lance	lance	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Black	Blacka	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
nímž	jenž	k3xRgNnSc7
v	v	k7c6
březnu	březen	k1gInSc6
2015	#num#	k4
oslavil	oslavit	k5eAaPmAgMnS
druhé	druhý	k4xOgNnSc4
výročí	výročí	k1gNnSc4
vztahu	vztah	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
podzim	podzim	k1gInSc4
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
pár	pár	k4xCyI
ohlásil	ohlásit	k5eAaPmAgMnS
v	v	k7c6
tradiční	tradiční	k2eAgFnSc6d1
rubrice	rubrika	k1gFnSc6
deníku	deník	k1gInSc2
the	the	k?
Times	Times	k1gMnSc1
své	svůj	k3xOyFgNnSc4
zasnoubení	zasnoubení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
13	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
jejich	jejich	k3xOp3gFnSc1
svatba	svatba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Koncem	koncem	k7c2
června	červen	k1gInSc2
2018	#num#	k4
manželé	manžel	k1gMnPc1
oznámili	oznámit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jim	on	k3xPp3gMnPc3
náhradní	náhradní	k2eAgFnSc1d1
matka	matka	k1gFnSc1
z	z	k7c2
USA	USA	kA
porodila	porodit	k5eAaPmAgFnS
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
pojmenovaného	pojmenovaný	k2eAgMnSc2d1
po	po	k7c4
Daleyho	Daley	k1gMnSc4
zesnulém	zesnulý	k2eAgMnSc6d1
otci	otec	k1gMnSc6
Robert	Robert	k1gMnSc1
Ray	Ray	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtenáři	čtenář	k1gMnPc1
britského	britský	k2eAgInSc2d1
gay	gay	k1gMnSc1
magazínu	magazín	k1gInSc2
Attitude	Attitud	k1gInSc5
zvolili	zvolit	k5eAaPmAgMnP
Daleyho	Daleyha	k1gFnSc5
v	v	k7c6
letech	léto	k1gNnPc6
2013	#num#	k4
a	a	k8xC
2014	#num#	k4
nejvíc	hodně	k6eAd3,k6eAd1
sexy	sex	k1gInPc1
mužem	muž	k1gMnSc7
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
and	and	k?
Dan	Dan	k1gMnSc1
Goodfellow	Goodfellow	k1gMnSc1
win	win	k?
bronze	bronz	k1gInSc5
medal	medat	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
in	in	k?
synchronised	synchronised	k1gInSc1
diving	diving	k1gInSc1
final	finat	k5eAaImAgInS,k5eAaBmAgInS,k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PinkNews	PinkNews	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-08-08	2016-08-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
ZÁRODŇANSKÝ	ZÁRODŇANSKÝ	kA
<g/>
,	,	kIx,
Rastislav	Rastislav	k1gMnSc1
<g/>
;	;	kIx,
Famous	Famous	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympionik	olympionik	k1gMnSc1
Daley	Dalea	k1gFnSc2
se	se	k3xPyFc4
zasnoubil	zasnoubit	k5eAaPmAgMnS
s	s	k7c7
o	o	k7c4
20	#num#	k4
let	léto	k1gNnPc2
starším	starý	k2eAgMnSc7d2
scenáristou	scenárista	k1gMnSc7
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-10-02	2015-10-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ZÁRODŇANSKÝ	ZÁRODŇANSKÝ	kA
<g/>
,	,	kIx,
Rastislav	Rastislav	k1gMnSc1
(	(	kIx(
<g/>
zar	zar	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympionik	olympionik	k1gMnSc1
Daley	Dalea	k1gFnSc2
s	s	k7c7
manželem	manžel	k1gMnSc7
mají	mít	k5eAaImIp3nP
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevědí	vědět	k5eNaImIp3nP
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
je	být	k5eAaImIp3nS
otec	otec	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2018-06-30	2018-06-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
HAIGH	HAIGH	kA
<g/>
,	,	kIx,
Josh	Josh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
ITV	ITV	kA
show	show	k1gNnSc1
‘	‘	k?
<g/>
Splash	Splash	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
’	’	k?
to	ten	k3xDgNnSc1
be	be	k?
axed	axed	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-02-18	2014-02-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
</s>
<s>
HAIGH	HAIGH	kA
<g/>
,	,	kIx,
Josh	Josh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
No	no	k9
second	second	k1gInSc1
series	seriesa	k1gFnPc2
for	forum	k1gNnPc2
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
backpacking	backpacking	k1gInSc4
reality	realita	k1gFnPc1
show	show	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-05-22	2014-05-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
RIGBY	RIGBY	kA
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
ITV2	ITV2	k1gMnSc7
backpacking	backpacking	k1gInSc4
show	show	k1gFnSc2
begins	begins	k6eAd1
tonight	tonight	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-04-10	2014-04-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
RIGBY	RIGBY	kA
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
breaks	breaksa	k1gFnPc2
swinging	swinging	k1gInSc1
record	record	k1gMnSc1
in	in	k?
New	New	k1gMnSc4
Zealand	Zealand	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-04-21	2014-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
DYKE	DYKE	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
;	;	kIx,
BEGLEY	BEGLEY	kA
<g/>
,	,	kIx,
Katie	Katie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
Goes	Goes	k1gInSc1
Global	globat	k5eAaImAgInS
<g/>
:	:	kIx,
Diver	Diver	k1gInSc1
makes	makes	k1gMnSc1
an	an	k?
even	even	k1gMnSc1
bigger	bigger	k1gMnSc1
Splash	Splash	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daily	Daila	k1gMnSc2
Star	Star	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-04-21	2014-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
MURPHY	MURPHY	kA
<g/>
,	,	kIx,
Rian	Rian	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
on	on	k3xPp3gInSc1
coming	coming	k1gInSc1
out	out	k?
<g/>
:	:	kIx,
‘	‘	k?
<g/>
I	i	k9
didn	didn	k1gMnSc1
<g/>
’	’	k?
<g/>
t	t	k?
want	want	k1gInSc1
to	ten	k3xDgNnSc1
hide	hidat	k5eAaPmIp3nS
anymore	anymor	k1gInSc5
<g/>
’	’	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-21	2014-03-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
LEVINE	LEVINE	kA
<g/>
,	,	kIx,
Nick	Nick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gMnSc2
speaks	speaks	k6eAd1
about	about	k1gMnSc1
sexuality	sexualita	k1gFnSc2
<g/>
:	:	kIx,
‘	‘	k?
<g/>
I	i	k8xC
am	am	k?
dating	dating	k1gInSc1
a	a	k8xC
guy	guy	k?
<g/>
’	’	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-12-10	2013-12-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KHERAI	KHERAI	kA
<g/>
,	,	kIx,
Alim	Alim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dustin	Dustin	k1gInSc1
Lance	lance	k1gNnSc2
Black	Blacka	k1gFnPc2
talks	talksa	k1gFnPc2
long-distance	long-distance	k1gFnSc2
Daley	Dalea	k1gFnPc1
relationship	relationship	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-05-01	2014-05-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HAIGH	HAIGH	kA
<g/>
,	,	kIx,
Josh	Josh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
and	and	k?
Dustin	Dustin	k1gMnSc1
Lance	lance	k1gNnSc2
Black	Black	k1gMnSc1
share	shar	k1gInSc5
their	their	k1gMnSc1
anniversary	anniversar	k1gInPc4
pics	pics	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-03-23	2015-03-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
announces	announces	k1gMnSc1
engagement	engagement	k1gNnSc2
to	ten	k3xDgNnSc4
partner	partner	k1gMnSc1
Dustin	Dustin	k1gMnSc1
Lance	lance	k1gNnSc2
Black	Black	k1gMnSc1
in	in	k?
Times	Times	k1gMnSc1
newspaper	newspaper	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Newsbeat	Newsbeat	k1gInSc1
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-10-01	2015-10-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
;	;	kIx,
Famous	Famous	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympionik	olympionik	k1gMnSc1
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gMnSc2
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
snoubence	snoubenec	k1gMnPc4
při	při	k7c6
obřadu	obřad	k1gInSc6
jako	jako	k8xC,k8xS
z	z	k7c2
Romea	Romeo	k1gMnSc2
a	a	k8xC
Julie	Julie	k1gFnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-08	2017-05-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
STROUDE	STROUDE	kA
<g/>
,	,	kIx,
Will	Will	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Watch	Watch	k1gInSc1
Daley	Dalea	k1gFnSc2
thank	thanka	k1gFnPc2
fans	fans	k1gInSc1
after	after	k1gMnSc1
topping	topping	k1gInSc1
Attitude	Attitud	k1gInSc5
HOT	hot	k0
100	#num#	k4
poll	polla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-07-17	2014-07-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
</s>
<s>
STROUDE	STROUDE	kA
<g/>
,	,	kIx,
Will	Will	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gInSc5
HOT	hot	k0
100	#num#	k4
<g/>
:	:	kIx,
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
voted	voted	k1gMnSc1
world	world	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
sexiest	sexiest	k1gMnSc1
man	man	k1gMnSc1
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Attitude	Attitud	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-07-16	2014-07-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
;	;	kIx,
Famous	Famous	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejatraktivnějším	atraktivní	k2eAgMnSc7d3
mužem	muž	k1gMnSc7
současnosti	současnost	k1gFnSc2
podle	podle	k7c2
Attitude	Attitud	k1gInSc5
je	on	k3xPp3gNnPc4
gay	gay	k1gMnSc1
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revue	revue	k1gFnSc1
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-07-19	2014-07-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
TomDaley	TomDalea	k1gFnSc2
<g/>
.	.	kIx.
<g/>
tv	tv	k?
s	s	k7c7
vlogem	vlog	k1gInSc7
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Tom	Tom	k1gMnSc1
Daley	Dalea	k1gFnSc2
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
20081127006	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5681	#num#	k4
4012	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2012012736	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84279543	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2012012736	#num#	k4
</s>
