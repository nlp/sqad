<s>
Monika	Monika	k1gFnSc1	Monika
Žídková	Žídková	k1gFnSc1	Žídková
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Monika	Monika	k1gFnSc1	Monika
Brzesková	Brzesková	k1gFnSc1	Brzesková
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Kravaře	Kravaře	k1gInPc1	Kravaře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
bývalá	bývalý	k2eAgFnSc1d1	bývalá
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
Miss	miss	k1gFnSc1	miss
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
1995	[number]	k4	1995
a	a	k8xC	a
Miss	miss	k1gFnSc1	miss
Europe	Europ	k1gInSc5	Europ
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
podnikatelka	podnikatelka	k1gFnSc1	podnikatelka
<g/>
,	,	kIx,	,
příležitostná	příležitostný	k2eAgFnSc1d1	příležitostná
moderátorka	moderátorka	k1gFnSc1	moderátorka
a	a	k8xC	a
starostka	starostka	k1gFnSc1	starostka
Kravař	Kravaře	k1gInPc2	Kravaře
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
Střední	střední	k2eAgFnSc4d1	střední
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Krnově	Krnov	k1gInSc6	Krnov
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
Ostravské	ostravský	k2eAgFnSc6d1	Ostravská
univerzitě	univerzita	k1gFnSc6	univerzita
obor	obor	k1gInSc4	obor
Učitelství	učitelství	k1gNnSc2	učitelství
pro	pro	k7c4	pro
nižší	nízký	k2eAgInSc4d2	nižší
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
sedmnáctiletá	sedmnáctiletý	k2eAgFnSc1d1	sedmnáctiletá
studentka	studentka	k1gFnSc1	studentka
stala	stát	k5eAaPmAgFnS	stát
Miss	miss	k1gFnSc4	miss
Krnov	Krnov	k1gInSc1	Krnov
a	a	k8xC	a
v	v	k7c6	v
krnovské	krnovský	k2eAgFnSc6d1	Krnovská
soutěži	soutěž	k1gFnSc6	soutěž
tehdy	tehdy	k6eAd1	tehdy
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
také	také	k9	také
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Miss	miss	k1gFnSc1	miss
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
Miss	miss	k1gFnSc1	miss
Rýmařov	Rýmařovo	k1gNnPc2	Rýmařovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
nejdříve	dříve	k6eAd3	dříve
Miss	miss	k1gFnSc4	miss
severní	severní	k2eAgFnSc2d1	severní
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Miss	miss	k1gFnSc1	miss
Moravia	Moravia	k1gFnSc1	Moravia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
soutěž	soutěž	k1gFnSc1	soutěž
krásy	krása	k1gFnSc2	krása
Miss	miss	k1gFnPc2	miss
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ČR	ČR	kA	ČR
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
krásy	krása	k1gFnSc2	krása
Miss	miss	k1gFnSc2	miss
Europe	Europ	k1gMnSc5	Europ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
jako	jako	k9	jako
Miss	miss	k1gFnSc1	miss
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
soutěž	soutěž	k1gFnSc4	soutěž
Miss	miss	k1gFnSc1	miss
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
spojena	spojen	k2eAgFnSc1d1	spojena
se	se	k3xPyFc4	se
soutěží	soutěžit	k5eAaImIp3nS	soutěžit
Česká	český	k2eAgFnSc1d1	Česká
Miss	miss	k1gFnSc1	miss
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Žídková	Žídková	k1gFnSc1	Žídková
ve	v	k7c6	v
finálovém	finálový	k2eAgInSc6d1	finálový
večeru	večer	k1gInSc6	večer
stala	stát	k5eAaPmAgFnS	stát
vítězkou	vítězka	k1gFnSc7	vítězka
ankety	anketa	k1gFnSc2	anketa
Nejoblíbenější	oblíbený	k2eAgFnSc2d3	nejoblíbenější
miss	miss	k1gFnSc2	miss
v	v	k7c4	v
historie	historie	k1gFnPc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
taneční	taneční	k2eAgFnSc1d1	taneční
televizní	televizní	k2eAgFnSc1d1	televizní
soutěže	soutěž	k1gFnPc1	soutěž
Staance	Staance	k1gFnSc2	Staance
...	...	k?	...
<g/>
když	když	k8xS	když
hvězdy	hvězda	k1gFnPc1	hvězda
tančí	tančit	k5eAaImIp3nP	tančit
II	II	kA	II
<g/>
..	..	k?	..
Má	mít	k5eAaImIp3nS	mít
sestru	sestra	k1gFnSc4	sestra
Lucii	Lucie	k1gFnSc4	Lucie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
a	a	k8xC	a
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
Petrem	Petr	k1gMnSc7	Petr
Brzeskou	Brzeský	k2eAgFnSc7d1	Brzeský
založila	založit	k5eAaPmAgFnS	založit
a	a	k8xC	a
spoluvlastní	spoluvlastnit	k5eAaImIp3nS	spoluvlastnit
firmu	firma	k1gFnSc4	firma
Miss	miss	k1gFnSc4	miss
cosmetic	cosmetice	k1gFnPc2	cosmetice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
přírodní	přírodní	k2eAgFnSc7d1	přírodní
kosmetikou	kosmetika	k1gFnSc7	kosmetika
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Nikol	nikol	k1gInSc4	nikol
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
Davida	David	k1gMnSc2	David
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
Kravař	kravař	k1gMnSc1	kravař
jako	jako	k8xC	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
sdružení	sdružení	k1gNnSc4	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
Za	za	k7c4	za
Kravaře	Kravaře	k1gInPc4	Kravaře
prosperující	prosperující	k2eAgInPc4d1	prosperující
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ustavujícím	ustavující	k2eAgNnSc6d1	ustavující
zasedání	zasedání	k1gNnSc6	zasedání
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zvolena	zvolit	k5eAaPmNgFnS	zvolit
starostkou	starostka	k1gFnSc7	starostka
<g/>
.	.	kIx.	.
</s>
