<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Dunkerque	Dunkerqu	k1gFnSc2	Dunkerqu
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
díl	díl	k1gInSc4	díl
historie	historie	k1gFnSc2	historie
Československé	československý	k2eAgFnSc2d1	Československá
samostatné	samostatný	k2eAgFnPc4d1	samostatná
brigády	brigáda	k1gFnPc4	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
samostatná	samostatný	k2eAgFnSc1d1	samostatná
brigáda	brigáda	k1gFnSc1	brigáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přeplavila	přeplavit	k5eAaPmAgFnS	přeplavit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
prozatím	prozatím	k6eAd1	prozatím
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
brigády	brigáda	k1gFnSc2	brigáda
Alois	Alois	k1gMnSc1	Alois
Liška	Liška	k1gMnSc1	Liška
žádal	žádat	k5eAaImAgMnS	žádat
české	český	k2eAgInPc4d1	český
úřady	úřad	k1gInPc4	úřad
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
českoslovenští	československý	k2eAgMnPc1d1	československý
vojáci	voják	k1gMnPc1	voják
odesláni	odeslán	k2eAgMnPc1d1	odeslán
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
brigáda	brigáda	k1gFnSc1	brigáda
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
požadovanou	požadovaný	k2eAgFnSc7d1	požadovaná
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
však	však	k9	však
obávala	obávat	k5eAaImAgFnS	obávat
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
brigáda	brigáda	k1gFnSc1	brigáda
nebude	být	k5eNaImBp3nS	být
moci	moct	k5eAaImF	moct
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nechtěla	chtít	k5eNaImAgFnS	chtít
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojáky	voják	k1gMnPc4	voják
pověřit	pověřit	k5eAaPmF	pověřit
úkoly	úkol	k1gInPc4	úkol
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
postupně	postupně	k6eAd1	postupně
doplňovala	doplňovat	k5eAaImAgFnS	doplňovat
stavy	stav	k1gInPc4	stav
bojové	bojový	k2eAgFnSc2d1	bojová
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
září	září	k1gNnSc2	září
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
přesunuta	přesunut	k2eAgFnSc1d1	přesunuta
k	k	k7c3	k
francouzskému	francouzský	k2eAgInSc3d1	francouzský
přístavu	přístav	k1gInSc3	přístav
Dunkerque	Dunkerqu	k1gMnSc2	Dunkerqu
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
obklíčen	obklíčit	k5eAaPmNgMnS	obklíčit
<g/>
,	,	kIx,	,
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
vybudovány	vybudován	k2eAgFnPc4d1	vybudována
tři	tři	k4xCgFnPc4	tři
obranné	obranný	k2eAgFnPc4d1	obranná
linie	linie	k1gFnPc4	linie
<g/>
,	,	kIx,	,
síť	síť	k1gFnSc4	síť
plavebních	plavební	k2eAgInPc2d1	plavební
kanálů	kanál	k1gInPc2	kanál
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
Němcům	Němec	k1gMnPc3	Němec
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
zatopení	zatopení	k1gNnSc4	zatopení
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
předpolí	předpolí	k1gNnSc2	předpolí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
dvanáct	dvanáct	k4xCc1	dvanáct
tisíc	tisíc	k4xCgInPc2	tisíc
příslušníků	příslušník	k1gMnPc2	příslušník
německých	německý	k2eAgFnPc2d1	německá
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgMnSc4	ten
tvořili	tvořit	k5eAaImAgMnP	tvořit
asi	asi	k9	asi
2	[number]	k4	2
tisíce	tisíc	k4xCgInSc2	tisíc
příslušníci	příslušník	k1gMnPc1	příslušník
Waffen	Waffna	k1gFnPc2	Waffna
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Českoslovenští	československý	k2eAgMnPc1d1	československý
vojáci	voják	k1gMnPc1	voják
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
svá	svůj	k3xOyFgNnPc4	svůj
obranná	obranný	k2eAgNnPc4d1	obranné
postavení	postavení	k1gNnPc4	postavení
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
britskými	britský	k2eAgFnPc7d1	britská
a	a	k8xC	a
francouzskými	francouzský	k2eAgFnPc7d1	francouzská
jednotkami	jednotka	k1gFnPc7	jednotka
zahájili	zahájit	k5eAaPmAgMnP	zahájit
samotné	samotný	k2eAgFnPc4d1	samotná
obléhání	obléhání	k1gNnPc4	obléhání
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
než	než	k8xS	než
se	se	k3xPyFc4	se
stačili	stačit	k5eAaBmAgMnP	stačit
seznámit	seznámit	k5eAaPmF	seznámit
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
terénem	terén	k1gInSc7	terén
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
čelit	čelit	k5eAaImF	čelit
nepřátelskému	přátelský	k2eNgInSc3d1	nepřátelský
výpadu	výpad	k1gInSc3	výpad
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
noční	noční	k2eAgInSc4d1	noční
útok	útok	k1gInSc4	útok
byl	být	k5eAaImAgInS	být
odražen	odražen	k2eAgMnSc1d1	odražen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
odpoledne	odpoledne	k1gNnSc2	odpoledne
vrhli	vrhnout	k5eAaImAgMnP	vrhnout
Němci	Němec	k1gMnPc1	Němec
do	do	k7c2	do
boje	boj	k1gInSc2	boj
pěchotu	pěchota	k1gFnSc4	pěchota
a	a	k8xC	a
dokázali	dokázat	k5eAaPmAgMnP	dokázat
obsadit	obsadit	k5eAaPmF	obsadit
některá	některý	k3yIgNnPc4	některý
předsunutá	předsunutý	k2eAgNnPc4d1	předsunuté
postavení	postavení	k1gNnPc4	postavení
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
linii	linie	k1gFnSc4	linie
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
obnovit	obnovit	k5eAaPmF	obnovit
až	až	k9	až
následujícího	následující	k2eAgNnSc2d1	následující
rána	ráno	k1gNnSc2	ráno
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
britských	britský	k2eAgMnPc2d1	britský
tankistů	tankista	k1gMnPc2	tankista
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
boj	boj	k1gInSc1	boj
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
úkol	úkol	k1gInSc1	úkol
brigády	brigáda	k1gFnSc2	brigáda
bude	být	k5eAaImBp3nS	být
skutečně	skutečně	k6eAd1	skutečně
spočívat	spočívat	k5eAaImF	spočívat
především	především	k9	především
v	v	k7c6	v
průzkumné	průzkumný	k2eAgFnSc6d1	průzkumná
činnosti	činnost	k1gFnSc6	činnost
a	a	k8xC	a
kontrolování	kontrolování	k1gNnSc6	kontrolování
aktivity	aktivita	k1gFnSc2	aktivita
daleko	daleko	k6eAd1	daleko
silnějšího	silný	k2eAgMnSc2d2	silnější
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
větší	veliký	k2eAgFnSc1d2	veliký
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
československých	československý	k2eAgFnPc2d1	Československá
jednotek	jednotka	k1gFnPc2	jednotka
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c4	v
den	den	k1gInSc4	den
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
pěchoty	pěchota	k1gFnSc2	pěchota
podporovaní	podporovaný	k2eAgMnPc1d1	podporovaný
2	[number]	k4	2
<g/>
.	.	kIx.	.
tankovým	tankový	k2eAgInSc7d1	tankový
praporem	prapor	k1gInSc7	prapor
provedli	provést	k5eAaPmAgMnP	provést
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
nepřítel	nepřítel	k1gMnSc1	nepřítel
odepsal	odepsat	k5eAaPmAgMnS	odepsat
ze	z	k7c2	z
stavu	stav	k1gInSc2	stav
50	[number]	k4	50
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
14	[number]	k4	14
zajatých	zajatá	k1gFnPc2	zajatá
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
druhý	druhý	k4xOgInSc1	druhý
útok	útok	k1gInSc1	útok
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
padlo	padnout	k5eAaPmAgNnS	padnout
150	[number]	k4	150
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
6	[number]	k4	6
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
350	[number]	k4	350
mužů	muž	k1gMnPc2	muž
padlo	padnout	k5eAaImAgNnS	padnout
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
brigáda	brigáda	k1gFnSc1	brigáda
měla	mít	k5eAaImAgFnS	mít
menší	malý	k2eAgFnPc4d2	menší
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
při	při	k7c6	při
obou	dva	k4xCgFnPc6	dva
akcích	akce	k1gFnPc6	akce
padlo	padnout	k5eAaImAgNnS	padnout
36	[number]	k4	36
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
3	[number]	k4	3
zůstali	zůstat	k5eAaPmAgMnP	zůstat
nezvěstní	zvěstný	k2eNgMnPc1d1	nezvěstný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
brigády	brigáda	k1gFnSc2	brigáda
blahopřál	blahopřát	k5eAaImAgInS	blahopřát
i	i	k9	i
velitel	velitel	k1gMnSc1	velitel
britské	britský	k2eAgFnSc2d1	britská
21	[number]	k4	21
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
maršál	maršál	k1gMnSc1	maršál
Montgomery	Montgomera	k1gFnSc2	Montgomera
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
bojová	bojový	k2eAgFnSc1d1	bojová
akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
náletem	nálet	k1gInSc7	nálet
britských	britský	k2eAgNnPc2d1	Britské
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
ničila	ničit	k5eAaImAgNnP	ničit
raketami	raketa	k1gFnPc7	raketa
palebná	palebný	k2eAgFnSc1d1	palebná
postavení	postavení	k1gNnSc6	postavení
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dělostřelecké	dělostřelecký	k2eAgFnSc6d1	dělostřelecká
přípravě	příprava	k1gFnSc6	příprava
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
na	na	k7c4	na
zteč	zteč	k1gFnSc4	zteč
43	[number]	k4	43
československých	československý	k2eAgInPc2d1	československý
tanků	tank	k1gInPc2	tank
s	s	k7c7	s
pěchotou	pěchota	k1gFnSc7	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
však	však	k9	však
kladli	klást	k5eAaImAgMnP	klást
oproti	oproti	k7c3	oproti
předchozímu	předchozí	k2eAgInSc3d1	předchozí
útoku	útok	k1gInSc3	útok
silný	silný	k2eAgInSc1d1	silný
odpor	odpor	k1gInSc1	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
československým	československý	k2eAgMnPc3d1	československý
vojákům	voják	k1gMnPc3	voják
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
pevnůstky	pevnůstka	k1gFnPc4	pevnůstka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
tanky	tank	k1gInPc1	tank
uvázly	uváznout	k5eAaPmAgInP	uváznout
v	v	k7c6	v
minových	minový	k2eAgNnPc6d1	minové
polích	pole	k1gNnPc6	pole
a	a	k8xC	a
pěchota	pěchota	k1gFnSc1	pěchota
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gFnSc2	jejich
podpory	podpora	k1gFnSc2	podpora
nemohla	moct	k5eNaImAgFnS	moct
dokončit	dokončit	k5eAaPmF	dokončit
úkol	úkol	k1gInSc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgFnP	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnSc2	vojsko
ztratila	ztratit	k5eAaPmAgFnS	ztratit
150	[number]	k4	150
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
170	[number]	k4	170
bylo	být	k5eAaImAgNnS	být
zajato	zajat	k2eAgNnSc1d1	zajato
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
jednotky	jednotka	k1gFnPc1	jednotka
přišly	přijít	k5eAaPmAgFnP	přijít
o	o	k7c4	o
26	[number]	k4	26
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
9	[number]	k4	9
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
stalo	stát	k5eAaPmAgNnS	stát
nezvěstných	zvěstný	k2eNgMnPc2d1	nezvěstný
<g/>
,	,	kIx,	,
59	[number]	k4	59
vojáků	voják	k1gMnPc2	voják
bylo	být	k5eAaImAgNnS	být
raněno	ranit	k5eAaPmNgNnS	ranit
<g/>
.	.	kIx.	.
</s>
<s>
Padlí	padlý	k2eAgMnPc1d1	padlý
českoslovenští	československý	k2eAgMnPc1d1	československý
vojáci	voják	k1gMnPc1	voják
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
střetů	střet	k1gInPc2	střet
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
na	na	k7c6	na
vojenském	vojenský	k2eAgInSc6d1	vojenský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Adinkerke	Adinkerke	k1gFnSc6	Adinkerke
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
průběhu	průběh	k1gInSc6	průběh
bojů	boj	k1gInPc2	boj
docházelo	docházet	k5eAaImAgNnS	docházet
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
nevýznamným	významný	k2eNgFnPc3d1	nevýznamná
akcím	akce	k1gFnPc3	akce
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc2d1	způsobená
průzkumnou	průzkumný	k2eAgFnSc7d1	průzkumná
a	a	k8xC	a
hlídkovou	hlídkový	k2eAgFnSc7d1	hlídková
činností	činnost	k1gFnSc7	činnost
nebo	nebo	k8xC	nebo
dělostřeleckými	dělostřelecký	k2eAgInPc7d1	dělostřelecký
duely	duel	k1gInPc7	duel
<g/>
.	.	kIx.	.
</s>
<s>
Velení	velení	k1gNnSc1	velení
čs	čs	kA	čs
<g/>
.	.	kIx.	.
brigády	brigáda	k1gFnSc2	brigáda
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
působit	působit	k5eAaImF	působit
na	na	k7c4	na
nepřítele	nepřítel	k1gMnSc4	nepřítel
i	i	k9	i
psychologicky	psychologicky	k6eAd1	psychologicky
<g/>
,	,	kIx,	,
když	když	k8xS	když
vysílalo	vysílat	k5eAaImAgNnS	vysílat
z	z	k7c2	z
radiostanic	radiostanice	k1gFnPc2	radiostanice
relace	relace	k1gFnSc2	relace
německým	německý	k2eAgMnSc7d1	německý
vojákům	voják	k1gMnPc3	voják
či	či	k9wB	či
nechalo	nechat	k5eAaPmAgNnS	nechat
vystřelovat	vystřelovat	k5eAaImF	vystřelovat
granáty	granát	k1gInPc4	granát
s	s	k7c7	s
letáky	leták	k1gInPc7	leták
a	a	k8xC	a
dalším	další	k2eAgInSc7d1	další
propagandistickým	propagandistický	k2eAgInSc7d1	propagandistický
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
669	[number]	k4	669
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
9	[number]	k4	9
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
boje	boj	k1gInPc1	boj
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
odrazili	odrazit	k5eAaPmAgMnP	odrazit
čs	čs	kA	čs
<g/>
.	.	kIx.	.
tankisté	tankista	k1gMnPc1	tankista
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
z	z	k7c2	z
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
rotou	rota	k1gFnSc7	rota
německý	německý	k2eAgInSc1d1	německý
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
výpad	výpad	k1gInSc4	výpad
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
perimetru	perimetr	k1gInSc2	perimetr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
podnikl	podniknout	k5eAaPmAgMnS	podniknout
nepřítel	nepřítel	k1gMnSc1	nepřítel
nový	nový	k2eAgInSc4d1	nový
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
místních	místní	k2eAgInPc2d1	místní
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
zahájili	zahájit	k5eAaPmAgMnP	zahájit
čeští	český	k2eAgMnPc1d1	český
<g/>
,	,	kIx,	,
slovenští	slovenský	k2eAgMnPc1d1	slovenský
<g/>
,	,	kIx,	,
britští	britský	k2eAgMnPc1d1	britský
a	a	k8xC	a
francouzští	francouzský	k2eAgMnPc1d1	francouzský
vojáci	voják	k1gMnPc1	voják
protiútok	protiútok	k1gInSc4	protiútok
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
skončila	skončit	k5eAaPmAgFnS	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
zde	zde	k6eAd1	zde
zuřily	zuřit	k5eAaImAgInP	zuřit
do	do	k7c2	do
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
podstatnějších	podstatný	k2eAgFnPc2d2	podstatnější
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
chýlila	chýlit	k5eAaImAgFnS	chýlit
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
konci	konec	k1gInSc3	konec
a	a	k8xC	a
americké	americký	k2eAgFnSc2d1	americká
jednotky	jednotka	k1gFnSc2	jednotka
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
chebského	chebský	k2eAgInSc2d1	chebský
výběžku	výběžek	k1gInSc2	výběžek
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
oživena	oživen	k2eAgFnSc1d1	oživena
myšlenka	myšlenka	k1gFnSc1	myšlenka
uvolnění	uvolnění	k1gNnSc2	uvolnění
čs	čs	kA	čs
<g/>
.	.	kIx.	.
brigády	brigáda	k1gFnPc1	brigáda
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
odjel	odjet	k5eAaPmAgInS	odjet
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pouze	pouze	k6eAd1	pouze
narychlo	narychlo	k6eAd1	narychlo
sestavený	sestavený	k2eAgInSc4d1	sestavený
oddíl	oddíl	k1gInSc4	oddíl
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
pplk.	pplk.	kA	pplk.
Sítkem	sítko	k1gNnSc7	sítko
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c6	na
území	území	k1gNnSc6	území
předválečné	předválečný	k2eAgFnSc2d1	předválečná
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
večer	večer	k6eAd1	večer
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
v	v	k7c4	v
půl	půl	k6eAd1	půl
desáté	desátý	k4xOgNnSc4	desátý
dopoledne	dopoledne	k1gNnSc4	dopoledne
podepsal	podepsat	k5eAaPmAgMnS	podepsat
viceadmirál	viceadmirál	k1gMnSc1	viceadmirál
Friedrich	Friedrich	k1gMnSc1	Friedrich
Frisius	Frisius	k1gMnSc1	Frisius
na	na	k7c6	na
velitelství	velitelství	k1gNnSc6	velitelství
brigády	brigáda	k1gFnSc2	brigáda
kapitulaci	kapitulace	k1gFnSc3	kapitulace
tamní	tamní	k2eAgFnSc2d1	tamní
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Generálu	generál	k1gMnSc3	generál
Liškovi	Liška	k1gMnSc3	Liška
se	se	k3xPyFc4	se
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
asi	asi	k9	asi
10	[number]	k4	10
500	[number]	k4	500
německých	německý	k2eAgMnPc2d1	německý
obránců	obránce	k1gMnPc2	obránce
Dunkerque	Dunkerqu	k1gFnSc2	Dunkerqu
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
francouzským	francouzský	k2eAgInSc7d1	francouzský
přístavem	přístav	k1gInSc7	přístav
zavlála	zavlát	k5eAaPmAgFnS	zavlát
společně	společně	k6eAd1	společně
československá	československý	k2eAgFnSc1d1	Československá
a	a	k8xC	a
britská	britský	k2eAgFnSc1d1	britská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Bitvy	bitva	k1gFnPc1	bitva
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
Jitka	Jitka	k1gFnSc1	Jitka
Lenková	Lenková	k1gFnSc1	Lenková
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Pavlík	Pavlík	k1gMnSc1	Pavlík
<g/>
:	:	kIx,	:
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
bitvy	bitva	k1gFnPc1	bitva
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
Obléhání	obléhání	k1gNnSc2	obléhání
Dunkerque	Dunkerqu	k1gMnSc2	Dunkerqu
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
</s>
