<s>
Československá	československý	k2eAgFnSc1d1	Československá
samostatná	samostatný	k2eAgFnSc1d1	samostatná
brigáda	brigáda	k1gFnSc1	brigáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přeplavila	přeplavit	k5eAaPmAgFnS	přeplavit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
prozatím	prozatím	k6eAd1	prozatím
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
<g/>
.	.	kIx.	.
</s>
