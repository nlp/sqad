<s>
Béowulf	Béowulf	k1gInSc1	Béowulf
(	(	kIx(	(
<g/>
staroanglicky	staroanglicky	k6eAd1	staroanglicky
Vlk	Vlk	k1gMnSc1	Vlk
včel	včela	k1gFnPc2	včela
-	-	kIx~	-
kenning	kenning	k1gInSc1	kenning
pro	pro	k7c4	pro
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
staroanglický	staroanglický	k2eAgInSc4d1	staroanglický
hrdinský	hrdinský	k2eAgInSc4d1	hrdinský
epos	epos	k1gInSc4	epos
psaný	psaný	k2eAgInSc4d1	psaný
aliteračním	aliterační	k2eAgInSc7d1	aliterační
veršem	verš	k1gInSc7	verš
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
3182	[number]	k4	3182
verši	verš	k1gInPc7	verš
představuje	představovat	k5eAaImIp3nS	představovat
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
dílo	dílo	k1gNnSc1	dílo
staroanglické	staroanglický	k2eAgFnSc2d1	staroanglická
literatury	literatura	k1gFnSc2	literatura
<g/>
;	;	kIx,	;
zároveň	zároveň	k6eAd1	zároveň
tvoří	tvořit	k5eAaImIp3nS	tvořit
10	[number]	k4	10
%	%	kIx~	%
veškerého	veškerý	k3xTgInSc2	veškerý
textu	text	k1gInSc2	text
zachovaného	zachovaný	k2eAgInSc2d1	zachovaný
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
středověkých	středověký	k2eAgNnPc2d1	středověké
děl	dělo	k1gNnPc2	dělo
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
<g/>
,	,	kIx,	,
epos	epos	k1gInSc1	epos
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc1	žádný
původní	původní	k2eAgInSc1d1	původní
dobový	dobový	k2eAgInSc1d1	dobový
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
báseň	báseň	k1gFnSc1	báseň
označuje	označovat	k5eAaImIp3nS	označovat
jménem	jméno	k1gNnSc7	jméno
jejího	její	k3xOp3gMnSc2	její
hrdiny	hrdina	k1gMnSc2	hrdina
Béowulfa	Béowulf	k1gMnSc2	Béowulf
<g/>
.	.	kIx.	.
</s>
<s>
Béowulf	Béowulf	k1gInSc1	Béowulf
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
rukopise	rukopis	k1gInSc6	rukopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
knihovně	knihovna	k1gFnSc6	knihovna
(	(	kIx(	(
<g/>
British	British	k1gInSc1	British
Library	Librara	k1gFnSc2	Librara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kodex	kodex	k1gInSc1	kodex
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
knihovny	knihovna	k1gFnSc2	knihovna
sběratele	sběratel	k1gMnSc2	sběratel
sira	sir	k1gMnSc2	sir
Roberta	Robert	k1gMnSc2	Robert
Cottona	Cotton	k1gMnSc2	Cotton
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
původně	původně	k6eAd1	původně
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
,	,	kIx,	,
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
Cotton	Cotton	k1gInSc1	Cotton
Vitellius	Vitellius	k1gMnSc1	Vitellius
A.	A.	kA	A.
<g/>
xv	xv	k?	xv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1731	[number]	k4	1731
byl	být	k5eAaImAgInS	být
částečně	částečně	k6eAd1	částečně
poškozen	poškodit	k5eAaPmNgInS	poškodit
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
v	v	k7c4	v
Burnham	Burnham	k1gInSc4	Burnham
House	house	k1gNnSc1	house
<g/>
.	.	kIx.	.
</s>
<s>
Islandský	islandský	k2eAgMnSc1d1	islandský
učenec	učenec	k1gMnSc1	učenec
Grímur	Grímur	k1gMnSc1	Grímur
Jónsson	Jónsson	k1gMnSc1	Jónsson
Thorkelin	Thorkelina	k1gFnPc2	Thorkelina
nechal	nechat	k5eAaPmAgMnS	nechat
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
v	v	k7c6	v
Britském	britský	k2eAgNnSc6d1	Britské
muzeu	muzeum	k1gNnSc6	muzeum
pořídit	pořídit	k5eAaPmF	pořídit
jeho	jeho	k3xOp3gInSc1	jeho
opis	opis	k1gInSc1	opis
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
další	další	k2eAgFnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
špatnému	špatný	k2eAgInSc3d1	špatný
stavu	stav	k1gInSc3	stav
originálu	originál	k1gInSc2	originál
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgInPc4	tento
opisy	opis	k1gInPc4	opis
pro	pro	k7c4	pro
moderní	moderní	k2eAgMnPc4d1	moderní
badatele	badatel	k1gMnPc4	badatel
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Zápis	zápis	k1gInSc1	zápis
Béowulfa	Béowulf	k1gMnSc2	Béowulf
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
sbírky	sbírka	k1gFnSc2	sbírka
zvané	zvaný	k2eAgFnSc2d1	zvaná
Nowell	Nowell	k1gMnSc1	Nowell
Codex	Codex	k1gInSc1	Codex
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
kodexem	kodex	k1gInSc7	kodex
Southwick	Southwick	k1gInSc1	Southwick
tvoří	tvořit	k5eAaImIp3nP	tvořit
svazek	svazek	k1gInSc4	svazek
Cotton	Cotton	k1gInSc1	Cotton
Vitellius	Vitellius	k1gInSc1	Vitellius
<g/>
.	.	kIx.	.
</s>
<s>
Nowell	Nowell	k1gInSc1	Nowell
Codex	Codex	k1gInSc1	Codex
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dále	daleko	k6eAd2	daleko
další	další	k2eAgInPc4d1	další
staroanglické	staroanglický	k2eAgInPc4d1	staroanglický
prozaické	prozaický	k2eAgInPc4d1	prozaický
i	i	k8xC	i
básnické	básnický	k2eAgInPc4d1	básnický
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiná	k1gFnPc7	jiná
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
zlomek	zlomek	k1gInSc4	zlomek
básně	báseň	k1gFnSc2	báseň
Judith	Juditha	k1gFnPc2	Juditha
<g/>
.	.	kIx.	.
</s>
<s>
Nowell	Nowell	k1gInSc1	Nowell
Codex	Codex	k1gInSc1	Codex
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
dvěma	dva	k4xCgMnPc7	dva
různými	různý	k2eAgMnPc7d1	různý
písaři	písař	k1gMnPc7	písař
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc6	první
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
prozaické	prozaický	k2eAgInPc4d1	prozaický
texty	text	k1gInPc4	text
a	a	k8xC	a
Béowulfa	Béowulf	k1gMnSc4	Béowulf
po	po	k7c4	po
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
verš	verš	k1gInSc1	verš
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
vzniku	vznik	k1gInSc2	vznik
rukopisu	rukopis	k1gInSc2	rukopis
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
paleografických	paleografický	k2eAgFnPc2d1	paleografická
indicií	indicie	k1gFnPc2	indicie
pokládá	pokládat	k5eAaImIp3nS	pokládat
zhruba	zhruba	k6eAd1	zhruba
rok	rok	k1gInSc4	rok
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
sama	sám	k3xTgMnSc4	sám
je	být	k5eAaImIp3nS	být
však	však	k9	však
zřejmě	zřejmě	k6eAd1	zřejmě
starší	starý	k2eAgFnSc1d2	starší
<g/>
,	,	kIx,	,
přesná	přesný	k2eAgFnSc1d1	přesná
doba	doba	k1gFnSc1	doba
vzniku	vznik	k1gInSc2	vznik
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Starodávná	starodávný	k2eAgNnPc1d1	starodávné
slova	slovo	k1gNnPc1	slovo
v	v	k7c6	v
textu	text	k1gInSc6	text
by	by	kYmCp3nS	by
napovídala	napovídat	k5eAaBmAgFnS	napovídat
první	první	k4xOgFnSc4	první
polovinu	polovina	k1gFnSc4	polovina
osmého	osmý	k4xOgNnSc2	osmý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c6	o
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
<s>
Jazykem	jazyk	k1gInSc7	jazyk
básně	báseň	k1gFnSc2	báseň
je	být	k5eAaImIp3nS	být
pozdní	pozdní	k2eAgFnSc1d1	pozdní
západní	západní	k2eAgFnSc1d1	západní
saština	saština	k1gFnSc1	saština
(	(	kIx(	(
<g/>
west	west	k2eAgInSc1d1	west
Saxon	Saxon	k1gInSc1	Saxon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
stopy	stopa	k1gFnPc1	stopa
jiných	jiný	k2eAgMnPc2d1	jiný
anglosaských	anglosaský	k2eAgMnPc2d1	anglosaský
dialektů	dialekt	k1gInPc2	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
náznaků	náznak	k1gInPc2	náznak
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
báseň	báseň	k1gFnSc1	báseň
původně	původně	k6eAd1	původně
složena	složit	k5eAaPmNgFnS	složit
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
dialektu	dialekt	k1gInSc6	dialekt
Anglů	Angl	k1gMnPc2	Angl
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
mercijštině	mercijština	k1gFnSc6	mercijština
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
básně	báseň	k1gFnSc2	báseň
se	se	k3xPyFc4	se
neodehrává	odehrávat	k5eNaImIp3nS	odehrávat
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
Švédska	Švédsko	k1gNnSc2	Švédsko
pátého	pátý	k4xOgMnSc2	pátý
a	a	k8xC	a
šestého	šestý	k4xOgNnSc2	šestý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
skutečných	skutečný	k2eAgFnPc6d1	skutečná
historických	historický	k2eAgFnPc6d1	historická
osobnostech	osobnost	k1gFnPc6	osobnost
(	(	kIx(	(
<g/>
géatský	géatský	k2eAgMnSc1d1	géatský
král	král	k1gMnSc1	král
Hygelák	Hygelák	k1gMnSc1	Hygelák
<g/>
,	,	kIx,	,
mercijský	mercijský	k2eAgMnSc1d1	mercijský
král	král	k1gMnSc1	král
Offa	Offa	k1gMnSc1	Offa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Látka	látka	k1gFnSc1	látka
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
zřejmě	zřejmě	k6eAd1	zřejmě
společně	společně	k6eAd1	společně
s	s	k7c7	s
Angly	Angl	k1gMnPc7	Angl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
sem	sem	k6eAd1	sem
od	od	k7c2	od
pátého	pátý	k4xOgNnSc2	pátý
století	století	k1gNnSc2	století
přesídlovali	přesídlovat	k5eAaImAgMnP	přesídlovat
z	z	k7c2	z
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Vyprávění	vyprávění	k1gNnSc1	vyprávění
sleduje	sledovat	k5eAaImIp3nS	sledovat
osud	osud	k1gInSc1	osud
mladého	mladý	k2eAgMnSc2d1	mladý
hrdiny	hrdina	k1gMnSc2	hrdina
Béowulfa	Béowulf	k1gMnSc2	Béowulf
z	z	k7c2	z
géatského	géatský	k2eAgInSc2d1	géatský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Identita	identita	k1gFnSc1	identita
Géatů	Géata	k1gMnPc2	Géata
není	být	k5eNaImIp3nS	být
spolehlivě	spolehlivě	k6eAd1	spolehlivě
vysvětlena	vysvětlen	k2eAgFnSc1d1	vysvětlena
<g/>
;	;	kIx,	;
mohlo	moct	k5eAaImAgNnS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
Gauty	Gaut	k1gMnPc4	Gaut
<g/>
,	,	kIx,	,
Góty	Gót	k1gMnPc4	Gót
nebo	nebo	k8xC	nebo
i	i	k9	i
Juty	juta	k1gFnSc2	juta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejpravděpodobnější	pravděpodobný	k2eAgMnSc1d3	nejpravděpodobnější
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
Gauty	Gauta	k1gFnPc4	Gauta
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
se	s	k7c7	s
čtrnácti	čtrnáct	k4xCc7	čtrnáct
družiníky	družiník	k1gMnPc7	družiník
do	do	k7c2	do
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohli	pomoct	k5eAaPmAgMnP	pomoct
králi	král	k1gMnSc3	král
Hróthgárovi	Hróthgár	k1gMnSc3	Hróthgár
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
sužuje	sužovat	k5eAaImIp3nS	sužovat
lidožravý	lidožravý	k2eAgMnSc1d1	lidožravý
netvor	netvor	k1gMnSc1	netvor
Grendel	Grendlo	k1gNnPc2	Grendlo
<g/>
,	,	kIx,	,
líčený	líčený	k2eAgMnSc1d1	líčený
jako	jako	k9	jako
obr	obr	k1gMnSc1	obr
nebo	nebo	k8xC	nebo
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
tradici	tradice	k1gFnSc6	tradice
trol	trolit	k5eAaImRp2nS	trolit
<g/>
.	.	kIx.	.
</s>
<s>
Grendel	Grendlo	k1gNnPc2	Grendlo
v	v	k7c4	v
noci	noc	k1gFnPc4	noc
vniká	vnikat	k5eAaImIp3nS	vnikat
do	do	k7c2	do
Hróthgárovy	Hróthgárův	k2eAgFnSc2d1	Hróthgárův
dvorany	dvorana	k1gFnSc2	dvorana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napadá	napadat	k5eAaBmIp3nS	napadat
a	a	k8xC	a
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
královské	královský	k2eAgMnPc4d1	královský
družiníky	družiník	k1gMnPc4	družiník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
soubojích	souboj	k1gInPc6	souboj
Béowulf	Béowulf	k1gInSc1	Béowulf
nejprve	nejprve	k6eAd1	nejprve
smrtelně	smrtelně	k6eAd1	smrtelně
zraní	zranit	k5eAaPmIp3nS	zranit
Grendela	Grendela	k1gFnSc1	Grendela
a	a	k8xC	a
sleduje	sledovat	k5eAaImIp3nS	sledovat
jeho	jeho	k3xOp3gFnSc4	jeho
krvavou	krvavý	k2eAgFnSc4d1	krvavá
stopu	stopa	k1gFnSc4	stopa
až	až	k9	až
k	k	k7c3	k
temnému	temný	k2eAgNnSc3d1	temné
jezeru	jezero	k1gNnSc3	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nakonec	nakonec	k6eAd1	nakonec
pomocí	pomocí	k7c2	pomocí
obřího	obří	k2eAgInSc2d1	obří
meče	meč	k1gInSc2	meč
přemůže	přemoct	k5eAaPmIp3nS	přemoct
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
matku	matka	k1gFnSc4	matka
toužící	toužící	k2eAgFnSc4d1	toužící
po	po	k7c6	po
pomstě	pomsta	k1gFnSc6	pomsta
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
meče	meč	k1gInSc2	meč
se	se	k3xPyFc4	se
však	však	k9	však
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
v	v	k7c6	v
jedovaté	jedovatý	k2eAgFnSc6d1	jedovatá
obří	obří	k2eAgFnSc6d1	obří
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
Béowulfovi	Béowulf	k1gMnSc3	Béowulf
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jen	jen	k6eAd1	jen
jeho	jeho	k3xOp3gInSc1	jeho
skvostný	skvostný	k2eAgInSc1d1	skvostný
jílec	jílec	k1gInSc1	jílec
<g/>
.	.	kIx.	.
</s>
<s>
Béowulf	Béowulf	k1gMnSc1	Béowulf
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vrátí	vrátit	k5eAaPmIp3nS	vrátit
na	na	k7c4	na
dvůr	dvůr	k1gInSc4	dvůr
krále	král	k1gMnSc2	král
Hróthgára	Hróthgár	k1gMnSc2	Hróthgár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
odmění	odměnit	k5eAaPmIp3nS	odměnit
bohatými	bohatý	k2eAgInPc7d1	bohatý
dary	dar	k1gInPc7	dar
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
básně	báseň	k1gFnSc2	báseň
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Béowulf	Béowulf	k1gMnSc1	Béowulf
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
stává	stávat	k5eAaImIp3nS	stávat
géatským	géatský	k2eAgMnSc7d1	géatský
králem	král	k1gMnSc7	král
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
spravedlivě	spravedlivě	k6eAd1	spravedlivě
vládne	vládnout	k5eAaImIp3nS	vládnout
svému	svůj	k3xOyFgInSc3	svůj
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ale	ale	k9	ale
jistý	jistý	k2eAgMnSc1d1	jistý
zloděj	zloděj	k1gMnSc1	zloděj
odcizí	odcizit	k5eAaPmIp3nS	odcizit
klenot	klenot	k1gInSc4	klenot
z	z	k7c2	z
pokladu	poklad	k1gInSc2	poklad
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
mohyle	mohyla	k1gFnSc6	mohyla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hlídal	hlídat	k5eAaImAgMnS	hlídat
ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
drak	drak	k1gInSc4	drak
<g/>
.	.	kIx.	.
</s>
<s>
Krádež	krádež	k1gFnSc1	krádež
draka	drak	k1gMnSc2	drak
rozzuří	rozzuřit	k5eAaPmIp3nS	rozzuřit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyletí	vyletět	k5eAaPmIp3nS	vyletět
z	z	k7c2	z
mohyly	mohyla	k1gFnSc2	mohyla
a	a	k8xC	a
pustoší	pustošit	k5eAaImIp3nS	pustošit
géatskou	géatský	k2eAgFnSc4d1	géatský
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k9	jako
v	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
táhne	táhnout	k5eAaImIp3nS	táhnout
Béowulf	Béowulf	k1gInSc1	Béowulf
s	s	k7c7	s
početným	početný	k2eAgInSc7d1	početný
průvodem	průvod	k1gInSc7	průvod
proti	proti	k7c3	proti
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
drakovi	drak	k1gMnSc3	drak
ale	ale	k8xC	ale
Béowulfa	Béowulf	k1gMnSc2	Béowulf
jeho	jeho	k3xOp3gFnPc6	jeho
družiníci	družiník	k1gMnPc1	družiník
opustí	opustit	k5eAaPmIp3nP	opustit
a	a	k8xC	a
v	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
chvíli	chvíle	k1gFnSc6	chvíle
při	při	k7c6	při
hrdinovi	hrdina	k1gMnSc6	hrdina
stojí	stát	k5eAaImIp3nS	stát
jen	jen	k6eAd1	jen
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Wígláf	Wígláf	k1gMnSc1	Wígláf
<g/>
.	.	kIx.	.
</s>
<s>
Béowulfovi	Béowulf	k1gMnSc3	Béowulf
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
draka	drak	k1gMnSc4	drak
smrtelně	smrtelně	k6eAd1	smrtelně
zranit	zranit	k5eAaPmF	zranit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
své	svůj	k3xOyFgNnSc4	svůj
vítězství	vítězství	k1gNnSc4	vítězství
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
sežehne	sežehnout	k5eAaPmIp3nS	sežehnout
ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
dech	dech	k1gInSc1	dech
umírající	umírající	k2eAgFnSc2d1	umírající
nestvůry	nestvůra	k1gFnSc2	nestvůra
<g/>
.	.	kIx.	.
</s>
<s>
Béowulfovo	Béowulfův	k2eAgNnSc1d1	Béowulfův
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
při	při	k7c6	při
skvělém	skvělý	k2eAgInSc6d1	skvělý
pohřebním	pohřební	k2eAgInSc6d1	pohřební
obřadu	obřad	k1gInSc6	obřad
spáleno	spálen	k2eAgNnSc1d1	spáleno
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
i	i	k9	i
s	s	k7c7	s
dračím	dračí	k2eAgInSc7d1	dračí
pokladem	poklad	k1gInSc7	poklad
a	a	k8xC	a
Wígláf	Wígláf	k1gInSc1	Wígláf
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
novým	nový	k2eAgMnSc7d1	nový
králem	král	k1gMnSc7	král
Géatů	Géat	k1gInPc2	Géat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
je	být	k5eAaImIp3nS	být
popsaná	popsaný	k2eAgFnSc1d1	popsaná
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
cení	cenit	k5eAaImIp3nS	cenit
cti	čest	k1gFnSc2	čest
<g/>
,	,	kIx,	,
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
;	;	kIx,	;
bojovníci	bojovník	k1gMnPc1	bojovník
jsou	být	k5eAaImIp3nP	být
obdivováni	obdivován	k2eAgMnPc1d1	obdivován
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
významných	významný	k2eAgInPc2d1	významný
postů	post	k1gInPc2	post
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
králem	král	k1gMnSc7	král
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
ochráncem	ochránce	k1gMnSc7	ochránce
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
očekává	očekávat	k5eAaImIp3nS	očekávat
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
mužů	muž	k1gMnPc2	muž
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
za	za	k7c2	za
nasazení	nasazení	k1gNnSc2	nasazení
odměněni	odměnit	k5eAaPmNgMnP	odměnit
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
cennostmi	cennost	k1gFnPc7	cennost
a	a	k8xC	a
půdou	půda	k1gFnSc7	půda
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
jedince	jedinec	k1gMnSc2	jedinec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
Béowulf	Béowulf	k1gInSc1	Béowulf
jde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
bitev	bitva	k1gFnPc2	bitva
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikoli	nikoli	k9	nikoli
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
osud	osud	k1gInSc1	osud
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
o	o	k7c6	o
výsledku	výsledek	k1gInSc6	výsledek
-	-	kIx~	-
filozofie	filozofie	k1gFnSc1	filozofie
hluboce	hluboko	k6eAd1	hluboko
zakořeněná	zakořeněný	k2eAgFnSc1d1	zakořeněná
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
tradici	tradice	k1gFnSc6	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nP	mísit
severské	severský	k2eAgFnPc1d1	severská
a	a	k8xC	a
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
tradice	tradice	k1gFnPc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
prokazují	prokazovat	k5eAaImIp3nP	prokazovat
všechny	všechen	k3xTgFnPc4	všechen
charakterové	charakterový	k2eAgFnPc4d1	charakterová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
ceněné	ceněný	k2eAgFnPc4d1	ceněná
v	v	k7c6	v
germánské	germánský	k2eAgFnSc6d1	germánská
a	a	k8xC	a
severské	severský	k2eAgFnSc3d1	severská
tradici	tradice	k1gFnSc3	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Morální	morální	k2eAgInPc1d1	morální
soudy	soud	k1gInPc1	soud
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
často	často	k6eAd1	často
vynášeny	vynášet	k5eAaImNgInP	vynášet
z	z	k7c2	z
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Grendel	Grendlo	k1gNnPc2	Grendlo
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
potomek	potomek	k1gMnSc1	potomek
bratrovraha	bratrovrah	k1gMnSc2	bratrovrah
Kaina	Kain	k1gMnSc2	Kain
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
žebříčku	žebříček	k1gInSc2	žebříček
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Béowulf	Béowulf	k1gInSc1	Béowulf
představuje	představovat	k5eAaImIp3nS	představovat
pokřesťanštěné	pokřesťanštěný	k2eAgNnSc4d1	pokřesťanštěný
zpracování	zpracování	k1gNnSc4	zpracování
původní	původní	k2eAgFnSc2d1	původní
severské	severský	k2eAgFnSc2d1	severská
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Béowulf	Béowulf	k1gInSc1	Béowulf
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšel	vyjít	k5eAaPmAgInS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
ve	v	k7c6	v
výpravném	výpravný	k2eAgNnSc6d1	výpravné
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Torst	Torst	k1gInSc1	Torst
<g/>
.	.	kIx.	.
</s>
<s>
Překladatel	překladatel	k1gMnSc1	překladatel
Jan	Jan	k1gMnSc1	Jan
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
za	za	k7c4	za
překlad	překlad	k1gInSc4	překlad
obdržel	obdržet	k5eAaPmAgInS	obdržet
Cenu	cena	k1gFnSc4	cena
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmann	k1gMnSc2	Jungmann
<g/>
,	,	kIx,	,
knihu	kniha	k1gFnSc4	kniha
doplnil	doplnit	k5eAaPmAgMnS	doplnit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
studií	studie	k1gFnSc7	studie
a	a	k8xC	a
výkladovými	výkladový	k2eAgFnPc7d1	výkladová
poznámkami	poznámka	k1gFnPc7	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Irský	irský	k2eAgMnSc1d1	irský
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
Seamus	Seamus	k1gMnSc1	Seamus
Heaney	Heanea	k1gFnSc2	Heanea
vydal	vydat	k5eAaPmAgMnS	vydat
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
překlad	překlad	k1gInSc1	překlad
Béowulfa	Béowulf	k1gMnSc2	Béowulf
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
angličtiny	angličtina	k1gFnSc2	angličtina
s	s	k7c7	s
dodržením	dodržení	k1gNnSc7	dodržení
aliterace	aliterace	k1gFnSc2	aliterace
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
John	John	k1gMnSc1	John
Gardner	Gardner	k1gMnSc1	Gardner
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
v	v	k7c6	v
románu	román	k1gInSc6	román
Grendel	Grendlo	k1gNnPc2	Grendlo
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
netvora	netvora	k1gFnSc1	netvora
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
podkladem	podklad	k1gInSc7	podklad
libreta	libreto	k1gNnSc2	libreto
JD	JD	kA	JD
McClatchyho	McClatchy	k1gMnSc4	McClatchy
a	a	k8xC	a
Julie	Julie	k1gFnPc4	Julie
Taymorové	Taymorová	k1gFnSc2	Taymorová
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
Grendel	Grendlo	k1gNnPc2	Grendlo
Elliota	Elliot	k1gMnSc2	Elliot
Goldenthala	Goldenthal	k1gMnSc2	Goldenthal
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
Michaela	Michael	k1gMnSc2	Michael
Crichtona	Crichton	k1gMnSc2	Crichton
Pojídači	pojídač	k1gMnSc3	pojídač
mrtvých	mrtvý	k1gMnPc2	mrtvý
(	(	kIx(	(
<g/>
Eaters	Eaters	k1gInSc1	Eaters
of	of	k?	of
the	the	k?	the
Dead	Dead	k1gInSc1	Dead
<g/>
)	)	kIx)	)
propojil	propojit	k5eAaPmAgInS	propojit
Béowulfa	Béowulf	k1gMnSc2	Béowulf
s	s	k7c7	s
cestopisy	cestopis	k1gInPc7	cestopis
Ahmeda	Ahmed	k1gMnSc2	Ahmed
Ibn	Ibn	k1gMnSc2	Ibn
Fadlána	Fadlán	k2eAgFnSc1d1	Fadlána
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zfilmován	zfilmován	k2eAgMnSc1d1	zfilmován
s	s	k7c7	s
Antoniem	Antonio	k1gMnSc7	Antonio
Banderasem	Banderas	k1gMnSc7	Banderas
a	a	k8xC	a
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Kulichem	Kulich	k1gMnSc7	Kulich
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vikingové	Viking	k1gMnPc1	Viking
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Warrior	Warrior	k1gMnSc1	Warrior
<g/>
)	)	kIx)	)
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkien	k1gInSc1	Tolkien
převzal	převzít	k5eAaPmAgInS	převzít
jména	jméno	k1gNnPc4	jméno
a	a	k8xC	a
motivy	motiv	k1gInPc4	motiv
z	z	k7c2	z
Béowulfa	Béowulf	k1gMnSc2	Béowulf
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
románů	román	k1gInPc2	román
ze	z	k7c2	z
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
Rohanu	Rohan	k1gMnSc3	Rohan
<g/>
,	,	kIx,	,
epizoda	epizoda	k1gFnSc1	epizoda
jeho	on	k3xPp3gInSc2	on
románu	román	k1gInSc2	román
Hobit	hobit	k1gMnSc1	hobit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Bilbo	Bilba	k1gFnSc5	Bilba
Pytlík	pytlík	k1gMnSc1	pytlík
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
skvostný	skvostný	k2eAgInSc4d1	skvostný
pohár	pohár	k1gInSc4	pohár
z	z	k7c2	z
dračího	dračí	k2eAgInSc2d1	dračí
pokladu	poklad	k1gInSc2	poklad
v	v	k7c6	v
hoře	hora	k1gFnSc6	hora
Ereboru	Erebor	k1gInSc2	Erebor
a	a	k8xC	a
rozzuřený	rozzuřený	k2eAgInSc4d1	rozzuřený
drak	drak	k1gInSc4	drak
Šmak	šmak	k1gInSc4	šmak
následně	následně	k6eAd1	následně
vyletí	vyletět	k5eAaPmIp3nS	vyletět
z	z	k7c2	z
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
ničí	ničit	k5eAaImIp3nS	ničit
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
inspirován	inspirovat	k5eAaBmNgMnS	inspirovat
Béowulfem	Béowulf	k1gInSc7	Béowulf
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
začínal	začínat	k5eAaImAgInS	začínat
své	svůj	k3xOyFgFnPc4	svůj
univerzitní	univerzitní	k2eAgFnPc4d1	univerzitní
přednášky	přednáška	k1gFnPc4	přednáška
ze	z	k7c2	z
staroanglické	staroanglický	k2eAgFnSc2d1	staroanglická
literatury	literatura	k1gFnSc2	literatura
zvoláním	zvolání	k1gNnSc7	zvolání
hwaet	hwaeta	k1gFnPc2	hwaeta
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
začíná	začínat	k5eAaImIp3nS	začínat
i	i	k9	i
Béowulf	Béowulf	k1gInSc1	Béowulf
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
také	také	k9	také
odbornou	odborný	k2eAgFnSc4d1	odborná
studii	studie	k1gFnSc4	studie
o	o	k7c6	o
Béowulfovi	Béowulf	k1gMnSc6	Béowulf
<g/>
:	:	kIx,	:
Netvoři	netvor	k1gMnPc1	netvor
a	a	k8xC	a
kritikové	kritik	k1gMnPc1	kritik
(	(	kIx(	(
<g/>
The	The	k1gMnPc1	The
Monsters	Monstersa	k1gFnPc2	Monstersa
and	and	k?	and
the	the	k?	the
Critics	Critics	k1gInSc1	Critics
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc2	Georg
Lucas	Lucas	k1gMnSc1	Lucas
využil	využít	k5eAaPmAgMnS	využít
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
Béowulfa	Béowulf	k1gMnSc2	Béowulf
v	v	k7c6	v
Hvězdných	hvězdný	k2eAgFnPc6d1	hvězdná
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Epizoda	epizoda	k1gFnSc1	epizoda
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
"	"	kIx"	"
<g/>
Hrdinové	Hrdina	k1gMnPc1	Hrdina
a	a	k8xC	a
démoni	démon	k1gMnPc1	démon
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
loď	loď	k1gFnSc1	loď
Voyager	Voyagra	k1gFnPc2	Voyagra
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
Béowulfa	Béowulf	k1gMnSc2	Béowulf
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
film	film	k1gInSc1	film
Béowulf	Béowulf	k1gInSc1	Béowulf
s	s	k7c7	s
Christopherem	Christopher	k1gMnSc7	Christopher
Lambertem	Lambert	k1gMnSc7	Lambert
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
se	se	k3xPyFc4	se
však	však	k9	však
drží	držet	k5eAaImIp3nS	držet
jen	jen	k9	jen
vzdáleně	vzdáleně	k6eAd1	vzdáleně
<g/>
:	:	kIx,	:
děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
přemístěn	přemístit	k5eAaPmNgInS	přemístit
do	do	k7c2	do
apokalyptické	apokalyptický	k2eAgFnSc2d1	apokalyptická
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Neil	Neil	k1gMnSc1	Neil
Gaiman	Gaiman	k1gMnSc1	Gaiman
napsal	napsat	k5eAaPmAgMnS	napsat
futuristickou	futuristický	k2eAgFnSc4d1	futuristická
interpretaci	interpretace	k1gFnSc4	interpretace
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
epické	epický	k2eAgFnSc2d1	epická
básně	báseň	k1gFnSc2	báseň
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bay	Bay	k1gMnSc1	Bay
Wolf	Wolf	k1gMnSc1	Wolf
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Neowulf	Neowulf	k1gInSc1	Neowulf
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
drobných	drobný	k2eAgFnPc2d1	drobná
próz	próza	k1gFnPc2	próza
a	a	k8xC	a
básní	báseň	k1gFnPc2	báseň
N.	N.	kA	N.
Gaimana	Gaiman	k1gMnSc2	Gaiman
Kouř	kouř	k1gInSc1	kouř
a	a	k8xC	a
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Richarda	Richard	k1gMnSc2	Richard
Podaného	podaný	k2eAgMnSc2d1	podaný
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Polaris	Polaris	k1gFnSc2	Polaris
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Larry	Larra	k1gFnPc1	Larra
Niven	Nivna	k1gFnPc2	Nivna
<g/>
,	,	kIx,	,
Jerry	Jerr	k1gMnPc4	Jerr
Pournelle	Pournell	k1gMnSc4	Pournell
a	a	k8xC	a
Steven	Steven	k2eAgInSc4d1	Steven
Barnes	Barnes	k1gInSc4	Barnes
napsali	napsat	k5eAaPmAgMnP	napsat
knihu	kniha	k1gFnSc4	kniha
Odkaz	odkaz	k1gInSc4	odkaz
Heorotu	Heorot	k1gInSc2	Heorot
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
skupina	skupina	k1gFnSc1	skupina
kolonistů	kolonista	k1gMnPc2	kolonista
osidluje	osidlovat	k5eAaImIp3nS	osidlovat
cizí	cizí	k2eAgFnSc4d1	cizí
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jim	on	k3xPp3gMnPc3	on
nejdřív	dříve	k6eAd3	dříve
připadá	připadat	k5eAaPmIp3nS	připadat
jako	jako	k9	jako
ráj	ráj	k1gInSc1	ráj
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nesetkají	setkat	k5eNaPmIp3nP	setkat
s	s	k7c7	s
Grendelem	Grendel	k1gInSc7	Grendel
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
má	mít	k5eAaImIp3nS	mít
takový	takový	k3xDgInSc4	takový
výcvik	výcvik	k1gInSc4	výcvik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
příšeru	příšera	k1gFnSc4	příšera
přemohl	přemoct	k5eAaPmAgMnS	přemoct
<g/>
.	.	kIx.	.
</s>
<s>
Pokračování	pokračování	k1gNnSc1	pokračování
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Draci	drak	k1gMnPc1	drak
Heorotu	Heorot	k1gInSc2	Heorot
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
islandský	islandský	k2eAgMnSc1d1	islandský
režisér	režisér	k1gMnSc1	režisér
Sturla	Sturla	k1gMnSc1	Sturla
Gunnarsson	Gunnarsson	k1gMnSc1	Gunnarsson
látku	látka	k1gFnSc4	látka
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
Beowulfa	Beowulf	k1gMnSc2	Beowulf
s	s	k7c7	s
Gerardem	Gerard	k1gMnSc7	Gerard
Butlerem	Butler	k1gMnSc7	Butler
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
a	a	k8xC	a
Sarah	Sarah	k1gFnSc1	Sarah
Polleyovou	Polleyová	k1gFnSc4	Polleyová
v	v	k7c6	v
roli	role	k1gFnSc6	role
Selmy	Selma	k1gFnSc2	Selma
v	v	k7c6	v
naturalistickém	naturalistický	k2eAgInSc6d1	naturalistický
filmu	film	k1gInSc6	film
Beowulf	Beowulf	k1gMnSc1	Beowulf
&	&	k?	&
Grendel	Grendlo	k1gNnPc2	Grendlo
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
americkou	americký	k2eAgFnSc7d1	americká
televizní	televizní	k2eAgFnSc7d1	televizní
společností	společnost	k1gFnSc7	společnost
NBC	NBC	kA	NBC
Universal	Universal	k1gMnSc2	Universal
Television	Television	k1gInSc1	Television
odvysílán	odvysílán	k2eAgInSc1d1	odvysílán
film	film	k1gInSc4	film
Grendel	Grendlo	k1gNnPc2	Grendlo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
hrdiny	hrdina	k1gMnSc2	hrdina
Beowulfa	Beowulf	k1gMnSc2	Beowulf
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
Chris	Chris	k1gFnSc4	Chris
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
.	.	kIx.	.
</s>
<s>
Královnu	královna	k1gFnSc4	královna
Wealtheow	Wealtheow	k1gFnSc2	Wealtheow
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
Marina	Marina	k1gFnSc1	Marina
Sirtis	Sirtis	k1gFnSc1	Sirtis
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
známá	známý	k2eAgFnSc1d1	známá
ze	z	k7c2	z
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
filmů	film	k1gInPc2	film
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
Star	star	k1gFnSc2	star
Trek	Treka	k1gFnPc2	Treka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
lokace	lokace	k1gFnPc1	lokace
filmu	film	k1gInSc2	film
Grendel	Grendlo	k1gNnPc2	Grendlo
byly	být	k5eAaImAgFnP	být
natáčeny	natáčet	k5eAaImNgFnP	natáčet
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
2	[number]	k4	2
roky	rok	k1gInPc4	rok
natáčený	natáčený	k2eAgInSc1d1	natáčený
<g/>
,	,	kIx,	,
počítačově	počítačově	k6eAd1	počítačově
animovaný	animovaný	k2eAgInSc1d1	animovaný
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
Beowulf	Beowulf	k1gInSc1	Beowulf
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
eposu	epos	k1gInSc2	epos
poměrně	poměrně	k6eAd1	poměrně
věrně	věrně	k6eAd1	věrně
drží	držet	k5eAaImIp3nS	držet
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Robert	Robert	k1gMnSc1	Robert
Zemeckis	Zemeckis	k1gFnSc1	Zemeckis
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
obsadil	obsadit	k5eAaPmAgMnS	obsadit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
Angelinu	Angelin	k2eAgFnSc4d1	Angelina
Jolieovou	Jolieův	k2eAgFnSc7d1	Jolieův
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc2	John
Malkoviche	Malkovich	k1gMnSc2	Malkovich
či	či	k8xC	či
Anthonyho	Anthony	k1gMnSc2	Anthony
Hopkinse	Hopkins	k1gMnSc2	Hopkins
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
herci	herec	k1gMnPc1	herec
nahráli	nahrát	k5eAaBmAgMnP	nahrát
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
počítačové	počítačový	k2eAgInPc4d1	počítačový
zevnějšky	zevnějšek	k1gInPc4	zevnějšek
pohyby	pohyb	k1gInPc4	pohyb
<g/>
,	,	kIx,	,
hlas	hlas	k1gInSc4	hlas
i	i	k8xC	i
mimiku	mimika	k1gFnSc4	mimika
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Gardner	Gardner	k1gMnSc1	Gardner
<g/>
:	:	kIx,	:
Grendel	Grendlo	k1gNnPc2	Grendlo
<g/>
.	.	kIx.	.
</s>
<s>
Vintage	Vintagat	k5eAaPmIp3nS	Vintagat
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Clark	Clark	k1gInSc1	Clark
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-679-72311-0	[number]	k4	0-679-72311-0
John	John	k1gMnSc1	John
R.	R.	kA	R.
Tolkien	Tolkien	k1gInSc1	Tolkien
<g/>
:	:	kIx,	:
Beowulf	Beowulf	k1gInSc1	Beowulf
<g/>
,	,	kIx,	,
the	the	k?	the
monsters	monsters	k1gInSc1	monsters
and	and	k?	and
the	the	k?	the
critics	critics	k1gInSc1	critics
<g/>
.	.	kIx.	.
</s>
<s>
Sir	sir	k1gMnSc1	sir
Israel	Israel	k1gMnSc1	Israel
Gollancz	Gollancz	k1gMnSc1	Gollancz
memorial	memorial	k1gMnSc1	memorial
lecture	lectur	k1gMnSc5	lectur
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
Univ	Univ	k1gInSc1	Univ
<g/>
.	.	kIx.	.
</s>
<s>
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Arden	Ardeny	k1gFnPc2	Ardeny
Libr	Libra	k1gFnPc2	Libra
<g/>
,	,	kIx,	,
Darby	Darba	k1gFnSc2	Darba
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
Reprint	reprint	k1gInSc1	reprint
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Crichton	Crichton	k1gInSc1	Crichton
<g/>
:	:	kIx,	:
Eaters	Eaters	k1gInSc1	Eaters
of	of	k?	of
the	the	k?	the
dead	dead	k1gInSc1	dead
<g/>
,	,	kIx,	,
the	the	k?	the
manuscript	manuscript	k1gMnSc1	manuscript
of	of	k?	of
Ibn	Ibn	k1gMnSc1	Ibn
Fadlan	Fadlan	k1gMnSc1	Fadlan
relating	relating	k1gInSc4	relating
his	his	k1gNnSc2	his
experiences	experiencesa	k1gFnPc2	experiencesa
with	with	k1gMnSc1	with
the	the	k?	the
Northmen	Northmen	k1gInSc1	Northmen
in	in	k?	in
A.D.	A.D.	k1gFnSc2	A.D.
922	[number]	k4	922
Knopf	Knopf	k1gMnSc1	Knopf
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
NY	NY	kA	NY
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-394-49400-8	[number]	k4	0-394-49400-8
Beowulf	Beowulf	k1gInSc4	Beowulf
<g/>
.	.	kIx.	.
</s>
<s>
Transl	Transl	k1gInSc1	Transl
<g/>
.	.	kIx.	.
by	by	kYmCp3nS	by
Seamus	Seamus	k1gInSc1	Seamus
Heaney	Heanea	k1gFnSc2	Heanea
<g/>
.	.	kIx.	.
</s>
<s>
Faber	Faber	k1gMnSc1	Faber
and	and	k?	and
Faber	Faber	k1gMnSc1	Faber
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Norten	Norten	k2eAgMnSc1d1	Norten
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-393-97580-0	[number]	k4	0-393-97580-0
Larry	Larra	k1gFnSc2	Larra
Niven	Nivna	k1gFnPc2	Nivna
<g/>
,	,	kIx,	,
Jerry	Jerra	k1gFnSc2	Jerra
Pournelle	Pournelle	k1gFnSc2	Pournelle
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgInSc4d1	Steven
Barnes	Barnes	k1gInSc4	Barnes
<g/>
:	:	kIx,	:
Der	drát	k5eAaImRp2nS	drát
Held	Held	k1gInSc4	Held
von	von	k1gInSc1	von
Avalon	Avalon	k1gInSc1	Avalon
<g/>
.	.	kIx.	.
</s>
<s>
Science-fiction-Roman	Scienceiction-Roman	k1gMnSc1	Science-fiction-Roman
<g/>
.	.	kIx.	.
</s>
<s>
Bastei-Lübbe	Bastei-Lübbat	k5eAaPmIp3nS	Bastei-Lübbat
<g/>
,	,	kIx,	,
Bergisch	Bergisch	k1gInSc1	Bergisch
Gladbach	Gladbach	k1gInSc1	Gladbach
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
deutsch	deutsch	k1gInSc1	deutsch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-404-23089-2	[number]	k4	3-404-23089-2
Larry	Larra	k1gFnSc2	Larra
Niven	Nivna	k1gFnPc2	Nivna
<g/>
,	,	kIx,	,
Jerry	Jerra	k1gFnSc2	Jerra
Pournelle	Pournelle	k1gFnSc2	Pournelle
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgInSc1d1	Steven
Barnes	Barnes	k1gInSc1	Barnes
<g/>
:	:	kIx,	:
Beowulfs	Beowulfs	k1gInSc1	Beowulfs
Kinder	Kindra	k1gFnPc2	Kindra
<g/>
.	.	kIx.	.
</s>
<s>
Science-fiction-Roman	Scienceiction-Roman	k1gMnSc1	Science-fiction-Roman
<g/>
.	.	kIx.	.
</s>
<s>
Lübbe	Lübbat	k5eAaPmIp3nS	Lübbat
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
deutsch	deutsch	k1gInSc1	deutsch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-404-24223-8	[number]	k4	3-404-24223-8
Neil	Neil	k1gMnSc1	Neil
Gaiman	Gaiman	k1gMnSc1	Gaiman
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc1	Die
Messerkönigin	Messerkönigin	k1gMnSc1	Messerkönigin
<g/>
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Aus	Aus	k?	Aus
dem	dem	k?	dem
Engl	Engl	k1gInSc1	Engl
<g/>
.	.	kIx.	.
von	von	k1gInSc1	von
Ingrid	Ingrid	k1gFnSc2	Ingrid
Krane-Müschen	Krane-Müschna	k1gFnPc2	Krane-Müschna
<g/>
.	.	kIx.	.
</s>
<s>
Heyne	Heynout	k5eAaImIp3nS	Heynout
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-453-17798-3	[number]	k4	3-453-17798-3
Beowulf	Beowulf	k1gInSc4	Beowulf
<g/>
:	:	kIx,	:
eine	einat	k5eAaPmIp3nS	einat
Textauswahl	Textauswahl	k1gMnSc1	Textauswahl
mit	mit	k?	mit
Einleitung	Einleitung	k1gMnSc1	Einleitung
<g/>
,	,	kIx,	,
Übersetzung	Übersetzung	k1gMnSc1	Übersetzung
<g/>
,	,	kIx,	,
Kommentar	Kommentar	k1gMnSc1	Kommentar
und	und	k?	und
Glossar	Glossar	k1gMnSc1	Glossar
<g/>
,	,	kIx,	,
hg	hg	k?	hg
<g/>
.	.	kIx.	.
von	von	k1gInSc1	von
Ewald	Ewalda	k1gFnPc2	Ewalda
Standop	Standop	k1gInSc1	Standop
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
de	de	k?	de
Gruyter	Gruytrum	k1gNnPc2	Gruytrum
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-11-017608-4	[number]	k4	3-11-017608-4
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
1	[number]	k4	1
<g/>
/	/	kIx~	/
A-	A-	k1gMnPc2	A-
<g/>
L.	L.	kA	L.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Oden	Oden	k1gNnSc1	Oden
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
948	[number]	k4	948
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
475	[number]	k4	475
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Beowulf	Beowulf	k1gInSc1	Beowulf
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Beowulf	Beowulf	k1gInSc4	Beowulf
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc1	Commons
Rozbor	rozbor	k1gInSc1	rozbor
Béowulfa	Béowulf	k1gMnSc2	Béowulf
(	(	kIx(	(
<g/>
prof.	prof.	kA	prof.
Jan	Jan	k1gMnSc1	Jan
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
YouTube	YouTub	k1gInSc5	YouTub
<g/>
)	)	kIx)	)
</s>
