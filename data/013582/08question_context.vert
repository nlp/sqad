<s>
Ganga	Ganga	k1gFnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
užíván	užívat	k5eAaImNgInS
též	též	k9
tvar	tvar	k1gInSc1
Ganges	Ganges	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
hindsky	hindsky	k6eAd1
ग	ग	k?
<g/>
ं	ं	k?
<g/>
ग	ग	k?
<g/>
ा	ा	k?
<g/>
,	,	kIx,
bengálsky	bengálsky	k6eAd1
গ	গ	k?
<g/>
্	্	k?
<g/>
গ	গ	k?
<g/>
া	া	k?
<g/>
,	,	kIx,
v	v	k7c6
sanskrtu	sanskrt	k1gInSc6
ग	ग	k?
<g/>
्	्	k?
<g/>
ग	ग	k?
<g/>
ा	ा	k?
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Ganges	Ganges	k1gInSc1
River	River	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
řeka	řeka	k1gFnSc1
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>