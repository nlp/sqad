<s>
Ganga	Ganga	k1gFnSc1
</s>
<s>
Gangaग	Gangaग	k?
<g/>
ं	ं	k?
<g/>
ग	ग	k?
<g/>
ा	ा	k?
<g/>
গ	গ	k?
<g/>
্	্	k?
<g/>
গ	গ	k?
<g/>
া	া	k?
Řeka	řeka	k1gFnSc1
ve	v	k7c6
VáránasíZákladní	VáránasíZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
2	#num#	k4
510	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
1	#num#	k4
120	#num#	k4
000	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
14	#num#	k4
270	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Asie	Asie	k1gFnSc1
Zdrojnice	zdrojnice	k1gFnSc2
</s>
<s>
Bhagírathí	Bhagírathí	k1gFnSc1
a	a	k8xC
Alakanandá	Alakanandá	k1gFnSc1
v	v	k7c6
Himálaji	Himálaj	k1gFnSc6
30	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
42,39	42,39	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
78	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
50,85	50,85	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
7756	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
Bengálský	bengálský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
23	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
89	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
0	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Indie	Indie	k1gFnSc1
Indie	Indie	k1gFnSc1
(	(	kIx(
<g/>
Uttarákhand	Uttarákhand	k1gInSc1
<g/>
,	,	kIx,
Uttarpradéš	Uttarpradéš	k1gInSc1
<g/>
,	,	kIx,
Bihár	Bihár	k1gInSc1
<g/>
,	,	kIx,
Džhárkhand	Džhárkhand	k1gInSc1
<g/>
,	,	kIx,
Západní	západní	k2eAgNnSc1d1
Bengálsko	Bengálsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bangladéš	Bangladéš	k1gInSc4
Bangladéš	Bangladéš	k1gInSc4
Úmoří	úmoří	k1gNnPc2
<g/>
,	,	kIx,
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
Indický	indický	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Bengálský	bengálský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
<g/>
,	,	kIx,
Povodí	povodí	k1gNnSc1
Gangy	Ganga	k1gFnSc2
(	(	kIx(
<g/>
Indie	Indie	k1gFnSc1
58,01	58,01	k4
%	%	kIx~
<g/>
,	,	kIx,
ČLR	ČLR	kA
19,65	19,65	k4
%	%	kIx~
<g/>
,	,	kIx,
Nepál	Nepál	k1gInSc1
9,01	9,01	k4
%	%	kIx~
<g/>
,	,	kIx,
Bangladéš	Bangladéš	k1gInSc1
6,55	6,55	k4
%	%	kIx~
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
Indie	Indie	k1gFnSc2
nárokovaná	nárokovaný	k2eAgFnSc1d1
ČLR	ČLR	kA
4,11	4,11	k4
%	%	kIx~
<g/>
,	,	kIx,
Bhútán	Bhútán	k1gInSc1
2,44	2,44	k4
%	%	kIx~
<g/>
,	,	kIx,
území	území	k1gNnSc6
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
Indie	Indie	k1gFnSc2
nárokované	nárokovaný	k2eAgFnSc2d1
ČLR	ČLR	kA
0,07	0,07	k4
%	%	kIx~
<g/>
,	,	kIx,
Myanmar	Myanmar	k1gMnSc1
<	<	kIx(
<g/>
0,01	0,01	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geodata	Geodata	k1gFnSc1
OpenStreetMap	OpenStreetMap	k1gInSc1
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ganga	Ganga	k1gFnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
užíván	užívat	k5eAaImNgInS
též	též	k9
tvar	tvar	k1gInSc1
Ganges	Ganges	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
hindsky	hindsky	k6eAd1
ग	ग	k?
<g/>
ं	ं	k?
<g/>
ग	ग	k?
<g/>
ा	ा	k?
<g/>
,	,	kIx,
bengálsky	bengálsky	k6eAd1
গ	গ	k?
<g/>
্	্	k?
<g/>
গ	গ	k?
<g/>
া	া	k?
<g/>
,	,	kIx,
v	v	k7c6
sanskrtu	sanskrt	k1gInSc6
ग	ग	k?
<g/>
्	्	k?
<g/>
ग	ग	k?
<g/>
ा	ा	k?
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
Ganges	Ganges	k1gInSc1
River	River	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
řeka	řeka	k1gFnSc1
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protéká	protékat	k5eAaImIp3nS
státy	stát	k1gInPc4
Uttarákhand	Uttarákhanda	k1gFnPc2
<g/>
,	,	kIx,
Uttarpradéš	Uttarpradéš	k1gInSc1
<g/>
,	,	kIx,
Bihár	Bihár	k1gInSc1
<g/>
,	,	kIx,
Džhárkhand	Džhárkhand	k1gInSc1
a	a	k8xC
Západní	západní	k2eAgNnSc1d1
Bengálsko	Bengálsko	k1gNnSc1
v	v	k7c6
Indii	Indie	k1gFnSc6
a	a	k8xC
Bangladéšem	Bangladéš	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
průtoku	průtok	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
dohromady	dohromady	k6eAd1
s	s	k7c7
Brahmaputrou	Brahmaputra	k1gFnSc7
zaujímá	zaujímat	k5eAaImIp3nS
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
na	na	k7c6
světě	svět	k1gInSc6
po	po	k7c6
Amazonce	Amazonka	k1gFnSc6
a	a	k8xC
Kongu	Kongo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
2510	#num#	k4
km	km	kA
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
1	#num#	k4
120	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Včetně	včetně	k7c2
Brahmaputry	Brahmaputr	k1gInPc1
má	mít	k5eAaImIp3nS
povodí	povodí	k1gNnSc4
rozlohu	rozloh	k1gInSc2
2	#num#	k4
055	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Tok	tok	k1gInSc4
řeky	řeka	k1gFnSc2
můžeme	moct	k5eAaImIp1nP
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
čtyři	čtyři	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Horní	horní	k2eAgInSc1d1
tok	tok	k1gInSc1
</s>
<s>
Horní	horní	k2eAgInSc1d1
tok	tok	k1gInSc1
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
Himálaji	Himálaj	k1gFnSc6
dvěma	dva	k4xCgFnPc7
zdrojnicemi	zdrojnice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprava	zprava	k6eAd1
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
Bhagírathí	Bhagírathí	k2eAgFnSc1d1
a	a	k8xC
zleva	zleva	k6eAd1
Alakanandá	Alakanandá	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bhagírathí	Bhagírathí	k1gMnSc1
začíná	začínat	k5eAaImIp3nS
poblíž	poblíž	k7c2
ledovce	ledovec	k1gInSc2
Gangotri	Gangotr	k1gFnSc2
a	a	k8xC
Alakanandá	Alakanandá	k1gFnSc2
začíná	začínat	k5eAaImIp3nS
u	u	k7c2
ledovců	ledovec	k1gInPc2
Satopanth	Satopanth	k1gInSc1
a	a	k8xC
Bhagirath	Bhagirath	k1gMnSc1
Kharak	Kharak	k1gMnSc1
a	a	k8xC
protéká	protékat	k5eAaImIp3nS
poutním	poutní	k2eAgNnSc7d1
místem	místo	k1gNnSc7
Badrináth	Badrinátha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údolí	údolí	k1gNnSc6
Alakanandy	Alakananda	k1gFnSc2
je	být	k5eAaImIp3nS
asi	asi	k9
229	#num#	k4
km	km	kA
dlouhé	dlouhý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutok	soutok	k1gInSc1
Bhagírathí	Bhagírathí	k1gFnSc2
a	a	k8xC
Alakanandy	Alakananda	k1gFnSc2
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Dévprajág	Dévprajág	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
sanskrtu	sanskrt	k1gInSc6
Devaprayā	Devaprayā	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
řeka	řeka	k1gFnSc1
nese	nést	k5eAaImIp3nS
název	název	k1gInSc4
Ganga	Ganga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Střední	střední	k2eAgInSc1d1
tok	tok	k1gInSc1
</s>
<s>
Střední	střední	k2eAgInSc1d1
tok	tok	k1gInSc1
začíná	začínat	k5eAaImIp3nS
v	v	k7c6
místech	místo	k1gNnPc6
kde	kde	k6eAd1
řeka	řeka	k1gFnSc1
opouští	opouštět	k5eAaImIp3nS
hory	hora	k1gFnPc4
a	a	k8xC
vstupuje	vstupovat	k5eAaImIp3nS
do	do	k7c2
naplaveninové	naplaveninový	k2eAgFnSc2d1
Ganžské	ganžský	k2eAgFnSc2d1
roviny	rovina	k1gFnSc2
(	(	kIx(
<g/>
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
Indoganžské	Indoganžský	k2eAgFnSc2d1
roviny	rovina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
zde	zde	k6eAd1
řeka	řeka	k1gFnSc1
protíná	protínat	k5eAaImIp3nS
vlnitou	vlnitý	k2eAgFnSc4d1
a	a	k8xC
značně	značně	k6eAd1
zalesněnou	zalesněný	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
hustě	hustě	k6eAd1
rozčleněná	rozčleněný	k2eAgFnSc1d1
koryty	koryto	k1gNnPc7
řek	řeka	k1gFnPc2
stékajících	stékající	k2eAgFnPc2d1
s	s	k7c7
Himálají	Himálaj	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
pokojně	pokojně	k6eAd1
teče	téct	k5eAaImIp3nS
plochou	plochý	k2eAgFnSc7d1
rovinou	rovina	k1gFnSc7
v	v	k7c6
8	#num#	k4
až	až	k9
12	#num#	k4
km	km	kA
širokém	široký	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gMnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
mnoho	mnoho	k4c1
starých	starý	k2eAgNnPc2d1
ramen	rameno	k1gNnPc2
<g/>
,	,	kIx,
jezer	jezero	k1gNnPc2
<g/>
,	,	kIx,
průtoků	průtok	k1gInPc2
a	a	k8xC
bažin	bažina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šířka	šířka	k1gFnSc1
koryta	koryto	k1gNnSc2
je	být	k5eAaImIp3nS
400	#num#	k4
až	až	k9
600	#num#	k4
m.	m.	k?
Ganga	Ganga	k1gFnSc1
přijímá	přijímat	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
přítoků	přítok	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
největší	veliký	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
Jamuna	Jamuna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
teče	téct	k5eAaImIp3nS
téměř	téměř	k6eAd1
1	#num#	k4
000	#num#	k4
km	km	kA
paralelně	paralelně	k6eAd1
s	s	k7c7
Gangou	Ganga	k1gFnSc7
a	a	k8xC
spojuje	spojovat	k5eAaImIp3nS
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
u	u	k7c2
Ilahabadu	Ilahabad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
mezi	mezi	k7c7
těmito	tento	k3xDgFnPc7
dvěma	dva	k4xCgFnPc7
řekami	řeka	k1gFnPc7
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Doab	Doab	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
dvojříčí	dvojříčí	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejhustěji	husto	k6eAd3
zabydlených	zabydlený	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dolní	dolní	k2eAgInSc1d1
tok	tok	k1gInSc1
</s>
<s>
Dolní	dolní	k2eAgInSc1d1
tok	tok	k1gInSc1
začíná	začínat	k5eAaImIp3nS
od	od	k7c2
ústí	ústí	k1gNnSc2
Jamuny	Jamuna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
roviny	rovina	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
tomto	tento	k3xDgInSc6
úseku	úsek	k1gInSc6
postupně	postupně	k6eAd1
zmenšuje	zmenšovat	k5eAaImIp3nS
ze	z	k7c2
100	#num#	k4
až	až	k9
120	#num#	k4
m	m	kA
na	na	k7c4
25	#num#	k4
až	až	k9
30	#num#	k4
m	m	kA
na	na	k7c6
začátku	začátek	k1gInSc6
delty	delta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
pravému	pravý	k2eAgInSc3d1
břehu	břeh	k1gInSc3
místy	místy	k6eAd1
zasahují	zasahovat	k5eAaImIp3nP
výběžky	výběžek	k1gInPc1
pohoří	pohoří	k1gNnSc2
Čhota-Nagpur	Čhota-Nagpura	k1gFnPc2
(	(	kIx(
<g/>
vysočiny	vysočina	k1gFnPc1
Baghélkhand	Baghélkhando	k1gNnPc2
a	a	k8xC
Radžmahal	Radžmahal	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Břeh	břeh	k1gInSc1
je	být	k5eAaImIp3nS
vysoký	vysoký	k2eAgMnSc1d1
<g/>
,	,	kIx,
skalnatý	skalnatý	k2eAgMnSc1d1
a	a	k8xC
na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
peřeje	peřej	k1gFnPc1
(	(	kIx(
<g/>
Mirzápur	Mirzápur	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ostatních	ostatní	k2eAgNnPc6d1
místech	místo	k1gNnPc6
je	být	k5eAaImIp3nS
koryto	koryto	k1gNnSc1
široké	široký	k2eAgNnSc1d1
a	a	k8xC
dolina	dolina	k1gFnSc1
je	být	k5eAaImIp3nS
jasně	jasně	k6eAd1
vyznačena	vyznačen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úval	úval	k1gInSc1
je	být	k5eAaImIp3nS
členěn	členit	k5eAaImNgInS
starými	starý	k2eAgInPc7d1
koryty	koryto	k1gNnPc7
a	a	k8xC
mnohými	mnohý	k2eAgInPc7d1
průtoky	průtok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ganga	Ganga	k1gFnSc1
přijímá	přijímat	k5eAaImIp3nS
levé	levý	k2eAgInPc4d1
přítoky	přítok	k1gInPc4
stékající	stékající	k2eAgInPc4d1
z	z	k7c2
Himálají	Himálaj	k1gFnPc2
a	a	k8xC
její	její	k3xOp3gFnSc1
šířka	šířka	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
až	až	k9
2	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Delta	delta	k1gFnSc1
</s>
<s>
Satelitní	satelitní	k2eAgFnPc1d1
fotografie	fotografia	k1gFnPc1
delty	delta	k1gFnSc2
Gangy	Ganga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Delta	delta	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
pod	pod	k7c7
vysočinou	vysočina	k1gFnSc7
Radžmahal	Radžmahal	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Ganga	Ganga	k1gFnSc1
vtéká	vtékat	k5eAaImIp3nS
do	do	k7c2
Bengálské	bengálský	k2eAgFnSc2d1
nížiny	nížina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
Brahmaputrou	Brahmaputra	k1gFnSc7
zde	zde	k6eAd1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
delt	delta	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
složitou	složitý	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
skladby	skladba	k1gFnSc2
povrchu	povrch	k1gInSc2
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Severní	severní	k2eAgFnSc1d1
nezatopovaná	zatopovaný	k2eNgFnSc1d1
část	část	k1gFnSc1
delty	delta	k1gFnSc2
má	mít	k5eAaImIp3nS
dobře	dobře	k6eAd1
živenou	živený	k2eAgFnSc4d1
úrodnou	úrodný	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
pokrytá	pokrytý	k2eAgFnSc1d1
převážně	převážně	k6eAd1
plantážemi	plantáž	k1gFnPc7
rýže	rýže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
zatopovaná	zatopovaný	k2eAgFnSc1d1
část	část	k1gFnSc1
delty	delta	k1gFnSc2
je	být	k5eAaImIp3nS
rozlohou	rozloha	k1gFnSc7
menší	malý	k2eAgFnSc7d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazývá	nazývat	k5eAaImIp3nS
se	se	k3xPyFc4
Sundarbans	Sundarbans	k1gInSc1
a	a	k8xC
pokrývají	pokrývat	k5eAaImIp3nP
jí	on	k3xPp3gFnSc2
džungle	džungle	k1gFnSc2
a	a	k8xC
bažiny	bažina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
rozsáhlé	rozsáhlý	k2eAgInPc4d1
mangrovníkové	mangrovníkový	k2eAgInPc4d1
porosty	porost	k1gInPc4
a	a	k8xC
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
tygr	tygr	k1gMnSc1
indický	indický	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Delta	delta	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
krajích	kraj	k1gInPc6
ohraničena	ohraničen	k2eAgFnSc1d1
velkými	velký	k2eAgNnPc7d1
rameny	rameno	k1gNnPc7
Gangy	Ganga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
Huglí	Huglí	k1gNnSc4
a	a	k8xC
na	na	k7c6
východě	východ	k1gInSc6
Meghna	Meghn	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
Brahmaputrou	Brahmaputra	k1gFnSc7
230	#num#	k4
km	km	kA
od	od	k7c2
ústí	ústí	k1gNnSc2
a	a	k8xC
jeví	jevit	k5eAaImIp3nS
se	s	k7c7
hlavním	hlavní	k2eAgNnSc7d1
korytem	koryto	k1gNnSc7
Gangy	Ganga	k1gFnSc2
ústícím	ústící	k2eAgFnPc3d1
do	do	k7c2
Bengálského	bengálský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
čtyřmi	čtyři	k4xCgFnPc7
základními	základní	k2eAgNnPc7d1
rameny	rameno	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Přítoky	přítok	k1gInPc1
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc1d3
přítoky	přítok	k1gInPc1
jsou	být	k5eAaImIp3nP
zleva	zleva	k6eAd1
Ramganga	Ramganga	k1gFnSc1
<g/>
,	,	kIx,
Gómatí	Gómatý	k2eAgMnPc1d1
<g/>
,	,	kIx,
Ghághra	Ghághra	k1gFnSc1
<g/>
,	,	kIx,
Gandak	Gandak	k1gInSc1
<g/>
,	,	kIx,
Kósí	Kósí	k1gNnSc1
<g/>
,	,	kIx,
Burhi	Burhi	k1gNnPc7
Gandak	Gandak	k1gInSc1
<g/>
,	,	kIx,
Ghugri	Ghugri	k1gNnSc1
a	a	k8xC
Mahananda	Mahananda	k1gFnSc1
a	a	k8xC
zprava	zprava	k6eAd1
Jamuna	Jamuna	k1gFnSc1
a	a	k8xC
Són	Són	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
Ráno	ráno	k6eAd1
na	na	k7c6
Ganze	Ganga	k1gFnSc6
</s>
<s>
Zdroj	zdroj	k1gInSc1
vody	voda	k1gFnSc2
je	být	k5eAaImIp3nS
smíšený	smíšený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
především	především	k9
tající	tající	k2eAgInPc1d1
ledovce	ledovec	k1gInPc1
a	a	k8xC
u	u	k7c2
města	město	k1gNnSc2
Váránasí	Váránasý	k2eAgMnPc1d1
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
slavnosti	slavnost	k1gFnPc1
sněhových	sněhový	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
středním	střední	k2eAgInSc6d1
a	a	k8xC
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
jsou	být	k5eAaImIp3nP
zdrojem	zdroj	k1gInSc7
především	především	k6eAd1
monzunové	monzunový	k2eAgInPc4d1
deště	dešť	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
přicházejí	přicházet	k5eAaImIp3nP
převážně	převážně	k6eAd1
od	od	k7c2
července	červenec	k1gInSc2
do	do	k7c2
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc4
monzunů	monzun	k1gInPc2
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
rozprostřen	rozprostřen	k2eAgInSc1d1
i	i	k9
na	na	k7c4
horskou	horský	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
do	do	k7c2
výšky	výška	k1gFnSc2
až	až	k9
4	#num#	k4
000	#num#	k4
m.	m.	k?
Vzestup	vzestup	k1gInSc1
vody	voda	k1gFnSc2
začíná	začínat	k5eAaImIp3nS
na	na	k7c6
konci	konec	k1gInSc6
dubna	duben	k1gInSc2
až	až	k9
na	na	k7c6
začátku	začátek	k1gInSc6
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnPc4d3
úrovně	úroveň	k1gFnPc4
dosahuje	dosahovat	k5eAaImIp3nS
hladina	hladina	k1gFnSc1
většinou	většinou	k6eAd1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
srpna	srpen	k1gInSc2
až	až	k6eAd1
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
vzestup	vzestup	k1gInSc1
hladiny	hladina	k1gFnSc2
je	být	k5eAaImIp3nS
10	#num#	k4
m	m	kA
a	a	k8xC
při	při	k7c6
velkých	velký	k2eAgFnPc6d1
povodních	povodeň	k1gFnPc6
to	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
až	až	k9
15	#num#	k4
m	m	kA
i	i	k8xC
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
chladnou	chladný	k2eAgFnSc4d1
část	část	k1gFnSc4
roku	rok	k1gInSc2
má	mít	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
sníženou	snížený	k2eAgFnSc4d1
vodnatost	vodnatost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
úroveň	úroveň	k1gFnSc4
hladiny	hladina	k1gFnSc2
v	v	k7c6
deltě	delta	k1gFnSc6
mají	mít	k5eAaImIp3nP
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
mořské	mořský	k2eAgInPc1d1
přílivy	příliv	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ústí	ústí	k1gNnSc6
Huglí	Hugle	k1gFnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
5,5	5,5	k4
m	m	kA
a	a	k8xC
u	u	k7c2
Kalkaty	Kalkata	k1gFnSc2
3	#num#	k4
až	až	k9
3,4	3,4	k4
m.	m.	k?
Přílivy	příliv	k1gInPc1
zasahují	zasahovat	k5eAaImIp3nP
až	až	k9
300	#num#	k4
km	km	kA
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
průtok	průtok	k1gInSc1
vody	voda	k1gFnSc2
v	v	k7c6
ústí	ústí	k1gNnSc6
spolu	spolu	k6eAd1
s	s	k7c7
Brahmaputrou	Brahmaputra	k1gFnSc7
je	být	k5eAaImIp3nS
35	#num#	k4
000	#num#	k4
až	až	k9
38	#num#	k4
000	#num#	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
1	#num#	k4
200	#num#	k4
km³	km³	k?
za	za	k7c4
rok	rok	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
unáší	unášet	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
350	#num#	k4
Mt	Mt	k1gFnPc2
nánosů	nános	k1gInPc2
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
vysvětluje	vysvětlovat	k5eAaImIp3nS
značná	značný	k2eAgFnSc1d1
kalnost	kalnost	k1gFnSc1
vody	voda	k1gFnSc2
řeky	řeka	k1gFnSc2
a	a	k8xC
Bengálského	bengálský	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
u	u	k7c2
jejího	její	k3xOp3gNnSc2
ústí	ústí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Obřady	obřad	k1gInPc1
k	k	k7c3
uctění	uctění	k1gNnSc3
posvátné	posvátný	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
Gangy	Ganga	k1gFnSc2
</s>
<s>
Ganga	Ganga	k1gFnSc1
má	mít	k5eAaImIp3nS
velký	velký	k2eAgInSc4d1
ekonomický	ekonomický	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejím	její	k3xOp3gNnSc6
povodí	povodí	k1gNnSc6
leží	ležet	k5eAaImIp3nS
mnoho	mnoho	k4c1
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
jsou	být	k5eAaImIp3nP
Dillí	Dillí	k1gNnPc4
<g/>
,	,	kIx,
Ágra	Ágrum	k1gNnPc4
<g/>
,	,	kIx,
Murádábád	Murádábáda	k1gFnPc2
<g/>
,	,	kIx,
Kánpur	Kánpura	k1gFnPc2
<g/>
,	,	kIx,
Iláhábád	Iláhábád	k1gInSc4
<g/>
,	,	kIx,
Váránasí	Váránasý	k2eAgMnPc1d1
<g/>
,	,	kIx,
Patna	Patna	k1gFnSc1
<g/>
,	,	kIx,
Kalkata	Kalkata	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalkata	Kalkata	k1gFnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
mořským	mořský	k2eAgInSc7d1
přístavem	přístav	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
povodí	povodí	k1gNnSc2
se	se	k3xPyFc4
široce	široko	k6eAd1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
zavlažování	zavlažování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
z	z	k7c2
řeky	řeka	k1gFnSc2
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
rozebírat	rozebírat	k5eAaImF
hned	hned	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
opustí	opustit	k5eAaPmIp3nP
hory	hora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc4d3
magistrální	magistrální	k2eAgInSc4d1
zavlažovací	zavlažovací	k2eAgInSc4d1
kanály	kanál	k1gInPc4
jsou	být	k5eAaImIp3nP
Hornoganžský	Hornoganžský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Dolnoganžský	Dolnoganžský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Hornojamunský	Hornojamunský	k1gMnSc1
<g/>
,	,	kIx,
Dolnojamunský	Dolnojamunský	k1gMnSc1
<g/>
,	,	kIx,
Východojamunský	Východojamunský	k1gMnSc1
<g/>
,	,	kIx,
Západojamunský	Západojamunský	k1gMnSc1
<g/>
,	,	kIx,
Agra	Agra	k1gMnSc1
<g/>
,	,	kIx,
Sarda	Sard	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Splavná	splavný	k2eAgFnSc1d1
je	on	k3xPp3gMnPc4
do	do	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
1	#num#	k4
450	#num#	k4
km	km	kA
od	od	k7c2
ústí	ústí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
deltě	delta	k1gFnSc6
je	být	k5eAaImIp3nS
říční	říční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
základní	základní	k2eAgFnSc1d1
a	a	k8xC
často	často	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
možnou	možný	k2eAgFnSc7d1
dopravou	doprava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeka	řeka	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
stále	stále	k6eAd1
prudce	prudko	k6eAd1
rostoucí	rostoucí	k2eAgFnSc3d1
populaci	populace	k1gFnSc3
<g/>
,	,	kIx,
zemědělství	zemědělství	k1gNnSc6
i	i	k8xC
průmyslu	průmysl	k1gInSc6
jako	jako	k8xS,k8xC
zdroj	zdroj	k1gInSc1
pitné	pitný	k2eAgFnSc2d1
i	i	k8xC
užitkové	užitkový	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
kanál	kanál	k1gInSc4
i	i	k8xC
dopravní	dopravní	k2eAgFnSc1d1
tepna	tepna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
její	její	k3xOp3gNnSc1
enormní	enormní	k2eAgNnSc1d1
znečištění	znečištění	k1gNnSc1
<g/>
,	,	kIx,
kvalita	kvalita	k1gFnSc1
vody	voda	k1gFnSc2
se	se	k3xPyFc4
přitom	přitom	k6eAd1
stále	stále	k6eAd1
zhoršuje	zhoršovat	k5eAaImIp3nS
vlivem	vliv	k1gInSc7
průmyslového	průmyslový	k2eAgInSc2d1
a	a	k8xC
městského	městský	k2eAgInSc2d1
odpadu	odpad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doposud	doposud	k6eAd1
bylo	být	k5eAaImAgNnS
uzavřeno	uzavřít	k5eAaPmNgNnS
237	#num#	k4
velkých	velký	k2eAgInPc2d1
průmyslových	průmyslový	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
do	do	k7c2
řeky	řeka	k1gFnSc2
vypouštěly	vypouštět	k5eAaImAgFnP
nečistoty	nečistota	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
nedochází	docházet	k5eNaImIp3nS
k	k	k7c3
závažným	závažný	k2eAgFnPc3d1
epidemiím	epidemie	k1gFnPc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
by	by	kYmCp3nS
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
očekávat	očekávat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
voda	voda	k1gFnSc1
se	se	k3xPyFc4
ani	ani	k9
při	při	k7c6
dlouhodobém	dlouhodobý	k2eAgNnSc6d1
skladování	skladování	k1gNnSc6
nekazí	kazit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vysvětlení	vysvětlení	k1gNnSc3
tohoto	tento	k3xDgInSc2
jevu	jev	k1gInSc2
přispělo	přispět	k5eAaPmAgNnS
objevení	objevení	k1gNnSc1
bakteriofágů	bakteriofág	k1gInPc2
v	v	k7c6
její	její	k3xOp3gFnSc6
vodě	voda	k1gFnSc6
a	a	k8xC
neobvykle	obvykle	k6eNd1
vysoké	vysoký	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
vázat	vázat	k5eAaImF
kyslík	kyslík	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Posvátná	posvátný	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
</s>
<s>
Řeka	řeka	k1gFnSc1
Ganga	Ganga	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
védská	védský	k2eAgNnPc4d1
božstva	božstvo	k1gNnPc4
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
zobrazována	zobrazován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
říční	říční	k2eAgFnSc1d1
bohyně	bohyně	k1gFnSc1
jedoucí	jedoucí	k2eAgFnSc1d1
na	na	k7c6
krokodýlu	krokodýl	k1gMnSc6
a	a	k8xC
v	v	k7c6
mýtických	mýtický	k2eAgInPc6d1
textech	text	k1gInPc6
je	být	k5eAaImIp3nS
chotí	choť	k1gFnSc7
boha	bůh	k1gMnSc2
Šiva	Šivus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
jejích	její	k3xOp3gFnPc6
vodách	voda	k1gFnPc6
omývají	omývat	k5eAaImIp3nP
miliony	milion	k4xCgInPc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
rituální	rituální	k2eAgFnPc1d1
koupele	koupel	k1gFnPc1
mají	mít	k5eAaImIp3nP
očistit	očistit	k5eAaPmF
duši	duše	k1gFnSc4
i	i	k8xC
tělo	tělo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Gangy	Ganga	k1gFnSc2
se	se	k3xPyFc4
také	také	k9
sype	sypat	k5eAaImIp3nS
popel	popel	k1gInSc1
z	z	k7c2
těl	tělo	k1gNnPc2
zemřelých	zemřelý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
v	v	k7c6
naději	naděje	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
tak	tak	k6eAd1
budou	být	k5eAaImBp3nP
osvobozeni	osvobodit	k5eAaPmNgMnP
ze	z	k7c2
samsáry	samsár	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Mezinárodní	mezinárodní	k2eAgNnSc1d1
povodí	povodí	k1gNnSc4
řek	řeka	k1gFnPc2
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
Ganges-Brahmaputra-Meghna	Ganges-Brahmaputra-Meghen	k2eAgFnSc1d1
<g/>
↑	↑	k?
Internetová	internetový	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
připouští	připouštět	k5eAaImIp3nS
jak	jak	k6eAd1
tvar	tvar	k1gInSc4
Ganga	Ganga	k1gFnSc1
<g/>
,	,	kIx,
-y	-y	k?
(	(	kIx(
<g/>
ženský	ženský	k2eAgInSc1d1
rod	rod	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
tvar	tvar	k1gInSc1
Ganges	Ganges	k1gInSc1
<g/>
,	,	kIx,
-gu	-gu	k?
(	(	kIx(
<g/>
mužský	mužský	k2eAgInSc1d1
rod	rod	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
s	s	k7c7
poznámkou	poznámka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
frekventovanější	frekventovaný	k2eAgInPc1d2
jsou	být	k5eAaImIp3nP
tvary	tvar	k1gInPc1
podoby	podoba	k1gFnSc2
Ganga	Ganga	k1gFnSc1
<g/>
,	,	kIx,
ženský	ženský	k2eAgInSc1d1
rod	rod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidla	pravidlo	k1gNnPc1
českého	český	k2eAgInSc2d1
pravopisu	pravopis	k1gInSc2
z	z	k7c2
r.	r.	kA
1993	#num#	k4
znají	znát	k5eAaImIp3nP
pouze	pouze	k6eAd1
tvar	tvar	k1gInSc4
Ganga	Ganga	k1gFnSc1
<g/>
,	,	kIx,
-y	-y	k?
(	(	kIx(
<g/>
ženský	ženský	k2eAgInSc1d1
rod	rod	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgFnP
použity	použit	k2eAgFnPc1d1
informace	informace	k1gFnPc1
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
sovětské	sovětský	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Г	Г	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ganga	Ganga	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Tající	tající	k2eAgInPc1d1
ledovce	ledovec	k1gInPc1
děsí	děsit	k5eAaImIp3nP
Gangu	Ganga	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Indie	Indie	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
432467	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4019227-1	4019227-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85053016	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
134989216	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85053016	#num#	k4
</s>
