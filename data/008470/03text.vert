<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
Kaskádový	kaskádový	k2eAgInSc1d1	kaskádový
ledovec	ledovec	k1gInSc1	ledovec
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
South	South	k1gInSc1	South
Cascade	Cascad	k1gInSc5	Cascad
Glacier	Glacier	k1gInSc1	Glacier
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
horský	horský	k2eAgInSc1d1	horský
ledovec	ledovec	k1gInSc1	ledovec
v	v	k7c6	v
Severních	severní	k2eAgFnPc6d1	severní
Kaskádách	kaskáda	k1gFnPc6	kaskáda
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
2	[number]	k4	2
518	[number]	k4	518
metrů	metr	k1gInPc2	metr
vysokou	vysoký	k2eAgFnSc7d1	vysoká
horou	hora	k1gFnSc7	hora
Sentinel	sentinel	k1gInSc1	sentinel
Peak	Peako	k1gNnPc2	Peako
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
27	[number]	k4	27
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
hory	hora	k1gFnSc2	hora
Glacier	Glacier	k1gMnSc1	Glacier
Peak	Peak	k1gMnSc1	Peak
v	v	k7c6	v
divočině	divočina	k1gFnSc6	divočina
Glacier	Glacier	k1gMnSc1	Glacier
Peak	Peak	k1gMnSc1	Peak
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
z	z	k7c2	z
ledovce	ledovec	k1gInSc2	ledovec
je	být	k5eAaImIp3nS	být
odváděna	odvádět	k5eAaImNgFnS	odvádět
do	do	k7c2	do
Jižního	jižní	k2eAgNnSc2d1	jižní
Kaskádového	kaskádový	k2eAgNnSc2d1	kaskádové
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
jižního	jižní	k2eAgNnSc2d1	jižní
ramene	rameno	k1gNnSc2	rameno
řeky	řeka	k1gFnSc2	řeka
Cascade	Cascad	k1gInSc5	Cascad
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
přítokem	přítok	k1gInSc7	přítok
řeky	řeka	k1gFnSc2	řeka
Skagit	Skagit	k1gInSc1	Skagit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ledovec	ledovec	k1gInSc4	ledovec
nyní	nyní	k6eAd1	nyní
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
glaciologové	glaciolog	k1gMnPc1	glaciolog
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
studují	studovat	k5eAaImIp3nP	studovat
dopad	dopad	k1gInSc4	dopad
oteplování	oteplování	k1gNnSc2	oteplování
klimatu	klima	k1gNnSc2	klima
na	na	k7c4	na
tání	tání	k1gNnSc4	tání
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1958	[number]	k4	1958
a	a	k8xC	a
2009	[number]	k4	2009
se	se	k3xPyFc4	se
ledovec	ledovec	k1gInSc1	ledovec
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
o	o	k7c4	o
celou	celý	k2eAgFnSc4d1	celá
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
South	Southa	k1gFnPc2	Southa
Cascade	Cascad	k1gInSc5	Cascad
Glacier	Glaciero	k1gNnPc2	Glaciero
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
