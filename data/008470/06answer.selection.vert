<s>
Jižní	jižní	k2eAgInSc1d1	jižní
Kaskádový	kaskádový	k2eAgInSc1d1	kaskádový
ledovec	ledovec	k1gInSc1	ledovec
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
South	South	k1gInSc1	South
Cascade	Cascad	k1gInSc5	Cascad
Glacier	Glacier	k1gInSc1	Glacier
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
horský	horský	k2eAgInSc1d1	horský
ledovec	ledovec	k1gInSc1	ledovec
v	v	k7c6	v
Severních	severní	k2eAgFnPc6d1	severní
Kaskádách	kaskáda	k1gFnPc6	kaskáda
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
