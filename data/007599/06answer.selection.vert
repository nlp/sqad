<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gNnSc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
resp.	resp.	kA	resp.
československou	československý	k2eAgFnSc4d1	Československá
samostatnost	samostatnost	k1gFnSc4	samostatnost
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
zejména	zejména	k9	zejména
Edvarda	Edvard	k1gMnSc4	Edvard
Beneše	Beneš	k1gMnSc4	Beneš
a	a	k8xC	a
Milana	Milan	k1gMnSc4	Milan
Rastislava	Rastislav	k1gMnSc4	Rastislav
Štefánika	Štefánik	k1gMnSc4	Štefánik
<g/>
.	.	kIx.	.
</s>
