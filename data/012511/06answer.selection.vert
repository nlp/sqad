<s>
Waltz	waltz	k1gInSc1	waltz
je	být	k5eAaImIp3nS	být
tanec	tanec	k1gInSc4	tanec
v	v	k7c6	v
tříčtvrťovém	tříčtvrťový	k2eAgInSc6d1	tříčtvrťový
taktu	takt	k1gInSc6	takt
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
současná	současný	k2eAgFnSc1d1	současná
verze	verze	k1gFnSc1	verze
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
tance	tanec	k1gInSc2	tanec
boston	boston	k1gInSc1	boston
a	a	k8xC	a
evropského	evropský	k2eAgInSc2d1	evropský
ländleru	ländler	k1gInSc2	ländler
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc1	předchůdce
valčíku	valčík	k1gInSc2	valčík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
