<p>
<s>
Waltz	waltz	k1gInSc1	waltz
je	být	k5eAaImIp3nS	být
tanec	tanec	k1gInSc4	tanec
v	v	k7c6	v
tříčtvrťovém	tříčtvrťový	k2eAgInSc6d1	tříčtvrťový
taktu	takt	k1gInSc6	takt
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
současná	současný	k2eAgFnSc1d1	současná
verze	verze	k1gFnSc1	verze
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
tance	tanec	k1gInSc2	tanec
boston	boston	k1gInSc1	boston
a	a	k8xC	a
evropského	evropský	k2eAgInSc2d1	evropský
ländleru	ländler	k1gInSc2	ländler
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc1	předchůdce
valčíku	valčík	k1gInSc2	valčík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Waltz	waltz	k1gInSc1	waltz
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
tzv.	tzv.	kA	tzv.
standardních	standardní	k2eAgInPc2d1	standardní
tanců	tanec	k1gInPc2	tanec
(	(	kIx(	(
<g/>
waltz	waltz	k1gInSc1	waltz
<g/>
,	,	kIx,	,
tango	tango	k1gNnSc1	tango
<g/>
,	,	kIx,	,
valčík	valčík	k1gInSc1	valčík
<g/>
,	,	kIx,	,
slowfox	slowfox	k1gInSc1	slowfox
<g/>
,	,	kIx,	,
quickstep	quickstep	k1gInSc1	quickstep
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tempo	tempo	k1gNnSc1	tempo
waltzu	waltz	k1gInSc2	waltz
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
29	[number]	k4	29
taktů	takt	k1gInPc2	takt
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
(	(	kIx(	(
<g/>
87	[number]	k4	87
M.	M.	kA	M.
<g/>
M.	M.	kA	M.
=	=	kIx~	=
taktových	taktový	k2eAgInPc2d1	taktový
úderů	úder	k1gInPc2	úder
metronomu	metronom	k1gInSc2	metronom
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
tempo	tempo	k1gNnSc1	tempo
valčíku	valčík	k1gInSc2	valčík
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
dvojnásobné	dvojnásobný	k2eAgNnSc1d1	dvojnásobné
<g/>
,	,	kIx,	,
60	[number]	k4	60
taktů	takt	k1gInPc2	takt
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důrazná	důrazný	k2eAgFnSc1d1	důrazná
doba	doba	k1gFnSc1	doba
v	v	k7c6	v
taktu	takt	k1gInSc6	takt
je	být	k5eAaImIp3nS	být
první	první	k4xOgMnSc1	první
(	(	kIx(	(
<g/>
RAZ	razit	k5eAaImRp2nS	razit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
-dva-tři	var	k1gMnPc1	-dva-tr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Waltz	waltz	k1gInSc1	waltz
je	být	k5eAaImIp3nS	být
švihový	švihový	k2eAgInSc1d1	švihový
tanec	tanec	k1gInSc1	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Držení	držení	k1gNnSc1	držení
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgMnSc1d1	standardní
-	-	kIx~	-
partner	partner	k1gMnSc1	partner
drží	držet	k5eAaImIp3nS	držet
dlaní	dlaň	k1gFnSc7	dlaň
pravé	pravý	k2eAgFnSc2d1	pravá
ruky	ruka	k1gFnSc2	ruka
partnerku	partnerka	k1gFnSc4	partnerka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
levé	levý	k2eAgFnSc2d1	levá
lopatky	lopatka	k1gFnSc2	lopatka
<g/>
,	,	kIx,	,
partnerčina	partnerčin	k2eAgFnSc1d1	partnerčina
levá	levý	k2eAgFnSc1d1	levá
ruka	ruka	k1gFnSc1	ruka
volně	volně	k6eAd1	volně
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
partnerově	partnerův	k2eAgFnSc3d1	partnerova
pravé	pravý	k2eAgFnSc3d1	pravá
paži	paže	k1gFnSc3	paže
a	a	k8xC	a
prsty	prst	k1gInPc4	prst
objímá	objímat	k5eAaImIp3nS	objímat
jeho	jeho	k3xOp3gInSc4	jeho
deltový	deltový	k2eAgInSc4d1	deltový
sval	sval	k1gInSc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
Partnerova	partnerův	k2eAgFnSc1d1	partnerova
levá	levý	k2eAgFnSc1d1	levá
ruka	ruka	k1gFnSc1	ruka
drží	držet	k5eAaImIp3nS	držet
partnerčinu	partnerčin	k2eAgFnSc4d1	partnerčina
pravou	pravá	k1gFnSc4	pravá
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
očí	oko	k1gNnPc2	oko
partnerky	partnerka	k1gFnSc2	partnerka
<g/>
;	;	kIx,	;
lokty	loket	k1gInPc1	loket
jsou	být	k5eAaImIp3nP	být
ohnuté	ohnutý	k2eAgInPc1d1	ohnutý
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
cca	cca	kA	cca
135	[number]	k4	135
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
držení	držení	k1gNnSc2	držení
<g/>
,	,	kIx,	,
především	především	k9	především
spojených	spojený	k2eAgFnPc2d1	spojená
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
u	u	k7c2	u
každého	každý	k3xTgInSc2	každý
páru	pár	k1gInSc2	pár
trochu	trochu	k6eAd1	trochu
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
;	;	kIx,	;
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c6	na
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
výšce	výška	k1gFnSc6	výška
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
waltz	waltz	k1gInSc1	waltz
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Waltz	waltz	k1gInSc1	waltz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
Slow	Slow	k1gFnSc1	Slow
Waltz	waltz	k1gInSc1	waltz
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
English	English	k1gInSc1	English
Waltz	waltz	k1gInSc1	waltz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Valčík	valčík	k1gInSc1	valčík
pak	pak	k6eAd1	pak
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Viennese	Viennese	k1gFnSc1	Viennese
Waltz	waltz	k1gInSc1	waltz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
pohybem	pohyb	k1gInSc7	pohyb
ve	v	k7c6	v
waltzu	waltz	k1gInSc6	waltz
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
změna	změna	k1gFnSc1	změna
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Closed	Closed	k1gInSc1	Closed
Change	change	k1gFnSc2	change
<g/>
)	)	kIx)	)
pravou	pravý	k2eAgFnSc7d1	pravá
nohou	noha	k1gFnSc7	noha
nebo	nebo	k8xC	nebo
levou	levý	k2eAgFnSc7d1	levá
nohou	noha	k1gFnSc7	noha
(	(	kIx(	(
<g/>
vždy	vždy	k6eAd1	vždy
krok	krok	k1gInSc4	krok
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
krok	krok	k1gInSc4	krok
stranou	stranou	k6eAd1	stranou
a	a	k8xC	a
přísun	přísun	k1gInSc4	přísun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Landsfeld	Landsfeld	k1gMnSc1	Landsfeld
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Technika	technika	k1gFnSc1	technika
standardních	standardní	k2eAgInPc2d1	standardní
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
</s>
</p>
