<s>
Velká	velký	k2eAgFnSc1d1
Deštná	deštný	k2eAgFnSc1d1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Großkoppe	Großkopp	k1gInSc5
či	či	k8xC
Deschneyer	Deschneyer	k1gInSc1
Koppe	Kopp	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
1116	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
nejvyšší	vysoký	k2eAgFnSc1d3
horou	hora	k1gFnSc7
Orlických	orlický	k2eAgFnPc2d1
hor.	hor.	k?
Dříve	dříve	k6eAd2
udávaná	udávaný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
1115	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
je	být	k5eAaImIp3nS
výškou	výška	k1gFnSc7
geodetického	geodetický	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ne	ne	k9
nejvyšším	vysoký	k2eAgNnSc7d3
místem	místo	k1gNnSc7
<g/>
.	.	kIx.
</s>