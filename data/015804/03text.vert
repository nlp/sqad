<s>
Velká	velký	k2eAgFnSc1d1
Deštná	deštný	k2eAgFnSc1d1
</s>
<s>
Velká	velká	k1gFnSc1
Deštná	deštný	k2eAgFnSc1d1
Výhled	výhled	k1gInSc4
z	z	k7c2
rozhledny	rozhledna	k1gFnSc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Velké	velký	k2eAgFnSc2d1
Deštné	deštný	k2eAgFnSc2d1
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
1116	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
587	#num#	k4
m	m	kA
↓	↓	k?
Mladkovské	Mladkovský	k2eAgNnSc4d1
sedlo	sedlo	k1gNnSc4
Izolace	izolace	k1gFnSc2
</s>
<s>
29	#num#	k4
km	km	kA
→	→	k?
Czarna	Czarna	k1gFnSc1
Góra	Góra	k1gMnSc1
(	(	kIx(
<g/>
Král	Král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sněžník	Sněžník	k1gInSc1
<g/>
)	)	kIx)
Seznamy	seznam	k1gInPc1
</s>
<s>
Tisícovky	tisícovka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
#	#	kIx~
<g/>
177	#num#	k4
<g/>
Ultratisícovky	Ultratisícovka	k1gFnPc1
#	#	kIx~
<g/>
52	#num#	k4
<g/>
Nejprominentnější	prominentní	k2eAgFnPc1d3
hory	hora	k1gFnPc1
CZ	CZ	kA
#	#	kIx~
<g/>
8	#num#	k4
<g/>
Hory	hora	k1gFnSc2
a	a	k8xC
kopce	kopec	k1gInSc2
Orlických	orlický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
#	#	kIx~
<g/>
1	#num#	k4
Poznámka	poznámka	k1gFnSc1
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Orlických	orlický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
a	a	k8xC
okresu	okres	k1gInSc2
Rychnov	Rychnov	k1gInSc4
nad	nad	k7c7
Kněžnou	kněžna	k1gFnSc7
Poloha	poloha	k1gFnSc1
Stát	stát	k5eAaImF,k5eAaPmF
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Orlické	orlický	k2eAgFnPc1d1
hory	hora	k1gFnPc1
/	/	kIx~
Deštenská	Deštenský	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
/	/	kIx~
Orlický	orlický	k2eAgInSc1d1
hřbet	hřbet	k1gInSc1
/	/	kIx~
Deštenský	Deštenský	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Deštná	deštný	k2eAgFnSc1d1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
ortorula	ortorula	k1gFnSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Divoká	divoký	k2eAgFnSc1d1
Orlice	Orlice	k1gFnSc1
→	→	k?
Orlice	Orlice	k1gFnSc1
→	→	k?
Labe	Labe	k1gNnSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Deštná	deštný	k2eAgFnSc1d1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Großkoppe	Großkopp	k1gInSc5
či	či	k8xC
Deschneyer	Deschneyer	k1gInSc1
Koppe	Kopp	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
1116	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
nejvyšší	vysoký	k2eAgFnSc1d3
horou	hora	k1gFnSc7
Orlických	orlický	k2eAgFnPc2d1
hor.	hor.	k?
Dříve	dříve	k6eAd2
udávaná	udávaný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
1115	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
je	být	k5eAaImIp3nS
výškou	výška	k1gFnSc7
geodetického	geodetický	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
ne	ne	k9
nejvyšším	vysoký	k2eAgNnSc7d3
místem	místo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
3	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
Deštného	deštný	k2eAgNnSc2d1
v	v	k7c6
Orlických	orlický	k2eAgFnPc6d1
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Má	mít	k5eAaImIp3nS
také	také	k9
7	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc6d3
prominenci	prominence	k1gFnSc6
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc3d3
izolaci	izolace	k1gFnSc3
ze	z	k7c2
všech	všecek	k3xTgMnPc2
českých	český	k2eAgMnPc2d1
hor.	hor.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
stála	stát	k5eAaImAgFnS
již	již	k6eAd1
koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
dřevěná	dřevěný	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tu	tu	k6eAd1
sice	sice	k8xC
vichr	vichr	k1gInSc1
a	a	k8xC
bouře	bouř	k1gFnPc1
rozmetaly	rozmetat	k5eAaImAgFnP,k5eAaPmAgFnP
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
podobách	podoba	k1gFnPc6
pravidelně	pravidelně	k6eAd1
obnovována	obnovován	k2eAgFnSc1d1
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
stržena	stržen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1992	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
postavena	postaven	k2eAgFnSc1d1
8	#num#	k4
metrů	metr	k1gInPc2
vysoká	vysoký	k2eAgFnSc1d1
dřevěná	dřevěný	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
pyramidy	pyramida	k1gFnSc2
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
otesaných	otesaný	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
Štefanova	Štefanův	k2eAgFnSc1d1
vyhlídka	vyhlídka	k1gFnSc1
<g/>
,	,	kIx,
vystavěna	vystavěn	k2eAgFnSc1d1
skauty	skaut	k1gMnPc4
jako	jako	k8xC,k8xS
dárek	dárek	k1gInSc4
hajnému	hajný	k1gMnSc3
a	a	k8xC
členovi	člen	k1gMnSc3
Horské	Horské	k2eAgFnSc2d1
služby	služba	k1gFnSc2
Štefanu	Štefan	k1gMnSc3
Matějkovi	Matějka	k1gMnSc3
k	k	k7c3
jeho	jeho	k3xOp3gInSc7
60	#num#	k4
<g/>
.	.	kIx.
narozeninám	narozeniny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
rozhledna	rozhledna	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
z	z	k7c2
důvodu	důvod	k1gInSc2
bezpečnosti	bezpečnost	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
nahrazena	nahradit	k5eAaPmNgFnS
rozhlednou	rozhledna	k1gFnSc7
novou	nový	k2eAgFnSc7d1
<g/>
,	,	kIx,
také	také	k9
dřevěnou	dřevěný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
ale	ale	k8xC
také	také	k9
neodolala	odolat	k5eNaPmAgFnS
drsnému	drsný	k2eAgNnSc3d1
klimatu	klima	k1gNnSc3
a	a	k8xC
v	v	k7c6
létě	léto	k1gNnSc6
2010	#num#	k4
byla	být	k5eAaImAgFnS
stržena	stržen	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2018	#num#	k4
a	a	k8xC
2019	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
vystavěna	vystavěn	k2eAgFnSc1d1
devatenáctimetrová	devatenáctimetrový	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostní	slavnostní	k2eAgInSc4d1
otevření	otevření	k1gNnSc1
rozhledny	rozhledna	k1gFnSc2
proběhlo	proběhnout	k5eAaPmAgNnS
dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
rozhledny	rozhledna	k1gFnSc2
je	být	k5eAaImIp3nS
výhled	výhled	k1gInSc1
do	do	k7c2
širokého	široký	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
-	-	kIx~
na	na	k7c4
Rychnovsko	Rychnovsko	k1gNnSc4
<g/>
,	,	kIx,
Stolové	stolový	k2eAgFnPc4d1
hory	hora	k1gFnPc4
i	i	k8xC
Králický	králický	k2eAgInSc4d1
Sněžník	Sněžník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Přístup	přístup	k1gInSc1
</s>
<s>
Kiosek	kiosek	k1gInSc1
nedaleko	nedaleko	k7c2
vrcholu	vrchol	k1gInSc2
Velké	velký	k2eAgFnSc2d1
Deštné	deštný	k2eAgFnSc2d1
</s>
<s>
Přes	přes	k7c4
vrchol	vrchol	k1gInSc4
vede	vést	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc1d1
hřebenová	hřebenový	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
Orlických	orlický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
-	-	kIx~
červeně	červeně	k6eAd1
značená	značený	k2eAgFnSc1d1
Jiráskova	Jiráskův	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
v	v	k7c6
těchto	tento	k3xDgNnPc6
místech	místo	k1gNnPc6
kopíruje	kopírovat	k5eAaImIp3nS
i	i	k9
cyklotrasa	cyklotrasa	k1gFnSc1
č.	č.	k?
4071	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsnadnější	snadný	k2eAgInSc1d3
přístup	přístup	k1gInSc1
je	být	k5eAaImIp3nS
po	po	k7c6
Jiráskově	Jiráskův	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
ze	z	k7c2
sedla	sedlo	k1gNnSc2
pod	pod	k7c7
Šerlichem	Šerlich	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
parkoviště	parkoviště	k1gNnSc4
a	a	k8xC
zastávka	zastávka	k1gFnSc1
autobusu	autobus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
výstup	výstup	k1gInSc4
po	po	k7c6
zelené	zelený	k2eAgFnSc6d1
značce	značka	k1gFnSc6
od	od	k7c2
osady	osada	k1gFnSc2
Luisino	Luisin	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
je	být	k5eAaImIp3nS
nedaleko	nedaleko	k7c2
vrcholu	vrchol	k1gInSc2
otevřen	otevřít	k5eAaPmNgInS
kiosek	kiosek	k1gInSc1
(	(	kIx(
<g/>
chata	chata	k1gFnSc1
<g/>
)	)	kIx)
Horské	Horské	k2eAgFnSc2d1
služby	služba	k1gFnSc2
se	s	k7c7
základním	základní	k2eAgNnSc7d1
občerstvením	občerstvení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
ČR	ČR	kA
1	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
000	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřický	zeměměřický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Prohlížecí	prohlížecí	k2eAgFnSc1d1
služba	služba	k1gFnSc1
WMS	WMS	kA
-	-	kIx~
Bodová	bodový	k2eAgFnSc1d1
pole	pole	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřický	zeměměřický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
a	a	k8xC
Velká	velká	k1gFnSc1
Deštná	deštný	k2eAgFnSc1d1
na	na	k7c4
Ultratisicovky	Ultratisicovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Z	z	k7c2
rozhledny	rozhledna	k1gFnSc2
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Deštné	deštný	k2eAgInPc1d1
zbyly	zbýt	k5eAaPmAgInP
špalíky	špalík	k1gInPc1
(	(	kIx(
<g/>
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Na	na	k7c4
Velké	velký	k2eAgNnSc4d1
Deštné	deštný	k2eAgNnSc4d1
se	se	k3xPyFc4
otevře	otevřít	k5eAaPmIp3nS
nová	nový	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kudy	kudy	k6eAd1
z	z	k7c2
nudy	nuda	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-10-24	2019-10-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Velká	velká	k1gFnSc1
Deštná	deštný	k2eAgFnSc1d1
na	na	k7c4
Orlickehory	Orlickehor	k1gMnPc4
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
Velká	velká	k1gFnSc1
Deštná	deštný	k2eAgFnSc1d1
na	na	k7c4
Tisicovky	Tisicovka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Podrobný	podrobný	k2eAgInSc4d1
popis	popis	k1gInSc4
výstupu	výstup	k1gInSc2
na	na	k7c4
Velkou	velký	k2eAgFnSc4d1
Deštnou	deštný	k2eAgFnSc4d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Orlické	orlický	k2eAgFnSc2d1
tisícovky	tisícovka	k1gFnSc2
Hlavní	hlavní	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Deštná	deštný	k2eAgFnSc1d1
(	(	kIx(
<g/>
1116	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Koruna	koruna	k1gFnSc1
(	(	kIx(
<g/>
1101	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Malá	malý	k2eAgFnSc1d1
Deštná	deštný	k2eAgFnSc1d1
(	(	kIx(
<g/>
1090	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Jelenka	Jelenka	k1gFnSc1
(	(	kIx(
<g/>
1085	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Vrchmezí	Vrchmeze	k1gFnPc2
(	(	kIx(
<g/>
1084	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Sedloňovský	Sedloňovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
1050	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Tetřevec	tetřevec	k1gMnSc1
(	(	kIx(
<g/>
1043	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
U	u	k7c2
Kunštátské	kunštátský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
(	(	kIx(
<g/>
1041	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Kamenný	kamenný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
1037	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Šerlich	Šerlich	k1gInSc1
(	(	kIx(
<g/>
1027	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Homole	homole	k1gFnSc1
(	(	kIx(
<g/>
1001	#num#	k4
m	m	kA
<g/>
)	)	kIx)
Vedlejší	vedlejší	k2eAgInPc1d1
vrcholy	vrchol	k1gInPc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marušin	Marušin	k2eAgInSc1d1
kámen	kámen	k1gInSc1
(	(	kIx(
<g/>
1044	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
U	u	k7c2
Kunštátské	kunštátský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
JV	JV	kA
(	(	kIx(
<g/>
1040	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Nad	nad	k7c7
Bukačkou	Bukačka	k1gFnSc7
(	(	kIx(
<g/>
1035	#num#	k4
m	m	kA
<g/>
)	)	kIx)
•	•	k?
Šerlich	Šerlich	k1gInSc1
JV	JV	kA
(	(	kIx(
<g/>
1019	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
