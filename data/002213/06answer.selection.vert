<s>
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zkráceně	zkráceně	k6eAd1	zkráceně
též	též	k9	též
L.A.	L.A.	k1gFnSc1	L.A.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
druhé	druhý	k4xOgFnSc2	druhý
nejlidnatější	lidnatý	k2eAgFnSc2d3	nejlidnatější
město	město	k1gNnSc1	město
USA	USA	kA	USA
<g/>
,	,	kIx,	,
středisko	středisko	k1gNnSc1	středisko
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
okresu	okres	k1gInSc2	okres
<g/>
.	.	kIx.	.
</s>
