<s>
Velké	velký	k2eAgNnSc1d1	velké
Hořké	hořký	k2eAgNnSc1d1	hořké
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ل	ل	k?	ل
ا	ا	k?	ا
ا	ا	k?	ا
<g/>
,	,	kIx,	,
al-Buhayrah	al-Buhayrah	k1gMnSc1	al-Buhayrah
al-Murra	al-Murra	k1gMnSc1	al-Murra
al-Kubra	al-Kubra	k1gMnSc1	al-Kubra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slané	slaný	k2eAgNnSc4d1	slané
jezero	jezero	k1gNnSc4	jezero
rozdělující	rozdělující	k2eAgInSc1d1	rozdělující
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Úžinou	úžina	k1gFnSc7	úžina
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
Malým	malý	k2eAgNnSc7d1	malé
Hořkým	hořký	k2eAgNnSc7d1	hořké
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
mají	mít	k5eAaImIp3nP	mít
Hořká	hořký	k2eAgNnPc1d1	hořké
jezera	jezero	k1gNnPc1	jezero
plochu	plocha	k1gFnSc4	plocha
okolo	okolo	k7c2	okolo
250	[number]	k4	250
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
průplav	průplav	k1gInSc1	průplav
nemá	mít	k5eNaImIp3nS	mít
žádná	žádný	k3yNgNnPc4	žádný
zdymadla	zdymadlo	k1gNnPc4	zdymadlo
<g/>
,	,	kIx,	,
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
ze	z	k7c2	z
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
a	a	k8xC	a
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
teče	téct	k5eAaImIp3nS	téct
volně	volně	k6eAd1	volně
do	do	k7c2	do
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vypařila	vypařit	k5eAaPmAgFnS	vypařit
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
také	také	k9	také
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
vyrovnávací	vyrovnávací	k2eAgFnSc4d1	vyrovnávací
nádrž	nádrž	k1gFnSc4	nádrž
redukující	redukující	k2eAgInPc4d1	redukující
následky	následek	k1gInPc4	následek
přílivových	přílivový	k2eAgInPc2d1	přílivový
proudů	proud	k1gInPc2	proud
v	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
mořích	moře	k1gNnPc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
a	a	k8xC	a
Velké	velký	k2eAgNnSc1d1	velké
Hořké	hořký	k2eAgNnSc1d1	hořké
jezero	jezero	k1gNnSc1	jezero
před	před	k7c7	před
stavbou	stavba	k1gFnSc7	stavba
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
tvořily	tvořit	k5eAaImAgFnP	tvořit
suché	suchý	k2eAgNnSc4d1	suché
solné	solný	k2eAgNnSc4d1	solné
údolí	údolí	k1gNnSc4	údolí
13	[number]	k4	13
km	km	kA	km
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
5	[number]	k4	5
km	km	kA	km
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
Lesseps	Lesseps	k1gInSc1	Lesseps
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
tloušťku	tloušťka	k1gFnSc4	tloušťka
solného	solný	k2eAgNnSc2d1	solné
lůžka	lůžko	k1gNnSc2	lůžko
na	na	k7c4	na
13,2	[number]	k4	13,2
m	m	kA	m
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
970	[number]	k4	970
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
soli	sůl	k1gFnSc2	sůl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
zaplnil	zaplnit	k5eAaPmAgInS	zaplnit
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
pomalu	pomalu	k6eAd1	pomalu
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
trval	trvat	k5eAaImAgInS	trvat
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
solná	solný	k2eAgFnSc1d1	solná
vrstva	vrstva	k1gFnSc1	vrstva
zcela	zcela	k6eAd1	zcela
rozpuštěná	rozpuštěný	k2eAgFnSc1d1	rozpuštěná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
průplavu	průplav	k1gInSc2	průplav
byla	být	k5eAaImAgFnS	být
salinita	salinita	k1gFnSc1	salinita
68	[number]	k4	68
<g/>
‰	‰	k?	‰
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
52	[number]	k4	52
<g/>
‰	‰	k?	‰
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
49	[number]	k4	49
<g/>
‰	‰	k?	‰
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
44	[number]	k4	44
<g/>
‰	‰	k?	‰
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
Rudém	rudý	k2eAgNnSc6d1	Rudé
a	a	k8xC	a
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
salinita	salinita	k1gFnSc1	salinita
jezer	jezero	k1gNnPc2	jezero
původně	původně	k6eAd1	původně
bránila	bránit	k5eAaImAgFnS	bránit
migraci	migrace	k1gFnSc4	migrace
vodních	vodní	k2eAgMnPc2d1	vodní
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
Rudého	rudý	k1gMnSc2	rudý
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
migrace	migrace	k1gFnSc1	migrace
významný	významný	k2eAgInSc4d1	významný
fenomén	fenomén	k1gInSc4	fenomén
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
Lessepsovská	Lessepsovský	k2eAgFnSc1d1	Lessepsovský
migrace	migrace	k1gFnSc1	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
průplav	průplav	k1gInSc1	průplav
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
a	a	k8xC	a
14	[number]	k4	14
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jedné	jeden	k4xCgFnSc2	jeden
československé	československý	k2eAgFnSc2d1	Československá
lodi	loď	k1gFnSc2	loď
MS	MS	kA	MS
Lednice	Lednice	k1gFnSc2	Lednice
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
lodě	loď	k1gFnPc1	loď
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
flotila	flotila	k1gFnSc1	flotila
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnPc4	jejich
paluby	paluba	k1gFnPc4	paluba
brzo	brzo	k6eAd1	brzo
pokryl	pokrýt	k5eAaPmAgInS	pokrýt
pouštní	pouštní	k2eAgInSc1d1	pouštní
písek	písek	k1gInSc1	písek
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velké	velký	k2eAgFnSc2d1	velká
Hořké	hořká	k1gFnSc2	hořká
jezero	jezero	k1gNnSc4	jezero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
