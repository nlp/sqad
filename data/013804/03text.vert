<s>
Korund	korund	k1gInSc1
</s>
<s>
Korund	korund	k1gInSc1
</s>
<s>
Různé	různý	k2eAgInPc1d1
krystaly	krystal	k1gInPc1
korundu	korund	k1gInSc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Minerál	minerál	k1gInSc1
</s>
<s>
Chemický	chemický	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
bezbarvý	bezbarvý	k2eAgMnSc1d1
<g/>
,	,	kIx,
šedá	šedá	k1gFnSc1
<g/>
,	,	kIx,
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
<g/>
červená	červený	k2eAgFnSc1d1
<g/>
...	...	k?
</s>
<s>
Vzhled	vzhled	k1gInSc1
krystalu	krystal	k1gInSc2
</s>
<s>
bipyramidy	bipyramid	k1gInPc1
<g/>
,	,	kIx,
hexagonální	hexagonální	k2eAgInPc1d1
sloupce	sloupec	k1gInPc1
</s>
<s>
Soustava	soustava	k1gFnSc1
</s>
<s>
klencová	klencový	k2eAgFnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
Lesk	lesk	k1gInSc1
</s>
<s>
skelný	skelný	k2eAgMnSc1d1
<g/>
,	,	kIx,
matný	matný	k2eAgInSc1d1
<g/>
,	,	kIx,
mastný	mastný	k2eAgInSc1d1
</s>
<s>
Štěpnost	štěpnost	k1gFnSc1
</s>
<s>
neštěpný	štěpný	k2eNgInSc1d1
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
1,76	1,76	k4
<g/>
-	-	kIx~
<g/>
1,78	1,78	k4
</s>
<s>
Vryp	vryp	k1gInSc1
</s>
<s>
bílý	bílý	k2eAgInSc1d1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
4,0	4,0	k4
–	–	k?
4,1	4,1	k4
g	g	kA
⋅	⋅	k?
cm	cm	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
</s>
<s>
v	v	k7c6
kyselinách	kyselina	k1gFnPc6
nerozpustný	rozpustný	k2eNgInSc4d1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
–	–	k?
</s>
<s>
Model	model	k1gInSc1
krystalu	krystal	k1gInSc2
korundu	korund	k1gInSc2
</s>
<s>
Korund	korund	k1gInSc1
(	(	kIx(
<g/>
Estner	Estner	k1gInSc1
<g/>
,	,	kIx,
1795	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chemický	chemický	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
(	(	kIx(
<g/>
oxid	oxid	k1gInSc4
hlinitý	hlinitý	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
trigonální	trigonální	k2eAgInSc1d1
(	(	kIx(
<g/>
klencový	klencový	k2eAgInSc1d1
<g/>
)	)	kIx)
minerál	minerál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
historický	historický	k2eAgInSc1d1
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
ze	z	k7c2
staroindického	staroindický	k2eAgMnSc2d1
kuruvinda	kuruvind	k1gMnSc2
–	–	k?
rubín	rubín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Korund	korund	k1gInSc1
vzniká	vznikat	k5eAaImIp3nS
v	v	k7c6
pegmatitech	pegmatit	k1gInPc6
bohatých	bohatý	k2eAgMnPc2d1
na	na	k7c4
hliník	hliník	k1gInSc4
spolu	spolu	k6eAd1
s	s	k7c7
andalusitem	andalusit	k1gInSc7
a	a	k8xC
kyanitem	kyanit	k1gInSc7
a	a	k8xC
v	v	k7c6
horninách	hornina	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
prošly	projít	k5eAaPmAgFnP
kontaktně	kontaktně	k6eAd1
tepelně-tlakou	tepelně-tlaký	k2eAgFnSc7d1
metamorfózou	metamorfóza	k1gFnSc7
(	(	kIx(
<g/>
příkladem	příklad	k1gInSc7
jsou	být	k5eAaImIp3nP
metamorfované	metamorfovaný	k2eAgInPc1d1
bauxity	bauxit	k1gInPc1
<g/>
,	,	kIx,
terry	terra	k1gFnPc1
rosy	rosa	k1gFnSc2
<g/>
,	,	kIx,
jílovité	jílovitý	k2eAgFnSc2d1
horniny	hornina	k1gFnSc2
<g/>
,	,	kIx,
dolomity	dolomit	k1gInPc4
<g/>
,	,	kIx,
svory	svor	k1gInPc4
a	a	k8xC
břidlice	břidlice	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
vzniká	vznikat	k5eAaImIp3nS
v	v	k7c6
prostředí	prostředí	k1gNnSc6
bohatém	bohatý	k2eAgNnSc6d1
na	na	k7c4
hliník	hliník	k1gInSc4
a	a	k8xC
chudším	chudý	k2eAgMnSc7d2
na	na	k7c4
křemík	křemík	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
odolnosti	odolnost	k1gFnSc3
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
také	také	k9
v	v	k7c6
rozsypech	rozsyp	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Morfologie	morfologie	k1gFnSc1
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nP,k5eAaPmIp3nP
zrnité	zrnitý	k2eAgInPc1d1
agregáty	agregát	k1gInPc1
<g/>
,	,	kIx,
krystaly	krystal	k1gInPc1
mají	mít	k5eAaImIp3nP
tvary	tvar	k1gInPc4
tabulek	tabulka	k1gFnPc2
<g/>
,	,	kIx,
bipyramid	bipyramida	k1gFnPc2
<g/>
,	,	kIx,
romboedrů	romboedr	k1gInPc2
a	a	k8xC
sloupečků	sloupeček	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
dvojčatí	dvojčatit	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Valouny	valoun	k1gInPc7
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
v	v	k7c6
rozsypech	rozsyp	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalická	krystalický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
korundu	korund	k1gInSc2
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Tvrdost	tvrdost	k1gFnSc1
9	#num#	k4
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
nejtvrdší	tvrdý	k2eAgInSc1d3
minerál	minerál	k1gInSc1
na	na	k7c6
Mohsově	Mohsův	k2eAgFnSc6d1
stupnici	stupnice	k1gFnSc6
tvrdosti	tvrdost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
křehký	křehký	k2eAgInSc1d1
<g/>
,	,	kIx,
hustota	hustota	k1gFnSc1
4,0	4,0	k4
–	–	k?
4,1	4,1	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm³	cm³	k?
<g/>
,	,	kIx,
neštěpný	štěpný	k2eNgInSc1d1
<g/>
,	,	kIx,
častá	častý	k2eAgFnSc1d1
odlučnost	odlučnost	k1gFnSc1
podle	podle	k7c2
{	{	kIx(
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
,	,	kIx,
lom	lom	k1gInSc1
lasturnatý	lasturnatý	k2eAgInSc1d1
<g/>
,	,	kIx,
tříštivý	tříštivý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Optické	optický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Barva	barva	k1gFnSc1
<g/>
:	:	kIx,
bezbarvý	bezbarvý	k2eAgInSc1d1
<g/>
,	,	kIx,
šedá	šedý	k2eAgFnSc1d1
<g/>
,	,	kIx,
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
červená	červený	k2eAgFnSc1d1
<g/>
,	,	kIx,
hnědá	hnědý	k2eAgFnSc1d1
<g/>
,	,	kIx,
žlutá	žlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
fialová	fialový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesk	lesk	k1gInSc1
skelný	skelný	k2eAgInSc1d1
<g/>
,	,	kIx,
mastný	mastný	k2eAgInSc1d1
<g/>
,	,	kIx,
matný	matný	k2eAgInSc1d1
<g/>
,	,	kIx,
průhlednost	průhlednost	k1gFnSc1
<g/>
:	:	kIx,
průhledný	průhledný	k2eAgInSc1d1
až	až	k8xS
průsvitný	průsvitný	k2eAgInSc1d1
<g/>
,	,	kIx,
vryp	vryp	k1gInSc1
bílý	bílý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
:	:	kIx,
Složení	složení	k1gNnSc1
<g/>
:	:	kIx,
Al	ala	k1gFnPc2
52,93	52,93	k4
%	%	kIx~
<g/>
,	,	kIx,
O	o	k7c6
47,07	47,07	k4
%	%	kIx~
<g/>
,	,	kIx,
příměsi	příměs	k1gFnPc4
Cr	cr	k0
<g/>
,	,	kIx,
Fe	Fe	k1gMnSc1
<g/>
,	,	kIx,
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
V.	V.	kA
</s>
<s>
Odrůdy	odrůda	k1gFnPc1
</s>
<s>
safír	safír	k1gInSc1
–	–	k?
modře	modro	k6eAd1
zbarvený	zbarvený	k2eAgInSc4d1
</s>
<s>
rubín	rubín	k1gInSc1
–	–	k?
červeně	červeně	k6eAd1
zbarvený	zbarvený	k2eAgInSc4d1
</s>
<s>
leukosafír	leukosafír	k1gInSc1
–	–	k?
bezbarvý	bezbarvý	k2eAgInSc1d1
<g/>
,	,	kIx,
průhledný	průhledný	k2eAgInSc1d1
</s>
<s>
smirek	smirek	k1gInSc1
–	–	k?
šedočerný	šedočerný	k2eAgInSc1d1
až	až	k8xS
černý	černý	k2eAgMnSc1d1
<g/>
,	,	kIx,
použití	použití	k1gNnSc2
pro	pro	k7c4
brusiva	brusivo	k1gNnPc4
</s>
<s>
Získávání	získávání	k1gNnSc1
</s>
<s>
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
tvrdosti	tvrdost	k1gFnSc3
je	být	k5eAaImIp3nS
odolný	odolný	k2eAgInSc1d1
proti	proti	k7c3
erozním	erozní	k2eAgInPc3d1
vnějším	vnější	k2eAgInPc3d1
vlivům	vliv	k1gInPc3
<g/>
,	,	kIx,
takže	takže	k8xS
jeho	jeho	k3xOp3gNnSc4
naleziště	naleziště	k1gNnSc4
jsou	být	k5eAaImIp3nP
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
místy	místy	k6eAd1
náplavů	náplav	k1gInPc2
ve	v	k7c6
vrstvách	vrstva	k1gFnPc6
sedimentů	sediment	k1gInPc2
a	a	k8xC
nebo	nebo	k8xC
jako	jako	k9
nedokonale	dokonale	k6eNd1
omezený	omezený	k2eAgInSc1d1
krystal	krystal	k1gInSc1
v	v	k7c6
mateční	mateční	k2eAgFnSc6d1
hornině	hornina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Syntetický	syntetický	k2eAgInSc1d1
korund	korund	k1gInSc1
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
přetavením	přetavení	k1gNnSc7
oxidu	oxid	k1gInSc2
hlinitého	hlinitý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
tavba	tavba	k1gFnSc1
provádí	provádět	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
v	v	k7c6
elektrických	elektrický	k2eAgFnPc6d1
pecích	pec	k1gFnPc6
a	a	k8xC
výsledný	výsledný	k2eAgInSc1d1
produkt	produkt	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
elektrokorund	elektrokorund	k1gInSc1
-	-	kIx~
elektrit	elektrit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
korundu	korund	k1gInSc2
ho	on	k3xPp3gMnSc4
předurčuje	předurčovat	k5eAaImIp3nS
v	v	k7c4
použíti	použít	k5eAaPmF
v	v	k7c6
brusírenství	brusírenství	k1gNnSc6
na	na	k7c4
brusné	brusný	k2eAgInPc4d1
materiály	materiál	k1gInPc4
a	a	k8xC
kotouče	kotouč	k1gInPc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
je	být	k5eAaImIp3nS
s	s	k7c7
ním	on	k3xPp3gNnSc7
možné	možný	k2eAgNnSc1d1
brousit	brousit	k5eAaImF
vyjma	vyjma	k7c2
diamantu	diamant	k1gInSc2
všechny	všechen	k3xTgInPc4
ostatní	ostatní	k2eAgInPc4d1
minerály	minerál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
jeho	jeho	k3xOp3gNnSc1
použití	použití	k1gNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
ložisek	ložisko	k1gNnPc2
v	v	k7c6
přístrojích	přístroj	k1gInPc6
(	(	kIx(
<g/>
hodinky	hodinka	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
syntetické	syntetický	k2eAgInPc1d1
krystaly	krystal	k1gInPc1
(	(	kIx(
<g/>
rubín	rubín	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
využívány	využívat	k5eAaImNgInP,k5eAaPmNgInP
v	v	k7c6
optických	optický	k2eAgInPc6d1
systémech	systém	k1gInPc6
a	a	k8xC
na	na	k7c4
výrobu	výroba	k1gFnSc4
laserů	laser	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
šperkařství	šperkařství	k1gNnSc6
využíván	využíván	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
drahý	drahý	k2eAgInSc1d1
kámen	kámen	k1gInSc1
(	(	kIx(
<g/>
fasetové	fasetový	k2eAgInPc1d1
brusy	brus	k1gInPc1
<g/>
,	,	kIx,
kabošony	kabošon	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Korund	korund	k1gInSc1
se	se	k3xPyFc4
také	také	k9
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
opracování	opracování	k1gNnSc4
hliníkového	hliníkový	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
ve	v	k7c6
výrobě	výroba	k1gFnSc6
vagónů	vagón	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povrch	povrch	k1gInSc1
u	u	k7c2
hrubé	hrubý	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
tak	tak	k6eAd1
získá	získat	k5eAaPmIp3nS
zdrsněný	zdrsněný	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
tak	tak	k6eAd1
může	moct	k5eAaImIp3nS
lak	lak	k1gInSc4
vydržet	vydržet	k5eAaPmF
povětrnostní	povětrnostní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
pro	pro	k7c4
hořáky	hořák	k1gInPc4
sodíkových	sodíkový	k2eAgFnPc2d1
výbojek	výbojka	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
křemenného	křemenný	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
rtuťové	rtuťový	k2eAgFnPc4d1
<g/>
,	,	kIx,
xenonové	xenonový	k2eAgFnPc4d1
a	a	k8xC
halogenidové	halogenidový	k2eAgFnPc4d1
výbojky	výbojka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korund	korund	k1gInSc1
je	být	k5eAaImIp3nS
důležitým	důležitý	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
v	v	k7c6
průmyslu	průmysl	k1gInSc6
povrchových	povrchový	k2eAgFnPc2d1
úprav	úprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
druh	druh	k1gInSc1
abraziva	abraziv	k1gMnSc2
při	při	k7c6
tryskání	tryskání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Naleziště	naleziště	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
–	–	k?
Jizerská	jizerský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
u	u	k7c2
Kořenova	Kořenův	k2eAgMnSc2d1
(	(	kIx(
<g/>
safír	safír	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hazlov	Hazlov	k1gInSc1
<g/>
,	,	kIx,
Pokojovice	Pokojovice	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
–	–	k?
Vechec	Vechec	k1gMnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
–	–	k?
Mjas	Mjasa	k1gFnPc2
(	(	kIx(
<g/>
Ural	Ural	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Čajnit	Čajnit	k1gInSc1
(	(	kIx(
<g/>
Jakutsko	Jakutsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Srí	Srí	k?
Lanka	lanko	k1gNnPc1
</s>
<s>
Indie	Indie	k1gFnSc1
–	–	k?
Ásám	Ásám	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
minerálů	minerál	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Korund	korund	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
korund	korund	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Korund	korund	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
mindat	mindat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
org	org	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Korund	korund	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
Webmineral	Webmineral	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Korund	korund	k1gInSc1
v	v	k7c6
atlasu	atlas	k1gInSc6
minerálů	minerál	k1gInPc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4125010-2	4125010-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85033102	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85033102	#num#	k4
</s>
