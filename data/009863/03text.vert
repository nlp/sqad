<p>
<s>
Okres	okres	k1gInSc1	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
je	být	k5eAaImIp3nS	být
pohraniční	pohraniční	k2eAgInSc4d1	pohraniční
okres	okres	k1gInSc4	okres
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
dřívějším	dřívější	k2eAgNnSc7d1	dřívější
sídlem	sídlo	k1gNnSc7	sídlo
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc4	město
Břeclav	Břeclav	k1gFnSc1	Břeclav
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
63	[number]	k4	63
obcemi	obec	k1gFnPc7	obec
na	na	k7c6	na
69	[number]	k4	69
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
nejjižněji	jižně	k6eAd3	jižně
a	a	k8xC	a
nejníže	nízce	k6eAd3	nízce
položený	položený	k2eAgInSc4d1	položený
okres	okres	k1gInSc4	okres
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
Znojmo	Znojmo	k1gNnSc1	Znojmo
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
Vyškov	Vyškov	k1gInSc1	Vyškov
a	a	k8xC	a
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
okresem	okres	k1gInSc7	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jihu	jih	k1gInSc2	jih
je	být	k5eAaImIp3nS	být
okres	okres	k1gInSc1	okres
vymezen	vymezit	k5eAaPmNgInS	vymezit
státní	státní	k2eAgFnSc7d1	státní
hranicí	hranice	k1gFnSc7	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
z	z	k7c2	z
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
státní	státní	k2eAgFnSc7d1	státní
hranicí	hranice	k1gFnSc7	hranice
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
okresu	okres	k1gInSc2	okres
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
CHKO	CHKO	kA	CHKO
Pálava	Pálava	k1gFnSc1	Pálava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vinařství	vinařství	k1gNnSc2	vinařství
==	==	k?	==
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
celá	celý	k2eAgFnSc1d1	celá
část	část	k1gFnSc1	část
okresu	okres	k1gInSc2	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgMnSc7d1	významný
centrem	centr	k1gMnSc7	centr
vinařské	vinařský	k2eAgFnSc2d1	vinařská
turistiky	turistika	k1gFnSc2	turistika
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
okresu	okres	k1gInSc2	okres
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
především	především	k6eAd1	především
Mikulovská	mikulovský	k2eAgFnSc1d1	Mikulovská
vinařská	vinařský	k2eAgFnSc1d1	vinařská
podoblast	podoblast	k1gFnSc1	podoblast
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc7d1	jižní
částí	část	k1gFnSc7	část
Velkopavlovická	Velkopavlovický	k2eAgFnSc1d1	Velkopavlovická
vinařská	vinařský	k2eAgFnSc1d1	vinařská
podoblast	podoblast	k1gFnSc1	podoblast
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
okresního	okresní	k2eAgNnSc2d1	okresní
města	město	k1gNnSc2	město
Břeclav	Břeclav	k1gFnSc1	Břeclav
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
Slováckou	slovácký	k2eAgFnSc4d1	Slovácká
vinařskou	vinařský	k2eAgFnSc4d1	vinařská
podoblast	podoblast	k1gFnSc4	podoblast
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
vinařskými	vinařský	k2eAgMnPc7d1	vinařský
centry	centr	k1gMnPc7	centr
okresu	okres	k1gInSc2	okres
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
obce	obec	k1gFnPc1	obec
Mikulov	Mikulov	k1gInSc1	Mikulov
<g/>
,	,	kIx,	,
Valtice	Valtice	k1gFnPc1	Valtice
<g/>
,	,	kIx,	,
Hustopeče	Hustopeč	k1gFnPc1	Hustopeč
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnPc1d1	velká
Pavlovice	Pavlovice	k1gFnPc1	Pavlovice
<g/>
,	,	kIx,	,
Velké	velký	k2eAgInPc1d1	velký
Bílovice	Bílovice	k1gInPc1	Bílovice
<g/>
,	,	kIx,	,
Lednice	Lednice	k1gFnPc1	Lednice
<g/>
,	,	kIx,	,
Rakvice	Rakvice	k1gFnPc1	Rakvice
a	a	k8xC	a
Zaječí	zaječí	k2eAgFnPc1d1	zaječí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc4	území
okresu	okres	k1gInSc2	okres
okresu	okres	k1gInSc2	okres
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
správní	správní	k2eAgInPc4d1	správní
obvody	obvod	k1gInPc4	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
Hustopeče	Hustopeč	k1gFnPc1	Hustopeč
a	a	k8xC	a
Mikulov	Mikulov	k1gInSc1	Mikulov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Data	datum	k1gNnPc1	datum
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
povrchu	povrch	k1gInSc2	povrch
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2003	[number]	k4	2003
měl	mít	k5eAaImAgInS	mít
okres	okres	k1gInSc1	okres
celkovou	celkový	k2eAgFnSc4d1	celková
plochu	plocha	k1gFnSc4	plocha
1	[number]	k4	1
172,77	[number]	k4	172,77
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
68,32	[number]	k4	68,32
%	%	kIx~	%
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
z	z	k7c2	z
80,58	[number]	k4	80,58
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nS	tvořit
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
(	(	kIx(	(
<g/>
55,05	[number]	k4	55,05
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
okresu	okres	k1gInSc2	okres
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
31,68	[number]	k4	31,68
%	%	kIx~	%
ostatní	ostatní	k2eAgInPc1d1	ostatní
pozemky	pozemek	k1gInPc1	pozemek
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
51,58	[number]	k4	51,58
%	%	kIx~	%
lesy	les	k1gInPc1	les
(	(	kIx(	(
<g/>
16,34	[number]	k4	16,34
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
okresu	okres	k1gInSc2	okres
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Demografické	demografický	k2eAgInPc1d1	demografický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
obce	obec	k1gFnSc2	obec
<g/>
:	:	kIx,	:
1	[number]	k4	1
796	[number]	k4	796
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
žije	žít	k5eAaImIp3nS	žít
50,45	[number]	k4	50,45
%	%	kIx~	%
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
při	při	k7c6	při
vyloučení	vyloučení	k1gNnSc6	vyloučení
měst	město	k1gNnPc2	město
povýšených	povýšený	k2eAgMnPc2d1	povýšený
po	po	k7c6	po
r.	r.	kA	r.
1945	[number]	k4	1945
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
však	však	k9	však
jednalo	jednat	k5eAaImAgNnS	jednat
jen	jen	k9	jen
o	o	k7c4	o
39,08	[number]	k4	39,08
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Data	datum	k1gNnPc1	datum
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Největší	veliký	k2eAgNnPc4d3	veliký
sídla	sídlo	k1gNnPc4	sídlo
===	===	k?	===
</s>
</p>
<p>
<s>
Data	datum	k1gNnPc1	datum
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Zaměstnanost	zaměstnanost	k1gFnSc1	zaměstnanost
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Školství	školství	k1gNnSc2	školství
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Okresem	okres	k1gInSc7	okres
prochází	procházet	k5eAaImIp3nS	procházet
dálnice	dálnice	k1gFnSc1	dálnice
D2	D2	k1gFnSc1	D2
a	a	k8xC	a
silnice	silnice	k1gFnSc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
číslo	číslo	k1gNnSc1	číslo
I	i	k8xC	i
<g/>
/	/	kIx~	/
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
52	[number]	k4	52
a	a	k8xC	a
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnPc1	třída
jsou	být	k5eAaImIp3nP	být
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
380	[number]	k4	380
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
381	[number]	k4	381
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
414	[number]	k4	414
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
418	[number]	k4	418
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
420	[number]	k4	420
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
421	[number]	k4	421
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
422	[number]	k4	422
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
423	[number]	k4	423
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
424	[number]	k4	424
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
425	[number]	k4	425
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
==	==	k?	==
</s>
</p>
<p>
<s>
Města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
uvedena	uvést	k5eAaPmNgNnP	uvést
tučně	tučně	k6eAd1	tučně
<g/>
,	,	kIx,	,
městyse	městys	k1gInPc1	městys
kurzívou	kurzíva	k1gFnSc7	kurzíva
<g/>
,	,	kIx,	,
části	část	k1gFnSc3	část
obcí	obec	k1gFnPc2	obec
malince	malinko	k6eAd1	malinko
<g/>
.	.	kIx.	.
<g/>
Bavory	Bavory	k1gInPc1	Bavory
•	•	k?	•
</s>
</p>
<p>
<s>
Boleradice	Boleradice	k1gFnSc1	Boleradice
•	•	k?	•
</s>
</p>
<p>
<s>
Borkovany	Borkovany	k?	Borkovany
•	•	k?	•
</s>
</p>
<p>
<s>
Bořetice	Bořetika	k1gFnSc3	Bořetika
•	•	k?	•
</s>
</p>
<p>
<s>
Brod	Brod	k1gInSc1	Brod
nad	nad	k7c7	nad
Dyjí	Dyje	k1gFnSc7	Dyje
•	•	k?	•
</s>
</p>
<p>
<s>
Brumovice	Brumovice	k1gFnSc1	Brumovice
•	•	k?	•
</s>
</p>
<p>
<s>
Břeclav	Břeclav	k1gFnSc1	Břeclav
(	(	kIx(	(
<g/>
Charvátská	charvátský	k2eAgFnSc1d1	Charvátská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
•	•	k?	•
Poštorná	Poštorný	k2eAgFnSc1d1	Poštorná
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Březí	březí	k1gNnSc1	březí
•	•	k?	•
</s>
</p>
<p>
<s>
Bulhary	Bulhar	k1gMnPc7	Bulhar
•	•	k?	•
</s>
</p>
<p>
<s>
Diváky	divák	k1gMnPc4	divák
•	•	k?	•
</s>
</p>
<p>
<s>
Dobré	dobrý	k2eAgNnSc1d1	dobré
Pole	pole	k1gNnSc1	pole
•	•	k?	•
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
Dunajovice	Dunajovice	k1gFnSc1	Dunajovice
•	•	k?	•
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
•	•	k?	•
</s>
</p>
<p>
<s>
Drnholec	Drnholec	k1gInSc1	Drnholec
•	•	k?	•
</s>
</p>
<p>
<s>
Hlohovec	Hlohovec	k1gInSc1	Hlohovec
•	•	k?	•
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
Bojanovice	Bojanovice	k1gFnSc1	Bojanovice
•	•	k?	•
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnPc1d1	horní
Věstonice	Věstonice	k1gFnPc1	Věstonice
•	•	k?	•
</s>
</p>
<p>
<s>
Hrušky	hruška	k1gFnPc1	hruška
•	•	k?	•
</s>
</p>
<p>
<s>
Hustopeče	Hustopeč	k1gFnPc1	Hustopeč
•	•	k?	•
</s>
</p>
<p>
<s>
Jevišovka	Jevišovka	k1gFnSc1	Jevišovka
•	•	k?	•
</s>
</p>
<p>
<s>
Kašnice	Kašnice	k1gFnSc1	Kašnice
•	•	k?	•
</s>
</p>
<p>
<s>
Klentnice	Klentnice	k1gFnSc1	Klentnice
•	•	k?	•
</s>
</p>
<p>
<s>
Klobouky	Klobouky	k1gInPc1	Klobouky
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
Bohumilice	Bohumilice	k1gFnSc1	Bohumilice
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Kobylí	kobylí	k2eAgInSc1d1	kobylí
•	•	k?	•
</s>
</p>
<p>
<s>
Kostice	kostice	k1gFnSc1	kostice
•	•	k?	•
</s>
</p>
<p>
<s>
Krumvíř	Krumvíř	k1gFnSc1	Krumvíř
•	•	k?	•
</s>
</p>
<p>
<s>
Křepice	Křepice	k1gFnSc1	Křepice
•	•	k?	•
</s>
</p>
<p>
<s>
Kurdějov	Kurdějov	k1gInSc1	Kurdějov
•	•	k?	•
</s>
</p>
<p>
<s>
Ladná	ladný	k2eAgFnSc1d1	ladná
•	•	k?	•
</s>
</p>
<p>
<s>
Lanžhot	Lanžhot	k1gInSc1	Lanžhot
•	•	k?	•
</s>
</p>
<p>
<s>
Lednice	Lednice	k1gFnSc1	Lednice
(	(	kIx(	(
<g/>
Nejdek	Nejdek	k1gInSc1	Nejdek
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Mikulov	Mikulov	k1gInSc1	Mikulov
•	•	k?	•
</s>
</p>
<p>
<s>
Milovice	Milovice	k1gFnPc1	Milovice
•	•	k?	•
</s>
</p>
<p>
<s>
Moravská	moravský	k2eAgFnSc1d1	Moravská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
•	•	k?	•
</s>
</p>
<p>
<s>
Moravský	moravský	k2eAgInSc1d1	moravský
Žižkov	Žižkov	k1gInSc1	Žižkov
•	•	k?	•
</s>
</p>
<p>
<s>
Morkůvky	Morkůvka	k1gFnPc1	Morkůvka
•	•	k?	•
</s>
</p>
<p>
<s>
Němčičky	Němčička	k1gFnPc1	Němčička
•	•	k?	•
</s>
</p>
<p>
<s>
Nikolčice	Nikolčice	k1gFnSc1	Nikolčice
•	•	k?	•
</s>
</p>
<p>
<s>
Novosedly	Novosednout	k5eAaPmAgFnP	Novosednout
•	•	k?	•
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
Přerov	Přerov	k1gInSc1	Přerov
•	•	k?	•
</s>
</p>
<p>
<s>
Pavlov	Pavlov	k1gInSc1	Pavlov
•	•	k?	•
</s>
</p>
<p>
<s>
Perná	perný	k2eAgFnSc1d1	Perná
•	•	k?	•
</s>
</p>
<p>
<s>
Podivín	podivín	k1gMnSc1	podivín
•	•	k?	•
</s>
</p>
<p>
<s>
Popice	Popice	k1gFnSc1	Popice
•	•	k?	•
</s>
</p>
<p>
<s>
Pouzdřany	Pouzdřan	k1gMnPc4	Pouzdřan
•	•	k?	•
</s>
</p>
<p>
<s>
Přítluky	Přítluk	k1gInPc1	Přítluk
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgInPc1d1	Nové
Mlýny	mlýn	k1gInPc1	mlýn
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Rakvice	rakvice	k1gFnSc1	rakvice
•	•	k?	•
</s>
</p>
<p>
<s>
Sedlec	Sedlec	k1gInSc1	Sedlec
•	•	k?	•
</s>
</p>
<p>
<s>
Starovice	Starovice	k1gFnSc1	Starovice
•	•	k?	•
</s>
</p>
<p>
<s>
Starovičky	Starovička	k1gFnPc1	Starovička
•	•	k?	•
</s>
</p>
<p>
<s>
Strachotín	Strachotín	k1gInSc1	Strachotín
•	•	k?	•
</s>
</p>
<p>
<s>
Šakvice	Šakvice	k1gFnSc1	Šakvice
•	•	k?	•
</s>
</p>
<p>
<s>
Šitbořice	Šitbořice	k1gFnSc1	Šitbořice
•	•	k?	•
</s>
</p>
<p>
<s>
Tvrdonice	Tvrdonice	k1gFnSc1	Tvrdonice
•	•	k?	•
</s>
</p>
<p>
<s>
Týnec	Týnec	k1gInSc1	Týnec
•	•	k?	•
</s>
</p>
<p>
<s>
Uherčice	Uherčice	k1gFnSc1	Uherčice
•	•	k?	•
</s>
</p>
<p>
<s>
Valtice	Valtice	k1gFnPc1	Valtice
(	(	kIx(	(
<g/>
Úvaly	úval	k1gInPc1	úval
<g/>
)	)	kIx)	)
•	•	k?	•
</s>
</p>
<p>
<s>
Velké	velký	k2eAgInPc1d1	velký
Bílovice	Bílovice	k1gInPc1	Bílovice
•	•	k?	•
</s>
</p>
<p>
<s>
Velké	velký	k2eAgInPc1d1	velký
Hostěrádky	Hostěrádek	k1gInPc1	Hostěrádek
•	•	k?	•
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
Němčice	Němčice	k1gFnPc1	Němčice
•	•	k?	•
</s>
</p>
<p>
<s>
Velké	velký	k2eAgFnPc1d1	velká
Pavlovice	Pavlovice	k1gFnPc1	Pavlovice
•	•	k?	•
</s>
</p>
<p>
<s>
Vrbice	vrbice	k1gFnSc1	vrbice
•	•	k?	•
</s>
</p>
<p>
<s>
Zaječí	zaječet	k5eAaPmIp3nS	zaječet
</s>
</p>
<p>
<s>
===	===	k?	===
Změna	změna	k1gFnSc1	změna
hranice	hranice	k1gFnSc2	hranice
okresu	okres	k1gInSc2	okres
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Břeclav	Břeclav	k1gFnSc4	Břeclav
také	také	k6eAd1	také
obce	obec	k1gFnSc2	obec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Cvrčovice	Cvrčovice	k1gFnSc1	Cvrčovice
–	–	k?	–
poté	poté	k6eAd1	poté
okres	okres	k1gInSc1	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
</s>
</p>
<p>
<s>
Ivaň	Ivaň	k1gFnSc1	Ivaň
–	–	k?	–
poté	poté	k6eAd1	poté
okres	okres	k1gInSc1	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
</s>
</p>
<p>
<s>
Pasohlávky	Pasohlávka	k1gFnPc1	Pasohlávka
–	–	k?	–
poté	poté	k6eAd1	poté
okres	okres	k1gInSc1	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
</s>
</p>
<p>
<s>
Pohořelice	Pohořelice	k1gFnPc1	Pohořelice
–	–	k?	–
poté	poté	k6eAd1	poté
okres	okres	k1gInSc1	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
</s>
</p>
<p>
<s>
Přibice	Přibice	k1gFnSc1	Přibice
–	–	k?	–
poté	poté	k6eAd1	poté
okres	okres	k1gInSc1	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
</s>
</p>
<p>
<s>
Vlasatice	vlasatice	k1gFnSc1	vlasatice
–	–	k?	–
poté	poté	k6eAd1	poté
okres	okres	k1gInSc1	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
</s>
</p>
<p>
<s>
Vranovice	Vranovice	k1gFnSc1	Vranovice
–	–	k?	–
poté	poté	k6eAd1	poté
okres	okres	k1gInSc1	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Města	město	k1gNnPc1	město
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
tučně	tučně	k6eAd1	tučně
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2017	[number]	k4	2017
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lednicko-valtický	lednickoaltický	k2eAgInSc1d1	lednicko-valtický
areál	areál	k1gInSc1	areál
</s>
</p>
<p>
<s>
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Pálava	Pálava	k1gFnSc1	Pálava
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
</s>
</p>
<p>
<s>
Senátní	senátní	k2eAgInSc1d1	senátní
obvod	obvod	k1gInSc1	obvod
č.	č.	k?	č.
56	[number]	k4	56
-	-	kIx~	-
Břeclav	Břeclav	k1gFnSc1	Břeclav
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
okres	okres	k1gInSc4	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
správního	správní	k2eAgInSc2d1	správní
vývoje	vývoj	k1gInSc2	vývoj
obcí	obec	k1gFnPc2	obec
okresu	okres	k1gInSc3	okres
Břeclav	Břeclav	k1gFnSc4	Břeclav
</s>
</p>
<p>
<s>
Okres	okres	k1gInSc1	okres
Břeclav	Břeclav	k1gFnSc4	Břeclav
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
správa	správa	k1gFnSc1	správa
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc1d1	kulturní
zajímavosti	zajímavost	k1gFnPc1	zajímavost
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
</s>
</p>
