<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1901	[number]	k4	1901
Žižkov	Žižkov	k1gInSc1	Žižkov
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1986	[number]	k4	1986
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
hnutí	hnutí	k1gNnSc2	hnutí
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
českého	český	k2eAgInSc2d1	český
uměleckého	umělecký	k2eAgInSc2d1	umělecký
směru	směr	k1gInSc2	směr
poetismu	poetismus	k1gInSc2	poetismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
českým	český	k2eAgMnSc7d1	český
nositelem	nositel	k1gMnSc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
komplikovaným	komplikovaný	k2eAgInPc3d1	komplikovaný
vztahům	vztah	k1gInPc3	vztah
s	s	k7c7	s
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
mocí	moc	k1gFnSc7	moc
obdržel	obdržet	k5eAaPmAgInS	obdržet
titul	titul	k1gInSc1	titul
národního	národní	k2eAgMnSc2d1	národní
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
prvním	první	k4xOgMnPc3	první
signatářům	signatář	k1gMnPc3	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
chudých	chudý	k2eAgInPc2d1	chudý
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Bořivojově	Bořivojův	k2eAgFnSc6d1	Bořivojova
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Riegrově	Riegrův	k2eAgFnSc6d1	Riegrova
<g/>
)	)	kIx)	)
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Žižkově	Žižkov	k1gInSc6	Žižkov
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
816	[number]	k4	816
<g/>
/	/	kIx~	/
<g/>
104	[number]	k4	104
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtít	k5eAaPmNgInS	pokřtít
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
jako	jako	k8xC	jako
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nepříliš	příliš	k6eNd1	příliš
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
obrazy	obraz	k1gInPc7	obraz
a	a	k8xC	a
rámy	rám	k1gInPc7	rám
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dělník	dělník	k1gMnSc1	dělník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
uvědomělým	uvědomělý	k2eAgMnSc7d1	uvědomělý
socialistou	socialista	k1gMnSc7	socialista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
zbožná	zbožný	k2eAgFnSc1d1	zbožná
katolička	katolička	k1gFnSc1	katolička
<g/>
.	.	kIx.	.
</s>
<s>
Seifert	Seifert	k1gMnSc1	Seifert
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
později	pozdě	k6eAd2	pozdě
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tyto	tento	k3xDgInPc1	tento
protiklady	protiklad	k1gInPc1	protiklad
mi	já	k3xPp1nSc3	já
také	také	k9	také
trochu	trochu	k6eAd1	trochu
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
-	-	kIx~	-
v	v	k7c6	v
životě	život	k1gInSc6	život
i	i	k8xC	i
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
několikrát	několikrát	k6eAd1	několikrát
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
po	po	k7c6	po
různých	různý	k2eAgInPc6d1	různý
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
nuzných	nuzný	k2eAgFnPc2d1	nuzná
<g/>
,	,	kIx,	,
podnájmech	podnájem	k1gInPc6	podnájem
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Žižkova	Žižkov	k1gInSc2	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
středoškolská	středoškolský	k2eAgNnPc4d1	středoškolské
studia	studio	k1gNnPc4	studio
si	se	k3xPyFc3	se
Seifert	Seifert	k1gMnSc1	Seifert
odbýval	odbývat	k5eAaImAgMnS	odbývat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
c.	c.	k?	c.
k.	k.	k?	k.
vyšším	vysoký	k2eAgNnSc6d2	vyšší
gymnáziu	gymnázium	k1gNnSc6	gymnázium
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
privátní	privátní	k2eAgMnSc1d1	privátní
žák	žák	k1gMnSc1	žák
gymnázia	gymnázium	k1gNnSc2	gymnázium
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
bystrým	bystrý	k2eAgMnPc3d1	bystrý
žákům	žák	k1gMnPc3	žák
<g/>
,	,	kIx,	,
svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
neomluvených	omluvený	k2eNgFnPc2d1	neomluvená
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
trávil	trávit	k5eAaImAgInS	trávit
vesměs	vesměs	k6eAd1	vesměs
touláním	toulání	k1gNnSc7	toulání
se	se	k3xPyFc4	se
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
účastí	účast	k1gFnSc7	účast
na	na	k7c6	na
dělnických	dělnický	k2eAgFnPc6d1	Dělnická
demonstracích	demonstrace	k1gFnPc6	demonstrace
a	a	k8xC	a
horlivou	horlivý	k2eAgFnSc7d1	horlivá
četbou	četba	k1gFnSc7	četba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
mu	on	k3xPp3gMnSc3	on
začínaly	začínat	k5eAaImAgFnP	začínat
vycházet	vycházet	k5eAaImF	vycházet
první	první	k4xOgFnPc1	první
básně	báseň	k1gFnPc1	báseň
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
časopisech	časopis	k1gInPc6	časopis
a	a	k8xC	a
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Josefa	Josef	k1gMnSc2	Josef
Hory	hora	k1gFnSc2	hora
v	v	k7c6	v
Právu	právo	k1gNnSc6	právo
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
v	v	k7c6	v
slzách	slza	k1gFnPc6	slza
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
Seifert	Seifert	k1gMnSc1	Seifert
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
právě	právě	k6eAd1	právě
založené	založený	k2eAgFnSc2d1	založená
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
pravidelným	pravidelný	k2eAgMnSc7d1	pravidelný
přispěvatelem	přispěvatel	k1gMnSc7	přispěvatel
jejího	její	k3xOp3gInSc2	její
nově	nově	k6eAd1	nově
založeného	založený	k2eAgInSc2d1	založený
listu	list	k1gInSc2	list
Rudé	rudý	k2eAgNnSc1d1	Rudé
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
nebo	nebo	k8xC	nebo
spoluredaktor	spoluredaktor	k1gMnSc1	spoluredaktor
různých	různý	k2eAgInPc2d1	různý
uměleckých	umělecký	k2eAgInPc2d1	umělecký
a	a	k8xC	a
literárních	literární	k2eAgInPc2d1	literární
časopisů	časopis	k1gInPc2	časopis
(	(	kIx(	(
<g/>
Sršatec	sršatec	k1gMnSc1	sršatec
<g/>
,	,	kIx,	,
Reflektor	reflektor	k1gInSc1	reflektor
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1927	[number]	k4	1927
byl	být	k5eAaImAgMnS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
v	v	k7c6	v
Komunistickém	komunistický	k2eAgNnSc6d1	komunistické
knihkupectví	knihkupectví	k1gNnSc6	knihkupectví
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1929	[number]	k4	1929
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
šesti	šest	k4xCc7	šest
dalšími	další	k2eAgFnPc7d1	další
předními	přední	k2eAgFnPc7d1	přední
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
spisovateli	spisovatel	k1gMnSc6	spisovatel
<g/>
,	,	kIx,	,
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
z	z	k7c2	z
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Manifest	manifest	k1gInSc4	manifest
sedmi	sedm	k4xCc2	sedm
protestující	protestující	k2eAgInPc4d1	protestující
proti	proti	k7c3	proti
bolševizaci	bolševizace	k1gFnSc3	bolševizace
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
<g/>
,	,	kIx,	,
gottwaldovském	gottwaldovský	k2eAgNnSc6d1	gottwaldovské
vedení	vedení	k1gNnSc6	vedení
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Československé	československý	k2eAgFnSc2d1	Československá
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
pracovně	pracovně	k6eAd1	pracovně
zakotvil	zakotvit	k5eAaPmAgInS	zakotvit
v	v	k7c6	v
sociálnědemokratickém	sociálnědemokratický	k2eAgInSc6d1	sociálnědemokratický
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
práci	práce	k1gFnSc6	práce
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
odborářských	odborářský	k2eAgFnPc6d1	odborářská
novinách	novina	k1gFnPc6	novina
Práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
výčet	výčet	k1gInSc1	výčet
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
Seifert	Seifert	k1gMnSc1	Seifert
redakčně	redakčně	k6eAd1	redakčně
působil	působit	k5eAaImAgMnS	působit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
obsáhlý	obsáhlý	k2eAgInSc1d1	obsáhlý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
Seifert	Seifert	k1gMnSc1	Seifert
žurnalismu	žurnalismus	k1gInSc2	žurnalismus
zanechal	zanechat	k5eAaPmAgMnS	zanechat
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
výhradně	výhradně	k6eAd1	výhradně
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
Seifert	Seifert	k1gMnSc1	Seifert
již	již	k6eAd1	již
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
představitele	představitel	k1gMnSc4	představitel
československé	československý	k2eAgFnSc2d1	Československá
umělecké	umělecký	k2eAgFnSc2d1	umělecká
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
činnosti	činnost	k1gFnSc6	činnost
skupiny	skupina	k1gFnSc2	skupina
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Teigem	Teig	k1gMnSc7	Teig
redigoval	redigovat	k5eAaImAgInS	redigovat
Revoluční	revoluční	k2eAgInSc1d1	revoluční
sborník	sborník	k1gInSc1	sborník
Devětsilu	Devětsil	k1gInSc2	Devětsil
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
a	a	k8xC	a
promýšlel	promýšlet	k5eAaImAgMnS	promýšlet
formulace	formulace	k1gFnPc4	formulace
avantgardních	avantgardní	k2eAgMnPc2d1	avantgardní
manifestů	manifest	k1gInPc2	manifest
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
manifestu	manifest	k1gInSc2	manifest
poetismu	poetismus	k1gInSc2	poetismus
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
redakci	redakce	k1gFnSc6	redakce
avantgardních	avantgardní	k2eAgInPc2d1	avantgardní
časopisů	časopis	k1gInPc2	časopis
Disk	disco	k1gNnPc2	disco
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pásmo	pásmo	k1gNnSc4	pásmo
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
i	i	k9	i
brněnského	brněnský	k2eAgInSc2d1	brněnský
orgánu	orgán	k1gInSc2	orgán
Literární	literární	k2eAgFnSc2d1	literární
skupiny	skupina	k1gFnSc2	skupina
Host	host	k1gMnSc1	host
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
vedl	vést	k5eAaImAgInS	vést
literární	literární	k2eAgInSc1d1	literární
časopis	časopis	k1gInSc1	časopis
Kytice	kytice	k1gFnSc2	kytice
a	a	k8xC	a
redigoval	redigovat	k5eAaImAgInS	redigovat
básnickou	básnický	k2eAgFnSc4d1	básnická
edici	edice	k1gFnSc4	edice
Klín	klín	k1gInSc1	klín
(	(	kIx(	(
<g/>
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
klubu	klub	k1gInSc2	klub
Devětsilu	Devětsil	k1gInSc2	Devětsil
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
rovněž	rovněž	k9	rovněž
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
a	a	k8xC	a
následujících	následující	k2eAgInPc2d1	následující
avantgardních	avantgardní	k2eAgInPc2d1	avantgardní
experimentů	experiment	k1gInPc2	experiment
(	(	kIx(	(
<g/>
surrealismus	surrealismus	k1gInSc1	surrealismus
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
již	již	k6eAd1	již
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
pak	pak	k6eAd1	pak
našel	najít	k5eAaPmAgMnS	najít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
osobitý	osobitý	k2eAgInSc4d1	osobitý
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
poměrně	poměrně	k6eAd1	poměrně
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
okolním	okolní	k2eAgNnSc6d1	okolní
literárním	literární	k2eAgNnSc6d1	literární
dění	dění	k1gNnSc6	dění
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1928	[number]	k4	1928
se	se	k3xPyFc4	se
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
oženil	oženit	k5eAaPmAgMnS	oženit
na	na	k7c6	na
magistrátní	magistrátní	k2eAgFnSc6d1	magistrátní
úřadovně	úřadovna	k1gFnSc6	úřadovna
Praha-Žižkov	Praha-Žižkov	k1gInSc1	Praha-Žižkov
(	(	kIx(	(
<g/>
občanský	občanský	k2eAgInSc1d1	občanský
sňatek	sňatek	k1gInSc1	sňatek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Marie	Marie	k1gFnSc1	Marie
Ulrichová	Ulrichová	k1gFnSc1	Ulrichová
<g/>
,	,	kIx,	,
úřednice	úřednice	k1gFnSc1	úřednice
Zemské	zemský	k2eAgFnSc2d1	zemská
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
jičínského	jičínský	k2eAgMnSc2d1	jičínský
pekaře	pekař	k1gMnSc2	pekař
Antonína	Antonín	k1gMnSc2	Antonín
Ulricha	Ulrich	k1gMnSc2	Ulrich
a	a	k8xC	a
Karoliny	Karolinum	k1gNnPc7	Karolinum
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnPc4d1	rozená
Gaberové	Gaberová	k1gFnPc4	Gaberová
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
měli	mít	k5eAaImAgMnP	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Janu	Jana	k1gFnSc4	Jana
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
Seifert	Seifert	k1gMnSc1	Seifert
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
výhradně	výhradně	k6eAd1	výhradně
psaní	psaní	k1gNnSc2	psaní
<g/>
,	,	kIx,	,
těžká	těžký	k2eAgFnSc1d1	těžká
choroba	choroba	k1gFnSc1	choroba
pohybového	pohybový	k2eAgNnSc2d1	pohybové
ústrojí	ústrojí	k1gNnSc2	ústrojí
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
v	v	k7c4	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
soustavnou	soustavný	k2eAgFnSc4d1	soustavná
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
práci	práce	k1gFnSc4	práce
znemožňovala	znemožňovat	k5eAaImAgFnS	znemožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
autoritu	autorita	k1gFnSc4	autorita
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
svým	svůj	k3xOyFgNnSc7	svůj
vystoupením	vystoupení	k1gNnSc7	vystoupení
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
Sjezdu	sjezd	k1gInSc6	sjezd
československých	československý	k2eAgMnPc2d1	československý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
politiku	politika	k1gFnSc4	politika
režimu	režim	k1gInSc2	režim
vůči	vůči	k7c3	vůči
nepohodlným	pohodlný	k2eNgMnPc3d1	nepohodlný
autorům	autor	k1gMnPc3	autor
(	(	kIx(	(
<g/>
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
ostatně	ostatně	k6eAd1	ostatně
sám	sám	k3xTgMnSc1	sám
patřil	patřit	k5eAaImAgMnS	patřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uvolnění	uvolnění	k1gNnSc6	uvolnění
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc3	jeho
druhdy	druhdy	k6eAd1	druhdy
odmítaná	odmítaný	k2eAgFnSc1d1	odmítaná
tvorba	tvorba	k1gFnSc1	tvorba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
padesátých	padesátý	k4xOgNnPc2	padesátý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
Viktorce	Viktorka	k1gFnSc6	Viktorka
<g/>
)	)	kIx)	)
dočkala	dočkat	k5eAaPmAgFnS	dočkat
reedic	reedice	k1gFnPc2	reedice
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
národním	národní	k2eAgMnSc7d1	národní
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
nově	nově	k6eAd1	nově
ustaveného	ustavený	k2eAgInSc2d1	ustavený
Svazu	svaz	k1gInSc2	svaz
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
fungoval	fungovat	k5eAaImAgInS	fungovat
pouhý	pouhý	k2eAgInSc4d1	pouhý
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
po	po	k7c6	po
protestním	protestní	k2eAgNnSc6d1	protestní
sebeupálení	sebeupálení	k1gNnSc6	sebeupálení
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
,	,	kIx,	,
veřejně	veřejně	k6eAd1	veřejně
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
aby	aby	k9	aby
Palachův	Palachův	k2eAgInSc4d1	Palachův
čin	čin	k1gInSc4	čin
nenásledovali	následovat	k5eNaImAgMnP	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
občanské	občanský	k2eAgInPc4d1	občanský
postoje	postoj	k1gInPc4	postoj
i	i	k8xC	i
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
autoritu	autorita	k1gFnSc4	autorita
patřil	patřit	k5eAaImAgMnS	patřit
Seifert	Seifert	k1gMnSc1	Seifert
mezi	mezi	k7c7	mezi
autory	autor	k1gMnPc7	autor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
tzv.	tzv.	kA	tzv.
normalizace	normalizace	k1gFnSc2	normalizace
upadli	upadnout	k5eAaPmAgMnP	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
nového	nový	k2eAgInSc2d1	nový
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
stáhnout	stáhnout	k5eAaPmF	stáhnout
se	se	k3xPyFc4	se
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
mohly	moct	k5eAaImAgFnP	moct
vycházet	vycházet	k5eAaImF	vycházet
pouze	pouze	k6eAd1	pouze
ojedinělé	ojedinělý	k2eAgFnPc4d1	ojedinělá
reedice	reedice	k1gFnPc4	reedice
<g/>
,	,	kIx,	,
novou	nový	k2eAgFnSc4d1	nová
tvorbu	tvorba	k1gFnSc4	tvorba
směl	smět	k5eAaImAgMnS	smět
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
s	s	k7c7	s
výhradami	výhrada	k1gFnPc7	výhrada
až	až	k8xS	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
režim	režim	k1gInSc1	režim
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
popularitě	popularita	k1gFnSc3	popularita
hledal	hledat	k5eAaImAgMnS	hledat
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
s	s	k7c7	s
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
básníkem	básník	k1gMnSc7	básník
naložit	naložit	k5eAaPmF	naložit
<g/>
.	.	kIx.	.
</s>
<s>
Dohody	dohoda	k1gFnPc1	dohoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
vydávání	vydávání	k1gNnSc4	vydávání
knih	kniha	k1gFnPc2	kniha
nebude	být	k5eNaImBp3nS	být
veřejně	veřejně	k6eAd1	veřejně
vystupovat	vystupovat	k5eAaImF	vystupovat
a	a	k8xC	a
podepisovat	podepisovat	k5eAaImF	podepisovat
žádné	žádný	k3yNgFnPc4	žádný
petice	petice	k1gFnPc4	petice
<g/>
,	,	kIx,	,
Seifert	Seifert	k1gMnSc1	Seifert
opakovaně	opakovaně	k6eAd1	opakovaně
nedodržoval	dodržovat	k5eNaImAgMnS	dodržovat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1976	[number]	k4	1976
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
první	první	k4xOgMnPc4	první
signatáře	signatář	k1gMnPc4	signatář
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
pravidelně	pravidelně	k6eAd1	pravidelně
vycházela	vycházet	k5eAaImAgNnP	vycházet
v	v	k7c6	v
samizdatu	samizdat	k1gInSc6	samizdat
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Gruntorád	Gruntoráda	k1gFnPc2	Gruntoráda
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
samizdatové	samizdatový	k2eAgNnSc4d1	samizdatové
šíření	šíření	k1gNnSc4	šíření
Seifertových	Seifertová	k1gFnPc2	Seifertová
básní	básnit	k5eAaImIp3nS	básnit
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
třem	tři	k4xCgNnPc3	tři
letům	let	k1gInPc3	let
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Seifert	Seifert	k1gMnSc1	Seifert
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	s	k7c7	s
slovenským	slovenský	k2eAgMnSc7d1	slovenský
hercem	herec	k1gMnSc7	herec
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Chudíkem	Chudík	k1gMnSc7	Chudík
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
korespondence	korespondence	k1gFnSc1	korespondence
(	(	kIx(	(
<g/>
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Tichý	tichý	k2eAgInSc4d1	tichý
dvojhlas	dvojhlas	k1gInSc4	dvojhlas
<g/>
)	)	kIx)	)
začala	začít	k5eAaPmAgFnS	začít
dopisem	dopis	k1gInSc7	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
Ladislav	Ladislav	k1gMnSc1	Ladislav
Chudík	Chudík	k1gMnSc1	Chudík
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
Seifertovu	Seifertův	k2eAgFnSc4d1	Seifertova
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
výborně	výborně	k6eAd1	výborně
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
předčítal	předčítat	k5eAaImAgMnS	předčítat
Ladislav	Ladislav	k1gMnSc1	Ladislav
Chudík	Chudík	k1gMnSc1	Chudík
každoročně	každoročně	k6eAd1	každoročně
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
Seifertovy	Seifertův	k2eAgFnSc2d1	Seifertova
básně	báseň	k1gFnSc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
však	však	k9	však
za	za	k7c2	za
něj	on	k3xPp3gInSc2	on
přebírala	přebírat	k5eAaImAgFnS	přebírat
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
špatnému	špatný	k2eAgInSc3d1	špatný
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
významná	významný	k2eAgFnSc1d1	významná
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sdělovacích	sdělovací	k2eAgInPc6d1	sdělovací
prostředcích	prostředek	k1gInPc6	prostředek
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
režimem	režim	k1gInSc7	režim
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
padla	padnout	k5eAaImAgFnS	padnout
jenom	jenom	k6eAd1	jenom
suchá	suchý	k2eAgFnSc1d1	suchá
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
.	.	kIx.	.
</s>
<s>
Jiřina	Jiřina	k1gFnSc1	Jiřina
Šiklová	Šiklová	k1gFnSc1	Šiklová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
zaslala	zaslat	k5eAaPmAgNnP	zaslat
podklady	podklad	k1gInPc4	podklad
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
o	o	k7c6	o
Nobelově	Nobelův	k2eAgFnSc6d1	Nobelova
ceně	cena	k1gFnSc6	cena
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
rukopis	rukopis	k1gInSc4	rukopis
jeho	jeho	k3xOp3gFnSc2	jeho
pamětí	paměť	k1gFnSc7	paměť
Všecky	všecek	k3xTgFnPc4	všecek
krásy	krása	k1gFnPc4	krása
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
soudně	soudně	k6eAd1	soudně
stíhána	stíhat	k5eAaImNgFnS	stíhat
<g/>
.	.	kIx.	.
</s>
<s>
Fakt	fakt	k1gInSc1	fakt
udělení	udělení	k1gNnSc2	udělení
nejvýznamnějšího	významný	k2eAgNnSc2d3	nejvýznamnější
literárního	literární	k2eAgNnSc2d1	literární
ocenění	ocenění	k1gNnSc2	ocenění
však	však	k9	však
režim	režim	k1gInSc4	režim
musel	muset	k5eAaImAgMnS	muset
nakonec	nakonec	k6eAd1	nakonec
uznat	uznat	k5eAaPmF	uznat
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
umlčování	umlčování	k1gNnSc2	umlčování
podnikl	podniknout	k5eAaPmAgMnS	podniknout
posléze	posléze	k6eAd1	posléze
pokusy	pokus	k1gInPc4	pokus
přivlastnit	přivlastnit	k5eAaPmF	přivlastnit
si	se	k3xPyFc3	se
básníka	básník	k1gMnSc4	básník
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
propagandistické	propagandistický	k2eAgInPc4d1	propagandistický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
ledna	leden	k1gInSc2	leden
1986	[number]	k4	1986
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Strahově	Strahov	k1gInSc6	Strahov
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
pohřeb	pohřeb	k1gInSc1	pohřeb
v	v	k7c6	v
Rudolfinu	Rudolfinum	k1gNnSc6	Rudolfinum
hrozil	hrozit	k5eAaImAgInS	hrozit
přerůst	přerůst	k5eAaPmF	přerůst
v	v	k7c4	v
protikomunistickou	protikomunistický	k2eAgFnSc4d1	protikomunistická
manifestaci	manifestace	k1gFnSc4	manifestace
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
z	z	k7c2	z
příprav	příprava	k1gFnPc2	příprava
pohřbu	pohřeb	k1gInSc2	pohřeb
vyloučilo	vyloučit	k5eAaPmAgNnS	vyloučit
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgNnSc1d1	církevní
rozloučení	rozloučení	k1gNnSc1	rozloučení
konané	konaný	k2eAgNnSc1d1	konané
v	v	k7c6	v
břevnovském	břevnovský	k2eAgInSc6d1	břevnovský
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místo	k1gNnSc7	místo
posledního	poslední	k2eAgNnSc2d1	poslední
spočinutí	spočinutí	k1gNnSc2	spočinutí
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Seiferta	Seifert	k1gMnSc2	Seifert
jsou	být	k5eAaImIp3nP	být
Kralupy	Kralupy	k1gInPc1	Kralupy
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pocházeli	pocházet	k5eAaImAgMnP	pocházet
jeho	jeho	k3xOp3gMnPc4	jeho
prarodiče	prarodič	k1gMnPc4	prarodič
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
domě	dům	k1gInSc6	dům
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
U	u	k7c2	u
Ladronky	Ladronka	k1gFnSc2	Ladronka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
na	na	k7c6	na
Břevnově	Břevnov	k1gInSc6	Břevnov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
od	od	k7c2	od
června	červen	k1gInSc2	červen
1938	[number]	k4	1938
žil	žíla	k1gFnPc2	žíla
<g/>
,	,	kIx,	,
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
v	v	k7c6	v
slzách	slza	k1gFnPc6	slza
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
-	-	kIx~	-
V	v	k7c6	v
Seifertově	Seifertův	k2eAgFnSc6d1	Seifertova
prvotině	prvotina	k1gFnSc6	prvotina
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
proletářskou	proletářský	k2eAgFnSc7d1	proletářská
poezií	poezie	k1gFnSc7	poezie
a	a	k8xC	a
"	"	kIx"	"
<g/>
wolkerovským	wolkerovský	k2eAgInSc7d1	wolkerovský
<g/>
"	"	kIx"	"
naivismem	naivismus	k1gInSc7	naivismus
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
představy	představa	k1gFnPc1	představa
světa	svět	k1gInSc2	svět
bez	bez	k7c2	bez
bídy	bída	k1gFnSc2	bída
a	a	k8xC	a
nenávisti	nenávist	k1gFnSc2	nenávist
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
člověka	člověk	k1gMnSc2	člověk
získat	získat	k5eAaPmF	získat
šťastnější	šťastný	k2eAgInPc4d2	šťastnější
a	a	k8xC	a
lepší	dobrý	k2eAgInSc4d2	lepší
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
Seifert	Seifert	k1gMnSc1	Seifert
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kůži	kůže	k1gFnSc4	kůže
nepoznal	poznat	k5eNaPmAgInS	poznat
válku	válka	k1gFnSc4	válka
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
chudiny	chudina	k1gFnSc2	chudina
přesto	přesto	k8xC	přesto
dobře	dobře	k6eAd1	dobře
znal	znát	k5eAaImAgMnS	znát
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
"	"	kIx"	"
<g/>
městem	město	k1gNnSc7	město
v	v	k7c6	v
slzách	slza	k1gFnPc6	slza
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
především	především	k9	především
její	její	k3xOp3gNnPc1	její
chudá	chudý	k2eAgNnPc1d1	chudé
předměstí	předměstí	k1gNnPc1	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Verše	verš	k1gInPc1	verš
této	tento	k3xDgFnSc2	tento
sbírky	sbírka	k1gFnSc2	sbírka
jsou	být	k5eAaImIp3nP	být
záměrně	záměrně	k6eAd1	záměrně
prosté	prostý	k2eAgMnPc4d1	prostý
až	až	k9	až
primitivní	primitivní	k2eAgNnSc1d1	primitivní
<g/>
,	,	kIx,	,
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
spádu	spád	k1gInSc2	spád
hovorové	hovorový	k2eAgFnSc2d1	hovorová
řeči	řeč	k1gFnSc2	řeč
<g/>
;	;	kIx,	;
představují	představovat	k5eAaImIp3nP	představovat
jakousi	jakýsi	k3yIgFnSc4	jakýsi
literární	literární	k2eAgFnSc4d1	literární
obdobu	obdoba	k1gFnSc4	obdoba
naivního	naivní	k2eAgNnSc2d1	naivní
malířství	malířství	k1gNnSc2	malířství
(	(	kIx(	(
<g/>
H.	H.	kA	H.
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samá	samý	k3xTgFnSc1	samý
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
-	-	kIx~	-
Tato	tento	k3xDgFnSc1	tento
sbírka	sbírka	k1gFnSc1	sbírka
patří	patřit	k5eAaImIp3nS	patřit
ještě	ještě	k9	ještě
do	do	k7c2	do
proletářské	proletářský	k2eAgFnSc2d1	proletářská
poezie	poezie	k1gFnSc2	poezie
reminiscencemi	reminiscence	k1gFnPc7	reminiscence
na	na	k7c4	na
barvité	barvitý	k2eAgNnSc4d1	barvité
prostředí	prostředí	k1gNnSc4	prostředí
dělnického	dělnický	k2eAgInSc2d1	dělnický
Žižkova	Žižkov	k1gInSc2	Žižkov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
už	už	k6eAd1	už
i	i	k9	i
náznaky	náznak	k1gInPc1	náznak
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
později	pozdě	k6eAd2	pozdě
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
poetismus	poetismus	k1gInSc1	poetismus
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
nevěnuje	věnovat	k5eNaImIp3nS	věnovat
tolik	tolik	k6eAd1	tolik
pozornosti	pozornost	k1gFnPc4	pozornost
drastickým	drastický	k2eAgInPc3d1	drastický
sociálním	sociální	k2eAgInPc3d1	sociální
obrazům	obraz	k1gInPc3	obraz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
rozvinout	rozvinout	k5eAaPmF	rozvinout
krásu	krása	k1gFnSc4	krása
<g/>
.	.	kIx.	.
</s>
<s>
Přestává	přestávat	k5eAaImIp3nS	přestávat
zavrhovat	zavrhovat	k5eAaImF	zavrhovat
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
ho	on	k3xPp3gMnSc4	on
obdivovat	obdivovat	k5eAaImF	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
zintimnění	zintimnění	k1gNnSc3	zintimnění
a	a	k8xC	a
poetizaci	poetizace	k1gFnSc3	poetizace
i	i	k8xC	i
u	u	k7c2	u
velkých	velký	k2eAgNnPc2d1	velké
sociálních	sociální	k2eAgNnPc2d1	sociální
témat	téma	k1gNnPc2	téma
<g/>
:	:	kIx,	:
Na	na	k7c6	na
vlnách	vlna	k1gFnPc6	vlna
TSF	TSF	kA	TSF
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
TSF	TSF	kA	TSF
=	=	kIx~	=
Télegraphie	Télegraphie	k1gFnSc1	Télegraphie
sans	sans	k1gInSc1	sans
fil	fil	k?	fil
-	-	kIx~	-
tj.	tj.	kA	tj.
rozhlas	rozhlas	k1gInSc1	rozhlas
bez	bez	k7c2	bez
drátu	drát	k1gInSc2	drát
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Tato	tento	k3xDgFnSc1	tento
sbírka	sbírka	k1gFnSc1	sbírka
velmi	velmi	k6eAd1	velmi
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
vznikající	vznikající	k2eAgInSc4d1	vznikající
poetismus	poetismus	k1gInSc4	poetismus
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
tohoto	tento	k3xDgInSc2	tento
uměleckého	umělecký	k2eAgInSc2d1	umělecký
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Významová	významový	k2eAgFnSc1d1	významová
stránka	stránka	k1gFnSc1	stránka
veršů	verš	k1gInPc2	verš
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nesena	nést	k5eAaImNgFnS	nést
originální	originální	k2eAgFnSc7d1	originální
typografickou	typografický	k2eAgFnSc7d1	typografická
úpravou	úprava	k1gFnSc7	úprava
Karla	Karel	k1gMnSc2	Karel
Teigeho	Teige	k1gMnSc2	Teige
<g/>
:	:	kIx,	:
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
básně-obrazy	básněbraz	k1gInPc4	básně-obraz
<g/>
,	,	kIx,	,
působící	působící	k2eAgInSc1d1	působící
nejen	nejen	k6eAd1	nejen
slovem	slovo	k1gNnSc7	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vizuálně	vizuálně	k6eAd1	vizuálně
<g/>
;	;	kIx,	;
střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
jsou	být	k5eAaImIp3nP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
na	na	k7c6	na
slovních	slovní	k2eAgFnPc6d1	slovní
hříčkách	hříčka	k1gFnPc6	hříčka
a	a	k8xC	a
lyrických	lyrický	k2eAgFnPc6d1	lyrická
anekdodách	anekdoda	k1gFnPc6	anekdoda
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
motivy	motiv	k1gInPc4	motiv
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
volně	volně	k6eAd1	volně
spojovány	spojovat	k5eAaImNgFnP	spojovat
básníkovou	básníkův	k2eAgFnSc7d1	básníkova
fantazií	fantazie	k1gFnSc7	fantazie
a	a	k8xC	a
působením	působení	k1gNnSc7	působení
volných	volný	k2eAgFnPc2d1	volná
asociací	asociace	k1gFnPc2	asociace
<g/>
,	,	kIx,	,
svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
hravým	hravý	k2eAgNnSc7d1	hravé
<g/>
,	,	kIx,	,
radostným	radostný	k2eAgNnSc7d1	radostné
jevištěm	jeviště	k1gNnSc7	jeviště
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
sbírka	sbírka	k1gFnSc1	sbírka
přepracována	přepracovat	k5eAaPmNgFnS	přepracovat
a	a	k8xC	a
vydána	vydán	k2eAgFnSc1d1	vydána
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Svatební	svatební	k2eAgFnSc1d1	svatební
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Slavík	slavík	k1gInSc1	slavík
zpívá	zpívat	k5eAaImIp3nS	zpívat
špatně	špatně	k6eAd1	špatně
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
-	-	kIx~	-
Motivy	motiv	k1gInPc1	motiv
minulých	minulý	k2eAgFnPc2d1	minulá
i	i	k8xC	i
tušených	tušený	k2eAgFnPc2d1	tušená
budoucích	budoucí	k2eAgFnPc2d1	budoucí
válek	válka	k1gFnPc2	válka
vnášejí	vnášet	k5eAaImIp3nP	vnášet
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sbírce	sbírka	k1gFnSc6	sbírka
do	do	k7c2	do
Seifertovy	Seifertův	k2eAgFnSc2d1	Seifertova
hravosti	hravost	k1gFnSc2	hravost
hořkost	hořkost	k1gFnSc4	hořkost
a	a	k8xC	a
vážnější	vážní	k2eAgInPc4d2	vážnější
tóny	tón	k1gInPc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Poštovní	poštovní	k2eAgMnSc1d1	poštovní
holub	holub	k1gMnSc1	holub
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zde	zde	k6eAd1	zde
Seifert	Seifert	k1gMnSc1	Seifert
začíná	začínat	k5eAaImIp3nS	začínat
pomalu	pomalu	k6eAd1	pomalu
opouštět	opouštět	k5eAaImF	opouštět
avantgardní	avantgardní	k2eAgFnSc4d1	avantgardní
hravost	hravost	k1gFnSc4	hravost
a	a	k8xC	a
obrací	obracet	k5eAaImIp3nS	obracet
se	se	k3xPyFc4	se
k	k	k7c3	k
svým	svůj	k3xOyFgInPc3	svůj
typickým	typický	k2eAgInPc3d1	typický
<g/>
,	,	kIx,	,
intimnějším	intimní	k2eAgNnPc3d2	intimnější
tématům	téma	k1gNnPc3	téma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
<g/>
,	,	kIx,	,
matce	matka	k1gFnSc3	matka
a	a	k8xC	a
domovu	domov	k1gInSc3	domov
a	a	k8xC	a
určitá	určitý	k2eAgFnSc1d1	určitá
nostalgie	nostalgie	k1gFnSc1	nostalgie
<g/>
.	.	kIx.	.
</s>
<s>
Sílí	sílet	k5eAaImIp3nS	sílet
pocity	pocit	k1gInPc4	pocit
melancholie	melancholie	k1gFnSc2	melancholie
a	a	k8xC	a
skepse	skepse	k1gFnSc2	skepse
k	k	k7c3	k
možnému	možný	k2eAgNnSc3d1	možné
revolučnímu	revoluční	k2eAgNnSc3d1	revoluční
řešení	řešení	k1gNnSc3	řešení
sociálních	sociální	k2eAgInPc2d1	sociální
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jablko	jablko	k1gNnSc1	jablko
z	z	k7c2	z
klína	klín	k1gInSc2	klín
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
-	-	kIx~	-
Prolíná	prolínat	k5eAaImIp3nS	prolínat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nostalgie	nostalgie	k1gFnSc1	nostalgie
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tušenými	tušený	k2eAgFnPc7d1	tušená
(	(	kIx(	(
<g/>
ale	ale	k9	ale
neprožitými	prožitý	k2eNgFnPc7d1	neprožitá
<g/>
)	)	kIx)	)
krásami	krása	k1gFnPc7	krása
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
sbírkami	sbírka	k1gFnPc7	sbírka
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
jen	jen	k9	jen
občas	občas	k6eAd1	občas
objevuje	objevovat	k5eAaImIp3nS	objevovat
sociální	sociální	k2eAgFnSc1d1	sociální
problematika	problematika	k1gFnSc1	problematika
<g/>
,	,	kIx,	,
verše	verš	k1gInPc1	verš
jsou	být	k5eAaImIp3nP	být
střídmé	střídmý	k2eAgInPc1d1	střídmý
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
stavěné	stavěný	k2eAgFnPc1d1	stavěná
a	a	k8xC	a
melodicky	melodicky	k6eAd1	melodicky
zpěvné	zpěvný	k2eAgFnPc1d1	zpěvná
<g/>
,	,	kIx,	,
Seifert	Seifert	k1gMnSc1	Seifert
zde	zde	k6eAd1	zde
definitivně	definitivně	k6eAd1	definitivně
nalézá	nalézat	k5eAaImIp3nS	nalézat
svou	svůj	k3xOyFgFnSc4	svůj
typickou	typický	k2eAgFnSc4d1	typická
poetiku	poetika	k1gFnSc4	poetika
citlivého	citlivý	k2eAgNnSc2d1	citlivé
vnímání	vnímání	k1gNnSc2	vnímání
lyrického	lyrický	k2eAgInSc2d1	lyrický
subjektu	subjekt	k1gInSc2	subjekt
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnPc7	jeho
tématy	téma	k1gNnPc7	téma
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
<g/>
)	)	kIx)	)
krása	krása	k1gFnSc1	krása
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
nostalgie	nostalgie	k1gFnSc1	nostalgie
domova	domov	k1gInSc2	domov
a	a	k8xC	a
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
stesk	stesk	k1gInSc1	stesk
nad	nad	k7c7	nad
uplývajícím	uplývající	k2eAgInSc7d1	uplývající
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
křehké	křehký	k2eAgNnSc4d1	křehké
kouzlo	kouzlo	k1gNnSc4	kouzlo
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Ruce	ruka	k1gFnPc1	ruka
Venušiny	Venušin	k2eAgFnSc2d1	Venušina
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
Jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
sbohem	sbohem	k0	sbohem
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
Návrat	návrat	k1gInSc1	návrat
Seifertova	Seifertův	k2eAgInSc2d1	Seifertův
zájmu	zájem	k1gInSc2	zájem
k	k	k7c3	k
politickým	politický	k2eAgInPc3d1	politický
problémům	problém	k1gInPc3	problém
a	a	k8xC	a
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
naši	náš	k3xOp1gFnSc4	náš
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
předcházející	předcházející	k2eAgFnSc7d1	předcházející
sbírkou	sbírka	k1gFnSc7	sbírka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevují	objevovat	k5eAaImIp3nP	objevovat
ohlasy	ohlas	k1gInPc1	ohlas
na	na	k7c4	na
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
krizi	krize	k1gFnSc4	krize
a	a	k8xC	a
nastupující	nastupující	k2eAgInSc4d1	nastupující
fašismus	fašismus	k1gInSc4	fašismus
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnSc3	událost
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
-	-	kIx~	-
mobilizace	mobilizace	k1gFnSc1	mobilizace
a	a	k8xC	a
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
-	-	kIx~	-
na	na	k7c4	na
Seiferta	Seifert	k1gMnSc4	Seifert
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
českých	český	k2eAgMnPc2d1	český
autorů	autor	k1gMnPc2	autor
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
zapůsobily	zapůsobit	k5eAaPmAgFnP	zapůsobit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
publikuje	publikovat	k5eAaBmIp3nS	publikovat
cyklus	cyklus	k1gInSc1	cyklus
Osm	osm	k4xCc1	osm
dní	den	k1gInPc2	den
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
pohřbu	pohřeb	k1gInSc6	pohřeb
T.G.	T.G.	k1gMnSc2	T.G.
<g/>
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
;	;	kIx,	;
po	po	k7c6	po
Mnichovu	Mnichov	k1gInSc6	Mnichov
a	a	k8xC	a
okupaci	okupace	k1gFnSc6	okupace
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
poezie	poezie	k1gFnSc1	poezie
stává	stávat	k5eAaImIp3nS	stávat
nástrojem	nástroj	k1gInSc7	nástroj
upevňování	upevňování	k1gNnSc2	upevňování
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Zhasněte	zhasnout	k5eAaPmRp2nP	zhasnout
světla	světlo	k1gNnPc1	světlo
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
-	-	kIx~	-
Reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
zradu	zrada	k1gFnSc4	zrada
<g/>
,	,	kIx,	,
vydáno	vydán	k2eAgNnSc1d1	vydáno
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Lyrické	lyrický	k2eAgFnPc1d1	lyrická
glosy	glosa	k1gFnPc1	glosa
<g/>
;	;	kIx,	;
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
rodné	rodný	k2eAgFnSc6d1	rodná
zemi	zem	k1gFnSc6	zem
-	-	kIx~	-
rodná	rodný	k2eAgFnSc1d1	rodná
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
jistotou	jistota	k1gFnSc7	jistota
<g/>
,	,	kIx,	,
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Vějíř	vějíř	k1gInSc1	vějíř
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Lidově	lidově	k6eAd1	lidově
vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
poezie	poezie	k1gFnSc1	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Evokuje	evokovat	k5eAaBmIp3nS	evokovat
statečnost	statečnost	k1gFnSc1	statečnost
české	český	k2eAgFnSc2d1	Česká
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
a	a	k8xC	a
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
obrazu	obraz	k1gInSc3	obraz
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Světlem	světlo	k1gNnSc7	světlo
oděná	oděný	k2eAgFnSc1d1	oděná
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
-	-	kIx~	-
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kamenným	kamenný	k2eAgInSc7d1	kamenný
mostem	most	k1gInSc7	most
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
protiokupační	protiokupační	k2eAgFnSc4d1	protiokupační
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Motivy	motiv	k1gInPc1	motiv
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
české	český	k2eAgFnSc2d1	Česká
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
zejména	zejména	k9	zejména
Prahy	Praha	k1gFnSc2	Praha
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
jistoty	jistota	k1gFnSc2	jistota
a	a	k8xC	a
útěchy	útěcha	k1gFnSc2	útěcha
v	v	k7c6	v
těžkých	těžký	k2eAgFnPc6d1	těžká
protektorátních	protektorátní	k2eAgFnPc6d1	protektorátní
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
-	-	kIx~	-
Cyklus	cyklus	k1gInSc1	cyklus
pěti	pět	k4xCc2	pět
romancí	romance	k1gFnPc2	romance
a	a	k8xC	a
legend	legenda	k1gFnPc2	legenda
zasazených	zasazený	k2eAgFnPc2d1	zasazená
do	do	k7c2	do
májové	májový	k2eAgFnSc2d1	Májová
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Přilba	přilba	k1gFnSc1	přilba
hlíny	hlína	k1gFnSc2	hlína
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sbírka	sbírka	k1gFnSc1	sbírka
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
dík	dík	k7c3	dík
osvoboditelům	osvoboditel	k1gMnPc3	osvoboditel
a	a	k8xC	a
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
<g/>
,	,	kIx,	,
raduje	radovat	k5eAaImIp3nS	radovat
se	se	k3xPyFc4	se
z	z	k7c2	z
Pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
lituje	litovat	k5eAaImIp3nS	litovat
mrtvých	mrtvý	k1gMnPc2	mrtvý
na	na	k7c6	na
barikádách	barikáda	k1gFnPc6	barikáda
<g/>
.	.	kIx.	.
</s>
<s>
Autorova	autorův	k2eAgFnSc1d1	autorova
poetika	poetika	k1gFnSc1	poetika
se	se	k3xPyFc4	se
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
předchozím	předchozí	k2eAgNnSc7d1	předchozí
obdobím	období	k1gNnSc7	období
ještě	ještě	k9	ještě
nijak	nijak	k6eAd1	nijak
podstatně	podstatně	k6eAd1	podstatně
neproměňuje	proměňovat	k5eNaImIp3nS	proměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
Seifert	Seifert	k1gMnSc1	Seifert
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c4	v
své	svůj	k3xOyFgFnPc4	svůj
přechozí	přechozit	k5eAaPmIp3nP	přechozit
tvorbě	tvorba	k1gFnSc3	tvorba
vyznačující	vyznačující	k2eAgFnSc4d1	vyznačující
se	s	k7c7	s
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
veršem	verš	k1gInSc7	verš
<g/>
,	,	kIx,	,
lyrickou	lyrický	k2eAgFnSc7d1	lyrická
zpěvností	zpěvnost	k1gFnSc7	zpěvnost
a	a	k8xC	a
tematikou	tematika	k1gFnSc7	tematika
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
však	však	k9	však
coby	coby	k?	coby
někdejší	někdejší	k2eAgMnSc1d1	někdejší
představitel	představitel	k1gMnSc1	představitel
předválečné	předválečný	k2eAgFnSc2d1	předválečná
avantgardy	avantgarda	k1gFnSc2	avantgarda
ocitá	ocitat	k5eAaImIp3nS	ocitat
v	v	k7c6	v
nemilosti	nemilost	k1gFnSc6	nemilost
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
režimním	režimní	k2eAgInSc7d1	režimní
postihem	postih	k1gInSc7	postih
zachránil	zachránit	k5eAaPmAgMnS	zachránit
Seiferta	Seifert	k1gMnSc4	Seifert
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pouze	pouze	k6eAd1	pouze
zásah	zásah	k1gInSc1	zásah
dávného	dávný	k2eAgMnSc2d1	dávný
přítele	přítel	k1gMnSc2	přítel
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Nezvala	Nezval	k1gMnSc2	Nezval
<g/>
.	.	kIx.	.
</s>
<s>
Ruka	ruka	k1gFnSc1	ruka
a	a	k8xC	a
plamen	plamen	k1gInSc1	plamen
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Šel	jít	k5eAaImAgMnS	jít
malíř	malíř	k1gMnSc1	malíř
chudě	chudě	k6eAd1	chudě
do	do	k7c2	do
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
Verše	verš	k1gInSc2	verš
inspirované	inspirovaný	k2eAgInPc4d1	inspirovaný
obrázky	obrázek	k1gInPc4	obrázek
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
Alše	Aleš	k1gMnSc2	Aleš
<g/>
,	,	kIx,	,
oslava	oslava	k1gFnSc1	oslava
české	český	k2eAgFnSc2d1	Česká
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
české	český	k2eAgFnSc2d1	Česká
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
Viktorce	Viktorka	k1gFnSc6	Viktorka
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
-	-	kIx~	-
Tragický	tragický	k2eAgInSc1d1	tragický
osud	osud	k1gInSc1	osud
Viktorky	Viktorka	k1gFnSc2	Viktorka
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Babička	babička	k1gFnSc1	babička
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
s	s	k7c7	s
neméně	málo	k6eNd2	málo
obtížnými	obtížný	k2eAgInPc7d1	obtížný
osudy	osud	k1gInPc7	osud
její	její	k3xOp3gFnSc2	její
autorky	autorka	k1gFnSc2	autorka
B.	B.	kA	B.
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
křehké	křehký	k2eAgInPc4d1	křehký
<g/>
,	,	kIx,	,
melancholické	melancholický	k2eAgInPc4d1	melancholický
verše	verš	k1gInPc4	verš
spíše	spíše	k9	spíše
s	s	k7c7	s
útěšným	útěšný	k2eAgNnSc7d1	útěšné
vyzněním	vyznění	k1gNnSc7	vyznění
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
zakázána	zakázán	k2eAgFnSc1d1	zakázána
<g/>
.	.	kIx.	.
</s>
<s>
Maminka	maminka	k1gFnSc1	maminka
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
-	-	kIx~	-
Sbírka	sbírka	k1gFnSc1	sbírka
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
vzpomínkám	vzpomínka	k1gFnPc3	vzpomínka
na	na	k7c4	na
dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
především	především	k9	především
na	na	k7c4	na
Seifertovu	Seifertův	k2eAgFnSc4d1	Seifertova
maminku	maminka	k1gFnSc4	maminka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
udělat	udělat	k5eAaPmF	udělat
domov	domov	k1gInSc4	domov
krásným	krásný	k2eAgInSc7d1	krásný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
chudí	chudý	k2eAgMnPc1d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Opěvuje	opěvovat	k5eAaImIp3nS	opěvovat
zde	zde	k6eAd1	zde
její	její	k3xOp3gFnSc4	její
prostotu	prostota	k1gFnSc4	prostota
<g/>
,	,	kIx,	,
skromnost	skromnost	k1gFnSc4	skromnost
a	a	k8xC	a
obětavost	obětavost	k1gFnSc4	obětavost
hraničící	hraničící	k2eAgFnSc4d1	hraničící
se	s	k7c7	s
sebezapřením	sebezapření	k1gNnSc7	sebezapření
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
láskyplnou	láskyplný	k2eAgFnSc4d1	láskyplná
přítomnost	přítomnost	k1gFnSc4	přítomnost
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
prázdnotou	prázdnota	k1gFnSc7	prázdnota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nastala	nastat	k5eAaPmAgFnS	nastat
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc1	hvězda
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
"	"	kIx"	"
<g/>
Verše	verš	k1gInPc1	verš
k	k	k7c3	k
obrazům	obraz	k1gInPc3	obraz
a	a	k8xC	a
obrázkům	obrázek	k1gInPc3	obrázek
Josefa	Josef	k1gMnSc2	Josef
Lady	lady	k1gFnPc2	lady
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
melancholická	melancholický	k2eAgFnSc1d1	melancholická
s	s	k7c7	s
pohádkovými	pohádkový	k2eAgInPc7d1	pohádkový
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odmlce	odmlka	k1gFnSc6	odmlka
způsobené	způsobený	k2eAgNnSc1d1	způsobené
těžkou	těžký	k2eAgFnSc7d1	těžká
chorobou	choroba	k1gFnSc7	choroba
se	se	k3xPyFc4	se
Seifert	Seifert	k1gMnSc1	Seifert
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
změněnou	změněný	k2eAgFnSc7d1	změněná
poetikou	poetika	k1gFnSc7	poetika
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
rytmické	rytmický	k2eAgFnSc2d1	rytmická
pravidelnosti	pravidelnost	k1gFnSc2	pravidelnost
a	a	k8xC	a
rýmů	rým	k1gInPc2	rým
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
volný	volný	k2eAgInSc1d1	volný
verš	verš	k1gInSc1	verš
<g/>
,	,	kIx,	,
ubylo	ubýt	k5eAaPmAgNnS	ubýt
poetismů	poetismus	k1gInPc2	poetismus
a	a	k8xC	a
metafor	metafora	k1gFnPc2	metafora
<g/>
,	,	kIx,	,
výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
oproštěnější	oproštěný	k2eAgInPc4d2	oproštěný
a	a	k8xC	a
věcnější	věcní	k2eAgInPc4d2	věcní
<g/>
,	,	kIx,	,
typické	typický	k2eAgInPc4d1	typický
seifertovské	seifertovský	k2eAgInPc4d1	seifertovský
motivy	motiv	k1gInPc4	motiv
(	(	kIx(	(
<g/>
dětství	dětství	k1gNnSc1	dětství
<g/>
,	,	kIx,	,
domov	domov	k1gInSc1	domov
<g/>
,	,	kIx,	,
mládí	mládí	k1gNnSc1	mládí
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
poněkud	poněkud	k6eAd1	poněkud
zdrsněly	zdrsnět	k5eAaPmAgInP	zdrsnět
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
viděny	viděn	k2eAgFnPc1d1	viděna
skrz	skrz	k7c4	skrz
"	"	kIx"	"
<g/>
černé	černý	k2eAgNnSc4d1	černé
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
existenciální	existenciální	k2eAgInPc1d1	existenciální
motivy	motiv	k1gInPc1	motiv
umírání	umírání	k1gNnSc2	umírání
a	a	k8xC	a
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
pryč	pryč	k6eAd1	pryč
s	s	k7c7	s
onou	onen	k3xDgFnSc7	onen
básnickou	básnický	k2eAgFnSc7d1	básnická
veteší	veteš	k1gFnSc7	veteš
metafor	metafora	k1gFnPc2	metafora
a	a	k8xC	a
rýmů	rým	k1gInPc2	rým
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
Seifert	Seifert	k1gMnSc1	Seifert
v	v	k7c6	v
často	často	k6eAd1	často
citovaném	citovaný	k2eAgInSc6d1	citovaný
verši	verš	k1gInSc6	verš
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
až	až	k9	až
mrazivě	mrazivě	k6eAd1	mrazivě
holý	holý	k2eAgInSc1d1	holý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Koncert	koncert	k1gInSc1	koncert
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Odlévání	odlévání	k1gNnPc2	odlévání
zvonů	zvon	k1gInPc2	zvon
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Halleyova	Halleyův	k2eAgFnSc1d1	Halleyova
kometa	kometa	k1gFnSc1	kometa
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
V	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
normalizace	normalizace	k1gFnSc2	normalizace
se	se	k3xPyFc4	se
Seifert	Seifert	k1gMnSc1	Seifert
znovu	znovu	k6eAd1	znovu
ocitá	ocitat	k5eAaImIp3nS	ocitat
mimo	mimo	k7c4	mimo
oficiální	oficiální	k2eAgFnSc4d1	oficiální
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
verše	verš	k1gInPc4	verš
je	být	k5eAaImIp3nS	být
nucen	nutit	k5eAaImNgMnS	nutit
publikovat	publikovat	k5eAaBmF	publikovat
v	v	k7c6	v
samizdatu	samizdat	k1gInSc6	samizdat
<g/>
;	;	kIx,	;
v	v	k7c6	v
oficiálních	oficiální	k2eAgNnPc6d1	oficiální
nakladatelstvích	nakladatelství	k1gNnPc6	nakladatelství
vycházejí	vycházet	k5eAaImIp3nP	vycházet
až	až	k6eAd1	až
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
,	,	kIx,	,
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
nákladu	náklad	k1gInSc6	náklad
<g/>
,	,	kIx,	,
po	po	k7c6	po
cenzurních	cenzurní	k2eAgInPc6d1	cenzurní
zásazích	zásah	k1gInPc6	zásah
a	a	k8xC	a
bez	bez	k7c2	bez
pozornosti	pozornost	k1gFnSc2	pozornost
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Vzít	vzít	k5eAaPmF	vzít
básníka	básník	k1gMnSc4	básník
na	na	k7c4	na
milost	milost	k1gFnSc4	milost
musel	muset	k5eAaImAgMnS	muset
režim	režim	k1gInSc4	režim
až	až	k9	až
po	po	k7c6	po
udělení	udělení	k1gNnSc6	udělení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
jeho	jeho	k3xOp3gNnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
verši	verš	k1gInSc6	verš
<g/>
,	,	kIx,	,
s	s	k7c7	s
výrazem	výraz	k1gInSc7	výraz
oproštěným	oproštěný	k2eAgInSc7d1	oproštěný
téměř	téměř	k6eAd1	téměř
až	až	k6eAd1	až
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
rytmické	rytmický	k2eAgFnSc2d1	rytmická
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
Tematicky	tematicky	k6eAd1	tematicky
zde	zde	k6eAd1	zde
Seifert	Seifert	k1gMnSc1	Seifert
především	především	k6eAd1	především
bilancuje	bilancovat	k5eAaImIp3nS	bilancovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
veršů	verš	k1gInPc2	verš
věnuje	věnovat	k5eAaImIp3nS	věnovat
zemřelým	zemřelý	k2eAgMnPc3d1	zemřelý
přátelům	přítel	k1gMnPc3	přítel
a	a	k8xC	a
dávným	dávný	k2eAgFnPc3d1	dávná
láskám	láska	k1gFnPc3	láska
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
-	-	kIx~	-
Zakázaná	zakázaný	k2eAgFnSc1d1	zakázaná
sbírka	sbírka	k1gFnSc1	sbírka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
samizdatu	samizdat	k1gInSc6	samizdat
a	a	k8xC	a
1977	[number]	k4	1977
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
motivem	motiv	k1gInSc7	motiv
vzpomínkově	vzpomínkově	k6eAd1	vzpomínkově
laděné	laděný	k2eAgFnSc2d1	laděná
sbírky	sbírka	k1gFnSc2	sbírka
je	být	k5eAaImIp3nS	být
okouzlení	okouzlení	k1gNnSc1	okouzlení
lyrického	lyrický	k2eAgInSc2d1	lyrický
subjektu	subjekt	k1gInSc2	subjekt
láskou	láska	k1gFnSc7	láska
a	a	k8xC	a
krásou	krása	k1gFnSc7	krása
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
však	však	k9	však
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
tematiku	tematika	k1gFnSc4	tematika
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
pomíjivosti	pomíjivost	k1gFnSc2	pomíjivost
a	a	k8xC	a
tematiku	tematika	k1gFnSc4	tematika
hořké	hořký	k2eAgFnSc2d1	hořká
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
situaci	situace	k1gFnSc4	situace
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
okupaci	okupace	k1gFnSc6	okupace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
byla	být	k5eAaImAgFnS	být
sbírka	sbírka	k1gFnSc1	sbírka
vydána	vydán	k2eAgFnSc1d1	vydána
i	i	k9	i
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
.	.	kIx.	.
</s>
<s>
Deštník	deštník	k1gInSc1	deštník
z	z	k7c2	z
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Býti	být	k5eAaImF	být
básníkem	básník	k1gMnSc7	básník
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
Seifertova	Seifertův	k2eAgFnSc1d1	Seifertova
poslední	poslední	k2eAgFnSc1d1	poslední
sbírka	sbírka	k1gFnSc1	sbírka
je	být	k5eAaImIp3nS	být
jakousi	jakýsi	k3yIgFnSc7	jakýsi
básnickou	básnický	k2eAgFnSc7d1	básnická
rekapitulací	rekapitulace	k1gFnSc7	rekapitulace
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
i	i	k8xC	i
loučením	loučení	k1gNnSc7	loučení
se	s	k7c7	s
světem	svět	k1gInSc7	svět
<g/>
;	;	kIx,	;
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
měl	mít	k5eAaImAgInS	mít
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
miloval	milovat	k5eAaImAgMnS	milovat
a	a	k8xC	a
svými	svůj	k3xOyFgInPc7	svůj
verši	verš	k1gInPc7	verš
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
radost	radost	k6eAd1	radost
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
nad	nad	k7c7	nad
Rajskou	rajský	k2eAgFnSc7d1	rajská
zahradou	zahrada	k1gFnSc7	zahrada
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
-	-	kIx~	-
Autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
skica	skica	k1gFnSc1	skica
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hluboce	hluboko	k6eAd1	hluboko
ho	on	k3xPp3gMnSc4	on
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
poetismus	poetismus	k1gInSc1	poetismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
žižkovské	žižkovský	k2eAgNnSc4d1	Žižkovské
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Všecky	všecek	k3xTgFnPc4	všecek
krásy	krása	k1gFnPc4	krása
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
-	-	kIx~	-
Prozaická	prozaický	k2eAgFnSc1d1	prozaická
kniha	kniha	k1gFnSc1	kniha
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
jeho	jeho	k3xOp3gFnPc3	jeho
beletrizovaným	beletrizovaný	k2eAgFnPc3d1	beletrizovaná
životním	životní	k2eAgFnPc3d1	životní
vzpomínkám	vzpomínka	k1gFnPc3	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
zde	zde	k6eAd1	zde
svým	svůj	k3xOyFgNnSc7	svůj
osobitým	osobitý	k2eAgNnSc7d1	osobité
<g/>
,	,	kIx,	,
láskyplným	láskyplný	k2eAgNnSc7d1	láskyplné
a	a	k8xC	a
jemně	jemně	k6eAd1	jemně
sebeironickým	sebeironický	k2eAgNnSc7d1	sebeironické
viděním	vidění	k1gNnSc7	vidění
spíše	spíše	k9	spíše
poetizuje	poetizovat	k5eAaBmIp3nS	poetizovat
než	než	k8xS	než
dokumentárně	dokumentárně	k6eAd1	dokumentárně
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
krátkých	krátký	k2eAgFnPc6d1	krátká
črtách	črta	k1gFnPc6	črta
se	se	k3xPyFc4	se
ohlíží	ohlížet	k5eAaImIp3nS	ohlížet
za	za	k7c7	za
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
všedními	všední	k1gMnPc7	všední
i	i	k8xC	i
mimořádnými	mimořádný	k2eAgInPc7d1	mimořádný
momenty	moment	k1gInPc7	moment
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
proudily	proudit	k5eAaPmAgFnP	proudit
krása	krása	k1gFnSc1	krása
a	a	k8xC	a
poezie	poezie	k1gFnSc1	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
víceméně	víceméně	k9	víceméně
chronologicky	chronologicky	k6eAd1	chronologicky
navazujících	navazující	k2eAgFnPc2d1	navazující
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Co	co	k3yInSc1	co
všecko	všecek	k3xTgNnSc4	všecek
zavál	zavát	k5eAaPmAgInS	zavát
sníh	sníh	k1gInSc1	sníh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
žižkovské	žižkovský	k2eAgNnSc4d1	Žižkovské
dětství	dětství	k1gNnSc4	dětství
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Eós	Eós	k1gFnSc1	Eós
<g/>
,	,	kIx,	,
bohyně	bohyně	k1gFnSc1	bohyně
ranních	ranní	k2eAgInPc2d1	ranní
červánků	červánek	k1gInPc2	červánek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
básnické	básnický	k2eAgInPc1d1	básnický
začátky	začátek	k1gInPc1	začátek
<g/>
,	,	kIx,	,
mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
období	období	k1gNnSc1	období
prvních	první	k4xOgFnPc2	první
lásek	láska	k1gFnPc2	láska
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Uprostřed	uprostřed	k7c2	uprostřed
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
válečné	válečný	k2eAgFnSc2d1	válečná
a	a	k8xC	a
poválečné	poválečný	k2eAgFnSc2d1	poválečná
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Obloha	obloha	k1gFnSc1	obloha
plná	plný	k2eAgFnSc1d1	plná
havranů	havran	k1gMnPc2	havran
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
bilancování	bilancování	k1gNnSc1	bilancování
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc2	stáří
a	a	k8xC	a
úvahy	úvaha	k1gFnSc2	úvaha
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
překladatel	překladatel	k1gMnSc1	překladatel
Seifert	Seifert	k1gMnSc1	Seifert
převedl	převést	k5eAaPmAgMnS	převést
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
mj.	mj.	kA	mj.
básně	báseň	k1gFnSc2	báseň
Paula	Paul	k1gMnSc2	Paul
Verlaina	Verlain	k1gMnSc2	Verlain
<g/>
,	,	kIx,	,
Alexandra	Alexandr	k1gMnSc2	Alexandr
Bloka	Bloek	k1gMnSc2	Bloek
nebo	nebo	k8xC	nebo
Guillauma	Guillaum	k1gMnSc2	Guillaum
Apollinaira	Apollinair	k1gMnSc2	Apollinair
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
biblickou	biblický	k2eAgFnSc4d1	biblická
Píseň	píseň	k1gFnSc4	píseň
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
linie	linie	k1gFnSc2	linie
"	"	kIx"	"
<g/>
velkých	velká	k1gFnPc2	velká
<g/>
"	"	kIx"	"
básnických	básnický	k2eAgFnPc2d1	básnická
sbírek	sbírka	k1gFnPc2	sbírka
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Seifert	Seifert	k1gMnSc1	Seifert
řadu	řada	k1gFnSc4	řada
soukromých	soukromý	k2eAgMnPc2d1	soukromý
tisků	tisek	k1gMnPc2	tisek
a	a	k8xC	a
bibliofilií	bibliofilie	k1gFnPc2	bibliofilie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jen	jen	k9	jen
několik	několik	k4yIc1	několik
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
online	onlinout	k5eAaPmIp3nS	onlinout
ve	v	k7c6	v
Slovníku	slovník	k1gInSc6	slovník
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Databázi	databáze	k1gFnSc6	databáze
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Slezská	slezský	k2eAgFnSc1d1	Slezská
píseň	píseň	k1gFnSc1	píseň
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
soukromý	soukromý	k2eAgInSc1d1	soukromý
tisk	tisk	k1gInSc1	tisk
<g/>
)	)	kIx)	)
Hrst	hrst	k1gFnSc1	hrst
loňského	loňský	k2eAgInSc2d1	loňský
sněhu	sníh	k1gInSc2	sníh
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
bibliofilie	bibliofilie	k1gFnSc1	bibliofilie
<g/>
)	)	kIx)	)
Podzim	podzim	k1gInSc1	podzim
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
bibliofilie	bibliofilie	k1gFnSc1	bibliofilie
<g/>
)	)	kIx)	)
Devět	devět	k4xCc4	devět
rondeaux	rondeaux	k1gInSc4	rondeaux
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
úvodní	úvodní	k2eAgFnSc1d1	úvodní
ilustrace	ilustrace	k1gFnSc1	ilustrace
Karla	Karel	k1gMnSc2	Karel
Svolinského	Svolinský	k2eAgMnSc2d1	Svolinský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
nám	my	k3xPp1nPc3	my
neprší	pršet	k5eNaImIp3nS	pršet
na	na	k7c4	na
rakev	rakev	k1gFnSc4	rakev
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Václav	Václav	k1gMnSc1	Václav
Plátek	plátek	k1gInSc1	plátek
<g/>
)	)	kIx)	)
Suknice	suknice	k1gFnPc1	suknice
andělů	anděl	k1gMnPc2	anděl
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
Frenštátská	frenštátský	k2eAgFnSc1d1	Frenštátská
koleda	koleda	k1gFnSc1	koleda
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
soukromý	soukromý	k2eAgInSc1d1	soukromý
tisk	tisk	k1gInSc1	tisk
<g/>
)	)	kIx)	)
Pozdrav	pozdrav	k1gInSc1	pozdrav
Františku	František	k1gMnSc3	František
Halasovi	Halas	k1gMnSc3	Halas
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
bibliofilie	bibliofilie	k1gFnSc1	bibliofilie
<g/>
)	)	kIx)	)
Sníh	sníh	k1gInSc1	sníh
na	na	k7c6	na
střechách	střecha	k1gFnPc6	střecha
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
soukromý	soukromý	k2eAgInSc1d1	soukromý
tisk	tisk	k1gInSc1	tisk
<g/>
)	)	kIx)	)
Pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
vydání	vydání	k1gNnSc4	vydání
Seifertových	Seifertových	k2eAgInPc2d1	Seifertových
sebraných	sebraný	k2eAgInPc2d1	sebraný
spisů	spis	k1gInPc2	spis
byly	být	k5eAaImAgFnP	být
podnikány	podnikat	k5eAaImNgFnP	podnikat
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
řízené	řízený	k2eAgInPc1d1	řízený
Seifertovým	Seifertův	k2eAgMnSc7d1	Seifertův
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
básníkem	básník	k1gMnSc7	básník
A.	A.	kA	A.
M.	M.	kA	M.
Píšou	Píša	k1gMnSc7	Píša
vycházely	vycházet	k5eAaImAgFnP	vycházet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
bylo	být	k5eAaImAgNnS	být
prvních	první	k4xOgInPc2	první
sedm	sedm	k4xCc1	sedm
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
kompletní	kompletní	k2eAgInSc4d1	kompletní
a	a	k8xC	a
kritické	kritický	k2eAgNnSc4d1	kritické
vydání	vydání	k1gNnSc4	vydání
Seifertova	Seifertův	k2eAgNnSc2d1	Seifertovo
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
přinášeno	přinášen	k2eAgNnSc1d1	přinášeno
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
v	v	k7c4	v
edici	edice	k1gFnSc4	edice
Dílo	dílo	k1gNnSc1	dílo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Seiferta	Seifert	k1gMnSc2	Seifert
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
16	[number]	k4	16
svazků	svazek	k1gInPc2	svazek
rozdělené	rozdělená	k1gFnSc2	rozdělená
dílo	dílo	k1gNnSc4	dílo
řídí	řídit	k5eAaImIp3nS	řídit
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
Jiří	Jiří	k1gMnSc1	Jiří
Brabec	Brabec	k1gMnSc1	Brabec
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gMnSc4	on
dokončení	dokončení	k1gNnSc1	dokončení
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
vycházely	vycházet	k5eAaImAgFnP	vycházet
Seifertovy	Seifertův	k2eAgFnPc1d1	Seifertova
básně	báseň	k1gFnPc1	báseň
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
výborech	výbor	k1gInPc6	výbor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Básně	báseň	k1gFnSc2	báseň
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
-	-	kIx~	-
básníkem	básník	k1gMnSc7	básník
uspořádaný	uspořádaný	k2eAgInSc4d1	uspořádaný
výbor	výbor	k1gInSc4	výbor
<g/>
,	,	kIx,	,
prvních	první	k4xOgNnPc6	první
sto	sto	k4xCgNnSc4	sto
číslovaných	číslovaný	k2eAgInPc2d1	číslovaný
výtisků	výtisk	k1gInPc2	výtisk
<g/>
,	,	kIx,	,
básníkem	básník	k1gMnSc7	básník
a	a	k8xC	a
malířem	malíř	k1gMnSc7	malíř
podepsaných	podepsaný	k2eAgInPc2d1	podepsaný
na	na	k7c6	na
ručním	ruční	k2eAgInSc6d1	ruční
losinském	losinský	k2eAgInSc6d1	losinský
papíru	papír	k1gInSc6	papír
Jabloň	jabloň	k1gFnSc1	jabloň
se	s	k7c7	s
strunami	struna	k1gFnPc7	struna
pavučin	pavučina	k1gFnPc2	pavučina
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
-	-	kIx~	-
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
Ty	ty	k3xPp2nSc5	ty
<g/>
,	,	kIx,	,
lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
pozdravena	pozdraven	k2eAgFnSc1d1	pozdravena
buď	buď	k8xC	buď
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
M.	M.	kA	M.
Píša	Píša	k1gMnSc1	Píša
<g/>
)	)	kIx)	)
Zpěvy	zpěv	k1gInPc1	zpěv
o	o	k7c6	o
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
M.	M.	kA	M.
Píša	Píša	k1gMnSc1	Píša
<g/>
)	)	kIx)	)
Větvička	větvička	k1gFnSc1	větvička
jívy	jíva	k1gFnSc2	jíva
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
dosud	dosud	k6eAd1	dosud
netištěné	tištěný	k2eNgFnPc1d1	netištěná
básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
Čas	čas	k1gInSc1	čas
plný	plný	k2eAgInSc1d1	plný
písní	píseň	k1gFnSc7	píseň
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Havel	Havel	k1gMnSc1	Havel
<g/>
)	)	kIx)	)
Vrbatovská	Vrbatovský	k2eAgFnSc1d1	Vrbatovský
zahrada	zahrada	k1gFnSc1	zahrada
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
verše	verš	k1gInPc1	verš
ve	v	k7c6	v
fotografické	fotografický	k2eAgFnSc6d1	fotografická
publikaci	publikace	k1gFnSc6	publikace
L.	L.	kA	L.
Panchártkové	Panchártkové	k2eAgFnSc1d1	Panchártkové
<g/>
)	)	kIx)	)
Mnoho	mnoho	k4c1	mnoho
umělců	umělec	k1gMnPc2	umělec
Seifertovy	Seifertův	k2eAgFnSc2d1	Seifertova
básně	báseň	k1gFnSc2	báseň
rovněž	rovněž	k9	rovněž
zhudebňuje	zhudebňovat	k5eAaImIp3nS	zhudebňovat
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Karel	Karel	k1gMnSc1	Karel
Plíhal	Plíhal	k1gMnSc1	Plíhal
nebo	nebo	k8xC	nebo
Vladimír	Vladimír	k1gMnSc1	Vladimír
Veit	Veit	k1gMnSc1	Veit
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
J.	J.	kA	J.
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
,	,	kIx,	,
Ruce	ruka	k1gFnPc1	ruka
Venušiny	Venušin	k2eAgFnPc1d1	Venušina
</s>
