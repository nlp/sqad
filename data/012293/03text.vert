<p>
<s>
Paulie	Paulie	k1gFnSc1	Paulie
Garand	Garand	k1gInSc1	Garand
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Pavel	Pavel	k1gMnSc1	Pavel
Harant	Harant	k?	Harant
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
rapper	rapper	k1gMnSc1	rapper
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
pod	pod	k7c7	pod
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Ty	ten	k3xDgInPc4	ten
Nikdy	nikdy	k6eAd1	nikdy
Label	Label	k1gMnSc1	Label
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yRgNnSc7	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vydal	vydat	k5eAaPmAgInS	vydat
album	album	k1gNnSc4	album
Harant	Harant	k?	Harant
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
album	album	k1gNnSc4	album
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vydal	vydat	k5eAaPmAgMnS	vydat
desku	deska	k1gFnSc4	deska
Molo	molo	k1gNnSc4	molo
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
desku	deska	k1gFnSc4	deska
Boomerang	Boomeranga	k1gFnPc2	Boomeranga
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
desku	deska	k1gFnSc4	deska
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Lipem	Lipum	k1gNnSc7	Lipum
tvořil	tvořit	k5eAaImAgInS	tvořit
rapový	rapový	k2eAgInSc1d1	rapový
projekt	projekt	k1gInSc1	projekt
BPM	BPM	kA	BPM
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Básníci	básník	k1gMnPc1	básník
před	před	k7c7	před
mikrofonem	mikrofon	k1gInSc7	mikrofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vydal	vydat	k5eAaPmAgMnS	vydat
dvě	dva	k4xCgFnPc4	dva
desky	deska	k1gFnPc4	deska
<g/>
:	:	kIx,	:
Slova	slovo	k1gNnPc4	slovo
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
Horizonty	horizont	k1gInPc1	horizont
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
získaly	získat	k5eAaPmAgInP	získat
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Hip	hip	k0	hip
Hop	hop	k0	hop
a	a	k8xC	a
RnB	RnB	k1gFnPc6	RnB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
založil	založit	k5eAaPmAgInS	založit
Garand	Garand	k1gInSc1	Garand
Brand	Brand	k1gInSc4	Brand
<g/>
,	,	kIx,	,
značku	značka	k1gFnSc4	značka
oblečení	oblečení	k1gNnSc2	oblečení
a	a	k8xC	a
doplňků	doplněk	k1gInPc2	doplněk
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
designem	design	k1gInSc7	design
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
nejznámější	známý	k2eAgFnPc4d3	nejznámější
skladby	skladba	k1gFnPc4	skladba
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Molo	molo	k1gNnSc1	molo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
La	la	k1gNnSc2	la
Familia	Familium	k1gNnSc2	Familium
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Boomerang	Boomerang	k1gInSc1	Boomerang
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Tuláci	tulák	k1gMnPc1	tulák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hladina	hladina	k1gFnSc1	hladina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pavučina	pavučina	k1gFnSc1	pavučina
lží	lež	k1gFnPc2	lež
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
L.B.C.	L.B.C.	k1gFnSc1	L.B.C.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Play	play	k0	play
<g/>
"	"	kIx"	"
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2017	[number]	k4	2017
vydal	vydat	k5eAaPmAgMnS	vydat
sólovou	sólový	k2eAgFnSc4d1	sólová
desku	deska	k1gFnSc4	deska
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
vypustil	vypustit	k5eAaPmAgInS	vypustit
čtyři	čtyři	k4xCgInPc4	čtyři
singly	singl	k1gInPc4	singl
-	-	kIx~	-
"	"	kIx"	"
<g/>
Play	play	k0	play
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Fiedlerski	Fiedlerski	k1gNnSc1	Fiedlerski
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Neony	neon	k1gInPc4	neon
<g/>
"	"	kIx"	"
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Miris	Miris	k1gFnSc1	Miris
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Kenny	Kenn	k1gInPc1	Kenn
Rough	Rougha	k1gFnPc2	Rougha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nirvana	Nirvan	k1gMnSc4	Nirvan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Kenny	Kenn	k1gInPc1	Kenn
Rough	Rougha	k1gFnPc2	Rougha
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Bali	Bali	k1gNnSc1	Bali
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Maiky	Maik	k1gInPc1	Maik
Beatz	Beatza	k1gFnPc2	Beatza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klip	klip	k1gInSc1	klip
k	k	k7c3	k
singlu	singl	k1gInSc3	singl
Neony	neon	k1gInPc4	neon
natáčený	natáčený	k2eAgInSc4d1	natáčený
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
Jablonec	Jablonec	k1gInSc1	Jablonec
<g/>
,	,	kIx,	,
Javorník	Javorník	k1gInSc1	Javorník
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
filmem	film	k1gInSc7	film
Drive	drive	k1gInSc1	drive
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
hraje	hrát	k5eAaImIp3nS	hrát
vůz	vůz	k1gInSc1	vůz
Chevrolet	chevrolet	k1gInSc1	chevrolet
Corvette	Corvett	k1gInSc5	Corvett
C3	C3	k1gFnPc3	C3
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
patří	patřit	k5eAaImIp3nP	patřit
přímo	přímo	k6eAd1	přímo
Garandovi	Garandův	k2eAgMnPc1d1	Garandův
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
singlu	singl	k1gInSc3	singl
"	"	kIx"	"
<g/>
Play	play	k0	play
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgMnS	natáčet
na	na	k7c6	na
bývalém	bývalý	k2eAgNnSc6d1	bývalé
vojenském	vojenský	k2eAgNnSc6d1	vojenské
letišti	letiště	k1gNnSc6	letiště
Hradčany	Hradčany	k1gInPc4	Hradčany
u	u	k7c2	u
Mimoně	Mimoň	k1gFnSc2	Mimoň
a	a	k8xC	a
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Hamr	hamr	k1gInSc4	hamr
na	na	k7c6	na
Jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Občanská	občanský	k2eAgFnSc1d1	občanská
angažovanost	angažovanost	k1gFnSc1	angažovanost
==	==	k?	==
</s>
</p>
<p>
<s>
Paulie	Paulie	k1gFnSc1	Paulie
Garand	Garanda	k1gFnPc2	Garanda
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
videoklip	videoklip	k1gInSc4	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Pavučina	pavučina	k1gFnSc1	pavučina
lží	lež	k1gFnSc7	lež
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
chtěl	chtít	k5eAaImAgMnS	chtít
uctít	uctít	k5eAaPmF	uctít
památku	památka	k1gFnSc4	památka
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
architekta	architekt	k1gMnSc2	architekt
Karla	Karel	k1gMnSc2	Karel
Hubáčka	Hubáček	k1gMnSc2	Hubáček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
a	a	k8xC	a
klipu	klip	k1gInSc2	klip
odkazoval	odkazovat	k5eAaImAgInS	odkazovat
například	například	k6eAd1	například
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
žlutej	žlutat	k5eAaImRp2nS	žlutat
úl	úl	k1gInSc1	úl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	léto	k1gNnPc7	léto
zbouraný	zbouraný	k2eAgInSc1d1	zbouraný
Hubáčkův	Hubáčkův	k2eAgInSc1d1	Hubáčkův
Obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
Ještěd	Ještěd	k1gInSc1	Ještěd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Básníci	básník	k1gMnPc1	básník
před	před	k7c7	před
mikrofonem	mikrofon	k1gInSc7	mikrofon
(	(	kIx(	(
<g/>
Paulie	Paulie	k1gFnSc1	Paulie
Garand	Garanda	k1gFnPc2	Garanda
&	&	k?	&
Lipo	Lipo	k1gMnSc1	Lipo
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Slova	slovo	k1gNnPc4	slovo
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
A.	A.	kA	A.
<g/>
trick	tricka	k1gFnPc2	tricka
records	recordsa	k1gFnPc2	recordsa
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Madam	madam	k1gFnSc1	madam
poezie	poezie	k1gFnSc2	poezie
</s>
</p>
<p>
<s>
Jarní	jarní	k2eAgInPc4d1	jarní
květy	květ	k1gInPc4	květ
</s>
</p>
<p>
<s>
Za	za	k7c7	za
hranou	hrana	k1gFnSc7	hrana
stolu	stol	k1gInSc2	stol
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
nemá	mít	k5eNaImIp3nS	mít
co	co	k3yQnSc4	co
by	by	kYmCp3nS	by
daroval	darovat	k5eAaPmAgMnS	darovat
</s>
</p>
<p>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
sutin	sutina	k1gFnPc2	sutina
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
</s>
</p>
<p>
<s>
Nemilostná	milostný	k2eNgFnSc1d1	nemilostná
hrana	hrana	k1gFnSc1	hrana
</s>
</p>
<p>
<s>
Zbyl	zbýt	k5eAaPmAgMnS	zbýt
mi	já	k3xPp1nSc3	já
jen	jen	k9	jen
život	život	k1gInSc4	život
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
</s>
</p>
<p>
<s>
Rozmluva	rozmluva	k1gFnSc1	rozmluva
</s>
</p>
<p>
<s>
Plameňáci	plameňák	k1gMnPc1	plameňák
</s>
</p>
<p>
<s>
Dvůr	Dvůr	k1gInSc1	Dvůr
plnej	plnej	k?	plnej
listí	listí	k1gNnSc1	listí
</s>
</p>
<p>
<s>
Černý	Černý	k1gMnSc1	Černý
perly	perla	k1gFnSc2	perla
snů	sen	k1gInPc2	sen
</s>
</p>
<p>
<s>
====	====	k?	====
Slova	slovo	k1gNnPc1	slovo
<g/>
:	:	kIx,	:
Remixed	Remixed	k1gInSc1	Remixed
by	by	kYmCp3nP	by
Kenny	Kenna	k1gFnPc1	Kenna
Rough	Rougha	k1gFnPc2	Rougha
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
BPM	BPM	kA	BPM
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Madam	madam	k1gFnSc1	madam
poezie	poezie	k1gFnSc2	poezie
</s>
</p>
<p>
<s>
Jarní	jarní	k2eAgInPc4d1	jarní
květy	květ	k1gInPc4	květ
</s>
</p>
<p>
<s>
Za	za	k7c7	za
hranou	hrana	k1gFnSc7	hrana
stolu	stol	k1gInSc2	stol
</s>
</p>
<p>
<s>
Sonáta	sonáta	k1gFnSc1	sonáta
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
nemá	mít	k5eNaImIp3nS	mít
co	co	k3yQnSc4	co
by	by	kYmCp3nS	by
daroval	darovat	k5eAaPmAgMnS	darovat
</s>
</p>
<p>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
sutin	sutina	k1gFnPc2	sutina
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
</s>
</p>
<p>
<s>
Nemilostná	milostný	k2eNgFnSc1d1	nemilostná
hrana	hrana	k1gFnSc1	hrana
</s>
</p>
<p>
<s>
Zbyl	zbýt	k5eAaPmAgMnS	zbýt
mi	já	k3xPp1nSc3	já
jen	jen	k9	jen
život	život	k1gInSc4	život
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
</s>
</p>
<p>
<s>
Rozmluva	rozmluva	k1gFnSc1	rozmluva
</s>
</p>
<p>
<s>
Plameňáci	plameňák	k1gMnPc1	plameňák
</s>
</p>
<p>
<s>
Dvůr	Dvůr	k1gInSc1	Dvůr
plnej	plnej	k?	plnej
listí	listí	k1gNnSc1	listí
</s>
</p>
<p>
<s>
Černý	Černý	k1gMnSc1	Černý
perly	perla	k1gFnSc2	perla
snů	sen	k1gInPc2	sen
</s>
</p>
<p>
<s>
====	====	k?	====
Horizonty	horizont	k1gInPc1	horizont
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
BPM	BPM	kA	BPM
Records	Records	k1gInSc1	Records
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Chill	Chill	k1gMnSc1	Chill
</s>
</p>
<p>
<s>
Horizonty	horizont	k1gInPc1	horizont
</s>
</p>
<p>
<s>
Doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Kato	Kato	k1gMnSc1	Kato
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hlídky	hlídka	k1gFnPc1	hlídka
noci	noc	k1gFnSc2	noc
</s>
</p>
<p>
<s>
Kavárenský	kavárenský	k2eAgInSc1d1	kavárenský
anonym	anonym	k1gInSc1	anonym
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
Florencie	Florencie	k1gFnSc1	Florencie
-	-	kIx~	-
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Habbiel	Habbiel	k1gInSc1	Habbiel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlci	vlk	k1gMnPc1	vlk
a	a	k8xC	a
lvy	lev	k1gInPc1	lev
</s>
</p>
<p>
<s>
Trhák	trhák	k1gInSc1	trhák
</s>
</p>
<p>
<s>
Příběhy	příběh	k1gInPc1	příběh
z	z	k7c2	z
předměstí	předměstí	k1gNnSc2	předměstí
(	(	kIx(	(
<g/>
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Lyrik	lyrik	k1gMnSc1	lyrik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Heroica	Heroica	k1gMnSc1	Heroica
(	(	kIx(	(
<g/>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elegie	elegie	k1gFnSc1	elegie
</s>
</p>
<p>
<s>
Larry	Larra	k1gFnPc1	Larra
</s>
</p>
<p>
<s>
Černá	Černá	k1gFnSc1	Černá
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
</s>
</p>
<p>
<s>
===	===	k?	===
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Mozoly	mozol	k1gInPc7	mozol
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
FLC	FLC	kA	FLC
Records	Records	k1gInSc1	Records
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Dostih	dostih	k1gInSc1	dostih
</s>
</p>
<p>
<s>
Mozoly	mozol	k1gInPc1	mozol
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Seck	Seck	k6eAd1	Seck
</s>
</p>
<p>
<s>
Obrazy	obraz	k1gInPc1	obraz
skutečnosti	skutečnost	k1gFnSc2	skutečnost
č.	č.	k?	č.
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
Postmoderní	postmoderní	k2eAgInSc1d1	postmoderní
pláč	pláč	k1gInSc1	pláč
feat	feat	k2eAgInSc1d1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Lipo	Lipo	k6eAd1	Lipo
</s>
</p>
<p>
<s>
Loterie	loterie	k1gFnSc1	loterie
</s>
</p>
<p>
<s>
Za	za	k7c4	za
naše	náš	k3xOp1gFnPc4	náš
hodnoty	hodnota	k1gFnPc4	hodnota
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
čase	čas	k1gInSc6	čas
</s>
</p>
<p>
<s>
Mozoly	mozol	k1gInPc1	mozol
RMX	RMX	kA	RMX
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Seck	Seck	k6eAd1	Seck
</s>
</p>
<p>
<s>
A	a	k9	a
jsi	být	k5eAaImIp2nS	být
to	ten	k3xDgNnSc1	ten
ty	ten	k3xDgFnPc4	ten
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
rozpaky	rozpak	k1gInPc7	rozpak
</s>
</p>
<p>
<s>
====	====	k?	====
Harant	Harant	k?	Harant
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
Ty	ty	k3xPp2nSc1	ty
Nikdy	nikdy	k6eAd1	nikdy
Records	Records	k1gInSc4	Records
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Mámin	mámin	k2eAgMnSc1d1	mámin
hodnej	hodnej	k?	hodnej
syn	syn	k1gMnSc1	syn
</s>
</p>
<p>
<s>
Loutky	loutka	k1gFnPc1	loutka
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Oliver	Oliver	k1gInSc1	Oliver
Lowe	Low	k1gFnSc2	Low
</s>
</p>
<p>
<s>
Nonstop	nonstop	k2eAgInSc1d1	nonstop
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Idea	idea	k1gFnSc1	idea
</s>
</p>
<p>
<s>
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
<p>
<s>
Forbidden	Forbiddna	k1gFnPc2	Forbiddna
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Station	station	k1gInSc1	station
Peace	Peace	k1gFnSc2	Peace
</s>
</p>
<p>
<s>
Na	na	k7c4	na
zem	zem	k1gFnSc4	zem
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
<g/>
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
</s>
</p>
<p>
<s>
Nejsme	být	k5eNaImIp1nP	být
nic	nic	k3yNnSc4	nic
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
<g/>
Lipo	Lipo	k6eAd1	Lipo
a	a	k8xC	a
Eki	Eki	k1gFnSc1	Eki
</s>
</p>
<p>
<s>
Harant	Harant	k?	Harant
</s>
</p>
<p>
<s>
Hood	Hood	k1gMnSc1	Hood
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
<g/>
Seck	Seck	k1gMnSc1	Seck
a	a	k8xC	a
Rest	rest	k6eAd1	rest
</s>
</p>
<p>
<s>
Časovaná	časovaný	k2eAgFnSc1d1	časovaná
</s>
</p>
<p>
<s>
Fame	Fame	k6eAd1	Fame
</s>
</p>
<p>
<s>
H8	H8	k4	H8
</s>
</p>
<p>
<s>
Imprese	imprese	k1gFnSc1	imprese
ulic	ulice	k1gFnPc2	ulice
</s>
</p>
<p>
<s>
Kotě	kotě	k1gNnSc1	kotě
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
<g/>
Eki	Eki	k1gMnSc1	Eki
</s>
</p>
<p>
<s>
===	===	k?	===
Skladby	skladba	k1gFnPc4	skladba
umístnené	umístnený	k2eAgFnPc4d1	umístnený
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Pauliem	Paulium	k1gNnSc7	Paulium
Garandem	Garando	k1gNnSc7	Garando
na	na	k7c4	na
www.hiphopstage.cz	www.hiphopstage.cz	k1gInSc4	www.hiphopstage.cz
</s>
</p>
<p>
<s>
Paulie	Paulie	k1gFnSc1	Paulie
Garand	Garanda	k1gFnPc2	Garanda
na	na	k7c4	na
www.showbiz.cz	www.showbiz.cz	k1gInSc4	www.showbiz.cz
</s>
</p>
<p>
<s>
Musicweb	Musicwba	k1gFnPc2	Musicwba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
o	o	k7c6	o
připravované	připravovaný	k2eAgFnSc6d1	připravovaná
desce	deska	k1gFnSc6	deska
Paulieho	Paulie	k1gMnSc2	Paulie
Garanda	Garando	k1gNnSc2	Garando
</s>
</p>
<p>
<s>
Radio	radio	k1gNnSc1	radio
Wave	Wav	k1gInSc2	Wav
o	o	k7c6	o
druhé	druhý	k4xOgFnSc6	druhý
desce	deska	k1gFnSc6	deska
Paulieho	Paulie	k1gMnSc2	Paulie
Garanda	Garando	k1gNnSc2	Garando
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
Ty	ty	k3xPp2nSc5	ty
Nikdy	nikdy	k6eAd1	nikdy
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
Garand	Garanda	k1gFnPc2	Garanda
Brand	Branda	k1gFnPc2	Branda
</s>
</p>
<p>
<s>
Paulie	Paulie	k1gFnSc1	Paulie
Garand	Garanda	k1gFnPc2	Garanda
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Paulie	Paulie	k1gFnSc1	Paulie
Garand	Garanda	k1gFnPc2	Garanda
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Paulie	Paulie	k1gFnSc1	Paulie
Garand	Garanda	k1gFnPc2	Garanda
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
