<s>
První	první	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
byla	být	k5eAaImAgFnS
vyhlášena	vyhlášen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1095	[number]	k4
na	na	k7c6
clermontském	clermontský	k2eAgInSc6d1
koncilu	koncil	k1gInSc6
papežem	papež	k1gMnSc7
Urbanem	Urban	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
směřovala	směřovat	k5eAaImAgFnS
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
za	za	k7c4
znovudobytí	znovudobytí	k1gNnSc4
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
od	od	k7c2
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>