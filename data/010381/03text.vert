<p>
<s>
První	první	k4xOgFnSc1	první
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
roku	rok	k1gInSc2	rok
1095	[number]	k4	1095
na	na	k7c6	na
clermontském	clermontský	k2eAgInSc6d1	clermontský
koncilu	koncil	k1gInSc6	koncil
papežem	papež	k1gMnSc7	papež
Urbanem	Urban	k1gMnSc7	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
směřovala	směřovat	k5eAaImAgFnS	směřovat
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
za	za	k7c4	za
znovudobytí	znovudobytí	k1gNnSc4	znovudobytí
Božího	boží	k2eAgInSc2d1	boží
hrobu	hrob	k1gInSc2	hrob
od	od	k7c2	od
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
výpravy	výprava	k1gFnSc2	výprava
bylo	být	k5eAaImAgNnS	být
částečně	částečně	k6eAd1	částečně
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c4	na
postupné	postupný	k2eAgNnSc4d1	postupné
pronikání	pronikání	k1gNnSc4	pronikání
tureckých	turecký	k2eAgMnPc2d1	turecký
nájezdníků	nájezdník	k1gMnPc2	nájezdník
do	do	k7c2	do
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
Byzantské	byzantský	k2eAgFnSc6d1	byzantská
říši	říš	k1gFnSc6	říš
dobyli	dobýt	k5eAaPmAgMnP	dobýt
značná	značný	k2eAgNnPc4d1	značné
území	území	k1gNnPc4	území
a	a	k8xC	a
oslabili	oslabit	k5eAaPmAgMnP	oslabit
moc	moc	k1gFnSc4	moc
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Alexios	Alexios	k1gMnSc1	Alexios
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
papeže	papež	k1gMnSc4	papež
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
získat	získat	k5eAaPmF	získat
dobrovolníky	dobrovolník	k1gMnPc4	dobrovolník
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
žoldnéřské	žoldnéřský	k2eAgFnSc2d1	žoldnéřská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
ztracená	ztracený	k2eAgNnPc4d1	ztracené
území	území	k1gNnSc4	území
na	na	k7c6	na
východě	východ	k1gInSc6	východ
znovu	znovu	k6eAd1	znovu
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
výpravy	výprava	k1gFnSc2	výprava
bylo	být	k5eAaImAgNnS	být
zároveň	zároveň	k6eAd1	zároveň
motivováno	motivovat	k5eAaBmNgNnS	motivovat
snahou	snaha	k1gFnSc7	snaha
papeže	papež	k1gMnSc2	papež
Urbana	Urban	k1gMnSc2	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
o	o	k7c4	o
zmírnění	zmírnění	k1gNnSc4	zmírnění
schizmatu	schizma	k1gNnSc2	schizma
mezi	mezi	k7c7	mezi
západní	západní	k2eAgFnSc7d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Urban	Urban	k1gMnSc1	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
zároveň	zároveň	k6eAd1	zároveň
mohl	moct	k5eAaImAgInS	moct
využít	využít	k5eAaPmF	využít
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
výpravy	výprava	k1gFnSc2	výprava
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgInS	získat
navrch	navrch	k6eAd1	navrch
nad	nad	k7c7	nad
císařem	císař	k1gMnSc7	císař
Jindřichem	Jindřich	k1gMnSc7	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgMnSc7	svůj
protivníkem	protivník	k1gMnSc7	protivník
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
investituru	investitura	k1gFnSc4	investitura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Papežův	Papežův	k2eAgInSc1d1	Papežův
apel	apel	k1gInSc1	apel
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
východním	východní	k2eAgMnPc3d1	východní
křesťanům	křesťan	k1gMnPc3	křesťan
se	se	k3xPyFc4	se
zvrtl	zvrtnout	k5eAaPmAgMnS	zvrtnout
ve	v	k7c6	v
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
svaté	svatý	k2eAgFnSc2d1	svatá
války	válka	k1gFnSc2	válka
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
svatých	svatý	k2eAgNnPc2d1	svaté
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
i	i	k9	i
mezi	mezi	k7c7	mezi
evropskými	evropský	k2eAgMnPc7d1	evropský
venkovany	venkovan	k1gMnPc7	venkovan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1096	[number]	k4	1096
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
podněcováni	podněcovat	k5eAaImNgMnP	podněcovat
lidovými	lidový	k2eAgMnPc7d1	lidový
kazateli	kazatel	k1gMnPc7	kazatel
<g/>
,	,	kIx,	,
dorazili	dorazit	k5eAaPmAgMnP	dorazit
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Francie	Francie	k1gFnSc2	Francie
až	až	k9	až
do	do	k7c2	do
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
plenili	plenit	k5eAaImAgMnP	plenit
a	a	k8xC	a
loupili	loupit	k5eAaImAgMnP	loupit
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Alexios	Alexios	k1gMnSc1	Alexios
byl	být	k5eAaImAgMnS	být
zklamán	zklamat	k5eAaPmNgMnS	zklamat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tito	tento	k3xDgMnPc1	tento
poutníci	poutník	k1gMnPc1	poutník
nebyli	být	k5eNaImAgMnP	být
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc4	jaký
očekával	očekávat	k5eAaImAgInS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
rychle	rychle	k6eAd1	rychle
přepravit	přepravit	k5eAaPmF	přepravit
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začali	začít	k5eAaPmAgMnP	začít
pořádat	pořádat	k5eAaImF	pořádat
nájezdy	nájezd	k1gInPc4	nájezd
na	na	k7c4	na
turecká	turecký	k2eAgNnPc4d1	turecké
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
však	však	k9	však
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
odvetnou	odvetný	k2eAgFnSc4d1	odvetná
výpravu	výprava	k1gFnSc4	výprava
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
křižáky	křižák	k1gInPc4	křižák
pobili	pobít	k5eAaPmAgMnP	pobít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
shromáždily	shromáždit	k5eAaPmAgInP	shromáždit
další	další	k2eAgInPc1d1	další
chudinské	chudinský	k2eAgInPc1d1	chudinský
houfy	houf	k1gInPc1	houf
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
začaly	začít	k5eAaPmAgInP	začít
pořádat	pořádat	k5eAaImF	pořádat
židovské	židovský	k2eAgInPc1d1	židovský
pogromy	pogrom	k1gInPc1	pogrom
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc4d1	německý
křižáky	křižák	k1gInPc4	křižák
však	však	k9	však
dal	dát	k5eAaPmAgInS	dát
už	už	k6eAd1	už
na	na	k7c6	na
uherských	uherský	k2eAgFnPc6d1	uherská
hranicích	hranice	k1gFnPc6	hranice
rozehnat	rozehnat	k5eAaPmF	rozehnat
král	král	k1gMnSc1	král
Koloman	Koloman	k1gMnSc1	Koloman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1096	[number]	k4	1096
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
rytířská	rytířský	k2eAgNnPc1d1	rytířské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc7	jejich
veliteli	velitel	k1gMnPc7	velitel
byli	být	k5eAaImAgMnP	být
především	především	k9	především
francouzští	francouzský	k2eAgMnPc1d1	francouzský
a	a	k8xC	a
normanští	normanský	k2eAgMnPc1d1	normanský
velmoži	velmož	k1gMnPc1	velmož
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1096	[number]	k4	1096
<g/>
–	–	k?	–
<g/>
1097	[number]	k4	1097
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
složili	složit	k5eAaPmAgMnP	složit
lenní	lenní	k2eAgFnSc4d1	lenní
přísahu	přísaha	k1gFnSc4	přísaha
císaři	císař	k1gMnSc3	císař
Alexiovi	Alexius	k1gMnSc3	Alexius
<g/>
.	.	kIx.	.
</s>
<s>
Přísahou	přísaha	k1gFnSc7	přísaha
se	se	k3xPyFc4	se
křižáci	křižák	k1gMnPc1	křižák
zavázali	zavázat	k5eAaPmAgMnP	zavázat
navrátit	navrátit	k5eAaPmF	navrátit
všechna	všechen	k3xTgNnPc4	všechen
území	území	k1gNnPc4	území
zpět	zpět	k6eAd1	zpět
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
měli	mít	k5eAaImAgMnP	mít
slíbený	slíbený	k2eAgInSc4d1	slíbený
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
kořisti	kořist	k1gFnPc4	kořist
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
během	během	k7c2	během
výpravy	výprava	k1gFnSc2	výprava
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Nikáju	Nikáju	k1gFnSc4	Nikáju
a	a	k8xC	a
porazili	porazit	k5eAaPmAgMnP	porazit
Turky	turek	k1gInPc4	turek
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Dorylaea	Dorylae	k1gInSc2	Dorylae
<g/>
,	,	kIx,	,
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Edessu	Edessa	k1gFnSc4	Edessa
a	a	k8xC	a
Antiochii	Antiochie	k1gFnSc3	Antiochie
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1099	[number]	k4	1099
i	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
cíl	cíl	k1gInSc1	cíl
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
–	–	k?	–
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
své	svůj	k3xOyFgInPc4	svůj
závazky	závazek	k1gInPc4	závazek
vůči	vůči	k7c3	vůči
Byzantskému	byzantský	k2eAgMnSc3d1	byzantský
císaři	císař	k1gMnSc3	císař
nedodrželi	dodržet	k5eNaPmAgMnP	dodržet
<g/>
,	,	kIx,	,
nevrátili	vrátit	k5eNaPmAgMnP	vrátit
mu	on	k3xPp3gMnSc3	on
zpět	zpět	k6eAd1	zpět
kdysi	kdysi	k6eAd1	kdysi
byzantská	byzantský	k2eAgNnPc1d1	byzantské
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
zakládali	zakládat	k5eAaImAgMnP	zakládat
své	svůj	k3xOyFgInPc4	svůj
křižácké	křižácký	k2eAgInPc4d1	křižácký
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
budované	budovaný	k2eAgInPc4d1	budovaný
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
západoevropských	západoevropský	k2eAgFnPc2d1	západoevropská
feudálních	feudální	k2eAgFnPc2d1	feudální
monarchií	monarchie	k1gFnPc2	monarchie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
generace	generace	k1gFnPc4	generace
stalo	stát	k5eAaPmAgNnS	stát
ohniskem	ohnisko	k1gNnSc7	ohnisko
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
Byzantinci	Byzantinec	k1gMnPc7	Byzantinec
a	a	k8xC	a
západními	západní	k2eAgMnPc7d1	západní
Franky	Frank	k1gMnPc7	Frank
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
usadili	usadit	k5eAaPmAgMnP	usadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolnosti	okolnost	k1gFnSc3	okolnost
svolání	svolání	k1gNnSc2	svolání
výpravy	výprava	k1gFnSc2	výprava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začaly	začít	k5eAaPmAgFnP	začít
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
na	na	k7c4	na
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
pronikat	pronikat	k5eAaImF	pronikat
turecké	turecký	k2eAgInPc4d1	turecký
kmeny	kmen	k1gInPc4	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
drancovaly	drancovat	k5eAaImAgFnP	drancovat
civilizované	civilizovaný	k2eAgFnPc4d1	civilizovaná
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Džezíře	Džezíra	k1gFnSc6	Džezíra
se	se	k3xPyFc4	se
Turci	Turek	k1gMnPc1	Turek
nechali	nechat	k5eAaPmAgMnP	nechat
islamizovat	islamizovat	k5eAaBmF	islamizovat
a	a	k8xC	a
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
bagdádského	bagdádský	k2eAgMnSc2d1	bagdádský
chalífy	chalífa	k1gMnSc2	chalífa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgMnS	stát
jen	jen	k9	jen
loutkou	loutka	k1gFnSc7	loutka
závislou	závislý	k2eAgFnSc7d1	závislá
na	na	k7c4	na
vůli	vůle	k1gFnSc4	vůle
svých	svůj	k3xOyFgMnPc2	svůj
tureckých	turecký	k2eAgMnPc2d1	turecký
vezírů	vezír	k1gMnPc2	vezír
a	a	k8xC	a
rádců	rádce	k1gMnPc2	rádce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Abbásovců	Abbásovec	k1gMnPc2	Abbásovec
začali	začít	k5eAaPmAgMnP	začít
Turci	Turek	k1gMnPc1	Turek
útočit	útočit	k5eAaImF	útočit
na	na	k7c6	na
území	území	k1gNnSc6	území
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgNnPc1d1	turecké
vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgNnP	skládat
především	především	k9	především
z	z	k7c2	z
lehkých	lehký	k2eAgMnPc2d1	lehký
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	on	k3xPp3gInPc4	on
činilo	činit	k5eAaImAgNnS	činit
velmi	velmi	k6eAd1	velmi
mobilními	mobilní	k2eAgInPc7d1	mobilní
a	a	k8xC	a
byzantská	byzantský	k2eAgFnSc1d1	byzantská
armáda	armáda	k1gFnSc1	armáda
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
dostihla	dostihnout	k5eAaPmAgFnS	dostihnout
a	a	k8xC	a
utkala	utkat	k5eAaPmAgFnS	utkat
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Roman	Roman	k1gMnSc1	Roman
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Diogenes	Diogenes	k1gMnSc1	Diogenes
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
armádou	armáda	k1gFnSc7	armáda
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
svést	svést	k5eAaPmF	svést
s	s	k7c7	s
Turky	Turek	k1gMnPc7	Turek
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Císařova	Císařův	k2eAgFnSc1d1	Císařova
stotisícová	stotisícový	k2eAgFnSc1d1	stotisícová
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
s	s	k7c7	s
tureckým	turecký	k2eAgMnSc7d1	turecký
sultánem	sultán	k1gMnSc7	sultán
Alpazslanem	Alpazslan	k1gMnSc7	Alpazslan
utkala	utkat	k5eAaPmAgFnS	utkat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Mantzikertu	Mantzikert	k1gInSc2	Mantzikert
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
poražena	poražen	k2eAgFnSc1d1	poražena
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říš	k1gFnPc1	říš
se	se	k3xPyFc4	se
porážkou	porážka	k1gFnSc7	porážka
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
do	do	k7c2	do
chaosu	chaos	k1gInSc2	chaos
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Turci	Turek	k1gMnPc1	Turek
tak	tak	k6eAd1	tak
mohli	moct	k5eAaImAgMnP	moct
vtrhnout	vtrhnout	k5eAaPmF	vtrhnout
do	do	k7c2	do
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
,	,	kIx,	,
centra	centrum	k1gNnSc2	centrum
byzantské	byzantský	k2eAgFnSc2d1	byzantská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
mohl	moct	k5eAaImAgMnS	moct
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Turecké	turecký	k2eAgInPc1d1	turecký
kmeny	kmen	k1gInPc1	kmen
celou	celý	k2eAgFnSc4d1	celá
Anatolii	Anatolie	k1gFnSc4	Anatolie
vydrancovaly	vydrancovat	k5eAaPmAgFnP	vydrancovat
a	a	k8xC	a
spálily	spálit	k5eAaPmAgFnP	spálit
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
části	část	k1gFnSc2	část
Byzance	Byzanc	k1gFnSc2	Byzanc
stala	stát	k5eAaPmAgFnS	stát
prakticky	prakticky	k6eAd1	prakticky
poušť	poušť	k1gFnSc1	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
porážky	porážka	k1gFnSc2	porážka
u	u	k7c2	u
Manzikertu	Manzikert	k1gInSc2	Manzikert
se	se	k3xPyFc4	se
císařství	císařství	k1gNnSc1	císařství
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
plně	plně	k6eAd1	plně
nevzpamatovalo	vzpamatovat	k5eNaPmAgNnS	vzpamatovat
<g/>
.	.	kIx.	.
<g/>
Císař	Císař	k1gMnSc1	Císař
Alexios	Alexios	k1gMnSc1	Alexios
I.	I.	kA	I.
Komnenos	Komnenos	k1gMnSc1	Komnenos
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
byzantský	byzantský	k2eAgInSc4d1	byzantský
trůn	trůn	k1gInSc4	trůn
roku	rok	k1gInSc2	rok
1081	[number]	k4	1081
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
Anatolii	Anatolie	k1gFnSc3	Anatolie
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chybělo	chybět	k5eAaImAgNnS	chybět
mu	on	k3xPp3gNnSc3	on
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byzantští	byzantský	k2eAgMnPc1d1	byzantský
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
často	často	k6eAd1	často
rekrutovali	rekrutovat	k5eAaImAgMnP	rekrutovat
právě	právě	k9	právě
z	z	k7c2	z
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
.	.	kIx.	.
</s>
<s>
Obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
na	na	k7c4	na
západní	západní	k2eAgNnSc4d1	západní
křesťanstvo	křesťanstvo	k1gNnSc4	křesťanstvo
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
papeže	papež	k1gMnSc4	papež
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
osvobodit	osvobodit	k5eAaPmF	osvobodit
východní	východní	k2eAgMnPc4d1	východní
křesťany	křesťan	k1gMnPc4	křesťan
z	z	k7c2	z
jha	jho	k1gNnSc2	jho
tureckých	turecký	k2eAgMnPc2d1	turecký
barbarů	barbar	k1gMnPc2	barbar
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
muslimové	muslim	k1gMnPc1	muslim
okupují	okupovat	k5eAaBmIp3nP	okupovat
svatá	svatý	k2eAgNnPc4d1	svaté
místa	místo	k1gNnPc4	místo
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
nelze	lze	k6eNd1	lze
jen	jen	k9	jen
nečinně	činně	k6eNd1	činně
přihlížet	přihlížet	k5eAaImF	přihlížet
<g/>
.	.	kIx.	.
</s>
<s>
Alexiovi	Alexius	k1gMnSc3	Alexius
však	však	k9	však
šlo	jít	k5eAaImAgNnS	jít
jen	jen	k9	jen
o	o	k7c4	o
znovuzískání	znovuzískání	k1gNnSc4	znovuzískání
Anatolie	Anatolie	k1gFnSc2	Anatolie
a	a	k8xC	a
svatá	svatý	k2eAgNnPc1d1	svaté
místa	místo	k1gNnPc1	místo
ho	on	k3xPp3gMnSc4	on
zajímala	zajímat	k5eAaImAgFnS	zajímat
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
pokud	pokud	k8xS	pokud
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
Byzanc	Byzanc	k1gFnSc1	Byzanc
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
<g/>
Papež	Papež	k1gMnSc1	Papež
Urban	Urban	k1gMnSc1	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rovněž	rovněž	k9	rovněž
po	po	k7c6	po
velkém	velký	k2eAgNnSc6d1	velké
schismatu	schisma	k1gNnSc6	schisma
chtěl	chtít	k5eAaImAgMnS	chtít
zlepšit	zlepšit	k5eAaPmF	zlepšit
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
západní	západní	k2eAgFnSc7d1	západní
a	a	k8xC	a
ortodoxní	ortodoxní	k2eAgFnSc7d1	ortodoxní
církví	církev	k1gFnSc7	církev
<g/>
,	,	kIx,	,
udržoval	udržovat	k5eAaImAgMnS	udržovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1090	[number]	k4	1090
čilou	čilý	k2eAgFnSc4d1	čilá
diplomacii	diplomacie	k1gFnSc4	diplomacie
s	s	k7c7	s
byzantským	byzantský	k2eAgMnSc7d1	byzantský
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Alexios	Alexios	k1gInSc1	Alexios
roku	rok	k1gInSc2	rok
1095	[number]	k4	1095
vyslal	vyslat	k5eAaPmAgMnS	vyslat
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
na	na	k7c4	na
Urbanův	Urbanův	k2eAgInSc4d1	Urbanův
koncil	koncil	k1gInSc4	koncil
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Piacenze	Piacenza	k1gFnSc6	Piacenza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Byzantinci	Byzantinec	k1gMnPc1	Byzantinec
vylíčili	vylíčit	k5eAaPmAgMnP	vylíčit
utrpení	utrpení	k1gNnSc4	utrpení
východních	východní	k2eAgMnPc2d1	východní
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
zdůrazňovali	zdůrazňovat	k5eAaImAgMnP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
turecká	turecký	k2eAgFnSc1d1	turecká
hrozba	hrozba	k1gFnSc1	hrozba
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
jen	jen	k9	jen
Byzance	Byzanc	k1gFnPc4	Byzanc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
i	i	k9	i
západní	západní	k2eAgInSc4d1	západní
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přítomní	přítomný	k2eAgMnPc1d1	přítomný
západní	západní	k2eAgMnPc1d1	západní
biskupové	biskup	k1gMnPc1	biskup
byli	být	k5eAaImAgMnP	být
otřeseni	otřást	k5eAaPmNgMnP	otřást
<g/>
,	,	kIx,	,
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Urban	Urban	k1gMnSc1	Urban
začal	začít	k5eAaPmAgMnS	začít
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
žádosti	žádost	k1gFnSc6	žádost
vážně	vážně	k6eAd1	vážně
uvažovat	uvažovat	k5eAaImF	uvažovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Motivace	motivace	k1gFnSc1	motivace
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
výpravy	výprava	k1gFnSc2	výprava
a	a	k8xC	a
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
==	==	k?	==
</s>
</p>
<p>
<s>
Urbanova	Urbanův	k2eAgFnSc1d1	Urbanova
snaha	snaha	k1gFnSc1	snaha
nebyla	být	k5eNaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
pouze	pouze	k6eAd1	pouze
upřímnou	upřímný	k2eAgFnSc7d1	upřímná
snahou	snaha	k1gFnSc7	snaha
pomoci	pomoct	k5eAaPmF	pomoct
schizmatickým	schizmatický	k2eAgMnPc3d1	schizmatický
ortodoxním	ortodoxní	k2eAgMnPc3d1	ortodoxní
Byzantincům	Byzantinec	k1gMnPc3	Byzantinec
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
složitých	složitý	k2eAgFnPc2d1	složitá
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
úvah	úvaha	k1gFnPc2	úvaha
a	a	k8xC	a
pečlivé	pečlivý	k2eAgFnSc2d1	pečlivá
politické	politický	k2eAgFnSc2d1	politická
přípravy	příprava	k1gFnSc2	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Urban	Urban	k1gMnSc1	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgMnS	být
opatem	opat	k1gMnSc7	opat
kláštera	klášter	k1gInSc2	klášter
v	v	k7c4	v
Cluny	Clun	k1gMnPc4	Clun
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
mniši	mnich	k1gMnPc1	mnich
byli	být	k5eAaImAgMnP	být
nejenom	nejenom	k6eAd1	nejenom
průkopníky	průkopník	k1gMnPc4	průkopník
církevní	církevní	k2eAgFnSc2d1	církevní
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zkušenými	zkušený	k2eAgInPc7d1	zkušený
organizátory	organizátor	k1gInPc7	organizátor
poutí	pouť	k1gFnPc2	pouť
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
dobrými	dobrý	k2eAgMnPc7d1	dobrý
znalci	znalec	k1gMnPc7	znalec
poměrů	poměr	k1gInPc2	poměr
panujících	panující	k2eAgInPc2d1	panující
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
minulosti	minulost	k1gFnSc2	minulost
také	také	k9	také
dobře	dobře	k6eAd1	dobře
rozuměl	rozumět	k5eAaImAgInS	rozumět
podmínkám	podmínka	k1gFnPc3	podmínka
a	a	k8xC	a
zájmům	zájem	k1gInPc3	zájem
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
při	při	k7c6	při
propagaci	propagace	k1gFnSc6	propagace
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
soustředil	soustředit	k5eAaPmAgMnS	soustředit
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
výzvu	výzva	k1gFnSc4	výzva
také	také	k9	také
konzultoval	konzultovat	k5eAaImAgMnS	konzultovat
s	s	k7c7	s
několika	několik	k4yIc7	několik
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
šlechtici	šlechtic	k1gMnPc7	šlechtic
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
feudálních	feudální	k2eAgMnPc2d1	feudální
pánů	pan	k1gMnPc2	pan
Provence	Provence	k1gFnSc2	Provence
<g/>
,	,	kIx,	,
Raimondem	Raimond	k1gInSc7	Raimond
ze	z	k7c2	z
Saint	Sainta	k1gFnPc2	Sainta
Gilles	Gillesa	k1gFnPc2	Gillesa
<g/>
,	,	kIx,	,
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Toulouse	Toulouse	k1gInSc2	Toulouse
<g/>
.	.	kIx.	.
</s>
<s>
Předem	předem	k6eAd1	předem
vybral	vybrat	k5eAaPmAgMnS	vybrat
biskupa	biskup	k1gMnSc4	biskup
Adhémara	Adhémar	k1gMnSc4	Adhémar
z	z	k7c2	z
Le	Le	k1gFnSc2	Le
Puy	Puy	k1gMnSc2	Puy
jako	jako	k8xS	jako
papežského	papežský	k2eAgMnSc2d1	papežský
legáta	legát	k1gMnSc2	legát
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
výpravě	výprava	k1gFnSc6	výprava
zastupovat	zastupovat	k5eAaImF	zastupovat
<g/>
.	.	kIx.	.
</s>
<s>
Žádost	žádost	k1gFnSc1	žádost
byzantského	byzantský	k2eAgMnSc2d1	byzantský
císaře	císař	k1gMnSc2	císař
Alexia	Alexium	k1gNnSc2	Alexium
I.	I.	kA	I.
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
nevěřícím	věřící	k2eNgInPc3d1	nevěřící
Turkům	turek	k1gInPc3	turek
<g/>
,	,	kIx,	,
umožnila	umožnit	k5eAaPmAgFnS	umožnit
papeži	papež	k1gMnSc3	papež
vyzvat	vyzvat	k5eAaPmF	vyzvat
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kvůli	kvůli	k7c3	kvůli
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
nevěřícím	věřící	k2eNgInPc3d1	nevěřící
ukončili	ukončit	k5eAaPmAgMnP	ukončit
věčné	věčný	k2eAgInPc4d1	věčný
spory	spor	k1gInPc4	spor
a	a	k8xC	a
zastavili	zastavit	k5eAaPmAgMnP	zastavit
války	válka	k1gFnPc4	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
nadřadit	nadřadit	k5eAaPmF	nadřadit
tak	tak	k6eAd1	tak
obecně	obecně	k6eAd1	obecně
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
zájem	zájem	k1gInSc1	zájem
nad	nad	k7c4	nad
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
zájmy	zájem	k1gInPc4	zájem
světských	světský	k2eAgMnPc2d1	světský
panovníků	panovník	k1gMnPc2	panovník
a	a	k8xC	a
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
další	další	k2eAgInSc1d1	další
krok	krok	k1gInSc1	krok
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
podřízení	podřízení	k1gNnSc3	podřízení
světské	světský	k2eAgFnSc2d1	světská
moci	moc	k1gFnSc2	moc
církevní	církevní	k2eAgInSc1d1	církevní
autoritě	autorita	k1gFnSc3	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
dobře	dobře	k6eAd1	dobře
bylo	být	k5eAaImAgNnS	být
papežovo	papežův	k2eAgNnSc1d1	papežovo
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
Clermontu	Clermont	k1gInSc6	Clermont
připraveno	připravit	k5eAaPmNgNnS	připravit
<g/>
,	,	kIx,	,
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
bezprostřední	bezprostřední	k2eAgFnSc1d1	bezprostřední
odezva	odezva	k1gFnSc1	odezva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
u	u	k7c2	u
několika	několik	k4yIc2	několik
významných	významný	k2eAgMnPc2d1	významný
šlechticů	šlechtic	k1gMnPc2	šlechtic
a	a	k8xC	a
církevních	církevní	k2eAgMnPc2d1	církevní
prelátů	prelát	k1gMnPc2	prelát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Urbanovy	Urbanův	k2eAgFnSc2d1	Urbanova
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
propukli	propuknout	k5eAaPmAgMnP	propuknout
shromáždění	shromáždění	k1gNnSc4	shromáždění
duchovní	duchovní	k2eAgMnPc1d1	duchovní
a	a	k8xC	a
velmoži	velmož	k1gMnPc1	velmož
v	v	k7c6	v
nadšení	nadšení	k1gNnSc6	nadšení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
projevovali	projevovat	k5eAaImAgMnP	projevovat
výkřiky	výkřik	k1gInPc4	výkřik
"	"	kIx"	"
<g/>
Deus	Deus	k1gInSc1	Deus
le	le	k?	le
volt	volt	k1gInSc1	volt
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
to	ten	k3xDgNnSc4	ten
chce	chtít	k5eAaImIp3nS	chtít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Urbanova	Urbanův	k2eAgFnSc1d1	Urbanova
výzva	výzva	k1gFnSc1	výzva
v	v	k7c6	v
Clermontu	Clermont	k1gMnSc6	Clermont
<g/>
,	,	kIx,	,
adresována	adresován	k2eAgFnSc1d1	adresována
především	především	k9	především
jihofrancouzské	jihofrancouzský	k2eAgFnSc3d1	jihofrancouzská
šlechtě	šlechta	k1gFnSc3	šlechta
a	a	k8xC	a
kázána	kázán	k2eAgFnSc1d1	kázána
biskupy	biskup	k1gInPc7	biskup
a	a	k8xC	a
mnichy	mnich	k1gInPc7	mnich
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
francouzští	francouzský	k2eAgMnPc1d1	francouzský
velmoži	velmož	k1gMnPc1	velmož
přislíbili	přislíbit	k5eAaPmAgMnP	přislíbit
účast	účast	k1gFnSc4	účast
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obrovské	obrovský	k2eAgFnSc3d1	obrovská
spontánní	spontánní	k2eAgFnSc3d1	spontánní
odezvě	odezva	k1gFnSc3	odezva
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
nižších	nízký	k2eAgFnPc2d2	nižší
vrstev	vrstva	k1gFnPc2	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
podněcovaných	podněcovaný	k2eAgInPc2d1	podněcovaný
fanatickým	fanatický	k2eAgMnSc7d1	fanatický
kazatelem	kazatel	k1gMnSc7	kazatel
Petrem	Petr	k1gMnSc7	Petr
z	z	k7c2	z
Amiensu	Amiens	k1gInSc2	Amiens
<g/>
,	,	kIx,	,
zvaným	zvaný	k2eAgInSc7d1	zvaný
podle	podle	k7c2	podle
oděvu	oděv	k1gInSc2	oděv
Poustevník	poustevník	k1gMnSc1	poustevník
<g/>
,	,	kIx,	,
či	či	k8xC	či
podle	podle	k7c2	podle
postavy	postava	k1gFnSc2	postava
Malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Poustevník	poustevník	k1gMnSc1	poustevník
byl	být	k5eAaImAgMnS	být
odpadlý	odpadlý	k2eAgMnSc1d1	odpadlý
francouzský	francouzský	k2eAgMnSc1d1	francouzský
mnich	mnich	k1gMnSc1	mnich
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
tvrzení	tvrzení	k1gNnSc2	tvrzení
již	již	k9	již
o	o	k7c4	o
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
pokusil	pokusit	k5eAaPmAgMnS	pokusit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
cestě	cesta	k1gFnSc6	cesta
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
Turci	Turek	k1gMnPc1	Turek
špatně	špatně	k6eAd1	špatně
naložili	naložit	k5eAaPmAgMnP	naložit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vyhostili	vyhostit	k5eAaPmAgMnP	vyhostit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
nikdy	nikdy	k6eAd1	nikdy
nezapomněl	zapomnět	k5eNaImAgMnS	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
charismatický	charismatický	k2eAgMnSc1d1	charismatický
demagog	demagog	k1gMnSc1	demagog
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
posluchači	posluchač	k1gMnPc1	posluchač
jej	on	k3xPp3gMnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
svatého	svatý	k2eAgMnSc4d1	svatý
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Prostí	prostý	k2eAgMnPc1d1	prostý
venkované	venkovan	k1gMnPc1	venkovan
a	a	k8xC	a
městská	městský	k2eAgFnSc1d1	městská
chudina	chudina	k1gFnSc1	chudina
uposlechli	uposlechnout	k5eAaPmAgMnP	uposlechnout
jeho	jeho	k3xOp3gInPc2	jeho
horlivých	horlivý	k2eAgInPc2d1	horlivý
apelů	apel	k1gInPc2	apel
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
chystat	chystat	k5eAaImF	chystat
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
museli	muset	k5eAaImAgMnP	muset
prodat	prodat	k5eAaPmF	prodat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
chudobě	chudoba	k1gFnSc6	chudoba
měli	mít	k5eAaImAgMnP	mít
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pozemku	pozemek	k1gInSc2	pozemek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
živil	živit	k5eAaImAgMnS	živit
<g/>
,	,	kIx,	,
a	a	k8xC	a
obydlí	obydlet	k5eAaPmIp3nS	obydlet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
koupit	koupit	k5eAaPmF	koupit
alespoň	alespoň	k9	alespoň
nejnutnější	nutný	k2eAgFnPc4d3	nejnutnější
zásoby	zásoba	k1gFnPc4	zásoba
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Nadšení	nadšení	k1gNnSc4	nadšení
pro	pro	k7c4	pro
svatou	svatý	k2eAgFnSc4d1	svatá
věc	věc	k1gFnSc4	věc
hrálo	hrát	k5eAaImAgNnS	hrát
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
konání	konání	k1gNnSc6	konání
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
důležité	důležitý	k2eAgFnPc4d1	důležitá
však	však	k8xC	však
byly	být	k5eAaImAgFnP	být
také	také	k9	také
materiální	materiální	k2eAgNnSc4d1	materiální
a	a	k8xC	a
demografické	demografický	k2eAgNnSc4d1	demografické
pohnutky	pohnutka	k1gFnPc4	pohnutka
<g/>
,	,	kIx,	,
přání	přání	k1gNnSc4	přání
získat	získat	k5eAaPmF	získat
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
vyloučit	vyloučit	k5eAaPmF	vyloučit
nelze	lze	k6eNd1	lze
ani	ani	k8xC	ani
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
dobrodružství	dobrodružství	k1gNnSc6	dobrodružství
a	a	k8xC	a
slávě	sláva	k1gFnSc3	sláva
<g/>
.	.	kIx.	.
<g/>
Ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
výprava	výprava	k1gFnSc1	výprava
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
světě	svět	k1gInSc6	svět
něčím	něco	k3yInSc7	něco
novým	nový	k2eAgNnSc7d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Urban	Urban	k1gMnSc1	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
neočekával	očekávat	k5eNaImAgInS	očekávat
tak	tak	k9	tak
horlivou	horlivý	k2eAgFnSc4d1	horlivá
odezvu	odezva	k1gFnSc4	odezva
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1096	[number]	k4	1096
musel	muset	k5eAaImAgMnS	muset
zakázat	zakázat	k5eAaPmF	zakázat
několika	několik	k4yIc3	několik
duchovním	duchovní	k1gMnPc3	duchovní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vojenské	vojenský	k2eAgFnSc2d1	vojenská
výpravy	výprava	k1gFnSc2	výprava
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
iberské	iberský	k2eAgMnPc4d1	iberský
šlechtice	šlechtic	k1gMnPc4	šlechtic
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
výpravy	výprava	k1gFnSc2	výprava
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
Země	zem	k1gFnSc2	zem
zabývali	zabývat	k5eAaImAgMnP	zabývat
bojem	boj	k1gInSc7	boj
s	s	k7c7	s
Maury	Maur	k1gMnPc7	Maur
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Tarragony	Tarragona	k1gFnSc2	Tarragona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
byzantský	byzantský	k2eAgMnSc1d1	byzantský
císař	císař	k1gMnSc1	císař
Alexios	Alexios	k1gMnSc1	Alexios
nepřál	přát	k5eNaImAgMnS	přát
<g/>
:	:	kIx,	:
křižáci	křižák	k1gMnPc1	křižák
neměli	mít	k5eNaImAgMnP	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
stát	stát	k5eAaPmF	stát
se	s	k7c7	s
žoldnéři	žoldnéř	k1gMnPc7	žoldnéř
v	v	k7c6	v
byzantské	byzantský	k2eAgFnSc6d1	byzantská
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
osvobodit	osvobodit	k5eAaPmF	osvobodit
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
osudu	osud	k1gInSc6	osud
císaři	císař	k1gMnSc3	císař
Alexiovi	Alexius	k1gMnSc3	Alexius
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
nezáleželo	záležet	k5eNaImAgNnS	záležet
a	a	k8xC	a
o	o	k7c4	o
které	který	k3yIgMnPc4	který
Byzanc	Byzanc	k1gFnSc1	Byzanc
přišla	přijít	k5eAaPmAgFnS	přijít
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
614	[number]	k4	614
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tažení	tažení	k1gNnPc4	tažení
chudiny	chudina	k1gFnSc2	chudina
==	==	k?	==
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
účastníků	účastník	k1gMnPc2	účastník
první	první	k4xOgFnSc2	první
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
nejen	nejen	k6eAd1	nejen
těch	ten	k3xDgFnPc2	ten
prostých	prostý	k2eAgFnPc2d1	prostá
a	a	k8xC	a
chudých	chudý	k2eAgFnPc2d1	chudá
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
ponětí	ponětí	k1gNnSc3	ponětí
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bude	být	k5eAaImBp3nS	být
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
vypadat	vypadat	k5eAaPmF	vypadat
a	a	k8xC	a
s	s	k7c7	s
čím	co	k3yRnSc7	co
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
ve	v	k7c6	v
vítězství	vítězství	k1gNnSc6	vítězství
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
kříže	kříž	k1gInPc1	kříž
vyženou	vyhnat	k5eAaPmIp3nP	vyhnat
nevěřící	věřící	k2eNgMnPc4d1	nevěřící
muslimy	muslim	k1gMnPc4	muslim
ze	z	k7c2	z
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
Boží	boží	k2eAgInSc1d1	boží
hrob	hrob	k1gInSc1	hrob
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
však	však	k9	však
byla	být	k5eAaImAgFnS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
vydaly	vydat	k5eAaPmAgFnP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
křižácké	křižácký	k2eAgInPc4d1	křižácký
houfy	houf	k1gInPc4	houf
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Porýní	Porýní	k1gNnSc2	Porýní
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
tvořili	tvořit	k5eAaImAgMnP	tvořit
neozbrojení	ozbrojený	k2eNgMnPc1d1	neozbrojený
poutníci	poutník	k1gMnPc1	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
však	však	k9	však
neměli	mít	k5eNaImAgMnP	mít
žádné	žádný	k3yNgFnPc4	žádný
vojenské	vojenský	k2eAgFnPc4d1	vojenská
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
už	už	k6eAd1	už
vůbec	vůbec	k9	vůbec
ne	ne	k9	ne
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
kázeň	kázeň	k1gFnSc4	kázeň
<g/>
,	,	kIx,	,
jediné	jediné	k1gNnSc4	jediné
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
oplývali	oplývat	k5eAaImAgMnP	oplývat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
bojové	bojový	k2eAgNnSc1d1	bojové
nadšení	nadšení	k1gNnSc1	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
dále	daleko	k6eAd2	daleko
podněcoval	podněcovat	k5eAaImAgMnS	podněcovat
chudý	chudý	k2eAgMnSc1d1	chudý
rytíř	rytíř	k1gMnSc1	rytíř
Gautier	Gautier	k1gMnSc1	Gautier
Sans-Avoir	Sans-Avoir	k1gMnSc1	Sans-Avoir
–	–	k?	–
Bezzemek	bezzemek	k1gMnSc1	bezzemek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
s	s	k7c7	s
několika	několik	k4yIc7	několik
dalšími	další	k2eAgMnPc7d1	další
příslušníky	příslušník	k1gMnPc7	příslušník
drobné	drobný	k2eAgFnSc2d1	drobná
šlechty	šlechta	k1gFnSc2	šlechta
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Francie	Francie	k1gFnSc2	Francie
již	již	k6eAd1	již
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1096	[number]	k4	1096
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
poutníci	poutník	k1gMnPc1	poutník
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
pěšky	pěšky	k6eAd1	pěšky
a	a	k8xC	a
jenom	jenom	k9	jenom
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
směly	smět	k5eAaImAgFnP	smět
vézt	vézt	k5eAaImF	vézt
na	na	k7c6	na
povozech	povoz	k1gInPc6	povoz
tažených	tažený	k2eAgFnPc2d1	tažená
voly	vůl	k1gMnPc7	vůl
<g/>
,	,	kIx,	,
ušli	ujít	k5eAaPmAgMnP	ujít
tisícikilometrovou	tisícikilometrový	k2eAgFnSc4d1	tisícikilometrová
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
na	na	k7c4	na
uhersko-byzantskou	uherskoyzantský	k2eAgFnSc4d1	uhersko-byzantský
hranici	hranice	k1gFnSc4	hranice
za	za	k7c4	za
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
května	květen	k1gInSc2	květen
1096	[number]	k4	1096
stanuli	stanout	k5eAaPmAgMnP	stanout
před	před	k7c7	před
byzantskou	byzantský	k2eAgFnSc7d1	byzantská
pohraniční	pohraniční	k2eAgFnSc7d1	pohraniční
pevností	pevnost	k1gFnSc7	pevnost
Bělehradem	Bělehrad	k1gInSc7	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
střetu	střet	k1gInSc3	střet
křižáků	křižák	k1gMnPc2	křižák
s	s	k7c7	s
místním	místní	k2eAgNnSc7d1	místní
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Vyhladovělá	vyhladovělý	k2eAgFnSc1d1	vyhladovělá
chudina	chudina	k1gFnSc1	chudina
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
došly	dojít	k5eAaPmAgFnP	dojít
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
totiž	totiž	k9	totiž
plenit	plenit	k5eAaImF	plenit
vesnice	vesnice	k1gFnSc1	vesnice
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
tak	tak	k9	tak
několik	několik	k4yIc4	několik
ozbrojených	ozbrojený	k2eAgInPc2d1	ozbrojený
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
důsledkem	důsledek	k1gInSc7	důsledek
byli	být	k5eAaImAgMnP	být
první	první	k4xOgMnPc1	první
mrtví	mrtvý	k1gMnPc1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Niši	Niš	k1gInSc6	Niš
přijal	přijmout	k5eAaPmAgMnS	přijmout
Gautiera	Gautier	k1gMnSc4	Gautier
byzantský	byzantský	k2eAgMnSc1d1	byzantský
místodržící	místodržící	k1gMnSc1	místodržící
a	a	k8xC	a
zásobil	zásobit	k5eAaPmAgMnS	zásobit
jeho	jeho	k3xOp3gMnPc4	jeho
poutníky	poutník	k1gMnPc4	poutník
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
hůře	zle	k6eAd2	zle
si	se	k3xPyFc3	se
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
uherskou	uherský	k2eAgFnSc4d1	uherská
zemi	zem	k1gFnSc4	zem
počínal	počínat	k5eAaImAgInS	počínat
houf	houf	k1gInSc1	houf
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
přiváděl	přivádět	k5eAaImAgMnS	přivádět
Petr	Petr	k1gMnSc1	Petr
Poustevník	poustevník	k1gMnSc1	poustevník
z	z	k7c2	z
Porýní	Porýní	k1gNnSc2	Porýní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
negativním	negativní	k2eAgInSc7d1	negativní
jevem	jev	k1gInSc7	jev
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
protižidovské	protižidovský	k2eAgInPc1d1	protižidovský
pogromy	pogrom	k1gInPc1	pogrom
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
rozpoutaly	rozpoutat	k5eAaPmAgInP	rozpoutat
některé	některý	k3yIgFnPc4	některý
křižácké	křižácký	k2eAgFnPc4d1	křižácká
skupiny	skupina	k1gFnPc4	skupina
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nechvalně	chvalně	k6eNd1	chvalně
tím	ten	k3xDgNnSc7	ten
proslul	proslout	k5eAaPmAgInS	proslout
především	především	k9	především
houf	houf	k1gInSc1	houf
vedený	vedený	k2eAgInSc1d1	vedený
nepříliš	příliš	k6eNd1	příliš
majetným	majetný	k2eAgMnSc7d1	majetný
hrabětem	hrabě	k1gMnSc7	hrabě
Emerichem	Emerich	k1gMnSc7	Emerich
z	z	k7c2	z
Leisingenu	Leisingen	k1gInSc2	Leisingen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
řádil	řádit	k5eAaImAgInS	řádit
v	v	k7c6	v
porýnských	porýnský	k2eAgNnPc6d1	porýnské
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
Emerichových	Emerichův	k2eAgMnPc2d1	Emerichův
křižáků	křižák	k1gMnPc2	křižák
pak	pak	k6eAd1	pak
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
uherské	uherský	k2eAgFnSc6d1	uherská
hranici	hranice	k1gFnSc6	hranice
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
král	král	k1gMnSc1	král
Koloman	Koloman	k1gMnSc1	Koloman
je	být	k5eAaImIp3nS	být
nechal	nechat	k5eAaPmAgMnS	nechat
rozehnat	rozehnat	k5eAaPmF	rozehnat
ještě	ještě	k9	ještě
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
křižácká	křižácký	k2eAgFnSc1d1	křižácká
skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
i	i	k9	i
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
pogromy	pogrom	k1gInPc4	pogrom
proti	proti	k7c3	proti
Židům	Žid	k1gMnPc3	Žid
tak	tak	k8xS	tak
postihly	postihnout	k5eAaPmAgInP	postihnout
také	také	k9	také
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
1096	[number]	k4	1096
dorazily	dorazit	k5eAaPmAgInP	dorazit
oba	dva	k4xCgInPc1	dva
francouzské	francouzský	k2eAgInPc1d1	francouzský
chudinské	chudinský	k2eAgInPc1d1	chudinský
houfy	houf	k1gInPc1	houf
pod	pod	k7c4	pod
hradby	hradba	k1gFnPc4	hradba
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Byzantské	byzantský	k2eAgFnSc3d1	byzantská
vládě	vláda	k1gFnSc3	vláda
bylo	být	k5eAaImAgNnS	být
naprosto	naprosto	k6eAd1	naprosto
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
postavit	postavit	k5eAaPmF	postavit
seldžuckému	seldžucký	k2eAgNnSc3d1	seldžucký
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
nemohla	moct	k5eNaImAgFnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
před	před	k7c7	před
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
loupily	loupit	k5eAaImAgFnP	loupit
a	a	k8xC	a
sužovaly	sužovat	k5eAaImAgFnP	sužovat
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
přesunuty	přesunout	k5eAaPmNgInP	přesunout
na	na	k7c4	na
maloasijskou	maloasijský	k2eAgFnSc4d1	maloasijská
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
poblíž	poblíž	k7c2	poblíž
Nikomédie	Nikomédie	k1gFnSc2	Nikomédie
vybudován	vybudován	k2eAgInSc1d1	vybudován
opevněný	opevněný	k2eAgInSc1d1	opevněný
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Kibotos	Kibotos	k1gInSc1	Kibotos
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
schránka	schránka	k1gFnSc1	schránka
<g/>
,	,	kIx,	,
truhlice	truhlice	k1gFnSc1	truhlice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křižáky	křižák	k1gInPc1	křižák
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Civetot	Civetot	k1gInSc4	Civetot
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
měli	mít	k5eAaImAgMnP	mít
poutníci	poutník	k1gMnPc1	poutník
vyčkat	vyčkat	k5eAaPmF	vyčkat
příchodu	příchod	k1gInSc6	příchod
rytířských	rytířský	k2eAgNnPc2d1	rytířské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
francouzští	francouzský	k2eAgMnPc1d1	francouzský
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
,	,	kIx,	,
však	však	k9	však
nechtěli	chtít	k5eNaImAgMnP	chtít
čekat	čekat	k5eAaImF	čekat
a	a	k8xC	a
pořádali	pořádat	k5eAaImAgMnP	pořádat
výpravy	výprava	k1gFnPc4	výprava
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Nikomedie	Nikomedie	k1gFnSc2	Nikomedie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
obětí	oběť	k1gFnSc7	oběť
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
stával	stávat	k5eAaImAgInS	stávat
majetek	majetek	k1gInSc1	majetek
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
rolníků	rolník	k1gMnPc2	rolník
i	i	k9	i
rolníci	rolník	k1gMnPc1	rolník
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
proniknout	proniknout	k5eAaPmF	proniknout
až	až	k9	až
před	před	k7c7	před
Nikáiu	Nikáium	k1gNnSc6	Nikáium
<g/>
,	,	kIx,	,
obsazenou	obsazený	k2eAgFnSc4d1	obsazená
Seldžuky	Seldžuk	k1gInPc4	Seldžuk
<g/>
,	,	kIx,	,
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
před	před	k7c7	před
jejími	její	k3xOp3gFnPc7	její
hradbami	hradba	k1gFnPc7	hradba
značnou	značný	k2eAgFnSc4d1	značná
kořist	kořist	k1gFnSc1	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
v	v	k7c6	v
křižáckém	křižácký	k2eAgInSc6d1	křižácký
táboře	tábor	k1gInSc6	tábor
nadšení	nadšení	k1gNnSc2	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
Davy	Dav	k1gInPc1	Dav
nezkušené	zkušený	k2eNgFnSc2d1	nezkušená
chudiny	chudina	k1gFnSc2	chudina
vyrazily	vyrazit	k5eAaPmAgFnP	vyrazit
navzdory	navzdory	k7c3	navzdory
varování	varování	k1gNnSc3	varování
opatrnějších	opatrný	k2eAgInPc2d2	opatrnější
předáků	předák	k1gInPc2	předák
k	k	k7c3	k
Nikáii	Nikáie	k1gFnSc3	Nikáie
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
je	on	k3xPp3gMnPc4	on
zastihli	zastihnout	k5eAaPmAgMnP	zastihnout
a	a	k8xC	a
rozprášili	rozprášit	k5eAaPmAgMnP	rozprášit
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
kibotos	kibotos	k1gInSc4	kibotos
a	a	k8xC	a
pobili	pobít	k5eAaPmAgMnP	pobít
většinu	většina	k1gFnSc4	většina
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
byly	být	k5eAaImAgFnP	být
prodány	prodat	k5eAaPmNgFnP	prodat
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgMnPc4d1	zbylý
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
si	se	k3xPyFc3	se
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
převezli	převézt	k5eAaPmAgMnP	převézt
Byzantinci	Byzantinec	k1gMnPc1	Byzantinec
ke	k	k7c3	k
Konstantinopoli	Konstantinopol	k1gInSc3	Konstantinopol
a	a	k8xC	a
ubytovali	ubytovat	k5eAaPmAgMnP	ubytovat
je	on	k3xPp3gNnSc4	on
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zbraně	zbraň	k1gFnPc1	zbraň
jim	on	k3xPp3gMnPc3	on
raději	rád	k6eAd2	rád
předem	předem	k6eAd1	předem
odebrali	odebrat	k5eAaPmAgMnP	odebrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
svoji	svůj	k3xOyFgFnSc4	svůj
kruciátu	kruciáta	k1gFnSc4	kruciáta
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tažení	tažení	k1gNnSc1	tažení
rytířů	rytíř	k1gMnPc2	rytíř
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Papežovy	Papežův	k2eAgFnSc2d1	Papežova
agitace	agitace	k1gFnSc2	agitace
===	===	k?	===
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
skončil	skončit	k5eAaPmAgMnS	skončit
clermontský	clermontský	k2eAgInSc4d1	clermontský
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
Urban	Urban	k1gMnSc1	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
bezmála	bezmála	k6eAd1	bezmála
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
osobně	osobně	k6eAd1	osobně
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
rozesílal	rozesílat	k5eAaImAgInS	rozesílat
dopisy	dopis	k1gInPc4	dopis
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
příslušníkům	příslušník	k1gMnPc3	příslušník
vysoké	vysoký	k2eAgFnSc2d1	vysoká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
křížové	křížový	k2eAgFnPc1d1	křížová
výpravy	výprava	k1gFnPc1	výprava
se	se	k3xPyFc4	se
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
žádný	žádný	k3yNgMnSc1	žádný
evropský	evropský	k2eAgMnSc1d1	evropský
panovník	panovník	k1gMnSc1	panovník
<g/>
:	:	kIx,	:
římsko-německý	římskoěmecký	k2eAgMnSc1d1	římsko-německý
císař	císař	k1gMnSc1	císař
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
clermontském	clermontský	k2eAgInSc6d1	clermontský
koncilu	koncil	k1gInSc6	koncil
za	za	k7c4	za
bigamii	bigamie	k1gFnSc4	bigamie
exkomunikován	exkomunikován	k2eAgMnSc1d1	exkomunikován
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Vilém	Vilém	k1gMnSc1	Vilém
Ryšavý	Ryšavý	k1gMnSc1	Ryšavý
se	se	k3xPyFc4	se
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
příliš	příliš	k6eAd1	příliš
nehlásil	hlásit	k5eNaImAgMnS	hlásit
<g/>
,	,	kIx,	,
vládcové	vládce	k1gMnPc1	vládce
skandinávských	skandinávský	k2eAgFnPc2d1	skandinávská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
christianizovány	christianizovat	k5eAaImNgFnP	christianizovat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
příliš	příliš	k6eAd1	příliš
daleko	daleko	k6eAd1	daleko
a	a	k8xC	a
španělští	španělský	k2eAgMnPc1d1	španělský
králové	král	k1gMnPc1	král
byli	být	k5eAaImAgMnP	být
plně	plně	k6eAd1	plně
zaměstnáni	zaměstnat	k5eAaPmNgMnP	zaměstnat
vlastní	vlastní	k2eAgFnSc7d1	vlastní
reconquistou	reconquistý	k2eAgFnSc7d1	reconquistý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Toursu	Tours	k1gInSc6	Tours
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
sezván	sezván	k2eAgInSc4d1	sezván
další	další	k2eAgInSc4d1	další
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
papež	papež	k1gMnSc1	papež
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1096	[number]	k4	1096
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
Clermontu	Clermont	k1gInSc6	Clermont
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
promluvil	promluvit	k5eAaPmAgMnS	promluvit
ke	k	k7c3	k
shromážděnému	shromážděný	k2eAgInSc3d1	shromážděný
davu	dav	k1gInSc3	dav
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
projevili	projevit	k5eAaPmAgMnP	projevit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
výpravu	výprava	k1gFnSc4	výprava
významní	významný	k2eAgMnPc1d1	významný
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
předem	předem	k6eAd1	předem
informovaného	informovaný	k2eAgNnSc2d1	informované
a	a	k8xC	a
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
obeznámeného	obeznámený	k2eAgMnSc2d1	obeznámený
Raimonda	Raimond	k1gMnSc2	Raimond
z	z	k7c2	z
Toulouse	Toulouse	k1gInSc2	Toulouse
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
bratr	bratr	k1gMnSc1	bratr
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
I.	I.	kA	I.
Hugo	Hugo	k1gMnSc1	Hugo
z	z	k7c2	z
Vermandois	Vermandois	k1gFnSc2	Vermandois
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
Urbanem	Urban	k1gMnSc7	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
vyobcován	vyobcovat	k5eAaPmNgMnS	vyobcovat
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
účast	účast	k1gFnSc1	účast
Huga	Hugo	k1gMnSc2	Hugo
z	z	k7c2	z
Vermandois	Vermandois	k1gFnSc2	Vermandois
na	na	k7c6	na
výpravě	výprava	k1gFnSc6	výprava
důležitým	důležitý	k2eAgInSc7d1	důležitý
krokem	krok	k1gInSc7	krok
k	k	k7c3	k
urovnání	urovnání	k1gNnSc3	urovnání
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
papežem	papež	k1gMnSc7	papež
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
zájemce	zájemce	k1gMnPc4	zájemce
o	o	k7c4	o
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
výpravě	výprava	k1gFnSc6	výprava
byl	být	k5eAaImAgMnS	být
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Viléma	Vilém	k1gMnSc4	Vilém
Dobyvatele	dobyvatel	k1gMnSc4	dobyvatel
normandský	normandský	k2eAgMnSc1d1	normandský
vévoda	vévoda	k1gMnSc1	vévoda
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
dobrý	dobrý	k2eAgMnSc1d1	dobrý
válečník	válečník	k1gMnSc1	válečník
bez	bez	k7c2	bez
státnických	státnický	k2eAgFnPc2d1	státnická
kvalit	kvalita	k1gFnPc2	kvalita
<g/>
,	,	kIx,	,
a	a	k8xC	a
flanderský	flanderský	k2eAgMnSc1d1	flanderský
hrabě	hrabě	k1gMnSc1	hrabě
Robert	Robert	k1gMnSc1	Robert
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
otec	otec	k1gMnSc1	otec
navázal	navázat	k5eAaPmAgMnS	navázat
nadstandardní	nadstandardní	k2eAgInPc4d1	nadstandardní
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Byzantskou	byzantský	k2eAgFnSc7d1	byzantská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
své	svůj	k3xOyFgFnSc2	svůj
panovačné	panovačný	k2eAgFnSc2d1	panovačná
ženy	žena	k1gFnSc2	žena
Adély	Adéla	k1gFnSc2	Adéla
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnPc1	sestra
Roberta	Robert	k1gMnSc2	Robert
Normandského	normandský	k2eAgMnSc2d1	normandský
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
mít	mít	k5eAaImF	mít
ze	z	k7c2	z
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
slavného	slavný	k2eAgMnSc2d1	slavný
válečníka	válečník	k1gMnSc2	válečník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
váhavě	váhavě	k6eAd1	váhavě
přidal	přidat	k5eAaPmAgMnS	přidat
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Blois	Blois	k1gFnPc2	Blois
a	a	k8xC	a
Chartres	Chartresa	k1gFnPc2	Chartresa
<g/>
.	.	kIx.	.
</s>
<s>
Vojsko	vojsko	k1gNnSc1	vojsko
z	z	k7c2	z
Lotrinska	Lotrinsko	k1gNnSc2	Lotrinsko
vedli	vést	k5eAaImAgMnP	vést
bratři	bratr	k1gMnPc1	bratr
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Boulogne	Boulogn	k1gInSc5	Boulogn
<g/>
,	,	kIx,	,
dolnolotrinský	dolnolotrinský	k2eAgMnSc1d1	dolnolotrinský
vévoda	vévoda	k1gMnSc1	vévoda
Godefroy	Godefroa	k1gFnSc2	Godefroa
z	z	k7c2	z
Bouillonu	Bouillon	k1gInSc2	Bouillon
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
významný	významný	k2eAgMnSc1d1	významný
vazal	vazal	k1gMnSc1	vazal
římsko-německého	římskoěmecký	k2eAgMnSc2d1	římsko-německý
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
výpravě	výprava	k1gFnSc3	výprava
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Eustach	Eustach	k1gMnSc1	Eustach
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
jen	jen	k9	jen
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
politickou	politický	k2eAgFnSc4d1	politická
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
,	,	kIx,	,
o	o	k7c4	o
výpravu	výprava	k1gFnSc4	výprava
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vůbec	vůbec	k9	vůbec
nestál	stát	k5eNaImAgMnS	stát
a	a	k8xC	a
podřídil	podřídit	k5eAaPmAgMnS	podřídit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
Godefroyově	Godefroyův	k2eAgNnSc6d1	Godefroyův
velení	velení	k1gNnSc6	velení
<g/>
.	.	kIx.	.
</s>
<s>
Nejmladší	mladý	k2eAgMnSc1d3	nejmladší
Balduin	Balduin	k1gMnSc1	Balduin
byl	být	k5eAaImAgMnS	být
zcela	zcela	k6eAd1	zcela
nemajetný	majetný	k2eNgMnSc1d1	nemajetný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
s	s	k7c7	s
vidinou	vidina	k1gFnSc7	vidina
vydobytí	vydobytí	k1gNnSc2	vydobytí
si	se	k3xPyFc3	se
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Hodlal	hodlat	k5eAaImAgMnS	hodlat
se	se	k3xPyFc4	se
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
usadit	usadit	k5eAaPmF	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Jihoitalští	jihoitalský	k2eAgMnPc1d1	jihoitalský
Normané	Norman	k1gMnPc1	Norman
nechali	nechat	k5eAaPmAgMnP	nechat
papežovy	papežův	k2eAgFnPc4d1	papežova
výzvy	výzva	k1gFnPc4	výzva
zpočátku	zpočátku	k6eAd1	zpočátku
bez	bez	k7c2	bez
odezvy	odezva	k1gFnSc2	odezva
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	on	k3xPp3gMnPc4	on
zaměstnávaly	zaměstnávat	k5eAaImAgInP	zaměstnávat
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
syny	syn	k1gMnPc7	syn
Roberta	Robert	k1gMnSc2	Robert
Guiscarda	Guiscard	k1gMnSc2	Guiscard
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
normanském	normanský	k2eAgNnSc6d1	normanské
území	území	k1gNnSc6	území
objevili	objevit	k5eAaPmAgMnP	objevit
procházející	procházející	k2eAgMnPc1d1	procházející
francouzští	francouzský	k2eAgMnPc1d1	francouzský
křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
využil	využít	k5eAaPmAgMnS	využít
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Guiscardových	Guiscardův	k2eAgMnPc2d1	Guiscardův
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
Bohemund	Bohemund	k1gInSc4	Bohemund
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
<g/>
,	,	kIx,	,
příležitosti	příležitost	k1gFnSc2	příležitost
vydobýt	vydobýt	k5eAaPmF	vydobýt
si	se	k3xPyFc3	se
v	v	k7c6	v
Orientu	Orient	k1gInSc6	Orient
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
méně	málo	k6eAd2	málo
početné	početný	k2eAgFnPc4d1	početná
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
dobře	dobře	k6eAd1	dobře
vycvičené	vycvičený	k2eAgNnSc1d1	vycvičené
a	a	k8xC	a
zkušené	zkušený	k2eAgNnSc1d1	zkušené
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
ho	on	k3xPp3gNnSc4	on
jeho	jeho	k3xOp3gMnPc1	jeho
synovci	synovec	k1gMnPc1	synovec
Tankred	Tankred	k1gInSc4	Tankred
a	a	k8xC	a
Guillame	Guillam	k1gInSc5	Guillam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
evropském	evropský	k2eAgInSc6d1	evropský
území	území	k1gNnSc2	území
táhla	táhnout	k5eAaImAgNnP	táhnout
křižácká	křižácký	k2eAgNnPc1d1	křižácké
vojska	vojsko	k1gNnPc1	vojsko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
cestami	cesta	k1gFnPc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Armády	armáda	k1gFnPc1	armáda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
vyzbrojili	vyzbrojit	k5eAaPmAgMnP	vyzbrojit
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
evropští	evropský	k2eAgMnPc1d1	evropský
feudálové	feudál	k1gMnPc1	feudál
byly	být	k5eAaImAgFnP	být
různě	různě	k6eAd1	různě
početné	početný	k2eAgFnPc1d1	početná
a	a	k8xC	a
hrály	hrát	k5eAaImAgFnP	hrát
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
veliteli	velitel	k1gMnPc7	velitel
různě	různě	k6eAd1	různě
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
nadcházejícím	nadcházející	k2eAgNnSc6d1	nadcházející
tažení	tažení	k1gNnSc6	tažení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1096	[number]	k4	1096
Francouzi	Francouz	k1gMnPc1	Francouz
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Huga	Hugo	k1gMnSc2	Hugo
z	z	k7c2	z
Vermandois	Vermandois	k1gFnSc2	Vermandois
<g/>
.	.	kIx.	.
</s>
<s>
Hugovo	Hugův	k2eAgNnSc1d1	Hugovo
nepříliš	příliš	k6eNd1	příliš
početné	početný	k2eAgNnSc1d1	početné
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
rytířů	rytíř	k1gMnPc2	rytíř
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
panství	panství	k1gNnPc2	panství
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
kontingentu	kontingent	k1gInSc2	kontingent
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
křižáckých	křižácký	k2eAgMnPc2d1	křižácký
šlechticů	šlechtic	k1gMnPc2	šlechtic
byl	být	k5eAaImAgMnS	být
Hugo	Hugo	k1gMnSc1	Hugo
z	z	k7c2	z
Vermandois	Vermandois	k1gFnSc2	Vermandois
ten	ten	k3xDgMnSc1	ten
nejurozenější	urozený	k2eAgMnSc1d3	nejurozenější
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
bylo	být	k5eAaImAgNnS	být
jednáno	jednat	k5eAaImNgNnS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
proto	proto	k8xC	proto
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
poselstvo	poselstvo	k1gNnSc1	poselstvo
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gNnSc4	on
císař	císař	k1gMnSc1	císař
Alexios	Alexios	k1gMnSc1	Alexios
náležitě	náležitě	k6eAd1	náležitě
přivítal	přivítat	k5eAaPmAgMnS	přivítat
a	a	k8xC	a
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
sobě	se	k3xPyFc3	se
rovným	rovný	k2eAgInSc7d1	rovný
<g/>
.	.	kIx.	.
</s>
<s>
Hugovy	Hugův	k2eAgInPc1d1	Hugův
oddíly	oddíl	k1gInPc1	oddíl
se	se	k3xPyFc4	se
vypravily	vypravit	k5eAaPmAgFnP	vypravit
do	do	k7c2	do
jihoitalského	jihoitalský	k2eAgNnSc2d1	jihoitalské
Bari	Bari	k1gNnSc2	Bari
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
přibral	přibrat	k5eAaPmAgInS	přibrat
některé	některý	k3yIgInPc4	některý
zbylé	zbylý	k2eAgInPc4d1	zbylý
členy	člen	k1gInPc4	člen
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
Emericha	Emerich	k1gMnSc2	Emerich
z	z	k7c2	z
Leisingenu	Leisingen	k1gInSc2	Leisingen
a	a	k8xC	a
přidala	přidat	k5eAaPmAgFnS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
i	i	k9	i
skupina	skupina	k1gFnSc1	skupina
italo-normanských	italoormanský	k2eAgMnPc2d1	italo-normanský
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Hugo	Hugo	k1gMnSc1	Hugo
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Bari	Bari	k1gNnSc2	Bari
a	a	k8xC	a
odkud	odkud	k6eAd1	odkud
chtěl	chtít	k5eAaImAgMnS	chtít
dále	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
na	na	k7c4	na
byzantské	byzantský	k2eAgNnSc4d1	byzantské
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
období	období	k1gNnSc1	období
podzimních	podzimní	k2eAgFnPc2d1	podzimní
bouří	bouř	k1gFnPc2	bouř
<g/>
.	.	kIx.	.
<g/>
Uprostřed	uprostřed	k7c2	uprostřed
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
křižáky	křižák	k1gMnPc4	křižák
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
vichřice	vichřice	k1gFnSc1	vichřice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
potopila	potopit	k5eAaPmAgFnS	potopit
několik	několik	k4yIc4	několik
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
zahnala	zahnat	k5eAaPmAgFnS	zahnat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Ilýrie	Ilýrie	k1gFnSc2	Ilýrie
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
měli	mít	k5eAaImAgMnP	mít
velké	velký	k2eAgFnPc4d1	velká
materiální	materiální	k2eAgFnPc4d1	materiální
i	i	k8xC	i
lidské	lidský	k2eAgFnPc4d1	lidská
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Hugo	Hugo	k1gMnSc1	Hugo
ztroskotal	ztroskotat	k5eAaPmAgMnS	ztroskotat
poblíž	poblíž	k7c2	poblíž
svého	svůj	k3xOyFgInSc2	svůj
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
přístavu	přístav	k1gInSc2	přístav
Dyrrhachion	Dyrrhachion	k1gInSc1	Dyrrhachion
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
objevili	objevit	k5eAaPmAgMnP	objevit
vyslanci	vyslanec	k1gMnPc1	vyslanec
Jana	Jana	k1gFnSc1	Jana
Komnena	Komnena	k1gFnSc1	Komnena
<g/>
,	,	kIx,	,
byzantského	byzantský	k2eAgMnSc4d1	byzantský
guvernéra	guvernér	k1gMnSc4	guvernér
Dyrrhachia	Dyrrhachius	k1gMnSc4	Dyrrhachius
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Komnenos	Komnenos	k1gMnSc1	Komnenos
Huga	Hugo	k1gMnSc4	Hugo
přijal	přijmout	k5eAaPmAgMnS	přijmout
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnSc2	jeho
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
dal	dát	k5eAaPmAgMnS	dát
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
nenápadně	nápadně	k6eNd1	nápadně
sledovat	sledovat	k5eAaImF	sledovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
dále	daleko	k6eAd2	daleko
Huga	Hugo	k1gMnSc2	Hugo
doprovodila	doprovodit	k5eAaPmAgFnS	doprovodit
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
eskorta	eskorta	k1gFnSc1	eskorta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mezi	mezi	k7c7	mezi
Francouzi	Francouz	k1gMnPc7	Francouz
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrabě	hrabě	k1gMnSc1	hrabě
Hugo	Hugo	k1gMnSc1	Hugo
není	být	k5eNaImIp3nS	být
host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
vězeň	vězeň	k1gMnSc1	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
byl	být	k5eAaImAgMnS	být
Hugo	Hugo	k1gMnSc1	Hugo
zahrnut	zahrnout	k5eAaPmNgMnS	zahrnout
císařovou	císařův	k2eAgFnSc7d1	císařova
přízní	přízeň	k1gFnSc7	přízeň
i	i	k8xC	i
dary	dar	k1gInPc1	dar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Hugo	Hugo	k1gMnSc1	Hugo
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Byzantinců	Byzantinec	k1gMnPc2	Byzantinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1097	[number]	k4	1097
byla	být	k5eAaImAgFnS	být
Hugova	Hugův	k2eAgFnSc1d1	Hugova
armáda	armáda	k1gFnSc1	armáda
dopravena	dopravit	k5eAaPmNgFnS	dopravit
na	na	k7c4	na
asijskou	asijský	k2eAgFnSc4d1	asijská
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
k	k	k7c3	k
Nikomédii	Nikomédie	k1gFnSc3	Nikomédie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hugova	Hugův	k2eAgFnSc1d1	Hugova
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
přijít	přijít	k5eAaPmF	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Císaři	Císař	k1gMnSc3	Císař
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
křižáci	křižák	k1gMnPc1	křižák
nepřišli	přijít	k5eNaPmAgMnP	přijít
jako	jako	k9	jako
poslušní	poslušný	k2eAgMnPc1d1	poslušný
žoldnéři	žoldnér	k1gMnPc1	žoldnér
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
Byzanc	Byzanc	k1gFnSc4	Byzanc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostatně	ostatně	k6eAd1	ostatně
o	o	k7c6	o
ničem	nic	k3yNnSc6	nic
takovém	takový	k3xDgInSc6	takový
se	se	k3xPyFc4	se
Urban	Urban	k1gMnSc1	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
výzvách	výzva	k1gFnPc6	výzva
nezmiňoval	zmiňovat	k5eNaImAgMnS	zmiňovat
<g/>
.	.	kIx.	.
</s>
<s>
Alexius	Alexius	k1gInSc1	Alexius
byl	být	k5eAaImAgInS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
akceptovat	akceptovat	k5eAaBmF	akceptovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
křižáci	křižák	k1gMnPc1	křižák
získají	získat	k5eAaPmIp3nP	získat
na	na	k7c6	na
východě	východ	k1gInSc6	východ
vlastní	vlastní	k2eAgNnSc1d1	vlastní
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Byzanci	Byzanc	k1gFnSc3	Byzanc
budou	být	k5eAaImBp3nP	být
navrácena	navrácen	k2eAgNnPc1d1	navráceno
území	území	k1gNnPc1	území
dobytá	dobytý	k2eAgFnSc1d1	dobytá
Seldžuky	Seldžuk	k1gInPc7	Seldžuk
a	a	k8xC	a
křižácké	křižácký	k2eAgFnPc1d1	křižácká
državy	država	k1gFnPc1	država
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
byzantskými	byzantský	k2eAgNnPc7d1	byzantské
lény	léno	k1gNnPc7	léno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
budou	být	k5eAaImBp3nP	být
říši	říš	k1gFnSc6	říš
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
náporem	nápor	k1gInSc7	nápor
nevěřících	nevěřící	k1gMnPc2	nevěřící
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
zajistit	zajistit	k5eAaPmF	zajistit
v	v	k7c6	v
poměrech	poměr	k1gInPc6	poměr
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
lenním	lenní	k2eAgInSc7d1	lenní
slibem	slib	k1gInSc7	slib
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
z	z	k7c2	z
Vermandois	Vermandois	k1gFnSc2	Vermandois
pokládal	pokládat	k5eAaImAgMnS	pokládat
císařův	císařův	k2eAgInSc4d1	císařův
požadavek	požadavek	k1gInSc4	požadavek
za	za	k7c4	za
rozumný	rozumný	k2eAgInSc4d1	rozumný
a	a	k8xC	a
přísahu	přísaha	k1gFnSc4	přísaha
složil	složit	k5eAaPmAgMnS	složit
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
druzí	druhý	k4xOgMnPc1	druhý
se	s	k7c7	s
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1096	[number]	k4	1096
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
lotrinští	lotrinský	k2eAgMnPc1d1	lotrinský
bratři	bratr	k1gMnPc1	bratr
Godefroy	Godefroa	k1gFnSc2	Godefroa
<g/>
,	,	kIx,	,
Balduin	Balduin	k1gMnSc1	Balduin
a	a	k8xC	a
Eustach	Eustach	k1gMnSc1	Eustach
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnSc1	jejich
bratranec	bratranec	k1gMnSc1	bratranec
Balduin	Balduin	k1gMnSc1	Balduin
Le	Le	k1gMnSc1	Le
Bourg	Bourg	k1gMnSc1	Bourg
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
1000	[number]	k4	1000
dalších	další	k2eAgMnPc2d1	další
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
7000	[number]	k4	7000
pěších	pěší	k2eAgMnPc2d1	pěší
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
Godefroy	Godefroa	k1gFnPc4	Godefroa
stále	stále	k6eAd1	stále
leníkem	leník	k1gMnSc7	leník
římsko-německého	římskoěmecký	k2eAgMnSc4d1	římsko-německý
císaře	císař	k1gMnSc4	císař
<g/>
,	,	kIx,	,
výprava	výprava	k1gFnSc1	výprava
táhla	táhlo	k1gNnSc2	táhlo
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
přes	přes	k7c4	přes
Svatou	svatý	k2eAgFnSc4d1	svatá
říši	říše	k1gFnSc4	říše
římskou	římský	k2eAgFnSc4d1	římská
do	do	k7c2	do
Uherska	Uhersko	k1gNnSc2	Uhersko
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
lidové	lidový	k2eAgFnSc2d1	lidová
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ony	onen	k3xDgFnPc1	onen
<g/>
,	,	kIx,	,
i	i	k8xC	i
Godefroy	Godefro	k1gMnPc7	Godefro
vydíral	vydírat	k5eAaImAgInS	vydírat
lotrinské	lotrinský	k2eAgMnPc4d1	lotrinský
a	a	k8xC	a
německé	německý	k2eAgMnPc4d1	německý
Židy	Žid	k1gMnPc4	Žid
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
získal	získat	k5eAaPmAgInS	získat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
Godefroy	Godefroy	k1gInPc4	Godefroy
nikdy	nikdy	k6eAd1	nikdy
nezašel	zajít	k5eNaPmAgInS	zajít
až	až	k9	až
k	k	k7c3	k
pogromům	pogrom	k1gInPc3	pogrom
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
praktikoval	praktikovat	k5eAaImAgInS	praktikovat
Emerich	Emerich	k1gInSc1	Emerich
z	z	k7c2	z
Leisingenu	Leisingen	k1gInSc2	Leisingen
a	a	k8xC	a
jemu	on	k3xPp3gInSc3	on
podobní	podobný	k2eAgMnPc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
výprava	výprava	k1gFnSc1	výprava
dorazila	dorazit	k5eAaPmAgFnS	dorazit
na	na	k7c4	na
uherské	uherský	k2eAgFnPc4d1	uherská
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
Godefroy	Godefro	k1gMnPc7	Godefro
osobně	osobně	k6eAd1	osobně
s	s	k7c7	s
králem	král	k1gMnSc7	král
Kolomanem	Koloman	k1gMnSc7	Koloman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
předchozích	předchozí	k2eAgFnPc6d1	předchozí
zkušenostech	zkušenost	k1gFnPc6	zkušenost
s	s	k7c7	s
křižáky	křižák	k1gMnPc7	křižák
velmi	velmi	k6eAd1	velmi
obezřetný	obezřetný	k2eAgInSc1d1	obezřetný
<g/>
.	.	kIx.	.
</s>
<s>
Kolomana	Koloman	k1gMnSc4	Koloman
uklidnilo	uklidnit	k5eAaPmAgNnS	uklidnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Godefroy	Godefroa	k1gFnPc4	Godefroa
požadoval	požadovat	k5eAaImAgMnS	požadovat
jen	jen	k9	jen
potřebné	potřebný	k2eAgFnPc4d1	potřebná
věci	věc	k1gFnPc4	věc
k	k	k7c3	k
tažení	tažení	k1gNnSc3	tažení
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
za	za	k7c4	za
všechno	všechen	k3xTgNnSc4	všechen
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
Koloman	Koloman	k1gMnSc1	Koloman
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
Godefroyova	Godefroyův	k2eAgMnSc4d1	Godefroyův
bratra	bratr	k1gMnSc4	bratr
Balduina	Balduin	k1gMnSc4	Balduin
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
jako	jako	k9	jako
rukojmí	rukojmí	k1gMnPc1	rukojmí
a	a	k8xC	a
Godefroy	Godefroa	k1gMnSc2	Godefroa
se	se	k3xPyFc4	se
zaručil	zaručit	k5eAaPmAgMnS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
přechod	přechod	k1gInSc1	přechod
Uherska	Uhersko	k1gNnSc2	Uhersko
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Lotrinčani	Lotrinčan	k1gMnPc1	Lotrinčan
skutečně	skutečně	k6eAd1	skutečně
přešli	přejít	k5eAaPmAgMnP	přejít
bez	bez	k7c2	bez
incidentů	incident	k1gInPc2	incident
Kolomanovu	Kolomanův	k2eAgFnSc4d1	Kolomanův
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Překročili	překročit	k5eAaPmAgMnP	překročit
Sávu	Sáva	k1gMnSc4	Sáva
na	na	k7c4	na
byzantskou	byzantský	k2eAgFnSc4d1	byzantská
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gInPc4	on
již	již	k9	již
čekali	čekat	k5eAaImAgMnP	čekat
byzantští	byzantský	k2eAgMnPc1d1	byzantský
průvodci	průvodce	k1gMnPc1	průvodce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
výpravu	výprava	k1gMnSc4	výprava
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
dovedli	dovést	k5eAaPmAgMnP	dovést
k	k	k7c3	k
městu	město	k1gNnSc3	město
Selymbria	Selymbrium	k1gNnSc2	Selymbrium
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Marmarského	Marmarský	k2eAgNnSc2d1	Marmarské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
65	[number]	k4	65
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
křižáky	křižák	k1gInPc4	křižák
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
hraběti	hrabě	k1gMnSc6	hrabě
z	z	k7c2	z
Vermandois	Vermandois	k1gFnSc2	Vermandois
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Lotrinčané	Lotrinčan	k1gMnPc1	Lotrinčan
proto	proto	k8xC	proto
jako	jako	k8xS	jako
odvetu	odveta	k1gFnSc4	odveta
začali	začít	k5eAaPmAgMnP	začít
plenit	plenit	k5eAaImF	plenit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Selymbie	Selymbie	k1gFnSc2	Selymbie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
císař	císař	k1gMnSc1	císař
Alexios	Alexios	k1gMnSc1	Alexios
poslal	poslat	k5eAaPmAgMnS	poslat
Godefroyovi	Godefroya	k1gMnSc3	Godefroya
stížnosti	stížnost	k1gFnSc2	stížnost
<g/>
,	,	kIx,	,
Godefroy	Godefroa	k1gFnSc2	Godefroa
obnovil	obnovit	k5eAaPmAgInS	obnovit
ve	v	k7c6	v
vojsku	vojsko	k1gNnSc6	vojsko
pořádek	pořádek	k1gInSc4	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Godefroye	Godefroye	k1gFnPc7	Godefroye
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
bratry	bratr	k1gMnPc7	bratr
chtěl	chtít	k5eAaImAgMnS	chtít
Alexios	Alexios	k1gMnSc1	Alexios
přimět	přimět	k5eAaPmF	přimět
ke	k	k7c3	k
složení	složení	k1gNnSc3	složení
přísahy	přísaha	k1gFnSc2	přísaha
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
již	již	k6eAd1	již
složil	složit	k5eAaPmAgMnS	složit
Hugo	Hugo	k1gMnSc1	Hugo
z	z	k7c2	z
Vermandois	Vermandois	k1gFnSc2	Vermandois
<g/>
.	.	kIx.	.
</s>
<s>
Godefroy	Godefro	k1gMnPc4	Godefro
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	jenž	k3xRgFnSc4	jenž
přísahu	přísaha	k1gFnSc4	přísaha
složil	složit	k5eAaPmAgMnS	složit
římsko-německému	římskoěmecký	k2eAgMnSc3d1	římsko-německý
císaři	císař	k1gMnSc3	císař
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Godefroye	Godefroyat	k5eAaPmIp3nS	Godefroyat
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
ke	k	k7c3	k
složení	složení	k1gNnSc3	složení
přísahy	přísaha	k1gFnSc2	přísaha
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
i	i	k9	i
Hugo	Hugo	k1gMnSc1	Hugo
z	z	k7c2	z
Vermandois	Vermandois	k1gFnSc2	Vermandois
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
osobně	osobně	k6eAd1	osobně
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
lotrinského	lotrinský	k2eAgNnSc2d1	Lotrinské
ležení	ležení	k1gNnSc2	ležení
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
marně	marně	k6eAd1	marně
<g/>
.	.	kIx.	.
</s>
<s>
Vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
vázlo	váznout	k5eAaImAgNnS	váznout
a	a	k8xC	a
Alexia	Alexius	k1gMnSc2	Alexius
tlačil	tlačit	k5eAaImAgMnS	tlačit
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západu	západ	k1gInSc2	západ
se	se	k3xPyFc4	se
blížila	blížit	k5eAaImAgFnS	blížit
vojska	vojsko	k1gNnSc2	vojsko
ostatních	ostatní	k2eAgMnPc2d1	ostatní
křižáckých	křižácký	k2eAgMnPc2d1	křižácký
velmožů	velmož	k1gMnPc2	velmož
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Lotrinčané	Lotrinčan	k1gMnPc1	Lotrinčan
přísahali	přísahat	k5eAaImAgMnP	přísahat
Alexiovi	Alexiův	k2eAgMnPc1d1	Alexiův
věrnost	věrnost	k1gFnSc1	věrnost
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
dorazí	dorazit	k5eAaPmIp3nP	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Vánocích	Vánoce	k1gFnPc6	Vánoce
se	se	k3xPyFc4	se
Alexios	Alexios	k1gMnSc1	Alexios
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
k	k	k7c3	k
radikálnímu	radikální	k2eAgInSc3d1	radikální
kroku	krok	k1gInSc3	krok
a	a	k8xC	a
Lotrinčanům	Lotrinčan	k1gMnPc3	Lotrinčan
přerušil	přerušit	k5eAaPmAgMnS	přerušit
zásobování	zásobování	k1gNnSc4	zásobování
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc1	vedení
Balduinem	Balduino	k1gNnSc7	Balduino
z	z	k7c2	z
Boulogne	Boulogn	k1gInSc5	Boulogn
<g/>
,	,	kIx,	,
reagovali	reagovat	k5eAaBmAgMnP	reagovat
loupením	loupení	k1gNnSc7	loupení
a	a	k8xC	a
drancováním	drancování	k1gNnSc7	drancování
na	na	k7c6	na
konstantinopolských	konstantinopolský	k2eAgInPc6d1	konstantinopolský
předměstích	předměstí	k1gNnPc6	předměstí
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Alexios	Alexios	k1gMnSc1	Alexios
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
a	a	k8xC	a
dodávky	dodávka	k1gFnPc4	dodávka
obnovil	obnovit	k5eAaPmAgMnS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
velikonocemi	velikonoce	k1gFnPc7	velikonoce
Alexios	Alexios	k1gMnSc1	Alexios
znovu	znovu	k6eAd1	znovu
omezil	omezit	k5eAaPmAgMnS	omezit
zásobování	zásobování	k1gNnSc4	zásobování
<g/>
,	,	kIx,	,
křižáci	křižák	k1gMnPc1	křižák
obnovili	obnovit	k5eAaPmAgMnP	obnovit
plenění	plenění	k1gNnSc4	plenění
a	a	k8xC	a
Balduin	Balduin	k1gInSc1	Balduin
dokonce	dokonce	k9	dokonce
přesvědčil	přesvědčit	k5eAaPmAgInS	přesvědčit
Godefroye	Godefroye	k1gNnSc4	Godefroye
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
se	se	k3xPyFc4	se
seskupili	seskupit	k5eAaPmAgMnP	seskupit
a	a	k8xC	a
na	na	k7c4	na
zelený	zelený	k2eAgInSc4d1	zelený
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
městskou	městský	k2eAgFnSc4d1	městská
bránu	brána	k1gFnSc4	brána
pod	pod	k7c7	pod
palácem	palác	k1gInSc7	palác
Blachernae	Blacherna	k1gInSc2	Blacherna
<g/>
.	.	kIx.	.
</s>
<s>
Byzantinci	Byzantinec	k1gMnPc1	Byzantinec
byli	být	k5eAaImAgMnP	být
dokonale	dokonale	k6eAd1	dokonale
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
<g/>
,	,	kIx,	,
brány	brána	k1gFnSc2	brána
však	však	k8xC	však
útoku	útok	k1gInSc2	útok
odolaly	odolat	k5eAaPmAgFnP	odolat
<g/>
.	.	kIx.	.
</s>
<s>
Alexios	Alexios	k1gMnSc1	Alexios
poté	poté	k6eAd1	poté
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
protiútoku	protiútok	k1gInSc2	protiútok
a	a	k8xC	a
křižáky	křižák	k1gInPc4	křižák
rozehnal	rozehnat	k5eAaPmAgMnS	rozehnat
<g/>
.	.	kIx.	.
</s>
<s>
Godefroy	Godefroa	k1gFnPc1	Godefroa
si	se	k3xPyFc3	se
posléze	posléze	k6eAd1	posléze
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnSc1d1	další
roztržka	roztržka	k1gFnSc1	roztržka
s	s	k7c7	s
byzantským	byzantský	k2eAgNnSc7d1	byzantské
vojskem	vojsko	k1gNnSc7	vojsko
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
znamenat	znamenat	k5eAaImF	znamenat
pohromu	pohroma	k1gFnSc4	pohroma
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
změnit	změnit	k5eAaPmF	změnit
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
a	a	k8xC	a
Alexiovi	Alexiův	k2eAgMnPc1d1	Alexiův
lenní	lenní	k2eAgMnPc1d1	lenní
přísahu	přísaha	k1gFnSc4	přísaha
složit	složit	k5eAaPmF	složit
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
přísaha	přísaha	k1gFnSc1	přísaha
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c6	na
velikonoční	velikonoční	k2eAgFnSc6d1	velikonoční
neděli	neděle	k1gFnSc6	neděle
roku	rok	k1gInSc2	rok
1097	[number]	k4	1097
<g/>
.	.	kIx.	.
</s>
<s>
Godefroy	Godefro	k1gMnPc4	Godefro
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
feudálové	feudál	k1gMnPc1	feudál
byli	být	k5eAaImAgMnP	být
královsky	královsky	k6eAd1	královsky
přijati	přijat	k2eAgMnPc1d1	přijat
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
smíru	smír	k1gInSc2	smír
pozdravil	pozdravit	k5eAaPmAgMnS	pozdravit
polibkem	polibek	k1gInSc7	polibek
jako	jako	k8xS	jako
adoptivní	adoptivní	k2eAgMnPc4d1	adoptivní
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přísaha	přísaha	k1gFnSc1	přísaha
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
slibem	slib	k1gInSc7	slib
manské	manský	k2eAgFnPc4d1	manská
věrnosti	věrnost	k1gFnPc4	věrnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Godefroy	Godefroy	k1gInPc7	Godefroy
stane	stanout	k5eAaPmIp3nS	stanout
císařovým	císařův	k2eAgMnSc7d1	císařův
sluhou	sluha	k1gMnSc7	sluha
vystaveným	vystavený	k2eAgFnPc3d1	vystavená
Alexiově	Alexiův	k2eAgFnSc3d1	Alexiova
vůli	vůle	k1gFnSc3	vůle
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
spíše	spíše	k9	spíše
smlouvou	smlouva	k1gFnSc7	smlouva
mezi	mezi	k7c7	mezi
suverénními	suverénní	k2eAgFnPc7d1	suverénní
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byli	být	k5eAaImAgMnP	být
Lotrinčané	Lotrinčan	k1gMnPc1	Lotrinčan
Alexiem	Alexius	k1gMnSc7	Alexius
zahrnuti	zahrnout	k5eAaPmNgMnP	zahrnout
dary	dar	k1gInPc7	dar
a	a	k8xC	a
radami	rada	k1gFnPc7	rada
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
na	na	k7c6	na
byzantských	byzantský	k2eAgFnPc6d1	byzantská
lodích	loď	k1gFnPc6	loď
přesunuti	přesunout	k5eAaPmNgMnP	přesunout
na	na	k7c4	na
asijskou	asijský	k2eAgFnSc4d1	asijská
pevninu	pevnina	k1gFnSc4	pevnina
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
u	u	k7c2	u
Nikomedie	Nikomedie	k1gFnSc2	Nikomedie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
dlel	dlít	k5eAaImAgInS	dlít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
armádou	armáda	k1gFnSc7	armáda
Hugo	Hugo	k1gMnSc1	Hugo
z	z	k7c2	z
Vermandois	Vermandois	k1gFnSc2	Vermandois
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1096	[number]	k4	1096
se	se	k3xPyFc4	se
v	v	k7c6	v
italských	italský	k2eAgInPc6d1	italský
přístavech	přístav	k1gInPc6	přístav
Brindisi	Brindisi	k1gNnSc4	Brindisi
<g/>
,	,	kIx,	,
Otranto	Otranta	k1gFnSc5	Otranta
a	a	k8xC	a
Bari	Bari	k1gNnSc7	Bari
nalodila	nalodit	k5eAaPmAgFnS	nalodit
výprava	výprava	k1gFnSc1	výprava
jihoitalských	jihoitalský	k2eAgMnPc2d1	jihoitalský
Normanů	Norman	k1gMnPc2	Norman
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgInSc4d1	čítající
na	na	k7c4	na
500	[number]	k4	500
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
3500	[number]	k4	3500
pěších	pěší	k2eAgMnPc2d1	pěší
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
knížetem	kníže	k1gMnSc7	kníže
Bohemundem	Bohemund	k1gMnSc7	Bohemund
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výprava	výprava	k1gFnSc1	výprava
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
lotrinská	lotrinský	k2eAgFnSc1d1	lotrinská
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
lépe	dobře	k6eAd2	dobře
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
a	a	k8xC	a
zkušenější	zkušený	k2eAgMnSc1d2	zkušenější
a	a	k8xC	a
Bohemund	Bohemund	k1gMnSc1	Bohemund
byl	být	k5eAaImAgMnS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
zkušenosti	zkušenost	k1gFnPc4	zkušenost
jak	jak	k8xC	jak
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
se	s	k7c7	s
Saracény	Saracén	k1gMnPc7	Saracén
<g/>
,	,	kIx,	,
tak	tak	k9	tak
s	s	k7c7	s
Byzantinci	Byzantinec	k1gMnPc7	Byzantinec
a	a	k8xC	a
právě	právě	k6eAd1	právě
jeho	jeho	k3xOp3gFnSc4	jeho
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
výpravě	výprava	k1gFnSc6	výprava
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
v	v	k7c6	v
císaři	císař	k1gMnSc6	císař
Alexiovi	Alexius	k1gMnSc6	Alexius
největší	veliký	k2eAgFnSc4d3	veliký
obavy	obava	k1gFnPc4	obava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohemund	Bohemund	k1gMnSc1	Bohemund
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
chápal	chápat	k5eAaImAgInS	chápat
především	především	k6eAd1	především
jako	jako	k8xC	jako
vojenský	vojenský	k2eAgInSc1d1	vojenský
podnik	podnik	k1gInSc1	podnik
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
vydobytí	vydobytí	k1gNnSc2	vydobytí
si	se	k3xPyFc3	se
vlastního	vlastní	k2eAgNnSc2d1	vlastní
panství	panství	k1gNnSc3	panství
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
plavbě	plavba	k1gFnSc6	plavba
se	se	k3xPyFc4	se
vylodil	vylodit	k5eAaPmAgMnS	vylodit
v	v	k7c6	v
Epiru	Epir	k1gInSc6	Epir
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
byzantských	byzantský	k2eAgMnPc2d1	byzantský
průvodců	průvodce	k1gMnPc2	průvodce
protáhl	protáhnout	k5eAaPmAgInS	protáhnout
severním	severní	k2eAgNnSc7d1	severní
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
před	před	k7c7	před
velikonočním	velikonoční	k2eAgInSc7d1	velikonoční
svátky	svátek	k1gInPc4	svátek
dorazil	dorazit	k5eAaPmAgMnS	dorazit
ke	k	k7c3	k
Konstantinopoli	Konstantinopol	k1gInSc3	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
Normané	Norman	k1gMnPc1	Norman
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
město	město	k1gNnSc4	město
Kastoria	Kastorium	k1gNnSc2	Kastorium
<g/>
,	,	kIx,	,
či	či	k8xC	či
města	město	k1gNnPc1	město
obývaná	obývaný	k2eAgNnPc1d1	obývané
příslušníky	příslušník	k1gMnPc7	příslušník
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
sekty	sekta	k1gFnSc2	sekta
manichejců	manichejec	k1gMnPc2	manichejec
a	a	k8xC	a
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
února	únor	k1gInSc2	únor
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
císařskými	císařský	k2eAgMnPc7d1	císařský
žoldnéři	žoldnéř	k1gMnPc7	žoldnéř
z	z	k7c2	z
pohanského	pohanský	k2eAgInSc2d1	pohanský
kmene	kmen	k1gInSc2	kmen
Pečeněhů	Pečeněh	k1gInPc2	Pečeněh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1096	[number]	k4	1096
byl	být	k5eAaImAgInS	být
Bohemund	Bohemund	k1gInSc1	Bohemund
okázale	okázale	k6eAd1	okázale
přivítán	přivítat	k5eAaPmNgInS	přivítat
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
složil	složit	k5eAaPmAgMnS	složit
Alexiovi	Alexius	k1gMnSc3	Alexius
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
.	.	kIx.	.
</s>
<s>
Doufal	doufat	k5eAaImAgMnS	doufat
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc7	svůj
spoluprací	spolupráce	k1gFnSc7	spolupráce
získá	získat	k5eAaPmIp3nS	získat
Alexiovu	Alexiův	k2eAgFnSc4d1	Alexiova
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
titul	titul	k1gInSc4	titul
řecký	řecký	k2eAgInSc4d1	řecký
domestikos	domestikos	k1gInSc4	domestikos
tes	tes	k1gInSc4	tes
anatoles	anatolesa	k1gFnPc2	anatolesa
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jmenování	jmenování	k1gNnSc4	jmenování
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
výprava	výprava	k1gFnSc1	výprava
stala	stát	k5eAaPmAgFnS	stát
nástrojem	nástroj	k1gInSc7	nástroj
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInPc4	jeho
vlastní	vlastní	k2eAgInPc4d1	vlastní
dobyvačné	dobyvačný	k2eAgInPc4d1	dobyvačný
plány	plán	k1gInPc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Alexios	Alexios	k1gInSc1	Alexios
však	však	k9	však
jeho	on	k3xPp3gInSc4	on
záměr	záměr	k1gInSc4	záměr
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
a	a	k8xC	a
diplomaticky	diplomaticky	k6eAd1	diplomaticky
mu	on	k3xPp3gMnSc3	on
velení	velení	k1gNnSc2	velení
nepředal	předat	k5eNaPmAgMnS	předat
a	a	k8xC	a
Bohemund	Bohemund	k1gMnSc1	Bohemund
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
přísahu	přísaha	k1gFnSc4	přísaha
nezískal	získat	k5eNaPmAgMnS	získat
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
prudký	prudký	k2eAgMnSc1d1	prudký
synovec	synovec	k1gMnSc1	synovec
Tankred	Tankred	k1gMnSc1	Tankred
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
normanští	normanský	k2eAgMnPc1d1	normanský
velmoži	velmož	k1gMnPc1	velmož
přísahat	přísahat	k5eAaImF	přísahat
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
a	a	k8xC	a
dali	dát	k5eAaPmAgMnP	dát
se	se	k3xPyFc4	se
přepravit	přepravit	k5eAaPmF	přepravit
k	k	k7c3	k
Nikomedii	Nikomedie	k1gFnSc3	Nikomedie
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Bohemunda	Bohemund	k1gMnSc2	Bohemund
bohatě	bohatě	k6eAd1	bohatě
obdaroval	obdarovat	k5eAaPmAgMnS	obdarovat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
vůdce	vůdce	k1gMnPc4	vůdce
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
věrnost	věrnost	k1gFnSc1	věrnost
odpřisáhli	odpřisáhnout	k5eAaPmAgMnP	odpřisáhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1097	[number]	k4	1097
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
Bohemund	Bohemund	k1gInSc1	Bohemund
i	i	k9	i
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
jednotkami	jednotka	k1gFnPc7	jednotka
v	v	k7c6	v
Anatolii	Anatolie	k1gFnSc6	Anatolie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojsko	vojsko	k1gNnSc1	vojsko
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
až	až	k9	až
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1096	[number]	k4	1096
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
vůdcem	vůdce	k1gMnSc7	vůdce
byli	být	k5eAaImAgMnP	být
Raimond	Raimond	k1gMnSc1	Raimond
de	de	k?	de
Saint-Gilles	Saint-Gilles	k1gMnSc1	Saint-Gilles
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Toulouse	Toulouse	k1gInSc2	Toulouse
a	a	k8xC	a
markýz	markýz	k1gMnSc1	markýz
provensálský	provensálský	k2eAgMnSc1d1	provensálský
<g/>
,	,	kIx,	,
a	a	k8xC	a
papežský	papežský	k2eAgMnSc1d1	papežský
legát	legát	k1gMnSc1	legát
Adhémar	Adhémar	k1gMnSc1	Adhémar
z	z	k7c2	z
Le	Le	k1gMnSc2	Le
Puy	Puy	k1gMnSc2	Puy
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
Provence	Provence	k1gFnSc2	Provence
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
Raimondovým	Raimondový	k2eAgFnPc3d1	Raimondová
poddaným	poddaná	k1gFnPc3	poddaná
<g/>
.	.	kIx.	.
</s>
<s>
Raimond	Raimond	k1gMnSc1	Raimond
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
feudálů	feudál	k1gMnPc2	feudál
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
leníkem	leník	k1gMnSc7	leník
jak	jak	k8xC	jak
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
římsko-německého	římskoěmecký	k2eAgMnSc2d1	římsko-německý
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
bojem	boj	k1gInSc7	boj
s	s	k7c7	s
Maury	Maur	k1gMnPc7	Maur
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nadcházející	nadcházející	k2eAgNnSc4d1	nadcházející
tažení	tažení	k1gNnSc4	tažení
rozprodal	rozprodat	k5eAaPmAgInS	rozprodat
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dal	dát	k5eAaPmAgMnS	dát
také	také	k6eAd1	také
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
hodlá	hodlat	k5eAaImIp3nS	hodlat
usadit	usadit	k5eAaPmF	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
1200	[number]	k4	1200
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
8500	[number]	k4	8500
pěších	pěší	k1gMnPc2	pěší
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jeho	jeho	k3xOp3gFnSc4	jeho
výpravu	výprava	k1gFnSc4	výprava
činilo	činit	k5eAaImAgNnS	činit
největší	veliký	k2eAgNnSc1d3	veliký
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
křižáckých	křižácký	k2eAgMnPc2d1	křižácký
kontingentů	kontingent	k1gInPc2	kontingent
<g/>
.	.	kIx.	.
</s>
<s>
Provensálci	Provensálec	k1gMnPc1	Provensálec
táhli	táhnout	k5eAaImAgMnP	táhnout
severní	severní	k2eAgFnSc3d1	severní
Itálii	Itálie	k1gFnSc3	Itálie
a	a	k8xC	a
Dalmácií	Dalmácie	k1gFnPc2	Dalmácie
při	při	k7c6	při
jaderském	jaderský	k2eAgNnSc6d1	Jaderské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
špatné	špatný	k2eAgNnSc4d1	špatné
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
cesty	cesta	k1gFnPc1	cesta
byly	být	k5eAaImAgFnP	být
neschůdné	schůdný	k2eNgFnPc1d1	neschůdná
a	a	k8xC	a
místní	místní	k2eAgMnPc1d1	místní
Slované	Slovan	k1gMnPc1	Slovan
neustále	neustále	k6eAd1	neustále
křižáky	křižák	k1gInPc4	křižák
přepadali	přepadat	k5eAaImAgMnP	přepadat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřiceti	čtyřicet	k4xCc6	čtyřicet
dnech	den	k1gInPc6	den
těžkého	těžký	k2eAgNnSc2d1	těžké
putování	putování	k1gNnSc2	putování
křižáci	křižák	k1gMnPc1	křižák
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
Dyrrhachia	Dyrrhachium	k1gNnSc2	Dyrrhachium
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
doprovod	doprovod	k1gInSc1	doprovod
Pečeněhů	Pečeněh	k1gInPc2	Pečeněh
měl	mít	k5eAaImAgInS	mít
doprovodit	doprovodit	k5eAaPmF	doprovodit
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
křižáky	křižák	k1gMnPc7	křižák
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
doprovodem	doprovod	k1gInSc7	doprovod
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
střetům	střet	k1gInPc3	střet
a	a	k8xC	a
cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
plundrovalo	plundrovat	k5eAaImAgNnS	plundrovat
<g/>
,	,	kIx,	,
zranění	zranění	k1gNnSc3	zranění
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
i	i	k8xC	i
biskup	biskup	k1gMnSc1	biskup
Adhémar	Adhémar	k1gMnSc1	Adhémar
<g/>
,	,	kIx,	,
což	což	k9	což
provensálské	provensálský	k2eAgMnPc4d1	provensálský
poutníky	poutník	k1gMnPc4	poutník
velmi	velmi	k6eAd1	velmi
pobouřilo	pobouřit	k5eAaPmAgNnS	pobouřit
<g/>
.	.	kIx.	.
</s>
<s>
Raimond	Raimond	k1gMnSc1	Raimond
spěchal	spěchat	k5eAaImAgMnS	spěchat
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
doslechl	doslechnout	k5eAaPmAgMnS	doslechnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bohemund	Bohemund	k1gInSc1	Bohemund
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
vrchního	vrchní	k2eAgNnSc2d1	vrchní
velení	velení	k1gNnSc2	velení
nad	nad	k7c7	nad
výpravou	výprava	k1gFnSc7	výprava
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
hrabě	hrabě	k1gMnSc1	hrabě
nehodlal	hodlat	k5eNaImAgMnS	hodlat
akceptovat	akceptovat	k5eAaBmF	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
Raimond	Raimond	k1gInSc1	Raimond
odmítal	odmítat	k5eAaImAgInS	odmítat
císaři	císař	k1gMnSc3	císař
přísahat	přísahat	k5eAaImF	přísahat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bohemund	Bohemund	k1gInSc1	Bohemund
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
bude	být	k5eAaImBp3nS	být
jeho	jeho	k3xOp3gMnPc3	jeho
nadřízeným	nadřízený	k1gMnPc3	nadřízený
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
přemlouvání	přemlouvání	k1gNnPc2	přemlouvání
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
a	a	k8xC	a
koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
byli	být	k5eAaImAgMnP	být
Provensálci	Provensálec	k1gMnPc1	Provensálec
přeplaveni	přeplavit	k5eAaPmNgMnP	přeplavit
do	do	k7c2	do
Nikomedie	Nikomedie	k1gFnSc2	Nikomedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
poslední	poslední	k2eAgFnPc1d1	poslední
vyrazily	vyrazit	k5eAaPmAgFnP	vyrazit
skupiny	skupina	k1gFnPc1	skupina
z	z	k7c2	z
Flander	Flandry	k1gInPc2	Flandry
<g/>
,	,	kIx,	,
Normandie	Normandie	k1gFnSc2	Normandie
a	a	k8xC	a
Blois	Blois	k1gFnSc2	Blois
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
Robertem	Robert	k1gMnSc7	Robert
Flanderským	flanderský	k2eAgMnSc7d1	flanderský
<g/>
,	,	kIx,	,
Robertem	Robert	k1gMnSc7	Robert
Normandským	normandský	k2eAgMnSc7d1	normandský
a	a	k8xC	a
Štěpánem	Štěpán	k1gMnSc7	Štěpán
z	z	k7c2	z
Blois	Blois	k1gFnSc2	Blois
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
v	v	k7c6	v
těchto	tento	k3xDgFnPc2	tento
výprav	výprava	k1gFnPc2	výprava
čítala	čítat	k5eAaImAgFnS	čítat
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
účastníků	účastník	k1gMnPc2	účastník
<g/>
,	,	kIx,	,
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
poutníků	poutník	k1gMnPc2	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
Robertové	Robert	k1gMnPc1	Robert
táhli	táhnout	k5eAaImAgMnP	táhnout
většinou	většina	k1gFnSc7	většina
společně	společně	k6eAd1	společně
přes	přes	k7c4	přes
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lucce	Lucka	k1gFnSc6	Lucka
se	se	k3xPyFc4	se
křižáci	křižák	k1gMnPc1	křižák
setali	setat	k5eAaImAgMnP	setat
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
Urbanem	Urban	k1gMnSc7	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
od	od	k7c2	od
křižáků	křižák	k1gInPc2	křižák
požadoval	požadovat	k5eAaImAgMnS	požadovat
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
znovudobytí	znovudobytí	k1gNnSc6	znovudobytí
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
obsazeného	obsazený	k2eAgNnSc2d1	obsazené
německými	německý	k2eAgNnPc7d1	německé
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
křižáci	křižák	k1gMnPc1	křižák
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Apulii	Apulie	k1gFnSc6	Apulie
byli	být	k5eAaImAgMnP	být
Normani	Norman	k1gMnPc1	Norman
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
přivítáni	přivítat	k5eAaPmNgMnP	přivítat
jihoitalskými	jihoitalský	k2eAgMnPc7d1	jihoitalský
Normany	Norman	k1gMnPc7	Norman
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
normandského	normandský	k2eAgMnSc4d1	normandský
vévodu	vévoda	k1gMnSc4	vévoda
stále	stále	k6eAd1	stále
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Normandský	normandský	k2eAgMnSc1d1	normandský
se	s	k7c7	s
Štěpánem	Štěpán	k1gMnSc7	Štěpán
z	z	k7c2	z
Blois	Blois	k1gFnSc2	Blois
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
přezimovat	přezimovat	k5eAaBmF	přezimovat
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
normanských	normanský	k2eAgMnPc2d1	normanský
hostitelů	hostitel	k1gMnPc2	hostitel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Robert	Robert	k1gMnSc1	Robert
Flanderský	flanderský	k2eAgMnSc1d1	flanderský
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1096	[number]	k4	1096
nalodil	nalodit	k5eAaPmAgMnS	nalodit
v	v	k7c6	v
Bari	Bari	k1gNnSc6	Bari
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
Bohemundovi	Bohemund	k1gMnSc6	Bohemund
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Flandry	Flandry	k1gInPc4	Flandry
měli	mít	k5eAaImAgMnP	mít
s	s	k7c7	s
Byzancí	Byzanc	k1gFnSc7	Byzanc
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
neváhal	váhat	k5eNaImAgMnS	váhat
Robert	Robert	k1gMnSc1	Robert
Alexiovi	Alexius	k1gMnSc3	Alexius
složit	složit	k5eAaPmF	složit
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1097	[number]	k4	1097
se	se	k3xPyFc4	se
v	v	k7c6	v
Brindisi	Brindisi	k1gNnSc6	Brindisi
nalodili	nalodit	k5eAaPmAgMnP	nalodit
Štěpán	Štěpán	k1gMnSc1	Štěpán
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nechali	nechat	k5eAaPmAgMnP	nechat
přepravit	přepravit	k5eAaPmF	přepravit
do	do	k7c2	do
Dyrrhachia	Dyrrhachium	k1gNnSc2	Dyrrhachium
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
naloďování	naloďování	k1gNnSc2	naloďování
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
potopilo	potopit	k5eAaPmAgNnS	potopit
několik	několik	k4yIc1	několik
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
vojsko	vojsko	k1gNnSc1	vojsko
také	také	k9	také
dlouhým	dlouhý	k2eAgNnSc7d1	dlouhé
čekáním	čekání	k1gNnSc7	čekání
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
mělo	mít	k5eAaImAgNnS	mít
špatnou	špatný	k2eAgFnSc4d1	špatná
morálku	morálka	k1gFnSc4	morálka
a	a	k8xC	a
trpělo	trpět	k5eAaImAgNnS	trpět
dezercemi	dezerce	k1gFnPc7	dezerce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
až	až	k9	až
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1097	[number]	k4	1097
<g/>
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
i	i	k9	i
Robert	Robert	k1gMnSc1	Robert
Normandský	normandský	k2eAgMnSc1d1	normandský
složili	složit	k5eAaPmAgMnP	složit
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
přísahu	přísaha	k1gFnSc4	přísaha
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
rovněž	rovněž	k9	rovněž
přepraveni	přepraven	k2eAgMnPc1d1	přepraven
do	do	k7c2	do
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
ostatní	ostatní	k2eAgMnPc1d1	ostatní
křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
byzantským	byzantský	k2eAgNnSc7d1	byzantské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
napadli	napadnout	k5eAaPmAgMnP	napadnout
Nikaiu	Nikaium	k1gNnSc3	Nikaium
<g/>
,	,	kIx,	,
mocné	mocný	k2eAgNnSc1d1	mocné
řecké	řecký	k2eAgNnSc1d1	řecké
město	město	k1gNnSc1	město
v	v	k7c6	v
tureckých	turecký	k2eAgFnPc6d1	turecká
rukách	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Křižácká	křižácký	k2eAgFnSc1d1	křižácká
armáda	armáda	k1gFnSc1	armáda
celkem	celkem	k6eAd1	celkem
čítala	čítat	k5eAaImAgFnS	čítat
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
účastníků	účastník	k1gMnPc2	účastník
<g/>
,	,	kIx,	,
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
rytířů	rytíř	k1gMnPc2	rytíř
i	i	k8xC	i
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
velikost	velikost	k1gFnSc4	velikost
Řekům	Řek	k1gMnPc3	Řek
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
imponovala	imponovat	k5eAaImAgFnS	imponovat
<g/>
,	,	kIx,	,
představovala	představovat	k5eAaImAgFnS	představovat
největší	veliký	k2eAgFnSc4d3	veliký
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
Evropa	Evropa	k1gFnSc1	Evropa
postavila	postavit	k5eAaPmAgFnS	postavit
od	od	k7c2	od
pádu	pád	k1gInSc2	pád
Západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nikaia	Nikaia	k1gFnSc1	Nikaia
a	a	k8xC	a
cesta	cesta	k1gFnSc1	cesta
Anatolií	Anatolie	k1gFnPc2	Anatolie
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
Godefroy	Godefroa	k1gFnPc4	Godefroa
a	a	k8xC	a
Bohemund	Bohemund	k1gInSc4	Bohemund
jako	jako	k8xS	jako
předvoj	předvoj	k1gInSc4	předvoj
na	na	k7c4	na
turecké	turecký	k2eAgNnSc4d1	turecké
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
postupovala	postupovat	k5eAaImAgFnS	postupovat
byzantská	byzantský	k2eAgFnSc1d1	byzantská
žoldnéřská	žoldnéřský	k2eAgFnSc1d1	žoldnéřská
armáda	armáda	k1gFnSc1	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
primikeria	primikerium	k1gNnSc2	primikerium
(	(	kIx(	(
<g/>
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
byzantských	byzantský	k2eAgNnPc2d1	byzantské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
)	)	kIx)	)
Tatikia	Tatikius	k1gMnSc4	Tatikius
a	a	k8xC	a
admirála	admirál	k1gMnSc4	admirál
Manuela	Manuel	k1gMnSc4	Manuel
Butumita	Butumit	k1gMnSc4	Butumit
<g/>
.	.	kIx.	.
</s>
<s>
Nikája	Nikája	k1gFnSc1	Nikája
byla	být	k5eAaImAgFnS	být
silným	silný	k2eAgInSc7d1	silný
seldžuckým	seldžucký	k2eAgInSc7d1	seldžucký
opěrným	opěrný	k2eAgInSc7d1	opěrný
bodem	bod	k1gInSc7	bod
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
posádkou	posádka	k1gFnSc7	posádka
<g/>
,	,	kIx,	,
kontrolující	kontrolující	k2eAgFnSc4d1	kontrolující
hlavní	hlavní	k2eAgFnSc4d1	hlavní
trasu	trasa	k1gFnSc4	trasa
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
křižáci	křižák	k1gMnPc1	křižák
nemohli	moct	k5eNaImAgMnP	moct
obejít	obejít	k5eAaPmF	obejít
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Křižácké	křižácký	k2eAgNnSc1d1	křižácké
vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
největší	veliký	k2eAgFnSc7d3	veliký
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc7	jaký
kdy	kdy	k6eAd1	kdy
Evropané	Evropan	k1gMnPc1	Evropan
postavili	postavit	k5eAaPmAgMnP	postavit
od	od	k7c2	od
pádu	pád	k1gInSc2	pád
Západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
křižáci	křižák	k1gMnPc1	křižák
dorazili	dorazit	k5eAaPmAgMnP	dorazit
k	k	k7c3	k
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
opevněné	opevněný	k2eAgNnSc1d1	opevněné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
křižáci	křižák	k1gMnPc1	křižák
přejít	přejít	k5eAaPmF	přejít
do	do	k7c2	do
frontálního	frontální	k2eAgInSc2d1	frontální
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
uchýlit	uchýlit	k5eAaPmF	uchýlit
k	k	k7c3	k
obléhání	obléhání	k1gNnSc3	obléhání
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
neměli	mít	k5eNaImAgMnP	mít
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
velitele	velitel	k1gMnSc4	velitel
<g/>
,	,	kIx,	,
velení	velení	k1gNnSc4	velení
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
válečná	válečný	k2eAgFnSc1d1	válečná
rada	rada	k1gFnSc1	rada
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
křižáckých	křižácký	k2eAgInPc2d1	křižácký
kontingentů	kontingent	k1gInPc2	kontingent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sultán	sultán	k1gMnSc1	sultán
Kilič	Kilič	k1gMnSc1	Kilič
Arslan	Arslan	k1gMnSc1	Arslan
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
na	na	k7c6	na
východě	východ	k1gInSc6	východ
své	svůj	k3xOyFgFnSc2	svůj
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
schylovalo	schylovat	k5eAaImAgNnS	schylovat
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gNnSc7	on
a	a	k8xC	a
danišmendovskými	danišmendovský	k2eAgInPc7d1	danišmendovský
Turky	turek	k1gInPc7	turek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
křižáci	křižák	k1gMnPc1	křižák
přepluli	přeplout	k5eAaPmAgMnP	přeplout
Bospor	Bospor	k1gInSc4	Bospor
<g/>
,	,	kIx,	,
jen	jen	k9	jen
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
mávl	mávnout	k5eAaPmAgMnS	mávnout
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
zkušenostech	zkušenost	k1gFnPc6	zkušenost
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Poustevníkem	poustevník	k1gMnSc7	poustevník
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
poutníky	poutník	k1gMnPc7	poutník
neměl	mít	k5eNaImAgMnS	mít
o	o	k7c6	o
křižácích	křižák	k1gInPc6	křižák
valného	valný	k2eAgNnSc2d1	Valné
mínění	mínění	k1gNnSc2	mínění
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
však	však	k9	však
od	od	k7c2	od
Nikáje	Nikáj	k1gInSc2	Nikáj
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
poslové	posel	k1gMnPc1	posel
s	s	k7c7	s
naléhavými	naléhavý	k2eAgFnPc7d1	naléhavá
žádostmi	žádost	k1gFnPc7	žádost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Kilič	Kilič	k1gMnSc1	Kilič
Arslan	Arslan	k1gMnSc1	Arslan
tedy	tedy	k9	tedy
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
klid	klid	k1gInSc4	klid
zbraní	zbraň	k1gFnPc2	zbraň
s	s	k7c7	s
Danišmendovci	Danišmendovec	k1gMnPc7	Danišmendovec
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
Nikaiu	Nikaius	k1gMnSc3	Nikaius
pokusil	pokusit	k5eAaPmAgMnS	pokusit
osvobodit	osvobodit	k5eAaPmF	osvobodit
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
sultán	sultán	k1gMnSc1	sultán
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
křižácké	křižácký	k2eAgFnPc4d1	křižácká
pozice	pozice	k1gFnPc4	pozice
u	u	k7c2	u
jižního	jižní	k2eAgInSc2d1	jižní
úseku	úsek	k1gInSc2	úsek
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tábořili	tábořit	k5eAaImAgMnP	tábořit
Provensálci	Provensálec	k1gMnSc3	Provensálec
Raimona	Raimona	k1gFnSc1	Raimona
z	z	k7c2	z
Toulouse	Toulouse	k1gInSc2	Toulouse
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
s	s	k7c7	s
křižáky	křižák	k1gInPc7	křižák
utkat	utkat	k5eAaPmF	utkat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
zblízka	zblízka	k6eAd1	zblízka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jim	on	k3xPp3gMnPc3	on
nevyhovoval	vyhovovat	k5eNaImAgMnS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Turci	Turek	k1gMnPc1	Turek
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
náporu	nápor	k1gInSc6	nápor
nemohli	moct	k5eNaImAgMnP	moct
prorazit	prorazit	k5eAaPmF	prorazit
a	a	k8xC	a
Provensálcům	Provensálec	k1gMnPc3	Provensálec
po	po	k7c6	po
čase	čas	k1gInSc6	čas
začaly	začít	k5eAaPmAgFnP	začít
proudit	proudit	k5eAaPmF	proudit
posily	posila	k1gFnPc1	posila
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celodenní	celodenní	k2eAgFnSc6d1	celodenní
bitvě	bitva	k1gFnSc6	bitva
sultán	sultán	k1gMnSc1	sultán
boj	boj	k1gInSc4	boj
přerušil	přerušit	k5eAaPmAgMnS	přerušit
a	a	k8xC	a
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
měli	mít	k5eAaImAgMnP	mít
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Gentu	Gent	k1gInSc2	Gent
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
významným	významný	k2eAgMnSc7d1	významný
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
,	,	kIx,	,
položil	položit	k5eAaPmAgMnS	položit
život	život	k1gInSc4	život
na	na	k7c6	na
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vítězství	vítězství	k1gNnSc1	vítězství
nad	nad	k7c7	nad
Turky	turek	k1gInPc7	turek
naplnilo	naplnit	k5eAaPmAgNnS	naplnit
křižáky	křižák	k1gInPc4	křižák
velkým	velký	k2eAgNnPc3d1	velké
sebevědomím	sebevědomí	k1gNnPc3	sebevědomí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
turecká	turecký	k2eAgFnSc1d1	turecká
posádka	posádka	k1gFnSc1	posádka
v	v	k7c6	v
Nikáii	Nikáie	k1gFnSc6	Nikáie
vzdala	vzdát	k5eAaPmAgFnS	vzdát
Byzantincům	Byzantinec	k1gMnPc3	Byzantinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pak	pak	k6eAd1	pak
turecké	turecký	k2eAgMnPc4d1	turecký
obránce	obránce	k1gMnPc4	obránce
za	za	k7c4	za
výkupné	výkupné	k1gNnSc4	výkupné
propustili	propustit	k5eAaPmAgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
popudilo	popudit	k5eAaPmAgNnS	popudit
křižácké	křižácký	k2eAgMnPc4d1	křižácký
velitele	velitel	k1gMnPc4	velitel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
o	o	k7c6	o
svaté	svatý	k2eAgFnSc6d1	svatá
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
nevěřícími	nevěřící	k1gMnPc7	nevěřící
jinou	jiný	k2eAgFnSc4d1	jiná
představu	představa	k1gFnSc4	představa
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
křižáci	křižák	k1gMnPc1	křižák
získali	získat	k5eAaPmAgMnP	získat
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
bohatou	bohatý	k2eAgFnSc4d1	bohatá
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
především	především	k6eAd1	především
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Nikaie	Nikaie	k1gFnSc2	Nikaie
se	se	k3xPyFc4	se
křižáci	křižák	k1gMnPc1	křižák
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
;	;	kIx,	;
Normané	Norman	k1gMnPc1	Norman
<g/>
,	,	kIx,	,
Vlámové	Vlám	k1gMnPc1	Vlám
a	a	k8xC	a
Severofrancouzi	Severofrancouh	k1gMnPc1	Severofrancouh
vedení	vedení	k1gNnSc2	vedení
Bohemundem	Bohemund	k1gInSc7	Bohemund
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
první	první	k4xOgNnSc4	první
a	a	k8xC	a
s	s	k7c7	s
denním	denní	k2eAgInSc7d1	denní
odstupem	odstup	k1gInSc7	odstup
je	být	k5eAaImIp3nS	být
následovali	následovat	k5eAaImAgMnP	následovat
Jihofrancouzi	Jihofrancouze	k1gFnSc4	Jihofrancouze
Raimonda	Raimond	k1gMnSc2	Raimond
z	z	k7c2	z
Toulouse	Toulouse	k1gInSc2	Toulouse
a	a	k8xC	a
Lotrinčané	Lotrinčan	k1gMnPc1	Lotrinčan
Godefroye	Godefroye	k1gFnPc2	Godefroye
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
bratrů	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
Raimond	Raimond	k1gMnSc1	Raimond
tak	tak	k9	tak
Bohemund	Bohemund	k1gInSc4	Bohemund
si	se	k3xPyFc3	se
dělali	dělat	k5eAaImAgMnP	dělat
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
vrchní	vrchní	k2eAgNnSc4d1	vrchní
velení	velení	k1gNnSc4	velení
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc4	rozdělení
vojska	vojsko	k1gNnSc2	vojsko
byl	být	k5eAaImAgInS	být
dobrý	dobrý	k2eAgInSc1d1	dobrý
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
utišit	utišit	k5eAaPmF	utišit
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1097	[number]	k4	1097
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
skupina	skupina	k1gFnSc1	skupina
přepadena	přepaden	k2eAgFnSc1d1	přepadena
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mezitím	mezitím	k6eAd1	mezitím
nanovo	nanovo	k6eAd1	nanovo
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
Kilič	Kilič	k1gMnSc1	Kilič
Arslan	Arslan	k1gMnSc1	Arslan
<g/>
,	,	kIx,	,
posílené	posílený	k2eAgInPc4d1	posílený
o	o	k7c4	o
sbory	sbor	k1gInPc4	sbor
danišmendovských	danišmendovský	k2eAgMnPc2d1	danišmendovský
Turků	Turek	k1gMnPc2	Turek
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
Kilič	Kilič	k1gInSc4	Kilič
Arslan	Arslan	k1gInSc1	Arslan
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
spojenectví	spojenectví	k1gNnSc4	spojenectví
<g/>
,	,	kIx,	,
u	u	k7c2	u
městečka	městečko	k1gNnSc2	městečko
Dorylaion	Dorylaion	k1gInSc1	Dorylaion
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
bitvě	bitva	k1gFnSc6	bitva
zpočátku	zpočátku	k6eAd1	zpočátku
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
velkou	velký	k2eAgFnSc7d1	velká
pohyblivostí	pohyblivost	k1gFnSc7	pohyblivost
tureckých	turecký	k2eAgMnPc2d1	turecký
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
plné	plný	k2eAgFnPc4d1	plná
jízdy	jízda	k1gFnPc4	jízda
zasypali	zasypat	k5eAaPmAgMnP	zasypat
obrovským	obrovský	k2eAgNnSc7d1	obrovské
množstvím	množství	k1gNnSc7	množství
šípů	šíp	k1gInPc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Velitelskou	velitelský	k2eAgFnSc4d1	velitelská
iniciativu	iniciativa	k1gFnSc4	iniciativa
převzal	převzít	k5eAaPmAgInS	převzít
Bohemund	Bohemund	k1gInSc1	Bohemund
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
Normani	Norman	k1gMnPc1	Norman
ze	z	k7c2	z
Sicílie	Sicílie	k1gFnSc2	Sicílie
jako	jako	k8xC	jako
jediní	jediný	k2eAgMnPc1d1	jediný
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
boje	boj	k1gInSc2	boj
znali	znát	k5eAaImAgMnP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Bohemund	Bohemund	k1gMnSc1	Bohemund
rytířům	rytíř	k1gMnPc3	rytíř
přikázal	přikázat	k5eAaPmAgMnS	přikázat
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
krýt	krýt	k5eAaImF	krýt
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
asi	asi	k9	asi
čtyřicet	čtyřicet	k4xCc1	čtyřicet
rytířů	rytíř	k1gMnPc2	rytíř
ho	on	k3xPp3gMnSc4	on
neposlechlo	poslechnout	k5eNaPmAgNnS	poslechnout
a	a	k8xC	a
vyrazilo	vyrazit	k5eAaPmAgNnS	vyrazit
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
však	však	k9	však
byli	být	k5eAaImAgMnP	být
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
postříleni	postřílen	k2eAgMnPc1d1	postřílen
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
Bohemundův	Bohemundův	k2eAgMnSc1d1	Bohemundův
mladší	mladý	k2eAgMnSc1d2	mladší
synovec	synovec	k1gMnSc1	synovec
Guillaume	Guillaum	k1gInSc5	Guillaum
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
odpoledne	odpoledne	k6eAd1	odpoledne
dorazily	dorazit	k5eAaPmAgInP	dorazit
křižákům	křižák	k1gMnPc3	křižák
posily	posila	k1gFnSc2	posila
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
křižáci	křižák	k1gMnPc1	křižák
přešli	přejít	k5eAaPmAgMnP	přejít
do	do	k7c2	do
protiútoku	protiútok	k1gInSc2	protiútok
a	a	k8xC	a
Kilič	Kilič	k1gInSc4	Kilič
Arslanovu	Arslanův	k2eAgFnSc4d1	Arslanův
armádu	armáda	k1gFnSc4	armáda
obrátili	obrátit	k5eAaPmAgMnP	obrátit
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
<g/>
Křižáci	křižák	k1gMnPc1	křižák
táhli	táhnout	k5eAaImAgMnP	táhnout
dále	daleko	k6eAd2	daleko
Anatolií	Anatolie	k1gFnPc2	Anatolie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vojsku	vojsko	k1gNnSc6	vojsko
propukaly	propukat	k5eAaImAgFnP	propukat
nemoci	nemoc	k1gFnPc1	nemoc
a	a	k8xC	a
ve	v	k7c6	v
vyprahlé	vyprahlý	k2eAgFnSc6d1	vyprahlá
anatolské	anatolský	k2eAgFnSc6d1	Anatolská
pustině	pustina	k1gFnSc6	pustina
bylo	být	k5eAaImAgNnS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
sehnat	sehnat	k5eAaPmF	sehnat
dostatek	dostatek	k1gInSc4	dostatek
potravin	potravina	k1gFnPc2	potravina
i	i	k8xC	i
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
propukaly	propukat	k5eAaImAgFnP	propukat
rozepře	rozepře	k1gFnPc1	rozepře
mezi	mezi	k7c7	mezi
západními	západní	k2eAgMnPc7d1	západní
křižáky	křižák	k1gMnPc7	křižák
a	a	k8xC	a
Byzantinci	Byzantinec	k1gMnPc7	Byzantinec
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
obsadili	obsadit	k5eAaPmAgMnP	obsadit
opuštěné	opuštěný	k2eAgNnSc4d1	opuštěné
město	město	k1gNnSc4	město
Iconium	Iconium	k1gNnSc1	Iconium
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gMnPc1	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
před	před	k7c7	před
křižáky	křižák	k1gMnPc7	křižák
prchli	prchnout	k5eAaPmAgMnP	prchnout
do	do	k7c2	do
hor.	hor.	k?	hor.
Přes	přes	k7c4	přes
mnohé	mnohý	k2eAgMnPc4d1	mnohý
mrtvé	mrtvý	k1gMnPc4	mrtvý
si	se	k3xPyFc3	se
křižáci	křižák	k1gMnPc1	křižák
udržovali	udržovat	k5eAaImAgMnP	udržovat
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
morálku	morálka	k1gFnSc4	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
duše	duše	k1gFnPc1	duše
padlých	padlý	k1gMnPc2	padlý
putují	putovat	k5eAaImIp3nP	putovat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
ráje	ráj	k1gInSc2	ráj
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
slíbil	slíbit	k5eAaPmAgMnS	slíbit
papež	papež	k1gMnSc1	papež
Urban	Urban	k1gMnSc1	Urban
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Herakley	Heraklea	k1gFnSc2	Heraklea
asi	asi	k9	asi
130	[number]	k4	130
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
od	od	k7c2	od
Iconia	Iconium	k1gNnSc2	Iconium
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
křižáci	křižák	k1gMnPc1	křižák
znovu	znovu	k6eAd1	znovu
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
Kilič	Kilič	k1gMnSc1	Kilič
Arslanem	Arslan	k1gMnSc7	Arslan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bitva	bitva	k1gFnSc1	bitva
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
krátká	krátká	k1gFnSc1	krátká
a	a	k8xC	a
sultán	sultán	k1gMnSc1	sultán
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Bohemund	Bohemund	k1gInSc1	Bohemund
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
přešel	přejít	k5eAaPmAgMnS	přejít
s	s	k7c7	s
rytíři	rytíř	k1gMnPc7	rytíř
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
následně	následně	k6eAd1	následně
museli	muset	k5eAaImAgMnP	muset
přejít	přejít	k5eAaPmF	přejít
pohoří	pohoří	k1gNnSc4	pohoří
Antitaurus	Antitaurus	k1gInSc1	Antitaurus
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Křižáci	křižák	k1gMnPc1	křižák
dále	daleko	k6eAd2	daleko
táhli	táhnout	k5eAaImAgMnP	táhnout
po	po	k7c6	po
syrském	syrský	k2eAgNnSc6d1	syrské
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
byli	být	k5eAaImAgMnP	být
vystaveni	vystaven	k2eAgMnPc1d1	vystaven
nejen	nejen	k6eAd1	nejen
neustálé	neustálý	k2eAgFnSc3d1	neustálá
hrozbě	hrozba	k1gFnSc3	hrozba
vojenských	vojenský	k2eAgInPc2d1	vojenský
útoků	útok	k1gInPc2	útok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
úmornému	úmorný	k2eAgNnSc3d1	úmorné
vedru	vedro	k1gNnSc3	vedro
<g/>
,	,	kIx,	,
na	na	k7c4	na
jaké	jaký	k3yQgInPc4	jaký
nebyli	být	k5eNaImAgMnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
<g/>
,	,	kIx,	,
trpěli	trpět	k5eAaImAgMnP	trpět
žízní	žíznit	k5eAaImIp3nP	žíznit
i	i	k9	i
hladem	hlad	k1gInSc7	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Neměli	mít	k5eNaImAgMnP	mít
kam	kam	k6eAd1	kam
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byli	být	k5eAaImAgMnP	být
obklopeni	obklopen	k2eAgMnPc1d1	obklopen
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
a	a	k8xC	a
nemohli	moct	k5eNaImAgMnP	moct
čekat	čekat	k5eAaImF	čekat
příchod	příchod	k1gInSc4	příchod
čerstvých	čerstvý	k2eAgFnPc2d1	čerstvá
posil	posila	k1gFnPc2	posila
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
nepochybně	pochybně	k6eNd1	pochybně
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
jejich	jejich	k3xOp3gFnSc2	jejich
výjimečné	výjimečný	k2eAgFnSc2d1	výjimečná
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
brát	brát	k5eAaImF	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
muslimové	muslim	k1gMnPc1	muslim
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
nebyli	být	k5eNaImAgMnP	být
jednotní	jednotný	k2eAgMnPc1d1	jednotný
<g/>
.	.	kIx.	.
</s>
<s>
Rozděloval	rozdělovat	k5eAaImAgMnS	rozdělovat
je	být	k5eAaImIp3nS	být
především	především	k9	především
mocenský	mocenský	k2eAgInSc1d1	mocenský
zápas	zápas	k1gInSc1	zápas
sunnitských	sunnitský	k2eAgMnPc2d1	sunnitský
Seldžuků	Seldžuk	k1gMnPc2	Seldžuk
s	s	k7c7	s
šíitskými	šíitský	k2eAgInPc7d1	šíitský
Fátimovci	Fátimovec	k1gInPc7	Fátimovec
a	a	k8xC	a
soupeření	soupeření	k1gNnSc1	soupeření
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
tureckých	turecký	k2eAgMnPc2d1	turecký
vládců	vládce	k1gMnPc2	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byla	být	k5eAaImAgFnS	být
maloasijská	maloasijský	k2eAgFnSc1d1	maloasijská
Anatolie	Anatolie	k1gFnSc1	Anatolie
pouze	pouze	k6eAd1	pouze
okrajovým	okrajový	k2eAgNnSc7d1	okrajové
územím	území	k1gNnSc7	území
turecké	turecký	k2eAgFnSc2d1	turecká
zájmové	zájmový	k2eAgFnSc2d1	zájmová
sféry	sféra	k1gFnSc2	sféra
<g/>
,	,	kIx,	,
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
Palestiny	Palestina	k1gFnSc2	Palestina
usilovali	usilovat	k5eAaImAgMnP	usilovat
Seldžukové	Seldžuk	k1gMnPc1	Seldžuk
i	i	k8xC	i
Fátimovci	Fátimovec	k1gMnPc1	Fátimovec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
vojska	vojsko	k1gNnSc2	vojsko
dobyla	dobýt	k5eAaPmAgFnS	dobýt
roku	rok	k1gInSc2	rok
1095	[number]	k4	1095
nazpět	nazpět	k6eAd1	nazpět
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
oblasti	oblast	k1gFnSc6	oblast
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
nepřáteli	nepřítel	k1gMnPc7	nepřítel
křižáků	křižák	k1gInPc2	křižák
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
začala	začít	k5eAaPmAgFnS	začít
fátimovská	fátimovský	k2eAgFnSc1d1	fátimovská
diplomacie	diplomacie	k1gFnSc1	diplomacie
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
spojenectví	spojenectví	k1gNnSc6	spojenectví
s	s	k7c7	s
křižáckými	křižácký	k2eAgNnPc7d1	křižácké
vojsky	vojsko	k1gNnPc7	vojsko
proti	proti	k7c3	proti
Seldžukům	Seldžuk	k1gMnPc3	Seldžuk
a	a	k8xC	a
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
jim	on	k3xPp3gMnPc3	on
rozdělení	rozdělení	k1gNnSc6	rozdělení
území	území	k1gNnSc4	území
–	–	k?	–
<g/>
Sýrie	Sýrie	k1gFnSc1	Sýrie
měla	mít	k5eAaImAgFnS	mít
připadnout	připadnout	k5eAaPmF	připadnout
křižákům	křižák	k1gMnPc3	křižák
<g/>
,	,	kIx,	,
Palestina	Palestina	k1gFnSc1	Palestina
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Vycházela	vycházet	k5eAaImAgFnS	vycházet
přitom	přitom	k6eAd1	přitom
z	z	k7c2	z
mylného	mylný	k2eAgInSc2d1	mylný
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
císař	císař	k1gMnSc1	císař
Alexios	Alexios	k1gMnSc1	Alexios
I.	I.	kA	I.
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgInPc4d1	podobný
dohodě	dohoda	k1gFnSc6	dohoda
nakloněn	naklonit	k5eAaPmNgMnS	naklonit
a	a	k8xC	a
křižáci	křižák	k1gMnPc1	křižák
přicházejí	přicházet	k5eAaImIp3nP	přicházet
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
bojovníci	bojovník	k1gMnPc1	bojovník
se	se	k3xPyFc4	se
však	však	k9	však
vypravili	vypravit	k5eAaPmAgMnP	vypravit
do	do	k7c2	do
dalekých	daleký	k2eAgInPc2d1	daleký
krajů	kraj	k1gInPc2	kraj
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
Svatou	svatý	k2eAgFnSc4d1	svatá
zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
nevěřících	nevěřící	k1gMnPc2	nevěřící
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ji	on	k3xPp3gFnSc4	on
drží	držet	k5eAaImIp3nP	držet
bojovnější	bojovný	k2eAgMnPc1d2	bojovnější
sunnitští	sunnitský	k2eAgMnPc1d1	sunnitský
Turci	Turek	k1gMnPc1	Turek
nebo	nebo	k8xC	nebo
tolerantnější	tolerantní	k2eAgMnPc1d2	tolerantnější
šíitští	šíitský	k2eAgMnPc1d1	šíitský
Arabové	Arab	k1gMnPc1	Arab
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
nehrálo	hrát	k5eNaImAgNnS	hrát
žádnou	žádný	k3yNgFnSc4	žádný
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nezměrné	nezměrný	k2eAgNnSc1d1	nezměrné
úsilí	úsilí	k1gNnSc1	úsilí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
křižáci	křižák	k1gMnPc1	křižák
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
během	během	k7c2	během
namáhavého	namáhavý	k2eAgNnSc2d1	namáhavé
tažení	tažení	k1gNnSc2	tažení
Anatolií	Anatolie	k1gFnPc2	Anatolie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1098	[number]	k4	1098
korunováno	korunovat	k5eAaBmNgNnS	korunovat
prvními	první	k4xOgInPc7	první
úspěchy	úspěch	k1gInPc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jarních	jarní	k2eAgInPc2d1	jarní
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
Balduin	Balduin	k1gInSc1	Balduin
z	z	k7c2	z
Boulogne	Boulogn	k1gMnSc5	Boulogn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
bojovníci	bojovník	k1gMnPc1	bojovník
se	se	k3xPyFc4	se
odloučili	odloučit	k5eAaPmAgMnP	odloučit
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Edesse	Edessa	k1gFnSc6	Edessa
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgNnPc2d1	významné
center	centrum	k1gNnPc2	centrum
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
Eufratu	Eufrat	k1gInSc6	Eufrat
obývané	obývaný	k2eAgFnSc2d1	obývaná
monofyzitskými	monofyzitský	k2eAgMnPc7d1	monofyzitský
Armény	Armén	k1gMnPc7	Armén
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prchali	prchat	k5eAaImAgMnP	prchat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
před	před	k7c7	před
rozšiřující	rozšiřující	k2eAgFnSc7d1	rozšiřující
se	se	k3xPyFc4	se
mocí	moc	k1gFnSc7	moc
Seldžuků	Seldžuk	k1gInPc2	Seldžuk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
dostihla	dostihnout	k5eAaPmAgFnS	dostihnout
i	i	k9	i
tady	tady	k6eAd1	tady
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
křižáky	křižák	k1gMnPc4	křižák
pohlíželi	pohlížet	k5eAaImAgMnP	pohlížet
jako	jako	k8xC	jako
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
osvoboditele	osvoboditel	k1gMnPc4	osvoboditel
a	a	k8xC	a
v	v	k7c6	v
maloasijské	maloasijský	k2eAgFnSc6d1	maloasijská
Kilíkii	Kilíkie	k1gFnSc6	Kilíkie
dokonce	dokonce	k9	dokonce
využili	využít	k5eAaPmAgMnP	využít
jejich	jejich	k3xOp3gNnSc4	jejich
tažení	tažení	k1gNnSc4	tažení
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
vlastního	vlastní	k2eAgInSc2d1	vlastní
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
království	království	k1gNnSc1	království
zvaného	zvaný	k2eAgInSc2d1	zvaný
Malá	malý	k2eAgFnSc1d1	malá
Arménie	Arménie	k1gFnSc1	Arménie
<g/>
.	.	kIx.	.
</s>
<s>
Edessa	Edessa	k1gFnSc1	Edessa
se	s	k7c7	s
sesazením	sesazení	k1gNnSc7	sesazení
byzantského	byzantský	k2eAgMnSc2d1	byzantský
vládce	vládce	k1gMnSc2	vládce
Thora	Thor	k1gMnSc2	Thor
dostala	dostat	k5eAaPmAgFnS	dostat
přímo	přímo	k6eAd1	přímo
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
křižáků	křižák	k1gMnPc2	křižák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Balduin	Balduin	k1gMnSc1	Balduin
přijal	přijmout	k5eAaPmAgMnS	přijmout
titul	titul	k1gInSc4	titul
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Edessy	Edessa	k1gFnSc2	Edessa
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
si	se	k3xPyFc3	se
–	–	k?	–
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
lenní	lenní	k2eAgFnSc4d1	lenní
přísahu	přísaha	k1gFnSc4	přísaha
císaři	císař	k1gMnPc1	císař
–	–	k?	–
<g/>
počínat	počínat	k5eAaImF	počínat
jako	jako	k9	jako
suverénní	suverénní	k2eAgMnPc4d1	suverénní
vládce	vládce	k1gMnPc4	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
první	první	k4xOgInSc4	první
z	z	k7c2	z
křižáckých	křižácký	k2eAgInPc2d1	křižácký
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
stanul	stanout	k5eAaPmAgMnS	stanout
potomek	potomek	k1gMnSc1	potomek
významného	významný	k2eAgInSc2d1	významný
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
druhorozeným	druhorozený	k2eAgMnSc7d1	druhorozený
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
doma	doma	k6eAd1	doma
neměl	mít	k5eNaImAgInS	mít
vyhlídky	vyhlídka	k1gFnSc2	vyhlídka
získat	získat	k5eAaPmF	získat
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
z	z	k7c2	z
rodinného	rodinný	k2eAgNnSc2d1	rodinné
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Antiochie	Antiochie	k1gFnSc2	Antiochie
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
křižácké	křižácký	k2eAgNnSc1d1	křižácké
vojsko	vojsko	k1gNnSc1	vojsko
obléhalo	obléhat	k5eAaImAgNnS	obléhat
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1097	[number]	k4	1097
syrskou	syrský	k2eAgFnSc4d1	Syrská
Antiochii	Antiochie	k1gFnSc4	Antiochie
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
třetí	třetí	k4xOgNnSc4	třetí
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
město	město	k1gNnSc4	město
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
pevnost	pevnost	k1gFnSc4	pevnost
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
dobré	dobrá	k1gFnSc2	dobrá
udržované	udržovaný	k2eAgNnSc1d1	udržované
opevnění	opevnění	k1gNnSc1	opevnění
pocházelo	pocházet	k5eAaImAgNnS	pocházet
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
císaře	císař	k1gMnSc2	císař
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
a	a	k8xC	a
kde	kde	k6eAd1	kde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1085	[number]	k4	1085
vládli	vládnout	k5eAaImAgMnP	vládnout
Seldžukové	Seldžuk	k1gMnPc1	Seldžuk
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
řeckých	řecký	k2eAgMnPc2d1	řecký
a	a	k8xC	a
arménských	arménský	k2eAgMnPc2d1	arménský
obyvatel	obyvatel	k1gMnPc2	obyvatel
zůstala	zůstat	k5eAaPmAgFnS	zůstat
křesťany	křesťan	k1gMnPc4	křesťan
(	(	kIx(	(
<g/>
hlásili	hlásit	k5eAaImAgMnP	hlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
ortodoxnímu	ortodoxní	k2eAgNnSc3d1	ortodoxní
vyznání	vyznání	k1gNnSc3	vyznání
a	a	k8xC	a
k	k	k7c3	k
monofysitismu	monofysitismus	k1gInSc3	monofysitismus
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
Turky	Turek	k1gMnPc4	Turek
tolerována	tolerován	k2eAgFnSc1d1	tolerována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Antiochie	Antiochie	k1gFnSc2	Antiochie
se	se	k3xPyFc4	se
vleklo	vleknout	k5eAaImAgNnS	vleknout
osm	osm	k4xCc1	osm
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
v	v	k7c6	v
křižáckém	křižácký	k2eAgNnSc6d1	křižácké
vojsku	vojsko	k1gNnSc6	vojsko
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
šířit	šířit	k5eAaImF	šířit
poraženecká	poraženecký	k2eAgFnSc1d1	poraženecká
nálada	nálada	k1gFnSc1	nálada
a	a	k8xC	a
klesat	klesat	k5eAaImF	klesat
morálka	morálka	k1gFnSc1	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgMnPc1d1	západní
rytíři	rytíř	k1gMnPc1	rytíř
marně	marně	k6eAd1	marně
očekávali	očekávat	k5eAaImAgMnP	očekávat
příchod	příchod	k1gInSc4	příchod
byzantských	byzantský	k2eAgInPc2d1	byzantský
oddílů	oddíl	k1gInPc2	oddíl
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
přísun	přísun	k1gInSc1	přísun
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Obleženým	obležený	k2eAgInSc7d1	obležený
přišly	přijít	k5eAaPmAgFnP	přijít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
posily	posila	k1gFnPc1	posila
od	od	k7c2	od
vládců	vládce	k1gMnPc2	vládce
z	z	k7c2	z
Damašku	Damašek	k1gInSc2	Damašek
a	a	k8xC	a
Aleppa	Aleppa	k1gFnSc1	Aleppa
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
osobně	osobně	k6eAd1	osobně
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
početného	početný	k2eAgNnSc2d1	početné
vojska	vojsko	k1gNnSc2	vojsko
obávaný	obávaný	k2eAgInSc4d1	obávaný
mosulský	mosulský	k2eAgInSc4d1	mosulský
atabeg	atabeg	k1gInSc4	atabeg
Körbugha	Körbugh	k1gMnSc2	Körbugh
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
komplikovaly	komplikovat	k5eAaBmAgInP	komplikovat
spory	spor	k1gInPc1	spor
křižáckých	křižácký	k2eAgMnPc2d1	křižácký
velitelů	velitel	k1gMnPc2	velitel
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
komu	kdo	k3yInSc3	kdo
Antiochie	Antiochie	k1gFnSc1	Antiochie
napříště	napříště	k6eAd1	napříště
připadne	připadnout	k5eAaPmIp3nS	připadnout
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
odmítala	odmítat	k5eAaImAgFnS	odmítat
nároky	nárok	k1gInPc4	nárok
Alexia	Alexium	k1gNnSc2	Alexium
I.	I.	kA	I.
na	na	k7c6	na
navrácení	navrácení	k1gNnSc6	navrácení
dobytého	dobytý	k2eAgNnSc2d1	dobyté
území	území	k1gNnSc2	území
Byzanci	Byzanc	k1gFnSc3	Byzanc
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ho	on	k3xPp3gMnSc4	on
vinila	vinit	k5eAaImAgFnS	vinit
kvůli	kvůli	k7c3	kvůli
neposkytnutí	neposkytnutí	k1gNnSc3	neposkytnutí
pomoci	pomoc	k1gFnSc2	pomoc
ze	z	k7c2	z
zrady	zrada	k1gFnSc2	zrada
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc4d3	veliký
ambice	ambice	k1gFnPc4	ambice
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
bohatém	bohatý	k2eAgNnSc6d1	bohaté
městě	město	k1gNnSc6	město
měl	mít	k5eAaImAgInS	mít
Bohemund	Bohemund	k1gInSc1	Bohemund
z	z	k7c2	z
Tarentu	Tarent	k1gInSc2	Tarent
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
protivníkem	protivník	k1gMnSc7	protivník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
především	především	k9	především
Raimond	Raimond	k1gMnSc1	Raimond
z	z	k7c2	z
Toulouse	Toulouse	k1gInSc2	Toulouse
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
hájil	hájit	k5eAaImAgMnS	hájit
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
císařova	císařův	k2eAgNnSc2d1	císařovo
práva	právo	k1gNnSc2	právo
s	s	k7c7	s
nadějí	naděje	k1gFnSc7	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaPmF	stát
zdejším	zdejší	k2eAgMnPc3d1	zdejší
byzantským	byzantský	k2eAgMnPc3d1	byzantský
místodržícím	místodržící	k1gMnPc3	místodržící
<g/>
.	.	kIx.	.
</s>
<s>
Bohemundova	Bohemundův	k2eAgFnSc1d1	Bohemundův
autorita	autorita	k1gFnSc1	autorita
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgMnS	vyznamenat
jako	jako	k9	jako
schopný	schopný	k2eAgMnSc1d1	schopný
stratég	stratég	k1gMnSc1	stratég
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
s	s	k7c7	s
Körbugovými	Körbugův	k2eAgMnPc7d1	Körbugův
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
vnutil	vnutit	k5eAaPmAgInS	vnutit
západní	západní	k2eAgInSc4d1	západní
způsob	způsob	k1gInSc4	způsob
boje	boj	k1gInSc2	boj
a	a	k8xC	a
tvrdě	tvrdě	k6eAd1	tvrdě
je	být	k5eAaImIp3nS	být
porazil	porazit	k5eAaPmAgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
obránci	obránce	k1gMnSc3	obránce
antiochijské	antiochijský	k2eAgFnSc2d1	Antiochijská
pevnosti	pevnost	k1gFnSc2	pevnost
vzdali	vzdát	k5eAaPmAgMnP	vzdát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antiochie	Antiochie	k1gFnSc1	Antiochie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kořistí	kořist	k1gFnSc7	kořist
křižáků	křižák	k1gInPc2	křižák
a	a	k8xC	a
Bohemund	Bohemund	k1gInSc1	Bohemund
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
jejím	její	k3xOp3gMnSc7	její
suverénním	suverénní	k2eAgMnSc7d1	suverénní
vládcem	vládce	k1gMnSc7	vládce
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
kníže	kníže	k1gMnSc1	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Raimond	Raimond	k1gInSc1	Raimond
z	z	k7c2	z
Toulouse	Toulouse	k1gInSc2	Toulouse
poté	poté	k6eAd1	poté
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
požadavku	požadavek	k1gInSc2	požadavek
řadových	řadový	k2eAgMnPc2d1	řadový
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
poutníků	poutník	k1gMnPc2	poutník
a	a	k8xC	a
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
dobytí	dobytí	k1gNnSc1	dobytí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
křižáckých	křižácký	k2eAgInPc2d1	křižácký
států	stát	k1gInPc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
jasné	jasný	k2eAgFnSc3d1	jasná
dohodě	dohoda	k1gFnSc3	dohoda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
bude	být	k5eAaImBp3nS	být
velet	velet	k5eAaImF	velet
dalšímu	další	k2eAgNnSc3d1	další
tažení	tažení	k1gNnSc3	tažení
<g/>
,	,	kIx,	,
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
osobností	osobnost	k1gFnSc7	osobnost
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
příštích	příští	k2eAgInPc2d1	příští
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hrabě	hrabě	k1gMnSc1	hrabě
Raimond	Raimond	k1gMnSc1	Raimond
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
také	také	k9	také
Robert	Robert	k1gMnSc1	Robert
z	z	k7c2	z
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Tankred	Tankred	k1gInSc1	Tankred
s	s	k7c7	s
italskými	italský	k2eAgInPc7d1	italský
normany	norman	k1gInPc7	norman
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
následovali	následovat	k5eAaImAgMnP	následovat
Godefroy	Godefroa	k1gFnSc2	Godefroa
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Flanderský	flanderský	k2eAgMnSc1d1	flanderský
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
města	město	k1gNnSc2	město
Kafarját	Kafarját	k1gInSc4	Kafarját
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
jednání	jednání	k1gNnSc1	jednání
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
drobných	drobný	k2eAgMnPc2d1	drobný
arabských	arabský	k2eAgMnPc2d1	arabský
emírů	emír	k1gMnPc2	emír
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
významní	významný	k2eAgMnPc1d1	významný
byli	být	k5eAaImAgMnP	být
zejména	zejména	k9	zejména
emírové	emír	k1gMnPc1	emír
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Šajzar	Šajzar	k1gInSc1	Šajzar
a	a	k8xC	a
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
postupovat	postupovat	k5eAaImF	postupovat
dále	daleko	k6eAd2	daleko
vnitrozemím	vnitrozemí	k1gNnSc7	vnitrozemí
nebo	nebo	k8xC	nebo
přejít	přejít	k5eAaPmF	přejít
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
se	se	k3xPyFc4	se
názory	názor	k1gInPc7	názor
velitelů	velitel	k1gMnPc2	velitel
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Tankred	Tankred	k1gInSc1	Tankred
byl	být	k5eAaImAgInS	být
proti	proti	k7c3	proti
tažení	tažení	k1gNnSc3	tažení
podél	podél	k7c2	podél
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
namítal	namítat	k5eAaImAgMnS	namítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
leží	ležet	k5eAaImIp3nS	ležet
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
dobývat	dobývat	k5eAaImF	dobývat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
útok	útok	k1gInSc1	útok
na	na	k7c4	na
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
značně	značně	k6eAd1	značně
oddálil	oddálit	k5eAaPmAgInS	oddálit
<g/>
.	.	kIx.	.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
Raimond	Raimond	k1gMnSc1	Raimond
odporoval	odporovat	k5eAaImAgMnS	odporovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pevnosti	pevnost	k1gFnSc2	pevnost
většinou	většinou	k6eAd1	většinou
nebude	být	k5eNaImBp3nS	být
třeba	třeba	k6eAd1	třeba
dobývat	dobývat	k5eAaImF	dobývat
<g/>
,	,	kIx,	,
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
navázat	navázat	k5eAaPmF	navázat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
arabskými	arabský	k2eAgMnPc7d1	arabský
emíry	emír	k1gMnPc7	emír
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
byla	být	k5eAaImAgFnS	být
krajina	krajina	k1gFnSc1	krajina
bohatší	bohatý	k2eAgFnSc1d2	bohatší
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
naděje	naděje	k1gFnSc1	naděje
na	na	k7c4	na
lepší	dobrý	k2eAgNnSc4d2	lepší
zásobování	zásobování	k1gNnSc4	zásobování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křižácká	křižácký	k2eAgNnPc1d1	křižácké
vojska	vojsko	k1gNnPc1	vojsko
táhla	táhnout	k5eAaImAgNnP	táhnout
tedy	tedy	k9	tedy
po	po	k7c6	po
syrském	syrský	k2eAgNnSc6d1	syrské
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
dobře	dobře	k6eAd1	dobře
zásobované	zásobovaný	k2eAgFnPc4d1	zásobovaná
pevnosti	pevnost	k1gFnPc4	pevnost
Hisn	Hisno	k1gNnPc2	Hisno
al-Akrád	al-Akráda	k1gFnPc2	al-Akráda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
později	pozdě	k6eAd2	pozdě
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
Crac	Crac	k1gInSc4	Crac
de	de	k?	de
Chevaliers	Chevaliers	k1gInSc1	Chevaliers
<g/>
.	.	kIx.	.
</s>
<s>
Syrské	syrský	k2eAgNnSc1d1	syrské
pobřeží	pobřeží	k1gNnSc1	pobřeží
bylo	být	k5eAaImAgNnS	být
opravdu	opravdu	k6eAd1	opravdu
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
arabských	arabský	k2eAgMnPc2d1	arabský
emírů	emír	k1gMnPc2	emír
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ale	ale	k9	ale
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
nejednotní	jednotný	k2eNgMnPc1d1	nejednotný
<g/>
.	.	kIx.	.
</s>
<s>
Arabové	Arab	k1gMnPc1	Arab
nechtěli	chtít	k5eNaImAgMnP	chtít
měřit	měřit	k5eAaImF	měřit
síly	síla	k1gFnPc4	síla
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
nepřítelem	nepřítel	k1gMnSc7	nepřítel
a	a	k8xC	a
vykupovali	vykupovat	k5eAaImAgMnP	vykupovat
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc4	jeho
neutralitu	neutralita	k1gFnSc4	neutralita
bohatými	bohatý	k2eAgInPc7d1	bohatý
dary	dar	k1gInPc7	dar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
emírem	emír	k1gMnSc7	emír
z	z	k7c2	z
Hamá	hamat	k5eAaImIp3nS	hamat
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
také	také	k9	také
s	s	k7c7	s
emírem	emír	k1gMnSc7	emír
v	v	k7c6	v
Tripolisu	Tripolis	k1gInSc6	Tripolis
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
viděli	vidět	k5eAaImAgMnP	vidět
bohatost	bohatost	k1gFnSc4	bohatost
zdejších	zdejší	k2eAgFnPc2d1	zdejší
končin	končina	k1gFnPc2	končina
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vojsko	vojsko	k1gNnSc1	vojsko
zároveň	zároveň	k6eAd1	zároveň
oblehne	oblehnout	k5eAaPmIp3nS	oblehnout
město	město	k1gNnSc1	město
Arku	arkus	k1gInSc2	arkus
a	a	k8xC	a
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
na	na	k7c4	na
přístav	přístav	k1gInSc4	přístav
Tortosu	Tortosa	k1gFnSc4	Tortosa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
přístavem	přístav	k1gInSc7	přístav
mezi	mezi	k7c7	mezi
Latakií	Latakie	k1gFnSc7	Latakie
a	a	k8xC	a
Palestinou	Palestina	k1gFnSc7	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Tortosu	Tortosa	k1gFnSc4	Tortosa
dobyli	dobýt	k5eAaPmAgMnP	dobýt
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
slabou	slabý	k2eAgFnSc4d1	slabá
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
před	před	k7c7	před
Provensálci	Provensálec	k1gMnPc7	Provensálec
utekla	utéct	k5eAaPmAgFnS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Obsazením	obsazení	k1gNnSc7	obsazení
Tortosy	Tortosa	k1gFnSc2	Tortosa
získali	získat	k5eAaPmAgMnP	získat
křižáci	křižák	k1gMnPc1	křižák
přístav	přístav	k1gInSc1	přístav
klíčového	klíčový	k2eAgInSc2d1	klíčový
významu	význam	k1gInSc2	význam
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Kyprem	Kypr	k1gInSc7	Kypr
<g/>
,	,	kIx,	,
Antiochií	Antiochie	k1gFnSc7	Antiochie
i	i	k8xC	i
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
tažení	tažení	k1gNnSc1	tažení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
již	již	k6eAd1	již
v	v	k7c6	v
rychlém	rychlý	k2eAgNnSc6d1	rychlé
tempu	tempo	k1gNnSc6	tempo
a	a	k8xC	a
bez	bez	k7c2	bez
vážnějších	vážní	k2eAgFnPc2d2	vážnější
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Bejrútem	Bejrút	k1gInSc7	Bejrút
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
křižáci	křižák	k1gMnPc1	křižák
nezničí	zničit	k5eNaPmIp3nP	zničit
okolí	okolí	k1gNnSc4	okolí
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
dostanou	dostat	k5eAaPmIp3nP	dostat
od	od	k7c2	od
emíra	emír	k1gMnSc2	emír
výkupné	výkupný	k2eAgNnSc4d1	výkupné
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
vykoupila	vykoupit	k5eAaPmAgFnS	vykoupit
i	i	k9	i
Akkra	Akkra	k1gFnSc1	Akkra
či	či	k8xC	či
Akkon	Akkon	k1gInSc1	Akkon
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Cesareou	Cesarea	k1gFnSc7	Cesarea
oslavili	oslavit	k5eAaPmAgMnP	oslavit
křižáci	křižák	k1gMnPc1	křižák
svatodušní	svatodušní	k2eAgInSc4d1	svatodušní
svátky	svátek	k1gInPc4	svátek
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
obrátili	obrátit	k5eAaPmAgMnP	obrátit
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
ke	k	k7c3	k
městě	město	k1gNnSc6	město
Ramla	Ramlo	k1gNnSc2	Ramlo
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
a	a	k8xC	a
křižáci	křižák	k1gMnPc1	křižák
vtáhli	vtáhnout	k5eAaPmAgMnP	vtáhnout
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založili	založit	k5eAaPmAgMnP	založit
další	další	k2eAgNnSc4d1	další
latinské	latinský	k2eAgNnSc4d1	latinské
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vůdcové	vůdce	k1gMnPc1	vůdce
výpravy	výprava	k1gFnSc2	výprava
sešli	sejít	k5eAaPmAgMnP	sejít
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
poradě	porada	k1gFnSc3	porada
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
křižáci	křižák	k1gMnPc1	křižák
konečně	konečně	k6eAd1	konečně
stanuli	stanout	k5eAaPmAgMnP	stanout
před	před	k7c7	před
hradbami	hradba	k1gFnPc7	hradba
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horkém	horký	k2eAgNnSc6d1	horké
červnovém	červnový	k2eAgNnSc6d1	červnové
počasí	počasí	k1gNnSc6	počasí
a	a	k8xC	a
při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
vody	voda	k1gFnSc2	voda
zamítli	zamítnout	k5eAaPmAgMnP	zamítnout
velitelé	velitel	k1gMnPc1	velitel
možnost	možnost	k1gFnSc4	možnost
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
obléhání	obléhání	k1gNnSc2	obléhání
města	město	k1gNnSc2	město
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
přímý	přímý	k2eAgInSc4d1	přímý
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1099	[number]	k4	1099
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
obléhání	obléhání	k1gNnSc6	obléhání
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
padl	padnout	k5eAaImAgInS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Boží	boží	k2eAgInSc1d1	boží
hrob	hrob	k1gInSc1	hrob
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Dobytý	dobytý	k2eAgInSc1d1	dobytý
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
byl	být	k5eAaImAgInS	být
křižákům	křižák	k1gMnPc3	křižák
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c4	v
plen	plen	k1gInSc4	plen
<g/>
,	,	kIx,	,
zajatci	zajatec	k1gMnPc1	zajatec
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
stali	stát	k5eAaPmAgMnP	stát
obětí	oběť	k1gFnSc7	oběť
masakru	masakr	k1gInSc2	masakr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
zanechali	zanechat	k5eAaPmAgMnP	zanechat
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
i	i	k8xC	i
arabští	arabský	k2eAgMnPc1d1	arabský
kronikáři	kronikář	k1gMnPc1	kronikář
<g/>
,	,	kIx,	,
kradli	krást	k5eAaImAgMnP	krást
<g/>
,	,	kIx,	,
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
a	a	k8xC	a
dopouštěli	dopouštět	k5eAaImAgMnP	dopouštět
se	se	k3xPyFc4	se
strašlivých	strašlivý	k2eAgNnPc2d1	strašlivé
zvěrstev	zvěrstvo	k1gNnPc2	zvěrstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
křižáků	křižák	k1gMnPc2	křižák
dovršení	dovršení	k1gNnSc2	dovršení
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
kořistí	kořist	k1gFnSc7	kořist
se	se	k3xPyFc4	se
vraceli	vracet	k5eAaImAgMnP	vracet
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
vracejícími	vracející	k2eAgMnPc7d1	vracející
se	se	k3xPyFc4	se
bojovníky	bojovník	k1gMnPc4	bojovník
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
normandský	normandský	k2eAgMnSc1d1	normandský
vévoda	vévoda	k1gMnSc1	vévoda
Robert	Robert	k1gMnSc1	Robert
III	III	kA	III
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
Robert	Robert	k1gMnSc1	Robert
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Flanderský	flanderský	k2eAgInSc1d1	flanderský
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
někteří	některý	k3yIgMnPc1	některý
zůstali	zůstat	k5eAaPmAgMnP	zůstat
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
na	na	k7c6	na
dobytém	dobytý	k2eAgNnSc6d1	dobyté
území	území	k1gNnSc6	území
čtyři	čtyři	k4xCgInPc1	čtyři
křižácké	křižácký	k2eAgInPc1d1	křižácký
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
formovaly	formovat	k5eAaImAgInP	formovat
a	a	k8xC	a
upevňovaly	upevňovat	k5eAaImAgInP	upevňovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
muslimským	muslimský	k2eAgMnSc7d1	muslimský
nepřítelem	nepřítel	k1gMnSc7	nepřítel
i	i	k8xC	i
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Byzantské	byzantský	k2eAgFnSc3d1	byzantská
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
arménským	arménský	k2eAgMnPc3d1	arménský
knížatům	kníže	k1gMnPc3wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Antiochijského	antiochijský	k2eAgNnSc2d1	Antiochijské
knížectví	knížectví	k1gNnSc2	knížectví
a	a	k8xC	a
Edesského	Edesský	k2eAgNnSc2d1	Edesský
hrabství	hrabství	k1gNnSc2	hrabství
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1	Jeruzalémské
království	království	k1gNnSc1	království
a	a	k8xC	a
hrabství	hrabství	k1gNnSc1	hrabství
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
klíčové	klíčový	k2eAgNnSc4d1	klíčové
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
usiloval	usilovat	k5eAaImAgMnS	usilovat
opět	opět	k6eAd1	opět
Raimond	Raimond	k1gMnSc1	Raimond
z	z	k7c2	z
Toulouse	Toulouse	k1gInSc2	Toulouse
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
vedle	vedle	k7c2	vedle
Godefroye	Godefroy	k1gInSc2	Godefroy
z	z	k7c2	z
Bouillonu	Bouillon	k1gInSc2	Bouillon
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nakonec	nakonec	k6eAd1	nakonec
přijal	přijmout	k5eAaPmAgInS	přijmout
funkci	funkce	k1gFnSc4	funkce
panovníka	panovník	k1gMnSc2	panovník
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
útvaru	útvar	k1gInSc2	útvar
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
Ochránce	ochránce	k1gMnSc1	ochránce
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
Godefroyově	Godefroyův	k2eAgFnSc6d1	Godefroyův
brzké	brzký	k2eAgFnSc6d1	brzká
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
1100	[number]	k4	1100
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Balduin	Balduin	k1gMnSc1	Balduin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dosud	dosud	k6eAd1	dosud
vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
Edesse	Edess	k1gInSc6	Edess
<g/>
,	,	kIx,	,
korunován	korunován	k2eAgInSc1d1	korunován
o	o	k7c6	o
vánocích	vánoce	k1gFnPc6	vánoce
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
jeruzalémským	jeruzalémský	k2eAgMnSc7d1	jeruzalémský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
<g/>
Raimond	Raimond	k1gMnSc1	Raimond
vyšel	vyjít	k5eAaPmAgMnS	vyjít
opět	opět	k6eAd1	opět
s	s	k7c7	s
prázdnou	prázdná	k1gFnSc7	prázdná
<g/>
.	.	kIx.	.
</s>
<s>
Odtáhl	odtáhnout	k5eAaPmAgMnS	odtáhnout
proto	proto	k8xC	proto
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
byzantské	byzantský	k2eAgFnSc6d1	byzantská
Latakii	Latakie	k1gFnSc6	Latakie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zachránil	zachránit	k5eAaPmAgMnS	zachránit
před	před	k7c7	před
útokem	útok	k1gInSc7	útok
Bohemondových	Bohemondový	k2eAgMnPc2d1	Bohemondový
Normanů	Norman	k1gMnPc2	Norman
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
zde	zde	k6eAd1	zde
budovat	budovat	k5eAaImF	budovat
základy	základ	k1gInPc4	základ
posledního	poslední	k2eAgInSc2d1	poslední
křižáckého	křižácký	k2eAgInSc2d1	křižácký
státečku	státeček	k1gInSc2	státeček
jako	jako	k9	jako
leník	leník	k1gMnSc1	leník
byzantského	byzantský	k2eAgMnSc2d1	byzantský
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1103	[number]	k4	1103
dobyl	dobýt	k5eAaPmAgInS	dobýt
Tortosu	Tortosa	k1gFnSc4	Tortosa
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
obléhat	obléhat	k5eAaImF	obléhat
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
mnohaletého	mnohaletý	k2eAgNnSc2d1	mnohaleté
obléhání	obléhání	k1gNnSc2	obléhání
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
padlo	padnout	k5eAaImAgNnS	padnout
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1109	[number]	k4	1109
a	a	k8xC	a
představitelem	představitel	k1gMnSc7	představitel
hrabství	hrabství	k1gNnSc2	hrabství
tripolského	tripolský	k2eAgNnSc2d1	Tripolské
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
nový	nový	k2eAgInSc1d1	nový
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
nazývat	nazývat	k5eAaImF	nazývat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Raimondův	Raimondův	k2eAgMnSc1d1	Raimondův
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
Bernard	Bernard	k1gMnSc1	Bernard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc4	výsledek
první	první	k4xOgFnSc2	první
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
byla	být	k5eAaImAgFnS	být
nepochybně	pochybně	k6eNd1	pochybně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
dobytím	dobytí	k1gNnSc7	dobytí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
proklamovaného	proklamovaný	k2eAgInSc2d1	proklamovaný
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Ovládnutím	ovládnutí	k1gNnSc7	ovládnutí
patriarchátů	patriarchát	k1gInPc2	patriarchát
v	v	k7c6	v
Antiochii	Antiochie	k1gFnSc6	Antiochie
a	a	k8xC	a
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
latinská	latinský	k2eAgFnSc1d1	Latinská
církev	církev	k1gFnSc1	církev
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dříve	dříve	k6eAd2	dříve
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
1099	[number]	k4	1099
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
synoda	synoda	k1gFnSc1	synoda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jeruzalémským	jeruzalémský	k2eAgMnSc7d1	jeruzalémský
patriarchou	patriarcha	k1gMnSc7	patriarcha
papežský	papežský	k2eAgMnSc1d1	papežský
legát	legát	k1gMnSc1	legát
a	a	k8xC	a
pisánský	pisánský	k2eAgMnSc1d1	pisánský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Daimbert	Daimbert	k1gMnSc1	Daimbert
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
převzali	převzít	k5eAaPmAgMnP	převzít
Godefroy	Godefroa	k1gFnPc4	Godefroa
<g/>
,	,	kIx,	,
Bohemund	Bohemund	k1gInSc4	Bohemund
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgMnPc1d1	další
velmoži	velmož	k1gMnPc1	velmož
své	svůj	k3xOyFgFnSc2	svůj
državy	država	k1gFnSc2	država
jako	jako	k8xC	jako
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Byzantskou	byzantský	k2eAgFnSc4d1	byzantská
říši	říše	k1gFnSc4	říše
představovaly	představovat	k5eAaImAgInP	představovat
křižácké	křižácký	k2eAgInPc1d1	křižácký
státy	stát	k1gInPc1	stát
nárazníkové	nárazníkový	k2eAgInPc1d1	nárazníkový
území	území	k1gNnSc4	území
před	před	k7c7	před
hrozbou	hrozba	k1gFnSc7	hrozba
seldžuckých	seldžucký	k2eAgInPc2d1	seldžucký
Turků	turek	k1gInPc2	turek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
byzantská	byzantský	k2eAgFnSc1d1	byzantská
diplomacie	diplomacie	k1gFnSc1	diplomacie
snažila	snažit	k5eAaImAgFnS	snažit
držet	držet	k5eAaImF	držet
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
korektní	korektní	k2eAgInPc1d1	korektní
vztahy	vztah	k1gInPc1	vztah
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
vztah	vztah	k1gInSc4	vztah
samotných	samotný	k2eAgInPc2d1	samotný
Franků	Franky	k1gInPc2	Franky
nebyl	být	k5eNaImAgInS	být
k	k	k7c3	k
Byzanci	Byzanc	k1gFnSc3	Byzanc
vždy	vždy	k6eAd1	vždy
přátelský	přátelský	k2eAgInSc1d1	přátelský
a	a	k8xC	a
vstřícný	vstřícný	k2eAgMnSc1d1	vstřícný
<g/>
.	.	kIx.	.
</s>
<s>
Jasně	jasně	k6eAd1	jasně
protibyzantskou	protibyzantský	k2eAgFnSc4d1	protibyzantský
politiku	politika	k1gFnSc4	politika
vedl	vést	k5eAaImAgMnS	vést
především	především	k9	především
antiochijský	antiochijský	k2eAgMnSc1d1	antiochijský
kníže	kníže	k1gMnSc1	kníže
Bohemund	Bohemund	k1gMnSc1	Bohemund
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1104	[number]	k4	1104
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
majetkem	majetek	k1gInSc7	majetek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
papeže	papež	k1gMnSc2	papež
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
Byzanci	Byzanc	k1gFnSc3	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Normanská	normanský	k2eAgNnPc1d1	normanské
vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
vylodila	vylodit	k5eAaPmAgNnP	vylodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1107	[number]	k4	1107
pod	pod	k7c7	pod
dračskými	dračský	k2eAgFnPc7d1	dračský
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
však	však	k9	však
byla	být	k5eAaImAgFnS	být
obklíčena	obklíčit	k5eAaPmNgFnS	obklíčit
Byzantinci	Byzantinec	k1gMnPc7	Byzantinec
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
Bohemund	Bohemund	k1gMnSc1	Bohemund
marně	marně	k6eAd1	marně
snažil	snažit	k5eAaImAgMnS	snažit
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
obležení	obležení	k1gNnSc2	obležení
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
kapituloval	kapitulovat	k5eAaBmAgInS	kapitulovat
a	a	k8xC	a
uznal	uznat	k5eAaPmAgInS	uznat
lenní	lenní	k2eAgFnSc4d1	lenní
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
nad	nad	k7c7	nad
Antiochií	Antiochie	k1gFnSc7	Antiochie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
současně	současně	k6eAd1	současně
dosazen	dosazen	k2eAgMnSc1d1	dosazen
ortodoxní	ortodoxní	k2eAgMnSc1d1	ortodoxní
patriarcha	patriarcha	k1gMnSc1	patriarcha
<g/>
.	.	kIx.	.
</s>
<s>
Bohemund	Bohemund	k1gMnSc1	Bohemund
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
na	na	k7c4	na
Východ	východ	k1gInSc4	východ
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Správy	správa	k1gFnPc1	správa
antiochijského	antiochijský	k2eAgNnSc2d1	Antiochijské
knížectví	knížectví	k1gNnSc2	knížectví
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Tankred	Tankred	k1gMnSc1	Tankred
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
Byzanci	Byzanc	k1gFnSc6	Byzanc
nikdy	nikdy	k6eAd1	nikdy
neuznal	uznat	k5eNaPmAgMnS	uznat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
složil	složit	k5eAaPmAgMnS	složit
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
velmi	velmi	k6eAd1	velmi
neochotně	ochotně	k6eNd1	ochotně
<g/>
,	,	kIx,	,
lenní	lenní	k2eAgFnSc4d1	lenní
přísahu	přísaha	k1gFnSc4	přísaha
jeruzalémskému	jeruzalémský	k2eAgMnSc3d1	jeruzalémský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
muslimské	muslimský	k2eAgInPc4d1	muslimský
státní	státní	k2eAgInPc4d1	státní
útvary	útvar	k1gInPc4	útvar
nepředstavovaly	představovat	k5eNaImAgInP	představovat
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
křižácké	křižácký	k2eAgFnSc2d1	křižácká
državy	država	k1gFnSc2	država
na	na	k7c6	na
Východě	východ	k1gInSc6	východ
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgNnSc4	žádný
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Abbásovský	Abbásovský	k2eAgMnSc1d1	Abbásovský
chalífa	chalífa	k1gMnSc1	chalífa
al-Mustazhir	al-Mustazhir	k1gMnSc1	al-Mustazhir
i	i	k8xC	i
Velký	velký	k2eAgMnSc1d1	velký
Seldžuk	Seldžuk	k1gMnSc1	Seldžuk
Barkijaruk	Barkijaruk	k1gMnSc1	Barkijaruk
považovali	považovat	k5eAaImAgMnP	považovat
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
za	za	k7c4	za
pouhý	pouhý	k2eAgInSc4d1	pouhý
nájezd	nájezd	k1gInSc4	nájezd
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
nejednotnost	nejednotnost	k1gFnSc1	nejednotnost
islámského	islámský	k2eAgInSc2d1	islámský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stále	stále	k6eAd1	stále
rozdělovalo	rozdělovat	k5eAaImAgNnS	rozdělovat
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
šíitskými	šíitský	k2eAgInPc7d1	šíitský
Fátimovci	Fátimovec	k1gInPc7	Fátimovec
a	a	k8xC	a
sunnitskými	sunnitský	k2eAgInPc7d1	sunnitský
Seldžuky	Seldžuk	k1gInPc7	Seldžuk
<g/>
,	,	kIx,	,
rozpory	rozpor	k1gInPc1	rozpor
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
mocenskými	mocenský	k2eAgMnPc7d1	mocenský
centry	centr	k1gMnPc7	centr
Aleppem	Alepp	k1gMnSc7	Alepp
<g/>
,	,	kIx,	,
Damaškem	Damašek	k1gInSc7	Damašek
a	a	k8xC	a
Mosulem	Mosul	k1gInSc7	Mosul
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
konflikty	konflikt	k1gInPc4	konflikt
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
a	a	k8xC	a
úpadek	úpadek	k1gInSc4	úpadek
fátimovského	fátimovský	k2eAgInSc2d1	fátimovský
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
umožnila	umožnit	k5eAaPmAgFnS	umožnit
drobným	drobný	k2eAgMnPc3d1	drobný
křesťanským	křesťanský	k2eAgMnPc3d1	křesťanský
státům	stát	k1gInPc3	stát
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
etablovaly	etablovat	k5eAaBmAgFnP	etablovat
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
také	také	k9	také
konsolidovaly	konsolidovat	k5eAaBmAgFnP	konsolidovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
příznivá	příznivý	k2eAgFnSc1d1	příznivá
i	i	k9	i
pro	pro	k7c4	pro
Byzanc	Byzanc	k1gFnSc4	Byzanc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Alexia	Alexium	k1gNnSc2	Alexium
I.	I.	kA	I.
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
upevnění	upevnění	k1gNnSc3	upevnění
císařské	císařský	k2eAgFnSc2d1	císařská
autority	autorita	k1gFnSc2	autorita
a	a	k8xC	a
centrální	centrální	k2eAgFnSc3d1	centrální
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
obnovení	obnovení	k1gNnSc3	obnovení
vojenského	vojenský	k2eAgInSc2d1	vojenský
potenciálu	potenciál	k1gInSc2	potenciál
státu	stát	k1gInSc2	stát
a	a	k8xC	a
překvapivému	překvapivý	k2eAgNnSc3d1	překvapivé
posílení	posílení	k1gNnSc3	posílení
zahraničně	zahraničně	k6eAd1	zahraničně
politických	politický	k2eAgFnPc2d1	politická
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Alexios	Alexios	k1gMnSc1	Alexios
umíral	umírat	k5eAaImAgMnS	umírat
<g/>
,	,	kIx,	,
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
se	se	k3xPyFc4	se
byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
opět	opět	k6eAd1	opět
od	od	k7c2	od
jadranského	jadranský	k2eAgNnSc2d1	Jadranské
pobřeží	pobřeží	k1gNnSc2	pobřeží
až	až	k9	až
do	do	k7c2	do
východního	východní	k2eAgNnSc2d1	východní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BALDWIN	BALDWIN	kA	BALDWIN
<g/>
,	,	kIx,	,
Marshall	Marshall	k1gInSc1	Marshall
W.	W.	kA	W.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Crusades	Crusades	k1gInSc1	Crusades
<g/>
.	.	kIx.	.
</s>
<s>
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
first	first	k1gMnSc1	first
hundred	hundred	k1gMnSc1	hundred
years	years	k6eAd1	years
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gInSc1	Madison
<g/>
:	:	kIx,	:
University	universita	k1gFnSc2	universita
of	of	k?	of
Wisconsin	Wisconsin	k2eAgInSc1d1	Wisconsin
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
707	[number]	k4	707
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BRIDGE	BRIDGE	kA	BRIDGE
<g/>
,	,	kIx,	,
Antony	anton	k1gInPc7	anton
<g/>
.	.	kIx.	.
</s>
<s>
Křížové	Křížové	k2eAgFnPc1d1	Křížové
výpravy	výprava	k1gFnPc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
228	[number]	k4	228
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
512	[number]	k4	512
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DUGGAN	DUGGAN	kA	DUGGAN
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
<g/>
.	.	kIx.	.
</s>
<s>
Křižácké	křižácký	k2eAgFnPc1d1	křižácká
výpravy	výprava	k1gFnPc1	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
214	[number]	k4	214
s.	s.	k?	s.
</s>
</p>
<p>
<s>
GABRIELI	Gabriel	k1gMnSc5	Gabriel
<g/>
,	,	kIx,	,
Francesco	Francesco	k6eAd1	Francesco
<g/>
.	.	kIx.	.
</s>
<s>
Křížové	Křížové	k2eAgFnPc4d1	Křížové
výpravy	výprava	k1gFnPc4	výprava
očima	oko	k1gNnPc7	oko
arabských	arabský	k2eAgInPc2d1	arabský
kronikářů	kronikář	k1gMnPc2	kronikář
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
344	[number]	k4	344
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
333	[number]	k4	333
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1	Hrochová
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g/>
;	;	kIx,	;
HROCH	Hroch	k1gMnSc1	Hroch
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Křižáci	křižák	k1gMnPc1	křižák
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
289	[number]	k4	289
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
621	[number]	k4	621
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HROCHOVÁ	Hrochová	k1gFnSc1	Hrochová
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g/>
.	.	kIx.	.
</s>
<s>
Křížové	Křížové	k2eAgFnPc1d1	Křížové
výpravy	výprava	k1gFnPc1	výprava
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
soudobých	soudobý	k2eAgFnPc2d1	soudobá
kronik	kronika	k1gFnPc2	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
255	[number]	k4	255
s.	s.	k?	s.
</s>
</p>
<p>
<s>
CHAZAN	CHAZAN	kA	CHAZAN
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
the	the	k?	the
year	year	k1gInSc1	year
1096	[number]	k4	1096
:	:	kIx,	:
the	the	k?	the
First	First	k1gInSc1	First
Crusade	Crusad	k1gInSc5	Crusad
and	and	k?	and
the	the	k?	the
Jews	Jews	k1gInSc1	Jews
<g/>
.	.	kIx.	.
</s>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
:	:	kIx,	:
Jewish	Jewish	k1gInSc1	Jewish
Publication	Publication	k1gInSc1	Publication
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
186	[number]	k4	186
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8276	[number]	k4	8276
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
575	[number]	k4	575
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
IBN	IBN	kA	IBN
MUNKIZ	MUNKIZ	kA	MUNKIZ
<g/>
,	,	kIx,	,
Usáma	Usámum	k1gNnSc2	Usámum
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
zkušeností	zkušenost	k1gFnPc2	zkušenost
arabského	arabský	k2eAgMnSc2d1	arabský
bojovníka	bojovník	k1gMnSc2	bojovník
s	s	k7c7	s
křižáky	křižák	k1gMnPc7	křižák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
348	[number]	k4	348
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1814	[number]	k4	1814
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOMNÉNA	KOMNÉNA	kA	KOMNÉNA
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Paměti	paměť	k1gFnPc1	paměť
byzantské	byzantský	k2eAgFnSc2d1	byzantská
princezny	princezna	k1gFnSc2	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
565	[number]	k4	565
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
527	[number]	k4	527
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Meč	meč	k1gInSc1	meč
a	a	k8xC	a
kříž	kříž	k1gInSc1	kříž
:	:	kIx,	:
(	(	kIx(	(
<g/>
1066	[number]	k4	1066
<g/>
-	-	kIx~	-
<g/>
1214	[number]	k4	1214
<g/>
)	)	kIx)	)
:	:	kIx,	:
rytířské	rytířský	k2eAgFnPc1d1	rytířská
bitvy	bitva	k1gFnPc1	bitva
a	a	k8xC	a
osudy	osud	k1gInPc1	osud
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
278	[number]	k4	278
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1289	[number]	k4	1289
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NICOLLE	NICOLLE	kA	NICOLLE
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
1096-99	[number]	k4	1096-99
:	:	kIx,	:
dobytí	dobytí	k1gNnSc4	dobytí
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PETERS	PETERS	kA	PETERS
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
First	First	k1gInSc1	First
Crusade	Crusad	k1gInSc5	Crusad
<g/>
:	:	kIx,	:
the	the	k?	the
chronicle	chronicle	k1gInSc1	chronicle
of	of	k?	of
Fulcher	Fulchra	k1gFnPc2	Fulchra
of	of	k?	of
Chartres	Chartres	k1gMnSc1	Chartres
and	and	k?	and
other	other	k1gMnSc1	other
source	source	k1gMnSc1	source
materials	materials	k6eAd1	materials
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
317	[number]	k4	317
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8122	[number]	k4	8122
<g/>
-	-	kIx~	-
<g/>
1656	[number]	k4	1656
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RILEY-SMITH	RILEY-SMITH	k?	RILEY-SMITH
<g/>
,	,	kIx,	,
Jonathan	Jonathan	k1gMnSc1	Jonathan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
First	First	k1gInSc1	First
Crusade	Crusad	k1gInSc5	Crusad
and	and	k?	and
the	the	k?	the
Idea	idea	k1gFnSc1	idea
of	of	k?	of
crusading	crusading	k1gInSc1	crusading
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Continuum	Continuum	k1gInSc1	Continuum
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
227	[number]	k4	227
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
-	-	kIx~	-
<g/>
648431	[number]	k4	648431
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RILEY-SMITH	RILEY-SMITH	k?	RILEY-SMITH
<g/>
,	,	kIx,	,
Jonathan	Jonathan	k1gMnSc1	Jonathan
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Oxford	Oxford	k1gInSc1	Oxford
illustrated	illustrated	k1gInSc1	illustrated
history	histor	k1gInPc1	histor
of	of	k?	of
the	the	k?	the
Crusades	Crusades	k1gInSc1	Crusades
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
436	[number]	k4	436
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
820435	[number]	k4	820435
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RUNCIMAN	RUNCIMAN	kA	RUNCIMAN
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgInSc1d1	Steven
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
history	histor	k1gInPc1	histor
of	of	k?	of
the	the	k?	the
Crusades	Crusades	k1gInSc1	Crusades
<g/>
.	.	kIx.	.
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
first	first	k1gMnSc1	first
Crusades	Crusades	k1gMnSc1	Crusades
and	and	k?	and
the	the	k?	the
foundation	foundation	k1gInSc1	foundation
of	of	k?	of
the	the	k?	the
Kingdom	Kingdom	k1gInSc1	Kingdom
of	of	k?	of
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
376	[number]	k4	376
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6161	[number]	k4	6161
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SMAIL	SMAIL	kA	SMAIL
<g/>
,	,	kIx,	,
Raymond	Raymond	k1gMnSc1	Raymond
Charles	Charles	k1gMnSc1	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Crusading	Crusading	k1gInSc1	Crusading
Warfare	Warfar	k1gMnSc5	Warfar
(	(	kIx(	(
<g/>
1097	[number]	k4	1097
<g/>
-	-	kIx~	-
<g/>
1193	[number]	k4	1193
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Barnes	Barnes	k1gInSc1	Barnes
&	&	k?	&
Noble	Noble	k1gInSc1	Noble
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
272	[number]	k4	272
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
56619	[number]	k4	56619
<g/>
-	-	kIx~	-
<g/>
769	[number]	k4	769
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TYERMAN	TYERMAN	kA	TYERMAN
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
<g/>
.	.	kIx.	.
</s>
<s>
Svaté	svatý	k2eAgFnPc1d1	svatá
války	válka	k1gFnPc1	válka
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
926	[number]	k4	926
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZÁSTĚROVÁ	Zástěrová	k1gFnSc1	Zástěrová
<g/>
,	,	kIx,	,
Bohumila	Bohumila	k1gFnSc1	Bohumila
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
529	[number]	k4	529
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
454	[number]	k4	454
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Křižácká	křižácký	k2eAgNnPc1d1	křižácké
tažení	tažení	k1gNnPc1	tažení
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
</s>
</p>
<p>
<s>
Křižácké	křižácký	k2eAgInPc1d1	křižácký
státy	stát	k1gInPc1	stát
</s>
</p>
<p>
<s>
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1	Jeruzalémské
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
První	první	k4xOgFnSc1	první
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Výzva	výzva	k1gFnSc1	výzva
papeže	papež	k1gMnSc2	papež
Urbana	Urban	k1gMnSc2	Urban
II	II	kA	II
<g/>
.	.	kIx.	.
ke	k	k7c3	k
křížové	křížový	k2eAgFnSc3d1	křížová
výpravě	výprava	k1gFnSc3	výprava
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Úryvky	úryvek	k1gInPc7	úryvek
z	z	k7c2	z
Alexiady	Alexiada	k1gFnSc2	Alexiada
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
První	první	k4xOgFnSc1	první
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Alan	alan	k1gInSc1	alan
V.	V.	kA	V.
Murray	Murray	k1gInPc1	Murray
<g/>
:	:	kIx,	:
Bibliography	Bibliographa	k1gFnPc1	Bibliographa
of	of	k?	of
the	the	k?	the
First	First	k1gInSc1	First
Crusade	Crusad	k1gInSc5	Crusad
(	(	kIx(	(
<g/>
1095	[number]	k4	1095
<g/>
–	–	k?	–
<g/>
1099	[number]	k4	1099
<g/>
)	)	kIx)	)
</s>
</p>
