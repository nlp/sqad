<s>
Představa	představa	k1gFnSc1
</s>
<s>
Představa	představa	k1gFnSc1
(	(	kIx(
<g/>
kalk	kalk	k1gInSc1
z	z	k7c2
něm.	něm.	k?
Vor-stellung	Vor-stellung	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
široké	široký	k2eAgNnSc1d1
označení	označení	k1gNnSc1
pro	pro	k7c4
obsahy	obsah	k1gInPc4
či	či	k8xC
obrazy	obraz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
si	se	k3xPyFc3
vědomí	vědomí	k1gNnSc1
„	„	k?
<g/>
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
před	před	k7c4
sebe	sebe	k3xPyFc4
<g/>
“	“	k?
jako	jako	k8xC,k8xS
témata	téma	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
jsou	být	k5eAaImIp3nP
představy	představa	k1gFnPc4
vždy	vždy	k6eAd1
více	hodně	k6eAd2
nebo	nebo	k8xC
méně	málo	k6eAd2
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
zkušenostech	zkušenost	k1gFnPc6
(	(	kIx(
<g/>
vjemech	vjem	k1gInPc6
<g/>
,	,	kIx,
zážitcích	zážitek	k1gInPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
charakteristický	charakteristický	k2eAgInSc1d1
aktivní	aktivní	k2eAgInSc1d1
podíl	podíl	k1gInSc1
vlastního	vlastní	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
nebo	nebo	k8xC
dokonce	dokonce	k9
tvořivosti	tvořivost	k1gFnSc2
a	a	k8xC
také	také	k9
určitá	určitý	k2eAgFnSc1d1
celkovost	celkovost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
představa	představa	k1gFnSc1
určitější	určitý	k2eAgFnSc1d2
než	než	k8xS
pocit	pocit	k1gInSc4
nebo	nebo	k8xC
dojem	dojem	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
není	být	k5eNaImIp3nS
ještě	ještě	k6eAd1
vyjádřena	vyjádřit	k5eAaPmNgFnS
slovy	slovo	k1gNnPc7
a	a	k8xC
člověk	člověk	k1gMnSc1
takové	takový	k3xDgNnSc4
vyjádření	vyjádření	k1gNnSc4
musí	muset	k5eAaImIp3nS
teprve	teprve	k6eAd1
hledat	hledat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představy	představa	k1gFnPc1
jsou	být	k5eAaImIp3nP
hlavním	hlavní	k2eAgInSc7d1
nástrojem	nástroj	k1gInSc7
praktické	praktický	k2eAgFnSc2d1
orientace	orientace	k1gFnSc2
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
rozhodování	rozhodování	k1gNnSc6
a	a	k8xC
jednání	jednání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Představou	představa	k1gFnSc7
tedy	tedy	k9
rozumíme	rozumět	k5eAaImIp1nP
celkový	celkový	k2eAgInSc4d1
a	a	k8xC
málo	málo	k6eAd1
určitý	určitý	k2eAgInSc1d1
„	„	k?
<g/>
obraz	obraz	k1gInSc1
<g/>
“	“	k?
nějaké	nějaký	k3yIgFnSc3
věci	věc	k1gFnSc3
<g/>
,	,	kIx,
předmětu	předmět	k1gInSc3
nebo	nebo	k8xC
záměru	záměr	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
obvykle	obvykle	k6eAd1
nedovedeme	dovést	k5eNaPmIp1nP
popsat	popsat	k5eAaPmF
a	a	k8xC
vyjádřit	vyjádřit	k5eAaPmF
slovy	slovo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
představě	představa	k1gFnSc6
mluvíme	mluvit	k5eAaImIp1nP
obvykle	obvykle	k6eAd1
ve	v	k7c6
dvojí	dvojí	k4xRgFnSc6
různé	různý	k2eAgFnSc6d1
souvislosti	souvislost	k1gFnSc6
<g/>
:	:	kIx,
</s>
<s>
Reproduktivní	reproduktivní	k2eAgFnSc1d1
představa	představa	k1gFnSc1
je	být	k5eAaImIp3nS
jakýsi	jakýsi	k3yIgInSc4
zhuštěný	zhuštěný	k2eAgInSc4d1
souhrn	souhrn	k1gInSc4
<g/>
,	,	kIx,
syntéza	syntéza	k1gFnSc1
nejrůznějších	různý	k2eAgInPc2d3
vjemů	vjem	k1gInPc2
a	a	k8xC
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
„	„	k?
<g/>
poznáme	poznat	k5eAaPmIp1nP
<g/>
“	“	k?
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
jest	být	k5eAaImIp3nS
odlišíme	odlišit	k5eAaPmIp1nP
například	například	k6eAd1
psa	pes	k1gMnSc4
od	od	k7c2
jiných	jiný	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
vítr	vítr	k1gInSc1
od	od	k7c2
jiných	jiný	k2eAgInPc2d1
jevů	jev	k1gInPc2
nebo	nebo	k8xC
zdvořilost	zdvořilost	k1gFnSc1
od	od	k7c2
nezdvořilosti	nezdvořilost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgInPc4
představě	představa	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
si	se	k3xPyFc3
člověk	člověk	k1gMnSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
od	od	k7c2
dětství	dětství	k1gNnSc2
zkušenostmi	zkušenost	k1gFnPc7
<g/>
,	,	kIx,
potom	potom	k6eAd1
obvykle	obvykle	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
také	také	k9
pojem	pojem	k1gInSc1
a	a	k8xC
slovo	slovo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
se	se	k3xPyFc4
představa	představa	k1gFnSc1
příliš	příliš	k6eAd1
neliší	lišit	k5eNaImIp3nS
od	od	k7c2
Platónových	Platónův	k2eAgFnPc2d1
idejí	idea	k1gFnPc2
a	a	k8xC
Aristotelova	Aristotelův	k2eAgFnSc1d1
eidos	eidos	k1gInSc1
–	–	k?
vzhledu	vzhled	k1gInSc2
či	či	k8xC
„	„	k?
<g/>
vidu	vid	k1gInSc2
<g/>
“	“	k?
nějaké	nějaký	k3yIgFnPc4
věci	věc	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
může	moct	k5eAaImIp3nS
představa	představa	k1gFnSc1
znamenat	znamenat	k5eAaImF
předběžný	předběžný	k2eAgInSc4d1
rozvrh	rozvrh	k1gInSc4
něčeho	něco	k3yInSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
člověk	člověk	k1gMnSc1
má	mít	k5eAaImIp3nS
teprve	teprve	k6eAd1
před	před	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
k	k	k7c3
čemu	co	k3yQnSc3,k3yInSc3,k3yRnSc3
se	se	k3xPyFc4
chystá	chystat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkovou	celkový	k2eAgFnSc7d1
a	a	k8xC
neurčitou	určitý	k2eNgFnSc7d1
první	první	k4xOgFnSc7
představou	představa	k1gFnSc7
začíná	začínat	k5eAaImIp3nS
umělec	umělec	k1gMnSc1
i	i	k8xC
konstruktér	konstruktér	k1gMnSc1
<g/>
,	,	kIx,
než	než	k8xS
ji	on	k3xPp3gFnSc4
začne	začít	k5eAaPmIp3nS
rozvíjet	rozvíjet	k5eAaImF
do	do	k7c2
náčrtů	náčrt	k1gInPc2
<g/>
,	,	kIx,
skic	skica	k1gFnPc2
a	a	k8xC
popisů	popis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
naopak	naopak	k6eAd1
představa	představa	k1gFnSc1
předchází	předcházet	k5eAaImIp3nS
hotové	hotový	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
a	a	k8xC
i	i	k9
když	když	k8xS
samozřejmě	samozřejmě	k6eAd1
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
známých	známý	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
něco	něco	k3yInSc1
nového	nový	k2eAgMnSc2d1
<g/>
,	,	kIx,
původního	původní	k2eAgMnSc2d1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
zde	zde	k6eAd1
ještě	ještě	k6eAd1
nebylo	být	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
se	se	k3xPyFc4
na	na	k7c6
představě	představa	k1gFnSc6
podstatně	podstatně	k6eAd1
podílí	podílet	k5eAaImIp3nS
nápad	nápad	k1gInSc1
<g/>
,	,	kIx,
fantazie	fantazie	k1gFnSc1
<g/>
,	,	kIx,
tvořivost	tvořivost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Představivost	představivost	k1gFnSc1
</s>
<s>
Lidskou	lidský	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
vytvářet	vytvářet	k5eAaImF
představy	představa	k1gFnPc1
nazýváme	nazývat	k5eAaImIp1nP
představivost	představivost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
jako	jako	k8xS,k8xC
představa	představa	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
představivost	představivost	k1gFnSc1
převážně	převážně	k6eAd1
zraková	zrakový	k2eAgFnSc1d1
čili	čili	k8xC
vizuální	vizuální	k2eAgFnSc1d1
<g/>
,	,	kIx,
sluchová	sluchový	k2eAgFnSc1d1
čili	čili	k8xC
auditivní	auditivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
pohybová	pohybový	k2eAgFnSc1d1
čili	čili	k8xC
motorická	motorický	k2eAgFnSc1d1
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
představ	představa	k1gFnPc2
ale	ale	k8xC
není	být	k5eNaImIp3nS
takto	takto	k6eAd1
rozdělena	rozdělit	k5eAaPmNgFnS
a	a	k8xC
má	mít	k5eAaImIp3nS
řadu	řada	k1gFnSc4
různých	různý	k2eAgFnPc2d1
složek	složka	k1gFnPc2
<g/>
:	:	kIx,
k	k	k7c3
představě	představa	k1gFnSc3
psa	pes	k1gMnSc2
patří	patřit	k5eAaImIp3nP
i	i	k9
štěkot	štěkot	k1gInSc4
nebo	nebo	k8xC
hmatový	hmatový	k2eAgInSc4d1
dojem	dojem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
u	u	k7c2
představivosti	představivost	k1gFnSc2
můžeme	moct	k5eAaImIp1nP
rozlišit	rozlišit	k5eAaPmF
různé	různý	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
závislosti	závislost	k1gFnSc2
na	na	k7c4
zkušenosti	zkušenost	k1gFnPc4
nebo	nebo	k8xC
naopak	naopak	k6eAd1
na	na	k7c4
fantazii	fantazie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technik	technik	k1gMnSc1
i	i	k9
řemeslník	řemeslník	k1gMnSc1
potřebuje	potřebovat	k5eAaImIp3nS
představivost	představivost	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
z	z	k7c2
výkresů	výkres	k1gInPc2
vytvořil	vytvořit	k5eAaPmAgMnS
představu	představa	k1gFnSc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
má	mít	k5eAaImIp3nS
vyrobit	vyrobit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
hovoříme	hovořit	k5eAaImIp1nP
o	o	k7c4
prostorové	prostorový	k2eAgFnPc4d1
nebo	nebo	k8xC
geometrické	geometrický	k2eAgFnPc4d1
představivosti	představivost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
si	se	k3xPyFc3
má	mít	k5eAaImIp3nS
archeolog	archeolog	k1gMnSc1
na	na	k7c6
základě	základ	k1gInSc6
vykopávek	vykopávka	k1gFnPc2
představit	představit	k5eAaPmF
vzhled	vzhled	k1gInSc4
dávné	dávný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
či	či	k8xC
města	město	k1gNnSc2
<g/>
,	,	kIx,
potřebuje	potřebovat	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
patrně	patrně	k6eAd1
větší	veliký	k2eAgInPc1d2
podíl	podíl	k1gInSc1
fantazie	fantazie	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
představa	představa	k1gFnSc1
se	se	k3xPyFc4
tudíž	tudíž	k8xC
také	také	k9
může	moct	k5eAaImIp3nS
se	s	k7c7
skutečností	skutečnost	k1gFnSc7
míjet	míjet	k5eAaImF
<g/>
,	,	kIx,
být	být	k5eAaImF
iluzí	iluze	k1gFnSc7
a	a	k8xC
omylem	omyl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k9
dětem	dítě	k1gFnPc3
stačí	stačit	k5eAaBmIp3nS
slovní	slovní	k2eAgNnSc1d1
vyprávění	vyprávění	k1gNnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
samy	sám	k3xTgFnPc1
představily	představit	k5eAaPmAgFnP
příběh	příběh	k1gInSc4
<g/>
,	,	kIx,
krajinu	krajina	k1gFnSc4
<g/>
,	,	kIx,
postavy	postava	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
ovšem	ovšem	k9
pocházejí	pocházet	k5eAaImIp3nP
převážně	převážně	k6eAd1
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
vlastních	vlastní	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
dovedou	dovést	k5eAaPmIp3nP
spojovat	spojovat	k5eAaImF
či	či	k8xC
asociovat	asociovat	k5eAaBmF
na	na	k7c6
základě	základ	k1gInSc6
vyprávěné	vyprávěný	k2eAgFnSc2d1
pohádky	pohádka	k1gFnSc2
(	(	kIx(
<g/>
eidetismus	eidetismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
<g/>
-li	-li	k?
pak	pak	k6eAd1
tutéž	týž	k3xTgFnSc4
pohádku	pohádka	k1gFnSc4
nakreslit	nakreslit	k5eAaPmF
<g/>
,	,	kIx,
nakreslí	nakreslit	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
každé	každý	k3xTgNnSc4
dítě	dítě	k1gNnSc4
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
neboť	neboť	k8xC
kreslí	kreslit	k5eAaImIp3nS
svoji	svůj	k3xOyFgFnSc4
představu	představa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
George	Georg	k1gMnSc2
Lakoffa	Lakoff	k1gMnSc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
psychologů	psycholog	k1gMnPc2
si	se	k3xPyFc3
člověk	člověk	k1gMnSc1
nevytváří	vytvářet	k5eNaImIp3nS,k5eNaPmIp3nS
pojmy	pojem	k1gInPc4
na	na	k7c6
základě	základ	k1gInSc6
vymezení	vymezení	k1gNnSc2
a	a	k8xC
definic	definice	k1gFnPc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
na	na	k7c6
základě	základ	k1gInSc6
jakési	jakýsi	k3yIgFnSc2
základní	základní	k2eAgFnSc2d1
představy	představa	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
typického	typický	k2eAgMnSc4d1
psa	pes	k1gMnSc4
nebo	nebo	k8xC
typické	typický	k2eAgFnPc1d1
židle	židle	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
buduje	budovat	k5eAaImIp3nS
a	a	k8xC
rozšiřuje	rozšiřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
méně	málo	k6eAd2
typickými	typický	k2eAgMnPc7d1
psy	pes	k1gMnPc7
a	a	k8xC
židlemi	židle	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fantazie	fantazie	k1gFnSc1
</s>
<s>
Fantazií	fantazie	k1gFnPc2
(	(	kIx(
<g/>
z	z	k7c2
řec.	řec.	k?
fantasma	fantasma	k1gNnSc4
<g/>
,	,	kIx,
obraz	obraz	k1gInSc4
<g/>
,	,	kIx,
vidění	vidění	k1gNnSc4
<g/>
,	,	kIx,
přelud	přelud	k1gInSc4
<g/>
)	)	kIx)
rozumíme	rozumět	k5eAaImIp1nP
rozvinutou	rozvinutý	k2eAgFnSc4d1
představivost	představivost	k1gFnSc4
či	či	k8xC
obrazotvornost	obrazotvornost	k1gFnSc4
<g/>
,	,	kIx,
silně	silně	k6eAd1
uvolněnou	uvolněný	k2eAgFnSc4d1
od	od	k7c2
všech	všecek	k3xTgFnPc2
zkušeností	zkušenost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
psychologii	psychologie	k1gFnSc6
se	se	k3xPyFc4
zkouší	zkoušet	k5eAaImIp3nS
například	například	k6eAd1
tak	tak	k6eAd1
zvaným	zvaný	k2eAgInSc7d1
Rorschachovým	Rorschachův	k2eAgInSc7d1
testem	test	k1gInSc7
<g/>
,	,	kIx,
sérií	série	k1gFnSc7
obrázků	obrázek	k1gInPc2
nepravidelných	pravidelný	k2eNgFnPc2d1
inkoustových	inkoustový	k2eAgFnPc2d1
skvrn	skvrna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pacient	pacient	k1gMnSc1
si	se	k3xPyFc3
je	on	k3xPp3gFnPc4
má	mít	k5eAaImIp3nS
dotvořit	dotvořit	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
u	u	k7c2
každého	každý	k3xTgInSc2
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
co	co	k8xS
mu	on	k3xPp3gMnSc3
obrázek	obrázek	k1gInSc1
připomněl	připomnět	k5eAaPmAgInS
(	(	kIx(
<g/>
pareidolie	pareidolie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
si	se	k3xPyFc3
lidé	člověk	k1gMnPc1
seskupili	seskupit	k5eAaPmAgMnP
hvězdy	hvězda	k1gFnPc4
na	na	k7c6
obloze	obloha	k1gFnSc6
do	do	k7c2
„	„	k?
<g/>
souhvězdí	souhvězdí	k1gNnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
skupiny	skupina	k1gFnPc1
hvězd	hvězda	k1gFnPc2
jim	on	k3xPp3gMnPc3
patrně	patrně	k6eAd1
cosi	cosi	k3yInSc1
připomínaly	připomínat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Malíř	malíř	k1gMnSc1
či	či	k8xC
sochař	sochař	k1gMnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
nechat	nechat	k5eAaPmF
více	hodně	k6eAd2
nebo	nebo	k8xC
méně	málo	k6eAd2
vést	vést	k5eAaImF
svým	svůj	k3xOyFgInSc7
předmětem	předmět	k1gInSc7
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
ani	ani	k8xC
realistický	realistický	k2eAgInSc1d1
obraz	obraz	k1gInSc1
není	být	k5eNaImIp3nS
totéž	týž	k3xTgNnSc1
co	co	k9
fotografie	fotografie	k1gFnSc1
a	a	k8xC
každý	každý	k3xTgMnSc1
malíř	malíř	k1gMnSc1
namaluje	namalovat	k5eAaPmIp3nS
tentýž	týž	k3xTgInSc4
předmět	předmět	k1gInSc4
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
architekta	architekt	k1gMnSc4
a	a	k8xC
konstruktéra	konstruktér	k1gMnSc4
jsou	být	k5eAaImIp3nP
inspirací	inspirace	k1gFnSc7
jiné	jiný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
a	a	k8xC
konstrukce	konstrukce	k1gFnSc2
a	a	k8xC
ovšem	ovšem	k9
také	také	k9
požadavky	požadavek	k1gInPc1
na	na	k7c4
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
velcí	velký	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
a	a	k8xC
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
i	i	k8xC
konstruktéři	konstruktér	k1gMnPc1
mají	mít	k5eAaImIp3nP
také	také	k9
svůj	svůj	k3xOyFgInSc4
jedinečný	jedinečný	k2eAgInSc4d1
„	„	k?
<g/>
osobní	osobní	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
hraje	hrát	k5eAaImIp3nS
fantazie	fantazie	k1gFnSc1
v	v	k7c6
hudbě	hudba	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
přímé	přímý	k2eAgFnPc1d1
inspirace	inspirace	k1gFnPc1
zkušeností	zkušenost	k1gFnPc2
nejsou	být	k5eNaImIp3nP
právě	právě	k6eAd1
běžné	běžný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
Antonínu	Antonín	k1gMnSc6
Dvořákovi	Dvořák	k1gMnSc6
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
pro	pro	k7c4
jednu	jeden	k4xCgFnSc4
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
symfonií	symfonie	k1gFnPc2
inspiroval	inspirovat	k5eAaBmAgInS
kohoutím	kohoutí	k2eAgNnSc7d1
kokrháním	kokrhání	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
hudební	hudební	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
je	být	k5eAaImIp3nS
patrně	patrně	k6eAd1
čistě	čistě	k6eAd1
fantazijní	fantazijní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Specificky	specificky	k6eAd1
dětská	dětský	k2eAgFnSc1d1
fantazie	fantazie	k1gFnSc1
neexistuje	existovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
však	však	k9
rozlišují	rozlišovat	k5eAaImIp3nP
mezi	mezi	k7c7
uměleckou	umělecký	k2eAgFnSc7d1
a	a	k8xC
vědeckou	vědecký	k2eAgFnSc7d1
fantazií	fantazie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Důležité	důležitý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
rozlišovat	rozlišovat	k5eAaImF
<g/>
,	,	kIx,
zda	zda	k8xS
potulování	potulování	k1gNnSc1
mysli	mysl	k1gFnSc2
je	být	k5eAaImIp3nS
úmyslné	úmyslný	k2eAgNnSc1d1
či	či	k8xC
nikoli	nikoli	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
jednoduchých	jednoduchý	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
jej	on	k3xPp3gMnSc4
člověk	člověk	k1gMnSc1
zapojuje	zapojovat	k5eAaImIp3nS
z	z	k7c2
nudy	nuda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nečiní	činit	k5eNaImIp3nS
tak	tak	k6eAd1
u	u	k7c2
úkolů	úkol	k1gInPc2
vyžadující	vyžadující	k2eAgNnPc4d1
soustředění	soustředění	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klamné	klamný	k2eAgFnPc1d1
představy	představa	k1gFnPc1
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1
poznávací	poznávací	k2eAgFnPc1d1
schopnosti	schopnost	k1gFnPc1
se	se	k3xPyFc4
vyvinuly	vyvinout	k5eAaPmAgFnP
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
běžného	běžný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
potřebuje	potřebovat	k5eAaImIp3nS
rychle	rychle	k6eAd1
orientovat	orientovat	k5eAaBmF
a	a	k8xC
rozhodovat	rozhodovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představivost	představivost	k1gFnSc1
a	a	k8xC
představy	představa	k1gFnPc1
mu	on	k3xPp3gNnSc3
umožňují	umožňovat	k5eAaImIp3nP
rychle	rychle	k6eAd1
rozpoznat	rozpoznat	k5eAaPmF
příležitost	příležitost	k1gFnSc4
nebo	nebo	k8xC
nebezpečí	nebezpečí	k1gNnSc4
a	a	k8xC
jednat	jednat	k5eAaImF
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
právě	právě	k9
toto	tento	k3xDgNnSc1
snadné	snadný	k2eAgNnSc1d1
a	a	k8xC
rychlé	rychlý	k2eAgNnSc1d1
„	„	k?
<g/>
poznávání	poznávání	k1gNnSc1
<g/>
“	“	k?
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
omylům	omyl	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
odhalit	odhalit	k5eAaPmF
jen	jen	k9
podrobným	podrobný	k2eAgMnSc7d1
a	a	k8xC
pečlivým	pečlivý	k2eAgNnSc7d1
zkoumáním	zkoumání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickým	typický	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
dojem	dojem	k1gInSc4
„	„	k?
<g/>
mokré	mokrý	k2eAgFnSc2d1
vozovky	vozovka	k1gFnSc2
<g/>
“	“	k?
na	na	k7c6
asfaltu	asfalt	k1gInSc6
a	a	k8xC
v	v	k7c6
horkém	horký	k2eAgNnSc6d1
létě	léto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omyl	omyl	k1gInSc1
nevzniká	vznikat	k5eNaImIp3nS
špatnou	špatný	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
oka	oko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
představivostí	představivost	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
lesklou	lesklý	k2eAgFnSc4d1
vozovku	vozovka	k1gFnSc4
na	na	k7c6
základě	základ	k1gInSc6
zkušeností	zkušenost	k1gFnPc2
automaticky	automaticky	k6eAd1
vyhodnotí	vyhodnotit	k5eAaPmIp3nS
jako	jako	k9
mokrou	mokrý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řidič	řidič	k1gMnSc1
by	by	kYmCp3nS
musel	muset	k5eAaImAgMnS
zastavit	zastavit	k5eAaPmF
a	a	k8xC
přesvědčit	přesvědčit	k5eAaPmF
se	se	k3xPyFc4
například	například	k6eAd1
hmatem	hmat	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
úplně	úplně	k6eAd1
suchá	suchý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Iluze	iluze	k1gFnSc1
</s>
<s>
Poznání	poznání	k1gNnSc1
</s>
<s>
Vnímání	vnímání	k1gNnSc1
</s>
<s>
Vzpomínka	vzpomínka	k1gFnSc1
</s>
<s>
Eidetismus	Eidetismus	k1gInSc1
</s>
<s>
Halucinace	halucinace	k1gFnSc1
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Představa	představa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
20	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
610	#num#	k4
</s>
<s>
Filosofický	filosofický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
FIN	Fin	k1gMnSc1
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Představa	představa	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
332	#num#	k4
</s>
<s>
G.	G.	kA
Lakoff	Lakoff	k1gInSc1
<g/>
,	,	kIx,
Ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
oheň	oheň	k1gInSc1
a	a	k8xC
nebezpečné	bezpečný	k2eNgFnPc1d1
věci	věc	k1gFnPc1
<g/>
:	:	kIx,
co	co	k9
kategorie	kategorie	k1gFnSc2
vypovídají	vypovídat	k5eAaImIp3nP,k5eAaPmIp3nP
o	o	k7c6
naší	náš	k3xOp1gFnSc6
mysli	mysl	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Triáda	triáda	k1gFnSc1
2006	#num#	k4
–	–	k?
655	#num#	k4
s.	s.	k?
:	:	kIx,
obr	obr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86138-78-X	80-86138-78-X	k4
</s>
<s>
Říčan	Říčany	k1gInPc2
<g/>
,	,	kIx,
P.	P.	kA
<g/>
,	,	kIx,
Psychologie	psychologie	k1gFnSc2
osobnosti	osobnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
1973	#num#	k4
</s>
<s>
Říčan	Říčany	k1gInPc2
<g/>
,	,	kIx,
P.	P.	kA
<g/>
,	,	kIx,
Psychologie	psychologie	k1gFnSc1
<g/>
:	:	kIx,
příručka	příručka	k1gFnSc1
pro	pro	k7c4
studenty	student	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
–	–	k?
286	#num#	k4
s.	s.	k?
<g/>
;	;	kIx,
21	#num#	k4
cm	cm	kA
ISBN	ISBN	kA
80-7178-923-2	80-7178-923-2	k4
</s>
<s>
Sokol	Sokol	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
24	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
G.	G.	kA
Lakoff	Lakoff	k1gInSc1
<g/>
,	,	kIx,
Ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
oheň	oheň	k1gInSc1
a	a	k8xC
nebezpečné	bezpečný	k2eNgFnPc1d1
věci	věc	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Triáda	triáda	k1gFnSc1
2006	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.ncbi.nlm.nih.gov/pubmed/9418217	http://www.ncbi.nlm.nih.gov/pubmed/9418217	k4
-	-	kIx~
Thinking	Thinking	k1gInSc1
about	about	k1gInSc1
fantasy	fantas	k1gInPc1
<g/>
:	:	kIx,
are	ar	k1gInSc5
children	childrna	k1gFnPc2
fundamentally	fundamentalla	k1gFnSc2
different	different	k1gInSc1
thinkers	thinkers	k1gInSc1
and	and	k?
believers	believers	k6eAd1
from	from	k6eAd1
adults	adults	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
http://www.scienceworld.cz/clovek/feynman-mohlo-by-umeni-mit-vlastnosti-vedy-340/	http://www.scienceworld.cz/clovek/feynman-mohlo-by-umeni-mit-vlastnosti-vedy-340/	k4
-	-	kIx~
Feynman	Feynman	k1gMnSc1
<g/>
:	:	kIx,
Mohlo	moct	k5eAaImAgNnS
by	by	kYmCp3nS
umění	umění	k1gNnSc1
mít	mít	k5eAaImF
vlastnosti	vlastnost	k1gFnPc4
vědy	věda	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
http://medicalxpress.com/news/2016-03-mind-equal.html	http://medicalxpress.com/news/2016-03-mind-equal.htmla	k1gFnPc2
-	-	kIx~
Not	nota	k1gFnPc2
all	all	k?
mind	mind	k1gInSc1
wandering	wandering	k1gInSc1
is	is	k?
created	created	k1gInSc1
equal	equal	k1gInSc1
<g/>
↑	↑	k?
J.	J.	kA
Sokol	Sokol	k1gMnSc1
<g/>
,	,	kIx,
Malá	malý	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
104	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
představa	představa	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Představa	představa	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Téma	téma	k1gNnSc1
Fantazie	fantazie	k1gFnSc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
</s>
