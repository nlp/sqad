<s>
Představou	představa	k1gFnSc7
tedy	tedy	k9
rozumíme	rozumět	k5eAaImIp1nP
celkový	celkový	k2eAgInSc4d1
a	a	k8xC
málo	málo	k6eAd1
určitý	určitý	k2eAgInSc1d1
„	„	k?
<g/>
obraz	obraz	k1gInSc1
<g/>
“	“	k?
nějaké	nějaký	k3yIgFnSc3
věci	věc	k1gFnSc3
<g/>
,	,	kIx,
předmětu	předmět	k1gInSc3
nebo	nebo	k8xC
záměru	záměr	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
obvykle	obvykle	k6eAd1
nedovedeme	dovést	k5eNaPmIp1nP
popsat	popsat	k5eAaPmF
a	a	k8xC
vyjádřit	vyjádřit	k5eAaPmF
slovy	slovo	k1gNnPc7
<g/>
.	.	kIx.
</s>