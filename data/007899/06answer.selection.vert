<s>
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
Canada	Canada	k1gFnSc1	Canada
<g/>
,	,	kIx,	,
vyslovuj	vyslovovat	k5eAaImRp2nS	vyslovovat
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
[	[	kIx(	[
<g/>
kanada	kanada	k1gFnSc1	kanada
<g/>
]	]	kIx)	]
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozlohou	rozloha	k1gFnSc7	rozloha
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
země	země	k1gFnSc1	země
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
