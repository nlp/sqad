<s>
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
Canada	Canada	k1gFnSc1	Canada
<g/>
,	,	kIx,	,
vyslovuj	vyslovovat	k5eAaImRp2nS	vyslovovat
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
[	[	kIx(	[
<g/>
kanada	kanada	k1gFnSc1	kanada
<g/>
]	]	kIx)	]
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozlohou	rozloha	k1gFnSc7	rozloha
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
země	země	k1gFnSc1	země
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
se	se	k3xPyFc4	se
Severním	severní	k2eAgInSc7d1	severní
ledovým	ledový	k2eAgInSc7d1	ledový
oceánem	oceán	k1gInSc7	oceán
(	(	kIx(	(
<g/>
sever	sever	k1gInSc1	sever
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Atlantikem	Atlantik	k1gInSc7	Atlantik
(	(	kIx(	(
<g/>
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
jih	jih	k1gInSc1	jih
a	a	k8xC	a
severozápad	severozápad	k1gInSc1	severozápad
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
(	(	kIx(	(
<g/>
západ	západ	k1gInSc1	západ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
osídleném	osídlený	k2eAgNnSc6d1	osídlené
Indiány	Indián	k1gMnPc7	Indián
a	a	k8xC	a
Eskymáky	Eskymák	k1gMnPc7	Eskymák
jako	jako	k8xC	jako
unie	unie	k1gFnSc1	unie
britských	britský	k2eAgNnPc2d1	Britské
zámořských	zámořský	k2eAgNnPc2d1	zámořské
teritorií	teritorium	k1gNnPc2	teritorium
a	a	k8xC	a
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
předtím	předtím	k6eAd1	předtím
součástí	součást	k1gFnSc7	součást
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
získala	získat	k5eAaPmAgFnS	získat
mírovou	mírový	k2eAgFnSc7d1	mírová
cestou	cesta	k1gFnSc7	cesta
Zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
Britské	britský	k2eAgFnSc6d1	britská
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
British	British	k1gMnSc1	British
North	North	k1gMnSc1	North
America	America	k1gMnSc1	America
Act	Act	k1gMnSc1	Act
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
a	a	k8xC	a
Canada	Canada	k1gFnSc1	Canada
Actem	Actem	k1gInSc1	Actem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
federací	federace	k1gFnSc7	federace
deseti	deset	k4xCc2	deset
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
tří	tři	k4xCgNnPc2	tři
spolkových	spolkový	k2eAgNnPc2d1	Spolkové
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
,	,	kIx,	,
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
zemi	zem	k1gFnSc4	zem
dvoujazyčnou	dvoujazyčný	k2eAgFnSc4d1	dvoujazyčná
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgInPc1d1	oficiální
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
)	)	kIx)	)
a	a	k8xC	a
multikulturní	multikulturní	k2eAgMnSc1d1	multikulturní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
hlediska	hledisko	k1gNnSc2	hledisko
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
technicky	technicky	k6eAd1	technicky
vyspělou	vyspělý	k2eAgFnSc4d1	vyspělá
zemi	zem	k1gFnSc4	zem
disponující	disponující	k2eAgFnSc7d1	disponující
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
přírodními	přírodní	k2eAgInPc7d1	přírodní
a	a	k8xC	a
nerostnými	nerostný	k2eAgInPc7d1	nerostný
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
úzké	úzký	k2eAgInPc4d1	úzký
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
vojensky	vojensky	k6eAd1	vojensky
nestřeženou	střežený	k2eNgFnSc4d1	nestřežená
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
dalším	další	k2eAgInSc7d1	další
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
lze	lze	k6eAd1	lze
smysluplně	smysluplně	k6eAd1	smysluplně
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
od	od	k7c2	od
jehož	jenž	k3xRgNnSc2	jenž
závislého	závislý	k2eAgNnSc2d1	závislé
území	území	k1gNnSc2	území
-	-	kIx~	-
Grónska	Grónsko	k1gNnSc2	Grónsko
-	-	kIx~	-
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
kanadské	kanadský	k2eAgInPc4d1	kanadský
arktické	arktický	k2eAgInPc4d1	arktický
ostrovy	ostrov	k1gInPc4	ostrov
jen	jen	k9	jen
úzké	úzký	k2eAgInPc4d1	úzký
průlivy	průliv	k1gInPc4	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Canada	Canada	k1gFnSc1	Canada
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeči	řeč	k1gFnSc2	řeč
prvních	první	k4xOgInPc2	první
národů	národ	k1gInPc2	národ
-	-	kIx~	-
konkrétněji	konkrétně	k6eAd2	konkrétně
kmene	kmen	k1gInSc2	kmen
Huronů	Huron	k1gInPc2	Huron
-	-	kIx~	-
a	a	k8xC	a
zní	znět	k5eAaImIp3nS	znět
kanata	kanat	k2eAgFnSc1d1	kanat
a	a	k8xC	a
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jej	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
uskupení	uskupení	k1gNnSc1	uskupení
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
osídlení	osídlení	k1gNnSc1	osídlení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1535	[number]	k4	1535
použili	použít	k5eAaPmAgMnP	použít
indiáni	indián	k1gMnPc1	indián
toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
osady	osada	k1gFnSc2	osada
Stadacona	Stadacona	k1gFnSc1	Stadacona
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Jacquesem	Jacques	k1gMnSc7	Jacques
Cartierem	Cartier	k1gMnSc7	Cartier
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
oblasti	oblast	k1gFnSc2	oblast
okolo	okolo	k7c2	okolo
své	svůj	k3xOyFgFnSc2	svůj
osady	osada	k1gFnSc2	osada
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
jako	jako	k8xS	jako
část	část	k1gFnSc1	část
Québec	Québec	k1gInSc4	Québec
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
jako	jako	k8xS	jako
označení	označení	k1gNnSc1	označení
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
okolní	okolní	k2eAgFnSc2d1	okolní
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
původní	původní	k2eAgNnSc4d1	původní
označení	označení	k1gNnSc4	označení
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přestávalo	přestávat	k5eAaImAgNnS	přestávat
používat	používat	k5eAaImF	používat
a	a	k8xC	a
Canada	Canada	k1gFnSc1	Canada
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
území	území	k1gNnSc2	území
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
USA	USA	kA	USA
a	a	k8xC	a
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
kolonie	kolonie	k1gFnSc1	kolonie
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Francie	Francie	k1gFnSc1	Francie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
a	a	k8xC	a
severně	severně	k6eAd1	severně
od	od	k7c2	od
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
získala	získat	k5eAaPmAgFnS	získat
území	území	k1gNnSc4	území
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
posléze	posléze	k6eAd1	posléze
ustavila	ustavit	k5eAaPmAgFnS	ustavit
dvě	dva	k4xCgFnPc4	dva
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
:	:	kIx,	:
Horní	horní	k2eAgInSc1d1	horní
(	(	kIx(	(
<g/>
Upper	Upper	k1gInSc1	Upper
Canada	Canada	k1gFnSc1	Canada
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc4d1	dolní
Kanadu	Kanada	k1gFnSc4	Kanada
(	(	kIx(	(
<g/>
Lower	Lower	k1gInSc1	Lower
Canada	Canada	k1gFnSc1	Canada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
Kanady	Kanada	k1gFnSc2	Kanada
The	The	k1gMnSc1	The
Canadas	Canadas	k1gMnSc1	Canadas
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
sjednoceny	sjednotit	k5eAaPmNgFnP	sjednotit
v	v	k7c4	v
Sjednocenou	sjednocený	k2eAgFnSc4d1	sjednocená
kanadskou	kanadský	k2eAgFnSc4d1	kanadská
provincii	provincie	k1gFnSc4	provincie
(	(	kIx(	(
<g/>
United	United	k1gInSc1	United
Province	province	k1gFnSc2	province
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
se	se	k3xPyFc4	se
Kanada	Kanada	k1gFnSc1	Kanada
stala	stát	k5eAaPmAgFnS	stát
konfederací	konfederace	k1gFnSc7	konfederace
a	a	k8xC	a
britským	britský	k2eAgNnSc7d1	Britské
dominiem	dominion	k1gNnSc7	dominion
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
Dominion	dominion	k1gNnSc1	dominion
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Canadian	Canadian	k1gInSc1	Canadian
Confederation	Confederation	k1gInSc1	Confederation
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
mírou	míra	k1gFnSc7	míra
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
vypouštělo	vypouštět	k5eAaImAgNnS	vypouštět
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
Dominion	dominion	k1gNnSc1	dominion
of	of	k?	of
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
Canada	Canada	k1gFnSc1	Canada
Act	Act	k1gMnSc1	Act
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
jediné	jediný	k2eAgNnSc4d1	jediné
oficiální	oficiální	k2eAgNnSc4d1	oficiální
jméno	jméno	k1gNnSc4	jméno
země	zem	k1gFnSc2	zem
Canada	Canada	k1gFnSc1	Canada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
Dominion	dominion	k1gNnSc1	dominion
Day	Day	k1gFnSc2	Day
na	na	k7c6	na
Canada	Canada	k1gFnSc1	Canada
Day	Day	k1gFnPc7	Day
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgFnPc1d1	archeologická
studie	studie	k1gFnPc1	studie
datují	datovat	k5eAaImIp3nP	datovat
první	první	k4xOgNnSc4	první
lidské	lidský	k2eAgNnSc4d1	lidské
osídlení	osídlení	k1gNnSc4	osídlení
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
cca	cca	kA	cca
26	[number]	k4	26
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
případě	případ	k1gInSc6	případ
severního	severní	k2eAgInSc2d1	severní
Yukonu	Yukon	k1gInSc2	Yukon
a	a	k8xC	a
před	před	k7c7	před
cca	cca	kA	cca
9	[number]	k4	9
500	[number]	k4	500
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jižního	jižní	k2eAgNnSc2d1	jižní
Ontaria	Ontario	k1gNnSc2	Ontario
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
evropské	evropský	k2eAgNnSc4d1	Evropské
osídlení	osídlení	k1gNnSc4	osídlení
založili	založit	k5eAaPmAgMnP	založit
na	na	k7c6	na
kanadském	kanadský	k2eAgNnSc6d1	kanadské
území	území	k1gNnSc6	území
Vikingové	Viking	k1gMnPc1	Viking
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
v	v	k7c6	v
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Anse	Anse	k1gInSc1	Anse
aux	aux	k?	aux
Meadows	Meadows	k1gInSc1	Meadows
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
existovalo	existovat	k5eAaImAgNnS	existovat
pouze	pouze	k6eAd1	pouze
krátce	krátce	k6eAd1	krátce
a	a	k8xC	a
z	z	k7c2	z
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
hlediska	hledisko	k1gNnSc2	hledisko
nemělo	mít	k5eNaImAgNnS	mít
žádný	žádný	k3yNgInSc4	žádný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
její	její	k3xOp3gInPc4	její
východní	východní	k2eAgInPc4d1	východní
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
znovu	znovu	k6eAd1	znovu
objevena	objevit	k5eAaPmNgFnS	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
plavbou	plavba	k1gFnSc7	plavba
Johna	John	k1gMnSc2	John
Cabota	Cabot	k1gMnSc2	Cabot
(	(	kIx(	(
<g/>
1497	[number]	k4	1497
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc2d1	další
výpravy	výprava	k1gFnSc2	výprava
sem	sem	k6eAd1	sem
podnikli	podniknout	k5eAaPmAgMnP	podniknout
Jacques	Jacques	k1gMnSc1	Jacques
Cartier	Cartier	k1gMnSc1	Cartier
(	(	kIx(	(
<g/>
1534	[number]	k4	1534
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
evropské	evropský	k2eAgNnSc4d1	Evropské
osídlení	osídlení	k1gNnSc4	osídlení
zde	zde	k6eAd1	zde
založili	založit	k5eAaPmAgMnP	založit
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
Port	port	k1gInSc4	port
Royal	Royal	k1gInSc1	Royal
(	(	kIx(	(
<g/>
1605	[number]	k4	1605
<g/>
)	)	kIx)	)
a	a	k8xC	a
Québec	Québec	k1gMnSc1	Québec
(	(	kIx(	(
<g/>
1608	[number]	k4	1608
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
je	on	k3xPp3gNnSc4	on
následovali	následovat	k5eAaImAgMnP	následovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Newfoundlandu	Newfoundland	k1gInSc2	Newfoundland
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
se	se	k3xPyFc4	se
na	na	k7c6	na
kanadském	kanadský	k2eAgNnSc6d1	kanadské
území	území	k1gNnSc6	území
začaly	začít	k5eAaPmAgFnP	začít
rychle	rychle	k6eAd1	rychle
šířit	šířit	k5eAaImF	šířit
evropské	evropský	k2eAgFnPc4d1	Evropská
choroby	choroba	k1gFnPc4	choroba
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
decimovaly	decimovat	k5eAaBmAgFnP	decimovat
původní	původní	k2eAgMnPc4d1	původní
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
otevíraly	otevírat	k5eAaImAgInP	otevírat
kolonistům	kolonista	k1gMnPc3	kolonista
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
indiánských	indiánský	k2eAgNnPc2d1	indiánské
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
kolonizovali	kolonizovat	k5eAaBmAgMnP	kolonizovat
okolí	okolí	k1gNnSc2	okolí
řeky	řeka	k1gFnSc2	řeka
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Britové	Brit	k1gMnPc1	Brit
osidlovali	osidlovat	k5eAaImAgMnP	osidlovat
třináct	třináct	k4xCc4	třináct
kolonií	kolonie	k1gFnPc2	kolonie
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
dnešních	dnešní	k2eAgMnPc2d1	dnešní
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
Země	zem	k1gFnSc2	zem
prince	princ	k1gMnSc2	princ
Ruprechta	Ruprecht	k1gMnSc2	Ruprecht
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1689	[number]	k4	1689
<g/>
-	-	kIx~	-
<g/>
1763	[number]	k4	1763
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sérii	série	k1gFnSc3	série
tzv.	tzv.	kA	tzv.
francouzských	francouzský	k2eAgFnPc2d1	francouzská
a	a	k8xC	a
indiánských	indiánský	k2eAgFnPc2d1	indiánská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
války	válka	k1gFnPc1	válka
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
zánik	zánik	k1gInSc4	zánik
francouzské	francouzský	k2eAgFnSc2d1	francouzská
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připadla	připadnout	k5eAaPmAgFnS	připadnout
Velké	velká	k1gFnPc4	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1774	[number]	k4	1774
schválila	schválit	k5eAaPmAgFnS	schválit
pro	pro	k7c4	pro
uklidnění	uklidnění	k1gNnSc4	uklidnění
situace	situace	k1gFnSc2	situace
tzv.	tzv.	kA	tzv.
Quebec	Quebec	k1gMnSc1	Quebec
Act	Act	k1gMnSc1	Act
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
akceptoval	akceptovat	k5eAaBmAgMnS	akceptovat
katolické	katolický	k2eAgNnSc4d1	katolické
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgInSc4d1	francouzský
civilní	civilní	k2eAgInSc4d1	civilní
zákoník	zákoník	k1gInSc4	zákoník
a	a	k8xC	a
francouzštinu	francouzština	k1gFnSc4	francouzština
jakožto	jakožto	k8xS	jakožto
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
na	na	k7c6	na
území	území	k1gNnSc6	území
provincie	provincie	k1gFnSc2	provincie
Québec	Québec	k1gInSc1	Québec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
se	se	k3xPyFc4	se
Kanada	Kanada	k1gFnSc1	Kanada
nepřipojila	připojit	k5eNaPmAgFnS	připojit
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
13	[number]	k4	13
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
cílovou	cílový	k2eAgFnSc7d1	cílová
zemí	zem	k1gFnSc7	zem
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
loajalistů	loajalista	k1gMnPc2	loajalista
odcházejících	odcházející	k2eAgMnPc2d1	odcházející
z	z	k7c2	z
území	území	k1gNnPc2	území
dnešních	dnešní	k2eAgNnPc2d1	dnešní
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c6	na
území	území	k1gNnSc6	území
Québecu	Québecus	k1gInSc2	Québecus
řada	řada	k1gFnSc1	řada
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgMnPc2d1	mluvící
protestantů	protestant	k1gMnPc2	protestant
usídlujících	usídlující	k2eAgMnPc2d1	usídlující
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
rozdělení	rozdělení	k1gNnSc3	rozdělení
provincie	provincie	k1gFnSc2	provincie
Québec	Québec	k1gInSc1	Québec
na	na	k7c4	na
Horní	horní	k2eAgFnSc4d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc4d1	dolní
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Horní	horní	k2eAgFnSc4d1	horní
Kanadu	Kanada	k1gFnSc4	Kanada
(	(	kIx(	(
<g/>
nynější	nynější	k2eAgFnSc1d1	nynější
provincie	provincie	k1gFnSc1	provincie
Ontario	Ontario	k1gNnSc4	Ontario
<g/>
)	)	kIx)	)
nebyl	být	k5eNaImAgInS	být
Quebec	Quebec	k1gMnSc1	Quebec
Act	Act	k1gMnSc1	Act
nadále	nadále	k6eAd1	nadále
relevantní	relevantní	k2eAgMnSc1d1	relevantní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
britsko-americké	britskomerický	k2eAgFnSc6d1	britsko-americká
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1812	[number]	k4	1812
<g/>
-	-	kIx~	-
<g/>
1815	[number]	k4	1815
hrála	hrát	k5eAaImAgFnS	hrát
kanadská	kanadský	k2eAgFnSc1d1	kanadská
fronta	fronta	k1gFnSc1	fronta
enormně	enormně	k6eAd1	enormně
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
se	se	k3xPyFc4	se
opakovaně	opakovaně	k6eAd1	opakovaně
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
obsadit	obsadit	k5eAaPmF	obsadit
Horní	horní	k2eAgFnSc4d1	horní
i	i	k8xC	i
Dolní	dolní	k2eAgFnSc4d1	dolní
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInPc1	jejich
vpády	vpád	k1gInPc1	vpád
byly	být	k5eAaImAgInP	být
odraženy	odrazit	k5eAaPmNgInP	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
postoj	postoj	k1gInSc4	postoj
indiánského	indiánský	k2eAgNnSc2d1	indiánské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podporovalo	podporovat	k5eAaImAgNnS	podporovat
spíše	spíše	k9	spíše
Brity	Brit	k1gMnPc4	Brit
(	(	kIx(	(
<g/>
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
straně	strana	k1gFnSc6	strana
bojovaly	bojovat	k5eAaImAgInP	bojovat
hlavní	hlavní	k2eAgInPc1d1	hlavní
kmeny	kmen	k1gInPc1	kmen
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
i	i	k8xC	i
Tecumsehova	Tecumsehova	k1gFnSc1	Tecumsehova
aliance	aliance	k1gFnSc2	aliance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
místních	místní	k2eAgMnPc2d1	místní
kolonistů	kolonista	k1gMnPc2	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
Války	válka	k1gFnPc1	válka
se	se	k3xPyFc4	se
francouzští	francouzštit	k5eAaImIp3nP	francouzštit
obyvatelé	obyvatel	k1gMnPc1	obyvatel
většinou	většinou	k6eAd1	většinou
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Britů	Brit	k1gMnPc2	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
USA	USA	kA	USA
skončila	skončit	k5eAaPmAgFnS	skončit
nakonec	nakonec	k6eAd1	nakonec
uznáním	uznání	k1gNnSc7	uznání
statu	status	k1gInSc2	status
quo	quo	k?	quo
ante	ante	k1gInSc1	ante
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
expanze	expanze	k1gFnSc1	expanze
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
západ	západ	k1gInSc4	západ
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
Společnosti	společnost	k1gFnSc2	společnost
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
intenzívně	intenzívně	k6eAd1	intenzívně
probíhala	probíhat	k5eAaImAgFnS	probíhat
již	již	k9	již
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
části	část	k1gFnSc2	část
francouzského	francouzský	k2eAgNnSc2d1	francouzské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
povstání	povstání	k1gNnSc4	povstání
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
potlačení	potlačení	k1gNnSc6	potlačení
se	se	k3xPyFc4	se
Británie	Británie	k1gFnSc1	Británie
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
Horní	horní	k2eAgFnSc2d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc2d1	dolní
Kanady	Kanada	k1gFnSc2	Kanada
jeden	jeden	k4xCgInSc1	jeden
celek	celek	k1gInSc1	celek
(	(	kIx(	(
<g/>
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
kanadská	kanadský	k2eAgFnSc1d1	kanadská
provincie	provincie	k1gFnSc1	provincie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
byly	být	k5eAaImAgInP	být
podepsáním	podepsání	k1gNnSc7	podepsání
Oregonské	Oregonský	k2eAgFnSc2d1	Oregonská
úmluvy	úmluva	k1gFnSc2	úmluva
ukončeny	ukončen	k2eAgFnPc4d1	ukončena
teritoriální	teritoriální	k2eAgFnPc4d1	teritoriální
spory	spora	k1gFnPc4	spora
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Kanadou	Kanada	k1gFnSc7	Kanada
(	(	kIx(	(
<g/>
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
na	na	k7c6	na
západě	západ	k1gInSc6	západ
byla	být	k5eAaImAgFnS	být
určena	určen	k2eAgFnSc1d1	určena
49	[number]	k4	49
<g/>
.	.	kIx.	.
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prudký	prudký	k2eAgInSc1d1	prudký
růst	růst	k1gInSc1	růst
kanadské	kanadský	k2eAgFnSc2d1	kanadská
populace	populace	k1gFnSc2	populace
díky	díky	k7c3	díky
velké	velký	k2eAgFnSc3d1	velká
imigraci	imigrace	k1gFnSc3	imigrace
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
vysoké	vysoký	k2eAgFnSc2d1	vysoká
porodnosti	porodnost	k1gFnSc2	porodnost
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
osidlování	osidlování	k1gNnSc3	osidlování
kanadského	kanadský	k2eAgNnSc2d1	kanadské
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
a	a	k8xC	a
zakládání	zakládání	k1gNnSc6	zakládání
nových	nový	k2eAgFnPc2d1	nová
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
:	:	kIx,	:
Vancouver	Vancouver	k1gInSc1	Vancouver
a	a	k8xC	a
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
někteří	některý	k3yIgMnPc1	některý
Evropané	Evropan	k1gMnPc1	Evropan
začali	začít	k5eAaPmAgMnP	začít
Kanadu	Kanada	k1gFnSc4	Kanada
opouštět	opouštět	k5eAaImF	opouštět
a	a	k8xC	a
odcházeli	odcházet	k5eAaImAgMnP	odcházet
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
Britské	britský	k2eAgFnSc6d1	britská
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sjednotil	sjednotit	k5eAaPmAgMnS	sjednotit
britské	britský	k2eAgFnSc2d1	britská
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
kolonie	kolonie	k1gFnSc2	kolonie
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
provincie	provincie	k1gFnSc1	provincie
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Skotsko	Skotsko	k1gNnSc1	Skotsko
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Brunšvik	Brunšvik	k1gInSc1	Brunšvik
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
autonomní	autonomní	k2eAgNnSc4d1	autonomní
dominium	dominium	k1gNnSc4	dominium
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
znovuvytvoření	znovuvytvoření	k1gNnSc3	znovuvytvoření
provincií	provincie	k1gFnPc2	provincie
Ontario	Ontario	k1gNnSc4	Ontario
a	a	k8xC	a
Québec	Québec	k1gInSc4	Québec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
provincií	provincie	k1gFnPc2	provincie
byla	být	k5eAaImAgFnS	být
začleněna	začleněn	k2eAgFnSc1d1	začleněna
Ruprechtova	Ruprechtův	k2eAgFnSc1d1	Ruprechtova
země	země	k1gFnSc1	země
a	a	k8xC	a
přičleněny	přičleněn	k2eAgFnPc1d1	přičleněna
byly	být	k5eAaImAgFnP	být
oblasti	oblast	k1gFnPc1	oblast
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
společnosti	společnost	k1gFnSc2	společnost
jakožto	jakožto	k8xS	jakožto
Severozápadní	severozápadní	k2eAgNnPc4d1	severozápadní
teritoria	teritorium	k1gNnPc4	teritorium
a	a	k8xC	a
nově	nově	k6eAd1	nově
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
provincie	provincie	k1gFnSc1	provincie
Manitoba	Manitoba	k1gFnSc1	Manitoba
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
s	s	k7c7	s
Vancouverem	Vancouvero	k1gNnSc7	Vancouvero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osidlování	osidlování	k1gNnSc1	osidlování
nových	nový	k2eAgNnPc2d1	nové
území	území	k1gNnPc2	území
bylo	být	k5eAaImAgNnS	být
podpořeno	podpořit	k5eAaPmNgNnS	podpořit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
výstavbou	výstavba	k1gFnSc7	výstavba
tři	tři	k4xCgFnPc4	tři
transkontinentálních	transkontinentální	k2eAgFnPc2d1	transkontinentální
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
ustavením	ustavení	k1gNnSc7	ustavení
královské	královský	k2eAgFnSc2d1	královská
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
jízdní	jízdní	k2eAgFnSc2d1	jízdní
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
další	další	k2eAgFnSc1d1	další
kolonie	kolonie	k1gFnSc1	kolonie
(	(	kIx(	(
<g/>
Ostrov	ostrov	k1gInSc1	ostrov
prince	princ	k1gMnSc2	princ
Edvarda	Edvard	k1gMnSc2	Edvard
<g/>
)	)	kIx)	)
a	a	k8xC	a
provinční	provinční	k2eAgInSc4d1	provinční
status	status	k1gInSc4	status
získaly	získat	k5eAaPmAgFnP	získat
části	část	k1gFnPc1	část
Severozápadních	severozápadní	k2eAgNnPc2d1	severozápadní
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
:	:	kIx,	:
Alberta	Alberta	k1gFnSc1	Alberta
a	a	k8xC	a
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
automaticky	automaticky	k6eAd1	automaticky
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
války	válka	k1gFnSc2	válka
Velkou	velká	k1gFnSc7	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
vyslala	vyslat	k5eAaPmAgFnS	vyslat
na	na	k7c4	na
evropská	evropský	k2eAgNnPc4d1	Evropské
bojiště	bojiště	k1gNnSc4	bojiště
kontingent	kontingent	k1gInSc4	kontingent
složený	složený	k2eAgInSc4d1	složený
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Ztráty	ztráta	k1gFnPc1	ztráta
však	však	k9	však
byly	být	k5eAaImAgFnP	být
natolik	natolik	k6eAd1	natolik
těžké	těžký	k2eAgFnPc1d1	těžká
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
doplnění	doplnění	k1gNnSc4	doplnění
kanadských	kanadský	k2eAgFnPc2d1	kanadská
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
musel	muset	k5eAaImAgMnS	muset
premiér	premiér	k1gMnSc1	premiér
Robert	Robert	k1gMnSc1	Robert
Borden	Bordna	k1gFnPc2	Bordna
vypsat	vypsat	k5eAaPmF	vypsat
povinné	povinný	k2eAgInPc4d1	povinný
odvody	odvod	k1gInPc4	odvod
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
Kanada	Kanada	k1gFnSc1	Kanada
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dalšího	další	k2eAgNnSc2d1	další
osamostatňování	osamostatňování	k1gNnSc2	osamostatňování
sama	sám	k3xTgFnSc1	sám
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
Westminsterský	Westminsterský	k2eAgInSc1d1	Westminsterský
statut	statut	k1gInSc1	statut
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
její	její	k3xOp3gFnSc4	její
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
i	i	k9	i
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
Kanada	Kanada	k1gFnSc1	Kanada
podporovala	podporovat	k5eAaImAgFnS	podporovat
politiku	politika	k1gFnSc4	politika
appeasementu	appeasement	k1gInSc2	appeasement
vůči	vůči	k7c3	vůči
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
po	po	k7c4	po
přepadení	přepadení	k1gNnSc4	přepadení
Polska	Polsko	k1gNnSc2	Polsko
pak	pak	k6eAd1	pak
její	její	k3xOp3gMnSc1	její
premiér	premiér	k1gMnSc1	premiér
William	William	k1gInSc4	William
Lyon	Lyon	k1gInSc1	Lyon
Mackenzie	Mackenzie	k1gFnSc2	Mackenzie
King	King	k1gMnSc1	King
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
kanadském	kanadský	k2eAgInSc6d1	kanadský
parlamentu	parlament	k1gInSc6	parlament
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obrovskému	obrovský	k2eAgInSc3d1	obrovský
rozvoji	rozvoj	k1gInSc3	rozvoj
válečného	válečný	k2eAgInSc2d1	válečný
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
zásobovala	zásobovat	k5eAaImAgFnS	zásobovat
jeho	jeho	k3xOp3gInPc4	jeho
produkty	produkt	k1gInPc4	produkt
nejen	nejen	k6eAd1	nejen
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
pozemní	pozemní	k2eAgFnPc1d1	pozemní
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
nejdříve	dříve	k6eAd3	dříve
dislokovány	dislokovat	k5eAaBmNgFnP	dislokovat
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
válečných	válečný	k2eAgFnPc2d1	válečná
operací	operace	k1gFnSc7	operace
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
i	i	k8xC	i
válečné	válečný	k2eAgNnSc1d1	válečné
loďstvo	loďstvo	k1gNnSc1	loďstvo
hrálo	hrát	k5eAaImAgNnS	hrát
enormně	enormně	k6eAd1	enormně
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Atlantik	Atlantik	k1gInSc4	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
formálně	formálně	k6eAd1	formálně
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Dominium	dominium	k1gNnSc1	dominium
Newfoundland	Newfoundlanda	k1gFnPc2	Newfoundlanda
připojilo	připojit	k5eAaPmAgNnS	připojit
ke	k	k7c3	k
Kanadě	Kanada	k1gFnSc3	Kanada
jako	jako	k8xS	jako
její	její	k3xOp3gInSc1	její
10	[number]	k4	10
<g/>
.	.	kIx.	.
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
silné	silný	k2eAgFnSc2d1	silná
poválečné	poválečný	k2eAgFnSc2d1	poválečná
imigrace	imigrace	k1gFnSc2	imigrace
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
tichá	tichý	k2eAgFnSc1d1	tichá
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
Québecu	Québecus	k1gInSc6	Québecus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zřetelně	zřetelně	k6eAd1	zřetelně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
podíl	podíl	k1gInSc1	podíl
francouzsky	francouzsky	k6eAd1	francouzsky
mluvících	mluvící	k2eAgMnPc2d1	mluvící
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
sekularizaci	sekularizace	k1gFnSc3	sekularizace
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
tzv.	tzv.	kA	tzv.
o	o	k7c6	o
patriaci	patriace	k1gFnSc6	patriace
(	(	kIx(	(
<g/>
Patriation	Patriation	k1gInSc1	Patriation
<g/>
)	)	kIx)	)
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
definitivně	definitivně	k6eAd1	definitivně
učinit	učinit	k5eAaPmF	učinit
Kanadu	Kanada	k1gFnSc4	Kanada
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1982	[number]	k4	1982
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
představitelů	představitel	k1gMnPc2	představitel
Québecu	Québecus	k1gInSc2	Québecus
k	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
Kanada	Kanada	k1gFnSc1	Kanada
stala	stát	k5eAaPmAgFnS	stát
zcela	zcela	k6eAd1	zcela
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
monarcha	monarcha	k1gMnSc1	monarcha
sdílený	sdílený	k2eAgMnSc1d1	sdílený
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgMnPc1d1	sociální
a	a	k8xC	a
společenské	společenský	k2eAgFnPc1d1	společenská
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kanadského	kanadský	k2eAgNnSc2d1	kanadské
zřízení	zřízení	k1gNnSc2	zřízení
a	a	k8xC	a
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
ústavních	ústavní	k2eAgFnPc2d1	ústavní
změn	změna	k1gFnPc2	změna
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
nárůstu	nárůst	k1gInSc3	nárůst
autonomizačních	autonomizační	k2eAgFnPc2d1	autonomizační
a	a	k8xC	a
separatistických	separatistický	k2eAgFnPc2d1	separatistická
snah	snaha	k1gFnPc2	snaha
Québecu	Québecus	k1gInSc2	Québecus
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
dvě	dva	k4xCgNnPc4	dva
referenda	referendum	k1gNnPc4	referendum
o	o	k7c4	o
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
této	tento	k3xDgFnSc2	tento
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
bylo	být	k5eAaImAgNnS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zamítnuto	zamítnout	k5eAaPmNgNnS	zamítnout
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
změnou	změna	k1gFnSc7	změna
ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
druhého	druhý	k4xOgMnSc4	druhý
byl	být	k5eAaImAgInS	být
již	již	k9	již
velmi	velmi	k6eAd1	velmi
těsný	těsný	k2eAgMnSc1d1	těsný
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
odtržení	odtržení	k1gNnSc4	odtržení
49,4	[number]	k4	49,4
<g/>
%	%	kIx~	%
hlasujících	hlasující	k1gMnPc2	hlasující
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kanadský	kanadský	k2eAgInSc1d1	kanadský
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
však	však	k9	však
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
,	,	kIx,	,
že	že	k8xS	že
Québec	Québec	k1gMnSc1	Québec
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
jednostranné	jednostranný	k2eAgNnSc4d1	jednostranné
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Separatistické	separatistický	k2eAgNnSc1d1	separatistické
hnutí	hnutí	k1gNnSc1	hnutí
v	v	k7c6	v
Québecu	Québecum	k1gNnSc6	Québecum
ovšem	ovšem	k9	ovšem
stále	stále	k6eAd1	stále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
činnosti	činnost	k1gFnSc6	činnost
a	a	k8xC	a
víceméně	víceméně	k9	víceméně
odmítá	odmítat	k5eAaImIp3nS	odmítat
toto	tento	k3xDgNnSc4	tento
stanovisko	stanovisko	k1gNnSc4	stanovisko
akceptovat	akceptovat	k5eAaBmF	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
poválečných	poválečný	k2eAgNnPc2d1	poválečné
let	léto	k1gNnPc2	léto
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc3d2	veliký
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
integraci	integrace	k1gFnSc3	integrace
mezi	mezi	k7c7	mezi
Kanadou	Kanada	k1gFnSc7	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
kanadsko-americkou	kanadskomerický	k2eAgFnSc4d1	kanadsko-americká
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
(	(	kIx(	(
<g/>
Canada-United	Canada-United	k1gInSc1	Canada-United
States	Statesa	k1gFnPc2	Statesa
Free	Fre	k1gInSc2	Fre
Trade	Trad	k1gInSc5	Trad
Agreement	Agreement	k1gMnSc1	Agreement
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
kanadsko-americkou	kanadskomerický	k2eAgFnSc4d1	kanadsko-americká
zónu	zóna	k1gFnSc4	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
americkým	americký	k2eAgInSc7d1	americký
vlivem	vliv	k1gInSc7	vliv
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
kanadská	kanadský	k2eAgNnPc4d1	kanadské
média	médium	k1gNnPc4	médium
(	(	kIx(	(
<g/>
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
příhraničí	příhraničí	k1gNnSc6	příhraničí
a	a	k8xC	a
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
obecně	obecně	k6eAd1	obecně
skomírající	skomírající	k2eAgFnSc2d1	skomírající
pod	pod	k7c7	pod
konkurencí	konkurence	k1gFnSc7	konkurence
médiích	médium	k1gNnPc6	médium
amerických	americký	k2eAgNnPc6d1	americké
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
ke	k	k7c3	k
snahám	snaha	k1gFnPc3	snaha
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
posílit	posílit	k5eAaPmF	posílit
integraci	integrace	k1gFnSc4	integrace
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
signifikantní	signifikantní	k2eAgFnSc1d1	signifikantní
skupina	skupina	k1gFnSc1	skupina
usilující	usilující	k2eAgFnSc1d1	usilující
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
tzv.	tzv.	kA	tzv.
Severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
administrativním	administrativní	k2eAgNnSc6d1	administrativní
členění	členění	k1gNnSc6	členění
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
Severozápadních	severozápadní	k2eAgNnPc2d1	severozápadní
teritorií	teritorium	k1gNnPc2	teritorium
vyčleněno	vyčleněn	k2eAgNnSc4d1	vyčleněno
teritorium	teritorium	k1gNnSc4	teritorium
Nunavut	Nunavut	k1gInSc4	Nunavut
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Kanadské	kanadský	k2eAgNnSc1d1	kanadské
území	území	k1gNnSc1	území
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
patří	patřit	k5eAaImIp3nS	patřit
řada	řada	k1gFnSc1	řada
arktických	arktický	k2eAgInPc2d1	arktický
ostrovů	ostrov	k1gInPc2	ostrov
zasahující	zasahující	k2eAgFnSc4d1	zasahující
až	až	k9	až
k	k	k7c3	k
Severnímu	severní	k2eAgInSc3d1	severní
ledovému	ledový	k2eAgInSc3d1	ledový
oceánu	oceán	k1gInSc3	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
ostrovy	ostrov	k1gInPc4	ostrov
úzkými	úzké	k1gInPc7	úzké
průlivy	průliv	k1gInPc4	průliv
odděleny	oddělen	k2eAgInPc4d1	oddělen
od	od	k7c2	od
dánského	dánský	k2eAgNnSc2d1	dánské
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
reklamuje	reklamovat	k5eAaImIp3nS	reklamovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
své	svůj	k3xOyFgNnSc1	svůj
právo	právo	k1gNnSc1	právo
na	na	k7c4	na
část	část	k1gFnSc4	část
Arktidy	Arktida	k1gFnSc2	Arktida
mezi	mezi	k7c7	mezi
60	[number]	k4	60
a	a	k8xC	a
141	[number]	k4	141
<g/>
°	°	k?	°
západní	západní	k2eAgFnSc2d1	západní
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
nároky	nárok	k1gInPc1	nárok
však	však	k9	však
neuznávají	uznávat	k5eNaImIp3nP	uznávat
všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
pobřeží	pobřeží	k1gNnSc1	pobřeží
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
USA	USA	kA	USA
(	(	kIx(	(
<g/>
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
východu	východ	k1gInSc2	východ
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
státy	stát	k1gInPc4	stát
Maine	Main	k1gMnSc5	Main
<g/>
,	,	kIx,	,
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
,	,	kIx,	,
Vermont	Vermont	k1gMnSc1	Vermont
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
,	,	kIx,	,
Wisconsin	Wisconsin	k1gInSc1	Wisconsin
<g/>
,	,	kIx,	,
Minnesota	Minnesota	k1gFnSc1	Minnesota
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
Montana	Montana	k1gFnSc1	Montana
<g/>
,	,	kIx,	,
Idaho	Ida	k1gMnSc2	Ida
a	a	k8xC	a
Washington	Washington	k1gInSc1	Washington
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
pacifické	pacifický	k2eAgNnSc1d1	pacifické
pobřeží	pobřeží	k1gNnSc1	pobřeží
a	a	k8xC	a
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
americkým	americký	k2eAgInSc7d1	americký
státem	stát	k1gInSc7	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
po	po	k7c4	po
Minnesotu	Minnesota	k1gFnSc4	Minnesota
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
USA	USA	kA	USA
systém	systém	k1gInSc1	systém
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
související	související	k2eAgFnPc4d1	související
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
kanadského	kanadský	k2eAgNnSc2d1	kanadské
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
3,26	[number]	k4	3,26
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozmístění	rozmístění	k1gNnSc1	rozmístění
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c4	v
160	[number]	k4	160
km	km	kA	km
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
pásu	pás	k1gInSc6	pás
okolo	okolo	k7c2	okolo
amerických	americký	k2eAgFnPc2d1	americká
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
v	v	k7c6	v
koridoru	koridor	k1gInSc6	koridor
Québec-Winsdor	Québec-Winsdor	k1gInSc1	Québec-Winsdor
<g/>
.	.	kIx.	.
</s>
<s>
Nejsevernějším	severní	k2eAgMnSc7d3	nejsevernější
trvale	trvale	k6eAd1	trvale
osídleným	osídlený	k2eAgNnSc7d1	osídlené
místem	místo	k1gNnSc7	místo
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
a	a	k8xC	a
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
CFS	CFS	kA	CFS
Alert	Alert	k1gInSc1	Alert
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc1	stanice
kanadských	kanadský	k2eAgFnPc2d1	kanadská
sil	síla	k1gFnPc2	síla
Alert	Alerta	k1gFnPc2	Alerta
<g/>
)	)	kIx)	)
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
výběžku	výběžek	k1gInSc6	výběžek
Ellesmerova	Ellesmerův	k2eAgInSc2d1	Ellesmerův
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
450	[number]	k4	450
námořních	námořní	k2eAgFnPc2d1	námořní
mil	míle	k1gFnPc2	míle
od	od	k7c2	od
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Středo-severní	Středoeverní	k2eAgFnSc1d1	Středo-severní
část	část	k1gFnSc1	část
Kanady	Kanada	k1gFnSc2	Kanada
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
Kanadský	kanadský	k2eAgInSc1d1	kanadský
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
Hudsonův	Hudsonův	k2eAgInSc1d1	Hudsonův
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
skal	skála	k1gFnPc2	skála
odřených	odřený	k2eAgFnPc2d1	odřená
předchozí	předchozí	k2eAgFnSc7d1	předchozí
činností	činnost	k1gFnSc7	činnost
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
jen	jen	k9	jen
slabá	slabý	k2eAgFnSc1d1	slabá
vrstva	vrstva	k1gFnSc1	vrstva
půdy	půda	k1gFnSc2	půda
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
Kanada	Kanada	k1gFnSc1	Kanada
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
jezer	jezero	k1gNnPc2	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
jezerech	jezero	k1gNnPc6	jezero
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
též	též	k9	též
největší	veliký	k2eAgFnSc1d3	veliký
světová	světový	k2eAgFnSc1d1	světová
zásoba	zásoba	k1gFnSc1	zásoba
sladké	sladký	k2eAgFnSc2d1	sladká
povrchové	povrchový	k2eAgFnSc2d1	povrchová
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Kanady	Kanada	k1gFnSc2	Kanada
ústí	ústit	k5eAaImIp3nS	ústit
řeka	řek	k1gMnSc4	řek
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc4	Vavřinec
do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
největším	veliký	k2eAgNnSc7d3	veliký
nálevkovitým	nálevkovitý	k2eAgNnSc7d1	nálevkovité
ústím	ústí	k1gNnSc7	ústí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
za	za	k7c4	za
ně	on	k3xPp3gFnPc4	on
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
celý	celý	k2eAgInSc4d1	celý
záliv	záliv	k1gInSc4	záliv
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
zálivu	záliv	k1gInSc2	záliv
leží	ležet	k5eAaImIp3nS	ležet
ostrov	ostrov	k1gInSc4	ostrov
Newfoundland	Newfoundlanda	k1gFnPc2	Newfoundlanda
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
poloostrova	poloostrov	k1gInSc2	poloostrov
Gaspé	Gaspý	k2eAgMnPc4d1	Gaspý
leží	ležet	k5eAaImIp3nP	ležet
tzv.	tzv.	kA	tzv.
Přímořské	přímořský	k2eAgFnPc4d1	přímořská
nebo	nebo	k8xC	nebo
Pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
provincie	provincie	k1gFnPc4	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
teoreticky	teoreticky	k6eAd1	teoreticky
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
provincii	provincie	k1gFnSc4	provincie
vyjma	vyjma	k7c2	vyjma
Alberty	Alberta	k1gFnSc2	Alberta
a	a	k8xC	a
Saskatchewanu	Saskatchewan	k1gInSc2	Saskatchewan
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
zpravidla	zpravidla	k6eAd1	zpravidla
pouze	pouze	k6eAd1	pouze
tyto	tento	k3xDgFnPc4	tento
tři	tři	k4xCgFnPc4	tři
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgNnSc4	žádný
hluboké	hluboký	k2eAgNnSc4d1	hluboké
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Brunšvik	Brunšvik	k1gInSc1	Brunšvik
a	a	k8xC	a
Nové	Nové	k2eAgNnSc1d1	Nové
Skotsko	Skotsko	k1gNnSc1	Skotsko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
zálivem	záliv	k1gInSc7	záliv
Fundy	fund	k1gInPc4	fund
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozpětím	rozpětí	k1gNnSc7	rozpětí
mezi	mezi	k7c7	mezi
přílivem	příliv	k1gInSc7	příliv
a	a	k8xC	a
odlivem	odliv	k1gInSc7	odliv
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
Kanady	Kanada	k1gFnSc2	Kanada
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
klimatického	klimatický	k2eAgNnSc2d1	klimatické
i	i	k8xC	i
tranzitního	tranzitní	k2eAgNnSc2d1	tranzitní
hlediska	hledisko	k1gNnSc2	hledisko
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
pevniny	pevnina	k1gFnSc2	pevnina
se	s	k7c7	s
vlamujícím	vlamující	k2eAgInSc7d1	vlamující
Hudsonovým	Hudsonův	k2eAgInSc7d1	Hudsonův
zálivem	záliv	k1gInSc7	záliv
a	a	k8xC	a
Jamesovou	Jamesový	k2eAgFnSc7d1	Jamesová
zátokou	zátoka	k1gFnSc7	zátoka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
systémem	systém	k1gInSc7	systém
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jednak	jednak	k8xC	jednak
prérie	prérie	k1gFnSc1	prérie
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
tzv.	tzv.	kA	tzv.
Prérijními	prérijní	k2eAgFnPc7d1	prérijní
provinciemi	provincie	k1gFnPc7	provincie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Skalisté	skalistý	k2eAgFnPc1d1	skalistý
hory	hora	k1gFnPc1	hora
odtínají	odtínat	k5eAaImIp3nP	odtínat
od	od	k7c2	od
jediné	jediný	k2eAgFnSc2d1	jediná
"	"	kIx"	"
<g/>
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
<g/>
"	"	kIx"	"
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vegetačního	vegetační	k2eAgNnSc2d1	vegetační
a	a	k8xC	a
klimatického	klimatický	k2eAgNnSc2d1	klimatické
hlediska	hledisko	k1gNnSc2	hledisko
přechází	přecházet	k5eAaImIp3nS	přecházet
arktické	arktický	k2eAgNnSc4d1	arktické
území	území	k1gNnSc4	území
ledových	ledový	k2eAgFnPc2d1	ledová
pouští	poušť	k1gFnPc2	poušť
přes	přes	k7c4	přes
tundru	tundra	k1gFnSc4	tundra
a	a	k8xC	a
severský	severský	k2eAgInSc4d1	severský
jehličnatý	jehličnatý	k2eAgInSc4d1	jehličnatý
les	les	k1gInSc4	les
až	až	k9	až
k	k	k7c3	k
prériím	prérie	k1gFnPc3	prérie
a	a	k8xC	a
smíšeným	smíšený	k2eAgInPc3d1	smíšený
a	a	k8xC	a
listnatým	listnatý	k2eAgInPc3d1	listnatý
lesům	les	k1gInPc3	les
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velká	k1gFnSc1	velká
jezera	jezero	k1gNnSc2	jezero
pak	pak	k6eAd1	pak
stabilizují	stabilizovat	k5eAaBmIp3nP	stabilizovat
přilehlá	přilehlý	k2eAgNnPc1d1	přilehlé
území	území	k1gNnPc1	území
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
mrzne	mrznout	k5eAaImIp3nS	mrznout
jen	jen	k9	jen
málo	málo	k6eAd1	málo
a	a	k8xC	a
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
příznivým	příznivý	k2eAgFnPc3d1	příznivá
mikroklimatickým	mikroklimatický	k2eAgFnPc3d1	mikroklimatická
podmínkám	podmínka	k1gFnPc3	podmínka
dají	dát	k5eAaPmIp3nP	dát
pěstovat	pěstovat	k5eAaImF	pěstovat
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
subtropické	subtropický	k2eAgFnPc4d1	subtropická
až	až	k8xS	až
tropické	tropický	k2eAgFnPc4d1	tropická
rostliny	rostlina	k1gFnPc4	rostlina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
oblast	oblast	k1gFnSc1	oblast
Niagarského	niagarský	k2eAgInSc2d1	niagarský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
hlavou	hlava	k1gFnSc7	hlava
je	být	k5eAaImIp3nS	být
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Královniným	královnin	k2eAgMnSc7d1	královnin
zástupcem	zástupce	k1gMnSc7	zástupce
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
je	být	k5eAaImIp3nS	být
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
též	též	k9	též
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
demokracie	demokracie	k1gFnSc1	demokracie
s	s	k7c7	s
federálním	federální	k2eAgNnSc7d1	federální
uspořádáním	uspořádání	k1gNnSc7	uspořádání
a	a	k8xC	a
silnými	silný	k2eAgFnPc7d1	silná
demokratickými	demokratický	k2eAgFnPc7d1	demokratická
tradicemi	tradice	k1gFnPc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
ústava	ústava	k1gFnSc1	ústava
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
právní	právní	k2eAgInSc4d1	právní
rámec	rámec	k1gInSc4	rámec
existence	existence	k1gFnSc2	existence
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
psaného	psaný	k2eAgInSc2d1	psaný
obsahu	obsah	k1gInSc2	obsah
a	a	k8xC	a
nepsaných	nepsaný	k2eAgFnPc2d1	nepsaná
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
konvencí	konvence	k1gFnPc2	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
základ	základ	k1gInSc1	základ
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
Zákoně	zákon	k1gInSc6	zákon
o	o	k7c6	o
britské	britský	k2eAgFnSc6d1	britská
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
British	British	k1gMnSc1	British
North	North	k1gMnSc1	North
America	America	k1gMnSc1	America
Act	Act	k1gMnSc1	Act
1867	[number]	k4	1867
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
Constitution	Constitution	k1gInSc1	Constitution
Act	Act	k1gFnSc2	Act
1867	[number]	k4	1867
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kanada	Kanada	k1gFnSc1	Kanada
má	mít	k5eAaImIp3nS	mít
ústavu	ústava	k1gFnSc4	ústava
podobnou	podobný	k2eAgFnSc4d1	podobná
principům	princip	k1gInPc3	princip
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
moc	moc	k6eAd1	moc
mezi	mezi	k7c4	mezi
federální	federální	k2eAgFnPc4d1	federální
a	a	k8xC	a
provinční	provinční	k2eAgFnPc4d1	provinční
vlády	vláda	k1gFnPc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tzv.	tzv.	kA	tzv.
Kanadskou	kanadský	k2eAgFnSc4d1	kanadská
chartu	charta	k1gFnSc4	charta
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Canadian	Canadian	k1gMnSc1	Canadian
Charter	Charter	k1gMnSc1	Charter
of	of	k?	of
Rights	Rights	k1gInSc1	Rights
and	and	k?	and
Freedoms	Freedoms	k1gInSc1	Freedoms
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
zaručující	zaručující	k2eAgInSc1d1	zaručující
základní	základní	k2eAgInSc1d1	základní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
obecném	obecný	k2eAgNnSc6d1	obecné
měřítku	měřítko	k1gNnSc6	měřítko
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
potlačeny	potlačit	k5eAaPmNgInP	potlačit
žádnými	žádný	k3yNgInPc7	žádný
legislativními	legislativní	k2eAgInPc7d1	legislativní
nebo	nebo	k8xC	nebo
procedurálními	procedurální	k2eAgInPc7d1	procedurální
kroky	krok	k1gInPc7	krok
na	na	k7c6	na
žádné	žádný	k3yNgFnSc6	žádný
z	z	k7c2	z
úrovní	úroveň	k1gFnPc2	úroveň
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Federálnímu	federální	k2eAgInSc3d1	federální
parlamentu	parlament	k1gInSc3	parlament
a	a	k8xC	a
provinčním	provinční	k2eAgFnPc3d1	provinční
správám	správa	k1gFnPc3	správa
nicméně	nicméně	k8xC	nicméně
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
některé	některý	k3yIgFnSc2	některý
její	její	k3xOp3gFnSc2	její
další	další	k2eAgFnSc2d1	další
části	část	k1gFnSc2	část
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnPc4	funkce
premiéra	premiér	k1gMnSc2	premiér
náleží	náležet	k5eAaImIp3nP	náležet
vůdci	vůdce	k1gMnPc1	vůdce
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
získá	získat	k5eAaPmIp3nS	získat
důvěru	důvěra	k1gFnSc4	důvěra
většiny	většina	k1gFnSc2	většina
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
kanadského	kanadský	k2eAgInSc2d1	kanadský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
zastupující	zastupující	k2eAgFnSc4d1	zastupující
královnu	královna	k1gFnSc4	královna
formálně	formálně	k6eAd1	formálně
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
vládu	vláda	k1gFnSc4	vláda
i	i	k8xC	i
premiéra	premiér	k1gMnSc4	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
konvencí	konvence	k1gFnPc2	konvence
však	však	k9	však
volbu	volba	k1gFnSc4	volba
premiéra	premiér	k1gMnSc2	premiér
respektuje	respektovat	k5eAaImIp3nS	respektovat
<g/>
.	.	kIx.	.
</s>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
vláda	vláda	k1gFnSc1	vláda
tradičně	tradičně	k6eAd1	tradičně
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
strany	strana	k1gFnSc2	strana
prvního	první	k4xOgMnSc2	první
ministra	ministr	k1gMnSc2	ministr
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
křeslo	křeslo	k1gNnSc4	křeslo
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
vázáni	vázat	k5eAaImNgMnP	vázat
přísahou	přísaha	k1gFnSc7	přísaha
ke	k	k7c3	k
Královnině	královnin	k2eAgFnSc3d1	Královnina
tajné	tajný	k2eAgFnSc3d1	tajná
radě	rada	k1gFnSc3	rada
pro	pro	k7c4	pro
Kanadu	Kanada	k1gFnSc4	Kanada
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
ministry	ministr	k1gMnPc7	ministr
britské	britský	k2eAgFnSc2d1	britská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
má	mít	k5eAaImIp3nS	mít
širokou	široký	k2eAgFnSc4d1	široká
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
především	především	k9	především
při	při	k7c6	při
jmenování	jmenování	k1gNnSc6	jmenování
dalších	další	k2eAgFnPc2d1	další
vysokých	vysoká	k1gFnPc2	vysoká
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Jeho	jeho	k3xOp3gFnSc2	jeho
excelence	excelence	k1gFnSc2	excelence
David	David	k1gMnSc1	David
Lloyd	Lloyd	k1gMnSc1	Lloyd
Johnston	Johnston	k1gInSc4	Johnston
<g/>
,	,	kIx,	,
současným	současný	k2eAgMnSc7d1	současný
premiérem	premiér	k1gMnSc7	premiér
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vůdce	vůdce	k1gMnSc1	vůdce
Kanadské	kanadský	k2eAgFnSc2d1	kanadská
Liberální	liberální	k2eAgFnSc2d1	liberální
strany	strana	k1gFnSc2	strana
Justin	Justin	k1gMnSc1	Justin
Trudeau	Trudeaa	k1gMnSc4	Trudeaa
<g/>
.	.	kIx.	.
</s>
<s>
Kanadský	kanadský	k2eAgInSc1d1	kanadský
parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
z	z	k7c2	z
volené	volený	k2eAgFnSc2d1	volená
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
sněmovny	sněmovna	k1gFnSc2	sněmovna
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
a	a	k8xC	a
jmenovaného	jmenovaný	k2eAgInSc2d1	jmenovaný
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
ze	z	k7c2	z
členů	člen	k1gInPc2	člen
sněmovny	sněmovna	k1gFnSc2	sněmovna
lidu	lid	k1gInSc2	lid
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
většinovým	většinový	k2eAgInSc7d1	většinový
volebním	volební	k2eAgInSc7d1	volební
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
volby	volba	k1gFnSc2	volba
vypisuje	vypisovat	k5eAaImIp3nS	vypisovat
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
volby	volba	k1gFnPc1	volba
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vypsány	vypsat	k5eAaPmNgInP	vypsat
do	do	k7c2	do
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
od	od	k7c2	od
posledních	poslední	k2eAgFnPc2d1	poslední
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
křesla	křeslo	k1gNnPc1	křeslo
jsou	být	k5eAaImIp3nP	být
přidělována	přidělován	k2eAgNnPc1d1	přidělováno
na	na	k7c6	na
regionálním	regionální	k2eAgInSc6d1	regionální
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vybírání	vybírání	k1gNnPc2	vybírání
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
pak	pak	k6eAd1	pak
zastávají	zastávat	k5eAaImIp3nP	zastávat
až	až	k9	až
do	do	k7c2	do
věku	věk	k1gInSc2	věk
75	[number]	k4	75
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřmi	čtyři	k4xCgFnPc7	čtyři
hlavními	hlavní	k2eAgFnPc7d1	hlavní
kanadskými	kanadský	k2eAgFnPc7d1	kanadská
politickými	politický	k2eAgFnPc7d1	politická
stranami	strana	k1gFnPc7	strana
jsou	být	k5eAaImIp3nP	být
Conservative	Conservativ	k1gInSc5	Conservativ
Party	parta	k1gFnPc1	parta
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
(	(	kIx(	(
<g/>
Konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liberal	Liberal	k1gMnSc1	Liberal
Party	parta	k1gFnSc2	parta
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
(	(	kIx(	(
<g/>
Liberální	liberální	k2eAgFnSc1d1	liberální
strana	strana	k1gFnSc1	strana
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Democratic	Democratice	k1gFnPc2	Democratice
Party	party	k1gFnPc2	party
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
NDP	NDP	kA	NDP
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Bloc	Bloc	k1gFnSc1	Bloc
Québécois	Québécois	k1gFnSc1	Québécois
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Quebecký	quebecký	k2eAgInSc1d1	quebecký
blok	blok	k1gInSc1	blok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
konzervativní	konzervativní	k2eAgFnSc7d1	konzervativní
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
vládne	vládnout	k5eAaImIp3nS	vládnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
strany	strana	k1gFnPc1	strana
jako	jako	k8xC	jako
Green	Green	k1gInSc1	Green
Party	parta	k1gFnSc2	parta
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
(	(	kIx(	(
<g/>
Strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
strany	strana	k1gFnPc1	strana
zastoupení	zastoupení	k1gNnSc2	zastoupení
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Izraelsko-kanadské	izraelskoanadský	k2eAgInPc4d1	izraelsko-kanadský
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
má	mít	k5eAaImIp3nS	mít
úzké	úzký	k2eAgInPc4d1	úzký
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
má	mít	k5eAaImIp3nS	mít
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
nebráněnou	bráněný	k2eNgFnSc4d1	nebráněná
hranici	hranice	k1gFnSc4	hranice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
společnou	společný	k2eAgFnSc4d1	společná
leteckou	letecký	k2eAgFnSc4d1	letecká
obranu	obrana	k1gFnSc4	obrana
(	(	kIx(	(
<g/>
NORAD	NORAD	kA	NORAD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
spolu	spolu	k6eAd1	spolu
často	často	k6eAd1	často
kooperují	kooperovat	k5eAaImIp3nP	kooperovat
jak	jak	k8xS	jak
na	na	k7c4	na
politické	politický	k2eAgNnSc4d1	politické
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
vojenské	vojenský	k2eAgFnSc6d1	vojenská
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
ekonomiky	ekonomika	k1gFnSc2	ekonomika
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
úzce	úzko	k6eAd1	úzko
provázané	provázaný	k2eAgNnSc1d1	provázané
(	(	kIx(	(
<g/>
vizte	vidět	k5eAaImRp2nP	vidět
také	také	k9	také
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
unie	unie	k1gFnSc1	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
má	mít	k5eAaImIp3nS	mít
Kanada	Kanada	k1gFnSc1	Kanada
nadstandardní	nadstandardní	k2eAgInPc4d1	nadstandardní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgFnPc7	dva
bývalými	bývalý	k2eAgFnPc7d1	bývalá
imperiálními	imperiální	k2eAgFnPc7d1	imperiální
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
hrály	hrát	k5eAaImAgFnP	hrát
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
roli	role	k1gFnSc4	role
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
vzniku	vznik	k1gInSc6	vznik
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
a	a	k8xC	a
La	la	k1gNnSc2	la
Francophonie	Francophonie	k1gFnSc2	Francophonie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
společnou	společný	k2eAgFnSc4d1	společná
formální	formální	k2eAgFnSc4d1	formální
hlavu	hlava	k1gFnSc4	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
Kanada	Kanada	k1gFnSc1	Kanada
advokátem	advokát	k1gMnSc7	advokát
multilateralismu	multilateralismus	k1gInSc2	multilateralismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
úsilí	úsilí	k1gNnSc4	úsilí
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
řešení	řešení	k1gNnSc2	řešení
globálních	globální	k2eAgInPc2d1	globální
problémů	problém	k1gInPc2	problém
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
národy	národ	k1gInPc7	národ
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
výrazných	výrazný	k2eAgFnPc2d1	výrazná
demonstrací	demonstrace	k1gFnPc2	demonstrace
tohoto	tento	k3xDgNnSc2	tento
stanoviska	stanovisko	k1gNnSc2	stanovisko
je	být	k5eAaImIp3nS	být
vystupování	vystupování	k1gNnSc4	vystupování
L.	L.	kA	L.
B.	B.	kA	B.
Pearsona	Pearsona	k1gFnSc1	Pearsona
za	za	k7c2	za
Suezské	suezský	k2eAgFnSc2d1	Suezská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
vyslání	vyslání	k1gNnSc4	vyslání
mírových	mírový	k2eAgFnPc2d1	mírová
sil	síla	k1gFnPc2	síla
OSN	OSN	kA	OSN
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mírových	mírový	k2eAgFnPc2d1	mírová
misí	mise	k1gFnPc2	mise
OSN	OSN	kA	OSN
vůdčí	vůdčí	k2eAgFnSc4d1	vůdčí
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesáti	padesát	k4xCc7	padesát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
však	však	k9	však
její	její	k3xOp3gInSc1	její
příspěvek	příspěvek	k1gInSc1	příspěvek
krátí	krátit	k5eAaImIp3nS	krátit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2009	[number]	k4	2009
až	až	k8xS	až
2012	[number]	k4	2012
měla	mít	k5eAaImAgFnS	mít
Kanada	Kanada	k1gFnSc1	Kanada
jednostrannou	jednostranný	k2eAgFnSc4d1	jednostranná
vízovou	vízový	k2eAgFnSc4d1	vízová
povinnost	povinnost	k1gFnSc4	povinnost
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
ke	k	k7c3	k
14.11	[number]	k4	14.11
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
vízovou	vízový	k2eAgFnSc4d1	vízová
povinnost	povinnost	k1gFnSc4	povinnost
taktéž	taktéž	k?	taktéž
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
disponuje	disponovat	k5eAaBmIp3nS	disponovat
62	[number]	k4	62
000	[number]	k4	000
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
26	[number]	k4	26
000	[number]	k4	000
rezervisty	rezervista	k1gMnPc7	rezervista
<g/>
.	.	kIx.	.
</s>
<s>
Kanadské	kanadský	k2eAgFnPc1d1	kanadská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
sestávají	sestávat	k5eAaImIp3nP	sestávat
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Disponují	disponovat	k5eAaBmIp3nP	disponovat
1400	[number]	k4	1400
obrněnými	obrněný	k2eAgNnPc7d1	obrněné
vozidly	vozidlo	k1gNnPc7	vozidlo
<g/>
,	,	kIx,	,
34	[number]	k4	34
válečnými	válečný	k2eAgNnPc7d1	válečné
plavidly	plavidlo	k1gNnPc7	plavidlo
a	a	k8xC	a
861	[number]	k4	861
letadly	letadlo	k1gNnPc7	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
Kanada	Kanada	k1gFnSc1	Kanada
kromě	kromě	k7c2	kromě
mírových	mírový	k2eAgFnPc2d1	mírová
misí	mise	k1gFnPc2	mise
OSN	OSN	kA	OSN
přímo	přímo	k6eAd1	přímo
účastnila	účastnit	k5eAaImAgFnS	účastnit
i	i	k9	i
válečných	válečný	k2eAgInPc2d1	válečný
konfliktů	konflikt	k1gInPc2	konflikt
jakožto	jakožto	k8xS	jakožto
válčící	válčící	k2eAgFnSc1d1	válčící
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
druhé	druhý	k4xOgFnPc1	druhý
búrské	búrský	k2eAgFnPc1d1	búrská
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
světové	světový	k2eAgInPc1d1	světový
války	válek	k1gInPc1	válek
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
korejské	korejský	k2eAgFnPc1d1	Korejská
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
války	válka	k1gFnPc1	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
a	a	k8xC	a
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
započítali	započítat	k5eAaPmAgMnP	započítat
i	i	k9	i
prehistorii	prehistorie	k1gFnSc4	prehistorie
Kanady	Kanada	k1gFnSc2	Kanada
-	-	kIx~	-
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
připočíst	připočíst	k5eAaPmF	připočíst
ještě	ještě	k9	ještě
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
Kanaďanů	Kanaďan	k1gMnPc2	Kanaďan
v	v	k7c6	v
britsko-americké	britskomerický	k2eAgFnSc6d1	britsko-americká
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
oddíly	oddíl	k1gInPc1	oddíl
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
přírodními	přírodní	k2eAgFnPc7d1	přírodní
katastrofami	katastrofa	k1gFnPc7	katastrofa
(	(	kIx(	(
<g/>
Disaster	Disaster	k1gInSc1	Disaster
Assistance	Assistance	k1gFnSc2	Assistance
Response	response	k1gFnSc2	response
Team	team	k1gInSc1	team
<g/>
,	,	kIx,	,
DART	DART	kA	DART
<g/>
)	)	kIx)	)
zasahovaly	zasahovat	k5eAaImAgFnP	zasahovat
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
při	při	k7c6	při
třech	tři	k4xCgFnPc6	tři
mimořádně	mimořádně	k6eAd1	mimořádně
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
katastrofách	katastrofa	k1gFnPc6	katastrofa
<g/>
:	:	kIx,	:
Zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
Kašmíru	Kašmír	k1gInSc6	Kašmír
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
hurikánu	hurikán	k1gInSc2	hurikán
Katrina	Katrino	k1gNnSc2	Katrino
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kanadské	kanadský	k2eAgFnSc2d1	kanadská
provincie	provincie	k1gFnSc2	provincie
a	a	k8xC	a
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
deseti	deset	k4xCc2	deset
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
tří	tři	k4xCgNnPc2	tři
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnPc4	provincie
jsou	být	k5eAaImIp3nP	být
Alberta	Alberta	k1gFnSc1	Alberta
<g/>
,	,	kIx,	,
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Manitoba	Manitoba	k1gFnSc1	Manitoba
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Brunšvik	Brunšvik	k1gInSc1	Brunšvik
<g/>
,	,	kIx,	,
Newfoundland	Newfoundland	k1gInSc1	Newfoundland
a	a	k8xC	a
Labrador	Labrador	k1gInSc1	Labrador
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
prince	princ	k1gMnSc2	princ
Edwarda	Edward	k1gMnSc2	Edward
<g/>
,	,	kIx,	,
Québec	Québec	k1gInSc4	Québec
a	a	k8xC	a
Saskatchewan	Saskatchewan	k1gInSc4	Saskatchewan
<g/>
.	.	kIx.	.
</s>
<s>
Teritoria	teritorium	k1gNnPc1	teritorium
jsou	být	k5eAaImIp3nP	být
Severozápadní	severozápadní	k2eAgNnPc1d1	severozápadní
teritoria	teritorium	k1gNnPc1	teritorium
<g/>
,	,	kIx,	,
Nunavut	Nunavut	k1gInSc1	Nunavut
a	a	k8xC	a
Yukon	Yukon	k1gInSc1	Yukon
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnPc1	provincie
mají	mít	k5eAaImIp3nP	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
stupeň	stupeň	k1gInSc4	stupeň
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
teritoria	teritorium	k1gNnPc4	teritorium
poněkud	poněkud	k6eAd1	poněkud
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	obojí	k4xRgInPc1	obojí
disponují	disponovat	k5eAaBmIp3nP	disponovat
vlastními	vlastní	k2eAgInPc7d1	vlastní
provincionálními	provincionální	k2eAgInPc7d1	provincionální
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
teritoriálními	teritoriální	k2eAgInPc7d1	teritoriální
<g/>
)	)	kIx)	)
symboly	symbol	k1gInPc7	symbol
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
seznam	seznam	k1gInSc4	seznam
kanadských	kanadský	k2eAgInPc2d1	kanadský
provinciálních	provinciální	k2eAgInPc2d1	provinciální
a	a	k8xC	a
teritoriálních	teritoriální	k2eAgInPc2d1	teritoriální
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnPc1	provincie
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
většinu	většina	k1gFnSc4	většina
státních	státní	k2eAgInPc2d1	státní
sociálních	sociální	k2eAgInPc2d1	sociální
programů	program	k1gInPc2	program
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
péče	péče	k1gFnSc2	péče
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
systém	systém	k1gInSc1	systém
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
sociální	sociální	k2eAgFnPc4d1	sociální
dávky	dávka	k1gFnPc4	dávka
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
)	)	kIx)	)
a	a	k8xC	a
společně	společně	k6eAd1	společně
soustředí	soustředit	k5eAaPmIp3nS	soustředit
více	hodně	k6eAd2	hodně
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
než	než	k8xS	než
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
finanční	finanční	k2eAgFnSc2d1	finanční
a	a	k8xC	a
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
autonomie	autonomie	k1gFnSc2	autonomie
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velice	velice	k6eAd1	velice
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
může	moct	k5eAaImIp3nS	moct
iniciovat	iniciovat	k5eAaBmF	iniciovat
národní	národní	k2eAgInPc4d1	národní
programy	program	k1gInPc4	program
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
provinciích	provincie	k1gFnPc6	provincie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Canada	Canada	k1gFnSc1	Canada
Health	Health	k1gMnSc1	Health
Act	Act	k1gMnSc1	Act
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
provincie	provincie	k1gFnPc1	provincie
mohou	moct	k5eAaImIp3nP	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nebudou	být	k5eNaImBp3nP	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
politice	politika	k1gFnSc6	politika
podílet	podílet	k5eAaImF	podílet
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
neděje	dít	k5eNaImIp3nS	dít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
vyrovnanou	vyrovnaný	k2eAgFnSc4d1	vyrovnaná
úroveň	úroveň	k1gFnSc4	úroveň
služeb	služba	k1gFnPc2	služba
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
provinciích	provincie	k1gFnPc6	provincie
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
tzv.	tzv.	kA	tzv.
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
platby	platba	k1gFnSc2	platba
řízené	řízený	k2eAgNnSc1d1	řízené
Federální	federální	k2eAgNnSc1d1	federální
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přesouvají	přesouvat	k5eAaImIp3nP	přesouvat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
od	od	k7c2	od
bohatších	bohatý	k2eAgFnPc2d2	bohatší
provincií	provincie	k1gFnPc2	provincie
do	do	k7c2	do
rozpočtů	rozpočet	k1gInPc2	rozpočet
provincií	provincie	k1gFnPc2	provincie
chudších	chudý	k2eAgFnPc2d2	chudší
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
provincie	provincie	k1gFnPc1	provincie
i	i	k8xC	i
teritoria	teritorium	k1gNnPc1	teritorium
mají	mít	k5eAaImIp3nP	mít
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
volený	volený	k2eAgInSc4d1	volený
zákonodárný	zákonodárný	k2eAgInSc4d1	zákonodárný
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
premiér	premiér	k1gMnSc1	premiér
<g/>
,	,	kIx,	,
vybíraný	vybíraný	k2eAgMnSc1d1	vybíraný
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
ministr	ministr	k1gMnSc1	ministr
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
provincie	provincie	k1gFnSc1	provincie
viceguvernéra	viceguvernér	k1gMnSc2	viceguvernér
(	(	kIx(	(
<g/>
Lieutenant-Governor	Lieutenant-Governor	k1gMnSc1	Lieutenant-Governor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
Korunu	koruna	k1gFnSc4	koruna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
analogií	analogie	k1gFnSc7	analogie
generálního	generální	k2eAgMnSc4d1	generální
guvernéra	guvernér	k1gMnSc4	guvernér
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
teritorií	teritorium	k1gNnPc2	teritorium
je	být	k5eAaImIp3nS	být
obdobou	obdoba	k1gFnSc7	obdoba
viceguvernéra	viceguvernér	k1gMnSc4	viceguvernér
korunní	korunní	k2eAgMnSc1d1	korunní
komisař	komisař	k1gMnSc1	komisař
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
příjmy	příjem	k1gInPc7	příjem
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
člena	člen	k1gMnSc4	člen
OECD	OECD	kA	OECD
a	a	k8xC	a
skupiny	skupina	k1gFnSc2	skupina
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
tržní	tržní	k2eAgFnSc1d1	tržní
a	a	k8xC	a
ač	ač	k8xS	ač
více	hodně	k6eAd2	hodně
regulovaná	regulovaný	k2eAgFnSc1d1	regulovaná
než	než	k8xS	než
v	v	k7c6	v
sousedních	sousední	k2eAgInPc6d1	sousední
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
regulovaná	regulovaný	k2eAgFnSc1d1	regulovaná
než	než	k8xS	než
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgInSc1d2	nižší
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
než	než	k8xS	než
její	její	k3xOp3gMnSc1	její
jižní	jižní	k2eAgMnSc1d1	jižní
soused	soused	k1gMnSc1	soused
(	(	kIx(	(
<g/>
bohatství	bohatství	k1gNnSc1	bohatství
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
rovnoměrněji	rovnoměrně	k6eAd2	rovnoměrně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
však	však	k9	však
vyšší	vysoký	k2eAgFnSc2d2	vyšší
než	než	k8xS	než
velké	velký	k2eAgFnSc2d1	velká
rozvinuté	rozvinutý	k2eAgFnSc2d1	rozvinutá
západoevropské	západoevropský	k2eAgFnSc2d1	západoevropská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
desetiletí	desetiletí	k1gNnSc2	desetiletí
její	její	k3xOp3gFnSc1	její
ekonomika	ekonomika	k1gFnSc1	ekonomika
celkově	celkově	k6eAd1	celkově
rychle	rychle	k6eAd1	rychle
rostla	růst	k5eAaImAgFnS	růst
a	a	k8xC	a
země	země	k1gFnSc1	země
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
nízkou	nízký	k2eAgFnSc4d1	nízká
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
6,3	[number]	k4	6,3
%	%	kIx~	%
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
výsledek	výsledek	k1gInSc1	výsledek
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hodnot	hodnota	k1gFnPc2	hodnota
naopak	naopak	k6eAd1	naopak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
na	na	k7c6	na
Newfoundlandu	Newfoundland	k1gInSc6	Newfoundland
a	a	k8xC	a
Labradoru	Labrador	k1gInSc6	Labrador
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
okolo	okolo	k7c2	okolo
14,5	[number]	k4	14,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulém	minulý	k2eAgNnSc6d1	Minulé
století	století	k1gNnSc6	století
růst	růst	k5eAaImF	růst
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
těžby	těžba	k1gFnSc2	těžba
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
přeměnil	přeměnit	k5eAaPmAgMnS	přeměnit
ekonomiku	ekonomika	k1gFnSc4	ekonomika
ze	z	k7c2	z
zemědělsky	zemědělsky	k6eAd1	zemědělsky
zaměřeného	zaměřený	k2eAgInSc2d1	zaměřený
státu	stát	k1gInSc2	stát
na	na	k7c4	na
především	především	k6eAd1	především
urbanizovaný	urbanizovaný	k2eAgInSc4d1	urbanizovaný
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
rozvinutého	rozvinutý	k2eAgInSc2d1	rozvinutý
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kanadské	kanadský	k2eAgFnSc6d1	kanadská
ekonomice	ekonomika	k1gFnSc6	ekonomika
dominuje	dominovat	k5eAaImIp3nS	dominovat
sektor	sektor	k1gInSc1	sektor
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
Kanaďanů	Kanaďan	k1gMnPc2	Kanaďan
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
rozvinutými	rozvinutý	k2eAgFnPc7d1	rozvinutá
zeměmi	zem	k1gFnPc7	zem
je	být	k5eAaImIp3nS	být
však	však	k9	však
Kanada	Kanada	k1gFnSc1	Kanada
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
v	v	k7c6	v
míře	míra	k1gFnSc6	míra
důležitosti	důležitost	k1gFnSc2	důležitost
primárního	primární	k2eAgInSc2d1	primární
sektoru	sektor	k1gInSc2	sektor
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
těžbou	těžba	k1gFnSc7	těžba
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nemnoha	nemnoha	k1gFnSc1	nemnoha
rozvinutých	rozvinutý	k2eAgInPc2d1	rozvinutý
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
energii	energie	k1gFnSc4	energie
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
velké	velký	k2eAgFnPc4d1	velká
zásoby	zásoba	k1gFnPc4	zásoba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
při	při	k7c6	při
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
velké	velký	k2eAgFnPc4d1	velká
zásoby	zásoba	k1gFnPc4	zásoba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
Albertě	Alberta	k1gFnSc6	Alberta
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
v	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
a	a	k8xC	a
v	v	k7c6	v
Saskatchewanu	Saskatchewan	k1gInSc6	Saskatchewan
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
ropné	ropný	k2eAgFnPc1d1	ropná
zásoby	zásoba	k1gFnPc1	zásoba
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
druhé	druhý	k4xOgFnPc4	druhý
největší	veliký	k2eAgFnPc4d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c7	za
Saudskou	saudský	k2eAgFnSc7d1	Saudská
Arábií	Arábie	k1gFnSc7	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Québecu	Québecus	k1gInSc6	Québecus
<g/>
,	,	kIx,	,
Britské	britský	k2eAgFnSc6d1	britská
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
Newfoundlandu	Newfoundland	k1gInSc6	Newfoundland
<g/>
,	,	kIx,	,
Labradoru	Labrador	k1gInSc6	Labrador
<g/>
,	,	kIx,	,
Ontariu	Ontario	k1gNnSc6	Ontario
a	a	k8xC	a
v	v	k7c6	v
Manitobě	Manitoba	k1gFnSc6	Manitoba
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
velmi	velmi	k6eAd1	velmi
levná	levný	k2eAgFnSc1d1	levná
energie	energie	k1gFnSc1	energie
z	z	k7c2	z
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgMnPc2d3	nejdůležitější
světových	světový	k2eAgMnPc2d1	světový
dodavatelů	dodavatel	k1gMnPc2	dodavatel
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Bývalé	bývalý	k2eAgFnPc1d1	bývalá
kanadské	kanadský	k2eAgFnPc1d1	kanadská
prérie	prérie	k1gFnPc1	prérie
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
nedůležitějších	důležitý	k2eNgFnPc2d2	nedůležitější
oblastí	oblast	k1gFnPc2	oblast
pěstování	pěstování	k1gNnSc2	pěstování
pšenice	pšenice	k1gFnSc2	pšenice
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
obilovin	obilovina	k1gFnPc2	obilovina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
vývozcem	vývozce	k1gMnSc7	vývozce
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
předním	přední	k2eAgMnSc7d1	přední
světovým	světový	k2eAgMnSc7d1	světový
vývozcem	vývozce	k1gMnSc7	vývozce
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
sídel	sídlo	k1gNnPc2	sídlo
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
díky	díky	k7c3	díky
těžařství	těžařství	k1gNnSc3	těžařství
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
minerálních	minerální	k2eAgFnPc2d1	minerální
surovin	surovina	k1gFnPc2	surovina
nebo	nebo	k8xC	nebo
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
značně	značně	k6eAd1	značně
velký	velký	k2eAgInSc1d1	velký
výrobní	výrobní	k2eAgInSc1d1	výrobní
sektor	sektor	k1gInSc1	sektor
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c6	v
Ontariu	Ontario	k1gNnSc6	Ontario
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
především	především	k6eAd1	především
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Québecu	Québecus	k1gInSc2	Québecus
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
mnoho	mnoho	k4c1	mnoho
firem	firma	k1gFnPc2	firma
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	se	k3xPyFc4	se
kosmonautikou	kosmonautika	k1gFnSc7	kosmonautika
a	a	k8xC	a
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
míře	míra	k1gFnSc6	míra
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
obchodu	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Bilaterální	bilaterální	k2eAgFnSc1d1	bilaterální
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Kanadou	Kanada	k1gFnSc7	Kanada
a	a	k8xC	a
USA	USA	kA	USA
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
(	(	kIx(	(
<g/>
FTA	FTA	kA	FTA
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
Severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
(	(	kIx(	(
<g/>
NAFTA	nafta	k1gFnSc1	nafta
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
i	i	k8xC	i
Mexiko	Mexiko	k1gNnSc1	Mexiko
spustila	spustit	k5eAaPmAgFnS	spustit
dramatický	dramatický	k2eAgInSc4d1	dramatický
růst	růst	k1gInSc4	růst
míry	míra	k1gFnSc2	míra
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
propojení	propojení	k1gNnSc2	propojení
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
země	zem	k1gFnSc2	zem
úspěšně	úspěšně	k6eAd1	úspěšně
překonala	překonat	k5eAaPmAgFnS	překonat
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
pokles	pokles	k1gInSc4	pokles
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
si	se	k3xPyFc3	se
tak	tak	k9	tak
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
celkový	celkový	k2eAgInSc4d1	celkový
výkon	výkon	k1gInSc4	výkon
ekonomiky	ekonomika	k1gFnSc2	ekonomika
mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc1d1	státní
rozpočet	rozpočet	k1gInSc1	rozpočet
v	v	k7c6	v
trvalém	trvalý	k2eAgInSc6d1	trvalý
přebytku	přebytek	k1gInSc6	přebytek
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k9	tak
k	k	k7c3	k
pokračujícímu	pokračující	k2eAgNnSc3d1	pokračující
snižování	snižování	k1gNnSc3	snižování
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
měla	mít	k5eAaImAgFnS	mít
Kanada	Kanada	k1gFnSc1	Kanada
30	[number]	k4	30
007	[number]	k4	007
094	[number]	k4	094
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
populaci	populace	k1gFnSc4	populace
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
Statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
na	na	k7c4	na
33,5	[number]	k4	33,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
růstu	růst	k1gInSc2	růst
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
imigrace	imigrace	k1gFnSc1	imigrace
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
porodnost	porodnost	k1gFnSc1	porodnost
<g/>
.	.	kIx.	.
</s>
<s>
Osídlení	osídlení	k1gNnSc1	osídlení
je	být	k5eAaImIp3nS	být
zřetelně	zřetelně	k6eAd1	zřetelně
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žije	žít	k5eAaImIp3nS	žít
poblíž	poblíž	k7c2	poblíž
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
či	či	k8xC	či
do	do	k7c2	do
160	[number]	k4	160
km	km	kA	km
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
nahuštění	nahuštění	k1gNnSc1	nahuštění
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zejména	zejména	k9	zejména
v	v	k7c6	v
koridorech	koridor	k1gInPc6	koridor
Québec-Windsor	Québec-Windsor	k1gInSc1	Québec-Windsor
<g/>
,	,	kIx,	,
Calgary-Edmonton	Calgary-Edmonton	k1gInSc1	Calgary-Edmonton
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Lower	Lowra	k1gFnPc2	Lowra
Mainland	Mainlanda	k1gFnPc2	Mainlanda
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
obrovská	obrovský	k2eAgNnPc1d1	obrovské
teritoriální	teritoriální	k2eAgNnPc1d1	teritoriální
území	území	k1gNnPc1	území
na	na	k7c6	na
severu	sever	k1gInSc6	sever
jsou	být	k5eAaImIp3nP	být
osídlena	osídlit	k5eAaPmNgNnP	osídlit
velice	velice	k6eAd1	velice
řídce	řídce	k6eAd1	řídce
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
etnicky	etnicky	k6eAd1	etnicky
velice	velice	k6eAd1	velice
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
34	[number]	k4	34
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
minimálně	minimálně	k6eAd1	minimálně
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
francouzsky	francouzsky	k6eAd1	francouzsky
mluvících	mluvící	k2eAgFnPc2d1	mluvící
<g/>
)	)	kIx)	)
navíc	navíc	k6eAd1	navíc
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
Kanaďanech	Kanaďan	k1gMnPc6	Kanaďan
i	i	k8xC	i
jako	jako	k9	jako
o	o	k7c6	o
např.	např.	kA	např.
Francouzích	Francouz	k1gMnPc6	Francouz
a	a	k8xC	a
ve	v	k7c4	v
sčítání	sčítání	k1gNnSc4	sčítání
udávají	udávat	k5eAaImIp3nP	udávat
obě	dva	k4xCgFnPc1	dva
tato	tento	k3xDgNnPc1	tento
etnická	etnický	k2eAgNnPc1d1	etnické
zařazení	zařazení	k1gNnPc1	zařazení
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc4d3	veliký
etnické	etnický	k2eAgFnPc4d1	etnická
skupiny	skupina	k1gFnPc4	skupina
jsou	být	k5eAaImIp3nP	být
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
(	(	kIx(	(
<g/>
39,4	[number]	k4	39,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Angličané	Angličan	k1gMnPc1	Angličan
(	(	kIx(	(
<g/>
20,2	[number]	k4	20,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
(	(	kIx(	(
<g/>
15,8	[number]	k4	15,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Skotové	Skot	k1gMnPc1	Skot
(	(	kIx(	(
<g/>
14,0	[number]	k4	14,0
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Irové	Ir	k1gMnPc1	Ir
(	(	kIx(	(
<g/>
12,9	[number]	k4	12,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
9,3	[number]	k4	9,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
(	(	kIx(	(
<g/>
4,3	[number]	k4	4,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Číňané	Číňan	k1gMnPc1	Číňan
(	(	kIx(	(
<g/>
3,7	[number]	k4	3,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
(	(	kIx(	(
<g/>
3,6	[number]	k4	3,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
První	první	k4xOgInPc1	první
národy	národ	k1gInPc1	národ
(	(	kIx(	(
<g/>
3,4	[number]	k4	3,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzsky	francouzsky	k6eAd1	francouzsky
mluvící	mluvící	k2eAgFnSc1d1	mluvící
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
zejména	zejména	k9	zejména
v	v	k7c6	v
Québecu	Québecus	k1gInSc6	Québecus
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
silné	silný	k2eAgFnPc4d1	silná
separatistické	separatistický	k2eAgFnPc4d1	separatistická
tendence	tendence	k1gFnPc4	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Eskymáci	Eskymák	k1gMnPc1	Eskymák
jsou	být	k5eAaImIp3nP	být
soustředěni	soustředit	k5eAaPmNgMnP	soustředit
zejména	zejména	k9	zejména
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
národy	národ	k1gInPc1	národ
a	a	k8xC	a
Métis	Métis	k1gFnPc1	Métis
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
země	zem	k1gFnSc2	zem
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
(	(	kIx(	(
<g/>
prérijní	prérijní	k2eAgFnSc2d1	prérijní
provincie	provincie	k1gFnSc2	provincie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgNnPc2	tři
původních	původní	k2eAgNnPc2d1	původní
etnik	etnikum	k1gNnPc2	etnikum
roste	růst	k5eAaImIp3nS	růst
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
zbytek	zbytek	k1gInSc1	zbytek
kanadské	kanadský	k2eAgFnSc2d1	kanadská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
kanadská	kanadský	k2eAgFnSc1d1	kanadská
vláda	vláda	k1gFnSc1	vláda
oficiálně	oficiálně	k6eAd1	oficiálně
přijala	přijmout	k5eAaPmAgFnS	přijmout
politiku	politika	k1gFnSc4	politika
multikulturalismu	multikulturalismus	k1gInSc2	multikulturalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Kanada	Kanada	k1gFnSc1	Kanada
přijímá	přijímat	k5eAaImIp3nS	přijímat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
okolo	okolo	k7c2	okolo
280	[number]	k4	280
000	[number]	k4	000
imigrantů	imigrant	k1gMnPc2	imigrant
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
nově	nově	k6eAd1	nově
příchozích	příchozí	k1gFnPc2	příchozí
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
patřilo	patřit	k5eAaImAgNnS	patřit
13,4	[number]	k4	13,4
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
viditelným	viditelný	k2eAgFnPc3d1	viditelná
menšinám	menšina	k1gFnPc3	menšina
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
k	k	k7c3	k
viditelným	viditelný	k2eAgFnPc3d1	viditelná
menšinám	menšina	k1gFnPc3	menšina
hlásilo	hlásit	k5eAaImAgNnS	hlásit
už	už	k9	už
19.1	[number]	k4	19.1
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
67,3	[number]	k4	67,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
církví	církev	k1gFnSc7	církev
je	být	k5eAaImIp3nS	být
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
43,6	[number]	k4	43,6
%	%	kIx~	%
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
tam	tam	k6eAd1	tam
i	i	k9	i
početné	početný	k2eAgFnSc2d1	početná
diecéze	diecéze	k1gFnSc2	diecéze
východních	východní	k2eAgMnPc2d1	východní
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
7	[number]	k4	7
církví	církev	k1gFnPc2	církev
<g/>
:	:	kIx,	:
arménští	arménský	k2eAgMnPc1d1	arménský
katolící	katolící	k2eAgMnPc1d1	katolící
<g/>
,	,	kIx,	,
chaldejští	chaldejský	k2eAgMnPc1d1	chaldejský
katolící	katolící	k2eAgMnPc1d1	katolící
z	z	k7c2	z
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
maronité	maronitý	k2eAgFnPc1d1	maronitý
<g/>
,	,	kIx,	,
řeckokatolíci-melchité	řeckokatolícielchita	k1gMnPc1	řeckokatolíci-melchita
<g/>
,	,	kIx,	,
ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
řeckokatolíci	řeckokatolík	k1gMnPc1	řeckokatolík
<g/>
,	,	kIx,	,
rumunští	rumunský	k2eAgMnPc1d1	rumunský
řeckokatolíci	řeckokatolík	k1gMnPc1	řeckokatolík
<g/>
,	,	kIx,	,
slovenští	slovenský	k2eAgMnPc1d1	slovenský
řeckokatolíci	řeckokatolík	k1gMnPc1	řeckokatolík
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
celkem	celkem	k6eAd1	celkem
251	[number]	k4	251
500	[number]	k4	500
věřících	věřící	k1gMnPc2	věřící
a	a	k8xC	a
část	část	k1gFnSc4	část
z	z	k7c2	z
36	[number]	k4	36
000	[number]	k4	000
arménských	arménský	k2eAgMnPc2d1	arménský
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
část	část	k1gFnSc4	část
z	z	k7c2	z
6	[number]	k4	6
200	[number]	k4	200
rumunských	rumunský	k2eAgMnPc2d1	rumunský
řeckokatolíků	řeckokatolík	k1gMnPc2	řeckokatolík
mají	mít	k5eAaImIp3nP	mít
diecéze	diecéze	k1gFnPc1	diecéze
pro	pro	k7c4	pro
USA	USA	kA	USA
a	a	k8xC	a
Kanadu	Kanada	k1gFnSc4	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
protestantskou	protestantský	k2eAgFnSc7d1	protestantská
denominací	denominace	k1gFnSc7	denominace
je	být	k5eAaImIp3nS	být
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
církev	církev	k1gFnSc1	církev
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
9,6	[number]	k4	9,6
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
ji	on	k3xPp3gFnSc4	on
anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nekřesťanských	křesťanský	k2eNgNnPc2d1	nekřesťanské
náboženství	náboženství	k1gNnPc2	náboženství
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
se	se	k3xPyFc4	se
profiluje	profilovat	k5eAaImIp3nS	profilovat
23,9	[number]	k4	23,9
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
různých	různý	k2eAgNnPc2d1	různé
vyznání	vyznání	k1gNnPc2	vyznání
není	být	k5eNaImIp3nS	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
rovnoměrné	rovnoměrný	k2eAgNnSc1d1	rovnoměrné
-	-	kIx~	-
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
má	mít	k5eAaImIp3nS	mít
enormně	enormně	k6eAd1	enormně
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
Québecu	Québecus	k1gInSc6	Québecus
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
irského	irský	k2eAgInSc2d1	irský
a	a	k8xC	a
francouzského	francouzský	k2eAgInSc2d1	francouzský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Sikhismus	Sikhismus	k1gInSc4	Sikhismus
<g/>
,	,	kIx,	,
islám	islám	k1gInSc4	islám
a	a	k8xC	a
buddhismus	buddhismus	k1gInSc4	buddhismus
mají	mít	k5eAaImIp3nP	mít
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
jako	jako	k8xC	jako
Toronto	Toronto	k1gNnSc1	Toronto
nebo	nebo	k8xC	nebo
Vancouver	Vancouver	k1gInSc1	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
je	být	k5eAaImIp3nS	být
nejrychleji	rychle	k6eAd3	rychle
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
náboženstvím	náboženství	k1gNnSc7	náboženství
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gMnPc2	jeho
vyznavačů	vyznavač	k1gMnPc2	vyznavač
stoupl	stoupnout	k5eAaPmAgMnS	stoupnout
z	z	k7c2	z
98	[number]	k4	98
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
liberální	liberální	k2eAgFnSc2d1	liberální
imigrační	imigrační	k2eAgFnSc2d1	imigrační
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
je	být	k5eAaImIp3nS	být
multikulturní	multikulturní	k2eAgFnSc1d1	multikulturní
země	země	k1gFnSc1	země
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
profiluje	profilovat	k5eAaImIp3nS	profilovat
jako	jako	k9	jako
dvojjazyčná	dvojjazyčný	k2eAgFnSc1d1	dvojjazyčná
<g/>
,	,	kIx,	,
úřední	úřední	k2eAgFnPc1d1	úřední
řeči	řeč	k1gFnPc4	řeč
jsou	být	k5eAaImIp3nP	být
angličtina	angličtina	k1gFnSc1	angličtina
(	(	kIx(	(
<g/>
mateřský	mateřský	k2eAgInSc1d1	mateřský
jazyk	jazyk	k1gInSc1	jazyk
59,7	[number]	k4	59,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
(	(	kIx(	(
<g/>
mateřský	mateřský	k2eAgInSc1d1	mateřský
jazyk	jazyk	k1gInSc1	jazyk
23,2	[number]	k4	23,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
jazyků	jazyk	k1gInPc2	jazyk
ovládá	ovládat	k5eAaImIp3nS	ovládat
98,5	[number]	k4	98,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
jen	jen	k6eAd1	jen
angličtinu	angličtina	k1gFnSc4	angličtina
67,5	[number]	k4	67,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
francouzštinu	francouzština	k1gFnSc4	francouzština
13,3	[number]	k4	13,3
%	%	kIx~	%
a	a	k8xC	a
oba	dva	k4xCgInPc4	dva
jazyky	jazyk	k1gInPc4	jazyk
17,7	[number]	k4	17,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
podmínka	podmínka	k1gFnSc1	podmínka
k	k	k7c3	k
udělení	udělení	k1gNnSc3	udělení
občanství	občanství	k1gNnSc2	občanství
žadatelům	žadatel	k1gMnPc3	žadatel
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
řečí	řeč	k1gFnSc7	řeč
většiny	většina	k1gFnSc2	většina
obyvatel	obyvatel	k1gMnSc1	obyvatel
všech	všecek	k3xTgFnPc2	všecek
provincií	provincie	k1gFnPc2	provincie
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Québecu	Québecus	k1gInSc2	Québecus
a	a	k8xC	a
ve	v	k7c6	v
dvou	dva	k4xCgInPc2	dva
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
:	:	kIx,	:
Yukonu	Yukon	k1gMnSc3	Yukon
a	a	k8xC	a
Severozápadních	severozápadní	k2eAgNnPc6d1	severozápadní
teritoriích	teritorium	k1gNnPc6	teritorium
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
do	do	k7c2	do
etnických	etnický	k2eAgFnPc2d1	etnická
statistik	statistika	k1gFnPc2	statistika
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
francouzské	francouzský	k2eAgFnSc3d1	francouzská
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nutně	nutně	k6eAd1	nutně
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
u	u	k7c2	u
dalších	další	k2eAgNnPc2d1	další
etnik	etnikum	k1gNnPc2	etnikum
-	-	kIx~	-
Irů	Ir	k1gMnPc2	Ir
a	a	k8xC	a
Skotů	Skot	k1gMnPc2	Skot
-	-	kIx~	-
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
nedá	dát	k5eNaPmIp3nS	dát
uvažovat	uvažovat	k5eAaImF	uvažovat
už	už	k6eAd1	už
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
řeč	řeč	k1gFnSc4	řeč
udává	udávat	k5eAaImIp3nS	udávat
angličtinu	angličtina	k1gFnSc4	angličtina
i	i	k9	i
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
potomků	potomek	k1gMnPc2	potomek
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Francouzština	francouzština	k1gFnSc1	francouzština
má	mít	k5eAaImIp3nS	mít
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
Québecu	Québecus	k1gInSc6	Québecus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
jediným	jediný	k2eAgInSc7d1	jediný
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Québec	Québec	k1gInSc1	Québec
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedinou	jediný	k2eAgFnSc7d1	jediná
provincií	provincie	k1gFnSc7	provincie
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
nemá	mít	k5eNaImIp3nS	mít
angličtina	angličtina	k1gFnSc1	angličtina
žádný	žádný	k3yNgInSc4	žádný
oficiální	oficiální	k2eAgInSc4d1	oficiální
status	status	k1gInSc4	status
a	a	k8xC	a
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
anglicky	anglicky	k6eAd1	anglicky
nedomluví	domluvit	k5eNaPmIp3nS	domluvit
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Québec	Québec	k1gInSc4	Québec
má	můj	k3xOp1gFnSc1	můj
francouzština	francouzština	k1gFnSc1	francouzština
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Nový	nový	k2eAgInSc4d1	nový
Brunšvik	Brunšvik	k1gInSc4	Brunšvik
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
provincií	provincie	k1gFnSc7	provincie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
celý	celý	k2eAgInSc1d1	celý
stát	stát	k1gInSc1	stát
dvojjazyčný	dvojjazyčný	k2eAgInSc1d1	dvojjazyčný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ontariu	Ontario	k1gNnSc6	Ontario
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
pouze	pouze	k6eAd1	pouze
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
polooficiální	polooficiální	k2eAgInSc4d1	polooficiální
status	status	k1gInSc4	status
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
styku	styk	k1gInSc6	styk
používána	používán	k2eAgFnSc1d1	používána
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
Manitoba	Manitoba	k1gFnSc1	Manitoba
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
nemá	mít	k5eNaImIp3nS	mít
oficiální	oficiální	k2eAgInSc4d1	oficiální
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
angličtina	angličtina	k1gFnSc1	angličtina
jím	jíst	k5eAaImIp1nS	jíst
de	de	k?	de
facto	facto	k1gNnSc1	facto
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
prince	princ	k1gMnSc2	princ
Edwarda	Edward	k1gMnSc2	Edward
a	a	k8xC	a
Newfoundland	Newfoundland	k1gInSc4	Newfoundland
a	a	k8xC	a
Labrador	Labrador	k1gInSc4	Labrador
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
vedenou	vedený	k2eAgFnSc4d1	vedená
pouze	pouze	k6eAd1	pouze
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
na	na	k7c6	na
nižších	nízký	k2eAgFnPc6d2	nižší
úrovních	úroveň	k1gFnPc6	úroveň
služeb	služba	k1gFnPc2	služba
se	se	k3xPyFc4	se
však	však	k9	však
lze	lze	k6eAd1	lze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
komunit	komunita	k1gFnPc2	komunita
domluvit	domluvit	k5eAaPmF	domluvit
různě	různě	k6eAd1	různě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Skotsku	Skotsko	k1gNnSc6	Skotsko
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
mají	mít	k5eAaImIp3nP	mít
významnou	významný	k2eAgFnSc4d1	významná
pozici	pozice	k1gFnSc4	pozice
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
irština	irština	k1gFnSc1	irština
a	a	k8xC	a
skotština	skotština	k1gFnSc1	skotština
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgInSc1d1	úřední
jazyk	jazyk	k1gInSc1	jazyk
není	být	k5eNaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Albertě	Alberta	k1gFnSc6	Alberta
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Yukonu	Yukon	k1gInSc6	Yukon
jsou	být	k5eAaImIp3nP	být
úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbylých	zbylý	k2eAgNnPc6d1	zbylé
dvou	dva	k4xCgNnPc6	dva
teritoriích	teritorium	k1gNnPc6	teritorium
jsou	být	k5eAaImIp3nP	být
jako	jako	k8xS	jako
úřední	úřední	k2eAgInPc4d1	úřední
jazyky	jazyk	k1gInPc4	jazyk
vedeny	veden	k2eAgInPc4d1	veden
krom	krom	k7c2	krom
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
ještě	ještě	k9	ještě
jazyky	jazyk	k1gInPc4	jazyk
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
Severozápadních	severozápadní	k2eAgNnPc6d1	severozápadní
teritoriích	teritorium	k1gNnPc6	teritorium
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
6	[number]	k4	6
<g/>
,	,	kIx,	,
v	v	k7c6	v
Nunavutu	Nunavut	k1gInSc6	Nunavut
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
Tabulka	tabulka	k1gFnSc1	tabulka
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
jedno-	jedno-	k?	jedno-
i	i	k8xC	i
vícečetné	vícečetný	k2eAgFnPc1d1	vícečetná
odpovědi	odpověď	k1gFnPc1	odpověď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
kultura	kultura	k1gFnSc1	kultura
představuje	představovat	k5eAaImIp3nS	představovat
velmi	velmi	k6eAd1	velmi
nesourodou	sourodý	k2eNgFnSc4d1	nesourodá
směs	směs	k1gFnSc4	směs
různých	různý	k2eAgFnPc2d1	různá
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zbytky	zbytek	k1gInPc4	zbytek
tradice	tradice	k1gFnSc2	tradice
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
Indiánů	Indián	k1gMnPc2	Indián
a	a	k8xC	a
Eskymáků	Eskymák	k1gMnPc2	Eskymák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnSc1	tradice
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
,	,	kIx,	,
Irů	Ir	k1gMnPc2	Ir
a	a	k8xC	a
Skotů	Skot	k1gMnPc2	Skot
a	a	k8xC	a
Angličanů	Angličan	k1gMnPc2	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
silný	silný	k2eAgInSc1d1	silný
vliv	vliv	k1gInSc1	vliv
americké	americký	k2eAgFnSc2d1	americká
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
amerických	americký	k2eAgNnPc2d1	americké
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgFnSc4d1	mluvící
část	část	k1gFnSc4	část
Kanaďanů	Kanaďan	k1gMnPc2	Kanaďan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc1	oblast
kultury	kultura	k1gFnSc2	kultura
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
společné	společný	k2eAgFnPc4d1	společná
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
a	a	k8xC	a
přetrvávání	přetrvávání	k1gNnSc1	přetrvávání
kanadských	kanadský	k2eAgInPc2d1	kanadský
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
kulturních	kulturní	k2eAgInPc2d1	kulturní
počinů	počin	k1gInPc2	počin
jsou	být	k5eAaImIp3nP	být
ovlivňovány	ovlivňován	k2eAgInPc1d1	ovlivňován
a	a	k8xC	a
podporovány	podporován	k2eAgInPc1d1	podporován
federálními	federální	k2eAgInPc7d1	federální
programy	program	k1gInPc7	program
<g/>
,	,	kIx,	,
zákony	zákon	k1gInPc7	zákon
a	a	k8xC	a
institucemi	instituce	k1gFnPc7	instituce
i	i	k8xC	i
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
nižších	nízký	k2eAgInPc2d2	nižší
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
hrají	hrát	k5eAaImIp3nP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
Canadian	Canadiana	k1gFnPc2	Canadiana
Broadcasting	Broadcasting	k1gInSc4	Broadcasting
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
CBC	CBC	kA	CBC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
National	National	k1gFnSc1	National
Film	film	k1gInSc1	film
Board	Board	k1gMnSc1	Board
of	of	k?	of
Canada	Canada	k1gFnSc1	Canada
(	(	kIx(	(
<g/>
NFB	NFB	kA	NFB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Canadian	Canadian	k1gInSc1	Canadian
Radio-television	Radioelevision	k1gInSc1	Radio-television
and	and	k?	and
Telecommunications	Telecommunications	k1gInSc1	Telecommunications
Commission	Commission	k1gInSc1	Commission
(	(	kIx(	(
<g/>
CRTC	CRTC	kA	CRTC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
Kanada	Kanada	k1gFnSc1	Kanada
geograficky	geograficky	k6eAd1	geograficky
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
a	a	k8xC	a
etnicky	etnicky	k6eAd1	etnicky
velice	velice	k6eAd1	velice
diversifikovaná	diversifikovaný	k2eAgFnSc1d1	diversifikovaná
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
obrovské	obrovský	k2eAgInPc4d1	obrovský
kulturní	kulturní	k2eAgInPc4d1	kulturní
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
provinciemi	provincie	k1gFnPc7	provincie
i	i	k8xC	i
nižšími	nízký	k2eAgInPc7d2	nižší
regiony	region	k1gInPc7	region
<g/>
.	.	kIx.	.
</s>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
stále	stále	k6eAd1	stále
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
současnou	současný	k2eAgFnSc7d1	současná
imigrací	imigrace	k1gFnSc7	imigrace
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
si	se	k3xPyFc3	se
tohoto	tento	k3xDgInSc2	tento
multikulturalismu	multikulturalismus	k1gInSc2	multikulturalismus
velice	velice	k6eAd1	velice
cení	cenit	k5eAaImIp3nP	cenit
a	a	k8xC	a
považují	považovat	k5eAaImIp3nP	považovat
ho	on	k3xPp3gInSc4	on
za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
klad	klad	k1gInSc4	klad
a	a	k8xC	a
přednost	přednost	k1gFnSc4	přednost
kanadské	kanadský	k2eAgFnSc2d1	kanadská
kultury	kultura	k1gFnSc2	kultura
celkově	celkově	k6eAd1	celkově
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kanadské	kanadský	k2eAgInPc4d1	kanadský
národní	národní	k2eAgInPc4d1	národní
symboly	symbol	k1gInPc4	symbol
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
přírodními	přírodní	k2eAgFnPc7d1	přírodní
podmínkami	podmínka	k1gFnPc7	podmínka
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
historií	historie	k1gFnSc7	historie
a	a	k8xC	a
též	též	k9	též
tradicemi	tradice	k1gFnPc7	tradice
Prvních	první	k4xOgInPc2	první
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
a	a	k8xC	a
nejznámějším	známý	k2eAgInSc7d3	nejznámější
státním	státní	k2eAgInSc7d1	státní
symbolem	symbol	k1gInSc7	symbol
je	být	k5eAaImIp3nS	být
javorový	javorový	k2eAgInSc1d1	javorový
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
první	první	k4xOgNnSc4	první
používání	používání	k1gNnSc4	používání
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
až	až	k9	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
na	na	k7c6	na
současné	současný	k2eAgFnSc6d1	současná
kanadské	kanadský	k2eAgFnSc6d1	kanadská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
