<s>
Kyberšikana	Kyberšikana	k1gFnSc1
</s>
<s>
Kyberšikana	Kyberšikana	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
kybernetická	kybernetický	k2eAgFnSc1d1
šikana	šikana	k1gFnSc1
<g/>
,	,	kIx,
počítačová	počítačový	k2eAgFnSc1d1
šikana	šikana	k1gFnSc1
či	či	k8xC
cyberbullying	cyberbullying	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kolektivní	kolektivní	k2eAgNnSc1d1
označení	označení	k1gNnSc1
forem	forma	k1gFnPc2
šikany	šikana	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
elektronických	elektronický	k2eAgNnPc2d1
médií	médium	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
internet	internet	k1gInSc1
a	a	k8xC
mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
slouží	sloužit	k5eAaImIp3nP
k	k	k7c3
agresivnímu	agresivní	k2eAgNnSc3d1
a	a	k8xC
záměrnému	záměrný	k2eAgNnSc3d1
poškození	poškození	k1gNnSc3
uživatele	uživatel	k1gMnPc4
těchto	tento	k3xDgNnPc2
médií	médium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
tradiční	tradiční	k2eAgFnSc1d1
šikana	šikana	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
i	i	k9
kyberšikana	kyberšikana	k1gFnSc1
opakované	opakovaný	k2eAgNnSc1d1
chování	chování	k1gNnSc4
a	a	k8xC
nepoměr	nepoměr	k1gInSc4
sil	síla	k1gFnPc2
mezi	mezi	k7c7
agresorem	agresor	k1gMnSc7
a	a	k8xC
obětí	oběť	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
další	další	k2eAgNnPc4d1
kritéria	kritérion	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
identifikují	identifikovat	k5eAaBmIp3nP
kyberšikanu	kyberšikana	k1gFnSc4
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
oběť	oběť	k1gFnSc1
vnímá	vnímat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
nepříjemné	příjemný	k2eNgNnSc1d1
a	a	k8xC
ubližující	ubližující	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kyberšikana	Kyberšikana	k1gFnSc1
však	však	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
způsobena	způsobit	k5eAaPmNgFnS
také	také	k9
neúmyslně	úmyslně	k6eNd1
-	-	kIx~
nevhodný	vhodný	k2eNgInSc1d1
žert	žert	k1gInSc1
se	se	k3xPyFc4
např.	např.	kA
vymkne	vymknout	k5eAaPmIp3nS
kontrole	kontrola	k1gFnSc3
a	a	k8xC
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
rozvinout	rozvinout	k5eAaPmF
do	do	k7c2
podoby	podoba	k1gFnSc2
intenzivní	intenzivní	k2eAgFnSc2d1
kyberšikany	kyberšikana	k1gFnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
kyberšikany	kyberšikana	k1gFnSc2
</s>
<s>
Kyberstalking	Kyberstalking	k1gInSc1
</s>
<s>
V	v	k7c6
překladu	překlad	k1gInSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
pronásledování	pronásledování	k1gNnSc4
v	v	k7c6
kyberprostoru	kyberprostor	k1gInSc6
nejčastěji	často	k6eAd3
pomocí	pomocí	k7c2
SMS	SMS	kA
<g/>
,	,	kIx,
chatu	chata	k1gFnSc4
<g/>
,	,	kIx,
emailu	email	k1gInSc2
<g/>
,	,	kIx,
telefonu	telefon	k1gInSc2
<g/>
,	,	kIx,
sociálních	sociální	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
,	,	kIx,
Skypu	Skyp	k1gInSc2
apod.	apod.	kA
Oběti	oběť	k1gFnSc2
většinou	většina	k1gFnSc7
pronásledovatele	pronásledovatel	k1gMnSc2
(	(	kIx(
<g/>
stalkera	stalkero	k1gNnSc2
<g/>
)	)	kIx)
znají	znát	k5eAaImIp3nP
<g/>
,	,	kIx,
často	často	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
bývalého	bývalý	k2eAgMnSc4d1
milence	milenec	k1gMnSc4
<g/>
/	/	kIx~
<g/>
milenku	milenka	k1gFnSc4
<g/>
,	,	kIx,
kamaráda	kamarád	k1gMnSc4
<g/>
,	,	kIx,
zrazeného	zrazený	k2eAgMnSc4d1
přítele	přítel	k1gMnSc4
nebo	nebo	k8xC
milovníka	milovník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalker	Stalker	k1gMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ale	ale	k8xC
i	i	k9
neznámý	známý	k2eNgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
oběť	oběť	k1gFnSc4
vyhlédl	vyhlédnout	k5eAaPmAgInS
náhodně	náhodně	k6eAd1
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pronásledované	pronásledovaný	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
hrozí	hrozit	k5eAaImIp3nS
naprostá	naprostý	k2eAgFnSc1d1
ztráta	ztráta	k1gFnSc1
soukromí	soukromí	k1gNnSc2
<g/>
,	,	kIx,
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
a	a	k8xC
pocitu	pocit	k1gInSc2
bezpečí	bezpečí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalking	Stalking	k1gInSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
nabyl	nabýt	k5eAaPmAgInS
skutkovou	skutkový	k2eAgFnSc4d1
podstatu	podstata	k1gFnSc4
trestného	trestný	k2eAgInSc2d1
činu	čin	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
pod	pod	k7c7
názvem	název	k1gInSc7
"	"	kIx"
<g/>
nebezpečné	bezpečný	k2eNgNnSc1d1
pronásledování	pronásledování	k1gNnSc1
<g/>
"	"	kIx"
zapsán	zapsán	k2eAgMnSc1d1
v	v	k7c6
trestním	trestní	k2eAgInSc6d1
zákoníku	zákoník	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kyberharašení	Kyberharašení	k1gNnSc1
</s>
<s>
Za	za	k7c4
kyberharašení	kyberharašení	k1gNnSc4
lze	lze	k6eAd1
označit	označit	k5eAaPmF
opakované	opakovaný	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
zasílané	zasílaný	k2eAgNnSc1d1
agresorem	agresor	k1gMnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
jsou	být	k5eAaImIp3nP
oběti	oběť	k1gFnPc1
nepříjemné	příjemný	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
situace	situace	k1gFnSc1
může	moct	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
i	i	k9
ze	z	k7c2
vzájemné	vzájemný	k2eAgFnSc2d1
konverzace	konverzace	k1gFnSc2
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
nepříjemnou	příjemný	k2eNgFnSc7d1
a	a	k8xC
oběť	oběť	k1gFnSc1
není	být	k5eNaImIp3nS
schopná	schopný	k2eAgFnSc1d1
ji	on	k3xPp3gFnSc4
ukončit	ukončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agresor	agresor	k1gMnSc1
většinou	většinou	k6eAd1
oběť	oběť	k1gFnSc4
začne	začít	k5eAaPmIp3nS
bombardovat	bombardovat	k5eAaImF
zprávami	zpráva	k1gFnPc7
ihned	ihned	k6eAd1
po	po	k7c4
připojení	připojení	k1gNnSc4
na	na	k7c4
internet	internet	k1gInSc4
nebo	nebo	k8xC
jí	on	k3xPp3gFnSc3
zasílá	zasílat	k5eAaImIp3nS
nežádoucí	žádoucí	k2eNgMnSc1d1
SMS	SMS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyloučení	vyloučení	k1gNnSc1
a	a	k8xC
ostrakizace	ostrakizace	k1gFnSc1
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
formě	forma	k1gFnSc6
kyberšikany	kyberšikana	k1gFnSc2
je	být	k5eAaImIp3nS
oběť	oběť	k1gFnSc1
vyloučena	vyloučen	k2eAgFnSc1d1
z	z	k7c2
nějaké	nějaký	k3yIgFnSc2
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
by	by	kYmCp3nS
chtěla	chtít	k5eAaImAgFnS
či	či	k8xC
měla	mít	k5eAaImAgFnS
patřit	patřit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrakizace	Ostrakizace	k1gFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
oběť	oběť	k1gFnSc4
velmi	velmi	k6eAd1
bolestná	bolestný	k2eAgFnSc1d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
postrádá	postrádat	k5eAaImIp3nS
přímý	přímý	k2eAgInSc4d1
prvek	prvek	k1gInSc4
agrese	agrese	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběť	oběť	k1gFnSc1
trpí	trpět	k5eAaImIp3nS
frustrací	frustrace	k1gFnSc7
z	z	k7c2
nenaplnění	nenaplnění	k1gNnSc2
potřeby	potřeba	k1gFnSc2
někam	někam	k6eAd1
patřit	patřit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
internetu	internet	k1gInSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
často	často	k6eAd1
horší	zlý	k2eAgMnSc1d2
než	než	k8xS
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
tam	tam	k6eAd1
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
oblíbený	oblíbený	k2eAgMnSc1d1
a	a	k8xC
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
ne	ne	k9
<g/>
,	,	kIx,
např.	např.	kA
je	být	k5eAaImIp3nS
oběť	oběť	k1gFnSc1
vyloučena	vyloučen	k2eAgFnSc1d1
z	z	k7c2
facebookové	facebookový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
to	ten	k3xDgNnSc1
většinou	většinou	k6eAd1
vidí	vidět	k5eAaImIp3nS
větší	veliký	k2eAgNnSc4d2
množství	množství	k1gNnSc4
lidí	člověk	k1gMnPc2
než	než	k8xS
v	v	k7c6
realitě	realita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kybergrooming	Kybergrooming	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Kybergrooming	Kybergrooming	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
překladu	překlad	k1gInSc6
jde	jít	k5eAaImIp3nS
o	o	k7c4
manipulaci	manipulace	k1gFnSc4
v	v	k7c6
kyberprostoru	kyberprostor	k1gInSc6
s	s	k7c7
cílem	cíl	k1gInSc7
přimět	přimět	k5eAaPmF
uživatele	uživatel	k1gMnPc4
k	k	k7c3
osobní	osobní	k2eAgFnSc3d1
schůzce	schůzka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útočník	útočník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
za	za	k7c4
někoho	někdo	k3yInSc4
jiného	jiný	k2eAgMnSc4d1
<g/>
,	,	kIx,
si	se	k3xPyFc3
vyhledá	vyhledat	k5eAaPmIp3nS
vhodnou	vhodný	k2eAgFnSc4d1
osobu	osoba	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
postupem	postupem	k7c2
času	čas	k1gInSc2
vzbudí	vzbudit	k5eAaPmIp3nS
důvěru	důvěra	k1gFnSc4
a	a	k8xC
přinutí	přinutit	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
k	k	k7c3
osobní	osobní	k2eAgFnSc3d1
schůzce	schůzka	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
pak	pak	k6eAd1
nějakým	nějaký	k3yIgInSc7
způsobem	způsob	k1gInSc7
oběť	oběť	k1gFnSc4
zneužije	zneužít	k5eAaPmIp3nS
či	či	k8xC
využije	využít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
nejvíce	nejvíce	k6eAd1,k6eAd3
ohroženy	ohrozit	k5eAaPmNgFnP
děti	dítě	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
závislé	závislý	k2eAgFnPc1d1
na	na	k7c6
technologiích	technologie	k1gFnPc6
<g/>
,	,	kIx,
tráví	trávit	k5eAaImIp3nP
na	na	k7c6
internetu	internet	k1gInSc6
většinu	většina	k1gFnSc4
času	čas	k1gInSc2
a	a	k8xC
většinu	většina	k1gFnSc4
přátel	přítel	k1gMnPc2
mají	mít	k5eAaImIp3nP
pouze	pouze	k6eAd1
ve	v	k7c6
virtuálním	virtuální	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Flaming	Flaming	k1gInSc1
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
nepřátelské	přátelský	k2eNgNnSc4d1
chování	chování	k1gNnSc4
útočníka	útočník	k1gMnSc2
vůči	vůči	k7c3
oběti	oběť	k1gFnSc3
ve	v	k7c6
virtuálním	virtuální	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
výrazně	výrazně	k6eAd1
vyhrocená	vyhrocený	k2eAgFnSc1d1
a	a	k8xC
agresivní	agresivní	k2eAgFnSc1d1
diskuze	diskuze	k1gFnSc1
až	až	k8xS
hádka	hádka	k1gFnSc1
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
uživatelé	uživatel	k1gMnPc1
úmyslně	úmyslně	k6eAd1
podobné	podobný	k2eAgFnPc4d1
diskuze	diskuze	k1gFnPc4
provokují	provokovat	k5eAaImIp3nP
vkládáním	vkládání	k1gNnSc7
různých	různý	k2eAgInPc2d1
kontroverzních	kontroverzní	k2eAgInPc2d1
příspěvků	příspěvek	k1gInPc2
<g/>
,	,	kIx,
urážením	urážení	k1gNnPc3
účastníků	účastník	k1gMnPc2
diskuzí	diskuze	k1gFnPc2
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Výzkumy	výzkum	k1gInPc1
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
slovní	slovní	k2eAgNnSc1d1
napadání	napadání	k1gNnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
virtuálním	virtuální	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
až	až	k9
čtyřikrát	čtyřikrát	k6eAd1
častější	častý	k2eAgNnSc1d2
než	než	k8xS
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sexting	Sexting	k1gInSc1
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
zasílání	zasílání	k1gNnSc4
textů	text	k1gInPc2
<g/>
,	,	kIx,
fotografií	fotografia	k1gFnPc2
a	a	k8xC
videí	video	k1gNnPc2
se	s	k7c7
sexuální	sexuální	k2eAgFnSc7d1
tematikou	tematika	k1gFnSc7
prostřednictvím	prostřednictvím	k7c2
elektronických	elektronický	k2eAgNnPc2d1
médií	médium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
materiály	materiál	k1gInPc4
pak	pak	k6eAd1
často	často	k6eAd1
končí	končit	k5eAaImIp3nS
na	na	k7c6
internetu	internet	k1gInSc6
a	a	k8xC
můžou	můžou	k?
mít	mít	k5eAaImF
pro	pro	k7c4
oběť	oběť	k1gFnSc4
fatální	fatální	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
použity	použít	k5eAaPmNgFnP
jako	jako	k8xS,k8xC
prostředek	prostředek	k1gInSc1
k	k	k7c3
vydírání	vydírání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
případy	případ	k1gInPc4
mohou	moct	k5eAaImIp3nP
skončit	skončit	k5eAaPmF
až	až	k9
smrtí	smrt	k1gFnSc7
oběti	oběť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útočník	útočník	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
oběť	oběť	k1gFnSc1
mladší	mladý	k2eAgFnSc1d2
18	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
dopouští	dopouštět	k5eAaImIp3nS
trestné	trestný	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
šíření	šíření	k1gNnSc2
dětské	dětský	k2eAgFnSc2d1
pornografie	pornografie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Podrobný	podrobný	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
sextingu	sexting	k1gInSc2
u	u	k7c2
české	český	k2eAgFnSc2d1
populace	populace	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
zrealizoval	zrealizovat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
tým	tým	k1gInSc1
projektu	projekt	k1gInSc2
E-Bezpečí	E-Bezpeč	k1gFnPc2
pod	pod	k7c7
názvem	název	k1gInSc7
Sexting	Sexting	k1gInSc1
a	a	k8xC
rizikové	rizikový	k2eAgNnSc1d1
chování	chování	k1gNnSc1
českých	český	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
v	v	k7c6
kyberprostoru	kyberprostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Happy	Happa	k1gFnPc1
Slapping	Slapping	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
celkem	celkem	k6eAd1
novou	nový	k2eAgFnSc4d1
„	„	k?
<g/>
zábavu	zábava	k1gFnSc4
<g/>
“	“	k?
mladých	mladý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
agresor	agresor	k1gMnSc1
si	se	k3xPyFc3
vybere	vybrat	k5eAaPmIp3nS
oběť	oběť	k1gFnSc4
a	a	k8xC
následně	následně	k6eAd1
ji	on	k3xPp3gFnSc4
fyzicky	fyzicky	k6eAd1
napadne	napadnout	k5eAaPmIp3nS
(	(	kIx(
<g/>
zfackuje	zfackovat	k5eAaPmIp3nS
<g/>
)	)	kIx)
a	a	k8xC
celé	celý	k2eAgNnSc1d1
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
nahrává	nahrávat	k5eAaImIp3nS
na	na	k7c4
mobilní	mobilní	k2eAgInSc4d1
telefon	telefon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
je	být	k5eAaImIp3nS
nahrávka	nahrávka	k1gFnSc1
zveřejněna	zveřejnit	k5eAaPmNgFnS
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
happy	happa	k1gFnSc2
slapping	slapping	k1gInSc1
neobsahuje	obsahovat	k5eNaImIp3nS
jen	jen	k9
fackování	fackování	k?
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k9
i	i	k9
závažnější	závažný	k2eAgInPc4d2
útoky	útok	k1gInPc4
za	za	k7c7
hranicí	hranice	k1gFnSc7
zákona	zákon	k1gInSc2
a	a	k8xC
i	i	k9
svlékání	svlékání	k1gNnSc2
oběti	oběť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zveřejnění	zveřejnění	k1gNnSc1
a	a	k8xC
šíření	šíření	k1gNnSc3
těchto	tento	k3xDgNnPc2
videí	video	k1gNnPc2
vedlo	vést	k5eAaImAgNnS
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
až	až	k9
k	k	k7c3
sebevraždě	sebevražda	k1gFnSc3
oběti	oběť	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
aktérů	aktér	k1gMnPc2
kyberšikany	kyberšikana	k1gFnSc2
</s>
<s>
Role	role	k1gFnSc1
„	„	k?
<g/>
oběť-agresor-přihlížející	oběť-agresor-přihlížející	k2eAgNnSc1d1
<g/>
“	“	k?
jsou	být	k5eAaImIp3nP
v	v	k7c6
kyberprostoru	kyberprostor	k1gInSc6
rozloženy	rozložit	k5eAaPmNgFnP
jinak	jinak	k6eAd1
než	než	k8xS
u	u	k7c2
tradiční	tradiční	k2eAgFnSc2d1
šikany	šikana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdánlivě	zdánlivě	k6eAd1
jasný	jasný	k2eAgInSc1d1
případ	případ	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
mnohem	mnohem	k6eAd1
složitější	složitý	k2eAgMnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oběť	oběť	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c4
nejčastější	častý	k2eAgFnPc4d3
oběti	oběť	k1gFnPc4
kyberšikany	kyberšikana	k1gFnSc2
patří	patřit	k5eAaImIp3nP
děti	dítě	k1gFnPc1
a	a	k8xC
teenageři	teenager	k1gMnPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
odmítány	odmítat	k5eAaImNgInP
kolektivem	kolektiv	k1gInSc7
z	z	k7c2
důvodu	důvod	k1gInSc2
osobnostní	osobnostní	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
plachost	plachost	k1gFnSc1
<g/>
,	,	kIx,
stydlivost	stydlivost	k1gFnSc1
<g/>
,	,	kIx,
nejistota	nejistota	k1gFnSc1
a	a	k8xC
fyzické	fyzický	k2eAgInPc1d1
atributy	atribut	k1gInPc1
(	(	kIx(
<g/>
barva	barva	k1gFnSc1
vlasů	vlas	k1gInPc2
<g/>
,	,	kIx,
pleti	pleť	k1gFnSc2
<g/>
,	,	kIx,
styl	styl	k1gInSc4
oblékání	oblékání	k1gNnSc2
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
i	i	k9
případy	případ	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
agresor	agresor	k1gMnSc1
stane	stanout	k5eAaPmIp3nS
obětí	oběť	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
v	v	k7c6
případech	případ	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
takto	takto	k6eAd1
jeho	jeho	k3xOp3gFnSc1
oběť	oběť	k1gFnSc1
mstí	mstít	k5eAaImIp3nS
nebo	nebo	k8xC
se	se	k3xPyFc4
proti	proti	k7c3
jeho	jeho	k3xOp3gNnSc3
chování	chování	k1gNnSc3
zvedne	zvednout	k5eAaPmIp3nS
nepřiměřený	přiměřený	k2eNgInSc4d1
odpor	odpor	k1gInSc4
na	na	k7c6
internetu	internet	k1gInSc6
ze	z	k7c2
strany	strana	k1gFnSc2
dalších	další	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Fenomén	fenomén	k1gInSc1
přepínání	přepínání	k1gNnSc2
rolí	role	k1gFnPc2
u	u	k7c2
kybernetických	kybernetický	k2eAgFnPc2d1
forem	forma	k1gFnPc2
agrese	agrese	k1gFnSc2
podrobně	podrobně	k6eAd1
mapují	mapovat	k5eAaImIp3nP
výzkumy	výzkum	k1gInPc1
Centra	centrum	k1gNnSc2
PRVoK	prvok	k1gMnSc1
PdF	PdF	k1gMnSc1
UP	UP	kA
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběť	oběť	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
projevovaným	projevovaný	k2eAgNnSc7d1
nepřátelstvím	nepřátelství	k1gNnSc7
silně	silně	k6eAd1
trápí	trápit	k5eAaImIp3nS
<g/>
,	,	kIx,
pociťuje	pociťovat	k5eAaImIp3nS
vztek	vztek	k1gInSc4
<g/>
,	,	kIx,
zoufalství	zoufalství	k1gNnSc4
či	či	k8xC
bezmoc	bezmoc	k1gFnSc4
a	a	k8xC
trpí	trpět	k5eAaImIp3nS
nespavostí	nespavost	k1gFnSc7
a	a	k8xC
bolestmi	bolest	k1gFnPc7
hlavy	hlava	k1gFnSc2
nebo	nebo	k8xC
břicha	břicho	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Agresor	agresor	k1gMnSc1
</s>
<s>
Agresorům	agresor	k1gMnPc3
kyberšikany	kyberšikana	k1gFnSc2
je	být	k5eAaImIp3nS
společná	společný	k2eAgFnSc1d1
nižší	nízký	k2eAgFnSc1d2
míra	míra	k1gFnSc1
empatie	empatie	k1gFnSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
dětmi	dítě	k1gFnPc7
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
neumějí	umět	k5eNaImIp3nP
vcítit	vcítit	k5eAaPmF
do	do	k7c2
oběti	oběť	k1gFnSc2
a	a	k8xC
chápat	chápat	k5eAaImF
<g/>
,	,	kIx,
jaké	jaký	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
zranění	zranění	k1gNnSc4
způsobují	způsobovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agresor	agresor	k1gMnSc1
svou	svůj	k3xOyFgFnSc4
oběť	oběť	k1gFnSc4
nevidí	vidět	k5eNaImIp3nS
a	a	k8xC
nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
její	její	k3xOp3gFnPc4
reakce	reakce	k1gFnPc4
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
nedokáže	dokázat	k5eNaPmIp3nS
odhadnout	odhadnout	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
velkou	velký	k2eAgFnSc4d1
újmu	újma	k1gFnSc4
by	by	kYmCp3nS
ji	on	k3xPp3gFnSc4
mohl	moct	k5eAaImAgMnS
způsobit	způsobit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
anonymita	anonymita	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
internet	internet	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
snižuje	snižovat	k5eAaImIp3nS
práh	práh	k1gInSc1
zábran	zábrana	k1gFnPc2
vůči	vůči	k7c3
šikanování	šikanování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anonymita	anonymita	k1gFnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
předpokladem	předpoklad	k1gInSc7
pro	pro	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
pachatel	pachatel	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
strhnout	strhnout	k5eAaPmF
k	k	k7c3
takovým	takový	k3xDgInPc3
způsobům	způsob	k1gInPc3
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
jaké	jaký	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
by	by	kYmCp3nS
dříve	dříve	k6eAd2
patrně	patrně	k6eAd1
neprojevil	projevit	k5eNaPmAgMnS
ze	z	k7c2
strachu	strach	k1gInSc2
před	před	k7c7
sociální	sociální	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přihlížející	přihlížející	k2eAgFnSc1d1
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
druhů	druh	k1gInPc2
přihlížejících	přihlížející	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
jsou	být	k5eAaImIp3nP
následovníci	následovník	k1gMnPc1
agresora	agresor	k1gMnSc2
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
jsou	být	k5eAaImIp3nP
nezúčastnění	nezúčastnění	k1gNnSc4
a	a	k8xC
někteří	některý	k3yIgMnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
straně	strana	k1gFnSc6
oběti	oběť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Role	role	k1gFnSc1
těchto	tento	k3xDgFnPc2
přihlížejících	přihlížející	k2eAgFnPc2d1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
důležitá	důležitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
proti	proti	k7c3
kyberšikaně	kyberšikaně	k6eAd1
otevřeně	otevřeně	k6eAd1
vystoupí	vystoupit	k5eAaPmIp3nP
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
ale	ale	k9
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
bohužel	bohužel	k9
nestává	stávat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kyberšikana	Kyberšikana	k1gFnSc1
mezi	mezi	k7c7
mladistvými	mladistvý	k1gMnPc7
</s>
<s>
Podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
organizace	organizace	k1gFnSc2
YoungMinds	YoungMindsa	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgNnSc6,k3yIgNnSc6,k3yQgNnSc6
bylo	být	k5eAaImAgNnS
62	#num#	k4
%	%	kIx~
dotazovaných	dotazovaný	k2eAgInPc2d1
mladších	mladý	k2eAgInPc2d2
18	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
v	v	k7c6
polovině	polovina	k1gFnSc6
šlo	jít	k5eAaImAgNnS
o	o	k7c4
ženy	žena	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zaznamenala	zaznamenat	k5eAaPmAgFnS
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
takové	takový	k3xDgNnSc1
sdělení	sdělení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
označili	označit	k5eAaPmAgMnP
za	za	k7c4
šokující	šokující	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
47	#num#	k4
%	%	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
dotazovaných	dotazovaný	k2eAgMnPc2d1
mělo	mít	k5eAaImAgNnS
zkušenost	zkušenost	k1gFnSc4
s	s	k7c7
nevhodnými	vhodný	k2eNgFnPc7d1
zprávami	zpráva	k1gFnPc7
nebo	nebo	k8xC
komentáři	komentář	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Přestože	přestože	k8xS
většina	většina	k1gFnSc1
sociálních	sociální	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
umožňuje	umožňovat	k5eAaImIp3nS
založení	založení	k1gNnSc1
účtu	účet	k1gInSc2
až	až	k6eAd1
od	od	k7c2
13	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
61	#num#	k4
procent	procento	k1gNnPc2
dotazovaných	dotazovaný	k2eAgMnPc2d1
mělo	mít	k5eAaImAgNnS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
účet	účet	k1gInSc4
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
ve	v	k7c6
12	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Podle	podle	k7c2
výsledků	výsledek	k1gInPc2
šetření	šetření	k1gNnSc6
mladí	mladý	k2eAgMnPc1d1
považují	považovat	k5eAaImIp3nP
kyberšikanu	kyberšikana	k1gFnSc4
za	za	k7c4
nevyhnutelnou	vyhnutelný	k2eNgFnSc4d1
součást	součást	k1gFnSc4
sociálních	sociální	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
dospělých	dospělý	k2eAgFnPc2d1
je	být	k5eAaImIp3nS
řešením	řešení	k1gNnSc7
v	v	k7c6
případě	případ	k1gInSc6
kyberšikany	kyberšikana	k1gFnSc2
vymazat	vymazat	k5eAaPmF
účet	účet	k1gInSc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
podle	podle	k7c2
dospívajících	dospívající	k2eAgFnPc2d1
toto	tento	k3xDgNnSc1
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
chtít	chtít	k5eAaImF
po	po	k7c6
někom	někdo	k3yInSc6
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
vyrůstá	vyrůstat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kyberšikana	Kyberšikana	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c4
učitele	učitel	k1gMnPc4
</s>
<s>
Specifickou	specifický	k2eAgFnSc4d1
formu	forma	k1gFnSc4
kyberšikany	kyberšikana	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
kyberšikana	kyberšikana	k1gFnSc1
cílená	cílený	k2eAgFnSc1d1
na	na	k7c4
učitele	učitel	k1gMnSc4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
má	mít	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
podob	podoba	k1gFnPc2
společných	společný	k2eAgFnPc2d1
s	s	k7c7
kyberšikanou	kyberšikaný	k2eAgFnSc7d1
cílenou	cílený	k2eAgFnSc7d1
na	na	k7c4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
zahrnuje	zahrnovat	k5eAaImIp3nS
však	však	k9
nové	nový	k2eAgFnPc4d1
formy	forma	k1gFnPc4
agrese	agrese	k1gFnSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yRgInPc3,k3yIgInPc3,k3yQgInPc3
patří	patřit	k5eAaImIp3nP
prankování	prankování	k1gNnSc4
učitelů	učitel	k1gMnPc2
v	v	k7c6
online	onlinout	k5eAaPmIp3nS
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
kyberbaiting	kyberbaiting	k1gInSc1
<g/>
,	,	kIx,
agresivní	agresivní	k2eAgNnSc1d1
hodnocení	hodnocení	k1gNnSc1
učitelů	učitel	k1gMnPc2
prostřednictvím	prostřednictvím	k7c2
online	onlinout	k5eAaPmIp3nS
www	www	k?
stránek	stránka	k1gFnPc2
apod.	apod.	kA
Podrobnosti	podrobnost	k1gFnSc2
o	o	k7c6
výskytu	výskyt	k1gInSc6
kyberšikany	kyberšikana	k1gFnSc2
u	u	k7c2
českých	český	k2eAgMnPc2d1
učitelů	učitel	k1gMnPc2
mapoval	mapovat	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
např.	např.	kA
Národní	národní	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
kyberšikany	kyberšikana	k1gFnSc2
učitelů	učitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PRICE	PRICE	kA
<g/>
,	,	kIx,
Megan	Megan	k1gMnSc1
<g/>
;	;	kIx,
DALGLEISH	DALGLEISH	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyberbullying	Cyberbullying	k1gInSc1
<g/>
:	:	kIx,
Experiences	Experiences	k1gInSc1
<g/>
,	,	kIx,
Impacts	Impacts	k1gInSc1
and	and	k?
Coping	Coping	k1gInSc1
Strategies	Strategiesa	k1gFnPc2
as	as	k9
Described	Described	k1gInSc4
by	by	kYmCp3nS
Australian	Australian	k1gMnSc1
Young	Young	k1gMnSc1
People	People	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Youth	Youth	k1gMnSc1
Studies	Studies	k1gMnSc1
Australia	Australia	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
51	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
ČERNÁ	Černá	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyberšikana	Kyberšikana	k1gFnSc1
<g/>
:	:	kIx,
Průvodce	průvodce	k1gMnSc1
novým	nový	k2eAgInSc7d1
fenoménem	fenomén	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
1977	#num#	k4
<g/>
-	-	kIx~
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Kopecký	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Kamil	Kamil	k1gMnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rizikové	rizikový	k2eAgFnPc1d1
formy	forma	k1gFnPc1
chování	chování	k1gNnSc2
českých	český	k2eAgFnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
v	v	k7c6
prostředí	prostředí	k1gNnSc6
internetu	internet	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
169	#num#	k4
stran	strana	k1gFnPc2
s.	s.	k?
ISBN	ISBN	kA
9788024448619	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
8024448610	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
944217915	#num#	k4
1	#num#	k4
2	#num#	k4
3	#num#	k4
SPITZER	SPITZER	kA
<g/>
,	,	kIx,
Manfred	Manfred	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kybernemoc	Kybernemoc	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Host	host	k1gMnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7491	#num#	k4
<g/>
-	-	kIx~
<g/>
792	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
140	#num#	k4
<g/>
-	-	kIx~
<g/>
141	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
BURDOVÁ	Burdová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
;	;	kIx,
TRAXLER	TRAXLER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezpečně	bezpečně	k6eAd1
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
se	s	k7c7
Vzdělávacím	vzdělávací	k2eAgInSc7d1
institutem	institut	k1gInSc7
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
VISK	VISK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ECKERTOVÁ	ECKERTOVÁ	kA
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
;	;	kIx,
DOČEKAL	Dočekal	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezpečnost	bezpečnost	k1gFnSc1
dětí	dítě	k1gFnPc2
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
:	:	kIx,
Rádce	rádce	k1gMnSc1
zodpovědného	zodpovědný	k2eAgMnSc2d1
rodiče	rodič	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
ŠEVČÍKOVÁ	Ševčíková	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děti	dítě	k1gFnPc1
a	a	k8xC
dospívající	dospívající	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
Vybraná	vybraná	k1gFnSc1
rizika	riziko	k1gNnSc2
používání	používání	k1gNnSc2
internetu	internet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
1977	#num#	k4
<g/>
-	-	kIx~
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Kopecký	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Kamil	Kamil	k1gMnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rizikové	rizikový	k2eAgFnPc1d1
formy	forma	k1gFnPc1
chování	chování	k1gNnSc2
českých	český	k2eAgFnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
v	v	k7c6
prostředí	prostředí	k1gNnSc6
internetu	internet	k1gInSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
169	#num#	k4
stran	strana	k1gFnPc2
s.	s.	k?
ISBN	ISBN	kA
9788024448619	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
8024448610	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
944217915	#num#	k4
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
-jan-	-jan-	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyberšikana	Kyberšikana	k1gFnSc1
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospívající	dospívající	k2eAgFnSc4d1
požadují	požadovat	k5eAaImIp3nP
řešení	řešení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
26.02	26.02	k4
<g/>
.2018	.2018	k4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
1977	#num#	k4
<g/>
-	-	kIx~
<g/>
,	,	kIx,
Szotkowski	Szotkowski	k1gNnSc1
<g/>
,	,	kIx,
René	René	k1gFnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyberšikana	Kyberšikana	k1gFnSc1
a	a	k8xC
další	další	k2eAgMnPc4d1
druhy	druh	k1gMnPc4
online	onlinout	k5eAaPmIp3nS
agrese	agrese	k1gFnSc1
zaměřené	zaměřený	k2eAgNnSc1d1
na	na	k7c4
učitele	učitel	k1gMnSc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
128	#num#	k4
stran	strana	k1gFnPc2
s.	s.	k?
ISBN	ISBN	kA
9788024453347	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
8024453347	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
1056249320	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kyberšikana	Kyberšikan	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kyberšikana	Kyberšikana	k1gFnSc1
</s>
<s>
Legislativa	legislativa	k1gFnSc1
kyberšikany	kyberšikana	k1gFnSc2
</s>
<s>
Jiří	Jiří	k1gMnSc1
Peterka	Peterka	k1gMnSc1
<g/>
:	:	kIx,
SID	SID	kA
2009	#num#	k4
<g/>
:	:	kIx,
do	do	k7c2
boje	boj	k1gInSc2
proti	proti	k7c3
kybernetické	kybernetický	k2eAgFnSc3d1
šikaně	šikana	k1gFnSc3
<g/>
!	!	kIx.
</s>
<s>
Rizika	riziko	k1gNnPc1
virtuální	virtuální	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
-	-	kIx~
brožura	brožura	k1gFnSc1
pro	pro	k7c4
učitele	učitel	k1gMnPc4
a	a	k8xC
rodiče	rodič	k1gMnPc4
</s>
<s>
Kyberšikana	Kyberšikana	k1gFnSc1
českých	český	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
-	-	kIx~
výzkum	výzkum	k1gInSc4
E-Bezpečí	E-Bezpeč	k1gFnPc2
2009	#num#	k4
</s>
<s>
Národní	národní	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
kyberšikany	kyberšikana	k1gFnSc2
učitelů	učitel	k1gMnPc2
</s>
<s>
Instituce	instituce	k1gFnSc1
zaměřené	zaměřený	k2eAgNnSc4d1
na	na	k7c6
prevenci	prevence	k1gFnSc6
kyberšikany	kyberšikana	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
nebezpečných	bezpečný	k2eNgInPc2d1
jevů	jev	k1gInPc2
</s>
<s>
Seznam	seznam	k1gInSc1
se	se	k3xPyFc4
bezpečně	bezpečně	k6eAd1
<g/>
!	!	kIx.
</s>
<s>
Centrum	centrum	k1gNnSc1
prevence	prevence	k1gFnSc2
rizikové	rizikový	k2eAgFnSc2d1
virtuální	virtuální	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
PdF	PdF	k1gFnSc2
UP	UP	kA
</s>
<s>
Projekt	projekt	k1gInSc1
E-Bezpečí	E-Bezpeč	k1gFnPc2
</s>
<s>
Národní	národní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
bezpečnějšího	bezpečný	k2eAgInSc2d2
internetu	internet	k1gInSc2
(	(	kIx(
<g/>
SaferInternet	SaferInternet	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Projekt	projekt	k1gInSc1
Minimalizace	minimalizace	k1gFnSc2
šikany	šikana	k1gFnSc2
</s>
<s>
Kyberšikana	Kyberšikana	k1gFnSc1
-	-	kIx~
Cyberhelp	Cyberhelp	k1gInSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
<g/>
,	,	kIx,
praktické	praktický	k2eAgFnPc1d1
rady	rada	k1gFnPc1
pro	pro	k7c4
učitelé	učitel	k1gMnPc1
</s>
<s>
Nebuď	budit	k5eNaImRp2nS
oběť	oběť	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7614486-0	7614486-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2006004667	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2006004667	#num#	k4
</s>
