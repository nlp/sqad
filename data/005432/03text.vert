<s>
Tibera	Tibera	k1gFnSc1	Tibera
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Tiber	Tiber	k1gInSc1	Tiber
<g/>
,	,	kIx,	,
-is	s	k?	-is
<g/>
,	,	kIx,	,
m.	m.	k?	m.
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
Tevere	Tever	k1gMnSc5	Tever
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Lazio	Lazio	k1gNnSc1	Lazio
<g/>
,	,	kIx,	,
Emilia-Romagna	Emilia-Romagna	k1gFnSc1	Emilia-Romagna
<g/>
,	,	kIx,	,
Toskánsko	Toskánsko	k1gNnSc1	Toskánsko
<g/>
,	,	kIx,	,
Umbrie	Umbrie	k1gFnSc1	Umbrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
délkou	délka	k1gFnSc7	délka
405	[number]	k4	405
km	km	kA	km
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
třetí	třetí	k4xOgFnSc4	třetí
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
řeku	řeka	k1gFnSc4	řeka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
po	po	k7c6	po
Pádu	Pád	k1gInSc6	Pád
a	a	k8xC	a
Adiži	Adiže	k1gFnSc6	Adiže
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
16	[number]	k4	16
500	[number]	k4	500
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
řeka	řeka	k1gFnSc1	řeka
spadala	spadat	k5eAaImAgFnS	spadat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
starořímského	starořímský	k2eAgMnSc2d1	starořímský
boha	bůh	k1gMnSc2	bůh
Tibera	Tibera	k1gFnSc1	Tibera
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nP	pramenit
v	v	k7c6	v
Toskánsko-Emiliánských	Toskánsko-Emiliánský	k2eAgFnPc6d1	Toskánsko-Emiliánský
Apeninách	Apeniny	k1gFnPc6	Apeniny
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
teče	téct	k5eAaImIp3nS	téct
převážně	převážně	k6eAd1	převážně
mezi	mezi	k7c7	mezi
horami	hora	k1gFnPc7	hora
v	v	k7c6	v
soutěskách	soutěska	k1gFnPc6	soutěska
a	a	k8xC	a
kotlinách	kotlina	k1gFnPc6	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gInPc6	jejich
přítocích	přítok	k1gInPc6	přítok
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vodopády	vodopád	k1gInPc1	vodopád
a	a	k8xC	a
splavy	splav	k1gInPc1	splav
(	(	kIx(	(
<g/>
La	la	k0	la
Marmore	Marmor	k1gMnSc5	Marmor
<g/>
,	,	kIx,	,
Tivoli	Tivole	k1gFnSc4	Tivole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
protéká	protékat	k5eAaImIp3nS	protékat
rovinou	rovina	k1gFnSc7	rovina
Maremma	maremma	k1gFnSc1	maremma
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
Tyrhénského	tyrhénský	k2eAgNnSc2d1	Tyrhénské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
deltu	delta	k1gFnSc4	delta
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
250	[number]	k4	250
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
dešťový	dešťový	k2eAgInSc1d1	dešťový
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
vodnosti	vodnost	k1gFnSc2	vodnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
března	březen	k1gInSc2	březen
až	až	k8xS	až
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
povodním	povodeň	k1gFnPc3	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
činí	činit	k5eAaImIp3nS	činit
260	[number]	k4	260
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Řeka	Řek	k1gMnSc2	Řek
unáší	unášet	k5eAaImIp3nS	unášet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
pro	pro	k7c4	pro
menší	malý	k2eAgFnPc4d2	menší
lodě	loď	k1gFnPc4	loď
pod	pod	k7c7	pod
Římem	Řím	k1gInSc7	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
přítocích	přítok	k1gInPc6	přítok
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
několik	několik	k4yIc1	několik
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
(	(	kIx(	(
<g/>
Korbarabasci	Korbarabasek	k1gMnPc1	Korbarabasek
<g/>
,	,	kIx,	,
Galleto	Gallet	k2eAgNnSc4d1	Gallet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
říční	říční	k2eAgFnSc1d1	říční
cesta	cesta	k1gFnSc1	cesta
mezi	mezi	k7c7	mezi
ostijským	ostijský	k2eAgInSc7d1	ostijský
přístavem	přístav	k1gInSc7	přístav
a	a	k8xC	a
Římem	Řím	k1gInSc7	Řím
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
bylo	být	k5eAaImAgNnS	být
lodní	lodní	k2eAgNnSc1d1	lodní
spojení	spojení	k1gNnSc1	spojení
udržováno	udržován	k2eAgNnSc1d1	udržováno
i	i	k9	i
za	za	k7c7	za
Římem	Řím	k1gInSc7	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
často	často	k6eAd1	často
zaplavovala	zaplavovat	k5eAaImAgFnS	zaplavovat
níže	nízce	k6eAd2	nízce
položené	položený	k2eAgFnPc4d1	položená
části	část	k1gFnPc4	část
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
ležely	ležet	k5eAaImAgFnP	ležet
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
metrů	metr	k1gInPc2	metr
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Т	Т	k?	Т
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tibera	Tibera	k1gFnSc1	Tibera
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Tibera	Tibera	k1gFnSc1	Tibera
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
