<s>
Szatmárský	Szatmárský	k2eAgInSc1d1
mír	mír	k1gInSc1
</s>
<s>
Szatmárský	Szatmárský	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Satumarský	Satumarský	k2eAgInSc4d1
mír	mír	k1gInSc4
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
Szatmári	Szatmári	k1gNnSc1
béke	bék	k1gFnSc2
<g/>
;	;	kIx,
německy	německy	k6eAd1
Friede	Fried	k1gMnSc5
von	von	k1gInSc4
Sathmar	Sathmar	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
dohoda	dohoda	k1gFnSc1
o	o	k7c6
ukončení	ukončení	k1gNnSc6
protihabsburského	protihabsburský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
Františka	František	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rákóczyho	Rákóczyha	k1gFnSc5
uzavřená	uzavřený	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1711	#num#	k4
v	v	k7c6
uherském	uherský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Szatmáru	Szatmár	k1gInSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Satu	Sat	k2eAgFnSc4d1
Mare	Mare	k1gFnSc4
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
<g/>
)	)	kIx)
mezi	mezi	k7c7
hlavním	hlavní	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
rákóczyovských	rákóczyovský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
hrabětem	hrabě	k1gMnSc7
Sándorem	Sándor	k1gMnSc7
(	(	kIx(
<g/>
Alexandrem	Alexandr	k1gMnSc7
<g/>
)	)	kIx)
Károlyim	Károlyim	k1gMnSc1
a	a	k8xC
císařským	císařský	k2eAgMnSc7d1
maršálem	maršál	k1gMnSc7
<g/>
,	,	kIx,
hrabětem	hrabě	k1gMnSc7
Janem	Jan	k1gMnSc7
Pálffym	Pálffym	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Mírová	mírový	k2eAgNnPc4d1
jednání	jednání	k1gNnPc4
začala	začít	k5eAaPmAgFnS
už	už	k6eAd1
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1711	#num#	k4
–	–	k?
po	po	k7c6
odchodu	odchod	k1gInSc6
Rákóczyho	Rákóczy	k1gMnSc2
za	za	k7c7
ruským	ruský	k2eAgMnSc7d1
carem	car	k1gMnSc7
Petrem	Petr	k1gMnSc7
Velikým	veliký	k2eAgMnSc7d1
<g/>
,	,	kIx,
u	u	k7c2
nějž	jenž	k3xRgInSc2
usiloval	usilovat	k5eAaImAgInS
získat	získat	k5eAaPmF
podporu	podpora	k1gFnSc4
a	a	k8xC
pomoc	pomoc	k1gFnSc4
v	v	k7c6
bojích	boj	k1gInPc6
–	–	k?
a	a	k8xC
skončila	skončit	k5eAaPmAgFnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
odbojné	odbojný	k2eAgInPc1d1
stavy	stav	k1gInPc1
nahlédly	nahlédnout	k5eAaPmAgInP
bezvýchodnost	bezvýchodnost	k1gFnSc4
situace	situace	k1gFnSc2
<g/>
,	,	kIx,
získaly	získat	k5eAaPmAgInP
jistotu	jistota	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
nebudou	být	k5eNaImBp3nP
habsburským	habsburský	k2eAgInSc7d1
dvorem	dvůr	k1gInSc7
stíhány	stíhán	k2eAgFnPc4d1
pro	pro	k7c4
odboj	odboj	k1gInSc4
a	a	k8xC
udrží	udržet	k5eAaPmIp3nS
si	se	k3xPyFc3
v	v	k7c4
monarchii	monarchie	k1gFnSc4
své	svůj	k3xOyFgFnSc2
výsady	výsada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazítří	nazítří	k1gNnSc1
po	po	k7c6
podepsání	podepsání	k1gNnSc6
míru	mír	k1gInSc2
tehdejší	tehdejší	k2eAgNnPc1d1
stavovská	stavovský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
složila	složit	k5eAaPmAgFnS
zbraně	zbraň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Szatmárským	Szatmárský	k2eAgInSc7d1
mírem	mír	k1gInSc7
skončila	skončit	k5eAaPmAgFnS
série	série	k1gFnSc1
protihabsburských	protihabsburský	k2eAgNnPc2d1
stavovských	stavovský	k2eAgNnPc2d1
povstání	povstání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherská	uherský	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
si	se	k3xPyFc3
mírem	mír	k1gInSc7
obhájila	obhájit	k5eAaPmAgFnS
svoje	svůj	k3xOyFgFnPc4
výsady	výsada	k1gFnPc4
<g/>
,	,	kIx,
Habsburkové	Habsburk	k1gMnPc1
se	se	k3xPyFc4
udrželi	udržet	k5eAaPmAgMnP
a	a	k8xC
navíc	navíc	k6eAd1
posílili	posílit	k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
výsostné	výsostný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
na	na	k7c6
uherském	uherský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Satmársky	Satmársky	k1gMnSc1
mier	mier	k1gMnSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
