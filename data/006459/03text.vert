<s>
Zápalky	zápalka	k1gFnPc1	zápalka
(	(	kIx(	(
<g/>
též	též	k9	též
sirky	sirka	k1gFnSc2	sirka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
podlouhlé	podlouhlý	k2eAgInPc4d1	podlouhlý
kousky	kousek	k1gInPc4	kousek
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
osikového	osikový	k2eAgInSc2d1	osikový
<g/>
,	,	kIx,	,
smrkového	smrkový	k2eAgInSc2d1	smrkový
či	či	k8xC	či
topolového	topolový	k2eAgInSc2d1	topolový
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
z	z	k7c2	z
lepenky	lepenka	k1gFnSc2	lepenka
se	s	k7c7	s
zápalnou	zápalný	k2eAgFnSc7d1	zápalná
látkou	látka	k1gFnSc7	látka
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
konců	konec	k1gInPc2	konec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
rozdělávání	rozdělávání	k1gNnSc3	rozdělávání
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Dřívka	dřívko	k1gNnPc1	dřívko
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
nasycena	nasytit	k5eAaPmNgFnS	nasytit
látkou	látka	k1gFnSc7	látka
usnadňující	usnadňující	k2eAgFnSc2d1	usnadňující
hoření	hoření	k2eAgFnSc2d1	hoření
<g/>
.	.	kIx.	.
</s>
<s>
Zápalná	zápalný	k2eAgFnSc1d1	zápalná
látka	látka	k1gFnSc1	látka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dřívka	dřívko	k1gNnSc2	dřívko
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
hlavička	hlavička	k1gFnSc1	hlavička
<g/>
,	,	kIx,	,
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tření	tření	k1gNnSc2	tření
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozdělávání	rozdělávání	k1gNnSc3	rozdělávání
ohně	oheň	k1gInSc2	oheň
našim	náš	k3xOp1gMnPc3	náš
prapředkům	prapředek	k1gMnPc3	prapředek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
sloužila	sloužit	k5eAaImAgFnS	sloužit
dvě	dva	k4xCgNnPc4	dva
vhodná	vhodný	k2eAgNnPc4d1	vhodné
dřívka	dřívko	k1gNnPc4	dřívko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
třela	třít	k5eAaImAgNnP	třít
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
až	až	k9	až
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
třecí	třecí	k2eAgFnPc1d1	třecí
plochy	plocha	k1gFnPc1	plocha
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
přiložili	přiložit	k5eAaPmAgMnP	přiložit
troud	troud	k1gInSc4	troud
a	a	k8xC	a
foukáním	foukání	k1gNnSc7	foukání
oheň	oheň	k1gInSc4	oheň
roznítili	roznítit	k5eAaPmAgMnP	roznítit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
u	u	k7c2	u
primitivních	primitivní	k2eAgInPc2d1	primitivní
národů	národ	k1gInPc2	národ
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
lidem	člověk	k1gMnPc3	člověk
podařilo	podařit	k5eAaPmAgNnS	podařit
vyvinout	vyvinout	k5eAaPmF	vyvinout
křesadlo	křesadlo	k1gNnSc1	křesadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vykřesaná	vykřesaný	k2eAgFnSc1d1	vykřesaná
jiskra	jiskra	k1gFnSc1	jiskra
zanítila	zanítit	k5eAaPmAgFnS	zanítit
hubku	hubka	k1gFnSc4	hubka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
lidé	člověk	k1gMnPc1	člověk
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
chemické	chemický	k2eAgInPc4d1	chemický
zapalovače	zapalovač	k1gInPc4	zapalovač
a	a	k8xC	a
sirky	sirka	k1gFnPc4	sirka
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
používaly	používat	k5eAaImAgFnP	používat
fosforové	fosforový	k2eAgFnPc1d1	fosforová
zápalky	zápalka	k1gFnPc1	zápalka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zapalovaly	zapalovat	k5eAaImAgFnP	zapalovat
při	při	k7c6	při
energickém	energický	k2eAgNnSc6d1	energické
škrtnutí	škrtnutí	k1gNnSc6	škrtnutí
o	o	k7c4	o
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
suchý	suchý	k2eAgInSc4d1	suchý
a	a	k8xC	a
drsný	drsný	k2eAgInSc4d1	drsný
povrch	povrch	k1gInSc4	povrch
(	(	kIx(	(
<g/>
např.	např.	kA	např.
o	o	k7c4	o
podrážku	podrážka	k1gFnSc4	podrážka
boty	bota	k1gFnSc2	bota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
byla	být	k5eAaImAgFnS	být
možnost	možnost	k1gFnSc1	možnost
nechtěného	chtěný	k2eNgNnSc2d1	nechtěné
samovznícení	samovznícení	k1gNnSc2	samovznícení
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
frankfurtský	frankfurtský	k2eAgMnSc1d1	frankfurtský
profesor	profesor	k1gMnSc1	profesor
chemie	chemie	k1gFnSc2	chemie
Rudolph	Rudolph	k1gMnSc1	Rudolph
Christian	Christian	k1gMnSc1	Christian
Boettger	Boettger	k1gMnSc1	Boettger
(	(	kIx(	(
<g/>
1806	[number]	k4	1806
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
zápalky	zápalka	k1gFnSc2	zápalka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zapalují	zapalovat	k5eAaImIp3nP	zapalovat
o	o	k7c4	o
škrtátko	škrtátko	k1gNnSc4	škrtátko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vynález	vynález	k1gInSc4	vynález
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nebyl	být	k5eNaImAgInS	být
zájem	zájem	k1gInSc1	zájem
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jej	on	k3xPp3gMnSc4	on
koupili	koupit	k5eAaPmAgMnP	koupit
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
.	.	kIx.	.
</s>
<s>
Švéd	Švéd	k1gMnSc1	Švéd
Johan	Johan	k1gMnSc1	Johan
Edward	Edward	k1gMnSc1	Edward
Lundström	Lundström	k1gMnSc1	Lundström
zápalky	zápalka	k1gFnSc2	zápalka
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
<g/>
,	,	kIx,	,
opatřil	opatřit	k5eAaPmAgMnS	opatřit
i	i	k9	i
zasouvací	zasouvací	k2eAgFnSc7d1	zasouvací
krabičkou	krabička	k1gFnSc7	krabička
<g/>
,	,	kIx,	,
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
hlavičky	hlavička	k1gFnPc1	hlavička
ze	z	k7c2	z
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
bílý	bílý	k2eAgInSc4d1	bílý
fosfor	fosfor	k1gInSc4	fosfor
nebo	nebo	k8xC	nebo
sulfid	sulfid	k1gInSc4	sulfid
fosforu	fosfor	k1gInSc2	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
fosfor	fosfor	k1gInSc1	fosfor
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zápalek	zápalka	k1gFnPc2	zápalka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
výrobcem	výrobce	k1gMnSc7	výrobce
zápalek	zápalka	k1gFnPc2	zápalka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
podnik	podnik	k1gInSc1	podnik
SOLO	SOLO	kA	SOLO
Sušice	Sušice	k1gFnSc1	Sušice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
většina	většina	k1gFnSc1	většina
zápalek	zápalka	k1gFnPc2	zápalka
chytá	chytat	k5eAaImIp3nS	chytat
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
patřičně	patřičně	k6eAd1	patřičně
energického	energický	k2eAgNnSc2d1	energické
škrtnutí	škrtnutí	k1gNnSc2	škrtnutí
hlavičky	hlavička	k1gFnSc2	hlavička
o	o	k7c4	o
speciálně	speciálně	k6eAd1	speciálně
připravenou	připravený	k2eAgFnSc4d1	připravená
plochu	plocha	k1gFnSc4	plocha
–	–	k?	–
škrtátko	škrtátko	k1gNnSc4	škrtátko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
prakticky	prakticky	k6eAd1	prakticky
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
samovolné	samovolný	k2eAgNnSc4d1	samovolné
či	či	k8xC	či
nekontrolované	kontrolovaný	k2eNgNnSc4d1	nekontrolované
vzplanutí	vzplanutí	k1gNnSc4	vzplanutí
zápalky	zápalka	k1gFnSc2	zápalka
náhodným	náhodný	k2eAgNnSc7d1	náhodné
otřením	otření	k1gNnSc7	otření
zápalky	zápalka	k1gFnSc2	zápalka
o	o	k7c4	o
jiný	jiný	k2eAgInSc4d1	jiný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Hlavička	hlavička	k1gFnSc1	hlavička
zápalek	zápalka	k1gFnPc2	zápalka
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
skládá	skládat	k5eAaImIp3nS	skládat
zejména	zejména	k9	zejména
z	z	k7c2	z
chlorečnanu	chlorečnan	k1gInSc2	chlorečnan
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
sulfidu	sulfid	k1gInSc2	sulfid
antimonitého	antimonitý	k2eAgInSc2d1	antimonitý
<g/>
,	,	kIx,	,
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
barviva	barvivo	k1gNnSc2	barvivo
a	a	k8xC	a
mletého	mletý	k2eAgNnSc2d1	mleté
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
hlavičce	hlavička	k1gFnSc3	hlavička
drsnost	drsnost	k1gFnSc4	drsnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
tření	tření	k1gNnSc1	tření
<g/>
.	.	kIx.	.
</s>
<s>
Dřívka	dřívko	k1gNnSc2	dřívko
zápalek	zápalka	k1gFnPc2	zápalka
jsou	být	k5eAaImIp3nP	být
nasycena	nasycen	k2eAgNnPc4d1	nasyceno
tekutým	tekutý	k2eAgInSc7d1	tekutý
parafínem	parafín	k1gInSc7	parafín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
hoření	hoření	k2eAgInSc1d1	hoření
a	a	k8xC	a
fosforečnanem	fosforečnan	k1gInSc7	fosforečnan
sodným	sodný	k2eAgInSc7d1	sodný
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
doutnání	doutnání	k1gNnSc4	doutnání
zápalky	zápalka	k1gFnSc2	zápalka
po	po	k7c6	po
zhasnutí	zhasnutí	k1gNnSc6	zhasnutí
plamene	plamen	k1gInSc2	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Škrtátko	škrtátko	k1gNnSc1	škrtátko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
červený	červený	k2eAgInSc4d1	červený
fosfor	fosfor	k1gInSc4	fosfor
<g/>
,	,	kIx,	,
mleté	mletý	k2eAgNnSc4d1	mleté
sklo	sklo	k1gNnSc4	sklo
a	a	k8xC	a
pojivo	pojivo	k1gNnSc4	pojivo
<g/>
.	.	kIx.	.
</s>
<s>
Škrtnutím	škrtnutí	k1gNnSc7	škrtnutí
zápalky	zápalka	k1gFnSc2	zápalka
o	o	k7c4	o
škrtátko	škrtátko	k1gNnSc4	škrtátko
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
na	na	k7c6	na
styčném	styčný	k2eAgInSc6d1	styčný
bodu	bod	k1gInSc6	bod
teplota	teplota	k1gFnSc1	teplota
asi	asi	k9	asi
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
1100	[number]	k4	1100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
postačí	postačit	k5eAaPmIp3nS	postačit
pro	pro	k7c4	pro
zapálení	zapálení	k1gNnSc4	zapálení
hlavičky	hlavička	k1gFnSc2	hlavička
a	a	k8xC	a
následně	následně	k6eAd1	následně
dřívka	dřívko	k1gNnSc2	dřívko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
zápalky	zápalka	k1gFnSc2	zápalka
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnPc1d1	standardní
malé	malý	k2eAgFnPc1d1	malá
zápalky	zápalka	k1gFnPc1	zápalka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kuřácké	kuřácký	k2eAgInPc1d1	kuřácký
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
cca	cca	kA	cca
4	[number]	k4	4
cm	cm	kA	cm
a	a	k8xC	a
prodávají	prodávat	k5eAaImIp3nP	prodávat
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
papírových	papírový	k2eAgFnPc6d1	papírová
krabičkách	krabička	k1gFnPc6	krabička
s	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
x	x	k?	x
3,5	[number]	k4	3,5
x	x	k?	x
1,5	[number]	k4	1,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
se	s	k7c7	s
škrtátkem	škrtátko	k1gNnSc7	škrtátko
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
krabičky	krabička	k1gFnSc2	krabička
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
zápalek	zápalka	k1gFnPc2	zápalka
v	v	k7c6	v
krabičce	krabička	k1gFnSc6	krabička
bývá	bývat	k5eAaImIp3nS	bývat
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Ploché	plochý	k2eAgFnPc1d1	plochá
zápalky	zápalka	k1gFnPc1	zápalka
odlamovací	odlamovací	k2eAgFnPc1d1	odlamovací
(	(	kIx(	(
<g/>
knížečkové	knížečková	k1gFnPc1	knížečková
<g/>
)	)	kIx)	)
–	–	k?	–
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
nebo	nebo	k8xC	nebo
papírové	papírový	k2eAgFnSc2d1	papírová
zápalky	zápalka	k1gFnSc2	zápalka
jsou	být	k5eAaImIp3nP	být
vlepené	vlepený	k2eAgFnPc1d1	vlepená
do	do	k7c2	do
plochého	plochý	k2eAgInSc2d1	plochý
papírového	papírový	k2eAgInSc2d1	papírový
přebalu	přebal	k1gInSc2	přebal
<g/>
,	,	kIx,	,
zápalka	zápalka	k1gFnSc1	zápalka
se	se	k3xPyFc4	se
před	před	k7c7	před
použitím	použití	k1gNnSc7	použití
odlomí	odlomit	k5eAaPmIp3nS	odlomit
či	či	k8xC	či
odtrhne	odtrhnout	k5eAaPmIp3nS	odtrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
například	například	k6eAd1	například
v	v	k7c6	v
USA	USA	kA	USA
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
reklamní	reklamní	k2eAgInPc1d1	reklamní
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnPc1d1	standardní
prodloužené	prodloužená	k1gFnPc1	prodloužená
nebo	nebo	k8xC	nebo
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
zápalky	zápalka	k1gFnPc1	zápalka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
domácnostní	domácnostní	k2eAgInPc1d1	domácnostní
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInPc1d2	delší
(	(	kIx(	(
<g/>
cca	cca	kA	cca
5	[number]	k4	5
cm	cm	kA	cm
<g/>
)	)	kIx)	)
–	–	k?	–
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
usnadnění	usnadnění	k1gNnSc2	usnadnění
zapálení	zapálení	k1gNnSc2	zapálení
hořáku	hořák	k1gInSc2	hořák
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
stojí	stát	k5eAaImIp3nS	stát
např.	např.	kA	např.
hrnec	hrnec	k1gInSc1	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Prodávají	prodávat	k5eAaImIp3nP	prodávat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
ve	v	k7c6	v
standardních	standardní	k2eAgFnPc6d1	standardní
krabičkách	krabička	k1gFnPc6	krabička
s	s	k7c7	s
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
délkou	délka	k1gFnSc7	délka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
krabičkách	krabička	k1gFnPc6	krabička
(	(	kIx(	(
<g/>
např.	např.	kA	např.
8	[number]	k4	8
×	×	k?	×
5,5	[number]	k4	5,5
×	×	k?	×
3	[number]	k4	3
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
zápalek	zápalka	k1gFnPc2	zápalka
<g/>
.	.	kIx.	.
</s>
<s>
Krbové	krbový	k2eAgFnPc1d1	krbová
zápalky	zápalka	k1gFnPc1	zápalka
jsou	být	k5eAaImIp3nP	být
prodloužené	prodloužená	k1gFnSc3	prodloužená
na	na	k7c4	na
cca	cca	kA	cca
20	[number]	k4	20
cm	cm	kA	cm
(	(	kIx(	(
<g/>
účel	účel	k1gInSc1	účel
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
názvu	název	k1gInSc2	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krabičky	krabička	k1gFnPc1	krabička
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
však	však	k9	však
bývají	bývat	k5eAaImIp3nP	bývat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
úzké	úzký	k2eAgFnPc1d1	úzká
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečnostní	bezpečnostní	k2eNgFnSc1d1	bezpečnostní
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kovbojské	kovbojský	k2eAgInPc1d1	kovbojský
<g/>
)	)	kIx)	)
zápalky	zápalka	k1gFnSc2	zápalka
–	–	k?	–
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
škrtátko	škrtátko	k1gNnSc1	škrtátko
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
škrtat	škrtat	k5eAaImF	škrtat
o	o	k7c4	o
různé	různý	k2eAgInPc4d1	různý
drsné	drsný	k2eAgInPc4d1	drsný
povrchy	povrch	k1gInPc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Voděodolné	Voděodolný	k2eAgFnPc1d1	Voděodolná
zápalky	zápalka	k1gFnPc1	zápalka
–	–	k?	–
opatřené	opatřený	k2eAgFnPc1d1	opatřená
impregnací	impregnace	k1gFnSc7	impregnace
proti	proti	k7c3	proti
vlhkosti	vlhkost	k1gFnSc3	vlhkost
<g/>
;	;	kIx,	;
impregnovat	impregnovat	k5eAaBmF	impregnovat
lze	lze	k6eAd1	lze
i	i	k9	i
běžné	běžný	k2eAgFnSc2d1	běžná
zápalky	zápalka	k1gFnSc2	zápalka
například	například	k6eAd1	například
ponořením	ponoření	k1gNnSc7	ponoření
do	do	k7c2	do
rozehřátého	rozehřátý	k2eAgInSc2d1	rozehřátý
parafínu	parafín	k1gInSc2	parafín
<g/>
.	.	kIx.	.
</s>
<s>
Větruodolné	Větruodolný	k2eAgFnPc1d1	Větruodolná
zápalky	zápalka	k1gFnPc1	zápalka
–	–	k?	–
s	s	k7c7	s
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
hlavičkou	hlavička	k1gFnSc7	hlavička
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
zápalek	zápalka	k1gFnPc2	zápalka
<g/>
,	,	kIx,	,
vyznačujících	vyznačující	k2eAgFnPc2d1	vyznačující
se	s	k7c7	s
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
rozměry	rozměr	k1gInPc7	rozměr
nebo	nebo	k8xC	nebo
balením	balení	k1gNnSc7	balení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
často	často	k6eAd1	často
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
reklamním	reklamní	k2eAgInPc3d1	reklamní
účelům	účel	k1gInPc3	účel
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
upomínkové	upomínkový	k2eAgInPc4d1	upomínkový
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Zápalky	zápalka	k1gFnPc1	zápalka
slepované	slepovaný	k2eAgFnPc1d1	slepovaná
lepidlem	lepidlo	k1gNnSc7	lepidlo
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
modelů	model	k1gInPc2	model
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
kuriózních	kuriózní	k2eAgInPc2d1	kuriózní
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
například	například	k6eAd1	například
Tomáš	Tomáš	k1gMnSc1	Tomáš
Korda	Korda	k1gMnSc1	Korda
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
dílo	dílo	k1gNnSc1	dílo
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
Muzeum	muzeum	k1gNnSc4	muzeum
rekordů	rekord	k1gInPc2	rekord
a	a	k8xC	a
kuriozit	kuriozita	k1gFnPc2	kuriozita
v	v	k7c6	v
Pelhřimově	Pelhřimov	k1gInSc6	Pelhřimov
<g/>
.	.	kIx.	.
</s>
<s>
Zápalka	zápalka	k1gFnSc1	zápalka
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgInPc2d1	používaný
předmětů	předmět	k1gInPc2	předmět
ilustrujících	ilustrující	k2eAgInPc2d1	ilustrující
měřítko	měřítko	k1gNnSc4	měřítko
velikosti	velikost	k1gFnSc2	velikost
v	v	k7c6	v
makrofotografii	makrofotografie	k1gFnSc6	makrofotografie
<g/>
.	.	kIx.	.
</s>
<s>
Filumenie	filumenie	k1gFnSc1	filumenie
–	–	k?	–
zápalky	zápalka	k1gFnSc2	zápalka
a	a	k8xC	a
obzvláště	obzvláště	k6eAd1	obzvláště
nálepky	nálepek	k1gInPc1	nálepek
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
krabiček	krabička	k1gFnPc2	krabička
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
sběratelství	sběratelství	k1gNnSc2	sběratelství
<g/>
.	.	kIx.	.
</s>
