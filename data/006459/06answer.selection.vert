<s>
Zápalky	zápalka	k1gFnPc1	zápalka
(	(	kIx(	(
<g/>
též	též	k9	též
sirky	sirka	k1gFnSc2	sirka
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
podlouhlé	podlouhlý	k2eAgInPc4d1	podlouhlý
kousky	kousek	k1gInPc4	kousek
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
osikového	osikový	k2eAgInSc2d1	osikový
<g/>
,	,	kIx,	,
smrkového	smrkový	k2eAgInSc2d1	smrkový
či	či	k8xC	či
topolového	topolový	k2eAgInSc2d1	topolový
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
z	z	k7c2	z
lepenky	lepenka	k1gFnSc2	lepenka
se	s	k7c7	s
zápalnou	zápalný	k2eAgFnSc7d1	zápalná
látkou	látka	k1gFnSc7	látka
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
konců	konec	k1gInPc2	konec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
rozdělávání	rozdělávání	k1gNnSc3	rozdělávání
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
