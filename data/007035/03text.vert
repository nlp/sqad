<s>
Špinavá	špinavý	k2eAgFnSc1d1	špinavá
bomba	bomba	k1gFnSc1	bomba
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgNnSc4d1	způsobující
radioaktivní	radioaktivní	k2eAgNnSc4d1	radioaktivní
zamoření	zamoření	k1gNnSc4	zamoření
rozmetáním	rozmetání	k1gNnSc7	rozmetání
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
klasickou	klasický	k2eAgFnSc7d1	klasická
výbušninou	výbušnina	k1gFnSc7	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
nálože	nálož	k1gFnSc2	nálož
klasické	klasický	k2eAgFnSc2d1	klasická
trhaviny	trhavina	k1gFnSc2	trhavina
a	a	k8xC	a
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
materiálu	materiál	k1gInSc2	materiál
–	–	k?	–
například	například	k6eAd1	například
vyhořelého	vyhořelý	k2eAgNnSc2d1	vyhořelé
paliva	palivo	k1gNnSc2	palivo
z	z	k7c2	z
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
nebo	nebo	k8xC	nebo
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
a	a	k8xC	a
medicínských	medicínský	k2eAgInPc2d1	medicínský
zářičů	zářič	k1gInPc2	zářič
<g/>
.	.	kIx.	.
</s>
<s>
Špinavá	špinavý	k2eAgFnSc1d1	špinavá
bomba	bomba	k1gFnSc1	bomba
funguje	fungovat	k5eAaImIp3nS	fungovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
výbuch	výbuch	k1gInSc1	výbuch
klasické	klasický	k2eAgFnSc2d1	klasická
trhaviny	trhavina	k1gFnSc2	trhavina
rozpráší	rozprášit	k5eAaPmIp3nP	rozprášit
radioaktivní	radioaktivní	k2eAgFnPc1d1	radioaktivní
látky	látka	k1gFnPc1	látka
do	do	k7c2	do
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
zamoření	zamoření	k1gNnSc1	zamoření
závisí	záviset	k5eAaImIp3nS	záviset
zejména	zejména	k9	zejména
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
výšce	výška	k1gFnSc6	výška
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
odpálení	odpálení	k1gNnSc3	odpálení
<g/>
,	,	kIx,	,
rychlosti	rychlost	k1gFnSc3	rychlost
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
okolním	okolní	k2eAgInSc6d1	okolní
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
sestrojit	sestrojit	k5eAaPmF	sestrojit
podstatně	podstatně	k6eAd1	podstatně
snáze	snadno	k6eAd2	snadno
<g/>
,	,	kIx,	,
než	než	k8xS	než
klasickou	klasický	k2eAgFnSc4d1	klasická
jadernou	jaderný	k2eAgFnSc4d1	jaderná
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
zbraň	zbraň	k1gFnSc4	zbraň
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
speciální	speciální	k2eAgInSc1d1	speciální
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
materiál	materiál	k1gInSc1	materiál
vysoké	vysoký	k2eAgFnSc2d1	vysoká
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
konstrukce	konstrukce	k1gFnSc1	konstrukce
klasické	klasický	k2eAgFnSc2d1	klasická
jaderné	jaderný	k2eAgFnSc2d1	jaderná
bomby	bomba	k1gFnSc2	bomba
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
složitější	složitý	k2eAgMnSc1d2	složitější
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Panuje	panovat	k5eAaImIp3nS	panovat
proto	proto	k8xC	proto
obava	obava	k1gFnSc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
špinavá	špinavý	k2eAgFnSc1d1	špinavá
bomba	bomba	k1gFnSc1	bomba
může	moct	k5eAaImIp3nS	moct
stát	stát	k1gInSc1	stát
zbraní	zbraň	k1gFnPc2	zbraň
teroristů	terorista	k1gMnPc2	terorista
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobit	vyrobit	k5eAaPmF	vyrobit
špinavou	špinavý	k2eAgFnSc4d1	špinavá
bombu	bomba	k1gFnSc4	bomba
se	se	k3xPyFc4	se
pokusily	pokusit	k5eAaPmAgInP	pokusit
jak	jak	k8xS	jak
USA	USA	kA	USA
tak	tak	k6eAd1	tak
i	i	k9	i
Irák	Irák	k1gInSc1	Irák
a	a	k8xC	a
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
zemích	zem	k1gFnPc6	zem
experti	expert	k1gMnPc1	expert
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozsah	rozsah	k1gInSc1	rozsah
rozšíření	rozšíření	k1gNnSc2	rozšíření
radiace	radiace	k1gFnSc2	radiace
je	být	k5eAaImIp3nS	být
mizivý	mizivý	k2eAgInSc1d1	mizivý
<g/>
.	.	kIx.	.
</s>
<s>
Odpálení	odpálení	k1gNnSc1	odpálení
takovéto	takovýto	k3xDgFnSc2	takovýto
zbraně	zbraň	k1gFnSc2	zbraň
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
patrně	patrně	k6eAd1	patrně
jen	jen	k9	jen
velký	velký	k2eAgInSc4d1	velký
psychologický	psychologický	k2eAgInSc4d1	psychologický
efekt	efekt	k1gInSc4	efekt
na	na	k7c4	na
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
rentability	rentabilita	k1gFnSc2	rentabilita
následků	následek	k1gInPc2	následek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
proto	proto	k8xC	proto
pro	pro	k7c4	pro
teroristy	terorista	k1gMnPc4	terorista
lepší	lepšit	k5eAaImIp3nP	lepšit
použít	použít	k5eAaPmF	použít
chemické	chemický	k2eAgFnPc1d1	chemická
nebo	nebo	k8xC	nebo
biologické	biologický	k2eAgFnPc1d1	biologická
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Špinavá	špinavý	k2eAgFnSc1d1	špinavá
bomba	bomba	k1gFnSc1	bomba
na	na	k7c4	na
military	militar	k1gMnPc4	militar
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
