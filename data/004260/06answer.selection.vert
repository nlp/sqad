<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
architektonické	architektonický	k2eAgFnSc2d1	architektonická
soutěže	soutěž	k1gFnSc2	soutěž
nová	nový	k2eAgFnSc1d1	nová
pozdemní	pozdemní	k2eAgFnSc1d1	pozdemní
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
hala	hala	k1gFnSc1	hala
(	(	kIx(	(
<g/>
slohově	slohově	k6eAd1	slohově
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
řadit	řadit	k5eAaImF	řadit
k	k	k7c3	k
brutalismu	brutalismus	k1gInSc3	brutalismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
část	část	k1gFnSc1	část
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
sadů	sad	k1gInPc2	sad
<g/>
.	.	kIx.	.
</s>
