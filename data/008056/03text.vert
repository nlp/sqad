<s>
Prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
předseda	předseda	k1gMnSc1	předseda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc2d1	obchodní
společnosti	společnost	k1gFnSc2	společnost
nebo	nebo	k8xC	nebo
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
označuje	označovat	k5eAaImIp3nS	označovat
hlavu	hlava	k1gFnSc4	hlava
státu	stát	k1gInSc2	stát
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
nepřímo	přímo	k6eNd1	přímo
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
obyvateli	obyvatel	k1gMnPc7	obyvatel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Prezidenti	prezident	k1gMnPc1	prezident
často	často	k6eAd1	často
stojí	stát	k5eAaImIp3nP	stát
i	i	k9	i
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
různých	různý	k2eAgInPc2d1	různý
diktátorských	diktátorský	k2eAgInPc2d1	diktátorský
režimů	režim	k1gInPc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
titulem	titul	k1gInSc7	titul
hlav	hlava	k1gFnPc2	hlava
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
volené	volený	k2eAgFnPc1d1	volená
hlavy	hlava	k1gFnPc1	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
zastávali	zastávat	k5eAaImAgMnP	zastávat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
výhradně	výhradně	k6eAd1	výhradně
jen	jen	k9	jen
muži	muž	k1gMnPc1	muž
(	(	kIx(	(
<g/>
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
manžela	manžel	k1gMnSc2	manžel
Juana	Juan	k1gMnSc2	Juan
Perona	Peron	k1gMnSc2	Peron
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
prezidentkou	prezidentka	k1gFnSc7	prezidentka
jeho	jeho	k3xOp3gFnSc1	jeho
třetí	třetí	k4xOgFnSc1	třetí
žena	žena	k1gFnSc1	žena
Isabela	Isabela	k1gFnSc1	Isabela
Perón	perón	k1gInSc4	perón
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Peron	peron	k1gInSc1	peron
dosadil	dosadit	k5eAaPmAgInS	dosadit
na	na	k7c4	na
post	post	k1gInSc4	post
viceprezidentky	viceprezidentka	k1gFnSc2	viceprezidentka
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
demokraticky	demokraticky	k6eAd1	demokraticky
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
Vigdis	Vigdis	k1gFnSc2	Vigdis
Finnbogadóttir	Finnbogadóttira	k1gFnPc2	Finnbogadóttira
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
oddělení	oddělení	k1gNnSc4	oddělení
státních	státní	k2eAgFnPc2d1	státní
mocí	moc	k1gFnPc2	moc
-	-	kIx~	-
soudní	soudní	k2eAgFnSc2d1	soudní
<g/>
,	,	kIx,	,
výkonné	výkonný	k2eAgFnSc2d1	výkonná
i	i	k8xC	i
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
moci	moc	k1gFnSc2	moc
výkonné	výkonný	k2eAgFnSc2d1	výkonná
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
zároveň	zároveň	k6eAd1	zároveň
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
na	na	k7c6	na
parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
pravomoci	pravomoc	k1gFnPc4	pravomoc
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
parlamentu	parlament	k1gInSc3	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
občany	občan	k1gMnPc4	občan
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Soustředění	soustředění	k1gNnSc1	soustředění
takového	takový	k3xDgNnSc2	takový
množství	množství	k1gNnSc2	množství
moci	moct	k5eAaImF	moct
do	do	k7c2	do
jedněch	jeden	k4xCgFnPc2	jeden
rukou	ruka	k1gFnPc2	ruka
však	však	k9	však
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
vzniku	vznik	k1gInSc2	vznik
autoritativního	autoritativní	k2eAgInSc2d1	autoritativní
státu	stát	k1gInSc2	stát
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
diktatury	diktatura	k1gFnSc2	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
nebývá	bývat	k5eNaImIp3nS	bývat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
formou	forma	k1gFnSc7	forma
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentní	parlamentní	k2eAgFnSc6d1	parlamentní
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
reprezentantem	reprezentant	k1gInSc7	reprezentant
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
státu	stát	k1gInSc2	stát
navenek	navenek	k6eAd1	navenek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
vnitrostátní	vnitrostátní	k2eAgFnPc1d1	vnitrostátní
pravomoci	pravomoc	k1gFnPc1	pravomoc
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgFnPc1d1	omezená
a	a	k8xC	a
často	často	k6eAd1	často
jen	jen	k9	jen
formální	formální	k2eAgMnSc1d1	formální
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
pravomoc	pravomoc	k1gFnSc4	pravomoc
rozpustit	rozpustit	k5eAaPmF	rozpustit
parlament	parlament	k1gInSc4	parlament
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
jen	jen	k9	jen
v	v	k7c6	v
ústavně	ústavně	k6eAd1	ústavně
stanovených	stanovený	k2eAgInPc6d1	stanovený
případech	případ	k1gInPc6	případ
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
nové	nový	k2eAgFnPc4d1	nová
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
většinou	většinou	k6eAd1	většinou
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Poloprezidentská	Poloprezidentský	k2eAgFnSc1d1	Poloprezidentský
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Poloprezidentská	Poloprezidentský	k2eAgFnSc1d1	Poloprezidentský
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
prvky	prvek	k1gInPc4	prvek
obou	dva	k4xCgInPc2	dva
předchozích	předchozí	k2eAgInPc2d1	předchozí
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
pravomoci	pravomoc	k1gFnPc4	pravomoc
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
zákonodárné	zákonodárný	k2eAgFnSc3d1	zákonodárná
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
k	k	k7c3	k
zákonům	zákon	k1gInPc3	zákon
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
předkládat	předkládat	k5eAaImF	předkládat
zákony	zákon	k1gInPc4	zákon
parlamentu	parlament	k1gInSc2	parlament
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
občanům	občan	k1gMnPc3	občan
formou	forma	k1gFnSc7	forma
referenda	referendum	k1gNnSc2	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
poloprezidentské	poloprezidentský	k2eAgFnPc4d1	poloprezidentská
republiky	republika	k1gFnPc4	republika
je	být	k5eAaImIp3nS	být
ústavní	ústavní	k2eAgInSc1d1	ústavní
systém	systém	k1gInSc1	systém
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
poloprezidentská	poloprezidentský	k2eAgFnSc1d1	poloprezidentská
je	být	k5eAaImIp3nS	být
i	i	k9	i
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diktatura	diktatura	k1gFnSc1	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
nedemokratických	demokratický	k2eNgInPc6d1	nedemokratický
režimech	režim	k1gInPc6	režim
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většinou	k6eAd1	většinou
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
sám	sám	k3xTgMnSc1	sám
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
si	se	k3xPyFc3	se
svévolně	svévolně	k6eAd1	svévolně
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
formou	forma	k1gFnSc7	forma
referenda	referendum	k1gNnSc2	referendum
<g/>
)	)	kIx)	)
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
svoje	svůj	k3xOyFgNnPc4	svůj
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
nebo	nebo	k8xC	nebo
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
doživotním	doživotní	k2eAgMnSc7d1	doživotní
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentské	prezidentský	k2eAgFnPc1d1	prezidentská
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
nekonají	konat	k5eNaImIp3nP	konat
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
diktátor	diktátor	k1gMnSc1	diktátor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
jediným	jediný	k2eAgMnSc7d1	jediný
kandidátem	kandidát	k1gMnSc7	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
drží	držet	k5eAaImIp3nS	držet
veškerou	veškerý	k3xTgFnSc4	veškerý
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
jen	jen	k6eAd1	jen
nesamostatná	samostatný	k2eNgFnSc1d1	nesamostatná
figurka	figurka	k1gFnSc1	figurka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
v	v	k7c6	v
moci	moc	k1gFnSc6	moc
diktátora	diktátor	k1gMnSc2	diktátor
nebo	nebo	k8xC	nebo
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
pravomoci	pravomoc	k1gFnPc1	pravomoc
prakticky	prakticky	k6eAd1	prakticky
nulové	nulový	k2eAgFnPc1d1	nulová
<g/>
.	.	kIx.	.
</s>
