<s>
První	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
demokraticky	demokraticky	k6eAd1	demokraticky
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
Vigdis	Vigdis	k1gFnSc2	Vigdis
Finnbogadóttir	Finnbogadóttira	k1gFnPc2	Finnbogadóttira
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
<g/>
.	.	kIx.	.
</s>
