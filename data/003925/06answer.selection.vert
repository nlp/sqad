<s>
Singapur	Singapur	k1gInSc1	Singapur
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
Malajské	malajský	k2eAgFnSc2d1	malajská
federace	federace	k1gFnSc2	federace
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
závažným	závažný	k2eAgFnPc3d1	závažná
neshodám	neshoda	k1gFnPc3	neshoda
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
dodnes	dodnes	k6eAd1	dodnes
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
