<s>
Ornithobakterióza	Ornithobakterióza	k1gFnSc1	Ornithobakterióza
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
respirační	respirační	k2eAgNnSc1d1	respirační
onemocnění	onemocnění	k1gNnSc1	onemocnění
hrabavé	hrabavý	k2eAgFnSc2d1	hrabavá
drůbeže	drůbež	k1gFnSc2	drůbež
vyvolávané	vyvolávaný	k2eAgFnSc2d1	vyvolávaná
bakterií	bakterie	k1gFnSc7	bakterie
Ornithobacterium	Ornithobacterium	k1gNnSc1	Ornithobacterium
rhinotracheale	rhinotracheale	k6eAd1	rhinotracheale
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
zhoršeným	zhoršený	k2eAgInSc7d1	zhoršený
růstem	růst	k1gInSc7	růst
<g/>
,	,	kIx,	,
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
mortalitou	mortalita	k1gFnSc7	mortalita
<g/>
,	,	kIx,	,
zánětem	zánět	k1gInSc7	zánět
průdušek	průduška	k1gFnPc2	průduška
<g/>
,	,	kIx,	,
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
pohrudnice	pohrudnice	k1gFnSc2	pohrudnice
a	a	k8xC	a
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
význam	význam	k1gInSc1	význam
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zejména	zejména	k9	zejména
v	v	k7c6	v
rozmnožovacích	rozmnožovací	k2eAgInPc6d1	rozmnožovací
chovech	chov	k1gInPc6	chov
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Izolace	izolace	k1gFnPc1	izolace
a	a	k8xC	a
identifikace	identifikace	k1gFnPc1	identifikace
bakterií	bakterie	k1gFnSc7	bakterie
Ornithobacterium	Ornithobacterium	k1gNnSc1	Ornithobacterium
rhinotracheale	rhinotracheale	k6eAd1	rhinotracheale
(	(	kIx(	(
<g/>
ORT	ort	k1gInSc1	ort
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
její	její	k3xOp3gFnSc4	její
etiologickou	etiologický	k2eAgFnSc4d1	etiologická
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
respiračním	respirační	k2eAgNnSc6d1	respirační
onemocnění	onemocnění	k1gNnSc6	onemocnění
kura	kur	k1gMnSc2	kur
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
prokázat	prokázat	k5eAaPmF	prokázat
teprve	teprve	k6eAd1	teprve
nedávno	nedávno	k6eAd1	nedávno
(	(	kIx(	(
<g/>
Vandamme	Vandamme	k1gFnSc1	Vandamme
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
hrabavé	hrabavý	k2eAgFnSc2d1	hrabavá
drůbeže	drůbež	k1gFnSc2	drůbež
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
sekundární	sekundární	k2eAgMnSc1d1	sekundární
nebo	nebo	k8xC	nebo
oportunní	oportunní	k2eAgInSc1d1	oportunní
patogen	patogen	k1gInSc1	patogen
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
byla	být	k5eAaImAgFnS	být
izolována	izolovat	k5eAaBmNgFnS	izolovat
u	u	k7c2	u
5	[number]	k4	5
<g/>
týdenních	týdenní	k2eAgFnPc2d1	týdenní
krůt	krůta	k1gFnPc2	krůta
s	s	k7c7	s
respiračními	respirační	k2eAgInPc7d1	respirační
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k8xC	jako
Pasteurella-like	Pasteurellaike	k1gFnSc1	Pasteurella-like
organismus	organismus	k1gInSc1	organismus
(	(	kIx(	(
<g/>
Hinz	Hinz	k1gInSc1	Hinz
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
byly	být	k5eAaImAgInP	být
identifikovány	identifikován	k2eAgInPc1d1	identifikován
další	další	k2eAgInPc1d1	další
izoláty	izolát	k1gInPc1	izolát
získané	získaný	k2eAgInPc1d1	získaný
z	z	k7c2	z
postižených	postižený	k2eAgInPc2d1	postižený
chovů	chov	k1gInPc2	chov
krůt	krůta	k1gFnPc2	krůta
nebo	nebo	k8xC	nebo
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
USA	USA	kA	USA
či	či	k8xC	či
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
bylo	být	k5eAaImAgNnS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
také	také	k9	také
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
mírnou	mírný	k2eAgFnSc4d1	mírná
formu	forma	k1gFnSc4	forma
cholery	cholera	k1gFnSc2	cholera
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
infekce	infekce	k1gFnSc2	infekce
Ornithobacterium	Ornithobacterium	k1gNnSc1	Ornithobacterium
sp	sp	k?	sp
<g/>
.	.	kIx.	.
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
Ornithobacterium	Ornithobacterium	k1gNnSc1	Ornithobacterium
rhinotracheale	rhinotracheale	k6eAd1	rhinotracheale
je	být	k5eAaImIp3nS	být
gramnegativní	gramnegativní	k2eAgFnSc1d1	gramnegativní
<g/>
,	,	kIx,	,
pleomorfní	pleomorfní	k2eAgFnSc1d1	pleomorfní
a	a	k8xC	a
nepohyblivá	pohyblivý	k2eNgFnSc1d1	nepohyblivá
tyčinka	tyčinka	k1gFnSc1	tyčinka
průměrné	průměrný	k2eAgFnSc2d1	průměrná
velikosti	velikost	k1gFnSc2	velikost
0,2	[number]	k4	0,2
<g/>
-	-	kIx~	-
<g/>
0,9	[number]	k4	0,9
x	x	k?	x
1-3	[number]	k4	1-3
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mezofilní	mezofilní	k2eAgNnSc1d1	mezofilní
<g/>
,	,	kIx,	,
s	s	k7c7	s
chemoorganotrofním	chemoorganotrofní	k2eAgInSc7d1	chemoorganotrofní
metabolismem	metabolismus	k1gInSc7	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
ORT	ort	k1gInSc1	ort
roste	růst	k5eAaImIp3nS	růst
mikroaerofilně	mikroaerofilně	k6eAd1	mikroaerofilně
nebo	nebo	k8xC	nebo
anaerobně	anaerobně	k6eAd1	anaerobně
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
ale	ale	k8xC	ale
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
7,5	[number]	k4	7,5
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
%	%	kIx~	%
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
teplot	teplota	k1gFnPc2	teplota
30-42	[number]	k4	30-42
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
krevním	krevní	k2eAgInSc6d1	krevní
nebo	nebo	k8xC	nebo
čokoládovém	čokoládový	k2eAgInSc6d1	čokoládový
agaru	agar	k1gInSc6	agar
z	z	k7c2	z
ovčí	ovčí	k2eAgFnSc2d1	ovčí
nebo	nebo	k8xC	nebo
koňské	koňský	k2eAgFnSc2d1	koňská
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
48	[number]	k4	48
hod	hod	k1gInSc4	hod
inkubaci	inkubace	k1gFnSc4	inkubace
vznikají	vznikat	k5eAaImIp3nP	vznikat
drobné	drobný	k2eAgInPc1d1	drobný
(	(	kIx(	(
<g/>
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
1-2	[number]	k4	1-2
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kulaté	kulatý	k2eAgFnSc2d1	kulatá
<g/>
,	,	kIx,	,
konvexní	konvexní	k2eAgFnSc2d1	konvexní
a	a	k8xC	a
šedobílé	šedobílý	k2eAgFnSc2d1	šedobílá
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Kolonie	kolonie	k1gFnSc1	kolonie
po	po	k7c4	po
24	[number]	k4	24
hod	hod	k1gInSc4	hod
růstu	růst	k1gInSc2	růst
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
(	(	kIx(	(
<g/>
<	<	kIx(	<
1	[number]	k4	1
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přehlédnuty	přehlédnout	k5eAaPmNgFnP	přehlédnout
nebo	nebo	k8xC	nebo
maskovány	maskovat	k5eAaBmNgFnP	maskovat
přerůstajícími	přerůstající	k2eAgFnPc7d1	přerůstající
kontaminujícími	kontaminující	k2eAgFnPc7d1	kontaminující
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
E.	E.	kA	E.
coli	coli	k6eAd1	coli
<g/>
.	.	kIx.	.
</s>
<s>
Přídavek	přídavek	k1gInSc1	přídavek
gentamicinu	gentamicin	k2eAgFnSc4d1	gentamicin
nebo	nebo	k8xC	nebo
polymyxinu	polymyxina	k1gFnSc4	polymyxina
do	do	k7c2	do
agaru	agar	k1gInSc2	agar
působí	působit	k5eAaImIp3nS	působit
selektivně	selektivně	k6eAd1	selektivně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
MacConkeyově	MacConkeyův	k2eAgInSc6d1	MacConkeyův
agaru	agar	k1gInSc6	agar
ORT	ort	k1gInSc1	ort
neroste	růst	k5eNaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Ornithobacterium	Ornithobacterium	k1gNnSc1	Ornithobacterium
sp	sp	k?	sp
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
kataláza	kataláza	k1gFnSc1	kataláza
negativní	negativní	k2eAgFnSc1d1	negativní
a	a	k8xC	a
oxidáza	oxidáza	k1gFnSc1	oxidáza
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
izolátů	izolát	k1gInPc2	izolát
fermentujeglukózu	fermentujeglukóza	k1gFnSc4	fermentujeglukóza
<g/>
,	,	kIx,	,
galaktózu	galaktóza	k1gFnSc4	galaktóza
<g/>
,	,	kIx,	,
laktózu	laktóza	k1gFnSc4	laktóza
<g/>
,	,	kIx,	,
maltózu	maltóza	k1gFnSc4	maltóza
a	a	k8xC	a
fruktózu	fruktóza	k1gFnSc4	fruktóza
<g/>
;	;	kIx,	;
nefermentuje	fermentovat	k5eNaBmIp3nS	fermentovat
inozitol	inozitol	k1gInSc4	inozitol
<g/>
,	,	kIx,	,
rafinózu	rafinóza	k1gFnSc4	rafinóza
<g/>
,	,	kIx,	,
sorbitol	sorbitol	k1gInSc4	sorbitol
<g/>
,	,	kIx,	,
tehalózu	tehalóza	k1gFnSc4	tehalóza
a	a	k8xC	a
xylózu	xylóza	k1gFnSc4	xylóza
<g/>
.	.	kIx.	.
</s>
<s>
Produkuje	produkovat	k5eAaImIp3nS	produkovat
hyaluronidázu	hyaluronidáza	k1gFnSc4	hyaluronidáza
<g/>
,	,	kIx,	,
močovinu	močovina	k1gFnSc4	močovina
variabilně	variabilně	k6eAd1	variabilně
<g/>
.	.	kIx.	.
</s>
<s>
Indol	indol	k1gInSc1	indol
neprodukuje	produkovat	k5eNaImIp3nS	produkovat
<g/>
,	,	kIx,	,
želatina	želatina	k1gFnSc1	želatina
není	být	k5eNaImIp3nS	být
hydrolyzována	hydrolyzovat	k5eAaBmNgFnS	hydrolyzovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
charakterizaci	charakterizace	k1gFnSc3	charakterizace
izolátů	izolát	k1gInPc2	izolát
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
komerční	komerční	k2eAgInPc4d1	komerční
diagnostické	diagnostický	k2eAgInPc4d1	diagnostický
systémy	systém	k1gInPc4	systém
API-NFT	API-NFT	k1gMnPc2	API-NFT
a	a	k8xC	a
API-ZYM	API-ZYM	k1gMnPc2	API-ZYM
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
precipitace	precipitace	k1gFnSc2	precipitace
v	v	k7c6	v
agarovém	agarový	k2eAgInSc6d1	agarový
gelu	gel	k1gInSc6	gel
bylo	být	k5eAaImAgNnS	být
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
7	[number]	k4	7
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
sérotypů	sérotyp	k1gInPc2	sérotyp
(	(	kIx(	(
<g/>
A-G	A-G	k1gFnPc2	A-G
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
bakterií	bakterie	k1gFnPc2	bakterie
Ornithobacterium	Ornithobacterium	k1gNnSc1	Ornithobacterium
sp	sp	k?	sp
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
izolaci	izolace	k1gFnSc6	izolace
O.	O.	kA	O.
rhinotracheale	rhinotracheale	k6eAd1	rhinotracheale
z	z	k7c2	z
havranů	havran	k1gMnPc2	havran
<g/>
,	,	kIx,	,
koroptví	koroptev	k1gFnPc2	koroptev
<g/>
,	,	kIx,	,
orebic	orebice	k1gFnPc2	orebice
<g/>
,	,	kIx,	,
bažantů	bažant	k1gMnPc2	bažant
a	a	k8xC	a
holubů	holub	k1gMnPc2	holub
<g/>
.	.	kIx.	.
<g/>
Mortalita	mortalita	k1gFnSc1	mortalita
v	v	k7c6	v
postižených	postižený	k2eAgNnPc6d1	postižené
hejnech	hejno	k1gNnPc6	hejno
hrabavé	hrabavý	k2eAgFnSc2d1	hrabavá
drůbeže	drůbež	k1gFnSc2	drůbež
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
2-11	[number]	k4	2-11
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
zhoršován	zhoršovat	k5eAaImNgInS	zhoršovat
konkurentními	konkurentní	k2eAgFnPc7d1	konkurentní
infekcemi	infekce	k1gFnPc7	infekce
<g/>
,	,	kIx,	,
imunosupresí	imunosuprese	k1gFnSc7	imunosuprese
hostitele	hostitel	k1gMnSc2	hostitel
a	a	k8xC	a
stresy	stres	k1gInPc7	stres
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
infekce	infekce	k1gFnSc2	infekce
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
častější	častý	k2eAgNnSc1d2	častější
u	u	k7c2	u
dospívající	dospívající	k2eAgFnSc2d1	dospívající
a	a	k8xC	a
dospělé	dospělý	k2eAgFnSc2d1	dospělá
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
také	také	k9	také
u	u	k7c2	u
masného	masný	k2eAgInSc2d1	masný
typu	typ	k1gInSc2	typ
kura	kura	k1gFnSc1	kura
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
drůbeží	drůbež	k1gFnSc7	drůbež
nosného	nosný	k2eAgInSc2d1	nosný
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zejména	zejména	k9	zejména
v	v	k7c6	v
chovných	chovný	k2eAgNnPc6d1	chovné
hejnech	hejno	k1gNnPc6	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
virulence	virulence	k1gFnSc2	virulence
původce	původce	k1gMnSc2	původce
může	moct	k5eAaImIp3nS	moct
infekce	infekce	k1gFnSc1	infekce
probíhat	probíhat	k5eAaImF	probíhat
jako	jako	k9	jako
primární	primární	k2eAgNnSc1d1	primární
nebo	nebo	k8xC	nebo
sekundární	sekundární	k2eAgNnSc1d1	sekundární
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
jako	jako	k9	jako
subklinická	subklinický	k2eAgFnSc1d1	subklinická
infekce	infekce	k1gFnSc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
imunních	imunní	k2eAgMnPc2d1	imunní
rodičů	rodič	k1gMnPc2	rodič
mají	mít	k5eAaImIp3nP	mít
pasivní	pasivní	k2eAgFnPc1d1	pasivní
protilátky	protilátka	k1gFnPc1	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
různě	různě	k6eAd1	různě
intenzivními	intenzivní	k2eAgInPc7d1	intenzivní
respiračními	respirační	k2eAgInPc7d1	respirační
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
silně	silně	k6eAd1	silně
připomínají	připomínat	k5eAaImIp3nP	připomínat
bordetelózu	bordetelóza	k1gFnSc4	bordetelóza
nebo	nebo	k8xC	nebo
pneumovirovou	pneumovirový	k2eAgFnSc4d1	pneumovirový
rhinotracheitidu	rhinotracheitida	k1gFnSc4	rhinotracheitida
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
lokomoční	lokomoční	k2eAgFnPc4d1	lokomoční
poruchy	porucha	k1gFnPc4	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
ORT	ort	k1gInSc1	ort
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
u	u	k7c2	u
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
týdenních	týdenní	k2eAgNnPc2d1	týdenní
výkrmových	výkrmový	k2eAgNnPc2d1	výkrmový
kuřat	kuře	k1gNnPc2	kuře
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
mírnými	mírný	k2eAgInPc7d1	mírný
respiračními	respirační	k2eAgInPc7d1	respirační
příznaky	příznak	k1gInPc7	příznak
(	(	kIx(	(
<g/>
rýma	rýma	k1gFnSc1	rýma
<g/>
,	,	kIx,	,
kýchání	kýchání	k1gNnSc1	kýchání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
mortalitou	mortalita	k1gFnSc7	mortalita
a	a	k8xC	a
vyššími	vysoký	k2eAgFnPc7d2	vyšší
konfiskacemi	konfiskace	k1gFnPc7	konfiskace
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
postiženy	postižen	k2eAgInPc1d1	postižen
rozmnožovací	rozmnožovací	k2eAgInPc1d1	rozmnožovací
chovy	chov	k1gInPc1	chov
masného	masný	k2eAgInSc2d1	masný
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
RCHM	RCHM	kA	RCHM
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
24	[number]	k4	24
<g/>
.	.	kIx.	.
až	až	k9	až
52	[number]	k4	52
<g/>
.	.	kIx.	.
týdnem	týden	k1gInSc7	týden
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
období	období	k1gNnSc6	období
vrcholící	vrcholící	k2eAgFnSc2d1	vrcholící
snášky	snáška	k1gFnSc2	snáška
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
se	se	k3xPyFc4	se
snížený	snížený	k2eAgInSc4d1	snížený
příjem	příjem	k1gInSc4	příjem
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
mortalita	mortalita	k1gFnSc1	mortalita
a	a	k8xC	a
lehké	lehký	k2eAgInPc1d1	lehký
respirační	respirační	k2eAgInPc1d1	respirační
příznaky	příznak	k1gInPc1	příznak
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
pokles	pokles	k1gInSc1	pokles
snášky	snáška	k1gFnSc2	snáška
o	o	k7c4	o
2-5	[number]	k4	2-5
%	%	kIx~	%
<g/>
,	,	kIx,	,
zhoršená	zhoršený	k2eAgFnSc1d1	zhoršená
kvalita	kvalita	k1gFnSc1	kvalita
vaječné	vaječný	k2eAgFnSc2d1	vaječná
skořápky	skořápka	k1gFnSc2	skořápka
a	a	k8xC	a
menší	malý	k2eAgFnSc4d2	menší
velikost	velikost	k1gFnSc4	velikost
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
byla	být	k5eAaImAgFnS	být
infekce	infekce	k1gFnSc1	infekce
ORT	ort	k1gInSc1	ort
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
u	u	k7c2	u
2	[number]	k4	2
<g/>
týdenních	týdenní	k2eAgNnPc2d1	týdenní
krůťat	krůtě	k1gNnPc2	krůtě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohem	mnohem	k6eAd1	mnohem
vážnější	vážní	k2eAgFnPc1d2	vážnější
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
(	(	kIx(	(
<g/>
>	>	kIx)	>
14	[number]	k4	14
týdnů	týden	k1gInPc2	týden
<g/>
)	)	kIx)	)
a	a	k8xC	a
chovných	chovný	k2eAgFnPc2d1	chovná
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
u	u	k7c2	u
2	[number]	k4	2
<g/>
týdenních	týdenní	k2eAgNnPc2d1	týdenní
krůťat	krůtě	k1gNnPc2	krůtě
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
respiračními	respirační	k2eAgInPc7d1	respirační
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
výtokem	výtok	k1gInSc7	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
otokem	otok	k1gInSc7	otok
tváří	tvář	k1gFnPc2	tvář
a	a	k8xC	a
zduřením	zduření	k1gNnSc7	zduření
podočnicových	podočnicový	k2eAgFnPc2d1	podočnicový
dutin	dutina	k1gFnPc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
neteční	tečný	k2eNgMnPc1d1	tečný
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
načepýřené	načepýřený	k2eAgNnSc4d1	načepýřené
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
snížený	snížený	k2eAgInSc4d1	snížený
příjem	příjem	k1gInSc4	příjem
krmiva	krmivo	k1gNnSc2	krmivo
i	i	k8xC	i
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
mortalitu	mortalita	k1gFnSc4	mortalita
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
krůt	krůta	k1gFnPc2	krůta
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
jediným	jediný	k2eAgInSc7d1	jediný
příznakem	příznak	k1gInSc7	příznak
náhlé	náhlý	k2eAgNnSc4d1	náhlé
zvýšení	zvýšení	k1gNnSc4	zvýšení
úhynů	úhyn	k1gInPc2	úhyn
anebo	anebo	k8xC	anebo
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
úhynem	úhyn	k1gInSc7	úhyn
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
netečnost	netečnost	k1gFnSc1	netečnost
<g/>
,	,	kIx,	,
cyanóza	cyanóza	k1gFnSc1	cyanóza
<g/>
,	,	kIx,	,
dyspnoe	dyspnoe	k1gFnSc1	dyspnoe
a	a	k8xC	a
vykašlávání	vykašlávání	k1gNnSc4	vykašlávání
krví	krev	k1gFnPc2	krev
zbarveného	zbarvený	k2eAgInSc2d1	zbarvený
hlenu	hlen	k1gInSc2	hlen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chovném	chovný	k2eAgNnSc6d1	chovné
hejnu	hejno	k1gNnSc6	hejno
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
mírnému	mírný	k2eAgNnSc3d1	mírné
snížení	snížení	k1gNnSc3	snížení
snášky	snáška	k1gFnSc2	snáška
o	o	k7c4	o
2-5	[number]	k4	2-5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Septický	septický	k2eAgInSc1d1	septický
průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
sporadický	sporadický	k2eAgMnSc1d1	sporadický
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
připomínat	připomínat	k5eAaImF	připomínat
choleru	cholera	k1gFnSc4	cholera
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
postižení	postižení	k1gNnSc6	postižení
kloubů	kloub	k1gInPc2	kloub
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
končetin	končetina	k1gFnPc2	končetina
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
krůty	krůta	k1gFnPc4	krůta
nad	nad	k7c4	nad
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
věku	věk	k1gInSc2	věk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
pohybové	pohybový	k2eAgFnPc4d1	pohybová
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
lze	lze	k6eAd1	lze
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
základě	základ	k1gInSc6	základ
izolace	izolace	k1gFnSc2	izolace
a	a	k8xC	a
identifikace	identifikace	k1gFnSc2	identifikace
původce	původce	k1gMnSc2	původce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sérologické	sérologický	k2eAgNnSc4d1	sérologické
vyšetření	vyšetření	k1gNnSc4	vyšetření
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
imunodifuze	imunodifuze	k1gFnSc1	imunodifuze
nebo	nebo	k8xC	nebo
ELISA	ELISA	kA	ELISA
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
odlišit	odlišit	k5eAaPmF	odlišit
podobně	podobně	k6eAd1	podobně
probíhající	probíhající	k2eAgNnSc4d1	probíhající
respirační	respirační	k2eAgNnSc4d1	respirační
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
bordetelóza	bordetelóza	k1gFnSc1	bordetelóza
a	a	k8xC	a
pneumovirová	pneumovirový	k2eAgFnSc1d1	pneumovirový
rhinotracheitida	rhinotracheitida	k1gFnSc1	rhinotracheitida
(	(	kIx(	(
<g/>
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
syndrom	syndrom	k1gInSc4	syndrom
oteklé	oteklý	k2eAgFnSc2d1	oteklá
hlavy	hlava	k1gFnSc2	hlava
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
při	při	k7c6	při
akutním	akutní	k2eAgInSc6d1	akutní
průběhu	průběh	k1gInSc6	průběh
ornithobakteriózy	ornithobakterióza	k1gFnSc2	ornithobakterióza
pak	pak	k6eAd1	pak
zejména	zejména	k9	zejména
choleru	cholera	k1gFnSc4	cholera
<g/>
.	.	kIx.	.
</s>
<s>
Diagnózu	diagnóza	k1gFnSc4	diagnóza
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
komplikovat	komplikovat	k5eAaBmF	komplikovat
bakterie	bakterie	k1gFnPc4	bakterie
vyvolávající	vyvolávající	k2eAgFnSc2d1	vyvolávající
serozitidy	serozitis	k1gFnSc2	serozitis
(	(	kIx(	(
<g/>
E.	E.	kA	E.
coli	coli	k1gNnSc1	coli
<g/>
,	,	kIx,	,
Riemerella	Riemerella	k1gFnSc1	Riemerella
anatipestifer	anatipestifer	k1gMnSc1	anatipestifer
nebo	nebo	k8xC	nebo
Chlamydophila	Chlamydophila	k1gFnSc1	Chlamydophila
psittaci	psittace	k1gFnSc4	psittace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
účinné	účinný	k2eAgFnSc2d1	účinná
antimikrobiální	antimikrobiální	k2eAgFnSc2d1	antimikrobiální
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc4	citlivost
Ornithobacterium	Ornithobacterium	k1gNnSc4	Ornithobacterium
rhinotracheale	rhinotracheale	k6eAd1	rhinotracheale
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
geograficky	geograficky	k6eAd1	geograficky
<g/>
,	,	kIx,	,
např.	např.	kA	např.
izoláty	izolát	k1gInPc7	izolát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgFnSc4d2	širší
rezistenci	rezistence	k1gFnSc4	rezistence
než	než	k8xS	než
izoláty	izolát	k1gInPc4	izolát
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
vakcín	vakcína	k1gFnPc2	vakcína
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
používá	používat	k5eAaImIp3nS	používat
autogenní	autogenní	k2eAgFnSc1d1	autogenní
inaktivovaná	inaktivovaný	k2eAgFnSc1d1	inaktivovaná
olejová	olejový	k2eAgFnSc1d1	olejová
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
i	i	k9	i
biologických	biologický	k2eAgFnPc6d1	biologická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
i	i	k8xC	i
epizootologii	epizootologie	k1gFnSc6	epizootologie
Ornithobacterium	Ornithobacterium	k1gNnSc1	Ornithobacterium
sp	sp	k?	sp
<g/>
.	.	kIx.	.
jsou	být	k5eAaImIp3nP	být
zatím	zatím	k6eAd1	zatím
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
prevence	prevence	k1gFnSc1	prevence
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
obvyklých	obvyklý	k2eAgNnPc2d1	obvyklé
veterinárních	veterinární	k2eAgNnPc2d1	veterinární
i	i	k8xC	i
zoohygienických	zoohygienický	k2eAgNnPc2d1	zoohygienický
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
se	se	k3xPyFc4	se
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
zavlečení	zavlečení	k1gNnSc1	zavlečení
infekce	infekce	k1gFnSc2	infekce
do	do	k7c2	do
chovu	chov	k1gInSc2	chov
i	i	k8xC	i
jeho	on	k3xPp3gNnSc2	on
šíření	šíření	k1gNnSc2	šíření
<g/>
.	.	kIx.	.
</s>
<s>
JURAJDA	JURAJDA	kA	JURAJDA
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc4	nemoc
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
ptactva	ptactvo	k1gNnSc2	ptactvo
-	-	kIx~	-
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
a	a	k8xC	a
mykotické	mykotický	k2eAgFnSc2d1	mykotická
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
ES	ES	kA	ES
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
185	[number]	k4	185
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7305	[number]	k4	7305
<g/>
-	-	kIx~	-
<g/>
464	[number]	k4	464
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
RITCHIE	RITCHIE	kA	RITCHIE
<g/>
,	,	kIx,	,
B.W.	B.W.	k1gFnSc1	B.W.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
..	..	k?	..
Avian	Avian	k1gInSc1	Avian
Medicine	Medicin	k1gInSc5	Medicin
<g/>
:	:	kIx,	:
Principles	Principles	k1gInSc1	Principles
and	and	k?	and
Application	Application	k1gInSc1	Application
<g/>
.	.	kIx.	.
</s>
<s>
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
:	:	kIx,	:
Wingers	Wingers	k1gInSc1	Wingers
Publ	Publ	k1gInSc1	Publ
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
1384	[number]	k4	1384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
9636996	[number]	k4	9636996
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SAIF	SAIF	kA	SAIF
<g/>
,	,	kIx,	,
Y.	Y.	kA	Y.
<g/>
M.	M.	kA	M.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Diseases	Diseases	k1gInSc1	Diseases
of	of	k?	of
Poultry	Poultr	k1gMnPc7	Poultr
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ames	Ames	k1gInSc1	Ames
<g/>
,	,	kIx,	,
USA	USA	kA	USA
:	:	kIx,	:
Iowa	Iowa	k1gMnSc1	Iowa
State	status	k1gInSc5	status
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Blackwell	Blackwell	k1gMnSc1	Blackwell
Publ	Publ	k1gMnSc1	Publ
<g/>
.	.	kIx.	.
</s>
<s>
Comp	Comp	k1gInSc1	Comp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
1231	[number]	k4	1231
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8138	[number]	k4	8138
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
423	[number]	k4	423
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
