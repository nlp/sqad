<s>
Sála	Sála	k1gFnSc1	Sála
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Saale	Saal	k1gMnSc2	Saal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
,	,	kIx,	,
Durynsko	Durynsko	k1gNnSc1	Durynsko
a	a	k8xC	a
Sasko-Anhaltsko	Sasko-Anhaltsko	k1gNnSc1	Sasko-Anhaltsko
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
Vltavě	Vltava	k1gFnSc6	Vltava
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
druhý	druhý	k4xOgInSc1	druhý
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
přítok	přítok	k1gInSc1	přítok
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
433,9	[number]	k4	433,9
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
jejího	její	k3xOp3gNnSc2	její
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
24	[number]	k4	24
0	[number]	k4	0
<g/>
79,1	[number]	k4	79,1
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
ve	v	k7c6	v
Smrčinách	Smrčiny	k1gFnPc6	Smrčiny
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Zell	Zell	k1gInSc1	Zell
im	im	k?	im
Fichtelgebirge	Fichtelgebirg	k1gFnSc2	Fichtelgebirg
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
728	[number]	k4	728
m	m	kA	m
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
protéká	protékat	k5eAaImIp3nS	protékat
přes	přes	k7c4	přes
Durynskou	durynský	k2eAgFnSc4d1	Durynská
rovinu	rovina	k1gFnSc4	rovina
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
lesnaté	lesnatý	k2eAgFnSc6d1	lesnatá
dolině	dolina	k1gFnSc6	dolina
(	(	kIx(	(
<g/>
Durynsko	Durynsko	k1gNnSc1	Durynsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Naumburgem	Naumburg	k1gInSc7	Naumburg
(	(	kIx(	(
<g/>
Sasko-Anhaltsko	Sasko-Anhaltsko	k1gNnSc1	Sasko-Anhaltsko
<g/>
)	)	kIx)	)
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
nížinou	nížina	k1gFnSc7	nížina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
její	její	k3xOp3gNnPc4	její
povodí	povodí	k1gNnPc4	povodí
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
část	část	k1gFnSc1	část
Saska	Sasko	k1gNnSc2	Sasko
a	a	k8xC	a
také	také	k9	také
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
řeky	řeka	k1gFnSc2	řeka
Weiße	Weiße	k1gNnSc2	Weiße
Elster	Elstra	k1gFnPc2	Elstra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Ašském	ašský	k2eAgInSc6d1	ašský
výběžku	výběžek	k1gInSc6	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
ústí	ústit	k5eAaImIp3nS	ústit
u	u	k7c2	u
města	město	k1gNnSc2	město
Barby	Barba	k1gFnSc2	Barba
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
49,5	[number]	k4	49,5
m.	m.	k?	m.
Schwesnitz	Schwesnitza	k1gFnPc2	Schwesnitza
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
?	?	kIx.	?
</s>
<s>
Rokytnice	Rokytnice	k1gFnSc1	Rokytnice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Südliche	Südliche	k1gNnSc1	Südliche
Regnitz	Regnitza	k1gFnPc2	Regnitza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
?	?	kIx.	?
</s>
<s>
Schwarza	Schwarza	k?	Schwarza
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
?	?	kIx.	?
</s>
<s>
Ilm	Ilm	k?	Ilm
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
181,3	[number]	k4	181,3
Unstrut	Unstrut	k1gInSc4	Unstrut
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
161,8	[number]	k4	161,8
Bílý	bílý	k2eAgInSc1d1	bílý
Halštrov	Halštrov	k1gInSc1	Halštrov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Weiße	Weiße	k1gNnSc1	Weiße
Elster	Elstra	k1gFnPc2	Elstra
<g/>
)	)	kIx)	)
pramenící	pramenící	k2eAgFnSc1d1	pramenící
u	u	k7c2	u
Aše	Aš	k1gInSc2	Aš
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
102,7	[number]	k4	102,7
Wipper	Wippero	k1gNnPc2	Wippero
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
37,7	[number]	k4	37,7
Fuhne	Fuhn	k1gMnSc5	Fuhn
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
33,7	[number]	k4	33,7
Bode	bůst	k5eAaImIp3nS	bůst
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
27,6	[number]	k4	27,6
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
činí	činit	k5eAaImIp3nS	činit
117	[number]	k4	117
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
u	u	k7c2	u
Naumburgu	Naumburg	k1gInSc2	Naumburg
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
60	[number]	k4	60
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
úrovní	úroveň	k1gFnPc2	úroveň
hladiny	hladina	k1gFnSc2	hladina
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
od	od	k7c2	od
Naumburgu	Naumburg	k1gInSc2	Naumburg
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
175	[number]	k4	175
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Halle	Halle	k1gFnSc2	Halle
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
regulovaná	regulovaný	k2eAgFnSc1d1	regulovaná
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
zdymadel	zdymadlo	k1gNnPc2	zdymadlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
vodní	vodní	k2eAgFnPc1d1	vodní
elektrárny	elektrárna	k1gFnPc1	elektrárna
(	(	kIx(	(
<g/>
Grafenward	Grafenward	k1gInSc1	Grafenward
<g/>
,	,	kIx,	,
Beliloch	Beliloch	k1gInSc1	Beliloch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
leží	ležet	k5eAaImIp3nP	ležet
města	město	k1gNnPc1	město
Hof	Hof	k1gMnSc1	Hof
<g/>
,	,	kIx,	,
Saalfeld	Saalfeld	k1gMnSc1	Saalfeld
<g/>
,	,	kIx,	,
Rudolstadt	Rudolstadt	k1gMnSc1	Rudolstadt
<g/>
,	,	kIx,	,
Jena	Jena	k1gMnSc1	Jena
<g/>
,	,	kIx,	,
Naumburg	Naumburg	k1gMnSc1	Naumburg
<g/>
,	,	kIx,	,
Weißenfels	Weißenfels	k1gInSc1	Weißenfels
<g/>
,	,	kIx,	,
Merseburg	Merseburg	k1gInSc1	Merseburg
<g/>
,	,	kIx,	,
Halle	Halle	k1gInSc1	Halle
<g/>
,	,	kIx,	,
Bernburg	Bernburg	k1gInSc1	Bernburg
<g/>
,	,	kIx,	,
Calbe	Calb	k1gMnSc5	Calb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
З	З	k?	З
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Sála	sát	k5eAaImAgFnS	sát
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Sála	Sála	k1gFnSc1	Sála
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sála	sát	k5eAaImAgFnS	sát
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Sála	Sála	k1gFnSc1	Sála
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Sála	Sála	k1gFnSc1	Sála
-	-	kIx~	-
říční	říční	k2eAgFnSc1d1	říční
kilometráž	kilometráž	k1gFnSc1	kilometráž
</s>
