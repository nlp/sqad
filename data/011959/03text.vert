<p>
<s>
Jubilejní	jubilejní	k2eAgInSc1d1	jubilejní
háj	háj	k1gInSc1	háj
je	být	k5eAaImIp3nS	být
lesní	lesní	k2eAgInSc4d1	lesní
park	park	k1gInSc4	park
v	v	k7c6	v
Týně	Týn	k1gInSc6	Týn
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
<g/>
.	.	kIx.	.
</s>
<s>
Zbudován	zbudován	k2eAgMnSc1d1	zbudován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
ke	k	k7c3	k
dvacátému	dvacátý	k4xOgNnSc3	dvacátý
výročí	výročí	k1gNnSc3	výročí
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
údolí	údolí	k1gNnSc2	údolí
Týnského	týnský	k2eAgInSc2d1	týnský
potoka	potok	k1gInSc2	potok
při	při	k7c6	při
Hladovém	hladový	k2eAgInSc6d1	hladový
rybníce	rybník	k1gInSc6	rybník
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
Koutkovou	Koutkův	k2eAgFnSc7d1	Koutkova
<g/>
,	,	kIx,	,
Táborskou	táborský	k2eAgFnSc7d1	táborská
a	a	k8xC	a
Budíkovickou	Budíkovický	k2eAgFnSc7d1	Budíkovická
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
funkční	funkční	k2eAgFnSc7d1	funkční
součástí	součást	k1gFnSc7	součást
jakožto	jakožto	k8xS	jakožto
místa	místo	k1gNnPc4	místo
oddychu	oddych	k1gInSc2	oddych
je	být	k5eAaImIp3nS	být
i	i	k9	i
dětské	dětský	k2eAgNnSc1d1	dětské
hřiště	hřiště	k1gNnSc1	hřiště
u	u	k7c2	u
obratiště	obratiště	k1gNnSc2	obratiště
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
jubilejního	jubilejní	k2eAgInSc2d1	jubilejní
háje	háj	k1gInSc2	háj
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
1	[number]	k4	1
ha	ha	kA	ha
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgInSc4d1	lesní
porost	porost	k1gInSc4	porost
měří	měřit	k5eAaImIp3nS	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
asi	asi	k9	asi
270	[number]	k4	270
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Jubilejní	jubilejní	k2eAgInSc4d1	jubilejní
háj	háj	k1gInSc4	háj
zbudovala	zbudovat	k5eAaPmAgFnS	zbudovat
týnská	týnský	k2eAgFnSc1d1	Týnská
osvětová	osvětový	k2eAgFnSc1d1	osvětová
komise	komise	k1gFnSc1	komise
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
Týnské	týnský	k2eAgFnPc1d1	Týnská
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
místo	místo	k6eAd1	místo
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
stráň	stráň	k1gFnSc1	stráň
nad	nad	k7c7	nad
Týnským	týnský	k2eAgInSc7d1	týnský
potokem	potok	k1gInSc7	potok
a	a	k8xC	a
rybníkem	rybník	k1gInSc7	rybník
Hladovým	hladový	k2eAgInSc7d1	hladový
při	při	k7c6	při
polní	polní	k2eAgFnSc6d1	polní
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
otevření	otevření	k1gNnSc2	otevření
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1938	[number]	k4	1938
byly	být	k5eAaImAgFnP	být
přítomny	přítomen	k2eAgFnPc1d1	přítomna
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
Týňané	Týňan	k1gMnPc1	Týňan
památník	památník	k1gInSc4	památník
uschovali	uschovat	k5eAaPmAgMnP	uschovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
v	v	k7c6	v
září	září	k1gNnSc6	září
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Neudržován	udržován	k2eNgInSc1d1	neudržován
a	a	k8xC	a
pozapomenut	pozapomenut	k2eAgInSc1d1	pozapomenut
přečkal	přečkat	k5eAaPmAgInS	přečkat
památník	památník	k1gInSc1	památník
dobu	doba	k1gFnSc4	doba
komunistické	komunistický	k2eAgFnSc2d1	komunistická
doby	doba	k1gFnSc2	doba
nesvobody	nesvoboda	k1gFnSc2	nesvoboda
<g/>
.	.	kIx.	.
</s>
<s>
Obnova	obnova	k1gFnSc1	obnova
jubilejního	jubilejní	k2eAgInSc2d1	jubilejní
háje	háj	k1gInSc2	háj
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
pamětní	pamětní	k2eAgFnSc6d1	pamětní
desce	deska	k1gFnSc6	deska
uprostřed	uprostřed	k7c2	uprostřed
jubilejního	jubilejní	k2eAgInSc2d1	jubilejní
háje	háj	k1gInSc2	háj
<g/>
,	,	kIx,	,
mající	mající	k2eAgInPc1d1	mající
připomínat	připomínat	k5eAaImF	připomínat
dvacáté	dvacátý	k4xOgNnSc4	dvacátý
výročí	výročí	k1gNnSc4	výročí
osvobození	osvobození	k1gNnSc2	osvobození
vlasti	vlast	k1gFnSc2	vlast
od	od	k7c2	od
cizí	cizí	k2eAgFnSc2d1	cizí
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
portréty	portrét	k1gInPc4	portrét
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
prezidentů	prezident	k1gMnPc2	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
a	a	k8xC	a
pod	pod	k7c7	pod
nimi	on	k3xPp3gFnPc7	on
nápis	nápis	k1gInSc1	nápis
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
nápis	nápis	k1gInSc1	nápis
je	být	k5eAaImIp3nS	být
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
citací	citace	k1gFnSc7	citace
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
proslovu	proslov	k1gInSc2	proslov
prezidenta	prezident	k1gMnSc2	prezident
Beneše	Beneš	k1gMnSc2	Beneš
nad	nad	k7c7	nad
rakví	rakev	k1gFnSc7	rakev
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jubilejní	jubilejní	k2eAgInSc1d1	jubilejní
háj	háj	k1gInSc1	háj
pravidelně	pravidelně	k6eAd1	pravidelně
hostívá	hostívat	k5eAaImIp3nS	hostívat
vzpomínková	vzpomínkový	k2eAgNnPc4d1	vzpomínkové
shromáždění	shromáždění	k1gNnPc4	shromáždění
k	k	k7c3	k
uctění	uctění	k1gNnSc3	uctění
památky	památka	k1gFnSc2	památka
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc4	Masaryk
a	a	k8xC	a
výročí	výročí	k1gNnSc4	výročí
obnovení	obnovení	k1gNnSc2	obnovení
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přístupný	přístupný	k2eAgMnSc1d1	přístupný
je	být	k5eAaImIp3nS	být
pěšinou	pěšina	k1gFnSc7	pěšina
podél	podél	k7c2	podél
Týnského	týnský	k2eAgInSc2d1	týnský
potoka	potok	k1gInSc2	potok
od	od	k7c2	od
ulice	ulice	k1gFnSc2	ulice
Táborské	Táborská	k1gFnSc2	Táborská
i	i	k9	i
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
Budíkovické	Budíkovický	k2eAgFnSc6d1	Budíkovická
od	od	k7c2	od
Týnského	týnský	k2eAgInSc2d1	týnský
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
památník	památník	k1gInSc1	památník
je	být	k5eAaImIp3nS	být
chodníkem	chodník	k1gInSc7	chodník
napojen	napojen	k2eAgInSc4d1	napojen
na	na	k7c4	na
komunikace	komunikace	k1gFnPc4	komunikace
Koutkovy	Koutkův	k2eAgFnSc2d1	Koutkova
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
památník	památník	k1gInSc1	památník
rekonstruován	rekonstruovat	k5eAaBmNgInS	rekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgFnSc1d1	generální
oprava	oprava	k1gFnSc1	oprava
památníku	památník	k1gInSc2	památník
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
výročí	výročí	k1gNnSc2	výročí
100	[number]	k4	100
let	léto	k1gNnPc2	léto
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
byl	být	k5eAaImAgInS	být
posouzen	posoudit	k5eAaPmNgInS	posoudit
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
shledán	shledán	k2eAgMnSc1d1	shledán
jako	jako	k8xC	jako
nevyhovující	vyhovující	k2eNgMnSc1d1	nevyhovující
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
věrnou	věrný	k2eAgFnSc7d1	věrná
kopií	kopie	k1gFnSc7	kopie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jubilejní	jubilejní	k2eAgInSc4d1	jubilejní
háj	háj	k1gInSc4	háj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
