<s>
Germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
obývaly	obývat	k5eAaImAgFnP	obývat
naše	náš	k3xOp1gNnSc4	náš
území	území	k1gNnSc4	území
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
Markomany	Markoman	k1gMnPc4	Markoman
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
z	z	k7c2	z
našeho	náš	k3xOp1gNnSc2	náš
území	území	k1gNnSc2	území
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
keltské	keltský	k2eAgInPc4d1	keltský
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
