<p>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
nebo	nebo	k8xC	nebo
Sudetoněmci	Sudetoněmec	k1gMnPc1	Sudetoněmec
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Sudetendeutsche	Sudetendeutsche	k1gInSc1	Sudetendeutsche
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Deutschböhmen	Deutschböhmen	k1gInSc1	Deutschböhmen
–	–	k?	–
čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Deutschmährer	Deutschmährer	k1gMnSc1	Deutschmährer
–	–	k?	–
moravští	moravský	k2eAgMnPc1d1	moravský
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Deutschschlesier	Deutschschlesier	k1gInSc1	Deutschschlesier
–	–	k?	–
slezští	slezský	k2eAgMnPc1d1	slezský
Němci	Němec	k1gMnPc1	Němec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
geograficky	geograficky	k6eAd1	geograficky
i	i	k9	i
historicky	historicky	k6eAd1	historicky
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
užívané	užívaný	k2eAgNnSc1d1	užívané
od	od	k7c2	od
konce	konec	k1gInSc2	konec
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
německými	německý	k2eAgMnPc7d1	německý
nacionalisty	nacionalista	k1gMnPc7	nacionalista
v	v	k7c6	v
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pohraničí	pohraničí	k1gNnSc6	pohraničí
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ty	k3xPp2nSc3	ty
tento	tento	k3xDgInSc4	tento
pojem	pojem	k1gInSc4	pojem
vztahovali	vztahovat	k5eAaImAgMnP	vztahovat
na	na	k7c4	na
německé	německý	k2eAgNnSc4d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
už	už	k6eAd1	už
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
na	na	k7c6	na
pohraničním	pohraniční	k2eAgNnSc6d1	pohraniční
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
označovaném	označovaný	k2eAgNnSc6d1	označované
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
nepřesně	přesně	k6eNd1	přesně
a	a	k8xC	a
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Sudety	Sudety	k1gFnPc4	Sudety
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
separatismu	separatismus	k1gInSc2	separatismus
v	v	k7c6	v
první	první	k4xOgFnSc6	první
Československé	československý	k2eAgFnSc6d1	Československá
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
reprezentovaného	reprezentovaný	k2eAgInSc2d1	reprezentovaný
Sudetoněmeckou	sudetoněmecký	k2eAgFnSc7d1	Sudetoněmecká
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
pojem	pojem	k1gInSc4	pojem
Sudety	Sudety	k1gInPc7	Sudety
a	a	k8xC	a
sudetský	sudetský	k2eAgMnSc1d1	sudetský
Němec	Němec	k1gMnSc1	Němec
svévolně	svévolně	k6eAd1	svévolně
vztahován	vztahován	k2eAgMnSc1d1	vztahován
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
pohraničí	pohraničí	k1gNnSc4	pohraničí
obývané	obývaný	k2eAgInPc1d1	obývaný
německou	německý	k2eAgFnSc7d1	německá
menšinou	menšina	k1gFnSc7	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
dobové	dobový	k2eAgFnSc2d1	dobová
atmosféry	atmosféra	k1gFnSc2	atmosféra
začali	začít	k5eAaPmAgMnP	začít
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tento	tento	k3xDgInSc1	tento
jazykový	jazykový	k2eAgInSc1d1	jazykový
novotvar	novotvar	k1gInSc1	novotvar
používat	používat	k5eAaImF	používat
i	i	k9	i
česká	český	k2eAgNnPc1d1	české
média	médium	k1gNnPc1	médium
a	a	k8xC	a
čeští	český	k2eAgMnPc1d1	český
politici	politik	k1gMnPc1	politik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
přívržence	přívrženec	k1gMnPc4	přívrženec
Sudetoněmecké	sudetoněmecký	k2eAgFnSc2d1	Sudetoněmecká
strany	strana	k1gFnSc2	strana
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
označení	označení	k1gNnSc4	označení
sudetský	sudetský	k2eAgMnSc1d1	sudetský
Němec	Němec	k1gMnSc1	Němec
použito	použít	k5eAaPmNgNnS	použít
vcelku	vcelku	k6eAd1	vcelku
logicky	logicky	k6eAd1	logicky
<g/>
,	,	kIx,	,
problematické	problematický	k2eAgNnSc1d1	problematické
je	být	k5eAaImIp3nS	být
vztáhnutí	vztáhnutí	k1gNnSc1	vztáhnutí
tohoto	tento	k3xDgInSc2	tento
pojmu	pojem	k1gInSc2	pojem
na	na	k7c4	na
německojazyčné	německojazyčný	k2eAgMnPc4d1	německojazyčný
antifašisty	antifašista	k1gMnPc4	antifašista
nebo	nebo	k8xC	nebo
osoby	osoba	k1gFnPc4	osoba
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zcela	zcela	k6eAd1	zcela
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
označovat	označovat	k5eAaImF	označovat
takto	takto	k6eAd1	takto
české	český	k2eAgMnPc4d1	český
Němce	Němec	k1gMnPc4	Němec
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
důvodů	důvod	k1gInPc2	důvod
užívá	užívat	k5eAaImIp3nS	užívat
řada	řada	k1gFnSc1	řada
autorů	autor	k1gMnPc2	autor
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
německy	německy	k6eAd1	německy
hovořících	hovořící	k2eAgMnPc2d1	hovořící
obyvatel	obyvatel	k1gMnPc2	obyvatel
Česka	Česko	k1gNnSc2	Česko
méně	málo	k6eAd2	málo
zatížený	zatížený	k2eAgInSc1d1	zatížený
výraz	výraz	k1gInSc4	výraz
čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vztáhnout	vztáhnout	k5eAaPmF	vztáhnout
jak	jak	k6eAd1	jak
na	na	k7c4	na
současnost	současnost	k1gFnSc4	současnost
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
starší	starý	k2eAgNnSc4d2	starší
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
předcházející	předcházející	k2eAgMnPc1d1	předcházející
nacionálně	nacionálně	k6eAd1	nacionálně
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
vypjaté	vypjatý	k2eAgFnSc6d1	vypjatá
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soužití	soužití	k1gNnPc4	soužití
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Do	do	k7c2	do
Sudet	Sudety	k1gInPc2	Sudety
–	–	k?	–
které	který	k3yRgNnSc1	který
v	v	k7c6	v
geografickém	geografický	k2eAgInSc6d1	geografický
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
označují	označovat	k5eAaImIp3nP	označovat
jen	jen	k9	jen
Krkonošsko-Jesenickou	krkonošskoesenický	k2eAgFnSc4d1	krkonošsko-jesenický
soustavu	soustava	k1gFnSc4	soustava
–	–	k?	–
patří	patřit	k5eAaImIp3nS	patřit
z	z	k7c2	z
politického	politický	k2eAgNnSc2d1	politické
hlediska	hledisko	k1gNnSc2	hledisko
část	část	k1gFnSc4	část
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
Českého	český	k2eAgNnSc2d1	české
Slezska	Slezsko	k1gNnSc2	Slezsko
kromě	kromě	k7c2	kromě
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
slezského	slezský	k2eAgNnSc2d1	Slezské
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
národnostně	národnostně	k6eAd1	národnostně
různorodé	různorodý	k2eAgNnSc1d1	různorodé
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
mnohá	mnohý	k2eAgNnPc4d1	mnohé
staletí	staletí	k1gNnPc4	staletí
pospolu	pospolu	k6eAd1	pospolu
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Češi	Čech	k1gMnPc1	Čech
i	i	k8xC	i
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
celými	celý	k2eAgInPc7d1	celý
Sudety	Sudety	k1gInPc7	Sudety
prochází	procházet	k5eAaImIp3nS	procházet
hraniční	hraniční	k2eAgNnSc1d1	hraniční
pohoří	pohoří	k1gNnSc1	pohoří
kolem	kolem	k7c2	kolem
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Novohradské	novohradský	k2eAgFnPc1d1	Novohradská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
,	,	kIx,	,
Slavkovský	slavkovský	k2eAgInSc1d1	slavkovský
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Lužické	lužický	k2eAgFnPc1d1	Lužická
a	a	k8xC	a
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
,	,	kIx,	,
Orlické	orlický	k2eAgFnPc1d1	Orlická
hory	hora	k1gFnPc1	hora
a	a	k8xC	a
Hrubý	hrubý	k2eAgInSc1d1	hrubý
a	a	k8xC	a
Nízký	nízký	k2eAgInSc1d1	nízký
Jeseník	Jeseník	k1gInSc1	Jeseník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
poněkud	poněkud	k6eAd1	poněkud
hanlivé	hanlivý	k2eAgNnSc1d1	hanlivé
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
skopčáci	skopčák	k1gMnPc1	skopčák
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
kopců	kopec	k1gInPc2	kopec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
obývaly	obývat	k5eAaImAgFnP	obývat
naše	náš	k3xOp1gNnSc4	náš
území	území	k1gNnSc4	území
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
Markomany	Markoman	k1gMnPc4	Markoman
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
z	z	k7c2	z
našeho	náš	k3xOp1gNnSc2	náš
území	území	k1gNnSc2	území
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
keltské	keltský	k2eAgInPc4d1	keltský
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
stěhování	stěhování	k1gNnPc2	stěhování
národů	národ	k1gInPc2	národ
z	z	k7c2	z
našeho	náš	k3xOp1gNnSc2	náš
území	území	k1gNnSc2	území
většina	většina	k1gFnSc1	většina
Germánů	Germán	k1gMnPc2	Germán
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německá	německý	k2eAgNnPc1d1	německé
osidlování	osidlování	k1gNnPc1	osidlování
vesměs	vesměs	k6eAd1	vesměs
málo	málo	k6eAd1	málo
obydleného	obydlený	k2eAgNnSc2d1	obydlené
pohraničí	pohraničí	k1gNnSc2	pohraničí
počíná	počínat	k5eAaImIp3nS	počínat
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
vlády	vláda	k1gFnSc2	vláda
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
především	především	k9	především
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c2	za
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Kolonisté	kolonista	k1gMnPc1	kolonista
přicházeli	přicházet	k5eAaImAgMnP	přicházet
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
málo	málo	k6eAd1	málo
či	či	k8xC	či
zcela	zcela	k6eAd1	zcela
neosídlené	osídlený	k2eNgFnPc4d1	neosídlená
pohraniční	pohraniční	k2eAgFnPc4d1	pohraniční
české	český	k2eAgFnPc4d1	Česká
oblasti	oblast	k1gFnPc4	oblast
kultivovali	kultivovat	k5eAaImAgMnP	kultivovat
<g/>
,	,	kIx,	,
hospodářsky	hospodářsky	k6eAd1	hospodářsky
oživili	oživit	k5eAaPmAgMnP	oživit
a	a	k8xC	a
tak	tak	k6eAd1	tak
také	také	k9	také
posílili	posílit	k5eAaPmAgMnP	posílit
moc	moc	k1gFnSc4	moc
panovníka	panovník	k1gMnSc2	panovník
oproti	oproti	k7c3	oproti
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
totiž	totiž	k9	totiž
žili	žít	k5eAaImAgMnP	žít
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
podél	podél	k7c2	podél
říčních	říční	k2eAgInPc2d1	říční
toků	tok	k1gInPc2	tok
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
životem	život	k1gInSc7	život
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Příchozí	příchozí	k1gFnSc7	příchozí
Němci	Němec	k1gMnPc1	Němec
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
osídlili	osídlit	k5eAaPmAgMnP	osídlit
<g/>
,	,	kIx,	,
půdu	půda	k1gFnSc4	půda
zúrodnili	zúrodnit	k5eAaPmAgMnP	zúrodnit
a	a	k8xC	a
vesměs	vesměs	k6eAd1	vesměs
úspěšně	úspěšně	k6eAd1	úspěšně
zde	zde	k6eAd1	zde
hospodařili	hospodařit	k5eAaImAgMnP	hospodařit
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c4	o
obyvatele	obyvatel	k1gMnPc4	obyvatel
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kolonisty	kolonista	k1gMnPc7	kolonista
kteří	který	k3yQgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
českých	český	k2eAgNnPc2d1	české
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
i	i	k9	i
obyvatelé	obyvatel	k1gMnPc1	obyvatel
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
však	však	k9	však
poněmčili	poněmčit	k5eAaPmAgMnP	poněmčit
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
například	například	k6eAd1	například
Nymburk	Nymburk	k1gInSc4	Nymburk
osídlili	osídlit	k5eAaPmAgMnP	osídlit
němečtí	německý	k2eAgMnPc1d1	německý
a	a	k8xC	a
holandští	holandský	k2eAgMnPc1d1	holandský
kolonisté	kolonista	k1gMnPc1	kolonista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
počeštili	počeštit	k5eAaPmAgMnP	počeštit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgNnPc1	tento
dvě	dva	k4xCgNnPc1	dva
etnika	etnikum	k1gNnPc1	etnikum
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
prostoru	prostor	k1gInSc6	prostor
pospolu	pospolu	k6eAd1	pospolu
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
staletí	staletí	k1gNnPc4	staletí
<g/>
,	,	kIx,	,
mísila	mísit	k5eAaImAgFnS	mísit
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
kultura	kultura	k1gFnSc1	kultura
českou	český	k2eAgFnSc4d1	Česká
výrazně	výrazně	k6eAd1	výrazně
obohatila	obohatit	k5eAaPmAgFnS	obohatit
<g/>
,	,	kIx,	,
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
ji	on	k3xPp3gFnSc4	on
vytvářet	vytvářet	k5eAaImF	vytvářet
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc7	její
pevnou	pevný	k2eAgFnSc7d1	pevná
součástí	součást	k1gFnSc7	součást
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
kolem	kolem	k7c2	kolem
jejich	jejich	k3xOp3gNnPc2	jejich
soužití	soužití	k1gNnPc2	soužití
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vyhrocovat	vyhrocovat	k5eAaImF	vyhrocovat
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
nacionalismů	nacionalismus	k1gInPc2	nacionalismus
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
národnostní	národnostní	k2eAgInSc1d1	národnostní
rozměr	rozměr	k1gInSc1	rozměr
měly	mít	k5eAaImAgInP	mít
již	již	k6eAd1	již
husitské	husitský	k2eAgFnSc2d1	husitská
bouře	bouř	k1gFnSc2	bouř
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
německé	německý	k2eAgNnSc1d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
ve	v	k7c6	v
městech	město	k1gNnPc6	město
i	i	k8xC	i
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
stavělo	stavět	k5eAaImAgNnS	stavět
proti	proti	k7c3	proti
kalichu	kalich	k1gInSc3	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Hasištejnský	Hasištejnský	k1gMnSc1	Hasištejnský
napsal	napsat	k5eAaBmAgMnS	napsat
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pražané	Pražan	k1gMnPc1	Pražan
"	"	kIx"	"
<g/>
k	k	k7c3	k
cizím	cizí	k2eAgFnPc3d1	cizí
jsou	být	k5eAaImIp3nP	být
přívětiví	přívětivý	k2eAgMnPc1d1	přívětivý
<g/>
,	,	kIx,	,
toliko	toliko	k6eAd1	toliko
Němcův	Němcův	k2eAgMnSc1d1	Němcův
nenávidí	návidět	k5eNaImIp3nS	návidět
<g/>
,	,	kIx,	,
majíce	mít	k5eAaImSgFnP	mít
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
protivníky	protivník	k1gMnPc7	protivník
smýšlení	smýšlení	k1gNnSc2	smýšlení
svého	svůj	k3xOyFgMnSc2	svůj
u	u	k7c2	u
víře	víra	k1gFnSc6	víra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1495	[number]	k4	1495
cech	cech	k1gInSc4	cech
řezníků	řezník	k1gMnPc2	řezník
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
převážně	převážně	k6eAd1	převážně
německý	německý	k2eAgInSc4d1	německý
ráz	ráz	k1gInSc4	ráz
<g/>
,	,	kIx,	,
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
cechu	cech	k1gInSc2	cech
nechtěl	chtít	k5eNaImAgMnS	chtít
přijímat	přijímat	k5eAaImF	přijímat
žádné	žádný	k3yNgFnPc4	žádný
Čechy	Čechy	k1gFnPc4	Čechy
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pehm	Pehm	k1gInSc1	Pehm
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
řezníky	řezník	k1gMnPc7	řezník
"	"	kIx"	"
<g/>
dobrého	dobrý	k2eAgInSc2d1	dobrý
německého	německý	k2eAgInSc2d1	německý
druhu	druh	k1gInSc2	druh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
guter	guter	k1gInSc1	guter
deutscher	deutschra	k1gFnPc2	deutschra
Art	Art	k1gFnSc2	Art
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Národnostní	národnostní	k2eAgFnPc1d1	národnostní
třenice	třenice	k1gFnPc1	třenice
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
i	i	k9	i
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
usnesla	usnést	k5eAaPmAgFnS	usnést
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
městské	městský	k2eAgNnSc1d1	Městské
právo	právo	k1gNnSc1	právo
nebylo	být	k5eNaImAgNnS	být
udělováno	udělovat	k5eAaImNgNnS	udělovat
Němcům	Němec	k1gMnPc3	Němec
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kteříž	kteříž	k?	kteříž
nic	nic	k3yNnSc4	nic
česky	česky	k6eAd1	česky
neumějí	umět	k5eNaImIp3nP	umět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1514	[number]	k4	1514
usnesla	usnést	k5eAaPmAgFnS	usnést
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
do	do	k7c2	do
města	město	k1gNnSc2	město
nebyli	být	k5eNaImAgMnP	být
přijímání	přijímání	k1gNnSc4	přijímání
přichozí	přichohý	k2eAgMnPc1d1	přichohý
Němci	Němec	k1gMnPc1	Němec
ani	ani	k8xC	ani
jiní	jiný	k2eAgMnPc1d1	jiný
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
deputace	deputace	k1gFnSc1	deputace
litoměřických	litoměřický	k2eAgMnPc2d1	litoměřický
konšelů	konšel	k1gMnPc2	konšel
napadena	napadnout	k5eAaPmNgFnS	napadnout
německým	německý	k2eAgNnSc7d1	německé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jim	on	k3xPp3gMnPc3	on
spílalo	spílat	k5eAaImAgNnS	spílat
do	do	k7c2	do
"	"	kIx"	"
<g/>
kacířů	kacíř	k1gMnPc2	kacíř
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Česko-německé	českoěmecký	k2eAgInPc1d1	česko-německý
vztahy	vztah	k1gInPc1	vztah
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
volební	volební	k2eAgFnSc2d1	volební
agitace	agitace	k1gFnSc2	agitace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jazyk	jazyk	k1gInSc1	jazyk
stal	stát	k5eAaPmAgInS	stát
politickým	politický	k2eAgInSc7d1	politický
faktorem	faktor	k1gInSc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1897	[number]	k4	1897
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgMnSc4d1	vydán
Badeniho	Badeni	k1gMnSc4	Badeni
jazyková	jazykový	k2eAgNnPc4d1	jazykové
nařízení	nařízení	k1gNnSc4	nařízení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zrovnoprávnila	zrovnoprávnit	k5eAaPmAgFnS	zrovnoprávnit
češtinu	čeština	k1gFnSc4	čeština
s	s	k7c7	s
němčinou	němčina	k1gFnSc7	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
poslanci	poslanec	k1gMnPc1	poslanec
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
zrovnoprávnění	zrovnoprávnění	k1gNnSc3	zrovnoprávnění
češtiny	čeština	k1gFnSc2	čeština
zahájili	zahájit	k5eAaPmAgMnP	zahájit
obstrukce	obstrukce	k1gFnSc2	obstrukce
v	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
1897	[number]	k4	1897
se	se	k3xPyFc4	se
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
oblastech	oblast	k1gFnPc6	oblast
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
konaly	konat	k5eAaImAgFnP	konat
protivládní	protivládní	k2eAgFnPc1d1	protivládní
a	a	k8xC	a
protičeské	protičeský	k2eAgFnPc1d1	protičeská
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
někdy	někdy	k6eAd1	někdy
přerůstaly	přerůstat	k5eAaImAgFnP	přerůstat
i	i	k9	i
v	v	k7c6	v
drancování	drancování	k1gNnSc6	drancování
českých	český	k2eAgNnPc2d1	české
obydlí	obydlí	k1gNnPc2	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
demisi	demise	k1gFnSc6	demise
Badeniho	Badeni	k1gMnSc2	Badeni
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1897	[number]	k4	1897
se	se	k3xPyFc4	se
národnostní	národnostní	k2eAgInPc1d1	národnostní
nepokoje	nepokoj	k1gInPc1	nepokoj
přenesly	přenést	k5eAaPmAgInP	přenést
také	také	k9	také
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1908	[number]	k4	1908
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
několikadenní	několikadenní	k2eAgFnSc2d1	několikadenní
pouliční	pouliční	k2eAgFnSc2d1	pouliční
bitky	bitka	k1gFnSc2	bitka
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
dav	dav	k1gInSc1	dav
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
i	i	k9	i
na	na	k7c4	na
pražské	pražský	k2eAgNnSc4d1	Pražské
německé	německý	k2eAgNnSc4d1	německé
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
==	==	k?	==
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
-	-	kIx~	-
zejména	zejména	k9	zejména
zpočátku	zpočátku	k6eAd1	zpočátku
-	-	kIx~	-
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
uznat	uznat	k5eAaPmF	uznat
vytvoření	vytvoření	k1gNnSc4	vytvoření
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
naopak	naopak	k6eAd1	naopak
Češi	Čech	k1gMnPc1	Čech
chápali	chápat	k5eAaImAgMnP	chápat
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
národní	národní	k2eAgInSc4d1	národní
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechtělo	chtít	k5eNaImAgNnS	chtít
se	se	k3xPyFc4	se
smířit	smířit	k5eAaPmF	smířit
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
pozice	pozice	k1gFnSc2	pozice
majoritní	majoritní	k2eAgFnSc2d1	majoritní
národnosti	národnost	k1gFnSc2	národnost
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
podunajské	podunajský	k2eAgFnSc2d1	Podunajská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
28	[number]	k4	28
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc6	říjen
1918	[number]	k4	1918
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
pokusili	pokusit	k5eAaPmAgMnP	pokusit
odvolat	odvolat	k5eAaPmF	odvolat
na	na	k7c4	na
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
vycházející	vycházející	k2eAgNnSc4d1	vycházející
z	z	k7c2	z
mírové	mírový	k2eAgFnSc2d1	mírová
politiky	politika	k1gFnSc2	politika
Woodrowa	Woodrowus	k1gMnSc2	Woodrowus
Wilsona	Wilson	k1gMnSc2	Wilson
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
oblasti	oblast	k1gFnPc1	oblast
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
většinou	většina	k1gFnSc7	většina
setrvaly	setrvat	k5eAaPmAgFnP	setrvat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
poválečného	poválečný	k2eAgInSc2d1	poválečný
rakouského	rakouský	k2eAgInSc2d1	rakouský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Sudetoněmečtí	sudetoněmecký	k2eAgMnPc1d1	sudetoněmecký
poslanci	poslanec	k1gMnPc1	poslanec
z	z	k7c2	z
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
separatistické	separatistický	k2eAgInPc1d1	separatistický
celky	celek	k1gInPc1	celek
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
připojit	připojit	k5eAaPmF	připojit
se	se	k3xPyFc4	se
ke	k	k7c3	k
vznikajícímu	vznikající	k2eAgNnSc3d1	vznikající
německému	německý	k2eAgNnSc3d1	německé
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
provincie	provincie	k1gFnPc4	provincie
Německé	německý	k2eAgFnPc4d1	německá
Čechy	Čechy	k1gFnPc4	Čechy
(	(	kIx(	(
<g/>
Deutschböhmen	Deutschböhmen	k1gInSc1	Deutschböhmen
<g/>
)	)	kIx)	)
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
a	a	k8xC	a
Sudetsko	Sudetsko	k1gNnSc1	Sudetsko
(	(	kIx(	(
<g/>
Sudetenland	Sudetenland	k1gInSc1	Sudetenland
<g/>
)	)	kIx)	)
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
další	další	k2eAgMnPc1d1	další
převážně	převážně	k6eAd1	převážně
německé	německý	k2eAgFnSc3d1	německá
oblasti	oblast	k1gFnSc3	oblast
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
připojeny	připojit	k5eAaPmNgInP	připojit
k	k	k7c3	k
Horním	horní	k2eAgFnPc3d1	horní
a	a	k8xC	a
Dolním	dolní	k2eAgFnPc3d1	dolní
Rakousám	Rakousám	k1gFnPc3	Rakousám
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaPmF	stát
součástí	součást	k1gFnSc7	součást
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
republiky	republika	k1gFnSc2	republika
i	i	k8xC	i
města	město	k1gNnSc2	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
a	a	k8xC	a
Jihlava	Jihlava	k1gFnSc1	Jihlava
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
žádnou	žádný	k3yNgFnSc4	žádný
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
separatistických	separatistický	k2eAgFnPc2d1	separatistická
snah	snaha	k1gFnPc2	snaha
neuznala	uznat	k5eNaPmAgFnS	uznat
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
československé	československý	k2eAgNnSc4d1	Československé
území	území	k1gNnSc4	území
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
operací	operace	k1gFnSc7	operace
<g/>
.	.	kIx.	.
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
se	se	k3xPyFc4	se
uchýlili	uchýlit	k5eAaPmAgMnP	uchýlit
k	k	k7c3	k
víceméně	víceméně	k9	víceméně
pasivnímu	pasivní	k2eAgNnSc3d1	pasivní
odporu	odpor	k1gInSc2	odpor
vůči	vůči	k7c3	vůči
českému	český	k2eAgNnSc3d1	české
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
postupně	postupně	k6eAd1	postupně
obsazovalo	obsazovat	k5eAaImAgNnS	obsazovat
pohraničí	pohraničí	k1gNnSc1	pohraničí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bojům	boj	k1gInPc3	boj
nebo	nebo	k8xC	nebo
krvavým	krvavý	k2eAgFnPc3d1	krvavá
srážkám	srážka	k1gFnPc3	srážka
docházelo	docházet	k5eAaImAgNnS	docházet
pouze	pouze	k6eAd1	pouze
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ztráty	ztráta	k1gFnSc2	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
desítky	desítka	k1gFnPc1	desítka
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1919	[number]	k4	1919
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
pokojně	pokojně	k6eAd1	pokojně
demonstrovali	demonstrovat	k5eAaBmAgMnP	demonstrovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
demonstrace	demonstrace	k1gFnPc1	demonstrace
byly	být	k5eAaImAgFnP	být
podpořeny	podpořit	k5eAaPmNgFnP	podpořit
jednodenní	jednodenní	k2eAgFnSc7d1	jednodenní
generální	generální	k2eAgFnSc7d1	generální
stávkou	stávka	k1gFnSc7	stávka
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnPc1	demonstrace
byly	být	k5eAaImAgFnP	být
potlačeny	potlačit	k5eAaPmNgFnP	potlačit
československou	československý	k2eAgFnSc7d1	Československá
armádou	armáda	k1gFnSc7	armáda
s	s	k7c7	s
54	[number]	k4	54
mrtvými	mrtvý	k1gMnPc7	mrtvý
a	a	k8xC	a
84	[number]	k4	84
zraněnými	zraněný	k2eAgFnPc7d1	zraněná
osobami	osoba	k1gFnPc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
demonstrace	demonstrace	k1gFnPc4	demonstrace
např.	např.	kA	např.
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgFnSc6d1	Moravská
Třebové	Třebová	k1gFnSc6	Třebová
nebo	nebo	k8xC	nebo
Kadani	Kadaň	k1gFnSc6	Kadaň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zabitými	zabitý	k2eAgMnPc7d1	zabitý
Němci	Němec	k1gMnPc7	Němec
byly	být	k5eAaImAgInP	být
desítky	desítka	k1gFnPc4	desítka
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
<g/>
Mírová	mírový	k2eAgFnSc1d1	mírová
Saint-germainská	Saintermainský	k2eAgFnSc1d1	Saint-germainská
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1919	[number]	k4	1919
definitivně	definitivně	k6eAd1	definitivně
přiřkla	přiřknout	k5eAaPmAgFnS	přiřknout
sudetské	sudetský	k2eAgMnPc4d1	sudetský
území	území	k1gNnSc2	území
československému	československý	k2eAgInSc3d1	československý
státu	stát	k1gInSc3	stát
a	a	k8xC	a
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
tak	tak	k6eAd1	tak
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
roli	role	k1gFnSc6	role
početné	početný	k2eAgFnSc2d1	početná
národnostní	národnostní	k2eAgFnSc2d1	národnostní
menšiny	menšina	k1gFnSc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
bylo	být	k5eAaImAgNnS	být
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
mnohonárodnostního	mnohonárodnostní	k2eAgNnSc2d1	mnohonárodnostní
Československa	Československo	k1gNnSc2	Československo
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
6,6	[number]	k4	6,6
milionů	milion	k4xCgInPc2	milion
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
3,3	[number]	k4	3,3
milionů	milion	k4xCgInPc2	milion
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
2	[number]	k4	2
milionů	milion	k4xCgInPc2	milion
Slováků	Slovák	k1gMnPc2	Slovák
(	(	kIx(	(
<g/>
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Čechy	Čech	k1gMnPc7	Čech
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
často	často	k6eAd1	často
počítáni	počítán	k2eAgMnPc1d1	počítán
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
československého	československý	k2eAgInSc2d1	československý
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
700	[number]	k4	700
000	[number]	k4	000
Maďarů	maďar	k1gInPc2	maďar
<g/>
,	,	kIx,	,
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
Rusínů	Rusín	k1gMnPc2	Rusín
<g/>
,	,	kIx,	,
300	[number]	k4	300
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
100	[number]	k4	100
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
z	z	k7c2	z
Romů	Rom	k1gMnPc2	Rom
(	(	kIx(	(
<g/>
Cikánů	cikán	k1gMnPc2	cikán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
tvořili	tvořit	k5eAaImAgMnP	tvořit
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
veškerého	veškerý	k3xTgNnSc2	veškerý
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
cca	cca	kA	cca
23,4	[number]	k4	23,4
procent	procento	k1gNnPc2	procento
československé	československý	k2eAgFnSc2d1	Československá
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
čítající	čítající	k2eAgMnSc1d1	čítající
celkem	celkem	k6eAd1	celkem
13,6	[number]	k4	13,6
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sudet	Sudety	k1gFnPc2	Sudety
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
významný	významný	k2eAgInSc1d1	významný
podíl	podíl	k1gInSc1	podíl
chemického	chemický	k2eAgInSc2d1	chemický
<g/>
,	,	kIx,	,
těžebního	těžební	k2eAgInSc2d1	těžební
<g/>
,	,	kIx,	,
textilního	textilní	k2eAgInSc2d1	textilní
a	a	k8xC	a
sklářského	sklářský	k2eAgInSc2d1	sklářský
průmyslu	průmysl	k1gInSc2	průmysl
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějším	výrazný	k2eAgNnSc7d3	nejvýraznější
centrem	centrum	k1gNnSc7	centrum
všeněmeckého	všeněmecký	k2eAgInSc2d1	všeněmecký
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Cheb	Cheb	k1gInSc1	Cheb
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
obývali	obývat	k5eAaImAgMnP	obývat
i	i	k9	i
zaostalé	zaostalý	k2eAgFnPc4d1	zaostalá
zemědělské	zemědělský	k2eAgFnPc4d1	zemědělská
oblasti	oblast	k1gFnPc4	oblast
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
jihozápadě	jihozápad	k1gInSc6	jihozápad
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
byla	být	k5eAaImAgFnS	být
většinově	většinově	k6eAd1	většinově
německým	německý	k2eAgNnSc7d1	německé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
osídlena	osídlit	k5eAaPmNgFnS	osídlit
především	především	k9	především
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
významnější	významný	k2eAgFnPc4d2	významnější
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
německojazyčné	německojazyčný	k2eAgFnPc1d1	německojazyčná
"	"	kIx"	"
<g/>
kapsy	kapsa	k1gFnPc1	kapsa
<g/>
"	"	kIx"	"
kolem	kolem	k7c2	kolem
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
Brna	Brno	k1gNnSc2	Brno
či	či	k8xC	či
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
byl	být	k5eAaImAgInS	být
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
částech	část	k1gFnPc6	část
poněkud	poněkud	k6eAd1	poněkud
utlumen	utlumen	k2eAgMnSc1d1	utlumen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
platilo	platit	k5eAaImAgNnS	platit
i	i	k9	i
o	o	k7c6	o
oblasti	oblast	k1gFnSc6	oblast
Českého	český	k2eAgNnSc2d1	české
Slezska	Slezsko	k1gNnSc2	Slezsko
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
těžebním	těžební	k2eAgInSc7d1	těžební
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tamní	tamní	k2eAgMnPc1d1	tamní
Němci	Němec	k1gMnPc1	Němec
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
obávali	obávat	k5eAaImAgMnP	obávat
silné	silný	k2eAgFnPc4d1	silná
konkurence	konkurence	k1gFnPc4	konkurence
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
etničtí	etnický	k2eAgMnPc1d1	etnický
Němci	Němec	k1gMnPc1	Němec
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
územně	územně	k6eAd1	územně
celistvých	celistvý	k2eAgFnPc6d1	celistvá
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
vymezitelných	vymezitelný	k2eAgFnPc6d1	vymezitelná
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
bylo	být	k5eAaImAgNnS	být
české	český	k2eAgNnSc1d1	české
a	a	k8xC	a
německé	německý	k2eAgNnSc1d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
smíšené	smíšený	k2eAgNnSc1d1	smíšené
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
zástupců	zástupce	k1gMnPc2	zástupce
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
měla	mít	k5eAaImAgFnS	mít
alespoň	alespoň	k9	alespoň
částečné	částečný	k2eAgFnPc4d1	částečná
znalosti	znalost	k1gFnPc4	znalost
jazyka	jazyk	k1gInSc2	jazyk
druhé	druhý	k4xOgFnSc2	druhý
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
duplicitní	duplicitní	k2eAgFnSc2d1	duplicitní
etnicky	etnicky	k6eAd1	etnicky
diverzifikované	diverzifikovaný	k2eAgFnSc2d1	diverzifikovaná
struktury	struktura	k1gFnSc2	struktura
kulturních	kulturní	k2eAgFnPc2d1	kulturní
<g/>
,	,	kIx,	,
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
<g/>
,	,	kIx,	,
politických	politický	k2eAgFnPc2d1	politická
a	a	k8xC	a
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
prohlubovalo	prohlubovat	k5eAaImAgNnS	prohlubovat
a	a	k8xC	a
upevňovalo	upevňovat	k5eAaImAgNnS	upevňovat
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
národnostní	národnostní	k2eAgFnSc4d1	národnostní
izolaci	izolace	k1gFnSc4	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
zastoupení	zastoupení	k1gNnSc1	zastoupení
v	v	k7c6	v
orgánech	orgán	k1gInPc6	orgán
RČS	RČS	kA	RČS
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
měli	mít	k5eAaImAgMnP	mít
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
své	svůj	k3xOyFgNnSc4	svůj
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
Třetí	třetí	k4xOgFnSc1	třetí
vláda	vláda	k1gFnSc1	vláda
Antonína	Antonín	k1gMnSc2	Antonín
Švehly	Švehla	k1gMnSc2	Švehla
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
své	svůj	k3xOyFgNnSc4	svůj
zastoupení	zastoupení	k1gNnSc4	zastoupení
i	i	k9	i
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
republiky	republika	k1gFnSc2	republika
volila	volit	k5eAaImAgFnS	volit
většina	většina	k1gFnSc1	většina
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
převážně	převážně	k6eAd1	převážně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
se	s	k7c7	s
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
vlnou	vlna	k1gFnSc7	vlna
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
vzrůstaly	vzrůstat	k5eAaImAgInP	vzrůstat
i	i	k9	i
hlasy	hlas	k1gInPc1	hlas
pro	pro	k7c4	pro
populisty	populista	k1gMnPc4	populista
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
dokonce	dokonce	k9	dokonce
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
strana	strana	k1gFnSc1	strana
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Masaryk	Masaryk	k1gMnSc1	Masaryk
však	však	k9	však
předsedu	předseda	k1gMnSc4	předseda
strany	strana	k1gFnSc2	strana
Henleina	Henleina	k1gFnSc1	Henleina
nepověřil	pověřit	k5eNaPmAgInS	pověřit
sestavením	sestavení	k1gNnSc7	sestavení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zastoupení	zastoupení	k1gNnSc4	zastoupení
německých	německý	k2eAgFnPc2d1	německá
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
NS	NS	kA	NS
RČS	RČS	kA	RČS
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
==	==	k?	==
</s>
</p>
<p>
<s>
Národnostní	národnostní	k2eAgFnSc1d1	národnostní
politika	politika	k1gFnSc1	politika
Československa	Československo	k1gNnSc2	Československo
vycházela	vycházet	k5eAaImAgFnS	vycházet
sice	sice	k8xC	sice
z	z	k7c2	z
idey	idea	k1gFnSc2	idea
národního	národní	k2eAgInSc2d1	národní
státu	stát	k1gInSc2	stát
"	"	kIx"	"
<g/>
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
systém	systém	k1gInSc1	systém
měl	mít	k5eAaImAgInS	mít
mnohé	mnohý	k2eAgInPc4d1	mnohý
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
přesto	přesto	k8xC	přesto
chovalo	chovat	k5eAaImAgNnS	chovat
k	k	k7c3	k
menšinám	menšina	k1gFnPc3	menšina
na	na	k7c6	na
tehdy	tehdy	k6eAd1	tehdy
existující	existující	k2eAgInPc4d1	existující
poměry	poměr	k1gInPc4	poměr
vzorně	vzorně	k6eAd1	vzorně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
doložily	doložit	k5eAaPmAgInP	doložit
i	i	k9	i
předválečné	předválečný	k2eAgInPc1d1	předválečný
výzkumy	výzkum	k1gInPc1	výzkum
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
volených	volený	k2eAgMnPc2d1	volený
zástupců	zástupce	k1gMnPc2	zástupce
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
prvorepublikové	prvorepublikový	k2eAgFnSc6d1	prvorepubliková
správě	správa	k1gFnSc6	správa
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
německých	německý	k2eAgFnPc2d1	německá
základních	základní	k2eAgFnPc2d1	základní
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc2d1	střední
i	i	k8xC	i
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
či	či	k8xC	či
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
knih	kniha	k1gFnPc2	kniha
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
jazyce	jazyk	k1gInSc6	jazyk
byl	být	k5eAaImAgInS	být
daleko	daleko	k6eAd1	daleko
široko	široko	k6eAd1	široko
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
<g/>
procentní	procentní	k2eAgFnSc1d1	procentní
menšina	menšina	k1gFnSc1	menšina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
druhým	druhý	k4xOgInSc7	druhý
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
jazyk	jazyk	k1gInSc1	jazyk
menšiny	menšina	k1gFnSc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Národnostní	národnostní	k2eAgFnPc1d1	národnostní
menšiny	menšina	k1gFnPc1	menšina
mohly	moct	k5eAaImAgFnP	moct
svobodně	svobodně	k6eAd1	svobodně
zakládat	zakládat	k5eAaImF	zakládat
organizace	organizace	k1gFnPc4	organizace
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
spolky	spolek	k1gInPc4	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
státu	stát	k1gInSc2	stát
a	a	k8xC	a
občanů	občan	k1gMnPc2	občan
nestál	stát	k5eNaImAgInS	stát
na	na	k7c6	na
národnostním	národnostní	k2eAgInSc6d1	národnostní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
občanském	občanský	k2eAgInSc6d1	občanský
principu	princip	k1gInSc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Němci	Němec	k1gMnPc1	Němec
se	se	k3xPyFc4	se
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
jako	jako	k8xC	jako
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
vlastí	vlast	k1gFnSc7	vlast
ve	v	k7c6	v
valné	valný	k2eAgFnSc6d1	valná
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
neztotožňovali	ztotožňovat	k5eNaImAgMnP	ztotožňovat
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
situaci	situace	k1gFnSc4	situace
mírně	mírně	k6eAd1	mírně
napomohl	napomoct	k5eAaPmAgMnS	napomoct
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
míra	míra	k1gFnSc1	míra
svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
dávala	dávat	k5eAaImAgFnS	dávat
jasně	jasně	k6eAd1	jasně
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
profilovat	profilovat	k5eAaImF	profilovat
na	na	k7c6	na
představách	představa	k1gFnPc6	představa
"	"	kIx"	"
<g/>
státního	státní	k2eAgInSc2d1	státní
národa	národ	k1gInSc2	národ
Československého	československý	k2eAgInSc2d1	československý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
oslabit	oslabit	k5eAaPmF	oslabit
vliv	vliv	k1gInSc4	vliv
německých	německý	k2eAgNnPc2d1	německé
politických	politický	k2eAgNnPc2d1	politické
hnutí	hnutí	k1gNnPc2	hnutí
a	a	k8xC	a
organizací	organizace	k1gFnPc2	organizace
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
upravovala	upravovat	k5eAaImAgFnS	upravovat
volební	volební	k2eAgInPc4d1	volební
obvody	obvod	k1gInPc4	obvod
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
Češi	Čech	k1gMnPc1	Čech
vždy	vždy	k6eAd1	vždy
získali	získat	k5eAaPmAgMnP	získat
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zájmy	zájem	k1gInPc1	zájem
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
zprvu	zprvu	k6eAd1	zprvu
hájili	hájit	k5eAaImAgMnP	hájit
i	i	k9	i
komunisté	komunista	k1gMnPc1	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
pronesl	pronést	k5eAaPmAgMnS	pronést
komunistický	komunistický	k2eAgMnSc1d1	komunistický
poslanec	poslanec	k1gMnSc1	poslanec
Václav	Václav	k1gMnSc1	Václav
Kopecký	Kopecký	k1gMnSc1	Kopecký
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
ostře	ostro	k6eAd1	ostro
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
vládu	vláda	k1gFnSc4	vláda
za	za	k7c4	za
pošlapání	pošlapání	k1gNnSc4	pošlapání
práv	právo	k1gNnPc2	právo
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunisté	komunista	k1gMnPc1	komunista
budou	být	k5eAaImBp3nP	být
hájit	hájit	k5eAaImF	hájit
jejich	jejich	k3xOp3gNnSc4	jejich
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
ale	ale	k9	ale
komunisté	komunista	k1gMnPc1	komunista
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Kominterny	Kominterna	k1gFnSc2	Kominterna
otočili	otočit	k5eAaPmAgMnP	otočit
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
hájit	hájit	k5eAaImF	hájit
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Československo	Československo	k1gNnSc4	Československo
proti	proti	k7c3	proti
nacismu	nacismus	k1gInSc3	nacismus
<g/>
.	.	kIx.	.
<g/>
Světová	světový	k2eAgFnSc1d1	světová
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
německé	německý	k2eAgNnSc4d1	německé
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Pohraničí	pohraničí	k1gNnSc1	pohraničí
bylo	být	k5eAaImAgNnS	být
navázáno	navázat	k5eAaPmNgNnS	navázat
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
závody	závod	k1gInPc4	závod
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
získávalo	získávat	k5eAaImAgNnS	získávat
jak	jak	k8xS	jak
dodávky	dodávka	k1gFnPc1	dodávka
<g/>
,	,	kIx,	,
tak	tak	k9	tak
tam	tam	k6eAd1	tam
vyváželo	vyvážet	k5eAaImAgNnS	vyvážet
svoje	svůj	k3xOyFgInPc4	svůj
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Nedobrá	dobrý	k2eNgFnSc1d1	nedobrá
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
prohloubená	prohloubený	k2eAgNnPc4d1	prohloubené
vysokým	vysoký	k2eAgInSc7d1	vysoký
směnným	směnný	k2eAgInSc7d1	směnný
kurzem	kurz	k1gInSc7	kurz
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
a	a	k8xC	a
prudký	prudký	k2eAgInSc1d1	prudký
růst	růst	k1gInSc1	růst
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
(	(	kIx(	(
<g/>
až	až	k9	až
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
projevil	projevit	k5eAaPmAgInS	projevit
na	na	k7c6	na
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
začínalo	začínat	k5eAaImAgNnS	začínat
sympatizovat	sympatizovat	k5eAaImF	sympatizovat
s	s	k7c7	s
krajně	krajně	k6eAd1	krajně
nacionalistickými	nacionalistický	k2eAgInPc7d1	nacionalistický
názory	názor	k1gInPc7	názor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
představoval	představovat	k5eAaImAgMnS	představovat
Konrád	Konrád	k1gMnSc1	Konrád
Henlein	Henlein	k1gMnSc1	Henlein
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
Sudetendeutsche	Sudetendeutsche	k1gFnSc1	Sudetendeutsche
Heimatsfront	Heimatsfront	k1gMnSc1	Heimatsfront
(	(	kIx(	(
<g/>
SHF	SHF	kA	SHF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přeměněné	přeměněný	k2eAgInPc1d1	přeměněný
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
Sudetendeutsche	Sudetendeutsche	k1gNnSc6	Sudetendeutsche
Partei	Parte	k1gFnSc2	Parte
(	(	kIx(	(
<g/>
SdP	SdP	k1gMnSc1	SdP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Henlein	Henlein	k1gMnSc1	Henlein
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
k	k	k7c3	k
otevřenému	otevřený	k2eAgInSc3d1	otevřený
odporu	odpor	k1gInSc3	odpor
proti	proti	k7c3	proti
české	český	k2eAgFnSc3d1	Česká
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
Němců	Němec	k1gMnPc2	Němec
začalo	začít	k5eAaPmAgNnS	začít
jeho	jeho	k3xOp3gInPc4	jeho
názory	názor	k1gInPc4	názor
přijímat	přijímat	k5eAaImF	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecních	obecní	k2eAgFnPc6d1	obecní
volbách	volba	k1gFnPc6	volba
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1938	[number]	k4	1938
získala	získat	k5eAaPmAgFnS	získat
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
SdP	SdP	k1gFnSc1	SdP
<g/>
)	)	kIx)	)
bezmála	bezmála	k6eAd1	bezmála
90	[number]	k4	90
<g/>
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
sudetoněmeckých	sudetoněmecký	k2eAgInPc2d1	sudetoněmecký
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
<g/>
Situace	situace	k1gFnSc1	situace
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
dekády	dekáda	k1gFnSc2	dekáda
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
zahájili	zahájit	k5eAaPmAgMnP	zahájit
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
protesty	protest	k1gInPc4	protest
a	a	k8xC	a
masový	masový	k2eAgInSc4d1	masový
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ozbrojeného	ozbrojený	k2eAgNnSc2d1	ozbrojené
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
využilo	využít	k5eAaPmAgNnS	využít
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
vlastní	vlastní	k2eAgFnSc2d1	vlastní
politiky	politika	k1gFnSc2	politika
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
a	a	k8xC	a
svolání	svolání	k1gNnSc2	svolání
konference	konference	k1gFnSc2	konference
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
,	,	kIx,	,
známého	známý	k1gMnSc2	známý
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
nechvalně	chvalně	k6eNd1	chvalně
proslulou	proslulý	k2eAgFnSc7d1	proslulá
Mnichovskou	mnichovský	k2eAgFnSc7d1	Mnichovská
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgFnP	být
Sudety	Sudety	k1gFnPc1	Sudety
připojeny	připojit	k5eAaPmNgFnP	připojit
k	k	k7c3	k
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
občany	občan	k1gMnPc7	občan
Říše	říš	k1gFnSc2	říš
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
narukovat	narukovat	k5eAaPmF	narukovat
do	do	k7c2	do
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
bývalí	bývalý	k2eAgMnPc1d1	bývalý
čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
přivítali	přivítat	k5eAaPmAgMnP	přivítat
tuto	tento	k3xDgFnSc4	tento
úpravu	úprava	k1gFnSc4	úprava
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
viděnou	viděný	k2eAgFnSc7d1	viděná
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k9	jako
krutou	krutý	k2eAgFnSc4d1	krutá
daň	daň	k1gFnSc4	daň
za	za	k7c4	za
zachování	zachování	k1gNnSc4	zachování
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
za	za	k7c2	za
války	válka	k1gFnSc2	válka
povětšinou	povětšinou	k6eAd1	povětšinou
sloužili	sloužit	k5eAaImAgMnP	sloužit
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
armádě	armáda	k1gFnSc6	armáda
(	(	kIx(	(
<g/>
Wehrmachtu	wehrmacht	k1gInSc6	wehrmacht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
aktivními	aktivní	k2eAgFnPc7d1	aktivní
odpůrci	odpůrce	k1gMnPc7	odpůrce
nacismu	nacismus	k1gInSc2	nacismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vysídlení	vysídlení	k1gNnSc2	vysídlení
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
koncem	konec	k1gInSc7	konec
války	válka	k1gFnSc2	válka
byli	být	k5eAaImAgMnP	být
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
výjimek	výjimka	k1gFnPc2	výjimka
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
odsunuti	odsunut	k2eAgMnPc1d1	odsunut
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc4	majetek
byl	být	k5eAaImAgInS	být
zabavován	zabavován	k2eAgMnSc1d1	zabavován
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
jich	on	k3xPp3gNnPc2	on
vysídlení	vysídlení	k1gNnPc2	vysídlení
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Rakouska	Rakousko	k1gNnSc2	Rakousko
nepřežilo	přežít	k5eNaPmAgNnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
vysídlení	vysídlení	k1gNnSc1	vysídlení
těchto	tento	k3xDgMnPc2	tento
obyvatel	obyvatel	k1gMnPc2	obyvatel
tématem	téma	k1gNnSc7	téma
mnoha	mnoho	k4c2	mnoho
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
se	se	k3xPyFc4	se
ztotožňovali	ztotožňovat	k5eAaImAgMnP	ztotožňovat
s	s	k7c7	s
nacistickou	nacistický	k2eAgFnSc7d1	nacistická
ideologií	ideologie	k1gFnSc7	ideologie
a	a	k8xC	a
nebyli	být	k5eNaImAgMnP	být
v	v	k7c6	v
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
politiků	politik	k1gMnPc2	politik
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
současná	současný	k2eAgFnSc1d1	současná
česká	český	k2eAgFnSc1d1	Česká
i	i	k8xC	i
někdejší	někdejší	k2eAgFnSc1d1	někdejší
komunistická	komunistický	k2eAgFnSc1d1	komunistická
historiografie	historiografie	k1gFnSc1	historiografie
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
termínů	termín	k1gInPc2	termín
odsun	odsun	k1gInSc4	odsun
či	či	k8xC	či
vysídlení	vysídlení	k1gNnSc4	vysídlení
<g/>
.	.	kIx.	.
</s>
<s>
Vysídlení	vysídlený	k2eAgMnPc1d1	vysídlený
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
mluvčím	mluvčí	k1gMnSc7	mluvčí
je	být	k5eAaImIp3nS	být
především	především	k9	především
Sudetoněmecké	sudetoněmecký	k2eAgNnSc1d1	Sudetoněmecké
krajanské	krajanský	k2eAgNnSc1d1	krajanské
sdružení	sdružení	k1gNnSc1	sdružení
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Sudetendeutsche	Sudetendeutsche	k1gInSc1	Sudetendeutsche
Landsmannschaft	Landsmannschaft	k1gInSc1	Landsmannschaft
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
vyhnání	vyhnání	k1gNnSc6	vyhnání
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Vertreibung	Vertreibung	k1gInSc1	Vertreibung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příčinou	příčina	k1gFnSc7	příčina
této	tento	k3xDgFnSc2	tento
jazykové	jazykový	k2eAgFnSc2d1	jazyková
dvojkolejnosti	dvojkolejnost	k1gFnSc2	dvojkolejnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
ani	ani	k8xC	ani
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
neexistuje	existovat	k5eNaImIp3nS	existovat
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
deportace	deportace	k1gFnPc4	deportace
bezpříznakové	bezpříznakový	k2eAgNnSc1d1	bezpříznakové
označení	označení	k1gNnSc1	označení
<g/>
:	:	kIx,	:
kdo	kdo	k3yInSc1	kdo
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
odsunu	odsun	k1gInSc6	odsun
nebo	nebo	k8xC	nebo
vysídlení	vysídlení	k1gNnSc6	vysídlení
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
automaticky	automaticky	k6eAd1	automaticky
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
jeho	on	k3xPp3gMnSc2	on
obhájce	obhájce	k1gMnSc2	obhájce
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
o	o	k7c6	o
vyhnání	vyhnání	k1gNnSc6	vyhnání
<g/>
,	,	kIx,	,
za	za	k7c4	za
kritika	kritik	k1gMnSc4	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplnost	úplnost	k1gFnSc4	úplnost
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
deportace	deportace	k1gFnPc4	deportace
užívá	užívat	k5eAaImIp3nS	užívat
výrazu	výraz	k1gInSc3	výraz
transfer	transfer	k1gInSc1	transfer
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
Postupimská	postupimský	k2eAgFnSc1d1	Postupimská
deklarace	deklarace	k1gFnSc1	deklarace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
–	–	k?	–
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
mu	on	k3xPp3gMnSc3	on
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
výraz	výraz	k1gInSc1	výraz
přesun	přesun	k1gInSc1	přesun
<g/>
.	.	kIx.	.
</s>
<s>
Česko-německá	českoěmecký	k2eAgFnSc1d1	česko-německá
deklarace	deklarace	k1gFnSc1	deklarace
o	o	k7c6	o
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
vztazích	vztah	k1gInPc6	vztah
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
budoucím	budoucí	k2eAgInSc6d1	budoucí
rozvoji	rozvoj	k1gInSc6	rozvoj
používá	používat	k5eAaImIp3nS	používat
slovní	slovní	k2eAgNnSc4d1	slovní
spojení	spojení	k1gNnSc4	spojení
"	"	kIx"	"
<g/>
poválečné	poválečný	k2eAgNnSc4d1	poválečné
vyhnání	vyhnání	k1gNnSc4	vyhnání
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
nucené	nucený	k2eAgNnSc1d1	nucené
vysídlení	vysídlení	k1gNnSc1	vysídlení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
verzi	verze	k1gFnSc6	verze
textu	text	k1gInSc2	text
"	"	kIx"	"
<g/>
Vertreibung	Vertreibung	k1gInSc1	Vertreibung
sowie	sowie	k1gFnSc2	sowie
zwangsweise	zwangsweise	k1gFnSc2	zwangsweise
Aussiedlung	Aussiedlung	k1gMnSc1	Aussiedlung
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
částečně	částečně	k6eAd1	částečně
analogickou	analogický	k2eAgFnSc4d1	analogická
situaci	situace	k1gFnSc4	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
opustit	opustit	k5eAaPmF	opustit
území	území	k1gNnSc4	území
Sudet	Sudety	k1gInPc2	Sudety
Češi	česat	k5eAaImIp1nS	česat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
vyhnání	vyhnání	k1gNnSc2	vyhnání
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
odsun	odsun	k1gInSc4	odsun
Čechů	Čech	k1gMnPc2	Čech
či	či	k8xC	či
vysídlení	vysídlení	k1gNnPc2	vysídlení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
ovšem	ovšem	k9	ovšem
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
Češi	Čech	k1gMnPc1	Čech
ze	z	k7c2	z
Sudet	Sudety	k1gInPc2	Sudety
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
vyhnáni	vyhnán	k2eAgMnPc1d1	vyhnán
byli	být	k5eAaImAgMnP	být
jen	jen	k9	jen
aktivní	aktivní	k2eAgMnPc1d1	aktivní
odpůrci	odpůrce	k1gMnPc1	odpůrce
henleinovců	henleinovec	k1gMnPc2	henleinovec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
včas	včas	k6eAd1	včas
utéci	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
Čechů	Čech	k1gMnPc2	Čech
přišlo	přijít	k5eAaPmAgNnS	přijít
do	do	k7c2	do
Sudet	Sudety	k1gInPc2	Sudety
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
poštmistři	poštmistr	k1gMnPc1	poštmistr
<g/>
,	,	kIx,	,
výpravčí	výpravčí	k1gMnPc1	výpravčí
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
státní	státní	k2eAgMnPc1d1	státní
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
hned	hned	k6eAd1	hned
propuštěni	propustit	k5eAaPmNgMnP	propustit
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
přišel	přijít	k5eAaPmAgMnS	přijít
Němec	Němec	k1gMnSc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
Češi	Čech	k1gMnPc1	Čech
odešli	odejít	k5eAaPmAgMnP	odejít
sami	sám	k3xTgMnPc1	sám
z	z	k7c2	z
existenčních	existenční	k2eAgInPc2d1	existenční
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
nebyli	být	k5eNaImAgMnP	být
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměli	mít	k5eNaImAgMnP	mít
tam	tam	k6eAd1	tam
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Odcházely	odcházet	k5eAaImAgFnP	odcházet
také	také	k9	také
rodiny	rodina	k1gFnPc1	rodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
více	hodně	k6eAd2	hodně
synů	syn	k1gMnPc2	syn
-	-	kIx~	-
báli	bát	k5eAaImAgMnP	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
povoláni	povolat	k5eAaPmNgMnP	povolat
do	do	k7c2	do
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
nějaký	nějaký	k3yIgMnSc1	nějaký
Čech	Čech	k1gMnSc1	Čech
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
wehrmachtu	wehrmacht	k1gInSc6	wehrmacht
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
tam	tam	k6eAd1	tam
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
národnosti	národnost	k1gFnSc3	národnost
<g/>
;	;	kIx,	;
Češi	česat	k5eAaImIp1nS	česat
rukovat	rukovat	k5eAaImF	rukovat
nemuseli	muset	k5eNaImAgMnP	muset
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počet	počet	k1gInSc1	počet
odsunutých	odsunutý	k2eAgMnPc2d1	odsunutý
Němců	Němec	k1gMnPc2	Němec
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
předválečného	předválečný	k2eAgNnSc2d1	předválečné
sčítání	sčítání	k1gNnSc2	sčítání
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
3,1	[number]	k4	3,1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
přihlásily	přihlásit	k5eAaPmAgFnP	přihlásit
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
těchto	tento	k3xDgNnPc2	tento
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
i	i	k9	i
odhad	odhad	k1gInSc1	odhad
počtu	počet	k1gInSc2	počet
odsunutých	odsunutý	k2eAgMnPc2d1	odsunutý
Němců	Němec	k1gMnPc2	Němec
po	po	k7c6	po
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
i	i	k9	i
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
dotkly	dotknout	k5eAaPmAgFnP	dotknout
válečné	válečný	k2eAgFnPc1d1	válečná
události	událost	k1gFnPc1	událost
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
až	až	k9	až
500	[number]	k4	500
tisíc	tisíc	k4xCgInPc2	tisíc
Sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
padlo	padnout	k5eAaPmAgNnS	padnout
jako	jako	k8xC	jako
vojáci	voják	k1gMnPc1	voják
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
a	a	k8xC	a
SS	SS	kA	SS
na	na	k7c6	na
frontách	fronta	k1gFnPc6	fronta
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
při	při	k7c6	při
válečných	válečný	k2eAgFnPc6d1	válečná
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
konference	konference	k1gFnSc2	konference
vítězných	vítězný	k2eAgFnPc2d1	vítězná
mocností	mocnost	k1gFnPc2	mocnost
v	v	k7c6	v
Postupimi	Postupim	k1gFnSc6	Postupim
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
minimálně	minimálně	k6eAd1	minimálně
700	[number]	k4	700
tisíc	tisíc	k4xCgInPc2	tisíc
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
divoce	divoce	k6eAd1	divoce
vyhnáno	vyhnán	k2eAgNnSc1d1	vyhnáno
anebo	anebo	k8xC	anebo
sami	sám	k3xTgMnPc1	sám
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jich	on	k3xPp3gMnPc2	on
cca	cca	kA	cca
300	[number]	k4	300
tisíc	tisíc	k4xCgInSc1	tisíc
uteklo	utéct	k5eAaPmAgNnS	utéct
samo	sám	k3xTgNnSc1	sám
před	před	k7c7	před
postupující	postupující	k2eAgFnSc7d1	postupující
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
Neronova	Neronův	k2eAgInSc2d1	Neronův
rozkazu	rozkaz	k1gInSc2	rozkaz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vydal	vydat	k5eAaPmAgMnS	vydat
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
o	o	k7c4	o
Němce	Němec	k1gMnPc4	Němec
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalších	další	k2eAgInPc2d1	další
cca	cca	kA	cca
400	[number]	k4	400
tisíc	tisíc	k4xCgInSc1	tisíc
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
vyhnáno	vyhnat	k5eAaPmNgNnS	vyhnat
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
divokém	divoký	k2eAgNnSc6d1	divoké
odsunu	odsunout	k5eAaPmIp1nS	odsunout
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
údajů	údaj	k1gInPc2	údaj
společné	společný	k2eAgFnSc2d1	společná
komise	komise	k1gFnSc2	komise
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
německých	německý	k2eAgMnPc2d1	německý
historiků	historik	k1gMnPc2	historik
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
19	[number]	k4	19
až	až	k8xS	až
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
nezvěstných	zvěstný	k2eNgMnPc2d1	nezvěstný
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
nebo	nebo	k8xC	nebo
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
až	až	k9	až
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nebo	nebo	k8xC	nebo
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
stali	stát	k5eAaPmAgMnP	stát
obětí	oběť	k1gFnSc7	oběť
starousedlíků	starousedlík	k1gMnPc2	starousedlík
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
je	on	k3xPp3gNnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
o	o	k7c4	o
vše	všechen	k3xTgNnSc4	všechen
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
obdržel	obdržet	k5eAaPmAgMnS	obdržet
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
výkaz	výkaz	k1gInSc1	výkaz
-	-	kIx~	-
pamětní	pamětní	k2eAgInSc1d1	pamětní
spis	spis	k1gInSc1	spis
od	od	k7c2	od
zplnomocněnce	zplnomocněnec	k1gMnSc2	zplnomocněnec
pro	pro	k7c4	pro
odsun	odsun	k1gInSc4	odsun
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
psalo	psát	k5eAaImAgNnS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
odsunuto	odsunout	k5eAaPmNgNnS	odsunout
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
170	[number]	k4	170
598	[number]	k4	598
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
počtu	počet	k1gInSc2	počet
1	[number]	k4	1
420	[number]	k4	420
598	[number]	k4	598
osob	osoba	k1gFnPc2	osoba
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
750	[number]	k4	750
000	[number]	k4	000
do	do	k7c2	do
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
Sudet	Sudety	k1gInPc2	Sudety
znovu	znovu	k6eAd1	znovu
dosídlena	dosídlen	k2eAgFnSc1d1	dosídlen
převážně	převážně	k6eAd1	převážně
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
hospodařit	hospodařit	k5eAaImF	hospodařit
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
však	však	k9	však
i	i	k9	i
ničit	ničit	k5eAaImF	ničit
a	a	k8xC	a
rozkrádat	rozkrádat	k5eAaImF	rozkrádat
<g/>
)	)	kIx)	)
na	na	k7c6	na
chalupách	chalupa	k1gFnPc6	chalupa
po	po	k7c6	po
odešlých	odešlý	k2eAgMnPc6d1	odešlý
Němcích	Němec	k1gMnPc6	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Dosídlování	Dosídlování	k1gNnSc4	Dosídlování
pohraničí	pohraničí	k1gNnSc2	pohraničí
byl	být	k5eAaImAgInS	být
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
najednou	najednou	k6eAd1	najednou
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
volných	volný	k2eAgInPc2d1	volný
objektů	objekt	k1gInPc2	objekt
než	než	k8xS	než
zájemců	zájemce	k1gMnPc2	zájemce
<g/>
.	.	kIx.	.
</s>
<s>
Dosídlování	Dosídlování	k1gNnSc1	Dosídlování
trvalo	trvat	k5eAaImAgNnS	trvat
zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgFnPc4	tři
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pohraničí	pohraničí	k1gNnSc2	pohraničí
vyrovnalo	vyrovnat	k5eAaBmAgNnS	vyrovnat
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
už	už	k9	už
jen	jen	k9	jen
historický	historický	k2eAgInSc1d1	historický
pojem	pojem	k1gInSc1	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Dosídlování	Dosídlování	k1gNnSc1	Dosídlování
probíhalo	probíhat	k5eAaImAgNnS	probíhat
postupně	postupně	k6eAd1	postupně
a	a	k8xC	a
nepravidelně	pravidelně	k6eNd1	pravidelně
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byla	být	k5eAaImAgNnP	být
dosídlena	dosídlen	k2eAgNnPc1d1	dosídlen
bohatá	bohatý	k2eAgNnPc1d1	bohaté
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
dosídlenci	dosídlenec	k1gMnPc1	dosídlenec
získali	získat	k5eAaPmAgMnP	získat
i	i	k9	i
vilky	vilka	k1gFnPc4	vilka
nebo	nebo	k8xC	nebo
rodinné	rodinný	k2eAgInPc4d1	rodinný
domky	domek	k1gInPc4	domek
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byly	být	k5eAaImAgFnP	být
také	také	k9	také
obsazeny	obsadit	k5eAaPmNgFnP	obsadit
bohaté	bohatý	k2eAgFnPc1d1	bohatá
vesnice	vesnice	k1gFnPc1	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
do	do	k7c2	do
chudých	chudý	k1gMnPc2	chudý
vsí	ves	k1gFnPc2	ves
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
odlehlost	odlehlost	k1gFnSc4	odlehlost
navíc	navíc	k6eAd1	navíc
špatně	špatně	k6eAd1	špatně
dopravně	dopravně	k6eAd1	dopravně
přístupných	přístupný	k2eAgFnPc2d1	přístupná
<g/>
,	,	kIx,	,
nechtěl	chtít	k5eNaImAgMnS	chtít
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
tam	tam	k6eAd1	tam
posláni	poslat	k5eAaPmNgMnP	poslat
např.	např.	kA	např.
chudí	chudý	k2eAgMnPc1d1	chudý
rumunští	rumunský	k2eAgMnPc1d1	rumunský
dosídlenci	dosídlenec	k1gMnPc1	dosídlenec
nebo	nebo	k8xC	nebo
Romové	Rom	k1gMnPc1	Rom
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tam	tam	k6eAd1	tam
alespoň	alespoň	k9	alespoň
někdo	někdo	k3yInSc1	někdo
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
vsi	ves	k1gFnSc6	ves
bydlely	bydlet	k5eAaImAgFnP	bydlet
dvě	dva	k4xCgFnPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgFnPc1	tři
rodiny	rodina	k1gFnPc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Vládly	vládnout	k5eAaImAgFnP	vládnout
tam	tam	k6eAd1	tam
dnes	dnes	k6eAd1	dnes
těžko	těžko	k6eAd1	těžko
pochopitelné	pochopitelný	k2eAgInPc4d1	pochopitelný
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
někdo	někdo	k3yInSc1	někdo
opravoval	opravovat	k5eAaImAgMnS	opravovat
domek	domek	k1gInSc4	domek
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
neobydleného	obydlený	k2eNgInSc2d1	neobydlený
domku	domek	k1gInSc2	domek
vzal	vzít	k5eAaPmAgMnS	vzít
tašky	taška	k1gFnPc4	taška
ze	z	k7c2	z
střechy	střecha	k1gFnSc2	střecha
<g/>
,	,	kIx,	,
vyboural	vybourat	k5eAaPmAgMnS	vybourat
si	se	k3xPyFc3	se
okna	okno	k1gNnPc4	okno
nebo	nebo	k8xC	nebo
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Domek	domek	k1gInSc1	domek
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
časem	čas	k1gInSc7	čas
v	v	k7c4	v
dezolátní	dezolátní	k2eAgInSc4d1	dezolátní
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
silách	síla	k1gFnPc6	síla
úřadů	úřad	k1gInPc2	úřad
vše	všechen	k3xTgNnSc4	všechen
uhlídat	uhlídat	k5eAaImF	uhlídat
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
poměry	poměr	k1gInPc1	poměr
přetrvávaly	přetrvávat	k5eAaImAgInP	přetrvávat
asi	asi	k9	asi
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
let	léto	k1gNnPc2	léto
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
let	léto	k1gNnPc2	léto
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
dosídlování	dosídlování	k1gNnSc2	dosídlování
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
točeny	točen	k2eAgInPc1d1	točen
filmy	film	k1gInPc1	film
(	(	kIx(	(
<g/>
i	i	k9	i
televizní	televizní	k2eAgFnSc1d1	televizní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
nemohou	moct	k5eNaImIp3nP	moct
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
sehnat	sehnat	k5eAaPmF	sehnat
byt	byt	k1gInSc4	byt
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
dosídlit	dosídlit	k5eAaPmF	dosídlit
pohraničí	pohraničí	k1gNnSc4	pohraničí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byt	byt	k1gInSc4	byt
nebo	nebo	k8xC	nebo
domek	domek	k1gInSc4	domek
ihned	ihned	k6eAd1	ihned
dostanou	dostat	k5eAaPmIp3nP	dostat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
versus	versus	k7c1	versus
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
použil	použít	k5eAaPmAgMnS	použít
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
Franz	Franz	k1gMnSc1	Franz
Jesser	Jesser	k1gMnSc1	Jesser
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
studií	studie	k1gFnPc2	studie
poprvé	poprvé	k6eAd1	poprvé
termín	termín	k1gInSc1	termín
Sudetendeutsche	Sudetendeutsche	k1gInSc1	Sudetendeutsche
(	(	kIx(	(
<g/>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
<g/>
)	)	kIx)	)
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
německojazyčné	německojazyčný	k2eAgNnSc4d1	německojazyčné
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Alpendeutsche	Alpendeutsch	k1gFnSc2	Alpendeutsch
–	–	k?	–
tedy	tedy	k8xC	tedy
Němců	Němec	k1gMnPc2	Němec
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgFnPc2d1	následující
dekád	dekáda	k1gFnPc2	dekáda
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
a	a	k8xC	a
doposud	doposud	k6eAd1	doposud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
široce	široko	k6eAd1	široko
užíváno	užíván	k2eAgNnSc1d1	užíváno
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
logické	logický	k2eAgNnSc1d1	logické
<g/>
,	,	kIx,	,
že	že	k8xS	že
utvářející	utvářející	k2eAgMnSc1d1	utvářející
se	se	k3xPyFc4	se
nacistické	nacistický	k2eAgNnSc1d1	nacistické
hnutí	hnutí	k1gNnSc1	hnutí
tzv.	tzv.	kA	tzv.
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
nemohlo	moct	k5eNaImAgNnS	moct
užít	užít	k5eAaPmF	užít
označení	označení	k1gNnSc4	označení
čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
aspoň	aspoň	k9	aspoň
böhmische	böhmische	k6eAd1	böhmische
nebo	nebo	k8xC	nebo
mährische	mährische	k6eAd1	mährische
Deutsche	Deutsche	k1gFnSc1	Deutsche
<g/>
,	,	kIx,	,
Deutschböhmen	Deutschböhmen	k1gInSc1	Deutschböhmen
nebo	nebo	k8xC	nebo
Deutschmährer	Deutschmährer	k1gInSc1	Deutschmährer
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zeměpisně	zeměpisně	k6eAd1	zeměpisně
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
atd.	atd.	kA	atd.
správné	správný	k2eAgFnPc1d1	správná
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
zcela	zcela	k6eAd1	zcela
běžné	běžný	k2eAgNnSc4d1	běžné
označení	označení	k1gNnSc4	označení
slovenští	slovenský	k2eAgMnPc1d1	slovenský
<g/>
,	,	kIx,	,
rakouští	rakouský	k2eAgMnPc1d1	rakouský
<g/>
,	,	kIx,	,
američtí	americký	k2eAgMnPc1d1	americký
<g/>
,	,	kIx,	,
brazilští	brazilský	k2eAgMnPc1d1	brazilský
apod.	apod.	kA	apod.
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
němečtí	německý	k2eAgMnPc1d1	německý
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
Kurdové	Kurd	k1gMnPc1	Kurd
apod.	apod.	kA	apod.
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
tschechische	tschechische	k1gFnSc1	tschechische
Deutsche	Deutsch	k1gFnSc2	Deutsch
<g/>
"	"	kIx"	"
by	by	kYmCp3nP	by
totiž	totiž	k9	totiž
vyjadřovalo	vyjadřovat	k5eAaImAgNnS	vyjadřovat
a	a	k8xC	a
upevňovalo	upevňovat	k5eAaImAgNnS	upevňovat
integritu	integrita	k1gFnSc4	integrita
vícenárodnostního	vícenárodnostní	k2eAgInSc2d1	vícenárodnostní
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
skládajícího	skládající	k2eAgMnSc2d1	skládající
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
z	z	k7c2	z
již	již	k6eAd1	již
jen	jen	k9	jen
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
,	,	kIx,	,
Mähren	Mährna	k1gFnPc2	Mährna
a	a	k8xC	a
Schlesien	Schlesina	k1gFnPc2	Schlesina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zbytek	zbytek	k1gInSc1	zbytek
Česka	Česko	k1gNnSc2	Česko
dostal	dostat	k5eAaPmAgInS	dostat
označení	označení	k1gNnSc4	označení
Protektorat	Protektorat	k1gInSc4	Protektorat
Böhmen	Böhmen	k2eAgInSc4d1	Böhmen
und	und	k?	und
Mähren	Mährna	k1gFnPc2	Mährna
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
Protektorat	Protektorat	k1gInSc1	Protektorat
Tschechei	Tscheche	k1gFnSc2	Tscheche
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Protektorat	Protektorat	k1gInSc1	Protektorat
Tschechien	Tschechina	k1gFnPc2	Tschechina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgInSc1d2	přesnější
název	název	k1gInSc1	název
než	než	k8xS	než
"	"	kIx"	"
<g/>
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
čeští	český	k2eAgMnPc1d1	český
<g/>
,	,	kIx,	,
moravští	moravský	k2eAgMnPc1d1	moravský
Němci	Němec	k1gMnPc1	Němec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
"	"	kIx"	"
<g/>
böhmische	böhmische	k1gFnSc1	böhmische
<g/>
,	,	kIx,	,
mährische	mährische	k1gFnSc1	mährische
Deutsche	Deutsche	k1gFnSc1	Deutsche
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
Němci	Němec	k1gMnPc1	Němec
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
zemské	zemský	k2eAgNnSc1d1	zemské
zřízení	zřízení	k1gNnSc1	zřízení
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
definitivně	definitivně	k6eAd1	definitivně
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
ani	ani	k9	ani
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
geograficky	geograficky	k6eAd1	geograficky
správným	správný	k2eAgNnSc7d1	správné
označením	označení	k1gNnSc7	označení
těch	ten	k3xDgMnPc2	ten
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
"	"	kIx"	"
<g/>
tschechische	tschechische	k6eAd1	tschechische
Deutsche	Deutsch	k1gMnSc2	Deutsch
<g/>
"	"	kIx"	"
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
Tschechien-Deutsche	Tschechien-Deutsche	k1gFnSc1	Tschechien-Deutsche
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc4	označení
těch	ten	k3xDgMnPc2	ten
českých	český	k2eAgMnPc2d1	český
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
odsunuti	odsunout	k5eAaPmNgMnP	odsunout
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
či	či	k8xC	či
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
českého	český	k2eAgNnSc2d1	české
hlediska	hledisko	k1gNnSc2	hledisko
nejspíše	nejspíše	k9	nejspíše
"	"	kIx"	"
<g/>
Němci	Němec	k1gMnPc1	Němec
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
"	"	kIx"	"
<g/>
Deutsche	Deutsche	k1gFnSc1	Deutsche
aus	aus	k?	aus
Tschechien	Tschechien	k1gInSc1	Tschechien
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označený	k2eAgMnPc1d1	označený
čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
"	"	kIx"	"
<g/>
naši	náš	k3xOp1gMnPc1	náš
Němci	Němec	k1gMnPc1	Němec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
užívali	užívat	k5eAaImAgMnP	užívat
jak	jak	k8xC	jak
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
tak	tak	k9	tak
E.	E.	kA	E.
Beneš	Beneš	k1gMnSc1	Beneš
–	–	k?	–
srov.	srov.	kA	srov.
jeho	jeho	k3xOp3gNnSc1	jeho
Paměti	paměť	k1gFnPc1	paměť
<g/>
.	.	kIx.	.
<g/>
Prvním	první	k4xOgInSc7	první
oficiálním	oficiální	k2eAgInSc7d1	oficiální
polistopadovým	polistopadový	k2eAgInSc7d1	polistopadový
dokumentem	dokument	k1gInSc7	dokument
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
termín	termín	k1gInSc4	termín
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Česko-německá	českoěmecký	k2eAgFnSc1d1	česko-německá
deklarace	deklarace	k1gFnSc1	deklarace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgMnSc1d1	přední
český	český	k2eAgMnSc1d1	český
znalec	znalec	k1gMnSc1	znalec
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
prof.	prof.	kA	prof.
Václav	Václav	k1gMnSc1	Václav
Pavlíček	Pavlíček	k1gMnSc1	Pavlíček
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
deklaraci	deklarace	k1gFnSc6	deklarace
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
pojem	pojem	k1gInSc4	pojem
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
to	ten	k3xDgNnSc1	ten
však	však	k9	však
doposud	doposud	k6eAd1	doposud
vždy	vždy	k6eAd1	vždy
byli	být	k5eAaImAgMnP	být
bývalí	bývalý	k2eAgMnPc1d1	bývalý
čs	čs	kA	čs
<g/>
.	.	kIx.	.
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
je	být	k5eAaImIp3nS	být
zkreslující	zkreslující	k2eAgInSc4d1	zkreslující
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sotva	sotva	k6eAd1	sotva
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
záměrem	záměr	k1gInSc7	záměr
<g/>
,	,	kIx,	,
než	než	k8xS	než
učinit	učinit	k5eAaPmF	učinit
z	z	k7c2	z
národnostní	národnostní	k2eAgFnSc2d1	národnostní
menšiny	menšina	k1gFnSc2	menšina
samostatný	samostatný	k2eAgInSc1d1	samostatný
národ	národ	k1gInSc1	národ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zavedení	zavedení	k1gNnSc1	zavedení
pojmů	pojem	k1gInPc2	pojem
"	"	kIx"	"
<g/>
Sudety	Sudety	k1gInPc4	Sudety
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
sudetský	sudetský	k2eAgMnSc1d1	sudetský
Němec	Němec	k1gMnSc1	Němec
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
bylo	být	k5eAaImAgNnS	být
základním	základní	k2eAgInSc7d1	základní
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
formulování	formulování	k1gNnSc4	formulování
politické	politický	k2eAgFnSc2d1	politická
<g />
.	.	kIx.	.
</s>
<s>
role	role	k1gFnSc1	role
Němců	Němec	k1gMnPc2	Němec
žijících	žijící	k2eAgMnPc2d1	žijící
dříve	dříve	k6eAd2	dříve
na	na	k7c6	na
území	území	k1gNnSc6	území
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
formulování	formulování	k1gNnSc4	formulování
jejich	jejich	k3xOp3gNnSc2	jejich
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
expanzivní	expanzivní	k2eAgFnSc6d1	expanzivní
velkoněmecké	velkoněmecký	k2eAgFnSc6d1	Velkoněmecká
politice	politika	k1gFnSc6	politika
a	a	k8xC	a
pro	pro	k7c4	pro
specifikaci	specifikace	k1gFnSc4	specifikace
termínů	termín	k1gInPc2	termín
"	"	kIx"	"
<g/>
národní	národní	k2eAgNnSc4d1	národní
společenství	společenství	k1gNnSc4	společenství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
sudetoněmecký	sudetoněmecký	k2eAgInSc1d1	sudetoněmecký
kmen	kmen	k1gInSc1	kmen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
však	však	k9	však
termínu	termín	k1gInSc3	termín
"	"	kIx"	"
<g/>
národní	národní	k2eAgFnSc1d1	národní
skupina	skupina	k1gFnSc1	skupina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Volksgruppe	Volksgrupp	k1gMnSc5	Volksgrupp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
době	doba	k1gFnSc6	doba
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
cílů	cíl	k1gInPc2	cíl
politiky	politika	k1gFnSc2	politika
Drang	Drang	k1gInSc1	Drang
nach	nach	k1gInSc1	nach
Osten	osten	k1gInSc1	osten
důležitou	důležitý	k2eAgFnSc4d1	důležitá
a	a	k8xC	a
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sudeťáci	sudeťák	k1gMnPc1	sudeťák
versus	versus	k7c1	versus
sudeťáci	sudeťák	k1gMnPc1	sudeťák
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
pojmy	pojem	k1gInPc4	pojem
sudeťáci	sudeťák	k1gMnPc1	sudeťák
a	a	k8xC	a
Sudeťáci	sudeťák	k1gMnPc1	sudeťák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
Sudeťák	sudeťák	k1gMnSc1	sudeťák
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
s	s	k7c7	s
je	být	k5eAaImIp3nS	být
jednoslovné	jednoslovný	k2eAgNnSc4d1	jednoslovné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
sudetského	sudetský	k2eAgMnSc4d1	sudetský
Němce	Němec	k1gMnSc4	Němec
<g/>
,	,	kIx,	,
Sudetoněmce	Sudetoněmec	k1gMnSc4	Sudetoněmec
<g/>
,	,	kIx,	,
Sudeťana	Sudeťan	k1gMnSc4	Sudeťan
<g/>
,	,	kIx,	,
obyvatele	obyvatel	k1gMnPc4	obyvatel
Sudet	Sudety	k1gFnPc2	Sudety
<g/>
.	.	kIx.	.
</s>
<s>
Sudeťák	sudeťák	k1gMnSc1	sudeťák
je	být	k5eAaImIp3nS	být
Sudetoněmec	Sudetoněmec	k1gMnSc1	Sudetoněmec
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
obyvatel	obyvatel	k1gMnSc1	obyvatel
Sudet	Sudety	k1gFnPc2	Sudety
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Skopčák	skopčák	k1gMnSc1	skopčák
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
sudeťák	sudeťák	k1gMnSc1	sudeťák
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
s	s	k7c7	s
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
příslušník	příslušník	k1gMnSc1	příslušník
nacistického	nacistický	k2eAgNnSc2d1	nacistické
(	(	kIx(	(
<g/>
henleinovského	henleinovský	k2eAgNnSc2d1	henleinovské
<g/>
)	)	kIx)	)
hnutí	hnutí	k1gNnSc1	hnutí
mezi	mezi	k7c7	mezi
bývalými	bývalý	k2eAgMnPc7d1	bývalý
československými	československý	k2eAgMnPc7d1	československý
Němci	Němec	k1gMnPc7	Němec
<g/>
;	;	kIx,	;
henleinovec	henleinovec	k1gMnSc1	henleinovec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
označení	označení	k1gNnSc2	označení
přebralo	přebrat	k5eAaPmAgNnS	přebrat
i	i	k9	i
obyvatelské	obyvatelský	k2eAgNnSc1d1	obyvatelské
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Sudeťák	sudeťák	k1gMnSc1	sudeťák
<g/>
"	"	kIx"	"
pejorativní	pejorativní	k2eAgInSc1d1	pejorativní
nádech	nádech	k1gInSc1	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Tendence	tendence	k1gFnSc1	tendence
kolektivně	kolektivně	k6eAd1	kolektivně
spojovat	spojovat	k5eAaImF	spojovat
sudetské	sudetský	k2eAgMnPc4d1	sudetský
Němce	Němec	k1gMnPc4	Němec
s	s	k7c7	s
nacismem	nacismus	k1gInSc7	nacismus
a	a	k8xC	a
útočným	útočný	k2eAgInSc7d1	útočný
pangermanismem	pangermanismus	k1gInSc7	pangermanismus
se	se	k3xPyFc4	se
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
společnosti	společnost	k1gFnSc6	společnost
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
důsledku	důsledek	k1gInSc6	důsledek
politického	politický	k2eAgNnSc2d1	politické
působení	působení	k1gNnSc2	působení
sudetoněmeckých	sudetoněmecký	k2eAgFnPc2d1	Sudetoněmecká
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
spolupodílu	spolupodíla	k1gFnSc4	spolupodíla
na	na	k7c6	na
rozbití	rozbití	k1gNnSc6	rozbití
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
okupace	okupace	k1gFnSc1	okupace
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
českou	český	k2eAgFnSc4d1	Česká
společnost	společnost	k1gFnSc4	společnost
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
zradikalizovala	zradikalizovat	k5eAaPmAgFnS	zradikalizovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
získalo	získat	k5eAaPmAgNnS	získat
slovní	slovní	k2eAgNnSc1d1	slovní
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
<g/>
"	"	kIx"	"
vysloveně	vysloveně	k6eAd1	vysloveně
hanlivý	hanlivý	k2eAgInSc4d1	hanlivý
<g/>
,	,	kIx,	,
odsuzující	odsuzující	k2eAgInSc4d1	odsuzující
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
později	pozdě	k6eAd2	pozdě
prohloubený	prohloubený	k2eAgInSc1d1	prohloubený
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
propagandou	propaganda	k1gFnSc7	propaganda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
eminentní	eminentní	k2eAgInSc4d1	eminentní
zájem	zájem	k1gInSc4	zájem
na	na	k7c4	na
udržování	udržování	k1gNnSc4	udržování
obrazu	obraz	k1gInSc2	obraz
německého	německý	k2eAgMnSc2d1	německý
nepřítele	nepřítel	k1gMnSc2	nepřítel
(	(	kIx(	(
<g/>
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k8xC	i
ospravedlnění	ospravedlnění	k1gNnSc4	ospravedlnění
"	"	kIx"	"
<g/>
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
"	"	kIx"	"
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
varovala	varovat	k5eAaImAgFnS	varovat
před	před	k7c7	před
"	"	kIx"	"
<g/>
německými	německý	k2eAgMnPc7d1	německý
revanšisty	revanšista	k1gMnPc7	revanšista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
nacionalisté	nacionalista	k1gMnPc1	nacionalista
mají	mít	k5eAaImIp3nP	mít
antisudeťáctví	antisudeťáctví	k1gNnSc4	antisudeťáctví
v	v	k7c6	v
programu	program	k1gInSc6	program
dodnes	dodnes	k6eAd1	dodnes
<g/>
:	:	kIx,	:
hovoří	hovořit	k5eAaImIp3nS	hovořit
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
sudetských	sudetský	k2eAgMnPc6d1	sudetský
Němcích	Němec	k1gMnPc6	Němec
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
vystupovat	vystupovat	k5eAaImF	vystupovat
,	,	kIx,	,
<g/>
proti	proti	k7c3	proti
neoprávněným	oprávněný	k2eNgInPc3d1	neoprávněný
nárokům	nárok	k1gInPc3	nárok
tzv.	tzv.	kA	tzv.
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
...	...	k?	...
proti	proti	k7c3	proti
odškodnění	odškodnění	k1gNnSc3	odškodnění
tzv.	tzv.	kA	tzv.
vyhnaných	vyhnaný	k2eAgMnPc2d1	vyhnaný
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
o	o	k7c6	o
revanšistických	revanšistický	k2eAgFnPc6d1	revanšistická
organizacích	organizace	k1gFnPc6	organizace
odsunutých	odsunutý	k2eAgMnPc2d1	odsunutý
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skopčáci	skopčák	k1gMnPc5	skopčák
===	===	k?	===
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
pejorativní	pejorativní	k2eAgNnSc1d1	pejorativní
označení	označení	k1gNnSc1	označení
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
západočeských	západočeský	k2eAgMnPc2d1	západočeský
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
okupace	okupace	k1gFnSc1	okupace
přeneslo	přenést	k5eAaPmAgNnS	přenést
zejména	zejména	k9	zejména
na	na	k7c4	na
nacistické	nacistický	k2eAgMnPc4d1	nacistický
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
obecně	obecně	k6eAd1	obecně
Němce	Němec	k1gMnPc4	Němec
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zajímavý	zajímavý	k2eAgInSc4d1	zajímavý
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
u	u	k7c2	u
Jungmanna	Jungmann	k1gInSc2	Jungmann
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
pastýř	pastýř	k1gMnSc1	pastýř
skopců	skopec	k1gMnPc2	skopec
a	a	k8xC	a
beranů	beran	k1gMnPc2	beran
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
takto	takto	k6eAd1	takto
čeští	český	k2eAgMnPc1d1	český
sedláci	sedlák	k1gMnPc1	sedlák
označovali	označovat	k5eAaImAgMnP	označovat
své	svůj	k3xOyFgMnPc4	svůj
německé	německý	k2eAgMnPc4d1	německý
sousedy	soused	k1gMnPc4	soused
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Chebska	Chebsko	k1gNnSc2	Chebsko
<g/>
,	,	kIx,	,
Krušných	krušný	k2eAgFnPc2d1	krušná
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
Šumavy	Šumava	k1gFnSc2	Šumava
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přicházeli	přicházet	k5eAaImAgMnP	přicházet
na	na	k7c6	na
jarmarky	jarmarky	k?	jarmarky
"	"	kIx"	"
<g/>
z	z	k7c2	z
kopců	kopec	k1gInPc2	kopec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Fuchs	Fuchs	k1gMnSc1	Fuchs
psal	psát	k5eAaImAgMnS	psát
o	o	k7c6	o
Skopčácích	skopčák	k1gMnPc6	skopčák
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Chebu	Cheb	k1gInSc2	Cheb
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
ve	v	k7c6	v
Švejkovi	Švejk	k1gMnSc6	Švejk
Skopčáky	skopčák	k1gMnPc4	skopčák
od	od	k7c2	od
Kašperských	kašperský	k2eAgFnPc2d1	Kašperská
Hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
Heda	Heda	k1gFnSc1	Heda
Kaufmannová	Kaufmannová	k1gFnSc1	Kaufmannová
se	se	k3xPyFc4	se
zmiňovala	zmiňovat	k5eAaImAgFnS	zmiňovat
o	o	k7c6	o
Skopčácích	skopčák	k1gMnPc6	skopčák
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Frýdlantského	frýdlantský	k2eAgInSc2d1	frýdlantský
výběžku	výběžek	k1gInSc2	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nárůstu	nárůst	k1gInSc2	nárůst
německého	německý	k2eAgInSc2d1	německý
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
,	,	kIx,	,
však	však	k9	však
převládla	převládnout	k5eAaPmAgFnS	převládnout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
paronymie	paronymie	k1gFnSc2	paronymie
asociace	asociace	k1gFnSc2	asociace
se	se	k3xPyFc4	se
slovem	slovem	k6eAd1	slovem
skopec	skopec	k1gMnSc1	skopec
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
nabylo	nabýt	k5eAaPmAgNnS	nabýt
pejorativního	pejorativní	k2eAgInSc2d1	pejorativní
významu	význam	k1gInSc2	význam
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
starší	starý	k2eAgFnSc4d2	starší
nadávku	nadávka	k1gFnSc4	nadávka
tupému	tupý	k2eAgMnSc3d1	tupý
člověku	člověk	k1gMnSc3	člověk
"	"	kIx"	"
<g/>
skopová	skopová	k1gFnSc1	skopová
hlavo	hlava	k1gFnSc5	hlava
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Takto	takto	k6eAd1	takto
ho	on	k3xPp3gMnSc4	on
používá	používat	k5eAaImIp3nS	používat
napŕ	napŕ	k?	napŕ
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
ve	v	k7c6	v
Zbabělcích	zbabělec	k1gMnPc6	zbabělec
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
vývoji	vývoj	k1gInSc6	vývoj
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
satirická	satirický	k2eAgFnSc1d1	satirická
povídka	povídka	k1gFnSc1	povídka
pražského	pražský	k2eAgMnSc2d1	pražský
německého	německý	k2eAgMnSc2d1	německý
spisovatele	spisovatel	k1gMnSc2	spisovatel
Gustava	Gustav	k1gMnSc2	Gustav
Meyrinka	Meyrinka	k1gFnSc1	Meyrinka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
Skopcoglobin	Skopcoglobina	k1gFnPc2	Skopcoglobina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vysmívá	vysmívat	k5eAaImIp3nS	vysmívat
německému	německý	k2eAgInSc3d1	německý
militarismu	militarismus	k1gInSc3	militarismus
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
z	z	k7c2	z
autorových	autorův	k2eAgFnPc2d1	autorova
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgMnPc4d1	využívající
skopce	skopec	k1gMnPc4	skopec
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
omezenosti	omezenost	k1gFnSc2	omezenost
<g/>
,	,	kIx,	,
šosáctví	šosáctví	k1gNnSc2	šosáctví
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BRANDES	BRANDES	kA	BRANDES
<g/>
,	,	kIx,	,
Detlef	Detlef	k1gInSc1	Detlef
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
vyhnání	vyhnání	k1gNnSc3	vyhnání
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
499	[number]	k4	499
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7260	[number]	k4	7260
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
70	[number]	k4	70
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BRANDES	BRANDES	kA	BRANDES
<g/>
,	,	kIx,	,
Detlef	Detlef	k1gInSc1	Detlef
<g/>
.	.	kIx.	.
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
krizovém	krizový	k2eAgInSc6d1	krizový
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
430	[number]	k4	430
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
605	[number]	k4	605
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BRÜGEL	BRÜGEL	kA	BRÜGEL
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
846	[number]	k4	846
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1440	[number]	k4	1440
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BRÜGEL	BRÜGEL	kA	BRÜGEL
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
415	[number]	k4	415
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1637	[number]	k4	1637
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DOUGLAS	DOUGLAS	kA	DOUGLAS
<g/>
,	,	kIx,	,
R.	R.	kA	R.
M.	M.	kA	M.
Orderly	Orderla	k1gMnSc2	Orderla
and	and	k?	and
humane	humanout	k5eAaPmIp3nS	humanout
:	:	kIx,	:
the	the	k?	the
expulsion	expulsion	k1gInSc1	expulsion
of	of	k?	of
the	the	k?	the
Germans	Germans	k1gInSc1	Germans
after	after	k1gMnSc1	after
the	the	k?	the
Second	Second	k1gMnSc1	Second
World	World	k1gMnSc1	World
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Haven	Haven	k1gInSc1	Haven
<g/>
:	:	kIx,	:
Yale	Yale	k1gInSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
486	[number]	k4	486
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
30016660	[number]	k4	30016660
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
EMMERT	EMMERT	kA	EMMERT
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
ve	v	k7c6	v
Wehrmachtu	wehrmacht	k1gInSc6	wehrmacht
:	:	kIx,	:
zamlčované	zamlčovaný	k2eAgInPc1d1	zamlčovaný
osudy	osud	k1gInPc1	osud
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Auditorium	auditorium	k1gNnSc1	auditorium
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
205	[number]	k4	205
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87284	[number]	k4	87284
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FRANZEL	FRANZEL	kA	FRANZEL
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
<g/>
.	.	kIx.	.
</s>
<s>
Sudetendeutsche	Sudetendeutschus	k1gMnSc5	Sudetendeutschus
Geschichte	Geschicht	k1gMnSc5	Geschicht
<g/>
.	.	kIx.	.
</s>
<s>
Würzburg	Würzburg	k1gMnSc1	Würzburg
<g/>
:	:	kIx,	:
Flechsig	Flechsig	k1gMnSc1	Flechsig
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
491	[number]	k4	491
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
88189	[number]	k4	88189
<g/>
-	-	kIx~	-
<g/>
449	[number]	k4	449
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HAHNOVÁ	HAHNOVÁ	kA	HAHNOVÁ
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
;	;	kIx,	;
HAHN	HAHN	kA	HAHN
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Henning	Henning	k1gInSc1	Henning
<g/>
.	.	kIx.	.
</s>
<s>
Sudetoněmecká	sudetoněmecký	k2eAgNnPc1d1	Sudetoněmecké
vzpomínání	vzpomínání	k1gNnPc1	vzpomínání
a	a	k8xC	a
zapomínání	zapomínání	k1gNnPc1	zapomínání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
234	[number]	k4	234
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7220	[number]	k4	7220
<g/>
-	-	kIx~	-
<g/>
117	[number]	k4	117
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
К	К	k?	К
С	С	k?	С
С	С	k?	С
н	н	k?	н
<g/>
:	:	kIx,	:
н	н	k?	н
б	б	k?	б
р	р	k?	р
<g/>
.	.	kIx.	.
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
В	В	k?	В
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
5-7455-1135-4	[number]	k4	5-7455-1135-4
</s>
</p>
<p>
<s>
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
:	:	kIx,	:
Odsunové	odsunový	k2eAgFnPc1d1	odsunová
ztráty	ztráta	k1gFnPc1	ztráta
sudetoněmeckého	sudetoněmecký	k2eAgNnSc2d1	Sudetoněmecké
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
jejich	jejich	k3xOp3gNnSc2	jejich
přesného	přesný	k2eAgNnSc2d1	přesné
vyčíslení	vyčíslení	k1gNnSc2	vyčíslení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Federální	federální	k2eAgNnSc1d1	federální
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
:	:	kIx,	:
Odsun	odsun	k1gInSc1	odsun
nebo	nebo	k8xC	nebo
vyhnání	vyhnání	k1gNnSc1	vyhnání
<g/>
?	?	kIx.	?
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Jinočany	Jinočan	k1gMnPc7	Jinočan
:	:	kIx,	:
H	H	kA	H
&	&	k?	&
H	H	kA	H
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
KURAL	KURAL	kA	KURAL
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
RADVANOVSKÝ	RADVANOVSKÝ	kA	RADVANOVSKÝ
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Sudety	Sudety	k1gFnPc1	Sudety
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
hákovým	hákový	k2eAgInSc7d1	hákový
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
:	:	kIx,	:
Albis	Albis	k1gFnSc4	Albis
international	internationat	k5eAaPmAgMnS	internationat
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
547	[number]	k4	547
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86067	[number]	k4	86067
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Meixner	Meixner	k1gMnSc1	Meixner
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
:	:	kIx,	:
Geschichte	Geschicht	k1gInSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
Sudetendeutschen	Sudetendeutschen	k1gInSc4	Sudetendeutschen
<g/>
.	.	kIx.	.
</s>
<s>
Nürnberg	Nürnberg	k1gMnSc1	Nürnberg
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
921332	[number]	k4	921332
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NAIMARK	NAIMARK	kA	NAIMARK
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Plameny	plamen	k1gInPc1	plamen
nenávisti	nenávist	k1gFnSc2	nenávist
:	:	kIx,	:
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
235	[number]	k4	235
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
751	[number]	k4	751
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NĚMEČEK	Němeček	k1gMnSc1	Němeček
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
dekretům	dekret	k1gInPc3	dekret
a	a	k8xC	a
odsunu	odsun	k1gInSc2	odsun
Němců	Němec	k1gMnPc2	Němec
:	:	kIx,	:
datová	datový	k2eAgFnSc1d1	datová
příručka	příručka	k1gFnSc1	příručka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Littera	Litter	k1gMnSc4	Litter
Bohemica	Bohemicus	k1gMnSc4	Bohemicus
;	;	kIx,	;
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
152	[number]	k4	152
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7214	[number]	k4	7214
<g/>
-	-	kIx~	-
<g/>
519	[number]	k4	519
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PEŠEK	Pešek	k1gMnSc1	Pešek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc1d1	německá
menšiny	menšina	k1gFnPc1	menšina
v	v	k7c6	v
právních	právní	k2eAgFnPc6d1	právní
normách	norma	k1gFnPc6	norma
1938-1948	[number]	k4	1938-1948
:	:	kIx,	:
Československo	Československo	k1gNnSc1	Československo
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
vybranými	vybraný	k2eAgFnPc7d1	vybraná
evropskými	evropský	k2eAgFnPc7d1	Evropská
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Doplněk	doplněk	k1gInSc1	doplněk
;	;	kIx,	;
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
soudobé	soudobý	k2eAgFnPc4d1	soudobá
dějiny	dějiny	k1gFnPc4	dějiny
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
601	[number]	k4	601
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7239	[number]	k4	7239
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7285	[number]	k4	7285
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
76	[number]	k4	76
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prauser	Prauser	k1gInSc1	Prauser
<g/>
,	,	kIx,	,
Steffen	Steffen	k2eAgInSc1d1	Steffen
and	and	k?	and
Rees	Rees	k1gInSc1	Rees
<g/>
,	,	kIx,	,
Arfon	Arfon	k1gInSc1	Arfon
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Expulsion	Expulsion	k1gInSc1	Expulsion
of	of	k?	of
the	the	k?	the
"	"	kIx"	"
<g/>
German	German	k1gMnSc1	German
<g/>
"	"	kIx"	"
Communities	Communities	k1gMnSc1	Communities
from	from	k1gMnSc1	from
Eastern	Eastern	k1gMnSc1	Eastern
Europe	Europ	k1gInSc5	Europ
at	at	k?	at
the	the	k?	the
End	End	k1gFnSc2	End
of	of	k?	of
the	the	k?	the
2	[number]	k4	2
<g/>
nd	nd	k?	nd
World	World	k1gMnSc1	World
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
</s>
<s>
Florence	Florenc	k1gFnPc4	Florenc
<g/>
,	,	kIx,	,
Italy	Ital	k1gMnPc4	Ital
<g/>
,	,	kIx,	,
European	European	k1gMnSc1	European
University	universita	k1gFnSc2	universita
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SLÁDEK	Sládek	k1gMnSc1	Sládek
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
:	:	kIx,	:
německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
Československu	Československo	k1gNnSc6	Československo
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Pragma	Pragma	k1gNnSc1	Pragma
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
205	[number]	k4	205
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7205	[number]	k4	7205
<g/>
-	-	kIx~	-
<g/>
901	[number]	k4	901
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SUPPAN	SUPPAN	kA	SUPPAN
<g/>
,	,	kIx,	,
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
.	.	kIx.	.
</s>
<s>
Österreicher	Österreichra	k1gFnPc2	Österreichra
<g/>
,	,	kIx,	,
Tschechen	Tschechen	k2eAgInSc1d1	Tschechen
und	und	k?	und
Sudetendeutschen	Sudetendeutschen	k1gInSc1	Sudetendeutschen
als	als	k?	als
Konfliktgeleinschaft	Konfliktgeleinschaft	k1gInSc1	Konfliktgeleinschaft
im	im	k?	im
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Jahrhundert	Jahrhundert	k1gMnSc1	Jahrhundert
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
157	[number]	k4	157
<g/>
-	-	kIx~	-
<g/>
209	[number]	k4	209
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ján	Ján	k1gMnSc1	Ján
Štefanica	Štefanica	k1gMnSc1	Štefanica
<g/>
:	:	kIx,	:
Odsuny	odsun	k1gInPc1	odsun
a	a	k8xC	a
výmeny	výmeno	k1gNnPc7	výmeno
skupín	skupín	k1gMnSc1	skupín
obyvateľstva	obyvateľstvo	k1gNnSc2	obyvateľstvo
európskych	európskych	k1gInSc1	európskych
štátov	štátovo	k1gNnPc2	štátovo
v	v	k7c6	v
prvej	prvej	k?	prvej
polovici	polovice	k1gFnSc6	polovice
20	[number]	k4	20
<g/>
.	.	kIx.	.
storočia	storočium	k1gNnSc2	storočium
<g/>
,	,	kIx,	,
Historia	Historium	k1gNnSc2	Historium
et	et	k?	et
theoria	theorium	k1gNnSc2	theorium
iuris	iuris	k1gFnPc2	iuris
<g/>
,	,	kIx,	,
Roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
č.	č.	k?	č.
4	[number]	k4	4
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
78-93	[number]	k4	78-93
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ján	Ján	k1gMnSc1	Ján
Štefanica	Štefanica	k1gMnSc1	Štefanica
<g/>
:	:	kIx,	:
Problematika	problematika	k1gFnSc1	problematika
právneho	právneze	k6eAd1	právneze
postavenia	postavenium	k1gNnSc2	postavenium
národnostných	národnostný	k2eAgFnPc2d1	národnostný
menšín	menšína	k1gFnPc2	menšína
v	v	k7c4	v
európskom	európskom	k1gInSc4	európskom
priestore	priestor	k1gMnSc5	priestor
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
II	II	kA	II
<g/>
.	.	kIx.	.
svetovej	svetovat	k5eAaPmRp2nS	svetovat
vojny	vojna	k1gFnSc2	vojna
a	a	k8xC	a
riešenie	riešenie	k1gFnSc2	riešenie
tejto	tejto	k1gNnSc1	tejto
otázky	otázka	k1gFnSc2	otázka
<g/>
,	,	kIx,	,
Historické	historický	k2eAgNnSc1d1	historické
právne	právnout	k5eAaPmIp3nS	právnout
systémy	systém	k1gInPc1	systém
a	a	k8xC	a
integrácia	integrácia	k1gFnSc1	integrácia
Európy	Európa	k1gFnSc2	Európa
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislave	Bratislav	k1gMnSc5	Bratislav
<g/>
,	,	kIx,	,
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
S.	S.	kA	S.
155-161	[number]	k4	155-161
</s>
</p>
<p>
<s>
THER	THER	kA	THER
<g/>
,	,	kIx,	,
Philipp	Philipp	k1gInSc1	Philipp
<g/>
.	.	kIx.	.
</s>
<s>
Temná	temný	k2eAgFnSc1d1	temná
strana	strana	k1gFnSc1	strana
národních	národní	k2eAgInPc2d1	národní
států	stát	k1gInPc2	stát
:	:	kIx,	:
etnické	etnický	k2eAgFnPc4d1	etnická
čistky	čistka	k1gFnPc4	čistka
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
295	[number]	k4	295
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
2133	[number]	k4	2133
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VELČOVSKÝ	VELČOVSKÝ	kA	VELČOVSKÝ
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Jazyk	jazyk	k1gInSc1	jazyk
jako	jako	k8xS	jako
fetiš	fetiš	k1gInSc1	fetiš
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
o	o	k7c6	o
Češích	Čech	k1gMnPc6	Čech
a	a	k8xC	a
českých	český	k2eAgFnPc6d1	Česká
Němcích	Němec	k1gMnPc6	Němec
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7308-903-0	[number]	k4	978-80-7308-903-0
(	(	kIx(	(
<g/>
print	print	k1gInSc1	print
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7308-904-7	[number]	k4	978-80-7308-904-7
(	(	kIx(	(
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DE	DE	k?	DE
ZAYAS	ZAYAS	kA	ZAYAS
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
M.	M.	kA	M.
A	a	k9	a
terrible	terrible	k6eAd1	terrible
Revenge	Revenge	k1gFnSc7	Revenge
<g/>
.	.	kIx.	.
</s>
<s>
Palgrave	Palgravat	k5eAaPmIp3nS	Palgravat
<g/>
/	/	kIx~	/
<g/>
Macmillan	Macmillan	k1gMnSc1	Macmillan
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4039	[number]	k4	4039
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DE	DE	k?	DE
ZAYAS	ZAYAS	kA	ZAYAS
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
M.	M.	kA	M.
Nemesis	Nemesis	k1gFnPc1	Nemesis
at	at	k?	at
Potsdam	Potsdam	k1gInSc1	Potsdam
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8032	[number]	k4	8032
<g/>
-	-	kIx~	-
<g/>
4910	[number]	k4	4910
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZIMMERMANN	Zimmermann	k1gMnSc1	Zimmermann
<g/>
,	,	kIx,	,
Volker	Volker	k1gMnSc1	Volker
<g/>
.	.	kIx.	.
</s>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
nacistickém	nacistický	k2eAgInSc6d1	nacistický
státě	stát	k1gInSc6	stát
:	:	kIx,	:
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
nálada	nálada	k1gFnSc1	nálada
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
říšské	říšský	k2eAgFnSc6d1	říšská
župě	župa	k1gFnSc6	župa
Sudety	Sudety	k1gFnPc1	Sudety
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
;	;	kIx,	;
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
577	[number]	k4	577
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7260	[number]	k4	7260
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
390	[number]	k4	390
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sudety	Sudety	k1gFnPc1	Sudety
</s>
</p>
<p>
<s>
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
strana	strana	k1gFnSc1	strana
</s>
</p>
<p>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
program	program	k1gInSc1	program
</s>
</p>
<p>
<s>
Vysídlení	vysídlení	k1gNnSc1	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
Benešovy	Benešův	k2eAgInPc1d1	Benešův
dekrety	dekret	k1gInPc1	dekret
</s>
</p>
<p>
<s>
Volksdeutsche	Volksdeutsche	k6eAd1	Volksdeutsche
</s>
</p>
<p>
<s>
Sudetoněmecké	sudetoněmecký	k2eAgNnSc1d1	Sudetoněmecké
krajanské	krajanský	k2eAgNnSc1d1	krajanské
sdružení	sdružení	k1gNnSc1	sdružení
</s>
</p>
<p>
<s>
Sudetoněmecký	sudetoněmecký	k2eAgInSc1d1	sudetoněmecký
Freikorps	Freikorps	k1gInSc1	Freikorps
</s>
</p>
<p>
<s>
Krkonošsko-jesenická	krkonošskoesenický	k2eAgFnSc1d1	krkonošsko-jesenický
subprovincie	subprovincie	k1gFnSc1	subprovincie
<g/>
#	#	kIx~	#
<g/>
Sudetská	sudetský	k2eAgFnSc1d1	sudetská
vs	vs	k?	vs
<g/>
.	.	kIx.	.
krkonoško-jesenická	krkonoškoesenický	k2eAgFnSc1d1	krkonoško-jesenický
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
němčina	němčina	k1gFnSc1	němčina
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Češi	Čech	k1gMnPc1	Čech
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Češi	Čech	k1gMnPc1	Čech
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Češi	Čech	k1gMnPc1	Čech
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Češi	Čech	k1gMnPc1	Čech
ve	v	k7c6	v
wehrmachtu	wehrmacht	k1gInSc6	wehrmacht
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
</s>
</p>
