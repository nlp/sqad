<p>
<s>
Keidži	Keidzat	k5eAaPmIp1nS	Keidzat
Suzuki	suzuki	k1gNnSc1	suzuki
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1980	[number]	k4	1980
Džósó	Džósó	k1gFnPc2	Džósó
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
japonský	japonský	k2eAgMnSc1d1	japonský
zápasník	zápasník	k1gMnSc1	zápasník
<g/>
–	–	k?	–
<g/>
judista	judista	k1gMnSc1	judista
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vrcholová	vrcholový	k2eAgFnSc1d1	vrcholová
sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
univerzitou	univerzita	k1gFnSc7	univerzita
Kokušikan	Kokušikana	k1gFnPc2	Kokušikana
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
seniorské	seniorský	k2eAgFnSc6d1	seniorská
reprezentaci	reprezentace	k1gFnSc6	reprezentace
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
v	v	k7c6	v
polotěžké	polotěžký	k2eAgFnSc6d1	polotěžká
váze	váha	k1gFnSc6	váha
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
Kóseie	Kóseie	k1gFnSc2	Kóseie
Inoueho	Inoue	k1gMnSc2	Inoue
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
uspěl	uspět	k5eAaPmAgMnS	uspět
při	při	k7c6	při
japonské	japonský	k2eAgFnSc6d1	japonská
olympijské	olympijský	k2eAgFnSc6d1	olympijská
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
v	v	k7c6	v
těžké	těžký	k2eAgFnSc6d1	těžká
váze	váha	k1gFnSc6	váha
nad	nad	k7c4	nad
100	[number]	k4	100
kg	kg	kA	kg
nad	nad	k7c7	nad
Jasujuki	Jasujuki	k1gNnSc7	Jasujuki
Munetou	Muneta	k1gFnSc7	Muneta
a	a	k8xC	a
startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
předvedl	předvést	k5eAaPmAgMnS	předvést
vynikající	vynikající	k2eAgInSc4d1	vynikající
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazil	porazit	k5eAaPmAgMnS	porazit
Tamerlana	Tamerlan	k1gMnSc4	Tamerlan
Tmenova	Tmenův	k2eAgMnSc4d1	Tmenův
na	na	k7c4	na
ippon	ippon	k1gInSc4	ippon
technikou	technika	k1gFnSc7	technika
de-aši-harai	dešiarai	k1gNnSc2	de-aši-harai
a	a	k8xC	a
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
v	v	k7c6	v
polotěžké	polotěžký	k2eAgFnSc6d1	polotěžká
váze	váha	k1gFnSc6	váha
do	do	k7c2	do
100	[number]	k4	100
kg	kg	kA	kg
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Inoueho	Inoue	k1gMnSc4	Inoue
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nakonec	nakonec	k6eAd1	nakonec
výsledkově	výsledkově	k6eAd1	výsledkově
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
kvalifikoval	kvalifikovat	k5eAaBmAgMnS	kvalifikovat
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
olympijské	olympijský	k2eAgFnSc6d1	olympijská
nominaci	nominace	k1gFnSc6	nominace
dostal	dostat	k5eAaPmAgMnS	dostat
přednost	přednost	k1gFnSc4	přednost
mladší	mladý	k2eAgFnSc2d2	mladší
Daiki	Daik	k1gFnSc2	Daik
Kamikawa	Kamikaw	k1gInSc2	Kamikaw
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
ukončil	ukončit	k5eAaPmAgMnS	ukončit
sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
trenérské	trenérský	k2eAgFnSc3d1	trenérská
a	a	k8xC	a
manažérské	manažérský	k2eAgFnSc3d1	manažérská
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
===	===	k?	===
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
1	[number]	k4	1
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Leonding	Leonding	k1gInSc4	Leonding
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
3	[number]	k4	3
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Kano	Kano	k1gNnSc1	Kano
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
2	[number]	k4	2
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Kano	Kano	k1gNnSc1	Kano
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
–	–	k?	–
1	[number]	k4	1
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Hamburk	Hamburk	k1gInSc1	Hamburk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
–	–	k?	–
1	[number]	k4	1
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Čching-tao	Čchingao	k6eAd1	Čching-tao
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
–	–	k?	–
1	[number]	k4	1
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Tunis	Tunis	k1gInSc4	Tunis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Váhové	váhový	k2eAgFnSc2d1	váhová
kategorie	kategorie	k1gFnSc2	kategorie
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
vah	váha	k1gFnPc2	váha
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Keidži	Keidž	k1gFnSc3	Keidž
Suzuki	suzuki	k1gNnSc2	suzuki
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
novinky	novinka	k1gFnPc1	novinka
Keidži	Keidž	k1gFnSc6	Keidž
Suzukiho	Suzuki	k1gMnSc2	Suzuki
na	na	k7c6	na
judoinside	judoinsid	k1gInSc5	judoinsid
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
