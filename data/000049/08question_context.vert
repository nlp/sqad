<s>
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Delphinus	Delphinus	k1gInSc1	Delphinus
delphis	delphis	k1gInSc1	delphis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
kytovce	kytovec	k1gMnSc2	kytovec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
delfínovití	delfínovitý	k2eAgMnPc1d1	delfínovitý
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
činí	činit	k5eAaImIp3nS	činit
1,8	[number]	k4	1,8
<g/>
-	-	kIx~	-
<g/>
2,6	[number]	k4	2,6
m	m	kA	m
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
90-140	[number]	k4	90-140
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
delfín	delfín	k1gMnSc1	delfín
skákavý	skákavý	k2eAgMnSc1d1	skákavý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
štíhlé	štíhlý	k2eAgNnSc1d1	štíhlé
<g/>
,	,	kIx,	,
základní	základní	k2eAgNnSc1d1	základní
zbarvení	zbarvení	k1gNnSc1	zbarvení
hnědé	hnědý	k2eAgFnSc2d1	hnědá
až	až	k8xS	až
černé	černý	k2eAgFnSc2d1	černá
<g/>
,	,	kIx,	,
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
bělavé	bělavý	k2eAgInPc1d1	bělavý
<g/>
,	,	kIx,	,
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
světlé	světlý	k2eAgFnSc2d1	světlá
skvrny	skvrna	k1gFnSc2	skvrna
a	a	k8xC	a
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
zaoblená	zaoblený	k2eAgFnSc1d1	zaoblená
<g/>
,	,	kIx,	,
40-47	[number]	k4	40-47
zubů	zub	k1gInPc2	zub
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
polovině	polovina	k1gFnSc6	polovina
čelisti	čelist	k1gFnSc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
ploutev	ploutev	k1gFnSc1	ploutev
ostrá	ostrý	k2eAgFnSc1d1	ostrá
<g/>
,	,	kIx,	,
prsní	prsní	k2eAgFnPc1d1	prsní
ploutve	ploutev	k1gFnPc1	ploutev
zakřivené	zakřivený	k2eAgFnPc1d1	zakřivená
<g/>
.	.	kIx.	.
</s>
<s>
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
je	být	k5eAaImIp3nS	být
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
mořích	moře	k1gNnPc6	moře
od	od	k7c2	od
tropického	tropický	k2eAgMnSc4d1	tropický
po	po	k7c4	po
mírné	mírný	k2eAgNnSc4d1	mírné
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zejména	zejména	k9	zejména
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejpočetnějším	početní	k2eAgInSc7d3	nejpočetnější
druhem	druh	k1gInSc7	druh
delfína	delfín	k1gMnSc2	delfín
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
miliónů	milión	k4xCgInPc2	milión
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
společenský	společenský	k2eAgInSc1d1	společenský
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
společenství	společenství	k1gNnSc6	společenství
o	o	k7c6	o
deseti	deset	k4xCc7	deset
až	až	k9	až
několika	několik	k4yIc7	několik
tisících	tisící	k4xOgMnPc6	tisící
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
hluboké	hluboký	k2eAgFnPc4d1	hluboká
vody	voda	k1gFnPc4	voda
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
lovu	lov	k1gInSc2	lov
kořisti	kořist	k1gFnSc2	kořist
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
mnoho	mnoho	k4c4	mnoho
skupin	skupina	k1gFnPc2	skupina
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
spolu	spolu	k6eAd1	spolu
různé	různý	k2eAgFnPc1d1	různá
skupiny	skupina	k1gFnPc1	skupina
soupeří	soupeřit	k5eAaImIp3nP	soupeřit
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
spánku	spánek	k1gInSc3	spánek
využívá	využívat	k5eAaImIp3nS	využívat
jen	jen	k9	jen
polovinu	polovina	k1gFnSc4	polovina
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
hemisférou	hemisféra	k1gFnSc7	hemisféra
je	být	k5eAaImIp3nS	být
bdělý	bdělý	k2eAgInSc1d1	bdělý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc4	dva
poloviny	polovina	k1gFnPc4	polovina
může	moct	k5eAaImIp3nS	moct
střídat	střídat	k5eAaImF	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Plave	plavat	k5eAaImIp3nS	plavat
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
hloubkou	hloubka	k1gFnSc7	hloubka
ponoru	ponor	k1gInSc2	ponor
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
