<s>
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Delphinus	Delphinus	k1gInSc1	Delphinus
delphis	delphis	k1gInSc1	delphis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
kytovce	kytovec	k1gMnSc2	kytovec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
delfínovití	delfínovitý	k2eAgMnPc1d1	delfínovitý
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
činí	činit	k5eAaImIp3nS	činit
1,8	[number]	k4	1,8
<g/>
-	-	kIx~	-
<g/>
2,6	[number]	k4	2,6
m	m	kA	m
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
90-140	[number]	k4	90-140
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
delfín	delfín	k1gMnSc1	delfín
skákavý	skákavý	k2eAgMnSc1d1	skákavý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
štíhlé	štíhlý	k2eAgNnSc1d1	štíhlé
<g/>
,	,	kIx,	,
základní	základní	k2eAgNnSc1d1	základní
zbarvení	zbarvení	k1gNnSc1	zbarvení
hnědé	hnědý	k2eAgFnSc2d1	hnědá
až	až	k8xS	až
černé	černý	k2eAgFnSc2d1	černá
<g/>
,	,	kIx,	,
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
bělavé	bělavý	k2eAgInPc1d1	bělavý
<g/>
,	,	kIx,	,
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
světlé	světlý	k2eAgFnSc2d1	světlá
skvrny	skvrna	k1gFnSc2	skvrna
a	a	k8xC	a
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
zaoblená	zaoblený	k2eAgFnSc1d1	zaoblená
<g/>
,	,	kIx,	,
40-47	[number]	k4	40-47
zubů	zub	k1gInPc2	zub
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
polovině	polovina	k1gFnSc6	polovina
čelisti	čelist	k1gFnSc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
ploutev	ploutev	k1gFnSc1	ploutev
ostrá	ostrý	k2eAgFnSc1d1	ostrá
<g/>
,	,	kIx,	,
prsní	prsní	k2eAgFnPc1d1	prsní
ploutve	ploutev	k1gFnPc1	ploutev
zakřivené	zakřivený	k2eAgFnPc1d1	zakřivená
<g/>
.	.	kIx.	.
</s>
<s>
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
je	být	k5eAaImIp3nS	být
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
mořích	moře	k1gNnPc6	moře
od	od	k7c2	od
tropického	tropický	k2eAgMnSc4d1	tropický
po	po	k7c4	po
mírné	mírný	k2eAgNnSc4d1	mírné
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zejména	zejména	k9	zejména
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejpočetnějším	početní	k2eAgInSc7d3	nejpočetnější
druhem	druh	k1gInSc7	druh
delfína	delfín	k1gMnSc2	delfín
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
miliónů	milión	k4xCgInPc2	milión
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
společenský	společenský	k2eAgInSc1d1	společenský
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
společenství	společenství	k1gNnSc6	společenství
o	o	k7c6	o
deseti	deset	k4xCc7	deset
až	až	k9	až
několika	několik	k4yIc7	několik
tisících	tisící	k4xOgMnPc6	tisící
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
hluboké	hluboký	k2eAgFnPc4d1	hluboká
vody	voda	k1gFnPc4	voda
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
lovu	lov	k1gInSc2	lov
kořisti	kořist	k1gFnSc2	kořist
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
mnoho	mnoho	k4c4	mnoho
skupin	skupina	k1gFnPc2	skupina
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
jindy	jindy	k6eAd1	jindy
spolu	spolu	k6eAd1	spolu
různé	různý	k2eAgFnPc1d1	různá
skupiny	skupina	k1gFnPc1	skupina
soupeří	soupeřit	k5eAaImIp3nP	soupeřit
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
spánku	spánek	k1gInSc3	spánek
využívá	využívat	k5eAaImIp3nS	využívat
jen	jen	k9	jen
polovinu	polovina	k1gFnSc4	polovina
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
hemisférou	hemisféra	k1gFnSc7	hemisféra
je	být	k5eAaImIp3nS	být
bdělý	bdělý	k2eAgInSc1d1	bdělý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc4	dva
poloviny	polovina	k1gFnPc4	polovina
může	moct	k5eAaImIp3nS	moct
střídat	střídat	k5eAaImF	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Plave	plavat	k5eAaImIp3nS	plavat
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
hloubkou	hloubka	k1gFnSc7	hloubka
ponoru	ponor	k1gInSc2	ponor
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
silnou	silný	k2eAgFnSc4d1	silná
ocasní	ocasní	k2eAgFnSc4d1	ocasní
ploutev	ploutev	k1gFnSc4	ploutev
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
skoky	skok	k1gInPc4	skok
nad	nad	k7c4	nad
hladinu	hladina	k1gFnSc4	hladina
i	i	k8xC	i
poskakování	poskakování	k1gNnSc4	poskakování
ve	v	k7c6	v
vzpřímeném	vzpřímený	k2eAgInSc6d1	vzpřímený
postoji	postoj	k1gInSc6	postoj
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
rychlých	rychlý	k2eAgInPc2d1	rychlý
úderů	úder	k1gInPc2	úder
o	o	k7c4	o
hladinu	hladina	k1gFnSc4	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
zůstat	zůstat	k5eAaPmF	zůstat
až	až	k9	až
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
nadechnutí	nadechnutí	k1gNnSc4	nadechnutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Domlouvá	domlouvat	k5eAaImIp3nS	domlouvat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
echolokace	echolokace	k1gFnSc2	echolokace
na	na	k7c6	na
frekvencích	frekvence	k1gFnPc6	frekvence
1000	[number]	k4	1000
Hz-	Hz-	k1gFnSc1	Hz-
<g/>
150	[number]	k4	150
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
11,5	[number]	k4	11,5
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
období	období	k1gNnSc2	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
je	být	k5eAaImIp3nS	být
jaro	jaro	k1gNnSc4	jaro
a	a	k8xC	a
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
v	v	k7c6	v
tropickém	tropický	k2eAgNnSc6d1	tropické
pásmu	pásmo	k1gNnSc6	pásmo
celoročně	celoročně	k6eAd1	celoročně
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
ocasem	ocas	k1gInSc7	ocas
napřed	napřed	k6eAd1	napřed
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
0.8	[number]	k4	0.8
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
m	m	kA	m
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Delfíni	Delfín	k1gMnPc1	Delfín
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
až	až	k9	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nejčastější	častý	k2eAgFnSc1d3	nejčastější
potrava	potrava	k1gFnSc1	potrava
jsou	být	k5eAaImIp3nP	být
sledi	sleď	k1gMnPc1	sleď
a	a	k8xC	a
makrely	makrela	k1gFnPc1	makrela
<g/>
,	,	kIx,	,
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
je	on	k3xPp3gNnSc4	on
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
shánějí	shánět	k5eAaImIp3nP	shánět
ryby	ryba	k1gFnPc1	ryba
k	k	k7c3	k
sobě	se	k3xPyFc3	se
a	a	k8xC	a
nahánějí	nahánět	k5eAaImIp3nP	nahánět
je	on	k3xPp3gNnSc4	on
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
byl	být	k5eAaImAgMnS	být
znám	znám	k2eAgMnSc1d1	znám
již	již	k9	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
antiky	antika	k1gFnSc2	antika
mezi	mezi	k7c4	mezi
Řeky	Řek	k1gMnPc4	Řek
a	a	k8xC	a
Římany	Říman	k1gMnPc4	Říman
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
předávaných	předávaný	k2eAgInPc2d1	předávaný
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
generaci	generace	k1gFnSc4	generace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
vozili	vozit	k5eAaImAgMnP	vozit
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
jako	jako	k9	jako
koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
zachraňovali	zachraňovat	k5eAaImAgMnP	zachraňovat
tonoucí	tonoucí	k2eAgMnPc1d1	tonoucí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jej	on	k3xPp3gMnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
"	"	kIx"	"
<g/>
delfín	delfín	k1gMnSc1	delfín
<g/>
"	"	kIx"	"
a	a	k8xC	a
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
savce	savec	k1gMnPc4	savec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
<g/>
.	.	kIx.	.
</s>
<s>
Carl	Carl	k1gMnSc1	Carl
Linné	Linné	k2eAgMnPc1d1	Linné
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Delphinus	Delphinus	k1gMnSc1	Delphinus
delphus	delphus	k1gMnSc1	delphus
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
výjimečnosti	výjimečnost	k1gFnSc6	výjimečnost
vztahu	vztah	k1gInSc2	vztah
kytovců	kytovec	k1gMnPc2	kytovec
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
mluví	mluvit	k5eAaImIp3nS	mluvit
nástěnné	nástěnný	k2eAgFnPc4d1	nástěnná
malby	malba	k1gFnPc4	malba
(	(	kIx(	(
<g/>
Grotta	Grotta	k1gFnSc1	Grotta
del	del	k?	del
Genovese	Genovese	k1gFnSc1	Genovese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
freska	freska	k1gFnSc1	freska
delfína	delfín	k1gMnSc2	delfín
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nalezená	nalezený	k2eAgFnSc1d1	nalezená
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Knossos	Knossosa	k1gFnPc2	Knossosa
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
nebo	nebo	k8xC	nebo
nalezené	nalezený	k2eAgNnSc1d1	nalezené
mumifikované	mumifikovaný	k2eAgNnSc1d1	mumifikované
tělo	tělo	k1gNnSc1	tělo
delfína	delfín	k1gMnSc2	delfín
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
v	v	k7c4	v
Askollen	Askollen	k1gInSc4	Askollen
<g/>
,	,	kIx,	,
Vestfold	Vestfold	k1gInSc4	Vestfold
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc1	zobrazení
delfína	delfín	k1gMnSc2	delfín
se	se	k3xPyFc4	se
našlo	najít	k5eAaPmAgNnS	najít
také	také	k9	také
na	na	k7c6	na
starověkých	starověký	k2eAgFnPc6d1	starověká
mincích	mince	k1gFnPc6	mince
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
kytovců	kytovec	k1gMnPc2	kytovec
i	i	k9	i
delfín	delfín	k1gMnSc1	delfín
obecný	obecný	k2eAgMnSc1d1	obecný
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
těžký	těžký	k2eAgInSc1d1	těžký
mozek	mozek	k1gInSc1	mozek
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
neokortexem	neokortex	k1gInSc7	neokortex
<g/>
,	,	kIx,	,
šedá	šedat	k5eAaImIp3nS	šedat
kůra	kůra	k1gFnSc1	kůra
mozková	mozkový	k2eAgFnSc1d1	mozková
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
hojnější	hojný	k2eAgMnSc1d2	hojnější
než	než	k8xS	než
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
opic	opice	k1gFnPc2	opice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
hojná	hojný	k2eAgFnSc1d1	hojná
než	než	k8xS	než
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
opakování	opakování	k1gNnSc2	opakování
a	a	k8xC	a
porozumění	porozumění	k1gNnSc2	porozumění
pojmům	pojem	k1gInPc3	pojem
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
delfínů	delfín	k1gMnPc2	delfín
prokázaná	prokázaný	k2eAgFnSc1d1	prokázaná
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
duševní	duševní	k2eAgFnSc4d1	duševní
činnost	činnost	k1gFnSc4	činnost
lze	lze	k6eAd1	lze
vypozorovat	vypozorovat	k5eAaPmF	vypozorovat
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
zejména	zejména	k9	zejména
při	při	k7c6	při
lovení	lovení	k1gNnSc6	lovení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
při	při	k7c6	při
námluvách	námluva	k1gFnPc6	námluva
a	a	k8xC	a
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
schopni	schopen	k2eAgMnPc1d1	schopen
rozvazovat	rozvazovat	k5eAaImF	rozvazovat
uzly	uzel	k1gInPc4	uzel
na	na	k7c6	na
rybářských	rybářský	k2eAgFnPc6d1	rybářská
sítích	síť	k1gFnPc6	síť
<g/>
,	,	kIx,	,
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
smluvený	smluvený	k2eAgInSc4d1	smluvený
signál	signál	k1gInSc4	signál
písknutí	písknutí	k1gNnSc2	písknutí
<g/>
,	,	kIx,	,
nahánět	nahánět	k5eAaImF	nahánět
ryby	ryba	k1gFnPc4	ryba
ve	v	k7c6	v
formacích	formace	k1gFnPc6	formace
apod.	apod.	kA	apod.
U	u	k7c2	u
delfínů	delfín	k1gMnPc2	delfín
držených	držený	k2eAgMnPc2d1	držený
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
schopnost	schopnost	k1gFnSc1	schopnost
používat	používat	k5eAaImF	používat
sled	sled	k1gInSc1	sled
pojmů	pojem	k1gInPc2	pojem
např.	např.	kA	např.
"	"	kIx"	"
<g/>
sahat	sahat	k5eAaImF	sahat
-	-	kIx~	-
míč	míč	k1gInSc4	míč
-	-	kIx~	-
ocas	ocas	k1gInSc4	ocas
-	-	kIx~	-
hledat	hledat	k5eAaImF	hledat
-	-	kIx~	-
talíř	talíř	k1gInSc4	talíř
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
průmyslově	průmyslově	k6eAd1	průmyslově
používaných	používaný	k2eAgFnPc2d1	používaná
sítí	síť	k1gFnPc2	síť
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
bylo	být	k5eAaImAgNnS	být
takto	takto	k6eAd1	takto
chyceno	chytit	k5eAaPmNgNnS	chytit
až	až	k9	až
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zlepšenému	zlepšený	k2eAgInSc3d1	zlepšený
způsobu	způsob	k1gInSc3	způsob
rybolovu	rybolov	k1gInSc2	rybolov
a	a	k8xC	a
přísnějším	přísný	k2eAgNnSc7d2	přísnější
nařízením	nařízení	k1gNnSc7	nařízení
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
počet	počet	k1gInSc4	počet
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
značně	značně	k6eAd1	značně
snížen	snížit	k5eAaPmNgInS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
s	s	k7c7	s
velrybářskou	velrybářský	k2eAgFnSc7d1	velrybářská
tradicí	tradice	k1gFnSc7	tradice
dodnes	dodnes	k6eAd1	dodnes
loví	lovit	k5eAaImIp3nP	lovit
i	i	k9	i
delfína	delfín	k1gMnSc2	delfín
obecného	obecný	k2eAgMnSc2d1	obecný
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
velrybářské	velrybářský	k2eAgFnSc2d1	velrybářská
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
klauzule	klauzule	k1gFnSc1	klauzule
o	o	k7c6	o
"	"	kIx"	"
<g/>
výjimce	výjimka	k1gFnSc6	výjimka
pro	pro	k7c4	pro
domorodce	domorodec	k1gMnPc4	domorodec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
negativní	negativní	k2eAgInSc1d1	negativní
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
kontaminace	kontaminace	k1gFnPc4	kontaminace
mořských	mořský	k2eAgFnPc2d1	mořská
vod	voda	k1gFnPc2	voda
zejména	zejména	k9	zejména
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
jedinci	jedinec	k1gMnPc1	jedinec
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
kontaminovaní	kontaminovaný	k2eAgMnPc1d1	kontaminovaný
rtutí	rtuť	k1gFnPc2	rtuť
<g/>
,	,	kIx,	,
freony	freon	k1gInPc7	freon
a	a	k8xC	a
těžkými	těžký	k2eAgInPc7d1	těžký
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
chovu	chov	k1gInSc3	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
delfín	delfín	k1gMnSc1	delfín
obecný	obecný	k2eAgInSc4d1	obecný
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
o	o	k7c4	o
delfína	delfín	k1gMnSc4	delfín
skákavého	skákavý	k2eAgMnSc4d1	skákavý
nebo	nebo	k8xC	nebo
kosatek	kosatka	k1gFnPc2	kosatka
černých	černý	k2eAgFnPc2d1	černá
příliš	příliš	k6eAd1	příliš
nehodí	hodit	k5eNaPmIp3nP	hodit
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
odmítá	odmítat	k5eAaImIp3nS	odmítat
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
a	a	k8xC	a
takoví	takový	k3xDgMnPc1	takový
jedinci	jedinec	k1gMnPc1	jedinec
zpravidla	zpravidla	k6eAd1	zpravidla
do	do	k7c2	do
roka	rok	k1gInSc2	rok
uhynuli	uhynout	k5eAaPmAgMnP	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
také	také	k6eAd1	také
páchali	páchat	k5eAaImAgMnP	páchat
sebevraždy	sebevražda	k1gFnSc2	sebevražda
naražením	naražení	k1gNnSc7	naražení
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Delfín	Delfín	k1gMnSc1	Delfín
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
