<p>
<s>
Estuár	estuár	k1gInSc1	estuár
nebo	nebo	k8xC	nebo
též	též	k9	též
estuárium	estuárium	k1gNnSc4	estuárium
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
říčního	říční	k2eAgNnSc2d1	říční
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
<g/>
,	,	kIx,	,
nálevkovitý	nálevkovitý	k2eAgInSc4d1	nálevkovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
typům	typ	k1gInPc3	typ
ústí	ústí	k1gNnSc2	ústí
řek	řeka	k1gFnPc2	řeka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
a	a	k8xC	a
výrazném	výrazný	k2eAgInSc6d1	výrazný
projevu	projev	k1gInSc6	projev
slapových	slapový	k2eAgInPc2d1	slapový
jevů	jev	k1gInPc2	jev
(	(	kIx(	(
<g/>
příliv	příliv	k1gInSc1	příliv
a	a	k8xC	a
odliv	odliv	k1gInSc1	odliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgNnPc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pronikání	pronikání	k1gNnSc1	pronikání
slané	slaný	k2eAgFnSc2d1	slaná
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
brakické	brakický	k2eAgFnSc2d1	brakická
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
odnos	odnos	k1gInSc1	odnos
říčních	říční	k2eAgInPc2d1	říční
sedimentů	sediment	k1gInPc2	sediment
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
(	(	kIx(	(
<g/>
zlepšení	zlepšení	k1gNnSc4	zlepšení
podmínek	podmínka	k1gFnPc2	podmínka
vodní	vodní	k2eAgFnSc2d1	vodní
dopravy	doprava	k1gFnSc2	doprava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
eroze	eroze	k1gFnSc1	eroze
břehů	břeh	k1gInPc2	břeh
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
poloze	poloha	k1gFnSc6	poloha
toku	tok	k1gInSc2	tok
–	–	k?	–
uplatnění	uplatnění	k1gNnSc2	uplatnění
Coriolisovy	Coriolisův	k2eAgFnSc2d1	Coriolisova
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ztížení	ztížení	k1gNnSc1	ztížení
budování	budování	k1gNnSc2	budování
přístavů	přístav	k1gInPc2	přístav
(	(	kIx(	(
<g/>
výkyvy	výkyv	k1gInPc4	výkyv
hladin	hladina	k1gFnPc2	hladina
<g/>
)	)	kIx)	)
<g/>
Estuár	estuár	k1gInSc1	estuár
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
např.	např.	kA	např.
u	u	k7c2	u
řek	řeka	k1gFnPc2	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
Temže	Temže	k1gFnSc2	Temže
nebo	nebo	k8xC	nebo
Něvy	Něva	k1gFnSc2	Něva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
estuár	estuár	k1gInSc4	estuár
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
Río	Río	k1gFnSc1	Río
de	de	k?	de
la	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Delta	delta	k1gFnSc1	delta
</s>
</p>
<p>
<s>
Liman	Liman	k1gMnSc1	Liman
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Aestuarium	Aestuarium	k1gNnSc4	Aestuarium
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
estuár	estuár	k1gInSc1	estuár
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
