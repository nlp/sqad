<s>
Vejce	vejce	k1gNnSc1
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1
popis	popis	k1gInSc1
vejce	vejce	k1gNnSc2
v	v	k7c6
podélném	podélný	k2eAgInSc6d1
řezu	řez	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
skořápka	skořápka	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
vnější	vnější	k2eAgFnSc1d1
papírová	papírový	k2eAgFnSc1d1
blána	blána	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
vnitřní	vnitřní	k2eAgFnSc1d1
papírová	papírový	k2eAgFnSc1d1
blána	blána	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
poutko	poutko	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
vnější	vnější	k2eAgInSc4d1
řídký	řídký	k2eAgInSc4d1
bílek	bílek	k1gInSc4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
hustý	hustý	k2eAgInSc1d1
bílek	bílek	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
žloutková	žloutkový	k2eAgFnSc1d1
blána	blána	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
výživný	výživný	k2eAgInSc1d1
žloutek	žloutek	k1gInSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
zárodečný	zárodečný	k2eAgInSc1d1
terčík	terčík	k1gInSc1
(	(	kIx(
<g/>
tvořivý	tvořivý	k2eAgInSc1d1
žloutek	žloutek	k1gInSc1
+	+	kIx~
zárodek	zárodek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
tmavý	tmavý	k2eAgInSc1d1
(	(	kIx(
<g/>
žlutý	žlutý	k2eAgInSc1d1
<g/>
)	)	kIx)
žloutek	žloutek	k1gInSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
světlý	světlý	k2eAgInSc1d1
žloutek	žloutek	k1gInSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
vnitřní	vnitřní	k2eAgInSc1d1
řídký	řídký	k2eAgInSc1d1
bílek	bílek	k1gInSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
poutko	poutko	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
vzduchová	vzduchový	k2eAgFnSc1d1
komůrka	komůrka	k1gFnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
kutikula	kutikula	k1gFnSc1
</s>
<s>
Vejce	vejce	k1gNnSc1
je	být	k5eAaImIp3nS
plod	plod	k1gInSc4
plazů	plaz	k1gMnPc2
<g/>
,	,	kIx,
ptáků	pták	k1gMnPc2
a	a	k8xC
vejcorodých	vejcorodý	k2eAgMnPc2d1
savců	savec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
základními	základní	k2eAgFnPc7d1
strukturálními	strukturální	k2eAgFnPc7d1
složkami	složka	k1gFnPc7
jsou	být	k5eAaImIp3nP
žloutek	žloutek	k1gInSc1
<g/>
,	,	kIx,
bílek	bílek	k1gInSc1
a	a	k8xC
vaječná	vaječný	k2eAgFnSc1d1
skořápka	skořápka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vejce	vejce	k1gNnSc1
vzniká	vznikat	k5eAaImIp3nS
v	v	k7c6
těle	tělo	k1gNnSc6
samice	samice	k1gFnSc2
z	z	k7c2
oplodněného	oplodněný	k2eAgNnSc2d1
vajíčka	vajíčko	k1gNnSc2
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
vše	všechen	k3xTgNnSc1
podstatné	podstatný	k2eAgNnSc1d1
pro	pro	k7c4
zrození	zrození	k1gNnSc4
a	a	k8xC
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
klade	klást	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
či	či	k8xC
více	hodně	k6eAd2
vajec	vejce	k1gNnPc2
do	do	k7c2
vhodného	vhodný	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
vyvíjí	vyvíjet	k5eAaImIp3nS
samostatně	samostatně	k6eAd1
až	až	k9
do	do	k7c2
vylíhnutí	vylíhnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
vejcoživorodých	vejcoživorodý	k2eAgInPc2d1
se	se	k3xPyFc4
vejce	vejce	k1gNnSc1
vyvíjí	vyvíjet	k5eAaImIp3nS
v	v	k7c6
těle	tělo	k1gNnSc6
matky	matka	k1gFnSc2
a	a	k8xC
mláďata	mládě	k1gNnPc1
se	se	k3xPyFc4
líhnou	líhnout	k5eAaImIp3nP
těsně	těsně	k6eAd1
před	před	k7c7
snůškou	snůška	k1gFnSc7
(	(	kIx(
<g/>
porodem	porod	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
jejím	její	k3xOp3gInSc6
průběhu	průběh	k1gInSc6
nebo	nebo	k8xC
vzápětí	vzápětí	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhled	vzhled	k1gInSc1
vajec	vejce	k1gNnPc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
podle	podle	k7c2
živočišného	živočišný	k2eAgInSc2d1
druhu	druh	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gInSc1
obsah	obsah	k1gInSc1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
chráněn	chránit	k5eAaImNgInS
pevnějším	pevný	k2eAgInSc7d2
povrchem	povrch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdému	tvrdý	k2eAgInSc3d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
pórovitému	pórovitý	k2eAgInSc3d1
povrchu	povrch	k1gInSc3
vejce	vejce	k1gNnSc2
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
skořápka	skořápka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
zárodečného	zárodečný	k2eAgInSc2d1
terčíku	terčík	k1gInSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
vyvinout	vyvinout	k5eAaPmF
mládě	mládě	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
nemusí	muset	k5eNaImIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	on	k3xPp3gNnPc4
vejce	vejce	k1gNnPc4
neoplozené	oplozený	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zárodečnému	zárodečný	k2eAgInSc3d1
terčíku	terčík	k1gInSc3
se	se	k3xPyFc4
také	také	k9
říká	říkat	k5eAaImIp3nS
očko	očko	k1gNnSc1
nebo	nebo	k8xC
zárodek	zárodek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vejce	vejce	k1gNnSc1
se	se	k3xPyFc4
snadno	snadno	k6eAd1
stává	stávat	k5eAaImIp3nS
výživnou	výživný	k2eAgFnSc7d1
potravou	potrava	k1gFnSc7
ostatních	ostatní	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
nebo	nebo	k8xC
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
lidském	lidský	k2eAgInSc6d1
jídelníčku	jídelníček	k1gInSc6
jsou	být	k5eAaImIp3nP
oblíbená	oblíbený	k2eAgNnPc1d1
zejména	zejména	k9
vejce	vejce	k1gNnPc4
slepičí	slepičí	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristický	charakteristický	k2eAgInSc1d1
oválný	oválný	k2eAgInSc1d1
tvar	tvar	k1gInSc1
slepičího	slepičí	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
estetickým	estetický	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
a	a	k8xC
symbolem	symbol	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Oologie	Oologie	k1gFnSc1
je	být	k5eAaImIp3nS
věda	věda	k1gFnSc1
<g/>
,	,	kIx,
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
studiem	studio	k1gNnSc7
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
ptačích	ptačí	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
vajec	vejce	k1gNnPc2
domácí	domácí	k2eAgFnSc2d1
drůbeže	drůbež	k1gFnSc2
</s>
<s>
Základní	základní	k2eAgInSc1d1
význam	význam	k1gInSc1
vajec	vejce	k1gNnPc2
domácí	domácí	k2eAgFnSc2d1
drůbeže	drůbež	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
biologický	biologický	k2eAgInSc1d1
<g/>
,	,	kIx,
tj.	tj.	kA
zajistit	zajistit	k5eAaPmF
reprodukci	reprodukce	k1gFnSc4
daného	daný	k2eAgInSc2d1
druhu	druh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
k	k	k7c3
vývoji	vývoj	k1gInSc3
nového	nový	k2eAgMnSc2d1
jedince	jedinec	k1gMnSc2
dochází	docházet	k5eAaImIp3nS
mimo	mimo	k7c4
tělo	tělo	k1gNnSc4
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
vejce	vejce	k1gNnSc1
všechny	všechen	k3xTgFnPc1
důležité	důležitý	k2eAgFnPc1d1
výživné	výživný	k2eAgFnPc1d1
složky	složka	k1gFnPc1
nezbytné	nezbytný	k2eAgFnPc1d1,k2eNgFnPc1d1
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
nového	nový	k2eAgInSc2d1
organismu	organismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
vejce	vejce	k1gNnPc4
krůt	krůta	k1gFnPc2
<g/>
,	,	kIx,
kachen	kachna	k1gFnPc2
a	a	k8xC
hus	husa	k1gFnPc2
jsou	být	k5eAaImIp3nP
produkována	produkovat	k5eAaImNgNnP
hlavně	hlavně	k9
pro	pro	k7c4
účely	účel	k1gInPc4
reprodukční	reprodukční	k2eAgInSc1d1
<g/>
,	,	kIx,
tj.	tj.	kA
slouží	sloužit	k5eAaImIp3nP
jako	jako	k9
vejce	vejce	k1gNnPc1
násadová	násadový	k2eAgNnPc1d1
<g/>
,	,	kIx,
slepičí	slepičí	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
slouží	sloužit	k5eAaImIp3nP
také	také	k9
jako	jako	k9
vejce	vejce	k1gNnPc1
konzumní	konzumní	k2eAgNnPc1d1
a	a	k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
součástí	součást	k1gFnSc7
lidské	lidský	k2eAgFnSc2d1
výživy	výživa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejce	vejce	k1gNnSc1
mají	mít	k5eAaImIp3nP
vysoký	vysoký	k2eAgInSc4d1
obsah	obsah	k1gInSc4
plnohodnotných	plnohodnotný	k2eAgFnPc2d1
bílkovin	bílkovina	k1gFnPc2
(	(	kIx(
<g/>
obsahují	obsahovat	k5eAaImIp3nP
všechny	všechen	k3xTgInPc4
aminokyseliny	aminokyselina	k1gFnPc1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
nezbytné	zbytný	k2eNgNnSc1d1,k2eAgNnSc1d1
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
poměru	poměr	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
nejpříznivější	příznivý	k2eAgMnSc1d3
ze	z	k7c2
všech	všecek	k3xTgFnPc2
běžných	běžný	k2eAgFnPc2d1
potravin	potravina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejce	vejce	k1gNnPc1
dále	daleko	k6eAd2
obsahují	obsahovat	k5eAaImIp3nP
tuky	tuk	k1gInPc4
<g/>
,	,	kIx,
vitamíny	vitamín	k1gInPc4
a	a	k8xC
minerální	minerální	k2eAgFnPc4d1
látky	látka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
obsahují	obsahovat	k5eAaImIp3nP
i	i	k9
vysoké	vysoký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
cholesterolu	cholesterol	k1gInSc2
<g/>
,	,	kIx,
takže	takže	k8xS
konzumace	konzumace	k1gFnSc1
3	#num#	k4
a	a	k8xC
více	hodně	k6eAd2
vajec	vejce	k1gNnPc2
denně	denně	k6eAd1
prokazatelně	prokazatelně	k6eAd1
zvyšuje	zvyšovat	k5eAaImIp3nS
riziko	riziko	k1gNnSc4
onemocnění	onemocnění	k1gNnSc2
a	a	k8xC
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
přímé	přímý	k2eAgFnSc2d1
spotřeby	spotřeba	k1gFnSc2
vajec	vejce	k1gNnPc2
jako	jako	k8xC,k8xS
potraviny	potravina	k1gFnPc4
se	se	k3xPyFc4
vejce	vejce	k1gNnSc1
(	(	kIx(
<g/>
vaječná	vaječný	k2eAgFnSc1d1
hmota	hmota	k1gFnSc1
<g/>
,	,	kIx,
bílek	bílek	k1gInSc1
<g/>
,	,	kIx,
žloutek	žloutek	k1gInSc1
<g/>
)	)	kIx)
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
jako	jako	k8xC,k8xS
surovina	surovina	k1gFnSc1
v	v	k7c6
různých	různý	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
potravinářského	potravinářský	k2eAgNnSc2d1
(	(	kIx(
<g/>
pekařství	pekařství	k1gNnSc1
<g/>
,	,	kIx,
cukrářství	cukrářství	k1gNnSc1
<g/>
,	,	kIx,
výroba	výroba	k1gFnSc1
trvanlivého	trvanlivý	k2eAgNnSc2d1
pečiva	pečivo	k1gNnSc2
<g/>
,	,	kIx,
těstovin	těstovina	k1gFnPc2
<g/>
,	,	kIx,
masných	masný	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
aj.	aj.	kA
<g/>
)	)	kIx)
i	i	k8xC
nepotravinářského	potravinářský	k2eNgInSc2d1
průmyslu	průmysl	k1gInSc2
(	(	kIx(
<g/>
farmaceutický	farmaceutický	k2eAgInSc4d1
<g/>
,	,	kIx,
kožedělný	kožedělný	k2eAgInSc4d1
<g/>
,	,	kIx,
textilní	textilní	k2eAgInSc4d1
<g/>
,	,	kIx,
chemický	chemický	k2eAgInSc4d1
<g/>
,	,	kIx,
fotografický	fotografický	k2eAgInSc4d1
<g/>
,	,	kIx,
sklářský	sklářský	k2eAgInSc4d1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neméně	málo	k6eNd2
významné	významný	k2eAgNnSc4d1
je	být	k5eAaImIp3nS
využití	využití	k1gNnSc4
vajec	vejce	k1gNnPc2
v	v	k7c6
humánní	humánní	k2eAgFnSc6d1
i	i	k8xC
veterinární	veterinární	k2eAgFnSc6d1
medicíně	medicína	k1gFnSc6
(	(	kIx(
<g/>
výroba	výroba	k1gFnSc1
očkovacích	očkovací	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
ředidlo	ředidlo	k1gNnSc1
semene	semeno	k1gNnSc2
při	při	k7c6
inseminaci	inseminace	k1gFnSc6
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jakost	jakost	k1gFnSc1
konzumních	konzumní	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
je	být	k5eAaImIp3nS
dána	dát	k5eAaPmNgFnS
souhrnem	souhrn	k1gInSc7
jejich	jejich	k3xOp3gFnPc2
vnějších	vnější	k2eAgFnPc2d1
a	a	k8xC
vnitřních	vnitřní	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnější	vnější	k2eAgFnSc1d1
(	(	kIx(
<g/>
technologická	technologický	k2eAgFnSc1d1
<g/>
)	)	kIx)
hodnota	hodnota	k1gFnSc1
konzumních	konzumní	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
je	být	k5eAaImIp3nS
určována	určovat	k5eAaImNgFnS
hmotností	hmotnost	k1gFnSc7
(	(	kIx(
<g/>
velikostí	velikost	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tvarem	tvar	k1gInSc7
<g/>
,	,	kIx,
podílem	podíl	k1gInSc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
vejce	vejce	k1gNnSc2
a	a	k8xC
kvalitou	kvalita	k1gFnSc7
vaječné	vaječný	k2eAgFnSc2d1
skořápky	skořápka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
vejce	vejce	k1gNnSc2
(	(	kIx(
<g/>
žloutek	žloutek	k1gInSc1
<g/>
,	,	kIx,
bílek	bílek	k1gInSc1
a	a	k8xC
skořápka	skořápka	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ovlivňován	ovlivňovat	k5eAaImNgInS
druhem	druh	k1gInSc7
a	a	k8xC
plemenem	plemeno	k1gNnSc7
drůbeže	drůbež	k1gFnSc2
<g/>
,	,	kIx,
velikostí	velikost	k1gFnSc7
vejce	vejce	k1gNnSc2
<g/>
,	,	kIx,
počtem	počet	k1gInSc7
snesených	snesený	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
a	a	k8xC
individuálními	individuální	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
nosnic	nosnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitřní	vnitřní	k2eAgFnSc1d1
(	(	kIx(
<g/>
nutriční	nutriční	k2eAgFnSc1d1
<g/>
)	)	kIx)
hodnota	hodnota	k1gFnSc1
konzumních	konzumní	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
jako	jako	k8xS,k8xC
potraviny	potravina	k1gFnPc4
je	být	k5eAaImIp3nS
dána	dát	k5eAaPmNgFnS
obsahem	obsah	k1gInSc7
živin	živina	k1gFnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc7
stravitelností	stravitelnost	k1gFnSc7
<g/>
,	,	kIx,
chutí	chuť	k1gFnSc7
a	a	k8xC
vůní	vůně	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Kráječ	kráječ	k1gInSc1
vajec	vejce	k1gNnPc2
</s>
<s>
Vzhled	vzhled	k1gInSc1
ptačího	ptačí	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
slepičího	slepičí	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
<g/>
58,0	58,0	k4
gDelší	gDelšet	k5eAaImIp3nP,k5eAaPmIp3nP
obvod	obvod	k1gInSc1
<g/>
15,7	15,7	k4
cm	cm	kA
</s>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1
osa	osa	k1gFnSc1
<g/>
5,7	5,7	k4
cmKratší	cmKratší	k2eAgInSc1d1
obvod	obvod	k1gInSc1
<g/>
13,5	13,5	k4
cm	cm	kA
</s>
<s>
Krátká	krátký	k2eAgFnSc1d1
osa	osa	k1gFnSc1
<g/>
4,2	4,2	k4
cmObjem	cmObj	k1gInSc7
<g/>
53,0	53,0	k4
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Index	index	k1gInSc1
tvaru	tvar	k1gInSc2
<g/>
1,36	1,36	k4
<g/>
Povrch	povrch	k7c2wR
<g/>
68,0	68,0	k4
cm	cm	kA
<g/>
2	#num#	k4
</s>
<s>
Bílek	bílek	k1gInSc1
<g/>
:	:	kIx,
index	index	k1gInSc1
min	min	kA
<g/>
.80	.80	k4
<g/>
Výška	výška	k1gFnSc1
vzd	vzd	k?
<g/>
.	.	kIx.
komůrky	komůrka	k1gFnSc2
<g/>
4	#num#	k4
mm	mm	kA
</s>
<s>
výška	výška	k1gFnSc1
<g/>
4,64	4,64	k4
mmTloušťka	mmTloušťka	k1gFnSc1
skořápky	skořápka	k1gFnSc2
<g/>
0,3	0,3	k4
mm	mm	kA
</s>
<s>
šířka	šířka	k1gFnSc1
<g/>
5,2	5,2	k4
cmPočet	cmPočet	k1gInSc4
pórů	pór	k1gInPc2
<g/>
10	#num#	k4
000	#num#	k4
</s>
<s>
délka	délka	k1gFnSc1
<g/>
6,4	6,4	k4
cmŽloutek	cmŽloutek	k1gInSc1
<g/>
:	:	kIx,
index	index	k1gInSc1
min	min	kA
<g/>
.0	.0	k4
<g/>
,48	,48	k4
</s>
<s>
Žloutek	žloutek	k1gInSc1
<g/>
:	:	kIx,
<g/>
barvasytě	barvasytě	k6eAd1
oranžovávýška	oranžovávýška	k1gFnSc1
<g/>
16	#num#	k4
mm	mm	kA
</s>
<s>
hodnocení	hodnocení	k1gNnSc1
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
st.	st.	kA
LaRochešířka	LaRochešířka	k1gFnSc1
<g/>
3,4	3,4	k4
cm	cm	kA
</s>
<s>
Tvar	tvar	k1gInSc1
</s>
<s>
Ptačí	ptačí	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
má	mít	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
oválný	oválný	k2eAgInSc1d1
tvar	tvar	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
vytvářející	vytvářející	k2eAgInSc4d1
ostřejší	ostrý	k2eAgInSc4d2
vrchol	vrchol	k1gInSc4
(	(	kIx(
<g/>
špičku	špička	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
vejce	vejce	k1gNnPc1
se	se	k3xPyFc4
ovšem	ovšem	k9
od	od	k7c2
tohoto	tento	k3xDgInSc2
tvaru	tvar	k1gInSc2
poněkud	poněkud	k6eAd1
odlišují	odlišovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejce	vejce	k1gNnPc4
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
hnízdících	hnízdící	k2eAgInPc2d1
na	na	k7c6
útesech	útes	k1gInPc6
<g/>
,	,	kIx,
například	například	k6eAd1
alkounů	alkoun	k1gMnPc2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
relativně	relativně	k6eAd1
výraznou	výrazný	k2eAgFnSc4d1
špičku	špička	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
jejich	jejich	k3xOp3gInSc1
tvar	tvar	k1gInSc1
připomíná	připomínat	k5eAaImIp3nS
kužel	kužel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
vejce	vejce	k1gNnPc1
neskutálela	skutálet	k5eNaPmAgNnP
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
se	se	k3xPyFc4
pouze	pouze	k6eAd1
otáčela	otáčet	k5eAaImAgFnS
na	na	k7c6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
ptáci	pták	k1gMnPc1
<g/>
,	,	kIx,
žijící	žijící	k2eAgMnPc1d1
v	v	k7c6
dutinách	dutina	k1gFnPc6
<g/>
,	,	kIx,
mívají	mívat	k5eAaImIp3nP
vejce	vejce	k1gNnPc1
kulovitá	kulovitý	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Tvar	tvar	k1gInSc1
vejce	vejce	k1gNnSc2
</s>
<s>
Podlouhlé	podlouhlý	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
ptáka	pták	k1gMnSc2
emu	emu	k1gMnSc2
</s>
<s>
Kulovité	kulovitý	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
papouška	papoušek	k1gMnSc2
senegalského	senegalský	k2eAgMnSc2d1
</s>
<s>
Zašpičatělé	zašpičatělý	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
(	(	kIx(
<g/>
vyhynulé	vyhynulý	k2eAgFnPc1d1
<g/>
)	)	kIx)
alky	alka	k1gFnPc1
velké	velký	k2eAgFnSc3d1
</s>
<s>
Z	z	k7c2
pohledu	pohled	k1gInSc2
matematiky	matematika	k1gFnSc2
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
výzvou	výzva	k1gFnSc7
stanovení	stanovení	k1gNnSc2
rovnice	rovnice	k1gFnSc2
křivky	křivka	k1gFnSc2
popisující	popisující	k2eAgInSc4d1
tvar	tvar	k1gInSc4
vejce	vejce	k1gNnSc2
.	.	kIx.
</s>
<s>
Velikost	velikost	k1gFnSc1
</s>
<s>
Velikost	velikost	k1gFnSc1
vejce	vejce	k1gNnSc2
<g/>
:	:	kIx,
Kolibří	kolibří	k2eAgNnPc1d1
<g/>
,	,	kIx,
slepičí	slepičí	k2eAgNnPc1d1
a	a	k8xC
pštrosí	pštrosí	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
</s>
<s>
Velikost	velikost	k1gFnSc1
vejce	vejce	k1gNnSc2
se	se	k3xPyFc4
značně	značně	k6eAd1
liší	lišit	k5eAaImIp3nP
podle	podle	k7c2
druhu	druh	k1gInSc2
–	–	k?
nejmenší	malý	k2eAgNnSc4d3
vejce	vejce	k1gNnSc4
ze	z	k7c2
všech	všecek	k3xTgMnPc2
žijících	žijící	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
snáší	snášet	k5eAaImIp3nS
kolibřík	kolibřík	k1gMnSc1
–	–	k?
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
vejce	vejce	k1gNnSc2
váží	vážit	k5eAaImIp3nS
0,25	0,25	k4
g	g	kA
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
největší	veliký	k2eAgNnSc4d3
vejce	vejce	k1gNnSc4
ze	z	k7c2
všech	všecek	k3xTgMnPc2
žijících	žijící	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
snáší	snášet	k5eAaImIp3nS
pštros	pštros	k1gMnSc1
–	–	k?
má	mít	k5eAaImIp3nS
hmotnost	hmotnost	k1gFnSc1
750	#num#	k4
až	až	k9
1600	#num#	k4
gramů	gram	k1gInPc2
<g/>
,	,	kIx,
rozměry	rozměr	k1gInPc1
cca	cca	kA
16	#num#	k4
<g/>
×	×	k?
<g/>
13	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
vyhynulí	vyhynulý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
a	a	k8xC
Aepyornis	Aepyornis	k1gInSc4
měli	mít	k5eAaImAgMnP
ovšem	ovšem	k9
vejce	vejce	k1gNnSc4
větší	veliký	k2eAgNnSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnPc4d3
slepičí	slepičí	k2eAgNnPc4d1
vejce	vejce	k1gNnPc4
na	na	k7c6
světě	svět	k1gInSc6
váží	vážit	k5eAaImIp3nS
169	#num#	k4
gramů	gram	k1gInPc2
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
vejce	vejce	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
snesla	snést	k5eAaPmAgFnS
slepice	slepice	k1gFnSc1
v	v	k7c6
německém	německý	k2eAgNnSc6d1
Langwege	Langwege	k1gNnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
asi	asi	k9
trojnásobně	trojnásobně	k6eAd1
větší	veliký	k2eAgNnSc4d2
než	než	k8xS
běžné	běžný	k2eAgNnSc4d1
vejce	vejce	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíly	rozdíl	k1gInPc7
jsou	být	k5eAaImIp3nP
ovšem	ovšem	k9
i	i	k9
v	v	k7c6
poměru	poměr	k1gInSc6
hmotnosti	hmotnost	k1gFnSc2
ptáka	pták	k1gMnSc2
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
vejcem	vejce	k1gNnSc7
–	–	k?
tak	tak	k9
právě	právě	k6eAd1
vejce	vejce	k1gNnSc1
kolibříka	kolibřík	k1gMnSc2
představuje	představovat	k5eAaImIp3nS
asi	asi	k9
25	#num#	k4
%	%	kIx~
hmotnosti	hmotnost	k1gFnSc2
ptáka	pták	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nejvíce	nejvíce	k6eAd1,k6eAd3
mezi	mezi	k7c4
ptáky	pták	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
u	u	k7c2
ptáka	pták	k1gMnSc2
kivi	kivi	k1gMnSc1
představuje	představovat	k5eAaImIp3nS
vejce	vejce	k1gNnSc2
čtvrtinu	čtvrtina	k1gFnSc4
hmotnosti	hmotnost	k1gFnSc2
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
pštrosí	pštrosí	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
váží	vážit	k5eAaImIp3nP
jen	jen	k9
1	#num#	k4
%	%	kIx~
hmotnosti	hmotnost	k1gFnSc2
dospělého	dospělý	k1gMnSc4
jedince	jedinec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Skořápka	skořápka	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
skořápka	skořápka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vejce	vejce	k1gNnSc1
tinamy	tinama	k1gFnSc2
tečkované	tečkovaný	k2eAgFnSc2d1
</s>
<s>
Skořápka	skořápka	k1gFnSc1
je	být	k5eAaImIp3nS
vápenitý	vápenitý	k2eAgInSc4d1
obal	obal	k1gInSc4
ptačího	ptačí	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Bývá	bývat	k5eAaImIp3nS
barevná	barevný	k2eAgFnSc1d1
(	(	kIx(
<g/>
podle	podle	k7c2
barvy	barva	k1gFnSc2
prostředí	prostředí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dostatečně	dostatečně	k6eAd1
silná	silný	k2eAgFnSc1d1
aby	aby	kYmCp3nS
unesla	unést	k5eAaPmAgFnS
ptáka	pták	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
na	na	k7c6
ní	on	k3xPp3gFnSc6
sedí	sedit	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
dost	dost	k6eAd1
tenká	tenký	k2eAgFnSc1d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
líhnoucí	líhnoucí	k2eAgNnSc1d1
ptáče	ptáče	k1gNnSc1
dokázalo	dokázat	k5eAaPmAgNnS
z	z	k7c2
vejce	vejce	k1gNnSc2
proklovat	proklovat	k5eAaPmF
a	a	k8xC
navíc	navíc	k6eAd1
pórovitá	pórovitý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základními	základní	k2eAgFnPc7d1
strukturálními	strukturální	k2eAgFnPc7d1
součástmi	součást	k1gFnPc7
skořápky	skořápka	k1gFnPc4
jsou	být	k5eAaImIp3nP
podskořápečné	podskořápečný	k2eAgFnPc4d1
blány	blána	k1gFnPc4
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgFnSc1d1
skořápka	skořápka	k1gFnSc1
a	a	k8xC
kutikula	kutikula	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uváděné	uváděný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
se	se	k3xPyFc4
týkají	týkat	k5eAaImIp3nP
převážně	převážně	k6eAd1
slepičích	slepičí	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
o	o	k7c6
kterých	který	k3yIgFnPc6,k3yRgFnPc6,k3yQgFnPc6
existuje	existovat	k5eAaImIp3nS
nejvíce	nejvíce	k6eAd1,k6eAd3
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sušina	sušina	k1gFnSc1
skořápky	skořápka	k1gFnSc2
činí	činit	k5eAaImIp3nS
asi	asi	k9
98	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
kolem	kolem	k7c2
2	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
%	%	kIx~
organických	organický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
(	(	kIx(
<g/>
glykoproteinový	glykoproteinový	k2eAgInSc1d1
komplex	komplex	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
množství	množství	k1gNnSc1
se	s	k7c7
směrem	směr	k1gInSc7
k	k	k7c3
povrchu	povrch	k1gInSc3
skořápky	skořápka	k1gFnSc2
postupně	postupně	k6eAd1
snižuje	snižovat	k5eAaImIp3nS
<g/>
)	)	kIx)
a	a	k8xC
95	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
%	%	kIx~
anorganických	anorganický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
(	(	kIx(
<g/>
89	#num#	k4
<g/>
–	–	k?
<g/>
97	#num#	k4
%	%	kIx~
uhličitanu	uhličitan	k1gInSc2
vápenatého	vápenatý	k2eAgInSc2d1
<g/>
,	,	kIx,
až	až	k9
2	#num#	k4
%	%	kIx~
uhličitanu	uhličitan	k1gInSc2
hořečnatého	hořečnatý	k2eAgInSc2d1
<g/>
,	,	kIx,
0,5	0,5	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
%	%	kIx~
fosforečnanu	fosforečnan	k1gInSc2
vápenatého	vápenatý	k2eAgInSc2d1
a	a	k8xC
hořečnatého	hořečnatý	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čerstvá	čerstvý	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
jsou	být	k5eAaImIp3nP
potažena	potáhnout	k5eAaPmNgNnP
solemi	sůl	k1gFnPc7
draslíku	draslík	k1gInSc2
a	a	k8xC
sodíku	sodík	k1gInSc2
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
využít	využít	k5eAaPmF
k	k	k7c3
rozpoznání	rozpoznání	k1gNnSc3
stáří	stář	k1gFnPc2
vajec	vejce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
U	u	k7c2
běžných	běžný	k2eAgNnPc2d1
plemen	plemeno	k1gNnPc2
odpovídá	odpovídat	k5eAaImIp3nS
barva	barva	k1gFnSc1
skořápky	skořápka	k1gFnSc2
barvě	barva	k1gFnSc6
ušnic	ušnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slepice	slepice	k1gFnSc1
s	s	k7c7
bílými	bílý	k2eAgFnPc7d1
ušnicemi	ušnice	k1gFnPc7
snášejí	snášet	k5eAaImIp3nP
vejce	vejce	k1gNnPc1
bílá	bílý	k2eAgNnPc1d1
<g/>
,	,	kIx,
slepice	slepice	k1gFnPc1
s	s	k7c7
červenými	červený	k2eAgFnPc7d1
ušnicemi	ušnice	k1gFnPc7
hnědá	hnědat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
Přes	přes	k7c4
tyto	tento	k3xDgFnPc4
vnější	vnější	k2eAgFnPc4d1
odlišnosti	odlišnost	k1gFnPc4
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
vnitřní	vnitřní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
všech	všecek	k3xTgNnPc2
ptačích	ptačí	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
víceméně	víceméně	k9
stejná	stejná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odlišnosti	odlišnost	k1gFnPc1
jsou	být	k5eAaImIp3nP
především	především	k9
ve	v	k7c6
velikosti	velikost	k1gFnSc6
žloutku	žloutek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
u	u	k7c2
nekrmivých	krmivý	k2eNgMnPc2d1
ptáků	pták	k1gMnPc2
asi	asi	k9
dvakrát	dvakrát	k6eAd1
objemnější	objemný	k2eAgMnSc1d2
než	než	k8xS
u	u	k7c2
krmivých	krmivý	k2eAgInPc2d1
–	–	k?
protože	protože	k8xS
čerstvě	čerstvě	k6eAd1
vylíhlé	vylíhlý	k2eAgNnSc1d1
ptáče	ptáče	k1gNnSc1
musí	muset	k5eAaImIp3nS
přežít	přežít	k5eAaPmF
než	než	k8xS
si	se	k3xPyFc3
samo	sám	k3xTgNnSc1
dokáže	dokázat	k5eAaPmIp3nS
obstarat	obstarat	k5eAaPmF
potravu	potrava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Chemické	chemický	k2eAgNnSc1d1
složení	složení	k1gNnSc1
vejce	vejce	k1gNnSc2
</s>
<s>
Uváděné	uváděný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
se	se	k3xPyFc4
týkají	týkat	k5eAaImIp3nP
převážně	převážně	k6eAd1
slepičích	slepičí	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
o	o	k7c6
kterých	který	k3yRgFnPc6,k3yQgFnPc6,k3yIgFnPc6
existuje	existovat	k5eAaImIp3nS
nejvíce	nejvíce	k6eAd1,k6eAd3
poznatků	poznatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slepičí	slepičit	k5eAaImIp3nS
vejce	vejce	k1gNnSc2
obsahují	obsahovat	k5eAaImIp3nP
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
jejím	její	k3xOp3gInSc7
největším	veliký	k2eAgInSc7d3
zdrojem	zdroj	k1gInSc7
je	být	k5eAaImIp3nS
bílek	bílek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
složení	složení	k1gNnSc6
sušiny	sušina	k1gFnSc2
se	se	k3xPyFc4
podílejí	podílet	k5eAaImIp3nP
především	především	k9
bílkoviny	bílkovina	k1gFnPc1
a	a	k8xC
tuk	tuk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílkoviny	bílkovina	k1gFnPc1
jsou	být	k5eAaImIp3nP
obsaženy	obsáhnout	k5eAaPmNgFnP
hlavně	hlavně	k9
v	v	k7c6
bílku	bílek	k1gInSc6
a	a	k8xC
žloutku	žloutek	k1gInSc6
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
tuky	tuk	k1gInPc1
jsou	být	k5eAaImIp3nP
soustředěny	soustředit	k5eAaPmNgInP
jenom	jenom	k9
ve	v	k7c6
žloutku	žloutek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc4
cukrů	cukr	k1gInPc2
ve	v	k7c6
vejci	vejce	k1gNnSc6
je	být	k5eAaImIp3nS
minimální	minimální	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minerální	minerální	k2eAgFnPc4d1
látky	látka	k1gFnPc4
jsou	být	k5eAaImIp3nP
obsaženy	obsáhnout	k5eAaPmNgFnP
především	především	k9
ve	v	k7c6
skořápce	skořápka	k1gFnSc6
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
ve	v	k7c6
vaječné	vaječný	k2eAgFnSc6d1
hmotě	hmota	k1gFnSc6
je	být	k5eAaImIp3nS
jich	on	k3xPp3gMnPc2
velmi	velmi	k6eAd1
málo	málo	k4c1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíly	rozdíl	k1gInPc4
v	v	k7c6
chemickém	chemický	k2eAgNnSc6d1
složení	složení	k1gNnSc6
vajec	vejce	k1gNnPc2
jednotlivých	jednotlivý	k2eAgInPc2d1
druhů	druh	k1gInPc2
drůbeže	drůbež	k1gFnSc2
nejsou	být	k5eNaImIp3nP
velké	velký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složení	složení	k1gNnSc1
vejce	vejce	k1gNnSc4
je	být	k5eAaImIp3nS
však	však	k9
ovlivňováno	ovlivňován	k2eAgNnSc1d1
plemenem	plemeno	k1gNnSc7
<g/>
,	,	kIx,
věkem	věk	k1gInSc7
drůbeže	drůbež	k1gFnSc2
<g/>
,	,	kIx,
úrovní	úroveň	k1gFnSc7
a	a	k8xC
intenzitou	intenzita	k1gFnSc7
snášky	snáška	k1gFnSc2
<g/>
,	,	kIx,
ročním	roční	k2eAgNnSc7d1
obdobím	období	k1gNnSc7
<g/>
,	,	kIx,
podmínkami	podmínka	k1gFnPc7
vnějšího	vnější	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
kvalitou	kvalita	k1gFnSc7
a	a	k8xC
kvantitou	kvantita	k1gFnSc7
krmení	krmení	k1gNnSc2
a	a	k8xC
řadou	řada	k1gFnSc7
dalších	další	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
zdravotního	zdravotní	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Slepičí	slepičí	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
obsahují	obsahovat	k5eAaImIp3nP
mnoho	mnoho	k6eAd1
cholesterolu	cholesterol	k1gInSc2
(	(	kIx(
<g/>
424	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
vajec	vejce	k1gNnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgNnPc2
studií	studio	k1gNnPc2
však	však	k9
tolik	tolik	k6eAd1
nepřispívají	přispívat	k5eNaImIp3nP
ke	k	k7c3
zvyšování	zvyšování	k1gNnSc3
krevního	krevní	k2eAgInSc2d1
cholesterolu	cholesterol	k1gInSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
navíc	navíc	k6eAd1
obsahují	obsahovat	k5eAaImIp3nP
lecitin	lecitin	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
množství	množství	k1gNnSc3
cholesterolu	cholesterol	k1gInSc2
v	v	k7c6
krvi	krev	k1gFnSc6
snižuje	snižovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tabulka	tabulka	k1gFnSc1
udává	udávat	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
živin	živina	k1gFnPc2
<g/>
,	,	kIx,
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
vitamínů	vitamín	k1gInPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
nutričních	nutriční	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
zjištěných	zjištěný	k2eAgInPc2d1
ve	v	k7c6
slepičích	slepičí	k2eAgNnPc6d1
vejcích	vejce	k1gNnPc6
(	(	kIx(
<g/>
průměr	průměr	k1gInSc1
na	na	k7c4
celé	celý	k2eAgNnSc4d1
vejce	vejce	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Složka	složka	k1gFnSc1
</s>
<s>
Jednotka	jednotka	k1gFnSc1
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
</s>
<s>
Prvek	prvek	k1gInSc1
(	(	kIx(
<g/>
mg	mg	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
)	)	kIx)
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
</s>
<s>
Složka	složka	k1gFnSc1
(	(	kIx(
<g/>
mg	mg	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
g	g	kA
<g/>
)	)	kIx)
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
</s>
<s>
vodag	vodag	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
75,1	75,1	k4
<g/>
Na	na	k7c4
<g/>
140	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
C0	C0	k1gFnSc2
</s>
<s>
bílkovinyg	bílkovinyg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
12,5	12,5	k4
<g/>
K	k	k7c3
<g/>
130	#num#	k4
<g/>
vitamin	vitamin	k1gInSc1
D	D	kA
<g/>
0,0018	0,0018	k4
</s>
<s>
tukyg	tukyg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
11,2	11,2	k4
<g/>
Ca	ca	kA
<g/>
57	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
E	E	kA
<g/>
1,91	1,91	k4
</s>
<s>
cukryg	cukryg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
gstopyMg	gstopyMg	k1gInSc1
<g/>
12	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
B	B	kA
<g/>
60,12	60,12	k4
</s>
<s>
celkový	celkový	k2eAgInSc1d1
dusíkg	dusíkg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
2,01	2,01	k4
<g/>
P	P	kA
<g/>
200	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
B	B	kA
<g/>
120,002	120,002	k4
<g/>
5	#num#	k4
</s>
<s>
vlákninag	vlákninag	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
0	#num#	k4
<g/>
Fe	Fe	k1gFnSc2
<g/>
1,9	1,9	k4
<g/>
karotenystopy	karotenystopa	k1gFnSc2
</s>
<s>
mastné	mastné	k1gNnSc1
kyselinyg	kyselinyga	k1gFnPc2
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
9,3	9,3	k4
<g/>
Cu	Cu	k1gFnSc2
<g/>
0,08	0,08	k4
<g/>
thiamin	thiamin	k1gInSc1
<g/>
0,09	0,09	k4
</s>
<s>
cholesterolmg	cholesterolmg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
391	#num#	k4
<g/>
Zn	zn	kA
<g/>
1,3	1,3	k4
<g/>
riboflavin	riboflavin	k1gInSc1
<g/>
0,47	0,47	k4
</s>
<s>
Semg	Semg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
0,011	0,011	k4
<g/>
I	i	k9
<g/>
0,053	0,053	k4
<g/>
niacin	niacina	k1gFnPc2
<g/>
0,1	0,1	k4
</s>
<s>
energiekJ	energiekJ	k?
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
627	#num#	k4
<g/>
MnstopyCl	MnstopyCl	k1gInSc1
<g/>
160	#num#	k4
</s>
<s>
Organoleptické	organoleptický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
vajec	vejce	k1gNnPc2
</s>
<s>
Čerstvě	čerstvě	k6eAd1
snesené	snesený	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
specifickou	specifický	k2eAgFnSc4d1
vůni	vůně	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
snesení	snesení	k1gNnSc6
může	moct	k5eAaImIp3nS
získat	získat	k5eAaPmF
vlivem	vliv	k1gInSc7
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
je	být	k5eAaImIp3nS
skladováno	skladován	k2eAgNnSc1d1
<g/>
,	,	kIx,
i	i	k8xC
nežádoucí	žádoucí	k2eNgInPc4d1
pachy	pach	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejce	vejce	k1gNnPc1
proto	proto	k8xC
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
skladována	skladován	k2eAgFnSc1d1
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
produkty	produkt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chuť	chuť	k1gFnSc1
vejce	vejce	k1gNnSc2
určuje	určovat	k5eAaImIp3nS
více	hodně	k6eAd2
žloutek	žloutek	k1gInSc1
než	než	k8xS
bílek	bílek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chuť	chuť	k1gFnSc1
je	být	k5eAaImIp3nS
ovlivněna	ovlivnit	k5eAaPmNgFnS
především	především	k6eAd1
krmivem	krmivo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
pozorovaná	pozorovaný	k2eAgFnSc1d1
rybí	rybí	k2eAgFnSc1d1
chuť	chuť	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
způsobena	způsobit	k5eAaPmNgFnS
zkrmováním	zkrmování	k1gNnSc7
většího	veliký	k2eAgInSc2d2
podílu	podíl	k1gInSc2
rybí	rybí	k2eAgFnSc2d1
moučky	moučka	k1gFnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
tuku	tuk	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
při	při	k7c6
nezkrmování	nezkrmování	k1gNnSc6
rybí	rybí	k2eAgFnSc2d1
moučky	moučka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
možné	možný	k2eAgFnPc4d1
příčiny	příčina	k1gFnPc4
této	tento	k3xDgFnSc2
rybí	rybí	k2eAgFnSc2d1
chuti	chuť	k1gFnSc2
se	se	k3xPyFc4
uvádějí	uvádět	k5eAaImIp3nP
produkty	produkt	k1gInPc1
metabolismu	metabolismus	k1gInSc2
bakterií	bakterie	k1gFnPc2
(	(	kIx(
<g/>
E.	E.	kA
coli	col	k1gFnSc2
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
krmivo	krmivo	k1gNnSc4
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
mastných	mastný	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
,	,	kIx,
betainu	betain	k1gInSc2
<g/>
,	,	kIx,
porucha	porucha	k1gFnSc1
organismu	organismus	k1gInSc2
nosnice	nosnice	k1gFnSc2
metabolizovat	metabolizovat	k5eAaImF
trimetylamin	trimetylamin	k1gInSc1
či	či	k8xC
zkrmování	zkrmování	k1gNnSc1
většího	veliký	k2eAgNnSc2d2
množství	množství	k1gNnSc2
řepkové	řepkový	k2eAgFnSc2d1
moučky	moučka	k1gFnSc2
(	(	kIx(
<g/>
>	>	kIx)
<g/>
10	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
apod.	apod.	kA
Chuť	chuť	k1gFnSc1
připomínající	připomínající	k2eAgNnSc4d1
seno	seno	k1gNnSc4
se	se	k3xPyFc4
někdy	někdy	k6eAd1
zjišťuje	zjišťovat	k5eAaImIp3nS
u	u	k7c2
chladírenských	chladírenský	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
<g/>
;	;	kIx,
bývá	bývat	k5eAaImIp3nS
způsobena	způsobit	k5eAaPmNgFnS
také	také	k9
metabolickými	metabolický	k2eAgInPc7d1
produkty	produkt	k1gInPc7
bakterií	bakterie	k1gFnPc2
<g/>
,	,	kIx,
schopnými	schopný	k2eAgFnPc7d1
metabolismu	metabolismus	k1gInSc3
při	při	k7c6
teplotě	teplota	k1gFnSc6
0	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
Pseudomonas	Pseudomonas	k1gMnSc1
spp	spp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Abnormality	abnormalita	k1gFnPc1
vajec	vejce	k1gNnPc2
</s>
<s>
Při	při	k7c6
tvorbě	tvorba	k1gFnSc6
vejce	vejce	k1gNnSc2
v	v	k7c6
pohlavních	pohlavní	k2eAgInPc6d1
orgánech	orgán	k1gInPc6
nosnice	nosnice	k1gFnPc1
vznikají	vznikat	k5eAaImIp3nP
někdy	někdy	k6eAd1
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
normálním	normální	k2eAgInSc7d1
průběhem	průběh	k1gInSc7
různé	různý	k2eAgFnPc1d1
tvarové	tvarový	k2eAgFnPc1d1
i	i	k8xC
funkční	funkční	k2eAgFnPc1d1
změny	změna	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
projevují	projevovat	k5eAaImIp3nP
i	i	k9
na	na	k7c6
kvalitě	kvalita	k1gFnSc6
konzumních	konzumní	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitřní	vnitřní	k2eAgFnSc1d1
kvalita	kvalita	k1gFnSc1
vajec	vejce	k1gNnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
narušena	narušit	k5eAaPmNgFnS
i	i	k9
při	při	k7c6
nezměněné	změněný	k2eNgFnSc6d1
snášce	snáška	k1gFnSc6
Tyto	tento	k3xDgFnPc1
změny	změna	k1gFnPc1
vznikají	vznikat	k5eAaImIp3nP
nejčastěji	často	k6eAd3
v	v	k7c6
důsledku	důsledek	k1gInSc6
poruch	poruch	k1gInSc1
funkce	funkce	k1gFnSc2
vejcovodu	vejcovod	k1gInSc2
z	z	k7c2
různých	různý	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
–	–	k?
endogenních	endogenní	k2eAgInPc2d1
(	(	kIx(
<g/>
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
nosnic	nosnice	k1gFnPc2
<g/>
)	)	kIx)
i	i	k8xC
exogenních	exogenní	k2eAgInPc2d1
(	(	kIx(
<g/>
bakterie	bakterie	k1gFnPc1
<g/>
,	,	kIx,
plísně	plíseň	k1gFnPc1
<g/>
,	,	kIx,
krmivo	krmivo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abnormální	abnormální	k2eAgNnSc1d1
vejce	vejce	k1gNnPc1
jsou	být	k5eAaImIp3nP
nepoživatelná	poživatelný	k2eNgNnPc1d1
<g/>
,	,	kIx,
používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
jen	jen	k9
jako	jako	k8xC,k8xS
surovina	surovina	k1gFnSc1
k	k	k7c3
technickému	technický	k2eAgNnSc3d1
zpracování	zpracování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgNnPc1d1
jednožloutková	jednožloutkový	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
snášejí	snášet	k5eAaImIp3nP
nosnice	nosnice	k1gFnPc4
občas	občas	k6eAd1
<g/>
;	;	kIx,
vznikají	vznikat	k5eAaImIp3nP
při	při	k7c6
dočasné	dočasný	k2eAgFnSc6d1
poruše	porucha	k1gFnSc6
funkce	funkce	k1gFnSc2
vejcovodu	vejcovod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgNnPc1
vejce	vejce	k1gNnPc4
obsahují	obsahovat	k5eAaImIp3nP
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc4
bílkovin	bílkovina	k1gFnPc2
(	(	kIx(
<g/>
procházejí	procházet	k5eAaImIp3nP
vejcovodem	vejcovod	k1gInSc7
pomalu	pomalu	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
ohrožují	ohrožovat	k5eAaImIp3nP
při	při	k7c6
snášení	snášení	k1gNnSc6
pohlavní	pohlavní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
nosnice	nosnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Trpasličí	trpasličí	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
(	(	kIx(
<g/>
též	též	k9
pohádková	pohádkový	k2eAgNnPc4d1
nebo	nebo	k8xC
kohoutí	kohoutí	k2eAgNnPc4d1
vejce	vejce	k1gNnPc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
představují	představovat	k5eAaImIp3nP
abnormálně	abnormálně	k6eAd1
malá	malý	k2eAgNnPc4d1
vejce	vejce	k1gNnPc4
(	(	kIx(
<g/>
10	#num#	k4
g	g	kA
i	i	k8xC
méně	málo	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikají	vznikat	k5eAaImIp3nP
často	často	k6eAd1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
žloutek	žloutek	k1gInSc1
se	se	k3xPyFc4
po	po	k7c6
ovulaci	ovulace	k1gFnSc6
nedostane	dostat	k5eNaPmIp3nS
do	do	k7c2
vejcovodu	vejcovod	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
do	do	k7c2
vejcovodu	vejcovod	k1gInSc2
pronikne	proniknout	k5eAaPmIp3nS
jen	jen	k9
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
žloutku	žloutek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hormonální	hormonální	k2eAgInSc1d1
impuls	impuls	k1gInSc1
však	však	k9
působí	působit	k5eAaImIp3nS
na	na	k7c4
žlázy	žláza	k1gFnPc4
vejcovodu	vejcovod	k1gInSc2
a	a	k8xC
vylučovaný	vylučovaný	k2eAgInSc1d1
bílek	bílek	k1gInSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
obalen	obalit	k5eAaPmNgInS
papírovými	papírový	k2eAgFnPc7d1
blanami	blána	k1gFnPc7
a	a	k8xC
skořápkou	skořápka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
vejce	vejce	k1gNnPc1
většinou	většinou	k6eAd1
nemají	mít	k5eNaImIp3nP
žloutek	žloutek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutno	nutno	k6eAd1
odlišit	odlišit	k5eAaPmF
od	od	k7c2
tzv.	tzv.	kA
kuřecích	kuřecí	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
jsou	být	k5eAaImIp3nP
vejce	vejce	k1gNnPc1
produkovaná	produkovaný	k2eAgNnPc1d1
někdy	někdy	k6eAd1
nosnicemi	nosnice	k1gFnPc7
na	na	k7c6
počátku	počátek	k1gInSc6
snášky	snáška	k1gFnSc2
(	(	kIx(
<g/>
hmotnost	hmotnost	k1gFnSc1
35	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
g	g	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dvoužloutková	dvoužloutkový	k2eAgNnPc1d1
nebo	nebo	k8xC
třížloutková	třížloutkový	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
vznikají	vznikat	k5eAaImIp3nP
při	při	k7c6
krátkých	krátký	k2eAgInPc6d1
časových	časový	k2eAgInPc6d1
úsecích	úsek	k1gInPc6
mezi	mezi	k7c7
ovulacemi	ovulace	k1gFnPc7
žloutků	žloutek	k1gInPc2
nebo	nebo	k8xC
při	při	k7c6
současné	současný	k2eAgFnSc6d1
ovulaci	ovulace	k1gFnSc6
2-3	2-3	k4
zralých	zralý	k2eAgInPc2d1
folikulů	folikul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejcovodem	vejcovod	k1gInSc7
potom	potom	k6eAd1
prostupuje	prostupovat	k5eAaImIp3nS
najednou	najednou	k6eAd1
více	hodně	k6eAd2
žloutků	žloutek	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
mohou	moct	k5eAaImIp3nP
obdržet	obdržet	k5eAaPmF
jeden	jeden	k4xCgInSc4
bílkový	bílkový	k2eAgInSc4d1
obal	obal	k1gInSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
má	mít	k5eAaImIp3nS
každý	každý	k3xTgInSc1
žloutek	žloutek	k1gInSc1
vlastní	vlastnit	k5eAaImIp3nS
bílkový	bílkový	k2eAgInSc4d1
obal	obal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
interval	interval	k1gInSc4
mezi	mezi	k7c7
ovulacemi	ovulace	k1gFnPc7
dvou	dva	k4xCgInPc2
žloutků	žloutek	k1gInPc2
dostatečný	dostatečný	k2eAgMnSc1d1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
nosnice	nosnice	k1gFnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
24	#num#	k4
hodin	hodina	k1gFnPc2
snést	snést	k5eAaPmF
dvě	dva	k4xCgNnPc4
normální	normální	k2eAgNnPc4d1
vejce	vejce	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Vejce	vejce	k1gNnSc1
ve	v	k7c6
vejci	vejce	k1gNnSc6
vzniká	vznikat	k5eAaImIp3nS
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
již	již	k9
téměř	téměř	k6eAd1
hotové	hotový	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
je	být	k5eAaImIp3nS
vrženo	vrhnout	k5eAaPmNgNnS
opačnou	opačný	k2eAgFnSc7d1
peristaltikou	peristaltika	k1gFnSc7
vejcovodu	vejcovod	k1gInSc2
zpět	zpět	k6eAd1
až	až	k9
k	k	k7c3
nálevce	nálevka	k1gFnSc3
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
opětovně	opětovně	k6eAd1
obaluje	obalovat	k5eAaImIp3nS
vrstvou	vrstva	k1gFnSc7
bílku	bílek	k1gInSc2
a	a	k8xC
skořápky	skořápka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vejce	vejce	k1gNnSc4
s	s	k7c7
cizími	cizí	k2eAgNnPc7d1
tělísky	tělísko	k1gNnPc7
vznikají	vznikat	k5eAaImIp3nP
v	v	k7c6
důsledku	důsledek	k1gInSc6
vniknutí	vniknutí	k1gNnSc2
cizích	cizí	k2eAgNnPc2d1
tělísek	tělísko	k1gNnPc2
(	(	kIx(
<g/>
kamínek	kamínka	k1gNnPc2
<g/>
,	,	kIx,
zrnko	zrnko	k1gNnSc1
písku	písek	k1gInSc2
aj.	aj.	kA
<g/>
)	)	kIx)
do	do	k7c2
vejcovodu	vejcovod	k1gInSc2
kloakou	kloaka	k1gFnSc7
v	v	k7c6
průběhu	průběh	k1gInSc6
tvorby	tvorba	k1gFnSc2
vejce	vejce	k1gNnSc4
(	(	kIx(
<g/>
především	především	k9
při	při	k7c6
páření	páření	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
cizí	cizí	k2eAgNnPc1d1
tělíska	tělísko	k1gNnPc1
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
bílku	bílek	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
mohou	moct	k5eAaImIp3nP
nahrazovat	nahrazovat	k5eAaImF
i	i	k9
žloutek	žloutek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tuhé	tuhý	k2eAgInPc1d1
žloutky	žloutek	k1gInPc1
byly	být	k5eAaImAgInP
ojediněle	ojediněle	k6eAd1
pozorovány	pozorovat	k5eAaImNgInP
v	v	k7c6
určitých	určitý	k2eAgFnPc6d1
lokalitách	lokalita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žloutky	žloutek	k1gInPc7
měly	mít	k5eAaImAgInP
konzistenci	konzistence	k1gFnSc3
žloutku	žloutek	k1gInSc2
vejce	vejce	k1gNnSc2
na	na	k7c4
tvrdo	tvrdo	k1gNnSc4
uvařeného	uvařený	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčina	příčina	k1gFnSc1
nebyla	být	k5eNaImAgFnS
zjištěna	zjistit	k5eAaPmNgFnS
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
poruchu	porucha	k1gFnSc4
resorbce	resorbec	k1gInSc2
a	a	k8xC
využití	využití	k1gNnSc2
vitamínů	vitamín	k1gInPc2
skupiny	skupina	k1gFnSc2
B.	B.	kA
</s>
<s>
Krevní	krevní	k2eAgInSc1d1
kroužek	kroužek	k1gInSc1
viditelný	viditelný	k2eAgInSc1d1
při	při	k7c6
prosvěcování	prosvěcování	k1gNnSc6
vajec	vejce	k1gNnPc2
vzniká	vznikat	k5eAaImIp3nS
jako	jako	k8xS,k8xC
následek	následek	k1gInSc4
odumření	odumření	k1gNnSc2
zárodku	zárodek	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
oplozené	oplozený	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
bylo	být	k5eAaImAgNnS
inkubováno	inkubovat	k5eAaImNgNnS
po	po	k7c4
dobu	doba	k1gFnSc4
1-2	1-2	k4
dnů	den	k1gInPc2
nebo	nebo	k8xC
skladováno	skladovat	k5eAaImNgNnS
v	v	k7c6
teplotě	teplota	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
počáteční	počáteční	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
zárodečného	zárodečný	k2eAgInSc2d1
terčíku	terčík	k1gInSc2
umožnila	umožnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Krevní	krevní	k2eAgFnPc1d1
a	a	k8xC
masové	masový	k2eAgFnPc1d1
skvrny	skvrna	k1gFnPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
ve	v	k7c6
žloutku	žloutek	k1gInSc6
i	i	k8xC
bílku	bílek	k1gInSc6
a	a	k8xC
znehodnocují	znehodnocovat	k5eAaImIp3nP
vejce	vejce	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výskyt	výskyt	k1gInSc1
krevních	krevní	k2eAgFnPc2d1
skvrn	skvrna	k1gFnPc2
je	být	k5eAaImIp3nS
závislý	závislý	k2eAgInSc1d1
na	na	k7c6
pevnosti	pevnost	k1gFnSc6
stěn	stěn	k1gInSc1
kapilár	kapilára	k1gFnPc2
vaječníku	vaječník	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
protrombinovém	protrombinový	k2eAgInSc6d1
čase	čas	k1gInSc6
a	a	k8xC
krevním	krevní	k2eAgInSc6d1
tlaku	tlak	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krevní	krevní	k2eAgFnPc4d1
skvrny	skvrna	k1gFnPc4
na	na	k7c6
žloutku	žloutek	k1gInSc6
jsou	být	k5eAaImIp3nP
krevní	krevní	k2eAgFnPc1d1
sraženiny	sraženina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
krvácením	krvácení	k1gNnPc3
při	při	k7c6
ovulaci	ovulace	k1gFnSc6
(	(	kIx(
<g/>
folikul	folikul	k1gInSc1
praskne	prasknout	k5eAaPmIp3nS
na	na	k7c6
jiném	jiný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
než	než	k8xS
je	být	k5eAaImIp3nS
folikulární	folikulární	k2eAgNnSc1d1
stigma	stigma	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejce	vejce	k1gNnSc1
s	s	k7c7
krevní	krevní	k2eAgFnSc7d1
stopou	stopa	k1gFnSc7
na	na	k7c6
bílku	bílek	k1gInSc6
vznikají	vznikat	k5eAaImIp3nP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
krev	krev	k1gFnSc1
z	z	k7c2
prasklé	prasklý	k2eAgFnSc2d1
cévy	céva	k1gFnSc2
vejcovodu	vejcovod	k1gInSc2
obalí	obalit	k5eAaPmIp3nP
bílkem	bílek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšší	vysoký	k2eAgInSc1d2
výskyt	výskyt	k1gInSc1
skvrn	skvrna	k1gFnPc2
se	se	k3xPyFc4
pozoruje	pozorovat	k5eAaImIp3nS
u	u	k7c2
slepic	slepice	k1gFnPc2
těžšího	těžký	k2eAgInSc2d2
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nižší	nízký	k2eAgInSc1d2
obsah	obsah	k1gInSc1
vitamínu	vitamín	k1gInSc2
A	a	k9
v	v	k7c6
krmné	krmný	k2eAgFnSc6d1
směsi	směs	k1gFnSc6
(	(	kIx(
<g/>
pod	pod	k7c4
2400-3000	2400-3000	k4
mj.	mj.	kA
/	/	kIx~
<g/>
kg	kg	kA
<g/>
)	)	kIx)
zvyšuje	zvyšovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gInSc4
výskyt	výskyt	k1gInSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
vyšší	vysoký	k2eAgInSc1d2
obsah	obsah	k1gInSc1
vitamínu	vitamín	k1gInSc2
K.	K.	kA
Proto	proto	k8xC
zařazení	zařazení	k1gNnSc4
krmiv	krmivo	k1gNnPc2
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
obsahem	obsah	k1gInSc7
vitamínu	vitamín	k1gInSc2
K	K	kA
(	(	kIx(
<g/>
vojtěšková	vojtěškový	k2eAgFnSc1d1
moučka	moučka	k1gFnSc1
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
zvýšit	zvýšit	k5eAaPmF
počet	počet	k1gInSc4
vajec	vejce	k1gNnPc2
s	s	k7c7
masovými	masový	k2eAgFnPc7d1
skvrnami	skvrna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snášení	snášení	k1gNnSc1
vajec	vejce	k1gNnPc2
s	s	k7c7
krevními	krevní	k2eAgFnPc7d1
skvrnami	skvrna	k1gFnPc7
je	být	k5eAaImIp3nS
u	u	k7c2
některých	některý	k3yIgFnPc2
nosnic	nosnice	k1gFnPc2
dědičné	dědičný	k2eAgFnPc1d1
(	(	kIx(
<g/>
h	h	k?
<g/>
2	#num#	k4
=	=	kIx~
0,3	0,3	k4
<g/>
-	-	kIx~
<g/>
0,6	0,6	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masové	masový	k2eAgFnPc4d1
skvrny	skvrna	k1gFnPc4
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
krevní	krevní	k2eAgFnPc4d1
skvrny	skvrna	k1gFnPc4
pozměněné	pozměněný	k2eAgFnPc4d1
během	během	k7c2
průchodu	průchod	k1gInSc2
vejce	vejce	k1gNnSc1
vejcovodem	vejcovod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krevní	krevní	k2eAgFnPc4d1
skvrny	skvrna	k1gFnPc4
vznikají	vznikat	k5eAaImIp3nP
pravděpodobně	pravděpodobně	k6eAd1
ve	v	k7c6
vaječníku	vaječník	k1gInSc6
před	před	k7c7
nebo	nebo	k8xC
po	po	k7c6
ovulaci	ovulace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masové	masový	k2eAgFnPc4d1
skvrny	skvrna	k1gFnPc4
mají	mít	k5eAaImIp3nP
buď	buď	k8xC
hnědou	hnědý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
z	z	k7c2
krevních	krevní	k2eAgFnPc2d1
skvrn	skvrna	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgNnPc6,k3yQgNnPc6,k3yIgNnPc6
se	se	k3xPyFc4
hemoglobin	hemoglobin	k1gInSc1
oxidací	oxidace	k1gFnPc2
změnil	změnit	k5eAaPmAgInS
na	na	k7c4
hematin	hematin	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
(	(	kIx(
<g/>
vznikají	vznikat	k5eAaImIp3nP
i	i	k9
ve	v	k7c6
vejcovodu	vejcovod	k1gInSc6
uvolněním	uvolnění	k1gNnSc7
řasy	řasa	k1gFnSc2
sliznice	sliznice	k1gFnSc2
vejcovodu	vejcovod	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vyšších	vysoký	k2eAgFnPc6d2
teplotách	teplota	k1gFnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
krevní	krevní	k2eAgInSc1d1
tlak	tlak	k1gInSc1
je	být	k5eAaImIp3nS
nižší	nízký	k2eAgMnSc1d2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nižší	nízký	k2eAgMnSc1d2
i	i	k8xC
výskyt	výskyt	k1gInSc1
krevních	krevní	k2eAgFnPc2d1
skvrn	skvrna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Abnormality	abnormalita	k1gFnPc1
skořápky	skořápka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
povrchu	povrch	k1gInSc6
vejce	vejce	k1gNnSc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
vyskytovat	vyskytovat	k5eAaImF
usazené	usazený	k2eAgInPc1d1
shluky	shluk	k1gInPc1
skořápkové	skořápkový	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
nebo	nebo	k8xC
hrbolky	hrbolek	k1gInPc4
<g/>
;	;	kIx,
část	část	k1gFnSc1
povrchu	povrch	k1gInSc2
nebo	nebo	k8xC
celá	celý	k2eAgFnSc1d1
skořápka	skořápka	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rýhovaná	rýhovaný	k2eAgFnSc1d1
nebo	nebo	k8xC
pokryta	pokryt	k2eAgFnSc1d1
vruby	vrub	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abnormality	abnormalita	k1gFnPc4
skořápky	skořápka	k1gFnSc2
vznikají	vznikat	k5eAaImIp3nP
nahromaděním	nahromadění	k1gNnSc7
hmoty	hmota	k1gFnSc2
vaječných	vaječný	k2eAgFnPc2d1
blan	blána	k1gFnPc2
<g/>
,	,	kIx,
matrix	matrix	k1gInSc1
nebo	nebo	k8xC
kutikuly	kutikula	k1gFnPc1
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
skořápky	skořápka	k1gFnSc2
v	v	k7c6
děloze	děloha	k1gFnSc6
nebo	nebo	k8xC
zvýšenou	zvýšený	k2eAgFnSc7d1
sekrecí	sekrece	k1gFnSc7
vápenné	vápenný	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
v	v	k7c6
děloze	děloha	k1gFnSc6
vejcovodu	vejcovod	k1gInSc2
nebo	nebo	k8xC
zpomalením	zpomalení	k1gNnSc7
postupu	postup	k1gInSc2
vejce	vejce	k1gNnSc1
vejcovodem	vejcovod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vroubkované	vroubkovaný	k2eAgInPc1d1
nebo	nebo	k8xC
rýhované	rýhovaný	k2eAgFnPc1d1
skořápky	skořápka	k1gFnPc1
jsou	být	k5eAaImIp3nP
náchylnější	náchylný	k2eAgFnPc1d2
k	k	k7c3
prasknutí	prasknutí	k1gNnSc3
a	a	k8xC
způsobují	způsobovat	k5eAaImIp3nP
podstatně	podstatně	k6eAd1
rychlejší	rychlý	k2eAgNnSc4d2
vysychání	vysychání	k1gNnSc4
vajec	vejce	k1gNnPc2
při	při	k7c6
skladování	skladování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc4d1
deformace	deformace	k1gFnPc4
skořápky	skořápka	k1gFnSc2
mohou	moct	k5eAaImIp3nP
vznikat	vznikat	k5eAaImF
při	pře	k1gFnSc4
různých	různý	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
drůbeže	drůbež	k1gFnSc2
(	(	kIx(
<g/>
syndrom	syndrom	k1gInSc1
poklesu	pokles	k1gInSc2
snášky	snáška	k1gFnSc2
<g/>
,	,	kIx,
infekční	infekční	k2eAgFnSc1d1
bronchitida	bronchitida	k1gFnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trhliny	trhlina	k1gFnSc2
ve	v	k7c6
skořápce	skořápka	k1gFnSc6
jsou	být	k5eAaImIp3nP
způsobovány	způsobovat	k5eAaImNgFnP
nedostatečně	dostatečně	k6eNd1
silnou	silný	k2eAgFnSc7d1
skořápkou	skořápka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
neodolá	odolat	k5eNaPmIp3nS
mechanickým	mechanický	k2eAgInSc7d1
vlivům	vliv	k1gInPc3
působících	působící	k2eAgMnPc2d1
na	na	k7c4
ni	on	k3xPp3gFnSc4
během	během	k7c2
sběru	sběr	k1gInSc2
vajec	vejce	k1gNnPc2
anebo	anebo	k8xC
jejich	jejich	k3xOp3gFnSc2
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
prosvěcování	prosvěcování	k1gNnSc6
vajec	vejce	k1gNnPc2
se	se	k3xPyFc4
pozoruje	pozorovat	k5eAaImIp3nS
zeslabení	zeslabení	k1gNnSc1
skořápky	skořápka	k1gFnSc2
ve	v	k7c6
formě	forma	k1gFnSc6
trhlin	trhlina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
však	však	k9
neporušují	porušovat	k5eNaImIp3nP
celistvost	celistvost	k1gFnSc4
skořápky	skořápka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Barevné	barevný	k2eAgFnPc1d1
abnormality	abnormalita	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olivové	Olivové	k2eAgInSc4d1
až	až	k8xS
hnědé	hnědý	k2eAgNnSc4d1
zbarvení	zbarvení	k1gNnSc4
žloutku	žloutek	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
skvrnitost	skvrnitost	k1gFnSc4
jsou	být	k5eAaImIp3nP
způsobovány	způsobovat	k5eAaImNgFnP
zkrmováním	zkrmování	k1gNnSc7
bavlníkového	bavlníkový	k2eAgInSc2d1
šrotu	šrot	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
obsahuje	obsahovat	k5eAaImIp3nS
gossypol	gossypol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přídavkem	přídavek	k1gInSc7
0,5	0,5	k4
%	%	kIx~
Fe	Fe	k1gFnSc1
<g/>
2	#num#	k4
<g/>
SO	So	kA
<g/>
4	#num#	k4
krmné	krmný	k2eAgFnSc2d1
směsi	směs	k1gFnSc2
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
nepříznivý	příznivý	k2eNgInSc1d1
účinek	účinek	k1gInSc1
gossypolu	gossypol	k1gInSc2
ruší	rušit	k5eAaImIp3nS
(	(	kIx(
<g/>
vznikají	vznikat	k5eAaImIp3nP
komplexy	komplex	k1gInPc4
iontu	ion	k1gInSc6
železa	železo	k1gNnSc2
s	s	k7c7
gossypolem	gossypol	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skvrnitost	skvrnitost	k1gFnSc1
žloutku	žloutek	k1gInSc2
se	se	k3xPyFc4
někdy	někdy	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
i	i	k9
u	u	k7c2
nosnic	nosnice	k1gFnPc2
na	na	k7c6
počátku	počátek	k1gInSc6
snášky	snáška	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
zkrmování	zkrmování	k1gNnSc6
antikokcidika	antikokcidikum	k1gNnSc2
Nikarbazinu	Nikarbazin	k1gInSc2
nebo	nebo	k8xC
při	při	k7c6
vyšším	vysoký	k2eAgNnSc6d2
množství	množství	k1gNnSc6
taninu	tanin	k1gInSc2
v	v	k7c6
krmné	krmný	k2eAgFnSc6d1
dávce	dávka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobnou	pravděpodobný	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
je	být	k5eAaImIp3nS
změněná	změněný	k2eAgFnSc1d1
propustnost	propustnost	k1gFnSc1
vitelinní	vitelinný	k2eAgMnPc1d1
membrány	membrána	k1gFnSc2
žloutku	žloutek	k1gInSc2
pro	pro	k7c4
vodu	voda	k1gFnSc4
a	a	k8xC
pronikání	pronikání	k1gNnSc4
vody	voda	k1gFnSc2
z	z	k7c2
bílku	bílek	k1gInSc2
do	do	k7c2
žloutku	žloutek	k1gInSc2
při	při	k7c6
zvýšeném	zvýšený	k2eAgNnSc6d1
pH	ph	kA
bílku	bílek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	Voda	k1gMnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
nerovnoměrně	rovnoměrně	k6eNd1
usazuje	usazovat	k5eAaImIp3nS
na	na	k7c4
vitelinní	vitelinný	k2eAgMnPc1d1
membráně	membrána	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Růžové	růžový	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
bílku	bílek	k1gInSc2
je	být	k5eAaImIp3nS
porucha	porucha	k1gFnSc1
vyskytující	vyskytující	k2eAgFnSc1d1
se	se	k3xPyFc4
po	po	k7c6
zkrmování	zkrmování	k1gNnSc6
moučky	moučka	k1gFnSc2
z	z	k7c2
bavlníkových	bavlníkový	k2eAgNnPc2d1
semen	semeno	k1gNnPc2
(	(	kIx(
<g/>
obsah	obsah	k1gInSc1
kyseliny	kyselina	k1gFnSc2
cyklopropenové	cyklopropenová	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílek	Bílek	k1gMnSc1
má	mít	k5eAaImIp3nS
růžovou	růžový	k2eAgFnSc4d1
nebo	nebo	k8xC
oranžově	oranžově	k6eAd1
červenou	červený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
udržuje	udržovat	k5eAaImIp3nS
po	po	k7c4
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
po	po	k7c6
snesení	snesení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Mikrobiální	mikrobiální	k2eAgFnPc1d1
změny	změna	k1gFnPc1
ve	v	k7c6
vejci	vejce	k1gNnSc6
</s>
<s>
Po	po	k7c6
prolomení	prolomení	k1gNnSc6
všech	všecek	k3xTgFnPc2
ochranných	ochranný	k2eAgFnPc2d1
bariér	bariéra	k1gFnPc2
vejce	vejce	k1gNnSc2
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
proniknutí	proniknutí	k1gNnSc3
bakterií	bakterie	k1gFnPc2
do	do	k7c2
vaječného	vaječný	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
<g/>
,	,	kIx,
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
pomnožení	pomnožení	k1gNnSc3
a	a	k8xC
enzymatickým	enzymatický	k2eAgNnSc7d1
působením	působení	k1gNnSc7
mikroorganismů	mikroorganismus	k1gInPc2
k	k	k7c3
pronikavým	pronikavý	k2eAgFnPc3d1
chemickým	chemický	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
vaječného	vaječný	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
–	–	k?
vznikají	vznikat	k5eAaImIp3nP
mikrobiální	mikrobiální	k2eAgInPc1d1
změny	změna	k1gFnPc4
vajec	vejce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
formy	forma	k1gFnPc1
mikrobiální	mikrobiální	k2eAgFnPc1d1
zkázy	zkáza	k1gFnPc1
vajec	vejce	k1gNnPc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Černá	černý	k2eAgFnSc1d1
hniloba	hniloba	k1gFnSc1
je	být	k5eAaImIp3nS
charakterizována	charakterizovat	k5eAaBmNgFnS
neprůhledností	neprůhlednost	k1gFnSc7
vejce	vejce	k1gNnSc4
při	při	k7c6
prosvěcování	prosvěcování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k7c2
vejce	vejce	k1gNnSc2
se	se	k3xPyFc4
hromadí	hromadit	k5eAaImIp3nS
plyn	plyn	k1gInSc1
a	a	k8xC
následkem	následkem	k7c2
toho	ten	k3xDgNnSc2
vejce	vejce	k1gNnSc2
praskají	praskat	k5eAaImIp3nP
<g/>
;	;	kIx,
vaječný	vaječný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
odporně	odporně	k6eAd1
zapáchá	zapáchat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žloutek	žloutek	k1gInSc1
bývá	bývat	k5eAaImIp3nS
hnědavé	hnědavý	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
tuhý	tuhý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bílek	bílek	k1gInSc1
je	být	k5eAaImIp3nS
řídký	řídký	k2eAgInSc1d1
a	a	k8xC
žlutozelený	žlutozelený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
bývají	bývat	k5eAaImIp3nP
bakterie	bakterie	k1gFnPc1
rodu	rod	k1gInSc2
Proteus	Proteus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Červená	červený	k2eAgFnSc1d1
hniloba	hniloba	k1gFnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
při	při	k7c6
prosvěcování	prosvěcování	k1gNnSc1
červenou	červený	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
žloutku	žloutek	k1gInSc2
a	a	k8xC
načervenalou	načervenalý	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
bílku	bílek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řídký	řídký	k2eAgInSc1d1
bílek	bílek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
podskořápečné	podskořápečný	k2eAgFnPc1d1
blány	blána	k1gFnPc1
načervenalé	načervenalý	k2eAgFnSc2d1
nebo	nebo	k8xC
žlutavé	žlutavý	k2eAgFnSc2d1
skvrny	skvrna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původcem	původce	k1gMnSc7
bývají	bývat	k5eAaImIp3nP
bakterie	bakterie	k1gFnPc1
z	z	k7c2
rodu	rod	k1gInSc2
Pseudomonas	Pseudomonasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
hniloba	hniloba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
prosvěcování	prosvěcování	k1gNnSc6
nejsou	být	k5eNaImIp3nP
změny	změna	k1gFnPc1
<g/>
,	,	kIx,
hlavně	hlavně	k9
na	na	k7c6
začátku	začátek	k1gInSc6
hniloby	hniloba	k1gFnSc2
<g/>
,	,	kIx,
nápadné	nápadný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typická	typický	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
zelenavá	zelenavý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
bílku	bílek	k1gInSc2
<g/>
;	;	kIx,
žloutek	žloutek	k1gInSc1
zpočátku	zpočátku	k6eAd1
není	být	k5eNaImIp3nS
postižen	postihnout	k5eAaPmNgMnS
<g/>
,	,	kIx,
později	pozdě	k6eAd2
tuhne	tuhnout	k5eAaImIp3nS
a	a	k8xC
tmavne	tmavnout	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristický	charakteristický	k2eAgInSc4d1
je	být	k5eAaImIp3nS
nakyslý	nakyslý	k2eAgInSc4d1
pach	pach	k1gInSc4
vaječného	vaječný	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
připomínající	připomínající	k2eAgNnSc4d1
ovoce	ovoce	k1gNnSc4
<g/>
,	,	kIx,
sýry	sýr	k1gInPc4
nebo	nebo	k8xC
zeleninu	zelenina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vyvolávána	vyvolávat	k5eAaImNgFnS
bakteriemi	bakterie	k1gFnPc7
rodu	rod	k1gInSc2
Pseudomonas	Pseudomonasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
hniloba	hniloba	k1gFnSc1
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
při	při	k7c6
prosvěcování	prosvěcování	k1gNnSc6
jasně	jasně	k6eAd1
bílou	bílý	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
a	a	k8xC
žloutek	žloutek	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
vejci	vejce	k1gNnSc6
pohyblivý	pohyblivý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vaječného	vaječný	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
bývají	bývat	k5eAaImIp3nP
izolovány	izolován	k2eAgFnPc1d1
nejčastěji	často	k6eAd3
bakterie	bakterie	k1gFnPc4
rodu	rod	k1gInSc2
Pseudomonas	Pseudomonasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Senná	senný	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
jsou	být	k5eAaImIp3nP
nazývána	nazýván	k2eAgNnPc1d1
ta	ten	k3xDgFnSc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
obsah	obsah	k1gInSc4
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
pach	pach	k1gInSc4
připomínající	připomínající	k2eAgNnSc4d1
seno	seno	k1gNnSc4
nebo	nebo	k8xC
trávu	tráva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původcem	původce	k1gMnSc7
bývají	bývat	k5eAaImIp3nP
bakterie	bakterie	k1gFnPc1
rodů	rod	k1gInPc2
Pseudomonas	Pseudomonasa	k1gFnPc2
<g/>
,	,	kIx,
Alcaligenes	Alcaligenesa	k1gFnPc2
a	a	k8xC
Aerobacter	Aerobactra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Smíšená	smíšený	k2eAgFnSc1d1
hniloba	hniloba	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
vyvolána	vyvolat	k5eAaPmNgFnS
bakteriemi	bakterie	k1gFnPc7
Pseudomonas	Pseudomonasa	k1gFnPc2
fluorescens	fluorescens	k1gInSc1
<g/>
,	,	kIx,
Micrococcus	Micrococcus	k1gMnSc1
roseus	roseus	k1gMnSc1
<g/>
,	,	kIx,
Micrococcus	Micrococcus	k1gMnSc1
aureus	aureus	k1gMnSc1
<g/>
,	,	kIx,
Serratia	Serratia	k1gFnSc1
marcescens	marcescens	k1gInSc1
aj.	aj.	kA
</s>
<s>
Zakalené	zakalený	k2eAgInPc1d1
bílky	bílek	k1gInPc1
bývají	bývat	k5eAaImIp3nP
někdy	někdy	k6eAd1
znakem	znak	k1gInSc7
bakteriálního	bakteriální	k2eAgNnSc2d1
kažení	kažení	k1gNnSc2
<g/>
,	,	kIx,
vyvolaného	vyvolaný	k2eAgInSc2d1
bakteriemi	bakterie	k1gFnPc7
rodu	rod	k1gInSc2
Micrococcus	Micrococcus	k1gInSc1
a	a	k8xC
Sarcina	sarcina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
ale	ale	k8xC
i	i	k9
bílek	bílek	k1gInSc1
čerstvě	čerstvě	k6eAd1
sneseného	snesený	k2eAgNnSc2d1
vejce	vejce	k1gNnSc2
bývá	bývat	k5eAaImIp3nS
zakalen	zakalit	k5eAaPmNgInS
zvýšenou	zvýšený	k2eAgFnSc7d1
koncentrací	koncentrace	k1gFnSc7
oxidu	oxid	k1gInSc2
uhličitého	uhličitý	k2eAgInSc2d1
</s>
<s>
Plesnivá	plesnivý	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
povrchu	povrch	k1gInSc6
skořápky	skořápka	k1gFnSc2
nacházíme	nacházet	k5eAaImIp1nP
vždy	vždy	k6eAd1
spory	spor	k1gInPc1
plísní	plíseň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vhodných	vhodný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
(	(	kIx(
<g/>
vlhkost	vlhkost	k1gFnSc1
a	a	k8xC
teplo	teplo	k1gNnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c6
povrchu	povrch	k1gInSc6
skořápky	skořápka	k1gFnSc2
porosty	porost	k1gInPc1
plísní	plíseň	k1gFnPc2
nebo	nebo	k8xC
prorůstají	prorůstat	k5eAaImIp3nP
skořápkou	skořápka	k1gFnSc7
a	a	k8xC
vytvářejí	vytvářet	k5eAaImIp3nP
kruhovité	kruhovitý	k2eAgFnPc4d1
kolonie	kolonie	k1gFnPc4
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
v	v	k7c6
místě	místo	k1gNnSc6
vzduchové	vzduchový	k2eAgFnSc2d1
bubliny	bublina	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
při	při	k7c6
prosvěcování	prosvěcování	k1gNnSc6
vajec	vejce	k1gNnPc2
dobře	dobře	k6eAd1
viditelné	viditelný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
vajíčka	vajíčko	k1gNnSc2
páchne	páchnout	k5eAaImIp3nS
po	po	k7c6
plísních	plíseň	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Cizorodé	cizorodý	k2eAgFnPc1d1
látky	látka	k1gFnPc1
ve	v	k7c6
vejcích	vejce	k1gNnPc6
</s>
<s>
Cizorodé	cizorodý	k2eAgFnPc1d1
látky	látka	k1gFnPc1
se	se	k3xPyFc4
do	do	k7c2
vejce	vejce	k1gNnSc2
dostávají	dostávat	k5eAaImIp3nP
především	především	k9
prostřednictvím	prostřednictvím	k7c2
krmiva	krmivo	k1gNnSc2
<g/>
,	,	kIx,
medikovaného	medikovaný	k2eAgNnSc2d1
krmiva	krmivo	k1gNnSc2
nebo	nebo	k8xC
napájecí	napájecí	k2eAgFnSc2d1
vody	voda	k1gFnSc2
nebo	nebo	k8xC
aplikací	aplikace	k1gFnPc2
veterinárních	veterinární	k2eAgInPc2d1
přípravků	přípravek	k1gInPc2
drůbeži	drůbež	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
látky	látka	k1gFnPc4
organického	organický	k2eAgMnSc2d1
nebo	nebo	k8xC
anorganického	anorganický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
v	v	k7c4
zemědělství	zemědělství	k1gNnSc4
jako	jako	k8xC,k8xS
herbicidy	herbicid	k1gInPc4
nebo	nebo	k8xC
insekticidy	insekticid	k1gInPc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
jsou	být	k5eAaImIp3nP
přidávány	přidávat	k5eAaImNgFnP
do	do	k7c2
krmiva	krmivo	k1gNnSc2
nebo	nebo	k8xC
vody	voda	k1gFnSc2
jako	jako	k8xS,k8xC
léčiva	léčivo	k1gNnSc2
v	v	k7c6
preventivních	preventivní	k2eAgFnPc6d1
nebo	nebo	k8xC
léčebných	léčebný	k2eAgFnPc6d1
koncentracích	koncentrace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidávání	přidávání	k1gNnSc1
různých	různý	k2eAgInPc2d1
léků	lék	k1gInPc2
jako	jako	k8xC,k8xS
antikokcidik	antikokcidika	k1gFnPc2
nebo	nebo	k8xC
antibiotik	antibiotikum	k1gNnPc2
je	být	k5eAaImIp3nS
omezeno	omezit	k5eAaPmNgNnS
příslušnými	příslušný	k2eAgInPc7d1
předpisy	předpis	k1gInPc7
a	a	k8xC
jsou	být	k5eAaImIp3nP
používána	používán	k2eAgNnPc1d1
pouze	pouze	k6eAd1
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
v	v	k7c6
organismu	organismus	k1gInSc6
nevytváří	vytvářet	k5eNaImIp3nP,k5eNaPmIp3nP
rezidua	reziduum	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přednost	přednost	k1gFnSc1
je	být	k5eAaImIp3nS
dávána	dávat	k5eAaImNgFnS
těm	ten	k3xDgFnPc3
látkám	látka	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
rychle	rychle	k6eAd1
z	z	k7c2
organismu	organismus	k1gInSc2
vylučují	vylučovat	k5eAaImIp3nP
nebo	nebo	k8xC
metabolizují	metabolizovat	k5eAaImIp3nP
na	na	k7c4
neškodné	škodný	k2eNgFnPc4d1
složky	složka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Léčiv	léčivo	k1gNnPc2
<g/>
,	,	kIx,
chemických	chemický	k2eAgInPc2d1
<g/>
,	,	kIx,
biologických	biologický	k2eAgInPc2d1
a	a	k8xC
fyzikálních	fyzikální	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
lze	lze	k6eAd1
použít	použít	k5eAaPmF
jen	jen	k9
po	po	k7c6
souhlasu	souhlas	k1gInSc6
veterinárního	veterinární	k2eAgMnSc2d1
lékaře	lékař	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zároveň	zároveň	k6eAd1
nařídí	nařídit	k5eAaPmIp3nS
provedení	provedení	k1gNnSc3
příslušných	příslušný	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
výskytu	výskyt	k1gInSc3
cizorodých	cizorodý	k2eAgFnPc2d1
látek	látka	k1gFnPc2
v	v	k7c6
produkovaných	produkovaný	k2eAgNnPc6d1
vejcích	vejce	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Epizootologický	Epizootologický	k2eAgInSc1d1
význam	význam	k1gInSc1
vajec	vejce	k1gNnPc2
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1
přenosu	přenos	k1gInSc2
infekce	infekce	k1gFnSc2
na	na	k7c4
člověka	člověk	k1gMnSc4
vejcem	vejce	k1gNnSc7
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
menší	malý	k2eAgFnSc1d2
než	než	k8xS
u	u	k7c2
jiných	jiný	k2eAgFnPc2d1
potravin	potravina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zejména	zejména	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
vaječný	vaječný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
skořápkou	skořápka	k1gFnSc7
před	před	k7c7
kontaminací	kontaminace	k1gFnSc7
zvnějšku	zvnějšku	k6eAd1
<g/>
,	,	kIx,
takže	takže	k8xS
v	v	k7c6
okamžiku	okamžik	k1gInSc6
snášky	snáška	k1gFnSc2
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
sterilní	sterilní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
výjimkám	výjimka	k1gFnPc3
dochází	docházet	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
vertikálního	vertikální	k2eAgInSc2d1
přenosu	přenos	k1gInSc2
některých	některý	k3yIgInPc2
mikroorganismů	mikroorganismus	k1gInPc2
nebo	nebo	k8xC
po	po	k7c6
kontaminaci	kontaminace	k1gFnSc6
vejce	vejce	k1gNnSc2
po	po	k7c6
snesení	snesení	k1gNnSc6
v	v	k7c6
kontaminovaném	kontaminovaný	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
či	či	k8xC
nedodržováním	nedodržování	k1gNnSc7
hygienických	hygienický	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
při	při	k7c6
manipulaci	manipulace	k1gFnSc6
s	s	k7c7
vejci	vejce	k1gNnPc7
nebo	nebo	k8xC
s	s	k7c7
vaječným	vaječný	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
mechanismu	mechanismus	k1gInSc2
přestupu	přestup	k1gInSc2
mikroorganismů	mikroorganismus	k1gInPc2
do	do	k7c2
vejce	vejce	k1gNnSc2
rozlišujeme	rozlišovat	k5eAaImIp1nP
transovariální	transovariální	k2eAgInSc4d1
přenos	přenos	k1gInSc4
přímý	přímý	k2eAgInSc4d1
(	(	kIx(
<g/>
vertikální	vertikální	k2eAgInSc4d1
<g/>
)	)	kIx)
a	a	k8xC
nepřímý	přímý	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
některých	některý	k3yIgNnPc6
onemocněních	onemocnění	k1gNnPc6
se	se	k3xPyFc4
mikroorganismy	mikroorganismus	k1gInPc7
dostávají	dostávat	k5eAaImIp3nP
přímo	přímo	k6eAd1
z	z	k7c2
krve	krev	k1gFnSc2
infikované	infikovaný	k2eAgFnSc2d1
nosnice	nosnice	k1gFnSc2
do	do	k7c2
vaječníku	vaječník	k1gInSc2
a	a	k8xC
do	do	k7c2
tvořící	tvořící	k2eAgFnSc2d1
se	se	k3xPyFc4
žloutkové	žloutkový	k2eAgFnSc2d1
koule	koule	k1gFnSc2
(	(	kIx(
<g/>
salmonely	salmonela	k1gFnPc1
<g/>
,	,	kIx,
mykobakterie	mykobakterie	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žloutek	žloutek	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
také	také	k6eAd1
kontaminován	kontaminovat	k5eAaBmNgInS
kontaktem	kontakt	k1gInSc7
s	s	k7c7
membránami	membrána	k1gFnPc7
vzdušných	vzdušný	k2eAgMnPc2d1
vaků	vak	k1gInPc2
(	(	kIx(
<g/>
mykoplasmata	mykoplasma	k1gNnPc4
<g/>
)	)	kIx)
při	při	k7c6
pronikání	pronikání	k1gNnSc6
do	do	k7c2
infundibula	infundibulum	k1gNnSc2
vejcovodu	vejcovod	k1gInSc2
<g/>
;	;	kIx,
vaječný	vaječný	k2eAgInSc1d1
bílek	bílek	k1gInSc1
pak	pak	k6eAd1
patogeny	patogen	k1gInPc7
lokalizovanými	lokalizovaný	k2eAgInPc7d1
v	v	k7c6
bílkotvorných	bílkotvorný	k2eAgFnPc6d1
žlázách	žláza	k1gFnPc6
vejcovodu	vejcovod	k1gInSc2
(	(	kIx(
<g/>
retroviry	retrovira	k1gFnSc2
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vejcovod	vejcovod	k1gInSc1
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
sterilní	sterilní	k2eAgNnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
části	část	k1gFnSc2
blízko	blízko	k7c2
kloaky	kloaka	k1gFnSc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
má	mít	k5eAaImIp3nS
prostředí	prostředí	k1gNnSc4
vejcovodu	vejcovod	k1gInSc2
baktericidní	baktericidní	k2eAgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgInPc6
těchto	tento	k3xDgInPc6
případech	případ	k1gInPc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
přímý	přímý	k2eAgInSc4d1
(	(	kIx(
<g/>
vertikální	vertikální	k2eAgInSc4d1
<g/>
)	)	kIx)
transovariální	transovariální	k2eAgInSc4d1
přenos	přenos	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nepřímému	přímý	k2eNgInSc3d1
transovariálnímu	transovariální	k2eAgInSc3d1
přenosu	přenos	k1gInSc3
infekce	infekce	k1gFnSc2
dochází	docházet	k5eAaImIp3nS
při	při	k7c6
kontaminaci	kontaminace	k1gFnSc6
vejce	vejce	k1gNnSc2
zvenčí	zvenčí	k6eAd1
přes	přes	k7c4
skořápku	skořápka	k1gFnSc4
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
již	již	k9
trusem	trus	k1gInSc7
při	při	k7c6
průchodu	průchod	k1gInSc6
kloakou	kloaka	k1gFnSc7
nebo	nebo	k8xC
stykem	styk	k1gInSc7
s	s	k7c7
vnějším	vnější	k2eAgNnSc7d1
prostředím	prostředí	k1gNnSc7
(	(	kIx(
<g/>
podestýlka	podestýlka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
mikroorganismy	mikroorganismus	k1gInPc1
prorůstají	prorůstat	k5eAaImIp3nP
aktivně	aktivně	k6eAd1
přes	přes	k7c4
neporušenou	porušený	k2eNgFnSc4d1
skořápku	skořápka	k1gFnSc4
do	do	k7c2
vaječného	vaječný	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
(	(	kIx(
<g/>
salmonely	salmonela	k1gFnPc1
<g/>
,	,	kIx,
plísně	plíseň	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
se	se	k3xPyFc4
zase	zase	k9
mohou	moct	k5eAaImIp3nP
dostat	dostat	k5eAaPmF
dovnitř	dovnitř	k6eAd1
pasivně	pasivně	k6eAd1
nasátím	nasátí	k1gNnSc7
nebo	nebo	k8xC
přes	přes	k7c4
mechanicky	mechanicky	k6eAd1
porušenou	porušený	k2eAgFnSc4d1
skořápku	skořápka	k1gFnSc4
(	(	kIx(
<g/>
praskliny	prasklina	k1gFnPc1
<g/>
,	,	kIx,
naklování	naklování	k1gNnSc1
skořápky	skořápka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
např.	např.	kA
půdní	půdní	k2eAgFnSc2d1
bakterie	bakterie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
také	také	k9
mohou	moct	k5eAaImIp3nP
nastat	nastat	k5eAaPmF
ojediněle	ojediněle	k6eAd1
případy	případ	k1gInPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
vejcovod	vejcovod	k1gInSc1
infikován	infikován	k2eAgInSc1d1
zvenčí	zvenčí	k6eAd1
(	(	kIx(
<g/>
pojímání	pojímání	k1gNnSc1
kachen	kachna	k1gFnPc2
na	na	k7c6
rybníku	rybník	k1gInSc6
<g/>
,	,	kIx,
umělá	umělý	k2eAgFnSc1d1
inseminace	inseminace	k1gFnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
a	a	k8xC
pak	pak	k6eAd1
mohou	moct	k5eAaImIp3nP
mikroorganismu	mikroorganismus	k1gInSc3
proniknout	proniknout	k5eAaPmF
až	až	k9
do	do	k7c2
vaječného	vaječný	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Snůška	snůška	k1gFnSc1
</s>
<s>
Výraz	výraz	k1gInSc1
„	„	k?
<g/>
snůška	snůška	k1gFnSc1
<g/>
“	“	k?
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
používá	používat	k5eAaImIp3nS
v	v	k7c6
ornitologii	ornitologie	k1gFnSc6
a	a	k8xC
myslivosti	myslivost	k1gFnSc6
<g/>
;	;	kIx,
v	v	k7c6
drůbežnictví	drůbežnictví	k1gNnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
termín	termín	k1gInSc1
„	„	k?
<g/>
snáška	snáška	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
kladení	kladení	k1gNnSc1
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
ovipozice	ovipozice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Velikost	velikost	k1gFnSc1
snášky	snáška	k1gFnSc2
(	(	kIx(
<g/>
počet	počet	k1gInSc1
snesených	snesený	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
jak	jak	k6eAd1
mezi	mezi	k7c7
ptačími	ptačí	k2eAgInPc7d1
druhy	druh	k1gInPc7
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
v	v	k7c6
rámci	rámec	k1gInSc6
jednoho	jeden	k4xCgInSc2
druhu	druh	k1gInSc2
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
přírodních	přírodní	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
–	–	k?
aby	aby	kYmCp3nS
pro	pro	k7c4
vylíhnutá	vylíhnutý	k2eAgNnPc4d1
mláďata	mládě	k1gNnPc4
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
zajistit	zajistit	k5eAaPmF
dostatek	dostatek	k1gInSc4
potravy	potrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadměrné	nadměrný	k2eAgFnPc4d1
množství	množství	k1gNnSc1
vajec	vejce	k1gNnPc2
také	také	k9
může	moct	k5eAaImIp3nS
snadněji	snadno	k6eAd2
odhalit	odhalit	k5eAaPmF
hnízdo	hnízdo	k1gNnSc4
predátorům	predátor	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vliv	vliv	k1gInSc1
má	mít	k5eAaImIp3nS
také	také	k9
stáří	stáří	k1gNnSc1
ptáka	pták	k1gMnSc2
(	(	kIx(
<g/>
starší	starý	k2eAgMnPc1d2
jedinci	jedinec	k1gMnPc1
snášejí	snášet	k5eAaImIp3nP
více	hodně	k6eAd2
vajec	vejce	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
roční	roční	k2eAgNnSc4d1
období	období	k1gNnSc4
(	(	kIx(
<g/>
čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
později	pozdě	k6eAd2
pták	pták	k1gMnSc1
zahnízdí	zahnízdit	k5eAaPmIp3nS
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
bývá	bývat	k5eAaImIp3nS
snáška	snáška	k1gFnSc1
menší	malý	k2eAgFnSc1d2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
snášky	snáška	k1gFnSc2
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
ovlivněna	ovlivnit	k5eAaPmNgFnS
i	i	k9
geneticky	geneticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
ptačí	ptačí	k2eAgInPc1d1
druhy	druh	k1gInPc1
snášejí	snášet	k5eAaImIp3nP
jedno	jeden	k4xCgNnSc1
„	„	k?
<g/>
pojistné	pojistné	k1gNnSc4
<g/>
“	“	k?
vejce	vejce	k1gNnSc2
navíc	navíc	k6eAd1
–	–	k?
vylíhnuté	vylíhnutý	k2eAgNnSc1d1
mládě	mládě	k1gNnSc1
z	z	k7c2
něj	on	k3xPp3gMnSc2
je	být	k5eAaImIp3nS
slabší	slabý	k2eAgMnPc1d2
a	a	k8xC
rodiče	rodič	k1gMnPc1
je	on	k3xPp3gInPc4
zpravidla	zpravidla	k6eAd1
nechají	nechat	k5eAaPmIp3nP
uhynout	uhynout	k5eAaPmF
<g/>
,	,	kIx,
avšak	avšak	k8xC
pokud	pokud	k8xS
z	z	k7c2
nějakého	nějaký	k3yIgInSc2
důvodu	důvod	k1gInSc2
uhyne	uhynout	k5eAaPmIp3nS
silnější	silný	k2eAgNnSc4d2
mládě	mládě	k1gNnSc4
<g/>
,	,	kIx,
rodiče	rodič	k1gMnPc1
mají	mít	k5eAaImIp3nP
ještě	ještě	k9
jedno	jeden	k4xCgNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
uplatňuje	uplatňovat	k5eAaImIp3nS
například	například	k6eAd1
u	u	k7c2
tučňáků	tučňák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
dravců	dravec	k1gMnPc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
dokonce	dokonce	k9
tzv.	tzv.	kA
kainismus	kainismus	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
silnější	silný	k2eAgMnSc1d2
z	z	k7c2
mláďat	mládě	k1gNnPc2
slabší	slabý	k2eAgMnSc1d2
usmrtí	usmrtit	k5eAaPmIp3nS
a	a	k8xC
někdy	někdy	k6eAd1
i	i	k9
sežere	sežrat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
příhodných	příhodný	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
ovšem	ovšem	k9
rodiče	rodič	k1gMnPc4
odchovají	odchovat	k5eAaPmIp3nP
oba	dva	k4xCgMnPc4
sourozence	sourozenec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Produkce	produkce	k1gFnSc1
vajec	vejce	k1gNnPc2
domácí	domácí	k2eAgFnSc2d1
drůbeže	drůbež	k1gFnSc2
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
snášky	snáška	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
jak	jak	k6eAd1
počet	počet	k1gInSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
hmotnost	hmotnost	k1gFnSc4
a	a	k8xC
kvalitu	kvalita	k1gFnSc4
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nosnice	nosnice	k1gFnPc1
snesou	snést	k5eAaPmIp3nP
za	za	k7c4
určité	určitý	k2eAgNnSc4d1
časové	časový	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
konzumních	konzumní	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
vhodnější	vhodný	k2eAgNnSc1d2
vyjadřovat	vyjadřovat	k5eAaImF
snášku	snáška	k1gFnSc4
podle	podle	k7c2
produkce	produkce	k1gFnSc2
vaječné	vaječný	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
(	(	kIx(
<g/>
podle	podle	k7c2
počtu	počet	k1gInSc2
vajec	vejce	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
hmotnosti	hmotnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
u	u	k7c2
násadových	násadový	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
počtem	počet	k1gInSc7
vajec	vejce	k1gNnPc2
vhodných	vhodný	k2eAgNnPc2d1
k	k	k7c3
nasazení	nasazení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickými	charakteristický	k2eAgInPc7d1
ukazateli	ukazatel	k1gInPc7
snášky	snáška	k1gFnSc2
jsou	být	k5eAaImIp3nP
její	její	k3xOp3gFnSc1
intenzita	intenzita	k1gFnSc1
a	a	k8xC
stálost	stálost	k1gFnSc1
(	(	kIx(
<g/>
perzistence	perzistence	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intenzitou	intenzita	k1gFnSc7
snášky	snáška	k1gFnSc2
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
počet	počet	k1gInSc1
po	po	k7c6
sobě	se	k3xPyFc3
snesených	snesený	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
(	(	kIx(
<g/>
série	série	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
délka	délka	k1gFnSc1
intervalů	interval	k1gInPc2
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
sériemi	série	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjadřuje	vyjadřovat	k5eAaImIp3nS
se	se	k3xPyFc4
tzv.	tzv.	kA
procentuální	procentuální	k2eAgFnSc7d1
snáškou	snáška	k1gFnSc7
(	(	kIx(
<g/>
počet	počet	k1gInSc1
vajec	vejce	k1gNnPc2
snesených	snesený	k2eAgNnPc2d1
za	za	k7c4
určité	určitý	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průběh	průběh	k1gInSc1
snášky	snáška	k1gFnSc2
se	se	k3xPyFc4
graficky	graficky	k6eAd1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
tzv.	tzv.	kA
snáškovou	snáškový	k2eAgFnSc7d1
křivkou	křivka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Snáškový	snáškový	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
začíná	začínat	k5eAaImIp3nS
snesením	snesení	k1gNnSc7
prvního	první	k4xOgInSc2
vejce	vejce	k1gNnPc4
po	po	k7c4
dosažení	dosažení	k1gNnSc4
pohlavní	pohlavní	k2eAgFnSc2d1
dospělosti	dospělost	k1gFnSc2
samice	samice	k1gFnSc2
drůbeže	drůbež	k1gFnSc2
a	a	k8xC
končí	končit	k5eAaImIp3nS
pelicháním	pelichání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slepice	slepice	k1gFnSc1
snášejí	snášet	k5eAaImIp3nP
vejce	vejce	k1gNnPc1
v	v	k7c6
sériích	série	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnSc1d2
pauza	pauza	k1gFnSc1
ve	v	k7c6
snášce	snáška	k1gFnSc6
je	být	k5eAaImIp3nS
fyziologicky	fyziologicky	k6eAd1
způsobena	způsobit	k5eAaPmNgFnS
pelicháním	pelichání	k1gNnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
během	během	k7c2
10	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
dní	den	k1gInPc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
atrofii	atrofie	k1gFnSc3
pohlavních	pohlavní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
<g/>
;	;	kIx,
stejně	stejně	k6eAd1
rychle	rychle	k6eAd1
ale	ale	k8xC
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
obnově	obnova	k1gFnSc3
jejich	jejich	k3xOp3gFnSc2
činnosti	činnost	k1gFnSc2
po	po	k7c6
ukončení	ukončení	k1gNnSc6
pelichání	pelichání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snáškových	snáškový	k2eAgMnPc2d1
cyklů	cyklus	k1gInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
několik	několik	k4yIc4
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
a	a	k8xC
délka	délka	k1gFnSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
druhu	druh	k1gInSc6
domácí	domácí	k2eAgFnSc2d1
drůbeže	drůbež	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
slepic	slepice	k1gFnPc2
v	v	k7c6
intenzivních	intenzivní	k2eAgInPc6d1
chovech	chov	k1gInPc6
trvá	trvat	k5eAaImIp3nS
snáškový	snáškový	k2eAgInSc4d1
cyklus	cyklus	k1gInSc4
zpravidla	zpravidla	k6eAd1
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
(	(	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
v	v	k7c6
ojedinělých	ojedinělý	k2eAgInPc6d1
případech	případ	k1gInPc6
se	se	k3xPyFc4
slepice	slepice	k1gFnPc1
chovají	chovat	k5eAaImIp3nP
po	po	k7c4
dobu	doba	k1gFnSc4
dvou	dva	k4xCgInPc2
snáškových	snáškový	k2eAgInPc2d1
cyklů	cyklus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roční	roční	k2eAgFnSc1d1
snáška	snáška	k1gFnSc1
u	u	k7c2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
drůbeže	drůbež	k1gFnSc2
se	se	k3xPyFc4
orientačně	orientačně	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
u	u	k7c2
slepic	slepice	k1gFnPc2
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
240	#num#	k4
<g/>
–	–	k?
<g/>
360	#num#	k4
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
kachen	kachna	k1gFnPc2
150	#num#	k4
<g/>
–	–	k?
<g/>
290	#num#	k4
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
krůt	krůta	k1gFnPc2
100	#num#	k4
<g/>
–	–	k?
<g/>
150	#num#	k4
vajec	vejce	k1gNnPc2
<g/>
,	,	kIx,
hus	husa	k1gFnPc2
50	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
vajec	vejce	k1gNnPc2
a	a	k8xC
perliček	perlička	k1gFnPc2
100	#num#	k4
<g/>
–	–	k?
<g/>
150	#num#	k4
vajec	vejce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Označování	označování	k1gNnSc1
vajec	vejce	k1gNnPc2
</s>
<s>
V	v	k7c6
EU	EU	kA
platí	platit	k5eAaImIp3nS
povinnost	povinnost	k1gFnSc4
označování	označování	k1gNnSc2
prodávaných	prodávaný	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
typem	typ	k1gInSc7
chovu	chov	k1gInSc2
<g/>
,	,	kIx,
zemí	zem	k1gFnPc2
původu	původ	k1gInSc2
a	a	k8xC
registračním	registrační	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
hospodářství	hospodářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
text	text	k1gInSc4
na	na	k7c6
obalu	obal	k1gInSc6
vajec	vejce	k1gNnPc2
a	a	k8xC
kód	kód	k1gInSc4
přímo	přímo	k6eAd1
na	na	k7c6
vejcích	vejce	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příklad	příklad	k1gInSc1
kódu	kód	k1gInSc2
<g/>
:	:	kIx,
1	#num#	k4
CZ	CZ	kA
6789	#num#	k4
</s>
<s>
První	první	k4xOgFnSc1
číslice	číslice	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
způsob	způsob	k1gInSc4
chovu	chov	k1gInSc2
nosnic	nosnice	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
0	#num#	k4
–	–	k?
Vejce	vejce	k1gNnSc2
nosnic	nosnice	k1gFnPc2
v	v	k7c6
ekologickém	ekologický	k2eAgNnSc6d1
zemědělství	zemědělství	k1gNnSc6
(	(	kIx(
<g/>
BIO	BIO	k?
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
–	–	k?
Vejce	vejce	k1gNnSc2
nosnic	nosnice	k1gFnPc2
ve	v	k7c6
volném	volný	k2eAgInSc6d1
výběhu	výběh	k1gInSc6
</s>
<s>
2	#num#	k4
–	–	k?
Vejce	vejce	k1gNnSc2
nosnic	nosnice	k1gFnPc2
v	v	k7c6
halách	hala	k1gFnPc6
(	(	kIx(
<g/>
na	na	k7c6
podestýlce	podestýlka	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
–	–	k?
Vejce	vejce	k1gNnSc2
nosnic	nosnice	k1gFnPc2
v	v	k7c6
klecích	klek	k1gInPc6
</s>
<s>
Další	další	k2eAgNnPc4d1
dvě	dva	k4xCgNnPc4
písmena	písmeno	k1gNnPc4
značí	značit	k5eAaImIp3nS
kód	kód	k1gInSc4
státu	stát	k1gInSc2
(	(	kIx(
<g/>
dle	dle	k7c2
ISO	ISO	kA
3166	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
kterého	který	k3yRgNnSc2,k3yQgNnSc2,k3yIgNnSc2
vejce	vejce	k1gNnSc2
pochází	pocházet	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
CZ	CZ	kA
–	–	k?
Česko	Česko	k1gNnSc1
</s>
<s>
AT	AT	kA
–	–	k?
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
DE	DE	k?
–	–	k?
Německo	Německo	k1gNnSc1
</s>
<s>
PL	PL	kA
–	–	k?
Polsko	Polsko	k1gNnSc1
</s>
<s>
SK	Sk	kA
–	–	k?
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1
číslicí	číslice	k1gFnSc7
je	být	k5eAaImIp3nS
registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
hospodářství	hospodářství	k1gNnSc2
(	(	kIx(
<g/>
vyhledávání	vyhledávání	k1gNnSc1
pro	pro	k7c4
CZ	CZ	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
6789	#num#	k4
–	–	k?
V	v	k7c6
tomto	tento	k3xDgInSc6
příkladu	příklad	k1gInSc6
náhodně	náhodně	k6eAd1
zvolené	zvolený	k2eAgNnSc4d1
</s>
<s>
Hmotnostní	hmotnostní	k2eAgFnPc1d1
třídy	třída	k1gFnPc1
vajec	vejce	k1gNnPc2
</s>
<s>
Velikost	velikost	k1gFnSc4
prodávaných	prodávaný	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
se	se	k3xPyFc4
pro	pro	k7c4
prodejní	prodejní	k2eAgInPc4d1
účely	účel	k1gInPc4
rozděluji	rozdělovat	k5eAaImIp1nS
dle	dle	k7c2
hmotnosti	hmotnost	k1gFnSc2
do	do	k7c2
čtyř	čtyři	k4xCgFnPc2
skupin	skupina	k1gFnPc2
(	(	kIx(
<g/>
tříd	třída	k1gFnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
S	s	k7c7
-	-	kIx~
velikost	velikost	k1gFnSc1
malá	malý	k2eAgFnSc1d1
<g/>
,	,	kIx,
hmotnost	hmotnost	k1gFnSc1
do	do	k7c2
53	#num#	k4
gramů	gram	k1gInPc2
</s>
<s>
M	M	kA
-	-	kIx~
velikost	velikost	k1gFnSc1
střední	střední	k2eAgFnSc1d1
<g/>
,	,	kIx,
hmotnost	hmotnost	k1gFnSc1
53-63	53-63	k4
gramů	gram	k1gInPc2
</s>
<s>
L	L	kA
-	-	kIx~
velikost	velikost	k1gFnSc1
velká	velká	k1gFnSc1
<g/>
,	,	kIx,
hmotnost	hmotnost	k1gFnSc1
63-73	63-73	k4
gramů	gram	k1gInPc2
</s>
<s>
XL	XL	kA
-	-	kIx~
velikost	velikost	k1gFnSc1
velmi	velmi	k6eAd1
velká	velký	k2eAgFnSc1d1
<g/>
,	,	kIx,
hmotnost	hmotnost	k1gFnSc1
nad	nad	k7c4
73	#num#	k4
gramů	gram	k1gInPc2
</s>
<s>
Nejmenší	malý	k2eAgNnPc4d3
vajíčka	vajíčko	k1gNnPc4
snáší	snášet	k5eAaImIp3nS
nosnice	nosnice	k1gFnSc1
na	na	k7c6
počátku	počátek	k1gInSc6
snášky	snáška	k1gFnSc2
<g/>
,	,	kIx,
váží	vážit	k5eAaImIp3nS
35-53	35-53	k4
gramů	gram	k1gInPc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
označována	označovat	k5eAaImNgNnP
písmenem	písmeno	k1gNnSc7
S.	S.	kA
První	první	k4xOgNnPc4
vajíčka	vajíčko	k1gNnPc4
od	od	k7c2
nosnic	nosnice	k1gFnPc2
bývají	bývat	k5eAaImIp3nP
po	po	k7c6
stránce	stránka	k1gFnSc6
kvalitativních	kvalitativní	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
nejlepší	dobrý	k2eAgFnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
vajec	vejce	k1gNnPc2
se	se	k3xPyFc4
se	s	k7c7
stárnutím	stárnutí	k1gNnSc7
nosnic	nosnice	k1gFnPc2
zvětšuje	zvětšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
žloutku	žloutek	k1gInSc2
je	být	k5eAaImIp3nS
ve	v	k7c6
vejcích	vejce	k1gNnPc6
přibližně	přibližně	k6eAd1
stejný	stejný	k2eAgInSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
velikostí	velikost	k1gFnSc7
vajec	vejce	k1gNnPc2
se	se	k3xPyFc4
zvyšuje	zvyšovat	k5eAaImIp3nS
obsah	obsah	k1gInSc1
bílku	bílek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
snášce	snáška	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
1-2	1-2	k4
%	%	kIx~
abnormalit	abnormalita	k1gFnPc2
ve	v	k7c6
velikosti	velikost	k1gFnSc6
vajec	vejce	k1gNnPc2
<g/>
:	:	kIx,
velmi	velmi	k6eAd1
velká	velký	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
s	s	k7c7
hmotnosti	hmotnost	k1gFnSc2
105-115	105-115	k4
gramů	gram	k1gInPc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
i	i	k9
více	hodně	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
výjimečná	výjimečný	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
obsahují	obsahovat	k5eAaImIp3nP
většinou	většina	k1gFnSc7
2	#num#	k4
až	až	k9
3	#num#	k4
žloutky	žloutek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zkamenělá	zkamenělý	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
již	již	k9
paleontologové	paleontolog	k1gMnPc1
znají	znát	k5eAaImIp3nP
tisíce	tisíc	k4xCgInPc1
fosilních	fosilní	k2eAgNnPc2d1
vajíček	vajíčko	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
od	od	k7c2
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
obojživelníků	obojživelník	k1gMnPc2
<g/>
,	,	kIx,
plazů	plaz	k1gMnPc2
i	i	k8xC
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšími	veliký	k2eAgInPc7d3
známými	známý	k2eAgInPc7d1
vajíčky	vajíčko	k1gNnPc7
byla	být	k5eAaImAgFnS
fosilní	fosilní	k2eAgNnSc4d1
vejce	vejce	k1gNnSc4
jakéhosi	jakýsi	k3yIgMnSc2
křídového	křídový	k2eAgMnSc2d1
teropodního	teropodní	k2eAgMnSc2d1
oviraptorosaurního	oviraptorosaurní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
z	z	k7c2
území	území	k1gNnSc2
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
Vnitřního	vnitřní	k2eAgNnSc2d1
Mongolska	Mongolsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dosahující	dosahující	k2eAgFnPc1d1
délky	délka	k1gFnPc1
až	až	k9
kolem	kolem	k7c2
61	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Známá	známý	k2eAgNnPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
hnízdiště	hnízdiště	k1gNnPc1
dinosaurů	dinosaurus	k1gMnPc2
z	z	k7c2
období	období	k1gNnSc2
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Původci	původce	k1gMnPc7
těchto	tento	k3xDgNnPc2
podlouhlých	podlouhlý	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
kladli	klást	k5eAaImAgMnP
vajíčka	vajíčko	k1gNnPc4
do	do	k7c2
kruhových	kruhový	k2eAgNnPc2d1
hnízd	hnízdo	k1gNnPc2
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
do	do	k7c2
doby	doba	k1gFnSc2
vylíhnutí	vylíhnutí	k1gNnSc2
chránili	chránit	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
obří	obří	k2eAgFnSc1d1
dinosauří	dinosauří	k2eAgNnSc4d1
vejce	vejce	k1gNnSc4
jsou	být	k5eAaImIp3nP
známá	známá	k1gFnSc1
ze	z	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
i	i	k8xC
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
patřila	patřit	k5eAaImAgFnS
největší	veliký	k2eAgFnSc1d3
známá	známá	k1gFnSc1
vajíčka	vajíčko	k1gNnSc2
sauropodnímu	sauropodní	k2eAgMnSc3d1
dinosaurovi	dinosaurus	k1gMnSc3
rodu	rod	k1gInSc2
Hypselosaurus	Hypselosaurus	k1gInSc1
-	-	kIx~
tato	tento	k3xDgNnPc1
kulatá	kulatý	k2eAgNnPc1d1
vejce	vejce	k1gNnPc1
mají	mít	k5eAaImIp3nP
rozměr	rozměr	k1gInSc4
fotbalového	fotbalový	k2eAgInSc2d1
míče	míč	k1gInSc2
(	(	kIx(
<g/>
průměr	průměr	k1gInSc1
kolem	kolem	k7c2
30	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největší	veliký	k2eAgFnSc1d3
dosud	dosud	k6eAd1
známá	známý	k2eAgFnSc1d1
snůška	snůška	k1gFnSc1
vajec	vejce	k1gNnPc2
druhohorního	druhohorní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
z	z	k7c2
Číny	Čína	k1gFnSc2
čítala	čítat	k5eAaImAgFnS
77	#num#	k4
exemplářů	exemplář	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
dinosauřích	dinosauří	k2eAgNnPc2d1
vajec	vejce	k1gNnPc2
byla	být	k5eAaImAgFnS
potvrzena	potvrzen	k2eAgFnSc1d1
přítomnost	přítomnost	k1gFnSc1
barev	barva	k1gFnPc2
na	na	k7c6
skořápce	skořápka	k1gFnSc6
<g/>
,	,	kIx,
podobných	podobný	k2eAgFnPc2d1
jako	jako	k9
u	u	k7c2
vajec	vejce	k1gNnPc2
současných	současný	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
fosilních	fosilní	k2eAgNnPc6d1
vejcích	vejce	k1gNnPc6
titanosaurních	titanosaurní	k2eAgInPc2d1
sauropodů	sauropod	k1gInPc2
<g/>
,	,	kIx,
starých	starý	k2eAgInPc2d1
80	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
objeveny	objevit	k5eAaPmNgFnP
stopy	stopa	k1gFnPc1
po	po	k7c6
původních	původní	k2eAgFnPc6d1
aminokyselinách	aminokyselina	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Nejmenší	malý	k2eAgFnSc1d3
známá	známý	k2eAgFnSc1d1
fosilní	fosilní	k2eAgNnPc1d1
dinosauří	dinosauří	k2eAgNnPc1d1
vajíčka	vajíčko	k1gNnPc1
byla	být	k5eAaImAgNnP
objevena	objevit	k5eAaPmNgNnP
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
v	v	k7c6
sedimentech	sediment	k1gInPc6
starých	starý	k2eAgFnPc2d1
asi	asi	k9
110	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měří	měřit	k5eAaImIp3nS
pouze	pouze	k6eAd1
2,5	2,5	k4
x	x	k?
4	#num#	k4
cm	cm	kA
a	a	k8xC
vážila	vážit	k5eAaImAgFnS
zhruba	zhruba	k6eAd1
9,9	9,9	k4
gramu	gram	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Fosilní	fosilní	k2eAgNnPc1d1
vajíčka	vajíčko	k1gNnPc1
některých	některý	k3yIgFnPc2
skupin	skupina	k1gFnPc2
dinosaurů	dinosaurus	k1gMnPc2
byla	být	k5eAaImAgFnS
pravděpodobně	pravděpodobně	k6eAd1
měkká	měkký	k2eAgFnSc1d1
a	a	k8xC
kožovitá	kožovitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
pevná	pevný	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
u	u	k7c2
současných	současný	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zkamenělé	zkamenělý	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
dinosaura	dinosaurus	k1gMnSc2
z	z	k7c2
pouště	poušť	k1gFnSc2
Gobi	Gobi	k1gFnSc2
<g/>
,	,	kIx,
expozice	expozice	k1gFnSc2
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4
s	s	k7c7
fosilizovanými	fosilizovaný	k2eAgNnPc7d1
vajíčky	vajíčko	k1gNnPc7
kachnozobého	kachnozobý	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
druhu	druh	k1gInSc2
Maiasaura	Maiasaura	k1gFnSc1
peeblesorum	peeblesorum	k1gInSc4
v	v	k7c4
Museum	museum	k1gNnSc4
of	of	k?
the	the	k?
Rockies	Rockies	k1gMnSc1
<g/>
,	,	kIx,
Bozeman	Bozeman	k1gMnSc1
<g/>
,	,	kIx,
Montana	Montana	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://medicalxpress.com/news/2019-06-cholesterol-eggs-tied-cardiac-disease.html	https://medicalxpress.com/news/2019-06-cholesterol-eggs-tied-cardiac-disease.html	k1gInSc1
-	-	kIx~
Study	stud	k1gInPc1
<g/>
:	:	kIx,
Cholesterol	cholesterol	k1gInSc1
in	in	k?
eggs	eggs	k6eAd1
tied	tied	k6eAd1
to	ten	k3xDgNnSc1
cardiac	cardiac	k6eAd1
disease	disease	k6eAd1
<g/>
,	,	kIx,
death	death	k1gInSc1
<g/>
↑	↑	k?
http://www.biolib.cz/cz/glossaryterm/id637/	http://www.biolib.cz/cz/glossaryterm/id637/	k4
<g/>
↑	↑	k?
Zbarvení	zbarvení	k1gNnPc2
vajec	vejce	k1gNnPc2
<g/>
↑	↑	k?
Chicken	Chicken	k1gInSc1
egg	egg	k?
<g/>
,	,	kIx,
whole	whole	k1gInSc1
<g/>
,	,	kIx,
hard-boiled	hard-boiled	k1gInSc1
<g/>
,	,	kIx,
http://www.nal.usda.gov/fnic/foodcomp/search/	http://www.nal.usda.gov/fnic/foodcomp/search/	k?
Archivováno	archivován	k2eAgNnSc1d1
3	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
University	universita	k1gFnPc1
Science	Scienec	k1gMnSc2
article	articl	k1gMnSc2
on	on	k3xPp3gMnSc1
eggs	eggs	k6eAd1
and	and	k?
cholesterol	cholesterol	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unisci	Unisek	k1gMnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2001-10-29	2001-10-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
McCance	McCance	k1gFnSc1
a	a	k8xC
Widdowson	Widdowson	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
:	:	kIx,
<g/>
The	The	k1gMnSc2
Composition	Composition	k1gInSc1
of	of	k?
Foods	Foods	k1gInSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Summary	Summara	k1gFnPc1
edition	edition	k1gInSc1
<g/>
,	,	kIx,
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
Chemistry	Chemistr	k1gMnPc4
Cambridge	Cambridge	k1gFnSc2
a	a	k8xC
Food	Food	k1gInSc4
Standard	standard	k1gInSc1
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
85404	#num#	k4
<g/>
-	-	kIx~
<g/>
428	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
↑	↑	k?
http://www.kavapo.cz/nedokonalosti-vajec/	http://www.kavapo.cz/nedokonalosti-vajec/	k?
<g/>
↑	↑	k?
http://www.drubezarnapribor.cz/znaceni-vajec/	http://www.drubezarnapribor.cz/znaceni-vajec/	k?
<g/>
↑	↑	k?
http://www.osel.cz/10497-nejvetsi-vejce-vsech-dob.html	http://www.osel.cz/10497-nejvetsi-vejce-vsech-dob.html	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauří	dinosauří	k2eAgFnPc4d1
vejce	vejce	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://veda.instory.cz/1419-nejvetsi-vajicko-vsech-dob-patrilo-jednomu-podivnemu-dinosaurovi.html	https://veda.instory.cz/1419-nejvetsi-vajicko-vsech-dob-patrilo-jednomu-podivnemu-dinosaurovi.html	k1gMnSc1
<g/>
↑	↑	k?
Shukang	Shukang	k1gMnSc1
Zhang	Zhang	k1gMnSc1
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
dinosaur	dinosaur	k1gMnSc1
egg	egg	k?
material	material	k1gMnSc1
from	from	k1gMnSc1
Yunxian	Yunxian	k1gMnSc1
<g/>
,	,	kIx,
Hubei	Hube	k1gMnPc1
Province	province	k1gFnSc2
<g/>
,	,	kIx,
China	China	k1gFnSc1
resolves	resolves	k1gInSc1
the	the	k?
classification	classification	k1gInSc1
of	of	k?
dendroolithid	dendroolithid	k1gInSc1
eggs	eggs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Acta	Act	k2eAgFnSc1d1
Palaeontologica	Palaeontologic	k2eAgFnSc1d1
Polonica	Polonica	k1gFnSc1
(	(	kIx(
<g/>
in	in	k?
press	press	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.4202/app.00523.2018	https://doi.org/10.4202/app.00523.2018	k4
<g/>
↑	↑	k?
Jasmina	Jasmin	k2eAgFnSc1d1
Wiemann	Wiemann	k1gInSc1
<g/>
,	,	kIx,
Tzu-Ruei	Tzu-Ruei	k1gNnSc1
Yang	Yanga	k1gFnPc2
&	&	k?
Mark	Mark	k1gMnSc1
A.	A.	kA
Norell	Norell	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaur	Dinosaur	k1gMnSc1
egg	egg	k?
colour	colour	k1gMnSc1
had	had	k1gMnSc1
a	a	k8xC
single	singl	k1gInSc5
evolutionary	evolutionar	k1gMnPc4
origin	origin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1038/s41586-018-0646-5	https://doi.org/10.1038/s41586-018-0646-5	k4
<g/>
↑	↑	k?
https://www.nationalgeographic.com/science/2018/11/news-eggshells-fossils-maniraptorans-birds-evolution/	https://www.nationalgeographic.com/science/2018/11/news-eggshells-fossils-maniraptorans-birds-evolution/	k4
<g/>
↑	↑	k?
Evan	Evan	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Saitta	Saitta	k1gMnSc1
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Non-avian	Non-avian	k1gMnSc1
dinosaur	dinosaur	k1gMnSc1
eggshell	eggshell	k1gMnSc1
calcite	calcit	k1gInSc5
contains	containsa	k1gFnPc2
ancient	ancient	k1gInSc1
<g/>
,	,	kIx,
endogenous	endogenous	k1gInSc1
amino	amino	k6eAd1
acids	acids	k6eAd1
<g/>
.	.	kIx.
bioRxiv	bioRxiv	k6eAd1
2	#num#	k4
<g/>
020.06.02.129	020.06.02.129	k4
<g/>
999	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1101/2020.06.02.129999.	https://doi.org/10.1101/2020.06.02.129999.	k4
<g/>
↑	↑	k?
https://english.kyodonews.net/news/2020/06/eaf6cb9c2911-worlds-smallest-dinosaur-egg-fossil-discovered-in-japan.html	https://english.kyodonews.net/news/2020/06/eaf6cb9c2911-worlds-smallest-dinosaur-egg-fossil-discovered-in-japan.html	k1gMnSc1
<g/>
↑	↑	k?
https://phys.org/news/2020-06-tiny-japanese-dinosaur-eggs-unscramble.html	https://phys.org/news/2020-06-tiny-japanese-dinosaur-eggs-unscramble.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.osel.cz/11237-prvni-dinosauri-kladli-mekka-vejce.html	https://www.osel.cz/11237-prvni-dinosauri-kladli-mekka-vejce.html	k1gMnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
VESELOVSKÝ	Veselovský	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecná	obecný	k2eAgFnSc1d1
ornitologie	ornitologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
857	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
250	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠATAVA	ŠATAVA	kA
<g/>
,	,	kIx,
M.	M.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chov	chov	k1gInSc1
drůbeže	drůbež	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SZN	SZN	kA
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
</s>
<s>
STURKIE	STURKIE	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
D.	D.	kA
Avian	Avian	k1gMnSc1
Physiology	Physiolog	k1gMnPc7
<g/>
.	.	kIx.
5	#num#	k4
<g/>
th	th	k?
Ed	Ed	k1gMnSc1
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Elsevier	Elsevier	k1gMnSc1
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
704	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
747605	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KOŽUŠNÍK	KOŽUŠNÍK	kA
<g/>
,	,	kIx,
Z.	Z.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drůbež	drůbež	k1gFnSc1
–	–	k?
zdravotní	zdravotní	k2eAgFnSc1d1
problematika	problematika	k1gFnSc1
velkochovů	velkochov	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SZN	SZN	kA
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
.	.	kIx.
216	#num#	k4
s.	s.	k?
</s>
<s>
Daniel	Daniel	k1gMnSc1
G.	G.	kA
Blackburn	Blackburn	k1gMnSc1
&	&	k?
James	James	k1gMnSc1
R.	R.	kA
Stewart	Stewart	k1gMnSc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morphological	Morphological	k1gMnSc1
Research	Research	k1gMnSc1
on	on	k3xPp3gMnSc1
Amniote	Amniot	k1gInSc5
Eggs	Eggs	k1gInSc4
and	and	k?
Embryos	Embryos	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gFnSc1
Introduction	Introduction	k1gInSc1
and	and	k?
Historical	Historical	k1gFnSc2
Retrospective	Retrospectiv	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Morphology	Morpholog	k1gMnPc7
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1002/jmor.21320	https://doi.org/10.1002/jmor.21320	k4
</s>
<s>
Gautron	Gautron	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avian	Avian	k1gMnSc1
eggshell	eggshelnout	k5eAaPmAgMnS
biomineralization	biomineralization	k1gInSc4
<g/>
:	:	kIx,
an	an	k?
update	update	k1gInSc1
on	on	k3xPp3gMnSc1
its	its	k?
structure	structur	k1gMnSc5
<g/>
,	,	kIx,
mineralogy	mineralog	k1gMnPc4
and	and	k?
protein	protein	k1gInSc1
tool	tool	k1gMnSc1
kit	kit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BMC	BMC	kA
Molecular	Molecular	k1gMnSc1
and	and	k?
Cell	cello	k1gNnPc2
Biology	biolog	k1gMnPc7
<g/>
.	.	kIx.
22	#num#	k4
<g/>
,	,	kIx,
Article	Article	k1gFnSc1
number	number	k1gMnSc1
<g/>
:	:	kIx,
11	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1186/s12860-021-00350-0	https://doi.org/10.1186/s12860-021-00350-0	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kolumbovo	Kolumbův	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
</s>
<s>
Kraslice	kraslice	k1gFnSc1
</s>
<s>
Kráječ	kráječ	k1gInSc1
vajec	vejce	k1gNnPc2
</s>
<s>
Slepice	slepice	k1gFnPc1
nebo	nebo	k8xC
vejce	vejce	k1gNnPc1
<g/>
?	?	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vejce	vejce	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
vejce	vejce	k1gNnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Gastronomie	gastronomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4013700-4	4013700-4	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
8643	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5833	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5833	#num#	k4
</s>
