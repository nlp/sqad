<s>
Nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
není	být	k5eNaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
praktický	praktický	k2eAgInSc1d1	praktický
prostředek	prostředek	k1gInSc1	prostředek
(	(	kIx(	(
<g/>
avšak	avšak	k8xC	avšak
používaný	používaný	k2eAgInSc1d1	používaný
také	také	k6eAd1	také
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
nomenklaturou	nomenklatura	k1gFnSc7	nomenklatura
rozumí	rozumět	k5eAaImIp3nS	rozumět
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
neměnná	neměnný	k2eAgFnSc1d1	neměnná
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
latinsko-anglická	latinskonglický	k2eAgFnSc1d1	latinsko-anglický
soustava	soustava	k1gFnSc1	soustava
termínů	termín	k1gInPc2	termín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
terminologie	terminologie	k1gFnSc2	terminologie
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pouze	pouze	k6eAd1	pouze
názvy	název	k1gInPc7	název
povahy	povaha	k1gFnSc2	povaha
substantivní	substantivní	k2eAgMnPc1d1	substantivní
pro	pro	k7c4	pro
objekty	objekt	k1gInPc4	objekt
zkoumání	zkoumání	k1gNnSc2	zkoumání
(	(	kIx(	(
<g/>
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
třídy	třída	k1gFnPc1	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
např.	např.	kA	např.
slovesa	sloveso	k1gNnSc2	sloveso
<g/>
,	,	kIx,	,
adjektiva	adjektivum	k1gNnSc2	adjektivum
<g/>
,	,	kIx,	,
názvy	název	k1gInPc7	název
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
binominální	binominální	k2eAgFnSc1d1	binominální
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
nebo	nebo	k8xC	nebo
chemická	chemický	k2eAgFnSc1d1	chemická
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
<g/>
.	.	kIx.	.
</s>
<s>
Nadřazeným	nadřazený	k2eAgInSc7d1	nadřazený
termínem	termín	k1gInSc7	termín
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
systematika	systematik	k1gMnSc2	systematik
<g/>
.	.	kIx.	.
</s>
<s>
Binomická	binomický	k2eAgFnSc1d1	binomická
nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
</s>
