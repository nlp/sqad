<s>
Ústav	ústav	k1gInSc1
Českého	český	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
</s>
<s>
Ústav	ústav	k1gInSc1
Českého	český	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
zkr.	zkr.	kA
ÚČNK	ÚČNK	kA
<g/>
,	,	kIx,
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
a	a	k8xC
spravuje	spravovat	k5eAaImIp3nS
Český	český	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
korpus	korpus	k1gInSc1
a	a	k8xC
vedle	vedle	k7c2
vědy	věda	k1gFnSc2
a	a	k8xC
výzkumu	výzkum	k1gInSc2
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
též	též	k6eAd1
výuce	výuka	k1gFnSc3
a	a	k8xC
podpoře	podpora	k1gFnSc6
uživatelů	uživatel	k1gMnPc2
Českého	český	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
a	a	k8xC
paralelních	paralelní	k2eAgInPc2d1
korpusů	korpus	k1gInPc2
řady	řada	k1gFnSc2
InterCorp	InterCorp	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Ředitelem	ředitel	k1gMnSc7
ústavu	ústav	k1gInSc2
je	být	k5eAaImIp3nS
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Křen	křen	k1gInSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
<s>
Ústav	ústav	k1gInSc1
je	být	k5eAaImIp3nS
rozčleněn	rozčlenit	k5eAaPmNgInS
na	na	k7c4
následující	následující	k2eAgFnPc4d1
sekce	sekce	k1gFnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lingvistická	lingvistický	k2eAgFnSc1d1
(	(	kIx(
<g/>
vedoucí	vedoucí	k2eAgFnSc1d1
Mgr.	Mgr.	kA
Anna	Anna	k1gFnSc1
Čermáková	Čermáková	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
</s>
<s>
Komputační	Komputační	k2eAgMnSc1d1
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnSc1
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Vondřička	Vondřička	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
</s>
<s>
Mluvené	mluvený	k2eAgInPc1d1
korpusy	korpus	k1gInPc1
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnPc1
PhDr.	PhDr.	kA
Marie	Marie	k1gFnSc1
Kopřivová	Kopřivová	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
</s>
<s>
Diachronní	diachronní	k2eAgInPc1d1
korpusy	korpus	k1gInPc1
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnSc1
Mgr.	Mgr.	kA
Martin	Martin	k1gMnSc1
Stluka	Stluk	k1gMnSc2
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
</s>
<s>
Lingvistická	lingvistický	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
a	a	k8xC
anotace	anotace	k1gFnSc1
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnSc1
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Paralelní	paralelní	k2eAgInPc1d1
korpusy	korpus	k1gInPc1
(	(	kIx(
<g/>
vedoucí	vedoucí	k1gMnSc1
Ing.	ing.	kA
Alexandr	Alexandr	k1gMnSc1
Rosen	rosen	k2eAgMnSc1d1
<g/>
,	,	kIx,
PhD	PhD	k1gFnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1
posláním	poslání	k1gNnSc7
ÚČNK	ÚČNK	kA
je	být	k5eAaImIp3nS
kontinuální	kontinuální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
budování	budování	k1gNnSc4
jazykových	jazykový	k2eAgInPc2d1
korpusů	korpus	k1gInPc2
různých	různý	k2eAgInPc2d1
typů	typ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představují	představovat	k5eAaImIp3nP
reprezentativní	reprezentativní	k2eAgFnPc4d1
lingvisticky	lingvisticky	k6eAd1
zpracované	zpracovaný	k2eAgFnPc4d1
datové	datový	k2eAgFnPc4d1
základny	základna	k1gFnPc4
pro	pro	k7c4
empirický	empirický	k2eAgInSc4d1
a	a	k8xC
exaktní	exaktní	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
jde	jít	k5eAaImIp3nS
především	především	k9
o	o	k7c4
korpusy	korpus	k1gInPc4
zachycující	zachycující	k2eAgFnSc4d1
češtinu	čeština	k1gFnSc4
v	v	k7c6
jejím	její	k3xOp3gInSc6
současném	současný	k2eAgInSc6d1
stavu	stav	k1gInSc6
(	(	kIx(
<g/>
synchronní	synchronní	k2eAgInPc1d1
korpusy	korpus	k1gInPc1
psaného	psaný	k2eAgMnSc2d1
a	a	k8xC
mluveného	mluvený	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
jejím	její	k3xOp3gInSc6
historickém	historický	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
(	(	kIx(
<g/>
diachronní	diachronní	k2eAgInPc1d1
korpusy	korpus	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
překladovém	překladový	k2eAgNnSc6d1
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
(	(	kIx(
<g/>
paralelní	paralelní	k2eAgInPc1d1
korpusy	korpus	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
budováním	budování	k1gNnSc7
korpusů	korpus	k1gInPc2
úzce	úzko	k6eAd1
souvisí	souviset	k5eAaImIp3nS
též	též	k9
bezplatná	bezplatný	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
služba	služba	k1gFnSc1
poskytování	poskytování	k1gNnSc2
internetového	internetový	k2eAgInSc2d1
uživatelského	uživatelský	k2eAgInSc2d1
přístupu	přístup	k1gInSc2
ke	k	k7c3
všem	všecek	k3xTgInPc3
korpusům	korpus	k1gInPc3
pomocí	pomoc	k1gFnSc7
specializovaných	specializovaný	k2eAgNnPc2d1
rozhraní	rozhraní	k1gNnPc2
a	a	k8xC
nástrojů	nástroj	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
nástroj	nástroj	k1gInSc1
pro	pro	k7c4
porovnání	porovnání	k1gNnSc4
variant	varianta	k1gFnPc2
SyD	SyD	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
podpora	podpora	k1gFnSc1
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Řady	řada	k1gFnPc1
korpusů	korpus	k1gInPc2
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
projekt	projekt	k1gInSc1
ČNK	ČNK	kA
spravuje	spravovat	k5eAaImIp3nS
následující	následující	k2eAgFnPc4d1
řady	řada	k1gFnPc4
korpusů	korpus	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Korpusy	korpus	k1gInPc1
psané	psaný	k2eAgFnSc2d1
současné	současný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
o	o	k7c6
celkovém	celkový	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
1300	#num#	k4
milionů	milion	k4xCgInPc2
textových	textový	k2eAgInPc2d1
slov	slovo	k1gNnPc2
jsou	být	k5eAaImIp3nP
referenční	referenční	k2eAgInPc1d1
(	(	kIx(
<g/>
tj.	tj.	kA
neměnné	neměnný	k2eAgFnPc1d1,k2eNgFnPc1d1
<g/>
,	,	kIx,
lze	lze	k6eAd1
na	na	k7c4
ně	on	k3xPp3gMnPc4
odkazovat	odkazovat	k5eAaImF
a	a	k8xC
opakované	opakovaný	k2eAgInPc1d1
dotazy	dotaz	k1gInPc1
dají	dát	k5eAaPmIp3nP
tytéž	týž	k3xTgInPc1
výsledky	výsledek	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
reprezentativní	reprezentativní	k2eAgNnSc1d1
(	(	kIx(
<g/>
vyváženě	vyváženě	k6eAd1
pokrývají	pokrývat	k5eAaImIp3nP
celou	celý	k2eAgFnSc4d1
šíři	šíře	k1gFnSc4
žánrů	žánr	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
český	český	k2eAgMnSc1d1
čtenář	čtenář	k1gMnSc1
recipuje	recipovat	k5eAaBmIp3nS
skrze	skrze	k?
tištěné	tištěný	k2eAgInPc4d1
psané	psaný	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
reprezentují	reprezentovat	k5eAaImIp3nP
tak	tak	k6eAd1
úzus	úzus	k1gInSc4
tištěné	tištěný	k2eAgFnSc2d1
psané	psaný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
SYN2000	SYN2000	k4
-	-	kIx~
100	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
<g/>
žánrově	žánrově	k6eAd1
vyvážený	vyvážený	k2eAgInSc4d1
korpus	korpus	k1gInSc4
<g/>
,	,	kIx,
převažují	převažovat	k5eAaImIp3nP
texty	text	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
1990	#num#	k4
-	-	kIx~
1999	#num#	k4
</s>
<s>
SYN2005	SYN2005	k4
-	-	kIx~
100	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
žánrově	žánrově	k6eAd1
vyvážený	vyvážený	k2eAgInSc4d1
korpus	korpus	k1gInSc4
<g/>
,	,	kIx,
převažují	převažovat	k5eAaImIp3nP
texty	text	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
2000	#num#	k4
-	-	kIx~
2004	#num#	k4
</s>
<s>
SYN2006PUB	SYN2006PUB	k4
-	-	kIx~
300	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
korpus	korpus	k1gInSc1
publicistických	publicistický	k2eAgInPc2d1
textů	text	k1gInPc2
z	z	k7c2
let	léto	k1gNnPc2
1989	#num#	k4
-	-	kIx~
2004	#num#	k4
</s>
<s>
SYN2009PUB	SYN2009PUB	k4
-	-	kIx~
700	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
korpus	korpus	k1gInSc1
publicistických	publicistický	k2eAgInPc2d1
textů	text	k1gInPc2
z	z	k7c2
let	léto	k1gNnPc2
1995	#num#	k4
-	-	kIx~
2007	#num#	k4
</s>
<s>
SYN2010	SYN2010	k4
-	-	kIx~
100	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
žánrově	žánrově	k6eAd1
vyvážený	vyvážený	k2eAgInSc4d1
korpus	korpus	k1gInSc4
<g/>
,	,	kIx,
převažují	převažovat	k5eAaImIp3nP
texty	text	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
2005	#num#	k4
-	-	kIx~
2009	#num#	k4
</s>
<s>
Korpusy	korpus	k1gInPc1
mluvené	mluvený	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
o	o	k7c6
celkovém	celkový	k2eAgInSc6d1
objemu	objem	k1gInSc6
cca	cca	kA
3	#num#	k4
miliony	milion	k4xCgInPc7
textových	textový	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvené	mluvený	k2eAgInPc1d1
korpusy	korpus	k1gInPc1
řady	řada	k1gFnSc2
ORAL	orat	k5eAaImAgMnS
zachycují	zachycovat	k5eAaImIp3nP
autentickou	autentický	k2eAgFnSc4d1
mluvu	mluva	k1gFnSc4
v	v	k7c6
neformálních	formální	k2eNgFnPc6d1
situacích	situace	k1gFnPc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
doplněny	doplněn	k2eAgInPc1d1
základními	základní	k2eAgInPc7d1
sociolingvistickými	sociolingvistický	k2eAgInPc7d1
údaji	údaj	k1gInPc7
o	o	k7c6
mluvčích	mluvčí	k1gFnPc6
<g/>
,	,	kIx,
korpus	korpus	k1gInSc1
ORAL	orat	k5eAaImAgInS
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
pokrývající	pokrývající	k2eAgNnSc4d1
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
navíc	navíc	k6eAd1
vyvážen	vyvážen	k2eAgInSc1d1
v	v	k7c6
hlavních	hlavní	k2eAgFnPc6d1
sociolingvistických	sociolingvistický	k2eAgFnPc6d1
kategoriích	kategorie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravované	připravovaný	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
zpřístupní	zpřístupnit	k5eAaPmIp3nS
i	i	k9
anonymizované	anonymizovaný	k2eAgFnPc4d1
zvukové	zvukový	k2eAgFnPc4d1
nahrávky	nahrávka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1
mluvený	mluvený	k2eAgInSc1d1
korpus	korpus	k1gInSc1
</s>
<s>
Brněnský	brněnský	k2eAgInSc1d1
mluvený	mluvený	k2eAgInSc1d1
korpus	korpus	k1gInSc1
</s>
<s>
Oral	orat	k5eAaImAgMnS
<g/>
2006	#num#	k4
</s>
<s>
Oral	orat	k5eAaImAgMnS
<g/>
2008	#num#	k4
</s>
<s>
Korpus	korpus	k1gInSc1
diachronní	diachronní	k2eAgFnSc2d1
(	(	kIx(
<g/>
DIAKORP	DIAKORP	kA
<g/>
)	)	kIx)
</s>
<s>
Korpusy	korpus	k1gInPc1
paralelní	paralelní	k2eAgInPc1d1
(	(	kIx(
<g/>
InterCorp	InterCorp	k1gInSc1
<g/>
)	)	kIx)
zahrnující	zahrnující	k2eAgInPc1d1
překladové	překladový	k2eAgInPc1d1
ekvivalenty	ekvivalent	k1gInPc1
českých	český	k2eAgInPc2d1
textů	text	k1gInPc2
do	do	k7c2
více	hodně	k6eAd2
než	než	k8xS
dvaceti	dvacet	k4xCc2
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Publikace	publikace	k1gFnSc1
</s>
<s>
ÚČNK	ÚČNK	kA
dlouhodobě	dlouhodobě	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
Nakladatelstvím	nakladatelství	k1gNnSc7
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
mj.	mj.	kA
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
řadu	řada	k1gFnSc4
Studie	studie	k1gFnSc1
z	z	k7c2
korpusové	korpusový	k2eAgFnSc2d1
lingvistiky	lingvistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
vyšlo	vyjít	k5eAaPmAgNnS
<g/>
:	:	kIx,
</s>
<s>
Frekvenční	frekvenční	k2eAgInSc1d1
slovník	slovník	k1gInSc1
češtiny	čeština	k1gFnSc2
(	(	kIx(
<g/>
NLN	NLN	kA
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Frekvenční	frekvenční	k2eAgInSc1d1
slovník	slovník	k1gInSc1
mluvené	mluvený	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
(	(	kIx(
<g/>
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A	a	k9
Frequency	Frequenc	k2eAgInPc1d1
Dictionary	Dictionar	k1gInPc1
of	of	k?
Czech	Czech	k1gInSc1
<g/>
:	:	kIx,
Core	Core	k1gNnSc1
Vocabulary	Vocabulara	k1gFnSc2
for	forum	k1gNnPc2
Learners	Learners	k1gInSc1
(	(	kIx(
<g/>
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mluvnice	mluvnice	k1gFnSc1
současné	současný	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
(	(	kIx(
<g/>
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ke	k	k7c3
stažení	stažení	k1gNnSc3
jsou	být	k5eAaImIp3nP
zpřístupněny	zpřístupnit	k5eAaPmNgInP
abecední	abecední	k2eAgInPc1d1
a	a	k8xC
retrográdní	retrográdní	k2eAgInPc1d1
slovníky	slovník	k1gInPc1
<g/>
,	,	kIx,
založené	založený	k2eAgInPc1d1
na	na	k7c6
korpusech	korpus	k1gInPc6
řady	řada	k1gFnSc2
SYN	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ústav	ústav	k1gInSc1
Českého	český	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
9	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidelnou	pravidelný	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
vyvíjí	vyvíjet	k5eAaImIp3nS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1996	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
sestaven	sestaven	k2eAgInSc1d1
kolektiv	kolektiv	k1gInSc1
stálých	stálý	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2000	#num#	k4
byl	být	k5eAaImAgInS
zveřejněn	zveřejnit	k5eAaPmNgInS
první	první	k4xOgInSc1
stomilionový	stomilionový	k2eAgInSc1d1
žánrově	žánrově	k6eAd1
vyvážený	vyvážený	k2eAgInSc1d1
korpus	korpus	k1gInSc1
<g/>
,	,	kIx,
SYN	syn	k1gMnSc1
<g/>
2000	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Ústav	ústav	k1gInSc1
Českého	český	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
<g/>
↑	↑	k?
Dostupné	dostupný	k2eAgInPc4d1
korpusy	korpus	k1gInPc4
<g/>
.	.	kIx.
www.korpus.cz	www.korpus.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
přehled	přehled	k1gInSc1
publikací	publikace	k1gFnPc2
<g/>
.	.	kIx.
ucnk	ucnk	k1gMnSc1
<g/>
.	.	kIx.
<g/>
ff	ff	kA
<g/>
.	.	kIx.
<g/>
cuni	cuni	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ikaros	Ikaros	k1gMnSc1
–	–	k?
Český	český	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
korpus	korpus	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
internetový	internetový	k2eAgInSc1d1
portál	portál	k1gInSc1
</s>
<s>
Veřejný	veřejný	k2eAgInSc1d1
přístup	přístup	k1gInSc1
ke	k	k7c3
korpusu	korpus	k1gInSc3
SYN2010	SYN2010	k1gFnSc2
(	(	kIx(
<g/>
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
registrace	registrace	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
SyD	SyD	k?
-	-	kIx~
nástroj	nástroj	k1gInSc1
pro	pro	k7c4
porovnávání	porovnávání	k1gNnSc4
variant	varianta	k1gFnPc2
v	v	k7c6
češtině	čeština	k1gFnSc6
na	na	k7c6
základě	základ	k1gInSc6
synchronních	synchronní	k2eAgNnPc2d1
<g/>
,	,	kIx,
diachronních	diachronní	k2eAgNnPc2d1
a	a	k8xC
mluvených	mluvený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
ČNK	ČNK	kA
(	(	kIx(
<g/>
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
registrace	registrace	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
InterCorp	InterCorp	k1gInSc1
-	-	kIx~
paralelní	paralelní	k2eAgInPc1d1
překladové	překladový	k2eAgInPc1d1
korpusy	korpus	k1gInPc1
více	hodně	k6eAd2
než	než	k8xS
dvaceti	dvacet	k4xCc2
jazyků	jazyk	k1gInPc2
</s>
<s>
Blog	Blog	k1gInSc1
korpusového	korpusový	k2eAgMnSc2d1
lingvisty	lingvista	k1gMnSc2
-	-	kIx~
V.	V.	kA
Cvrček	Cvrček	k1gMnSc1
na	na	k7c6
Aktuálně	aktuálně	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2005122117	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
137569926	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2005122117	#num#	k4
</s>
