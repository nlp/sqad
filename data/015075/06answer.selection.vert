<s>
Ústav	ústav	k1gInSc1
Českého	český	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
zkr.	zkr.	kA
ÚČNK	ÚČNK	kA
<g/>
,	,	kIx,
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
a	a	k8xC
spravuje	spravovat	k5eAaImIp3nS
Český	český	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
korpus	korpus	k1gInSc1
a	a	k8xC
vedle	vedle	k7c2
vědy	věda	k1gFnSc2
a	a	k8xC
výzkumu	výzkum	k1gInSc2
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
též	též	k6eAd1
výuce	výuka	k1gFnSc3
a	a	k8xC
podpoře	podpora	k1gFnSc6
uživatelů	uživatel	k1gMnPc2
Českého	český	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
a	a	k8xC
paralelních	paralelní	k2eAgInPc2d1
korpusů	korpus	k1gInPc2
řady	řada	k1gFnSc2
InterCorp	InterCorp	k1gMnSc1
<g/>
.	.	kIx.
</s>