<s>
Jefferson	Jefferson	k1gInSc1	Jefferson
City	City	k1gFnSc2	City
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Missouri	Missouri	k1gNnSc2	Missouri
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
okresní	okresní	k2eAgNnSc4d1	okresní
město	město	k1gNnSc4	město
Cole	cola	k1gFnSc3	cola
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
Thomasovi	Thomas	k1gMnSc6	Thomas
Jeffersonovi	Jefferson	k1gMnSc6	Jefferson
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgMnSc6	třetí
prezidentu	prezident	k1gMnSc3	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
orientováno	orientovat	k5eAaBmNgNnS	orientovat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
Missouri	Missouri	k1gFnSc2	Missouri
<g/>
,	,	kIx,	,
blízko	blízko	k6eAd1	blízko
geografickému	geografický	k2eAgInSc3d1	geografický
středu	střed	k1gInSc3	střed
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zabírá	zabírat	k5eAaImIp3nS	zabírat
celkovou	celkový	k2eAgFnSc4d1	celková
plochu	plocha	k1gFnSc4	plocha
73,2	[number]	k4	73,2
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
70,6	[number]	k4	70,6
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
2,6	[number]	k4	2,6
km2	km2	k4	km2
(	(	kIx(	(
<g/>
3,61	[number]	k4	3,61
%	%	kIx~	%
<g/>
)	)	kIx)	)
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
vymezeno	vymezit	k5eAaPmNgNnS	vymezit
teritorium	teritorium	k1gNnSc4	teritorium
Missouri	Missouri	k1gFnPc2	Missouri
<g/>
,	,	kIx,	,
sídlem	sídlo	k1gNnSc7	sídlo
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
St.	st.	kA	st.
Louis	louis	k1gInSc2	louis
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
sloužilo	sloužit	k5eAaImAgNnS	sloužit
Saint	Saint	k1gInSc4	Saint
Charles	Charles	k1gMnSc1	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Jefferson	Jefferson	k1gNnSc1	Jefferson
City	City	k1gFnSc2	City
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
novým	nový	k2eAgNnSc7d1	nové
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
43	[number]	k4	43
079	[number]	k4	079
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
sídlilo	sídlit	k5eAaImAgNnS	sídlit
v	v	k7c4	v
Jefferson	Jefferson	k1gNnSc4	Jefferson
City	city	k1gNnSc1	city
39	[number]	k4	39
636	[number]	k4	636
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
15	[number]	k4	15
794	[number]	k4	794
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
9	[number]	k4	9
207	[number]	k4	207
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
561,6	[number]	k4	561,6
km2	km2	k4	km2
<g/>
.	.	kIx.	.
78,0	[number]	k4	78,0
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
16,9	[number]	k4	16,9
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
1,8	[number]	k4	1,8
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
0,8	[number]	k4	0,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,2	[number]	k4	2,2
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
2,6	[number]	k4	2,6
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<	<	kIx(	<
<g/>
18	[number]	k4	18
let	léto	k1gNnPc2	léto
-	-	kIx~	-
20,9	[number]	k4	20,9
%	%	kIx~	%
<	<	kIx(	<
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
let	léto	k1gNnPc2	léto
-	-	kIx~	-
11,0	[number]	k4	11,0
%	%	kIx~	%
<	<	kIx(	<
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
let	léto	k1gNnPc2	léto
-	-	kIx~	-
32,1	[number]	k4	32,1
%	%	kIx~	%
<	<	kIx(	<
<g/>
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
22,0	[number]	k4	22,0
%	%	kIx~	%
>	>	kIx)	>
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
-	-	kIx~	-
14,0	[number]	k4	14,0
%	%	kIx~	%
průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
-	-	kIx~	-
36	[number]	k4	36
let	léto	k1gNnPc2	léto
Münchberg	Münchberg	k1gInSc1	Münchberg
<g/>
,	,	kIx,	,
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Jack	Jacko	k1gNnPc2	Jacko
Kilby	Kilba	k1gFnSc2	Kilba
(	(	kIx(	(
<g/>
*	*	kIx~	*
1923	[number]	k4	1923
-	-	kIx~	-
†	†	k?	†
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Christian	Christian	k1gMnSc1	Christian
Cantwell	Cantwell	k1gMnSc1	Cantwell
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atlet-koulař	atletoulař	k1gMnSc1	atlet-koulař
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jefferson	Jefferson	k1gNnSc1	Jefferson
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gFnSc1	Missouri
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jefferson	Jeffersona	k1gFnPc2	Jeffersona
City	City	k1gFnSc2	City
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
