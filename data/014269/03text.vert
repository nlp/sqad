<s>
Kjúdó	Kjúdó	k?
</s>
<s>
Mistr	mistr	k1gMnSc1
Jošimiči	Jošimič	k1gMnPc1
Genširo	Genširo	k1gNnSc4
Inagaki	Inagaki	k1gNnSc2
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
dan	dan	k?
<g/>
,	,	kIx,
hlava	hlava	k1gFnSc1
školy	škola	k1gFnSc2
Heki	Hek	k1gFnSc2
rjú	rjú	k?
insai-ha	insai-ha	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
dódžó	dódžó	k?
Národní	národní	k2eAgFnSc2d1
technické	technický	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Cukubě	Cukuba	k1gFnSc6
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
listopad	listopad	k1gInSc4
1993	#num#	k4
</s>
<s>
Kjúdó	Kjúdó	k1gNnSc1
(	(	kIx(
<g/>
弓	弓	kIx~
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
japonské	japonský	k2eAgNnSc1d1
bojové	bojový	k2eAgNnSc1d1
umění	umění	k1gNnSc1
lukostřelby	lukostřelba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mistrům	mistr	k1gMnPc3
kjúdó	kjúdó	k?
se	se	k3xPyFc4
japonsky	japonsky	k6eAd1
říká	říkat	k5eAaImIp3nS
kjúdóka	kjúdóka	k1gFnSc1
(	(	kIx(
<g/>
弓	弓	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kjúdó	Kjúdó	k1gNnSc1
je	být	k5eAaImIp3nS
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
tradičním	tradiční	k2eAgNnSc6d1
umění	umění	k1gNnSc6
lukostřelby	lukostřelba	k1gFnSc2
<g/>
,	,	kIx,
kjúdžucu	kjúdžucu	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1
se	se	k3xPyFc4
zrodilo	zrodit	k5eAaPmAgNnS
ve	v	k7c6
společenské	společenský	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
samurajů	samuraj	k1gMnPc2
ve	v	k7c6
feudálním	feudální	k2eAgNnSc6d1
Japonsku	Japonsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Kjúdó	Kjúdó	k1gNnSc4
provozují	provozovat	k5eAaImIp3nP
tisíce	tisíc	k4xCgInPc1
lidí	člověk	k1gMnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc1
lukostřelby	lukostřelba	k1gFnSc2
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
do	do	k7c2
prehistorie	prehistorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
záznamy	záznam	k1gInPc1
se	s	k7c7
zřetelně	zřetelně	k6eAd1
vyobrazeným	vyobrazený	k2eAgInSc7d1
japonským	japonský	k2eAgInSc7d1
dlouhým	dlouhý	k2eAgInSc7d1
asymetrickým	asymetrický	k2eAgInSc7d1
lukem	luk	k1gInSc7
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
Období	období	k1gNnSc2
Jajoi	Jajo	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
500	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
–	–	k?
300	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
Společenské	společenský	k2eAgFnPc1d1
změny	změna	k1gFnPc1
a	a	k8xC
nástup	nástup	k1gInSc1
vojenské	vojenský	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
samurajů	samuraj	k1gMnPc2
k	k	k7c3
moci	moc	k1gFnSc3
na	na	k7c6
konci	konec	k1gInSc6
prvního	první	k4xOgNnSc2
tisíciletí	tisíciletí	k1gNnSc2
vyvolaly	vyvolat	k5eAaPmAgInP
potřebu	potřeba	k1gFnSc4
výuky	výuka	k1gFnSc2
lukostřelby	lukostřelba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
zrození	zrození	k1gNnSc3
prvního	první	k4xOgInSc2
stylu	styl	k1gInSc2
kjúdžucu	kjúdžucu	k5eAaPmIp1nS
takzvaného	takzvaný	k2eAgMnSc4d1
Henmi-ryū	Henmi-ryū	k1gMnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
zakladatelem	zakladatel	k1gMnSc7
byl	být	k5eAaImAgInS
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
prapředek	prapředek	k1gMnSc1
klanu	klan	k1gInSc2
Takeda	Taked	k1gMnSc4
Kijomicu	Kijomica	k1gMnSc4
Henmi	Hen	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnPc1
potomci	potomek	k1gMnPc1
později	pozdě	k6eAd2
založili	založit	k5eAaPmAgMnP
školu	škola	k1gFnSc4
Takeda	Taked	k1gMnSc2
(	(	kIx(
<g/>
Takeda-rjú	Takeda-rjú	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
jezdeckou	jezdecký	k2eAgFnSc4d1
lukostřeleckou	lukostřelecký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
Ogasawara	Ogasawar	k1gMnSc2
(	(	kIx(
<g/>
Ogasawara-rjú	Ogasawara-rjú	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potřeba	potřeba	k1gFnSc1
lukostřelců	lukostřelec	k1gMnPc2
dramaticky	dramaticky	k6eAd1
vzrostla	vzrůst	k5eAaPmAgFnS
během	během	k7c2
války	válka	k1gFnSc2
Genpei	Genpe	k1gFnSc2
(	(	kIx(
<g/>
1180	#num#	k4
<g/>
–	–	k?
<g/>
1185	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
počal	počnout	k5eAaPmAgMnS
zakladatel	zakladatel	k1gMnSc1
školy	škola	k1gFnSc2
Ogasawara	Ogasawara	k1gFnSc1
Nagakijo	Nagakijo	k1gNnSc4
Ogasawara	Ogasawar	k1gMnSc2
vyučovat	vyučovat	k5eAaImF
jezdeckou	jezdecký	k2eAgFnSc4d1
lukostřelbu	lukostřelba	k1gFnSc4
Jabusame	Jabusam	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
Sengoku	Sengok	k1gInSc2
</s>
<s>
Od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
pustošila	pustošit	k5eAaImAgFnS
Japonsko	Japonsko	k1gNnSc4
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
způsobil	způsobit	k5eAaPmAgMnS
Danjō	Danjō	k1gMnSc1
Masacugu	Masacug	k1gMnSc3
Heki	Hek	k1gMnSc3
převrat	převrat	k1gInSc1
v	v	k7c6
lukostřelbě	lukostřelba	k1gFnSc6
vytyčením	vytyčení	k1gNnSc7
nových	nový	k2eAgFnPc2d1
základních	základní	k2eAgFnPc2d1
dovedností	dovednost	k1gFnPc2
pro	pro	k7c4
pěší	pěší	k2eAgMnPc4d1
lukostřelce	lukostřelec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
hlavní	hlavní	k2eAgFnSc1d1
zásada	zásada	k1gFnSc1
zněla	znět	k5eAaImAgFnS
hi	hi	k0
<g/>
,	,	kIx,
kan	kan	k?
<g/>
,	,	kIx,
čū	čū	k?
neboli	neboli	k8xC
letět	letět	k5eAaImF
<g/>
,	,	kIx,
probodnout	probodnout	k5eAaPmF
<g/>
,	,	kIx,
střed	střed	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
učení	učení	k1gNnSc1
se	se	k3xPyFc4
rychle	rychle	k6eAd1
šířilo	šířit	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
mnoho	mnoho	k4c1
nových	nový	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
některé	některý	k3yIgInPc1
z	z	k7c2
nich	on	k3xPp3gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Heki-rjú	Heki-rjú	k1gMnSc1
čikurin-ha	čikurin-ha	k1gMnSc1
<g/>
,	,	kIx,
Heki-rjú	Heki-rjú	k1gMnSc1
sekka-ha	sekka-ha	k1gMnSc1
a	a	k8xC
Heki-rjú	Heki-rjú	k1gMnSc1
insai-ha	insai-ha	k1gMnSc1
zůstávají	zůstávat	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Jumi	Jumi	k6eAd1
při	při	k7c6
nátahu	nátah	k1gInSc6
</s>
<s>
Japonské	japonský	k2eAgFnPc1d1
tanegašimy	tanegašima	k1gFnPc1
</s>
<s>
Význam	význam	k1gInSc1
japonského	japonský	k2eAgInSc2d1
luku	luk	k1gInSc2
jumi	jum	k1gFnSc2
jako	jako	k8xS,k8xC
válečné	válečný	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
začal	začít	k5eAaPmAgInS
klesat	klesat	k5eAaImF
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1543	#num#	k4
přistáli	přistát	k5eAaImAgMnP,k5eAaPmAgMnP
Portugalci	Portugalec	k1gMnPc1
a	a	k8xC
přivezli	přivézt	k5eAaPmAgMnP
s	s	k7c7
sebou	se	k3xPyFc7
palné	palný	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
s	s	k7c7
doutnákovým	doutnákový	k2eAgInSc7d1
zámkem	zámek	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Japonci	Japonec	k1gMnPc1
začali	začít	k5eAaPmAgMnP
záhy	záhy	k6eAd1
vyrábět	vyrábět	k5eAaImF
vlastní	vlastní	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
doutnákového	doutnákový	k2eAgInSc2d1
zámku	zámek	k1gInSc2
a	a	k8xC
arkebuzy	arkebuza	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
jím	on	k3xPp3gInSc7
byly	být	k5eAaImAgFnP
vybaveny	vybavit	k5eAaPmNgFnP
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývaly	nazývat	k5eAaImAgFnP
tanegašima	tanegašima	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
stejnojmenného	stejnojmenný	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
první	první	k4xOgMnPc1
Portugalci	Portugalec	k1gMnPc1
zakotvili	zakotvit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
arkebuzy	arkebuza	k1gFnPc4
se	se	k3xPyFc4
posléze	posléze	k6eAd1
staly	stát	k5eAaPmAgFnP
spolu	spolu	k6eAd1
s	s	k7c7
kopím	kopí	k1gNnSc7
jari	jar	k1gFnSc2
oblíbenou	oblíbený	k2eAgFnSc7d1
pěchotní	pěchotní	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
a	a	k8xC
zatlačily	zatlačit	k5eAaPmAgInP
tak	tak	k9
používání	používání	k1gNnSc1
luku	luk	k1gInSc2
jumi	jum	k1gFnSc2
do	do	k7c2
pozadí	pozadí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
luky	luk	k1gInPc1
se	se	k3xPyFc4
po	po	k7c4
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
používaly	používat	k5eAaImAgInP
v	v	k7c6
boji	boj	k1gInSc6
společně	společně	k6eAd1
s	s	k7c7
arkebuzami	arkebuza	k1gFnPc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
měly	mít	k5eAaImAgFnP
větší	veliký	k2eAgInSc4d2
dosah	dosah	k1gInSc4
<g/>
,	,	kIx,
přesnost	přesnost	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
zejména	zejména	k9
30	#num#	k4
až	až	k9
40	#num#	k4
<g/>
krát	krát	k6eAd1
vyšší	vysoký	k2eAgFnSc4d2
palebnou	palebný	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
než	než	k8xS
tanegašimy	tanegašima	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
používání	používání	k1gNnSc2
arkebuz	arkebuza	k1gFnPc2
nevyžadovalo	vyžadovat	k5eNaImAgNnS
tak	tak	k6eAd1
dlouhý	dlouhý	k2eAgInSc4d1
výcvik	výcvik	k1gInSc4
jako	jako	k8xC,k8xS
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
u	u	k7c2
jumi	jum	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožnilo	umožnit	k5eAaPmAgNnS
vojsku	vojsko	k1gNnSc3
Nobunagy	Nobunaga	k1gFnSc2
Ody	Ody	k1gFnSc2
složenému	složený	k2eAgMnSc3d1
převážně	převážně	k6eAd1
z	z	k7c2
rolníků	rolník	k1gMnPc2
a	a	k8xC
vyzbrojenému	vyzbrojený	k2eAgInSc3d1
tanegašimami	tanegašima	k1gFnPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
roce	rok	k1gInSc6
1575	#num#	k4
rozdrtilo	rozdrtit	k5eAaPmAgNnS
v	v	k7c6
jediné	jediný	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
tradiční	tradiční	k2eAgFnSc4d1
samurajskou	samurajský	k2eAgFnSc4d1
kavalerii	kavalerie	k1gFnSc4
vyzbrojenou	vyzbrojený	k2eAgFnSc4d1
luky	luk	k1gInPc5
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Během	během	k7c2
období	období	k1gNnSc2
Edo	Eda	k1gMnSc5
(	(	kIx(
<g/>
1603	#num#	k4
<g/>
–	–	k?
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Japonsku	Japonsko	k1gNnSc3
vládli	vládnout	k5eAaImAgMnP
šógunové	šógun	k1gMnPc1
z	z	k7c2
rodu	rod	k1gInSc2
Tokugawa	Tokugaw	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
země	země	k1gFnSc1
izolovaná	izolovaný	k2eAgFnSc1d1
před	před	k7c7
okolním	okolní	k2eAgInSc7d1
světem	svět	k1gInSc7
a	a	k8xC
uzavřená	uzavřený	k2eAgFnSc1d1
do	do	k7c2
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tokugawský	Tokugawský	k2eAgInSc1d1
šógunát	šógunát	k1gInSc1
znamenal	znamenat	k5eAaImAgInS
pro	pro	k7c4
Japonsko	Japonsko	k1gNnSc4
dlouhé	dlouhý	k2eAgNnSc1d1
období	období	k1gNnSc1
míru	mír	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
samurajové	samuraj	k1gMnPc1
začali	začít	k5eAaPmAgMnP
věnovat	věnovat	k5eAaPmF,k5eAaImF
administrativním	administrativní	k2eAgFnPc3d1
povinnostem	povinnost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradiční	tradiční	k2eAgFnPc1d1
bojové	bojový	k2eAgFnPc1d1
dovednosti	dovednost	k1gFnPc1
si	se	k3xPyFc3
však	však	k9
udržely	udržet	k5eAaPmAgFnP
svoji	svůj	k3xOyFgFnSc4
společenskou	společenský	k2eAgFnSc4d1
vážnost	vážnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lukostřelba	lukostřelba	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
stala	stát	k5eAaPmAgFnS
„	„	k?
<g/>
volitelnou	volitelný	k2eAgFnSc4d1
<g/>
“	“	k?
dovedností	dovednost	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
v	v	k7c6
rituální	rituální	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
provozovala	provozovat	k5eAaImAgFnS
zčásti	zčásti	k6eAd1
u	u	k7c2
dvora	dvůr	k1gInSc2
a	a	k8xC
částečně	částečně	k6eAd1
pak	pak	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
různých	různý	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
více	hodně	k6eAd2
připomínaly	připomínat	k5eAaImAgFnP
bojové	bojový	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umění	umění	k1gNnSc1
střelby	střelba	k1gFnSc2
z	z	k7c2
luku	luk	k1gInSc2
se	se	k3xPyFc4
šířilo	šířit	k5eAaImAgNnS
i	i	k9
mimo	mimo	k7c4
třídu	třída	k1gFnSc4
válečníků	válečník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samurajové	samuraj	k1gMnPc1
byli	být	k5eAaImAgMnP
ovlivněni	ovlivnit	k5eAaPmNgMnP
přímočarou	přímočarý	k2eAgFnSc7d1
<g/>
,	,	kIx,
spontánní	spontánní	k2eAgFnSc7d1
filozofií	filozofie	k1gFnSc7
zenového	zenový	k2eAgInSc2d1
buddhismu	buddhismus	k1gInSc2
zaměřenou	zaměřený	k2eAgFnSc4d1
k	k	k7c3
sebeovládání	sebeovládání	k1gNnSc3
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
do	do	k7c2
země	zem	k1gFnSc2
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přinesli	přinést	k5eAaPmAgMnP
mniši	mnich	k1gMnPc1
z	z	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lukostřelba	lukostřelba	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
původně	původně	k6eAd1
nazývala	nazývat	k5eAaImAgFnS
kjúdžucu	kjúdžuca	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
umění	umění	k1gNnSc1
luku	luk	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
mniši	mnich	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
působili	působit	k5eAaImAgMnP
i	i	k9
jako	jako	k8xS,k8xC
učitelé	učitel	k1gMnPc1
bojových	bojový	k2eAgNnPc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
přispěli	přispět	k5eAaPmAgMnP
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
nové	nový	k2eAgFnSc2d1
koncepce	koncepce	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
kjúdó	kjúdó	k?
neboli	neboli	k8xC
cesta	cesta	k1gFnSc1
luku	luk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Oživení	oživení	k1gNnSc1
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
Japonsku	Japonsko	k1gNnSc6
přineslo	přinést	k5eAaPmAgNnS
otevření	otevření	k1gNnSc1
se	se	k3xPyFc4
vnějšímu	vnější	k2eAgInSc3d1
světu	svět	k1gInSc3
počátkem	počátkem	k7c2
období	období	k1gNnSc2
Meidži	Meidž	k1gFnSc6
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ztratili	ztratit	k5eAaPmAgMnP
samurajové	samuraj	k1gMnPc1
svoje	svůj	k3xOyFgNnSc4
prestižní	prestižní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veškerá	veškerý	k3xTgNnPc1
bojová	bojový	k2eAgNnPc1d1
umění	umění	k1gNnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
kjúdó	kjúdó	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
proto	proto	k8xC
musela	muset	k5eAaImAgFnS
vypořádat	vypořádat	k5eAaPmF
se	s	k7c7
značným	značný	k2eAgInSc7d1
poklesem	pokles	k1gInSc7
veřejného	veřejný	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
ruku	ruka	k1gFnSc4
v	v	k7c6
ruce	ruka	k1gFnSc6
kráčel	kráčet	k5eAaImAgInS
výrazný	výrazný	k2eAgInSc1d1
úbytek	úbytek	k1gInSc1
zájemců	zájemce	k1gMnPc2
o	o	k7c4
jejich	jejich	k3xOp3gFnSc4
výuku	výuka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1896	#num#	k4
se	se	k3xPyFc4
proto	proto	k8xC
sešla	sejít	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
mistrů	mistr	k1gMnPc2
kjúdó	kjúdó	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
poradili	poradit	k5eAaPmAgMnP
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zachránit	zachránit	k5eAaPmF
tradiční	tradiční	k2eAgFnSc4d1
lukostřelbu	lukostřelba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tošizane	Tošizan	k1gMnSc5
Honda	Honda	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vyučoval	vyučovat	k5eAaImAgMnS
kjúdó	kjúdó	k?
na	na	k7c6
Císařské	císařský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Tokiu	Tokio	k1gNnSc6
<g/>
,	,	kIx,
spojil	spojit	k5eAaPmAgMnS
bojový	bojový	k2eAgInSc4d1
a	a	k8xC
rituální	rituální	k2eAgInSc4d1
střelecký	střelecký	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
smíšený	smíšený	k2eAgInSc4d1
styl	styl	k1gInSc4
nazvaný	nazvaný	k2eAgInSc4d1
Honda-rjú	Honda-rjú	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Všejaponská	Všejaponský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
kjúdó	kjúdó	k?
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
Zen	zen	k2eAgInSc1d1
Nihon	Nihon	k1gInSc1
Kjū	Kjū	k1gFnSc2
Renmei	Renme	k1gFnSc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrnice	směrnice	k1gFnSc1
zveřejněné	zveřejněný	k2eAgNnSc1d1
v	v	k7c6
příručce	příručka	k1gFnSc6
kjúdó	kjúdó	k?
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
stanovují	stanovovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
lukostřelci	lukostřelec	k1gMnPc1
z	z	k7c2
různých	různý	k2eAgFnPc2d1
škol	škola	k1gFnPc2
účastnit	účastnit	k5eAaImF
soutěží	soutěž	k1gFnPc2
nebo	nebo	k8xC
zkoušek	zkouška	k1gFnPc2
technické	technický	k2eAgFnSc2d1
vyspělosti	vyspělost	k1gFnSc2
společně	společně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Kyū	Kyū	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Samurai	Samurai	k1gNnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Weapons	Weapons	k1gInSc1
and	and	k?
Spirit	Spirit	k1gInSc1
of	of	k?
the	the	k?
Japanese	Japanese	k1gFnSc2
Warrior	Warrior	k1gMnSc1
<g/>
,	,	kIx,
Author	Author	k1gMnSc1
Clive	Cliv	k1gMnSc5
Sinclaire	Sinclair	k1gMnSc5
<g/>
,	,	kIx,
Publisher	Publishra	k1gFnPc2
Globe	globus	k1gInSc5
Pequot	Pequot	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59228	#num#	k4
<g/>
-	-	kIx~
<g/>
720	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-1-59228-720-8	978-1-59228-720-8	k4
P.	P.	kA
<g/>
121	#num#	k4
<g/>
↑	↑	k?
Yamada	Yamada	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Shō	Shō	k1gInPc7
<g/>
,	,	kIx,
The	The	k1gMnSc1
Myth	Myth	k1gMnSc1
of	of	k?
Zen	zen	k2eAgNnSc1d1
in	in	k?
the	the	k?
Art	Art	k1gFnSc2
of	of	k?
Archery	Archera	k1gFnSc2
<g/>
,	,	kIx,
Japanese	Japanese	k1gFnSc2
Journal	Journal	k1gFnSc2
of	of	k?
Religious	Religious	k1gMnSc1
Studies	Studies	k1gMnSc1
2001	#num#	k4
28	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
↑	↑	k?
Thomas	Thomas	k1gMnSc1
A.	A.	kA
Green	Green	k1gInSc1
<g/>
,	,	kIx,
Martial	Martial	k1gInSc1
Arts	Arts	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
↑	↑	k?
Tanegashima	Tanegashima	k1gFnSc1
<g/>
:	:	kIx,
the	the	k?
arrival	arrivat	k5eAaBmAgMnS,k5eAaPmAgMnS,k5eAaImAgMnS
of	of	k?
Europe	Europ	k1gInSc5
in	in	k?
Japan	japan	k1gInSc1
<g/>
,	,	kIx,
Olof	Olof	k1gMnSc1
G.	G.	kA
Lidin	Lidin	k1gMnSc1
<g/>
,	,	kIx,
Nordic	Nordic	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Asian	Asian	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
,	,	kIx,
NIAS	NIAS	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
P.	P.	kA
<g/>
1-14	1-14	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kjúdó	Kjúdó	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
International	Internationat	k5eAaImAgMnS,k5eAaPmAgMnS
Kyudo	Kyudo	k1gNnSc4
Federation	Federation	k1gInSc1
</s>
<s>
European	European	k1gInSc1
Kyudo	Kyudo	k1gNnSc1
Federation	Federation	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
