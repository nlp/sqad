<s>
Měsíční	měsíční	k2eAgInSc1d1	měsíční
prach	prach	k1gInSc1	prach
je	být	k5eAaImIp3nS	být
hard	hard	k6eAd1	hard
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc4	fiction
román	román	k1gInSc1	román
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Arthura	Arthur	k1gMnSc2	Arthur
C.	C.	kA	C.
Clarka	Clarka	k1gMnSc1	Clarka
vydaný	vydaný	k2eAgMnSc1d1	vydaný
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
se	se	k3xPyFc4	se
kniha	kniha	k1gFnSc1	kniha
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
A	a	k9	a
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
Moondust	Moondust	k1gMnSc1	Moondust
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Hugo	Hugo	k1gMnSc1	Hugo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
román	román	k1gInSc4	román
<g/>
"	"	kIx"	"
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
několik	několik	k4yIc1	několik
měsíčních	měsíční	k2eAgInPc2d1	měsíční
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Mare	Mare	k1gInSc1	Mare
Nubium	Nubium	k1gNnSc1	Nubium
(	(	kIx(	(
<g/>
Moře	moře	k1gNnSc1	moře
mraků	mrak	k1gInPc2	mrak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mare	Mare	k1gNnSc1	Mare
Imbrium	Imbrium	k1gNnSc1	Imbrium
(	(	kIx(	(
<g/>
Moře	moře	k1gNnSc1	moře
dešťů	dešť	k1gInPc2	dešť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mare	Mare	k1gFnSc1	Mare
Nectaris	Nectaris	k1gFnSc1	Nectaris
(	(	kIx(	(
<g/>
Moře	moře	k1gNnSc1	moře
nektaru	nektar	k1gInSc2	nektar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sinus	sinus	k1gInSc1	sinus
Roris	Roris	k1gInSc1	Roris
(	(	kIx(	(
<g/>
Záliv	záliv	k1gInSc1	záliv
rosy	rosa	k1gFnSc2	rosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Montes	Montes	k1gMnSc1	Montes
Apenninus	Apenninus	k1gMnSc1	Apenninus
(	(	kIx(	(
<g/>
pohoří	pohořet	k5eAaPmIp3nP	pohořet
Apeniny	Apeniny	k1gFnPc1	Apeniny
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátery	kráter	k1gInPc1	kráter
Archimedes	Archimedes	k1gMnSc1	Archimedes
<g/>
,	,	kIx,	,
Plato	Plato	k1gMnSc1	Plato
<g/>
,	,	kIx,	,
Aristillus	Aristillus	k1gMnSc1	Aristillus
<g/>
,	,	kIx,	,
Eudoxus	Eudoxus	k1gMnSc1	Eudoxus
<g/>
,	,	kIx,	,
hora	hora	k1gFnSc1	hora
Mons	Monsa	k1gFnPc2	Monsa
Pico	Pico	k6eAd1	Pico
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
však	však	k9	však
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
fiktivního	fiktivní	k2eAgNnSc2d1	fiktivní
Moře	moře	k1gNnSc2	moře
žízně	žízeň	k1gFnSc2	žízeň
(	(	kIx(	(
<g/>
ležícího	ležící	k2eAgMnSc2d1	ležící
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
rosy	rosa	k1gFnSc2	rosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
lidé	člověk	k1gMnPc1	člověk
osídlili	osídlit	k5eAaPmAgMnP	osídlit
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
i	i	k9	i
vesmírným	vesmírný	k2eAgMnPc3d1	vesmírný
turistům	turist	k1gMnPc3	turist
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
mohou	moct	k5eAaImIp3nP	moct
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1	turistická
loď	loď	k1gFnSc1	loď
Seléné	Seléná	k1gFnSc2	Seléná
zůstane	zůstat	k5eAaPmIp3nS	zůstat
i	i	k9	i
s	s	k7c7	s
pasažéry	pasažér	k1gMnPc7	pasažér
po	po	k7c6	po
měsíčním	měsíční	k2eAgNnSc6d1	měsíční
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
pod	pod	k7c7	pod
prachem	prach	k1gInSc7	prach
<g/>
.	.	kIx.	.
</s>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
souboj	souboj	k1gInSc1	souboj
s	s	k7c7	s
časem	čas	k1gInSc7	čas
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
záchranu	záchrana	k1gFnSc4	záchrana
<g/>
.	.	kIx.	.
</s>
<s>
Lawrence	Lawrenec	k1gInPc1	Lawrenec
-	-	kIx~	-
hlavní	hlavní	k2eAgMnSc1d1	hlavní
inženýr	inženýr	k1gMnSc1	inženýr
pro	pro	k7c4	pro
přivrácenou	přivrácený	k2eAgFnSc4d1	přivrácená
stranu	strana	k1gFnSc4	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Davis	Davis	k1gFnSc1	Davis
-	-	kIx~	-
ředitel	ředitel	k1gMnSc1	ředitel
Lunární	lunární	k2eAgFnSc2d1	lunární
cestovní	cestovní	k2eAgFnSc2d1	cestovní
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Lawson	Lawson	k1gMnSc1	Lawson
-	-	kIx~	-
mladý	mladý	k2eAgMnSc1d1	mladý
a	a	k8xC	a
bystrý	bystrý	k2eAgMnSc1d1	bystrý
astronom	astronom	k1gMnSc1	astronom
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Lagrange	Lagrang	k1gFnSc2	Lagrang
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Olsen	Olsen	k1gInSc1	Olsen
-	-	kIx~	-
guvernér	guvernér	k1gMnSc1	guvernér
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
otec	otec	k1gMnSc1	otec
Vincent	Vincent	k1gMnSc1	Vincent
Ferraro	Ferrara	k1gFnSc5	Ferrara
-	-	kIx~	-
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
selenofyzik	selenofyzik	k1gMnSc1	selenofyzik
<g/>
.	.	kIx.	.
profesor	profesor	k1gMnSc1	profesor
Kotelnikov	Kotelnikov	k1gInSc1	Kotelnikov
-	-	kIx~	-
astronom	astronom	k1gMnSc1	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Spencer	Spencer	k1gMnSc1	Spencer
-	-	kIx~	-
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
Meziplanetárního	meziplanetární	k2eAgMnSc2d1	meziplanetární
zpravodaje	zpravodaj	k1gMnSc2	zpravodaj
<g/>
.	.	kIx.	.
</s>
<s>
Jules	Jules	k1gMnSc1	Jules
Braques	Braques	k1gMnSc1	Braques
-	-	kIx~	-
kameraman	kameraman	k1gMnSc1	kameraman
z	z	k7c2	z
agentury	agentura	k1gFnSc2	agentura
Meziplanetární	meziplanetární	k2eAgMnSc1d1	meziplanetární
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
.	.	kIx.	.
</s>
<s>
Mike	Mike	k1gFnSc1	Mike
Graham	Graham	k1gMnSc1	Graham
-	-	kIx~	-
novinář	novinář	k1gMnSc1	novinář
z	z	k7c2	z
agentury	agentura	k1gFnSc2	agentura
Meziplanetární	meziplanetární	k2eAgMnSc1d1	meziplanetární
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
.	.	kIx.	.
</s>
<s>
Jerry	Jerra	k1gMnSc2	Jerra
Budker	Budker	k1gMnSc1	Budker
-	-	kIx~	-
kriminálník	kriminálník	k1gMnSc1	kriminálník
z	z	k7c2	z
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
kapitán	kapitán	k1gMnSc1	kapitán
Anson	Anson	k1gMnSc1	Anson
-	-	kIx~	-
kapitán	kapitán	k1gMnSc1	kapitán
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Auriga	Auriga	k1gFnSc1	Auriga
<g/>
.	.	kIx.	.
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Evans	Evans	k1gInSc1	Evans
-	-	kIx~	-
půdní	půdní	k2eAgMnSc1d1	půdní
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Jones	Jones	k1gMnSc1	Jones
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
záchranného	záchranný	k2eAgInSc2d1	záchranný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Sikorsky	Sikorsky	k6eAd1	Sikorsky
-	-	kIx~	-
člen	člen	k1gInSc1	člen
záchranného	záchranný	k2eAgInSc2d1	záchranný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Coleman	Coleman	k1gMnSc1	Coleman
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
záchranného	záchranný	k2eAgInSc2d1	záchranný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Matsui	Matsui	k1gNnSc1	Matsui
-	-	kIx~	-
člen	člen	k1gInSc1	člen
záchranného	záchranný	k2eAgInSc2d1	záchranný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
záchranného	záchranný	k2eAgInSc2d1	záchranný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Hodges	Hodges	k1gMnSc1	Hodges
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
záchranného	záchranný	k2eAgInSc2d1	záchranný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Greenwood	Greenwood	k1gInSc1	Greenwood
-	-	kIx~	-
člen	člen	k1gInSc1	člen
záchranného	záchranný	k2eAgInSc2d1	záchranný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Renaldi	Renald	k1gMnPc1	Renald
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
záchranného	záchranný	k2eAgInSc2d1	záchranný
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
signore	signore	k1gMnSc1	signore
Gusalli	Gusalle	k1gFnSc4	Gusalle
-	-	kIx~	-
amatérský	amatérský	k2eAgMnSc1d1	amatérský
vynálezce	vynálezce	k1gMnSc1	vynálezce
ze	z	k7c2	z
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Robertson	Robertson	k1gNnSc1	Robertson
-	-	kIx~	-
amatérský	amatérský	k2eAgMnSc1d1	amatérský
vynálezce	vynálezce	k1gMnSc1	vynálezce
z	z	k7c2	z
kanadské	kanadský	k2eAgFnSc2d1	kanadská
provincie	provincie	k1gFnSc2	provincie
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
.	.	kIx.	.
slečna	slečna	k1gFnSc1	slečna
Johnsonová	Johnsonová	k1gFnSc1	Johnsonová
-	-	kIx~	-
stewardka	stewardka	k1gFnSc1	stewardka
měsíčního	měsíční	k2eAgInSc2d1	měsíční
člunu	člun	k1gInSc2	člun
Seléné	Seléná	k1gFnSc2	Seléná
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
ztroskotané	ztroskotaný	k2eAgFnSc2d1	ztroskotaná
Seléné	Seléná	k1gFnSc2	Seléná
<g/>
:	:	kIx,	:
Pat	pat	k1gInSc1	pat
Harris	Harris	k1gFnSc2	Harris
-	-	kIx~	-
kapitán	kapitán	k1gMnSc1	kapitán
Seléné	Seléná	k1gFnSc2	Seléná
<g/>
.	.	kIx.	.
</s>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Wilkinsová	Wilkinsová	k1gFnSc1	Wilkinsová
-	-	kIx~	-
stewardka	stewardka	k1gFnSc1	stewardka
na	na	k7c4	na
Seléné	Seléné	k1gNnSc4	Seléné
<g/>
.	.	kIx.	.
komodor	komodor	k1gMnSc1	komodor
Hansteen	Hansteen	k2eAgMnSc1d1	Hansteen
-	-	kIx~	-
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
vesmírných	vesmírný	k2eAgMnPc2d1	vesmírný
dobrodruhů	dobrodruh	k1gMnPc2	dobrodruh
cestující	cestující	k2eAgNnSc1d1	cestující
inkognito	inkognito	k1gNnSc1	inkognito
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Seléné	Seléná	k1gFnSc2	Seléná
<g/>
.	.	kIx.	.
</s>
<s>
Pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Duncan	Duncan	k1gInSc1	Duncan
McKenzie	McKenzie	k1gFnSc2	McKenzie
-	-	kIx~	-
fyzik	fyzik	k1gMnSc1	fyzik
z	z	k7c2	z
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
Mount	Mounta	k1gFnPc2	Mounta
Stromlo	Stromlo	k1gNnSc4	Stromlo
v	v	k7c6	v
Canbeře	Canbera	k1gFnSc6	Canbera
<g/>
.	.	kIx.	.
</s>
<s>
Pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Blanchard	Blanchard	k1gMnSc1	Blanchard
-	-	kIx~	-
revizní	revizní	k2eAgFnSc1d1	revizní
účetní	účetní	k1gFnSc1	účetní
z	z	k7c2	z
Clavius	Clavius	k1gInSc4	Clavius
City	City	k1gFnSc4	City
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Phylis	Phylis	k1gFnSc1	Phylis
Morleyová	Morleyová	k1gFnSc1	Morleyová
-	-	kIx~	-
novinářka	novinářka	k1gFnSc1	novinářka
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Pasažérka	pasažérka	k1gFnSc1	pasažérka
<g/>
.	.	kIx.	.
</s>
<s>
Karl	Karl	k1gMnSc1	Karl
Johansen	Johansen	k2eAgMnSc1d1	Johansen
-	-	kIx~	-
nukleární	nukleární	k2eAgMnSc1d1	nukleární
inženýr	inženýr	k1gMnSc1	inženýr
z	z	k7c2	z
Ciolkovského	Ciolkovský	k2eAgInSc2d1	Ciolkovský
základny	základna	k1gFnSc2	základna
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Bryan	Bryan	k1gMnSc1	Bryan
-	-	kIx~	-
civilní	civilní	k2eAgMnSc1d1	civilní
inženýr	inženýr	k1gMnSc1	inženýr
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Kingstonu	Kingston	k1gInSc2	Kingston
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Irving	Irving	k1gInSc1	Irving
Schuster	Schuster	k1gInSc1	Schuster
-	-	kIx~	-
advokát	advokát	k1gMnSc1	advokát
z	z	k7c2	z
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Myra	Myr	k2eAgFnSc1d1	Myra
Schusterová	Schusterová	k1gFnSc1	Schusterová
-	-	kIx~	-
manželka	manželka	k1gFnSc1	manželka
Irvinga	Irvinga	k1gFnSc1	Irvinga
<g/>
.	.	kIx.	.
</s>
<s>
Pasažérka	pasažérka	k1gFnSc1	pasažérka
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Barrett	Barrett	k1gMnSc1	Barrett
-	-	kIx~	-
Angličan	Angličan	k1gMnSc1	Angličan
<g/>
,	,	kIx,	,
pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Nihal	Nihal	k1gMnSc1	Nihal
Džajavardhana	Džajavardhan	k1gMnSc2	Džajavardhan
-	-	kIx~	-
profesor	profesor	k1gMnSc1	profesor
zoologie	zoologie	k1gFnSc1	zoologie
z	z	k7c2	z
Perádenijské	Perádenijský	k2eAgFnSc2d1	Perádenijský
univerzity	univerzita	k1gFnSc2	univerzita
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
</s>
<s>
Pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Baldur	Baldur	k1gMnSc1	Baldur
-	-	kIx~	-
pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Radley	Radley	k1gInPc1	Radley
-	-	kIx~	-
pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
paní	paní	k1gFnSc1	paní
Williamsová	Williamsová	k1gFnSc1	Williamsová
-	-	kIx~	-
pasažérka	pasažérka	k1gFnSc1	pasažérka
<g/>
.	.	kIx.	.
pan	pan	k1gMnSc1	pan
Harding	Harding	k1gInSc1	Harding
-	-	kIx~	-
pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
vědecké	vědecký	k2eAgFnSc2d1	vědecká
základny	základna	k1gFnSc2	základna
a	a	k8xC	a
turistické	turistický	k2eAgFnSc2d1	turistická
kanceláře	kancelář	k1gFnSc2	kancelář
organizují	organizovat	k5eAaBmIp3nP	organizovat
turistické	turistický	k2eAgInPc4d1	turistický
zájezdy	zájezd	k1gInPc4	zájezd
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
atrakcí	atrakce	k1gFnPc2	atrakce
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
plavba	plavba	k1gFnSc1	plavba
<g/>
"	"	kIx"	"
po	po	k7c6	po
měsíčních	měsíční	k2eAgNnPc6d1	měsíční
mořích	moře	k1gNnPc6	moře
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
velice	velice	k6eAd1	velice
jemného	jemný	k2eAgInSc2d1	jemný
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
zkonstruovaná	zkonstruovaný	k2eAgFnSc1d1	zkonstruovaná
turistická	turistický	k2eAgFnSc1d1	turistická
loď	loď	k1gFnSc1	loď
Seléné	Seléná	k1gFnSc2	Seléná
se	se	k3xPyFc4	se
plaví	plavit	k5eAaImIp3nS	plavit
po	po	k7c6	po
prachu	prach	k1gInSc6	prach
na	na	k7c6	na
Moři	moře	k1gNnSc6	moře
žízně	žízeň	k1gFnSc2	žízeň
(	(	kIx(	(
<g/>
situovaného	situovaný	k2eAgInSc2d1	situovaný
do	do	k7c2	do
Zálivu	záliv	k1gInSc2	záliv
rosy	rosa	k1gFnSc2	rosa
-	-	kIx~	-
Sinus	sinus	k1gInSc1	sinus
Roris	Roris	k1gFnSc2	Roris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
jemný	jemný	k2eAgInSc1d1	jemný
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
vlastnosti	vlastnost	k1gFnPc4	vlastnost
vody	voda	k1gFnPc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
vyhlídkové	vyhlídkový	k2eAgFnSc6d1	vyhlídková
jízdě	jízda	k1gFnSc6	jízda
po	po	k7c6	po
Moři	moře	k1gNnSc6	moře
žízně	žízeň	k1gFnSc2	žízeň
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Záchvěvy	záchvěv	k1gInPc1	záchvěv
půdy	půda	k1gFnSc2	půda
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Seléné	Seléné	k1gNnSc1	Seléné
propadne	propadnout	k5eAaPmIp3nS	propadnout
do	do	k7c2	do
prašného	prašný	k2eAgNnSc2d1	prašné
podloží	podloží	k1gNnSc2	podloží
<g/>
.	.	kIx.	.
</s>
<s>
Zásoby	zásoba	k1gFnPc1	zásoba
vzduchu	vzduch	k1gInSc2	vzduch
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgFnPc1d1	omezená
<g/>
,	,	kIx,	,
teplo	teplo	k1gNnSc1	teplo
nemá	mít	k5eNaImIp3nS	mít
kam	kam	k6eAd1	kam
unikat	unikat	k5eAaImF	unikat
<g/>
,	,	kIx,	,
komunikace	komunikace	k1gFnSc1	komunikace
není	být	k5eNaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
přesně	přesně	k6eAd1	přesně
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
kapitána	kapitán	k1gMnSc4	kapitán
Pata	pata	k1gFnSc1	pata
Harrise	Harrise	k1gFnSc2	Harrise
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
cestující	cestující	k1gFnSc4	cestující
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
morálku	morálka	k1gFnSc4	morálka
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
musí	muset	k5eAaImIp3nS	muset
čekat	čekat	k5eAaImF	čekat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
záchrance	záchranka	k1gFnSc6	záchranka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
pasažéry	pasažér	k1gMnPc7	pasažér
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
zkušených	zkušený	k2eAgMnPc2d1	zkušený
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
vesmírný	vesmírný	k2eAgMnSc1d1	vesmírný
badatel	badatel	k1gMnSc1	badatel
v	v	k7c6	v
penzi	penze	k1gFnSc6	penze
<g/>
,	,	kIx,	,
komodor	komodor	k1gMnSc1	komodor
Hansteen	Hansteen	k1gInSc1	Hansteen
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
mu	on	k3xPp3gMnSc3	on
pomohou	pomoct	k5eAaPmIp3nP	pomoct
udržet	udržet	k5eAaPmF	udržet
na	na	k7c6	na
ztroskotaném	ztroskotaný	k2eAgNnSc6d1	ztroskotané
měsíčním	měsíční	k2eAgNnSc6d1	měsíční
plavidle	plavidlo	k1gNnSc6	plavidlo
optimistickou	optimistický	k2eAgFnSc4d1	optimistická
náladu	nálada	k1gFnSc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
inženýr	inženýr	k1gMnSc1	inženýr
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
strany	strana	k1gFnSc2	strana
Měsíce	měsíc	k1gInSc2	měsíc
Robert	Roberta	k1gFnPc2	Roberta
Lawrance	Lawrance	k1gFnSc2	Lawrance
je	být	k5eAaImIp3nS	být
skeptický	skeptický	k2eAgMnSc1d1	skeptický
ohledně	ohledně	k7c2	ohledně
možné	možný	k2eAgFnSc2d1	možná
záchrany	záchrana	k1gFnSc2	záchrana
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
člun	člun	k1gInSc1	člun
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
pod	pod	k7c7	pod
sesuvem	sesuv	k1gInSc7	sesuv
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vyproštění	vyproštění	k1gNnSc6	vyproštění
nejsou	být	k5eNaImIp3nP	být
lidské	lidský	k2eAgInPc1d1	lidský
ani	ani	k8xC	ani
materiální	materiální	k2eAgInPc1d1	materiální
zdroje	zdroj	k1gInPc1	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Lawrence	Lawrence	k1gFnSc1	Lawrence
zkontaktuje	zkontaktovat	k5eAaPmIp3nS	zkontaktovat
Thomas	Thomas	k1gMnSc1	Thomas
Lawson	Lawson	k1gMnSc1	Lawson
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgMnSc1d1	vynikající
a	a	k8xC	a
výstřední	výstřední	k2eAgMnSc1d1	výstřední
astronom	astronom	k1gMnSc1	astronom
z	z	k7c2	z
kosmické	kosmický	k2eAgFnSc2d1	kosmická
stanice	stanice	k1gFnSc2	stanice
Lagrange	Lagrang	k1gFnSc2	Lagrang
II	II	kA	II
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
výhodného	výhodný	k2eAgNnSc2d1	výhodné
postavení	postavení	k1gNnSc2	postavení
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
Měsíce	měsíc	k1gInSc2	měsíc
podaří	podařit	k5eAaPmIp3nS	podařit
zjistit	zjistit	k5eAaPmF	zjistit
zbytky	zbytek	k1gInPc4	zbytek
tepelné	tepelný	k2eAgFnSc2d1	tepelná
stopy	stopa	k1gFnSc2	stopa
člunu	člun	k1gInSc2	člun
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
člun	člun	k1gInSc1	člun
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
je	být	k5eAaImIp3nS	být
zorganizována	zorganizován	k2eAgFnSc1d1	zorganizována
výprava	výprava	k1gFnSc1	výprava
a	a	k8xC	a
Lawrence	Lawrence	k1gFnSc1	Lawrence
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Lawsona	Lawsona	k1gFnSc1	Lawsona
vskutku	vskutku	k9	vskutku
nalezne	naleznout	k5eAaPmIp3nS	naleznout
Seléné	Seléné	k1gNnSc1	Seléné
uvězněnou	uvězněný	k2eAgFnSc4d1	uvězněná
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
vyslána	vyslán	k2eAgFnSc1d1	vyslána
záchranná	záchranný	k2eAgFnSc1d1	záchranná
expedice	expedice	k1gFnSc1	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Čekajícím	čekající	k2eAgMnPc3d1	čekající
cestujícím	cestující	k1gMnPc3	cestující
se	se	k3xPyFc4	se
v	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
začíná	začínat	k5eAaImIp3nS	začínat
hromadit	hromadit	k5eAaImF	hromadit
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
Harris	Harris	k1gFnSc2	Harris
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
kolektivní	kolektivní	k2eAgFnSc4d1	kolektivní
aplikaci	aplikace	k1gFnSc4	aplikace
hypnotik	hypnotikum	k1gNnPc2	hypnotikum
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
je	být	k5eAaImIp3nS	být
Seléné	Seléná	k1gFnSc2	Seléná
vybavena	vybavit	k5eAaPmNgNnP	vybavit
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
sníží	snížit	k5eAaPmIp3nS	snížit
spotřeba	spotřeba	k1gFnSc1	spotřeba
vzduchu	vzduch	k1gInSc2	vzduch
až	až	k9	až
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Zachránci	zachránce	k1gMnPc1	zachránce
spustí	spustit	k5eAaPmIp3nP	spustit
k	k	k7c3	k
lodi	loď	k1gFnSc3	loď
trubky	trubka	k1gFnSc2	trubka
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
právě	právě	k9	právě
včas	včas	k6eAd1	včas
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
dílčí	dílčí	k2eAgInSc1d1	dílčí
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
unikající	unikající	k2eAgFnSc3d1	unikající
vodě	voda	k1gFnSc3	voda
z	z	k7c2	z
poškozených	poškozený	k2eAgFnPc2d1	poškozená
nádrží	nádrž	k1gFnPc2	nádrž
sjede	sjet	k5eAaPmIp3nS	sjet
člun	člun	k1gInSc1	člun
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
je	být	k5eAaImIp3nS	být
vypuknuvší	vypuknuvší	k2eAgInSc1d1	vypuknuvší
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
cestující	cestující	k1gFnSc1	cestující
a	a	k8xC	a
posádka	posádka	k1gFnSc1	posádka
jsou	být	k5eAaImIp3nP	být
zachráněni	zachráněn	k2eAgMnPc1d1	zachráněn
šachtou	šachta	k1gFnSc7	šachta
jen	jen	k9	jen
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
před	před	k7c7	před
výbuchem	výbuch	k1gInSc7	výbuch
nádrže	nádrž	k1gFnSc2	nádrž
s	s	k7c7	s
kapalným	kapalný	k2eAgInSc7d1	kapalný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
epilogu	epilog	k1gInSc6	epilog
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapitán	kapitán	k1gMnSc1	kapitán
Pat	pata	k1gFnPc2	pata
Harris	Harris	k1gInSc4	Harris
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
se	s	k7c7	s
stewardkou	stewardka	k1gFnSc7	stewardka
Zuzanou	Zuzana	k1gFnSc7	Zuzana
Wilkinsovou	Wilkinsová	k1gFnSc7	Wilkinsová
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
začal	začít	k5eAaPmAgInS	začít
ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
ztroskotání	ztroskotání	k1gNnSc2	ztroskotání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
čekají	čekat	k5eAaImIp3nP	čekat
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
nově	nově	k6eAd1	nově
zkonstruovaný	zkonstruovaný	k2eAgInSc1d1	zkonstruovaný
člun	člun	k1gInSc1	člun
Seléné	Seléná	k1gFnSc2	Seléná
II	II	kA	II
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
složit	složit	k5eAaPmF	složit
zkoušky	zkouška	k1gFnPc4	zkouška
na	na	k7c4	na
kapitána	kapitán	k1gMnSc4	kapitán
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tohle	tenhle	k3xDgNnSc1	tenhle
byla	být	k5eAaImAgFnS	být
přímá	přímý	k2eAgFnSc1d1	přímá
výzva	výzva	k1gFnSc1	výzva
jeho	jeho	k3xOp3gInSc3	jeho
vědeckému	vědecký	k2eAgInSc3d1	vědecký
důvtipu	důvtip	k1gInSc3	důvtip
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	tu	k6eAd1	tu
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
sázce	sázka	k1gFnSc6	sázka
tolik	tolik	k4xDc4	tolik
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdála	zdát	k5eAaImAgFnS	zdát
bezpodstatná	bezpodstatný	k2eAgFnSc1d1	bezpodstatná
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Tom	Tom	k1gMnSc1	Tom
Lawson	Lawson	k1gMnSc1	Lawson
nikdy	nikdy	k6eAd1	nikdy
nepovažoval	považovat	k5eNaImAgMnS	považovat
lidské	lidský	k2eAgFnSc3d1	lidská
bytosti	bytost	k1gFnSc3	bytost
za	za	k7c4	za
zvlášť	zvlášť	k6eAd1	zvlášť
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vesmíru	vesmír	k1gInSc2	vesmír
si	se	k3xPyFc3	se
vážil	vážit	k5eAaImAgMnS	vážit
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
bylo	být	k5eAaImAgNnS	být
soukromé	soukromý	k2eAgNnSc1d1	soukromé
zápolení	zápolení	k1gNnSc1	zápolení
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gInSc7	on
a	a	k8xC	a
vesmírem	vesmír	k1gInSc7	vesmír
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Otec	otec	k1gMnSc1	otec
Ferraro	Ferrara	k1gFnSc5	Ferrara
věřil	věřit	k5eAaImAgMnS	věřit
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Lawson	Lawson	k1gMnSc1	Lawson
nevěřil	věřit	k5eNaImAgMnS	věřit
v	v	k7c4	v
žádného	žádný	k3yNgMnSc4	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
zmínek	zmínka	k1gFnPc2	zmínka
o	o	k7c6	o
známých	známý	k2eAgFnPc6d1	známá
osobnostech	osobnost	k1gFnPc6	osobnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Kingsley	Kingslea	k1gFnSc2	Kingslea
Amis	Amis	k1gInSc1	Amis
<g/>
,	,	kIx,	,
Isaac	Isaac	k1gFnSc1	Isaac
Newton	Newton	k1gMnSc1	Newton
<g/>
,	,	kIx,	,
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
<g/>
,	,	kIx,	,
Nell	Nell	k1gInSc1	Nell
Gwynnová	Gwynnová	k1gFnSc1	Gwynnová
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickens	k1gInSc1	Dickens
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Pollard	Pollard	k1gMnSc1	Pollard
(	(	kIx(	(
<g/>
kapitán	kapitán	k1gMnSc1	kapitán
velrybářské	velrybářský	k2eAgFnSc2d1	velrybářská
lodi	loď	k1gFnSc2	loď
Essex	Essex	k1gInSc1	Essex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hector	Hector	k1gMnSc1	Hector
Berlioz	Berlioz	k1gMnSc1	Berlioz
<g/>
,	,	kIx,	,
Aram	Aram	k1gMnSc1	Aram
Chačaturjan	Chačaturjan	k1gMnSc1	Chačaturjan
<g/>
.	.	kIx.	.
</s>
<s>
Měsíční	měsíční	k2eAgInSc1d1	měsíční
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
edice	edice	k1gFnSc1	edice
Soudobá	soudobý	k2eAgFnSc1d1	soudobá
světová	světový	k2eAgFnSc1d1	světová
próza	próza	k1gFnSc1	próza
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
192	[number]	k4	192
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
,	,	kIx,	,
20	[number]	k4	20
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
obálky	obálka	k1gFnSc2	obálka
Milan	Milan	k1gMnSc1	Milan
Grygar	Grygar	k1gMnSc1	Grygar
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jarmila	Jarmila	k1gFnSc1	Jarmila
Emmerová	Emmerová	k1gFnSc1	Emmerová
Měsíční	měsíční	k2eAgFnSc1d1	měsíční
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
edice	edice	k1gFnSc1	edice
Sci-fi	scii	k1gFnSc1	sci-fi
<g/>
,	,	kIx,	,
brožovaná	brožovaný	k2eAgFnSc1d1	brožovaná
<g/>
,	,	kIx,	,
105	[number]	k4	105
000	[number]	k4	000
výtisků	výtisk	k1gInPc2	výtisk
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
obálky	obálka	k1gFnSc2	obálka
Joska	Joska	k1gMnSc1	Joska
Skalník	Skalník	k1gMnSc1	Skalník
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jarmila	Jarmila	k1gFnSc1	Jarmila
Emmerová	Emmerová	k1gFnSc1	Emmerová
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-207-0240-7	[number]	k4	80-207-0240-7
Měsíční	měsíční	k2eAgInSc1d1	měsíční
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
vázaná	vázaný	k2eAgFnSc1d1	vázaná
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
obálky	obálka	k1gFnSc2	obálka
Valentino	Valentina	k1gFnSc5	Valentina
Sani	saně	k1gFnSc4	saně
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jarmila	Jarmila	k1gFnSc1	Jarmila
Emmerová	Emmerová	k1gFnSc1	Emmerová
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7214-266-6	[number]	k4	80-7214-266-6
Dalším	další	k2eAgInSc7d1	další
literárním	literární	k2eAgInSc7d1	literární
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prach	prach	k1gInSc1	prach
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
způsobí	způsobit	k5eAaPmIp3nS	způsobit
lidem	člověk	k1gMnPc3	člověk
komplikace	komplikace	k1gFnSc2	komplikace
je	být	k5eAaImIp3nS	být
vědeckofantastická	vědeckofantastický	k2eAgFnSc1d1	vědeckofantastická
povídka	povídka	k1gFnSc1	povídka
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Hala	hala	k1gFnSc1	hala
Clementa	Clementa	k1gFnSc1	Clementa
"	"	kIx"	"
<g/>
Mít	mít	k5eAaImF	mít
tak	tak	k6eAd1	tak
s	s	k7c7	s
sebou	se	k3xPyFc7	se
prachovku	prachovka	k1gFnSc4	prachovka
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
