<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvořily	tvořit	k5eAaImAgInP	tvořit
ročníky	ročník	k1gInPc1	ročník
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
vítězným	vítězný	k2eAgInSc7d1	vítězný
filmem	film	k1gInSc7	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Otesánek	otesánek	k1gInSc1	otesánek
Jana	Jan	k1gMnSc2	Jan
Švankmajera	Švankmajer	k1gMnSc2	Švankmajer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
za	za	k7c4	za
Tmavomodrý	tmavomodrý	k2eAgInSc4d1	tmavomodrý
svět	svět	k1gInSc4	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
vítězným	vítězný	k2eAgInSc7d1	vítězný
filmem	film	k1gInSc7	film
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
Tajnosti	tajnost	k1gFnPc1	tajnost
Alice	Alice	k1gFnSc1	Alice
Nellis	Nellis	k1gFnSc1	Nellis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
za	za	k7c2	za
Vratné	vratný	k2eAgFnSc2d1	vratná
lahve	lahev	k1gFnSc2	lahev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
