<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
h.	h.	k?	h.
c.	c.	k?	c.
Ing.	ing.	kA	ing.
Rudolf	Rudolf	k1gMnSc1	Rudolf
Schuster	Schuster	k1gMnSc1	Schuster
<g/>
,	,	kIx,	,
PhD	PhD	k1gMnSc1	PhD
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Medzev	Medzev	k1gFnSc1	Medzev
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
karpato-německého	karpatoěmecký	k2eAgInSc2d1	karpato-německý
a	a	k8xC	a
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
maďarského	maďarský	k2eAgInSc2d1	maďarský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
až	až	k9	až
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
a	a	k8xC	a
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
byl	být	k5eAaImAgMnS	být
primátorem	primátor	k1gMnSc7	primátor
města	město	k1gNnSc2	město
Košice	Košice	k1gInPc1	Košice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Východoslovenského	východoslovenský	k2eAgInSc2d1	východoslovenský
krajského	krajský	k2eAgInSc2d1	krajský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
až	až	k9	až
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1990	[number]	k4	1990
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1999	[number]	k4	1999
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
přímých	přímý	k2eAgFnPc6d1	přímá
volbách	volba	k1gFnPc6	volba
zvolen	zvolit	k5eAaPmNgMnS	zvolit
slovenským	slovenský	k2eAgMnSc7d1	slovenský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
7,4	[number]	k4	7,4
%	%	kIx~	%
odevzdaných	odevzdaný	k2eAgInPc2d1	odevzdaný
hlasů	hlas	k1gInPc2	hlas
se	se	k3xPyFc4	se
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
voleb	volba	k1gFnPc2	volba
umístil	umístit	k5eAaPmAgInS	umístit
až	až	k6eAd1	až
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
vítěz	vítěz	k1gMnSc1	vítěz
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
voleb	volba	k1gFnPc2	volba
Ivan	Ivan	k1gMnSc1	Ivan
Gašparovič	Gašparovič	k1gMnSc1	Gašparovič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
získával	získávat	k5eAaImAgInS	získávat
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Střední	střední	k2eAgFnSc4d1	střední
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
školu	škola	k1gFnSc4	škola
stavební	stavební	k2eAgFnPc1d1	stavební
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc6d1	technická
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
obor	obor	k1gInSc4	obor
inženýrské	inženýrský	k2eAgNnSc1d1	inženýrské
stavitelství	stavitelství	k1gNnSc1	stavitelství
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
aspiranturu	aspirantura	k1gFnSc4	aspirantura
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
ekologie	ekologie	k1gFnSc2	ekologie
na	na	k7c6	na
Hornické	hornický	k2eAgFnSc6d1	hornická
fakultě	fakulta	k1gFnSc6	fakulta
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
technické	technický	k2eAgFnPc4d1	technická
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
titul	titul	k1gInSc4	titul
kandidáta	kandidát	k1gMnSc2	kandidát
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
CSc.	CSc.	kA	CSc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
projektant	projektant	k1gMnSc1	projektant
v	v	k7c6	v
Krajském	krajský	k2eAgInSc6d1	krajský
zemědělském	zemědělský	k2eAgInSc6d1	zemědělský
projektovém	projektový	k2eAgInSc6d1	projektový
ústavu	ústav	k1gInSc6	ústav
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
asistentem	asistent	k1gMnSc7	asistent
v	v	k7c6	v
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
východoslovenských	východoslovenský	k2eAgFnPc6d1	Východoslovenská
železárnách	železárna	k1gFnPc6	železárna
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
jako	jako	k8xC	jako
referent	referent	k1gMnSc1	referent
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
byl	být	k5eAaImAgMnS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
sekretariátu	sekretariát	k1gInSc2	sekretariát
a	a	k8xC	a
technickým	technický	k2eAgMnSc7d1	technický
asistentem	asistent	k1gMnSc7	asistent
podnikového	podnikový	k2eAgNnSc2d1	podnikové
ředitele	ředitel	k1gMnSc2	ředitel
ve	v	k7c6	v
východoslovenských	východoslovenský	k2eAgFnPc2d1	Východoslovenská
železáren	železárna	k1gFnPc2	železárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Primátor	primátor	k1gMnSc1	primátor
Košic	Košice	k1gInPc2	Košice
a	a	k8xC	a
Prezident	prezident	k1gMnSc1	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
národním	národní	k2eAgInSc6d1	národní
výboru	výbor	k1gInSc6	výbor
města	město	k1gNnSc2	město
Košice	Košice	k1gInPc1	Košice
jako	jako	k8xS	jako
místopředseda	místopředseda	k1gMnSc1	místopředseda
pro	pro	k7c4	pro
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
místopředseda	místopředseda	k1gMnSc1	místopředseda
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
poprvé	poprvé	k6eAd1	poprvé
primátorem	primátor	k1gMnSc7	primátor
města	město	k1gNnSc2	město
Košice	Košice	k1gInPc1	Košice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Východoslovenského	východoslovenský	k2eAgInSc2d1	východoslovenský
krajského	krajský	k2eAgInSc2d1	krajský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
a	a	k8xC	a
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
ČSFR	ČSFR	kA	ČSFR
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
působil	působit	k5eAaImAgInS	působit
podruhé	podruhé	k6eAd1	podruhé
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
primátora	primátor	k1gMnSc2	primátor
města	město	k1gNnSc2	město
Košice	Košice	k1gInPc4	Košice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zadlužil	zadlužit	k5eAaPmAgInS	zadlužit
město	město	k1gNnSc4	město
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zavést	zavést	k5eAaPmF	zavést
tzv.	tzv.	kA	tzv.
ozdravný	ozdravný	k2eAgInSc4d1	ozdravný
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
a	a	k8xC	a
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
uvalení	uvalení	k1gNnSc6	uvalení
nucené	nucený	k2eAgFnSc2d1	nucená
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
poslance	poslanec	k1gMnPc4	poslanec
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgInS	působit
také	také	k9	také
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
média	médium	k1gNnPc4	médium
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
národnosti	národnost	k1gFnPc4	národnost
(	(	kIx(	(
<g/>
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
Zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
výboru	výbor	k1gInSc2	výbor
NR	NR	kA	NR
SR	SR	kA	SR
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
činnosti	činnost	k1gFnSc2	činnost
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
informační	informační	k2eAgFnSc2d1	informační
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přímých	přímý	k2eAgFnPc6d1	přímá
volbách	volba	k1gFnPc6	volba
ve	v	k7c6	v
dnech	den	k1gInPc6	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
a	a	k8xC	a
1999	[number]	k4	1999
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
inaugurace	inaugurace	k1gFnSc1	inaugurace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1999	[number]	k4	1999
a	a	k8xC	a
zvolen	zvolen	k2eAgMnSc1d1	zvolen
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
v	v	k7c6	v
letech	let	k1gInPc6	let
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
svůj	svůj	k3xOyFgInSc4	svůj
mandát	mandát	k1gInSc4	mandát
obhájit	obhájit	k5eAaPmF	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
však	však	k9	však
vyřazen	vyřazen	k2eAgMnSc1d1	vyřazen
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
když	když	k8xS	když
obdržel	obdržet	k5eAaPmAgMnS	obdržet
pouhých	pouhý	k2eAgNnPc2d1	pouhé
7,4	[number]	k4	7,4
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
umístil	umístit	k5eAaPmAgMnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Schuster	Schuster	k1gMnSc1	Schuster
byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Irenou	Irena	k1gFnSc7	Irena
Schusterovou	Schusterův	k2eAgFnSc7d1	Schusterova
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povoláním	povolání	k1gNnSc7	povolání
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgFnSc4d1	srdeční
příhodu	příhoda	k1gFnSc4	příhoda
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
70	[number]	k4	70
let	léto	k1gNnPc2	léto
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Próza	próza	k1gFnSc1	próza
===	===	k?	===
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Necestami	necesta	k1gFnPc7	necesta
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
cestopis	cestopis	k1gInSc1	cestopis
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Ján	Ján	k1gMnSc1	Ján
Štiavnický	Štiavnický	k2eAgMnSc1d1	Štiavnický
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
–	–	k?	–
Prípad	Prípad	k1gInSc1	Prípad
"	"	kIx"	"
<g/>
Puntičkár	Puntičkár	k1gInSc1	Puntičkár
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
románová	románový	k2eAgFnSc1d1	románová
novela	novela	k1gFnSc1	novela
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
–	–	k?	–
Nežiaduci	Nežiaduce	k1gFnSc3	Nežiaduce
dôkaz	dôkaz	k1gInSc4	dôkaz
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Čierny	Čierna	k1gFnSc2	Čierna
notes	notes	k1gInSc1	notes
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Bol	bol	k1gInSc1	bol
som	soma	k1gFnPc2	soma
pri	pri	k?	pri
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
próza	próza	k1gFnSc1	próza
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
Rodné	rodný	k2eAgMnPc4d1	rodný
putá	putat	k5eAaPmIp3nS	putat
<g/>
,	,	kIx,	,
životopisné	životopisný	k2eAgNnSc1d1	životopisné
dílo	dílo	k1gNnSc1	dílo
o	o	k7c6	o
malíři	malíř	k1gMnSc6	malíř
Jánovi	Ján	k1gMnSc6	Ján
Fabinim	Fabinim	k1gInSc4	Fabinim
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
So	So	kA	So
skalpelom	skalpelom	k1gInSc4	skalpelom
a	a	k8xC	a
bez	bez	k1gInSc4	bez
neho	neho	k1gNnSc4	neho
<g/>
,	,	kIx,	,
životopisné	životopisný	k2eAgNnSc4d1	životopisné
dílo	dílo	k1gNnSc4	dílo
o	o	k7c4	o
nestorovi	nestor	k1gMnSc3	nestor
slovenské	slovenský	k2eAgFnSc2d1	slovenská
chirurgie	chirurgie	k1gFnSc2	chirurgie
Jánovi	Jánův	k2eAgMnPc1d1	Jánův
Kňazovickém	Kňazovický	k2eAgInSc6d1	Kňazovický
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Ján	Ján	k1gMnSc1	Ján
Štiavnický	Štiavnický	k2eAgMnSc1d1	Štiavnický
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Stopy	stopa	k1gFnSc2	stopa
viedli	viednout	k5eAaPmAgMnP	viednout
k	k	k7c3	k
Indiánom	Indiánom	k1gInSc1	Indiánom
<g/>
,	,	kIx,	,
reportáží	reportáž	k1gFnSc7	reportáž
výprava	výprava	k1gFnSc1	výprava
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
svého	své	k1gNnSc2	své
otce	otec	k1gMnSc2	otec
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Strieborný	Strieborný	k2eAgInSc4d1	Strieborný
mercedes	mercedes	k1gInSc4	mercedes
<g/>
,	,	kIx,	,
detektivní	detektivní	k2eAgInSc4d1	detektivní
příběh	příběh	k1gInSc4	příběh
na	na	k7c6	na
základě	základ	k1gInSc6	základ
skutečnosti	skutečnost	k1gFnSc2	skutečnost
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Ultimátum	ultimátum	k1gNnSc4	ultimátum
<g/>
,	,	kIx,	,
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
politické	politický	k2eAgNnSc1d1	politické
a	a	k8xC	a
společenské	společenský	k2eAgNnSc1d1	společenské
dění	dění	k1gNnSc1	dění
v	v	k7c6	v
letech	let	k1gInPc6	let
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
–	–	k?	–
Hlavná	Hlavný	k2eAgFnSc1d1	Hlavná
<g/>
,	,	kIx,	,
o	o	k7c6	o
problémech	problém	k1gInPc6	problém
výstavby	výstavba	k1gFnSc2	výstavba
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
veľkej	veľkat	k5eAaPmRp2nS	veľkat
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
komentuje	komentovat	k5eAaBmIp3nS	komentovat
politiku	politika	k1gFnSc4	politika
po	po	k7c6	po
roku	rok	k1gInSc6	rok
1993	[number]	k4	1993
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Explózia	Explózia	k1gFnSc1	Explózia
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
faktu	fakt	k1gInSc2	fakt
–	–	k?	–
o	o	k7c6	o
výbuchu	výbuch	k1gInSc6	výbuch
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
košickém	košický	k2eAgInSc6d1	košický
paneláku	panelák	k1gInSc6	panelák
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Ako	Ako	k1gFnSc2	Ako
to	ten	k3xDgNnSc1	ten
bolo	bola	k1gFnSc5	bola
<g/>
,	,	kIx,	,
o	o	k7c6	o
předvolebních	předvolební	k2eAgFnPc6d1	předvolební
a	a	k8xC	a
koaličních	koaliční	k2eAgNnPc6d1	koaliční
jednáních	jednání	k1gNnPc6	jednání
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Rozhovory	rozhovor	k1gInPc1	rozhovor
s	s	k7c7	s
Milanom	Milanom	k1gInSc1	Milanom
Čičom	Čičom	k1gInSc1	Čičom
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
dvoma	dvoma	k1gFnSc1	dvoma
srdcami	srdca	k1gFnPc7	srdca
<g/>
,	,	kIx,	,
životní	životní	k2eAgInSc1d1	životní
příběh	příběh	k1gInSc1	příběh
Arieha	Arieh	k1gMnSc2	Arieh
Kleina	Klein	k1gMnSc2	Klein
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
holocaust	holocaust	k1gInSc4	holocaust
</s>
</p>
<p>
<s>
===	===	k?	===
Televizní	televizní	k2eAgInPc1d1	televizní
scénáře	scénář	k1gInPc1	scénář
===	===	k?	===
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
–	–	k?	–
Kolaudácia	Kolaudácia	k1gFnSc1	Kolaudácia
<g/>
,	,	kIx,	,
zábavný	zábavný	k2eAgInSc1d1	zábavný
pořad	pořad	k1gInSc1	pořad
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
zábavný	zábavný	k2eAgInSc1d1	zábavný
pořad	pořad	k1gInSc1	pořad
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Prvý	prvý	k4xOgMnSc1	prvý
deň	deň	k?	deň
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc1d1	televizní
inscenace	inscenace	k1gFnSc1	inscenace
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Umývanie	Umývanie	k1gFnSc2	Umývanie
hláv	hláva	k1gFnPc2	hláva
</s>
</p>
<p>
<s>
===	===	k?	===
Rozhlasové	rozhlasový	k2eAgInPc1d1	rozhlasový
scénáře	scénář	k1gInPc1	scénář
===	===	k?	===
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
–	–	k?	–
Prémie	prémie	k1gFnSc1	prémie
budú	budú	k?	budú
<g/>
...	...	k?	...
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Vynález	vynález	k1gInSc1	vynález
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
kvapka	kvapka	k1gFnSc1	kvapka
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Príležitosť	Príležitosť	k1gFnSc1	Príležitosť
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Prvý	prvý	k4xOgInSc1	prvý
deň	deň	k?	deň
podpredsedu	podpredsedu	k6eAd1	podpredsedu
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
–	–	k?	–
Referent	referent	k1gMnSc1	referent
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Havária	Havárium	k1gNnSc2	Havárium
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Bezzubý	bezzubý	k2eAgMnSc1d1	bezzubý
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
–	–	k?	–
Generálka	generálka	k1gFnSc1	generálka
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
–	–	k?	–
Bocatius	Bocatius	k1gInSc1	Bocatius
<g/>
,	,	kIx,	,
dvojdílná	dvojdílný	k2eAgFnSc1d1	dvojdílná
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rudolf	Rudolf	k1gMnSc1	Rudolf
Schuster	Schuster	k1gMnSc1	Schuster
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Schuster	Schustrum	k1gNnPc2	Schustrum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Rudolf	Rudolf	k1gMnSc1	Rudolf
Schuster	Schuster	k1gMnSc1	Schuster
</s>
</p>
