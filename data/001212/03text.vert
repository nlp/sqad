<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1895	[number]	k4	1895
Hroznatín	Hroznatína	k1gFnPc2	Hroznatína
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1979	[number]	k4	1979
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vedl	vést	k5eAaImAgInS	vést
1	[number]	k4	1
<g/>
.	.	kIx.	.
československý	československý	k2eAgInSc1d1	československý
armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Poválečný	poválečný	k2eAgMnSc1d1	poválečný
ministr	ministr	k1gMnSc1	ministr
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
1945	[number]	k4	1945
až	až	k9	až
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
až	až	k9	až
1975	[number]	k4	1975
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Hroznatíně	Hroznatína	k1gFnSc6	Hroznatína
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Velké	velká	k1gFnSc2	velká
Meziříčí	Meziříčí	k1gNnSc2	Meziříčí
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
okres	okres	k1gInSc1	okres
Třebíč	Třebíč	k1gFnSc4	Třebíč
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vysočině	vysočina	k1gFnSc6	vysočina
<g/>
,	,	kIx,	,
do	do	k7c2	do
staré	starý	k2eAgFnSc2d1	stará
selské	selský	k2eAgFnSc2d1	selská
rodiny	rodina	k1gFnSc2	rodina
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
a	a	k8xC	a
Františky	Františka	k1gFnPc1	Františka
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Chalupové	Chalupová	k1gFnPc1	Chalupová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
sotva	sotva	k8xS	sotva
jeden	jeden	k4xCgInSc1	jeden
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
matriky	matrika	k1gFnSc2	matrika
na	na	k7c4	na
následky	následek	k1gInPc4	následek
kopnutí	kopnutí	k1gNnSc2	kopnutí
"	"	kIx"	"
<g/>
koňskou	koňský	k2eAgFnSc7d1	koňská
nohou	noha	k1gFnSc7	noha
do	do	k7c2	do
břicha	břicho	k1gNnSc2	břicho
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
podruhé	podruhé	k6eAd1	podruhé
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c2	za
Františka	František	k1gMnSc2	František
Nejedlého	Nejedlý	k1gMnSc2	Nejedlý
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
tak	tak	k6eAd1	tak
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
se	s	k7c7	s
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
třemi	tři	k4xCgMnPc7	tři
sourozenci	sourozenec	k1gMnPc7	sourozenec
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
matky	matka	k1gFnPc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
školy	škola	k1gFnSc2	škola
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Zemskou	zemský	k2eAgFnSc4d1	zemská
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
školu	škola	k1gFnSc4	škola
ve	v	k7c6	v
Velkém	velký	k2eAgNnSc6d1	velké
Meziříčí	Meziříčí	k1gNnSc6	Meziříčí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
agronomické	agronomický	k2eAgNnSc4d1	agronomické
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
si	se	k3xPyFc3	se
doplnil	doplnit	k5eAaPmAgMnS	doplnit
praxí	praxe	k1gFnSc7	praxe
ve	v	k7c4	v
vinařství	vinařství	k1gNnSc4	vinařství
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
Josef	Josef	k1gMnSc1	Josef
i	i	k8xC	i
Ludvík	Ludvík	k1gMnSc1	Ludvík
odvedeni	odveden	k2eAgMnPc1d1	odveden
do	do	k7c2	do
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
jako	jako	k8xS	jako
domobranec	domobranec	k1gMnSc1	domobranec
k	k	k7c3	k
pěšímu	pěší	k2eAgInSc3d1	pěší
pluku	pluk	k1gInSc3	pluk
č.	č.	k?	č.
81	[number]	k4	81
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
sestra	sestra	k1gFnSc1	sestra
Marie	Maria	k1gFnSc2	Maria
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
vdaná	vdaný	k2eAgFnSc1d1	vdaná
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
mladšími	mladý	k2eAgMnPc7d2	mladší
sourozenci	sourozenec	k1gMnPc7	sourozenec
zůstala	zůstat	k5eAaPmAgFnS	zůstat
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
druhého	druhý	k4xOgMnSc2	druhý
manžela	manžel	k1gMnSc2	manžel
na	na	k7c6	na
statku	statek	k1gInSc6	statek
hospodařit	hospodařit	k5eAaImF	hospodařit
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
byl	být	k5eAaImAgMnS	být
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
srbskou	srbský	k2eAgFnSc4d1	Srbská
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
Ludvík	Ludvík	k1gMnSc1	Ludvík
na	na	k7c4	na
ruskou	ruský	k2eAgFnSc4d1	ruská
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
přešel	přejít	k5eAaPmAgMnS	přejít
u	u	k7c2	u
Tarnopolu	Tarnopol	k1gInSc2	Tarnopol
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
nejdříve	dříve	k6eAd3	dříve
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
hasičského	hasičský	k2eAgInSc2d1	hasičský
sboru	sbor	k1gInSc2	sbor
města	město	k1gNnSc2	město
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
odborný	odborný	k2eAgInSc1d1	odborný
výcvik	výcvik	k1gInSc1	výcvik
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1916	[number]	k4	1916
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Bojoval	bojovat	k5eAaImAgMnS	bojovat
ve	v	k7c6	v
slavných	slavný	k2eAgFnPc6d1	slavná
bitvách	bitva	k1gFnPc6	bitva
u	u	k7c2	u
Zborova	Zborov	k1gInSc2	Zborov
<g/>
,	,	kIx,	,
u	u	k7c2	u
Bachmače	Bachmač	k1gInSc2	Bachmač
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
Sibiřskou	sibiřský	k2eAgFnSc4d1	sibiřská
magistrálu	magistrála	k1gFnSc4	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
legiích	legie	k1gFnPc6	legie
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
poručíkem	poručík	k1gMnSc7	poručík
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
kapitánem	kapitán	k1gMnSc7	kapitán
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1919	[number]	k4	1919
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Kurz	kurz	k1gInSc1	kurz
poddůstojnické	poddůstojnický	k2eAgFnSc2d1	poddůstojnická
školy	škola	k1gFnSc2	škola
ukončil	ukončit	k5eAaPmAgInS	ukončit
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Kurz	kurz	k1gInSc1	kurz
důstojnické	důstojnický	k2eAgFnSc2d1	důstojnická
školy	škola	k1gFnSc2	škola
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
lodních	lodní	k2eAgInPc2d1	lodní
transportů	transport	k1gInPc2	transport
přes	přes	k7c4	přes
Japonsko	Japonsko	k1gNnSc4	Japonsko
<g/>
,	,	kIx,	,
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
Panamský	panamský	k2eAgInSc1d1	panamský
průplav	průplav	k1gInSc1	průplav
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1920	[number]	k4	1920
se	se	k3xPyFc4	se
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
vrátil	vrátit	k5eAaPmAgInS	vrátit
s	s	k7c7	s
3	[number]	k4	3
<g/>
.	.	kIx.	.
plukem	pluk	k1gInSc7	pluk
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
posádky	posádka	k1gFnSc2	posádka
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
války	válka	k1gFnSc2	válka
okamžitě	okamžitě	k6eAd1	okamžitě
demobilizoval	demobilizovat	k5eAaBmAgMnS	demobilizovat
a	a	k8xC	a
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
rodného	rodný	k2eAgInSc2d1	rodný
statku	statek	k1gInSc2	statek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
hospodář	hospodář	k1gMnSc1	hospodář
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
armádě	armáda	k1gFnSc6	armáda
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
za	za	k7c4	za
neposlušnost	neposlušnost	k1gFnSc4	neposlušnost
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
španglemi	španglemi	k?	španglemi
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Krutým	krutý	k2eAgInSc7d1	krutý
trestem	trest	k1gInSc7	trest
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
armádě	armáda	k1gFnSc6	armáda
přežíval	přežívat	k5eAaImAgMnS	přežívat
z	z	k7c2	z
feudálních	feudální	k2eAgInPc2d1	feudální
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Pověsili	pověsit	k5eAaPmAgMnP	pověsit
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
palčivém	palčivý	k2eAgNnSc6d1	palčivé
slunci	slunce	k1gNnSc6	slunce
na	na	k7c4	na
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
za	za	k7c4	za
ruce	ruka	k1gFnPc4	ruka
spoutané	spoutaný	k2eAgFnPc4d1	spoutaná
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
<g/>
.	.	kIx.	.
</s>
<s>
Bratr	bratr	k1gMnSc1	bratr
tu	ten	k3xDgFnSc4	ten
trýzeň	trýzeň	k1gFnSc4	trýzeň
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
následky	následek	k1gInPc4	následek
překrvení	překrvení	k1gNnSc2	překrvení
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
úžehu	úžeh	k1gInSc2	úžeh
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
u	u	k7c2	u
srbského	srbský	k2eAgNnSc2d1	srbské
města	město	k1gNnSc2	město
Čačaku	Čačak	k1gInSc2	Čačak
v	v	k7c6	v
bratrské	bratrský	k2eAgFnSc6d1	bratrská
mohyle	mohyla	k1gFnSc6	mohyla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
pokusem	pokus	k1gInSc7	pokus
excísaře	excísař	k1gMnSc2	excísař
Karla	Karel	k1gMnSc2	Karel
zmocnit	zmocnit	k5eAaPmF	zmocnit
se	se	k3xPyFc4	se
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
trůnu	trůn	k1gInSc2	trůn
byl	být	k5eAaImAgMnS	být
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
mnozí	mnohý	k2eAgMnPc1d1	mnohý
legionáři	legionář	k1gMnPc1	legionář
<g/>
,	,	kIx,	,
mobilizován	mobilizován	k2eAgMnSc1d1	mobilizován
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
získán	získat	k5eAaPmNgInS	získat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zůstal	zůstat	k5eAaPmAgInS	zůstat
sloužit	sloužit	k5eAaImF	sloužit
v	v	k7c6	v
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
zakládající	zakládající	k2eAgFnSc3d1	zakládající
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc4	hospodářství
předal	předat	k5eAaPmAgMnS	předat
mladšímu	mladý	k2eAgMnSc3d2	mladší
bratru	bratr	k1gMnSc3	bratr
Františku	František	k1gMnSc3	František
Nejedlému	Nejedlý	k1gMnSc3	Nejedlý
a	a	k8xC	a
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
u	u	k7c2	u
svého	své	k1gNnSc2	své
mateřského	mateřský	k2eAgNnSc2d1	mateřské
3	[number]	k4	3
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Irenou	Irena	k1gFnSc7	Irena
Stratilovou	Stratilův	k2eAgFnSc7d1	Stratilova
z	z	k7c2	z
mlynářské	mlynářský	k2eAgFnSc2d1	Mlynářská
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
nedalekých	daleký	k2eNgFnPc2d1	nedaleká
Cvrčovic	Cvrčovice	k1gFnPc2	Cvrčovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
štábního	štábní	k2eAgMnSc4d1	štábní
kapitána	kapitán	k1gMnSc4	kapitán
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
službu	služba	k1gFnSc4	služba
na	na	k7c6	na
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
u	u	k7c2	u
36	[number]	k4	36
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
řadu	řada	k1gFnSc4	řada
kurzů	kurz	k1gInPc2	kurz
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
:	:	kIx,	:
střelecký	střelecký	k2eAgInSc1d1	střelecký
kurz	kurz	k1gInSc1	kurz
v	v	k7c6	v
Milovicích	Milovice	k1gFnPc6	Milovice
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stáž	stáž	k1gFnSc4	stáž
u	u	k7c2	u
dělostřeleckého	dělostřelecký	k2eAgInSc2d1	dělostřelecký
pluku	pluk	k1gInSc2	pluk
č.	č.	k?	č.
12	[number]	k4	12
v	v	k7c6	v
Užhorodě	Užhorod	k1gInSc6	Užhorod
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stáž	stáž	k1gFnSc4	stáž
u	u	k7c2	u
leteckého	letecký	k2eAgInSc2d1	letecký
pluku	pluk	k1gInSc2	pluk
v	v	k7c6	v
Piešťanech	Piešťany	k1gInPc6	Piešťany
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
maďarsky	maďarsky	k6eAd1	maďarsky
a	a	k8xC	a
složil	složit	k5eAaPmAgMnS	složit
zkoušku	zkouška	k1gFnSc4	zkouška
z	z	k7c2	z
maďarského	maďarský	k2eAgInSc2d1	maďarský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
Univerzitě	univerzita	k1gFnSc6	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nedostatku	nedostatek	k1gInSc2	nedostatek
pedagogů	pedagog	k1gMnPc2	pedagog
-	-	kIx~	-
důstojníků	důstojník	k1gMnPc2	důstojník
znalých	znalý	k2eAgMnPc2d1	znalý
maďarštiny	maďarština	k1gFnSc2	maďarština
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pověřen	pověřen	k2eAgMnSc1d1	pověřen
její	její	k3xOp3gFnSc7	její
výukou	výuka	k1gFnSc7	výuka
na	na	k7c6	na
Vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
v	v	k7c6	v
Hranicích	Hranice	k1gFnPc6	Hranice
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
podplukovníka	podplukovník	k1gMnSc4	podplukovník
a	a	k8xC	a
převelen	převelen	k2eAgInSc4d1	převelen
ke	k	k7c3	k
svému	své	k1gNnSc3	své
3	[number]	k4	3
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Prošel	projít	k5eAaPmAgMnS	projít
řadou	řada	k1gFnSc7	řada
velitelských	velitelský	k2eAgFnPc2d1	velitelská
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
velitelem	velitel	k1gMnSc7	velitel
náhradního	náhradní	k2eAgInSc2d1	náhradní
praporu	prapor	k1gInSc2	prapor
<g/>
,	,	kIx,	,
instrukčních	instrukční	k2eAgInPc2d1	instrukční
kurzů	kurz	k1gInPc2	kurz
záložních	záložní	k2eAgMnPc2d1	záložní
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
velitel	velitel	k1gMnSc1	velitel
náhradního	náhradní	k2eAgInSc2d1	náhradní
praporu	prapor	k1gInSc2	prapor
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
pověřen	pověřit	k5eAaPmNgMnS	pověřit
přípravou	příprava	k1gFnSc7	příprava
mobilizace	mobilizace	k1gFnPc1	mobilizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
odešel	odejít	k5eAaPmAgInS	odejít
s	s	k7c7	s
mobilizovaným	mobilizovaný	k2eAgInSc7d1	mobilizovaný
praporem	prapor	k1gInSc7	prapor
do	do	k7c2	do
pohraničí	pohraničí	k1gNnSc2	pohraničí
-	-	kIx~	-
Kounice	Kounice	k1gFnSc1	Kounice
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
musel	muset	k5eAaImAgInS	muset
předat	předat	k5eAaPmF	předat
kasárna	kasárna	k1gNnPc4	kasárna
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
rakouskému	rakouský	k2eAgMnSc3d1	rakouský
plukovníkovi	plukovník	k1gMnSc3	plukovník
z	z	k7c2	z
nacistické	nacistický	k2eAgFnSc2d1	nacistická
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
okupovala	okupovat	k5eAaBmAgFnS	okupovat
Kroměříž	Kroměříž	k1gFnSc4	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
si	se	k3xPyFc3	se
Svobodovi	Svobodův	k2eAgMnPc1d1	Svobodův
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
postavili	postavit	k5eAaPmAgMnP	postavit
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jim	on	k3xPp3gMnPc3	on
po	po	k7c6	po
záboru	zábor	k1gInSc6	zábor
pohraničí	pohraničí	k1gNnSc2	pohraničí
umožnil	umožnit	k5eAaPmAgInS	umožnit
poskytnout	poskytnout	k5eAaPmF	poskytnout
azyl	azyl	k1gInSc4	azyl
dvěma	dva	k4xCgFnPc7	dva
rodinám	rodina	k1gFnPc3	rodina
vyhnaným	vyhnaný	k2eAgMnPc3d1	vyhnaný
z	z	k7c2	z
pohraničí	pohraničí	k1gNnSc2	pohraničí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domě	dům	k1gInSc6	dům
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozbití	rozbití	k1gNnSc6	rozbití
Československa	Československo	k1gNnSc2	Československo
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
okupací	okupace	k1gFnSc7	okupace
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgInS	zapojit
do	do	k7c2	do
organizování	organizování	k1gNnSc2	organizování
vojenské	vojenský	k2eAgFnSc2d1	vojenská
odbojové	odbojový	k2eAgFnSc2d1	odbojová
organizace	organizace	k1gFnSc2	organizace
Obrana	obrana	k1gFnSc1	obrana
národa	národ	k1gInSc2	národ
na	na	k7c4	na
Kroměřížsku	Kroměřížska	k1gFnSc4	Kroměřížska
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
1939	[number]	k4	1939
přešel	přejít	k5eAaPmAgInS	přejít
ilegálně	ilegálně	k6eAd1	ilegálně
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Důstojníků	důstojník	k1gMnPc2	důstojník
jeho	on	k3xPp3gInSc2	on
věku	věk	k1gInSc2	věk
a	a	k8xC	a
hodnosti	hodnost	k1gFnSc2	hodnost
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
zanechali	zanechat	k5eAaPmAgMnP	zanechat
doma	doma	k6eAd1	doma
rodinu	rodina	k1gFnSc4	rodina
v	v	k7c6	v
nejistotě	nejistota	k1gFnSc6	nejistota
a	a	k8xC	a
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
a	a	k8xC	a
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
velel	velet	k5eAaImAgMnS	velet
vojenské	vojenský	k2eAgFnSc3d1	vojenská
skupině	skupina	k1gFnSc3	skupina
ve	v	k7c6	v
vojenském	vojenský	k2eAgInSc6d1	vojenský
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
-	-	kIx~	-
Malých	Malých	k2eAgInPc6d1	Malých
Bronowicích	Bronowik	k1gInPc6	Bronowik
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
tábor	tábor	k1gInSc4	tábor
procházely	procházet	k5eAaImAgFnP	procházet
stovky	stovka	k1gFnPc1	stovka
emigrujících	emigrující	k2eAgMnPc2d1	emigrující
mladších	mladý	k2eAgMnPc2d2	mladší
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
bylo	být	k5eAaImAgNnS	být
vypraveno	vypravit	k5eAaPmNgNnS	vypravit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
1200	[number]	k4	1200
letců	letec	k1gMnPc2	letec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
zůstali	zůstat	k5eAaPmAgMnP	zůstat
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
jednotku	jednotka	k1gFnSc4	jednotka
na	na	k7c6	na
území	území	k1gNnSc6	území
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
prezident	prezident	k1gMnSc1	prezident
povolil	povolit	k5eAaPmAgMnS	povolit
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
jednotku	jednotka	k1gFnSc4	jednotka
označenou	označený	k2eAgFnSc4d1	označená
"	"	kIx"	"
<g/>
Legion	legion	k1gInSc4	legion
český	český	k2eAgInSc4d1	český
a	a	k8xC	a
slovenský	slovenský	k2eAgInSc4d1	slovenský
<g/>
"	"	kIx"	"
až	až	k9	až
třetí	třetí	k4xOgInSc1	třetí
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
Polska	Polsko	k1gNnSc2	Polsko
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohl	moct	k5eAaImAgInS	moct
jen	jen	k9	jen
málo	málo	k6eAd1	málo
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
převedl	převést	k5eAaPmAgMnS	převést
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
skupinu	skupina	k1gFnSc4	skupina
více	hodně	k6eAd2	hodně
než	než	k8xS	než
700	[number]	k4	700
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
do	do	k7c2	do
azylu	azyl	k1gInSc2	azyl
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Únik	únik	k1gInSc1	únik
skupiny	skupina	k1gFnSc2	skupina
do	do	k7c2	do
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
,	,	kIx,	,
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
Rumuni	Rumun	k1gMnPc1	Rumun
předali	předat	k5eAaPmAgMnP	předat
Němcům	Němec	k1gMnPc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
ihned	ihned	k6eAd1	ihned
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
Východní	východní	k2eAgFnSc4d1	východní
skupinu	skupina	k1gFnSc4	skupina
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
přešla	přejít	k5eAaPmAgFnS	přejít
beze	beze	k7c2	beze
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
v	v	k7c6	v
civilu	civil	k1gMnSc6	civil
jako	jako	k8xC	jako
vojenský	vojenský	k2eAgInSc1d1	vojenský
útvar	útvar	k1gInSc1	útvar
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vyslance	vyslanec	k1gMnSc2	vyslanec
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
Juraje	Juraj	k1gInSc2	Juraj
Slávika	Slávikum	k1gNnSc2	Slávikum
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
dojednání	dojednání	k1gNnSc6	dojednání
se	s	k7c7	s
sovětskými	sovětský	k2eAgInPc7d1	sovětský
diplomatickými	diplomatický	k2eAgInPc7d1	diplomatický
orgány	orgán	k1gInPc7	orgán
na	na	k7c6	na
území	území	k1gNnSc6	území
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jako	jako	k9	jako
civilní	civilní	k2eAgMnPc1d1	civilní
emigranti	emigrant	k1gMnPc1	emigrant
nerozptýlili	rozptýlit	k5eNaPmAgMnP	rozptýlit
po	po	k7c6	po
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
vojáci	voják	k1gMnPc1	voják
od	od	k7c2	od
veřejnosti	veřejnost	k1gFnSc2	veřejnost
izolováni	izolován	k2eAgMnPc1d1	izolován
v	v	k7c6	v
internačních	internační	k2eAgInPc6d1	internační
táborech	tábor	k1gInPc6	tábor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
vedli	vést	k5eAaImAgMnP	vést
život	život	k1gInSc4	život
podle	podle	k7c2	podle
předpisů	předpis	k1gInPc2	předpis
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
internační	internační	k2eAgInPc1d1	internační
tábory	tábor	k1gInPc1	tábor
nebyly	být	k5eNaImAgInP	být
ani	ani	k8xC	ani
zajatecké	zajatecký	k2eAgInPc1d1	zajatecký
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
pracovní	pracovní	k2eAgInSc1d1	pracovní
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
gulagy	gulag	k1gInPc1	gulag
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
internace	internace	k1gFnSc2	internace
"	"	kIx"	"
<g/>
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojenské	vojenský	k2eAgFnSc2d1	vojenská
skupiny	skupina	k1gFnSc2	skupina
východní	východní	k2eAgFnSc2d1	východní
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
místech	místo	k1gNnPc6	místo
<g/>
:	:	kIx,	:
Kamenec	Kamenec	k1gInSc1	Kamenec
Podolský	podolský	k2eAgInSc1d1	podolský
<g/>
,	,	kIx,	,
Olchovce	Olchovka	k1gFnSc6	Olchovka
<g/>
,	,	kIx,	,
Jarmolince	Jarmolinka	k1gFnSc6	Jarmolinka
<g/>
,	,	kIx,	,
Oranky	Oranky	k1gFnPc1	Oranky
<g/>
,	,	kIx,	,
Spaso-Jevfimijův	Spaso-Jevfimijův	k2eAgInSc1d1	Spaso-Jevfimijův
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
Suzdalu	Suzdal	k1gInSc6	Suzdal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
SSSR	SSSR	kA	SSSR
nebylo	být	k5eNaImAgNnS	být
čs	čs	kA	čs
<g/>
.	.	kIx.	.
diplomatické	diplomatický	k2eAgNnSc4d1	diplomatické
zastoupení	zastoupení	k1gNnSc4	zastoupení
a	a	k8xC	a
pplk.	pplk.	kA	pplk.
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
vedl	vést	k5eAaImAgMnS	vést
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
diplomatická	diplomatický	k2eAgNnPc1d1	diplomatické
jednání	jednání	k1gNnPc1	jednání
se	s	k7c7	s
sovětskými	sovětský	k2eAgInPc7d1	sovětský
orgány	orgán	k1gInPc7	orgán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
vojenská	vojenský	k2eAgFnSc1d1	vojenská
skupina	skupina	k1gFnSc1	skupina
udržena	udržen	k2eAgFnSc1d1	udržena
v	v	k7c6	v
celku	celek	k1gInSc6	celek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
materiálně	materiálně	k6eAd1	materiálně
podporována	podporovat	k5eAaImNgFnS	podporovat
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
poddůstojníků	poddůstojník	k1gMnPc2	poddůstojník
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
byla	být	k5eAaImAgFnS	být
přepravena	přepravit	k5eAaPmNgFnS	přepravit
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
(	(	kIx(	(
<g/>
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
pádu	pád	k1gInSc6	pád
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Střední	střední	k2eAgInSc4d1	střední
východ	východ	k1gInSc4	východ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
vypraveno	vypravit	k5eAaPmNgNnS	vypravit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Sovětů	Sověty	k1gInPc2	Sověty
12	[number]	k4	12
transportů	transport	k1gInPc2	transport
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
662	[number]	k4	662
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
12	[number]	k4	12
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
6	[number]	k4	6
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
čs	čs	kA	čs
<g/>
.	.	kIx.	.
politické	politický	k2eAgNnSc1d1	politické
vedení	vedení	k1gNnSc1	vedení
v	v	k7c6	v
emigraci	emigrace	k1gFnSc6	emigrace
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Benešem	Beneš	k1gMnSc7	Beneš
nebylo	být	k5eNaImAgNnS	být
československými	československý	k2eAgMnPc7d1	československý
předmnichovskými	předmnichovský	k2eAgMnPc7d1	předmnichovský
spojenci	spojenec	k1gMnPc7	spojenec
-	-	kIx~	-
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
-	-	kIx~	-
uznáváno	uznáván	k2eAgNnSc1d1	uznáváno
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
neprohlásili	prohlásit	k5eNaPmAgMnP	prohlásit
mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
a	a	k8xC	a
protektorátní	protektorátní	k2eAgFnPc4d1	protektorátní
hranice	hranice	k1gFnPc4	hranice
za	za	k7c4	za
neplatné	platný	k2eNgNnSc4d1	neplatné
<g/>
.	.	kIx.	.
</s>
<s>
Českoslovenští	československý	k2eAgMnPc1d1	československý
letci	letec	k1gMnPc1	letec
museli	muset	k5eAaImAgMnP	muset
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
až	až	k9	až
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
napadení	napadení	k1gNnSc2	napadení
Německem	Německo	k1gNnSc7	Německo
sloužit	sloužit	k5eAaImF	sloužit
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
cizinecké	cizinecký	k2eAgFnSc6d1	cizinecká
legii	legie	k1gFnSc6	legie
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
hodnosti	hodnost	k1gFnSc2	hodnost
jim	on	k3xPp3gMnPc3	on
byly	být	k5eAaImAgInP	být
sníženy	snížit	k5eAaPmNgInP	snížit
nebo	nebo	k8xC	nebo
neuznány	uznat	k5eNaPmNgInP	uznat
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
SSSR	SSSR	kA	SSSR
Německem	Německo	k1gNnSc7	Německo
byla	být	k5eAaImAgFnS	být
československými	československý	k2eAgMnPc7d1	československý
politickými	politický	k2eAgMnPc7d1	politický
představiteli	představitel	k1gMnPc7	představitel
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1941	[number]	k4	1941
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
o	o	k7c6	o
obnově	obnova	k1gFnSc6	obnova
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
styků	styk	k1gInPc2	styk
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
spolupráci	spolupráce	k1gFnSc6	spolupráce
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
organizovat	organizovat	k5eAaBmF	organizovat
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
československou	československý	k2eAgFnSc4d1	Československá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Pplk.	pplk.	kA	pplk.
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
této	tento	k3xDgFnSc2	tento
vojenské	vojenský	k2eAgFnSc2d1	vojenská
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
podmínkách	podmínka	k1gFnPc6	podmínka
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
sovětských	sovětský	k2eAgFnPc2d1	sovětská
a	a	k8xC	a
československých	československý	k2eAgFnPc2d1	Československá
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
plk.	plk.	kA	plk.
Heliodor	Heliodor	k1gInSc1	Heliodor
Píka	píka	k1gFnSc1	píka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
několikrát	několikrát	k6eAd1	několikrát
opustil	opustit	k5eAaPmAgMnS	opustit
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
odjel	odjet	k5eAaPmAgInS	odjet
do	do	k7c2	do
Istanbulu	Istanbul	k1gInSc2	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1941	[number]	k4	1941
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
tajná	tajný	k2eAgFnSc1d1	tajná
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojenská	vojenský	k2eAgFnSc1d1	vojenská
mise	mise	k1gFnSc1	mise
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
velitelem	velitel	k1gMnSc7	velitel
byl	být	k5eAaImAgInS	být
plk.	plk.	kA	plk.
Heliodor	Heliodora	k1gFnPc2	Heliodora
Píka	píka	k1gFnSc1	píka
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
zástupcem	zástupce	k1gMnSc7	zástupce
pplk.	pplk.	kA	pplk.
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
a	a	k8xC	a
zpravodajský	zpravodajský	k2eAgMnSc1d1	zpravodajský
důstojník	důstojník	k1gMnSc1	důstojník
Hieke-Stoj	Hieke-Stoj	k1gInSc4	Hieke-Stoj
kontaktovali	kontaktovat	k5eAaImAgMnP	kontaktovat
L.	L.	kA	L.
Krna	Krnus	k1gMnSc2	Krnus
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc2	zástupce
vyslance	vyslanec	k1gMnSc2	vyslanec
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Hieke-Stoj	Hieke-Stoj	k1gInSc1	Hieke-Stoj
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
diplomata	diplomat	k1gMnSc4	diplomat
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
akcí	akce	k1gFnSc7	akce
byl	být	k5eAaImAgMnS	být
Svoboda	Svoboda	k1gMnSc1	Svoboda
zadržen	zadržet	k5eAaPmNgMnS	zadržet
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
kontrarozvědkou	kontrarozvědka	k1gFnSc7	kontrarozvědka
a	a	k8xC	a
obviněn	obvinit	k5eAaPmNgMnS	obvinit
ze	z	k7c2	z
spolčení	spolčení	k1gNnSc2	spolčení
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
ze	z	k7c2	z
špionáže	špionáž	k1gFnSc2	špionáž
<g/>
.	.	kIx.	.
</s>
<s>
Nedorozumění	nedorozumění	k1gNnSc1	nedorozumění
se	se	k3xPyFc4	se
vysvětlilo	vysvětlit	k5eAaPmAgNnS	vysvětlit
a	a	k8xC	a
Svoboda	Svoboda	k1gMnSc1	Svoboda
byl	být	k5eAaImAgMnS	být
obvinění	obvinění	k1gNnSc4	obvinění
zproštěn	zproštěn	k2eAgMnSc1d1	zproštěn
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
spekulativních	spekulativní	k2eAgFnPc2d1	spekulativní
úvah	úvaha	k1gFnPc2	úvaha
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
L.	L.	kA	L.
Svoboda	svoboda	k1gFnSc1	svoboda
"	"	kIx"	"
<g/>
zavázal	zavázat	k5eAaPmAgMnS	zavázat
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
se	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
tajnou	tajný	k2eAgFnSc7d1	tajná
službou	služba	k1gFnSc7	služba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
pak	pak	k6eAd1	pak
setrval	setrvat	k5eAaPmAgMnS	setrvat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
však	však	k9	však
nebyly	být	k5eNaImAgInP	být
nalezeny	nalezen	k2eAgInPc1d1	nalezen
důkazy	důkaz	k1gInPc1	důkaz
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
Svobodově	Svobodův	k2eAgInSc6d1	Svobodův
deníku	deník	k1gInSc6	deník
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
záležitosti	záležitost	k1gFnSc3	záležitost
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
zápis	zápis	k1gInSc1	zápis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
objasňuje	objasňovat	k5eAaImIp3nS	objasňovat
důvod	důvod	k1gInSc4	důvod
jeho	jeho	k3xOp3gNnSc3	jeho
zadržení	zadržení	k1gNnSc3	zadržení
a	a	k8xC	a
obvinění	obvinění	k1gNnSc4	obvinění
ze	z	k7c2	z
špionáže	špionáž	k1gFnSc2	špionáž
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
mise	mise	k1gFnSc1	mise
plk.	plk.	kA	plk.
Píka	píka	k1gFnSc1	píka
opomenul	opomenout	k5eAaPmAgInS	opomenout
oznámit	oznámit	k5eAaPmF	oznámit
kontakty	kontakt	k1gInPc4	kontakt
jeho	jeho	k3xOp3gFnSc2	jeho
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
členů	člen	k1gInPc2	člen
mise	mise	k1gFnSc2	mise
s	s	k7c7	s
diplomatem	diplomat	k1gMnSc7	diplomat
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Krnem	Krnem	k1gInSc1	Krnem
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
opomenutím	opomenutí	k1gNnSc7	opomenutí
"	"	kIx"	"
<g/>
...	...	k?	...
přivedl	přivést	k5eAaPmAgMnS	přivést
misi	mise	k1gFnSc4	mise
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
nepříjemné	příjemný	k2eNgFnSc2d1	nepříjemná
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jeho	jeho	k3xOp3gMnSc3	jeho
i	i	k9	i
moje	můj	k3xOp1gNnSc4	můj
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
a	a	k8xC	a
důvěra	důvěra	k1gFnSc1	důvěra
byla	být	k5eAaImAgFnS	být
otřesena	otřást	k5eAaPmNgFnS	otřást
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
požadován	požadován	k2eAgInSc4d1	požadován
šifrovací	šifrovací	k2eAgInSc4d1	šifrovací
klíč	klíč	k1gInSc4	klíč
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zostřena	zostřen	k2eAgFnSc1d1	zostřena
kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
radioprovozem	radioprovoz	k1gInSc7	radioprovoz
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Činnost	činnost	k1gFnSc1	činnost
L.	L.	kA	L.
Svobody	svoboda	k1gFnSc2	svoboda
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
další	další	k2eAgNnSc4d1	další
období	období	k1gNnSc4	období
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
organizací	organizace	k1gFnSc7	organizace
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojska	vojsko	k1gNnPc4	vojsko
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
velením	velení	k1gNnSc7	velení
<g/>
.	.	kIx.	.
</s>
<s>
Pplk.	pplk.	kA	pplk.
Svoboda	Svoboda	k1gMnSc1	Svoboda
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
93	[number]	k4	93
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
poddůstojníků	poddůstojník	k1gMnPc2	poddůstojník
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
během	během	k7c2	během
internace	internace	k1gFnSc2	internace
připravoval	připravovat	k5eAaImAgInS	připravovat
(	(	kIx(	(
<g/>
Oranská	Oranská	k1gFnSc1	Oranská
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
z	z	k7c2	z
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
-	-	kIx~	-
československých	československý	k2eAgMnPc2d1	československý
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
konců	konec	k1gInPc2	konec
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
samostatný	samostatný	k2eAgInSc1d1	samostatný
polní	polní	k2eAgInSc1d1	polní
prapor	prapor	k1gInSc1	prapor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
organizačně	organizačně	k6eAd1	organizačně
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
struktury	struktura	k1gFnSc2	struktura
sovětského	sovětský	k2eAgNnSc2d1	sovětské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jednotky	jednotka	k1gFnSc2	jednotka
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
Slováci	Slovák	k1gMnPc1	Slovák
i	i	k8xC	i
Rusíni	Rusín	k1gMnPc1	Rusín
z	z	k7c2	z
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
krajané	krajan	k1gMnPc1	krajan
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
němečtí	německý	k2eAgMnPc1d1	německý
a	a	k8xC	a
maďarští	maďarský	k2eAgMnPc1d1	maďarský
antifašisté	antifašista	k1gMnPc1	antifašista
-	-	kIx~	-
čsl	čsl	kA	čsl
<g/>
.	.	kIx.	.
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
přijal	přijmout	k5eAaPmAgMnS	přijmout
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
proti	proti	k7c3	proti
řádu	řád	k1gInSc3	řád
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Příchodem	příchod	k1gInSc7	příchod
dalších	další	k2eAgMnPc2d1	další
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Rusínů	Rusín	k1gMnPc2	Rusín
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
z	z	k7c2	z
gulagů	gulag	k1gInPc2	gulag
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přešli	přejít	k5eAaPmAgMnP	přejít
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
prapor	prapor	k1gInSc1	prapor
postupně	postupně	k6eAd1	postupně
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
na	na	k7c4	na
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
smíšenou	smíšený	k2eAgFnSc4d1	smíšená
brigádu	brigáda	k1gFnSc4	brigáda
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
na	na	k7c4	na
armádní	armádní	k2eAgInSc4d1	armádní
sbor	sbor	k1gInSc4	sbor
-	-	kIx~	-
největší	veliký	k2eAgFnSc4d3	veliký
československou	československý	k2eAgFnSc4d1	Československá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
jednotku	jednotka	k1gFnSc4	jednotka
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgNnSc1d1	Československé
vojsko	vojsko	k1gNnSc1	vojsko
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
působilo	působit	k5eAaImAgNnS	působit
na	na	k7c6	na
frontách	fronta	k1gFnPc6	fronta
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nejdéle	dlouho	k6eAd3	dlouho
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
jednotek	jednotka	k1gFnPc2	jednotka
našeho	náš	k3xOp1gNnSc2	náš
zahraničního	zahraniční	k2eAgNnSc2d1	zahraniční
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1943	[number]	k4	1943
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
plukovníka	plukovník	k1gMnSc4	plukovník
<g/>
.	.	kIx.	.
</s>
<s>
Plk.	plk.	kA	plk.
Svoboda	Svoboda	k1gMnSc1	Svoboda
velel	velet	k5eAaImAgMnS	velet
praporu	prapor	k1gInSc3	prapor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgInS	vyznamenat
u	u	k7c2	u
Sokolova	Sokolov	k1gInSc2	Sokolov
(	(	kIx(	(
<g/>
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
odvetné	odvetný	k2eAgFnSc3d1	odvetná
operaci	operace	k1gFnSc3	operace
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
za	za	k7c4	za
Stalingrad	Stalingrad	k1gInSc4	Stalingrad
a	a	k8xC	a
Charkov	Charkov	k1gInSc4	Charkov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velel	velet	k5eAaImAgMnS	velet
brigádě	brigáda	k1gFnSc3	brigáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sehrála	sehrát	k5eAaPmAgFnS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
osvobození	osvobození	k1gNnSc6	osvobození
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
Kyjeva	Kyjev	k1gInSc2	Kyjev
a	a	k8xC	a
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
západní	západní	k2eAgFnSc4d1	západní
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1943	[number]	k4	1943
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
Kyjeva	Kyjev	k1gInSc2	Kyjev
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
brigádním	brigádní	k2eAgMnSc7d1	brigádní
generálem	generál	k1gMnSc7	generál
<g/>
.	.	kIx.	.
</s>
<s>
Brigáda	brigáda	k1gFnSc1	brigáda
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
velením	velení	k1gNnSc7	velení
osvobozovala	osvobozovat	k5eAaImAgFnS	osvobozovat
města	město	k1gNnSc2	město
Rudu	ruda	k1gFnSc4	ruda
<g/>
,	,	kIx,	,
Bílou	bílý	k2eAgFnSc4d1	bílá
Cerkev	cerkev	k1gFnSc4	cerkev
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Žaškova	Žaškov	k1gInSc2	Žaškov
se	se	k3xPyFc4	se
brigáda	brigáda	k1gFnSc1	brigáda
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
Korsuň	Korsuň	k1gFnSc4	Korsuň
-	-	kIx~	-
ševčenkovské	ševčenkovský	k2eAgFnPc4d1	ševčenkovský
operace	operace	k1gFnPc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1944	[number]	k4	1944
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
brigádní	brigádní	k2eAgMnSc1d1	brigádní
generál	generál	k1gMnSc1	generál
Jan	Jan	k1gMnSc1	Jan
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
velením	velení	k1gNnSc7	velení
byl	být	k5eAaImAgInS	být
sbor	sbor	k1gInSc1	sbor
nasazen	nasadit	k5eAaPmNgInS	nasadit
v	v	k7c6	v
Karpatsko-dukelské	karpatskoukelský	k2eAgFnSc6d1	karpatsko-dukelská
operaci	operace	k1gFnSc6	operace
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Dňa	Dňa	k1gFnSc1	Dňa
10	[number]	k4	10
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1944	[number]	k4	1944
velenie	velenie	k1gFnPc4	velenie
nad	nad	k7c7	nad
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
zborom	zborom	k1gInSc1	zborom
prevzal	prevzat	k5eAaImAgInS	prevzat
od	od	k7c2	od
brigadneho	brigadne	k1gMnSc4	brigadne
generala	general	k1gMnSc2	general
Kratochvíla	Kratochvíl	k1gMnSc2	Kratochvíl
Jana	Jan	k1gMnSc2	Jan
brigádny	brigádna	k1gFnSc2	brigádna
generál	generál	k1gMnSc1	generál
Svoboda	Svoboda	k1gMnSc1	Svoboda
Ludvík	Ludvík	k1gMnSc1	Ludvík
(	(	kIx(	(
<g/>
na	na	k7c6	na
základe	základ	k1gInSc5	základ
rozkazu	rozkaz	k1gInSc2	rozkaz
veliteľa	veliteľ	k1gInSc2	veliteľ
1	[number]	k4	1
<g/>
.	.	kIx.	.
ukrajinskeho	ukrajinskeze	k6eAd1	ukrajinskeze
frontu	fronta	k1gFnSc4	fronta
maršála	maršál	k1gMnSc2	maršál
ZSSR	ZSSR	kA	ZSSR
Koneva	Konev	k1gMnSc2	Konev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zmena	Zmeen	k2eAgFnSc1d1	Zmena
veliteľa	veliteľ	k2eAgFnSc1d1	veliteľ
súvisela	súvisela	k1gFnSc1	súvisela
s	s	k7c7	s
neúspešným	úspešný	k2eNgMnPc3d1	úspešný
začatím	začatit	k5eAaPmIp1nS	začatit
operácie	operácie	k1gFnPc4	operácie
dňa	dňa	k?	dňa
9	[number]	k4	9
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
J.	J.	kA	J.
Bílek	Bílek	k1gMnSc1	Bílek
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
odvolání	odvolání	k1gNnSc4	odvolání
Kratochvíla	Kratochvíl	k1gMnSc2	Kratochvíl
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
neoprávněné	oprávněný	k2eNgNnSc1d1	neoprávněné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Bystrický	bystrický	k2eAgInSc4d1	bystrický
cituje	citovat	k5eAaBmIp3nS	citovat
hodnocení	hodnocení	k1gNnSc3	hodnocení
čs	čs	kA	čs
<g/>
.	.	kIx.	.
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
uznalo	uznat	k5eAaPmAgNnS	uznat
důvody	důvod	k1gInPc4	důvod
Koněvova	Koněvův	k2eAgNnSc2d1	Koněvův
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
byl	být	k5eAaImAgInS	být
organizován	organizovat	k5eAaBmNgInS	organizovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
příchodu	příchod	k1gInSc2	příchod
dalších	další	k2eAgMnPc2d1	další
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Volyňských	volyňský	k2eAgMnPc2d1	volyňský
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgInS	vyznamenat
v	v	k7c6	v
Karpatsko-dukelské	karpatskoukelský	k2eAgFnSc6d1	karpatsko-dukelská
operaci	operace	k1gFnSc6	operace
-	-	kIx~	-
největší	veliký	k2eAgFnSc3d3	veliký
horské	horský	k2eAgFnSc3d1	horská
operaci	operace	k1gFnSc3	operace
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
největší	veliký	k2eAgFnSc4d3	veliký
operaci	operace	k1gFnSc4	operace
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Dělostřelci	dělostřelec	k1gMnPc1	dělostřelec
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
mohutné	mohutný	k2eAgFnPc4d1	mohutná
dělostřelecké	dělostřelecký	k2eAgFnPc4d1	dělostřelecká
přípravy	příprava	k1gFnPc4	příprava
v	v	k7c6	v
Jaselské	jaselský	k2eAgFnSc6d1	Jaselská
operaci	operace	k1gFnSc6	operace
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgInSc4d1	směřující
k	k	k7c3	k
osvobození	osvobození	k1gNnSc3	osvobození
Krakova	Krakov	k1gInSc2	Krakov
a	a	k8xC	a
východního	východní	k2eAgNnSc2d1	východní
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
osvobozoval	osvobozovat	k5eAaImAgInS	osvobozovat
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mobilizaci	mobilizace	k1gFnSc6	mobilizace
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
zapojením	zapojení	k1gNnSc7	zapojení
místních	místní	k2eAgMnPc2d1	místní
partyzánů	partyzán	k1gMnPc2	partyzán
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
vojáků	voják	k1gMnPc2	voják
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
velení	velení	k1gNnSc2	velení
nad	nad	k7c7	nad
sborem	sbor	k1gInSc7	sbor
převzal	převzít	k5eAaPmAgMnS	převzít
generál	generál	k1gMnSc1	generál
Karel	Karel	k1gMnSc1	Karel
Klapálek	Klapálek	k?	Klapálek
<g/>
.	.	kIx.	.
</s>
<s>
Ostravsko-opavské	ostravskopavský	k2eAgFnSc2d1	ostravsko-opavská
operace	operace	k1gFnSc2	operace
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
čs	čs	kA	čs
<g/>
.	.	kIx.	.
tanková	tankový	k2eAgFnSc1d1	tanková
brigáda	brigáda	k1gFnSc1	brigáda
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
smíšenou	smíšený	k2eAgFnSc7d1	smíšená
leteckou	letecký	k2eAgFnSc7d1	letecká
divizí	divize	k1gFnSc7	divize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
sboru	sbor	k1gInSc2	sbor
na	na	k7c4	na
slovenské	slovenský	k2eAgNnSc4d1	slovenské
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
na	na	k7c4	na
přímou	přímý	k2eAgFnSc4d1	přímá
žádost	žádost	k1gFnSc4	žádost
gen.	gen.	kA	gen.
L.	L.	kA	L.
Z.	Z.	kA	Z.
Mechlise	Mechlise	k1gFnSc1	Mechlise
<g/>
,	,	kIx,	,
člena	člen	k1gMnSc4	člen
vojenské	vojenský	k2eAgFnSc2d1	vojenská
rady	rada	k1gFnSc2	rada
4	[number]	k4	4
<g/>
.	.	kIx.	.
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
frontu	front	k1gInSc2	front
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
koordinoval	koordinovat	k5eAaBmAgInS	koordinovat
činnost	činnost	k1gFnSc4	činnost
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
zřídil	zřídit	k5eAaPmAgMnS	zřídit
při	při	k7c6	při
armádním	armádní	k2eAgInSc6d1	armádní
sboru	sbor	k1gInSc6	sbor
vojenské	vojenský	k2eAgNnSc1d1	vojenské
obranné	obranný	k2eAgNnSc1d1	obranné
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
(	(	kIx(	(
<g/>
OBZ	obza	k1gFnPc2	obza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Svoboda	Svoboda	k1gMnSc1	Svoboda
původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgMnS	chtít
vedením	vedení	k1gNnSc7	vedení
OBZ	obza	k1gFnPc2	obza
pověřit	pověřit	k5eAaPmF	pověřit
mjr.	mjr.	kA	mjr.
Františka	František	k1gMnSc2	František
Sedláčka	Sedláček	k1gMnSc2	Sedláček
<g/>
.	.	kIx.	.
</s>
<s>
Gen.	gen.	kA	gen.
Mechlis	Mechlis	k1gInSc1	Mechlis
však	však	k8xC	však
prosadil	prosadit	k5eAaPmAgMnS	prosadit
npor.	npor.	kA	npor.
B.	B.	kA	B.
Reicina	Reicina	k1gMnSc1	Reicina
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
tzv.	tzv.	kA	tzv.
košickou	košický	k2eAgFnSc4d1	Košická
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
jako	jako	k8xC	jako
nestraník	nestraník	k1gMnSc1	nestraník
ministrem	ministr	k1gMnSc7	ministr
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
velením	velení	k1gNnSc7	velení
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
pověřil	pověřit	k5eAaPmAgMnS	pověřit
generála	generál	k1gMnSc4	generál
Klapálka	Klapálek	k1gMnSc4	Klapálek
<g/>
.	.	kIx.	.
</s>
<s>
Klapálek	Klapálek	k?	Klapálek
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
několika	několik	k4yIc7	několik
generály	generál	k1gMnPc7	generál
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
mladších	mladý	k2eAgMnPc2d2	mladší
důstojníků	důstojník	k1gMnPc2	důstojník
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c4	na
vyžádání	vyžádání	k1gNnSc4	vyžádání
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
důstojníci	důstojník	k1gMnPc1	důstojník
posílili	posílit	k5eAaPmAgMnP	posílit
velitelský	velitelský	k2eAgInSc4d1	velitelský
sbor	sbor	k1gInSc4	sbor
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
přišli	přijít	k5eAaPmAgMnP	přijít
již	již	k6eAd1	již
k	k	k7c3	k
brigádě	brigáda	k1gFnSc3	brigáda
před	před	k7c7	před
Kyjevem	Kyjev	k1gInSc7	Kyjev
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
až	až	k6eAd1	až
ke	k	k7c3	k
sboru	sbor	k1gInSc3	sbor
a	a	k8xC	a
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
se	se	k3xPyFc4	se
osvobození	osvobození	k1gNnSc1	osvobození
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
divizního	divizní	k2eAgMnSc4d1	divizní
generála	generál	k1gMnSc4	generál
byl	být	k5eAaImAgMnS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
povýšen	povýšen	k2eAgMnSc1d1	povýšen
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Armádním	armádní	k2eAgMnSc7d1	armádní
generálem	generál	k1gMnSc7	generál
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Irena	Irena	k1gFnSc1	Irena
Svobodová	Svobodová	k1gFnSc1	Svobodová
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
manžela	manžel	k1gMnSc2	manžel
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
s	s	k7c7	s
Obranou	obrana	k1gFnSc7	obrana
národa	národ	k1gInSc2	národ
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
několika	několik	k4yIc3	několik
skupinám	skupina	k1gFnPc3	skupina
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
poddůstojníků	poddůstojník	k1gMnPc2	poddůstojník
odejít	odejít	k5eAaPmF	odejít
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
azyl	azyl	k1gInSc4	azyl
skupině	skupina	k1gFnSc3	skupina
čs	čs	kA	čs
<g/>
.	.	kIx.	.
parašutistů	parašutista	k1gMnPc2	parašutista
s	s	k7c7	s
vysílačkou	vysílačka	k1gFnSc7	vysílačka
<g/>
.	.	kIx.	.
</s>
<s>
Výsadek	výsadek	k1gInSc1	výsadek
S	s	k7c7	s
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
R	R	kA	R
byl	být	k5eAaImAgInS	být
vyslán	vyslán	k2eAgInSc4d1	vyslán
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
misí	mise	k1gFnSc7	mise
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Irena	Irena	k1gFnSc1	Irena
Svobodová	Svobodová	k1gFnSc1	Svobodová
i	i	k8xC	i
její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
parašutisty	parašutista	k1gMnPc7	parašutista
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
gestapo	gestapo	k1gNnSc1	gestapo
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
stopu	stopa	k1gFnSc4	stopa
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1941	[number]	k4	1941
zatklo	zatknout	k5eAaPmAgNnS	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bojích	boj	k1gInPc6	boj
na	na	k7c6	na
Dukle	Dukla	k1gFnSc6	Dukla
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Miroslav	Miroslav	k1gMnSc1	Miroslav
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
necelých	celý	k2eNgNnPc6d1	necelé
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Mauthausen	Mauthausen	k1gInSc1	Mauthausen
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
i	i	k9	i
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
Ireny	Irena	k1gFnSc2	Irena
Svobodové	Svobodové	k2eAgMnPc1d1	Svobodové
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
a	a	k8xC	a
Eduard	Eduard	k1gMnSc1	Eduard
a	a	k8xC	a
synovec	synovec	k1gMnSc1	synovec
Jan	Jan	k1gMnSc1	Jan
Doležal	Doležal	k1gMnSc1	Doležal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Ravensbrück	Ravensbrück	k1gInSc4	Ravensbrück
byla	být	k5eAaImAgFnS	být
zavražděna	zavražděn	k2eAgFnSc1d1	zavražděna
Anežka	Anežka	k1gFnSc1	Anežka
Stratilová	Stratilová	k1gFnSc1	Stratilová
<g/>
,	,	kIx,	,
maminka	maminka	k1gFnSc1	maminka
Ireny	Irena	k1gFnSc2	Irena
Svobodové	Svobodová	k1gFnSc2	Svobodová
<g/>
.	.	kIx.	.
</s>
<s>
Irena	Irena	k1gFnSc1	Irena
Svobodová	Svobodová	k1gFnSc1	Svobodová
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
Zoe	Zoe	k1gFnSc7	Zoe
unikly	uniknout	k5eAaPmAgFnP	uniknout
zatčení	zatčení	k1gNnSc4	zatčení
a	a	k8xC	a
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1941	[number]	k4	1941
do	do	k7c2	do
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
se	se	k3xPyFc4	se
skrývaly	skrývat	k5eAaImAgFnP	skrývat
u	u	k7c2	u
statečných	statečný	k2eAgMnPc2d1	statečný
a	a	k8xC	a
obětavých	obětavý	k2eAgMnPc2d1	obětavý
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
ukrýváním	ukrývání	k1gNnSc7	ukrývání
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
Jan	Jan	k1gMnSc1	Jan
Dokulil	Dokulil	k1gMnSc1	Dokulil
<g/>
,	,	kIx,	,
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Uhřínově	Uhřínův	k2eAgMnSc6d1	Uhřínův
-	-	kIx~	-
švagr	švagr	k1gMnSc1	švagr
katolického	katolický	k2eAgMnSc2d1	katolický
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jana	Jan	k1gMnSc2	Jan
Zahradníčka	Zahradníček	k1gMnSc2	Zahradníček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1943	[number]	k4	1943
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Džbánice	Džbánice	k1gFnSc2	Džbánice
(	(	kIx(	(
<g/>
blízko	blízko	k7c2	blízko
Moravského	moravský	k2eAgInSc2d1	moravský
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgMnPc2d1	další
15	[number]	k4	15
členů	člen	k1gMnPc2	člen
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
rodin	rodina	k1gFnPc2	rodina
manželů	manžel	k1gMnPc2	manžel
Svobodových	Svobodová	k1gFnPc2	Svobodová
bylo	být	k5eAaImAgNnS	být
tři	tři	k4xCgNnPc1	tři
roky	rok	k1gInPc4	rok
vězněno	věznit	k5eAaImNgNnS	věznit
v	v	k7c6	v
internačním	internační	k2eAgInSc6d1	internační
táboře	tábor	k1gInSc6	tábor
ve	v	k7c6	v
Svatobořicích	Svatobořice	k1gFnPc6	Svatobořice
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
byl	být	k5eAaImAgMnS	být
ministrem	ministr	k1gMnSc7	ministr
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
jmenování	jmenování	k1gNnSc6	jmenování
členem	člen	k1gMnSc7	člen
košické	košický	k2eAgFnSc2d1	Košická
vlády	vláda	k1gFnSc2	vláda
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
do	do	k7c2	do
odvolání	odvolání	k1gNnSc2	odvolání
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
převzal	převzít	k5eAaPmAgMnS	převzít
po	po	k7c6	po
Janu	Jan	k1gMnSc6	Jan
Masarykovi	Masaryk	k1gMnSc6	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
budovala	budovat	k5eAaImAgFnS	budovat
podle	podle	k7c2	podle
Košického	košický	k2eAgInSc2d1	košický
vládního	vládní	k2eAgInSc2d1	vládní
programu	program	k1gInSc2	program
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
složek	složka	k1gFnPc2	složka
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
domácího	domácí	k2eAgInSc2d1	domácí
i	i	k8xC	i
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
ustanoveních	ustanovení	k1gNnPc6	ustanovení
Košického	košický	k2eAgInSc2d1	košický
vládního	vládní	k2eAgInSc2d1	vládní
programu	program	k1gInSc2	program
se	se	k3xPyFc4	se
promítala	promítat	k5eAaImAgFnS	promítat
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
velmoci	velmoc	k1gFnPc1	velmoc
přisoudily	přisoudit	k5eAaPmAgFnP	přisoudit
Československu	Československo	k1gNnSc6	Československo
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
sovětské	sovětský	k2eAgFnSc6d1	sovětská
sféře	sféra	k1gFnSc6	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
budována	budovat	k5eAaImNgFnS	budovat
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
a	a	k8xC	a
zkušeností	zkušenost	k1gFnPc2	zkušenost
sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc1	budování
armády	armáda	k1gFnSc2	armáda
bylo	být	k5eAaImAgNnS	být
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
bojem	boj	k1gInSc7	boj
o	o	k7c6	o
získání	získání	k1gNnSc6	získání
politické	politický	k2eAgFnSc2d1	politická
převahy	převaha	k1gFnSc2	převaha
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
v	v	k7c6	v
ustanovení	ustanovení	k1gNnSc6	ustanovení
nedílné	dílný	k2eNgFnSc2d1	nedílná
vlády	vláda	k1gFnSc2	vláda
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
nastoupení	nastoupení	k1gNnSc2	nastoupení
budování	budování	k1gNnSc2	budování
sovětského	sovětský	k2eAgInSc2d1	sovětský
modelu	model	k1gInSc2	model
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předválečné	předválečný	k2eAgFnSc2d1	předválečná
apolitické	apolitický	k2eAgFnSc2d1	apolitická
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
stranická	stranický	k2eAgFnSc1d1	stranická
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Důstojníci	důstojník	k1gMnPc1	důstojník
i	i	k8xC	i
vojáci	voják	k1gMnPc1	voják
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
členy	člen	k1gInPc4	člen
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
stranického	stranický	k2eAgInSc2d1	stranický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Velitelská	velitelský	k2eAgNnPc1d1	velitelské
místa	místo	k1gNnPc1	místo
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
obsazovat	obsazovat	k5eAaImF	obsazovat
nejen	nejen	k6eAd1	nejen
podle	podle	k7c2	podle
odbornosti	odbornost	k1gFnSc2	odbornost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
podle	podle	k7c2	podle
stranické	stranický	k2eAgFnSc2d1	stranická
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
(	(	kIx(	(
<g/>
stranický	stranický	k2eAgInSc1d1	stranický
<g/>
)	)	kIx)	)
profil	profil	k1gInSc1	profil
armády	armáda	k1gFnSc2	armáda
postupně	postupně	k6eAd1	postupně
kopíroval	kopírovat	k5eAaImAgInS	kopírovat
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
výzbroj	výzbroj	k1gFnSc1	výzbroj
ani	ani	k8xC	ani
výstroj	výstroj	k1gFnSc1	výstroj
<g/>
,	,	kIx,	,
využívalo	využívat	k5eAaPmAgNnS	využívat
se	se	k3xPyFc4	se
ukořistěného	ukořistěný	k2eAgInSc2d1	ukořistěný
německého	německý	k2eAgInSc2d1	německý
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
obnově	obnova	k1gFnSc6	obnova
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Likvidovala	likvidovat	k5eAaBmAgFnS	likvidovat
skupiny	skupina	k1gFnSc2	skupina
banderovců	banderovec	k1gMnPc2	banderovec
přicházející	přicházející	k2eAgFnPc1d1	přicházející
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
přes	přes	k7c4	přes
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
terorizovaly	terorizovat	k5eAaImAgInP	terorizovat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
1946	[number]	k4	1946
KSČ	KSČ	kA	KSČ
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sociálními	sociální	k2eAgMnPc7d1	sociální
demokraty	demokrat	k1gMnPc7	demokrat
výrazně	výrazně	k6eAd1	výrazně
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgMnSc1d1	samotný
Svoboda	Svoboda	k1gMnSc1	Svoboda
ale	ale	k8xC	ale
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
důvěru	důvěra	k1gFnSc4	důvěra
jak	jak	k8xS	jak
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
politické	politický	k2eAgNnSc4d1	politické
napětí	napětí	k1gNnSc4	napětí
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
v	v	k7c4	v
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
dvanáct	dvanáct	k4xCc1	dvanáct
nekomunistických	komunistický	k2eNgMnPc2d1	nekomunistický
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
členů	člen	k1gMnPc2	člen
národně	národně	k6eAd1	národně
socialistické	socialistický	k2eAgFnSc2d1	socialistická
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgFnSc2d1	lidová
a	a	k8xC	a
slovenské	slovenský	k2eAgFnSc2d1	slovenská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
podalo	podat	k5eAaPmAgNnS	podat
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
ministři	ministr	k1gMnPc1	ministr
spoléhali	spoléhat	k5eAaImAgMnP	spoléhat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
ji	on	k3xPp3gFnSc4	on
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
a	a	k8xC	a
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
podpoří	podpořit	k5eAaPmIp3nP	podpořit
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
sympatizujících	sympatizující	k2eAgInPc2d1	sympatizující
velitelů	velitel	k1gMnPc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
vyšly	vyjít	k5eAaPmAgInP	vyjít
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
pracujících	pracující	k1gMnPc2	pracující
podpořených	podpořený	k2eAgFnPc2d1	podpořená
Lidovými	lidový	k2eAgFnPc7d1	lidová
milicemi	milice	k1gFnPc7	milice
<g/>
,	,	kIx,	,
nelegálními	legální	k2eNgFnPc7d1	nelegální
polovojenskými	polovojenský	k2eAgFnPc7d1	polovojenská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
založenými	založený	k2eAgNnPc7d1	založené
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
jako	jako	k8xC	jako
ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
stráž	stráž	k1gFnSc1	stráž
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
ovládanými	ovládaný	k2eAgFnPc7d1	ovládaná
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
zůstala	zůstat	k5eAaPmAgFnS	zůstat
neutrální	neutrální	k2eAgFnSc1d1	neutrální
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
stanoviskem	stanovisko	k1gNnSc7	stanovisko
prezidenta	prezident	k1gMnSc2	prezident
i	i	k8xC	i
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Jana	Jan	k1gMnSc2	Jan
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vystoupení	vystoupení	k1gNnSc3	vystoupení
armády	armáda	k1gFnSc2	armáda
by	by	kYmCp3nS	by
"	"	kIx"	"
<g/>
muselo	muset	k5eAaImAgNnS	muset
dát	dát	k5eAaPmF	dát
povel	povel	k1gInSc4	povel
armádní	armádní	k2eAgNnSc4d1	armádní
velení	velení	k1gNnSc4	velení
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
ministr	ministr	k1gMnSc1	ministr
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
...	...	k?	...
Oba	dva	k4xCgMnPc1	dva
zastávali	zastávat	k5eAaImAgMnP	zastávat
princip	princip	k1gInSc4	princip
nevměšování	nevměšování	k1gNnSc2	nevměšování
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
vnitropolitických	vnitropolitický	k2eAgInPc2d1	vnitropolitický
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
protikomunistickému	protikomunistický	k2eAgInSc3d1	protikomunistický
odporu	odpor	k1gInSc3	odpor
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohly	moct	k5eAaImAgFnP	moct
odhodlat	odhodlat	k5eAaPmF	odhodlat
-	-	kIx~	-
ať	ať	k9	ať
již	již	k6eAd1	již
izolovaně	izolovaně	k6eAd1	izolovaně
nebo	nebo	k8xC	nebo
v	v	k7c6	v
koordinaci	koordinace	k1gFnSc6	koordinace
-	-	kIx~	-
jen	jen	k9	jen
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
útvary	útvar	k1gInPc4	útvar
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Armádní	armádní	k2eAgNnSc1d1	armádní
velení	velení	k1gNnSc1	velení
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
muselo	muset	k5eAaImAgNnS	muset
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
neplnění	neplnění	k1gNnSc4	neplnění
jeho	jeho	k3xOp3gInSc2	jeho
rozkazu	rozkaz	k1gInSc2	rozkaz
o	o	k7c4	o
neplnění	neplnění	k1gNnSc4	neplnění
neutrality	neutralita	k1gFnSc2	neutralita
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
konečně	konečně	k6eAd1	konečně
je	být	k5eAaImIp3nS	být
tu	ten	k3xDgFnSc4	ten
ještě	ještě	k9	ještě
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
hypotetický	hypotetický	k2eAgInSc1d1	hypotetický
otazník	otazník	k1gInSc1	otazník
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
odpovědělo	odpovědět	k5eAaPmAgNnS	odpovědět
komunistické	komunistický	k2eAgNnSc1d1	komunistické
vedení	vedení	k1gNnSc1	vedení
a	a	k8xC	a
K.	K.	kA	K.
Gottwald	Gottwald	k1gMnSc1	Gottwald
jako	jako	k8xS	jako
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
Stalinovo	Stalinův	k2eAgNnSc4d1	Stalinovo
doporučení	doporučení	k1gNnSc4	doporučení
použít	použít	k5eAaPmF	použít
pomoci	pomoc	k1gFnSc2	pomoc
sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Demokratické	demokratický	k2eAgFnPc1d1	demokratická
strany	strana	k1gFnPc1	strana
vyklidily	vyklidit	k5eAaPmAgFnP	vyklidit
pozice	pozice	k1gFnPc4	pozice
a	a	k8xC	a
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
přestala	přestat	k5eAaPmAgFnS	přestat
dělit	dělit	k5eAaImF	dělit
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
demokracie	demokracie	k1gFnSc1	demokracie
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
"	"	kIx"	"
<g/>
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
úlohou	úloha	k1gFnSc7	úloha
strany	strana	k1gFnSc2	strana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slibovaná	slibovaný	k2eAgFnSc1d1	slibovaná
specifická	specifický	k2eAgFnSc1d1	specifická
československá	československý	k2eAgFnSc1d1	Československá
cesta	cesta	k1gFnSc1	cesta
socialismu	socialismus	k1gInSc2	socialismus
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgFnS	měnit
v	v	k7c6	v
kopii	kopie	k1gFnSc6	kopie
sovětského	sovětský	k2eAgInSc2d1	sovětský
modelu	model	k1gInSc2	model
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
i	i	k8xC	i
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1948	[number]	k4	1948
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mu	on	k3xPp3gMnSc3	on
bývá	bývat	k5eAaImIp3nS	bývat
připisováno	připisovat	k5eAaImNgNnS	připisovat
dřívější	dřívější	k2eAgNnSc1d1	dřívější
tajné	tajný	k2eAgNnSc1d1	tajné
členství	členství	k1gNnSc1	členství
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Karel	Karel	k1gMnSc1	Karel
Kaplan	Kaplan	k1gMnSc1	Kaplan
však	však	k9	však
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
tajných	tajný	k2eAgInPc2d1	tajný
členů	člen	k1gInPc2	člen
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsem	být	k5eAaImIp1nS	být
jej	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgMnS	mít
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Svoboda	Svoboda	k1gMnSc1	Svoboda
tam	tam	k6eAd1	tam
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
také	také	k9	také
oficiálně	oficiálně	k6eAd1	oficiálně
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
dále	daleko	k6eAd2	daleko
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
armáda	armáda	k1gFnSc1	armáda
bude	být	k5eAaImBp3nS	být
budována	budován	k2eAgFnSc1d1	budována
s	s	k7c7	s
respektováním	respektování	k1gNnSc7	respektování
legionářských	legionářský	k2eAgFnPc2d1	legionářská
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
národní	národní	k2eAgFnSc1d1	národní
a	a	k8xC	a
lidová	lidový	k2eAgFnSc1d1	lidová
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
byli	být	k5eAaImAgMnP	být
zkušení	zkušený	k2eAgMnPc1d1	zkušený
velitelé	velitel	k1gMnPc1	velitel
ze	z	k7c2	z
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
i	i	k8xC	i
domácího	domácí	k2eAgInSc2d1	domácí
odboje	odboj	k1gInSc2	odboj
a	a	k8xC	a
legionáři	legionář	k1gMnPc1	legionář
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
armádu	armáda	k1gFnSc4	armáda
gradoval	gradovat	k5eAaImAgMnS	gradovat
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
prošla	projít	k5eAaPmAgFnS	projít
radikální	radikální	k2eAgFnSc7d1	radikální
kádrovou	kádrový	k2eAgFnSc7d1	kádrová
"	"	kIx"	"
<g/>
očistou	očista	k1gFnSc7	očista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
náčelník	náčelník	k1gMnSc1	náčelník
Hlavní	hlavní	k2eAgFnSc2d1	hlavní
správy	správa	k1gFnSc2	správa
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
osvěty	osvěta	k1gFnSc2	osvěta
<g/>
,	,	kIx,	,
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
K.	K.	kA	K.
Gottwaldovi	Gottwaldův	k2eAgMnPc1d1	Gottwaldův
již	již	k6eAd1	již
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1947	[number]	k4	1947
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
generála	generál	k1gMnSc2	generál
Svobodu	Svoboda	k1gMnSc4	Svoboda
z	z	k7c2	z
"	"	kIx"	"
<g/>
politické	politický	k2eAgFnSc2d1	politická
nespolehlivosti	nespolehlivost	k1gFnSc2	nespolehlivost
<g/>
,	,	kIx,	,
kolísání	kolísání	k1gNnSc2	kolísání
a	a	k8xC	a
žádá	žádat	k5eAaImIp3nS	žádat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neprodleně	prodleně	k6eNd1	prodleně
učinil	učinit	k5eAaPmAgMnS	učinit
nápravu	náprava	k1gFnSc4	náprava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
byla	být	k5eAaImAgFnS	být
zatčena	zatčen	k2eAgFnSc1d1	zatčena
řada	řada	k1gFnSc1	řada
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
Jindřich	Jindřich	k1gMnSc1	Jindřich
Macháček	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
plk.	plk.	kA	plk.
Karel	Karel	k1gMnSc1	Karel
Střelka	střelka	k1gFnSc1	střelka
<g/>
,	,	kIx,	,
ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
vyšetřováni	vyšetřovat	k5eAaImNgMnP	vyšetřovat
kvůli	kvůli	k7c3	kvůli
L.	L.	kA	L.
Svobodovi	Svoboda	k1gMnSc3	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
nesl	nést	k5eAaImAgMnS	nést
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
politickou	politický	k2eAgFnSc4d1	politická
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
čistky	čistka	k1gFnPc4	čistka
ve	v	k7c6	v
velitelském	velitelský	k2eAgInSc6d1	velitelský
sboru	sbor	k1gInSc6	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
"	"	kIx"	"
<g/>
očistou	očista	k1gFnSc7	očista
<g/>
"	"	kIx"	"
armády	armáda	k1gFnSc2	armáda
dohlížel	dohlížet	k5eAaImAgInS	dohlížet
Armádní	armádní	k2eAgInSc1d1	armádní
poradní	poradní	k2eAgInSc1d1	poradní
sbor	sbor	k1gInSc1	sbor
(	(	kIx(	(
<g/>
APS	APS	kA	APS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
členem	člen	k1gMnSc7	člen
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
Ústřední	ústřední	k2eAgInSc4d1	ústřední
akční	akční	k2eAgInSc4d1	akční
výbor	výbor	k1gInSc4	výbor
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
generálové	generálová	k1gFnSc2	generálová
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Klapálek	Klapálek	k?	Klapálek
<g/>
,	,	kIx,	,
Boček	Boček	k1gMnSc1	Boček
<g/>
,	,	kIx,	,
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Hasal	Hasal	k1gMnSc1	Hasal
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Drgáč	Drgáč	k1gInSc1	Drgáč
a	a	k8xC	a
plk.	plk.	kA	plk.
B.	B.	kA	B.
Reicin	Reicin	k1gMnSc1	Reicin
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
"	"	kIx"	"
<g/>
Návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
propouštění	propouštění	k1gNnSc4	propouštění
vypracovávaly	vypracovávat	k5eAaImAgInP	vypracovávat
orgány	orgán	k1gInPc1	orgán
OBZ	obza	k1gFnPc2	obza
vyšších	vysoký	k2eAgNnPc2d2	vyšší
vojenských	vojenský	k2eAgNnPc2d1	vojenské
velitelství	velitelství	k1gNnPc2	velitelství
<g/>
.	.	kIx.	.
...	...	k?	...
Konečné	Konečné	k2eAgInPc4d1	Konečné
návrhy	návrh	k1gInPc4	návrh
posuzoval	posuzovat	k5eAaImAgMnS	posuzovat
plk.	plk.	kA	plk.
Bedřich	Bedřich	k1gMnSc1	Bedřich
Reicin	Reicin	k1gMnSc1	Reicin
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
je	být	k5eAaImIp3nS	být
předkládal	předkládat	k5eAaImAgMnS	předkládat
na	na	k7c6	na
jednáních	jednání	k1gNnPc6	jednání
APS	APS	kA	APS
<g/>
.	.	kIx.	.
...	...	k?	...
Při	při	k7c6	při
projednávání	projednávání	k1gNnSc6	projednávání
návrhů	návrh	k1gInPc2	návrh
v	v	k7c6	v
APS	APS	kA	APS
se	se	k3xPyFc4	se
Reicin	Reicin	k1gMnSc1	Reicin
dostával	dostávat	k5eAaImAgMnS	dostávat
často	často	k6eAd1	často
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
gen.	gen.	kA	gen.
Svobodou	svoboda	k1gFnSc7	svoboda
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
členy	člen	k1gInPc7	člen
APS	APS	kA	APS
<g/>
,	,	kIx,	,
generály	generál	k1gMnPc4	generál
Klapálkem	Klapálkem	k?	Klapálkem
<g/>
,	,	kIx,	,
Bočkem	Boček	k1gMnSc7	Boček
a	a	k8xC	a
Drgáčem	Drgáč	k1gMnSc7	Drgáč
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
(	(	kIx(	(
<g/>
Reicin	Reicin	k1gInSc4	Reicin
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Svoboda	Svoboda	k1gMnSc1	Svoboda
bral	brát	k5eAaImAgMnS	brát
v	v	k7c4	v
ochranu	ochrana	k1gFnSc4	ochrana
nespolehlivé	spolehlivý	k2eNgFnPc4d1	nespolehlivá
důstojníky	důstojník	k1gMnPc7	důstojník
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
o	o	k7c6	o
jedněch	jeden	k4xCgFnPc6	jeden
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
mladí	mladý	k2eAgMnPc1d1	mladý
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
převychovat	převychovat	k5eAaPmF	převychovat
<g/>
,	,	kIx,	,
o	o	k7c6	o
druhých	druhý	k4xOgMnPc6	druhý
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInPc4	jejich
reakční	reakční	k2eAgInPc4d1	reakční
výroky	výrok	k1gInPc4	výrok
nelze	lze	k6eNd1	lze
brát	brát	k5eAaImF	brát
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
hájil	hájit	k5eAaImAgMnS	hájit
<g/>
,	,	kIx,	,
odvolávaje	odvolávat	k5eAaImSgInS	odvolávat
se	se	k3xPyFc4	se
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
odborné	odborný	k2eAgFnPc4d1	odborná
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
bral	brát	k5eAaImAgMnS	brát
systematicky	systematicky	k6eAd1	systematicky
v	v	k7c4	v
ochranu	ochrana	k1gFnSc4	ochrana
různé	různý	k2eAgMnPc4d1	různý
reakční	reakční	k2eAgMnPc4d1	reakční
legionáře	legionář	k1gMnPc4	legionář
a	a	k8xC	a
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
byl	být	k5eAaImAgInS	být
osobně	osobně	k6eAd1	osobně
spřátelen	spřátelit	k5eAaPmNgInS	spřátelit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
stavěli	stavět	k5eAaImAgMnP	stavět
proti	proti	k7c3	proti
propuštění	propuštění	k1gNnSc3	propuštění
nespolehlivých	spolehlivý	k2eNgMnPc2d1	nespolehlivý
důstojníků	důstojník	k1gMnPc2	důstojník
i	i	k9	i
generálové	generál	k1gMnPc1	generál
Klapálek	Klapálek	k?	Klapálek
<g/>
,	,	kIx,	,
Boček	boček	k1gInSc1	boček
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
gen.	gen.	kA	gen.
Drgáč	Drgáč	k1gInSc4	Drgáč
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ke	k	k7c3	k
dni	den	k1gInSc3	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
propuštěno	propustit	k5eAaPmNgNnS	propustit
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
2	[number]	k4	2
965	[number]	k4	965
důstojníků	důstojník	k1gMnPc2	důstojník
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
13	[number]	k4	13
366	[number]	k4	366
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
propuštěných	propuštěný	k2eAgFnPc2d1	propuštěná
se	se	k3xPyFc4	se
ničím	ničí	k3xOyNgNnSc7	ničí
neprovinila	provinit	k5eNaPmAgFnS	provinit
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
verbálně	verbálně	k6eAd1	verbálně
nevyjádřila	vyjádřit	k5eNaPmAgFnS	vyjádřit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
'	'	kIx"	'
<g/>
očisty	očista	k1gFnSc2	očista
<g/>
'	'	kIx"	'
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1948	[number]	k4	1948
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podstatným	podstatný	k2eAgFnPc3d1	podstatná
změnám	změna	k1gFnPc3	změna
také	také	k9	také
na	na	k7c6	na
nejvyšších	vysoký	k2eAgNnPc6d3	nejvyšší
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1948	[number]	k4	1948
Slánský	Slánský	k1gMnSc1	Slánský
prosadil	prosadit	k5eAaPmAgMnS	prosadit
vytvoření	vytvoření	k1gNnSc4	vytvoření
funkce	funkce	k1gFnSc2	funkce
náměstka	náměstek	k1gMnSc2	náměstek
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
pro	pro	k7c4	pro
věci	věc	k1gFnPc4	věc
osobní	osobní	k2eAgNnSc4d1	osobní
a	a	k8xC	a
zřízení	zřízení	k1gNnSc4	zřízení
kádrového	kádrový	k2eAgInSc2d1	kádrový
odboru	odbor	k1gInSc2	odbor
MNO	MNO	kA	MNO
<g/>
.	.	kIx.	.
</s>
<s>
Slánský	Slánský	k1gMnSc1	Slánský
zároveň	zároveň	k6eAd1	zároveň
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
gen.	gen.	kA	gen.
Svobodovi	Svoboda	k1gMnSc3	Svoboda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
plk.	plk.	kA	plk.
Bedřich	Bedřich	k1gMnSc1	Bedřich
Reicin	Reicin	k1gMnSc1	Reicin
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgInSc7	ten
Svoboda	Svoboda	k1gMnSc1	Svoboda
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vznesl	vznést	k5eAaPmAgMnS	vznést
jsem	být	k5eAaImIp1nS	být
námitky	námitka	k1gFnPc4	námitka
proti	proti	k7c3	proti
Reicinovi	Reicin	k1gMnSc3	Reicin
a	a	k8xC	a
žádal	žádat	k5eAaImAgInS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mi	já	k3xPp1nSc3	já
strana	strana	k1gFnSc1	strana
dala	dát	k5eAaPmAgFnS	dát
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reicin	Reicin	k1gInSc1	Reicin
je	být	k5eAaImIp3nS	být
zapracován	zapracovat	k5eAaPmNgInS	zapracovat
v	v	k7c6	v
obranném	obranný	k2eAgNnSc6d1	obranné
zpravodajství	zpravodajství	k1gNnSc6	zpravodajství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gNnSc3	on
toto	tento	k3xDgNnSc1	tento
bylo	být	k5eAaImAgNnS	být
ponecháno	ponechán	k2eAgNnSc1d1	ponecháno
<g/>
.	.	kIx.	.
</s>
<s>
Znal	znát	k5eAaImAgMnS	znát
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
Slánského	Slánský	k1gMnSc4	Slánský
jsem	být	k5eAaImIp1nS	být
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
poměr	poměr	k1gInSc4	poměr
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
bojí	bát	k5eAaImIp3nP	bát
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
ho	on	k3xPp3gMnSc4	on
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
diktátor	diktátor	k1gMnSc1	diktátor
a	a	k8xC	a
zlý	zlý	k2eAgMnSc1d1	zlý
a	a	k8xC	a
mstivý	mstivý	k2eAgMnSc1d1	mstivý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Slánský	Slánský	k1gMnSc1	Slánský
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc4	ten
vymlouval	vymlouvat	k5eAaImAgMnS	vymlouvat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reicina	Reicina	k1gFnSc1	Reicina
zná	znát	k5eAaImIp3nS	znát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mýlím	mýlit	k5eAaImIp1nS	mýlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takový	takový	k3xDgInSc4	takový
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
bude	být	k5eAaImBp3nS	být
ještě	ještě	k6eAd1	ještě
uvažovat	uvažovat	k5eAaImF	uvažovat
a	a	k8xC	a
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
sdělí	sdělit	k5eAaPmIp3nS	sdělit
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
přišel	přijít	k5eAaPmAgMnS	přijít
Slánský	Slánský	k1gMnSc1	Slánský
a	a	k8xC	a
sdělil	sdělit	k5eAaPmAgMnS	sdělit
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
Reicin	Reicin	k1gInSc1	Reicin
bude	být	k5eAaImBp3nS	být
mým	můj	k1gMnSc7	můj
náměstkem	náměstek	k1gMnSc7	náměstek
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
neměl	mít	k5eNaImAgMnS	mít
obav	obava	k1gFnPc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Reicin	Reicin	k1gMnSc1	Reicin
byl	být	k5eAaImAgMnS	být
ustaven	ustavit	k5eAaPmNgMnS	ustavit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
náměstka	náměstek	k1gMnSc2	náměstek
ministra	ministr	k1gMnSc2	ministr
NO	no	k9	no
pro	pro	k7c4	pro
věci	věc	k1gFnPc4	věc
osobní	osobní	k2eAgInSc1d1	osobní
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Reicin	Reicin	k1gInSc1	Reicin
<g/>
,	,	kIx,	,
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
a	a	k8xC	a
podporou	podpora	k1gFnSc7	podpora
Gottwalda	Gottwald	k1gMnSc2	Gottwald
a	a	k8xC	a
Slánského	Slánský	k1gMnSc2	Slánský
<g/>
,	,	kIx,	,
tak	tak	k9	tak
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
soustředil	soustředit	k5eAaPmAgMnS	soustředit
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
ničím	nic	k3yNnSc7	nic
a	a	k8xC	a
nikým	nikdo	k3yNnSc7	nikdo
nekontrolovatelnou	kontrolovatelný	k2eNgFnSc4d1	nekontrolovatelná
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
působení	působení	k1gNnSc2	působení
Reicina	Reicino	k1gNnSc2	Reicino
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
náměstka	náměstek	k1gMnSc2	náměstek
o	o	k7c6	o
personálním	personální	k2eAgNnSc6d1	personální
obsazení	obsazení	k1gNnSc6	obsazení
významnějších	významný	k2eAgNnPc2d2	významnější
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
rozhodovali	rozhodovat	k5eAaImAgMnP	rozhodovat
pouze	pouze	k6eAd1	pouze
Bedřich	Bedřich	k1gMnSc1	Bedřich
Reicin	Reicin	k1gMnSc1	Reicin
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Procházka	Procházka	k1gMnSc1	Procházka
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Šváb	Šváb	k1gMnSc1	Šváb
<g/>
.	.	kIx.	.
</s>
<s>
Gen.	gen.	kA	gen.
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
akceptovat	akceptovat	k5eAaBmF	akceptovat
jejich	jejich	k3xOp3gInPc4	jejich
návrhy	návrh	k1gInPc4	návrh
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
vznášel	vznášet	k5eAaImAgMnS	vznášet
námitky	námitka	k1gFnSc2	námitka
vůči	vůči	k7c3	vůči
některým	některý	k3yIgInPc3	některý
návrhům	návrh	k1gInPc3	návrh
Reicina	Reicino	k1gNnSc2	Reicino
<g/>
,	,	kIx,	,
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
Reicin	Reicin	k1gInSc1	Reicin
<g/>
,	,	kIx,	,
že	že	k8xS	že
strana	strana	k1gFnSc1	strana
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
<g/>
,	,	kIx,	,
že	že	k8xS	že
návrhy	návrh	k1gInPc4	návrh
již	již	k6eAd1	již
projednal	projednat	k5eAaPmAgInS	projednat
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
telefonoval	telefonovat	k5eAaImAgMnS	telefonovat
Svoboda	Svoboda	k1gMnSc1	Svoboda
Gottwaldovi	Gottwald	k1gMnSc3	Gottwald
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
vždy	vždy	k6eAd1	vždy
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
návrhu	návrh	k1gInSc6	návrh
informován	informovat	k5eAaBmNgInS	informovat
a	a	k8xC	a
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
Svobodovi	Svobodův	k2eAgMnPc1d1	Svobodův
vyčítají	vyčítat	k5eAaImIp3nP	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nezastal	zastat	k5eNaPmAgMnS	zastat
ani	ani	k8xC	ani
svých	svůj	k3xOyFgMnPc2	svůj
spolubojovníků	spolubojovník	k1gMnPc2	spolubojovník
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nepostavil	postavit	k5eNaPmAgMnS	postavit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
čistkám	čistka	k1gFnPc3	čistka
ve	v	k7c6	v
velitelském	velitelský	k2eAgInSc6d1	velitelský
sboru	sbor	k1gInSc6	sbor
armády	armáda	k1gFnSc2	armáda
ani	ani	k8xC	ani
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jeho	jeho	k3xOp3gInPc4	jeho
dlouholeté	dlouholetý	k2eAgInPc4d1	dlouholetý
bojové	bojový	k2eAgInPc4d1	bojový
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
Svoboda	Svoboda	k1gMnSc1	Svoboda
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
zastavil	zastavit	k5eAaPmAgInS	zastavit
neoprávněné	oprávněný	k2eNgNnSc4d1	neoprávněné
penzionování	penzionování	k1gNnSc4	penzionování
některých	některý	k3yIgMnPc2	některý
generálů	generál	k1gMnPc2	generál
a	a	k8xC	a
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Intervenoval	intervenovat	k5eAaImAgMnS	intervenovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
pplk.	pplk.	kA	pplk.
Františka	František	k1gMnSc2	František
Skokana	Skokan	k1gMnSc2	Skokan
<g/>
.	.	kIx.	.
</s>
<s>
Reicin	Reicin	k1gInSc1	Reicin
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zatčení	zatčení	k1gNnSc6	zatčení
při	při	k7c6	při
výpovědi	výpověď	k1gFnSc6	výpověď
označil	označit	k5eAaPmAgMnS	označit
L.	L.	kA	L.
Svobodu	svoboda	k1gFnSc4	svoboda
za	za	k7c4	za
"	"	kIx"	"
<g/>
hlavní	hlavní	k2eAgFnSc4d1	hlavní
brzdu	brzda	k1gFnSc4	brzda
<g/>
"	"	kIx"	"
tzv.	tzv.	kA	tzv.
očisty	očista	k1gFnSc2	očista
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
hodnotil	hodnotit	k5eAaImAgMnS	hodnotit
Reicin	Reicin	k2eAgInSc4d1	Reicin
vztah	vztah	k1gInSc4	vztah
Svobody	svoboda	k1gFnSc2	svoboda
k	k	k7c3	k
dalším	další	k2eAgNnPc3d1	další
opatřením	opatření	k1gNnPc3	opatření
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
prosazovalo	prosazovat	k5eAaImAgNnS	prosazovat
vedení	vedení	k1gNnSc1	vedení
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
sovětských	sovětský	k2eAgMnPc2d1	sovětský
činitelů	činitel	k1gMnPc2	činitel
<g/>
.	.	kIx.	.
</s>
<s>
Otevřeně	otevřeně	k6eAd1	otevřeně
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Svoboda	Svoboda	k1gMnSc1	Svoboda
pochybnosti	pochybnost	k1gFnSc2	pochybnost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zavedení	zavedení	k1gNnSc2	zavedení
útvarových	útvarový	k2eAgFnPc2d1	útvarová
organizací	organizace	k1gFnPc2	organizace
KSČ	KSČ	kA	KSČ
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Svoboda	Svoboda	k1gMnSc1	Svoboda
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
znepokojen	znepokojit	k5eAaPmNgMnS	znepokojit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
začaly	začít	k5eAaPmAgFnP	začít
ustanovovat	ustanovovat	k5eAaImF	ustanovovat
první	první	k4xOgFnPc1	první
útvarové	útvarový	k2eAgFnPc1d1	útvarová
organizace	organizace	k1gFnPc1	organizace
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
vyslovoval	vyslovovat	k5eAaImAgMnS	vyslovovat
neustálé	neustálý	k2eAgFnPc4d1	neustálá
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
znamenat	znamenat	k5eAaImF	znamenat
úpadek	úpadek	k1gInSc4	úpadek
kázně	kázeň	k1gFnSc2	kázeň
a	a	k8xC	a
konec	konec	k1gInSc4	konec
principu	princip	k1gInSc2	princip
vojenské	vojenský	k2eAgFnSc2d1	vojenská
podřízenosti	podřízenost	k1gFnSc2	podřízenost
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
příklad	příklad	k1gInSc1	příklad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
takové	takový	k3xDgInPc1	takový
zjevy	zjev	k1gInPc1	zjev
skutečně	skutečně	k6eAd1	skutečně
projevily	projevit	k5eAaPmAgInP	projevit
<g/>
,	,	kIx,	,
neopomenul	opomenout	k5eNaPmAgInS	opomenout
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
kritickými	kritický	k2eAgFnPc7d1	kritická
připomínkami	připomínka	k1gFnPc7	připomínka
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvádí	uvádět	k5eAaImIp3nS	uvádět
Reicin	Reicin	k1gMnSc1	Reicin
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
výpovědích	výpověď	k1gFnPc6	výpověď
a	a	k8xC	a
rozhovorech	rozhovor	k1gInPc6	rozhovor
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
Svoboda	Svoboda	k1gMnSc1	Svoboda
dále	daleko	k6eAd2	daleko
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nebylo	být	k5eNaImAgNnS	být
nic	nic	k6eAd1	nic
známo	znám	k2eAgNnSc1d1	známo
ani	ani	k8xC	ani
o	o	k7c4	o
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
důstojníků	důstojník	k1gMnPc2	důstojník
ve	v	k7c6	v
vězeních	vězení	k1gNnPc6	vězení
OBZ	obza	k1gFnPc2	obza
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
Armádního	armádní	k2eAgInSc2d1	armádní
poradního	poradní	k2eAgInSc2d1	poradní
sboru	sbor	k1gInSc2	sbor
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tamtoho	tamten	k3xDgMnSc4	tamten
nebo	nebo	k8xC	nebo
onoho	onen	k3xDgMnSc4	onen
zavřeli	zavřít	k5eAaPmAgMnP	zavřít
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
r.	r.	kA	r.
1948	[number]	k4	1948
až	až	k9	až
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
(	(	kIx(	(
<g/>
Reicin	Reicin	k2eAgMnSc1d1	Reicin
<g/>
)	)	kIx)	)
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
zatčení	zatčený	k1gMnPc1	zatčený
jsou	být	k5eAaImIp3nP	být
předáváni	předáván	k2eAgMnPc1d1	předáván
státní	státní	k2eAgMnPc4d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
já	já	k3xPp1nSc1	já
neměl	mít	k5eNaImAgMnS	mít
ani	ani	k8xC	ani
ponětí	ponětí	k1gNnSc4	ponětí
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
někdo	někdo	k3yInSc1	někdo
vyšetřován	vyšetřovat	k5eAaImNgMnS	vyšetřovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
KSČ	KSČ	kA	KSČ
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
i	i	k9	i
proti	proti	k7c3	proti
Svobodovi	Svoboda	k1gMnSc3	Svoboda
<g/>
,	,	kIx,	,
nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1948	[number]	k4	1948
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Reicinovi	Reicinův	k2eAgMnPc1d1	Reicinův
-	-	kIx~	-
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
K.	K.	kA	K.
Gottwalda	Gottwald	k1gMnSc2	Gottwald
(	(	kIx(	(
<g/>
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
a	a	k8xC	a
R.	R.	kA	R.
Slánského	Slánského	k2eAgMnSc1d1	Slánského
(	(	kIx(	(
<g/>
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
podařilo	podařit	k5eAaPmAgNnS	podařit
vyměnit	vyměnit	k5eAaPmF	vyměnit
celé	celý	k2eAgNnSc4d1	celé
vedení	vedení	k1gNnSc4	vedení
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
i	i	k8xC	i
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
útvarů	útvar	k1gInPc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
šla	jít	k5eAaImAgFnS	jít
udání	udání	k1gNnSc4	udání
o	o	k7c6	o
Svobodově	Svobodův	k2eAgFnSc6d1	Svobodova
nedůvěryhodnosti	nedůvěryhodnost	k1gFnSc6	nedůvěryhodnost
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Slánský	Slánský	k1gMnSc1	Slánský
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
Svobodu	Svoboda	k1gMnSc4	Svoboda
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1951	[number]	k4	1951
pro	pro	k7c4	pro
poradu	porada	k1gFnSc4	porada
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
"	"	kIx"	"
<g/>
...	...	k?	...
obklopoval	obklopovat	k5eAaImAgInS	obklopovat
se	se	k3xPyFc4	se
důstojníky	důstojník	k1gMnPc7	důstojník
jako	jako	k8xS	jako
Drgač	Drgač	k1gMnSc1	Drgač
<g/>
,	,	kIx,	,
Drnec	Drnec	k1gMnSc1	Drnec
<g/>
,	,	kIx,	,
Klapálek	Klapálek	k?	Klapálek
<g/>
,	,	kIx,	,
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Bulandr	Bulandr	k1gInSc1	Bulandr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
staří	starý	k2eAgMnPc1d1	starý
legionáři	legionář	k1gMnPc1	legionář
a	a	k8xC	a
odchovanci	odchovanec	k1gMnPc1	odchovanec
západních	západní	k2eAgFnPc2d1	západní
vojenských	vojenský	k2eAgFnPc2d1	vojenská
doktrín	doktrína	k1gFnPc2	doktrína
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
kladný	kladný	k2eAgInSc4d1	kladný
poměr	poměr	k1gInSc4	poměr
k	k	k7c3	k
sovětské	sovětský	k2eAgFnSc3d1	sovětská
vojenské	vojenský	k2eAgFnSc3d1	vojenská
<g />
.	.	kIx.	.
</s>
<s>
doktríně	doktrína	k1gFnSc3	doktrína
...	...	k?	...
Naší	náš	k3xOp1gFnSc7	náš
chybou	chyba	k1gFnSc7	chyba
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
Svobodu	Svoboda	k1gMnSc4	Svoboda
a	a	k8xC	a
jeho	on	k3xPp3gMnSc4	on
chráněnce	chráněnec	k1gMnSc4	chráněnec
ponechali	ponechat	k5eAaPmAgMnP	ponechat
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
na	na	k7c6	na
klíčových	klíčový	k2eAgFnPc6d1	klíčová
pozicích	pozice	k1gFnPc6	pozice
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
že	že	k8xS	že
teprve	teprve	k6eAd1	teprve
na	na	k7c4	na
naléhavou	naléhavý	k2eAgFnSc4d1	naléhavá
radu	rada	k1gFnSc4	rada
soudruha	soudruh	k1gMnSc2	soudruh
Stalina	Stalin	k1gMnSc2	Stalin
jsme	být	k5eAaImIp1nP	být
je	on	k3xPp3gMnPc4	on
odstranili	odstranit	k5eAaPmAgMnP	odstranit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1950	[number]	k4	1950
byl	být	k5eAaImAgInS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
ministra	ministr	k1gMnSc2	ministr
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
náměstkem	náměstek	k1gMnSc7	náměstek
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
pověřeného	pověřený	k2eAgInSc2d1	pověřený
vedením	vedení	k1gNnSc7	vedení
Československého	československý	k2eAgInSc2d1	československý
státního	státní	k2eAgInSc2d1	státní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
sport	sport	k1gInSc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1951	[number]	k4	1951
byl	být	k5eAaImAgMnS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
i	i	k8xC	i
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
nabídnut	nabídnut	k2eAgInSc4d1	nabídnut
"	"	kIx"	"
<g/>
důchod	důchod	k1gInSc4	důchod
z	z	k7c2	z
milosti	milost	k1gFnSc2	milost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Svoboda	Svoboda	k1gMnSc1	Svoboda
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1952	[number]	k4	1952
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
procesem	proces	k1gInSc7	proces
s	s	k7c7	s
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Slánským	Slánský	k1gMnSc7	Slánský
zatčen	zatknout	k5eAaPmNgMnS	zatknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
připravoval	připravovat	k5eAaImAgMnS	připravovat
B.	B.	kA	B.
Reicin	Reicin	k1gMnSc1	Reicin
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
před	před	k7c7	před
Svobodou	svoboda	k1gFnSc7	svoboda
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
i	i	k9	i
generál	generál	k1gMnSc1	generál
Klapálek	Klapálek	k?	Klapálek
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
intervenci	intervence	k1gFnSc3	intervence
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
byl	být	k5eAaImAgMnS	být
vyslýchán	vyslýchat	k5eAaImNgMnS	vyslýchat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
sabotáž	sabotáž	k1gFnSc4	sabotáž
Košického	košický	k2eAgInSc2d1	košický
vládního	vládní	k2eAgInSc2d1	vládní
programu	program	k1gInSc2	program
a	a	k8xC	a
sabotáže	sabotáž	k1gFnSc2	sabotáž
výstavby	výstavba	k1gFnSc2	výstavba
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
podle	podle	k7c2	podle
sovětského	sovětský	k2eAgInSc2d1	sovětský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
řádného	řádný	k2eAgInSc2d1	řádný
důchodu	důchod	k1gInSc2	důchod
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zatčení	zatčení	k1gNnSc1	zatčení
využilo	využít	k5eAaPmAgNnS	využít
okresní	okresní	k2eAgNnSc1d1	okresní
vedení	vedení	k1gNnSc1	vedení
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vysídlilo	vysídlit	k5eAaPmAgNnS	vysídlit
jeho	jeho	k3xOp3gMnSc4	jeho
nevlastního	vlastní	k2eNgMnSc4d1	nevlastní
bratra	bratr	k1gMnSc4	bratr
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
i	i	k8xC	i
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
maminkou	maminka	k1gFnSc7	maminka
z	z	k7c2	z
Hroznatína	Hroznatín	k1gInSc2	Hroznatín
na	na	k7c4	na
státní	státní	k2eAgInSc4d1	státní
statek	statek	k1gInSc4	statek
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
Lhotce	Lhotka	k1gFnSc6	Lhotka
<g/>
.	.	kIx.	.
</s>
<s>
JZD	JZD	kA	JZD
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
založit	založit	k5eAaPmF	založit
a	a	k8xC	a
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
v	v	k7c6	v
rodné	rodný	k2eAgFnSc3d1	rodná
obci	obec	k1gFnSc3	obec
Hroznatín	Hroznatína	k1gFnPc2	Hroznatína
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
okresní	okresní	k2eAgNnSc1d1	okresní
vedení	vedení	k1gNnSc1	vedení
KSČ	KSČ	kA	KSČ
jako	jako	k8xS	jako
kulacké	kulacký	k2eAgFnPc1d1	kulacká
<g/>
.	.	kIx.	.
</s>
<s>
Vysídleni	vysídlit	k5eAaPmNgMnP	vysídlit
byli	být	k5eAaImAgMnP	být
i	i	k9	i
další	další	k2eAgMnPc1d1	další
čtyři	čtyři	k4xCgMnPc1	čtyři
sedláci	sedlák	k1gMnPc1	sedlák
-	-	kIx~	-
zakládající	zakládající	k2eAgMnPc1d1	zakládající
členové	člen	k1gMnPc1	člen
družstva	družstvo	k1gNnSc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
domů	domů	k6eAd1	domů
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
vazby	vazba	k1gFnSc2	vazba
intenzivně	intenzivně	k6eAd1	intenzivně
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
družstvu	družstvo	k1gNnSc6	družstvo
<g/>
,	,	kIx,	,
rodáci	rodák	k1gMnPc1	rodák
ho	on	k3xPp3gMnSc4	on
zvolili	zvolit	k5eAaPmAgMnP	zvolit
čestným	čestný	k2eAgMnSc7d1	čestný
předsedou	předseda	k1gMnSc7	předseda
JZD	JZD	kA	JZD
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
zaměstnán	zaměstnat	k5eAaPmNgInS	zaměstnat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
účetního	účetní	k1gMnSc2	účetní
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
v	v	k7c6	v
JZD	JZD	kA	JZD
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
nebyl	být	k5eNaImAgMnS	být
a	a	k8xC	a
nepobíral	pobírat	k5eNaImAgMnS	pobírat
plat	plat	k1gInSc4	plat
ani	ani	k8xC	ani
důchod	důchod	k1gInSc4	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1	úmrtí
Stalina	Stalin	k1gMnSc2	Stalin
a	a	k8xC	a
Gottwalda	Gottwald	k1gMnSc2	Gottwald
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
odhalení	odhalení	k1gNnSc4	odhalení
Berii	Berie	k1gFnSc4	Berie
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
sjezd	sjezd	k1gInSc1	sjezd
KSSS	KSSS	kA	KSSS
předznamenaly	předznamenat	k5eAaPmAgFnP	předznamenat
konec	konec	k1gInSc4	konec
politiky	politika	k1gFnSc2	politika
padesátých	padesátý	k4xOgInPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
byl	být	k5eAaImAgMnS	být
rehabilitován	rehabilitovat	k5eAaBmNgMnS	rehabilitovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
díky	díky	k7c3	díky
Chruščovovi	Chruščova	k1gMnSc3	Chruščova
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
se	se	k3xPyFc4	se
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
nabízeno	nabízet	k5eAaImNgNnS	nabízet
<g/>
.	.	kIx.	.
</s>
<s>
Přijal	přijmout	k5eAaPmAgMnS	přijmout
funkci	funkce	k1gFnSc4	funkce
odbornou	odborný	k2eAgFnSc4d1	odborná
<g/>
,	,	kIx,	,
náčelníka	náčelník	k1gMnSc2	náčelník
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
akademie	akademie	k1gFnSc2	akademie
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
,	,	kIx,	,
velitelského	velitelský	k2eAgNnSc2d1	velitelské
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vykonával	vykonávat	k5eAaImAgInS	vykonávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
až	až	k9	až
do	do	k7c2	do
zvolení	zvolení	k1gNnSc2	zvolení
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byl	být	k5eAaImAgInS	být
poslancem	poslanec	k1gMnSc7	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
Žďársku	Žďársko	k1gNnSc6	Žďársko
<g/>
,	,	kIx,	,
Velkomeziříčsku	Velkomeziříčsko	k1gNnSc6	Velkomeziříčsko
a	a	k8xC	a
Třebíčsku	Třebíčsko	k1gNnSc6	Třebíčsko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Svazu	svaz	k1gInSc2	svaz
protifašistických	protifašistický	k2eAgMnPc2d1	protifašistický
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
Svazu	svaz	k1gInSc2	svaz
československo-sovětského	československoovětský	k2eAgNnSc2d1	československo-sovětský
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
pamětech	paměť	k1gFnPc6	paměť
ve	v	k7c6	v
Vojenském	vojenský	k2eAgInSc6d1	vojenský
historickém	historický	k2eAgInSc6d1	historický
ústavu	ústav	k1gInSc6	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
napsal	napsat	k5eAaBmAgInS	napsat
své	svůj	k3xOyFgFnPc4	svůj
vzpomínkové	vzpomínkový	k2eAgFnPc4d1	vzpomínková
knihy	kniha	k1gFnPc4	kniha
Z	z	k7c2	z
Buzuluku	Buzuluk	k1gInSc2	Buzuluk
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Cestami	cesta	k1gFnPc7	cesta
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgInS	věnovat
se	se	k3xPyFc4	se
intervencím	intervence	k1gFnPc3	intervence
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
rehabilitací	rehabilitace	k1gFnPc2	rehabilitace
neprávem	neprávo	k1gNnSc7	neprávo
perzekvovaných	perzekvovaný	k2eAgMnPc2d1	perzekvovaný
vojáků	voják	k1gMnPc2	voják
(	(	kIx(	(
<g/>
generálů	generál	k1gMnPc2	generál
K.	K.	kA	K.
Klapálka	Klapálka	k1gFnSc1	Klapálka
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Bočka	Boček	k1gMnSc2	Boček
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Nosála	nosál	k1gMnSc2	nosál
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
Nováka	Novák	k1gMnSc2	Novák
<g/>
,	,	kIx,	,
Š.	Š.	kA	Š.
Drgáče	Drgáč	k1gInSc2	Drgáč
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Drnka	Drnek	k1gInSc2	Drnek
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Bulandra	Bulandra	k1gFnSc1	Bulandra
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Ressla	Ressla	k1gFnSc4	Ressla
<g/>
,	,	kIx,	,
plk.	plk.	kA	plk.
J.	J.	kA	J.
<g />
.	.	kIx.	.
</s>
<s>
Knopa	Knopa	k1gFnSc1	Knopa
<g/>
,	,	kIx,	,
pplk.	pplk.	kA	pplk.
Bohumila	Bohumil	k1gMnSc2	Bohumil
Pelikána	Pelikán	k1gMnSc2	Pelikán
<g/>
,	,	kIx,	,
plk.	plk.	kA	plk.
Ing.	ing.	kA	ing.
A.	A.	kA	A.
Holba	holba	k1gFnSc1	holba
<g/>
,	,	kIx,	,
plk.	plk.	kA	plk.
F.	F.	kA	F.
Hieke-Stoje	Hieke-Stoj	k1gInPc1	Hieke-Stoj
<g/>
,	,	kIx,	,
revize	revize	k1gFnPc1	revize
rozsudku	rozsudek	k1gInSc2	rozsudek
pplk.	pplk.	kA	pplk.
F.	F.	kA	F.
Skokana	Skokan	k1gMnSc2	Skokan
<g/>
,	,	kIx,	,
studenta	student	k1gMnSc2	student
J.	J.	kA	J.
Procházky	procházka	k1gFnSc2	procházka
z	z	k7c2	z
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
<g/>
)	)	kIx)	)
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
mnoho	mnoho	k4c1	mnoho
projevů	projev	k1gInPc2	projev
a	a	k8xC	a
přednášek	přednáška	k1gFnPc2	přednáška
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
i	i	k8xC	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
besedoval	besedovat	k5eAaImAgMnS	besedovat
se	se	k3xPyFc4	se
školní	školní	k2eAgFnSc3d1	školní
mládeži	mládež	k1gFnSc3	mládež
a	a	k8xC	a
vojáky	voják	k1gMnPc4	voják
o	o	k7c6	o
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
ochráncem	ochránce	k1gMnSc7	ochránce
zájmů	zájem	k1gInPc2	zájem
poddukelského	poddukelský	k2eAgInSc2d1	poddukelský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dlouho	dlouho	k6eAd1	dlouho
nesl	nést	k5eAaImAgInS	nést
mimořádné	mimořádný	k2eAgInPc4d1	mimořádný
důsledky	důsledek	k1gInPc4	důsledek
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
mírové	mírový	k2eAgNnSc4d1	Mírové
řešení	řešení	k1gNnSc4	řešení
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
konfliktů	konflikt	k1gInPc2	konflikt
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
mírového	mírový	k2eAgNnSc2d1	Mírové
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
až	až	k9	až
1964	[number]	k4	1964
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
během	během	k7c2	během
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
byl	být	k5eAaImAgMnS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
Antonína	Antonín	k1gMnSc4	Antonín
Novotného	Novotný	k1gMnSc4	Novotný
<g/>
,	,	kIx,	,
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Dubčeka	Dubčeek	k1gMnSc2	Dubčeek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
návrh	návrh	k1gInSc4	návrh
Svazu	svaz	k1gInSc2	svaz
protifašistických	protifašistický	k2eAgMnPc2d1	protifašistický
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
zvolen	zvolen	k2eAgInSc4d1	zvolen
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
komunistických	komunistický	k2eAgMnPc2d1	komunistický
prezidentů	prezident	k1gMnPc2	prezident
volený	volený	k2eAgInSc1d1	volený
tajnou	tajný	k2eAgFnSc7d1	tajná
volbou	volba	k1gFnSc7	volba
a	a	k8xC	a
ne	ne	k9	ne
aklamací	aklamace	k1gFnSc7	aklamace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
L.	L.	kA	L.
Svobodu	svoboda	k1gFnSc4	svoboda
bylo	být	k5eAaImAgNnS	být
odevzdáno	odevzdat	k5eAaPmNgNnS	odevzdat
282	[number]	k4	282
hlasů	hlas	k1gInPc2	hlas
ze	z	k7c2	z
288	[number]	k4	288
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
symbolicky	symbolicky	k6eAd1	symbolicky
položil	položit	k5eAaPmAgMnS	položit
květy	květ	k1gInPc4	květ
na	na	k7c4	na
hroby	hrob	k1gInPc4	hrob
prezidentů	prezident	k1gMnPc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
i	i	k8xC	i
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
Gottwaldův	Gottwaldův	k2eAgInSc4d1	Gottwaldův
a	a	k8xC	a
Zápotockého	Zápotockého	k2eAgInSc4d1	Zápotockého
<g/>
.	.	kIx.	.
</s>
<s>
Navštívil	navštívit	k5eAaPmAgInS	navštívit
Památník	památník	k1gInSc1	památník
osvobození	osvobození	k1gNnSc2	osvobození
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vzdal	vzdát	k5eAaPmAgMnS	vzdát
úctu	úcta	k1gFnSc4	úcta
legionářům	legionář	k1gMnPc3	legionář
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
i	i	k8xC	i
bojovníkům	bojovník	k1gMnPc3	bojovník
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
novou	nový	k2eAgFnSc4d1	nová
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
převládali	převládat	k5eAaImAgMnP	převládat
stoupenci	stoupenec	k1gMnPc1	stoupenec
reformy	reforma	k1gFnSc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
parlamentu	parlament	k1gInSc2	parlament
až	až	k8xS	až
do	do	k7c2	do
zvolení	zvolení	k1gNnPc2	zvolení
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
až	až	k9	až
1976	[number]	k4	1976
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
prezidenta	prezident	k1gMnSc2	prezident
byla	být	k5eAaImAgFnS	být
bez	bez	k7c2	bez
přímého	přímý	k2eAgInSc2d1	přímý
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
svou	svůj	k3xOyFgFnSc7	svůj
autoritou	autorita	k1gFnSc7	autorita
prezidenta	prezident	k1gMnSc2	prezident
podporoval	podporovat	k5eAaImAgInS	podporovat
reformní	reformní	k2eAgInSc1d1	reformní
program	program	k1gInSc1	program
Dubčekova	Dubčekův	k2eAgNnSc2d1	Dubčekovo
vedení	vedení	k1gNnSc2	vedení
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
směřující	směřující	k2eAgFnSc2d1	směřující
k	k	k7c3	k
demokratizaci	demokratizace	k1gFnSc3	demokratizace
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
i	i	k8xC	i
ekonomice	ekonomika	k1gFnSc6	ekonomika
a	a	k8xC	a
za	za	k7c4	za
odstranění	odstranění	k1gNnSc4	odstranění
deformací	deformace	k1gFnPc2	deformace
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Činil	činit	k5eAaImAgInS	činit
tak	tak	k9	tak
na	na	k7c6	na
četných	četný	k2eAgNnPc6d1	četné
veřejných	veřejný	k2eAgNnPc6d1	veřejné
vystoupeních	vystoupení	k1gNnPc6	vystoupení
a	a	k8xC	a
setkáních	setkání	k1gNnPc6	setkání
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
a	a	k8xC	a
v	v	k7c6	v
družstvech	družstvo	k1gNnPc6	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
se	se	k3xPyFc4	se
o	o	k7c4	o
rehabilitace	rehabilitace	k1gFnPc4	rehabilitace
neprávem	neprávo	k1gNnSc7	neprávo
odsouzených	odsouzená	k1gFnPc2	odsouzená
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
odbojářů	odbojář	k1gMnPc2	odbojář
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgMnS	navázat
tak	tak	k9	tak
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
úsilí	úsilí	k1gNnSc4	úsilí
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Stalina	Stalin	k1gMnSc4	Stalin
a	a	k8xC	a
Gottwalda	Gottwald	k1gMnSc4	Gottwald
sice	sice	k8xC	sice
s	s	k7c7	s
rehabilitacemi	rehabilitace	k1gFnPc7	rehabilitace
začalo	začít	k5eAaPmAgNnS	začít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
liknavě	liknavě	k6eAd1	liknavě
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
bohaté	bohatý	k2eAgInPc4d1	bohatý
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
návštěva	návštěva	k1gFnSc1	návštěva
prezidenta	prezident	k1gMnSc2	prezident
Svobody	Svoboda	k1gMnSc2	Svoboda
vedla	vést	k5eAaImAgFnS	vést
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
navštívil	navštívit	k5eAaPmAgMnS	navštívit
všechny	všechen	k3xTgInPc4	všechen
kraje	kraj	k1gInPc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
President	president	k1gMnSc1	president
neměl	mít	k5eNaImAgMnS	mít
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
ani	ani	k8xC	ani
na	na	k7c4	na
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k8xS	jako
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Polednový	polednový	k2eAgInSc1d1	polednový
politický	politický	k2eAgInSc1d1	politický
vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vyvolával	vyvolávat	k5eAaImAgInS	vyvolávat
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
schůzi	schůze	k1gFnSc6	schůze
delegací	delegace	k1gFnPc2	delegace
šesti	šest	k4xCc2	šest
stran	strana	k1gFnPc2	strana
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
Svoboda	Svoboda	k1gMnSc1	Svoboda
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zazněla	zaznět	k5eAaImAgFnS	zaznět
kritika	kritika	k1gFnSc1	kritika
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
vedení	vedení	k1gNnSc2	vedení
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
že	že	k8xS	že
nečelí	čelit	k5eNaImIp3nS	čelit
kontrarevolučním	kontrarevoluční	k2eAgFnPc3d1	kontrarevoluční
tendencím	tendence	k1gFnPc3	tendence
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
politika	politika	k1gFnSc1	politika
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
restauraci	restaurace	k1gFnSc3	restaurace
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
kritice	kritika	k1gFnSc6	kritika
nesmlouvaví	smlouvavý	k2eNgMnPc1d1	nesmlouvavý
delegáti	delegát	k1gMnPc1	delegát
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
necelé	celý	k2eNgInPc4d1	necelý
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
zasedání	zasedání	k1gNnSc3	zasedání
představitelů	představitel	k1gMnPc2	představitel
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
tentokrát	tentokrát	k6eAd1	tentokrát
naše	náš	k3xOp1gFnSc1	náš
delegace	delegace	k1gFnSc1	delegace
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
dvoustranným	dvoustranný	k2eAgNnSc7d1	dvoustranné
jednáním	jednání	k1gNnSc7	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
zasedání	zasedání	k1gNnSc2	zasedání
byl	být	k5eAaImAgMnS	být
československému	československý	k2eAgNnSc3d1	Československé
vedení	vedení	k1gNnSc3	vedení
zaslán	zaslán	k2eAgInSc4d1	zaslán
dopis	dopis	k1gInSc4	dopis
pěti	pět	k4xCc2	pět
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
stran	strana	k1gFnPc2	strana
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
kritikou	kritika	k1gFnSc7	kritika
politiky	politika	k1gFnSc2	politika
československého	československý	k2eAgNnSc2d1	Československé
vedení	vedení	k1gNnSc2	vedení
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
došlo	dojít	k5eAaPmAgNnS	dojít
28	[number]	k4	28
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
mezi	mezi	k7c7	mezi
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
a	a	k8xC	a
československou	československý	k2eAgFnSc7d1	Československá
delegací	delegace	k1gFnSc7	delegace
v	v	k7c6	v
Čierné	Čierný	k2eAgFnSc6d1	Čierná
při	pře	k1gFnSc6	pře
Čopu	Čopus	k1gInSc2	Čopus
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
dvoustranné	dvoustranný	k2eAgFnSc2d1	dvoustranná
schůzky	schůzka	k1gFnSc2	schůzka
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
však	však	k9	však
vedoucím	vedoucí	k1gMnSc7	vedoucí
delegace	delegace	k1gFnSc2	delegace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
premiéra	premiér	k1gMnSc2	premiér
SSSR	SSSR	kA	SSSR
Kosygina	Kosygina	k1gMnSc1	Kosygina
varoval	varovat	k5eAaImAgMnS	varovat
Sověty	Sověty	k1gInPc4	Sověty
před	před	k7c7	před
vojenským	vojenský	k2eAgInSc7d1	vojenský
zásahem	zásah	k1gInSc7	zásah
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
narušil	narušit	k5eAaPmAgMnS	narušit
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
národy	národ	k1gInPc7	národ
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
následovala	následovat	k5eAaImAgFnS	následovat
další	další	k2eAgFnSc1d1	další
závažná	závažný	k2eAgFnSc1d1	závažná
schůze	schůze	k1gFnSc1	schůze
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
představitelů	představitel	k1gMnPc2	představitel
komunistických	komunistický	k2eAgMnPc2d1	komunistický
stran	stran	k7c2	stran
šesti	šest	k4xCc2	šest
států	stát	k1gInPc2	stát
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
jednání	jednání	k1gNnPc2	jednání
podával	podávat	k5eAaImAgMnS	podávat
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
tajemník	tajemník	k1gMnSc1	tajemník
KSČ	KSČ	kA	KSČ
Alexander	Alexandra	k1gFnPc2	Alexandra
Dubček	Dubček	k1gMnSc1	Dubček
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
O.	O.	kA	O.
Černík	Černík	k1gMnSc1	Černík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
"	"	kIx"	"
<g/>
založili	založit	k5eAaPmAgMnP	založit
svá	svůj	k3xOyFgNnPc4	svůj
vystoupení	vystoupení	k1gNnPc4	vystoupení
na	na	k7c6	na
optimistickém	optimistický	k2eAgNnSc6d1	optimistické
hodnocení	hodnocení	k1gNnSc6	hodnocení
vývoje	vývoj	k1gInSc2	vývoj
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
pětkou	pětka	k1gFnSc7	pětka
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Dávali	dávat	k5eAaImAgMnP	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
čeho	co	k3yQnSc2	co
se	se	k3xPyFc4	se
bát	bát	k5eAaImF	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
máme	mít	k5eAaImIp1nP	mít
před	před	k7c7	před
sebou	se	k3xPyFc7	se
periodu	perioda	k1gFnSc4	perioda
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
že	že	k8xS	že
hlavní	hlavní	k2eAgMnSc1d1	hlavní
je	být	k5eAaImIp3nS	být
dokončit	dokončit	k5eAaPmF	dokončit
přípravy	příprava	k1gFnPc4	příprava
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
dověděl	dovědět	k5eAaPmAgMnS	dovědět
od	od	k7c2	od
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
dnech	den	k1gInPc6	den
předal	předat	k5eAaPmAgMnS	předat
A.	A.	kA	A.
Dubčekovi	Dubčeek	k1gMnSc3	Dubčeek
závažný	závažný	k2eAgInSc4d1	závažný
dopis	dopis	k1gInSc4	dopis
k	k	k7c3	k
projednání	projednání	k1gNnSc3	projednání
kolektivem	kolektiv	k1gInSc7	kolektiv
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nedostal	dostat	k5eNaPmAgMnS	dostat
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
ho	on	k3xPp3gNnSc4	on
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
dopisu	dopis	k1gInSc2	dopis
seznámil	seznámit	k5eAaPmAgMnS	seznámit
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
dopisu	dopis	k1gInSc2	dopis
zněl	znět	k5eAaImAgInS	znět
jako	jako	k9	jako
"	"	kIx"	"
<g/>
závažné	závažný	k2eAgNnSc1d1	závažné
varování	varování	k1gNnSc1	varování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
prezident	prezident	k1gMnSc1	prezident
nepochopil	pochopit	k5eNaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
poslední	poslední	k2eAgNnSc1d1	poslední
varování	varování	k1gNnSc1	varování
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
úvod	úvod	k1gInSc1	úvod
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
politické	politický	k2eAgFnSc3d1	politická
konfrontaci	konfrontace	k1gFnSc3	konfrontace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dopis	dopis	k1gInSc1	dopis
do	do	k7c2	do
zasedání	zasedání	k1gNnSc2	zasedání
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
nebyl	být	k5eNaImAgInS	být
projednán	projednat	k5eAaPmNgInS	projednat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Invaze	invaze	k1gFnSc2	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Reformní	reformní	k2eAgInSc1d1	reformní
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
neohlášeným	ohlášený	k2eNgInSc7d1	neohlášený
vstupem	vstup	k1gInSc7	vstup
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
na	na	k7c4	na
československé	československý	k2eAgNnSc4d1	Československé
území	území	k1gNnSc4	území
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zadržení	zadržení	k1gNnSc3	zadržení
československých	československý	k2eAgMnPc2d1	československý
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
představitelů	představitel	k1gMnPc2	představitel
ústavních	ústavní	k2eAgInPc2d1	ústavní
<g/>
,	,	kIx,	,
státních	státní	k2eAgInPc2d1	státní
a	a	k8xC	a
stranických	stranický	k2eAgInPc2d1	stranický
-	-	kIx~	-
A.	A.	kA	A.
Dubčeka	Dubčeek	k1gMnSc2	Dubčeek
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Černíka	Černík	k1gMnSc2	Černík
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Smrkovského	Smrkovský	k2eAgInSc2d1	Smrkovský
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Kriegela	Kriegela	k1gFnSc1	Kriegela
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Špačka	Špaček	k1gMnSc2	Špaček
a	a	k8xC	a
B.	B.	kA	B.
Šimona	Šimona	k1gFnSc1	Šimona
<g/>
,	,	kIx,	,
sovětskými	sovětský	k2eAgInPc7d1	sovětský
orgány	orgán	k1gInPc7	orgán
a	a	k8xC	a
následoval	následovat	k5eAaImAgInS	následovat
jejich	jejich	k3xOp3gInSc1	jejich
únos	únos	k1gInSc1	únos
a	a	k8xC	a
internace	internace	k1gFnSc1	internace
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentovi	prezident	k1gMnSc3	prezident
byl	být	k5eAaImAgMnS	být
vstup	vstup	k1gInSc4	vstup
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
oznámen	oznámit	k5eAaPmNgMnS	oznámit
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
SSSR	SSSR	kA	SSSR
Červoněnkem	Červoněnek	k1gMnSc7	Červoněnek
v	v	k7c6	v
pozdních	pozdní	k2eAgFnPc6d1	pozdní
hodinách	hodina	k1gFnPc6	hodina
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
když	když	k8xS	když
letadla	letadlo	k1gNnPc4	letadlo
již	již	k9	již
přistávala	přistávat	k5eAaImAgFnS	přistávat
na	na	k7c6	na
ruzyňském	ruzyňský	k2eAgNnSc6d1	ruzyňské
letišti	letiště	k1gNnSc6	letiště
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
President	president	k1gMnSc1	president
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
protest	protest	k1gInSc4	protest
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
došlo	dojít	k5eAaPmAgNnS	dojít
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
dopise	dopis	k1gInSc6	dopis
sovětské	sovětský	k2eAgFnSc2d1	sovětská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
byla	být	k5eAaImAgNnP	být
dána	dán	k2eAgNnPc1d1	dáno
vedení	vedení	k1gNnPc1	vedení
ČSSR	ČSSR	kA	ČSSR
možnost	možnost	k1gFnSc1	možnost
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
reagovat	reagovat	k5eAaBmF	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vojska	vojsko	k1gNnPc4	vojsko
nepozval	pozvat	k5eNaPmAgMnS	pozvat
<g/>
,	,	kIx,	,
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
krveprolití	krveprolití	k1gNnSc2	krveprolití
a	a	k8xC	a
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškerá	veškerý	k3xTgFnSc1	veškerý
zodpovědnost	zodpovědnost	k1gFnSc1	zodpovědnost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Prezident	prezident	k1gMnSc1	prezident
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
když	když	k8xS	když
došel	dojít	k5eAaPmAgInS	dojít
na	na	k7c4	na
zasedající	zasedající	k2eAgNnSc4d1	zasedající
předsednictvo	předsednictvo	k1gNnSc4	předsednictvo
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgMnS	podepsat
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nejvyššími	vysoký	k2eAgMnPc7d3	nejvyšší
volenými	volený	k2eAgMnPc7d1	volený
představiteli	představitel	k1gMnPc7	představitel
prohlášení	prohlášení	k1gNnSc2	prohlášení
odsuzující	odsuzující	k2eAgFnSc4d1	odsuzující
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
výzvu	výzva	k1gFnSc4	výzva
k	k	k7c3	k
občanům	občan	k1gMnPc3	občan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
politických	politický	k2eAgMnPc2d1	politický
a	a	k8xC	a
státních	státní	k2eAgMnPc2d1	státní
činitelů	činitel	k1gMnPc2	činitel
zůstal	zůstat	k5eAaPmAgMnS	zůstat
prezident	prezident	k1gMnSc1	prezident
sám	sám	k3xTgMnSc1	sám
a	a	k8xC	a
podle	podle	k7c2	podle
Ústavy	ústava	k1gFnSc2	ústava
převzal	převzít	k5eAaPmAgMnS	převzít
dočasně	dočasně	k6eAd1	dočasně
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
řešení	řešení	k1gNnSc4	řešení
dané	daný	k2eAgFnSc2d1	daná
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
odpor	odpor	k1gInSc1	odpor
byl	být	k5eAaImAgInS	být
nemyslitelný	myslitelný	k2eNgMnSc1d1	nemyslitelný
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
obrovské	obrovský	k2eAgFnSc3d1	obrovská
početní	početní	k2eAgFnSc3d1	početní
převaze	převaha	k1gFnSc3	převaha
vojsk	vojsko	k1gNnPc2	vojsko
zemí	zem	k1gFnPc2	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Svoboda	Svoboda	k1gMnSc1	Svoboda
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
uznat	uznat	k5eAaPmF	uznat
zbytek	zbytek	k1gInSc4	zbytek
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
internovaného	internovaný	k2eAgMnSc2d1	internovaný
předsedy	předseda	k1gMnSc2	předseda
O.	O.	kA	O.
Černíka	Černík	k1gMnSc2	Černík
<g/>
,	,	kIx,	,
za	za	k7c4	za
funkční	funkční	k2eAgNnSc4d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
kategoricky	kategoricky	k6eAd1	kategoricky
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
návrh	návrh	k1gInSc4	návrh
předložený	předložený	k2eAgInSc4d1	předložený
skupinou	skupina	k1gFnSc7	skupina
bývalých	bývalý	k2eAgMnPc2d1	bývalý
stranických	stranický	k2eAgMnPc2d1	stranický
funkcionářů	funkcionář	k1gMnPc2	funkcionář
jmenovat	jmenovat	k5eAaBmF	jmenovat
novou	nový	k2eAgFnSc4d1	nová
revoluční	revoluční	k2eAgFnSc4d1	revoluční
dělnicko-rolnickou	dělnickoolnický	k2eAgFnSc4d1	dělnicko-rolnická
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
současně	současně	k6eAd1	současně
společným	společný	k2eAgInSc7d1	společný
stranickým	stranický	k2eAgInSc7d1	stranický
i	i	k8xC	i
státním	státní	k2eAgInSc7d1	státní
orgánem	orgán	k1gInSc7	orgán
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
vedená	vedený	k2eAgFnSc1d1	vedená
A.	A.	kA	A.
Indrou	Indra	k1gMnSc7	Indra
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
i	i	k9	i
druhou	druhý	k4xOgFnSc4	druhý
variantu	varianta	k1gFnSc4	varianta
tohoto	tento	k3xDgInSc2	tento
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
měl	mít	k5eAaImAgInS	mít
vládu	vláda	k1gFnSc4	vláda
vést	vést	k5eAaImF	vést
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
návrhy	návrh	k1gInPc1	návrh
by	by	kYmCp3nP	by
legalizovaly	legalizovat	k5eAaBmAgInP	legalizovat
situaci	situace	k1gFnSc4	situace
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
po	po	k7c6	po
odvlečení	odvlečení	k1gNnSc6	odvlečení
čs	čs	kA	čs
<g/>
.	.	kIx.	.
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
přímé	přímý	k2eAgNnSc4d1	přímé
jednání	jednání	k1gNnSc4	jednání
se	s	k7c7	s
sovětskými	sovětský	k2eAgMnPc7d1	sovětský
představiteli	představitel	k1gMnPc7	představitel
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
velvyslance	velvyslanec	k1gMnSc4	velvyslanec
o	o	k7c4	o
dojednání	dojednání	k1gNnSc4	dojednání
schůzky	schůzka	k1gFnSc2	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
úsilím	úsilí	k1gNnSc7	úsilí
bylo	být	k5eAaImAgNnS	být
navrátit	navrátit	k5eAaPmF	navrátit
politiky	politika	k1gFnPc4	politika
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
podmínil	podmínit	k5eAaPmAgMnS	podmínit
zasednutí	zasednutí	k1gNnSc4	zasednutí
za	za	k7c4	za
jednací	jednací	k2eAgInSc4d1	jednací
stůl	stůl	k1gInSc4	stůl
propuštěním	propuštění	k1gNnSc7	propuštění
zatčených	zatčený	k2eAgMnPc2d1	zatčený
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
jedině	jedině	k6eAd1	jedině
legitimních	legitimní	k2eAgMnPc2d1	legitimní
představitelů	představitel	k1gMnPc2	představitel
ČSSR	ČSSR	kA	ČSSR
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
začleněním	začlenění	k1gNnSc7	začlenění
do	do	k7c2	do
delegace	delegace	k1gFnSc2	delegace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc3	jeho
žádosti	žádost	k1gFnSc3	žádost
bylo	být	k5eAaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c6	na
F.	F.	kA	F.
Kriegela	Kriegel	k1gMnSc4	Kriegel
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
internovaní	internovaný	k2eAgMnPc1d1	internovaný
politici	politik	k1gMnPc1	politik
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
delegaci	delegace	k1gFnSc3	delegace
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
legální	legální	k2eAgNnSc1d1	legální
vedení	vedení	k1gNnSc1	vedení
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
státu	stát	k1gInSc2	stát
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
před	před	k7c7	před
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
tak	tak	k6eAd1	tak
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
na	na	k7c6	na
čem	co	k3yInSc6	co
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
řadě	řada	k1gFnSc6	řada
záleželo	záležet	k5eAaImAgNnS	záležet
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěl	chtít	k5eNaImAgMnS	chtít
připustit	připustit	k5eAaPmF	připustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
právě	právě	k9	právě
bez	bez	k7c2	bez
tohoto	tento	k3xDgNnSc2	tento
vedení	vedení	k1gNnSc2	vedení
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
čemkoliv	cokoliv	k3yInSc6	cokoliv
při	při	k7c6	při
řešení	řešení	k1gNnSc3	řešení
krizové	krizový	k2eAgFnSc2d1	krizová
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
zejména	zejména	k9	zejména
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
kroků	krok	k1gInPc2	krok
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
úsilí	úsilí	k1gNnSc1	úsilí
delegace	delegace	k1gFnSc2	delegace
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
nad	nad	k7c4	nad
práci	práce	k1gFnSc4	práce
nad	nad	k7c7	nad
závěrečným	závěrečný	k2eAgInSc7d1	závěrečný
dokumentem	dokument	k1gInSc7	dokument
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
meritorně	meritorně	k6eAd1	meritorně
nevměšoval	vměšovat	k5eNaImAgMnS	vměšovat
<g/>
.	.	kIx.	.
</s>
<s>
Přenechal	přenechat	k5eAaPmAgMnS	přenechat
ji	on	k3xPp3gFnSc4	on
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
příslušných	příslušný	k2eAgMnPc2d1	příslušný
ústavních	ústavní	k2eAgMnPc2d1	ústavní
činitelů	činitel	k1gMnPc2	činitel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
představitelé	představitel	k1gMnPc1	představitel
museli	muset	k5eAaImAgMnP	muset
tak	tak	k6eAd1	tak
jednat	jednat	k5eAaImF	jednat
ne	ne	k9	ne
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
"	"	kIx"	"
<g/>
dělnicko-rolnickou	dělnickoolnický	k2eAgFnSc4d1	dělnicko-rolnická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
představovali	představovat	k5eAaImAgMnP	představovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
původními	původní	k2eAgMnPc7d1	původní
politiky	politik	k1gMnPc7	politik
<g/>
.	.	kIx.	.
</s>
<s>
Delegace	delegace	k1gFnSc1	delegace
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
některé	některý	k3yIgMnPc4	některý
další	další	k2eAgMnPc4d1	další
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přiletěli	přiletět	k5eAaPmAgMnP	přiletět
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
jednání	jednání	k1gNnSc4	jednání
byl	být	k5eAaImAgInS	být
podepsán	podepsán	k2eAgInSc1d1	podepsán
tzv.	tzv.	kA	tzv.
Moskevský	moskevský	k2eAgInSc1d1	moskevský
protokol	protokol	k1gInSc1	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
jednání	jednání	k1gNnSc2	jednání
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
bez	bez	k7c2	bez
Fr.	Fr.	k1gMnSc2	Fr.
Kriegla	Kriegl	k1gMnSc2	Kriegl
<g/>
,	,	kIx,	,
předsedy	předseda	k1gMnSc2	předseda
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Moskevský	moskevský	k2eAgInSc1d1	moskevský
protokol	protokol	k1gInSc1	protokol
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
přiveden	přivést	k5eAaPmNgInS	přivést
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
letadla	letadlo	k1gNnSc2	letadlo
před	před	k7c7	před
odletem	odlet	k1gInSc7	odlet
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
svým	svůj	k3xOyFgNnSc7	svůj
jednáním	jednání	k1gNnSc7	jednání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
např.	např.	kA	např.
nedošlo	dojít	k5eNaPmAgNnS	dojít
již	již	k6eAd1	již
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
soudní	soudní	k2eAgInSc1d1	soudní
tribunál	tribunál	k1gInSc1	tribunál
dělnicko-rolnické	dělnickoolnický	k2eAgFnSc2d1	dělnicko-rolnická
Indrovy	Indrův	k2eAgFnSc2d1	Indrova
vlády	vláda	k1gFnSc2	vláda
postavil	postavit	k5eAaPmAgMnS	postavit
před	před	k7c4	před
soud	soud	k1gInSc4	soud
Dubčeka	Dubčeek	k1gMnSc2	Dubčeek
<g/>
,	,	kIx,	,
Černíka	Černík	k1gMnSc2	Černík
<g/>
,	,	kIx,	,
Smrkovského	Smrkovský	k2eAgMnSc2d1	Smrkovský
<g/>
,	,	kIx,	,
Kriegla	Kriegl	k1gMnSc2	Kriegl
<g/>
,	,	kIx,	,
Špačka	Špaček	k1gMnSc2	Špaček
<g/>
,	,	kIx,	,
Šimona	Šimon	k1gMnSc2	Šimon
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
násilném	násilný	k2eAgNnSc6d1	násilné
odvlečení	odvlečení	k1gNnSc6	odvlečení
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
snahou	snaha	k1gFnSc7	snaha
prezidenta	prezident	k1gMnSc2	prezident
bylo	být	k5eAaImAgNnS	být
zabránit	zabránit	k5eAaPmF	zabránit
krveprolití	krveprolití	k1gNnSc1	krveprolití
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
tisíc	tisíc	k4xCgInPc2	tisíc
vojáků	voják	k1gMnPc2	voják
armád	armáda	k1gFnPc2	armáda
zemí	zem	k1gFnPc2	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
plně	plně	k6eAd1	plně
vyzbrojených	vyzbrojený	k2eAgFnPc2d1	vyzbrojená
těžkou	těžký	k2eAgFnSc7d1	těžká
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
delegace	delegace	k1gFnSc2	delegace
se	se	k3xPyFc4	se
ohrazovali	ohrazovat	k5eAaImAgMnP	ohrazovat
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podepsali	podepsat	k5eAaPmAgMnP	podepsat
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
jednání	jednání	k1gNnSc2	jednání
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
ústupky	ústupek	k1gInPc4	ústupek
sovětské	sovětský	k2eAgFnSc3d1	sovětská
straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Některé	některý	k3yIgInPc1	některý
zásahy	zásah	k1gInPc1	zásah
prezidenta	prezident	k1gMnSc2	prezident
Svobody	Svoboda	k1gMnSc2	Svoboda
během	během	k7c2	během
jednání	jednání	k1gNnSc2	jednání
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
nelze	lze	k6eNd1	lze
označit	označit	k5eAaPmF	označit
právě	právě	k9	právě
za	za	k7c4	za
šťastné	šťastný	k2eAgNnSc4d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
okamžicích	okamžik	k1gInPc6	okamžik
příliš	příliš	k6eAd1	příliš
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
rychlé	rychlý	k2eAgNnSc4d1	rychlé
přijetí	přijetí	k1gNnSc4	přijetí
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
diktovaných	diktovaný	k2eAgFnPc2d1	diktovaná
nám	my	k3xPp1nPc3	my
vedením	vedení	k1gNnSc7	vedení
KSSS	KSSS	kA	KSSS
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Brežněvem	Brežněv	k1gInSc7	Brežněv
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Prezident	prezident	k1gMnSc1	prezident
Svoboda	Svoboda	k1gMnSc1	Svoboda
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
přitom	přitom	k6eAd1	přitom
obavami	obava	k1gFnPc7	obava
z	z	k7c2	z
krveprolití	krveprolití	k1gNnSc2	krveprolití
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
v	v	k7c6	v
okupované	okupovaný	k2eAgFnSc6d1	okupovaná
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
...	...	k?	...
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Ludvíku	Ludvík	k1gMnSc3	Ludvík
Svobodovi	Svoboda	k1gMnSc3	Svoboda
zaslaném	zaslaný	k2eAgInSc6d1	zaslaný
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g />
.	.	kIx.	.
</s>
<s>
Národním	národní	k2eAgNnSc7d1	národní
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
,	,	kIx,	,
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
vysočanským	vysočanský	k2eAgInSc7d1	vysočanský
sjezdem	sjezd	k1gInSc7	sjezd
byla	být	k5eAaImAgFnS	být
pasáž	pasáž	k1gFnSc1	pasáž
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
prezidenta	prezident	k1gMnSc2	prezident
opravňovala	opravňovat	k5eAaImAgFnS	opravňovat
<g/>
,	,	kIx,	,
žádat	žádat	k5eAaImF	žádat
rychlé	rychlý	k2eAgNnSc4d1	rychlé
zakončení	zakončení	k1gNnSc4	zakončení
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
dopise	dopis	k1gInSc6	dopis
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
Napětí	napětí	k1gNnSc1	napětí
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
stupňuje	stupňovat	k5eAaImIp3nS	stupňovat
...	...	k?	...
jen	jen	k9	jen
s	s	k7c7	s
krajním	krajní	k2eAgNnSc7d1	krajní
vypětím	vypětí	k1gNnSc7	vypětí
je	být	k5eAaImIp3nS	být
udržován	udržován	k2eAgInSc1d1	udržován
klid	klid	k1gInSc1	klid
...	...	k?	...
Za	za	k7c4	za
záminku	záminka	k1gFnSc4	záminka
ke	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
cokoli	cokoli	k3yInSc1	cokoli
<g/>
.	.	kIx.	.
</s>
<s>
Okupačním	okupační	k2eAgInPc3d1	okupační
orgánům	orgán	k1gInPc3	orgán
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nepodařilo	podařit	k5eNaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
nejmenší	malý	k2eAgFnSc4d3	nejmenší
podporu	podpora	k1gFnSc4	podpora
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jejich	jejich	k3xOp3gFnSc4	jejich
nervozitu	nervozita	k1gFnSc4	nervozita
...	...	k?	...
Nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
stupňující	stupňující	k2eAgFnSc1d1	stupňující
se	se	k3xPyFc4	se
únava	únava	k1gFnSc1	únava
a	a	k8xC	a
nervové	nervový	k2eAgNnSc1d1	nervové
vypětí	vypětí	k1gNnSc1	vypětí
okupačních	okupační	k2eAgNnPc2d1	okupační
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
našeho	náš	k3xOp1gNnSc2	náš
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc4	napětí
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
...	...	k?	...
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Šilhán	šilhán	k2eAgMnSc1d1	šilhán
<g/>
,	,	kIx,	,
s.	s.	k?	s.
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
vojsk	vojsko	k1gNnPc2	vojsko
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c4	za
tichého	tichý	k2eAgMnSc4d1	tichý
souhlasů	souhlas	k1gInPc2	souhlas
zemí	zem	k1gFnPc2	zem
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
formálně	formálně	k6eAd1	formálně
protestovaly	protestovat	k5eAaBmAgFnP	protestovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
širšího	široký	k2eAgNnSc2d2	širší
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
hlediska	hledisko	k1gNnSc2	hledisko
primárním	primární	k2eAgInSc7d1	primární
problémem	problém	k1gInSc7	problém
vztahů	vztah	k1gInPc2	vztah
USA	USA	kA	USA
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
bylo	být	k5eAaImAgNnS	být
jednání	jednání	k1gNnSc1	jednání
o	o	k7c6	o
paritě	parita	k1gFnSc6	parita
strategických	strategický	k2eAgFnPc2d1	strategická
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g/>
jednání	jednání	k1gNnPc1	jednání
Brežněv	Brežněv	k1gFnPc2	Brežněv
-	-	kIx~	-
Johnson	Johnson	k1gMnSc1	Johnson
o	o	k7c6	o
základech	základ	k1gInPc6	základ
mírového	mírový	k2eAgNnSc2d1	Mírové
soužití	soužití	k1gNnSc2	soužití
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
společenskými	společenský	k2eAgNnPc7d1	společenské
zřízeními	zřízení	k1gNnPc7	zřízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
neměly	mít	k5eNaImAgFnP	mít
zájem	zájem	k1gInSc4	zájem
na	na	k7c4	na
eskalaci	eskalace	k1gFnSc4	eskalace
československého	československý	k2eAgInSc2d1	československý
problému	problém	k1gInSc2	problém
v	v	k7c4	v
širší	široký	k2eAgInSc4d2	širší
vojenský	vojenský	k2eAgInSc4d1	vojenský
konflikt	konflikt	k1gInSc4	konflikt
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
na	na	k7c6	na
intervenci	intervence	k1gFnSc6	intervence
proti	proti	k7c3	proti
invazi	invaze	k1gFnSc3	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
tak	tak	k6eAd1	tak
neměla	mít	k5eNaImAgFnS	mít
pouze	pouze	k6eAd1	pouze
ideologický	ideologický	k2eAgInSc4d1	ideologický
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vojensko-strategické	vojenskotrategický	k2eAgInPc4d1	vojensko-strategický
aspekty	aspekt	k1gInPc4	aspekt
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgNnSc4d1	sovětské
armádní	armádní	k2eAgNnSc4d1	armádní
velení	velení	k1gNnSc4	velení
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
snahu	snaha	k1gFnSc4	snaha
umístit	umístit	k5eAaPmF	umístit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
na	na	k7c4	na
čs	čs	kA	čs
<g/>
.	.	kIx.	.
západní	západní	k2eAgFnSc1d1	západní
hranice	hranice	k1gFnSc1	hranice
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
připravený	připravený	k2eAgInSc4d1	připravený
druhý	druhý	k4xOgInSc4	druhý
sled	sled	k1gInSc4	sled
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jak	jak	k8xS	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
moskevského	moskevský	k2eAgInSc2d1	moskevský
protokolu	protokol	k1gInSc2	protokol
byl	být	k5eAaImAgMnS	být
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
míře	míra	k1gFnSc6	míra
<g/>
,	,	kIx,	,
realizovat	realizovat	k5eAaBmF	realizovat
představy	představa	k1gFnPc4	představa
ozdravění	ozdravění	k1gNnSc2	ozdravění
socializmu	socializmus	k1gInSc2	socializmus
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
pracovního	pracovní	k2eAgInSc2d1	pracovní
kolektivu	kolektiv	k1gInSc2	kolektiv
Kanceláře	kancelář	k1gFnSc2	kancelář
prezidenta	prezident	k1gMnSc2	prezident
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
přizval	přizvat	k5eAaPmAgInS	přizvat
jako	jako	k9	jako
své	svůj	k3xOyFgMnPc4	svůj
poradce	poradce	k1gMnPc4	poradce
proreformní	proreformní	k2eAgMnPc4d1	proreformní
odborníky	odborník	k1gMnPc4	odborník
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
,	,	kIx,	,
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
i	i	k8xC	i
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
profesory	profesor	k1gMnPc4	profesor
Miroslava	Miroslav	k1gMnSc4	Miroslav
Kadlece	Kadlec	k1gMnSc4	Kadlec
<g/>
,	,	kIx,	,
Miroslava	Miroslav	k1gMnSc4	Miroslav
Tučka	Tuček	k1gMnSc4	Tuček
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc4	Karel
Svobodu	Svoboda	k1gMnSc4	Svoboda
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kulturu	kultura	k1gFnSc4	kultura
Vladislava	Vladislav	k1gMnSc2	Vladislav
Stanovského	Stanovský	k2eAgMnSc2d1	Stanovský
a	a	k8xC	a
diplomata	diplomat	k1gMnSc2	diplomat
Miloše	Miloš	k1gMnSc2	Miloš
Parise	Paris	k1gMnSc2	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Projev	projev	k1gInSc1	projev
prezidenta	prezident	k1gMnSc2	prezident
k	k	k7c3	k
padesátému	padesátý	k4xOgMnSc3	padesátý
výročí	výročí	k1gNnSc3	výročí
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
hovořil	hovořit	k5eAaImAgInS	hovořit
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
zásluhy	zásluha	k1gFnPc4	zásluha
zakladatelů	zakladatel	k1gMnPc2	zakladatel
republiky	republika	k1gFnSc2	republika
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc4	Beneš
a	a	k8xC	a
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
<g/>
.	.	kIx.	.
</s>
<s>
Připomenul	připomenout	k5eAaPmAgMnS	připomenout
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
poučení	poučení	k1gNnSc4	poučení
pro	pro	k7c4	pro
politiku	politika	k1gFnSc4	politika
státu	stát	k1gInSc2	stát
naší	náš	k3xOp1gFnSc2	náš
polohy	poloha	k1gFnSc2	poloha
i	i	k8xC	i
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
O	o	k7c6	o
naší	náš	k3xOp1gFnSc6	náš
budoucnosti	budoucnost	k1gFnSc6	budoucnost
jak	jak	k8xS	jak
o	o	k7c6	o
sudbách	sudba	k1gFnPc6	sudba
všech	všecek	k3xTgInPc6	všecek
národu	národ	k1gInSc6	národ
<g/>
,	,	kIx,	,
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
přirozené	přirozený	k2eAgFnPc1d1	přirozená
a	a	k8xC	a
historické	historický	k2eAgFnPc1d1	historická
reality	realita	k1gFnPc1	realita
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
fantastické	fantastický	k2eAgInPc4d1	fantastický
plány	plán	k1gInPc4	plán
a	a	k8xC	a
přání	přání	k1gNnSc4	přání
nesoudných	soudný	k2eNgFnPc2d1	nesoudná
politik	politika	k1gFnPc2	politika
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
úkolem	úkol	k1gInSc7	úkol
politiků	politik	k1gMnPc2	politik
vzdělaných	vzdělaný	k2eAgInPc2d1	vzdělaný
<g/>
,	,	kIx,	,
úkolem	úkol	k1gInSc7	úkol
státníků	státník	k1gMnPc2	státník
<g/>
,	,	kIx,	,
jasně	jasně	k6eAd1	jasně
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
naši	náš	k3xOp1gFnSc4	náš
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
náš	náš	k3xOp1gInSc4	náš
a	a	k8xC	a
našich	náš	k3xOp1gMnPc2	náš
sousedů	soused	k1gMnPc2	soused
stále	stále	k6eAd1	stále
a	a	k8xC	a
bedlivě	bedlivě	k6eAd1	bedlivě
stopovat	stopovat	k5eAaImF	stopovat
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
si	se	k3xPyFc3	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
národů	národ	k1gInPc2	národ
nejsme	být	k5eNaImIp1nP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nejmenší	malý	k2eAgMnSc1d3	nejmenší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
naše	náš	k3xOp1gNnSc1	náš
postavení	postavení	k1gNnSc1	postavení
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
naše	náš	k3xOp1gFnSc1	náš
nepočetnost	nepočetnost	k1gFnSc1	nepočetnost
nutí	nutit	k5eAaImIp3nS	nutit
nás	my	k3xPp1nPc4	my
k	k	k7c3	k
politice	politika	k1gFnSc6	politika
bdělé	bdělý	k2eAgMnPc4d1	bdělý
a	a	k8xC	a
opatrné	opatrný	k2eAgMnPc4d1	opatrný
<g/>
:	:	kIx,	:
nikoli	nikoli	k9	nikoli
chytrácké	chytrácký	k2eAgFnSc2d1	chytrácká
-	-	kIx~	-
doby	doba	k1gFnSc2	doba
politického	politický	k2eAgNnSc2d1	politické
chytrákování	chytrákování	k1gNnSc2	chytrákování
minuly	minout	k5eAaImAgFnP	minout
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nepřinesly	přinést	k5eNaPmAgFnP	přinést
skutečného	skutečný	k2eAgInSc2d1	skutečný
užitku	užitek	k1gInSc2	užitek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
schválilo	schválit	k5eAaPmAgNnS	schválit
zákon	zákon	k1gInSc4	zákon
o	o	k7c4	o
federalizaci	federalizace	k1gFnSc4	federalizace
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
federalizaci	federalizace	k1gFnSc4	federalizace
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
,	,	kIx,	,
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
Slovensko	Slovensko	k1gNnSc1	Slovensko
po	po	k7c6	po
50	[number]	k4	50
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
prezident	prezident	k1gMnSc1	prezident
stvrdil	stvrdit	k5eAaPmAgMnS	stvrdit
federalizaci	federalizace	k1gFnSc4	federalizace
svým	svůj	k3xOyFgInSc7	svůj
podpisem	podpis	k1gInSc7	podpis
na	na	k7c6	na
Bratislavském	bratislavský	k2eAgInSc6d1	bratislavský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
federalizovaném	federalizovaný	k2eAgInSc6d1	federalizovaný
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgMnS	přijmout
prezident	prezident	k1gMnSc1	prezident
v	v	k7c6	v
odděleném	oddělený	k2eAgNnSc6d1	oddělené
slyšení	slyšení	k1gNnSc6	slyšení
vládu	vláda	k1gFnSc4	vláda
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
plénu	plénum	k1gNnSc6	plénum
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
strany	strana	k1gFnSc2	strana
i	i	k9	i
v	v	k7c6	v
aparátě	aparát	k1gInSc6	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
byla	být	k5eAaImAgFnS	být
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
protireformních	protireformní	k2eAgMnPc2d1	protireformní
dogmatiků	dogmatik	k1gMnPc2	dogmatik
a	a	k8xC	a
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
politiků	politik	k1gMnPc2	politik
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
vytvářet	vytvářet	k5eAaImF	vytvářet
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
návrat	návrat	k1gInSc4	návrat
starých	starý	k2eAgFnPc2d1	stará
praktik	praktika	k1gFnPc2	praktika
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
dělo	dít	k5eAaBmAgNnS	dít
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Sovětů	Sovět	k1gMnPc2	Sovět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pod	pod	k7c7	pod
nepřímým	přímý	k2eNgInSc7d1	nepřímý
vlivem	vliv	k1gInSc7	vliv
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
velmocemi	velmoc	k1gFnPc7	velmoc
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1968	[number]	k4	1968
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Československo	Československo	k1gNnSc4	Československo
americký	americký	k2eAgMnSc1d1	americký
kongresman	kongresman	k1gMnSc1	kongresman
Charles	Charles	k1gMnSc1	Charles
Vanik	Vanik	k1gMnSc1	Vanik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
své	svůj	k3xOyFgFnPc4	svůj
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
neurovnaných	urovnaný	k2eNgInPc2d1	neurovnaný
vztahů	vztah	k1gInPc2	vztah
Československa	Československo	k1gNnSc2	Československo
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
zdůraznil	zdůraznit	k5eAaPmAgInS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prioritou	priorita	k1gFnSc7	priorita
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
zachování	zachování	k1gNnSc4	zachování
rovnováhy	rovnováha	k1gFnSc2	rovnováha
sil	síla	k1gFnPc2	síla
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
spáchal	spáchat	k5eAaPmAgMnS	spáchat
student	student	k1gMnSc1	student
Jan	Jan	k1gMnSc1	Jan
Palach	palach	k1gInSc4	palach
sebevraždu	sebevražda	k1gFnSc4	sebevražda
upálením	upálení	k1gNnSc7	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Žádal	žádat	k5eAaImAgInS	žádat
zrušit	zrušit	k5eAaPmF	zrušit
cenzuru	cenzura	k1gFnSc4	cenzura
a	a	k8xC	a
zakázat	zakázat	k5eAaPmF	zakázat
šíření	šíření	k1gNnSc4	šíření
sovětské	sovětský	k2eAgFnSc2d1	sovětská
tiskoviny	tiskovina	k1gFnSc2	tiskovina
Zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidé	člověk	k1gMnPc1	člověk
zahájili	zahájit	k5eAaPmAgMnP	zahájit
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
těchto	tento	k3xDgInPc2	tento
požadavků	požadavek	k1gInPc2	požadavek
časově	časově	k6eAd1	časově
neomezenou	omezený	k2eNgFnSc4d1	neomezená
stávku	stávka	k1gFnSc4	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
nebyly	být	k5eNaImAgInP	být
požadavky	požadavek	k1gInPc1	požadavek
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
splněny	splněn	k2eAgFnPc4d1	splněna
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
vzplanout	vzplanout	k5eAaPmF	vzplanout
"	"	kIx"	"
<g/>
další	další	k2eAgFnPc4d1	další
pochodně	pochodeň	k1gFnPc4	pochodeň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgNnSc6d1	rozhlasové
vystoupení	vystoupení	k1gNnSc6	vystoupení
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
tragickému	tragický	k2eAgInSc3d1	tragický
činu	čin	k1gInSc3	čin
prezident	prezident	k1gMnSc1	prezident
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
Jako	jako	k8xC	jako
voják	voják	k1gMnSc1	voják
dovedu	dovést	k5eAaPmIp1nS	dovést
ocenit	ocenit	k5eAaPmF	ocenit
sebezapření	sebezapření	k1gNnSc4	sebezapření
i	i	k8xC	i
osobní	osobní	k2eAgFnSc4d1	osobní
statečnost	statečnost	k1gFnSc4	statečnost
Jana	Jan	k1gMnSc2	Jan
Palacha	Palach	k1gMnSc2	Palach
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
i	i	k8xC	i
občan	občan	k1gMnSc1	občan
naší	náš	k3xOp1gFnSc2	náš
republiky	republika	k1gFnSc2	republika
však	však	k9	však
nemohu	moct	k5eNaImIp1nS	moct
skrýt	skrýt	k5eAaPmF	skrýt
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesouhlasím	souhlasit	k5eNaImIp1nS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
vyjadřovaly	vyjadřovat	k5eAaImAgInP	vyjadřovat
politické	politický	k2eAgInPc1d1	politický
postoje	postoj	k1gInPc1	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
jsem	být	k5eAaImIp1nS	být
dostal	dostat	k5eAaPmAgMnS	dostat
otřesnou	otřesný	k2eAgFnSc4d1	otřesná
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
vztáhl	vztáhnout	k5eAaPmAgInS	vztáhnout
ruku	ruka	k1gFnSc4	ruka
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
další	další	k2eAgMnSc1d1	další
mladý	mladý	k2eAgMnSc1d1	mladý
člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Hlavatý	Hlavatý	k1gMnSc1	Hlavatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vaše	váš	k3xOp2gMnPc4	váš
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
za	za	k7c4	za
všechen	všechen	k3xTgInSc4	všechen
lid	lid	k1gInSc4	lid
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
i	i	k9	i
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
lidskosti	lidskost	k1gFnSc2	lidskost
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
zavázali	zavázat	k5eAaPmAgMnP	zavázat
<g/>
,	,	kIx,	,
vás	vy	k3xPp2nPc4	vy
žádám	žádat	k5eAaImIp1nS	žádat
<g/>
,	,	kIx,	,
zastavte	zastavit	k5eAaPmRp2nP	zastavit
tyto	tento	k3xDgInPc4	tento
strašlivé	strašlivý	k2eAgInPc4d1	strašlivý
činy	čin	k1gInPc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
už	už	k9	už
jsem	být	k5eAaImIp1nS	být
často	často	k6eAd1	často
hleděl	hledět	k5eAaImAgMnS	hledět
smrti	smrt	k1gFnPc4	smrt
do	do	k7c2	do
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
povinnost	povinnost	k1gFnSc4	povinnost
občana	občan	k1gMnSc2	občan
však	však	k9	však
považuji	považovat	k5eAaImIp1nS	považovat
dát	dát	k5eAaPmF	dát
své	své	k1gNnSc4	své
vlasti	vlast	k1gFnSc2	vlast
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
po	po	k7c6	po
srpnu	srpen	k1gInSc6	srpen
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
aktivitě	aktivita	k1gFnSc6	aktivita
<g/>
,	,	kIx,	,
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
pracoviště	pracoviště	k1gNnSc4	pracoviště
na	na	k7c6	na
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
setkával	setkávat	k5eAaImAgMnS	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
různých	různý	k2eAgInPc2d1	různý
oborů	obor	k1gInPc2	obor
a	a	k8xC	a
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Podporoval	podporovat	k5eAaImAgInS	podporovat
řadu	řada	k1gFnSc4	řada
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
považoval	považovat	k5eAaImAgInS	považovat
jako	jako	k9	jako
impuls	impuls	k1gInSc1	impuls
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
stavbu	stavba	k1gFnSc4	stavba
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc4	rozvoj
družstevního	družstevní	k2eAgNnSc2d1	družstevní
bytového	bytový	k2eAgNnSc2d1	bytové
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
řešila	řešit	k5eAaImAgFnS	řešit
bytová	bytový	k2eAgFnSc1d1	bytová
nouze	nouze	k1gFnSc1	nouze
<g/>
,	,	kIx,	,
otevření	otevření	k1gNnSc1	otevření
cesty	cesta	k1gFnSc2	cesta
pro	pro	k7c4	pro
přidruženou	přidružený	k2eAgFnSc4d1	přidružená
výrobu	výroba	k1gFnSc4	výroba
zemědělských	zemědělský	k2eAgNnPc2d1	zemědělské
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Navštívil	navštívit	k5eAaPmAgInS	navštívit
řadu	řada	k1gFnSc4	řada
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
věnoval	věnovat	k5eAaImAgMnS	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
účinnou	účinný	k2eAgFnSc4d1	účinná
pomoc	pomoc	k1gFnSc4	pomoc
východnímu	východní	k2eAgNnSc3d1	východní
Slovensku	Slovensko	k1gNnSc3	Slovensko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
odhalením	odhalení	k1gNnSc7	odhalení
sochy	socha	k1gFnSc2	socha
L.	L.	kA	L.
Svobody	Svoboda	k1gMnSc2	Svoboda
ve	v	k7c6	v
Svidníku	Svidník	k1gInSc6	Svidník
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
republika	republika	k1gFnSc1	republika
neupadla	upadnout	k5eNaPmAgFnS	upadnout
do	do	k7c2	do
izolace	izolace	k1gFnSc2	izolace
<g/>
,	,	kIx,	,
vykonal	vykonat	k5eAaPmAgMnS	vykonat
návštěvu	návštěva	k1gFnSc4	návštěva
Íránu	Írán	k1gInSc2	Írán
<g/>
,	,	kIx,	,
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
přijal	přijmout	k5eAaPmAgMnS	přijmout
návštěvy	návštěva	k1gFnPc4	návštěva
řady	řada	k1gFnSc2	řada
cizích	cizí	k2eAgFnPc2d1	cizí
hlav	hlava	k1gFnPc2	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přetrvávající	přetrvávající	k2eAgFnSc6d1	přetrvávající
rozjitřené	rozjitřený	k2eAgFnSc6d1	rozjitřená
situaci	situace	k1gFnSc6	situace
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
plenárním	plenární	k2eAgNnSc6d1	plenární
zasedání	zasedání	k1gNnSc6	zasedání
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
odvolán	odvolat	k5eAaPmNgInS	odvolat
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
1	[number]	k4	1
<g/>
.	.	kIx.	.
tajemník	tajemník	k1gMnSc1	tajemník
A.	A.	kA	A.
Dubček	Dubček	k1gMnSc1	Dubček
a	a	k8xC	a
místo	místo	k7c2	místo
něho	on	k3xPp3gMnSc2	on
zvolen	zvolit	k5eAaPmNgMnS	zvolit
G.	G.	kA	G.
Husák	Husák	k1gMnSc1	Husák
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
podpořil	podpořit	k5eAaPmAgMnS	podpořit
tuto	tento	k3xDgFnSc4	tento
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
G.	G.	kA	G.
Husákovi	Husák	k1gMnSc6	Husák
"	"	kIx"	"
<g/>
energického	energický	k2eAgMnSc4d1	energický
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
racionálně	racionálně	k6eAd1	racionálně
uvažujícího	uvažující	k2eAgMnSc4d1	uvažující
<g/>
,	,	kIx,	,
zapáleného	zapálený	k2eAgMnSc4d1	zapálený
stoupence	stoupenec	k1gMnSc4	stoupenec
nezbytných	zbytný	k2eNgNnPc2d1	nezbytné
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
L.	L.	kA	L.
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
živé	živý	k2eAgFnSc6d1	živá
paměti	paměť	k1gFnSc6	paměť
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
něho	on	k3xPp3gNnSc2	on
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
že	že	k8xS	že
G.	G.	kA	G.
Husák	Husák	k1gMnSc1	Husák
poznal	poznat	k5eAaPmAgMnS	poznat
nezákonnosti	nezákonnost	k1gFnSc6	nezákonnost
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
osobně	osobně	k6eAd1	osobně
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
kůži	kůže	k1gFnSc6	kůže
a	a	k8xC	a
dalo	dát	k5eAaPmAgNnS	dát
se	se	k3xPyFc4	se
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
dosti	dosti	k6eAd1	dosti
vůle	vůle	k1gFnSc2	vůle
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
usiloval	usilovat	k5eAaImAgMnS	usilovat
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
část	část	k1gFnSc1	část
reformních	reformní	k2eAgFnPc2d1	reformní
snah	snaha	k1gFnPc2	snaha
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k9	že
bude	být	k5eAaImBp3nS	být
stoupencem	stoupenec	k1gMnSc7	stoupenec
dokončení	dokončení	k1gNnSc6	dokončení
rehabilitací	rehabilitace	k1gFnPc2	rehabilitace
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
padesátých	padesátý	k4xOgInPc6	padesátý
<g />
.	.	kIx.	.
</s>
<s>
letech	let	k1gInPc6	let
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
odsouzených	odsouzený	k2eAgMnPc2d1	odsouzený
<g/>
.	.	kIx.	.
...	...	k?	...
jako	jako	k8xC	jako
bývalý	bývalý	k2eAgMnSc1d1	bývalý
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
bude	být	k5eAaImBp3nS	být
zárukou	záruka	k1gFnSc7	záruka
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
novým	nový	k2eAgInPc3d1	nový
politickým	politický	k2eAgInPc3d1	politický
procesům	proces	k1gInPc3	proces
<g/>
.	.	kIx.	.
...	...	k?	...
obava	obava	k1gFnSc1	obava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vývoj	vývoj	k1gInSc1	vývoj
nezvrtl	zvrtnout	k5eNaPmAgInS	zvrtnout
tímto	tento	k3xDgInSc7	tento
nežádoucím	žádoucí	k2eNgInSc7d1	nežádoucí
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
tady	tady	k6eAd1	tady
v	v	k7c6	v
podvědomí	podvědomí	k1gNnSc6	podvědomí
stále	stále	k6eAd1	stále
byla	být	k5eAaImAgFnS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Svobodu	Svoboda	k1gMnSc4	Svoboda
přinesla	přinést	k5eAaPmAgFnS	přinést
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
postupně	postupně	k6eAd1	postupně
umrtvení	umrtvení	k1gNnSc1	umrtvení
jeho	on	k3xPp3gInSc2	on
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
politického	politický	k2eAgInSc2d1	politický
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
vykázání	vykázání	k1gNnSc2	vykázání
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
do	do	k7c2	do
limitů	limit	k1gInPc2	limit
ústavně	ústavně	k6eAd1	ústavně
vytyčených	vytyčený	k2eAgFnPc2d1	vytyčená
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
muži	muž	k1gMnPc7	muž
postupně	postupně	k6eAd1	postupně
ochabovaly	ochabovat	k5eAaImAgFnP	ochabovat
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
když	když	k8xS	když
Husák	Husák	k1gMnSc1	Husák
začal	začít	k5eAaPmAgMnS	začít
energicky	energicky	k6eAd1	energicky
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
ústavně	ústavně	k6eAd1	ústavně
zakotvenou	zakotvený	k2eAgFnSc4d1	zakotvená
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
úlohu	úloha	k1gFnSc4	úloha
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
ambice	ambice	k1gFnPc4	ambice
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
prim	prima	k1gFnPc2	prima
prosazoval	prosazovat	k5eAaImAgInS	prosazovat
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
druhořadým	druhořadý	k2eAgMnSc7d1	druhořadý
<g/>
,	,	kIx,	,
podřízeným	podřízený	k2eAgMnSc7d1	podřízený
partnerem	partner	k1gMnSc7	partner
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
G.	G.	kA	G.
Husáka	Husák	k1gMnSc2	Husák
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
rozpuštěn	rozpuštěn	k2eAgInSc1d1	rozpuštěn
dokonce	dokonce	k9	dokonce
prezidentův	prezidentův	k2eAgInSc1d1	prezidentův
poradní	poradní	k2eAgInSc1d1	poradní
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Normalizace	normalizace	k1gFnSc1	normalizace
"	"	kIx"	"
<g/>
systematicky	systematicky	k6eAd1	systematicky
pohřbívala	pohřbívat	k5eAaImAgFnS	pohřbívat
jednu	jeden	k4xCgFnSc4	jeden
obrodnou	obrodný	k2eAgFnSc4d1	obrodná
vymoženost	vymoženost	k1gFnSc4	vymoženost
za	za	k7c7	za
druhou	druhý	k4xOgFnSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
zahájeny	zahájen	k2eAgFnPc1d1	zahájena
stranické	stranický	k2eAgFnPc1d1	stranická
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc1d1	pracovní
prověrky	prověrka	k1gFnPc1	prověrka
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
již	již	k6eAd1	již
hlavní	hlavní	k2eAgMnPc1d1	hlavní
představitelé	představitel	k1gMnPc1	představitel
demokratizačního	demokratizační	k2eAgInSc2d1	demokratizační
programu	program	k1gInSc2	program
zbaveni	zbavit	k5eAaPmNgMnP	zbavit
všech	všecek	k3xTgFnPc2	všecek
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
řídících	řídící	k2eAgNnPc2d1	řídící
postavení	postavení	k1gNnPc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Prověrkovým	prověrkový	k2eAgFnPc3d1	prověrková
komisím	komise	k1gFnPc3	komise
nestačilo	stačit	k5eNaBmAgNnS	stačit
potrestat	potrestat	k5eAaPmF	potrestat
reformní	reformní	k2eAgFnSc4d1	reformní
elitu	elita	k1gFnSc4	elita
<g/>
,	,	kIx,	,
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vyškrtnout	vyškrtnout	k5eAaPmF	vyškrtnout
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
přes	přes	k7c4	přes
400	[number]	k4	400
000	[number]	k4	000
dalších	další	k2eAgMnPc2d1	další
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
angažovaných	angažovaný	k2eAgMnPc2d1	angažovaný
a	a	k8xC	a
odborně	odborně	k6eAd1	odborně
vyspělých	vyspělý	k2eAgMnPc2d1	vyspělý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Husák	Husák	k1gMnSc1	Husák
si	se	k3xPyFc3	se
původně	původně	k6eAd1	původně
takový	takový	k3xDgInSc4	takový
kádrový	kádrový	k2eAgInSc4d1	kádrový
masakr	masakr	k1gInSc4	masakr
nepřál	přát	k5eNaImAgMnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
L.	L.	kA	L.
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
dotčený	dotčený	k2eAgInSc1d1	dotčený
masovou	masový	k2eAgFnSc7d1	masová
likvidací	likvidace	k1gFnSc7	likvidace
intelektuálních	intelektuální	k2eAgFnPc2d1	intelektuální
elit	elita	k1gFnPc2	elita
<g/>
,	,	kIx,	,
neuspěl	uspět	k5eNaPmAgMnS	uspět
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
někoho	někdo	k3yInSc4	někdo
z	z	k7c2	z
postižených	postižený	k1gMnPc2	postižený
uchránit	uchránit	k5eAaPmF	uchránit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
byl	být	k5eAaImAgMnS	být
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
spoluzodpovědným	spoluzodpovědný	k2eAgFnPc3d1	spoluzodpovědná
za	za	k7c4	za
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jiných	jiný	k1gMnPc2	jiný
prezident	prezident	k1gMnSc1	prezident
Svoboda	Svoboda	k1gMnSc1	Svoboda
nemohl	moct	k5eNaImAgMnS	moct
proces	proces	k1gInSc4	proces
normalizace	normalizace	k1gFnSc2	normalizace
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
formálně	formálně	k6eAd1	formálně
přizván	přizvat	k5eAaPmNgMnS	přizvat
do	do	k7c2	do
politbyra	politbyro	k1gNnSc2	politbyro
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Ideologičtí	ideologický	k2eAgMnPc1d1	ideologický
fundamentalisté	fundamentalista	k1gMnPc1	fundamentalista
opět	opět	k6eAd1	opět
začali	začít	k5eAaPmAgMnP	začít
střežit	střežit	k5eAaImF	střežit
"	"	kIx"	"
<g/>
čistotu	čistota	k1gFnSc4	čistota
<g/>
"	"	kIx"	"
zveřejňovaných	zveřejňovaný	k2eAgInPc2d1	zveřejňovaný
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Neobstály	obstát	k5eNaPmAgFnP	obstát
ani	ani	k9	ani
paměti	paměť	k1gFnPc1	paměť
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
nové	nový	k2eAgNnSc1d1	nové
vydání	vydání	k1gNnSc1	vydání
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
a	a	k8xC	a
do	do	k7c2	do
stoupy	stoupa	k1gFnSc2	stoupa
přišel	přijít	k5eAaPmAgMnS	přijít
i	i	k9	i
připravovaný	připravovaný	k2eAgInSc4d1	připravovaný
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
Cestami	cesta	k1gFnPc7	cesta
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1973	[number]	k4	1973
byl	být	k5eAaImAgMnS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
znovu	znovu	k6eAd1	znovu
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
postihla	postihnout	k5eAaPmAgFnS	postihnout
mozková	mozkový	k2eAgFnSc1d1	mozková
příhoda	příhoda	k1gFnSc1	příhoda
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
potíže	potíž	k1gFnPc1	potíž
se	se	k3xPyFc4	se
opakovaly	opakovat	k5eAaImAgFnP	opakovat
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1974	[number]	k4	1974
byl	být	k5eAaImAgInS	být
postižen	postihnout	k5eAaPmNgInS	postihnout
infarktem	infarkt	k1gInSc7	infarkt
plic	plíce	k1gFnPc2	plíce
(	(	kIx(	(
<g/>
plicní	plicní	k2eAgFnSc2d1	plicní
embolie	embolie	k1gFnSc2	embolie
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
měl	mít	k5eAaImAgInS	mít
několik	několik	k4yIc4	několik
mozkových	mozkový	k2eAgFnPc2d1	mozková
příhod	příhoda	k1gFnPc2	příhoda
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
vykonávat	vykonávat	k5eAaImF	vykonávat
nemohl	moct	k5eNaImAgMnS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
a	a	k8xC	a
publicistů	publicista	k1gMnPc2	publicista
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
lpěl	lpět	k5eAaImAgMnS	lpět
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
funkci	funkce	k1gFnSc6	funkce
a	a	k8xC	a
nechtěl	chtít	k5eNaImAgMnS	chtít
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
jeho	jeho	k3xOp3gFnSc2	jeho
dcery	dcera	k1gFnSc2	dcera
Zoe	Zoe	k1gFnPc2	Zoe
Klusákové	Klusákové	k2eAgInPc2d1	Klusákové
-	-	kIx~	-
Svobodové	Svobodové	k2eAgInPc2d1	Svobodové
však	však	k9	však
G.	G.	kA	G.
Husák	Husák	k1gMnSc1	Husák
nechtěl	chtít	k5eNaImAgMnS	chtít
přijmout	přijmout	k5eAaPmF	přijmout
jeho	jeho	k3xOp3gFnSc4	jeho
demisi	demise	k1gFnSc4	demise
<g/>
.	.	kIx.	.
</s>
<s>
Zdůvodňoval	zdůvodňovat	k5eAaImAgMnS	zdůvodňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
prezidenta	prezident	k1gMnSc2	prezident
připravena	připravit	k5eAaPmNgFnS	připravit
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
nedůvěrou	nedůvěra	k1gFnSc7	nedůvěra
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentské	prezidentský	k2eAgNnSc1d1	prezidentské
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
bylo	být	k5eAaImAgNnS	být
předčasně	předčasně	k6eAd1	předčasně
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
až	až	k9	až
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
přijatého	přijatý	k2eAgInSc2d1	přijatý
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
50	[number]	k4	50
<g/>
/	/	kIx~	/
<g/>
1975	[number]	k4	1975
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
G.	G.	kA	G.
Husák	Husák	k1gMnSc1	Husák
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
funkcí	funkce	k1gFnSc7	funkce
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
zakotveno	zakotvit	k5eAaPmNgNnS	zakotvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nemůže	moct	k5eNaImIp3nS	moct
<g/>
-li	i	k?	-li
prezident	prezident	k1gMnSc1	prezident
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
vykonávat	vykonávat	k5eAaImF	vykonávat
<g/>
,	,	kIx,	,
přísluší	příslušet	k5eAaImIp3nS	příslušet
výkon	výkon	k1gInSc4	výkon
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
funkcí	funkce	k1gFnPc2	funkce
vládě	vláda	k1gFnSc3	vláda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tím	ten	k3xDgNnSc7	ten
pověří	pověřit	k5eAaPmIp3nS	pověřit
ministerského	ministerský	k2eAgMnSc4d1	ministerský
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k9	tak
opět	opět	k6eAd1	opět
ke	k	k7c3	k
kumulaci	kumulace	k1gFnSc3	kumulace
obou	dva	k4xCgFnPc6	dva
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
funkcí	funkce	k1gFnPc2	funkce
-	-	kIx~	-
stranické	stranický	k2eAgFnSc2d1	stranická
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
tolik	tolik	k6eAd1	tolik
kritizované	kritizovaný	k2eAgInPc1d1	kritizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
žil	žít	k5eAaImAgMnS	žít
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Irenou	Irena	k1gFnSc7	Irena
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
Břevnově	Břevnov	k1gInSc6	Břevnov
<g/>
,	,	kIx,	,
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
vilce	vilka	k1gFnSc6	vilka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Svobodovi	Svobodův	k2eAgMnPc1d1	Svobodův
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
ještě	ještě	k9	ještě
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
do	do	k7c2	do
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Irena	Irena	k1gFnSc1	Irena
Svobodová	Svobodová	k1gFnSc1	Svobodová
ho	on	k3xPp3gMnSc4	on
následovala	následovat	k5eAaImAgFnS	následovat
za	za	k7c4	za
deset	deset	k4xCc4	deset
měsíců	měsíc	k1gInPc2	měsíc
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Pohřeb	pohřeb	k1gInSc1	pohřeb
byl	být	k5eAaImAgInS	být
státní	státní	k2eAgMnSc1d1	státní
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
<g/>
,	,	kIx,	,
rakev	rakev	k1gFnSc1	rakev
byla	být	k5eAaImAgFnS	být
vezena	vézt	k5eAaImNgFnS	vézt
na	na	k7c6	na
dělové	dělový	k2eAgFnSc6d1	dělová
lafetě	lafeta	k1gFnSc6	lafeta
z	z	k7c2	z
Hradu	hrad	k1gInSc2	hrad
na	na	k7c4	na
konec	konec	k1gInSc4	konec
Letenské	letenský	k2eAgFnSc2d1	Letenská
pláně	pláň	k1gFnSc2	pláň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
pohřebního	pohřební	k2eAgInSc2d1	pohřební
vozu	vůz	k1gInSc2	vůz
za	za	k7c2	za
zvuku	zvuk	k1gInSc2	zvuk
vojenské	vojenský	k2eAgFnSc2d1	vojenská
písně	píseň	k1gFnSc2	píseň
1	[number]	k4	1
<g/>
.	.	kIx.	.
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
"	"	kIx"	"
<g/>
Směr	směr	k1gInSc1	směr
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
a	a	k8xC	a
odvezena	odvézt	k5eAaPmNgFnS	odvézt
do	do	k7c2	do
krematoria	krematorium	k1gNnSc2	krematorium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pohřbů	pohřeb	k1gInPc2	pohřeb
předcházejících	předcházející	k2eAgMnPc2d1	předcházející
prezidentů	prezident	k1gMnPc2	prezident
účast	účast	k1gFnSc4	účast
veřejnosti	veřejnost	k1gFnSc2	veřejnost
nebyla	být	k5eNaImAgFnS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
L.	L.	kA	L.
Svobodu	svoboda	k1gFnSc4	svoboda
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
tisíce	tisíc	k4xCgInPc1	tisíc
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
přišly	přijít	k5eAaPmAgInP	přijít
rozloučit	rozloučit	k5eAaPmF	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kremaci	kremace	k1gFnSc6	kremace
byla	být	k5eAaImAgFnS	být
urna	urna	k1gFnSc1	urna
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
památníku	památník	k1gInSc6	památník
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
přenesena	přenést	k5eAaPmNgFnS	přenést
do	do	k7c2	do
rodinné	rodinný	k2eAgFnSc2d1	rodinná
hrobky	hrobka	k1gFnSc2	hrobka
Svobodových	Svobodových	k2eAgFnSc2d1	Svobodových
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Budujeme	budovat	k5eAaImIp1nP	budovat
novou	nový	k2eAgFnSc4d1	nová
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
87	[number]	k4	87
s.	s.	k?	s.
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Bojovali	bojovat	k5eAaImAgMnP	bojovat
sme	sme	k?	sme
po	po	k7c6	po
boku	bok	k1gInSc6	bok
velkej	velkej	k?	velkej
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Osveta	Osveta	k1gFnSc1	Osveta
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
54	[number]	k4	54
s.	s.	k?	s.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
projevů	projev	k1gInPc2	projev
a	a	k8xC	a
článků	článek	k1gInPc2	článek
1942	[number]	k4	1942
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
176	[number]	k4	176
s.	s.	k?	s.
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Buzuluku	Buzuluk	k1gInSc2	Buzuluk
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
224	[number]	k4	224
s.	s.	k?	s.
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Cestami	cesta	k1gFnPc7	cesta
života	život	k1gInSc2	život
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Orego	Orego	k1gNnSc1	Orego
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
401	[number]	k4	401
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902107	[number]	k4	902107
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Cestami	cesta	k1gFnPc7	cesta
života	život	k1gInSc2	život
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Prospektrum	Prospektrum	k1gNnSc1	Prospektrum
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
401	[number]	k4	401
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85431	[number]	k4	85431
<g/>
-	-	kIx~	-
<g/>
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
válečné	válečný	k2eAgFnSc2d1	válečná
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
301	[number]	k4	301
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgInPc1d1	československý
řády	řád	k1gInPc1	řád
a	a	k8xC	a
dekorace	dekorace	k1gFnPc1	dekorace
<g/>
:	:	kIx,	:
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hvězda	hvězda	k1gFnSc1	hvězda
Hrdiny	Hrdina	k1gMnSc2	Hrdina
ČSSR	ČSSR	kA	ČSSR
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hvězda	hvězda	k1gFnSc1	hvězda
Hrdiny	Hrdina	k1gMnSc2	Hrdina
ČSSR	ČSSR	kA	ČSSR
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hvězda	hvězda	k1gFnSc1	hvězda
Hrdiny	Hrdina	k1gMnSc2	Hrdina
ČSSR	ČSSR	kA	ČSSR
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
"	"	kIx"	"
<g/>
Za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
"	"	kIx"	"
Hvězda	hvězda	k1gFnSc1	hvězda
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
Řád	řád	k1gInSc1	řád
Sokola	Sokol	k1gMnSc2	Sokol
s	s	k7c7	s
meči	meč	k1gInPc7	meč
Řád	řád	k1gInSc1	řád
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
Československý	československý	k2eAgInSc1d1	československý
válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
1914-1918	[number]	k4	1914-1918
-	-	kIx~	-
čtyři	čtyři	k4xCgMnPc1	čtyři
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
medaile	medaile	k1gFnSc1	medaile
se	s	k7c7	s
štítkem	štítek	k1gInSc7	štítek
Zborov	Zborov	k1gInSc1	Zborov
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
medaile	medaile	k1gFnSc1	medaile
Za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
Zborovská	Zborovský	k2eAgFnSc1d1	Zborovská
pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
Bachmačská	Bachmačský	k2eAgFnSc1d1	Bachmačská
pamětní	pamětní	k2eAgFnPc4d1	pamětní
medaile	medaile	k1gFnPc4	medaile
Pamětní	pamětní	k2eAgFnSc2d1	pamětní
medaile	medaile	k1gFnSc2	medaile
čs	čs	kA	čs
<g/>
.	.	kIx.	.
obce	obec	k1gFnSc2	obec
dobrovolecké	dobrovolecký	k2eAgFnSc2d1	dobrovolecký
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
(	(	kIx(	(
<g/>
odznak	odznak	k1gInSc1	odznak
<g/>
)	)	kIx)	)
Pamětní	pamětní	k2eAgInSc1d1	pamětní
odznak	odznak	k1gInSc1	odznak
čs	čs	kA	čs
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
dobrovolníka	dobrovolník	k1gMnSc2	dobrovolník
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
kříž	kříž	k1gInSc1	kříž
<g/>
)	)	kIx)	)
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
3	[number]	k4	3
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
Pamětní	pamětní	k2eAgFnSc2d1	pamětní
medaile	medaile	k1gFnSc2	medaile
4	[number]	k4	4
<g/>
.	.	kIx.	.
střeleckého	střelecký	k2eAgInSc2d1	střelecký
pluku	pluk	k1gInSc2	pluk
Prokopa	Prokop	k1gMnSc2	Prokop
Velikého	veliký	k2eAgMnSc2d1	veliký
Pamětní	pamětní	k2eAgInSc4d1	pamětní
medaile	medaile	k1gFnPc4	medaile
5	[number]	k4	5
<g/>
.	.	kIx.	.
střeleckého	střelecký	k2eAgInSc2d1	střelecký
pluku	pluk	k1gInSc2	pluk
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
6	[number]	k4	6
<g/>
.	.	kIx.	.
střeleckého	střelecký	k2eAgInSc2d1	střelecký
<g />
.	.	kIx.	.
</s>
<s>
hanáckého	hanácký	k2eAgInSc2d1	hanácký
pluku	pluk	k1gInSc2	pluk
Pamětní	pamětní	k2eAgFnSc2d1	pamětní
medaile	medaile	k1gFnSc2	medaile
9	[number]	k4	9
<g/>
.	.	kIx.	.
střeleckého	střelecký	k2eAgInSc2d1	střelecký
pluku	pluk	k1gInSc2	pluk
K.H.	K.H.	k1gMnPc2	K.H.
Borovského	Borovského	k2eAgFnSc2d1	Borovského
Pamětní	pamětní	k2eAgFnSc2d1	pamětní
medaile	medaile	k1gFnSc2	medaile
10	[number]	k4	10
<g/>
.	.	kIx.	.
střeleckého	střelecký	k2eAgInSc2d1	střelecký
pluku	pluk	k1gInSc2	pluk
J.	J.	kA	J.
S.	S.	kA	S.
Koziny	kozin	k2eAgFnSc2d1	Kozina
z	z	k7c2	z
Hrádku	Hrádok	k1gInSc2	Hrádok
Pamětní	pamětní	k2eAgFnSc2d1	pamětní
medaile	medaile	k1gFnSc2	medaile
21	[number]	k4	21
<g/>
.	.	kIx.	.
střeleckého	střelecký	k2eAgInSc2d1	střelecký
pluku	pluk	k1gInSc2	pluk
terronského	terronský	k2eAgInSc2d1	terronský
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
30	[number]	k4	30
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
Pamětní	pamětní	k2eAgInSc4d1	pamětní
kříž	kříž	k1gInSc4	kříž
1	[number]	k4	1
<g/>
.	.	kIx.	.
jízdního	jízdní	k2eAgInSc2d1	jízdní
pluku	pluk	k1gInSc2	pluk
Jana	Jan	k1gMnSc2	Jan
Jiskry	jiskra	k1gFnSc2	jiskra
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
Brandýsa	Brandýs	k1gInSc2	Brandýs
Pamětní	pamětní	k2eAgInSc4d1	pamětní
kříž	kříž	k1gInSc4	kříž
2	[number]	k4	2
<g/>
.	.	kIx.	.
jízdního	jízdní	k2eAgInSc2d1	jízdní
pluku	pluk	k1gInSc2	pluk
Ruských	ruský	k2eAgFnPc2d1	ruská
legií	legie	k1gFnPc2	legie
(	(	kIx(	(
<g/>
Nitranských	nitranský	k2eAgFnPc2d1	Nitranská
kozáků-Sibiřského	kozáků-Sibiřský	k2eAgInSc2d1	kozáků-Sibiřský
<g/>
)	)	kIx)	)
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojska	vojsko	k1gNnPc4	vojsko
na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
strojírenské	strojírenský	k2eAgFnSc2d1	strojírenská
samostatné	samostatný	k2eAgFnSc2d1	samostatná
roty	rota	k1gFnSc2	rota
dopravní	dopravní	k2eAgFnSc2d1	dopravní
vlakové	vlakový	k2eAgFnSc2d1	vlaková
dílny	dílna	k1gFnSc2	dílna
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojska	vojsko	k1gNnPc4	vojsko
na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
Pamětní	pamětní	k2eAgFnSc2d1	pamětní
medaile	medaile	k1gFnSc2	medaile
čs	čs	kA	čs
<g/>
.	.	kIx.	.
dobrovoleckého	dobrovolecký	k2eAgInSc2d1	dobrovolecký
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
1918	[number]	k4	1918
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
Štefánikův	Štefánikův	k2eAgInSc1d1	Štefánikův
pamětní	pamětní	k2eAgInSc1d1	pamětní
odznak	odznak	k1gInSc1	odznak
Československý	československý	k2eAgInSc1d1	československý
válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
1939	[number]	k4	1939
-	-	kIx~	-
tři	tři	k4xCgMnPc1	tři
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
medaile	medaile	k1gFnSc1	medaile
"	"	kIx"	"
<g/>
Za	za	k7c4	za
chrabrost	chrabrost	k1gFnSc4	chrabrost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
medaile	medaile	k1gFnSc1	medaile
"	"	kIx"	"
<g/>
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
"	"	kIx"	"
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojenská	vojenský	k2eAgFnSc1d1	vojenská
pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
se	s	k7c7	s
štítkem	štítek	k1gInSc7	štítek
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Dukelská	dukelský	k2eAgFnSc1d1	Dukelská
pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
pamětní	pamětní	k2eAgFnPc4d1	pamětní
medaile	medaile	k1gFnPc4	medaile
Čestný	čestný	k2eAgInSc1d1	čestný
odznak	odznak	k1gInSc1	odznak
polního	polní	k2eAgMnSc2d1	polní
pilota	pilot	k1gMnSc2	pilot
letce	letec	k1gMnSc2	letec
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
Čestný	čestný	k2eAgInSc1d1	čestný
odznak	odznak	k1gInSc1	odznak
Československého	československý	k2eAgMnSc2d1	československý
vojenského	vojenský	k2eAgMnSc2d1	vojenský
pilota	pilot	k1gMnSc2	pilot
Odznak	odznak	k1gInSc4	odznak
čs	čs	kA	čs
<g/>
.	.	kIx.	.
partyzána	partyzán	k1gMnSc2	partyzán
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
II	II	kA	II
<g/>
.	.	kIx.	.
národního	národní	k2eAgInSc2d1	národní
odboje	odboj	k1gInSc2	odboj
Čestná	čestný	k2eAgFnSc1d1	čestná
medaile	medaile	k1gFnSc1	medaile
zasloužilého	zasloužilý	k2eAgMnSc2d1	zasloužilý
bojovníka	bojovník	k1gMnSc2	bojovník
proti	proti	k7c3	proti
fašizmu	fašizmus	k1gInSc3	fašizmus
I.	I.	kA	I.
stupně	stupeň	k1gInPc1	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgFnPc1d1	ruská
dekorace	dekorace	k1gFnPc1	dekorace
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Kříž	Kříž	k1gMnSc1	Kříž
Svatého	svatý	k2eAgMnSc2d1	svatý
Jiřího	Jiří	k1gMnSc2	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
Kříž	Kříž	k1gMnSc1	Kříž
Svatého	svatý	k2eAgMnSc2d1	svatý
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
za	za	k7c4	za
statečnost	statečnost	k1gFnSc4	statečnost
Sovětské	sovětský	k2eAgInPc4d1	sovětský
řády	řád	k1gInPc4	řád
a	a	k8xC	a
dekorace	dekorace	k1gFnPc4	dekorace
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hvězda	hvězda	k1gFnSc1	hvězda
Hrdiny	Hrdina	k1gMnSc2	Hrdina
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Leninův	Leninův	k2eAgInSc1d1	Leninův
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Suvorovův	Suvorovův	k2eAgInSc1d1	Suvorovův
řád	řád	k1gInSc1	řád
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g />
.	.	kIx.	.
</s>
<s>
Suvorovův	Suvorovův	k2eAgInSc1d1	Suvorovův
řád	řád	k1gInSc1	řád
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
Medaile	medaile	k1gFnSc2	medaile
"	"	kIx"	"
<g/>
Za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
Německem	Německo	k1gNnSc7	Německo
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
vlastenecké	vlastenecký	k2eAgFnSc6d1	vlastenecká
válce	válka	k1gFnSc6	válka
Medaile	medaile	k1gFnSc2	medaile
"	"	kIx"	"
<g/>
Za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
Prahy	Praha	k1gFnSc2	Praha
<g/>
"	"	kIx"	"
Polské	polský	k2eAgInPc1d1	polský
řády	řád	k1gInPc1	řád
a	a	k8xC	a
dekorace	dekorace	k1gFnPc1	dekorace
<g/>
:	:	kIx,	:
Vojenský	vojenský	k2eAgInSc1d1	vojenský
řád	řád	k1gInSc1	řád
"	"	kIx"	"
<g/>
Virtuti	Virtut	k2eAgMnPc1d1	Virtut
Militari	Militar	k1gMnPc1	Militar
<g/>
"	"	kIx"	"
-	-	kIx~	-
velkokříž	velkokříž	k1gInSc1	velkokříž
na	na	k7c4	na
velkostuze	velkostuze	k1gFnPc4	velkostuze
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
"	"	kIx"	"
<g/>
Polonia	polonium	k1gNnSc2	polonium
Restituta	Restitut	k1gMnSc2	Restitut
<g/>
"	"	kIx"	"
-	-	kIx~	-
velkokříž	velkokříž	k1gInSc1	velkokříž
na	na	k7c4	na
velkostuze	velkostuze	k1gFnPc4	velkostuze
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
Grunwaldského	Grunwaldský	k2eAgInSc2d1	Grunwaldský
kříže	kříž	k1gInSc2	kříž
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgInSc1d1	polský
válečný	válečný	k2eAgInSc1d1	válečný
kříž	kříž	k1gInSc1	kříž
-	-	kIx~	-
1944	[number]	k4	1944
Grunwaldský	Grunwaldský	k2eAgInSc1d1	Grunwaldský
odznak	odznak	k1gInSc1	odznak
Medaile	medaile	k1gFnSc2	medaile
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
Medaile	medaile	k1gFnSc2	medaile
za	za	k7c4	za
Varšavu	Varšava	k1gFnSc4	Varšava
1939	[number]	k4	1939
-	-	kIx~	-
1945	[number]	k4	1945
Medaile	medaile	k1gFnSc1	medaile
za	za	k7c4	za
Odru	Odra	k1gFnSc4	Odra
a	a	k8xC	a
Nisu	Nisa	k1gFnSc4	Nisa
a	a	k8xC	a
Balt	Balt	k1gMnSc1	Balt
Jugoslávské	jugoslávský	k2eAgInPc4d1	jugoslávský
řády	řád	k1gInPc4	řád
a	a	k8xC	a
dekorace	dekorace	k1gFnPc4	dekorace
<g/>
:	:	kIx,	:
Řád	řád	k1gInSc1	řád
národního	národní	k2eAgMnSc2d1	národní
hrdiny	hrdina	k1gMnSc2	hrdina
Jugoslavie	Jugoslavie	k1gFnSc2	Jugoslavie
(	(	kIx(	(
<g/>
Orden	Orden	k2eAgInSc1d1	Orden
narodnog	narodnog	k1gInSc1	narodnog
heroja	heroja	k?	heroja
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
zásluhy	zásluha	k1gFnSc2	zásluha
za	za	k7c4	za
národ	národ	k1gInSc4	národ
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
Orden	Orden	k2eAgInSc1d1	Orden
zasluge	zasluge	k1gInSc1	zasluge
za	za	k7c4	za
narod	narod	k1gInSc4	narod
<g/>
)	)	kIx)	)
Čestný	čestný	k2eAgInSc4d1	čestný
odznak	odznak	k1gInSc4	odznak
Jugoslávské	jugoslávský	k2eAgFnSc2d1	jugoslávská
armády	armáda	k1gFnSc2	armáda
Maďarské	maďarský	k2eAgInPc4d1	maďarský
řády	řád	k1gInPc4	řád
a	a	k8xC	a
dekorace	dekorace	k1gFnPc4	dekorace
<g/>
:	:	kIx,	:
Řád	řád	k1gInSc1	řád
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
-	-	kIx~	-
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
praporu	prapor	k1gInSc2	prapor
I.	I.	kA	I.
třídy	třída	k1gFnPc1	třída
Kříž	Kříž	k1gMnSc1	Kříž
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
1946	[number]	k4	1946
Rumunský	rumunský	k2eAgInSc1d1	rumunský
řád	řád	k1gInSc1	řád
a	a	k8xC	a
dekorace	dekorace	k1gFnPc1	dekorace
Řád	řád	k1gInSc1	řád
Obrany	obrana	k1gFnSc2	obrana
vlasti	vlast	k1gFnSc2	vlast
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Příležitostný	příležitostný	k2eAgInSc1d1	příležitostný
odznak	odznak	k1gInSc1	odznak
veteránů	veterán	k1gMnPc2	veterán
protifašistického	protifašistický	k2eAgInSc2d1	protifašistický
odboje	odboj	k1gInSc2	odboj
Řád	řád	k1gInSc1	řád
a	a	k8xC	a
dekorace	dekorace	k1gFnSc1	dekorace
Francie	Francie	k1gFnSc1	Francie
<g/>
:	:	kIx,	:
Řád	řád	k1gInSc1	řád
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
-	-	kIx~	-
velkodůstojník	velkodůstojník	k1gMnSc1	velkodůstojník
(	(	kIx(	(
<g/>
Grand	grand	k1gMnSc1	grand
Officier	Officier	k1gMnSc1	Officier
de	de	k?	de
la	la	k1gNnPc2	la
Legion	legion	k1gInSc1	legion
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Honneur	Honneur	k1gMnSc1	Honneur
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Válečný	válečný	k2eAgInSc4d1	válečný
kříž	kříž	k1gInSc4	kříž
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
Řád	řád	k1gInSc1	řád
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
:	:	kIx,	:
Řád	řád	k1gInSc1	řád
lázně	lázeň	k1gFnSc2	lázeň
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
-	-	kIx~	-
velitelský	velitelský	k2eAgMnSc1d1	velitelský
-	-	kIx~	-
(	(	kIx(	(
<g/>
Knight	Knight	k2eAgInSc1d1	Knight
Commander	Commander	k1gInSc1	Commander
of	of	k?	of
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
the	the	k?	the
Bath	Bath	k1gInSc1	Bath
<g/>
)	)	kIx)	)
Řád	řád	k1gInSc1	řád
USA	USA	kA	USA
<g/>
:	:	kIx,	:
Řád	řád	k1gInSc1	řád
Legie	legie	k1gFnSc2	legie
cti	čest	k1gFnSc2	čest
-	-	kIx~	-
velitelský	velitelský	k2eAgInSc1d1	velitelský
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
Legion	legion	k1gInSc1	legion
of	of	k?	of
Merit	meritum	k1gNnPc2	meritum
<g />
.	.	kIx.	.
</s>
<s>
degree	degree	k1gFnSc1	degree
of	of	k?	of
Commander	Commander	k1gInSc1	Commander
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Kloboučník	kloboučník	k1gMnSc1	kloboučník
<g/>
,	,	kIx,	,
pplk.	pplk.	kA	pplk.
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
dělostřeleckého	dělostřelecký	k2eAgInSc2d1	dělostřelecký
pluku	pluk	k1gInSc2	pluk
12	[number]	k4	12
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Škpt.	Škpt.	k1gMnSc1	Škpt.
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
36	[number]	k4	36
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přidělen	přidělit	k5eAaPmNgInS	přidělit
na	na	k7c6	na
zkušené	zkušená	k1gFnSc6	zkušená
u	u	k7c2	u
dělostřeleckého	dělostřelecký	k2eAgInSc2d1	dělostřelecký
pluku	pluk	k1gInSc2	pluk
12	[number]	k4	12
od	od	k7c2	od
21	[number]	k4	21
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
do	do	k7c2	do
31	[number]	k4	31
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
k	k	k7c3	k
velitelství	velitelství	k1gNnSc3	velitelství
oddílu	oddíl	k1gInSc2	oddíl
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Prokázal	prokázat	k5eAaPmAgMnS	prokázat
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
snahu	snaha	k1gFnSc4	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
zkušené	zkušená	k1gFnPc4	zkušená
u	u	k7c2	u
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
využil	využít	k5eAaPmAgMnS	využít
plně	plně	k6eAd1	plně
a	a	k8xC	a
správně	správně	k6eAd1	správně
k	k	k7c3	k
důkladnému	důkladný	k2eAgNnSc3d1	důkladné
seznámení	seznámení	k1gNnSc3	seznámení
se	se	k3xPyFc4	se
s	s	k7c7	s
činností	činnost	k1gFnSc7	činnost
této	tento	k3xDgFnSc2	tento
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
jeho	jeho	k3xOp3gFnSc4	jeho
pilnost	pilnost	k1gFnSc4	pilnost
a	a	k8xC	a
vytrvalost	vytrvalost	k1gFnSc4	vytrvalost
byla	být	k5eAaImAgFnS	být
vzorná	vzorný	k2eAgFnSc1d1	vzorná
<g/>
.	.	kIx.	.
</s>
<s>
Škpt.	Škpt.	k?	Škpt.
Svoboda	Svoboda	k1gMnSc1	Svoboda
je	být	k5eAaImIp3nS	být
výborný	výborný	k2eAgMnSc1d1	výborný
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
čestné	čestný	k2eAgFnPc1d1	čestná
povahy	povaha	k1gFnPc1	povaha
<g/>
,	,	kIx,	,
iniciativní	iniciativní	k2eAgInSc1d1	iniciativní
<g/>
,	,	kIx,	,
sebevědomý	sebevědomý	k2eAgInSc1d1	sebevědomý
<g/>
,	,	kIx,	,
přesný	přesný	k2eAgInSc1d1	přesný
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
,	,	kIx,	,
bezvadného	bezvadný	k2eAgNnSc2d1	bezvadné
a	a	k8xC	a
vzorného	vzorný	k2eAgNnSc2d1	vzorné
chování	chování	k1gNnSc2	chování
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
i	i	k9	i
mimo	mimo	k7c4	mimo
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
výsledek	výsledek	k1gInSc1	výsledek
služební	služební	k2eAgFnSc2d1	služební
činnosti	činnost	k1gFnSc2	činnost
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Velitel	velitel	k1gMnSc1	velitel
leteckého	letecký	k2eAgInSc2d1	letecký
pluku	pluk	k1gInSc2	pluk
3	[number]	k4	3
v	v	k7c6	v
Piešťanech	Piešťany	k1gInPc6	Piešťany
(	(	kIx(	(
<g/>
podpis	podpis	k1gInSc1	podpis
nečitelný	čitelný	k2eNgInSc1d1	nečitelný
<g/>
;	;	kIx,	;
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
během	během	k7c2	během
zkušené	zkušená	k1gFnSc2	zkušená
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
-	-	kIx~	-
15	[number]	k4	15
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
seznámen	seznámit	k5eAaPmNgInS	seznámit
se	s	k7c7	s
zařízeními	zařízení	k1gNnPc7	zařízení
letky	letka	k1gFnSc2	letka
<g/>
,	,	kIx,	,
chodem	chod	k1gInSc7	chod
služby	služba	k1gFnSc2	služba
u	u	k7c2	u
letky	letka	k1gFnSc2	letka
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pak	pak	k6eAd1	pak
s	s	k7c7	s
taktickou	taktický	k2eAgFnSc7d1	taktická
činností	činnost	k1gFnSc7	činnost
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Vykonal	vykonat	k5eAaPmAgMnS	vykonat
celkem	celek	k1gInSc7	celek
10	[number]	k4	10
letů	let	k1gInPc2	let
s	s	k7c7	s
krátkými	krátký	k2eAgFnPc7d1	krátká
úlohami	úloha	k1gFnPc7	úloha
pěch	pěch	k1gInSc1	pěch
<g/>
.	.	kIx.	.
a	a	k8xC	a
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
dobrým	dobrý	k2eAgInSc7d1	dobrý
výsledkem	výsledek	k1gInSc7	výsledek
a	a	k8xC	a
během	během	k7c2	během
těchto	tento	k3xDgInPc2	tento
letů	let	k1gInPc2	let
natolik	natolik	k6eAd1	natolik
seznal	seznat	k5eAaPmAgMnS	seznat
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
zbraně	zbraň	k1gFnSc2	zbraň
(	(	kIx(	(
<g/>
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
bude	být	k5eAaImBp3nS	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
informátorem	informátor	k1gMnSc7	informátor
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc1	jaký
úlohy	úloha	k1gFnPc1	úloha
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
od	od	k7c2	od
letce	letec	k1gMnSc2	letec
požadovati	požadovat	k5eAaImF	požadovat
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
službu	služba	k1gFnSc4	služba
leteckou	letecký	k2eAgFnSc4d1	letecká
-	-	kIx~	-
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
-	-	kIx~	-
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
rád	rád	k6eAd1	rád
iniciativně	iniciativně	k6eAd1	iniciativně
-	-	kIx~	-
v	v	k7c6	v
letounu	letoun	k1gInSc6	letoun
pracoval	pracovat	k5eAaImAgMnS	pracovat
klidně	klidně	k6eAd1	klidně
<g/>
,	,	kIx,	,
rozvážně	rozvážně	k6eAd1	rozvážně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
docílil	docílit	k5eAaPmAgInS	docílit
velmi	velmi	k6eAd1	velmi
dobrých	dobrý	k2eAgInPc2d1	dobrý
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
i	i	k9	i
mimo	mimo	k7c4	mimo
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
chování	chování	k1gNnSc1	chování
bylo	být	k5eAaImAgNnS	být
bezvadné	bezvadný	k2eAgNnSc1d1	bezvadné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
B.	B.	kA	B.
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
3	[number]	k4	3
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
J.	J.	kA	J.
Ž.	Ž.	kA	Ž.
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
posudek	posudek	k1gInSc1	posudek
<g/>
,	,	kIx,	,
kvalifikační	kvalifikační	k2eAgFnSc1d1	kvalifikační
listina	listina	k1gFnSc1	listina
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
vychovatel	vychovatel	k1gMnSc1	vychovatel
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
si	se	k3xPyFc3	se
získává	získávat	k5eAaImIp3nS	získávat
oddanost	oddanost	k1gFnSc4	oddanost
podřízených	podřízený	k1gMnPc2	podřízený
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
mravní	mravní	k2eAgNnSc1d1	mravní
a	a	k8xC	a
výcvikové	výcvikový	k2eAgNnSc1d1	výcvikové
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobrých	dobrý	k2eAgInPc2d1	dobrý
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
pořízených	pořízený	k2eAgMnPc2d1	pořízený
důstojníků	důstojník	k1gMnPc2	důstojník
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
způsobilý	způsobilý	k2eAgMnSc1d1	způsobilý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Josef	Josef	k1gMnSc1	Josef
Zmek	zmek	k1gMnSc1	zmek
<g/>
,	,	kIx,	,
brig	briga	k1gFnPc2	briga
<g/>
.	.	kIx.	.
gen.	gen.	kA	gen.
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
posudek	posudek	k1gInSc1	posudek
<g/>
,	,	kIx,	,
kvalifikační	kvalifikační	k2eAgFnSc1d1	kvalifikační
listina	listina	k1gFnSc1	listina
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Výborný	výborný	k2eAgMnSc1d1	výborný
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
značných	značný	k2eAgFnPc2d1	značná
vojenských	vojenský	k2eAgFnPc2d1	vojenská
vědomostí	vědomost	k1gFnPc2	vědomost
<g/>
,	,	kIx,	,
pracovitý	pracovitý	k2eAgMnSc1d1	pracovitý
spolehlivý	spolehlivý	k2eAgMnSc1d1	spolehlivý
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgInSc1d1	společenský
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
výtečná	výtečný	k2eAgFnSc1d1	výtečná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
J.	J.	kA	J.
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
<g/>
,	,	kIx,	,
brig	briga	k1gFnPc2	briga
<g/>
.	.	kIx.	.
gen	gen	k1gInSc1	gen
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
válečná	válečný	k2eAgFnSc1d1	válečná
vložka	vložka	k1gFnSc1	vložka
ke	k	k7c3	k
kvalifikační	kvalifikační	k2eAgFnSc3d1	kvalifikační
listině	listina	k1gFnSc3	listina
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zkušený	zkušený	k2eAgMnSc1d1	zkušený
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svoji	svůj	k3xOyFgFnSc4	svůj
brigádu	brigáda	k1gFnSc4	brigáda
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
vede	vést	k5eAaImIp3nS	vést
naprosto	naprosto	k6eAd1	naprosto
spolehlivě	spolehlivě	k6eAd1	spolehlivě
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
široký	široký	k2eAgInSc4d1	široký
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
i	i	k8xC	i
vojenský	vojenský	k2eAgInSc4d1	vojenský
rozhled	rozhled	k1gInSc4	rozhled
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
chrabrý	chrabrý	k2eAgInSc1d1	chrabrý
<g/>
,	,	kIx,	,
klidný	klidný	k2eAgInSc1d1	klidný
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc1d1	dobré
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
,	,	kIx,	,
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc4d1	dobrý
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc4d1	dobrý
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
čestnost	čestnost	k1gFnSc4	čestnost
všeobecně	všeobecně	k6eAd1	všeobecně
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
<g/>
.	.	kIx.	.
</s>
<s>
Výtečný	výtečný	k2eAgMnSc1d1	výtečný
důstojník	důstojník	k1gMnSc1	důstojník
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Václav	Václav	k1gMnSc1	Václav
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
literární	literární	k2eAgMnSc1d1	literární
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
)	)	kIx)	)
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Svobodu	svoboda	k1gFnSc4	svoboda
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
Pamětech	paměť	k1gFnPc6	paměť
přísně	přísně	k6eAd1	přísně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
celého	celý	k2eAgInSc2d1	celý
národa	národ	k1gInSc2	národ
jsem	být	k5eAaImIp1nS	být
nikdy	nikdy	k6eAd1	nikdy
ani	ani	k8xC	ani
minimum	minimum	k1gNnSc4	minimum
národní	národní	k2eAgFnSc2d1	národní
naděje	naděje	k1gFnSc2	naděje
nespojil	spojit	k5eNaPmAgMnS	spojit
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
generála	generál	k1gMnSc2	generál
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
,	,	kIx,	,
věděl	vědět	k5eAaImAgMnS	vědět
jsem	být	k5eAaImIp1nS	být
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
že	že	k8xS	že
postrádá	postrádat	k5eAaImIp3nS	postrádat
mravní	mravní	k2eAgFnPc4d1	mravní
přímosti	přímost	k1gFnPc4	přímost
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
myšlenkový	myšlenkový	k2eAgInSc4d1	myšlenkový
obzor	obzor	k1gInSc4	obzor
a	a	k8xC	a
potenciál	potenciál	k1gInSc4	potenciál
podřízeného	podřízený	k2eAgMnSc2d1	podřízený
velitele	velitel	k1gMnSc2	velitel
střední	střední	k2eAgFnSc2d1	střední
vojenské	vojenský	k2eAgFnSc2d1	vojenská
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
;	;	kIx,	;
národní	národní	k2eAgFnSc1d1	národní
neinformovanost	neinformovanost	k1gFnSc1	neinformovanost
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
z	z	k7c2	z
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
proměnila	proměnit	k5eAaPmAgFnS	proměnit
ho	on	k3xPp3gMnSc4	on
v	v	k7c4	v
symbol	symbol	k1gInSc4	symbol
národní	národní	k2eAgFnSc2d1	národní
brannosti	brannost	k1gFnSc2	brannost
<g/>
,	,	kIx,	,
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
pouze	pouze	k6eAd1	pouze
sám	sám	k3xTgMnSc1	sám
uvěřil	uvěřit	k5eAaPmAgMnS	uvěřit
kolektivnímu	kolektivní	k2eAgInSc3d1	kolektivní
omylu	omyl	k1gInSc3	omyl
<g/>
,	,	kIx,	,
vzal	vzít	k5eAaPmAgInS	vzít
za	za	k7c7	za
bernou	berný	k2eAgFnSc7d1	berná
<g />
.	.	kIx.	.
</s>
<s>
minci	mince	k1gFnSc4	mince
metály	metál	k1gInPc1	metál
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
kabátě	kabát	k1gInSc6	kabát
a	a	k8xC	a
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
ho	on	k3xPp3gMnSc4	on
postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
ověsili	ověsit	k5eAaPmAgMnP	ověsit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
sborníku	sborník	k1gInSc6	sborník
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Svobodou	Svoboda	k1gMnSc7	Svoboda
za	za	k7c2	za
války	válka	k1gFnSc2	válka
osobně	osobně	k6eAd1	osobně
setkali	setkat	k5eAaPmAgMnP	setkat
a	a	k8xC	a
zažili	zažít	k5eAaPmAgMnP	zažít
ho	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
velitele	velitel	k1gMnSc4	velitel
a	a	k8xC	a
nadřízeného	nadřízený	k1gMnSc4	nadřízený
je	být	k5eAaImIp3nS	být
L.	L.	kA	L.
Svoboda	Svoboda	k1gMnSc1	Svoboda
hodnocen	hodnocen	k2eAgMnSc1d1	hodnocen
jako	jako	k8xC	jako
výjimečný	výjimečný	k2eAgMnSc1d1	výjimečný
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
s	s	k7c7	s
upřímnou	upřímný	k2eAgFnSc7d1	upřímná
starostí	starost	k1gFnSc7	starost
o	o	k7c4	o
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
linii	linie	k1gFnSc6	linie
<g/>
.	.	kIx.	.
</s>
<s>
Generálporučík	generálporučík	k1gMnSc1	generálporučík
v.	v.	k?	v.
v.	v.	k?	v.
MUDr.	MUDr.	kA	MUDr.
František	František	k1gMnSc1	František
Engel	Engel	k1gMnSc1	Engel
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Podplukovník	podplukovník	k1gMnSc1	podplukovník
Svoboda	Svoboda	k1gMnSc1	Svoboda
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
prapor	prapor	k1gInSc1	prapor
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
dobře	dobře	k6eAd1	dobře
připraven	připravit	k5eAaPmNgMnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
-	-	kIx~	-
přes	přes	k7c4	přes
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
různorodost	různorodost	k1gFnSc4	různorodost
vojáků	voják	k1gMnPc2	voják
-	-	kIx~	-
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
důkladném	důkladný	k2eAgInSc6d1	důkladný
výcviku	výcvik	k1gInSc6	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Cvičilo	cvičit	k5eAaImAgNnS	cvičit
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
válečné	válečný	k2eAgFnSc6d1	válečná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kladla	klást	k5eAaImAgFnS	klást
na	na	k7c4	na
vojáky	voják	k1gMnPc4	voják
velké	velký	k2eAgInPc1d1	velký
nároky	nárok	k1gInPc1	nárok
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgInPc4d1	Dlouhé
denní	denní	k2eAgInPc4d1	denní
a	a	k8xC	a
noční	noční	k2eAgInPc4d1	noční
pochody	pochod	k1gInPc4	pochod
za	za	k7c2	za
každého	každý	k3xTgNnSc2	každý
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
s	s	k7c7	s
plným	plný	k2eAgNnSc7d1	plné
zatížením	zatížení	k1gNnSc7	zatížení
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
...	...	k?	...
Když	když	k8xS	když
únava	únava	k1gFnSc1	únava
byla	být	k5eAaImAgFnS	být
už	už	k6eAd1	už
nesnesitelná	snesitelný	k2eNgFnSc1d1	nesnesitelná
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
velitel	velitel	k1gMnSc1	velitel
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vojáky	voják	k1gMnPc4	voják
povzbudil	povzbudit	k5eAaPmAgInS	povzbudit
svým	svůj	k3xOyFgInSc7	svůj
osobním	osobní	k2eAgInSc7d1	osobní
příkladem	příklad	k1gInSc7	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
udržet	udržet	k5eAaPmF	udržet
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
nechtěli	chtít	k5eNaImAgMnP	chtít
se	se	k3xPyFc4	se
nechat	nechat	k5eAaPmF	nechat
starým	starý	k1gMnSc7	starý
pánem	pán	k1gMnSc7	pán
zahanbit	zahanbit	k5eAaPmF	zahanbit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
nikdy	nikdy	k6eAd1	nikdy
nebyla	být	k5eNaImAgFnS	být
únava	únava	k1gFnSc1	únava
znát	znát	k5eAaImF	znát
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
ji	on	k3xPp3gFnSc4	on
uměl	umět	k5eAaImAgMnS	umět
ignorovat	ignorovat	k5eAaImF	ignorovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
začátku	začátek	k1gInSc2	začátek
byl	být	k5eAaImAgMnS	být
podplukovník	podplukovník	k1gMnSc1	podplukovník
Svoboda	Svoboda	k1gMnSc1	Svoboda
nesmírně	smírně	k6eNd1	smírně
starostlivý	starostlivý	k2eAgMnSc1d1	starostlivý
velitel	velitel	k1gMnSc1	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Výtečně	výtečně	k6eAd1	výtečně
znal	znát	k5eAaImAgInS	znát
psychologii	psychologie	k1gFnSc4	psychologie
vojáka	voják	k1gMnSc2	voják
<g/>
,	,	kIx,	,
věděl	vědět	k5eAaImAgMnS	vědět
co	co	k3yQnSc4	co
mu	on	k3xPp3gMnSc3	on
může	moct	k5eAaImIp3nS	moct
-	-	kIx~	-
kromě	kromě	k7c2	kromě
té	ten	k3xDgFnSc2	ten
těžké	těžký	k2eAgFnSc2d1	těžká
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
-	-	kIx~	-
život	život	k1gInSc4	život
ztěžovat	ztěžovat	k5eAaImF	ztěžovat
a	a	k8xC	a
ztrpčovat	ztrpčovat	k5eAaImF	ztrpčovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
jednal	jednat	k5eAaImAgMnS	jednat
<g/>
.	.	kIx.	.
...	...	k?	...
Náš	náš	k3xOp1gMnSc1	náš
velitel	velitel	k1gMnSc1	velitel
to	ten	k3xDgNnSc4	ten
všechno	všechen	k3xTgNnSc4	všechen
prodělával	prodělávat	k5eAaImAgMnS	prodělávat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Nehřál	hřát	k5eNaImAgInS	hřát
se	se	k3xPyFc4	se
v	v	k7c6	v
teple	teplo	k1gNnSc6	teplo
<g/>
,	,	kIx,	,
nevysvětloval	vysvětlovat	k5eNaImAgMnS	vysvětlovat
plamennými	plamenný	k2eAgNnPc7d1	plamenné
slovy	slovo	k1gNnPc7	slovo
od	od	k7c2	od
zeleného	zelený	k2eAgInSc2d1	zelený
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
že	že	k8xS	že
vydržet	vydržet	k5eAaPmF	vydržet
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
všechno	všechen	k3xTgNnSc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
ho	on	k3xPp3gNnSc4	on
pak	pak	k6eAd1	pak
následovali	následovat	k5eAaImAgMnP	následovat
<g/>
,	,	kIx,	,
vydávali	vydávat	k5eAaImAgMnP	vydávat
ze	z	k7c2	z
sebe	sebe	k3xPyFc4	sebe
skutečně	skutečně	k6eAd1	skutečně
vše	všechen	k3xTgNnSc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Velel	velet	k5eAaImAgMnS	velet
tenkrát	tenkrát	k6eAd1	tenkrát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
jen	jen	k9	jen
malinké	malinký	k2eAgFnSc3d1	malinká
jednotce	jednotka	k1gFnSc3	jednotka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
dnes	dnes	k6eAd1	dnes
snad	snad	k9	snad
nikdo	nikdo	k3yNnSc1	nikdo
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
představit	představit	k5eAaPmF	představit
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
s	s	k7c7	s
ní	on	k3xPp3gFnSc6	on
byly	být	k5eAaImAgFnP	být
obrovské	obrovský	k2eAgFnPc1d1	obrovská
starosti	starost	k1gFnPc1	starost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
plně	plně	k6eAd1	plně
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatím	zatím	k6eAd1	zatím
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
jediný	jediný	k2eAgInSc1d1	jediný
prapor	prapor	k1gInSc1	prapor
<g/>
,	,	kIx,	,
prostě	prostě	k9	prostě
jedináček	jedináček	k1gMnSc1	jedináček
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jako	jako	k9	jako
s	s	k7c7	s
jedináčkem	jedináček	k1gMnSc7	jedináček
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
piplal	piplat	k5eAaImAgMnS	piplat
<g/>
.	.	kIx.	.
</s>
<s>
Málokdo	málokdo	k3yInSc1	málokdo
jiný	jiný	k2eAgMnSc1d1	jiný
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Generálmajor	generálmajor	k1gMnSc1	generálmajor
v	v	k7c6	v
z.	z.	k?	z.
Alfréd	Alfréd	k1gMnSc1	Alfréd
Ressel	Ressel	k1gMnSc1	Ressel
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Voják	voják	k1gMnSc1	voják
pozná	poznat	k5eAaPmIp3nS	poznat
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jeho	jeho	k3xOp3gMnSc1	jeho
velitel	velitel	k1gMnSc1	velitel
má	mít	k5eAaImIp3nS	mít
srdce	srdce	k1gNnSc4	srdce
-	-	kIx~	-
jestli	jestli	k8xS	jestli
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
představuje	představovat	k5eAaImIp3nS	představovat
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
anebo	anebo	k8xC	anebo
je	být	k5eAaImIp3nS	být
degradován	degradovat	k5eAaBmNgInS	degradovat
na	na	k7c4	na
pouhý	pouhý	k2eAgInSc4d1	pouhý
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
neznamená	znamenat	k5eNaImIp3nS	znamenat
nic	nic	k6eAd1	nic
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
prostředek	prostředek	k1gInSc1	prostředek
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
určitých	určitý	k2eAgInPc2d1	určitý
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
...	...	k?	...
Generál	generál	k1gMnSc1	generál
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
lidi	člověk	k1gMnPc4	člověk
k	k	k7c3	k
nímž	jenž	k3xRgMnSc7	jenž
jsem	být	k5eAaImIp1nS	být
získal	získat	k5eAaPmAgMnS	získat
rychle	rychle	k6eAd1	rychle
plnou	plný	k2eAgFnSc4d1	plná
důvěru	důvěra	k1gFnSc4	důvěra
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
humanistický	humanistický	k2eAgInSc4d1	humanistický
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
vojáci	voják	k1gMnPc1	voják
ho	on	k3xPp3gNnSc4	on
proto	proto	k8xC	proto
měli	mít	k5eAaImAgMnP	mít
rádi	rád	k2eAgMnPc1d1	rád
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
přímost	přímost	k1gFnSc4	přímost
<g/>
,	,	kIx,	,
otevřenost	otevřenost	k1gFnSc4	otevřenost
a	a	k8xC	a
citlivost	citlivost	k1gFnSc4	citlivost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Svoboda	Svoboda	k1gMnSc1	Svoboda
často	často	k6eAd1	často
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
předních	přední	k2eAgFnPc2d1	přední
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ani	ani	k8xC	ani
nemusel	muset	k5eNaImAgMnS	muset
<g/>
.	.	kIx.	.
...	...	k?	...
Sám	sám	k3xTgMnSc1	sám
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
to	ten	k3xDgNnSc4	ten
vlastně	vlastně	k9	vlastně
dělal	dělat	k5eAaImAgMnS	dělat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
chodíval	chodívat	k5eAaImAgInS	chodívat
záměrně	záměrně	k6eAd1	záměrně
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zle	zle	k6eAd1	zle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
trpí	trpět	k5eAaImIp3nP	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
žádné	žádný	k3yNgNnSc4	žádný
reklamní	reklamní	k2eAgNnSc4d1	reklamní
hrdinství	hrdinství	k1gNnSc4	hrdinství
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
stále	stále	k6eAd1	stále
hledal	hledat	k5eAaImAgMnS	hledat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc7	svůj
přítomností	přítomnost	k1gFnSc7	přítomnost
může	moct	k5eAaImIp3nS	moct
člověku	člověk	k1gMnSc3	člověk
pomoci	pomoct	k5eAaPmF	pomoct
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
není	být	k5eNaImIp3nS	být
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
téměř	téměř	k6eAd1	téměř
nikdy	nikdy	k6eAd1	nikdy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
nevynechal	vynechat	k5eNaPmAgMnS	vynechat
návštěvu	návštěva	k1gFnSc4	návštěva
obvaziště	obvaziště	k1gNnSc2	obvaziště
a	a	k8xC	a
polních	polní	k2eAgFnPc2d1	polní
nemocnic	nemocnice	k1gFnPc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mluvil	mluvit	k5eAaImAgMnS	mluvit
s	s	k7c7	s
raněnými	raněný	k1gMnPc7	raněný
<g/>
.	.	kIx.	.
...	...	k?	...
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
mnohokrát	mnohokrát	k6eAd1	mnohokrát
jako	jako	k8xC	jako
velitel	velitel	k1gMnSc1	velitel
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
sboru	sbor	k1gInSc2	sbor
přítomen	přítomno	k1gNnPc2	přítomno
přípravám	příprava	k1gFnPc3	příprava
<g/>
,	,	kIx,	,
...	...	k?	...
'	'	kIx"	'
<g/>
A	a	k9	a
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
konkrétně	konkrétně	k6eAd1	konkrétně
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
,	,	kIx,	,
záloha	záloha	k1gFnSc1	záloha
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
'	'	kIx"	'
tázal	tázat	k5eAaImAgMnS	tázat
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
pravidelně	pravidelně	k6eAd1	pravidelně
Svoboda	Svoboda	k1gMnSc1	Svoboda
-	-	kIx~	-
jako	jako	k8xC	jako
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
hospodář	hospodář	k1gMnSc1	hospodář
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
jde	jít	k5eAaImIp3nS	jít
celý	celý	k2eAgInSc1d1	celý
souhrn	souhrn	k1gInSc1	souhrn
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
cena	cena	k1gFnSc1	cena
k	k	k7c3	k
součtu	součet	k1gInSc3	součet
<g/>
.	.	kIx.	.
</s>
<s>
Znal	znát	k5eAaImAgMnS	znát
své	svůj	k3xOyFgMnPc4	svůj
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
dovedl	dovést	k5eAaPmAgMnS	dovést
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
také	také	k6eAd1	také
zastat	zastat	k5eAaPmF	zastat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Czechoslovakia	Czechoslovakia	k1gFnSc1	Czechoslovakia
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Spřízněni	spříznit	k5eAaPmNgMnP	spříznit
volbou	volba	k1gFnSc7	volba
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Odkaz	odkaz	k1gInSc1	odkaz
Dukly	Dukla	k1gFnSc2	Dukla
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vašíček	Vašíček	k1gMnSc1	Vašíček
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Svoboda	Svoboda	k1gMnSc1	Svoboda
/	/	kIx~	/
<g/>
...	...	k?	...
<g/>
/	/	kIx~	/
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
svých	svůj	k3xOyFgFnPc2	svůj
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
[	[	kIx(	[
<g/>
Cestami	cesta	k1gFnPc7	cesta
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
zaujetí	zaujetí	k1gNnSc2	zaujetí
JLF	JLF	kA	JLF
[	[	kIx(	[
J.	J.	kA	J.
L.	L.	kA	L.
Fischerem	Fischer	k1gMnSc7	Fischer
]	]	kIx)	]
v	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
podává	podávat	k5eAaImIp3nS	podávat
na	na	k7c6	na
dobrých	dobrý	k2eAgInPc6d1	dobrý
dvaceti	dvacet	k4xCc6	dvacet
stránkách	stránka	k1gFnPc6	stránka
obsah	obsah	k1gInSc4	obsah
jeho	jeho	k3xOp3gFnSc1	jeho
'	'	kIx"	'
<g/>
Krize	krize	k1gFnSc1	krize
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
Prezident	prezident	k1gMnSc1	prezident
Dobrotivý	dobrotivý	k2eAgMnSc1d1	dobrotivý
samozřejmě	samozřejmě	k6eAd1	samozřejmě
o	o	k7c6	o
JLF	JLF	kA	JLF
nikdy	nikdy	k6eAd1	nikdy
ani	ani	k8xC	ani
snad	snad	k9	snad
neslyšel	slyšet	k5eNaImAgMnS	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc1	ten
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jsem	být	k5eAaImIp1nS	být
tehdy	tehdy	k6eAd1	tehdy
seznámil	seznámit	k5eAaPmAgMnS	seznámit
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
ghostwriterů	ghostwriter	k1gInPc2	ghostwriter
jeho	jeho	k3xOp3gFnPc2	jeho
pamětí	paměť	k1gFnPc2	paměť
<g/>
,	,	kIx,	,
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Janečka	Janeček	k1gMnSc2	Janeček
<g/>
,	,	kIx,	,
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
JLF	JLF	kA	JLF
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
šířit	šířit	k5eAaImF	šířit
jeho	jeho	k3xOp3gFnPc4	jeho
myšlenky	myšlenka	k1gFnPc4	myšlenka
všemi	všecek	k3xTgInPc7	všecek
tehdy	tehdy	k6eAd1	tehdy
možnými	možný	k2eAgInPc7d1	možný
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
i	i	k8xC	i
tímto	tento	k3xDgNnSc7	tento
<g/>
.	.	kIx.	.
</s>
<s>
Nedivme	divit	k5eNaImRp1nP	divit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
paměti	paměť	k1gFnSc2	paměť
nebohého	nebohý	k2eAgMnSc2d1	nebohý
prezidenta	prezident	k1gMnSc2	prezident
byly	být	k5eAaImAgFnP	být
záhy	záhy	k6eAd1	záhy
staženy	stáhnout	k5eAaPmNgInP	stáhnout
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
