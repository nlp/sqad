<s>
Zvonek	zvonek	k1gInSc1	zvonek
jesenický	jesenický	k2eAgInSc1d1	jesenický
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
zvláště	zvláště	k6eAd1	zvláště
chráněných	chráněný	k2eAgFnPc2d1	chráněná
rostlin	rostlina	k1gFnPc2	rostlina
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
uvedenému	uvedený	k2eAgInSc3d1	uvedený
ve	v	k7c6	v
vyhlášce	vyhláška	k1gFnSc6	vyhláška
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
provádějí	provádět	k5eAaImIp3nP	provádět
některá	některý	k3yIgNnPc1	některý
ustanovení	ustanovení	k1gNnPc1	ustanovení
zákona	zákon	k1gInSc2	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
jej	on	k3xPp3gMnSc4	on
trhat	trhat	k5eAaImF	trhat
<g/>
,	,	kIx,	,
vykopávat	vykopávat	k5eAaImF	vykopávat
<g/>
,	,	kIx,	,
poškozovat	poškozovat	k5eAaImF	poškozovat
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
rušit	rušit	k5eAaImF	rušit
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
