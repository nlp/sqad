<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
oko	oko	k1gNnSc1	oko
vnímá	vnímat	k5eAaImIp3nS	vnímat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
daného	daný	k2eAgInSc2d1	daný
směru	směr	k1gInSc2	směr
přichází	přicházet	k5eAaImIp3nS	přicházet
světlo	světlo	k1gNnSc4	světlo
všech	všecek	k3xTgFnPc2	všecek
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Jejím	její	k3xOp3gInSc7	její
nejtmavším	tmavý	k2eAgInSc7d3	nejtmavší
odstínem	odstín	k1gInSc7	odstín
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
nedopadá	dopadat	k5eNaImIp3nS	dopadat
žádné	žádný	k3yNgNnSc4	žádný
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dojmu	dojmout	k5eAaPmIp1nS	dojmout
bílé	bílý	k2eAgFnPc4d1	bílá
barvy	barva	k1gFnPc4	barva
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
také	také	k9	také
smícháním	smíchání	k1gNnSc7	smíchání
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
aditivním	aditivní	k2eAgNnSc7d1	aditivní
mícháním	míchání	k1gNnSc7	míchání
<g/>
)	)	kIx)	)
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
spektra	spektrum	k1gNnSc2	spektrum
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
právě	právě	k9	právě
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
barvy	barva	k1gFnPc4	barva
je	být	k5eAaImIp3nS	být
lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
citlivé	citlivý	k2eAgNnSc1d1	citlivé
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
Isaaca	Isaacus	k1gMnSc2	Isaacus
Newtona	Newton	k1gMnSc2	Newton
popisoval	popisovat	k5eAaImAgInS	popisovat
převažující	převažující	k2eAgInSc1d1	převažující
vědecký	vědecký	k2eAgInSc1d1	vědecký
názor	názor	k1gInSc1	názor
bílou	bílý	k2eAgFnSc4d1	bílá
jako	jako	k8xS	jako
základní	základní	k2eAgFnSc4d1	základní
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
ostatní	ostatní	k2eAgFnSc2d1	ostatní
barvy	barva	k1gFnSc2	barva
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
bílému	bílý	k2eAgNnSc3d1	bílé
světlu	světlo	k1gNnSc3	světlo
přimíchá	přimíchat	k5eAaPmIp3nS	přimíchat
cosi	cosi	k3yInSc4	cosi
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ho	on	k3xPp3gMnSc4	on
zbarví	zbarvit	k5eAaPmIp3nS	zbarvit
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pokus	pokus	k1gInSc4	pokus
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
světelným	světelný	k2eAgInSc7d1	světelný
hranolem	hranol	k1gInSc7	hranol
rozložil	rozložit	k5eAaPmAgInS	rozložit
bílé	bílý	k2eAgNnSc4d1	bílé
světlo	světlo	k1gNnSc4	světlo
v	v	k7c4	v
barevné	barevný	k2eAgNnSc4d1	barevné
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dalším	další	k2eAgInSc7d1	další
hranolem	hranol	k1gInSc7	hranol
spojil	spojit	k5eAaPmAgInS	spojit
opět	opět	k6eAd1	opět
do	do	k7c2	do
bílého	bílý	k2eAgNnSc2d1	bílé
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
naopak	naopak	k6eAd1	naopak
barevné	barevný	k2eAgNnSc1d1	barevné
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgNnSc1d1	základní
a	a	k8xC	a
bílé	bílý	k2eAgNnSc1d1	bílé
světlo	světlo	k1gNnSc1	světlo
vzniká	vznikat	k5eAaImIp3nS	vznikat
smíšením	smíšení	k1gNnSc7	smíšení
světla	světlo	k1gNnSc2	světlo
všech	všecek	k3xTgFnPc2	všecek
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
za	za	k7c4	za
bílé	bílý	k2eAgNnSc4d1	bílé
světlo	světlo	k1gNnSc4	světlo
dá	dát	k5eAaPmIp3nS	dát
považovat	považovat	k5eAaImF	považovat
jak	jak	k6eAd1	jak
denní	denní	k2eAgNnSc1d1	denní
(	(	kIx(	(
<g/>
sluneční	sluneční	k2eAgNnSc1d1	sluneční
<g/>
)	)	kIx)	)
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
umělé	umělý	k2eAgNnSc1d1	umělé
osvětlení	osvětlení	k1gNnSc1	osvětlení
pomocí	pomocí	k7c2	pomocí
žárovek	žárovka	k1gFnPc2	žárovka
<g/>
,	,	kIx,	,
zářivek	zářivka	k1gFnPc2	zářivka
apod.	apod.	kA	apod.
Je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
světla	světlo	k1gNnSc2	světlo
nejsou	být	k5eNaImIp3nP	být
"	"	kIx"	"
<g/>
stejně	stejně	k6eAd1	stejně
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
světlo	světlo	k1gNnSc4	světlo
žárovky	žárovka	k1gFnSc2	žárovka
má	mít	k5eAaImIp3nS	mít
proti	proti	k7c3	proti
zářivce	zářivka	k1gFnSc3	zářivka
výrazně	výrazně	k6eAd1	výrazně
žlutější	žlutý	k2eAgInSc1d2	žlutější
odstín	odstín	k1gInSc1	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
odstíny	odstín	k1gInPc1	odstín
bílého	bílý	k2eAgNnSc2d1	bílé
světla	světlo	k1gNnSc2	světlo
jsou	být	k5eAaImIp3nP	být
popisovány	popisován	k2eAgFnPc1d1	popisována
tzv.	tzv.	kA	tzv.
barevnou	barevný	k2eAgFnSc7d1	barevná
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
parametr	parametr	k1gInSc1	parametr
udává	udávat	k5eAaImIp3nS	udávat
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
záření	záření	k1gNnSc4	záření
absolutně	absolutně	k6eAd1	absolutně
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
o	o	k7c6	o
příslušném	příslušný	k2eAgInSc6d1	příslušný
odstínu	odstín	k1gInSc6	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
běžná	běžný	k2eAgFnSc1d1	běžná
žárovka	žárovka	k1gFnSc1	žárovka
vydává	vydávat	k5eAaPmIp3nS	vydávat
světlo	světlo	k1gNnSc4	světlo
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
absolutně	absolutně	k6eAd1	absolutně
černému	černý	k2eAgNnSc3d1	černé
tělesu	těleso	k1gNnSc3	těleso
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
asi	asi	k9	asi
2850	[number]	k4	2850
K	K	kA	K
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
barevná	barevný	k2eAgFnSc1d1	barevná
teplota	teplota	k1gFnSc1	teplota
běžné	běžný	k2eAgFnSc2d1	běžná
žárovky	žárovka	k1gFnSc2	žárovka
je	být	k5eAaImIp3nS	být
2850	[number]	k4	2850
K	K	kA	K
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgNnSc1d1	denní
světlo	světlo	k1gNnSc1	světlo
má	mít	k5eAaImIp3nS	mít
typickou	typický	k2eAgFnSc4d1	typická
barevnou	barevný	k2eAgFnSc4d1	barevná
teplotu	teplota	k1gFnSc4	teplota
asi	asi	k9	asi
5400	[number]	k4	5400
K	K	kA	K
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
podmínkách	podmínka	k1gFnPc6	podmínka
(	(	kIx(	(
<g/>
denní	denní	k2eAgFnSc6d1	denní
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
počasí	počasí	k1gNnSc6	počasí
<g/>
,	,	kIx,	,
sluneční	sluneční	k2eAgFnSc6d1	sluneční
aktivitě	aktivita	k1gFnSc6	aktivita
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
standardy	standard	k1gInPc7	standard
bílého	bílý	k2eAgNnSc2d1	bílé
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
osvětlování	osvětlování	k1gNnSc4	osvětlování
(	(	kIx(	(
<g/>
CIE	CIE	kA	CIE
<g/>
)	)	kIx)	)
určila	určit	k5eAaPmAgFnS	určit
sérii	série	k1gFnSc4	série
standardních	standardní	k2eAgNnPc2d1	standardní
osvětlení	osvětlení	k1gNnPc2	osvětlení
(	(	kIx(	(
<g/>
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
série	série	k1gFnSc1	série
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
osvětlení	osvětlení	k1gNnSc2	osvětlení
D	D	kA	D
<g/>
65	[number]	k4	65
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
nastavené	nastavený	k2eAgNnSc1d1	nastavené
podle	podle	k7c2	podle
barevné	barevný	k2eAgFnSc2d1	barevná
teploty	teplota	k1gFnSc2	teplota
6500	[number]	k4	6500
K	K	kA	K
<g/>
,	,	kIx,	,
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
standardní	standardní	k2eAgNnSc1d1	standardní
denní	denní	k2eAgNnSc1d1	denní
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Počítačové	počítačový	k2eAgInPc1d1	počítačový
monitory	monitor	k1gInPc1	monitor
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
obvykle	obvykle	k6eAd1	obvykle
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
nastavit	nastavit	k5eAaPmF	nastavit
odstín	odstín	k1gInSc4	odstín
zobrazovaných	zobrazovaný	k2eAgFnPc2d1	zobrazovaná
barev	barva	k1gFnPc2	barva
právě	právě	k9	právě
pomocí	pomocí	k7c2	pomocí
volby	volba	k1gFnSc2	volba
barevné	barevný	k2eAgFnSc2d1	barevná
teploty	teplota	k1gFnSc2	teplota
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
z	z	k7c2	z
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
přednastavených	přednastavený	k2eAgFnPc2d1	přednastavená
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
pak	pak	k6eAd1	pak
elektronika	elektronika	k1gFnSc1	elektronika
monitoru	monitor	k1gInSc2	monitor
použije	použít	k5eAaPmIp3nS	použít
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
parametrů	parametr	k1gInPc2	parametr
zobrazované	zobrazovaný	k2eAgFnSc2d1	zobrazovaná
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
bílí	bílý	k1gMnPc1	bílý
jsou	být	k5eAaImIp3nP	být
označování	označování	k1gNnSc4	označování
členové	člen	k1gMnPc1	člen
bílé	bílý	k2eAgFnSc2d1	bílá
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
lidé	člověk	k1gMnPc1	člověk
světlé	světlý	k2eAgFnSc2d1	světlá
barvy	barva	k1gFnSc2	barva
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
bílá	bílý	k2eAgFnSc1d1	bílá
rasa	rasa	k1gFnSc1	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
symbolem	symbol	k1gInSc7	symbol
čistoty	čistota	k1gFnSc2	čistota
<g/>
,	,	kIx,	,
neposkvrněnosti	neposkvrněnost	k1gFnSc2	neposkvrněnost
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
např.	např.	kA	např.
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
tradiční	tradiční	k2eAgFnSc7d1	tradiční
barvou	barva	k1gFnSc7	barva
svatebních	svatební	k2eAgInPc2d1	svatební
šatů	šat	k1gInPc2	šat
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
<g/>
,	,	kIx,	,
vietnamské	vietnamský	k2eAgFnSc6d1	vietnamská
a	a	k8xC	a
indické	indický	k2eAgFnSc6d1	indická
tradici	tradice	k1gFnSc6	tradice
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
barvou	barva	k1gFnSc7	barva
duchů	duch	k1gMnPc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Smuteční	smuteční	k2eAgNnSc1d1	smuteční
oblečení	oblečení	k1gNnSc1	oblečení
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
vlajka	vlajka	k1gFnSc1	vlajka
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
kapitulaci	kapitulace	k1gFnSc4	kapitulace
či	či	k8xC	či
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
mírové	mírový	k2eAgInPc4d1	mírový
úmysly	úmysl	k1gInPc4	úmysl
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
období	období	k1gNnSc6	období
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Blikající	blikající	k2eAgNnSc1d1	blikající
bílé	bílý	k2eAgNnSc1d1	bílé
světlo	světlo	k1gNnSc1	světlo
na	na	k7c6	na
železničním	železniční	k2eAgInSc6d1	železniční
přejezdu	přejezd	k1gInSc6	přejezd
znamená	znamenat	k5eAaImIp3nS	znamenat
volno	volno	k6eAd1	volno
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
bílý	bílý	k2eAgInSc1d1	bílý
šum	šum	k1gInSc1	šum
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
signál	signál	k1gInSc1	signál
či	či	k8xC	či
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
směs	směs	k1gFnSc4	směs
signálů	signál	k1gInPc2	signál
všech	všecek	k3xTgFnPc2	všecek
frekvencí	frekvence	k1gFnPc2	frekvence
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
bílé	bílý	k2eAgNnSc1d1	bílé
světlo	světlo	k1gNnSc1	světlo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
směs	směs	k1gFnSc4	směs
světla	světlo	k1gNnSc2	světlo
všech	všecek	k3xTgFnPc2	všecek
barev	barva	k1gFnPc2	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k1gMnSc1	bílý
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc2	označení
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
jeho	jeho	k3xOp3gInPc2	jeho
hracích	hrací	k2eAgInPc2d1	hrací
kamenů	kámen	k1gInPc2	kámen
<g/>
)	)	kIx)	)
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
deskových	deskový	k2eAgFnPc6d1	desková
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
např.	např.	kA	např.
šachu	šach	k1gInSc2	šach
či	či	k8xC	či
go	go	k?	go
<g/>
.	.	kIx.	.
</s>
<s>
Soupeřovy	soupeřův	k2eAgInPc1d1	soupeřův
kameny	kámen	k1gInPc1	kámen
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
černé	černý	k2eAgFnPc1d1	černá
či	či	k8xC	či
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
značení	značení	k1gNnSc6	značení
odporů	odpor	k1gInPc2	odpor
znamená	znamenat	k5eAaImIp3nS	znamenat
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
číslici	číslice	k1gFnSc4	číslice
9	[number]	k4	9
</s>
<s>
Podle	podle	k7c2	podle
Dušana	Dušan	k1gMnSc2	Dušan
Třeštíka	Třeštík	k1gMnSc2	Třeštík
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
historiků	historik	k1gMnPc2	historik
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
bílá	bílý	k2eAgFnSc1d1	bílá
u	u	k7c2	u
starých	starý	k2eAgInPc2d1	starý
Slovanů	Slovan	k1gInPc2	Slovan
západ	západ	k1gInSc1	západ
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
použití	použití	k1gNnSc1	použití
jsou	být	k5eAaImIp3nP	být
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
Bílé	bílý	k2eAgNnSc1d1	bílé
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
západní	západní	k2eAgFnPc1d1	západní
části	část	k1gFnPc1	část
větších	veliký	k2eAgFnPc2d2	veliký
celku	celek	k1gInSc3	celek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
oleji	olej	k1gInSc6	olej
či	či	k8xC	či
temperové	temperový	k2eAgFnSc6d1	temperová
malbě	malba	k1gFnSc6	malba
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
bělobě	běloba	k1gFnSc6	běloba
(	(	kIx(	(
<g/>
běloba	běloba	k1gFnSc1	běloba
titanová	titanový	k2eAgFnSc5d1	titanová
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
