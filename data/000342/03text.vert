<s>
Zenit	zenit	k1gInSc1	zenit
(	(	kIx(	(
<g/>
nadhlavník	nadhlavník	k1gInSc1	nadhlavník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
bod	bod	k1gInSc4	bod
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
průsečík	průsečík	k1gInSc1	průsečík
kolmice	kolmice	k1gFnSc2	kolmice
na	na	k7c4	na
horizontální	horizontální	k2eAgFnSc4d1	horizontální
rovinu	rovina	k1gFnSc4	rovina
pozorovacího	pozorovací	k2eAgNnSc2d1	pozorovací
místa	místo	k1gNnSc2	místo
s	s	k7c7	s
nebeskou	nebeský	k2eAgFnSc7d1	nebeská
sférou	sféra	k1gFnSc7	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pólem	pólo	k1gNnSc7	pólo
horizontální	horizontální	k2eAgFnSc2d1	horizontální
souřadnicové	souřadnicový	k2eAgFnSc2d1	souřadnicová
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
zenitu	zenit	k1gInSc6	zenit
jen	jen	k9	jen
mezi	mezi	k7c7	mezi
obratníky	obratník	k1gInPc7	obratník
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
stát	stát	k5eAaImF	stát
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
nebo	nebo	k8xC	nebo
jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
směr	směr	k1gInSc1	směr
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
zenitu	zenit	k1gInSc3	zenit
(	(	kIx(	(
<g/>
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
pohledu	pohled	k1gInSc2	pohled
<g/>
)	)	kIx)	)
totožný	totožný	k2eAgInSc1d1	totožný
s	s	k7c7	s
rotační	rotační	k2eAgFnSc7d1	rotační
osou	osa	k1gFnSc7	osa
Země	zem	k1gFnSc2	zem
</s>
<s>
Opak	opak	k1gInSc1	opak
zenitu	zenit	k1gInSc2	zenit
je	být	k5eAaImIp3nS	být
nadir	nadir	k1gInSc1	nadir
<g/>
.	.	kIx.	.
</s>
<s>
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
sféra	sféra	k1gFnSc1	sféra
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zenit	zenit	k1gInSc1	zenit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zenit	zenit	k1gInSc1	zenit
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
