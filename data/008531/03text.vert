<p>
<s>
Den	den	k1gInSc1	den
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
státní	státní	k2eAgInSc1d1	státní
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
svátkem	svátek	k1gInSc7	svátek
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
přemyslovského	přemyslovský	k2eAgMnSc2d1	přemyslovský
knížete	kníže	k1gMnSc2	kníže
<g/>
,	,	kIx,	,
patrona	patron	k1gMnSc2	patron
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
tradičních	tradiční	k2eAgInPc2d1	tradiční
symbolů	symbol	k1gInPc2	symbol
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
den	den	k1gInSc4	den
jeho	on	k3xPp3gNnSc2	on
zavraždění	zavraždění	k1gNnSc2	zavraždění
asi	asi	k9	asi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
935	[number]	k4	935
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Václavova	Václavův	k2eAgMnSc4d1	Václavův
bratra	bratr	k1gMnSc4	bratr
Boleslava	Boleslav	k1gMnSc4	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
datu	datum	k1gNnSc3	datum
také	také	k6eAd1	také
připomíná	připomínat	k5eAaImIp3nS	připomínat
rok	rok	k1gInSc4	rok
995	[number]	k4	995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
Boleslava	Boleslav	k1gMnSc4	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
dobyta	dobyt	k2eAgFnSc1d1	dobyta
Libice	Libice	k1gFnPc1	Libice
a	a	k8xC	a
vyvražděna	vyvražděn	k2eAgFnSc1d1	vyvražděna
většina	většina	k1gFnSc1	většina
Slavníkovců	Slavníkovec	k1gMnPc2	Slavníkovec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
raného	raný	k2eAgInSc2d1	raný
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Svátek	svátek	k1gInSc4	svátek
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1951	[number]	k4	1951
památným	památný	k2eAgInSc7d1	památný
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
oslavy	oslava	k1gFnSc2	oslava
Svatováclavského	svatováclavský	k2eAgNnSc2d1	Svatováclavské
milénia	milénium	k1gNnSc2	milénium
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
prezidenta	prezident	k1gMnSc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigu	k1gInSc2	Garrigu
Masaryk	Masaryk	k1gMnSc1	Masaryk
byl	být	k5eAaImAgMnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
řečníkem	řečník	k1gMnSc7	řečník
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
svátku	svátek	k1gInSc2	svátek
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1929	[number]	k4	1929
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předal	předat	k5eAaPmAgMnS	předat
čestnou	čestný	k2eAgFnSc4d1	čestná
standartu	standarta	k1gFnSc4	standarta
Svatováclavskému	svatováclavský	k2eAgInSc3d1	svatováclavský
jezdeckému	jezdecký	k2eAgInSc3d1	jezdecký
pluku	pluk	k1gInSc3	pluk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgInSc1d1	státní
svátek	svátek	k1gInSc1	svátek
===	===	k?	===
</s>
</p>
<p>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
dne	den	k1gInSc2	den
státním	státní	k2eAgInSc7d1	státní
svátkem	svátek	k1gInSc7	svátek
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
usnesení	usnesení	k1gNnSc4	usnesení
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
mládež	mládež	k1gFnSc4	mládež
a	a	k8xC	a
tělovýchovu	tělovýchova	k1gFnSc4	tělovýchova
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
zvolil	zvolit	k5eAaPmAgInS	zvolit
pro	pro	k7c4	pro
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Den	den	k1gInSc1	den
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
–	–	k?	–
Svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
čtení	čtení	k1gNnSc6	čtení
návrhu	návrh	k1gInSc2	návrh
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
dni	den	k1gInSc3	den
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
nejprve	nejprve	k6eAd1	nejprve
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
svátek	svátek	k1gInSc1	svátek
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
historický	historický	k2eAgInSc4d1	historický
kontext	kontext	k1gInSc4	kontext
nepřímo	přímo	k6eNd1	přímo
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
servility	servilita	k1gFnSc2	servilita
a	a	k8xC	a
kolaborace	kolaborace	k1gFnSc2	kolaborace
<g/>
.	.	kIx.	.
</s>
<s>
Poslanec	poslanec	k1gMnSc1	poslanec
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
poté	poté	k6eAd1	poté
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vypuštění	vypuštění	k1gNnSc4	vypuštění
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
státních	státní	k2eAgInPc2d1	státní
svátků	svátek	k1gInPc2	svátek
uvedených	uvedený	k2eAgInPc2d1	uvedený
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
poslanec	poslanec	k1gMnSc1	poslanec
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svátek	svátek	k1gInSc1	svátek
nesl	nést	k5eAaImAgInS	nést
pouze	pouze	k6eAd1	pouze
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
Den	den	k1gInSc1	den
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Církevní	církevní	k2eAgFnPc1d1	církevní
oslavy	oslava	k1gFnPc1	oslava
==	==	k?	==
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
událostí	událost	k1gFnSc7	událost
konanou	konaný	k2eAgFnSc7d1	konaná
v	v	k7c4	v
den	den	k1gInSc4	den
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
je	být	k5eAaImIp3nS	být
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
Národní	národní	k2eAgFnSc1d1	národní
svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
pouť	pouť	k1gFnSc1	pouť
ve	v	k7c6	v
Staré	Staré	k2eAgFnSc6d1	Staré
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
je	být	k5eAaImIp3nS	být
poutní	poutní	k2eAgFnSc1d1	poutní
bohoslužba	bohoslužba	k1gFnSc1	bohoslužba
sloužená	sloužený	k2eAgFnSc1d1	sloužená
českými	český	k2eAgMnPc7d1	český
biskupy	biskup	k1gMnPc7	biskup
na	na	k7c6	na
tamějším	tamější	k2eAgNnSc6d1	tamější
Mariánském	mariánský	k2eAgNnSc6d1	Mariánské
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
nesoucím	nesoucí	k2eAgMnSc6d1	nesoucí
název	název	k1gInSc4	název
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
václavské	václavský	k2eAgFnSc6d1	Václavská
proboštské	proboštský	k2eAgFnSc6d1	Proboštská
louce	louka	k1gFnSc6	louka
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
v	v	k7c6	v
jubilejním	jubilejní	k2eAgInSc6d1	jubilejní
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
mši	mše	k1gFnSc4	mše
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
ostatních	ostatní	k2eAgInPc6d1	ostatní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
významných	významný	k2eAgInPc6d1	významný
dnech	den	k1gInPc6	den
a	a	k8xC	a
o	o	k7c6	o
dnech	den	k1gInPc6	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
101	[number]	k4	101
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZUNA	zuna	k1gFnSc1	zuna
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc4	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
české	český	k2eAgFnSc2d1	Česká
Státnosti	státnost	k1gFnSc2	státnost
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
935	[number]	k4	935
<g/>
)	)	kIx)	)
-	-	kIx~	-
Slavné	slavný	k2eAgInPc4d1	slavný
dny	den	k1gInPc4	den
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010-11-17	[number]	k4	2010-11-17
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
