<s>
Jak	jak	k6eAd1	jak
je	on	k3xPp3gMnPc4	on
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
nejpopulárnější	populární	k2eAgInSc1d3	nejpopulárnější
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
v	v	k7c6	v
Karlstadu	Karlstad	k1gInSc6	Karlstad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
švédský	švédský	k2eAgInSc1d1	švédský
šampionát	šampionát	k1gInSc1	šampionát
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
?	?	kIx.	?
</s>
