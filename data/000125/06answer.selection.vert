<s>
17	[number]	k4	17
fakult	fakulta	k1gFnPc2	fakulta
UK	UK	kA	UK
připravuje	připravovat	k5eAaImIp3nS	připravovat
odborníky	odborník	k1gMnPc4	odborník
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
oblast	oblast	k1gFnSc4	oblast
<g/>
:	:	kIx,	:
humanitních	humanitní	k2eAgFnPc2d1	humanitní
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
včetně	včetně	k7c2	včetně
učitelství	učitelství	k1gNnSc2	učitelství
<g/>
,	,	kIx,	,
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
filologie	filologie	k1gFnSc2	filologie
a	a	k8xC	a
překladatelství	překladatelství	k1gNnSc2	překladatelství
<g/>
,	,	kIx,	,
novinářství	novinářství	k1gNnSc2	novinářství
a	a	k8xC	a
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
<g/>
,	,	kIx,	,
teologie	teologie	k1gFnSc2	teologie
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
lékařských	lékařský	k2eAgFnPc2d1	lékařská
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
farmacie	farmacie	k1gFnSc2	farmacie
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
