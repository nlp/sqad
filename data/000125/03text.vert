<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgInSc2d1	Karlův
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
UK	UK	kA	UK
<g/>
,	,	kIx,	,
latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
Universitas	Universitas	k1gInSc1	Universitas
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
)	)	kIx)	)
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
česká	český	k2eAgFnSc1d1	Česká
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
evropských	evropský	k2eAgFnPc2d1	Evropská
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
severně	severně	k6eAd1	severně
od	od	k7c2	od
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
východně	východně	k6eAd1	východně
od	od	k7c2	od
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
studovalo	studovat	k5eAaImAgNnS	studovat
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zhruba	zhruba	k6eAd1	zhruba
šestina	šestina	k1gFnSc1	šestina
všech	všecek	k3xTgMnPc2	všecek
vysokoškoláků	vysokoškolák	k1gMnPc2	vysokoškolák
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
tuto	tento	k3xDgFnSc4	tento
školu	škola	k1gFnSc4	škola
vyhodnotila	vyhodnotit	k5eAaPmAgFnS	vyhodnotit
organizace	organizace	k1gFnSc1	organizace
Academic	Academic	k1gMnSc1	Academic
Ranking	Ranking	k1gInSc1	Ranking
of	of	k?	of
World	World	k1gMnSc1	World
Universities	Universities	k1gMnSc1	Universities
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
v	v	k7c6	v
regionu	region	k1gInSc6	region
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Šanghajském	šanghajský	k2eAgMnSc6d1	šanghajský
<g/>
)	)	kIx)	)
žebříčku	žebříček	k1gInSc6	žebříček
ji	on	k3xPp3gFnSc4	on
zařadila	zařadit	k5eAaPmAgFnS	zařadit
na	na	k7c4	na
201	[number]	k4	201
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
(	(	kIx(	(
<g/>
1347	[number]	k4	1347
<g/>
-	-	kIx~	-
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
univerzita	univerzita	k1gFnSc1	univerzita
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
nejméně	málo	k6eAd3	málo
třemi	tři	k4xCgInPc7	tři
akty	akt	k1gInPc7	akt
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
zakládající	zakládající	k2eAgFnSc7d1	zakládající
listinou	listina	k1gFnSc7	listina
papeže	papež	k1gMnSc2	papež
(	(	kIx(	(
<g/>
bulou	bula	k1gFnSc7	bula
<g/>
)	)	kIx)	)
Klementa	Klement	k1gMnSc4	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
potvrzenou	potvrzená	k1gFnSc4	potvrzená
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1347	[number]	k4	1347
<g/>
,	,	kIx,	,
nadační	nadační	k2eAgFnSc7d1	nadační
listinou	listina	k1gFnSc7	listina
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1348	[number]	k4	1348
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
tzv.	tzv.	kA	tzv.
Eisenašským	Eisenašský	k2eAgInSc7d1	Eisenašský
diplomem	diplom	k1gInSc7	diplom
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1349	[number]	k4	1349
<g/>
.	.	kIx.	.
</s>
<s>
Nejpozději	pozdě	k6eAd3	pozdě
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
jako	jako	k9	jako
den	den	k1gInSc4	den
jejího	její	k3xOp3gNnSc2	její
založení	založení	k1gNnSc2	založení
slaví	slavit	k5eAaImIp3nS	slavit
7	[number]	k4	7
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1348	[number]	k4	1348
<g/>
.	.	kIx.	.
</s>
<s>
Vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
uspořádání	uspořádání	k1gNnSc4	uspořádání
byly	být	k5eAaImAgFnP	být
univerzity	univerzita	k1gFnPc1	univerzita
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Bologni	Bologna	k1gFnSc6	Bologna
a	a	k8xC	a
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
,	,	kIx,	,
studium	studium	k1gNnSc1	studium
začínalo	začínat	k5eAaImAgNnS	začínat
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
artistické	artistický	k2eAgNnSc1d1	artistické
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
teologické	teologický	k2eAgFnSc6d1	teologická
<g/>
,	,	kIx,	,
právnické	právnický	k2eAgFnSc6d1	právnická
nebo	nebo	k8xC	nebo
lékařské	lékařský	k2eAgFnSc6d1	lékařská
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c6	o
celouniverzitních	celouniverzitní	k2eAgFnPc6d1	celouniverzitní
záležitostech	záležitost	k1gFnPc6	záležitost
byli	být	k5eAaImAgMnP	být
studenti	student	k1gMnPc1	student
i	i	k8xC	i
učitelé	učitel	k1gMnPc1	učitel
rozděleni	rozdělen	k2eAgMnPc1d1	rozdělen
do	do	k7c2	do
4	[number]	k4	4
"	"	kIx"	"
<g/>
národů	národ	k1gInPc2	národ
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
českého	český	k2eAgMnSc2d1	český
<g/>
,	,	kIx,	,
bavorského	bavorský	k2eAgMnSc2d1	bavorský
<g/>
,	,	kIx,	,
polského	polský	k2eAgInSc2d1	polský
a	a	k8xC	a
saského	saský	k2eAgInSc2d1	saský
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
národ	národ	k1gInSc1	národ
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
obyvatele	obyvatel	k1gMnPc4	obyvatel
Čech	Čechy	k1gFnPc2	Čechy
i	i	k8xC	i
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
německy	německy	k6eAd1	německy
mluvící	mluvící	k2eAgFnSc1d1	mluvící
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Jihoslovany	Jihoslovan	k1gMnPc4	Jihoslovan
a	a	k8xC	a
obyvatele	obyvatel	k1gMnPc4	obyvatel
Uher	uher	k1gInSc4	uher
<g/>
.	.	kIx.	.
</s>
<s>
Bavorský	bavorský	k2eAgInSc1d1	bavorský
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
Rakušany	Rakušan	k1gMnPc4	Rakušan
<g/>
,	,	kIx,	,
Šváby	Šváb	k1gMnPc4	Šváb
<g/>
,	,	kIx,	,
obyvatele	obyvatel	k1gMnPc4	obyvatel
Frank	Frank	k1gMnSc1	Frank
i	i	k9	i
Porýní	Porýní	k1gNnSc4	Porýní
<g/>
,	,	kIx,	,
polský	polský	k2eAgInSc4d1	polský
Slezany	Slezan	k1gMnPc4	Slezan
<g/>
,	,	kIx,	,
Poláky	Polák	k1gMnPc4	Polák
a	a	k8xC	a
Rusy	Rus	k1gMnPc4	Rus
<g/>
,	,	kIx,	,
saský	saský	k2eAgMnSc1d1	saský
obyvatele	obyvatel	k1gMnSc2	obyvatel
Míšeňska	Míšeňsko	k1gNnSc2	Míšeňsko
<g/>
,	,	kIx,	,
Durynska	Durynsko	k1gNnSc2	Durynsko
<g/>
,	,	kIx,	,
Horního	horní	k2eAgNnSc2d1	horní
<g/>
/	/	kIx~	/
<g/>
Dolního	dolní	k2eAgNnSc2d1	dolní
Saska	Sasko	k1gNnSc2	Sasko
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
české	český	k2eAgFnSc2d1	Česká
národnosti	národnost	k1gFnSc2	národnost
tvořili	tvořit	k5eAaImAgMnP	tvořit
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
sice	sice	k8xC	sice
začalo	začít	k5eAaPmAgNnS	začít
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1347	[number]	k4	1347
<g/>
,	,	kIx,	,
rozbíhalo	rozbíhat	k5eAaImAgNnS	rozbíhat
se	se	k3xPyFc4	se
však	však	k9	však
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
podstatným	podstatný	k2eAgInSc7d1	podstatný
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc1	založení
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
koleje	kolej	k1gFnSc2	kolej
roku	rok	k1gInSc2	rok
1366	[number]	k4	1366
a	a	k8xC	a
velkorysá	velkorysý	k2eAgFnSc1d1	velkorysá
stavba	stavba	k1gFnSc1	stavba
Karolina	Karolinum	k1gNnSc2	Karolinum
roku	rok	k1gInSc2	rok
1383	[number]	k4	1383
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1409	[number]	k4	1409
upravil	upravit	k5eAaPmAgMnS	upravit
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Kutnohorským	kutnohorský	k2eAgInSc7d1	kutnohorský
dekretem	dekret	k1gInSc7	dekret
rozhodovací	rozhodovací	k2eAgFnSc2d1	rozhodovací
pravomoci	pravomoc	k1gFnSc2	pravomoc
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
českých	český	k2eAgMnPc2d1	český
reformistů	reformista	k1gMnPc2	reformista
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
většiny	většina	k1gFnSc2	většina
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
založit	založit	k5eAaPmF	založit
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
odchodem	odchod	k1gInSc7	odchod
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
významu	význam	k1gInSc2	význam
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
stala	stát	k5eAaPmAgFnS	stát
pouhou	pouhý	k2eAgFnSc7d1	pouhá
regionální	regionální	k2eAgFnSc7d1	regionální
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
upadající	upadající	k2eAgFnSc7d1	upadající
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
,	,	kIx,	,
od	od	k7c2	od
husitských	husitský	k2eAgFnPc2d1	husitská
bouří	bouř	k1gFnPc2	bouř
univerzitou	univerzita	k1gFnSc7	univerzita
podobojí	podobojí	k2eAgFnPc2d1	podobojí
(	(	kIx(	(
<g/>
utrakvistickou	utrakvistický	k2eAgFnSc4d1	utrakvistická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
mezinárodně	mezinárodně	k6eAd1	mezinárodně
izolovalo	izolovat	k5eAaBmAgNnS	izolovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měla	mít	k5eAaImAgFnS	mít
pouze	pouze	k6eAd1	pouze
fakultu	fakulta	k1gFnSc4	fakulta
artistickou	artistický	k2eAgFnSc4d1	artistická
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
zde	zde	k6eAd1	zde
působilo	působit	k5eAaImAgNnS	působit
několik	několik	k4yIc1	několik
významných	významný	k2eAgMnPc2d1	významný
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
pečovala	pečovat	k5eAaImAgFnS	pečovat
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
výchovu	výchova	k1gFnSc4	výchova
učitelů	učitel	k1gMnPc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
kolej	kolej	k1gFnSc1	kolej
<g/>
,	,	kIx,	,
povýšená	povýšený	k2eAgFnSc1d1	povýšená
císařem	císař	k1gMnSc7	císař
Matyášem	Matyáš	k1gMnSc7	Matyáš
roku	rok	k1gInSc2	rok
1611	[number]	k4	1611
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
stará	starý	k2eAgFnSc1d1	stará
univerzita	univerzita	k1gFnSc1	univerzita
čelit	čelit	k5eAaImF	čelit
silné	silný	k2eAgFnSc3d1	silná
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
konkurenci	konkurence	k1gFnSc3	konkurence
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
nejprve	nejprve	k6eAd1	nejprve
odevzdána	odevzdat	k5eAaPmNgFnS	odevzdat
jezuitům	jezuita	k1gMnPc3	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1654	[number]	k4	1654
se	se	k3xPyFc4	se
jako	jako	k9	jako
Univerzita	univerzita	k1gFnSc1	univerzita
Karlo-Ferdinandova	Karlo-Ferdinandův	k2eAgFnSc1d1	Karlo-Ferdinandova
obnovily	obnovit	k5eAaPmAgInP	obnovit
i	i	k9	i
fakulty	fakulta	k1gFnSc2	fakulta
právnická	právnický	k2eAgFnSc1d1	právnická
a	a	k8xC	a
lékařská	lékařský	k2eAgFnSc1d1	lékařská
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
mezi	mezi	k7c7	mezi
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
univerzita	univerzita	k1gFnSc1	univerzita
podřízena	podřízen	k2eAgFnSc1d1	podřízena
státu	stát	k1gInSc2	stát
a	a	k8xC	a
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přetvořena	přetvořen	k2eAgFnSc1d1	přetvořena
na	na	k7c4	na
učiliště	učiliště	k1gNnSc4	učiliště
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
učitelů	učitel	k1gMnPc2	učitel
<g/>
,	,	kIx,	,
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
úředníků	úředník	k1gMnPc2	úředník
absolutistického	absolutistický	k2eAgInSc2d1	absolutistický
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
vykázáni	vykázán	k2eAgMnPc1d1	vykázán
a	a	k8xC	a
vyučovacím	vyučovací	k2eAgInSc7d1	vyučovací
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
místo	místo	k7c2	místo
latiny	latina	k1gFnSc2	latina
němčina	němčina	k1gFnSc1	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
univerzitní	univerzitní	k2eAgMnPc1d1	univerzitní
studenti	student	k1gMnPc1	student
významně	významně	k6eAd1	významně
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c4	na
povstání	povstání	k1gNnSc4	povstání
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
změnil	změnit	k5eAaPmAgMnS	změnit
ministr	ministr	k1gMnSc1	ministr
hrabě	hrabě	k1gMnSc1	hrabě
Thun	Thun	k1gMnSc1	Thun
novým	nový	k2eAgInSc7d1	nový
zákonem	zákon	k1gInSc7	zákon
všechny	všechen	k3xTgFnPc1	všechen
rakouské	rakouský	k2eAgFnPc1d1	rakouská
univerzity	univerzita	k1gFnPc1	univerzita
podle	podle	k7c2	podle
německého	německý	k2eAgMnSc2d1	německý
von	von	k1gInSc4	von
Humboldtova	Humboldtův	k2eAgInSc2d1	Humboldtův
vzoru	vzor	k1gInSc2	vzor
na	na	k7c4	na
svobodná	svobodný	k2eAgNnPc4d1	svobodné
učiliště	učiliště	k1gNnPc4	učiliště
věd	věda	k1gFnPc2	věda
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
autonomií	autonomie	k1gFnSc7	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
ovšem	ovšem	k9	ovšem
rostlo	růst	k5eAaImAgNnS	růst
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
až	až	k9	až
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
byla	být	k5eAaImAgFnS	být
univerzita	univerzita	k1gFnSc1	univerzita
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
německou	německý	k2eAgFnSc4d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc1	počet
studentů	student	k1gMnPc2	student
pak	pak	k6eAd1	pak
rychle	rychle	k6eAd1	rychle
rostly	růst	k5eAaImAgFnP	růst
a	a	k8xC	a
čeští	český	k2eAgMnPc1d1	český
studenti	student	k1gMnPc1	student
měli	mít	k5eAaImAgMnP	mít
brzy	brzy	k6eAd1	brzy
početní	početní	k2eAgFnSc4d1	početní
převahu	převaha	k1gFnSc4	převaha
<g/>
;	;	kIx,	;
obě	dva	k4xCgFnPc1	dva
univerzity	univerzita	k1gFnPc1	univerzita
získaly	získat	k5eAaPmAgFnP	získat
řadu	řada	k1gFnSc4	řada
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
univerzitě	univerzita	k1gFnSc6	univerzita
působili	působit	k5eAaImAgMnP	působit
například	například	k6eAd1	například
vědci	vědec	k1gMnPc1	vědec
Ernst	Ernst	k1gMnSc1	Ernst
Mach	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
Christian	Christian	k1gMnSc1	Christian
Doppler	Doppler	k1gMnSc1	Doppler
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pozdější	pozdní	k2eAgMnSc1d2	pozdější
rakouský	rakouský	k2eAgMnSc1d1	rakouský
politik	politik	k1gMnSc1	politik
Anton	Anton	k1gMnSc1	Anton
Rintelen	Rintelna	k1gFnPc2	Rintelna
<g/>
,	,	kIx,	,
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
také	také	k9	také
pozdější	pozdní	k2eAgMnSc1d2	pozdější
prezident	prezident	k1gMnSc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
a	a	k8xC	a
založení	založení	k1gNnSc6	založení
Československa	Československo	k1gNnSc2	Československo
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Československý	československý	k2eAgInSc1d1	československý
parlament	parlament	k1gInSc1	parlament
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
lex	lex	k?	lex
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
nositelem	nositel	k1gMnSc7	nositel
tradice	tradice	k1gFnSc2	tradice
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
česká	český	k2eAgFnSc1d1	Česká
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
nových	nový	k2eAgFnPc2d1	nová
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
pro	pro	k7c4	pro
univerzitu	univerzita	k1gFnSc4	univerzita
německou	německý	k2eAgFnSc7d1	německá
<g/>
,	,	kIx,	,
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
se	se	k3xPyFc4	se
studium	studium	k1gNnSc1	studium
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
i	i	k9	i
nové	nový	k2eAgFnSc2d1	nová
fakulty	fakulta	k1gFnSc2	fakulta
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
<g/>
.	.	kIx.	.
</s>
<s>
Národnostní	národnostní	k2eAgNnSc1d1	národnostní
napětí	napětí	k1gNnSc1	napětí
však	však	k9	však
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
a	a	k8xC	a
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacismu	nacismus	k1gInSc2	nacismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
pražská	pražský	k2eAgFnSc1d1	Pražská
německá	německý	k2eAgFnSc1d1	německá
univerzita	univerzita	k1gFnSc1	univerzita
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
přiklonila	přiklonit	k5eAaPmAgFnS	přiklonit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
studentských	studentský	k2eAgFnPc6d1	studentská
manifestacích	manifestace	k1gFnPc6	manifestace
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
byly	být	k5eAaImAgFnP	být
české	český	k2eAgFnPc1d1	Česká
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
"	"	kIx"	"
<g/>
dočasně	dočasně	k6eAd1	dočasně
<g/>
"	"	kIx"	"
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
UK	UK	kA	UK
převzala	převzít	k5eAaPmAgFnS	převzít
univerzita	univerzita	k1gFnSc1	univerzita
německá	německý	k2eAgFnSc1d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
(	(	kIx(	(
<g/>
od	od	k7c2	od
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
univerzita	univerzita	k1gFnSc1	univerzita
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
majetek	majetek	k1gInSc4	majetek
převzala	převzít	k5eAaPmAgFnS	převzít
UK	UK	kA	UK
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
historických	historický	k2eAgInPc2d1	historický
dokumentů	dokument	k1gInPc2	dokument
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
zakládací	zakládací	k2eAgFnSc2d1	zakládací
listiny	listina	k1gFnSc2	listina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
však	však	k9	však
při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
letech	léto	k1gNnPc6	léto
rychle	rychle	k6eAd1	rychle
rostl	růst	k5eAaImAgInS	růst
počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
po	po	k7c6	po
komunistickém	komunistický	k2eAgNnSc6d1	komunistické
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
však	však	k9	však
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
vyloučena	vyloučen	k2eAgFnSc1d1	vyloučena
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
učitelů	učitel	k1gMnPc2	učitel
propuštěno	propustit	k5eAaPmNgNnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Radikální	radikální	k2eAgFnPc1d1	radikální
reformy	reforma	k1gFnPc1	reforma
ministra	ministr	k1gMnSc2	ministr
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Nejedlého	Nejedlého	k2eAgInSc1d1	Nejedlého
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
se	se	k3xPyFc4	se
řídily	řídit	k5eAaImAgFnP	řídit
podle	podle	k7c2	podle
sovětských	sovětský	k2eAgInPc2d1	sovětský
vzorů	vzor	k1gInPc2	vzor
<g/>
,	,	kIx,	,
zavedly	zavést	k5eAaPmAgInP	zavést
povinné	povinný	k2eAgInPc1d1	povinný
studijní	studijní	k2eAgInPc1d1	studijní
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
zkoušek	zkouška	k1gFnPc2	zkouška
a	a	k8xC	a
univerzitu	univerzita	k1gFnSc4	univerzita
zcela	zcela	k6eAd1	zcela
podřídily	podřídit	k5eAaPmAgInP	podřídit
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jistém	jistý	k2eAgInSc6d1	jistý
pokusu	pokus	k1gInSc6	pokus
o	o	k7c6	o
liberalizaci	liberalizace	k1gFnSc6	liberalizace
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
postihla	postihnout	k5eAaPmAgFnS	postihnout
i	i	k9	i
UK	UK	kA	UK
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
čistky	čistka	k1gFnPc4	čistka
mezi	mezi	k7c7	mezi
studenty	student	k1gMnPc7	student
i	i	k8xC	i
učiteli	učitel	k1gMnPc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Podporu	podpora	k1gFnSc4	podpora
měly	mít	k5eAaImAgFnP	mít
hlavně	hlavně	k9	hlavně
přírodovědecké	přírodovědecký	k2eAgInPc4d1	přírodovědecký
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Matematicko-fyzikální	matematickoyzikální	k2eAgFnSc1d1	matematicko-fyzikální
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fakulta	fakulta	k1gFnSc1	fakulta
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
sametové	sametový	k2eAgFnSc3d1	sametová
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
mírou	míra	k1gFnSc7	míra
přičinili	přičinit	k5eAaPmAgMnP	přičinit
pražští	pražský	k2eAgMnPc1d1	pražský
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
akademická	akademický	k2eAgFnSc1d1	akademická
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
senáty	senát	k1gInPc1	senát
a	a	k8xC	a
volby	volba	k1gFnPc1	volba
funkcionářů	funkcionář	k1gMnPc2	funkcionář
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
byly	být	k5eAaImAgFnP	být
státní	státní	k2eAgFnPc1d1	státní
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
přetvořeny	přetvořen	k2eAgFnPc1d1	přetvořena
na	na	k7c4	na
školy	škola	k1gFnPc4	škola
veřejné	veřejný	k2eAgFnPc4d1	veřejná
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
rozhodováním	rozhodování	k1gNnSc7	rozhodování
<g/>
.	.	kIx.	.
</s>
<s>
Prudkému	prudký	k2eAgInSc3d1	prudký
růstu	růst	k1gInSc3	růst
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
však	však	k9	však
neodpovídal	odpovídat	k5eNaImAgMnS	odpovídat
nárůst	nárůst	k1gInSc4	nárůst
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
zejména	zejména	k9	zejména
investic	investice	k1gFnPc2	investice
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
řada	řada	k1gFnSc1	řada
fakult	fakulta	k1gFnPc2	fakulta
se	se	k3xPyFc4	se
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
prostorovými	prostorový	k2eAgInPc7d1	prostorový
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
dařilo	dařit	k5eAaImAgNnS	dařit
navazovat	navazovat	k5eAaImF	navazovat
přerušené	přerušený	k2eAgInPc4d1	přerušený
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
styky	styk	k1gInPc4	styk
včetně	včetně	k7c2	včetně
studentských	studentský	k2eAgFnPc2d1	studentská
výměn	výměna	k1gFnPc2	výměna
a	a	k8xC	a
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc1d2	veliký
důraz	důraz	k1gInSc1	důraz
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
činnost	činnost	k1gFnSc4	činnost
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
UK	UK	kA	UK
podle	podle	k7c2	podle
vládní	vládní	k2eAgFnSc2d1	vládní
metodiky	metodika	k1gFnSc2	metodika
téměř	téměř	k6eAd1	téměř
třetinu	třetina	k1gFnSc4	třetina
celého	celý	k2eAgInSc2d1	celý
základního	základní	k2eAgInSc2d1	základní
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc1d2	veliký
třetina	třetina	k1gFnSc1	třetina
připadla	připadnout	k5eAaPmAgFnS	připadnout
na	na	k7c4	na
Akademii	akademie	k1gFnSc4	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
menší	malý	k2eAgMnSc1d2	menší
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
přílohy	příloha	k1gFnSc2	příloha
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
172	[number]	k4	172
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
univerzita	univerzita	k1gFnSc1	univerzita
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
nazývala	nazývat	k5eAaImAgFnS	nazývat
"	"	kIx"	"
<g/>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
příloha	příloha	k1gFnSc1	příloha
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
ale	ale	k9	ale
objevily	objevit	k5eAaPmAgFnP	objevit
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
názvu	název	k1gInSc3	název
bez	bez	k7c2	bez
dodatku	dodatek	k1gInSc2	dodatek
označujícího	označující	k2eAgInSc2d1	označující
její	její	k3xOp3gFnSc3	její
sídelní	sídelní	k2eAgNnSc1d1	sídelní
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
už	už	k9	už
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
pražských	pražský	k2eAgFnPc2d1	Pražská
univerzit	univerzita	k1gFnPc2	univerzita
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
novelizován	novelizovat	k5eAaBmNgInS	novelizovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
od	od	k7c2	od
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
změnil	změnit	k5eAaPmAgInS	změnit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
"	"	kIx"	"
<g/>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgInSc2d1	Karlův
<g/>
"	"	kIx"	"
bez	bez	k7c2	bez
dodatku	dodatek	k1gInSc2	dodatek
označujícího	označující	k2eAgInSc2d1	označující
její	její	k3xOp3gNnPc4	její
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
17	[number]	k4	17
fakult	fakulta	k1gFnPc2	fakulta
UK	UK	kA	UK
připravuje	připravovat	k5eAaImIp3nS	připravovat
odborníky	odborník	k1gMnPc4	odborník
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
oblast	oblast	k1gFnSc4	oblast
<g/>
:	:	kIx,	:
humanitních	humanitní	k2eAgFnPc2d1	humanitní
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
včetně	včetně	k7c2	včetně
učitelství	učitelství	k1gNnSc2	učitelství
<g/>
,	,	kIx,	,
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
filologie	filologie	k1gFnSc2	filologie
a	a	k8xC	a
překladatelství	překladatelství	k1gNnSc2	překladatelství
<g/>
,	,	kIx,	,
novinářství	novinářství	k1gNnSc2	novinářství
a	a	k8xC	a
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
<g/>
,	,	kIx,	,
teologie	teologie	k1gFnSc2	teologie
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
lékařských	lékařský	k2eAgFnPc2d1	lékařská
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
farmacie	farmacie	k1gFnSc2	farmacie
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
oborů	obor	k1gInPc2	obor
je	být	k5eAaImIp3nS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
jedinou	jediný	k2eAgFnSc7d1	jediná
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dané	daný	k2eAgNnSc4d1	dané
vzdělání	vzdělání	k1gNnSc4	vzdělání
nabízí	nabízet	k5eAaImIp3nS	nabízet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
studovalo	studovat	k5eAaImAgNnS	studovat
na	na	k7c4	na
UK	UK	kA	UK
52	[number]	k4	52
842	[number]	k4	842
studentů	student	k1gMnPc2	student
v	v	k7c6	v
625	[number]	k4	625
studijních	studijní	k2eAgInPc6d1	studijní
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
7972	[number]	k4	7972
doktorských	doktorský	k2eAgMnPc2d1	doktorský
studentů	student	k1gMnPc2	student
a	a	k8xC	a
přes	přes	k7c4	přes
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
dostala	dostat	k5eAaPmAgFnS	dostat
UK	UK	kA	UK
61	[number]	k4	61
621	[number]	k4	621
přihlášek	přihláška	k1gFnPc2	přihláška
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
18	[number]	k4	18
020	[number]	k4	020
nových	nový	k2eAgMnPc2d1	nový
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
jejích	její	k3xOp3gMnPc2	její
absolventů	absolvent	k1gMnPc2	absolvent
byla	být	k5eAaImAgFnS	být
1,6	[number]	k4	1,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
VŠE	všechen	k3xTgNnSc4	všechen
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
hodnota	hodnota	k1gFnSc1	hodnota
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Erasmus	Erasmus	k1gInSc1	Erasmus
vycestovalo	vycestovat	k5eAaPmAgNnS	vycestovat
na	na	k7c4	na
semestr	semestr	k1gInSc4	semestr
1858	[number]	k4	1858
a	a	k8xC	a
přicestovalo	přicestovat	k5eAaPmAgNnS	přicestovat
1759	[number]	k4	1759
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
placených	placený	k2eAgInPc6d1	placený
kurzech	kurz	k1gInPc6	kurz
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
studovalo	studovat	k5eAaImAgNnS	studovat
16	[number]	k4	16
555	[number]	k4	555
<g/>
,	,	kIx,	,
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
4574	[number]	k4	4574
zájemců	zájemce	k1gMnPc2	zájemce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
474	[number]	k4	474
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
640	[number]	k4	640
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
1000	[number]	k4	1000
docentů	docent	k1gMnPc2	docent
<g/>
,	,	kIx,	,
1500	[number]	k4	1500
vědeckých	vědecký	k2eAgMnPc2d1	vědecký
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
přes	přes	k7c4	přes
4600	[number]	k4	4600
asistentů	asistent	k1gMnPc2	asistent
a	a	k8xC	a
lektorů	lektor	k1gMnPc2	lektor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
UK	UK	kA	UK
významné	významný	k2eAgNnSc1d1	významné
místo	místo	k1gNnSc1	místo
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
produkci	produkce	k1gFnSc6	produkce
mezinárodně	mezinárodně	k6eAd1	mezinárodně
hodnocených	hodnocený	k2eAgInPc2d1	hodnocený
-	-	kIx~	-
hlavně	hlavně	k9	hlavně
přírodovědeckých	přírodovědecký	k2eAgFnPc2d1	Přírodovědecká
a	a	k8xC	a
medicínských	medicínský	k2eAgFnPc2d1	medicínská
-	-	kIx~	-
publikací	publikace	k1gFnPc2	publikace
(	(	kIx(	(
<g/>
WoS	WoS	k1gFnSc1	WoS
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
UK	UK	kA	UK
podílí	podílet	k5eAaImIp3nS	podílet
30	[number]	k4	30
%	%	kIx~	%
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
produkce	produkce	k1gFnSc2	produkce
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
bibliometrický	bibliometrický	k2eAgInSc1d1	bibliometrický
index	index	k1gInSc1	index
H	H	kA	H
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
na	na	k7c4	na
42	[number]	k4	42
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
V	v	k7c6	v
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
žebříčcích	žebříček	k1gInPc6	žebříček
(	(	kIx(	(
<g/>
Shanghai	Shanghai	k1gNnSc4	Shanghai
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Times	Times	k1gMnSc1	Times
HE	he	k0	he
Supplement	Supplement	k1gMnSc1	Supplement
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ovšem	ovšem	k9	ovšem
silně	silně	k6eAd1	silně
preferují	preferovat	k5eAaImIp3nP	preferovat
přírodní	přírodní	k2eAgFnPc1d1	přírodní
a	a	k8xC	a
lékařské	lékařský	k2eAgFnPc1d1	lékařská
vědy	věda	k1gFnPc1	věda
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgFnSc1d1	mluvící
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
500	[number]	k4	500
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
někde	někde	k6eAd1	někde
kolem	kolem	k7c2	kolem
250	[number]	k4	250
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Identifikační	identifikační	k2eAgNnSc1d1	identifikační
číslo	číslo	k1gNnSc1	číslo
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
IČO	IČO	kA	IČO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
216208	[number]	k4	216208
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
univerzity	univerzita	k1gFnSc2	univerzita
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
přes	přes	k7c4	přes
8	[number]	k4	8
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
téměř	téměř	k6eAd1	téměř
2,5	[number]	k4	2,5
mld.	mld.	k?	mld.
jsou	být	k5eAaImIp3nP	být
vlastní	vlastní	k2eAgInPc4d1	vlastní
příjmy	příjem	k1gInPc4	příjem
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
státní	státní	k2eAgInSc4d1	státní
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
své	svůj	k3xOyFgFnSc2	svůj
akademické	akademický	k2eAgFnSc2d1	akademická
obci	obec	k1gFnSc3	obec
rovněž	rovněž	k9	rovněž
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
řadu	řada	k1gFnSc4	řada
podpůrných	podpůrný	k2eAgFnPc2d1	podpůrná
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
ubytování	ubytování	k1gNnSc1	ubytování
<g/>
,	,	kIx,	,
stravování	stravování	k1gNnSc1	stravování
(	(	kIx(	(
<g/>
Koleje	kolej	k1gFnPc1	kolej
a	a	k8xC	a
menzy	menza	k1gFnPc1	menza
<g/>
,	,	kIx,	,
KaM	kam	k6eAd1	kam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydavatelská	vydavatelský	k2eAgFnSc1d1	vydavatelská
činnost	činnost	k1gFnSc1	činnost
(	(	kIx(	(
<g/>
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
)	)	kIx)	)
a	a	k8xC	a
knihovny	knihovna	k1gFnPc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
knihovní	knihovní	k2eAgInSc1d1	knihovní
fond	fond	k1gInSc1	fond
činí	činit	k5eAaImIp3nS	činit
4,66	[number]	k4	4,66
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
721	[number]	k4	721
tisíc	tisíc	k4xCgInPc2	tisíc
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
výběru	výběr	k1gInSc6	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
univerzitě	univerzita	k1gFnSc6	univerzita
působí	působit	k5eAaImIp3nP	působit
tři	tři	k4xCgInPc1	tři
hudební	hudební	k2eAgInPc1d1	hudební
sbory	sbor	k1gInPc1	sbor
<g/>
:	:	kIx,	:
Sbor	sbor	k1gInSc1	sbor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
Vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
umělecký	umělecký	k2eAgInSc4d1	umělecký
soubor	soubor	k1gInSc4	soubor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
a	a	k8xC	a
Smíšený	smíšený	k2eAgInSc4d1	smíšený
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
sbor	sbor	k1gInSc4	sbor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
Orchestr	orchestr	k1gInSc1	orchestr
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
UK	UK	kA	UK
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
časopis	časopis	k1gInSc4	časopis
Forum	forum	k1gNnSc1	forum
a	a	k8xC	a
internetový	internetový	k2eAgInSc1d1	internetový
magazín	magazín	k1gInSc1	magazín
iForum	iForum	k1gInSc1	iForum
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
spojení	spojení	k1gNnSc2	spojení
v	v	k7c6	v
občanském	občanský	k2eAgNnSc6d1	občanské
sdružení	sdružení	k1gNnSc6	sdružení
UK	UK	kA	UK
media	medium	k1gNnSc2	medium
provozují	provozovat	k5eAaImIp3nP	provozovat
celouniverzitní	celouniverzitní	k2eAgInSc4d1	celouniverzitní
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
portál	portál	k1gInSc4	portál
UKáčko	UKáčko	k6eAd1	UKáčko
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
a	a	k8xC	a
vydávají	vydávat	k5eAaImIp3nP	vydávat
tištěné	tištěný	k2eAgInPc4d1	tištěný
časopisy	časopis	k1gInPc4	časopis
Sociál	sociál	k1gMnSc1	sociál
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
FFakt	FFakt	k1gInSc1	FFakt
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vychází	vycházet	k5eAaImIp3nS	vycházet
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
humanitních	humanitní	k2eAgFnPc2d1	humanitní
studií	studie	k1gFnPc2	studie
časopis	časopis	k1gInSc1	časopis
Humr	humr	k1gMnSc1	humr
a	a	k8xC	a
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Primalex	primalex	k1gInSc4	primalex
<g/>
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnSc1	medaile
Emanuela	Emanuel	k1gMnSc2	Emanuel
Bořického	Bořický	k2eAgInSc2d1	Bořický
Bolzanova	Bolzanův	k2eAgInSc2d1	Bolzanův
cena	cena	k1gFnSc1	cena
Karlova	Karlův	k2eAgFnSc1d1	Karlova
univerzita	univerzita	k1gFnSc1	univerzita
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
1,5	[number]	k4	1,5
<g/>
%	%	kIx~	%
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
světových	světový	k2eAgFnPc2d1	světová
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
univerzita	univerzita	k1gFnSc1	univerzita
ČR	ČR	kA	ČR
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
univerzit	univerzita	k1gFnPc2	univerzita
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
Východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
soutěžící	soutěžící	k1gFnSc2	soutěžící
s	s	k7c7	s
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
Ruskou	ruský	k2eAgFnSc7d1	ruská
Lomonosovovou	Lomonosovový	k2eAgFnSc7d1	Lomonosovový
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Karlova	Karlův	k2eAgFnSc1d1	Karlova
univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
31	[number]	k4	31
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c4	v
Times	Times	k1gInSc4	Times
BRICS	BRICS	kA	BRICS
&	&	k?	&
Emerging	Emerging	k1gInSc1	Emerging
Economies	Economies	k1gInSc1	Economies
Rankings	Rankings	k1gInSc1	Rankings
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
Karlova	Karlův	k2eAgFnSc1d1	Karlova
Univerzita	univerzita	k1gFnSc1	univerzita
umístila	umístit	k5eAaPmAgFnS	umístit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
jako	jako	k8xS	jako
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
.	.	kIx.	.
mezi	mezi	k7c7	mezi
500	[number]	k4	500
hodnocenými	hodnocený	k2eAgInPc7d1	hodnocený
dle	dle	k7c2	dle
Academic	Academice	k1gInPc2	Academice
Ranking	Ranking	k1gInSc4	Ranking
of	of	k?	of
World	World	k1gInSc1	World
Universities	Universities	k1gInSc1	Universities
(	(	kIx(	(
<g/>
Šanghajský	šanghajský	k2eAgInSc1d1	šanghajský
žebříček	žebříček	k1gInSc1	žebříček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
233	[number]	k4	233
<g/>
.	.	kIx.	.
z	z	k7c2	z
500	[number]	k4	500
v	v	k7c6	v
QS	QS	kA	QS
World	World	k1gInSc4	World
University	universita	k1gFnSc2	universita
Rankings	Rankingsa	k1gFnPc2	Rankingsa
<g/>
,	,	kIx,	,
401	[number]	k4	401
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
<g/>
.	.	kIx.	.
z	z	k7c2	z
800	[number]	k4	800
(	(	kIx(	(
<g/>
výsledek	výsledek	k1gInSc1	výsledek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
hodnocených	hodnocený	k2eAgInPc2d1	hodnocený
in	in	k?	in
Times	Times	k1gInSc1	Times
Higher	Highra	k1gFnPc2	Highra
Education	Education	k1gInSc4	Education
World	World	k1gInSc1	World
University	universita	k1gFnSc2	universita
Rankings	Rankingsa	k1gFnPc2	Rankingsa
a	a	k8xC	a
485	[number]	k4	485
<g/>
.	.	kIx.	.
dle	dle	k7c2	dle
CWTS	CWTS	kA	CWTS
Leiden	Leidna	k1gFnPc2	Leidna
Ranking	Ranking	k1gInSc1	Ranking
zohledňujícím	zohledňující	k2eAgFnPc3d1	zohledňující
500	[number]	k4	500
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgInPc1d1	dřívější
výsledky	výsledek	k1gInPc1	výsledek
viz	vidět	k5eAaImRp2nS	vidět
tabulky	tabulka	k1gFnPc4	tabulka
níže	nízce	k6eAd2	nízce
<g/>
:	:	kIx,	:
V	v	k7c6	v
QS	QS	kA	QS
žebříčku	žebříček	k1gInSc6	žebříček
dle	dle	k7c2	dle
předmětů	předmět	k1gInPc2	předmět
patří	patřit	k5eAaImIp3nS	patřit
Karlova	Karlův	k2eAgFnSc1d1	Karlova
univerzita	univerzita	k1gFnSc1	univerzita
mezi	mezi	k7c4	mezi
51-100	[number]	k4	51-100
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
univerzit	univerzita	k1gFnPc2	univerzita
na	na	k7c6	na
světe	svět	k1gInSc5	svět
v	v	k7c6	v
geografii	geografie	k1gFnSc6	geografie
a	a	k8xC	a
lingvistice	lingvistika	k1gFnSc6	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Hasner	Hasnra	k1gFnPc2	Hasnra
von	von	k1gInSc1	von
Artha	Artha	k1gMnSc1	Artha
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Československa	Československo	k1gNnSc2	Československo
Václav	Václav	k1gMnSc1	Václav
Bělohradský	Bělohradský	k1gMnSc1	Bělohradský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
sociolog	sociolog	k1gMnSc1	sociolog
Bernard	Bernard	k1gMnSc1	Bernard
<g />
.	.	kIx.	.
</s>
<s>
Bolzano	Bolzana	k1gFnSc5	Bolzana
(	(	kIx(	(
<g/>
1781	[number]	k4	1781
<g/>
-	-	kIx~	-
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnSc1d1	píšící
pražský	pražský	k2eAgMnSc1d1	pražský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Carl	Carl	k1gMnSc1	Carl
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Cori	Core	k1gFnSc4	Core
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biochemik	biochemik	k1gMnSc1	biochemik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
<g />
.	.	kIx.	.
</s>
<s>
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Gerty	Gert	k1gMnPc4	Gert
Coriová	Coriový	k2eAgFnSc1d1	Coriová
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biochemička	biochemička	k1gFnSc1	biochemička
<g/>
,	,	kIx,	,
nositelka	nositelka	k1gFnSc1	nositelka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Eduard	Eduard	k1gMnSc1	Eduard
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
Václav	Václav	k1gMnSc1	Václav
<g />
.	.	kIx.	.
</s>
<s>
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
vědec	vědec	k1gMnSc1	vědec
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
(	(	kIx(	(
<g/>
1753	[number]	k4	1753
<g/>
-	-	kIx~	-
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Prokop	Prokop	k1gMnSc1	Prokop
Drtina	drtina	k1gFnSc1	drtina
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Max	Max	k1gMnSc1	Max
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
Karel	Karel	k1gMnSc1	Karel
Engliš	Engliš	k1gMnSc1	Engliš
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
Karel	Karel	k1gMnSc1	Karel
Farský	farský	k2eAgMnSc1d1	farský
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
Jan	Jan	k1gMnSc1	Jan
Gebauer	Gebauer	k1gMnSc1	Gebauer
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lingvista	lingvista	k1gMnSc1	lingvista
Ernest	Ernest	k1gMnSc1	Ernest
Gellner	Gellner	k1gMnSc1	Gellner
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
Jiří	Jiří	k1gMnSc1	Jiří
Grygar	Grygar	k1gMnSc1	Grygar
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
astrofyzik	astrofyzik	k1gMnSc1	astrofyzik
a	a	k8xC	a
významný	významný	k2eAgMnSc1d1	významný
popularizátor	popularizátor	k1gMnSc1	popularizátor
vědy	věda	k1gFnSc2	věda
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Heyrovský	Heyrovský	k2eAgMnSc1d1	Heyrovský
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
-	-	kIx~	-
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Václav	Václav	k1gMnSc1	Václav
Hlavatý	Hlavatý	k1gMnSc1	Hlavatý
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
Antonín	Antonín	k1gMnSc1	Antonín
Holý	Holý	k1gMnSc1	Holý
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právnička	právnička	k1gFnSc1	právnička
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
politička	politička	k1gFnSc1	politička
Ivan	Ivana	k1gFnPc2	Ivana
Horbaczewski	Horbaczewsk	k1gFnSc2	Horbaczewsk
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabal	Hrabal	k1gMnSc1	Hrabal
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hroch	Hroch	k1gMnSc1	Hroch
(	(	kIx(	(
<g/>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1371	[number]	k4	1371
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náboženský	náboženský	k2eAgMnSc1d1	náboženský
myslitel	myslitel	k1gMnSc1	myslitel
a	a	k8xC	a
reformátor	reformátor	k1gMnSc1	reformátor
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
(	(	kIx(	(
<g/>
1375	[number]	k4	1375
<g/>
-	-	kIx~	-
<g/>
1429	[number]	k4	1429
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
Jan	Jan	k1gMnSc1	Jan
Janský	janský	k2eAgMnSc1d1	janský
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
krevních	krevní	k2eAgInPc2d1	krevní
typů	typ	k1gInPc2	typ
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Josef	Josef	k1gMnSc1	Josef
Jireček	Jireček	k1gMnSc1	Jireček
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slavista	slavista	k1gMnSc1	slavista
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
(	(	kIx(	(
<g/>
1773	[number]	k4	1773
<g/>
-	-	kIx~	-
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bohemista	bohemista	k1gMnSc1	bohemista
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Karel	Karel	k1gMnSc1	Karel
<g />
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Miroslav	Miroslav	k1gMnSc1	Miroslav
Katětov	Katětov	k1gInSc1	Katětov
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
Egon	Egon	k1gMnSc1	Egon
Erwin	Erwin	k1gMnSc1	Erwin
Kisch	Kisch	k1gMnSc1	Kisch
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgInSc1d1	píšící
pražský	pražský	k2eAgInSc1d1	pražský
<g />
.	.	kIx.	.
</s>
<s>
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
Mistr	mistr	k1gMnSc1	mistr
Klaret	Klaret	k1gMnSc1	Klaret
(	(	kIx(	(
<g/>
1320	[number]	k4	1320
<g/>
-	-	kIx~	-
<g/>
1370	[number]	k4	1370
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
lexikograf	lexikograf	k1gMnSc1	lexikograf
Václav	Václav	k1gMnSc1	Václav
Kliment	Kliment	k1gMnSc1	Kliment
Klicpera	Klicpera	k1gFnSc1	Klicpera
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
-	-	kIx~	-
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Luboš	Luboš	k1gMnSc1	Luboš
Kohoutek	Kohoutek	k1gMnSc1	Kohoutek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
Jan	Jan	k1gMnSc1	Jan
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kolár	Kolár	k1gMnSc1	Kolár
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Jaromír	Jaromír	k1gMnSc1	Jaromír
Korčák	Korčák	k1gMnSc1	Korčák
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
geograf	geograf	k1gMnSc1	geograf
<g/>
,	,	kIx,	,
demograf	demograf	k1gMnSc1	demograf
a	a	k8xC	a
statistik	statistik	k1gMnSc1	statistik
Viktorin	Viktorin	k1gInSc4	Viktorin
Kornel	Kornel	k1gMnSc1	Kornel
ze	z	k7c2	z
Všehrd	Všehrd	k1gMnSc1	Všehrd
(	(	kIx(	(
<g/>
1460	[number]	k4	1460
<g/>
-	-	kIx~	-
<g/>
1520	[number]	k4	1520
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
František	František	k1gMnSc1	František
Kovář	Kovář	k1gMnSc1	Kovář
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Jiří	Jiří	k1gMnSc1	Jiří
Krupička	Krupička	k1gMnSc1	Krupička
(	(	kIx(	(
<g/>
*	*	kIx~	*
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
geolog	geolog	k1gMnSc1	geolog
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Křišťan	Křišťan	k1gMnSc1	Křišťan
z	z	k7c2	z
Prachatic	Prachatice	k1gFnPc2	Prachatice
(	(	kIx(	(
<g/>
1370	[number]	k4	1370
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1439	[number]	k4	1439
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Milan	Milan	k1gMnSc1	Milan
Kundera	Kundera	k1gFnSc1	Kundera
(	(	kIx(	(
<g/>
*	*	kIx~	*
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
František	František	k1gMnSc1	František
Lexa	Lexa	k1gMnSc1	Lexa
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
egyptolog	egyptolog	k1gMnSc1	egyptolog
Johann	Johann	k1gMnSc1	Johann
Josef	Josef	k1gMnSc1	Josef
Loschmidt	Loschmidt	k1gMnSc1	Loschmidt
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
-	-	kIx~	-
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
Lukáš	Lukáš	k1gMnSc1	Lukáš
Pražský	pražský	k2eAgMnSc1d1	pražský
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1460	[number]	k4	1460
<g/>
-	-	kIx~	-
<g/>
1528	[number]	k4	1528
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
-	-	kIx~	-
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
Jan	Jan	k1gMnSc1	Jan
Marcus	Marcus	k1gMnSc1	Marcus
Marci	Marek	k1gMnPc1	Marek
(	(	kIx(	(
<g/>
1595	[number]	k4	1595
<g/>
-	-	kIx~	-
<g/>
1667	[number]	k4	1667
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
Vilém	Vilém	k1gMnSc1	Vilém
Mathesius	Mathesius	k1gMnSc1	Mathesius
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lingvista	lingvista	k1gMnSc1	lingvista
Jan	Jan	k1gMnSc1	Jan
Mukařovský	Mukařovský	k1gMnSc1	Mukařovský
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
vědec	vědec	k1gMnSc1	vědec
Jan	Jan	k1gMnSc1	Jan
Patočka	Patočka	k1gMnSc1	Patočka
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
František	František	k1gMnSc1	František
Martin	Martin	k1gMnSc1	Martin
Pelcl	Pelcl	k1gMnSc1	Pelcl
(	(	kIx(	(
<g/>
1734	[number]	k4	1734
<g/>
-	-	kIx~	-
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bohemista	bohemista	k1gMnSc1	bohemista
<g />
.	.	kIx.	.
</s>
<s>
Georg	Georg	k1gInSc1	Georg
Placzek	Placzek	k1gInSc1	Placzek
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Karel	Karel	k1gMnSc1	Karel
Poláček	Poláček	k1gMnSc1	Poláček
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
Leopold	Leopold	k1gMnSc1	Leopold
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česko-americký	českomerický	k2eAgMnSc1d1	česko-americký
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
antropolog	antropolog	k1gMnSc1	antropolog
<g />
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
(	(	kIx(	(
<g/>
1787	[number]	k4	1787
<g/>
-	-	kIx~	-
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyziolog	fyziolog	k1gMnSc1	fyziolog
Emanuel	Emanuel	k1gMnSc1	Emanuel
Rádl	Rádl	k1gMnSc1	Rádl
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Alois	Alois	k1gMnSc1	Alois
Rašín	Rašín	k1gMnSc1	Rašín
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Milada	Milada	k1gFnSc1	Milada
Součková	Součková	k1gFnSc1	Součková
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Pavel	Pavel	k1gMnSc1	Pavel
Stránský	Stránský	k1gMnSc1	Stránský
(	(	kIx(	(
<g/>
1583	[number]	k4	1583
<g/>
-	-	kIx~	-
<g/>
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Šalda	Šalda	k1gMnSc1	Šalda
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
Ota	Ota	k1gMnSc1	Ota
Šik	šik	k6eAd1	šik
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
František	František	k1gMnSc1	František
Šmahel	Šmahel	k1gMnSc1	Šmahel
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Alois	Alois	k1gMnSc1	Alois
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šmilovský	Šmilovský	k2eAgMnSc1d1	Šmilovský
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1837	[number]	k4	1837
<g/>
-	-	kIx~	-
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Josef	Josef	k1gMnSc1	Josef
Šusta	Šusta	k1gMnSc1	Šusta
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Stolička	stolička	k1gFnSc1	stolička
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paleontolog	paleontolog	k1gMnSc1	paleontolog
Dominik	Dominik	k1gMnSc1	Dominik
Tatarka	Tatarka	k1gFnSc1	Tatarka
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Václav	Václava	k1gFnPc2	Václava
Vladivoj	Vladivoj	k1gInSc1	Vladivoj
Tomek	Tomek	k1gMnSc1	Tomek
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Peter	Peter	k1gMnSc1	Peter
Tomka	Tomek	k1gMnSc2	Tomek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
Dušan	Dušan	k1gMnSc1	Dušan
Třeštík	Třeštík	k1gMnSc1	Třeštík
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Michal	Michal	k1gMnSc1	Michal
Viewegh	Viewegh	k1gMnSc1	Viewegh
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jan	Jan	k1gMnSc1	Jan
Campanus	Campanus	k1gMnSc1	Campanus
Vodňanský	vodňanský	k2eAgMnSc1d1	vodňanský
(	(	kIx(	(
<g/>
1572	[number]	k4	1572
<g/>
-	-	kIx~	-
<g/>
1622	[number]	k4	1622
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
humanista	humanista	k1gMnSc1	humanista
Petr	Petr	k1gMnSc1	Petr
Vopěnka	Vopěnka	k1gFnSc1	Vopěnka
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Jiří	Jiří	k1gMnSc1	Jiří
Weil	Weil	k1gMnSc1	Weil
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
René	René	k1gMnSc1	René
Wellek	Wellek	k1gMnSc1	Wellek
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
Max	Max	k1gMnSc1	Max
Wertheimer	Wertheimer	k1gMnSc1	Wertheimer
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psycholog	psycholog	k1gMnSc1	psycholog
Petr	Petr	k1gMnSc1	Petr
Zenkl	Zenkl	k1gInSc1	Zenkl
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Mezi	mezi	k7c4	mezi
původní	původní	k2eAgFnPc4d1	původní
čtyři	čtyři	k4xCgFnPc4	čtyři
fakulty	fakulta	k1gFnPc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
patřily	patřit	k5eAaImAgInP	patřit
<g/>
:	:	kIx,	:
právnická	právnický	k2eAgFnSc1d1	právnická
<g/>
,	,	kIx,	,
lékařská	lékařský	k2eAgFnSc1d1	lékařská
<g/>
,	,	kIx,	,
teologická	teologický	k2eAgFnSc1d1	teologická
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
katolická	katolický	k2eAgFnSc1d1	katolická
teologická	teologický	k2eAgFnSc1d1	teologická
<g/>
)	)	kIx)	)
a	a	k8xC	a
artistická	artistický	k2eAgFnSc1d1	artistická
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
filozofická	filozofický	k2eAgFnSc1d1	filozofická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
UK	UK	kA	UK
tvoří	tvořit	k5eAaImIp3nS	tvořit
17	[number]	k4	17
fakult	fakulta	k1gFnPc2	fakulta
sídlících	sídlící	k2eAgFnPc2d1	sídlící
především	především	k6eAd1	především
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc1	dva
sídlí	sídlet	k5eAaImIp3nP	sídlet
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
Evangelická	evangelický	k2eAgFnSc1d1	evangelická
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
Husitská	husitský	k2eAgFnSc1d1	husitská
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
1	[number]	k4	1
<g/>
.	.	kIx.	.
lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
2	[number]	k4	2
<g/>
.	.	kIx.	.
lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
3	[number]	k4	3
<g/>
.	.	kIx.	.
lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králové	k2eAgFnSc1d1	Králové
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králové	k2eAgFnSc1d1	Králové
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
Matematicko-fyzikální	matematickoyzikální	k2eAgFnSc1d1	matematicko-fyzikální
fakulta	fakulta	k1gFnSc1	fakulta
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
Fakulta	fakulta	k1gFnSc1	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
<g />
.	.	kIx.	.
</s>
<s>
věd	věda	k1gFnPc2	věda
Fakulta	fakulta	k1gFnSc1	fakulta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
Fakulta	fakulta	k1gFnSc1	fakulta
humanitních	humanitní	k2eAgFnPc2d1	humanitní
studií	studie	k1gFnPc2	studie
Ústav	ústav	k1gInSc1	ústav
dějin	dějiny	k1gFnPc2	dějiny
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
a	a	k8xC	a
archiv	archiv	k1gInSc1	archiv
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Centrum	centrum	k1gNnSc4	centrum
pro	pro	k7c4	pro
teoretická	teoretický	k2eAgNnPc4d1	teoretické
studia	studio	k1gNnPc4	studio
(	(	kIx(	(
<g/>
CTS	CTS	kA	CTS
<g/>
)	)	kIx)	)
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
doktorské	doktorský	k2eAgNnSc4d1	doktorské
studium	studium	k1gNnSc4	studium
CERGE-EI	CERGE-EI	k1gFnSc2	CERGE-EI
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
otázky	otázka	k1gFnPc4	otázka
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
Ústav	ústav	k1gInSc1	ústav
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
poznatků	poznatek	k1gInPc2	poznatek
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
Ústav	ústava	k1gFnPc2	ústava
jazykové	jazykový	k2eAgFnSc2d1	jazyková
a	a	k8xC	a
odborné	odborný	k2eAgFnSc2d1	odborná
přípravy	příprava	k1gFnSc2	příprava
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
knihovna	knihovna	k1gFnSc1	knihovna
Agentura	agentura	k1gFnSc1	agentura
Rady	rada	k1gFnSc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
Koleje	kolej	k1gFnSc2	kolej
a	a	k8xC	a
menzy	menza	k1gFnSc2	menza
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
seminář	seminář	k1gInSc1	seminář
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Karolinum	Karolinum	k1gNnSc1	Karolinum
Správa	správa	k1gFnSc1	správa
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
zařízení	zařízení	k1gNnSc2	zařízení
Sportovní	sportovní	k2eAgFnSc6d1	sportovní
centrum	centrum	k1gNnSc1	centrum
</s>
