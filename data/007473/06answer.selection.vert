<s>
Zoo	zoo	k1gFnSc1	zoo
Ostrava	Ostrava	k1gFnSc1	Ostrava
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
-	-	kIx~	-
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Orel	Orel	k1gMnSc1	Orel
mořský	mořský	k2eAgMnSc1d1	mořský
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
heraldickým	heraldický	k2eAgNnSc7d1	heraldické
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
i	i	k8xC	i
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
