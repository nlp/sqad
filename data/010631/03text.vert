<p>
<s>
Polštář	polštář	k1gInSc1	polštář
(	(	kIx(	(
<g/>
moravsky	moravsky	k6eAd1	moravsky
zhlavec	zhlavec	k1gInSc1	zhlavec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
účelové	účelový	k2eAgNnSc1d1	účelové
a	a	k8xC	a
dekorativní	dekorativní	k2eAgNnSc1d1	dekorativní
vybavení	vybavení	k1gNnSc1	vybavení
obytných	obytný	k2eAgInPc2d1	obytný
prostorů	prostor	k1gInPc2	prostor
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
posteli	postel	k1gFnSc6	postel
jako	jako	k8xC	jako
podložka	podložka	k1gFnSc1	podložka
pod	pod	k7c4	pod
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Použití	použití	k1gNnSc1	použití
polštáře	polštář	k1gInSc2	polštář
při	při	k7c6	při
spánku	spánek	k1gInSc6	spánek
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
k	k	k7c3	k
vyvýšení	vyvýšení	k1gNnSc3	vyvýšení
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
efekt	efekt	k1gInSc4	efekt
narovnání	narovnání	k1gNnSc2	narovnání
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
krční	krční	k2eAgFnSc2d1	krční
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
odpočine	odpočinout	k5eAaPmIp3nS	odpočinout
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
regenerovat	regenerovat	k5eAaBmF	regenerovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nepoužití	nepoužití	k1gNnSc6	nepoužití
polštáře	polštář	k1gInSc2	polštář
se	se	k3xPyFc4	se
krk	krk	k1gInSc1	krk
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
do	do	k7c2	do
nepřirozené	přirozený	k2eNgFnSc2d1	nepřirozená
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
enormně	enormně	k6eAd1	enormně
namáhá	namáhat	k5eAaImIp3nS	namáhat
krční	krční	k2eAgInPc4d1	krční
svaly	sval	k1gInPc4	sval
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
výrazné	výrazný	k2eAgFnPc4d1	výrazná
bolesti	bolest	k1gFnPc4	bolest
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Správné	správný	k2eAgNnSc1d1	správné
vyrovnání	vyrovnání	k1gNnSc1	vyrovnání
páteře	páteř	k1gFnSc2	páteř
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
dalšími	další	k2eAgInPc7d1	další
záležitostí	záležitost	k1gFnSc7	záležitost
vhodně	vhodně	k6eAd1	vhodně
zvoleného	zvolený	k2eAgInSc2d1	zvolený
polštáře	polštář	k1gInSc2	polštář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polštáře	polštář	k1gInPc1	polštář
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
většinou	většinou	k6eAd1	většinou
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
–	–	k?	–
obalu	obal	k1gInSc2	obal
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
náplně	náplň	k1gFnSc2	náplň
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
opatřeny	opatřit	k5eAaPmNgInP	opatřit
povlečením	povlečení	k1gNnSc7	povlečení
<g/>
.	.	kIx.	.
</s>
<s>
Obal	obal	k1gInSc1	obal
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
pevný	pevný	k2eAgInSc1d1	pevný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vydržel	vydržet	k5eAaPmAgMnS	vydržet
i	i	k9	i
hrubší	hrubý	k2eAgNnSc4d2	hrubší
zacházení	zacházení	k1gNnSc4	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Náplň	náplň	k1gFnSc1	náplň
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
ptačího	ptačí	k2eAgNnSc2d1	ptačí
peří	peří	k1gNnSc2	peří
<g/>
.	.	kIx.	.
</s>
<s>
Peřový	peřový	k2eAgInSc1d1	peřový
polštář	polštář	k1gInSc1	polštář
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
majetnější	majetný	k2eAgFnSc4d2	majetnější
část	část	k1gFnSc4	část
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
chudší	chudý	k2eAgFnSc1d2	chudší
část	část	k1gFnSc1	část
si	se	k3xPyFc3	se
vycpávala	vycpávat	k5eAaImAgFnS	vycpávat
polštář	polštář	k1gInSc4	polštář
slámou	sláma	k1gFnSc7	sláma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
náplní	náplň	k1gFnPc2	náplň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
i	i	k9	i
pro	pro	k7c4	pro
alergiky	alergik	k1gMnPc4	alergik
a	a	k8xC	a
astmatiky	astmatik	k1gMnPc4	astmatik
(	(	kIx(	(
<g/>
takové	takový	k3xDgInPc4	takový
polštáře	polštář	k1gInPc4	polštář
nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc1	žádný
prach	prach	k1gInSc1	prach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
krk	krk	k1gInSc1	krk
</s>
</p>
<p>
<s>
peřina	peřina	k1gFnSc1	peřina
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
polštář	polštář	k1gInSc1	polštář
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
polštář	polštář	k1gInSc1	polštář
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
