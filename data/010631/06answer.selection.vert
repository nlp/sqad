<s>
Polštář	polštář	k1gInSc1	polštář
(	(	kIx(	(
<g/>
moravsky	moravsky	k6eAd1	moravsky
zhlavec	zhlavec	k1gInSc1	zhlavec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
účelové	účelový	k2eAgNnSc1d1	účelové
a	a	k8xC	a
dekorativní	dekorativní	k2eAgNnSc1d1	dekorativní
vybavení	vybavení	k1gNnSc1	vybavení
obytných	obytný	k2eAgInPc2d1	obytný
prostorů	prostor	k1gInPc2	prostor
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
posteli	postel	k1gFnSc6	postel
jako	jako	k8xS	jako
podložka	podložka	k1gFnSc1	podložka
pod	pod	k7c4	pod
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
