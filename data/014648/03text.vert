<s>
Služba	služba	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
hospodářské	hospodářský	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
uspokojující	uspokojující	k2eAgFnSc4d1
lidskou	lidský	k2eAgFnSc4d1
potřebu	potřeba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Služba	služba	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Služba	služba	k1gFnSc1
je	být	k5eAaImIp3nS
hospodářská	hospodářský	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
uspokojující	uspokojující	k2eAgFnSc4d1
určitou	určitý	k2eAgFnSc4d1
potřebu	potřeba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služby	služba	k1gFnPc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
rozlišují	rozlišovat	k5eAaImIp3nP
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
uspokojují	uspokojovat	k5eAaImIp3nP
potřeby	potřeba	k1gFnPc4
kolektivní	kolektivní	k2eAgFnSc2d1
nebo	nebo	k8xC
individuální	individuální	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služby	služba	k1gFnPc4
uspokojující	uspokojující	k2eAgFnSc2d1
kolektivní	kolektivní	k2eAgFnSc2d1
potřeby	potřeba	k1gFnSc2
jsou	být	k5eAaImIp3nP
hrazeny	hradit	k5eAaImNgInP
z	z	k7c2
veřejných	veřejný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
(	(	kIx(
<g/>
stát	stát	k1gInSc1
<g/>
,	,	kIx,
obce	obec	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
služby	služba	k1gFnPc1
uspokojující	uspokojující	k2eAgFnSc2d1
individuální	individuální	k2eAgFnSc2d1
potřeby	potřeba	k1gFnSc2
jsou	být	k5eAaImIp3nP
hrazeny	hradit	k5eAaImNgInP
ze	z	k7c2
soukromých	soukromý	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělí	dělit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
základní	základní	k2eAgNnSc4d1
a	a	k8xC
doplňkové	doplňkový	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
služeb	služba	k1gFnPc2
definovanou	definovaný	k2eAgFnSc7d1
zákonem	zákon	k1gInSc7
č.	č.	k?
67	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
jsou	být	k5eAaImIp3nP
služby	služba	k1gFnPc1
spojené	spojený	k2eAgFnPc1d1
s	s	k7c7
užíváním	užívání	k1gNnSc7
bytů	byt	k1gInPc2
(	(	kIx(
<g/>
zejména	zejména	k9
dodávka	dodávka	k1gFnSc1
tepla	teplo	k1gNnSc2
a	a	k8xC
centralizované	centralizovaný	k2eAgNnSc4d1
poskytování	poskytování	k1gNnSc4
teplé	teplý	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
dodávka	dodávka	k1gFnSc1
vody	voda	k1gFnSc2
a	a	k8xC
odvádění	odvádění	k1gNnSc2
odpadních	odpadní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
provoz	provoz	k1gInSc4
výtahu	výtah	k1gInSc2
<g/>
,	,	kIx,
osvětlení	osvětlení	k1gNnSc1
společných	společný	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
v	v	k7c6
domě	dům	k1gInSc6
<g/>
,	,	kIx,
úklid	úklid	k1gInSc4
společných	společný	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
v	v	k7c6
domě	dům	k1gInSc6
<g/>
,	,	kIx,
odvoz	odvoz	k1gInSc1
odpadních	odpadní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
a	a	k8xC
čištění	čištění	k1gNnSc2
jímek	jímka	k1gFnPc2
<g/>
,	,	kIx,
umožnění	umožnění	k1gNnSc1
příjmu	příjem	k1gInSc2
rozhlasového	rozhlasový	k2eAgInSc2d1
a	a	k8xC
televizního	televizní	k2eAgInSc2d1
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
provoz	provoz	k1gInSc1
a	a	k8xC
čištění	čištění	k1gNnSc1
komínů	komín	k1gInPc2
a	a	k8xC
odvoz	odvoz	k1gInSc4
komunálního	komunální	k2eAgInSc2d1
odpadu	odpad	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
neskladovatelnost	neskladovatelnost	k1gFnSc1
–	–	k?
služby	služba	k1gFnSc2
nelze	lze	k6eNd1
vyrábět	vyrábět	k5eAaImF
do	do	k7c2
zásoby	zásoba	k1gFnSc2
</s>
<s>
nedělitelnost	nedělitelnost	k1gFnSc1
–	–	k?
poskytnuté	poskytnutý	k2eAgFnPc4d1
služby	služba	k1gFnPc4
nelze	lze	k6eNd1
nijak	nijak	k6eAd1
dělit	dělit	k5eAaImF
</s>
<s>
nehmotnost	nehmotnost	k1gFnSc1
–	–	k?
služby	služba	k1gFnSc2
nemají	mít	k5eNaImIp3nP
hmotnou	hmotný	k2eAgFnSc4d1
(	(	kIx(
<g/>
fyzickou	fyzický	k2eAgFnSc4d1
<g/>
)	)	kIx)
podstatu	podstata	k1gFnSc4
</s>
<s>
proměnlivost	proměnlivost	k1gFnSc1
–	–	k?
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
a	a	k8xC
kde	kde	k6eAd1
je	on	k3xPp3gInPc4
poskytuje	poskytovat	k5eAaImIp3nS
</s>
<s>
nemožnost	nemožnost	k1gFnSc1
vlastnictví	vlastnictví	k1gNnSc2
–	–	k?
zákazník	zákazník	k1gMnSc1
vlastní	vlastní	k2eAgNnSc4d1
pouze	pouze	k6eAd1
právo	právo	k1gNnSc4
na	na	k7c4
poskytnutí	poskytnutí	k1gNnSc4
služby	služba	k1gFnSc2
</s>
<s>
Dělení	dělení	k1gNnSc1
</s>
<s>
kolektivní	kolektivní	k2eAgFnSc1d1
–	–	k?
obrana	obrana	k1gFnSc1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
justice	justice	k1gFnSc2
<g/>
,	,	kIx,
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
osvětlení	osvětlení	k1gNnSc2
ulic	ulice	k1gFnPc2
apod.	apod.	kA
</s>
<s>
individuální	individuální	k2eAgMnSc1d1
–	–	k?
cestovní	cestovní	k2eAgInSc1d1
ruch	ruch	k1gInSc1
<g/>
,	,	kIx,
kadeřnictví	kadeřnictví	k1gNnSc1
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgNnSc1d1
stravování	stravování	k1gNnSc1
<g/>
,	,	kIx,
čistírny	čistírna	k1gFnPc1
apod.	apod.	kA
</s>
<s>
Jiné	jiný	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
</s>
<s>
věcné	věcný	k2eAgFnPc4d1
–	–	k?
práce	práce	k1gFnSc2
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
obnovením	obnovení	k1gNnSc7
funkce	funkce	k1gFnSc2
výrobků	výrobek	k1gInPc2
(	(	kIx(
<g/>
opravny	opravna	k1gFnPc1
<g/>
,	,	kIx,
čistírny	čistírna	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
osobní	osobní	k2eAgInSc1d1
–	–	k?
obohacují	obohacovat	k5eAaImIp3nP
duševní	duševní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
člověka	člověk	k1gMnSc2
(	(	kIx(
<g/>
knihovny	knihovna	k1gFnPc1
<g/>
,	,	kIx,
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
zdravotní	zdravotní	k2eAgFnSc1d1
péče	péče	k1gFnSc1
<g/>
…	…	k?
<g/>
)	)	kIx)
</s>
<s>
obchodní	obchodní	k2eAgInPc1d1
–	–	k?
maloobchody	maloobchod	k1gInPc1
<g/>
,	,	kIx,
velkoobchody	velkoobchod	k1gInPc1
<g/>
,	,	kIx,
banky	banka	k1gFnPc1
</s>
<s>
Podle	podle	k7c2
dostupnosti	dostupnost	k1gFnSc2
</s>
<s>
placené	placený	k2eAgNnSc1d1
–	–	k?
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc1
dostupné	dostupný	k2eAgFnPc1d1
</s>
<s>
neplacené	placený	k2eNgFnPc4d1
–	–	k?
informace	informace	k1gFnPc4
v	v	k7c6
infocentru	infocentrum	k1gNnSc6
<g/>
,	,	kIx,
poradenství	poradenství	k1gNnSc6
a	a	k8xC
konzultace	konzultace	k1gFnPc4
</s>
<s>
Logistika	logistika	k1gFnSc1
definuje	definovat	k5eAaBmIp3nS
logistické	logistický	k2eAgInPc4d1
cíle	cíl	k1gInPc4
<g/>
,	,	kIx,
logistické	logistický	k2eAgInPc4d1
výkony	výkon	k1gInPc4
a	a	k8xC
logistické	logistický	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Preclík	preclík	k1gInSc1
Vratislav	Vratislav	k1gFnSc1
<g/>
:	:	kIx,
Průmyslová	průmyslový	k2eAgFnSc1d1
logistika	logistika	k1gFnSc1
<g/>
,	,	kIx,
116	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2139	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
Vydavatelství	vydavatelství	k1gNnSc1
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
2000	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
služba	služba	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
Služba	služba	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
služba	služba	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mikroekonomie	mikroekonomie	k1gFnPc1
Hlavní	hlavní	k2eAgFnPc1d1
témata	téma	k1gNnPc4
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
</s>
<s>
Dokonalá	dokonalý	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
•	•	k?
Nedokonalá	dokonalý	k2eNgFnSc1d1
konkurence	konkurence	k1gFnSc1
•	•	k?
Monopolistická	monopolistický	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
Náklady	náklad	k1gInPc1
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1
•	•	k?
Mezní	mezní	k2eAgFnPc1d1
•	•	k?
Obětované	obětovaný	k2eAgFnPc1d1
příležitosti	příležitost	k1gFnPc1
•	•	k?
Sociální	sociální	k2eAgInSc4d1
•	•	k?
Utopené	utopený	k2eAgNnSc1d1
•	•	k?
Transakční	transakční	k2eAgInSc1d1
•	•	k?
Fixní	fixní	k2eAgInSc1d1
•	•	k?
Variabilní	variabilní	k2eAgInSc4d1
•	•	k?
Celkové	celkový	k2eAgNnSc1d1
Struktura	struktura	k1gFnSc1
trhu	trh	k1gInSc6
</s>
<s>
Monopol	monopol	k1gInSc1
•	•	k?
Monopson	Monopson	k1gInSc1
•	•	k?
Oligopol	Oligopol	k1gInSc1
(	(	kIx(
<g/>
duopol	duopol	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Oligopson	Oligopson	k1gInSc1
•	•	k?
Přirozený	přirozený	k2eAgInSc1d1
monopol	monopol	k1gInSc1
•	•	k?
Koncern	koncern	k1gInSc1
(	(	kIx(
<g/>
holding	holding	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Trust	trust	k1gInSc1
•	•	k?
Kartel	kartel	k1gInSc1
•	•	k?
Syndikát	syndikát	k1gInSc1
•	•	k?
Konsorcium	konsorcium	k1gNnSc1
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Agregace	agregace	k1gFnSc1
•	•	k?
Rozpočet	rozpočet	k1gInSc1
•	•	k?
Teorie	teorie	k1gFnSc2
spotřebitele	spotřebitel	k1gMnSc2
•	•	k?
Konvexnost	konvexnost	k1gFnSc4
•	•	k?
Nekonvexnost	Nekonvexnost	k1gFnSc1
•	•	k?
Analýza	analýza	k1gFnSc1
nákladů	náklad	k1gInPc2
a	a	k8xC
přínosů	přínos	k1gInPc2
•	•	k?
Náklady	náklad	k1gInPc1
mrtvé	mrtvý	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
•	•	k?
Distribuce	distribuce	k1gFnSc2
•	•	k?
Úspory	úspora	k1gFnSc2
z	z	k7c2
rozsahu	rozsah	k1gInSc2
•	•	k?
Úspory	úspora	k1gFnSc2
z	z	k7c2
prostoru	prostor	k1gInSc2
•	•	k?
Elasticita	elasticita	k1gFnSc1
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
rovnováha	rovnováha	k1gFnSc1
•	•	k?
Obchod	obchod	k1gInSc1
•	•	k?
Externalita	externalita	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
firmy	firma	k1gFnSc2
•	•	k?
Statek	statek	k1gInSc1
•	•	k?
Služba	služba	k1gFnSc1
•	•	k?
Rodinná	rodinný	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Křivka	křivka	k1gFnSc1
příjmu	příjem	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
spotřeby	spotřeba	k1gFnSc2
•	•	k?
Informace	informace	k1gFnPc1
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
•	•	k?
Indiferenční	Indiferenční	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Mezičasová	Mezičasový	k2eAgFnSc1d1
volba	volba	k1gFnSc1
•	•	k?
Trh	trh	k1gInSc1
(	(	kIx(
<g/>
ekonomie	ekonomie	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Selhání	selhání	k1gNnSc1
trhu	trh	k1gInSc2
•	•	k?
Paretovo	Paretův	k2eAgNnSc1d1
optimum	optimum	k1gNnSc1
•	•	k?
Preference	preference	k1gFnSc1
•	•	k?
Cena	cena	k1gFnSc1
•	•	k?
Produkce	produkce	k1gFnSc1
•	•	k?
Zisk	zisk	k1gInSc1
•	•	k?
Veřejný	veřejný	k2eAgInSc1d1
statek	statek	k1gInSc1
•	•	k?
Přídělový	přídělový	k2eAgInSc1d1
systém	systém	k1gInSc1
•	•	k?
Renta	renta	k1gFnSc1
•	•	k?
Averze	averze	k1gFnSc2
k	k	k7c3
riziku	riziko	k1gNnSc3
•	•	k?
Vzácnost	vzácnost	k1gFnSc1
•	•	k?
Nedostatek	nedostatek	k1gInSc1
•	•	k?
Substitut	substitut	k1gInSc1
•	•	k?
Substitutuční	Substitutuční	k2eAgInSc1d1
efekt	efekt	k1gInSc1
•	•	k?
Přebytek	přebytek	k1gInSc1
•	•	k?
Sociální	sociální	k2eAgFnSc1d1
volba	volba	k1gFnSc1
•	•	k?
Nabídka	nabídka	k1gFnSc1
a	a	k8xC
poptávka	poptávka	k1gFnSc1
•	•	k?
Nejistota	nejistota	k1gFnSc1
•	•	k?
Užitek	užitek	k1gInSc1
<g/>
(	(	kIx(
Očekávaný	očekávaný	k2eAgInSc1d1
<g/>
,	,	kIx,
Mezní	mezní	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Mzda	mzda	k1gFnSc1
</s>
<s>
Vedlejší	vedlejší	k2eAgNnPc1d1
témata	téma	k1gNnPc1
</s>
<s>
Behaviorální	behaviorální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
rozhodování	rozhodování	k1gNnSc2
•	•	k?
Ekonometrie	Ekonometrie	k1gFnSc2
•	•	k?
Evoluční	evoluční	k2eAgInSc4d1
přístup	přístup	k1gInSc4
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
•	•	k?
Eperimentální	Eperimentální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
her	hra	k1gFnPc2
•	•	k?
Institucionální	institucionální	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
•	•	k?
Ekonomie	ekonomie	k1gFnSc2
práce	práce	k1gFnSc2
•	•	k?
Právo	právo	k1gNnSc1
•	•	k?
Operační	operační	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
•	•	k?
Optimalizace	optimalizace	k1gFnSc1
•	•	k?
Blahobyt	blahobyt	k1gInSc1
•	•	k?
Regulace	regulace	k1gFnSc1
cen	cena	k1gFnPc2
•	•	k?
Ekonomika	ekonomik	k1gMnSc2
Robinsona	Robinson	k1gMnSc2
Crusoe	Crusoe	k1gNnSc2
•	•	k?
Engelova	Engelův	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Hranice	hranice	k1gFnSc2
produkčních	produkční	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
•	•	k?
Zákon	zákon	k1gInSc1
klesajících	klesající	k2eAgInPc2d1
výnosů	výnos	k1gInPc2
•	•	k?
Udržitelný	udržitelný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
K	k	k7c3
vidění	vidění	k1gNnSc3
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Makroekonomie	makroekonomie	k1gFnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4012178-1	4012178-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
1389	#num#	k4
</s>
