<s>
Chloroform	chloroform	k1gInSc1	chloroform
neboli	neboli	k8xC	neboli
trichlormethan	trichlormethan	k1gInSc1	trichlormethan
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
chemii	chemie	k1gFnSc4	chemie
dle	dle	k7c2	dle
PČP	PČP	kA	PČP
trichlormetan	trichlormetan	k1gInSc1	trichlormetan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
těkavá	těkavý	k2eAgFnSc1d1	těkavá
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nehořlavá	hořlavý	k2eNgFnSc1d1	nehořlavá
kapalina	kapalina	k1gFnSc1	kapalina
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
nasládlého	nasládlý	k2eAgInSc2d1	nasládlý
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
4,12	[number]	k4	4,12
<g/>
krát	krát	k6eAd1	krát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
prostorách	prostora	k1gFnPc6	prostora
hromadí	hromadit	k5eAaImIp3nP	hromadit
u	u	k7c2	u
podlahy	podlaha	k1gFnSc2	podlaha
a	a	k8xC	a
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
vyvětrává	vyvětrávat	k5eAaImIp3nS	vyvětrávat
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
chloroform	chloroform	k1gInSc1	chloroform
připraven	připravit	k5eAaPmNgInS	připravit
Liebenovou	Liebenový	k2eAgFnSc7d1	Liebenový
reakcí	reakce	k1gFnSc7	reakce
(	(	kIx(	(
<g/>
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
haloformní	haloformní	k2eAgFnSc1d1	haloformní
reakce	reakce	k1gFnSc1	reakce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
působením	působení	k1gNnSc7	působení
chlornanu	chlornan	k1gInSc2	chlornan
vápenatého	vápenatý	k2eAgNnSc2d1	vápenaté
(	(	kIx(	(
<g/>
chlorového	chlorový	k2eAgNnSc2d1	chlorové
vápna	vápno	k1gNnSc2	vápno
<g/>
)	)	kIx)	)
na	na	k7c4	na
ethanol	ethanol	k1gInSc4	ethanol
2	[number]	k4	2
CH3CH2OH	CH3CH2OH	k1gFnPc2	CH3CH2OH
+	+	kIx~	+
4	[number]	k4	4
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
ClO	clo	k1gNnSc1	clo
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
→	→	k?	→
2	[number]	k4	2
CHCl	CHCl	k1gInSc1	CHCl
<g/>
3	[number]	k4	3
+	+	kIx~	+
(	(	kIx(	(
<g/>
HCOO	HCOO	kA	HCOO
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
Ca	ca	kA	ca
+	+	kIx~	+
CaCl	CaCl	k1gInSc1	CaCl
<g/>
2	[number]	k4	2
+	+	kIx~	+
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
2	[number]	k4	2
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
vznikají	vznikat	k5eAaImIp3nP	vznikat
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
produkty	produkt	k1gInPc4	produkt
mravenčan	mravenčan	k1gInSc1	mravenčan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc1	chlorid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
,	,	kIx,	,
hydroxid	hydroxid	k1gInSc1	hydroxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
chloroform	chloroform	k1gInSc4	chloroform
též	též	k9	též
působením	působení	k1gNnSc7	působení
chlornanu	chlornan	k1gInSc2	chlornan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
na	na	k7c4	na
acetaldehyd	acetaldehyd	k1gInSc4	acetaldehyd
nebo	nebo	k8xC	nebo
aceton	aceton	k1gInSc4	aceton
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
místo	místo	k7c2	místo
chlornanu	chlornan	k1gInSc2	chlornan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
použit	použít	k5eAaPmNgInS	použít
též	též	k9	též
chlornan	chlornan	k1gInSc1	chlornan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
chloroform	chloroform	k1gInSc1	chloroform
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
zahříváním	zahřívání	k1gNnSc7	zahřívání
směsi	směs	k1gFnSc2	směs
chloru	chlor	k1gInSc2	chlor
s	s	k7c7	s
chlormethanem	chlormethan	k1gInSc7	chlormethan
nebo	nebo	k8xC	nebo
methanem	methan	k1gInSc7	methan
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
teplotě	teplota	k1gFnSc6	teplota
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
radikál	radikál	k1gInSc1	radikál
chlóru	chlór	k1gInSc2	chlór
(	(	kIx(	(
<g/>
podrobnosti	podrobnost	k1gFnPc1	podrobnost
viz	vidět	k5eAaImRp2nS	vidět
příprava	příprava	k1gFnSc1	příprava
tetrachlormethanu	tetrachlormethan	k1gInSc2	tetrachlormethan
<g/>
)	)	kIx)	)
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
nahradí	nahradit	k5eAaPmIp3nS	nahradit
v	v	k7c6	v
methanu	methan	k1gInSc6	methan
nebo	nebo	k8xC	nebo
chlormethanu	chlormethanout	k5eAaPmIp1nS	chlormethanout
vodíkové	vodíkový	k2eAgInPc4d1	vodíkový
atomy	atom	k1gInPc4	atom
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
<g />
.	.	kIx.	.
</s>
<s>
směs	směs	k1gFnSc4	směs
čtyř	čtyři	k4xCgInPc2	čtyři
chlorovaných	chlorovaný	k2eAgInPc2d1	chlorovaný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
(	(	kIx(	(
<g/>
chlormethan	chlormethan	k1gMnSc1	chlormethan
<g/>
,	,	kIx,	,
dichlormethan	dichlormethan	k1gMnSc1	dichlormethan
<g/>
,	,	kIx,	,
trichlormethan	trichlormethan	k1gMnSc1	trichlormethan
(	(	kIx(	(
<g/>
chloroform	chloroform	k1gInSc1	chloroform
<g/>
)	)	kIx)	)
a	a	k8xC	a
tetrachlormethan	tetrachlormethan	k1gInSc1	tetrachlormethan
<g/>
)	)	kIx)	)
CH4	CH4	k1gFnSc1	CH4
+	+	kIx~	+
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
→	→	k?	→
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
Cl	Cl	k1gFnPc1	Cl
+	+	kIx~	+
HCl	HCl	k1gFnPc1	HCl
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
Cl	Cl	k1gFnPc1	Cl
+	+	kIx~	+
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
→	→	k?	→
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
+	+	kIx~	+
HCl	HCl	k1gFnSc2	HCl
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
<g/>
2	[number]	k4	2
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
+	+	kIx~	+
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
→	→	k?	→
CHCl	CHCl	k1gInSc1	CHCl
<g/>
3	[number]	k4	3
+	+	kIx~	+
HCl	HCl	k1gFnSc1	HCl
<g/>
,	,	kIx,	,
CHCl	CHCl	k1gInSc1	CHCl
<g/>
3	[number]	k4	3
+	+	kIx~	+
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
→	→	k?	→
CCl	CCl	k1gMnSc1	CCl
<g/>
4	[number]	k4	4
+	+	kIx~	+
HCl	HCl	k1gFnSc2	HCl
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
směs	směs	k1gFnSc1	směs
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
destilací	destilace	k1gFnSc7	destilace
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
čisté	čistý	k2eAgFnPc4d1	čistá
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
chloroformu	chloroform	k1gInSc2	chloroform
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
trojčetnou	trojčetný	k2eAgFnSc4d1	trojčetná
osu	osa	k1gFnSc4	osa
symetrie	symetrie	k1gFnSc2	symetrie
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
roviny	rovina	k1gFnPc4	rovina
symetrie	symetrie	k1gFnSc2	symetrie
(	(	kIx(	(
<g/>
bodová	bodový	k2eAgFnSc1d1	bodová
grupa	grupa	k1gFnSc1	grupa
symetrie	symetrie	k1gFnSc2	symetrie
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
v	v	k7c4	v
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
střed	střed	k1gInSc4	střed
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
polaritě	polarita	k1gFnSc3	polarita
vazeb	vazba	k1gFnPc2	vazba
C	C	kA	C
<g/>
–	–	k?	–
<g/>
Cl	Cl	k1gFnSc1	Cl
celá	celý	k2eAgFnSc1d1	celá
molekula	molekula	k1gFnSc1	molekula
polární	polární	k2eAgFnSc1d1	polární
(	(	kIx(	(
<g/>
dipólový	dipólový	k2eAgInSc4d1	dipólový
moment	moment	k1gInSc4	moment
1,08	[number]	k4	1,08
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
chloroform	chloroform	k1gInSc1	chloroform
velmi	velmi	k6eAd1	velmi
dobrým	dobrý	k2eAgNnSc7d1	dobré
rozpouštědlem	rozpouštědlo	k1gNnSc7	rozpouštědlo
zejména	zejména	k9	zejména
tuků	tuk	k1gInPc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
polarita	polarita	k1gFnSc1	polarita
vazeb	vazba	k1gFnPc2	vazba
C	C	kA	C
<g/>
–	–	k?	–
<g/>
Cl	Cl	k1gMnSc1	Cl
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
atom	atom	k1gInSc4	atom
molekuly	molekula	k1gFnSc2	molekula
chloroformu	chloroform	k1gInSc2	chloroform
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
vodík	vodík	k1gInSc1	vodík
snadno	snadno	k6eAd1	snadno
atakuje	atakovat	k5eAaBmIp3nS	atakovat
oxidačními	oxidační	k2eAgNnPc7d1	oxidační
činidly	činidlo	k1gNnPc7	činidlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
pouhým	pouhý	k2eAgInSc7d1	pouhý
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
za	za	k7c2	za
současného	současný	k2eAgNnSc2d1	současné
působení	působení	k1gNnSc2	působení
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzniká	vznikat	k5eAaImIp3nS	vznikat
jedovatý	jedovatý	k2eAgInSc4d1	jedovatý
fosgen	fosgen	k1gInSc4	fosgen
<g/>
;	;	kIx,	;
jako	jako	k8xC	jako
meziprodukt	meziprodukt	k1gInSc1	meziprodukt
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nestabilní	stabilní	k2eNgInSc1d1	nestabilní
trichlormethanol	trichlormethanol	k1gInSc1	trichlormethanol
2	[number]	k4	2
CHCl	CHCla	k1gFnPc2	CHCla
<g/>
3	[number]	k4	3
→	→	k?	→
[	[	kIx(	[
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
3	[number]	k4	3
<g/>
COH	COH	kA	COH
<g/>
]	]	kIx)	]
→	→	k?	→
HCl	HCl	k1gFnSc1	HCl
+	+	kIx~	+
COCl	COCl	k1gInSc1	COCl
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
silných	silný	k2eAgInPc2d1	silný
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
,	,	kIx,	,
např.	např.	kA	např.
roztoku	roztok	k1gInSc2	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
se	se	k3xPyFc4	se
odštěpují	odštěpovat	k5eAaImIp3nP	odštěpovat
z	z	k7c2	z
molekuly	molekula	k1gFnSc2	molekula
chloroformu	chloroform	k1gInSc2	chloroform
atomy	atom	k1gInPc7	atom
chloru	chlor	k1gInSc2	chlor
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyselina	kyselina	k1gFnSc1	kyselina
mravenčí	mravenčí	k2eAgFnSc1d1	mravenčí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
neutralizuje	neutralizovat	k5eAaBmIp3nS	neutralizovat
na	na	k7c4	na
mravenčan	mravenčan	k1gInSc4	mravenčan
sodný	sodný	k2eAgInSc4d1	sodný
CHCl	CHCl	k1gInSc4	CHCl
<g/>
3	[number]	k4	3
+	+	kIx~	+
4	[number]	k4	4
NaOH	NaOH	k1gFnSc2	NaOH
→	→	k?	→
2	[number]	k4	2
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
3	[number]	k4	3
NaCl	NaCla	k1gFnPc2	NaCla
+	+	kIx~	+
HCOONa	HCOONa	k1gFnSc1	HCOONa
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgNnSc7d1	současné
působením	působení	k1gNnSc7	působení
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
a	a	k8xC	a
amoniaku	amoniak	k1gInSc2	amoniak
vzniká	vznikat	k5eAaImIp3nS	vznikat
kyanid	kyanid	k1gInSc1	kyanid
draselný	draselný	k2eAgInSc1d1	draselný
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
CHCl	CHClum	k1gNnPc2	CHClum
<g/>
3	[number]	k4	3
+	+	kIx~	+
NH3	NH3	k1gFnSc2	NH3
+	+	kIx~	+
4	[number]	k4	4
KOH	KOH	kA	KOH
→	→	k?	→
3	[number]	k4	3
KCl	KCl	k1gFnPc2	KCl
+	+	kIx~	+
4	[number]	k4	4
H2O	H2O	k1gFnPc2	H2O
+	+	kIx~	+
KCN	KCN	kA	KCN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásaditém	zásaditý	k2eAgNnSc6d1	zásadité
prostředí	prostředí	k1gNnSc6	prostředí
reaguje	reagovat	k5eAaBmIp3nS	reagovat
chloroform	chloroform	k1gInSc1	chloroform
s	s	k7c7	s
aldehydy	aldehyd	k1gInPc7	aldehyd
a	a	k8xC	a
ketony	keton	k1gInPc7	keton
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
resp.	resp.	kA	resp.
terciárních	terciární	k2eAgInPc2d1	terciární
alkoholů	alkohol	k1gInPc2	alkohol
s	s	k7c7	s
koncovou	koncový	k2eAgFnSc7d1	koncová
trichlormethylovou	trichlormethylový	k2eAgFnSc7d1	trichlormethylový
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
s	s	k7c7	s
acetonem	aceton	k1gInSc7	aceton
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
1,1	[number]	k4	1,1
<g/>
,1	,1	k4	,1
<g/>
-trichlor-	richlor-	k?	-trichlor-
<g/>
2	[number]	k4	2
<g/>
-methyl-propan-	ethylropan-	k?	-methyl-propan-
<g/>
2	[number]	k4	2
<g/>
-ol	l	k?	-ol
CH3COCH3	CH3COCH3	k1gMnSc1	CH3COCH3
+	+	kIx~	+
CHCl	CHCl	k1gInSc1	CHCl
<g/>
3	[number]	k4	3
→	→	k?	→
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
C	C	kA	C
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
CCl	CCl	k1gMnSc1	CCl
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
fluoridů	fluorid	k1gInPc2	fluorid
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
fluoridu	fluorid	k1gInSc2	fluorid
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
nebo	nebo	k8xC	nebo
fluoridu	fluorid	k1gInSc2	fluorid
antimonitého	antimonitý	k2eAgInSc2d1	antimonitý
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
nebo	nebo	k8xC	nebo
úplné	úplný	k2eAgFnSc3d1	úplná
výměně	výměna	k1gFnSc3	výměna
atomů	atom	k1gInPc2	atom
chloru	chlor	k1gInSc2	chlor
atomy	atom	k1gInPc7	atom
fluoru	fluor	k1gInSc2	fluor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
CHCl	CHCl	k1gInSc4	CHCl
<g/>
3	[number]	k4	3
+	+	kIx~	+
SbF	SbF	k1gFnSc1	SbF
<g/>
3	[number]	k4	3
→	→	k?	→
CHF3	CHF3	k1gMnSc1	CHF3
+	+	kIx~	+
SbCl	SbCl	k1gMnSc1	SbCl
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
chloroformu	chloroform	k1gInSc2	chloroform
na	na	k7c4	na
fenoly	fenol	k1gInPc4	fenol
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
silných	silný	k2eAgInPc2d1	silný
anorganických	anorganický	k2eAgInPc2d1	anorganický
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
aromatické	aromatický	k2eAgInPc1d1	aromatický
hydroxyaldehydy	hydroxyaldehyd	k1gInPc1	hydroxyaldehyd
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
aldehydická	aldehydický	k2eAgFnSc1d1	aldehydická
skupina	skupina	k1gFnSc1	skupina
–	–	k?	–
<g/>
CHO	cho	k0	cho
vnáší	vnášet	k5eAaImIp3nP	vnášet
především	především	k9	především
do	do	k7c2	do
orto	orto	k6eAd1	orto
polohy	poloha	k1gFnPc1	poloha
vůči	vůči	k7c3	vůči
fenolickému	fenolický	k2eAgInSc3d1	fenolický
hydroxylu	hydroxyl	k1gInSc3	hydroxyl
<g/>
;	;	kIx,	;
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
para	para	k2eAgInPc1d1	para
deriváty	derivát	k1gInPc1	derivát
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
svých	svůj	k3xOyFgMnPc2	svůj
objevitelů	objevitel	k1gMnPc2	objevitel
nazývána	nazývat	k5eAaImNgFnS	nazývat
Reimerova-Tiemannova	Reimerova-Tiemannův	k2eAgFnSc1d1	Reimerova-Tiemannův
syntéza	syntéza	k1gFnSc1	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
např.	např.	kA	např.
z	z	k7c2	z
fenolu	fenol	k1gInSc2	fenol
vzniká	vznikat	k5eAaImIp3nS	vznikat
směs	směs	k1gFnSc1	směs
o-hydroxybenzaldehydu	oydroxybenzaldehyd	k1gInSc2	o-hydroxybenzaldehyd
(	(	kIx(	(
<g/>
salicylaldehydu	salicylaldehydu	k6eAd1	salicylaldehydu
<g/>
)	)	kIx)	)
a	a	k8xC	a
p-hydroxybenzaldehydu	pydroxybenzaldehydu	k6eAd1	p-hydroxybenzaldehydu
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
chloroform	chloroform	k1gInSc1	chloroform
používán	používat	k5eAaImNgInS	používat
při	při	k7c6	při
operacích	operace	k1gFnPc6	operace
jako	jako	k9	jako
inhalační	inhalační	k2eAgNnSc4d1	inhalační
anestetikum	anestetikum	k1gNnSc4	anestetikum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dočasně	dočasně	k6eAd1	dočasně
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
méně	málo	k6eAd2	málo
příjemný	příjemný	k2eAgMnSc1d1	příjemný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zdravotně	zdravotně	k6eAd1	zdravotně
bezpečnější	bezpečný	k2eAgFnPc1d2	bezpečnější
diethylether	diethylethra	k1gFnPc2	diethylethra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
chloroformu	chloroform	k1gInSc2	chloroform
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
opuštěn	opuštěn	k2eAgInSc4d1	opuštěn
<g/>
,	,	kIx,	,
nahrazen	nahrazen	k2eAgInSc4d1	nahrazen
opět	opět	k6eAd1	opět
nejdříve	dříve	k6eAd3	dříve
diethyletherem	diethylethero	k1gNnSc7	diethylethero
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jinými	jiný	k2eAgInPc7d1	jiný
bezpečnějšími	bezpečný	k2eAgInPc7d2	bezpečnější
přípravky	přípravek	k1gInPc7	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
hlavně	hlavně	k9	hlavně
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
chladicího	chladicí	k2eAgNnSc2d1	chladicí
media	medium	k1gNnSc2	medium
pro	pro	k7c4	pro
ledničky	lednička	k1gFnPc4	lednička
a	a	k8xC	a
klimatizace	klimatizace	k1gFnSc2	klimatizace
<g/>
,	,	kIx,	,
freonu	freon	k1gInSc2	freon
R-22	R-22	k1gFnPc2	R-22
(	(	kIx(	(
<g/>
chlordifluormethan	chlordifluormethan	k1gInSc1	chlordifluormethan
<g/>
,	,	kIx,	,
CHClF	CHClF	k1gFnSc1	CHClF
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gNnSc1	jeho
využívání	využívání	k1gNnSc1	využívání
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
se	se	k3xPyFc4	se
však	však	k9	však
bude	být	k5eAaImBp3nS	být
stále	stále	k6eAd1	stále
snižovat	snižovat	k5eAaImF	snižovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
omezováním	omezování	k1gNnSc7	omezování
používání	používání	k1gNnSc2	používání
freonů	freon	k1gInPc2	freon
podle	podle	k7c2	podle
ustanovení	ustanovení	k1gNnSc2	ustanovení
Montrealského	montrealský	k2eAgInSc2d1	montrealský
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgFnPc2d1	další
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
aromatických	aromatický	k2eAgInPc2d1	aromatický
aldehydů	aldehyd	k1gInPc2	aldehyd
<g/>
.	.	kIx.	.
</s>
<s>
Chloroform	chloroform	k1gInSc1	chloroform
se	se	k3xPyFc4	se
též	též	k9	též
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
,	,	kIx,	,
ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgInSc6d1	farmaceutický
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
pesticidů	pesticid	k1gInPc2	pesticid
a	a	k8xC	a
nátěrových	nátěrový	k2eAgFnPc2d1	nátěrová
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výborným	výborný	k2eAgNnSc7d1	výborné
lepidlem	lepidlo	k1gNnSc7	lepidlo
některých	některý	k3yIgInPc2	některý
plastů	plast	k1gInPc2	plast
<g/>
,	,	kIx,	,
např.	např.	kA	např.
polystyrenu	polystyren	k1gInSc2	polystyren
nebo	nebo	k8xC	nebo
plexiskla	plexisklo	k1gNnSc2	plexisklo
<g/>
.	.	kIx.	.
</s>
<s>
Deuterochloroform	Deuterochloroform	k1gInSc1	Deuterochloroform
<g/>
,	,	kIx,	,
derivát	derivát	k1gInSc1	derivát
chloroformu	chloroform	k1gInSc2	chloroform
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgNnSc2	který
byl	být	k5eAaImAgInS	být
vodíkový	vodíkový	k2eAgInSc1d1	vodíkový
atom	atom	k1gInSc1	atom
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
atomem	atom	k1gInSc7	atom
deuteria	deuterium	k1gNnSc2	deuterium
(	(	kIx(	(
<g/>
těžkým	těžký	k2eAgInSc7d1	těžký
vodíkem	vodík	k1gInSc7	vodík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
CDCl	CDCl	k1gInSc1	CDCl
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejpoužívanějším	používaný	k2eAgNnSc7d3	nejpoužívanější
rozpouštědlem	rozpouštědlo	k1gNnSc7	rozpouštědlo
v	v	k7c6	v
NMR	NMR	kA	NMR
spektroskopii	spektroskopie	k1gFnSc6	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
činila	činit	k5eAaImAgFnS	činit
světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
chloroformu	chloroform	k1gInSc2	chloroform
cca	cca	kA	cca
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
může	moct	k5eAaImIp3nS	moct
chloroform	chloroform	k1gInSc1	chloroform
pronikat	pronikat	k5eAaImF	pronikat
všemi	všecek	k3xTgFnPc7	všecek
cestami	cesta	k1gFnPc7	cesta
–	–	k?	–
vdechováním	vdechování	k1gNnSc7	vdechování
<g/>
,	,	kIx,	,
požitím	požití	k1gNnSc7	požití
nebo	nebo	k8xC	nebo
vstřebáním	vstřebání	k1gNnSc7	vstřebání
přes	přes	k7c4	přes
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Dráždí	dráždit	k5eAaImIp3nS	dráždit
pokožku	pokožka	k1gFnSc4	pokožka
a	a	k8xC	a
sliznice	sliznice	k1gFnPc4	sliznice
<g/>
,	,	kIx,	,
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
nekrózu	nekróza	k1gFnSc4	nekróza
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
vředů	vřed	k1gInPc2	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstřebání	vstřebání	k1gNnSc6	vstřebání
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
tukových	tukový	k2eAgFnPc6d1	tuková
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
ještě	ještě	k9	ještě
řadu	řada	k1gFnSc4	řada
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vdechování	vdechování	k1gNnSc6	vdechování
par	para	k1gFnPc2	para
chloroformu	chloroform	k1gInSc2	chloroform
se	se	k3xPyFc4	se
vstřebá	vstřebat	k5eAaPmIp3nS	vstřebat
64	[number]	k4	64
<g/>
–	–	k?	–
<g/>
67	[number]	k4	67
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
anestetických	anestetický	k2eAgFnPc6d1	anestetická
koncentracích	koncentrace	k1gFnPc6	koncentrace
(	(	kIx(	(
<g/>
8000	[number]	k4	8000
<g/>
-	-	kIx~	-
<g/>
10000	[number]	k4	10000
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
hodnot	hodnota	k1gFnPc2	hodnota
okolo	okolo	k7c2	okolo
100	[number]	k4	100
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Zhruba	zhruba	k6eAd1	zhruba
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
%	%	kIx~	%
chloroformu	chloroform	k1gInSc2	chloroform
se	se	k3xPyFc4	se
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
v	v	k7c6	v
nezměněné	změněný	k2eNgFnSc6d1	nezměněná
podobě	podoba	k1gFnSc6	podoba
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vydechovaného	vydechovaný	k2eAgInSc2d1	vydechovaný
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
zbývajících	zbývající	k2eAgInPc2d1	zbývající
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
%	%	kIx~	%
odchází	odcházet	k5eAaImIp3nS	odcházet
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
metabolitů	metabolit	k1gInPc2	metabolit
močí	moč	k1gFnSc7	moč
a	a	k8xC	a
stolicí	stolice	k1gFnSc7	stolice
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
metabolizace	metabolizace	k1gFnSc2	metabolizace
chloroformu	chloroform	k1gInSc2	chloroform
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
dávce	dávka	k1gFnSc6	dávka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
dávkách	dávka	k1gFnPc6	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Inhalace	inhalace	k1gFnSc1	inhalace
par	para	k1gFnPc2	para
chloroformu	chloroform	k1gInSc2	chloroform
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
tlumení	tlumení	k1gNnSc3	tlumení
centrálního	centrální	k2eAgInSc2d1	centrální
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vdechování	vdechování	k1gNnSc1	vdechování
vzduchu	vzduch	k1gInSc2	vzduch
obsahujícího	obsahující	k2eAgInSc2d1	obsahující
900	[number]	k4	900
ppm	ppm	k?	ppm
po	po	k7c4	po
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
závratě	závrať	k1gFnSc2	závrať
<g/>
,	,	kIx,	,
ospalost	ospalost	k1gFnSc1	ospalost
a	a	k8xC	a
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
dávky	dávka	k1gFnPc1	dávka
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
hluboké	hluboký	k2eAgNnSc4d1	hluboké
kóma	kóma	k1gNnSc4	kóma
a	a	k8xC	a
útlum	útlum	k1gInSc4	útlum
dechového	dechový	k2eAgNnSc2d1	dechové
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
dávky	dávka	k1gFnPc1	dávka
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
také	také	k9	také
srdeční	srdeční	k2eAgFnSc4d1	srdeční
arytmii	arytmie	k1gFnSc4	arytmie
až	až	k8xS	až
fibrilaci	fibrilace	k1gFnSc4	fibrilace
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přímým	přímý	k2eAgInSc7d1	přímý
účinkem	účinek	k1gInSc7	účinek
na	na	k7c4	na
myokard	myokard	k1gInSc4	myokard
<g/>
,	,	kIx,	,
stimulací	stimulace	k1gFnSc7	stimulace
vagu	vagus	k1gInSc2	vagus
nebo	nebo	k8xC	nebo
senzibilizací	senzibilizace	k1gFnPc2	senzibilizace
na	na	k7c4	na
katecholaminy	katecholamin	k1gInPc4	katecholamin
<g/>
.	.	kIx.	.
</s>
<s>
Fibrilace	Fibrilace	k1gFnSc1	Fibrilace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Chloroform	chloroform	k1gInSc1	chloroform
také	také	k9	také
snižuje	snižovat	k5eAaImIp3nS	snižovat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
oslabením	oslabení	k1gNnSc7	oslabení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
kontrakce	kontrakce	k1gFnSc2	kontrakce
a	a	k8xC	a
periferní	periferní	k2eAgFnSc7d1	periferní
vazodilatací	vazodilatace	k1gFnSc7	vazodilatace
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
následkem	následkem	k7c2	následkem
stimulace	stimulace	k1gFnSc2	stimulace
vagu	vagus	k1gInSc2	vagus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
smrtná	smrtný	k2eAgFnSc1d1	Smrtná
dávka	dávka	k1gFnSc1	dávka
LDLo	LDLo	k1gNnSc1	LDLo
pozorovaná	pozorovaný	k2eAgFnSc1d1	pozorovaná
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
při	při	k7c6	při
požití	požití	k1gNnSc6	požití
byla	být	k5eAaImAgFnS	být
2514	[number]	k4	2514
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krys	krysa	k1gFnPc2	krysa
byla	být	k5eAaImAgFnS	být
experimentálně	experimentálně	k6eAd1	experimentálně
stanovena	stanoven	k2eAgFnSc1d1	stanovena
hodnota	hodnota	k1gFnSc1	hodnota
LD50	LD50	k1gFnSc2	LD50
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
695	[number]	k4	695
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
894	[number]	k4	894
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
při	pře	k1gFnSc4	pře
intraperitoneální	intraperitoneální	k2eAgFnSc7d1	intraperitoneální
aplikací	aplikace	k1gFnSc7	aplikace
(	(	kIx(	(
<g/>
injekcí	injekce	k1gFnSc7	injekce
do	do	k7c2	do
břišní	břišní	k2eAgFnSc2d1	břišní
dutiny	dutina	k1gFnSc2	dutina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Chloroform	chloroform	k1gInSc1	chloroform
je	být	k5eAaImIp3nS	být
toxický	toxický	k2eAgMnSc1d1	toxický
pro	pro	k7c4	pro
játra	játra	k1gNnPc4	játra
a	a	k8xC	a
ledviny	ledvina	k1gFnPc4	ledvina
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
odpovědné	odpovědný	k2eAgNnSc1d1	odpovědné
zřejmě	zřejmě	k6eAd1	zřejmě
metabolity	metabolit	k1gInPc7	metabolit
chloroformu	chloroform	k1gInSc2	chloroform
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
fosgen	fosgen	k1gInSc1	fosgen
<g/>
,	,	kIx,	,
karben	karben	k1gInSc1	karben
a	a	k8xC	a
chlor	chlor	k1gInSc1	chlor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
na	na	k7c4	na
makromolekuly	makromolekula	k1gFnPc4	makromolekula
jaterních	jaterní	k2eAgFnPc2d1	jaterní
a	a	k8xC	a
ledvinových	ledvinový	k2eAgFnPc2d1	ledvinová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Toxicita	toxicita	k1gFnSc1	toxicita
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
látkami	látka	k1gFnPc7	látka
působícími	působící	k2eAgFnPc7d1	působící
na	na	k7c4	na
jaterní	jaterní	k2eAgInPc4d1	jaterní
enzymy	enzym	k1gInPc4	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Barbituráty	barbiturát	k1gInPc1	barbiturát
<g/>
,	,	kIx,	,
DDT	DDT	kA	DDT
<g/>
,	,	kIx,	,
ethanol	ethanol	k1gInSc4	ethanol
apod.	apod.	kA	apod.
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
míru	míra	k1gFnSc4	míra
poškození	poškození	k1gNnSc2	poškození
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Chloroform	chloroform	k1gInSc1	chloroform
je	být	k5eAaImIp3nS	být
prokázaný	prokázaný	k2eAgInSc1d1	prokázaný
karcinogen	karcinogen	k1gInSc1	karcinogen
pro	pro	k7c4	pro
myši	myš	k1gFnPc4	myš
a	a	k8xC	a
potkany	potkan	k1gMnPc4	potkan
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
požívání	požívání	k1gNnSc1	požívání
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc2d1	malá
dávek	dávka	k1gFnPc2	dávka
(	(	kIx(	(
<g/>
0,75	[number]	k4	0,75
a	a	k8xC	a
75	[number]	k4	75
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prokazatelně	prokazatelně	k6eAd1	prokazatelně
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
růst	růst	k1gInSc4	růst
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
nádorů	nádor	k1gInPc2	nádor
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
nejsou	být	k5eNaImIp3nP	být
dostatečné	dostatečný	k2eAgInPc4d1	dostatečný
důkazy	důkaz	k1gInPc4	důkaz
pro	pro	k7c4	pro
prokázání	prokázání	k1gNnSc4	prokázání
karcinogenity	karcinogenita	k1gFnSc2	karcinogenita
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
chloroform	chloroform	k1gInSc1	chloroform
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
potenciální	potenciální	k2eAgInSc4d1	potenciální
karcinogen	karcinogen	k1gInSc4	karcinogen
<g/>
.	.	kIx.	.
</s>
<s>
Chloroform	chloroform	k1gInSc1	chloroform
je	být	k5eAaImIp3nS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
teratogenní	teratogenní	k2eAgNnSc1d1	teratogenní
pro	pro	k7c4	pro
myši	myš	k1gFnPc4	myš
<g/>
,	,	kIx,	,
potkany	potkan	k1gMnPc4	potkan
a	a	k8xC	a
králíky	králík	k1gMnPc4	králík
<g/>
.	.	kIx.	.
</s>
<s>
Mutagenita	mutagenita	k1gFnSc1	mutagenita
nebyla	být	k5eNaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
vlivů	vliv	k1gInPc2	vliv
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dostatek	dostatek	k1gInSc1	dostatek
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Chloroform	chloroform	k1gInSc1	chloroform
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1831	[number]	k4	1831
americký	americký	k2eAgMnSc1d1	americký
lékař	lékař	k1gMnSc1	lékař
Samuel	Samuel	k1gMnSc1	Samuel
Guthrie	Guthrie	k1gFnSc1	Guthrie
(	(	kIx(	(
<g/>
1782	[number]	k4	1782
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
Eugè	Eugè	k1gFnSc2	Eugè
Soubeiran	Soubeiran	k1gInSc1	Soubeiran
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
–	–	k?	–
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Justus	Justus	k1gInSc4	Justus
von	von	k1gInSc1	von
Liebig	Liebig	k1gInSc1	Liebig
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
–	–	k?	–
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soubeiran	Soubeiran	k1gInSc1	Soubeiran
připravil	připravit	k5eAaPmAgInS	připravit
chloroform	chloroform	k1gInSc4	chloroform
již	již	k6eAd1	již
zmiňovanou	zmiňovaný	k2eAgFnSc7d1	zmiňovaná
Liebenovou	Liebenový	k2eAgFnSc7d1	Liebenový
reakcí	reakce	k1gFnSc7	reakce
z	z	k7c2	z
ethanolu	ethanol	k1gInSc2	ethanol
případně	případně	k6eAd1	případně
acetonu	aceton	k1gInSc2	aceton
<g/>
.	.	kIx.	.
</s>
<s>
Chloroform	chloroform	k1gInSc4	chloroform
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
a	a	k8xC	a
odvodil	odvodit	k5eAaPmAgMnS	odvodit
jeho	jeho	k3xOp3gNnSc4	jeho
chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Dumas	Dumas	k1gMnSc1	Dumas
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
narkotické	narkotický	k2eAgInPc1d1	narkotický
účinky	účinek	k1gInPc1	účinek
poprvé	poprvé	k6eAd1	poprvé
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
francouzský	francouzský	k2eAgInSc1d1	francouzský
fyziolog	fyziolog	k1gMnSc1	fyziolog
Marie-Jean-Pierre	Marie-Jean-Pierr	k1gInSc5	Marie-Jean-Pierr
Flourens	Flourensa	k1gFnPc2	Flourensa
(	(	kIx(	(
<g/>
1794	[number]	k4	1794
<g/>
–	–	k?	–
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
James	James	k1gMnSc1	James
Fegle	Fegle	k1gFnSc1	Fegle
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
–	–	k?	–
<g/>
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
tamější	tamější	k2eAgMnSc1d1	tamější
porodník	porodník	k1gMnSc1	porodník
James	James	k1gMnSc1	James
Young	Young	k1gMnSc1	Young
Simpson	Simpson	k1gMnSc1	Simpson
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
chloroformu	chloroform	k1gInSc2	chloroform
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
anestezi	anesteze	k1gFnSc3	anesteze
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc3	jeho
používání	používání	k1gNnSc3	používání
při	při	k7c6	při
chirurgických	chirurgický	k2eAgInPc6d1	chirurgický
zákrocích	zákrok	k1gInPc6	zákrok
rychle	rychle	k6eAd1	rychle
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
anesteziolog	anesteziolog	k1gMnSc1	anesteziolog
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
John	John	k1gMnSc1	John
Snow	Snow	k1gMnSc1	Snow
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
podáním	podání	k1gNnSc7	podání
chloroformu	chloroform	k1gInSc2	chloroform
britské	britský	k2eAgFnSc3d1	britská
královně	královna	k1gFnSc3	královna
Viktorii	Viktoria	k1gFnSc3	Viktoria
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
k	k	k7c3	k
porodu	porod	k1gInSc3	porod
prince	princ	k1gMnSc2	princ
Leopolda	Leopold	k1gMnSc2	Leopold
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnSc1d2	pozdější
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Albany	Albana	k1gFnSc2	Albana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
však	však	k9	však
jeho	on	k3xPp3gNnSc2	on
užívání	užívání	k1gNnSc2	užívání
místo	místo	k7c2	místo
diethyletheru	diethylether	k1gInSc2	diethylether
se	se	k3xPyFc4	se
ujalo	ujmout	k5eAaPmAgNnS	ujmout
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
srdeční	srdeční	k2eAgNnSc4d1	srdeční
selhání	selhání	k1gNnSc4	selhání
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
rychle	rychle	k6eAd1	rychle
opuštěn	opuštěn	k2eAgInSc1d1	opuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc1	jeho
náhrada	náhrada	k1gFnSc1	náhrada
byl	být	k5eAaImAgInS	být
zkoušen	zkoušen	k2eAgInSc1d1	zkoušen
také	také	k9	také
trichlorethen	trichlorethen	k2eAgInSc1d1	trichlorethen
(	(	kIx(	(
<g/>
trichlorethylen	trichlorethylen	k1gInSc1	trichlorethylen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
jeho	jeho	k3xOp3gFnSc2	jeho
karcinogenicity	karcinogenicita	k1gFnSc2	karcinogenicita
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
halogenovaných	halogenovaný	k2eAgInPc2d1	halogenovaný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
chloroform	chloroform	k1gInSc1	chloroform
mezi	mezi	k7c4	mezi
látky	látka	k1gFnPc4	látka
potenciálně	potenciálně	k6eAd1	potenciálně
ohrožující	ohrožující	k2eAgFnSc4d1	ohrožující
ozónovou	ozónový	k2eAgFnSc4d1	ozónová
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vysoké	vysoký	k2eAgFnPc1d1	vysoká
specifické	specifický	k2eAgFnPc1d1	specifická
hmotnosti	hmotnost	k1gFnPc1	hmotnost
v	v	k7c6	v
plynném	plynný	k2eAgNnSc6d1	plynné
skupenství	skupenství	k1gNnSc6	skupenství
je	být	k5eAaImIp3nS	být
však	však	k9	však
méně	málo	k6eAd2	málo
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
než	než	k8xS	než
freony	freon	k1gInPc1	freon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
snadněji	snadno	k6eAd2	snadno
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
vysokých	vysoký	k2eAgFnPc2d1	vysoká
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
toxicitě	toxicita	k1gFnSc3	toxicita
pro	pro	k7c4	pro
vodní	vodní	k2eAgInPc4d1	vodní
organismy	organismus	k1gInPc4	organismus
je	být	k5eAaImIp3nS	být
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
látkou	látka	k1gFnSc7	látka
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
znečištění	znečištění	k1gNnSc2	znečištění
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
odpadními	odpadní	k2eAgFnPc7d1	odpadní
vodami	voda	k1gFnPc7	voda
z	z	k7c2	z
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
chloroform	chloroform	k1gInSc1	chloroform
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
také	také	k9	také
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
množstvích	množství	k1gNnPc6	množství
z	z	k7c2	z
některých	některý	k3yIgFnPc2	některý
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
alkoholy	alkohol	k1gInPc1	alkohol
<g/>
,	,	kIx,	,
ketony	keton	k1gInPc1	keton
<g/>
,	,	kIx,	,
aldehydy	aldehyd	k1gInPc1	aldehyd
<g/>
)	)	kIx)	)
při	při	k7c6	při
dezinfekci	dezinfekce	k1gFnSc6	dezinfekce
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
chlorováním	chlorování	k1gNnSc7	chlorování
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
chronickým	chronický	k2eAgFnPc3d1	chronická
zdravotním	zdravotní	k2eAgFnPc3d1	zdravotní
potížím	potíž	k1gFnPc3	potíž
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
používání	používání	k1gNnSc6	používání
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
pokrmů	pokrm	k1gInPc2	pokrm
a	a	k8xC	a
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Chloroformem	chloroform	k1gInSc7	chloroform
napuštěný	napuštěný	k2eAgInSc1d1	napuštěný
kapesník	kapesník	k1gInSc1	kapesník
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgInSc7d1	tradiční
nástrojem	nástroj	k1gInSc7	nástroj
určeným	určený	k2eAgInSc7d1	určený
k	k	k7c3	k
omámení	omámení	k1gNnSc3	omámení
oběti	oběť	k1gFnSc2	oběť
ve	v	k7c6	v
filmech	film	k1gInPc6	film
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
Knight	Knighta	k1gFnPc2	Knighta
Rider	Ridra	k1gFnPc2	Ridra
nebo	nebo	k8xC	nebo
King	Kinga	k1gFnPc2	Kinga
Kong	Kongo	k1gNnPc2	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Zlosyn	zlosyn	k1gMnSc1	zlosyn
obvykle	obvykle	k6eAd1	obvykle
kápne	kápnout	k5eAaPmIp3nS	kápnout
několik	několik	k4yIc1	několik
kapek	kapka	k1gFnPc2	kapka
chloroformu	chloroform	k1gInSc2	chloroform
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
lahvičky	lahvička	k1gFnSc2	lahvička
na	na	k7c4	na
kapesník	kapesník	k1gInSc4	kapesník
<g/>
,	,	kIx,	,
přikrade	přikrást	k5eAaPmIp3nS	přikrást
se	se	k3xPyFc4	se
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
a	a	k8xC	a
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
zápase	zápas	k1gInSc6	zápas
oběť	oběť	k1gFnSc4	oběť
upadá	upadat	k5eAaImIp3nS	upadat
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
dávka	dávka	k1gFnSc1	dávka
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
dávky	dávka	k1gFnSc2	dávka
smrtelné	smrtelný	k2eAgFnSc2d1	smrtelná
<g/>
.	.	kIx.	.
</s>
<s>
Fluoroform	Fluoroform	k1gInSc1	Fluoroform
Bromoform	bromoform	k1gInSc1	bromoform
Jodoform	jodoform	k1gInSc4	jodoform
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chloroform	chloroform	k1gInSc1	chloroform
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Concise	Concise	k1gFnSc1	Concise
International	International	k1gFnSc1	International
Chemical	Chemical	k1gFnSc1	Chemical
Assessment	Assessment	k1gInSc4	Assessment
Document	Document	k1gInSc1	Document
58	[number]	k4	58
History	Histor	k1gInPc7	Histor
of	of	k?	of
chloroform	chloroform	k1gInSc4	chloroform
anesthesia	anesthesius	k1gMnSc2	anesthesius
IARC	IARC	kA	IARC
Monograph	Monograph	k1gMnSc1	Monograph
on	on	k3xPp3gMnSc1	on
Chloroform	chloroform	k1gInSc4	chloroform
International	International	k1gFnPc1	International
Chemical	Chemical	k1gFnSc2	Chemical
Safety	Safeta	k1gFnSc2	Safeta
Card	Card	k1gInSc4	Card
0027	[number]	k4	0027
National	National	k1gMnSc1	National
Pollutant	Pollutant	k1gMnSc1	Pollutant
Inventory	Inventor	k1gInPc1	Inventor
–	–	k?	–
Chloroform	chloroform	k1gInSc1	chloroform
and	and	k?	and
trichloromethane	trichloromethanout	k5eAaPmIp3nS	trichloromethanout
NIOSH	NIOSH	kA	NIOSH
Pocket	Pocket	k1gInSc1	Pocket
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
Chemical	Chemical	k1gFnSc1	Chemical
Hazards	Hazardsa	k1gFnPc2	Hazardsa
NIST	NIST	kA	NIST
Standard	standard	k1gInSc1	standard
Reference	reference	k1gFnSc1	reference
Database	Databasa	k1gFnSc3	Databasa
Story	story	k1gFnSc4	story
on	on	k3xPp3gInSc1	on
Chloroform	chloroform	k1gInSc1	chloroform
from	from	k1gInSc4	from
BBC	BBC	kA	BBC
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
The	The	k1gFnSc7	The
Material	Material	k1gMnSc1	Material
World	World	k1gMnSc1	World
(	(	kIx(	(
<g/>
28	[number]	k4	28
July	Jula	k1gFnSc2	Jula
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Sudden	Suddna	k1gFnPc2	Suddna
Sniffer	Sniffer	k1gInSc1	Sniffer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Death	Death	k1gInSc1	Death
Syndrome	syndrom	k1gInSc5	syndrom
article	article	k1gNnPc7	article
at	at	k?	at
Carolinas	Carolinas	k1gMnSc1	Carolinas
Poison	Poison	k1gMnSc1	Poison
Center	centrum	k1gNnPc2	centrum
</s>
