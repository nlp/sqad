<s>
Chloroform	chloroform	k1gInSc1	chloroform
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1831	[number]	k4	1831
americký	americký	k2eAgMnSc1d1	americký
lékař	lékař	k1gMnSc1	lékař
Samuel	Samuel	k1gMnSc1	Samuel
Guthrie	Guthrie	k1gFnSc1	Guthrie
(	(	kIx(	(
<g/>
1782	[number]	k4	1782
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
Eugè	Eugè	k1gFnSc2	Eugè
Soubeiran	Soubeiran	k1gInSc1	Soubeiran
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
–	–	k?	–
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Justus	Justus	k1gInSc4	Justus
von	von	k1gInSc1	von
Liebig	Liebig	k1gInSc1	Liebig
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
–	–	k?	–
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
