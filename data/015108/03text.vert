<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
TUL	Tula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Tul	tulit	k5eAaImRp2nS
–	–	k?
sestava	sestava	k1gFnSc1
Taekwon-do	Taekwon-do	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Logo	logo	k1gNnSc4
Nový	nový	k2eAgInSc1d1
rektorát	rektorát	k1gInSc1
a	a	k8xC
informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
TUL	Tula	k1gFnPc2
Rok	rok	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
1953	#num#	k4
Typ	typ	k1gInSc1
školy	škola	k1gFnSc2
</s>
<s>
veřejná	veřejný	k2eAgNnPc1d1
Vedení	vedení	k1gNnPc1
Rektor	rektor	k1gMnSc1
</s>
<s>
doc.	doc.	kA
RNDr.	RNDr.	kA
Miroslav	Miroslav	k1gMnSc1
Brzezina	Brzezina	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Kvestor	kvestor	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Vladimír	Vladimír	k1gMnSc1
Stach	Stach	k1gMnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
a	a	k8xC
vzdělávání	vzdělávání	k1gNnSc4
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Miroslav	Miroslav	k1gMnSc1
Žižka	Žižka	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
vědu	věda	k1gFnSc4
<g/>
,	,	kIx,
výzkum	výzkum	k1gInSc4
a	a	k8xC
zahraničí	zahraničí	k1gNnSc2
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Pavel	Pavel	k1gMnSc1
Mokrý	Mokrý	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
informatiku	informatika	k1gFnSc4
</s>
<s>
doc.	doc.	kA
RNDr.	RNDr.	kA
Pavel	Pavel	k1gMnSc1
Satrapa	satrapa	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
vnější	vnější	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
</s>
<s>
Ing.	ing.	kA
arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radek	Radek	k1gMnSc1
Suchánek	Suchánek	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Počty	počet	k1gInPc1
akademiků	akademik	k1gMnPc2
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
bakalářských	bakalářský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
4289	#num#	k4
Počet	počet	k1gInSc4
magisterských	magisterský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
1735	#num#	k4
Počet	počet	k1gInSc4
doktorandů	doktorand	k1gMnPc2
</s>
<s>
318	#num#	k4
Počet	počet	k1gInSc4
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
</s>
<s>
592	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Počet	počet	k1gInSc1
fakult	fakulta	k1gFnPc2
</s>
<s>
7	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Liberec	Liberec	k1gInSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Studentská	studentská	k1gFnSc1
1402	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
460	#num#	k4
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
http://www.tul.cz	http://www.tul.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
(	(	kIx(
<g/>
TUL	tulit	k5eAaImRp2nS
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
založená	založený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1953	#num#	k4
ve	v	k7c6
městě	město	k1gNnSc6
Liberci	Liberec	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
má	mít	k5eAaImIp3nS
sedm	sedm	k4xCc4
fakult	fakulta	k1gFnPc2
a	a	k8xC
jeden	jeden	k4xCgInSc4
odborný	odborný	k2eAgInSc4d1
ústav	ústav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdělává	vzdělávat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
ní	on	k3xPp3gFnSc6
kolem	kolem	k7c2
6	#num#	k4
tisíc	tisíc	k4xCgInPc2
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Škola	škola	k1gFnSc1
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
rozhodnutím	rozhodnutí	k1gNnSc7
vlády	vláda	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1953	#num#	k4
jako	jako	k8xS,k8xC
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
(	(	kIx(
<g/>
VŠS	VŠS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
novou	nový	k2eAgFnSc4d1
školu	škola	k1gFnSc4
byla	být	k5eAaImAgFnS
uvolněna	uvolněn	k2eAgFnSc1d1
budova	budova	k1gFnSc1
tehdejšího	tehdejší	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
F.	F.	kA
X.	X.	kA
Šaldy	Šalda	k1gMnSc2
v	v	k7c6
Hálkově	Hálkův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
roku	rok	k1gInSc2
1953	#num#	k4
nastoupilo	nastoupit	k5eAaPmAgNnS
do	do	k7c2
prvních	první	k4xOgInPc2
ročníků	ročník	k1gInPc2
čerstvě	čerstvě	k6eAd1
otevřené	otevřený	k2eAgFnSc2d1
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
259	#num#	k4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
měla	mít	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
šest	šest	k4xCc4
kateder	katedra	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
působilo	působit	k5eAaImAgNnS
19	#num#	k4
pedagogů	pedagog	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřovala	zaměřovat	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c4
obory	obor	k1gInPc4
typické	typický	k2eAgInPc4d1
pro	pro	k7c4
severní	severní	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
<g/>
:	:	kIx,
strojírenský	strojírenský	k2eAgInSc4d1
<g/>
,	,	kIx,
textilní	textilní	k2eAgInSc4d1
<g/>
,	,	kIx,
oděvní	oděvní	k2eAgInSc4d1
<g/>
,	,	kIx,
sklářský	sklářský	k2eAgInSc4d1
a	a	k8xC
keramický	keramický	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgMnPc1d1
studenti	student	k1gMnPc1
byli	být	k5eAaImAgMnP
ubytováni	ubytovat	k5eAaPmNgMnP
na	na	k7c6
internátě	internát	k1gInSc6
v	v	k7c6
Zeyerově	Zeyerův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
následujících	následující	k2eAgNnPc2d1
let	léto	k1gNnPc2
se	se	k3xPyFc4
škola	škola	k1gFnSc1
rozrůstala	rozrůstat	k5eAaImAgFnS
nejen	nejen	k6eAd1
o	o	k7c4
nové	nový	k2eAgMnPc4d1
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
o	o	k7c4
nové	nový	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
<g/>
:	:	kIx,
byly	být	k5eAaImAgFnP
postaveny	postaven	k2eAgFnPc1d1
nové	nový	k2eAgFnPc1d1
koleje	kolej	k1gFnPc1
<g/>
,	,	kIx,
získána	získán	k2eAgFnSc1d1
budova	budova	k1gFnSc1
bývalé	bývalý	k2eAgFnSc2d1
textilní	textilní	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
v	v	k7c6
Doubí	doubí	k1gNnSc6
a	a	k8xC
další	další	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
v	v	k7c6
okolí	okolí	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Studentského	studentský	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1958	#num#	k4
dokončilo	dokončit	k5eAaPmAgNnS
školu	škola	k1gFnSc4
prvních	první	k4xOgNnPc6
121	#num#	k4
absolventů	absolvent	k1gMnPc2
slavnostní	slavnostní	k2eAgFnSc7d1
promocí	promoce	k1gFnSc7
v	v	k7c6
libereckém	liberecký	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1960	#num#	k4
byla	být	k5eAaImAgFnS
škola	škola	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
fakultu	fakulta	k1gFnSc4
strojní	strojní	k2eAgFnSc4d1
a	a	k8xC
textilní	textilní	k2eAgInPc4d1
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
z	z	k7c2
ní	on	k3xPp3gFnSc2
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
a	a	k8xC
textilní	textilní	k2eAgFnSc1d1
v	v	k7c6
Liberci	Liberec	k1gInSc6
(	(	kIx(
<g/>
VŠST	VŠST	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byly	být	k5eAaImAgFnP
získány	získat	k5eAaPmNgFnP
budovy	budova	k1gFnPc1
v	v	k7c6
Sokolské	sokolský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
S	s	k7c7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
objekt	objekt	k1gInSc4
po	po	k7c6
zrušeném	zrušený	k2eAgInSc6d1
pedagogickém	pedagogický	k2eAgInSc6d1
institutu	institut	k1gInSc6
v	v	k7c6
Komenského	Komenského	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
(	(	kIx(
<g/>
budova	budova	k1gFnSc1
P	P	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
nárůstem	nárůst	k1gInSc7
počtu	počet	k1gInSc2
studentů	student	k1gMnPc2
samozřejmě	samozřejmě	k6eAd1
přestala	přestat	k5eAaPmAgFnS
stačit	stačit	k5eAaBmF
ubytovací	ubytovací	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
tehdejších	tehdejší	k2eAgFnPc2d1
kolejí	kolej	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1977	#num#	k4
v	v	k7c6
libereckém	liberecký	k2eAgInSc6d1
Starém	starý	k2eAgInSc6d1
Harcově	Harcův	k2eAgInSc6d1
zahájena	zahájit	k5eAaPmNgFnS
výstavba	výstavba	k1gFnSc1
komplexu	komplex	k1gInSc2
šesti	šest	k4xCc2
kolejních	kolejní	k2eAgInPc2d1
bloků	blok	k1gInPc2
o	o	k7c6
kapacitě	kapacita	k1gFnSc6
2300	#num#	k4
lůžek	lůžko	k1gNnPc2
<g/>
,	,	kIx,
nové	nový	k2eAgFnSc2d1
menzy	menza	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplex	komplex	k1gInSc1
byl	být	k5eAaImAgInS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
dnešní	dnešní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
dokončen	dokončen	k2eAgInSc1d1
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
získána	získán	k2eAgFnSc1d1
budova	budova	k1gFnSc1
bývalého	bývalý	k2eAgInSc2d1
Stavoprojektu	Stavoprojekt	k1gInSc2
ve	v	k7c6
Voroněžské	voroněžský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
(	(	kIx(
<g/>
budova	budova	k1gFnSc1
H	H	kA
<g/>
)	)	kIx)
včetně	včetně	k7c2
dočasného	dočasný	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
Investiční	investiční	k2eAgFnSc2d1
a	a	k8xC
poštovní	poštovní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
nově	nově	k6eAd1
přestavěného	přestavěný	k2eAgNnSc2d1
na	na	k7c4
univerzitní	univerzitní	k2eAgFnSc4d1
knihovnu	knihovna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
získala	získat	k5eAaPmAgFnS
škola	škola	k1gFnSc1
také	také	k9
komplex	komplex	k1gInSc4
ve	v	k7c6
Vesci	Vesce	k1gFnSc6
sloužící	sloužící	k1gFnSc2
jako	jako	k8xC,k8xS
koleje	kolej	k1gFnSc2
a	a	k8xC
laboratoře	laboratoř	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
někdejší	někdejší	k2eAgInSc4d1
Dům	dům	k1gInSc4
politické	politický	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
na	na	k7c6
třídě	třída	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
máje	máj	k1gInSc2
(	(	kIx(
<g/>
budova	budova	k1gFnSc1
K	K	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1990	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
zřídila	zřídit	k5eAaPmAgFnS
škola	škola	k1gFnSc1
další	další	k2eAgFnPc4d1
čtyři	čtyři	k4xCgFnPc4
fakulty	fakulta	k1gFnPc4
<g/>
:	:	kIx,
pedagogickou	pedagogický	k2eAgFnSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
<g/>
,	,	kIx,
hospodářskou	hospodářský	k2eAgFnSc7d1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
architektury	architektura	k1gFnSc2
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
mechatroniky	mechatronika	k1gFnSc2
a	a	k8xC
mezioborových	mezioborový	k2eAgFnPc2d1
inženýrských	inženýrský	k2eAgFnPc2d1
studií	studie	k1gFnPc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
takovému	takový	k3xDgInSc3
růstu	růst	k1gInSc3
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgFnSc3d1
výzkumné	výzkumný	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
a	a	k8xC
zahraničním	zahraniční	k2eAgInPc3d1
stykům	styk	k1gInPc3
byl	být	k5eAaImAgInS
škole	škola	k1gFnSc6
zákonem	zákon	k1gInSc7
č.	č.	k?
192	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z	z	k7c2
27	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1994	#num#	k4
přiznán	přiznat	k5eAaPmNgInS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
název	název	k1gInSc1
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rektoři	rektor	k1gMnPc1
Technické	technický	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Liberci	Liberec	k1gInSc6
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Dr	dr	kA
<g/>
.	.	kIx.
techn	techn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Kožoušek	Kožoušek	k1gMnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
doc.	doc.	kA
Ing.	ing.	kA
Vojtěch	Vojtěch	k1gMnSc1
Dráb	dráb	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
akad	akad	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jovan	Jovan	k1gMnSc1
Čirlič	Čirlič	k1gMnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Jiří	Jiří	k1gMnSc1
Mayer	Mayer	k1gMnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
akad	akad	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jovan	Jovan	k1gMnSc1
Čirlič	Čirlič	k1gMnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
RNDr.	RNDr.	kA
Bohuslav	Bohuslava	k1gFnPc2
Stříž	stříž	k1gFnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Zdeněk	Zdeněk	k1gMnSc1
Kovář	Kovář	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
RNDr.	RNDr.	kA
David	David	k1gMnSc1
Lukáš	Lukáš	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Vojtěch	Vojtěch	k1gMnSc1
Konopa	Konop	k1gMnSc2
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
Dr	dr	kA
<g/>
.	.	kIx.
Ing.	ing.	kA
Zdeněk	Zdeněk	k1gMnSc1
Kůs	Kůs	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
doc.	doc.	kA
RNDr.	RNDr.	kA
Miroslav	Miroslav	k1gMnSc1
Brzezina	Brzezina	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rektoři	rektor	k1gMnPc1
TUL	Tula	k1gFnPc2
</s>
<s>
Fakulty	fakulta	k1gFnPc1
a	a	k8xC
univerzitní	univerzitní	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
B	B	kA
</s>
<s>
Univerzita	univerzita	k1gFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
sedm	sedm	k4xCc4
fakult	fakulta	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
1953	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
textilní	textilní	k2eAgFnSc1d1
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
1960	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
přírodovědně-humanitní	přírodovědně-humanitní	k2eAgFnSc1d1
a	a	k8xC
pedagogická	pedagogický	k2eAgFnSc1d1
(	(	kIx(
<g/>
FPHP	FPHP	kA
<g/>
/	/	kIx~
<g/>
FP	FP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
1990	#num#	k4
jako	jako	k8xC,k8xS
Fakulta	fakulta	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
(	(	kIx(
<g/>
FP	FP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přejmenována	přejmenován	k2eAgFnSc1d1
2008	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
EF	EF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
1992	#num#	k4
jako	jako	k8xC,k8xS
Hospodářská	hospodářský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
HF	HF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přejmenována	přejmenován	k2eAgFnSc1d1
2009	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
umění	umění	k1gNnSc2
a	a	k8xC
architektury	architektura	k1gFnSc2
(	(	kIx(
<g/>
FUA	FUA	kA
<g/>
/	/	kIx~
<g/>
FA	fa	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
1994	#num#	k4
jako	jako	k8xC,k8xS
Fakulta	fakulta	k1gFnSc1
architektury	architektura	k1gFnSc2
(	(	kIx(
<g/>
FA	fa	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přejmenována	přejmenován	k2eAgFnSc1d1
2007	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
mechatroniky	mechatronika	k1gFnSc2
<g/>
,	,	kIx,
informatiky	informatika	k1gFnSc2
a	a	k8xC
mezioborových	mezioborový	k2eAgFnPc2d1
studií	studie	k1gFnPc2
(	(	kIx(
<g/>
FMIMS	FMIMS	kA
<g/>
/	/	kIx~
<g/>
FM	FM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
1995	#num#	k4
jako	jako	k8xC,k8xS
Fakulta	fakulta	k1gFnSc1
mechatroniky	mechatronika	k1gFnSc2
a	a	k8xC
mezioborových	mezioborový	k2eAgFnPc2d1
inženýrských	inženýrský	k2eAgFnPc2d1
studií	studie	k1gFnPc2
(	(	kIx(
<g/>
FMMIS	FMMIS	kA
<g/>
/	/	kIx~
<g/>
FM	FM	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přejmenována	přejmenován	k2eAgFnSc1d1
2008	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
zdravotnických	zdravotnický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
(	(	kIx(
<g/>
FZS	FZS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
2016	#num#	k4
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
z	z	k7c2
Ústavu	ústav	k1gInSc2
zdravotnických	zdravotnický	k2eAgFnPc2d1
studií	studie	k1gFnPc2
(	(	kIx(
<g/>
ÚZS	ÚZS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
</s>
<s>
a	a	k8xC
jeden	jeden	k4xCgInSc1
univerzitní	univerzitní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
nanomateriály	nanomateriál	k1gInPc4
<g/>
,	,	kIx,
pokročilé	pokročilý	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
a	a	k8xC
inovace	inovace	k1gFnSc1
(	(	kIx(
<g/>
CxI	CxI	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založen	založen	k2eAgMnSc1d1
2009	#num#	k4
</s>
<s>
Univerzita	univerzita	k1gFnSc1
dále	daleko	k6eAd2
provozuje	provozovat	k5eAaImIp3nS
Centrum	centrum	k1gNnSc1
dalšího	další	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
(	(	kIx(
<g/>
CDV	CDV	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrum	centrum	k1gNnSc1
organizuje	organizovat	k5eAaBmIp3nS
řadu	řada	k1gFnSc4
kurzů	kurz	k1gInPc2
dalšího	další	k2eAgNnSc2d1
a	a	k8xC
celoživotního	celoživotní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
kurzy	kurz	k1gInPc1
pedagogické	pedagogický	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
nebo	nebo	k8xC
jazykové	jazykový	k2eAgInPc4d1
kurzy	kurz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
CDV	CDV	kA
zajišťuje	zajišťovat	k5eAaImIp3nS
univerzitu	univerzita	k1gFnSc4
třetího	třetí	k4xOgInSc2
věku	věk	k1gInSc2
pro	pro	k7c4
zájemce	zájemce	k1gMnPc4
od	od	k7c2
50	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Fakulty	fakulta	k1gFnPc1
a	a	k8xC
ústavy	ústav	k1gInPc1
TUL	Tula	k1gFnPc2
</s>
<s>
Významné	významný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Anděl	Anděl	k1gMnSc1
</s>
<s>
Jovan	Jovan	k1gMnSc1
Čirlič	Čirlič	k1gMnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Exner	Exner	k1gMnSc1
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Jirsák	Jirsák	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Nouza	Nouz	k1gMnSc2
</s>
<s>
Miloš	Miloš	k1gMnSc1
Raban	Raban	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Satrapa	satrapa	k1gMnSc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Stibor	Stibor	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Suchomel	Suchomel	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Svatý	svatý	k2eAgMnSc1d1
</s>
<s>
Bořek	Bořek	k1gMnSc1
Šípek	Šípek	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Umlauf	Umlauf	k1gMnSc1
</s>
<s>
Současnost	současnost	k1gFnSc1
TUL	Tula	k1gFnPc2
</s>
<s>
V	v	k7c6
areálu	areál	k1gInSc6
Husova	Husův	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
menza	menza	k1gFnSc1
<g/>
,	,	kIx,
informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
,	,	kIx,
studentský	studentský	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
pobočka	pobočka	k1gFnSc1
Univerzitní	univerzitní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
a	a	k8xC
školka	školka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzitní	univerzitní	k2eAgFnSc1d1
školka	školka	k1gFnSc1
„	„	k?
<g/>
ŠkaTULka	škatulka	k1gFnSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
48	#num#	k4
dětí	dítě	k1gFnPc2
ve	v	k7c6
věku	věk	k1gInSc6
od	od	k7c2
3	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
nabízí	nabízet	k5eAaImIp3nS
přístup	přístup	k1gInSc1
Montessori	Montessor	k1gFnSc2
a	a	k8xC
Waldorfské	waldorfský	k2eAgFnSc2d1
pedagogiky	pedagogika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
životě	život	k1gInSc6
na	na	k7c6
Technické	technický	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Liberci	Liberec	k1gInSc6
informuje	informovat	k5eAaBmIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
zpravodajský	zpravodajský	k2eAgInSc1d1
časopis	časopis	k1gInSc1
T-UNI	T-UNI	k1gFnSc2
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
univerzitě	univerzita	k1gFnSc6
dále	daleko	k6eAd2
působí	působit	k5eAaImIp3nS
řada	řada	k1gFnSc1
studentských	studentský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
včetně	včetně	k7c2
studentské	studentský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ubytování	ubytování	k1gNnSc1
</s>
<s>
Studentské	studentský	k2eAgFnPc1d1
koleje	kolej	k1gFnPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgFnP
ve	v	k7c6
Starém	starý	k2eAgNnSc6d1
Harcově	Harcův	k2eAgNnSc6d1
na	na	k7c6
ulici	ulice	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
587	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2011	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
a	a	k8xC
2014	#num#	k4
zvítězily	zvítězit	k5eAaPmAgFnP
ve	v	k7c6
studentské	studentský	k2eAgFnSc6d1
anketě	anketa	k1gFnSc6
„	„	k?
<g/>
Kolej	kolej	k1gFnSc1
roku	rok	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
areálu	areál	k1gInSc2
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
sportovní	sportovní	k2eAgFnPc1d1
haly	hala	k1gFnPc1
<g/>
,	,	kIx,
tělocvičny	tělocvična	k1gFnPc1
<g/>
,	,	kIx,
lezecká	lezecký	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
<g/>
,	,	kIx,
sauna	sauna	k1gFnSc1
<g/>
,	,	kIx,
posilovna	posilovna	k1gFnSc1
<g/>
,	,	kIx,
minigolf	minigolf	k1gInSc1
<g/>
,	,	kIx,
lanové	lanový	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
,	,	kIx,
fotbalové	fotbalový	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
a	a	k8xC
hřiště	hřiště	k1gNnSc4
na	na	k7c4
beach	beach	k1gInSc4
volejbal	volejbal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dětská	dětský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
probíhá	probíhat	k5eAaImIp3nS
na	na	k7c6
TUL	Tula	k1gFnPc2
Dětská	dětský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
celoroční	celoroční	k2eAgNnSc1d1
volnočasové	volnočasový	k2eAgNnSc1d1
neformální	formální	k2eNgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
dětí	dítě	k1gFnPc2
a	a	k8xC
mládeže	mládež	k1gFnSc2
ve	v	k7c6
věku	věk	k1gInSc6
od	od	k7c2
6	#num#	k4
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
oborech	obor	k1gInPc6
<g/>
:	:	kIx,
elektrotechnika	elektrotechnika	k1gFnSc1
<g/>
,	,	kIx,
fyzika	fyzika	k1gFnSc1
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
strojírenství	strojírenství	k1gNnSc2
<g/>
,	,	kIx,
textilní	textilní	k2eAgInPc4d1
obory	obor	k1gInPc4
<g/>
,	,	kIx,
robotika	robotika	k1gFnSc1
<g/>
,	,	kIx,
programování	programování	k1gNnSc1
a	a	k8xC
matematika	matematika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuka	výuka	k1gFnSc1
kopíruje	kopírovat	k5eAaImIp3nS
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
včetně	včetně	k7c2
promoce	promoce	k1gFnSc2
<g/>
,	,	kIx,
zápočtů	zápočet	k1gInPc2
a	a	k8xC
zpracování	zpracování	k1gNnSc2
závěrečné	závěrečný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
O	o	k7c4
sportoviště	sportoviště	k1gNnSc4
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
Akademické	akademický	k2eAgNnSc1d1
sportovní	sportovní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
organizuje	organizovat	k5eAaBmIp3nS
také	také	k9
fotbalovou	fotbalový	k2eAgFnSc4d1
<g/>
,	,	kIx,
florbalovou	florbalový	k2eAgFnSc4d1
<g/>
,	,	kIx,
basketbalovou	basketbalový	k2eAgFnSc4d1
a	a	k8xC
volejbalovou	volejbalový	k2eAgFnSc4d1
ligu	liga	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
TUL	Tula	k1gFnPc2
existuje	existovat	k5eAaImIp3nS
Volejbalový	volejbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
a	a	k8xC
Badmintonový	badmintonový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
další	další	k2eAgInSc1d1
z	z	k7c2
nich	on	k3xPp3gInPc2
se	se	k3xPyFc4
soustředí	soustředit	k5eAaPmIp3nS
v	v	k7c6
Univerzitním	univerzitní	k2eAgInSc6d1
sportovním	sportovní	k2eAgInSc6d1
klubu	klub	k1gInSc6
Slavia	Slavia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Univerzitní	univerzitní	k2eAgInSc4d1
sportovní	sportovní	k2eAgInSc4d1
klub	klub	k1gInSc4
Slavia	Slavia	k1gFnSc1
Liberec	Liberec	k1gInSc1
</s>
<s>
Současný	současný	k2eAgInSc1d1
Univerzitní	univerzitní	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Slavia	Slavium	k1gNnSc2
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
USK	USK	kA
Slavia	Slavia	k1gFnSc1
Liberec	Liberec	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
říjnu	říjen	k1gInSc6
1953	#num#	k4
Jaroslavem	Jaroslav	k1gMnSc7
Tyšlem	Tyšl	k1gMnSc7
jako	jako	k8xC,k8xS
Vysokoškolská	vysokoškolský	k2eAgFnSc1d1
tělovýchovná	tělovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
strojní	strojní	k2eAgFnSc2d1
(	(	kIx(
<g/>
VŠTJ	VŠTJ	kA
Slavia	Slavia	k1gFnSc1
VŠS	VŠS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
původně	původně	k6eAd1
pět	pět	k4xCc4
oddílů	oddíl	k1gInPc2
<g/>
:	:	kIx,
odbíjená	odbíjená	k1gFnSc1
<g/>
,	,	kIx,
basketbal	basketbal	k1gInSc1
<g/>
,	,	kIx,
kopaná	kopaná	k1gFnSc1
<g/>
,	,	kIx,
lyžování	lyžování	k1gNnSc1
a	a	k8xC
lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc3
2016	#num#	k4
měl	mít	k5eAaImAgInS
klub	klub	k1gInSc1
255	#num#	k4
členů	člen	k1gMnPc2
v	v	k7c6
osmi	osm	k4xCc6
sportovních	sportovní	k2eAgInPc6d1
oddílech	oddíl	k1gInPc6
<g/>
:	:	kIx,
basketbal	basketbal	k1gInSc1
<g/>
,	,	kIx,
volejbal	volejbal	k1gInSc1
<g/>
,	,	kIx,
lyžování	lyžování	k1gNnSc1
<g/>
,	,	kIx,
tenis	tenis	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
horolezectví	horolezectví	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
karate	karate	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1977	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
futsal	futsat	k5eAaImAgMnS,k5eAaPmAgMnS
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
florbal	florbal	k1gInSc4
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
Technické	technický	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Liberci	Liberec	k1gInSc6
za	za	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
42	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
§	§	k?
2	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
192	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
změně	změna	k1gFnSc6
názvu	název	k1gInSc2
některých	některý	k3yIgFnPc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
uvedených	uvedený	k2eAgFnPc2d1
v	v	k7c6
příloze	příloha	k1gFnSc6
zákona	zákon	k1gInSc2
č.	č.	k?
172	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlin	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
T-UNI	T-UNI	k?
online	onlinout	k5eAaPmIp3nS
–	–	k?
zpravodaj	zpravodaj	k1gInSc4
Technické	technický	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Univerzitní	univerzitní	k2eAgFnSc1d1
Knihovna	knihovna	k1gFnSc1
</s>
<s>
Dětská	dětský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
</s>
<s>
Akademické	akademický	k2eAgNnSc1d1
sportovní	sportovní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
TUL	Tula	k1gFnPc2
</s>
<s>
USK	USK	kA
Slavia	Slavia	k1gFnSc1
Liberec	Liberec	k1gInSc1
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
textilní	textilní	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
přírodovědně-humanitní	přírodovědně-humanitní	k2eAgFnSc1d1
a	a	k8xC
pedagogická	pedagogický	k2eAgFnSc1d1
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Fakulta	fakulta	k1gFnSc1
umění	umění	k1gNnSc2
a	a	k8xC
architektury	architektura	k1gFnSc2
•	•	k?
Fakulta	fakulta	k1gFnSc1
mechatroniky	mechatronika	k1gFnSc2
<g/>
,	,	kIx,
informatiky	informatika	k1gFnSc2
a	a	k8xC
mezioborových	mezioborový	k2eAgFnPc2d1
studií	studie	k1gFnPc2
•	•	k?
Fakulta	fakulta	k1gFnSc1
zdravotnických	zdravotnický	k2eAgFnPc2d1
studií	studie	k1gFnPc2
Univerzitní	univerzitní	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
</s>
<s>
Ústav	ústav	k1gInSc1
pro	pro	k7c4
nanomateriály	nanomateriál	k1gInPc4
<g/>
,	,	kIx,
pokročilé	pokročilý	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
a	a	k8xC
inovace	inovace	k1gFnSc1
Další	další	k2eAgFnPc1d1
součásti	součást	k1gFnPc1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Centrum	centrum	k1gNnSc1
dalšího	další	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Praha	Praha	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Akademie	akademie	k1gFnSc2
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Česká	český	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
České	český	k2eAgFnSc6d1
vysoké	vysoká	k1gFnSc6
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
chemicko-technologická	chemicko-technologický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Jihočeská	jihočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
technická	technický	k2eAgFnSc1d1
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Liberec	Liberec	k1gInSc1
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
polytechnická	polytechnický	k2eAgFnSc1d1
Jihlava	Jihlava	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
•	•	k?
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Mendelova	Mendelův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
•	•	k?
Veterinární	veterinární	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Brno	Brno	k1gNnSc1
•	•	k?
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Brně	Brno	k1gNnSc6
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Ostravská	ostravský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
báňská	báňský	k2eAgFnSc1d1
–	–	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
Opava	Opava	k1gFnSc1
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Opavě	Opava	k1gFnSc6
Zlín	Zlín	k1gInSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
</s>
<s>
Liberec	Liberec	k1gInSc1
Doprava	doprava	k1gFnSc1
</s>
<s>
Doprava	doprava	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Autobusové	autobusový	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Historie	historie	k1gFnSc1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Liberce	Liberec	k1gInSc2
•	•	k?
Dějiny	dějiny	k1gFnPc4
Liberce	Liberec	k1gInSc2
•	•	k?
Ještěd	Ještěd	k1gInSc1
(	(	kIx(
<g/>
obchodní	obchodní	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Reichenberger	Reichenberger	k1gInSc1
Tagesbote	Tagesbot	k1gInSc5
•	•	k?
Reichenberger	Reichenberger	k1gMnSc1
Zeitung	Zeitung	k1gMnSc1
•	•	k?
Valdštejnské	valdštejnský	k2eAgInPc4d1
domky	domek	k1gInPc4
Kultura	kultura	k1gFnSc1
</s>
<s>
Botanická	botanický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Centrum	centrum	k1gNnSc1
Babylon	Babylon	k1gInSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Divadlo	divadlo	k1gNnSc1
F.	F.	kA
X.	X.	kA
Šaldy	Šalda	k1gMnSc2
•	•	k?
Krajská	krajský	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Liberecký	liberecký	k2eAgInSc1d1
zámek	zámek	k1gInSc1
•	•	k?
Oblastní	oblastní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Severočeské	severočeský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Technické	technický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Liberec	Liberec	k1gInSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
Božského	božský	k2eAgNnSc2d1
srdce	srdce	k1gNnSc2
Páně	páně	k2eAgInSc4d1
•	•	k?
Kostel	kostel	k1gInSc4
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Bonifáce	Bonifác	k1gMnSc2
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Antonína	Antonín	k1gMnSc2
Velikého	veliký	k2eAgNnSc2d1
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Antonína	Antonín	k1gMnSc2
Paduánského	paduánský	k2eAgNnSc2d1
•	•	k?
Kostel	kostel	k1gInSc1
svaté	svatá	k1gFnSc2
Máří	Máří	k?
Magdalény	Magdaléna	k1gFnSc2
a	a	k8xC
kapucínský	kapucínský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
•	•	k?
Kostel	kostel	k1gInSc1
Nalezení	nalezení	k1gNnSc2
svatého	svatý	k2eAgInSc2d1
Kříže	kříž	k1gInSc2
•	•	k?
Kostel	kostel	k1gInSc1
Navštívení	navštívení	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
•	•	k?
Kostel	kostel	k1gInSc1
Matky	matka	k1gFnSc2
Boží	božit	k5eAaImIp3nS
U	u	k7c2
Obrázku	obrázek	k1gInSc2
•	•	k?
Kostel	kostel	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
Pamětihodnosti	pamětihodnost	k1gFnSc2
</s>
<s>
Appeltův	Appeltův	k2eAgInSc1d1
dům	dům	k1gInSc1
•	•	k?
Liberecká	liberecký	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
•	•	k?
Městské	městský	k2eAgFnPc4d1
lázně	lázeň	k1gFnPc4
•	•	k?
Palác	palác	k1gInSc1
Adria	Adria	k1gFnSc1
•	•	k?
Vila	vít	k5eAaImAgFnS
Franze	Franze	k1gFnSc1
Bognera	Bogner	k1gMnSc2
Sport	sport	k1gInSc1
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1
Tygři	tygr	k1gMnPc1
Liberec	Liberec	k1gInSc1
•	•	k?
FC	FC	kA
Slovan	Slovan	k1gInSc4
Liberec	Liberec	k1gInSc1
Studium	studium	k1gNnSc1
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
pedagogická	pedagogický	k2eAgFnSc1d1
Jeronýmova	Jeronýmův	k2eAgFnSc1d1
•	•	k?
Gymnázium	gymnázium	k1gNnSc1
F.	F.	kA
X.	X.	kA
Šaldy	Šalda	k1gMnSc2
•	•	k?
Obchodní	obchodní	k2eAgFnPc1d1
akademie	akademie	k1gFnPc1
a	a	k8xC
Jazyková	jazykový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Střední	střední	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
stavební	stavební	k2eAgFnSc1d1
Liberec	Liberec	k1gInSc4
•	•	k?
Střední	střední	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
a	a	k8xC
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
a	a	k8xC
Vyšší	vysoký	k2eAgFnSc1d2
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Osobnosti	osobnost	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
představitelů	představitel	k1gMnPc2
Liberce	Liberec	k1gInSc2
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Dům	dům	k1gInSc1
obuvi	obuv	k1gFnSc2
Baťa	Baťa	k1gMnSc1
•	•	k?
Obchodní	obchodní	k2eAgInSc4d1
dům	dům	k1gInSc4
Brouk	brouk	k1gMnSc1
a	a	k8xC
Babka	babka	k1gFnSc1
•	•	k?
Krajská	krajský	k2eAgFnSc1d1
nemocnice	nemocnice	k1gFnSc1
Liberec	Liberec	k1gInSc1
•	•	k?
Okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
•	•	k?
Vazební	vazební	k2eAgFnSc1d1
věznice	věznice	k1gFnSc1
Liberec	Liberec	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010709340	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1015	#num#	k4
1740	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2008010338	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
146257970	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2008010338	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Liberec	Liberec	k1gInSc1
</s>
