<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tedy	tedy	k9	tedy
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
,	,	kIx,	,
mezigalaktický	mezigalaktický	k2eAgInSc1d1	mezigalaktický
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
temnou	temný	k2eAgFnSc4d1	temná
hmotu	hmota	k1gFnSc4	hmota
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
.	.	kIx.	.
</s>
