<s>
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gNnSc1
/	/	kIx~
The	The	k1gFnSc1
Lyre	Lyr	k1gFnSc2
of	of	k?
Orpheus	Orpheus	k1gMnSc1
</s>
<s>
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gNnSc1
/	/	kIx~
The	The	k1gFnSc1
Lyre	Lyr	k1gFnSc2
of	of	k?
OrpheusInterpretNick	OrpheusInterpretNick	k1gMnSc1
Cave	Cav	k1gFnSc2
and	and	k?
the	the	k?
Bad	Bad	k1gFnSc2
SeedsDruh	SeedsDruha	k1gFnPc2
albastudiové	albastudiový	k2eAgNnSc4d1
albumVydáno	albumVydán	k2eAgNnSc4d1
<g/>
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2004	#num#	k4
<g/>
Nahránobřezen-duben	Nahránobřezen-duben	k2eAgInSc4d1
2004	#num#	k4
<g/>
Žánrgothic	Žánrgothice	k1gInPc2
rockDélka	rockDélek	k1gMnSc2
<g/>
82	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
JazykangličtinaVydavatelstvíMute	JazykangličtinaVydavatelstvíMut	k1gInSc5
RecordsProducentNick	RecordsProducentNick	k1gInSc1
LaunayProfesionální	LaunayProfesionální	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
</s>
<s>
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
The	The	k?
Guardian	Guardian	k1gInSc1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rolling	Rolling	k1gInSc1
Stone	ston	k1gInSc5
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Entertainment	Entertainment	k1gInSc1
Weekly	Weekl	k1gInPc1
(	(	kIx(
<g/>
A	a	k9
<g/>
–	–	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nick	Nick	k1gMnSc1
Cave	Cav	k1gFnSc2
and	and	k?
the	the	k?
Bad	Bad	k1gFnSc2
Seeds	Seedsa	k1gFnPc2
chronologicky	chronologicky	k6eAd1
</s>
<s>
Nocturama	Nocturama	k1gFnSc1
<g/>
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gNnSc1
/	/	kIx~
The	The	k1gFnSc1
Lyre	Lyr	k1gFnSc2
of	of	k?
Orpheus	Orpheus	k1gMnSc1
<g/>
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
B-Sides	B-Sides	k1gMnSc1
&	&	k?
Rarities	Rarities	k1gMnSc1
<g/>
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gNnSc1
/	/	kIx~
The	The	k1gFnSc1
Lyre	Lyre	k1gFnSc1
of	of	k?
Orpheus	Orpheus	k1gInSc4
je	být	k5eAaImIp3nS
třinácté	třináctý	k4xOgNnSc1
studiové	studiový	k2eAgNnSc1d1
album	album	k1gNnSc1
australské	australský	k2eAgFnSc2d1
rockové	rockový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Nick	Nick	k1gFnPc2
Cave	Cave	k1gFnSc2
and	and	k?
the	the	k?
Bad	Bad	k1gFnSc2
Seeds	Seedsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
dvojalbum	dvojalbum	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
sedmnáct	sedmnáct	k4xCc4
písní	píseň	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
prvních	první	k4xOgFnPc2
devět	devět	k4xCc1
je	být	k5eAaImIp3nS
na	na	k7c6
disku	disco	k1gNnSc6
nazvaném	nazvaný	k2eAgNnSc6d1
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gFnPc2
a	a	k8xC
zbylých	zbylý	k2eAgNnPc2d1
osm	osm	k4xCc4
na	na	k7c6
The	The	k1gFnSc6
Lyre	Lyr	k1gInSc2
of	of	k?
Orpheus	Orpheus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
roku	rok	k1gInSc2
2004	#num#	k4
společností	společnost	k1gFnPc2
Mute	Mute	k?
Records	Recordsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
nahrávání	nahrávání	k1gNnSc1
probíhalo	probíhat	k5eAaImAgNnS
mezi	mezi	k7c7
březnem	březen	k1gInSc7
a	a	k8xC
dubnem	duben	k1gInSc7
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
v	v	k7c6
pařížském	pařížský	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
Ferber	Ferbra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Producentem	producent	k1gMnSc7
alba	album	k1gNnSc2
byl	být	k5eAaImAgMnS
Nick	Nick	k1gMnSc1
Launay	Launaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hitparádě	hitparáda	k1gFnSc6
Billboard	billboard	k1gInSc4
200	#num#	k4
se	se	k3xPyFc4
album	album	k1gNnSc1
umístilo	umístit	k5eAaPmAgNnS
na	na	k7c4
126	#num#	k4
<g/>
.	.	kIx.
příčce	příčka	k1gFnSc6
<g/>
.	.	kIx.
jde	jít	k5eAaImIp3nS
o	o	k7c4
vůbec	vůbec	k9
první	první	k4xOgNnSc4
album	album	k1gNnSc4
v	v	k7c6
historii	historie	k1gFnSc6
kapely	kapela	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
nehrál	hrát	k5eNaImAgMnS
zakládající	zakládající	k2eAgMnSc1d1
člen	člen	k1gMnSc1
Blixa	Blixa	k1gMnSc1
Bargeld	Bargeld	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gNnSc2
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
NázevAutor	NázevAutor	k1gMnSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Get	Get	k1gFnSc1
Ready	ready	k0
for	forum	k1gNnPc2
Love	lov	k1gInSc5
<g/>
“	“	k?
Nick	Nick	k1gInSc4
Cave	Cave	k1gFnPc2
<g/>
,	,	kIx,
Warren	Warrna	k1gFnPc2
Ellis	Ellis	k1gFnPc2
<g/>
,	,	kIx,
Martyn	Martyn	k1gNnSc1
P.	P.	kA
Casey	Casea	k1gFnSc2
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
Sclavunos	Sclavunos	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Cannibal	Cannibal	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Hymn	Hymn	k1gInSc1
<g/>
“	“	k?
Cave	Cave	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Hiding	Hiding	k1gInSc1
All	All	k1gMnSc2
Away	Awaa	k1gMnSc2
<g/>
“	“	k?
Cave	Cav	k1gMnSc2
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Messiah	Messiah	k1gMnSc1
Ward	Ward	k1gMnSc1
<g/>
“	“	k?
Cave	Cave	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
There	Ther	k1gMnSc5
She	She	k1gMnSc5
Goes	Goesa	k1gFnPc2
<g/>
,	,	kIx,
My	my	k3xPp1nPc1
Beautiful	Beautifula	k1gFnPc2
World	Worldo	k1gNnPc2
<g/>
“	“	k?
Cave	Cav	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Nature	Natur	k1gMnSc5
Boy	boa	k1gNnPc7
<g/>
“	“	k?
Cave	Cave	k1gFnPc2
<g/>
,	,	kIx,
Ellis	Ellis	k1gFnPc2
<g/>
,	,	kIx,
Casey	Casea	k1gFnSc2
<g/>
,	,	kIx,
Sclavunos	Sclavunos	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Abattoir	Abattoir	k1gMnSc1
Blues	blues	k1gNnSc1
<g/>
“	“	k?
Cave	Cave	k1gFnPc2
<g/>
,	,	kIx,
Ellis	Ellis	k1gFnPc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Let	let	k1gInSc1
the	the	k?
Bells	Bells	k1gInSc1
Ring	ring	k1gInSc1
<g/>
“	“	k?
Cave	Cave	k1gInSc1
<g/>
,	,	kIx,
Ellis	Ellis	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Fable	Fable	k1gInSc1
of	of	k?
the	the	k?
Brown	Brown	k1gMnSc1
Ape	Ape	k1gMnSc1
<g/>
“	“	k?
Cave	Cave	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
43	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
The	The	k?
Lyre	Lyre	k1gInSc1
of	of	k?
Orpheus	Orpheus	k1gInSc1
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
NázevAutor	NázevAutor	k1gMnSc1
</s>
<s>
Délka	délka	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
The	The	k1gFnSc1
Lyre	Lyr	k1gFnSc2
of	of	k?
Orpheus	Orpheus	k1gMnSc1
<g/>
“	“	k?
Cave	Cav	k1gFnSc2
<g/>
,	,	kIx,
Ellis	Ellis	k1gFnSc2
<g/>
,	,	kIx,
Casey	Casea	k1gFnSc2
<g/>
,	,	kIx,
Sclavunos	Sclavunos	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Breathless	Breathless	k1gInSc1
<g/>
“	“	k?
Cave	Cave	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Babe	babit	k5eAaImSgInS
<g/>
,	,	kIx,
You	You	k1gMnSc1
Turn	Turn	k1gMnSc1
Me	Me	k1gMnSc1
On	on	k3xPp3gMnSc1
<g/>
“	“	k?
Cave	Cave	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Easy	Eas	k2eAgInPc4d1
Money	Money	k1gInPc4
<g/>
“	“	k?
Cave	Cav	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Supernaturally	Supernaturall	k1gInPc1
<g/>
“	“	k?
Cave	Cav	k1gInSc2
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
37	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Spell	Spell	k1gInSc1
<g/>
“	“	k?
Cave	Cav	k1gFnPc1
<g/>
,	,	kIx,
Ellis	Ellis	k1gFnPc1
<g/>
,	,	kIx,
Casey	Casea	k1gFnPc1
<g/>
,	,	kIx,
Sclavunos	Sclavunos	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Carry	Carra	k1gFnPc4
Me	Me	k1gFnSc2
<g/>
“	“	k?
Cave	Cav	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
37	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
O	o	k7c6
Children	Childrno	k1gNnPc2
<g/>
“	“	k?
Cave	Cav	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
51	#num#	k4
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
39	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Nick	Nick	k1gMnSc1
Cave	Cav	k1gFnSc2
and	and	k?
the	the	k?
Bad	Bad	k1gFnSc2
Seeds	Seedsa	k1gFnPc2
</s>
<s>
Nick	Nick	k1gMnSc1
Cave	Cav	k1gFnSc2
–	–	k?
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
klavír	klavír	k1gInSc1
</s>
<s>
Mick	Mick	k1gMnSc1
Harvey	Harvea	k1gFnSc2
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Warren	Warrna	k1gFnPc2
Ellis	Ellis	k1gFnSc2
–	–	k?
housle	housle	k1gFnPc1
<g/>
,	,	kIx,
mandolína	mandolína	k1gFnSc1
<g/>
,	,	kIx,
buzuki	buzuki	k1gNnSc1
<g/>
,	,	kIx,
flétna	flétna	k1gFnSc1
</s>
<s>
Martyn	Martyn	k1gMnSc1
P.	P.	kA
Casey	Casea	k1gFnPc1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Conway	Conwa	k2eAgInPc1d1
Savage	Savag	k1gInPc1
–	–	k?
klavír	klavír	k1gInSc4
</s>
<s>
James	James	k1gInSc1
Johnston	Johnston	k1gInSc1
–	–	k?
varhany	varhany	k1gFnPc1
</s>
<s>
Jim	on	k3xPp3gFnPc3
Sclavunos	Sclavunos	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnPc4
</s>
<s>
Thomas	Thomas	k1gMnSc1
Wydler	Wydler	k1gMnSc1
–	–	k?
bicí	bicí	k2eAgMnSc1d1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnSc1
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1
hudebníci	hudebník	k1gMnPc1
</s>
<s>
Å	Å	k6eAd1
Bergstrø	Bergstrø	k1gInSc1
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Donovan	Donovan	k1gMnSc1
Lawrence	Lawrence	k1gFnSc2
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Geo	Geo	k?
Onayomake	Onayomak	k1gInPc1
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Lena	Lena	k1gFnSc1
Palmer	Palmra	k1gFnPc2
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Stephanie	Stephanie	k1gFnSc1
Meade	Mead	k1gInSc5
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Wendy	Wendy	k6eAd1
Rose	Rose	k1gMnSc1
–	–	k?
doprovodné	doprovodný	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
JUREK	Jurek	k1gMnSc1
<g/>
,	,	kIx,
Thom	Thom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nick	Nick	k1gInSc1
Cave	Cav	k1gFnSc2
&	&	k?
the	the	k?
Bad	Bad	k1gFnSc2
Seeds	Seedsa	k1gFnPc2
/	/	kIx~
Nick	Nick	k1gInSc1
Cave	Cav	k1gInSc2
<g/>
:	:	kIx,
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gNnSc1
<g/>
/	/	kIx~
<g/>
The	The	k1gFnSc1
Lyre	Lyr	k1gFnSc2
of	of	k?
Orpheus	Orpheus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PETRIDIS	PETRIDIS	kA
<g/>
,	,	kIx,
Alexis	Alexis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nick	Nick	k1gInSc1
Cave	Cav	k1gFnSc2
and	and	k?
the	the	k?
Bad	Bad	k1gFnSc1
Seeds	Seeds	k1gInSc1
<g/>
,	,	kIx,
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gNnSc1
<g/>
/	/	kIx~
The	The	k1gFnSc1
Lyre	Lyr	k1gFnSc2
of	of	k?
Orpheus	Orpheus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
2004-09-17	2004-09-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BLASHILL	BLASHILL	kA
<g/>
,	,	kIx,
Pat	pat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nick	Nick	k1gInSc1
Cave	Cave	k1gInSc4
<g/>
:	:	kIx,
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rolling	Rolling	k1gInSc1
Stone	ston	k1gInSc5
<g/>
,	,	kIx,
2004-11-25	2004-11-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BROWNE	BROWNE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
latest	latest	k1gMnSc1
by	by	kYmCp3nS
Nick	Nicka	k1gFnPc2
Cave	Cave	k1gNnSc2
and	and	k?
Leonard	Leonard	k1gMnSc1
Cohen	Cohen	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gMnSc1
Weekly	Weekl	k1gInPc4
<g/>
,	,	kIx,
2005-04-03	2005-04-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Nick	Nick	k1gMnSc1
Cave	Cav	k1gFnSc2
and	and	k?
the	the	k?
Bad	Bad	k1gFnSc2
Seeds	Seedsa	k1gFnPc2
Nick	Nick	k1gInSc1
Cave	Cav	k1gInSc2
•	•	k?
Warren	Warrna	k1gFnPc2
Ellis	Ellis	k1gFnSc1
•	•	k?
Martyn	Martyno	k1gNnPc2
P.	P.	kA
Casey	Casea	k1gFnSc2
•	•	k?
Toby	Toba	k1gFnSc2
Dammit	Dammita	k1gFnPc2
•	•	k?
Jim	on	k3xPp3gFnPc3
Sclavunos	Sclavunos	k1gInSc1
•	•	k?
George	Georg	k1gMnSc2
Vjestica	Vjesticus	k1gMnSc2
•	•	k?
Thomas	Thomas	k1gMnSc1
WydlerBarry	WydlerBarra	k1gFnSc2
Adamson	Adamson	k1gMnSc1
•	•	k?
Blixa	Blixa	k1gMnSc1
Bargeld	Bargeld	k1gMnSc1
•	•	k?
Mick	Mick	k1gMnSc1
Harvey	Harvea	k1gFnSc2
•	•	k?
James	James	k1gInSc1
Johnston	Johnston	k1gInSc1
•	•	k?
Anita	Anit	k1gMnSc2
Lane	Lan	k1gMnSc2
•	•	k?
Kid	Kid	k1gMnSc1
Congo	Congo	k1gMnSc1
Powers	Powersa	k1gFnPc2
•	•	k?
Hugo	Hugo	k1gMnSc1
Race	Rac	k1gMnSc2
•	•	k?
Conway	Conwaa	k1gMnSc2
Savage	Savag	k1gMnSc2
•	•	k?
Roland	Roland	k1gInSc1
Wolf	Wolf	k1gMnSc1
Studiová	studiový	k2eAgNnPc5d1
alba	album	k1gNnPc4
</s>
<s>
From	Fro	k1gNnSc7
Her	hra	k1gFnPc2
to	ten	k3xDgNnSc1
Eternity	eternit	k1gInPc4
•	•	k?
The	The	k1gFnSc2
Firstborn	Firstborn	k1gMnSc1
Is	Is	k1gMnSc1
Dead	Dead	k1gMnSc1
•	•	k?
Kicking	Kicking	k1gInSc1
Against	Against	k1gInSc1
the	the	k?
Pricks	Pricks	k1gInSc1
•	•	k?
Your	Your	k1gInSc1
Funeral	Funeral	k1gFnSc1
<g/>
…	…	k?
My	my	k3xPp1nPc1
Trial	trial	k1gInSc4
•	•	k?
Tender	tender	k1gInSc1
Prey	Prea	k1gFnSc2
•	•	k?
The	The	k1gFnSc2
Good	Gooda	k1gFnPc2
Son	son	k1gInSc1
•	•	k?
Henry	henry	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Dream	Dream	k1gInSc1
•	•	k?
Let	let	k1gInSc1
Love	lov	k1gInSc5
In	In	k1gMnPc2
•	•	k?
Murder	Murdero	k1gNnPc2
Ballads	Ballads	k1gInSc1
•	•	k?
The	The	k1gMnSc1
Boatman	Boatman	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Call	Call	k1gInSc1
•	•	k?
No	no	k9
More	mor	k1gInSc5
Shall	Shall	k1gInSc4
We	We	k1gFnSc3
Part	part	k1gInSc1
•	•	k?
Nocturama	Nocturama	k1gFnSc1
•	•	k?
Abattoir	Abattoir	k1gInSc1
Blues	blues	k1gNnSc1
/	/	kIx~
The	The	k1gFnSc1
Lyre	Lyr	k1gFnSc2
of	of	k?
Orpheus	Orpheus	k1gMnSc1
•	•	k?
Dig	Dig	k1gMnSc1
<g/>
,	,	kIx,
Lazarus	Lazarus	k1gMnSc1
<g/>
,	,	kIx,
Dig	Dig	k1gMnSc1
<g/>
!!!	!!!	k?
•	•	k?
Push	Push	k1gMnSc1
the	the	k?
Sky	Sky	k1gMnSc1
Away	Awaa	k1gFnSc2
•	•	k?
Skeleton	skeleton	k1gInSc1
Tree	Tree	k1gFnSc1
•	•	k?
Ghosteen	Ghosteen	k1gInSc4
Koncertní	koncertní	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Live	Live	k6eAd1
Seeds	Seeds	k1gInSc1
•	•	k?
The	The	k1gFnSc2
Abattoir	Abattoira	k1gFnPc2
Blues	blues	k1gNnSc1
Tour	Tour	k1gMnSc1
•	•	k?
Live	Live	k1gInSc1
at	at	k?
the	the	k?
Royal	Royal	k1gMnSc1
Albert	Albert	k1gMnSc1
Hall	Hall	k1gMnSc1
•	•	k?
Live	Live	k1gInSc1
from	from	k1gInSc1
KCRW	KCRW	kA
Kompilační	kompilační	k2eAgFnSc2d1
alba	album	k1gNnPc4
</s>
<s>
The	The	k?
Best	Best	k1gMnSc1
of	of	k?
Nick	Nick	k1gMnSc1
Cave	Cav	k1gFnSc2
and	and	k?
The	The	k1gMnSc1
Bad	Bad	k1gMnSc1
Seeds	Seedsa	k1gFnPc2
•	•	k?
B-Sides	B-Sides	k1gMnSc1
&	&	k?
Rarities	Rarities	k1gMnSc1
•	•	k?
Lovely	Lovela	k1gFnSc2
Creatures	Creatures	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
