<s>
Po	po	k7c6	po
Filipově	Filipův	k2eAgFnSc6d1	Filipova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
věnoval	věnovat	k5eAaImAgMnS	věnovat
vědě	věda	k1gFnSc3	věda
a	a	k8xC	a
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
335	[number]	k4	335
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
školu	škola	k1gFnSc4	škola
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Lykeion	Lykeion	k1gInSc4	Lykeion
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
peripatetická	peripatetický	k2eAgFnSc1d1	peripatetická
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
z	z	k7c2	z
názvu	název	k1gInSc2	název
krytého	krytý	k2eAgNnSc2d1	kryté
sloupořadí	sloupořadí	k1gNnSc2	sloupořadí
peripatos	peripatosa	k1gFnPc2	peripatosa
(	(	kIx(	(
<g/>
π	π	k?	π
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
žáci	žák	k1gMnPc1	žák
učili	učit	k5eAaImAgMnP	učit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
od	od	k7c2	od
slovesa	sloveso	k1gNnSc2	sloveso
peripatein	peripatein	k1gInSc1	peripatein
(	(	kIx(	(
<g/>
procházet	procházet	k5eAaImF	procházet
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
se	se	k3xPyFc4	se
prý	prý	k9	prý
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
žáky	žák	k1gMnPc7	žák
při	při	k7c6	při
výuce	výuka	k1gFnSc6	výuka
procházel	procházet	k5eAaImAgMnS	procházet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
