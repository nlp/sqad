<s>
Basingstoke	Basingstoke	k6eAd1
</s>
<s>
Basingstoke	Basingstoke	k1gFnSc1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
16	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
1	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Basingstoke	Basingstoke	k6eAd1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
29,2	29,2	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
107	#num#	k4
355	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
3	#num#	k4
678,4	678,4	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.basingstoke.gov.uk	www.basingstoke.gov.uk	k6eAd1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
01256	#num#	k4
PSČ	PSČ	kA
</s>
<s>
RG	RG	kA
<g/>
21	#num#	k4
<g/>
,	,	kIx,
RG	RG	kA
<g/>
22	#num#	k4
<g/>
,	,	kIx,
RG	RG	kA
<g/>
23	#num#	k4
<g/>
,	,	kIx,
RG24	RG24	k1gFnSc1
a	a	k8xC
RG25	RG25	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Basingstoke	Basingstoke	k1gInSc1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgNnSc1d3
město	město	k1gNnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
anglickém	anglický	k2eAgNnSc6d1
hrabství	hrabství	k1gNnSc6
Hampshire	Hampshire	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozprostírá	rozprostírat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
údolí	údolí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pramení	pramenit	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Loddon	Loddona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
48	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Southamptonu	Southampton	k1gInSc2
<g/>
,	,	kIx,
77	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Londýna	Londýn	k1gInSc2
a	a	k8xC
31	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Winchesteru	Winchester	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
ve	v	k7c6
městě	město	k1gNnSc6
žilo	žít	k5eAaImAgNnS
107	#num#	k4
355	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Basingstoke	Basingstok	k1gInSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nepočítaje	nepočítaje	k7c4
města	město	k1gNnPc4
se	s	k7c7
statusem	status	k1gInSc7
"	"	kIx"
<g/>
city	cit	k1gInPc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
na	na	k7c6
území	území	k1gNnSc6
hrabství	hrabství	k1gNnSc2
leží	ležet	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
spravována	spravovat	k5eAaImNgNnP
nezávisle	závisle	k6eNd1
(	(	kIx(
<g/>
Southampton	Southampton	k1gInSc1
<g/>
,	,	kIx,
Portsmouth	Portsmouth	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
1037066	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4789463-5	4789463-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
87112514	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
156029898	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
87112514	#num#	k4
</s>
