<s>
Geologie	geologie	k1gFnSc1	geologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
o	o	k7c4	o
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
její	její	k3xOp3gNnSc4	její
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
historický	historický	k2eAgInSc4d1	historický
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
