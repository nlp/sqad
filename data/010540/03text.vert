<p>
<s>
Geologie	geologie	k1gFnSc1	geologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
o	o	k7c4	o
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
její	její	k3xOp3gNnSc4	její
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
historický	historický	k2eAgInSc4d1	historický
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
pochody	pochod	k1gInPc1	pochod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
uvnitř	uvnitř	k7c2	uvnitř
planety	planeta	k1gFnSc2	planeta
i	i	k9	i
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
(	(	kIx(	(
<g/>
geologické	geologický	k2eAgInPc1d1	geologický
děje	děj	k1gInPc1	děj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vědu	věda	k1gFnSc4	věda
deskriptivní	deskriptivní	k2eAgFnSc4d1	deskriptivní
(	(	kIx(	(
<g/>
popisnou	popisný	k2eAgFnSc4d1	popisná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
analytickou	analytický	k2eAgFnSc7d1	analytická
(	(	kIx(	(
<g/>
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
výklad	výklad	k1gInSc1	výklad
dějů	děj	k1gInPc2	děj
<g/>
)	)	kIx)	)
a	a	k8xC	a
historickou	historický	k2eAgFnSc4d1	historická
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vědu	věda	k1gFnSc4	věda
časoprostorovou	časoprostorový	k2eAgFnSc4d1	časoprostorová
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zhodnocuje	zhodnocovat	k5eAaImIp3nS	zhodnocovat
jevy	jev	k1gInPc4	jev
určitého	určitý	k2eAgInSc2d1	určitý
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
geologie	geologie	k1gFnSc1	geologie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
geologické	geologický	k2eAgInPc4d1	geologický
obory	obor	k1gInPc4	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
geologie	geologie	k1gFnSc2	geologie
prvně	prvně	k?	prvně
použil	použít	k5eAaPmAgMnS	použít
Jean-André	Jean-Andrý	k2eAgNnSc4d1	Jean-Andrý
Deluc	Deluc	k1gFnSc1	Deluc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
Horace-Bénédict	Horace-Bénédict	k1gInSc4	Horace-Bénédict
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
použito	použít	k5eAaPmNgNnS	použít
jako	jako	k8xC	jako
termín	termín	k1gInSc1	termín
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
této	tento	k3xDgFnSc2	tento
vědní	vědní	k2eAgFnSc2d1	vědní
disciplíny	disciplína	k1gFnSc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
používán	používán	k2eAgInSc1d1	používán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
třetího	třetí	k4xOgNnSc2	třetí
vydání	vydání	k1gNnSc2	vydání
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
Britannica	Britannicum	k1gNnSc2	Britannicum
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Geologie	geologie	k1gFnSc1	geologie
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
členěna	členit	k5eAaImNgFnS	členit
na	na	k7c4	na
specializované	specializovaný	k2eAgInPc4d1	specializovaný
podobory	podobor	k1gInPc4	podobor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Geochemie	geochemie	k1gFnSc1	geochemie
–	–	k?	–
obor	obor	k1gInSc4	obor
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
sledování	sledování	k1gNnSc4	sledování
rozšíření	rozšíření	k1gNnSc2	rozšíření
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
poznání	poznání	k1gNnSc2	poznání
zákonů	zákon	k1gInPc2	zákon
jejich	jejich	k3xOp3gNnSc4	jejich
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
<g/>
Dynamická	dynamický	k2eAgFnSc1d1	dynamická
geologie	geologie	k1gFnSc1	geologie
–	–	k?	–
část	část	k1gFnSc1	část
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
dynamickými	dynamický	k2eAgInPc7d1	dynamický
pochody	pochod	k1gInPc7	pochod
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tyto	tento	k3xDgInPc1	tento
pochody	pochod	k1gInPc1	pochod
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
endogenní	endogenní	k2eAgFnSc4d1	endogenní
a	a	k8xC	a
exogenní	exogenní	k2eAgFnSc4d1	exogenní
geologii	geologie	k1gFnSc4	geologie
<g/>
.	.	kIx.	.
</s>
<s>
Endogenní	endogenní	k2eAgInPc1d1	endogenní
pochody	pochod	k1gInPc1	pochod
jsou	být	k5eAaImIp3nP	být
svázány	svázat	k5eAaPmNgInP	svázat
s	s	k7c7	s
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
pochody	pochod	k1gInPc7	pochod
v	v	k7c6	v
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
například	například	k6eAd1	například
k	k	k7c3	k
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
,	,	kIx,	,
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
,	,	kIx,	,
orogenezi	orogeneze	k1gFnSc3	orogeneze
anebo	anebo	k8xC	anebo
pohybu	pohyb	k1gInSc3	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Exogenní	exogenní	k2eAgInPc1d1	exogenní
pochody	pochod	k1gInPc1	pochod
jsou	být	k5eAaImIp3nP	být
spjaty	spjat	k2eAgInPc1d1	spjat
s	s	k7c7	s
činností	činnost	k1gFnSc7	činnost
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
zvětráváním	zvětrávání	k1gNnSc7	zvětrávání
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strukturní	strukturní	k2eAgFnSc1d1	strukturní
geologie	geologie	k1gFnSc1	geologie
–	–	k?	–
obor	obor	k1gInSc4	obor
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
studiem	studio	k1gNnSc7	studio
geologických	geologický	k2eAgFnPc2d1	geologická
struktur	struktura	k1gFnPc2	struktura
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
vrásy	vrása	k1gFnPc1	vrása
<g/>
,	,	kIx,	,
zlomy	zlom	k1gInPc1	zlom
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
jejich	jejich	k3xOp3gInSc7	jejich
popisem	popis	k1gInSc7	popis
a	a	k8xC	a
výkladem	výklad	k1gInSc7	výklad
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ke	k	k7c3	k
vzniku	vznik	k1gInSc2	vznik
těchto	tento	k3xDgFnPc2	tento
struktur	struktura	k1gFnPc2	struktura
vedly	vést	k5eAaImAgFnP	vést
<g/>
.	.	kIx.	.
</s>
<s>
Zpětně	zpětně	k6eAd1	zpětně
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
dohledat	dohledat	k5eAaPmF	dohledat
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
panovaly	panovat	k5eAaImAgFnP	panovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
geologie	geologie	k1gFnSc1	geologie
–	–	k?	–
obor	obor	k1gInSc4	obor
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zmapovat	zmapovat	k5eAaPmF	zmapovat
časové	časový	k2eAgFnPc4d1	časová
události	událost	k1gFnPc4	událost
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
pomůcky	pomůcka	k1gFnPc4	pomůcka
datace	datace	k1gFnSc2	datace
patří	patřit	k5eAaImIp3nS	patřit
paleontologie	paleontologie	k1gFnSc1	paleontologie
a	a	k8xC	a
objevování	objevování	k1gNnSc1	objevování
zkamenělých	zkamenělý	k2eAgFnPc2d1	zkamenělá
fosílií	fosílie	k1gFnPc2	fosílie
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
ve	v	k7c6	v
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
pomůcky	pomůcka	k1gFnPc4	pomůcka
historické	historický	k2eAgFnSc2d1	historická
geologie	geologie	k1gFnSc2	geologie
patří	patřit	k5eAaImIp3nS	patřit
stratigrafická	stratigrafický	k2eAgFnSc1d1	stratigrafická
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
historie	historie	k1gFnSc1	historie
planety	planeta	k1gFnSc2	planeta
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedimentární	sedimentární	k2eAgFnSc1d1	sedimentární
geologie	geologie	k1gFnSc1	geologie
–	–	k?	–
obor	obor	k1gInSc4	obor
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
sedimentárním	sedimentární	k2eAgFnPc3d1	sedimentární
horninám	hornina	k1gFnPc3	hornina
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc3	jejich
vzniku	vznik	k1gInSc3	vznik
a	a	k8xC	a
strukturám	struktura	k1gFnPc3	struktura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
exogenní	exogenní	k2eAgInPc4d1	exogenní
pochody	pochod	k1gInPc4	pochod
a	a	k8xC	a
historickou	historický	k2eAgFnSc4d1	historická
geologii	geologie	k1gFnSc4	geologie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
určovat	určovat	k5eAaImF	určovat
stáří	stáří	k1gNnSc4	stáří
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ložisková	ložiskový	k2eAgFnSc1d1	ložisková
geologie	geologie	k1gFnSc1	geologie
–	–	k?	–
obor	obor	k1gInSc4	obor
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
studiem	studio	k1gNnSc7	studio
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
nerud	neruda	k1gFnPc2	neruda
a	a	k8xC	a
kaustobiolitů	kaustobiolit	k1gInPc2	kaustobiolit
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
jejich	jejich	k3xOp3gFnSc1	jejich
akumulace	akumulace	k1gFnSc1	akumulace
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
rentabilní	rentabilní	k2eAgMnSc1d1	rentabilní
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
úkoly	úkol	k1gInPc4	úkol
patří	patřit	k5eAaImIp3nS	patřit
mapovat	mapovat	k5eAaImF	mapovat
<g/>
,	,	kIx,	,
odhadovat	odhadovat	k5eAaImF	odhadovat
a	a	k8xC	a
oceňovat	oceňovat	k5eAaImF	oceňovat
objevená	objevený	k2eAgNnPc4d1	objevené
ložiska	ložisko	k1gNnPc4	ložisko
a	a	k8xC	a
určovat	určovat	k5eAaImF	určovat
vhodnost	vhodnost	k1gFnSc4	vhodnost
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
geologie	geologie	k1gFnSc1	geologie
–	–	k?	–
obor	obor	k1gInSc4	obor
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
menší	malý	k2eAgInPc4d2	menší
geologické	geologický	k2eAgInPc4d1	geologický
celky	celek	k1gInPc4	celek
v	v	k7c6	v
zájmové	zájmový	k2eAgFnSc6d1	zájmová
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
podrobný	podrobný	k2eAgInSc4d1	podrobný
geologický	geologický	k2eAgInSc4d1	geologický
obraz	obraz	k1gInSc4	obraz
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
geologickým	geologický	k2eAgNnSc7d1	geologické
mapováním	mapování	k1gNnSc7	mapování
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
trojrozměrná	trojrozměrný	k2eAgFnSc1d1	trojrozměrná
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
sledované	sledovaný	k2eAgFnSc6d1	sledovaná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geonika	Geonik	k1gMnSc4	Geonik
–	–	k?	–
obor	obor	k1gInSc4	obor
geologie	geologie	k1gFnSc2	geologie
sledující	sledující	k2eAgInPc4d1	sledující
dopady	dopad	k1gInPc4	dopad
činností	činnost	k1gFnSc7	činnost
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
vyvolaných	vyvolaný	k2eAgFnPc2d1	vyvolaná
aktivit	aktivita	k1gFnPc2	aktivita
na	na	k7c4	na
přírodní	přírodní	k2eAgNnSc4d1	přírodní
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
interakci	interakce	k1gFnSc4	interakce
přírodního	přírodní	k2eAgNnSc2d1	přírodní
a	a	k8xC	a
antropogenního	antropogenní	k2eAgNnSc2d1	antropogenní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
geologie	geologie	k1gFnSc1	geologie
–	–	k?	–
obor	obor	k1gInSc4	obor
geologie	geologie	k1gFnSc2	geologie
využívající	využívající	k2eAgInSc4d1	využívající
poznatků	poznatek	k1gInPc2	poznatek
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
geologických	geologický	k2eAgInPc2d1	geologický
a	a	k8xC	a
technických	technický	k2eAgInPc2d1	technický
oborů	obor	k1gInPc2	obor
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
jejich	jejich	k3xOp3gNnSc2	jejich
využití	využití	k1gNnSc2	využití
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
různých	různý	k2eAgInPc2d1	různý
praktických	praktický	k2eAgInPc2d1	praktický
úkolů	úkol	k1gInPc2	úkol
např.	např.	kA	např.
ve	v	k7c6	v
stavebním	stavební	k2eAgNnSc6d1	stavební
inženýrství	inženýrství	k1gNnSc6	inženýrství
<g/>
,	,	kIx,	,
hornictví	hornictví	k1gNnSc6	hornictví
<g/>
,	,	kIx,	,
vodohospodářství	vodohospodářství	k1gNnSc2	vodohospodářství
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
průmyslu	průmysl	k1gInSc2	průmysl
atd.	atd.	kA	atd.
Jeho	jeho	k3xOp3gMnSc1	jeho
nástroji	nástroj	k1gInPc7	nástroj
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
tyto	tento	k3xDgInPc1	tento
aplikované	aplikovaný	k2eAgInPc1d1	aplikovaný
geologické	geologický	k2eAgInPc1d1	geologický
obory	obor	k1gInPc1	obor
<g/>
:	:	kIx,	:
hydrogeologie	hydrogeologie	k1gFnSc1	hydrogeologie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
geologie	geologie	k1gFnSc1	geologie
a	a	k8xC	a
užitá	užitý	k2eAgFnSc1d1	užitá
geofyzika	geofyzika	k1gFnSc1	geofyzika
<g/>
.	.	kIx.	.
<g/>
Geologie	geologie	k1gFnSc1	geologie
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
oborů	obor	k1gInPc2	obor
planetologie	planetologie	k1gFnSc2	planetologie
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
aplikací	aplikace	k1gFnSc7	aplikace
poznatků	poznatek	k1gInPc2	poznatek
a	a	k8xC	a
teorií	teorie	k1gFnPc2	teorie
např.	např.	kA	např.
geofyziky	geofyzika	k1gFnSc2	geofyzika
ale	ale	k8xC	ale
i	i	k9	i
dalších	další	k2eAgFnPc2d1	další
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
věd	věda	k1gFnPc2	věda
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
objektů	objekt	k1gInPc2	objekt
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
)	)	kIx)	)
a	a	k8xC	a
ještě	ještě	k9	ještě
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
měřítku	měřítko	k1gNnSc6	měřítko
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
kosmické	kosmický	k2eAgFnPc4d1	kosmická
vědy	věda	k1gFnPc4	věda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geologické	geologický	k2eAgInPc1d1	geologický
obory	obor	k1gInPc1	obor
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
geografie	geografie	k1gFnSc1	geografie
</s>
</p>
<p>
<s>
Geoetika	Geoetika	k1gFnSc1	Geoetika
</s>
</p>
<p>
<s>
Geochemie	geochemie	k1gFnSc1	geochemie
</s>
</p>
<p>
<s>
Geofyzika	geofyzika	k1gFnSc1	geofyzika
</s>
</p>
<p>
<s>
Geoinformatika	Geoinformatika	k1gFnSc1	Geoinformatika
</s>
</p>
<p>
<s>
Geomorfologie	geomorfologie	k1gFnSc1	geomorfologie
</s>
</p>
<p>
<s>
Geonika	Geonik	k1gMnSc4	Geonik
</s>
</p>
<p>
<s>
Geotechnika	geotechnika	k1gFnSc1	geotechnika
</s>
</p>
<p>
<s>
Gemologie	Gemologie	k1gFnSc1	Gemologie
</s>
</p>
<p>
<s>
Glaciologie	glaciologie	k1gFnSc1	glaciologie
</s>
</p>
<p>
<s>
Inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
geologie	geologie	k1gFnSc1	geologie
</s>
</p>
<p>
<s>
Hydrogeologie	hydrogeologie	k1gFnSc1	hydrogeologie
</s>
</p>
<p>
<s>
Karsologie	Karsologie	k1gFnSc1	Karsologie
</s>
</p>
<p>
<s>
Ložisková	ložiskový	k2eAgFnSc1d1	ložisková
geologie	geologie	k1gFnSc1	geologie
</s>
</p>
<p>
<s>
Mineralogie	mineralogie	k1gFnSc1	mineralogie
</s>
</p>
<p>
<s>
Petrologie	petrologie	k1gFnSc1	petrologie
</s>
</p>
<p>
<s>
Paleontologie	paleontologie	k1gFnSc1	paleontologie
</s>
</p>
<p>
<s>
Paleogeografie	paleogeografie	k1gFnSc1	paleogeografie
</s>
</p>
<p>
<s>
Paleoklimatologie	paleoklimatologie	k1gFnSc1	paleoklimatologie
</s>
</p>
<p>
<s>
Paleomagnetismus	Paleomagnetismus	k1gInSc1	Paleomagnetismus
</s>
</p>
<p>
<s>
Vulkanologie	vulkanologie	k1gFnSc1	vulkanologie
</s>
</p>
<p>
<s>
Sedimentologie	Sedimentologie	k1gFnSc1	Sedimentologie
</s>
</p>
<p>
<s>
Stratigrafie	stratigrafie	k1gFnSc1	stratigrafie
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Svoboda	svoboda	k1gFnSc1	svoboda
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Encyklopedický	encyklopedický	k2eAgInSc1d1	encyklopedický
slovník	slovník	k1gInSc1	slovník
geologických	geologický	k2eAgFnPc2d1	geologická
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Geologický	geologický	k2eAgInSc1d1	geologický
čas	čas	k1gInSc1	čas
</s>
</p>
<p>
<s>
Stratifikace	stratifikace	k1gFnSc1	stratifikace
Země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
Geologické	geologický	k2eAgInPc1d1	geologický
děje	děj	k1gInPc1	děj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
geologie	geologie	k1gFnSc2	geologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Geologie	geologie	k1gFnSc2	geologie
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
geologie	geologie	k1gFnSc2	geologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Geologie	geologie	k1gFnSc2	geologie
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
OneGeology-Europe	OneGeology-Europ	k1gMnSc5	OneGeology-Europ
</s>
</p>
