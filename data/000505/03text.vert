<s>
Chrám	chrám	k1gInSc1	chrám
Sagrada	Sagrada	k1gFnSc1	Sagrada
Familia	Familius	k1gMnSc2	Familius
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
chrám	chrám	k1gInSc1	chrám
Svaté	svatý	k2eAgFnSc2d1	svatá
rodiny	rodina	k1gFnSc2	rodina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Španělska	Španělsko	k1gNnSc2	Španělsko
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1	stavební
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
trvají	trvat	k5eAaImIp3nP	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
stavbou	stavba	k1gFnSc7	stavba
slavného	slavný	k2eAgMnSc2d1	slavný
architekta	architekt	k1gMnSc2	architekt
Antonia	Antonio	k1gMnSc2	Antonio
Gaudího	Gaudí	k1gMnSc2	Gaudí
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
neobyčejné	obyčejný	k2eNgNnSc1d1	neobyčejné
stavební	stavební	k2eAgNnSc1d1	stavební
dílo	dílo	k1gNnSc1	dílo
mělo	mít	k5eAaImAgNnS	mít
podnítit	podnítit	k5eAaPmF	podnítit
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
učení	učení	k1gNnSc3	učení
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
papežem	papež	k1gMnSc7	papež
Benediktem	Benedikt	k1gMnSc7	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
titul	titul	k1gInSc1	titul
basillica	basillica	k6eAd1	basillica
minor	minor	k2eAgInSc1d1	minor
<g/>
.	.	kIx.	.
</s>
<s>
Gaudího	Gaudí	k1gMnSc4	Gaudí
práce	práce	k1gFnSc2	práce
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
vlivy	vliv	k1gInPc4	vliv
maurské	maurský	k2eAgFnSc2d1	maurská
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
Art	Art	k1gFnSc4	Art
nouveau	nouveaus	k1gInSc2	nouveaus
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
architektuře	architektura	k1gFnSc3	architektura
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
individuální	individuální	k2eAgMnSc1d1	individuální
a	a	k8xC	a
nekonvenční	konvenční	k2eNgInSc1d1	nekonvenční
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
přesných	přesný	k2eAgInPc2d1	přesný
projektů	projekt	k1gInPc2	projekt
zhotovoval	zhotovovat	k5eAaImAgMnS	zhotovovat
neostré	ostrý	k2eNgFnPc4d1	neostrá
skici	skica	k1gFnPc4	skica
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
staveniště	staveniště	k1gNnSc4	staveniště
a	a	k8xC	a
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
všechny	všechen	k3xTgInPc4	všechen
detaily	detail	k1gInPc4	detail
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
mohl	moct	k5eAaImAgInS	moct
případně	případně	k6eAd1	případně
pozměnit	pozměnit	k5eAaPmF	pozměnit
<g/>
.	.	kIx.	.
</s>
<s>
Gaudí	Gaudí	k1gNnSc4	Gaudí
převzal	převzít	k5eAaPmAgInS	převzít
projekt	projekt	k1gInSc1	projekt
Sagrada	Sagrada	k1gFnSc1	Sagrada
Familia	Familia	k1gFnSc1	Familia
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
raném	raný	k2eAgNnSc6d1	rané
stadiu	stadion	k1gNnSc6	stadion
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaImAgMnS	věnovat
mu	on	k3xPp3gMnSc3	on
hodně	hodně	k6eAd1	hodně
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
nemožné	nemožná	k1gFnPc4	nemožná
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
v	v	k7c6	v
jeho	jeho	k3xOp3gMnSc6	jeho
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Gaudí	Gaudí	k1gNnSc4	Gaudí
nezůstal	zůstat	k5eNaPmAgInS	zůstat
u	u	k7c2	u
zbarvení	zbarvení	k1gNnPc2	zbarvení
přírodního	přírodní	k2eAgInSc2d1	přírodní
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
rád	rád	k6eAd1	rád
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
jeho	jeho	k3xOp3gFnPc2	jeho
prací	práce	k1gFnPc2	práce
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
používání	používání	k1gNnSc4	používání
různých	různý	k2eAgInPc2d1	různý
tónů	tón	k1gInPc2	tón
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Gaudí	Gaudí	k1gNnSc4	Gaudí
zamýšlel	zamýšlet	k5eAaImAgInS	zamýšlet
ze	z	k7c2	z
Sagrady	Sagrada	k1gFnSc2	Sagrada
Familii	Familie	k1gFnSc4	Familie
vybudovat	vybudovat	k5eAaPmF	vybudovat
"	"	kIx"	"
<g/>
poslední	poslední	k2eAgFnSc4d1	poslední
velkou	velký	k2eAgFnSc4d1	velká
svatyni	svatyně	k1gFnSc4	svatyně
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
katedrála	katedrála	k1gFnSc1	katedrála
velmi	velmi	k6eAd1	velmi
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejpromyšlenějších	promyšlený	k2eAgInPc2d3	nejpromyšlenější
znaků	znak	k1gInPc2	znak
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
12	[number]	k4	12
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
4	[number]	k4	4
evangelisty	evangelista	k1gMnSc2	evangelista
<g/>
,	,	kIx,	,
Pannu	Panna	k1gFnSc4	Panna
Marii	Maria	k1gFnSc4	Maria
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Věže	věž	k1gFnPc1	věž
reprezentující	reprezentující	k2eAgMnPc4d1	reprezentující
evangelisty	evangelista	k1gMnPc4	evangelista
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
doplněny	doplnit	k5eAaPmNgInP	doplnit
sochami	socha	k1gFnPc7	socha
zpodobňujícími	zpodobňující	k2eAgFnPc7d1	zpodobňující
tradiční	tradiční	k2eAgInSc4d1	tradiční
symboly	symbol	k1gInPc4	symbol
těchto	tento	k3xDgMnPc2	tento
světců	světec	k1gMnPc2	světec
<g/>
:	:	kIx,	:
býk	býk	k1gMnSc1	býk
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anděl	anděl	k1gMnSc1	anděl
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Matouš	Matouš	k1gMnSc1	Matouš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orel	orel	k1gMnSc1	orel
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
<g/>
)	)	kIx)	)
a	a	k8xC	a
lev	lev	k1gMnSc1	lev
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Marek	Marek	k1gMnSc1	Marek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
věž	věž	k1gFnSc1	věž
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
bude	být	k5eAaImBp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
obrovským	obrovský	k2eAgInSc7d1	obrovský
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gaudí	Gaudí	k1gNnSc4	Gaudí
také	také	k9	také
chtěl	chtít	k5eAaImAgMnS	chtít
vybavit	vybavit	k5eAaPmF	vybavit
chrám	chrám	k1gInSc4	chrám
třemi	tři	k4xCgFnPc7	tři
monumentálními	monumentální	k2eAgFnPc7d1	monumentální
fasádami	fasáda	k1gFnPc7	fasáda
<g/>
:	:	kIx,	:
Narození	narození	k1gNnSc1	narození
<g/>
,	,	kIx,	,
Umučení	umučení	k1gNnSc1	umučení
a	a	k8xC	a
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc1	zmrtvýchvstání
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
své	své	k1gNnSc4	své
4	[number]	k4	4
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dostavěna	dostavět	k5eAaPmNgFnS	dostavět
pouze	pouze	k6eAd1	pouze
východní	východní	k2eAgFnSc1d1	východní
fasáda	fasáda	k1gFnSc1	fasáda
-	-	kIx~	-
Narození	narození	k1gNnSc1	narození
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholech	vrchol	k1gInPc6	vrchol
jejích	její	k3xOp3gFnPc2	její
věží	věž	k1gFnPc2	věž
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
geometrické	geometrický	k2eAgInPc1d1	geometrický
tvary	tvar	k1gInPc1	tvar
-	-	kIx~	-
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
stavbě	stavba	k1gFnSc6	stavba
byl	být	k5eAaImAgInS	být
Gaudí	Gaudí	k1gNnSc4	Gaudí
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
kubismem	kubismus	k1gInSc7	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
Dekorace	dekorace	k1gFnSc1	dekorace
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
liturgických	liturgický	k2eAgInPc2d1	liturgický
textů	text	k1gInPc2	text
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Hosanna	Hosanna	k1gFnSc1	Hosanna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Gloria	Gloria	k1gFnSc1	Gloria
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Sanctus	Sanctus	k1gInSc1	Sanctus
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
dveřích	dveře	k1gFnPc6	dveře
fasády	fasáda	k1gFnSc2	fasáda
Narození	narození	k1gNnSc2	narození
můžeme	moct	k5eAaImIp1nP	moct
číst	číst	k5eAaImF	číst
různé	různý	k2eAgInPc4d1	různý
texty	text	k1gInPc4	text
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
včetně	včetně	k7c2	včetně
katalánštiny	katalánština	k1gFnSc2	katalánština
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dokončení	dokončení	k1gNnSc6	dokončení
kostela	kostel	k1gInSc2	kostel
probíhala	probíhat	k5eAaImAgFnS	probíhat
řada	řada	k1gFnSc1	řada
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokončení	dokončení	k1gNnSc1	dokončení
by	by	kYmCp3nP	by
bylo	být	k5eAaImAgNnS	být
jako	jako	k9	jako
kdybychom	kdyby	kYmCp1nP	kdyby
Venuši	Venuše	k1gFnSc6	Venuše
Mélské	Mélská	k1gFnSc6	Mélská
přidělali	přidělat	k5eAaPmAgMnP	přidělat
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
by	by	kYmCp3nP	by
s	s	k7c7	s
dostavbou	dostavba	k1gFnSc7	dostavba
jen	jen	k6eAd1	jen
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
úplně	úplně	k6eAd1	úplně
nová	nový	k2eAgFnSc1d1	nová
fasáda	fasáda	k1gFnSc1	fasáda
představující	představující	k2eAgFnSc1d1	představující
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
místy	místy	k6eAd1	místy
velice	velice	k6eAd1	velice
nepodobná	podobný	k2eNgFnSc1d1	nepodobná
Gaudího	Gaudí	k2eAgInSc2d1	Gaudí
dílu	díl	k1gInSc2	díl
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
myšlence	myšlenka	k1gFnSc3	myšlenka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Celkové	celkový	k2eAgNnSc1d1	celkové
dokončení	dokončení	k1gNnSc1	dokončení
katedrály	katedrála	k1gFnSc2	katedrála
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2026	[number]	k4	2026
-	-	kIx~	-
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
smrti	smrt	k1gFnSc2	smrt
Antonia	Antonio	k1gMnSc2	Antonio
Gaudího	Gaudí	k1gMnSc2	Gaudí
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
celkem	celkem	k6eAd1	celkem
144	[number]	k4	144
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Architekti	architekt	k1gMnPc1	architekt
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Gaudího	Gaudí	k1gMnSc2	Gaudí
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
katedrály	katedrála	k1gFnSc2	katedrála
podíleli	podílet	k5eAaImAgMnP	podílet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
dodržovat	dodržovat	k5eAaImF	dodržovat
původní	původní	k2eAgInSc4d1	původní
Gaudího	Gaudí	k2eAgInSc2d1	Gaudí
plány	plán	k1gInPc4	plán
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
stavby	stavba	k1gFnSc2	stavba
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
katedrála	katedrála	k1gFnSc1	katedrála
staví	stavit	k5eAaImIp3nS	stavit
z	z	k7c2	z
darů	dar	k1gInPc2	dar
a	a	k8xC	a
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
.	.	kIx.	.
</s>
