<s>
Řecko	Řecko	k1gNnSc1	Řecko
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Ε	Ε	k?	Ε
[	[	kIx(	[
<g/>
eˈ	eˈ	k?	eˈ
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
Ε	Ε	k?	Ε
[	[	kIx(	[
<g/>
eˈ	eˈ	k?	eˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Řecká	řecký	k2eAgFnSc1d1	řecká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Ε	Ε	k?	Ε
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc1	stát
ležící	ležící	k2eAgInSc1d1	ležící
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
–	–	k?	–
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Balkánského	balkánský	k2eAgInSc2d1	balkánský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
četných	četný	k2eAgInPc6d1	četný
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
<g/>
,	,	kIx,	,
Krétském	krétský	k2eAgNnSc6d1	krétské
<g/>
,	,	kIx,	,
Thráckém	thrácký	k2eAgNnSc6d1	thrácké
<g/>
,	,	kIx,	,
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
a	a	k8xC	a
Jónském	jónský	k2eAgNnSc6d1	Jónské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
jsou	být	k5eAaImIp3nP	být
Albánie	Albánie	k1gFnPc1	Albánie
<g/>
,	,	kIx,	,
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
jsou	být	k5eAaImIp3nP	být
Athény	Athéna	k1gFnPc4	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
řeckého	řecký	k2eAgInSc2d1	řecký
národa	národ	k1gInSc2	národ
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
země	zem	k1gFnSc2	zem
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
jazyků	jazyk	k1gMnPc2	jazyk
včetně	včetně	k7c2	včetně
češtiny	čeština	k1gFnSc2	čeština
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
tří	tři	k4xCgInPc2	tři
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
a	a	k8xC	a
nejčastější	častý	k2eAgFnSc1d3	nejčastější
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
české	český	k2eAgNnSc1d1	české
Řecko	Řecko	k1gNnSc1	Řecko
a	a	k8xC	a
např.	např.	kA	např.
také	také	k9	také
anglické	anglický	k2eAgFnSc2d1	anglická
Greece	Greece	k1gFnSc2	Greece
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Grè	Grè	k1gFnSc2	Grè
i	i	k8xC	i
německé	německý	k2eAgInPc1d1	německý
Griechenland	Griechenland	k1gInSc1	Griechenland
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
výrazu	výraz	k1gInSc2	výraz
Graeci	Graece	k1gMnSc3	Graece
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
starověkým	starověký	k2eAgInSc7d1	starověký
názvem	název	k1gInSc7	název
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
např.	např.	kA	např.
v	v	k7c6	v
tádžičtině	tádžičtina	k1gFnSc6	tádžičtina
a	a	k8xC	a
uzbečtině	uzbečtina	k1gFnSc6	uzbečtina
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
názvu	název	k1gInSc2	název
území	území	k1gNnSc2	území
Iónie	Iónie	k1gFnSc2	Iónie
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Iónía	Ióní	k2eAgFnSc1d1	Ióní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
podobou	podoba	k1gFnSc7	podoba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
sami	sám	k3xTgMnPc1	sám
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Héllas	Héllas	k1gMnSc1	Héllas
podle	podle	k7c2	podle
mýtické	mýtický	k2eAgFnSc2d1	mýtická
Heleny	Helena	k1gFnSc2	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Gruzínsky	gruzínsky	k6eAd1	gruzínsky
se	se	k3xPyFc4	se
Řekům	Řek	k1gMnPc3	Řek
říká	říkat	k5eAaImIp3nS	říkat
Berdzeni	Berdzen	k2eAgMnPc1d1	Berdzen
podle	podle	k7c2	podle
gruzínského	gruzínský	k2eAgNnSc2d1	gruzínské
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
brdzeni	brdzen	k2eAgMnPc1d1	brdzen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
moudrý	moudrý	k2eAgMnSc1d1	moudrý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgFnPc1d1	řecká
dějiny	dějiny	k1gFnPc1	dějiny
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
evropských	evropský	k2eAgFnPc2d1	Evropská
a	a	k8xC	a
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Začínají	začínat	k5eAaImIp3nP	začínat
ještě	ještě	k9	ještě
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
starověkých	starověký	k2eAgMnPc2d1	starověký
Řeků	Řek	k1gMnPc2	Řek
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stalo	stát	k5eAaPmAgNnS	stát
sídelním	sídelní	k2eAgNnSc7d1	sídelní
územím	území	k1gNnSc7	území
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
které	který	k3yRgInPc4	který
zabíralo	zabírat	k5eAaImAgNnS	zabírat
také	také	k9	také
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
řeckým	řecký	k2eAgNnSc7d1	řecké
osídlením	osídlení	k1gNnSc7	osídlení
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
Pelasgové	Pelasgový	k2eAgNnSc4d1	Pelasgový
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgNnSc4d1	kontinentální
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Minóané	Minóaný	k2eAgNnSc1d1	Minóaný
(	(	kIx(	(
<g/>
Kréta	Kréta	k1gFnSc1	Kréta
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
před	před	k7c7	před
Kristem	Kristus	k1gMnSc7	Kristus
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
pronikly	proniknout	k5eAaPmAgInP	proniknout
první	první	k4xOgInPc1	první
řecké	řecký	k2eAgInPc1d1	řecký
kmeny	kmen	k1gInPc1	kmen
–	–	k?	–
Achajci	Achajce	k1gMnPc7	Achajce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
mykénskou	mykénský	k2eAgFnSc4d1	mykénská
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
usadily	usadit	k5eAaPmAgInP	usadit
další	další	k2eAgInPc1d1	další
řecké	řecký	k2eAgInPc1d1	řecký
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
Dórové	Dór	k1gMnPc1	Dór
<g/>
,	,	kIx,	,
Ionové	Ionus	k1gMnPc1	Ionus
<g/>
,	,	kIx,	,
Aiolové	Aiol	k1gMnPc1	Aiol
<g/>
,	,	kIx,	,
Epiróti	Epirót	k1gMnPc1	Epirót
a	a	k8xC	a
Makedonci	Makedonec	k1gMnPc1	Makedonec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
promíchaly	promíchat	k5eAaPmAgFnP	promíchat
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
starověký	starověký	k2eAgInSc1d1	starověký
řecký	řecký	k2eAgInSc1d1	řecký
národ	národ	k1gInSc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
starořeckém	starořecký	k2eAgNnSc6d1	starořecké
(	(	kIx(	(
<g/>
antickém	antický	k2eAgNnSc6d1	antické
<g/>
)	)	kIx)	)
období	období	k1gNnSc6	období
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Řekové	Řek	k1gMnPc1	Řek
velkou	velký	k2eAgFnSc4d1	velká
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovaly	následovat	k5eAaImAgFnP	následovat
postupně	postupně	k6eAd1	postupně
římská	římský	k2eAgFnSc1d1	římská
vláda	vláda	k1gFnSc1	vláda
nad	nad	k7c7	nad
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
řeckým	řecký	k2eAgNnSc7d1	řecké
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
byzantské	byzantský	k2eAgNnSc1d1	byzantské
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
nadvláda	nadvláda	k1gFnSc1	nadvláda
<g/>
,	,	kIx,	,
období	období	k1gNnSc1	období
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
moderního	moderní	k2eAgNnSc2d1	moderní
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Starověké	starověký	k2eAgNnSc1d1	starověké
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
Řecka	Řecko	k1gNnSc2	Řecko
u	u	k7c2	u
Egejského	egejský	k2eAgNnSc2d1	Egejské
moře	moře	k1gNnSc2	moře
bylo	být	k5eAaImAgNnS	být
místem	místo	k1gNnSc7	místo
vzestupu	vzestup	k1gInSc2	vzestup
první	první	k4xOgFnSc2	první
civilizace	civilizace	k1gFnSc2	civilizace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Minóané	Minóan	k1gMnPc1	Minóan
a	a	k8xC	a
Mykéňané	Mykéňan	k1gMnPc1	Mykéňan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následovala	následovat	k5eAaImAgFnS	následovat
Temná	temný	k2eAgFnSc1d1	temná
doba	doba	k1gFnSc1	doba
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
800	[number]	k4	800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začala	začít	k5eAaPmAgFnS	začít
nová	nový	k2eAgFnSc1d1	nová
éra	éra	k1gFnSc1	éra
řeckých	řecký	k2eAgInPc2d1	řecký
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Sparta	Sparta	k1gFnSc1	Sparta
nebo	nebo	k8xC	nebo
Athény	Athéna	k1gFnPc1	Athéna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
získávaly	získávat	k5eAaImAgFnP	získávat
kolonie	kolonie	k1gFnPc4	kolonie
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Středozemí	středozemí	k1gNnSc6	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
éře	éra	k1gFnSc6	éra
impéria	impérium	k1gNnPc4	impérium
Alexandra	Alexandr	k1gMnSc2	Alexandr
Velikého	veliký	k2eAgMnSc2d1	veliký
přišlo	přijít	k5eAaPmAgNnS	přijít
období	období	k1gNnSc3	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
helénské	helénský	k2eAgFnSc2d1	helénská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Achaia	Achaium	k1gNnSc2	Achaium
(	(	kIx(	(
<g/>
římská	římský	k2eAgFnSc1d1	římská
provincie	provincie	k1gFnSc1	provincie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
Řecko	Řecko	k1gNnSc4	Řecko
vojensky	vojensky	k6eAd1	vojensky
sláblo	slábnout	k5eAaImAgNnS	slábnout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
po	po	k7c6	po
čase	čas	k1gInSc6	čas
podřídit	podřídit	k5eAaPmF	podřídit
Římu	Řím	k1gInSc3	Řím
–	–	k?	–
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
168	[number]	k4	168
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
si	se	k3xPyFc3	se
řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
spíše	spíše	k9	spíše
podmanila	podmanit	k5eAaPmAgFnS	podmanit
život	život	k1gInSc4	život
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
provincií	provincie	k1gFnSc7	provincie
Římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
stále	stále	k6eAd1	stále
dominovala	dominovat	k5eAaImAgFnS	dominovat
východnímu	východní	k2eAgMnSc3d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
říše	říše	k1gFnSc1	říše
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
Východořímskou	východořímský	k2eAgFnSc4d1	Východořímská
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
později	pozdě	k6eAd2	pozdě
jako	jako	k8xS	jako
Byzanc	Byzanc	k1gFnSc1	Byzanc
<g/>
,	,	kIx,	,
a	a	k8xC	a
Západořímskou	západořímský	k2eAgFnSc4d1	Západořímská
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
okolo	okolo	k7c2	okolo
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
řecká	řecký	k2eAgNnPc1d1	řecké
<g/>
,	,	kIx,	,
zahrnujíc	zahrnovat	k5eAaImSgFnS	zahrnovat
samotné	samotný	k2eAgNnSc1d1	samotné
Řecko	Řecko	k1gNnSc4	Řecko
a	a	k8xC	a
také	také	k9	také
řeckou	řecký	k2eAgFnSc4d1	řecká
Malou	malý	k2eAgFnSc4d1	malá
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
přežila	přežít	k5eAaPmAgFnS	přežít
jedenáct	jedenáct	k4xCc4	jedenáct
století	století	k1gNnPc2	století
útoků	útok	k1gInPc2	útok
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
západu	západ	k1gInSc2	západ
a	a	k8xC	a
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
až	až	k9	až
byla	být	k5eAaImAgFnS	být
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1453	[number]	k4	1453
dobyta	dobyt	k2eAgFnSc1d1	dobyta
a	a	k8xC	a
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
padl	padnout	k5eAaPmAgMnS	padnout
poslední	poslední	k2eAgMnSc1d1	poslední
císař	císař	k1gMnSc1	císař
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Palaiologů	Palaiolog	k1gMnPc2	Palaiolog
<g/>
,	,	kIx,	,
Konstantin	Konstantin	k1gMnSc1	Konstantin
XI	XI	kA	XI
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
Řecka	Řecko	k1gNnSc2	Řecko
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postupně	postupně	k6eAd1	postupně
podmaňován	podmaňovat	k5eAaImNgMnS	podmaňovat
Osmany	Osman	k1gMnPc7	Osman
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Osmané	Osman	k1gMnPc1	Osman
dokončovali	dokončovat	k5eAaImAgMnP	dokončovat
dobytí	dobytí	k1gNnSc4	dobytí
pevninské	pevninský	k2eAgFnSc2d1	pevninská
části	část	k1gFnSc2	část
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
odehrávaly	odehrávat	k5eAaImAgInP	odehrávat
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
migrace	migrace	k1gFnPc1	migrace
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
stěhování	stěhování	k1gNnSc1	stěhování
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
odchod	odchod	k1gInSc4	odchod
řecké	řecký	k2eAgFnSc2d1	řecká
inteligence	inteligence	k1gFnSc2	inteligence
do	do	k7c2	do
Západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
před	před	k7c4	před
Turky	Turek	k1gMnPc4	Turek
<g/>
,	,	kIx,	,
a	a	k8xC	a
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
nástupu	nástup	k1gInSc3	nástup
renesanční	renesanční	k2eAgFnSc2d1	renesanční
epochy	epocha	k1gFnSc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
stěhování	stěhování	k1gNnSc6	stěhování
Řekové	Řek	k1gMnPc1	Řek
opouštěli	opouštět	k5eAaImAgMnP	opouštět
pláně	pláně	k1gNnSc4	pláně
Peloponéského	peloponéský	k2eAgInSc2d1	peloponéský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
usazovali	usazovat	k5eAaImAgMnP	usazovat
se	se	k3xPyFc4	se
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Osmané	Osman	k1gMnPc1	Osman
nebyli	být	k5eNaImAgMnP	být
schopní	schopný	k2eAgMnPc1d1	schopný
vytvořit	vytvořit	k5eAaPmF	vytvořit
stálou	stálý	k2eAgFnSc4d1	stálá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
a	a	k8xC	a
administrativní	administrativní	k2eAgFnSc4d1	administrativní
přítomnost	přítomnost	k1gFnSc4	přítomnost
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
řecké	řecký	k2eAgInPc1d1	řecký
horské	horský	k2eAgInPc1d1	horský
klany	klan	k1gInPc1	klan
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
udržely	udržet	k5eAaPmAgFnP	udržet
statut	statut	k1gInSc4	statut
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Řekové	Řek	k1gMnPc1	Řek
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
planiny	planina	k1gFnPc4	planina
a	a	k8xC	a
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
milletu	millést	k5eAaPmIp1nS	millést
(	(	kIx(	(
<g/>
turecký	turecký	k2eAgInSc4d1	turecký
termín	termín	k1gInSc4	termín
označující	označující	k2eAgInSc4d1	označující
právní	právní	k2eAgFnSc4d1	právní
ochranu	ochrana	k1gFnSc4	ochrana
náboženské	náboženský	k2eAgFnSc2d1	náboženská
menšiny	menšina	k1gFnSc2	menšina
<g/>
)	)	kIx)	)
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
etnické	etnický	k2eAgFnSc3d1	etnická
soudržnosti	soudržnost	k1gFnSc3	soudržnost
mezi	mezi	k7c7	mezi
ortodoxními	ortodoxní	k2eAgMnPc7d1	ortodoxní
Řeky	řeka	k1gFnSc2	řeka
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
izoloval	izolovat	k5eAaBmAgMnS	izolovat
různé	různý	k2eAgMnPc4d1	různý
lidi	člověk	k1gMnPc4	člověk
uvnitř	uvnitř	k7c2	uvnitř
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
vyznáním	vyznání	k1gNnSc7	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Ortodoxní	ortodoxní	k2eAgFnSc1d1	ortodoxní
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
náboženská	náboženský	k2eAgFnSc1d1	náboženská
instituce	instituce	k1gFnSc1	instituce
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
národním	národní	k2eAgInSc7d1	národní
charakterem	charakter	k1gInSc7	charakter
<g/>
,	,	kIx,	,
pomohla	pomoct	k5eAaPmAgFnS	pomoct
Řekům	Řek	k1gMnPc3	Řek
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
území	území	k1gNnPc2	území
poloostrova	poloostrov	k1gInSc2	poloostrov
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
planin	planina	k1gFnPc2	planina
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
zachovat	zachovat	k5eAaPmF	zachovat
jejich	jejich	k3xOp3gNnSc4	jejich
etnické	etnický	k2eAgNnSc4d1	etnické
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgNnSc4d1	kulturní
a	a	k8xC	a
jazykové	jazykový	k2eAgNnSc4d1	jazykové
dědictví	dědictví	k1gNnSc4	dědictví
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
církvi	církev	k1gFnSc3	církev
ještě	ještě	k6eAd1	ještě
neříkalo	říkat	k5eNaImAgNnS	říkat
řecká	řecký	k2eAgFnSc1d1	řecká
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
až	až	k9	až
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zůstali	zůstat	k5eAaPmAgMnP	zůstat
na	na	k7c6	na
planinách	planina	k1gFnPc6	planina
během	během	k7c2	během
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
buď	buď	k8xC	buď
křesťané	křesťan	k1gMnPc1	křesťan
vypořádávající	vypořádávající	k2eAgMnSc1d1	vypořádávající
se	se	k3xPyFc4	se
s	s	k7c7	s
břemenem	břemeno	k1gNnSc7	břemeno
cizí	cizí	k2eAgFnSc2d1	cizí
nadvlády	nadvláda	k1gFnSc2	nadvláda
nebo	nebo	k8xC	nebo
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
krypto-křesťané	kryptořesťan	k1gMnPc1	krypto-křesťan
(	(	kIx(	(
<g/>
řečtí	řecký	k2eAgMnPc1d1	řecký
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
tajnými	tajný	k2eAgMnPc7d1	tajný
vyznavači	vyznavač	k1gMnPc7	vyznavač
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
víry	víra	k1gFnSc2	víra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
vysokému	vysoký	k2eAgNnSc3d1	vysoké
zdanění	zdanění	k1gNnSc3	zdanění
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
konvertovali	konvertovat	k5eAaBmAgMnP	konvertovat
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
a	a	k8xC	a
nebyli	být	k5eNaImAgMnP	být
krypto-křesťany	kryptořesťan	k1gMnPc7	krypto-křesťan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
ortodoxních	ortodoxní	k2eAgMnPc2d1	ortodoxní
Řeků	Řek	k1gMnPc2	Řek
Turky	Turek	k1gMnPc4	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Nebyli	být	k5eNaImAgMnP	být
zde	zde	k6eAd1	zde
žádní	žádný	k3yNgMnPc1	žádný
řečtí	řecký	k2eAgMnPc1d1	řecký
muslimové	muslim	k1gMnPc1	muslim
ani	ani	k8xC	ani
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyznání	vyznání	k1gNnSc1	vyznání
hrálo	hrát	k5eAaImAgNnS	hrát
neodlučitelnou	odlučitelný	k2eNgFnSc4d1	neodlučitelná
roli	role	k1gFnSc4	role
ve	v	k7c4	v
utváření	utváření	k1gNnSc4	utváření
moderních	moderní	k2eAgFnPc2d1	moderní
řeckých	řecký	k2eAgFnPc2d1	řecká
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
post-osmanských	postsmanský	k2eAgFnPc2d1	post-osmanský
národních	národní	k2eAgFnPc2d1	národní
identit	identita	k1gFnPc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Řecké	řecký	k2eAgNnSc1d1	řecké
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Řecké	řecký	k2eAgNnSc1d1	řecké
království	království	k1gNnSc1	království
získalo	získat	k5eAaPmAgNnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
velmoci	velmoc	k1gFnSc6	velmoc
uznaly	uznat	k5eAaPmAgInP	uznat
nezávislost	nezávislost	k1gFnSc4	nezávislost
Řecka	Řecko	k1gNnSc2	Řecko
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
(	(	kIx(	(
<g/>
Turecko	Turecko	k1gNnSc1	Turecko
uznalo	uznat	k5eAaPmAgNnS	uznat
nezávislost	nezávislost	k1gFnSc4	nezávislost
až	až	k6eAd1	až
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Řecko	Řecko	k1gNnSc1	Řecko
postupně	postupně	k6eAd1	postupně
připojovalo	připojovat	k5eAaImAgNnS	připojovat
sousední	sousední	k2eAgInPc4d1	sousední
ostrovy	ostrov	k1gInPc4	ostrov
s	s	k7c7	s
řecky	řecky	k6eAd1	řecky
mluvícím	mluvící	k2eAgNnSc7d1	mluvící
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Řecko	Řecko	k1gNnSc1	Řecko
hodně	hodně	k6eAd1	hodně
získalo	získat	k5eAaPmAgNnS	získat
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
sever	sever	k1gInSc4	sever
–	–	k?	–
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Makedonské	makedonský	k2eAgFnSc2d1	makedonská
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
ale	ale	k9	ale
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
kulturní	kulturní	k2eAgInSc4d1	kulturní
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
Anatolii	Anatolie	k1gFnSc6	Anatolie
<g/>
,	,	kIx,	,
anatolští	anatolský	k2eAgMnPc1d1	anatolský
Řekové	Řek	k1gMnPc1	Řek
byli	být	k5eAaImAgMnP	být
přesídleni	přesídlet	k5eAaPmNgMnP	přesídlet
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Turci	Turek	k1gMnPc1	Turek
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
byli	být	k5eAaImAgMnP	být
vystěhováni	vystěhován	k2eAgMnPc1d1	vystěhován
do	do	k7c2	do
později	pozdě	k6eAd2	pozdě
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
Turecké	turecký	k2eAgFnSc2d1	turecká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
44	[number]	k4	44
bylo	být	k5eAaImAgNnS	být
Řecko	Řecko	k1gNnSc1	Řecko
obsazeno	obsazen	k2eAgNnSc1d1	obsazeno
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
k	k	k7c3	k
osvobození	osvobození	k1gNnSc3	osvobození
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
komunisty	komunista	k1gMnSc2	komunista
organizovaného	organizovaný	k2eAgNnSc2d1	organizované
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
Řecko	Řecko	k1gNnSc1	Řecko
orientovalo	orientovat	k5eAaBmAgNnS	orientovat
na	na	k7c4	na
západní	západní	k2eAgFnPc4d1	západní
demokracie	demokracie	k1gFnPc4	demokracie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
NATO	nato	k6eAd1	nato
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
společně	společně	k6eAd1	společně
s	s	k7c7	s
dosud	dosud	k6eAd1	dosud
nepřátelským	přátelský	k2eNgNnSc7d1	nepřátelské
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
jaderný	jaderný	k2eAgInSc4d1	jaderný
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
bylo	být	k5eAaImAgNnS	být
Řecko	Řecko	k1gNnSc1	Řecko
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
diktaturou	diktatura	k1gFnSc7	diktatura
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Řečtí	řecký	k2eAgMnPc1d1	řecký
plukovníci	plukovník	k1gMnPc1	plukovník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
politických	politický	k2eAgFnPc2d1	politická
svobod	svoboda	k1gFnPc2	svoboda
omezeno	omezit	k5eAaPmNgNnS	omezit
<g/>
,	,	kIx,	,
a	a	k8xC	a
řecký	řecký	k2eAgMnSc1d1	řecký
král	král	k1gMnSc1	král
byl	být	k5eAaImAgMnS	být
donucen	donutit	k5eAaPmNgMnS	donutit
opustit	opustit	k5eAaPmF	opustit
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Demokratické	demokratický	k2eAgFnPc1d1	demokratická
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
referendem	referendum	k1gNnSc7	referendum
ukončily	ukončit	k5eAaPmAgInP	ukončit
období	období	k1gNnSc6	období
vojenské	vojenský	k2eAgFnSc2d1	vojenská
diktatury	diktatura	k1gFnSc2	diktatura
a	a	k8xC	a
zrušily	zrušit	k5eAaPmAgFnP	zrušit
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
referenda	referendum	k1gNnSc2	referendum
stalo	stát	k5eAaPmAgNnS	stát
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
Řecko	Řecko	k1gNnSc1	Řecko
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
evropských	evropský	k2eAgNnPc2d1	Evropské
společenství	společenství	k1gNnPc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Problematická	problematický	k2eAgFnSc1d1	problematická
historie	historie	k1gFnSc1	historie
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
i	i	k9	i
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
sloganu	slogan	k1gInSc6	slogan
–	–	k?	–
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
pevninské	pevninský	k2eAgFnSc2d1	pevninská
části	část	k1gFnSc2	část
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
konci	konec	k1gInSc6	konec
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
,	,	kIx,	,
Peloponéského	peloponéský	k2eAgInSc2d1	peloponéský
poloostrova	poloostrov	k1gInSc2	poloostrov
(	(	kIx(	(
<g/>
spojeného	spojený	k2eAgNnSc2d1	spojené
s	s	k7c7	s
pevninou	pevnina	k1gFnSc7	pevnina
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Korintské	korintský	k2eAgFnSc2d1	Korintská
šíje	šíj	k1gFnSc2	šíj
<g/>
)	)	kIx)	)
a	a	k8xC	a
početných	početný	k2eAgInPc2d1	početný
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
3000	[number]	k4	3000
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
Kréty	Kréta	k1gFnSc2	Kréta
<g/>
,	,	kIx,	,
Rhodu	Rhodos	k1gInSc2	Rhodos
<g/>
,	,	kIx,	,
Euboie	Euboie	k1gFnSc2	Euboie
nebo	nebo	k8xC	nebo
Dodekanésu	Dodekanés	k1gInSc2	Dodekanés
či	či	k8xC	či
Kykladů	Kyklad	k1gInPc2	Kyklad
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Jónském	jónský	k2eAgNnSc6d1	Jónské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kontinentální	kontinentální	k2eAgFnPc1d1	kontinentální
státní	státní	k2eAgFnPc1d1	státní
hranice	hranice	k1gFnPc1	hranice
Řecka	Řecko	k1gNnSc2	Řecko
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
1180,71	[number]	k4	1180,71
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Albánií	Albánie	k1gFnSc7	Albánie
(	(	kIx(	(
<g/>
246,70	[number]	k4	246,70
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Makedonií	Makedonie	k1gFnSc7	Makedonie
(	(	kIx(	(
<g/>
256,31	[number]	k4	256,31
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
(	(	kIx(	(
<g/>
474,70	[number]	k4	474,70
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
(	(	kIx(	(
<g/>
203,00	[number]	k4	203,00
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
leží	ležet	k5eAaImIp3nS	ležet
Egejské	egejský	k2eAgNnSc4d1	Egejské
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Řecko	Řecko	k1gNnSc1	Řecko
omývá	omývat	k5eAaImIp3nS	omývat
Jónské	jónský	k2eAgNnSc4d1	Jónské
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Libyjské	libyjský	k2eAgNnSc4d1	Libyjské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
021	[number]	k4	021
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
80	[number]	k4	80
%	%	kIx~	%
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
horami	hora	k1gFnPc7	hora
nebo	nebo	k8xC	nebo
kopci	kopec	k1gInPc7	kopec
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Řecko	Řecko	k1gNnSc1	Řecko
činí	činit	k5eAaImIp3nS	činit
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejhornatějších	hornatý	k2eAgFnPc2d3	nejhornatější
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Horská	horský	k2eAgNnPc1d1	horské
pásma	pásmo	k1gNnPc1	pásmo
na	na	k7c6	na
území	území	k1gNnSc6	území
Řecka	Řecko	k1gNnSc2	Řecko
se	se	k3xPyFc4	se
souhrnně	souhrnně	k6eAd1	souhrnně
označují	označovat	k5eAaImIp3nP	označovat
Helenidy	Helenida	k1gFnPc1	Helenida
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
Řecko	Řecko	k1gNnSc1	Řecko
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
mokřady	mokřad	k1gInPc7	mokřad
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgNnSc1d1	centrální
horstvo	horstvo	k1gNnSc1	horstvo
Pindos	Pindos	k1gInSc1	Pindos
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
2636	[number]	k4	2636
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Horské	Horské	k2eAgNnSc1d1	Horské
pásmo	pásmo	k1gNnSc1	pásmo
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Peloponésu	Peloponés	k1gInSc2	Peloponés
<g/>
,	,	kIx,	,
ostrovů	ostrov	k1gInPc2	ostrov
Kythera	Kythero	k1gNnSc2	Kythero
a	a	k8xC	a
Antikythera	Antikythero	k1gNnSc2	Antikythero
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kréta	Kréta	k1gFnSc1	Kréta
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vrcholy	vrchol	k1gInPc4	vrchol
podvodních	podvodní	k2eAgFnPc2d1	podvodní
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
kdysi	kdysi	k6eAd1	kdysi
byly	být	k5eAaImAgInP	být
prodloužením	prodloužení	k1gNnSc7	prodloužení
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Řecka	Řecko	k1gNnSc2	Řecko
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vysoké	vysoký	k2eAgInPc4d1	vysoký
strmé	strmý	k2eAgInPc4d1	strmý
vrcholy	vrchol	k1gInPc4	vrchol
přerušované	přerušovaný	k2eAgInPc4d1	přerušovaný
mnoha	mnoho	k4c7	mnoho
kaňony	kaňon	k1gInPc7	kaňon
a	a	k8xC	a
ostatními	ostatní	k2eAgInPc7d1	ostatní
krasovými	krasový	k2eAgInPc7d1	krasový
útvary	útvar	k1gInPc7	útvar
včetně	včetně	k7c2	včetně
roklin	roklina	k1gFnPc2	roklina
Meteora	Meteora	k1gFnSc1	Meteora
a	a	k8xC	a
Vikos	Vikos	k1gInSc1	Vikos
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohořet	k5eAaPmIp3nP	pohořet
Olymp	Olymp	k1gInSc4	Olymp
s	s	k7c7	s
horou	hora	k1gFnSc7	hora
Mytikas	Mytikasa	k1gFnPc2	Mytikasa
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
2919	[number]	k4	2919
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
severní	severní	k2eAgNnSc1d1	severní
Řecko	Řecko	k1gNnSc1	Řecko
tvořeno	tvořen	k2eAgNnSc1d1	tvořeno
dalším	další	k2eAgNnSc7d1	další
pohořím	pohoří	k1gNnSc7	pohoří
–	–	k?	–
Rodopy	Rodopa	k1gFnSc2	Rodopa
<g/>
,	,	kIx,	,
nacházejícími	nacházející	k2eAgFnPc7d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Makedonii	Makedonie	k1gFnSc6	Makedonie
a	a	k8xC	a
Thrákii	Thrákie	k1gFnSc6	Thrákie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
a	a	k8xC	a
hustými	hustý	k2eAgInPc7d1	hustý
lesy	les	k1gInPc7	les
jako	jako	k8xS	jako
například	například	k6eAd1	například
známá	známý	k2eAgFnSc1d1	známá
Dadia	Dadia	k1gFnSc1	Dadia
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
je	být	k5eAaImIp3nS	být
členskou	členský	k2eAgFnSc7d1	členská
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
a	a	k8xC	a
NATO	NATO	kA	NATO
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Planiny	planina	k1gFnPc1	planina
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Thesálii	Thesálie	k1gFnSc6	Thesálie
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc6d1	střední
Makedonii	Makedonie	k1gFnSc6	Makedonie
a	a	k8xC	a
Thrákii	Thrákie	k1gFnSc6	Thrákie
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1	řecké
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
rozdělené	rozdělený	k2eAgNnSc1d1	rozdělené
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
dobře	dobře	k6eAd1	dobře
definovatelné	definovatelný	k2eAgFnSc2d1	definovatelná
třídy	třída	k1gFnSc2	třída
–	–	k?	–
přímořské	přímořský	k2eAgNnSc1d1	přímořské
<g/>
,	,	kIx,	,
horské	horský	k2eAgNnSc1d1	horské
a	a	k8xC	a
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mírné	mírný	k2eAgFnSc2d1	mírná
vlhké	vlhký	k2eAgFnSc2d1	vlhká
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
horká	horký	k2eAgNnPc4d1	horké
suchá	suchý	k2eAgNnPc4d1	suché
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
teploty	teplota	k1gFnPc1	teplota
vzácně	vzácně	k6eAd1	vzácně
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
extrémů	extrém	k1gInPc2	extrém
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
příležitostně	příležitostně	k6eAd1	příležitostně
může	moct	k5eAaImIp3nS	moct
sněžit	sněžit	k5eAaImF	sněžit
i	i	k9	i
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
<g/>
,	,	kIx,	,
na	na	k7c6	na
Kykladech	Kyklad	k1gInPc6	Kyklad
nebo	nebo	k8xC	nebo
Krétě	Kréta	k1gFnSc6	Kréta
<g/>
.	.	kIx.	.
</s>
<s>
Horské	Horské	k2eAgMnSc1d1	Horské
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Řecku	Řecko	k1gNnSc6	Řecko
(	(	kIx(	(
<g/>
Epirus	Epirus	k1gInSc1	Epirus
<g/>
,	,	kIx,	,
Střední	střední	k2eAgNnSc1d1	střední
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Thesálie	Thesálie	k1gFnSc1	Thesálie
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnSc1d1	západní
Makedonie	Makedonie	k1gFnSc1	Makedonie
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
částech	část	k1gFnPc6	část
Peloponésu	Peloponés	k1gInSc2	Peloponés
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Achaia	Achaia	k1gFnSc1	Achaia
<g/>
,	,	kIx,	,
Arkádie	Arkádie	k1gFnPc1	Arkádie
a	a	k8xC	a
části	část	k1gFnPc1	část
Lakónie	Lakónie	k1gFnSc2	Lakónie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
Alpami	Alpy	k1gFnPc7	Alpy
přímo	přímo	k6eAd1	přímo
sousedí	sousedit	k5eAaImIp3nP	sousedit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
mírné	mírný	k2eAgNnSc1d1	mírné
pásmo	pásmo	k1gNnSc1	pásmo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Makedonii	Makedonie	k1gFnSc6	Makedonie
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Thrákii	Thrákie	k1gFnSc6	Thrákie
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Komotini	Komotin	k2eAgMnPc1d1	Komotin
<g/>
,	,	kIx,	,
Xanthi	Xanth	k1gMnPc1	Xanth
a	a	k8xC	a
severní	severní	k2eAgMnSc1d1	severní
Evros	Evros	k1gMnSc1	Evros
<g/>
;	;	kIx,	;
s	s	k7c7	s
chladnými	chladný	k2eAgFnPc7d1	chladná
a	a	k8xC	a
sychravými	sychravý	k2eAgFnPc7d1	sychravá
zimami	zima	k1gFnPc7	zima
a	a	k8xC	a
horkými	horký	k2eAgNnPc7d1	horké
suchými	suchý	k2eAgNnPc7d1	suché
léty	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Atény	Atény	k1gFnPc1	Atény
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
na	na	k7c6	na
přechodovém	přechodový	k2eAgNnSc6d1	přechodové
území	území	k1gNnSc6	území
mezi	mezi	k7c7	mezi
přímořským	přímořský	k2eAgNnSc7d1	přímořské
a	a	k8xC	a
horským	horský	k2eAgNnSc7d1	horské
klimatem	klima	k1gNnSc7	klima
–	–	k?	–
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
čtvrtích	čtvrt	k1gFnPc6	čtvrt
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
klima	klima	k1gNnSc1	klima
přímořské	přímořský	k2eAgNnSc1d1	přímořské
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
čtvrtích	čtvrt	k1gFnPc6	čtvrt
panuje	panovat	k5eAaImIp3nS	panovat
to	ten	k3xDgNnSc1	ten
horské	horský	k2eAgNnSc1d1	horské
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
50	[number]	k4	50
%	%	kIx~	%
řeckého	řecký	k2eAgNnSc2d1	řecké
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
lesy	les	k1gInPc1	les
s	s	k7c7	s
bohatě	bohatě	k6eAd1	bohatě
rozličnou	rozličný	k2eAgFnSc7d1	rozličná
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
od	od	k7c2	od
horských	horský	k2eAgInPc2d1	horský
jehličnanů	jehličnan	k1gInPc2	jehličnan
až	až	k9	až
k	k	k7c3	k
přímořskému	přímořský	k2eAgInSc3d1	přímořský
typu	typ	k1gInSc3	typ
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgInPc1d1	řecký
lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
domovem	domov	k1gInSc7	domov
posledním	poslední	k2eAgMnPc3d1	poslední
hnědým	hnědý	k2eAgMnPc3d1	hnědý
medvědům	medvěd	k1gMnPc3	medvěd
a	a	k8xC	a
rysům	rys	k1gMnPc3	rys
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
dalším	další	k2eAgInPc3d1	další
druhům	druh	k1gInPc3	druh
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
například	například	k6eAd1	například
vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
srnci	srnec	k1gMnPc1	srnec
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgFnPc1d1	divoká
kozy	koza	k1gFnPc1	koza
<g/>
,	,	kIx,	,
lišky	liška	k1gFnPc1	liška
a	a	k8xC	a
divoká	divoký	k2eAgNnPc1d1	divoké
prasata	prase	k1gNnPc1	prase
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moři	moře	k1gNnSc6	moře
kolem	kolem	k7c2	kolem
Řecka	Řecko	k1gNnSc2	Řecko
žijí	žít	k5eAaImIp3nP	žít
tuleni	tuleň	k1gMnPc1	tuleň
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnPc1d1	mořská
želvy	želva	k1gFnPc1	želva
<g/>
,	,	kIx,	,
žraloci	žralok	k1gMnPc1	žralok
<g/>
,	,	kIx,	,
delfíni	delfín	k1gMnPc1	delfín
i	i	k8xC	i
krakatice	krakatice	k1gFnPc1	krakatice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
parlamentem	parlament	k1gInSc7	parlament
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Demokracie	demokracie	k1gFnSc1	demokracie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
právě	právě	k9	právě
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
ji	on	k3xPp3gFnSc4	on
Kleisthenés	Kleisthenés	k1gInSc4	Kleisthenés
<g/>
,	,	kIx,	,
v	v	k7c6	v
novověkém	novověký	k2eAgNnSc6d1	novověké
Řecku	Řecko	k1gNnSc6	Řecko
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
po	po	k7c4	po
osvobození	osvobození	k1gNnSc4	osvobození
zpod	zpod	k7c2	zpod
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
ale	ale	k8xC	ale
nově	nově	k6eAd1	nově
nastolena	nastolen	k2eAgFnSc1d1	nastolena
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
patří	patřit	k5eAaImIp3nS	patřit
Řecko	Řecko	k1gNnSc4	Řecko
mezi	mezi	k7c4	mezi
jedny	jeden	k4xCgFnPc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
evropských	evropský	k2eAgInPc2d1	evropský
novodobých	novodobý	k2eAgInPc2d1	novodobý
demokratických	demokratický	k2eAgInPc2d1	demokratický
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Demokracie	demokracie	k1gFnSc1	demokracie
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgInSc2d1	francouzský
vzoru	vzor	k1gInSc2	vzor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Turků	turek	k1gInPc2	turek
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
prosadit	prosadit	k5eAaPmF	prosadit
ji	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k6eAd1	až
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
státním	státní	k2eAgInPc3d1	státní
převratům	převrat	k1gInPc3	převrat
i	i	k8xC	i
politickým	politický	k2eAgFnPc3d1	politická
vraždám	vražda	k1gFnPc3	vražda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgMnS	být
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
politik	politik	k1gMnSc1	politik
původem	původ	k1gInSc7	původ
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Korfu	Korfu	k1gNnSc1	Korfu
<g/>
,	,	kIx,	,
Joannis	Joannis	k1gFnSc1	Joannis
Kapodistrias	Kapodistrias	k1gInSc1	Kapodistrias
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zavražděn	zavražděn	k2eAgInSc4d1	zavražděn
opozicí	opozice	k1gFnSc7	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
demokracie	demokracie	k1gFnSc1	demokracie
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
ustavena	ustavit	k5eAaPmNgFnS	ustavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
padla	padnout	k5eAaPmAgFnS	padnout
vojenská	vojenský	k2eAgFnSc1d1	vojenská
diktatura	diktatura	k1gFnSc1	diktatura
Jeorjiosa	Jeorjiosa	k1gFnSc1	Jeorjiosa
Papadopulos	Papadopulos	k1gInSc4	Papadopulos
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
pravicová	pravicový	k2eAgFnSc1d1	pravicová
strana	strana	k1gFnSc1	strana
Nea	Nea	k1gFnSc1	Nea
Dimokratia	Dimokratia	k1gFnSc1	Dimokratia
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgInS	založit
Konstantínos	Konstantínos	k1gInSc1	Konstantínos
Karamanlís	Karamanlís	k1gInSc1	Karamanlís
(	(	kIx(	(
<g/>
starší	starý	k2eAgMnSc1d2	starší
<g/>
)	)	kIx)	)
a	a	k8xC	a
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
chopila	chopit	k5eAaPmAgFnS	chopit
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
politik	politik	k1gMnSc1	politik
Andreas	Andreas	k1gMnSc1	Andreas
Papandreu	Papandre	k2eAgFnSc4d1	Papandre
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
stranu	strana	k1gFnSc4	strana
PASOK	PASOK	kA	PASOK
(	(	kIx(	(
<g/>
Panellinio	Panellinio	k1gMnSc1	Panellinio
sosialistiko	sosialistika	k1gFnSc5	sosialistika
kinima-Panhelénské	kinima-Panhelénský	k2eAgNnSc1d1	kinima-Panhelénský
socialistické	socialistický	k2eAgNnSc1d1	socialistické
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
patří	patřit	k5eAaImIp3nP	patřit
dodnes	dodnes	k6eAd1	dodnes
k	k	k7c3	k
nejsilnějším	silný	k2eAgFnPc3d3	nejsilnější
politickým	politický	k2eAgFnPc3d1	politická
stranám	strana	k1gFnPc3	strana
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
větší	veliký	k2eAgInSc4d2	veliký
pokles	pokles	k1gInSc4	pokles
poté	poté	k6eAd1	poté
co	co	k9	co
byly	být	k5eAaImAgInP	být
odhaleny	odhalen	k2eAgInPc1d1	odhalen
podvody	podvod	k1gInPc1	podvod
se	s	k7c7	s
statistikami	statistika	k1gFnPc7	statistika
strany	strana	k1gFnSc2	strana
PASOK	PASOK	kA	PASOK
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
Prokopis	Prokopis	k1gInSc4	Prokopis
Pavlopulos	Pavlopulosa	k1gFnPc2	Pavlopulosa
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
vůli	vůle	k1gFnSc6	vůle
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
300	[number]	k4	300
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
Vuleftes	Vuleftes	k1gMnSc1	Vuleftes
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
lidem	lid	k1gInSc7	lid
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
předčasnými	předčasný	k2eAgFnPc7d1	předčasná
volbami	volba	k1gFnPc7	volba
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
soudem	soud	k1gInSc7	soud
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Areopagus	Areopagus	k1gInSc1	Areopagus
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Arios	arioso	k1gNnPc2	arioso
Pagos	Pagosa	k1gFnPc2	Pagosa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Areův	Areův	k2eAgInSc1d1	Areův
vrch	vrch	k1gInSc1	vrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
13	[number]	k4	13
krajů	kraj	k1gInPc2	kraj
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
do	do	k7c2	do
74	[number]	k4	74
regionálních	regionální	k2eAgFnPc2d1	regionální
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
perifereiakés	perifereiakésit	k5eAaPmRp2nS	perifereiakésit
enóti	enóť	k1gFnSc2	enóť
<g/>
̱	̱	k?	̱
<g/>
tes	tes	k1gInSc1	tes
<g/>
;	;	kIx,	;
jednotné	jednotný	k2eAgNnSc1d1	jednotné
číslo	číslo	k1gNnSc1	číslo
<g/>
:	:	kIx,	:
perifereiakí	perifereiakí	k1gFnSc1	perifereiakí
<g/>
̱	̱	k?	̱
enóti	enóť	k1gFnSc2	enóť
<g/>
̱	̱	k?	̱
<g/>
ta	ten	k3xDgFnSc1	ten
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
1	[number]	k4	1
autonomní	autonomní	k2eAgInSc4d1	autonomní
stát	stát	k1gInSc4	stát
(	(	kIx(	(
<g/>
Athos	Athos	k1gInSc4	Athos
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Attika	Attika	k1gFnSc1	Attika
–	–	k?	–
Jižní	jižní	k2eAgFnSc2d1	jižní
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
Ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnPc1d1	severní
Athény	Athéna	k1gFnPc1	Athéna
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnPc1d1	střední
Athény	Athéna	k1gFnPc1	Athéna
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnPc1d1	západní
Athény	Athéna	k1gFnPc1	Athéna
<g/>
,	,	kIx,	,
Východní	východní	k2eAgFnSc1d1	východní
Attika	Attika	k1gFnSc1	Attika
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnSc1d1	západní
Attika	Attika	k1gFnSc1	Attika
<g/>
,	,	kIx,	,
Pireus	Pireus	k1gMnSc1	Pireus
Střední	střední	k2eAgNnSc1d1	střední
Řecko	Řecko	k1gNnSc1	Řecko
–	–	k?	–
Bojótie	Bojótie	k1gFnSc1	Bojótie
<g/>
,	,	kIx,	,
Euboia	Euboia	k1gFnSc1	Euboia
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Euritánie	Euritánie	k1gFnSc1	Euritánie
<g/>
,	,	kIx,	,
Fókida	Fókida	k1gFnSc1	Fókida
<g/>
,	,	kIx,	,
Fthiótida	Fthiótida	k1gFnSc1	Fthiótida
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc1d1	střední
Makedonie	Makedonie	k1gFnSc1	Makedonie
–	–	k?	–
Chalkidiki	Chalkidik	k1gFnSc2	Chalkidik
<g/>
,	,	kIx,	,
Imathie	Imathie	k1gFnSc2	Imathie
<g/>
,	,	kIx,	,
Kilkis	Kilkis	k1gFnSc2	Kilkis
<g/>
,	,	kIx,	,
Pella	Pello	k1gNnSc2	Pello
<g/>
,	,	kIx,	,
Pieria	Pierium	k1gNnSc2	Pierium
<g/>
,	,	kIx,	,
Serres	Serres	k1gInSc1	Serres
<g/>
,	,	kIx,	,
Soluň	Soluň	k1gFnSc1	Soluň
Kréta	Kréta	k1gFnSc1	Kréta
–	–	k?	–
Chania	Chanium	k1gNnSc2	Chanium
<g/>
,	,	kIx,	,
Heraklion	Heraklion	k1gInSc1	Heraklion
<g/>
,	,	kIx,	,
Lasithi	Lasithi	k1gNnSc1	Lasithi
<g/>
,	,	kIx,	,
Rethymno	Rethymno	k6eAd1	Rethymno
Východní	východní	k2eAgFnSc1d1	východní
Makedonie	Makedonie	k1gFnSc1	Makedonie
a	a	k8xC	a
Thrákie	Thrákie	k1gFnSc1	Thrákie
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
Evros	Evros	k1gInSc1	Evros
<g/>
,	,	kIx,	,
Kavala	Kavala	k1gFnSc1	Kavala
<g/>
,	,	kIx,	,
Rodopy	Rodopa	k1gFnPc1	Rodopa
<g/>
,	,	kIx,	,
Thasos	Thasos	k1gInSc1	Thasos
<g/>
,	,	kIx,	,
Xánthi	Xánthi	k1gNnSc1	Xánthi
Epirus	Epirus	k1gMnSc1	Epirus
–	–	k?	–
Arta	Arta	k1gMnSc1	Arta
<g/>
,	,	kIx,	,
Janina	Janin	k2eAgFnSc1d1	Janina
<g/>
,	,	kIx,	,
Preveza	Preveza	k1gFnSc1	Preveza
<g/>
,	,	kIx,	,
Thesprotie	Thesprotie	k1gFnPc1	Thesprotie
Jónské	jónský	k2eAgInPc4d1	jónský
ostrovy	ostrov	k1gInPc4	ostrov
–	–	k?	–
Ithaka	Ithaka	k1gFnSc1	Ithaka
<g/>
,	,	kIx,	,
Kefalonie	Kefalonie	k1gFnSc1	Kefalonie
<g/>
,	,	kIx,	,
Korfu	Korfu	k1gNnSc1	Korfu
<g/>
,	,	kIx,	,
Lefkada	Lefkada	k1gFnSc1	Lefkada
<g/>
,	,	kIx,	,
Zakynthos	Zakynthos	k1gMnSc1	Zakynthos
Severní	severní	k2eAgFnSc2d1	severní
Egeis	Egeis	k1gFnSc2	Egeis
–	–	k?	–
Chios	Chios	k1gMnSc1	Chios
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Ikaria	Ikarium	k1gNnPc1	Ikarium
<g/>
,	,	kIx,	,
Lémnos	Lémnos	k1gInSc1	Lémnos
<g/>
,	,	kIx,	,
Lesbos	Lesbos	k1gInSc1	Lesbos
<g/>
,	,	kIx,	,
Samos	Samos	k1gInSc1	Samos
Peloponés	Peloponés	k1gInSc1	Peloponés
–	–	k?	–
Arkádie	Arkádie	k1gFnSc1	Arkádie
<g/>
,	,	kIx,	,
Argolis	Argolis	k1gFnSc1	Argolis
<g/>
,	,	kIx,	,
Korinthie	Korinthie	k1gFnSc1	Korinthie
<g/>
,	,	kIx,	,
Lakónie	Lakónie	k1gFnSc1	Lakónie
<g/>
,	,	kIx,	,
Messénie	Messénie	k1gFnSc1	Messénie
Jižní	jižní	k2eAgFnSc2d1	jižní
Egeis	Egeis	k1gFnSc2	Egeis
–	–	k?	–
Andros	Androsa	k1gFnPc2	Androsa
<g/>
,	,	kIx,	,
Kalymnos	Kalymnos	k1gMnSc1	Kalymnos
<g/>
,	,	kIx,	,
Karpathos	Karpathos	k1gMnSc1	Karpathos
<g/>
,	,	kIx,	,
Kea-Kythnos	Kea-Kythnos	k1gMnSc1	Kea-Kythnos
<g/>
,	,	kIx,	,
Kos	Kos	k1gMnSc1	Kos
<g/>
,	,	kIx,	,
Milos	Milos	k1gMnSc1	Milos
<g/>
,	,	kIx,	,
Mykonos	Mykonos	k1gMnSc1	Mykonos
<g/>
,	,	kIx,	,
Naxos	Naxos	k1gInSc1	Naxos
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Paros	Paros	k1gInSc1	Paros
<g/>
,	,	kIx,	,
Rhodos	Rhodos	k1gInSc1	Rhodos
<g/>
,	,	kIx,	,
Syros	Syros	k1gMnSc1	Syros
<g/>
,	,	kIx,	,
Théra	Théra	k1gMnSc1	Théra
<g/>
,	,	kIx,	,
Tinos	Tinos	k1gMnSc1	Tinos
Thesálie	Thesálie	k1gFnSc2	Thesálie
–	–	k?	–
Karditsa	Karditsa	k1gFnSc1	Karditsa
<g/>
,	,	kIx,	,
Lárisa	Lárisa	k1gFnSc1	Lárisa
<g/>
,	,	kIx,	,
Magnesia	magnesium	k1gNnSc2	magnesium
<g/>
,	,	kIx,	,
Sporady	Sporada	k1gFnSc2	Sporada
<g/>
,	,	kIx,	,
Trikala	Trikal	k1gMnSc2	Trikal
Západní	západní	k2eAgNnSc1d1	západní
Řecko	Řecko	k1gNnSc1	Řecko
–	–	k?	–
Achaia	Achaia	k1gFnSc1	Achaia
<g/>
,	,	kIx,	,
Aitólie-Akarnánie	Aitólie-Akarnánie	k1gFnSc1	Aitólie-Akarnánie
<g/>
,	,	kIx,	,
Élida	Élida	k1gFnSc1	Élida
Západní	západní	k2eAgFnSc2d1	západní
Makedonie	Makedonie	k1gFnSc2	Makedonie
–	–	k?	–
Florina	Florina	k1gFnSc1	Florina
<g/>
,	,	kIx,	,
Grevena	Greven	k2eAgFnSc1d1	Grevena
<g/>
,	,	kIx,	,
Kastoria	Kastorium	k1gNnSc2	Kastorium
<g/>
,	,	kIx,	,
Kozani	Kozaň	k1gFnSc3	Kozaň
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
ekonomika	ekonomika	k1gFnSc1	ekonomika
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejslabší	slabý	k2eAgFnPc4d3	nejslabší
ekonomiky	ekonomika	k1gFnPc4	ekonomika
EU	EU	kA	EU
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zapříčiněno	zapříčiněn	k2eAgNnSc1d1	zapříčiněno
několika	několik	k4yIc2	několik
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dějinného	dějinný	k2eAgInSc2d1	dějinný
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znamenal	znamenat	k5eAaImAgInS	znamenat
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
Řecku	Řecko	k1gNnSc6	Řecko
nevyřešené	vyřešený	k2eNgInPc1d1	nevyřešený
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
hraniční	hraniční	k2eAgFnSc1d1	hraniční
otázka	otázka	k1gFnSc1	otázka
s	s	k7c7	s
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
fakticky	fakticky	k6eAd1	fakticky
pozastavena	pozastavit	k5eAaPmNgFnS	pozastavit
možná	možný	k2eAgFnSc1d1	možná
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zemi	zem	k1gFnSc4	zem
unikají	unikat	k5eAaImIp3nP	unikat
potenciálně	potenciálně	k6eAd1	potenciálně
značné	značný	k2eAgInPc4d1	značný
příjmy	příjem	k1gInPc4	příjem
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Řecko	Řecko	k1gNnSc1	Řecko
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc4d1	malá
plochu	plocha	k1gFnSc4	plocha
úrodné	úrodný	k2eAgFnSc2d1	úrodná
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
i	i	k9	i
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
)	)	kIx)	)
zabírají	zabírat	k5eAaImIp3nP	zabírat
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
kamenitá	kamenitý	k2eAgFnSc1d1	kamenitá
neúrodná	úrodný	k2eNgFnSc1d1	neúrodná
půda	půda	k1gFnSc1	půda
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
Řekové	Řek	k1gMnPc1	Řek
tuto	tento	k3xDgFnSc4	tento
bezútěšnou	bezútěšný	k2eAgFnSc4d1	bezútěšná
situaci	situace	k1gFnSc4	situace
řešili	řešit	k5eAaImAgMnP	řešit
kolonizací	kolonizace	k1gFnSc7	kolonizace
ostatního	ostatní	k2eAgNnSc2d1	ostatní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgNnSc1d1	moderní
Řecko	Řecko	k1gNnSc1	Řecko
musí	muset	k5eAaImIp3nS	muset
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
potřebných	potřebný	k2eAgFnPc2d1	potřebná
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
surovin	surovina	k1gFnPc2	surovina
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
původu	původ	k1gInSc2	původ
dovážet	dovážet	k5eAaImF	dovážet
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
řecké	řecký	k2eAgFnSc2d1	řecká
ekonomiky	ekonomika	k1gFnSc2	ekonomika
tvoří	tvořit	k5eAaImIp3nS	tvořit
turistický	turistický	k2eAgInSc1d1	turistický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgNnSc7d1	důležité
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
námořní	námořní	k2eAgInSc1d1	námořní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
rovněž	rovněž	k9	rovněž
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
mnoho	mnoho	k4c4	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
flotilu	flotila	k1gFnSc4	flotila
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
pracuje	pracovat	k5eAaImIp3nS	pracovat
i	i	k9	i
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgNnSc4d1	řecké
obchodní	obchodní	k2eAgNnSc4d1	obchodní
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
loďstvo	loďstvo	k1gNnSc4	loďstvo
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgNnSc4d3	veliký
světové	světový	k2eAgNnSc4d1	světové
loďstvo	loďstvo	k1gNnSc4	loďstvo
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
až	až	k9	až
18	[number]	k4	18
<g/>
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
řeckou	řecký	k2eAgFnSc4d1	řecká
ekonomiku	ekonomika	k1gFnSc4	ekonomika
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
i	i	k8xC	i
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
a	a	k8xC	a
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
chovu	chov	k1gInSc2	chov
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
koz	koza	k1gFnPc2	koza
<g/>
,	,	kIx,	,
prožívá	prožívat	k5eAaImIp3nS	prožívat
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
hluboký	hluboký	k2eAgInSc4d1	hluboký
úpadek	úpadek	k1gInSc4	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Hovězí	hovězí	k2eAgInSc4d1	hovězí
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
krav	kráva	k1gFnPc2	kráva
na	na	k7c4	na
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chován	chovat	k5eAaImNgInS	chovat
jen	jen	k9	jen
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
ekonomika	ekonomika	k1gFnSc1	ekonomika
začala	začít	k5eAaPmAgFnS	začít
rapidně	rapidně	k6eAd1	rapidně
postupovat	postupovat	k5eAaImF	postupovat
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
ujal	ujmout	k5eAaPmAgMnS	ujmout
pravicový	pravicový	k2eAgMnSc1d1	pravicový
politik	politik	k1gMnSc1	politik
Konstantinos	Konstantinosa	k1gFnPc2	Konstantinosa
Karamanlis	Karamanlis	k1gInSc1	Karamanlis
(	(	kIx(	(
<g/>
starší	starý	k2eAgMnSc1d2	starší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gNnPc2	jehož
období	období	k1gNnPc2	období
byl	být	k5eAaImAgInS	být
zmodernizován	zmodernizovat	k5eAaPmNgInS	zmodernizovat
především	především	k9	především
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
srovnání	srovnání	k1gNnSc6	srovnání
málo	málo	k6eAd1	málo
výkonný	výkonný	k2eAgMnSc1d1	výkonný
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
politika	politika	k1gFnSc1	politika
Andrease	Andreasa	k1gFnSc3	Andreasa
Papandreu	Papandrea	k1gFnSc4	Papandrea
pomohla	pomoct	k5eAaPmAgFnS	pomoct
vyřešit	vyřešit	k5eAaPmF	vyřešit
těžké	těžký	k2eAgInPc4d1	těžký
sociální	sociální	k2eAgInPc4d1	sociální
problémy	problém	k1gInPc4	problém
mnoha	mnoho	k4c3	mnoho
občanů	občan	k1gMnPc2	občan
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
vyspělý	vyspělý	k2eAgInSc4d1	vyspělý
školský	školský	k2eAgInSc4d1	školský
a	a	k8xC	a
zdravotnický	zdravotnický	k2eAgInSc4d1	zdravotnický
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
touto	tento	k3xDgFnSc7	tento
štědrou	štědrý	k2eAgFnSc7d1	štědrá
sociální	sociální	k2eAgFnSc7d1	sociální
politikou	politika	k1gFnSc7	politika
začalo	začít	k5eAaPmAgNnS	začít
postupné	postupný	k2eAgNnSc1d1	postupné
zadlužování	zadlužování	k1gNnSc1	zadlužování
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
vlády	vláda	k1gFnSc2	vláda
soustředila	soustředit	k5eAaPmAgFnS	soustředit
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
a	a	k8xC	a
ne	ne	k9	ne
na	na	k7c4	na
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
průmysl	průmysl	k1gInSc1	průmysl
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
velké	velký	k2eAgFnPc4d1	velká
investice	investice	k1gFnPc4	investice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
stát	stát	k1gInSc1	stát
nemohl	moct	k5eNaImAgInS	moct
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
řeckou	řecký	k2eAgFnSc7d1	řecká
ekonomikou	ekonomika	k1gFnSc7	ekonomika
se	se	k3xPyFc4	se
prohloubily	prohloubit	k5eAaPmAgFnP	prohloubit
začátkem	začátkem	k7c2	začátkem
tohoto	tento	k3xDgNnSc2	tento
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c2	za
další	další	k2eAgFnSc2d1	další
vlády	vláda	k1gFnSc2	vláda
PASOK	PASOK	kA	PASOK
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
premiérem	premiér	k1gMnSc7	premiér
Kostas	Kostas	k1gMnSc1	Kostas
Simitis	Simitis	k1gFnSc1	Simitis
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
však	však	k9	však
ekonomika	ekonomika	k1gFnSc1	ekonomika
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
během	během	k7c2	během
pravicové	pravicový	k2eAgFnSc2d1	pravicová
vlády	vláda	k1gFnSc2	vláda
Nea	Nea	k1gFnSc2	Nea
ND	ND	kA	ND
<g/>
,	,	kIx,	,
Kostase	Kostasa	k1gFnSc6	Kostasa
Karamanlise	Karamanlise	k1gFnSc1	Karamanlise
mladšího	mladý	k2eAgMnSc2d2	mladší
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgInSc1d1	vládní
kabinet	kabinet	k1gInSc1	kabinet
po	po	k7c6	po
odstoupení	odstoupení	k1gNnSc6	odstoupení
čelil	čelit	k5eAaImAgInS	čelit
skandálům	skandál	k1gInPc3	skandál
s	s	k7c7	s
korupcí	korupce	k1gFnSc7	korupce
a	a	k8xC	a
zatajování	zatajování	k1gNnSc1	zatajování
státního	státní	k2eAgInSc2d1	státní
deficitu	deficit	k1gInSc2	deficit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
až	až	k9	až
na	na	k7c4	na
14	[number]	k4	14
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
šetřila	šetřit	k5eAaImAgFnS	šetřit
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
obstarávala	obstarávat	k5eAaImAgFnS	obstarávat
další	další	k2eAgFnPc4d1	další
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
půjčky	půjčka	k1gFnPc4	půjčka
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
standardu	standard	k1gInSc2	standard
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
Jeorjosa	Jeorjosa	k1gFnSc1	Jeorjosa
Papandreu	Papandreus	k1gInSc2	Papandreus
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
musela	muset	k5eAaImAgFnS	muset
přijmout	přijmout	k5eAaPmF	přijmout
tvrdá	tvrdý	k2eAgNnPc4d1	tvrdé
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
např.	např.	kA	např.
snížení	snížení	k1gNnSc1	snížení
platů	plat	k1gInPc2	plat
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
důchodového	důchodový	k2eAgInSc2d1	důchodový
věku	věk	k1gInSc2	věk
ze	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
na	na	k7c4	na
65	[number]	k4	65
<g/>
.	.	kIx.	.
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
i	i	k9	i
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
19	[number]	k4	19
na	na	k7c4	na
21	[number]	k4	21
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
kroky	krok	k1gInPc4	krok
dostala	dostat	k5eAaPmAgFnS	dostat
řecká	řecký	k2eAgFnSc1d1	řecká
vláda	vláda	k1gFnSc1	vláda
půjčku	půjčka	k1gFnSc4	půjčka
od	od	k7c2	od
EU	EU	kA	EU
a	a	k8xC	a
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
celkově	celkově	k6eAd1	celkově
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
114	[number]	k4	114
miliard	miliarda	k4xCgFnPc2	miliarda
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Radikálními	radikální	k2eAgNnPc7d1	radikální
opatřeními	opatření	k1gNnPc7	opatření
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
finanční	finanční	k2eAgFnSc7d1	finanční
půjčkou	půjčka	k1gFnSc7	půjčka
se	se	k3xPyFc4	se
Řecko	Řecko	k1gNnSc1	Řecko
vyhnulo	vyhnout	k5eAaPmAgNnS	vyhnout
bankrotu	bankrot	k1gInSc3	bankrot
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
snížit	snížit	k5eAaPmF	snížit
schodek	schodek	k1gInSc4	schodek
na	na	k7c4	na
8	[number]	k4	8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Radikální	radikální	k2eAgInPc1d1	radikální
postupy	postup	k1gInPc1	postup
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
však	však	k9	však
neobešly	obešt	k5eNaBmAgFnP	obešt
bez	bez	k7c2	bez
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
protestovalo	protestovat	k5eAaBmAgNnS	protestovat
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Snížit	snížit	k5eAaPmF	snížit
deficit	deficit	k1gInSc4	deficit
pod	pod	k7c4	pod
8	[number]	k4	8
%	%	kIx~	%
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
nedařilo	dařit	k5eNaImAgNnS	dařit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vláda	vláda	k1gFnSc1	vláda
plánovala	plánovat	k5eAaImAgFnS	plánovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
musela	muset	k5eAaImAgFnS	muset
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
přijmout	přijmout	k5eAaPmF	přijmout
další	další	k2eAgFnPc4d1	další
radikální	radikální	k2eAgFnPc4d1	radikální
úsporná	úsporný	k2eAgNnPc4d1	úsporné
opatření	opatření	k1gNnPc4	opatření
a	a	k8xC	a
změnit	změnit	k5eAaPmF	změnit
vládní	vládní	k2eAgInSc4d1	vládní
kabinet	kabinet	k1gInSc4	kabinet
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
Jorgos	Jorgos	k1gMnSc1	Jorgos
Papandreu	Papandrea	k1gFnSc4	Papandrea
však	však	k9	však
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
sjednána	sjednán	k2eAgFnSc1d1	sjednána
nová	nový	k2eAgFnSc1d1	nová
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
odpis	odpis	k1gInSc1	odpis
50	[number]	k4	50
%	%	kIx~	%
řeckého	řecký	k2eAgInSc2d1	řecký
dluhu	dluh	k1gInSc2	dluh
vůči	vůči	k7c3	vůči
bankovním	bankovní	k2eAgInPc3d1	bankovní
subjektům	subjekt	k1gInPc3	subjekt
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
půjčka	půjčka	k1gFnSc1	půjčka
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
120	[number]	k4	120
miliard	miliarda	k4xCgFnPc2	miliarda
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neshodě	neshoda	k1gFnSc6	neshoda
mezi	mezi	k7c7	mezi
některými	některý	k3yIgMnPc7	některý
vládními	vládní	k2eAgMnPc7d1	vládní
poslanci	poslanec	k1gMnPc7	poslanec
a	a	k8xC	a
premiérem	premiér	k1gMnSc7	premiér
Papandreuem	Papandreu	k1gMnSc7	Papandreu
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
sestavena	sestavit	k5eAaPmNgFnS	sestavit
nová	nový	k2eAgFnSc1d1	nová
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
nezávislým	závislý	k2eNgMnSc7d1	nezávislý
ekonomem	ekonom	k1gMnSc7	ekonom
Lukasem	Lukas	k1gMnSc7	Lukas
Papadimosem	Papadimos	k1gMnSc7	Papadimos
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
úsporných	úsporný	k2eAgNnPc6d1	úsporné
opatřeních	opatření	k1gNnPc6	opatření
<g/>
,	,	kIx,	,
parlament	parlament	k1gInSc1	parlament
přijal	přijmout	k5eAaPmAgInS	přijmout
novou	nový	k2eAgFnSc4d1	nová
půjčku	půjčka	k1gFnSc4	půjčka
a	a	k8xC	a
poloviční	poloviční	k2eAgInSc4d1	poloviční
odpis	odpis	k1gInSc4	odpis
řeckého	řecký	k2eAgInSc2d1	řecký
dluhu	dluh	k1gInSc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
následných	následný	k2eAgFnPc6d1	následná
volbách	volba	k1gFnPc6	volba
odhlasovala	odhlasovat	k5eAaPmAgFnS	odhlasovat
koalice	koalice	k1gFnSc1	koalice
premiéra	premiér	k1gMnSc2	premiér
Antonise	Antonise	k1gFnSc2	Antonise
Samarase	Samarasa	k1gFnSc3	Samarasa
další	další	k2eAgFnSc2d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetí	třetí	k4xOgFnSc2	třetí
velké	velká	k1gFnSc2	velká
memorandum	memorandum	k1gNnSc1	memorandum
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Memorandum	memorandum	k1gNnSc1	memorandum
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
domluvené	domluvený	k2eAgNnSc1d1	domluvené
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
tzv.	tzv.	kA	tzv.
trojky	trojka	k1gFnSc2	trojka
(	(	kIx(	(
<g/>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
velká	velký	k2eAgNnPc4d1	velké
úsporná	úsporný	k2eAgNnPc4d1	úsporné
opatření	opatření	k1gNnPc4	opatření
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
11	[number]	k4	11
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Jannise	Jannise	k1gFnSc2	Jannise
Sturnarase	Sturnarasa	k1gFnSc6	Sturnarasa
to	ten	k3xDgNnSc1	ten
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
poslední	poslední	k2eAgNnPc4d1	poslední
velká	velký	k2eAgNnPc4d1	velké
úsporná	úsporný	k2eAgNnPc4d1	úsporné
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
řecký	řecký	k2eAgInSc1d1	řecký
státní	státní	k2eAgInSc1d1	státní
rozpočet	rozpočet	k1gInSc1	rozpočet
první	první	k4xOgNnSc1	první
plusové	plusový	k2eAgNnSc1d1	plusové
hospodaření	hospodaření	k1gNnSc1	hospodaření
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
primární	primární	k2eAgInSc1d1	primární
přebytek	přebytek	k1gInSc1	přebytek
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
se	se	k3xPyFc4	se
Řecko	Řecko	k1gNnSc1	Řecko
mohlo	moct	k5eAaImAgNnS	moct
zčásti	zčásti	k6eAd1	zčásti
dočasně	dočasně	k6eAd1	dočasně
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
finanční	finanční	k2eAgInPc4d1	finanční
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
Samaras	Samaras	k1gMnSc1	Samaras
tehdy	tehdy	k6eAd1	tehdy
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
a	a	k8xC	a
Řecko	Řecko	k1gNnSc1	Řecko
nebude	být	k5eNaImBp3nS	být
potřebovat	potřebovat	k5eAaImF	potřebovat
další	další	k2eAgInPc4d1	další
záchranné	záchranný	k2eAgInPc4d1	záchranný
úvěry	úvěr	k1gInPc4	úvěr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
poklesu	pokles	k1gInSc2	pokles
<g/>
,	,	kIx,	,
činil	činit	k5eAaImAgInS	činit
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
PPP	PPP	kA	PPP
<g/>
)	)	kIx)	)
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
27	[number]	k4	27
tisíc	tisíc	k4xCgInPc2	tisíc
USD	USD	kA	USD
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
nominálně	nominálně	k6eAd1	nominálně
méně	málo	k6eAd2	málo
než	než	k8xS	než
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
nových	nový	k2eAgInPc6d1	nový
členských	členský	k2eAgInPc6d1	členský
státech	stát	k1gInPc6	stát
EU	EU	kA	EU
(	(	kIx(	(
<g/>
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
a	a	k8xC	a
také	také	k9	také
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samarasova	Samarasův	k2eAgFnSc1d1	Samarasův
předpověď	předpověď	k1gFnSc1	předpověď
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nepotvrdila	potvrdit	k5eNaPmAgFnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
prožívají	prožívat	k5eAaImIp3nP	prožívat
Řecko	Řecko	k1gNnSc4	Řecko
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
Evropská	evropský	k2eAgFnSc1d1	Evropská
měnová	měnový	k2eAgFnSc1d1	měnová
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
EMU	emu	k1gMnSc1	emu
<g/>
)	)	kIx)	)
mimořádně	mimořádně	k6eAd1	mimořádně
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
práceschopného	práceschopný	k2eAgNnSc2d1	práceschopné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
na	na	k7c4	na
27	[number]	k4	27
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dluhová	dluhový	k2eAgFnSc1d1	dluhová
krize	krize	k1gFnSc1	krize
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Dluhová	dluhový	k2eAgFnSc1d1	dluhová
krize	krize	k1gFnSc1	krize
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
počínání	počínání	k1gNnSc6	počínání
řeckých	řecký	k2eAgFnPc2d1	řecká
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
vládami	vláda	k1gFnPc7	vláda
Kostase	Kostasa	k1gFnSc6	Kostasa
Simitise	Simitise	k1gFnPc1	Simitise
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
Kostase	Kostasa	k1gFnSc3	Kostasa
Karamanlise	Karamanlise	k1gFnSc2	Karamanlise
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
nesprávných	správný	k2eNgFnPc2d1	nesprávná
statistik	statistika	k1gFnPc2	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
usnadnění	usnadnění	k1gNnSc1	usnadnění
vstupu	vstup	k1gInSc2	vstup
země	zem	k1gFnSc2	zem
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Eurozóny	Eurozóna	k1gFnSc2	Eurozóna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlády	vláda	k1gFnPc1	vláda
připustily	připustit	k5eAaPmAgFnP	připustit
nadměrné	nadměrný	k2eAgInPc4d1	nadměrný
výdaje	výdaj	k1gInPc4	výdaj
jak	jak	k6eAd1	jak
státního	státní	k2eAgInSc2d1	státní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
soukromého	soukromý	k2eAgInSc2d1	soukromý
sektoru	sektor	k1gInSc2	sektor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nebyly	být	k5eNaImAgFnP	být
kryté	krytý	k2eAgInPc1d1	krytý
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
výkonností	výkonnost	k1gFnSc7	výkonnost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
mimořádně	mimořádně	k6eAd1	mimořádně
vysoké	vysoký	k2eAgNnSc4d1	vysoké
zadlužení	zadlužení	k1gNnSc4	zadlužení
u	u	k7c2	u
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
věřitelů	věřitel	k1gMnPc2	věřitel
<g/>
,	,	kIx,	,
usnadněné	usnadněný	k2eAgInPc1d1	usnadněný
oboustrannou	oboustranný	k2eAgFnSc7d1	oboustranná
vírou	víra	k1gFnSc7	víra
v	v	k7c4	v
evropskou	evropský	k2eAgFnSc4d1	Evropská
měnovou	měnový	k2eAgFnSc4d1	měnová
solidaritu	solidarita	k1gFnSc4	solidarita
<g/>
,	,	kIx,	,
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
neúnosný	únosný	k2eNgInSc1d1	neúnosný
schodek	schodek	k1gInSc1	schodek
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prohloubení	prohloubení	k1gNnSc3	prohloubení
domácí	domácí	k2eAgFnSc2d1	domácí
krize	krize	k1gFnSc2	krize
značně	značně	k6eAd1	značně
přispěla	přispět	k5eAaPmAgFnS	přispět
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
krize	krize	k1gFnSc1	krize
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
a	a	k8xC	a
především	především	k6eAd1	především
finanční	finanční	k2eAgInPc1d1	finanční
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
není	být	k5eNaImIp3nS	být
podle	podle	k7c2	podle
úsudku	úsudek	k1gInSc2	úsudek
ekonomů	ekonom	k1gMnPc2	ekonom
schopné	schopný	k2eAgFnSc2d1	schopná
postupně	postupně	k6eAd1	postupně
splácet	splácet	k5eAaImF	splácet
své	svůj	k3xOyFgInPc4	svůj
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
dluhy	dluh	k1gInPc4	dluh
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
astronomické	astronomický	k2eAgFnPc4d1	astronomická
výše	výše	k1gFnPc4	výše
324	[number]	k4	324
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
zcela	zcela	k6eAd1	zcela
splatit	splatit	k5eAaPmF	splatit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
vyhlášeném	vyhlášený	k2eAgNnSc6d1	vyhlášené
novou	nový	k2eAgFnSc7d1	nová
vládou	vláda	k1gFnSc7	vláda
premiéra	premiér	k1gMnSc2	premiér
Alexise	Alexis	k1gInSc6	Alexis
Tsiprase	Tsiprasa	k1gFnSc3	Tsiprasa
(	(	kIx(	(
<g/>
hnutí	hnutí	k1gNnSc1	hnutí
Syriza	Syriza	k1gFnSc1	Syriza
<g/>
)	)	kIx)	)
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
řecký	řecký	k2eAgInSc1d1	řecký
národ	národ	k1gInSc1	národ
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přijme	přijmout	k5eAaPmIp3nS	přijmout
nebo	nebo	k8xC	nebo
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
podmínky	podmínka	k1gFnPc4	podmínka
EMU	emu	k1gMnPc3	emu
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Řecku	Řecko	k1gNnSc6	Řecko
hrozí	hrozit	k5eAaImIp3nS	hrozit
tzv.	tzv.	kA	tzv.
grexit	grexit	k1gInSc1	grexit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dobrovolné	dobrovolný	k2eAgNnSc1d1	dobrovolné
nebo	nebo	k8xC	nebo
nedobrovolné	dobrovolný	k2eNgNnSc1d1	nedobrovolné
opuštění	opuštění	k1gNnSc1	opuštění
eurozóny	eurozóna	k1gFnSc2	eurozóna
a	a	k8xC	a
znovuzavedení	znovuzavedení	k1gNnSc2	znovuzavedení
bývalé	bývalý	k2eAgFnSc2d1	bývalá
měny	měna	k1gFnSc2	měna
(	(	kIx(	(
<g/>
drachma	drachma	k1gFnSc1	drachma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
problémy	problém	k1gInPc7	problém
pro	pro	k7c4	pro
hospodářství	hospodářství	k1gNnSc4	hospodářství
země	zem	k1gFnSc2	zem
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
se	s	k7c7	s
61	[number]	k4	61
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
proti	proti	k7c3	proti
podmínkám	podmínka	k1gFnPc3	podmínka
věřitelů	věřitel	k1gMnPc2	věřitel
země	zem	k1gFnSc2	zem
a	a	k8xC	a
39	[number]	k4	39
%	%	kIx~	%
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
Janis	Janis	k1gFnPc2	Janis
Varufakis	Varufakis	k1gFnSc2	Varufakis
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Euklidis	Euklidis	k1gFnSc2	Euklidis
Tsakalotos	Tsakalotos	k1gInSc1	Tsakalotos
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
již	již	k6eAd1	již
vedl	vést	k5eAaImAgInS	vést
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
trojkou	trojka	k1gFnSc7	trojka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
vedli	vést	k5eAaImAgMnP	vést
představitelé	představitel	k1gMnPc1	představitel
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EMU	emu	k1gMnSc1	emu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Angela	Angela	k1gFnSc1	Angela
Merkelová	Merkelová	k1gFnSc1	Merkelová
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
Hollande	Holland	k1gInSc5	Holland
<g/>
,	,	kIx,	,
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Juncker	Juncker	k1gMnSc1	Juncker
a	a	k8xC	a
Donald	Donald	k1gMnSc1	Donald
Tusk	Tusk	k1gMnSc1	Tusk
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
Alexisem	Alexis	k1gInSc7	Alexis
Tsiprasem	Tsiprasma	k1gFnPc2	Tsiprasma
o	o	k7c4	o
řešení	řešení	k1gNnSc4	řešení
řecké	řecký	k2eAgFnSc2d1	řecká
dluhové	dluhový	k2eAgFnSc2d1	dluhová
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Tsipras	Tsipras	k1gInSc1	Tsipras
o	o	k7c4	o
tato	tento	k3xDgNnPc4	tento
jednání	jednání	k1gNnPc4	jednání
požádal	požádat	k5eAaPmAgMnS	požádat
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
výsledek	výsledek	k1gInSc4	výsledek
referenda	referendum	k1gNnSc2	referendum
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
krize	krize	k1gFnSc1	krize
během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
vážným	vážný	k2eAgInSc7d1	vážný
způsobem	způsob	k1gInSc7	způsob
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
podepsáno	podepsán	k2eAgNnSc4d1	podepsáno
předběžné	předběžný	k2eAgNnSc4d1	předběžné
ujednání	ujednání	k1gNnSc4	ujednání
mezi	mezi	k7c7	mezi
představiteli	představitel	k1gMnPc7	představitel
EMU	emu	k1gMnSc7	emu
a	a	k8xC	a
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
Tsiprasem	Tsipras	k1gMnSc7	Tsipras
o	o	k7c6	o
poskytnutí	poskytnutí	k1gNnSc6	poskytnutí
nových	nový	k2eAgInPc2d1	nový
úvěrů	úvěr	k1gInPc2	úvěr
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
zhruba	zhruba	k6eAd1	zhruba
86	[number]	k4	86
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
po	po	k7c6	po
odpočtu	odpočet	k1gInSc6	odpočet
předpokládaného	předpokládaný	k2eAgInSc2d1	předpokládaný
vlastního	vlastní	k2eAgInSc2d1	vlastní
řeckého	řecký	k2eAgInSc2d1	řecký
příspěvku	příspěvek	k1gInSc2	příspěvek
jen	jen	k9	jen
72	[number]	k4	72
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
třetího	třetí	k4xOgInSc2	třetí
záchranného	záchranný	k2eAgInSc2d1	záchranný
programu	program	k1gInSc2	program
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
restrukturalizaci	restrukturalizace	k1gFnSc3	restrukturalizace
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
započtena	započten	k2eAgFnSc1d1	započtena
také	také	k9	také
částka	částka	k1gFnSc1	částka
25	[number]	k4	25
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
na	na	k7c4	na
nezbytnou	zbytný	k2eNgFnSc4d1	zbytný
výpomoc	výpomoc	k1gFnSc4	výpomoc
pro	pro	k7c4	pro
opětné	opětný	k2eAgNnSc4d1	opětné
zprovoznění	zprovoznění	k1gNnSc4	zprovoznění
řeckého	řecký	k2eAgInSc2d1	řecký
finančního	finanční	k2eAgInSc2d1	finanční
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Potřebné	potřebný	k2eAgInPc4d1	potřebný
úvěry	úvěr	k1gInPc4	úvěr
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
Řecko	Řecko	k1gNnSc4	Řecko
obdržet	obdržet	k5eAaPmF	obdržet
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
Evropského	evropský	k2eAgInSc2d1	evropský
stabilizačního	stabilizační	k2eAgInSc2d1	stabilizační
mechanismu	mechanismus	k1gInSc2	mechanismus
(	(	kIx(	(
<g/>
EMS	EMS	kA	EMS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
však	však	k9	však
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
vůbec	vůbec	k9	vůbec
uskutečněn	uskutečněn	k2eAgMnSc1d1	uskutečněn
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
mj.	mj.	kA	mj.
schválen	schválit	k5eAaPmNgInS	schválit
parlamenty	parlament	k1gInPc7	parlament
některých	některý	k3yIgFnPc2	některý
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
EMU	emu	k1gMnSc1	emu
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
řecký	řecký	k2eAgInSc1d1	řecký
parlament	parlament	k1gInSc1	parlament
do	do	k7c2	do
středy	středa	k1gFnSc2	středa
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
přijmul	přijmout	k5eAaPmAgMnS	přijmout
12	[number]	k4	12
bodů	bod	k1gInPc2	bod
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
zvýšení	zvýšení	k1gNnSc4	zvýšení
sazeb	sazba	k1gFnPc2	sazba
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
přidané	přidaný	k2eAgFnSc2d1	přidaná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
věku	věk	k1gInSc2	věk
pro	pro	k7c4	pro
odchod	odchod	k1gInSc4	odchod
do	do	k7c2	do
starobního	starobní	k2eAgInSc2d1	starobní
důchodu	důchod	k1gInSc2	důchod
na	na	k7c4	na
67	[number]	k4	67
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc6	vytvoření
fondu	fond	k1gInSc2	fond
státního	státní	k2eAgInSc2d1	státní
majetku	majetek	k1gInSc2	majetek
s	s	k7c7	s
předpokládanou	předpokládaný	k2eAgFnSc7d1	předpokládaná
hodnotou	hodnota	k1gFnSc7	hodnota
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zprivatizován	zprivatizován	k2eAgInSc1d1	zprivatizován
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
z	z	k7c2	z
privatizace	privatizace	k1gFnSc2	privatizace
státního	státní	k2eAgInSc2d1	státní
majetku	majetek	k1gInSc2	majetek
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zčásti	zčásti	k6eAd1	zčásti
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
splacení	splacení	k1gNnSc6	splacení
stávajícího	stávající	k2eAgInSc2d1	stávající
státního	státní	k2eAgInSc2d1	státní
dluhu	dluh	k1gInSc2	dluh
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
25	[number]	k4	25
%	%	kIx~	%
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
předpokladu	předpoklad	k1gInSc6	předpoklad
12,5	[number]	k4	12,5
miliardy	miliarda	k4xCgFnSc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
na	na	k7c4	na
investice	investice	k1gFnPc4	investice
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
samotném	samotný	k2eAgNnSc6d1	samotné
(	(	kIx(	(
<g/>
tohoto	tento	k3xDgInSc2	tento
ústupku	ústupek	k1gInSc2	ústupek
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Tsipras	Tsipras	k1gInSc1	Tsipras
v	v	k7c6	v
bilaterálním	bilaterální	k2eAgNnSc6d1	bilaterální
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
kancléřkou	kancléřka	k1gFnSc7	kancléřka
Merkelovou	Merkelová	k1gFnSc7	Merkelová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgInSc2d1	oficiální
úřadu	úřad	k1gInSc2	úřad
ELSTAT	ELSTAT	kA	ELSTAT
mělo	mít	k5eAaImAgNnS	mít
Řecko	Řecko	k1gNnSc1	Řecko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
10	[number]	k4	10
815	[number]	k4	815
197	[number]	k4	197
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
98	[number]	k4	98
%	%	kIx~	%
<g/>
)	)	kIx)	)
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
řecké	řecký	k2eAgNnSc1d1	řecké
ortodoxní	ortodoxní	k2eAgNnSc1d1	ortodoxní
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
jen	jen	k9	jen
asi	asi	k9	asi
1,3	[number]	k4	1,3
%	%	kIx~	%
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
je	být	k5eAaImIp3nS	být
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
dvou	dva	k4xCgFnPc2	dva
největších	veliký	k2eAgFnPc2d3	veliký
řeckých	řecký	k2eAgFnPc2d1	řecká
měst	město	k1gNnPc2	město
–	–	k?	–
Athén	Athéna	k1gFnPc2	Athéna
a	a	k8xC	a
Soluně	Soluň	k1gFnSc2	Soluň
–	–	k?	–
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
počtu	počet	k1gInSc2	počet
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
4	[number]	k4	4
milióny	milión	k4xCgInPc1	milión
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
mírně	mírně	k6eAd1	mírně
přes	přes	k7c4	přes
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
moderního	moderní	k2eAgNnSc2d1	moderní
Řecka	Řecko	k1gNnSc2	Řecko
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
narození	narození	k1gNnPc2	narození
přesažen	přesáhnout	k5eAaPmNgInS	přesáhnout
počtem	počet	k1gInSc7	počet
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1	řecké
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
10	[number]	k4	10
930	[number]	k4	930
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
760	[number]	k4	760
000	[number]	k4	000
bylo	být	k5eAaImAgNnS	být
původu	původ	k1gInSc2	původ
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
řeckého	řecký	k2eAgNnSc2d1	řecké
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
rozličných	rozličný	k2eAgFnPc2d1	rozličná
jazykové	jazykový	k2eAgNnSc4d1	jazykové
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Neúplný	úplný	k2eNgInSc1d1	neúplný
výčet	výčet	k1gInSc1	výčet
by	by	kYmCp3nS	by
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
Pomaky	Pomak	k1gInPc4	Pomak
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
skupiny	skupina	k1gFnPc4	skupina
Romů	Rom	k1gMnPc2	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc4	množství
náboženských	náboženský	k2eAgFnPc2d1	náboženská
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
muslimů	muslim	k1gMnPc2	muslim
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Thrákii	Thrákie	k1gFnSc6	Thrákie
<g/>
,	,	kIx,	,
tvořících	tvořící	k2eAgInPc6d1	tvořící
většinu	většina	k1gFnSc4	většina
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
vytvářejících	vytvářející	k2eAgMnPc2d1	vytvářející
tak	tak	k6eAd1	tak
druhou	druhý	k4xOgFnSc4	druhý
nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
je	být	k5eAaImIp3nS	být
novořečtina	novořečtina	k1gFnSc1	novořečtina
Dimotiki	Dimotik	k1gFnSc2	Dimotik
(	(	kIx(	(
<g/>
lidový	lidový	k2eAgInSc4d1	lidový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
hovořila	hovořit	k5eAaImAgFnS	hovořit
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Dimotiki	Dimotiki	k6eAd1	Dimotiki
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
ze	z	k7c2	z
starořečtiny	starořečtina	k1gFnSc2	starořečtina
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
koiné	koiné	k1gFnSc2	koiné
(	(	kIx(	(
<g/>
společná	společný	k2eAgFnSc1d1	společná
řeč	řeč	k1gFnSc1	řeč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
Kr.	Kr.	k1gMnSc1	Kr.
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jónské	jónský	k2eAgFnSc2d1	jónská
attičtiny	attičtina	k1gFnSc2	attičtina
jako	jako	k8xS	jako
univerzální	univerzální	k2eAgInSc4d1	univerzální
jazyk	jazyk	k1gInSc4	jazyk
všech	všecek	k3xTgMnPc2	všecek
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
standardní	standardní	k2eAgFnSc2d1	standardní
novořečtiny	novořečtina	k1gFnSc2	novořečtina
dimotiki	dimotiki	k6eAd1	dimotiki
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
novořecké	novořecký	k2eAgInPc1d1	novořecký
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
hovoří	hovořit	k5eAaImIp3nP	hovořit
určité	určitý	k2eAgFnPc4d1	určitá
skupiny	skupina	k1gFnPc4	skupina
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jazyky	jazyk	k1gInPc4	jazyk
pontská	pontský	k2eAgFnSc1d1	pontský
řečtina	řečtina	k1gFnSc1	řečtina
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tsakonština	tsakonština	k1gFnSc1	tsakonština
(	(	kIx(	(
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
starověké	starověký	k2eAgFnSc2d1	starověká
dórštiny	dórština	k1gFnSc2	dórština
<g/>
)	)	kIx)	)
a	a	k8xC	a
kapadócká	kapadócký	k2eAgFnSc1d1	kapadócký
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
menšinovým	menšinový	k2eAgInSc7d1	menšinový
řeckým	řecký	k2eAgInSc7d1	řecký
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
pontská	pontský	k2eAgFnSc1d1	pontský
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mluví	mluvit	k5eAaImIp3nP	mluvit
pontští	pontský	k2eAgMnPc1d1	pontský
Řekové	Řek	k1gMnPc1	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Kapadóčtina	Kapadóčtina	k1gFnSc1	Kapadóčtina
a	a	k8xC	a
cakonština	cakonština	k1gFnSc1	cakonština
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
také	také	k9	také
albánskou	albánský	k2eAgFnSc7d1	albánská
arvanitštinou	arvanitština	k1gFnSc7	arvanitština
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
používají	používat	k5eAaImIp3nP	používat
Arvanité	Arvanitý	k2eAgNnSc1d1	Arvanitý
(	(	kIx(	(
<g/>
asi	asi	k9	asi
200	[number]	k4	200
000	[number]	k4	000
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
románskou	románský	k2eAgFnSc7d1	románská
arumunštinou	arumunština	k1gFnSc7	arumunština
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mluví	mluvit	k5eAaImIp3nP	mluvit
řečtí	řecký	k2eAgMnPc1d1	řecký
Arumuni	Arumun	k1gMnPc1	Arumun
(	(	kIx(	(
<g/>
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
řeckou	řecký	k2eAgFnSc7d1	řecká
slovanštinou	slovanština	k1gFnSc7	slovanština
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
hovoří	hovořit	k5eAaImIp3nP	hovořit
řečtí	řecký	k2eAgMnPc1d1	řecký
Slované	Slovan	k1gMnPc1	Slovan
(	(	kIx(	(
<g/>
asi	asi	k9	asi
60	[number]	k4	60
000	[number]	k4	000
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muslimská	muslimský	k2eAgFnSc1d1	muslimská
turecká	turecký	k2eAgFnSc1d1	turecká
menšina	menšina	k1gFnSc1	menšina
mluví	mluvit	k5eAaImIp3nS	mluvit
turecky	turecky	k6eAd1	turecky
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
prošlo	projít	k5eAaPmAgNnS	projít
po	po	k7c6	po
silné	silný	k2eAgFnSc6d1	silná
reemigrační	reemigrační	k2eAgFnSc6d1	reemigrační
vlně	vlna	k1gFnSc6	vlna
změnou	změna	k1gFnSc7	změna
z	z	k7c2	z
vystěhovalecké	vystěhovalecký	k2eAgFnSc2d1	vystěhovalecká
země	zem	k1gFnSc2	zem
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
imigrační	imigrační	k2eAgFnSc6d1	imigrační
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
pracoval	pracovat	k5eAaImAgMnS	pracovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
asi	asi	k9	asi
milion	milion	k4xCgInSc4	milion
Řeků	Řek	k1gMnPc2	Řek
/	/	kIx~	/
<g/>
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vrátila	vrátit	k5eAaPmAgFnS	vrátit
domů	dům	k1gInPc2	dům
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
asi	asi	k9	asi
603	[number]	k4	603
000	[number]	k4	000
migrantů	migrant	k1gMnPc2	migrant
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
340	[number]	k4	340
000	[number]	k4	000
s	s	k7c7	s
řádným	řádný	k2eAgNnSc7d1	řádné
povolením	povolení	k1gNnSc7	povolení
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
asi	asi	k9	asi
260	[number]	k4	260
000	[number]	k4	000
"	"	kIx"	"
<g/>
ilegálních	ilegální	k2eAgMnPc2d1	ilegální
migrantů	migrant	k1gMnPc2	migrant
<g/>
"	"	kIx"	"
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
3000	[number]	k4	3000
utečenců	utečenec	k1gMnPc2	utečenec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
legálními	legální	k2eAgMnPc7d1	legální
imigranty	imigrant	k1gMnPc7	imigrant
převažovali	převažovat	k5eAaImAgMnP	převažovat
řečtí	řecký	k2eAgMnPc1d1	řecký
"	"	kIx"	"
<g/>
navrátilci	navrátilec	k1gMnPc1	navrátilec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
především	především	k9	především
Řekové	Řek	k1gMnPc1	Řek
ze	z	k7c2	z
severního	severní	k2eAgNnSc2d1	severní
Černomoří	Černomoří	k1gNnSc2	Černomoří
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
"	"	kIx"	"
<g/>
ilegály	ilegál	k1gMnPc4	ilegál
<g/>
"	"	kIx"	"
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odstupem	odstup	k1gInSc7	odstup
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
následovaly	následovat	k5eAaImAgFnP	následovat
země	zem	k1gFnPc1	zem
"	"	kIx"	"
<g/>
třetího	třetí	k4xOgMnSc2	třetí
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
a	a	k8xC	a
Filipínami	Filipíny	k1gFnPc7	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgInSc1d1	právní
status	status	k1gInSc1	status
přistěhovalce	přistěhovalka	k1gFnSc3	přistěhovalka
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
velmi	velmi	k6eAd1	velmi
nejasný	jasný	k2eNgInSc1d1	nejasný
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
ilegálních	ilegální	k2eAgMnPc2d1	ilegální
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
vedlo	vést	k5eAaImAgNnS	vést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
k	k	k7c3	k
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
3	[number]	k4	3
legalizačních	legalizační	k2eAgInPc2d1	legalizační
programů	program	k1gInPc2	program
nařízených	nařízený	k2eAgInPc2d1	nařízený
řeckým	řecký	k2eAgInSc7d1	řecký
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
imigrovalo	imigrovat	k5eAaBmAgNnS	imigrovat
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
několik	několik	k4yIc1	několik
prominentních	prominentní	k2eAgMnPc2d1	prominentní
řeckých	řecký	k2eAgMnPc2d1	řecký
sportovců	sportovec	k1gMnPc2	sportovec
jako	jako	k8xS	jako
etničtí	etnický	k2eAgMnPc1d1	etnický
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Albánie	Albánie	k1gFnSc2	Albánie
a	a	k8xC	a
Gruzie	Gruzie	k1gFnSc2	Gruzie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
legendárních	legendární	k2eAgMnPc2d1	legendární
vzpěračů	vzpěrač	k1gMnPc2	vzpěrač
Pyrrose	Pyrrosa	k1gFnSc3	Pyrrosa
Dimase	Dimasa	k1gFnSc3	Dimasa
a	a	k8xC	a
Kakhiho	Kakhi	k1gMnSc4	Kakhi
Kakhiashviliho	Kakhiashvili	k1gMnSc4	Kakhiashvili
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
nadvládou	nadvláda	k1gFnSc7	nadvláda
Osmanů	Osman	k1gMnPc2	Osman
bylo	být	k5eAaImAgNnS	být
Řecko	Řecko	k1gNnSc4	Řecko
součástí	součást	k1gFnPc2	součást
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
náboženské	náboženský	k2eAgNnSc4d1	náboženské
centrum	centrum	k1gNnSc4	centrum
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Konstantinem	Konstantin	k1gMnSc7	Konstantin
I.	I.	kA	I.
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Konstantinových	Konstantinových	k2eAgInPc2d1	Konstantinových
časů	čas	k1gInPc2	čas
se	se	k3xPyFc4	se
ortodoxní	ortodoxní	k2eAgFnSc1d1	ortodoxní
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
víra	víra	k1gFnSc1	víra
úspěšně	úspěšně	k6eAd1	úspěšně
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
a	a	k8xC	a
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
Turků	turek	k1gInPc2	turek
a	a	k8xC	a
po	po	k7c6	po
opakovaných	opakovaný	k2eAgInPc6d1	opakovaný
pokusech	pokus	k1gInPc6	pokus
jezuitů	jezuita	k1gMnPc2	jezuita
a	a	k8xC	a
poté	poté	k6eAd1	poté
protestantů	protestant	k1gMnPc2	protestant
o	o	k7c6	o
převrácení	převrácení	k1gNnSc6	převrácení
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
ortodoxní	ortodoxní	k2eAgNnSc4d1	ortodoxní
křesťanství	křesťanství	k1gNnSc4	křesťanství
přežívalo	přežívat	k5eAaImAgNnS	přežívat
a	a	k8xC	a
vzkvétalo	vzkvétat	k5eAaImAgNnS	vzkvétat
<g/>
.	.	kIx.	.
</s>
<s>
Úloha	úloha	k1gFnSc1	úloha
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
udržování	udržování	k1gNnSc1	udržování
řecké	řecký	k2eAgFnSc2d1	řecká
etnické	etnický	k2eAgFnSc2d1	etnická
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc2d1	kulturní
identity	identita	k1gFnSc2	identita
během	během	k7c2	během
400	[number]	k4	400
let	léto	k1gNnPc2	léto
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
nadvlády	nadvláda	k1gFnSc2	nadvláda
posílila	posílit	k5eAaPmAgFnS	posílit
pouto	pouto	k1gNnSc4	pouto
mezi	mezi	k7c7	mezi
náboženstvím	náboženství	k1gNnSc7	náboženství
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Řeků	Řek	k1gMnPc2	Řek
i	i	k8xC	i
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
stoupenci	stoupenec	k1gMnPc7	stoupenec
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
uctívají	uctívat	k5eAaImIp3nP	uctívat
a	a	k8xC	a
respektují	respektovat	k5eAaImIp3nP	respektovat
ortodoxní	ortodoxní	k2eAgFnSc4d1	ortodoxní
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nS	chodit
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
slaví	slavit	k5eAaImIp3nP	slavit
hlavní	hlavní	k2eAgInPc4d1	hlavní
svátky	svátek	k1gInPc4	svátek
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
citově	citově	k6eAd1	citově
spjati	spjat	k2eAgMnPc1d1	spjat
s	s	k7c7	s
ortodoxním	ortodoxní	k2eAgNnSc7d1	ortodoxní
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
jako	jako	k8xC	jako
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
národní	národní	k2eAgFnSc7d1	národní
vírou	víra	k1gFnSc7	víra
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
ústava	ústava	k1gFnSc1	ústava
odráží	odrážet	k5eAaImIp3nS	odrážet
tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
absolutní	absolutní	k2eAgFnSc4d1	absolutní
svobodu	svoboda	k1gFnSc4	svoboda
vyznání	vyznání	k1gNnPc2	vyznání
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
definuje	definovat	k5eAaBmIp3nS	definovat
"	"	kIx"	"
<g/>
převládající	převládající	k2eAgNnSc4d1	převládající
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
"	"	kIx"	"
Řecka	Řecko	k1gNnSc2	Řecko
jako	jako	k9	jako
východní	východní	k2eAgFnSc4d1	východní
ortodoxní	ortodoxní	k2eAgFnSc4d1	ortodoxní
církev	církev	k1gFnSc4	církev
Ježíšovu	Ježíšův	k2eAgFnSc4d1	Ježíšova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jsou	být	k5eAaImIp3nP	být
ortodoxní	ortodoxní	k2eAgFnSc4d1	ortodoxní
církev	církev	k1gFnSc4	církev
a	a	k8xC	a
světský	světský	k2eAgInSc4d1	světský
stav	stav	k1gInSc4	stav
vzájemně	vzájemně	k6eAd1	vzájemně
blízce	blízce	k6eAd1	blízce
spjati	spjat	k2eAgMnPc1d1	spjat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
společný	společný	k2eAgInSc1d1	společný
souhlas	souhlas	k1gInSc1	souhlas
je	být	k5eAaImIp3nS	být
potřebný	potřebný	k2eAgInSc1d1	potřebný
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
také	také	k9	také
zablokovala	zablokovat	k5eAaPmAgFnS	zablokovat
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
stavbu	stavba	k1gFnSc4	stavba
míst	místo	k1gNnPc2	místo
uctívání	uctívání	k1gNnSc3	uctívání
jiných	jiný	k2eAgFnPc2d1	jiná
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
přijímají	přijímat	k5eAaImIp3nP	přijímat
plat	plat	k1gInSc4	plat
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
přísahá	přísahat	k5eAaImIp3nS	přísahat
na	na	k7c4	na
Bibli	bible	k1gFnSc4	bible
svatou	svatý	k2eAgFnSc4d1	svatá
a	a	k8xC	a
ortodoxnímu	ortodoxní	k2eAgNnSc3d1	ortodoxní
křesťanství	křesťanství	k1gNnSc3	křesťanství
je	být	k5eAaImIp3nS	být
dáváno	dáván	k2eAgNnSc4d1	dáváno
přednostní	přednostní	k2eAgNnSc4d1	přednostní
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
náboženských	náboženský	k2eAgInPc6d1	náboženský
předmětech	předmět	k1gInPc6	předmět
v	v	k7c6	v
prvotním	prvotní	k2eAgNnSc6d1	prvotní
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Církvi	církev	k1gFnSc3	církev
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
umožněno	umožněn	k2eAgNnSc1d1	umožněno
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
portfolio	portfolio	k1gNnSc4	portfolio
finančního	finanční	k2eAgInSc2d1	finanční
majetku	majetek	k1gInSc2	majetek
vyňatého	vyňatý	k2eAgInSc2d1	vyňatý
z	z	k7c2	z
daňové	daňový	k2eAgFnSc2d1	daňová
a	a	k8xC	a
rozpočtové	rozpočtový	k2eAgFnSc2d1	rozpočtová
evidence	evidence	k1gFnSc2	evidence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
vyšla	vyjít	k5eAaPmAgFnS	vyjít
najevo	najevo	k6eAd1	najevo
série	série	k1gFnSc1	série
korupčních	korupční	k2eAgInPc2d1	korupční
skandálů	skandál	k1gInPc2	skandál
zahrnující	zahrnující	k2eAgInPc1d1	zahrnující
vysoce	vysoce	k6eAd1	vysoce
postavené	postavený	k2eAgMnPc4d1	postavený
představitele	představitel	k1gMnPc4	představitel
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
,	,	kIx,	,
i	i	k8xC	i
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nevěřících	věřící	k2eNgMnPc2d1	nevěřící
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
na	na	k7c6	na
dokončení	dokončení	k1gNnSc6	dokončení
oddělení	oddělení	k1gNnSc2	oddělení
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
státu	stát	k1gInSc2	stát
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
kontrolu	kontrola	k1gFnSc4	kontrola
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Řeků	Řek	k1gMnPc2	Řek
(	(	kIx(	(
<g/>
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
98	[number]	k4	98
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
alespoň	alespoň	k9	alespoň
formálními	formální	k2eAgInPc7d1	formální
členy	člen	k1gInPc7	člen
východní	východní	k2eAgFnSc2d1	východní
ortodoxní	ortodoxní	k2eAgFnSc2d1	ortodoxní
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zachovávání	zachovávání	k1gNnSc1	zachovávání
náboženských	náboženský	k2eAgFnPc2d1	náboženská
tradic	tradice	k1gFnPc2	tradice
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Řečtí	řecký	k2eAgMnPc1d1	řecký
muslimové	muslim	k1gMnPc1	muslim
tvoří	tvořit	k5eAaImIp3nP	tvořit
okolo	okolo	k7c2	okolo
1,3	[number]	k4	1,3
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Thrákii	Thrákie	k1gFnSc6	Thrákie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
příslušníci	příslušník	k1gMnPc1	příslušník
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Patra	patro	k1gNnSc2	patro
<g/>
,	,	kIx,	,
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Kyklad	Kyklady	k1gFnPc2	Kyklady
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Syros	Syros	k1gInSc1	Syros
<g/>
,	,	kIx,	,
Paros	Paros	k1gInSc1	Paros
a	a	k8xC	a
Naxos	Naxos	k1gInSc1	Naxos
<g/>
,	,	kIx,	,
protestanti	protestant	k1gMnPc1	protestant
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
bylo	být	k5eAaImAgNnS	být
kdysi	kdysi	k6eAd1	kdysi
důležitým	důležitý	k2eAgNnSc7d1	důležité
židovským	židovský	k2eAgNnSc7d1	Židovské
městem	město	k1gNnSc7	město
až	až	k9	až
do	do	k7c2	do
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Určité	určitý	k2eAgFnPc1d1	určitá
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
se	se	k3xPyFc4	se
také	také	k9	také
snažily	snažit	k5eAaImAgFnP	snažit
oživit	oživit	k5eAaPmF	oživit
staré	starý	k2eAgNnSc1d1	staré
řecké	řecký	k2eAgNnSc1d1	řecké
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
poloostrov	poloostrov	k1gInSc1	poloostrov
Athos	Athos	k1gInSc1	Athos
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
též	též	k9	též
uvádí	uvádět	k5eAaImIp3nS	uvádět
jen	jen	k9	jen
hora	hora	k1gFnSc1	hora
Athos	Athos	k1gMnSc1	Athos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
řeckou	řecký	k2eAgFnSc7d1	řecká
ústavou	ústava	k1gFnSc7	ústava
za	za	k7c4	za
autonomní	autonomní	k2eAgFnSc4d1	autonomní
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
přesto	přesto	k8xC	přesto
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
výsadou	výsada	k1gFnSc7	výsada
řeckého	řecký	k2eAgInSc2d1	řecký
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Duchovně	duchovně	k6eAd1	duchovně
je	být	k5eAaImIp3nS	být
Athos	Athos	k1gInSc1	Athos
pod	pod	k7c7	pod
patriarchátem	patriarchát	k1gInSc7	patriarchát
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
ve	v	k7c4	v
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
kláštery	klášter	k1gInPc7	klášter
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Athos	Athos	k1gInSc4	Athos
a	a	k8xC	a
s	s	k7c7	s
ortodoxní	ortodoxní	k2eAgFnSc7d1	ortodoxní
církví	církev	k1gFnSc7	církev
zastoupenou	zastoupený	k2eAgFnSc7d1	zastoupená
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
klášter	klášter	k1gInSc1	klášter
se	se	k3xPyFc4	se
nedávno	nedávno	k6eAd1	nedávno
odloučil	odloučit	k5eAaPmAgInS	odloučit
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
zcela	zcela	k6eAd1	zcela
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
schisma	schisma	k1gNnSc4	schisma
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
hoře	hora	k1gFnSc6	hora
–	–	k?	–
Klášter	klášter	k1gInSc4	klášter
Esphygmenou	Esphygmený	k2eAgFnSc7d1	Esphygmený
<g/>
.	.	kIx.	.
</s>
<s>
Esphygmenou	Esphygmený	k2eAgFnSc7d1	Esphygmený
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
117	[number]	k4	117
mnichů	mnich	k1gMnPc2	mnich
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
oponují	oponovat	k5eAaImIp3nP	oponovat
hlavě	hlava	k1gFnSc6	hlava
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
už	už	k6eAd1	už
ji	on	k3xPp3gFnSc4	on
více	hodně	k6eAd2	hodně
neuznávají	uznávat	k5eNaImIp3nP	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
posledními	poslední	k2eAgMnPc7d1	poslední
zbylými	zbylý	k2eAgMnPc7d1	zbylý
opravdovými	opravdový	k2eAgMnPc7d1	opravdový
křesťany	křesťan	k1gMnPc7	křesťan
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
ortodoxní	ortodoxní	k2eAgMnPc1d1	ortodoxní
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
zkaženými	zkažený	k2eAgInPc7d1	zkažený
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vedou	vést	k5eAaImIp3nP	vést
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
vírami	víra	k1gFnPc7	víra
dialog	dialog	k1gInSc1	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mají	mít	k5eAaImIp3nP	mít
výhrady	výhrada	k1gFnPc4	výhrada
vůči	vůči	k7c3	vůči
patriarchovi	patriarcha	k1gMnSc3	patriarcha
Athenagorasovi	Athenagoras	k1gMnSc3	Athenagoras
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
nevoli	nevole	k1gFnSc4	nevole
k	k	k7c3	k
římsko-katolické	římskoatolický	k2eAgFnSc3d1	římsko-katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
moderní	moderní	k2eAgFnSc2d1	moderní
evropské	evropský	k2eAgFnSc2d1	Evropská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
poznatky	poznatek	k1gInPc1	poznatek
byly	být	k5eAaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
již	již	k6eAd1	již
starým	starý	k2eAgMnPc3d1	starý
Řekům	Řek	k1gMnPc3	Řek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
založili	založit	k5eAaPmAgMnP	založit
např.	např.	kA	např.
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
astronomii	astronomie	k1gFnSc4	astronomie
<g/>
,	,	kIx,	,
rétoriku	rétorika	k1gFnSc4	rétorika
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc4d1	moderní
medicínu	medicína	k1gFnSc4	medicína
či	či	k8xC	či
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
je	být	k5eAaImIp3nS	být
i	i	k9	i
vliv	vliv	k1gInSc4	vliv
řeckého	řecký	k2eAgInSc2d1	řecký
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
jako	jako	k8xS	jako
ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
,	,	kIx,	,
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
strategie	strategie	k1gFnSc1	strategie
či	či	k8xC	či
aristokracie	aristokracie	k1gFnPc1	aristokracie
jsou	být	k5eAaImIp3nP	být
řeckého	řecký	k2eAgMnSc4d1	řecký
původu	původa	k1gMnSc4	původa
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
prožila	prožít	k5eAaPmAgFnS	prožít
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
velký	velký	k2eAgInSc4d1	velký
rozmach	rozmach	k1gInSc4	rozmach
během	během	k7c2	během
byzantské	byzantský	k2eAgFnSc2d1	byzantská
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c4	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
pak	pak	k8xC	pak
dalšímu	další	k2eAgInSc3d1	další
velkému	velký	k2eAgInSc3d1	velký
rozvoji	rozvoj	k1gInSc3	rozvoj
bránila	bránit	k5eAaImAgFnS	bránit
osmanská	osmanský	k2eAgFnSc1d1	Osmanská
nadvláda	nadvláda	k1gFnSc1	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
nejkulturnější	kulturní	k2eAgNnPc4d3	nejkulturnější
evropská	evropský	k2eAgNnPc4d1	Evropské
centra	centrum	k1gNnPc4	centrum
i	i	k8xC	i
Kréta	Kréta	k1gFnSc1	Kréta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vládli	vládnout	k5eAaImAgMnP	vládnout
Benátčané	Benátčan	k1gMnPc1	Benátčan
<g/>
.	.	kIx.	.
</s>
<s>
Vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
kultura	kultura	k1gFnSc1	kultura
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
i	i	k9	i
na	na	k7c6	na
Jónských	jónský	k2eAgInPc6d1	jónský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Turci	Turek	k1gMnPc1	Turek
nikdy	nikdy	k6eAd1	nikdy
nevládli	vládnout	k5eNaImAgMnP	vládnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Anatolii	Anatolie	k1gFnSc6	Anatolie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sice	sice	k8xC	sice
patřila	patřit	k5eAaImAgFnS	patřit
Turkům	Turek	k1gMnPc3	Turek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
vyspělým	vyspělý	k2eAgNnPc3d1	vyspělé
velkoměstům	velkoměsto	k1gNnPc3	velkoměsto
si	se	k3xPyFc3	se
Maloasijští	maloasijský	k2eAgMnPc1d1	maloasijský
Řekové	Řek	k1gMnPc1	Řek
dokázali	dokázat	k5eAaPmAgMnP	dokázat
udržet	udržet	k5eAaPmF	udržet
moderní	moderní	k2eAgFnSc4d1	moderní
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Obrození	obrození	k1gNnSc1	obrození
nastává	nastávat	k5eAaImIp3nS	nastávat
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Kultura	kultura	k1gFnSc1	kultura
současného	současný	k2eAgNnSc2d1	současné
Řecka	Řecko	k1gNnSc2	Řecko
vychází	vycházet	k5eAaImIp3nS	vycházet
především	především	k9	především
z	z	k7c2	z
byzantských	byzantský	k2eAgFnPc2d1	byzantská
a	a	k8xC	a
ze	z	k7c2	z
starověkých	starověký	k2eAgFnPc2d1	starověká
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
aspektem	aspekt	k1gInSc7	aspekt
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
řecká	řecký	k2eAgFnSc1d1	řecká
ortodoxní	ortodoxní	k2eAgFnSc1d1	ortodoxní
byzantská	byzantský	k2eAgFnSc1d1	byzantská
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
většina	většina	k1gFnSc1	většina
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
má	mít	k5eAaImIp3nS	mít
řecká	řecký	k2eAgFnSc1d1	řecká
kultura	kultura	k1gFnSc1	kultura
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
orientálním	orientální	k2eAgFnPc3d1	orientální
zemím	zem	k1gFnPc3	zem
než	než	k8xS	než
k	k	k7c3	k
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
tak	tak	k6eAd1	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
jakousi	jakýsi	k3yIgFnSc4	jakýsi
vstupní	vstupní	k2eAgFnSc4d1	vstupní
bránu	brána	k1gFnSc4	brána
mezi	mezi	k7c7	mezi
západem	západ	k1gInSc7	západ
a	a	k8xC	a
orientem	orient	k1gInSc7	orient
<g/>
.	.	kIx.	.
</s>
<s>
Orientální	orientální	k2eAgInPc1d1	orientální
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
kultuře	kultura	k1gFnSc6	kultura
patrné	patrný	k2eAgFnPc4d1	patrná
už	už	k9	už
od	od	k7c2	od
mykénské	mykénský	k2eAgFnSc2d1	mykénská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
jsou	být	k5eAaImIp3nP	být
národ	národ	k1gInSc4	národ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
baví	bavit	k5eAaImIp3nS	bavit
při	při	k7c6	při
dobré	dobrý	k2eAgFnSc6d1	dobrá
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
tanci	tanec	k1gInSc6	tanec
<g/>
,	,	kIx,	,
jídle	jídlo	k1gNnSc6	jídlo
a	a	k8xC	a
víně	víno	k1gNnSc6	víno
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Řeky	Řek	k1gMnPc4	Řek
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
částí	část	k1gFnPc2	část
jejich	jejich	k3xOp3gInSc2	jejich
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
žánrem	žánr	k1gInSc7	žánr
je	být	k5eAaImIp3nS	být
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vycházející	vycházející	k2eAgFnSc2d1	vycházející
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
jiné	jiný	k2eAgFnSc6d1	jiná
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
není	být	k5eNaImIp3nS	být
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
tradiční	tradiční	k2eAgInPc1d1	tradiční
tance	tanec	k1gInPc1	tanec
tak	tak	k6eAd1	tak
populární	populární	k2eAgInPc1d1	populární
jako	jako	k8xC	jako
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dokonce	dokonce	k9	dokonce
i	i	k9	i
dnes	dnes	k6eAd1	dnes
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgFnPc4d1	nová
lidové	lidový	k2eAgFnPc4d1	lidová
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Řecká	řecký	k2eAgFnSc1d1	řecká
hudba	hudba	k1gFnSc1	hudba
má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
starořecké	starořecký	k2eAgFnSc6d1	starořecká
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
již	již	k6eAd1	již
od	od	k7c2	od
mykénské	mykénský	k2eAgFnSc2d1	mykénská
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nS	patřit
pod	pod	k7c4	pod
orientální	orientální	k2eAgInPc4d1	orientální
styly	styl	k1gInPc4	styl
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
tradičních	tradiční	k2eAgInPc6d1	tradiční
krajích	kraj	k1gInPc6	kraj
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
se	se	k3xPyFc4	se
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
pevninského	pevninský	k2eAgNnSc2d1	pevninské
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
Kréty	Kréta	k1gFnSc2	Kréta
<g/>
,	,	kIx,	,
Jónských	jónský	k2eAgInPc2d1	jónský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
Egejských	egejský	k2eAgInPc2d1	egejský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
z	z	k7c2	z
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
obývané	obývaný	k2eAgMnPc4d1	obývaný
Řeky	Řek	k1gMnPc4	Řek
do	do	k7c2	do
řecko-turecké	řeckourecký	k2eAgFnSc2d1	řecko-turecká
výměny	výměna	k1gFnSc2	výměna
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
r.	r.	kA	r.
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
Pontských	pontský	k2eAgMnPc2d1	pontský
<g/>
,	,	kIx,	,
maloasijských	maloasijský	k2eAgMnPc2d1	maloasijský
a	a	k8xC	a
Kapadóckých	Kapadócký	k2eAgMnPc2d1	Kapadócký
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Pevninská	pevninský	k2eAgFnSc1d1	pevninská
hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
Makedonie	Makedonie	k1gFnSc2	Makedonie
a	a	k8xC	a
trakt	trakt	k1gInSc1	trakt
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
ostatní	ostatní	k2eAgFnSc3d1	ostatní
balkánské	balkánský	k2eAgFnSc3d1	balkánská
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
Epiru	Epir	k1gInSc2	Epir
<g/>
,	,	kIx,	,
Thesálie	Thesálie	k1gFnSc2	Thesálie
<g/>
,	,	kIx,	,
Rumélie	Rumélie	k1gFnSc2	Rumélie
a	a	k8xC	a
Peloponésu	Peloponés	k1gInSc2	Peloponés
(	(	kIx(	(
<g/>
dimotiki	dimotiki	k1gNnSc1	dimotiki
Musik	musika	k1gFnPc2	musika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
specifická	specifický	k2eAgFnSc1d1	specifická
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
celořecká	celořecký	k2eAgFnSc1d1	celořecký
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
nástroje	nástroj	k1gInPc1	nástroj
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
dechové	dechový	k2eAgNnSc4d1	dechové
–	–	k?	–
klarinet	klarinet	k1gInSc4	klarinet
<g/>
,	,	kIx,	,
zurna	zurna	k1gFnSc1	zurna
a	a	k8xC	a
flojera	flojera	k1gFnSc1	flojera
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
zní	znět	k5eAaImIp3nS	znět
brnkací	brnkace	k1gFnPc2	brnkace
udi	udi	k?	udi
či	či	k8xC	či
laut	laut	k1gInSc1	laut
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
novořeckých	novořecký	k2eAgInPc2d1	novořecký
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
Sarakacanů	Sarakacan	k1gInPc2	Sarakacan
a	a	k8xC	a
Karagunidů	Karagunid	k1gInPc2	Karagunid
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
peloponéského	peloponéský	k2eAgInSc2d1	peloponéský
kraje	kraj	k1gInSc2	kraj
Arkádie	Arkádie	k1gFnSc2	Arkádie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
písních	píseň	k1gFnPc6	píseň
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
i	i	k9	i
hrdinský	hrdinský	k2eAgInSc4d1	hrdinský
boj	boj	k1gInSc4	boj
Řeků	Řek	k1gMnPc2	Řek
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
mužskou	mužský	k2eAgFnSc7d1	mužská
ozdobou	ozdoba	k1gFnSc7	ozdoba
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
suknice	suknice	k1gFnSc1	suknice
fustanella	fustanella	k1gFnSc1	fustanella
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
řeckých	řecký	k2eAgInPc2d1	řecký
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
nisiotika	nisiotika	k1gFnSc1	nisiotika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
dominují	dominovat	k5eAaImIp3nP	dominovat
zde	zde	k6eAd1	zde
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
melodická	melodický	k2eAgFnSc1d1	melodická
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
hudbu	hudba	k1gFnSc4	hudba
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
Maloasijští	maloasijský	k2eAgMnPc1d1	maloasijský
Řekové	Řek	k1gMnPc1	Řek
(	(	kIx(	(
<g/>
mikrasiatika	mikrasiatika	k1gFnSc1	mikrasiatika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
tradiční	tradiční	k2eAgFnSc6d1	tradiční
hudbě	hudba	k1gFnSc6	hudba
převládají	převládat	k5eAaImIp3nP	převládat
převážně	převážně	k6eAd1	převážně
brnkací	brnkací	k2eAgInPc1d1	brnkací
nástroje	nástroj	k1gInPc1	nástroj
(	(	kIx(	(
<g/>
kanonaki	kanonak	k1gFnPc1	kanonak
<g/>
,	,	kIx,	,
udi	udi	k?	udi
<g/>
)	)	kIx)	)
a	a	k8xC	a
housle	housle	k1gFnPc4	housle
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
Pontu	Pont	k1gInSc2	Pont
(	(	kIx(	(
<g/>
pondiaka	pondiak	k1gMnSc2	pondiak
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
hraná	hraný	k2eAgFnSc1d1	hraná
na	na	k7c4	na
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
nástroj	nástroj	k1gInSc4	nástroj
kemence	kemence	k1gFnSc2	kemence
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc1	píseň
jsou	být	k5eAaImIp3nP	být
rychlé	rychlý	k2eAgFnPc1d1	rychlá
v	v	k7c6	v
silném	silný	k2eAgInSc6d1	silný
orientálním	orientální	k2eAgInSc6d1	orientální
tónu	tón	k1gInSc6	tón
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
hudba	hudba	k1gFnSc1	hudba
z	z	k7c2	z
Kréty	Kréta	k1gFnSc2	Kréta
(	(	kIx(	(
<g/>
kritika	kritika	k1gFnSc1	kritika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgInSc7d1	tradiční
nástrojem	nástroj	k1gInSc7	nástroj
smyčcová	smyčcový	k2eAgFnSc1d1	smyčcová
lyra	lyra	k1gFnSc1	lyra
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgFnSc1d1	podobná
jako	jako	k8xC	jako
kemence	kemence	k1gFnSc1	kemence
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
rychlé	rychlý	k2eAgFnPc1d1	rychlá
a	a	k8xC	a
melodické	melodický	k2eAgFnPc1d1	melodická
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
mandináda	mandináda	k1gFnSc1	mandináda
<g/>
,	,	kIx,	,
vtipná	vtipný	k2eAgFnSc1d1	vtipná
rýmovaná	rýmovaný	k2eAgFnSc1d1	rýmovaná
pořekadlová	pořekadlový	k2eAgFnSc1d1	pořekadlový
píseň	píseň	k1gFnSc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Mandinády	Mandináda	k1gFnPc1	Mandináda
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInPc4d1	tradiční
řecké	řecký	k2eAgInPc4d1	řecký
tance	tanec	k1gInPc4	tanec
(	(	kIx(	(
<g/>
choroi	choroi	k1gNnSc2	choroi
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
(	(	kIx(	(
<g/>
kyklos	kyklos	k1gInSc1	kyklos
<g/>
)	)	kIx)	)
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
starověký	starověký	k2eAgInSc4d1	starověký
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
tanec	tanec	k1gInSc1	tanec
syrtos	syrtos	k1gInSc1	syrtos
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
po	po	k7c6	po
Kr.	Kr.	k1gFnSc6	Kr.
Podobným	podobný	k2eAgInSc7d1	podobný
tancem	tanec	k1gInSc7	tanec
je	být	k5eAaImIp3nS	být
i	i	k9	i
kalamatianos	kalamatianosa	k1gFnPc2	kalamatianosa
a	a	k8xC	a
klistos	klistosa	k1gFnPc2	klistosa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nP	tančit
v	v	k7c6	v
pevninském	pevninský	k2eAgNnSc6d1	pevninské
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
tancem	tanec	k1gInSc7	tanec
pevninského	pevninský	k2eAgNnSc2d1	pevninské
Řecka	Řecko	k1gNnSc2	Řecko
je	být	k5eAaImIp3nS	být
akrobatický	akrobatický	k2eAgInSc4d1	akrobatický
tanec	tanec	k1gInSc4	tanec
tsamiko	tsamika	k1gFnSc5	tsamika
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
také	také	k9	také
kleftikos	kleftikos	k1gInSc4	kleftikos
Choros	Chorosa	k1gFnPc2	Chorosa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zbojnický	zbojnický	k2eAgInSc1d1	zbojnický
tanec	tanec	k1gInSc1	tanec
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
Na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
tance	tanec	k1gInSc2	tanec
susta	susto	k1gNnSc2	susto
či	či	k8xC	či
pidichtos	pidichtosa	k1gFnPc2	pidichtosa
<g/>
,	,	kIx,	,
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
divoký	divoký	k2eAgInSc1d1	divoký
tanec	tanec	k1gInSc1	tanec
pendozalis	pendozalis	k1gFnPc2	pendozalis
či	či	k8xC	či
siganos	siganosa	k1gFnPc2	siganosa
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
mužské	mužský	k2eAgInPc1d1	mužský
tance	tanec	k1gInPc1	tanec
bývají	bývat	k5eAaImIp3nP	bývat
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
výstřely	výstřel	k1gInPc1	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
maloasijské	maloasijský	k2eAgInPc1d1	maloasijský
tance	tanec	k1gInPc1	tanec
jsou	být	k5eAaImIp3nP	být
zeimbekiko	zeimbekika	k1gFnSc5	zeimbekika
<g/>
,	,	kIx,	,
<g/>
karsilamas	karsilamas	k1gMnSc1	karsilamas
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
břišní	břišní	k2eAgInSc4d1	břišní
tanec	tanec	k1gInSc4	tanec
tsifteteli	tsiftetet	k5eAaImAgMnP	tsiftetet
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Pontské	pontský	k2eAgInPc4d1	pontský
tance	tanec	k1gInPc4	tanec
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
divoké	divoký	k2eAgFnPc4d1	divoká
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc4d1	známá
jsou	být	k5eAaImIp3nP	být
tik	tik	k1gInSc1	tik
<g/>
,	,	kIx,	,
dipat	dipat	k1gInSc1	dipat
či	či	k8xC	či
trigona	trigona	k1gFnSc1	trigona
<g/>
.	.	kIx.	.
</s>
<s>
Kapadóčtí	Kapadócký	k2eAgMnPc1d1	Kapadócký
Řekové	Řek	k1gMnPc1	Řek
tančí	tančit	k5eAaImIp3nP	tančit
tance	tanec	k1gInPc4	tanec
seite	seiit	k5eAaBmRp2nP	seiit
<g/>
,	,	kIx,	,
es	es	k1gNnPc7	es
vassilis	vassilis	k1gInSc1	vassilis
či	či	k8xC	či
leilalum	leilalum	k1gInSc1	leilalum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výměně	výměna	k1gFnSc6	výměna
obyvatel	obyvatel	k1gMnPc2	obyvatel
mezi	mezi	k7c7	mezi
Řeckem	Řecko	k1gNnSc7	Řecko
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
přinesli	přinést	k5eAaPmAgMnP	přinést
Maloasijští	maloasijský	k2eAgMnPc1d1	maloasijský
Řekové	Řek	k1gMnPc1	Řek
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
svou	svůj	k3xOyFgFnSc4	svůj
tradiční	tradiční	k2eAgFnSc4d1	tradiční
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
zpívali	zpívat	k5eAaImAgMnP	zpívat
ironicky	ironicky	k6eAd1	ironicky
<g/>
,	,	kIx,	,
o	o	k7c6	o
problémech	problém	k1gInPc6	problém
<g/>
,	,	kIx,	,
drogách	droga	k1gFnPc6	droga
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Rembetiko	Rembetika	k1gFnSc5	Rembetika
<g/>
.	.	kIx.	.
</s>
<s>
Hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
v	v	k7c6	v
tradičních	tradiční	k2eAgInPc6d1	tradiční
anatolských	anatolský	k2eAgInPc6d1	anatolský
tónech	tón	k1gInPc6	tón
<g/>
,	,	kIx,	,
převažovaly	převažovat	k5eAaImAgFnP	převažovat
zde	zde	k6eAd1	zde
brnkací	brnkací	k2eAgInPc1d1	brnkací
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
zpopularizoval	zpopularizovat	k5eAaPmAgInS	zpopularizovat
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
nástroje	nástroj	k1gInPc1	nástroj
jako	jako	k8xS	jako
buzuki	buzuki	k1gNnSc1	buzuki
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
nástrojem	nástroj	k1gInSc7	nástroj
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
řecké	řecký	k2eAgFnSc2d1	řecká
hudby	hudba	k1gFnSc2	hudba
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
styl	styl	k1gInSc1	styl
laika	laik	k1gMnSc2	laik
<g/>
,	,	kIx,	,
hraný	hraný	k2eAgInSc1d1	hraný
na	na	k7c6	na
buzuki	buzuk	k1gFnSc6	buzuk
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
tzv.	tzv.	kA	tzv.
písně	píseň	k1gFnPc4	píseň
syrtaki	syrtak	k1gFnPc4	syrtak
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
skládali	skládat	k5eAaImAgMnP	skládat
známí	známý	k2eAgMnPc1d1	známý
skladatelé	skladatel	k1gMnPc1	skladatel
jako	jako	k8xC	jako
Mikis	Mikis	k1gFnSc1	Mikis
Theodorakis	Theodorakis	k1gFnSc1	Theodorakis
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
Řek	Řek	k1gMnSc1	Řek
Zorba	Zorba	k1gMnSc1	Zorba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vassilis	Vassilis	k1gInSc4	Vassilis
Tsitsanis	Tsitsanis	k1gFnPc2	Tsitsanis
<g/>
,	,	kIx,	,
Manos	manosa	k1gFnPc2	manosa
Chatzidakis	Chatzidakis	k1gFnPc2	Chatzidakis
<g/>
,	,	kIx,	,
Stavros	Stavrosa	k1gFnPc2	Stavrosa
Xarchakos	Xarchakosa	k1gFnPc2	Xarchakosa
<g/>
,	,	kIx,	,
Nikos	Nikosa	k1gFnPc2	Nikosa
Gatsos	Gatsosa	k1gFnPc2	Gatsosa
či	či	k8xC	či
Manos	manosa	k1gFnPc2	manosa
Loizos	Loizosa	k1gFnPc2	Loizosa
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
zpěváci	zpěvák	k1gMnPc1	zpěvák
byli	být	k5eAaImAgMnP	být
Jorgos	Jorgos	k1gInSc4	Jorgos
Dalaras	Dalarasa	k1gFnPc2	Dalarasa
<g/>
,	,	kIx,	,
Grigoris	Grigoris	k1gFnSc1	Grigoris
Bithikotsis	Bithikotsis	k1gFnSc1	Bithikotsis
či	či	k8xC	či
Stelios	Stelios	k1gInSc1	Stelios
Kazantzidis	Kazantzidis	k1gFnSc2	Kazantzidis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
tzv.	tzv.	kA	tzv.
synchroni	synchroň	k1gFnSc6	synchroň
laika	laik	k1gMnSc2	laik
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
stylem	styl	k1gInSc7	styl
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
zde	zde	k6eAd1	zde
chybět	chybět	k5eAaImF	chybět
buzuki	buzuke	k1gFnSc4	buzuke
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
populárním	populární	k2eAgInSc7d1	populární
stylem	styl	k1gInSc7	styl
i	i	k8xC	i
tradiční	tradiční	k2eAgInSc4d1	tradiční
břišní	břišní	k2eAgInSc4d1	břišní
tanec	tanec	k1gInSc4	tanec
tsifteteli	tsiftetet	k5eAaImAgMnP	tsiftetet
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
buzuki	buzuk	k1gFnSc2	buzuk
<g/>
.	.	kIx.	.
</s>
<s>
Věnují	věnovat	k5eAaPmIp3nP	věnovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
zpěváci	zpěvák	k1gMnPc1	zpěvák
jako	jako	k9	jako
Tasos	Tasos	k1gMnSc1	Tasos
Bugas	Bugas	k1gMnSc1	Bugas
<g/>
,	,	kIx,	,
Jorgos	Jorgos	k1gMnSc1	Jorgos
Dalaras	Dalaras	k1gMnSc1	Dalaras
či	či	k8xC	či
Notis	Notis	k1gFnSc1	Notis
Sfakianakis	Sfakianakis	k1gFnSc1	Sfakianakis
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
skladateli	skladatel	k1gMnPc7	skladatel
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
proslul	proslout	k5eAaPmAgMnS	proslout
avantgardní	avantgardní	k2eAgMnSc1d1	avantgardní
tvůrce	tvůrce	k1gMnSc1	tvůrce
Iannis	Iannis	k1gFnSc2	Iannis
Xenakis	Xenakis	k1gFnSc2	Xenakis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
interpretů	interpret	k1gMnPc2	interpret
si	se	k3xPyFc3	se
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
slávu	sláva	k1gFnSc4	sláva
vydobyla	vydobýt	k5eAaPmAgFnS	vydobýt
sopranistka	sopranistka	k1gFnSc1	sopranistka
Maria	Maria	k1gFnSc1	Maria
Callasová	Callasová	k1gFnSc1	Callasová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
stopách	stopa	k1gFnPc6	stopa
šla	jít	k5eAaImAgFnS	jít
Agnes	Agnes	k1gInSc4	Agnes
Baltsaová	Baltsaový	k2eAgFnSc1d1	Baltsaový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
Nana	Nanum	k1gNnSc2	Nanum
Mouskouri	Mouskour	k1gFnSc2	Mouskour
<g/>
,	,	kIx,	,
Demis	Demis	k1gFnSc1	Demis
Roussos	Roussos	k1gMnSc1	Roussos
<g/>
,	,	kIx,	,
Melina	Melina	k1gMnSc1	Melina
Mercouri	Mercour	k1gFnSc2	Mercour
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
Vangelis	Vangelis	k1gFnSc2	Vangelis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
pak	pak	k6eAd1	pak
Sakis	Sakis	k1gInSc4	Sakis
Rouvas	Rouvasa	k1gFnPc2	Rouvasa
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ctí	ctít	k5eAaImIp3nP	ctít
své	svůj	k3xOyFgFnPc4	svůj
starobylé	starobylý	k2eAgFnPc4d1	starobylá
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
vycházející	vycházející	k2eAgFnSc1d1	vycházející
ještě	ještě	k9	ještě
z	z	k7c2	z
pohanských	pohanský	k2eAgFnPc2d1	pohanská
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgFnPc7d3	veliký
řeckými	řecký	k2eAgFnPc7d1	řecká
slavnostmi	slavnost	k1gFnPc7	slavnost
jsou	být	k5eAaImIp3nP	být
tradiční	tradiční	k2eAgFnPc4d1	tradiční
vesnické	vesnický	k2eAgFnPc4d1	vesnická
veselice	veselice	k1gFnPc4	veselice
panijiria	panijirium	k1gNnSc2	panijirium
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
různých	různý	k2eAgFnPc2d1	různá
svatých	svatá	k1gFnPc2	svatá
(	(	kIx(	(
<g/>
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
bohů	bůh	k1gMnPc2	bůh
<g/>
)	)	kIx)	)
či	či	k8xC	či
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
dobré	dobrý	k2eAgFnSc2d1	dobrá
úrody	úroda	k1gFnSc2	úroda
(	(	kIx(	(
<g/>
svátek	svátek	k1gInSc1	svátek
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
melounů	meloun	k1gInPc2	meloun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těchto	tento	k3xDgFnPc6	tento
zábavách	zábava	k1gFnPc6	zábava
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
na	na	k7c6	na
hlavním	hlavní	k2eAgNnSc6d1	hlavní
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hoduje	hodovat	k5eAaImIp3nS	hodovat
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnPc1d1	tradiční
oslavy	oslava	k1gFnPc1	oslava
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
svátků	svátek	k1gInPc2	svátek
jako	jako	k8xC	jako
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
a	a	k8xC	a
Vánoce	Vánoce	k1gFnPc1	Vánoce
si	se	k3xPyFc3	se
uchovaly	uchovat	k5eAaPmAgFnP	uchovat
množství	množství	k1gNnSc4	množství
zvyků	zvyk	k1gInPc2	zvyk
z	z	k7c2	z
předkřesťanské	předkřesťanský	k2eAgFnSc2d1	předkřesťanská
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
mytologie	mytologie	k1gFnSc1	mytologie
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
starořecké	starořecký	k2eAgFnSc2d1	starořecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plná	plný	k2eAgFnSc1d1	plná
pohanských	pohanský	k2eAgMnPc2d1	pohanský
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
pověr	pověra	k1gFnPc2	pověra
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
dřevorubci	dřevorubec	k1gMnSc6	dřevorubec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
říční	říční	k2eAgMnSc1d1	říční
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
trestá	trestat	k5eAaImIp3nS	trestat
lidskou	lidský	k2eAgFnSc4d1	lidská
chamtivost	chamtivost	k1gFnSc4	chamtivost
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c4	o
Jannise	Jannise	k1gFnPc4	Jannise
a	a	k8xC	a
Mandritse	Mandritse	k1gFnPc4	Mandritse
<g/>
,	,	kIx,	,
o	o	k7c6	o
nerozlučné	rozlučný	k2eNgFnSc6d1	nerozlučná
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
řeckých	řecký	k2eAgInPc2d1	řecký
mýtů	mýtus	k1gInPc2	mýtus
o	o	k7c6	o
Filemonovi	Filemon	k1gMnSc6	Filemon
a	a	k8xC	a
Baucis	Baucis	k1gFnSc6	Baucis
či	či	k8xC	či
o	o	k7c6	o
králi	král	k1gMnSc6	král
Midasovi	Midas	k1gMnSc6	Midas
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
svátkem	svátek	k1gInSc7	svátek
je	být	k5eAaImIp3nS	být
i	i	k9	i
svatba	svatba	k1gFnSc1	svatba
(	(	kIx(	(
<g/>
gamos	gamos	k1gInSc1	gamos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
není	být	k5eNaImIp3nS	být
výjimečné	výjimečný	k2eAgNnSc1d1	výjimečné
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
až	až	k9	až
1000	[number]	k4	1000
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Řeků	Řek	k1gMnPc2	Řek
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
drží	držet	k5eAaImIp3nS	držet
spolu	spolu	k6eAd1	spolu
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nerozlučitelná	rozlučitelný	k2eNgFnSc1d1	nerozlučitelná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
děti	dítě	k1gFnPc1	dítě
žijí	žít	k5eAaImIp3nP	žít
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
i	i	k9	i
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Řeky	Řek	k1gMnPc4	Řek
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
pohostinnost	pohostinnost	k1gFnSc1	pohostinnost
(	(	kIx(	(
<g/>
filoxenia	filoxenium	k1gNnSc2	filoxenium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
literatura	literatura	k1gFnSc1	literatura
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
mýtografem	mýtograf	k1gMnSc7	mýtograf
Homérem	Homér	k1gMnSc7	Homér
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
poté	poté	k6eAd1	poté
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
epos	epos	k1gInSc4	epos
i	i	k8xC	i
drama	drama	k1gNnSc4	drama
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
komedii	komedie	k1gFnSc4	komedie
a	a	k8xC	a
tragédii	tragédie	k1gFnSc4	tragédie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
starověcí	starověký	k2eAgMnPc1d1	starověký
spisovatelé	spisovatel	k1gMnPc1	spisovatel
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
autoři	autor	k1gMnPc1	autor
tragédií	tragédie	k1gFnSc7	tragédie
Sofoklés	Sofoklés	k1gInSc1	Sofoklés
<g/>
,	,	kIx,	,
Eurípidés	Eurípidés	k1gInSc1	Eurípidés
a	a	k8xC	a
Aischylos	Aischylos	k1gMnSc1	Aischylos
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
komedií	komedie	k1gFnPc2	komedie
Aristofanés	Aristofanés	k1gInSc1	Aristofanés
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
biografií	biografie	k1gFnPc2	biografie
Plútarchos	Plútarchos	k1gMnSc1	Plútarchos
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
Hésiodos	Hésiodos	k1gMnSc1	Hésiodos
<g/>
,	,	kIx,	,
básnířka	básnířka	k1gFnSc1	básnířka
Sapfó	Sapfó	k1gMnSc1	Sapfó
<g/>
,	,	kIx,	,
bajkař	bajkař	k1gMnSc1	bajkař
Ezop	Ezop	k1gMnSc1	Ezop
či	či	k8xC	či
Lúkianos	Lúkianos	k1gMnSc1	Lúkianos
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
..	..	k?	..
Nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
byzantský	byzantský	k2eAgInSc1d1	byzantský
román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
neznámého	známý	k2eNgMnSc2d1	neznámý
autora	autor	k1gMnSc2	autor
<g/>
:	:	kIx,	:
Digenis	Digenis	k1gFnSc1	Digenis
Akritas	Akritasa	k1gFnPc2	Akritasa
<g/>
.	.	kIx.	.
</s>
<s>
Líčí	líčit	k5eAaImIp3nS	líčit
příběh	příběh	k1gInSc1	příběh
byzantského	byzantský	k2eAgMnSc2d1	byzantský
vojáka	voják	k1gMnSc2	voják
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zabije	zabít	k5eAaPmIp3nS	zabít
draka	drak	k1gMnSc4	drak
a	a	k8xC	a
zažívá	zažívat	k5eAaImIp3nS	zažívat
velkou	velký	k2eAgFnSc4d1	velká
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
řecké	řecký	k2eAgFnSc2d1	řecká
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Visentzos	Visentzos	k1gInSc1	Visentzos
Kornaros	Kornarosa	k1gFnPc2	Kornarosa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
napsal	napsat	k5eAaPmAgMnS	napsat
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
renesančních	renesanční	k2eAgNnPc2d1	renesanční
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
Erotokritos	Erotokritos	k1gInSc1	Erotokritos
<g/>
,	,	kIx,	,
milostný	milostný	k2eAgInSc1d1	milostný
román	román	k1gInSc1	román
zasazený	zasazený	k2eAgInSc1d1	zasazený
do	do	k7c2	do
starověkých	starověký	k2eAgFnPc2d1	starověká
Athén	Athéna	k1gFnPc2	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
působili	působit	k5eAaImAgMnP	působit
velikáni	velikán	k1gMnPc1	velikán
řecké	řecký	k2eAgFnSc2d1	řecká
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Dionysios	Dionysios	k1gMnSc1	Dionysios
Solomos	Solomos	k1gMnSc1	Solomos
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
řecké	řecký	k2eAgFnSc2d1	řecká
hymny	hymna	k1gFnSc2	hymna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Konstantinos	Konstantinos	k1gMnSc1	Konstantinos
Kavafis	Kavafis	k1gFnSc1	Kavafis
<g/>
,	,	kIx,	,
Janis	Janis	k1gFnSc1	Janis
Ritsos	Ritsos	k1gMnSc1	Ritsos
<g/>
,	,	kIx,	,
Angelos	Angelos	k1gMnSc1	Angelos
Sikelianos	Sikelianos	k1gMnSc1	Sikelianos
<g/>
,	,	kIx,	,
Kostis	Kostis	k1gFnSc1	Kostis
Palamas	Palamas	k1gMnSc1	Palamas
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
textu	text	k1gInSc2	text
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hymny	hymna	k1gFnSc2	hymna
<g/>
,	,	kIx,	,
a	a	k8xC	a
nositelé	nositel	k1gMnPc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Jorgos	Jorgos	k1gMnSc1	Jorgos
Seferis	Seferis	k1gInSc1	Seferis
a	a	k8xC	a
Odysseas	Odysseas	k1gInSc1	Odysseas
Elytis	Elytis	k1gFnSc2	Elytis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prozaiků	prozaik	k1gMnPc2	prozaik
byli	být	k5eAaImAgMnP	být
známí	známý	k1gMnPc1	známý
Adamantios	Adamantiosa	k1gFnPc2	Adamantiosa
Korais	Korais	k1gFnSc2	Korais
a	a	k8xC	a
Rigas	Rigas	k1gMnSc1	Rigas
Feraios	Feraios	k1gMnSc1	Feraios
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
slavný	slavný	k2eAgMnSc1d1	slavný
Nikos	Nikos	k1gMnSc1	Nikos
Kazantzakis	Kazantzakis	k1gMnSc1	Kazantzakis
<g/>
,	,	kIx,	,
stvořitel	stvořitel	k1gMnSc1	stvořitel
Řeka	Řek	k1gMnSc2	Řek
Zorby	Zorba	k1gMnSc2	Zorba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
francouzským	francouzský	k2eAgInSc7d1	francouzský
kulturním	kulturní	k2eAgInSc7d1	kulturní
prostorem	prostor	k1gInSc7	prostor
splynul	splynout	k5eAaPmAgMnS	splynout
básník	básník	k1gMnSc1	básník
Jean	Jean	k1gMnSc1	Jean
Moréas	Moréas	k1gMnSc1	Moréas
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prozaiků	prozaik	k1gMnPc2	prozaik
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
například	například	k6eAd1	například
Alexandros	Alexandrosa	k1gFnPc2	Alexandrosa
Papadiamantis	Papadiamantis	k1gFnPc4	Papadiamantis
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
antickými	antický	k2eAgMnPc7d1	antický
sochaři	sochař	k1gMnPc7	sochař
byli	být	k5eAaImAgMnP	být
Feidiás	Feidiása	k1gFnPc2	Feidiása
a	a	k8xC	a
Praxiteles	Praxitelesa	k1gFnPc2	Praxitelesa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
malíř	malíř	k1gMnSc1	malíř
El	Ela	k1gFnPc2	Ela
Greco	Greco	k1gMnSc1	Greco
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Dominikos	Dominikos	k1gMnSc1	Dominikos
Theotokopulos	Theotokopulos	k1gMnSc1	Theotokopulos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
prosadil	prosadit	k5eAaPmAgMnS	prosadit
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
nejen	nejen	k6eAd1	nejen
za	za	k7c2	za
největšího	veliký	k2eAgMnSc2d3	veliký
řeckého	řecký	k2eAgMnSc2d1	řecký
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
i	i	k9	i
nejslavnějším	slavný	k2eAgInPc3d3	nejslavnější
moderním	moderní	k2eAgInPc3d1	moderní
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
neantickým	antický	k2eNgMnSc7d1	antický
a	a	k8xC	a
nebyzantským	byzantský	k2eNgMnSc7d1	byzantský
<g/>
)	)	kIx)	)
Řekem	Řek	k1gMnSc7	Řek
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Nejoceňovanějšími	oceňovaný	k2eAgMnPc7d3	nejoceňovanější
řeckými	řecký	k2eAgMnPc7d1	řecký
filmovými	filmový	k2eAgMnPc7d1	filmový
režiséry	režisér	k1gMnPc7	režisér
jsou	být	k5eAaImIp3nP	být
Theo	Thea	k1gFnSc5	Thea
Angelopoulos	Angelopoulos	k1gMnSc1	Angelopoulos
a	a	k8xC	a
Costa-Gavras	Costa-Gavras	k1gMnSc1	Costa-Gavras
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
se	se	k3xPyFc4	se
akčními	akční	k2eAgInPc7d1	akční
trháky	trhák	k1gInPc7	trhák
prosadil	prosadit	k5eAaPmAgMnS	prosadit
George	Georg	k1gMnSc2	Georg
Pan	Pan	k1gMnSc1	Pan
Cosmatos	Cosmatos	k1gMnSc1	Cosmatos
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Řecká	řecký	k2eAgFnSc1d1	řecká
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
kuchyně	kuchyně	k1gFnSc1	kuchyně
patří	patřit	k5eAaImIp3nS	patřit
pod	pod	k7c4	pod
typickou	typický	k2eAgFnSc4d1	typická
středomořskou	středomořský	k2eAgFnSc4d1	středomořská
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
zdravá	zdravý	k2eAgFnSc1d1	zdravá
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
olivového	olivový	k2eAgInSc2d1	olivový
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
skoro	skoro	k6eAd1	skoro
každého	každý	k3xTgNnSc2	každý
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jídel	jídlo	k1gNnPc2	jídlo
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starověkého	starověký	k2eAgNnSc2d1	starověké
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mnohé	mnohý	k2eAgInPc1d1	mnohý
mají	mít	k5eAaImIp3nP	mít
dnes	dnes	k6eAd1	dnes
turecká	turecký	k2eAgNnPc1d1	turecké
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
tradiční	tradiční	k2eAgInSc1d1	tradiční
medový	medový	k2eAgInSc1d1	medový
zákusek	zákusek	k1gInSc1	zákusek
baklava	baklava	k1gFnSc1	baklava
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
již	již	k6eAd1	již
spisovatel	spisovatel	k1gMnSc1	spisovatel
Athénaios	Athénaios	k1gMnSc1	Athénaios
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
K	k	k7c3	k
tradičním	tradiční	k2eAgNnPc3d1	tradiční
řeckým	řecký	k2eAgNnPc3d1	řecké
jídlům	jídlo	k1gNnPc3	jídlo
patří	patřit	k5eAaImIp3nS	patřit
pečené	pečený	k2eAgNnSc4d1	pečené
maso	maso	k1gNnSc4	maso
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
jehněčí	jehněčí	k2eAgFnSc2d1	jehněčí
a	a	k8xC	a
kozí	kozí	k2eAgFnSc2d1	kozí
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
mořští	mořský	k2eAgMnPc1d1	mořský
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
saláty	salát	k1gInPc1	salát
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
choriatiki-vesnický	choriatikiesnický	k2eAgInSc1d1	choriatiki-vesnický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sýry	sýr	k1gInPc1	sýr
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
feta	feta	k1gFnSc1	feta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mussaka	mussak	k1gMnSc4	mussak
(	(	kIx(	(
<g/>
zapékaný	zapékaný	k2eAgInSc4d1	zapékaný
lilek	lilek	k1gInSc4	lilek
s	s	k7c7	s
rajčatovou	rajčatový	k2eAgFnSc7d1	rajčatová
omáčkou	omáčka	k1gFnSc7	omáčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
suvlaki	suvlake	k1gFnSc4	suvlake
a	a	k8xC	a
gyros	gyros	k1gInSc4	gyros
<g/>
,	,	kIx,	,
pastitsio	pastitsio	k1gNnSc4	pastitsio
<g/>
,	,	kIx,	,
dolmades	dolmades	k1gInSc4	dolmades
(	(	kIx(	(
<g/>
vinné	vinný	k2eAgInPc4d1	vinný
listy	list	k1gInPc4	list
plněné	plněný	k2eAgNnSc4d1	plněné
ragú	ragú	k1gNnSc4	ragú
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
omáček	omáčka	k1gFnPc2	omáčka
na	na	k7c4	na
těstoviny	těstovina	k1gFnPc4	těstovina
a	a	k8xC	a
omáčka	omáčka	k1gFnSc1	omáčka
tzatziki	tzatzik	k1gFnSc2	tzatzik
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgFnPc1d1	řecká
zákusky	zákuska	k1gFnPc1	zákuska
jsou	být	k5eAaImIp3nP	být
medové	medový	k2eAgFnPc1d1	medová
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
sladké	sladký	k2eAgFnPc1d1	sladká
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
baklava	baklava	k1gFnSc1	baklava
<g/>
.	.	kIx.	.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1	řecká
kuchyně	kuchyně	k1gFnSc1	kuchyně
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
italskou	italský	k2eAgFnSc7d1	italská
kuchyní	kuchyně	k1gFnSc7	kuchyně
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
pocházejí	pocházet	k5eAaImIp3nP	pocházet
jídla	jídlo	k1gNnPc1	jídlo
jako	jako	k8xC	jako
pastitsio	pastitsio	k1gNnSc1	pastitsio
<g/>
,	,	kIx,	,
těstoviny	těstovina	k1gFnPc1	těstovina
či	či	k8xC	či
stifádo	stifáda	k1gFnSc5	stifáda
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
je	být	k5eAaImIp3nS	být
i	i	k9	i
řecké	řecký	k2eAgNnSc1d1	řecké
víno	víno	k1gNnSc1	víno
a	a	k8xC	a
alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
ouzo	ouzo	k1gNnSc1	ouzo
<g/>
,	,	kIx,	,
retsina	retsina	k1gFnSc1	retsina
a	a	k8xC	a
metaxa	metaxa	k1gFnSc1	metaxa
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
nápojem	nápoj	k1gInSc7	nápoj
je	být	k5eAaImIp3nS	být
i	i	k9	i
tradiční	tradiční	k2eAgFnPc4d1	tradiční
frapé	frapý	k2eAgFnPc4d1	frapý
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Zakladateli	zakladatel	k1gMnPc7	zakladatel
evropské	evropský	k2eAgFnSc2d1	Evropská
exaktní	exaktní	k2eAgFnSc2d1	exaktní
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
oni	onen	k3xDgMnPc1	onen
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
necítili	cítit	k5eNaImAgMnP	cítit
být	být	k5eAaImF	být
ještě	ještě	k9	ještě
nijak	nijak	k6eAd1	nijak
odděleni	oddělen	k2eAgMnPc1d1	oddělen
od	od	k7c2	od
filozofů	filozof	k1gMnPc2	filozof
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
Archimédés	Archimédés	k1gInSc4	Archimédés
<g/>
,	,	kIx,	,
Eukleidés	Eukleidés	k1gInSc1	Eukleidés
<g/>
,	,	kIx,	,
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
<g/>
,	,	kIx,	,
Démokritos	Démokritos	k1gMnSc1	Démokritos
<g/>
,	,	kIx,	,
Hippokratés	Hippokratés	k1gInSc1	Hippokratés
<g/>
,	,	kIx,	,
Klaudios	Klaudios	k1gMnSc1	Klaudios
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
<g/>
,	,	kIx,	,
Thalés	Thalés	k1gInSc1	Thalés
z	z	k7c2	z
Milétu	Milét	k1gInSc2	Milét
<g/>
,	,	kIx,	,
Galén	Galén	k1gInSc1	Galén
<g/>
,	,	kIx,	,
Eratosthenés	Eratosthenés	k1gInSc1	Eratosthenés
z	z	k7c2	z
Kyrény	Kyréna	k1gFnSc2	Kyréna
<g/>
,	,	kIx,	,
Anaximandros	Anaximandrosa	k1gFnPc2	Anaximandrosa
<g/>
,	,	kIx,	,
Hipparchos	Hipparchosa	k1gFnPc2	Hipparchosa
<g/>
,	,	kIx,	,
Hérón	Hérón	k1gInSc1	Hérón
Alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
<g/>
,	,	kIx,	,
Leukippos	Leukippos	k1gInSc1	Leukippos
z	z	k7c2	z
Milétu	Milét	k1gInSc2	Milét
<g/>
,	,	kIx,	,
Theofrastos	Theofrastos	k1gMnSc1	Theofrastos
<g/>
,	,	kIx,	,
Diofantos	Diofantos	k1gMnSc1	Diofantos
<g/>
,	,	kIx,	,
Aristarchos	Aristarchos	k1gMnSc1	Aristarchos
ze	z	k7c2	z
Samu	Samos	k1gInSc2	Samos
<g/>
,	,	kIx,	,
Apollónios	Apollónios	k1gInSc1	Apollónios
z	z	k7c2	z
Pergy	Perga	k1gFnSc2	Perga
<g/>
,	,	kIx,	,
Eudoxos	Eudoxos	k1gInSc4	Eudoxos
z	z	k7c2	z
Knidu	Knid	k1gInSc2	Knid
<g/>
,	,	kIx,	,
Poseidónios	Poseidóniosa	k1gFnPc2	Poseidóniosa
nebo	nebo	k8xC	nebo
Archytas	Archytasa	k1gFnPc2	Archytasa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
Řecku	Řecko	k1gNnSc6	Řecko
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
filozofy	filozof	k1gMnPc7	filozof
byly	být	k5eAaImAgFnP	být
Aristoteles	Aristoteles	k1gMnSc1	Aristoteles
<g/>
,	,	kIx,	,
Platón	Platón	k1gMnSc1	Platón
<g/>
,	,	kIx,	,
Sokratés	Sokratés	k1gInSc1	Sokratés
<g/>
,	,	kIx,	,
Hérakleitos	Hérakleitosa	k1gFnPc2	Hérakleitosa
<g/>
,	,	kIx,	,
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
<g/>
,	,	kIx,	,
Díogenés	Díogenés	k1gInSc1	Díogenés
ze	z	k7c2	z
Sinópé	Sinópý	k2eAgFnSc2d1	Sinópý
<g/>
,	,	kIx,	,
Anaxagorás	Anaxagorás	k1gInSc1	Anaxagorás
<g/>
,	,	kIx,	,
Parmenidés	Parmenidés	k1gInSc1	Parmenidés
<g/>
,	,	kIx,	,
Zénón	Zénón	k1gInSc1	Zénón
z	z	k7c2	z
Eleje	Elea	k1gFnSc2	Elea
<g/>
,	,	kIx,	,
Empedoklés	Empedoklés	k1gInSc1	Empedoklés
<g/>
,	,	kIx,	,
Plótínos	Plótínos	k1gInSc1	Plótínos
<g/>
,	,	kIx,	,
Xenofanés	Xenofanés	k1gInSc1	Xenofanés
<g/>
,	,	kIx,	,
Prótagorás	Prótagorás	k1gInSc1	Prótagorás
z	z	k7c2	z
Abdér	Abdéra	k1gFnPc2	Abdéra
<g/>
,	,	kIx,	,
Zénón	Zénón	k1gInSc1	Zénón
z	z	k7c2	z
Kitia	Kitium	k1gNnSc2	Kitium
<g/>
,	,	kIx,	,
Antisthenés	Antisthenés	k1gInSc1	Antisthenés
<g/>
,	,	kIx,	,
Epiktétos	Epiktétos	k1gInSc1	Epiktétos
<g/>
,	,	kIx,	,
Gorgiás	Gorgiás	k1gInSc1	Gorgiás
z	z	k7c2	z
Leontín	Leontína	k1gFnPc2	Leontína
<g/>
,	,	kIx,	,
Aristippos	Aristippos	k1gInSc1	Aristippos
z	z	k7c2	z
Kyrény	Kyréna	k1gFnSc2	Kyréna
či	či	k8xC	či
Pyrrhón	Pyrrhón	k1gInSc1	Pyrrhón
z	z	k7c2	z
Élidy	Élida	k1gFnSc2	Élida
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
oblast	oblast	k1gFnSc1	oblast
dnešních	dnešní	k2eAgFnPc2d1	dnešní
humanitních	humanitní	k2eAgFnPc2d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
má	mít	k5eAaImIp3nS	mít
své	své	k1gNnSc4	své
otce	otec	k1gMnSc2	otec
zakladatele	zakladatel	k1gMnSc2	zakladatel
na	na	k7c6	na
Peloponésu	Peloponés	k1gInSc6	Peloponés
<g/>
,	,	kIx,	,
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
a	a	k8xC	a
Thúkydidés	Thúkydidésa	k1gFnPc2	Thúkydidésa
jsou	být	k5eAaImIp3nP	být
otci	otec	k1gMnPc7	otec
dějepravy	dějeprava	k1gFnSc2	dějeprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
stopách	stopa	k1gFnPc6	stopa
šel	jít	k5eAaImAgInS	jít
v	v	k7c6	v
římských	římský	k2eAgInPc6d1	římský
časech	čas	k1gInPc6	čas
Polybios	Polybiosa	k1gFnPc2	Polybiosa
<g/>
.	.	kIx.	.
</s>
<s>
Díogenés	Díogenés	k6eAd1	Díogenés
Laertios	Laertios	k1gMnSc1	Laertios
sepsal	sepsat	k5eAaPmAgMnS	sepsat
první	první	k4xOgFnPc4	první
dějiny	dějiny	k1gFnPc4	dějiny
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
teoretických	teoretický	k2eAgMnPc2d1	teoretický
ekonomů	ekonom	k1gMnPc2	ekonom
byl	být	k5eAaImAgMnS	být
Xenofón	Xenofón	k1gMnSc1	Xenofón
<g/>
.	.	kIx.	.
</s>
<s>
Intelektuálními	intelektuální	k2eAgFnPc7d1	intelektuální
oporami	opora	k1gFnPc7	opora
řecko-římské	řecko-římský	k2eAgFnSc2d1	řecko-římská
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
východořímské	východořímský	k2eAgFnSc2d1	Východořímská
či	či	k8xC	či
byzantské	byzantský	k2eAgFnSc2d1	byzantská
civilizace	civilizace	k1gFnSc2	civilizace
byli	být	k5eAaImAgMnP	být
Atanáš	Atanáš	k1gMnSc1	Atanáš
<g/>
,	,	kIx,	,
Basileios	Basileios	k1gMnSc1	Basileios
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
Řehoř	Řehoř	k1gMnSc1	Řehoř
z	z	k7c2	z
Nazianzu	Nazianz	k1gInSc2	Nazianz
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zlatoústý	zlatoústý	k2eAgMnSc1d1	zlatoústý
<g/>
,	,	kIx,	,
Řehoř	Řehoř	k1gMnSc1	Řehoř
z	z	k7c2	z
Nyssy	Nyssa	k1gFnSc2	Nyssa
<g/>
,	,	kIx,	,
Kléméns	Kléméns	k1gInSc1	Kléméns
Alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
<g/>
,	,	kIx,	,
Cassiodorus	Cassiodorus	k1gMnSc1	Cassiodorus
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Malalas	Malalas	k1gMnSc1	Malalas
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Damašku	Damašek	k1gInSc2	Damašek
<g/>
,	,	kIx,	,
Konstantin	Konstantin	k1gMnSc1	Konstantin
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Porfyrogennetos	Porfyrogennetos	k1gInSc1	Porfyrogennetos
<g/>
,	,	kIx,	,
i	i	k9	i
české	český	k2eAgFnPc4d1	Česká
dějiny	dějiny	k1gFnPc4	dějiny
silně	silně	k6eAd1	silně
ovlivňující	ovlivňující	k2eAgMnSc1d1	ovlivňující
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
Prokopios	Prokopios	k1gMnSc1	Prokopios
z	z	k7c2	z
Kaisareie	Kaisareie	k1gFnSc2	Kaisareie
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Psellos	Psellos	k1gMnSc1	Psellos
<g/>
,	,	kIx,	,
Gregorios	Gregorios	k1gMnSc1	Gregorios
Palamas	Palamas	k1gMnSc1	Palamas
<g/>
,	,	kIx,	,
Niketas	Niketas	k1gMnSc1	Niketas
Choniates	Choniates	k1gMnSc1	Choniates
<g/>
,	,	kIx,	,
Ioannes	Ioannes	k1gMnSc1	Ioannes
Zonaras	Zonaras	k1gMnSc1	Zonaras
<g/>
,	,	kIx,	,
Georgios	Georgios	k1gMnSc1	Georgios
Gemistos	Gemistos	k1gMnSc1	Gemistos
Pléthón	Pléthón	k1gMnSc1	Pléthón
<g/>
,	,	kIx,	,
Georgios	Georgios	k1gMnSc1	Georgios
Pachyméres	Pachyméres	k1gMnSc1	Pachyméres
<g/>
,	,	kIx,	,
Georgios	Georgios	k1gMnSc1	Georgios
Akropolites	Akropolites	k1gMnSc1	Akropolites
a	a	k8xC	a
Basilius	Basilius	k1gMnSc1	Basilius
Bessarion	Bessarion	k1gInSc1	Bessarion
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
musel	muset	k5eAaImAgInS	muset
z	z	k7c2	z
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
prchat	prchat	k5eAaImF	prchat
před	před	k7c4	před
Turky	Turek	k1gMnPc4	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
například	například	k6eAd1	například
geograf	geograf	k1gMnSc1	geograf
Yaqut	Yaqut	k1gMnSc1	Yaqut
al-Hamawi	al-Hamawi	k6eAd1	al-Hamawi
ar-Rumi	ar-Ru	k1gFnPc7	ar-Ru
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
původem	původ	k1gInSc7	původ
Řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
novověkých	novověký	k2eAgMnPc2d1	novověký
vědců	vědec	k1gMnPc2	vědec
byli	být	k5eAaImAgMnP	být
matematik	matematik	k1gMnSc1	matematik
Constantin	Constantin	k1gMnSc1	Constantin
Carathéodory	Carathéodor	k1gInPc4	Carathéodor
či	či	k8xC	či
astronom	astronom	k1gMnSc1	astronom
Eugè	Eugè	k1gMnSc1	Eugè
Michel	Michel	k1gMnSc1	Michel
Antoniadi	Antoniad	k1gMnPc1	Antoniad
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
působili	působit	k5eAaImAgMnP	působit
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
věhlasu	věhlas	k1gInSc2	věhlas
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
PAP	PAP	kA	PAP
testu	test	k1gInSc2	test
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
Georgios	Georgios	k1gMnSc1	Georgios
Papanikolaou	Papanikolaa	k1gMnSc7	Papanikolaa
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
novořeckým	novořecký	k2eAgMnSc7d1	novořecký
filozofem	filozof	k1gMnSc7	filozof
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
Cornelius	Cornelius	k1gInSc4	Cornelius
Castoriadis	Castoriadis	k1gFnSc2	Castoriadis
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
vyspělý	vyspělý	k2eAgInSc4d1	vyspělý
systém	systém	k1gInSc4	systém
školské	školský	k2eAgFnSc2d1	školská
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
školského	školský	k2eAgInSc2d1	školský
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
zčásti	zčásti	k6eAd1	zčásti
podobná	podobný	k2eAgFnSc1d1	podobná
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
lidové	lidový	k2eAgFnSc6d1	lidová
škole	škola	k1gFnSc6	škola
(	(	kIx(	(
<g/>
Dimotiko	Dimotika	k1gFnSc5	Dimotika
scholio	scholia	k1gMnSc5	scholia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
střední	střední	k2eAgFnSc7d1	střední
školou	škola	k1gFnSc7	škola
(	(	kIx(	(
<g/>
Gymnasio	Gymnasio	k6eAd1	Gymnasio
<g/>
)	)	kIx)	)
a	a	k8xC	a
středoškolské	středoškolský	k2eAgNnSc1d1	středoškolské
vzdělání	vzdělání	k1gNnSc1	vzdělání
se	se	k3xPyFc4	se
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
na	na	k7c6	na
lyceu	lyceum	k1gNnSc6	lyceum
(	(	kIx(	(
<g/>
Lykie	Lykie	k1gFnSc1	Lykie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Odborné-technické	Odbornéechnický	k2eAgFnSc6d1	Odborné-technický
škole	škola	k1gFnSc6	škola
(	(	kIx(	(
<g/>
Technika	technika	k1gFnSc1	technika
ke	k	k7c3	k
epangelmatika	epangelmatika	k1gFnSc1	epangelmatika
ekpedeftiria	ekpedeftirium	k1gNnSc2	ekpedeftirium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
výukou	výuka	k1gFnSc7	výuka
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
Panepistimio	Panepistimio	k6eAd1	Panepistimio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejkvalitnější	kvalitní	k2eAgMnPc4d3	nejkvalitnější
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
Athénská	athénský	k2eAgFnSc1d1	Athénská
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
Akadimia	Akadimia	k1gFnSc1	Akadimia
Athinon	Athinon	k1gMnSc1	Athinon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
a	a	k8xC	a
Kapodistriasova	Kapodistriasův	k2eAgFnSc1d1	Kapodistriasův
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
(	(	kIx(	(
<g/>
Ethnikon	Ethnikon	k1gInSc4	Ethnikon
ke	k	k7c3	k
Kapodistriakon	Kapodistriakon	k1gInSc4	Kapodistriakon
Panepistimion	Panepistimion	k1gInSc1	Panepistimion
Athinon	Athinon	k1gInSc1	Athinon
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
univerzitou	univerzita	k1gFnSc7	univerzita
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
Balkánu	Balkán	k1gInSc2	Balkán
je	být	k5eAaImIp3nS	být
Aristotelova	Aristotelův	k2eAgFnSc1d1	Aristotelova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
(	(	kIx(	(
<g/>
Aristotelio	Aristotelio	k6eAd1	Aristotelio
Panepistimio	Panepistimio	k1gNnSc1	Panepistimio
Thessalonikis	Thessalonikis	k1gFnSc2	Thessalonikis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slavnými	slavný	k2eAgMnPc7d1	slavný
basketbalisty	basketbalista	k1gMnPc7	basketbalista
byli	být	k5eAaImAgMnP	být
Nikos	Nikos	k1gInSc4	Nikos
Galis	Galis	k1gFnSc2	Galis
či	či	k8xC	či
Theodoros	Theodorosa	k1gFnPc2	Theodorosa
Papaloukas	Papaloukasa	k1gFnPc2	Papaloukasa
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
je	být	k5eAaImIp3nS	být
řecká	řecký	k2eAgFnSc1d1	řecká
vzpěračská	vzpěračský	k2eAgFnSc1d1	vzpěračská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
vzpěrači	vzpěrač	k1gMnPc1	vzpěrač
Pyrros	Pyrrosa	k1gFnPc2	Pyrrosa
Dimas	Dimas	k1gInSc4	Dimas
a	a	k8xC	a
Akakios	Akakios	k1gInSc4	Akakios
Kakiasvilis	Kakiasvilis	k1gFnSc2	Kakiasvilis
mají	mít	k5eAaImIp3nP	mít
tři	tři	k4xCgFnPc4	tři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Demetrius	Demetrius	k1gMnSc1	Demetrius
Vikelas	Vikelas	k1gMnSc1	Vikelas
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
HRADEČNÝ	HRADEČNÝ	kA	HRADEČNÝ
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
883	[number]	k4	883
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
DOSTÁLOVÁ	Dostálová	k1gFnSc1	Dostálová
<g/>
,	,	kIx,	,
Růžena	Růžena	k1gFnSc1	Růžena
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
110	[number]	k4	110
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Řecka	Řecko	k1gNnSc2	Řecko
Řecké	řecký	k2eAgNnSc4d1	řecké
království	království	k1gNnSc4	království
Starověká	starověký	k2eAgFnSc1d1	starověká
Makedonie	Makedonie	k1gFnSc1	Makedonie
Starověké	starověký	k2eAgNnSc4d1	starověké
Řecko	Řecko	k1gNnSc4	Řecko
Řečtí	řecký	k2eAgMnPc1d1	řecký
plukovníci	plukovník	k1gMnPc1	plukovník
Řecká	řecký	k2eAgFnSc1d1	řecká
hudba	hudba	k1gFnSc1	hudba
Řecký	řecký	k2eAgInSc1d1	řecký
folklór	folklór	k1gInSc4	folklór
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Řecko	Řecko	k1gNnSc4	Řecko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Průvodce	průvodka	k1gFnSc3	průvodka
Řecko	Řecko	k1gNnSc1	Řecko
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
Téma	téma	k1gNnSc1	téma
Řecko	Řecko	k1gNnSc4	Řecko
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc1	heslo
Řecko	Řecko	k1gNnSc4	Řecko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kategorie	kategorie	k1gFnSc2	kategorie
Řecko	Řecko	k1gNnSc1	Řecko
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Řecko	Řecko	k1gNnSc1	Řecko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
ČR	ČR	kA	ČR
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Řecko	Řecko	k1gNnSc1	Řecko
na	na	k7c4	na
euroskop	euroskop	k1gInSc4	euroskop
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
českého	český	k2eAgNnSc2d1	české
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Řecko	Řecko	k1gNnSc1	Řecko
v	v	k7c6	v
atlasu	atlas	k1gInSc6	atlas
zemí	zem	k1gFnPc2	zem
časopisu	časopis	k1gInSc2	časopis
Spiegel	Spiegel	k1gMnSc1	Spiegel
Greece	Greece	k1gMnSc1	Greece
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k2eAgFnSc4d1	Burea
of	of	k?	of
European	Europeana	k1gFnPc2	Europeana
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Greece	Greece	k1gFnSc1	Greece
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Greece	Greece	k1gFnSc1	Greece
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Řecko	Řecko	k1gNnSc1	Řecko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
BOWMAN	BOWMAN	kA	BOWMAN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
S	s	k7c7	s
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Greece	Greece	k1gFnSc1	Greece
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
