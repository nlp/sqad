<s>
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
(	(	kIx(	(
<g/>
též	též	k9	též
zvaná	zvaný	k2eAgFnSc1d1	zvaná
paprsková	paprskový	k2eAgFnSc1d1	paprsková
optika	optika	k1gFnSc1	optika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
šíření	šíření	k1gNnSc4	šíření
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
rozměry	rozměr	k1gInPc1	rozměr
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
nevšímá	všímat	k5eNaImIp3nS	všímat
vlnových	vlnový	k2eAgFnPc2d1	vlnová
vlastností	vlastnost	k1gFnPc2	vlastnost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
několika	několik	k4yIc6	několik
principech	princip	k1gInPc6	princip
princip	princip	k1gInSc1	princip
přímočarého	přímočarý	k2eAgNnSc2d1	přímočaré
šíření	šíření	k1gNnSc2	šíření
světla	světlo	k1gNnSc2	světlo
princip	princip	k1gInSc1	princip
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
nezávislosti	nezávislost	k1gFnSc2	nezávislost
paprsků	paprsek	k1gInPc2	paprsek
princip	princip	k1gInSc4	princip
záměnnosti	záměnnost	k1gFnSc2	záměnnost
chodu	chod	k1gInSc2	chod
paprsků	paprsek	k1gInPc2	paprsek
zákon	zákon	k1gInSc4	zákon
odrazu	odraz	k1gInSc2	odraz
zákon	zákon	k1gInSc1	zákon
lomu	lom	k1gInSc2	lom
Tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
posloužila	posloužit	k5eAaPmAgNnP	posloužit
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
Fermatova	Fermatův	k2eAgInSc2d1	Fermatův
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
přímočarého	přímočarý	k2eAgNnSc2d1	přímočaré
šíření	šíření	k1gNnSc2	šíření
světla	světlo	k1gNnSc2	světlo
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
homogenním	homogenní	k2eAgNnSc7d1	homogenní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
překážky	překážka	k1gFnPc4	překážka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgInPc1d1	velký
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
přímočaře	přímočaro	k6eAd1	přímočaro
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
přímočaré	přímočarý	k2eAgNnSc1d1	přímočaré
šíření	šíření	k1gNnSc1	šíření
světla	světlo	k1gNnSc2	světlo
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zavést	zavést	k5eAaPmF	zavést
představu	představa	k1gFnSc4	představa
světelného	světelný	k2eAgInSc2d1	světelný
paprsku	paprsek	k1gInSc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
máme	mít	k5eAaImIp1nP	mít
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
(	(	kIx(	(
<g/>
bodový	bodový	k2eAgInSc4d1	bodový
<g/>
)	)	kIx)	)
zdroj	zdroj	k1gInSc1	zdroj
světla	světlo	k1gNnSc2	světlo
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
v	v	k7c6	v
neprůhledné	průhledný	k2eNgFnSc6d1	neprůhledná
schránce	schránka	k1gFnSc6	schránka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
malý	malý	k2eAgInSc1d1	malý
kruhový	kruhový	k2eAgInSc1d1	kruhový
otvor	otvor	k1gInSc1	otvor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kruhový	kruhový	k2eAgInSc4d1	kruhový
otvor	otvor	k1gInSc4	otvor
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
ohybovým	ohybový	k2eAgInPc3d1	ohybový
jevům	jev	k1gInPc3	jev
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
světlo	světlo	k1gNnSc1	světlo
může	moct	k5eAaImIp3nS	moct
unikat	unikat	k5eAaImF	unikat
ze	z	k7c2	z
schránky	schránka	k1gFnSc2	schránka
pouze	pouze	k6eAd1	pouze
kruhovým	kruhový	k2eAgInSc7d1	kruhový
otvorem	otvor	k1gInSc7	otvor
a	a	k8xC	a
šíří	šíř	k1gFnSc7	šíř
se	s	k7c7	s
vnějším	vnější	k2eAgNnSc7d1	vnější
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
světelný	světelný	k2eAgInSc4d1	světelný
kužel	kužel	k1gInSc4	kužel
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Průřez	průřez	k1gInSc1	průřez
kužele	kužel	k1gInSc2	kužel
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
velikostí	velikost	k1gFnSc7	velikost
kruhového	kruhový	k2eAgInSc2d1	kruhový
otvoru	otvor	k1gInSc2	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Světelný	světelný	k2eAgInSc1d1	světelný
kužel	kužel	k1gInSc1	kužel
lze	lze	k6eAd1	lze
dobře	dobře	k6eAd1	dobře
spatřit	spatřit	k5eAaPmF	spatřit
na	na	k7c6	na
stínítku	stínítko	k1gNnSc3	stínítko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
umístíme	umístit	k5eAaPmIp1nP	umístit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
světelný	světelný	k2eAgInSc4d1	světelný
kužel	kužel	k1gInSc4	kužel
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
v	v	k7c6	v
mírně	mírně	k6eAd1	mírně
znečistěném	znečistěný	k2eAgNnSc6d1	znečistěné
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
částečkami	částečka	k1gFnPc7	částečka
prachu	prach	k1gInSc2	prach
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
zviditelnění	zviditelnění	k1gNnSc3	zviditelnění
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozptylu	rozptyl	k1gInSc2	rozptyl
na	na	k7c6	na
znečišťujících	znečišťující	k2eAgFnPc6d1	znečišťující
částicích	částice	k1gFnPc6	částice
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
světelný	světelný	k2eAgInSc1d1	světelný
kužel	kužel	k1gInSc1	kužel
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
svazkem	svazek	k1gInSc7	svazek
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
paprsků	paprsek	k1gInPc2	paprsek
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
jen	jen	k9	jen
některými	některý	k3yIgInPc7	některý
jejími	její	k3xOp3gInPc7	její
význačnými	význačný	k2eAgInPc7d1	význačný
paprsky	paprsek	k1gInPc7	paprsek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
svazek	svazek	k1gInSc1	svazek
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
např.	např.	kA	např.
o	o	k7c4	o
paprsek	paprsek	k1gInSc4	paprsek
probíhající	probíhající	k2eAgInSc4d1	probíhající
středem	středem	k7c2	středem
svazku	svazek	k1gInSc2	svazek
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
na	na	k7c6	na
nějaké	nějaký	k3yIgFnSc6	nějaký
hraně	hrana	k1gFnSc6	hrana
svazku	svazek	k1gInSc2	svazek
apod.	apod.	kA	apod.
Paraxiálním	Paraxiální	k2eAgInSc7d1	Paraxiální
(	(	kIx(	(
<g/>
nulovým	nulový	k2eAgInSc7d1	nulový
<g/>
)	)	kIx)	)
paprskem	paprsek	k1gInSc7	paprsek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
takový	takový	k3xDgInSc1	takový
paprsek	paprsek	k1gInSc1	paprsek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
optické	optický	k2eAgFnSc2d1	optická
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
svírá	svírat	k5eAaImIp3nS	svírat
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
úhel	úhel	k1gInSc4	úhel
(	(	kIx(	(
<g/>
menší	malý	k2eAgInPc4d2	menší
než	než	k8xS	než
2	[number]	k4	2
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
nezávislosti	nezávislost	k1gFnSc2	nezávislost
paprsků	paprsek	k1gInPc2	paprsek
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
platný	platný	k2eAgInSc4d1	platný
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nepřihlížíme	přihlížet	k5eNaImIp1nP	přihlížet
k	k	k7c3	k
ohybovým	ohybový	k2eAgInPc3d1	ohybový
jevům	jev	k1gInPc3	jev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
ovlivňování	ovlivňování	k1gNnSc3	ovlivňování
paprsků	paprsek	k1gInPc2	paprsek
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
interference	interference	k1gFnSc1	interference
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
paprsky	paprsek	k1gInPc1	paprsek
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
nebo	nebo	k8xC	nebo
různých	různý	k2eAgInPc2d1	různý
zdrojů	zdroj	k1gInPc2	zdroj
postupují	postupovat	k5eAaImIp3nP	postupovat
prostorem	prostor	k1gInSc7	prostor
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
ostatní	ostatní	k2eAgInPc1d1	ostatní
paprsky	paprsek	k1gInPc1	paprsek
neexistovaly	existovat	k5eNaImAgInP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
zastínění	zastínění	k1gNnSc3	zastínění
svazku	svazek	k1gInSc2	svazek
světelných	světelný	k2eAgInPc2d1	světelný
paprsků	paprsek	k1gInPc2	paprsek
clonou	clona	k1gFnSc7	clona
<g/>
,	,	kIx,	,
postupují	postupovat	k5eAaImIp3nP	postupovat
paprsky	paprsek	k1gInPc1	paprsek
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
nezastíněném	zastíněný	k2eNgNnSc6d1	nezastíněné
clonou	clona	k1gFnSc7	clona
dále	daleko	k6eAd2	daleko
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
vlivu	vliv	k1gInSc2	vliv
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
clonou	clona	k1gFnSc7	clona
odstíněny	odstíněn	k2eAgFnPc1d1	odstíněn
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
také	také	k9	také
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
protínání	protínání	k1gNnSc3	protínání
paprsků	paprsek	k1gInPc2	paprsek
z	z	k7c2	z
více	hodně	k6eAd2	hodně
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
záměnnosti	záměnnost	k1gFnSc2	záměnnost
chodu	chod	k1gInSc2	chod
paprsků	paprsek	k1gInPc2	paprsek
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
paprsek	paprsek	k1gInSc1	paprsek
šíří	šířit	k5eAaImIp3nS	šířit
z	z	k7c2	z
bodu	bod	k1gInSc2	bod
A	a	k8xC	a
do	do	k7c2	do
bodu	bod	k1gInSc2	bod
B	B	kA	B
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
šířit	šířit	k5eAaImF	šířit
také	také	k9	také
z	z	k7c2	z
bodu	bod	k1gInSc2	bod
B	B	kA	B
do	do	k7c2	do
bodu	bod	k1gInSc2	bod
A.	A.	kA	A.
Tento	tento	k3xDgInSc4	tento
princip	princip	k1gInSc4	princip
je	být	k5eAaImIp3nS	být
platný	platný	k2eAgMnSc1d1	platný
i	i	k8xC	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
odrazu	odraz	k1gInSc2	odraz
nebo	nebo	k8xC	nebo
lomu	lom	k1gInSc2	lom
paprsku	paprsek	k1gInSc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Změníme	změnit	k5eAaPmIp1nP	změnit
<g/>
-li	i	k?	-li
tedy	tedy	k9	tedy
směr	směr	k1gInSc4	směr
libovolného	libovolný	k2eAgInSc2d1	libovolný
paprsku	paprsek	k1gInSc2	paprsek
na	na	k7c4	na
opačný	opačný	k2eAgInSc4d1	opačný
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
paprsek	paprsek	k1gInSc1	paprsek
postupovat	postupovat	k5eAaImF	postupovat
stejnou	stejný	k2eAgFnSc7d1	stejná
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
optického	optický	k2eAgNnSc2d1	optické
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pomocí	pomocí	k7c2	pomocí
čoček	čočka	k1gFnPc2	čočka
a	a	k8xC	a
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Znalost	znalost	k1gFnSc1	znalost
optického	optický	k2eAgNnSc2d1	optické
zobrazení	zobrazení	k1gNnSc2	zobrazení
posloužila	posloužit	k5eAaPmAgFnS	posloužit
pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
a	a	k8xC	a
konstrukci	konstrukce	k1gFnSc4	konstrukce
optických	optický	k2eAgInPc2d1	optický
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Optika	optika	k1gFnSc1	optika
Světlo	světlo	k1gNnSc4	světlo
Huygensův	Huygensův	k2eAgInSc1d1	Huygensův
princip	princip	k1gInSc1	princip
Eikonálová	Eikonálový	k2eAgFnSc1d1	Eikonálový
rovnice	rovnice	k1gFnSc1	rovnice
</s>
