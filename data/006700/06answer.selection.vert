<s>
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
optika	optika	k1gFnSc1	optika
(	(	kIx(	(
<g/>
též	též	k9	též
zvaná	zvaný	k2eAgFnSc1d1	zvaná
paprsková	paprskový	k2eAgFnSc1d1	paprsková
optika	optika	k1gFnSc1	optika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
šíření	šíření	k1gNnSc4	šíření
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
rozměry	rozměr	k1gInPc1	rozměr
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
