<s>
Šíleně	šíleně	k6eAd1	šíleně
smutná	smutný	k2eAgFnSc1d1	smutná
princezna	princezna	k1gFnSc1	princezna
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
žánru	žánr	k1gInSc2	žánr
hudební	hudební	k2eAgFnSc2d1	hudební
komedie	komedie	k1gFnSc2	komedie
režiséra	režisér	k1gMnSc2	režisér
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
Zemana	Zeman	k1gMnSc2	Zeman
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
s	s	k7c7	s
Helenou	Helena	k1gFnSc7	Helena
Vondráčkovou	Vondráčková	k1gFnSc7	Vondráčková
a	a	k8xC	a
Václavem	Václav	k1gMnSc7	Václav
Neckářem	Neckář	k1gMnSc7	Neckář
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Neckář	Neckář	k1gMnSc1	Neckář
<g/>
)	)	kIx)	)
jede	jet	k5eAaImIp3nS	jet
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
na	na	k7c4	na
zásnuby	zásnub	k1gInPc4	zásnub
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
"	"	kIx"	"
<g/>
veselého	veselý	k2eAgNnSc2d1	veselé
<g/>
"	"	kIx"	"
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ženit	ženit	k5eAaImF	ženit
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
<g/>
)	)	kIx)	)
jej	on	k3xPp3gMnSc4	on
totiž	totiž	k9	totiž
již	již	k6eAd1	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
sezdat	sezdat	k5eAaPmF	sezdat
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
Helenou	Helena	k1gFnSc7	Helena
(	(	kIx(	(
<g/>
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
míru	mír	k1gInSc2	mír
se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
princ	princ	k1gMnSc1	princ
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jde	jít	k5eAaImIp3nS	jít
odskočit	odskočit	k5eAaPmF	odskočit
<g/>
,	,	kIx,	,
uteče	utéct	k5eAaPmIp3nS	utéct
z	z	k7c2	z
královského	královský	k2eAgInSc2d1	královský
kočáru	kočár	k1gInSc2	kočár
a	a	k8xC	a
jede	jet	k5eAaImIp3nS	jet
se	se	k3xPyFc4	se
na	na	k7c6	na
vypůjčené	vypůjčený	k2eAgFnSc6d1	vypůjčená
krávě	kráva	k1gFnSc6	kráva
podívat	podívat	k5eAaImF	podívat
na	na	k7c4	na
princeznu	princezna	k1gFnSc4	princezna
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
.	.	kIx.	.
</s>
<s>
Tajně	tajně	k6eAd1	tajně
vnikne	vniknout	k5eAaPmIp3nS	vniknout
do	do	k7c2	do
princezniny	princeznin	k2eAgFnSc2d1	princeznina
zámecké	zámecký	k2eAgFnSc2d1	zámecká
zahrady	zahrada	k1gFnSc2	zahrada
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Bojnický	bojnický	k2eAgInSc4d1	bojnický
zámek	zámek	k1gInSc4	zámek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyleze	vylézt	k5eAaPmIp3nS	vylézt
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lépe	dobře	k6eAd2	dobře
viděl	vidět	k5eAaImAgInS	vidět
do	do	k7c2	do
princezniných	princeznin	k2eAgFnPc2d1	princeznina
komnat	komnata	k1gFnPc2	komnata
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
bezděčně	bezděčně	k6eAd1	bezděčně
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
seznámí	seznámit	k5eAaPmIp3nS	seznámit
(	(	kIx(	(
<g/>
zcela	zcela	k6eAd1	zcela
inkognito	inkognito	k6eAd1	inkognito
<g/>
)	)	kIx)	)
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
Helenou	Helena	k1gFnSc7	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jej	on	k3xPp3gNnSc4	on
chytí	chytit	k5eAaPmIp3nP	chytit
zámecké	zámecký	k2eAgFnPc1d1	zámecká
stráže	stráž	k1gFnPc1	stráž
a	a	k8xC	a
zavřou	zavřít	k5eAaPmIp3nP	zavřít
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
(	(	kIx(	(
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
<g/>
)	)	kIx)	)
kvůli	kvůli	k7c3	kvůli
sousednímu	sousední	k2eAgMnSc3d1	sousední
králi	král	k1gMnSc3	král
(	(	kIx(	(
<g/>
Bohuš	Bohuš	k1gMnSc1	Bohuš
Záhorský	záhorský	k2eAgMnSc1d1	záhorský
<g/>
)	)	kIx)	)
lže	lhát	k5eAaImIp3nS	lhát
<g/>
,	,	kIx,	,
vymlouvá	vymlouvat	k5eAaImIp3nS	vymlouvat
se	se	k3xPyFc4	se
a	a	k8xC	a
stydí	stydět	k5eAaImIp3nS	stydět
se	se	k3xPyFc4	se
za	za	k7c2	za
prince	princ	k1gMnSc2	princ
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
utekl	utéct	k5eAaPmAgMnS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
syn	syn	k1gMnSc1	syn
nemocen	nemocen	k2eAgMnSc1d1	nemocen
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
lží	lež	k1gFnPc2	lež
zamotává	zamotávat	k5eAaImIp3nS	zamotávat
<g/>
.	.	kIx.	.
</s>
<s>
Princezna	princezna	k1gFnSc1	princezna
se	se	k3xPyFc4	se
ale	ale	k9	ale
do	do	k7c2	do
neznámého	známý	k2eNgMnSc2d1	neznámý
vězně	vězeň	k1gMnSc2	vězeň
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
,	,	kIx,	,
netušíc	tušit	k5eNaImSgFnS	tušit
přitom	přitom	k6eAd1	přitom
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
princ	princ	k1gMnSc1	princ
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
budoucí	budoucí	k2eAgMnSc1d1	budoucí
snoubenec	snoubenec	k1gMnSc1	snoubenec
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zrušila	zrušit	k5eAaPmAgFnS	zrušit
očekávané	očekávaný	k2eAgInPc4d1	očekávaný
zásnuby	zásnub	k1gInPc4	zásnub
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
doposud	doposud	k6eAd1	doposud
nezná	neznat	k5eAaImIp3nS	neznat
<g/>
,	,	kIx,	,
předstírá	předstírat	k5eAaImIp3nS	předstírat
na	na	k7c4	na
truc	truc	k1gInSc4	truc
smutek	smutek	k1gInSc1	smutek
a	a	k8xC	a
vyhlásí	vyhlásit	k5eAaPmIp3nS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
za	za	k7c4	za
muže	muž	k1gMnSc4	muž
pouze	pouze	k6eAd1	pouze
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ji	on	k3xPp3gFnSc4	on
rozesměje	rozesmát	k5eAaPmIp3nS	rozesmát
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
z	z	k7c2	z
lidových	lidový	k2eAgFnPc2d1	lidová
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povede	vést	k5eAaImIp3nS	vést
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc4	ten
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
tušil	tušit	k5eAaImAgMnS	tušit
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
princi	princ	k1gMnSc3	princ
Václavovi	Václav	k1gMnSc3	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
inkognito	inkognito	k6eAd1	inkognito
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
ale	ale	k9	ale
prozrazeno	prozrazen	k2eAgNnSc1d1	prozrazeno
a	a	k8xC	a
princezna	princezna	k1gFnSc1	princezna
Helena	Helena	k1gFnSc1	Helena
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
nakrátko	nakrátko	k6eAd1	nakrátko
urazí	urazit	k5eAaPmIp3nS	urazit
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
Václava	Václav	k1gMnSc4	Václav
opět	opět	k6eAd1	opět
zavřít	zavřít	k5eAaPmF	zavřít
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
popravit	popravit	k5eAaPmF	popravit
-	-	kIx~	-
fakticky	fakticky	k6eAd1	fakticky
se	se	k3xPyFc4	se
ale	ale	k9	ale
ve	v	k7c6	v
"	"	kIx"	"
<g/>
veselém	veselý	k2eAgNnSc6d1	veselé
<g/>
"	"	kIx"	"
království	království	k1gNnSc6	království
nikdy	nikdy	k6eAd1	nikdy
nepopravuje	popravovat	k5eNaImIp3nS	popravovat
(	(	kIx(	(
<g/>
kata	kata	k9	kata
a	a	k8xC	a
správce	správce	k1gMnSc1	správce
vězení	vězení	k1gNnSc2	vězení
hraje	hrát	k5eAaImIp3nS	hrát
František	František	k1gMnSc1	František
Dibarbora	Dibarbor	k1gMnSc2	Dibarbor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
královští	královský	k2eAgMnPc1d1	královský
rádcové	rádce	k1gMnPc1	rádce
Y	Y	kA	Y
(	(	kIx(	(
<g/>
Darek	Darek	k1gInSc1	Darek
Vostřel	Vostřel	k1gInSc1	Vostřel
<g/>
)	)	kIx)	)
a	a	k8xC	a
X	X	kA	X
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Kemr	Kemr	k1gMnSc1	Kemr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
princ	princ	k1gMnSc1	princ
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
nechtějí	chtít	k5eNaImIp3nP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
oběma	dva	k4xCgMnPc7	dva
králům	král	k1gMnPc3	král
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
princ	princ	k1gMnSc1	princ
Václav	Václav	k1gMnSc1	Václav
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
generálové	generál	k1gMnPc1	generál
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
bývalí	bývalý	k2eAgMnPc1d1	bývalý
rádcové	rádce	k1gMnPc1	rádce
X	X	kA	X
a	a	k8xC	a
Y	Y	kA	Y
přebírají	přebírat	k5eAaImIp3nP	přebírat
velení	velení	k1gNnSc4	velení
nad	nad	k7c7	nad
oběma	dva	k4xCgFnPc7	dva
armádami	armáda	k1gFnPc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
a	a	k8xC	a
princezna	princezna	k1gFnSc1	princezna
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
usmíří	usmířit	k5eAaPmIp3nS	usmířit
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
oba	dva	k4xCgMnPc1	dva
generály	generál	k1gMnPc4	generál
zajmou	zajmout	k5eAaPmIp3nP	zajmout
a	a	k8xC	a
válku	válka	k1gFnSc4	válka
tím	ten	k3xDgNnSc7	ten
ukončí	ukončit	k5eAaPmIp3nS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
končí	končit	k5eAaImIp3nS	končit
předáním	předání	k1gNnSc7	předání
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
oběma	dva	k4xCgNnPc7	dva
královstvími	království	k1gNnPc7	království
princi	princ	k1gMnSc6	princ
Václavovi	Václav	k1gMnSc6	Václav
a	a	k8xC	a
svatbou	svatba	k1gFnSc7	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Proradní	proradný	k2eAgMnPc1d1	proradný
generálové	generál	k1gMnPc1	generál
a	a	k8xC	a
intrikující	intrikující	k2eAgMnPc1d1	intrikující
rádcové	rádce	k1gMnPc1	rádce
X	X	kA	X
a	a	k8xC	a
Y	Y	kA	Y
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
zásluze	zásluha	k1gFnSc6	zásluha
potrestáni	potrestat	k5eAaPmNgMnP	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Dibarbora	Dibarbor	k1gMnSc2	Dibarbor
(	(	kIx(	(
<g/>
kat	kat	k1gMnSc1	kat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Dědek	Dědek	k1gMnSc1	Dědek
<g/>
,	,	kIx,	,
Bronislav	Bronislav	k1gMnSc1	Bronislav
Koreň	Koreň	k1gFnSc1	Koreň
<g/>
,	,	kIx,	,
Ondrej	Ondrej	k1gMnSc1	Ondrej
Jariabek	Jariabek	k1gMnSc1	Jariabek
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Kuchár	Kuchár	k1gMnSc1	Kuchár
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Effa	Effa	k1gMnSc1	Effa
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Haverle	Haverle	k1gFnSc2	Haverle
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pixa	Pixum	k1gNnSc2	Pixum
Námět	námět	k1gInSc1	námět
a	a	k8xC	a
scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Vlček	Vlček	k1gMnSc1	Vlček
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Zeman	Zeman	k1gMnSc1	Zeman
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
František	František	k1gMnSc1	František
Valert	Valert	k1gMnSc1	Valert
Architekt	architekt	k1gMnSc1	architekt
<g/>
:	:	kIx,	:
Boris	Boris	k1gMnSc1	Boris
Moravec	Moravec	k1gMnSc1	Moravec
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Hammer	Hammer	k1gMnSc1	Hammer
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Zeman	Zeman	k1gMnSc1	Zeman
Produkce	produkce	k1gFnSc1	produkce
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Pokorný	Pokorný	k1gMnSc1	Pokorný
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
zazněla	zaznět	k5eAaImAgFnS	zaznět
řada	řada	k1gFnSc1	řada
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
filmu	film	k1gInSc2	film
staly	stát	k5eAaPmAgFnP	stát
hity	hit	k1gInPc4	hit
<g/>
:	:	kIx,	:
Znám	znát	k5eAaImIp1nS	znát
jednu	jeden	k4xCgFnSc4	jeden
krásnou	krásný	k2eAgFnSc4d1	krásná
princeznu	princezna	k1gFnSc4	princezna
Já	já	k3xPp1nSc1	já
už	už	k9	už
to	ten	k3xDgNnSc1	ten
vím	vědět	k5eAaImIp1nS	vědět
Slza	slza	k1gFnSc1	slza
z	z	k7c2	z
tváře	tvář	k1gFnSc2	tvář
padá	padat	k5eAaImIp3nS	padat
Miluju	milovat	k5eAaImIp1nS	milovat
a	a	k8xC	a
maluju	malovat	k5eAaImIp1nS	malovat
Kujme	kout	k5eAaImRp1nP	kout
pikle	pikel	k1gInPc1	pikel
Znám	znát	k5eAaImIp1nS	znát
jednu	jeden	k4xCgFnSc4	jeden
starou	starý	k2eAgFnSc4d1	stará
zahradu	zahrada	k1gFnSc4	zahrada
Část	část	k1gFnSc4	část
exteriérů	exteriér	k1gInPc2	exteriér
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
natáčela	natáčet	k5eAaImAgFnS	natáčet
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
na	na	k7c6	na
palfyovském	palfyovský	k2eAgInSc6d1	palfyovský
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Bojnicích	Bojnice	k1gFnPc6	Bojnice
a	a	k8xC	a
část	část	k1gFnSc4	část
v	v	k7c4	v
Blatné	blatný	k2eAgNnSc4d1	Blatné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
jsou	být	k5eAaImIp3nP	být
ukázky	ukázka	k1gFnPc1	ukázka
z	z	k7c2	z
několika	několik	k4yIc2	několik
amerických	americký	k2eAgFnPc2d1	americká
grotesek	groteska	k1gFnPc2	groteska
s	s	k7c7	s
Charlie	Charlie	k1gMnSc1	Charlie
Chaplinem	Chaplin	k1gInSc7	Chaplin
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgNnSc4d1	divadelní
nastudování	nastudování	k1gNnSc4	nastudování
Studia	studio	k1gNnSc2	studio
DVA	dva	k4xCgInPc4	dva
mělo	mít	k5eAaImAgNnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Šimona	Šimon	k1gMnSc2	Šimon
Cabana	Caban	k1gMnSc2	Caban
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc4d1	ústřední
dvojici	dvojice	k1gFnSc4	dvojice
prince	princ	k1gMnSc2	princ
a	a	k8xC	a
princezny	princezna	k1gFnPc4	princezna
hrají	hrát	k5eAaImIp3nP	hrát
Jan	Jan	k1gMnSc1	Jan
Cina	Cina	k1gMnSc1	Cina
a	a	k8xC	a
Berenika	Berenik	k1gMnSc2	Berenik
Kohoutová	Kohoutová	k1gFnSc1	Kohoutová
<g/>
.	.	kIx.	.
</s>
