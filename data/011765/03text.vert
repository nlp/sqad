<p>
<s>
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Višňa	Višňa	k1gMnSc1	Višňa
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c2	za
ZVL	zvl	kA	zvl
Žilina	Žilina	k1gFnSc1	Žilina
a	a	k8xC	a
ZVL	zvl	kA	zvl
Považská	Považský	k2eAgFnSc1d1	Považská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
36	[number]	k4	36
utkáních	utkání	k1gNnPc6	utkání
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
5	[number]	k4	5
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ligová	ligový	k2eAgFnSc1d1	ligová
bilance	bilance	k1gFnSc1	bilance
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Player	Player	k1gInSc1	Player
History	Histor	k1gInPc1	Histor
</s>
</p>
