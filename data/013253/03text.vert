<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Žežulka	Žežulka	k1gMnSc1	Žežulka
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1974	[number]	k4	1974
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Bubnoval	bubnovat	k5eAaImAgInS	bubnovat
léta	léto	k1gNnPc4	léto
v	v	k7c6	v
Arakainu	Arakain	k1gInSc6	Arakain
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
u	u	k7c2	u
Divokýho	Divokýho	k?	Divokýho
Billa	Bill	k1gMnSc2	Bill
a	a	k8xC	a
Dana	Dan	k1gMnSc2	Dan
Landy	Landa	k1gMnSc2	Landa
<g/>
,	,	kIx,	,
prošel	projít	k5eAaPmAgMnS	projít
si	se	k3xPyFc3	se
i	i	k8xC	i
black	black	k6eAd1	black
metalovou	metalový	k2eAgFnSc7d1	metalová
kapelou	kapela	k1gFnSc7	kapela
Törr	Törra	k1gFnPc2	Törra
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
též	též	k9	též
v	v	k7c6	v
rockové	rockový	k2eAgFnSc6d1	rocková
úpravě	úprava	k1gFnSc6	úprava
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
–	–	k?	–
Rockquiem	Rockquium	k1gNnSc7	Rockquium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Žežulka	Žežulka	k1gMnSc1	Žežulka
je	být	k5eAaImIp3nS	být
firemním	firemní	k2eAgMnSc7d1	firemní
hráčem	hráč	k1gMnSc7	hráč
značky	značka	k1gFnSc2	značka
Zildjian	Zildjiana	k1gFnPc2	Zildjiana
a	a	k8xC	a
Gretsch	Gretscha	k1gFnPc2	Gretscha
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
bicí	bicí	k2eAgFnSc4d1	bicí
sadu	sada	k1gFnSc4	sada
New	New	k1gFnSc1	New
Classic	Classice	k1gFnPc2	Classice
a	a	k8xC	a
činely	činela	k1gFnSc2	činela
řady	řada	k1gFnSc2	řada
"	"	kIx"	"
<g/>
A	a	k9	a
Custom	Custom	k1gInSc1	Custom
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Z	Z	kA	Z
Custom	Custom	k1gInSc1	Custom
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
