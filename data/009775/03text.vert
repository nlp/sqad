<p>
<s>
Jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
genus	genus	k1gInSc1	genus
nominis	nominis	k1gFnSc2	nominis
či	či	k8xC	či
jen	jen	k9	jen
genus	genus	k1gInSc1	genus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mluvnická	mluvnický	k2eAgFnSc1d1	mluvnická
kategorie	kategorie	k1gFnSc1	kategorie
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
nomina	nomina	k1gFnSc1	nomina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
,	,	kIx,	,
číslovek	číslovka	k1gFnPc2	číslovka
a	a	k8xC	a
případně	případně	k6eAd1	případně
dalších	další	k2eAgInPc2d1	další
slovních	slovní	k2eAgInPc2d1	slovní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jejich	jejich	k3xOp3gNnSc4	jejich
skloňování	skloňování	k1gNnSc4	skloňování
a	a	k8xC	a
také	také	k9	také
tvary	tvar	k1gInPc1	tvar
sloves	sloveso	k1gNnPc2	sloveso
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
příčestí	příčestí	k1gNnSc1	příčestí
<g/>
.	.	kIx.	.
</s>
<s>
Jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
gramatických	gramatický	k2eAgInPc2d1	gramatický
(	(	kIx(	(
<g/>
mluvnických	mluvnický	k2eAgInPc2d1	mluvnický
<g/>
)	)	kIx)	)
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
zapomíná	zapomínat	k5eAaImIp3nS	zapomínat
na	na	k7c4	na
existenci	existence	k1gFnSc4	existence
slovesného	slovesný	k2eAgInSc2d1	slovesný
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
genus	genus	k1gInSc1	genus
verbi	verb	k1gFnSc2	verb
<g/>
)	)	kIx)	)
a	a	k8xC	a
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
se	se	k3xPyFc4	se
jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
gramatického	gramatický	k2eAgInSc2d1	gramatický
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
jen	jen	k9	jen
o	o	k7c6	o
rodu	rod	k1gInSc6	rod
<g/>
.	.	kIx.	.
<g/>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
rod	rod	k1gInSc1	rod
mužský	mužský	k2eAgInSc1d1	mužský
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
masculinum	masculinum	k1gInSc1	masculinum
<g/>
,	,	kIx,	,
počeštěně	počeštěně	k6eAd1	počeštěně
maskulinum	maskulinum	k1gNnSc1	maskulinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
(	(	kIx(	(
<g/>
femininum	femininum	k1gNnSc1	femininum
<g/>
)	)	kIx)	)
a	a	k8xC	a
střední	střední	k2eAgNnSc4d1	střední
(	(	kIx(	(
<g/>
neutrum	neutrum	k1gNnSc4	neutrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
však	však	k9	však
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
rodů	rod	k1gInPc2	rod
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
často	často	k6eAd1	často
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
biologickou	biologický	k2eAgFnSc7d1	biologická
pohlavností	pohlavnost	k1gFnSc7	pohlavnost
a	a	k8xC	a
s	s	k7c7	s
životností	životnost	k1gFnSc7	životnost
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
neživotností	neživotnost	k1gFnPc2	neživotnost
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
od	od	k7c2	od
biologické	biologický	k2eAgFnSc2d1	biologická
pohlavnosti	pohlavnost	k1gFnSc2	pohlavnost
odlišný	odlišný	k2eAgInSc1d1	odlišný
<g/>
,	,	kIx,	,
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
–	–	k?	–
např.	např.	kA	např.
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
neživé	živý	k2eNgFnPc4d1	neživá
věci	věc	k1gFnPc4	věc
nejen	nejen	k6eAd1	nejen
rodu	rod	k1gInSc2	rod
středního	střední	k1gMnSc2	střední
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mužského	mužský	k2eAgNnSc2d1	mužské
nebo	nebo	k8xC	nebo
ženského	ženský	k2eAgNnSc2d1	ženské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
indoevropských	indoevropský	k2eAgInPc6d1	indoevropský
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
rozlišoval	rozlišovat	k5eAaImAgInS	rozlišovat
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
románských	románský	k2eAgInPc6d1	románský
jazycích	jazyk	k1gInPc6	jazyk
však	však	k9	však
dnes	dnes	k6eAd1	dnes
neexistuje	existovat	k5eNaImIp3nS	existovat
rod	rod	k1gInSc1	rod
střední	střední	k2eAgFnSc2d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
severogermánských	severogermánský	k2eAgInPc6d1	severogermánský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc1d1	ženský
rod	rod	k1gInSc1	rod
splynul	splynout	k5eAaPmAgInS	splynout
s	s	k7c7	s
mužským	mužský	k2eAgInSc7d1	mužský
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
rod	rod	k1gInSc4	rod
nazýváme	nazývat	k5eAaImIp1nP	nazývat
společný	společný	k2eAgInSc4d1	společný
(	(	kIx(	(
<g/>
commune	commun	k1gInSc5	commun
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
pak	pak	k6eAd1	pak
u	u	k7c2	u
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
podrod	podrod	k1gInSc4	podrod
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
věcný	věcný	k2eAgInSc4d1	věcný
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
rodu	rod	k1gInSc3	rod
středního	střední	k2eAgNnSc2d1	střední
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
systém	systém	k1gInSc1	systém
mluvnických	mluvnický	k2eAgInPc2d1	mluvnický
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
rozlišení	rozlišení	k1gNnSc1	rozlišení
přirozeného	přirozený	k2eAgInSc2d1	přirozený
rodu	rod	k1gInSc2	rod
u	u	k7c2	u
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgMnPc4d1	mnohý
jazyky	jazyk	k1gMnPc4	jazyk
kategorii	kategorie	k1gFnSc6	kategorie
mluvnického	mluvnický	k2eAgInSc2d1	mluvnický
rodu	rod	k1gInSc2	rod
vůbec	vůbec	k9	vůbec
neznají	neznat	k5eAaImIp3nP	neznat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
u	u	k7c2	u
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
případem	případ	k1gInSc7	případ
jsou	být	k5eAaImIp3nP	být
ugrofinské	ugrofinský	k2eAgInPc1d1	ugrofinský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
finština	finština	k1gFnSc1	finština
nebo	nebo	k8xC	nebo
maďarština	maďarština	k1gFnSc1	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
jména	jméno	k1gNnSc2	jméno
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tří	tři	k4xCgFnPc2	tři
rodových	rodový	k2eAgFnPc2d1	rodová
kategorií	kategorie	k1gFnPc2	kategorie
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
i	i	k9	i
kategorie	kategorie	k1gFnSc1	kategorie
životnosti	životnost	k1gFnSc2	životnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rod	rod	k1gInSc4	rod
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
Čeština	čeština	k1gFnSc1	čeština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
,	,	kIx,	,
u	u	k7c2	u
jmen	jméno	k1gNnPc2	jméno
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
diferencuje	diferencovat	k5eAaImIp3nS	diferencovat
na	na	k7c4	na
životná	životný	k2eAgNnPc4d1	životné
a	a	k8xC	a
neživotná	životný	k2eNgNnPc4d1	neživotné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
podmětu	podmět	k1gInSc2	podmět
shodou	shoda	k1gFnSc7	shoda
(	(	kIx(	(
<g/>
kongruencí	kongruence	k1gFnSc7	kongruence
<g/>
)	)	kIx)	)
promítá	promítat	k5eAaImIp3nS	promítat
i	i	k9	i
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
slovesných	slovesný	k2eAgInPc2d1	slovesný
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
příčestí	příčestí	k1gNnSc2	příčestí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
substantiv	substantivum	k1gNnPc2	substantivum
gramatický	gramatický	k2eAgInSc1d1	gramatický
rod	rod	k1gInSc1	rod
kolísá	kolísat	k5eAaImIp3nS	kolísat
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
varianta	varianta	k1gFnSc1	varianta
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příznaková	příznakový	k2eAgFnSc1d1	příznaková
(	(	kIx(	(
<g/>
regionálně	regionálně	k6eAd1	regionálně
<g/>
,	,	kIx,	,
stylisticky	stylisticky	k6eAd1	stylisticky
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
rodu	rod	k1gInSc2	rod
slouží	sloužit	k5eAaImIp3nS	sloužit
tzv.	tzv.	kA	tzv.
přechylování	přechylování	k1gNnSc1	přechylování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
jazyků	jazyk	k1gInPc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gMnPc4	jazyk
bez	bez	k7c2	bez
mluvnických	mluvnický	k2eAgInPc2d1	mluvnický
rodů	rod	k1gInPc2	rod
===	===	k?	===
</s>
</p>
<p>
<s>
Afrikánština	afrikánština	k1gFnSc1	afrikánština
</s>
</p>
<p>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
(	(	kIx(	(
<g/>
Angličtina	angličtina	k1gFnSc1	angličtina
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
zájmen	zájmeno	k1gNnPc2	zájmeno
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc3	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
systém	systém	k1gInSc1	systém
přirozených	přirozený	k2eAgInPc2d1	přirozený
rodů	rod	k1gInPc2	rod
(	(	kIx(	(
<g/>
he	he	k0	he
<g/>
,	,	kIx,	,
she	she	k?	she
<g/>
,	,	kIx,	,
it	it	k?	it
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
mluvnické	mluvnický	k2eAgInPc4d1	mluvnický
rody	rod	k1gInPc4	rod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Arménština	arménština	k1gFnSc1	arménština
</s>
</p>
<p>
<s>
Barmština	barmština	k1gFnSc1	barmština
</s>
</p>
<p>
<s>
Baskičtina	baskičtina	k1gFnSc1	baskičtina
</s>
</p>
<p>
<s>
Bengálština	bengálština	k1gFnSc1	bengálština
</s>
</p>
<p>
<s>
Bislama	Bislama	k1gFnSc1	Bislama
</s>
</p>
<p>
<s>
Bugis	Bugis	k1gFnSc1	Bugis
</s>
</p>
<p>
<s>
Cebuano	Cebuana	k1gFnSc5	Cebuana
</s>
</p>
<p>
<s>
Čínština	čínština	k1gFnSc1	čínština
</s>
</p>
<p>
<s>
Esperanto	esperanto	k1gNnSc1	esperanto
(	(	kIx(	(
<g/>
Esperanto	esperanto	k1gNnSc1	esperanto
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
rodem	rod	k1gInSc7	rod
zájmena	zájmeno	k1gNnSc2	zájmeno
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc6	osoba
a	a	k8xC	a
příponu	přípona	k1gFnSc4	přípona
označující	označující	k2eAgInSc4d1	označující
ženský	ženský	k2eAgInSc4d1	ženský
rod	rod	k1gInSc4	rod
<g/>
.	.	kIx.	.
)	)	kIx)	)
</s>
</p>
<p>
<s>
Estonština	estonština	k1gFnSc1	estonština
</s>
</p>
<p>
<s>
Finština	finština	k1gFnSc1	finština
</s>
</p>
<p>
<s>
Guaraní	Guaranit	k5eAaPmIp3nS	Guaranit
</s>
</p>
<p>
<s>
Gruzínština	gruzínština	k1gFnSc1	gruzínština
</s>
</p>
<p>
<s>
Grónština	grónština	k1gFnSc1	grónština
(	(	kIx(	(
<g/>
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Havajština	havajština	k1gFnSc1	havajština
</s>
</p>
<p>
<s>
Chol	Chol	k1gMnSc1	Chol
</s>
</p>
<p>
<s>
Ido	ido	k1gNnSc1	ido
(	(	kIx(	(
<g/>
Ido	ido	k1gNnSc1	ido
má	mít	k5eAaImIp3nS	mít
mužský	mužský	k2eAgInSc4d1	mužský
infix	infix	k1gInSc4	infix
-ul	l	k?	-ul
a	a	k8xC	a
ženský	ženský	k2eAgInSc1d1	ženský
infix	infix	k1gInSc1	infix
-in	n	k?	-in
pro	pro	k7c4	pro
živé	živé	k1gNnSc4	živé
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
infixy	infix	k1gInPc1	infix
jsou	být	k5eAaImIp3nP	být
volitelné	volitelný	k2eAgInPc1d1	volitelný
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
zamezení	zamezení	k1gNnSc4	zamezení
záměn	záměna	k1gFnPc2	záměna
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
mají	mít	k5eAaImIp3nP	mít
osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
i	i	k8xC	i
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
tvary	tvar	k1gInPc1	tvar
pro	pro	k7c4	pro
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ilocano	Ilocana	k1gFnSc5	Ilocana
</s>
</p>
<p>
<s>
Indonéština	indonéština	k1gFnSc1	indonéština
</s>
</p>
<p>
<s>
Interlingua	Interlingua	k6eAd1	Interlingua
</s>
</p>
<p>
<s>
Japonština	japonština	k1gFnSc1	japonština
</s>
</p>
<p>
<s>
Kannadština	Kannadština	k1gFnSc1	Kannadština
(	(	kIx(	(
<g/>
Kannadština	Kannadština	k1gFnSc1	Kannadština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
rod	rod	k1gInSc4	rod
u	u	k7c2	u
životných	životný	k2eAgNnPc2d1	životné
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
učitel	učitel	k1gMnSc1	učitel
<g/>
/	/	kIx~	/
<g/>
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
,	,	kIx,	,
sluha	sluha	k1gMnSc1	sluha
<g/>
/	/	kIx~	/
<g/>
služka	služka	k1gFnSc1	služka
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
souvisejících	související	k2eAgInPc2d1	související
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kečua	Kečua	k6eAd1	Kečua
</s>
</p>
<p>
<s>
Khmerština	khmerština	k1gFnSc1	khmerština
</s>
</p>
<p>
<s>
Klingonština	Klingonština	k1gFnSc1	Klingonština
</s>
</p>
<p>
<s>
Korejština	korejština	k1gFnSc1	korejština
</s>
</p>
<p>
<s>
Laoština	Laoština	k1gFnSc1	Laoština
</s>
</p>
<p>
<s>
Laponské	laponský	k2eAgInPc1d1	laponský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
Lojban	Lojban	k1gMnSc1	Lojban
</s>
</p>
<p>
<s>
Makasar	Makasar	k1gMnSc1	Makasar
</s>
</p>
<p>
<s>
Malajština	malajština	k1gFnSc1	malajština
</s>
</p>
<p>
<s>
Malayalam	Malayalam	k6eAd1	Malayalam
</s>
</p>
<p>
<s>
Malgaština	malgaština	k1gFnSc1	malgaština
</s>
</p>
<p>
<s>
Mandar	Mandar	k1gMnSc1	Mandar
</s>
</p>
<p>
<s>
Maďarština	maďarština	k1gFnSc1	maďarština
</s>
</p>
<p>
<s>
Nahuatl	Nahuatnout	k5eAaPmAgMnS	Nahuatnout
</s>
</p>
<p>
<s>
Papiamentu	Papiament	k1gMnSc3	Papiament
</s>
</p>
<p>
<s>
Perština	perština	k1gFnSc1	perština
</s>
</p>
<p>
<s>
Pirahã	Pirahã	k?	Pirahã
</s>
</p>
<p>
<s>
Quenya	Quenya	k6eAd1	Quenya
</s>
</p>
<p>
<s>
Sindarin	Sindarin	k1gInSc1	Sindarin
</s>
</p>
<p>
<s>
Sinhálština	sinhálština	k1gFnSc1	sinhálština
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
Yup	Yup	k1gFnSc1	Yup
<g/>
'	'	kIx"	'
<g/>
ik	ik	k?	ik
</s>
</p>
<p>
<s>
Tagalog	Tagalog	k1gMnSc1	Tagalog
</s>
</p>
<p>
<s>
Telugština	Telugština	k1gFnSc1	Telugština
</s>
</p>
<p>
<s>
Thajština	thajština	k1gFnSc1	thajština
</s>
</p>
<p>
<s>
Tlingit	Tlingit	k1gMnSc1	Tlingit
</s>
</p>
<p>
<s>
Tok	tok	k1gInSc1	tok
Pisin	Pisina	k1gFnPc2	Pisina
</s>
</p>
<p>
<s>
Toki	Toki	k6eAd1	Toki
Pona	Pon	k2eAgFnSc1d1	Pon
</s>
</p>
<p>
<s>
Tulu	Tula	k1gFnSc4	Tula
</s>
</p>
<p>
<s>
Turečtina	turečtina	k1gFnSc1	turečtina
</s>
</p>
<p>
<s>
Tzeltal	Tzeltal	k1gMnSc1	Tzeltal
</s>
</p>
<p>
<s>
Tzotzil	Tzotzit	k5eAaPmAgMnS	Tzotzit
</s>
</p>
<p>
<s>
Vietnamština	vietnamština	k1gFnSc1	vietnamština
</s>
</p>
<p>
<s>
Yoruba	Yoruba	k1gFnSc1	Yoruba
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
mluvnickými	mluvnický	k2eAgInPc7d1	mluvnický
rody	rod	k1gInPc7	rod
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
rod	rod	k1gInSc4	rod
====	====	k?	====
</s>
</p>
<p>
<s>
Akkadština	akkadština	k1gFnSc1	akkadština
</s>
</p>
<p>
<s>
Arabština	arabština	k1gFnSc1	arabština
</s>
</p>
<p>
<s>
Aramejština	aramejština	k1gFnSc1	aramejština
</s>
</p>
<p>
<s>
Bengálština	bengálština	k1gFnSc1	bengálština
</s>
</p>
<p>
<s>
Katalánština	katalánština	k1gFnSc1	katalánština
</s>
</p>
<p>
<s>
Koptština	koptština	k1gFnSc1	koptština
</s>
</p>
<p>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
</s>
</p>
<p>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
</s>
</p>
<p>
<s>
Hindština	hindština	k1gFnSc1	hindština
</s>
</p>
<p>
<s>
Irština	irština	k1gFnSc1	irština
</s>
</p>
<p>
<s>
Italština	italština	k1gFnSc1	italština
</s>
</p>
<p>
<s>
Litevština	litevština	k1gFnSc1	litevština
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
též	též	k6eAd1	též
"	"	kIx"	"
<g/>
Tři	tři	k4xCgInPc1	tři
rody	rod	k1gInPc1	rod
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
systémem	systém	k1gInSc7	systém
rozdělení	rozdělení	k1gNnSc2	rozdělení
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Lotyština	lotyština	k1gFnSc1	lotyština
</s>
</p>
<p>
<s>
Okcitánština	Okcitánština	k1gFnSc1	Okcitánština
</s>
</p>
<p>
<s>
Paňdžábština	paňdžábština	k1gFnSc1	paňdžábština
</s>
</p>
<p>
<s>
Portugalština	portugalština	k1gFnSc1	portugalština
</s>
</p>
<p>
<s>
Skotská	skotský	k2eAgFnSc1d1	skotská
gaelština	gaelština	k1gFnSc1	gaelština
</s>
</p>
<p>
<s>
Starověká	starověký	k2eAgFnSc1d1	starověká
egyptština	egyptština	k1gFnSc1	egyptština
</s>
</p>
<p>
<s>
Španělština	španělština	k1gFnSc1	španělština
</s>
</p>
<p>
<s>
Tamazight	Tamazight	k1gInSc1	Tamazight
(	(	kIx(	(
<g/>
Berberština	berberština	k1gFnSc1	berberština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Urdština	urdština	k1gFnSc1	urdština
</s>
</p>
<p>
<s>
Velština	velština	k1gFnSc1	velština
</s>
</p>
<p>
<s>
====	====	k?	====
Společný	společný	k2eAgInSc4d1	společný
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
rod	rod	k1gInSc4	rod
====	====	k?	====
</s>
</p>
<p>
<s>
Dánština	dánština	k1gFnSc1	dánština
</s>
</p>
<p>
<s>
Dolnoněmčina	dolnoněmčina	k1gFnSc1	dolnoněmčina
</s>
</p>
<p>
<s>
Norština	norština	k1gFnSc1	norština
(	(	kIx(	(
<g/>
Riksmå	Riksmå	k1gFnSc1	Riksmå
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Švédština	švédština	k1gFnSc1	švédština
</s>
</p>
<p>
<s>
====	====	k?	====
Životný	životný	k2eAgInSc4d1	životný
a	a	k8xC	a
neživotný	životný	k2eNgInSc4d1	neživotný
rod	rod	k1gInSc4	rod
====	====	k?	====
</s>
</p>
<p>
<s>
Algonkinské	Algonkinský	k2eAgInPc1d1	Algonkinský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
Baskičtina	baskičtina	k1gFnSc1	baskičtina
</s>
</p>
<p>
<s>
Chetitština	chetitština	k1gFnSc1	chetitština
</s>
</p>
<p>
<s>
SumerštinaV	SumerštinaV	k?	SumerštinaV
mnoha	mnoho	k4c3	mnoho
těchto	tento	k3xDgInPc6	tento
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
životnost	životnost	k1gFnSc1	životnost
<g/>
"	"	kIx"	"
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
míněna	mínit	k5eAaImNgFnS	mínit
spíše	spíše	k9	spíše
jiná	jiný	k2eAgFnSc1d1	jiná
distinkce	distinkce	k1gFnSc1	distinkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lidský	lidský	k2eAgMnSc1d1	lidský
a	a	k8xC	a
ne-lidský	idský	k2eNgMnSc1d1	-lidský
<g/>
,	,	kIx,	,
racionální	racionální	k2eAgInPc1d1	racionální
a	a	k8xC	a
neracionální	racionální	k2eNgInPc1d1	neracionální
<g/>
,	,	kIx,	,
sociálně	sociálně	k6eAd1	sociálně
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
pasivní	pasivní	k2eAgInSc4d1	pasivní
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
mluvnickými	mluvnický	k2eAgInPc7d1	mluvnický
rody	rod	k1gInPc7	rod
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
rod	rod	k1gInSc4	rod
====	====	k?	====
</s>
</p>
<p>
<s>
Albánština	albánština	k1gFnSc1	albánština
(	(	kIx(	(
<g/>
Střední	střední	k2eAgInSc1d1	střední
rod	rod	k1gInSc1	rod
však	však	k9	však
téměř	téměř	k6eAd1	téměř
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Běloruština	běloruština	k1gFnSc1	běloruština
</s>
</p>
<p>
<s>
Bosenština	Bosenština	k1gFnSc1	Bosenština
</s>
</p>
<p>
<s>
Bulharština	bulharština	k1gFnSc1	bulharština
</s>
</p>
<p>
<s>
Čeština	čeština	k1gFnSc1	čeština
</s>
</p>
<p>
<s>
Faerština	Faerština	k1gFnSc1	Faerština
</s>
</p>
<p>
<s>
Gujarati	Gujarat	k1gMnPc1	Gujarat
</s>
</p>
<p>
<s>
Chorvatština	chorvatština	k1gFnSc1	chorvatština
</s>
</p>
<p>
<s>
Islandština	islandština	k1gFnSc1	islandština
</s>
</p>
<p>
<s>
Jidiš	jidiš	k6eAd1	jidiš
</s>
</p>
<p>
<s>
Latina	latina	k1gFnSc1	latina
</s>
</p>
<p>
<s>
Lužická	lužický	k2eAgFnSc1d1	Lužická
srbština	srbština	k1gFnSc1	srbština
</s>
</p>
<p>
<s>
Marathi	Marathi	k6eAd1	Marathi
</s>
</p>
<p>
<s>
Němčina	němčina	k1gFnSc1	němčina
</s>
</p>
<p>
<s>
Nizozemština	nizozemština	k1gFnSc1	nizozemština
(	(	kIx(	(
<g/>
Mužský	mužský	k2eAgInSc1d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc1d1	ženský
rod	rod	k1gInSc1	rod
téměř	téměř	k6eAd1	téměř
splynuly	splynout	k5eAaPmAgFnP	splynout
<g/>
,	,	kIx,	,
rozdíly	rozdíl	k1gInPc4	rozdíl
však	však	k9	však
přesto	přesto	k6eAd1	přesto
stále	stále	k6eAd1	stále
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Norština	norština	k1gFnSc1	norština
</s>
</p>
<p>
<s>
Polština	polština	k1gFnSc1	polština
</s>
</p>
<p>
<s>
Rumunština	rumunština	k1gFnSc1	rumunština
</s>
</p>
<p>
<s>
Ruština	ruština	k1gFnSc1	ruština
</s>
</p>
<p>
<s>
Řečtina	řečtina	k1gFnSc1	řečtina
</s>
</p>
<p>
<s>
Sanskrt	sanskrt	k1gInSc1	sanskrt
</s>
</p>
<p>
<s>
Srbština	srbština	k1gFnSc1	srbština
</s>
</p>
<p>
<s>
Slovenština	slovenština	k1gFnSc1	slovenština
</s>
</p>
<p>
<s>
Slovinština	slovinština	k1gFnSc1	slovinština
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
angličtina	angličtina	k1gFnSc1	angličtina
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
pruština	pruština	k1gFnSc1	pruština
</s>
</p>
<p>
<s>
Starořečtina	Starořečtina	k1gFnSc1	Starořečtina
</s>
</p>
<p>
<s>
Tamilština	tamilština	k1gFnSc1	tamilština
(	(	kIx(	(
<g/>
Má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
oslovování	oslovování	k1gNnSc3	oslovování
učitelů	učitel	k1gMnPc2	učitel
a	a	k8xC	a
vyšších	vysoký	k2eAgMnPc2d2	vyšší
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
něco	něco	k3yInSc1	něco
jako	jako	k9	jako
české	český	k2eAgNnSc4d1	české
vykání	vykání	k1gNnSc4	vykání
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
</s>
</p>
<p>
<s>
====	====	k?	====
Tři	tři	k4xCgInPc1	tři
rody	rod	k1gInPc1	rod
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
systémem	systém	k1gInSc7	systém
rozdělení	rozdělení	k1gNnSc2	rozdělení
====	====	k?	====
</s>
</p>
<p>
<s>
Klingonština	Klingonština	k1gFnSc1	Klingonština
(	(	kIx(	(
<g/>
umělý	umělý	k2eAgInSc1d1	umělý
jazyk	jazyk	k1gInSc1	jazyk
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Star	Star	kA	Star
Treku	Trek	k1gInSc3	Trek
<g/>
;	;	kIx,	;
schopní	schopný	k2eAgMnPc1d1	schopný
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Litevština	litevština	k1gFnSc1	litevština
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tři	tři	k4xCgInPc4	tři
rody	rod	k1gInPc4	rod
<g/>
:	:	kIx,	:
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
rodu	rod	k1gInSc2	rod
<g/>
"	"	kIx"	"
-	-	kIx~	-
tedy	tedy	k9	tedy
nikoliv	nikoliv	k9	nikoliv
rod	rod	k1gInSc1	rod
střední	střední	k2eAgFnSc2d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
bez	bez	k7c2	bez
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
u	u	k7c2	u
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
tvarů	tvar	k1gInPc2	tvar
přechodníků	přechodník	k1gInPc2	přechodník
sloves	sloveso	k1gNnPc2	sloveso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třemi	tři	k4xCgInPc7	tři
mluvnickými	mluvnický	k2eAgInPc7d1	mluvnický
rody	rod	k1gInPc7	rod
===	===	k?	===
</s>
</p>
<p>
<s>
Bantuské	bantuský	k2eAgInPc1d1	bantuský
jazyky	jazyk	k1gInPc1	jazyk
</s>
</p>
<p>
<s>
Bats	Bats	k6eAd1	Bats
</s>
</p>
<p>
<s>
Dyirbal	Dyirbal	k1gInSc1	Dyirbal
</s>
</p>
<p>
<s>
Svahilština	svahilština	k1gFnSc1	svahilština
</s>
</p>
<p>
<s>
ZuluNěkteré	ZuluNěkterý	k2eAgInPc1d1	ZuluNěkterý
slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
mužský	mužský	k2eAgInSc4d1	mužský
rod	rod	k1gInSc4	rod
životný	životný	k2eAgInSc4d1	životný
a	a	k8xC	a
neživotný	životný	k2eNgInSc4d1	neživotný
<g/>
.	.	kIx.	.
</s>
<s>
Polština	polština	k1gFnSc1	polština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
životný	životný	k2eAgInSc4d1	životný
a	a	k8xC	a
neživotný	životný	k2eNgInSc4d1	neživotný
mužský	mužský	k2eAgInSc4d1	mužský
rod	rod	k1gInSc4	rod
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
rod	rod	k1gInSc4	rod
mužský	mužský	k2eAgInSc1d1	mužský
osobní	osobní	k2eAgInSc1d1	osobní
a	a	k8xC	a
ostatní	ostatní	k2eAgMnSc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švédština	švédština	k1gFnSc1	švédština
ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
rodě	rod	k1gInSc6	rod
u	u	k7c2	u
osobních	osobní	k2eAgNnPc2d1	osobní
zájmen	zájmeno	k1gNnPc2	zájmeno
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
podrod	podrod	k1gInSc1	podrod
mužský	mužský	k2eAgInSc1d1	mužský
(	(	kIx(	(
<g/>
han	hana	k1gFnPc2	hana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
(	(	kIx(	(
<g/>
hon	hon	k1gInSc4	hon
<g/>
)	)	kIx)	)
a	a	k8xC	a
věcný	věcný	k2eAgInSc1d1	věcný
(	(	kIx(	(
<g/>
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
rodě	rod	k1gInSc6	rod
má	mít	k5eAaImIp3nS	mít
zájmeno	zájmeno	k1gNnSc4	zájmeno
det	det	k?	det
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgInPc1	tři
rody	rod	k1gInPc1	rod
včetně	včetně	k7c2	včetně
označení	označení	k1gNnSc2	označení
míry	míra	k1gFnSc2	míra
====	====	k?	====
</s>
</p>
<p>
<s>
Ainština	Ainština	k1gFnSc1	Ainština
</s>
</p>
<p>
<s>
Bengálština	bengálština	k1gFnSc1	bengálština
</s>
</p>
<p>
<s>
Čínština	čínština	k1gFnSc1	čínština
</s>
</p>
<p>
<s>
Japonština	japonština	k1gFnSc1	japonština
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Korejština	korejština	k1gFnSc1	korejština
</s>
</p>
<p>
<s>
Thajština	thajština	k1gFnSc1	thajština
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Slovesný	slovesný	k2eAgInSc1d1	slovesný
rod	rod	k1gInSc1	rod
</s>
</p>
