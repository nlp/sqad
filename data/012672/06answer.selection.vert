<s>
Konstriktivy	konstriktiva	k1gFnPc1	konstriktiva
–	–	k?	–
úžinové	úžinový	k2eAgFnPc1d1	úžinová
souhlásky	souhláska	k1gFnPc1	souhláska
–	–	k?	–
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
těsným	těsný	k2eAgNnSc7d1	těsné
přiblížením	přiblížení	k1gNnSc7	přiblížení
dvou	dva	k4xCgInPc2	dva
artikulátorů	artikulátor	k1gInPc2	artikulátor
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
úžina	úžina	k1gFnSc1	úžina
(	(	kIx(	(
<g/>
konstrikce	konstrikce	k1gFnSc1	konstrikce
<g/>
)	)	kIx)	)
a	a	k8xC	a
silný	silný	k2eAgInSc4d1	silný
šum	šum	k1gInSc4	šum
<g/>
.	.	kIx.	.
</s>
