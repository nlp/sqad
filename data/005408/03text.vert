<s>
Škára	škára	k1gFnSc1	škára
(	(	kIx(	(
<g/>
dermis	dermis	k1gFnSc1	dermis
<g/>
,	,	kIx,	,
corium	corium	k1gNnSc1	corium
<g/>
,	,	kIx,	,
cutis	cutis	k1gFnSc1	cutis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrstva	vrstva	k1gFnSc1	vrstva
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pojivové	pojivový	k2eAgFnSc2d1	pojivová
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
tělo	tělo	k1gNnSc4	tělo
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Škára	škára	k1gFnSc1	škára
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
pokožce	pokožka	k1gFnSc3	pokožka
(	(	kIx(	(
<g/>
epidermis	epidermis	k1gFnSc1	epidermis
<g/>
)	)	kIx)	)
membránou	membrána	k1gFnSc7	membrána
zvanou	zvaný	k2eAgFnSc4d1	zvaná
bazální	bazální	k2eAgFnSc4d1	bazální
membrána	membrána	k1gFnSc1	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škáře	škára	k1gFnSc6	škára
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
nervových	nervový	k2eAgNnPc2d1	nervové
zakončení	zakončení	k1gNnPc2	zakončení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
vnímají	vnímat	k5eAaImIp3nP	vnímat
hmat	hmat	k1gInSc4	hmat
a	a	k8xC	a
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Škára	škára	k1gFnSc1	škára
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vlasové	vlasový	k2eAgInPc4d1	vlasový
folikuly	folikul	k1gInPc4	folikul
<g/>
,	,	kIx,	,
potní	potní	k2eAgFnPc4d1	potní
žlázy	žláza	k1gFnPc4	žláza
<g/>
,	,	kIx,	,
mazové	mazový	k2eAgFnPc4d1	mazová
žlázy	žláza	k1gFnPc4	žláza
a	a	k8xC	a
krevní	krevní	k2eAgFnPc4d1	krevní
cévy	céva	k1gFnPc4	céva
<g/>
.	.	kIx.	.
</s>
<s>
Cévy	céva	k1gFnPc1	céva
zásobují	zásobovat	k5eAaImIp3nP	zásobovat
živinami	živina	k1gFnPc7	živina
nejen	nejen	k6eAd1	nejen
buňky	buňka	k1gFnPc1	buňka
škáry	škára	k1gFnSc2	škára
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyživují	vyživovat	k5eAaImIp3nP	vyživovat
i	i	k9	i
stratum	stratum	k1gNnSc4	stratum
basale	basale	k6eAd1	basale
v	v	k7c6	v
epidermis	epidermis	k1gFnSc6	epidermis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
dermis	dermis	k1gFnSc2	dermis
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
škára	škára	k1gFnSc1	škára
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
