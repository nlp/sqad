<s>
Škára	škára	k1gFnSc1	škára
(	(	kIx(	(
<g/>
dermis	dermis	k1gFnSc1	dermis
<g/>
,	,	kIx,	,
corium	corium	k1gNnSc1	corium
<g/>
,	,	kIx,	,
cutis	cutis	k1gFnSc1	cutis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrstva	vrstva	k1gFnSc1	vrstva
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pojivové	pojivový	k2eAgFnSc2d1	pojivová
tkáně	tkáň	k1gFnSc2	tkáň
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
tělo	tělo	k1gNnSc4	tělo
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
