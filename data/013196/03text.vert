<p>
<s>
Hudební	hudební	k2eAgFnPc1d1	hudební
ceny	cena	k1gFnPc1	cena
Vinyla	Vinyla	k1gFnSc1	Vinyla
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
a	a	k8xC	a
ocenit	ocenit	k5eAaPmF	ocenit
důležité	důležitý	k2eAgInPc4d1	důležitý
počiny	počin	k1gInPc4	počin
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
české	český	k2eAgFnSc6d1	Česká
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vinyla	Vinýt	k5eAaPmAgFnS	Vinýt
se	se	k3xPyFc4	se
programově	programově	k6eAd1	programově
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
autory	autor	k1gMnPc4	autor
a	a	k8xC	a
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
do	do	k7c2	do
hudby	hudba	k1gFnSc2	hudba
přinášejí	přinášet	k5eAaImIp3nP	přinášet
svébytné	svébytný	k2eAgInPc4d1	svébytný
<g/>
,	,	kIx,	,
originální	originální	k2eAgInPc4d1	originální
a	a	k8xC	a
novátorské	novátorský	k2eAgInPc4d1	novátorský
elementy	element	k1gInPc4	element
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
napříč	napříč	k7c7	napříč
žánry	žánr	k1gInPc7	žánr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnPc1d1	hudební
ceny	cena	k1gFnPc1	cena
Vinyla	Vinyla	k1gFnPc2	Vinyla
jsou	být	k5eAaImIp3nP	být
udělovány	udělovat	k5eAaImNgInP	udělovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
:	:	kIx,	:
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
Počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
udílení	udílení	k1gNnSc1	udílení
cen	cena	k1gFnPc2	cena
vychází	vycházet	k5eAaImIp3nS	vycházet
výroční	výroční	k2eAgInSc4d1	výroční
vinyl	vinyl	k1gInSc4	vinyl
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
100	[number]	k4	100
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skladby	skladba	k1gFnPc4	skladba
nominovaných	nominovaný	k2eAgMnPc2d1	nominovaný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Obal	obal	k1gInSc1	obal
vinylu	vinyl	k1gInSc2	vinyl
vzniká	vznikat	k5eAaImIp3nS	vznikat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
českými	český	k2eAgMnPc7d1	český
výtvarníky	výtvarník	k1gMnPc7	výtvarník
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
vinyl	vinyl	k1gInSc1	vinyl
<g/>
,	,	kIx,	,
autorsky	autorsky	k6eAd1	autorsky
dotvořený	dotvořený	k2eAgMnSc1d1	dotvořený
<g/>
,	,	kIx,	,
také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
ocenění	ocenění	k1gNnSc1	ocenění
(	(	kIx(	(
<g/>
artefakt	artefakt	k1gInSc1	artefakt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ceny	cena	k1gFnPc1	cena
Vinyla	Vinyla	k1gFnPc2	Vinyla
produkčně	produkčně	k6eAd1	produkčně
a	a	k8xC	a
koordinačně	koordinačně	k6eAd1	koordinačně
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Goliart	Goliart	k1gInSc1	Goliart
<g/>
,	,	kIx,	,
o.s.	o.s.	k?	o.s.
Hlavním	hlavní	k2eAgInSc7d1	hlavní
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
Rada	rada	k1gFnSc1	rada
ceny	cena	k1gFnSc2	cena
Vinyla	Vinyla	k1gFnSc2	Vinyla
<g/>
,	,	kIx,	,
čtyřčlenný	čtyřčlenný	k2eAgInSc4d1	čtyřčlenný
sbor	sbor	k1gInSc4	sbor
aktivních	aktivní	k2eAgMnPc2d1	aktivní
a	a	k8xC	a
respektovaných	respektovaný	k2eAgMnPc2d1	respektovaný
hudebních	hudební	k2eAgMnPc2d1	hudební
publicistů	publicista	k1gMnPc2	publicista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Radě	rada	k1gFnSc6	rada
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
jeden	jeden	k4xCgMnSc1	jeden
člen	člen	k1gMnSc1	člen
obmění	obměnit	k5eAaPmIp3nS	obměnit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
má	mít	k5eAaImIp3nS	mít
zajistit	zajistit	k5eAaPmF	zajistit
myšlenkovou	myšlenkový	k2eAgFnSc4d1	myšlenková
kontinuitu	kontinuita	k1gFnSc4	kontinuita
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
učinit	učinit	k5eAaPmF	učinit
cenu	cena	k1gFnSc4	cena
dostatečně	dostatečně	k6eAd1	dostatečně
dynamickou	dynamický	k2eAgFnSc7d1	dynamická
a	a	k8xC	a
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
novým	nový	k2eAgInPc3d1	nový
impulzům	impulz	k1gInPc3	impulz
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
rady	rada	k1gFnSc2	rada
je	být	k5eAaImIp3nS	být
delegovat	delegovat	k5eAaBmF	delegovat
porotu	porota	k1gFnSc4	porota
a	a	k8xC	a
dohlížet	dohlížet	k5eAaImF	dohlížet
průběh	průběh	k1gInSc4	průběh
udílení	udílení	k1gNnSc2	udílení
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
porotce	porotce	k1gMnSc1	porotce
(	(	kIx(	(
<g/>
hlasovatel	hlasovatel	k1gMnSc1	hlasovatel
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
každý	každý	k3xTgInSc4	každý
aktivní	aktivní	k2eAgFnSc1d1	aktivní
hudební	hudební	k2eAgFnSc1d1	hudební
publicita	publicita	k1gFnSc1	publicita
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
českou	český	k2eAgFnSc4d1	Česká
scénu	scéna	k1gFnSc4	scéna
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zajímá	zajímat	k5eAaImIp3nS	zajímat
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
formovat	formovat	k5eAaImF	formovat
myšlení	myšlení	k1gNnSc4	myšlení
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Držitelé	držitel	k1gMnPc1	držitel
ocenění	oceněný	k2eAgMnPc1d1	oceněný
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vinyla	Vinyla	k1gFnPc2	Vinyla
2011	[number]	k4	2011
===	===	k?	===
</s>
</p>
<p>
<s>
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
B4	B4	k1gMnSc1	B4
–	–	k?	–
Didaktik	didaktik	k1gMnSc1	didaktik
Nation	Nation	k1gInSc4	Nation
Legendary	Legendara	k1gFnSc2	Legendara
Rock	rock	k1gInSc1	rock
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Fiordmoss	Fiordmoss	k1gInSc1	Fiordmoss
</s>
</p>
<p>
<s>
Počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Festival	festival	k1gInSc1	festival
Creepy	Creepa	k1gFnSc2	Creepa
Teepee	Teepe	k1gFnSc2	Teepe
</s>
</p>
<p>
<s>
===	===	k?	===
Vinyla	Vinyla	k1gFnPc2	Vinyla
2012	[number]	k4	2012
===	===	k?	===
</s>
</p>
<p>
<s>
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Květy	Květa	k1gFnPc1	Květa
–	–	k?	–
Bílé	bílý	k2eAgFnPc1d1	bílá
včely	včela	k1gFnPc1	včela
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Planety	planeta	k1gFnPc1	planeta
</s>
</p>
<p>
<s>
Počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
vydavatelské	vydavatelský	k2eAgFnSc2d1	vydavatelská
aktivity	aktivita	k1gFnSc2	aktivita
Polí	pole	k1gFnSc7	pole
<g/>
5	[number]	k4	5
</s>
</p>
<p>
<s>
===	===	k?	===
Vinyla	Vinyla	k1gFnPc2	Vinyla
2013	[number]	k4	2013
===	===	k?	===
</s>
</p>
<p>
<s>
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Vložte	vložit	k5eAaPmRp2nP	vložit
kočku	kočka	k1gFnSc4	kočka
–	–	k?	–
SEAT	SEAT	kA	SEAT
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
NYLON	nylon	k1gInSc1	nylon
JAIL	JAIL	kA	JAIL
</s>
</p>
<p>
<s>
Počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Piana	piano	k1gNnPc4	piano
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
</s>
</p>
<p>
<s>
===	===	k?	===
Vinyla	Vinyla	k1gFnPc2	Vinyla
2014	[number]	k4	2014
===	===	k?	===
</s>
</p>
<p>
<s>
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
DVA	dva	k4xCgMnPc1	dva
–	–	k?	–
Nipomo	Nipoma	k1gFnSc5	Nipoma
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
–	–	k?	–
Schwarzprior	Schwarzprior	k1gMnSc1	Schwarzprior
</s>
</p>
<p>
<s>
Počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
–	–	k?	–
Studio	studio	k1gNnSc1	studio
Needles	Needlesa	k1gFnPc2	Needlesa
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
audioknize	audiokniza	k1gFnSc3	audiokniza
Solaris	Solaris	k1gFnSc2	Solaris
</s>
</p>
<p>
<s>
===	===	k?	===
Vinyla	Vinyla	k1gFnPc2	Vinyla
2015	[number]	k4	2015
===	===	k?	===
</s>
</p>
<p>
<s>
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Dizzcock	Dizzcock	k1gInSc1	Dizzcock
–	–	k?	–
Elegy	Elega	k1gFnSc2	Elega
Of	Of	k1gMnSc1	Of
Unsung	Unsung	k1gMnSc1	Unsung
Heroes	Heroes	k1gMnSc1	Heroes
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
–	–	k?	–
Aid	Aida	k1gFnPc2	Aida
Kid	Kid	k1gFnSc1	Kid
</s>
</p>
<p>
<s>
Počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
–	–	k?	–
kompilace	kompilace	k1gFnSc2	kompilace
Jdi	jít	k5eAaImRp2nS	jít
a	a	k8xC	a
dívej	dívat	k5eAaImRp2nS	dívat
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
===	===	k?	===
Vinyla	Vinyla	k1gFnPc2	Vinyla
2016	[number]	k4	2016
===	===	k?	===
</s>
</p>
<p>
<s>
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
dné	dné	k?	dné
–	–	k?	–
These	these	k1gFnSc2	these
Semi	Semi	k1gNnSc2	Semi
Feelings	Feelingsa	k1gFnPc2	Feelingsa
<g/>
,	,	kIx,	,
They	Thea	k1gFnSc2	Thea
Are	ar	k1gInSc5	ar
Everywhere	Everywher	k1gMnSc5	Everywher
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
–	–	k?	–
Orient	Orient	k1gInSc1	Orient
</s>
</p>
<p>
<s>
Počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
–	–	k?	–
festival	festival	k1gInSc1	festival
Itch	Itch	k1gInSc1	Itch
My	my	k3xPp1nPc1	my
Ha	ha	kA	ha
Ha	ha	kA	ha
Ha	ha	kA	ha
</s>
</p>
<p>
<s>
===	===	k?	===
Vinyla	Vinyla	k1gFnPc2	Vinyla
2017	[number]	k4	2017
===	===	k?	===
</s>
</p>
<p>
<s>
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Pacino	Pacino	k1gNnSc1	Pacino
–	–	k?	–
Půl	půl	k6eAd1	půl
litru	litr	k1gInSc2	litr
země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
–	–	k?	–
Enchanted	Enchanted	k1gInSc1	Enchanted
Lands	Lands	k1gInSc1	Lands
</s>
</p>
<p>
<s>
Počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
–	–	k?	–
aktivity	aktivita	k1gFnSc2	aktivita
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Genot	Genota	k1gFnPc2	Genota
Centre	centr	k1gInSc5	centr
</s>
</p>
<p>
<s>
===	===	k?	===
Vinyla	Vinyla	k1gFnPc2	Vinyla
2018	[number]	k4	2018
===	===	k?	===
</s>
</p>
<p>
<s>
Deska	deska	k1gFnSc1	deska
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
Povodí	povodí	k1gNnSc1	povodí
Ohře	Ohře	k1gFnSc2	Ohře
–	–	k?	–
Povodí	povodí	k1gNnSc1	povodí
Ohře	Ohře	k1gFnSc2	Ohře
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
roku	rok	k1gInSc2	rok
–	–	k?	–
Bílej	Bílej	k?	Bílej
kluk	kluk	k1gMnSc1	kluk
</s>
</p>
<p>
<s>
Počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
–	–	k?	–
aktivity	aktivita	k1gFnSc2	aktivita
Zvuk	zvuk	k1gInSc4	zvuk
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Vinyla	Vinýt	k5eAaPmAgFnS	Vinýt
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
Ceny	cena	k1gFnPc1	cena
Vinyla	Vinyla	k1gFnPc2	Vinyla
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jste	být	k5eAaImIp2nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
neznali	znát	k5eNaImAgMnP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Seznamte	seznámit	k5eAaPmRp2nP	seznámit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
iHNED	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
11.3	[number]	k4	11.3
<g/>
.2012	.2012	k4	.2012
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnPc1d1	hudební
ceny	cena	k1gFnPc1	cena
Vinyla	Vinyla	k1gFnPc2	Vinyla
poprvé	poprvé	k6eAd1	poprvé
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc1	radio
Wave	Wave	k1gFnPc2	Wave
<g/>
,	,	kIx,	,
7.3	[number]	k4	7.3
<g/>
.2012	.2012	k4	.2012
</s>
</p>
<p>
<s>
Včera	včera	k6eAd1	včera
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
hudební	hudební	k2eAgFnSc1d1	hudební
cena	cena	k1gFnSc1	cena
Vinyla	Vinyla	k1gFnSc2	Vinyla
<g/>
,	,	kIx,	,
Literární	literární	k2eAgFnPc4d1	literární
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
11.3	[number]	k4	11.3
<g/>
.2012	.2012	k4	.2012
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
hudební	hudební	k2eAgFnPc1d1	hudební
ceny	cena	k1gFnPc1	cena
nechtějí	chtít	k5eNaImIp3nP	chtít
porcovat	porcovat	k5eAaImF	porcovat
komerčního	komerční	k2eAgMnSc4d1	komerční
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
,	,	kIx,	,
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
19.1	[number]	k4	19.1
<g/>
.2012	.2012	k4	.2012
</s>
</p>
