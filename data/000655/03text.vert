<p>
<s>
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
a	a	k8xC	a
latinsky	latinsky	k6eAd1	latinsky
Roma	Rom	k1gMnSc4	Rom
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
též	též	k9	též
Věčné	věčný	k2eAgNnSc1d1	věčné
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnSc2	oblast
Lazio	Lazio	k6eAd1	Lazio
a	a	k8xC	a
provincie	provincie	k1gFnSc1	provincie
Roma	Rom	k1gMnSc2	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Tibeře	Tibera	k1gFnSc6	Tibera
asi	asi	k9	asi
27	[number]	k4	27
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
2,873	[number]	k4	2,873
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
italským	italský	k2eAgInSc7d1	italský
a	a	k8xC	a
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
politickým	politický	k2eAgInSc7d1	politický
<g/>
,	,	kIx,	,
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
a	a	k8xC	a
kulturním	kulturní	k2eAgInSc7d1	kulturní
centrem	centr	k1gInSc7	centr
mimořádně	mimořádně	k6eAd1	mimořádně
bohatým	bohatý	k2eAgInSc7d1	bohatý
na	na	k7c4	na
umělecké	umělecký	k2eAgFnPc4d1	umělecká
a	a	k8xC	a
historické	historický	k2eAgFnPc4d1	historická
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2 700	[number]	k4	2 700
lety	let	k1gInPc7	let
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
římské	římský	k2eAgFnSc2d1	římská
tradice	tradice	k1gFnSc2	tradice
roku	rok	k1gInSc2	rok
753	[number]	k4	753
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgNnPc3d3	nejstarší
městům	město	k1gNnPc3	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
byl	být	k5eAaImAgInS	být
Řím	Řím	k1gInSc1	Řím
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc2d3	veliký
evropské	evropský	k2eAgFnSc2d1	Evropská
velmoci	velmoc	k1gFnSc2	velmoc
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
100	[number]	k4	100
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
1,6	[number]	k4	1,6
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
centrem	centrum	k1gNnSc7	centrum
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
římský	římský	k2eAgInSc1d1	římský
jazyk	jazyk	k1gInSc1	jazyk
latina	latina	k1gFnSc1	latina
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
mnoha	mnoho	k4c2	mnoho
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
římské	římský	k2eAgNnSc1d1	římské
právo	právo	k1gNnSc1	právo
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
právních	právní	k2eAgInPc2d1	právní
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
původně	původně	k6eAd1	původně
římské	římský	k2eAgNnSc4d1	římské
písmo	písmo	k1gNnSc4	písmo
se	se	k3xPyFc4	se
jako	jako	k9	jako
latinka	latinka	k1gFnSc1	latinka
stalo	stát	k5eAaPmAgNnS	stát
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
písmem	písmo	k1gNnSc7	písmo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
čtyři	čtyři	k4xCgInPc4	čtyři
státní	státní	k2eAgInPc4d1	státní
<g/>
,	,	kIx,	,
šestnáct	šestnáct	k4xCc4	šestnáct
soukromých	soukromý	k2eAgInPc2d1	soukromý
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
papežských	papežský	k2eAgFnPc2d1	Papežská
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
italská	italský	k2eAgFnSc1d1	italská
Akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnPc1d1	hlavní
italská	italský	k2eAgNnPc1d1	italské
média	médium	k1gNnPc1	médium
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc4	sídlo
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnSc2	hlava
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
nazývané	nazývaný	k2eAgFnSc6d1	nazývaná
Campagna	Campagno	k1gNnPc1	Campagno
di	di	k?	di
Roma	Rom	k1gMnSc2	Rom
<g/>
,	,	kIx,	,
27	[number]	k4	27
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Tyrhénského	tyrhénský	k2eAgNnSc2d1	Tyrhénské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historické	historický	k2eAgNnSc1d1	historické
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
na	na	k7c4	na
"	"	kIx"	"
<g/>
sedmi	sedm	k4xCc6	sedm
pahorcích	pahorek	k1gInPc6	pahorek
<g/>
"	"	kIx"	"
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Tibery	Tibera	k1gFnSc2	Tibera
(	(	kIx(	(
<g/>
Tevere	Tever	k1gInSc5	Tever
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Kapitol	Kapitol	k1gInSc1	Kapitol
<g/>
,	,	kIx,	,
lat	lat	k1gInSc1	lat
<g/>
.	.	kIx.	.
</s>
<s>
Capitolium	Capitolium	k1gNnSc1	Capitolium
<g/>
,	,	kIx,	,
it	it	k?	it
<g/>
.	.	kIx.	.
</s>
<s>
Campidoglio	Campidoglio	k6eAd1	Campidoglio
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Aventin	Aventin	k1gInSc1	Aventin
<g/>
,	,	kIx,	,
lat	lat	k1gInSc1	lat
<g/>
.	.	kIx.	.
</s>
<s>
Aventinus	Aventinus	k1gMnSc1	Aventinus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Palatin	Palatin	k1gInSc1	Palatin
<g/>
,	,	kIx,	,
lat	lat	k1gInSc1	lat
<g/>
.	.	kIx.	.
</s>
<s>
Palatinus	Palatinus	k1gMnSc1	Palatinus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Caelius	Caelius	k1gMnSc1	Caelius
</s>
</p>
<p>
<s>
Kvirinál	Kvirinál	k1gInSc1	Kvirinál
<g/>
,	,	kIx,	,
<g/>
lat	lat	k1gInSc1	lat
<g/>
.	.	kIx.	.
</s>
<s>
Quirinalis	Quirinalis	k1gFnSc1	Quirinalis
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
sídlo	sídlo	k1gNnSc1	sídlo
italského	italský	k2eAgMnSc2d1	italský
presidenta	president	k1gMnSc2	president
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Viminál	Viminál	k1gInSc1	Viminál
<g/>
,	,	kIx,	,
lat	lat	k1gInSc1	lat
<g/>
.	.	kIx.	.
</s>
<s>
Viminalis	Viminalis	k1gFnSc1	Viminalis
a	a	k8xC	a
</s>
</p>
<p>
<s>
Esquilin	Esquilin	k1gInSc1	Esquilin
<g/>
,	,	kIx,	,
lat	lat	k1gInSc1	lat
<g/>
.	.	kIx.	.
</s>
<s>
Esquilinus	Esquilinus	k1gInSc1	Esquilinus
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
pahorky	pahorek	k1gInPc4	pahorek
protékalo	protékat	k5eAaImAgNnS	protékat
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
několik	několik	k4yIc4	několik
říček	říčka	k1gFnPc2	říčka
tvořících	tvořící	k2eAgFnPc2d1	tvořící
bažiny	bažina	k1gFnSc2	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
bažinou	bažina	k1gFnSc7	bažina
byla	být	k5eAaImAgFnS	být
Caprea	Caprea	k1gFnSc1	Caprea
palus	palus	k1gInSc4	palus
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
říčky	říčka	k1gFnSc2	říčka
Petronia	Petronium	k1gNnSc2	Petronium
do	do	k7c2	do
Tibery	Tibera	k1gFnSc2	Tibera
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
města	město	k1gNnSc2	město
začínají	začínat	k5eAaImIp3nP	začínat
Abruzzi	Abruzze	k1gFnSc4	Abruzze
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Sabinské	Sabinský	k2eAgFnSc2d1	Sabinský
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Albánské	albánský	k2eAgFnSc2d1	albánská
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
dějinami	dějiny	k1gFnPc7	dějiny
se	se	k3xPyFc4	se
návštěvník	návštěvník	k1gMnSc1	návštěvník
Říma	Řím	k1gInSc2	Řím
setkává	setkávat	k5eAaImIp3nS	setkávat
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
kroku	krok	k1gInSc6	krok
a	a	k8xC	a
dochované	dochovaný	k2eAgFnPc1d1	dochovaná
památky	památka	k1gFnPc1	památka
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgNnPc4	dva
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spojením	spojení	k1gNnSc7	spojení
několika	několik	k4yIc2	několik
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
roku	rok	k1gInSc2	rok
753	[number]	k4	753
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
7.	[number]	k4	7.
století	století	k1gNnSc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
mu	on	k3xPp3gMnSc3	on
vládli	vládnout	k5eAaImAgMnP	vládnout
etruští	etruský	k2eAgMnPc1d1	etruský
králové	král	k1gMnPc1	král
<g/>
;	;	kIx,	;
název	název	k1gInSc1	název
Roma	Rom	k1gMnSc2	Rom
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
podle	podle	k7c2	podle
etruského	etruský	k2eAgInSc2d1	etruský
rodu	rod	k1gInSc2	rod
Ruma	Rum	k1gInSc2	Rum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
509	[number]	k4	509
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
králové	král	k1gMnPc1	král
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
římská	římský	k2eAgFnSc1d1	římská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
4.	[number]	k4	4.
století	století	k1gNnPc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
dostalo	dostat	k5eAaPmAgNnS	dostat
město	město	k1gNnSc1	město
první	první	k4xOgFnSc4	první
kamennou	kamenný	k2eAgFnSc4d1	kamenná
hradbu	hradba	k1gFnSc4	hradba
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
270	[number]	k4	270
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
republika	republika	k1gFnSc1	republika
postupně	postupně	k6eAd1	postupně
zabrala	zabrat	k5eAaPmAgFnS	zabrat
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
Apeninského	apeninský	k2eAgInSc2d1	apeninský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
pak	pak	k6eAd1	pak
obrátila	obrátit	k5eAaPmAgFnS	obrátit
pozornost	pozornost	k1gFnSc4	pozornost
k	k	k7c3	k
zámořským	zámořský	k2eAgFnPc3d1	zámořská
državám	država	k1gFnPc3	država
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
1.	[number]	k4	1.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Rozpínající	rozpínající	k2eAgFnSc1d1	rozpínající
se	se	k3xPyFc4	se
říše	říše	k1gFnSc1	říše
nabízela	nabízet	k5eAaImAgFnS	nabízet
příležitosti	příležitost	k1gFnPc4	příležitost
mocichtivým	mocichtivý	k2eAgMnPc3d1	mocichtivý
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
a	a	k8xC	a
konflikty	konflikt	k1gInPc1	konflikt
mezi	mezi	k7c7	mezi
silnými	silný	k2eAgFnPc7d1	silná
osobnostmi	osobnost	k1gFnPc7	osobnost
vedly	vést	k5eAaImAgFnP	vést
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
republikánského	republikánský	k2eAgNnSc2d1	republikánské
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
říši	říše	k1gFnSc4	říše
vládl	vládnout	k5eAaImAgMnS	vládnout
diktátorsky	diktátorsky	k6eAd1	diktátorsky
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
synovec	synovec	k1gMnSc1	synovec
Octavianus	Octavianus	k1gMnSc1	Octavianus
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
přijal	přijmout	k5eAaPmAgMnS	přijmout
titul	titul	k1gInSc4	titul
Augustus	Augustus	k1gMnSc1	Augustus
(	(	kIx(	(
<g/>
Vznešený	vznešený	k2eAgMnSc1d1	vznešený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Augusta	August	k1gMnSc2	August
se	se	k3xPyFc4	se
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
budovaného	budovaný	k2eAgNnSc2d1	budované
dosud	dosud	k6eAd1	dosud
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
reprezentativní	reprezentativní	k2eAgInSc4d1	reprezentativní
střed	střed	k1gInSc4	střed
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgFnP	vznikat
veřejné	veřejný	k2eAgFnPc1d1	veřejná
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
fóra	fórum	k1gNnPc1	fórum
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
cirky	cirk	k1gInPc1	cirk
<g/>
,	,	kIx,	,
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
hlavním	hlavní	k2eAgInSc7d1	hlavní
stavebním	stavební	k2eAgInSc7d1	stavební
materiálem	materiál	k1gInSc7	materiál
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
mramor	mramor	k1gInSc1	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
275	[number]	k4	275
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
rozloze	rozloha	k1gFnSc6	rozloha
13,7	[number]	k4	13,7
km2	km2	k4	km2
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
Aurelianovou	Aurelianový	k2eAgFnSc7d1	Aurelianový
hradbou	hradba	k1gFnSc7	hradba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Augustovy	Augustův	k2eAgFnSc2d1	Augustova
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Ježíš	Ježíš	k1gMnSc1	Ježíš
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
křesťané	křesťan	k1gMnPc1	křesťan
byli	být	k5eAaImAgMnP	být
až	až	k6eAd1	až
do	do	k7c2	do
4.	[number]	k4	4.
století	století	k1gNnSc2	století
pronásledováni	pronásledován	k2eAgMnPc1d1	pronásledován
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc1d1	nové
náboženství	náboženství	k1gNnSc1	náboženství
se	se	k3xPyFc4	se
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
a	a	k8xC	a
Řím	Řím	k1gInSc4	Řím
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
centrem	centr	k1gMnSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
3.	[number]	k4	3.
a	a	k8xC	a
4.	[number]	k4	4.
století	století	k1gNnSc6	století
ovšem	ovšem	k9	ovšem
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
význam	význam	k1gInSc4	význam
Říma	Řím	k1gInSc2	Řím
jako	jako	k8xC	jako
politického	politický	k2eAgNnSc2d1	politické
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
císařové	císař	k1gMnPc1	císař
upřednostňovali	upřednostňovat	k5eAaImAgMnP	upřednostňovat
jako	jako	k9	jako
svá	svůj	k3xOyFgNnPc4	svůj
sídla	sídlo	k1gNnPc4	sídlo
jiná	jiný	k2eAgNnPc4d1	jiné
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
přenesl	přenést	k5eAaPmAgInS	přenést
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
na	na	k7c4	na
východ	východ	k1gInSc4	východ
do	do	k7c2	do
nově	nově	k6eAd1	nově
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
města	město	k1gNnSc2	město
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
I.	I.	kA	I.
Veliký	veliký	k2eAgInSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
sice	sice	k8xC	sice
nadále	nadále	k6eAd1	nadále
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
sídlem	sídlo	k1gNnSc7	sídlo
senátu	senát	k1gInSc2	senát
a	a	k8xC	a
legislativním	legislativní	k2eAgNnSc7d1	legislativní
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
upadajícím	upadající	k2eAgInSc7d1	upadající
vlivem	vliv	k1gInSc7	vliv
upadalo	upadat	k5eAaImAgNnS	upadat
i	i	k9	i
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
410	[number]	k4	410
město	město	k1gNnSc4	město
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
plenili	plenit	k5eAaImAgMnP	plenit
Alarichovi	Alarichův	k2eAgMnPc1d1	Alarichův
Vizigóti	Vizigót	k1gMnPc1	Vizigót
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
455	[number]	k4	455
pak	pak	k6eAd1	pak
Vandalové	Vandal	k1gMnPc1	Vandal
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Geisericha	Geiserich	k1gMnSc2	Geiserich
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
klesl	klesnout	k5eAaPmAgInS	klesnout
z	z	k7c2	z
1,6	[number]	k4	1,6
milionu	milion	k4xCgInSc2	milion
až	až	k6eAd1	až
na	na	k7c4	na
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
město	město	k1gNnSc4	město
obnovili	obnovit	k5eAaPmAgMnP	obnovit
papežové	papež	k1gMnPc1	papež
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Lev	lev	k1gInSc1	lev
I.	I.	kA	I.
Veliký	veliký	k2eAgInSc1d1	veliký
(	(	kIx(	(
<g/>
†	†	k?	†
461	[number]	k4	461
<g/>
)	)	kIx)	)
a	a	k8xC	a
Řehoř	Řehoř	k1gMnSc1	Řehoř
I.	I.	kA	I.
Veliký	veliký	k2eAgInSc1d1	veliký
(	(	kIx(	(
<g/>
†	†	k?	†
604	[number]	k4	604
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
vpádu	vpád	k1gInSc6	vpád
Vandalů	Vandal	k1gMnPc2	Vandal
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pobořili	pobořit	k5eAaPmAgMnP	pobořit
mnoho	mnoho	k4c4	mnoho
starověkých	starověký	k2eAgFnPc2d1	starověká
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
852	[number]	k4	852
rozšířeny	rozšířen	k2eAgFnPc1d1	rozšířena
městské	městský	k2eAgFnPc1d1	městská
hradby	hradba	k1gFnPc1	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
9.	[number]	k4	9.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
opravovaly	opravovat	k5eAaImAgInP	opravovat
a	a	k8xC	a
stavěly	stavět	k5eAaImAgInP	stavět
nové	nový	k2eAgInPc1d1	nový
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
poutníkům	poutník	k1gMnPc3	poutník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
však	však	k9	však
žilo	žít	k5eAaImAgNnS	žít
jen	jen	k9	jen
asi	asi	k9	asi
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
papežským	papežský	k2eAgNnSc7d1	papežské
sídlem	sídlo	k1gNnSc7	sídlo
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
let	léto	k1gNnPc2	léto
1309	[number]	k4	1309
<g/>
–	–	k?	–
<g/>
1377	[number]	k4	1377
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
papežové	papež	k1gMnPc1	papež
sídlili	sídlit	k5eAaImAgMnP	sídlit
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Avignonu	Avignon	k1gInSc6	Avignon
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Avignonské	avignonský	k2eAgNnSc1d1	avignonské
zajetí	zajetí	k1gNnSc1	zajetí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
odešel	odejít	k5eAaPmAgMnS	odejít
i	i	k9	i
jejich	jejich	k3xOp3gInSc1	jejich
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1378	[number]	k4	1378
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
papežové	papež	k1gMnPc1	papež
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
,	,	kIx,	,
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
ruinách	ruina	k1gFnPc6	ruina
Říma	Řím	k1gInSc2	Řím
sotva	sotva	k6eAd1	sotva
20 000	[number]	k4	20 000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
postavení	postavení	k1gNnSc1	postavení
centra	centrum	k1gNnSc2	centrum
západního	západní	k2eAgNnSc2d1	západní
křesťanství	křesťanství	k1gNnSc2	křesťanství
si	se	k3xPyFc3	se
Řím	Řím	k1gInSc1	Řím
znovu	znovu	k6eAd1	znovu
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
15.	[number]	k4	15.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
za	za	k7c4	za
papeže	papež	k1gMnSc4	papež
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
V.	V.	kA	V.
začala	začít	k5eAaPmAgFnS	začít
nákladná	nákladný	k2eAgFnSc1d1	nákladná
přestavba	přestavba	k1gFnSc1	přestavba
města	město	k1gNnSc2	město
v	v	k7c6	v
renesančním	renesanční	k2eAgMnSc6d1	renesanční
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1506	[number]	k4	1506
začala	začít	k5eAaPmAgFnS	začít
stavba	stavba	k1gFnSc1	stavba
nové	nový	k2eAgFnSc2d1	nová
baziliky	bazilika	k1gFnSc2	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
podle	podle	k7c2	podle
Bramanteho	Bramante	k1gMnSc2	Bramante
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
podíleli	podílet	k5eAaImAgMnP	podílet
například	například	k6eAd1	například
Raffael	Raffael	k1gInSc4	Raffael
Santi	Sanť	k1gFnSc2	Sanť
<g/>
,	,	kIx,	,
Michelangelo	Michelangela	k1gFnSc5	Michelangela
Buonarroti	Buonarrot	k1gMnPc1	Buonarrot
nebo	nebo	k8xC	nebo
Gian	Gian	k1gNnSc1	Gian
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Bernini	Bernin	k2eAgMnPc5d1	Bernin
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1527	[number]	k4	1527
město	město	k1gNnSc4	město
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
císařští	císařský	k2eAgMnPc1d1	císařský
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
obnova	obnova	k1gFnSc1	obnova
v	v	k7c6	v
barokním	barokní	k2eAgMnSc6d1	barokní
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1629	[number]	k4	1629
byla	být	k5eAaImAgFnS	být
bazilika	bazilika	k1gFnSc1	bazilika
vysvěcena	vysvětit	k5eAaPmNgFnS	vysvětit
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
starších	starý	k2eAgFnPc2d2	starší
staveb	stavba	k1gFnPc2	stavba
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
obnovena	obnovit	k5eAaPmNgFnS	obnovit
a	a	k8xC	a
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
množství	množství	k1gNnSc3	množství
nových	nový	k2eAgMnPc2d1	nový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
sjednocování	sjednocování	k1gNnSc2	sjednocování
Itálie	Itálie	k1gFnSc2	Itálie
v	v	k7c4	v
19.	[number]	k4	19.
století	století	k1gNnPc2	století
dobyla	dobýt	k5eAaPmAgFnS	dobýt
italská	italský	k2eAgFnSc1d1	italská
armáda	armáda	k1gFnSc1	armáda
většinu	většina	k1gFnSc4	většina
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
i	i	k9	i
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
1871	[number]	k4	1871
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
začal	začít	k5eAaPmAgInS	začít
prudký	prudký	k2eAgInSc4d1	prudký
nárůst	nárůst	k1gInSc4	nárůst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
a	a	k8xC	a
modernizace	modernizace	k1gFnSc2	modernizace
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
Pochodu	pochod	k1gInSc6	pochod
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
<g/>
,	,	kIx,	,
se	s	k7c7	s
premiérem	premiér	k1gMnSc7	premiér
stal	stát	k5eAaPmAgMnS	stát
Benito	Benita	k1gMnSc5	Benita
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
vládl	vládnout	k5eAaImAgMnS	vládnout
jako	jako	k9	jako
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Lateránskou	lateránský	k2eAgFnSc7d1	Lateránská
smlouvou	smlouva	k1gFnSc7	smlouva
jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
Vatikán	Vatikán	k1gInSc1	Vatikán
a	a	k8xC	a
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
Itálie	Itálie	k1gFnSc1	Itálie
stala	stát	k5eAaPmAgFnS	stát
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Římskými	římský	k2eAgFnPc7d1	římská
smlouvami	smlouva	k1gFnPc7	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Evropské	evropský	k2eAgNnSc1d1	Evropské
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
společenství	společenství	k1gNnSc1	společenství
(	(	kIx(	(
<g/>
EHS	EHS	kA	EHS
<g/>
)	)	kIx)	)
a	a	k8xC	a
Euratom	Euratom	k1gInSc1	Euratom
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jejich	jejich	k3xOp3gMnPc7	jejich
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
konaly	konat	k5eAaImAgFnP	konat
letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
druhý	druhý	k4xOgInSc4	druhý
vatikánský	vatikánský	k2eAgInSc4d1	vatikánský
koncil	koncil	k1gInSc4	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
je	být	k5eAaImIp3nS	být
staré	starý	k2eAgNnSc1d1	staré
město	město	k1gNnSc1	město
a	a	k8xC	a
Vatikán	Vatikán	k1gInSc1	Vatikán
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
jazyka	jazyk	k1gInSc2	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Původním	původní	k2eAgInSc7d1	původní
jazykem	jazyk	k1gInSc7	jazyk
Římanů	Říman	k1gMnPc2	Říman
byla	být	k5eAaImAgFnS	být
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
v	v	k7c4	v
italštinu	italština	k1gFnSc4	italština
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
románské	románský	k2eAgInPc4d1	románský
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
i	i	k9	i
další	další	k2eAgInPc4d1	další
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
jazyka	jazyk	k1gInSc2	jazyk
ovlivnily	ovlivnit	k5eAaPmAgInP	ovlivnit
různé	různý	k2eAgInPc1d1	různý
regionální	regionální	k2eAgInPc1d1	regionální
dialekty	dialekt	k1gInPc1	dialekt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
římské	římský	k2eAgNnSc1d1	římské
nářečí	nářečí	k1gNnSc1	nářečí
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Romanesco	Romanesco	k6eAd1	Romanesco
neboli	neboli	k8xC	neboli
římština	římština	k1gFnSc1	římština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
spíše	spíše	k9	spíše
jihoitalským	jihoitalský	k2eAgNnSc7d1	jihoitalské
nářečím	nářečí	k1gNnSc7	nářečí
<g/>
,	,	kIx,	,
blízkým	blízký	k2eAgNnSc7d1	blízké
neapolštině	neapolština	k1gFnSc3	neapolština
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
florentské	florentský	k2eAgFnSc2d1	florentská
kultury	kultura	k1gFnSc2	kultura
během	během	k7c2	během
renesance	renesance	k1gFnSc2	renesance
způsobil	způsobit	k5eAaPmAgMnS	způsobit
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
nářečí	nářečí	k1gNnSc6	nářečí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
více	hodně	k6eAd2	hodně
podobat	podobat	k5eAaImF	podobat
toskánštině	toskánština	k1gFnSc3	toskánština
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
19.	[number]	k4	19.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
omezily	omezit	k5eAaPmAgFnP	omezit
na	na	k7c4	na
hranice	hranice	k1gFnPc4	hranice
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
Lazia	Lazium	k1gNnSc2	Lazium
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc7d1	dnešní
římštinou	římština	k1gFnSc7	římština
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
i	i	k9	i
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
města	město	k1gNnSc2	město
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
standardní	standardní	k2eAgFnSc1d1	standardní
italštině	italština	k1gFnSc3	italština
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
odlišná	odlišný	k2eAgNnPc1d1	odlišné
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
nářečí	nářečí	k1gNnPc2	nářečí
Lazia	Lazium	k1gNnSc2	Lazium
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
psaná	psaný	k2eAgFnSc1d1	psaná
římským	římský	k2eAgNnSc7d1	římské
nářečím	nářečí	k1gNnSc7	nářečí
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
díla	dílo	k1gNnPc4	dílo
takových	takový	k3xDgMnPc2	takový
autorů	autor	k1gMnPc2	autor
jako	jako	k8xS	jako
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Gioachino	Gioachina	k1gMnSc5	Gioachina
Belli	Bell	k1gMnSc5	Bell
<g/>
,	,	kIx,	,
Trilussa	Trilussa	k1gFnSc1	Trilussa
a	a	k8xC	a
Cesare	Cesar	k1gMnSc5	Cesar
Pascarella	Pascarello	k1gNnPc4	Pascarello
<g/>
.	.	kIx.	.
</s>
<s>
Soudobou	soudobý	k2eAgFnSc4d1	soudobá
římštinu	římština	k1gFnSc4	římština
představují	představovat	k5eAaImIp3nP	představovat
populární	populární	k2eAgMnPc1d1	populární
herci	herec	k1gMnPc1	herec
jako	jako	k8xC	jako
Aldo	Aldo	k6eAd1	Aldo
Fabrizi	Fabrih	k1gMnPc1	Fabrih
<g/>
,	,	kIx,	,
Alberto	Alberta	k1gFnSc5	Alberta
Sordi	Sord	k1gMnPc1	Sord
<g/>
,	,	kIx,	,
Nino	Nina	k1gFnSc5	Nina
Manfredi	Manfred	k1gMnPc1	Manfred
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Magnaniová	Magnaniový	k2eAgFnSc1d1	Magnaniová
<g/>
,	,	kIx,	,
Grigi	Grigi	k1gNnSc1	Grigi
Proietti	Proietť	k1gFnSc2	Proietť
<g/>
,	,	kIx,	,
Enrico	Enrico	k6eAd1	Enrico
Montesano	Montesana	k1gFnSc5	Montesana
a	a	k8xC	a
Carlo	Carla	k1gMnSc5	Carla
Verdone	Verdon	k1gMnSc5	Verdon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Územní	územní	k2eAgFnSc2d1	územní
organizace	organizace	k1gFnSc2	organizace
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Řím	Řím	k1gInSc1	Řím
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
15	[number]	k4	15
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
municipio	municipio	k1gMnSc1	municipio
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
číslovaných	číslovaný	k2eAgInPc2d1	číslovaný
jako	jako	k8xC	jako
Řím	Řím	k1gInSc1	Řím
I	i	k9	i
až	až	k9	až
Řím	Řím	k1gInSc1	Řím
XV	XV	kA	XV
(	(	kIx(	(
<g/>
M.	M.	kA	M.
I	i	k9	i
až	až	k9	až
M.	M.	kA	M.
XV	XV	kA	XV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnPc4d1	vlastní
volené	volená	k1gFnPc4	volená
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
a	a	k8xC	a
starostu	starosta	k1gMnSc4	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
Řím	Řím	k1gInSc1	Řím
I	i	k9	i
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
starověkému	starověký	k2eAgNnSc3d1	starověké
a	a	k8xC	a
středověkému	středověký	k2eAgNnSc3d1	středověké
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
vymezila	vymezit	k5eAaPmAgFnS	vymezit
Aurelianova	Aurelianův	k2eAgFnSc1d1	Aurelianův
hradba	hradba	k1gFnSc1	hradba
ze	z	k7c2	z
3.	[number]	k4	3.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
a	a	k8xC	a
tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
22	[number]	k4	22
rioni	rioeň	k1gFnSc6	rioeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Historické	historický	k2eAgNnSc4d1	historické
členění	členění	k1gNnSc4	členění
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c2	za
krále	král	k1gMnSc2	král
Servia	Servium	k1gNnSc2	Servium
Tullia	Tullium	k1gNnSc2	Tullium
se	se	k3xPyFc4	se
Řím	Řím	k1gInSc1	Řím
dělil	dělit	k5eAaImAgInS	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
regiones	regionesa	k1gFnPc2	regionesa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
městským	městský	k2eAgInSc7d1	městský
tribus	tribus	k1gInSc4	tribus
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Augusta	August	k1gMnSc2	August
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
12	[number]	k4	12
a	a	k8xC	a
7	[number]	k4	7
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
město	město	k1gNnSc1	město
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
na	na	k7c4	na
14	[number]	k4	14
regiones	regionesa	k1gFnPc2	regionesa
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
pouze	pouze	k6eAd1	pouze
číslovaných	číslovaný	k2eAgFnPc2d1	číslovaná
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
dostaly	dostat	k5eAaPmAgInP	dostat
jména	jméno	k1gNnPc4	jméno
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
14.	[number]	k4	14.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
začalo	začít	k5eAaPmAgNnS	začít
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
zprvu	zprvu	k6eAd1	zprvu
12	[number]	k4	12
rioni	rioň	k1gMnPc7	rioň
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
dělením	dělení	k1gNnSc7	dělení
postupně	postupně	k6eAd1	postupně
přibývalo	přibývat	k5eAaImAgNnS	přibývat
<g/>
,	,	kIx,	,
až	až	k9	až
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
rione	rionout	k5eAaPmIp3nS	rionout
XXII	XXII	kA	XXII
Prati	Prať	k1gFnSc2	Prať
(	(	kIx(	(
<g/>
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Tibery	Tibera	k1gFnSc2	Tibera
severně	severně	k6eAd1	severně
od	od	k7c2	od
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nP	ležet
všechna	všechen	k3xTgNnPc4	všechen
uvnitř	uvnitř	k7c2	uvnitř
Aureliovy	Aureliův	k2eAgFnSc2d1	Aureliova
hradby	hradba	k1gFnSc2	hradba
<g/>
,	,	kIx,	,
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
zkráceně	zkráceně	k6eAd1	zkráceně
R.	R.	kA	R.
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
dnes	dnes	k6eAd1	dnes
municipio	municipio	k6eAd1	municipio
Řím	Řím	k1gInSc1	Řím
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
19.	[number]	k4	19.
století	století	k1gNnSc2	století
město	město	k1gNnSc1	město
staré	starý	k2eAgFnSc2d1	stará
hradby	hradba	k1gFnSc2	hradba
přesáhlo	přesáhnout	k5eAaPmAgNnS	přesáhnout
a	a	k8xC	a
nové	nový	k2eAgFnPc1d1	nová
čtvrti	čtvrt	k1gFnPc1	čtvrt
vně	vně	k7c2	vně
hradeb	hradba	k1gFnPc2	hradba
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
začaly	začít	k5eAaPmAgInP	začít
označovat	označovat	k5eAaImF	označovat
jako	jako	k9	jako
quartiere	quartirat	k5eAaPmIp3nS	quartirat
(	(	kIx(	(
<g/>
Q.	Q.	kA	Q.
<g/>
)	)	kIx)	)
I	i	k9	i
až	až	k9	až
XXXV	XXXV	kA	XXXV
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc4d1	další
sídla	sídlo	k1gNnPc4	sídlo
jako	jako	k8xC	jako
suburbia	suburbium	k1gNnPc4	suburbium
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
S.	S.	kA	S.
<g/>
,	,	kIx,	,
a	a	k8xC	a
ještě	ještě	k9	ještě
vzdálenější	vzdálený	k2eAgInSc1d2	vzdálenější
pás	pás	k1gInSc1	pás
jako	jako	k8xS	jako
zone	zone	k1gFnSc1	zone
(	(	kIx(	(
<g/>
Z.	Z.	kA	Z.
<g/>
)	)	kIx)	)
I	I	kA	I
až	až	k8xS	až
LIX	LIX	kA	LIX
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
spontánně	spontánně	k6eAd1	spontánně
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
sídelní	sídelní	k2eAgFnSc2d1	sídelní
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
frazioni	frazion	k1gMnPc1	frazion
<g/>
,	,	kIx,	,
zone	zone	k6eAd1	zone
O	o	k7c6	o
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
radikální	radikální	k2eAgFnSc1d1	radikální
reforma	reforma	k1gFnSc1	reforma
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
velkého	velký	k2eAgInSc2d1	velký
Říma	Řím	k1gInSc2	Řím
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
20	[number]	k4	20
administrativních	administrativní	k2eAgFnPc2d1	administrativní
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
circonscrizione	circonscrizion	k1gInSc5	circonscrizion
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
municipio	municipio	k6eAd1	municipio
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c4	na
15.	[number]	k4	15.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Řím	Řím	k1gInSc1	Řím
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
letiště	letiště	k1gNnPc4	letiště
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc4	letiště
Řím-Fiumicino	Řím-Fiumicin	k2eAgNnSc4d1	Řím-Fiumicino
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
starší	starý	k2eAgNnSc4d2	starší
a	a	k8xC	a
menší	malý	k2eAgNnSc4d2	menší
Ciampino	Ciampin	k2eAgNnSc4d1	Ciampino
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
užívají	užívat	k5eAaImIp3nP	užívat
hlavně	hlavně	k9	hlavně
nízkonákladové	nízkonákladový	k2eAgFnPc1d1	nízkonákladová
společnosti	společnost	k1gFnPc1	společnost
a	a	k8xC	a
letiště	letiště	k1gNnPc1	letiště
Řím-Urbe	Řím-Urb	k1gInSc5	Řím-Urb
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
soukromé	soukromý	k2eAgInPc4d1	soukromý
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
železniční	železniční	k2eAgInSc4d1	železniční
uzel	uzel	k1gInSc4	uzel
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
nádražím	nádraží	k1gNnSc7	nádraží
Termini	Termin	k2eAgMnPc1d1	Termin
blízko	blízko	k7c2	blízko
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
<g/>
;	;	kIx,	;
se	s	k7c7	s
400	[number]	k4	400
tisíci	tisíc	k4xCgInPc7	tisíc
cestujícími	cestující	k1gMnPc7	cestující
denně	denně	k6eAd1	denně
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejrušnějším	rušný	k2eAgNnPc3d3	nejrušnější
nádražím	nádraží	k1gNnPc3	nádraží
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dálkové	dálkový	k2eAgInPc4d1	dálkový
rychlíky	rychlík	k1gInPc4	rychlík
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
Římem	Řím	k1gInSc7	Řím
projíždějí	projíždět	k5eAaImIp3nP	projíždět
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
rekonstruované	rekonstruovaný	k2eAgNnSc4d1	rekonstruované
nádraží	nádraží	k1gNnSc4	nádraží
Roma-Tiburtina	Roma-Tiburtin	k1gMnSc2	Roma-Tiburtin
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
má	mít	k5eAaImIp3nS	mít
Řím	Řím	k1gInSc1	Řím
ještě	ještě	k6eAd1	ještě
6	[number]	k4	6
nádraží	nádraží	k1gNnSc2	nádraží
pro	pro	k7c4	pro
lokální	lokální	k2eAgFnSc4d1	lokální
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
velký	velký	k2eAgInSc4d1	velký
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
sice	sice	k8xC	sice
dálniční	dálniční	k2eAgInSc1d1	dálniční
obchvat	obchvat	k1gInSc1	obchvat
(	(	kIx(	(
<g/>
GRA	GRA	kA	GRA
<g/>
)	)	kIx)	)
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
10	[number]	k4	10
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc1d1	vlastní
město	město	k1gNnSc1	město
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
starověkou	starověký	k2eAgFnSc4d1	starověká
hvězdicovou	hvězdicový	k2eAgFnSc4d1	hvězdicová
strukturu	struktura	k1gFnSc4	struktura
hlavních	hlavní	k2eAgFnPc2d1	hlavní
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tak	tak	k9	tak
musí	muset	k5eAaImIp3nP	muset
jezdit	jezdit	k5eAaImF	jezdit
přes	přes	k7c4	přes
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
zablokované	zablokovaný	k2eAgNnSc1d1	zablokované
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
oblastí	oblast	k1gFnPc2	oblast
proto	proto	k8xC	proto
platí	platit	k5eAaImIp3nS	platit
režim	režim	k1gInSc4	režim
omezeného	omezený	k2eAgInSc2d1	omezený
provozu	provoz	k1gInSc2	provoz
(	(	kIx(	(
<g/>
ZTL	ZTL	kA	ZTL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sem	sem	k6eAd1	sem
auta	auto	k1gNnPc1	auto
smějí	smát	k5eAaImIp3nP	smát
zajíždět	zajíždět	k5eAaImF	zajíždět
jen	jen	k9	jen
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
a	a	k8xC	a
mezi	mezi	k7c7	mezi
6	[number]	k4	6
a	a	k8xC	a
18	[number]	k4	18
hodinou	hodina	k1gFnSc7	hodina
jen	jen	k9	jen
na	na	k7c4	na
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
povolení	povolení	k1gNnSc4	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
Trastevere	Trastever	k1gMnSc5	Trastever
<g/>
,	,	kIx,	,
San	San	k1gMnSc5	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
<g/>
,	,	kIx,	,
Testaccio	Testaccio	k1gMnSc1	Testaccio
<g/>
)	)	kIx)	)
platí	platit	k5eAaImIp3nS	platit
zákaz	zákaz	k1gInSc1	zákaz
vjezdu	vjezd	k1gInSc2	vjezd
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnPc1	radnice
bohužel	bohužel	k9	bohužel
vydávají	vydávat	k5eAaImIp3nP	vydávat
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
opatření	opatření	k1gNnPc1	opatření
nejsou	být	k5eNaImIp3nP	být
účinná	účinný	k2eAgNnPc1d1	účinné
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
platí	platit	k5eAaImIp3nP	platit
při	při	k7c6	při
vjezdu	vjezd	k1gInSc6	vjezd
do	do	k7c2	do
města	město	k1gNnSc2	město
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgNnSc4d1	vysoké
mýtné	mýtné	k1gNnSc4	mýtné
(	(	kIx(	(
<g/>
až	až	k9	až
210	[number]	k4	210
EUR	euro	k1gNnPc2	euro
<g/>
)	)	kIx)	)
a	a	k8xC	a
nesmějí	smát	k5eNaImIp3nP	smát
tu	tu	k6eAd1	tu
vůbec	vůbec	k9	vůbec
parkovat	parkovat	k5eAaImF	parkovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
často	často	k6eAd1	často
přetížena	přetížen	k2eAgFnSc1d1	přetížena
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
tři	tři	k4xCgFnPc1	tři
linky	linka	k1gFnPc1	linka
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
60	[number]	k4	60
km	km	kA	km
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
obcházejí	obcházet	k5eAaImIp3nP	obcházet
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
výstavba	výstavba	k1gFnSc1	výstavba
tunelů	tunel	k1gInPc2	tunel
mohla	moct	k5eAaImAgFnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Vedou	vést	k5eAaImIp3nP	vést
poměrně	poměrně	k6eAd1	poměrně
daleko	daleko	k6eAd1	daleko
za	za	k7c4	za
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
rozvětvují	rozvětvovat	k5eAaImIp3nP	rozvětvovat
<g/>
,	,	kIx,	,
nárokům	nárok	k1gInPc3	nárok
však	však	k8xC	však
nemohou	moct	k5eNaImIp3nP	moct
stačit	stačit	k5eAaBmF	stačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zahájena	zahájit	k5eAaPmNgFnS	zahájit
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
provozovala	provozovat	k5eAaImAgFnS	provozovat
městská	městský	k2eAgFnSc1d1	městská
dopravní	dopravní	k2eAgFnSc1d1	dopravní
společnost	společnost	k1gFnSc1	společnost
ATAC	ATAC	kA	ATAC
šest	šest	k4xCc1	šest
linek	linka	k1gFnPc2	linka
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
51,3	[number]	k4	51,3
km	km	kA	km
a	a	k8xC	a
se	s	k7c7	s
192	[number]	k4	192
zastávkami	zastávka	k1gFnPc7	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
přestávce	přestávka	k1gFnSc6	přestávka
opět	opět	k6eAd1	opět
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jedna	jeden	k4xCgFnSc1	jeden
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
linka	linka	k1gFnSc1	linka
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
11,3	[number]	k4	11,3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Autobusovou	autobusový	k2eAgFnSc4d1	autobusová
síť	síť	k1gFnSc4	síť
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
350	[number]	k4	350
linek	linka	k1gFnPc2	linka
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8 000	[number]	k4	8 000
zastávkami	zastávka	k1gFnPc7	zastávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
pamětihodnosti	pamětihodnost	k1gFnPc4	pamětihodnost
patří	patřit	k5eAaImIp3nP	patřit
Koloseum	Koloseum	k1gNnSc1	Koloseum
<g/>
,	,	kIx,	,
Forum	forum	k1gNnSc1	forum
Romanum	Romanum	k?	Romanum
<g/>
,	,	kIx,	,
Pantheon	Pantheon	k1gInSc1	Pantheon
<g/>
,	,	kIx,	,
Andělský	andělský	k2eAgInSc1d1	andělský
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
(	(	kIx(	(
<g/>
S.	S.	kA	S.
Maria	Maria	k1gFnSc1	Maria
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lateránská	lateránský	k2eAgFnSc1d1	Lateránská
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Sixtinská	sixtinský	k2eAgFnSc1d1	Sixtinská
kaple	kaple	k1gFnSc1	kaple
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc4	náměstí
Piazza	Piazz	k1gMnSc2	Piazz
Navona	Navon	k1gMnSc2	Navon
<g/>
,	,	kIx,	,
Španělské	španělský	k2eAgInPc1d1	španělský
schody	schod	k1gInPc1	schod
a	a	k8xC	a
fontána	fontána	k1gFnSc1	fontána
di	di	k?	di
Trevi	Treev	k1gFnSc6	Treev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgInSc4d1	tradiční
poutnický	poutnický	k2eAgInSc4d1	poutnický
okruh	okruh	k1gInSc4	okruh
tvořily	tvořit	k5eAaImAgFnP	tvořit
čtyři	čtyři	k4xCgFnPc4	čtyři
papežské	papežský	k2eAgFnPc4d1	Papežská
baziliky	bazilika	k1gFnPc4	bazilika
(	(	kIx(	(
<g/>
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
v	v	k7c6	v
Lateránu	Laterán	k1gInSc6	Laterán
<g/>
,	,	kIx,	,
Panny	Panna	k1gFnPc4	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgFnPc1d1	sněžná
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
a	a	k8xC	a
svatého	svatý	k2eAgMnSc4d1	svatý
Šebastiána	Šebastián	k1gMnSc4	Šebastián
v	v	k7c6	v
katakombách	katakomby	k1gFnPc6	katakomby
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Antické	antický	k2eAgFnPc1d1	antická
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
starověkou	starověký	k2eAgFnSc7d1	starověká
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
Koloseum	Koloseum	k1gNnSc1	Koloseum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
80	[number]	k4	80
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
antické	antický	k2eAgNnSc4d1	antické
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
Pantheon	Pantheon	k1gInSc1	Pantheon
(	(	kIx(	(
<g/>
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
118	[number]	k4	118
<g/>
–	–	k?	–
<g/>
128	[number]	k4	128
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc1	zbytek
antického	antický	k2eAgNnSc2d1	antické
fóra	fórum	k1gNnSc2	fórum
na	na	k7c4	na
Forum	forum	k1gNnSc4	forum
Romanum	Romanum	k?	Romanum
<g/>
,	,	kIx,	,
mohutné	mohutný	k2eAgFnPc1d1	mohutná
Caracallovy	Caracallův	k2eAgFnPc1d1	Caracallova
lázně	lázeň	k1gFnPc1	lázeň
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
217	[number]	k4	217
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
triumfální	triumfální	k2eAgInPc1d1	triumfální
oblouky	oblouk	k1gInPc1	oblouk
a	a	k8xC	a
Andělský	andělský	k2eAgInSc1d1	andělský
hrad	hrad	k1gInSc1	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vodovody	vodovod	k1gInPc1	vodovod
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc1	zbytek
Serviovy	Serviův	k2eAgInPc1d1	Serviův
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
zachovaná	zachovaný	k2eAgFnSc1d1	zachovaná
Aurelianova	Aurelianův	k2eAgFnSc1d1	Aurelianův
hradba	hradba	k1gFnSc1	hradba
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
menších	malý	k2eAgFnPc2d2	menší
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
základy	základ	k1gInPc1	základ
později	pozdě	k6eAd2	pozdě
přestavěných	přestavěný	k2eAgFnPc2d1	přestavěná
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Trajánův	Trajánův	k2eAgInSc1d1	Trajánův
sloup	sloup	k1gInSc1	sloup
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
Navona	Navona	k1gFnSc1	Navona
</s>
</p>
<p>
<s>
Circus	Circus	k1gMnSc1	Circus
Maximus	Maximus	k1gMnSc1	Maximus
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
Colonna	Colonn	k1gInSc2	Colonn
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Fontány	fontán	k1gInPc4	fontán
===	===	k?	===
</s>
</p>
<p>
<s>
Řím	Řím	k1gInSc1	Řím
se	se	k3xPyFc4	se
už	už	k6eAd1	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
honosí	honosit	k5eAaImIp3nP	honosit
proslulým	proslulý	k2eAgInSc7d1	proslulý
systémem	systém	k1gInSc7	systém
svých	svůj	k3xOyFgInPc2	svůj
11	[number]	k4	11
veřejných	veřejný	k2eAgInPc2d1	veřejný
vodovodů	vodovod	k1gInPc2	vodovod
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
9	[number]	k4	9
dodnes	dodnes	k6eAd1	dodnes
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
Aqua	Aqu	k2eAgFnSc1d1	Aqua
Appia	Appia	k1gFnSc1	Appia
z	z	k7c2	z
roku	rok	k1gInSc2	rok
312	[number]	k4	312
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
16,5	[number]	k4	16,5
km	km	kA	km
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
Aqua	Aqu	k2eAgFnSc1d1	Aqua
Marcia	Marcia	k1gFnSc1	Marcia
z	z	k7c2	z
roku	rok	k1gInSc2	rok
140	[number]	k4	140
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
měří	měřit	k5eAaImIp3nS	měřit
91,5	[number]	k4	91,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
soustava	soustava	k1gFnSc1	soustava
fontán	fontána	k1gFnPc2	fontána
a	a	k8xC	a
kašen	kašna	k1gFnPc2	kašna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
nejen	nejen	k6eAd1	nejen
umělecky	umělecky	k6eAd1	umělecky
cenné	cenný	k2eAgNnSc1d1	cenné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
turisticky	turisticky	k6eAd1	turisticky
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
i	i	k9	i
jako	jako	k8xC	jako
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
relaxaci	relaxace	k1gFnSc4	relaxace
a	a	k8xC	a
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
římskou	římský	k2eAgFnSc7d1	římská
fontánou	fontána	k1gFnSc7	fontána
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
Fontána	fontána	k1gFnSc1	fontána
di	di	k?	di
Trevi	Treev	k1gFnSc6	Treev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kostely	kostel	k1gInPc1	kostel
a	a	k8xC	a
chrámy	chrám	k1gInPc1	chrám
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
Claudia	Claudia	k1gFnSc1	Claudia
Rendiny	Rendina	k1gFnSc2	Rendina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
2573	[number]	k4	2573
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
či	či	k8xC	či
poutníky	poutník	k1gMnPc4	poutník
jsou	být	k5eAaImIp3nP	být
zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
tři	tři	k4xCgFnPc1	tři
stovky	stovka	k1gFnPc1	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
podzemními	podzemní	k2eAgFnPc7d1	podzemní
katakombami	katakomby	k1gFnPc7	katakomby
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
pohřbívalo	pohřbívat	k5eAaImAgNnS	pohřbívat
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
scházeli	scházet	k5eAaImAgMnP	scházet
první	první	k4xOgMnPc1	první
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
menší	malý	k2eAgInPc4d2	menší
i	i	k8xC	i
větší	veliký	k2eAgInPc4d2	veliký
středověké	středověký	k2eAgInPc4d1	středověký
kostelíky	kostelík	k1gInPc4	kostelík
až	až	k9	až
po	po	k7c4	po
velkolepé	velkolepý	k2eAgFnPc4d1	velkolepá
středověké	středověký	k2eAgFnPc4d1	středověká
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgFnPc4d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnPc4d1	barokní
baziliky	bazilika	k1gFnPc4	bazilika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
kostely	kostel	k1gInPc1	kostel
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
z	z	k7c2	z
antických	antický	k2eAgInPc2d1	antický
římských	římský	k2eAgInPc2d1	římský
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Pantheon	Pantheon	k1gInSc4	Pantheon
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgInS	zůstat
téměř	téměř	k6eAd1	téměř
nezměněn	změnit	k5eNaPmNgInS	změnit
od	od	k7c2	od
2.	[number]	k4	2.
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
antické	antický	k2eAgInPc1d1	antický
chrámy	chrám	k1gInPc1	chrám
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
dobách	doba	k1gFnPc6	doba
začlenily	začlenit	k5eAaPmAgFnP	začlenit
do	do	k7c2	do
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
kostelů	kostel	k1gInPc2	kostel
<g/>
:	:	kIx,	:
Romulův	Romulův	k2eAgInSc1d1	Romulův
chrám	chrám	k1gInSc1	chrám
(	(	kIx(	(
<g/>
Santi	Santi	k1gNnSc1	Santi
Cosma	Cosmum	k1gNnSc2	Cosmum
e	e	k0	e
Damiano	Damiana	k1gFnSc5	Damiana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chrám	chrám	k1gInSc1	chrám
Antonina	Antonina	k1gFnSc1	Antonina
a	a	k8xC	a
Faustiny	Faustin	k2eAgFnPc1d1	Faustina
(	(	kIx(	(
<g/>
San	San	k1gFnPc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
in	in	k?	in
Miranda	Mirando	k1gNnPc1	Mirando
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Santa	Santa	k1gFnSc1	Santa
Constanza	Constanza	k1gFnSc1	Constanza
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
čtveřice	čtveřice	k1gFnSc1	čtveřice
"	"	kIx"	"
<g/>
větších	veliký	k2eAgFnPc2d2	veliký
<g/>
"	"	kIx"	"
bazilik	bazilika	k1gFnPc2	bazilika
(	(	kIx(	(
<g/>
basilica	basilica	k1gMnSc1	basilica
maior	maior	k1gMnSc1	maior
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
<g/>
,	,	kIx,	,
Lateránská	lateránský	k2eAgFnSc1d1	Lateránská
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
a	a	k8xC	a
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
řada	řada	k1gFnSc1	řada
"	"	kIx"	"
<g/>
menších	malý	k2eAgFnPc2d2	menší
<g/>
"	"	kIx"	"
bazilik	bazilika	k1gFnPc2	bazilika
(	(	kIx(	(
<g/>
basilica	basilica	k6eAd1	basilica
minor	minor	k2eAgFnSc1d1	minor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Šebestiána	Šebestián	k1gMnSc2	Šebestián
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
Svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
in	in	k?	in
Damaso	Damasa	k1gFnSc5	Damasa
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
řetězech	řetěz	k1gInPc6	řetěz
<g/>
,	,	kIx,	,
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
in	in	k?	in
Cosmedin	Cosmedina	k1gFnPc2	Cosmedina
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Trastevere	Trastever	k1gInSc5	Trastever
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
in	in	k?	in
Montesanto	Montesanta	k1gFnSc5	Montesanta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
významné	významný	k2eAgInPc1d1	významný
kostely	kostel	k1gInPc1	kostel
a	a	k8xC	a
chrámy	chrám	k1gInPc1	chrám
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Sant	Sant	k1gInSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Agnese	Agnese	k1gFnSc1	Agnese
in	in	k?	in
Agone	agon	k1gInSc5	agon
</s>
</p>
<p>
<s>
Sant	Sant	k1gInSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Andrea	Andrea	k1gFnSc1	Andrea
al	ala	k1gFnPc2	ala
Quirinale	Quirinale	k1gFnSc2	Quirinale
</s>
</p>
<p>
<s>
San	San	k?	San
Bartolomeo	Bartolomeo	k1gMnSc1	Bartolomeo
all	all	k?	all
<g/>
'	'	kIx"	'
<g/>
Isola	Isola	k1gMnSc1	Isola
</s>
</p>
<p>
<s>
San	San	k?	San
Carlo	Carla	k1gMnSc5	Carla
alle	allus	k1gMnSc5	allus
Quattro	Quattra	k1gMnSc5	Quattra
Fontane	Fontan	k1gInSc5	Fontan
</s>
</p>
<p>
<s>
Bazilika	bazilika	k1gFnSc1	bazilika
svaté	svatý	k2eAgFnSc2d1	svatá
Cecílie	Cecílie	k1gFnSc2	Cecílie
v	v	k7c6	v
Trastevere	Trastever	k1gInSc5	Trastever
</s>
</p>
<p>
<s>
San	San	k?	San
Clemente	Clement	k1gMnSc5	Clement
(	(	kIx(	(
<g/>
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dio	Dio	k?	Dio
Padre	Padr	k1gMnSc5	Padr
Misericordioso	Misericordiosa	k1gFnSc5	Misericordiosa
</s>
</p>
<p>
<s>
Il	Il	k?	Il
Gesù	Gesù	k1gFnSc1	Gesù
</s>
</p>
<p>
<s>
Sant	Sant	k1gMnSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Ignazio	Ignazio	k1gMnSc1	Ignazio
di	di	k?	di
Loyola	Loyola	k1gFnSc1	Loyola
in	in	k?	in
Campo	Campa	k1gFnSc5	Campa
Marzio	Marzia	k1gMnSc5	Marzia
</s>
</p>
<p>
<s>
Sant	Sant	k1gMnSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Ivo	Ivo	k1gMnSc1	Ivo
alla	allus	k1gMnSc2	allus
Sapienza	Sapienz	k1gMnSc2	Sapienz
</s>
</p>
<p>
<s>
Santa	Sant	k1gMnSc4	Sant
Maria	Mario	k1gMnSc4	Mario
degli	degnout	k5eAaPmAgMnP	degnout
Angeli	angel	k1gMnSc5	angel
e	e	k0	e
dei	dei	k?	dei
Martiri	Martir	k1gFnPc1	Martir
(	(	kIx(	(
<g/>
Diokleciánovy	Diokleciánův	k2eAgFnPc1d1	Diokleciánova
lázně	lázeň	k1gFnPc1	lázeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Maria	Mario	k1gMnSc2	Mario
in	in	k?	in
Aracoeli	Aracoel	k1gInPc7	Aracoel
</s>
</p>
<p>
<s>
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
sopra	sopra	k1gFnSc1	sopra
Minerva	Minerva	k1gFnSc1	Minerva
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Maria	Mario	k1gMnSc2	Mario
dei	dei	k?	dei
Miracoli	Miracole	k1gFnSc4	Miracole
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Maria	Mario	k1gMnSc2	Mario
del	del	k?	del
Popolo	Popola	k1gFnSc5	Popola
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Maria	Mario	k1gMnSc2	Mario
in	in	k?	in
Vallicella	Vallicell	k1gMnSc2	Vallicell
(	(	kIx(	(
<g/>
Chiesa	Chiesa	k1gFnSc1	Chiesa
Nuova	Nuova	k1gFnSc1	Nuova
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Maria	Mario	k1gMnSc2	Mario
della	dello	k1gNnSc2	dello
Vittoria	Vittorium	k1gNnSc2	Vittorium
</s>
</p>
<p>
<s>
San	San	k?	San
Pietro	Pietro	k1gNnSc1	Pietro
in	in	k?	in
Vincoli	Vincole	k1gFnSc3	Vincole
(	(	kIx(	(
<g/>
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bazilika	bazilika	k1gFnSc1	bazilika
Santa	Sant	k1gInSc2	Sant
Prassede	Prassed	k1gMnSc5	Prassed
</s>
</p>
<p>
<s>
Santa	Santa	k1gFnSc1	Santa
Pudenziana	Pudenziana	k1gFnSc1	Pudenziana
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Sabina	Sabina	k1gMnSc1	Sabina
</s>
</p>
<p>
<s>
Sixtinská	sixtinský	k2eAgFnSc1d1	Sixtinská
kaple	kaple	k1gFnSc1	kaple
</s>
</p>
<p>
<s>
Trinità	Trinità	k?	Trinità
dei	dei	k?	dei
Monti	Monť	k1gFnSc2	Monť
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
Španělské	španělský	k2eAgInPc1d1	španělský
schody	schod	k1gInPc1	schod
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Římská	římský	k2eAgNnPc1d1	římské
náměstí	náměstí	k1gNnPc1	náměstí
===	===	k?	===
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
Navona	Navona	k1gFnSc1	Navona
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
náměstí	náměstí	k1gNnPc2	náměstí
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Tevere	Tever	k1gMnSc5	Tever
<g/>
,	,	kIx,	,
naproti	naproti	k6eAd1	naproti
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
;	;	kIx,	;
hlavní	hlavní	k2eAgFnSc7d1	hlavní
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgInSc1d1	barokní
chrám	chrám	k1gInSc1	chrám
Sant	Sant	k1gInSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Agnese	Agnese	k1gFnSc1	Agnese
in	in	k?	in
Agone	agon	k1gInSc5	agon
vystavěný	vystavěný	k2eAgInSc4d1	vystavěný
v	v	k7c6	v
letech	let	k1gInPc6	let
1652	[number]	k4	1652
<g/>
–	–	k?	–
<g/>
1672	[number]	k4	1672
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
náměstí	náměstí	k1gNnSc2	náměstí
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
Berniniho	Berniniha	k1gFnSc5	Berniniha
Fontána	fontána	k1gFnSc1	fontána
čtyř	čtyři	k4xCgFnPc2	čtyři
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
chrámu	chrám	k1gInSc2	chrám
Sant	Santa	k1gFnPc2	Santa
<g/>
'	'	kIx"	'
<g/>
Agnese	Agnese	k1gFnSc1	Agnese
stojí	stát	k5eAaImIp3nS	stát
Palazzo	Palazza	k1gFnSc5	Palazza
Pamphilj	Pamphilj	k1gFnSc1	Pamphilj
ze	z	k7c2	z
17.	[number]	k4	17.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatopetrské	svatopetrský	k2eAgNnSc1d1	Svatopetrské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
světově	světově	k6eAd1	světově
známé	známý	k2eAgNnSc4d1	známé
hlavní	hlavní	k2eAgNnSc4d1	hlavní
vatikánské	vatikánský	k2eAgNnSc4d1	Vatikánské
náměstí	náměstí	k1gNnSc4	náměstí
se	s	k7c7	s
čtyřnásobnými	čtyřnásobný	k2eAgFnPc7d1	čtyřnásobná
polokruhovými	polokruhový	k2eAgFnPc7d1	polokruhová
kolonádami	kolonáda	k1gFnPc7	kolonáda
a	a	k8xC	a
bazilikou	bazilika	k1gFnSc7	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
di	di	k?	di
Spagna	Spagna	k1gFnSc1	Spagna
leží	ležet	k5eAaImIp3nS	ležet
pod	pod	k7c7	pod
vrchem	vrch	k1gInSc7	vrch
Monte	Mont	k1gInSc5	Mont
Pincio	Pincio	k1gNnSc4	Pincio
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Říma	Řím	k1gInSc2	Řím
<g/>
;	;	kIx,	;
odtud	odtud	k6eAd1	odtud
vedou	vést	k5eAaImIp3nP	vést
známé	známý	k2eAgInPc4d1	známý
Španělské	španělský	k2eAgInPc4d1	španělský
schody	schod	k1gInPc4	schod
vzhůru	vzhůru	k6eAd1	vzhůru
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
Trinita	Trinita	k1gFnSc1	Trinita
dei	dei	k?	dei
Monti	Monť	k1gFnSc2	Monť
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1723	[number]	k4	1723
<g/>
–	–	k?	–
<g/>
1726	[number]	k4	1726
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
Venezia	Venezium	k1gNnSc2	Venezium
propojuje	propojovat	k5eAaImIp3nS	propojovat
historický	historický	k2eAgInSc1d1	historický
a	a	k8xC	a
antický	antický	k2eAgInSc1d1	antický
střed	střed	k1gInSc1	střed
Říma	Řím	k1gInSc2	Řím
<g/>
;	;	kIx,	;
dominantní	dominantní	k2eAgFnSc7d1	dominantní
stavbou	stavba	k1gFnSc7	stavba
náměstí	náměstí	k1gNnSc2	náměstí
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgInSc1d1	národní
památník	památník	k1gInSc1	památník
V.	V.	kA	V.
Emanuela	Emanuela	k1gFnSc1	Emanuela
II	II	kA	II
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
severozápadně	severozápadně	k6eAd1	severozápadně
leží	ležet	k5eAaImIp3nS	ležet
renesanční	renesanční	k2eAgFnSc7d1	renesanční
Palazzo	Palazza	k1gFnSc5	Palazza
Venezia	Venezium	k1gNnSc2	Venezium
(	(	kIx(	(
<g/>
Benátský	benátský	k2eAgInSc1d1	benátský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
postavený	postavený	k2eAgMnSc1d1	postavený
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
15.	[number]	k4	15.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
Campo	Campa	k1gFnSc5	Campa
dei	dei	k?	dei
Fiori	Fior	k1gMnSc6	Fior
leží	ležet	k5eAaImIp3nS	ležet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Piazza	Piazz	k1gMnSc2	Piazz
Navona	Navon	k1gMnSc2	Navon
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
trhy	trh	k1gInPc1	trh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
kaváren	kavárna	k1gFnPc2	kavárna
a	a	k8xC	a
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
je	být	k5eAaImIp3nS	být
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
u	u	k7c2	u
Římanů	Říman	k1gMnPc2	Říman
i	i	k8xC	i
turistů	turist	k1gMnPc2	turist
<g/>
;	;	kIx,	;
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
náměstí	náměstí	k1gNnSc2	náměstí
stojí	stát	k5eAaImIp3nS	stát
socha	socha	k1gFnSc1	socha
Giordana	Giordan	k1gMnSc2	Giordan
Bruna	Bruno	k1gMnSc2	Bruno
<g/>
,	,	kIx,	,
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
kašna	kašna	k1gFnSc1	kašna
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
del	del	k?	del
Popolo	Popola	k1gFnSc5	Popola
leží	ležet	k5eAaImIp3nS	ležet
západně	západně	k6eAd1	západně
od	od	k7c2	od
vrchu	vrch	k1gInSc2	vrch
Monte	Mont	k1gMnSc5	Mont
Pincio	Pincia	k1gMnSc5	Pincia
<g/>
,	,	kIx,	,
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
náměstí	náměstí	k1gNnSc2	náměstí
je	být	k5eAaImIp3nS	být
kostel	kostel	k1gInSc1	kostel
S.	S.	kA	S.
Maria	Mario	k1gMnSc2	Mario
del	del	k?	del
Popolo	Popola	k1gFnSc5	Popola
z	z	k7c2	z
11.	[number]	k4	11.
až	až	k9	až
16.	[number]	k4	16.
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
městská	městský	k2eAgFnSc1d1	městská
brána	brána	k1gFnSc1	brána
Porta	porta	k1gFnSc1	porta
del	del	k?	del
Popolo	Popola	k1gFnSc5	Popola
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1475	[number]	k4	1475
<g/>
;	;	kIx,	;
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
stojí	stát	k5eAaImIp3nS	stát
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
S.	S.	kA	S.
Maria	Mario	k1gMnSc2	Mario
in	in	k?	in
Montesanto	Montesanta	k1gFnSc5	Montesanta
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1662	[number]	k4	1662
<g/>
–	–	k?	–
<g/>
1679	[number]	k4	1679
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
del	del	k?	del
Campidoglio	Campidoglio	k6eAd1	Campidoglio
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
Kapitolu	Kapitol	k1gInSc2	Kapitol
(	(	kIx(	(
<g/>
Campidoglio	Campidoglio	k6eAd1	Campidoglio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
politické	politický	k2eAgNnSc1d1	politické
i	i	k8xC	i
duchovní	duchovní	k2eAgNnSc1d1	duchovní
centrum	centrum	k1gNnSc1	centrum
Starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
tržiště	tržiště	k1gNnSc1	tržiště
s	s	k7c7	s
radnicí	radnice	k1gFnSc7	radnice
(	(	kIx(	(
<g/>
Senátorský	senátorský	k2eAgInSc1d1	senátorský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
upravil	upravit	k5eAaPmAgMnS	upravit
a	a	k8xC	a
dostavěl	dostavět	k5eAaPmAgMnS	dostavět
Michelangelo	Michelangela	k1gFnSc5	Michelangela
Buonarroti	Buonarrot	k1gMnPc5	Buonarrot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nových	Nových	k2eAgInPc6d1	Nových
palácích	palác	k1gInPc6	palác
jsou	být	k5eAaImIp3nP	být
Kapitolská	kapitolský	k2eAgNnPc1d1	Kapitolské
muzea	muzeum	k1gNnPc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
skalce	skalka	k1gFnSc6	skalka
nad	nad	k7c7	nad
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starověkého	starověký	k2eAgInSc2d1	starověký
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
mincovny	mincovna	k1gFnSc2	mincovna
stojí	stát	k5eAaImIp3nS	stát
chrám	chrám	k1gInSc1	chrám
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
in	in	k?	in
Aracoeli	Aracoel	k1gInPc7	Aracoel
<g/>
,	,	kIx,	,
z	z	k7c2	z
terasy	terasa	k1gFnSc2	terasa
vedle	vedle	k7c2	vedle
radnice	radnice	k1gFnSc2	radnice
lze	lze	k6eAd1	lze
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
vykopávky	vykopávka	k1gFnPc4	vykopávka
Fora	forum	k1gNnSc2	forum
Romana	Roman	k1gMnSc2	Roman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
della	della	k1gFnSc1	della
Rotonda	Rotonda	k1gFnSc1	Rotonda
s	s	k7c7	s
Pantheonem	Pantheon	k1gInSc7	Pantheon
<g/>
,	,	kIx,	,
kruhovým	kruhový	k2eAgInSc7d1	kruhový
římským	římský	k2eAgInSc7d1	římský
chrámem	chrám	k1gInSc7	chrám
ze	z	k7c2	z
2.	[number]	k4	2.
století	století	k1gNnSc2	století
s	s	k7c7	s
litou	litý	k2eAgFnSc7d1	litá
kopulí	kopule	k1gFnSc7	kopule
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
východně	východně	k6eAd1	východně
od	od	k7c2	od
Piazza	Piazz	k1gMnSc2	Piazz
Navona	Navon	k1gMnSc2	Navon
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
Farnese	Farnese	k1gFnSc2	Farnese
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
del	del	k?	del
Quirinale	Quirinale	k1gFnSc1	Quirinale
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
na	na	k7c6	na
pahorku	pahorek	k1gInSc6	pahorek
Quirinale	Quirinale	k1gFnSc2	Quirinale
(	(	kIx(	(
<g/>
Kvirinál	Kvirinál	k1gInSc1	Kvirinál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
stojí	stát	k5eAaImIp3nS	stát
kašna	kašna	k1gFnSc1	kašna
s	s	k7c7	s
egyptským	egyptský	k2eAgInSc7d1	egyptský
obeliskem	obelisk	k1gInSc7	obelisk
<g/>
;	;	kIx,	;
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
letní	letní	k2eAgFnSc1d1	letní
rezidence	rezidence	k1gFnSc1	rezidence
papežů	papež	k1gMnPc2	papež
Palazzo	Palazza	k1gFnSc5	Palazza
del	del	k?	del
Quirinale	Quirinale	k1gFnPc4	Quirinale
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
16.	[number]	k4	16.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
sídlo	sídlo	k1gNnSc4	sídlo
italského	italský	k2eAgMnSc2d1	italský
prezidenta	prezident	k1gMnSc2	prezident
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
della	dell	k1gMnSc2	dell
Repubblica	Repubblicus	k1gMnSc2	Repubblicus
je	být	k5eAaImIp3nS	být
prostranství	prostranství	k1gNnSc1	prostranství
před	před	k7c7	před
nádražím	nádraží	k1gNnSc7	nádraží
Termini	Termin	k2eAgMnPc1d1	Termin
</s>
</p>
<p>
<s>
Piazza	Piazza	k1gFnSc1	Piazza
Colonna	Colonn	k1gInSc2	Colonn
<g/>
,	,	kIx,	,
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
náměstí	náměstí	k1gNnSc6	náměstí
stojí	stát	k5eAaImIp3nS	stát
sloup	sloup	k1gInSc1	sloup
Marca	Marcus	k1gMnSc2	Marcus
Aurelia	Aurelius	k1gMnSc2	Aurelius
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
leží	ležet	k5eAaImIp3nS	ležet
Palazzo	Palazza	k1gFnSc5	Palazza
Chigi	Chigi	k1gNnPc7	Chigi
z	z	k7c2	z
konce	konec	k1gInSc2	konec
16.	[number]	k4	16.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
italské	italský	k2eAgFnSc2d1	italská
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kinematografie	kinematografie	k1gFnSc1	kinematografie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
studia	studio	k1gNnPc4	studio
Cinecittá	Cinecittý	k2eAgNnPc4d1	Cinecittý
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnPc4d3	veliký
zázemí	zázemí	k1gNnPc4	zázemí
televizní	televizní	k2eAgFnSc2d1	televizní
a	a	k8xC	a
filmové	filmový	k2eAgFnSc2d1	filmová
produkce	produkce	k1gFnSc2	produkce
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentě	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
také	také	k9	také
centrum	centrum	k1gNnSc1	centrum
Italské	italský	k2eAgFnSc2d1	italská
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
natáčí	natáčet	k5eAaImIp3nS	natáčet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
současných	současný	k2eAgInPc2d1	současný
kasovních	kasovní	k2eAgInPc2d1	kasovní
trháků	trhák	k1gInPc2	trhák
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřiceti	čtyřicet	k4xCc3	čtyřicet
hektarový	hektarový	k2eAgInSc1d1	hektarový
komplex	komplex	k1gInSc1	komplex
studií	studie	k1gFnPc2	studie
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Říma	Řím	k1gInSc2	Řím
vzdálen	vzdálen	k2eAgInSc4d1	vzdálen
9	[number]	k4	9
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
produkčních	produkční	k2eAgFnPc2d1	produkční
komunit	komunita	k1gFnPc2	komunita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
po	po	k7c6	po
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
přes	přes	k7c4	přes
5000	[number]	k4	5000
profesionálů	profesionál	k1gMnPc2	profesionál
–	–	k?	–
od	od	k7c2	od
specialistů	specialista	k1gMnPc2	specialista
na	na	k7c4	na
šití	šití	k1gNnSc4	šití
dobových	dobový	k2eAgInPc2d1	dobový
kostýmů	kostým	k1gInPc2	kostým
po	po	k7c4	po
experty	expert	k1gMnPc4	expert
přes	přes	k7c4	přes
vizuální	vizuální	k2eAgInSc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
bylo	být	k5eAaImAgNnS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3000	[number]	k4	3000
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
od	od	k7c2	od
nedávných	dávný	k2eNgInPc2d1	nedávný
filmů	film	k1gInPc2	film
jako	jako	k8xS	jako
Umučení	umučení	k1gNnSc4	umučení
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
Gangy	Ganga	k1gFnSc2	Ganga
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
seriál	seriál	k1gInSc1	seriál
HBO	HBO	kA	HBO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vodní	vodní	k2eAgInSc1d1	vodní
život	život	k1gInSc1	život
a	a	k8xC	a
Dekameron	Dekameron	k1gInSc1	Dekameron
Dina	Din	k1gInSc2	Din
De	De	k?	De
Laurentiise	Laurentiise	k1gFnSc2	Laurentiise
po	po	k7c4	po
filmové	filmový	k2eAgFnPc4d1	filmová
klasiky	klasika	k1gFnPc4	klasika
jako	jako	k8xC	jako
Ben-Hur	Ben-Hur	k1gMnSc1	Ben-Hur
<g/>
,	,	kIx,	,
Kleopatra	Kleopatra	k1gFnSc1	Kleopatra
a	a	k8xC	a
filmy	film	k1gInPc1	film
Federika	Federik	k1gMnSc2	Federik
Felliniho	Fellini	k1gMnSc2	Fellini
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studia	studio	k1gNnPc4	studio
Cinecittá	Cinecittý	k2eAgFnSc1d1	Cinecittá
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
Benitem	Benit	k1gInSc7	Benit
Mussolinim	Mussolinima	k1gFnPc2	Mussolinima
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
bombardována	bombardovat	k5eAaImNgFnS	bombardovat
Spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
místem	místo	k1gNnSc7	místo
natáčení	natáčení	k1gNnSc2	natáčení
některých	některý	k3yIgFnPc2	některý
velkých	velký	k2eAgFnPc2d1	velká
amerických	americký	k2eAgFnPc2d1	americká
filmových	filmový	k2eAgFnPc2d1	filmová
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
následně	následně	k6eAd1	následně
nejvíce	hodně	k6eAd3	hodně
spjatá	spjatý	k2eAgFnSc1d1	spjatá
právě	právě	k9	právě
s	s	k7c7	s
Federikem	Federik	k1gMnSc7	Federik
Fellinim	Fellinima	k1gFnPc2	Fellinima
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
komplex	komplex	k1gInSc1	komplex
Cinecittá	Cinecittý	k2eAgFnSc1d1	Cinecittá
jediným	jediný	k2eAgNnSc7d1	jediné
studiem	studio	k1gNnSc7	studio
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
plně	plně	k6eAd1	plně
před-produkčním	předrodukční	k2eAgNnSc7d1	před-produkční
<g/>
,	,	kIx,	,
produkčním	produkční	k2eAgNnSc7d1	produkční
a	a	k8xC	a
po-produkčním	porodukční	k2eAgNnSc7d1	po-produkční
vybavením	vybavení	k1gNnSc7	vybavení
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
režisérům	režisér	k1gMnPc3	režisér
a	a	k8xC	a
producentům	producent	k1gMnPc3	producent
vkročit	vkročit	k5eAaPmF	vkročit
se	se	k3xPyFc4	se
scénářem	scénář	k1gInSc7	scénář
a	a	k8xC	a
odejít	odejít	k5eAaPmF	odejít
s	s	k7c7	s
hotovým	hotový	k2eAgInSc7d1	hotový
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Achacachi	Achacachi	k1gNnSc1	Achacachi
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
</s>
</p>
<p>
<s>
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
</s>
</p>
<p>
<s>
Cincinnati	Cincinnat	k5eAaImF	Cincinnat
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Peking	Peking	k1gInSc1	Peking
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
</s>
</p>
<p>
<s>
Plovdiv	Plovdiv	k1gInSc1	Plovdiv
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
</s>
</p>
<p>
<s>
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Roma	Rom	k1gMnSc2	Rom
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Garwood	Garwood	k1gInSc1	Garwood
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
:	:	kIx,	:
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svojtka	Svojtka	k1gFnSc1	Svojtka
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Koudelková	Koudelková	k1gFnSc1	Koudelková
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
:	:	kIx,	:
Česká	český	k2eAgNnPc1d1	české
zastavení	zastavení	k1gNnPc1	zastavení
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
</s>
</p>
<p>
<s>
Hrůza	Hrůza	k1gMnSc1	Hrůza
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
Urbanismus	urbanismus	k1gInSc1	urbanismus
světových	světový	k2eAgNnPc2d1	světové
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Athény	Athéna	k1gFnPc1	Athéna
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ČVUT	ČVUT	kA	ČVUT
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-01-03677-8	[number]	k4	978-80-01-03677-8
</s>
</p>
<p>
<s>
Koltermann	Koltermann	k1gMnSc1	Koltermann
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
:	:	kIx,	:
Řím	Řím	k1gInSc1	Řím
krok	krok	k1gInSc1	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Vašut	Vašut	k1gMnSc1	Vašut
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
Laurence	Laurence	k1gFnSc1	Laurence
<g/>
,	,	kIx,	,
Ray	Ray	k1gFnSc1	Ray
<g/>
:	:	kIx,	:
Řím	Řím	k1gInSc1	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
300	[number]	k4	300
<g/>
.	.	kIx.	.
</s>
<s>
Volvox	Volvox	k1gInSc1	Volvox
globator	globator	k1gInSc1	globator
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
21	[number]	k4	21
<g/>
,	,	kIx,	,
str.	str.	kA	str.
748	[number]	k4	748
</s>
</p>
<p>
<s>
Pelz	Pelz	k1gInSc1	Pelz
<g/>
,	,	kIx,	,
Monika	Monika	k1gFnSc1	Monika
<g/>
:	:	kIx,	:
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Vašut	Vašut	k1gMnSc1	Vašut
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Listopad	listopad	k1gInSc1	listopad
2015.	[number]	k4	2015.
72	[number]	k4	72
s	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
F.	F.	kA	F.
-	-	kIx~	-
Simeoni	Simeoň	k1gFnSc3	Simeoň
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
<g/>
:	:	kIx,	:
Řím	Řím	k1gInSc1	Řím
<g/>
:	:	kIx,	:
kompletní	kompletní	k2eAgMnSc1d1	kompletní
průvodce	průvodce	k1gMnSc1	průvodce
na	na	k7c4	na
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Cpress	Cpress	k1gInSc1	Cpress
2012	[number]	k4	2012
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
papežství	papežství	k1gNnSc2	papežství
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Říma	Řím	k1gInSc2	Řím
</s>
</p>
<p>
<s>
Starověký	starověký	k2eAgInSc1d1	starověký
Řím	Řím	k1gInSc1	Řím
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Řím	Řím	k1gInSc1	Řím
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Řím	Řím	k1gInSc1	Řím
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Virtuální	virtuální	k2eAgInSc1d1	virtuální
Řím	Řím	k1gInSc1	Řím
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
3D	[number]	k4	3D
fotografií	fotografia	k1gFnPc2	fotografia
Říma	Řím	k1gInSc2	Řím
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Bronková	Bronková	k1gFnSc1	Bronková
<g/>
:	:	kIx,	:
Baziliky	bazilika	k1gFnPc1	bazilika
<g/>
,	,	kIx,	,
chrámy	chrám	k1gInPc1	chrám
a	a	k8xC	a
kostelíky	kostelík	k1gInPc1	kostelík
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
české	český	k2eAgFnSc2d1	Česká
sekce	sekce	k1gFnSc2	sekce
Vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
rozhlasu	rozhlas	k1gInSc2	rozhlas
</s>
</p>
