<p>
<s>
Poručík	poručík	k1gMnSc1	poručík
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
důstojnická	důstojnický	k2eAgFnSc1d1	důstojnická
hodnost	hodnost	k1gFnSc1	hodnost
policií	policie	k1gFnPc2	policie
<g/>
,	,	kIx,	,
armád	armáda	k1gFnPc2	armáda
i	i	k8xC	i
hasičských	hasičský	k2eAgInPc2d1	hasičský
sborů	sbor	k1gInPc2	sbor
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Por.	Por.	k?	Por.
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
aktivního	aktivní	k2eAgMnSc4d1	aktivní
poručíka	poručík	k1gMnSc4	poručík
a	a	k8xC	a
Por.	Por.	k1gFnSc4	Por.
<g/>
v.v.	v.v.	k?	v.v.
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
hodnosti	hodnost	k1gFnSc2	hodnost
poručíka	poručík	k1gMnSc2	poručík
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
formách	forma	k1gFnPc6	forma
<g/>
,	,	kIx,	,
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
policii	policie	k1gFnSc6	policie
jako	jako	k9	jako
poručík	poručík	k1gMnSc1	poručík
a	a	k8xC	a
nadporučík	nadporučík	k1gMnSc1	nadporučík
<g/>
,	,	kIx,	,
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
slovenské	slovenský	k2eAgMnPc4d1	slovenský
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
verze	verze	k1gFnSc1	verze
pod-	pod-	k?	pod-
a	a	k8xC	a
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
americké	americký	k2eAgFnSc6d1	americká
je	být	k5eAaImIp3nS	být
stupeň	stupeň	k1gInSc4	stupeň
poručické	poručický	k2eAgFnSc2d1	poručický
hodnosti	hodnost	k1gFnSc2	hodnost
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
pořadovou	pořadový	k2eAgFnSc7d1	pořadová
číslovkou	číslovka	k1gFnSc7	číslovka
(	(	kIx(	(
<g/>
Second	Second	k1gMnSc1	Second
nebo	nebo	k8xC	nebo
First	First	k1gMnSc1	First
Lieutenant	Lieutenant	k1gMnSc1	Lieutenant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
armádě	armáda	k1gFnSc6	armáda
je	být	k5eAaImIp3nS	být
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hodností	hodnost	k1gFnSc7	hodnost
nadporučík	nadporučík	k1gMnSc1	nadporučík
<g/>
,	,	kIx,	,
nižší	nízký	k2eAgFnSc1d2	nižší
hodnost	hodnost	k1gFnSc1	hodnost
podporučík	podporučík	k1gMnSc1	podporučík
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Poručík	poručík	k1gMnSc1	poručík
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nižší	nízký	k2eAgMnPc4d2	nižší
důstojníky	důstojník	k1gMnPc4	důstojník
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
např.	např.	kA	např.
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Hodnostní	hodnostní	k2eAgNnSc4d1	hodnostní
označení	označení	k1gNnSc4	označení
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc1	dva
trojcípé	trojcípý	k2eAgFnPc1d1	trojcípá
zlaté	zlatý	k2eAgFnPc1d1	zlatá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekvivalentní	ekvivalentní	k2eAgFnSc4d1	ekvivalentní
hodnost	hodnost	k1gFnSc4	hodnost
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
==	==	k?	==
</s>
</p>
<p>
<s>
Angola	Angola	k1gFnSc1	Angola
Segundo	Segundo	k6eAd1	Segundo
Tenente	Tenent	k1gInSc5	Tenent
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
Teniente	Tenient	k1gInSc5	Tenient
</s>
</p>
<p>
<s>
Bolívie	Bolívie	k1gFnSc1	Bolívie
Teniente	Tenient	k1gMnSc5	Tenient
</s>
</p>
<p>
<s>
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
Porucnik	Porucnik	k1gInSc1	Porucnik
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
Tenente	Tenent	k1gInSc5	Tenent
</s>
</p>
<p>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
Lø	Lø	k1gFnSc2	Lø
</s>
</p>
<p>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
Luutnantti	Luutnantť	k1gFnSc2	Luutnantť
</s>
</p>
<p>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
Poručnik	Poručnik	k1gMnSc1	Poručnik
</s>
</p>
<p>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
Tenente	Tenent	k1gMnSc5	Tenent
</s>
</p>
<p>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
Lietnants	Lietnantsa	k1gFnPc2	Lietnantsa
</s>
</p>
<p>
<s>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Hadnagy	Hadnaga	k1gFnSc2	Hadnaga
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
Fenrik	Fenrika	k1gFnPc2	Fenrika
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
Leutnant	Leutnanta	k1gFnPc2	Leutnanta
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
Porucznik	Porucznik	k1gMnSc1	Porucznik
</s>
</p>
<p>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
Poručík	poručík	k1gMnSc1	poručík
</s>
</p>
<p>
<s>
==	==	k?	==
Příbuzná	příbuzný	k2eAgNnPc1d1	příbuzné
hesla	heslo	k1gNnPc1	heslo
==	==	k?	==
</s>
</p>
<p>
<s>
•	•	k?	•
Lajtnant	lajtnant	k1gMnSc1	lajtnant
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
poručík	poručík	k1gMnSc1	poručík
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
