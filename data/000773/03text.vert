<s>
Cyrilice	cyrilice	k1gFnSc1	cyrilice
(	(	kIx(	(
<g/>
též	též	k9	též
cyrilika	cyrilika	k1gFnSc1	cyrilika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
písmo	písmo	k1gNnSc4	písmo
původně	původně	k6eAd1	původně
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
používané	používaný	k2eAgInPc1d1	používaný
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
církevní	církevní	k2eAgFnSc2d1	církevní
slovanštiny	slovanština	k1gFnSc2	slovanština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c4	na
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
navázala	navázat	k5eAaPmAgFnS	navázat
<g/>
.	.	kIx.	.
</s>
<s>
Cyrilici	cyrilice	k1gFnSc4	cyrilice
sloužila	sloužit	k5eAaImAgFnS	sloužit
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
řecká	řecký	k2eAgFnSc1d1	řecká
alfabeta	alfabeta	k1gFnSc1	alfabeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
cyrilice	cyrilice	k1gFnSc1	cyrilice
<g/>
"	"	kIx"	"
často	často	k6eAd1	často
užívá	užívat	k5eAaImIp3nS	užívat
jen	jen	k9	jen
pro	pro	k7c4	pro
starou	starý	k2eAgFnSc4d1	stará
formu	forma	k1gFnSc4	forma
tohoto	tento	k3xDgNnSc2	tento
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
a	a	k8xC	a
církevní	církevní	k2eAgFnPc1d1	církevní
slovanštiny	slovanština	k1gFnPc1	slovanština
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
národní	národní	k2eAgFnPc1d1	národní
abecedy	abeceda	k1gFnPc1	abeceda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vývojem	vývoj	k1gInSc7	vývoj
cyrilice	cyrilice	k1gFnPc1	cyrilice
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xS	jako
azbuka	azbuka	k1gFnSc1	azbuka
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
azbuka	azbuka	k1gFnSc1	azbuka
je	být	k5eAaImIp3nS	být
zkratkou	zkratka	k1gFnSc7	zkratka
<g/>
,	,	kIx,	,
utvořenou	utvořený	k2eAgFnSc7d1	utvořená
ze	z	k7c2	z
začátečních	začáteční	k2eAgFnPc2d1	začáteční
hlásek	hláska	k1gFnPc2	hláska
názvů	název	k1gInPc2	název
prvních	první	k4xOgNnPc2	první
písmen	písmeno	k1gNnPc2	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
:	:	kIx,	:
a	a	k8xC	a
(	(	kIx(	(
<g/>
az	az	k?	az
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
b	b	k?	b
(	(	kIx(	(
<g/>
buki	buk	k1gFnSc2	buk
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
obdobně	obdobně	k6eAd1	obdobně
jsou	být	k5eAaImIp3nP	být
odvozena	odvozen	k2eAgNnPc1d1	odvozeno
i	i	k8xC	i
pojmenování	pojmenování	k1gNnPc1	pojmenování
jiných	jiný	k2eAgFnPc2d1	jiná
písemných	písemný	k2eAgFnPc2d1	písemná
soustav	soustava	k1gFnPc2	soustava
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
alfabeta	alfabeta	k1gFnSc1	alfabeta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
alefbet	alefbet	k1gInSc1	alefbet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
abugida	abugida	k1gFnSc1	abugida
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
bopomofo	bopomofo	k1gNnSc1	bopomofo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Cyrilice	cyrilice	k1gFnSc1	cyrilice
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
6	[number]	k4	6
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
bulharština	bulharština	k1gFnSc1	bulharština
<g/>
,	,	kIx,	,
makedonština	makedonština	k1gFnSc1	makedonština
<g/>
,	,	kIx,	,
srbština	srbština	k1gFnSc1	srbština
<g/>
,	,	kIx,	,
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
<g/>
,	,	kIx,	,
běloruština	běloruština	k1gFnSc1	běloruština
a	a	k8xC	a
ruština	ruština	k1gFnSc1	ruština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
neslovanských	slovanský	k2eNgInPc2d1	neslovanský
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
kazaština	kazaština	k1gFnSc1	kazaština
<g/>
,	,	kIx,	,
kyrgyzština	kyrgyzština	k1gFnSc1	kyrgyzština
<g/>
,	,	kIx,	,
tatarština	tatarština	k1gFnSc1	tatarština
<g/>
,	,	kIx,	,
baškirština	baškirština	k1gFnSc1	baškirština
<g/>
,	,	kIx,	,
čečenština	čečenština	k1gFnSc1	čečenština
<g/>
,	,	kIx,	,
čuvaština	čuvaština	k1gFnSc1	čuvaština
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
satelitních	satelitní	k2eAgInPc2d1	satelitní
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
mongolština	mongolština	k1gFnSc1	mongolština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
cyrilice	cyrilice	k1gFnSc1	cyrilice
používala	používat	k5eAaImAgFnS	používat
i	i	k9	i
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
také	také	k6eAd1	také
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
latinkou	latinka	k1gFnSc7	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
latinku	latinka	k1gFnSc4	latinka
také	také	k6eAd1	také
přešly	přejít	k5eAaPmAgInP	přejít
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
jazyky	jazyk	k1gInPc1	jazyk
států	stát	k1gInPc2	stát
vzniklých	vzniklý	k2eAgMnPc2d1	vzniklý
rozpadem	rozpad	k1gInSc7	rozpad
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
např.	např.	kA	např.
azerština	azerština	k1gFnSc1	azerština
<g/>
,	,	kIx,	,
turkmenština	turkmenština	k1gFnSc1	turkmenština
a	a	k8xC	a
uzbečtina	uzbečtina	k1gFnSc1	uzbečtina
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
podle	podle	k7c2	podle
názvu	název	k1gInSc2	název
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaImF	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	k9	by
tvůrcem	tvůrce	k1gMnSc7	tvůrce
cyrilice	cyrilice	k1gFnSc2	cyrilice
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
svatý	svatý	k2eAgMnSc1d1	svatý
Cyril	Cyril	k1gMnSc1	Cyril
(	(	kIx(	(
<g/>
Konstantin	Konstantin	k1gMnSc1	Konstantin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
Cyril	Cyril	k1gMnSc1	Cyril
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
složitější	složitý	k2eAgNnSc4d2	složitější
a	a	k8xC	a
starší	starý	k2eAgNnSc4d2	starší
písmo	písmo	k1gNnSc4	písmo
hlaholici	hlaholice	k1gFnSc4	hlaholice
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
cyrilice	cyrilice	k1gFnSc1	cyrilice
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
(	(	kIx(	(
<g/>
Cyrillus	Cyrillus	k1gMnSc1	Cyrillus
et	et	k?	et
Methudius	Methudius	k1gMnSc1	Methudius
inventis	inventis	k1gFnSc2	inventis
Bulgarorum	Bulgarorum	k1gInSc1	Bulgarorum
litteris	litteris	k1gFnSc4	litteris
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
výraz	výraz	k1gInSc1	výraz
jasného	jasný	k2eAgInSc2d1	jasný
příklonu	příklon	k1gInSc2	příklon
k	k	k7c3	k
byzantské	byzantský	k2eAgFnSc3d1	byzantská
kulturní	kulturní	k2eAgFnSc3d1	kulturní
sféře	sféra	k1gFnSc3	sféra
<g/>
,	,	kIx,	,
o	o	k7c4	o
nějž	jenž	k3xRgMnSc4	jenž
usilovali	usilovat	k5eAaImAgMnP	usilovat
zejména	zejména	k9	zejména
řečtí	řecký	k2eAgMnPc1d1	řecký
duchovní	duchovní	k1gMnPc1	duchovní
na	na	k7c6	na
slovanských	slovanský	k2eAgNnPc6d1	slovanské
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Cyrilici	cyrilice	k1gFnSc4	cyrilice
sloužilo	sloužit	k5eAaImAgNnS	sloužit
za	za	k7c4	za
vzor	vzor	k1gInSc4	vzor
řecké	řecký	k2eAgNnSc1d1	řecké
písmo	písmo	k1gNnSc1	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Cyrilice	cyrilice	k1gFnSc1	cyrilice
jim	on	k3xPp3gMnPc3	on
byla	být	k5eAaImAgFnS	být
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
původu	původ	k1gInSc3	původ
v	v	k7c6	v
řeckém	řecký	k2eAgNnSc6d1	řecké
písmu	písmo	k1gNnSc6	písmo
bližší	blízký	k2eAgMnSc1d2	bližší
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
a	a	k8xC	a
praktičtější	praktický	k2eAgMnSc1d2	praktičtější
než	než	k8xS	než
hlaholice	hlaholice	k1gFnSc1	hlaholice
<g/>
.	.	kIx.	.
</s>
<s>
Autorství	autorství	k1gNnSc1	autorství
cyrilice	cyrilice	k1gFnSc2	cyrilice
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
Metodějovu	Metodějův	k2eAgMnSc3d1	Metodějův
žáku	žák	k1gMnSc3	žák
Klimentovi	Kliment	k1gMnSc3	Kliment
<g/>
,	,	kIx,	,
důkazy	důkaz	k1gInPc4	důkaz
však	však	k9	však
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
těžiště	těžiště	k1gNnSc1	těžiště
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
přeneslo	přenést	k5eAaPmAgNnS	přenést
z	z	k7c2	z
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
do	do	k7c2	do
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
cyrilice	cyrilice	k1gFnSc1	cyrilice
šířila	šířit	k5eAaImAgFnS	šířit
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc1d1	zásadní
reforma	reforma	k1gFnSc1	reforma
církevně-slovanské	církevnělovanský	k2eAgFnSc2d1	církevně-slovanský
cyrilice	cyrilice	k1gFnSc2	cyrilice
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
za	za	k7c4	za
Petra	Petr	k1gMnSc4	Petr
Velikého	veliký	k2eAgMnSc4d1	veliký
v	v	k7c6	v
letech	let	k1gInPc6	let
1708	[number]	k4	1708
<g/>
-	-	kIx~	-
<g/>
1711	[number]	k4	1711
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgNnP	být
některá	některý	k3yIgNnPc1	některý
písmena	písmeno	k1gNnPc1	písmeno
upravena	upraven	k2eAgNnPc1d1	upraveno
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
méně	málo	k6eAd2	málo
používaná	používaný	k2eAgNnPc1d1	používané
písmena	písmeno	k1gNnPc1	písmeno
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
písmo	písmo	k1gNnSc4	písmo
nazývané	nazývaný	k2eAgNnSc4d1	nazývané
graždanka	graždanka	k1gFnSc1	graždanka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
občanské	občanský	k2eAgNnSc1d1	občanské
písmo	písmo	k1gNnSc1	písmo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
už	už	k6eAd1	už
tvary	tvar	k1gInPc4	tvar
současnému	současný	k2eAgNnSc3d1	současné
písmu	písmo	k1gNnSc6	písmo
používanému	používaný	k2eAgInSc3d1	používaný
v	v	k7c6	v
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
však	však	k9	však
představovalo	představovat	k5eAaImAgNnS	představovat
určité	určitý	k2eAgNnSc1d1	určité
přiblížení	přiblížení	k1gNnSc1	přiblížení
k	k	k7c3	k
latince	latinka	k1gFnSc3	latinka
(	(	kIx(	(
<g/>
latinizace	latinizace	k1gFnSc1	latinizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
odmítáno	odmítat	k5eAaImNgNnS	odmítat
zejména	zejména	k9	zejména
v	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
i	i	k9	i
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
pravoslavných	pravoslavný	k2eAgInPc2d1	pravoslavný
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
úprava	úprava	k1gFnSc1	úprava
písma	písmo	k1gNnSc2	písmo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
čtyři	čtyři	k4xCgFnPc4	čtyři
<g/>
[	[	kIx(	[
<g/>
ujasnit	ujasnit	k5eAaPmF	ujasnit
<g/>
]	]	kIx)	]
méně	málo	k6eAd2	málo
používaná	používaný	k2eAgNnPc4d1	používané
písmena	písmeno	k1gNnPc4	písmeno
z	z	k7c2	z
azbuky	azbuka	k1gFnSc2	azbuka
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
cyrilice	cyrilice	k1gFnSc1	cyrilice
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
900	[number]	k4	900
vypadala	vypadat	k5eAaImAgFnS	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Tvary	tvar	k1gInPc1	tvar
písmen	písmeno	k1gNnPc2	písmeno
a	a	k8xC	a
pořadí	pořadí	k1gNnSc6	pořadí
abecedy	abeceda	k1gFnSc2	abeceda
v	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
slovanštině	slovanština	k1gFnSc6	slovanština
ruské	ruský	k2eAgFnSc2d1	ruská
redakce	redakce	k1gFnSc2	redakce
<g/>
:	:	kIx,	:
Názvy	název	k1gInPc1	název
písmen	písmeno	k1gNnPc2	písmeno
(	(	kIx(	(
<g/>
ke	k	k7c3	k
každému	každý	k3xTgNnSc3	každý
písmenu	písmeno	k1gNnSc3	písmeno
bylo	být	k5eAaImAgNnS	být
přiřazeno	přiřazen	k2eAgNnSc1d1	přiřazeno
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
existovala	existovat	k5eAaImAgFnS	existovat
báseň	báseň	k1gFnSc1	báseň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
:	:	kIx,	:
az	az	k?	az
-	-	kIx~	-
buki	buki	k1gNnSc1	buki
-	-	kIx~	-
vedi	vedi	k1gNnSc1	vedi
-	-	kIx~	-
glagol	glagol	k1gInSc1	glagol
<g/>
'	'	kIx"	'
-	-	kIx~	-
dobro	dobro	k1gNnSc1	dobro
-	-	kIx~	-
jest	být	k5eAaImIp3nS	být
<g/>
'	'	kIx"	'
-	-	kIx~	-
živjote	živjot	k1gMnSc5	živjot
-	-	kIx~	-
dzelo	dzelo	k1gNnSc1	dzelo
-	-	kIx~	-
zemlja	zemlja	k1gFnSc1	zemlja
-	-	kIx~	-
iže	iže	k?	iže
-	-	kIx~	-
i	i	k9	i
-	-	kIx~	-
kako	kako	k6eAd1	kako
-	-	kIx~	-
ljudi	ljud	k1gMnPc1	ljud
-	-	kIx~	-
myslete	myslet	k5eAaImRp2nP	myslet
-	-	kIx~	-
naš	naš	k?	naš
-	-	kIx~	-
on	on	k3xPp3gMnSc1	on
-	-	kIx~	-
pokoj	pokoj	k1gInSc4	pokoj
-	-	kIx~	-
rcy	rcy	k?	rcy
-	-	kIx~	-
slovo	slovo	k1gNnSc1	slovo
-	-	kIx~	-
tvjordo	tvjordo	k1gNnSc1	tvjordo
-	-	kIx~	-
uk	uk	k?	uk
-	-	kIx~	-
fert	fert	k1gMnSc1	fert
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
cher	cher	k1gInSc1	cher
-	-	kIx~	-
omega	omega	k1gFnSc1	omega
-	-	kIx~	-
červ	červ	k1gMnSc1	červ
<g/>
'	'	kIx"	'
-	-	kIx~	-
cy	cy	k?	cy
-	-	kIx~	-
ša	ša	k?	ša
-	-	kIx~	-
šta	šta	k?	šta
-	-	kIx~	-
jer	jer	k1gNnSc1	jer
-	-	kIx~	-
jery	jery	k1gNnSc1	jery
-	-	kIx~	-
jer	jer	k1gNnSc1	jer
<g/>
'	'	kIx"	'
-	-	kIx~	-
jat	jat	k2eAgMnSc1d1	jat
<g/>
'	'	kIx"	'
-	-	kIx~	-
ju	ju	k0	ju
-	-	kIx~	-
ja	ja	k?	ja
-	-	kIx~	-
jotované	jotovaný	k2eAgNnSc1d1	jotovaný
jest	být	k5eAaImIp3nS	být
<g/>
'	'	kIx"	'
-	-	kIx~	-
malý	malý	k2eAgInSc1d1	malý
jus	jus	k?	jus
-	-	kIx~	-
jotovaný	jotovaný	k2eAgInSc1d1	jotovaný
malý	malý	k2eAgInSc1d1	malý
jus	jus	k?	jus
-	-	kIx~	-
velký	velký	k2eAgInSc1d1	velký
jus	jus	k?	jus
-	-	kIx~	-
jotovaný	jotovaný	k2eAgInSc1d1	jotovaný
velký	velký	k2eAgInSc1d1	velký
jus	jus	k?	jus
-	-	kIx~	-
ksi	ksi	k?	ksi
-	-	kIx~	-
psi	pes	k1gMnPc1	pes
-	-	kIx~	-
fita	fita	k1gMnSc1	fita
-	-	kIx~	-
ižica	ižica	k1gMnSc1	ižica
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
písmen	písmeno	k1gNnPc2	písmeno
se	se	k3xPyFc4	se
během	během	k7c2	během
času	čas	k1gInSc2	čas
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
písmena	písmeno	k1gNnPc1	písmeno
zanikla	zaniknout	k5eAaPmAgNnP	zaniknout
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgFnSc1d1	jiná
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
nových	nový	k2eAgMnPc2d1	nový
jazyků	jazyk	k1gMnPc2	jazyk
zavedla	zavést	k5eAaPmAgFnS	zavést
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
pořadí	pořadí	k1gNnSc4	pořadí
písmen	písmeno	k1gNnPc2	písmeno
v	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
(	(	kIx(	(
<g/>
azbuce	azbuka	k1gFnSc6	azbuka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
liší	lišit	k5eAaImIp3nS	lišit
zdroj	zdroj	k1gInSc1	zdroj
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
azbuce	azbuka	k1gFnSc6	azbuka
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
mají	mít	k5eAaImIp3nP	mít
jinou	jiný	k2eAgFnSc4d1	jiná
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
doplněné	doplněný	k2eAgInPc1d1	doplněný
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
o	o	k7c4	o
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
významu	význam	k1gInSc6	význam
(	(	kIx(	(
<g/>
nejdříve	dříve	k6eAd3	dříve
český	český	k2eAgInSc4d1	český
či	či	k8xC	či
nejbližší	blízký	k2eAgInSc4d3	Nejbližší
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
popis	popis	k1gInSc4	popis
v	v	k7c6	v
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ovšem	ovšem	k9	ovšem
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
přehled	přehled	k1gInSc1	přehled
základní	základní	k2eAgInSc1d1	základní
<g/>
,	,	kIx,	,
majuskulní	majuskulní	k2eAgFnSc2d1	majuskulní
podoby	podoba	k1gFnSc2	podoba
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
velká	velký	k2eAgNnPc4d1	velké
písmena	písmeno	k1gNnPc4	písmeno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ižica	Ižica	k1gFnSc1	Ižica
se	se	k3xPyFc4	se
vyslovovala	vyslovovat	k5eAaImAgFnS	vyslovovat
V	V	kA	V
po	po	k7c4	po
A	A	kA	A
a	a	k8xC	a
E	E	kA	E
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
I.	I.	kA	I.
Jer	jer	k1gNnSc1	jer
(	(	kIx(	(
<g/>
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
znak	znak	k1gInSc1	znak
<g/>
)	)	kIx)	)
zřejmě	zřejmě	k6eAd1	zřejmě
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
dnešní	dnešní	k2eAgFnSc3d1	dnešní
výslovnosti	výslovnost	k1gFnSc3	výslovnost
v	v	k7c6	v
bulharštině	bulharština	k1gFnSc6	bulharština
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
podobnou	podobný	k2eAgFnSc4d1	podobná
samohlásku	samohláska	k1gFnSc4	samohláska
jako	jako	k8xC	jako
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
psal	psát	k5eAaImAgInS	psát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
končila	končit	k5eAaImAgFnS	končit
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
,	,	kIx,	,
např.	např.	kA	např.
б	б	k?	б
=	=	kIx~	=
bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
uživatelů	uživatel	k1gMnPc2	uživatel
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
ruská	ruský	k2eAgFnSc1d1	ruská
cyrilice	cyrilice	k1gFnSc1	cyrilice
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
azbuka	azbuka	k1gFnSc1	azbuka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
následující	následující	k2eAgInPc4d1	následující
znaky	znak	k1gInPc4	znak
<g/>
:	:	kIx,	:
Ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
používá	používat	k5eAaImIp3nS	používat
navíc	navíc	k6eAd1	navíc
písmena	písmeno	k1gNnPc4	písmeno
Є	Є	k?	Є
<g/>
,	,	kIx,	,
І	І	k?	І
<g/>
,	,	kIx,	,
Ї	Ї	k?	Ї
<g/>
,	,	kIx,	,
Ґ	Ґ	k?	Ґ
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
nepoužívá	používat	k5eNaImIp3nS	používat
např.	např.	kA	např.
Ё	Ё	k?	Ё
<g/>
,	,	kIx,	,
Ъ	Ъ	k?	Ъ
<g/>
,	,	kIx,	,
Ы	Ы	k?	Ы
<g/>
,	,	kIx,	,
Э	Э	k?	Э
<g />
.	.	kIx.	.
</s>
<s>
Г	Г	k?	Г
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nečte	číst	k5eNaImIp3nS	číst
G	G	kA	G
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
H.	H.	kA	H.
Běloruština	běloruština	k1gFnSc1	běloruština
používá	používat	k5eAaImIp3nS	používat
navíc	navíc	k6eAd1	navíc
písmena	písmeno	k1gNnPc4	písmeno
І	І	k?	І
<g/>
,	,	kIx,	,
Ў	Ў	k?	Ў
<g/>
,	,	kIx,	,
Ґ	Ґ	k?	Ґ
<g/>
;	;	kIx,	;
Ў	Ў	k?	Ў
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
polské	polský	k2eAgNnSc1d1	polské
Ł	Ł	kA	Ł
<g/>
;	;	kIx,	;
nepoužívá	používat	k5eNaImIp3nS	používat
např.	např.	kA	např.
И	И	k?	И
<g/>
,	,	kIx,	,
Щ	Щ	k?	Щ
<g/>
,	,	kIx,	,
Ъ	Ъ	k?	Ъ
Г	Г	k?	Г
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nečte	číst	k5eNaImIp3nS	číst
G	G	kA	G
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g />
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
Srbština	srbština	k1gFnSc1	srbština
používá	používat	k5eAaImIp3nS	používat
Ј	Ј	k?	Ј
místo	místo	k7c2	místo
Й	Й	k?	Й
a	a	k8xC	a
nepoužívá	používat	k5eNaImIp3nS	používat
písmena	písmeno	k1gNnPc4	písmeno
Ю	Ю	k?	Ю
<g/>
,	,	kIx,	,
Я	Я	k?	Я
Navíc	navíc	k6eAd1	navíc
má	mít	k5eAaImIp3nS	mít
Ђ	Ђ	k?	Ђ
(	(	kIx(	(
<g/>
Đ	Đ	kA	Đ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ћ	Ћ	k?	Ћ
(	(	kIx(	(
<g/>
Ć	Ć	kA	Ć
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Љ	Љ	k?	Љ
(	(	kIx(	(
<g/>
LJ	LJ	kA	LJ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Њ	Њ	k?	Њ
(	(	kIx(	(
<g/>
NJ	NJ	kA	NJ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Џ	Џ	k?	Џ
(	(	kIx(	(
<g/>
DŽ	DŽ	kA	DŽ
<g/>
)	)	kIx)	)
-	-	kIx~	-
písmena	písmeno	k1gNnPc4	písmeno
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gInPc1	jejich
ekvivalenty	ekvivalent	k1gInPc1	ekvivalent
v	v	k7c6	v
chorvatské	chorvatský	k2eAgFnSc6d1	chorvatská
latince	latinka	k1gFnSc6	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Makedonština	makedonština	k1gFnSc1	makedonština
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
společného	společný	k2eAgNnSc2d1	společné
se	s	k7c7	s
srbštinou	srbština	k1gFnSc7	srbština
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
písmeno	písmeno	k1gNnSc1	písmeno
Ѕ	Ѕ	k?	Ѕ
(	(	kIx(	(
<g/>
DZ	DZ	kA	DZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
Ђ	Ђ	k?	Ђ
má	mít	k5eAaImIp3nS	mít
Ѓ	Ѓ	k?	Ѓ
a	a	k8xC	a
místo	místo	k1gNnSc1	místo
Ћ	Ћ	k?	Ћ
má	mít	k5eAaImIp3nS	mít
Ќ	Ќ	k?	Ќ
Bulharština	bulharština	k1gFnSc1	bulharština
čte	číst	k5eAaImIp3nS	číst
Щ	Щ	k?	Щ
jako	jako	k8xS	jako
ŠT	ŠT	kA	ŠT
<g/>
,	,	kIx,	,
ne	ne	k9	ne
ŠČ	ŠČ	kA	ŠČ
<g/>
;	;	kIx,	;
Ъ	Ъ	k?	Ъ
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
stejnou	stejný	k2eAgFnSc4d1	stejná
samohlásku	samohláska	k1gFnSc4	samohláska
jako	jako	k8xS	jako
rumunské	rumunský	k2eAgFnSc2d1	rumunská
Ă.	Ă.	k1gFnSc2	Ă.
Mongolština	mongolština	k1gFnSc1	mongolština
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
znaky	znak	k1gInPc4	znak
Ө	Ө	k?	Ө
<g/>
/	/	kIx~	/
<g/>
ө	ө	k?	ө
<g/>
,	,	kIx,	,
Ү	Ү	k?	Ү
<g/>
/	/	kIx~	/
<g/>
ү	ү	k?	ү
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
jako	jako	k9	jako
V	v	k7c4	v
<g/>
/	/	kIx~	/
<g/>
v	v	k7c6	v
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc1	písmeno
Ж	Ж	k?	Ж
(	(	kIx(	(
<g/>
Ž	Ž	kA	Ž
<g/>
)	)	kIx)	)
a	a	k8xC	a
З	З	k?	З
(	(	kIx(	(
<g/>
Z	Z	kA	Z
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
čtou	číst	k5eAaImIp3nP	číst
a	a	k8xC	a
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
jako	jako	k9	jako
Dž	Dž	k1gMnPc1	Dž
a	a	k8xC	a
Dz	Dz	k1gMnPc1	Dz
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
minuskulní	minuskulní	k2eAgFnSc2d1	minuskulní
azbuky	azbuka	k1gFnSc2	azbuka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
malá	malý	k2eAgNnPc4d1	malé
písmena	písmeno	k1gNnPc4	písmeno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
kolmým	kolmý	k2eAgNnSc7d1	kolmé
a	a	k8xC	a
kurzívním	kurzívní	k2eAgNnSc7d1	kurzívní
písmem	písmo	k1gNnSc7	písmo
ruské	ruský	k2eAgFnSc2d1	ruská
azbuky	azbuka	k1gFnSc2	azbuka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kurzívní	kurzívní	k2eAgInPc1d1	kurzívní
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bulharštině	bulharština	k1gFnSc6	bulharština
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
tištěném	tištěný	k2eAgInSc6d1	tištěný
textu	text	k1gInSc6	text
písmena	písmeno	k1gNnSc2	písmeno
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rukopisném	rukopisný	k2eAgInSc6d1	rukopisný
stylu	styl	k1gInSc6	styl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
některá	některý	k3yIgNnPc4	některý
písmena	písmeno	k1gNnPc4	písmeno
naprosto	naprosto	k6eAd1	naprosto
odlišná	odlišný	k2eAgNnPc4d1	odlišné
i	i	k8xC	i
v	v	k7c6	v
základním	základní	k2eAgMnSc6d1	základní
(	(	kIx(	(
<g/>
kolmém	kolmý	k2eAgInSc6d1	kolmý
<g/>
)	)	kIx)	)
typu	typ	k1gInSc2	typ
písma	písmo	k1gNnSc2	písmo
a	a	k8xC	a
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
kurzivě	kurziva	k1gFnSc3	kurziva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
se	se	k3xPyFc4	se
písmeno	písmeno	k1gNnSc1	písmeno
д	д	k?	д
píše	psát	k5eAaImIp3nS	psát
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
malé	malý	k2eAgInPc1d1	malý
"	"	kIx"	"
<g/>
g	g	kA	g
<g/>
"	"	kIx"	"
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
písmeno	písmeno	k1gNnSc1	písmeno
т	т	k?	т
jako	jako	k8xC	jako
malé	malý	k2eAgInPc1d1	malý
"	"	kIx"	"
<g/>
m	m	kA	m
<g/>
"	"	kIx"	"
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
písmeno	písmeno	k1gNnSc1	písmeno
и	и	k?	и
jako	jako	k8xC	jako
malé	malý	k2eAgFnPc1d1	malá
"	"	kIx"	"
<g/>
u	u	k7c2	u
<g/>
"	"	kIx"	"
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
písmeno	písmeno	k1gNnSc1	písmeno
п	п	k?	п
jako	jako	k8xS	jako
malé	malý	k2eAgMnPc4d1	malý
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
n	n	k0	n
<g/>
"	"	kIx"	"
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
písmeno	písmeno	k1gNnSc1	písmeno
г	г	k?	г
jako	jako	k8xC	jako
zrcadlově	zrcadlově	k6eAd1	zrcadlově
otočené	otočený	k2eAgNnSc1d1	otočené
"	"	kIx"	"
<g/>
s	s	k7c7	s
<g/>
"	"	kIx"	"
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
písmeno	písmeno	k1gNnSc1	písmeno
к	к	k?	к
jako	jako	k8xS	jako
malé	malý	k2eAgMnPc4d1	malý
"	"	kIx"	"
<g/>
k	k	k7c3	k
<g/>
"	"	kIx"	"
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
a	a	k8xC	a
písmeno	písmeno	k1gNnSc1	písmeno
л	л	k?	л
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
obrácené	obrácený	k2eAgFnSc2d1	obrácená
"	"	kIx"	"
<g/>
v	v	k7c4	v
<g/>
"	"	kIx"	"
tedy	tedy	k8xC	tedy
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
řecká	řecký	k2eAgFnSc1d1	řecká
Λ	Λ	k?	Λ
Bringhurst	Bringhurst	k1gFnSc1	Bringhurst
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Elements	Elements	k1gInSc1	Elements
of	of	k?	of
Typographic	Typographice	k1gInPc2	Typographice
Style	styl	k1gInSc5	styl
(	(	kIx(	(
<g/>
version	version	k1gInSc1	version
2.5	[number]	k4	2.5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
262	[number]	k4	262
<g/>
-	-	kIx~	-
<g/>
264	[number]	k4	264
<g/>
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
<g/>
,	,	kIx,	,
Hartley	Hartley	k1gInPc1	Hartley
&	&	k?	&
Marks	Marks	k1gInSc1	Marks
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
88179	[number]	k4	88179
<g/>
-	-	kIx~	-
<g/>
133	[number]	k4	133
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
používajících	používající	k2eAgInPc2d1	používající
cyrilici	cyrilice	k1gFnSc6	cyrilice
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cyrilice	cyrilice	k1gFnSc2	cyrilice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cyrilice	cyrilice	k1gFnSc2	cyrilice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stránky	stránka	k1gFnSc2	stránka
o	o	k7c4	o
cyrilici	cyrilice	k1gFnSc4	cyrilice
Cyrilice	cyrilice	k1gFnSc2	cyrilice
na	na	k7c4	na
Omniglotu	Omniglota	k1gFnSc4	Omniglota
Staroslověnština	staroslověnština	k1gFnSc1	staroslověnština
na	na	k7c4	na
Omniglotu	Omniglota	k1gFnSc4	Omniglota
Font	font	k1gInSc4	font
cyrilice	cyrilice	k1gFnSc2	cyrilice
Cyrillic	Cyrillice	k1gFnPc2	Cyrillice
Old	Olda	k1gFnPc2	Olda
Face	Fac	k1gFnSc2	Fac
Cyrilice	cyrilice	k1gFnSc2	cyrilice
na	na	k7c4	na
Unicode	Unicod	k1gInSc5	Unicod
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Kazašská	kazašský	k2eAgFnSc1d1	kazašská
cyrilice	cyrilice	k1gFnSc1	cyrilice
a	a	k8xC	a
fonty	font	k1gInPc1	font
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
Život	život	k1gInSc1	život
sv.	sv.	kA	sv.
Metoděje	Metoděj	k1gMnSc2	Metoděj
-	-	kIx~	-
ukázka	ukázka	k1gFnSc1	ukázka
cyrilského	cyrilský	k2eAgInSc2d1	cyrilský
textu	text	k1gInSc2	text
</s>
