<s>
Hmyz	hmyz	k1gInSc1	hmyz
(	(	kIx(	(
<g/>
Insecta	Insecta	k1gFnSc1	Insecta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třída	třída	k1gFnSc1	třída
šestinohých	šestinohý	k2eAgMnPc2d1	šestinohý
živočichů	živočich	k1gMnPc2	živočich
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
členovců	členovec	k1gMnPc2	členovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
tělo	tělo	k1gNnSc4	tělo
rozdělené	rozdělená	k1gFnSc2	rozdělená
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
hruď	hruď	k1gFnSc1	hruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc1	zadeček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
