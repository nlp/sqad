<s>
Hmyz	hmyz	k1gInSc1	hmyz
(	(	kIx(	(
<g/>
Insecta	Insecta	k1gFnSc1	Insecta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třída	třída	k1gFnSc1	třída
šestinohých	šestinohý	k2eAgMnPc2d1	šestinohý
živočichů	živočich	k1gMnPc2	živočich
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
členovců	členovec	k1gMnPc2	členovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
tělo	tělo	k1gNnSc4	tělo
rozdělené	rozdělená	k1gFnSc2	rozdělená
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
hruď	hruď	k1gFnSc1	hruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc1	zadeček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
tři	tři	k4xCgInPc1	tři
páry	pár	k1gInPc1	pár
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
mají	mít	k5eAaImIp3nP	mít
složené	složený	k2eAgNnSc4d1	složené
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
tykadla	tykadlo	k1gNnPc4	tykadlo
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgMnPc7d1	jediný
členovci	členovec	k1gMnPc7	členovec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
umějí	umět	k5eAaImIp3nP	umět
aktivně	aktivně	k6eAd1	aktivně
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejrůznorodější	různorodý	k2eAgFnSc4d3	nejrůznorodější
skupinu	skupina	k1gFnSc4	skupina
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milión	milión	k4xCgInSc4	milión
popsaných	popsaný	k2eAgInPc2d1	popsaný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc1	hmyz
představuje	představovat	k5eAaImIp3nS	představovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
žijících	žijící	k2eAgInPc2d1	žijící
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
existujících	existující	k2eAgInPc2d1	existující
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
že	že	k8xS	že
představují	představovat	k5eAaImIp3nP	představovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
živých	živý	k2eAgFnPc2d1	živá
forem	forma	k1gFnPc2	forma
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc4	hmyz
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
prostředí	prostředí	k1gNnSc6	prostředí
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
například	například	k6eAd1	například
jen	jen	k9	jen
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
obývá	obývat	k5eAaImIp3nS	obývat
oceány	oceán	k1gInPc7	oceán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
převahu	převaha	k1gFnSc4	převaha
korýši	korýš	k1gMnSc3	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
hmyz	hmyz	k1gInSc4	hmyz
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
největší	veliký	k2eAgFnPc4d3	veliký
rozmanitosti	rozmanitost	k1gFnPc4	rozmanitost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nové	nový	k2eAgInPc1d1	nový
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
např.	např.	kA	např.
i	i	k8xC	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
dnešních	dnešní	k2eAgInPc2d1	dnešní
druhů	druh	k1gInPc2	druh
dospělého	dospělý	k2eAgInSc2d1	dospělý
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
0,139	[number]	k4	0,139
mm	mm	kA	mm
u	u	k7c2	u
chalcidky	chalcidka	k1gFnSc2	chalcidka
druhu	druh	k1gInSc2	druh
Dicopomorpha	Dicopomorpha	k1gFnSc1	Dicopomorpha
echmepterygis	echmepterygis	k1gFnSc2	echmepterygis
až	až	k6eAd1	až
do	do	k7c2	do
567	[number]	k4	567
mm	mm	kA	mm
u	u	k7c2	u
strašilky	strašilka	k1gFnSc2	strašilka
druhu	druh	k1gInSc2	druh
Phobaeticus	Phobaeticus	k1gInSc1	Phobaeticus
chani	chan	k1gMnPc1	chan
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěžší	těžký	k2eAgInSc1d3	nejtěžší
zdokumentovaný	zdokumentovaný	k2eAgInSc1d1	zdokumentovaný
současný	současný	k2eAgInSc1d1	současný
druh	druh	k1gInSc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
kobylka	kobylka	k1gFnSc1	kobylka
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Deinacrida	Deinacrid	k1gMnSc2	Deinacrid
vážicí	vážicí	k2eAgFnSc2d1	vážicí
70	[number]	k4	70
g.	g.	k?	g.
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
žil	žít	k5eAaImAgMnS	žít
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc4d2	veliký
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
známa	znám	k2eAgFnSc1d1	známa
je	být	k5eAaImIp3nS	být
vážka	vážka	k1gFnSc1	vážka
Meganeura	Meganeur	k1gMnSc2	Meganeur
monyi	mony	k1gFnSc2	mony
z	z	k7c2	z
karbonu	karbon	k1gInSc2	karbon
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
rozpětí	rozpětí	k1gNnSc4	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
až	až	k8xS	až
750	[number]	k4	750
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Evoluční	evoluční	k2eAgInSc1d1	evoluční
vztah	vztah	k1gInSc1	vztah
hmyzu	hmyz	k1gInSc2	hmyz
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
skupinám	skupina	k1gFnPc3	skupina
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
důkazy	důkaz	k1gInPc1	důkaz
že	že	k8xS	že
hmyz	hmyz	k1gInSc1	hmyz
a	a	k8xC	a
korýši	korýš	k1gMnPc1	korýš
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgInPc4d1	stejný
předky	předek	k1gInPc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
třída	třída	k1gFnSc1	třída
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ze	z	k7c2	z
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
popsaných	popsaný	k2eAgInPc2d1	popsaný
druhů	druh	k1gInPc2	druh
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
100	[number]	k4	100
druhů	druh	k1gInPc2	druh
vážek	vážka	k1gFnPc2	vážka
<g/>
,	,	kIx,	,
2	[number]	k4	2
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
kudlanek	kudlanka	k1gFnPc2	kudlanka
<g/>
,	,	kIx,	,
20	[number]	k4	20
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
rovnokřídlých	rovnokřídlý	k2eAgInPc2d1	rovnokřídlý
<g/>
,	,	kIx,	,
170	[number]	k4	170
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
120	[number]	k4	120
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
dvoukřídlých	dvoukřídlí	k1gMnPc2	dvoukřídlí
<g/>
,	,	kIx,	,
82	[number]	k4	82
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
ploštic	ploštice	k1gFnPc2	ploštice
<g/>
,	,	kIx,	,
350	[number]	k4	350
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
brouků	brouk	k1gMnPc2	brouk
a	a	k8xC	a
110	[number]	k4	110
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
blanokřídlých	blanokřídlí	k1gMnPc2	blanokřídlí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
včely	včela	k1gFnSc2	včela
a	a	k8xC	a
mravenci	mravenec	k1gMnPc1	mravenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
Latinsky	latinsky	k6eAd1	latinsky
Insecta	Insecta	k1gFnSc1	Insecta
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
dělený	dělený	k2eAgMnSc1d1	dělený
do	do	k7c2	do
sekcí	sekce	k1gFnPc2	sekce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
entomologie	entomologie	k1gFnSc1	entomologie
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
ε	ε	k?	ε
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
opět	opět	k6eAd1	opět
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
vyřezaný	vyřezaný	k2eAgMnSc1d1	vyřezaný
do	do	k7c2	do
sekcí	sekce	k1gFnPc2	sekce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Insecta	Insect	k1gInSc2	Insect
tedy	tedy	k9	tedy
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozdělení	rozdělení	k1gNnSc2	rozdělení
těla	tělo	k1gNnSc2	tělo
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
A	A	kA	A
(	(	kIx(	(
<g/>
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
tělových	tělový	k2eAgInPc2d1	tělový
článků	článek	k1gInPc2	článek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hruď	hruď	k1gFnSc4	hruď
B	B	kA	B
(	(	kIx(	(
<g/>
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
tělových	tělový	k2eAgInPc2d1	tělový
článků	článek	k1gInPc2	článek
<g/>
)	)	kIx)	)
a	a	k8xC	a
zadeček	zadeček	k1gInSc4	zadeček
C	C	kA	C
(	(	kIx(	(
<g/>
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
viditelných	viditelný	k2eAgInPc2d1	viditelný
článků	článek	k1gInPc2	článek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
hmyz	hmyz	k1gInSc4	hmyz
používán	používat	k5eAaImNgInS	používat
také	také	k6eAd1	také
název	název	k1gInSc1	název
Hexapoda	Hexapoda	k1gFnSc1	Hexapoda
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
šestinozí	šestinohý	k2eAgMnPc1d1	šestinohý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnSc4d2	vyšší
skupinu	skupina	k1gFnSc4	skupina
(	(	kIx(	(
<g/>
podkmen	podkmen	k1gInSc4	podkmen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
nebylo	být	k5eNaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
vztah	vztah	k1gInSc1	vztah
má	mít	k5eAaImIp3nS	mít
hmyz	hmyz	k1gInSc4	hmyz
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
skupinám	skupina	k1gFnPc3	skupina
členovců	členovec	k1gMnPc2	členovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
genetických	genetický	k2eAgFnPc2d1	genetická
studií	studie	k1gFnPc2	studie
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
hmyz	hmyz	k1gInSc1	hmyz
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
"	"	kIx"	"
<g/>
šestinozí	šestinohý	k2eAgMnPc1d1	šestinohý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
blízkým	blízký	k2eAgMnSc7d1	blízký
příbuzným	příbuzný	k1gMnSc7	příbuzný
korýšů	korýš	k1gMnPc2	korýš
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
jejich	jejich	k3xOp3gFnSc7	jejich
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
skupinou	skupina	k1gFnSc7	skupina
(	(	kIx(	(
<g/>
hmyz	hmyz	k1gInSc1	hmyz
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
jistých	jistý	k2eAgMnPc2d1	jistý
korýšů	korýš	k1gMnPc2	korýš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejpříbuznějšími	příbuzný	k2eAgMnPc7d3	nejpříbuznější
korýši	korýš	k1gMnPc7	korýš
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
lupenonožci	lupenonožec	k1gMnPc1	lupenonožec
(	(	kIx(	(
<g/>
Branchiopoda	Branchiopoda	k1gMnSc1	Branchiopoda
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
rakovci	rakovec	k1gMnPc1	rakovec
(	(	kIx(	(
<g/>
Malacostraca	Malacostraca	k1gMnSc1	Malacostraca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nejnovějších	nový	k2eAgFnPc2d3	nejnovější
studií	studie	k1gFnPc2	studie
spíše	spíše	k9	spíše
obskurní	obskurní	k2eAgFnSc2d1	obskurní
skupiny	skupina	k1gFnSc2	skupina
Cephalocarida	Cephalocarid	k1gMnSc2	Cephalocarid
a	a	k8xC	a
Remipedia	Remipedium	k1gNnPc1	Remipedium
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
suchozemští	suchozemský	k2eAgMnPc1d1	suchozemský
členovci	členovec	k1gMnPc1	členovec
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
stonožky	stonožka	k1gFnPc1	stonožka
<g/>
,	,	kIx,	,
štíři	štír	k1gMnPc1	štír
a	a	k8xC	a
pavouci	pavouk	k1gMnPc1	pavouk
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
laiky	laik	k1gMnPc7	laik
zaměňováni	zaměňovat	k5eAaImNgMnP	zaměňovat
za	za	k7c4	za
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
podobné	podobný	k2eAgNnSc1d1	podobné
hmyzímu	hmyzí	k2eAgNnSc3d1	hmyzí
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
spojené	spojený	k2eAgInPc4d1	spojený
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
všichni	všechen	k3xTgMnPc1	všechen
členovci	členovec	k1gMnPc1	členovec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bližším	blízký	k2eAgInSc6d2	bližší
pohledu	pohled	k1gInSc6	pohled
se	se	k3xPyFc4	se
však	však	k9	však
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
hmyzu	hmyz	k1gInSc2	hmyz
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
šest	šest	k4xCc4	šest
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
podkmene	podkmen	k1gInSc2	podkmen
Hexapoda	Hexapod	k1gMnSc2	Hexapod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
skupin	skupina	k1gFnPc2	skupina
jako	jako	k8xC	jako
např.	např.	kA	např.
chvostoskoci	chvostoskok	k1gMnPc1	chvostoskok
(	(	kIx(	(
<g/>
Collembolla	Collembolla	k1gMnSc1	Collembolla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
pokládány	pokládat	k5eAaImNgInP	pokládat
za	za	k7c4	za
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
je	on	k3xPp3gFnPc4	on
z	z	k7c2	z
hmyzu	hmyz	k1gInSc2	hmyz
vyřazují	vyřazovat	k5eAaImIp3nP	vyřazovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
případ	případ	k1gInSc4	případ
zbytku	zbytek	k1gInSc2	zbytek
skupiny	skupina	k1gFnSc2	skupina
Entognatha	Entognatha	k1gMnSc1	Entognatha
-	-	kIx~	-
hmyzenek	hmyzenka	k1gFnPc2	hmyzenka
<g/>
,	,	kIx,	,
chvostoskoků	chvostoskok	k1gMnPc2	chvostoskok
a	a	k8xC	a
vidličnatek	vidličnatka	k1gFnPc2	vidličnatka
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
hmyz	hmyz	k1gInSc1	hmyz
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členovců	členovec	k1gMnPc2	členovec
především	především	k6eAd1	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
"	"	kIx"	"
<g/>
ektoghátní	ektoghátní	k2eAgMnSc1d1	ektoghátní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
jevnočelistný	jevnočelistný	k2eAgMnSc1d1	jevnočelistný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
úplně	úplně	k6eAd1	úplně
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
odkryté	odkrytý	k2eAgNnSc4d1	odkryté
ústní	ústní	k2eAgNnSc4d1	ústní
ústrojí	ústrojí	k1gNnSc4	ústrojí
vyčnívající	vyčnívající	k2eAgNnSc4d1	vyčnívající
z	z	k7c2	z
hlavové	hlavový	k2eAgFnSc2d1	hlavová
schránky	schránka	k1gFnSc2	schránka
a	a	k8xC	a
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zadeček	zadeček	k1gInSc1	zadeček
(	(	kIx(	(
<g/>
abdomen	abdomen	k1gInSc1	abdomen
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
jedenáct	jedenáct	k4xCc1	jedenáct
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
pravý	pravý	k2eAgInSc1d1	pravý
hmyz	hmyz	k1gInSc1	hmyz
někdy	někdy	k6eAd1	někdy
uváděn	uvádět	k5eAaImNgInS	uvádět
právě	právě	k6eAd1	právě
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ectognatha	Ectognatha	k1gMnSc1	Ectognatha
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
hmyzích	hmyzí	k2eAgMnPc2d1	hmyzí
dospělců	dospělec	k1gMnPc2	dospělec
je	být	k5eAaImIp3nS	být
okřídleno	okřídlen	k2eAgNnSc1d1	okřídlen
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
infratřídy	infratřída	k1gMnSc2	infratřída
Neoptera	Neopter	k1gMnSc2	Neopter
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
Orthopteroida	Orthopteroida	k1gFnSc1	Orthopteroida
(	(	kIx(	(
<g/>
s	s	k7c7	s
párovými	párový	k2eAgInPc7d1	párový
přívěsky	přívěsek	k1gInPc7	přívěsek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
cerky	cerka	k1gFnSc2	cerka
-	-	kIx~	-
Cercus	Cercus	k1gInSc1	Cercus
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hemipteroida	Hemipteroida	k1gFnSc1	Hemipteroida
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
párových	párový	k2eAgInPc2d1	párový
přívěsků	přívěsek	k1gInPc2	přívěsek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
nižší	nízký	k2eAgInPc1d2	nižší
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc1d2	vyšší
Exopterygota	Exopterygota	k1gFnSc1	Exopterygota
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
článkované	článkovaný	k2eAgNnSc1d1	článkované
tělo	tělo	k1gNnSc1	tělo
podporované	podporovaný	k2eAgNnSc1d1	podporované
exoskeletonem	exoskeleton	k1gInSc7	exoskeleton
<g/>
,	,	kIx,	,
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
vnějším	vnější	k2eAgInSc7d1	vnější
pláštěm	plášť	k1gInSc7	plášť
tvořeným	tvořený	k2eAgInSc7d1	tvořený
chitinem	chitin	k1gInSc7	chitin
a	a	k8xC	a
proteiny	protein	k1gInPc7	protein
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
(	(	kIx(	(
<g/>
caput	caput	k1gInSc4	caput
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hruď	hruď	k1gFnSc1	hruď
(	(	kIx(	(
<g/>
thorax	thorax	k1gInSc1	thorax
<g/>
)	)	kIx)	)
a	a	k8xC	a
zadeček	zadeček	k1gInSc1	zadeček
(	(	kIx(	(
<g/>
abdomen	abdomen	k1gInSc1	abdomen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
nese	nést	k5eAaImIp3nS	nést
obvykle	obvykle	k6eAd1	obvykle
pár	pár	k4xCyI	pár
smyslových	smyslový	k2eAgNnPc2d1	smyslové
tykadel	tykadlo	k1gNnPc2	tykadlo
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
složených	složený	k2eAgNnPc2d1	složené
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
ústní	ústní	k2eAgFnSc7d1	ústní
ústrojí	ústroj	k1gFnSc7	ústroj
<g/>
.	.	kIx.	.
</s>
<s>
Hruď	hruď	k1gFnSc1	hruď
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
předohruď	předohruď	k1gFnSc1	předohruď
-	-	kIx~	-
prothorax	prothorax	k1gInSc1	prothorax
<g/>
,	,	kIx,	,
středohruď	středohruď	k1gFnSc1	středohruď
-	-	kIx~	-
mesothorax	mesothorax	k1gInSc1	mesothorax
a	a	k8xC	a
zadohruď	zadohruď	k1gFnSc1	zadohruď
-	-	kIx~	-
metahorax	metahorax	k1gInSc1	metahorax
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
článek	článek	k1gInSc1	článek
hrudi	hruď	k1gFnSc2	hruď
nese	nést	k5eAaImIp3nS	nést
jeden	jeden	k4xCgInSc4	jeden
pár	pár	k4xCyI	pár
článkovaných	článkovaný	k2eAgFnPc6d1	článkovaná
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středohrudi	středohruď	k1gFnSc6	středohruď
a	a	k8xC	a
zadohrudi	zadohruď	k1gFnSc6	zadohruď
se	se	k3xPyFc4	se
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
hmyzu	hmyz	k1gInSc2	hmyz
nachází	nacházet	k5eAaImIp3nS	nacházet
2	[number]	k4	2
páry	pára	k1gFnSc2	pára
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Zadeček	zadeček	k1gInSc1	zadeček
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
redukované	redukovaný	k2eAgInPc1d1	redukovaný
nebo	nebo	k8xC	nebo
sloučené	sloučený	k2eAgInPc1d1	sloučený
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dýchací	dýchací	k2eAgFnPc4d1	dýchací
<g/>
,	,	kIx,	,
vylučovací	vylučovací	k2eAgFnPc4d1	vylučovací
a	a	k8xC	a
reprodukční	reprodukční	k2eAgFnPc4d1	reprodukční
struktury	struktura	k1gFnPc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
na	na	k7c4	na
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
břišní	břišní	k2eAgFnSc4d1	břišní
(	(	kIx(	(
<g/>
ventrální	ventrální	k2eAgFnSc4d1	ventrální
<g/>
)	)	kIx)	)
nervovou	nervový	k2eAgFnSc4d1	nervová
pásku	páska	k1gFnSc4	páska
<g/>
.	.	kIx.	.
</s>
<s>
Hlavový	hlavový	k2eAgInSc1d1	hlavový
oddíl	oddíl	k1gInSc1	oddíl
(	(	kIx(	(
<g/>
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
splynutím	splynutí	k1gNnSc7	splynutí
šesti	šest	k4xCc2	šest
původních	původní	k2eAgInPc2d1	původní
článků	článek	k1gInPc2	článek
v	v	k7c4	v
jednolitý	jednolitý	k2eAgInSc4d1	jednolitý
celek	celek	k1gInSc4	celek
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc1	šest
párů	pár	k1gInPc2	pár
tzv.	tzv.	kA	tzv.
ganglií	ganglie	k1gFnPc2	ganglie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
tři	tři	k4xCgInPc1	tři
páry	pár	k1gInPc1	pár
splynuly	splynout	k5eAaPmAgInP	splynout
v	v	k7c4	v
nadjícnový	nadjícnový	k2eAgInSc4d1	nadjícnový
ganglion	ganglion	k1gNnSc1	ganglion
(	(	kIx(	(
<g/>
mozek	mozek	k1gInSc1	mozek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tři	tři	k4xCgInPc1	tři
následující	následující	k2eAgInPc1d1	následující
páry	pár	k1gInPc1	pár
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
podjícnový	podjícnový	k2eAgInSc4d1	podjícnový
ganglion	ganglion	k1gNnSc4	ganglion
<g/>
.	.	kIx.	.
</s>
<s>
Hrudní	hrudní	k2eAgInPc1d1	hrudní
segmenty	segment	k1gInPc1	segment
mají	mít	k5eAaImIp3nP	mít
jeden	jeden	k4xCgMnSc1	jeden
ganglion	ganglion	k1gNnSc1	ganglion
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgFnPc1d1	spojená
do	do	k7c2	do
páru	pár	k1gInSc2	pár
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
vychází	vycházet	k5eAaImIp3nS	vycházet
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
na	na	k7c4	na
segment	segment	k1gInSc4	segment
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaPmF	nalézt
také	také	k9	také
v	v	k7c6	v
zadečku	zadeček	k1gInSc6	zadeček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
prvních	první	k4xOgFnPc6	první
osmi	osm	k4xCc2	osm
článcích	článek	k1gInPc6	článek
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
má	mít	k5eAaImIp3nS	mít
počet	počet	k1gInSc1	počet
ganglionů	ganglion	k1gInPc2	ganglion
redukovaný	redukovaný	k2eAgMnSc1d1	redukovaný
díky	díky	k7c3	díky
jejich	jejich	k3xOp3gNnSc3	jejich
sloučení	sloučení	k1gNnSc3	sloučení
nebo	nebo	k8xC	nebo
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
švábi	šváb	k1gMnPc1	šváb
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
zadečku	zadeček	k1gInSc6	zadeček
právě	právě	k9	právě
šest	šest	k4xCc4	šest
ganglionů	ganglion	k1gInPc2	ganglion
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
sršeň	sršeň	k1gFnSc1	sršeň
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vespa	Vespa	k1gFnSc1	Vespa
crabro	crabro	k6eAd1	crabro
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc1	dva
v	v	k7c6	v
hrudi	hruď	k1gFnSc6	hruď
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
v	v	k7c6	v
zadečku	zadeček	k1gInSc6	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
moucha	moucha	k1gFnSc1	moucha
domácí	domácí	k2eAgFnSc1d1	domácí
(	(	kIx(	(
<g/>
Musca	Musc	k2eAgFnSc1d1	Musca
domestica	domestica	k1gFnSc1	domestica
<g/>
)	)	kIx)	)
splynuly	splynout	k5eAaPmAgFnP	splynout
všechny	všechen	k3xTgFnPc1	všechen
tělové	tělový	k2eAgFnPc1d1	tělová
gangliony	ganglion	k1gInPc4	ganglion
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
velkého	velký	k2eAgInSc2d1	velký
hrudního	hrudní	k2eAgInSc2d1	hrudní
ganglionu	ganglion	k1gInSc2	ganglion
<g/>
.	.	kIx.	.
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
žebříčkovitá	žebříčkovitý	k2eAgFnSc1d1	žebříčkovitá
Hmyz	hmyz	k1gInSc1	hmyz
má	mít	k5eAaImIp3nS	mít
kompletní	kompletní	k2eAgInSc1d1	kompletní
trávicí	trávicí	k2eAgInSc1d1	trávicí
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
základem	základ	k1gInSc7	základ
jejich	jejich	k3xOp3gFnSc2	jejich
trávicí	trávicí	k2eAgFnSc2d1	trávicí
soustavy	soustava	k1gFnSc2	soustava
je	být	k5eAaImIp3nS	být
trubice	trubice	k1gFnSc1	trubice
probíhající	probíhající	k2eAgFnSc1d1	probíhající
od	od	k7c2	od
úst	ústa	k1gNnPc2	ústa
až	až	k9	až
po	po	k7c4	po
konečník	konečník	k1gInSc4	konečník
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
s	s	k7c7	s
neúplnými	úplný	k2eNgInPc7d1	neúplný
trávicími	trávicí	k2eAgInPc7d1	trávicí
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
u	u	k7c2	u
jednodušších	jednoduchý	k2eAgMnPc2d2	jednodušší
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
<s>
Vylučovací	vylučovací	k2eAgInSc1d1	vylučovací
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
malpighických	malpighický	k2eAgFnPc2d1	malpighický
trubic	trubice	k1gFnPc2	trubice
pro	pro	k7c4	pro
vylučování	vylučování	k1gNnSc4	vylučování
odpadních	odpadní	k2eAgFnPc2d1	odpadní
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
zadních	zadní	k2eAgFnPc2d1	zadní
vnitřností	vnitřnost	k1gFnPc2	vnitřnost
pro	pro	k7c4	pro
osmoregulaci	osmoregulace	k1gFnSc4	osmoregulace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malpighických	malpighický	k2eAgFnPc6d1	malpighický
trubicích	trubice	k1gFnPc6	trubice
se	se	k3xPyFc4	se
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
odnímají	odnímat	k5eAaImIp3nP	odnímat
odpadní	odpadní	k2eAgFnPc1d1	odpadní
látky	látka	k1gFnPc1	látka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přeměňovány	přeměňovat	k5eAaImNgInP	přeměňovat
na	na	k7c4	na
moč	moč	k1gFnSc4	moč
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
zadního	zadní	k2eAgNnSc2d1	zadní
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Koncovou	koncový	k2eAgFnSc7d1	koncová
částí	část	k1gFnSc7	část
střeva	střevo	k1gNnSc2	střevo
je	být	k5eAaImIp3nS	být
hmyz	hmyz	k1gInSc1	hmyz
schopen	schopen	k2eAgInSc1d1	schopen
zpětně	zpětně	k6eAd1	zpětně
vstřebávat	vstřebávat	k5eAaImF	vstřebávat
vodu	voda	k1gFnSc4	voda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
draselnými	draselný	k2eAgInPc7d1	draselný
a	a	k8xC	a
sodíkovými	sodíkový	k2eAgInPc7d1	sodíkový
ionty	ion	k1gInPc7	ion
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
hmyz	hmyz	k1gInSc1	hmyz
většinou	většinou	k6eAd1	většinou
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
výměšky	výměšek	k1gInPc7	výměšek
i	i	k8xC	i
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uskladňuje	uskladňovat	k5eAaImIp3nS	uskladňovat
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
zpětného	zpětný	k2eAgNnSc2d1	zpětné
vstřebávání	vstřebávání	k1gNnSc2	vstřebávání
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
odolávat	odolávat	k5eAaImF	odolávat
horkému	horký	k2eAgNnSc3d1	horké
a	a	k8xC	a
suchému	suchý	k2eAgNnSc3d1	suché
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hmyzích	hmyzí	k2eAgInPc2d1	hmyzí
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
páry	pár	k1gInPc4	pár
křídel	křídlo	k1gNnPc2	křídlo
umístěných	umístěný	k2eAgNnPc2d1	umístěné
na	na	k7c6	na
druhém	druhý	k4xOgMnSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
hrudním	hrudní	k2eAgInSc6d1	hrudní
článku	článek	k1gInSc6	článek
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc1	hmyz
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
skupinou	skupina	k1gFnSc7	skupina
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
schopnost	schopnost	k1gFnSc1	schopnost
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
veliký	veliký	k2eAgInSc4d1	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc1	hmyz
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
podtřídy	podtřída	k1gFnPc4	podtřída
<g/>
,	,	kIx,	,
na	na	k7c4	na
okřídlený	okřídlený	k2eAgInSc4d1	okřídlený
hmyz	hmyz	k1gInSc4	hmyz
(	(	kIx(	(
<g/>
Pterygota	Pterygota	k1gFnSc1	Pterygota
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc4	jejich
bezkřídlé	bezkřídlý	k2eAgMnPc4d1	bezkřídlý
příbuzné	příbuzný	k1gMnPc4	příbuzný
(	(	kIx(	(
<g/>
Apterygota	Apterygota	k1gFnSc1	Apterygota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
hmyzu	hmyz	k1gInSc2	hmyz
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
úplně	úplně	k6eAd1	úplně
dokonale	dokonale	k6eAd1	dokonale
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
<g/>
,	,	kIx,	,
spočívá	spočívat	k5eAaImIp3nS	spočívat
na	na	k7c6	na
turbulentních	turbulentní	k2eAgInPc6d1	turbulentní
aerodynamických	aerodynamický	k2eAgInPc6d1	aerodynamický
efektech	efekt	k1gInPc6	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Primitivní	primitivní	k2eAgFnPc1d1	primitivní
skupiny	skupina	k1gFnPc1	skupina
hmyzu	hmyz	k1gInSc2	hmyz
využívají	využívat	k5eAaImIp3nP	využívat
síly	síla	k1gFnPc1	síla
svalů	sval	k1gInPc2	sval
působením	působení	k1gNnSc7	působení
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
strukturu	struktura	k1gFnSc4	struktura
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Pokročilejší	pokročilý	k2eAgFnPc1d2	pokročilejší
skupiny	skupina	k1gFnPc1	skupina
působí	působit	k5eAaImIp3nP	působit
svaly	sval	k1gInPc4	sval
na	na	k7c4	na
stěnu	stěna	k1gFnSc4	stěna
hrudi	hruď	k1gFnSc2	hruď
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c4	na
sklopná	sklopný	k2eAgNnPc4d1	sklopné
křídla	křídlo	k1gNnPc4	křídlo
nepřímo	přímo	k6eNd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
svaly	sval	k1gInPc1	sval
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
provádět	provádět	k5eAaImF	provádět
kontrakci	kontrakce	k1gFnSc4	kontrakce
bez	bez	k7c2	bez
nervových	nervový	k2eAgInPc2d1	nervový
popudů	popud	k1gInPc2	popud
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
rychleji	rychle	k6eAd2	rychle
tlouci	tlouct	k5eAaImF	tlouct
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
hmyzí	hmyzí	k2eAgInPc4d1	hmyzí
let	let	k1gInSc4	let
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
vnější	vnější	k2eAgFnSc1d1	vnější
kostra	kostra	k1gFnSc1	kostra
<g/>
,	,	kIx,	,
pokožka	pokožka	k1gFnSc1	pokožka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvěma	dva	k4xCgFnPc7	dva
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
;	;	kIx,	;
epikutikula	epikutikula	k1gFnSc1	epikutikula
je	být	k5eAaImIp3nS	být
tenká	tenký	k2eAgFnSc1d1	tenká
a	a	k8xC	a
vosková	voskový	k2eAgFnSc1d1	vosková
<g/>
,	,	kIx,	,
vodě	voda	k1gFnSc6	voda
odolná	odolný	k2eAgFnSc1d1	odolná
vrchní	vrchní	k2eAgFnSc1d1	vrchní
vrstva	vrstva	k1gFnSc1	vrstva
neobsahující	obsahující	k2eNgFnSc1d1	neobsahující
chitin	chitin	k1gInSc1	chitin
<g/>
,	,	kIx,	,
vrstva	vrstva	k1gFnSc1	vrstva
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
prokutikula	prokutikula	k1gFnSc1	prokutikula
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
tvořená	tvořený	k2eAgFnSc1d1	tvořená
chitinem	chitin	k1gInSc7	chitin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
hodně	hodně	k6eAd1	hodně
silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
vrchní	vrchní	k2eAgFnSc1d1	vrchní
vrstva	vrstva	k1gFnSc1	vrstva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvěma	dva	k4xCgFnPc7	dva
částmi	část	k1gFnPc7	část
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgMnSc1d1	vnější
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
exokutikula	exokutikula	k1gFnSc1	exokutikula
zatímco	zatímco	k8xS	zatímco
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
je	být	k5eAaImIp3nS	být
endokutikula	endokutikula	k1gFnSc1	endokutikula
<g/>
.	.	kIx.	.
</s>
<s>
Tuhá	tuhý	k2eAgFnSc1d1	tuhá
a	a	k8xC	a
ohebná	ohebný	k2eAgFnSc1d1	ohebná
endokutikula	endokutikula	k1gFnSc1	endokutikula
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
z	z	k7c2	z
početných	početný	k2eAgFnPc2d1	početná
vrstev	vrstva	k1gFnPc2	vrstva
vláknitého	vláknitý	k2eAgInSc2d1	vláknitý
chitinu	chitin	k1gInSc2	chitin
a	a	k8xC	a
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
prokládaných	prokládaný	k2eAgFnPc2d1	prokládaná
křížem	křížem	k6eAd1	křížem
krážem	krážem	k6eAd1	krážem
jako	jako	k8xS	jako
v	v	k7c4	v
obložený	obložený	k2eAgInSc4d1	obložený
chléb	chléb	k1gInSc4	chléb
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
exokutikula	exokutikula	k1gFnSc1	exokutikula
je	být	k5eAaImIp3nS	být
sklerotizována	sklerotizován	k2eAgFnSc1d1	sklerotizován
<g/>
.	.	kIx.	.
</s>
<s>
Exoskelet	Exoskelet	k1gInSc1	Exoskelet
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
lehký	lehký	k2eAgInSc1d1	lehký
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
naprosto	naprosto	k6eAd1	naprosto
neproniknutelný	proniknutelný	k2eNgInSc1d1	neproniknutelný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dýchání	dýchání	k1gNnSc3	dýchání
hmyz	hmyz	k1gInSc4	hmyz
používá	používat	k5eAaImIp3nS	používat
systém	systém	k1gInSc1	systém
vzdušnic	vzdušnice	k1gFnPc2	vzdušnice
(	(	kIx(	(
<g/>
trachejí	trachej	k1gFnPc2	trachej
<g/>
)	)	kIx)	)
otevřených	otevřený	k2eAgInPc2d1	otevřený
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
hrudi	hruď	k1gFnSc6	hruď
a	a	k8xC	a
zadečku	zadeček	k1gInSc6	zadeček
stigmaty	stigma	k1gNnPc7	stigma
(	(	kIx(	(
<g/>
průduchy	průduch	k1gInPc7	průduch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
k	k	k7c3	k
vnitřním	vnitřní	k2eAgFnPc3d1	vnitřní
tkáním	tkáň	k1gFnPc3	tkáň
pomocí	pomoc	k1gFnPc2	pomoc
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
rozvětvující	rozvětvující	k2eAgFnPc1d1	rozvětvující
sítě	síť	k1gFnPc1	síť
tohoto	tento	k3xDgInSc2	tento
tracheálního	tracheální	k2eAgInSc2d1	tracheální
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
tělový	tělový	k2eAgInSc4d1	tělový
segment	segment	k1gInSc4	segment
obvykle	obvykle	k6eAd1	obvykle
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc1	jeden
pár	pár	k1gInSc1	pár
tracheálních	tracheální	k2eAgFnPc2d1	tracheální
trubic	trubice	k1gFnPc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
vzdušnice	vzdušnice	k1gFnSc2	vzdušnice
v	v	k7c6	v
osmi	osm	k4xCc6	osm
segmentech	segment	k1gInPc6	segment
zadečku	zadeček	k1gInSc2	zadeček
a	a	k8xC	a
dvou	dva	k4xCgInPc6	dva
hrudních	hrudní	k2eAgInPc6d1	hrudní
segmentech	segment	k1gInPc6	segment
(	(	kIx(	(
<g/>
omezených	omezený	k2eAgFnPc2d1	omezená
na	na	k7c4	na
mesothorax	mesothorax	k1gInSc4	mesothorax
a	a	k8xC	a
metathorax	metathorax	k1gInSc4	metathorax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
hmyzu	hmyz	k1gInSc2	hmyz
mají	mít	k5eAaImIp3nP	mít
redukovaný	redukovaný	k2eAgInSc4d1	redukovaný
počet	počet	k1gInSc4	počet
stigmat	stigma	k1gNnPc2	stigma
a	a	k8xC	a
některý	některý	k3yIgInSc4	některý
létající	létající	k2eAgInSc4d1	létající
hmyz	hmyz	k1gInSc4	hmyz
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc1	žádný
průduchy	průduch	k1gInPc1	průduch
v	v	k7c6	v
segmentech	segment	k1gInPc6	segment
zadečku	zadeček	k1gInSc2	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
také	také	k9	také
o	o	k7c4	o
fyzikální	fyzikální	k2eAgInSc4d1	fyzikální
limit	limit	k1gInSc4	limit
týkající	týkající	k2eAgInSc4d1	týkající
se	se	k3xPyFc4	se
tlaku	tlak	k1gInSc3	tlak
na	na	k7c4	na
stěny	stěna	k1gFnPc4	stěna
vzdušnic	vzdušnice	k1gFnPc2	vzdušnice
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
jsou	být	k5eAaImIp3nP	být
přes	přes	k7c4	přes
vyztužení	vyztužení	k1gNnSc1	vyztužení
chitinovými	chitinový	k2eAgFnPc7d1	chitinová
páskami	páska	k1gFnPc7	páska
schopny	schopen	k2eAgFnPc1d1	schopna
odolat	odolat	k5eAaPmF	odolat
bez	bez	k7c2	bez
zhroucení	zhroucení	k1gNnSc2	zhroucení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
hmyz	hmyz	k1gInSc4	hmyz
tak	tak	k6eAd1	tak
relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušnice	vzdušnice	k1gFnPc1	vzdušnice
mají	mít	k5eAaImIp3nP	mít
svaly	sval	k1gInPc4	sval
řízené	řízený	k2eAgFnSc2d1	řízená
záklopky	záklopka	k1gFnSc2	záklopka
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc1d1	umožňující
hmyzu	hmyz	k1gInSc3	hmyz
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
utopení	utopení	k1gNnSc1	utopení
pod	pod	k7c4	pod
vodu	voda	k1gFnSc4	voda
nebo	nebo	k8xC	nebo
zabránit	zabránit	k5eAaPmF	zabránit
vysušení	vysušení	k1gNnSc4	vysušení
<g/>
.	.	kIx.	.
</s>
<s>
Průduchy	průduch	k1gInPc1	průduch
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
chloupky	chloupek	k1gInPc4	chloupek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
filtrovat	filtrovat	k5eAaImF	filtrovat
vstupující	vstupující	k2eAgInSc4d1	vstupující
vzduch	vzduch	k1gInSc4	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Madagaskarští	madagaskarský	k2eAgMnPc1d1	madagaskarský
syčící	syčící	k2eAgMnPc1d1	syčící
švábi	šváb	k1gMnPc1	šváb
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
ohrožení	ohrožení	k1gNnSc6	ohrožení
průduchy	průduch	k1gInPc1	průduch
na	na	k7c4	na
hlasité	hlasitý	k2eAgNnSc4d1	hlasité
vypouštění	vypouštění	k1gNnSc4	vypouštění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Dýchání	dýchání	k1gNnSc1	dýchání
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
pasivní	pasivní	k2eAgInSc4d1	pasivní
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Výměna	výměna	k1gFnSc1	výměna
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
regulována	regulován	k2eAgFnSc1d1	regulována
a	a	k8xC	a
kontrolována	kontrolován	k2eAgFnSc1d1	kontrolována
svaly	sval	k1gInPc7	sval
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vodního	vodní	k2eAgInSc2d1	vodní
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
další	další	k2eAgFnSc1d1	další
modifikace	modifikace	k1gFnSc1	modifikace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dýchání	dýchání	k1gNnSc4	dýchání
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
vodních	vodní	k2eAgMnPc2d1	vodní
brouků	brouk	k1gMnPc2	brouk
a	a	k8xC	a
jiného	jiný	k2eAgInSc2d1	jiný
vodního	vodní	k2eAgInSc2d1	vodní
hmyzu	hmyz	k1gInSc2	hmyz
schopnost	schopnost	k1gFnSc4	schopnost
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
na	na	k7c6	na
zvláštním	zvláštní	k2eAgInSc6d1	zvláštní
povrchu	povrch	k1gInSc6	povrch
vzduchovou	vzduchový	k2eAgFnSc4d1	vzduchová
bublinu	bublina	k1gFnSc4	bublina
(	(	kIx(	(
<g/>
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
žábry	žábry	k1gFnPc4	žábry
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
dýchání	dýchání	k1gNnSc3	dýchání
objemově	objemově	k6eAd1	objemově
stálý	stálý	k2eAgInSc4d1	stálý
plastron	plastron	k1gInSc4	plastron
nebo	nebo	k8xC	nebo
dýchací	dýchací	k2eAgFnPc4d1	dýchací
trubičky	trubička	k1gFnPc4	trubička
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
hmyzí	hmyzí	k2eAgFnPc1d1	hmyzí
larvy	larva	k1gFnPc1	larva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tráví	trávit	k5eAaImIp3nP	trávit
svůj	svůj	k3xOyFgInSc4	svůj
vývoj	vývoj	k1gInSc4	vývoj
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zřekly	zřeknout	k5eAaPmAgFnP	zřeknout
dýchání	dýchání	k1gNnSc4	dýchání
pomocí	pomocí	k7c2	pomocí
vzdušnic	vzdušnice	k1gFnPc2	vzdušnice
a	a	k8xC	a
kyslík	kyslík	k1gInSc1	kyslík
přijímají	přijímat	k5eAaImIp3nP	přijímat
žábrami	žábry	k1gFnPc7	žábry
nebo	nebo	k8xC	nebo
celým	celý	k2eAgNnSc7d1	celé
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Oběhová	oběhový	k2eAgFnSc1d1	oběhová
soustava	soustava	k1gFnSc1	soustava
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
členovců	členovec	k1gMnPc2	členovec
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
:	:	kIx,	:
funkci	funkce	k1gFnSc4	funkce
srdce	srdce	k1gNnSc2	srdce
zastává	zastávat	k5eAaImIp3nS	zastávat
velká	velký	k2eAgFnSc1d1	velká
hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
céva	céva	k1gFnSc1	céva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pumpuje	pumpovat	k5eAaImIp3nS	pumpovat
krvomízu	krvomíza	k1gFnSc4	krvomíza
(	(	kIx(	(
<g/>
hemolymfa	hemolymf	k1gMnSc2	hemolymf
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
dopředu	dopředu	k6eAd1	dopředu
k	k	k7c3	k
mozkovým	mozkový	k2eAgFnPc3d1	mozková
gangliím	ganglie	k1gFnPc3	ganglie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
hemolymfa	hemolymf	k1gMnSc2	hemolymf
dostává	dostávat	k5eAaImIp3nS	dostávat
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
volně	volně	k6eAd1	volně
omývá	omývat	k5eAaImIp3nS	omývat
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozváděna	rozváděn	k2eAgFnSc1d1	rozváděna
i	i	k9	i
do	do	k7c2	do
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
zpětná	zpětný	k2eAgFnSc1d1	zpětná
cirkulace	cirkulace	k1gFnSc1	cirkulace
se	se	k3xPyFc4	se
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pomocí	pomocí	k7c2	pomocí
otvorů	otvor	k1gInPc2	otvor
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
hřbetní	hřbetní	k2eAgFnSc2d1	hřbetní
cévy	céva	k1gFnSc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
při	při	k7c6	při
uvolnění	uvolnění	k1gNnSc6	uvolnění
otevírají	otevírat	k5eAaImIp3nP	otevírat
a	a	k8xC	a
nasávají	nasávat	k5eAaImIp3nP	nasávat
hemolymfu	hemolymf	k1gInSc3	hemolymf
zpět	zpět	k6eAd1	zpět
z	z	k7c2	z
tělní	tělní	k2eAgFnSc2d1	tělní
dutiny	dutina	k1gFnSc2	dutina
do	do	k7c2	do
hřbetní	hřbetní	k2eAgFnSc2d1	hřbetní
cévy	céva	k1gFnSc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Hemolymfou	Hemolymfou	k6eAd1	Hemolymfou
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
krve	krev	k1gFnSc2	krev
rozváděny	rozváděn	k2eAgFnPc4d1	rozváděna
pouze	pouze	k6eAd1	pouze
živiny	živina	k1gFnPc4	živina
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
někteří	některý	k3yIgMnPc1	některý
jiní	jiný	k2eAgMnPc1d1	jiný
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
hmyz	hmyz	k1gInSc1	hmyz
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
cholesterol	cholesterol	k1gInSc4	cholesterol
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
ho	on	k3xPp3gMnSc4	on
přijímat	přijímat	k5eAaImF	přijímat
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
několika	několik	k4yIc7	několik
málo	málo	k6eAd1	málo
výjimkami	výjimka	k1gFnPc7	výjimka
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
také	také	k6eAd1	také
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
stravě	strava	k1gFnSc6	strava
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
řetězce	řetězec	k1gInPc4	řetězec
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
těchto	tento	k3xDgFnPc2	tento
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
vede	vést	k5eAaImIp3nS	vést
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
k	k	k7c3	k
zpoždění	zpoždění	k1gNnSc3	zpoždění
jejich	jejich	k3xOp3gInSc2	jejich
vývoje	vývoj	k1gInSc2	vývoj
nebo	nebo	k8xC	nebo
k	k	k7c3	k
deformacím	deformace	k1gFnPc3	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Chironomidae	Chironomidae	k1gInSc1	Chironomidae
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
larválního	larvální	k2eAgNnSc2d1	larvální
stadia	stadion	k1gNnSc2	stadion
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
pravé	pravý	k2eAgNnSc1d1	pravé
krevní	krevní	k2eAgNnSc1d1	krevní
barvivo	barvivo	k1gNnSc1	barvivo
podobné	podobný	k2eAgNnSc1d1	podobné
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
průdušnice	průdušnice	k1gFnPc1	průdušnice
jsou	být	k5eAaImIp3nP	být
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
často	často	k6eAd1	často
redukovány	redukován	k2eAgFnPc1d1	redukována
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
může	moct	k5eAaImIp3nS	moct
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
kyslík	kyslík	k1gInSc4	kyslík
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
žít	žít	k5eAaImF	žít
např.	např.	kA	např.
v	v	k7c6	v
bahnu	bahno	k1gNnSc6	bahno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
kyslíků	kyslík	k1gInPc2	kyslík
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jistého	jistý	k2eAgInSc2d1	jistý
druhu	druh	k1gInSc2	druh
ploštic	ploštice	k1gFnPc2	ploštice
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
páry	pár	k1gInPc1	pár
průduchů	průduch	k1gInPc2	průduch
pokryty	pokrýt	k5eAaPmNgFnP	pokrýt
blánou	blána	k1gFnSc7	blána
citlivou	citlivý	k2eAgFnSc7d1	citlivá
na	na	k7c4	na
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
určit	určit	k5eAaPmF	určit
jejich	jejich	k3xOp3gFnSc4	jejich
polohu	poloha	k1gFnSc4	poloha
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
břišní	břišní	k2eAgInSc1d1	břišní
průduch	průduch	k1gInSc1	průduch
a	a	k8xC	a
přidružená	přidružený	k2eAgFnSc1d1	přidružená
průdušnice	průdušnice	k1gFnSc1	průdušnice
jisté	jistý	k2eAgFnSc2d1	jistá
motýlí	motýlí	k2eAgFnSc2d1	motýlí
housenky	housenka	k1gFnSc2	housenka
je	být	k5eAaImIp3nS	být
upravena	upravit	k5eAaPmNgFnS	upravit
v	v	k7c4	v
průdušnicovou	průdušnicový	k2eAgFnSc4d1	průdušnicový
plíci	plíce	k1gFnSc4	plíce
adaptovanou	adaptovaný	k2eAgFnSc4d1	adaptovaná
na	na	k7c4	na
hemocytální	hemocytální	k2eAgFnSc4d1	hemocytální
výměnu	výměna	k1gFnSc4	výměna
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Krátké	Krátké	k2eAgInPc1d1	Krátké
tracheoly	tracheol	k1gInPc1	tracheol
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
průdušnice	průdušnice	k1gFnSc2	průdušnice
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
uzlech	uzel	k1gInPc6	uzel
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
bazální	bazální	k2eAgFnSc2d1	bazální
membrány	membrána	k1gFnSc2	membrána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
haemocoelu	haemocoel	k1gInSc6	haemocoel
zadečku	zadeček	k1gInSc2	zadeček
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rozptýlená	rozptýlený	k2eAgFnSc1d1	rozptýlená
tkáň	tkáň	k1gFnSc1	tkáň
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
tukové	tukový	k2eAgNnSc4d1	tukové
tělísko	tělísko	k1gNnSc4	tělísko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
akumulaci	akumulace	k1gFnSc4	akumulace
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
metabolické	metabolický	k2eAgInPc4d1	metabolický
procesy	proces	k1gInPc4	proces
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
játra	játra	k1gNnPc1	játra
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
z	z	k7c2	z
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
jsou	být	k5eAaImIp3nP	být
vejcoživorodí	vejcoživorodý	k2eAgMnPc1d1	vejcoživorodý
nebo	nebo	k8xC	nebo
živorodí	živorodý	k2eAgMnPc1d1	živorodý
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
dospělci	dospělec	k1gMnPc1	dospělec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
výjimky	výjimka	k1gFnPc1	výjimka
(	(	kIx(	(
<g/>
jev	jev	k1gInSc1	jev
zvaný	zvaný	k2eAgInSc1d1	zvaný
pedogeneze	pedogeneze	k1gFnSc2	pedogeneze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
hmyz	hmyz	k1gInSc1	hmyz
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
pohlavní	pohlavní	k2eAgNnSc4d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
zástupci	zástupce	k1gMnPc1	zástupce
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
pravidelně	pravidelně	k6eAd1	pravidelně
množí	množit	k5eAaImIp3nP	množit
partenogeneticky	partenogeneticky	k6eAd1	partenogeneticky
(	(	kIx(	(
<g/>
z	z	k7c2	z
neoplozených	oplozený	k2eNgNnPc2d1	neoplozené
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
hmyzu	hmyz	k1gInSc3	hmyz
obecně	obecně	k6eAd1	obecně
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
žloutku	žloutek	k1gInSc2	žloutek
<g/>
,	,	kIx,	,
látky	látka	k1gFnPc1	látka
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
glykolipoproteiny	glykolipoprotein	k2eAgFnPc4d1	glykolipoprotein
vitelogeniny	vitelogenina	k1gFnPc4	vitelogenina
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
obklopeny	obklopit	k5eAaPmNgInP	obklopit
podpůrnými	podpůrný	k2eAgFnPc7d1	podpůrná
buňkami	buňka	k1gFnPc7	buňka
(	(	kIx(	(
<g/>
nurse	nurse	k6eAd1	nurse
cells	cells	k6eAd1	cells
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
rovněž	rovněž	k9	rovněž
vyživují	vyživovat	k5eAaImIp3nP	vyživovat
vyvíjející	vyvíjející	k2eAgInSc4d1	vyvíjející
se	se	k3xPyFc4	se
oocyt	oocyt	k1gInSc4	oocyt
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vajíčka	vajíčko	k1gNnSc2	vajíčko
obvykle	obvykle	k6eAd1	obvykle
směřují	směřovat	k5eAaImIp3nP	směřovat
vejcovodem	vejcovod	k1gInSc7	vejcovod
ven	ven	k6eAd1	ven
a	a	k8xC	a
prochází	procházet	k5eAaImIp3nS	procházet
kolem	kolem	k7c2	kolem
spermatéky	spermatéka	k1gFnSc2	spermatéka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
uskladněny	uskladněn	k2eAgFnPc4d1	uskladněna
spermie	spermie	k1gFnPc4	spermie
od	od	k7c2	od
samce	samec	k1gInSc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Spermie	spermie	k1gFnSc1	spermie
se	se	k3xPyFc4	se
do	do	k7c2	do
samičího	samičí	k2eAgInSc2d1	samičí
rozmnožovacího	rozmnožovací	k2eAgInSc2d1	rozmnožovací
traktu	trakt	k1gInSc2	trakt
dostanou	dostat	k5eAaPmIp3nP	dostat
buď	buď	k8xC	buď
vložením	vložení	k1gNnSc7	vložení
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
orgánu	orgán	k1gInSc2	orgán
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
aedeagus	aedeagus	k1gInSc1	aedeagus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
přes	přes	k7c4	přes
výživný	výživný	k2eAgInSc4d1	výživný
"	"	kIx"	"
<g/>
balíček	balíček	k1gInSc4	balíček
<g/>
"	"	kIx"	"
spermií	spermie	k1gFnPc2	spermie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
spermatofor	spermatofor	k1gInSc1	spermatofor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
vložen	vložen	k2eAgMnSc1d1	vložen
do	do	k7c2	do
ústí	ústí	k1gNnSc2	ústí
samičí	samičí	k2eAgFnSc2d1	samičí
rozmnožovací	rozmnožovací	k2eAgFnSc2d1	rozmnožovací
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oplodnění	oplodnění	k1gNnSc6	oplodnění
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c4	o
pohlaví	pohlaví	k1gNnPc4	pohlaví
potomků	potomek	k1gMnPc2	potomek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
existují	existovat	k5eAaImIp3nP	existovat
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgInPc1	tři
systémy	systém	k1gInPc1	systém
určení	určení	k1gNnSc2	určení
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
:	:	kIx,	:
heterogamické	heterogamický	k2eAgNnSc1d1	heterogamický
pohlaví	pohlaví	k1gNnSc1	pohlaví
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
samčí	samčí	k2eAgInSc1d1	samčí
(	(	kIx(	(
<g/>
typ	typ	k1gInSc1	typ
Drosophila	Drosophil	k1gMnSc2	Drosophil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
samičí	samičí	k2eAgInSc1d1	samičí
(	(	kIx(	(
<g/>
typ	typ	k1gInSc1	typ
Abraxas	Abraxas	k1gInSc1	Abraxas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k9	konečně
např.	např.	kA	např.
u	u	k7c2	u
blanokřídlých	blanokřídlý	k2eAgMnPc2d1	blanokřídlý
jsou	být	k5eAaImIp3nP	být
samci	samec	k1gMnPc1	samec
haploidní	haploidní	k2eAgMnPc1d1	haploidní
a	a	k8xC	a
samice	samice	k1gFnPc1	samice
diploidní	diploidní	k2eAgFnPc1d1	diploidní
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
sexuální	sexuální	k2eAgInSc1d1	sexuální
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
hermafroditismus	hermafroditismus	k1gInSc4	hermafroditismus
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
podstupují	podstupovat	k5eAaImIp3nP	podstupovat
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
růstu	růst	k1gInSc2	růst
řadu	řad	k1gInSc2	řad
svlékání	svlékání	k1gNnPc2	svlékání
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
kostra	kostra	k1gFnSc1	kostra
tvořená	tvořený	k2eAgFnSc1d1	tvořená
pevnou	pevný	k2eAgFnSc7d1	pevná
kutikulou	kutikula	k1gFnSc7	kutikula
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
totiž	totiž	k9	totiž
plynulý	plynulý	k2eAgInSc1d1	plynulý
růst	růst	k1gInSc1	růst
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Svlékání	svlékání	k1gNnSc1	svlékání
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgNnSc2	který
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
těsná	těsný	k2eAgFnSc1d1	těsná
a	a	k8xC	a
růst	růst	k1gInSc4	růst
omezující	omezující	k2eAgFnSc1d1	omezující
kutikula	kutikula	k1gFnSc1	kutikula
nahrazována	nahrazovat	k5eAaImNgFnS	nahrazovat
vždy	vždy	k6eAd1	vždy
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
svlékáním	svlékání	k1gNnSc7	svlékání
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
pod	pod	k7c7	pod
dosavadní	dosavadní	k2eAgFnSc7d1	dosavadní
menší	malý	k2eAgFnSc7d2	menší
kutikulou	kutikula	k1gFnSc7	kutikula
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svlékání	svlékání	k1gNnSc6	svlékání
chitinový	chitinový	k2eAgInSc4d1	chitinový
obal	obal	k1gInSc4	obal
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
praskne	prasknout	k5eAaPmIp3nS	prasknout
a	a	k8xC	a
larva	larva	k1gFnSc1	larva
v	v	k7c6	v
poněkud	poněkud	k6eAd1	poněkud
větším	veliký	k2eAgMnSc6d2	veliký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
měkkém	měkký	k2eAgInSc6d1	měkký
kutikulárním	kutikulární	k2eAgInSc6d1	kutikulární
obalu	obal	k1gInSc6	obal
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
těsný	těsný	k2eAgInSc1d1	těsný
krunýř	krunýř	k1gInSc1	krunýř
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nějakou	nějaký	k3yIgFnSc4	nějaký
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
nová	nový	k2eAgFnSc1d1	nová
kutikula	kutikula	k1gFnSc1	kutikula
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
zpevní	zpevnit	k5eAaPmIp3nS	zpevnit
<g/>
,	,	kIx,	,
ztuhne	ztuhnout	k5eAaPmIp3nS	ztuhnout
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
pevnou	pevný	k2eAgFnSc7d1	pevná
oporou	opora	k1gFnSc7	opora
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
jsou	být	k5eAaImIp3nP	být
mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
nymfami	nymfa	k1gFnPc7	nymfa
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
nerozvinutých	rozvinutý	k2eNgNnPc2d1	nerozvinuté
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jim	on	k3xPp3gFnPc3	on
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
až	až	k9	až
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
podobají	podobat	k5eAaImIp3nP	podobat
dospělým	dospělý	k2eAgMnPc3d1	dospělý
jedincům	jedinec	k1gMnPc3	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
jedinci	jedinec	k1gMnPc7	jedinec
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
neprochází	procházet	k5eNaImIp3nS	procházet
stádiem	stádium	k1gNnSc7	stádium
kukly	kukla	k1gFnSc2	kukla
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
postupný	postupný	k2eAgInSc1d1	postupný
vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c4	v
dospělce	dospělec	k1gMnPc4	dospělec
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
proměna	proměna	k1gFnSc1	proměna
(	(	kIx(	(
<g/>
hemimetabolie	hemimetabolie	k1gFnSc1	hemimetabolie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vývojově	vývojově	k6eAd1	vývojově
pokročilejších	pokročilý	k2eAgFnPc2d2	pokročilejší
skupin	skupina	k1gFnPc2	skupina
hmyzu	hmyz	k1gInSc2	hmyz
larvy	larva	k1gFnSc2	larva
postrádají	postrádat	k5eAaImIp3nP	postrádat
základy	základ	k1gInPc1	základ
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
před	před	k7c7	před
vlastní	vlastní	k2eAgFnSc7d1	vlastní
proměnou	proměna	k1gFnSc7	proměna
prochází	procházet	k5eAaImIp3nS	procházet
stádiem	stádium	k1gNnSc7	stádium
kukly	kukla	k1gFnSc2	kukla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stádiu	stádium	k1gNnSc6	stádium
nepřijímají	přijímat	k5eNaImIp3nP	přijímat
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
s	s	k7c7	s
omezenými	omezený	k2eAgFnPc7d1	omezená
možnostmi	možnost	k1gFnPc7	možnost
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kukle	kukla	k1gFnSc6	kukla
pak	pak	k6eAd1	pak
probíhá	probíhat	k5eAaImIp3nS	probíhat
hluboká	hluboký	k2eAgFnSc1d1	hluboká
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
přestavba	přestavba	k1gFnSc1	přestavba
orgánů	orgán	k1gInPc2	orgán
larvy	larva	k1gFnSc2	larva
v	v	k7c4	v
orgány	orgán	k1gInPc4	orgán
dospělého	dospělý	k2eAgInSc2d1	dospělý
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
proměna	proměna	k1gFnSc1	proměna
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
(	(	kIx(	(
<g/>
holometabolie	holometabolie	k1gFnSc1	holometabolie
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
skupina	skupina	k1gFnSc1	skupina
hmyzu	hmyz	k1gInSc2	hmyz
někdy	někdy	k6eAd1	někdy
označovaná	označovaný	k2eAgFnSc1d1	označovaná
Endopterygota	Endopterygota	k1gFnSc1	Endopterygota
<g/>
.	.	kIx.	.
</s>
<s>
Druhům	druh	k1gMnPc3	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
patřícím	patřící	k2eAgInSc7d1	patřící
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
podřádu	podřád	k1gInSc2	podřád
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
vajíček	vajíčko	k1gNnPc2	vajíčko
líhnou	líhnout	k5eAaImIp3nP	líhnout
larvy	larva	k1gFnPc1	larva
připomínající	připomínající	k2eAgFnPc1d1	připomínající
"	"	kIx"	"
<g/>
červa	červ	k1gMnSc2	červ
<g/>
"	"	kIx"	"
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
larvy	larva	k1gFnPc1	larva
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
různých	různý	k2eAgFnPc2d1	různá
forem	forma	k1gFnPc2	forma
<g/>
:	:	kIx,	:
eruciforma	eruciforma	k1gFnSc1	eruciforma
(	(	kIx(	(
<g/>
housenka	housenka	k1gFnSc1	housenka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scarbaeiforma	scarbaeiforma	k1gFnSc1	scarbaeiforma
(	(	kIx(	(
<g/>
ponrava	ponrava	k1gFnSc1	ponrava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
campodeiforma	campodeiforma	k1gFnSc1	campodeiforma
(	(	kIx(	(
<g/>
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
<g/>
,	,	kIx,	,
zploštěná	zploštěný	k2eAgFnSc1d1	zploštěná
a	a	k8xC	a
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
elateriforma	elateriforma	k1gFnSc1	elateriforma
(	(	kIx(	(
<g/>
drátovec	drátovec	k1gInSc1	drátovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
vermiforma	vermiforma	k1gFnSc1	vermiforma
(	(	kIx(	(
<g/>
červ	červ	k1gMnSc1	červ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Larva	larva	k1gFnSc1	larva
postupně	postupně	k6eAd1	postupně
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
zakuklí	zakuklit	k5eAaPmIp3nS	zakuklit
v	v	k7c6	v
zámotku	zámotek	k1gInSc6	zámotek
nebo	nebo	k8xC	nebo
kukle	kukla	k1gFnSc6	kukla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různého	různý	k2eAgInSc2d1	různý
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
rozeznávány	rozeznáván	k2eAgInPc1d1	rozeznáván
tři	tři	k4xCgInPc1	tři
základní	základní	k2eAgInPc1d1	základní
typy	typ	k1gInPc1	typ
kukel	kukla	k1gFnPc2	kukla
<g/>
:	:	kIx,	:
kuklu	kukla	k1gFnSc4	kukla
mumiovou	mumiový	k2eAgFnSc4d1	mumiová
(	(	kIx(	(
<g/>
pupa	pupa	k1gFnSc1	pupa
obtecta	obtecta	k1gMnSc1	obtecta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
hlava	hlava	k1gFnSc1	hlava
s	s	k7c7	s
tykadly	tykadlo	k1gNnPc7	tykadlo
i	i	k8xC	i
sosákem	sosák	k1gInSc7	sosák
<g/>
,	,	kIx,	,
kuklu	kukla	k1gFnSc4	kukla
volnou	volnou	k6eAd1	volnou
(	(	kIx(	(
<g/>
pupa	pupa	k1gFnSc1	pupa
libera	libera	k1gFnSc1	libera
nebo	nebo	k8xC	nebo
také	také	k9	také
pupa	pupa	k1gFnSc1	pupa
exarata	exarata	k1gFnSc1	exarata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
zřetelné	zřetelný	k2eAgInPc4d1	zřetelný
základy	základ	k1gInPc4	základ
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
odstávající	odstávající	k2eAgFnSc1d1	odstávající
od	od	k7c2	od
vlastní	vlastní	k2eAgFnSc2d1	vlastní
kukly	kukla	k1gFnSc2	kukla
a	a	k8xC	a
kukla	kukla	k1gFnSc1	kukla
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
(	(	kIx(	(
<g/>
pupa	pupa	k1gFnSc1	pupa
coarctata	coarctata	k1gFnSc1	coarctata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
a	a	k8xC	a
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
se	se	k3xPyFc4	se
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
larvální	larvální	k2eAgFnSc6d1	larvální
kůži	kůže	k1gFnSc6	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kuklení	kuklení	k1gNnSc3	kuklení
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
v	v	k7c6	v
pouzdrech	pouzdro	k1gNnPc6	pouzdro
<g/>
,	,	kIx,	,
v	v	k7c6	v
zámotcích	zámotek	k1gInPc6	zámotek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
výměšků	výměšek	k1gInPc2	výměšek
snovacích	snovací	k2eAgFnPc2d1	snovací
žláz	žláza	k1gFnPc2	žláza
ústících	ústící	k2eAgFnPc2d1	ústící
do	do	k7c2	do
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
výměšků	výměšek	k1gInPc2	výměšek
slinných	slinný	k2eAgFnPc2d1	slinná
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
kukly	kukla	k1gFnSc2	kukla
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
hmyz	hmyz	k1gInSc1	hmyz
značnou	značný	k2eAgFnSc4d1	značná
proměnu	proměna	k1gFnSc4	proměna
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dospělce	dospělec	k1gMnSc4	dospělec
nebo	nebo	k8xC	nebo
imaga	imag	k1gMnSc4	imag
<g/>
.	.	kIx.	.
</s>
<s>
Motýli	motýl	k1gMnPc1	motýl
jsou	být	k5eAaImIp3nP	být
příkladem	příklad	k1gInSc7	příklad
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
kompletní	kompletní	k2eAgFnSc4d1	kompletní
metamorfózu	metamorfóza	k1gFnSc4	metamorfóza
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
pomocí	pomocí	k7c2	pomocí
hypermetamorfózy	hypermetamorfóza	k1gFnSc2	hypermetamorfóza
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
parazitické	parazitický	k2eAgFnPc1d1	parazitická
vosy	vosa	k1gFnPc1	vosa
<g/>
)	)	kIx)	)
procházejí	procházet	k5eAaImIp3nP	procházet
polyembryonií	polyembryonie	k1gFnSc7	polyembryonie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jednotlivé	jednotlivý	k2eAgNnSc1d1	jednotlivé
oplodněné	oplodněný	k2eAgNnSc1d1	oplodněné
vajíčko	vajíčko	k1gNnSc1	vajíčko
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
(	(	kIx(	(
<g/>
i	i	k9	i
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
)	)	kIx)	)
samostatných	samostatný	k2eAgFnPc2d1	samostatná
embryií	embryie	k1gFnPc2	embryie
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
citlivé	citlivý	k2eAgInPc4d1	citlivý
smyslové	smyslový	k2eAgInPc4d1	smyslový
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Některý	některý	k3yIgInSc4	některý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
včely	včela	k1gFnSc2	včela
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
spektru	spektrum	k1gNnSc6	spektrum
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samci	samec	k1gMnPc1	samec
můr	můra	k1gFnPc2	můra
mohou	moct	k5eAaImIp3nP	moct
detekovat	detekovat	k5eAaImF	detekovat
samičí	samičí	k2eAgInPc4d1	samičí
feromony	feromon	k1gInPc4	feromon
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mnoha	mnoho	k4c2	mnoho
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Chloupky	chloupek	k1gInPc1	chloupek
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
čidla	čidlo	k1gNnPc4	čidlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
je	on	k3xPp3gInPc4	on
čistit	čistit	k5eAaImF	čistit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některého	některý	k3yIgInSc2	některý
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
také	také	k9	také
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zřetelné	zřetelný	k2eAgNnSc1d1	zřetelné
mezi	mezi	k7c7	mezi
soliterními	soliterní	k2eAgFnPc7d1	soliterní
vosami	vosa	k1gFnPc7	vosa
<g/>
.	.	kIx.	.
</s>
<s>
Vosí	vosí	k2eAgFnSc1d1	vosí
matka	matka	k1gFnSc1	matka
pokládá	pokládat	k5eAaImIp3nS	pokládat
vejce	vejce	k1gNnPc4	vejce
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
každému	každý	k3xTgMnSc3	každý
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
přesně	přesně	k6eAd1	přesně
určený	určený	k2eAgInSc1d1	určený
počet	počet	k1gInSc1	počet
živých	živý	k2eAgFnPc2d1	živá
housenek	housenka	k1gFnPc2	housenka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
larvám	larva	k1gFnPc3	larva
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
jako	jako	k8xC	jako
potrava	potrava	k1gFnSc1	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
vos	vosa	k1gFnPc2	vosa
připravují	připravovat	k5eAaImIp3nP	připravovat
pro	pro	k7c4	pro
jednu	jeden	k4xCgFnSc4	jeden
buňku	buňka	k1gFnSc4	buňka
pět	pět	k4xCc4	pět
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc1d1	jiné
dvanáct	dvanáct	k4xCc1	dvanáct
a	a	k8xC	a
některé	některý	k3yIgNnSc1	některý
dokonce	dokonce	k9	dokonce
čtyřiadvacet	čtyřiadvacet	k4xCc4	čtyřiadvacet
housenek	housenka	k1gFnPc2	housenka
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
housenek	housenka	k1gFnPc2	housenka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
vosy	vosa	k1gFnSc2	vosa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
larvy	larva	k1gFnSc2	larva
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
samec	samec	k1gMnSc1	samec
vosy	vosa	k1gFnSc2	vosa
rodu	rod	k1gInSc2	rod
Eumenes	Eumenes	k1gInSc1	Eumenes
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
dodává	dodávat	k5eAaImIp3nS	dodávat
matka	matka	k1gFnSc1	matka
pouze	pouze	k6eAd1	pouze
pět	pět	k4xCc1	pět
housenek	housenka	k1gFnPc2	housenka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
budoucí	budoucí	k2eAgFnSc3d1	budoucí
samici	samice	k1gFnSc3	samice
připravuje	připravovat	k5eAaImIp3nS	připravovat
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
buňky	buňka	k1gFnSc2	buňka
housenek	housenka	k1gFnPc2	housenka
deset	deset	k4xCc4	deset
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
vosa	vosa	k1gFnSc1	vosa
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
schopna	schopen	k2eAgFnSc1d1	schopna
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c7	mezi
počtem	počet	k1gInSc7	počet
housenek	housenka	k1gFnPc2	housenka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
dodává	dodávat	k5eAaImIp3nS	dodávat
do	do	k7c2	do
daných	daný	k2eAgFnPc2d1	daná
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
buňka	buňka	k1gFnSc1	buňka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
samčí	samčí	k2eAgFnSc4d1	samčí
nebo	nebo	k8xC	nebo
samičí	samičí	k2eAgFnSc4d1	samičí
larvu	larva	k1gFnSc4	larva
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgInSc4d1	sociální
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mravenci	mravenec	k1gMnPc1	mravenec
a	a	k8xC	a
včely	včela	k1gFnPc1	včela
jsou	být	k5eAaImIp3nP	být
nejdůvěrněji	důvěrně	k6eAd3	důvěrně
známí	známý	k2eAgMnPc1d1	známý
eusociální	eusociální	k2eAgMnPc1d1	eusociální
živočichové	živočich	k1gMnPc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
společně	společně	k6eAd1	společně
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
organizace	organizace	k1gFnSc1	organizace
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
takové	takový	k3xDgFnSc6	takový
výši	výše	k1gFnSc6	výše
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgNnPc1	tento
společenství	společenství	k1gNnPc1	společenství
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
superorganismus	superorganismus	k1gInSc4	superorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hmyzí	hmyzí	k2eAgInSc4d1	hmyzí
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc1	hmyz
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
skupinou	skupina	k1gFnSc7	skupina
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
u	u	k7c2	u
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
schopnost	schopnost	k1gFnSc1	schopnost
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Evoluce	evoluce	k1gFnSc1	evoluce
hmyzího	hmyzí	k2eAgNnSc2d1	hmyzí
křídla	křídlo	k1gNnSc2	křídlo
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
odborné	odborný	k2eAgFnSc2d1	odborná
debaty	debata	k1gFnSc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zastánci	zastánce	k1gMnPc1	zastánce
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
křídla	křídlo	k1gNnSc2	křídlo
jsou	být	k5eAaImIp3nP	být
původem	původ	k1gInSc7	původ
paradorzální	paradorzální	k2eAgFnSc2d1	paradorzální
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
modifikované	modifikovaný	k2eAgFnPc4d1	modifikovaná
žábry	žábry	k1gFnPc4	žábry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
karbonu	karbon	k1gInSc6	karbon
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
některé	některý	k3yIgFnPc1	některý
vážky	vážka	k1gFnPc1	vážka
rodu	rod	k1gInSc2	rod
Meganeura	Meganeur	k1gMnSc2	Meganeur
až	až	k6eAd1	až
75	[number]	k4	75
cm	cm	kA	cm
rozpětí	rozpětí	k1gNnSc4	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
největší	veliký	k2eAgInSc1d3	veliký
létající	létající	k2eAgInSc1d1	létající
hmyz	hmyz	k1gInSc1	hmyz
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
hodně	hodně	k6eAd1	hodně
menší	malý	k2eAgNnSc4d2	menší
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
můr	můra	k1gFnPc2	můra
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
motýlů	motýl	k1gMnPc2	motýl
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
martináčovití	martináčovitý	k2eAgMnPc1d1	martináčovitý
<g/>
.	.	kIx.	.
</s>
<s>
Hmyzí	hmyzí	k2eAgInSc4d1	hmyzí
let	let	k1gInSc4	let
byl	být	k5eAaImAgMnS	být
předmětem	předmět	k1gInSc7	předmět
velkého	velký	k2eAgInSc2d1	velký
zájmu	zájem	k1gInSc2	zájem
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
aerodynamiky	aerodynamika	k1gFnSc2	aerodynamika
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nedostatečně	dostatečně	k6eNd1	dostatečně
propracované	propracovaný	k2eAgFnSc3d1	propracovaná
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc1d1	umožňující
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
zdvihu	zdvih	k1gInSc2	zdvih
generovaného	generovaný	k2eAgInSc2d1	generovaný
malinkými	malinká	k1gFnPc7	malinká
hmyzími	hmyzí	k2eAgNnPc7d1	hmyzí
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
hmyzích	hmyzí	k2eAgMnPc2d1	hmyzí
dospělců	dospělec	k1gMnPc2	dospělec
používá	používat	k5eAaImIp3nS	používat
svých	svůj	k3xOyFgMnPc2	svůj
šest	šest	k4xCc1	šest
nohou	noha	k1gFnPc2	noha
pro	pro	k7c4	pro
běh	běh	k1gInSc4	běh
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
adaptováni	adaptovat	k5eAaBmNgMnP	adaptovat
na	na	k7c6	na
tripedální	tripedální	k2eAgFnSc6d1	tripedální
chůzi	chůze	k1gFnSc6	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
chůze	chůze	k1gFnSc2	chůze
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rychlý	rychlý	k2eAgInSc1d1	rychlý
pohyb	pohyb	k1gInSc1	pohyb
při	při	k7c6	při
maximálním	maximální	k2eAgNnSc6d1	maximální
zachování	zachování	k1gNnSc6	zachování
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
studován	studovat	k5eAaImNgInS	studovat
především	především	k9	především
na	na	k7c6	na
švábech	šváb	k1gMnPc6	šváb
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
střídající	střídající	k2eAgMnSc1d1	střídající
se	s	k7c7	s
trojúhelníky	trojúhelník	k1gInPc7	trojúhelník
<g/>
,	,	kIx,	,
dotýkající	dotýkající	k2eAgFnPc4d1	dotýkající
se	se	k3xPyFc4	se
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvním	první	k4xOgInSc6	první
kroku	krok	k1gInSc6	krok
jsou	být	k5eAaImIp3nP	být
střední	střední	k2eAgFnSc1d1	střední
pravá	pravý	k2eAgFnSc1d1	pravá
a	a	k8xC	a
přední	přední	k2eAgFnSc1d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc1d1	zadní
levá	levý	k2eAgFnSc1d1	levá
noha	noha	k1gFnSc1	noha
opřeny	opřen	k2eAgInPc4d1	opřen
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
zatímco	zatímco	k8xS	zatímco
přední	přední	k2eAgFnPc4d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnPc4d1	zadní
pravé	pravý	k2eAgFnPc4d1	pravá
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
střední	střední	k2eAgFnSc1d1	střední
levá	levý	k2eAgFnSc1d1	levá
noha	noha	k1gFnSc1	noha
jsou	být	k5eAaImIp3nP	být
zvednuté	zvednutý	k2eAgInPc1d1	zvednutý
a	a	k8xC	a
přesunují	přesunovat	k5eAaImIp3nP	přesunovat
se	se	k3xPyFc4	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
nové	nový	k2eAgFnSc2d1	nová
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dotknou	dotknout	k5eAaPmIp3nP	dotknout
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
nový	nový	k2eAgInSc4d1	nový
stabilní	stabilní	k2eAgInSc4d1	stabilní
trojhran	trojhran	k1gInSc4	trojhran
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zvednuty	zvednut	k2eAgFnPc4d1	zvednuta
další	další	k2eAgFnPc4d1	další
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
přesunuty	přesunut	k2eAgFnPc4d1	přesunuta
a	a	k8xC	a
tak	tak	k6eAd1	tak
střídavě	střídavě	k6eAd1	střídavě
stále	stále	k6eAd1	stále
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
forma	forma	k1gFnSc1	forma
tripedální	tripedální	k2eAgFnSc2d1	tripedální
chůze	chůze	k1gFnSc2	chůze
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
u	u	k7c2	u
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
přesunujícího	přesunující	k2eAgInSc2d1	přesunující
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
si	se	k3xPyFc3	se
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
na	na	k7c6	na
přiloženém	přiložený	k2eAgInSc6d1	přiložený
animovaném	animovaný	k2eAgInSc6d1	animovaný
obrázku	obrázek	k1gInSc6	obrázek
chůze	chůze	k1gFnSc2	chůze
berušky	beruška	k1gFnSc2	beruška
(	(	kIx(	(
<g/>
Coccinellidae	Coccinellida	k1gMnSc2	Coccinellida
<g/>
,	,	kIx,	,
Coccinella	Coccinell	k1gMnSc2	Coccinell
septempunctata	septempunctat	k1gMnSc2	septempunctat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
pohybu	pohyb	k1gInSc2	pohyb
není	být	k5eNaImIp3nS	být
neměnný	měnný	k2eNgInSc1d1	neměnný
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc1	hmyz
ho	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
podle	podle	k7c2	podle
dané	daný	k2eAgFnSc2d1	daná
situace	situace	k1gFnSc2	situace
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
při	při	k7c6	při
pomalém	pomalý	k2eAgInSc6d1	pomalý
postupu	postup	k1gInSc6	postup
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
otáčení	otáčení	k1gNnSc1	otáčení
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vyhýbání	vyhýbání	k1gNnSc4	vyhýbání
se	s	k7c7	s
překážkám	překážka	k1gFnPc3	překážka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
země	země	k1gFnSc1	země
dotýkat	dotýkat	k5eAaImF	dotýkat
čtyři	čtyři	k4xCgInPc4	čtyři
a	a	k8xC	a
více	hodně	k6eAd2	hodně
nohou	noha	k1gFnSc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc1	hmyz
také	také	k9	také
dokáže	dokázat	k5eAaPmIp3nS	dokázat
svou	svůj	k3xOyFgFnSc4	svůj
chůzi	chůze	k1gFnSc4	chůze
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
ztrátě	ztráta	k1gFnSc6	ztráta
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Švábi	šváb	k1gMnPc1	šváb
jsou	být	k5eAaImIp3nP	být
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
běhavým	běhavý	k2eAgInSc7d1	běhavý
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
rychlosti	rychlost	k1gFnSc6	rychlost
vlastně	vlastně	k9	vlastně
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
bipedální	bipedální	k2eAgInSc4d1	bipedální
pohyb	pohyb	k1gInSc4	pohyb
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vysoké	vysoký	k2eAgFnSc2d1	vysoká
rychlosti	rychlost	k1gFnSc2	rychlost
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
velikosti	velikost	k1gFnSc3	velikost
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rychlosti	rychlost	k1gFnSc3	rychlost
švábů	šváb	k1gMnPc2	šváb
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
jejich	jejich	k3xOp3gInSc2	jejich
způsobu	způsob	k1gInSc2	způsob
chůze	chůze	k1gFnSc1	chůze
kameru	kamera	k1gFnSc4	kamera
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
natáčení	natáčení	k1gNnSc2	natáčení
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Pomalejší	pomalý	k2eAgInSc4d2	pomalejší
pohyb	pohyb	k1gInSc4	pohyb
vědci	vědec	k1gMnPc1	vědec
studovali	studovat	k5eAaImAgMnP	studovat
na	na	k7c6	na
takových	takový	k3xDgInPc6	takový
druzích	druh	k1gInPc6	druh
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
pakobylky	pakobylka	k1gFnPc1	pakobylka
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
Phasmatodea	Phasmatode	k1gInSc2	Phasmatode
<g/>
.	.	kIx.	.
</s>
<s>
Hmyzí	hmyzí	k2eAgFnSc1d1	hmyzí
chůze	chůze	k1gFnSc1	chůze
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
zájmu	zájem	k1gInSc2	zájem
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
alternativu	alternativa	k1gFnSc4	alternativa
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
robotů	robot	k1gInPc2	robot
pomocí	pomocí	k7c2	pomocí
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pohyb	pohyb	k1gInSc1	pohyb
robotů	robot	k1gInPc2	robot
<g/>
)	)	kIx)	)
Znakoplavka	znakoplavka	k1gFnSc1	znakoplavka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
již	již	k9	již
název	název	k1gInSc1	název
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
plave	plavat	k5eAaImIp3nS	plavat
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
naznak	naznak	k6eAd1	naznak
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
nohy	noha	k1gFnPc4	noha
přizpůsobené	přizpůsobený	k2eAgFnPc1d1	přizpůsobená
k	k	k7c3	k
pádlování	pádlování	k1gNnSc3	pádlování
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
hmyzu	hmyz	k1gInSc2	hmyz
žije	žít	k5eAaImIp3nS	žít
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
nedospělá	dospělý	k2eNgNnPc4d1	nedospělé
stádia	stádium	k1gNnPc4	stádium
hmyzu	hmyz	k1gInSc2	hmyz
tráví	trávit	k5eAaImIp3nP	trávit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
nebo	nebo	k8xC	nebo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
tráví	trávit	k5eAaImIp3nP	trávit
pod	pod	k7c7	pod
či	či	k8xC	či
nad	nad	k7c7	nad
vodou	voda	k1gFnSc7	voda
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
dospělého	dospělý	k2eAgInSc2d1	dospělý
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
adaptovalo	adaptovat	k5eAaBmAgNnS	adaptovat
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Potápníci	potápník	k1gMnPc1	potápník
a	a	k8xC	a
vodní	vodní	k2eAgFnPc1d1	vodní
ploštice	ploštice	k1gFnPc1	ploštice
mají	mít	k5eAaImIp3nP	mít
nohy	noha	k1gFnPc1	noha
přizpůsobené	přizpůsobený	k2eAgFnPc1d1	přizpůsobená
k	k	k7c3	k
pádlování	pádlování	k1gNnSc3	pádlování
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
larvy	larva	k1gFnPc1	larva
řádu	řád	k1gInSc2	řád
síťokřídlých	síťokřídlí	k1gMnPc2	síťokřídlí
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
pomocí	pomocí	k7c2	pomocí
vody	voda	k1gFnSc2	voda
vytlačované	vytlačovaný	k2eAgFnSc2d1	vytlačovaná
z	z	k7c2	z
rektální	rektální	k2eAgFnSc2d1	rektální
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc4	hmyz
můžeme	moct	k5eAaImIp1nP	moct
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
oceánů	oceán	k1gInPc2	oceán
nalézt	nalézt	k5eAaPmF	nalézt
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
biotopech	biotop	k1gInPc6	biotop
a	a	k8xC	a
částech	část	k1gFnPc6	část
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
hmyzích	hmyzí	k2eAgInPc2d1	hmyzí
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
extrémních	extrémní	k2eAgInPc6d1	extrémní
biotopech	biotop	k1gInPc6	biotop
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
polární	polární	k2eAgFnPc4d1	polární
krajiny	krajina	k1gFnPc4	krajina
<g/>
,	,	kIx,	,
velehory	velehora	k1gFnPc4	velehora
nebo	nebo	k8xC	nebo
mořská	mořský	k2eAgNnPc4d1	mořské
pobřeží	pobřeží	k1gNnPc4	pobřeží
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
vysoce	vysoce	k6eAd1	vysoce
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
např.	např.	kA	např.
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
pakomára	pakomár	k1gMnSc2	pakomár
Belgica	Belgicus	k1gMnSc2	Belgicus
antarctica	antarcticus	k1gMnSc2	antarcticus
nebo	nebo	k8xC	nebo
na	na	k7c6	na
mořské	mořský	k2eAgFnSc6d1	mořská
hladině	hladina	k1gFnSc6	hladina
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
přizpůsobené	přizpůsobený	k2eAgFnSc6d1	přizpůsobená
bruslařky	bruslařka	k1gFnPc1	bruslařka
patřících	patřící	k2eAgFnPc2d1	patřící
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
ploštic	ploštice	k1gFnPc2	ploštice
nebo	nebo	k8xC	nebo
komáry	komár	k1gMnPc4	komár
rodu	rod	k1gInSc2	rod
Clunio	Clunio	k1gMnSc1	Clunio
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
specializovány	specializován	k2eAgFnPc1d1	specializována
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
žít	žít	k5eAaImF	žít
pouze	pouze	k6eAd1	pouze
jen	jen	k9	jen
v	v	k7c6	v
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
zvlášť	zvlášť	k6eAd1	zvlášť
vhodných	vhodný	k2eAgInPc6d1	vhodný
biotopech	biotop	k1gInPc6	biotop
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
naopak	naopak	k6eAd1	naopak
mohou	moct	k5eAaImIp3nP	moct
žít	žít	k5eAaImF	žít
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
biotopech	biotop	k1gInPc6	biotop
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
těch	ten	k3xDgInPc2	ten
nejextrémnějších	extrémní	k2eAgInPc2d3	nejextrémnější
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
rozšiřovány	rozšiřován	k2eAgFnPc1d1	rozšiřována
samotnými	samotný	k2eAgMnPc7d1	samotný
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
kosmopolitní	kosmopolitní	k2eAgInPc1d1	kosmopolitní
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
skupině	skupina	k1gFnSc3	skupina
hmyzu	hmyz	k1gInSc2	hmyz
patří	patřit	k5eAaImIp3nP	patřit
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
švábů	šváb	k1gMnPc2	šváb
<g/>
,	,	kIx,	,
mravenců	mravenec	k1gMnPc2	mravenec
a	a	k8xC	a
termitů	termit	k1gMnPc2	termit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
hmyz	hmyz	k1gInSc1	hmyz
užitečný	užitečný	k2eAgInSc1d1	užitečný
jako	jako	k8xC	jako
např.	např.	kA	např.
medonosné	medonosný	k2eAgFnPc4d1	medonosná
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hmyzu	hmyz	k1gInSc2	hmyz
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
nebo	nebo	k8xC	nebo
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
na	na	k7c6	na
rostlinách	rostlina	k1gFnPc6	rostlina
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
počtu	počet	k1gInSc2	počet
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
vychází	vycházet	k5eAaImIp3nS	vycházet
např.	např.	kA	např.
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
tropický	tropický	k2eAgInSc1d1	tropický
strom	strom	k1gInSc1	strom
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
tvoří	tvořit	k5eAaImIp3nP	tvořit
sídlo	sídlo	k1gNnSc4	sídlo
pro	pro	k7c4	pro
přibližně	přibližně	k6eAd1	přibližně
600	[number]	k4	600
hmyzích	hmyzí	k2eAgInPc2d1	hmyzí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
při	při	k7c6	při
50	[number]	k4	50
000	[number]	k4	000
druzích	druh	k1gInPc6	druh
stromů	strom	k1gInPc2	strom
tak	tak	k6eAd1	tak
vychází	vycházet	k5eAaImIp3nS	vycházet
kolem	kolem	k7c2	kolem
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c6	na
mnohých	mnohý	k2eAgInPc6d1	mnohý
druzích	druh	k1gInPc6	druh
zvířat	zvíře	k1gNnPc2	zvíře
žije	žít	k5eAaImIp3nS	žít
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
jako	jako	k8xS	jako
ektoparazité	ektoparazit	k1gMnPc1	ektoparazit
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
vši	veš	k1gFnSc2	veš
a	a	k8xC	a
blechy	blecha	k1gFnSc2	blecha
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
nepředstavuje	představovat	k5eNaImIp3nS	představovat
žádnou	žádný	k3yNgFnSc4	žádný
výjimku	výjimka	k1gFnSc4	výjimka
<g/>
,	,	kIx,	,
nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
lidskými	lidský	k2eAgMnPc7d1	lidský
parazity	parazit	k1gMnPc7	parazit
z	z	k7c2	z
hmyzí	hmyzí	k2eAgFnSc2d1	hmyzí
říše	říš	k1gFnSc2	říš
jsou	být	k5eAaImIp3nP	být
vši	veš	k1gFnPc1	veš
<g/>
.	.	kIx.	.
</s>
<s>
Vzácnými	vzácný	k2eAgMnPc7d1	vzácný
jsou	být	k5eAaImIp3nP	být
endoparazité	endoparazit	k1gMnPc1	endoparazit
<g/>
,	,	kIx,	,
cizopasící	cizopasící	k2eAgMnPc1d1	cizopasící
uvnitř	uvnitř	k7c2	uvnitř
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
především	především	k9	především
k	k	k7c3	k
dvoukřídlým	dvoukřídlí	k1gMnPc3	dvoukřídlí
náležející	náležející	k2eAgFnSc2d1	náležející
střečkovití	střečkovitý	k2eAgMnPc1d1	střečkovitý
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
larvy	larva	k1gFnPc4	larva
cizopasí	cizopasit	k5eAaImIp3nS	cizopasit
a	a	k8xC	a
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
se	se	k3xPyFc4	se
v	v	k7c6	v
hltanu	hltan	k1gInSc6	hltan
(	(	kIx(	(
<g/>
střeček	střeček	k1gMnSc1	střeček
hltanový	hltanový	k2eAgMnSc1d1	hltanový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
chřípí	chřípí	k1gNnSc6	chřípí
(	(	kIx(	(
<g/>
střeček	střeček	k1gMnSc1	střeček
ovčí	ovčí	k2eAgMnSc1d1	ovčí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kůží	kůže	k1gFnSc7	kůže
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
(	(	kIx(	(
<g/>
Gastrophilus	Gastrophilus	k1gInSc1	Gastrophilus
intestinalis	intestinalis	k1gInSc1	intestinalis
<g/>
)	)	kIx)	)
býložravců	býložravec	k1gMnPc2	býložravec
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
rozmanitosti	rozmanitost	k1gFnSc3	rozmanitost
obsadil	obsadit	k5eAaPmAgInS	obsadit
dnes	dnes	k6eAd1	dnes
hmyz	hmyz	k1gInSc1	hmyz
každou	každý	k3xTgFnSc7	každý
<g/>
,	,	kIx,	,
přiměřeně	přiměřeně	k6eAd1	přiměřeně
velkou	velký	k2eAgFnSc4d1	velká
<g/>
,	,	kIx,	,
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
niku	nika	k1gFnSc4	nika
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
druhů	druh	k1gInPc2	druh
přitom	přitom	k6eAd1	přitom
hraje	hrát	k5eAaImIp3nS	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
při	při	k7c6	při
remineralizaci	remineralizace	k1gFnSc6	remineralizace
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
či	či	k8xC	či
při	při	k7c6	při
rozkladu	rozklad	k1gInSc6	rozklad
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
organických	organický	k2eAgFnPc2d1	organická
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skupině	skupina	k1gFnSc3	skupina
náleží	náležet	k5eAaImIp3nP	náležet
také	také	k9	také
mrchožrouti	mrchožrout	k1gMnPc1	mrchožrout
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
mrtvolách	mrtvola	k1gFnPc6	mrtvola
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
spektrum	spektrum	k1gNnSc1	spektrum
jejich	jejich	k3xOp3gInSc2	jejich
výskytu	výskyt	k1gInSc2	výskyt
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
kořínků	kořínek	k1gInPc2	kořínek
dřevin	dřevina	k1gFnPc2	dřevina
až	až	k9	až
po	po	k7c4	po
listy	list	k1gInPc4	list
a	a	k8xC	a
květy	květ	k1gInPc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
hmyzích	hmyzí	k2eAgInPc2d1	hmyzí
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
sbíráním	sbírání	k1gNnSc7	sbírání
nektaru	nektar	k1gInSc2	nektar
nebo	nebo	k8xC	nebo
pylu	pyl	k1gInSc2	pyl
z	z	k7c2	z
květů	květ	k1gInPc2	květ
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
tak	tak	k9	tak
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
opylování	opylování	k1gNnSc6	opylování
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
houbách	houba	k1gFnPc6	houba
a	a	k8xC	a
živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
jimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
skupiny	skupina	k1gFnPc1	skupina
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
lovem	lov	k1gInSc7	lov
jiných	jiný	k2eAgInPc2d1	jiný
hmyzích	hmyzí	k2eAgInPc2d1	hmyzí
druhů	druh	k1gInPc2	druh
nebo	nebo	k8xC	nebo
malých	malý	k2eAgNnPc2d1	malé
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
skupinu	skupina	k1gFnSc4	skupina
představuje	představovat	k5eAaImIp3nS	představovat
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
částmi	část	k1gFnPc7	část
větších	veliký	k2eAgNnPc2d2	veliký
zvířat	zvíře	k1gNnPc2	zvíře
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
chlupy	chlup	k1gInPc1	chlup
<g/>
,	,	kIx,	,
šupiny	šupina	k1gFnPc1	šupina
apod.	apod.	kA	apod.
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skupině	skupina	k1gFnSc3	skupina
také	také	k9	také
náleží	náležet	k5eAaImIp3nS	náležet
četný	četný	k2eAgInSc4d1	četný
parazitický	parazitický	k2eAgInSc4d1	parazitický
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
například	například	k6eAd1	například
saje	sát	k5eAaImIp3nS	sát
krev	krev	k1gFnSc4	krev
zvířat	zvíře	k1gNnPc2	zvíře
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
živé	živý	k2eAgFnSc6d1	živá
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštnost	zvláštnost	k1gFnSc1	zvláštnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hmyzu	hmyz	k1gInSc2	hmyz
představují	představovat	k5eAaImIp3nP	představovat
různé	různý	k2eAgFnPc1d1	různá
formy	forma	k1gFnPc1	forma
společenského	společenský	k2eAgInSc2d1	společenský
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
společného	společný	k2eAgInSc2d1	společný
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
u	u	k7c2	u
termitů	termit	k1gInPc2	termit
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
blanokřídlých	blanokřídlí	k1gMnPc2	blanokřídlí
(	(	kIx(	(
<g/>
mravenci	mravenec	k1gMnPc1	mravenec
<g/>
,	,	kIx,	,
včely	včela	k1gFnPc1	včela
<g/>
,	,	kIx,	,
vosy	vosa	k1gFnPc1	vosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
hmyzího	hmyzí	k2eAgNnSc2d1	hmyzí
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
jedinec	jedinec	k1gMnSc1	jedinec
svou	svůj	k3xOyFgFnSc4	svůj
určitou	určitý	k2eAgFnSc4d1	určitá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
kasty	kasta	k1gFnPc4	kasta
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
svým	svůj	k3xOyFgNnSc7	svůj
chováním	chování	k1gNnSc7	chování
a	a	k8xC	a
morfologií	morfologie	k1gFnSc7	morfologie
shodní	shodný	k2eAgMnPc5d1	shodný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgInPc2d1	mnohý
druhů	druh	k1gInPc2	druh
mravenců	mravenec	k1gMnPc2	mravenec
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
ošetřovatelky	ošetřovatelka	k1gFnPc4	ošetřovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
společenstvech	společenstvo	k1gNnPc6	společenstvo
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
jeho	jeho	k3xOp3gMnPc2	jeho
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
pouze	pouze	k6eAd1	pouze
jediná	jediný	k2eAgFnSc1d1	jediná
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
klade	klást	k5eAaImIp3nS	klást
oplodněná	oplodněný	k2eAgFnSc1d1	oplodněná
a	a	k8xC	a
neoplodněná	oplodněný	k2eNgNnPc1d1	neoplodněné
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc4	vztah
hmyzu	hmyz	k1gInSc2	hmyz
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
skupinám	skupina	k1gFnPc3	skupina
živočichů	živočich	k1gMnPc2	živočich
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nejasné	jasný	k2eNgInPc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
tradičně	tradičně	k6eAd1	tradičně
spojován	spojovat	k5eAaImNgMnS	spojovat
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
se	s	k7c7	s
stonožkami	stonožka	k1gFnPc7	stonožka
<g/>
,	,	kIx,	,
objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
důkazy	důkaz	k1gInPc7	důkaz
<g/>
,	,	kIx,	,
prokazující	prokazující	k2eAgFnPc1d1	prokazující
jejich	jejich	k3xOp3gFnPc1	jejich
bližší	blízký	k2eAgFnPc1d2	bližší
evoluční	evoluční	k2eAgFnPc1d1	evoluční
vazby	vazba	k1gFnPc1	vazba
ke	k	k7c3	k
korýšům	korýš	k1gMnPc3	korýš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
vzniku	vznik	k1gInSc2	vznik
hmyzu	hmyz	k1gInSc2	hmyz
tvoří	tvořit	k5eAaImIp3nS	tvořit
skupina	skupina	k1gFnSc1	skupina
Pancrustacea	Pancrustacea	k1gFnSc1	Pancrustacea
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
skupinami	skupina	k1gFnPc7	skupina
Remipedia	Remipedium	k1gNnSc2	Remipedium
a	a	k8xC	a
Malacostraca	Malacostracum	k1gNnSc2	Malacostracum
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
větev	větev	k1gFnSc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
některé	některý	k3yIgInPc4	některý
slibné	slibný	k2eAgInPc4d1	slibný
devonské	devonský	k2eAgInPc4d1	devonský
fragmenty	fragment	k1gInPc4	fragment
<g/>
,	,	kIx,	,
fosilní	fosilní	k2eAgInPc1d1	fosilní
otisky	otisk	k1gInPc1	otisk
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevují	objevovat	k5eAaImIp3nP	objevovat
až	až	k9	až
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
karbonu	karbon	k1gInSc2	karbon
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
před	před	k7c7	před
350	[number]	k4	350
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půltucet	půltucet	k1gInSc4	půltucet
různých	různý	k2eAgInPc2d1	různý
řádů	řád	k1gInPc2	řád
vysoce	vysoce	k6eAd1	vysoce
specializovaného	specializovaný	k2eAgInSc2d1	specializovaný
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
se	se	k3xPyFc4	se
první	první	k4xOgInSc1	první
hmyz	hmyz	k1gInSc1	hmyz
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
planetě	planeta	k1gFnSc6	planeta
objevil	objevit	k5eAaPmAgMnS	objevit
již	již	k6eAd1	již
v	v	k7c6	v
devonu	devon	k1gInSc6	devon
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
výskytu	výskyt	k1gInSc2	výskyt
zkamenělých	zkamenělý	k2eAgInPc2d1	zkamenělý
otisků	otisk	k1gInPc2	otisk
nejranějších	raný	k2eAgFnPc2d3	nejranější
forem	forma	k1gFnPc2	forma
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
stále	stále	k6eAd1	stále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
hmyzích	hmyzí	k2eAgNnPc2d1	hmyzí
křídel	křídlo	k1gNnPc2	křídlo
zatím	zatím	k6eAd1	zatím
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
od	od	k7c2	od
nejranějších	raný	k2eAgFnPc2d3	nejranější
forem	forma	k1gFnPc2	forma
okřídleného	okřídlený	k2eAgInSc2d1	okřídlený
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
aktuálně	aktuálně	k6eAd1	aktuálně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byli	být	k5eAaImAgMnP	být
schopní	schopný	k2eAgMnPc1d1	schopný
letci	letec	k1gMnPc1	letec
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zaniklé	zaniklý	k2eAgInPc1d1	zaniklý
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
měly	mít	k5eAaImAgInP	mít
další	další	k2eAgInPc1d1	další
pár	pár	k4xCyI	pár
křidélek	křidélko	k1gNnPc2	křidélko
<g/>
,	,	kIx,	,
připevněných	připevněný	k2eAgInPc2d1	připevněný
k	k	k7c3	k
první	první	k4xOgFnSc3	první
části	část	k1gFnSc3	část
hrudníku	hrudník	k1gInSc2	hrudník
<g/>
,	,	kIx,	,
úhrnem	úhrnem	k6eAd1	úhrnem
měly	mít	k5eAaImAgFnP	mít
tedy	tedy	k9	tedy
tří	tři	k4xCgFnPc2	tři
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
objevila	objevit	k5eAaPmAgFnS	objevit
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
nenasvědčovalo	nasvědčovat	k5eNaImAgNnS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaImF	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
skupin	skupina	k1gFnPc2	skupina
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
karbonu	karbon	k1gInSc6	karbon
a	a	k8xC	a
raném	raný	k2eAgInSc6d1	raný
permu	perm	k1gInSc6	perm
hmyzí	hmyzí	k2eAgInPc4d1	hmyzí
řády	řád	k1gInPc4	řád
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
několik	několik	k4yIc1	několik
nynějších	nynější	k2eAgInPc2d1	nynější
velmi	velmi	k6eAd1	velmi
starých	starý	k2eAgFnPc2d1	stará
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
prvohorních	prvohorní	k2eAgFnPc2d1	prvohorní
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
žily	žít	k5eAaImAgFnP	žít
také	také	k6eAd1	také
obří	obří	k2eAgFnPc1d1	obří
vážky	vážka	k1gFnPc1	vážka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
měly	mít	k5eAaImAgInP	mít
rozpětí	rozpětí	k1gNnSc4	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
od	od	k7c2	od
50	[number]	k4	50
až	až	k9	až
po	po	k7c4	po
70	[number]	k4	70
cm	cm	kA	cm
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
tak	tak	k6eAd1	tak
největším	veliký	k2eAgInSc7d3	veliký
hmyzím	hmyzí	k2eAgInSc7d1	hmyzí
druhem	druh	k1gInSc7	druh
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jejich	jejich	k3xOp3gFnPc1	jejich
nymfy	nymfa	k1gFnPc1	nymfa
musely	muset	k5eAaImAgFnP	muset
dosahovat	dosahovat	k5eAaImF	dosahovat
úctyhodných	úctyhodný	k2eAgInPc2d1	úctyhodný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
ohromné	ohromný	k2eAgInPc4d1	ohromný
rozměry	rozměr	k1gInPc4	rozměr
lze	lze	k6eAd1	lze
přičíst	přičíst	k5eAaPmF	přičíst
vyšší	vysoký	k2eAgFnSc3d2	vyšší
atmosférické	atmosférický	k2eAgFnSc3d1	atmosférická
hladině	hladina	k1gFnSc3	hladina
kyslíku	kyslík	k1gInSc2	kyslík
oproti	oproti	k7c3	oproti
dnešku	dnešek	k1gInSc3	dnešek
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dovolovala	dovolovat	k5eAaImAgFnS	dovolovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
účinnost	účinnost	k1gFnSc4	účinnost
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
faktorem	faktor	k1gInSc7	faktor
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
absence	absence	k1gFnSc1	absence
létajících	létající	k2eAgMnPc2d1	létající
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
druhové	druhový	k2eAgNnSc1d1	druhové
množství	množství	k1gNnSc1	množství
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
během	během	k7c2	během
období	období	k1gNnSc2	období
permu	perm	k1gInSc2	perm
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
před	před	k7c7	před
270	[number]	k4	270
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
skupin	skupina	k1gFnPc2	skupina
raných	raný	k2eAgFnPc2d1	raná
forem	forma	k1gFnPc2	forma
hmyzu	hmyz	k1gInSc2	hmyz
vyhynulo	vyhynout	k5eAaPmAgNnS	vyhynout
během	během	k7c2	během
velkého	velký	k2eAgNnSc2d1	velké
permského	permský	k2eAgNnSc2d1	Permské
vymírání	vymírání	k1gNnSc2	vymírání
<g/>
,	,	kIx,	,
největšího	veliký	k2eAgInSc2d3	veliký
úbytku	úbytek	k1gInSc2	úbytek
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
asi	asi	k9	asi
před	před	k7c7	před
252	[number]	k4	252
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
jury	jura	k1gFnSc2	jura
a	a	k8xC	a
křídy	křída	k1gFnSc2	křída
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
mnohé	mnohý	k2eAgInPc1d1	mnohý
moderní	moderní	k2eAgInPc1d1	moderní
typy	typ	k1gInPc1	typ
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
parazitické	parazitický	k2eAgFnPc1d1	parazitická
blechy	blecha	k1gFnPc1	blecha
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Pseudopulex	Pseudopulex	k1gInSc1	Pseudopulex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vši	veš	k1gFnPc1	veš
<g/>
,	,	kIx,	,
klíšťata	klíště	k1gNnPc1	klíště
aj.	aj.	kA	aj.
Pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
blanokřídlí	blanokřídlí	k1gMnPc1	blanokřídlí
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
křídě	křída	k1gFnSc6	křída
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc6d3	veliký
rozmanitosti	rozmanitost	k1gFnSc6	rozmanitost
druhů	druh	k1gInPc2	druh
pak	pak	k6eAd1	pak
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
třetihorách	třetihory	k1gFnPc6	třetihory
(	(	kIx(	(
<g/>
kenozoiku	kenozoikum	k1gNnSc6	kenozoikum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
této	tento	k3xDgFnSc2	tento
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
skupiny	skupina	k1gFnSc2	skupina
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
příklad	příklad	k1gInSc1	příklad
koevoluce	koevoluce	k1gFnSc2	koevoluce
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
moderního	moderní	k2eAgInSc2d1	moderní
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
během	běh	k1gInSc7	běh
třetihor	třetihory	k1gFnPc2	třetihory
<g/>
;	;	kIx,	;
hmyz	hmyz	k1gInSc1	hmyz
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
často	často	k6eAd1	často
nacházíme	nacházet	k5eAaImIp1nP	nacházet
zalitý	zalitý	k2eAgInSc4d1	zalitý
v	v	k7c6	v
jantaru	jantar	k1gInSc6	jantar
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
v	v	k7c6	v
dokonalém	dokonalý	k2eAgInSc6d1	dokonalý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
fosilií	fosilie	k1gFnPc2	fosilie
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
paleoentomologie	paleoentomologie	k1gFnSc1	paleoentomologie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
vyšších	vysoký	k2eAgInPc2d2	vyšší
taxonů	taxon	k1gInPc2	taxon
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
kmene	kmen	k1gInSc2	kmen
Hexapoda	Hexapoda	k1gFnSc1	Hexapoda
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
chvostoskoci	chvostoskok	k1gMnPc1	chvostoskok
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
je	on	k3xPp3gFnPc4	on
považují	považovat	k5eAaImIp3nP	považovat
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
jinému	jiný	k2eAgInSc3d1	jiný
evolučnímu	evoluční	k2eAgInSc3d1	evoluční
původu	původ	k1gInSc3	původ
za	za	k7c4	za
odlišné	odlišný	k2eAgNnSc4d1	odlišné
od	od	k7c2	od
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
případ	případ	k1gInSc1	případ
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
týkat	týkat	k5eAaImF	týkat
i	i	k9	i
zbytku	zbytek	k1gInSc2	zbytek
skupiny	skupina	k1gFnSc2	skupina
Entognatha	Entognatha	k1gFnSc1	Entognatha
<g/>
;	;	kIx,	;
Protura	Protura	k1gFnSc1	Protura
a	a	k8xC	a
Diplura	Diplura	k1gFnSc1	Diplura
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
Insecta	Insect	k1gInSc2	Insect
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členovců	členovec	k1gMnPc2	členovec
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jevnočelistný	jevnočelistný	k2eAgMnSc1d1	jevnočelistný
(	(	kIx(	(
<g/>
Ectognatha	Ectognatha	k1gMnSc1	Ectognatha
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zadeček	zadeček	k1gInSc4	zadeček
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
maximálně	maximálně	k6eAd1	maximálně
jedenácti	jedenáct	k4xCc2	jedenáct
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
původní	původní	k2eAgInSc1d1	původní
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
však	však	k9	však
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
embryonálním	embryonální	k2eAgNnSc6d1	embryonální
stadiu	stadion	k1gNnSc6	stadion
<g/>
,	,	kIx,	,
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
je	být	k5eAaImIp3nS	být
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
zadečkových	zadečkový	k2eAgInPc2d1	zadečkový
článků	článek	k1gInPc2	článek
značně	značně	k6eAd1	značně
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
hmyz	hmyz	k1gInSc1	hmyz
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
také	také	k9	také
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Ectognatha	Ectognatha	k1gFnSc1	Ectognatha
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
dospělci	dospělec	k1gMnPc1	dospělec
okřídleni	okřídlen	k2eAgMnPc1d1	okřídlen
<g/>
.	.	kIx.	.
</s>
<s>
Podtřída	podtřída	k1gFnSc1	podtřída
<g/>
:	:	kIx,	:
Bezkřídlí	bezkřídlí	k1gMnPc1	bezkřídlí
(	(	kIx(	(
<g/>
Apterygota	Apterygota	k1gFnSc1	Apterygota
<g/>
)	)	kIx)	)
<g/>
Thysanura	Thysanura	k1gFnSc1	Thysanura
(	(	kIx(	(
<g/>
šupinušky	šupinuška	k1gFnPc1	šupinuška
<g/>
)	)	kIx)	)
Archeognatha	Archeognatha	k1gFnSc1	Archeognatha
(	(	kIx(	(
<g/>
chvostnatky	chvostnatka	k1gFnPc1	chvostnatka
<g/>
)	)	kIx)	)
Zygentoma	Zygentoma	k1gNnSc1	Zygentoma
(	(	kIx(	(
<g/>
rybenky	rybenka	k1gFnPc1	rybenka
<g/>
)	)	kIx)	)
Monura	Monura	k1gFnSc1	Monura
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Podtřída	podtřída	k1gFnSc1	podtřída
<g/>
:	:	kIx,	:
Křídlatí	křídlatý	k2eAgMnPc1d1	křídlatý
(	(	kIx(	(
<g/>
Pterygota	Pterygot	k1gMnSc4	Pterygot
<g/>
)	)	kIx)	)
Infratřída	Infratříd	k1gMnSc4	Infratříd
Paleoptera	Paleopter	k1gMnSc4	Paleopter
Ephemeroptera	Ephemeropter	k1gMnSc4	Ephemeropter
(	(	kIx(	(
<g/>
jepice	jepice	k1gFnSc2	jepice
<g/>
)	)	kIx)	)
Palaeodictyoptera	Palaeodictyopter	k1gMnSc2	Palaeodictyopter
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Megasecoptera	Megasecopter	k1gMnSc2	Megasecopter
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Archodonata	Archodonata	k1gFnSc1	Archodonata
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Diaphanopterodea	Diaphanopterodea	k1gFnSc1	Diaphanopterodea
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Protodonata	Protodonata	k1gFnSc1	Protodonata
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Odonata	Odonata	k1gFnSc1	Odonata
(	(	kIx(	(
<g/>
vážky	vážka	k1gFnSc2	vážka
<g/>
)	)	kIx)	)
Infratřída	Infratříd	k1gMnSc2	Infratříd
Neoptera	Neopter	k1gMnSc2	Neopter
Nadřád	nadřád	k1gInSc1	nadřád
<g/>
:	:	kIx,	:
Exopterygota	Exopterygota	k1gFnSc1	Exopterygota
Caloneurodea	Caloneurodea	k1gFnSc1	Caloneurodea
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Titanoptera	Titanopter	k1gMnSc2	Titanopter
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Protorthoptera	Protorthopter	k1gMnSc2	Protorthopter
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Polyneoptera	Polyneopter	k1gMnSc4	Polyneopter
Grylloblattodea	Grylloblattodea	k1gFnSc1	Grylloblattodea
(	(	kIx(	(
<g/>
cvrčkovci	cvrčkovec	k1gMnPc1	cvrčkovec
<g/>
)	)	kIx)	)
Mantophasmatodea	Mantophasmatodea	k1gMnSc1	Mantophasmatodea
(	(	kIx(	(
<g/>
strašilkovci	strašilkovec	k1gInPc7	strašilkovec
<g/>
)	)	kIx)	)
Plecoptera	Plecopter	k1gMnSc2	Plecopter
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
pošvatky	pošvatka	k1gFnSc2	pošvatka
<g/>
)	)	kIx)	)
Embioptera	Embiopter	k1gMnSc4	Embiopter
(	(	kIx(	(
<g/>
snovatky	snovatka	k1gFnSc2	snovatka
<g/>
)	)	kIx)	)
Zoraptera	Zorapter	k1gMnSc2	Zorapter
(	(	kIx(	(
<g/>
drobnělky	drobnělka	k1gMnSc2	drobnělka
<g/>
)	)	kIx)	)
Dermaptera	Dermapter	k1gMnSc2	Dermapter
(	(	kIx(	(
<g/>
škvoři	škvor	k1gMnPc1	škvor
<g/>
)	)	kIx)	)
Orthoptera	Orthopter	k1gMnSc2	Orthopter
(	(	kIx(	(
<g/>
rovnokřídlí	rovnokřídlí	k1gMnPc1	rovnokřídlí
<g/>
)	)	kIx)	)
Phasmatodea	Phasmatodea	k1gMnSc1	Phasmatodea
(	(	kIx(	(
<g/>
strašilky	strašilka	k1gFnPc1	strašilka
<g/>
)	)	kIx)	)
Blattodea	Blattode	k2eAgFnSc1d1	Blattode
(	(	kIx(	(
<g/>
švábi	šváb	k1gMnPc1	šváb
<g/>
)	)	kIx)	)
Isoptera	Isopter	k1gMnSc2	Isopter
(	(	kIx(	(
<g/>
termiti	termit	k1gMnPc1	termit
<g/>
)	)	kIx)	)
Mantodea	Mantodea	k1gMnSc1	Mantodea
(	(	kIx(	(
<g/>
kudlanky	kudlanka	k1gFnPc1	kudlanka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Paraneoptera	Paraneopter	k1gMnSc4	Paraneopter
Psocoptera	Psocopter	k1gMnSc4	Psocopter
(	(	kIx(	(
<g/>
pisivky	pisivka	k1gFnSc2	pisivka
<g/>
)	)	kIx)	)
Thysanoptera	Thysanopter	k1gMnSc2	Thysanopter
(	(	kIx(	(
<g/>
třásnokřídlí	třásnokřídlý	k2eAgMnPc1d1	třásnokřídlý
<g/>
)	)	kIx)	)
Phthiraptera	Phthirapter	k1gMnSc4	Phthirapter
(	(	kIx(	(
<g/>
vši	veš	k1gFnSc2	veš
<g/>
)	)	kIx)	)
Hemiptera	Hemipter	k1gMnSc2	Hemipter
(	(	kIx(	(
<g/>
stejnokřídlí	stejnokřídlý	k2eAgMnPc1d1	stejnokřídlý
<g/>
)	)	kIx)	)
Nadřád	nadřád	k1gInSc4	nadřád
<g/>
:	:	kIx,	:
Endopterygota	Endopterygot	k1gMnSc2	Endopterygot
Hymenoptera	Hymenopter	k1gMnSc2	Hymenopter
(	(	kIx(	(
<g/>
blanokřídlí	blanokřídlí	k1gMnPc1	blanokřídlí
<g/>
)	)	kIx)	)
Coleoptera	Coleopter	k1gMnSc2	Coleopter
(	(	kIx(	(
<g/>
brouci	brouk	k1gMnPc1	brouk
<g/>
)	)	kIx)	)
Strepsiptera	Strepsipter	k1gMnSc2	Strepsipter
(	(	kIx(	(
<g/>
řásnokřídlí	řásnokřídlý	k2eAgMnPc1d1	řásnokřídlý
<g/>
)	)	kIx)	)
Raphidioptera	Raphidiopter	k1gMnSc2	Raphidiopter
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dlouhošíjky	dlouhošíjka	k1gFnSc2	dlouhošíjka
<g/>
)	)	kIx)	)
Megaloptera	Megalopter	k1gMnSc4	Megalopter
(	(	kIx(	(
<g/>
střechatky	střechatka	k1gFnSc2	střechatka
<g/>
)	)	kIx)	)
Neuroptera	Neuropter	k1gMnSc2	Neuropter
(	(	kIx(	(
<g/>
síťokřídlí	síťokřídlí	k1gMnPc1	síťokřídlí
<g/>
)	)	kIx)	)
Mecoptera	Mecopter	k1gMnSc4	Mecopter
(	(	kIx(	(
<g/>
srpice	srpice	k1gFnSc2	srpice
<g/>
)	)	kIx)	)
Siphonaptera	Siphonapter	k1gMnSc4	Siphonapter
(	(	kIx(	(
<g/>
blechy	blecha	k1gFnSc2	blecha
<g/>
)	)	kIx)	)
Diptera	Dipter	k1gMnSc2	Dipter
(	(	kIx(	(
<g/>
dvoukřídlí	dvoukřídlí	k1gMnPc1	dvoukřídlí
<g/>
)	)	kIx)	)
Protodiptera	Protodipter	k1gMnSc2	Protodipter
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Nadřád	nadřád	k1gInSc1	nadřád
<g/>
:	:	kIx,	:
Amphiesmenoptera	Amphiesmenopter	k1gMnSc2	Amphiesmenopter
Trichoptera	Trichopter	k1gMnSc2	Trichopter
(	(	kIx(	(
<g/>
chrostíci	chrostík	k1gMnPc1	chrostík
<g/>
)	)	kIx)	)
Lepidoptera	Lepidopter	k1gMnSc2	Lepidopter
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
motýli	motýl	k1gMnPc1	motýl
<g/>
)	)	kIx)	)
Incertae	Incertae	k1gFnSc1	Incertae
sedis	sedis	k1gFnSc1	sedis
Glosselytrodea	Glosselytrodea	k1gFnSc1	Glosselytrodea
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Miomoptera	Miomopter	k1gMnSc2	Miomopter
-	-	kIx~	-
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
výše	výše	k1gFnSc1	výše
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc1	hmyz
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
podtříd	podtřída	k1gFnPc2	podtřída
<g/>
;	;	kIx,	;
Apterygota	Apterygota	k1gFnSc1	Apterygota
(	(	kIx(	(
<g/>
bezkřídlí	bezkřídlý	k2eAgMnPc1d1	bezkřídlý
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pterygota	Pterygota	k1gFnSc1	Pterygota
(	(	kIx(	(
<g/>
křídlatí	křídlatý	k2eAgMnPc1d1	křídlatý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Apterygota	Apterygota	k1gFnSc1	Apterygota
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvěma	dva	k4xCgInPc7	dva
řády	řád	k1gInPc7	řád
<g/>
:	:	kIx,	:
Archaeognatha	Archaeognatha	k1gFnSc1	Archaeognatha
(	(	kIx(	(
<g/>
chvostnatky	chvostnatka	k1gFnPc1	chvostnatka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Thysanura	Thysanura	k1gFnSc1	Thysanura
(	(	kIx(	(
<g/>
šupinušky	šupinuška	k1gFnPc1	šupinuška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
navrhovaném	navrhovaný	k2eAgNnSc6d1	navrhované
třídění	třídění	k1gNnSc6	třídění
Archaeognatha	Archaeognath	k1gMnSc2	Archaeognath
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
Monocondylia	Monocondylia	k1gFnSc1	Monocondylia
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Thysanura	Thysanura	k1gFnSc1	Thysanura
a	a	k8xC	a
Pterygota	Pterygota	k1gFnSc1	Pterygota
jsou	být	k5eAaImIp3nP	být
seskupeny	seskupit	k5eAaPmNgInP	seskupit
společně	společně	k6eAd1	společně
jak	jak	k8xC	jak
Dicondylia	Dicondylia	k1gFnSc1	Dicondylia
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Thysanura	Thysanura	k1gFnSc1	Thysanura
samotná	samotný	k2eAgFnSc1d1	samotná
není	být	k5eNaImIp3nS	být
monofylní	monofylní	k2eAgFnSc1d1	monofylní
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
příbuznou	příbuzný	k2eAgFnSc4d1	příbuzná
skupinu	skupina	k1gFnSc4	skupina
Lepidotrichidae	Lepidotrichida	k1gInSc2	Lepidotrichida
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
sesterská	sesterský	k2eAgFnSc1d1	sesterská
skupina	skupina	k1gFnSc1	skupina
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
Dicondylia	Dicondylium	k1gNnSc2	Dicondylium
(	(	kIx(	(
<g/>
Pterygota	Pterygota	k1gFnSc1	Pterygota
+	+	kIx~	+
zbytek	zbytek	k1gInSc1	zbytek
skupiny	skupina	k1gFnSc2	skupina
Thysanura	Thysanur	k1gMnSc2	Thysanur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
infratřídě	infratříd	k1gInSc6	infratříd
Neoptera	Neopter	k1gMnSc2	Neopter
můžeme	moct	k5eAaImIp1nP	moct
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
brzké	brzký	k2eAgFnSc6d1	brzká
době	doba	k1gFnSc6	doba
očekávat	očekávat	k5eAaImF	očekávat
reorganizaci	reorganizace	k1gFnSc4	reorganizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
Neoptera	Neopter	k1gMnSc4	Neopter
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
nadřádů	nadřád	k1gInPc2	nadřád
Exopterygota	Exopterygot	k1gMnSc2	Exopterygot
a	a	k8xC	a
Endopterygota	Endopterygot	k1gMnSc2	Endopterygot
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
když	když	k8xS	když
Endopterygota	Endopterygota	k1gFnSc1	Endopterygota
bude	být	k5eAaImBp3nS	být
monofylní	monofylní	k2eAgFnSc1d1	monofylní
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
že	že	k8xS	že
Exopterygota	Exopterygota	k1gFnSc1	Exopterygota
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
parafylní	parafylní	k2eAgFnSc1d1	parafylní
a	a	k8xC	a
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
menší	malý	k2eAgFnPc4d2	menší
skupiny	skupina	k1gFnPc4	skupina
<g/>
;	;	kIx,	;
Paraneoptera	Paraneopter	k1gMnSc2	Paraneopter
<g/>
,	,	kIx,	,
Dictyoptera	Dictyopter	k1gMnSc2	Dictyopter
<g/>
,	,	kIx,	,
Orthopteroidea	Orthopteroideus	k1gMnSc2	Orthopteroideus
a	a	k8xC	a
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
Grylloblattodea	Grylloblattodeum	k1gNnSc2	Grylloblattodeum
+	+	kIx~	+
Mantophasmatodea	Mantophasmatodeus	k1gMnSc4	Mantophasmatodeus
a	a	k8xC	a
Plecoptera	Plecopter	k1gMnSc4	Plecopter
+	+	kIx~	+
Zoraptera	Zorapter	k1gMnSc4	Zorapter
+	+	kIx~	+
Dermaptera	Dermapter	k1gMnSc4	Dermapter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Phasmatodea	Phasmatodeus	k1gMnSc2	Phasmatodeus
a	a	k8xC	a
Embioptera	Embiopter	k1gMnSc2	Embiopter
byly	být	k5eAaImAgFnP	být
navrhovány	navrhován	k2eAgFnPc1d1	navrhována
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
skupiny	skupina	k1gFnSc2	skupina
Eukinolabia	Eukinolabium	k1gNnSc2	Eukinolabium
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Strepsiptera	Strepsipter	k1gMnSc4	Strepsipter
a	a	k8xC	a
Diptera	Dipter	k1gMnSc4	Dipter
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
seskupeny	seskupit	k5eAaPmNgInP	seskupit
společně	společně	k6eAd1	společně
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Halteria	Halterium	k1gNnSc2	Halterium
<g/>
.	.	kIx.	.
</s>
<s>
Paraneoptera	Paraneopter	k1gMnSc2	Paraneopter
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
úzce	úzko	k6eAd1	úzko
spřízněna	spříznit	k5eAaPmNgFnS	spříznit
s	s	k7c7	s
nadřádem	nadřád	k1gInSc7	nadřád
Endopterygota	Endopterygot	k1gMnSc2	Endopterygot
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
zbytkem	zbytek	k1gInSc7	zbytek
nadřádu	nadřád	k1gInSc2	nadřád
Exopterygota	Exopterygot	k1gMnSc4	Exopterygot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
docela	docela	k6eAd1	docela
jasné	jasný	k2eAgNnSc1d1	jasné
jak	jak	k8xS	jak
blízce	blízce	k6eAd1	blízce
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
zbývající	zbývající	k2eAgFnPc4d1	zbývající
skupiny	skupina	k1gFnPc4	skupina
Exopterygote	Exopterygot	k1gInSc5	Exopterygot
příbuzné	příbuzný	k1gMnPc4	příbuzný
a	a	k8xC	a
jestli	jestli	k8xS	jestli
jsou	být	k5eAaImIp3nP	být
sdruženy	sdružen	k2eAgInPc1d1	sdružen
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
větší	veliký	k2eAgFnSc6d2	veliký
jednotce	jednotka	k1gFnSc6	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
další	další	k2eAgInSc1d1	další
výzkum	výzkum	k1gInSc1	výzkum
může	moct	k5eAaImIp3nS	moct
přinést	přinést	k5eAaPmF	přinést
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
otázky	otázka	k1gFnPc4	otázka
odpovědi	odpověď	k1gFnSc3	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
molekulárně	molekulárně	k6eAd1	molekulárně
biologických	biologický	k2eAgFnPc2d1	biologická
analýz	analýza	k1gFnPc2	analýza
se	se	k3xPyFc4	se
fylogenetické	fylogenetický	k2eAgFnPc1d1	fylogenetická
představy	představa	k1gFnPc1	představa
stále	stále	k6eAd1	stále
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
a	a	k8xC	a
v	v	k7c4	v
tradiční	tradiční	k2eAgFnPc4d1	tradiční
výše	výše	k1gFnPc4	výše
uvedený	uvedený	k2eAgInSc1d1	uvedený
systém	systém	k1gInSc1	systém
tak	tak	k6eAd1	tak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nepřirozené	přirozený	k2eNgInPc4d1	nepřirozený
taxony	taxon	k1gInPc4	taxon
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
některých	některý	k3yIgInPc2	některý
zavedených	zavedený	k2eAgInPc2d1	zavedený
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
parafyletické	parafyletický	k2eAgInPc1d1	parafyletický
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
ukázaly	ukázat	k5eAaPmAgInP	ukázat
taxony	taxon	k1gInPc1	taxon
bezkřídlí	bezkřídlí	k1gMnPc5	bezkřídlí
(	(	kIx(	(
<g/>
Apterygota	Apterygota	k1gFnSc1	Apterygota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šupinušky	šupinuška	k1gFnSc2	šupinuška
(	(	kIx(	(
<g/>
Thysanura	Thysanura	k1gFnSc1	Thysanura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Exopterygota	Exopterygota	k1gFnSc1	Exopterygota
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
pojetí	pojetí	k1gNnSc6	pojetí
také	také	k9	také
Endopterygota	Endopterygot	k1gMnSc4	Endopterygot
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Amphiesmenoptera	Amphiesmenopter	k1gMnSc4	Amphiesmenopter
<g/>
;	;	kIx,	;
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
upřednostňován	upřednostňován	k2eAgInSc1d1	upřednostňován
název	název	k1gInSc1	název
Holometabola	Holometabola	k1gFnSc1	Holometabola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
švábi	šváb	k1gMnPc1	šváb
(	(	kIx(	(
<g/>
Blattodea	Blattodea	k1gMnSc1	Blattodea
<g/>
)	)	kIx)	)
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
všekazům	všekaz	k1gMnPc3	všekaz
<g/>
,	,	kIx,	,
srpice	srpice	k1gFnSc1	srpice
(	(	kIx(	(
<g/>
Mecoptera	Mecopter	k1gMnSc2	Mecopter
<g/>
)	)	kIx)	)
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
blechám	blecha	k1gFnPc3	blecha
a	a	k8xC	a
pisivky	pisivka	k1gFnPc1	pisivka
(	(	kIx(	(
<g/>
Psocoptera	Psocopter	k1gMnSc2	Psocopter
<g/>
)	)	kIx)	)
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
vším	veš	k1gFnPc3	veš
a	a	k8xC	a
všenkám	všenka	k1gFnPc3	všenka
<g/>
;	;	kIx,	;
vši	veš	k1gFnSc3	veš
a	a	k8xC	a
všenky	všenka	k1gFnSc2	všenka
(	(	kIx(	(
<g/>
Phthiraptera	Phthirapter	k1gMnSc2	Phthirapter
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
dokonce	dokonce	k9	dokonce
skupinou	skupina	k1gFnSc7	skupina
polyfyletickou	polyfyletický	k2eAgFnSc7d1	polyfyletická
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
studií	studie	k1gFnPc2	studie
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
parafyletické	parafyletický	k2eAgInPc1d1	parafyletický
i	i	k8xC	i
rybenky	rybenka	k1gFnPc4	rybenka
(	(	kIx(	(
<g/>
Zygentoma	Zygentoma	k1gNnSc4	Zygentoma
<g/>
)	)	kIx)	)
a	a	k8xC	a
strašilky	strašilka	k1gFnSc2	strašilka
(	(	kIx(	(
<g/>
Phasmatodea	Phasmatodea	k1gMnSc1	Phasmatodea
<g/>
)	)	kIx)	)
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
,	,	kIx,	,
nepřirozeným	přirozený	k2eNgNnPc3d1	nepřirozené
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
i	i	k9	i
vyšší	vysoký	k2eAgInSc4d2	vyšší
taxon	taxon	k1gInSc4	taxon
Antliophora	Antliophora	k1gFnSc1	Antliophora
(	(	kIx(	(
<g/>
dvoukřídlí	dvoukřídlí	k1gMnPc1	dvoukřídlí
<g/>
,	,	kIx,	,
blechy	blecha	k1gFnPc1	blecha
a	a	k8xC	a
srpice	srpice	k1gFnPc1	srpice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pevnou	pevný	k2eAgFnSc4d1	pevná
podporu	podpora	k1gFnSc4	podpora
nemá	mít	k5eNaImIp3nS	mít
ani	ani	k8xC	ani
pozice	pozice	k1gFnSc1	pozice
hmyzu	hmyz	k1gInSc2	hmyz
ve	v	k7c6	v
fylogenetickém	fylogenetický	k2eAgInSc6d1	fylogenetický
stromu	strom	k1gInSc6	strom
členovců	členovec	k1gMnPc2	členovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgNnPc6d1	dřívější
studiích	studio	k1gNnPc6	studio
vycházeli	vycházet	k5eAaImAgMnP	vycházet
jako	jako	k9	jako
sesterská	sesterský	k2eAgFnSc1d1	sesterská
skupina	skupina	k1gFnSc1	skupina
lupenonožci	lupenonožec	k1gMnSc3	lupenonožec
<g/>
/	/	kIx~	/
<g/>
žábronožci	žábronožec	k1gMnSc3	žábronožec
<g/>
,	,	kIx,	,
v	v	k7c6	v
novějších	nový	k2eAgInPc6d2	novější
převládají	převládat	k5eAaImIp3nP	převládat
veslonožci	veslonožec	k1gMnPc7	veslonožec
(	(	kIx(	(
<g/>
Remipedia	Remipedium	k1gNnSc2	Remipedium
<g/>
)	)	kIx)	)
či	či	k8xC	či
Xenocarida	Xenocarida	k1gFnSc1	Xenocarida
(	(	kIx(	(
<g/>
veslonožci	veslonožec	k1gMnPc1	veslonožec
+	+	kIx~	+
volnohlavci	volnohlavec	k1gMnPc1	volnohlavec
(	(	kIx(	(
<g/>
Cephalocarida	Cephalocarida	k1gFnSc1	Cephalocarida
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
představy	představa	k1gFnPc1	představa
o	o	k7c6	o
příbuznosti	příbuznost	k1gFnSc6	příbuznost
recentních	recentní	k2eAgFnPc2d1	recentní
skupin	skupina	k1gFnPc2	skupina
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
následující	následující	k2eAgInSc1d1	následující
fylogenetický	fylogenetický	k2eAgInSc1d1	fylogenetický
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
oba	dva	k4xCgInPc4	dva
taxony	taxon	k1gInPc4	taxon
používané	používaný	k2eAgInPc4d1	používaný
jako	jako	k8xC	jako
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
hmyz	hmyz	k1gInSc4	hmyz
(	(	kIx(	(
<g/>
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
šestinohých	šestinohý	k2eAgFnPc2d1	šestinohá
-	-	kIx~	-
Hexapoda	Hexapoda	k1gMnSc1	Hexapoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
jevnočelistných	jevnočelistný	k2eAgFnPc2d1	jevnočelistný
-	-	kIx~	-
Ectognatha	Ectognatha	k1gMnSc1	Ectognatha
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nepřirozené	přirozený	k2eNgFnSc2d1	nepřirozená
(	(	kIx(	(
<g/>
parafyletické	parafyletický	k2eAgFnSc2d1	parafyletická
či	či	k8xC	či
polyfyletické	polyfyletický	k2eAgFnSc2d1	polyfyletická
skupiny	skupina	k1gFnSc2	skupina
jsou	být	k5eAaImIp3nP	být
označeny	označen	k2eAgInPc1d1	označen
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
lidmi	člověk	k1gMnPc7	člověk
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
škodlivé	škodlivý	k2eAgNnSc4d1	škodlivé
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
škodlivého	škodlivý	k2eAgInSc2d1	škodlivý
hmyzu	hmyz	k1gInSc2	hmyz
jsou	být	k5eAaImIp3nP	být
zahrnováni	zahrnován	k2eAgMnPc1d1	zahrnován
parazité	parazit	k1gMnPc1	parazit
(	(	kIx(	(
<g/>
komáři	komár	k1gMnPc1	komár
<g/>
,	,	kIx,	,
vši	veš	k1gFnPc1	veš
<g/>
,	,	kIx,	,
štěnice	štěnice	k1gFnPc1	štěnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přenašeči	přenašeč	k1gInSc3	přenašeč
chorob	choroba	k1gFnPc2	choroba
(	(	kIx(	(
<g/>
komár	komár	k1gMnSc1	komár
<g/>
,	,	kIx,	,
moucha	moucha	k1gFnSc1	moucha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ničitelé	ničitel	k1gMnPc1	ničitel
staveb	stavba	k1gFnPc2	stavba
(	(	kIx(	(
<g/>
termiti	termit	k1gMnPc1	termit
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
škůdci	škůdce	k1gMnPc1	škůdce
na	na	k7c6	na
zemědělských	zemědělský	k2eAgFnPc6d1	zemědělská
plodinách	plodina	k1gFnPc6	plodina
(	(	kIx(	(
<g/>
saranče	saranče	k1gFnSc1	saranče
<g/>
,	,	kIx,	,
mandelinka	mandelinka	k1gFnSc1	mandelinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
entomologů	entomolog	k1gMnPc2	entomolog
se	se	k3xPyFc4	se
zapojuje	zapojovat	k5eAaImIp3nS	zapojovat
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
forem	forma	k1gFnPc2	forma
hubení	hubení	k1gNnPc2	hubení
hmyzích	hmyzí	k2eAgMnPc2d1	hmyzí
škůdců	škůdce	k1gMnPc2	škůdce
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
již	již	k9	již
často	často	k6eAd1	často
používanými	používaný	k2eAgInPc7d1	používaný
insekticidy	insekticid	k1gInPc7	insekticid
nebo	nebo	k8xC	nebo
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
rozvíjejícími	rozvíjející	k2eAgFnPc7d1	rozvíjející
metodami	metoda	k1gFnPc7	metoda
biokontroly	biokontrola	k1gFnSc2	biokontrola
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
obtížný	obtížný	k2eAgInSc1d1	obtížný
hmyz	hmyz	k1gInSc1	hmyz
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
největší	veliký	k2eAgFnSc4d3	veliký
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
působí	působit	k5eAaImIp3nS	působit
naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
blahodárně	blahodárně	k6eAd1	blahodárně
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
opylovači	opylovač	k1gInPc7	opylovač
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vosy	vosa	k1gFnPc1	vosa
<g/>
,	,	kIx,	,
včely	včela	k1gFnPc1	včela
<g/>
,	,	kIx,	,
motýli	motýl	k1gMnPc1	motýl
<g/>
,	,	kIx,	,
mravenci	mravenec	k1gMnPc1	mravenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opylování	opylování	k1gNnSc1	opylování
je	být	k5eAaImIp3nS	být
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
obchod	obchod	k1gInSc4	obchod
mezi	mezi	k7c7	mezi
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
je	on	k3xPp3gMnPc4	on
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
reprodukci	reprodukce	k1gFnSc3	reprodukce
<g/>
,	,	kIx,	,
a	a	k8xC	a
opylovači	opylovač	k1gMnPc1	opylovač
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
za	za	k7c4	za
ně	on	k3xPp3gFnPc4	on
berou	brát	k5eAaImIp3nP	brát
odměnu	odměna	k1gFnSc4	odměna
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nektaru	nektar	k1gInSc2	nektar
a	a	k8xC	a
pylu	pyl	k1gInSc2	pyl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
vážným	vážný	k2eAgInSc7d1	vážný
problémem	problém	k1gInSc7	problém
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
pokles	pokles	k1gInSc1	pokles
populací	populace	k1gFnPc2	populace
opylovačů	opylovač	k1gMnPc2	opylovač
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
také	také	k6eAd1	také
k	k	k7c3	k
opylování	opylování	k1gNnSc3	opylování
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
sadů	sad	k1gInPc2	sad
a	a	k8xC	a
skleníkových	skleníkový	k2eAgFnPc2d1	skleníková
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
době	doba	k1gFnSc6	doba
květu	květ	k1gInSc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc1	hmyz
také	také	k9	také
produkuje	produkovat	k5eAaImIp3nS	produkovat
užitečné	užitečný	k2eAgFnPc4d1	užitečná
suroviny	surovina	k1gFnPc4	surovina
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
med	med	k1gInSc4	med
<g/>
,	,	kIx,	,
vosk	vosk	k1gInSc4	vosk
<g/>
,	,	kIx,	,
lak	lak	k1gInSc4	lak
a	a	k8xC	a
přírodní	přírodní	k2eAgNnSc4d1	přírodní
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
medonosné	medonosný	k2eAgFnPc1d1	medonosná
byly	být	k5eAaImAgFnP	být
chovány	chován	k2eAgFnPc1d1	chována
po	po	k7c4	po
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
medu	med	k1gInSc3	med
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
jejich	jejich	k3xOp3gFnSc1	jejich
úloha	úloha	k1gFnSc1	úloha
při	při	k7c6	při
opylování	opylování	k1gNnSc6	opylování
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgNnSc1d1	přírodní
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
,	,	kIx,	,
produkt	produkt	k1gInSc1	produkt
housenek	housenka	k1gFnPc2	housenka
bource	bourec	k1gMnSc4	bourec
morušového	morušový	k2eAgMnSc4d1	morušový
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
veliký	veliký	k2eAgInSc4d1	veliký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
obchodu	obchod	k1gInSc2	obchod
mezi	mezi	k7c7	mezi
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
zbytkem	zbytek	k1gInSc7	zbytek
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Larvy	larva	k1gFnPc1	larva
much	moucha	k1gFnPc2	moucha
(	(	kIx(	(
<g/>
červi	červ	k1gMnPc1	červ
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
dříve	dříve	k6eAd2	dříve
přikládány	přikládat	k5eAaImNgInP	přikládat
na	na	k7c4	na
rány	rána	k1gFnPc4	rána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
zabráněno	zabráněn	k2eAgNnSc1d1	zabráněno
nebo	nebo	k8xC	nebo
zastaveno	zastaven	k2eAgNnSc1d1	zastaveno
vytváření	vytváření	k1gNnSc1	vytváření
sněti	sněť	k1gFnSc2	sněť
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
požírají	požírat	k5eAaImIp3nP	požírat
pouze	pouze	k6eAd1	pouze
rozkládající	rozkládající	k2eAgNnSc1d1	rozkládající
se	se	k3xPyFc4	se
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
ošetřování	ošetřování	k1gNnSc1	ošetřování
ran	rána	k1gFnPc2	rána
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
hmyzí	hmyzí	k2eAgMnPc1d1	hmyzí
jedinci	jedinec	k1gMnPc1	jedinec
jako	jako	k8xS	jako
např.	např.	kA	např.
cvrčci	cvrček	k1gMnPc1	cvrček
nebo	nebo	k8xC	nebo
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
larev	larva	k1gFnPc2	larva
se	se	k3xPyFc4	se
také	také	k9	také
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
jako	jako	k8xC	jako
rybářská	rybářský	k2eAgFnSc1d1	rybářská
návnada	návnada	k1gFnSc1	návnada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
hmyz	hmyz	k1gInSc1	hmyz
využíván	využívat	k5eAaPmNgInS	využívat
jako	jako	k8xS	jako
výživná	výživný	k2eAgFnSc1d1	výživná
potrava	potrava	k1gFnSc1	potrava
(	(	kIx(	(
<g/>
entomofágie	entomofágie	k1gFnSc1	entomofágie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
jiných	jiná	k1gFnPc6	jiná
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
konzumace	konzumace	k1gFnSc1	konzumace
tabu	tabu	k2eAgFnSc1d1	tabu
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
zastánců	zastánce	k1gMnPc2	zastánce
rozvoje	rozvoj	k1gInSc2	rozvoj
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
lidského	lidský	k2eAgNnSc2d1	lidské
stravování	stravování	k1gNnSc2	stravování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nelze	lze	k6eNd1	lze
zcela	zcela	k6eAd1	zcela
vyloučit	vyloučit	k5eAaPmF	vyloučit
otravu	otrava	k1gFnSc4	otrava
hmyzem	hmyz	k1gInSc7	hmyz
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
potravním	potravní	k2eAgInSc6d1	potravní
řetězci	řetězec	k1gInSc6	řetězec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
již	již	k9	již
hmyz	hmyz	k1gInSc1	hmyz
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
potravinách	potravina	k1gFnPc6	potravina
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
zrnech	zrno	k1gNnPc6	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
si	se	k3xPyFc3	se
neuvědomují	uvědomovat	k5eNaImIp3nP	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
není	být	k5eNaImIp3nS	být
podíl	podíl	k1gInSc1	podíl
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
různými	různý	k2eAgInPc7d1	různý
množstevními	množstevní	k2eAgInPc7d1	množstevní
limity	limit	k1gInPc7	limit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
antropologa	antropolog	k1gMnSc2	antropolog
Marvina	Marvin	k2eAgFnSc1d1	Marvina
Harrise	Harrise	k1gFnSc1	Harrise
je	být	k5eAaImIp3nS	být
konzumace	konzumace	k1gFnSc1	konzumace
hmyzu	hmyz	k1gInSc2	hmyz
tabu	tabu	k1gNnPc2	tabu
v	v	k7c6	v
kulturách	kultura	k1gFnPc6	kultura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získávání	získávání	k1gNnSc1	získávání
bílkovin	bílkovina	k1gFnPc2	bílkovina
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
velkou	velký	k2eAgFnSc4d1	velká
práci	práce	k1gFnSc4	práce
(	(	kIx(	(
<g/>
drůbeží	drůbeží	k2eAgInPc1d1	drůbeží
velkochovy	velkochov	k1gInPc1	velkochov
nebo	nebo	k8xC	nebo
velkofarmy	velkofarma	k1gFnPc1	velkofarma
dobytka	dobytek	k1gInSc2	dobytek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
chovány	chovat	k5eAaImNgInP	chovat
v	v	k7c6	v
teráriích	terárium	k1gNnPc6	terárium
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mravenci	mravenec	k1gMnPc1	mravenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
formikáriích	formikárium	k1gNnPc6	formikárium
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
blanokřídlý	blanokřídlý	k2eAgInSc4d1	blanokřídlý
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
blízcí	blízký	k2eAgMnPc1d1	blízký
příbuzní	příbuzný	k1gMnPc1	příbuzný
včel	včela	k1gFnPc2	včela
<g/>
,	,	kIx,	,
vos	vosa	k1gFnPc2	vosa
a	a	k8xC	a
sršní	sršeň	k1gFnPc2	sršeň
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
terarijním	terarijní	k2eAgInSc7d1	terarijní
hmyzem	hmyz	k1gInSc7	hmyz
jsou	být	k5eAaImIp3nP	být
strašilky	strašilka	k1gFnPc1	strašilka
<g/>
,	,	kIx,	,
kobylky	kobylka	k1gFnPc1	kobylka
<g/>
,	,	kIx,	,
různí	různý	k2eAgMnPc1d1	různý
brouci	brouk	k1gMnPc1	brouk
<g/>
,	,	kIx,	,
larvy	larva	k1gFnPc1	larva
a	a	k8xC	a
různý	různý	k2eAgInSc1d1	různý
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnSc1	sloužící
jako	jako	k8xS	jako
potrava	potrava	k1gFnSc1	potrava
pro	pro	k7c4	pro
plazy	plaz	k1gMnPc4	plaz
chované	chovaný	k2eAgFnSc2d1	chovaná
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
také	také	k9	také
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
chov	chov	k1gInSc1	chov
motýlů	motýl	k1gMnPc2	motýl
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
sklenících	skleník	k1gInPc6	skleník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
a	a	k8xC	a
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
jim	on	k3xPp3gFnPc3	on
tvary	tvar	k1gInPc4	tvar
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
motýlů	motýl	k1gMnPc2	motýl
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
končin	končina	k1gFnPc2	končina
naší	náš	k3xOp1gFnSc2	náš
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
lékařském	lékařský	k2eAgInSc6d1	lékařský
průmyslu	průmysl	k1gInSc6	průmysl
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
známým	známý	k2eAgInSc7d1	známý
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
španělské	španělský	k2eAgFnPc1d1	španělská
mušky	muška	k1gFnPc1	muška
(	(	kIx(	(
<g/>
brouci	brouk	k1gMnPc1	brouk
puchýřnící	puchýřnící	k2eAgMnPc1d1	puchýřnící
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
majky	majka	k1gFnPc4	majka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
látku	látka	k1gFnSc4	látka
kantaridin	kantaridin	k1gInSc1	kantaridin
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vědecká	vědecký	k2eAgNnPc1d1	vědecké
pokusná	pokusný	k2eAgNnPc1d1	pokusné
zvířata	zvíře	k1gNnPc1	zvíře
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
octomilky	octomilka	k1gFnPc1	octomilka
obecné	obecný	k2eAgFnPc1d1	obecná
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc1d1	různá
pakobylky	pakobylka	k1gFnPc1	pakobylka
a	a	k8xC	a
druhy	druh	k1gInPc1	druh
brouků	brouk	k1gMnPc2	brouk
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
červů	červ	k1gMnPc2	červ
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
larvy	larva	k1gFnPc1	larva
různých	různý	k2eAgFnPc2d1	různá
much	moucha	k1gFnPc2	moucha
nebo	nebo	k8xC	nebo
brouků	brouk	k1gMnPc2	brouk
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
vražd	vražda	k1gFnPc2	vražda
v	v	k7c6	v
kriminalistice	kriminalistika	k1gFnSc6	kriminalistika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
hmyzu	hmyz	k1gInSc2	hmyz
využívány	využívat	k5eAaImNgInP	využívat
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
barviv	barvivo	k1gNnPc2	barvivo
<g/>
,	,	kIx,	,
laků	lak	k1gInPc2	lak
nebo	nebo	k8xC	nebo
vosků	vosk	k1gInPc2	vosk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
šelak	šelak	k1gInSc4	šelak
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
brouci	brouk	k1gMnPc1	brouk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mrchožrouti	mrchožrout	k1gMnPc1	mrchožrout
a	a	k8xC	a
živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
uhynulými	uhynulý	k2eAgMnPc7d1	uhynulý
živočichy	živočich	k1gMnPc7	živočich
nebo	nebo	k8xC	nebo
padlými	padlý	k2eAgInPc7d1	padlý
stromy	strom	k1gInPc7	strom
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
recyklují	recyklovat	k5eAaBmIp3nP	recyklovat
biologický	biologický	k2eAgInSc4d1	biologický
materiál	materiál	k1gInSc4	materiál
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
vhodné	vhodný	k2eAgFnPc4d1	vhodná
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
dokonce	dokonce	k9	dokonce
brouka	brouk	k1gMnSc2	brouk
vrubouna	vruboun	k1gMnSc2	vruboun
posvátného	posvátný	k2eAgMnSc2d1	posvátný
(	(	kIx(	(
<g/>
scarabea	scarabeus	k1gMnSc2	scarabeus
<g/>
)	)	kIx)	)
uctívali	uctívat	k5eAaImAgMnP	uctívat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nejsou	být	k5eNaImIp3nP	být
moc	moc	k6eAd1	moc
známí	známý	k2eAgMnPc1d1	známý
<g/>
,	,	kIx,	,
nejužitečnějším	užitečný	k2eAgInSc7d3	nejužitečnější
hmyzem	hmyz	k1gInSc7	hmyz
jsou	být	k5eAaImIp3nP	být
hmyzožravci	hmyzožravec	k1gMnPc1	hmyzožravec
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
jiným	jiný	k2eAgInSc7d1	jiný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
kobylky	kobylka	k1gFnSc2	kobylka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
tak	tak	k6eAd1	tak
rychle	rychle	k6eAd1	rychle
množit	množit	k5eAaImF	množit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
mohly	moct	k5eAaImAgFnP	moct
doslova	doslova	k6eAd1	doslova
pokrýt	pokrýt	k5eAaPmF	pokrýt
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
vajíčky	vajíčko	k1gNnPc7	vajíčko
kobylek	kobylka	k1gFnPc2	kobylka
a	a	k8xC	a
některé	některý	k3yIgNnSc1	některý
dokonce	dokonce	k9	dokonce
i	i	k8xC	i
samotnými	samotný	k2eAgMnPc7d1	samotný
dospělými	dospělý	k2eAgMnPc7d1	dospělý
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
přisuzujeme	přisuzovat	k5eAaImIp1nP	přisuzovat
především	především	k6eAd1	především
ptactvu	ptactvo	k1gNnSc3	ptactvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
atraktivní	atraktivní	k2eAgInSc4d1	atraktivní
hmyz	hmyz	k1gInSc4	hmyz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
o	o	k7c4	o
hodně	hodně	k6eAd1	hodně
důležitější	důležitý	k2eAgNnSc4d2	důležitější
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
všechny	všechen	k3xTgFnPc4	všechen
škůdce	škůdce	k1gMnSc1	škůdce
hmyzu	hmyz	k1gInSc2	hmyz
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jmenovat	jmenovat	k5eAaBmF	jmenovat
např.	např.	kA	např.
parazitické	parazitický	k2eAgFnPc4d1	parazitická
vosy	vosa	k1gFnPc4	vosa
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
larvy	larva	k1gFnPc1	larva
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
svých	svůj	k3xOyFgMnPc2	svůj
hmyzích	hmyzí	k2eAgMnPc2d1	hmyzí
hostitelů	hostitel	k1gMnPc2	hostitel
a	a	k8xC	a
hrají	hrát	k5eAaImIp3nP	hrát
tak	tak	k6eAd1	tak
význačnou	význačný	k2eAgFnSc4d1	význačná
roli	role	k1gFnSc4	role
v	v	k7c6	v
regulaci	regulace	k1gFnSc6	regulace
populace	populace	k1gFnSc2	populace
některých	některý	k3yIgInPc2	některý
hmyzích	hmyzí	k2eAgInPc2d1	hmyzí
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Slunéčka	slunéčko	k1gNnPc1	slunéčko
(	(	kIx(	(
<g/>
dospělci	dospělec	k1gMnPc1	dospělec
i	i	k8xC	i
larvy	larva	k1gFnPc1	larva
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
užitečná	užitečný	k2eAgNnPc1d1	užitečné
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokážou	dokázat	k5eAaPmIp3nP	dokázat
masivně	masivně	k6eAd1	masivně
likvidovat	likvidovat	k5eAaBmF	likvidovat
škodící	škodící	k2eAgFnPc4d1	škodící
mšice	mšice	k1gFnPc4	mšice
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
ploštice	ploštice	k1gFnPc1	ploštice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kněžice	kněžice	k1gFnSc2	kněžice
<g/>
)	)	kIx)	)
vysávají	vysávat	k5eAaImIp3nP	vysávat
škodlivé	škodlivý	k2eAgFnPc1d1	škodlivá
housenky	housenka	k1gFnPc1	housenka
motýlů	motýl	k1gMnPc2	motýl
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
známého	známý	k2eAgMnSc2d1	známý
běláska	bělásek	k1gMnSc2	bělásek
zelného	zelný	k2eAgMnSc2d1	zelný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vosy	vosa	k1gFnPc1	vosa
a	a	k8xC	a
zejména	zejména	k9	zejména
sršně	sršně	k6eAd1	sršně
chytají	chytat	k5eAaImIp3nP	chytat
obtížný	obtížný	k2eAgInSc4d1	obtížný
dvoukřídlý	dvoukřídlý	k2eAgInSc4d1	dvoukřídlý
hmyz	hmyz	k1gInSc4	hmyz
(	(	kIx(	(
<g/>
mouchy	moucha	k1gFnPc4	moucha
a	a	k8xC	a
ovády	ovád	k1gMnPc4	ovád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
krmí	krmit	k5eAaImIp3nS	krmit
své	svůj	k3xOyFgFnPc4	svůj
larvy	larva	k1gFnPc4	larva
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgInPc1d1	lidský
pokusy	pokus	k1gInPc1	pokus
regulovat	regulovat	k5eAaImF	regulovat
škodlivé	škodlivý	k2eAgInPc4d1	škodlivý
druhy	druh	k1gInPc4	druh
hmyzu	hmyz	k1gInSc2	hmyz
pomocí	pomocí	k7c2	pomocí
insekticidů	insekticid	k1gInPc2	insekticid
většinou	většinou	k6eAd1	většinou
selhávají	selhávat	k5eAaImIp3nP	selhávat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	s	k7c7	s
škodlivým	škodlivý	k2eAgInSc7d1	škodlivý
hmyzem	hmyz	k1gInSc7	hmyz
jsou	být	k5eAaImIp3nP	být
systematicky	systematicky	k6eAd1	systematicky
otravovány	otravován	k2eAgInPc1d1	otravován
i	i	k8xC	i
užitečné	užitečný	k2eAgInPc1d1	užitečný
druhy	druh	k1gInPc1	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
jimi	on	k3xPp3gNnPc7	on
živí	živit	k5eAaImIp3nS	živit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
kultuře	kultura	k1gFnSc6	kultura
nachází	nacházet	k5eAaImIp3nS	nacházet
především	především	k9	především
uplatnění	uplatnění	k1gNnSc4	uplatnění
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
užitečné	užitečný	k2eAgInPc1d1	užitečný
druhy	druh	k1gInPc1	druh
hmyzu	hmyz	k1gInSc2	hmyz
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
včely	včela	k1gFnSc2	včela
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
žijící	žijící	k2eAgInPc1d1	žijící
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
nebo	nebo	k8xC	nebo
hmyz	hmyz	k1gInSc1	hmyz
s	s	k7c7	s
kterým	který	k3yRgMnSc7	který
přicházel	přicházet	k5eAaImAgMnS	přicházet
často	často	k6eAd1	často
do	do	k7c2	do
styku	styk	k1gInSc2	styk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
blechy	blecha	k1gFnSc2	blecha
nebo	nebo	k8xC	nebo
vši	veš	k1gFnSc2	veš
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
také	také	k9	také
tvarově	tvarově	k6eAd1	tvarově
a	a	k8xC	a
barevně	barevně	k6eAd1	barevně
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
druhy	druh	k1gInPc4	druh
hmyzu	hmyz	k1gInSc2	hmyz
jako	jako	k8xS	jako
např.	např.	kA	např.
motýli	motýl	k1gMnPc1	motýl
<g/>
,	,	kIx,	,
vážky	vážka	k1gFnPc1	vážka
či	či	k8xC	či
brouci	brouk	k1gMnPc1	brouk
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
zobrazení	zobrazení	k1gNnSc1	zobrazení
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
s	s	k7c7	s
názvem	název	k1gInSc7	název
Misař	Misař	k1gMnSc1	Misař
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
maleb	malba	k1gFnPc2	malba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
22	[number]	k4	22
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známý	k2eAgMnPc7d1	známý
malíři	malíř	k1gMnPc7	malíř
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zobrazovali	zobrazovat	k5eAaImAgMnP	zobrazovat
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
personifikacemi	personifikace	k1gFnPc7	personifikace
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
Gerrit	Gerrit	k1gInSc1	Gerrit
van	vana	k1gFnPc2	vana
Honthorst	Honthorst	k1gInSc4	Honthorst
s	s	k7c7	s
malbami	malba	k1gFnPc7	malba
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
lov	lov	k1gInSc1	lov
blech	blecha	k1gFnPc2	blecha
při	při	k7c6	při
světle	světlo	k1gNnSc6	světlo
svíce	svíce	k1gFnSc2	svíce
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
Murillo	Murillo	k1gNnSc1	Murillo
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
známým	známý	k2eAgInSc7d1	známý
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
znázorňujícím	znázorňující	k2eAgInSc7d1	znázorňující
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vybírá	vybírat	k5eAaImIp3nS	vybírat
vešky	veška	k1gFnPc4	veška
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
svého	svůj	k3xOyFgNnSc2	svůj
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byli	být	k5eAaImAgMnP	být
milovníky	milovník	k1gMnPc7	milovník
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
především	především	k9	především
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
Hollar	Hollar	k1gMnSc1	Hollar
a	a	k8xC	a
Švabinský	Švabinský	k2eAgMnSc1d1	Švabinský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
jsou	být	k5eAaImIp3nP	být
příslušníci	příslušník	k1gMnPc1	příslušník
hmyzí	hmyzí	k2eAgFnSc2d1	hmyzí
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgNnPc2d1	české
děl	dělo	k1gNnPc2	dělo
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
především	především	k6eAd1	především
knihy	kniha	k1gFnPc1	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
s	s	k7c7	s
hmyzími	hmyzí	k2eAgMnPc7d1	hmyzí
hrdiny	hrdina	k1gMnPc7	hrdina
od	od	k7c2	od
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Sekory	Sekora	k1gFnSc2	Sekora
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Ferda	Ferda	k1gMnSc1	Ferda
Mravenec	mravenec	k1gMnSc1	mravenec
nebo	nebo	k8xC	nebo
Čmelák	čmelák	k1gMnSc1	čmelák
Aninka	Aninka	k1gFnSc1	Aninka
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
populární	populární	k2eAgFnPc1d1	populární
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
také	také	k9	také
světlušky	světluška	k1gFnPc4	světluška
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Jana	Jan	k1gMnSc2	Jan
Karafiáta	Karafiát	k1gMnSc2	Karafiát
Broučci	Brouček	k1gMnPc5	Brouček
<g/>
.	.	kIx.	.
</s>
<s>
Pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
lidské	lidský	k2eAgFnPc4d1	lidská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
pomocí	pomocí	k7c2	pomocí
hmyzího	hmyzí	k2eAgInSc2d1	hmyzí
světa	svět	k1gInSc2	svět
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
dramatu	drama	k1gNnSc6	drama
Ze	z	k7c2	z
života	život	k1gInSc2	život
hmyzu	hmyz	k1gInSc2	hmyz
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Hmyz	hmyz	k1gInSc1	hmyz
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
také	také	k9	také
často	často	k6eAd1	často
vidět	vidět	k5eAaImF	vidět
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
přibližující	přibližující	k2eAgInSc4d1	přibližující
svět	svět	k1gInSc4	svět
hmyzu	hmyz	k1gInSc2	hmyz
z	z	k7c2	z
lidského	lidský	k2eAgNnSc2d1	lidské
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc1d1	francouzský
snímek	snímek	k1gInSc1	snímek
Mikrokosmos	mikrokosmos	k1gInSc4	mikrokosmos
<g/>
,	,	kIx,	,
oceněný	oceněný	k2eAgInSc4d1	oceněný
čtyřmi	čtyři	k4xCgInPc7	čtyři
Cézary	Cézar	k1gInPc7	Cézar
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
zajímavými	zajímavý	k2eAgInPc7d1	zajímavý
filmy	film	k1gInPc7	film
jsou	být	k5eAaImIp3nP	být
britský	britský	k2eAgInSc1d1	britský
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
Království	království	k1gNnSc1	království
hmyzu	hmyz	k1gInSc2	hmyz
3D	[number]	k4	3D
nebo	nebo	k8xC	nebo
francouzský	francouzský	k2eAgInSc1d1	francouzský
film	film	k1gInSc1	film
Válka	válka	k1gFnSc1	válka
termitů	termit	k1gMnPc2	termit
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
však	však	k9	však
hmyz	hmyz	k1gInSc1	hmyz
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
hraných	hraný	k2eAgInPc6d1	hraný
filmech	film	k1gInPc6	film
jako	jako	k8xC	jako
přírodní	přírodní	k2eAgFnSc1d1	přírodní
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
ohrožující	ohrožující	k2eAgMnSc1d1	ohrožující
člověka	člověk	k1gMnSc2	člověk
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
filmy	film	k1gInPc1	film
Roj	roj	k1gInSc1	roj
<g/>
,	,	kIx,	,
Moucha	moucha	k1gFnSc1	moucha
<g/>
,	,	kIx,	,
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
,	,	kIx,	,
Marabunta	Marabunta	k1gFnSc1	Marabunta
-	-	kIx~	-
mravenci	mravenec	k1gMnPc1	mravenec
útočí	útočit	k5eAaImIp3nP	útočit
<g/>
,	,	kIx,	,
Mumie	mumie	k1gFnSc1	mumie
apod.	apod.	kA	apod.
Ještě	ještě	k9	ještě
známější	známý	k2eAgMnPc1d2	známější
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
kreslené	kreslený	k2eAgInPc4d1	kreslený
snímky	snímek	k1gInPc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Celovečerní	celovečerní	k2eAgInPc1d1	celovečerní
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
jsou	být	k5eAaImIp3nP	být
hrdiny	hrdina	k1gMnPc7	hrdina
mravenci	mravenec	k1gMnPc1	mravenec
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
Mravenec	mravenec	k1gMnSc1	mravenec
Z	z	k7c2	z
<g/>
,	,	kIx,	,
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
Život	život	k1gInSc1	život
brouka	brouk	k1gMnSc2	brouk
anebo	anebo	k8xC	anebo
Mravenčí	mravenčit	k5eAaImIp3nS	mravenčit
polepšovna	polepšovna	k1gFnSc1	polepšovna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
televizních	televizní	k2eAgInPc2d1	televizní
seriálů	seriál	k1gInPc2	seriál
je	být	k5eAaImIp3nS	být
nepochybně	pochybně	k6eNd1	pochybně
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejznámější	známý	k2eAgFnSc1d3	nejznámější
Včelka	včelka	k1gFnSc1	včelka
Mája	Mája	k1gFnSc1	Mája
<g/>
,	,	kIx,	,
natočená	natočený	k2eAgFnSc1d1	natočená
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
Waldemara	Waldemar	k1gMnSc4	Waldemar
Bonselse	Bonsels	k1gMnSc4	Bonsels
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
animovaných	animovaný	k2eAgInPc2d1	animovaný
krátkometrážních	krátkometrážní	k2eAgInPc2d1	krátkometrážní
snímků	snímek	k1gInPc2	snímek
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejznámější	známý	k2eAgFnPc4d3	nejznámější
večerníčkovské	večerníčkovský	k2eAgFnPc4d1	večerníčkovská
série	série	k1gFnPc4	série
jako	jako	k8xC	jako
např.	např.	kA	např.
Jak	jak	k8xC	jak
Bzuk	bzuk	k1gInSc4	bzuk
a	a	k8xC	a
Ťuk	ťuk	k1gInSc4	ťuk
putovali	putovat	k5eAaImAgMnP	putovat
za	za	k7c7	za
sluníčkem	sluníčko	k1gNnSc7	sluníčko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
O	o	k7c6	o
cvrčkovi	cvrček	k1gMnSc6	cvrček
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
Příběhy	příběh	k1gInPc4	příběh
včelích	včelí	k2eAgMnPc2d1	včelí
medvídků	medvídek	k1gMnPc2	medvídek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
a	a	k8xC	a
Jak	jak	k6eAd1	jak
Bzuk	bzuk	k1gInSc4	bzuk
a	a	k8xC	a
Ťuk	ťuk	k1gInSc4	ťuk
nechtěli	chtít	k5eNaImAgMnP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pršelo	pršet	k5eAaImAgNnS	pršet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
také	také	k9	také
československý	československý	k2eAgInSc1d1	československý
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Jána	Ján	k1gMnSc2	Ján
Roháče	roháč	k1gMnSc2	roháč
Traja	Traja	k1gMnSc1	Traja
chrobáci	chrobák	k1gMnPc1	chrobák
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získal	získat	k5eAaPmAgInS	získat
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
pořadů	pořad	k1gInPc2	pořad
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
hudbě	hudba	k1gFnSc6	hudba
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nespornou	sporný	k2eNgFnSc4d1	nesporná
popularitu	popularita	k1gFnSc4	popularita
skladba	skladba	k1gFnSc1	skladba
N.	N.	kA	N.
A.	A.	kA	A.
Rimského	Rimský	k1gMnSc2	Rimský
Korsakova	Korsakov	k1gMnSc2	Korsakov
Let	léto	k1gNnPc2	léto
čmeláka	čmelák	k1gMnSc2	čmelák
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
moderní	moderní	k2eAgFnSc2d1	moderní
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejznámější	známý	k2eAgInSc1d3	nejznámější
název	název	k1gInSc1	název
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnSc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Brouk	brouk	k1gMnSc1	brouk
se	se	k3xPyFc4	se
také	také	k9	také
nazývá	nazývat	k5eAaImIp3nS	nazývat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgInPc2d3	nejpopulárnější
automobilových	automobilový	k2eAgInPc2d1	automobilový
vozů	vůz	k1gInPc2	vůz
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
německý	německý	k2eAgInSc1d1	německý
Volkswagen	volkswagen	k1gInSc1	volkswagen
Käfer	Käfra	k1gFnPc2	Käfra
<g/>
.	.	kIx.	.
</s>
