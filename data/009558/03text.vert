<p>
<s>
Alky	alka	k1gFnPc4	alka
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
podčeledi	podčeleď	k1gFnSc2	podčeleď
alek	alka	k1gFnPc2	alka
(	(	kIx(	(
<g/>
Alcinae	Alcinae	k1gFnSc1	Alcinae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čeledi	čeleď	k1gFnSc2	čeleď
rackovití	rackovitý	k2eAgMnPc1d1	rackovitý
(	(	kIx(	(
<g/>
Laridae	Laridae	k1gNnSc7	Laridae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řádu	řád	k1gInSc3	řád
dlouhokřídlých	dlouhokřídlý	k2eAgNnPc6d1	dlouhokřídlý
(	(	kIx(	(
<g/>
Charadriiformes	Charadriiformes	k1gMnSc1	Charadriiformes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
černobílé	černobílý	k2eAgNnSc4d1	černobílé
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
vzpřímený	vzpřímený	k2eAgInSc4d1	vzpřímený
postoj	postoj	k1gInSc4	postoj
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
zvyky	zvyk	k1gInPc4	zvyk
podobné	podobný	k2eAgInPc4d1	podobný
tučňákům	tučňák	k1gMnPc3	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
k	k	k7c3	k
tučňákům	tučňák	k1gMnPc3	tučňák
nemají	mít	k5eNaImIp3nP	mít
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tučňáků	tučňák	k1gMnPc2	tučňák
alky	alka	k1gFnSc2	alka
dovedou	dovést	k5eAaPmIp3nP	dovést
létat	létat	k5eAaImF	létat
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
vyhubené	vyhubený	k2eAgFnSc2d1	vyhubená
alky	alka	k1gFnSc2	alka
velké	velká	k1gFnSc2	velká
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dobří	dobrý	k2eAgMnPc1d1	dobrý
plavci	plavec	k1gMnPc1	plavec
a	a	k8xC	a
potápěči	potápěč	k1gMnPc1	potápěč
(	(	kIx(	(
<g/>
můžou	můžou	k?	můžou
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
hloubky	hloubek	k1gInPc4	hloubek
až	až	k9	až
100	[number]	k4	100
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chodí	chodit	k5eAaImIp3nP	chodit
nemotorně	nemotorně	k6eAd1	nemotorně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
letu	let	k1gInSc6	let
musí	muset	k5eAaImIp3nS	muset
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
mávat	mávat	k5eAaImF	mávat
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
nedostatečné	dostatečný	k2eNgFnSc3d1	nedostatečná
délce	délka	k1gFnSc3	délka
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc4	ocas
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
nohy	noha	k1gFnPc4	noha
jsou	být	k5eAaImIp3nP	být
posunuty	posunout	k5eAaPmNgFnP	posunout
dozadu	dozadu	k6eAd1	dozadu
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
přední	přední	k2eAgInPc4d1	přední
prsty	prst	k1gInPc4	prst
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
plovací	plovací	k2eAgFnSc7d1	plovací
blánou	blána	k1gFnSc7	blána
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
je	být	k5eAaImIp3nS	být
zakrnělý	zakrnělý	k2eAgMnSc1d1	zakrnělý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
plavání	plavání	k1gNnSc3	plavání
používají	používat	k5eAaImIp3nP	používat
křídla	křídlo	k1gNnPc1	křídlo
<g/>
,	,	kIx,	,
nohama	noha	k1gFnPc7	noha
kormidlují	kormidlovat	k5eAaImIp3nP	kormidlovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tlakem	tlak	k1gInSc7	tlak
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
hloubkách	hloubka	k1gFnPc6	hloubka
je	být	k5eAaImIp3nS	být
trup	trup	k1gInSc1	trup
chráněn	chránit	k5eAaImNgInS	chránit
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
prsní	prsní	k2eAgFnSc7d1	prsní
kostí	kost	k1gFnSc7	kost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alky	alka	k1gFnPc1	alka
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
chladných	chladný	k2eAgNnPc6d1	chladné
mořích	moře	k1gNnPc6	moře
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
a	a	k8xC	a
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
zamíří	zamířit	k5eAaPmIp3nS	zamířit
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
na	na	k7c6	na
příkrých	příkrý	k2eAgInPc6d1	příkrý
útesech	útes	k1gInPc6	útes
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
společně	společně	k6eAd1	společně
s	s	k7c7	s
racky	racek	k1gMnPc7	racek
<g/>
,	,	kIx,	,
buřňáky	buřňák	k1gMnPc7	buřňák
a	a	k8xC	a
tereji	terej	k1gMnPc7	terej
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
balvany	balvan	k1gMnPc7	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Snáší	snášet	k5eAaImIp3nS	snášet
jediné	jediný	k2eAgNnSc1d1	jediné
vejce	vejce	k1gNnSc1	vejce
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
skalní	skalní	k2eAgFnPc4d1	skalní
římsy	římsa	k1gFnPc4	římsa
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
kuželovitá	kuželovitý	k2eAgNnPc1d1	kuželovité
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	on	k3xPp3gMnPc4	on
chrání	chránit	k5eAaImIp3nS	chránit
proti	proti	k7c3	proti
skutálení	skutálení	k1gNnSc3	skutálení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vejce	vejce	k1gNnSc4	vejce
i	i	k8xC	i
mládě	mládě	k1gNnSc4	mládě
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
celý	celý	k2eAgInSc1d1	celý
pár	pár	k1gInSc1	pár
<g/>
,	,	kIx,	,
alky	alka	k1gFnPc1	alka
poprvé	poprvé	k6eAd1	poprvé
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
až	až	k9	až
ve	v	k7c6	v
4	[number]	k4	4
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dožívají	dožívat	k5eAaImIp3nP	dožívat
se	se	k3xPyFc4	se
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
odlišná	odlišný	k2eAgNnPc4d1	odlišné
pojmenování	pojmenování	k1gNnPc4	pojmenování
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
alkouni	alkoun	k1gMnPc1	alkoun
rodu	rod	k1gInSc2	rod
Uria	Uria	k1gFnSc1	Uria
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
koloniích	kolonie	k1gFnPc6	kolonie
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
útesů	útes	k1gInPc2	útes
<g/>
.	.	kIx.	.
</s>
<s>
Alkouni	alkoun	k1gMnPc1	alkoun
rodu	rod	k1gInSc2	rod
Cepphus	Cepphus	k1gInSc4	Cepphus
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
skupinách	skupina	k1gFnPc6	skupina
na	na	k7c6	na
skalnatých	skalnatý	k2eAgNnPc6d1	skalnaté
pobřežích	pobřeží	k1gNnPc6	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Systematické	systematický	k2eAgNnSc1d1	systematické
zařazení	zařazení	k1gNnSc1	zařazení
alek	alka	k1gFnPc2	alka
je	být	k5eAaImIp3nS	být
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgFnP	být
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
řádu	řád	k1gInSc2	řád
alky	alka	k1gFnSc2	alka
(	(	kIx(	(
<g/>
Alciformes	Alciformes	k1gInSc1	Alciformes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
podřád	podřád	k1gInSc4	podřád
dlouhokřídlých	dlouhokřídlý	k2eAgFnPc2d1	dlouhokřídlý
<g/>
,	,	kIx,	,
alky	alka	k1gFnPc1	alka
(	(	kIx(	(
<g/>
Alcae	Alca	k1gFnPc1	Alca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
jsou	být	k5eAaImIp3nP	být
zařazeni	zařazen	k2eAgMnPc1d1	zařazen
jako	jako	k8xS	jako
čeleď	čeleď	k1gFnSc1	čeleď
alkovití	alkovitý	k2eAgMnPc1d1	alkovitý
(	(	kIx(	(
<g/>
Alcidae	Alcidae	k1gNnSc7	Alcidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nové	nový	k2eAgFnSc2d1	nová
ptačí	ptačí	k2eAgFnSc2d1	ptačí
taxonomie	taxonomie	k1gFnSc2	taxonomie
jsou	být	k5eAaImIp3nP	být
alky	alka	k1gFnPc1	alka
podčeledí	podčeleď	k1gFnPc2	podčeleď
rackovitých	rackovitý	k2eAgFnPc2d1	rackovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Systematika	systematika	k1gFnSc1	systematika
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
žije	žít	k5eAaImIp3nS	žít
celkem	celkem	k6eAd1	celkem
23	[number]	k4	23
druhů	druh	k1gInPc2	druh
alek	alka	k1gFnPc2	alka
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
rozlišováno	rozlišovat	k5eAaImNgNnS	rozlišovat
22	[number]	k4	22
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
genetické	genetický	k2eAgFnSc2d1	genetická
analýzy	analýza	k1gFnSc2	analýza
oddělen	oddělit	k5eAaPmNgInS	oddělit
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
druh	druh	k1gInSc1	druh
Brachyramphus	Brachyramphus	k1gInSc1	Brachyramphus
perdix	perdix	k1gInSc4	perdix
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
považovaný	považovaný	k2eAgInSc1d1	považovaný
jen	jen	k9	jen
za	za	k7c4	za
asijský	asijský	k2eAgInSc4d1	asijský
poddruh	poddruh	k1gInSc4	poddruh
alkouna	alkoun	k1gMnSc2	alkoun
mramorovaného	mramorovaný	k2eAgMnSc2d1	mramorovaný
(	(	kIx(	(
<g/>
B.	B.	kA	B.
marmoratus	marmoratus	k1gInSc1	marmoratus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Uria	Uria	k1gFnSc1	Uria
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
úzkozobý	úzkozobý	k2eAgMnSc1d1	úzkozobý
(	(	kIx(	(
<g/>
Uria	Uri	k2eAgFnSc1d1	Uria
aalge	aalge	k1gFnSc1	aalge
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
tlustozobý	tlustozobý	k2eAgMnSc1d1	tlustozobý
(	(	kIx(	(
<g/>
Uria	Uri	k2eAgFnSc1d1	Uria
lomvia	lomvia	k1gFnSc1	lomvia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Alca	Alca	k1gFnSc1	Alca
</s>
</p>
<p>
<s>
alka	alka	k1gFnSc1	alka
malá	malý	k2eAgFnSc1d1	malá
(	(	kIx(	(
<g/>
Alca	Alc	k2eAgFnSc1d1	Alca
torda	torda	k1gFnSc1	torda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Cepphus	Cepphus	k1gInSc1	Cepphus
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Cepphus	Cepphus	k1gInSc1	Cepphus
grylle	grylle	k1gFnSc2	grylle
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
brýlatý	brýlatý	k2eAgMnSc1d1	brýlatý
(	(	kIx(	(
<g/>
Cepphus	Cepphus	k1gMnSc1	Cepphus
carbo	carba	k1gFnSc5	carba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
holubí	holubí	k2eAgMnSc1d1	holubí
(	(	kIx(	(
<g/>
Cepphus	Cepphus	k1gMnSc1	Cepphus
columba	columba	k1gMnSc1	columba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Alle	All	k1gFnSc2	All
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
malý	malý	k2eAgMnSc1d1	malý
(	(	kIx(	(
<g/>
Alle	Alle	k1gInSc1	Alle
alle	all	k1gInSc2	all
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Cerorhinca	Cerorhinc	k1gInSc2	Cerorhinc
</s>
</p>
<p>
<s>
papuchalk	papuchalk	k6eAd1	papuchalk
růžkatý	růžkatý	k2eAgInSc1d1	růžkatý
(	(	kIx(	(
<g/>
Cerorhinca	Cerorhinc	k2eAgFnSc1d1	Cerorhinc
monocerata	monocerata	k1gFnSc1	monocerata
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Aethia	Aethium	k1gNnSc2	Aethium
</s>
</p>
<p>
<s>
alkounek	alkounek	k1gMnSc1	alkounek
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
(	(	kIx(	(
<g/>
Aethia	Aethius	k1gMnSc4	Aethius
cristatella	cristatell	k1gMnSc4	cristatell
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkounek	alkounek	k1gMnSc1	alkounek
papouškovitý	papouškovitý	k2eAgMnSc1d1	papouškovitý
(	(	kIx(	(
<g/>
Aethia	Aethius	k1gMnSc4	Aethius
psittacula	psittacul	k1gMnSc4	psittacul
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkounek	alkounek	k1gMnSc1	alkounek
nejmenší	malý	k2eAgMnSc1d3	nejmenší
(	(	kIx(	(
<g/>
Aethia	Aethius	k1gMnSc4	Aethius
pusilla	pusill	k1gMnSc4	pusill
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkounek	alkounek	k1gMnSc1	alkounek
drobný	drobný	k2eAgMnSc1d1	drobný
(	(	kIx(	(
<g/>
Aethia	Aethius	k1gMnSc4	Aethius
pygmaea	pygmaeus	k1gMnSc4	pygmaeus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Brachyramphus	Brachyramphus	k1gInSc1	Brachyramphus
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
krátkozobý	krátkozobý	k2eAgMnSc1d1	krátkozobý
(	(	kIx(	(
<g/>
Brachyramphus	Brachyramphus	k1gInSc1	Brachyramphus
brevirostris	brevirostris	k1gFnSc2	brevirostris
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
mramorovaný	mramorovaný	k2eAgMnSc1d1	mramorovaný
(	(	kIx(	(
<g/>
Brachyramphus	Brachyramphus	k1gMnSc1	Brachyramphus
marmoratus	marmoratus	k1gMnSc1	marmoratus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brachyramphus	Brachyramphus	k1gInSc1	Brachyramphus
perdix	perdix	k1gInSc1	perdix
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Pinguinus	Pinguinus	k1gInSc1	Pinguinus
</s>
</p>
<p>
<s>
alka	alka	k1gFnSc1	alka
velká	velká	k1gFnSc1	velká
(	(	kIx(	(
<g/>
Pinguinus	Pinguinus	k1gInSc1	Pinguinus
impennis	impennis	k1gFnSc2	impennis
<g/>
)	)	kIx)	)
<g/>
†	†	k?	†
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Ptychoramphus	Ptychoramphus	k1gInSc1	Ptychoramphus
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
aleutský	aleutský	k2eAgMnSc1d1	aleutský
(	(	kIx(	(
<g/>
Ptychoramphus	Ptychoramphus	k1gMnSc1	Ptychoramphus
aleuticus	aleuticus	k1gMnSc1	aleuticus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Bracaramphus	Bracaramphus	k1gInSc1	Bracaramphus
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
modrá	modrat	k5eAaImIp3nS	modrat
(	(	kIx(	(
<g/>
Bracaramphus	Bracaramphus	k1gInSc1	Bracaramphus
kasalia	kasalium	k1gNnSc2	kasalium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Synthliboramphus	Synthliboramphus	k1gInSc1	Synthliboramphus
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
černohrdlý	černohrdlý	k2eAgMnSc1d1	černohrdlý
(	(	kIx(	(
<g/>
Synthliboramphus	Synthliboramphus	k1gMnSc1	Synthliboramphus
antiquus	antiquus	k1gMnSc1	antiquus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
japonský	japonský	k2eAgMnSc1d1	japonský
(	(	kIx(	(
<g/>
Synthliboramphus	Synthliboramphus	k1gMnSc1	Synthliboramphus
wumizusume	wumizusum	k1gInSc5	wumizusum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
kalifornský	kalifornský	k2eAgMnSc1d1	kalifornský
(	(	kIx(	(
<g/>
Synthliboramphus	Synthliboramphus	k1gMnSc1	Synthliboramphus
hypoleucus	hypoleucus	k1gMnSc1	hypoleucus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
alkoun	alkoun	k1gMnSc1	alkoun
útesový	útesový	k2eAgMnSc1d1	útesový
(	(	kIx(	(
<g/>
Synthliboramphus	Synthliboramphus	k1gInSc1	Synthliboramphus
craveri	craver	k1gFnSc2	craver
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Fratercula	Fraterculum	k1gNnSc2	Fraterculum
</s>
</p>
<p>
<s>
papuchalk	papuchalk	k6eAd1	papuchalk
chocholatý	chocholatý	k2eAgInSc1d1	chocholatý
(	(	kIx(	(
<g/>
Fratercula	Fratercula	k1gFnSc1	Fratercula
cirrhata	cirrhata	k1gFnSc1	cirrhata
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
papuchalk	papuchalk	k6eAd1	papuchalk
černobradý	černobradý	k2eAgInSc1d1	černobradý
(	(	kIx(	(
<g/>
Fratercula	Fratercula	k1gFnSc1	Fratercula
corniculata	cornicule	k1gNnPc5	cornicule
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
papuchalk	papuchalk	k6eAd1	papuchalk
severní	severní	k2eAgFnSc1d1	severní
(	(	kIx(	(
<g/>
Fratercula	Fratercula	k1gFnSc1	Fratercula
arctica	arctica	k1gFnSc1	arctica
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc2	galerie
alkovití	alkovitý	k2eAgMnPc1d1	alkovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
alkovití	alkovitý	k2eAgMnPc1d1	alkovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
alka	alka	k1gFnSc1	alka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Alky	alka	k1gFnPc1	alka
na	na	k7c4	na
systema	systema	k1gNnSc4	systema
naturae	naturae	k1gNnSc1	naturae
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alkovití	Alkovití	k1gMnPc1	Alkovití
na	na	k7c6	na
BioLibu	BioLib	k1gInSc6	BioLib
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
</s>
</p>
