<s>
Nu	nu	k9	nu
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
též	též	k9	též
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
nü	nü	k?	nü
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
new	new	k?	new
metal	metal	k1gInSc1	metal
nebo	nebo	k8xC	nebo
aggro	aggro	k6eAd1	aggro
metal	metat	k5eAaImAgMnS	metat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podžánr	podžánr	k1gMnSc1	podžánr
alternativního	alternativní	k2eAgInSc2d1	alternativní
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
populární	populární	k2eAgMnSc1d1	populární
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zažíval	zažívat	k5eAaImAgInS	zažívat
svou	svůj	k3xOyFgFnSc4	svůj
největší	veliký	k2eAgFnSc4d3	veliký
slávu	sláva	k1gFnSc4	sláva
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
