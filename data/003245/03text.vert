<s>
Nu	nu	k9	nu
metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
též	též	k9	též
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
nü	nü	k?	nü
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
new	new	k?	new
metal	metal	k1gInSc1	metal
nebo	nebo	k8xC	nebo
aggro	aggro	k6eAd1	aggro
metal	metat	k5eAaImAgMnS	metat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podžánr	podžánr	k1gMnSc1	podžánr
alternativního	alternativní	k2eAgInSc2d1	alternativní
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
populární	populární	k2eAgMnSc1d1	populární
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zažíval	zažívat	k5eAaImAgInS	zažívat
svou	svůj	k3xOyFgFnSc4	svůj
největší	veliký	k2eAgFnSc4d3	veliký
slávu	sláva	k1gFnSc4	sláva
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
kombinováním	kombinování	k1gNnSc7	kombinování
elementů	element	k1gInPc2	element
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
s	s	k7c7	s
elementy	element	k1gInPc7	element
jiných	jiný	k2eAgInPc2d1	jiný
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hip	hip	k0	hip
hopu	hopu	k6eAd1	hopu
<g/>
,	,	kIx,	,
grunge	grunge	k1gNnSc2	grunge
<g/>
,	,	kIx,	,
funku	funk	k1gInSc2	funk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
hardcore	hardcor	k1gMnSc5	hardcor
punku	punk	k1gMnSc5	punk
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Nu	nu	k9	nu
metal	metal	k1gInSc1	metal
převzal	převzít	k5eAaPmAgInS	převzít
také	také	k9	také
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
podžánrů	podžánr	k1gInPc2	podžánr
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
rap	rapa	k1gFnPc2	rapa
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
funk	funk	k1gInSc1	funk
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
thrash	thrasha	k1gFnPc2	thrasha
a	a	k8xC	a
groove	groov	k1gInSc5	groov
metalu	metal	k1gInSc3	metal
<g/>
,	,	kIx,	,
industrial	industrial	k1gInSc4	industrial
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
Nu	nu	k9	nu
metal	metal	k1gInSc1	metal
dává	dávat	k5eAaImIp3nS	dávat
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
metalových	metalový	k2eAgInPc2d1	metalový
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
rytmus	rytmus	k1gInSc4	rytmus
<g/>
,	,	kIx,	,
náladu	nálada	k1gFnSc4	nálada
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
než	než	k8xS	než
na	na	k7c4	na
komplexnost	komplexnost	k1gFnSc4	komplexnost
a	a	k8xC	a
melodii	melodie	k1gFnSc4	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
zkreslených	zkreslený	k2eAgInPc6d1	zkreslený
kytarových	kytarový	k2eAgInPc6d1	kytarový
riffech	riff	k1gInPc6	riff
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
kytaristi	kytaristi	k?	kytaristi
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
podlaďováním	podlaďování	k1gNnSc7	podlaďování
kytar	kytara	k1gFnPc2	kytara
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
pak	pak	k6eAd1	pak
zní	znět	k5eAaImIp3nS	znět
temněji	temně	k6eAd2	temně
a	a	k8xC	a
silněji	silně	k6eAd2	silně
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
kapely	kapela	k1gFnPc1	kapela
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
sedmistrunné	sedmistrunný	k2eAgFnPc1d1	sedmistrunná
(	(	kIx(	(
<g/>
zřídka	zřídka	k6eAd1	zřídka
i	i	k9	i
osmistrunné	osmistrunný	k2eAgFnPc1d1	osmistrunná
<g/>
)	)	kIx)	)
kytary	kytara	k1gFnPc1	kytara
namísto	namísto	k7c2	namísto
klasických	klasický	k2eAgInPc2d1	klasický
šestistrunných	šestistrunný	k2eAgInPc2d1	šestistrunný
<g/>
.	.	kIx.	.
</s>
<s>
Kytarová	kytarový	k2eAgNnPc1d1	kytarové
sóla	sólo	k1gNnPc1	sólo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nu	nu	k9	nu
metalových	metalový	k2eAgFnPc6d1	metalová
písních	píseň	k1gFnPc6	píseň
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Basisti	Basisti	k?	Basisti
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
nu	nu	k9	nu
metalu	metal	k1gInSc6	metal
větší	veliký	k2eAgFnSc4d2	veliký
roli	role	k1gFnSc4	role
než	než	k8xS	než
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
metalových	metalový	k2eAgInPc6d1	metalový
žánrech	žánr	k1gInPc6	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
bubeníky	bubeník	k1gMnPc7	bubeník
dodávají	dodávat	k5eAaImIp3nP	dodávat
písni	píseň	k1gFnSc6	píseň
rytmus	rytmus	k1gInSc4	rytmus
a	a	k8xC	a
tempo	tempo	k1gNnSc4	tempo
<g/>
;	;	kIx,	;
inspirováni	inspirován	k2eAgMnPc1d1	inspirován
hip	hip	k0	hip
hopem	hopem	k?	hopem
a	a	k8xC	a
funkem	funk	k1gInSc7	funk
hrají	hrát	k5eAaImIp3nP	hrát
groovy	groův	k2eAgFnPc1d1	groův
přirozené	přirozený	k2eAgFnPc1d1	přirozená
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
žánry	žánr	k1gInPc4	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
užívají	užívat	k5eAaImIp3nP	užívat
tzv.	tzv.	kA	tzv.
slap	slap	k1gInSc1	slap
techniku	technika	k1gFnSc4	technika
hraní	hraní	k1gNnSc2	hraní
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Pětistrunné	pětistrunný	k2eAgFnPc1d1	pětistrunná
basové	basový	k2eAgFnPc1d1	basová
kytary	kytara	k1gFnPc1	kytara
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
žánr	žánr	k1gInSc4	žánr
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
basisti	basisti	k?	basisti
i	i	k8xC	i
bubeníci	bubeník	k1gMnPc1	bubeník
jsou	být	k5eAaImIp3nP	být
ovlivněni	ovlivněn	k2eAgMnPc1d1	ovlivněn
hip	hip	k0	hip
hopem	hopem	k?	hopem
a	a	k8xC	a
funkem	funk	k1gInSc7	funk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nu	nu	k9	nu
metal	metal	k1gInSc4	metal
není	být	k5eNaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
užití	užití	k1gNnSc1	užití
bicí	bicí	k2eAgFnSc2d1	bicí
soupravy	souprava	k1gFnSc2	souprava
s	s	k7c7	s
dvojitým	dvojitý	k2eAgInSc7d1	dvojitý
basovým	basový	k2eAgInSc7d1	basový
bubnem	buben	k1gInSc7	buben
<g/>
.	.	kIx.	.
</s>
<s>
Kapely	kapela	k1gFnPc1	kapela
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
mívávají	mívávat	k5eAaImIp3nP	mívávat
DJ	DJ	kA	DJ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jim	on	k3xPp3gMnPc3	on
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
hudbu	hudba	k1gFnSc4	hudba
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
samply	sampnout	k5eAaPmAgInP	sampnout
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
dodatečné	dodatečný	k2eAgInPc1d1	dodatečný
syntetické	syntetický	k2eAgInPc1d1	syntetický
zvuky	zvuk	k1gInPc1	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Nu	nu	k9	nu
metaloví	metalový	k2eAgMnPc1d1	metalový
zpěváci	zpěvák	k1gMnPc1	zpěvák
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
písni	píseň	k1gFnSc6	píseň
vystřídat	vystřídat	k5eAaPmF	vystřídat
i	i	k9	i
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
vokálů	vokál	k1gInPc2	vokál
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
běžný	běžný	k2eAgInSc4d1	běžný
zpěv	zpěv	k1gInSc4	zpěv
<g/>
,	,	kIx,	,
rap	rap	k1gMnSc1	rap
(	(	kIx(	(
<g/>
vliv	vliv	k1gInSc1	vliv
hip	hip	k0	hip
hopu	hop	k1gMnSc6	hop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
screaming	screaming	k1gInSc4	screaming
a	a	k8xC	a
growling	growling	k1gInSc4	growling
(	(	kIx(	(
<g/>
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
extrémní	extrémní	k2eAgInPc4d1	extrémní
metalové	metalový	k2eAgInPc4d1	metalový
žánry	žánr	k1gInPc4	žánr
<g/>
,	,	kIx,	,
hardcore	hardcor	k1gInSc5	hardcor
punk	punk	k1gInSc1	punk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
bývají	bývat	k5eAaImIp3nP	bývat
plné	plný	k2eAgInPc1d1	plný
hněvu	hněv	k1gInSc2	hněv
<g/>
,	,	kIx,	,
frustrace	frustrace	k1gFnSc2	frustrace
<g/>
,	,	kIx,	,
úzkosti	úzkost	k1gFnSc2	úzkost
a	a	k8xC	a
bolesti	bolest	k1gFnSc2	bolest
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
vlivem	vliv	k1gInSc7	vliv
grunge	grung	k1gFnSc2	grung
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
osobní	osobní	k2eAgInPc4d1	osobní
prožitky	prožitek	k1gInPc4	prožitek
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
celé	celý	k2eAgFnSc2d1	celá
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
nu	nu	k9	nu
metalové	metalový	k2eAgFnPc4d1	metalová
písně	píseň	k1gFnPc4	píseň
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
intro	intro	k1gNnSc1	intro
-	-	kIx~	-
sloka	sloka	k1gFnSc1	sloka
-	-	kIx~	-
refrén	refrén	k1gInSc1	refrén
-	-	kIx~	-
sloka	sloka	k1gFnSc1	sloka
-	-	kIx~	-
refrén	refrén	k1gInSc1	refrén
-	-	kIx~	-
bridge	bridge	k1gInSc1	bridge
-	-	kIx~	-
refrén	refrén	k1gInSc1	refrén
-	-	kIx~	-
(	(	kIx(	(
<g/>
outro	outro	k6eAd1	outro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholnou	vrcholný	k2eAgFnSc7d1	vrcholná
částí	část	k1gFnSc7	část
písně	píseň	k1gFnSc2	píseň
obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
bridge	bridge	k6eAd1	bridge
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
ve	v	k7c6	v
slokách	sloka	k1gFnPc6	sloka
a	a	k8xC	a
refrénech	refrén	k1gInPc6	refrén
nejčastěji	často	k6eAd3	často
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
střídání	střídání	k1gNnSc3	střídání
rozsahů	rozsah	k1gInPc2	rozsah
vokálů	vokál	k1gInPc2	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Elementy	element	k1gInPc1	element
struktury	struktura	k1gFnSc2	struktura
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
podobnosti	podobnost	k1gFnPc4	podobnost
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
grunge	grung	k1gFnSc2	grung
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
absenci	absence	k1gFnSc3	absence
složitých	složitý	k2eAgNnPc2d1	složité
a	a	k8xC	a
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
kytarových	kytarový	k2eAgNnPc2d1	kytarové
sól	sólo	k1gNnPc2	sólo
a	a	k8xC	a
střednímu	střední	k2eAgNnSc3d1	střední
až	až	k8xS	až
rychlému	rychlý	k2eAgNnSc3d1	rychlé
tempu	tempo	k1gNnSc3	tempo
mívají	mívat	k5eAaImIp3nP	mívat
songy	song	k1gInPc1	song
délku	délka	k1gFnSc4	délka
okolo	okolo	k6eAd1	okolo
3-4	[number]	k4	3-4
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
utvářely	utvářet	k5eAaImAgFnP	utvářet
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
a	a	k8xC	a
dostávaly	dostávat	k5eAaImAgFnP	dostávat
jej	on	k3xPp3gNnSc4	on
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
ostatních	ostatní	k2eAgFnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrce	tvůrce	k1gMnSc1	tvůrce
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
nu	nu	k9	nu
metalu	metal	k1gInSc2	metal
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
KoЯ	KoЯ	k1gFnSc1	KoЯ
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
další	další	k2eAgFnPc4d1	další
skupiny	skupina	k1gFnPc4	skupina
jako	jako	k8xC	jako
Deftones	Deftonesa	k1gFnPc2	Deftonesa
<g/>
,	,	kIx,	,
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Papa	papa	k1gMnSc1	papa
Roach	Roach	k1gMnSc1	Roach
<g/>
,	,	kIx,	,
Static-X	Static-X	k1gMnSc1	Static-X
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
O.D.	O.D.	k1gFnSc2	O.D.
atd	atd	kA	atd
<g/>
;	;	kIx,	;
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
scéně	scéna	k1gFnSc6	scéna
pak	pak	k6eAd1	pak
nejvýrazněji	výrazně	k6eAd3	výrazně
skupina	skupina	k1gFnSc1	skupina
Cocotte	Cocott	k1gMnSc5	Cocott
Minute	Minut	k1gMnSc5	Minut
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
let	let	k1gInSc4	let
po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
začal	začít	k5eAaPmAgInS	začít
nu	nu	k9	nu
metal	metal	k1gInSc1	metal
ustupovat	ustupovat	k5eAaImF	ustupovat
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
jiné	jiný	k2eAgInPc1d1	jiný
žánry	žánr	k1gInPc1	žánr
jako	jako	k8xS	jako
metalcore	metalcor	k1gMnSc5	metalcor
<g/>
.	.	kIx.	.
</s>
<s>
Overhype	Overhyp	k1gInSc5	Overhyp
Adema	Ademum	k1gNnSc2	Ademum
Coal	Coal	k1gMnSc1	Coal
Chamber	Chamber	k1gMnSc1	Chamber
Deftones	Deftones	k1gMnSc1	Deftones
Disturbed	Disturbed	k1gMnSc1	Disturbed
Drowning	Drowning	k1gInSc4	Drowning
Pool	Pool	k1gInSc1	Pool
Dymytry	Dymytr	k1gInPc1	Dymytr
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
svůj	svůj	k3xOyFgInSc4	svůj
metalový	metalový	k2eAgInSc4d1	metalový
projev	projev	k1gInSc4	projev
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
Psy-core	Psyor	k1gInSc5	Psy-cor
<g/>
)	)	kIx)	)
Hazydecay	Hazydecaa	k1gMnSc2	Hazydecaa
Korn	Korn	k1gMnSc1	Korn
Full	Full	k1gMnSc1	Full
Blooded	Blooded	k1gMnSc1	Blooded
Mutt	Mutta	k1gFnPc2	Mutta
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkit	k2eAgInSc1d1	Bizkit
Linkin	Linkin	k2eAgInSc1d1	Linkin
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
starší	starý	k2eAgFnSc1d2	starší
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
)	)	kIx)	)
Lostprophets	Lostprophets	k1gInSc1	Lostprophets
Maximum	maximum	k1gNnSc1	maximum
The	The	k1gFnPc2	The
Hormone	hormon	k1gInSc5	hormon
Mudvayne	Mudvayn	k1gInSc5	Mudvayn
P.	P.	kA	P.
<g/>
O.D.	O.D.	k1gMnSc1	O.D.
Papa	papa	k1gMnSc1	papa
Roach	Roach	k1gMnSc1	Roach
Pleymo	Pleyma	k1gFnSc5	Pleyma
Sepultura	Sepultura	k1gFnSc1	Sepultura
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
<g />
.	.	kIx.	.
</s>
<s>
Roots	Roots	k1gInSc1	Roots
<g/>
)	)	kIx)	)
Sevendust	Sevendust	k1gMnSc1	Sevendust
Soulfly	Soulfly	k1gMnSc1	Soulfly
Spineshank	Spineshank	k1gMnSc1	Spineshank
Staind	Staind	k1gMnSc1	Staind
Static-X	Static-X	k1gMnSc1	Static-X
SlipKnoT	SlipKnoT	k1gMnSc1	SlipKnoT
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
Škwor	Škwor	k1gInSc1	Škwor
Taproot	Taproot	k1gMnSc1	Taproot
Thousand	Thousanda	k1gFnPc2	Thousanda
Foot	Foot	k1gMnSc1	Foot
Krutch	Krutch	k1gMnSc1	Krutch
The	The	k1gMnSc1	The
Slot	slota	k1gFnPc2	slota
Projekt	projekt	k1gInSc1	projekt
Parabelum	parabelum	k1gNnSc2	parabelum
FDK	FDK	kA	FDK
The	The	k1gMnSc1	The
Switch	Switch	k1gMnSc1	Switch
Cocotte	Cocott	k1gInSc5	Cocott
Minute	Minut	k1gInSc5	Minut
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gNnSc1	Down
(	(	kIx(	(
<g/>
Některé	některý	k3yIgInPc1	některý
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
kapela	kapela	k1gFnSc1	kapela
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
nu-metal	nuetat	k5eAaImAgMnS	nu-metat
<g/>
"	"	kIx"	"
nesnáší	snášet	k5eNaImIp3nS	snášet
<g/>
)	)	kIx)	)
Hollywood	Hollywood	k1gInSc1	Hollywood
Undead	Undead	k1gInSc1	Undead
Mushroomhead	Mushroomhead	k1gInSc4	Mushroomhead
</s>
