<s>
Fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
</s>
<s>
Fekální	fekální	k2eAgFnSc1d1
inkontinenceKlasifikace	inkontinenceKlasifikace	k1gFnSc1
MKN-10	MKN-10	k1gFnSc2
</s>
<s>
R	R	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
je	být	k5eAaImIp3nS
neschopnost	neschopnost	k1gFnSc4
kontrolovat	kontrolovat	k5eAaImF
vyprazdňování	vyprazdňování	k1gNnSc4
stolice	stolice	k1gFnSc2
(	(	kIx(
<g/>
defekaci	defekace	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
má	mít	k5eAaImIp3nS
někdo	někdo	k3yInSc1
náhlou	náhlý	k2eAgFnSc4d1
potřebu	potřeba	k1gFnSc4
defekace	defekace	k1gFnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
stát	stát	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
nedokáže	dokázat	k5eNaPmIp3nS
udržet	udržet	k5eAaPmF
výkaly	výkal	k1gInPc4
<g/>
,	,	kIx,
než	než	k8xS
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
toaletě	toaleta	k1gFnSc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
výkaly	výkal	k1gInPc1
mohou	moct	k5eAaImIp3nP
proniknout	proniknout	k5eAaPmF
z	z	k7c2
konečníku	konečník	k1gInSc2
neočekávaně	očekávaně	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
postihuje	postihovat	k5eAaImIp3nS
lidi	člověk	k1gMnPc4
každého	každý	k3xTgInSc2
věku	věk	k1gInSc2
-	-	kIx~
děti	dítě	k1gFnPc4
i	i	k8xC
dospělé	dospělí	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
je	být	k5eAaImIp3nS
víc	hodně	k6eAd2
obvyklá	obvyklý	k2eAgFnSc1d1
u	u	k7c2
žen	žena	k1gFnPc2
než	než	k8xS
u	u	k7c2
mužů	muž	k1gMnPc2
a	a	k8xC
víc	hodně	k6eAd2
obvyklá	obvyklý	k2eAgFnSc1d1
u	u	k7c2
starších	starý	k2eAgFnPc2d2
dospělých	dospělý	k2eAgFnPc2d1
než	než	k8xS
u	u	k7c2
mladších	mladý	k2eAgMnPc2d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
častý	častý	k2eAgInSc1d1
příznak	příznak	k1gInSc1
stárnutí	stárnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Lidé	člověk	k1gMnPc1
s	s	k7c7
fekální	fekální	k2eAgFnSc7d1
inkontinencí	inkontinence	k1gFnSc7
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
cítit	cítit	k5eAaImF
v	v	k7c6
rozpacích	rozpak	k1gInPc6
nebo	nebo	k8xC
poníženi	ponížen	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
nechtějí	chtít	k5eNaImIp3nP
chodit	chodit	k5eAaImF
ven	ven	k6eAd1
z	z	k7c2
obav	obava	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
mohli	moct	k5eAaImAgMnP
mít	mít	k5eAaImF
nehodu	nehoda	k1gFnSc4
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
skrýt	skrýt	k5eAaPmF
tento	tento	k3xDgInSc1
problém	problém	k1gInSc1
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
nejdéle	dlouho	k6eAd3
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
vyhýbají	vyhýbat	k5eAaImIp3nP
přátelům	přítel	k1gMnPc3
a	a	k8xC
rodině	rodina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sociální	sociální	k2eAgFnSc1d1
izolace	izolace	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
redukována	redukovat	k5eAaBmNgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
léčba	léčba	k1gFnSc1
může	moct	k5eAaImIp3nS
zlepšit	zlepšit	k5eAaPmF
kontrolu	kontrola	k1gFnSc4
střev	střevo	k1gNnPc2
a	a	k8xC
inkontinenci	inkontinence	k1gFnSc3
eliminovat	eliminovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
Příčiny	příčina	k1gFnPc4
</s>
<s>
Fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
několik	několik	k4yIc4
příčin	příčina	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
průjem	průjem	k1gInSc1
</s>
<s>
dysfunkce	dysfunkce	k1gFnSc1
pánevního	pánevní	k2eAgNnSc2d1
dna	dno	k1gNnSc2
</s>
<s>
delší	dlouhý	k2eAgFnSc1d2
perioda	perioda	k1gFnSc1
bez	bez	k7c2
defekace	defekace	k1gFnSc2
</s>
<s>
silnou	silný	k2eAgFnSc4d1
svalovou	svalový	k2eAgFnSc4d1
námahu	námaha	k1gFnSc4
v	v	k7c6
břišní	břišní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
během	během	k7c2
porodu	porod	k1gInSc2
</s>
<s>
poškození	poškození	k1gNnSc1
svalů	sval	k1gInPc2
análního	anální	k2eAgInSc2d1
svěrače	svěrač	k1gInSc2
</s>
<s>
poškození	poškození	k1gNnSc1
nervů	nerv	k1gInPc2
svalů	sval	k1gInPc2
análního	anální	k2eAgInSc2d1
svěrače	svěrač	k1gInSc2
nebo	nebo	k8xC
konečníku	konečník	k1gInSc2
</s>
<s>
ztráta	ztráta	k1gFnSc1
skladové	skladový	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
v	v	k7c6
konečníku	konečník	k1gInSc6
<g/>
,	,	kIx,
např.	např.	kA
po	po	k7c6
chirurgické	chirurgický	k2eAgFnSc6d1
operaci	operace	k1gFnSc6
</s>
<s>
nebo	nebo	k8xC
kombinací	kombinace	k1gFnSc7
více	hodně	k6eAd2
příčin	příčina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Svalové	svalový	k2eAgNnSc1d1
poškození	poškození	k1gNnSc1
</s>
<s>
Fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
zapříčiněna	zapříčinit	k5eAaPmNgFnS
úrazem	úraz	k1gInSc7
jednoho	jeden	k4xCgMnSc4
nebo	nebo	k8xC
obou	dva	k4xCgInPc2
kruhovitých	kruhovitý	k2eAgInPc2d1
svalů	sval	k1gInPc2
na	na	k7c6
konci	konec	k1gInSc6
konečníku	konečník	k1gInSc2
zvaných	zvaný	k2eAgInPc2d1
anální	anální	k2eAgInSc4d1
interní	interní	k2eAgInSc4d1
a	a	k8xC
externí	externí	k2eAgInSc4d1
svěrač	svěrač	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svěrače	svěrač	k1gInSc2
udržují	udržovat	k5eAaImIp3nP
stolici	stolice	k1gFnSc4
uvnitř	uvnitř	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
poškodí	poškodit	k5eAaPmIp3nS
<g/>
,	,	kIx,
výkaly	výkal	k1gInPc4
mohou	moct	k5eAaImIp3nP
unikat	unikat	k5eAaImF
ven	ven	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
žen	žena	k1gFnPc2
se	se	k3xPyFc4
poškození	poškození	k1gNnSc1
často	často	k6eAd1
vyskytne	vyskytnout	k5eAaPmIp3nS
během	během	k7c2
porodu	porod	k1gInSc2
<g/>
,	,	kIx,
riziko	riziko	k1gNnSc1
úrazu	úraz	k1gInSc2
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
<g/>
,	,	kIx,
když	když	k8xS
doktor	doktor	k1gMnSc1
používá	používat	k5eAaImIp3nS
lékařské	lékařský	k2eAgFnPc4d1
kleště	kleště	k1gFnPc4
(	(	kIx(
<g/>
forceps	forceps	k6eAd1
<g/>
)	)	kIx)
na	na	k7c4
usnadnění	usnadnění	k1gNnSc4
porodu	porod	k1gInSc2
nebo	nebo	k8xC
provádí	provádět	k5eAaImIp3nS
nástřih	nástřih	k1gInSc1
hráze	hráz	k1gFnSc2
(	(	kIx(
<g/>
nástřih	nástřih	k1gInSc1
ve	v	k7c6
vaginální	vaginální	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
jako	jako	k9
prevence	prevence	k1gFnSc1
protržení	protržení	k1gNnSc2
hráze	hráz	k1gFnSc2
během	během	k7c2
porodu	porod	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
hemoroidů	hemoroidy	k1gInPc2
též	též	k9
může	moct	k5eAaImIp3nS
svěrače	svěrač	k1gInPc4
poškodit	poškodit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Nervové	nervový	k2eAgNnSc1d1
poškození	poškození	k1gNnSc1
</s>
<s>
Fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
způsobena	způsobit	k5eAaPmNgFnS
i	i	k9
poškozením	poškození	k1gNnSc7
nervů	nerv	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
kontrolují	kontrolovat	k5eAaImIp3nP
anální	anální	k2eAgInPc1d1
svěrače	svěrač	k1gInPc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
nervů	nerv	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
kontrolují	kontrolovat	k5eAaImIp3nP
výkaly	výkal	k1gInPc4
v	v	k7c6
konečníku	konečník	k1gInSc6
(	(	kIx(
<g/>
senzorické	senzorický	k2eAgInPc1d1
nervy	nerv	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poškodí	poškodit	k5eAaPmIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
nervy	nerv	k1gInPc1
kontrolující	kontrolující	k2eAgInPc1d1
svěrače	svěrač	k1gInPc1
<g/>
,	,	kIx,
sval	sval	k1gInSc1
nepracuje	pracovat	k5eNaImIp3nS
správně	správně	k6eAd1
a	a	k8xC
může	moct	k5eAaImIp3nS
nastat	nastat	k5eAaPmF
inkontinence	inkontinence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
poškozené	poškozený	k2eAgInPc4d1
senzorické	senzorický	k2eAgInPc4d1
nervy	nerv	k1gInPc4
<g/>
,	,	kIx,
osoba	osoba	k1gFnSc1
necítí	cítit	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
výkaly	výkal	k1gInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
konečníku	konečník	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osoba	osoba	k1gFnSc1
pak	pak	k6eAd1
necítí	cítit	k5eNaImIp3nS
potřebu	potřeba	k1gFnSc4
použít	použít	k5eAaPmF
toaletu	toaleta	k1gFnSc4
<g/>
,	,	kIx,
až	až	k9
když	když	k8xS
výkaly	výkal	k1gInPc1
uniknou	uniknout	k5eAaPmIp3nP
ven	ven	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nervové	nervový	k2eAgNnSc1d1
poškození	poškození	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
způsobeno	způsobit	k5eAaPmNgNnS
porodem	porod	k1gInSc7
<g/>
,	,	kIx,
dlouhodobým	dlouhodobý	k2eAgInSc7d1
zvykem	zvyk	k1gInSc7
namáhavě	namáhavě	k6eAd1
protlačovat	protlačovat	k5eAaImF
výkaly	výkal	k1gInPc1
<g/>
,	,	kIx,
mozkovou	mozkový	k2eAgFnSc7d1
mrtvicí	mrtvice	k1gFnSc7
a	a	k8xC
nemocemi	nemoc	k1gFnPc7
postihujícími	postihující	k2eAgInPc7d1
nervy	nerv	k1gInPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
diabetes	diabetes	k1gInSc1
mellitus	mellitus	k1gInSc1
(	(	kIx(
<g/>
cukrovka	cukrovka	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
skleróza	skleróza	k1gFnSc1
multiplex	multiplex	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Snížení	snížení	k1gNnSc1
skladovací	skladovací	k2eAgFnSc2d1
kapacity	kapacita	k1gFnSc2
konečníku	konečník	k1gInSc2
</s>
<s>
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
konečník	konečník	k1gInSc1
pro	pro	k7c4
udržení	udržení	k1gNnSc4
stolice	stolice	k1gFnSc2
napíná	napínat	k5eAaImIp3nS
tak	tak	k6eAd1
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
dokud	dokud	k8xS
osoba	osoba	k1gFnSc1
nedojde	dojít	k5eNaPmIp3nS
na	na	k7c4
toaletu	toaleta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
konečníku	konečník	k1gInSc2
<g/>
,	,	kIx,
ozařovací	ozařovací	k2eAgFnSc1d1
léčba	léčba	k1gFnSc1
nebo	nebo	k8xC
zánětlivá	zánětlivý	k2eAgNnPc1d1
střevní	střevní	k2eAgNnPc1d1
onemocnění	onemocnění	k1gNnPc1
však	však	k9
mohou	moct	k5eAaImIp3nP
způsobit	způsobit	k5eAaPmF
zjizvení	zjizvení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
stěny	stěna	k1gFnSc2
konečníku	konečník	k1gInSc2
ztrácí	ztrácet	k5eAaImIp3nS
pružnost	pružnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečník	konečník	k1gInSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
nemůže	moct	k5eNaImIp3nS
tolik	tolik	k6eAd1
napínat	napínat	k5eAaImF
a	a	k8xC
nemůže	moct	k5eNaImIp3nS
udržet	udržet	k5eAaPmF
stolici	stolice	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
fekální	fekální	k2eAgFnSc3d1
inkontinenci	inkontinence	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stěny	stěna	k1gFnPc4
konečníku	konečník	k1gInSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
také	také	k9
podrážděny	podráždit	k5eAaPmNgFnP
zánětlivým	zánětlivý	k2eAgNnSc7d1
střevním	střevní	k2eAgNnSc7d1
onemocněním	onemocnění	k1gNnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
také	také	k9
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
neschopnost	neschopnost	k1gFnSc1
udržet	udržet	k5eAaPmF
stolici	stolice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Průjem	průjem	k1gInSc1
</s>
<s>
Průjem	průjem	k1gInSc1
nebo	nebo	k8xC
řídké	řídký	k2eAgInPc1d1
výkaly	výkal	k1gInPc1
se	se	k3xPyFc4
méně	málo	k6eAd2
snadno	snadno	k6eAd1
kontrolují	kontrolovat	k5eAaImIp3nP
než	než	k8xS
tuhé	tuhý	k2eAgInPc1d1
výkaly	výkal	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
lidé	člověk	k1gMnPc1
bez	bez	k7c2
fekální	fekální	k2eAgFnSc2d1
inkontinence	inkontinence	k1gFnSc2
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
nehodu	nehoda	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
mají	mít	k5eAaImIp3nP
průjem	průjem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dysfunkce	dysfunkce	k1gFnSc1
pánevního	pánevní	k2eAgNnSc2d1
dna	dno	k1gNnSc2
</s>
<s>
Abnormality	abnormalita	k1gFnPc1
pánevního	pánevní	k2eAgNnSc2d1
dna	dno	k1gNnSc2
mohou	moct	k5eAaImIp3nP
vést	vést	k5eAaImF
k	k	k7c3
fekální	fekální	k2eAgFnSc3d1
inkontinenci	inkontinence	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příklady	příklad	k1gInPc1
některých	některý	k3yIgFnPc2
abnormalit	abnormalita	k1gFnPc2
zahrnují	zahrnovat	k5eAaImIp3nP
snížené	snížený	k2eAgNnSc1d1
vnímání	vnímání	k1gNnSc1
konečníkových	konečníkový	k2eAgInPc2d1
vjemů	vjem	k1gInPc2
<g/>
,	,	kIx,
snížené	snížený	k2eAgInPc1d1
tlaky	tlak	k1gInPc1
análního	anální	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
<g/>
,	,	kIx,
snížený	snížený	k2eAgInSc4d1
svíravý	svíravý	k2eAgInSc4d1
tlak	tlak	k1gInSc4
análního	anální	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
<g/>
,	,	kIx,
narušené	narušený	k2eAgNnSc1d1
anální	anální	k2eAgNnSc1d1
vnímání	vnímání	k1gNnSc1
<g/>
,	,	kIx,
snížení	snížení	k1gNnSc1
konečníku	konečník	k1gInSc2
(	(	kIx(
<g/>
konečníkový	konečníkový	k2eAgInSc1d1
prolaps	prolaps	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vysunutí	vysunutí	k1gNnSc1
konečníku	konečník	k1gInSc2
přes	přes	k7c4
vaginu	vagina	k1gFnSc4
(	(	kIx(
<g/>
rektokéla	rektokéla	k6eAd1
–	–	k?
výhřez	výhřez	k1gInSc4
konečníku	konečník	k1gInSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
obecnou	obecný	k2eAgFnSc4d1
slabost	slabost	k1gFnSc4
a	a	k8xC
pokles	pokles	k1gInSc4
pánevního	pánevní	k2eAgNnSc2d1
dna	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
je	být	k5eAaImIp3nS
příčinou	příčina	k1gFnSc7
dysfunkce	dysfunkce	k1gFnSc2
pánevního	pánevní	k2eAgNnSc2d1
dna	dno	k1gNnSc2
porod	porod	k1gInSc1
a	a	k8xC
inkontinence	inkontinence	k1gFnSc1
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
kolem	kolem	k7c2
40	#num#	k4
let	léto	k1gNnPc2
nebo	nebo	k8xC
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Diagnostika	diagnostika	k1gFnSc1
</s>
<s>
Doktor	doktor	k1gMnSc1
provede	provést	k5eAaPmIp3nS
zdravotní	zdravotní	k2eAgInPc4d1
dotazy	dotaz	k1gInPc4
<g/>
,	,	kIx,
fyzická	fyzický	k2eAgNnPc4d1
vyšetření	vyšetření	k1gNnPc4
a	a	k8xC
případně	případně	k6eAd1
jiné	jiný	k2eAgInPc4d1
medicínské	medicínský	k2eAgInPc4d1
testy	test	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Anální	anální	k2eAgFnSc1d1
manometrie	manometrie	k1gFnSc1
kontroluje	kontrolovat	k5eAaImIp3nS
pevnost	pevnost	k1gFnSc4
análního	anální	k2eAgInSc2d1
svěrače	svěrač	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
schopnost	schopnost	k1gFnSc4
reagovat	reagovat	k5eAaBmF
na	na	k7c4
signály	signál	k1gInPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
citlivost	citlivost	k1gFnSc4
a	a	k8xC
funkci	funkce	k1gFnSc4
konečníku	konečník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Anorektální	Anorektální	k2eAgFnSc1d1
ultrasonografie	ultrasonografie	k1gFnSc1
hodnotí	hodnotit	k5eAaImIp3nS
strukturu	struktura	k1gFnSc4
análního	anální	k2eAgInSc2d1
svěrače	svěrač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Proktografie	Proktografie	k1gFnPc1
<g/>
,	,	kIx,
též	též	k9
zvaná	zvaný	k2eAgFnSc1d1
defekografie	defekografie	k1gFnSc1
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
kolik	kolik	k4yRc4,k4yQc4,k4yIc4
výkalů	výkal	k1gInPc2
může	moct	k5eAaImIp3nS
konečník	konečník	k1gInSc1
držet	držet	k5eAaImF
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
dobře	dobře	k6eAd1
je	být	k5eAaImIp3nS
konečník	konečník	k1gInSc1
může	moct	k5eAaImIp3nS
držet	držet	k5eAaImF
a	a	k8xC
jak	jak	k6eAd1
dobře	dobře	k6eAd1
konečník	konečník	k1gInSc1
může	moct	k5eAaImIp3nS
výkaly	výkal	k1gInPc4
odvést	odvést	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Proktosigmoidoskopie	Proktosigmoidoskopie	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
doktorům	doktor	k1gMnPc3
kontrolovat	kontrolovat	k5eAaImF
vnitřek	vnitřek	k1gInSc4
konečníku	konečník	k1gInSc2
pro	pro	k7c4
příznaky	příznak	k1gInPc4
nemoci	nemoc	k1gFnSc2
nebo	nebo	k8xC
jiné	jiný	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mohou	moct	k5eAaImIp3nP
způsobit	způsobit	k5eAaPmF
fekální	fekální	k2eAgFnSc4d1
inkontinenci	inkontinence	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
zápal	zápal	k1gInSc1
<g/>
,	,	kIx,
nádory	nádor	k1gInPc1
nebo	nebo	k8xC
zjizvená	zjizvený	k2eAgFnSc1d1
tkáň	tkáň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Anální	anální	k2eAgFnSc1d1
elektromyografie	elektromyografie	k1gFnSc1
testuje	testovat	k5eAaImIp3nS
nervová	nervový	k2eAgNnPc1d1
poškození	poškození	k1gNnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
spojeny	spojit	k5eAaPmNgInP
s	s	k7c7
úrazem	úraz	k1gInSc7
u	u	k7c2
porodu	porod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Léčba	léčba	k1gFnSc1
</s>
<s>
Léčba	léčba	k1gFnSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
příčně	příčna	k1gFnSc6
a	a	k8xC
závažnosti	závažnost	k1gFnSc2
fekální	fekální	k2eAgFnSc2d1
inkontinence	inkontinence	k1gFnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
dietní	dietní	k2eAgFnPc4d1
změny	změna	k1gFnPc4
<g/>
,	,	kIx,
léky	lék	k1gInPc4
<g/>
,	,	kIx,
střevní	střevní	k2eAgInSc4d1
trénink	trénink	k1gInSc4
nebo	nebo	k8xC
operaci	operace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víc	hodně	k6eAd2
než	než	k8xS
jeden	jeden	k4xCgInSc1
typ	typ	k1gInSc1
léčby	léčba	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nutný	nutný	k2eAgMnSc1d1
pro	pro	k7c4
úspěšnou	úspěšný	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
defekace	defekace	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
schopnost	schopnost	k1gFnSc1
kontroly	kontrola	k1gFnSc2
je	být	k5eAaImIp3nS
komplexní	komplexní	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dietní	dietní	k2eAgFnPc1d1
změny	změna	k1gFnPc1
</s>
<s>
Potrava	potrava	k1gFnSc1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
konzistenci	konzistence	k1gFnSc4
(	(	kIx(
<g/>
hustotu	hustota	k1gFnSc4
<g/>
)	)	kIx)
výkalů	výkal	k1gInPc2
a	a	k8xC
rychlost	rychlost	k1gFnSc1
průchodu	průchod	k1gInSc2
zažívacím	zažívací	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
možností	možnost	k1gFnPc2
pomoci	pomoc	k1gFnSc2
kontrolovat	kontrolovat	k5eAaImF
fekální	fekální	k2eAgFnSc4d1
inkontinenci	inkontinence	k1gFnSc4
u	u	k7c2
některých	některý	k3yIgFnPc2
osob	osoba	k1gFnPc2
je	být	k5eAaImIp3nS
konzumovat	konzumovat	k5eAaBmF
potraviny	potravina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
přidávají	přidávat	k5eAaImIp3nP
výkalům	výkal	k1gInPc3
objem	objem	k1gInSc4
<g/>
,	,	kIx,
dělají	dělat	k5eAaImIp3nP
je	on	k3xPp3gInPc4
méně	málo	k6eAd2
vodnatými	vodnatý	k2eAgFnPc7d1
a	a	k8xC
snadněji	snadno	k6eAd2
kontrolovatelnými	kontrolovatelný	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Též	též	k6eAd1
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
vyhnout	vyhnout	k5eAaPmF
se	se	k3xPyFc4
potravinám	potravina	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
přispívají	přispívat	k5eAaImIp3nP
k	k	k7c3
problému	problém	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
potraviny	potravina	k1gFnPc4
a	a	k8xC
nápoje	nápoj	k1gInPc4
obsahující	obsahující	k2eAgInSc4d1
kofein	kofein	k1gInSc4
jako	jako	k8xC,k8xS
káva	káva	k1gFnSc1
<g/>
,	,	kIx,
čaj	čaj	k1gInSc1
a	a	k8xC
čokoláda	čokoláda	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
uvolňují	uvolňovat	k5eAaImIp3nP
interní	interní	k2eAgInSc4d1
sval	sval	k1gInSc4
análního	anální	k2eAgInSc2d1
svěrače	svěrač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
přístup	přístup	k1gInSc1
je	být	k5eAaImIp3nS
konzumovat	konzumovat	k5eAaBmF
potraviny	potravina	k1gFnPc4
s	s	k7c7
malým	malý	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
vlákniny	vláknina	k1gFnSc2
pro	pro	k7c4
zmenšení	zmenšení	k1gNnSc4
práce	práce	k1gFnSc2
análních	anální	k2eAgInPc2d1
svěračů	svěrač	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovoce	ovoce	k1gNnSc1
může	moct	k5eAaImIp3nS
působit	působit	k5eAaImF
jako	jako	k9
přírodní	přírodní	k2eAgNnSc4d1
laxativum	laxativum	k1gNnSc4
(	(	kIx(
<g/>
projímadlo	projímadlo	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
konzumováno	konzumovat	k5eAaBmNgNnS
šetrně	šetrně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Možné	možný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
upravit	upravit	k5eAaPmF
množství	množství	k1gNnSc4
a	a	k8xC
složení	složení	k1gNnSc4
potravy	potrava	k1gFnSc2
na	na	k7c4
pomoc	pomoc	k1gFnSc4
kontroly	kontrola	k1gFnSc2
fekální	fekální	k2eAgFnSc2d1
inkontinence	inkontinence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pište	psát	k5eAaImRp2nP
si	se	k3xPyFc3
potravinový	potravinový	k2eAgInSc4d1
deník	deník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapisujte	zapisovat	k5eAaImRp2nP
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
a	a	k8xC
kolik	kolik	k9
konzumujete	konzumovat	k5eAaBmIp2nP
a	a	k8xC
případy	případ	k1gInPc4
inkontinence	inkontinence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
několika	několik	k4yIc6
dnech	den	k1gInPc6
můžete	moct	k5eAaImIp2nP
pozorovat	pozorovat	k5eAaImF
souvislosti	souvislost	k1gFnPc4
mezi	mezi	k7c7
konzumací	konzumace	k1gFnSc7
některých	některý	k3yIgFnPc2
potravin	potravina	k1gFnPc2
a	a	k8xC
inkontinencí	inkontinence	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
identifikaci	identifikace	k1gFnSc6
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
asi	asi	k9
způsobují	způsobovat	k5eAaImIp3nP
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
vynechte	vynecht	k1gInSc5
je	on	k3xPp3gMnPc4
a	a	k8xC
sledujte	sledovat	k5eAaImRp2nP
zda	zda	k8xS
se	se	k3xPyFc4
inkontinence	inkontinence	k1gFnSc1
zlepší	zlepšit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potraviny	potravina	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
často	často	k6eAd1
způsobují	způsobovat	k5eAaImIp3nP
průjem	průjem	k1gInSc4
a	a	k8xC
proto	proto	k8xC
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
vynechány	vynechat	k5eAaPmNgInP
<g/>
,	,	kIx,
obsahují	obsahovat	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
kofein	kofein	k1gInSc1
</s>
<s>
upravované	upravovaný	k2eAgNnSc4d1
nebo	nebo	k8xC
uzené	uzený	k2eAgNnSc4d1
maso	maso	k1gNnSc4
jako	jako	k8xS,k8xC
např.	např.	kA
klobása	klobása	k1gFnSc1
<g/>
,	,	kIx,
šunka	šunka	k1gFnSc1
nebo	nebo	k8xC
kachna	kachna	k1gFnSc1
</s>
<s>
kořeněné	kořeněný	k2eAgFnPc1d1
potraviny	potravina	k1gFnPc1
</s>
<s>
alkohol	alkohol	k1gInSc1
</s>
<s>
mléčné	mléčný	k2eAgInPc1d1
produkty	produkt	k1gInPc1
jako	jako	k8xC,k8xS
mléko	mléko	k1gNnSc1
<g/>
,	,	kIx,
sýr	sýr	k1gInSc1
a	a	k8xC
zmrzlina	zmrzlina	k1gFnSc1
</s>
<s>
ovoce	ovoce	k1gNnPc1
jako	jako	k8xS,k8xC
jablka	jablko	k1gNnPc1
<g/>
,	,	kIx,
broskve	broskev	k1gFnPc1
nebo	nebo	k8xC
hrušky	hruška	k1gFnPc1
</s>
<s>
tukové	tukový	k2eAgFnPc1d1
a	a	k8xC
mastné	mastný	k2eAgFnPc1d1
potraviny	potravina	k1gFnPc1
</s>
<s>
sladidla	sladidlo	k1gNnPc1
<g/>
,	,	kIx,
např.	např.	kA
sorbitol	sorbitol	k1gInSc1
<g/>
,	,	kIx,
xylitol	xylitol	k1gInSc1
<g/>
,	,	kIx,
mannitol	mannitol	k1gInSc1
<g/>
,	,	kIx,
aspartam	aspartam	k1gInSc1
a	a	k8xC
fruktóza	fruktóza	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
dietních	dietní	k2eAgInPc6d1
nápojích	nápoj	k1gInPc6
<g/>
,	,	kIx,
žvýkačkách	žvýkačka	k1gFnPc6
bez	bez	k7c2
cukru	cukr	k1gInSc2
<g/>
,	,	kIx,
kandizovaném	kandizovaný	k2eAgInSc6d1
cukru	cukr	k1gInSc6
<g/>
,	,	kIx,
čokoládě	čokoláda	k1gFnSc6
a	a	k8xC
ovocných	ovocný	k2eAgInPc6d1
džusech	džus	k1gInPc6
</s>
<s>
Konzumujte	konzumovat	k5eAaBmRp2nP
menší	malý	k2eAgFnSc4d2
porce	porce	k1gFnPc4
a	a	k8xC
častěji	často	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některým	některý	k3yIgMnPc3
lidem	člověk	k1gMnPc3
velké	velký	k2eAgFnSc2d1
porce	porce	k1gFnSc2
způsobují	způsobovat	k5eAaImIp3nP
střevní	střevní	k2eAgFnPc1d1
kontrakce	kontrakce	k1gFnPc1
(	(	kIx(
<g/>
stahy	stah	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vedou	vést	k5eAaImIp3nP
k	k	k7c3
průjmu	průjem	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stále	stále	k6eAd1
můžete	moct	k5eAaImIp2nP
konzumovat	konzumovat	k5eAaBmF
stejné	stejný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
potravy	potrava	k1gFnSc2
denně	denně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
rozdělte	rozdělit	k5eAaPmRp2nP
příjem	příjem	k1gInSc4
do	do	k7c2
víc	hodně	k6eAd2
časových	časový	k2eAgInPc2d1
intervalů	interval	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Konzumujte	konzumovat	k5eAaBmRp2nP
potravu	potrava	k1gFnSc4
a	a	k8xC
nápoje	nápoj	k1gInPc4
odděleně	odděleně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapaliny	kapalina	k1gFnPc1
pomáhají	pomáhat	k5eAaImIp3nP
posouvat	posouvat	k5eAaImF
potravu	potrava	k1gFnSc4
zažívacím	zažívací	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
když	když	k8xS
chcete	chtít	k5eAaImIp2nP
pohyb	pohyb	k1gInSc4
zpomalit	zpomalit	k5eAaPmF
<g/>
,	,	kIx,
pijte	pít	k5eAaImRp2nP
asi	asi	k9
30	#num#	k4
minut	minuta	k1gFnPc2
před	před	k7c7
nebo	nebo	k8xC
po	po	k7c6
stravě	strava	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
s	s	k7c7
potravou	potrava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Konzumujte	konzumovat	k5eAaBmRp2nP
víc	hodně	k6eAd2
vlákniny	vláknina	k1gFnPc4
<g/>
,	,	kIx,
dělají	dělat	k5eAaImIp3nP
výkaly	výkal	k1gInPc4
jemnými	jemný	k2eAgInPc7d1
<g/>
,	,	kIx,
formovanými	formovaný	k2eAgInPc7d1
a	a	k8xC
kontrolovatelnějšími	kontrolovatelný	k2eAgInPc7d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlákninu	vláknina	k1gFnSc4
obsahuje	obsahovat	k5eAaImIp3nS
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
zelenina	zelenina	k1gFnSc1
a	a	k8xC
zrna	zrno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potřebujete	potřebovat	k5eAaImIp2nP
konzumovat	konzumovat	k5eAaBmF
20	#num#	k4
-	-	kIx~
30	#num#	k4
gramů	gram	k1gInPc2
vlákniny	vláknina	k1gFnSc2
denně	denně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
přidejte	přidat	k5eAaPmRp2nP
je	on	k3xPp3gFnPc4
k	k	k7c3
vaší	váš	k3xOp2gFnSc3
dietě	dieta	k1gFnSc3
pomalu	pomalu	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
tělo	tělo	k1gNnSc1
mohlo	moct	k5eAaImAgNnS
přizpůsobit	přizpůsobit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příliš	příliš	k6eAd1
mnoho	mnoho	k6eAd1
vlákniny	vláknina	k1gFnSc2
najednou	najednou	k6eAd1
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
nadýmání	nadýmání	k1gNnSc4
nebo	nebo	k8xC
i	i	k9
průjem	průjem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Též	též	k6eAd1
mnoho	mnoho	k6eAd1
nerozpustné	rozpustný	k2eNgFnPc4d1
nebo	nebo	k8xC
nestravitelné	stravitelný	k2eNgFnPc4d1
vlákniny	vláknina	k1gFnPc4
může	moct	k5eAaImIp3nS
přispět	přispět	k5eAaPmF
k	k	k7c3
průjmu	průjem	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
shledáte	shledat	k5eAaPmIp2nP
<g/>
-li	-li	k?
<g/>
,	,	kIx,
že	že	k8xS
konzumace	konzumace	k1gFnSc1
víc	hodně	k6eAd2
vlákniny	vláknina	k1gFnSc2
může	moct	k5eAaImIp3nS
váš	váš	k3xOp2gInSc1
průjem	průjem	k1gInSc1
zhoršit	zhoršit	k5eAaPmF
<g/>
,	,	kIx,
vraťte	vrátit	k5eAaPmRp2nP
se	se	k3xPyFc4
k	k	k7c3
2	#num#	k4
porcím	porce	k1gFnPc3
z	z	k7c2
každého	každý	k3xTgNnSc2
ovoce	ovoce	k1gNnSc2
a	a	k8xC
zeleniny	zelenina	k1gFnSc2
a	a	k8xC
vynechte	vynecht	k1gInSc5
slupky	slupka	k1gFnPc1
a	a	k8xC
semena	semeno	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Konzumujte	konzumovat	k5eAaBmRp2nP
potraviny	potravina	k1gFnPc4
dělající	dělající	k2eAgInSc4d1
výkaly	výkal	k1gInPc7
většími	veliký	k2eAgInPc7d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potraviny	potravina	k1gFnPc1
obsahující	obsahující	k2eAgFnSc4d1
rozpustnou	rozpustný	k2eAgFnSc4d1
nebo	nebo	k8xC
stravitelnou	stravitelný	k2eAgFnSc4d1
vlákninu	vláknina	k1gFnSc4
zpomalují	zpomalovat	k5eAaImIp3nP
vyprazdňování	vyprazdňování	k1gNnSc4
střev	střevo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
jsou	být	k5eAaImIp3nP
banány	banán	k1gInPc1
<g/>
,	,	kIx,
rýže	rýže	k1gFnPc1
<g/>
,	,	kIx,
chléb	chléb	k1gInSc1
<g/>
,	,	kIx,
brambory	brambora	k1gFnPc1
<g/>
,	,	kIx,
sýr	sýr	k1gInSc1
<g/>
,	,	kIx,
jemné	jemný	k2eAgNnSc1d1
arašídové	arašídový	k2eAgNnSc1d1
máslo	máslo	k1gNnSc1
<g/>
,	,	kIx,
jogurt	jogurt	k1gInSc1
<g/>
,	,	kIx,
těstoviny	těstovina	k1gFnPc1
a	a	k8xC
ovesné	ovesný	k2eAgInPc1d1
vločky	vloček	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Pijte	pít	k5eAaImRp2nP
mnoho	mnoho	k4c4
tekutin	tekutina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučeno	doporučen	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
pít	pít	k5eAaImF
1,5	1,5	k4
-	-	kIx~
2	#num#	k4
litry	litr	k1gInPc4
denně	denně	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
horku	horko	k1gNnSc6
i	i	k9
3	#num#	k4
litry	litr	k1gInPc4
denně	denně	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
předchází	předcházet	k5eAaImIp3nS
dehydrataci	dehydratace	k1gFnSc4
a	a	k8xC
udržuje	udržovat	k5eAaImIp3nS
výkaly	výkal	k1gInPc4
jemné	jemný	k2eAgInPc4d1
a	a	k8xC
formované	formovaný	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
je	být	k5eAaImIp3nS
dobrá	dobrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
vyhněte	vyhníst	k5eAaPmIp3nS
se	se	k3xPyFc4
nápojům	nápoj	k1gInPc3
s	s	k7c7
kofeinem	kofein	k1gInSc7
<g/>
,	,	kIx,
alkoholem	alkohol	k1gInSc7
<g/>
,	,	kIx,
mlékem	mléko	k1gNnSc7
nebo	nebo	k8xC
syceným	sycený	k2eAgInSc7d1
oxidem	oxid	k1gInSc7
uhličitým	uhličitý	k2eAgInSc7d1
(	(	kIx(
<g/>
CO	co	k9
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
shledáte	shledat	k5eAaPmIp2nP
<g/>
-li	-li	k?
<g/>
,	,	kIx,
že	že	k8xS
způsobují	způsobovat	k5eAaImIp3nP
průjem	průjem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Časem	čas	k1gInSc7
může	moct	k5eAaImIp3nS
průjem	průjem	k1gInSc1
způsobit	způsobit	k5eAaPmF
nedostatek	nedostatek	k1gInSc4
vitaminů	vitamin	k1gInPc2
a	a	k8xC
minerálů	minerál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeptejte	zeptat	k5eAaPmRp2nP
se	se	k3xPyFc4
doktora	doktor	k1gMnSc4
<g/>
,	,	kIx,
potřebujete	potřebovat	k5eAaImIp2nP
<g/>
-li	-li	k?
vitaminové	vitaminový	k2eAgInPc4d1
doplňky	doplněk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Léky	lék	k1gInPc1
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS
<g/>
-li	-li	k?
průjem	průjem	k1gInSc1
inkontinenci	inkontinence	k1gFnSc3
<g/>
,	,	kIx,
léky	lék	k1gInPc1
mohou	moct	k5eAaImIp3nP
pomoct	pomoct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
doktoři	doktor	k1gMnPc1
doporučují	doporučovat	k5eAaImIp3nP
použití	použití	k1gNnSc4
objemových	objemový	k2eAgNnPc2d1
laxativ	laxativum	k1gNnPc2
(	(	kIx(
<g/>
projímadel	projímadlo	k1gNnPc2
<g/>
)	)	kIx)
na	na	k7c4
pomoc	pomoc	k1gFnSc4
vývoje	vývoj	k1gInSc2
pravidelnějších	pravidelní	k2eAgFnPc2d2
střevních	střevní	k2eAgFnPc2d1
vzorek	vzorek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebo	nebo	k8xC
doktor	doktor	k1gMnSc1
může	moct	k5eAaImIp3nS
předepsat	předepsat	k5eAaPmF
léky	lék	k1gInPc4
proti	proti	k7c3
průjmu	průjem	k1gInSc3
<g/>
,	,	kIx,
např.	např.	kA
Loperamid	Loperamid	k1gInSc1
nebo	nebo	k8xC
Difenoxylát	Difenoxylát	k1gInSc1
na	na	k7c4
zpomalení	zpomalení	k1gNnSc4
střev	střevo	k1gNnPc2
a	a	k8xC
pomoci	pomoc	k1gFnSc2
kontrolovat	kontrolovat	k5eAaImF
problém	problém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Střevní	střevní	k2eAgInSc1d1
trénink	trénink	k1gInSc1
</s>
<s>
Střevní	střevní	k2eAgInSc1d1
trénink	trénink	k1gInSc1
pomáhá	pomáhat	k5eAaImIp3nS
některým	některý	k3yIgMnPc3
lidem	člověk	k1gMnPc3
naučit	naučit	k5eAaPmF
se	se	k3xPyFc4
znovu	znovu	k6eAd1
kontrolovat	kontrolovat	k5eAaImF
svá	svůj	k3xOyFgNnPc4
střeva	střevo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
to	ten	k3xDgNnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
posílení	posílení	k1gNnSc4
svalů	sval	k1gInPc2
<g/>
,	,	kIx,
jindy	jindy	k6eAd1
to	ten	k3xDgNnSc1
značí	značit	k5eAaImIp3nS
trénink	trénink	k1gInSc1
střeva	střevo	k1gNnSc2
na	na	k7c4
vyprázdnění	vyprázdnění	k1gNnSc4
ve	v	k7c4
specifický	specifický	k2eAgInSc4d1
čas	čas	k1gInSc4
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Použijte	použít	k5eAaPmRp2nP
biofeedback	biofeedback	k6eAd1
(	(	kIx(
<g/>
biologickou	biologický	k2eAgFnSc4d1
zpětnou	zpětný	k2eAgFnSc4d1
vazbu	vazba	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biofeedback	Biofeedback	k1gInSc1
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc4
na	na	k7c4
posílení	posílení	k1gNnSc4
a	a	k8xC
koordinaci	koordinace	k1gFnSc4
svalů	sval	k1gInPc2
a	a	k8xC
některým	některý	k3yIgMnPc3
lidem	člověk	k1gMnPc3
pomůže	pomoct	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgInSc4d1
vybavení	vybavení	k1gNnSc1
měří	měřit	k5eAaImIp3nS
svalové	svalový	k2eAgFnPc4d1
kontrakce	kontrakce	k1gFnPc4
během	během	k7c2
cvičení	cvičení	k1gNnSc2
(	(	kIx(
<g/>
Kegelova	Kegelův	k2eAgNnSc2d1
cvičení	cvičení	k1gNnSc2
<g/>
)	)	kIx)
na	na	k7c4
posílení	posílení	k1gNnSc4
konečníku	konečník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
cvičení	cvičení	k1gNnPc2
posilují	posilovat	k5eAaImIp3nP
svaly	sval	k1gInPc1
pánevního	pánevní	k2eAgNnSc2d1
dna	dno	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
svalů	sval	k1gInPc2
zapojených	zapojený	k2eAgInPc2d1
do	do	k7c2
kontroly	kontrola	k1gFnSc2
výkalů	výkal	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístroj	přístroj	k1gInSc1
referuje	referovat	k5eAaBmIp3nS
jak	jak	k8xS,k8xC
svaly	sval	k1gInPc4
pracují	pracovat	k5eAaImIp3nP
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
zda	zda	k8xS
provádíte	provádět	k5eAaImIp2nP
cvičení	cvičení	k1gNnSc4
správně	správně	k6eAd1
a	a	k8xC
zda	zda	k8xS
se	se	k3xPyFc4
svaly	sval	k1gInPc1
posilují	posilovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zda	zda	k8xS
biofeedback	biofeedback	k6eAd1
pomůže	pomoct	k5eAaPmIp3nS
i	i	k9
vám	vy	k3xPp2nPc3
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
příčině	příčina	k1gFnSc6
vaší	váš	k3xOp2gFnSc2
fekální	fekální	k2eAgFnSc2d1
inkontinence	inkontinence	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
vážné	vážné	k1gNnSc1
je	být	k5eAaImIp3nS
svalové	svalový	k2eAgNnSc4d1
poškození	poškození	k1gNnSc4
a	a	k8xC
na	na	k7c6
vaší	váš	k3xOp2gFnSc6
schopnosti	schopnost	k1gFnSc6
cvičit	cvičit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Dosáhněte	dosáhnout	k5eAaPmRp2nP
regulární	regulární	k2eAgFnSc4d1
formu	forma	k1gFnSc4
střevních	střevní	k2eAgInPc2d1
pohybů	pohyb	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
lidé	člověk	k1gMnPc1
-	-	kIx~
zvláště	zvláště	k6eAd1
když	když	k8xS
jejich	jejich	k3xOp3gFnSc4
fekální	fekální	k2eAgFnSc4d1
inkontinenci	inkontinence	k1gFnSc4
způsobila	způsobit	k5eAaPmAgFnS
zácpa	zácpa	k1gFnSc1
-	-	kIx~
dosahují	dosahovat	k5eAaImIp3nP
střevní	střevní	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
svým	svůj	k3xOyFgInSc7
tréninkem	trénink	k1gInSc7
aby	aby	kYmCp3nP
měli	mít	k5eAaImAgMnP
střevní	střevní	k2eAgInPc4d1
pohyby	pohyb	k1gInPc4
(	(	kIx(
<g/>
defekaci	defekace	k1gFnSc4
<g/>
)	)	kIx)
ve	v	k7c4
specifickou	specifický	k2eAgFnSc4d1
denní	denní	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
po	po	k7c6
konzumaci	konzumace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatou	podstata	k1gFnSc7
tohoto	tento	k3xDgInSc2
přístupu	přístup	k1gInSc2
je	být	k5eAaImIp3nS
vytrvalost	vytrvalost	k1gFnSc1
-	-	kIx~
dosažení	dosažení	k1gNnSc1
regulární	regulární	k2eAgFnSc2d1
formy	forma	k1gFnSc2
může	moct	k5eAaImIp3nS
trvat	trvat	k5eAaImF
déle	dlouho	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležité	důležitý	k2eAgInPc1d1
je	být	k5eAaImIp3nS
vytrvat	vytrvat	k5eAaPmF
a	a	k8xC
nevzdávat	vzdávat	k5eNaImF
to	ten	k3xDgNnSc4
<g/>
.	.	kIx.
</s>
<s>
Chirurgická	chirurgický	k2eAgFnSc1d1
operace	operace	k1gFnSc1
</s>
<s>
Operace	operace	k1gFnSc1
může	moct	k5eAaImIp3nS
pomoct	pomoct	k5eAaPmF
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
kterých	který	k3yIgMnPc2,k3yRgMnPc2,k3yQgMnPc2
fekální	fekální	k2eAgFnSc6d1
inkontinenci	inkontinence	k1gFnSc6
způsobil	způsobit	k5eAaPmAgInS
úraz	úraz	k1gInSc4
pánevního	pánevní	k2eAgNnSc2d1
dna	dno	k1gNnSc2
<g/>
,	,	kIx,
análního	anální	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
nebo	nebo	k8xC
análního	anální	k2eAgInSc2d1
svěrače	svěrač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
provedeny	provést	k5eAaPmNgFnP
různé	různý	k2eAgFnPc1d1
procedury	procedura	k1gFnPc1
od	od	k7c2
jednoduchých	jednoduchý	k2eAgFnPc2d1
jako	jako	k8xC,k8xS
např.	např.	kA
oprava	oprava	k1gFnSc1
poškozených	poškozený	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
po	po	k7c4
komplexní	komplexní	k2eAgNnSc4d1
jako	jako	k9
připojení	připojení	k1gNnSc4
umělého	umělý	k2eAgInSc2d1
análního	anální	k2eAgInSc2d1
svěrače	svěrač	k1gInSc2
nebo	nebo	k8xC
náhrada	náhrada	k1gFnSc1
análního	anální	k2eAgInSc2d1
svalu	sval	k1gInSc2
svalem	sval	k1gInSc7
z	z	k7c2
nohy	noha	k1gFnSc2
nebo	nebo	k8xC
předloktí	předloktí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
s	s	k7c7
vážnou	vážný	k2eAgFnSc7d1
fekální	fekální	k2eAgFnSc7d1
inkontinencí	inkontinence	k1gFnSc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
neodpovídají	odpovídat	k5eNaImIp3nP
na	na	k7c4
jiné	jiný	k2eAgFnPc4d1
léčby	léčba	k1gFnPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
rozhodnout	rozhodnout	k5eAaPmF
pro	pro	k7c4
kolostomii	kolostomie	k1gFnSc4
(	(	kIx(
<g/>
vyústění	vyústění	k1gNnSc2
tlustého	tlustý	k2eAgNnSc2d1
střeva	střevo	k1gNnSc2
ven	ven	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
vynětí	vynětí	k1gNnSc4
části	část	k1gFnSc2
střeva	střevo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylá	zbylý	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
připevněna	připevněn	k2eAgFnSc1d1
k	k	k7c3
análnímu	anální	k2eAgInSc3d1
otvoru	otvor	k1gInSc3
<g/>
,	,	kIx,
funguje	fungovat	k5eAaImIp3nS
<g/>
-li	-li	k?
pořád	pořád	k6eAd1
správně	správně	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
k	k	k7c3
otvoru	otvor	k1gInSc3
na	na	k7c6
břichu	břich	k1gInSc6
zvanému	zvaný	k2eAgNnSc3d1
stoma	stom	k1gMnSc4
<g/>
,	,	kIx,
přes	přes	k7c4
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
výkaly	výkal	k1gInPc1
opouštějí	opouštět	k5eAaImIp3nP
tělo	tělo	k1gNnSc4
a	a	k8xC
hromadí	hromadit	k5eAaImIp3nP
se	se	k3xPyFc4
do	do	k7c2
vaku	vak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Řešení	řešení	k1gNnSc1
análních	anální	k2eAgFnPc2d1
potíží	potíž	k1gFnPc2
</s>
<s>
Kůže	kůže	k1gFnSc1
kolem	kolem	k7c2
análního	anální	k2eAgInSc2d1
otvoru	otvor	k1gInSc2
je	být	k5eAaImIp3nS
jemná	jemný	k2eAgFnSc1d1
a	a	k8xC
citlivá	citlivý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zácpa	zácpa	k1gFnSc1
a	a	k8xC
průjem	průjem	k1gInSc1
nebo	nebo	k8xC
kontakt	kontakt	k1gInSc1
mezi	mezi	k7c4
kůži	kůže	k1gFnSc4
a	a	k8xC
výkaly	výkal	k1gInPc4
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
bolest	bolest	k1gFnSc4
nebo	nebo	k8xC
svědění	svědění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučený	doporučený	k2eAgInSc4d1
postup	postup	k1gInSc4
na	na	k7c4
úlevu	úleva	k1gFnSc4
potíží	potíž	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Po	po	k7c4
defekaci	defekace	k1gFnSc4
omyjte	omýt	k5eAaPmRp2nP
oblast	oblast	k1gFnSc4
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
mýdla	mýdlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mýdlo	mýdlo	k1gNnSc1
může	moct	k5eAaImIp3nS
vysušit	vysušit	k5eAaPmF
kůži	kůže	k1gFnSc4
a	a	k8xC
zhoršit	zhoršit	k5eAaPmF
potíže	potíž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
možno	možno	k6eAd1
<g/>
,	,	kIx,
myjte	mýt	k5eAaImRp2nP
se	se	k3xPyFc4
ve	v	k7c6
sprše	sprcha	k1gFnSc6
s	s	k7c7
vlažnou	vlažný	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
nebo	nebo	k8xC
použijte	použít	k5eAaPmRp2nP
mělký	mělký	k2eAgInSc4d1
vlažný	vlažný	k2eAgInSc4d1
koupel	koupel	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebo	nebo	k8xC
zkuste	zkusit	k5eAaPmRp2nP
nevymývací	vymývací	k2eNgInSc4d1
kožní	kožní	k2eAgInSc4d1
čistič	čistič	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkuste	zkusit	k5eAaPmRp2nP
nepoužít	použít	k5eNaPmF
toaletní	toaletní	k2eAgInSc4d1
papír	papír	k1gInSc4
na	na	k7c4
utírání	utírání	k1gNnSc4
-	-	kIx~
tření	tření	k1gNnSc4
se	s	k7c7
suchým	suchý	k2eAgInSc7d1
toaletním	toaletní	k2eAgInSc7d1
papírem	papír	k1gInSc7
může	moct	k5eAaImIp3nS
kůži	kůže	k1gFnSc4
ještě	ještě	k9
víc	hodně	k6eAd2
podráždit	podráždit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navlhčené	navlhčený	k2eAgFnPc4d1
utěrky	utěrka	k1gFnPc4
bez	bez	k7c2
alkoholu	alkohol	k1gInSc2
jsou	být	k5eAaImIp3nP
mnohem	mnohem	k6eAd1
lepší	dobrý	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s>
Nechte	nechat	k5eAaPmRp2nP
omytou	omytý	k2eAgFnSc4d1
část	část	k1gFnSc4
těla	tělo	k1gNnSc2
oschnout	oschnout	k5eAaPmF
vzduchem	vzduch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemáte	mít	k5eNaImIp2nP
<g/>
-li	-li	k?
čas	čas	k1gInSc4
<g/>
,	,	kIx,
jemně	jemně	k6eAd1
přiložte	přiložit	k5eAaPmRp2nP
čistou	čistá	k1gFnSc7
jemnou	jemný	k2eAgFnSc4d1
látku	látka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Použijte	použít	k5eAaPmRp2nP
hydratační	hydratační	k2eAgInSc4d1
(	(	kIx(
<g/>
zvlhčující	zvlhčující	k2eAgInSc4d1
<g/>
)	)	kIx)
krém	krém	k1gInSc4
vhodný	vhodný	k2eAgInSc4d1
na	na	k7c4
prevenci	prevence	k1gFnSc4
podráždění	podráždění	k1gNnSc2
kůže	kůže	k1gFnSc2
z	z	k7c2
přímého	přímý	k2eAgInSc2d1
kontaktu	kontakt	k1gInSc2
s	s	k7c7
výkaly	výkal	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použití	použití	k1gNnSc1
análních	anální	k2eAgFnPc2d1
mastí	mast	k1gFnPc2
a	a	k8xC
konzultujte	konzultovat	k5eAaImRp2nP
s	s	k7c7
doktorem	doktor	k1gMnSc7
<g/>
,	,	kIx,
protože	protože	k8xS
některé	některý	k3yIgInPc4
mohou	moct	k5eAaImIp3nP
obsahovat	obsahovat	k5eAaImF
dráždící	dráždící	k2eAgFnPc4d1
ingredience	ingredience	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
použitím	použití	k1gNnSc7
krému	krém	k1gInSc2
anální	anální	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
jemně	jemně	k6eAd1
omyjte	omýt	k5eAaPmRp2nP
a	a	k8xC
vysušte	vysušit	k5eAaPmRp2nP
na	na	k7c4
prevenci	prevence	k1gFnSc4
bakteriální	bakteriální	k2eAgFnSc2d1
infekce	infekce	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
další	další	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zkuste	zkusit	k5eAaPmRp2nP
použít	použít	k5eAaPmF
kosmetický	kosmetický	k2eAgInSc4d1
zásyp	zásyp	k1gInSc4
nebo	nebo	k8xC
obilní	obilní	k2eAgInSc4d1
škrob	škrob	k1gInSc4
na	na	k7c4
zmírnění	zmírnění	k1gNnSc4
análních	anální	k2eAgFnPc2d1
potíží	potíž	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Noste	nosit	k5eAaImRp2nP
bavlněné	bavlněný	k2eAgNnSc4d1
spodní	spodní	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
a	a	k8xC
volný	volný	k2eAgInSc4d1
oděv	oděv	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
„	„	k?
<g/>
dýchá	dýchat	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těsný	těsný	k2eAgInSc1d1
oděv	oděv	k1gInSc1
blokující	blokující	k2eAgInSc1d1
vzduch	vzduch	k1gInSc1
může	moct	k5eAaImIp3nS
anální	anální	k2eAgInPc4d1
problémy	problém	k1gInPc4
zhoršit	zhoršit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyměňte	vyměnit	k5eAaPmRp2nP
špinavé	špinavý	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
co	co	k9
nejdříve	dříve	k6eAd3
<g/>
.	.	kIx.
</s>
<s>
Používáte	používat	k5eAaImIp2nP
<g/>
-li	-li	k?
vycpávky	vycpávka	k1gFnPc1
nebo	nebo	k8xC
plínky	plínka	k1gFnPc1
<g/>
,	,	kIx,
ujistěte	ujistit	k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
mají	mít	k5eAaImIp3nP
absorbující	absorbující	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
na	na	k7c6
vrchu	vrch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Produkty	produkt	k1gInPc4
s	s	k7c7
absorbující	absorbující	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
chrání	chránit	k5eAaImIp3nP
kůži	kůže	k1gFnSc4
tažením	tažení	k1gNnSc7
výkalů	výkal	k1gInPc2
a	a	k8xC
vlhka	vlhko	k1gNnSc2
z	z	k7c2
kůže	kůže	k1gFnSc2
do	do	k7c2
vycpávky	vycpávka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Emocionální	emocionální	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
</s>
<s>
Protože	protože	k8xS
fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
stres	stres	k1gInSc4
z	z	k7c2
rozpaků	rozpak	k1gInPc2
<g/>
,	,	kIx,
obav	obava	k1gFnPc2
a	a	k8xC
samoty	samota	k1gFnSc2
<g/>
,	,	kIx,
důležité	důležitý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
podniknout	podniknout	k5eAaPmF
kroky	krok	k1gInPc4
pro	pro	k7c4
zlepšení	zlepšení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Léčba	léčba	k1gFnSc1
může	moct	k5eAaImIp3nS
zlepšit	zlepšit	k5eAaPmF
váš	váš	k3xOp2gInSc4
život	život	k1gInSc4
a	a	k8xC
pomoci	pomoct	k5eAaPmF
vám	vy	k3xPp2nPc3
cítit	cítit	k5eAaImF
se	se	k3xPyFc4
lépe	dobře	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nenavštívili	navštívit	k5eNaPmAgMnP
<g/>
-li	-li	k?
jste	být	k5eAaImIp2nP
dosud	dosud	k6eAd1
doktora	doktor	k1gMnSc2
<g/>
,	,	kIx,
udělejte	udělat	k5eAaPmRp2nP
to	ten	k3xDgNnSc4
co	co	k9
nejdřív	dříve	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Též	též	k6eAd1
uvažujte	uvažovat	k5eAaImRp2nP
kontaktovat	kontaktovat	k5eAaImF
organizace	organizace	k1gFnSc2
zaměřené	zaměřený	k2eAgFnSc2d1
na	na	k7c6
inkontinenci	inkontinence	k1gFnSc6
ve	v	k7c6
vašem	váš	k3xOp2gNnSc6
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
společnosti	společnost	k1gFnSc2
vám	vy	k3xPp2nPc3
pomohou	pomoct	k5eAaPmIp3nP
najít	najít	k5eAaPmF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
a	a	k8xC
případně	případně	k6eAd1
vás	vy	k3xPp2nPc4
doporučí	doporučit	k5eAaPmIp3nS
doktorům	doktor	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
specializují	specializovat	k5eAaBmIp3nP
na	na	k7c4
léčbu	léčba	k1gFnSc4
fekální	fekální	k2eAgFnSc2d1
inkontinence	inkontinence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Každodenní	každodenní	k2eAgFnPc1d1
praktické	praktický	k2eAgFnPc1d1
rady	rada	k1gFnPc1
</s>
<s>
V	v	k7c6
příruční	příruční	k2eAgFnSc6d1
tašce	taška	k1gFnSc6
noste	nosit	k5eAaImRp2nP
čisticí	čisticí	k2eAgInPc1d1
potřeby	potřeba	k1gFnPc4
a	a	k8xC
náhradní	náhradní	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Poznejte	poznat	k5eAaPmRp2nP
veřejné	veřejný	k2eAgNnSc4d1
toalety	toaleta	k1gFnPc4
dopředu	dopředu	k6eAd1
<g/>
,	,	kIx,
abyste	aby	kYmCp2nP
je	on	k3xPp3gMnPc4
pak	pak	k6eAd1
nemuseli	muset	k5eNaImAgMnP
hledat	hledat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Použijte	použít	k5eAaPmRp2nP
toaletu	toaleta	k1gFnSc4
před	před	k7c7
odchodem	odchod	k1gInSc7
ven	ven	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Myslíte	myslet	k5eAaImIp2nP
<g/>
-li	-li	k?
<g/>
,	,	kIx,
že	že	k8xS
inkontinence	inkontinence	k1gFnSc1
je	být	k5eAaImIp3nS
pravděpodobná	pravděpodobný	k2eAgNnPc1d1
<g/>
,	,	kIx,
noste	nosit	k5eAaImRp2nP
vhodný	vhodný	k2eAgInSc4d1
„	„	k?
<g/>
pododěv	pododět	k5eAaPmDgInS
<g/>
“	“	k?
nebo	nebo	k8xC
sanitární	sanitární	k2eAgFnPc1d1
vložky	vložka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
inkontinence	inkontinence	k1gFnSc2
časté	častý	k2eAgNnSc1d1
<g/>
,	,	kIx,
použijte	použít	k5eAaPmRp2nP
fekální	fekální	k2eAgInPc4d1
deodoranty	deodorant	k1gInPc4
na	na	k7c4
zpříjemnění	zpříjemnění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
u	u	k7c2
dětí	dítě	k1gFnPc2
</s>
<s>
Trpí	trpět	k5eAaImIp3nP
<g/>
-li	-li	k?
dítě	dítě	k1gNnSc4
fekální	fekální	k2eAgFnSc7d1
inkontinencí	inkontinence	k1gFnSc7
<g/>
,	,	kIx,
nutno	nutno	k6eAd1
navštívit	navštívit	k5eAaPmF
doktora	doktor	k1gMnSc4
na	na	k7c4
určení	určení	k1gNnSc4
příčiny	příčina	k1gFnSc2
a	a	k8xC
léčby	léčba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
u	u	k7c2
dětí	dítě	k1gFnPc2
vyskytnout	vyskytnout	k5eAaPmF
pro	pro	k7c4
porodní	porodní	k2eAgInSc4d1
defekt	defekt	k1gInSc4
nebo	nebo	k8xC
nemoc	nemoc	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
pro	pro	k7c4
chronickou	chronický	k2eAgFnSc4d1
zácpu	zácpa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Děti	dítě	k1gFnPc1
trénované	trénovaný	k2eAgFnPc1d1
na	na	k7c4
nočník	nočník	k1gInSc4
mají	mít	k5eAaImIp3nP
často	často	k6eAd1
zácpu	zácpa	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
odmítají	odmítat	k5eAaImIp3nP
chodit	chodit	k5eAaImF
na	na	k7c4
toaletu	toaleta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problém	problém	k1gInSc1
může	moct	k5eAaImIp3nS
pocházet	pocházet	k5eAaImF
z	z	k7c2
rozpaků	rozpak	k1gInPc2
z	z	k7c2
použití	použití	k1gNnSc2
veřejné	veřejný	k2eAgFnSc2d1
toalety	toaleta	k1gFnSc2
nebo	nebo	k8xC
z	z	k7c2
nevůle	nevůle	k1gFnSc2
stopnout	stopnout	k5eAaPmF
hraní	hraní	k1gNnSc4
a	a	k8xC
jít	jít	k5eAaImF
na	na	k7c4
toaletu	toaleta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
-li	-li	k?
dítě	dítě	k1gNnSc1
v	v	k7c6
zadržování	zadržování	k1gNnSc6
výkalů	výkal	k1gInPc2
<g/>
,	,	kIx,
ty	ten	k3xDgInPc1
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
hromadit	hromadit	k5eAaImF
a	a	k8xC
tvrdnout	tvrdnout	k5eAaImF
v	v	k7c6
konečníku	konečník	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dítě	dítě	k1gNnSc1
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
bolesti	bolest	k1gFnPc4
žaludku	žaludek	k1gInSc2
a	a	k8xC
málo	málo	k6eAd1
jíst	jíst	k5eAaImF
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
má	mít	k5eAaImIp3nS
hlad	hlad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
když	když	k8xS
případně	případně	k6eAd1
výkaly	výkal	k1gInPc4
vyloučí	vyloučit	k5eAaPmIp3nS
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
to	ten	k3xDgNnSc1
být	být	k5eAaImF
bolestivé	bolestivý	k2eAgNnSc1d1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
strachu	strach	k1gInSc3
z	z	k7c2
defekace	defekace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dítě	dítě	k1gNnSc1
se	se	k3xPyFc4
zácpou	zácpa	k1gFnSc7
může	moct	k5eAaImIp3nS
zašpinit	zašpinit	k5eAaPmF
spodní	spodní	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znečištění	znečištění	k1gNnSc1
nastane	nastat	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
tekuté	tekutý	k2eAgInPc1d1
výkaly	výkal	k1gInPc1
z	z	k7c2
další	další	k2eAgFnSc2d1
kumulace	kumulace	k1gFnSc2
ve	v	k7c6
střevě	střevo	k1gNnSc6
prosakují	prosakovat	k5eAaImIp3nP
přes	přes	k7c4
starší	starší	k1gMnPc4
tuhé	tuhý	k2eAgInPc1d1
výkaly	výkal	k1gInPc1
v	v	k7c6
konečníku	konečník	k1gInSc6
a	a	k8xC
tečou	téct	k5eAaImIp3nP
ven	ven	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znečištění	znečištění	k1gNnPc4
je	být	k5eAaImIp3nS
znakem	znak	k1gInSc7
fekální	fekální	k2eAgFnSc2d1
inkontinence	inkontinence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležité	důležitý	k2eAgInPc1d1
je	být	k5eAaImIp3nS
si	se	k3xPyFc3
uvědomit	uvědomit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
dítě	dítě	k1gNnSc1
to	ten	k3xDgNnSc1
nedělá	dělat	k5eNaImIp3nS
úmyslně	úmyslně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
jednoduše	jednoduše	k6eAd1
nemůže	moct	k5eNaImIp3nS
kontrolovat	kontrolovat	k5eAaImF
kapalné	kapalný	k2eAgInPc4d1
výkaly	výkal	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
krok	krok	k1gInSc1
je	být	k5eAaImIp3nS
vyloučení	vyloučení	k1gNnSc4
vytvořených	vytvořený	k2eAgInPc2d1
výkalů	výkal	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doktor	doktor	k1gMnSc1
může	moct	k5eAaImIp3nS
předepsat	předepsat	k5eAaPmF
1	#num#	k4
nebo	nebo	k8xC
víc	hodně	k6eAd2
klystýrů	klystýr	k1gInPc2
nebo	nebo	k8xC
nápojů	nápoj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
pomohou	pomoct	k5eAaPmIp3nP
vyčistit	vyčistit	k5eAaPmF
střevo	střevo	k1gNnSc4
<g/>
,	,	kIx,
např.	např.	kA
hořčíkový	hořčíkový	k2eAgInSc1d1
citrát	citrát	k1gInSc1
<g/>
,	,	kIx,
minerální	minerální	k2eAgInSc1d1
olej	olej	k1gInSc1
nebo	nebo	k8xC
polyetylenglykol	polyetylenglykol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInSc1d1
krok	krok	k1gInSc1
je	být	k5eAaImIp3nS
prevence	prevence	k1gFnSc1
nové	nový	k2eAgFnSc2d1
zácpy	zácpa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dítě	Dítě	k1gMnSc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
naučit	naučit	k5eAaPmF
pravidelně	pravidelně	k6eAd1
vylučovat	vylučovat	k5eAaImF
výkaly	výkal	k1gInPc4
pomocí	pomocí	k7c2
tréninku	trénink	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experti	expert	k1gMnPc1
doporučují	doporučovat	k5eAaImIp3nP
rodičům	rodič	k1gMnPc3
nechat	nechat	k5eAaPmF
dítě	dítě	k1gNnSc4
sedět	sedět	k5eAaImF
na	na	k7c6
toaletě	toaleta	k1gFnSc6
4	#num#	k4
krát	krát	k6eAd1
denně	denně	k6eAd1
(	(	kIx(
<g/>
po	po	k7c6
jídle	jídlo	k1gNnSc6
a	a	k8xC
před	před	k7c7
spánkem	spánek	k1gInSc7
<g/>
)	)	kIx)
na	na	k7c4
5	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dítě	Dítě	k1gMnSc1
může	moct	k5eAaImIp3nS
dostat	dostat	k5eAaPmF
odměnu	odměna	k1gFnSc4
za	za	k7c4
defekaci	defekace	k1gFnSc4
na	na	k7c6
toaletě	toaleta	k1gFnSc6
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
pomůže	pomoct	k5eAaPmIp3nS
vyvinout	vyvinout	k5eAaPmF
pozitivní	pozitivní	k2eAgInSc1d1
návyk	návyk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležité	důležitý	k2eAgInPc1d1
je	být	k5eAaImIp3nS
netrestat	trestat	k5eNaImF
dítě	dítě	k1gNnSc4
za	za	k7c4
případy	případ	k1gInPc4
inkontinence	inkontinence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Určité	určitý	k2eAgFnPc1d1
změny	změna	k1gFnPc1
zvyků	zvyk	k1gInPc2
u	u	k7c2
konzumace	konzumace	k1gFnSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
užitečné	užitečný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dítě	dítě	k1gNnSc1
by	by	kYmCp3nP
mělo	mít	k5eAaImAgNnS
konzumovat	konzumovat	k5eAaBmF
víc	hodně	k6eAd2
vláknité	vláknitý	k2eAgFnPc1d1
potravy	potrava	k1gFnSc2
na	na	k7c4
zjemnění	zjemnění	k1gNnSc4
výkalů	výkal	k1gInPc2
<g/>
,	,	kIx,
vynechat	vynechat	k5eAaPmF
mléčné	mléčný	k2eAgInPc4d1
produkty	produkt	k1gInPc4
způsobují	způsobovat	k5eAaImIp3nP
<g/>
-li	-li	k?
zácpu	zácpa	k1gFnSc4
a	a	k8xC
pít	pít	k5eAaImF
denně	denně	k6eAd1
mnoho	mnoho	k4c4
tekutin	tekutina	k1gFnPc2
včetně	včetně	k7c2
vody	voda	k1gFnSc2
a	a	k8xC
ovocných	ovocný	k2eAgInPc2d1
džusů	džus	k1gInPc2
(	(	kIx(
<g/>
švestky	švestka	k1gFnPc1
<g/>
,	,	kIx,
grapefruit	grapefruit	k1gInSc1
<g/>
,	,	kIx,
meruňky	meruňka	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
pomáhají	pomáhat	k5eAaImIp3nP
předejít	předejít	k5eAaPmF
zácpě	zácpa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
může	moct	k5eAaImIp3nS
doktor	doktor	k1gMnSc1
předepsat	předepsat	k5eAaPmF
laxativa	laxativum	k1gNnPc4
(	(	kIx(
<g/>
projímadla	projímadlo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Může	moct	k5eAaImIp3nS
trvat	trvat	k5eAaImF
i	i	k9
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
dítě	dítě	k1gNnSc1
zbaví	zbavit	k5eAaPmIp3nS
zvyku	zvyk	k1gInSc2
zadržovat	zadržovat	k5eAaImF
výkaly	výkal	k1gInPc1
a	a	k8xC
mít	mít	k5eAaImF
zácpu	zácpa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
případy	případ	k1gInPc1
inkontinence	inkontinence	k1gFnSc2
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
vyskytnout	vyskytnout	k5eAaPmF
i	i	k9
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležité	důležitý	k2eAgInPc1d1
je	být	k5eAaImIp3nS
věnovat	věnovat	k5eAaImF,k5eAaPmF
návykům	návyk	k1gInPc3
na	na	k7c4
toaletu	toaleta	k1gFnSc4
trvalou	trvalý	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
varující	varující	k2eAgInSc4d1
příznaky	příznak	k1gInPc4
pro	pro	k7c4
sledování	sledování	k1gNnSc4
zahrnují	zahrnovat	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
bolesti	bolest	k1gFnPc1
při	při	k7c6
střevních	střevní	k2eAgInPc6d1
pohybech	pohyb	k1gInPc6
</s>
<s>
tuhé	tuhý	k2eAgInPc1d1
výkaly	výkal	k1gInPc1
</s>
<s>
zácpa	zácpa	k1gFnSc1
</s>
<s>
odmítání	odmítání	k1gNnSc4
chodit	chodit	k5eAaImF
na	na	k7c4
toaletu	toaleta	k1gFnSc4
</s>
<s>
špinavé	špinavý	k2eAgNnSc4d1
spodní	spodní	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
</s>
<s>
znaky	znak	k1gInPc1
zadržování	zadržování	k1gNnSc2
defekace	defekace	k1gFnSc2
jako	jako	k8xC,k8xS
dřepění	dřepění	k1gNnSc2
<g/>
,	,	kIx,
překládání	překládání	k1gNnSc2
nohou	noha	k1gFnPc2
nebo	nebo	k8xC
kolísání	kolísání	k1gNnSc1
dopředu	dopředu	k6eAd1
a	a	k8xC
dozadu	dozadu	k6eAd1
</s>
<s>
Příčiny	příčina	k1gFnPc4
zácpy	zácpa	k1gFnSc2
u	u	k7c2
dětí	dítě	k1gFnPc2
</s>
<s>
Trénink	trénink	k1gInSc4
na	na	k7c4
nočník	nočník	k1gInSc4
začal	začít	k5eAaPmAgMnS
příliš	příliš	k6eAd1
brzo	brzo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Odmítají	odmítat	k5eAaImIp3nP
defekaci	defekace	k1gFnSc4
(	(	kIx(
<g/>
pro	pro	k7c4
bolesti	bolest	k1gFnPc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
,	,	kIx,
rozpaky	rozpak	k1gInPc4
<g/>
,	,	kIx,
tvrdohlavost	tvrdohlavost	k1gFnSc4
nebo	nebo	k8xC
i	i	k9
pro	pro	k7c4
odpor	odpor	k1gInSc4
k	k	k7c3
toaletám	toaleta	k1gFnPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jsou	být	k5eAaImIp3nP
na	na	k7c6
neznámém	známý	k2eNgNnSc6d1
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Reagují	reagovat	k5eAaBmIp3nP
na	na	k7c4
rodinný	rodinný	k2eAgInSc4d1
stres	stres	k1gInSc4
jako	jako	k8xC,k8xS
např.	např.	kA
nový	nový	k2eAgMnSc1d1
sourozenec	sourozenec	k1gMnSc1
nebo	nebo	k8xC
neshody	neshoda	k1gFnPc1
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Nemohou	moct	k5eNaImIp3nP
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
na	na	k7c4
toaletu	toaleta	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
potřebují	potřebovat	k5eAaImIp3nP
a	a	k8xC
proto	proto	k8xC
výkaly	výkal	k1gInPc1
zadrží	zadržet	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
konečník	konečník	k1gInSc1
zaplní	zaplnit	k5eAaPmIp3nS
výkaly	výkal	k1gInPc4
<g/>
,	,	kIx,
dítě	dítě	k1gNnSc1
může	moct	k5eAaImIp3nS
ztratit	ztratit	k5eAaPmF
nutkání	nutkání	k1gNnSc4
na	na	k7c4
defekaci	defekace	k1gFnSc4
a	a	k8xC
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
zácpě	zácpa	k1gFnSc3
<g/>
,	,	kIx,
výkaly	výkal	k1gInPc1
schnou	schnout	k5eAaImIp3nP
a	a	k8xC
tuhnou	tuhnout	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vyměšování	vyměšování	k1gNnSc1
</s>
<s>
Hygiena	hygiena	k1gFnSc1
</s>
<s>
Inkontinence	inkontinence	k1gFnSc1
</s>
<s>
Záchod	záchod	k1gInSc1
</s>
<s>
Toaletní	toaletní	k2eAgInSc1d1
papír	papír	k1gInSc1
</s>
<s>
Větry	vítr	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Web	web	k1gInSc1
Anamneza	Anamnez	k1gMnSc2
</s>
<s>
Na	na	k7c6
webu	web	k1gInSc6
Zdraví	zdraví	k1gNnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Web	web	k1gInSc1
Top	topit	k5eAaImRp2nS
lékař	lékař	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Symptomy	symptom	k1gInPc1
a	a	k8xC
příznaky	příznak	k1gInPc1
<g/>
:	:	kIx,
trávicí	trávicí	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
a	a	k8xC
břicho	břicho	k1gNnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
R	R	kA
<g/>
19	#num#	k4
<g/>
,	,	kIx,
787	#num#	k4
<g/>
)	)	kIx)
trávicí	trávicí	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
horní	horní	k2eAgFnSc1d1
</s>
<s>
nevolnost	nevolnost	k1gFnSc1
<g/>
/	/	kIx~
<g/>
zvracení	zvracení	k1gNnSc1
·	·	k?
pálení	pálení	k1gNnSc1
žáhy	žáha	k1gFnSc2
·	·	k?
dysfagie	dysfagie	k1gFnSc2
(	(	kIx(
<g/>
orofaryngeální	orofaryngeální	k2eAgInSc4d1
<g/>
,	,	kIx,
esofageální	esofageální	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
zápach	zápach	k1gInSc4
z	z	k7c2
úst	ústa	k1gNnPc2
dolní	dolní	k2eAgFnSc2d1
</s>
<s>
větry	vítr	k1gInPc1
(	(	kIx(
<g/>
flatulence	flatulence	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
příbuzné	příbuzný	k2eAgInPc1d1
stavy	stav	k1gInPc1
(	(	kIx(
<g/>
nadýmání	nadýmání	k1gNnSc1
<g/>
,	,	kIx,
plynatost	plynatost	k1gFnSc1
<g/>
,	,	kIx,
říhání	říhání	k1gNnSc1
<g/>
,	,	kIx,
bubínkový	bubínkový	k2eAgInSc1d1
poklep	poklep	k1gInSc1
<g/>
)	)	kIx)
fekální	fekální	k2eAgFnSc1d1
inkontinence	inkontinence	k1gFnSc1
(	(	kIx(
<g/>
enkopréza	enkopréza	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
rektální	rektální	k2eAgInSc4d1
tenesmus	tenesmus	k1gInSc4
okultní	okultní	k2eAgNnSc4d1
krvácení	krvácení	k1gNnSc4
ve	v	k7c6
stolici	stolice	k1gFnSc6
</s>
<s>
žlázy	žláza	k1gFnPc1
<g/>
/	/	kIx~
<g/>
slezina	slezina	k1gFnSc1
</s>
<s>
hepatosplenomegalie	hepatosplenomegalie	k1gFnSc1
(	(	kIx(
<g/>
hepatomegalie	hepatomegalie	k1gFnSc1
<g/>
,	,	kIx,
splenomegalie	splenomegalie	k1gFnSc1
<g/>
)	)	kIx)
žloutenka	žloutenka	k1gFnSc1
břišní	břišní	k2eAgFnSc2d1
–	–	k?
obecné	obecná	k1gFnSc2
</s>
<s>
bolest	bolest	k1gFnSc1
břicha	břicho	k1gNnSc2
(	(	kIx(
<g/>
akutní	akutní	k2eAgNnSc1d1
břicho	břicho	k1gNnSc1
<g/>
,	,	kIx,
kolika	kolika	k1gFnSc1
<g/>
)	)	kIx)
ascites	ascites	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4148806-4	4148806-4	k4
</s>
