<s>
Korsika	Korsika	k1gFnSc1	Korsika
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
spadající	spadající	k2eAgInSc1d1	spadající
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
ostrova	ostrov	k1gInSc2	ostrov
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
8680	[number]	k4	8680
km2	km2	k4	km2
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
zaťatou	zaťatý	k2eAgFnSc4d1	zaťatá
pěst	pěst	k1gFnSc4	pěst
se	s	k7c7	s
vztyčeným	vztyčený	k2eAgInSc7d1	vztyčený
palcem	palec	k1gInSc7	palec
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
183	[number]	k4	183
km	km	kA	km
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
83	[number]	k4	83
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přibližně	přibližně	k6eAd1	přibližně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vzdálenostem	vzdálenost	k1gFnPc3	vzdálenost
ostrova	ostrov	k1gInSc2	ostrov
od	od	k7c2	od
evropské	evropský	k2eAgFnSc2d1	Evropská
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
180	[number]	k4	180
km	km	kA	km
<g/>
,	,	kIx,	,
od	od	k7c2	od
Itálie	Itálie	k1gFnSc2	Itálie
80	[number]	k4	80
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Obýván	obýván	k2eAgMnSc1d1	obýván
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
322	[number]	k4	322
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
Monte	Mont	k1gMnSc5	Mont
Cinto	cinta	k1gMnSc5	cinta
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
2706	[number]	k4	2706
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
ale	ale	k8xC	ale
často	často	k6eAd1	často
italsky	italsky	k6eAd1	italsky
a	a	k8xC	a
také	také	k9	také
korsicky	korsicky	k6eAd1	korsicky
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
původní	původní	k2eAgInSc1d1	původní
jazyk	jazyk	k1gInSc1	jazyk
byl	být	k5eAaImAgInS	být
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
zakázán	zakázán	k2eAgInSc1d1	zakázán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
italského	italský	k2eAgNnSc2d1	italské
nářečí	nářečí	k1gNnSc2	nářečí
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
toskánských	toskánský	k2eAgInPc2d1	toskánský
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Korsika	Korsika	k1gFnSc1	Korsika
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
narození	narození	k1gNnSc2	narození
Napoleona	Napoleon	k1gMnSc2	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
a	a	k8xC	a
nedotčenou	dotčený	k2eNgFnSc7d1	nedotčená
krajinou	krajina	k1gFnSc7	krajina
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
ledovcových	ledovcový	k2eAgNnPc2d1	ledovcové
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
ostrova	ostrov	k1gInSc2	ostrov
jsou	být	k5eAaImIp3nP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
v	v	k7c6	v
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
bělavé	bělavý	k2eAgFnPc4d1	bělavá
pláže	pláž	k1gFnPc4	pláž
a	a	k8xC	a
čisté	čistý	k2eAgNnSc4d1	čisté
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
1047	[number]	k4	1047
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gMnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
častý	častý	k2eAgInSc4d1	častý
cíl	cíl	k1gInSc4	cíl
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
záznamy	záznam	k1gInPc1	záznam
o	o	k7c4	o
osídlení	osídlení	k1gNnSc4	osídlení
ostrova	ostrov	k1gInSc2	ostrov
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
osídlen	osídlen	k2eAgInSc1d1	osídlen
Řeky	Řek	k1gMnPc7	Řek
<g/>
,	,	kIx,	,
Etrusky	Etrusk	k1gMnPc7	Etrusk
a	a	k8xC	a
Kartaginci	Kartaginec	k1gMnPc7	Kartaginec
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
564	[number]	k4	564
př.n.l.	př.n.l.	k?	př.n.l.
založili	založit	k5eAaPmAgMnP	založit
Řekové	Řek	k1gMnPc1	Řek
dnešní	dnešní	k2eAgFnSc4d1	dnešní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Ajaccio	Ajaccio	k6eAd1	Ajaccio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
227	[number]	k4	227
př.n.l.	př.n.l.	k?	př.n.l.
připadl	připadnout	k5eAaPmAgInS	připadnout
ostrov	ostrov	k1gInSc1	ostrov
do	do	k7c2	do
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
ostrov	ostrov	k1gInSc1	ostrov
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgInS	stát
cílem	cíl	k1gInSc7	cíl
mnohých	mnohý	k2eAgInPc2d1	mnohý
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
střídali	střídat	k5eAaImAgMnP	střídat
vládci	vládce	k1gMnPc1	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
ostrov	ostrov	k1gInSc1	ostrov
patřil	patřit	k5eAaImAgInS	patřit
italskému	italský	k2eAgMnSc3d1	italský
státu	stát	k1gInSc2	stát
Pise	Pisa	k1gFnSc6	Pisa
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Janovu	Janův	k2eAgFnSc4d1	Janova
<g/>
.	.	kIx.	.
</s>
<s>
Přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
Calvi	Calev	k1gFnSc3	Calev
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
možných	možný	k2eAgNnPc2d1	možné
rodišť	rodiště	k1gNnPc2	rodiště
"	"	kIx"	"
<g/>
Janovana	Janovan	k1gMnSc2	Janovan
<g/>
"	"	kIx"	"
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
začaly	začít	k5eAaPmAgInP	začít
pěstovat	pěstovat	k5eAaImF	pěstovat
jedlé	jedlý	k2eAgInPc1d1	jedlý
kaštany	kaštan	k1gInPc1	kaštan
a	a	k8xC	a
olivy	oliva	k1gFnPc1	oliva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
nad	nad	k7c7	nad
Itálií	Itálie	k1gFnSc7	Itálie
získala	získat	k5eAaPmAgFnS	získat
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c4	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
navrácen	navrátit	k5eAaPmNgInS	navrátit
opět	opět	k6eAd1	opět
městu	město	k1gNnSc3	město
Janov	Janov	k1gInSc1	Janov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
nejistém	jistý	k2eNgNnSc6d1	nejisté
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
o	o	k7c4	o
samostatnost	samostatnost	k1gFnSc4	samostatnost
Korsiky	Korsika	k1gFnSc2	Korsika
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
význačný	význačný	k2eAgMnSc1d1	význačný
rodák	rodák	k1gMnSc1	rodák
Pasquale	Pasquala	k1gFnSc3	Pasquala
Paoli	Paol	k1gMnPc1	Paol
<g/>
.	.	kIx.	.
</s>
<s>
Korsika	Korsika	k1gFnSc1	Korsika
definitivně	definitivně	k6eAd1	definitivně
připadla	připadnout	k5eAaPmAgFnS	připadnout
do	do	k7c2	do
područí	područí	k1gNnSc2	područí
Francie	Francie	k1gFnSc2	Francie
roku	rok	k1gInSc2	rok
1768	[number]	k4	1768
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
okupována	okupován	k2eAgFnSc1d1	okupována
italskými	italský	k2eAgNnPc7d1	italské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Francie	Francie	k1gFnSc2	Francie
autonomii	autonomie	k1gFnSc4	autonomie
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
regionálním	regionální	k2eAgInSc7d1	regionální
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
v	v	k7c6	v
městě	město	k1gNnSc6	město
Ajaccio	Ajaccio	k1gMnSc1	Ajaccio
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k6eAd1	také
narodil	narodit	k5eAaPmAgInS	narodit
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1769	[number]	k4	1769
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
prst	prst	k1gInSc1	prst
<g/>
)	)	kIx)	)
ostrova	ostrov	k1gInSc2	ostrov
kulminuje	kulminovat	k5eAaImIp3nS	kulminovat
vrcholem	vrchol	k1gInSc7	vrchol
Mt	Mt	k1gFnSc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Stello	Stella	k1gFnSc5	Stella
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
na	na	k7c4	na
Mt	Mt	k1gFnSc4	Mt
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Pietro	Pietro	k1gNnSc1	Pietro
(	(	kIx(	(
<g/>
1	[number]	k4	1
766	[number]	k4	766
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
horská	horský	k2eAgFnSc1d1	horská
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
mladšího	mladý	k2eAgNnSc2d2	mladší
data	datum	k1gNnSc2	datum
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
krystalické	krystalický	k2eAgFnPc1d1	krystalická
břidlice	břidlice	k1gFnPc1	břidlice
a	a	k8xC	a
druhohorní	druhohorní	k2eAgInPc1d1	druhohorní
sedimenty	sediment	k1gInPc1	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Vápenité	vápenitý	k2eAgFnPc1d1	vápenitá
břidlice	břidlice	k1gFnPc1	břidlice
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ještě	ještě	k9	ještě
fylity	fylit	k1gInPc1	fylit
a	a	k8xC	a
polohy	poloha	k1gFnPc1	poloha
dioritů	diorit	k1gInPc2	diorit
a	a	k8xC	a
diabasů	diabas	k1gInPc2	diabas
<g/>
.	.	kIx.	.
</s>
<s>
Konečnou	konečný	k2eAgFnSc4d1	konečná
podobu	podoba	k1gFnSc4	podoba
dalo	dát	k5eAaPmAgNnS	dát
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
alpinské	alpinský	k2eAgNnSc4d1	alpinské
vrásnění	vrásnění	k1gNnSc4	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
kolem	kolem	k7c2	kolem
Bonifacia	Bonifacium	k1gNnSc2	Bonifacium
se	se	k3xPyFc4	se
vklínily	vklínit	k5eAaPmAgInP	vklínit
neogenní	neogenní	k2eAgInPc1d1	neogenní
vápencové	vápencový	k2eAgInPc1d1	vápencový
sedimenty	sediment	k1gInPc1	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
zabírají	zabírat	k5eAaImIp3nP	zabírat
skalní	skalní	k2eAgInPc1d1	skalní
štíty	štít	k1gInPc1	štít
se	s	k7c7	s
strmými	strmý	k2eAgFnPc7d1	strmá
stěnami	stěna	k1gFnPc7	stěna
<g/>
,	,	kIx,	,
přecházející	přecházející	k2eAgFnSc1d1	přecházející
na	na	k7c6	na
západě	západ	k1gInSc6	západ
v	v	k7c6	v
pobřeží	pobřeží	k1gNnSc6	pobřeží
fjordového	fjordový	k2eAgInSc2d1	fjordový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
teorie	teorie	k1gFnPc1	teorie
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c4	o
pokračování	pokračování	k1gNnSc4	pokračování
vrásné	vrásný	k2eAgFnSc2d1	vrásný
zóny	zóna	k1gFnSc2	zóna
španělské	španělský	k2eAgFnSc2d1	španělská
Sierry	Sierra	k1gFnSc2	Sierra
Nevady	Nevada	k1gFnSc2	Nevada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
krystalický	krystalický	k2eAgInSc1d1	krystalický
masiv	masiv	k1gInSc1	masiv
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
stavbě	stavba	k1gFnSc6	stavba
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
hlavně	hlavně	k9	hlavně
žuly	žula	k1gFnPc1	žula
variského	variský	k2eAgNnSc2d1	variské
stáří	stáří	k1gNnSc2	stáří
s	s	k7c7	s
četnými	četný	k2eAgInPc7d1	četný
zlomy	zlom	k1gInPc7	zlom
a	a	k8xC	a
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
porfyry	porfyr	k1gInPc1	porfyr
(	(	kIx(	(
<g/>
Paglia	Paglia	k1gFnSc1	Paglia
<g/>
,	,	kIx,	,
Orba	orba	k1gFnSc1	orba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostré	ostrý	k2eAgInPc1d1	ostrý
vrcholy	vrchol	k1gInPc1	vrchol
<g/>
,	,	kIx,	,
hluboce	hluboko	k6eAd1	hluboko
zaříznutá	zaříznutý	k2eAgNnPc1d1	zaříznuté
údolí	údolí	k1gNnPc1	údolí
alpského	alpský	k2eAgInSc2d1	alpský
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
ledovcové	ledovcový	k2eAgFnSc2d1	ledovcová
činnosti	činnost	k1gFnSc2	činnost
(	(	kIx(	(
<g/>
období	období	k1gNnSc1	období
pleistocénu	pleistocén	k1gInSc2	pleistocén
<g/>
)	)	kIx)	)
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
karů	kar	k1gInPc2	kar
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
(	(	kIx(	(
<g/>
Lac	Lac	k1gFnSc1	Lac
du	du	k?	du
Rotondo	Rotondo	k6eAd1	Rotondo
<g/>
,	,	kIx,	,
Nino	Nina	k1gFnSc5	Nina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
Korsické	korsický	k2eAgFnPc1d1	Korsická
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
je	být	k5eAaImIp3nS	být
orientováno	orientovat	k5eAaBmNgNnS	orientovat
SSZ-JJV	SSZ-JJV	k1gFnSc7	SSZ-JJV
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
hřebeny	hřeben	k1gInPc1	hřeben
stojí	stát	k5eAaImIp3nP	stát
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
ose	osa	k1gFnSc3	osa
příčně	příčna	k1gFnSc3	příčna
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
hor	hora	k1gFnPc2	hora
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
fyzicky	fyzicky	k6eAd1	fyzicky
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
směru	směr	k1gInSc6	směr
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
široká	široký	k2eAgNnPc1d1	široké
a	a	k8xC	a
hluboká	hluboký	k2eAgNnPc1d1	hluboké
sedla	sedlo	k1gNnPc1	sedlo
<g/>
,	,	kIx,	,
oddělující	oddělující	k2eAgFnPc1d1	oddělující
dílčí	dílčí	k2eAgFnPc4d1	dílčí
horské	horský	k2eAgFnPc4d1	horská
skupiny	skupina	k1gFnPc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
ně	on	k3xPp3gInPc4	on
vede	vést	k5eAaImIp3nS	vést
čára	čára	k1gFnSc1	čára
rozvodí	rozvodí	k1gNnSc2	rozvodí
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
silnice	silnice	k1gFnSc2	silnice
(	(	kIx(	(
<g/>
Col	cola	k1gFnPc2	cola
de	de	k?	de
Vergio	Vergio	k1gMnSc1	Vergio
<g/>
,	,	kIx,	,
Vizzavona	Vizzavona	k1gFnSc1	Vizzavona
<g/>
,	,	kIx,	,
Verda	Verda	k1gFnSc1	Verda
<g/>
,	,	kIx,	,
Bavella	Bavella	k1gFnSc1	Bavella
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
správní	správní	k2eAgFnPc4d1	správní
oblasti	oblast	k1gFnPc4	oblast
-	-	kIx~	-
départementy	département	k1gInPc4	département
<g/>
::	::	k?	::
Corse-du-Sud	Corseu-Sud	k1gInSc4	Corse-du-Sud
-	-	kIx~	-
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
se	s	k7c7	s
správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
Ajaccio	Ajaccio	k1gNnSc1	Ajaccio
(	(	kIx(	(
<g/>
poznávací	poznávací	k2eAgFnSc1d1	poznávací
značka	značka	k1gFnSc1	značka
vozidel	vozidlo	k1gNnPc2	vozidlo
2	[number]	k4	2
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
20	[number]	k4	20
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
Haute-Corse	Haute-Corse	k1gFnSc1	Haute-Corse
-	-	kIx~	-
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
části	část	k1gFnSc2	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
sprácní	sprácní	k2eAgNnSc1d1	sprácní
středisko	středisko	k1gNnSc1	středisko
Bastia	Bastia	k1gFnSc1	Bastia
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
přístav	přístav	k1gInSc1	přístav
Korsiky	Korsika	k1gFnSc2	Korsika
(	(	kIx(	(
<g/>
poznávací	poznávací	k2eAgFnSc1d1	poznávací
značka	značka	k1gFnSc1	značka
vozidel	vozidlo	k1gNnPc2	vozidlo
2	[number]	k4	2
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
20	[number]	k4	20
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
Na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
podnebí	podnebí	k1gNnSc1	podnebí
dost	dost	k6eAd1	dost
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
16	[number]	k4	16
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
pod	pod	k7c7	pod
nulou	nula	k1gFnSc7	nula
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
lze	lze	k6eAd1	lze
i	i	k9	i
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
zažít	zažít	k5eAaPmF	zažít
pod	pod	k7c7	pod
horou	hora	k1gFnSc7	hora
Monte	Mont	k1gInSc5	Mont
Cinto	cinta	k1gFnSc5	cinta
vydatnou	vydatný	k2eAgFnSc4d1	vydatná
sněhovou	sněhový	k2eAgFnSc4d1	sněhová
nadílku	nadílka	k1gFnSc4	nadílka
<g/>
.	.	kIx.	.
</s>
<s>
Místy	místo	k1gNnPc7	místo
se	se	k3xPyFc4	se
sníh	sníh	k1gInSc1	sníh
drží	držet	k5eAaImIp3nS	držet
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
Lac	Lac	k1gFnSc2	Lac
du	du	k?	du
Rotondo	Rotondo	k6eAd1	Rotondo
svírá	svírat	k5eAaImIp3nS	svírat
často	často	k6eAd1	často
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
ledový	ledový	k2eAgInSc1d1	ledový
prstenec	prstenec	k1gInSc1	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
dvoutisícovkou	dvoutisícovka	k1gFnSc7	dvoutisícovka
Monte	Mont	k1gInSc5	Mont
Incudine	Incudin	k1gInSc5	Incudin
je	on	k3xPp3gMnPc4	on
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
možné	možný	k2eAgNnSc1d1	možné
ráno	ráno	k1gNnSc1	ráno
chodit	chodit	k5eAaImF	chodit
po	po	k7c6	po
jinovatce	jinovatka	k1gFnSc6	jinovatka
a	a	k8xC	a
za	za	k7c4	za
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyhřívat	vyhřívat	k5eAaImF	vyhřívat
při	při	k7c6	při
32	[number]	k4	32
°	°	k?	°
<g/>
C.	C.	kA	C.
Srážky	srážka	k1gFnSc2	srážka
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
běžných	běžný	k2eAgInPc2d1	běžný
1	[number]	k4	1
500	[number]	k4	500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholech	vrchol	k1gInPc6	vrchol
i	i	k8xC	i
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
za	za	k7c2	za
pěkného	pěkný	k2eAgNnSc2d1	pěkné
počasí	počasí	k1gNnSc2	počasí
dost	dost	k6eAd1	dost
fouká	foukat	k5eAaImIp3nS	foukat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
východu	východ	k1gInSc2	východ
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
vane	vanout	k5eAaImIp3nS	vanout
vítr	vítr	k1gInSc1	vítr
Lavante	Lavant	k1gMnSc5	Lavant
<g/>
,	,	kIx,	,
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Scirocco	scirocco	k1gNnSc1	scirocco
a	a	k8xC	a
až	až	k9	až
stokilometrovou	stokilometrový	k2eAgFnSc7d1	stokilometrová
rychlostí	rychlost	k1gFnSc7	rychlost
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
na	na	k7c6	na
východních	východní	k2eAgInPc6d1	východní
svazích	svah	k1gInPc6	svah
padavé	padavý	k2eAgInPc1d1	padavý
větry	vítr	k1gInPc1	vítr
Libeccio	Libeccio	k6eAd1	Libeccio
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
pro	pro	k7c4	pro
pobyt	pobyt	k1gInSc4	pobyt
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
východní	východní	k2eAgNnSc4d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
pak	pak	k6eAd1	pak
okolí	okolí	k1gNnSc2	okolí
města	město	k1gNnSc2	město
Porto-Vecchio	Porto-Vecchio	k1gMnSc1	Porto-Vecchio
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
je	být	k5eAaImIp3nS	být
proslulé	proslulý	k2eAgInPc1d1	proslulý
písčitými	písčitý	k2eAgFnPc7d1	písčitá
plážemi	pláž	k1gFnPc7	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
cílem	cíl	k1gInSc7	cíl
pro	pro	k7c4	pro
pěší	pěší	k2eAgFnSc4d1	pěší
turistiku	turistika	k1gFnSc4	turistika
jsou	být	k5eAaImIp3nP	být
kaskády	kaskáda	k1gFnPc1	kaskáda
(	(	kIx(	(
<g/>
Cascades	Cascades	k1gInSc1	Cascades
du	du	k?	du
Polischellu	Polischell	k1gInSc2	Polischell
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
korsických	korsický	k2eAgFnPc6d1	Korsická
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejkrásnější	krásný	k2eAgFnSc4d3	nejkrásnější
hřebenovou	hřebenový	k2eAgFnSc4d1	hřebenová
túru	túra	k1gFnSc4	túra
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
pokládána	pokládán	k2eAgFnSc1d1	pokládána
horská	horský	k2eAgFnSc1d1	horská
třítýdenní	třítýdenní	k2eAgFnSc1d1	třítýdenní
trasa	trasa	k1gFnSc1	trasa
GR	GR	kA	GR
20	[number]	k4	20
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
protíná	protínat	k5eAaImIp3nS	protínat
celý	celý	k2eAgInSc4d1	celý
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
jsou	být	k5eAaImIp3nP	být
Janovské	Janovské	k2eAgFnPc1d1	Janovské
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
zříceniny	zřícenina	k1gFnPc1	zřícenina
strážních	strážní	k2eAgFnPc2d1	strážní
věží	věž	k1gFnPc2	věž
postavených	postavený	k2eAgInPc2d1	postavený
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
proti	proti	k7c3	proti
pirátům	pirát	k1gMnPc3	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Korsika	Korsika	k1gFnSc1	Korsika
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
mezi	mezi	k7c7	mezi
jachtaři	jachtař	k1gMnPc7	jachtař
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
pobřeží	pobřeží	k1gNnSc1	pobřeží
obrácené	obrácený	k2eAgNnSc1d1	obrácené
do	do	k7c2	do
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
od	od	k7c2	od
posádky	posádka	k1gFnSc2	posádka
velké	velký	k2eAgFnSc2d1	velká
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
východní	východní	k2eAgNnSc1d1	východní
pobřeží	pobřeží	k1gNnSc1	pobřeží
otočené	otočený	k2eAgNnSc1d1	otočené
k	k	k7c3	k
Tyrhénskému	tyrhénský	k2eAgNnSc3d1	Tyrhénské
moři	moře	k1gNnSc3	moře
je	být	k5eAaImIp3nS	být
klidnější	klidný	k2eAgMnSc1d2	klidnější
a	a	k8xC	a
bezpečnější	bezpečný	k2eAgMnSc1d2	bezpečnější
<g/>
.	.	kIx.	.
</s>
<s>
Výpravy	výprava	k1gFnPc1	výprava
na	na	k7c6	na
mořských	mořský	k2eAgInPc6d1	mořský
kajacích	kajak	k1gInPc6	kajak
směřují	směřovat	k5eAaImIp3nP	směřovat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
podél	podél	k7c2	podél
zajímavějšího	zajímavý	k2eAgNnSc2d2	zajímavější
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
se	se	k3xPyFc4	se
v	v	k7c6	v
korsickém	korsický	k2eAgNnSc6d1	korsické
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
splouvají	splouvat	k5eAaImIp3nP	splouvat
velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgFnPc1d1	náročná
řeky	řeka	k1gFnPc1	řeka
na	na	k7c6	na
raftech	raft	k1gInPc6	raft
a	a	k8xC	a
kajacích	kajak	k1gInPc6	kajak
<g/>
.	.	kIx.	.
</s>
<s>
Hornaté	hornatý	k2eAgNnSc1d1	hornaté
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
i	i	k8xC	i
pobřeží	pobřeží	k1gNnSc1	pobřeží
Korsiky	Korsika	k1gFnSc2	Korsika
je	být	k5eAaImIp3nS	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
jarní	jarní	k2eAgFnSc7d1	jarní
destinací	destinace	k1gFnSc7	destinace
pro	pro	k7c4	pro
silniční	silniční	k2eAgFnSc4d1	silniční
cyklistiku	cyklistika	k1gFnSc4	cyklistika
<g/>
.	.	kIx.	.
</s>
<s>
Náročná	náročný	k2eAgFnSc1d1	náročná
cykloturistická	cykloturistický	k2eAgFnSc1d1	cykloturistická
trasa	trasa	k1gFnSc1	trasa
vede	vést	k5eAaImIp3nS	vést
okolo	okolo	k7c2	okolo
celého	celý	k2eAgInSc2d1	celý
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
tisíc	tisíc	k4xCgInSc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
objevení	objevení	k1gNnSc2	objevení
hor	hora	k1gFnPc2	hora
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jmenovat	jmenovat	k5eAaBmF	jmenovat
horolezce	horolezec	k1gMnPc4	horolezec
Tucketta	Tuckett	k1gInSc2	Tuckett
<g/>
,	,	kIx,	,
Freshfielda	Freshfielda	k1gMnSc1	Freshfielda
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Horolezecky	horolezecky	k6eAd1	horolezecky
oblast	oblast	k1gFnSc4	oblast
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
a	a	k8xC	a
1904	[number]	k4	1904
stuttgartský	stuttgartský	k2eAgMnSc1d1	stuttgartský
doktor	doktor	k1gMnSc1	doktor
Felix	Felix	k1gMnSc1	Felix
von	von	k1gInSc4	von
Cube	Cub	k1gInSc2	Cub
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
mapu	mapa	k1gFnSc4	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Plateau	Platea	k2eAgFnSc4d1	Platea
de	de	k?	de
Stagnu	Stagna	k1gFnSc4	Stagna
umístěna	umístit	k5eAaPmNgFnS	umístit
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
Regionální	regionální	k2eAgInSc1d1	regionální
přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
Korsika	Korsika	k1gFnSc1	Korsika
(	(	kIx(	(
<g/>
Parc	Parc	k1gFnSc1	Parc
Natural	Natural	k?	Natural
Regional	Regional	k1gFnPc2	Regional
de	de	k?	de
la	la	k1gNnSc4	la
Corse	Corse	k1gFnSc2	Corse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
skoro	skoro	k6eAd1	skoro
40	[number]	k4	40
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vede	vést	k5eAaImIp3nS	vést
na	na	k7c6	na
Korsice	Korsika	k1gFnSc6	Korsika
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
horolezeckých	horolezecký	k2eAgFnPc2d1	horolezecká
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgNnSc1d2	kratší
sportovní	sportovní	k2eAgNnSc1d1	sportovní
lezení	lezení	k1gNnSc1	lezení
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
i	i	k8xC	i
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
klasických	klasický	k2eAgFnPc2d1	klasická
cest	cesta	k1gFnPc2	cesta
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
vícedélkové	vícedélkový	k2eAgNnSc4d1	vícedélkový
lezení	lezení	k1gNnSc4	lezení
ve	v	k7c6	v
vnitrozemských	vnitrozemský	k2eAgFnPc6d1	vnitrozemská
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
zapsali	zapsat	k5eAaPmAgMnP	zapsat
při	při	k7c6	při
výstupech	výstup	k1gInPc6	výstup
na	na	k7c4	na
místní	místní	k2eAgInPc4d1	místní
vrcholy	vrchol	k1gInPc4	vrchol
také	také	k9	také
čeští	český	k2eAgMnPc1d1	český
horolezci	horolezec	k1gMnPc1	horolezec
z	z	k7c2	z
Jablonce	Jablonec	k1gInSc2	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
(	(	kIx(	(
<g/>
Simm	Simm	k1gMnSc1	Simm
<g/>
,	,	kIx,	,
Skořepa	Skořepa	k1gMnSc1	Skořepa
<g/>
,	,	kIx,	,
Deml	Deml	k1gMnSc1	Deml
a	a	k8xC	a
Skrbek	Skrbek	k1gMnSc1	Skrbek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
320	[number]	k4	320
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Bastia	Bastium	k1gNnSc2	Bastium
a	a	k8xC	a
Ajaccio	Ajaccio	k6eAd1	Ajaccio
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
drsným	drsný	k2eAgFnPc3d1	drsná
horským	horský	k2eAgFnPc3d1	horská
podmínkám	podmínka	k1gFnPc3	podmínka
patří	patřit	k5eAaImIp3nS	patřit
Korsika	Korsika	k1gFnSc1	Korsika
k	k	k7c3	k
oblastem	oblast	k1gFnPc3	oblast
s	s	k7c7	s
nejmenším	malý	k2eAgNnSc7d3	nejmenší
zalidněním	zalidnění	k1gNnSc7	zalidnění
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
generace	generace	k1gFnSc1	generace
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
pracovních	pracovní	k2eAgFnPc2d1	pracovní
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Korsice	Korsika	k1gFnSc6	Korsika
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
rybářských	rybářský	k2eAgFnPc2d1	rybářská
vesniček	vesnička	k1gFnPc2	vesnička
-	-	kIx~	-
rybaření	rybařený	k2eAgMnPc1d1	rybařený
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
hlavní	hlavní	k2eAgFnSc1d1	hlavní
obživa	obživa	k1gFnSc1	obživa
tamějších	tamější	k2eAgMnPc2d1	tamější
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
ostrova	ostrov	k1gInSc2	ostrov
byly	být	k5eAaImAgFnP	být
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
velmi	velmi	k6eAd1	velmi
rušné	rušný	k2eAgFnPc1d1	rušná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Říma	Řím	k1gInSc2	Řím
přes	přes	k7c4	přes
Saracény	Saracén	k1gMnPc4	Saracén
<g/>
,	,	kIx,	,
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
okupaci	okupace	k1gFnSc4	okupace
Janovem	Janov	k1gInSc7	Janov
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
samostatnosti	samostatnost	k1gFnSc2	samostatnost
až	až	k9	až
po	po	k7c4	po
jeho	jeho	k3xOp3gNnSc4	jeho
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Obtížně	obtížně	k6eAd1	obtížně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
hory	hora	k1gFnPc1	hora
byly	být	k5eAaImAgFnP	být
hrdým	hrdý	k2eAgInSc7d1	hrdý
Korsičanům	Korsičan	k1gMnPc3	Korsičan
vždy	vždy	k6eAd1	vždy
dobrým	dobrý	k2eAgInSc7d1	dobrý
domovem	domov	k1gInSc7	domov
a	a	k8xC	a
macchie	macchie	k1gFnSc2	macchie
laskavou	laskavý	k2eAgFnSc7d1	laskavá
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
<g/>
"	"	kIx"	"
ostrova	ostrov	k1gInSc2	ostrov
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
useknutou	useknutý	k2eAgFnSc4d1	useknutá
hlavu	hlava	k1gFnSc4	hlava
Saracéna	Saracén	k1gMnSc2	Saracén
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
ukazovali	ukazovat	k5eAaImAgMnP	ukazovat
Korsičané	Korsičan	k1gMnPc1	Korsičan
hlavu	hlava	k1gFnSc4	hlava
velitele	velitel	k1gMnSc2	velitel
na	na	k7c6	na
kůlu	kůl	k1gInSc6	kůl
proti	proti	k7c3	proti
bílé	bílý	k2eAgFnSc3d1	bílá
plachtě	plachta	k1gFnSc3	plachta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
odradili	odradit	k5eAaPmAgMnP	odradit
od	od	k7c2	od
útoku	útok	k1gInSc2	útok
další	další	k2eAgMnPc4d1	další
nájezdníky	nájezdník	k1gMnPc4	nájezdník
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
tohoto	tento	k3xDgInSc2	tento
symbolu	symbol	k1gInSc2	symbol
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
až	až	k9	až
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
novověku	novověk	k1gInSc2	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Korsika	Korsika	k1gFnSc1	Korsika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Korsika	Korsika	k1gFnSc1	Korsika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
