<s>
B	B	kA	B
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
písmeno	písmeno	k1gNnSc1	písmeno
latinské	latinský	k2eAgFnSc2d1	Latinská
i	i	k8xC	i
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
řazeno	řadit	k5eAaImNgNnS	řadit
podle	podle	k7c2	podle
pořadí	pořadí	k1gNnSc2	pořadí
v	v	k7c6	v
Unicode	Unicod	k1gInSc5	Unicod
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
B	B	kA	B
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
B	B	kA	B
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
B	B	kA	B
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
B	B	kA	B
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
