<p>
<s>
Říše	říše	k1gFnSc1	říše
slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc1d1	americké
válečné	válečný	k2eAgNnSc1d1	válečné
filmové	filmový	k2eAgNnSc1d1	filmové
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
natočené	natočený	k2eAgNnSc1d1	natočené
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
autobiografické	autobiografický	k2eAgFnSc2d1	autobiografická
novely	novela	k1gFnSc2	novela
J.	J.	kA	J.
G.	G.	kA	G.
Ballarda	Ballard	k1gMnSc2	Ballard
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
jsou	být	k5eAaImIp3nP	být
Christian	Christian	k1gMnSc1	Christian
Bale	bal	k1gInSc5	bal
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
<g/>
,	,	kIx,	,
Miranda	Miranda	k1gFnSc1	Miranda
Richardson	Richardsona	k1gFnPc2	Richardsona
a	a	k8xC	a
Nigel	Nigela	k1gFnPc2	Nigela
Havers	Haversa	k1gFnPc2	Haversa
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
o	o	k7c4	o
Jamie	Jamie	k1gFnPc4	Jamie
"	"	kIx"	"
<g/>
Jimu	Jim	k2eAgFnSc4d1	Jima
<g/>
"	"	kIx"	"
Grahamovi	Graham	k1gMnSc6	Graham
<g/>
,	,	kIx,	,
mladém	mladý	k2eAgMnSc6d1	mladý
chlapci	chlapec	k1gMnSc6	chlapec
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
britské	britský	k2eAgFnSc2d1	britská
rodiny	rodina	k1gFnSc2	rodina
žijící	žijící	k2eAgFnSc2d1	žijící
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
vězněm	vězeň	k1gMnSc7	vězeň
japonského	japonský	k2eAgInSc2d1	japonský
zajateckého	zajatecký	k2eAgInSc2d1	zajatecký
tábora	tábor	k1gInSc2	tábor
Lunghua	Lunghu	k1gInSc2	Lunghu
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Japonské	japonský	k2eAgNnSc1d1	Japonské
císařství	císařství	k1gNnSc1	císařství
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
1937	[number]	k4	1937
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
žije	žít	k5eAaImIp3nS	žít
kluk	kluk	k1gMnSc1	kluk
jménem	jméno	k1gNnSc7	jméno
Jamie	Jamie	k1gFnSc2	Jamie
Graham	Graham	k1gMnSc1	Graham
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
školák	školák	k1gMnSc1	školák
vyšší	vysoký	k2eAgFnSc2d2	vyšší
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
užívá	užívat	k5eAaImIp3nS	užívat
dětství	dětství	k1gNnSc4	dětství
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
<g/>
.	.	kIx.	.
</s>
<s>
Brzo	brzo	k6eAd1	brzo
však	však	k9	však
vypuknou	vypuknout	k5eAaPmIp3nP	vypuknout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nepokoje	nepokoj	k1gInSc2	nepokoj
a	a	k8xC	a
Jamie	Jamie	k1gFnSc1	Jamie
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
oddělen	oddělen	k2eAgInSc1d1	oddělen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
"	"	kIx"	"
<g/>
cizí	cizí	k2eAgFnSc6d1	cizí
<g/>
"	"	kIx"	"
zemi	zem	k1gFnSc6	zem
plné	plný	k2eAgFnSc6d1	plná
japonských	japonský	k2eAgMnPc2d1	japonský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
znalostí	znalost	k1gFnPc2	znalost
japonštiny	japonština	k1gFnSc2	japonština
se	se	k3xPyFc4	se
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
Basiem	Basius	k1gMnSc7	Basius
<g/>
,	,	kIx,	,
americkým	americký	k2eAgMnSc7d1	americký
prodavačem	prodavač	k1gMnSc7	prodavač
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ho	on	k3xPp3gMnSc4	on
pojmenuje	pojmenovat	k5eAaPmIp3nS	pojmenovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Jim	on	k3xPp3gMnPc3	on
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
zajatci	zajatec	k1gMnPc1	zajatec
zajateckého	zajatecký	k2eAgInSc2d1	zajatecký
tábora	tábor	k1gInSc2	tábor
Lunghua	Lunghu	k2eAgMnSc4d1	Lunghu
v	v	k7c4	v
Su-čou	Su-čá	k1gFnSc4	Su-čá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
Pacifické	pacifický	k2eAgFnSc2d1	Pacifická
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Jim	on	k3xPp3gMnPc3	on
táboru	tábor	k1gMnSc6	tábor
zcela	zcela	k6eAd1	zcela
přizpůsobí	přizpůsobit	k5eAaPmIp3nS	přizpůsobit
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
své	svůj	k3xOyFgFnPc4	svůj
kondice	kondice	k1gFnPc4	kondice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
krást	krást	k5eAaImF	krást
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
poté	poté	k6eAd1	poté
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
dále	daleko	k6eAd2	daleko
obchodovat	obchodovat	k5eAaImF	obchodovat
<g/>
.	.	kIx.	.
</s>
<s>
Věci	věc	k1gFnPc1	věc
však	však	k9	však
patří	patřit	k5eAaImIp3nP	patřit
i	i	k8xC	i
seržantu	seržant	k1gMnSc3	seržant
Nagatovi	Nagat	k1gMnSc3	Nagat
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
tábor	tábor	k1gInSc4	tábor
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britský	britský	k2eAgMnSc1d1	britský
doktor	doktor	k1gMnSc1	doktor
Rawlins	Rawlinsa	k1gFnPc2	Rawlinsa
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
naučit	naučit	k5eAaPmF	naučit
Jima	Jimus	k1gMnSc4	Jimus
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
jiné	jiný	k2eAgInPc4d1	jiný
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
velkou	velký	k2eAgFnSc4d1	velká
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
letadlech	letadlo	k1gNnPc6	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
vězení	vězení	k1gNnSc2	vězení
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
japonským	japonský	k2eAgMnSc7d1	japonský
chlapcem	chlapec	k1gMnSc7	chlapec
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
podá	podat	k5eAaPmIp3nS	podat
model	model	k1gInSc1	model
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
omylem	omylem	k6eAd1	omylem
hodil	hodit	k5eAaPmAgMnS	hodit
přes	přes	k7c4	přes
hranice	hranice	k1gFnPc4	hranice
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
stále	stále	k6eAd1	stále
sympatizuje	sympatizovat	k5eAaImIp3nS	sympatizovat
s	s	k7c7	s
Basiem	Basius	k1gMnSc7	Basius
a	a	k8xC	a
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
ubytovny	ubytovna	k1gFnSc2	ubytovna
<g/>
.	.	kIx.	.
</s>
<s>
Basie	Basie	k1gFnSc1	Basie
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
kolegové	kolega	k1gMnPc1	kolega
se	se	k3xPyFc4	se
hádají	hádat	k5eAaImIp3nP	hádat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
jsou	být	k5eAaImIp3nP	být
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
tábora	tábor	k1gInSc2	tábor
miny	mina	k1gFnSc2	mina
a	a	k8xC	a
tak	tak	k6eAd1	tak
tam	tam	k6eAd1	tam
posílají	posílat	k5eAaImIp3nP	posílat
Jima	Jimum	k1gNnPc1	Jimum
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
nastavit	nastavit	k5eAaPmF	nastavit
pasti	past	k1gFnPc4	past
na	na	k7c4	na
krocany	krocan	k1gMnPc4	krocan
za	za	k7c4	za
území	území	k1gNnSc4	území
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
možnost	možnost	k1gFnSc4	možnost
bydlet	bydlet	k5eAaImF	bydlet
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
ubytovně	ubytovna	k1gFnSc6	ubytovna
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pomoci	pomoc	k1gFnSc3	pomoc
japonského	japonský	k2eAgMnSc2d1	japonský
chlapce	chlapec	k1gMnSc2	chlapec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
úkol	úkol	k1gInSc1	úkol
podaří	podařit	k5eAaPmIp3nS	podařit
udělat	udělat	k5eAaPmF	udělat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
přestěhuje	přestěhovat	k5eAaPmIp3nS	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Basie	Basie	k1gFnSc1	Basie
plánuje	plánovat	k5eAaImIp3nS	plánovat
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nagata	Nagata	k1gFnSc1	Nagata
udělá	udělat	k5eAaPmIp3nS	udělat
prohlídku	prohlídka	k1gFnSc4	prohlídka
Basieho	Basie	k1gMnSc2	Basie
ubytovny	ubytovna	k1gFnSc2	ubytovna
a	a	k8xC	a
nalezne	nalézt	k5eAaBmIp3nS	nalézt
mýdlo	mýdlo	k1gNnSc4	mýdlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
předtím	předtím	k6eAd1	předtím
Jim	on	k3xPp3gMnPc3	on
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
<g/>
.	.	kIx.	.
</s>
<s>
Nagaty	Nagata	k1gFnPc1	Nagata
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
Basie	Basie	k1gFnSc1	Basie
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
zmlátí	zmlátit	k5eAaPmIp3nS	zmlátit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
uzdravení	uzdravení	k1gNnSc6	uzdravení
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
ubytovnu	ubytovna	k1gFnSc4	ubytovna
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
jeho	jeho	k3xOp3gFnPc1	jeho
věci	věc	k1gFnPc1	věc
jsou	být	k5eAaImIp3nP	být
rozkradeny	rozkraden	k2eAgFnPc1d1	rozkradena
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
tábor	tábor	k1gInSc1	tábor
pod	pod	k7c7	pod
útokem	útok	k1gInSc7	útok
skupiny	skupina	k1gFnSc2	skupina
amerických	americký	k2eAgFnPc2d1	americká
P-51	P-51	k1gFnPc2	P-51
Mustangů	mustang	k1gMnPc2	mustang
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tábor	tábor	k1gInSc1	tábor
přemístí	přemístit	k5eAaPmIp3nS	přemístit
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Noc	noc	k1gFnSc1	noc
předtím	předtím	k6eAd1	předtím
prchá	prchat	k5eAaImIp3nS	prchat
Basie	Basie	k1gFnPc4	Basie
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jimu	Jima	k1gFnSc4	Jima
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
vezme	vzít	k5eAaPmIp3nS	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pochodu	pochod	k1gInSc2	pochod
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
vězňů	vězeň	k1gMnPc2	vězeň
rapidně	rapidně	k6eAd1	rapidně
sníží	snížit	k5eAaPmIp3nS	snížit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
,	,	kIx,	,
vyhladovění	vyhladovění	k1gNnSc2	vyhladovění
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc2	nedostatek
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
krátké	krátký	k2eAgFnSc2d1	krátká
zastávky	zastávka	k1gFnSc2	zastávka
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
se	se	k3xPyFc4	se
Jim	on	k3xPp3gMnPc3	on
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Nechá	nechat	k5eAaPmIp3nS	nechat
svou	svůj	k3xOyFgFnSc4	svůj
skupinu	skupina	k1gFnSc4	skupina
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Su-čou	Su-ča	k1gMnSc7	Su-ča
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vyčerpávající	vyčerpávající	k2eAgFnSc2d1	vyčerpávající
cesty	cesta	k1gFnSc2	cesta
vidí	vidět	k5eAaImIp3nS	vidět
záblesk	záblesk	k1gInSc4	záblesk
jaderné	jaderný	k2eAgFnSc2d1	jaderná
bomby	bomba	k1gFnSc2	bomba
shozené	shozený	k2eAgFnSc2d1	shozená
na	na	k7c4	na
tisíce	tisíc	k4xCgInPc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
Nagasaki	Nagasaki	k1gNnSc4	Nagasaki
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
definitivně	definitivně	k6eAd1	definitivně
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
japonskou	japonský	k2eAgFnSc4d1	japonská
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
náletu	nálet	k1gInSc6	nálet
"	"	kIx"	"
<g/>
záchranných	záchranný	k2eAgFnPc2d1	záchranná
bomb	bomba	k1gFnPc2	bomba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vodu	voda	k1gFnSc4	voda
i	i	k9	i
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Jim	on	k3xPp3gMnPc3	on
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
Su-čou	Su-čá	k1gFnSc4	Su-čá
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkává	potkávat	k5eAaImIp3nS	potkávat
svého	svůj	k3xOyFgMnSc4	svůj
japonského	japonský	k2eAgMnSc4d1	japonský
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
mango	mango	k1gNnSc4	mango
a	a	k8xC	a
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gNnSc4	on
rozkrojí	rozkrojit	k5eAaPmIp3nS	rozkrojit
katanou	kataná	k1gFnSc4	kataná
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
Basie	Basie	k1gFnSc1	Basie
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
americkou	americký	k2eAgFnSc7d1	americká
bandou	banda	k1gFnSc7	banda
a	a	k8xC	a
sbírá	sbírat	k5eAaImIp3nS	sbírat
zbytky	zbytek	k1gInPc7	zbytek
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
bomby	bomba	k1gFnPc4	bomba
nabízejí	nabízet	k5eAaImIp3nP	nabízet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zahlédnou	zahlédnout	k5eAaPmIp3nP	zahlédnout
Jima	Jimus	k1gMnSc4	Jimus
s	s	k7c7	s
japonským	japonský	k2eAgMnSc7d1	japonský
klukem	kluk	k1gMnSc7	kluk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
drží	držet	k5eAaImIp3nS	držet
katanu	katan	k1gMnSc3	katan
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
ho	on	k3xPp3gMnSc4	on
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
začne	začít	k5eAaPmIp3nS	začít
mlátit	mlátit	k5eAaImF	mlátit
Američana	Američan	k1gMnSc4	Američan
a	a	k8xC	a
Basie	Basie	k1gFnSc1	Basie
ho	on	k3xPp3gMnSc4	on
musí	muset	k5eAaImIp3nS	muset
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Slibuje	slibovat	k5eAaImIp3nS	slibovat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
najít	najít	k5eAaPmF	najít
jeho	jeho	k3xOp3gMnPc4	jeho
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
však	však	k9	však
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
zachráněn	zachránit	k5eAaPmNgInS	zachránit
americkými	americký	k2eAgMnPc7d1	americký
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
přemístěn	přemístit	k5eAaPmNgMnS	přemístit
do	do	k7c2	do
sirotčince	sirotčinec	k1gInSc2	sirotčinec
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
ztratili	ztratit	k5eAaPmAgMnP	ztratit
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
přichází	přicházet	k5eAaImIp3nP	přicházet
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
Jim	on	k3xPp3gMnPc3	on
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
již	již	k6eAd1	již
nepozná	poznat	k5eNaPmIp3nS	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
upadá	upadat	k5eAaPmIp3nS	upadat
do	do	k7c2	do
matčiny	matčin	k2eAgFnSc2d1	matčina
náruče	náruč	k1gFnSc2	náruč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Menší	malý	k2eAgFnPc1d2	menší
role	role	k1gFnPc1	role
===	===	k?	===
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
hlavní	hlavní	k2eAgFnSc2d1	hlavní
herců	herc	k1gInPc2	herc
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
také	také	k6eAd1	také
objevili	objevit	k5eAaPmAgMnP	objevit
Leslie	Leslie	k1gFnSc2	Leslie
Phillips	Phillips	k1gInSc1	Phillips
<g/>
,	,	kIx,	,
Burt	Burt	k1gMnSc1	Burt
Kwouk	Kwouk	k1gMnSc1	Kwouk
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
McGann	McGann	k1gMnSc1	McGann
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnSc1	Joe
Pantoliano	Pantoliana	k1gFnSc5	Pantoliana
a	a	k8xC	a
Ben	Ben	k1gInSc1	Ben
Stiller	Stiller	k1gInSc1	Stiller
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Masatō	Masatō	k1gFnPc7	Masatō
Ibu	Ibu	k1gFnPc2	Ibu
a	a	k8xC	a
Guts	Gutsa	k1gFnPc2	Gutsa
Ishimatsu	Ishimats	k1gInSc2	Ishimats
jako	jako	k9	jako
japonští	japonský	k2eAgMnPc1d1	japonský
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Produkce	produkce	k1gFnSc1	produkce
==	==	k?	==
</s>
</p>
<p>
<s>
Warner	Warner	k1gInSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
zakoupila	zakoupit	k5eAaPmAgNnP	zakoupit
filmová	filmový	k2eAgNnPc1d1	filmové
práva	právo	k1gNnPc1	právo
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
obsadit	obsadit	k5eAaPmF	obsadit
Harolda	Harold	k1gMnSc2	Harold
Beckera	Becker	k1gMnSc2	Becker
jako	jako	k8xS	jako
režiséra	režisér	k1gMnSc2	režisér
a	a	k8xC	a
Roberta	Robert	k1gMnSc2	Robert
Shapira	Shapir	k1gMnSc2	Shapir
jako	jako	k8xS	jako
producenta	producent	k1gMnSc2	producent
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
Stoppad	Stoppad	k1gInSc4	Stoppad
napsal	napsat	k5eAaBmAgMnS	napsat
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
i	i	k9	i
J.	J.	kA	J.
G.	G.	kA	G.
Ballard	Ballard	k1gMnSc1	Ballard
<g/>
.	.	kIx.	.
</s>
<s>
Becker	Becker	k1gMnSc1	Becker
však	však	k9	však
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
postu	post	k1gInSc2	post
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
David	David	k1gMnSc1	David
Lean	Lean	k1gMnSc1	Lean
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Spielbergem	Spielberg	k1gMnSc7	Spielberg
přišli	přijít	k5eAaPmAgMnP	přijít
jako	jako	k9	jako
další	další	k2eAgMnPc1d1	další
producenti	producent	k1gMnPc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Lean	Lean	k1gMnSc1	Lean
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Pracoval	pracovat	k5eAaImAgInS	pracovat
jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
asi	asi	k9	asi
rok	rok	k1gInSc4	rok
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
přišlo	přijít	k5eAaPmAgNnS	přijít
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k9	jako
deník	deník	k1gInSc4	deník
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
dobře	dobře	k6eAd1	dobře
napsané	napsaný	k2eAgNnSc1d1	napsané
a	a	k8xC	a
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přenechal	přenechat	k5eAaPmAgMnS	přenechat
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
Stevovi	Steve	k1gMnSc3	Steve
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Spielberg	Spielberg	k1gMnSc1	Spielberg
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Od	od	k7c2	od
momentu	moment	k1gInSc2	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
přečetl	přečíst	k5eAaPmAgMnS	přečíst
novelu	novela	k1gFnSc4	novela
od	od	k7c2	od
J.	J.	kA	J.
G.	G.	kA	G.
Ballarda	Ballard	k1gMnSc2	Ballard
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
tajně	tajně	k6eAd1	tajně
chtěl	chtít	k5eAaImAgMnS	chtít
film	film	k1gInSc4	film
natočit	natočit	k5eAaBmF	natočit
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Spielberg	Spielberg	k1gMnSc1	Spielberg
vzal	vzít	k5eAaPmAgMnS	vzít
natáčení	natáčení	k1gNnSc3	natáčení
filmu	film	k1gInSc2	film
velmi	velmi	k6eAd1	velmi
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gInSc4	jeho
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
Leanův	Leanův	k2eAgInSc4d1	Leanův
film	film	k1gInSc4	film
Most	most	k1gInSc1	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Kwai	Kwa	k1gFnSc2	Kwa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
podobném	podobný	k2eAgInSc6d1	podobný
japonském	japonský	k2eAgInSc6d1	japonský
zajateckým	zajatecký	k2eAgFnPc3d1	zajatecká
táboře	tábor	k1gInSc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Spielbergovy	Spielbergův	k2eAgFnPc4d1	Spielbergova
sympatie	sympatie	k1gFnPc4	sympatie
ke	k	k7c3	k
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
letectvu	letectvo	k1gNnSc6	letectvo
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vyvolal	vyvolat	k5eAaPmAgMnS	vyvolat
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
jeho	jeho	k3xOp3gFnPc4	jeho
historky	historka	k1gFnPc4	historka
ze	z	k7c2	z
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
zažil	zažít	k5eAaPmAgMnS	zažít
jako	jako	k8xC	jako
radioman	radioman	k1gMnSc1	radioman
letadla	letadlo	k1gNnSc2	letadlo
B-25	B-25	k1gMnSc1	B-25
Mitchell	Mitchell	k1gMnSc1	Mitchell
v	v	k7c4	v
China-Burma	China-Burma	k1gNnSc4	China-Burma
Theater	Theatra	k1gFnPc2	Theatra
<g/>
.	.	kIx.	.
</s>
<s>
Spielberg	Spielberg	k1gMnSc1	Spielberg
si	se	k3xPyFc3	se
najal	najmout	k5eAaPmAgMnS	najmout
Menno	Menno	k1gNnSc4	Menno
Meyjese	Meyjese	k1gFnSc2	Meyjese
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokončil	dokončit	k5eAaPmAgInS	dokončit
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
Říše	říš	k1gFnSc2	říš
slunce	slunce	k1gNnSc2	slunce
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
v	v	k7c6	v
Elstree	Elstree	k1gFnSc6	Elstree
Studios	Studios	k?	Studios
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
Šanghaje	Šanghaj	k1gFnSc2	Šanghaj
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Filmaři	filmař	k1gMnPc1	filmař
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
hledat	hledat	k5eAaImF	hledat
na	na	k7c6	na
asijském	asijský	k2eAgInSc6d1	asijský
kontinentu	kontinent	k1gInSc6	kontinent
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
podobalo	podobat	k5eAaImAgNnS	podobat
Šanghaji	Šanghaj	k1gFnSc3	Šanghaj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
navázali	navázat	k5eAaPmAgMnP	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Shanghai	Shanghai	k1gNnSc7	Shanghai
Film	film	k1gInSc1	film
Studios	Studios	k?	Studios
a	a	k8xC	a
China	China	k1gFnSc1	China
Film	film	k1gInSc1	film
Co-Production	Co-Production	k1gInSc1	Co-Production
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	léto	k1gNnPc6	léto
spolupráce	spolupráce	k1gFnSc2	spolupráce
získali	získat	k5eAaPmAgMnP	získat
povolení	povolení	k1gNnSc4	povolení
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
natáčet	natáčet	k5eAaImF	natáčet
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
natáčen	natáčen	k2eAgInSc1d1	natáčen
v	v	k7c6	v
Šanghaji	Šanghaj	k1gFnSc6	Šanghaj
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgInPc1d1	čínský
úřady	úřad	k1gInPc1	úřad
jim	on	k3xPp3gMnPc3	on
dovolily	dovolit	k5eAaPmAgFnP	dovolit
použití	použití	k1gNnSc4	použití
tradiční	tradiční	k2eAgNnSc4d1	tradiční
čínských	čínský	k2eAgMnPc2d1	čínský
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
použili	použít	k5eAaPmAgMnP	použít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5000	[number]	k4	5000
rekvizit	rekvizita	k1gFnPc2	rekvizita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
připomínaly	připomínat	k5eAaImAgFnP	připomínat
japonskou	japonský	k2eAgFnSc4d1	japonská
okupaci	okupace	k1gFnSc4	okupace
Šanghaje	Šanghaj	k1gFnSc2	Šanghaj
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
osvobozenecké	osvobozenecký	k2eAgFnSc2d1	osvobozenecká
armády	armáda	k1gFnSc2	armáda
hráli	hrát	k5eAaImAgMnP	hrát
japonské	japonský	k2eAgMnPc4d1	japonský
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
místa	místo	k1gNnPc4	místo
natáčení	natáčení	k1gNnSc2	natáčení
byly	být	k5eAaImAgFnP	být
Trebujena	Trebujen	k2eAgFnSc1d1	Trebujen
<g/>
,	,	kIx,	,
Andalusie	Andalusie	k1gFnSc1	Andalusie
<g/>
,	,	kIx,	,
Knutsford	Knutsford	k1gInSc1	Knutsford
<g/>
,	,	kIx,	,
Sunningdale	Sunningdal	k1gMnSc5	Sunningdal
a	a	k8xC	a
Berkshire	Berkshir	k1gMnSc5	Berkshir
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
část	část	k1gFnSc4	část
s	s	k7c7	s
atomovou	atomový	k2eAgFnSc7d1	atomová
bombou	bomba	k1gFnSc7	bomba
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
byla	být	k5eAaImAgFnS	být
použita	použit	k2eAgFnSc1d1	použita
technika	technika	k1gFnSc1	technika
computer-generated	computerenerated	k1gInSc4	computer-generated
imagery	imagera	k1gFnSc2	imagera
<g/>
.	.	kIx.	.
</s>
<s>
Letadla	letadlo	k1gNnPc1	letadlo
typu	typ	k1gInSc2	typ
A6M	A6M	k1gMnSc1	A6M
Zero	Zero	k1gMnSc1	Zero
a	a	k8xC	a
P-51	P-51	k1gMnSc1	P-51
Mustang	mustang	k1gMnSc1	mustang
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsme	být	k5eAaImIp1nP	být
ve	v	k7c6	v
filmu	film	k1gInSc6	film
mohli	moct	k5eAaImAgMnP	moct
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
pouze	pouze	k6eAd1	pouze
umělé	umělý	k2eAgInPc4d1	umělý
modely	model	k1gInPc4	model
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
visuální	visuální	k2eAgInPc4d1	visuální
efekty	efekt	k1gInPc4	efekt
se	se	k3xPyFc4	se
postaralo	postarat	k5eAaPmAgNnS	postarat
studio	studio	k1gNnSc1	studio
Industrial	Industrial	k1gInSc1	Industrial
Light	Light	k1gMnSc1	Light
&	&	k?	&
Magic	Magic	k1gMnSc1	Magic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Empire	empir	k1gInSc5	empir
of	of	k?	of
the	the	k?	the
Sun	Sun	kA	Sun
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Anglická	anglický	k2eAgFnSc1d1	anglická
bibliografie	bibliografie	k1gFnSc1	bibliografie
===	===	k?	===
</s>
</p>
<p>
<s>
Ballard	Ballard	k1gMnSc1	Ballard
<g/>
,	,	kIx,	,
J.	J.	kA	J.
G.	G.	kA	G.
Empire	empir	k1gInSc5	empir
of	of	k?	of
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Victor	Victor	k1gMnSc1	Victor
Gollancz	Gollancz	k1gMnSc1	Gollancz
Ltd	ltd	kA	ltd
<g/>
,	,	kIx,	,
First	First	k1gFnSc1	First
edition	edition	k1gInSc1	edition
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
575	[number]	k4	575
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3483	[number]	k4	3483
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dolan	dolan	k1gMnSc1	dolan
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
F.	F.	kA	F.
Hollywood	Hollywood	k1gInSc1	Hollywood
Goes	Goes	k1gInSc1	Goes
to	ten	k3xDgNnSc1	ten
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Hamlyn	Hamlyn	k1gMnSc1	Hamlyn
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
86124	[number]	k4	86124
<g/>
-	-	kIx~	-
<g/>
229	[number]	k4	229
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evans	Evans	k1gInSc1	Evans
<g/>
,	,	kIx,	,
Alun	Alun	k1gInSc1	Alun
<g/>
.	.	kIx.	.
</s>
<s>
Brassey	Brassea	k1gFnPc1	Brassea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc4	ten
War	War	k1gFnPc1	War
Films	Filmsa	k1gFnPc2	Filmsa
<g/>
.	.	kIx.	.
</s>
<s>
Dulles	Dulles	k1gInSc1	Dulles
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnPc1	Virginium
<g/>
:	:	kIx,	:
Potomac	Potomac	k1gInSc1	Potomac
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
57488	[number]	k4	57488
<g/>
-	-	kIx~	-
<g/>
263	[number]	k4	263
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gordon	Gordon	k1gMnSc1	Gordon
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
and	and	k?	and
Frank	Frank	k1gMnSc1	Frank
Gormile	Gormila	k1gFnSc3	Gormila
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Films	Films	k1gInSc1	Films
of	of	k?	of
Steven	Steven	k2eAgInSc1d1	Steven
Spielberg	Spielberg	k1gInSc1	Spielberg
<g/>
.	.	kIx.	.
</s>
<s>
Lanham	Lanham	k1gInSc1	Lanham
<g/>
,	,	kIx,	,
MD	MD	kA	MD
:	:	kIx,	:
Scarecrow	Scarecrow	k1gMnSc1	Scarecrow
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
pp	pp	k?	pp
<g/>
.	.	kIx.	.
109	[number]	k4	109
<g/>
–	–	k?	–
<g/>
123	[number]	k4	123
<g/>
,	,	kIx,	,
127	[number]	k4	127
<g/>
–	–	k?	–
<g/>
137	[number]	k4	137
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8108	[number]	k4	8108
<g/>
-	-	kIx~	-
<g/>
4182	[number]	k4	4182
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hardwick	Hardwick	k1gMnSc1	Hardwick
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
and	and	k?	and
Ed	Ed	k1gMnSc1	Ed
Schnepf	Schnepf	k1gMnSc1	Schnepf
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
A	a	k9	a
Viewer	Viewer	k1gInSc1	Viewer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
Aviation	Aviation	k1gInSc4	Aviation
Movies	Movies	k1gInSc4	Movies
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
The	The	k1gFnSc1	The
Making	Making	k1gInSc1	Making
of	of	k?	of
the	the	k?	the
Great	Great	k2eAgInSc4d1	Great
Aviation	Aviation	k1gInSc4	Aviation
Films	Filmsa	k1gFnPc2	Filmsa
<g/>
.	.	kIx.	.
</s>
<s>
General	Generat	k5eAaImAgInS	Generat
Aviation	Aviation	k1gInSc1	Aviation
Series	Seriesa	k1gFnPc2	Seriesa
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
2	[number]	k4	2
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
McBride	McBrid	k1gInSc5	McBrid
<g/>
,	,	kIx,	,
Joseph	Josepha	k1gFnPc2	Josepha
<g/>
.	.	kIx.	.
</s>
<s>
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
<g/>
:	:	kIx,	:
A	A	kA	A
Biography	Biograph	k1gInPc1	Biograph
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Faber	Faber	k1gInSc1	Faber
&	&	k?	&
Faber	Faber	k1gInSc1	Faber
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
571	[number]	k4	571
<g/>
-	-	kIx~	-
<g/>
19177	[number]	k4	19177
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Walker	Walker	k1gMnSc1	Walker
<g/>
,	,	kIx,	,
Jeff	Jeff	k1gMnSc1	Jeff
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Empire	empir	k1gInSc5	empir
of	of	k?	of
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Air	Air	k1gFnSc1	Air
Classics	Classicsa	k1gFnPc2	Classicsa
Volume	volum	k1gInSc5	volum
24	[number]	k4	24
<g/>
,	,	kIx,	,
Number	Number	k1gInSc1	Number
1	[number]	k4	1
<g/>
,	,	kIx,	,
January	Januara	k1gFnPc1	Januara
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Říše	říše	k1gFnSc1	říše
slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
filmu	film	k1gInSc2	film
na	na	k7c6	na
fdb	fdb	k?	fdb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
filmu	film	k1gInSc2	film
na	na	k7c4	na
imdb	imdb	k1gInSc4	imdb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
