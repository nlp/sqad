<s>
Říše	říše	k1gFnSc1	říše
slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc1d1	americké
válečné	válečný	k2eAgNnSc1d1	válečné
filmové	filmový	k2eAgNnSc1d1	filmové
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
natočené	natočený	k2eAgNnSc1d1	natočené
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
autobiografické	autobiografický	k2eAgFnSc2d1	autobiografická
novely	novela	k1gFnSc2	novela
J.	J.	kA	J.
G.	G.	kA	G.
Ballarda	Ballard	k1gMnSc2	Ballard
<g/>
.	.	kIx.	.
</s>
