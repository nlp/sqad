<s>
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
</s>
<s>
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Zuikaku	Zuikak	k1gInSc2
a	a	k8xC
dva	dva	k4xCgMnPc4
torpédoborce	torpédoborec	k1gMnPc4
během	během	k7c2
náletu	nálet	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1944	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Filipínské	filipínský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Mariany	Mariana	k1gFnPc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Americké	americký	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Džisaburó	Džisaburó	k?
OzawaKakudži	OzawaKakudž	k1gFnSc3
Kakuta	Kakut	k2eAgFnSc1d1
</s>
<s>
Raymond	Raymond	k1gMnSc1
A.	A.	kA
Spruance	Spruance	k1gFnSc1
Marc	Marc	k1gInSc1
A.	A.	kA
Mitscher	Mitschra	k1gFnPc2
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
6	#num#	k4
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
3	#num#	k4
lehké	lehký	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
5	#num#	k4
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
13	#num#	k4
těžkých	těžký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
6	#num#	k4
lehkých	lehký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
27	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
<g/>
24	#num#	k4
ponorekcca	ponorekcc	k1gInSc2
450	#num#	k4
palubních	palubní	k2eAgFnPc2d1
letadelcca	letadelcca	k6eAd1
300	#num#	k4
letadel	letadlo	k1gNnPc2
z	z	k7c2
pozemních	pozemní	k2eAgFnPc2d1
základen	základna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
8	#num#	k4
lehkých	lehký	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
7	#num#	k4
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
8	#num#	k4
těžkých	těžký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
13	#num#	k4
lehkých	lehký	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
58	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
<g/>
28	#num#	k4
ponorek	ponorka	k1gFnPc2
<g/>
956	#num#	k4
palubních	palubní	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
3	#num#	k4
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
2	#num#	k4
tankeryvíce	tankeryvit	k5eAaImSgFnP
než	než	k8xS
600	#num#	k4
letadel	letadlo	k1gNnPc2
</s>
<s>
123	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
1	#num#	k4
bitevní	bitevní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
poškozena	poškodit	k5eAaPmNgFnS
</s>
<s>
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
námořních	námořní	k2eAgFnPc2d1
bitev	bitva	k1gFnPc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
mezi	mezi	k7c7
americkým	americký	k2eAgMnSc7d1
a	a	k8xC
japonským	japonský	k2eAgNnSc7d1
námořnictvem	námořnictvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
poslední	poslední	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
bylo	být	k5eAaImAgNnS
císařské	císařský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
schopné	schopný	k2eAgNnSc1d1
masově	masově	k6eAd1
nasadit	nasadit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
19	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1944	#num#	k4
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
v	v	k7c6
počátku	počátek	k1gInSc6
americké	americký	k2eAgFnSc2d1
invaze	invaze	k1gFnSc2
na	na	k7c4
Mariany	Marian	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
bitva	bitva	k1gFnSc1
byla	být	k5eAaImAgFnS
pátou	pátá	k1gFnSc4
(	(	kIx(
<g/>
a	a	k8xC
de	de	k?
facto	fact	k2eAgNnSc1d1
poslední	poslední	k2eAgNnSc1d1
<g/>
)	)	kIx)
velkou	velký	k2eAgFnSc7d1
bitvou	bitva	k1gFnSc7
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
během	během	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Akce	akce	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
katastrofou	katastrofa	k1gFnSc7
pro	pro	k7c4
japonské	japonský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
ztratilo	ztratit	k5eAaPmAgNnS
3	#num#	k4
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
dvě	dva	k4xCgFnPc1
akcí	akce	k1gFnPc2
amerických	americký	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
letadel	letadlo	k1gNnPc2
a	a	k8xC
pilotů	pilot	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
bitvě	bitva	k1gFnSc6
už	už	k6eAd1
hrály	hrát	k5eAaImAgFnP
japonské	japonský	k2eAgFnPc1d1
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
jen	jen	k9
podružnou	podružný	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
bitvu	bitva	k1gFnSc4
se	se	k3xPyFc4
vžil	vžít	k5eAaPmAgInS
název	název	k1gInSc1
„	„	k?
<g/>
velké	velký	k2eAgNnSc4d1
střílení	střílení	k1gNnSc4
krocanů	krocan	k1gMnPc2
na	na	k7c6
Marianách	Mariana	k1gFnPc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Great	Great	k2eAgMnSc1d1
Marianas	Marianas	k1gMnSc1
Turkey	Turkea	k1gFnSc2
Shoot	Shoot	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neboť	neboť	k8xC
většina	většina	k1gFnSc1
japonských	japonský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
bitvy	bitva	k1gFnPc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
proti	proti	k7c3
americkým	americký	k2eAgInPc3d1
protějškům	protějšek	k1gInPc3
zastaralá	zastaralý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ovládali	ovládat	k5eAaImAgMnP
je	on	k3xPp3gNnSc4
nezkušení	zkušený	k2eNgMnPc1d1
piloti	pilot	k1gMnPc1
a	a	k8xC
byla	být	k5eAaImAgFnS
ve	v	k7c6
značné	značný	k2eAgFnSc6d1
početní	početní	k2eAgFnSc6d1
nevýhodě	nevýhoda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
fakta	faktum	k1gNnPc1
vedla	vést	k5eAaImAgNnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
Američané	Američan	k1gMnPc1
sestřelovali	sestřelovat	k5eAaImAgMnP
jedno	jeden	k4xCgNnSc4
japonské	japonský	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
za	za	k7c7
druhým	druhý	k4xOgInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
amerických	americký	k2eAgMnPc2d1
pilotů	pilot	k1gMnPc2
po	po	k7c6
přistání	přistání	k1gNnSc6
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
jako	jako	k9
střílení	střílení	k1gNnSc1
krocanů	krocan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Plán	plán	k1gInSc1
operace	operace	k1gFnSc2
A-gó	A-gó	k1gFnSc2
a	a	k8xC
japonské	japonský	k2eAgFnSc2d1
síly	síla	k1gFnSc2
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1943	#num#	k4
rozhodl	rozhodnout	k5eAaPmAgInS
japonský	japonský	k2eAgInSc1d1
hlavní	hlavní	k2eAgInSc1d1
stan	stan	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
přišla	přijít	k5eAaPmAgFnS
chvíle	chvíle	k1gFnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
převzít	převzít	k5eAaPmF
iniciativu	iniciativa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operace	operace	k1gFnSc1
A-gó	A-gó	k1gFnSc1
(	(	kIx(
<g/>
あ	あ	k?
<g/>
,	,	kIx,
A-gó	A-gó	k1gFnSc1
sakusen	sakusna	k1gFnPc2
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
začít	začít	k5eAaPmF
někdy	někdy	k6eAd1
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
a	a	k8xC
spočívala	spočívat	k5eAaImAgFnS
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
námořních	námořní	k2eAgFnPc2d1
a	a	k8xC
pozemních	pozemní	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
byla	být	k5eAaImAgFnS
operace	operace	k1gFnSc1
odložena	odložit	k5eAaPmNgFnS
na	na	k7c4
neurčito	neurčito	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
zahájili	zahájit	k5eAaPmAgMnP
Američané	Američan	k1gMnPc1
invazi	invaze	k1gFnSc4
na	na	k7c4
Saipan	Saipan	k1gInSc4
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
Tojoda	Tojoda	k1gMnSc1
o	o	k7c4
zahájení	zahájení	k1gNnSc4
operace	operace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitel	velitel	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
mobilního	mobilní	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
viceadmirál	viceadmirál	k1gMnSc1
Džisaburó	Džisaburó	k1gMnSc1
Ozawa	Ozawa	k1gMnSc1
měl	mít	k5eAaImAgMnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
tyto	tento	k3xDgInPc4
svazy	svaz	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Předsunutý	předsunutý	k2eAgInSc1d1
svaz	svaz	k1gInSc1
–	–	k?
velitel	velitel	k1gMnSc1
viceadmirál	viceadmirál	k1gMnSc1
Takeo	Takeo	k1gMnSc1
Kurita	Kurita	k1gMnSc1
</s>
<s>
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Čitose	Čitosa	k1gFnSc3
<g/>
,	,	kIx,
Čijoda	Čijoda	k1gFnSc1
a	a	k8xC
Zuihó	Zuihó	k1gFnSc1
</s>
<s>
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Jamato	Jamat	k2eAgNnSc4d1
<g/>
,	,	kIx,
Musaši	Musaš	k1gInSc3
<g/>
,	,	kIx,
Haruna	Haruna	k1gFnSc1
a	a	k8xC
Kongó	Kongó	k1gFnSc1
</s>
<s>
těžké	těžký	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
Atago	Atago	k1gNnSc1
<g/>
,	,	kIx,
Čókai	Čókai	k1gNnSc1
<g/>
,	,	kIx,
Takao	Takao	k1gNnSc1
<g/>
,	,	kIx,
Maja	Maja	k1gFnSc1
<g/>
,	,	kIx,
Kumano	Kumana	k1gFnSc5
<g/>
,	,	kIx,
Suzuja	Suzuja	k1gMnSc1
<g/>
,	,	kIx,
Tone	tonout	k5eAaImIp3nS
a	a	k8xC
Čikuma	Čikuma	k1gFnSc1
</s>
<s>
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Noširo	Noširo	k1gNnSc1
</s>
<s>
8	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
</s>
<s>
Svaz	svaz	k1gInSc1
A	a	k8xC
–	–	k?
velitel	velitel	k1gMnSc1
viceadmirál	viceadmirál	k1gMnSc1
Džisaburó	Džisaburó	k1gMnSc1
Ozawa	Ozawa	k1gMnSc1
</s>
<s>
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Taihó	Taihó	k1gFnSc2
<g/>
,	,	kIx,
Šókaku	Šókak	k1gInSc2
a	a	k8xC
Zuikaku	Zuikak	k1gInSc2
</s>
<s>
těžké	těžký	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
Mjókó	Mjókó	k1gFnPc2
a	a	k8xC
Haguro	Hagura	k1gFnSc5
</s>
<s>
lehký	lehký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Jahagi	Jahag	k1gFnSc2
</s>
<s>
7	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
</s>
<s>
Svaz	svaz	k1gInSc1
B	B	kA
–	–	k?
velitel	velitel	k1gMnSc1
viceadmirál	viceadmirál	k1gMnSc1
Takacugu	Takacuga	k1gFnSc4
Džódžima	Džódžimum	k1gNnSc2
</s>
<s>
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Džunjó	Džunjó	k1gFnSc2
<g/>
,	,	kIx,
Hijó	Hijó	k1gFnSc2
a	a	k8xC
Rjúhó	Rjúhó	k1gFnSc2
</s>
<s>
bitevní	bitevní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Nagato	Nagat	k2eAgNnSc1d1
</s>
<s>
hybridní	hybridní	k2eAgInSc1d1
těžký	těžký	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Mogami	Moga	k1gFnPc7
</s>
<s>
16	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
</s>
<s>
6	#num#	k4
tankerů	tanker	k1gInPc2
</s>
<s>
Složení	složení	k1gNnSc1
amerických	americký	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
</s>
<s>
Japonská	japonský	k2eAgNnPc1d1
plavidla	plavidlo	k1gNnPc1
byla	být	k5eAaImAgNnP
zpozorována	zpozorovat	k5eAaPmNgNnP
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
americkými	americký	k2eAgFnPc7d1
ponorkami	ponorka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitel	velitel	k1gMnSc1
5	#num#	k4
<g/>
.	.	kIx.
loďstva	loďstvo	k1gNnSc2
Raymond	Raymonda	k1gFnPc2
A.	A.	kA
Spruance	Spruance	k1gFnSc2
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
zůstat	zůstat	k5eAaPmF
u	u	k7c2
vyloďujících	vyloďující	k2eAgFnPc2d1
se	se	k3xPyFc4
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
měl	mít	k5eAaImAgInS
operační	operační	k2eAgInSc1d1
svaz	svaz	k1gInSc1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
TF	tf	k0wR
58	#num#	k4
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
velel	velet	k5eAaImAgMnS
viceadmirál	viceadmirál	k1gMnSc1
Marc	Marc	k1gFnSc4
A.	A.	kA
Mitscher	Mitschra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
TF	tf	k0wR
58	#num#	k4
se	se	k3xPyFc4
skládal	skládat	k5eAaImAgMnS
z	z	k7c2
těchto	tento	k3xDgFnPc2
operačních	operační	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
TG	tg	kA
58.1	58.1	k4
–	–	k?
velitel	velitel	k1gMnSc1
kontradmirál	kontradmirál	k1gMnSc1
Joseph	Joseph	k1gMnSc1
J.	J.	kA
Clark	Clark	k1gInSc1
</s>
<s>
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Hornet	Horneta	k1gFnPc2
<g/>
,	,	kIx,
Yorktown	Yorktowna	k1gFnPc2
<g/>
,	,	kIx,
Belleau	Belleaus	k1gInSc2
Wood	Woodo	k1gNnPc2
a	a	k8xC
Bataan	Bataana	k1gFnPc2
</s>
<s>
křižníky	křižník	k1gInPc1
Boston	Boston	k1gInSc1
<g/>
,	,	kIx,
Baltimore	Baltimore	k1gInSc1
<g/>
,	,	kIx,
Canberra	Canberra	k1gMnSc1
<g/>
,	,	kIx,
San	San	k1gMnSc1
Juan	Juan	k1gMnSc1
a	a	k8xC
Oakland	Oakland	k1gInSc1
</s>
<s>
14	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
</s>
<s>
TG	tg	kA
58.2	58.2	k4
–	–	k?
velitel	velitel	k1gMnSc1
kontradmirál	kontradmirál	k1gMnSc1
Alfred	Alfred	k1gMnSc1
E.	E.	kA
Montgomery	Montgomera	k1gFnPc1
</s>
<s>
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Bunker	Bunker	k1gMnSc1
Hill	Hill	k1gMnSc1
<g/>
,	,	kIx,
Wasp	Wasp	k1gMnSc1
<g/>
,	,	kIx,
Monterey	Monterea	k1gFnPc1
a	a	k8xC
Cabot	Cabot	k1gInSc1
</s>
<s>
křižníky	křižník	k1gInPc1
Santa	Santo	k1gNnSc2
Fe	Fe	k1gFnSc4
<g/>
,	,	kIx,
Mobile	mobile	k1gNnSc4
a	a	k8xC
Biloxi	Biloxe	k1gFnSc4
</s>
<s>
12	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
</s>
<s>
TG	tg	kA
58.3	58.3	k4
–	–	k?
velitel	velitel	k1gMnSc1
kontradmirál	kontradmirál	k1gMnSc1
John	John	k1gMnSc1
W.	W.	kA
Reeves	Reeves	k1gMnSc1
</s>
<s>
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Enterprise	Enterprise	k1gFnSc1
<g/>
,	,	kIx,
Lexington	Lexington	k1gInSc1
<g/>
,	,	kIx,
San	San	k1gFnSc1
Jacinto	Jacinta	k1gFnSc5
a	a	k8xC
Princeton	Princeton	k1gInSc1
</s>
<s>
křižníky	křižník	k1gInPc1
Indianapolis	Indianapolis	k1gFnSc2
<g/>
,	,	kIx,
Reno	Reno	k1gNnSc1
<g/>
,	,	kIx,
Montpelier	Montpelier	k1gInSc1
<g/>
,	,	kIx,
Cleveland	Cleveland	k1gInSc1
a	a	k8xC
Birmingham	Birmingham	k1gInSc1
</s>
<s>
13	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
</s>
<s>
TG	tg	kA
58.4	58.4	k4
–	–	k?
velitel	velitel	k1gMnSc1
kontradmirál	kontradmirál	k1gMnSc1
William	William	k1gInSc4
K.	K.	kA
Harrill	Harrill	k1gInSc1
</s>
<s>
letadlové	letadlový	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Essex	Essex	k1gInSc1
<g/>
,	,	kIx,
Langley	Langley	k1gInPc1
a	a	k8xC
Cowpens	Cowpens	k1gInSc1
</s>
<s>
křižníky	křižník	k1gInPc1
San	San	k1gFnSc2
Diego	Diego	k6eAd1
<g/>
,	,	kIx,
Vincennes	Vincennes	k1gInSc1
<g/>
,	,	kIx,
Houston	Houston	k1gInSc1
a	a	k8xC
Miami	Miami	k1gNnSc1
</s>
<s>
14	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
</s>
<s>
TG	tg	kA
58.7	58.7	k4
–	–	k?
velitel	velitel	k1gMnSc1
viceadmirál	viceadmirál	k1gMnSc1
Willis	Willis	k1gFnSc1
A.	A.	kA
Lee	Lea	k1gFnSc6
</s>
<s>
bitevní	bitevní	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
North	North	k1gInSc1
Carolina	Carolina	k1gFnSc1
<g/>
,	,	kIx,
Iowa	Iowa	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
Jersey	Jersea	k1gFnSc2
<g/>
,	,	kIx,
Indiana	Indiana	k1gFnSc1
<g/>
,	,	kIx,
South	South	k1gInSc1
Dakota	Dakota	k1gFnSc1
a	a	k8xC
Alabama	Alabama	k1gFnSc1
</s>
<s>
křižníky	křižník	k1gInPc1
Wichita	Wichitum	k1gNnSc2
<g/>
,	,	kIx,
Minneapolis	Minneapolis	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
Orleans	Orleans	k1gInSc1
a	a	k8xC
San	San	k1gFnSc1
Francisco	Francisco	k6eAd1
</s>
<s>
14	#num#	k4
torpédoborců	torpédoborec	k1gInPc2
</s>
<s>
Dále	daleko	k6eAd2
v	v	k7c6
oblasti	oblast	k1gFnSc6
působil	působit	k5eAaImAgInS
svaz	svaz	k1gInSc1
hlídkových	hlídkový	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
(	(	kIx(
<g/>
TF	tf	k0wR
17	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yRgMnSc3,k3yIgMnSc3
velel	velet	k5eAaImAgInS
viceadmirál	viceadmirál	k1gMnSc1
Charles	Charles	k1gMnSc1
A.	A.	kA
Lockwood	Lockwood	k1gInSc1
s	s	k7c7
18	#num#	k4
ponorkami	ponorka	k1gFnPc7
a	a	k8xC
ponorky	ponorka	k1gFnPc1
7	#num#	k4
<g/>
.	.	kIx.
loďstva	loďstvo	k1gNnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
kontradmirála	kontradmirál	k1gMnSc2
Christieho	Christie	k1gMnSc2
s	s	k7c7
9	#num#	k4
ponorkami	ponorka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Japonské	japonský	k2eAgInPc1d1
útoky	útok	k1gInPc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
ráno	ráno	k6eAd1
našel	najít	k5eAaPmAgMnS
průzkumný	průzkumný	k2eAgInSc4d1
letoun	letoun	k1gInSc4
z	z	k7c2
Guamu	Guam	k1gInSc2
americké	americký	k2eAgFnSc2d1
letadlové	letadlový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
a	a	k8xC
všech	všecek	k3xTgInPc2
30	#num#	k4
letounů	letoun	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
na	na	k7c6
Guamu	Guam	k1gInSc6
<g/>
,	,	kIx,
odstartovalo	odstartovat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedosáhly	dosáhnout	k5eNaPmAgFnP
však	však	k9
žádného	žádný	k3yNgInSc2
úspěchu	úspěch	k1gInSc2
a	a	k8xC
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
sestřelena	sestřelen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spruance	Spruance	k1gFnSc1
očekával	očekávat	k5eAaImAgInS
další	další	k2eAgInPc4d1
útoky	útok	k1gInPc4
poslal	poslat	k5eAaPmAgMnS
do	do	k7c2
vzduchu	vzduch	k1gInSc2
většinu	většina	k1gFnSc4
svých	svůj	k3xOyFgFnPc2
stíhaček	stíhačka	k1gFnPc2
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
450	#num#	k4
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
japonských	japonský	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
odstartovala	odstartovat	k5eAaPmAgFnS
v	v	k7c6
8	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
první	první	k4xOgInSc4
vlna	vlna	k1gFnSc1
16	#num#	k4
stíhaček	stíhačka	k1gFnPc2
Zeke	Zeke	k1gFnPc2
<g/>
,	,	kIx,
45	#num#	k4
stíhacích	stíhací	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
Zeke	Zek	k1gInSc2
a	a	k8xC
8	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
Jill	Jilla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
vlna	vlna	k1gFnSc1
odstartovala	odstartovat	k5eAaPmAgFnS
v	v	k7c6
8	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
byla	být	k5eAaImAgFnS
složena	složit	k5eAaPmNgFnS
z	z	k7c2
48	#num#	k4
stíhaček	stíhačka	k1gFnPc2
Zero	Zero	k6eAd1
<g/>
,	,	kIx,
53	#num#	k4
střemhlavých	střemhlavý	k2eAgFnPc2d1
Judy	judo	k1gNnPc7
a	a	k8xC
27	#num#	k4
torpédových	torpédový	k2eAgFnPc2d1
Jill	Jilla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
vlna	vlna	k1gFnSc1
odstartovala	odstartovat	k5eAaPmAgFnS
v	v	k7c6
10	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
složena	složit	k5eAaPmNgFnS
z	z	k7c2
15	#num#	k4
stíhaček	stíhačka	k1gFnPc2
Zero	Zero	k6eAd1
<g/>
,	,	kIx,
25	#num#	k4
stíhacích	stíhací	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
Zeke	Zek	k1gInSc2
a	a	k8xC
7	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
letounů	letoun	k1gInPc2
Jill	Jilla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtá	čtvrtá	k1gFnSc1
vlna	vlna	k1gFnSc1
odstartovala	odstartovat	k5eAaPmAgFnS
v	v	k7c6
11	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
složena	složit	k5eAaPmNgFnS
z	z	k7c2
30	#num#	k4
stíhaček	stíhačka	k1gFnPc2
Zero	Zero	k6eAd1
<g/>
,	,	kIx,
15	#num#	k4
stíhacích	stíhací	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
Zeke	Zek	k1gFnSc2
9	#num#	k4
střemhlavých	střemhlavý	k2eAgMnPc2d1
Judy	judo	k1gNnPc7
<g/>
,	,	kIx,
27	#num#	k4
střemhlavých	střemhlavý	k2eAgInPc2d1
Val	val	k1gInSc4
a	a	k8xC
6	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
letounů	letoun	k1gInPc2
Jill	Jilla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
japonských	japonský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
byla	být	k5eAaImAgFnS
sestřelena	sestřelen	k2eAgFnSc1d1
americkými	americký	k2eAgMnPc7d1
stíhači	stíhač	k1gMnPc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
protileteckou	protiletecký	k2eAgFnSc7d1
obranou	obrana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgInP
až	až	k9
nad	nad	k7c4
americké	americký	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
nezpůsobily	způsobit	k5eNaPmAgInP
žádné	žádný	k3yNgFnPc4
větší	veliký	k2eAgFnPc4d2
škody	škoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
přišli	přijít	k5eAaPmAgMnP
Japonci	Japonec	k1gMnPc1
o	o	k7c4
293	#num#	k4
letadel	letadlo	k1gNnPc2
a	a	k8xC
Američané	Američan	k1gMnPc1
o	o	k7c4
30	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Americké	americký	k2eAgFnPc1d1
ponorky	ponorka	k1gFnPc1
</s>
<s>
Americké	americký	k2eAgNnSc1d1
velení	velení	k1gNnSc1
rozmístilo	rozmístit	k5eAaPmAgNnS
ponorky	ponorka	k1gFnPc4
Albacore	Albacor	k1gInSc5
<g/>
,	,	kIx,
Cavalla	Cavalla	k1gMnSc1
<g/>
,	,	kIx,
Finback	Finback	k1gMnSc1
<g/>
,	,	kIx,
Bang	Bang	k1gMnSc1
a	a	k8xC
Stingray	Stingraa	k1gFnPc1
na	na	k7c4
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
kudy	kudy	k6eAd1
předpokládaly	předpokládat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
japonský	japonský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
proplout	proplout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
rozmístění	rozmístění	k1gNnSc3
zkorigovaly	zkorigovat	k5eAaPmAgFnP
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
v	v	k7c6
8	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
zpozorovala	zpozorovat	k5eAaPmAgFnS
ponorka	ponorka	k1gFnSc1
Albacore	Albacor	k1gInSc5
japonské	japonský	k2eAgFnSc2d1
lodě	loď	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
Albacore	Albacor	k1gInSc5
odpálila	odpálit	k5eAaPmAgFnS
6	#num#	k4
torpéd	torpédo	k1gNnPc2
na	na	k7c4
nejbližší	blízký	k2eAgFnSc4d3
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
a	a	k8xC
ponořila	ponořit	k5eAaPmAgFnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Taihó	Taihó	k1gFnPc2
byla	být	k5eAaImAgFnS
zasažena	zasáhnout	k5eAaPmNgFnS
dvěma	dva	k4xCgInPc7
torpédy	torpédo	k1gNnPc7
a	a	k8xC
po	po	k7c6
sedmnácté	sedmnáctý	k4xOgFnSc6
hodině	hodina	k1gFnSc6
po	po	k7c6
sérii	série	k1gFnSc6
výbuchů	výbuch	k1gInPc2
se	se	k3xPyFc4
potopila	potopit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
vypustila	vypustit	k5eAaPmAgFnS
ponorka	ponorka	k1gFnSc1
Cavalla	Cavalla	k1gFnSc1
6	#num#	k4
torpéd	torpédo	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
3	#num#	k4
zasáhly	zasáhnout	k5eAaPmAgFnP
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Šókaku	Šókak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
1	#num#	k4
zmizela	zmizet	k5eAaPmAgFnS
Šókaku	Šókak	k1gInSc2
pod	pod	k7c7
hladinou	hladina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Admirál	admirál	k1gMnSc1
Ozawa	Ozawa	k1gMnSc1
přesedl	přesednout	k5eAaPmAgMnS
na	na	k7c4
křižník	křižník	k1gInSc4
Haguro	Hagura	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Americký	americký	k2eAgInSc1d1
protiúder	protiúder	k1gInSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
v	v	k7c6
15	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
nalezl	nalézt	k5eAaBmAgInS,k5eAaPmAgInS
americký	americký	k2eAgInSc1d1
průzkumný	průzkumný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
japonské	japonský	k2eAgFnSc2d1
letadlové	letadlový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
16	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
odstartovala	odstartovat	k5eAaPmAgFnS
americká	americký	k2eAgFnSc1d1
útočná	útočný	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
216	#num#	k4
letadel	letadlo	k1gNnPc2
složená	složený	k2eAgFnSc1d1
z	z	k7c2
85	#num#	k4
stíhaček	stíhačka	k1gFnPc2
<g/>
,	,	kIx,
77	#num#	k4
střemhlavých	střemhlavý	k2eAgFnPc2d1
a	a	k8xC
54	#num#	k4
torpédových	torpédový	k2eAgInPc2d1
bombardérů	bombardér	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
letadla	letadlo	k1gNnSc2
potopila	potopit	k5eAaPmAgFnS
letadlovou	letadlový	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Hijó	Hijó	k1gFnSc2
a	a	k8xC
tankery	tanker	k1gInPc4
Genjó	Genjó	k1gFnPc2
Maru	Maru	k1gFnPc2
a	a	k8xC
Seijó	Seijó	k1gFnPc2
Maru	Maru	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poškodily	poškodit	k5eAaPmAgFnP
letadlové	letadlový	k2eAgFnPc1d1
lodi	loď	k1gFnPc1
Zuikaku	Zuikak	k1gInSc2
a	a	k8xC
Čijodu	Čijod	k1gInSc2
<g/>
,	,	kIx,
bitevní	bitevní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Haruna	Haruna	k1gFnSc1
a	a	k8xC
křižník	křižník	k1gInSc1
Maja	Maja	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
Američanům	Američan	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
sestřelit	sestřelit	k5eAaPmF
40	#num#	k4
japonských	japonský	k2eAgNnPc2d1
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
při	při	k7c6
ztrátě	ztráta	k1gFnSc6
20	#num#	k4
vlastních	vlastní	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoha	mnoho	k4c3
americkým	americký	k2eAgMnSc7d1
letadlům	letadlo	k1gNnPc3
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
zpáteční	zpáteční	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
palivo	palivo	k1gNnSc4
a	a	k8xC
téměř	téměř	k6eAd1
80	#num#	k4
letadel	letadlo	k1gNnPc2
(	(	kIx(
<g/>
17	#num#	k4
Hellcat	Hellcat	k1gFnPc2
<g/>
,	,	kIx,
35	#num#	k4
Helldiver	Helldivra	k1gFnPc2
a	a	k8xC
20	#num#	k4
Avenger	Avengra	k1gFnPc2
<g/>
)	)	kIx)
muselo	muset	k5eAaImAgNnS
přistát	přistát	k5eAaPmF,k5eAaImF
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
havarovalo	havarovat	k5eAaPmAgNnS
při	při	k7c6
přistání	přistání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Battle	Battle	k1gFnSc2
of	of	k?
the	the	k?
Philippine	Philippin	k1gMnSc5
Sea	Sea	k1gMnSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
DULL	DULL	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
S.	S.	kA
A	a	k9
Battle	Battle	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Imperial	Imperial	k1gInSc1
Japanese	Japanese	k1gFnSc1
Navy	Navy	k?
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
219	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
;	;	kIx,
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořní	námořní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
vrcholí	vrcholit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
obléhání	obléhání	k1gNnSc2
Malty	Malta	k1gFnSc2
k	k	k7c3
boji	boj	k1gInSc3
u	u	k7c2
Severního	severní	k2eAgInSc2d1
mysu	mys	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
345	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
206	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
443	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
HUBÁČEK	Hubáček	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
477	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
781	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
na	na	k7c6
Palba	palba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
na	na	k7c6
Palba	palba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Malajsii	Malajsie	k1gFnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
•	•	k?
Boje	boj	k1gInPc4
o	o	k7c4
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
Guadalcanal	Guadalcanal	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrova	ostrov	k1gInSc2
Savo	Savo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
východních	východní	k2eAgMnPc2d1
Šalomounů	Šalomoun	k1gMnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
mysu	mys	k1gInSc2
Esperance	Esperance	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrovů	ostrov	k1gInPc2
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Guadalcanalu	Guadalcanal	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tassafarongy	Tassafarong	k1gInPc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Rennellova	Rennellův	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Bismarckově	Bismarckův	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Komandorských	Komandorský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Tarawu	Tarawus	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Operace	operace	k1gFnSc1
Forager	Forager	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Saipan	Saipan	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Peleliu	Pelelium	k1gNnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Leyte	Leyt	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Manilu	Manila	k1gFnSc4
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Iwodžimu	Iwodžimo	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Okinawu	Okinawa	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Japonsko	Japonsko	k1gNnSc1
|	|	kIx~
Loďstvo	loďstvo	k1gNnSc1
</s>
