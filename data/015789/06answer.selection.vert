<s>
Akce	akce	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
katastrofou	katastrofa	k1gFnSc7
pro	pro	k7c4
japonské	japonský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
ztratilo	ztratit	k5eAaPmAgNnS
3	#num#	k4
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
dvě	dva	k4xCgFnPc1
akcí	akce	k1gFnPc2
amerických	americký	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
letadel	letadlo	k1gNnPc2
a	a	k8xC
pilotů	pilot	k1gMnPc2
<g/>
.	.	kIx.
</s>