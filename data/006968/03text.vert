<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Milčakov	Milčakov	k1gInSc1	Milčakov
(	(	kIx(	(
<g/>
Д	Д	k?	Д
С	С	k?	С
М	М	k?	М
<g/>
;	;	kIx,	;
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
březen	březen	k1gInSc4	březen
1986	[number]	k4	1986
Minsk	Minsk	k1gInSc1	Minsk
<g/>
,	,	kIx,	,
Běloruská	běloruský	k2eAgFnSc1d1	Běloruská
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
profesionální	profesionální	k2eAgMnSc1d1	profesionální
běloruský	běloruský	k2eAgMnSc1d1	běloruský
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgMnSc1d1	hrající
na	na	k7c6	na
postu	post	k1gInSc6	post
brankáře	brankář	k1gMnSc2	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
současným	současný	k2eAgInSc7d1	současný
týmem	tým	k1gInSc7	tým
je	být	k5eAaImIp3nS	být
Metallurg	Metallurg	k1gInSc1	Metallurg
Žlobin	Žlobin	k2eAgInSc1d1	Žlobin
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
hraje	hrát	k5eAaImIp3nS	hrát
Běloruskou	běloruský	k2eAgFnSc4d1	Běloruská
extraligu	extraliga	k1gFnSc4	extraliga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
běloruské	běloruský	k2eAgFnSc2d1	Běloruská
reprezentace	reprezentace	k1gFnSc2	reprezentace
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
tří	tři	k4xCgNnPc2	tři
mistrovství	mistrovství	k1gNnPc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
(	(	kIx(	(
<g/>
MS	MS	kA	MS
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
MS	MS	kA	MS
2011	[number]	k4	2011
a	a	k8xC	a
MS	MS	kA	MS
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Dmitri	Dmitri	k1gNnSc1	Dmitri
Milchakov	Milchakov	k1gInSc1	Milchakov
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
