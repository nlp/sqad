<p>
<s>
Horákovi	Horák	k1gMnSc3	Horák
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc1d1	natočený
společností	společnost	k1gFnSc7	společnost
Dramedy	Dramed	k1gMnPc7	Dramed
Productions	Productions	k1gInSc4	Productions
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
</s>
<s>
Předlohou	předloha	k1gFnSc7	předloha
je	být	k5eAaImIp3nS	být
španělský	španělský	k2eAgInSc1d1	španělský
seriál	seriál	k1gInSc1	seriál
Los	los	k1gInSc1	los
Serrano	Serrana	k1gFnSc5	Serrana
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc4	seriál
začala	začít	k5eAaPmAgFnS	začít
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
vysílat	vysílat	k5eAaImF	vysílat
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
39	[number]	k4	39
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Divák	divák	k1gMnSc1	divák
sleduje	sledovat	k5eAaImIp3nS	sledovat
každodenní	každodenní	k2eAgFnPc4d1	každodenní
radosti	radost	k1gFnPc4	radost
i	i	k8xC	i
starosti	starost	k1gFnPc4	starost
moderní	moderní	k2eAgFnSc2d1	moderní
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dějově	dějově	k6eAd1	dějově
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
však	však	k9	však
spojují	spojovat	k5eAaImIp3nP	spojovat
osudy	osud	k1gInPc4	osud
a	a	k8xC	a
rodinné	rodinný	k2eAgInPc4d1	rodinný
vztahy	vztah	k1gInPc4	vztah
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Web	web	k1gInSc1	web
SerialZone	SerialZon	k1gInSc5	SerialZon
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
1	[number]	k4	1
<g/>
.	.	kIx.	.
série	série	k1gFnSc1	série
končí	končit	k5eAaImIp3nS	končit
po	po	k7c4	po
22	[number]	k4	22
<g/>
.	.	kIx.	.
díle	dílo	k1gNnSc6	dílo
Vetřelec	vetřelec	k1gMnSc1	vetřelec
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
série	série	k1gFnSc1	série
začíná	začínat	k5eAaImIp3nS	začínat
1	[number]	k4	1
<g/>
.	.	kIx.	.
dílem	dílem	k6eAd1	dílem
Věštba	věštba	k1gFnSc1	věštba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Kadlecová	Kadlecová	k1gFnSc1	Kadlecová
Horáková	Horáková	k1gFnSc1	Horáková
(	(	kIx(	(
<g/>
Lucie	Lucie	k1gFnSc1	Lucie
Benešová	Benešová	k1gFnSc1	Benešová
<g/>
)	)	kIx)	)
–	–	k?	–
rozvedena	rozveden	k2eAgFnSc1d1	rozvedena
<g/>
,	,	kIx,	,
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bývalým	bývalý	k2eAgMnSc7d1	bývalý
manželem	manžel	k1gMnSc7	manžel
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
Evu	Eva	k1gFnSc4	Eva
a	a	k8xC	a
Terezu	Tereza	k1gFnSc4	Tereza
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
společně	společně	k6eAd1	společně
s	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
manželem	manžel	k1gMnSc7	manžel
Janem	Jan	k1gMnSc7	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
učitelka	učitelka	k1gFnSc1	učitelka
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
chodí	chodit	k5eAaImIp3nP	chodit
téměř	téměř	k6eAd1	téměř
každé	každý	k3xTgNnSc4	každý
dítě	dítě	k1gNnSc4	dítě
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
nové	nový	k2eAgFnSc2d1	nová
i	i	k8xC	i
staré	starý	k2eAgFnSc2d1	stará
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Horák	Horák	k1gMnSc1	Horák
(	(	kIx(	(
<g/>
Igor	Igor	k1gMnSc1	Igor
Bareš	Bareš	k1gMnSc1	Bareš
<g/>
)	)	kIx)	)
–	–	k?	–
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
manželky	manželka	k1gFnSc2	manželka
Marty	Marta	k1gFnSc2	Marta
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
našel	najít	k5eAaPmAgMnS	najít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
bývalou	bývalý	k2eAgFnSc7d1	bývalá
láskou	láska	k1gFnSc7	láska
Lucií	Lucie	k1gFnPc2	Lucie
a	a	k8xC	a
vzali	vzít	k5eAaPmAgMnP	vzít
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
třem	tři	k4xCgMnPc3	tři
klukům	kluk	k1gMnPc3	kluk
<g/>
,	,	kIx,	,
Tomášovi	Tomáš	k1gMnSc3	Tomáš
<g/>
,	,	kIx,	,
Ondrovi	Ondra	k1gMnSc3	Ondra
a	a	k8xC	a
Kubíkovi	Kubík	k1gMnSc3	Kubík
<g/>
,	,	kIx,	,
přibyly	přibýt	k5eAaPmAgFnP	přibýt
dvě	dva	k4xCgFnPc1	dva
dívky	dívka	k1gFnPc1	dívka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgMnPc4	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
být	být	k5eAaImF	být
tím	ten	k3xDgMnSc7	ten
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
otcem	otec	k1gMnSc7	otec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
hospodě	hospodě	k?	hospodě
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
bratra	bratr	k1gMnSc2	bratr
Standy	Standa	k1gMnSc2	Standa
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k6eAd1	ještě
bratra	bratr	k1gMnSc2	bratr
Matěje	Matěj	k1gMnSc2	Matěj
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
milionář	milionář	k1gMnSc1	milionář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Krátká	krátký	k2eAgFnSc1d1	krátká
(	(	kIx(	(
<g/>
Hana	Hana	k1gFnSc1	Hana
Vagnerová	Vagnerová	k1gFnSc1	Vagnerová
<g/>
)	)	kIx)	)
–	–	k?	–
Luciina	Luciin	k2eAgFnSc1d1	Luciina
starší	starý	k2eAgFnSc1d2	starší
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
chytrá	chytrý	k2eAgFnSc1d1	chytrá
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
prospívat	prospívat	k5eAaImF	prospívat
na	na	k7c4	na
výbornou	výborná	k1gFnSc4	výborná
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc4d1	pravý
opak	opak	k1gInSc4	opak
svého	svůj	k3xOyFgMnSc2	svůj
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
známostí	známost	k1gFnPc2	známost
měla	mít	k5eAaImAgFnS	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
a	a	k8xC	a
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
budou	být	k5eAaImBp3nP	být
moct	moct	k5eAaImF	moct
být	být	k5eAaImF	být
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
<s>
Chodí	chodit	k5eAaImIp3nP	chodit
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
třídy	třída	k1gFnSc2	třída
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
<g/>
,	,	kIx,	,
a	a	k8xC	a
zde	zde	k6eAd1	zde
učí	učit	k5eAaImIp3nS	učit
i	i	k9	i
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
Krátká	krátký	k2eAgFnSc1d1	krátká
(	(	kIx(	(
<g/>
Mariana	Mariana	k1gFnSc1	Mariana
Prachařová	Prachařová	k1gFnSc1	Prachařová
<g/>
)	)	kIx)	)
–	–	k?	–
Luciina	Luciin	k2eAgFnSc1d1	Luciina
mladší	mladý	k2eAgFnSc1d2	mladší
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
energická	energický	k2eAgFnSc1d1	energická
<g/>
,	,	kIx,	,
impulsivní	impulsivní	k2eAgFnSc1d1	impulsivní
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
má	mít	k5eAaImIp3nS	mít
nos	nos	k1gInSc4	nos
pěkně	pěkně	k6eAd1	pěkně
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
chytrá	chytrý	k2eAgFnSc1d1	chytrá
a	a	k8xC	a
zajímá	zajímat	k5eAaImIp3nS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
válku	válka	k1gFnSc4	válka
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
nevlastním	vlastní	k2eNgMnSc7d1	nevlastní
bratrem	bratr	k1gMnSc7	bratr
Ondrou	Ondra	k1gMnSc7	Ondra
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
chodí	chodit	k5eAaImIp3nS	chodit
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
roste	růst	k5eAaImIp3nS	růst
mladá	mladý	k2eAgFnSc1d1	mladá
slečna	slečna	k1gFnSc1	slečna
a	a	k8xC	a
do	do	k7c2	do
života	život	k1gInSc2	život
jí	on	k3xPp3gFnSc2	on
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
nové	nový	k2eAgFnPc1d1	nová
emoce	emoce	k1gFnPc1	emoce
jako	jako	k9	jako
třeba	třeba	k6eAd1	třeba
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Horák	Horák	k1gMnSc1	Horák
(	(	kIx(	(
<g/>
Jakub	Jakub	k1gMnSc1	Jakub
Chromeček	Chromeček	k1gInSc1	Chromeček
<g/>
)	)	kIx)	)
–	–	k?	–
Janův	Janův	k2eAgMnSc1d1	Janův
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nS	chodit
na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
neprospívá	prospívat	k5eNaImIp3nS	prospívat
tak	tak	k9	tak
dobře	dobře	k6eAd1	dobře
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
sestra	sestra	k1gFnSc1	sestra
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
chodí	chodit	k5eAaImIp3nP	chodit
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
kamarádem	kamarád	k1gMnSc7	kamarád
je	být	k5eAaImIp3nS	být
Patrik	Patrik	k1gMnSc1	Patrik
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
synem	syn	k1gMnSc7	syn
jejich	jejich	k3xOp3gFnSc2	jejich
učitelky	učitelka	k1gFnSc2	učitelka
Soni	Soňa	k1gFnSc2	Soňa
<g/>
.	.	kIx.	.
</s>
<s>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
ho	on	k3xPp3gInSc4	on
fotbal	fotbal	k1gInSc4	fotbal
a	a	k8xC	a
i	i	k9	i
dívky	dívka	k1gFnPc4	dívka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
trochu	trochu	k6eAd1	trochu
pozmění	pozměnit	k5eAaPmIp3nS	pozměnit
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
zprvu	zprvu	k6eAd1	zprvu
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
přiznat	přiznat	k5eAaPmF	přiznat
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
ho	on	k3xPp3gNnSc4	on
přebít	přebít	k5eAaPmF	přebít
vztahem	vztah	k1gInSc7	vztah
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
češtinářkou	češtinářka	k1gFnSc7	češtinářka
Markétou	Markéta	k1gFnSc7	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
dráze	dráha	k1gFnSc6	dráha
slavného	slavný	k2eAgMnSc2d1	slavný
hudebníka	hudebník	k1gMnSc2	hudebník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ondra	Ondra	k1gMnSc1	Ondra
Horák	Horák	k1gMnSc1	Horák
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Komínek	Komínek	k1gMnSc1	Komínek
<g/>
)	)	kIx)	)
–	–	k?	–
Janův	Janův	k2eAgMnSc1d1	Janův
prostřední	prostřední	k2eAgMnSc1d1	prostřední
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
libuje	libovat	k5eAaImIp3nS	libovat
si	se	k3xPyFc3	se
ve	v	k7c6	v
šprýmech	šprým	k1gInPc6	šprým
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
nejen	nejen	k6eAd1	nejen
plný	plný	k2eAgInSc4d1	plný
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taky	taky	k6eAd1	taky
plná	plný	k2eAgFnSc1d1	plná
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
moc	moc	k6eAd1	moc
nejde	jít	k5eNaImIp3nS	jít
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgInPc1d1	jediný
v	v	k7c6	v
čem	co	k3yRnSc6	co
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
dobrý	dobrý	k2eAgInSc1d1	dobrý
je	být	k5eAaImIp3nS	být
sport	sport	k1gInSc1	sport
a	a	k8xC	a
technika	technika	k1gFnSc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
si	se	k3xPyFc3	se
ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
dělá	dělat	k5eAaImIp3nS	dělat
srandu	sranda	k1gFnSc4	sranda
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
partou	parta	k1gFnSc7	parta
kluků	kluk	k1gMnPc2	kluk
kuje	kout	k5eAaImIp3nS	kout
pikle	pikel	k1gInSc2	pikel
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
zvítězit	zvítězit	k5eAaPmF	zvítězit
nad	nad	k7c7	nad
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
Terezou	Tereza	k1gFnSc7	Tereza
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pěkné	pěkný	k2eAgNnSc1d1	pěkné
kvítko	kvítko	k1gNnSc1	kvítko
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
úhlavním	úhlavní	k2eAgMnSc7d1	úhlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
je	být	k5eAaImIp3nS	být
záchodová	záchodový	k2eAgFnSc1d1	záchodová
štětka	štětka	k1gFnSc1	štětka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Horák	Horák	k1gMnSc1	Horák
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
)	)	kIx)	)
–	–	k?	–
Janův	Janův	k2eAgMnSc1d1	Janův
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
benjamínek	benjamínek	k1gMnSc1	benjamínek
celé	celý	k2eAgFnSc2d1	celá
rodiny	rodina	k1gFnSc2	rodina
Horákových	Horáková	k1gFnPc2	Horáková
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
Klobáska	klobáska	k1gFnSc1	klobáska
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
otcova	otcův	k2eAgInSc2d1	otcův
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Zastává	zastávat	k5eAaImIp3nS	zastávat
doma	doma	k6eAd1	doma
většinu	většina	k1gFnSc4	většina
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gMnSc4	on
nebaví	bavit	k5eNaImIp3nS	bavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
rád	rád	k6eAd1	rád
svého	svůj	k3xOyFgMnSc4	svůj
tatínka	tatínek	k1gMnSc4	tatínek
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
aby	aby	kYmCp3nP	aby
byl	být	k5eAaImAgInS	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Lucii	Lucie	k1gFnSc4	Lucie
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
dobrou	dobrý	k2eAgFnSc4d1	dobrá
maminku	maminka	k1gFnSc4	maminka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ji	on	k3xPp3gFnSc4	on
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
svoje	svůj	k3xOyFgMnPc4	svůj
bratry	bratr	k1gMnPc4	bratr
a	a	k8xC	a
sestry	sestra	k1gFnPc4	sestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patrik	Patrik	k1gMnSc1	Patrik
Procházka	Procházka	k1gMnSc1	Procházka
(	(	kIx(	(
<g/>
Jakub	Jakub	k1gMnSc1	Jakub
Prachař	Prachař	k1gMnSc1	Prachař
<g/>
)	)	kIx)	)
–	–	k?	–
Tomášův	Tomášův	k2eAgMnSc1d1	Tomášův
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
Edův	Edův	k2eAgMnSc1d1	Edův
a	a	k8xC	a
Sonin	Sonin	k2eAgMnSc1d1	Sonin
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
zajímá	zajímat	k5eAaImIp3nS	zajímat
jen	jen	k9	jen
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
sex	sex	k1gInSc1	sex
a	a	k8xC	a
erotika	erotika	k1gFnSc1	erotika
<g/>
.	.	kIx.	.
</s>
<s>
Láskou	láska	k1gFnSc7	láska
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
Eva	Eva	k1gFnSc1	Eva
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
několikrát	několikrát	k6eAd1	několikrát
zabodoval	zabodovat	k5eAaPmAgMnS	zabodovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Tomáš	Tomáš	k1gMnSc1	Tomáš
hraje	hrát	k5eAaImIp3nS	hrát
fotbal	fotbal	k1gInSc4	fotbal
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
přijít	přijít	k5eAaPmF	přijít
s	s	k7c7	s
nějakou	nějaký	k3yIgFnSc7	nějaký
dobrou	dobrý	k2eAgFnSc7d1	dobrá
radou	rada	k1gFnSc7	rada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
super	super	k1gInSc2	super
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinému	jiný	k2eAgMnSc3d1	jiný
připadá	připadat	k5eAaPmIp3nS	připadat
jako	jako	k9	jako
totální	totální	k2eAgFnSc1d1	totální
blbost	blbost	k1gFnSc1	blbost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdeňka	Zdeňka	k1gFnSc1	Zdeňka
Kadlecová	Kadlecová	k1gFnSc1	Kadlecová
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
Šulcová	Šulcová	k1gFnSc1	Šulcová
<g/>
)	)	kIx)	)
–	–	k?	–
Luciina	Luciin	k2eAgFnSc1d1	Luciina
babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
Horákových	Horáková	k1gFnPc2	Horáková
<g/>
.	.	kIx.	.
</s>
<s>
Drží	držet	k5eAaImIp3nS	držet
nad	nad	k7c7	nad
svou	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
pevnou	pevný	k2eAgFnSc4d1	pevná
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
ji	on	k3xPp3gFnSc4	on
ke	k	k7c3	k
spořádanosti	spořádanost	k1gFnSc3	spořádanost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bezchybná	bezchybný	k2eAgFnSc1d1	bezchybná
babička	babička	k1gFnSc1	babička
někdy	někdy	k6eAd1	někdy
nesplete	splést	k5eNaPmIp3nS	splést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soňa	Soňa	k1gFnSc1	Soňa
Procházková	procházkový	k2eAgFnSc1d1	Procházková
(	(	kIx(	(
<g/>
Nela	Nela	k1gFnSc1	Nela
Boudová	Boudová	k1gFnSc1	Boudová
<g/>
)	)	kIx)	)
–	–	k?	–
přísná	přísný	k2eAgFnSc1d1	přísná
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
,	,	kIx,	,
Luciina	Luciin	k2eAgFnSc1d1	Luciina
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamarádka	kamarádka	k1gFnSc1	kamarádka
<g/>
,	,	kIx,	,
Edova	Edův	k2eAgFnSc1d1	Edova
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
boj	boj	k1gInSc1	boj
s	s	k7c7	s
žákem	žák	k1gMnSc7	žák
Ondrou	Ondra	k1gMnSc7	Ondra
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
partou	parta	k1gFnSc7	parta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
neustále	neustále	k6eAd1	neustále
v	v	k7c6	v
patách	pata	k1gFnPc6	pata
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
i	i	k9	i
její	její	k3xOp3gFnSc1	její
milá	milý	k2eAgFnSc1d1	Milá
povaha	povaha	k1gFnSc1	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jen	jen	k9	jen
někdy	někdy	k6eAd1	někdy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eda	Eda	k1gMnSc1	Eda
Procházka	Procházka	k1gMnSc1	Procházka
(	(	kIx(	(
<g/>
Ctirad	Ctirad	k1gMnSc1	Ctirad
Götz	Götz	k1gMnSc1	Götz
<g/>
)	)	kIx)	)
–	–	k?	–
automechanik	automechanik	k1gMnSc1	automechanik
<g/>
,	,	kIx,	,
Sonin	Sonin	k2eAgMnSc1d1	Sonin
manžel	manžel	k1gMnSc1	manžel
a	a	k8xC	a
Patrikův	Patrikův	k2eAgMnSc1d1	Patrikův
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
viděn	vidět	k5eAaImNgMnS	vidět
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
Standy	Standa	k1gMnSc2	Standa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc3	svůj
manželce	manželka	k1gFnSc3	manželka
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
milovník	milovník	k1gMnSc1	milovník
<g/>
,	,	kIx,	,
jakého	jaký	k3yIgMnSc4	jaký
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Horák	Horák	k1gMnSc1	Horák
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Hraběta	Hraběta	k1gMnSc1	Hraběta
<g/>
)	)	kIx)	)
–	–	k?	–
Janův	Janův	k2eAgMnSc1d1	Janův
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yIgInPc3	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
hospodu	hospodu	k?	hospodu
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
kryl	krýt	k5eAaImAgInS	krýt
bratra	bratr	k1gMnSc4	bratr
Matěje	Matěj	k1gMnSc4	Matěj
před	před	k7c7	před
Janem	Jan	k1gMnSc7	Jan
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nechtěl	chtít	k5eNaImAgMnS	chtít
aby	aby	kYmCp3nS	aby
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
bratr	bratr	k1gMnSc1	bratr
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Matěj	Matěj	k1gMnSc1	Matěj
podvodník	podvodník	k1gMnSc1	podvodník
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
svobodný	svobodný	k2eAgMnSc1d1	svobodný
a	a	k8xC	a
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anežka	Anežka	k1gFnSc1	Anežka
Králová	Králová	k1gFnSc1	Králová
(	(	kIx(	(
<g/>
Dana	Dana	k1gFnSc1	Dana
Batulková	Batulková	k1gFnSc1	Batulková
<g/>
)	)	kIx)	)
–	–	k?	–
učitelka	učitelka	k1gFnSc1	učitelka
náboženství	náboženství	k1gNnSc2	náboženství
(	(	kIx(	(
<g/>
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
chodí	chodit	k5eAaImIp3nP	chodit
se	s	k7c7	s
Standou	Standa	k1gFnSc7	Standa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kristýna	Kristýna	k1gFnSc1	Kristýna
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Šildová	Šildová	k1gFnSc1	Šildová
<g/>
)	)	kIx)	)
–	–	k?	–
Evina	Evin	k2eAgFnSc1d1	Evina
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamarádka	kamarádka	k1gFnSc1	kamarádka
(	(	kIx(	(
<g/>
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
dílu	díl	k1gInSc2	díl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
Bernášková	Bernášková	k1gFnSc1	Bernášková
<g/>
)	)	kIx)	)
–	–	k?	–
učitelka	učitelka	k1gFnSc1	učitelka
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dává	dávat	k5eAaImIp3nS	dávat
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
nejen	nejen	k6eAd1	nejen
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
před	před	k7c7	před
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
(	(	kIx(	(
<g/>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Česák	Česák	k1gMnSc1	Česák
<g/>
)	)	kIx)	)
–	–	k?	–
Ondrův	Ondrův	k2eAgMnSc1d1	Ondrův
kamarád	kamarád	k1gMnSc1	kamarád
</s>
</p>
<p>
<s>
Vít	Vít	k1gMnSc1	Vít
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
Gallo	Gallo	k1gNnSc4	Gallo
<g/>
)	)	kIx)	)
–	–	k?	–
Ondrův	Ondrův	k2eAgMnSc1d1	Ondrův
kamarád	kamarád	k1gMnSc1	kamarád
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
Daniel	Daniel	k1gMnSc1	Daniel
Rous	Rous	k1gMnSc1	Rous
<g/>
)	)	kIx)	)
–	–	k?	–
ředitel	ředitel	k1gMnSc1	ředitel
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
Sonin	Sonin	k2eAgMnSc1d1	Sonin
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
do	do	k7c2	do
Lucie	Lucie	k1gFnSc2	Lucie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
Zelenka	Zelenka	k1gMnSc1	Zelenka
(	(	kIx(	(
<g/>
Michal	Michal	k1gMnSc1	Michal
Zelenka	Zelenka	k1gMnSc1	Zelenka
<g/>
)	)	kIx)	)
–	–	k?	–
číšník	číšník	k1gMnSc1	číšník
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
si	se	k3xPyFc3	se
Standa	Standa	k1gMnSc1	Standa
vylévá	vylévat	k5eAaImIp3nS	vylévat
zlost	zlost	k1gFnSc4	zlost
</s>
</p>
<p>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
Horák	Horák	k1gMnSc1	Horák
(	(	kIx(	(
<g/>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Navrátil	Navrátil	k1gMnSc1	Navrátil
<g/>
)	)	kIx)	)
–	–	k?	–
Janův	Janův	k2eAgMnSc1d1	Janův
a	a	k8xC	a
Standův	Standův	k2eAgMnSc1d1	Standův
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
podvádí	podvádět	k5eAaImIp3nS	podvádět
<g/>
,	,	kIx,	,
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Luciiny	Luciin	k2eAgFnSc2d1	Luciina
matky	matka	k1gFnSc2	matka
Zdeny	Zdena	k1gFnSc2	Zdena
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
dílů	díl	k1gInPc2	díl
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
řada	řada	k1gFnSc1	řada
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Horákovi	Horákův	k2eAgMnPc1d1	Horákův
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
