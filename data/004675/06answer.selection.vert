<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Bosco	Bosco	k1gMnSc1	Bosco
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Don	Don	k1gMnSc1	Don
Bosco	Bosco	k1gMnSc1	Bosco
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
česky	česky	k6eAd1	česky
psán	psán	k2eAgInSc1d1	psán
jako	jako	k8xS	jako
Bosko	bosko	k6eAd1	bosko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Giovanni	Giovanň	k1gMnSc5	Giovanň
Melchiorre	Melchiorr	k1gMnSc5	Melchiorr
Bosco	Bosca	k1gMnSc5	Bosca
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
Castelnuovo	Castelnuovo	k1gNnSc1	Castelnuovo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Asti	Ast	k1gFnSc2	Ast
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Castelnuovo	Castelnuovo	k1gNnSc1	Castelnuovo
Don	dona	k1gFnPc2	dona
Bosco	Bosco	k1gNnSc1	Bosco
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Turín	Turín	k1gInSc1	Turín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
světec	světec	k1gMnSc1	světec
<g/>
,	,	kIx,	,
vychovatel	vychovatel	k1gMnSc1	vychovatel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
první	první	k4xOgFnSc1	první
hlava	hlava	k1gFnSc1	hlava
Salesiánů	salesián	k1gMnPc2	salesián
Dona	Don	k1gMnSc4	Don
Bosca	Boscus	k1gMnSc4	Boscus
<g/>
.	.	kIx.	.
</s>
