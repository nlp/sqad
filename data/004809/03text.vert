<s>
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
攻	攻	k?	攻
<g/>
,	,	kIx,	,
Kókaku	Kókak	k1gMnSc3	Kókak
kidótai	kidóta	k1gMnSc3	kidóta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
překladu	překlad	k1gInSc6	překlad
Zásahová	zásahový	k2eAgFnSc1d1	zásahová
obrněná	obrněný	k2eAgFnSc1d1	obrněná
pořádková	pořádkový	k2eAgFnSc1d1	pořádková
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
post-kyberpunková	postyberpunkový	k2eAgFnSc1d1	post-kyberpunkový
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
započala	započnout	k5eAaPmAgFnS	započnout
mangou	manga	k1gFnSc7	manga
vytvořenou	vytvořený	k2eAgFnSc7d1	vytvořená
Masamunem	Masamun	k1gInSc7	Masamun
Širóem	Širó	k1gInSc7	Širó
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
vycházet	vycházet	k5eAaImF	vycházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Šúkan	Šúkan	k1gMnSc1	Šúkan
Young	Young	k1gMnSc1	Young
Magazine	Magazin	k1gMnSc5	Magazin
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnPc1d1	původní
manga	mango	k1gNnPc1	mango
byla	být	k5eAaImAgNnP	být
zadaptována	zadaptovat	k5eAaPmNgNnP	zadaptovat
do	do	k7c2	do
filmové	filmový	k2eAgFnSc2d1	filmová
podoby	podoba	k1gFnSc2	podoba
<g/>
:	:	kIx,	:
Přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vychází	vycházet	k5eAaImIp3nS	vycházet
anime	animat	k5eAaPmIp3nS	animat
<g/>
:	:	kIx,	:
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
volné	volný	k2eAgNnSc4d1	volné
pokračování	pokračování	k1gNnSc4	pokračování
Ghost	Ghost	k1gMnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
2	[number]	k4	2
<g/>
:	:	kIx,	:
Innocence	Innocence	k1gFnSc2	Innocence
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dva	dva	k4xCgInPc1	dva
kreslené	kreslený	k2eAgInPc1d1	kreslený
televizní	televizní	k2eAgInPc1d1	televizní
seriály	seriál	k1gInPc1	seriál
<g/>
,	,	kIx,	,
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc1	Complex
a	a	k8xC	a
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc1	Complex
2	[number]	k4	2
<g/>
nd	nd	k?	nd
GIG	giga	k1gFnPc2	giga
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dějově	dějově	k6eAd1	dějově
leží	ležet	k5eAaImIp3nP	ležet
někde	někde	k6eAd1	někde
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
dvěma	dva	k4xCgInPc7	dva
filmy	film	k1gInPc7	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
seriály	seriál	k1gInPc4	seriál
pak	pak	k6eAd1	pak
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
animované	animovaný	k2eAgInPc1d1	animovaný
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc4	Complex
–	–	k?	–
Solid	solid	k1gInSc1	solid
State	status	k1gInSc5	status
Society	societa	k1gFnPc4	societa
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
nejnovější	nový	k2eAgInSc1d3	nejnovější
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
filmů	film	k1gInPc2	film
a	a	k8xC	a
seriálů	seriál	k1gInPc2	seriál
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
tří	tři	k4xCgFnPc2	tři
videohry	videohra	k1gFnPc4	videohra
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
na	na	k7c4	na
PlayStation	PlayStation	k1gInSc4	PlayStation
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
na	na	k7c4	na
PlayStation	PlayStation	k1gInSc4	PlayStation
2	[number]	k4	2
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
na	na	k7c4	na
PlayStation	PlayStation	k1gInSc4	PlayStation
Portable	portable	k1gInSc1	portable
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc1	film
a	a	k8xC	a
anime	animat	k5eAaPmIp3nS	animat
seriály	seriál	k1gInPc4	seriál
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
studio	studio	k1gNnSc1	studio
Production	Production	k1gInSc1	Production
I.G.	I.G.	k1gMnSc1	I.G.
Ghost	Ghost	k1gMnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
je	být	k5eAaImIp3nS	být
futuristický	futuristický	k2eAgInSc1d1	futuristický
policejní	policejní	k2eAgInSc1d1	policejní
thriller	thriller	k1gInSc1	thriller
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
žena-kyborg	ženayborg	k1gInSc4	žena-kyborg
Motoko	Motoko	k1gNnSc4	Motoko
Kusanagi	Kusanag	k1gFnSc2	Kusanag
<g/>
,	,	kIx,	,
příslušnice	příslušnice	k1gFnPc4	příslušnice
tajné	tajný	k2eAgFnSc2d1	tajná
divize	divize	k1gFnSc2	divize
Japonské	japonský	k2eAgFnSc2d1	japonská
národní	národní	k2eAgFnSc2d1	národní
veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
komise	komise	k1gFnSc2	komise
známé	známý	k2eAgMnPc4d1	známý
jako	jako	k8xS	jako
Sekce	sekce	k1gFnSc1	sekce
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
trestným	trestný	k2eAgInPc3d1	trestný
činům	čin	k1gInPc3	čin
spojeným	spojený	k2eAgInPc3d1	spojený
s	s	k7c7	s
nelegálním	legální	k2eNgNnSc7d1	nelegální
vyžíváním	vyžívání	k1gNnSc7	vyžívání
moderních	moderní	k2eAgInPc2d1	moderní
(	(	kIx(	(
<g/>
především	především	k9	především
počítačových	počítačový	k2eAgFnPc2d1	počítačová
<g/>
)	)	kIx)	)
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Kusanagi	Kusanagi	k6eAd1	Kusanagi
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
Tří	tři	k4xCgInPc2	tři
císařských	císařský	k2eAgInPc2d1	císařský
klenotů	klenot	k1gInPc2	klenot
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Širó	Širó	k?	Širó
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Kusanagi	Kusanagi	k6eAd1	Kusanagi
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
oslovována	oslovován	k2eAgFnSc1d1	oslovována
a	a	k8xC	a
zmiňována	zmiňován	k2eAgFnSc1d1	zmiňována
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Major	major	k1gMnSc1	major
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
minulosti	minulost	k1gFnSc3	minulost
v	v	k7c6	v
Japonských	japonský	k2eAgFnPc6d1	japonská
obranných	obranný	k2eAgFnPc6d1	obranná
silách	síla	k1gFnPc6	síla
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
konstrukci	konstrukce	k1gFnSc3	konstrukce
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
nadlidských	nadlidský	k2eAgInPc2d1	nadlidský
výkonů	výkon	k1gInPc2	výkon
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
úspěšně	úspěšně	k6eAd1	úspěšně
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
–	–	k?	–
její	její	k3xOp3gNnSc4	její
tělo	tělo	k1gNnSc4	tělo
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc1d1	celé
kompletně	kompletně	k6eAd1	kompletně
umělé	umělý	k2eAgNnSc1d1	umělé
<g/>
;	;	kIx,	;
jen	jen	k9	jen
její	její	k3xOp3gInSc4	její
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
část	část	k1gFnSc4	část
míchy	mícha	k1gFnSc2	mícha
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
organické	organický	k2eAgFnPc1d1	organická
<g/>
.	.	kIx.	.
</s>
<s>
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
je	být	k5eAaImIp3nS	být
kyberpunk	kyberpunk	k6eAd1	kyberpunk
nebo	nebo	k8xC	nebo
post-kyberpunk	postyberpunk	k6eAd1	post-kyberpunk
stavbou	stavba	k1gFnSc7	stavba
podobný	podobný	k2eAgInSc4d1	podobný
dílu	dílo	k1gNnSc3	dílo
Williama	William	k1gMnSc2	William
Gibsona	Gibson	k1gMnSc2	Gibson
<g/>
,	,	kIx,	,
především	především	k9	především
jeho	jeho	k3xOp3gFnSc4	jeho
trilogii	trilogie	k1gFnSc4	trilogie
Sprawl	Sprawl	k1gInSc1	Sprawl
(	(	kIx(	(
<g/>
začínající	začínající	k2eAgMnSc1d1	začínající
slavným	slavný	k2eAgInSc7d1	slavný
Neuromancerem	Neuromancer	k1gInSc7	Neuromancer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kusanagi	Kusanag	k1gMnPc1	Kusanag
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
kolegové	kolega	k1gMnPc1	kolega
čelí	čelit	k5eAaImIp3nP	čelit
jak	jak	k6eAd1	jak
vnějším	vnější	k2eAgFnPc3d1	vnější
hrozbám	hrozba	k1gFnPc3	hrozba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bojují	bojovat	k5eAaImIp3nP	bojovat
s	s	k7c7	s
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
rozpory	rozpor	k1gInPc7	rozpor
vycházejícími	vycházející	k2eAgInPc7d1	vycházející
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
vlastní	vlastní	k2eAgFnSc2d1	vlastní
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
(	(	kIx(	(
<g/>
napsal	napsat	k5eAaBmAgInS	napsat
i	i	k8xC	i
nakreslil	nakreslit	k5eAaPmAgInS	nakreslit
<g/>
)	)	kIx)	)
originální	originální	k2eAgNnSc4d1	originální
mangy	mango	k1gNnPc7	mango
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgNnPc2	dva
pokračování	pokračování	k1gNnPc2	pokračování
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Masamune	Masamun	k1gInSc5	Masamun
Širó	Širó	k?	Širó
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
první	první	k4xOgFnSc1	první
kapitola	kapitola	k1gFnSc1	kapitola
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Šúkan	Šúkan	k1gMnSc1	Šúkan
Young	Young	k1gMnSc1	Young
Magazine	Magazin	k1gInSc5	Magazin
<g/>
.	.	kIx.	.
</s>
<s>
Souborně	souborně	k6eAd1	souborně
(	(	kIx(	(
<g/>
tankō	tankō	k?	tankō
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
svazek	svazek	k1gInSc1	svazek
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
pokračování	pokračování	k1gNnSc1	pokračování
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
2	[number]	k4	2
<g/>
:	:	kIx,	:
Man	mana	k1gFnPc2	mana
<g/>
/	/	kIx~	/
<g/>
Machine	Machin	k1gInSc5	Machin
Interface	interface	k1gInSc1	interface
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
nato	nato	k6eAd1	nato
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
třetí	třetí	k4xOgInSc1	třetí
svazek	svazek	k1gInSc1	svazek
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
1.5	[number]	k4	1.5
<g/>
:	:	kIx,	:
Human-Error	Human-Error	k1gMnSc1	Human-Error
Processor	Processor	k1gMnSc1	Processor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
několik	několik	k4yIc4	několik
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
použity	použít	k5eAaPmNgInP	použít
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
manga	mango	k1gNnSc2	mango
byl	být	k5eAaImAgInS	být
zpracován	zpracovat	k5eAaPmNgInS	zpracovat
také	také	k9	také
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc1	Complex
(	(	kIx(	(
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
Arise	Arise	k1gFnSc1	Arise
série	série	k1gFnSc1	série
(	(	kIx(	(
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ghost	Ghost	k1gMnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
2	[number]	k4	2
<g/>
:	:	kIx,	:
Innocence	Innocence	k1gFnSc1	Innocence
a	a	k8xC	a
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gMnSc1	Shell
<g/>
:	:	kIx,	:
S.	S.	kA	S.
<g/>
A.C.	A.C.	k1gFnSc1	A.C.
Solid	solid	k1gInSc1	solid
State	status	k1gInSc5	status
Society	societa	k1gFnPc1	societa
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
už	už	k9	už
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
a	a	k8xC	a
OVA	OVA	kA	OVA
(	(	kIx(	(
<g/>
Original	Original	k1gMnSc6	Original
Video	video	k1gNnSc1	video
Animation	Animation	k1gInSc1	Animation
<g/>
)	)	kIx)	)
patřících	patřící	k2eAgNnPc2d1	patřící
ke	k	k7c3	k
"	"	kIx"	"
<g/>
značce	značka	k1gFnSc3	značka
<g/>
"	"	kIx"	"
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
vycházejí	vycházet	k5eAaImIp3nP	vycházet
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
jsou	být	k5eAaImIp3nP	být
propojeny	propojen	k2eAgInPc1d1	propojen
s	s	k7c7	s
TV	TV	kA	TV
seriály	seriál	k1gInPc1	seriál
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc4	Complex
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
Arise	Aris	k1gInSc6	Aris
sérii	série	k1gFnSc4	série
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc1	mango
byla	být	k5eAaImAgNnP	být
poprvé	poprvé	k6eAd1	poprvé
upravená	upravený	k2eAgNnPc1d1	upravené
v	v	k7c6	v
anime	animat	k5eAaPmIp3nS	animat
filmové	filmový	k2eAgFnSc3d1	filmová
adaptaci	adaptace	k1gFnSc3	adaptace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
s	s	k7c7	s
názvem	název	k1gInSc7	název
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
režíroval	režírovat	k5eAaImAgMnS	režírovat
Mamoru	Mamora	k1gFnSc4	Mamora
Ošii	Oši	k1gFnSc2	Oši
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc4d1	původní
první	první	k4xOgInSc4	první
film	film	k1gInSc4	film
byl	být	k5eAaImAgInS	být
přepracován	přepracovat	k5eAaPmNgInS	přepracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
použitá	použitý	k2eAgFnSc1d1	použitá
moderní	moderní	k2eAgFnSc1d1	moderní
počítačová	počítačový	k2eAgFnSc1d1	počítačová
animace	animace	k1gFnSc1	animace
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
D-CGI	D-CGI	k1gFnPc2	D-CGI
<g/>
)	)	kIx)	)
a	a	k8xC	a
zvuk	zvuk	k1gInSc1	zvuk
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
nahrán	nahrát	k5eAaPmNgInS	nahrát
(	(	kIx(	(
<g/>
v	v	k7c6	v
6.1	[number]	k4	6.1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
–	–	k?	–
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
matoucím	matoucí	k2eAgInSc7d1	matoucí
názvem	název	k1gInSc7	název
(	(	kIx(	(
<g/>
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
2.0	[number]	k4	2.0
(	(	kIx(	(
<g/>
Redux	Redux	k1gInSc1	Redux
<g/>
)	)	kIx)	)
–	–	k?	–
měla	mít	k5eAaImAgFnS	mít
japonskou	japonský	k2eAgFnSc4d1	japonská
premiéru	premiéra	k1gFnSc4	premiéra
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgNnSc1d1	skutečné
pokračování	pokračování	k1gNnSc1	pokračování
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
2	[number]	k4	2
<g/>
:	:	kIx,	:
Innocence	Innocence	k1gFnSc1	Innocence
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
režíroval	režírovat	k5eAaImAgInS	režírovat
Mamoru	Mamora	k1gFnSc4	Mamora
Ošii	Ošii	k1gNnSc2	Ošii
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
zájmu	zájem	k1gInSc2	zájem
je	být	k5eAaImIp3nS	být
Motoko	Motoko	k1gNnSc1	Motoko
(	(	kIx(	(
<g/>
a	a	k8xC	a
tajemný	tajemný	k2eAgMnSc1d1	tajemný
"	"	kIx"	"
<g/>
Puppet	Puppet	k1gMnSc1	Puppet
Master	master	k1gMnSc1	master
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
pozici	pozice	k1gFnSc4	pozice
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
přebírá	přebírat	k5eAaImIp3nS	přebírat
její	její	k3xOp3gMnSc1	její
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
parťák	parťák	k1gMnSc1	parťák
Bató	Bató	k1gMnSc1	Bató
s	s	k7c7	s
vydatnou	vydatný	k2eAgFnSc7d1	vydatná
pomocí	pomoc	k1gFnSc7	pomoc
Togusy	Togus	k1gInPc1	Togus
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
kolegů	kolega	k1gMnPc2	kolega
známých	známý	k2eAgMnPc2d1	známý
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
ději	děj	k1gInSc6	děj
z	z	k7c2	z
první	první	k4xOgFnSc2	první
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
vychází	vycházet	k5eAaImIp3nS	vycházet
prakticky	prakticky	k6eAd1	prakticky
z	z	k7c2	z
jediné	jediný	k2eAgFnSc2d1	jediná
kapitoly	kapitola	k1gFnSc2	kapitola
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Robot	robot	k1gInSc1	robot
Rondo	rondo	k1gNnSc1	rondo
<g/>
"	"	kIx"	"
doplněné	doplněný	k2eAgInPc1d1	doplněný
některými	některý	k3yIgInPc7	některý
prvky	prvek	k1gInPc7	prvek
z	z	k7c2	z
"	"	kIx"	"
<g/>
Phantom	Phantom	k1gInSc1	Phantom
Fund	fund	k1gInSc1	fund
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
tři	tři	k4xCgInPc1	tři
filmy	film	k1gInPc1	film
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
navázány	navázat	k5eAaPmNgInP	navázat
na	na	k7c4	na
TV	TV	kA	TV
seriály	seriál	k1gInPc1	seriál
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc4	Complex
(	(	kIx(	(
<g/>
vyšly	vyjít	k5eAaPmAgInP	vyjít
i	i	k9	i
společně	společně	k6eAd1	společně
jako	jako	k8xC	jako
Ghost	Ghost	k1gMnSc1	Ghost
In	In	k1gMnSc1	In
The	The	k1gMnSc1	The
Shell	Shell	k1gMnSc1	Shell
–	–	k?	–
S.	S.	kA	S.
<g/>
A.C.	A.C.	k1gFnSc2	A.C.
Trilogy	Triloga	k1gFnSc2	Triloga
Box	box	k1gInSc1	box
Set	set	k1gInSc1	set
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
film	film	k1gInSc4	film
The	The	k1gMnPc2	The
Laughing	Laughing	k1gInSc1	Laughing
Man	mana	k1gFnPc2	mana
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
sestřihem	sestřih	k1gInSc7	sestřih
a	a	k8xC	a
shrnutím	shrnutí	k1gNnSc7	shrnutí
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
částí	část	k1gFnPc2	část
první	první	k4xOgFnSc2	první
série	série	k1gFnSc2	série
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc4	Complex
<g/>
,	,	kIx,	,
pojících	pojící	k2eAgFnPc2d1	pojící
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
už	už	k6eAd1	už
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
)	)	kIx)	)
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
Laughing	Laughing	k1gInSc4	Laughing
Man	mana	k1gFnPc2	mana
incidentu	incident	k1gInSc2	incident
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
druhý	druhý	k4xOgInSc4	druhý
film	film	k1gInSc4	film
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gMnSc1	Shell
<g/>
:	:	kIx,	:
S.	S.	kA	S.
<g/>
A.C.	A.C.	k1gFnSc1	A.C.
2	[number]	k4	2
<g/>
nd	nd	k?	nd
GIG	gig	k1gInSc1	gig
–	–	k?	–
Individual	Individual	k1gInSc1	Individual
Eleven	Elevna	k1gFnPc2	Elevna
je	být	k5eAaImIp3nS	být
převyprávěním	převyprávění	k1gNnSc7	převyprávění
druhé	druhý	k4xOgFnSc2	druhý
série	série	k1gFnSc2	série
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
nd	nd	k?	nd
GIG	gig	k1gInSc1	gig
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
Individual	Individual	k1gMnSc1	Individual
Eleven	Elevna	k1gFnPc2	Elevna
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc4	Complex
–	–	k?	–
Solid	solid	k1gInSc1	solid
State	status	k1gInSc5	status
Society	societa	k1gFnPc1	societa
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
(	(	kIx(	(
<g/>
které	který	k3yIgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
řazeny	řazen	k2eAgFnPc1d1	řazena
spíše	spíše	k9	spíše
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
OVA	OVA	kA	OVA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
sestřihem	sestřih	k1gInSc7	sestřih
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navazuje	navazovat	k5eAaImIp3nS	navazovat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gNnSc7	její
pokračováním	pokračování	k1gNnSc7	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
Tématicky	tématicky	k6eAd1	tématicky
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
k	k	k7c3	k
hackerovi	hacker	k1gMnSc3	hacker
známému	známý	k1gMnSc3	známý
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Puppeteer	Puppeteer	k1gInSc1	Puppeteer
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
loutkař	loutkařit	k5eAaImRp2nS	loutkařit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
režírován	režírován	k2eAgMnSc1d1	režírován
Kendžim	Kendžim	k1gMnSc1	Kendžim
Kamijamou	Kamijama	k1gFnSc7	Kamijama
<g/>
,	,	kIx,	,
premiéra	premiéra	k1gFnSc1	premiéra
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
SKY	SKY	kA	SKY
Perfect	Perfect	k1gInSc4	Perfect
<g/>
,	,	kIx,	,
satelitní	satelitní	k2eAgFnSc4d1	satelitní
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
nejnovějším	nový	k2eAgInSc7d3	nejnovější
přírůstkem	přírůstek	k1gInSc7	přírůstek
je	být	k5eAaImIp3nS	být
OVA	OVA	kA	OVA
série	série	k1gFnSc1	série
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
<g/>
:	:	kIx,	:
Arise	Arise	k1gFnSc1	Arise
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2013	[number]	k4	2013
až	až	k6eAd1	až
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pět	pět	k4xCc4	pět
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
označovaných	označovaný	k2eAgInPc2d1	označovaný
jako	jako	k8xC	jako
Border	Bordra	k1gFnPc2	Bordra
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vyšla	vyjít	k5eAaPmAgFnS	vyjít
také	také	k9	také
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
desetidílného	desetidílný	k2eAgInSc2d1	desetidílný
TV	TV	kA	TV
seriálu	seriál	k1gInSc2	seriál
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Arise	Arise	k1gFnSc1	Arise
–	–	k?	–
Alternative	Alternativ	k1gInSc5	Alternativ
Architecture	Architectur	k1gMnSc5	Architectur
<g/>
)	)	kIx)	)
a	a	k8xC	a
dějově	dějově	k6eAd1	dějově
předchází	předcházet	k5eAaImIp3nS	předcházet
původní	původní	k2eAgInSc1d1	původní
mangu	mango	k1gNnSc3	mango
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
sérii	série	k1gFnSc4	série
navazuje	navazovat	k5eAaImIp3nS	navazovat
další	další	k2eAgNnSc1d1	další
(	(	kIx(	(
<g/>
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
)	)	kIx)	)
anime	animat	k5eAaPmIp3nS	animat
film	film	k1gInSc4	film
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
New	New	k1gFnSc1	New
Movie	Movie	k1gFnSc1	Movie
(	(	kIx(	(
<g/>
taktéž	taktéž	k?	taktéž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
hollywoodské	hollywoodský	k2eAgNnSc1d1	hollywoodské
studio	studio	k1gNnSc4	studio
DreamWorks	DreamWorksa	k1gFnPc2	DreamWorksa
a	a	k8xC	a
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
získali	získat	k5eAaPmAgMnP	získat
práva	práv	k2eAgMnSc4d1	práv
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
hrané	hraný	k2eAgFnSc2d1	hraná
filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
původní	původní	k2eAgFnSc2d1	původní
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
producenti	producent	k1gMnPc1	producent
byli	být	k5eAaImAgMnP	být
navrženi	navržen	k2eAgMnPc1d1	navržen
Avi	Avi	k1gMnPc1	Avi
Arad	Arad	k1gMnSc1	Arad
a	a	k8xC	a
Steven	Steven	k2eAgMnSc1d1	Steven
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
mangy	mango	k1gNnPc7	mango
do	do	k7c2	do
scénáře	scénář	k1gInSc2	scénář
byl	být	k5eAaImAgInS	být
zprvu	zprvu	k6eAd1	zprvu
najat	najat	k2eAgInSc1d1	najat
Jamie	Jamius	k1gMnPc4	Jamius
Moss	Moss	k1gInSc4	Moss
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
ho	on	k3xPp3gMnSc4	on
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
Laeta	Laeta	k1gFnSc1	Laeta
Kalogridis	Kalogridis	k1gFnSc1	Kalogridis
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
hotov	hotov	k2eAgInSc1d1	hotov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k9	ještě
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
změnám	změna	k1gFnPc3	změna
a	a	k8xC	a
posunům	posun	k1gInPc3	posun
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Rupert	Rupert	k1gMnSc1	Rupert
Sanders	Sanders	k1gInSc1	Sanders
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
Jonathan	Jonathan	k1gMnSc1	Jonathan
Herman	Herman	k1gMnSc1	Herman
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
Scarlett	Scarlett	k1gMnSc1	Scarlett
Johansson	Johansson	k1gMnSc1	Johansson
<g/>
)	)	kIx)	)
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
vydání	vydání	k1gNnSc1	vydání
očekává	očekávat	k5eAaImIp3nS	očekávat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
dvě	dva	k4xCgFnPc4	dva
anime	animat	k5eAaPmIp3nS	animat
série	série	k1gFnSc1	série
po	po	k7c6	po
26	[number]	k4	26
dílech	díl	k1gInPc6	díl
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
děj	děj	k1gInSc1	děj
časově	časově	k6eAd1	časově
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
prvního	první	k4xOgMnSc2	první
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
cca	cca	kA	cca
rok	rok	k1gInSc4	rok
2030	[number]	k4	2030
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc1	Complex
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
a	a	k8xC	a
režírován	režírován	k2eAgInSc1d1	režírován
Kendžim	Kendžim	k1gInSc1	Kendžim
Kamijamou	Kamijamý	k2eAgFnSc4d1	Kamijamý
a	a	k8xC	a
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
Production	Production	k1gInSc4	Production
I.G	I.G	k1gFnSc2	I.G
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
odvysíláno	odvysílat	k5eAaPmNgNnS	odvysílat
na	na	k7c6	na
Animaxu	Animax	k1gInSc6	Animax
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pokračování	pokračování	k1gNnSc2	pokračování
Ghost	Ghost	k1gMnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gMnSc1	Shell
<g/>
:	:	kIx,	:
S.	S.	kA	S.
<g/>
A.C.	A.C.	k1gFnSc1	A.C.
2	[number]	k4	2
<g/>
nd	nd	k?	nd
GIG	giga	k1gFnPc2	giga
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
scénáře	scénář	k1gInSc2	scénář
a	a	k8xC	a
režie	režie	k1gFnSc2	režie
opět	opět	k6eAd1	opět
chopil	chopit	k5eAaPmAgMnS	chopit
Kendži	Kendž	k1gFnSc3	Kendž
Kamijama	Kamijam	k1gMnSc2	Kamijam
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
Production	Production	k1gInSc1	Production
I.G	I.G	k1gFnSc2	I.G
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
nd	nd	k?	nd
GIG	gig	k1gInSc1	gig
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2004	[number]	k4	2004
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
seriálům	seriál	k1gInPc3	seriál
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
tři	tři	k4xCgInPc4	tři
filmy	film	k1gInPc4	film
OVA	OVA	kA	OVA
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Laughing	Laughing	k1gInSc1	Laughing
Man	Man	k1gMnSc1	Man
k	k	k7c3	k
první	první	k4xOgFnSc3	první
sérii	série	k1gFnSc3	série
<g/>
,	,	kIx,	,
Individual	Individual	k1gInSc4	Individual
Eleven	Elevna	k1gFnPc2	Elevna
k	k	k7c3	k
2	[number]	k4	2
<g/>
nd	nd	k?	nd
GIG	giga	k1gFnPc2	giga
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
Solid	solid	k1gInSc1	solid
State	status	k1gInSc5	status
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc1	dva
manga	mango	k1gNnPc1	mango
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
,	,	kIx,	,
série	série	k1gFnSc2	série
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
několik	několik	k4yIc4	několik
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
OVA	OVA	kA	OVA
série	série	k1gFnSc1	série
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
<g/>
:	:	kIx,	:
Arise	Arise	k1gFnSc1	Arise
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2013	[number]	k4	2013
až	až	k8xS	až
2015	[number]	k4	2015
vyšla	vyjít	k5eAaPmAgFnS	vyjít
také	také	k9	také
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
TV	TV	kA	TV
seriálu	seriál	k1gInSc2	seriál
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Arise	Arise	k1gFnSc1	Arise
–	–	k?	–
Alternative	Alternativ	k1gInSc5	Alternativ
Architecture	Architectur	k1gMnSc5	Architectur
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
10	[number]	k4	10
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Prvně	Prvně	k?	Prvně
byl	být	k5eAaImAgInS	být
japonskámi	japonská	k1gFnPc7	japonská
televizními	televizní	k2eAgFnPc7d1	televizní
stanicemi	stanice	k1gFnPc7	stanice
vysílán	vysílán	k2eAgInSc4d1	vysílán
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
sérii	série	k1gFnSc3	série
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořen	k2eAgNnPc1d1	vytvořeno
manga	mango	k1gNnPc4	mango
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
After	After	k1gMnSc1	After
the	the	k?	the
Long	Long	k1gMnSc1	Long
Goodbye	Goodby	k1gMnSc2	Goodby
<g/>
:	:	kIx,	:
napsáno	napsat	k5eAaBmNgNnS	napsat
Masakim	Masaki	k1gNnSc7	Masaki
Jamadou	Jamada	k1gFnSc7	Jamada
jako	jako	k9	jako
před-příběh	předříběh	k1gInSc1	před-příběh
ke	k	k7c3	k
Ghost	Ghost	k1gMnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
2	[number]	k4	2
<g/>
:	:	kIx,	:
Innocence	Innocence	k1gFnSc2	Innocence
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Lost	Lost	k1gInSc1	Lost
Memory	Memora	k1gFnSc2	Memora
<g/>
,	,	kIx,	,
Revenge	Reveng	k1gFnSc2	Reveng
of	of	k?	of
the	the	k?	the
Cold	Cold	k1gMnSc1	Cold
Machines	Machines	k1gMnSc1	Machines
a	a	k8xC	a
White	Whit	k1gInSc5	Whit
Maze	Maga	k1gFnSc3	Maga
<g/>
:	:	kIx,	:
trilogie	trilogie	k1gFnSc1	trilogie
novel	novela	k1gFnPc2	novela
napsaná	napsaný	k2eAgFnSc1d1	napsaná
Džun	Džun	k1gInSc1	Džun
<g/>
'	'	kIx"	'
<g/>
ičim	ičim	k1gInSc1	ičim
Fudžisakim	Fudžisakim	k1gInSc1	Fudžisakim
související	související	k2eAgInSc1d1	související
s	s	k7c7	s
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc1	Complex
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
PlayStation	PlayStation	k1gInSc4	PlayStation
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
Exactem	Exacto	k1gNnSc7	Exacto
a	a	k8xC	a
publikována	publikovat	k5eAaBmNgFnS	publikovat
THQ	THQ	kA	THQ
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
hra	hra	k1gFnSc1	hra
nesoucí	nesoucí	k2eAgFnSc1d1	nesoucí
název	název	k1gInSc4	název
anime	animat	k5eAaPmIp3nS	animat
seriálu	seriál	k1gInSc2	seriál
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc1	Complex
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
na	na	k7c4	na
PlayStation	PlayStation	k1gInSc4	PlayStation
2	[number]	k4	2
<g/>
,	,	kIx,	,
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
SCEJ	SCEJ	kA	SCEJ
a	a	k8xC	a
Caviou	Caviá	k1gFnSc4	Caviá
a	a	k8xC	a
publikována	publikovat	k5eAaBmNgFnS	publikovat
společností	společnost	k1gFnSc7	společnost
Bandai	Banda	k1gFnSc2	Banda
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
G-Artists	G-Artists	k1gInSc1	G-Artists
a	a	k8xC	a
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
opět	opět	k6eAd1	opět
společností	společnost	k1gFnSc7	společnost
Bandai	Banda	k1gFnSc2	Banda
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
pro	pro	k7c4	pro
PlayStation	PlayStation	k1gInSc4	PlayStation
Portable	portable	k1gInSc4	portable
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
pokračování	pokračování	k1gNnSc1	pokračování
hry	hra	k1gFnSc2	hra
z	z	k7c2	z
PlayStation	PlayStation	k1gInSc4	PlayStation
2	[number]	k4	2
mělo	mít	k5eAaImAgNnS	mít
kompletně	kompletně	k6eAd1	kompletně
jiný	jiný	k2eAgInSc4d1	jiný
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
nastavení	nastavení	k1gNnSc4	nastavení
a	a	k8xC	a
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
PC	PC	kA	PC
platformu	platforma	k1gFnSc4	platforma
vyšla	vyjít	k5eAaPmAgFnS	vyjít
28.7	[number]	k4	28.7
<g/>
.2016	.2016	k4	.2016
multi-playerová	multilayerová	k1gFnSc1	multi-playerová
střílečka	střílečka	k1gFnSc1	střílečka
s	s	k7c7	s
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc1	Complex
–	–	k?	–
First	First	k1gMnSc1	First
Assault	Assault	k1gMnSc1	Assault
Online	Onlin	k1gInSc5	Onlin
<g/>
"	"	kIx"	"
Filmy	film	k1gInPc1	film
a	a	k8xC	a
televizní	televizní	k2eAgInPc1d1	televizní
seriály	seriál	k1gInPc1	seriál
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
v	v	k7c6	v
alternativní	alternativní	k2eAgFnSc6d1	alternativní
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
film	film	k1gInSc1	film
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2029	[number]	k4	2029
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
pokračování	pokračování	k1gNnSc4	pokračování
Innocence	Innocence	k1gFnSc2	Innocence
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2032	[number]	k4	2032
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
anime	animat	k5eAaPmIp3nS	animat
seriálech	seriál	k1gInPc6	seriál
Ghost	Ghost	k1gMnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shella	k1gFnPc2	Shella
<g/>
:	:	kIx,	:
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
Complex	Complex	k1gInSc1	Complex
a	a	k8xC	a
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gMnSc1	Shell
<g/>
:	:	kIx,	:
S.	S.	kA	S.
<g/>
A.C.	A.C.	k1gFnSc1	A.C.
2	[number]	k4	2
<g/>
nd	nd	k?	nd
GIG	gig	k1gInSc1	gig
je	být	k5eAaImIp3nS	být
děj	děj	k1gInSc4	děj
zařazen	zařazen	k2eAgInSc4d1	zařazen
do	do	k7c2	do
let	léto	k1gNnPc2	léto
2030	[number]	k4	2030
až	až	k6eAd1	až
2032	[number]	k4	2032
<g/>
,	,	kIx,	,
a	a	k8xC	a
film	film	k1gInSc1	film
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gMnSc1	Shell
<g/>
:	:	kIx,	:
S.	S.	kA	S.
<g/>
A.C.	A.C.	k1gFnSc1	A.C.
Solid	solid	k1gInSc1	solid
State	status	k1gInSc5	status
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
mnohé	mnohý	k2eAgFnPc4d1	mnohá
události	událost	k1gFnPc4	událost
ze	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2034	[number]	k4	2034
<g/>
.	.	kIx.	.
</s>
<s>
Sourozenci	sourozenec	k1gMnPc1	sourozenec
Wachowští	Wachowský	k1gMnPc1	Wachowský
<g/>
,	,	kIx,	,
tvůrci	tvůrce	k1gMnPc1	tvůrce
trilogie	trilogie	k1gFnSc2	trilogie
Matrix	Matrix	k1gInSc1	Matrix
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
Matrixu	Matrix	k1gInSc2	Matrix
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
měl	mít	k5eAaImAgInS	mít
vliv	vliv	k1gInSc1	vliv
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
<g/>
.	.	kIx.	.
</s>
<s>
Producent	producent	k1gMnSc1	producent
Joel	Joel	k1gMnSc1	Joel
Silver	Silver	k1gMnSc1	Silver
také	také	k9	také
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
na	na	k7c4	na
DVD	DVD	kA	DVD
pro	pro	k7c4	pro
Animatrix	Animatrix	k1gInSc4	Animatrix
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
styl	styl	k1gInSc4	styl
a	a	k8xC	a
vzhled	vzhled	k1gInSc4	vzhled
Matrixu	Matrix	k1gInSc2	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
byl	být	k5eAaImAgInS	být
analyzován	analyzovat	k5eAaImNgInS	analyzovat
Williamem	William	k1gInSc7	William
Gibsonem	Gibson	k1gInSc7	Gibson
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Neuromancer	Neuromancra	k1gFnPc2	Neuromancra
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
4	[number]	k4	4
Orienting	Orienting	k1gInSc4	Orienting
the	the	k?	the
Future	Futur	k1gMnSc5	Futur
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Wendy	Wenda	k1gFnSc2	Wenda
Hui	Hui	k1gMnSc1	Hui
Kyong	Kyong	k1gMnSc1	Kyong
Chunové	Chunové	k2eAgInSc4d1	Chunové
Control	Control	k1gInSc4	Control
and	and	k?	and
Freedom	Freedom	k1gInSc1	Freedom
–	–	k?	–
Power	Power	k1gInSc1	Power
and	and	k?	and
Paranoia	paranoia	k1gFnSc1	paranoia
in	in	k?	in
the	the	k?	the
Age	Age	k1gFnSc2	Age
of	of	k?	of
Fiber	Fiber	k1gInSc1	Fiber
Optics	Optics	k1gInSc1	Optics
<g/>
.	.	kIx.	.
</s>
<s>
Vypadá	vypadat	k5eAaImIp3nS	vypadat
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
aspekt	aspekt	k1gInSc4	aspekt
vysokého	vysoký	k2eAgInSc2d1	vysoký
technologického	technologický	k2eAgInSc2d1	technologický
orientalismu	orientalismus	k1gInSc2	orientalismus
uvedeného	uvedený	k2eAgInSc2d1	uvedený
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
filmu	film	k1gInSc6	film
na	na	k7c4	na
Manga	mango	k1gNnPc4	mango
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
info	info	k6eAd1	info
Odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
manga	mango	k1gNnSc2	mango
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
OST	OST	kA	OST
<g/>
.	.	kIx.	.
</s>
<s>
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc4	Shell
recenze	recenze	k1gFnSc2	recenze
prvního	první	k4xOgInSc2	první
filmu	film	k1gInSc2	film
na	na	k7c6	na
wiki	wik	k1gFnSc6	wik
serveru	server	k1gInSc2	server
http://www.kyberpunk.org	[url]	k1gInSc4	http://www.kyberpunk.org
FILM	film	k1gInSc1	film
<g/>
:	:	kIx,	:
Ghost	Ghost	k1gInSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc4	Shell
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
Recenze	recenze	k1gFnSc1	recenze
prvního	první	k4xOgNnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
filmu	film	k1gInSc2	film
na	na	k7c6	na
serveru	server	k1gInSc6	server
http://neviditelnypes.lidovky.cz	[url]	k1gInSc1	http://neviditelnypes.lidovky.cz
Odkaz	odkaz	k1gInSc1	odkaz
na	na	k7c4	na
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
PC	PC	kA	PC
hry	hra	k1gFnSc2	hra
</s>
