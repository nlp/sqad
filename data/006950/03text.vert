<p>
<s>
Google	Google	k1gFnSc1	Google
[	[	kIx(	[
<g/>
gu	gu	k?	gu
<g/>
:	:	kIx,	:
<g/>
gl	gl	k?	gl
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c4	v
Mountain	Mountain	k1gInSc4	Mountain
View	View	k1gFnSc2	View
v	v	k7c6	v
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gFnSc2	Vallea
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
;	;	kIx,	;
její	její	k3xOp3gInSc1	její
areál	areál	k1gInSc1	areál
je	být	k5eAaImIp3nS	být
přezdíván	přezdívat	k5eAaImNgInS	přezdívat
Googleplex	Googleplex	k1gInSc1	Googleplex
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
holdingovou	holdingový	k2eAgFnSc7d1	holdingová
společností	společnost	k1gFnSc7	společnost
Alphabet	Alphabet	k1gMnSc1	Alphabet
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
Google	Google	k1gInSc1	Google
oficiálně	oficiálně	k6eAd1	oficiálně
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
pobočkou	pobočka	k1gFnSc7	pobočka
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
díky	díky	k7c3	díky
svému	svůj	k3xOyFgMnSc3	svůj
internetovému	internetový	k2eAgMnSc3d1	internetový
vyhledávači	vyhledávač	k1gMnSc3	vyhledávač
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
popularita	popularita	k1gFnSc1	popularita
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
sloveso	sloveso	k1gNnSc4	sloveso
googlovat	googlovat	k5eAaPmF	googlovat
či	či	k8xC	či
googlit	googlit	k5eAaImF	googlit
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
hledat	hledat	k5eAaImF	hledat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
sloveso	sloveso	k1gNnSc1	sloveso
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c4	v
Oxford	Oxford	k1gInSc4	Oxford
English	English	k1gInSc4	English
Dictionary	Dictionara	k1gFnSc2	Dictionara
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
Google	Google	k1gInSc1	Google
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
dle	dle	k7c2	dle
vzoru	vzor	k1gInSc2	vzor
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
Google	Google	k1gInSc1	Google
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
Googlu	Googl	k1gInSc2	Googl
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
průzkumu	průzkum	k1gInSc2	průzkum
Brandz	Brandz	k1gMnSc1	Brandz
Top	topit	k5eAaImRp2nS	topit
100	[number]	k4	100
agentury	agentura	k1gFnSc2	agentura
Millward	Millwarda	k1gFnPc2	Millwarda
Brown	Brown	k1gMnSc1	Brown
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
deníkem	deník	k1gInSc7	deník
Financial	Financial	k1gMnSc1	Financial
Times	Times	k1gMnSc1	Times
stal	stát	k5eAaPmAgMnS	stát
Google	Google	k1gInSc4	Google
nejcennější	cenný	k2eAgFnSc7d3	nejcennější
značkou	značka	k1gFnSc7	značka
světa	svět	k1gInSc2	svět
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
66,434	[number]	k4	66,434
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
meziroční	meziroční	k2eAgInSc1d1	meziroční
růst	růst	k1gInSc1	růst
o	o	k7c4	o
77	[number]	k4	77
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Google	Google	k1gFnSc2	Google
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
googol	googol	k1gInSc1	googol
<g/>
"	"	kIx"	"
označujícího	označující	k2eAgNnSc2d1	označující
číslo	číslo	k1gNnSc4	číslo
složené	složený	k2eAgNnSc4d1	složené
z	z	k7c2	z
jedničky	jednička	k1gFnSc2	jednička
a	a	k8xC	a
sta	sto	k4xCgNnPc4	sto
nul	nula	k1gFnPc2	nula
(	(	kIx(	(
<g/>
10100	[number]	k4	10100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
použito	použít	k5eAaPmNgNnS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
devítiletým	devítiletý	k2eAgInSc7d1	devítiletý
Miltonem	Milton	k1gInSc7	Milton
Sirrotou	Sirrota	k1gFnSc7	Sirrota
<g/>
,	,	kIx,	,
synovcem	synovec	k1gMnSc7	synovec
amerického	americký	k2eAgMnSc2d1	americký
matematika	matematik	k1gMnSc2	matematik
Edwarda	Edward	k1gMnSc2	Edward
Kasnera	Kasner	k1gMnSc2	Kasner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
dostal	dostat	k5eAaPmAgInS	dostat
Google	Google	k1gInSc1	Google
pokutu	pokuta	k1gFnSc4	pokuta
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
$	$	kIx~	$
od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
nařízení	nařízení	k1gNnSc2	nařízení
GDPR	GDPR	kA	GDPR
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
společnost	společnost	k1gFnSc1	společnost
Google	Google	k1gFnSc2	Google
Alphabet	Alphabet	k1gMnSc1	Alphabet
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
takovouto	takovýto	k3xDgFnSc4	takovýto
částku	částka	k1gFnSc4	částka
schopna	schopen	k2eAgFnSc1d1	schopna
vydělat	vydělat	k5eAaPmF	vydělat
za	za	k7c7	za
asi	asi	k9	asi
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2019	[number]	k4	2019
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
Google	Google	k1gInSc1	Google
čínské	čínský	k2eAgFnSc2d1	čínská
společnosti	společnost	k1gFnSc2	společnost
Huawei	Huawei	k1gNnSc2	Huawei
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
společnosti	společnost	k1gFnPc4	společnost
Huawei	Huawei	k1gNnPc2	Huawei
využívat	využívat	k5eAaImF	využívat
klíčové	klíčový	k2eAgFnPc4d1	klíčová
neotevřené	otevřený	k2eNgFnPc4d1	neotevřená
komponenty	komponenta	k1gFnPc4	komponenta
systému	systém	k1gInSc2	systém
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
dobu	doba	k1gFnSc4	doba
90	[number]	k4	90
dní	den	k1gInPc2	den
mohla	moct	k5eAaImAgFnS	moct
Huawei	Huawe	k1gFnSc3	Huawe
nakupovat	nakupovat	k5eAaBmF	nakupovat
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
komponenty	komponent	k1gInPc4	komponent
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zachování	zachování	k1gNnSc2	zachování
stávajících	stávající	k2eAgFnPc2d1	stávající
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
aktualizací	aktualizace	k1gFnPc2	aktualizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
nových	nový	k2eAgInPc2d1	nový
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Vypovězení	vypovězení	k1gNnSc1	vypovězení
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
znamená	znamenat	k5eAaImIp3nS	znamenat
téměř	téměř	k6eAd1	téměř
úplnou	úplný	k2eAgFnSc4d1	úplná
likvidaci	likvidace	k1gFnSc4	likvidace
společnosti	společnost	k1gFnSc2	společnost
Huawei	Huawe	k1gFnSc2	Huawe
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Internetové	internetový	k2eAgNnSc1d1	internetové
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
Google	Google	k1gFnSc2	Google
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
Google	Google	k1gFnSc2	Google
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejpoužívanější	používaný	k2eAgMnSc1d3	nejpoužívanější
světový	světový	k2eAgMnSc1d1	světový
internetový	internetový	k2eAgMnSc1d1	internetový
vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
každodenně	každodenně	k6eAd1	každodenně
obslouží	obsloužit	k5eAaPmIp3nS	obsloužit
přes	přes	k7c4	přes
dvě	dva	k4xCgFnPc4	dva
miliardy	miliarda	k4xCgFnPc4	miliarda
dotazů	dotaz	k1gInPc2	dotaz
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
47,7	[number]	k4	47,7
<g/>
%	%	kIx~	%
vyhledávání	vyhledávání	k1gNnSc3	vyhledávání
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
:	:	kIx,	:
Nielsen	Nielsen	k2eAgInSc4d1	Nielsen
<g/>
//	//	k?	//
<g/>
NetRatings	NetRatings	k1gInSc4	NetRatings
<g/>
,	,	kIx,	,
vyhledavače	vyhledavač	k1gMnPc4	vyhledavač
<g/>
.	.	kIx.	.
<g/>
info	info	k6eAd1	info
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
umí	umět	k5eAaImIp3nS	umět
prohledávat	prohledávat	k5eAaImF	prohledávat
také	také	k9	také
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
usenetové	usenetový	k2eAgFnPc4d1	usenetový
diskusní	diskusní	k2eAgFnPc4d1	diskusní
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
zpravodajské	zpravodajský	k2eAgInPc4d1	zpravodajský
servery	server	k1gInPc4	server
a	a	k8xC	a
nabídky	nabídka	k1gFnPc4	nabídka
on-line	onin	k1gInSc5	on-lin
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
i	i	k9	i
osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
stránky	stránka	k1gFnPc4	stránka
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
vlastní	vlastní	k2eAgFnSc6d1	vlastní
sociální	sociální	k2eAgFnSc6d1	sociální
síti	síť	k1gFnSc6	síť
Google	Google	k1gFnSc2	Google
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
dokázal	dokázat	k5eAaPmAgInS	dokázat
Google	Google	k1gInSc1	Google
hledat	hledat	k5eAaImF	hledat
v	v	k7c6	v
8,05	[number]	k4	8,05
miliardách	miliarda	k4xCgFnPc6	miliarda
stránek	stránka	k1gFnPc2	stránka
<g/>
,	,	kIx,	,
1,3	[number]	k4	1,3
miliardách	miliarda	k4xCgFnPc6	miliarda
obrázků	obrázek	k1gInPc2	obrázek
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
miliardě	miliarda	k4xCgFnSc3	miliarda
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
diskusních	diskusní	k2eAgFnPc2d1	diskusní
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
tohoto	tento	k3xDgInSc2	tento
obsahu	obsah	k1gInSc2	obsah
také	také	k9	také
archivuje	archivovat	k5eAaBmIp3nS	archivovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
ale	ale	k8xC	ale
mapuje	mapovat	k5eAaImIp3nS	mapovat
přibližně	přibližně	k6eAd1	přibližně
pouze	pouze	k6eAd1	pouze
7	[number]	k4	7
%	%	kIx~	%
webu	web	k1gInSc2	web
<g/>
.	.	kIx.	.
</s>
<s>
Google	Google	k6eAd1	Google
nabízí	nabízet	k5eAaImIp3nS	nabízet
své	svůj	k3xOyFgNnSc4	svůj
rozhraní	rozhraní	k1gNnSc4	rozhraní
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc4	výsledek
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
lze	lze	k6eAd1	lze
omezit	omezit	k5eAaPmF	omezit
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
času	čas	k1gInSc3	čas
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
původně	původně	k6eAd1	původně
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
Sergey	Serge	k1gMnPc4	Serge
Brin	Brina	k1gFnPc2	Brina
a	a	k8xC	a
Larry	Larra	k1gFnSc2	Larra
Page	Pag	k1gInSc2	Pag
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgInSc2	svůj
výzkumu	výzkum	k1gInSc2	výzkum
na	na	k7c6	na
Stanfordově	Stanfordův	k2eAgFnSc6d1	Stanfordova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ověřili	ověřit	k5eAaPmAgMnP	ověřit
funkčnost	funkčnost	k1gFnSc4	funkčnost
svého	svůj	k3xOyFgInSc2	svůj
algoritmu	algoritmus	k1gInSc2	algoritmus
pro	pro	k7c4	pro
ohodnocování	ohodnocování	k1gNnSc4	ohodnocování
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
PageRank	PageRank	k1gMnSc1	PageRank
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvalita	kvalita	k1gFnSc1	kvalita
jeho	jeho	k3xOp3gInPc2	jeho
výsledků	výsledek	k1gInPc2	výsledek
natolik	natolik	k6eAd1	natolik
převyšovala	převyšovat	k5eAaImAgFnS	převyšovat
tehdy	tehdy	k6eAd1	tehdy
dostupné	dostupný	k2eAgMnPc4d1	dostupný
vyhledávače	vyhledávač	k1gMnPc4	vyhledávač
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Google	Google	k1gFnSc1	Google
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
téměř	téměř	k6eAd1	téměř
převálcoval	převálcovat	k5eAaPmAgMnS	převálcovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
řazení	řazení	k1gNnSc2	řazení
výsledků	výsledek	k1gInPc2	výsledek
podle	podle	k7c2	podle
PageRanku	PageRanka	k1gFnSc4	PageRanka
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Googlu	Googl	k1gInSc6	Googl
novinkou	novinka	k1gFnSc7	novinka
i	i	k8xC	i
kladení	kladení	k1gNnSc1	kladení
důrazu	důraz	k1gInSc2	důraz
na	na	k7c4	na
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
frází	fráze	k1gFnPc2	fráze
(	(	kIx(	(
<g/>
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
nestávalo	stávat	k5eNaImAgNnS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
víceslovný	víceslovný	k2eAgInSc1d1	víceslovný
dotaz	dotaz	k1gInSc1	dotaz
vrátil	vrátit	k5eAaPmAgInS	vrátit
stránky	stránka	k1gFnPc4	stránka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
vůbec	vůbec	k9	vůbec
nevyskytovala	vyskytovat	k5eNaImAgNnP	vyskytovat
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
)	)	kIx)	)
a	a	k8xC	a
ukládání	ukládání	k1gNnSc1	ukládání
plného	plný	k2eAgInSc2d1	plný
textu	text	k1gInSc2	text
indexovaných	indexovaný	k2eAgFnPc2d1	indexovaná
stránek	stránka	k1gFnPc2	stránka
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
u	u	k7c2	u
výsledných	výsledný	k2eAgFnPc2d1	výsledná
stránek	stránka	k1gFnPc2	stránka
rovnou	rovnou	k6eAd1	rovnou
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
relevantní	relevantní	k2eAgInPc1d1	relevantní
fragmenty	fragment	k1gInPc1	fragment
textu	text	k1gInSc2	text
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získalo	získat	k5eAaPmAgNnS	získat
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gNnSc2	Google
poprvé	poprvé	k6eAd1	poprvé
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Křišťálové	křišťálový	k2eAgFnSc6d1	Křišťálová
Lupě	lupa	k1gFnSc6	lupa
<g/>
.	.	kIx.	.
<g/>
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
Google	Google	k1gFnSc2	Google
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
jako	jako	k9	jako
kalkulátor	kalkulátor	k1gMnSc1	kalkulátor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
7*5	[number]	k4	7*5
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
(	(	kIx(	(
<g/>
i	i	k8xC	i
pi	pi	k0	pi
<g/>
)	)	kIx)	)
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Eulerova	Eulerův	k2eAgFnSc1d1	Eulerova
rovnost	rovnost	k1gFnSc1	rovnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
převod	převod	k1gInSc1	převod
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
in	in	k?	in
mph	mph	k?	mph
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
užitečné	užitečný	k2eAgFnPc4d1	užitečná
hodnoty	hodnota	k1gFnPc4	hodnota
(	(	kIx(	(
<g/>
speed	speed	k1gInSc1	speed
of	of	k?	of
light	light	k1gInSc1	light
<g/>
,	,	kIx,	,
answer	answer	k1gInSc1	answer
to	ten	k3xDgNnSc1	ten
life	lifat	k5eAaPmIp3nS	lifat
<g/>
,	,	kIx,	,
the	the	k?	the
universe	universe	k1gFnSc1	universe
<g/>
,	,	kIx,	,
and	and	k?	and
everything	everything	k1gInSc1	everything
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
definice	definice	k1gFnSc1	definice
pojmů	pojem	k1gInPc2	pojem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
define	definout	k5eAaPmIp3nS	definout
<g/>
:	:	kIx,	:
<g/>
ad	ad	k7c4	ad
hoc	hoc	k?	hoc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
filmech	film	k1gInPc6	film
a	a	k8xC	a
hercích	herc	k1gInPc6	herc
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hardware	hardware	k1gInSc4	hardware
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c6	na
několika	několik	k4yIc6	několik
serverových	serverový	k2eAgFnPc6d1	serverová
farmách	farma	k1gFnPc6	farma
používajících	používající	k2eAgInPc2d1	používající
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	Linux	kA	Linux
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
farmy	farma	k1gFnPc1	farma
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
distribuovaný	distribuovaný	k2eAgInSc4d1	distribuovaný
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
je	být	k5eAaImIp3nS	být
veškerá	veškerý	k3xTgFnSc1	veškerý
funkcionalita	funkcionalita	k1gFnSc1	funkcionalita
redundantně	redundantně	k6eAd1	redundantně
jištěna	jištěn	k2eAgFnSc1d1	jištěna
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc1d1	přesná
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
vlastnostech	vlastnost	k1gFnPc6	vlastnost
a	a	k8xC	a
umístění	umístění	k1gNnSc4	umístění
této	tento	k3xDgFnSc2	tento
techniky	technika	k1gFnSc2	technika
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Hennessy	Hennessa	k1gFnSc2	Hennessa
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Patterson	Patterson	k1gMnSc1	Patterson
<g/>
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Architecture	Architectur	k1gMnSc5	Architectur
<g/>
:	:	kIx,	:
A	a	k9	a
Quantitative	Quantitativ	k1gInSc5	Quantitativ
Approach	Approach	k1gInSc4	Approach
<g/>
)	)	kIx)	)
tehdy	tehdy	k6eAd1	tehdy
cluster	cluster	k1gInSc1	cluster
Googlu	Googl	k1gInSc2	Googl
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
asi	asi	k9	asi
6 000	[number]	k4	6 000
procesorů	procesor	k1gInPc2	procesor
a	a	k8xC	a
12 000	[number]	k4	12 000
běžných	běžný	k2eAgFnPc2d1	běžná
IDE	IDE	kA	IDE
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
sídlil	sídlit	k5eAaImAgMnS	sídlit
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
:	:	kIx,	:
dvou	dva	k4xCgMnPc6	dva
v	v	k7c6	v
Silicon	Silicon	kA	Silicon
Valley	Valle	k1gMnPc7	Valle
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
lokalita	lokalita	k1gFnSc1	lokalita
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
Internetu	Internet	k1gInSc3	Internet
typu	typ	k1gInSc2	typ
OC	OC	kA	OC
48	[number]	k4	48
(	(	kIx(	(
<g/>
2	[number]	k4	2
488	[number]	k4	488
Mb	Mb	k1gFnPc2	Mb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
;	;	kIx,	;
širokopásmové	širokopásmový	k2eAgNnSc4d1	širokopásmové
internetové	internetový	k2eAgNnSc4d1	internetové
připojení	připojení	k1gNnSc4	připojení
<g/>
)	)	kIx)	)
a	a	k8xC	a
připojení	připojení	k1gNnSc1	připojení
typu	typ	k1gInSc2	typ
OC	OC	kA	OC
12	[number]	k4	12
(	(	kIx(	(
<g/>
622	[number]	k4	622
Mb	Mb	k1gFnPc2	Mb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
lokalitám	lokalita	k1gFnPc3	lokalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
uveřejněných	uveřejněný	k2eAgFnPc2d1	uveřejněná
firmou	firma	k1gFnSc7	firma
Google	Googla	k1gFnSc3	Googla
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
vstupu	vstup	k1gInSc6	vstup
na	na	k7c4	na
burzu	burza	k1gFnSc4	burza
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
se	se	k3xPyFc4	se
současný	současný	k2eAgInSc1d1	současný
obsah	obsah	k1gInSc1	obsah
serverové	serverový	k2eAgFnSc2d1	serverová
farmy	farma	k1gFnSc2	farma
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
719	[number]	k4	719
racků	racek	k1gMnPc2	racek
</s>
</p>
<p>
<s>
63 272	[number]	k4	63 272
počítačů	počítač	k1gInPc2	počítač
</s>
</p>
<p>
<s>
126 544	[number]	k4	126 544
procesorů	procesor	k1gInPc2	procesor
</s>
</p>
<p>
<s>
253 088	[number]	k4	253 088
GHz	GHz	k1gFnSc1	GHz
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
výkonu	výkon	k1gInSc2	výkon
</s>
</p>
<p>
<s>
126 544	[number]	k4	126 544
GiB	GiB	k1gFnSc1	GiB
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
</s>
</p>
<p>
<s>
5 062	[number]	k4	5 062
TiB	tiba	k1gFnPc2	tiba
kapacity	kapacita	k1gFnSc2	kapacita
pevných	pevný	k2eAgInPc2d1	pevný
diskůPodle	diskůPodle	k6eAd1	diskůPodle
tohoto	tento	k3xDgInSc2	tento
odhadu	odhad	k1gInSc2	odhad
by	by	kYmCp3nS	by
serverová	serverový	k2eAgFnSc1d1	serverová
farma	farma	k1gFnSc1	farma
Googlu	Googl	k1gInSc2	Googl
tvořila	tvořit	k5eAaImAgFnS	tvořit
nejvýkonnější	výkonný	k2eAgInSc4d3	nejvýkonnější
superpočítač	superpočítač	k1gInSc4	superpočítač
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
PageRank	PageRank	k1gInSc1	PageRank
===	===	k?	===
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Googlebot	Googlebot	k1gInSc1	Googlebot
pravidelně	pravidelně	k6eAd1	pravidelně
žádá	žádat	k5eAaImIp3nS	žádat
aktuální	aktuální	k2eAgFnSc1d1	aktuální
verze	verze	k1gFnSc1	verze
všech	všecek	k3xTgFnPc2	všecek
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
ví	vědět	k5eAaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
na	na	k7c6	na
nějaké	nějaký	k3yIgFnSc6	nějaký
stránce	stránka	k1gFnSc6	stránka
nalezne	nalézt	k5eAaBmIp3nS	nalézt
(	(	kIx(	(
<g/>
hypertextový	hypertextový	k2eAgInSc4d1	hypertextový
<g/>
)	)	kIx)	)
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
jemu	on	k3xPp3gMnSc3	on
dosud	dosud	k6eAd1	dosud
neznámou	známý	k2eNgFnSc4d1	neznámá
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
přidá	přidat	k5eAaPmIp3nS	přidat
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
databáze	databáze	k1gFnSc2	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Načtené	načtený	k2eAgFnPc4d1	načtená
stránky	stránka	k1gFnPc4	stránka
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
archívu	archív	k1gInSc2	archív
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
a	a	k8xC	a
indexuje	indexovat	k5eAaImIp3nS	indexovat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jejich	jejich	k3xOp3gNnSc1	jejich
rychlé	rychlý	k2eAgNnSc1d1	rychlé
prohledávání	prohledávání	k1gNnSc1	prohledávání
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
archívu	archív	k1gInSc2	archív
a	a	k8xC	a
indexů	index	k1gInPc2	index
zabírají	zabírat	k5eAaImIp3nP	zabírat
několik	několik	k4yIc4	několik
terabytů	terabyt	k1gInPc2	terabyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
hodnocení	hodnocení	k1gNnSc4	hodnocení
stránek	stránka	k1gFnPc2	stránka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
zadanému	zadaný	k2eAgInSc3d1	zadaný
hledanému	hledaný	k2eAgInSc3d1	hledaný
výrazu	výraz	k1gInSc3	výraz
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
Google	Google	k1gFnSc1	Google
algoritmus	algoritmus	k1gInSc4	algoritmus
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
PageRank	PageRank	k1gInSc1	PageRank
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
indexovanou	indexovaný	k2eAgFnSc4d1	indexovaná
stránku	stránka	k1gFnSc4	stránka
specifickým	specifický	k2eAgInSc7d1	specifický
postupem	postup	k1gInSc7	postup
vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
specifická	specifický	k2eAgFnSc1d1	specifická
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
(	(	kIx(	(
<g/>
a	a	k8xC	a
jak	jak	k6eAd1	jak
významných	významný	k2eAgFnPc2d1	významná
<g/>
)	)	kIx)	)
stránek	stránka	k1gFnPc2	stránka
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
stránku	stránka	k1gFnSc4	stránka
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
odkazované	odkazovaný	k2eAgFnPc1d1	odkazovaná
stránky	stránka	k1gFnPc1	stránka
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
důležitější	důležitý	k2eAgNnSc4d2	důležitější
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
výsledcích	výsledek	k1gInPc6	výsledek
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
uvedeny	uveden	k2eAgFnPc1d1	uvedena
na	na	k7c6	na
předních	přední	k2eAgNnPc6d1	přední
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
základního	základní	k2eAgNnSc2d1	základní
kritéria	kritérion	k1gNnSc2	kritérion
používá	používat	k5eAaImIp3nS	používat
Google	Google	k1gFnSc1	Google
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgFnPc2d1	další
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
veřejně	veřejně	k6eAd1	veřejně
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
dokáže	dokázat	k5eAaPmIp3nS	dokázat
indexovat	indexovat	k5eAaImF	indexovat
nejenom	nejenom	k6eAd1	nejenom
textové	textový	k2eAgInPc1d1	textový
a	a	k8xC	a
HTML	HTML	kA	HTML
stránky	stránka	k1gFnSc2	stránka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
dokumenty	dokument	k1gInPc4	dokument
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
dalších	další	k2eAgInPc6d1	další
formátech	formát	k1gInPc6	formát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
PDF	PDF	kA	PDF
<g/>
,	,	kIx,	,
PostScript	PostScript	k1gMnSc1	PostScript
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Word	Word	kA	Word
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Excel	Excel	kA	Excel
či	či	k8xC	či
Shockwave	Shockwav	k1gInSc5	Shockwav
Flash	Flasha	k1gFnPc2	Flasha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
poskytované	poskytovaný	k2eAgFnPc4d1	poskytovaná
Googlem	Googl	k1gInSc7	Googl
==	==	k?	==
</s>
</p>
<p>
<s>
Navíc	navíc	k6eAd1	navíc
k	k	k7c3	k
internetovému	internetový	k2eAgMnSc3d1	internetový
vyhledávači	vyhledávač	k1gMnSc3	vyhledávač
nabízí	nabízet	k5eAaImIp3nS	nabízet
Google	Google	k1gInSc1	Google
některé	některý	k3yIgFnSc2	některý
další	další	k2eAgFnSc2d1	další
služby	služba	k1gFnSc2	služba
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Googl	k1gMnSc5	Googl
AdSense	AdSens	k1gMnSc5	AdSens
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
AdSense	AdSense	k1gFnSc2	AdSense
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
majitele	majitel	k1gMnPc4	majitel
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
libovolného	libovolný	k2eAgInSc2d1	libovolný
rozsahu	rozsah	k1gInSc2	rozsah
rychlý	rychlý	k2eAgMnSc1d1	rychlý
a	a	k8xC	a
snadný	snadný	k2eAgInSc1d1	snadný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
obsahových	obsahový	k2eAgFnPc6d1	obsahová
stránkách	stránka	k1gFnPc6	stránka
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
relevantní	relevantní	k2eAgFnSc2d1	relevantní
textové	textový	k2eAgFnSc2d1	textová
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
vydělávat	vydělávat	k5eAaImF	vydělávat
tím	ten	k3xDgNnSc7	ten
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
reklamy	reklama	k1gFnPc1	reklama
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
uživatelé	uživatel	k1gMnPc1	uživatel
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
hledají	hledat	k5eAaImIp3nP	hledat
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
obsahové	obsahový	k2eAgFnPc4d1	obsahová
stránky	stránka	k1gFnPc4	stránka
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
výdělku	výdělek	k1gInSc3	výdělek
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
obohatit	obohatit	k5eAaPmF	obohatit
odkazy	odkaz	k1gInPc4	odkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Googl	k1gMnSc5	Googl
Ads	Ads	k1gMnSc5	Ads
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
Ads	Ads	k1gMnPc1	Ads
jsou	být	k5eAaImIp3nP	být
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
nakoupit	nakoupit	k5eAaPmF	nakoupit
vysoce	vysoce	k6eAd1	vysoce
cílenou	cílený	k2eAgFnSc4d1	cílená
reklamu	reklama	k1gFnSc4	reklama
placenou	placený	k2eAgFnSc4d1	placená
za	za	k7c4	za
prokliky	proklika	k1gFnPc4	proklika
(	(	kIx(	(
<g/>
PPC	PPC	kA	PPC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Reklamy	reklama	k1gFnPc1	reklama
z	z	k7c2	z
AdWords	AdWordsa	k1gFnPc2	AdWordsa
se	se	k3xPyFc4	se
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
podél	podél	k7c2	podél
výsledků	výsledek	k1gInPc2	výsledek
hledání	hledání	k1gNnSc2	hledání
v	v	k7c6	v
Google	Googla	k1gFnSc6	Googla
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
vyhledávacích	vyhledávací	k2eAgFnPc6d1	vyhledávací
a	a	k8xC	a
obsahových	obsahový	k2eAgFnPc6d1	obsahová
stránkách	stránka	k1gFnPc6	stránka
jeho	jeho	k3xOp3gMnPc2	jeho
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Upozornění	upozornění	k1gNnSc3	upozornění
Google	Google	k1gNnSc3	Google
===	===	k?	===
</s>
</p>
<p>
<s>
Upozornění	upozornění	k1gNnSc1	upozornění
Google	Google	k1gNnSc1	Google
(	(	kIx(	(
<g/>
Google	Google	k1gNnSc1	Google
Alerts	Alertsa	k1gFnPc2	Alertsa
<g/>
)	)	kIx)	)
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
e-mailové	eailové	k2eAgFnPc1d1	e-mailové
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
nejnovějších	nový	k2eAgInPc6d3	nejnovější
výsledcích	výsledek	k1gInPc6	výsledek
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
Google	Google	k1gFnSc2	Google
na	na	k7c6	na
základě	základ	k1gInSc6	základ
předem	předem	k6eAd1	předem
zvoleného	zvolený	k2eAgNnSc2d1	zvolené
tématu	téma	k1gNnSc2	téma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gNnPc1	Google
Analytics	Analyticsa	k1gFnPc2	Analyticsa
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Analytics	Analyticsa	k1gFnPc2	Analyticsa
je	být	k5eAaImIp3nS	být
online	onlinout	k5eAaPmIp3nS	onlinout
aplikace	aplikace	k1gFnSc1	aplikace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
monitorovat	monitorovat	k5eAaImF	monitorovat
návštěvníky	návštěvník	k1gMnPc4	návštěvník
svých	svůj	k3xOyFgFnPc2	svůj
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
<g/>
,	,	kIx,	,
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
sledovat	sledovat	k5eAaImF	sledovat
a	a	k8xC	a
porovnávat	porovnávat	k5eAaImF	porovnávat
podrobné	podrobný	k2eAgFnPc4d1	podrobná
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
aktivitě	aktivita	k1gFnSc6	aktivita
na	na	k7c6	na
webu	web	k1gInSc6	web
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Služba	služba	k1gFnSc1	služba
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Android	android	k1gInSc4	android
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Android	android	k1gInSc1	android
je	být	k5eAaImIp3nS	být
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
určený	určený	k2eAgInSc1d1	určený
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
chytré	chytrý	k2eAgInPc1d1	chytrý
telefony	telefon	k1gInPc1	telefon
<g/>
,	,	kIx,	,
PDA	PDA	kA	PDA
<g/>
,	,	kIx,	,
navigace	navigace	k1gFnPc1	navigace
<g/>
,	,	kIx,	,
tablety	tablet	k1gInPc1	tablet
<g/>
)	)	kIx)	)
vyvinutý	vyvinutý	k2eAgMnSc1d1	vyvinutý
původně	původně	k6eAd1	původně
společností	společnost	k1gFnPc2	společnost
Android	android	k1gInSc1	android
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
akvizicí	akvizice	k1gFnSc7	akvizice
převzal	převzít	k5eAaPmAgInS	převzít
Google	Google	k1gInSc1	Google
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Google	Google	k1gFnSc2	Google
následně	následně	k6eAd1	následně
celou	celý	k2eAgFnSc4d1	celá
platformu	platforma	k1gFnSc4	platforma
i	i	k9	i
se	s	k7c7	s
zdrojovými	zdrojový	k2eAgInPc7d1	zdrojový
kódy	kód	k1gInPc7	kód
předala	předat	k5eAaPmAgFnS	předat
sdružení	sdružení	k1gNnSc4	sdružení
firem	firma	k1gFnPc2	firma
Open	Opena	k1gFnPc2	Opena
Handset	Handset	k1gInSc1	Handset
Alliance	Allianec	k1gInSc2	Allianec
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
je	být	k5eAaImIp3nS	být
také	také	k9	také
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Platforma	platforma	k1gFnSc1	platforma
Android	android	k1gInSc1	android
byla	být	k5eAaImAgFnS	být
ohlášena	ohlášet	k5eAaImNgFnS	ohlášet
5.	[number]	k4	5.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Android	android	k1gInSc1	android
1.	[number]	k4	1.
<g/>
0	[number]	k4	0
byl	být	k5eAaImAgMnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
i	i	k9	i
s	s	k7c7	s
vývojovým	vývojový	k2eAgNnSc7d1	vývojové
prostředím	prostředí	k1gNnSc7	prostředí
představen	představit	k5eAaPmNgInS	představit
23.	[number]	k4	23.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
chytrý	chytrý	k2eAgInSc4d1	chytrý
telefon	telefon	k1gInSc4	telefon
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Android	android	k1gInSc1	android
je	být	k5eAaImIp3nS	být
T-Mobile	T-Mobila	k1gFnSc3	T-Mobila
G1	G1	k1gMnPc2	G1
(	(	kIx(	(
<g/>
od	od	k7c2	od
výrobce	výrobce	k1gMnSc2	výrobce
HTC	HTC	kA	HTC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Android	android	k1gInSc4	android
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
na	na	k7c6	na
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
těchto	tento	k3xDgInPc2	tento
telefonů	telefon	k1gInPc2	telefon
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
ziskovosti	ziskovost	k1gFnSc2	ziskovost
a	a	k8xC	a
Google	Google	k1gFnSc2	Google
platí	platit	k5eAaImIp3nS	platit
výrobcům	výrobce	k1gMnPc3	výrobce
miliardy	miliarda	k4xCgFnSc2	miliarda
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
Google	Google	k1gInSc4	Google
výrobcům	výrobce	k1gMnPc3	výrobce
2.	[number]	k4	2.
<g/>
9	[number]	k4	9
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
o	o	k7c4	o
60	[number]	k4	60
%	%	kIx~	%
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ročně	ročně	k6eAd1	ročně
Google	Google	k1gFnSc1	Google
přispívá	přispívat	k5eAaImIp3nS	přispívat
12	[number]	k4	12
miliardami	miliarda	k4xCgFnPc7	miliarda
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
prodejů	prodej	k1gInPc2	prodej
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
s	s	k7c7	s
Androidem	android	k1gInSc7	android
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vyčísleny	vyčíslen	k2eAgInPc4d1	vyčíslen
na	na	k7c4	na
120	[number]	k4	120
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
Google	Google	k1gInSc1	Google
tak	tak	k6eAd1	tak
přispěl	přispět	k5eAaPmAgInS	přispět
výrobcům	výrobce	k1gMnPc3	výrobce
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gNnPc1	Google
Answers	Answersa	k1gFnPc2	Answersa
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
v	v	k7c6	v
ostrém	ostrý	k2eAgInSc6d1	ostrý
provozu	provoz	k1gInSc6	provoz
od	od	k7c2	od
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
nabízí	nabízet	k5eAaImIp3nS	nabízet
také	také	k9	také
službu	služba	k1gFnSc4	služba
s	s	k7c7	s
názvem	název	k1gInSc7	název
Google	Google	k1gFnSc2	Google
Answers	Answers	k1gInSc1	Answers
(	(	kIx(	(
<g/>
odpovědi	odpověď	k1gFnPc1	odpověď
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nalézt	nalézt	k5eAaBmF	nalézt
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
tazateli	tazatel	k1gMnSc3	tazatel
nepodařilo	podařit	k5eNaPmAgNnS	podařit
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
nalézt	nalézt	k5eAaPmF	nalézt
dostatek	dostatek	k1gInSc4	dostatek
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
položí	položit	k5eAaPmIp3nS	položit
dotaz	dotaz	k1gInSc4	dotaz
a	a	k8xC	a
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
za	za	k7c4	za
jeho	jeho	k3xOp3gMnPc4	jeho
zodpovězení	zodpovězený	k2eAgMnPc1d1	zodpovězený
peněžitou	peněžitý	k2eAgFnSc4d1	peněžitá
odměnu	odměna	k1gFnSc4	odměna
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
$	$	kIx~	$
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
$	$	kIx~	$
<g/>
200	[number]	k4	200
(	(	kIx(	(
<g/>
plus	plus	k6eAd1	plus
fixní	fixní	k2eAgInSc1d1	fixní
poplatek	poplatek	k1gInSc1	poplatek
$	$	kIx~	$
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
pak	pak	k6eAd1	pak
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
dotaz	dotaz	k1gInSc4	dotaz
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
dostane	dostat	k5eAaPmIp3nS	dostat
slíbenou	slíbený	k2eAgFnSc4d1	slíbená
odměnu	odměna	k1gFnSc4	odměna
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
25	[number]	k4	25
%	%	kIx~	%
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
ponechá	ponechat	k5eAaPmIp3nS	ponechat
Google	Google	k1gInSc4	Google
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zodpovězená	zodpovězený	k2eAgFnSc1d1	zodpovězená
otázka	otázka	k1gFnSc1	otázka
však	však	k9	však
poté	poté	k6eAd1	poté
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
volně	volně	k6eAd1	volně
(	(	kIx(	(
<g/>
a	a	k8xC	a
zdarma	zdarma	k6eAd1	zdarma
<g/>
)	)	kIx)	)
přístupná	přístupný	k2eAgFnSc1d1	přístupná
všem	všecek	k3xTgNnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
již	již	k6eAd1	již
služba	služba	k1gFnSc1	služba
nové	nový	k2eAgInPc4d1	nový
dotazy	dotaz	k1gInPc4	dotaz
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gNnSc1	Google
Apps	Appsa	k1gFnPc2	Appsa
for	forum	k1gNnPc2	forum
Work	Work	k1gInSc1	Work
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gNnSc1	Google
Apps	Appsa	k1gFnPc2	Appsa
for	forum	k1gNnPc2	forum
Work	Work	k1gMnSc1	Work
je	být	k5eAaImIp3nS	být
balíček	balíček	k1gMnSc1	balíček
aplikací	aplikace	k1gFnPc2	aplikace
Google	Google	k1gInSc4	Google
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
nasazení	nasazení	k1gNnSc4	nasazení
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
doméně	doména	k1gFnSc6	doména
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
služby	služba	k1gFnPc4	služba
Gmail	Gmaila	k1gFnPc2	Gmaila
<g/>
,	,	kIx,	,
Google	Google	k1gFnSc1	Google
Talk	Talk	k1gInSc1	Talk
<g/>
,	,	kIx,	,
Kalendář	kalendář	k1gInSc1	kalendář
Google	Google	k1gInSc1	Google
<g/>
,	,	kIx,	,
Dokumenty	dokument	k1gInPc1	dokument
Google	Google	k1gFnSc2	Google
<g/>
,	,	kIx,	,
Page	Page	k1gFnSc1	Page
Creator	Creator	k1gMnSc1	Creator
a	a	k8xC	a
Úvodní	úvodní	k2eAgFnSc1d1	úvodní
stránka	stránka	k1gFnSc1	stránka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
Ovládací	ovládací	k2eAgInSc1d1	ovládací
panel	panel	k1gInSc1	panel
správce	správce	k1gMnSc2	správce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
jsou	být	k5eAaImIp3nP	být
nabízeny	nabízet	k5eAaImNgFnP	nabízet
tři	tři	k4xCgFnPc1	tři
verze	verze	k1gFnPc1	verze
<g/>
,	,	kIx,	,
bezplatné	bezplatný	k2eAgNnSc1d1	bezplatné
Standard	standard	k1gInSc4	standard
a	a	k8xC	a
Education	Education	k1gInSc4	Education
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
placená	placený	k2eAgFnSc1d1	placená
Premier	Premier	k1gInSc1	Premier
(	(	kIx(	(
<g/>
zpoplatněno	zpoplatněn	k2eAgNnSc1d1	zpoplatněno
roční	roční	k2eAgFnSc7d1	roční
platbou	platba	k1gFnSc7	platba
za	za	k7c4	za
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
Education	Education	k1gInSc1	Education
a	a	k8xC	a
verze	verze	k1gFnSc1	verze
Premier	Premira	k1gFnPc2	Premira
mají	mít	k5eAaImIp3nP	mít
navíc	navíc	k6eAd1	navíc
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
API	API	kA	API
pro	pro	k7c4	pro
integraci	integrace	k1gFnSc4	integrace
do	do	k7c2	do
stávající	stávající	k2eAgFnSc2d1	stávající
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
k	k	k7c3	k
aplikacím	aplikace	k1gFnPc3	aplikace
a	a	k8xC	a
službám	služba	k1gFnPc3	služba
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
k	k	k7c3	k
nástrojům	nástroj	k1gInPc3	nástroj
pro	pro	k7c4	pro
migraci	migrace	k1gFnSc4	migrace
pošty	pošta	k1gFnSc2	pošta
a	a	k8xC	a
k	k	k7c3	k
nepřetržité	přetržitý	k2eNgFnSc3d1	nepřetržitá
asistenci	asistence	k1gFnSc3	asistence
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
telefonické	telefonický	k2eAgFnSc2d1	telefonická
podpory	podpora	k1gFnSc2	podpora
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
kritických	kritický	k2eAgInPc2d1	kritický
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
není	být	k5eNaImIp3nS	být
bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
verze	verze	k1gFnSc1	verze
dostupná	dostupný	k2eAgFnSc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gFnSc1	Google
Desktop	desktop	k1gInSc1	desktop
Search	Search	k1gInSc1	Search
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gInSc1	Google
Desktop	desktop	k1gInSc1	desktop
Search	Search	k1gInSc4	Search
je	být	k5eAaImIp3nS	být
aplikace	aplikace	k1gFnSc2	aplikace
umožňující	umožňující	k2eAgNnSc1d1	umožňující
prohledávání	prohledávání	k1gNnSc1	prohledávání
e-mailů	eail	k1gInPc2	e-mail
<g/>
,	,	kIx,	,
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
chatů	chat	k1gInPc2	chat
<g/>
,	,	kIx,	,
pošty	pošta	k1gFnSc2	pošta
Gmail	Gmaila	k1gFnPc2	Gmaila
<g/>
,	,	kIx,	,
navštívených	navštívený	k2eAgFnPc2d1	navštívená
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
atd.	atd.	kA	atd.
Aplikace	aplikace	k1gFnSc2	aplikace
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
shromažďování	shromažďování	k1gNnSc1	shromažďování
nových	nový	k2eAgFnPc2d1	nová
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
webu	web	k1gInSc2	web
a	a	k8xC	a
udržování	udržování	k1gNnSc4	udržování
pořádku	pořádek	k1gInSc2	pořádek
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
miniaplikací	miniaplikace	k1gFnPc2	miniaplikace
a	a	k8xC	a
postranního	postranní	k2eAgInSc2d1	postranní
panelu	panel	k1gInSc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
Miniaplikace	Miniaplikace	k1gFnSc1	Miniaplikace
Google	Google	k1gFnSc1	Google
jako	jako	k8xS	jako
např.	např.	kA	např.
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc1	poznámka
apod.	apod.	kA	apod.
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
umístit	umístit	k5eAaPmF	umístit
kamkoliv	kamkoliv	k6eAd1	kamkoliv
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
větší	veliký	k2eAgInSc4d2	veliký
pořádek	pořádek	k1gInSc4	pořádek
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
aplikace	aplikace	k1gFnPc4	aplikace
umístit	umístit	k5eAaPmF	umístit
do	do	k7c2	do
postranního	postranní	k2eAgInSc2d1	postranní
panelu	panel	k1gInSc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
je	být	k5eAaImIp3nS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
i	i	k9	i
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gInSc1	Google
Directory	Director	k1gInPc1	Director
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gInSc1	Google
Directory	Director	k1gInPc1	Director
(	(	kIx(	(
<g/>
Google	Google	k1gFnSc1	Google
adresář	adresář	k1gInSc1	adresář
<g/>
)	)	kIx)	)
vám	vy	k3xPp2nPc3	vy
umožní	umožnit	k5eAaPmIp3nS	umožnit
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
stránky	stránka	k1gFnPc4	stránka
podle	podle	k7c2	podle
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
projektu	projekt	k1gInSc6	projekt
Google	Google	k1gFnSc2	Google
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
katalogem	katalog	k1gInSc7	katalog
Open	Open	k1gMnSc1	Open
Directory	Director	k1gInPc4	Director
Project	Projectum	k1gNnPc2	Projectum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
,	,	kIx,	,
administrován	administrován	k2eAgInSc1d1	administrován
komunitou	komunita	k1gFnSc7	komunita
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
uživatelů	uživatel	k1gMnPc2	uživatel
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Disk	disk	k1gInSc1	disk
Google	Google	k1gFnSc2	Google
===	===	k?	===
</s>
</p>
<p>
<s>
Disk	disk	k1gInSc1	disk
Google	Google	k1gFnSc2	Google
je	být	k5eAaImIp3nS	být
internetová	internetový	k2eAgFnSc1d1	internetová
aplikace	aplikace	k1gFnSc1	aplikace
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
cloudové	cloudový	k2eAgNnSc1d1	cloudový
úložiště	úložiště	k1gNnSc1	úložiště
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
synchronizovat	synchronizovat	k5eAaBmF	synchronizovat
složku	složka	k1gFnSc4	složka
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
lokálním	lokální	k2eAgInSc6d1	lokální
disku	disk	k1gInSc6	disk
se	s	k7c7	s
složkou	složka	k1gFnSc7	složka
v	v	k7c6	v
úložišti	úložiště	k1gNnSc6	úložiště
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
on-line	onin	k1gInSc5	on-lin
vytvářet	vytvářet	k5eAaImF	vytvářet
speciální	speciální	k2eAgInPc4d1	speciální
soubory	soubor	k1gInPc4	soubor
typu	typ	k1gInSc2	typ
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
,	,	kIx,	,
prezentace	prezentace	k1gFnSc1	prezentace
<g/>
,	,	kIx,	,
dotazník	dotazník	k1gInSc1	dotazník
<g/>
,	,	kIx,	,
nákres	nákres	k1gInSc1	nákres
<g/>
.	.	kIx.	.
</s>
<s>
Dokumenty	dokument	k1gInPc1	dokument
Google	Google	k1gFnSc2	Google
Disku	disk	k1gInSc2	disk
jsou	být	k5eAaImIp3nP	být
obdobou	obdoba	k1gFnSc7	obdoba
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
Online	Onlin	k1gMnSc5	Onlin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gNnPc1	Google
Earth	Eartha	k1gFnPc2	Eartha
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
Earth	Earth	k1gInSc1	Earth
je	být	k5eAaImIp3nS	být
virtuální	virtuální	k2eAgInSc1d1	virtuální
glóbus	glóbus	k1gInSc1	glóbus
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Earth	Earth	k1gMnSc1	Earth
Viewer	Viewer	k1gMnSc1	Viewer
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
software	software	k1gInSc1	software
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
firmou	firma	k1gFnSc7	firma
Keyhole	Keyhole	k1gFnPc1	Keyhole
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnPc1	Inc
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zakoupen	zakoupen	k2eAgInSc1d1	zakoupen
portálem	portál	k1gInSc7	portál
Google	Google	k1gFnSc2	Google
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
prohlížet	prohlížet	k5eAaImF	prohlížet
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
a	a	k8xC	a
Měsíc	měsíc	k1gInSc1	měsíc
jako	jako	k8xS	jako
ze	z	k7c2	z
satelitu	satelit	k1gInSc2	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
naklonění	naklonění	k1gNnSc1	naklonění
a	a	k8xC	a
přiblížení	přiblížení	k1gNnSc1	přiblížení
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
rozlišení	rozlišení	k1gNnSc6	rozlišení
<g/>
,	,	kIx,	,
zobrazení	zobrazení	k1gNnSc6	zobrazení
3D	[number]	k4	3D
modelů	model	k1gInPc2	model
větších	veliký	k2eAgNnPc2d2	veliký
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
cestování	cestování	k1gNnSc1	cestování
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
zobrazení	zobrazení	k1gNnSc1	zobrazení
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
nad	nad	k7c7	nad
krajinou	krajina	k1gFnSc7	krajina
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
dostupná	dostupný	k2eAgFnSc1d1	dostupná
tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
také	také	k9	také
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
internetového	internetový	k2eAgInSc2d1	internetový
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
zobrazení	zobrazení	k1gNnSc2	zobrazení
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
Google	Google	k1gFnSc2	Google
Maps	Maps	k1gInSc1	Maps
(	(	kIx(	(
<g/>
po	po	k7c6	po
instalaci	instalace	k1gFnSc6	instalace
Google	Google	k1gNnSc2	Google
Earth	Earth	k1gInSc4	Earth
pluginu	plugin	k1gInSc2	plugin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Skupiny	skupina	k1gFnPc1	skupina
Google	Google	k1gFnPc1	Google
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
archiv	archiv	k1gInSc1	archiv
Usenetových	Usenetový	k2eAgFnPc2d1	Usenetový
diskusních	diskusní	k2eAgFnPc2d1	diskusní
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
e-mailové	eailové	k2eAgFnSc2d1	e-mailové
konference	konference	k1gFnSc2	konference
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Skupiny	skupina	k1gFnPc1	skupina
Google	Google	k1gFnSc2	Google
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
koncem	koncem	k7c2	koncem
90.	[number]	k4	90.
let	léto	k1gNnPc2	léto
tuto	tento	k3xDgFnSc4	tento
službu	služba	k1gFnSc4	služba
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
odlišná	odlišný	k2eAgFnSc1d1	odlišná
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
názvem	název	k1gInSc7	název
DejaNews	DejaNews	k1gInSc1	DejaNews
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
archiv	archiv	k1gInSc1	archiv
Google	Google	k1gNnSc2	Google
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gNnPc2	Google
Chrome	chromat	k5eAaImIp3nS	chromat
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
Chrome	chromat	k5eAaImIp3nS	chromat
je	on	k3xPp3gNnSc4	on
svobodný	svobodný	k2eAgMnSc1d1	svobodný
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gFnSc2	Google
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
open	open	k1gNnSc4	open
source	sourec	k1gInSc2	sourec
frameworku	frameworek	k1gInSc2	frameworek
WebKit	WebKita	k1gFnPc2	WebKita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
od	od	k7c2	od
2.	[number]	k4	2.
září	zářit	k5eAaImIp3nS	zářit
2008	[number]	k4	2008
ve	v	k7c6	v
43	[number]	k4	43
jazycích	jazyk	k1gInPc6	jazyk
včetně	včetně	k7c2	včetně
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Obrázky	obrázek	k1gInPc4	obrázek
Google	Google	k1gInSc1	Google
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gInSc1	Google
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
obrázky	obrázek	k1gInPc4	obrázek
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInPc2	jejich
popisků	popisek	k1gInPc2	popisek
<g/>
,	,	kIx,	,
názvu	název	k1gInSc2	název
a	a	k8xC	a
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Obrázky	obrázek	k1gInPc4	obrázek
Google	Google	k1gFnSc2	Google
<g/>
.	.	kIx.	.
</s>
<s>
Google	Google	k6eAd1	Google
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
zmenšeniny	zmenšenina	k1gFnSc2	zmenšenina
indexovaných	indexovaný	k2eAgInPc2d1	indexovaný
obrázků	obrázek	k1gInPc2	obrázek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
při	při	k7c6	při
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
nabízet	nabízet	k5eAaImF	nabízet
náhledy	náhled	k1gInPc4	náhled
nalezených	nalezený	k2eAgInPc2d1	nalezený
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
lze	lze	k6eAd1	lze
omezit	omezit	k5eAaPmF	omezit
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
velikost	velikost	k1gFnSc4	velikost
obrázku	obrázek	k1gInSc2	obrázek
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
<g/>
,	,	kIx,	,
ikona	ikona	k1gFnSc1	ikona
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
obrázku	obrázek	k1gInSc2	obrázek
(	(	kIx(	(
<g/>
obličej	obličej	k1gInSc1	obličej
<g/>
,	,	kIx,	,
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
klipart	klipart	k1gInSc1	klipart
<g/>
,	,	kIx,	,
kresba	kresba	k1gFnSc1	kresba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc4	barva
obrázku	obrázek	k1gInSc2	obrázek
(	(	kIx(	(
<g/>
libovolná	libovolný	k2eAgFnSc1d1	libovolná
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnPc1d1	plná
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
černobílá	černobílý	k2eAgFnSc1d1	černobílá
nebo	nebo	k8xC	nebo
12	[number]	k4	12
dalších	další	k2eAgFnPc2d1	další
barev	barva	k1gFnPc2	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vyhledaných	vyhledaný	k2eAgInPc2d1	vyhledaný
obrázků	obrázek	k1gInPc2	obrázek
lze	lze	k6eAd1	lze
také	také	k9	také
nastavit	nastavit	k5eAaPmF	nastavit
zobrazení	zobrazení	k1gNnSc4	zobrazení
velikosti	velikost	k1gFnSc2	velikost
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
najetí	najetí	k1gNnSc6	najetí
kurzorem	kurzor	k1gInSc7	kurzor
na	na	k7c4	na
obrázek	obrázek	k1gInSc4	obrázek
lze	lze	k6eAd1	lze
obrázek	obrázek	k1gInSc4	obrázek
trochu	trochu	k6eAd1	trochu
zvětšit	zvětšit	k5eAaPmF	zvětšit
a	a	k8xC	a
zobrazit	zobrazit	k5eAaPmF	zobrazit
tak	tak	k9	tak
název	název	k1gInSc4	název
obrázku	obrázek	k1gInSc2	obrázek
<g/>
,	,	kIx,	,
internetovou	internetový	k2eAgFnSc4d1	internetová
adresu	adresa	k1gFnSc4	adresa
obrázku	obrázek	k1gInSc2	obrázek
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
obrázku	obrázek	k1gInSc2	obrázek
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
podobné	podobný	k2eAgInPc4d1	podobný
obrázky	obrázek	k1gInPc4	obrázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Google	Google	k1gInSc1	Google
zprovoznil	zprovoznit	k5eAaPmAgInS	zprovoznit
webovou	webový	k2eAgFnSc4d1	webová
aplikaci	aplikace	k1gFnSc4	aplikace
Obrázků	obrázek	k1gInPc2	obrázek
Google	Google	k1gFnSc2	Google
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zájemcům	zájemce	k1gMnPc3	zájemce
anotovat	anotovat	k5eAaImF	anotovat
obrázky	obrázek	k1gInPc4	obrázek
z	z	k7c2	z
indexu	index	k1gInSc2	index
Obrázků	obrázek	k1gInPc2	obrázek
Google	Googla	k1gFnSc6	Googla
formou	forma	k1gFnSc7	forma
interaktivní	interaktivní	k2eAgFnSc2d1	interaktivní
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Mapy	mapa	k1gFnPc1	mapa
Google	Google	k1gFnPc1	Google
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Mapy	mapa	k1gFnPc1	mapa
Google	Google	k1gFnSc2	Google
(	(	kIx(	(
<g/>
Google	Google	k1gInSc1	Google
Maps	Maps	k1gInSc1	Maps
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
internetová	internetový	k2eAgFnSc1d1	internetová
mapová	mapový	k2eAgFnSc1d1	mapová
aplikace	aplikace	k1gFnSc1	aplikace
a	a	k8xC	a
technologie	technologie	k1gFnSc1	technologie
poskytována	poskytovat	k5eAaImNgFnS	poskytovat
zdarma	zdarma	k6eAd1	zdarma
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
nekomerční	komerční	k2eNgNnSc4d1	nekomerční
použití	použití	k1gNnSc4	použití
<g/>
)	)	kIx)	)
společností	společnost	k1gFnSc7	společnost
Google	Google	k1gFnSc2	Google
<g/>
.	.	kIx.	.
</s>
<s>
Služba	služba	k1gFnSc1	služba
nabízí	nabízet	k5eAaImIp3nS	nabízet
mapy	mapa	k1gFnPc4	mapa
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
plánovač	plánovač	k1gMnSc1	plánovač
cest	cesta	k1gFnPc2	cesta
pro	pro	k7c4	pro
cestování	cestování	k1gNnSc4	cestování
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
automobilem	automobil	k1gInSc7	automobil
nebo	nebo	k8xC	nebo
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
dopravou	doprava	k1gFnSc7	doprava
a	a	k8xC	a
polohu	poloha	k1gFnSc4	poloha
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mapy	mapa	k1gFnPc1	mapa
Google	Google	k1gFnSc2	Google
nabízejí	nabízet	k5eAaImIp3nP	nabízet
satelitní	satelitní	k2eAgInPc4d1	satelitní
snímky	snímek	k1gInPc4	snímek
ve	v	k7c6	v
vysokém	vysoký	k2eAgInSc6d1	vysoký
rozlišení	rozlišení	k1gNnSc2	rozlišení
mnoha	mnoho	k4c2	mnoho
městských	městský	k2eAgFnPc2d1	městská
oblastí	oblast	k1gFnPc2	oblast
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zprávy	zpráva	k1gFnPc1	zpráva
Google	Google	k1gFnPc1	Google
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2002	[number]	k4	2002
představil	představit	k5eAaPmAgInS	představit
Google	Google	k1gInSc1	Google
beta	beta	k1gNnSc1	beta
verzi	verze	k1gFnSc6	verze
automatického	automatický	k2eAgInSc2d1	automatický
agregátoru	agregátor	k1gInSc2	agregátor
zpráv	zpráva	k1gFnPc2	zpráva
nazvaného	nazvaný	k2eAgMnSc4d1	nazvaný
Zprávy	zpráva	k1gFnPc1	zpráva
Google	Google	k1gInSc4	Google
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
pěti	pět	k4xCc6	pět
jazycích	jazyk	k1gInPc6	jazyk
<g/>
:	:	kIx,	:
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
španělština	španělština	k1gFnSc1	španělština
a	a	k8xC	a
italština	italština	k1gFnSc1	italština
<g/>
.	.	kIx.	.
</s>
<s>
Počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
zcela	zcela	k6eAd1	zcela
automaticky	automaticky	k6eAd1	automaticky
vybírá	vybírat	k5eAaImIp3nS	vybírat
zprávy	zpráva	k1gFnPc4	zpráva
z	z	k7c2	z
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nalezených	nalezený	k2eAgInPc2d1	nalezený
článků	článek	k1gInPc2	článek
nabízí	nabízet	k5eAaImIp3nS	nabízet
prvních	první	k4xOgInPc2	první
200	[number]	k4	200
znaků	znak	k1gInPc2	znak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
text	text	k1gInSc4	text
článku	článek	k1gInSc2	článek
na	na	k7c6	na
původním	původní	k2eAgInSc6d1	původní
serveru	server	k1gInSc6	server
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zprávách	zpráva	k1gFnPc6	zpráva
Google	Google	k1gFnSc2	Google
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
jak	jak	k8xS	jak
pomocí	pomocí	k7c2	pomocí
klíčových	klíčový	k2eAgNnPc2d1	klíčové
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
vydání	vydání	k1gNnSc2	vydání
zprávy	zpráva	k1gFnSc2	zpráva
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
od	od	k7c2	od
data	datum	k1gNnSc2	datum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
zmiňované	zmiňovaný	k2eAgFnSc3d1	zmiňovaná
události	událost	k1gFnSc3	událost
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
podle	podle	k7c2	podle
zdroje	zdroj	k1gInSc2	zdroj
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
tematických	tematický	k2eAgFnPc2d1	tematická
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
uživatelé	uživatel	k1gMnPc1	uživatel
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
přihlásit	přihlásit	k5eAaPmF	přihlásit
k	k	k7c3	k
odběru	odběr	k1gInSc3	odběr
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
zadaným	zadaný	k2eAgInSc7d1	zadaný
klíčovým	klíčový	k2eAgInSc7d1	klíčový
slovům	slovo	k1gNnPc3	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uživateli	uživatel	k1gMnSc3	uživatel
zaslán	zaslán	k2eAgInSc4d1	zaslán
e-mail	eail	k1gInSc4	e-mail
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
Zpravy.google.cz	Zpravy.google.cza	k1gFnPc2	Zpravy.google.cza
i	i	k8xC	i
česká	český	k2eAgFnSc1d1	Česká
betaverze	betaverze	k1gFnSc1	betaverze
této	tento	k3xDgFnSc2	tento
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
spuštěním	spuštění	k1gNnSc7	spuštění
českých	český	k2eAgFnPc2d1	Česká
Zpráv	zpráva	k1gFnPc2	zpráva
Google	Google	k1gNnSc2	Google
čítá	čítat	k5eAaImIp3nS	čítat
rodina	rodina	k1gFnSc1	rodina
Zpráv	zpráva	k1gFnPc2	zpráva
Google	Google	k1gFnSc1	Google
48	[number]	k4	48
regionálních	regionální	k2eAgFnPc2d1	regionální
verzí	verze	k1gFnPc2	verze
v	v	k7c6	v
19	[number]	k4	19
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Nákupy	nákup	k1gInPc1	nákup
Google	Google	k1gFnSc2	Google
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
běží	běžet	k5eAaImIp3nS	běžet
ve	v	k7c6	v
zkušebním	zkušební	k2eAgInSc6d1	zkušební
provozu	provoz	k1gInSc6	provoz
služba	služba	k1gFnSc1	služba
Nákupy	nákup	k1gInPc4	nákup
Google	Google	k1gFnSc2	Google
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
v	v	k7c6	v
předmětech	předmět	k1gInPc6	předmět
nabízených	nabízený	k2eAgInPc6d1	nabízený
k	k	k7c3	k
on-line	onin	k1gInSc5	on-lin
prodeji	prodej	k1gInSc3	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
ve	v	k7c4	v
Wireless	Wireless	k1gInSc4	Wireless
Markup	Markup	k1gInSc1	Markup
Language	language	k1gFnSc1	language
(	(	kIx(	(
<g/>
WML	WML	kA	WML
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
její	její	k3xOp3gNnSc1	její
využití	využití	k1gNnSc1	využití
i	i	k9	i
přes	přes	k7c4	přes
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
přenosná	přenosný	k2eAgNnPc4d1	přenosné
zařízení	zařízení	k1gNnPc4	zařízení
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
WML	WML	kA	WML
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18.	[number]	k4	18.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
služba	služba	k1gFnSc1	služba
Froogle	Froogle	k1gFnSc2	Froogle
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c6	na
Google	Googla	k1gFnSc6	Googla
Product	Product	k1gMnSc1	Product
Search	Search	k1gMnSc1	Search
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Weby	web	k1gInPc4	web
Google	Google	k1gInSc1	Google
===	===	k?	===
</s>
</p>
<p>
<s>
Služba	služba	k1gFnSc1	služba
z	z	k7c2	z
balíku	balík	k1gInSc2	balík
Google	Google	k1gFnSc2	Google
Apps	Appsa	k1gFnPc2	Appsa
umožňující	umožňující	k2eAgMnSc1d1	umožňující
i	i	k8xC	i
bez	bez	k7c2	bez
znalosti	znalost	k1gFnSc2	znalost
programování	programování	k1gNnSc2	programování
vytvořit	vytvořit	k5eAaPmF	vytvořit
internetové	internetový	k2eAgFnPc4d1	internetová
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Postačí	postačit	k5eAaPmIp3nS	postačit
pro	pro	k7c4	pro
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
stránky	stránka	k1gFnPc4	stránka
pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
projekty	projekt	k1gInPc4	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Asistent	asistent	k1gMnSc1	asistent
Google	Google	k1gFnSc2	Google
===	===	k?	===
</s>
</p>
<p>
<s>
Asistent	asistent	k1gMnSc1	asistent
Google	Google	k1gFnSc2	Google
je	být	k5eAaImIp3nS	být
chytrý	chytrý	k2eAgMnSc1d1	chytrý
virtuální	virtuální	k2eAgMnSc1d1	virtuální
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
telefony	telefon	k1gInPc1	telefon
<g/>
,	,	kIx,	,
tablety	tableta	k1gFnPc1	tableta
<g/>
,	,	kIx,	,
reproduktory	reproduktor	k1gInPc1	reproduktor
<g/>
,	,	kIx,	,
chytré	chytrý	k2eAgInPc1d1	chytrý
displeje	displej	k1gInPc1	displej
<g/>
,	,	kIx,	,
televize	televize	k1gFnSc1	televize
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
asi	asi	k9	asi
v	v	k7c6	v
15	[number]	k4	15
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
polštiny	polština	k1gFnSc2	polština
<g/>
,	,	kIx,	,
čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gNnPc1	Google
Scholar	Scholara	k1gFnPc2	Scholara
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Scholar	Scholara	k1gFnPc2	Scholara
je	být	k5eAaImIp3nS	být
aplikace	aplikace	k1gFnPc4	aplikace
vyhledávací	vyhledávací	k2eAgFnSc2d1	vyhledávací
technologie	technologie	k1gFnSc2	technologie
Google	Google	k1gFnSc1	Google
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
akademického	akademický	k2eAgInSc2d1	akademický
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Index	index	k1gInSc1	index
vyhledávače	vyhledávač	k1gMnSc2	vyhledávač
Google	Googl	k1gMnSc2	Googl
Scholar	Scholar	k1gMnSc1	Scholar
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
veřejně	veřejně	k6eAd1	veřejně
přístupný	přístupný	k2eAgInSc1d1	přístupný
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc1	kniha
i	i	k8xC	i
články	článek	k1gInPc1	článek
přístupné	přístupný	k2eAgInPc1d1	přístupný
pouze	pouze	k6eAd1	pouze
uživatelům	uživatel	k1gMnPc3	uživatel
placených	placený	k2eAgMnPc2d1	placený
archívů	archív	k1gInPc2	archív
jako	jako	k8xC	jako
ACM	ACM	kA	ACM
Digital	Digital	kA	Digital
Library	Librara	k1gFnSc2	Librara
či	či	k8xC	či
IEEE	IEEE	kA	IEEE
Xplore	Xplor	k1gInSc5	Xplor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
internetový	internetový	k2eAgInSc4d1	internetový
prohledávač	prohledávač	k1gInSc4	prohledávač
<g/>
,	,	kIx,	,
i	i	k8xC	i
Google	Google	k1gFnSc1	Google
Scholar	Scholara	k1gFnPc2	Scholara
prohledává	prohledávat	k5eAaImIp3nS	prohledávat
plný	plný	k2eAgInSc4d1	plný
text	text	k1gInSc4	text
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
nadto	nadto	k6eAd1	nadto
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
omezit	omezit	k5eAaPmF	omezit
výsledky	výsledek	k1gInPc4	výsledek
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
podle	podle	k7c2	podle
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
vydavatele	vydavatel	k1gMnSc2	vydavatel
<g/>
,	,	kIx,	,
data	datum	k1gNnSc2	datum
publikace	publikace	k1gFnSc2	publikace
<g/>
,	,	kIx,	,
předdefinovaných	předdefinovaný	k2eAgInPc2d1	předdefinovaný
tematických	tematický	k2eAgInPc2d1	tematický
okruhů	okruh	k1gInPc2	okruh
či	či	k8xC	či
části	část	k1gFnSc2	část
titulu	titul	k1gInSc2	titul
hledaného	hledaný	k2eAgInSc2d1	hledaný
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Překladač	překladač	k1gInSc1	překladač
Google	Google	k1gFnSc2	Google
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Překladač	překladač	k1gInSc1	překladač
Google	Google	k1gNnSc1	Google
(	(	kIx(	(
<g/>
Google	Googl	k1gMnSc5	Googl
Translate	Translat	k1gMnSc5	Translat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
on-line	onin	k1gInSc5	on-lin
služba	služba	k1gFnSc1	služba
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
Googlem	Googl	k1gInSc7	Googl
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Překládá	překládat	k5eAaImIp3nS	překládat
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
věty	věta	k1gFnSc2	věta
a	a	k8xC	a
webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
je	být	k5eAaImIp3nS	být
zabudována	zabudovat	k5eAaPmNgFnS	zabudovat
v	v	k7c6	v
nejnovější	nový	k2eAgFnSc6d3	nejnovější
verzi	verze	k1gFnSc6	verze
internetového	internetový	k2eAgMnSc2d1	internetový
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
Google	Googl	k1gMnSc2	Googl
Chrome	chromat	k5eAaImIp3nS	chromat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
automaticky	automaticky	k6eAd1	automaticky
detekuje	detekovat	k5eAaImIp3nS	detekovat
cizí	cizí	k2eAgInSc4d1	cizí
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vysouvací	vysouvací	k2eAgFnSc2d1	vysouvací
nabídky	nabídka	k1gFnSc2	nabídka
pod	pod	k7c7	pod
adresním	adresní	k2eAgInSc7d1	adresní
řádkem	řádek	k1gInSc7	řádek
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
přeložení	přeložení	k1gNnSc4	přeložení
stránky	stránka	k1gFnSc2	stránka
či	či	k8xC	či
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Google	Google	k6eAd1	Google
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
umožnil	umožnit	k5eAaPmAgMnS	umožnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uživatelé	uživatel	k1gMnPc1	uživatel
mohli	moct	k5eAaImAgMnP	moct
pomoci	pomoct	k5eAaPmF	pomoct
s	s	k7c7	s
překládáním	překládání	k1gNnSc7	překládání
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Google	Google	k1gFnSc1	Google
Translate	Translat	k1gInSc5	Translat
Community	Communit	k2eAgInPc4d1	Communit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gFnSc1	Google
Tag	tag	k1gInSc1	tag
Manager	manager	k1gMnSc1	manager
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Google	Google	k1gNnSc1	Google
Tag	tag	k1gInSc1	tag
Manager	manager	k1gMnSc1	manager
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
GTM	GTM	kA	GTM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
jak	jak	k6eAd1	jak
pomocí	pomocí	k7c2	pomocí
jednoho	jeden	k4xCgInSc2	jeden
kódu	kód	k1gInSc2	kód
vkládat	vkládat	k5eAaImF	vkládat
na	na	k7c4	na
web	web	k1gInSc4	web
další	další	k2eAgInPc4d1	další
měřicí	měřicí	k2eAgInPc4d1	měřicí
kódy	kód	k1gInPc4	kód
pouze	pouze	k6eAd1	pouze
přes	přes	k7c4	přes
webové	webový	k2eAgNnSc4d1	webové
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
GTM	GTM	kA	GTM
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
1.	[number]	k4	1.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poskytován	poskytovat	k5eAaImNgInS	poskytovat
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
správu	správa	k1gFnSc4	správa
uživatelských	uživatelský	k2eAgFnPc2d1	Uživatelská
rolí	role	k1gFnPc2	role
a	a	k8xC	a
verzování	verzování	k1gNnPc2	verzování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gFnSc1	Google
Web	web	k1gInSc1	web
API	API	kA	API
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
Web	web	k1gInSc1	web
API	API	kA	API
(	(	kIx(	(
<g/>
též	též	k9	též
Google	Google	k1gInSc1	Google
Web	web	k1gInSc1	web
Services	Services	k1gInSc1	Services
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
aplikační	aplikační	k2eAgNnSc1d1	aplikační
rozhraní	rozhraní	k1gNnSc1	rozhraní
(	(	kIx(	(
<g/>
API	API	kA	API
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
registrovaným	registrovaný	k2eAgMnPc3d1	registrovaný
vývojářům	vývojář	k1gMnPc3	vývojář
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
z	z	k7c2	z
Googlu	Googl	k1gInSc2	Googl
pomocí	pomocí	k7c2	pomocí
rozhraní	rozhraní	k1gNnSc2	rozhraní
SOAP	SOAP	kA	SOAP
<g/>
.	.	kIx.	.
</s>
<s>
Využívání	využívání	k1gNnSc1	využívání
této	tento	k3xDgFnSc2	tento
služby	služba	k1gFnSc2	služba
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
limitem	limit	k1gInSc7	limit
1000	[number]	k4	1000
dotazů	dotaz	k1gInPc2	dotaz
denně	denně	k6eAd1	denně
od	od	k7c2	od
jednoho	jeden	k4xCgMnSc2	jeden
vývojáře	vývojář	k1gMnSc2	vývojář
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
Google	Googl	k1gMnSc2	Googl
Web	web	k1gInSc4	web
API	API	kA	API
fungují	fungovat	k5eAaImIp3nP	fungovat
některé	některý	k3yIgFnPc1	některý
populární	populární	k2eAgFnPc1d1	populární
služby	služba	k1gFnPc1	služba
jako	jako	k8xC	jako
Google	Google	k1gFnPc1	Google
Alert	Alert	k1gInSc1	Alert
(	(	kIx(	(
<g/>
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sledovat	sledovat	k5eAaImF	sledovat
výsledky	výsledek	k1gInPc4	výsledek
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
zadané	zadaný	k2eAgFnSc2d1	zadaná
fráze	fráze	k1gFnSc2	fráze
a	a	k8xC	a
zasílání	zasílání	k1gNnSc2	zasílání
výsledků	výsledek	k1gInPc2	výsledek
e-mailem	eail	k1gInSc7	e-mail
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Google	Googla	k1gFnSc3	Googla
Dance	Danka	k1gFnSc3	Danka
Tool	Toola	k1gFnPc2	Toola
(	(	kIx(	(
<g/>
sledující	sledující	k2eAgNnSc4d1	sledující
procházení	procházení	k1gNnSc4	procházení
Internetu	Internet	k1gInSc2	Internet
GoogleBotem	GoogleBot	k1gInSc7	GoogleBot
a	a	k8xC	a
výpočet	výpočet	k1gInSc4	výpočet
PageRanku	PageRanka	k1gFnSc4	PageRanka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Gmail	Gmail	k1gInSc4	Gmail
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
1.	[number]	k4	1.
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
představil	představit	k5eAaPmAgInS	představit
Google	Google	k1gInSc4	Google
svou	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
službu	služba	k1gFnSc4	služba
s	s	k7c7	s
názvem	název	k1gInSc7	název
Gmail	Gmail	k1gInSc1	Gmail
(	(	kIx(	(
<g/>
Google	Google	k1gInSc1	Google
Mail	mail	k1gInSc1	mail
–	–	k?	–
pošta	pošta	k1gFnSc1	pošta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
webmailový	webmailový	k2eAgInSc1d1	webmailový
server	server	k1gInSc1	server
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
kapacitou	kapacita	k1gFnSc7	kapacita
schránky	schránka	k1gFnSc2	schránka
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
činí	činit	k5eAaImIp3nS	činit
15	[number]	k4	15
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odlišným	odlišný	k2eAgNnSc7d1	odlišné
rozhraním	rozhraní	k1gNnSc7	rozhraní
(	(	kIx(	(
<g/>
vícenásobně	vícenásobně	k6eAd1	vícenásobně
kombinovatelné	kombinovatelný	k2eAgFnPc4d1	kombinovatelná
"	"	kIx"	"
<g/>
záložky	záložka	k1gFnPc4	záložka
<g/>
"	"	kIx"	"
místo	místo	k7c2	místo
rozdělování	rozdělování	k1gNnSc2	rozdělování
pošty	pošta	k1gFnSc2	pošta
do	do	k7c2	do
složek	složka	k1gFnPc2	složka
<g/>
)	)	kIx)	)
a	a	k8xC	a
výkonným	výkonný	k2eAgNnSc7d1	výkonné
vyhledáváním	vyhledávání	k1gNnSc7	vyhledávání
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc4d1	malé
promile	promile	k1gNnSc4	promile
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
tak	tak	k6eAd1	tak
velkou	velký	k2eAgFnSc4d1	velká
schránku	schránka	k1gFnSc4	schránka
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
spuštění	spuštění	k1gNnSc2	spuštění
a	a	k8xC	a
počáteční	počáteční	k2eAgFnSc6d1	počáteční
nabídce	nabídka	k1gFnSc6	nabídka
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
neuvěřitelné	uvěřitelný	k2eNgFnSc2d1	neuvěřitelná
kapacity	kapacita	k1gFnSc2	kapacita
1	[number]	k4	1
GB	GB	kA	GB
(	(	kIx(	(
<g/>
Hotmail	Hotmail	k1gInSc1	Hotmail
tehdy	tehdy	k6eAd1	tehdy
nabízel	nabízet	k5eAaImAgInS	nabízet
2	[number]	k4	2
MB	MB	kA	MB
schránku	schránka	k1gFnSc4	schránka
<g/>
)	)	kIx)	)
existovalo	existovat	k5eAaImAgNnS	existovat
po	po	k7c4	po
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
podezření	podezření	k1gNnSc2	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
jen	jen	k9	jen
o	o	k7c4	o
aprílový	aprílový	k2eAgInSc4d1	aprílový
žert	žert	k1gInSc4	žert
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ověření	ověření	k1gNnSc6	ověření
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
projektu	projekt	k1gInSc6	projekt
Gmail	Gmaila	k1gFnPc2	Gmaila
většina	většina	k1gFnSc1	většina
dosavadních	dosavadní	k2eAgMnPc2d1	dosavadní
provozovatelů	provozovatel	k1gMnPc2	provozovatel
reagovala	reagovat	k5eAaBmAgFnS	reagovat
zvýšením	zvýšení	k1gNnSc7	zvýšení
kapacity	kapacita	k1gFnSc2	kapacita
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Hotmail	Hotmail	k1gInSc1	Hotmail
nyní	nyní	k6eAd1	nyní
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
5	[number]	k4	5
GB	GB	kA	GB
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
automatického	automatický	k2eAgNnSc2d1	automatické
navýšení	navýšení	k1gNnSc2	navýšení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tato	tento	k3xDgNnPc4	tento
zvyšování	zvyšování	k1gNnSc4	zvyšování
u	u	k7c2	u
konkurence	konkurence	k1gFnSc2	konkurence
<g/>
,	,	kIx,	,
Google	Google	k1gInSc1	Google
reagoval	reagovat	k5eAaBmAgInS	reagovat
zavedením	zavedení	k1gNnSc7	zavedení
trvalého	trvalý	k2eAgNnSc2d1	trvalé
zvyšování	zvyšování	k1gNnSc2	zvyšování
kapacity	kapacita	k1gFnSc2	kapacita
–	–	k?	–
aktuální	aktuální	k2eAgFnSc1d1	aktuální
dostupná	dostupný	k2eAgFnSc1d1	dostupná
velikost	velikost	k1gFnSc1	velikost
schránky	schránka	k1gFnSc2	schránka
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
úvodní	úvodní	k2eAgFnSc6d1	úvodní
stránce	stránka	k1gFnSc6	stránka
služby	služba	k1gFnSc2	služba
(	(	kIx(	(
<g/>
http://gmail.com	[url]	k1gInSc1	http://gmail.com
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Kritika	kritika	k1gFnSc1	kritika
====	====	k?	====
</s>
</p>
<p>
<s>
Gmail	Gmail	k1gMnSc1	Gmail
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
kritikou	kritika	k1gFnSc7	kritika
ohledně	ohledně	k7c2	ohledně
otázek	otázka	k1gFnPc2	otázka
ochrany	ochrana	k1gFnSc2	ochrana
soukromí	soukromí	k1gNnSc2	soukromí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
e-mailových	eailův	k2eAgFnPc2d1	e-mailova
zpráv	zpráva	k1gFnPc2	zpráva
se	se	k3xPyFc4	se
v	v	k7c6	v
postranním	postranní	k2eAgInSc6d1	postranní
sloupci	sloupec	k1gInSc6	sloupec
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
reklamy	reklama	k1gFnPc1	reklama
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
cílené	cílený	k2eAgFnPc1d1	cílená
podle	podle	k7c2	podle
obsahu	obsah	k1gInSc2	obsah
čtené	čtený	k2eAgFnSc2d1	čtená
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
cílení	cílení	k1gNnSc1	cílení
je	být	k5eAaImIp3nS	být
však	však	k9	však
prováděno	provádět	k5eAaImNgNnS	provádět
automaticky	automaticky	k6eAd1	automaticky
počítačovým	počítačový	k2eAgInSc7d1	počítačový
programem	program	k1gInSc7	program
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
porušení	porušení	k1gNnSc2	porušení
listovního	listovní	k2eAgNnSc2d1	listovní
tajemství	tajemství	k1gNnSc2	tajemství
nějakým	nějaký	k3yIgMnSc7	nějaký
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
kontroverzním	kontroverzní	k2eAgNnSc7d1	kontroverzní
tématem	téma	k1gNnSc7	téma
byla	být	k5eAaImAgFnS	být
věta	věta	k1gFnSc1	věta
z	z	k7c2	z
podmínek	podmínka	k1gFnPc2	podmínka
používání	používání	k1gNnSc2	používání
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
kopie	kopie	k1gFnSc2	kopie
e-mailů	eail	k1gInPc2	e-mail
mohou	moct	k5eAaImIp3nP	moct
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
zůstat	zůstat	k5eAaPmF	zůstat
uložené	uložený	k2eAgInPc1d1	uložený
na	na	k7c6	na
serverech	server	k1gInPc6	server
Google	Google	k1gFnSc2	Google
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
uživatel	uživatel	k1gMnSc1	uživatel
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
účtu	účet	k1gInSc2	účet
smazal	smazat	k5eAaPmAgMnS	smazat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
účet	účet	k1gInSc1	účet
úplně	úplně	k6eAd1	úplně
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
obávalo	obávat	k5eAaImAgNnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Google	Google	k1gFnSc1	Google
hodlá	hodlat	k5eAaImIp3nS	hodlat
trvale	trvale	k6eAd1	trvale
archivovat	archivovat	k5eAaBmF	archivovat
jejich	jejich	k3xOp3gFnSc4	jejich
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zástupci	zástupce	k1gMnPc1	zástupce
Google	Google	k1gFnSc2	Google
objasnili	objasnit	k5eAaPmAgMnP	objasnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
technický	technický	k2eAgInSc4d1	technický
požadavek	požadavek	k1gInSc4	požadavek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Google	Google	k1gFnSc1	Google
se	se	k3xPyFc4	se
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
podniknout	podniknout	k5eAaPmF	podniknout
rozumné	rozumný	k2eAgInPc4d1	rozumný
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
smazaných	smazaný	k2eAgFnPc2d1	smazaná
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
co	co	k9	co
nejrychlejším	rychlý	k2eAgInSc6d3	nejrychlejší
prakticky	prakticky	k6eAd1	prakticky
dosažitelném	dosažitelný	k2eAgInSc6d1	dosažitelný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Picasa	Picasa	k1gFnSc1	Picasa
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Picasa	Picasa	k1gFnSc1	Picasa
je	být	k5eAaImIp3nS	být
freewarový	freewarový	k2eAgInSc1d1	freewarový
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
katalogizaci	katalogizace	k1gFnSc4	katalogizace
<g/>
,	,	kIx,	,
úpravu	úprava	k1gFnSc4	úprava
a	a	k8xC	a
předvádění	předvádění	k1gNnSc4	předvádění
obrázků	obrázek	k1gInPc2	obrázek
<g/>
,	,	kIx,	,
především	především	k9	především
digitálních	digitální	k2eAgFnPc2d1	digitální
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
katalogizaci	katalogizace	k1gFnSc4	katalogizace
do	do	k7c2	do
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
přidávání	přidávání	k1gNnSc1	přidávání
tagů	tag	k1gInPc2	tag
<g/>
,	,	kIx,	,
objednávání	objednávání	k1gNnSc1	objednávání
tisku	tisk	k1gInSc2	tisk
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalšího	další	k2eAgNnSc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
verzi	verze	k1gFnSc6	verze
je	být	k5eAaImIp3nS	být
integrované	integrovaný	k2eAgNnSc1d1	integrované
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
Picasa	Picas	k1gMnSc4	Picas
Web	web	k1gInSc4	web
Albums	Albums	k1gInSc1	Albums
–	–	k?	–
web	web	k1gInSc1	web
pro	pro	k7c4	pro
online	onlinout	k5eAaPmIp3nS	onlinout
sdílení	sdílení	k1gNnSc4	sdílení
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
např.	např.	kA	např.
Flickru	Flickr	k1gInSc6	Flickr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
lze	lze	k6eAd1	lze
provádět	provádět	k5eAaImF	provádět
základní	základní	k2eAgFnPc4d1	základní
úpravy	úprava	k1gFnPc4	úprava
jasu	jas	k1gInSc2	jas
a	a	k8xC	a
barevného	barevný	k2eAgNnSc2d1	barevné
podání	podání	k1gNnSc2	podání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
filtrů	filtr	k1gInPc2	filtr
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
úpravy	úprava	k1gFnPc1	úprava
neprovádí	provádět	k5eNaImIp3nP	provádět
na	na	k7c6	na
originálu	originál	k1gInSc6	originál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
programu	program	k1gInSc6	program
–	–	k?	–
i	i	k9	i
přesto	přesto	k8xC	přesto
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
Picasa	Picasa	k1gFnSc1	Picasa
nadstandardní	nadstandardní	k2eAgFnSc1d1	nadstandardní
rychlost	rychlost	k1gFnSc4	rychlost
prohlížení	prohlížení	k1gNnSc2	prohlížení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
SketchUp	SketchUp	k1gInSc4	SketchUp
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
SketchUp	SketchUp	k1gInSc1	SketchUp
je	být	k5eAaImIp3nS	být
CAD	CAD	kA	CAD
software	software	k1gInSc1	software
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
3D	[number]	k4	3D
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trhu	trh	k1gInSc6	trh
je	být	k5eAaImIp3nS	být
verze	verze	k1gFnSc1	verze
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nejen	nejen	k6eAd1	nejen
vytvářet	vytvářet	k5eAaImF	vytvářet
3D	[number]	k4	3D
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
texturovat	texturovat	k5eAaPmF	texturovat
jejich	jejich	k3xOp3gInSc4	jejich
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
také	také	k9	také
geografické	geografický	k2eAgNnSc4d1	geografické
umístění	umístění	k1gNnSc4	umístění
kdekoliv	kdekoliv	k6eAd1	kdekoliv
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Google	Google	k1gNnSc2	Google
Earth	Eartha	k1gFnPc2	Eartha
a	a	k8xC	a
propojení	propojení	k1gNnSc2	propojení
se	se	k3xPyFc4	se
softwarem	software	k1gInSc7	software
GIS	gis	k1gNnSc2	gis
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
verze	verze	k1gFnPc1	verze
–	–	k?	–
základní	základní	k2eAgFnSc1d1	základní
bezplatná	bezplatný	k2eAgFnSc1d1	bezplatná
a	a	k8xC	a
pokročilá	pokročilý	k2eAgFnSc1d1	pokročilá
placená	placený	k2eAgFnSc1d1	placená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Googla	k1gFnSc6	Googla
<g/>
+	+	kIx~	+
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
<g/>
+	+	kIx~	+
(	(	kIx(	(
<g/>
také	také	k9	také
Google	Google	k1gInSc1	Google
Plus	plus	k1gInSc1	plus
nebo	nebo	k8xC	nebo
zkratka	zkratka	k1gFnSc1	zkratka
G	G	kA	G
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sociální	sociální	k2eAgFnSc1d1	sociální
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Google	Google	k1gInSc1	Google
uvedl	uvést	k5eAaPmAgInS	uvést
28.	[number]	k4	28.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
přímou	přímý	k2eAgFnSc4d1	přímá
konkurenci	konkurence	k1gFnSc4	konkurence
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
proti	proti	k7c3	proti
největšímu	veliký	k2eAgMnSc3d3	veliký
konkurentovi	konkurent	k1gMnSc3	konkurent
jsou	být	k5eAaImIp3nP	být
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
kruhy	kruh	k1gInPc4	kruh
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgFnPc2	který
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
přidávat	přidávat	k5eAaImF	přidávat
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
pohodlně	pohodlně	k6eAd1	pohodlně
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
poté	poté	k6eAd1	poté
sdílet	sdílet	k5eAaImF	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zbraní	zbraň	k1gFnSc7	zbraň
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
provázanost	provázanost	k1gFnSc1	provázanost
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
služby	služba	k1gFnPc4	služba
Googlu	Googl	k1gInSc2	Googl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
uvedení	uvedení	k1gNnSc2	uvedení
fungovala	fungovat	k5eAaImAgFnS	fungovat
síť	síť	k1gFnSc1	síť
jen	jen	k9	jen
jako	jako	k8xS	jako
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
komunita	komunita	k1gFnSc1	komunita
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dostat	dostat	k5eAaPmF	dostat
jen	jen	k9	jen
s	s	k7c7	s
pozvánkou	pozvánka	k1gFnSc7	pozvánka
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc2	který
byl	být	k5eAaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
do	do	k7c2	do
Google	Google	k1gFnSc2	Google
<g/>
+	+	kIx~	+
proniklo	proniknout	k5eAaPmAgNnS	proniknout
přes	přes	k7c4	přes
osm	osm	k4xCc4	osm
milionů	milion	k4xCgInPc2	milion
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hardware	hardware	k1gInSc4	hardware
a	a	k8xC	a
sítě	síť	k1gFnPc4	síť
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
začíná	začínat	k5eAaImIp3nS	začínat
pronikat	pronikat	k5eAaImF	pronikat
i	i	k9	i
do	do	k7c2	do
hardwarové	hardwarový	k2eAgFnSc2d1	hardwarová
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
představil	představit	k5eAaPmAgMnS	představit
sérii	série	k1gFnSc4	série
Google	Google	k1gFnSc2	Google
Nexus	nexus	k1gInSc1	nexus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
smartphony	smartphona	k1gFnPc4	smartphona
jako	jako	k8xC	jako
Nexus	nexus	k1gInSc4	nexus
One	One	k1gFnSc2	One
<g/>
,	,	kIx,	,
Nexus	nexus	k1gInSc4	nexus
S	s	k7c7	s
a	a	k8xC	a
Galaxy	Galax	k1gInPc4	Galax
Nexus	nexus	k1gInSc1	nexus
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
tablet	tablet	k1gInSc4	tablet
Nexus	nexus	k1gInSc1	nexus
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sérii	série	k1gFnSc4	série
Google	Google	k1gFnSc2	Google
Nexus	nexus	k1gInSc1	nexus
vždy	vždy	k6eAd1	vždy
Google	Google	k1gInSc1	Google
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
výrobci	výrobce	k1gMnPc7	výrobce
mobilního	mobilní	k2eAgInSc2d1	mobilní
hardwaru	hardware	k1gInSc2	hardware
<g/>
,	,	kIx,	,
např.	např.	kA	např.
se	s	k7c7	s
Samsungem	Samsung	k1gInSc7	Samsung
na	na	k7c4	na
Galaxy	Galax	k1gInPc4	Galax
Nexus	nexus	k1gInSc4	nexus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
také	také	k9	také
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
poskytování	poskytování	k1gNnSc4	poskytování
připojení	připojení	k1gNnSc2	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
a	a	k8xC	a
internetové	internetový	k2eAgFnSc2d1	internetová
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
Google	Google	k1gNnPc2	Google
zahájil	zahájit	k5eAaPmAgMnS	zahájit
projekt	projekt	k1gInSc4	projekt
Google	Googla	k1gFnSc3	Googla
Fiber	Fiber	k1gInSc1	Fiber
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
poskytnout	poskytnout	k5eAaPmF	poskytnout
levné	levný	k2eAgNnSc1d1	levné
vysokorychlostní	vysokorychlostní	k2eAgNnSc1d1	vysokorychlostní
optické	optický	k2eAgNnSc1d1	optické
připojení	připojení	k1gNnSc1	připojení
do	do	k7c2	do
každé	každý	k3xTgFnSc2	každý
domácnosti	domácnost	k1gFnSc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Google	Google	k1gInSc1	Google
začal	začít	k5eAaPmAgInS	začít
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
první	první	k4xOgFnSc2	první
optické	optický	k2eAgFnSc2d1	optická
sítě	síť	k1gFnSc2	síť
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
Missouri	Missouri	k1gFnSc2	Missouri
a	a	k8xC	a
Kansas	Kansas	k1gInSc1	Kansas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
občas	občas	k6eAd1	občas
uvede	uvést	k5eAaPmIp3nS	uvést
namísto	namísto	k7c2	namísto
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
loga	logo	k1gNnSc2	logo
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
Google	Google	k1gInSc4	Google
Doodle	Doodle	k1gFnSc2	Doodle
(	(	kIx(	(
<g/>
Sváteční	sváteční	k2eAgFnSc1d1	sváteční
logo	logo	k1gNnSc4	logo
Google	Google	k1gInSc1	Google
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
humorná	humorný	k2eAgFnSc1d1	humorná
varianta	varianta	k1gFnSc1	varianta
nápisu	nápis	k1gInSc2	nápis
Google	Google	k1gFnSc2	Google
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
na	na	k7c6	na
serverech	server	k1gInPc6	server
Google	Google	k1gNnSc2	Google
zpřístupnil	zpřístupnit	k5eAaPmAgInS	zpřístupnit
americký	americký	k2eAgInSc1d1	americký
časopis	časopis	k1gInSc1	časopis
LIFE	LIFE	kA	LIFE
svůj	svůj	k3xOyFgInSc4	svůj
archiv	archiv	k1gInSc4	archiv
otištěných	otištěný	k2eAgFnPc2d1	otištěná
fotografií	fotografia	k1gFnPc2	fotografia
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ukončené	ukončený	k2eAgFnPc4d1	ukončená
služby	služba	k1gFnPc4	služba
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Googl	k1gMnSc5	Googl
Code	Codus	k1gMnSc5	Codus
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Code	Cod	k1gInSc2	Cod
je	být	k5eAaImIp3nS	být
portál	portál	k1gInSc1	portál
Google	Google	k1gNnSc2	Google
pro	pro	k7c4	pro
vývojáře	vývojář	k1gMnSc4	vývojář
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
informace	informace	k1gFnSc1	informace
o	o	k7c4	o
API	API	kA	API
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
některé	některý	k3yIgInPc1	některý
softwarové	softwarový	k2eAgInPc1d1	softwarový
balíky	balík	k1gInPc1	balík
pod	pod	k7c7	pod
open-source	openourka	k1gFnSc6	open-sourka
licencí	licence	k1gFnPc2	licence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
takovéhoto	takovýto	k3xDgInSc2	takovýto
balíku	balík	k1gInSc2	balík
je	být	k5eAaImIp3nS	být
Google	Google	k1gFnSc1	Google
Web	web	k1gInSc4	web
Toolkit	Toolkit	k1gInSc1	Toolkit
(	(	kIx(	(
<g/>
GWT	GWT	kA	GWT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Google	Google	k1gInSc4	Google
Web	web	k1gInSc1	web
Toolkit	Toolkit	k1gInSc1	Toolkit
–	–	k?	–
návod	návod	k1gInSc1	návod
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
GWT	GWT	kA	GWT
pro	pro	k7c4	pro
vývojáře	vývojář	k1gMnSc4	vývojář
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Služba	služba	k1gFnSc1	služba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
pro	pro	k7c4	pro
nové	nový	k2eAgInPc4d1	nový
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
aktualizace	aktualizace	k1gFnSc2	aktualizace
stávajících	stávající	k2eAgInPc2d1	stávající
projektů	projekt	k1gInPc2	projekt
bude	být	k5eAaImBp3nS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
24.	[number]	k4	24.
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
25.	[number]	k4	25.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
bude	být	k5eAaImBp3nS	být
služba	služba	k1gFnSc1	služba
ukončena	ukončit	k5eAaPmNgFnS	ukončit
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgFnPc1d1	dostupná
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
pouze	pouze	k6eAd1	pouze
balíčky	balíček	k1gInPc1	balíček
tar	tar	k?	tar
se	s	k7c7	s
zdrojovými	zdrojový	k2eAgInPc7d1	zdrojový
kódy	kód	k1gInPc7	kód
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Orkut	Orkut	k2eAgMnSc1d1	Orkut
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Orkut	Orkut	k1gInSc1	Orkut
je	být	k5eAaImIp3nS	být
komunitní	komunitní	k2eAgInSc1d1	komunitní
server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
socioware	sociowar	k1gInSc5	sociowar
<g/>
.	.	kIx.	.
</s>
<s>
Sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
společné	společný	k2eAgInPc4d1	společný
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
bydliště	bydliště	k1gNnPc4	bydliště
<g/>
,	,	kIx,	,
národnost	národnost	k1gFnSc4	národnost
<g/>
,	,	kIx,	,
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
znají	znát	k5eAaImIp3nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Orkut	Orkut	k1gInSc1	Orkut
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Orkut	Orkut	k2eAgMnSc1d1	Orkut
Büyükkökten	Büyükkökten	k2eAgMnSc1d1	Büyükkökten
<g/>
,	,	kIx,	,
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gFnSc2	Google
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
i	i	k9	i
provozuje	provozovat	k5eAaImIp3nS	provozovat
<g/>
.	.	kIx.	.
</s>
<s>
Služba	služba	k1gFnSc1	služba
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
30.	[number]	k4	30.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
a	a	k8xC	a
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
komplexnější	komplexní	k2eAgFnSc7d2	komplexnější
sociální	sociální	k2eAgFnSc7d1	sociální
sítí	síť	k1gFnSc7	síť
Google	Google	k1gFnSc2	Google
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Google	k1gNnPc1	Google
Reader	Readra	k1gFnPc2	Readra
===	===	k?	===
</s>
</p>
<p>
<s>
Služba	služba	k1gFnSc1	služba
Google	Google	k1gFnSc2	Google
Reader	Readra	k1gFnPc2	Readra
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
webová	webový	k2eAgFnSc1d1	webová
čtečka	čtečka	k1gFnSc1	čtečka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
lze	lze	k6eAd1	lze
přistupovat	přistupovat	k5eAaImF	přistupovat
pouze	pouze	k6eAd1	pouze
přes	přes	k7c4	přes
webové	webový	k2eAgNnSc4d1	webové
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
syndikaci	syndikace	k1gFnSc4	syndikace
obsahu	obsah	k1gInSc2	obsah
využívá	využívat	k5eAaPmIp3nS	využívat
rodiny	rodina	k1gFnPc1	rodina
formátu	formát	k1gInSc2	formát
Atom	atom	k1gInSc1	atom
a	a	k8xC	a
RSS	RSS	kA	RSS
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
Google	Google	k1gFnSc2	Google
Readeru	Reader	k1gInSc2	Reader
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
k	k	k7c3	k
1.	[number]	k4	1.
červenci	červenec	k1gInSc3	červenec
2013.	[number]	k4	2013.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Googl	k1gMnSc5	Googl
Wave	Wavus	k1gMnSc5	Wavus
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
představený	představený	k2eAgInSc1d1	představený
společností	společnost	k1gFnSc7	společnost
Google	Google	k1gFnSc1	Google
28.	[number]	k4	28.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
Google	Google	k1gFnSc2	Google
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
O.	O.	kA	O.
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
webovou	webový	k2eAgFnSc4d1	webová
aplikaci	aplikace	k1gFnSc4	aplikace
a	a	k8xC	a
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
platformu	platforma	k1gFnSc4	platforma
snažící	snažící	k2eAgFnSc4d1	snažící
se	se	k3xPyFc4	se
sjednotit	sjednotit	k5eAaPmF	sjednotit
e-mail	eail	k1gInSc1	e-mail
<g/>
,	,	kIx,	,
instant	instant	k?	instant
messaging	messaging	k1gInSc4	messaging
<g/>
,	,	kIx,	,
wiki	wike	k1gFnSc4	wike
a	a	k8xC	a
sociální	sociální	k2eAgFnPc4d1	sociální
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
Google	Google	k1gInSc1	Google
Wave	Wav	k1gInSc2	Wav
postupně	postupně	k6eAd1	postupně
zpřístupňován	zpřístupňován	k2eAgInSc1d1	zpřístupňován
vybraným	vybraný	k2eAgMnPc3d1	vybraný
uživatelům	uživatel	k1gMnPc3	uživatel
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
19.	[number]	k4	19.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
;	;	kIx,	;
4.	[number]	k4	4.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
však	však	k9	však
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc1	vývoj
projektu	projekt	k1gInSc2	projekt
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
Googlu	Googl	k1gInSc2	Googl
nezaznamenal	zaznamenat	k5eNaPmAgInS	zaznamenat
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
zájem	zájem	k1gInSc1	zájem
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Googla	k1gFnSc6	Googla
URL	URL	kA	URL
Shortener	Shortener	k1gInSc1	Shortener
===	===	k?	===
</s>
</p>
<p>
<s>
Zkracovač	Zkracovač	k1gMnSc1	Zkracovač
Goo	Goa	k1gFnSc5	Goa
<g/>
.	.	kIx.	.
<g/>
gl	gl	k?	gl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
představen	představen	k2eAgInSc1d1	představen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
od	od	k7c2	od
13.	[number]	k4	13.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
(	(	kIx(	(
<g/>
pátek	pátek	k1gInSc4	pátek
13.	[number]	k4	13.
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
pro	pro	k7c4	pro
nové	nový	k2eAgInPc4d1	nový
a	a	k8xC	a
anonymní	anonymní	k2eAgMnPc4d1	anonymní
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
službu	služba	k1gFnSc4	služba
již	již	k6eAd1	již
někdy	někdy	k6eAd1	někdy
použili	použít	k5eAaPmAgMnP	použít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
služba	služba	k1gFnSc1	služba
dostupná	dostupný	k2eAgFnSc1d1	dostupná
do	do	k7c2	do
30.	[number]	k4	30.
března	březen	k1gInSc2	březen
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
odkazy	odkaz	k1gInPc4	odkaz
ale	ale	k8xC	ale
budou	být	k5eAaImBp3nP	být
nadále	nadále	k6eAd1	nadále
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgFnPc1d1	funkční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Google	Googla	k1gFnSc6	Googla
<g/>
+	+	kIx~	+
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
<g/>
+	+	kIx~	+
(	(	kIx(	(
<g/>
také	také	k9	také
Google	Google	k1gInSc1	Google
Plus	plus	k1gInSc1	plus
nebo	nebo	k8xC	nebo
zkratka	zkratka	k1gFnSc1	zkratka
G	G	kA	G
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sociální	sociální	k2eAgFnSc1d1	sociální
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Google	Google	k1gInSc1	Google
uvedl	uvést	k5eAaPmAgInS	uvést
28.	[number]	k4	28.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
přímou	přímý	k2eAgFnSc4d1	přímá
konkurenci	konkurence	k1gFnSc4	konkurence
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
proti	proti	k7c3	proti
největšímu	veliký	k2eAgMnSc3d3	veliký
konkurentovi	konkurent	k1gMnSc3	konkurent
jsou	být	k5eAaImIp3nP	být
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
kruhy	kruh	k1gInPc4	kruh
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
přidávat	přidávat	k5eAaImF	přidávat
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
pohodlně	pohodlně	k6eAd1	pohodlně
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
poté	poté	k6eAd1	poté
sdílet	sdílet	k5eAaImF	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zbraní	zbraň	k1gFnSc7	zbraň
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
provázanost	provázanost	k1gFnSc1	provázanost
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
služby	služba	k1gFnPc4	služba
Googlu	Googl	k1gInSc2	Googl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
uvedení	uvedení	k1gNnSc2	uvedení
fungovala	fungovat	k5eAaImAgFnS	fungovat
síť	síť	k1gFnSc1	síť
jen	jen	k9	jen
jako	jako	k8xS	jako
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
komunita	komunita	k1gFnSc1	komunita
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dostat	dostat	k5eAaPmF	dostat
jen	jen	k9	jen
s	s	k7c7	s
pozvánkou	pozvánka	k1gFnSc7	pozvánka
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
byl	být	k5eAaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
do	do	k7c2	do
Google	Google	k1gFnSc2	Google
<g/>
+	+	kIx~	+
proniklo	proniknout	k5eAaPmAgNnS	proniknout
přes	přes	k7c4	přes
osm	osm	k4xCc4	osm
milionů	milion	k4xCgInPc2	milion
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
skončit	skončit	k5eAaPmF	skončit
okolo	okolo	k7c2	okolo
srpna	srpen	k1gInSc2	srpen
2019.	[number]	k4	2019.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hardware	hardware	k1gInSc4	hardware
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Clips	Clips	k1gInSc4	Clips
===	===	k?	===
</s>
</p>
<p>
<s>
Fotoaparát	fotoaparát	k1gInSc1	fotoaparát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neustále	neustále	k6eAd1	neustále
sleduje	sledovat	k5eAaImIp3nS	sledovat
okolí	okolí	k1gNnSc4	okolí
a	a	k8xC	a
automaticky	automaticky	k6eAd1	automaticky
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
důležité	důležitý	k2eAgInPc4d1	důležitý
okamžiky	okamžik	k1gInPc4	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
rozpoznává	rozpoznávat	k5eAaImIp3nS	rozpoznávat
důležité	důležitý	k2eAgMnPc4d1	důležitý
lidi	člověk	k1gMnPc4	člověk
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
majitel	majitel	k1gMnSc1	majitel
zařízení	zařízení	k1gNnSc4	zařízení
tráví	trávit	k5eAaImIp3nS	trávit
nejvíce	nejvíce	k6eAd1	nejvíce
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
identifikovat	identifikovat	k5eAaBmF	identifikovat
jejich	jejich	k3xOp3gInPc4	jejich
obličeje	obličej	k1gInPc4	obličej
a	a	k8xC	a
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
nudné	nudný	k2eAgFnPc4d1	nudná
chvíle	chvíle	k1gFnPc4	chvíle
od	od	k7c2	od
klíčových	klíčový	k2eAgInPc2d1	klíčový
okamžiků	okamžik	k1gInPc2	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Googleu	Googleus	k1gInSc2	Googleus
Clips	Clips	k1gInSc1	Clips
nekomunikuje	komunikovat	k5eNaImIp3nS	komunikovat
s	s	k7c7	s
externími	externí	k2eAgInPc7d1	externí
servery	server	k1gInPc7	server
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Google	Googl	k1gMnSc5	Googl
Cookie	Cookius	k1gMnSc5	Cookius
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
detekováno	detekován	k2eAgNnSc1d1	detekováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
cookies	cookies	k1gInSc4	cookies
v	v	k7c6	v
prohlížeči	prohlížeč	k1gMnSc6	prohlížeč
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
cookie	cookie	k1gFnPc1	cookie
serveru	server	k1gInSc2	server
google.com	google.com	k1gInSc1	google.com
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
jako	jako	k8xS	jako
PREF	PREF	kA	PREF
i	i	k9	i
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uživatel	uživatel	k1gMnSc1	uživatel
navštívil	navštívit	k5eAaPmAgMnS	navštívit
nějakou	nějaký	k3yIgFnSc4	nějaký
stránku	stránka	k1gFnSc4	stránka
Googlu	Googl	k1gInSc2	Googl
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
cookie	cookius	k1gMnPc4	cookius
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ochrany	ochrana	k1gFnSc2	ochrana
Safe	safe	k1gInSc1	safe
Browsing	Browsing	k1gInSc1	Browsing
<g/>
,	,	kIx,	,
Googlem	Googl	k1gMnSc7	Googl
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
ochrany	ochrana	k1gFnSc2	ochrana
uživatelů	uživatel	k1gMnPc2	uživatel
před	před	k7c7	před
malware	malwar	k1gMnSc5	malwar
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
využívají	využívat	k5eAaPmIp3nP	využívat
prohlížeče	prohlížeč	k1gInPc1	prohlížeč
Safari	safari	k1gNnSc1	safari
<g/>
,	,	kIx,	,
Google	Google	k1gInSc1	Google
Chrome	chromat	k5eAaImIp3nS	chromat
a	a	k8xC	a
Mozilla	Mozilla	k1gFnSc1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejné	k1gNnSc1	stejné
cookie	cookie	k1gFnSc2	cookie
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
využíváno	využívat	k5eAaImNgNnS	využívat
i	i	k8xC	i
jinými	jiný	k2eAgFnPc7d1	jiná
službami	služba	k1gFnPc7	služba
Googlu	Googl	k1gInSc2	Googl
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tak	tak	k9	tak
mezistránkové	mezistránkový	k2eAgNnSc1d1	mezistránkový
sledování	sledování	k1gNnSc1	sledování
tlačítka	tlačítko	k1gNnSc2	tlačítko
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
cookie	cookie	k1gFnSc1	cookie
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
adresou	adresa	k1gFnSc7	adresa
stránky	stránka	k1gFnSc2	stránka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
tlačítko	tlačítko	k1gNnSc1	tlačítko
+	+	kIx~	+
<g/>
1	[number]	k4	1
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
obsaženou	obsažený	k2eAgFnSc4d1	obsažená
v	v	k7c6	v
hlavičce	hlavička	k1gFnSc6	hlavička
referer	referra	k1gFnPc2	referra
posíláno	posílat	k5eAaImNgNnS	posílat
Googlu	Googla	k1gFnSc4	Googla
kdykoliv	kdykoliv	k6eAd1	kdykoliv
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
stránka	stránka	k1gFnSc1	stránka
obsahující	obsahující	k2eAgFnSc1d1	obsahující
toto	tento	k3xDgNnSc4	tento
tlačítko	tlačítko	k1gNnSc4	tlačítko
načtena	načten	k2eAgFnSc1d1	načtena
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
cookie	cookie	k1gFnSc1	cookie
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
osobní	osobní	k2eAgInPc4d1	osobní
údaje	údaj	k1gInPc4	údaj
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stejného	stejný	k2eAgMnSc4d1	stejný
uživatele	uživatel	k1gMnSc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Googlu	Googl	k1gInSc2	Googl
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
využívána	využívat	k5eAaPmNgFnS	využívat
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
údržby	údržba	k1gFnSc2	údržba
a	a	k8xC	a
ladění	ladění	k1gNnSc4	ladění
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jiná	jiný	k2eAgFnSc1d1	jiná
služba	služba	k1gFnSc1	služba
Googlu	Googl	k1gInSc2	Googl
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mají	mít	k5eAaImIp3nP	mít
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
předinstalovanou	předinstalovaný	k2eAgFnSc7d1	předinstalovaná
<g/>
,	,	kIx,	,
služba	služba	k1gFnSc1	služba
Search	Searcha	k1gFnPc2	Searcha
Suggestions	Suggestionsa	k1gFnPc2	Suggestionsa
se	se	k3xPyFc4	se
cookie	cookie	k1gFnSc2	cookie
nastavovat	nastavovat	k5eAaImF	nastavovat
nepokouší	pokoušet	k5eNaImIp3nS	pokoušet
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
používá	používat	k5eAaImIp3nS	používat
svůj	svůj	k3xOyFgInSc4	svůj
systém	systém	k1gInSc4	systém
ochrany	ochrana	k1gFnSc2	ochrana
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
cookie	cookie	k1gFnSc1	cookie
PREF	PREF	kA	PREF
serveru	server	k1gInSc2	server
.	.	kIx.	.
<g/>
google.com	google.com	k1gInSc1	google.com
automaticky	automaticky	k6eAd1	automaticky
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
.	.	kIx.	.
<g/>
Automatické	automatický	k2eAgNnSc4d1	automatické
objevování	objevování	k1gNnSc4	objevování
se	se	k3xPyFc4	se
tohoto	tento	k3xDgInSc2	tento
cookie	cookie	k1gFnPc1	cookie
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
odesílání	odesílání	k1gNnPc2	odesílání
Googlu	Googl	k1gInSc2	Googl
při	při	k7c6	při
využívání	využívání	k1gNnSc6	využívání
Safe	safe	k1gInSc4	safe
Browsingu	Browsing	k1gInSc2	Browsing
byly	být	k5eAaImAgInP	být
nahlášeny	nahlásit	k5eAaPmNgInP	nahlásit
jako	jako	k8xS	jako
chyby	chyba	k1gFnPc1	chyba
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
Mozilla	Mozilla	k1gFnSc1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Inovace	inovace	k1gFnPc1	inovace
města	město	k1gNnSc2	město
Mountain	Mountaina	k1gFnPc2	Mountaina
View	View	k1gFnSc2	View
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
konečných	konečný	k2eAgFnPc2d1	konečná
verzí	verze	k1gFnPc2	verze
projektu	projekt	k1gInSc2	projekt
inovace	inovace	k1gFnSc2	inovace
domovského	domovský	k2eAgNnSc2d1	domovské
města	město	k1gNnSc2	město
Googlu	Googl	k1gInSc2	Googl
Mountain	Mountain	k1gMnSc1	Mountain
View	View	k1gMnSc1	View
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
projektem	projekt	k1gInSc7	projekt
stojí	stát	k5eAaImIp3nP	stát
společnosti	společnost	k1gFnPc1	společnost
Bjarke	Bjark	k1gInSc2	Bjark
Ingels	Ingels	k1gInSc1	Ingels
a	a	k8xC	a
Heatherwich	Heatherwich	k1gInSc1	Heatherwich
Studio	studio	k1gNnSc1	studio
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
Googlu	Googl	k1gInSc2	Googl
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
modernější	moderní	k2eAgNnSc4d2	modernější
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
šetrné	šetrný	k2eAgNnSc1d1	šetrné
k	k	k7c3	k
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Mountain	Mountain	k2eAgMnSc1d1	Mountain
View	View	k1gMnSc1	View
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
"	"	kIx"	"
<g/>
chytrým	chytrý	k2eAgNnSc7d1	chytré
zeleným	zelený	k2eAgNnSc7d1	zelené
městem	město	k1gNnSc7	město
<g/>
"	"	kIx"	"
s	s	k7c7	s
minimálním	minimální	k2eAgInSc7d1	minimální
podílem	podíl	k1gInSc7	podíl
automobilové	automobilový	k2eAgFnSc2d1	automobilová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
převládat	převládat	k5eAaImF	převládat
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
doprava	doprava	k1gFnSc1	doprava
pěší	pěší	k2eAgFnSc1d1	pěší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
oblastni	oblastnout	k5eAaPmRp2nS	oblastnout
Mountain	Mountain	k1gMnSc1	Mountain
View	View	k1gMnSc1	View
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
na	na	k7c4	na
milionu	milion	k4xCgInSc2	milion
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
metrů	metr	k1gInPc2	metr
vzniknout	vzniknout	k5eAaPmF	vzniknout
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
,	,	kIx,	,
osm	osm	k4xCc4	osm
tisíc	tisíc	k4xCgInPc2	tisíc
domů	dům	k1gInPc2	dům
a	a	k8xC	a
asi	asi	k9	asi
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pětina	pětina	k1gFnSc1	pětina
bytů	byt	k1gInPc2	byt
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
cenově	cenově	k6eAd1	cenově
dostupná	dostupný	k2eAgFnSc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
předběžných	předběžný	k2eAgInPc2d1	předběžný
odhadů	odhad	k1gInPc2	odhad
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
projekt	projekt	k1gInSc1	projekt
trvat	trvat	k5eAaImF	trvat
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
kontroverze	kontroverze	k1gFnSc1	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
efektivitu	efektivita	k1gFnSc4	efektivita
je	být	k5eAaImIp3nS	být
Google	Google	k1gInSc1	Google
nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
internetovým	internetový	k2eAgMnSc7d1	internetový
vyhledávačem	vyhledávač	k1gMnSc7	vyhledávač
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
s	s	k7c7	s
sebou	se	k3xPyFc7	se
ovšem	ovšem	k9	ovšem
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
určité	určitý	k2eAgInPc4d1	určitý
rysy	rys	k1gInPc4	rys
monopolu	monopol	k1gInSc2	monopol
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
postavení	postavení	k1gNnSc3	postavení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
je	být	k5eAaImIp3nS	být
Google	Google	k1gFnSc1	Google
stále	stále	k6eAd1	stále
i	i	k9	i
technickou	technický	k2eAgFnSc7d1	technická
komunitou	komunita	k1gFnSc7	komunita
neobvykle	obvykle	k6eNd1	obvykle
oblíbeným	oblíbený	k2eAgInPc3d1	oblíbený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
zavádění	zavádění	k1gNnSc6	zavádění
nových	nový	k2eAgFnPc2d1	nová
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
Google	Google	k1gFnSc1	Google
Toolbar	Toolbar	k1gInSc1	Toolbar
<g/>
,	,	kIx,	,
Google	Google	k1gInSc1	Google
Mail	mail	k1gInSc1	mail
<g/>
,	,	kIx,	,
Google	Google	k1gInSc1	Google
analytics	analytics	k1gInSc1	analytics
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
ozývají	ozývat	k5eAaImIp3nP	ozývat
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Google	Google	k1gFnSc1	Google
získává	získávat	k5eAaImIp3nS	získávat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
dat	datum	k1gNnPc2	datum
o	o	k7c6	o
uživatelích	uživatel	k1gMnPc6	uživatel
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
připojil	připojit	k5eAaPmAgMnS	připojit
ke	k	k7c3	k
sledovacímu	sledovací	k2eAgInSc3d1	sledovací
programu	program	k1gInSc3	program
PRISM	PRISM	kA	PRISM
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kritika	kritika	k1gFnSc1	kritika
Google	Google	k1gNnSc2	Google
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
expanzi	expanze	k1gFnSc6	expanze
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podřídí	podřídit	k5eAaPmIp3nP	podřídit
čínským	čínský	k2eAgInPc3d1	čínský
zákonům	zákon	k1gInPc3	zákon
a	a	k8xC	a
omezí	omezit	k5eAaPmIp3nP	omezit
výsledky	výsledek	k1gInPc1	výsledek
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
místním	místní	k2eAgNnSc7d1	místní
právem	právo	k1gNnSc7	právo
(	(	kIx(	(
<g/>
kdyby	kdyby	kYmCp3nS	kdyby
tyto	tento	k3xDgInPc4	tento
požadavky	požadavek	k1gInPc4	požadavek
nesplnil	splnit	k5eNaPmAgInS	splnit
<g/>
,	,	kIx,	,
čínská	čínský	k2eAgFnSc1d1	čínská
vláda	vláda	k1gFnSc1	vláda
by	by	kYmCp3nS	by
Google	Googl	k1gMnPc4	Googl
zakázala	zakázat	k5eAaPmAgFnS	zakázat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
čínská	čínský	k2eAgFnSc1d1	čínská
verze	verze	k1gFnSc1	verze
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
státu	stát	k1gInSc2	stát
nepohodlné	pohodlný	k2eNgInPc4d1	nepohodlný
odkazy	odkaz	k1gInPc4	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
nepřípustnou	přípustný	k2eNgFnSc4d1	nepřípustná
cenzuru	cenzura	k1gFnSc4	cenzura
a	a	k8xC	a
podporování	podporování	k1gNnSc4	podporování
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
Google	Google	k1gFnSc1	Google
naopak	naopak	k6eAd1	naopak
odporuje	odporovat	k5eAaImIp3nS	odporovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
dává	dávat	k5eAaImIp3nS	dávat
Číňanům	Číňan	k1gMnPc3	Číňan
přístup	přístup	k1gInSc4	přístup
alespoň	alespoň	k9	alespoň
k	k	k7c3	k
části	část	k1gFnSc3	část
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
podporuje	podporovat	k5eAaImIp3nS	podporovat
jejich	jejich	k3xOp3gInSc4	jejich
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Mathiase	Mathiasa	k1gFnSc6	Mathiasa
Döpfnera	Döpfner	k1gMnSc2	Döpfner
má	mít	k5eAaImIp3nS	mít
Google	Google	k1gInSc4	Google
větší	veliký	k2eAgInSc4d2	veliký
sféru	sféra	k1gFnSc4	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
a	a	k8xC	a
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
ho	on	k3xPp3gMnSc4	on
ke	k	k7c3	k
státním	státní	k2eAgMnPc3d1	státní
monopolům	monopol	k1gInPc3	monopol
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
biologických	biologický	k2eAgInPc2d1	biologický
virů	vir	k1gInPc2	vir
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
šířilo	šířit	k5eAaImAgNnS	šířit
tak	tak	k6eAd1	tak
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
efektivně	efektivně	k6eAd1	efektivně
a	a	k8xC	a
agresivně	agresivně	k6eAd1	agresivně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
právě	právě	k9	právě
tyto	tento	k3xDgFnPc4	tento
technologické	technologický	k2eAgFnPc4d1	technologická
platformy	platforma	k1gFnPc4	platforma
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
Google	Google	k1gFnSc1	Google
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
pravidla	pravidlo	k1gNnPc4	pravidlo
užívání	užívání	k1gNnSc3	užívání
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
začal	začít	k5eAaPmAgMnS	začít
pohlížet	pohlížet	k5eAaImF	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
produkt	produkt	k1gInSc4	produkt
a	a	k8xC	a
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
uživateli	uživatel	k1gMnSc3	uživatel
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
napříč	napříč	k7c7	napříč
všemi	všecek	k3xTgFnPc7	všecek
službami	služba	k1gFnPc7	služba
<g/>
,	,	kIx,	,
do	do	k7c2	do
propojení	propojení	k1gNnSc2	propojení
nebyla	být	k5eNaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
pouze	pouze	k6eAd1	pouze
služba	služba	k1gFnSc1	služba
Google	Google	k1gFnSc2	Google
Wallet	Wallet	k1gInSc1	Wallet
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
Google	Google	k1gFnSc2	Google
Books	Booksa	k1gFnPc2	Booksa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Googlu	Googl	k1gInSc2	Googl
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
používání	používání	k1gNnSc1	používání
Googlu	Googla	k1gFnSc4	Googla
bylo	být	k5eAaImAgNnS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
a	a	k8xC	a
intuitivnější	intuitivní	k2eAgNnSc1d2	intuitivnější
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
uživatelem	uživatel	k1gMnSc7	uživatel
jednal	jednat	k5eAaImAgMnS	jednat
jako	jako	k9	jako
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
média	médium	k1gNnPc1	médium
ale	ale	k9	ale
upozornila	upozornit	k5eAaPmAgNnP	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
lépe	dobře	k6eAd2	dobře
cílenou	cílený	k2eAgFnSc4d1	cílená
reklamu	reklama	k1gFnSc4	reklama
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
větší	veliký	k2eAgInSc4d2	veliký
zisk	zisk	k1gInSc4	zisk
<g/>
.	.	kIx.	.
<g/>
Kontroverzi	kontroverze	k1gFnSc4	kontroverze
také	také	k9	také
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
propuštění	propuštění	k1gNnSc1	propuštění
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
Jamese	Jamese	k1gFnSc2	Jamese
Damorea	Damoreus	k1gMnSc2	Damoreus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
"	"	kIx"	"
<g/>
Google	Googlo	k1gNnSc6	Googlo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ideological	Ideological	k1gFnPc2	Ideological
Echo	echo	k1gNnSc1	echo
Chamber	Chamber	k1gMnSc1	Chamber
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
diskriminace	diskriminace	k1gFnPc4	diskriminace
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
<g/>
Google	Google	k1gInSc1	Google
algoritmicky	algoritmicky	k6eAd1	algoritmicky
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
výsledky	výsledek	k1gInPc4	výsledek
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
algoritmy	algoritmus	k1gInPc1	algoritmus
jsou	být	k5eAaImIp3nP	být
ovlivnitelné	ovlivnitelný	k2eAgInPc1d1	ovlivnitelný
vstupními	vstupní	k2eAgNnPc7d1	vstupní
daty	datum	k1gNnPc7	datum
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
první	první	k4xOgNnPc4	první
místa	místo	k1gNnPc4	místo
mezi	mezi	k7c4	mezi
vyhledané	vyhledaný	k2eAgInPc4d1	vyhledaný
výsledky	výsledek	k1gInPc4	výsledek
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
vymyšlená	vymyšlený	k2eAgFnSc1d1	vymyšlená
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
po	po	k7c6	po
případu	případ	k1gInSc6	případ
střelby	střelba	k1gFnSc2	střelba
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
se	se	k3xPyFc4	se
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
místech	místo	k1gNnPc6	místo
při	při	k7c6	při
vyhledávání	vyhledávání	k1gNnSc6	vyhledávání
jména	jméno	k1gNnSc2	jméno
střelce	střelec	k1gMnPc4	střelec
objevovala	objevovat	k5eAaImAgFnS	objevovat
vymyšlená	vymyšlený	k2eAgFnSc1d1	vymyšlená
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
střelec	střelec	k1gMnSc1	střelec
byl	být	k5eAaImAgMnS	být
radikální	radikální	k2eAgMnSc1d1	radikální
komunista	komunista	k1gMnSc1	komunista
napojený	napojený	k2eAgInSc4d1	napojený
na	na	k7c4	na
hnutí	hnutí	k1gNnSc4	hnutí
Antifa	Antif	k1gMnSc2	Antif
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Google	Google	k1gInSc1	Google
zjistí	zjistit	k5eAaPmIp3nS	zjistit
nějaký	nějaký	k3yIgInSc4	nějaký
takovýto	takovýto	k3xDgInSc4	takovýto
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
řešen	řešit	k5eAaImNgInS	řešit
ručním	ruční	k2eAgInSc7d1	ruční
zásahem	zásah	k1gInSc7	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
ale	ale	k8xC	ale
málokdo	málokdo	k3yInSc1	málokdo
řešil	řešit	k5eAaImAgMnS	řešit
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
výsledky	výsledek	k1gInPc1	výsledek
dodané	dodaný	k2eAgInPc1d1	dodaný
Googlem	Googl	k1gInSc7	Googl
někdo	někdo	k3yInSc1	někdo
nebo	nebo	k8xC	nebo
něco	něco	k3yInSc1	něco
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uživatele	uživatel	k1gMnSc2	uživatel
nedozvěděl	dozvědět	k5eNaPmAgMnS	dozvědět
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nemá	mít	k5eNaImIp3nS	mít
vědět	vědět	k5eAaImF	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Google	Google	k6eAd1	Google
sleduje	sledovat	k5eAaImIp3nS	sledovat
polohu	poloha	k1gFnSc4	poloha
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc4	ten
uživatel	uživatel	k1gMnSc1	uživatel
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
loga	logo	k1gNnSc2	logo
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
společnost	společnost	k1gFnSc1	společnost
řadu	řad	k1gInSc2	řad
log	log	kA	log
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Produkty	produkt	k1gInPc1	produkt
Google	Google	k1gFnSc2	Google
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Cardboard	Cardboarda	k1gFnPc2	Cardboarda
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Glass	Glassa	k1gFnPc2	Glassa
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Home	Hom	k1gFnSc2	Hom
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Wifi	Wif	k1gFnSc2	Wif
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
DayDream	DayDream	k1gInSc1	DayDream
VR	vr	k0	vr
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Chromecast	Chromecast	k1gFnSc1	Chromecast
/	/	kIx~	/
Chromecast	Chromecast	k1gInSc1	Chromecast
Ultra	ultra	k2eAgInSc2d1	ultra
</s>
</p>
<p>
<s>
řada	řada	k1gFnSc1	řada
Google	Google	k1gFnSc2	Google
Pixel	pixel	k1gInSc1	pixel
</s>
</p>
<p>
<s>
řada	řada	k1gFnSc1	řada
Google	Google	k1gFnSc2	Google
Chromebook	Chromebook	k1gInSc1	Chromebook
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Konkurence	konkurence	k1gFnSc1	konkurence
==	==	k?	==
</s>
</p>
<p>
<s>
Yahoo	Yahoo	k6eAd1	Yahoo
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Bing	bingo	k1gNnPc2	bingo
</s>
</p>
<p>
<s>
Seznam.cz	Seznam.cz	k1gInSc1	Seznam.cz
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DuckDuckGo	DuckDuckGo	k6eAd1	DuckDuckGo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
bomba	bomba	k1gFnSc1	bomba
</s>
</p>
<p>
<s>
Google	Google	k1gFnSc1	Google
Glass	Glassa	k1gFnPc2	Glassa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
produktů	produkt	k1gInPc2	produkt
Google	Google	k1gFnSc2	Google
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Google	Google	k1gFnSc2	Google
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Vyhledávač	vyhledávač	k1gMnSc1	vyhledávač
Google.cz	Google.cz	k1gMnSc1	Google.cz
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Google	Google	k6eAd1	Google
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
Google	Google	k1gFnSc1	Google
Blog	Bloga	k1gFnPc2	Bloga
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
služby	služba	k1gFnPc1	služba
Googlu	Googl	k1gInSc2	Googl
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Googlu	Googl	k1gInSc2	Googl
z	z	k7c2	z
11.	[number]	k4	11.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
na	na	k7c4	na
Internet	Internet	k1gInSc4	Internet
Archive	archiv	k1gInSc5	archiv
</s>
</p>
