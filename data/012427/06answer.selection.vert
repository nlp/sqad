<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
označovaly	označovat	k5eAaImAgInP	označovat
jako	jako	k9	jako
vlaky	vlak	k1gInPc1	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
kategorie	kategorie	k1gFnPc4	kategorie
expresů	expres	k1gInPc2	expres
<g/>
,	,	kIx,	,
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
zavedly	zavést	k5eAaPmAgFnP	zavést
i	i	k9	i
kategorii	kategorie	k1gFnSc6	kategorie
rychlík	rychlík	k1gMnSc1	rychlík
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
(	(	kIx(	(
<g/>
Rx	Rx	k1gMnSc1	Rx
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
