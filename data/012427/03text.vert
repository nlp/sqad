<p>
<s>
Vlak	vlak	k1gInSc1	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
vlaky	vlak	k1gInPc4	vlak
určené	určený	k2eAgInPc4d1	určený
k	k	k7c3	k
cestám	cesta	k1gFnPc3	cesta
na	na	k7c6	na
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
poskytující	poskytující	k2eAgFnSc4d1	poskytující
vyšší	vysoký	k2eAgFnSc4d2	vyšší
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
komfort	komfort	k1gInSc4	komfort
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
kategoriím	kategorie	k1gFnPc3	kategorie
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
všechny	všechen	k3xTgInPc1	všechen
vlaky	vlak	k1gInPc1	vlak
kategorií	kategorie	k1gFnPc2	kategorie
Expres	expres	k2eAgFnPc2d1	expres
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
InterCity	InterCit	k1gInPc1	InterCit
(	(	kIx(	(
<g/>
IC	IC	kA	IC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
EuroCity	EuroCit	k1gInPc1	EuroCit
(	(	kIx(	(
<g/>
EC	EC	kA	EC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SuperCity	SuperCit	k1gInPc1	SuperCit
(	(	kIx(	(
<g/>
SC	SC	kA	SC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Railjet	Railjet	k1gMnSc1	Railjet
(	(	kIx(	(
<g/>
rj	rj	k?	rj
<g/>
)	)	kIx)	)
a	a	k8xC	a
noční	noční	k2eAgInPc1d1	noční
vlaky	vlak	k1gInPc1	vlak
EuroNight	EuroNighta	k1gFnPc2	EuroNighta
(	(	kIx(	(
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
vlaky	vlak	k1gInPc1	vlak
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
kvalitu	kvalita	k1gFnSc4	kvalita
poskytovaných	poskytovaný	k2eAgFnPc2d1	poskytovaná
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
modernějšími	moderní	k2eAgFnPc7d2	modernější
soupravami	souprava	k1gFnPc7	souprava
s	s	k7c7	s
jídelními	jídelní	k2eAgInPc7d1	jídelní
či	či	k8xC	či
bistro	bistro	k1gNnSc1	bistro
vozy	vůz	k1gInPc1	vůz
a	a	k8xC	a
vozy	vůz	k1gInPc1	vůz
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
přednosti	přednost	k1gFnSc3	přednost
před	před	k7c7	před
ostatními	ostatní	k2eAgInPc7d1	ostatní
vlaky	vlak	k1gInPc7	vlak
vyšších	vysoký	k2eAgFnPc2d2	vyšší
jízdních	jízdní	k2eAgFnPc2d1	jízdní
rychlostí	rychlost	k1gFnPc2	rychlost
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
kratších	krátký	k2eAgFnPc2d2	kratší
jízdních	jízdní	k2eAgFnPc2d1	jízdní
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
České	český	k2eAgFnSc2d1	Česká
dráhy	dráha	k1gFnSc2	dráha
označovaly	označovat	k5eAaImAgInP	označovat
jako	jako	k9	jako
vlaky	vlak	k1gInPc1	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
kategorie	kategorie	k1gFnPc4	kategorie
expresů	expres	k1gInPc2	expres
<g/>
,	,	kIx,	,
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
zavedly	zavést	k5eAaPmAgFnP	zavést
i	i	k9	i
kategorii	kategorie	k1gFnSc6	kategorie
rychlík	rychlík	k1gMnSc1	rychlík
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
(	(	kIx(	(
<g/>
Rx	Rx	k1gMnSc1	Rx
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
kategorii	kategorie	k1gFnSc4	kategorie
zrušili	zrušit	k5eAaPmAgMnP	zrušit
a	a	k8xC	a
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
kategorie	kategorie	k1gFnSc1	kategorie
rychlík	rychlík	k1gInSc1	rychlík
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
rychlíky	rychlík	k1gMnPc4	rychlík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
vlaky	vlak	k1gInPc1	vlak
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
kvality	kvalita	k1gFnSc2	kvalita
jsou	být	k5eAaImIp3nP	být
označovány	označován	k2eAgFnPc1d1	označována
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
vlaky	vlak	k1gInPc1	vlak
kategorie	kategorie	k1gFnSc2	kategorie
SuperCity	SuperCit	k2eAgInPc1d1	SuperCit
(	(	kIx(	(
<g/>
SC	SC	kA	SC
<g/>
)	)	kIx)	)
či	či	k8xC	či
švýcarské	švýcarský	k2eAgInPc1d1	švýcarský
noční	noční	k2eAgInPc1d1	noční
expresní	expresní	k2eAgInPc1d1	expresní
vlaky	vlak	k1gInPc1	vlak
CityNightLine	CityNightLin	k1gInSc5	CityNightLin
(	(	kIx(	(
<g/>
CNL	CNL	kA	CNL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nezařazené	zařazený	k2eNgInPc1d1	nezařazený
vlaky	vlak	k1gInPc1	vlak
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
vlaků	vlak	k1gInPc2	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
nejsou	být	k5eNaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
(	(	kIx(	(
<g/>
a	a	k8xC	a
neformálně	formálně	k6eNd1	formálně
tak	tak	k9	tak
bývají	bývat	k5eAaImIp3nP	bývat
označovány	označovat	k5eAaImNgInP	označovat
například	například	k6eAd1	například
jako	jako	k8xC	jako
Vlak	vlak	k1gInSc1	vlak
nižší	nízký	k2eAgFnSc2d2	nižší
kvality	kvalita	k1gFnSc2	kvalita
)	)	kIx)	)
vlaky	vlak	k1gInPc1	vlak
kategorií	kategorie	k1gFnPc2	kategorie
rychlík	rychlík	k1gMnSc1	rychlík
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spěšný	spěšný	k2eAgInSc4d1	spěšný
vlak	vlak	k1gInSc4	vlak
(	(	kIx(	(
<g/>
Sp	Sp	k1gFnSc4	Sp
<g/>
)	)	kIx)	)
a	a	k8xC	a
osobní	osobní	k2eAgInSc4d1	osobní
vlak	vlak	k1gInSc4	vlak
(	(	kIx(	(
<g/>
Os	osa	k1gFnPc2	osa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Expres	expres	k1gInSc1	expres
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
InterCity	InterCita	k1gFnPc1	InterCita
(	(	kIx(	(
<g/>
IC	IC	kA	IC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
EuroCity	EuroCita	k1gFnPc1	EuroCita
(	(	kIx(	(
<g/>
EC	EC	kA	EC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SuperCity	SuperCita	k1gFnPc1	SuperCita
(	(	kIx(	(
<g/>
SC	SC	kA	SC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
<g/>
:	:	kIx,	:
Vlak	vlak	k1gInSc1	vlak
vyšší	vysoký	k2eAgFnSc2d2	vyšší
kvality	kvalita	k1gFnSc2	kvalita
(	(	kIx(	(
<g/>
odkaz	odkaz	k1gInSc4	odkaz
nefunkční	funkční	k2eNgInSc4d1	nefunkční
<g/>
)	)	kIx)	)
</s>
</p>
