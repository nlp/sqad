<s>
Michail	Michail	k1gMnSc1
Kovaljov	Kovaljov	k1gInSc4
</s>
<s>
Michail	Michail	k1gMnSc1
Prokofjevič	Prokofjevič	k1gMnSc1
Kovaljov	Kovaljov	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1897	#num#	k4
Brjuchověckaja	Brjuchověckajum	k1gNnSc2
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1967	#num#	k4
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
SSSR	SSSR	kA
Vojenská	vojenský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
Hodnost	hodnost	k1gFnSc1
</s>
<s>
Generálplukovník	generálplukovník	k1gMnSc1
Doba	doba	k1gFnSc1
služby	služba	k1gFnSc2
</s>
<s>
1915	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
Sloužil	sloužit	k5eAaImAgInS
</s>
<s>
SSSR	SSSR	kA
Složka	složka	k1gFnSc1
</s>
<s>
Rudá	rudý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
Jednotka	jednotka	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
střelecký	střelecký	k2eAgInSc1d1
sborBěloruský	sborBěloruský	k2eAgInSc1d1
frontBěloruský	frontBěloruský	k2eAgInSc1d1
zvláštní	zvláštní	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
okruhZabajkalský	okruhZabajkalský	k2eAgInSc1d1
front	front	k1gInSc1
Války	válka	k1gFnSc2
</s>
<s>
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válkaRuská	válkaRuský	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válkaTambovské	válkaTambovská	k1gFnSc2
povstáníPolsko-sovětská	povstáníPolsko-sovětský	k2eAgFnSc1d1
válkaZimní	válkaZimní	k2eAgFnSc3d1
válkaInvaze	válkaInvaha	k1gFnSc3
do	do	k7c2
Polska	Polsko	k1gNnSc2
Vyznamenání	vyznamenání	k1gNnSc2
</s>
<s>
Leninův	Leninův	k2eAgInSc1d1
řád	řád	k1gInSc1
(	(	kIx(
<g/>
druhé	druhý	k4xOgFnPc1
třídy	třída	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
rudého	rudý	k2eAgInSc2d1
praporu	prapor	k1gInSc2
(	(	kIx(
<g/>
třetí	třetí	k4xOgFnSc2
třídy	třída	k1gFnSc2
<g/>
)	)	kIx)
<g/>
Řád	řád	k1gInSc1
Suvorova	Suvorův	k2eAgInSc2d1
</s>
<s>
Generálplukovník	generálplukovník	k1gMnSc1
Michail	Michail	k1gMnSc1
Prokofjevič	Prokofjevič	k1gMnSc1
Kovaljov	Kovaljov	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
:	:	kIx,
М	М	k?
П	П	k?
К	К	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
*	*	kIx~
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1897	#num#	k4
<g/>
,	,	kIx,
Brjuchověckaja	Brjuchověckaja	k1gFnSc1
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1967	#num#	k4
<g/>
,	,	kIx,
Petrohrad	Petrohrad	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
sovětský	sovětský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
a	a	k8xC
vysoký	vysoký	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Čerstvě	čerstvě	k6eAd1
vystudovaný	vystudovaný	k2eAgInSc1d1
Kovaljov	Kovaljov	k1gInSc1
nastoupil	nastoupit	k5eAaPmAgInS
do	do	k7c2
carské	carský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
roku	rok	k1gInSc2
1915	#num#	k4
a	a	k8xC
zapojil	zapojit	k5eAaPmAgInS
se	se	k3xPyFc4
jako	jako	k9
praporčík	praporčík	k1gMnSc1
do	do	k7c2
bojů	boj	k1gInPc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
Říjnové	říjnový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
byl	být	k5eAaImAgMnS
štábním	štábní	k2eAgMnSc7d1
kapitánem	kapitán	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
Ruské	ruský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
velel	velet	k5eAaImAgInS
pluku	pluk	k1gInSc3
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
proti	proti	k7c3
Bělogvardějcům	bělogvardějec	k1gMnPc3
a	a	k8xC
během	během	k7c2
Tambovského	Tambovský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
proti	proti	k7c3
vzbouřeným	vzbouřený	k2eAgMnPc3d1
rolníkům	rolník	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
května	květen	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
do	do	k7c2
června	červen	k1gInSc2
roku	rok	k1gInSc2
1936	#num#	k4
velel	velet	k5eAaImAgInS
9	#num#	k4
<g/>
.	.	kIx.
střeleckému	střelecký	k2eAgInSc3d1
sboru	sbor	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k8xS,k8xC
velitel	velitel	k1gMnSc1
Běloruského	běloruský	k2eAgInSc2d1
frontu	front	k1gInSc2
velel	velet	k5eAaImAgInS
jednomu	jeden	k4xCgInSc3
frontu	front	k1gInSc3
sovětské	sovětský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
při	při	k7c6
obsazení	obsazení	k1gNnSc6
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
druhému	druhý	k4xOgInSc3
–	–	k?
Ukrajinskému	ukrajinský	k2eAgInSc3d1
velel	velet	k5eAaImAgInS
Semjon	Semjon	k1gInSc1
Timošenko	Timošenka	k1gFnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
listopadu	listopad	k1gInSc2
1939	#num#	k4
velel	velet	k5eAaImAgInS
Běloruskému	běloruský	k2eAgInSc3d1
zvláštnímu	zvláštní	k2eAgInSc3d1
vojenskému	vojenský	k2eAgInSc3d1
okruhu	okruh	k1gInSc3
<g/>
,	,	kIx,
okruhu	okruh	k1gInSc3
vytvořenému	vytvořený	k2eAgInSc3d1
z	z	k7c2
Běloruského	běloruský	k2eAgInSc2d1
frontu	front	k1gInSc2
a	a	k8xC
Běloruského	běloruský	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
pro	pro	k7c4
účely	účel	k1gInPc4
již	již	k6eAd1
zmíněné	zmíněný	k2eAgFnPc4d1
sovětské	sovětský	k2eAgFnPc4d1
invaze	invaze	k1gFnPc4
do	do	k7c2
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
funkci	funkce	k1gFnSc6
ho	on	k3xPp3gMnSc4
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
1940	#num#	k4
vystřídal	vystřídat	k5eAaPmAgInS
Fjodor	Fjodor	k1gInSc1
Kuzněcov	Kuzněcov	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
velel	velet	k5eAaImAgMnS
nově	nově	k6eAd1
vzniklému	vzniklý	k2eAgInSc3d1
Zabajkalskému	zabajkalský	k2eAgInSc3d1
frontu	front	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
po	po	k7c6
něm	on	k3xPp3gNnSc6
v	v	k7c6
červenci	červenec	k1gInSc6
1945	#num#	k4
převzal	převzít	k5eAaPmAgMnS
maršál	maršál	k1gMnSc1
Malinovskij	Malinovskij	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
působil	působit	k5eAaImAgMnS
i	i	k9
nadále	nadále	k6eAd1
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
jako	jako	k9
několikanásobně	několikanásobně	k6eAd1
vyznamenaný	vyznamenaný	k2eAgMnSc1d1
válečný	válečný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
v	v	k7c4
Sankt	Sankt	k1gInSc4
Petěrburgu	Petěrburg	k1gInSc2
<g/>
,	,	kIx,
tehdejším	tehdejší	k2eAgNnSc6d1
Leningradu	Leningrad	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mikhail	Mikhaila	k1gFnPc2
Kovalyov	Kovalyovo	k1gNnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
9	#num#	k4
<g/>
.	.	kIx.
střelecký	střelecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
-	-	kIx~
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Důvody	důvod	k1gInPc1
a	a	k8xC
následky	následek	k1gInPc1
sovětské	sovětský	k2eAgFnSc2d1
invaze	invaze	k1gFnSc2
do	do	k7c2
Polska	Polsko	k1gNnSc2
v	v	k7c6
září	září	k1gNnSc6
1939	#num#	k4
<g/>
↑	↑	k?
Běloruský	běloruský	k2eAgInSc1d1
zvláštní	zvláštní	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
-	-	kIx~
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
forum	forum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zabajkalský	zabajkalský	k2eAgInSc4d1
front	front	k1gInSc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
forum	forum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Životopis	životopis	k1gInSc1
Kovaljova	Kovaljův	k2eAgInSc2d1
</s>
