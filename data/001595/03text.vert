<s>
John	John	k1gMnSc1	John
Douglas	Douglas	k1gMnSc1	Douglas
Cockcroft	Cockcroft	k1gMnSc1	Cockcroft
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1897	[number]	k4	1897
Todmorden	Todmordna	k1gFnPc2	Todmordna
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1967	[number]	k4	1967
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
E.	E.	kA	E.
T.	T.	kA	T.
S.	S.	kA	S.
Waltonem	Walton	k1gInSc7	Walton
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
za	za	k7c2	za
objevné	objevný	k2eAgFnSc2d1	objevná
práce	práce	k1gFnSc2	práce
přeměny	přeměna	k1gFnSc2	přeměna
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
uměle	uměle	k6eAd1	uměle
urychlenými	urychlený	k2eAgFnPc7d1	urychlená
jadernými	jaderný	k2eAgFnPc7d1	jaderná
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
