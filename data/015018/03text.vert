<s>
Psychologické	psychologický	k2eAgFnPc1d1
disciplíny	disciplína	k1gFnPc1
</s>
<s>
Obory	obor	k1gInPc1
psychologie	psychologie	k1gFnSc2
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
podle	podle	k7c2
předmětu	předmět	k1gInSc2
zkoumání	zkoumání	k1gNnSc2
na	na	k7c4
dvě	dva	k4xCgFnPc4
nebo	nebo	k8xC
tři	tři	k4xCgFnPc1
oblasti	oblast	k1gFnPc1
Psychologických	psychologický	k2eAgFnPc2d1
disciplín	disciplína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc7d1
(	(	kIx(
<g/>
neboli	neboli	k8xC
teoretické	teoretický	k2eAgInPc1d1
<g/>
)	)	kIx)
obory	obor	k1gInPc1
psychologie	psychologie	k1gFnSc1
mají	mít	k5eAaImIp3nP
obecný	obecný	k2eAgInSc4d1
charakter	charakter	k1gInSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
zaměřeny	zaměřit	k5eAaPmNgFnP
na	na	k7c4
získávání	získávání	k1gNnSc4
obecných	obecný	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvíjeny	rozvíjen	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
na	na	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
či	či	k8xC
výzkumných	výzkumný	k2eAgInPc6d1
psychologických	psychologický	k2eAgInPc6d1
ústavech	ústav	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikované	aplikovaný	k2eAgInPc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
praktické	praktický	k2eAgInPc1d1
obory	obor	k1gInPc1
psychologie	psychologie	k1gFnSc2
usilují	usilovat	k5eAaImIp3nP
o	o	k7c4
využití	využití	k1gNnSc4
psychologických	psychologický	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
v	v	k7c6
praxi	praxe	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
zaměřují	zaměřovat	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c4
nejrůznější	různý	k2eAgInPc4d3
obory	obor	k1gInPc4
lidských	lidský	k2eAgFnPc2d1
činností	činnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
při	při	k7c6
tom	ten	k3xDgNnSc6
výrazně	výrazně	k6eAd1
odkloňují	odkloňovat	k5eAaImIp3nP
od	od	k7c2
teoretických	teoretický	k2eAgInPc2d1
psychologických	psychologický	k2eAgInPc2d1
oborů	obor	k1gInPc2
a	a	k8xC
vytvářejí	vytvářet	k5eAaImIp3nP
svébytný	svébytný	k2eAgInSc4d1
pojmový	pojmový	k2eAgInSc4d1
aparát	aparát	k1gInSc4
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
autoři	autor	k1gMnPc1
pak	pak	k6eAd1
ještě	ještě	k6eAd1
odlišují	odlišovat	k5eAaImIp3nP
psychologické	psychologický	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
speciální	speciální	k2eAgFnPc4d1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
zaměřeny	zaměřen	k2eAgFnPc1d1
na	na	k7c4
relativně	relativně	k6eAd1
úzký	úzký	k2eAgInSc4d1
předmět	předmět	k1gInSc4
studia	studio	k1gNnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
některé	některý	k3yIgFnPc4
metodologické	metodologický	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
jejichž	jejichž	k3xOyRp3gInPc2
poznatků	poznatek	k1gInPc2
a	a	k8xC
postupů	postup	k1gInPc2
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
základní	základní	k2eAgInPc1d1
i	i	k8xC
aplikované	aplikovaný	k2eAgInPc1d1
psychologické	psychologický	k2eAgInPc1d1
obory	obor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Uvedené	uvedený	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
není	být	k5eNaImIp3nS
závazné	závazný	k2eAgNnSc1d1
a	a	k8xC
různí	různý	k2eAgMnPc1d1
psychologové	psycholog	k1gMnPc1
mají	mít	k5eAaImIp3nP
na	na	k7c4
zařazení	zařazení	k1gNnSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
oborů	obor	k1gInPc2
odlišné	odlišný	k2eAgInPc4d1
názory	názor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
základních	základní	k2eAgInPc2d1
a	a	k8xC
aplikovaných	aplikovaný	k2eAgInPc2d1
oborů	obor	k1gInPc2
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
velmi	velmi	k6eAd1
neostré	ostrý	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgInPc1d1
obory	obor	k1gInPc1
psychologie	psychologie	k1gFnSc2
</s>
<s>
biologická	biologický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zkoumá	zkoumat	k5eAaImIp3nS
vzájemné	vzájemný	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
tělesnými	tělesný	k2eAgInPc7d1
a	a	k8xC
psychickými	psychický	k2eAgInPc7d1
procesy	proces	k1gInPc7
</s>
<s>
psychologická	psychologický	k2eAgFnSc1d1
metodologie	metodologie	k1gFnSc1
-	-	kIx~
řeší	řešit	k5eAaImIp3nP
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
souvisí	souviset	k5eAaImIp3nP
s	s	k7c7
pochopením	pochopení	k1gNnSc7
metodologických	metodologický	k2eAgInPc2d1
přístupů	přístup	k1gInPc2
(	(	kIx(
<g/>
výzkumných	výzkumný	k2eAgInPc2d1
projektů	projekt	k1gInPc2
a	a	k8xC
teorií	teorie	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
užití	užití	k1gNnSc4
v	v	k7c6
psychologii	psychologie	k1gFnSc6
</s>
<s>
obecná	obecný	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
přináší	přinášet	k5eAaImIp3nS
obecné	obecný	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
o	o	k7c6
psychice	psychika	k1gFnSc6
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
studuje	studovat	k5eAaImIp3nS
chování	chování	k1gNnSc4
<g/>
,	,	kIx,
motivaci	motivace	k1gFnSc4
i	i	k8xC
prožívání	prožívání	k1gNnSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
význam	význam	k1gInSc4
při	při	k7c6
zpracování	zpracování	k1gNnSc6
základních	základní	k2eAgFnPc2d1
teoretických	teoretický	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
</s>
<s>
psychologie	psychologie	k1gFnSc1
osobnosti	osobnost	k1gFnSc2
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
strukturou	struktura	k1gFnSc7
<g/>
,	,	kIx,
vývojem	vývoj	k1gInSc7
a	a	k8xC
dynamikou	dynamika	k1gFnSc7
osobnosti	osobnost	k1gFnSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
její	její	k3xOp3gFnSc7
významnou	významný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
diferenciální	diferenciální	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
</s>
<s>
psychopatologie	psychopatologie	k1gFnSc1
-	-	kIx~
zájem	zájem	k1gInSc1
o	o	k7c4
změny	změna	k1gFnPc4
(	(	kIx(
<g/>
chorobné	chorobný	k2eAgFnPc4d1
<g/>
)	)	kIx)
psychických	psychický	k2eAgInPc2d1
jevů	jev	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
různých	různý	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
(	(	kIx(
<g/>
společenských	společenský	k2eAgFnPc2d1
<g/>
,	,	kIx,
neurologických	urologický	k2eNgFnPc2d1,k2eAgFnPc2d1
<g/>
,	,	kIx,
endokrinologických	endokrinologický	k2eAgFnPc2d1
<g/>
,	,	kIx,
etc	etc	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
sociální	sociální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
vlivem	vliv	k1gInSc7
společenských	společenský	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
a	a	k8xC
vzájemné	vzájemný	k2eAgFnSc2d1
společenské	společenský	k2eAgFnSc2d1
interakce	interakce	k1gFnSc2
na	na	k7c4
lidskou	lidský	k2eAgFnSc4d1
psychiku	psychika	k1gFnSc4
</s>
<s>
kognitivní	kognitivní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zkoumá	zkoumat	k5eAaImIp3nS
způsob	způsob	k1gInSc4
vnímání	vnímání	k1gNnSc2
a	a	k8xC
zpracování	zpracování	k1gNnSc2
informací	informace	k1gFnPc2
dodaných	dodaný	k2eAgFnPc2d1
smyslovými	smyslový	k2eAgInPc7d1
orgány	orgán	k1gInPc7
</s>
<s>
vývojová	vývojový	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
studuje	studovat	k5eAaImIp3nS
fylogenetický	fylogenetický	k2eAgMnSc1d1
i	i	k8xC
ontogenetický	ontogenetický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
lidské	lidský	k2eAgFnSc2d1
psychiky	psychika	k1gFnSc2
<g/>
,	,	kIx,
sleduje	sledovat	k5eAaImIp3nS
změny	změna	k1gFnPc4
psychiky	psychika	k1gFnSc2
od	od	k7c2
početí	početí	k1gNnSc2
do	do	k7c2
smrti	smrt	k1gFnSc2
</s>
<s>
Aplikované	aplikovaný	k2eAgInPc1d1
obory	obor	k1gInPc1
psychologie	psychologie	k1gFnSc2
</s>
<s>
Výčet	výčet	k1gInSc1
aplikovaných	aplikovaný	k2eAgInPc2d1
oborů	obor	k1gInPc2
psychologie	psychologie	k1gFnSc2
není	být	k5eNaImIp3nS
úplný	úplný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychologové	psycholog	k1gMnPc1
působí	působit	k5eAaImIp3nP
ve	v	k7c6
většině	většina	k1gFnSc6
sfér	sféra	k1gFnPc2
lidské	lidský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
a	a	k8xC
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
označena	označit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
samostatný	samostatný	k2eAgInSc4d1
obor	obor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohé	k1gNnSc1
z	z	k7c2
níže	nízce	k6eAd2
uvedených	uvedený	k2eAgInPc2d1
oborů	obor	k1gInPc2
jsou	být	k5eAaImIp3nP
také	také	k9
dílčími	dílčí	k2eAgFnPc7d1
součástmi	součást	k1gFnPc7
základních	základní	k2eAgFnPc2d1
psychologických	psychologický	k2eAgFnPc2d1
disciplín	disciplína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
psychologie	psychologie	k1gFnSc1
dopravy	doprava	k1gFnSc2
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	s	k7c7
psychikou	psychika	k1gFnSc7
řidičů	řidič	k1gMnPc2
(	(	kIx(
<g/>
zejména	zejména	k9
profesionálních	profesionální	k2eAgMnPc2d1
<g/>
)	)	kIx)
a	a	k8xC
prožíváním	prožívání	k1gNnSc7
situací	situace	k1gFnPc2
vznikajících	vznikající	k2eAgFnPc2d1
při	při	k7c6
řízení	řízení	k1gNnSc6
</s>
<s>
forenzní	forenzní	k2eAgFnSc1d1
(	(	kIx(
<g/>
soudní	soudní	k2eAgFnSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zájem	zájem	k1gInSc1
o	o	k7c4
chování	chování	k1gNnSc4
a	a	k8xC
prožívání	prožívání	k1gNnSc4
lidí	člověk	k1gMnPc2
v	v	k7c6
situacích	situace	k1gFnPc6
regulovaných	regulovaný	k2eAgFnPc2d1
právem	právo	k1gNnSc7
(	(	kIx(
<g/>
např.	např.	kA
přístup	přístup	k1gInSc1
k	k	k7c3
výslechu	výslech	k1gInSc2
svědků	svědek	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
zajišťuje	zajišťovat	k5eAaImIp3nS
odborné	odborný	k2eAgInPc4d1
posudky	posudek	k1gInPc4
v	v	k7c6
soudních	soudní	k2eAgFnPc6d1
záležitostech	záležitost	k1gFnPc6
</s>
<s>
inženýrská	inženýrský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
konstrukcí	konstrukce	k1gFnSc7
strojů	stroj	k1gInPc2
a	a	k8xC
strojních	strojní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
ve	v	k7c6
vztahu	vztah	k1gInSc6
člověku	člověk	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
má	mít	k5eAaImIp3nS
daný	daný	k2eAgInSc4d1
nástroj	nástroj	k1gInSc4
obsluhovat	obsluhovat	k5eAaImF
</s>
<s>
klinická	klinický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
stará	starat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
diagnózu	diagnóza	k1gFnSc4
<g/>
,	,	kIx,
terapii	terapie	k1gFnSc4
i	i	k8xC
prevenci	prevence	k1gFnSc4
duševních	duševní	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
a	a	k8xC
chorob	choroba	k1gFnPc2
<g/>
,	,	kIx,
zabývá	zabývat	k5eAaImIp3nS
se	s	k7c7
vztahem	vztah	k1gInSc7
lékaře	lékař	k1gMnSc2
a	a	k8xC
pacienta	pacient	k1gMnSc2
<g/>
,	,	kIx,
i	i	k8xC
pacienta	pacient	k1gMnSc2
k	k	k7c3
nemocnici	nemocnice	k1gFnSc3
</s>
<s>
psychologie	psychologie	k1gFnSc1
konfliktu	konflikt	k1gInSc2
-	-	kIx~
střetnutí	střetnutí	k1gNnPc2
dvou	dva	k4xCgNnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
více	hodně	k6eAd2
protikladných	protikladný	k2eAgFnPc2d1
tendencí	tendence	k1gFnPc2
či	či	k8xC
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
usilující	usilující	k2eAgFnPc1d1
o	o	k7c4
tutéž	týž	k3xTgFnSc4
věc	věc	k1gFnSc4
s	s	k7c7
přibližně	přibližně	k6eAd1
stejnou	stejný	k2eAgFnSc7d1
motivační	motivační	k2eAgFnSc7d1
valencí	valence	k1gFnSc7
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
provázena	provázet	k5eAaImNgFnS
emočními	emoční	k2eAgInPc7d1
a	a	k8xC
pocitovými	pocitový	k2eAgInPc7d1
jevy	jev	k1gInPc7
</s>
<s>
kosmická	kosmický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zkoumá	zkoumat	k5eAaImIp3nS
psychiku	psychika	k1gFnSc4
kosmonautů	kosmonaut	k1gMnPc2
<g/>
,	,	kIx,
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
psychologickou	psychologický	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
při	při	k7c6
dlouhodobých	dlouhodobý	k2eAgInPc6d1
vesmírných	vesmírný	k2eAgInPc6d1
projektech	projekt	k1gInPc6
<g/>
,	,	kIx,
dlouhým	dlouhý	k2eAgNnSc7d1
odloučením	odloučení	k1gNnSc7
člověka	člověk	k1gMnSc2
od	od	k7c2
ostatních	ostatní	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
problémy	problém	k1gInPc4
samoty	samota	k1gFnSc2
<g/>
,	,	kIx,
vypořádáním	vypořádání	k1gNnSc7
se	se	k3xPyFc4
s	s	k7c7
nebezpečím	nebezpečí	k1gNnSc7
</s>
<s>
lékařská	lékařský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
působením	působení	k1gNnSc7
lékařské	lékařský	k2eAgFnSc2d1
péče	péče	k1gFnSc2
na	na	k7c4
psychiku	psychika	k1gFnSc4
člověka	člověk	k1gMnSc2
</s>
<s>
manažerská	manažerský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
manažerské	manažerský	k2eAgFnPc4d1
dovednosti	dovednost	k1gFnPc4
<g/>
,	,	kIx,
osobnost	osobnost	k1gFnSc4
manažera	manažer	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
obchodní	obchodní	k2eAgFnSc2d1
dovednosti	dovednost	k1gFnSc2
(	(	kIx(
<g/>
umění	umění	k1gNnSc1
vedení	vedení	k1gNnSc2
kolektivu	kolektiv	k1gInSc2
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
psychologie	psychologie	k1gFnSc1
náboženství	náboženství	k1gNnSc2
-	-	kIx~
studuje	studovat	k5eAaImIp3nS
náboženství	náboženství	k1gNnSc4
<g/>
,	,	kIx,
religiozitu	religiozita	k1gFnSc4
a	a	k8xC
spiritualitu	spiritualita	k1gFnSc4
a	a	k8xC
obecně	obecně	k6eAd1
lidskou	lidský	k2eAgFnSc4d1
víru	víra	k1gFnSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
vlivu	vliv	k1gInSc2
na	na	k7c4
psychiku	psychika	k1gFnSc4
člověka	člověk	k1gMnSc2
</s>
<s>
pedagogická	pedagogický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zkoumá	zkoumat	k5eAaImIp3nS
psychologické	psychologický	k2eAgInPc4d1
základy	základ	k1gInPc4
<g/>
,	,	kIx,
činitele	činitel	k1gInPc4
a	a	k8xC
zákonitosti	zákonitost	k1gFnPc4
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
vzdělávání	vzdělávání	k1gNnSc2
a	a	k8xC
vyučování	vyučování	k1gNnSc2
</s>
<s>
politická	politický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
uměním	umění	k1gNnSc7
vyjednávat	vyjednávat	k5eAaImF
<g/>
,	,	kIx,
přesvědčovat	přesvědčovat	k5eAaImF
a	a	k8xC
řešit	řešit	k5eAaImF
konflikty	konflikt	k1gInPc4
</s>
<s>
poradenská	poradenský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
napomáhá	napomáhat	k5eAaImIp3nS,k5eAaBmIp3nS
v	v	k7c6
lepší	dobrý	k2eAgFnSc6d2
orientaci	orientace	k1gFnSc6
při	při	k7c6
složitějších	složitý	k2eAgFnPc6d2
životních	životní	k2eAgFnPc6d1
situacích	situace	k1gFnPc6
<g/>
,	,	kIx,
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
lepšího	dobrý	k2eAgNnSc2d2
poznání	poznání	k1gNnSc2
sebe	sebe	k3xPyFc4
sama	sám	k3xTgFnSc1
<g/>
,	,	kIx,
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c6
optimalizování	optimalizování	k1gNnSc6
rozvoje	rozvoj	k1gInSc2
lidské	lidský	k2eAgFnSc2d1
osobnosti	osobnost	k1gFnSc2
</s>
<s>
psychoterapie	psychoterapie	k1gFnSc1
-	-	kIx~
ačkoliv	ačkoliv	k8xS
je	být	k5eAaImIp3nS
často	často	k6eAd1
uváděna	uvádět	k5eAaImNgFnS
jako	jako	k8xS,k8xC
samostatný	samostatný	k2eAgInSc4d1
obor	obor	k1gInSc4
nebo	nebo	k8xC
praktická	praktický	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
psychologických	psychologický	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
k	k	k7c3
léčení	léčení	k1gNnSc3
a	a	k8xC
studuje	studovat	k5eAaImIp3nS
možnosti	možnost	k1gFnPc1
odstranění	odstranění	k1gNnSc2
duševních	duševní	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
a	a	k8xC
obtíží	obtíž	k1gFnPc2
psychologickou	psychologický	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
</s>
<s>
školní	školní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
školní	školní	k2eAgMnPc1d1
psychologové	psycholog	k1gMnPc1
působí	působit	k5eAaImIp3nP
přímo	přímo	k6eAd1
na	na	k7c6
školách	škola	k1gFnPc6
a	a	k8xC
zabývají	zabývat	k5eAaImIp3nP
se	se	k3xPyFc4
například	například	k6eAd1
diagnostikou	diagnostika	k1gFnSc7
a	a	k8xC
nápravou	náprava	k1gFnSc7
překážek	překážka	k1gFnPc2
v	v	k7c6
učení	učení	k1gNnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
specifických	specifický	k2eAgFnPc2d1
poruch	porucha	k1gFnPc2
učení	učení	k1gNnSc2
<g/>
,	,	kIx,
kariérním	kariérní	k2eAgNnSc7d1
poradenstvím	poradenství	k1gNnSc7
či	či	k8xC
diagnostikou	diagnostika	k1gFnSc7
a	a	k8xC
prevencí	prevence	k1gFnSc7
šikany	šikana	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
psychologie	psychologie	k1gFnSc1
práce	práce	k1gFnSc2
-	-	kIx~
otázky	otázka	k1gFnPc1
bezpečnosti	bezpečnost	k1gFnSc2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
pracovní	pracovní	k2eAgInSc1d1
kolektiv	kolektiv	k1gInSc1
<g/>
,	,	kIx,
studuje	studovat	k5eAaImIp3nS
vliv	vliv	k1gInSc1
pracovního	pracovní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
na	na	k7c4
pracovní	pracovní	k2eAgInSc4d1
výkon	výkon	k1gInSc4
</s>
<s>
psychologie	psychologie	k1gFnSc1
reklamy	reklama	k1gFnSc2
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
efektivitou	efektivita	k1gFnSc7
jednotlivých	jednotlivý	k2eAgMnPc2d1
druhů	druh	k1gInPc2
reklamy	reklama	k1gFnSc2
<g/>
,	,	kIx,
užití	užití	k1gNnSc1
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
log	logo	k1gNnPc2
a	a	k8xC
značek	značka	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
působení	působení	k1gNnSc4
na	na	k7c4
koncového	koncový	k2eAgMnSc4d1
uživatele	uživatel	k1gMnSc4
(	(	kIx(
<g/>
zákazníka	zákazník	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
psychologie	psychologie	k1gFnSc1
sportu	sport	k1gInSc2
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	s	k7c7
tréninkem	trénink	k1gInSc7
<g/>
,	,	kIx,
vlivem	vliv	k1gInSc7
psychiky	psychika	k1gFnSc2
na	na	k7c4
výkonnost	výkonnost	k1gFnSc4
sportovce	sportovec	k1gMnSc2
<g/>
,	,	kIx,
osobností	osobnost	k1gFnSc7
trenéra	trenér	k1gMnSc2
apod.	apod.	kA
</s>
<s>
psychologie	psychologie	k1gFnSc1
trhu	trh	k1gInSc2
-	-	kIx~
zkoumá	zkoumat	k5eAaImIp3nS
trh	trh	k1gInSc4
<g/>
,	,	kIx,
rozhodování	rozhodování	k1gNnSc4
spotřebitelů	spotřebitel	k1gMnPc2
a	a	k8xC
úzce	úzko	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
psychologií	psychologie	k1gFnSc7
reklamy	reklama	k1gFnSc2
</s>
<s>
psychologie	psychologie	k1gFnSc1
umění	umění	k1gNnSc2
-	-	kIx~
zkoumá	zkoumat	k5eAaImIp3nS
vlivy	vliv	k1gInPc1
umění	umění	k1gNnSc4
na	na	k7c4
psychiku	psychika	k1gFnSc4
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc1
prožívání	prožívání	k1gNnSc1
a	a	k8xC
"	"	kIx"
<g/>
čtení	čtení	k1gNnSc1
<g/>
"	"	kIx"
uměleckých	umělecký	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
</s>
<s>
vojenská	vojenský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
podmínkami	podmínka	k1gFnPc7
výcviku	výcvik	k1gInSc2
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
interpersonálními	interpersonální	k2eAgInPc7d1
vztahy	vztah	k1gInPc7
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
,	,	kIx,
zkoumá	zkoumat	k5eAaImIp3nS
odolnost	odolnost	k1gFnSc4
vůči	vůči	k7c3
zátěži	zátěž	k1gFnSc3
v	v	k7c6
podmínkách	podmínka	k1gFnPc6
válečných	válečný	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
<g/>
,	,	kIx,
potažmo	potažmo	k6eAd1
chování	chování	k1gNnSc1
celých	celý	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
(	(	kIx(
<g/>
armád	armáda	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c6
bitevním	bitevní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
a	a	k8xC
jejich	jejich	k3xOp3gInPc4
psychologické	psychologický	k2eAgInPc4d1
dopady	dopad	k1gInPc4
na	na	k7c4
civilní	civilní	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
</s>
<s>
psychologie	psychologie	k1gFnSc1
zdraví	zdraví	k1gNnSc2
-	-	kIx~
aplikuje	aplikovat	k5eAaBmIp3nS
psychologii	psychologie	k1gFnSc4
na	na	k7c4
oblast	oblast	k1gFnSc4
osvěty	osvěta	k1gFnSc2
zdraví	zdraví	k1gNnSc2
a	a	k8xC
zdravého	zdravý	k2eAgInSc2d1
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
a	a	k8xC
prevencí	prevence	k1gFnPc2
<g/>
,	,	kIx,
pomáhá	pomáhat	k5eAaImIp3nS
trpícím	trpící	k2eAgInPc3d1
a	a	k8xC
nevyléčitelně	vyléčitelně	k6eNd1
nemocným	nemocná	k1gFnPc3
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
podporu	podpora	k1gFnSc4
kvalitního	kvalitní	k2eAgInSc2d1
a	a	k8xC
zdravého	zdravý	k2eAgInSc2d1
života	život	k1gInSc2
od	od	k7c2
narození	narození	k1gNnSc2
po	po	k7c4
smrt	smrt	k1gFnSc4
</s>
<s>
psychologie	psychologie	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
též	též	k9
environmentální	environmentální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g/>
)	)	kIx)
-	-	kIx~
zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
vzájemné	vzájemný	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
životním	životní	k2eAgNnSc7d1
prostředím	prostředí	k1gNnSc7
a	a	k8xC
lidským	lidský	k2eAgNnSc7d1
vnímáním	vnímání	k1gNnSc7
<g/>
,	,	kIx,
chováním	chování	k1gNnSc7
a	a	k8xC
prožíváním	prožívání	k1gNnSc7
</s>
<s>
Speciální	speciální	k2eAgInPc1d1
</s>
<s>
biopsychologie	biopsychologie	k1gFnSc1
-	-	kIx~
shromažďuje	shromažďovat	k5eAaImIp3nS
poznatky	poznatek	k1gInPc1
o	o	k7c6
biologicko-fyziologické	biologicko-fyziologický	k2eAgFnSc6d1
determinaci	determinace	k1gFnSc6
psychiky	psychika	k1gFnSc2
</s>
<s>
dějiny	dějiny	k1gFnPc1
psychologie	psychologie	k1gFnSc2
-	-	kIx~
zkoumá	zkoumat	k5eAaImIp3nS
vývoj	vývoj	k1gInSc1
psychologického	psychologický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
během	během	k7c2
času	čas	k1gInSc2
od	od	k7c2
nejstarších	starý	k2eAgFnPc2d3
dob	doba	k1gFnPc2
</s>
<s>
farmakopsychologie	farmakopsychologie	k1gFnSc1
-	-	kIx~
sleduje	sledovat	k5eAaImIp3nS
účinky	účinek	k1gInPc4
chemických	chemický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
drog	droga	k1gFnPc2
<g/>
,	,	kIx,
léků	lék	k1gInPc2
<g/>
,	,	kIx,
atd.	atd.	kA
na	na	k7c4
psychiku	psychika	k1gFnSc4
</s>
<s>
psychodiagnostika	psychodiagnostika	k1gFnSc1
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
diagnostikou	diagnostika	k1gFnSc7
<g/>
,	,	kIx,
tedy	tedy	k9
výzkumem	výzkum	k1gInSc7
vlastností	vlastnost	k1gFnPc2
konkrétního	konkrétní	k2eAgMnSc2d1
jedince	jedinec	k1gMnSc2
s	s	k7c7
cílem	cíl	k1gInSc7
poznat	poznat	k5eAaPmF
jeho	jeho	k3xOp3gFnPc4
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
navrhnout	navrhnout	k5eAaPmF
vhodný	vhodný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
léčby	léčba	k1gFnSc2
či	či	k8xC
předvídat	předvídat	k5eAaImF
jeho	jeho	k3xOp3gNnSc4
budoucí	budoucí	k2eAgNnSc4d1
chování	chování	k1gNnSc4
</s>
<s>
psycholingvistika	psycholingvistika	k1gFnSc1
-	-	kIx~
zajímá	zajímat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
myšlením	myšlení	k1gNnSc7
a	a	k8xC
řečí	řeč	k1gFnSc7
u	u	k7c2
lidí	člověk	k1gMnPc2
</s>
<s>
psychometrika	psychometrika	k1gFnSc1
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
konstrukcí	konstrukce	k1gFnSc7
testů	test	k1gInPc2
a	a	k8xC
technikou	technika	k1gFnSc7
měření	měření	k1gNnSc2
mentálních	mentální	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
či	či	k8xC
osobnostních	osobnostní	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
</s>
<s>
zoopsychologie	zoopsychologie	k1gFnSc1
-	-	kIx~
zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
psychikou	psychika	k1gFnSc7
zvířat	zvíře	k1gNnPc2
různého	různý	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
vývoje	vývoj	k1gInSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HARTL	Hartl	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychologický	psychologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
303	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
HAYES	HAYES	kA
<g/>
,	,	kIx,
Nicky	nicka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikovaná	aplikovaný	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
807	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PLHÁKOVÁ	PLHÁKOVÁ	kA
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učebnice	učebnice	k1gFnSc1
obecné	obecný	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1086	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PLHÁKOVÁ	PLHÁKOVÁ	kA
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učebnice	učebnice	k1gFnSc1
obecné	obecný	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1086	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Psychologie	psychologie	k1gFnSc1
</s>
<s>
Psychologické	psychologický	k2eAgInPc1d1
směry	směr	k1gInPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Psychologie	psychologie	k1gFnSc1
Teoretické	teoretický	k2eAgFnSc2d1
psychologické	psychologický	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
</s>
<s>
Základní	základní	k2eAgFnPc1d1
psychologické	psychologický	k2eAgFnPc1d1
disciplíny	disciplína	k1gFnPc1
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
(	(	kIx(
<g/>
Psychologie	psychologie	k1gFnSc1
barev	barva	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Biologická	biologický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
(	(	kIx(
<g/>
Psychobiologie	Psychobiologie	k1gFnSc1
•	•	k?
Neuropsychologie	Neuropsychologie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Psychologie	psychologie	k1gFnSc1
osobnosti	osobnost	k1gFnSc2
•	•	k?
Vývojová	vývojový	k2eAgFnSc1d1
(	(	kIx(
<g/>
ontogenetická	ontogenetický	k2eAgFnSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
•	•	k?
Sociální	sociální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologická	psychologický	k2eAgFnSc1d1
metodologie	metodologie	k1gFnSc1
•	•	k?
Psychopatologie	psychopatologie	k1gFnSc2
•	•	k?
Dějiny	dějiny	k1gFnPc1
psychologie	psychologie	k1gFnSc2
•	•	k?
Srovnávací	srovnávací	k2eAgNnSc1d1
(	(	kIx(
<g/>
komparativní	komparativní	k2eAgFnSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
Speciální	speciální	k2eAgFnSc2d1
psychologické	psychologický	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1
(	(	kIx(
<g/>
diferenciační	diferenciační	k2eAgFnSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
•	•	k?
Biopsychologie	Biopsychologie	k1gFnSc2
•	•	k?
Psychofyziologie	psychofyziologie	k1gFnSc2
•	•	k?
Genetická	genetický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Chronopsychologie	Chronopsychologie	k1gFnSc1
•	•	k?
Psychometrie	Psychometrie	k1gFnSc1
=	=	kIx~
psychometrika	psychometrika	k1gFnSc1
•	•	k?
Patopsychologie	patopsychologie	k1gFnSc2
•	•	k?
Farmakopsychologie	Farmakopsychologie	k1gFnSc2
•	•	k?
Zoopsychologie	zoopsychologie	k1gFnSc2
•	•	k?
Kulturně	kulturně	k6eAd1
srovnávací	srovnávací	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
mezikulturní	mezikulturní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Geopsychologie	Geopsychologie	k1gFnSc1
•	•	k?
Psychodiagnostika	psychodiagnostika	k1gFnSc1
•	•	k?
Psycholingvistika	psycholingvistika	k1gFnSc1
•	•	k?
Psychofyzika	psychofyzika	k1gFnSc1
•	•	k?
Matematická	matematický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc2
myšlení	myšlení	k1gNnSc2
•	•	k?
Smyslová	smyslový	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Kognitivní	kognitivní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Percepční	percepční	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
</s>
<s>
Aplikované	aplikovaný	k2eAgFnPc1d1
(	(	kIx(
<g/>
užité	užitý	k2eAgFnPc1d1
<g/>
)	)	kIx)
psychologické	psychologický	k2eAgFnPc1d1
disciplíny	disciplína	k1gFnPc1
(	(	kIx(
<g/>
aplikovaná	aplikovaný	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Poradenská	poradenský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Klinická	klinický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Lékařská	lékařský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Zdravotnická	zdravotnický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Duševní	duševní	k2eAgFnSc1d1
hygiena	hygiena	k1gFnSc1
=	=	kIx~
psychohygiena	psychohygiena	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc2
zdraví	zdraví	k1gNnSc2
•	•	k?
Environmentální	environmentální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
ekologická	ekologický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
psychologie	psychologie	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
Psychologie	psychologie	k1gFnSc2
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Ekopsychologie	Ekopsychologie	k1gFnSc2
•	•	k?
Environmentální	environmentální	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
dítěte	dítě	k1gNnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc1
mládeže	mládež	k1gFnSc2
•	•	k?
Pedagogická	pedagogický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Školní	školní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
vzdělávání	vzdělávání	k1gNnSc2
•	•	k?
Interkulturní	Interkulturní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnPc4
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
•	•	k?
Psychologie	psychologie	k1gFnPc4
volného	volný	k2eAgInSc2d1
času	čas	k1gInSc2
•	•	k?
Psychologie	psychologie	k1gFnSc1
sportu	sport	k1gInSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
kultury	kultura	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
umění	umění	k1gNnSc2
•	•	k?
Bibliopsychologie	Bibliopsychologie	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
čtenáře	čtenář	k1gMnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
náboženství	náboženství	k1gNnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Právní	právní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Policejní	policejní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
•	•	k?
Kriminální	kriminální	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
•	•	k?
Forenzní	forenzní	k2eAgNnSc1d1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
soudní	soudní	k2eAgInSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
(	(	kIx(
<g/>
Kriminologická	kriminologický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Penitenciární	Penitenciární	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
psychologie	psychologie	k1gFnSc1
vězeňství	vězeňství	k1gNnSc2
•	•	k?
Postpenitenciární	Postpenitenciární	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Politické	politický	k2eAgNnSc1d1
profilování	profilování	k1gNnSc1
•	•	k?
Psychologické	psychologický	k2eAgNnSc4d1
profilování	profilování	k1gNnSc4
•	•	k?
Psychologie	psychologie	k1gFnSc2
katastrof	katastrofa	k1gFnPc2
•	•	k?
Vojenská	vojenský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Kosmická	kosmický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc1
konfliktu	konflikt	k1gInSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
práce	práce	k1gFnSc2
a	a	k8xC
organizace	organizace	k1gFnSc2
=	=	kIx~
psychologie	psychologie	k1gFnSc1
práce	práce	k1gFnSc2
=	=	kIx~
průmyslová	průmyslový	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Inženýrská	inženýrský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Personální	personální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
poradenská	poradenský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
práce	práce	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
organizace	organizace	k1gFnSc2
a	a	k8xC
řízení	řízení	k1gNnSc2
=	=	kIx~
sociální	sociální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
práce	práce	k1gFnSc2
/	/	kIx~
<g/>
Psychologie	psychologie	k1gFnSc1
řízení	řízení	k1gNnSc2
<g/>
/	/	kIx~
•	•	k?
Psychologie	psychologie	k1gFnSc2
pracovní	pracovní	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc1
dopravy	doprava	k1gFnSc2
=	=	kIx~
dopravní	dopravní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Psychologie	psychologie	k1gFnSc1
organizace	organizace	k1gFnSc2
•	•	k?
Manažerská	manažerský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Marketingová	marketingový	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc1
trhu	trh	k1gInSc2
(	(	kIx(
<g/>
Psychologie	psychologie	k1gFnSc1
propagace	propagace	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
reklamy	reklama	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Psychologie	psychologie	k1gFnSc1
obchodu	obchod	k1gInSc2
Psychologické	psychologický	k2eAgInPc4d1
směry	směr	k1gInPc4
</s>
<s>
Behaviorismus	behaviorismus	k1gInSc1
•	•	k?
Neobehaviorismus	Neobehaviorismus	k1gInSc1
•	•	k?
Gestaltismus	Gestaltismus	k1gInSc1
=	=	kIx~
gestaltpsychologie	gestaltpsychologie	k1gFnSc1
=	=	kIx~
gestaltistická	gestaltistický	k2eAgFnSc1d1
(	(	kIx(
<g/>
tvarová	tvarový	k2eAgFnSc1d1
<g/>
,	,	kIx,
celostní	celostní	k2eAgFnSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
•	•	k?
Neogestaltismus	Neogestaltismus	k1gInSc1
•	•	k?
Psychoanalýza	psychoanalýza	k1gFnSc1
•	•	k?
Neopsychoanalýza	Neopsychoanalýza	k1gFnSc1
•	•	k?
Kognitivní	kognitivní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Humanistická	humanistický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Experimentální	experimentální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Hlubinná	hlubinný	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
Psychologické	psychologický	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
•	•	k?
Předvědecká	předvědecký	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Pozitivní	pozitivní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psycholog	psycholog	k1gMnSc1
•	•	k?
Kategorie	kategorie	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
9213	#num#	k4
</s>
