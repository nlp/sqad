<s>
JUDr.	JUDr.	kA	JUDr.
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1901	[number]	k4	1901
Královské	královský	k2eAgInPc1d1	královský
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
politička	politička	k1gFnSc1	politička
<g/>
,	,	kIx,	,
oběť	oběť	k1gFnSc1	oběť
justiční	justiční	k2eAgFnSc2d1	justiční
vraždy	vražda	k1gFnSc2	vražda
během	během	k7c2	během
komunistických	komunistický	k2eAgInPc2d1	komunistický
politických	politický	k2eAgInPc2d1	politický
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
za	za	k7c4	za
vykonstruované	vykonstruovaný	k2eAgNnSc4d1	vykonstruované
spiknutí	spiknutí	k1gNnSc4	spiknutí
a	a	k8xC	a
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
ženou	žena	k1gFnSc7	žena
popravenou	popravený	k2eAgFnSc7d1	popravená
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgFnPc2	tento
soudních	soudní	k2eAgFnPc2d1	soudní
řízení	řízení	k1gNnPc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
neústupnosti	neústupnost	k1gFnSc3	neústupnost
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
soudu	soud	k1gInSc2	soud
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
totalitě	totalita	k1gFnSc3	totalita
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
na	na	k7c6	na
Královských	královský	k2eAgInPc6d1	královský
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Praha	Praha	k1gFnSc1	Praha
2	[number]	k4	2
<g/>
-Vinohrady	-Vinohrada	k1gFnPc1	-Vinohrada
<g/>
)	)	kIx)	)
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Čeňka	Čeněk	k1gMnSc2	Čeněk
Krále	Král	k1gMnSc2	Král
<g/>
,	,	kIx,	,
oddaného	oddaný	k2eAgMnSc2d1	oddaný
Masarykově	Masarykův	k2eAgFnSc3d1	Masarykova
myšlence	myšlenka	k1gFnSc3	myšlenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
protirakouských	protirakouský	k2eAgInPc2d1	protirakouský
postojů	postoj	k1gInPc2	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
tři	tři	k4xCgMnPc4	tři
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
-	-	kIx~	-
starší	starý	k2eAgFnSc1d2	starší
Marta	Marta	k1gFnSc1	Marta
a	a	k8xC	a
mladší	mladý	k2eAgMnSc1d2	mladší
Jiří	Jiří	k1gMnSc1	Jiří
-	-	kIx~	-
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
na	na	k7c4	na
septickou	septický	k2eAgFnSc4d1	septická
spálu	spála	k1gFnSc4	spála
<g/>
.	.	kIx.	.
</s>
<s>
Dospělosti	dospělost	k1gFnPc1	dospělost
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dožila	dožít	k5eAaPmAgFnS	dožít
jen	jen	k9	jen
o	o	k7c4	o
16	[number]	k4	16
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
Věra	Věra	k1gFnSc1	Věra
(	(	kIx(	(
<g/>
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Tůmová	Tůmová	k1gFnSc1	Tůmová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
Milada	Milada	k1gFnSc1	Milada
po	po	k7c6	po
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
matky	matka	k1gFnSc2	matka
také	také	k9	také
starala	starat	k5eAaImAgFnS	starat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
za	za	k7c2	za
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
protiválečných	protiválečný	k2eAgFnPc6d1	protiválečná
demonstracích	demonstrace	k1gFnPc6	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
studentstvu	studentstvo	k1gNnSc6	studentstvo
zakázané	zakázaný	k2eAgNnSc1d1	zakázané
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jí	on	k3xPp3gFnSc3	on
však	však	k9	však
byl	být	k5eAaImAgInS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
přestup	přestup	k1gInSc1	přestup
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
pražské	pražský	k2eAgNnSc4d1	Pražské
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
maturovala	maturovat	k5eAaBmAgFnS	maturovat
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zvažovala	zvažovat	k5eAaImAgFnS	zvažovat
studium	studium	k1gNnSc4	studium
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c4	po
doporučení	doporučení	k1gNnSc4	doporučení
otce	otec	k1gMnSc2	otec
padlo	padnout	k5eAaImAgNnS	padnout
její	její	k3xOp3gFnSc4	její
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
pro	pro	k7c4	pro
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
promovala	promovat	k5eAaBmAgFnS	promovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
těžce	těžce	k6eAd1	těžce
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
rodinně	rodinně	k6eAd1	rodinně
osudovou	osudový	k2eAgFnSc7d1	osudová
spálou	spála	k1gFnSc7	spála
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
mládí	mládí	k1gNnSc3	mládí
však	však	k8xC	však
nemoci	nemoc	k1gFnSc2	nemoc
nepodlehla	podlehnout	k5eNaPmAgFnS	podlehnout
a	a	k8xC	a
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
setkala	setkat	k5eAaPmAgNnP	setkat
se	s	k7c7	s
senátorkou	senátorka	k1gFnSc7	senátorka
Františkou	Františka	k1gFnSc7	Františka
Plamínkovou	plamínkový	k2eAgFnSc7d1	Plamínková
<g/>
,	,	kIx,	,
zakladatelkou	zakladatelka	k1gFnSc7	zakladatelka
Ženské	ženský	k2eAgFnSc2d1	ženská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ŽNR	ŽNR	kA	ŽNR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
také	také	k9	také
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
oporou	opora	k1gFnSc7	opora
Plamínkové	plamínkový	k2eAgFnPc1d1	Plamínková
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
a	a	k8xC	a
následně	následně	k6eAd1	následně
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
osobností	osobnost	k1gFnSc7	osobnost
ŽNR	ŽNR	kA	ŽNR
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Bohuslava	Bohuslav	k1gMnSc4	Bohuslav
Horáka	Horák	k1gMnSc4	Horák
<g/>
,	,	kIx,	,
zemědělského	zemědělský	k2eAgMnSc4d1	zemědělský
ekonoma	ekonom	k1gMnSc4	ekonom
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
znala	znát	k5eAaImAgFnS	znát
již	již	k6eAd1	již
ze	z	k7c2	z
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Horák	Horák	k1gMnSc1	Horák
byl	být	k5eAaImAgMnS	být
redaktorem	redaktor	k1gMnSc7	redaktor
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
jeho	jeho	k3xOp3gMnSc7	jeho
programovým	programový	k2eAgMnSc7d1	programový
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
pak	pak	k6eAd1	pak
hluboce	hluboko	k6eAd1	hluboko
věřící	věřící	k2eAgFnSc1d1	věřící
Horáková	Horáková	k1gFnSc1	Horáková
přestoupila	přestoupit	k5eAaPmAgFnS	přestoupit
i	i	k9	i
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
od	od	k7c2	od
římsko-katolické	římskoatolický	k2eAgFnSc2d1	římsko-katolická
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
Českobratrské	českobratrský	k2eAgFnSc3d1	Českobratrská
církvi	církev	k1gFnSc3	církev
evangelické	evangelický	k2eAgFnPc1d1	evangelická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
manželům	manžel	k1gMnPc3	manžel
Horákovým	Horáková	k1gFnPc3	Horáková
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
významnou	významný	k2eAgFnSc7d1	významná
českou	český	k2eAgFnSc7d1	Česká
feministkou	feministka	k1gFnSc7	feministka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
národně	národně	k6eAd1	národně
sociální	sociální	k2eAgFnSc2d1	sociální
(	(	kIx(	(
<g/>
ČSNS	ČSNS	kA	ČSNS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
těžiště	těžiště	k1gNnSc1	těžiště
její	její	k3xOp3gFnSc2	její
činnosti	činnost	k1gFnSc2	činnost
byla	být	k5eAaImAgFnS	být
nestranická	stranický	k2eNgFnSc1d1	nestranická
Ženská	ženský	k2eAgFnSc1d1	ženská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
ŽNR	ŽNR	kA	ŽNR
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
angažovala	angažovat	k5eAaBmAgFnS	angažovat
se	se	k3xPyFc4	se
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
zrovnoprávnění	zrovnoprávnění	k1gNnSc2	zrovnoprávnění
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
legislativě	legislativa	k1gFnSc6	legislativa
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
staré	starý	k2eAgInPc1d1	starý
zákony	zákon	k1gInPc1	zákon
musely	muset	k5eAaImAgInP	muset
být	být	k5eAaImF	být
podřízeny	podřízen	k2eAgInPc1d1	podřízen
nové	nový	k2eAgInPc1d1	nový
ústavě	ústava	k1gFnSc3	ústava
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
zakotvena	zakotven	k2eAgFnSc1d1	zakotvena
rovnost	rovnost	k1gFnSc1	rovnost
<g/>
.	.	kIx.	.
</s>
<s>
Podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
nového	nový	k2eAgInSc2d1	nový
občanského	občanský	k2eAgInSc2d1	občanský
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
,	,	kIx,	,
na	na	k7c6	na
částech	část	k1gFnPc6	část
o	o	k7c6	o
rodině	rodina	k1gFnSc6	rodina
a	a	k8xC	a
ženské	ženský	k2eAgFnSc6d1	ženská
rovnoprávnosti	rovnoprávnost	k1gFnSc6	rovnoprávnost
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
jako	jako	k9	jako
celek	celek	k1gInSc1	celek
nebyl	být	k5eNaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
socialistka	socialistka	k1gFnSc1	socialistka
byla	být	k5eAaImAgFnS	být
také	také	k9	také
aktivní	aktivní	k2eAgFnSc1d1	aktivní
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
solidarity	solidarita	k1gFnSc2	solidarita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
sociální	sociální	k2eAgFnSc2d1	sociální
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
Československého	československý	k2eAgInSc2d1	československý
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgInPc2d1	další
sociálních	sociální	k2eAgInPc2d1	sociální
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
činnosti	činnost	k1gFnSc3	činnost
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaPmAgFnS	věnovat
i	i	k9	i
profesionálně	profesionálně	k6eAd1	profesionálně
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
Ústřední	ústřední	k2eAgInSc4d1	ústřední
sociální	sociální	k2eAgInSc4d1	sociální
úřad	úřad	k1gInSc4	úřad
pražského	pražský	k2eAgInSc2d1	pražský
magistrátu	magistrát	k1gInSc2	magistrát
pod	pod	k7c4	pod
někdejšího	někdejší	k2eAgMnSc4d1	někdejší
vedoucího	vedoucí	k2eAgMnSc4d1	vedoucí
Petra	Petr	k1gMnSc4	Petr
Zenkla	Zenkla	k1gMnSc4	Zenkla
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
vedoucí	vedoucí	k1gFnSc1	vedoucí
odboru	odbor	k1gInSc2	odbor
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Cestovala	cestovat	k5eAaImAgFnS	cestovat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
rozšíření	rozšíření	k1gNnSc2	rozšíření
obzorů	obzor	k1gInPc2	obzor
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
o	o	k7c6	o
bolševickém	bolševický	k2eAgNnSc6d1	bolševické
Rusku	Rusko	k1gNnSc6	Rusko
tak	tak	k6eAd1	tak
neměla	mít	k5eNaImAgFnS	mít
žádné	žádný	k3yNgFnPc4	žádný
iluze	iluze	k1gFnPc4	iluze
<g/>
,	,	kIx,	,
zvládala	zvládat	k5eAaImAgFnS	zvládat
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
francouzštinu	francouzština	k1gFnSc4	francouzština
a	a	k8xC	a
němčinu	němčina	k1gFnSc4	němčina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
byly	být	k5eAaImAgFnP	být
vdané	vdaný	k2eAgFnPc1d1	vdaná
ženy	žena	k1gFnPc1	žena
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
službách	služba	k1gFnPc6	služba
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
penzionovány	penzionován	k2eAgInPc4d1	penzionován
pro	pro	k7c4	pro
uvolnění	uvolnění	k1gNnSc4	uvolnění
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
odsunuté	odsunutý	k2eAgMnPc4d1	odsunutý
z	z	k7c2	z
území	území	k1gNnPc2	území
připojeného	připojený	k2eAgInSc2d1	připojený
k	k	k7c3	k
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
a	a	k8xC	a
Horáková	Horáková	k1gFnSc1	Horáková
proto	proto	k8xC	proto
musela	muset	k5eAaImAgFnS	muset
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Skrze	Skrze	k?	Skrze
ŽNR	ŽNR	kA	ŽNR
organizovala	organizovat	k5eAaBmAgFnS	organizovat
sociální	sociální	k2eAgFnSc4d1	sociální
pomoc	pomoc	k1gFnSc4	pomoc
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
světovou	světový	k2eAgFnSc7d1	světová
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
i	i	k9	i
uprchlíkům	uprchlík	k1gMnPc3	uprchlík
z	z	k7c2	z
pohraničí	pohraničí	k1gNnSc2	pohraničí
ČSR	ČSR	kA	ČSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
odbojové	odbojový	k2eAgFnSc2d1	odbojová
organizace	organizace	k1gFnSc2	organizace
Petiční	petiční	k2eAgInSc1d1	petiční
výbor	výbor	k1gInSc1	výbor
Věrni	věren	k2eAgMnPc1d1	věren
zůstaneme	zůstat	k5eAaPmIp1nP	zůstat
(	(	kIx(	(
<g/>
PVVZ	PVVZ	kA	PVVZ
<g/>
)	)	kIx)	)
a	a	k8xC	a
Politické	politický	k2eAgNnSc1d1	politické
ústředí	ústředí	k1gNnSc1	ústředí
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
aktivně	aktivně	k6eAd1	aktivně
vyhledávala	vyhledávat	k5eAaImAgFnS	vyhledávat
na	na	k7c6	na
struktuře	struktura	k1gFnSc6	struktura
bývalé	bývalý	k2eAgFnSc6d1	bývalá
ŽNR	ŽNR	kA	ŽNR
nové	nový	k2eAgMnPc4d1	nový
ilegální	ilegální	k2eAgMnPc4d1	ilegální
pracovníky	pracovník	k1gMnPc4	pracovník
<g/>
,	,	kIx,	,
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
tajné	tajný	k2eAgInPc4d1	tajný
byty	byt	k1gInPc4	byt
<g/>
,	,	kIx,	,
získávala	získávat	k5eAaImAgFnS	získávat
zpravodajské	zpravodajský	k2eAgFnPc4d1	zpravodajská
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
klíčové	klíčový	k2eAgFnPc4d1	klíčová
osobnosti	osobnost	k1gFnPc4	osobnost
PVVZ	PVVZ	kA	PVVZ
a	a	k8xC	a
podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
formulování	formulování	k1gNnSc4	formulování
jeho	jeho	k3xOp3gInSc2	jeho
prvotního	prvotní	k2eAgInSc2d1	prvotní
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Neušla	ujít	k5eNaPmAgFnS	ujít
však	však	k9	však
pozornosti	pozornost	k1gFnPc4	pozornost
gestapa	gestapo	k1gNnSc2	gestapo
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
zatčena	zatčen	k2eAgFnSc1d1	zatčena
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
ŽNR	ŽNR	kA	ŽNR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
napojení	napojení	k1gNnSc2	napojení
na	na	k7c4	na
odboj	odboj	k1gInSc4	odboj
byla	být	k5eAaImAgFnS	být
tvrdě	tvrdě	k6eAd1	tvrdě
vyslýchána	vyslýchán	k2eAgFnSc1d1	vyslýchána
a	a	k8xC	a
bita	bit	k2eAgFnSc1d1	bita
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
gestapo	gestapo	k1gNnSc4	gestapo
však	však	k9	však
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
byla	být	k5eAaImAgFnS	být
zadržena	zadržet	k5eAaPmNgFnS	zadržet
na	na	k7c6	na
Pankráci	Pankrác	k1gFnSc6	Pankrác
a	a	k8xC	a
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
byla	být	k5eAaImAgFnS	být
převezena	převezen	k2eAgFnSc1d1	převezena
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
pevnosti	pevnost	k1gFnSc2	pevnost
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
později	pozdě	k6eAd2	pozdě
vedla	vést	k5eAaImAgFnS	vést
oddělení	oddělení	k1gNnSc4	oddělení
tamější	tamější	k2eAgFnSc2d1	tamější
"	"	kIx"	"
<g/>
marodky	marodka	k1gFnSc2	marodka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jí	on	k3xPp3gFnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
relativně	relativně	k6eAd1	relativně
volný	volný	k2eAgInSc4d1	volný
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
lágru	lágr	k1gInSc6	lágr
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
setkala	setkat	k5eAaPmAgNnP	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vzorem	vzor	k1gInSc7	vzor
Františkou	Františka	k1gFnSc7	Františka
Plamínkovou	plamínkový	k2eAgFnSc7d1	Plamínková
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
popravenou	popravený	k2eAgFnSc4d1	popravená
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
pevnosti	pevnost	k1gFnSc2	pevnost
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1944	[number]	k4	1944
převezena	převézt	k5eAaPmNgFnS	převézt
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
s	s	k7c7	s
Horákovou	Horáková	k1gFnSc7	Horáková
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sama	sám	k3xTgFnSc1	sám
hájila	hájit	k5eAaImAgFnS	hájit
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Prokurátor	prokurátor	k1gMnSc1	prokurátor
jí	jíst	k5eAaImIp3nS	jíst
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
soud	soud	k1gInSc1	soud
nakonec	nakonec	k6eAd1	nakonec
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
8	[number]	k4	8
roků	rok	k1gInPc2	rok
káznice	káznice	k1gFnSc2	káznice
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
si	se	k3xPyFc3	se
odpykávala	odpykávat	k5eAaImAgFnS	odpykávat
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
věznici	věznice	k1gFnSc6	věznice
v	v	k7c6	v
Aichachu	Aichach	k1gInSc6	Aichach
u	u	k7c2	u
Mnichova	Mnichov	k1gInSc2	Mnichov
(	(	kIx(	(
<g/>
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Dachau	Dachaus	k1gInSc2	Dachaus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
pochod	pochod	k1gInSc4	pochod
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konzultacích	konzultace	k1gFnPc6	konzultace
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Benešem	Beneš	k1gMnSc7	Beneš
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
apeloval	apelovat	k5eAaImAgMnS	apelovat
na	na	k7c4	na
potřebu	potřeba	k1gFnSc4	potřeba
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
stranické	stranický	k2eAgFnSc2d1	stranická
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
velení	velení	k1gNnSc2	velení
své	svůj	k3xOyFgFnSc2	svůj
obnovené	obnovený	k2eAgFnSc2d1	obnovená
strany	strana	k1gFnSc2	strana
ČSNS	ČSNS	kA	ČSNS
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Obnovila	obnovit	k5eAaPmAgFnS	obnovit
také	také	k9	také
ŽNR	ŽNR	kA	ŽNR
pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
Rady	rada	k1gFnSc2	rada
československých	československý	k2eAgFnPc2d1	Československá
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
RČŽ	RČŽ	kA	RČŽ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
<g/>
;	;	kIx,	;
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
spolupracovnicemi	spolupracovnice	k1gFnPc7	spolupracovnice
z	z	k7c2	z
Rady	rada	k1gFnSc2	rada
založila	založit	k5eAaPmAgFnS	založit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
ženský	ženský	k2eAgInSc1d1	ženský
časopis	časopis	k1gInSc1	časopis
s	s	k7c7	s
názvem	název	k1gInSc7	název
Vlasta	Vlasta	k1gMnSc1	Vlasta
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
nejprodávanější	prodávaný	k2eAgNnSc1d3	nejprodávanější
periodikum	periodikum	k1gNnSc1	periodikum
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
vychází	vycházet	k5eAaImIp3nS	vycházet
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
místopředsedkyní	místopředsedkyně	k1gFnSc7	místopředsedkyně
Svazu	svaz	k1gInSc2	svaz
osvobozených	osvobozený	k2eAgMnPc2d1	osvobozený
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
členkou	členka	k1gFnSc7	členka
Svazu	svaz	k1gInSc2	svaz
přátel	přítel	k1gMnPc2	přítel
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
za	za	k7c4	za
ČSNS	ČSNS	kA	ČSNS
v	v	k7c6	v
Zemi	zem	k1gFnSc6	zem
české	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
a	a	k8xC	a
ústavněprávního	ústavněprávní	k2eAgInSc2d1	ústavněprávní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
kritická	kritický	k2eAgFnSc1d1	kritická
vůči	vůči	k7c3	vůči
činnosti	činnost	k1gFnSc3	činnost
poválečných	poválečný	k2eAgInPc2d1	poválečný
lidových	lidový	k2eAgInPc2d1	lidový
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sociálních	sociální	k2eAgFnPc2d1	sociální
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
(	(	kIx(	(
<g/>
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
Marshallova	Marshallův	k2eAgInSc2d1	Marshallův
plánu	plán	k1gInSc2	plán
<g/>
)	)	kIx)	)
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
rozpoznala	rozpoznat	k5eAaPmAgFnS	rozpoznat
servilní	servilní	k2eAgFnSc1d1	servilní
poslušnost	poslušnost	k1gFnSc1	poslušnost
československých	československý	k2eAgMnPc2d1	československý
komunistů	komunista	k1gMnPc2	komunista
vůči	vůči	k7c3	vůči
Moskvě	Moskva	k1gFnSc3	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
byla	být	k5eAaImAgFnS	být
monitorována	monitorován	k2eAgFnSc1d1	monitorována
StB	StB	k1gFnSc1	StB
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k9	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
ovládána	ovládán	k2eAgFnSc1d1	ovládána
právě	právě	k9	právě
komunisty	komunista	k1gMnPc4	komunista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
vládní	vládní	k2eAgFnSc2d1	vládní
krize	krize	k1gFnSc2	krize
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
o	o	k7c4	o
svolání	svolání	k1gNnSc4	svolání
schůze	schůze	k1gFnSc2	schůze
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stranické	stranický	k2eAgFnSc6d1	stranická
schůzi	schůze	k1gFnSc6	schůze
ČSNS	ČSNS	kA	ČSNS
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
odhalila	odhalit	k5eAaPmAgFnS	odhalit
Aloise	Alois	k1gMnSc4	Alois
Neumana	Neuman	k1gMnSc4	Neuman
jako	jako	k8xS	jako
skrytého	skrytý	k2eAgMnSc4d1	skrytý
komunistu	komunista	k1gMnSc4	komunista
<g/>
,	,	kIx,	,
Neuman	Neuman	k1gMnSc1	Neuman
ze	z	k7c2	z
schůze	schůze	k1gFnSc2	schůze
utekl	utéct	k5eAaPmAgMnS	utéct
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
ještě	ještě	k9	ještě
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
obrozené	obrozený	k2eAgFnSc2d1	obrozená
Gottwaldovy	Gottwaldův	k2eAgFnSc2d1	Gottwaldova
vlády	vláda	k1gFnSc2	vláda
jako	jako	k8xC	jako
ministr-zástupce	ministrástupce	k1gFnSc2	ministr-zástupce
ČSNS	ČSNS	kA	ČSNS
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
prosadila	prosadit	k5eAaPmAgFnS	prosadit
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
usnesení	usnesení	k1gNnSc4	usnesení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
RČŽ	RČŽ	kA	RČŽ
nevstupovala	vstupovat	k5eNaImAgFnS	vstupovat
do	do	k7c2	do
Ústředního	ústřední	k2eAgInSc2d1	ústřední
akčního	akční	k2eAgInSc2d1	akční
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
však	však	k9	však
byla	být	k5eAaImAgFnS	být
Horáková	Horáková	k1gFnSc1	Horáková
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc4	vedení
převzala	převzít	k5eAaPmAgFnS	převzít
komunistka	komunistka	k1gFnSc1	komunistka
Anežka	Anežka	k1gFnSc1	Anežka
Hodinová-Spurná	Hodinová-Spurný	k2eAgFnSc1d1	Hodinová-Spurná
a	a	k8xC	a
RČŽ	RČŽ	kA	RČŽ
do	do	k7c2	do
akčního	akční	k2eAgInSc2d1	akční
výboru	výbor	k1gInSc2	výbor
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
vyloučena	vyloučen	k2eAgFnSc1d1	vyloučena
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
veřejných	veřejný	k2eAgFnPc2d1	veřejná
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
proklamativně	proklamativně	k6eAd1	proklamativně
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc6	který
nebyla	být	k5eNaImAgFnS	být
členkou	členka	k1gFnSc7	členka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
boji	boj	k1gInSc6	boj
s	s	k7c7	s
finálním	finální	k2eAgInSc7d1	finální
nástupem	nástup	k1gInSc7	nástup
komunismu	komunismus	k1gInSc2	komunismus
se	s	k7c7	s
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
demonstrativně	demonstrativně	k6eAd1	demonstrativně
vzdát	vzdát	k5eAaPmF	vzdát
svého	svůj	k3xOyFgInSc2	svůj
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
mandátu	mandát	k1gInSc2	mandát
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gNnPc3	jejich
jednání	jednání	k1gNnSc3	jednání
<g/>
,	,	kIx,	,
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
právě	právě	k6eAd1	právě
v	v	k7c4	v
den	den	k1gInSc4	den
smrti	smrt	k1gFnSc2	smrt
Jana	Jan	k1gMnSc2	Jan
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
opět	opět	k6eAd1	opět
na	na	k7c4	na
pražský	pražský	k2eAgInSc4d1	pražský
magistrát	magistrát	k1gInSc4	magistrát
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
jako	jako	k9	jako
sociální	sociální	k2eAgFnSc1d1	sociální
referentka	referentka	k1gFnSc1	referentka
Ústředního	ústřední	k2eAgInSc2d1	ústřední
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
měla	mít	k5eAaImAgFnS	mít
možnost	možnost	k1gFnSc4	možnost
opustit	opustit	k5eAaPmF	opustit
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
vlasti	vlast	k1gFnSc2	vlast
oddalovala	oddalovat	k5eAaImAgFnS	oddalovat
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
politicky	politicky	k6eAd1	politicky
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1948	[number]	k4	1948
založila	založit	k5eAaPmAgFnS	založit
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
poslancem	poslanec	k1gMnSc7	poslanec
Josefem	Josef	k1gMnSc7	Josef
Nestávalem	Nestával	k1gMnSc7	Nestával
neformální	formální	k2eNgFnSc4d1	neformální
skupinu	skupina	k1gFnSc4	skupina
okolo	okolo	k7c2	okolo
ČSNS	ČSNS	kA	ČSNS
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
udržovala	udržovat	k5eAaImAgFnS	udržovat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
exilovými	exilový	k2eAgMnPc7d1	exilový
politiky	politik	k1gMnPc7	politik
<g/>
,	,	kIx,	,
Petrem	Petr	k1gMnSc7	Petr
Zenklem	Zenkl	k1gInSc7	Zenkl
a	a	k8xC	a
Hubertem	Hubert	k1gMnSc7	Hubert
Ripkou	Ripka	k1gMnSc7	Ripka
<g/>
.	.	kIx.	.
</s>
<s>
Podporovala	podporovat	k5eAaImAgFnS	podporovat
lidi	člověk	k1gMnPc4	člověk
usilující	usilující	k2eAgFnSc1d1	usilující
o	o	k7c6	o
emigraci	emigrace	k1gFnSc6	emigrace
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
zasazovala	zasazovat	k5eAaImAgFnS	zasazovat
proti	proti	k7c3	proti
komunistům	komunista	k1gMnPc3	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Pomalu	pomalu	k6eAd1	pomalu
tak	tak	k6eAd1	tak
vznikal	vznikat	k5eAaImAgInS	vznikat
třetí	třetí	k4xOgInSc4	třetí
protikomunistický	protikomunistický	k2eAgInSc4d1	protikomunistický
odboj	odboj	k1gInSc4	odboj
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
svou	svůj	k3xOyFgFnSc7	svůj
činností	činnost	k1gFnSc7	činnost
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nedostal	dostat	k5eNaPmAgMnS	dostat
na	na	k7c4	na
hranu	hrana	k1gFnSc4	hrana
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
za	za	k7c4	za
hranu	hrana	k1gFnSc4	hrana
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Horáková	Horáková	k1gFnSc1	Horáková
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
hledáčku	hledáček	k1gInSc2	hledáček
jako	jako	k8xS	jako
vhodný	vhodný	k2eAgMnSc1d1	vhodný
kandidát	kandidát	k1gMnSc1	kandidát
pro	pro	k7c4	pro
akci	akce	k1gFnSc4	akce
StB	StB	k1gFnSc2	StB
"	"	kIx"	"
<g/>
Střed	střed	k1gInSc1	střed
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
níž	nízce	k6eAd2	nízce
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1949	[number]	k4	1949
zatčena	zatčen	k2eAgFnSc1d1	zatčena
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
předvedení	předvedení	k1gNnSc4	předvedení
do	do	k7c2	do
vazby	vazba	k1gFnSc2	vazba
se	se	k3xPyFc4	se
doma	doma	k6eAd1	doma
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
nezdařil	zdařit	k5eNaPmAgMnS	zdařit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
služebná	služebná	k1gFnSc1	služebná
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
a	a	k8xC	a
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c4	v
nestřeženou	střežený	k2eNgFnSc4d1	nestřežená
chvíli	chvíle	k1gFnSc4	chvíle
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Varovat	varovat	k5eAaImF	varovat
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
zatčena	zatknout	k5eAaPmNgFnS	zatknout
o	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
dříve	dříve	k6eAd2	dříve
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kanceláři	kancelář	k1gFnSc6	kancelář
v	v	k7c6	v
Masné	masný	k2eAgFnSc6d1	Masná
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Proces	proces	k1gInSc1	proces
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
.	.	kIx.	.
</s>
<s>
StB	StB	k?	StB
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
nechvalně	chvalně	k6eNd1	chvalně
proslula	proslout	k5eAaPmAgFnS	proslout
svými	svůj	k3xOyFgFnPc7	svůj
brutálními	brutální	k2eAgFnPc7d1	brutální
metodami	metoda	k1gFnPc7	metoda
výslechů	výslech	k1gInPc2	výslech
a	a	k8xC	a
vynucených	vynucený	k2eAgNnPc2d1	vynucené
přiznání	přiznání	k1gNnPc2	přiznání
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
sovětských	sovětský	k2eAgMnPc2d1	sovětský
poradců	poradce	k1gMnPc2	poradce
užívala	užívat	k5eAaImAgFnS	užívat
StB	StB	k1gFnSc7	StB
jak	jak	k6eAd1	jak
fyzického	fyzický	k2eAgNnSc2d1	fyzické
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
bití	bití	k1gNnSc4	bití
<g/>
,	,	kIx,	,
nedostatku	nedostatek	k1gInSc3	nedostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
nuceného	nucený	k2eAgNnSc2d1	nucené
bdění	bdění	k1gNnSc2	bdění
<g/>
,	,	kIx,	,
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
stísněných	stísněný	k2eAgFnPc2d1	stísněná
místností	místnost	k1gFnPc2	místnost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
narkotik	narkotik	k1gMnSc1	narkotik
(	(	kIx(	(
<g/>
archivně	archivně	k6eAd1	archivně
doložená	doložená	k1gFnSc1	doložená
u	u	k7c2	u
Anastáze	Anastáze	k1gFnSc2	Anastáze
Opaska	Opask	k1gInSc2	Opask
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
psychického	psychický	k2eAgNnSc2d1	psychické
týrání	týrání	k1gNnSc2	týrání
a	a	k8xC	a
vydírání	vydírání	k1gNnSc2	vydírání
vedoucího	vedoucí	k1gMnSc2	vedoucí
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
racionální	racionální	k2eAgFnSc2d1	racionální
sebekontroly	sebekontrola	k1gFnSc2	sebekontrola
a	a	k8xC	a
duševní	duševní	k2eAgFnSc4d1	duševní
dezorientaci	dezorientace	k1gFnSc4	dezorientace
<g/>
.	.	kIx.	.
</s>
<s>
Týrání	týrání	k1gNnSc1	týrání
Horákové	Horáková	k1gFnSc2	Horáková
dosvědčuje	dosvědčovat	k5eAaImIp3nS	dosvědčovat
její	její	k3xOp3gFnSc1	její
spoluvězeňkyně	spoluvězeňkyně	k1gFnSc1	spoluvězeňkyně
z	z	k7c2	z
pankrácké	pankrácký	k2eAgFnSc2d1	Pankrácká
věznice	věznice	k1gFnSc2	věznice
Zdena	Zdena	k1gFnSc1	Zdena
Mašínová	Mašínová	k1gFnSc1	Mašínová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zatčení	zatčení	k1gNnSc6	zatčení
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Kopeckého	Kopecký	k1gMnSc2	Kopecký
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1949	[number]	k4	1949
si	se	k3xPyFc3	se
StB	StB	k1gFnSc1	StB
nakonec	nakonec	k6eAd1	nakonec
zvolila	zvolit	k5eAaPmAgFnS	zvolit
Horákovou	Horáková	k1gFnSc4	Horáková
do	do	k7c2	do
role	role	k1gFnSc2	role
ústřední	ústřední	k2eAgFnSc2d1	ústřední
postavy	postava	k1gFnSc2	postava
v	v	k7c6	v
inscenovaném	inscenovaný	k2eAgNnSc6d1	inscenované
spiknutí	spiknutí	k1gNnSc6	spiknutí
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
smyšlené	smyšlený	k2eAgFnSc2d1	smyšlená
ilegální	ilegální	k2eAgFnSc2d1	ilegální
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
StB	StB	k1gFnSc1	StB
nazvala	nazvat	k5eAaBmAgFnS	nazvat
"	"	kIx"	"
<g/>
Direktoria	direktorium	k1gNnPc4	direktorium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pětičlenné	pětičlenný	k2eAgNnSc4d1	pětičlenné
"	"	kIx"	"
<g/>
direktorium	direktorium	k1gNnSc4	direktorium
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
vláda	vláda	k1gFnSc1	vláda
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
československými	československý	k2eAgMnPc7d1	československý
legionáři	legionář	k1gMnPc7	legionář
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
Kolčak	Kolčak	k1gMnSc1	Kolčak
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
vlády	vláda	k1gFnPc4	vláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1949	[number]	k4	1949
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
380	[number]	k4	380
bývalých	bývalý	k2eAgMnPc2d1	bývalý
funkcionářů	funkcionář	k1gMnPc2	funkcionář
ČSNS	ČSNS	kA	ČSNS
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1949	[number]	k4	1949
byly	být	k5eAaImAgFnP	být
vykonány	vykonat	k5eAaPmNgInP	vykonat
rovněž	rovněž	k9	rovněž
tresty	trest	k1gInPc4	trest
smrti	smrt	k1gFnSc2	smrt
nad	nad	k7c7	nad
představiteli	představitel	k1gMnPc7	představitel
skupin	skupina	k1gFnPc2	skupina
nestraníků	nestraník	k1gMnPc2	nestraník
Vratislavem	Vratislav	k1gMnSc7	Vratislav
Polesným	polesný	k1gMnSc7	polesný
<g/>
,	,	kIx,	,
Vratislavem	Vratislav	k1gMnSc7	Vratislav
Jandou	Janda	k1gMnSc7	Janda
<g/>
,	,	kIx,	,
Josefem	Josef	k1gMnSc7	Josef
Charvátem	Charvát	k1gMnSc7	Charvát
<g/>
,	,	kIx,	,
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Čančíkem	Čančík	k1gMnSc7	Čančík
<g/>
,	,	kIx,	,
Květoslavem	Květoslav	k1gMnSc7	Květoslav
Prokešem	Prokeš	k1gMnSc7	Prokeš
a	a	k8xC	a
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Borkovcem	Borkovec	k1gMnSc7	Borkovec
<g/>
,	,	kIx,	,
odsouzenými	odsouzený	k2eAgInPc7d1	odsouzený
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
údajného	údajný	k2eAgNnSc2d1	údajné
květnového	květnový	k2eAgNnSc2d1	květnové
protikomunistického	protikomunistický	k2eAgNnSc2d1	protikomunistické
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
popravy	poprava	k1gFnPc1	poprava
byly	být	k5eAaImAgFnP	být
první	první	k4xOgFnSc7	první
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
justiční	justiční	k2eAgFnSc7d1	justiční
vraždou	vražda	k1gFnSc7	vražda
nového	nový	k2eAgInSc2d1	nový
totalitního	totalitní	k2eAgInSc2d1	totalitní
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
předznamenaly	předznamenat	k5eAaPmAgFnP	předznamenat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bude	být	k5eAaImBp3nS	být
nastávající	nastávající	k2eAgInSc1d1	nastávající
proces	proces	k1gInSc1	proces
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
paní	paní	k1gFnSc2	paní
doktorky	doktorka	k1gFnSc2	doktorka
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
vykonstruovaný	vykonstruovaný	k2eAgInSc1d1	vykonstruovaný
"	"	kIx"	"
<g/>
monstrproces	monstrproces	k1gInSc1	monstrproces
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
velký	velký	k2eAgInSc4d1	velký
politický	politický	k2eAgInSc4d1	politický
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
přípravě	příprava	k1gFnSc6	příprava
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
významně	významně	k6eAd1	významně
podíleli	podílet	k5eAaImAgMnP	podílet
sovětští	sovětský	k2eAgMnPc1d1	sovětský
poradci	poradce	k1gMnPc1	poradce
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
poradci	poradce	k1gMnPc1	poradce
Lichačev	Lichačev	k1gFnPc2	Lichačev
a	a	k8xC	a
Makarov	Makarovo	k1gNnPc2	Makarovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
jejich	jejich	k3xOp3gFnPc1	jejich
metody	metoda	k1gFnPc1	metoda
výslechu	výslech	k1gInSc2	výslech
a	a	k8xC	a
předem	předem	k6eAd1	předem
naučených	naučený	k2eAgInPc2d1	naučený
"	"	kIx"	"
<g/>
scénářů	scénář	k1gInPc2	scénář
<g/>
"	"	kIx"	"
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Doktorku	doktorka	k1gFnSc4	doktorka
Horákovou	Horáková	k1gFnSc4	Horáková
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
předtím	předtím	k6eAd1	předtím
gestapu	gestapo	k1gNnSc3	gestapo
<g/>
,	,	kIx,	,
zlomit	zlomit	k5eAaPmF	zlomit
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
s	s	k7c7	s
Miladou	Milada	k1gFnSc7	Milada
Horákovou	Horákův	k2eAgFnSc7d1	Horákova
a	a	k8xC	a
s	s	k7c7	s
jejími	její	k3xOp3gMnPc7	její
dvanácti	dvanáct	k4xCc7	dvanáct
kolegy	kolega	k1gMnPc4	kolega
<g/>
,	,	kIx,	,
řízený	řízený	k2eAgInSc1d1	řízený
JUDr.	JUDr.	kA	JUDr.
Karlem	Karel	k1gMnSc7	Karel
Trudákem	Trudák	k1gMnSc7	Trudák
<g/>
,	,	kIx,	,
probíhal	probíhat	k5eAaImAgInS	probíhat
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zinscenován	zinscenován	k2eAgInSc4d1	zinscenován
jako	jako	k8xS	jako
veřejný	veřejný	k2eAgInSc4d1	veřejný
"	"	kIx"	"
<g/>
politický	politický	k2eAgInSc4d1	politický
proces	proces	k1gInSc4	proces
<g/>
"	"	kIx"	"
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
sovětských	sovětský	k2eAgFnPc2d1	sovětská
velkých	velký	k2eAgFnPc2d1	velká
čistek	čistka	k1gFnPc2	čistka
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
příkaz	příkaz	k1gInSc4	příkaz
prezidenta	prezident	k1gMnSc2	prezident
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
trvající	trvající	k2eAgInSc1d1	trvající
proces	proces	k1gInSc1	proces
měl	mít	k5eAaImAgInS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vynucený	vynucený	k2eAgInSc4d1	vynucený
"	"	kIx"	"
<g/>
scénář	scénář	k1gInSc4	scénář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
donuceni	donucen	k2eAgMnPc1d1	donucen
obžalovaní	obžalovaný	k1gMnPc1	obžalovaný
chovat	chovat	k5eAaImF	chovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
momentech	moment	k1gInPc6	moment
přesto	přesto	k6eAd1	přesto
jednali	jednat	k5eAaImAgMnP	jednat
proti	proti	k7c3	proti
režii	režie	k1gFnSc3	režie
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
doktorka	doktorka	k1gFnSc1	doktorka
Horáková	Horáková	k1gFnSc1	Horáková
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyslechnuta	vyslechnut	k2eAgFnSc1d1	vyslechnuta
1	[number]	k4	1
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nepoddala	poddat	k5eNaPmAgFnS	poddat
a	a	k8xC	a
přes	přes	k7c4	přes
vynucenou	vynucený	k2eAgFnSc4d1	vynucená
režii	režie	k1gFnSc4	režie
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
místy	místy	k6eAd1	místy
podařilo	podařit	k5eAaPmAgNnS	podařit
bránit	bránit	k5eAaImF	bránit
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
ideály	ideál	k1gInPc4	ideál
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
svoje	svůj	k3xOyFgInPc4	svůj
šance	šance	k1gFnPc1	šance
na	na	k7c4	na
mírnější	mírný	k2eAgInSc4d2	mírnější
trest	trest	k1gInSc4	trest
jen	jen	k9	jen
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
procesu	proces	k1gInSc2	proces
byly	být	k5eAaImAgFnP	být
nošeny	nosit	k5eAaImNgFnP	nosit
do	do	k7c2	do
soudní	soudní	k2eAgFnSc2d1	soudní
síně	síň	k1gFnSc2	síň
koše	koš	k1gInSc2	koš
s	s	k7c7	s
tisíci	tisíc	k4xCgInPc7	tisíc
rezolucemi	rezoluce	k1gFnPc7	rezoluce
lidí	člověk	k1gMnPc2	člověk
žádajících	žádající	k2eAgFnPc2d1	žádající
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
tresty	trest	k1gInPc4	trest
pro	pro	k7c4	pro
obviněné	obviněný	k1gMnPc4	obviněný
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
organizovány	organizovat	k5eAaBmNgInP	organizovat
nejen	nejen	k6eAd1	nejen
závodními	závodní	k2eAgInPc7d1	závodní
výbory	výbor	k1gInPc7	výbor
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
milicemi	milice	k1gFnPc7	milice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
uličními	uliční	k2eAgInPc7d1	uliční
výbory	výbor	k1gInPc7	výbor
(	(	kIx(	(
<g/>
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
stála	stát	k5eAaImAgFnS	stát
tajná	tajný	k2eAgFnSc1d1	tajná
státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
a	a	k8xC	a
sovětští	sovětský	k2eAgMnPc1d1	sovětský
poradci	poradce	k1gMnPc1	poradce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejní	obyčejný	k2eAgMnPc1d1	obyčejný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
pamatující	pamatující	k2eAgFnSc4d1	pamatující
Masarykovu	Masarykův	k2eAgFnSc4d1	Masarykova
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
paní	paní	k1gFnSc1	paní
Milada	Milada	k1gFnSc1	Milada
otevřeně	otevřeně	k6eAd1	otevřeně
během	během	k7c2	během
procesu	proces	k1gInSc2	proces
hlásila	hlásit	k5eAaImAgFnS	hlásit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
uvrženi	uvrhnout	k5eAaPmNgMnP	uvrhnout
již	již	k9	již
samotným	samotný	k2eAgInSc7d1	samotný
procesem	proces	k1gInSc7	proces
do	do	k7c2	do
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
o	o	k7c4	o
svoje	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mladou	mladý	k2eAgFnSc4d1	mladá
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Osmý	osmý	k4xOgInSc1	osmý
den	den	k1gInSc1	den
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
po	po	k7c6	po
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
řeči	řeč	k1gFnSc6	řeč
státních	státní	k2eAgMnPc2d1	státní
zástupců	zástupce	k1gMnPc2	zástupce
Juraje	Juraj	k1gInSc2	Juraj
Viesky	Vieska	k1gFnSc2	Vieska
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
Urválka	Urválek	k1gMnSc2	Urválek
a	a	k8xC	a
Ludmily	Ludmila	k1gFnSc2	Ludmila
Brožové-Polednové	Brožové-Polednová	k1gFnSc2	Brožové-Polednová
(	(	kIx(	(
<g/>
se	s	k7c7	s
známým	známý	k2eAgInSc7d1	známý
obratem	obrat	k1gInSc7	obrat
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
taková	takový	k3xDgFnSc1	takový
Horáková	Horáková	k1gFnSc1	Horáková
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
poslední	poslední	k2eAgFnSc2d1	poslední
řeči	řeč	k1gFnSc2	řeč
všech	všecek	k3xTgFnPc2	všecek
odsouzených	odsouzená	k1gFnPc2	odsouzená
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
mediím	medium	k1gNnPc3	medium
cenzurována	cenzurovat	k5eAaImNgNnP	cenzurovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Horáková	Horáková	k1gFnSc1	Horáková
statečně	statečně	k6eAd1	statečně
staví	stavit	k5eAaImIp3nS	stavit
za	za	k7c4	za
svoje	svůj	k3xOyFgInPc4	svůj
ideály	ideál	k1gInPc4	ideál
<g/>
,	,	kIx,	,
za	za	k7c2	za
ideje	idea	k1gFnSc2	idea
Beneše	Beneš	k1gMnSc2	Beneš
a	a	k8xC	a
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byly	být	k5eAaImAgFnP	být
vyneseny	vynesen	k2eAgInPc4d1	vynesen
rozsudky	rozsudek	k1gInPc4	rozsudek
<g/>
:	:	kIx,	:
čtyři	čtyři	k4xCgInPc4	čtyři
tresty	trest	k1gInPc4	trest
smrti	smrt	k1gFnSc2	smrt
oběšením	oběšení	k1gNnSc7	oběšení
včetně	včetně	k7c2	včetně
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
tresty	trest	k1gInPc4	trest
doživotního	doživotní	k2eAgNnSc2d1	doživotní
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
pět	pět	k4xCc4	pět
trestů	trest	k1gInPc2	trest
od	od	k7c2	od
dvaceti	dvacet	k4xCc2	dvacet
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
v	v	k7c6	v
časných	časný	k2eAgFnPc6d1	časná
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Pankráci	Pankrác	k1gFnSc6	Pankrác
vykonána	vykonán	k2eAgFnSc1d1	vykonána
poprava	poprava	k1gFnSc1	poprava
studenta	student	k1gMnSc2	student
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
a	a	k8xC	a
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
Veleslava	Veleslava	k1gFnSc1	Veleslava
Wahla	Wahl	k1gMnSc2	Wahl
za	za	k7c4	za
propagaci	propagace	k1gFnSc4	propagace
myšlenek	myšlenka	k1gFnPc2	myšlenka
Masarykových	Masaryková	k1gFnPc2	Masaryková
(	(	kIx(	(
<g/>
rozvracení	rozvracení	k1gNnSc2	rozvracení
"	"	kIx"	"
<g/>
lidově	lidově	k6eAd1	lidově
demokratické	demokratický	k2eAgFnSc2d1	demokratická
<g/>
"	"	kIx"	"
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
před	před	k7c7	před
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Miladou	Milada	k1gFnSc7	Milada
Horákovou	Horáková	k1gFnSc7	Horáková
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
i	i	k8xC	i
advokát	advokát	k1gMnSc1	advokát
tak	tak	k6eAd1	tak
učinili	učinit	k5eAaPmAgMnP	učinit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
mnohé	mnohý	k2eAgFnPc1d1	mnohá
významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gMnSc1	Churchill
nebo	nebo	k8xC	nebo
Eleanor	Eleanor	k1gMnSc1	Eleanor
Rooseveltová	Rooseveltová	k1gFnSc1	Rooseveltová
<g/>
,	,	kIx,	,
snažili	snažit	k5eAaImAgMnP	snažit
svými	svůj	k3xOyFgMnPc7	svůj
dopisy	dopis	k1gInPc7	dopis
získat	získat	k5eAaPmF	získat
pro	pro	k7c4	pro
odsouzené	odsouzený	k2eAgNnSc4d1	odsouzené
milost	milost	k1gFnSc1	milost
u	u	k7c2	u
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
ministra	ministr	k1gMnSc2	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
rozsudky	rozsudek	k1gInPc4	rozsudek
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
podepsal	podepsat	k5eAaPmAgInS	podepsat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc4d1	určený
především	především	k9	především
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaPmAgFnS	napsat
v	v	k7c4	v
den	den	k1gInSc4	den
popravy	poprava	k1gFnSc2	poprava
ve	v	k7c6	v
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
:	:	kIx,	:
Poprava	poprava	k1gFnSc1	poprava
Horákové	Horáková	k1gFnSc2	Horáková
byla	být	k5eAaImAgFnS	být
vykonána	vykonat	k5eAaPmNgFnS	vykonat
oběšením	oběšení	k1gNnSc7	oběšení
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
pankrácké	pankrácký	k2eAgFnSc2d1	Pankrácká
věznice	věznice	k1gFnSc2	věznice
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1950	[number]	k4	1950
v	v	k7c4	v
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
poslední	poslední	k2eAgMnPc1d1	poslední
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
čtyř	čtyři	k4xCgMnPc2	čtyři
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
odsouzených	odsouzený	k1gMnPc2	odsouzený
(	(	kIx(	(
<g/>
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
byli	být	k5eAaImAgMnP	být
oběšeni	oběsit	k5eAaPmNgMnP	oběsit
Jan	Jan	k1gMnSc1	Jan
Buchal	buchat	k5eAaImAgMnS	buchat
<g/>
,	,	kIx,	,
Záviš	Záviš	k1gMnSc1	Záviš
Kalandra	Kalandra	k1gFnSc1	Kalandra
a	a	k8xC	a
Oldřich	Oldřich	k1gMnSc1	Oldřich
Pecl	Pecl	k1gMnSc1	Pecl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Pankráci	Pankrác	k1gFnSc6	Pankrác
byla	být	k5eAaImAgFnS	být
používána	používán	k2eAgFnSc1d1	používána
primitivní	primitivní	k2eAgFnSc1d1	primitivní
forma	forma	k1gFnSc1	forma
oběšení	oběšení	k1gNnSc2	oběšení
dlouho	dlouho	k6eAd1	dlouho
trvajícím	trvající	k2eAgNnSc7d1	trvající
škrcením	škrcení	k1gNnSc7	škrcení
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
zpopelněny	zpopelnit	k5eAaPmNgInP	zpopelnit
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
u	u	k7c2	u
Heliodora	Heliodor	k1gMnSc2	Heliodor
Píky	píka	k1gFnSc2	píka
nepohřbeny	pohřben	k2eNgMnPc4d1	pohřben
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nebyly	být	k5eNaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kremaci	kremace	k1gFnSc6	kremace
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
pankrácké	pankrácký	k2eAgFnSc2d1	Pankrácká
věznice	věznice	k1gFnSc2	věznice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
komunistická	komunistický	k2eAgFnSc1d1	komunistická
moc	moc	k1gFnSc1	moc
schovávala	schovávat	k5eAaImAgFnS	schovávat
před	před	k7c7	před
veřejností	veřejnost	k1gFnSc7	veřejnost
ostatky	ostatek	k1gInPc7	ostatek
stovek	stovka	k1gFnPc2	stovka
odpůrců	odpůrce	k1gMnPc2	odpůrce
režimu	režim	k1gInSc2	režim
-	-	kIx~	-
patrně	patrně	k6eAd1	patrně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pohřeb	pohřeb	k1gInSc1	pohřeb
nemohl	moct	k5eNaImAgInS	moct
změnit	změnit	k5eAaPmF	změnit
v	v	k7c6	v
protirežimní	protirežimní	k2eAgFnSc6d1	protirežimní
manifestaci	manifestace	k1gFnSc6	manifestace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
uren	urna	k1gFnPc2	urna
ve	v	k7c6	v
skladu	sklad	k1gInSc6	sklad
příliš	příliš	k6eAd1	příliš
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
rozvezeny	rozvézt	k5eAaPmNgFnP	rozvézt
po	po	k7c6	po
menších	malý	k2eAgInPc6d2	menší
počtech	počet	k1gInPc6	počet
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
hřbitovy	hřbitov	k1gInPc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
urny	urna	k1gFnPc1	urna
byly	být	k5eAaImAgFnP	být
úmyslně	úmyslně	k6eAd1	úmyslně
zničeny	zničen	k2eAgInPc1d1	zničen
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Václav	Václav	k1gMnSc1	Václav
Švéda	Švéda	k1gMnSc1	Švéda
<g/>
,	,	kIx,	,
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Janata	Janata	k1gMnSc1	Janata
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
bratří	bratr	k1gMnPc2	bratr
Mašínů	Mašín	k1gInPc2	Mašín
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
"	"	kIx"	"
<g/>
jejich	jejich	k3xOp3gMnPc1	jejich
spolupachatelé	spolupachatel	k1gMnPc1	spolupachatel
svobodně	svobodně	k6eAd1	svobodně
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
USA	USA	kA	USA
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Justiční	justiční	k2eAgFnSc1d1	justiční
vražda	vražda	k1gFnSc1	vražda
provedená	provedený	k2eAgFnSc1d1	provedená
na	na	k7c6	na
Miladě	Milada	k1gFnSc6	Milada
Horákové	Horáková	k1gFnSc2	Horáková
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
velký	velký	k2eAgInSc4d1	velký
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
ohlas	ohlas	k1gInSc4	ohlas
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Manžel	manžel	k1gMnSc1	manžel
Ing.	ing.	kA	ing.
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Horák	Horák	k1gMnSc1	Horák
se	se	k3xPyFc4	se
po	po	k7c6	po
úniku	únik	k1gInSc6	únik
ze	z	k7c2	z
zatčení	zatčení	k1gNnSc2	zatčení
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
skrýval	skrývat	k5eAaImAgInS	skrývat
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
své	svůj	k3xOyFgFnSc2	svůj
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
žít	žít	k5eAaImF	žít
v	v	k7c6	v
ilegalitě	ilegalita	k1gFnSc6	ilegalita
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
rizikové	rizikový	k2eAgInPc1d1	rizikový
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1949	[number]	k4	1949
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
uniknout	uniknout	k5eAaPmF	uniknout
do	do	k7c2	do
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Valka	Valek	k1gMnSc2	Valek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
získal	získat	k5eAaPmAgInS	získat
povolení	povolení	k1gNnSc4	povolení
se	se	k3xPyFc4	se
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
komplikaci	komplikace	k1gFnSc4	komplikace
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
před	před	k7c7	před
emigrací	emigrace	k1gFnSc7	emigrace
podal	podat	k5eAaPmAgMnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
provdaná	provdaný	k2eAgNnPc1d1	provdané
Kánská	Kánské	k1gNnPc1	Kánské
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgNnP	být
vychována	vychovat	k5eAaPmNgNnP	vychovat
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
matky	matka	k1gFnSc2	matka
u	u	k7c2	u
sestry	sestra	k1gFnSc2	sestra
Věry	Věra	k1gFnSc2	Věra
<g/>
,	,	kIx,	,
příjmením	příjmení	k1gNnSc7	příjmení
Králové-Tůmové	Králové-Tůmová	k1gFnSc2	Králové-Tůmová
<g/>
.	.	kIx.	.
</s>
<s>
Nemohla	moct	k5eNaImAgFnS	moct
však	však	k9	však
studovat	studovat	k5eAaImF	studovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
ovšem	ovšem	k9	ovšem
dostala	dostat	k5eAaPmAgFnS	dostat
vystěhovalecké	vystěhovalecký	k2eAgNnSc4d1	vystěhovalecký
vízum	vízum	k1gNnSc4	vízum
a	a	k8xC	a
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
za	za	k7c7	za
otcem	otec	k1gMnSc7	otec
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Sestra	sestra	k1gFnSc1	sestra
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milady	Milada	k1gFnSc2	Milada
Horákové	Horákové	k2eAgFnSc1d1	Horákové
Věra	Věra	k1gFnSc1	Věra
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
aktivně	aktivně	k6eAd1	aktivně
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
činnosti	činnost	k1gFnSc6	činnost
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
demokratického	demokratický	k2eAgNnSc2d1	demokratické
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
od	od	k7c2	od
jehož	jenž	k3xRgNnSc2	jenž
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
převzala	převzít	k5eAaPmAgFnS	převzít
pro	pro	k7c4	pro
paní	paní	k1gFnSc4	paní
Miladu	Milada	k1gFnSc4	Milada
Čestnou	čestný	k2eAgFnSc4d1	čestná
medaili	medaile	k1gFnSc4	medaile
TGM	TGM	kA	TGM
za	za	k7c4	za
věrnost	věrnost	k1gFnSc4	věrnost
jeho	on	k3xPp3gInSc2	on
odkazu	odkaz	k1gInSc2	odkaz
až	až	k9	až
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
myšlenkou	myšlenka	k1gFnSc7	myšlenka
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc4	Masaryk
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
Edvarda	Edvard	k1gMnSc4	Edvard
Beneše	Beneš	k1gMnSc4	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
idejí	idea	k1gFnPc2	idea
plynul	plynout	k5eAaImAgInS	plynout
její	její	k3xOp3gInSc1	její
nekompromisní	kompromisní	k2eNgInSc1d1	nekompromisní
boj	boj	k1gInSc1	boj
za	za	k7c4	za
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
totalitám	totalita	k1gFnPc3	totalita
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgInPc1d1	královský
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Praha-Vinohrady	Praha-Vinohrada	k1gFnPc4	Praha-Vinohrada
<g/>
)	)	kIx)	)
-	-	kIx~	-
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
Praha-Smíchov	Praha-Smíchov	k1gInSc4	Praha-Smíchov
<g/>
,	,	kIx,	,
Zapova	Zapův	k2eAgFnSc1d1	Zapova
ul	ul	kA	ul
<g/>
.	.	kIx.	.
376	[number]	k4	376
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
-	-	kIx~	-
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
Malá	malý	k2eAgFnSc1d1	malá
pevnost	pevnost	k1gFnSc1	pevnost
(	(	kIx(	(
<g/>
Terezín	Terezín	k1gInSc1	Terezín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
<g/>
,	,	kIx,	,
Drážďany	Drážďany	k1gInPc1	Drážďany
<g/>
,	,	kIx,	,
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Praha-Staré	Praha-Starý	k2eAgNnSc1d1	Praha-Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Masná	masný	k2eAgFnSc1d1	Masná
-	-	kIx~	-
její	její	k3xOp3gFnSc1	její
kancelář	kancelář	k1gFnSc1	kancelář
Věznice	věznice	k1gFnSc2	věznice
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
Věznice	věznice	k1gFnSc2	věznice
Pankrác	Pankrác	k1gMnSc1	Pankrác
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1968	[number]	k4	1968
nezákonný	zákonný	k2eNgInSc1d1	nezákonný
rozsudek	rozsudek	k1gInSc1	rozsudek
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
,	,	kIx,	,
plná	plný	k2eAgFnSc1d1	plná
rehabilitace	rehabilitace	k1gFnSc1	rehabilitace
však	však	k9	však
mohla	moct	k5eAaImAgFnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
žijící	žijící	k2eAgFnSc1d1	žijící
prokurátorka	prokurátorka	k1gFnSc1	prokurátorka
procesu	proces	k1gInSc2	proces
Ludmila	Ludmila	k1gFnSc1	Ludmila
Brožová-Polednová	Brožová-Polednová	k1gFnSc1	Brožová-Polednová
vrchním	vrchní	k2eAgMnSc7d1	vrchní
soudem	soud	k1gInSc7	soud
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
k	k	k7c3	k
6	[number]	k4	6
letům	let	k1gInPc3	let
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
nepodmíněně	podmíněně	k6eNd1	podmíněně
za	za	k7c4	za
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
justiční	justiční	k2eAgFnSc6d1	justiční
vraždě	vražda	k1gFnSc6	vražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
udělil	udělit	k5eAaPmAgMnS	udělit
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
Brožové	Brožové	k2eAgMnSc1d1	Brožové
-	-	kIx~	-
Polednové	polednový	k2eAgFnPc1d1	Polednová
milost	milost	k1gFnSc1	milost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
udělení	udělení	k1gNnSc1	udělení
bylo	být	k5eAaImAgNnS	být
vysvětleno	vysvětlit	k5eAaPmNgNnS	vysvětlit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
"	"	kIx"	"
<g/>
přihlédl	přihlédnout	k5eAaPmAgMnS	přihlédnout
k	k	k7c3	k
vysokému	vysoký	k2eAgInSc3d1	vysoký
věku	věk	k1gInSc3	věk
odsouzené	odsouzená	k1gFnSc2	odsouzená
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
trestu	trest	k1gInSc2	trest
již	již	k6eAd1	již
vykonala	vykonat	k5eAaPmAgFnS	vykonat
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
udělil	udělit	k5eAaPmAgMnS	udělit
JUDr.	JUDr.	kA	JUDr.
Miladě	Milada	k1gFnSc6	Milada
Horákové	Horákové	k2eAgFnSc6d1	Horákové
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
Řád	řád	k1gInSc1	řád
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigua	Garriguus	k1gMnSc2	Garriguus
Masaryka	Masaryk	k1gMnSc2	Masaryk
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
27	[number]	k4	27
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
popravy	poprava	k1gFnSc2	poprava
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
,	,	kIx,	,
vyhlášen	vyhlášen	k2eAgMnSc1d1	vyhlášen
významným	významný	k2eAgInSc7d1	významný
dnem	den	k1gInSc7	den
jako	jako	k8xS	jako
Den	den	k1gInSc1	den
památky	památka	k1gFnSc2	památka
obětí	oběť	k1gFnPc2	oběť
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
Miladě	Milada	k1gFnSc6	Milada
Horákové	Horáková	k1gFnSc2	Horáková
udělena	udělit	k5eAaPmNgFnS	udělit
americkou	americký	k2eAgFnSc7d1	americká
Nadací	nadace	k1gFnSc7	nadace
pro	pro	k7c4	pro
vybudování	vybudování	k1gNnSc4	vybudování
washingtonského	washingtonský	k2eAgInSc2d1	washingtonský
památníku	památník	k1gInSc2	památník
obětem	oběť	k1gFnPc3	oběť
zločinů	zločin	k1gInPc2	zločin
komunismu	komunismus	k1gInSc3	komunismus
Medaile	medaile	k1gFnSc1	medaile
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
obdarována	obdarovat	k5eAaPmNgFnS	obdarovat
Záslužným	záslužný	k2eAgInSc7d1	záslužný
křížem	kříž	k1gInSc7	kříž
ministryně	ministryně	k1gFnSc2	ministryně
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
armádních	armádní	k2eAgNnPc2d1	armádní
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
vně	vně	k6eAd1	vně
severní	severní	k2eAgFnSc2d1	severní
lodi	loď	k1gFnSc2	loď
chrámu	chrámat	k5eAaImIp1nS	chrámat
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
symbolický	symbolický	k2eAgInSc1d1	symbolický
hrob	hrob	k1gInSc1	hrob
(	(	kIx(	(
<g/>
kenotaf	kenotaf	k1gInSc1	kenotaf
<g/>
)	)	kIx)	)
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
od	od	k7c2	od
akademické	akademický	k2eAgFnSc2d1	akademická
sochařky	sochařka	k1gFnSc2	sochařka
Jaroslavy	Jaroslava	k1gFnSc2	Jaroslava
Lukešové	Lukešová	k1gFnSc2	Lukešová
<g/>
,	,	kIx,	,
modrošedým	modrošedý	k2eAgInSc7d1	modrošedý
náhrobkem	náhrobek	k1gInSc7	náhrobek
a	a	k8xC	a
pamětní	pamětní	k2eAgFnSc7d1	pamětní
deskou	deska	k1gFnSc7	deska
<g/>
,	,	kIx,	,
věnovanou	věnovaný	k2eAgFnSc7d1	věnovaná
všem	všecek	k3xTgNnPc3	všecek
obětem	oběť	k1gFnPc3	oběť
totalitních	totalitní	k2eAgMnPc2d1	totalitní
režimů	režim	k1gInPc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
památník	památník	k1gInSc4	památník
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
sochař	sochař	k1gMnSc1	sochař
Karel	Karel	k1gMnSc1	Karel
Hořínek	Hořínek	k1gMnSc1	Hořínek
<g/>
.	.	kIx.	.
</s>
<s>
Bysta	bysta	k1gFnSc1	bysta
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
bronzová	bronzový	k2eAgFnSc1d1	bronzová
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
odcizení	odcizení	k1gNnSc6	odcizení
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
umístěna	umístit	k5eAaPmNgFnS	umístit
jen	jen	k6eAd1	jen
její	její	k3xOp3gFnSc1	její
kopie	kopie	k1gFnSc1	kopie
z	z	k7c2	z
pozlacené	pozlacený	k2eAgFnSc2d1	pozlacená
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
Nové	Nové	k2eAgFnSc2d1	Nové
radnice	radnice	k1gFnSc2	radnice
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Mariánském	mariánský	k2eAgNnSc6d1	Mariánské
náměstí	náměstí	k1gNnSc6	náměstí
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
od	od	k7c2	od
akademické	akademický	k2eAgFnSc2d1	akademická
sochařky	sochařka	k1gFnSc2	sochařka
Jaroslavy	Jaroslava	k1gFnSc2	Jaroslava
Lukešové	Lukešová	k1gFnSc2	Lukešová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
budově	budova	k1gFnSc6	budova
úřadu	úřad	k1gInSc2	úřad
Městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Prahy	Praha	k1gFnSc2	Praha
7	[number]	k4	7
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
2	[number]	k4	2
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
a	a	k8xC	a
plaketa	plaketa	k1gFnSc1	plaketa
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
Hrdinů	Hrdina	k1gMnPc2	Hrdina
(	(	kIx(	(
<g/>
před	před	k7c7	před
Vrchním	vrchní	k2eAgInSc7d1	vrchní
soudem	soud	k1gInSc7	soud
a	a	k8xC	a
Věznicí	věznice	k1gFnSc7	věznice
Pankrác	Pankrác	k1gMnSc1	Pankrác
<g/>
)	)	kIx)	)
odhalena	odhalen	k2eAgFnSc1d1	odhalena
busta	busta	k1gFnSc1	busta
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
na	na	k7c6	na
památníku	památník	k1gInSc6	památník
obětem	oběť	k1gFnPc3	oběť
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
památník	památník	k1gInSc1	památník
kontroverze	kontroverze	k1gFnSc2	kontroverze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
bustu	busta	k1gFnSc4	busta
přispěla	přispět	k5eAaPmAgFnS	přispět
mj.	mj.	kA	mj.
i	i	k8xC	i
KSČM	KSČM	kA	KSČM
(	(	kIx(	(
<g/>
nejštědřejším	štědrý	k2eAgMnSc7d3	nejštědřejší
sponzorem	sponzor	k1gMnSc7	sponzor
byla	být	k5eAaImAgFnS	být
údajně	údajně	k6eAd1	údajně
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k9	i
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
stavby	stavba	k1gFnSc2	stavba
se	se	k3xPyFc4	se
distancoval	distancovat	k5eAaBmAgInS	distancovat
i	i	k9	i
Klub	klub	k1gInSc1	klub
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
žulového	žulový	k2eAgInSc2d1	žulový
pomníku	pomník	k1gInSc2	pomník
a	a	k8xC	a
bronzové	bronzový	k2eAgFnSc2d1	bronzová
busty	busta	k1gFnSc2	busta
jsou	být	k5eAaImIp3nP	být
architekt	architekt	k1gMnSc1	architekt
Jiří	Jiří	k1gMnSc1	Jiří
Lasovský	Lasovský	k1gMnSc1	Lasovský
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
Milan	Milan	k1gMnSc1	Milan
Knobloch	Knobloch	k1gMnSc1	Knobloch
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
smíchovským	smíchovský	k2eAgInSc7d1	smíchovský
kostelem	kostel	k1gInSc7	kostel
Českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
odhalen	odhalen	k2eAgInSc4d1	odhalen
památník	památník	k1gInSc4	památník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
sochař	sochař	k1gMnSc1	sochař
Olbram	Olbram	k1gInSc4	Olbram
Zoubek	Zoubek	k1gMnSc1	Zoubek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Pětikostelním	pětikostelní	k2eAgNnSc6d1	pětikostelní
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
stojí	stát	k5eAaImIp3nS	stát
řečnický	řečnický	k2eAgInSc4d1	řečnický
pult	pult	k1gInSc4	pult
podobný	podobný	k2eAgInSc4d1	podobný
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
hájila	hájit	k5eAaImAgFnS	hájit
během	během	k7c2	během
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mikrofonu	mikrofon	k1gInSc6	mikrofon
sedí	sedit	k5eAaImIp3nS	sedit
skřivánek	skřivánek	k1gMnSc1	skřivánek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
autora	autor	k1gMnSc2	autor
pomníku	pomník	k1gInSc2	pomník
Josefa	Josef	k1gMnSc2	Josef
Faltuse	Faltuse	k1gFnSc2	Faltuse
symbolicky	symbolicky	k6eAd1	symbolicky
připomínat	připomínat	k5eAaImF	připomínat
samotnou	samotný	k2eAgFnSc4d1	samotná
Horákovou	Horáková	k1gFnSc4	Horáková
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c4	v
něj	on	k3xPp3gMnSc4	on
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
odlétá	odlétat	k5eAaPmIp3nS	odlétat
za	za	k7c7	za
svobodou	svoboda	k1gFnSc7	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
slova	slovo	k1gNnPc4	slovo
napsaná	napsaný	k2eAgNnPc4d1	napsané
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
posledním	poslední	k2eAgInSc6d1	poslední
dopise	dopis	k1gInSc6	dopis
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
přeložená	přeložený	k2eAgFnSc1d1	přeložená
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
světových	světový	k2eAgInPc2d1	světový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
odhalení	odhalení	k1gNnSc3	odhalení
došlo	dojít	k5eAaPmAgNnS	dojít
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
dcery	dcera	k1gFnSc2	dcera
Jany	Jana	k1gFnSc2	Jana
Kánské	Kánská	k1gFnSc2	Kánská
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
doktorky	doktorka	k1gFnSc2	doktorka
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
byla	být	k5eAaImAgFnS	být
jejím	její	k3xOp3gMnSc7	její
jménem	jméno	k1gNnSc7	jméno
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
i	i	k8xC	i
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Na	na	k7c6	na
Planině	planina	k1gFnSc6	planina
13	[number]	k4	13
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
Klub	klub	k1gInSc1	klub
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
i	i	k8xC	i
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
se	s	k7c7	s
symbolickým	symbolický	k2eAgInSc7d1	symbolický
reliéfem	reliéf	k1gInSc7	reliéf
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
vinořské	vinořský	k2eAgFnSc2d1	vinořská
fary	fara	k1gFnSc2	fara
připomíná	připomínat	k5eAaImIp3nS	připomínat
protikomunistický	protikomunistický	k2eAgInSc4d1	protikomunistický
odboj	odboj	k1gInSc4	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
JUDr.	JUDr.	kA	JUDr.
Miladě	Milada	k1gFnSc6	Milada
Horákové	Horákové	k2eAgFnSc6d1	Horákové
<g/>
,	,	kIx,	,
Vojtovi	Vojta	k1gMnSc3	Vojta
Benešovi	Beneš	k1gMnSc3	Beneš
<g/>
,	,	kIx,	,
Vojtěchu	Vojtěch	k1gMnSc3	Vojtěch
Jandečkovi	Jandeček	k1gMnSc3	Jandeček
<g/>
,	,	kIx,	,
Josefu	Josef	k1gMnSc3	Josef
Nestávalovi	Nestával	k1gMnSc3	Nestával
<g/>
,	,	kIx,	,
Zdeňku	Zdeněk	k1gMnSc3	Zdeněk
Peškovi	Pešek	k1gMnSc3	Pešek
a	a	k8xC	a
řádovým	řádový	k2eAgMnPc3d1	řádový
kněžím	kněz	k1gMnPc3	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
farního	farní	k2eAgInSc2d1	farní
úřadu	úřad	k1gInSc2	úřad
ve	v	k7c6	v
Vinoři	Vinoř	k1gFnSc6	Vinoř
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
<g/>
:	:	kIx,	:
Vinořské	Vinořský	k2eAgNnSc1d1	Vinořské
náměstí	náměstí	k1gNnSc1	náměstí
16	[number]	k4	16
<g/>
,	,	kIx,	,
190	[number]	k4	190
17	[number]	k4	17
Praha	Praha	k1gFnSc1	Praha
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
GPS	GPS	kA	GPS
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
°	°	k?	°
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
35,350	[number]	k4	35,350
<g/>
''	''	k?	''
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
°	°	k?	°
<g/>
34	[number]	k4	34
<g/>
'	'	kIx"	'
<g/>
48,080	[number]	k4	48,080
<g/>
''	''	k?	''
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
Výše	vysoce	k6eAd2	vysoce
jmenovaní	jmenovaný	k2eAgMnPc1d1	jmenovaný
bývalí	bývalý	k2eAgMnPc1d1	bývalý
funkcionáři	funkcionář	k1gMnPc1	funkcionář
českých	český	k2eAgFnPc2d1	Česká
nekomunistických	komunistický	k2eNgFnPc2d1	nekomunistická
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
Horáková	Horáková	k1gFnSc1	Horáková
a	a	k8xC	a
Nestával	stávat	k5eNaImAgMnS	stávat
za	za	k7c4	za
národní	národní	k2eAgMnPc4d1	národní
socialisty	socialist	k1gMnPc4	socialist
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byli	být	k5eAaImAgMnP	být
i	i	k8xC	i
členy	člen	k1gMnPc7	člen
národněsocialistické	národněsocialistický	k2eAgFnSc2d1	národněsocialistická
"	"	kIx"	"
<g/>
politické	politický	k2eAgFnSc2d1	politická
šestky	šestka	k1gFnSc2	šestka
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Beneš	Beneš	k1gMnSc1	Beneš
a	a	k8xC	a
Peška	Peška	k1gMnSc1	Peška
za	za	k7c4	za
sociální	sociální	k2eAgMnPc4d1	sociální
demokraty	demokrat	k1gMnPc4	demokrat
<g/>
;	;	kIx,	;
Jandečka	Jandeček	k1gMnSc4	Jandeček
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
lidovou	lidový	k2eAgFnSc4d1	lidová
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
sešli	sejít	k5eAaPmAgMnP	sejít
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1948	[number]	k4	1948
aby	aby	kYmCp3nP	aby
projednali	projednat	k5eAaPmAgMnP	projednat
otázku	otázka	k1gFnSc4	otázka
možné	možný	k2eAgFnSc2d1	možná
koordinace	koordinace	k1gFnSc2	koordinace
politických	politický	k2eAgFnPc2d1	politická
aktivit	aktivita	k1gFnPc2	aktivita
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
závěr	závěr	k1gInSc1	závěr
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
dospěli	dochvít	k5eAaPmAgMnP	dochvít
bylo	být	k5eAaImAgNnS	být
konstatování	konstatování	k1gNnSc1	konstatování
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
strana	strana	k1gFnSc1	strana
bude	být	k5eAaImBp3nS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
politickou	politický	k2eAgFnSc4d1	politická
činnost	činnost	k1gFnSc4	činnost
samostatně	samostatně	k6eAd1	samostatně
a	a	k8xC	a
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
informování	informování	k1gNnSc4	informování
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
v	v	k7c6	v
individuální	individuální	k2eAgFnSc6d1	individuální
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vinořská	vinořský	k2eAgFnSc1d1	vinořská
schůzka	schůzka	k1gFnSc1	schůzka
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
ústředních	ústřední	k2eAgInPc2d1	ústřední
bodů	bod	k1gInPc2	bod
konstrukce	konstrukce	k1gFnSc2	konstrukce
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
obžaloby	obžaloba	k1gFnSc2	obžaloba
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
byli	být	k5eAaImAgMnP	být
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
<g/>
:	:	kIx,	:
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Nestával	stávat	k5eNaImAgMnS	stávat
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Peška	Peška	k1gMnSc1	Peška
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Jandečka	Jandečko	k1gNnSc2	Jandečko
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
souzen	soudit	k5eAaImNgInS	soudit
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
funkcionářů	funkcionář	k1gMnPc2	funkcionář
spolku	spolek	k1gInSc2	spolek
Orel	Orel	k1gMnSc1	Orel
<g/>
.	.	kIx.	.
</s>
<s>
Vojta	Vojta	k1gMnSc1	Vojta
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1949	[number]	k4	1949
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Řádový	řádový	k2eAgInSc1d1	řádový
dům	dům	k1gInSc1	dům
salesiánů	salesián	k1gMnPc2	salesián
Dona	Don	k1gMnSc2	Don
Bosca	Boscus	k1gMnSc2	Boscus
ve	v	k7c6	v
Vinoři	Vinoř	k1gFnSc6	Vinoř
byl	být	k5eAaImAgInS	být
zlikvidován	zlikvidovat	k5eAaPmNgInS	zlikvidovat
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
(	(	kIx(	(
<g/>
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1950	[number]	k4	1950
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
Akce	akce	k1gFnSc2	akce
K	K	kA	K
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
kláštery	klášter	k1gInPc7	klášter
a	a	k8xC	a
mužskými	mužský	k2eAgInPc7d1	mužský
řeholními	řeholní	k2eAgInPc7d1	řeholní
řády	řád	k1gInPc7	řád
<g/>
.	.	kIx.	.
</s>
<s>
Pamětní	pamětní	k2eAgNnPc4d1	pamětní
místa	místo	k1gNnPc4	místo
připomínající	připomínající	k2eAgNnPc4d1	připomínající
M.	M.	kA	M.
Horákovou	Horáková	k1gFnSc4	Horáková
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
by	by	kYmCp3nP	by
měl	mít	k5eAaImAgInS	mít
vyjít	vyjít	k5eAaPmF	vyjít
dlouho	dlouho	k6eAd1	dlouho
připravovaný	připravovaný	k2eAgInSc4d1	připravovaný
životopisný	životopisný	k2eAgInSc4d1	životopisný
a	a	k8xC	a
historický	historický	k2eAgInSc4d1	historický
film	film	k1gInSc4	film
Milada	Milada	k1gFnSc1	Milada
<g/>
.	.	kIx.	.
</s>
<s>
DVOŘÁK	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
;	;	kIx,	;
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Žoldnéři	žoldnér	k1gMnPc1	žoldnér
války	válka	k1gFnSc2	válka
<g/>
:	:	kIx,	:
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
s	s	k7c7	s
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Horákovou	Horáková	k1gFnSc4	Horáková
a	a	k8xC	a
spol	spol	k1gInSc4	spol
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
HORÁKOVÁ	Horáková	k1gFnSc1	Horáková
<g/>
,	,	kIx,	,
Milada	Milada	k1gFnSc1	Milada
<g/>
.	.	kIx.	.
</s>
<s>
Dopisy	dopis	k1gInPc1	dopis
z	z	k7c2	z
cely	cela	k1gFnSc2	cela
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pankrácké	pankrácký	k2eAgFnSc2d1	Pankrácká
cely	cela	k1gFnSc2	cela
smrti	smrt	k1gFnSc2	smrt
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
27.6	[number]	k4	27.6
<g/>
.1950	.1950	k4	.1950
<g/>
..	..	k?	..
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
:	:	kIx,	:
Louč	louč	k1gFnSc1	louč
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
73	[number]	k4	73
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
KAPLAN	Kaplan	k1gMnSc1	Kaplan
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
politický	politický	k2eAgInSc1d1	politický
proces	proces	k1gInSc1	proces
<g/>
:	:	kIx,	:
M.	M.	kA	M.
Horáková	Horáková	k1gFnSc1	Horáková
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
..	..	k?	..
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Doplněk	doplněk	k1gInSc1	doplněk
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85765	[number]	k4	85765
<g/>
-	-	kIx~	-
<g/>
58	[number]	k4	58
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
350	[number]	k4	350
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
KAPLAN	Kaplan	k1gMnSc1	Kaplan
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
-	-	kIx~	-
rehabilitační	rehabilitační	k2eAgNnSc1d1	rehabilitační
řízení	řízení	k1gNnSc1	řízení
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
582	[number]	k4	582
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
KOSATÍK	KOSATÍK	kA	KOSATÍK
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
demokraté	demokrat	k1gMnPc1	demokrat
:	:	kIx,	:
50	[number]	k4	50
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
2307	[number]	k4	2307
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
KOURA	KOURA	k?	KOURA
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
FORMÁNKOVÁ	Formánková	k1gFnSc1	Formánková
Pavlína	Pavlína	k1gFnSc1	Pavlína
<g/>
.	.	kIx.	.
</s>
<s>
Žádáme	žádat	k5eAaImIp1nP	žádat
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
!	!	kIx.	!
</s>
<s>
Propagandistická	propagandistický	k2eAgFnSc1d1	propagandistická
kampaň	kampaň	k1gFnSc1	kampaň
provázející	provázející	k2eAgFnSc2d1	provázející
proces	proces	k1gInSc4	proces
s	s	k7c7	s
Miladou	Milada	k1gFnSc7	Milada
Horákovou	Horáková	k1gFnSc7	Horáková
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
studie	studie	k1gFnSc1	studie
a	a	k8xC	a
edice	edice	k1gFnSc1	edice
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
ÚSTR	ÚSTR	kA	ÚSTR
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
608	[number]	k4	608
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-87211-03-8	[number]	k4	978-80-87211-03-8
NAVARA	NAVARA	kA	NAVARA
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
<g/>
;	;	kIx,	;
GAZDÍK	GAZDÍK	kA	GAZDÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
vraždy	vražda	k1gFnPc1	vražda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vůbec	vůbec	k9	vůbec
žádní	žádný	k3yNgMnPc1	žádný
vrazi	vrah	k1gMnPc1	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
s.	s.	k?	s.
C	C	kA	C
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
1168	[number]	k4	1168
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
s	s	k7c7	s
Miladou	Milada	k1gFnSc7	Milada
Horákovou	Horáková	k1gFnSc7	Horáková
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
</s>
<s>
Ludmila	Ludmila	k1gFnSc1	Ludmila
Brožová-Polednová	Brožová-Polednová	k1gFnSc1	Brožová-Polednová
Anna	Anna	k1gFnSc1	Anna
Pollertová	Pollertová	k1gFnSc1	Pollertová
skupina	skupina	k1gFnSc1	skupina
bratří	bratr	k1gMnPc2	bratr
Mašínů	Mašín	k1gInPc2	Mašín
Josef	Josef	k1gMnSc1	Josef
Toufar	Toufar	k1gMnSc1	Toufar
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Dílo	dílo	k1gNnSc1	dílo
Rozsudek	rozsudek	k1gInSc1	rozsudek
Státního	státní	k2eAgInSc2d1	státní
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ze	z	k7c2	z
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g/>
j.	j.	k?	j.
Or	Or	k1gMnSc7	Or
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
VII	VII	kA	VII
65	[number]	k4	65
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
50	[number]	k4	50
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Osoba	osoba	k1gFnSc1	osoba
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
Proces	proces	k1gInSc1	proces
H	H	kA	H
dokumentární	dokumentární	k2eAgFnSc2d1	dokumentární
série	série	k1gFnSc2	série
scenáristy	scenárista	k1gMnSc2	scenárista
a	a	k8xC	a
režiséra	režisér	k1gMnSc2	režisér
Martina	Martin	k1gInSc2	Martin
Vadase	Vadasa	k1gFnSc3	Vadasa
rekonstruuje	rekonstruovat	k5eAaBmIp3nS	rekonstruovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
archivů	archiv	k1gInPc2	archiv
průběh	průběh	k1gInSc1	průběh
Největšího	veliký	k2eAgInSc2d3	veliký
politického	politický	k2eAgInSc2d1	politický
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
nazval	nazvat	k5eAaBmAgMnS	nazvat
historik	historik	k1gMnSc1	historik
Karel	Karel	k1gMnSc1	Karel
Kaplan	Kaplan	k1gMnSc1	Kaplan
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
×	×	k?	×
<g/>
52	[number]	k4	52
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
©	©	k?	©
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
série	série	k1gFnSc1	série
reprízovaná	reprízovaný	k2eAgFnSc1d1	reprízovaná
ve	v	k7c6	v
vysílání	vysílání	k1gNnSc6	vysílání
ČT	ČT	kA	ČT
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bývala	bývat	k5eAaImAgFnS	bývat
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
i-vysílání	iysílání	k1gNnSc6	i-vysílání
ČT	ČT	kA	ČT
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Luděk	Luděk	k1gMnSc1	Luděk
Navara	Navar	k1gMnSc2	Navar
<g/>
:	:	kIx,	:
Případ	případ	k1gInSc1	případ
Horák	Horák	k1gMnSc1	Horák
in	in	k?	in
Příběhy	příběh	k1gInPc1	příběh
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
Tomáš	Tomáš	k1gMnSc1	Tomáš
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
:	:	kIx,	:
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
in	in	k?	in
Totalita	totalita	k1gFnSc1	totalita
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Lubomír	Lubomír	k1gMnSc1	Lubomír
Boháč	Boháč	k1gMnSc1	Boháč
<g/>
:	:	kIx,	:
Největší	veliký	k2eAgInSc1d3	veliký
politický	politický	k2eAgInSc1d1	politický
proces	proces	k1gInSc1	proces
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
in	in	k?	in
Listy	lista	k1gFnSc2	lista
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
Petr	Petr	k1gMnSc1	Petr
Koura	Koura	k?	Koura
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
Kudrys	Kudrys	k1gInSc1	Kudrys
<g/>
:	:	kIx,	:
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
in	in	k?	in
Portréty	portrét	k1gInPc1	portrét
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
plné	plný	k2eAgNnSc4d1	plné
znění	znění	k1gNnSc4	znění
rozsudku	rozsudek	k1gInSc2	rozsudek
(	(	kIx(	(
<g/>
dobová	dobový	k2eAgFnSc1d1	dobová
strojová	strojový	k2eAgFnSc1d1	strojová
kopie	kopie	k1gFnSc1	kopie
<g/>
)	)	kIx)	)
Národní	národní	k2eAgInSc1d1	národní
archiv	archiv	k1gInSc1	archiv
Eliška	Eliška	k1gFnSc1	Eliška
Klepalová	Klepalová	k1gFnSc1	Klepalová
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Janáč	Janáč	k1gMnSc1	Janáč
<g/>
:	:	kIx,	:
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
promluví	promluvit	k5eAaPmIp3nS	promluvit
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
55	[number]	k4	55
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
trezorový	trezorový	k2eAgInSc1d1	trezorový
záznam	záznam	k1gInSc1	záznam
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
řeči	řeč	k1gFnSc2	řeč
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
Doležálková	Doležálková	k1gFnSc1	Doležálková
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Voříšek	Voříšek	k1gMnSc1	Voříšek
<g/>
:	:	kIx,	:
1950	[number]	k4	1950
-	-	kIx~	-
vražda	vražda	k1gFnSc1	vražda
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
-	-	kIx~	-
zločin	zločin	k1gInSc1	zločin
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
ČCE	ČCE	kA	ČCE
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
<g/>
-Smíchov	-Smíchov	k1gInSc1	-Smíchov
Dokument	dokument	k1gInSc4	dokument
o	o	k7c6	o
procesu	proces	k1gInSc6	proces
zveřejněný	zveřejněný	k2eAgMnSc1d1	zveřejněný
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
stránkách	stránka	k1gFnPc6	stránka
ČRo	ČRo	k1gMnPc2	ČRo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
odkazu	odkaz	k1gInSc2	odkaz
na	na	k7c4	na
dvoudílný	dvoudílný	k2eAgInSc4d1	dvoudílný
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
pořad	pořad	k1gInSc4	pořad
<g/>
)	)	kIx)	)
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Formánek	Formánek	k1gMnSc1	Formánek
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
procesu	proces	k1gInSc2	proces
H	H	kA	H
<g/>
,	,	kIx,	,
červen	červen	k1gInSc1	červen
2009	[number]	k4	2009
JUDr.	JUDr.	kA	JUDr.
Milada	Milada	k1gFnSc1	Milada
Horáková	Horáková	k1gFnSc1	Horáková
na	na	k7c6	na
webu	web	k1gInSc6	web
PIS	Pisa	k1gFnPc2	Pisa
Klub	klub	k1gInSc4	klub
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
:	:	kIx,	:
www.miladahorakova.cz	www.miladahorakova.cz	k1gMnSc1	www.miladahorakova.cz
Jan	Jan	k1gMnSc1	Jan
Přeučil	přeučit	k5eAaPmAgMnS	přeučit
<g/>
:	:	kIx,	:
Můj	můj	k3xOp1gMnSc1	můj
tatínek	tatínek	k1gMnSc1	tatínek
dostal	dostat	k5eAaPmAgMnS	dostat
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
</s>
