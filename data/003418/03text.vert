<s>
Albatrosovití	Albatrosovitý	k2eAgMnPc1d1	Albatrosovitý
(	(	kIx(	(
<g/>
Diomedeidae	Diomedeidae	k1gNnSc7	Diomedeidae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čeleď	čeleď	k1gFnSc1	čeleď
největších	veliký	k2eAgMnPc2d3	veliký
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
trubkonosých	trubkonosý	k2eAgFnPc2d1	trubkonosý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
příbuzných	příbuzný	k2eAgFnPc2d1	příbuzná
čeledí	čeleď	k1gFnPc2	čeleď
buřňákovitých	buřňákovitý	k2eAgFnPc2d1	buřňákovitý
<g/>
,	,	kIx,	,
buřňáčkovitých	buřňáčkovitý	k2eAgFnPc2d1	buřňáčkovitý
a	a	k8xC	a
buřníkovitých	buřníkovitý	k2eAgFnPc2d1	buřníkovitý
se	se	k3xPyFc4	se
zřetelně	zřetelně	k6eAd1	zřetelně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
trubkovité	trubkovitý	k2eAgFnPc1d1	trubkovitá
nozdry	nozdra	k1gFnPc1	nozdra
umístěné	umístěný	k2eAgFnPc1d1	umístěná
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
horní	horní	k2eAgFnPc1d1	horní
čelisti	čelist	k1gFnPc1	čelist
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ti	ten	k3xDgMnPc1	ten
druzí	druhý	k4xOgMnPc1	druhý
mají	mít	k5eAaImIp3nP	mít
nozdru	nozdra	k1gFnSc4	nozdra
jen	jen	k9	jen
jedinou	jediný	k2eAgFnSc4d1	jediná
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
albatrosovití	albatrosovitý	k2eAgMnPc1d1	albatrosovitý
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
čtyřmi	čtyři	k4xCgInPc7	čtyři
recentními	recentní	k2eAgInPc7d1	recentní
rody	rod	k1gInPc7	rod
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgNnSc4d1	stejné
české	český	k2eAgNnSc4d1	české
jméno	jméno	k1gNnSc4	jméno
albatros	albatros	k1gMnSc1	albatros
<g/>
.	.	kIx.	.
</s>
<s>
Albatrosi	albatros	k1gMnPc1	albatros
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
jen	jen	k9	jen
albatrosi	albatros	k1gMnPc1	albatros
bělohřbetí	bělohřbetý	k2eAgMnPc1d1	bělohřbetý
<g/>
,	,	kIx,	,
albatrosi	albatros	k1gMnPc1	albatros
černonozí	černonohý	k2eAgMnPc1d1	černonohý
a	a	k8xC	a
albatrosi	albatros	k1gMnPc1	albatros
laysanští	laysanštit	k5eAaPmIp3nP	laysanštit
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Pacifiku	Pacifik	k1gInSc6	Pacifik
na	na	k7c6	na
Havajských	havajský	k2eAgInPc6d1	havajský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ještě	ještě	k9	ještě
albatrosi	albatros	k1gMnPc1	albatros
galapážští	galapážský	k2eAgMnPc1d1	galapážský
hnízdící	hnízdící	k2eAgFnPc4d1	hnízdící
na	na	k7c6	na
Galapážských	galapážský	k2eAgInPc6d1	galapážský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
lovící	lovící	k2eAgFnPc4d1	lovící
v	v	k7c6	v
chladném	chladný	k2eAgInSc6d1	chladný
Peruánském	peruánský	k2eAgInSc6d1	peruánský
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Atlantického	atlantický	k2eAgInSc2d1	atlantický
oceánu	oceán	k1gInSc2	oceán
žádní	žádný	k3yNgMnPc1	žádný
albatrosi	albatros	k1gMnPc1	albatros
nežijí	žít	k5eNaImIp3nP	žít
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
na	na	k7c6	na
pobřežích	pobřeží	k1gNnPc6	pobřeží
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jejich	jejich	k3xOp3gInPc1	jejich
fosilní	fosilní	k2eAgInPc1d1	fosilní
kosterní	kosterní	k2eAgInPc1d1	kosterní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
vůbec	vůbec	k9	vůbec
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
létající	létající	k2eAgMnPc4d1	létající
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vysocí	vysoký	k2eAgMnPc1d1	vysoký
až	až	k9	až
50	[number]	k4	50
cm	cm	kA	cm
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
váhy	váha	k1gFnPc4	váha
10	[number]	k4	10
kg	kg	kA	kg
a	a	k8xC	a
rozpětí	rozpětí	k1gNnSc2	rozpětí
jejich	jejich	k3xOp3gNnPc2	jejich
křídel	křídlo	k1gNnPc2	křídlo
je	být	k5eAaImIp3nS	být
i	i	k9	i
přes	přes	k7c4	přes
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
mají	mít	k5eAaImIp3nP	mít
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
<g/>
,	,	kIx,	,
úzká	úzký	k2eAgNnPc4d1	úzké
s	s	k7c7	s
extrémně	extrémně	k6eAd1	extrémně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
loketní	loketní	k2eAgFnSc4d1	loketní
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
letu	let	k1gInSc6	let
využívají	využívat	k5eAaImIp3nP	využívat
speciální	speciální	k2eAgFnPc1d1	speciální
techniky	technika	k1gFnPc1	technika
(	(	kIx(	(
<g/>
dynamic	dynamice	k1gFnPc2	dynamice
soaring	soaring	k1gInSc1	soaring
<g/>
)	)	kIx)	)
-	-	kIx~	-
rozdílné	rozdílný	k2eAgFnSc6d1	rozdílná
rychlosti	rychlost	k1gFnSc6	rychlost
větru	vítr	k1gInSc2	vítr
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
a	a	k8xC	a
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
výšce	výška	k1gFnSc6	výška
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Získají	získat	k5eAaPmIp3nP	získat
energii	energie	k1gFnSc4	energie
letem	letem	k6eAd1	letem
po	po	k7c6	po
větru	vítr	k1gInSc6	vítr
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	s	k7c7	s
zpět	zpět	k6eAd1	zpět
mírnějším	mírný	k2eAgInSc7d2	mírnější
protivětrem	protivítr	k1gInSc7	protivítr
u	u	k7c2	u
hladiny	hladina	k1gFnSc2	hladina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
využijí	využít	k5eAaPmIp3nP	využít
i	i	k9	i
závětří	závětří	k1gNnSc4	závětří
v	v	k7c6	v
brázdě	brázda	k1gFnSc6	brázda
mezi	mezi	k7c7	mezi
vlnami	vlna	k1gFnPc7	vlna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
každým	každý	k3xTgInSc7	každý
takovým	takový	k3xDgInSc7	takový
cyklem	cyklus	k1gInSc7	cyklus
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přebytek	přebytek	k1gInSc1	přebytek
jejich	jejich	k3xOp3gFnSc2	jejich
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
pak	pak	k6eAd1	pak
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
postupnému	postupný	k2eAgInSc3d1	postupný
bočnímu	boční	k2eAgInSc3d1	boční
pohybu	pohyb	k1gInSc3	pohyb
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
částečně	částečně	k6eAd1	částečně
proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
-	-	kIx~	-
a	a	k8xC	a
bez	bez	k7c2	bez
potřeby	potřeba	k1gFnSc2	potřeba
mávání	mávání	k1gNnSc2	mávání
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
nezávisle	závisle	k6eNd1	závisle
objevil	objevit	k5eAaPmAgMnS	objevit
teprve	teprve	k6eAd1	teprve
před	před	k7c7	před
necelými	celý	k2eNgNnPc7d1	necelé
50	[number]	k4	50
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
analogickému	analogický	k2eAgInSc3d1	analogický
postupu	postup	k1gInSc3	postup
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
svahu	svah	k1gInSc2	svah
a	a	k8xC	a
rozdílné	rozdílný	k2eAgFnSc2d1	rozdílná
rychlosti	rychlost	k1gFnSc2	rychlost
větru	vítr	k1gInSc2	vítr
nad	nad	k7c7	nad
kopcem	kopec	k1gInSc7	kopec
a	a	k8xC	a
v	v	k7c6	v
závětří	závětří	k1gNnSc6	závětří
dnes	dnes	k6eAd1	dnes
bezmotorové	bezmotorový	k2eAgInPc1d1	bezmotorový
RC	RC	kA	RC
větroně	větroň	k1gInPc1	větroň
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
špičkových	špičkový	k2eAgFnPc2d1	špičková
rychlostí	rychlost	k1gFnPc2	rychlost
až	až	k9	až
800	[number]	k4	800
<g/>
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
U	u	k7c2	u
žádného	žádný	k3yNgMnSc4	žádný
jiného	jiný	k2eAgMnSc4d1	jiný
ptáka	pták	k1gMnSc4	pták
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
zatím	zatím	k6eAd1	zatím
nebyla	být	k5eNaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nemusí	muset	k5eNaImIp3nS	muset
mávat	mávat	k5eAaImF	mávat
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
překonává	překonávat	k5eAaImIp3nS	překonávat
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
i	i	k8xC	i
proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
s	s	k7c7	s
minimálním	minimální	k2eAgInSc7d1	minimální
výdejem	výdej	k1gInSc7	výdej
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
nejvíce	nejvíce	k6eAd1	nejvíce
sil	síla	k1gFnPc2	síla
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musí	muset	k5eAaImIp3nS	muset
opakovaně	opakovaně	k6eAd1	opakovaně
vzlétat	vzlétat	k5eAaImF	vzlétat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vznesení	vznesení	k1gNnSc6	vznesení
se	se	k3xPyFc4	se
z	z	k7c2	z
hladiny	hladina	k1gFnSc2	hladina
nebo	nebo	k8xC	nebo
z	z	k7c2	z
rovné	rovný	k2eAgFnSc2d1	rovná
půdy	půda	k1gFnSc2	půda
jim	on	k3xPp3gMnPc3	on
dělají	dělat	k5eAaImIp3nP	dělat
problémy	problém	k1gInPc1	problém
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
dlouze	dlouho	k6eAd1	dlouho
rozběhnout	rozběhnout	k5eAaPmF	rozběhnout
<g/>
,	,	kIx,	,
nejraději	rád	k6eAd3	rád
proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
dospělého	dospělý	k2eAgMnSc2d1	dospělý
albatrosa	albatros	k1gMnSc2	albatros
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
směsici	směsice	k1gFnSc4	směsice
hnědé	hnědý	k2eAgFnPc1d1	hnědá
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
odstínech	odstín	k1gInPc6	odstín
a	a	k8xC	a
bílé	bílý	k2eAgFnPc4d1	bílá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
od	od	k7c2	od
albatros	albatros	k1gMnSc1	albatros
Sanfordova	Sanfordův	k2eAgFnSc1d1	Sanfordův
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
černé	černé	k1gNnSc4	černé
jen	jen	k9	jen
konce	konec	k1gInPc1	konec
křídel	křídlo	k1gNnPc2	křídlo
až	až	k9	až
po	po	k7c4	po
albatrosa	albatros	k1gMnSc4	albatros
hnědého	hnědý	k2eAgMnSc4d1	hnědý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc1d1	celý
hnědý	hnědý	k2eAgInSc1d1	hnědý
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
mají	mít	k5eAaImIp3nP	mít
krátké	krátký	k2eAgFnPc1d1	krátká
a	a	k8xC	a
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgFnPc1d1	umístěná
více	hodně	k6eAd2	hodně
vzadu	vzadu	k6eAd1	vzadu
<g/>
.	.	kIx.	.
</s>
<s>
Chodí	chodit	k5eAaImIp3nP	chodit
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
kolébavě	kolébavě	k6eAd1	kolébavě
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
vytrvalí	vytrvalý	k2eAgMnPc1d1	vytrvalý
chodci	chodec	k1gMnPc1	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc1	tři
přední	přední	k2eAgInPc1d1	přední
prsty	prst	k1gInPc1	prst
které	který	k3yRgMnPc4	který
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
spojené	spojený	k2eAgInPc1d1	spojený
plovací	plovací	k2eAgFnSc7d1	plovací
blánou	blána	k1gFnSc7	blána
<g/>
.	.	kIx.	.
</s>
<s>
Zobáky	zobák	k1gInPc1	zobák
mají	mít	k5eAaImIp3nP	mít
mohutné	mohutný	k2eAgMnPc4d1	mohutný
<g/>
,	,	kIx,	,
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
14	[number]	k4	14
až	až	k9	až
19	[number]	k4	19
cm	cm	kA	cm
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
čelist	čelist	k1gFnSc1	čelist
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
a	a	k8xC	a
přes	přes	k7c4	přes
spodní	spodní	k2eAgFnSc4d1	spodní
je	být	k5eAaImIp3nS	být
hákovitě	hákovitě	k6eAd1	hákovitě
zahnutá	zahnutý	k2eAgFnSc1d1	zahnutá
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
dvě	dva	k4xCgFnPc1	dva
čichové	čichový	k2eAgFnPc1d1	čichová
nozdry	nozdra	k1gFnPc1	nozdra
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
nápomocny	nápomocen	k2eAgFnPc1d1	nápomocna
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
albatrosi	albatros	k1gMnPc1	albatros
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ptáků	pták	k1gMnPc2	pták
mají	mít	k5eAaImIp3nP	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
čich	čich	k1gInSc4	čich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
zobáku	zobák	k1gInSc2	zobák
jsou	být	k5eAaImIp3nP	být
nosní	nosní	k2eAgFnPc4d1	nosní
žlázy	žláza	k1gFnPc4	žláza
vylučující	vylučující	k2eAgFnSc4d1	vylučující
nadbytečnou	nadbytečný	k2eAgFnSc4d1	nadbytečná
sůl	sůl	k1gFnSc4	sůl
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
spolykají	spolykat	k5eAaPmIp3nP	spolykat
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
kořistí	kořist	k1gFnSc7	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vynikající	vynikající	k2eAgInSc4d1	vynikající
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
dokážou	dokázat	k5eAaPmIp3nP	dokázat
s	s	k7c7	s
velké	velký	k2eAgFnSc2d1	velká
výšky	výška	k1gFnSc2	výška
spatřit	spatřit	k5eAaPmF	spatřit
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
doby	doba	k1gFnPc4	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
nevstupují	vstupovat	k5eNaImIp3nP	vstupovat
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
prožijí	prožít	k5eAaPmIp3nP	prožít
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Létají	létat	k5eAaImIp3nP	létat
po	po	k7c6	po
pravidelných	pravidelný	k2eAgFnPc6d1	pravidelná
trasách	trasa	k1gFnPc6	trasa
vedoucích	vedoucí	k1gFnPc2	vedoucí
po	po	k7c6	po
tradičních	tradiční	k2eAgNnPc6d1	tradiční
místech	místo	k1gNnPc6	místo
výskytu	výskyt	k1gInSc2	výskyt
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
druh	druh	k1gInSc4	druh
loví	lovit	k5eAaImIp3nP	lovit
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
poblíž	poblíž	k7c2	poblíž
chladných	chladný	k2eAgInPc2d1	chladný
<g/>
,	,	kIx,	,
úživných	úživný	k2eAgInPc2d1	úživný
mořských	mořský	k2eAgInPc2d1	mořský
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mořské	mořský	k2eAgFnSc6d1	mořská
hladině	hladina	k1gFnSc6	hladina
i	i	k9	i
spí	spát	k5eAaImIp3nP	spát
<g/>
,	,	kIx,	,
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
tak	tak	k9	tak
sami	sám	k3xTgMnPc1	sám
kořistí	kořistit	k5eAaImIp3nP	kořistit
velkých	velká	k1gFnPc2	velká
vodních	vodní	k2eAgMnPc2d1	vodní
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Žerou	žrát	k5eAaImIp3nP	žrát
výhradně	výhradně	k6eAd1	výhradně
živočichy	živočich	k1gMnPc4	živočich
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
různě	různě	k6eAd1	různě
specializují	specializovat	k5eAaBmIp3nP	specializovat
na	na	k7c4	na
rybky	rybka	k1gFnPc4	rybka
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgMnPc4d1	drobný
hlavonožce	hlavonožec	k1gMnPc4	hlavonožec
nebo	nebo	k8xC	nebo
korýše	korýš	k1gMnPc4	korýš
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
i	i	k9	i
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
menší	malý	k2eAgFnPc1d2	menší
chobotnice	chobotnice	k1gFnPc1	chobotnice
které	který	k3yRgMnPc4	který
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
dne	den	k1gInSc2	den
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
potravu	potrava	k1gFnSc4	potrava
sbírají	sbírat	k5eAaImIp3nP	sbírat
z	z	k7c2	z
hladiny	hladina	k1gFnSc2	hladina
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
ale	ale	k9	ale
dokáži	dokázat	k5eAaPmIp1nS	dokázat
potopit	potopit	k5eAaPmF	potopit
i	i	k9	i
do	do	k7c2	do
5	[number]	k4	5
m.	m.	k?	m.
V	v	k7c6	v
období	období	k1gNnSc6	období
nedostatku	nedostatek	k1gInSc2	nedostatek
potravy	potrava	k1gFnSc2	potrava
žerou	žrát	k5eAaImIp3nP	žrát
i	i	k9	i
mršiny	mršina	k1gFnPc1	mršina
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
po	po	k7c6	po
mořské	mořský	k2eAgFnSc6d1	mořská
hladině	hladina	k1gFnSc6	hladina
<g/>
,	,	kIx,	,
tučňáky	tučňák	k1gMnPc4	tučňák
<g/>
,	,	kIx,	,
tuleně	tuleň	k1gMnPc4	tuleň
i	i	k8xC	i
velryby	velryba	k1gFnPc4	velryba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kril	kril	k1gMnSc1	kril
žijící	žijící	k2eAgMnSc1d1	žijící
blízko	blízko	k7c2	blízko
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Létají	létat	k5eAaImIp3nP	létat
také	také	k9	také
za	za	k7c7	za
rybářskými	rybářský	k2eAgFnPc7d1	rybářská
loděmi	loď	k1gFnPc7	loď
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
vyhozenými	vyhozený	k2eAgInPc7d1	vyhozený
rybími	rybí	k2eAgInPc7d1	rybí
zbytky	zbytek	k1gInPc7	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
i	i	k8xC	i
albatrosi	albatros	k1gMnPc1	albatros
vole	vole	k1gNnSc1	vole
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
ukládají	ukládat	k5eAaImIp3nP	ukládat
potravu	potrava	k1gFnSc4	potrava
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
při	při	k7c6	při
krmení	krmení	k1gNnSc6	krmení
<g/>
.	.	kIx.	.
</s>
<s>
Albatros	albatros	k1gMnSc1	albatros
pije	pít	k5eAaImIp3nS	pít
slanou	slaný	k2eAgFnSc4d1	slaná
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
monogamních	monogamní	k2eAgInPc6d1	monogamní
párech	pár	k1gInPc6	pár
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
rozpadnou	rozpadnout	k5eAaPmIp3nP	rozpadnout
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nepodaří	podařit	k5eNaPmIp3nS	podařit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
sezon	sezona	k1gFnPc2	sezona
po	po	k7c6	po
sobě	se	k3xPyFc3	se
vyvést	vyvést	k5eAaPmF	vyvést
mladé	mladý	k1gMnPc4	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdění	hnízdění	k1gNnSc1	hnízdění
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
energeticky	energeticky	k6eAd1	energeticky
velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgInPc1d1	náročný
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc1d2	menší
druhy	druh	k1gInPc1	druh
obvykle	obvykle	k6eAd1	obvykle
zahnízdí	zahnízdit	k5eAaPmIp3nP	zahnízdit
ob	ob	k7c4	ob
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc4d2	veliký
co	co	k9	co
dva	dva	k4xCgInPc4	dva
až	až	k8xS	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
větší	veliký	k2eAgInSc1d2	veliký
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
snáší	snášet	k5eAaImIp3nS	snášet
větší	veliký	k2eAgNnPc4d2	veliký
vejce	vejce	k1gNnPc4	vejce
a	a	k8xC	a
déle	dlouho	k6eAd2	dlouho
o	o	k7c4	o
mládě	mládě	k1gNnSc4	mládě
pečuje	pečovat	k5eAaImIp3nS	pečovat
<g/>
,	,	kIx,	,
od	od	k7c2	od
snesení	snesení	k1gNnSc2	snesení
vejce	vejce	k1gNnSc2	vejce
po	po	k7c6	po
opeření	opeření	k1gNnSc6	opeření
mláděte	mládě	k1gNnSc2	mládě
to	ten	k3xDgNnSc1	ten
trvá	trvat	k5eAaImIp3nS	trvat
téměř	téměř	k6eAd1	téměř
i	i	k9	i
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
tradičně	tradičně	k6eAd1	tradičně
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nebylo	být	k5eNaImAgNnS	být
suchozemských	suchozemský	k2eAgFnPc2d1	suchozemská
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
jim	on	k3xPp3gMnPc3	on
mohly	moct	k5eAaImAgInP	moct
ublížit	ublížit	k5eAaPmF	ublížit
<g/>
.	.	kIx.	.
</s>
<s>
Hnízda	hnízdo	k1gNnPc1	hnízdo
stavějí	stavět	k5eAaImIp3nP	stavět
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc4	ten
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
čítají	čítat	k5eAaImIp3nP	čítat
stovky	stovka	k1gFnPc1	stovka
nebo	nebo	k8xC	nebo
tisíce	tisíc	k4xCgInSc2	tisíc
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
veliká	veliký	k2eAgFnSc1d1	veliká
rozdílnost	rozdílnost	k1gFnSc1	rozdílnost
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
hnízdění	hnízdění	k1gNnSc2	hnízdění
i	i	k8xC	i
způsobu	způsob	k1gInSc2	způsob
stavění	stavění	k1gNnSc6	stavění
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
a	a	k8xC	a
stavějí	stavět	k5eAaImIp3nP	stavět
si	se	k3xPyFc3	se
hnízda	hnízdo	k1gNnSc2	hnízdo
z	z	k7c2	z
klestí	klest	k1gFnPc2	klest
a	a	k8xC	a
travin	travina	k1gFnPc2	travina
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
v	v	k7c6	v
travnatém	travnatý	k2eAgInSc6d1	travnatý
porostu	porost	k1gInSc6	porost
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
si	se	k3xPyFc3	se
netvoří	tvořit	k5eNaImIp3nP	tvořit
hnízdo	hnízdo	k1gNnSc4	hnízdo
žádné	žádný	k3yNgFnPc4	žádný
a	a	k8xC	a
kladou	klást	k5eAaImIp3nP	klást
vejce	vejce	k1gNnPc4	vejce
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
skálu	skála	k1gFnSc4	skála
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
důlek	důlek	k1gInSc4	důlek
v	v	k7c6	v
písku	písek	k1gInSc6	písek
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hustota	hustota	k1gFnSc1	hustota
hnízd	hnízdo	k1gNnPc2	hnízdo
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
je	být	k5eAaImIp3nS	být
rozdílná	rozdílný	k2eAgNnPc1d1	rozdílné
<g/>
,	,	kIx,	,
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
je	být	k5eAaImIp3nS	být
však	však	k9	však
dobrý	dobrý	k2eAgInSc4d1	dobrý
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
albatrosů	albatros	k1gMnPc2	albatros
k	k	k7c3	k
zahnízdění	zahnízdění	k1gNnSc3	zahnízdění
si	se	k3xPyFc3	se
staré	starý	k2eAgFnPc1d1	stará
páry	pára	k1gFnPc1	pára
většinou	většina	k1gFnSc7	většina
vybírají	vybírat	k5eAaImIp3nP	vybírat
stejná	stejný	k2eAgNnPc1d1	stejné
místa	místo	k1gNnPc1	místo
pro	pro	k7c4	pro
postavení	postavení	k1gNnSc4	postavení
hnízda	hnízdo	k1gNnSc2	hnízdo
jako	jako	k9	jako
v	v	k7c6	v
minulostí	minulost	k1gFnSc7	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
následují	následovat	k5eAaImIp3nP	následovat
obřadní	obřadní	k2eAgInPc4d1	obřadní
tance	tanec	k1gInPc4	tanec
a	a	k8xC	a
po	po	k7c6	po
spáření	spáření	k1gNnSc6	spáření
samice	samice	k1gFnSc2	samice
snese	snést	k5eAaPmIp3nS	snést
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
veliké	veliký	k2eAgNnSc4d1	veliké
bílé	bílý	k2eAgNnSc4d1	bílé
vejce	vejce	k1gNnSc4	vejce
s	s	k7c7	s
hnědými	hnědý	k2eAgFnPc7d1	hnědá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
až	až	k9	až
500	[number]	k4	500
gramů	gram	k1gInPc2	gram
(	(	kIx(	(
<g/>
slepičí	slepičit	k5eAaImIp3nS	slepičit
80	[number]	k4	80
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
inkubační	inkubační	k2eAgFnSc4d1	inkubační
dobu	doba	k1gFnSc4	doba
trvající	trvající	k2eAgFnSc4d1	trvající
70	[number]	k4	70
až	až	k8xS	až
80	[number]	k4	80
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
při	při	k7c6	při
sezení	sezení	k1gNnSc6	sezení
na	na	k7c6	na
vejci	vejce	k1gNnSc6	vejce
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
týdny	týden	k1gInPc4	týden
po	po	k7c4	po
vylíhnutí	vylíhnutí	k1gNnSc4	vylíhnutí
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgMnSc1	jeden
rodič	rodič	k1gMnSc1	rodič
mládě	mládě	k1gNnSc4	mládě
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
a	a	k8xC	a
střeží	střežit	k5eAaImIp3nS	střežit
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
mu	on	k3xPp3gMnSc3	on
přináší	přinášet	k5eAaImIp3nS	přinášet
již	již	k9	již
natrávenou	natrávený	k2eAgFnSc4d1	natrávená
potravu	potrava	k1gFnSc4	potrava
ve	v	k7c6	v
voleti	vole	k1gNnSc6	vole
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
již	již	k6eAd1	již
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
mládě	mládě	k1gNnSc1	mládě
samo	sám	k3xTgNnSc1	sám
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
odlétají	odlétat	k5eAaImIp3nP	odlétat
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
lovit	lovit	k5eAaImF	lovit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
do	do	k7c2	do
plného	plný	k2eAgNnSc2d1	plné
opeření	opeření	k1gNnSc2	opeření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
již	již	k9	již
hmotnost	hmotnost	k1gFnSc4	hmotnost
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
140	[number]	k4	140
do	do	k7c2	do
280	[number]	k4	280
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
po	po	k7c4	po
ten	ten	k3xDgInSc4	ten
čas	čas	k1gInSc4	čas
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc1	mládě
stále	stále	k6eAd1	stále
krmena	krmen	k2eAgNnPc1d1	krmeno
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
najednou	najednou	k6eAd1	najednou
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
rodiče	rodič	k1gMnPc4	rodič
přestanou	přestat	k5eAaPmIp3nP	přestat
starat	starat	k5eAaImF	starat
a	a	k8xC	a
odletí	odletět	k5eAaPmIp3nP	odletět
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
odkázáno	odkázat	k5eAaPmNgNnS	odkázat
samo	sám	k3xTgNnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
jen	jen	k9	jen
vrozené	vrozený	k2eAgInPc4d1	vrozený
instinkty	instinkt	k1gInPc4	instinkt
mu	on	k3xPp3gMnSc3	on
poradí	poradit	k5eAaPmIp3nS	poradit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
a	a	k8xC	a
kudy	kudy	k6eAd1	kudy
letět	letět	k5eAaImF	letět
a	a	k8xC	a
lovit	lovit	k5eAaImF	lovit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospělá	dospělý	k2eAgNnPc1d1	dospělé
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc4	mládě
asi	asi	k9	asi
ve	v	k7c6	v
4	[number]	k4	4
až	až	k8xS	až
6	[number]	k4	6
létech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
období	období	k1gNnSc2	období
páření	páření	k1gNnSc2	páření
přilétají	přilétat	k5eAaImIp3nP	přilétat
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
odkud	odkud	k6eAd1	odkud
pocházejí	pocházet	k5eAaImIp3nP	pocházet
a	a	k8xC	a
zkouší	zkoušet	k5eAaImIp3nP	zkoušet
první	první	k4xOgInPc4	první
zásnubní	zásnubní	k2eAgInPc4d1	zásnubní
tance	tanec	k1gInPc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
však	však	k9	však
další	další	k2eAgNnSc1d1	další
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
páry	pár	k1gInPc1	pár
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
zahnízdí	zahnízdit	k5eAaPmIp3nS	zahnízdit
<g/>
.	.	kIx.	.
</s>
<s>
Albatrosi	albatros	k1gMnPc1	albatros
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
40	[number]	k4	40
a	a	k8xC	a
více	hodně	k6eAd2	hodně
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
při	při	k7c6	při
samém	samý	k3xTgInSc6	samý
hnízdění	hnízdění	k1gNnSc6	hnízdění
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hodně	hodně	k6eAd1	hodně
vajec	vejce	k1gNnPc2	vejce
praskne	prasknout	k5eAaPmIp3nS	prasknout
na	na	k7c6	na
kamenitém	kamenitý	k2eAgInSc6d1	kamenitý
podkladu	podklad	k1gInSc6	podklad
nebo	nebo	k8xC	nebo
čerstvě	čerstvě	k6eAd1	čerstvě
vylíhnutá	vylíhnutý	k2eAgNnPc1d1	vylíhnuté
jsou	být	k5eAaImIp3nP	být
při	pře	k1gFnSc4	pře
nepozorností	nepozornost	k1gFnPc2	nepozornost
rodičů	rodič	k1gMnPc2	rodič
uchvácena	uchvátit	k5eAaPmNgFnS	uchvátit
chaluhami	chaluha	k1gFnPc7	chaluha
nebo	nebo	k8xC	nebo
racky	racek	k1gMnPc7	racek
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
hrozí	hrozit	k5eAaImIp3nS	hrozit
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
minulostí	minulost	k1gFnSc7	minulost
zavlekli	zavleknout	k5eAaPmAgMnP	zavleknout
krysy	krysa	k1gFnSc2	krysa
a	a	k8xC	a
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
je	on	k3xPp3gNnSc4	on
loví	lovit	k5eAaImIp3nP	lovit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
od	od	k7c2	od
králíků	králík	k1gMnPc2	králík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
hrabáním	hrabání	k1gNnSc7	hrabání
rozrývají	rozrývat	k5eAaImIp3nP	rozrývat
jejich	jejich	k3xOp3gNnPc4	jejich
hnízda	hnízdo	k1gNnPc4	hnízdo
a	a	k8xC	a
ničí	ničit	k5eAaImIp3nS	ničit
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Riskantní	riskantní	k2eAgInPc1d1	riskantní
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
pokusy	pokus	k1gInPc1	pokus
mláďat	mládě	k1gNnPc2	mládě
o	o	k7c4	o
prvý	prvý	k4xOgInSc4	prvý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
čekávají	čekávat	k5eAaImIp3nP	čekávat
žraloci	žralok	k1gMnPc1	žralok
a	a	k8xC	a
draví	dravý	k2eAgMnPc1d1	dravý
delfíni	delfín	k1gMnPc1	delfín
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
lidé	člověk	k1gMnPc1	člověk
svou	svůj	k3xOyFgFnSc4	svůj
činnosti	činnost	k1gFnSc2	činnost
tyto	tento	k3xDgMnPc4	tento
ptáky	pták	k1gMnPc4	pták
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
záměrné	záměrný	k2eAgNnSc1d1	záměrné
lovení	lovení	k1gNnSc1	lovení
albatrosů	albatros	k1gMnPc2	albatros
již	již	k6eAd1	již
snad	snad	k9	snad
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Rybářské	rybářský	k2eAgFnPc1d1	rybářská
lodě	loď	k1gFnPc1	loď
chytají	chytat	k5eAaImIp3nP	chytat
ohromná	ohromný	k2eAgNnPc4d1	ohromné
množství	množství	k1gNnPc4	množství
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
na	na	k7c4	na
hejna	hejno	k1gNnPc4	hejno
ptáků	pták	k1gMnPc2	pták
již	již	k9	již
moc	moc	k6eAd1	moc
nezbývá	zbývat	k5eNaImIp3nS	zbývat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
albatrosi	albatros	k1gMnPc1	albatros
okolo	okolo	k7c2	okolo
lodí	loď	k1gFnPc2	loď
loví	lovit	k5eAaImIp3nP	lovit
rybky	rybka	k1gFnPc1	rybka
připravené	připravený	k2eAgFnPc1d1	připravená
jako	jako	k9	jako
nástraha	nástraha	k1gFnSc1	nástraha
a	a	k8xC	a
chytnou	chytnout	k5eAaPmIp3nP	chytnout
se	se	k3xPyFc4	se
na	na	k7c4	na
háček	háček	k1gInSc4	háček
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
zapletou	zaplést	k5eAaPmIp3nP	zaplést
do	do	k7c2	do
rybářské	rybářský	k2eAgFnSc2d1	rybářská
sítě	síť	k1gFnSc2	síť
s	s	k7c7	s
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
utonou	utonout	k5eAaPmIp3nP	utonout
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
velkým	velký	k2eAgNnSc7d1	velké
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
albatrosy	albatros	k1gMnPc4	albatros
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
plasty	plast	k1gInPc1	plast
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ptáci	pták	k1gMnPc1	pták
omylem	omylem	k6eAd1	omylem
pozřou	pozřít	k5eAaPmIp3nP	pozřít
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
jim	on	k3xPp3gMnPc3	on
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
trávicí	trávicí	k2eAgFnPc1d1	trávicí
orgány	orgány	k1gFnPc1	orgány
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
hynou	hynout	k5eAaImIp3nP	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečím	bezpečit	k5eNaImIp1nS	bezpečit
ohrožujícím	ohrožující	k2eAgFnPc3d1	ohrožující
velká	velký	k2eAgNnPc1d1	velké
množství	množství	k1gNnPc1	množství
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
ropné	ropný	k2eAgFnPc1d1	ropná
havárie	havárie	k1gFnPc1	havárie
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hnízdišť	hnízdiště	k1gNnPc2	hnízdiště
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
několik	několik	k4yIc1	několik
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
mající	mající	k2eAgInPc4d1	mající
tato	tento	k3xDgNnPc1	tento
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
eliminovat	eliminovat	k5eAaBmF	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
kriticky	kriticky	k6eAd1	kriticky
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
(	(	kIx(	(
<g/>
CR	cr	k0	cr
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
albatros	albatros	k1gMnSc1	albatros
amsterdamský	amsterdamský	k2eAgMnSc1d1	amsterdamský
<g/>
,	,	kIx,	,
albatros	albatros	k1gMnSc1	albatros
galapážský	galapážský	k2eAgMnSc1d1	galapážský
<g/>
,	,	kIx,	,
albatros	albatros	k1gMnSc1	albatros
tristanský	tristanský	k2eAgMnSc1d1	tristanský
a	a	k8xC	a
za	za	k7c4	za
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
(	(	kIx(	(
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
Carterův	Carterův	k2eAgMnSc1d1	Carterův
<g/>
,	,	kIx,	,
albatros	albatros	k1gMnSc1	albatros
černobrvý	černobrvý	k2eAgMnSc1d1	černobrvý
<g/>
,	,	kIx,	,
albatros	albatros	k1gMnSc1	albatros
černonohý	černonohý	k2eAgMnSc1d1	černonohý
<g/>
,	,	kIx,	,
albatros	albatros	k1gMnSc1	albatros
hnědý	hnědý	k2eAgMnSc1d1	hnědý
<g/>
,	,	kIx,	,
albatros	albatros	k1gMnSc1	albatros
pestrozobý	pestrozobý	k2eAgMnSc1d1	pestrozobý
a	a	k8xC	a
albatros	albatros	k1gMnSc1	albatros
Sanfordův	Sanfordův	k2eAgMnSc1d1	Sanfordův
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgInPc1d1	vědecký
názory	názor	k1gInPc1	názor
na	na	k7c4	na
počty	počet	k1gInPc4	počet
rodů	rod	k1gInPc2	rod
a	a	k8xC	a
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
zatřídění	zatřídění	k1gNnPc2	zatřídění
<g/>
:	:	kIx,	:
rod	rod	k1gInSc1	rod
Diomedea	Diomedea	k1gMnSc1	Diomedea
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
albatros	albatros	k1gMnSc1	albatros
amsterdamský	amsterdamský	k2eAgMnSc1d1	amsterdamský
(	(	kIx(	(
<g/>
Diomedea	Diomede	k2eAgFnSc1d1	Diomedea
amsterdamensis	amsterdamensis	k1gFnSc1	amsterdamensis
<g/>
)	)	kIx)	)
Roux	Roux	k1gInSc1	Roux
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
albatros	albatros	k1gMnSc1	albatros
jižní	jižní	k2eAgMnSc1d1	jižní
(	(	kIx(	(
<g/>
Diomedea	Diomedea	k1gMnSc1	Diomedea
<g />
.	.	kIx.	.
</s>
<s>
antipodensis	antipodensis	k1gFnSc1	antipodensis
<g/>
)	)	kIx)	)
Robertson	Robertson	k1gInSc1	Robertson
et	et	k?	et
Warham	Warham	k1gInSc1	Warham
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
albatros	albatros	k1gMnSc1	albatros
tristanský	tristanský	k2eAgMnSc1d1	tristanský
(	(	kIx(	(
<g/>
Diomedea	Diomede	k2eAgFnSc1d1	Diomedea
dabbenena	dabbenena	k1gFnSc1	dabbenena
<g/>
)	)	kIx)	)
Mathews	Mathews	k1gInSc1	Mathews
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
albatros	albatros	k1gMnSc1	albatros
královský	královský	k2eAgMnSc1d1	královský
(	(	kIx(	(
<g/>
Diomedea	Diomede	k2eAgFnSc1d1	Diomedea
epomophora	epomophora	k1gFnSc1	epomophora
<g/>
)	)	kIx)	)
Lesson	Lesson	k1gNnSc1	Lesson
<g/>
,	,	kIx,	,
1825	[number]	k4	1825
albatros	albatros	k1gMnSc1	albatros
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
(	(	kIx(	(
<g/>
Diomedea	Diomedea	k1gFnSc1	Diomedea
exulans	exulans	k1gInSc1	exulans
<g/>
)	)	kIx)	)
Linnaeus	Linnaeus	k1gInSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
albatros	albatros	k1gMnSc1	albatros
Sanfordův	Sanfordův	k2eAgMnSc1d1	Sanfordův
(	(	kIx(	(
<g/>
Diomedea	Diomedea	k1gMnSc1	Diomedea
<g />
.	.	kIx.	.
</s>
<s>
sanfordi	sanford	k1gMnPc1	sanford
<g/>
)	)	kIx)	)
Murphy	Murph	k1gInPc1	Murph
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
rod	rod	k1gInSc1	rod
Phoebastria	Phoebastrium	k1gNnSc2	Phoebastrium
Reichenbach	Reichenbacha	k1gFnPc2	Reichenbacha
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
albatros	albatros	k1gMnSc1	albatros
bělohřbetý	bělohřbetý	k2eAgMnSc1d1	bělohřbetý
(	(	kIx(	(
<g/>
Phoebastria	Phoebastrium	k1gNnSc2	Phoebastrium
albatrus	albatrus	k1gInSc4	albatrus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Pallas	Pallas	k1gMnSc1	Pallas
<g/>
,	,	kIx,	,
1769	[number]	k4	1769
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
laysanský	laysanský	k2eAgMnSc1d1	laysanský
(	(	kIx(	(
Phoebastria	Phoebastrium	k1gNnPc1	Phoebastrium
immutabilis	immutabilis	k1gFnPc2	immutabilis
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Rothschild	Rothschild	k1gMnSc1	Rothschild
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
galapážský	galapážský	k2eAgMnSc1d1	galapážský
(	(	kIx(	(
<g/>
Phoebastria	Phoebastrium	k1gNnPc4	Phoebastrium
irrorata	irrorat	k2eAgNnPc4d1	irrorat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Salvin	Salvina	k1gFnPc2	Salvina
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
albatros	albatros	k1gMnSc1	albatros
černonohý	černonohý	k2eAgMnSc1d1	černonohý
(	(	kIx(	(
<g/>
Phoebastria	Phoebastrium	k1gNnPc1	Phoebastrium
nigripes	nigripesa	k1gFnPc2	nigripesa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Audubon	Audubon	k1gMnSc1	Audubon
<g/>
,	,	kIx,	,
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
Phoebetria	Phoebetrium	k1gNnSc2	Phoebetrium
Reichenbach	Reichenbacha	k1gFnPc2	Reichenbacha
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
albatros	albatros	k1gMnSc1	albatros
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
Phoebetria	Phoebetrium	k1gNnSc2	Phoebetrium
fusca	fusc	k1gInSc2	fusc
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Hilsenberg	Hilsenberg	k1gMnSc1	Hilsenberg
<g/>
,	,	kIx,	,
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
světlohřbetý	světlohřbetý	k2eAgMnSc1d1	světlohřbetý
(	(	kIx(	(
<g/>
Phoebetria	Phoebetrium	k1gNnPc4	Phoebetrium
palpebrata	palpebrat	k2eAgNnPc4d1	palpebrat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Forster	Forster	k1gMnSc1	Forster
<g/>
,	,	kIx,	,
1785	[number]	k4	1785
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
Thalassarche	Thalassarche	k1gNnSc1	Thalassarche
Reichenbach	Reichenbach	k1gInSc1	Reichenbach
<g/>
,	,	kIx,	,
1853	[number]	k4	1853
albatros	albatros	k1gMnSc1	albatros
Bullerův	Bullerův	k2eAgMnSc1d1	Bullerův
(	(	kIx(	(
<g/>
Thalassarche	Thalassarche	k1gInSc1	Thalassarche
bulleri	buller	k1gFnSc2	buller
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Rothschild	Rothschild	k1gMnSc1	Rothschild
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
Carterův	Carterův	k2eAgMnSc1d1	Carterův
(	(	kIx(	(
<g/>
Thalassarche	Thalassarche	k1gInSc1	Thalassarche
carteri	carter	k1gFnSc2	carter
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Rothschild	Rothschild	k1gMnSc1	Rothschild
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
šelfový	šelfový	k2eAgMnSc1d1	šelfový
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Thalassarche	Thalassarch	k1gMnSc4	Thalassarch
cauta	caut	k1gMnSc4	caut
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Gould	Gould	k1gMnSc1	Gould
<g/>
,	,	kIx,	,
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
pestrozobý	pestrozobý	k2eAgMnSc1d1	pestrozobý
(	(	kIx(	(
<g/>
Thalassarche	Thalassarche	k1gInSc1	Thalassarche
chlororhynchos	chlororhynchos	k1gInSc1	chlororhynchos
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
J.	J.	kA	J.
F.	F.	kA	F.
Gmelin	Gmelina	k1gFnPc2	Gmelina
<g/>
,	,	kIx,	,
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
šedohlavý	šedohlavý	k2eAgMnSc1d1	šedohlavý
(	(	kIx(	(
<g/>
Thalassarche	Thalassarch	k1gMnSc4	Thalassarch
chrysostoma	chrysostom	k1gMnSc4	chrysostom
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Forster	Forster	k1gMnSc1	Forster
<g/>
,	,	kIx,	,
1785	[number]	k4	1785
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
chathamský	chathamský	k2eAgMnSc1d1	chathamský
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Thalassarche	Thalassarch	k1gMnSc4	Thalassarch
eremita	eremit	k1gMnSc4	eremit
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Murphy	Murph	k1gInPc1	Murph
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
campbellský	campbellský	k2eAgMnSc1d1	campbellský
(	(	kIx(	(
<g/>
Thalassarche	Thalassarch	k1gMnSc4	Thalassarch
impavida	impavid	k1gMnSc4	impavid
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Mathews	Mathews	k1gInSc1	Mathews
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
černobrvý	černobrvý	k2eAgMnSc1d1	černobrvý
(	(	kIx(	(
<g/>
Thalassarche	Thalassarche	k1gInSc1	Thalassarche
melanophris	melanophris	k1gFnSc2	melanophris
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Temminck	Temminck	k1gMnSc1	Temminck
<g/>
,	,	kIx,	,
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
albatros	albatros	k1gMnSc1	albatros
snarský	snarský	k2eAgMnSc1d1	snarský
(	(	kIx(	(
<g/>
Thalassarche	Thalassarch	k1gMnPc4	Thalassarch
salvini	salvin	k2eAgMnPc1d1	salvin
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Rothschild	Rothschild	k1gMnSc1	Rothschild
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
Thalassarche	Thalassarch	k1gFnPc1	Thalassarch
steadi	stead	k1gMnPc1	stead
Falla	Falla	k1gMnSc1	Falla
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
</s>
