<s>
Čeleď	čeleď	k1gFnSc1	čeleď
albatrosovití	albatrosovitý	k2eAgMnPc1d1	albatrosovitý
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
čtyřmi	čtyři	k4xCgInPc7	čtyři
recentními	recentní	k2eAgInPc7d1	recentní
rody	rod	k1gInPc7	rod
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgNnSc4d1	stejné
české	český	k2eAgNnSc4d1	české
jméno	jméno	k1gNnSc4	jméno
albatros	albatros	k1gMnSc1	albatros
<g/>
.	.	kIx.	.
</s>
