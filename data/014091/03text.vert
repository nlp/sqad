<s>
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
</s>
<s>
Dánská	dánský	k2eAgFnSc1d1
korunaDanske	korunaDanske	k1gFnSc1
kroner	kronra	k1gFnPc2
(	(	kIx(
<g/>
dánsky	dánsky	k6eAd1
<g/>
)	)	kIx)
mince	mince	k1gFnSc1
5	#num#	k4
korunZemě	korunZemě	k6eAd1
</s>
<s>
Dánsko	Dánsko	k1gNnSc1
DánskoFaerské	DánskoFaerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Faerské	Faerský	k2eAgInPc1d1
ostrovyGrónsko	ostrovyGrónsko	k6eAd1
Grónsko	Grónsko	k1gNnSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
DKK	DKK	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
1,1	1,1	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
;	;	kIx,
pouze	pouze	k6eAd1
Dánsko	Dánsko	k1gNnSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
kr.	kr.	k?
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
ø	ø	k1gMnSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
50	#num#	k4
ø	ø	k1gInSc5
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
korun	koruna	k1gFnPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
korun	koruna	k1gFnPc2
</s>
<s>
vývoj	vývoj	k1gInSc1
kurzu	kurz	k1gInSc2
dánské	dánský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
a	a	k8xC
eura	euro	k1gNnSc2
ve	v	k7c6
fluktuačním	fluktuační	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
dle	dle	k7c2
ERM	ERM	kA
II	II	kA
</s>
<s>
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
je	být	k5eAaImIp3nS
měna	měna	k1gFnSc1
Dánského	dánský	k2eAgNnSc2d1
království	království	k1gNnSc2
-	-	kIx~
tzn	tzn	kA
Dánska	Dánsko	k1gNnSc2
<g/>
,	,	kIx,
Faerských	Faerský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
a	a	k8xC
Grónska	Grónsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
koruna	koruna	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
100	#num#	k4
ø	ø	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
kódové	kódový	k2eAgNnSc1d1
označení	označení	k1gNnSc1
podle	podle	k7c2
normy	norma	k1gFnSc2
ISO	ISO	kA
4217	#num#	k4
je	být	k5eAaImIp3nS
DKK	DKK	kA
<g/>
,	,	kIx,
domácí	domácí	k2eAgFnSc1d1
zkratkou	zkratka	k1gFnSc7
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
„	„	kIx"
<g/>
kr.	kr.	kA
<g/>
“	“	kIx"
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1999	#num#	k4
je	být	k5eAaImIp3nS
zapojena	zapojit	k5eAaPmNgFnS
do	do	k7c2
ERM	ERM	kA
II	II	kA
s	s	k7c7
centrální	centrální	k2eAgFnSc7d1
paritou	parita	k1gFnSc7
1	#num#	k4
EUR	euro	k1gNnPc2
=	=	kIx~
7,460	7,460	k4
<g/>
38	#num#	k4
DKK	DKK	kA
a	a	k8xC
úzkým	úzký	k2eAgNnSc7d1
fluktuačním	fluktuační	k2eAgNnSc7d1
pásmem	pásmo	k1gNnSc7
±	±	k?
2,25	2,25	k4
%	%	kIx~
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
má	mít	k5eAaImIp3nS
Dánsko	Dánsko	k1gNnSc1
zajištěný	zajištěný	k2eAgInSc4d1
tzv.	tzv.	kA
opt-out	opt-out	k5eAaImF,k5eAaPmF
a	a	k8xC
není	být	k5eNaImIp3nS
povinnováno	povinnován	k2eAgNnSc1d1
zavést	zavést	k5eAaPmF
euro	euro	k1gNnSc4
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
referendum	referendum	k1gNnSc1
o	o	k7c4
přistoupení	přistoupení	k1gNnSc4
k	k	k7c3
euru	euro	k1gNnSc3
<g/>
,	,	kIx,
při	při	k7c6
87,6	87,6	k4
%	%	kIx~
účastí	účastí	k1gNnSc1
bylo	být	k5eAaImAgNnS
46,8	46,8	k4
%	%	kIx~
voličů	volič	k1gInPc2
pro	pro	k7c4
a	a	k8xC
53.2	53.2	k4
%	%	kIx~
proti	proti	k7c3
<g/>
,	,	kIx,
přistoupení	přistoupený	k2eAgMnPc1d1
k	k	k7c3
euru	euro	k1gNnSc3
tedy	tedy	k9
bylo	být	k5eAaImAgNnS
zamítnuto	zamítnut	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1873	#num#	k4
a	a	k8xC
1914	#num#	k4
byla	být	k5eAaImAgFnS
mezi	mezi	k7c7
Švédskem	Švédsko	k1gNnSc7
(	(	kIx(
<g/>
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
unii	unie	k1gFnSc6
s	s	k7c7
Norskem	Norsko	k1gNnSc7
<g/>
)	)	kIx)
a	a	k8xC
Dánskem	Dánsko	k1gNnSc7
(	(	kIx(
<g/>
jehož	jehož	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
byly	být	k5eAaImAgFnP
Island	Island	k1gInSc4
<g/>
,	,	kIx,
Faerské	Faerský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
a	a	k8xC
Grónsko	Grónsko	k1gNnSc4
<g/>
)	)	kIx)
Skandinávská	skandinávský	k2eAgFnSc1d1
měnová	měnový	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
se	se	k3xPyFc4
ve	v	k7c6
všech	všecek	k3xTgInPc6
zmíněných	zmíněný	k2eAgInPc6d1
státech	stát	k1gInPc6
platilo	platit	k5eAaImAgNnS
jednotnou	jednotný	k2eAgFnSc7d1
měnou-	měnou-	k?
korunou	koruna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
unie	unie	k1gFnSc2
začal	začít	k5eAaPmAgMnS
každý	každý	k3xTgMnSc1
stát	stát	k5eAaPmF,k5eAaImF
používat	používat	k5eAaImF
vlastní	vlastní	k2eAgFnSc2d1
nezávislé	závislý	k2eNgFnSc2d1
měny	měna	k1gFnSc2
odvozené	odvozený	k2eAgFnSc2d1
od	od	k7c2
zaniklé	zaniklý	k2eAgFnSc2d1
společné	společný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
nástupnické	nástupnický	k2eAgFnPc4d1
měny	měna	k1gFnPc4
si	se	k3xPyFc3
ale	ale	k9
zachovaly	zachovat	k5eAaPmAgFnP
stejný	stejný	k2eAgInSc4d1
název	název	k1gInSc4
„	„	k?
<g/>
koruna	koruna	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
státech	stát	k1gInPc6
přizpůsobil	přizpůsobit	k5eAaPmAgMnS
místní	místní	k2eAgFnSc2d1
výslovnosti	výslovnost	k1gFnSc2
a	a	k8xC
pravopisu	pravopis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
vznikem	vznik	k1gInSc7
koruny	koruna	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
Dánsku	Dánsko	k1gNnSc6
používal	používat	k5eAaImAgMnS
rigsdaler	rigsdaler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Mince	mince	k1gFnPc1
mají	mít	k5eAaImIp3nP
hodnotu	hodnota	k1gFnSc4
50	#num#	k4
ø	ø	k1gInSc5
a	a	k8xC
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
HodnotaLícRub	HodnotaLícRub	k1gMnSc1
</s>
<s>
50	#num#	k4
ø	ø	k1gFnSc1
krále	král	k1gMnSc4
Kristiána	Kristián	k1gMnSc4
V.	V.	kA
<g/>
Srdce	srdce	k1gNnSc1
(	(	kIx(
<g/>
symbol	symbol	k1gInSc1
Královské	královský	k2eAgFnSc2d1
mincovny	mincovna	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
korunaMonogram	korunaMonogram	k1gInSc1
královny	královna	k1gFnSc2
Markéty	Markéta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
Tradiční	tradiční	k2eAgInSc1d1
design	design	k1gInSc1
(	(	kIx(
<g/>
děrovaný	děrovaný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
korunyMonogram	korunyMonogram	k1gInSc1
královny	královna	k1gFnSc2
Markéty	Markéta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
Tradiční	tradiční	k2eAgInSc1d1
design	design	k1gInSc1
(	(	kIx(
<g/>
děrovaný	děrovaný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
korunMonogram	korunMonogram	k1gInSc1
královny	královna	k1gFnSc2
Markéty	Markéta	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
Tradiční	tradiční	k2eAgInSc1d1
design	design	k1gInSc1
(	(	kIx(
<g/>
děrovaný	děrovaný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
korunKrálovna	korunKrálovna	k1gFnSc1
Markéta	Markéta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Dánska	Dánsko	k1gNnSc2
</s>
<s>
20	#num#	k4
korunKrálovna	korunKrálovna	k1gFnSc1
Markéta	Markéta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Dánska	Dánsko	k1gNnSc2
</s>
<s>
Bankovky	bankovka	k1gFnPc1
jsou	být	k5eAaImIp3nP
vydávány	vydávat	k5eAaPmNgFnP,k5eAaImNgFnP
v	v	k7c6
nominálních	nominální	k2eAgFnPc6d1
hodnotách	hodnota	k1gFnPc6
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
byla	být	k5eAaImAgFnS
zaváděna	zaváděn	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
série	série	k1gFnSc1
bankovek	bankovka	k1gFnPc2
s	s	k7c7
motivem	motiv	k1gInSc7
mostů	most	k1gInPc2
na	na	k7c6
líci	líc	k1gFnSc6
a	a	k8xC
archeologických	archeologický	k2eAgInPc2d1
nálezů	nález	k1gInPc2
na	na	k7c6
rubu	rub	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bankovky	bankovka	k1gFnPc1
Dánské	dánský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
,	,	kIx,
série	série	k1gFnSc1
2009	#num#	k4
</s>
<s>
VyobrazeníHodnotaRozměryBarvaPopis	VyobrazeníHodnotaRozměryBarvaPopis	k1gInSc1
</s>
<s>
LícRubLícRubUvedení	LícRubLícRubUvedení	k1gNnSc1
do	do	k7c2
oběhu	oběh	k1gInSc2
</s>
<s>
50	#num#	k4
kr.	kr.	k?
</s>
<s>
125	#num#	k4
×	×	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
Fialová	Fialová	k1gFnSc1
</s>
<s>
Most	most	k1gInSc1
Sallingsung	Sallingsunga	k1gFnPc2
</s>
<s>
nádoba	nádoba	k1gFnSc1
z	z	k7c2
doby	doba	k1gFnSc2
kamenné	kamenný	k2eAgFnSc2d1
nalezená	nalezený	k2eAgFnSc1d1
nedaleko	nedaleko	k7c2
Skarpsallingu	Skarpsalling	k1gInSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
</s>
<s>
100	#num#	k4
kr.	kr.	k?
</s>
<s>
135	#num#	k4
×	×	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
Oranžovo-žlutá	Oranžovo-žlutý	k2eAgFnSc1d1
</s>
<s>
Starý	starý	k2eAgInSc1d1
most	most	k1gInSc1
přes	přes	k7c4
Malý	malý	k2eAgInSc4d1
Belt	Belt	k1gInSc4
</s>
<s>
dýka	dýka	k1gFnSc1
z	z	k7c2
doby	doba	k1gFnSc2
kamenné	kamenný	k2eAgFnSc2d1
nalezená	nalezený	k2eAgFnSc1d1
nedaleko	nedaleko	k7c2
Hindsgavlu	Hindsgavl	k1gInSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2010	#num#	k4
</s>
<s>
200	#num#	k4
kr.	kr.	k?
</s>
<s>
145	#num#	k4
×	×	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Knippelsbro	Knippelsbro	k6eAd1
</s>
<s>
opasková	opaskový	k2eAgFnSc1d1
spona	spona	k1gFnSc1
z	z	k7c2
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgFnSc2d1
nalezená	nalezený	k2eAgFnSc1d1
nedaleko	nedaleko	k7c2
Langstrupu	Langstrup	k1gInSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
</s>
<s>
500	#num#	k4
kr.	kr.	k?
</s>
<s>
155	#num#	k4
×	×	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Most	most	k1gInSc1
královny	královna	k1gFnSc2
Alexandriny	alexandrin	k1gInPc5
</s>
<s>
nádoba	nádoba	k1gFnSc1
z	z	k7c2
4	#num#	k4
<g/>
.	.	kIx.
či	či	k8xC
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
nalezená	nalezený	k2eAgFnSc1d1
nedaleko	daleko	k6eNd1
Keldby	Keldb	k1gInPc7
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
</s>
<s>
1000	#num#	k4
kr.	kr.	k?
</s>
<s>
165	#num#	k4
×	×	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
Červená	červený	k2eAgFnSc1d1
</s>
<s>
Most	most	k1gInSc1
přes	přes	k7c4
Velký	velký	k2eAgInSc4d1
Belt	Belt	k1gInSc4
</s>
<s>
Trundholmský	Trundholmský	k2eAgInSc1d1
sluneční	sluneční	k2eAgInSc1d1
vozík	vozík	k1gInSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
</s>
<s>
Bankovky	bankovka	k1gFnPc1
Faerské	Faerský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
používají	používat	k5eAaImIp3nP
kromě	kromě	k7c2
dánských	dánský	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
i	i	k8xC
bankovky	bankovka	k1gFnPc4
s	s	k7c7
vlastními	vlastní	k2eAgInPc7d1
motivy	motiv	k1gInPc7
svého	svůj	k3xOyFgNnSc2
přírodního	přírodní	k2eAgNnSc2d1
bohatství	bohatství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
je	být	k5eAaImIp3nS
jen	jen	k9
lokální	lokální	k2eAgFnSc7d1
variantou	varianta	k1gFnSc7
dánské	dánský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
s	s	k7c7
ní	on	k3xPp3gFnSc7
pevně	pevně	k6eAd1
svázána	svázat	k5eAaPmNgFnS
v	v	k7c6
poměru	poměr	k1gInSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
-	-	kIx~
The	The	k1gFnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Danish	Danish	k1gInSc1
coins	coins	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Danmarks	Danmarks	k1gInSc1
Nationalbank	Nationalbank	k1gInSc4
-	-	kIx~
Dánská	dánský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Danish	Danish	k1gMnSc1
banknotes	banknotes	k1gMnSc1
2009	#num#	k4
series	series	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Danmarks	Danmarks	k1gInSc1
Nationalbank	Nationalbank	k1gInSc4
-	-	kIx~
Dánská	dánský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Faroese	Faroese	k1gFnSc1
banknotes	banknotes	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Danmarks	Danmarks	k1gInSc1
Nationalbank	Nationalbank	k1gInSc4
-	-	kIx~
Dánská	dánský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dánské	dánský	k2eAgInPc1d1
euromince	eurominec	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Aktuální	aktuální	k2eAgInSc1d1
kurz	kurz	k1gInSc1
měny	měna	k1gFnSc2
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Podle	podle	k7c2
ČNB	ČNB	kA
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
Podle	podle	k7c2
Google	Google	k1gFnSc2
Finance	finance	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
(	(	kIx(
<g/>
Graf	graf	k1gInSc1
Banky	banka	k1gFnSc2
<g/>
)	)	kIx)
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Yahoo	Yahoo	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Finance	finance	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
</s>
<s>
Měny	měna	k1gFnPc1
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Dánsko	Dánsko	k1gNnSc1
</s>
