<p>
<s>
Boleradice	Boleradice	k1gFnSc1	Boleradice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Polehraditz	Polehraditz	k1gInSc1	Polehraditz
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
městys	městys	k1gInSc4	městys
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Břeclav	Břeclav	k1gFnSc1	Břeclav
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
6	[number]	k4	6
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Hustopečí	Hustopeč	k1gFnPc2	Hustopeč
na	na	k7c6	na
potoku	potok	k1gInSc6	potok
Haraska	haraska	k1gFnSc1	haraska
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
906	[number]	k4	906
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
od	od	k7c2	od
varianty	varianta	k1gFnSc2	varianta
Boleradicih	Boleradicih	k1gInSc1	Boleradicih
(	(	kIx(	(
<g/>
1131	[number]	k4	1131
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
de	de	k?	de
Boleradiz	Boleradiz	k1gInSc1	Boleradiz
(	(	kIx(	(
<g/>
1235	[number]	k4	1235
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
de	de	k?	de
Bolehradicz	Bolehradicz	k1gInSc1	Bolehradicz
(	(	kIx(	(
<g/>
1255	[number]	k4	1255
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
de	de	k?	de
Poleradiz	Poleradiz	k1gInSc1	Poleradiz
(	(	kIx(	(
<g/>
1271	[number]	k4	1271
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
de	de	k?	de
Polehradicz	Polehradicz	k1gInSc1	Polehradicz
(	(	kIx(	(
<g/>
1289	[number]	k4	1289
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
de	de	k?	de
Polerdiz	Polerdiz	k1gInSc1	Polerdiz
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1298	[number]	k4	1298
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
de	de	k?	de
Polheradicz	Polheradicz	k1gInSc1	Polheradicz
(	(	kIx(	(
<g/>
1365	[number]	k4	1365
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Boleradicz	Boleradicz	k1gMnSc1	Boleradicz
(	(	kIx(	(
<g/>
1373	[number]	k4	1373
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pellertitz	Pellertitz	k1gMnSc1	Pellertitz
(	(	kIx(	(
<g/>
1674	[number]	k4	1674
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pollehraditz	Pollehraditz	k1gMnSc1	Pollehraditz
(	(	kIx(	(
<g/>
1718	[number]	k4	1718
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polehraditz	Polehraditz	k1gInSc1	Polehraditz
<g/>
,	,	kIx,	,
Polehradice	Polehradice	k1gFnSc1	Polehradice
a	a	k8xC	a
Boleradice	Boleradice	k1gFnSc1	Boleradice
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polehradice	Polehradice	k1gFnSc1	Polehradice
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polehradice	Polehradice	k1gFnSc1	Polehradice
t.	t.	k?	t.
Boleradice	Boleradice	k1gFnSc1	Boleradice
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
až	až	k6eAd1	až
k	k	k7c3	k
podobě	podoba	k1gFnSc3	podoba
Boleradice	Boleradice	k1gFnSc2	Boleradice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc1d1	místní
jméno	jméno	k1gNnSc1	jméno
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
osobního	osobní	k2eAgNnSc2d1	osobní
jména	jméno	k1gNnSc2	jméno
Bolerad	Bolerada	k1gFnPc2	Bolerada
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
byla	být	k5eAaImAgFnS	být
připojena	připojen	k2eAgFnSc1d1	připojena
přípona	přípona	k1gFnSc1	přípona
-ice	cat	k5eAaPmIp3nS	-icat
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ves	ves	k1gFnSc4	ves
lidí	člověk	k1gMnPc2	člověk
Boleradových	Boleradový	k2eAgMnPc2d1	Boleradový
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
variantě	varianta	k1gFnSc3	varianta
Bolehradice	Bolehradice	k1gFnSc1	Bolehradice
byla	být	k5eAaImAgFnS	být
nářečně	nářečně	k6eAd1	nářečně
přidávaná	přidávaný	k2eAgFnSc1d1	přidávaná
hláska	hláska	k1gFnSc1	hláska
h.	h.	k?	h.
Německá	německý	k2eAgFnSc1d1	německá
varianta	varianta	k1gFnSc1	varianta
kolísala	kolísat	k5eAaImAgFnS	kolísat
mezi	mezi	k7c7	mezi
znělou	znělý	k2eAgFnSc7d1	znělá
a	a	k8xC	a
neznělou	znělý	k2eNgFnSc7d1	neznělá
variantou	varianta	k1gFnSc7	varianta
P	P	kA	P
<g/>
/	/	kIx~	/
<g/>
Bolehradice	Bolehradice	k1gFnSc1	Bolehradice
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
je	být	k5eAaImIp3nS	být
rodu	rod	k1gInSc2	rod
ženského	ženský	k2eAgInSc2d1	ženský
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
pomnožného	pomnožný	k2eAgNnSc2d1	pomnožné
a	a	k8xC	a
genitiv	genitiv	k1gInSc1	genitiv
zní	znět	k5eAaImIp3nS	znět
Boleradic	Boleradic	k1gMnSc1	Boleradic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Archeologický	archeologický	k2eAgInSc1d1	archeologický
průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
staroslovanské	staroslovanský	k2eAgNnSc1d1	staroslovanské
pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
z	z	k7c2	z
období	období	k1gNnSc2	období
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
soupisu	soupis	k1gInSc6	soupis
majetku	majetek	k1gInSc2	majetek
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
biskupství	biskupství	k1gNnSc2	biskupství
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1141	[number]	k4	1141
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Boleradicích	Boleradice	k1gFnPc6	Boleradice
patřilo	patřit	k5eAaImAgNnS	patřit
5	[number]	k4	5
lánů	lán	k1gInPc2	lán
polí	pole	k1gFnSc7	pole
břeclavskému	břeclavský	k2eAgInSc3d1	břeclavský
hradskému	hradský	k2eAgInSc3d1	hradský
kostelu	kostel	k1gInSc3	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1235	[number]	k4	1235
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
uveden	uveden	k2eAgInSc1d1	uveden
Lev	lev	k1gInSc1	lev
z	z	k7c2	z
Boleradice	Boleradice	k1gFnSc2	Boleradice
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1310	[number]	k4	1310
je	být	k5eAaImIp3nS	být
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
Ota	Ota	k1gMnSc1	Ota
z	z	k7c2	z
Boleradic	Boleradice	k1gFnPc2	Boleradice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
bolaradickém	bolaradický	k2eAgInSc6d1	bolaradický
hradu	hrad	k1gInSc6	hrad
pak	pak	k6eAd1	pak
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1373	[number]	k4	1373
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1531	[number]	k4	1531
až	až	k9	až
1535	[number]	k4	1535
byl	být	k5eAaImAgInS	být
však	však	k9	však
opuštěn	opustit	k5eAaPmNgInS	opustit
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
pánech	pan	k1gMnPc6	pan
z	z	k7c2	z
Boleradic	Boleradice	k1gFnPc2	Boleradice
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
vesnici	vesnice	k1gFnSc4	vesnice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1358	[number]	k4	1358
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ji	on	k3xPp3gFnSc4	on
roku	rok	k1gInSc2	rok
1530	[number]	k4	1530
prodali	prodat	k5eAaPmAgMnP	prodat
pánům	pan	k1gMnPc3	pan
z	z	k7c2	z
Víckova	Víckov	k1gInSc2	Víckov
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1512	[number]	k4	1512
byly	být	k5eAaImAgFnP	být
Boleradice	Boleradice	k1gFnPc1	Boleradice
povýšeny	povýšen	k2eAgFnPc1d1	povýšena
na	na	k7c4	na
městečko	městečko	k1gNnSc4	městečko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
o	o	k7c4	o
čtvrt	čtvrt	k1gFnSc4	čtvrt
století	století	k1gNnSc2	století
později	pozdě	k6eAd2	pozdě
stvrdil	stvrdit	k5eAaPmAgMnS	stvrdit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
udělením	udělení	k1gNnSc7	udělení
práva	právo	k1gNnSc2	právo
tržního	tržní	k2eAgNnSc2d1	tržní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1535	[number]	k4	1535
byla	být	k5eAaImAgFnS	být
povolena	povolit	k5eAaPmNgFnS	povolit
stavba	stavba	k1gFnSc1	stavba
zdejší	zdejší	k2eAgFnSc2d1	zdejší
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1607	[number]	k4	1607
zřízena	zřízen	k2eAgFnSc1d1	zřízena
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
pečeť	pečeť	k1gFnSc1	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
horenské	horenský	k2eAgNnSc1d1	Horenské
právo	právo	k1gNnSc1	právo
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
výsadby	výsadba	k1gFnSc2	výsadba
vinic	vinice	k1gFnPc2	vinice
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
rybníky	rybník	k1gInPc1	rybník
a	a	k8xC	a
mlýny	mlýn	k1gInPc1	mlýn
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1622	[number]	k4	1622
byl	být	k5eAaImAgInS	být
však	však	k9	však
Janu	Jan	k1gMnSc3	Jan
Adamovi	Adam	k1gMnSc3	Adam
z	z	k7c2	z
Víckova	Víckov	k1gInSc2	Víckov
konfiskován	konfiskován	k2eAgInSc4d1	konfiskován
majetek	majetek	k1gInSc4	majetek
za	za	k7c4	za
jeho	on	k3xPp3gInSc4	on
protihabsburský	protihabsburský	k2eAgInSc4d1	protihabsburský
odboj	odboj	k1gInSc4	odboj
a	a	k8xC	a
městys	městys	k1gInSc4	městys
získali	získat	k5eAaPmAgMnP	získat
brněnští	brněnský	k2eAgMnPc1d1	brněnský
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zrušení	zrušení	k1gNnSc4	zrušení
jejich	jejich	k3xOp3gInSc2	jejich
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1773	[number]	k4	1773
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
držení	držení	k1gNnSc2	držení
náboženského	náboženský	k2eAgInSc2d1	náboženský
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
650	[number]	k4	650
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
200	[number]	k4	200
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obydlených	obydlený	k2eAgInPc2d1	obydlený
domů	dům	k1gInPc2	dům
ze	z	k7c2	z
120	[number]	k4	120
na	na	k7c4	na
33	[number]	k4	33
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
obec	obec	k1gFnSc1	obec
dlouho	dlouho	k6eAd1	dlouho
vzpamatovávala	vzpamatovávat	k5eAaImAgFnS	vzpamatovávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1750	[number]	k4	1750
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poddanskému	poddanský	k2eAgNnSc3d1	poddanské
vzbouření	vzbouření	k1gNnSc3	vzbouření
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
ubylo	ubýt	k5eAaPmAgNnS	ubýt
51	[number]	k4	51
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
až	až	k9	až
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
necelé	celý	k2eNgFnSc3d1	necelá
tisícovce	tisícovka	k1gFnSc3	tisícovka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obec	obec	k1gFnSc1	obec
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
hustopečskému	hustopečský	k2eAgNnSc3d1	Hustopečské
hejtmanství	hejtmanství	k1gNnSc3	hejtmanství
a	a	k8xC	a
ke	k	k7c3	k
klobouckému	kloboucký	k2eAgInSc3d1	kloboucký
soudnímu	soudní	k2eAgInSc3d1	soudní
okresu	okres	k1gInSc3	okres
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
spolky	spolek	k1gInPc1	spolek
jako	jako	k8xC	jako
čtenářský	čtenářský	k2eAgInSc1d1	čtenářský
Hrad	hrad	k1gInSc1	hrad
Polehradský	Polehradský	k2eAgInSc1d1	Polehradský
<g/>
,	,	kIx,	,
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
pátera	páter	k1gMnSc2	páter
Filipa	Filip	k1gMnSc2	Filip
Toufara	Toufar	k1gMnSc2	Toufar
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
pěvecká	pěvecký	k2eAgFnSc1d1	pěvecká
Cyrilská	cyrilský	k2eAgFnSc1d1	Cyrilská
jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
potravní	potravní	k2eAgInSc1d1	potravní
Blahobyt	blahobyt	k1gInSc1	blahobyt
<g/>
,	,	kIx,	,
veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
hasičský	hasičský	k2eAgInSc1d1	hasičský
spolek	spolek	k1gInSc1	spolek
založil	založit	k5eAaPmAgInS	založit
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
učitel	učitel	k1gMnSc1	učitel
Cyril	Cyril	k1gMnSc1	Cyril
Hladký	Hladký	k1gMnSc1	Hladký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
bylo	být	k5eAaImAgNnS	být
sehráno	sehrát	k5eAaPmNgNnS	sehrát
první	první	k4xOgNnSc4	první
ochotnické	ochotnický	k2eAgNnSc4d1	ochotnické
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přibyly	přibýt	k5eAaPmAgFnP	přibýt
pobočky	pobočka	k1gFnPc1	pobočka
Omladiny	Omladina	k1gFnSc2	Omladina
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
,	,	kIx,	,
Orla	Orel	k1gMnSc2	Orel
<g/>
,	,	kIx,	,
včelařského	včelařský	k2eAgInSc2d1	včelařský
spolku	spolek	k1gInSc2	spolek
<g/>
...	...	k?	...
Za	za	k7c2	za
První	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
četnická	četnický	k2eAgFnSc1d1	četnická
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
hasičská	hasičský	k2eAgFnSc1d1	hasičská
zbrojnice	zbrojnice	k1gFnSc1	zbrojnice
<g/>
,	,	kIx,	,
Lidový	lidový	k2eAgInSc1d1	lidový
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
hrála	hrát	k5eAaImAgNnP	hrát
se	se	k3xPyFc4	se
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
a	a	k8xC	a
pořádaly	pořádat	k5eAaImAgInP	pořádat
plesy	ples	k1gInPc1	ples
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
obce	obec	k1gFnSc2	obec
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
následovalo	následovat	k5eAaImAgNnS	následovat
zřízení	zřízení	k1gNnSc1	zřízení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
střediska	středisko	k1gNnSc2	středisko
<g/>
,	,	kIx,	,
dětského	dětský	k2eAgInSc2d1	dětský
útulku	útulek	k1gInSc2	útulek
či	či	k8xC	či
oprava	oprava	k1gFnSc1	oprava
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k9	však
rozvoj	rozvoj	k1gInSc4	rozvoj
obce	obec	k1gFnSc2	obec
stagnoval	stagnovat	k5eAaImAgMnS	stagnovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
obracet	obracet	k5eAaImF	obracet
až	až	k9	až
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akci	akce	k1gFnSc6	akce
Z	z	k7c2	z
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
pohostinství	pohostinství	k1gNnSc2	pohostinství
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavěn	k2eAgNnSc1d1	vystavěno
kino	kino	k1gNnSc1	kino
a	a	k8xC	a
víceúčelový	víceúčelový	k2eAgInSc1d1	víceúčelový
společenský	společenský	k2eAgInSc1d1	společenský
sál	sál	k1gInSc1	sál
či	či	k8xC	či
nákupní	nákupní	k2eAgNnSc1d1	nákupní
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
a	a	k8xC	a
modernizována	modernizovat	k5eAaBmNgFnS	modernizovat
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
plynofikována	plynofikovat	k5eAaImNgFnS	plynofikovat
<g/>
,	,	kIx,	,
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
byl	být	k5eAaImAgInS	být
vodovod	vodovod	k1gInSc1	vodovod
<g/>
,	,	kIx,	,
obnoven	obnoven	k2eAgInSc1d1	obnoven
rybník	rybník	k1gInSc1	rybník
a	a	k8xC	a
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
obci	obec	k1gFnSc6	obec
vrácen	vrátit	k5eAaPmNgInS	vrátit
status	status	k1gInSc1	status
městyse	městys	k1gInSc2	městys
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
žilo	žít	k5eAaImAgNnS	žít
celkem	celek	k1gInSc7	celek
882	[number]	k4	882
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
425	[number]	k4	425
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
457	[number]	k4	457
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
41,6	[number]	k4	41,6
<g/>
%	%	kIx~	%
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
provedeném	provedený	k2eAgInSc6d1	provedený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
žilo	žít	k5eAaImAgNnS	žít
860	[number]	k4	860
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
bylo	být	k5eAaImAgNnS	být
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
od	od	k7c2	od
30	[number]	k4	30
do	do	k7c2	do
39	[number]	k4	39
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
do	do	k7c2	do
14	[number]	k4	14
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
tvořily	tvořit	k5eAaImAgFnP	tvořit
14,5	[number]	k4	14,5
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
senioři	senior	k1gMnPc1	senior
nad	nad	k7c4	nad
70	[number]	k4	70
let	léto	k1gNnPc2	léto
úhrnem	úhrnem	k6eAd1	úhrnem
6,6	[number]	k4	6,6
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
735	[number]	k4	735
občanů	občan	k1gMnPc2	občan
obce	obec	k1gFnSc2	obec
starších	starší	k1gMnPc2	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
mělo	mít	k5eAaImAgNnS	mít
vzdělání	vzdělání	k1gNnSc1	vzdělání
37,7	[number]	k4	37,7
<g/>
%	%	kIx~	%
střední	střední	k2eAgNnSc1d1	střední
vč.	vč.	k?	vč.
vyučení	vyučení	k1gNnSc1	vyučení
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
maturity	maturita	k1gFnSc2	maturita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
vysokoškoláků	vysokoškolák	k1gMnPc2	vysokoškolák
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
6,9	[number]	k4	6,9
<g/>
%	%	kIx~	%
a	a	k8xC	a
bez	bez	k7c2	bez
vzdělání	vzdělání	k1gNnSc2	vzdělání
bylo	být	k5eAaImAgNnS	být
naopak	naopak	k6eAd1	naopak
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cenzu	cenzus	k1gInSc2	cenzus
dále	daleko	k6eAd2	daleko
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
410	[number]	k4	410
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
87,6	[number]	k4	87,6
<g/>
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
řadilo	řadit	k5eAaImAgNnS	řadit
mezi	mezi	k7c4	mezi
zaměstnané	zaměstnaný	k1gMnPc4	zaměstnaný
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
67,8	[number]	k4	67,8
<g/>
%	%	kIx~	%
patřilo	patřit	k5eAaImAgNnS	patřit
mezi	mezi	k7c4	mezi
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
<g/>
,	,	kIx,	,
2,7	[number]	k4	2,7
<g/>
%	%	kIx~	%
k	k	k7c3	k
zaměstnavatelům	zaměstnavatel	k1gMnPc3	zaměstnavatel
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
vlastní	vlastní	k2eAgInSc4d1	vlastní
účet	účet	k1gInSc4	účet
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
celých	celý	k2eAgNnPc2d1	celé
48	[number]	k4	48
<g/>
%	%	kIx~	%
občanů	občan	k1gMnPc2	občan
nebylo	být	k5eNaImAgNnS	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
nepracující	pracující	k2eNgMnPc1d1	nepracující
důchodci	důchodce	k1gMnPc1	důchodce
či	či	k8xC	či
žáci	žák	k1gMnPc1	žák
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
nebo	nebo	k8xC	nebo
učni	učeň	k1gMnPc1	učeň
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
svou	svůj	k3xOyFgFnSc4	svůj
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
aktivitu	aktivita	k1gFnSc4	aktivita
uvést	uvést	k5eAaPmF	uvést
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Úhrnem	úhrnem	k6eAd1	úhrnem
286	[number]	k4	286
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
33,3	[number]	k4	33,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
311	[number]	k4	311
obyvatel	obyvatel	k1gMnPc2	obyvatel
bylo	být	k5eAaImAgNnS	být
Moravanů	Moravan	k1gMnPc2	Moravan
a	a	k8xC	a
3	[number]	k4	3
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
.	.	kIx.	.
</s>
<s>
Celých	celý	k2eAgMnPc2d1	celý
213	[number]	k4	213
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
však	však	k9	však
svou	svůj	k3xOyFgFnSc4	svůj
národnost	národnost	k1gFnSc4	národnost
neuvedlo	uvést	k5eNaPmAgNnS	uvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
obec	obec	k1gFnSc4	obec
i	i	k9	i
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
uvádí	uvádět	k5eAaImIp3nS	uvádět
tabulka	tabulka	k1gFnSc1	tabulka
níže	níže	k1gFnSc1	níže
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
i	i	k9	i
příslušnost	příslušnost	k1gFnSc4	příslušnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
či	či	k8xC	či
následné	následný	k2eAgNnSc4d1	následné
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženský	náboženský	k2eAgInSc4d1	náboženský
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
farnosti	farnost	k1gFnSc2	farnost
Boleradice	Boleradice	k1gFnSc2	Boleradice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
děkanátu	děkanát	k1gInSc2	děkanát
Hustpopeče	Hustpopeč	k1gInSc2	Hustpopeč
–	–	k?	–
Brněnské	brněnský	k2eAgFnSc3d1	brněnská
dicéze	dicéza	k1gFnSc3	dicéza
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
provincii	provincie	k1gFnSc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
censu	census	k1gInSc6	census
prováděném	prováděný	k2eAgInSc6d1	prováděný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
370	[number]	k4	370
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
43	[number]	k4	43
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
označilo	označit	k5eAaPmAgNnS	označit
za	za	k7c4	za
věřící	věřící	k1gFnSc4	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
se	se	k3xPyFc4	se
327	[number]	k4	327
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
či	či	k8xC	či
náboženské	náboženský	k2eAgFnSc3d1	náboženská
obci	obec	k1gFnSc3	obec
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
296	[number]	k4	296
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
(	(	kIx(	(
<g/>
34	[number]	k4	34
<g/>
%	%	kIx~	%
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
obce	obec	k1gFnSc2	obec
<g/>
)	)	kIx)	)
a	a	k8xC	a
7	[number]	k4	7
k	k	k7c3	k
českobratrským	českobratrský	k2eAgMnPc3d1	českobratrský
evangelíkům	evangelík	k1gMnPc3	evangelík
<g/>
.	.	kIx.	.
</s>
<s>
Úhrnem	úhrnem	k6eAd1	úhrnem
152	[number]	k4	152
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
bez	bez	k7c2	bez
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
338	[number]	k4	338
lidí	člověk	k1gMnPc2	člověk
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
své	svůj	k3xOyFgFnSc2	svůj
náboženské	náboženský	k2eAgFnSc2d1	náboženská
víry	víra	k1gFnSc2	víra
odpovědět	odpovědět	k5eAaPmF	odpovědět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Rocha	Roch	k1gMnSc2	Roch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
</s>
</p>
<p>
<s>
Rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
Nedánově	Nedánův	k2eAgFnSc6d1	Nedánův
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
Kuntínov	Kuntínov	k1gInSc1	Kuntínov
–	–	k?	–
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
a	a	k8xC	a
Boleradic	Boleradice	k1gInPc2	Boleradice
(	(	kIx(	(
<g/>
†	†	k?	†
1371	[number]	k4	1371
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
boleradicko-loučské	boleradickooučský	k2eAgFnSc2d1	boleradicko-loučský
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Markovič	Markovič	k1gMnSc1	Markovič
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kriminalista	kriminalista	k1gMnSc1	kriminalista
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Boleradice	Boleradice	k1gFnSc2	Boleradice
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boleradice	Boleradice	k1gFnSc2	Boleradice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Boleradice	Boleradice	k1gFnSc2	Boleradice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Divadelní	divadelní	k2eAgInSc1d1	divadelní
spolek	spolek	k1gInSc1	spolek
bratří	bratr	k1gMnPc2	bratr
Mrštíků	Mrštík	k1gMnPc2	Mrštík
Boleradice	Boleradice	k1gFnPc1	Boleradice
</s>
</p>
