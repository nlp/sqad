<p>
<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
přírodních	přírodní	k2eAgFnPc6d1	přírodní
a	a	k8xC	a
technických	technický	k2eAgFnPc6d1	technická
vědách	věda	k1gFnPc6	věda
se	se	k3xPyFc4	se
pojmem	pojem	k1gInSc7	pojem
konstanta	konstanta	k1gFnSc1	konstanta
označuje	označovat	k5eAaImIp3nS	označovat
nějaké	nějaký	k3yIgNnSc4	nějaký
pevně	pevně	k6eAd1	pevně
dané	daný	k2eAgNnSc4d1	dané
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
neměnná	měnný	k2eNgFnSc1d1	neměnná
veličina	veličina	k1gFnSc1	veličina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hodnota	hodnota	k1gFnSc1	hodnota
ovšem	ovšem	k9	ovšem
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
známá	známý	k2eAgFnSc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
konstanty	konstanta	k1gFnSc2	konstanta
je	být	k5eAaImIp3nS	být
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
(	(	kIx(	(
<g/>
potenciálně	potenciálně	k6eAd1	potenciálně
<g/>
)	)	kIx)	)
libovolné	libovolný	k2eAgFnSc2d1	libovolná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstanty	konstanta	k1gFnSc2	konstanta
bez	bez	k7c2	bez
známé	známý	k2eAgFnSc2d1	známá
hodnoty	hodnota	k1gFnSc2	hodnota
==	==	k?	==
</s>
</p>
<p>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
obecných	obecný	k2eAgInPc6d1	obecný
matematických	matematický	k2eAgInPc6d1	matematický
vzorcích	vzorec	k1gInPc6	vzorec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
označují	označovat	k5eAaImIp3nP	označovat
člen	člen	k1gInSc1	člen
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
parametru	parametr	k1gInSc6	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
lineární	lineární	k2eAgFnSc1d1	lineární
rovnice	rovnice	k1gFnSc1	rovnice
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
q	q	k?	q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
i	i	k8xC	i
q	q	k?	q
jsou	být	k5eAaImIp3nP	být
nějaká	nějaký	k3yIgNnPc4	nějaký
pevně	pevně	k6eAd1	pevně
daná	daný	k2eAgNnPc4d1	dané
čísla	číslo	k1gNnPc4	číslo
nezávislá	závislý	k2eNgNnPc4d1	nezávislé
na	na	k7c4	na
proměnné	proměnný	k2eAgInPc4d1	proměnný
x.	x.	k?	x.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstanty	konstanta	k1gFnPc1	konstanta
se	s	k7c7	s
známou	známý	k2eAgFnSc7d1	známá
hodnotou	hodnota	k1gFnSc7	hodnota
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
konstanty	konstanta	k1gFnPc1	konstanta
mají	mít	k5eAaImIp3nP	mít
svá	svůj	k3xOyFgNnPc4	svůj
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
vzorcích	vzorec	k1gInPc6	vzorec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
základní	základní	k2eAgInPc4d1	základní
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
</s>
</p>
<p>
<s>
Ludolfovo	Ludolfův	k2eAgNnSc1d1	Ludolfovo
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
π	π	k?	π
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eulerovo	Eulerův	k2eAgNnSc1d1	Eulerovo
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
zlatý	zlatý	k2eAgInSc1d1	zlatý
řez	řez	k1gInSc1	řez
(	(	kIx(	(
<g/>
φ	φ	k?	φ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
(	(	kIx(	(
<g/>
podrobněji	podrobně	k6eAd2	podrobně
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
konstanta	konstanta	k1gFnSc1	konstanta
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
náboj	náboj	k1gInSc1	náboj
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Permitivita	Permitivita	k1gFnSc1	Permitivita
vakua	vakuum	k1gNnSc2	vakuum
(	(	kIx(	(
<g/>
ε	ε	k?	ε
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Konstanta	konstanta	k1gFnSc1	konstanta
jemné	jemný	k2eAgFnSc2d1	jemná
struktury	struktura	k1gFnSc2	struktura
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Faradayova	Faradayův	k2eAgFnSc1d1	Faradayova
konstanta	konstanta	k1gFnSc1	konstanta
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Avogadrova	Avogadrův	k2eAgFnSc1d1	Avogadrova
konstanta	konstanta	k1gFnSc1	konstanta
(	(	kIx(	(
<g/>
NA	na	k7c4	na
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Matematické	matematický	k2eAgFnPc1d1	matematická
konstanty	konstanta	k1gFnPc1	konstanta
na	na	k7c6	na
MathWorldu	MathWorld	k1gInSc6	MathWorld
</s>
</p>
<p>
<s>
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
hodnoty	hodnota	k1gFnPc1	hodnota
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
konstant	konstanta	k1gFnPc2	konstanta
dle	dle	k7c2	dle
poslední	poslední	k2eAgFnSc2d1	poslední
adjustace	adjustace	k1gFnSc2	adjustace
</s>
</p>
