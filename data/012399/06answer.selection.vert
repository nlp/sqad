<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
,	,	kIx,	,
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
přírodních	přírodní	k2eAgFnPc6d1	přírodní
a	a	k8xC	a
technických	technický	k2eAgFnPc6d1	technická
vědách	věda	k1gFnPc6	věda
se	se	k3xPyFc4	se
pojmem	pojem	k1gInSc7	pojem
konstanta	konstanta	k1gFnSc1	konstanta
označuje	označovat	k5eAaImIp3nS	označovat
nějaké	nějaký	k3yIgNnSc4	nějaký
pevně	pevně	k6eAd1	pevně
dané	daný	k2eAgNnSc4d1	dané
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
neměnná	měnný	k2eNgFnSc1d1	neměnná
veličina	veličina	k1gFnSc1	veličina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hodnota	hodnota	k1gFnSc1	hodnota
ovšem	ovšem	k9	ovšem
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
známá	známý	k2eAgFnSc1d1	známá
<g/>
.	.	kIx.	.
</s>
