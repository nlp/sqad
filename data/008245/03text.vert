<p>
<s>
Obec	obec	k1gFnSc1	obec
Vícov	Vícov	k1gInSc1	Vícov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Prostějov	Prostějov	k1gInSc1	Prostějov
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
544	[number]	k4	544
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
Viec	Viec	k1gFnSc4	Viec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
původní	původní	k2eAgMnSc1d1	původní
majitel	majitel	k1gMnSc1	majitel
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Vícov	Vícov	k1gInSc1	Vícov
tedy	tedy	k9	tedy
znamenal	znamenat	k5eAaImAgInS	znamenat
Viecův	Viecův	k2eAgInSc1d1	Viecův
statek	statek	k1gInSc1	statek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
obec	obec	k1gFnSc4	obec
i	i	k9	i
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
uvádí	uvádět	k5eAaImIp3nS	uvádět
tabulka	tabulka	k1gFnSc1	tabulka
níže	níže	k1gFnSc1	níže
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
i	i	k9	i
příslušnost	příslušnost	k1gFnSc4	příslušnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
či	či	k8xC	či
následné	následný	k2eAgNnSc4d1	následné
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vícovská	Vícovský	k2eAgFnSc1d1	Vícovský
tragédie	tragédie	k1gFnSc1	tragédie
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
partyzánskou	partyzánský	k2eAgFnSc7d1	Partyzánská
skupinou	skupina	k1gFnSc7	skupina
Jermak	Jermak	k1gInSc1	Jermak
přepadena	přepaden	k2eAgFnSc1d1	přepadena
kolona	kolona	k1gFnSc1	kolona
ustupujících	ustupující	k2eAgMnPc2d1	ustupující
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přestřelce	přestřelka	k1gFnSc6	přestřelka
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
deset	deset	k4xCc1	deset
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
padlo	padnout	k5eAaImAgNnS	padnout
několik	několik	k4yIc1	několik
partyzánů	partyzán	k1gMnPc2	partyzán
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
následně	následně	k6eAd1	následně
ustoupili	ustoupit	k5eAaPmAgMnP	ustoupit
do	do	k7c2	do
okolních	okolní	k2eAgInPc2d1	okolní
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Odvetou	odveta	k1gFnSc7	odveta
nacisté	nacista	k1gMnPc1	nacista
do	do	k7c2	do
nedalekého	daleký	k2eNgMnSc2d1	nedaleký
Suchého	Suchý	k1gMnSc2	Suchý
odvlekli	odvléct	k5eAaPmAgMnP	odvléct
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
popravili	popravit	k5eAaPmAgMnP	popravit
desítku	desítka	k1gFnSc4	desítka
vícovských	vícovský	k2eAgMnPc2d1	vícovský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
byli	být	k5eAaImAgMnP	být
pochováni	pochován	k2eAgMnPc1d1	pochován
ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
hrobě	hrob	k1gInSc6	hrob
nedaleko	nedaleko	k7c2	nedaleko
Suchého	Suchý	k1gMnSc2	Suchý
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
událost	událost	k1gFnSc1	událost
nazývána	nazývat	k5eAaImNgFnS	nazývat
vícovskou	vícovský	k2eAgFnSc7d1	vícovský
tragédií	tragédie	k1gFnSc7	tragédie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ježův	Ježův	k2eAgInSc1d1	Ježův
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Floriána	Florián	k1gMnSc4	Florián
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1749	[number]	k4	1749
</s>
</p>
<p>
<s>
Boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Panorama	panorama	k1gNnSc1	panorama
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Vícov	Vícov	k1gInSc1	Vícov
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vícov	Vícovo	k1gNnPc2	Vícovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Vícov	Vícov	k1gInSc1	Vícov
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
