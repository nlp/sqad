<s>
Indické	indický	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
teritoria	teritorium	k1gNnPc1
</s>
<s>
Ándhrapradéš	Ándhrapradéš	k5eAaPmIp2nS
</s>
<s>
Arunáčalpradéš	Arunáčalpradéš	k5eAaPmIp2nS
</s>
<s>
Ásám	Ásám	k6eAd1
</s>
<s>
Bihár	Bihár	k1gInSc1
</s>
<s>
Čhattísgarh	Čhattísgarh	k1gMnSc1
</s>
<s>
Dillí	Dillí	k1gNnSc1
</s>
<s>
Goa	Goa	k1gFnSc1
</s>
<s>
Gudžarát	Gudžarát	k1gInSc1
</s>
<s>
Harijána	Harijána	k1gFnSc1
</s>
<s>
Himáčalpradéš	Himáčalpradéš	k5eAaPmIp2nS
</s>
<s>
Džammú	Džammú	k?
a	a	k8xC
Kašmír	Kašmír	k1gInSc1
</s>
<s>
Džhárkhand	Džhárkhand	k1gInSc1
</s>
<s>
Karnátaka	Karnátak	k1gMnSc4
</s>
<s>
Kérala	Kérala	k1gFnSc1
</s>
<s>
Madhjapradéš	Madhjapradéš	k5eAaPmIp2nS
</s>
<s>
Maháráštra	Maháráštra	k1gFnSc1
</s>
<s>
Manípur	Manípur	k1gMnSc1
</s>
<s>
Méghálaj	Méghálaj	k1gFnSc1
</s>
<s>
Mizóram	Mizóram	k1gInSc1
</s>
<s>
Nágáland	Nágáland	k1gInSc1
</s>
<s>
Urísa	Urísa	k1gFnSc1
</s>
<s>
Paňdžáb	Paňdžáb	k1gInSc1
</s>
<s>
Rádžasthán	Rádžasthán	k2eAgInSc1d1
</s>
<s>
Sikkim	Sikkim	k6eAd1
</s>
<s>
Tamilnádu	Tamilnáda	k1gFnSc4
</s>
<s>
Telangána	Telangána	k1gFnSc1
</s>
<s>
Tripura	Tripura	k1gFnSc1
</s>
<s>
Uttarákhand	Uttarákhand	k1gInSc1
</s>
<s>
Uttarpradéš	Uttarpradéš	k5eAaPmIp2nS
</s>
<s>
Západní	západní	k2eAgNnSc1d1
Bengálsko	Bengálsko	k1gNnSc1
</s>
<s>
Andamany	Andamany	k1gFnPc1
aNikobary	aNikobara	k1gFnSc2
</s>
<s>
Čandígarh	Čandígarh	k1gMnSc1
</s>
<s>
Dádra	Dádra	k1gMnSc1
a	a	k8xC
Nagar	Nagar	k1gMnSc1
Havélí	Havélý	k1gMnPc1
aDaman	aDaman	k1gMnSc1
a	a	k8xC
Díu	Díu	k1gMnSc1
</s>
<s>
Lakadivy	Lakadivy	k1gFnPc1
</s>
<s>
Puduččéri	Puduččéri	k6eAd1
</s>
<s>
Ladak	Ladak	k6eAd1
</s>
<s>
Mapa	mapa	k1gFnSc1
států	stát	k1gInPc2
a	a	k8xC
teritorií	teritorium	k1gNnPc2
Indie	Indie	k1gFnSc2
</s>
<s>
V	v	k7c6
Indii	Indie	k1gFnSc6
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
osm	osm	k4xCc4
svazových	svazový	k2eAgNnPc2d1
teritorií	teritorium	k1gNnPc2
a	a	k8xC
28	#num#	k4
států	stát	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
do	do	k7c2
okresů	okres	k1gInPc2
a	a	k8xC
menších	malý	k2eAgFnPc2d2
jednotek	jednotka	k1gFnPc2
(	(	kIx(
<g/>
stav	stav	k1gInSc1
k	k	k7c3
0	#num#	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současné	současný	k2eAgFnPc1d1
rozdělení	rozdělení	k1gNnSc4
Indie	Indie	k1gFnSc2
odráží	odrážet	k5eAaImIp3nS
její	její	k3xOp3gInSc1
historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
indické	indický	k2eAgNnSc4d1
území	území	k1gNnSc4
ovládaly	ovládat	k5eAaImAgFnP
různorodé	různorodý	k2eAgFnPc1d1
etnické	etnický	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
indické	indický	k2eAgNnSc4d1
území	území	k1gNnSc4
ovládali	ovládat	k5eAaImAgMnP
Britové	Brit	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tzv.	tzv.	kA
Britská	britský	k2eAgFnSc1d1
Indie	Indie	k1gFnSc1
tehdy	tehdy	k6eAd1
zahrnovala	zahrnovat	k5eAaImAgFnS
většinu	většina	k1gFnSc4
území	území	k1gNnSc2
dnešní	dnešní	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
Pákistánu	Pákistán	k1gInSc2
a	a	k8xC
Bangladéše	Bangladéš	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
britské	britský	k2eAgFnSc2d1
navlády	navláda	k1gFnSc2
byly	být	k5eAaImAgFnP
jednotlivé	jednotlivý	k2eAgFnPc1d1
části	část	k1gFnPc1
Indie	Indie	k1gFnSc2
spravovány	spravován	k2eAgFnPc1d1
buď	buď	k8xC
přímo	přímo	k6eAd1
Brity	Brit	k1gMnPc4
či	či	k8xC
lokálními	lokální	k2eAgMnPc7d1
panovníky	panovník	k1gMnPc7
–	–	k?
rádžy	rádžum	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
získala	získat	k5eAaPmAgFnS
Indie	Indie	k1gFnSc1
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
rozdělení	rozdělení	k1gNnSc4
indického	indický	k2eAgNnSc2d1
území	území	k1gNnSc2
z	z	k7c2
dob	doba	k1gFnPc2
Britů	Brit	k1gMnPc2
zůstalo	zůstat	k5eAaPmAgNnS
víceméně	víceméně	k9
zachováno	zachovat	k5eAaPmNgNnS
<g/>
,	,	kIx,
Paňdžáb	Paňdžáb	k1gInSc4
a	a	k8xC
Bengálsko	Bengálsko	k1gNnSc4
byly	být	k5eAaImAgFnP
rozděleny	rozdělit	k5eAaPmNgFnP
mezi	mezi	k7c4
Indii	Indie	k1gFnSc4
a	a	k8xC
Pákistán	Pákistán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
hranice	hranice	k1gFnSc1
nově	nově	k6eAd1
vzniklých	vzniklý	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
teritorií	teritorium	k1gNnPc2
pocházející	pocházející	k2eAgInSc1d1
z	z	k7c2
dob	doba	k1gFnPc2
britské	britský	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
často	často	k6eAd1
nerespektovaly	respektovat	k5eNaImAgFnP
etnické	etnický	k2eAgFnPc1d1
a	a	k8xC
jazykové	jazykový	k2eAgNnSc1d1
složení	složení	k1gNnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Proto	proto	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
reorganizace	reorganizace	k1gFnSc1
těchto	tento	k3xDgFnPc2
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1956	#num#	k4
navíc	navíc	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
několika	několik	k4yIc3
dalším	další	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
,	,	kIx,
např.	např.	kA
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
byl	být	k5eAaImAgInS
stát	stát	k1gInSc1
Bombaj	Bombaj	k1gFnSc4
rozdělen	rozdělen	k2eAgInSc1d1
na	na	k7c4
Gudžarát	Gudžarát	k1gInSc4
a	a	k8xC
Maháráštru	Maháráštrum	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
stát	stát	k1gInSc1
Mizóram	Mizóram	k1gInSc1
apod.	apod.	kA
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
vznikly	vzniknout	k5eAaPmAgInP
tři	tři	k4xCgInPc1
svazové	svazový	k2eAgInPc1d1
státy	stát	k1gInPc1
Čhattísgarh	Čhattísgarha	k1gFnPc2
<g/>
,	,	kIx,
Džhárkhand	Džhárkhanda	k1gFnPc2
a	a	k8xC
Uttarákhand	Uttarákhanda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2014	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
svazový	svazový	k2eAgInSc1d1
stát	stát	k1gInSc1
Telangána	Telangána	k1gFnSc1
vyčleněním	vyčlenění	k1gNnSc7
z	z	k7c2
Ándhrapradéše	Ándhrapradéše	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
říjnu	říjen	k1gInSc3
2019	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
výjimečnému	výjimečný	k2eAgInSc3d1
kroku	krok	k1gInSc3
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
bylo	být	k5eAaImAgNnS
zrušení	zrušení	k1gNnSc1
státu	stát	k1gInSc2
Džammú	Džammú	k1gFnSc2
a	a	k8xC
Kašmír	Kašmír	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
rozdělení	rozdělení	k1gNnSc1
na	na	k7c4
dvě	dva	k4xCgNnPc4
nová	nový	k2eAgNnPc4d1
svazová	svazový	k2eAgNnPc4d1
teritoria	teritorium	k1gNnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc6
západnější	západní	k2eAgMnSc1d2
převzalo	převzít	k5eAaPmAgNnS
název	název	k1gInSc4
původního	původní	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
východním	východní	k2eAgMnPc3d1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Ladak	Ladak	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2020	#num#	k4
vzniklo	vzniknout	k5eAaPmAgNnS
nové	nový	k2eAgNnSc1d1
svazové	svazový	k2eAgNnSc1d1
teritorium	teritorium	k1gNnSc1
Dádra	Dádr	k1gInSc2
a	a	k8xC
Nagar	Nagar	k1gInSc4
Havélí	Havélí	k1gNnSc2
a	a	k8xC
Daman	daman	k1gMnSc1
a	a	k8xC
Díu	Díu	k1gFnSc1
sloučením	sloučení	k1gNnSc7
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
odděleně	odděleně	k6eAd1
spravovaných	spravovaný	k2eAgNnPc2d1
území	území	k1gNnPc2
Dádra	Dádra	k1gMnSc1
a	a	k8xC
Nagar	Nagar	k1gMnSc1
Havélí	Havélý	k2eAgMnPc1d1
a	a	k8xC
Daman	daman	k1gMnSc1
a	a	k8xC
Díu	Díu	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
států	stát	k1gInPc2
a	a	k8xC
teritorií	teritorium	k1gNnPc2
</s>
<s>
Kód	kód	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stát	stát	k1gInSc1
/	/	kIx~
Teritorium	teritorium	k1gNnSc1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Datum	datum	k1gNnSc1
vzniku	vznik	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozloha	rozloha	k1gFnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Populace	populace	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
%	%	kIx~
z	z	k7c2
celkovéhoobyvatelstva	celkovéhoobyvatelstvo	k1gNnSc2
Indie	Indie	k1gFnSc2
</s>
<s>
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
<g/>
(	(	kIx(
<g/>
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Míra	Míra	k1gFnSc1
urbanizace	urbanizace	k1gFnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Míra	Míra	k1gFnSc1
gramotnostiobyvatelstva	gramotnostiobyvatelstvo	k1gNnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
IN-UPUttarpradéšLakhnaú	IN-UPUttarpradéšLakhnaú	k?
<g/>
19500126	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
240928.000000240	240928.000000240	k4
928	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
199812341.000000199	199812341.000000199	k4
812	#num#	k4
341	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
16.500	16.500	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
16,50	16,50	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
828.000	828.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
828	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22.260	22.260	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22,26	22,26	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
67.680	67.680	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
67,68	67,68	k4
%	%	kIx~
</s>
<s>
IN-MHMaháráštraBombaj	IN-MHMaháráštraBombaj	k1gFnSc1
<g/>
19600501	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1960	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
307713.000000307	307713.000000307	k4
713	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
112374333.000000112	112374333.000000112	k4
374	#num#	k4
333	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9.280	9.280	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9,28	9,28	k4
%	%	kIx~
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
365.000	365.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
365	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
45.230	45.230	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
45,23	45,23	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
82.340	82.340	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
82,34	82,34	k4
%	%	kIx~
</s>
<s>
IN-BRBihárPatna	IN-BRBihárPatna	k1gFnSc1
<g/>
19500126	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
94163.00000094	94163.00000094	k4
163	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
104099452.000000104	104099452.000000104	k4
099	#num#	k4
452	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
8.600	8.600	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
8,60	8,60	k4
%	%	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1102.0000001	1102.0000001	k4
102	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11.270	11.270	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11,27	11,27	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
61.800	61.800	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
61,80	61,80	k4
%	%	kIx~
</s>
<s>
IN-WBZápadní	IN-WBZápadní	k2eAgFnSc1d1
BengálskoKalkata	BengálskoKalkata	k1gFnSc1
<g/>
19500126	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
88752.00000088	88752.00000088	k4
752	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
91276115.00000091	91276115.00000091	k4
276	#num#	k4
115	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7.540	7.540	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
7,54	7,54	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1030.0000001	1030.0000001	k4
0	#num#	k4
<g/>
30	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
31.920	31.920	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
31,92	31,92	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
76.260	76.260	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
76,26	76,26	k4
%	%	kIx~
</s>
<s>
IN-MPMadhjapradéšBhópál	IN-MPMadhjapradéšBhópál	k1gInSc1
<g/>
19500126	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
308245.000000308	308245.000000308	k4
245	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
72626809.00000072	72626809.00000072	k4
626	#num#	k4
809	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6.000	6.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6,00	6,00	k4
%	%	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
236.000	236.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
236	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27.620	27.620	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27,62	27,62	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
69.320	69.320	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
69,32	69,32	k4
%	%	kIx~
</s>
<s>
IN-TNTamilnáduMadrás	IN-TNTamilnáduMadrás	k1gInSc1
<g/>
19561101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1956	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
130058.000000130	130058.000000130	k4
0	#num#	k4
<g/>
58	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
72147030.00000072	72147030.00000072	k4
147	#num#	k4
0	#num#	k4
<g/>
30	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5.960	5.960	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
5,96	5,96	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
555.000	555.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
555	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
48.440	48.440	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
48,44	48,44	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
80.090	80.090	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
80,09	80,09	k4
%	%	kIx~
</s>
<s>
IN-RJRádžasthánPžajpur	IN-RJRádžasthánPžajpur	k1gMnSc1
<g/>
19500126	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
342239.000000342	342239.000000342	k4
239	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
68548437.00000068	68548437.00000068	k4
548	#num#	k4
437	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5.660	5.660	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5,66	5,66	k4
%	%	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
201.000	201.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
201	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24.920	24.920	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24,92	24,92	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
66.110	66.110	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
66,11	66,11	k4
%	%	kIx~
</s>
<s>
IN-KAKarnátakaBengalúr	IN-KAKarnátakaBengalúr	k1gInSc1
<g/>
19561101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1956	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
191791.000000191	191791.000000191	k4
791	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
61095297.00000061	61095297.00000061	k4
095	#num#	k4
297	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5.050	5.050	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5,05	5,05	k4
%	%	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
319.000	319.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
319	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
38.590	38.590	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
38,59	38,59	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
75.360	75.360	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
75,36	75,36	k4
%	%	kIx~
</s>
<s>
IN-GJGudžarátGándhínagar	IN-GJGudžarátGándhínagar	k1gInSc1
<g/>
19600501	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1960	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
196024.000000196	196024.000000196	k4
0	#num#	k4
<g/>
24	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
60439692.00000060	60439692.00000060	k4
439	#num#	k4
692	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4.990	4.990	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
4,99	4,99	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
308.000	308.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
308	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
42.540	42.540	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
42,54	42,54	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
78.030	78.030	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
78,03	78,03	k4
%	%	kIx~
</s>
<s>
IN-APÁndhrapradéšAmaravati	IN-APÁndhrapradéšAmaravat	k5eAaBmF,k5eAaImF,k5eAaPmF
(	(	kIx(
<g/>
de	de	k?
facto	facto	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
19561101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1956	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
162972.000000162	162972.000000162	k4
972	#num#	k4
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
49386799.00000049	49386799.00000049	k4
386	#num#	k4
799	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
4.070	4.070	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4,07	4,07	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
303.000	303.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
303	#num#	k4
<g/>
&	&	k?
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
operátor	operátor	k1gInSc1
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
1.100	1.100	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
&	&	k?
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc1d1
operátor	operátor	k1gInSc1
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
1.100	1.100	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
IN-ORUrísaBhuvanéšvar	IN-ORUrísaBhuvanéšvar	k1gInSc1
<g/>
19500126	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
155707.000000155	155707.000000155	k4
707	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
41974218.00000041	41974218.00000041	k4
974	#num#	k4
218	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3.470	3.470	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3,47	3,47	k4
%	%	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
269.000	269.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
269	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
16.670	16.670	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
16,67	16,67	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
72.870	72.870	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
72,87	72,87	k4
%	%	kIx~
</s>
<s>
IN-TGTelangánaHajdarábád	IN-TGTelangánaHajdarábáda	k1gFnPc2
<g/>
20140602	#num#	k4
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
112077.000000112	112077.000000112	k4
0	#num#	k4
<g/>
77	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
35193978.00000035	35193978.00000035	k4
193	#num#	k4
978	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2.900	2.900	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2,90	2,90	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
314.000	314.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
314	#num#	k4
<g/>
&	&	k?
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc1d1
operátor	operátor	k1gInSc1
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
1.100	1.100	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
&	&	k?
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc1d1
operátor	operátor	k1gInSc1
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
1.100	1.100	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
IN-KLKéralaTiruvanantapuram	IN-KLKéralaTiruvanantapuram	k1gInSc1
<g/>
19561101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1956	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
38863.00000038	38863.00000038	k4
863	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
33406061.00000033	33406061.00000033	k4
406	#num#	k4
0	#num#	k4
<g/>
61	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2.760	2.760	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
2,76	2,76	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
859.000	859.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
859	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
47.690	47.690	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
47,69	47,69	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
94.000	94.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
94,00	94,00	k4
%	%	kIx~
</s>
<s>
IN-JHDžhárkhandRáňčí	IN-JHDžhárkhandRáňčí	k1gNnSc1
<g/>
20001115	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2000	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
79714.00000079	79714.00000079	k4
714	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
32988134.00000032	32988134.00000032	k4
988	#num#	k4
134	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2.720	2.720	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2,72	2,72	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
414.000	414.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
414	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24.040	24.040	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24,04	24,04	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
66.410	66.410	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
66,41	66,41	k4
%	%	kIx~
</s>
<s>
IN-ASÁsámDispur	IN-ASÁsámDispur	k1gMnSc1
<g/>
19500126	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1950	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
78438.00000078	78438.00000078	k4
438	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
31205576.00000031	31205576.00000031	k4
205	#num#	k4
576	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2.580	2.580	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2,58	2,58	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
397.000	397.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
397	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
14.060	14.060	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
14,06	14,06	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
72.190	72.190	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
72,19	72,19	k4
%	%	kIx~
</s>
<s>
IN-PBPaňdžábČandígarh	IN-PBPaňdžábČandígarh	k1gInSc1
<g/>
19661101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1966	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
50362.00000050	50362.00000050	k4
362	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27743338.00000027	27743338.00000027	k4
743	#num#	k4
338	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2.290	2.290	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2,29	2,29	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
550.000	550.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
550	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
37.440	37.440	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
37,44	37,44	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
75.840	75.840	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
75,84	75,84	k4
%	%	kIx~
</s>
<s>
IN-CTČhattísgarhRájpúr	IN-CTČhattísgarhRájpúr	k1gInSc1
<g/>
20001101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2000	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
135191.000000135	135191.000000135	k4
191	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
25545198.00000025	25545198.00000025	k4
545	#num#	k4
198	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2.110	2.110	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2,11	2,11	k4
%	%	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
189.000	189.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
189	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
23.240	23.240	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
23,24	23,24	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
70.280	70.280	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
70,28	70,28	k4
%	%	kIx~
</s>
<s>
IN-HRHarijánaČandígarh	IN-HRHarijánaČandígarh	k1gInSc1
<g/>
19661101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1966	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
44212.00000044	44212.00000044	k4
212	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
25351462.00000025	25351462.00000025	k4
351	#num#	k4
462	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2.090	2.090	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2,09	2,09	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
573.000	573.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
573	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
34.800	34.800	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
34,80	34,80	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
75.550	75.550	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
75,55	75,55	k4
%	%	kIx~
</s>
<s>
IN-DLDillíDillí	IN-DLDillíDillí	k1gNnSc1
<g/>
19561101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1956	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1484.0000001	1484.0000001	k4
484	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
16787941.00000016	16787941.00000016	k4
787	#num#	k4
941	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1.390	1.390	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
1,39	1,39	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11297.00000011	11297.00000011	k4
297	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
76.880	76.880	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
76,88	76,88	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
86.210	86.210	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
86,21	86,21	k4
%	%	kIx~
</s>
<s>
IN-JKDžammú	IN-JKDžammú	k?
a	a	k8xC
KašmírŠrínagar	KašmírŠrínagar	k1gMnSc1
+	+	kIx~
Džammú	Džammú	k1gMnSc1
<g/>
20191031	#num#	k4
<g/>
a	a	k8xC
<g/>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
42241.00000042	42241.00000042	k4
241	#num#	k4
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
12267013.00000012	12267013.00000012	k4
267	#num#	k4
0	#num#	k4
<g/>
13	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1.010	1.010	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1,01	1,01	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
290.000	290.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
290	#num#	k4
<g/>
&	&	k?
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc1d1
operátor	operátor	k1gInSc1
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
1.100	1.100	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
&	&	k?
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc1d1
operátor	operátor	k1gInSc1
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
1.100	1.100	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
IN-UTUttarákhandDéhrádún	IN-UTUttarákhandDéhrádún	k1gNnSc1
<g/>
20001109	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2000	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
53483.00000053	53483.00000053	k4
483	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10086292.00000010	10086292.00000010	k4
086	#num#	k4
292	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.8300000	0.8300000	k4
<g/>
,83	,83	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
189.000	189.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
189	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
30.650	30.650	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
30,65	30,65	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
79.630	79.630	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
79,63	79,63	k4
%	%	kIx~
</s>
<s>
IN-HPHimáčalpradéšŠimla	IN-HPHimáčalpradéšŠimla	k1gFnSc1
<g/>
19710125	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1971	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
55673.00000055	55673.00000055	k4
673	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6864602.0000006	6864602.0000006	k4
864	#num#	k4
602	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.5700000	0.5700000	k4
<g/>
,57	,57	k4
%	%	kIx~
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
123.000	123.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
123	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10.030	10.030	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10,03	10,03	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
82.800	82.800	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
82,80	82,80	k4
%	%	kIx~
</s>
<s>
IN-TRTripuraAgartala	IN-TRTripuraAgartat	k5eAaPmAgFnS
<g/>
19720121	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1972	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10486.00000010	10486.00000010	k4
486	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3673917.0000003	3673917.0000003	k4
673	#num#	k4
917	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.3000000	0.3000000	k4
<g/>
,30	,30	k4
%	%	kIx~
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
350.000	350.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
350	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
26.160	26.160	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
26,16	26,16	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
87.220	87.220	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
87,22	87,22	k4
%	%	kIx~
</s>
<s>
IN-MLMéghálajŠilaung	IN-MLMéghálajŠilaung	k1gInSc1
<g/>
19720121	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1972	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22429.00000022	22429.00000022	k4
429	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2966889.0000002	2966889.0000002	k4
966	#num#	k4
889	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.2500000	0.2500000	k4
<g/>
,25	,25	k4
%	%	kIx~
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
132.000	132.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
132	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
20.060	20.060	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
20,06	20,06	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
74.430	74.430	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
74,43	74,43	k4
%	%	kIx~
</s>
<s>
IN-MNManípurImphál	IN-MNManípurImphál	k1gInSc1
<g/>
19720121	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1972	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22327.00000022	22327.00000022	k4
327	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2721756.0000002	2721756.0000002	k4
721	#num#	k4
756	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.2100000	0.2100000	k4
<g/>
,21	,21	k4
%	%	kIx~
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
122.000	122.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
122	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
30.210	30.210	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
30,21	30,21	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
79.210	79.210	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
79,21	79,21	k4
%	%	kIx~
</s>
<s>
IN-NLNágálandKóhíma	IN-NLNágálandKóhí	k2eAgFnPc7d1
<g/>
19631201	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1963	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
16579.00000016	16579.00000016	k4
579	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1978502.0000001	1978502.0000001	k4
978	#num#	k4
502	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.1600000	0.1600000	k4
<g/>
,16	,16	k4
%	%	kIx~
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
119.000	119.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
119	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
29.000	29.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
29,00	29,00	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
79.550	79.550	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
79,55	79,55	k4
%	%	kIx~
</s>
<s>
IN-GAGoaPanadží	IN-GAGoaPanadží	k1gNnSc1
<g/>
19870530	#num#	k4
<g/>
a	a	k8xC
<g/>
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1987	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3702.0000003	3702.0000003	k4
702	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1458545.0000001	1458545.0000001	k4
458	#num#	k4
545	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.1200000	0.1200000	k4
<g/>
,12	,12	k4
%	%	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
394.000	394.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
394	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
62.140	62.140	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
62,14	62,14	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
88.700	88.700	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
88,70	88,70	k4
%	%	kIx~
</s>
<s>
IN-ARArunáčalpradéšItánagar	IN-ARArunáčalpradéšItánagar	k1gInSc1
<g/>
19870220	#num#	k4
<g/>
a	a	k8xC
<g/>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1987	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
83743.00000083	83743.00000083	k4
743	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1383727.0000001	1383727.0000001	k4
383	#num#	k4
727	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.1100000	0.1100000	k4
<g/>
,11	,11	k4
%	%	kIx~
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
17.000	17.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
17	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22.650	22.650	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22,65	22,65	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
65.380	65.380	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
65,38	65,38	k4
%	%	kIx~
</s>
<s>
IN-PYPuduččériPuduččéri	IN-PYPuduččériPuduččéri	k1gNnSc1
<g/>
19620816	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1962	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
479.000	479.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
479	#num#	k4
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1247953.0000001	1247953.0000001	k4
247	#num#	k4
953	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.1000000	0.1000000	k4
<g/>
,10	,10	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2598.0000002	2598.0000002	k4
598	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
68.120	68.120	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
68,12	68,12	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
85.850	85.850	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
85,85	85,85	k4
%	%	kIx~
</s>
<s>
IN-MZMizóramÁizal	IN-MZMizóramÁizat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
19870220	#num#	k4
<g/>
a	a	k8xC
<g/>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1987	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
21081.00000021	21081.00000021	k4
0	#num#	k4
<g/>
81	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1097206.0000001	1097206.0000001	k4
097	#num#	k4
206	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.0900000	0.0900000	k4
<g/>
,09	,09	k4
%	%	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
52.000	52.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
52	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
51.220	51.220	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
51,22	51,22	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
91.330	91.330	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
91,33	91,33	k4
%	%	kIx~
</s>
<s>
IN-CHČandígarhČandígarh	IN-CHČandígarhČandígarh	k1gInSc1
<g/>
19661101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1966	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
114.000	114.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
114	#num#	k4
<g/>
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1055450.0000001	1055450.0000001	k4
055	#num#	k4
450	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0.0900000	0.0900000	k4
<g/>
,09	,09	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9252.0000009	9252.0000009	k4
252	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
97.180	97.180	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
97,18	97,18	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
86.050	86.050	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
86,05	86,05	k4
%	%	kIx~
</s>
<s>
IN-SKSikkimGángtók	IN-SKSikkimGángtók	k1gInSc1
<g/>
19750516	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1975	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7096.0000007	7096.0000007	k4
0	#num#	k4
<g/>
96	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
610577.000000610	610577.000000610	k4
577	#num#	k4
<g/>
&	&	k?
<g/>
-	-	kIx~
<g/>
1000000000000000.0500000	1000000000000000.0500000	k4
<g/>
,05	,05	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
86.000	86.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
86	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24.850	24.850	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24,85	24,85	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
81.420	81.420	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
81,42	81,42	k4
%	%	kIx~
</s>
<s>
IN-ANAndamany	IN-ANAndaman	k1gInPc1
a	a	k8xC
NikobaryPort	NikobaryPort	k1gInSc1
Blair	Blair	k1gMnSc1
<g/>
19561101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1956	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
8249.0000008	8249.0000008	k4
249	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
380581.000000380	380581.000000380	k4
581	#num#	k4
<g/>
&	&	k?
<g/>
-	-	kIx~
<g/>
1000000000000000.0300000	1000000000000000.0300000	k4
<g/>
,03	,03	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
46.000	46.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
46	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
35.610	35.610	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
35,61	35,61	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
86.630	86.630	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
86,63	86,63	k4
%	%	kIx~
</s>
<s>
IN-DHDádra	IN-DHDádra	k1gFnSc1
a	a	k8xC
Nagar	Nagar	k1gInSc1
Havélí	Havélí	k1gNnSc2
a	a	k8xC
Daman	daman	k1gMnSc1
a	a	k8xC
DíuDaman	DíuDaman	k1gMnSc1
<g/>
20200126	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
603.000	603.000	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
603	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
586956.000000586	586956.000000586	k4
956	#num#	k4
<g/>
&	&	k?
<g/>
-	-	kIx~
<g/>
1000000000000000.0500000	1000000000000000.0500000	k4
<g/>
,05	,05	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
973.000	973.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
973	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
58.340	58.340	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
58,34	58,34	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
80.740	80.740	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
80,74	80,74	k4
%	%	kIx~
</s>
<s>
IN-LALadakLéh	IN-LALadakLéh	k1gInSc1
<g/>
20191031	#num#	k4
<g/>
a	a	k8xC
<g/>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
59146.00000059	59146.00000059	k4
146	#num#	k4
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
274289.000000274	274289.000000274	k4
289	#num#	k4
<g/>
&	&	k?
<g/>
-	-	kIx~
<g/>
1000000000000000.0200000	1000000000000000.0200000	k4
<g/>
,02	,02	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5.000	5.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
&	&	k?
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
1.100	1.100	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
&	&	k?
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc1d1
operátor	operátor	k1gInSc1
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
Chyba	chyba	k1gFnSc1
ve	v	k7c6
výrazu	výraz	k1gInSc6
<g/>
:	:	kIx,
Neočekávaný	očekávaný	k2eNgInSc4d1
operátor	operátor	k1gInSc4
/	/	kIx~
<g/>
1.100	1.100	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
</s>
<s>
IN-LDLakadivyKavaratti	IN-LDLakadivyKavaratti	k1gNnSc1
<g/>
19561101	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1956	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
32.000	32.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
32	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
64473.00000064	64473.00000064	k4
473	#num#	k4
<g/>
&	&	k?
<g/>
-	-	kIx~
<g/>
1000000000000000.0100000	1000000000000000.0100000	k4
<g/>
,01	,01	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2013.0000002	2013.0000002	k4
0	#num#	k4
<g/>
13	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
78.030	78.030	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
78,03	78,03	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
91.850	91.850	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
91,85	91,85	k4
%	%	kIx~
</s>
<s>
IndieDillí	IndieDillí	k1gNnSc1
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3287240.0000003	3287240.0000003	k4
287	#num#	k4
240	#num#	k4
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1210854977.0000001	1210854977.0000001	k4
210	#num#	k4
854	#num#	k4
977	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3827.0000003	3827.0000003	k4
827	#num#	k4
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
31.160	31.160	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
31,16	31,16	k4
%	%	kIx~
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
74.040	74.040	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
74,04	74,04	k4
</s>
<s>
Názvy	název	k1gInPc1
států	stát	k1gInPc2
a	a	k8xC
teritorií	teritorium	k1gNnPc2
v	v	k7c6
různých	různý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
</s>
<s>
Následující	následující	k2eAgInSc1d1
vícejazyčný	vícejazyčný	k2eAgInSc1d1
seznam	seznam	k1gInSc1
indických	indický	k2eAgInPc2d1
států	stát	k1gInPc2
ukazuje	ukazovat	k5eAaImIp3nS
názvy	název	k1gInPc7
všech	všecek	k3xTgInPc2
indických	indický	k2eAgInPc2d1
spolkových	spolkový	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
svazových	svazový	k2eAgNnPc2d1
teritorií	teritorium	k1gNnPc2
česky	česky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
hindštině	hindština	k1gFnSc6
a	a	k8xC
lokálním	lokální	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stav	stav	k1gInSc1
k	k	k7c3
0	#num#	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
(	(	kIx(
<g/>
tzn.	tzn.	kA
28	#num#	k4
států	stát	k1gInPc2
a	a	k8xC
8	#num#	k4
teritorií	teritorium	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnPc1
jsou	být	k5eAaImIp3nP
seřazena	seřadit	k5eAaPmNgNnP
dle	dle	k7c2
svého	svůj	k3xOyFgInSc2
názvu	název	k1gInSc2
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Čeština	čeština	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
3	#num#	k4
<g/>
]	]	kIx)
<g/>
AngličtinaHindštinamístní	AngličtinaHindštinamístní	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stát	stát	k1gInSc1
/	/	kIx~
TeritoriumHlavní	TeritoriumHlavní	k2eAgInSc1d1
městoStát	městoStát	k1gInSc1
/	/	kIx~
TeritoriumHlavní	TeritoriumHlavní	k2eAgInSc1d1
městoStát	městoStát	k1gInSc1
/	/	kIx~
TeritoriumHlavní	TeritoriumHlavní	k2eAgInSc1d1
městoStát	městoStát	k1gInSc1
/	/	kIx~
TeritoriumHlavní	TeritoriumHlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Andamany	Andamany	k1gFnPc1
a	a	k8xC
NikobaryPort	NikobaryPort	k1gInSc1
BlairAndaman	BlairAndaman	k1gMnSc1
and	and	k?
Nicobar	Nicobar	k1gInSc1
IslandsPort	IslandsPort	k1gInSc1
Blairअ	Blairअ	k1gFnSc1
<g/>
ं	ं	k?
<g/>
ड	ड	k?
<g/>
ा	ा	k?
<g/>
न	न	k?
औ	औ	k?
न	न	k?
<g/>
ि	ि	k?
<g/>
क	क	k?
<g/>
ो	ो	k?
<g/>
ब	ब	k?
<g/>
ा	ा	k?
<g/>
र	र	k?
द	द	k?
<g/>
्	्	k?
<g/>
व	व	k?
<g/>
ी	ी	k?
<g/>
प	प	k?
/	/	kIx~
Amdamā	Amdamā	k1gMnSc1
ôr	ôr	k?
Nikobā	Nikobā	k1gMnSc1
dvī	dvī	k?
<g/>
ो	ो	k?
<g/>
र	र	k?
<g/>
्	्	k?
<g/>
ट	ट	k?
ब	ब	k?
<g/>
्	्	k?
<g/>
ल	ल	k?
<g/>
े	े	k?
<g/>
अ	अ	k?
/	/	kIx~
Porţ	Porţ	k1gMnSc1
Blear	Blear	k1gMnSc1
</s>
<s>
ÁndhrapradéšHajdarábád	ÁndhrapradéšHajdarábáda	k1gFnPc2
(	(	kIx(
<g/>
de	de	k?
iure	iure	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
Andhra	Andhra	k1gFnSc1
PradeshHyderabadआ	PradeshHyderabadआ	k1gFnSc2
<g/>
ं	ं	k?
<g/>
ध	ध	k?
<g/>
्	्	k?
<g/>
र	र	k?
प	प	k?
<g/>
्	्	k?
<g/>
र	र	k?
<g/>
े	े	k?
<g/>
श	श	k?
/	/	kIx~
Ā	Ā	k1gFnSc1
Pradešह	Pradešह	k1gFnSc2
<g/>
ै	ै	k?
<g/>
द	द	k?
<g/>
ा	ा	k?
<g/>
ब	ब	k?
<g/>
ा	ा	k?
<g/>
द	द	k?
/	/	kIx~
Hæ	Hæ	k1gMnSc1
ఆ	ఆ	k?
<g/>
ం	ం	k?
<g/>
ధ	ధ	k?
ద	ద	k?
<g/>
ే	ే	k?
<g/>
శ	శ	k?
<g/>
ం	ం	k?
/	/	kIx~
Ā	Ā	k1gFnSc1
Dē	Dē	k1gFnSc2
హ	హ	k?
<g/>
ై	ై	k?
<g/>
ద	ద	k?
<g/>
ా	ా	k?
<g/>
బ	బ	k?
<g/>
ా	ా	k?
<g/>
ద	ద	k?
<g/>
ు	ు	k?
/	/	kIx~
Haidarā	Haidarā	k1gInSc2
</s>
<s>
ArunáčalpradéšItánagarArunachal	ArunáčalpradéšItánagarArunachat	k5eAaPmAgMnS,k5eAaImAgMnS
PradeshItanagarअ	PradeshItanagarअ	k1gMnSc1
<g/>
ु	ु	k?
<g/>
ण	ण	k?
<g/>
ा	ा	k?
<g/>
च	च	k?
प	प	k?
<g/>
्	्	k?
<g/>
र	र	k?
<g/>
े	े	k?
<g/>
श	श	k?
/	/	kIx~
Arunā	Arunā	k1gMnSc1
Pradešइ	Pradešइ	k1gMnSc1
<g/>
ा	ा	k?
<g/>
न	न	k?
/	/	kIx~
Iţā	Iţā	k1gMnSc1
</s>
<s>
ÁsámDispurAssamDispurअ	ÁsámDispurAssamDispurअ	k?
/	/	kIx~
Asamद	Asamद	k1gMnSc1
<g/>
ि	ि	k?
<g/>
स	स	k?
<g/>
ु	ु	k?
<g/>
र	र	k?
/	/	kIx~
Dispurásámsky	Dispurásámsky	k1gMnSc1
অ	অ	k?
/	/	kIx~
Asambengálsky	Asambengálsky	k1gMnSc1
আ	আ	k?
<g/>
া	া	k?
<g/>
ম	ম	k?
/	/	kIx~
Ā	Ā	k6eAd1
দ	দ	k?
<g/>
ি	ি	k?
<g/>
স	স	k?
<g/>
ু	ু	k?
<g/>
র	র	k?
/	/	kIx~
Dispur	Dispur	k1gMnSc1
</s>
<s>
BihárPatnaBiharPatnaब	BihárPatnaBiharPatnaब	k?
<g/>
ि	ि	k?
<g/>
ह	ह	k?
<g/>
ा	ा	k?
<g/>
र	र	k?
/	/	kIx~
Bihā	Bihā	k1gMnSc1
<g/>
ा	ा	k?
/	/	kIx~
Paţnā	Paţnā	k1gMnSc1
</s>
<s>
ČandígarhČandígarhChandigarhChandigarhच	ČandígarhČandígarhChandigarhChandigarhच	k?
<g/>
ं	ं	k?
<g/>
ड	ड	k?
<g/>
ी	ी	k?
<g/>
ग	ग	k?
<g/>
़	़	k?
/	/	kIx~
Čamdī	Čamdī	k1gMnSc1
<g/>
ं	ं	k?
<g/>
ड	ड	k?
<g/>
ी	ी	k?
<g/>
ग	ग	k?
<g/>
़	़	k?
/	/	kIx~
Čamdī	Čamdī	k1gMnSc1
</s>
<s>
ČhattísgarhRájpúrChattisgarhRaipurछ	ČhattísgarhRájpúrChattisgarhRaipurछ	k?
<g/>
्	्	k?
<g/>
त	त	k?
<g/>
ी	ी	k?
<g/>
स	स	k?
<g/>
़	़	k?
/	/	kIx~
Čhattī	Čhattī	k1gMnSc1
<g/>
ा	ा	k?
<g/>
य	य	k?
<g/>
ु	ु	k?
<g/>
र	र	k?
/	/	kIx~
Rā	Rā	k1gMnSc1
</s>
<s>
Dádra	Dádra	k1gMnSc1
a	a	k8xC
Nagar	Nagar	k1gMnSc1
Havélí	Havélý	k2eAgMnPc1d1
a	a	k8xC
Daman	daman	k1gMnSc1
a	a	k8xC
DíuDamanPadra	DíuDamanPadra	k1gFnSc1
and	and	k?
Nagar	Nagar	k1gInSc1
Haveli	Havel	k1gInSc6
and	and	k?
Daman	daman	k1gMnSc1
and	and	k?
DiuDamanद	DiuDamanद	k1gMnSc1
<g/>
ा	ा	k?
<g/>
द	द	k?
<g/>
ा	ा	k?
औ	औ	k?
न	न	k?
ह	ह	k?
<g/>
े	े	k?
<g/>
ल	ल	k?
<g/>
ी	ी	k?
औ	औ	k?
द	द	k?
औ	औ	k?
द	द	k?
<g/>
ी	ी	k?
<g/>
व	व	k?
/	/	kIx~
Dā	Dā	k1gMnSc2
ôr	ôr	k?
Nagar	Nagar	k1gMnSc1
ôr	ôr	k?
Havelī	Havelī	k1gMnSc1
Daman	daman	k1gMnSc1
ôr	ôr	k?
Diyū	Diyū	k1gMnSc1
/	/	kIx~
Damangudžarátsky	Damangudžarátsky	k1gMnSc1
દ	દ	k?
<g/>
ા	ા	k?
<g/>
દ	દ	k?
<g/>
ા	ા	k?
અ	અ	k?
<g/>
ે	ે	k?
ન	ન	k?
હ	હ	k?
<g/>
ે	ે	k?
<g/>
લ	લ	k?
<g/>
ી	ી	k?
અ	અ	k?
<g/>
ે	ે	k?
દ	દ	k?
અ	અ	k?
<g/>
ે	ે	k?
દ	દ	k?
<g/>
ી	ી	k?
<g/>
વ	વ	k?
/	/	kIx~
Dā	Dā	k1gMnSc5
ane	án	k1gMnSc5
Nagar	Nagar	k1gInSc1
Havelī	Havelī	k1gMnSc5
ane	án	k1gMnSc5
Damaṇ	Damaṇ	k1gMnSc5
ane	án	k1gMnSc5
Dī	Dī	k1gFnPc6
દ	દ	k?
/	/	kIx~
Daman	daman	k1gMnSc1
</s>
<s>
DillíDillíDelhiDelhiद	DillíDillíDelhiDelhiद	k?
<g/>
ि	ि	k?
<g/>
ल	ल	k?
<g/>
्	्	k?
<g/>
ल	ल	k?
<g/>
ी	ी	k?
/	/	kIx~
Dillī	Dillī	k1gMnSc1
<g/>
ि	ि	k?
<g/>
ल	ल	k?
<g/>
्	्	k?
<g/>
ल	ल	k?
<g/>
ी	ी	k?
/	/	kIx~
Dillī	Dillī	k1gMnSc1
</s>
<s>
Džammú	Džammú	k?
a	a	k8xC
KašmírŠrínagar	KašmírŠrínagar	k1gMnSc1
+	+	kIx~
DžammúJammu	DžammúJamm	k1gInSc2
and	and	k?
KashmirSrinagar	KashmirSrinagar	k1gMnSc1
+	+	kIx~
Jammuज	Jammuज	k1gMnSc1
<g/>
्	्	k?
<g/>
म	म	k?
<g/>
ू	ू	k?
औ	औ	k?
क	क	k?
<g/>
्	्	k?
<g/>
म	म	k?
<g/>
ी	ी	k?
<g/>
र	र	k?
/	/	kIx~
Jammū	Jammū	k1gMnSc1
ôr	ôr	k?
Kašmī	Kašmī	k1gMnSc1
<g/>
्	्	k?
<g/>
र	र	k?
<g/>
ी	ी	k?
<g/>
न	न	k?
/	/	kIx~
Šrī	Šrī	k1gMnSc1
+	+	kIx~
ज	ज	k?
<g/>
्	्	k?
<g/>
म	म	k?
<g/>
ू	ू	k?
/	/	kIx~
Jammū	Jammū	k1gNnPc7
ज	ज	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
ॅ	ॅ	k?
<g/>
म	म	k?
त	त	k?
<g/>
ु	ु	k?
<g/>
'	'	kIx"
क	क	k?
<g/>
'	'	kIx"
<g/>
श	श	k?
<g/>
ी	ी	k?
<g/>
र	र	k?
/	/	kIx~
ج	ج	k?
ت	ت	k?
<g/>
ٕ	ٕ	k?
ک	ک	k?
<g/>
ٔ	ٔ	k?
<g/>
ش	ش	k?
/	/	kIx~
Jọ	Jọ	k1gMnSc1
tụ	tụ	k?
Kạ	Kạ	k1gMnSc1
ج	ج	k?
و	و	k?
ک	ک	k?
/	/	kIx~
Jammū	Jammū	k1gMnSc1
va	va	k0wR
Kašmī	Kašmī	k1gNnPc7
स	स	k?
<g/>
ि	ि	k?
<g/>
र	र	k?
<g/>
ी	ी	k?
<g/>
न	न	k?
/	/	kIx~
س	س	k?
<g/>
ِ	ِ	k?
<g/>
ر	ر	k?
<g/>
َ	َ	k?
<g/>
گ	گ	k?
<g/>
َ	َ	k?
<g/>
ر	ر	k?
/	/	kIx~
Sirī	Sirī	k1gMnSc1
+	+	kIx~
ज	ज	k?
<g/>
ॅ	ॅ	k?
<g/>
म	म	k?
/	/	kIx~
ج	ج	k?
/	/	kIx~
Jọ	Jọ	k1gMnSc1
س	س	k?
ن	ن	k?
/	/	kIx~
س	س	k?
/	/	kIx~
Sirī	Sirī	k1gMnSc1
+	+	kIx~
ج	ج	k?
/	/	kIx~
Jammū	Jammū	k1gMnSc1
</s>
<s>
DžhárkhandRáňčíJharkhandRanchiझ	DžhárkhandRáňčíJharkhandRanchiझ	k?
<g/>
ा	ा	k?
<g/>
र	र	k?
<g/>
ं	ं	k?
<g/>
ड	ड	k?
/	/	kIx~
Jhā	Jhā	k1gMnSc1
<g/>
ा	ा	k?
<g/>
ं	ं	k?
<g/>
च	च	k?
<g/>
ी	ी	k?
/	/	kIx~
Rā	Rā	k1gMnSc1
</s>
<s>
GoaPanadžíGoaPanajiग	GoaPanadžíGoaPanajiग	k?
<g/>
ो	ो	k?
<g/>
व	व	k?
<g/>
ा	ा	k?
/	/	kIx~
Govā	Govā	k1gMnSc1
<g/>
ी	ी	k?
/	/	kIx~
Panajī	Panajī	k1gFnSc2
ग	ग	k?
<g/>
ो	ो	k?
<g/>
आ	आ	k?
/	/	kIx~
Gō	Gō	k1gFnSc2
प	प	k?
<g/>
ी	ी	k?
/	/	kIx~
Panajī	Panajī	k1gMnSc1
</s>
<s>
GudžarátGándhínagarGujaratGandhinagarग	GudžarátGándhínagarGujaratGandhinagarग	k?
<g/>
ु	ु	k?
<g/>
ज	ज	k?
<g/>
ा	ा	k?
<g/>
त	त	k?
/	/	kIx~
Gujarā	Gujarā	k1gMnSc1
<g/>
ा	ा	k?
<g/>
ँ	ँ	k?
<g/>
ध	ध	k?
<g/>
ी	ी	k?
<g/>
न	न	k?
/	/	kIx~
Gā	Gā	k1gMnSc1
<g/>
̃	̃	k?
<g/>
dhī	dhī	k6eAd1
ગ	ગ	k?
<g/>
ુ	ુ	k?
<g/>
જ	જ	k?
<g/>
ા	ા	k?
<g/>
ત	ત	k?
/	/	kIx~
Gujarā	Gujarā	k1gMnSc1
ગ	ગ	k?
<g/>
ા	ા	k?
<g/>
ં	ં	k?
<g/>
ધ	ધ	k?
<g/>
ી	ી	k?
<g/>
ન	ન	k?
/	/	kIx~
Gā	Gā	k1gMnSc1
</s>
<s>
HarijánaČandígarhHaryanaChandigarhह	HarijánaČandígarhHaryanaChandigarhह	k?
<g/>
ि	ि	k?
<g/>
य	य	k?
<g/>
ा	ा	k?
<g/>
ण	ण	k?
<g/>
ा	ा	k?
/	/	kIx~
Hariyā	Hariyā	k1gMnSc1
<g/>
ं	ं	k?
<g/>
ड	ड	k?
<g/>
ी	ी	k?
<g/>
ग	ग	k?
<g/>
़	़	k?
/	/	kIx~
Čamdī	Čamdī	k1gMnSc1
</s>
<s>
HimáčalpradéšŠimlaHimachal	HimáčalpradéšŠimlaHimachat	k5eAaImAgMnS,k5eAaPmAgMnS
PradeshShimlaह	PradeshShimlaह	k1gMnSc1
<g/>
ि	ि	k?
<g/>
म	म	k?
<g/>
ा	ा	k?
<g/>
च	च	k?
प	प	k?
<g/>
्	्	k?
<g/>
र	र	k?
<g/>
े	े	k?
<g/>
श	श	k?
/	/	kIx~
Himā	Himā	k1gMnSc1
Pradešश	Pradešश	k1gMnSc1
<g/>
ि	ि	k?
<g/>
म	म	k?
<g/>
ा	ा	k?
/	/	kIx~
Šimlā	Šimlā	k1gMnSc1
</s>
<s>
KarnátakaBengalúrKarnatakaBangaloreक	KarnátakaBengalúrKarnatakaBangaloreक	k?
<g/>
्	्	k?
<g/>
न	न	k?
<g/>
ा	ा	k?
<g/>
ट	ट	k?
/	/	kIx~
Karnā	Karnā	k1gMnSc1
<g/>
ं	ं	k?
<g/>
ग	ग	k?
<g/>
ू	ू	k?
<g/>
र	र	k?
/	/	kIx~
Bamgalū	Bamgalū	k1gMnSc1
ಕ	ಕ	k?
<g/>
್	್	k?
<g/>
ನ	ನ	k?
<g/>
ಾ	ಾ	k?
<g/>
ಟ	ಟ	k?
/	/	kIx~
Karnā	Karnā	k1gMnSc1
ಬ	ಬ	k?
<g/>
ೆ	ೆ	k?
<g/>
ಂ	ಂ	k?
<g/>
ಗ	ಗ	k?
<g/>
ೂ	ೂ	k?
<g/>
ರ	ರ	k?
<g/>
ು	ು	k?
/	/	kIx~
Bemgaļ	Bemgaļ	k1gInSc2
</s>
<s>
KéralaTiruvanantapuramKeralaTrivandrumक	KéralaTiruvanantapuramKeralaTrivandrumक	k?
<g/>
े	े	k?
<g/>
र	र	k?
/	/	kIx~
Keralaत	Keralaत	k1gMnSc1
<g/>
ि	ि	k?
<g/>
र	र	k?
<g/>
ु	ु	k?
<g/>
व	व	k?
<g/>
ं	ं	k?
<g/>
त	त	k?
<g/>
ु	ु	k?
<g/>
र	र	k?
/	/	kIx~
Tiruvanamtapurammalajálamsky	Tiruvanamtapurammalajálamsky	k1gMnSc1
ക	ക	k?
<g/>
േ	േ	k?
<g/>
ര	ര	k?
<g/>
ം	ം	k?
/	/	kIx~
Kē	Kē	k1gMnSc1
ത	ത	k?
<g/>
ി	ി	k?
<g/>
ര	ര	k?
<g/>
ു	ു	k?
<g/>
വ	വ	k?
<g/>
്	്	k?
<g/>
ത	ത	k?
<g/>
ു	ു	k?
<g/>
ര	ര	k?
<g/>
ം	ം	k?
/	/	kIx~
Tiruvanantapuram	Tiruvanantapuram	k1gInSc1
</s>
<s>
LadakLéhLadakhLehल	LadakLéhLadakhLehल	k?
<g/>
्	्	k?
<g/>
द	द	k?
<g/>
ा	ा	k?
<g/>
ख	ख	k?
<g/>
़	़	k?
/	/	kIx~
Laddā	Laddā	k1gMnSc1
<g/>
े	े	k?
<g/>
ह	ह	k?
/	/	kIx~
Lē	Lē	k1gFnSc2
ལ	ལ	k?
<g/>
་	་	k?
<g/>
ད	ད	k?
<g/>
ྭ	ྭ	k?
<g/>
ག	ག	k?
/	/	kIx~
la	la	k1gNnSc1
<g/>
'	'	kIx"
<g/>
dwagsladaksky	dwagsladaksky	k6eAd1
ག	ག	k?
<g/>
ླ	ླ	k?
<g/>
ེ	ེ	k?
<g/>
་	་	k?
</s>
<s>
LakadivyKavarattiLakshadweepKavarattiल	LakadivyKavarattiLakshadweepKavarattiल	k?
<g/>
्	्	k?
<g/>
ष	ष	k?
<g/>
्	्	k?
<g/>
व	व	k?
<g/>
ी	ी	k?
<g/>
प	प	k?
/	/	kIx~
Lakśadvī	Lakśadvī	k1gMnSc1
<g/>
्	्	k?
<g/>
त	त	k?
<g/>
ी	ी	k?
/	/	kIx~
Kavarattī	Kavarattī	k1gMnSc1
ല	ല	k?
<g/>
്	്	k?
<g/>
ഷ	ഷ	k?
<g/>
്	്	k?
<g/>
വ	വ	k?
<g/>
ീ	ീ	k?
<g/>
പ	പ	k?
<g/>
്	്	k?
/	/	kIx~
Lakşadvī	Lakşadvī	k1gMnSc1
ക	ക	k?
<g/>
്	്	k?
<g/>
ത	ത	k?
<g/>
ി	ി	k?
/	/	kIx~
Kavaratti	Kavaratť	k1gFnSc2
</s>
<s>
MadhjapradéšBhópálMadhya	MadhjapradéšBhópálMadhy	k2eAgFnSc1d1
PradeshBhopal	PradeshBhopal	k1gFnSc1
<g/>
]	]	kIx)
<g/>
म	म	k?
<g/>
्	्	k?
<g/>
य	य	k?
प	प	k?
<g/>
्	्	k?
<g/>
र	र	k?
<g/>
े	े	k?
<g/>
श	श	k?
/	/	kIx~
Madhya	Madhya	k1gMnSc1
Pradešभ	Pradešभ	k1gMnSc1
<g/>
ो	ो	k?
<g/>
प	प	k?
<g/>
ा	ा	k?
<g/>
ल	ल	k?
/	/	kIx~
Bhopā	Bhopā	k1gMnSc1
</s>
<s>
MaháráštraBombajMaharashtraMumbai	MaháráštraBombajMaharashtraMumbai	k1gNnSc1
(	(	kIx(
<g/>
Bombay	Bombaa	k1gFnPc1
<g/>
)	)	kIx)
<g/>
म	म	k?
<g/>
ा	ा	k?
<g/>
र	र	k?
<g/>
ा	ा	k?
<g/>
ष	ष	k?
<g/>
्	्	k?
<g/>
ट	ट	k?
<g/>
्	्	k?
<g/>
र	र	k?
/	/	kIx~
Mahā	Mahā	k1gMnSc1
<g/>
ु	ु	k?
<g/>
ं	ं	k?
<g/>
ब	ब	k?
/	/	kIx~
Mumbaī	Mumbaī	k1gFnSc2
म	म	k?
<g/>
ा	ा	k?
<g/>
र	र	k?
<g/>
ा	ा	k?
<g/>
ष	ष	k?
<g/>
्	्	k?
<g/>
ट	ट	k?
<g/>
्	्	k?
<g/>
र	र	k?
/	/	kIx~
Mahā	Mahā	k1gFnSc2
म	म	k?
<g/>
ु	ु	k?
<g/>
ं	ं	k?
<g/>
ब	ब	k?
/	/	kIx~
Mumbaī	Mumbaī	k1gMnSc1
</s>
<s>
ManípurImphálManipurImphalम	ManípurImphálManipurImphalम	k?
<g/>
ि	ि	k?
<g/>
प	प	k?
<g/>
ु	ु	k?
<g/>
र	र	k?
/	/	kIx~
Manipurइ	Manipurइ	k1gMnSc1
<g/>
्	्	k?
<g/>
फ	फ	k?
<g/>
ा	ा	k?
<g/>
ल	ल	k?
/	/	kIx~
Imphā	Imphā	k1gMnSc1
ম	ম	k?
<g/>
ি	ি	k?
<g/>
প	প	k?
<g/>
ু	ু	k?
<g/>
র	র	k?
/	/	kIx~
Manipurmanipursky	Manipurmanipursky	k1gMnSc1
ই	ই	k?
<g/>
া	া	k?
<g/>
ল	ল	k?
/	/	kIx~
Imphā	Imphā	k1gMnSc1
</s>
<s>
MéghálajŠilaungMeghalayaShillongम	MéghálajŠilaungMeghalayaShillongम	k?
<g/>
े	े	k?
<g/>
घ	घ	k?
<g/>
ा	ा	k?
<g/>
ल	ल	k?
/	/	kIx~
Meghā	Meghā	k1gMnSc1
<g/>
ि	ि	k?
<g/>
ल	ल	k?
<g/>
ॉ	ॉ	k?
<g/>
ं	ं	k?
<g/>
ग	ग	k?
/	/	kIx~
Šilò	Šilò	k1gMnSc1
</s>
<s>
MizóramÁizalMizoramAizawlम	MizóramÁizalMizoramAizawlम	k?
<g/>
ि	ि	k?
<g/>
ज	ज	k?
<g/>
़	़	k?
<g/>
ो	ो	k?
<g/>
र	र	k?
/	/	kIx~
Mizoramऐ	Mizoramऐ	k1gMnSc1
<g/>
़	़	k?
<g/>
ौ	ौ	k?
<g/>
ल	ल	k?
/	/	kIx~
Æ	Æ	k1gMnSc1
</s>
<s>
NágálandKóhímaNagalandKohimaन	NágálandKóhímaNagalandKohimaन	k?
<g/>
ा	ा	k?
<g/>
ग	ग	k?
<g/>
ा	ा	k?
<g/>
ल	ल	k?
<g/>
ै	ै	k?
<g/>
ं	ं	k?
<g/>
ड	ड	k?
/	/	kIx~
Nā	Nā	k1gMnSc1
<g/>
ो	ो	k?
<g/>
ह	ह	k?
<g/>
ि	ि	k?
<g/>
म	म	k?
<g/>
ा	ा	k?
/	/	kIx~
Kohimā	Kohimā	k1gMnSc1
</s>
<s>
PaňdžábČandígarhPunjabChandigarhप	PaňdžábČandígarhPunjabChandigarhप	k?
<g/>
ं	ं	k?
<g/>
ज	ज	k?
<g/>
ा	ा	k?
<g/>
ब	ब	k?
/	/	kIx~
Pamjā	Pamjā	k1gMnSc1
<g/>
ं	ं	k?
<g/>
ड	ड	k?
<g/>
ी	ी	k?
<g/>
ग	ग	k?
<g/>
़	़	k?
/	/	kIx~
Čamdī	Čamdī	k1gMnSc1
ਪ	ਪ	k?
<g/>
ੰ	ੰ	k?
<g/>
ਜ	ਜ	k?
<g/>
ਾ	ਾ	k?
<g/>
ਬ	ਬ	k?
/	/	kIx~
Pamjā	Pamjā	k1gMnSc1
ਚ	ਚ	k?
<g/>
ੰ	ੰ	k?
<g/>
ਡ	ਡ	k?
<g/>
ੀ	ੀ	k?
<g/>
ਗ	ਗ	k?
<g/>
਼	਼	k?
/	/	kIx~
Čamdī	Čamdī	k1gMnSc1
</s>
<s>
PuduččériPuduččériPondicherryPondicherryप	PuduččériPuduččériPondicherryPondicherryप	k?
<g/>
ॉ	ॉ	k?
<g/>
ं	ं	k?
<g/>
ड	ड	k?
<g/>
ि	ि	k?
<g/>
च	च	k?
<g/>
े	े	k?
<g/>
र	र	k?
<g/>
ी	ी	k?
/	/	kIx~
Pò	Pò	k1gMnSc1
<g/>
ॉ	ॉ	k?
<g/>
ं	ं	k?
<g/>
ड	ड	k?
<g/>
ि	ि	k?
<g/>
च	च	k?
<g/>
े	े	k?
<g/>
र	र	k?
<g/>
ी	ी	k?
/	/	kIx~
Pò	Pò	k1gMnSc1
ப	ப	k?
<g/>
ா	ா	k?
<g/>
ண	ண	k?
<g/>
்	்	k?
<g/>
ட	ட	k?
<g/>
ி	ி	k?
<g/>
ச	ச	k?
<g/>
்	்	k?
<g/>
ச	ச	k?
<g/>
ே	ே	k?
<g/>
ர	ர	k?
<g/>
ி	ி	k?
/	/	kIx~
Pā	Pā	k1gMnSc1
ப	ப	k?
<g/>
ா	ா	k?
<g/>
ண	ண	k?
<g/>
்	்	k?
<g/>
ட	ட	k?
<g/>
ி	ி	k?
<g/>
ச	ச	k?
<g/>
்	்	k?
<g/>
ச	ச	k?
<g/>
ே	ே	k?
<g/>
ர	ர	k?
<g/>
ி	ி	k?
/	/	kIx~
Pā	Pā	k1gFnSc2
</s>
<s>
RádžasthánPžajpurRajasthanJaipurर	RádžasthánPžajpurRajasthanJaipurर	k?
<g/>
ा	ा	k?
<g/>
ज	ज	k?
<g/>
्	्	k?
<g/>
थ	थ	k?
<g/>
ा	ा	k?
<g/>
न	न	k?
/	/	kIx~
Rā	Rā	k1gMnSc1
<g/>
ु	ु	k?
<g/>
र	र	k?
/	/	kIx~
Jaypur	Jaypur	k1gMnSc1
</s>
<s>
SikkimGángtókSikkimGangtokस	SikkimGángtókSikkimGangtokस	k?
<g/>
ि	ि	k?
<g/>
क	क	k?
<g/>
्	्	k?
<g/>
क	क	k?
<g/>
ि	ि	k?
<g/>
म	म	k?
/	/	kIx~
Sikkimग	Sikkimग	k1gMnSc1
<g/>
ं	ं	k?
<g/>
ग	ग	k?
<g/>
ो	ो	k?
<g/>
क	क	k?
/	/	kIx~
Gamgţoknepálsky	Gamgţoknepálsky	k1gMnSc1
स	स	k?
<g/>
ि	ि	k?
<g/>
क	क	k?
<g/>
्	्	k?
<g/>
क	क	k?
<g/>
ि	ि	k?
<g/>
म	म	k?
/	/	kIx~
Sikkimnepálsky	Sikkimnepálsky	k1gMnSc1
ग	ग	k?
<g/>
ं	ं	k?
<g/>
ग	ग	k?
<g/>
ो	ो	k?
<g/>
क	क	k?
/	/	kIx~
Gamgţok	Gamgţok	k1gInSc1
</s>
<s>
TamilnáduMadrásTamil	TamilnáduMadrásTamit	k5eAaPmAgMnS
NaduChennai	NaduChennae	k1gFnSc4
(	(	kIx(
<g/>
Madras	madras	k1gInSc4
<g/>
)	)	kIx)
<g/>
त	त	k?
<g/>
ि	ि	k?
<g/>
ल	ल	k?
<g/>
ा	ा	k?
<g/>
ड	ड	k?
<g/>
ु	ु	k?
/	/	kIx~
Tamilnā	Tamilnā	k1gMnSc1
<g/>
े	े	k?
<g/>
न	न	k?
<g/>
्	्	k?
<g/>
न	न	k?
/	/	kIx~
Čennaitamilsky	Čennaitamilsky	k1gMnSc1
த	த	k?
<g/>
ி	ி	k?
<g/>
ழ	ழ	k?
<g/>
்	்	k?
<g/>
ந	ந	k?
<g/>
ா	ா	k?
<g/>
ட	ட	k?
<g/>
ு	ு	k?
/	/	kIx~
Tamiļ	Tamiļ	k1gMnSc1
ச	ச	k?
<g/>
ெ	ெ	k?
<g/>
ன	ன	k?
<g/>
்	்	k?
<g/>
ன	ன	k?
<g/>
ை	ை	k?
/	/	kIx~
Čeņ	Čeņ	k1gFnSc2
</s>
<s>
TelangánaHajdarábádTelanganaHyderabadत	TelangánaHajdarábádTelanganaHyderabadत	k?
<g/>
े	े	k?
<g/>
ल	ल	k?
<g/>
ं	ं	k?
<g/>
ग	ग	k?
<g/>
ा	ा	k?
<g/>
न	न	k?
<g/>
ा	ा	k?
<g/>
ह	ह	k?
<g/>
ै	ै	k?
<g/>
द	द	k?
<g/>
ा	ा	k?
<g/>
ब	ब	k?
<g/>
ा	ा	k?
<g/>
द	द	k6eAd1
త	త	k?
<g/>
ె	ె	k?
<g/>
ల	ల	k?
<g/>
ం	ం	k?
<g/>
గ	గ	k?
<g/>
ా	ా	k?
<g/>
ణ	ణ	k?
/	/	kIx~
Telaṅ	Telaṅ	k1gMnSc1
హ	హ	k?
<g/>
ై	ై	k?
<g/>
ద	ద	k?
<g/>
ా	ా	k?
<g/>
బ	బ	k?
<g/>
ా	ా	k?
<g/>
ద	ద	k?
<g/>
ు	ు	k?
</s>
<s>
TripuraAgartalaTripuraAgartalaत	TripuraAgartalaTripuraAgartalaत	k?
<g/>
्	्	k?
<g/>
र	र	k?
<g/>
ि	ि	k?
<g/>
प	प	k?
<g/>
ु	ु	k?
<g/>
र	र	k?
<g/>
ा	ा	k?
/	/	kIx~
Tripurā	Tripurā	k1gMnSc1
<g/>
ा	ा	k?
/	/	kIx~
Agartalā	Agartalā	k1gMnSc1
ত	ত	k?
<g/>
্	্	k?
<g/>
র	র	k?
<g/>
ি	ি	k?
<g/>
প	প	k?
<g/>
ু	ু	k?
<g/>
র	র	k?
<g/>
া	া	k?
/	/	kIx~
Tripurā	Tripurā	k1gMnSc1
আ	আ	k?
<g/>
া	া	k?
<g/>
র	র	k?
<g/>
্	্	k?
<g/>
ত	ত	k?
<g/>
া	া	k?
<g/>
ল	ল	k?
<g/>
া	া	k?
/	/	kIx~
Ā	Ā	k?
</s>
<s>
UrísaBhuvanéšvarOrissaBhubaneshwarउ	UrísaBhuvanéšvarOrissaBhubaneshwarउ	k?
<g/>
़	़	k?
<g/>
ी	ी	k?
<g/>
स	स	k?
<g/>
ा	ा	k?
/	/	kIx~
Urī	Urī	k1gMnSc1
<g/>
ु	ु	k?
<g/>
व	व	k?
<g/>
े	े	k?
<g/>
श	श	k?
<g/>
्	्	k?
<g/>
व	व	k?
/	/	kIx~
Bhuvanešvarurijsky	Bhuvanešvarurijsky	k1gMnSc1
ଓ	ଓ	k?
<g/>
଼	଼	k?
<g/>
ି	ି	k?
<g/>
ଶ	ଶ	k?
<g/>
ା	ା	k?
/	/	kIx~
Orišā	Orišā	k1gMnSc1
ଭ	ଭ	k?
<g/>
ୁ	ୁ	k?
<g/>
ବ	ବ	k?
<g/>
େ	େ	k?
<g/>
ଶ	ଶ	k?
/	/	kIx~
Bhubanešbar	Bhubanešbar	k1gMnSc1
</s>
<s>
UttarákhandDéhrádúnUttarakhandDehra	UttarákhandDéhrádúnUttarakhandDehra	k1gFnSc1
Dunउ	Dunउ	k1gFnSc2
<g/>
्	्	k?
<g/>
त	त	k?
<g/>
ा	ा	k?
<g/>
ख	ख	k?
<g/>
ं	ं	k?
<g/>
ड	ड	k?
/	/	kIx~
Uttarakhandद	Uttarakhandद	k1gMnSc1
<g/>
े	े	k?
<g/>
ह	ह	k?
<g/>
ा	ा	k?
<g/>
द	द	k?
<g/>
ू	ू	k?
<g/>
न	न	k?
/	/	kIx~
Dehrā	Dehrā	k1gMnSc1
</s>
<s>
UttarpradéšLakhnaúUttar	UttarpradéšLakhnaúUttar	k1gMnSc1
PradeshLucknowउ	PradeshLucknowउ	k1gMnSc1
<g/>
्	्	k?
<g/>
त	त	k?
प	प	k?
<g/>
्	्	k?
<g/>
र	र	k?
<g/>
े	े	k?
<g/>
श	श	k?
/	/	kIx~
Uttar	Uttar	k1gMnSc1
Pradešल	Pradešल	k1gMnSc1
/	/	kIx~
Lakhnaū	Lakhnaū	k1gMnSc1
</s>
<s>
Západní	západní	k2eAgInSc1d1
BengálskoKalkataWest	BengálskoKalkataWest	k1gInSc1
BengalKolkata	BengalKolkata	k1gFnSc1
(	(	kIx(
<g/>
Calcutta	Calcutta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
प	प	k?
<g/>
्	्	k?
<g/>
च	च	k?
<g/>
ि	ि	k?
<g/>
म	म	k?
ब	ब	k?
<g/>
ं	ं	k?
<g/>
ग	ग	k?
<g/>
ा	ा	k?
<g/>
ल	ल	k?
/	/	kIx~
Paščim	Paščim	k1gMnSc1
Bamgā	Bamgā	k1gMnSc1
<g/>
ो	ो	k?
<g/>
ल	ल	k?
<g/>
ा	ा	k?
<g/>
त	त	k?
<g/>
ा	ा	k?
/	/	kIx~
Kolkā	Kolkā	k1gMnSc1
প	প	k?
<g/>
্	্	k?
<g/>
চ	চ	k?
<g/>
ি	ি	k?
<g/>
ম	ম	k?
ব	ব	k?
<g/>
্	্	k?
<g/>
গ	গ	k?
/	/	kIx~
Pôščim	Pôščim	k1gMnSc1
Bôngôbengálsky	Bôngôbengálsky	k1gMnSc1
ক	ক	k?
<g/>
া	া	k?
<g/>
ত	ত	k?
<g/>
া	া	k?
/	/	kIx~
Kôlkā	Kôlkā	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Ándhrapradéš	Ándhrapradéš	k1gMnSc1
byl	být	k5eAaImAgMnS
rozdělen	rozdělit	k5eAaPmNgMnS
na	na	k7c4
dva	dva	k4xCgInPc4
státy	stát	k1gInPc4
<g/>
,	,	kIx,
Telangána	Telangána	k1gFnSc1
a	a	k8xC
zbytek	zbytek	k1gInSc1
Ándhrapradéše	Ándhrapradéše	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Hajdarábád	Hajdarábád	k1gInSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
nového	nový	k2eAgInSc2d1
státu	stát	k1gInSc2
Telángana	Telángana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
zůstane	zůstat	k5eAaPmIp3nS
po	po	k7c4
dobu	doba	k1gFnSc4
maximálně	maximálně	k6eAd1
10	#num#	k4
let	léto	k1gNnPc2
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
i	i	k8xC
Ándhrapradéše	Ándhrapradéše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřady	úřada	k1gMnSc2
Ándhrapradéše	Ándhrapradéš	k1gMnSc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stěhují	stěhovat	k5eAaImIp3nP
do	do	k7c2
Amaravati	Amaravati	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
do	do	k7c2
Vijayawada	Vijayawada	k1gFnSc1
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Hodnoty	hodnota	k1gFnPc1
zahrnují	zahrnovat	k5eAaImIp3nP
i	i	k9
území	území	k1gNnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Kašmíru	Kašmír	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
si	se	k3xPyFc3
Indie	Indie	k1gFnSc1
nárokuje	nárokovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
nemá	mít	k5eNaImIp3nS
nad	nad	k7c7
nimi	on	k3xPp3gFnPc7
faktickou	faktický	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pákistánský	pákistánský	k2eAgInSc4d1
Ázád	Ázád	k1gInSc4
Kašmír	Kašmír	k1gInSc1
(	(	kIx(
<g/>
13	#num#	k4
297	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
Indie	Indie	k1gFnSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
součást	součást	k1gFnSc4
svazového	svazový	k2eAgNnSc2d1
teritoria	teritorium	k1gNnSc2
Džammú	Džammú	k1gFnSc2
a	a	k8xC
Kašmír	Kašmír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pákistnáský	pákistnáský	k2eAgInSc1d1
Gilgit-Baltistán	Gilgit-Baltistán	k1gInSc1
(	(	kIx(
<g/>
72	#num#	k4
971	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
a	a	k8xC
čínskou	čínský	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
Aksai	Aksai	k1gNnSc2
Čin	čina	k1gFnPc2
(	(	kIx(
<g/>
37	#num#	k4
555	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
teritoria	teritorium	k1gNnSc2
Ladak	Ladak	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
České	český	k2eAgInPc1d1
názvy	název	k1gInPc1
států	stát	k1gInPc2
podle	podle	k7c2
knihy	kniha	k1gFnSc2
Stranda	Strando	k1gNnSc2
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Indie	Indie	k1gFnSc2
(	(	kIx(
<g/>
Lidové	lidový	k2eAgFnPc1d1
Noviny	novina	k1gFnPc1
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
k	k	k7c3
jejich	jejich	k3xOp3gInPc3
přepisům	přepis	k1gInPc3
viz	vidět	k5eAaImRp2nS
poznámka	poznámka	k1gFnSc1
na	na	k7c6
konci	konec	k1gInSc6
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
code	code	k1gInSc1
<g/>
:	:	kIx,
<g/>
3166	#num#	k4
<g/>
:	:	kIx,
<g/>
IN	IN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.iso.org	www.iso.org	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
States	States	k1gInSc1
of	of	k?
India	indium	k1gNnSc2
since	sinko	k6eAd1
1947	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.worldstatesmen.org	www.worldstatesmen.org	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
States	States	k1gMnSc1
Census	census	k1gInSc4
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.censu2011.co.in	www.censu2011.co.in	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
States	States	k1gInSc1
Census	census	k1gInSc1
2011	#num#	k4
-	-	kIx~
Chapter	Chapter	k1gInSc1
1	#num#	k4
(	(	kIx(
<g/>
Population	Population	k1gInSc1
<g/>
,	,	kIx,
Size	Size	k1gInSc1
and	and	k?
Decadal	Decadal	k1gFnSc1
Change	change	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.censu2011.co.in	www.censu2011.co.in	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Languages	Languages	k1gInSc1
in	in	k?
India	indium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.mapsofindia.com	www.mapsofindia.com	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Indian	Indiana	k1gFnPc2
Languages	Languages	k1gMnSc1
Map	mapa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.mapsofindia.com	www.mapsofindia.com	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
indické	indický	k2eAgInPc1d1
státy	stát	k1gInPc1
a	a	k8xC
teritoria	teritorium	k1gNnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
States	States	k1gMnSc1
and	and	k?
territories	territories	k1gMnSc1
of	of	k?
India	indium	k1gNnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Asie	Asie	k1gFnSc2
Země	zem	k1gFnSc2
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1
•	•	k?
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
(	(	kIx(
<g/>
Barma	Barma	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Sýrie	Sýrie	k1gFnSc2
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc4
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
•	•	k?
Vánoční	vánoční	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Indie	Indie	k1gFnSc1
</s>
