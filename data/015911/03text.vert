<s>
Neve	Neve	k1gFnSc1
Campbellová	Campbellová	k1gFnSc1
</s>
<s>
Neve	Neve	k1gFnSc1
Campbellová	Campbellová	k1gFnSc1
Neve	Neve	k1gFnSc1
Campbellová	Campbellová	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
Rodné	rodný	k2eAgInPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Neve	Nevus	k1gMnSc5
Adrianne	Adriann	k1gMnSc5
Campbell	Campbell	k1gMnSc1
Narození	narození	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1973	#num#	k4
(	(	kIx(
<g/>
47	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Guelph	Guelph	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
scenáristka	scenáristka	k1gFnSc1
<g/>
,	,	kIx,
producentka	producentka	k1gFnSc1
Výška	výška	k1gFnSc1
</s>
<s>
166	#num#	k4
cm	cm	kA
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Jeff	Jeff	k1gMnSc1
Colt	Colt	k1gMnSc1
(	(	kIx(
<g/>
1995	#num#	k4
-	-	kIx~
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
John	John	k1gMnSc1
Light	Light	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
-	-	kIx~
2011	#num#	k4
<g/>
)	)	kIx)
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
JJ	JJ	kA
Feild	Feild	k1gMnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
2	#num#	k4
Rodiče	rodič	k1gMnPc1
</s>
<s>
Marnie	Marnie	k1gFnSc1
CampbellGerry	CampbellGerra	k1gFnSc2
Campbell	Campbell	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Neve	Neve	k1gFnSc1
Campbellová	Campbellová	k1gFnSc1
(	(	kIx(
<g/>
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Neve	Nev	k1gFnSc2
Adrianne	Adriann	k1gInSc5
Campbell	Campbell	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
*	*	kIx~
1973	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kanadská	kanadský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
producentka	producentka	k1gFnSc1
a	a	k8xC
scenáristka	scenáristka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodila	narodit	k5eAaPmAgFnS
se	s	k7c7
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1973	#num#	k4
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
ve	v	k7c6
městě	město	k1gNnSc6
Guelph	Guelph	k1gInSc4
Gerrymu	Gerrym	k1gInSc2
a	a	k8xC
Marnie	Marnie	k1gFnSc2
Campbellovým	Campbellová	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
ze	z	k7c2
Skotska	Skotsko	k1gNnSc2
a	a	k8xC
matka	matka	k1gFnSc1
z	z	k7c2
Holandska	Holandsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
její	její	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
rozvedli	rozvést	k5eAaPmAgMnP
a	a	k8xC
Neve	Neve	k1gInSc4
se	se	k3xPyFc4
s	s	k7c7
otcem	otec	k1gMnSc7
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
Toronta	Toronto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
bratra	bratr	k1gMnSc4
Christiana	Christian	k1gMnSc4
a	a	k8xC
nevlastní	vlastní	k2eNgMnPc4d1
bratry	bratr	k1gMnPc4
Alexe	Alex	k1gMnSc5
Campbella	Campbell	k1gMnSc4
a	a	k8xC
Damiana	Damian	k1gMnSc4
McDonalda	McDonald	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
devíti	devět	k4xCc6
letech	léto	k1gNnPc6
získala	získat	k5eAaPmAgFnS
místo	místo	k6eAd1
v	v	k7c6
celostátní	celostátní	k2eAgFnSc6d1
Kanadské	kanadský	k2eAgFnSc6d1
baletní	baletní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
tanečnímu	taneční	k2eAgInSc3d1
talentu	talent	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
ale	ale	k9
zaměřila	zaměřit	k5eAaPmAgFnS
na	na	k7c6
herectví	herectví	k1gNnSc6
kvůli	kvůli	k7c3
častým	častý	k2eAgNnPc3d1
zraněním	zranění	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
seriálu	seriál	k1gInSc6
Catwalk	Catwalka	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
v	v	k7c6
seriálu	seriál	k1gInSc6
Party	parta	k1gFnSc2
of	of	k?
Five	Fiv	k1gFnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
byla	být	k5eAaImAgFnS
obsazena	obsadit	k5eAaPmNgFnS
do	do	k7c2
známé	známý	k2eAgFnSc2d1
role	role	k1gFnSc2
Sidney	Sidnea	k1gFnSc2
Prescottové	Prescottová	k1gFnSc2
do	do	k7c2
hororové	hororový	k2eAgFnSc2d1
série	série	k1gFnSc2
Vřískot	vřískot	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
vdala	vdát	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
za	za	k7c4
Jeffa	Jeff	k1gMnSc4
Colta	Colt	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
toto	tento	k3xDgNnSc1
manželství	manželství	k1gNnSc1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
nevydařilo	vydařit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
tak	tak	k8xS,k8xC
druhé	druhý	k4xOgFnPc1
s	s	k7c7
Johnem	John	k1gMnSc7
Lightem	Light	k1gMnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
započalo	započnout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
je	být	k5eAaImIp3nS
jejím	její	k3xOp3gMnSc7
přítelem	přítel	k1gMnSc7
J.	J.	kA
J.	J.	kA
Feild	Feilda	k1gFnPc2
<g/>
,	,	kIx,
za	za	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
se	se	k3xPyFc4
zatím	zatím	k6eAd1
nevdala	vdát	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodilo	narodit	k5eAaPmAgNnS
dítě	dítě	k1gNnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
narození	narození	k1gNnSc2
se	se	k3xPyFc4
Neve	Neve	k1gInSc1
na	na	k7c4
pár	pár	k4xCyI
týdnů	týden	k1gInPc2
podařilo	podařit	k5eAaPmAgNnS
utajit	utajit	k5eAaPmF
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
2018	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
prostřednictvím	prostřednictvím	k7c2
svého	svůj	k3xOyFgInSc2
profilu	profil	k1gInSc2
na	na	k7c6
Instagramu	Instagram	k1gInSc6
adopci	adopce	k1gFnSc4
chlapečka	chlapeček	k1gMnSc4
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yQgMnSc3,k3yIgMnSc3
dali	dát	k5eAaPmAgMnP
jméno	jméno	k1gNnSc4
Raynor	Raynora	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vybraná	vybraný	k2eAgFnSc1d1
filmografie	filmografie	k1gFnSc1
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Year	Year	k1gMnSc1
</s>
<s>
Title	titla	k1gFnSc3
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Notes	notes	k1gInSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
Bestie	bestie	k1gFnSc1
</s>
<s>
Officer	Officer	k1gMnSc1
Jesse	Jess	k1gMnSc2
Donovan	Donovan	k1gMnSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
Paint	Paint	k1gInSc1
Cans	Cansa	k1gFnPc2
</s>
<s>
Tristesse	Tristesse	k6eAd1
</s>
<s>
1994	#num#	k4
</s>
<s>
The	The	k?
Passion	Passion	k1gInSc1
of	of	k?
John	John	k1gMnSc1
Ruskin	Ruskin	k1gMnSc1
</s>
<s>
Effie	Effie	k1gFnSc1
Gray	Graa	k1gFnSc2
</s>
<s>
Krátký	krátký	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
1996	#num#	k4
</s>
<s>
Love	lov	k1gInSc5
Child	Child	k1gMnSc1
</s>
<s>
Deidre	Deidr	k1gInSc5
</s>
<s>
1996	#num#	k4
</s>
<s>
Čarodějky	čarodějka	k1gFnPc1
</s>
<s>
Bonnie	Bonnie	k1gFnSc1
Harper	Harpra	k1gFnPc2
</s>
<s>
1996	#num#	k4
</s>
<s>
Vřískot	vřískot	k1gInSc1
</s>
<s>
Sidney	Sidnea	k1gFnPc1
Prescott	Prescotta	k1gFnPc2
</s>
<s>
1997	#num#	k4
</s>
<s>
Vřískot	vřískot	k1gInSc1
2	#num#	k4
</s>
<s>
Sidney	Sidnea	k1gFnPc1
Prescott	Prescotta	k1gFnPc2
</s>
<s>
1998	#num#	k4
</s>
<s>
Nebezpečné	bezpečný	k2eNgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Suzie	Suzie	k1gFnSc1
Marie	Maria	k1gFnSc2
Toller	Toller	k1gInSc1
</s>
<s>
1998	#num#	k4
</s>
<s>
Klub	klub	k1gInSc1
54	#num#	k4
</s>
<s>
Julie	Julie	k1gFnSc1
Black	Blacka	k1gFnPc2
</s>
<s>
1998	#num#	k4
</s>
<s>
Příliš	příliš	k6eAd1
jednoduché	jednoduchý	k2eAgNnSc1d1
</s>
<s>
Renée	René	k1gMnPc4
Weber	Weber	k1gMnSc1
</s>
<s>
1998	#num#	k4
</s>
<s>
Lví	lví	k2eAgMnSc1d1
král	král	k1gMnSc1
2	#num#	k4
<g/>
:	:	kIx,
Simbův	Simbův	k2eAgInSc1d1
příběh	příběh	k1gInSc1
</s>
<s>
Kiara	Kiara	k1gFnSc1
(	(	kIx(
<g/>
voice	voice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Direct-to-video	Direct-to-video	k6eAd1
</s>
<s>
1999	#num#	k4
</s>
<s>
Tři	tři	k4xCgMnPc1
do	do	k7c2
Tanga	tango	k1gNnSc2
</s>
<s>
Amy	Amy	k?
Post	post	k1gInSc1
</s>
<s>
2000	#num#	k4
</s>
<s>
460	#num#	k4
podezřelých	podezřelý	k2eAgMnPc2d1
</s>
<s>
Ellen	Ellen	k2eAgInSc1d1
Rash	Rash	k1gInSc1
</s>
<s>
2000	#num#	k4
</s>
<s>
Panika	panika	k1gFnSc1
</s>
<s>
Sarah	Sarah	k1gFnSc1
Cassidy	Cassida	k1gFnSc2
</s>
<s>
2000	#num#	k4
</s>
<s>
Vřískot	vřískot	k1gInSc1
3	#num#	k4
</s>
<s>
Sidney	Sidnea	k1gFnPc1
Prescott	Prescotta	k1gFnPc2
</s>
<s>
2002	#num#	k4
</s>
<s>
Tajemství	tajemství	k1gNnSc1
sexu	sex	k1gInSc2
</s>
<s>
Alice	Alice	k1gFnSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
Ztráta	ztráta	k1gFnSc1
souvislostí	souvislost	k1gFnPc2
</s>
<s>
Missy	Missa	k1gFnPc1
Lofton	Lofton	k1gInSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
The	The	k?
Company	Compan	k1gInPc1
</s>
<s>
Loretta	Loretta	k1gMnSc1
"	"	kIx"
<g/>
Ry	Ry	k1gMnSc1
<g/>
"	"	kIx"
Ryan	Ryan	k1gMnSc1
</s>
<s>
Zároveň	zároveň	k6eAd1
napsala	napsat	k5eAaBmAgFnS,k5eAaPmAgFnS
a	a	k8xC
produkovala	produkovat	k5eAaImAgFnS
</s>
<s>
2003	#num#	k4
</s>
<s>
Spiknutí	spiknutí	k1gNnSc1
</s>
<s>
Chloe	Chloe	k1gFnSc1
Richards	Richardsa	k1gFnPc2
</s>
<s>
2004	#num#	k4
</s>
<s>
Milenka	Milenka	k1gFnSc1
</s>
<s>
Vera	Ver	k2eAgFnSc1d1
Barrie	Barrie	k1gFnSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
Fešák	fešák	k1gMnSc1
Churchill	Churchill	k1gMnSc1
</s>
<s>
Princess	Princess	k6eAd1
Elizabeth	Elizabeth	k1gFnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Našli	najít	k5eAaPmAgMnP
mě	já	k3xPp1nSc4
naši	naši	k1gMnPc1
</s>
<s>
Ellen	Ellen	k1gInSc1
Minola	Minola	k1gFnSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Hranice	hranice	k1gFnSc1
</s>
<s>
Margaret	Margareta	k1gFnPc2
Stilwell	Stilwella	k1gFnPc2
</s>
<s>
2007	#num#	k4
</s>
<s>
I	i	k9
Really	Reall	k1gInPc1
Hate	Hat	k1gFnSc2
My	my	k3xPp1nPc1
Job	Job	k1gMnSc1
</s>
<s>
Abi	Abi	k?
</s>
<s>
2007	#num#	k4
</s>
<s>
Tajemství	tajemství	k1gNnSc1
prstenu	prsten	k1gInSc2
</s>
<s>
Marie	Marie	k1gFnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Agent	agent	k1gMnSc1
Crush	Crush	k1gMnSc1
</s>
<s>
Cassie	Cassie	k1gFnSc1
(	(	kIx(
<g/>
voice	voice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2011	#num#	k4
</s>
<s>
Scream	Scream	k6eAd1
4	#num#	k4
</s>
<s>
Sidney	Sidnea	k1gFnPc1
Prescott	Prescotta	k1gFnPc2
</s>
<s>
2011	#num#	k4
</s>
<s>
The	The	k?
Glass	Glass	k1gInSc1
Man	Man	k1gMnSc1
</s>
<s>
Julie	Julie	k1gFnSc1
Pyrite	pyrit	k1gInSc5
</s>
<s>
2015	#num#	k4
</s>
<s>
Walter	Walter	k1gMnSc1
</s>
<s>
Allie	Allie	k1gFnSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
Mrakodrap	mrakodrap	k1gInSc1
</s>
<s>
Sarah	Sarah	k1gFnSc1
Sawyer	Sawyra	k1gFnPc2
</s>
<s>
2018	#num#	k4
</s>
<s>
Hot	hot	k0
Air	Air	k1gFnPc2
</s>
<s>
Valerie	Valerie	k1gFnSc1
Gannon	Gannona	k1gFnPc2
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
Year	Year	k1gMnSc1
</s>
<s>
Title	titla	k1gFnSc3
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Notes	notes	k1gInSc1
</s>
<s>
1991	#num#	k4
</s>
<s>
My	my	k3xPp1nPc1
Secret	Secreta	k1gFnPc2
Identity	identita	k1gFnPc4
</s>
<s>
Student	student	k1gMnSc1
</s>
<s>
Uncredited	Uncredited	k1gInSc1
<g/>
;	;	kIx,
Epizoda	epizoda	k1gFnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Pirate	Pirat	k1gMnSc5
Radio	radio	k1gNnSc4
<g/>
"	"	kIx"
</s>
<s>
1992	#num#	k4
</s>
<s>
The	The	k?
Kids	Kids	k1gInSc1
in	in	k?
the	the	k?
Hall	Hall	k1gInSc1
</s>
<s>
Laura	Laura	k1gFnSc1
Capelli	Capelle	k1gFnSc4
</s>
<s>
Epizoda	epizoda	k1gFnSc1
<g/>
:	:	kIx,
#	#	kIx~
<g/>
3.13	3.13	k4
</s>
<s>
1992	#num#	k4
</s>
<s>
Catwalk	Catwalk	k6eAd1
</s>
<s>
Daisy	Daisa	k1gFnPc1
McKenzie	McKenzie	k1gFnSc2
</s>
<s>
4	#num#	k4
epizody	epizoda	k1gFnPc4
</s>
<s>
1994	#num#	k4
</s>
<s>
Nebezpečné	bezpečný	k2eNgNnSc1d1
manželství	manželství	k1gNnSc1
</s>
<s>
Beth	Beth	k1gMnSc1
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
The	The	k?
Forget-Me-Not	Forget-Me-Not	k1gInSc1
Murders	Murders	k1gInSc1
</s>
<s>
Jess	Jess	k1gInSc1
Foy	Foy	k1gFnSc2
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
Bojíte	bát	k5eAaImIp2nP
se	se	k3xPyFc4
tmy	tma	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s>
Nonnie	Nonnie	k1gFnSc1
Walker	Walkra	k1gFnPc2
</s>
<s>
Epizoda	epizoda	k1gFnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Tale	Tale	k1gInSc1
of	of	k?
the	the	k?
Dangerous	Dangerous	k1gMnSc1
Soup	Soup	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
1994	#num#	k4
</s>
<s>
Kung	Kung	k1gMnSc1
Fu	fu	k0
<g/>
:	:	kIx,
The	The	k1gMnSc1
Legend	legenda	k1gFnPc2
Continues	Continues	k1gMnSc1
</s>
<s>
Trish	Trish	k1gInSc1
Collins	Collinsa	k1gFnPc2
</s>
<s>
Epizoda	epizoda	k1gFnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Kundela	Kundela	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
1994	#num#	k4
</s>
<s>
Vražedná	vražedný	k2eAgFnSc1d1
paměť	paměť	k1gFnSc1
</s>
<s>
Nepeese	Nepeese	k1gFnSc1
</s>
<s>
Epizode	Epizod	k1gMnSc5
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Bari	Bari	k1gNnSc7
<g/>
"	"	kIx"
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
</s>
<s>
Správná	správný	k2eAgFnSc1d1
pětka	pětka	k1gFnSc1
</s>
<s>
Julia	Julius	k1gMnSc2
Salinger	Salinger	k1gMnSc1
</s>
<s>
142	#num#	k4
epizod	epizoda	k1gFnPc2
</s>
<s>
1995	#num#	k4
</s>
<s>
MADtv	MADtv	k1gMnSc1
</s>
<s>
Julia	Julius	k1gMnSc2
Salinger	Salinger	k1gMnSc1
</s>
<s>
Epizoda	epizoda	k1gFnSc1
<g/>
:	:	kIx,
#	#	kIx~
<g/>
1.6	1.6	k4
</s>
<s>
1996	#num#	k4
</s>
<s>
Strašidlo	strašidlo	k1gNnSc1
cantervileské	cantervileský	k2eAgFnSc2d1
</s>
<s>
Virginia	Virginium	k1gNnPc1
"	"	kIx"
<g/>
Ginny	Ginn	k1gInPc1
<g/>
"	"	kIx"
Otis	Otis	k1gInSc1
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
1997	#num#	k4
</s>
<s>
Saturday	Saturda	k1gMnPc4
Night	Nightum	k1gNnPc2
Live	Live	k1gNnSc2
</s>
<s>
Host	host	k1gMnSc1
</s>
<s>
Epizoda	epizoda	k1gFnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Neve	Neve	k1gFnSc1
Campbell	Campbell	k1gMnSc1
<g/>
/	/	kIx~
<g/>
David	David	k1gMnSc1
Bowie	Bowie	k1gFnSc2
<g/>
"	"	kIx"
</s>
<s>
2002	#num#	k4
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1
volání	volání	k1gNnSc1
</s>
<s>
Frances	Frances	k1gMnSc1
Kroll	Kroll	k1gMnSc1
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Tráva	tráva	k1gFnSc1
</s>
<s>
Miss	miss	k1gFnSc1
Poppy	Poppa	k1gFnSc2
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Médium	médium	k1gNnSc1
</s>
<s>
Debra	Debra	k6eAd1
</s>
<s>
3	#num#	k4
epizody	epizoda	k1gFnPc4
</s>
<s>
2008	#num#	k4
</s>
<s>
Burn	Burn	k1gMnSc1
Up	Up	k1gMnSc1
</s>
<s>
Holly	Holla	k1gFnPc1
</s>
<s>
2	#num#	k4
epizody	epizoda	k1gFnPc4
</s>
<s>
2009	#num#	k4
</s>
<s>
The	The	k?
Philanthropist	Philanthropist	k1gInSc1
</s>
<s>
Olivia	Olivia	k1gFnSc1
Maidstone	Maidston	k1gInSc5
</s>
<s>
8	#num#	k4
epizody	epizoda	k1gFnPc4
</s>
<s>
2009	#num#	k4
</s>
<s>
Mořský	mořský	k2eAgMnSc1d1
vlk	vlk	k1gMnSc1
</s>
<s>
Maud	Maud	k1gMnSc1
Brewster	Brewster	k1gMnSc1
</s>
<s>
Miniseries	Miniseries	k1gMnSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
Simpsonsonovi	Simpsonsonův	k2eAgMnPc1d1
</s>
<s>
Cassandra	Cassandra	k1gFnSc1
(	(	kIx(
<g/>
voice	voice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Epizoda	epizoda	k1gFnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Čarodějky	čarodějka	k1gFnPc1
ze	z	k7c2
Springfieldu	Springfield	k1gInSc2
<g/>
"	"	kIx"
</s>
<s>
2012	#num#	k4
</s>
<s>
Titanic	Titanic	k1gInSc1
<g/>
:	:	kIx,
Krev	krev	k1gFnSc1
a	a	k8xC
ocel	ocel	k1gFnSc1
</s>
<s>
Joanna	Joanna	k6eAd1
</s>
<s>
6	#num#	k4
epizod	epizoda	k1gFnPc2
</s>
<s>
2012	#num#	k4
</s>
<s>
Chirurgové	chirurg	k1gMnPc1
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Lizzie	Lizzie	k1gFnSc1
Shepherd	Shepherda	k1gFnPc2
</s>
<s>
2	#num#	k4
epizody	epizoda	k1gFnPc4
</s>
<s>
2013	#num#	k4
</s>
<s>
Slib	slib	k1gInSc1
mlčení	mlčení	k1gNnSc2
</s>
<s>
Kate	kat	k1gMnSc5
Burkholder	Burkholder	k1gMnSc1
</s>
<s>
Televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2014	#num#	k4
</s>
<s>
Mad	Mad	k?
Men	Men	k1gFnSc1
</s>
<s>
Lee	Lea	k1gFnSc3
Cabot	Cabot	k1gMnSc1
</s>
<s>
Epizode	Epizod	k1gMnSc5
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Time	Time	k1gFnSc1
Zones	Zones	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
2015	#num#	k4
</s>
<s>
Vítejte	vítat	k5eAaImRp2nP
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
</s>
<s>
Diane	Dianout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
4	#num#	k4
epizody	epizoda	k1gFnPc4
</s>
<s>
2015	#num#	k4
</s>
<s>
Šílenci	šílenec	k1gMnPc1
z	z	k7c2
Mangattanu	Mangattan	k1gInSc2
</s>
<s>
Kitty	Kitta	k1gFnPc1
Oppenheimer	Oppenheimra	k1gFnPc2
</s>
<s>
2	#num#	k4
epizody	epizoda	k1gFnPc4
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
</s>
<s>
Domek	domek	k1gInSc1
z	z	k7c2
karet	kareta	k1gFnPc2
</s>
<s>
LeAnn	LeAnn	k1gInSc1
Harvey	Harvea	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Campbellová	Campbellová	k1gFnSc1
z	z	k7c2
Vřískotu	vřískot	k1gInSc2
pár	pár	k4xCyI
týdnů	týden	k1gInPc2
po	po	k7c6
porodu	porod	k1gInSc6
vzala	vzít	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
dítě	dítě	k1gNnSc4
na	na	k7c4
pláž	pláž	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idnes	Idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
20.8	20.8	k4
<g/>
.2012	.2012	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Neve	Neve	k1gInSc1
Campbell	Campbell	k1gMnSc1
reveals	reveals	k1gInSc1
she	she	k?
adopted	adopted	k1gInSc1
a	a	k8xC
baby	baby	k1gNnSc1
boy	boy	k1gMnSc1
named	named	k1gMnSc1
Raynor	Raynor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mail	mail	k1gInSc1
Online	Onlin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-06-30	2018-06-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
An	An	k1gMnSc1
Amish	Amish	k1gMnSc1
Murder	Murder	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Lifetime	Lifetim	k1gMnSc5
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Neve	Neve	k1gFnPc2
Campbellová	Campbellová	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Neve	Nevat	k5eAaPmIp3nS
Campbellová	Campbellová	k1gFnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Neve	Neve	k1gFnSc1
Campbellová	Campbellová	k1gFnSc1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
102733	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
136076750	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7823	#num#	k4
3593	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
97037814	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
208548	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
97037814	#num#	k4
</s>
