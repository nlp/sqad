<s>
Campus	Campus	k1gInSc1	Campus
Square	square	k1gInSc1	square
je	být	k5eAaImIp3nS	být
brněnské	brněnský	k2eAgNnSc4d1	brněnské
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
společnosti	společnost	k1gFnSc2	společnost
AIG	AIG	kA	AIG
<g/>
/	/	kIx~	/
<g/>
LINCOLN	Lincoln	k1gMnSc1	Lincoln
NEW	NEW	kA	NEW
PROJECT	PROJECT	kA	PROJECT
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
otevřené	otevřený	k2eAgNnSc4d1	otevřené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
nově	nově	k6eAd1	nově
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
developerského	developerský	k2eAgInSc2d1	developerský
projektu	projekt	k1gInSc2	projekt
THE	THE	kA	THE
CAMPUS	CAMPUS	kA	CAMPUS
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Bohunice	Bohunice	k1gFnPc1	Bohunice
a	a	k8xC	a
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
kampusu	kampus	k1gInSc2	kampus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nákupním	nákupní	k2eAgNnSc7d1	nákupní
centrem	centrum	k1gNnSc7	centrum
a	a	k8xC	a
FN	FN	kA	FN
Brno	Brno	k1gNnSc1	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dopravní	dopravní	k2eAgInSc1d1	dopravní
uzel	uzel	k1gInSc1	uzel
Nemocnice	nemocnice	k1gFnPc1	nemocnice
Bohunice	Bohunice	k1gFnPc1	Bohunice
Na	na	k7c4	na
20	[number]	k4	20
000	[number]	k4	000
čtverečních	čtvereční	k2eAgInPc6d1	čtvereční
metrech	metr	k1gInPc6	metr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
značkových	značkový	k2eAgInPc2d1	značkový
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
supermarket	supermarket	k1gInSc1	supermarket
Tesco	Tesco	k6eAd1	Tesco
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
food	food	k1gInSc1	food
court	court	k1gInSc1	court
s	s	k7c7	s
několika	několik	k4yIc7	několik
restauracemi	restaurace	k1gFnPc7	restaurace
typu	typ	k1gInSc2	typ
fast	fasta	k1gFnPc2	fasta
food	food	k6eAd1	food
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
katastrální	katastrální	k2eAgFnSc4d1	katastrální
hranici	hranice	k1gFnSc4	hranice
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Brno-Bohunice	Brno-Bohunice	k1gFnSc2	Brno-Bohunice
a	a	k8xC	a
Brno-Starý	Brno-Starý	k2eAgInSc1d1	Brno-Starý
Lískovec	Lískovec	k1gInSc1	Lískovec
mezi	mezi	k7c7	mezi
zástavbou	zástavba	k1gFnSc7	zástavba
Bohunic	Bohunice	k1gFnPc2	Bohunice
<g/>
,	,	kIx,	,
Starého	Starého	k2eAgInSc2d1	Starého
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Lískovce	Lískovec	k1gInSc2	Lískovec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
ulicí	ulice	k1gFnSc7	ulice
Bítešskou	Bítešský	k2eAgFnSc7d1	Bítešská
a	a	k8xC	a
Jihlavskou	jihlavský	k2eAgFnSc7d1	Jihlavská
<g/>
,	,	kIx,	,
asi	asi	k9	asi
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
cesty	cesta	k1gFnSc2	cesta
autem	auto	k1gNnSc7	auto
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
obslužnost	obslužnost	k1gFnSc1	obslužnost
je	být	k5eAaImIp3nS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
řadou	řada	k1gFnSc7	řada
autobusových	autobusový	k2eAgFnPc2d1	autobusová
a	a	k8xC	a
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
linka	linka	k1gFnSc1	linka
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
přivedena	přiveden	k2eAgFnSc1d1	přivedena
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
území	území	k1gNnSc4	území
vnímána	vnímán	k2eAgFnSc1d1	vnímána
jako	jako	k8xC	jako
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
<g/>
.	.	kIx.	.
</s>
<s>
Subway	Subwa	k2eAgFnPc1d1	Subwa
Albi	Alb	k1gFnPc1	Alb
Camaieu	Camaieus	k1gInSc2	Camaieus
Charles	Charles	k1gMnSc1	Charles
Vögele	Vögel	k1gInSc2	Vögel
Deichmann	Deichmann	k1gMnSc1	Deichmann
dm	dm	kA	dm
drogerie	drogerie	k1gFnSc1	drogerie
FAnn	FAnn	k1gInSc1	FAnn
parfumerie	parfumerie	k1gFnSc1	parfumerie
Grand	grand	k1gMnSc1	grand
Optical	Optical	k1gMnSc1	Optical
Hervis	Hervis	k1gFnSc4	Hervis
Humanic	Humanice	k1gFnPc2	Humanice
Kappa	kappa	k1gNnSc2	kappa
Koberce	koberec	k1gInSc2	koberec
Breno	Breno	k6eAd1	Breno
Marks	Marks	k1gInSc1	Marks
&	&	k?	&
Spencer	Spencer	k1gMnSc1	Spencer
New	New	k1gMnSc1	New
Yorker	Yorker	k1gMnSc1	Yorker
Pet	Pet	k1gMnSc1	Pet
Center	centrum	k1gNnPc2	centrum
PLANEO	PLANEO	kA	PLANEO
Elektro	Elektro	k1gNnSc1	Elektro
Red	Red	k1gMnSc1	Red
and	and	k?	and
Cube	Cube	k1gInSc1	Cube
Takko	Takko	k1gNnSc1	Takko
Fashion	Fashion	k1gInSc1	Fashion
Tesco	Tesco	k6eAd1	Tesco
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
..	..	k?	..
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Campus	Campus	k1gInSc1	Campus
Square	square	k1gInSc4	square
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Campus	Campus	k1gInSc1	Campus
Square	square	k1gInSc1	square
</s>
