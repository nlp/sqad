<s>
Magnetostatika	Magnetostatika	k1gFnSc1	Magnetostatika
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
magnetické	magnetický	k2eAgInPc4d1	magnetický
jevy	jev	k1gInPc4	jev
související	související	k2eAgInPc4d1	související
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
(	(	kIx(	(
<g/>
časově	časově	k6eAd1	časově
<g/>
)	)	kIx)	)
neměnného	měnný	k2eNgNnSc2d1	neměnné
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
