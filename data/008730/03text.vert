<p>
<s>
Magnetostatika	Magnetostatika	k1gFnSc1	Magnetostatika
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
magnetické	magnetický	k2eAgInPc4d1	magnetický
jevy	jev	k1gInPc4	jev
související	související	k2eAgInPc4d1	související
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
(	(	kIx(	(
<g/>
časově	časově	k6eAd1	časově
<g/>
)	)	kIx)	)
neměnného	měnný	k2eNgNnSc2d1	neměnné
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnetostatika	Magnetostatika	k1gFnSc1	Magnetostatika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
především	především	k9	především
studiem	studio	k1gNnSc7	studio
magnetostatického	magnetostatický	k2eAgNnSc2d1	magnetostatický
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Magnetismus	magnetismus	k1gInSc1	magnetismus
</s>
</p>
<p>
<s>
Elektromagnetismus	elektromagnetismus	k1gInSc1	elektromagnetismus
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
proud	proud	k1gInSc1	proud
</s>
</p>
<p>
<s>
Magnetostatické	Magnetostatický	k2eAgNnSc1d1	Magnetostatický
pole	pole	k1gNnSc1	pole
</s>
</p>
