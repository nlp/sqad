<s>
V	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
potom	potom	k6eAd1	potom
celkově	celkově	k6eAd1	celkově
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kolapsu	kolaps	k1gInSc3	kolaps
hollywoodského	hollywoodský	k2eAgInSc2d1	hollywoodský
studiového	studiový	k2eAgInSc2d1	studiový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
zavedením	zavedení	k1gNnSc7	zavedení
auteurismu	auteurismus	k1gInSc2	auteurismus
mezi	mezi	k7c7	mezi
hollywoodskými	hollywoodský	k2eAgMnPc7d1	hollywoodský
režiséry	režisér	k1gMnPc7	režisér
a	a	k8xC	a
kritiky	kritik	k1gMnPc7	kritik
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
se	s	k7c7	s
zvýšením	zvýšení	k1gNnSc7	zvýšení
vlivu	vliv	k1gInSc2	vliv
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
filmů	film	k1gInPc2	film
a	a	k8xC	a
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
filmařů	filmař	k1gMnPc2	filmař
<g/>
.	.	kIx.	.
</s>
