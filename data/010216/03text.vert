<p>
<s>
Atlantropa	Atlantrop	k1gMnSc2	Atlantrop
nebo	nebo	k8xC	nebo
Panropa	Panrop	k1gMnSc2	Panrop
byl	být	k5eAaImAgInS	být
gigantický	gigantický	k2eAgInSc1d1	gigantický
inženýrský	inženýrský	k2eAgInSc1d1	inženýrský
projekt	projekt	k1gInSc1	projekt
snížení	snížení	k1gNnSc2	snížení
hladiny	hladina	k1gFnSc2	hladina
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
zavlažování	zavlažování	k1gNnSc2	zavlažování
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ho	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
německý	německý	k2eAgMnSc1d1	německý
architekt	architekt	k1gMnSc1	architekt
Herman	Herman	k1gMnSc1	Herman
Sörgel	Sörgel	k1gMnSc1	Sörgel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gNnSc4	on
následně	následně	k6eAd1	následně
propagoval	propagovat	k5eAaImAgMnS	propagovat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
projektu	projekt	k1gInSc2	projekt
byla	být	k5eAaImAgFnS	být
přehrada	přehrada	k1gFnSc1	přehrada
přes	přes	k7c4	přes
Gibraltarskou	gibraltarský	k2eAgFnSc4d1	Gibraltarská
úžinu	úžina	k1gFnSc4	úžina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
snížila	snížit	k5eAaPmAgFnS	snížit
hladinu	hladina	k1gFnSc4	hladina
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
o	o	k7c4	o
100	[number]	k4	100
m.	m.	k?	m.
Další	další	k2eAgFnSc1d1	další
přehrada	přehrada	k1gFnSc1	přehrada
by	by	kYmCp3nS	by
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
mezi	mezi	k7c7	mezi
Sicílií	Sicílie	k1gFnSc7	Sicílie
a	a	k8xC	a
Tuniskem	Tunisko	k1gNnSc7	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
snížila	snížit	k5eAaPmAgFnS	snížit
hladinu	hladina	k1gFnSc4	hladina
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
100	[number]	k4	100
m.	m.	k?	m.
Černé	Černé	k2eAgNnSc2d1	Černé
moře	moře	k1gNnSc2	moře
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
odděleno	oddělen	k2eAgNnSc1d1	odděleno
přehradou	přehrada	k1gFnSc7	přehrada
přes	přes	k7c4	přes
úžinu	úžina	k1gFnSc4	úžina
Dardanely	Dardanely	k1gFnPc1	Dardanely
<g/>
.	.	kIx.	.
</s>
<s>
Suezský	suezský	k2eAgInSc1d1	suezský
kanál	kanál	k1gInSc1	kanál
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
doplněn	doplněn	k2eAgMnSc1d1	doplněn
o	o	k7c4	o
zdymadla	zdymadlo	k1gNnPc4	zdymadlo
<g/>
.	.	kIx.	.
</s>
<s>
Sahara	Sahara	k1gFnSc1	Sahara
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
zavlažována	zavlažovat	k5eAaImNgFnS	zavlažovat
vodou	voda	k1gFnSc7	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
kanálem	kanál	k1gInSc7	kanál
přiváděna	přiváděn	k2eAgFnSc1d1	přiváděna
do	do	k7c2	do
Čadského	čadský	k2eAgNnSc2d1	Čadské
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
posunutí	posunutí	k1gNnSc1	posunutí
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
čáry	čára	k1gFnSc2	čára
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c4	na
evropské	evropský	k2eAgNnSc4d1	Evropské
<g/>
,	,	kIx,	,
tak	tak	k9	tak
africké	africký	k2eAgFnSc3d1	africká
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
posun	posun	k1gInSc1	posun
by	by	kYmCp3nS	by
nastal	nastat	k5eAaPmAgInS	nastat
na	na	k7c6	na
tuniském	tuniský	k2eAgNnSc6d1	tuniské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
moře	moře	k1gNnSc1	moře
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
o	o	k7c4	o
stovky	stovka	k1gFnPc4	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poklesu	pokles	k1gInSc2	pokles
hladiny	hladina	k1gFnSc2	hladina
by	by	kYmCp3nP	by
také	také	k9	také
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
Jaderské	jaderský	k2eAgNnSc4d1	Jaderské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Benátky	Benátky	k1gFnPc4	Benátky
tak	tak	k8xS	tak
byl	být	k5eAaImAgInS	být
naplánován	naplánovat	k5eAaBmNgInS	naplánovat
úzký	úzký	k2eAgInSc1d1	úzký
průplav	průplav	k1gInSc1	průplav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
je	on	k3xPp3gMnPc4	on
stále	stále	k6eAd1	stále
pojil	pojit	k5eAaImAgMnS	pojit
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
na	na	k7c4	na
řecké	řecký	k2eAgInPc4d1	řecký
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
turecké	turecký	k2eAgFnSc3d1	turecká
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
by	by	kYmCp3nS	by
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
Sicílie	Sicílie	k1gFnSc1	Sicílie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Messinské	messinský	k2eAgFnSc2d1	Messinská
úžiny	úžina	k1gFnSc2	úžina
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgInPc1d1	velký
územní	územní	k2eAgInPc1d1	územní
zisky	zisk	k1gInPc1	zisk
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
obecně	obecně	k6eAd1	obecně
všech	všecek	k3xTgInPc2	všecek
ostrovů	ostrov	k1gInPc2	ostrov
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sörgel	Sörgel	k1gMnSc1	Sörgel
plánoval	plánovat	k5eAaImAgMnS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
výstavba	výstavba	k1gFnSc1	výstavba
zabrala	zabrat	k5eAaPmAgFnS	zabrat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
projekt	projekt	k1gInSc4	projekt
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
mírovou	mírový	k2eAgFnSc4d1	mírová
alternativu	alternativa	k1gFnSc4	alternativa
ke	k	k7c3	k
konceptu	koncept	k1gInSc3	koncept
Lebensraumu	Lebensraum	k1gInSc2	Lebensraum
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
také	také	k9	také
ohromné	ohromný	k2eAgNnSc1d1	ohromné
množství	množství	k1gNnSc1	množství
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
by	by	kYmCp3nP	by
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
tři	tři	k4xCgFnPc4	tři
velké	velká	k1gFnPc4	velká
mořské	mořský	k2eAgFnSc2d1	mořská
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
politicky	politicky	k6eAd1	politicky
značně	značně	k6eAd1	značně
neprůchodný	průchodný	k2eNgInSc1d1	neprůchodný
projekt	projekt	k1gInSc1	projekt
prakticky	prakticky	k6eAd1	prakticky
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
po	po	k7c6	po
Sörgelově	Sörgelův	k2eAgFnSc6d1	Sörgelův
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Atlantropa	Atlantrop	k1gMnSc2	Atlantrop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Plán	plán	k1gInSc1	plán
jménem	jméno	k1gNnSc7	jméno
Atlantropa	Atlantrop	k1gMnSc2	Atlantrop
<g/>
:	:	kIx,	:
Evropa	Evropa	k1gFnSc1	Evropa
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
!	!	kIx.	!
</s>
<s>
Epocha	epocha	k1gFnSc1	epocha
Plus	plus	k1gInSc1	plus
</s>
</p>
<p>
<s>
Atlantropa	Atlantropa	k1gFnSc1	Atlantropa
-	-	kIx~	-
sen	sen	k1gInSc1	sen
o	o	k7c6	o
novém	nový	k2eAgInSc6d1	nový
světadílu	světadíl	k1gInSc6	světadíl
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
</s>
</p>
