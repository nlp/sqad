<s>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
kvašený	kvašený	k2eAgInSc4d1	kvašený
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
hořké	hořký	k2eAgFnSc2d1	hořká
chuti	chuť	k1gFnSc2	chuť
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
v	v	k7c6	v
pivovaru	pivovar	k1gInSc6	pivovar
z	z	k7c2	z
obilného	obilný	k2eAgInSc2d1	obilný
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
nezbytně	zbytně	k6eNd1	zbytně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
<g/>
)	)	kIx)	)
chmele	chmel	k1gInSc2	chmel
pomocí	pomocí	k7c2	pomocí
pivovarských	pivovarský	k2eAgFnPc2d1	Pivovarská
kvasinek	kvasinka	k1gFnPc2	kvasinka
(	(	kIx(	(
<g/>
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
cerevisiae	cerevisia	k1gFnSc2	cerevisia
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
eventuálně	eventuálně	k6eAd1	eventuálně
divokých	divoký	k2eAgFnPc2d1	divoká
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
značné	značný	k2eAgFnSc3d1	značná
oblibě	obliba	k1gFnSc3	obliba
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejkonzumovanější	konzumovaný	k2eAgInSc4d3	konzumovaný
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
české	český	k2eAgNnSc1d1	české
pivo	pivo	k1gNnSc1	pivo
chráněno	chráněn	k2eAgNnSc1d1	chráněno
jako	jako	k8xS	jako
zeměpisné	zeměpisný	k2eAgNnSc1d1	zeměpisné
označení	označení	k1gNnSc1	označení
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
vařeno	vařen	k2eAgNnSc1d1	vařeno
již	již	k6eAd1	již
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
určit	určit	k5eAaPmF	určit
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
uvařeno	uvařen	k2eAgNnSc4d1	uvařeno
první	první	k4xOgNnSc4	první
pivo	pivo	k1gNnSc4	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
země	zem	k1gFnPc1	zem
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
již	již	k6eAd1	již
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ale	ale	k8xC	ale
možná	možná	k9	možná
Sumerové	Sumer	k1gMnPc1	Sumer
připravovali	připravovat	k5eAaImAgMnP	připravovat
pouze	pouze	k6eAd1	pouze
kvas	kvas	k1gInSc4	kvas
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
staroslověnské	staroslověnský	k2eAgNnSc1d1	staroslověnské
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
označovalo	označovat	k5eAaImAgNnS	označovat
"	"	kIx"	"
<g/>
nápoj	nápoj	k1gInSc1	nápoj
nejobyčejnější	obyčejný	k2eAgInPc1d3	nejobyčejnější
a	a	k8xC	a
nejrozšířenější	rozšířený	k2eAgInPc1d3	nejrozšířenější
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
pivo	pivo	k1gNnSc1	pivo
konzumováno	konzumovat	k5eAaBmNgNnS	konzumovat
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
drží	držet	k5eAaImIp3nP	držet
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Česka	Česko	k1gNnSc2	Česko
přední	přední	k2eAgFnSc6d1	přední
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
spotřebě	spotřeba	k1gFnSc6	spotřeba
piva	pivo	k1gNnSc2	pivo
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
160	[number]	k4	160
litrů	litr	k1gInPc2	litr
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
35	[number]	k4	35
až	až	k9	až
44	[number]	k4	44
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
spotřebou	spotřeba	k1gFnSc7	spotřeba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
týdenní	týdenní	k2eAgFnSc1d1	týdenní
spotřeba	spotřeba	k1gFnSc1	spotřeba
alkoholu	alkohol	k1gInSc2	alkohol
mužů	muž	k1gMnPc2	muž
zhruba	zhruba	k6eAd1	zhruba
9	[number]	k4	9
litrů	litr	k1gInPc2	litr
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
2	[number]	k4	2
litry	litr	k1gInPc4	litr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
ekvivalentu	ekvivalent	k1gInSc2	ekvivalent
450	[number]	k4	450
litrů	litr	k1gInPc2	litr
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
°	°	k?	°
piv	pivo	k1gNnPc2	pivo
<g/>
)	)	kIx)	)
respektive	respektive	k9	respektive
100	[number]	k4	100
litrů	litr	k1gInPc2	litr
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
WHO	WHO	kA	WHO
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
průměrně	průměrně	k6eAd1	průměrně
přes	přes	k7c4	přes
16	[number]	k4	16
litrů	litr	k1gInPc2	litr
čistého	čistý	k2eAgInSc2d1	čistý
alkoholu	alkohol	k1gInSc2	alkohol
celkem	celkem	k6eAd1	celkem
na	na	k7c4	na
dospělého	dospělý	k1gMnSc4	dospělý
(	(	kIx(	(
<g/>
na	na	k7c4	na
neabstinujícího	abstinující	k2eNgMnSc4d1	abstinující
dospělého	dospělý	k2eAgMnSc4d1	dospělý
muže	muž	k1gMnSc4	muž
dokonce	dokonce	k9	dokonce
přes	přes	k7c4	přes
26	[number]	k4	26
litrů	litr	k1gInPc2	litr
<g/>
)	)	kIx)	)
a	a	k8xC	a
přes	přes	k7c4	přes
8	[number]	k4	8
litrů	litr	k1gInPc2	litr
jen	jen	k9	jen
na	na	k7c4	na
pivo	pivo	k1gNnSc4	pivo
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgInSc7d1	tradiční
a	a	k8xC	a
populárním	populární	k2eAgInSc7d1	populární
nápojem	nápoj	k1gInSc7	nápoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
alkoholické	alkoholický	k2eAgInPc4d1	alkoholický
nápoje	nápoj	k1gInPc4	nápoj
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
g	g	kA	g
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
alkoholu	alkohol	k1gInSc2	alkohol
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pivo	pivo	k1gNnSc4	pivo
také	také	k9	také
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
000	[number]	k4	000
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
významné	významný	k2eAgNnSc1d1	významné
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
výrazně	výrazně	k6eAd1	výrazně
zavodňující	zavodňující	k2eAgInSc4d1	zavodňující
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
sacharidy	sacharid	k1gInPc4	sacharid
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rychlých	rychlý	k2eAgFnPc2d1	rychlá
kalorií	kalorie	k1gFnPc2	kalorie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
hořké	hořký	k2eAgFnPc4d1	hořká
látky	látka	k1gFnPc4	látka
chmele	chmel	k1gInSc2	chmel
<g/>
,	,	kIx,	,
polyfenolické	polyfenolický	k2eAgFnPc1d1	polyfenolická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc4	vitamín
a	a	k8xC	a
minerální	minerální	k2eAgFnPc4d1	minerální
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnPc1	kombinace
těchto	tento	k3xDgFnPc2	tento
složek	složka	k1gFnPc2	složka
dává	dávat	k5eAaImIp3nS	dávat
fyziologicky	fyziologicky	k6eAd1	fyziologicky
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
roztok	roztok	k1gInSc1	roztok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
s	s	k7c7	s
osmotickým	osmotický	k2eAgInSc7d1	osmotický
tlakem	tlak	k1gInSc7	tlak
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
zastoupení	zastoupení	k1gNnSc1	zastoupení
minerálů	minerál	k1gInPc2	minerál
v	v	k7c6	v
pivu	pivo	k1gNnSc6	pivo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nacházíme	nacházet	k5eAaImIp1nP	nacházet
kromě	kromě	k7c2	kromě
draslíku	draslík	k1gInSc2	draslík
a	a	k8xC	a
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
v	v	k7c6	v
příznivém	příznivý	k2eAgInSc6d1	příznivý
poměru	poměr	k1gInSc6	poměr
<g/>
,	,	kIx,	,
také	také	k9	také
chloridy	chlorid	k1gInPc1	chlorid
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
a	a	k8xC	a
křemík	křemík	k1gInSc1	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vitamínů	vitamín	k1gInPc2	vitamín
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
pivu	pivo	k1gNnSc6	pivo
jsou	být	k5eAaImIp3nP	být
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
vitaminy	vitamin	k1gInPc1	vitamin
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
-	-	kIx~	-
thiamin	thiamin	k1gInSc1	thiamin
(	(	kIx(	(
<g/>
3	[number]	k4	3
%	%	kIx~	%
denní	denní	k2eAgFnSc2d1	denní
spotřeby	spotřeba	k1gFnSc2	spotřeba
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
piva	pivo	k1gNnSc2	pivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
riboflavin	riboflavin	k1gInSc1	riboflavin
(	(	kIx(	(
<g/>
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pyridoxin	pyridoxin	k1gMnSc1	pyridoxin
(	(	kIx(	(
<g/>
31	[number]	k4	31
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
niacin	niacin	k1gMnSc1	niacin
(	(	kIx(	(
<g/>
45	[number]	k4	45
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
listová	listový	k2eAgFnSc1d1	listová
(	(	kIx(	(
<g/>
52	[number]	k4	52
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
litr	litr	k1gInSc1	litr
piva	pivo	k1gNnSc2	pivo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
-	-	kIx~	-
500	[number]	k4	500
kilokalorií	kilokalorie	k1gFnPc2	kilokalorie
představující	představující	k2eAgFnSc4d1	představující
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
%	%	kIx~	%
denní	denní	k2eAgFnSc2d1	denní
spotřeby	spotřeba	k1gFnSc2	spotřeba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
např.	např.	kA	např.
jablečná	jablečný	k2eAgFnSc1d1	jablečná
šťáva	šťáva	k1gFnSc1	šťáva
<g/>
,	,	kIx,	,
a	a	k8xC	a
200	[number]	k4	200
mg	mg	kA	mg
biologicky	biologicky	k6eAd1	biologicky
aktivních	aktivní	k2eAgFnPc2d1	aktivní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
používat	používat	k5eAaImF	používat
jako	jako	k8xC	jako
nápoj	nápoj	k1gInSc4	nápoj
vhodný	vhodný	k2eAgInSc4d1	vhodný
k	k	k7c3	k
utišení	utišení	k1gNnSc3	utišení
žízně	žízeň	k1gFnSc2	žízeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
nutriční	nutriční	k2eAgFnSc4d1	nutriční
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
vyváženost	vyváženost	k1gFnSc4	vyváženost
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
vitaminů	vitamin	k1gInPc2	vitamin
a	a	k8xC	a
polyfenolů	polyfenol	k1gInPc2	polyfenol
(	(	kIx(	(
<g/>
flavonoidů	flavonoid	k1gInPc2	flavonoid
<g/>
)	)	kIx)	)
s	s	k7c7	s
antioxidačním	antioxidační	k2eAgInSc7d1	antioxidační
účinkem	účinek	k1gInSc7	účinek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovšem	ovšem	k9	ovšem
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Příznivé	příznivý	k2eAgInPc1d1	příznivý
účinky	účinek	k1gInPc1	účinek
piva	pivo	k1gNnSc2	pivo
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
projevit	projevit	k5eAaPmF	projevit
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
střídmé	střídmý	k2eAgFnSc6d1	střídmá
konzumaci	konzumace	k1gFnSc6	konzumace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nepřevažují	převažovat	k5eNaImIp3nP	převažovat
negativní	negativní	k2eAgInPc4d1	negativní
účinky	účinek	k1gInPc4	účinek
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
i	i	k9	i
střídmé	střídmý	k2eAgNnSc4d1	střídmé
pití	pití	k1gNnSc4	pití
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
škodí	škodit	k5eAaImIp3nP	škodit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
studie	studie	k1gFnPc1	studie
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
ovlivněny	ovlivněn	k2eAgInPc1d1	ovlivněn
vlivy	vliv	k1gInPc1	vliv
systematických	systematický	k2eAgFnPc2d1	systematická
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
abstinentů	abstinent	k1gMnPc2	abstinent
například	například	k6eAd1	například
spadají	spadat	k5eAaPmIp3nP	spadat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nepijí	pít	k5eNaImIp3nP	pít
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hořké	hořký	k2eAgFnPc4d1	hořká
chmelové	chmelový	k2eAgFnPc4d1	chmelová
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
sekreci	sekrece	k1gFnSc4	sekrece
žluči	žluč	k1gFnSc2	žluč
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přímo	přímo	k6eAd1	přímo
podporuje	podporovat	k5eAaImIp3nS	podporovat
trávení	trávení	k1gNnSc4	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
výrazně	výrazně	k6eAd1	výrazně
podporuje	podporovat	k5eAaImIp3nS	podporovat
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
při	při	k7c6	při
nestřídmé	střídmý	k2eNgFnSc6d1	nestřídmá
konzumaci	konzumace	k1gFnSc6	konzumace
pokrmů	pokrm	k1gInPc2	pokrm
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
vyšší	vysoký	k2eAgInSc1d2	vyšší
obsah	obsah	k1gInSc1	obsah
flavonoidů	flavonoid	k1gInPc2	flavonoid
pocházejících	pocházející	k2eAgInPc2d1	pocházející
z	z	k7c2	z
obilnin	obilnina	k1gFnPc2	obilnina
a	a	k8xC	a
chmelu	chmel	k1gInSc2	chmel
pivo	pivo	k1gNnSc1	pivo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
vyšší	vysoký	k2eAgInPc4d2	vyšší
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
antioxidační	antioxidační	k2eAgInSc1d1	antioxidační
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc4	obsah
flavonoidů	flavonoid	k1gInPc2	flavonoid
zvyšujeme	zvyšovat	k5eAaImIp1nP	zvyšovat
delší	dlouhý	k2eAgFnSc7d2	delší
a	a	k8xC	a
intenzivnější	intenzivní	k2eAgFnSc7d2	intenzivnější
extrakcí	extrakce	k1gFnSc7	extrakce
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
zvýšením	zvýšení	k1gNnSc7	zvýšení
množství	množství	k1gNnSc2	množství
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
přidáním	přidání	k1gNnSc7	přidání
sladu	slad	k1gInSc2	slad
nebo	nebo	k8xC	nebo
výtažku	výtažek	k1gInSc2	výtažek
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
obilnin	obilnina	k1gFnPc2	obilnina
a	a	k8xC	a
obilovin	obilovina	k1gFnPc2	obilovina
<g/>
,	,	kIx,	,
u	u	k7c2	u
bylinných	bylinný	k2eAgNnPc2d1	bylinné
a	a	k8xC	a
ovocných	ovocný	k2eAgNnPc2d1	ovocné
piv	pivo	k1gNnPc2	pivo
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
ovoce	ovoce	k1gNnPc1	ovoce
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
antioxidační	antioxidační	k2eAgInSc4d1	antioxidační
účinky	účinek	k1gInPc4	účinek
mají	mít	k5eAaImIp3nP	mít
flavonoidy	flavonoid	k1gInPc4	flavonoid
ostružin	ostružina	k1gFnPc2	ostružina
a	a	k8xC	a
malin	malina	k1gFnPc2	malina
<g/>
.	.	kIx.	.
</s>
<s>
Barvení	barvený	k2eAgMnPc1d1	barvený
kulérem	kulér	k1gInSc7	kulér
(	(	kIx(	(
<g/>
karamelem	karamel	k1gInSc7	karamel
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pražením	pražení	k1gNnSc7	pražení
sladu	slad	k1gInSc2	slad
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
pro	pro	k7c4	pro
pivo	pivo	k1gNnSc4	pivo
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
antioxidační	antioxidační	k2eAgFnSc4d1	antioxidační
hodnotu	hodnota	k1gFnSc4	hodnota
piva	pivo	k1gNnSc2	pivo
nezvyšuje	zvyšovat	k5eNaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ovocné	ovocný	k2eAgFnPc1d1	ovocná
šťávy	šťáva	k1gFnPc1	šťáva
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
samotné	samotný	k2eAgNnSc4d1	samotné
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
)	)	kIx)	)
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
flavonidů	flavonid	k1gMnPc2	flavonid
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
alkohol	alkohol	k1gInSc4	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
konzumace	konzumace	k1gFnSc1	konzumace
piva	pivo	k1gNnSc2	pivo
má	mít	k5eAaImIp3nS	mít
příznivé	příznivý	k2eAgInPc4d1	příznivý
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
dobrou	dobrý	k2eAgFnSc4d1	dobrá
náladu	nálada	k1gFnSc4	nálada
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
rizika	riziko	k1gNnSc2	riziko
srdečních	srdeční	k2eAgFnPc2d1	srdeční
příhod	příhoda	k1gFnPc2	příhoda
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
vysokému	vysoký	k2eAgInSc3d1	vysoký
krevnímu	krevní	k2eAgInSc3d1	krevní
tlaku	tlak	k1gInSc3	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Konzumace	konzumace	k1gFnSc1	konzumace
piva	pivo	k1gNnSc2	pivo
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
alkoholismu	alkoholismus	k1gInSc2	alkoholismus
s	s	k7c7	s
poškozením	poškození	k1gNnSc7	poškození
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
nejen	nejen	k6eAd1	nejen
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiné	jiný	k2eAgFnSc2d1	jiná
látky	látka	k1gFnSc2	látka
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
konzumací	konzumace	k1gFnSc7	konzumace
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
především	především	k6eAd1	především
tato	tento	k3xDgNnPc4	tento
onemocnění	onemocnění	k1gNnPc4	onemocnění
a	a	k8xC	a
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
<g/>
:	:	kIx,	:
cirhóza	cirhóza	k1gFnSc1	cirhóza
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
kolorektální	kolorektální	k2eAgInSc1d1	kolorektální
karcinom	karcinom	k1gInSc1	karcinom
<g/>
,	,	kIx,	,
karcinom	karcinom	k1gInSc1	karcinom
prsu	prs	k1gInSc2	prs
<g/>
,	,	kIx,	,
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
IARC	IARC	kA	IARC
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc4	každý
alkoholický	alkoholický	k2eAgInSc4d1	alkoholický
nápoj	nápoj	k1gInSc4	nápoj
považován	považován	k2eAgInSc4d1	považován
za	za	k7c4	za
prokázaný	prokázaný	k2eAgInSc4d1	prokázaný
karcinogen	karcinogen	k1gInSc4	karcinogen
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
od	od	k7c2	od
0,5	[number]	k4	0,5
do	do	k7c2	do
cca	cca	kA	cca
20	[number]	k4	20
procent	procento	k1gNnPc2	procento
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
pivech	pivo	k1gNnPc6	pivo
je	být	k5eAaImIp3nS	být
alkoholu	alkohol	k1gInSc2	alkohol
nejčastěji	často	k6eAd3	často
mezi	mezi	k7c4	mezi
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
odbourávat	odbourávat	k5eAaImF	odbourávat
alkohol	alkohol	k1gInSc4	alkohol
dle	dle	k7c2	dle
individuálních	individuální	k2eAgFnPc2d1	individuální
měřítek	měřítko	k1gNnPc2	měřítko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
,	,	kIx,	,
pohlaví	pohlaví	k1gNnSc6	pohlaví
<g/>
,	,	kIx,	,
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
stupni	stupeň	k1gInSc6	stupeň
únavy	únava	k1gFnSc2	únava
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
faktorech	faktor	k1gInPc6	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
odbourá	odbourat	k5eAaPmIp3nS	odbourat
alkohol	alkohol	k1gInSc4	alkohol
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
po	po	k7c6	po
konzumaci	konzumace	k1gFnSc6	konzumace
piva	pivo	k1gNnSc2	pivo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
jen	jen	k9	jen
nepřesně	přesně	k6eNd1	přesně
odhadovat	odhadovat	k5eAaImF	odhadovat
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
zásada	zásada	k1gFnSc1	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
obéznější	obézní	k2eAgMnSc1d2	obéznější
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
pomaleji	pomale	k6eAd2	pomale
se	se	k3xPyFc4	se
alkohol	alkohol	k1gInSc1	alkohol
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
odbourává	odbourávat	k5eAaImIp3nS	odbourávat
<g/>
.	.	kIx.	.
</s>
<s>
Orientačně	orientačně	k6eAd1	orientačně
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
uvádí	uvádět	k5eAaImIp3nS	uvádět
čas	čas	k1gInSc4	čas
okolo	okolo	k7c2	okolo
2	[number]	k4	2
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
pro	pro	k7c4	pro
80	[number]	k4	80
kg	kg	kA	kg
vážícího	vážící	k2eAgMnSc4d1	vážící
muže	muž	k1gMnSc4	muž
u	u	k7c2	u
10	[number]	k4	10
<g/>
°	°	k?	°
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
3	[number]	k4	3
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
u	u	k7c2	u
ženy	žena	k1gFnSc2	žena
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
60	[number]	k4	60
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
12	[number]	k4	12
<g/>
°	°	k?	°
pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
uvádí	uvádět	k5eAaImIp3nS	uvádět
při	při	k7c6	při
stejných	stejný	k2eAgInPc6d1	stejný
fyziologických	fyziologický	k2eAgInPc6d1	fyziologický
parametrech	parametr	k1gInPc6	parametr
doba	doba	k1gFnSc1	doba
2	[number]	k4	2
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
respektive	respektive	k9	respektive
4	[number]	k4	4
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
faktorů	faktor	k1gInPc2	faktor
schopnosti	schopnost	k1gFnSc2	schopnost
odbourávat	odbourávat	k5eAaImF	odbourávat
alkohol	alkohol	k1gInSc4	alkohol
je	být	k5eAaImIp3nS	být
enzym	enzym	k1gInSc4	enzym
ADH	ADH	kA	ADH
-	-	kIx~	-
alkoholdehydrogenáza	alkoholdehydrogenáza	k1gFnSc1	alkoholdehydrogenáza
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
enzym	enzym	k1gInSc4	enzym
však	však	k9	však
některým	některý	k3yIgInPc3	některý
národům	národ	k1gInPc3	národ
úplně	úplně	k6eAd1	úplně
chybí	chybit	k5eAaPmIp3nS	chybit
(	(	kIx(	(
<g/>
eskymáci	eskymák	k1gMnPc1	eskymák
<g/>
,	,	kIx,	,
indiáni	indián	k1gMnPc1	indián
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
asijské	asijský	k2eAgInPc1d1	asijský
národy	národ	k1gInPc1	národ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
právním	právní	k2eAgInSc6d1	právní
řádu	řád	k1gInSc6	řád
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
pivem	pivo	k1gNnSc7	pivo
zabývá	zabývat	k5eAaImIp3nS	zabývat
zejména	zejména	k9	zejména
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
335	[number]	k4	335
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provádí	provádět	k5eAaImIp3nS	provádět
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
potravinách	potravina	k1gFnPc6	potravina
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
110	[number]	k4	110
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyhláška	vyhláška	k1gFnSc1	vyhláška
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jak	jak	k8xC	jak
definici	definice	k1gFnSc4	definice
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
tak	tak	k9	tak
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
jakost	jakost	k1gFnSc4	jakost
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc4	rozdělení
druhů	druh	k1gInPc2	druh
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
označování	označování	k1gNnSc4	označování
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
piva	pivo	k1gNnSc2	pivo
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
pro	pro	k7c4	pro
samotnou	samotný	k2eAgFnSc4d1	samotná
výrobu	výroba	k1gFnSc4	výroba
nápoje	nápoj	k1gInSc2	nápoj
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
-	-	kIx~	-
na	na	k7c4	na
1	[number]	k4	1
litr	litr	k1gInSc4	litr
vystaveného	vystavený	k2eAgNnSc2d1	vystavené
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
spotřebováno	spotřebován	k2eAgNnSc1d1	spotřebováno
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
až	až	k9	až
osmkrát	osmkrát	k6eAd1	osmkrát
více	hodně	k6eAd2	hodně
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
použití	použití	k1gNnSc1	použití
přímo	přímo	k6eAd1	přímo
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
kvalitu	kvalita	k1gFnSc4	kvalita
výsledného	výsledný	k2eAgInSc2d1	výsledný
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Pivovary	pivovar	k1gInPc1	pivovar
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
budovány	budovat	k5eAaImNgFnP	budovat
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
výrazné	výrazný	k2eAgInPc4d1	výrazný
zdroje	zdroj	k1gInPc4	zdroj
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Chmel	chmel	k1gInSc1	chmel
otáčivý	otáčivý	k2eAgInSc1d1	otáčivý
<g/>
.	.	kIx.	.
</s>
<s>
Chmel	chmel	k1gInSc1	chmel
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
surovin	surovina	k1gFnPc2	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
dodává	dodávat	k5eAaImIp3nS	dodávat
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
nahořklou	nahořklý	k2eAgFnSc4d1	nahořklá
chuť	chuť	k1gFnSc4	chuť
pomocí	pomocí	k7c2	pomocí
chmelových	chmelový	k2eAgFnPc2d1	chmelová
pryskyřic	pryskyřice	k1gFnPc2	pryskyřice
a	a	k8xC	a
chmelové	chmelový	k2eAgNnSc1d1	chmelové
aroma	aroma	k1gNnSc1	aroma
vlivem	vlivem	k7c2	vlivem
silic	silice	k1gFnPc2	silice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
piva	pivo	k1gNnSc2	pivo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
samičí	samičí	k2eAgFnPc4d1	samičí
chmelové	chmelový	k2eAgFnPc4d1	chmelová
hlávky	hlávka	k1gFnPc4	hlávka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
chmelové	chmelový	k2eAgInPc4d1	chmelový
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
granule	granule	k1gFnPc4	granule
připravené	připravený	k2eAgFnPc4d1	připravená
z	z	k7c2	z
hlávek	hlávka	k1gFnPc2	hlávka
po	po	k7c6	po
usušení	usušení	k1gNnSc6	usušení
<g/>
,	,	kIx,	,
rozemletí	rozemletí	k1gNnSc6	rozemletí
a	a	k8xC	a
následném	následný	k2eAgInSc6d1	následný
peletizaci	peletizace	k1gFnSc6	peletizace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
českých	český	k2eAgNnPc2d1	české
piv	pivo	k1gNnPc2	pivo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
česká	český	k2eAgFnSc1d1	Česká
odrůda	odrůda	k1gFnSc1	odrůda
chmele	chmel	k1gInSc2	chmel
tzv.	tzv.	kA	tzv.
žatecký	žatecký	k2eAgInSc1d1	žatecký
poloraný	poloraný	k2eAgInSc1d1	poloraný
červeňák	červeňák	k1gInSc1	červeňák
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
řazen	řadit	k5eAaImNgInS	řadit
mezi	mezi	k7c4	mezi
nejkvalitnější	kvalitní	k2eAgInSc4d3	nejkvalitnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
jemných	jemný	k2eAgInPc2d1	jemný
aromatických	aromatický	k2eAgInPc2d1	aromatický
chmelů	chmel	k1gInPc2	chmel
<g/>
.	.	kIx.	.
</s>
<s>
Chmel	chmel	k1gInSc1	chmel
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
pěstován	pěstovat	k5eAaImNgInS	pěstovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
hlavních	hlavní	k2eAgFnPc6d1	hlavní
oblastech	oblast	k1gFnPc6	oblast
-	-	kIx~	-
Žatecké	žatecký	k2eAgNnSc4d1	žatecké
<g/>
,	,	kIx,	,
Úštěcké	úštěcký	k2eAgNnSc4d1	Úštěcké
a	a	k8xC	a
Tršické	Tršický	k2eAgNnSc4d1	Tršický
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Chmelové	Chmelové	k2eAgFnPc1d1	Chmelové
hlávky	hlávka	k1gFnPc1	hlávka
jsou	být	k5eAaImIp3nP	být
chemicky	chemicky	k6eAd1	chemicky
složité	složitý	k2eAgInPc1d1	složitý
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
např.	např.	kA	např.
alfa	alfa	k1gNnSc4	alfa
kyseliny	kyselina	k1gFnSc2	kyselina
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xC	jako
humolony	humolon	k1gInPc1	humolon
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Slad	slad	k1gInSc1	slad
<g/>
.	.	kIx.	.
</s>
<s>
Slad	slad	k1gInSc1	slad
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ze	z	k7c2	z
speciálně	speciálně	k6eAd1	speciálně
vyšlechtěných	vyšlechtěný	k2eAgInPc2d1	vyšlechtěný
druhů	druh	k1gInPc2	druh
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ječmene	ječmen	k1gInSc2	ječmen
či	či	k8xC	či
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
pšenice	pšenice	k1gFnPc1	pšenice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
výsledné	výsledný	k2eAgFnSc6d1	výsledná
chuti	chuť	k1gFnSc6	chuť
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc3	jeho
barvě	barva	k1gFnSc3	barva
a	a	k8xC	a
aromatu	aroma	k1gNnSc3	aroma
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
světlý	světlý	k2eAgInSc1d1	světlý
a	a	k8xC	a
tmavý	tmavý	k2eAgInSc1d1	tmavý
slad	slad	k1gInSc1	slad
<g/>
(	(	kIx(	(
<g/>
slad	slad	k1gInSc1	slad
plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
a	a	k8xC	a
bavorský	bavorský	k2eAgMnSc1d1	bavorský
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
slady	slad	k1gInPc1	slad
speciální	speciální	k2eAgInPc1d1	speciální
(	(	kIx(	(
<g/>
karamelový	karamelový	k2eAgInSc1d1	karamelový
<g/>
,	,	kIx,	,
pražený	pražený	k2eAgInSc1d1	pražený
<g/>
,	,	kIx,	,
diastatický	diastatický	k2eAgMnSc1d1	diastatický
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
ječné	ječný	k2eAgNnSc1d1	ječné
zrno	zrno	k1gNnSc1	zrno
tzv.	tzv.	kA	tzv.
obilka	obilka	k1gFnSc1	obilka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
zárodku	zárodek	k1gInSc2	zárodek
klíčku	klíček	k1gInSc2	klíček
a	a	k8xC	a
endospermu	endosperm	k1gInSc2	endosperm
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sladu	slad	k1gInSc2	slad
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
sleduje	sledovat	k5eAaImIp3nS	sledovat
hlavně	hlavně	k9	hlavně
klíčivost	klíčivost	k1gFnSc1	klíčivost
a	a	k8xC	a
klíčivá	klíčivý	k2eAgFnSc1d1	klíčivá
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
sladu	slad	k1gInSc2	slad
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
sladovnách	sladovna	k1gFnPc6	sladovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
sklizené	sklizený	k2eAgNnSc4d1	sklizené
obilí	obilí	k1gNnSc4	obilí
v	v	k7c6	v
případě	případ	k1gInSc6	případ
světlého	světlý	k2eAgInSc2d1	světlý
sladu	slad	k1gInSc2	slad
nejprve	nejprve	k6eAd1	nejprve
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c4	po
3	[number]	k4	3
dny	dna	k1gFnSc2	dna
máčeno	máčet	k5eAaImNgNnS	máčet
v	v	k7c6	v
měkké	měkký	k2eAgFnSc6d1	měkká
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zrna	zrno	k1gNnPc1	zrno
vstřebají	vstřebat	k5eAaPmIp3nP	vstřebat
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
váhy	váha	k1gFnSc2	váha
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
rozloženo	rozložen	k2eAgNnSc1d1	rozloženo
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
silné	silný	k2eAgFnSc6d1	silná
mezi	mezi	k7c4	mezi
10	[number]	k4	10
a	a	k8xC	a
15	[number]	k4	15
cm	cm	kA	cm
a	a	k8xC	a
nechává	nechávat	k5eAaImIp3nS	nechávat
se	se	k3xPyFc4	se
naklíčit	naklíčit	k5eAaPmF	naklíčit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
klíčení	klíčení	k1gNnSc2	klíčení
se	se	k3xPyFc4	se
zrno	zrno	k1gNnSc1	zrno
několikrát	několikrát	k6eAd1	několikrát
obrací	obracet	k5eAaImIp3nS	obracet
<g/>
.	.	kIx.	.
</s>
<s>
Klíčení	klíčení	k1gNnSc1	klíčení
zrna	zrno	k1gNnSc2	zrno
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
klíček	klíček	k1gInSc1	klíček
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
3⁄	3⁄	k?	3⁄
délky	délka	k1gFnSc2	délka
zrna	zrno	k1gNnSc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
zelený	zelený	k2eAgInSc1d1	zelený
slad	slad	k1gInSc1	slad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podstupuje	podstupovat	k5eAaImIp3nS	podstupovat
několik	několik	k4yIc4	několik
fází	fáze	k1gFnPc2	fáze
sušení	sušení	k1gNnSc2	sušení
nejprve	nejprve	k6eAd1	nejprve
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
35	[number]	k4	35
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
50	[number]	k4	50
a	a	k8xC	a
60	[number]	k4	60
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
fáze	fáze	k1gFnSc1	fáze
sušení	sušení	k1gNnSc2	sušení
za	za	k7c2	za
stálého	stálý	k2eAgNnSc2d1	stálé
větrání	větrání	k1gNnSc2	větrání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
teplota	teplota	k1gFnSc1	teplota
mezi	mezi	k7c7	mezi
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
°	°	k?	°
<g/>
C.	C.	kA	C.
Kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
sladovnický	sladovnický	k2eAgInSc1d1	sladovnický
ječmen	ječmen	k1gInSc1	ječmen
po	po	k7c6	po
vysušení	vysušení	k1gNnSc6	vysušení
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
62	[number]	k4	62
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
%	%	kIx~	%
škrobu	škrob	k1gInSc2	škrob
v	v	k7c6	v
sušině	sušina	k1gFnSc6	sušina
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc7d3	nejnižší
teplotou	teplota	k1gFnSc7	teplota
sušený	sušený	k2eAgInSc1d1	sušený
světlý	světlý	k2eAgInSc1d1	světlý
slad	slad	k1gInSc1	slad
je	být	k5eAaImIp3nS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
pro	pro	k7c4	pro
piva	pivo	k1gNnPc4	pivo
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
vyšší	vysoký	k2eAgFnSc2d2	vyšší
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
slad	slad	k1gInSc1	slad
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
Malt	Malta	k1gFnPc2	Malta
či	či	k8xC	či
alternativního	alternativní	k2eAgNnSc2d1	alternativní
pojmenování	pojmenování	k1gNnSc2	pojmenování
"	"	kIx"	"
<g/>
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
<g/>
"	"	kIx"	"
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
použitá	použitý	k2eAgFnSc1d1	použitá
teplota	teplota	k1gFnSc1	teplota
ještě	ještě	k6eAd1	ještě
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
slad	slad	k1gInSc4	slad
pro	pro	k7c4	pro
bavorský	bavorský	k2eAgInSc4d1	bavorský
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nárůstu	nárůst	k1gInSc6	nárůst
teploty	teplota	k1gFnSc2	teplota
následuje	následovat	k5eAaImIp3nS	následovat
tmavý	tmavý	k2eAgInSc4d1	tmavý
slad	slad	k1gInSc4	slad
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
do	do	k7c2	do
hnědé	hnědý	k2eAgFnSc2d1	hnědá
až	až	k8xS	až
karamelové	karamelový	k2eAgFnSc2d1	karamelová
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
užívaný	užívaný	k2eAgInSc1d1	užívaný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
tmavých	tmavý	k2eAgNnPc2d1	tmavé
piv	pivo	k1gNnPc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
sladu	slad	k1gInSc2	slad
se	se	k3xPyFc4	se
také	také	k9	také
praží	pražit	k5eAaImIp3nS	pražit
ve	v	k7c6	v
speciálních	speciální	k2eAgInPc6d1	speciální
pražících	pražící	k2eAgInPc6d1	pražící
bubnech	buben	k1gInPc6	buben
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
využívána	využívat	k5eAaImNgFnS	využívat
k	k	k7c3	k
dochucení	dochucení	k1gNnSc3	dochucení
tmavých	tmavý	k2eAgNnPc2d1	tmavé
piv	pivo	k1gNnPc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
procesem	proces	k1gInSc7	proces
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
škrobovitý	škrobovitý	k2eAgInSc1d1	škrobovitý
roztok	roztok	k1gInSc1	roztok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
uvolnění	uvolnění	k1gNnSc4	uvolnění
fermentovaných	fermentovaný	k2eAgInPc2d1	fermentovaný
cukrů	cukr	k1gInPc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pivovarské	pivovarský	k2eAgFnSc2d1	Pivovarská
kvasinky	kvasinka	k1gFnSc2	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Kvasinky	kvasinka	k1gFnPc4	kvasinka
jsou	být	k5eAaImIp3nP	být
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
biotechnologické	biotechnologický	k2eAgFnSc6d1	biotechnologická
výrobě	výroba	k1gFnSc6	výroba
piva	pivo	k1gNnSc2	pivo
-	-	kIx~	-
pivovarnictví	pivovarnictví	k1gNnSc2	pivovarnictví
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
průběhu	průběh	k1gInSc2	průběh
<g/>
)	)	kIx)	)
fermentace	fermentace	k1gFnPc1	fermentace
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
svrchní	svrchní	k2eAgNnSc4d1	svrchní
kvašení	kvašení	k1gNnSc4	kvašení
(	(	kIx(	(
<g/>
cca	cca	kA	cca
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
průběhem	průběh	k1gInSc7	průběh
fermentace	fermentace	k1gFnSc2	fermentace
kvasinky	kvasinka	k1gFnSc2	kvasinka
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
<g/>
)	)	kIx)	)
používány	používán	k2eAgFnPc1d1	používána
kvasinky	kvasinka	k1gFnPc1	kvasinka
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
ceravisiae	ceravisia	k1gFnSc2	ceravisia
ssp	ssp	k?	ssp
<g/>
.	.	kIx.	.
carlsbergensis	carlsbergensis	k1gFnSc2	carlsbergensis
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
jejich	jejich	k3xOp3gMnSc2	jejich
objevitele	objevitel	k1gMnSc2	objevitel
Emila	Emil	k1gMnSc2	Emil
Christiana	Christian	k1gMnSc2	Christian
Hansena	Hansen	k2eAgFnSc1d1	Hansena
z	z	k7c2	z
dánského	dánský	k2eAgInSc2d1	dánský
Carlsbergu	Carlsberg	k1gInSc2	Carlsberg
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
spodní	spodní	k2eAgNnSc4d1	spodní
kvašení	kvašení	k1gNnSc4	kvašení
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
během	během	k7c2	během
fermentace	fermentace	k1gFnSc2	fermentace
kvasinky	kvasinka	k1gFnSc2	kvasinka
sedimentují	sedimentovat	k5eAaImIp3nP	sedimentovat
<g/>
)	)	kIx)	)
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
ceravisiae	ceravisia	k1gInSc2	ceravisia
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
piva	pivo	k1gNnPc1	pivo
jsou	být	k5eAaImIp3nP	být
kvašena	kvasit	k5eAaImNgNnP	kvasit
i	i	k9	i
spontánním	spontánní	k2eAgNnSc7d1	spontánní
kvašením	kvašení	k1gNnSc7	kvašení
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
divokých	divoký	k2eAgFnPc2d1	divoká
kvasinek	kvasinka	k1gFnPc2	kvasinka
a	a	k8xC	a
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
přítomných	přítomný	k2eAgInPc2d1	přítomný
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
výroby	výroba	k1gFnSc2	výroba
bez	bez	k7c2	bez
dodatečného	dodatečný	k2eAgNnSc2d1	dodatečné
dodání	dodání	k1gNnSc2	dodání
čisté	čistý	k2eAgFnSc2d1	čistá
kvasničné	kvasničný	k2eAgFnSc2d1	kvasničná
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c6	v
pivu	pivo	k1gNnSc6	pivo
bublinky	bublinka	k1gFnSc2	bublinka
a	a	k8xC	a
pěnu	pěna	k1gFnSc4	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Čepované	čepovaný	k2eAgNnSc1d1	čepované
pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
krémovou	krémový	k2eAgFnSc7d1	krémová
pěnou	pěna	k1gFnSc7	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
pěna	pěna	k1gFnSc1	pěna
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
udržet	udržet	k5eAaPmF	udržet
lehčí	lehký	k2eAgFnSc4d2	lehčí
minci	mince	k1gFnSc4	mince
<g/>
.	.	kIx.	.
</s>
<s>
Pěna	pěna	k1gFnSc1	pěna
tlumí	tlumit	k5eAaImIp3nS	tlumit
povrchové	povrchový	k2eAgNnSc4d1	povrchové
vlnění	vlnění	k1gNnSc4	vlnění
hladiny	hladina	k1gFnSc2	hladina
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
přípravy	příprava	k1gFnSc2	příprava
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
stejný	stejný	k2eAgInSc4d1	stejný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
zdokonalovaly	zdokonalovat	k5eAaImAgFnP	zdokonalovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
technologické	technologický	k2eAgInPc4d1	technologický
kroky	krok	k1gInPc4	krok
a	a	k8xC	a
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Primitivní	primitivní	k2eAgFnSc4d1	primitivní
přípravu	příprava	k1gFnSc4	příprava
piva	pivo	k1gNnSc2	pivo
postupně	postupně	k6eAd1	postupně
nahradila	nahradit	k5eAaPmAgFnS	nahradit
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
kolébku	kolébka	k1gFnSc4	kolébka
piva	pivo	k1gNnSc2	pivo
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
tzv.	tzv.	kA	tzv.
úrodného	úrodný	k2eAgInSc2d1	úrodný
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Eufratem	Eufrat	k1gInSc7	Eufrat
a	a	k8xC	a
Tigridem	Tigris	k1gInSc7	Tigris
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
před	před	k7c7	před
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
obilí	obilí	k1gNnSc4	obilí
Sumerové	Sumer	k1gMnPc1	Sumer
<g/>
,	,	kIx,	,
Akkadové	Akkadový	k2eAgFnPc1d1	Akkadový
<g/>
,	,	kIx,	,
Babyloňané	Babyloňan	k1gMnPc1	Babyloňan
a	a	k8xC	a
Asyřané	Asyřan	k1gMnPc1	Asyřan
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pivo	pivo	k1gNnSc1	pivo
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
náhodně	náhodně	k6eAd1	náhodně
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tehdejšímu	tehdejší	k2eAgNnSc3d1	tehdejší
skladování	skladování	k1gNnSc3	skladování
obilí	obilí	k1gNnSc2	obilí
v	v	k7c6	v
hliněných	hliněný	k2eAgFnPc6d1	hliněná
nádobách	nádoba	k1gFnPc6	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
nádob	nádoba	k1gFnPc2	nádoba
nejspíše	nejspíše	k9	nejspíše
natekla	natéct	k5eAaPmAgFnS	natéct
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kvašení	kvašení	k1gNnSc3	kvašení
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
voda	voda	k1gFnSc1	voda
s	s	k7c7	s
příjemnou	příjemný	k2eAgFnSc7d1	příjemná
omamnou	omamný	k2eAgFnSc7d1	omamná
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
z	z	k7c2	z
obilí	obilí	k1gNnSc2	obilí
připravovat	připravovat	k5eAaImF	připravovat
kvašené	kvašený	k2eAgInPc4d1	kvašený
nápoje	nápoj	k1gInPc4	nápoj
cíleně	cíleně	k6eAd1	cíleně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
druh	druh	k1gMnSc1	druh
piva	pivo	k1gNnSc2	pivo
Sumery	Sumer	k1gInPc4	Sumer
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
kaš	kaša	k1gFnPc2	kaša
<g/>
,	,	kIx,	,
Babyloňany	Babyloňan	k1gMnPc4	Babyloňan
šikarum	šikarum	k1gInSc4	šikarum
<g/>
.	.	kIx.	.
</s>
<s>
Vaření	vaření	k1gNnSc1	vaření
piva	pivo	k1gNnSc2	pivo
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
rozvoje	rozvoj	k1gInSc2	rozvoj
technické	technický	k2eAgFnSc2d1	technická
metody	metoda	k1gFnSc2	metoda
jeho	jeho	k3xOp3gFnSc2	jeho
přípravy	příprava	k1gFnSc2	příprava
většinou	většinou	k6eAd1	většinou
doménou	doména	k1gFnSc7	doména
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Sumerské	sumerský	k2eAgNnSc1d1	sumerské
pivo	pivo	k1gNnSc1	pivo
kaš	kaša	k1gFnPc2	kaša
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
současného	současný	k2eAgNnSc2d1	současné
piva	pivo	k1gNnSc2	pivo
připravováno	připravovat	k5eAaImNgNnS	připravovat
bez	bez	k7c2	bez
chmele	chmel	k1gInSc2	chmel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Kaš	kaša	k1gFnPc2	kaša
vznikal	vznikat	k5eAaImAgInS	vznikat
z	z	k7c2	z
ječného	ječný	k2eAgInSc2d1	ječný
chleba	chléb	k1gInSc2	chléb
a	a	k8xC	a
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
společně	společně	k6eAd1	společně
umístěny	umístit	k5eAaPmNgInP	umístit
do	do	k7c2	do
velikého	veliký	k2eAgInSc2d1	veliký
džbánu	džbán	k1gInSc2	džbán
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
kvašení	kvašení	k1gNnSc3	kvašení
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
chmelu	chmel	k1gInSc2	chmel
kaš	kaša	k1gFnPc2	kaša
nezískával	získávat	k5eNaImAgInS	získávat
hořkou	hořký	k2eAgFnSc4d1	hořká
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
dodání	dodání	k1gNnSc4	dodání
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
využívat	využívat	k5eAaImF	využívat
jiného	jiný	k2eAgInSc2d1	jiný
postupu	postup	k1gInSc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
využívalo	využívat	k5eAaPmAgNnS	využívat
pražení	pražení	k1gNnSc1	pražení
chleba	chléb	k1gInSc2	chléb
v	v	k7c6	v
horkém	horký	k2eAgInSc6d1	horký
popelu	popel	k1gInSc6	popel
<g/>
,	,	kIx,	,
či	či	k8xC	či
přidáváním	přidávání	k1gNnSc7	přidávání
zelené	zelený	k2eAgFnSc2d1	zelená
hořčice	hořčice	k1gFnSc2	hořčice
<g/>
,	,	kIx,	,
či	či	k8xC	či
sezamových	sezamový	k2eAgNnPc2d1	sezamové
semínek	semínko	k1gNnPc2	semínko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
kvašení	kvašení	k1gNnSc2	kvašení
sladu	slad	k1gInSc2	slad
začalo	začít	k5eAaPmAgNnS	začít
vznikat	vznikat	k5eAaImF	vznikat
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgFnP	lišit
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíce	tisíc	k4xCgInPc1	tisíc
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
pití	pití	k1gNnSc3	pití
piva	pivo	k1gNnSc2	pivo
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
zvykem	zvyk	k1gInSc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
pivo	pivo	k1gNnSc1	pivo
nepodstupovalo	podstupovat	k5eNaImAgNnS	podstupovat
proces	proces	k1gInSc4	proces
filtrace	filtrace	k1gFnSc2	filtrace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
přítomnost	přítomnost	k1gFnSc1	přítomnost
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
mechanických	mechanický	k2eAgFnPc2d1	mechanická
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pivo	pivo	k1gNnSc1	pivo
nebylo	být	k5eNaImAgNnS	být
čiré	čirý	k2eAgNnSc1d1	čiré
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
pití	pití	k1gNnSc4	pití
využívalo	využívat	k5eAaPmAgNnS	využívat
obilné	obilný	k2eAgNnSc4d1	obilné
stéblo	stéblo	k1gNnSc4	stéblo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
fungovalo	fungovat	k5eAaImAgNnS	fungovat
jako	jako	k9	jako
brčko	brčko	k1gNnSc1	brčko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tisíc	tisíc	k4xCgInSc4	tisíc
let	let	k1gInSc4	let
později	pozdě	k6eAd2	pozdě
v	v	k7c4	v
Chammurapiho	Chammurapi	k1gMnSc4	Chammurapi
zákoníku	zákoník	k1gInSc2	zákoník
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pocházejí	pocházet	k5eAaImIp3nP	pocházet
i	i	k9	i
první	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
veřejných	veřejný	k2eAgFnPc6d1	veřejná
provozovnách	provozovna	k1gFnPc6	provozovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
pivo	pivo	k1gNnSc1	pivo
zakoupit	zakoupit	k5eAaPmF	zakoupit
<g/>
.	.	kIx.	.
</s>
<s>
Zákoník	zákoník	k1gInSc1	zákoník
upravoval	upravovat	k5eAaImAgInS	upravovat
tresty	trest	k1gInPc4	trest
pro	pro	k7c4	pro
nepoctivé	poctivý	k2eNgFnPc4d1	nepoctivá
šenkýřky	šenkýřka	k1gFnPc4	šenkýřka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
trestem	trest	k1gInSc7	trest
vhození	vhození	k1gNnSc2	vhození
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
smrt	smrt	k1gFnSc1	smrt
utopením	utopení	k1gNnSc7	utopení
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
přípravě	příprava	k1gFnSc6	příprava
piva	pivo	k1gNnSc2	pivo
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
původní	původní	k2eAgFnSc4d1	původní
zemi	zem	k1gFnSc4	zem
objevu	objev	k1gInSc2	objev
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
používali	používat	k5eAaImAgMnP	používat
pro	pro	k7c4	pro
kvasící	kvasící	k2eAgFnSc4d1	kvasící
nádobu	nádoba	k1gFnSc4	nádoba
používanou	používaný	k2eAgFnSc4d1	používaná
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
slovo	slovo	k1gNnSc1	slovo
namset	namseta	k1gFnPc2	namseta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nejspíše	nejspíše	k9	nejspíše
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
akkadského	akkadský	k2eAgNnSc2d1	akkadské
slova	slovo	k1gNnSc2	slovo
namzítu	namzít	k1gInSc2	namzít
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
používali	používat	k5eAaImAgMnP	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
slad	slad	k1gInSc4	slad
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
pšenice	pšenice	k1gFnSc2	pšenice
namísto	namísto	k7c2	namísto
chmele	chmel	k1gInSc2	chmel
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
chmelu	chmel	k1gInSc2	chmel
pak	pak	k6eAd1	pak
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
pivo	pivo	k1gNnSc1	pivo
mělo	mít	k5eAaImAgNnS	mít
nasládlou	nasládlý	k2eAgFnSc4d1	nasládlá
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
podobnou	podobný	k2eAgFnSc4d1	podobná
spíše	spíše	k9	spíše
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
kvasu	kvas	k1gInSc3	kvas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Antiky	antika	k1gFnSc2	antika
se	se	k3xPyFc4	se
pivo	pivo	k1gNnSc1	pivo
nacházelo	nacházet	k5eAaImAgNnS	nacházet
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zájmu	zájem	k1gInSc2	zájem
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
konzumaci	konzumace	k1gFnSc4	konzumace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Středomoří	středomoří	k1gNnSc1	středomoří
jasně	jasně	k6eAd1	jasně
dominovalo	dominovat	k5eAaImAgNnS	dominovat
víno	víno	k1gNnSc1	víno
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
pivo	pivo	k1gNnSc4	pivo
nepili	pít	k5eNaImAgMnP	pít
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
Římany	Říman	k1gMnPc4	Říman
bylo	být	k5eAaImAgNnS	být
oblíbeno	oblíbit	k5eAaPmNgNnS	oblíbit
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
zvlášť	zvlášť	k6eAd1	zvlášť
v	v	k7c6	v
pohraničních	pohraniční	k2eAgFnPc6d1	pohraniční
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
panoval	panovat	k5eAaImAgInS	panovat
nedostatek	nedostatek	k1gInSc1	nedostatek
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Evropy	Evropa	k1gFnSc2	Evropa
obývané	obývaný	k2eAgMnPc4d1	obývaný
Kelty	Kelt	k1gMnPc4	Kelt
se	se	k3xPyFc4	se
piva	pivo	k1gNnSc2	pivo
pilo	pít	k5eAaImAgNnS	pít
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejváženějším	vážený	k2eAgInSc7d3	nejváženější
nápojem	nápoj	k1gInSc7	nápoj
Keltů	Kelt	k1gMnPc2	Kelt
byla	být	k5eAaImAgFnS	být
medovina	medovina	k1gFnSc1	medovina
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgMnSc3	ten
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
i	i	k9	i
nadále	nadále	k6eAd1	nadále
preferovaly	preferovat	k5eAaImAgFnP	preferovat
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
konzumaci	konzumace	k1gFnSc4	konzumace
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
konzumace	konzumace	k1gFnSc1	konzumace
piva	pivo	k1gNnSc2	pivo
hojně	hojně	k6eAd1	hojně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
patří	patřit	k5eAaImIp3nS	patřit
oblasti	oblast	k1gFnSc2	oblast
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
Vikingů	Viking	k1gMnPc2	Viking
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ale	ale	k9	ale
konzumovalo	konzumovat	k5eAaBmAgNnS	konzumovat
teplé	teplý	k2eAgNnSc1d1	teplé
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
např.	např.	kA	např.
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
i	i	k8xC	i
do	do	k7c2	do
mnohem	mnohem	k6eAd1	mnohem
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevení	objevení	k1gNnSc3	objevení
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
výrobu	výroba	k1gFnSc4	výroba
silnějšího	silný	k2eAgNnSc2d2	silnější
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
metodu	metoda	k1gFnSc4	metoda
tzv.	tzv.	kA	tzv.
vymrazování	vymrazování	k1gNnSc1	vymrazování
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgNnSc2	který
pivo	pivo	k1gNnSc1	pivo
zmrzlo	zmrznout	k5eAaPmAgNnS	zmrznout
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozdílné	rozdílný	k2eAgFnSc3d1	rozdílná
teplotě	teplota	k1gFnSc3	teplota
tání	tání	k1gNnSc2	tání
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
alkoholu	alkohol	k1gInSc2	alkohol
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
obsahu	obsah	k1gInSc2	obsah
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
pivě	pivo	k1gNnSc6	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
blížilo	blížit	k5eAaImAgNnS	blížit
k	k	k7c3	k
dnešním	dnešní	k2eAgInPc3d1	dnešní
ležákům	ležák	k1gInPc3	ležák
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
Keltové	Kelt	k1gMnPc1	Kelt
a	a	k8xC	a
Germáni	Germán	k1gMnPc1	Germán
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
letopočtu	letopočet	k1gInSc2	letopočet
začali	začít	k5eAaPmAgMnP	začít
do	do	k7c2	do
piva	pivo	k1gNnSc2	pivo
přidávat	přidávat	k5eAaImF	přidávat
chmel	chmel	k1gInSc4	chmel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
byl	být	k5eAaImAgInS	být
vypracován	vypracovat	k5eAaPmNgInS	vypracovat
bavorský	bavorský	k2eAgInSc1d1	bavorský
Reinheitsgebot	Reinheitsgebot	k1gInSc1	Reinheitsgebot
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
záruka	záruka	k1gFnSc1	záruka
čistoty	čistota	k1gFnSc2	čistota
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajistil	zajistit	k5eAaPmAgMnS	zajistit
produkci	produkce	k1gFnSc4	produkce
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
čistotě	čistota	k1gFnSc6	čistota
piva	pivo	k1gNnSc2	pivo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Reinheitsgebot	Reinheitsgebot	k1gInSc1	Reinheitsgebot
<g/>
,	,	kIx,	,
Deutsche	Deutsche	k1gNnSc1	Deutsche
Reinheitsgebot	Reinheitsgebota	k1gFnPc2	Reinheitsgebota
nebo	nebo	k8xC	nebo
Bayerische	Bayerische	k1gFnPc2	Bayerische
Reinheitsgebot	Reinheitsgebota	k1gFnPc2	Reinheitsgebota
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pivo	pivo	k1gNnSc1	pivo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
chmel	chmel	k1gInSc4	chmel
<g/>
,	,	kIx,	,
slad	slad	k1gInSc4	slad
<g/>
,	,	kIx,	,
kvasinky	kvasinka	k1gFnPc4	kvasinka
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
<g/>
,,	,,	k?	,,
Značné	značný	k2eAgFnSc2d1	značná
obliby	obliba	k1gFnSc2	obliba
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
pivo	pivo	k1gNnSc1	pivo
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hlavnímu	hlavní	k2eAgMnSc3d1	hlavní
konkurentovi	konkurent	k1gMnSc3	konkurent
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vína	víno	k1gNnSc2	víno
hrozilo	hrozit	k5eAaImAgNnS	hrozit
zničení	zničení	k1gNnSc4	zničení
hmyzím	hmyzí	k2eAgMnSc7d1	hmyzí
škůdcem	škůdce	k1gMnSc7	škůdce
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Phylloxera	Phylloxero	k1gNnSc2	Phylloxero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
vydává	vydávat	k5eAaPmIp3nS	vydávat
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
dílo	dílo	k1gNnSc4	dílo
Studie	studie	k1gFnSc1	studie
o	o	k7c6	o
pivu	pivo	k1gNnSc6	pivo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
podrobně	podrobně	k6eAd1	podrobně
popisuje	popisovat	k5eAaImIp3nS	popisovat
novodobou	novodobý	k2eAgFnSc4d1	novodobá
technologii	technologie	k1gFnSc4	technologie
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c4	na
filtraci	filtrace	k1gFnSc4	filtrace
a	a	k8xC	a
pasterizaci	pasterizace	k1gFnSc4	pasterizace
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
narůstaly	narůstat	k5eAaImAgInP	narůstat
poznatky	poznatek	k1gInPc1	poznatek
o	o	k7c6	o
přípravě	příprava	k1gFnSc6	příprava
<g/>
,	,	kIx,	,
kvašení	kvašení	k1gNnSc4	kvašení
a	a	k8xC	a
filtraci	filtrace	k1gFnSc4	filtrace
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
první	první	k4xOgInSc1	první
pivovar	pivovar	k1gInSc1	pivovar
určený	určený	k2eAgInSc1d1	určený
k	k	k7c3	k
masové	masový	k2eAgFnSc3d1	masová
produkci	produkce	k1gFnSc3	produkce
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
americký	americký	k2eAgInSc1d1	americký
pivovar	pivovar	k1gInSc1	pivovar
Budweiser	Budweisra	k1gFnPc2	Budweisra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
trendů	trend	k1gInPc2	trend
vyrábět	vyrábět	k5eAaImF	vyrábět
většinu	většina	k1gFnSc4	většina
piva	pivo	k1gNnSc2	pivo
v	v	k7c6	v
masových	masový	k2eAgInPc6d1	masový
pivovarech	pivovar	k1gInPc6	pivovar
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
zakládat	zakládat	k5eAaImF	zakládat
malé	malý	k2eAgInPc4d1	malý
pivovary	pivovar	k1gInPc4	pivovar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
objevily	objevit	k5eAaPmAgFnP	objevit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
území	území	k1gNnSc6	území
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Unifikovaná	unifikovaný	k2eAgNnPc1d1	unifikované
piva	pivo	k1gNnPc1	pivo
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
moderními	moderní	k2eAgFnPc7d1	moderní
metodami	metoda	k1gFnPc7	metoda
vaření	vaření	k1gNnSc2	vaření
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
CK	CK	kA	CK
tanky	tank	k1gInPc4	tank
(	(	kIx(	(
<g/>
cylindrokonické	cylindrokonický	k2eAgInPc1d1	cylindrokonický
tanky	tank	k1gInPc1	tank
<g/>
,	,	kIx,	,
CKT	CKT	kA	CKT
<g/>
)	)	kIx)	)
a	a	k8xC	a
dodatečným	dodatečný	k2eAgNnSc7d1	dodatečné
ředěním	ředění	k1gNnSc7	ředění
třinácti	třináct	k4xCc3	třináct
až	až	k9	až
patnáctistupňového	patnáctistupňový	k2eAgNnSc2d1	patnáctistupňové
piva	pivo	k1gNnSc2	pivo
vodou	voda	k1gFnSc7	voda
obohacenou	obohacený	k2eAgFnSc7d1	obohacená
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
na	na	k7c4	na
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
stupňovitost	stupňovitost	k1gFnSc4	stupňovitost
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
HGB	HGB	kA	HGB
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
lehce	lehko	k6eAd1	lehko
pejorativní	pejorativní	k2eAgNnSc4d1	pejorativní
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
europivo	europivo	k1gNnSc4	europivo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Archeologický	archeologický	k2eAgInSc1d1	archeologický
výzkum	výzkum	k1gInSc1	výzkum
území	území	k1gNnSc2	území
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
přinesl	přinést	k5eAaPmAgInS	přinést
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
dřívější	dřívější	k2eAgMnPc1d1	dřívější
obyvatelé	obyvatel	k1gMnPc1	obyvatel
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
připravovali	připravovat	k5eAaImAgMnP	připravovat
kvašené	kvašený	k2eAgInPc4d1	kvašený
nápoje	nápoj	k1gInPc4	nápoj
z	z	k7c2	z
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jsou	být	k5eAaImIp3nP	být
podrobnější	podrobný	k2eAgInPc1d2	podrobnější
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
přípravě	příprava	k1gFnSc6	příprava
piva	pivo	k1gNnSc2	pivo
keltskými	keltský	k2eAgInPc7d1	keltský
Boji	boj	k1gInPc7	boj
<g/>
,	,	kIx,	,
germánskými	germánský	k2eAgInPc7d1	germánský
kmeny	kmen	k1gInPc7	kmen
Markomanů	Markoman	k1gMnPc2	Markoman
<g/>
,	,	kIx,	,
Kvádů	Kvád	k1gMnPc2	Kvád
a	a	k8xC	a
Slovany	Slovan	k1gInPc1	Slovan
bájného	bájný	k2eAgMnSc2d1	bájný
praotce	praotec	k1gMnSc2	praotec
Čecha	Čech	k1gMnSc2	Čech
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c6	na
území	území	k1gNnSc6	území
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadili	usadit	k5eAaPmAgMnP	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
doložený	doložený	k2eAgInSc1d1	doložený
český	český	k2eAgInSc1d1	český
pivovar	pivovar	k1gInSc1	pivovar
je	být	k5eAaImIp3nS	být
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášterní	klášterní	k2eAgInSc1d1	klášterní
pivovar	pivovar	k1gInSc1	pivovar
v	v	k7c6	v
Břevnovském	břevnovský	k2eAgInSc6d1	břevnovský
klášteru	klášter	k1gInSc3	klášter
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
993	[number]	k4	993
<g/>
.	.	kIx.	.
</s>
<s>
Prvým	prvý	k4xOgInSc7	prvý
dokladem	doklad	k1gInSc7	doklad
souvisejícím	související	k2eAgInSc7d1	související
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
nadační	nadační	k2eAgFnSc1d1	nadační
listina	listina	k1gFnSc1	listina
prvního	první	k4xOgMnSc2	první
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Vratislava	Vratislav	k1gMnSc2	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
vyšehradskou	vyšehradský	k2eAgFnSc4d1	Vyšehradská
kapitulu	kapitula	k1gFnSc4	kapitula
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1088	[number]	k4	1088
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
mimo	mimo	k7c4	mimo
ostatní	ostatní	k2eAgInPc4d1	ostatní
dary	dar	k1gInPc4	dar
a	a	k8xC	a
privilegia	privilegium	k1gNnPc4	privilegium
panovník	panovník	k1gMnSc1	panovník
přidělil	přidělit	k5eAaPmAgMnS	přidělit
kapitule	kapitula	k1gFnSc3	kapitula
desátek	desátek	k1gInSc4	desátek
chmele	chmel	k1gInSc2	chmel
na	na	k7c4	na
vaření	vaření	k1gNnSc4	vaření
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
dokladem	doklad	k1gInSc7	doklad
o	o	k7c4	o
pěstování	pěstování	k1gNnSc4	pěstování
chmele	chmel	k1gInSc2	chmel
na	na	k7c6	na
území	území	k1gNnSc6	území
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
nadační	nadační	k2eAgFnSc1d1	nadační
listina	listina	k1gFnSc1	listina
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
knížete	kníže	k1gMnSc2	kníže
Břetislava	Břetislav	k1gMnSc2	Břetislav
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
rozkvět	rozkvět	k1gInSc1	rozkvět
výroby	výroba	k1gFnSc2	výroba
piva	pivo	k1gNnSc2	pivo
na	na	k7c6	na
území	území	k1gNnSc6	území
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
právo	právo	k1gNnSc4	právo
vařit	vařit	k5eAaImF	vařit
pivo	pivo	k1gNnSc4	pivo
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
starých	starý	k2eAgFnPc6d1	stará
dobách	doba	k1gFnPc6	doba
pivo	pivo	k1gNnSc1	pivo
vařily	vařit	k5eAaImAgFnP	vařit
velmi	velmi	k6eAd1	velmi
primitivním	primitivní	k2eAgInSc7d1	primitivní
postupem	postup	k1gInSc7	postup
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Nesloužilo	sloužit	k5eNaImAgNnS	sloužit
jenom	jenom	k9	jenom
jako	jako	k9	jako
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
připravovaly	připravovat	k5eAaImAgInP	připravovat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
různé	různý	k2eAgInPc1d1	různý
pokrmy	pokrm	k1gInPc1	pokrm
jako	jako	k8xS	jako
polévky	polévka	k1gFnSc2	polévka
<g/>
,	,	kIx,	,
kaše	kaše	k1gFnSc2	kaše
a	a	k8xC	a
omáčky	omáčka	k1gFnSc2	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
řemeslné	řemeslný	k2eAgFnSc2d1	řemeslná
výroby	výroba	k1gFnSc2	výroba
piva	pivo	k1gNnSc2	pivo
nastal	nastat	k5eAaPmAgInS	nastat
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
zakládáním	zakládání	k1gNnSc7	zakládání
nových	nový	k2eAgNnPc2d1	nové
královských	královský	k2eAgNnPc2d1	královské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dostávala	dostávat	k5eAaImAgFnS	dostávat
od	od	k7c2	od
panovníka	panovník	k1gMnSc2	panovník
řadu	řad	k1gInSc2	řad
privilegií	privilegium	k1gNnPc2	privilegium
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
pivovarství	pivovarství	k1gNnSc2	pivovarství
bylo	být	k5eAaImAgNnS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
přidělení	přidělení	k1gNnSc1	přidělení
práva	právo	k1gNnSc2	právo
várečného	várečný	k2eAgNnSc2d1	várečné
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
mílového	mílový	k2eAgNnSc2d1	mílové
(	(	kIx(	(
<g/>
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tato	tento	k3xDgNnPc1	tento
práva	právo	k1gNnPc1	právo
získala	získat	k5eAaPmAgNnP	získat
i	i	k9	i
poddanská	poddanský	k2eAgNnPc1d1	poddanské
města	město	k1gNnPc1	město
od	od	k7c2	od
příslušné	příslušný	k2eAgFnSc2d1	příslušná
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
pivovarství	pivovarství	k1gNnSc2	pivovarství
a	a	k8xC	a
kvalitu	kvalita	k1gFnSc4	kvalita
piva	pivo	k1gNnSc2	pivo
měly	mít	k5eAaImAgFnP	mít
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
sladovnické	sladovnický	k2eAgInPc1d1	sladovnický
cechy	cech	k1gInPc1	cech
<g/>
.	.	kIx.	.
</s>
<s>
Určovaly	určovat	k5eAaImAgInP	určovat
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
z	z	k7c2	z
jakého	jaký	k3yQgNnSc2	jaký
množství	množství	k1gNnSc2	množství
sladu	slad	k1gInSc2	slad
smí	smět	k5eAaImIp3nS	smět
jeden	jeden	k4xCgInSc4	jeden
dům	dům	k1gInSc4	dům
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
,	,	kIx,	,
kontrolovaly	kontrolovat	k5eAaImAgFnP	kontrolovat
jeho	jeho	k3xOp3gFnSc4	jeho
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
dohlížely	dohlížet	k5eAaImAgFnP	dohlížet
i	i	k9	i
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pivo	pivo	k1gNnSc4	pivo
vařil	vařit	k5eAaImAgMnS	vařit
jen	jen	k9	jen
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
oboru	obor	k1gInSc6	obor
řádně	řádně	k6eAd1	řádně
vyučil	vyučit	k5eAaPmAgMnS	vyučit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgNnP	být
česká	český	k2eAgNnPc1d1	české
piva	pivo	k1gNnPc1	pivo
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
a	a	k8xC	a
v	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
vyvážela	vyvážet	k5eAaImAgFnS	vyvážet
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
i	i	k9	i
na	na	k7c4	na
dvory	dvůr	k1gInPc4	dvůr
jiných	jiný	k2eAgMnPc2d1	jiný
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zbohatlí	zbohatlý	k2eAgMnPc1d1	zbohatlý
měšťané	měšťan	k1gMnPc1	měšťan
sdružovali	sdružovat	k5eAaImAgMnP	sdružovat
své	svůj	k3xOyFgInPc4	svůj
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
zakládali	zakládat	k5eAaImAgMnP	zakládat
společné	společný	k2eAgInPc4d1	společný
městské	městský	k2eAgInPc4d1	městský
pivovary	pivovar	k1gInPc4	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
výroba	výroba	k1gFnSc1	výroba
piva	pivo	k1gNnSc2	pivo
ve	v	k7c6	v
šlechtických	šlechtický	k2eAgInPc6d1	šlechtický
pivovarech	pivovar	k1gInPc6	pivovar
<g/>
,	,	kIx,	,
stabilně	stabilně	k6eAd1	stabilně
se	se	k3xPyFc4	se
udržovala	udržovat	k5eAaImAgFnS	udržovat
či	či	k8xC	či
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
v	v	k7c6	v
klášterních	klášterní	k2eAgInPc6d1	klášterní
pivovarech	pivovar	k1gInPc6	pivovar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
méně	málo	k6eAd2	málo
podléhaly	podléhat	k5eAaImAgInP	podléhat
vlivům	vliv	k1gInPc3	vliv
politických	politický	k2eAgFnPc2d1	politická
a	a	k8xC	a
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
řemeslné	řemeslný	k2eAgNnSc1d1	řemeslné
pivovarství	pivovarství	k1gNnSc1	pivovarství
začalo	začít	k5eAaPmAgNnS	začít
upadat	upadat	k5eAaImF	upadat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1547	[number]	k4	1547
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
měst	město	k1gNnPc2	město
vzbouřila	vzbouřit	k5eAaPmAgFnS	vzbouřit
proti	proti	k7c3	proti
nadvládě	nadvláda	k1gFnSc3	nadvláda
Habsburků	Habsburk	k1gMnPc2	Habsburk
a	a	k8xC	a
poté	poté	k6eAd1	poté
jim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgInS	být
konfiskován	konfiskovat	k5eAaBmNgInS	konfiskovat
majetek	majetek	k1gInSc1	majetek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
reformu	reforma	k1gFnSc4	reforma
výroby	výroba	k1gFnSc2	výroba
sladu	slad	k1gInSc2	slad
a	a	k8xC	a
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
typických	typický	k2eAgFnPc2d1	typická
vlastností	vlastnost	k1gFnPc2	vlastnost
současných	současný	k2eAgNnPc2d1	současné
českých	český	k2eAgNnPc2d1	české
piv	pivo	k1gNnPc2	pivo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
český	český	k2eAgMnSc1d1	český
sládek	sládek	k1gMnSc1	sládek
František	František	k1gMnSc1	František
Ondřej	Ondřej	k1gMnSc1	Ondřej
Poupě	Poupě	k1gMnSc1	Poupě
(	(	kIx(	(
<g/>
1753	[number]	k4	1753
<g/>
-	-	kIx~	-
<g/>
1805	[number]	k4	1805
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
řadu	řada	k1gFnSc4	řada
nových	nový	k2eAgNnPc2d1	nové
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
sladu	slad	k1gInSc2	slad
a	a	k8xC	a
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
sládky	sládek	k1gMnPc4	sládek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
používali	používat	k5eAaImAgMnP	používat
výhradně	výhradně	k6eAd1	výhradně
ječný	ječný	k2eAgInSc4d1	ječný
slad	slad	k1gInSc4	slad
<g/>
,	,	kIx,	,
upravil	upravit	k5eAaPmAgMnS	upravit
dávkování	dávkování	k1gNnSc4	dávkování
chmele	chmel	k1gInSc2	chmel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
života	život	k1gInSc2	život
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
pivovarskou	pivovarský	k2eAgFnSc4d1	Pivovarská
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgInSc4	první
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
ji	on	k3xPp3gFnSc4	on
řada	řada	k1gFnSc1	řada
nejen	nejen	k6eAd1	nejen
českých	český	k2eAgMnPc2d1	český
sládků	sládek	k1gMnPc2	sládek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pivovarníků	pivovarník	k1gMnPc2	pivovarník
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
pivovarství	pivovarství	k1gNnSc6	pivovarství
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc1	založení
Měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
pivovaru	pivovar	k1gInSc2	pivovar
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
dnešního	dnešní	k2eAgInSc2d1	dnešní
Prazdroje	prazdroj	k1gInSc2	prazdroj
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
výhradně	výhradně	k6eAd1	výhradně
spodně	spodně	k6eAd1	spodně
kvašená	kvašený	k2eAgNnPc4d1	kvašené
piva	pivo	k1gNnPc4	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
mělo	mít	k5eAaImAgNnS	mít
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc4d1	dobrá
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
všechny	všechen	k3xTgInPc1	všechen
pivovary	pivovar	k1gInPc1	pivovar
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
zavedly	zavést	k5eAaPmAgFnP	zavést
tuto	tento	k3xDgFnSc4	tento
technologii	technologie	k1gFnSc4	technologie
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nastal	nastat	k5eAaPmAgInS	nastat
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
českého	český	k2eAgNnSc2d1	české
pivovarství	pivovarství	k1gNnSc2	pivovarství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
silně	silně	k6eAd1	silně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
vývoj	vývoj	k1gInSc4	vývoj
tohoto	tento	k3xDgInSc2	tento
oboru	obor	k1gInSc2	obor
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
období	období	k1gNnSc4	období
zahájení	zahájení	k1gNnSc2	zahájení
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
sladu	slad	k1gInSc2	slad
a	a	k8xC	a
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Vynikající	vynikající	k2eAgInSc1d1	vynikající
rozvoj	rozvoj	k1gInSc1	rozvoj
českého	český	k2eAgNnSc2d1	české
pivovarství	pivovarství	k1gNnSc2	pivovarství
a	a	k8xC	a
kvalita	kvalita	k1gFnSc1	kvalita
jeho	jeho	k3xOp3gInPc2	jeho
výrobků	výrobek	k1gInPc2	výrobek
byla	být	k5eAaImAgFnS	být
podpořena	podpořit	k5eAaPmNgFnS	podpořit
třemi	tři	k4xCgInPc7	tři
základními	základní	k2eAgInPc7d1	základní
faktory	faktor	k1gInPc7	faktor
<g/>
:	:	kIx,	:
optimálními	optimální	k2eAgFnPc7d1	optimální
podmínkami	podmínka	k1gFnPc7	podmínka
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
surovin	surovina	k1gFnPc2	surovina
sladovnického	sladovnický	k2eAgInSc2d1	sladovnický
ječmene	ječmen	k1gInSc2	ječmen
a	a	k8xC	a
chmele	chmel	k1gInSc2	chmel
<g/>
,	,	kIx,	,
orientací	orientace	k1gFnSc7	orientace
rozvíjejícího	rozvíjející	k2eAgMnSc2d1	rozvíjející
se	se	k3xPyFc4	se
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
sladařských	sladařský	k2eAgFnPc2d1	sladařská
a	a	k8xC	a
pivovarských	pivovarský	k2eAgFnPc2d1	Pivovarská
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
vyvážela	vyvážet	k5eAaImAgFnS	vyvážet
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
do	do	k7c2	do
400	[number]	k4	400
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
zajištěním	zajištění	k1gNnSc7	zajištění
jak	jak	k6eAd1	jak
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
středoškolské	středoškolský	k2eAgFnSc2d1	středoškolská
výuky	výuka	k1gFnSc2	výuka
pivovarských	pivovarský	k2eAgMnPc2d1	pivovarský
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
možnost	možnost	k1gFnSc1	možnost
výstavby	výstavba	k1gFnSc2	výstavba
pivovarů	pivovar	k1gInPc2	pivovar
a	a	k8xC	a
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
okolo	okolo	k6eAd1	okolo
30	[number]	k4	30
nových	nový	k2eAgMnPc2d1	nový
měšťanských	měšťanský	k2eAgMnPc2d1	měšťanský
<g/>
,	,	kIx,	,
akciových	akciový	k2eAgInPc2d1	akciový
a	a	k8xC	a
soukromých	soukromý	k2eAgInPc2d1	soukromý
pivovarů	pivovar	k1gInPc2	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
celková	celkový	k2eAgFnSc1d1	celková
výroba	výroba	k1gFnSc1	výroba
piva	pivo	k1gNnSc2	pivo
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
pivovarů	pivovar	k1gInPc2	pivovar
<g/>
,	,	kIx,	,
malé	malý	k2eAgInPc1d1	malý
pivovary	pivovar	k1gInPc1	pivovar
zanikaly	zanikat	k5eAaImAgInP	zanikat
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
vyváželo	vyvážet	k5eAaImAgNnS	vyvážet
prakticky	prakticky	k6eAd1	prakticky
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
nová	nový	k2eAgFnSc1d1	nová
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
převzala	převzít	k5eAaPmAgFnS	převzít
z	z	k7c2	z
bývalé	bývalý	k2eAgFnSc2d1	bývalá
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
asi	asi	k9	asi
60	[number]	k4	60
%	%	kIx~	%
výrobního	výrobní	k2eAgInSc2d1	výrobní
potenciálu	potenciál	k1gInSc2	potenciál
pivovarů	pivovar	k1gInPc2	pivovar
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
562	[number]	k4	562
pivovarů	pivovar	k1gInPc2	pivovar
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Katastrofu	katastrofa	k1gFnSc4	katastrofa
českému	český	k2eAgNnSc3d1	české
pivovarnictví	pivovarnictví	k1gNnSc3	pivovarnictví
jako	jako	k8xS	jako
veškerému	veškerý	k3xTgInSc3	veškerý
světovému	světový	k2eAgInSc3d1	světový
vývoji	vývoj	k1gInSc3	vývoj
přinesla	přinést	k5eAaPmAgFnS	přinést
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
řada	řada	k1gFnSc1	řada
uzavřených	uzavřený	k2eAgInPc2d1	uzavřený
pivovarů	pivovar	k1gInPc2	pivovar
již	již	k6eAd1	již
neobnovila	obnovit	k5eNaPmAgFnS	obnovit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Pivovarský	pivovarský	k2eAgInSc1d1	pivovarský
a	a	k8xC	a
sladařský	sladařský	k2eAgInSc1d1	sladařský
průmysl	průmysl	k1gInSc1	průmysl
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Československé	československý	k2eAgFnSc6d1	Československá
republice	republika	k1gFnSc6	republika
zestátněn	zestátněn	k2eAgInSc1d1	zestátněn
a	a	k8xC	a
centrálně	centrálně	k6eAd1	centrálně
řízen	řídit	k5eAaImNgInS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
nebyly	být	k5eNaImAgFnP	být
do	do	k7c2	do
pivovarů	pivovar	k1gInPc2	pivovar
a	a	k8xC	a
sladoven	sladovna	k1gFnPc2	sladovna
vkládány	vkládat	k5eAaImNgInP	vkládat
potřebné	potřebný	k2eAgInPc1d1	potřebný
finanční	finanční	k2eAgInPc1d1	finanční
prostředky	prostředek	k1gInPc1	prostředek
na	na	k7c4	na
modernizaci	modernizace	k1gFnSc4	modernizace
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
pivovarský	pivovarský	k2eAgInSc1d1	pivovarský
obor	obor	k1gInSc1	obor
zajistil	zajistit	k5eAaPmAgInS	zajistit
na	na	k7c6	na
domácím	domácí	k2eAgInSc6d1	domácí
trhu	trh	k1gInSc6	trh
dostatek	dostatek	k1gInSc1	dostatek
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
schopný	schopný	k2eAgMnSc1d1	schopný
vyvážet	vyvážet	k5eAaImF	vyvážet
pivo	pivo	k1gNnSc4	pivo
a	a	k8xC	a
slad	slad	k1gInSc4	slad
nejen	nejen	k6eAd1	nejen
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
socialistických	socialistický	k2eAgFnPc2d1	socialistická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
náročný	náročný	k2eAgInSc4d1	náročný
trh	trh	k1gInSc4	trh
kapitalistických	kapitalistický	k2eAgFnPc2d1	kapitalistická
oblastí	oblast	k1gFnPc2	oblast
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgInPc4d1	světový
válce	válec	k1gInPc4	válec
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
postaveny	postavit	k5eAaPmNgInP	postavit
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
pivovary	pivovar	k1gInPc4	pivovar
<g/>
:	:	kIx,	:
Radegast	Radegast	k1gMnSc1	Radegast
a	a	k8xC	a
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Sladoven	sladovna	k1gFnPc2	sladovna
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
období	období	k1gNnSc6	období
socialistického	socialistický	k2eAgNnSc2d1	socialistické
Československa	Československo	k1gNnSc2	Československo
8	[number]	k4	8
pivovarů	pivovar	k1gInPc2	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
privatizace	privatizace	k1gFnSc1	privatizace
pivovarů	pivovar	k1gInPc2	pivovar
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
,	,	kIx,	,
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
kapitál	kapitál	k1gInSc1	kapitál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
36	[number]	k4	36
sladoven	sladovna	k1gFnPc2	sladovna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vyrobily	vyrobit	k5eAaPmAgFnP	vyrobit
483	[number]	k4	483
693	[number]	k4	693
tun	tuna	k1gFnPc2	tuna
sladu	slad	k1gInSc2	slad
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vývoz	vývoz	k1gInSc1	vývoz
činil	činit	k5eAaImAgInS	činit
213	[number]	k4	213
324	[number]	k4	324
tun	tuna	k1gFnPc2	tuna
(	(	kIx(	(
<g/>
44,10	[number]	k4	44,10
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Činných	činný	k2eAgInPc2d1	činný
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
pivovarů	pivovar	k1gInPc2	pivovar
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
48	[number]	k4	48
<g/>
,	,	kIx,	,
vyrobily	vyrobit	k5eAaPmAgFnP	vyrobit
18	[number]	k4	18
548	[number]	k4	548
314	[number]	k4	314
hl	hl	k?	hl
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
bylo	být	k5eAaImAgNnS	být
vyvezeno	vyvézt	k5eAaPmNgNnS	vyvézt
2	[number]	k4	2
129	[number]	k4	129
848	[number]	k4	848
hl	hl	k?	hl
(	(	kIx(	(
<g/>
11,48	[number]	k4	11,48
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roční	roční	k2eAgFnSc1d1	roční
spotřeba	spotřeba	k1gFnSc1	spotřeba
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
činila	činit	k5eAaImAgFnS	činit
160,9	[number]	k4	160,9
l	l	kA	l
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
největších	veliký	k2eAgInPc2d3	veliký
pivovarů	pivovar	k1gInPc2	pivovar
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
84	[number]	k4	84
%	%	kIx~	%
produkce	produkce	k1gFnSc2	produkce
českého	český	k2eAgNnSc2d1	české
piva	pivo	k1gNnSc2	pivo
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pivovary	pivovar	k1gInPc4	pivovar
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
Prazdroj	prazdroj	k1gInSc1	prazdroj
<g/>
,	,	kIx,	,
Budějovický	budějovický	k2eAgInSc1d1	budějovický
Budvar	budvar	k1gInSc1	budvar
<g/>
,	,	kIx,	,
Staropramen	staropramen	k1gInSc1	staropramen
<g/>
,	,	kIx,	,
Královský	královský	k2eAgInSc1d1	královský
Pivovar	pivovar	k1gInSc1	pivovar
Krušovice	Krušovice	k1gFnSc2	Krušovice
<g/>
,	,	kIx,	,
PMS	PMS	kA	PMS
Přerov	Přerov	k1gInSc1	Přerov
<g/>
,	,	kIx,	,
Drinks	Drinks	k1gInSc1	Drinks
Union	union	k1gInSc1	union
a	a	k8xC	a
Starobrno	Starobrno	k1gNnSc1	Starobrno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Druhy	druh	k1gInPc4	druh
piv	pivo	k1gNnPc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
z	z	k7c2	z
několika	několik	k4yIc2	několik
různých	různý	k2eAgNnPc2d1	různé
hledisek	hledisko	k1gNnPc2	hledisko
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
stupňovitost	stupňovitost	k1gFnSc1	stupňovitost
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
pivní	pivní	k2eAgInSc1d1	pivní
styl	styl	k1gInSc1	styl
<g/>
.	.	kIx.	.
</s>
<s>
Stupňovitost	stupňovitost	k1gFnSc1	stupňovitost
piva	pivo	k1gNnSc2	pivo
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
EPM	EPM	kA	EPM
(	(	kIx(	(
<g/>
extrakt	extrakt	k1gInSc1	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
obsah	obsah	k1gInSc4	obsah
všech	všecek	k3xTgFnPc2	všecek
extraktivních	extraktivní	k2eAgFnPc2d1	extraktivní
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
mladině	mladina	k1gFnSc6	mladina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
cukry	cukr	k1gInPc4	cukr
(	(	kIx(	(
<g/>
zkvasitelné	zkvasitelný	k2eAgInPc4d1	zkvasitelný
a	a	k8xC	a
nezkvasitelné	zkvasitelný	k2eNgInPc4d1	zkvasitelný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zkvasitelných	zkvasitelný	k2eAgInPc2d1	zkvasitelný
cukrů	cukr	k1gInPc2	cukr
pak	pak	k6eAd1	pak
při	při	k7c6	při
kvašení	kvašení	k1gNnSc6	kvašení
piva	pivo	k1gNnSc2	pivo
vzniká	vznikat	k5eAaImIp3nS	vznikat
alkohol	alkohol	k1gInSc1	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Stupňovitost	stupňovitost	k1gFnSc1	stupňovitost
piva	pivo	k1gNnSc2	pivo
tedy	tedy	k8xC	tedy
udává	udávat	k5eAaImIp3nS	udávat
plnost	plnost	k1gFnSc4	plnost
(	(	kIx(	(
<g/>
hutnost	hutnost	k1gFnSc4	hutnost
<g/>
)	)	kIx)	)
piva	pivo	k1gNnSc2	pivo
→	→	k?	→
10	[number]	k4	10
<g/>
°	°	k?	°
tedy	tedy	k9	tedy
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
10	[number]	k4	10
<g/>
%	%	kIx~	%
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
°	°	k?	°
pak	pak	k6eAd1	pak
12	[number]	k4	12
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
→	→	k?	→
čím	co	k3yInSc7	co
více	hodně	k6eAd2	hodně
extraktivních	extraktivní	k2eAgFnPc2d1	extraktivní
látek	látka	k1gFnPc2	látka
tím	ten	k3xDgNnSc7	ten
plnější	plný	k2eAgNnSc1d2	plnější
pivo	pivo	k1gNnSc1	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
také	také	k9	také
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
že	že	k8xS	že
stupňovitost	stupňovitost	k1gFnSc1	stupňovitost
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrná	úměrná	k1gFnSc1	úměrná
obsahu	obsah	k1gInSc2	obsah
alkoholu	alkohol	k1gInSc2	alkohol
(	(	kIx(	(
<g/>
čím	co	k3yInSc7	co
více	hodně	k6eAd2	hodně
zkvasitelných	zkvasitelný	k2eAgInPc2d1	zkvasitelný
cukrů	cukr	k1gInPc2	cukr
mladina	mladina	k1gFnSc1	mladina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tím	ten	k3xDgInSc7	ten
více	hodně	k6eAd2	hodně
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
piva	pivo	k1gNnSc2	pivo
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
piva	pivo	k1gNnSc2	pivo
dle	dle	k7c2	dle
stupňovitosti	stupňovitost	k1gFnSc2	stupňovitost
není	být	k5eNaImIp3nS	být
jednotné	jednotný	k2eAgNnSc1d1	jednotné
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stolní	stolní	k2eAgNnSc1d1	stolní
pivo	pivo	k1gNnSc1	pivo
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
1,01	[number]	k4	1,01
až	až	k9	až
6,99	[number]	k4	6,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Výčepní	výčepní	k2eAgNnSc4d1	výčepní
pivo	pivo	k1gNnSc4	pivo
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
7,00	[number]	k4	7,00
až	až	k9	až
10,99	[number]	k4	10,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Ležák	ležák	k1gMnSc1	ležák
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
11,00	[number]	k4	11,00
až	až	k9	až
12,99	[number]	k4	12,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Speciální	speciální	k2eAgNnSc4d1	speciální
pivo	pivo	k1gNnSc4	pivo
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
13,00	[number]	k4	13,00
až	až	k9	až
17,99	[number]	k4	17,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Porter	porter	k1gInSc1	porter
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g />
.	.	kIx.	.
</s>
<s>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
18,00	[number]	k4	18,00
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
a	a	k8xC	a
pak	pak	k6eAd1	pak
také	také	k9	také
dvě	dva	k4xCgFnPc4	dva
speciální	speciální	k2eAgFnPc4d1	speciální
skupiny	skupina	k1gFnPc4	skupina
Nealkoholické	alkoholický	k2eNgNnSc4d1	nealkoholické
pivo	pivo	k1gNnSc4	pivo
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
0,40	[number]	k4	0,40
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Pivo	pivo	k1gNnSc4	pivo
se	s	k7c7	s
sníženým	snížený	k2eAgInSc7d1	snížený
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
0,41	[number]	k4	0,41
až	až	k9	až
1,00	[number]	k4	1,00
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Takto	takto	k6eAd1	takto
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
pivo	pivo	k1gNnSc1	pivo
Předpis	předpis	k1gInSc1	předpis
č.	č.	k?	č.
335	[number]	k4	335
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
změna	změna	k1gFnSc1	změna
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
nepřesnosti	nepřesnost	k1gFnSc2	nepřesnost
a	a	k8xC	a
zastaralosti	zastaralost	k1gFnSc2	zastaralost
<g/>
.	.	kIx.	.
</s>
<s>
Einfachbier	Einfachbier	k1gInSc1	Einfachbier
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
1,51	[number]	k4	1,51
až	až	k9	až
6,99	[number]	k4	6,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Schankbier	Schankbier	k1gMnSc1	Schankbier
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
výčepní	výčepní	k2eAgNnSc1d1	výčepní
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
7,00	[number]	k4	7,00
až	až	k9	až
10,99	[number]	k4	10,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Vollbier	Vollbier	k1gMnSc1	Vollbier
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
plné	plný	k2eAgNnSc4d1	plné
pivo	pivo	k1gNnSc4	pivo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
11,00	[number]	k4	11,00
až	až	k9	až
15,99	[number]	k4	15,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Starkbier	Starkbier	k1gMnSc1	Starkbier
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
16,00	[number]	k4	16,00
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Abzugsbier	Abzugsbier	k1gMnSc1	Abzugsbier
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
9,00	[number]	k4	9,00
až	až	k9	až
9,99	[number]	k4	9,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Schankbier	Schankbier	k1gMnSc1	Schankbier
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
výčepní	výčepní	k2eAgNnSc1d1	výčepní
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
10,00	[number]	k4	10,00
až	až	k9	až
11,99	[number]	k4	11,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Vollbier	Vollbier	k1gMnSc1	Vollbier
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
plné	plný	k2eAgNnSc4d1	plné
pivo	pivo	k1gNnSc4	pivo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
12,00	[number]	k4	12,00
až	až	k9	až
14,99	[number]	k4	14,99
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Spezialbier	Spezialbier	k1gMnSc1	Spezialbier
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
speciální	speciální	k2eAgNnSc1d1	speciální
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13,00	[number]	k4	13,00
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Starkbier	Starkbier	k1gMnSc1	Starkbier
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
16,00	[number]	k4	16,00
%	%	kIx~	%
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
Barva	barva	k1gFnSc1	barva
piva	pivo	k1gNnSc2	pivo
popisuje	popisovat	k5eAaImIp3nS	popisovat
pivo	pivo	k1gNnSc4	pivo
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zrakového	zrakový	k2eAgNnSc2d1	zrakové
vnímání	vnímání	k1gNnSc2	vnímání
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
platných	platný	k2eAgInPc2d1	platný
český	český	k2eAgInSc4d1	český
zákonů	zákon	k1gInPc2	zákon
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
pivo	pivo	k1gNnSc4	pivo
světlé	světlý	k2eAgFnSc2d1	světlá
<g/>
,	,	kIx,	,
polotmavé	polotmavý	k2eAgFnSc2d1	polotmavá
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgFnSc2d1	tmavá
a	a	k8xC	a
řezané	řezaný	k2eAgFnSc2d1	řezaná
<g/>
.	.	kIx.	.
</s>
<s>
Světlé	světlý	k2eAgNnSc1d1	světlé
pivo	pivo	k1gNnSc1	pivo
-	-	kIx~	-
pivo	pivo	k1gNnSc1	pivo
vařené	vařený	k2eAgNnSc1d1	vařené
především	především	k9	především
ze	z	k7c2	z
světlých	světlý	k2eAgInPc2d1	světlý
sladů	slad	k1gInPc2	slad
Polotmavá	polotmavý	k2eAgFnSc1d1	polotmavá
a	a	k8xC	a
tmavá	tmavý	k2eAgNnPc4d1	tmavé
piva	pivo	k1gNnPc4	pivo
-	-	kIx~	-
pivo	pivo	k1gNnSc1	pivo
vařené	vařený	k2eAgNnSc1d1	vařené
z	z	k7c2	z
tmavých	tmavý	k2eAgInPc2d1	tmavý
<g/>
,	,	kIx,	,
karamelových	karamelový	k2eAgInPc2d1	karamelový
popř.	popř.	kA	popř.
barevných	barevný	k2eAgInPc2d1	barevný
sladů	slad	k1gInPc2	slad
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
světlými	světlý	k2eAgInPc7d1	světlý
slady	slad	k1gInPc7	slad
Řezané	řezaný	k2eAgNnSc1d1	řezané
pivo	pivo	k1gNnSc1	pivo
-	-	kIx~	-
pivo	pivo	k1gNnSc1	pivo
smíšené	smíšený	k2eAgNnSc1d1	smíšené
ze	z	k7c2	z
světlých	světlý	k2eAgNnPc2d1	světlé
a	a	k8xC	a
tmavých	tmavý	k2eAgNnPc2d1	tmavé
piv	pivo	k1gNnPc2	pivo
Svrchně	svrchně	k6eAd1	svrchně
kvašená	kvašený	k2eAgNnPc1d1	kvašené
piva	pivo	k1gNnPc1	pivo
-	-	kIx~	-
svrchně	svrchně	k6eAd1	svrchně
kvašená	kvašený	k2eAgNnPc1d1	kvašené
piva	pivo	k1gNnPc1	pivo
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
okolo	okolo	k7c2	okolo
15	[number]	k4	15
-	-	kIx~	-
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
kvasinek	kvasinka	k1gFnPc2	kvasinka
Saccharomyces	Saccharomyces	k1gInSc4	Saccharomyces
cerevisiae	cerevisiaat	k5eAaPmIp3nS	cerevisiaat
Rees	Rees	k1gInSc1	Rees
Mayen	Mayen	k2eAgInSc1d1	Mayen
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
Pšeničné	pšeničný	k2eAgNnSc1d1	pšeničné
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
Stout	Stout	k1gInSc1	Stout
<g/>
,	,	kIx,	,
Porter	porter	k1gInSc1	porter
<g/>
,	,	kIx,	,
Trappist	Trappist	k1gInSc1	Trappist
Spodně	spodně	k6eAd1	spodně
kvašená	kvašený	k2eAgNnPc1d1	kvašené
piva	pivo	k1gNnPc1	pivo
(	(	kIx(	(
<g/>
Ležáky	Ležáky	k1gInPc1	Ležáky
<g/>
)	)	kIx)	)
-Spodně	-Spodně	k6eAd1	-Spodně
kvašená	kvašený	k2eAgNnPc1d1	kvašené
piva	pivo	k1gNnPc1	pivo
vznikají	vznikat	k5eAaImIp3nP	vznikat
za	za	k7c4	za
kvašení	kvašení	k1gNnSc4	kvašení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
teplotách	teplota	k1gFnPc6	teplota
pohybujících	pohybující	k2eAgFnPc6d1	pohybující
se	se	k3xPyFc4	se
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
8	[number]	k4	8
-	-	kIx~	-
14	[number]	k4	14
°	°	k?	°
<g/>
C.	C.	kA	C.
Kvašení	kvašení	k1gNnSc1	kvašení
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
kvasinky	kvasinka	k1gFnSc2	kvasinka
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
cerevisiae	cerevisiaat	k5eAaPmIp3nS	cerevisiaat
Carlsbergensis	Carlsbergensis	k1gInSc1	Carlsbergensis
Hansen-Pilsner	Hansen-Pilsnra	k1gFnPc2	Hansen-Pilsnra
<g/>
,	,	kIx,	,
Bock	Bocka	k1gFnPc2	Bocka
<g/>
,	,	kIx,	,
Märzen	Märzna	k1gFnPc2	Märzna
<g/>
,	,	kIx,	,
Piva	pivo	k1gNnSc2	pivo
bavorského	bavorský	k2eAgInSc2d1	bavorský
typu	typ	k1gInSc2	typ
Spontánně	spontánně	k6eAd1	spontánně
kvašená	kvašený	k2eAgNnPc4d1	kvašené
piva	pivo	k1gNnPc4	pivo
-V	-V	k?	-V
historii	historie	k1gFnSc3	historie
jediný	jediný	k2eAgInSc1d1	jediný
způsob	způsob	k1gInSc1	způsob
kvašení	kvašení	k1gNnSc2	kvašení
piva	pivo	k1gNnSc2	pivo
-	-	kIx~	-
zkvasí	zkvasit	k5eAaPmIp3nS	zkvasit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
co	co	k9	co
zbude	zbýt	k5eAaPmIp3nS	zbýt
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
po	po	k7c6	po
předchozí	předchozí	k2eAgFnSc6d1	předchozí
várce	várka	k1gFnSc6	várka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
tato	tento	k3xDgNnPc1	tento
piva	pivo	k1gNnPc1	pivo
nejvíce	nejvíce	k6eAd1	nejvíce
rozšířena	rozšířen	k2eAgNnPc1d1	rozšířeno
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Lambik	Lambik	k1gInSc1	Lambik
<g/>
,	,	kIx,	,
Gueuze	Gueuze	k1gFnSc1	Gueuze
<g/>
,	,	kIx,	,
Kriek	Kriek	k1gInSc1	Kriek
<g/>
,	,	kIx,	,
Frambozen	Frambozen	k2eAgInSc1d1	Frambozen
<g/>
,	,	kIx,	,
Faro	fara	k1gFnSc5	fara
Svrchně	svrchně	k6eAd1	svrchně
kvašená	kvašený	k2eAgNnPc1d1	kvašené
piva	pivo	k1gNnPc1	pivo
obvykle	obvykle	k6eAd1	obvykle
kvasí	kvasit	k5eAaImIp3nP	kvasit
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
kolem	kolem	k7c2	kolem
15-20	[number]	k4	15-20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
i	i	k9	i
vyšších	vysoký	k2eAgFnPc2d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
vytváření	vytváření	k1gNnSc2	vytváření
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
kvasícího	kvasící	k2eAgNnSc2d1	kvasící
piva	pivo	k1gNnSc2	pivo
pěnu	pěn	k2eAgFnSc4d1	pěna
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
stoupajícím	stoupající	k2eAgMnPc3d1	stoupající
CO2	CO2	k1gMnPc3	CO2
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kvasnicemi	kvasnice	k1gFnPc7	kvasnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
piva	pivo	k1gNnPc1	pivo
nazývají	nazývat	k5eAaImIp3nP	nazývat
svrchně	svrchně	k6eAd1	svrchně
kvašená	kvašený	k2eAgNnPc1d1	kvašené
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
kvašení	kvašení	k1gNnSc2	kvašení
těchto	tento	k3xDgNnPc2	tento
piv	pivo	k1gNnPc2	pivo
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
tří	tři	k4xCgInPc2	tři
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
kvasit	kvasit	k5eAaImF	kvasit
i	i	k9	i
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
"	"	kIx"	"
<g/>
Ejl	Ejl	k1gMnSc1	Ejl
<g/>
"	"	kIx"	"
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejběžnější	běžný	k2eAgInSc4d3	nejběžnější
styl	styl	k1gInSc4	styl
svrchně	svrchně	k6eAd1	svrchně
kvašený	kvašený	k2eAgInSc4d1	kvašený
piv	pivo	k1gNnPc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Piva	pivo	k1gNnPc1	pivo
můžou	můžou	k?	můžou
mít	mít	k5eAaImF	mít
rozličnou	rozličný	k2eAgFnSc4d1	rozličná
barvu	barva	k1gFnSc4	barva
i	i	k8xC	i
stupňovitost	stupňovitost	k1gFnSc4	stupňovitost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ejly	Ejly	k1gInPc1	Ejly
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
širokou	široký	k2eAgFnSc7d1	široká
škálou	škála	k1gFnSc7	škála
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
vůní	vůně	k1gFnSc7	vůně
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
po	po	k7c6	po
vypití	vypití	k1gNnSc6	vypití
tóny	tón	k1gInPc4	tón
citrusů	citrus	k1gInPc2	citrus
či	či	k8xC	či
rozličných	rozličný	k2eAgNnPc2d1	rozličné
koření	koření	k1gNnPc2	koření
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
označením	označení	k1gNnSc7	označení
Ale	ale	k9	ale
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
i	i	k8xC	i
celé	celý	k2eAgFnPc4d1	celá
skupiny	skupina	k1gFnPc4	skupina
svrchně	svrchně	k6eAd1	svrchně
kvašených	kvašený	k2eAgNnPc2d1	kvašené
piv	pivo	k1gNnPc2	pivo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ejly	Ejly	k1gInPc1	Ejly
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
především	především	k9	především
podle	podle	k7c2	podle
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
uvařeny	uvařen	k2eAgFnPc1d1	uvařena
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
anglo-amerického	anglomerický	k2eAgInSc2d1	anglo-americký
typu	typ	k1gInSc2	typ
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
(	(	kIx(	(
<g/>
světlý	světlý	k2eAgInSc4d1	světlý
ejl	ejl	k?	ejl
<g/>
)	)	kIx)	)
-	-	kIx~	-
plné	plný	k2eAgNnSc4d1	plné
pivo	pivo	k1gNnSc4	pivo
se	s	k7c7	s
zlatou	zlatá	k1gFnSc7	zlatá
až	až	k9	až
tmavě	tmavě	k6eAd1	tmavě
měděnou	měděný	k2eAgFnSc7d1	měděná
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
chutí	chuť	k1gFnSc7	chuť
s	s	k7c7	s
ovocnými	ovocný	k2eAgInPc7d1	ovocný
tóny	tón	k1gInPc7	tón
Brown	Brown	k1gMnSc1	Brown
Ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
tmavý	tmavý	k2eAgMnSc1d1	tmavý
ejl	ejl	k?	ejl
<g/>
)	)	kIx)	)
-	-	kIx~	-
plné	plný	k2eAgNnSc4d1	plné
až	až	k8xS	až
silné	silný	k2eAgNnSc4d1	silné
pivo	pivo	k1gNnSc4	pivo
tmavě	tmavě	k6eAd1	tmavě
jantarové	jantarový	k2eAgFnPc1d1	jantarová
až	až	k8xS	až
hnědé	hnědý	k2eAgFnPc1d1	hnědá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
obvykle	obvykle	k6eAd1	obvykle
zabarvena	zabarven	k2eAgFnSc1d1	zabarvena
<g />
.	.	kIx.	.
</s>
<s>
čokoládově	čokoládově	k6eAd1	čokoládově
či	či	k8xC	či
oříškově	oříškově	k6eAd1	oříškově
<g/>
,	,	kIx,	,
vařeno	vařen	k2eAgNnSc1d1	vařeno
z	z	k7c2	z
tmavýh	tmavýha	k1gFnPc2	tmavýha
sladů	slad	k1gInPc2	slad
Mild	Milda	k1gFnPc2	Milda
Ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
mladý	mladý	k2eAgMnSc1d1	mladý
či	či	k8xC	či
lehký	lehký	k2eAgInSc1d1	lehký
ejl	ejl	k?	ejl
<g/>
)	)	kIx)	)
-	-	kIx~	-
výčepní	výčepní	k2eAgNnSc1d1	výčepní
pivo	pivo	k1gNnSc1	pivo
obvykle	obvykle	k6eAd1	obvykle
světlé	světlý	k2eAgFnPc4d1	světlá
barvy	barva	k1gFnPc4	barva
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
India	indium	k1gNnSc2	indium
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
(	(	kIx(	(
<g/>
světlý	světlý	k2eAgInSc1d1	světlý
chmelený	chmelený	k2eAgInSc1d1	chmelený
ejl	ejl	k?	ejl
<g/>
)	)	kIx)	)
-	-	kIx~	-
vychází	vycházet	k5eAaImIp3nS	vycházet
k	k	k7c3	k
klasického	klasický	k2eAgNnSc2d1	klasické
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
<g />
.	.	kIx.	.
</s>
<s>
chmelené	chmelený	k2eAgNnSc1d1	chmelené
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
verzích	verze	k1gFnPc6	verze
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
toto	tento	k3xDgNnSc4	tento
pivo	pivo	k1gNnSc4	pivo
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgInSc1d2	vyšší
obsah	obsah	k1gInSc1	obsah
alkoholu	alkohol	k1gInSc2	alkohol
Old	Olda	k1gFnPc2	Olda
Ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
starý	starý	k2eAgMnSc1d1	starý
ejl	ejl	k?	ejl
<g/>
)	)	kIx)	)
-	-	kIx~	-
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
tmavě	tmavě	k6eAd1	tmavě
jantarové	jantarový	k2eAgNnSc1d1	jantarové
až	až	k9	až
skoro	skoro	k6eAd1	skoro
černé	černý	k2eAgFnPc1d1	černá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
sladová	sladový	k2eAgFnSc1d1	sladová
chuť	chuť	k1gFnSc1	chuť
s	s	k7c7	s
podtóny	podtón	k1gInPc7	podtón
rybízu	rybíz	k1gInSc2	rybíz
či	či	k8xC	či
hrozinek	hrozinka	k1gFnPc2	hrozinka
<g/>
,	,	kIx,	,
proces	proces	k1gInSc1	proces
<g />
.	.	kIx.	.
</s>
<s>
kvašení	kvašení	k1gNnSc1	kvašení
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
Cascadian	Cascadian	k1gMnSc1	Cascadian
Dark	Dark	k1gMnSc1	Dark
Ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
tmavý	tmavý	k2eAgMnSc1d1	tmavý
ejl	ejl	k?	ejl
<g/>
)	)	kIx)	)
-	-	kIx~	-
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
tmavé	tmavý	k2eAgFnSc2d1	tmavá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tmavou	tmavý	k2eAgFnSc4d1	tmavá
verzi	verze	k1gFnSc4	verze
Indian	Indiana	k1gFnPc2	Indiana
Pale	pal	k1gInSc5	pal
Ale	ale	k9	ale
Strong	Strong	k1gInSc1	Strong
Ale	ale	k9	ale
(	(	kIx(	(
<g/>
silný	silný	k2eAgInSc4d1	silný
ejl	ejl	k?	ejl
<g/>
)	)	kIx)	)
-	-	kIx~	-
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
tmavé	tmavý	k2eAgFnSc2d1	tmavá
jantarové	jantarový	k2eAgFnSc2d1	jantarová
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
Amber	ambra	k1gFnPc2	ambra
Ale	ale	k8xC	ale
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
polotmavý	polotmavý	k2eAgMnSc1d1	polotmavý
ejl	ejl	k?	ejl
<g/>
)	)	kIx)	)
-	-	kIx~	-
plné	plný	k2eAgNnSc4d1	plné
až	až	k8xS	až
silné	silný	k2eAgNnSc4d1	silné
pivo	pivo	k1gNnSc4	pivo
měděné	měděný	k2eAgFnSc2d1	měděná
až	až	k8xS	až
hnědé	hnědý	k2eAgFnSc2d1	hnědá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
chmelovou	chmelový	k2eAgFnSc7d1	chmelová
chutí	chuť	k1gFnSc7	chuť
Ale	ale	k8xC	ale
belgicko-francouzkého	belgickorancouzký	k2eAgInSc2d1	belgicko-francouzký
typu	typ	k1gInSc2	typ
Tripel	tripel	k1gInSc1	tripel
-	-	kIx~	-
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
žluté	žlutý	k2eAgFnSc2d1	žlutá
až	až	k8xS	až
zlaté	zlatý	k2eAgFnSc2d1	zlatá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
kvasnicová	kvasnicový	k2eAgFnSc1d1	kvasnicová
<g/>
,	,	kIx,	,
kořeněná	kořeněný	k2eAgFnSc1d1	kořeněná
Strong	Strong	k1gInSc4	Strong
Dark	Dark	k1gInSc4	Dark
Ale	ale	k9	ale
(	(	kIx(	(
<g/>
silný	silný	k2eAgInSc4d1	silný
tmavý	tmavý	k2eAgInSc4d1	tmavý
ejl	ejl	k?	ejl
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
tmavé	tmavý	k2eAgFnSc2d1	tmavá
jantarové	jantarový	k2eAgFnSc2d1	jantarová
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
Dubbel	Dubbela	k1gFnPc2	Dubbela
-	-	kIx~	-
klášterní	klášterní	k2eAgNnSc1d1	klášterní
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgFnPc1d1	tmavá
barvy	barva	k1gFnPc1	barva
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
ovocnou	ovocný	k2eAgFnSc7d1	ovocná
a	a	k8xC	a
obilnou	obilný	k2eAgFnSc7d1	obilná
chutí	chuť	k1gFnSc7	chuť
Saison	Saisona	k1gFnPc2	Saisona
Flanders	Flandersa	k1gFnPc2	Flandersa
Red	Red	k1gFnSc2	Red
Ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
vlámský	vlámský	k2eAgInSc1d1	vlámský
červený	červený	k2eAgInSc1d1	červený
ejl	ejl	k?	ejl
<g/>
)	)	kIx)	)
-	-	kIx~	-
plné	plný	k2eAgNnSc4d1	plné
pivo	pivo	k1gNnSc4	pivo
tmavě	tmavě	k6eAd1	tmavě
červené	červený	k2eAgFnPc1d1	červená
až	až	k8xS	až
hnědé	hnědý	k2eAgFnPc1d1	hnědá
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
trpké	trpký	k2eAgFnPc1d1	trpká
až	až	k6eAd1	až
<g />
.	.	kIx.	.
</s>
<s>
kyselé	kyselý	k2eAgFnSc3d1	kyselá
chuti	chuť	k1gFnSc3	chuť
Quadrupel	Quadrupela	k1gFnPc2	Quadrupela
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc4d1	silné
pivo	pivo	k1gNnSc4	pivo
<g/>
,	,	kIx,	,
tmavší	tmavý	k2eAgFnPc4d2	tmavší
barvy	barva	k1gFnPc4	barva
vyzrálé	vyzrálý	k2eAgFnSc2d1	vyzrálá
ovocné	ovocný	k2eAgFnSc2d1	ovocná
chuti	chuť	k1gFnSc2	chuť
Ale	ale	k8xC	ale
německého	německý	k2eAgInSc2d1	německý
typu	typ	k1gInSc2	typ
typu	typ	k1gInSc2	typ
Altbier	Altbira	k1gFnPc2	Altbira
(	(	kIx(	(
<g/>
staré	starý	k2eAgNnSc1d1	staré
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
-	-	kIx~	-
plné	plný	k2eAgNnSc1d1	plné
pivo	pivo	k1gNnSc1	pivo
bronzové	bronzový	k2eAgFnSc2d1	bronzová
čiré	čirý	k2eAgFnSc2d1	čirá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
jemné	jemný	k2eAgFnSc2d1	jemná
nahořklé	nahořklý	k2eAgFnSc2d1	nahořklá
chuti	chuť	k1gFnSc2	chuť
Kölsch	Kölscha	k1gFnPc2	Kölscha
(	(	kIx(	(
<g/>
kolínské	kolínský	k2eAgNnSc1d1	kolínské
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
-	-	kIx~	-
plné	plný	k2eAgNnSc4d1	plné
pivo	pivo	k1gNnSc4	pivo
s	s	k7c7	s
bledou	bledý	k2eAgFnSc7d1	bledá
slámově	slámově	k6eAd1	slámově
žlutou	žlutý	k2eAgFnSc7d1	žlutá
<g />
.	.	kIx.	.
</s>
<s>
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
jemnou	jemný	k2eAgFnSc7d1	jemná
ovocnou	ovocný	k2eAgFnSc7d1	ovocná
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
vůní	vůně	k1gFnSc7	vůně
Roggenbier	Roggenbira	k1gFnPc2	Roggenbira
(	(	kIx(	(
<g/>
žitné	žitný	k2eAgNnSc1d1	žitné
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
-	-	kIx~	-
pivo	pivo	k1gNnSc1	pivo
vařené	vařený	k2eAgNnSc1d1	vařené
ze	z	k7c2	z
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
žita	žito	k1gNnSc2	žito
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
50	[number]	k4	50
%	%	kIx~	%
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
65	[number]	k4	65
%	%	kIx~	%
<g/>
)	)	kIx)	)
Stout	Stout	k1gInSc1	Stout
-	-	kIx~	-
Tato	tento	k3xDgNnPc1	tento
piva	pivo	k1gNnPc4	pivo
jsou	být	k5eAaImIp3nP	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
hustou	hustý	k2eAgFnSc7d1	hustá
pěnou	pěna	k1gFnSc7	pěna
<g/>
,	,	kIx,	,
tmavou	tmavý	k2eAgFnSc7d1	tmavá
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
příchutí	příchuť	k1gFnSc7	příchuť
po	po	k7c6	po
praženém	pražený	k2eAgInSc6d1	pražený
sladu	slad	k1gInSc6	slad
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
ovocnou	ovocný	k2eAgFnSc7d1	ovocná
příchutí	příchuť	k1gFnSc7	příchuť
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
silná	silný	k2eAgNnPc4d1	silné
piva	pivo	k1gNnPc4	pivo
(	(	kIx(	(
<g/>
podíl	podíl	k1gInSc1	podíl
alkoholu	alkohol	k1gInSc2	alkohol
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
8	[number]	k4	8
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Imperial	Imperial	k1gMnSc1	Imperial
Stout	Stout	k1gMnSc1	Stout
(	(	kIx(	(
<g/>
silný	silný	k2eAgInSc1d1	silný
stout	stout	k1gInSc1	stout
<g/>
)	)	kIx)	)
-silné	ilný	k2eAgNnSc1d1	-silný
pivo	pivo	k1gNnSc1	pivo
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
čokoládovými	čokoládový	k2eAgInPc7d1	čokoládový
tóny	tón	k1gInPc7	tón
Milk	Milka	k1gFnPc2	Milka
<g/>
/	/	kIx~	/
<g/>
Cream	Cream	k1gInSc4	Cream
Stout	Stout	k2eAgInSc4d1	Stout
(	(	kIx(	(
<g/>
mléčný	mléčný	k2eAgInSc4d1	mléčný
<g/>
/	/	kIx~	/
<g/>
krémový	krémový	k2eAgInSc4d1	krémový
stout	stout	k1gInSc4	stout
<g/>
)	)	kIx)	)
-	-	kIx~	-
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
nasládlé	nasládlý	k2eAgNnSc1d1	nasládlé
Extra	extra	k6eAd1	extra
<g/>
/	/	kIx~	/
<g/>
Foreign	Foreign	k1gInSc1	Foreign
Stout	Stout	k2eAgInSc1d1	Stout
-	-	kIx~	-
plné	plný	k2eAgNnSc1d1	plné
pivo	pivo	k1gNnSc1	pivo
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
Dry	Dry	k1gMnSc1	Dry
Stout	Stout	k1gMnSc1	Stout
(	(	kIx(	(
<g/>
suchý	suchý	k2eAgInSc1d1	suchý
stout	stout	k1gInSc1	stout
<g/>
)	)	kIx)	)
-	-	kIx~	-
silné	silný	k2eAgNnSc1d1	silné
pivo	pivo	k1gNnSc1	pivo
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
Oatmetal	Oatmetal	k1gMnSc1	Oatmetal
Stout	Stout	k1gMnSc1	Stout
(	(	kIx(	(
<g/>
ovesný	ovesný	k2eAgInSc1d1	ovesný
stout	stout	k1gInSc1	stout
<g/>
)	)	kIx)	)
-	-	kIx~	-
plné	plný	k2eAgNnSc4d1	plné
pivo	pivo	k1gNnSc4	pivo
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
ovesných	ovesný	k2eAgInPc2d1	ovesný
sladů	slad	k1gInPc2	slad
Pšeničné	pšeničný	k2eAgNnSc1d1	pšeničné
pivo	pivo	k1gNnSc1	pivo
-	-	kIx~	-
Při	při	k7c6	při
vaření	vaření	k1gNnSc2	vaření
těchto	tento	k3xDgNnPc2	tento
piv	pivo	k1gNnPc2	pivo
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
také	také	k9	také
pšeničných	pšeničný	k2eAgInPc2d1	pšeničný
sladů	slad	k1gInPc2	slad
minimálně	minimálně	k6eAd1	minimálně
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Piva	pivo	k1gNnPc1	pivo
vynikají	vynikat	k5eAaImIp3nP	vynikat
hustou	hustý	k2eAgFnSc7d1	hustá
pěnou	pěna	k1gFnSc7	pěna
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
kalnou	kalný	k2eAgFnSc7d1	kalná
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
obilnou	obilný	k2eAgFnSc7d1	obilná
příchutí	příchuť	k1gFnSc7	příchuť
často	často	k6eAd1	často
ovocným	ovocný	k2eAgInSc7d1	ovocný
podtónem	podtón	k1gInSc7	podtón
(	(	kIx(	(
<g/>
citron	citron	k1gInSc1	citron
<g/>
,	,	kIx,	,
<g/>
banán	banán	k1gInSc1	banán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
WitBier	WitBier	k1gInSc1	WitBier
(	(	kIx(	(
<g/>
bílé	bílý	k2eAgNnSc1d1	bílé
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
-	-	kIx~	-
plné	plný	k2eAgNnSc4d1	plné
nebo	nebo	k8xC	nebo
výčepní	výčepní	k2eAgNnSc4d1	výčepní
pivo	pivo	k1gNnSc4	pivo
,	,	kIx,	,
<g/>
bledé	bledý	k2eAgFnPc4d1	bledá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
ovocné	ovocný	k2eAgFnPc4d1	ovocná
chuti	chuť	k1gFnPc4	chuť
<g/>
,	,	kIx,	,
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
nesladovaná	sladovaný	k2eNgFnSc1d1	sladovaný
pšenice	pšenice	k1gFnSc1	pšenice
Dunkelweizen	Dunkelweizen	k2eAgInSc1d1	Dunkelweizen
(	(	kIx(	(
<g/>
tmavé	tmavý	k2eAgNnSc1d1	tmavé
pšeničné	pšeničný	k2eAgNnSc1d1	pšeničné
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
-	-	kIx~	-
tmavé	tmavý	k2eAgNnSc1d1	tmavé
pšeničné	pšeničný	k2eAgNnSc1d1	pšeničné
pivo	pivo	k1gNnSc1	pivo
vařené	vařený	k2eAgNnSc1d1	vařené
za	za	k7c4	za
užití	užití	k1gNnSc4	užití
tmavých	tmavý	k2eAgInPc2d1	tmavý
sladů	slad	k1gInPc2	slad
Hefewizen	Hefewizen	k2eAgMnSc1d1	Hefewizen
<g/>
/	/	kIx~	/
<g/>
Weissbier	Weissbier	k1gInSc1	Weissbier
(	(	kIx(	(
<g/>
pšeničné	pšeničný	k2eAgNnSc1d1	pšeničné
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
-	-	kIx~	-
klasické	klasický	k2eAgNnSc4d1	klasické
pšeničné	pšeničný	k2eAgNnSc4d1	pšeničné
pivo	pivo	k1gNnSc4	pivo
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
Kristalweizen	Kristalweizen	k2eAgMnSc1d1	Kristalweizen
(	(	kIx(	(
<g/>
čiré	čirý	k2eAgNnSc1d1	čiré
pšeničné	pšeničný	k2eAgNnSc1d1	pšeničné
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
-	-	kIx~	-
Weizenbock	Weizenbock	k1gInSc1	Weizenbock
(	(	kIx(	(
<g/>
pšeničný	pšeničný	k2eAgMnSc1d1	pšeničný
kozel	kozel	k1gMnSc1	kozel
<g/>
)	)	kIx)	)
-	-	kIx~	-
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Výroba	výroba	k1gFnSc1	výroba
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Principem	princip	k1gInSc7	princip
výroby	výroba	k1gFnSc2	výroba
piva	pivo	k1gNnSc2	pivo
jako	jako	k8xS	jako
alkoholického	alkoholický	k2eAgInSc2d1	alkoholický
nápoje	nápoj	k1gInSc2	nápoj
z	z	k7c2	z
obilí	obilí	k1gNnSc2	obilí
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgNnSc4	prvý
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
rozštěpení	rozštěpení	k1gNnSc4	rozštěpení
v	v	k7c6	v
obilných	obilný	k2eAgNnPc6d1	obilné
zrnech	zrno	k1gNnPc6	zrno
přítomných	přítomný	k1gMnPc2	přítomný
složitých	složitý	k2eAgInPc2d1	složitý
cukrů	cukr	k1gInPc2	cukr
(	(	kIx(	(
<g/>
škrobu	škrob	k1gInSc2	škrob
<g/>
)	)	kIx)	)
na	na	k7c4	na
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
zkvasitelné	zkvasitelný	k2eAgInPc4d1	zkvasitelný
cukry	cukr	k1gInPc4	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
následné	následný	k2eAgNnSc4d1	následné
zkvašení	zkvašení	k1gNnSc4	zkvašení
těchto	tento	k3xDgInPc2	tento
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
cukrů	cukr	k1gInPc2	cukr
pomocí	pomocí	k7c2	pomocí
kultury	kultura	k1gFnSc2	kultura
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
výrobního	výrobní	k2eAgInSc2d1	výrobní
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
smíchání	smíchání	k1gNnSc1	smíchání
surovin	surovina	k1gFnPc2	surovina
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
převedení	převedení	k1gNnSc4	převedení
využitelných	využitelný	k2eAgFnPc2d1	využitelná
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
vodného	vodný	k2eAgInSc2d1	vodný
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
slad	slad	k1gInSc4	slad
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
ve	v	k7c6	v
sladovně	sladovna	k1gFnSc6	sladovna
naklíčením	naklíčení	k1gNnSc7	naklíčení
obilných	obilný	k2eAgNnPc2d1	obilné
zrn	zrno	k1gNnPc2	zrno
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
šetrným	šetrný	k2eAgNnSc7d1	šetrné
usušením	usušení	k1gNnSc7	usušení
za	za	k7c4	za
určité	určitý	k2eAgFnPc4d1	určitá
teploty	teplota	k1gFnPc4	teplota
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
sladu	slad	k1gInSc2	slad
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
přesun	přesun	k1gInSc1	přesun
suroviny	surovina	k1gFnSc2	surovina
do	do	k7c2	do
pivovaru	pivovar	k1gInSc2	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
začátku	začátek	k1gInSc6	začátek
samotné	samotný	k2eAgFnSc2d1	samotná
výroby	výroba	k1gFnSc2	výroba
piva	pivo	k1gNnSc2	pivo
se	se	k3xPyFc4	se
slad	slad	k1gInSc1	slad
šrotuje	šrotovat	k5eAaImIp3nS	šrotovat
(	(	kIx(	(
<g/>
rozemele	rozemlít	k5eAaPmIp3nS	rozemlít
<g/>
)	)	kIx)	)
a	a	k8xC	a
smísí	smísit	k5eAaPmIp3nP	smísit
se	se	k3xPyFc4	se
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
směsi	směs	k1gFnSc6	směs
se	se	k3xPyFc4	se
za	za	k7c4	za
zvyšující	zvyšující	k2eAgFnPc4d1	zvyšující
se	se	k3xPyFc4	se
teploty	teplota	k1gFnSc2	teplota
vlivem	vliv	k1gInSc7	vliv
enzymů	enzym	k1gInPc2	enzym
štěpí	štěpit	k5eAaImIp3nS	štěpit
v	v	k7c6	v
zrně	zrno	k1gNnSc6	zrno
obsažený	obsažený	k2eAgInSc4d1	obsažený
škrob	škrob	k1gInSc4	škrob
na	na	k7c4	na
zkvasitelné	zkvasitelný	k2eAgInPc4d1	zkvasitelný
cukry	cukr	k1gInPc4	cukr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
procedura	procedura	k1gFnSc1	procedura
je	být	k5eAaImIp3nS	být
prováděna	provádět	k5eAaImNgFnS	provádět
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
nazývaném	nazývaný	k2eAgInSc6d1	nazývaný
varna	varna	k1gFnSc1	varna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
metody	metoda	k1gFnPc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
metoda	metoda	k1gFnSc1	metoda
spařování	spařování	k1gNnSc2	spařování
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
přípravě	příprava	k1gFnSc3	příprava
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
až	až	k9	až
tří	tři	k4xCgFnPc2	tři
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
slad	slad	k1gInSc1	slad
vystavován	vystavován	k2eAgInSc1d1	vystavován
teplotě	teplota	k1gFnSc6	teplota
okolo	okolo	k7c2	okolo
65	[number]	k4	65
-	-	kIx~	-
68	[number]	k4	68
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
Evropě	Evropa	k1gFnSc6	Evropa
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
převládá	převládat	k5eAaImIp3nS	převládat
tzv.	tzv.	kA	tzv.
dekokční	dekokční	k2eAgInSc1d1	dekokční
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
spařování	spařování	k1gNnSc3	spařování
složitější	složitý	k2eAgFnSc2d2	složitější
procedurou	procedura	k1gFnSc7	procedura
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Jeho	jeho	k3xOp3gInSc7	jeho
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
roztok	roztok	k1gInSc1	roztok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
důslednějším	důsledný	k2eAgFnPc3d2	důslednější
přeměnám	přeměna	k1gFnPc3	přeměna
škrobu	škrob	k1gInSc2	škrob
na	na	k7c4	na
cukry	cukr	k1gInPc4	cukr
a	a	k8xC	a
štěpení	štěpení	k1gNnSc4	štěpení
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
přečerpání	přečerpání	k1gNnSc4	přečerpání
části	část	k1gFnSc2	část
várky	várka	k1gFnSc2	várka
z	z	k7c2	z
vystírací	vystírací	k2eAgFnSc2d1	vystírací
kádě	káď	k1gFnSc2	káď
do	do	k7c2	do
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
nádoby	nádoba	k1gFnSc2	nádoba
rmutovacího	rmutovací	k2eAgInSc2d1	rmutovací
kotle	kotel	k1gInSc2	kotel
(	(	kIx(	(
<g/>
pánve	pánev	k1gFnSc2	pánev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
varu	var	k1gInSc2	var
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
škrobnatých	škrobnatý	k2eAgInPc2d1	škrobnatý
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lepší	dobrý	k2eAgInSc1d2	lepší
přístup	přístup	k1gInSc1	přístup
enzymů	enzym	k1gInPc2	enzym
k	k	k7c3	k
molekulám	molekula	k1gFnPc3	molekula
škrobu	škrob	k1gInSc2	škrob
po	po	k7c6	po
opětovném	opětovný	k2eAgNnSc6d1	opětovné
smíchání	smíchání	k1gNnSc6	smíchání
s	s	k7c7	s
chladnější	chladný	k2eAgFnSc7d2	chladnější
částí	část	k1gFnSc7	část
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
Postupným	postupný	k2eAgNnSc7d1	postupné
zvyšováním	zvyšování	k1gNnSc7	zvyšování
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
až	až	k9	až
maximální	maximální	k2eAgFnPc4d1	maximální
teploty	teplota	k1gFnPc4	teplota
okolo	okolo	k7c2	okolo
76	[number]	k4	76
°	°	k?	°
<g/>
C.	C.	kA	C.
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rmutování	rmutování	k1gNnSc6	rmutování
(	(	kIx(	(
<g/>
klasickém	klasický	k2eAgInSc6d1	klasický
i	i	k8xC	i
dekokčním	dekokční	k2eAgInSc6d1	dekokční
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
prodlevy	prodleva	k1gFnPc1	prodleva
při	při	k7c6	při
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
teplotách	teplota	k1gFnPc6	teplota
(	(	kIx(	(
<g/>
cukrotvorná	cukrotvorný	k2eAgFnSc1d1	cukrotvorná
<g/>
,	,	kIx,	,
bílkovinoštěpná	bílkovinoštěpný	k2eAgFnSc1d1	bílkovinoštěpný
atd	atd	kA	atd
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
enzymatickým	enzymatický	k2eAgInPc3d1	enzymatický
procesům	proces	k1gInPc3	proces
-	-	kIx~	-
např.	např.	kA	např.
enzymů	enzym	k1gInPc2	enzym
alfa	alfa	k1gNnSc1	alfa
a	a	k8xC	a
beta	beta	k1gNnSc1	beta
amyláza	amyláza	k1gFnSc1	amyláza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
obou	dva	k4xCgFnPc2	dva
metod	metoda	k1gFnPc2	metoda
je	být	k5eAaImIp3nS	být
rmut	rmut	k1gInSc1	rmut
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
přečerpán	přečerpán	k2eAgInSc1d1	přečerpán
do	do	k7c2	do
tak	tak	k6eAd1	tak
zvané	zvaný	k2eAgFnSc2d1	zvaná
"	"	kIx"	"
<g/>
scezovací	scezovací	k2eAgFnSc2d1	scezovací
kádě	káď	k1gFnSc2	káď
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
zcezování	zcezování	k1gNnSc2	zcezování
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
hrubé	hrubý	k2eAgFnPc1d1	hrubá
částice	částice	k1gFnPc1	částice
-	-	kIx~	-
většinou	většinou	k6eAd1	většinou
obaly	obal	k1gInPc4	obal
zrn	zrno	k1gNnPc2	zrno
tzv.	tzv.	kA	tzv.
pluchy	plucha	k1gFnSc2	plucha
filtrační	filtrační	k2eAgFnSc4d1	filtrační
vrstvu	vrstva	k1gFnSc4	vrstva
zvanou	zvaný	k2eAgFnSc4d1	zvaná
mláto	mláto	k1gNnSc4	mláto
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
opouští	opouštět	k5eAaImIp3nS	opouštět
čirá	čirý	k2eAgFnSc1d1	čirá
sladina	sladina	k1gFnSc1	sladina
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
využitelné	využitelný	k2eAgInPc1d1	využitelný
podíly	podíl	k1gInPc1	podíl
zachycené	zachycený	k2eAgInPc1d1	zachycený
v	v	k7c6	v
mlátě	mláto	k1gNnSc6	mláto
se	se	k3xPyFc4	se
vymývají	vymývat	k5eAaImIp3nP	vymývat
skrápěním	skrápění	k1gNnSc7	skrápění
či	či	k8xC	či
promýváním	promývání	k1gNnSc7	promývání
vrstvy	vrstva	k1gFnSc2	vrstva
mláta	mláto	k1gNnSc2	mláto
teplou	teplý	k2eAgFnSc7d1	teplá
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
sladká	sladký	k2eAgFnSc1d1	sladká
tekutina	tekutina	k1gFnSc1	tekutina
zvaná	zvaný	k2eAgFnSc1d1	zvaná
sladina	sladina	k1gFnSc1	sladina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
filtrováním	filtrování	k1gNnSc7	filtrování
(	(	kIx(	(
<g/>
scezováním	scezování	k1gNnSc7	scezování
<g/>
)	)	kIx)	)
na	na	k7c6	na
nebo	nebo	k8xC	nebo
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
zařízení	zařízení	k1gNnSc6	zařízení
zvaném	zvaný	k2eAgNnSc6d1	zvané
"	"	kIx"	"
<g/>
sladinový	sladinový	k2eAgInSc4d1	sladinový
filtr	filtr	k1gInSc4	filtr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chmelovar	chmelovar	k1gInSc1	chmelovar
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
sladina	sladina	k1gFnSc1	sladina
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
varu	var	k1gInSc2	var
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
piva	pivo	k1gNnSc2	pivo
dodávána	dodáván	k2eAgFnSc1d1	dodávána
důležitá	důležitý	k2eAgFnSc1d1	důležitá
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
chuti	chuť	k1gFnSc2	chuť
-	-	kIx~	-
chmel	chmel	k1gInSc1	chmel
<g/>
.	.	kIx.	.
</s>
<s>
Chmel	Chmel	k1gMnSc1	Chmel
se	se	k3xPyFc4	se
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
preferencích	preference	k1gFnPc6	preference
sládka	sládek	k1gMnSc4	sládek
může	moct	k5eAaImIp3nS	moct
přidávat	přidávat	k5eAaImF	přidávat
jak	jak	k6eAd1	jak
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
formě	forma	k1gFnSc6	forma
šišek	šiška	k1gFnPc2	šiška
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
lisovaný	lisovaný	k2eAgMnSc1d1	lisovaný
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tekutého	tekutý	k2eAgInSc2d1	tekutý
extraktu	extrakt	k1gInSc2	extrakt
<g/>
.	.	kIx.	.
</s>
<s>
Chmel	chmel	k1gInSc1	chmel
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
vařeného	vařený	k2eAgNnSc2d1	vařené
piva	pivo	k1gNnSc2	pivo
přidávat	přidávat	k5eAaImF	přidávat
vícekrát	vícekrát	k6eAd1	vícekrát
než	než	k8xS	než
jednou	jeden	k4xCgFnSc7	jeden
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
obecné	obecný	k2eAgNnSc1d1	obecné
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
dříve	dříve	k6eAd2	dříve
je	být	k5eAaImIp3nS	být
chmel	chmel	k1gInSc1	chmel
do	do	k7c2	do
piva	pivo	k1gNnSc2	pivo
přidán	přidat	k5eAaPmNgMnS	přidat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
hořčí	hořč	k1gFnPc2	hořč
chuť	chuť	k1gFnSc4	chuť
pivo	pivo	k1gNnSc4	pivo
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
<g/>
.	.	kIx.	.
</s>
<s>
Smícháním	smíchání	k1gNnSc7	smíchání
roztoku	roztok	k1gInSc2	roztok
s	s	k7c7	s
chmelem	chmel	k1gInSc7	chmel
vzniká	vznikat	k5eAaImIp3nS	vznikat
mladina	mladina	k1gFnSc1	mladina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
chmelovaru	chmelovar	k1gInSc6	chmelovar
vlivem	vlivem	k7c2	vlivem
hořkých	hořký	k2eAgFnPc2d1	hořká
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
pryskyřic	pryskyřice	k1gFnPc2	pryskyřice
a	a	k8xC	a
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
)	)	kIx)	)
k	k	k7c3	k
vysrážení	vysrážení	k1gNnSc3	vysrážení
tzv.	tzv.	kA	tzv.
klků	klk	k1gInPc2	klk
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
chuchvalců	chuchvalec	k1gInPc2	chuchvalec
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
chmelových	chmelový	k2eAgInPc2d1	chmelový
zbytků	zbytek	k1gInPc2	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Mladina	mladina	k1gFnSc1	mladina
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
přečerpává	přečerpávat	k5eAaImIp3nS	přečerpávat
do	do	k7c2	do
odkalovacího	odkalovací	k2eAgNnSc2d1	odkalovací
zařízení	zařízení	k1gNnSc2	zařízení
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
tzv.	tzv.	kA	tzv.
štoky	štok	k1gInPc1	štok
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
například	například	k6eAd1	například
vířivá	vířivý	k2eAgFnSc1d1	vířivá
káď	káď	k1gFnSc1	káď
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
chmelových	chmelový	k2eAgInPc2d1	chmelový
zbytků	zbytek	k1gInPc2	zbytek
a	a	k8xC	a
klků	klk	k1gInPc2	klk
před	před	k7c7	před
zchlazením	zchlazení	k1gNnSc7	zchlazení
a	a	k8xC	a
kvašením	kvašení	k1gNnSc7	kvašení
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
obecných	obecný	k2eAgFnPc2d1	obecná
náležitostí	náležitost	k1gFnPc2	náležitost
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
piva	pivo	k1gNnSc2	pivo
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
324	[number]	k4	324
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
335	[number]	k4	335
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
pivo	pivo	k1gNnSc4	pivo
označit	označit	k5eAaPmF	označit
<g/>
:	:	kIx,	:
názvem	název	k1gInSc7	název
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pivo	pivo	k1gNnSc1	pivo
ležák	ležák	k1gInSc1	ležák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahem	obsah	k1gInSc7	obsah
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
světlé	světlý	k2eAgNnSc4d1	světlé
či	či	k8xC	či
tmavé	tmavý	k2eAgNnSc4d1	tmavé
pivo	pivo	k1gNnSc4	pivo
a	a	k8xC	a
některými	některý	k3yIgInPc7	některý
dalšími	další	k2eAgInPc7d1	další
údaji	údaj	k1gInPc7	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
extraktu	extrakt	k1gInSc6	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
není	být	k5eNaImIp3nS	být
nutno	nutno	k6eAd1	nutno
povinně	povinně	k6eAd1	povinně
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnSc3d1	současná
vyhlášce	vyhláška	k1gFnSc3	vyhláška
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
tradiční	tradiční	k2eAgNnSc1d1	tradiční
značení	značení	k1gNnSc1	značení
piv	pivo	k1gNnPc2	pivo
stupni	stupeň	k1gInSc3	stupeň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
12	[number]	k4	12
<g/>
stupňové	stupňový	k2eAgNnSc4d1	stupňové
pivo	pivo	k1gNnSc4	pivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takové	takový	k3xDgNnSc1	takový
značení	značení	k1gNnSc1	značení
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c4	v
ČR	ČR	kA	ČR
nezákonné	zákonný	k2eNgInPc1d1	nezákonný
/	/	kIx~	/
<g/>
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
není	být	k5eNaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
stupňovitostí	stupňovitost	k1gFnPc2	stupňovitost
bylo	být	k5eAaImAgNnS	být
opuštěno	opustit	k5eAaPmNgNnS	opustit
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
stupňů	stupeň	k1gInPc2	stupeň
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
uvádět	uvádět	k5eAaImF	uvádět
tzv.	tzv.	kA	tzv.
extrakt	extrakt	k1gInSc4	extrakt
původní	původní	k2eAgFnSc2d1	původní
mladiny	mladina	k1gFnSc2	mladina
(	(	kIx(	(
<g/>
EPM	EPM	kA	EPM
<g/>
)	)	kIx)	)
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
hmotnostních	hmotnostní	k2eAgNnPc6d1	hmotnostní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
12	[number]	k4	12
<g/>
%	%	kIx~	%
pivo	pivo	k1gNnSc4	pivo
<g/>
;	;	kIx,	;
pozor	pozor	k1gInSc4	pozor
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
procenta	procento	k1gNnPc1	procento
si	se	k3xPyFc3	se
nelze	lze	k6eNd1	lze
plést	plést	k5eAaImF	plést
s	s	k7c7	s
procenty	procent	k1gInPc7	procent
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
běžných	běžný	k2eAgNnPc2d1	běžné
piv	pivo	k1gNnPc2	pivo
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
výrobce	výrobce	k1gMnSc4	výrobce
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
zaručena	zaručen	k2eAgFnSc1d1	zaručena
přesnost	přesnost	k1gFnSc1	přesnost
EPM	EPM	kA	EPM
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
tak	tak	k9	tak
výrobce	výrobce	k1gMnSc1	výrobce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
daňových	daňový	k2eAgInPc2d1	daňový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postačí	postačit	k5eAaPmIp3nS	postačit
uvedení	uvedení	k1gNnSc1	uvedení
druhu	druh	k1gInSc2	druh
piva	pivo	k1gNnSc2	pivo
podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	vysoce	k6eAd2	vysoce
druhy	druh	k1gInPc4	druh
piva	pivo	k1gNnSc2	pivo
podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
EPM	EPM	kA	EPM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
druhu	druh	k1gInSc2	druh
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
vždy	vždy	k6eAd1	vždy
(	(	kIx(	(
<g/>
i	i	k9	i
při	při	k7c6	při
uvedení	uvedení	k1gNnSc6	uvedení
EPM	EPM	kA	EPM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
značení	značení	k1gNnSc2	značení
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
)	)	kIx)	)
nesprávné	správný	k2eNgNnSc1d1	nesprávné
značení	značení	k1gNnSc1	značení
<g/>
:	:	kIx,	:
pivo	pivo	k1gNnSc1	pivo
10	[number]	k4	10
<g/>
stupňové	stupňový	k2eAgInPc1d1	stupňový
<g/>
,	,	kIx,	,
pivo	pivo	k1gNnSc1	pivo
12	[number]	k4	12
<g/>
stupňové	stupňový	k2eAgFnSc6d1	stupňová
<g/>
,	,	kIx,	,
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
<g/>
,	,	kIx,	,
silné	silný	k2eAgFnPc1d1	silná
14	[number]	k4	14
<g/>
stupňové	stupňový	k2eAgFnPc1d1	stupňová
pivo	pivo	k1gNnSc4	pivo
<g/>
;	;	kIx,	;
b	b	k?	b
<g/>
)	)	kIx)	)
správné	správný	k2eAgNnSc1d1	správné
značení	značení	k1gNnSc1	značení
piva	pivo	k1gNnSc2	pivo
<g/>
:	:	kIx,	:
pivo	pivo	k1gNnSc1	pivo
výčepní	výčepní	k1gFnSc1	výčepní
<g/>
,	,	kIx,	,
pivo	pivo	k1gNnSc1	pivo
ležák	ležák	k1gInSc1	ležák
<g/>
,	,	kIx,	,
speciál	speciál	k1gInSc1	speciál
14	[number]	k4	14
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
je	být	k5eAaImIp3nS	být
celosvětově	celosvětově	k6eAd1	celosvětově
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
nápoj	nápoj	k1gInSc4	nápoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Holandsko	Holandsko	k1gNnSc1	Holandsko
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
celosvětovým	celosvětový	k2eAgMnSc7d1	celosvětový
producentem	producent	k1gMnSc7	producent
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
470	[number]	k4	470
milionu	milion	k4xCgInSc2	milion
hektolitrů	hektolitr	k1gInPc2	hektolitr
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
21,1	[number]	k4	21,1
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
USA	USA	kA	USA
s	s	k7c7	s
232,8	[number]	k4	232,8
miliony	milion	k4xCgInPc7	milion
hektolitry	hektolitr	k1gInPc7	hektolitr
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgInPc1d1	následovaný
Ruskem	Rusko	k1gNnSc7	Rusko
s	s	k7c7	s
109,8	[number]	k4	109,8
miliony	milion	k4xCgInPc7	milion
hektolitry	hektolitr	k1gInPc7	hektolitr
<g/>
.	.	kIx.	.
</s>
<s>
Nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
pivem	pivo	k1gNnSc7	pivo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
holandské	holandský	k2eAgNnSc4d1	holandské
pivo	pivo	k1gNnSc4	pivo
Heineken	Heinekna	k1gFnPc2	Heinekna
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
také	také	k9	také
na	na	k7c4	na
území	území	k1gNnSc4	území
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
často	často	k6eAd1	často
o	o	k7c4	o
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
převládá	převládat	k5eAaImIp3nS	převládat
protestantství	protestantství	k1gNnSc1	protestantství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nepovažuje	považovat	k5eNaImIp3nS	považovat
víno	víno	k1gNnSc1	víno
za	za	k7c4	za
krev	krev	k1gFnSc4	krev
Krista	Kristus	k1gMnSc2	Kristus
pomocí	pomocí	k7c2	pomocí
transsubstanciace	transsubstanciace	k1gFnSc2	transsubstanciace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
víno	víno	k1gNnSc1	víno
nemá	mít	k5eNaImIp3nS	mít
podporu	podpora	k1gFnSc4	podpora
(	(	kIx(	(
<g/>
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
převažovala	převažovat	k5eAaImAgFnS	převažovat
produkce	produkce	k1gFnSc1	produkce
vína	vína	k1gFnSc1	vína
mnichy	mnich	k1gInPc1	mnich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českých	český	k2eAgInPc2d1	český
předpisů	předpis	k1gInPc2	předpis
se	se	k3xPyFc4	se
pivem	pivo	k1gNnSc7	pivo
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
:	:	kIx,	:
pěnivý	pěnivý	k2eAgInSc4d1	pěnivý
nápoj	nápoj	k1gInSc4	nápoj
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
zkvašením	zkvašení	k1gNnSc7	zkvašení
mladiny	mladina	k1gFnSc2	mladina
připravené	připravený	k2eAgFnSc2d1	připravená
ze	z	k7c2	z
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
neupraveného	upravený	k2eNgInSc2d1	neupravený
chmele	chmel	k1gInSc2	chmel
<g/>
,	,	kIx,	,
upraveného	upravený	k2eAgInSc2d1	upravený
chmele	chmel	k1gInSc2	chmel
nebo	nebo	k8xC	nebo
chmelových	chmelový	k2eAgInPc2d1	chmelový
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vedle	vedle	k6eAd1	vedle
kvasným	kvasný	k2eAgInSc7d1	kvasný
procesem	proces	k1gInSc7	proces
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
alkoholu	alkohol	k1gInSc2	alkohol
(	(	kIx(	(
<g/>
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
)	)	kIx)	)
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
určité	určitý	k2eAgNnSc1d1	určité
množství	množství	k1gNnSc1	množství
neprokvašeného	prokvašený	k2eNgInSc2d1	prokvašený
extraktu	extrakt	k1gInSc2	extrakt
(	(	kIx(	(
<g/>
§	§	k?	§
11	[number]	k4	11
písm	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
a	a	k8xC	a
<g/>
/	/	kIx~	/
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
335	[number]	k4	335
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
pivovarů	pivovar	k1gInPc2	pivovar
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
daňových	daňový	k2eAgInPc2d1	daňový
zákonů	zákon	k1gInPc2	zákon
se	se	k3xPyFc4	se
pivem	pivo	k1gNnSc7	pivo
zabývá	zabývat	k5eAaImIp3nS	zabývat
především	především	k9	především
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
353	[number]	k4	353
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
spotřebních	spotřební	k2eAgFnPc6d1	spotřební
daních	daň	k1gFnPc6	daň
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
upravuje	upravovat	k5eAaImIp3nS	upravovat
v	v	k7c6	v
§	§	k?	§
80	[number]	k4	80
a	a	k8xC	a
násl	násnout	k5eAaPmAgMnS	násnout
<g/>
.	.	kIx.	.
spotřební	spotřební	k2eAgFnSc4d1	spotřební
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Daň	daň	k1gFnSc1	daň
není	být	k5eNaImIp3nS	být
povinna	povinen	k2eAgFnSc1d1	povinna
odvádět	odvádět	k5eAaImF	odvádět
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
vaří	vařit	k5eAaImIp3nP	vařit
pivo	pivo	k1gNnSc4	pivo
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
potřebu	potřeba	k1gFnSc4	potřeba
do	do	k7c2	do
200	[number]	k4	200
litrů	litr	k1gInPc2	litr
za	za	k7c4	za
rok	rok	k1gInSc4	rok
a	a	k8xC	a
pivo	pivo	k1gNnSc1	pivo
neprodává	prodávat	k5eNaImIp3nS	prodávat
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
oznámí	oznámit	k5eAaPmIp3nS	oznámit
celnímu	celní	k2eAgInSc3d1	celní
úřadu	úřad	k1gInSc3	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
daňové	daňový	k2eAgFnPc1d1	daňová
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
pro	pro	k7c4	pro
malé	malý	k2eAgInPc4d1	malý
nezávislé	závislý	k2eNgInPc4d1	nezávislý
pivovary	pivovar	k1gInPc4	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
daně	daň	k1gFnSc2	daň
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
piva	pivo	k1gNnSc2	pivo
v	v	k7c6	v
hektolitrech	hektolitr	k1gInPc6	hektolitr
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
daňová	daňový	k2eAgFnSc1d1	daňová
sazba	sazba	k1gFnSc1	sazba
za	za	k7c4	za
1	[number]	k4	1
hektolitr	hektolitr	k1gInSc4	hektolitr
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
procento	procento	k1gNnSc4	procento
EPM	EPM	kA	EPM
je	být	k5eAaImIp3nS	být
24	[number]	k4	24
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prodeji	prodej	k1gInSc6	prodej
piva	pivo	k1gNnSc2	pivo
se	se	k3xPyFc4	se
také	také	k9	také
vedle	vedle	k7c2	vedle
spotřební	spotřební	k2eAgFnSc2d1	spotřební
daně	daň	k1gFnSc2	daň
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
přidané	přidaný	k2eAgFnSc2d1	přidaná
hodnoty	hodnota	k1gFnSc2	hodnota
(	(	kIx(	(
<g/>
DPH	DPH	kA	DPH
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
235	[number]	k4	235
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozdílem	rozdíl	k1gInSc7	rozdíl
české	český	k2eAgFnSc2d1	Česká
spotřební	spotřební	k2eAgFnSc2d1	spotřební
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
z	z	k7c2	z
vína	víno	k1gNnSc2	víno
(	(	kIx(	(
<g/>
vína	víno	k1gNnSc2	víno
mají	mít	k5eAaImIp3nP	mít
nulovou	nulový	k2eAgFnSc4d1	nulová
spotřební	spotřební	k2eAgFnSc4d1	spotřební
daň	daň	k1gFnSc4	daň
<g/>
)	)	kIx)	)
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
malých	malý	k2eAgInPc2d1	malý
nezávislých	závislý	k2eNgInPc2d1	nezávislý
pivovarů	pivovar	k1gInPc2	pivovar
prodávat	prodávat	k5eAaImF	prodávat
pivo	pivo	k1gNnSc4	pivo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
sladové	sladový	k2eAgNnSc4d1	sladové
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
piva	pivo	k1gNnSc2	pivo
zažila	zažít	k5eAaPmAgFnS	zažít
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
značný	značný	k2eAgInSc4d1	značný
nárůst	nárůst	k1gInSc4	nárůst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
komunismu	komunismus	k1gInSc2	komunismus
byla	být	k5eAaImAgFnS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
cena	cena	k1gFnSc1	cena
piva	pivo	k1gNnSc2	pivo
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
téměř	téměř	k6eAd1	téměř
neměnná	neměnný	k2eAgFnSc1d1	neměnná
až	až	k9	až
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
zdražení	zdražení	k1gNnPc4	zdražení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
10	[number]	k4	10
<g/>
°	°	k?	°
lahvového	lahvový	k2eAgNnSc2d1	lahvové
piva	pivo	k1gNnSc2	pivo
zvedla	zvednout	k5eAaPmAgFnS	zvednout
z	z	k7c2	z
1,4	[number]	k4	1,4
Kčs	Kčs	kA	Kčs
na	na	k7c4	na
1,7	[number]	k4	1,7
Kčs	Kčs	kA	Kčs
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhé	druhý	k4xOgFnPc1	druhý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cena	cena	k1gFnSc1	cena
narostla	narůst	k5eAaPmAgFnS	narůst
z	z	k7c2	z
1,7	[number]	k4	1,7
Kčs	Kčs	kA	Kčs
na	na	k7c4	na
2,5	[number]	k4	2,5
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
nárůst	nárůst	k1gInSc1	nárůst
ceny	cena	k1gFnSc2	cena
začal	začít	k5eAaPmAgInS	začít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
a	a	k8xC	a
trval	trvat	k5eAaImAgInS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
8,59	[number]	k4	8,59
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
mírnému	mírný	k2eAgInSc3d1	mírný
poklesu	pokles	k1gInSc3	pokles
ceny	cena	k1gFnSc2	cena
až	až	k9	až
na	na	k7c4	na
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
hodnotu	hodnota	k1gFnSc4	hodnota
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
stálo	stát	k5eAaImAgNnS	stát
pivo	pivo	k1gNnSc1	pivo
8,4	[number]	k4	8,4
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
spotřeba	spotřeba	k1gFnSc1	spotřeba
piva	pivo	k1gNnSc2	pivo
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Česko	Česko	k1gNnSc1	Česko
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
vypitého	vypitý	k2eAgNnSc2d1	vypité
piva	pivo	k1gNnSc2	pivo
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
všechny	všechen	k3xTgMnPc4	všechen
obyvatele	obyvatel	k1gMnPc4	obyvatel
Česka	Česko	k1gNnPc4	Česko
včetně	včetně	k7c2	včetně
kojenců	kojenec	k1gMnPc2	kojenec
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Prvenství	prvenství	k1gNnSc1	prvenství
však	však	k9	však
před	před	k7c7	před
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
držela	držet	k5eAaImAgFnS	držet
Belgie	Belgie	k1gFnSc1	Belgie
(	(	kIx(	(
<g/>
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
velkých	velký	k2eAgFnPc2d1	velká
daní	daň	k1gFnPc2	daň
na	na	k7c4	na
víno	víno	k1gNnSc4	víno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
tam	tam	k6eAd1	tam
byla	být	k5eAaImAgFnS	být
spotřeba	spotřeba	k1gFnSc1	spotřeba
na	na	k7c4	na
1	[number]	k4	1
obyvatele	obyvatel	k1gMnSc4	obyvatel
228	[number]	k4	228
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
však	však	k9	však
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
neustále	neustále	k6eAd1	neustále
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
ještě	ještě	k9	ještě
vedla	vést	k5eAaImAgFnS	vést
NSR	NSR	kA	NSR
se	s	k7c7	s
145	[number]	k4	145
litry	litr	k1gInPc7	litr
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
každý	každý	k3xTgMnSc1	každý
Čech	Čech	k1gMnSc1	Čech
vypije	vypít	k5eAaPmIp3nS	vypít
dle	dle	k7c2	dle
statistik	statistika	k1gFnPc2	statistika
přibližně	přibližně	k6eAd1	přibližně
okolo	okolo	k7c2	okolo
160	[number]	k4	160
litrů	litr	k1gInPc2	litr
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
lokálního	lokální	k2eAgNnSc2d1	lokální
maxima	maximum	k1gNnSc2	maximum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
připadala	připadat	k5eAaImAgFnS	připadat
na	na	k7c4	na
každého	každý	k3xTgMnSc4	každý
obyvatele	obyvatel	k1gMnSc4	obyvatel
163,5	[number]	k4	163,5
litru	litr	k1gInSc6	litr
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
následoval	následovat	k5eAaImAgInS	následovat
propad	propad	k1gInSc1	propad
na	na	k7c4	na
159,1	[number]	k4	159,1
litru	litr	k1gInSc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
%	%	kIx~	%
se	se	k3xPyFc4	se
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
spotřebě	spotřeba	k1gFnSc6	spotřeba
piva	pivo	k1gNnSc2	pivo
podílejí	podílet	k5eAaImIp3nP	podílet
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
turisté	turist	k1gMnPc1	turist
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
si	se	k3xPyFc3	se
tak	tak	k9	tak
udržuje	udržovat	k5eAaImIp3nS	udržovat
přední	přední	k2eAgFnSc4d1	přední
pozici	pozice	k1gFnSc4	pozice
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
spotřebě	spotřeba	k1gFnSc6	spotřeba
piva	pivo	k1gNnSc2	pivo
se	se	k3xPyFc4	se
nově	nově	k6eAd1	nově
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Irové	Ir	k1gMnPc1	Ir
se	s	k7c7	s
130	[number]	k4	130
litry	litr	k1gInPc7	litr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tak	tak	k6eAd1	tak
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
odsunuli	odsunout	k5eAaPmAgMnP	odsunout
Němce	Němec	k1gMnSc4	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vypili	vypít	k5eAaPmAgMnP	vypít
111,7	[number]	k4	111,7
litru	litr	k1gInSc2	litr
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
trh	trh	k1gInSc1	trh
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ale	ale	k8xC	ale
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
útlumu	útlum	k1gInSc6	útlum
a	a	k8xC	a
množství	množství	k1gNnSc6	množství
vypitého	vypitý	k2eAgNnSc2d1	vypité
piva	pivo	k1gNnSc2	pivo
zde	zde	k6eAd1	zde
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
piva	pivo	k1gNnSc2	pivo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
s	s	k7c7	s
ročním	roční	k2eAgInSc7d1	roční
výstavem	výstav	k1gInSc7	výstav
okolo	okolo	k7c2	okolo
19,897	[number]	k4	19,897
milionu	milion	k4xCgInSc2	milion
hektolitrů	hektolitr	k1gInPc2	hektolitr
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
devátým	devátý	k4xOgMnSc7	devátý
největším	veliký	k2eAgMnSc7d3	veliký
celosvětovým	celosvětový	k2eAgMnSc7d1	celosvětový
vývozcem	vývozce	k1gMnSc7	vývozce
piva	pivo	k1gNnSc2	pivo
s	s	k7c7	s
neustávajícím	ustávající	k2eNgInSc7d1	neustávající
růstem	růst	k1gInSc7	růst
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
Prazdroj	prazdroj	k1gInSc1	prazdroj
(	(	kIx(	(
<g/>
Gambrinus	Gambrinus	k1gMnSc1	Gambrinus
<g/>
,	,	kIx,	,
Radegast	Radegast	k1gMnSc1	Radegast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
SABMiller	SABMiller	k1gInSc4	SABMiller
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Staropramen	staropramen	k1gInSc1	staropramen
(	(	kIx(	(
<g/>
Molson	Molson	k1gInSc1	Molson
Coors	Coors	k1gInSc1	Coors
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Heineken	Heineken	k2eAgMnSc1d1	Heineken
a	a	k8xC	a
pak	pak	k6eAd1	pak
teprve	teprve	k6eAd1	teprve
české	český	k2eAgInPc1d1	český
pivovary	pivovar	k1gInPc1	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
piva	pivo	k1gNnSc2	pivo
na	na	k7c4	na
pokožku	pokožka	k1gFnSc4	pokožka
Jeden	jeden	k4xCgInSc4	jeden
pivovar	pivovar	k1gInSc4	pivovar
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
pivo	pivo	k1gNnSc1	pivo
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
kožních	kožní	k2eAgInPc2d1	kožní
problémů	problém	k1gInPc2	problém
(	(	kIx(	(
<g/>
pití	pití	k1gNnSc2	pití
i	i	k9	i
kouple	kouple	k6eAd1	kouple
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nezodpovědné	zodpovědný	k2eNgNnSc1d1	nezodpovědné
<g/>
.	.	kIx.	.
</s>
<s>
Alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc1	nápoj
působí	působit	k5eAaImIp3nP	působit
a	a	k8xC	a
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
řadu	řada	k1gFnSc4	řada
kožních	kožní	k2eAgFnPc2d1	kožní
nemocí	nemoc	k1gFnPc2	nemoc
včetně	včetně	k7c2	včetně
lupénky	lupénka	k1gFnSc2	lupénka
a	a	k8xC	a
melanomu	melanom	k1gInSc2	melanom
(	(	kIx(	(
<g/>
Rota	rota	k1gFnSc1	rota
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pití	pití	k1gNnSc1	pití
piva	pivo	k1gNnSc2	pivo
při	při	k7c6	při
opalování	opalování	k1gNnSc6	opalování
je	být	k5eAaImIp3nS	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
nastává	nastávat	k5eAaImIp3nS	nastávat
posílen	posílit	k5eAaPmNgInS	posílit
dvou	dva	k4xCgInPc2	dva
rizikových	rizikový	k2eAgInPc2d1	rizikový
faktorů	faktor	k1gInPc2	faktor
(	(	kIx(	(
<g/>
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
plus	plus	k6eAd1	plus
alkohol	alkohol	k1gInSc1	alkohol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Nešpor	nešpor	k1gInSc1	nešpor
K.	K.	kA	K.
Alkohol	alkohol	k1gInSc1	alkohol
a	a	k8xC	a
kožní	kožní	k2eAgFnSc2d1	kožní
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
<g/>
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
slovenská	slovenský	k2eAgFnSc1d1	slovenská
psychiatrie	psychiatrie	k1gFnSc1	psychiatrie
2006	[number]	k4	2006
<g/>
;	;	kIx,	;
102	[number]	k4	102
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
389	[number]	k4	389
<g/>
.	.	kIx.	.
</s>
<s>
Alergeny	alergen	k1gInPc1	alergen
v	v	k7c6	v
pivu	pivo	k1gNnSc6	pivo
K	k	k7c3	k
alergenům	alergen	k1gInPc3	alergen
patří	patřit	k5eAaImIp3nS	patřit
mykotoxiny	mykotoxin	k1gInPc4	mykotoxin
z	z	k7c2	z
plesnivého	plesnivý	k2eAgInSc2d1	plesnivý
sladu	slad	k1gInSc2	slad
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
třetinách	třetina	k1gFnPc6	třetina
českých	český	k2eAgNnPc2d1	české
piv	pivo	k1gNnPc2	pivo
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
časopis	časopis	k1gInSc1	časopis
D-test	Dest	k1gFnSc1	D-test
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gluten	gluten	k1gInSc1	gluten
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
piva	pivo	k1gNnSc2	pivo
přidává	přidávat	k5eAaImIp3nS	přidávat
jako	jako	k8xC	jako
a	a	k8xC	a
konzervační	konzervační	k2eAgFnSc1d1	konzervační
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
