<s>
Havárie	havárie	k1gFnSc1	havárie
japonské	japonský	k2eAgFnSc2d1	japonská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
I	i	k8xC	i
společnosti	společnost	k1gFnSc2	společnost
TEPCO	TEPCO	kA	TEPCO
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
jaderná	jaderný	k2eAgFnSc1d1	jaderná
havárie	havárie	k1gFnSc1	havárie
od	od	k7c2	od
Černobylu	Černobyl	k1gInSc2	Černobyl
1986	[number]	k4	1986
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
jediná	jediný	k2eAgFnSc1d1	jediná
další	další	k2eAgFnSc1d1	další
havárie	havárie	k1gFnSc1	havárie
označená	označený	k2eAgFnSc1d1	označená
na	na	k7c6	na
stupnici	stupnice	k1gFnSc6	stupnice
INES	INES	kA	INES
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
stupněm	stupeň	k1gInSc7	stupeň
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
