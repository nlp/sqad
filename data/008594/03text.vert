<p>
<s>
UnixWare	UnixWar	k1gMnSc5	UnixWar
je	on	k3xPp3gMnPc4	on
Unixový	unixový	k2eAgInSc1d1	unixový
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
UNIVEL	UNIVEL	kA	UNIVEL
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgInSc1d1	schopný
současně	současně	k6eAd1	současně
využívat	využívat	k5eAaPmF	využívat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
dvou	dva	k4xCgInPc2	dva
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Unix	Unix	k1gInSc4	Unix
a	a	k8xC	a
NetWare	NetWar	k1gMnSc5	NetWar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
UnixWare	UnixWar	k1gMnSc5	UnixWar
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
založený	založený	k2eAgInSc1d1	založený
na	na	k7c4	na
UNIX	Unix	k1gInSc4	Unix
System	Syst	k1gInSc7	Syst
V	V	kA	V
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
využíván	využívat	k5eAaPmNgInS	využívat
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
systém	systém	k1gInSc1	systém
odvozený	odvozený	k2eAgInSc1d1	odvozený
od	od	k7c2	od
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zase	zase	k9	zase
častěji	často	k6eAd2	často
využíván	využívat	k5eAaPmNgInS	využívat
v	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
verzí	verze	k1gFnSc7	verze
UNIX	Unix	k1gInSc4	Unix
System	Syst	k1gInSc7	Syst
V	V	kA	V
a	a	k8xC	a
BSD	BSD	kA	BSD
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
před	před	k7c7	před
několika	několik	k4yIc7	několik
lety	léto	k1gNnPc7	léto
verze	verze	k1gFnSc1	verze
s	s	k7c7	s
názvem	název	k1gInSc7	název
System	Systo	k1gNnSc7	Systo
V	V	kA	V
verze	verze	k1gFnSc2	verze
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Univel	Univlo	k1gNnPc2	Univlo
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
SVR4	SVR4	k1gFnSc6	SVR4
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
sloučení	sloučení	k1gNnSc4	sloučení
systémů	systém	k1gInPc2	systém
SunOS	SunOS	k1gFnSc2	SunOS
a	a	k8xC	a
System	Syst	k1gInSc7	Syst
V.	V.	kA	V.
Unix	Unix	k1gInSc4	Unix
System	Syst	k1gInSc7	Syst
laboratoře	laboratoř	k1gFnSc2	laboratoř
(	(	kIx(	(
<g/>
USL	USL	kA	USL
<g/>
)	)	kIx)	)
firmy	firma	k1gFnSc2	firma
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Novell	Novell	kA	Novell
partnerství	partnerství	k1gNnSc4	partnerství
Univel	Univel	k1gInSc1	Univel
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
rozvoje	rozvoj	k1gInSc2	rozvoj
PC	PC	kA	PC
verze	verze	k1gFnSc1	verze
Unixu	Unix	k1gInSc2	Unix
s	s	k7c7	s
kódovým	kódový	k2eAgNnSc7d1	kódové
označením	označení	k1gNnSc7	označení
Destiny	Destina	k1gFnSc2	Destina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Destiny	Destina	k1gFnSc2	Destina
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
Unix	Unix	k1gInSc4	Unix
System	Systo	k1gNnSc7	Systo
V	v	k7c6	v
verze	verze	k1gFnSc1	verze
4.2	[number]	k4	4.2
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
grafické	grafický	k2eAgNnSc4d1	grafické
rozhraní	rozhraní	k1gNnSc4	rozhraní
byl	být	k5eAaImAgMnS	být
použit	použít	k5eAaPmNgMnS	použít
toolkit	toolkit	k1gMnSc1	toolkit
MoOLIT	MoOLIT	k1gMnSc1	MoOLIT
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
volit	volit	k5eAaImF	volit
mezi	mezi	k7c7	mezi
vzhledem	vzhled	k1gInSc7	vzhled
a	a	k8xC	a
chováním	chování	k1gNnSc7	chování
typu	typ	k1gInSc2	typ
OPEN	OPEN	kA	OPEN
LOOK	LOOK	kA	LOOK
nebo	nebo	k8xC	nebo
Motif	Motif	kA	Motif
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
systém	systém	k1gInSc1	systém
stal	stát	k5eAaPmAgInS	stát
na	na	k7c6	na
PC	PC	kA	PC
stabilnějším	stabilní	k2eAgFnPc3d2	stabilnější
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
UFS	UFS	kA	UFS
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
SVR4	SVR4	k1gFnSc6	SVR4
nahrazen	nahradit	k5eAaPmNgInS	nahradit
žurnálovacím	žurnálovací	k2eAgInSc7d1	žurnálovací
systémem	systém	k1gInSc7	systém
souborů	soubor	k1gInPc2	soubor
Veritas	Veritasa	k1gFnPc2	Veritasa
VXFS	VXFS	kA	VXFS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Destiny	Destina	k1gFnPc1	Destina
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
jako	jako	k8xC	jako
UnixWare	UnixWar	k1gMnSc5	UnixWar
1.0	[number]	k4	1.0
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
–	–	k?	–
Personal	Personal	k1gFnSc2	Personal
edition	edition	k1gInSc1	edition
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
Novell	Novell	kA	Novell
IPX	IPX	kA	IPX
sítě	síť	k1gFnSc2	síť
ale	ale	k8xC	ale
ne	ne	k9	ne
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
a	a	k8xC	a
Advanced	Advanced	k1gMnSc1	Advanced
Server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
i	i	k9	i
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
serverový	serverový	k2eAgInSc4d1	serverový
software	software	k1gInSc4	software
<g/>
.	.	kIx.	.
</s>
<s>
Personal	Personat	k5eAaImAgInS	Personat
edition	edition	k1gInSc1	edition
byla	být	k5eAaImAgFnS	být
limitována	limitovat	k5eAaBmNgFnS	limitovat
dvěma	dva	k4xCgMnPc7	dva
aktivními	aktivní	k2eAgMnPc7d1	aktivní
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhá	druhý	k4xOgFnSc1	druhý
verze	verze	k1gFnSc1	verze
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
neomezenou	omezený	k2eNgFnSc4d1	neomezená
uživatelskou	uživatelský	k2eAgFnSc4d1	Uživatelská
licenci	licence	k1gFnSc4	licence
<g/>
.	.	kIx.	.
</s>
<s>
Prodáno	prodán	k2eAgNnSc1d1	prodáno
bylo	být	k5eAaImAgNnS	být
okolo	okolo	k6eAd1	okolo
35	[number]	k4	35
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
UnixWare	UnixWar	k1gMnSc5	UnixWar
1.0	[number]	k4	1.0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
koupil	koupit	k5eAaPmAgMnS	koupit
Novell	Novell	kA	Novell
USL	USL	kA	USL
od	od	k7c2	od
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
a	a	k8xC	a
spojil	spojit	k5eAaPmAgInS	spojit
Univel	Univel	k1gInSc1	Univel
a	a	k8xC	a
USL	USL	kA	USL
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
Unix	Unix	k1gInSc1	Unix
System	Systo	k1gNnSc7	Systo
Group	Group	k1gMnSc1	Group
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novell	Novell	kA	Novell
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vydal	vydat	k5eAaPmAgMnS	vydat
Novell	Novell	kA	Novell
UnixWare	UnixWar	k1gMnSc5	UnixWar
1.1	[number]	k4	1.1
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Knihovny	knihovna	k1gFnPc1	knihovna
MOTIF	Motif	kA	Motif
1.2	[number]	k4	1.2
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
pro	pro	k7c4	pro
shodu	shoda	k1gFnSc4	shoda
s	s	k7c7	s
COSE	COSE	kA	COSE
(	(	kIx(	(
<g/>
Common	Common	k1gInSc1	Common
Open	Open	k1gNnSc1	Open
Software	software	k1gInSc1	software
Environment	Environment	k1gMnSc1	Environment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
NUC	NUC	kA	NUC
(	(	kIx(	(
<g/>
NetWare	NetWar	k1gMnSc5	NetWar
Unix	Unix	k1gInSc1	Unix
Client	Client	k1gInSc1	Client
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
pro	pro	k7c4	pro
integraci	integrace	k1gFnSc4	integrace
se	s	k7c7	s
servery	server	k1gInPc7	server
Novell	Novell	kA	Novell
NetWare	NetWar	k1gInSc5	NetWar
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
Advanced	Advanced	k1gInSc4	Advanced
Merge	Merg	k1gFnSc2	Merg
byla	být	k5eAaImAgFnS	být
instalována	instalovat	k5eAaBmNgFnS	instalovat
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
verzích	verze	k1gFnPc6	verze
ke	k	k7c3	k
spuštění	spuštění	k1gNnSc3	spuštění
aplikací	aplikace	k1gFnPc2	aplikace
DOS	DOS	kA	DOS
a	a	k8xC	a
Windows	Windows	kA	Windows
3.1	[number]	k4	3.1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novell	Novell	kA	Novell
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgMnS	vydat
opravné	opravný	k2eAgFnPc4d1	opravná
verze	verze	k1gFnPc4	verze
1.1	[number]	k4	1.1
<g/>
.1	.1	k4	.1
<g/>
,	,	kIx,	,
1.1	[number]	k4	1.1
<g/>
.2	.2	k4	.2
<g/>
,	,	kIx,	,
1.1	[number]	k4	1.1
<g/>
.3	.3	k4	.3
až	až	k9	až
1.1	[number]	k4	1.1
<g/>
.4	.4	k4	.4
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydaná	vydaný	k2eAgFnSc1d1	vydaná
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
UnixWare	UnixWar	k1gMnSc5	UnixWar
2.0	[number]	k4	2.0
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
jádře	jádro	k1gNnSc6	jádro
Unix	Unix	k1gInSc4	Unix
System	Systo	k1gNnSc7	Systo
V	v	k7c4	v
verze	verze	k1gFnPc4	verze
4.2	[number]	k4	4.2
<g/>
MP	MP	kA	MP
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přidal	přidat	k5eAaPmAgMnS	přidat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
procesorů	procesor	k1gInPc2	procesor
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
dodáván	dodávat	k5eAaImNgInS	dodávat
do	do	k7c2	do
OEM	OEM	kA	OEM
a	a	k8xC	a
vývojářům	vývojář	k1gMnPc3	vývojář
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1994	[number]	k4	1994
a	a	k8xC	a
na	na	k7c4	na
spotřebitelský	spotřebitelský	k2eAgInSc4d1	spotřebitelský
trh	trh	k1gInSc4	trh
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
verze	verze	k1gFnPc1	verze
podporovaly	podporovat	k5eAaImAgFnP	podporovat
duální	duální	k2eAgInPc4d1	duální
procesorové	procesorový	k2eAgInPc4d1	procesorový
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
nákupu	nákup	k1gInSc2	nákup
"	"	kIx"	"
<g/>
extra	extra	k2eAgInSc1d1	extra
processor	processor	k1gInSc1	processor
upgrade	upgrade	k1gInSc1	upgrade
<g/>
"	"	kIx"	"
licence	licence	k1gFnSc1	licence
pro	pro	k7c4	pro
serverovou	serverový	k2eAgFnSc4d1	serverová
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Podporované	podporovaný	k2eAgInPc1d1	podporovaný
víceprocesorové	víceprocesorový	k2eAgInPc1d1	víceprocesorový
systémy	systém	k1gInPc1	systém
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
standard	standard	k1gInSc4	standard
Intel	Intel	kA	Intel
MP	MP	kA	MP
1.1	[number]	k4	1.1
symetrických	symetrický	k2eAgInPc2d1	symetrický
víceprocesorových	víceprocesorový	k2eAgInPc2d1	víceprocesorový
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
Corollary	Corollara	k1gFnSc2	Corollara
C-bus	Cus	k1gInSc1	C-bus
systémy	systém	k1gInPc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
podporovaly	podporovat	k5eAaImAgInP	podporovat
síťové	síťový	k2eAgInPc1d1	síťový
drivery	driver	k1gInPc1	driver
NetWare	NetWar	k1gMnSc5	NetWar
ODI	ODI	kA	ODI
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
zvýšit	zvýšit	k5eAaPmF	zvýšit
počet	počet	k1gInSc4	počet
podporovaných	podporovaný	k2eAgNnPc2d1	podporované
síťových	síťový	k2eAgNnPc2d1	síťové
rozhraní	rozhraní	k1gNnPc2	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
nové	nový	k2eAgFnPc1d1	nová
vlastnosti	vlastnost	k1gFnPc1	vlastnost
verze	verze	k1gFnSc2	verze
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
knihovnu	knihovna	k1gFnSc4	knihovna
POSIX	POSIX	kA	POSIX
Threads	Threads	k1gInSc4	Threads
navíc	navíc	k6eAd1	navíc
ke	k	k7c3	k
starší	starý	k2eAgFnSc3d2	starší
knihovně	knihovna	k1gFnSc3	knihovna
UI	UI	kA	UI
Threads	Threads	k1gInSc1	Threads
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Santa	Sant	k1gInSc2	Sant
Cruz	Cruz	k1gInSc1	Cruz
Operation	Operation	k1gInSc1	Operation
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
Operation	Operation	k1gInSc1	Operation
(	(	kIx(	(
<g/>
SCO	SCO	kA	SCO
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
UnixWare	UnixWar	k1gMnSc5	UnixWar
od	od	k7c2	od
Novellu	Novell	k1gInSc2	Novell
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc4d1	přesná
podmínky	podmínka	k1gFnPc4	podmínka
této	tento	k3xDgFnSc2	tento
transakce	transakce	k1gFnSc2	transakce
byly	být	k5eAaImAgInP	být
sporné	sporný	k2eAgInPc1d1	sporný
<g/>
,	,	kIx,	,
soud	soud	k1gInSc1	soud
následně	následně	k6eAd1	následně
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Novell	Novell	kA	Novell
udržel	udržet	k5eAaPmAgMnS	udržet
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
Unixu	Unix	k1gInSc2	Unix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
převod	převod	k1gInSc1	převod
zveřejněn	zveřejněn	k2eAgInSc1d1	zveřejněn
<g/>
,	,	kIx,	,
SCO	SCO	kA	SCO
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
sloučení	sloučení	k1gNnSc4	sloučení
UnixWare	UnixWar	k1gMnSc5	UnixWar
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
OpenServerem	OpenServer	k1gInSc7	OpenServer
SVR	SVR	kA	SVR
<g/>
3.2	[number]	k4	3.2
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c4	na
OS	OS	kA	OS
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
UnixWare	UnixWar	k1gMnSc5	UnixWar
od	od	k7c2	od
SCO	SCO	kA	SCO
byla	být	k5eAaImAgFnS	být
verze	verze	k1gFnSc1	verze
2.1	[number]	k4	2.1
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
UnixWare	UnixWar	k1gMnSc5	UnixWar
2.1	[number]	k4	2.1
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zamýšlené	zamýšlený	k2eAgNnSc1d1	zamýšlené
UnixWare	UnixWar	k1gMnSc5	UnixWar
<g/>
/	/	kIx~	/
<g/>
OpenServer	OpenServra	k1gFnPc2	OpenServra
sloučení	sloučení	k1gNnSc2	sloučení
bylo	být	k5eAaImAgNnS	být
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k9	jako
projekt	projekt	k1gInSc4	projekt
Gemini	Gemin	k1gMnPc1	Gemin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bude	být	k5eAaImBp3nS	být
dostupný	dostupný	k2eAgMnSc1d1	dostupný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
a	a	k8xC	a
64	[number]	k4	64
bitová	bitový	k2eAgFnSc1d1	bitová
verze	verze	k1gFnSc1	verze
UnixWare	UnixWar	k1gMnSc5	UnixWar
byla	být	k5eAaImAgFnS	být
vyvíjena	vyvíjet	k5eAaImNgFnS	vyvíjet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kontroverzní	kontroverzní	k2eAgFnSc7d1	kontroverzní
změnou	změna	k1gFnSc7	změna
bylo	být	k5eAaImAgNnS	být
přijetí	přijetí	k1gNnSc1	přijetí
OpenServeru	OpenServer	k1gInSc2	OpenServer
jako	jako	k8xC	jako
uživatelské	uživatelský	k2eAgFnSc2d1	Uživatelská
licenční	licenční	k2eAgFnSc2d1	licenční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Univel	Univel	k1gInSc1	Univel
a	a	k8xC	a
Novell	Novell	kA	Novell
verze	verze	k1gFnSc1	verze
UnixWare	UnixWar	k1gMnSc5	UnixWar
povolovaly	povolovat	k5eAaImAgFnP	povolovat
2	[number]	k4	2
uživatele	uživatel	k1gMnPc4	uživatel
v	v	k7c6	v
personal	personat	k5eAaImAgInS	personat
edition	edition	k1gInSc1	edition
a	a	k8xC	a
neomezené	omezený	k2eNgNnSc1d1	neomezené
množství	množství	k1gNnSc1	množství
uživatelů	uživatel	k1gMnPc2	uživatel
v	v	k7c4	v
server	server	k1gInSc4	server
edition	edition	k1gInSc1	edition
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
UnixWare	UnixWar	k1gMnSc5	UnixWar
2.1	[number]	k4	2.1
server	server	k1gInSc4	server
edition	edition	k1gInSc4	edition
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
licence	licence	k1gFnSc1	licence
maximálně	maximálně	k6eAd1	maximálně
5	[number]	k4	5
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Zákazníci	zákazník	k1gMnPc1	zákazník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
více	hodně	k6eAd2	hodně
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
koupit	koupit	k5eAaPmF	koupit
10	[number]	k4	10
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
500	[number]	k4	500
či	či	k8xC	či
neomezené	omezený	k2eNgNnSc4d1	neomezené
rozšíření	rozšíření	k1gNnSc4	rozšíření
uživatelské	uživatelský	k2eAgFnSc2d1	Uživatelská
licence	licence	k1gFnSc2	licence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCO	SCO	kA	SCO
vydala	vydat	k5eAaPmAgFnS	vydat
3	[number]	k4	3
updaty	update	k1gInPc1	update
k	k	k7c3	k
UnixWare	UnixWar	k1gMnSc5	UnixWar
2.1	[number]	k4	2.1
<g/>
.	.	kIx.	.
</s>
<s>
UnixWare	UnixWar	k1gMnSc5	UnixWar
2.1	[number]	k4	2.1
<g/>
.1	.1	k4	.1
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
označení	označení	k1gNnSc4	označení
Unix	Unix	k1gInSc1	Unix
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
UnixWare	UnixWar	k1gMnSc5	UnixWar
2.1	[number]	k4	2.1
<g/>
.2	.2	k4	.2
a	a	k8xC	a
2.1	[number]	k4	2.1
<g/>
.3	.3	k4	.3
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgInPc1d1	dostupný
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
převážně	převážně	k6eAd1	převážně
verzemi	verze	k1gFnPc7	verze
opravujícími	opravující	k2eAgFnPc7d1	opravující
závažné	závažný	k2eAgFnPc4d1	závažná
chyby	chyba	k1gFnPc4	chyba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
firma	firma	k1gFnSc1	firma
Compaq	Compaq	kA	Compaq
vydala	vydat	k5eAaPmAgFnS	vydat
balíček	balíček	k1gInSc4	balíček
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Integrity	integrita	k1gFnPc1	integrita
XC	XC	kA	XC
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
single-system	singleyst	k1gFnPc3	single-syst
image	image	k1gInSc4	image
(	(	kIx(	(
<g/>
SSI	SSI	kA	SSI
<g/>
)	)	kIx)	)
clusterů	cluster	k1gInPc2	cluster
ze	z	k7c2	z
serverů	server	k1gInPc2	server
Proliant	Proliant	k1gInSc1	Proliant
s	s	k7c7	s
verzí	verze	k1gFnSc7	verze
UnixWare	UnixWar	k1gMnSc5	UnixWar
2.1	[number]	k4	2.1
a	a	k8xC	a
UnixWare	UnixWar	k1gMnSc5	UnixWar
NonStop	nonstop	k2eAgMnPc2d1	nonstop
clusterů	cluster	k1gInPc2	cluster
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
výsledky	výsledek	k1gInPc1	výsledek
projektu	projekt	k1gInSc2	projekt
Geminy	Gemina	k1gFnSc2	Gemina
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
dostupnými	dostupný	k2eAgInPc7d1	dostupný
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
jako	jako	k8xS	jako
UnixWare	UnixWar	k1gMnSc5	UnixWar
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SCO	SCO	kA	SCO
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
verzi	verze	k1gFnSc4	verze
jádra	jádro	k1gNnSc2	jádro
Unix	Unix	k1gInSc1	Unix
Systém	systém	k1gInSc1	systém
V	V	kA	V
verze	verze	k1gFnSc2	verze
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
převážně	převážně	k6eAd1	převážně
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
UnixWare	UnixWar	k1gMnSc5	UnixWar
2.1	[number]	k4	2.1
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozšířeními	rozšíření	k1gNnPc7	rozšíření
pro	pro	k7c4	pro
kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
ovladačů	ovladač	k1gInPc2	ovladač
s	s	k7c7	s
OpenServerem	OpenServer	k1gInSc7	OpenServer
<g/>
,	,	kIx,	,
umožňujícím	umožňující	k2eAgInSc7d1	umožňující
použití	použití	k1gNnSc4	použití
síťových	síťový	k2eAgInPc2d1	síťový
driverů	driver	k1gInPc2	driver
OpenServeru	OpenServer	k1gInSc2	OpenServer
<g/>
.	.	kIx.	.
</s>
<s>
Systémové	systémový	k2eAgInPc1d1	systémový
administrační	administrační	k2eAgInPc1d1	administrační
nástroje	nástroj	k1gInPc1	nástroj
OpenServeru	OpenServer	k1gInSc2	OpenServer
<g/>
,	,	kIx,	,
scoadmin	scoadmin	k2eAgInSc1d1	scoadmin
<g/>
,	,	kIx,	,
nahradily	nahradit	k5eAaPmAgInP	nahradit
původní	původní	k2eAgFnSc4d1	původní
UnixWare	UnixWar	k1gMnSc5	UnixWar
sysadmin	sysadmin	k1gInSc4	sysadmin
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
nové	nový	k2eAgFnPc4d1	nová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
UnixWare	UnixWar	k1gMnSc5	UnixWar
7	[number]	k4	7
patří	patřit	k5eAaImIp3nS	patřit
multi-path	multiath	k1gMnSc1	multi-path
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc1d1	velký
soubory	soubor	k1gInPc1	soubor
a	a	k8xC	a
souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
velké	velký	k2eAgInPc4d1	velký
paměťové	paměťový	k2eAgInPc4d1	paměťový
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Překvapující	překvapující	k2eAgFnSc7d1	překvapující
vlastností	vlastnost	k1gFnSc7	vlastnost
UnixWare	UnixWar	k1gMnSc5	UnixWar
7	[number]	k4	7
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
chyběla	chybět	k5eAaImAgFnS	chybět
vlastnost	vlastnost	k1gFnSc1	vlastnost
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
Xenix	Xenix	k1gInSc1	Xenix
obou	dva	k4xCgFnPc6	dva
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
licenčních	licenční	k2eAgMnPc2d1	licenční
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
placení	placení	k1gNnSc4	placení
Microsoftu	Microsoft	k1gInSc2	Microsoft
za	za	k7c4	za
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
měli	mít	k5eAaImAgMnP	mít
zahrnutý	zahrnutý	k2eAgInSc4d1	zahrnutý
v	v	k7c4	v
SVR	SVR	kA	SVR
<g/>
3.2	[number]	k4	3.2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
SCO	SCO	kA	SCO
vydala	vydat	k5eAaPmAgFnS	vydat
UnixWare	UnixWar	k1gMnSc5	UnixWar
7.1	[number]	k4	7.1
update	update	k1gInSc4	update
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
počet	počet	k1gInSc4	počet
verzí	verze	k1gFnPc2	verze
<g/>
;	;	kIx,	;
Business	business	k1gInSc1	business
edition	edition	k1gInSc1	edition
–	–	k?	–
5	[number]	k4	5
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
Department	department	k1gInSc1	department
edition	edition	k1gInSc1	edition
–	–	k?	–
25	[number]	k4	25
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	a
Enterprise	Enterprise	k1gFnSc1	Enterprise
edition	edition	k1gInSc1	edition
–	–	k?	–
50	[number]	k4	50
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
nahradily	nahradit	k5eAaPmAgInP	nahradit
dřívější	dřívější	k2eAgInPc1d1	dřívější
personal	personat	k5eAaImAgInS	personat
a	a	k8xC	a
server	server	k1gInSc1	server
edition	edition	k1gInSc1	edition
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
WebTop	WebTop	k1gInSc1	WebTop
z	z	k7c2	z
Tarantelly	Tarantella	k1gFnSc2	Tarantella
byla	být	k5eAaImAgFnS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
SCO	SCO	kA	SCO
vydala	vydat	k5eAaPmAgFnS	vydat
UnixWare	UnixWar	k1gMnSc5	UnixWar
7.1	[number]	k4	7.1
<g/>
.1	.1	k4	.1
update	update	k1gInSc1	update
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
balíček	balíček	k1gInSc1	balíček
UnixWare	UnixWar	k1gMnSc5	UnixWar
NonStop	nonstop	k2eAgInPc1d1	nonstop
clustery	cluster	k1gInPc1	cluster
7.1	[number]	k4	7.1
<g/>
.1	.1	k4	.1
+	+	kIx~	+
IP	IP	kA	IP
single-system	singleyst	k1gInSc7	single-syst
image	image	k1gInSc1	image
cluster	cluster	k1gInSc1	cluster
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
balíček	balíček	k1gInSc1	balíček
povoloval	povolovat	k5eAaImAgInS	povolovat
užívat	užívat	k5eAaImF	užívat
PC	PC	kA	PC
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
jako	jako	k8xS	jako
patentovaný	patentovaný	k2eAgInSc1d1	patentovaný
hardware	hardware	k1gInSc1	hardware
Compaq	Compaq	kA	Compaq
<g/>
,	,	kIx,	,
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
dřívějším	dřívější	k2eAgInSc7d1	dřívější
produktem	produkt	k1gInSc7	produkt
Integrity	integrita	k1gFnSc2	integrita
XC	XC	kA	XC
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
přímo	přímo	k6eAd1	přímo
dostupný	dostupný	k2eAgMnSc1d1	dostupný
z	z	k7c2	z
SCO	SCO	kA	SCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
SCO	SCO	kA	SCO
prodala	prodat	k5eAaPmAgFnS	prodat
jejich	jejich	k3xOp3gInSc4	jejich
Serverový	serverový	k2eAgInSc4d1	serverový
software	software	k1gInSc4	software
a	a	k8xC	a
divizi	divize	k1gFnSc4	divize
služeb	služba	k1gFnPc2	služba
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
právy	právo	k1gNnPc7	právo
k	k	k7c3	k
produktům	produkt	k1gInPc3	produkt
UnixWare	UnixWar	k1gMnSc5	UnixWar
a	a	k8xC	a
Open	Open	k1gNnSc1	Open
Serveru	server	k1gInSc2	server
společnosti	společnost	k1gFnSc2	společnost
Caldera	Caldera	k1gFnSc1	Caldera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Caldera	Calder	k1gMnSc4	Calder
<g/>
/	/	kIx~	/
<g/>
SCO	SCO	kA	SCO
<g/>
/	/	kIx~	/
<g/>
The	The	k1gMnSc1	The
SCO	SCO	kA	SCO
Group	Group	k1gMnSc1	Group
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
koupila	koupit	k5eAaPmAgFnS	koupit
Caldera	Caldera	k1gFnSc1	Caldera
Systems	Systemsa	k1gFnPc2	Systemsa
divize	divize	k1gFnSc2	divize
server	server	k1gInSc1	server
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
společnosti	společnost	k1gFnSc2	společnost
The	The	k1gMnSc1	The
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
Operation	Operation	k1gInSc4	Operation
včetně	včetně	k7c2	včetně
produktových	produktový	k2eAgFnPc2d1	produktová
řad	řada	k1gFnPc2	řada
obou	dva	k4xCgInPc2	dva
produktů	produkt	k1gInPc2	produkt
-	-	kIx~	-
Open	Open	k1gNnSc1	Open
Serveru	server	k1gInSc2	server
a	a	k8xC	a
UnixWare	UnixWar	k1gInSc5	UnixWar
<g/>
.	.	kIx.	.
</s>
<s>
Caldera	Caldera	k1gFnSc1	Caldera
zároveň	zároveň	k6eAd1	zároveň
změnila	změnit	k5eAaPmAgFnS	změnit
jméno	jméno	k1gNnSc4	jméno
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
SCO	SCO	kA	SCO
a	a	k8xC	a
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
jejich	jejich	k3xOp3gFnSc2	jejich
produktové	produktový	k2eAgFnSc2d1	produktová
řady	řada	k1gFnSc2	řada
o	o	k7c4	o
mobilní	mobilní	k2eAgInPc4d1	mobilní
produkty	produkt	k1gInPc4	produkt
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
změnila	změnit	k5eAaPmAgFnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c6	na
The	The	k1gFnSc6	The
SCO	SCO	kA	SCO
Group	Group	k1gInSc1	Group
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgNnSc1d1	prvotní
vydání	vydání	k1gNnSc1	vydání
UnixWare	UnixWar	k1gMnSc5	UnixWar
od	od	k7c2	od
Caldery	Caldera	k1gFnSc2	Caldera
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c6	na
OpenUNIX	OpenUNIX	k1gFnSc6	OpenUNIX
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vydání	vydání	k1gNnSc1	vydání
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
UnixWare	UnixWar	k1gMnSc5	UnixWar
7.1	[number]	k4	7.1
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
SCO	SCO	kA	SCO
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
k	k	k7c3	k
původnímu	původní	k2eAgNnSc3d1	původní
označení	označení	k1gNnSc3	označení
UnixWare	UnixWar	k1gMnSc5	UnixWar
a	a	k8xC	a
číslování	číslování	k1gNnSc1	číslování
verzí	verze	k1gFnPc2	verze
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
UnixWare	UnixWar	k1gMnSc5	UnixWar
7.1	[number]	k4	7.1
<g/>
.3	.3	k4	.3
a	a	k8xC	a
7.1	[number]	k4	7.1
<g/>
.4	.4	k4	.4
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgFnPc1	žádný
další	další	k2eAgFnPc1d1	další
verze	verze	k1gFnPc1	verze
OpenUNIX	OpenUNIX	k1gMnPc2	OpenUNIX
nebyly	být	k5eNaImAgFnP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
a	a	k8xC	a
OpenUNIX	OpenUNIX	k1gFnSc1	OpenUNIX
8.1	[number]	k4	8.1
<g/>
.2	.2	k4	.2
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
vydán	vydat	k5eAaPmNgInS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
SCO	SCO	kA	SCO
Group	Group	k1gInSc1	Group
UnixWare	UnixWar	k1gMnSc5	UnixWar
nadále	nadále	k6eAd1	nadále
udržuje	udržovat	k5eAaImIp3nS	udržovat
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaImIp3nS	vydávat
pravidelně	pravidelně	k6eAd1	pravidelně
aktualizace	aktualizace	k1gFnSc1	aktualizace
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
systému	systém	k1gInSc2	systém
UnixWare	UnixWar	k1gInSc5	UnixWar
==	==	k?	==
</s>
</p>
<p>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
UnixWare	UnixWar	k1gMnSc5	UnixWar
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Personal	Personat	k5eAaImAgInS	Personat
Edition	Edition	k1gInSc1	Edition
</s>
</p>
<p>
<s>
Application	Application	k1gInSc1	Application
Server	server	k1gInSc1	server
</s>
</p>
<p>
<s>
===	===	k?	===
Personal	Personal	k1gFnPc2	Personal
Edition	Edition	k1gInSc1	Edition
===	===	k?	===
</s>
</p>
<p>
<s>
Personal	Personat	k5eAaPmAgInS	Personat
Edition	Edition	k1gInSc1	Edition
se	se	k3xPyFc4	se
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
do	do	k7c2	do
grafického	grafický	k2eAgInSc2d1	grafický
jednouživatelského	jednouživatelský	k2eAgInSc2d1	jednouživatelský
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
lze	lze	k6eAd1	lze
využívat	využívat	k5eAaImF	využívat
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
pracovní	pracovní	k2eAgFnSc4d1	pracovní
stanici	stanice	k1gFnSc4	stanice
</s>
</p>
<p>
<s>
klienta	klient	k1gMnSc4	klient
v	v	k7c6	v
distribuovaném	distribuovaný	k2eAgInSc6d1	distribuovaný
prostředíSlužby	prostředíSlužb	k1gInPc4	prostředíSlužb
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
Personal	Personal	k1gMnSc1	Personal
Edition	Edition	k1gInSc4	Edition
uživateli	uživatel	k1gMnSc3	uživatel
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
multitaskingové	multitaskingový	k2eAgNnSc1d1	multitaskingový
spouštění	spouštění	k1gNnSc1	spouštění
unixových	unixový	k2eAgFnPc2d1	unixová
a	a	k8xC	a
DOSových	dosový	k2eAgFnPc2d1	dosová
aplikací	aplikace	k1gFnPc2	aplikace
</s>
</p>
<p>
<s>
výhody	výhoda	k1gFnPc1	výhoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
přejít	přejít	k5eAaPmF	přejít
kdykoliv	kdykoliv	k6eAd1	kdykoliv
k	k	k7c3	k
jinému	jiný	k2eAgInSc3d1	jiný
programu	program	k1gInSc3	program
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
předešlá	předešlý	k2eAgFnSc1d1	předešlá
práce	práce	k1gFnSc1	práce
musela	muset	k5eAaImAgFnS	muset
ukončit	ukončit	k5eAaPmF	ukončit
</s>
</p>
<p>
<s>
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
programy	program	k1gInPc1	program
mohou	moct	k5eAaImIp3nP	moct
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
</s>
</p>
<p>
<s>
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
špatně	špatně	k6eAd1	špatně
sestaven	sestavit	k5eAaPmNgInS	sestavit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
negativně	negativně	k6eAd1	negativně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
právě	právě	k6eAd1	právě
běží	běžet	k5eAaImIp3nS	běžet
</s>
</p>
<p>
<s>
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
zahlcení	zahlcení	k1gNnSc3	zahlcení
systému	systém	k1gInSc2	systém
</s>
</p>
<p>
<s>
systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
velké	velký	k2eAgInPc4d1	velký
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
hardware	hardware	k1gInSc4	hardware
počítače	počítač	k1gInSc2	počítač
</s>
</p>
<p>
<s>
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
zdroji	zdroj	k1gInSc3	zdroj
NetWare	NetWar	k1gMnSc5	NetWar
</s>
</p>
<p>
<s>
===	===	k?	===
Application	Application	k1gInSc1	Application
Server	server	k1gInSc1	server
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Personal	Personal	k1gFnSc2	Personal
Edition	Edition	k1gInSc1	Edition
je	být	k5eAaImIp3nS	být
Application	Application	k1gInSc4	Application
Server	server	k1gInSc4	server
síťově	síťově	k6eAd1	síťově
orientovaný	orientovaný	k2eAgInSc4d1	orientovaný
víceuživatelský	víceuživatelský	k2eAgInSc4d1	víceuživatelský
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
Personal	Personal	k1gFnSc2	Personal
Edition	Edition	k1gInSc4	Edition
a	a	k8xC	a
na	na	k7c4	na
víc	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
o	o	k7c4	o
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
a	a	k8xC	a
NFS	NFS	kA	NFS
</s>
</p>
<p>
<s>
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
nainstalování	nainstalování	k1gNnSc4	nainstalování
Personal	Personal	k1gFnSc2	Personal
Edition	Edition	k1gInSc4	Edition
nebo	nebo	k8xC	nebo
Application	Application	k1gInSc1	Application
Server	server	k1gInSc1	server
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
počítači	počítač	k1gInSc6	počítač
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Přístupová	přístupový	k2eAgNnPc1d1	přístupové
práva	právo	k1gNnPc1	právo
v	v	k7c6	v
Unixu	Unix	k1gInSc6	Unix
</s>
</p>
<p>
<s>
Uživatelské	uživatelský	k2eAgInPc4d1	uživatelský
účty	účet	k1gInPc4	účet
v	v	k7c6	v
Unixu	Unix	k1gInSc6	Unix
</s>
</p>
