<s>
Konstantin	Konstantin	k1gMnSc1
Valkov	Valkov	k1gInSc4
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Anatoljevič	Anatoljevič	k1gMnSc1
ValkovKosmonaut	ValkovKosmonaut	k1gMnSc1
CPK	CPK	kA
Státní	státní	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1971	#num#	k4
(	(	kIx(
<g/>
49	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Kamensk-Uralskij	Kamensk-Uralskít	k5eAaPmRp2nS
<g/>
,	,	kIx,
Sverdlovská	sverdlovský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
RSFSR	RSFSR	kA
<g/>
,	,	kIx,
SSSR	SSSR	kA
Předchozízaměstnání	Předchozízaměstnání	k1gNnSc1
</s>
<s>
Vojenský	vojenský	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
Hodnost	hodnost	k1gFnSc4
</s>
<s>
plukovník	plukovník	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2009	#num#	k4
<g/>
)	)	kIx)
Čas	čas	k1gInSc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
</s>
<s>
dosud	dosud	k6eAd1
neletěl	letět	k5eNaImAgMnS
Kosmonaut	kosmonaut	k1gMnSc1
od	od	k7c2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1997	#num#	k4
Kosmonaut	kosmonaut	k1gMnSc1
do	do	k7c2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Anatoljevič	Anatoljevič	k1gMnSc1
Valkov	Valkov	k1gMnSc1xF
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
К	К	k?
А	А	k?
В	В	k?
<g/>
,	,	kIx,
*	*	kIx~
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1971	#num#	k4
<g/>
,	,	kIx,
Kamensk-Uralskij	Kamensk-Uralskij	k1gFnSc1
<g/>
,	,	kIx,
Sverdlovská	sverdlovský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
RSFSR	RSFSR	kA
<g/>
,	,	kIx,
SSSR	SSSR	kA
<g/>
)	)	kIx)
sloužil	sloužit	k5eAaImAgMnS
v	v	k7c6
ruském	ruský	k2eAgNnSc6d1
vojenském	vojenský	k2eAgNnSc6d1
letectvu	letectvo	k1gNnSc6
jako	jako	k8xS,k8xC
pilot	pilot	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1997	#num#	k4
<g/>
–	–	kIx~
<g/>
2012	#num#	k4
byl	být	k5eAaImAgInS
ruským	ruský	k2eAgMnSc7d1
kosmonautem	kosmonaut	k1gMnSc7
<g/>
,	,	kIx,
členem	člen	k1gMnSc7
oddílu	oddíl	k1gInSc2
CPK	CPK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
vesmíru	vesmír	k1gInSc2
se	se	k3xPyFc4
nevypravil	vypravit	k5eNaPmAgMnS
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
je	být	k5eAaImIp3nS
instruktorem	instruktor	k1gMnSc7
Střediska	středisko	k1gNnSc2
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
(	(	kIx(
<g/>
CPK	CPK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Anatoljevič	Anatoljevič	k1gMnSc1
Valkov	Valkov	k1gInSc4
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
města	město	k1gNnSc2
Kamensk-Uralskij	Kamensk-Uralskij	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
místní	místní	k2eAgFnSc2d1
střední	střední	k2eAgFnSc2d1
školy	škola	k1gFnSc2
se	se	k3xPyFc4
přihlásil	přihlásit	k5eAaPmAgMnS
na	na	k7c4
Charkovskou	charkovský	k2eAgFnSc4d1
vysokou	vysoký	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
(	(	kIx(
<g/>
Х	Х	k?
в	в	k?
в	в	k?
а	а	k?
у	у	k?
л	л	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1993	#num#	k4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
rozpadem	rozpad	k1gInSc7
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
přešel	přejít	k5eAaPmAgInS
na	na	k7c4
Barnaulskou	Barnaulský	k2eAgFnSc4d1
vysokou	vysoký	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
(	(	kIx(
<g/>
Б	Б	k?
в	в	k?
в	в	k?
а	а	k?
у	у	k?
л	л	k?
и	и	k?
<g/>
.	.	kIx.
К	К	k?
<g/>
В	В	k?
<g/>
)	)	kIx)
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
ji	on	k3xPp3gFnSc4
s	s	k7c7
úspěchem	úspěch	k1gInSc7
dokončil	dokončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
sloužil	sloužit	k5eAaImAgInS
v	v	k7c6
letectvu	letectvo	k1gNnSc6
<g/>
,	,	kIx,
létal	létat	k5eAaImAgMnS
na	na	k7c6
Su-	Su-	k1gFnSc6
<g/>
24	#num#	k4
v	v	k7c4
67	#num#	k4
<g/>
.	.	kIx.
bombardovacím	bombardovací	k2eAgInSc6d1
pluku	pluk	k1gInSc6
149	#num#	k4
<g/>
.	.	kIx.
bombardovací	bombardovací	k2eAgFnSc2d1
divize	divize	k1gFnSc2
76	#num#	k4
<g/>
.	.	kIx.
letecké	letecký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
v	v	k7c6
Leningradské	leningradský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kosmonaut	kosmonaut	k1gMnSc1
</s>
<s>
Roku	rok	k1gInSc2
1997	#num#	k4
se	se	k3xPyFc4
přihlásil	přihlásit	k5eAaPmAgMnS
k	k	k7c3
kosmonautickému	kosmonautický	k2eAgInSc3d1
výcviku	výcvik	k1gInSc3
<g/>
,	,	kIx,
prošel	projít	k5eAaPmAgInS
lékařskými	lékařský	k2eAgFnPc7d1
prohlídkami	prohlídka	k1gFnPc7
a	a	k8xC
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1997	#num#	k4
byl	být	k5eAaImAgInS
Státní	státní	k2eAgInSc1d1
meziresortní	meziresortní	k2eAgInSc1d1
komisí	komise	k1gFnPc2
doporučen	doporučen	k2eAgMnSc1d1
k	k	k7c3
zařazení	zařazení	k1gNnSc3
do	do	k7c2
oddílu	oddíl	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
CPK	CPK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
oddílu	oddíl	k1gInSc2
byl	být	k5eAaImAgInS
začleněn	začlenit	k5eAaPmNgInS
na	na	k7c4
pozici	pozice	k1gFnSc4
kandidáta	kandidát	k1gMnSc2
na	na	k7c4
kosmonauta	kosmonaut	k1gMnSc4
až	až	k9
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolvoval	absolvovat	k5eAaPmAgMnS
dvouletou	dvouletý	k2eAgFnSc4d1
všeobecnou	všeobecný	k2eAgFnSc4d1
kosmickou	kosmický	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1999	#num#	k4
získal	získat	k5eAaPmAgMnS
kvalifikaci	kvalifikace	k1gFnSc4
zkušební	zkušební	k2eAgMnSc1d1
kosmonaut	kosmonaut	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
listopadu	listopad	k1gInSc2
2000	#num#	k4
do	do	k7c2
července	červenec	k1gInSc2
2001	#num#	k4
a	a	k8xC
znova	znova	k6eAd1
od	od	k7c2
září	září	k1gNnSc2
2004	#num#	k4
byl	být	k5eAaImAgMnS
představitelem	představitel	k1gMnSc7
Střediska	středisko	k1gNnSc2
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
(	(	kIx(
<g/>
CPK	CPK	kA
<g/>
)	)	kIx)
v	v	k7c4
Johnsonovu	Johnsonův	k2eAgFnSc4d1
vesmírném	vesmírný	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
NASA	NASA	kA
v	v	k7c6
Houstonu	Houston	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2010	#num#	k4
byl	být	k5eAaImAgInS
podle	podle	k7c2
neoficiálních	neoficiální	k2eAgFnPc2d1,k2eNgFnPc2d1
informací	informace	k1gFnPc2
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
členem	člen	k1gMnSc7
záložní	záložní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
Expedice	expedice	k1gFnSc2
29	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
na	na	k7c4
Mezinárodní	mezinárodní	k2eAgFnSc4d1
vesmírnou	vesmírný	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
(	(	kIx(
<g/>
ISS	ISS	kA
<g/>
)	)	kIx)
startující	startující	k2eAgMnSc1d1
v	v	k7c6
září	září	k1gNnSc6
2011	#num#	k4
a	a	k8xC
hlavní	hlavní	k2eAgFnPc1d1
posádky	posádka	k1gFnPc1
Expedice	expedice	k1gFnSc1
31	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
s	s	k7c7
plánovaným	plánovaný	k2eAgInSc7d1
startem	start	k1gInSc7
v	v	k7c6
březnu	březen	k1gInSc6
2012	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
červenci	červenec	k1gInSc6
2010	#num#	k4
jmenování	jmenování	k1gNnSc4
potvrdila	potvrdit	k5eAaPmAgFnS
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
2011	#num#	k4
byl	být	k5eAaImAgMnS
z	z	k7c2
posádky	posádka	k1gFnSc2
odstraněn	odstraněn	k2eAgMnSc1d1
(	(	kIx(
<g/>
kvůli	kvůli	k7c3
překročení	překročení	k1gNnSc3
váhového	váhový	k2eAgInSc2d1
limitu	limit	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
nahrazen	nahradit	k5eAaPmNgInS
Sergejem	Sergej	k1gMnSc7
Revinem	Revin	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2012	#num#	k4
byl	být	k5eAaImAgInS
uvolněn	uvolněn	k2eAgInSc1d1
z	z	k7c2
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
i	i	k8xC
oddílu	oddíl	k1gInSc2
kosmonautů	kosmonaut	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zůstal	zůstat	k5eAaPmAgMnS
ve	v	k7c6
Středisku	středisko	k1gNnSc6
přípravy	příprava	k1gFnSc2
kosmonautů	kosmonaut	k1gMnPc2
jako	jako	k8xS,k8xC
vyučující	vyučující	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konstantin	Konstantin	k1gMnSc1
Valkov	Valkov	k1gInSc4
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
je	být	k5eAaImIp3nS
psycholožka	psycholožka	k1gFnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
dceru	dcera	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
IVANOV	IVANOV	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
К	К	k?
э	э	k?
ASTROnote	ASTROnot	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2013-6-2	2013-6-2	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
К	К	k?
А	А	k?
В	В	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
