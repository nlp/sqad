<s>
Konstantin	Konstantin	k1gMnSc1
Anatoljevič	Anatoljevič	k1gMnSc1
Valkov	Valkov	k1gMnSc1xF
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
К	К	k?
А	А	k?
В	В	k?
<g/>
,	,	kIx,
*	*	kIx~
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1971	#num#	k4
<g/>
,	,	kIx,
Kamensk-Uralskij	Kamensk-Uralskij	k1gFnSc1
<g/>
,	,	kIx,
Sverdlovská	sverdlovský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
RSFSR	RSFSR	kA
<g/>
,	,	kIx,
SSSR	SSSR	kA
<g/>
)	)	kIx)
sloužil	sloužit	k5eAaImAgMnS
v	v	k7c6
ruském	ruský	k2eAgNnSc6d1
vojenském	vojenský	k2eAgNnSc6d1
letectvu	letectvo	k1gNnSc6
jako	jako	k8xS,k8xC
pilot	pilot	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1997	#num#	k4
<g/>
–	–	kIx~
<g/>
2012	#num#	k4
byl	být	k5eAaImAgInS
ruským	ruský	k2eAgMnSc7d1
kosmonautem	kosmonaut	k1gMnSc7
<g/>
,	,	kIx,
členem	člen	k1gMnSc7
oddílu	oddíl	k1gInSc2
CPK	CPK	kA
<g/>
.	.	kIx.
</s>