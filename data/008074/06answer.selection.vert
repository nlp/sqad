<s>
Vznik	vznik	k1gInSc1	vznik
lasagní	lasageň	k1gFnPc2	lasageň
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
klade	klást	k5eAaImIp3nS	klást
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc1	první
moderní	moderní	k2eAgInSc1d1	moderní
recept	recept	k1gInSc1	recept
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dal	dát	k5eAaPmAgInS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
tradičnímu	tradiční	k2eAgInSc3d1	tradiční
pokrmu	pokrm	k1gInSc3	pokrm
<g/>
.	.	kIx.	.
</s>
