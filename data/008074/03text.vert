<s>
Lasagne	Lasagnout	k5eAaImIp3nS	Lasagnout
(	(	kIx(	(
<g/>
singulár	singulár	k1gInSc1	singulár
lasagna	lasagn	k1gInSc2	lasagn
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
italské	italský	k2eAgFnPc1d1	italská
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgFnPc1d1	široká
těstoviny	těstovina	k1gFnPc1	těstovina
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
také	také	k9	také
běžně	běžně	k6eAd1	běžně
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
pokrmu	pokrm	k1gInSc3	pokrm
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
těstovinových	těstovinový	k2eAgInPc2d1	těstovinový
plátů	plát	k1gInPc2	plát
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
lasagní	lasageň	k1gFnPc2	lasageň
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
klade	klást	k5eAaImIp3nS	klást
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc1	první
moderní	moderní	k2eAgInSc1d1	moderní
recept	recept	k1gInSc1	recept
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dal	dát	k5eAaPmAgInS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
tradičnímu	tradiční	k2eAgInSc3d1	tradiční
pokrmu	pokrm	k1gInSc3	pokrm
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnSc1d1	tradiční
lasagne	lasagnout	k5eAaImIp3nS	lasagnout
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
střídáním	střídání	k1gNnSc7	střídání
vrstev	vrstva	k1gFnPc2	vrstva
těstovinového	těstovinový	k2eAgInSc2d1	těstovinový
plátu	plát	k1gInSc2	plát
<g/>
,	,	kIx,	,
bešamelové	bešamelový	k2eAgFnSc2d1	bešamelová
omáčky	omáčka	k1gFnSc2	omáčka
a	a	k8xC	a
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
variacích	variace	k1gFnPc6	variace
se	se	k3xPyFc4	se
do	do	k7c2	do
lasagní	lasagní	k1gNnSc2	lasagní
přidává	přidávat	k5eAaImIp3nS	přidávat
ricotta	ricotta	k1gFnSc1	ricotta
nebo	nebo	k8xC	nebo
mozzarella	mozzarella	k1gFnSc1	mozzarella
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
zeleniny	zelenina	k1gFnSc2	zelenina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
špenát	špenát	k1gInSc1	špenát
<g/>
,	,	kIx,	,
cuketa	cuketa	k1gFnSc1	cuketa
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc1	houba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lasagne	Lasagnout	k5eAaPmIp3nS	Lasagnout
se	se	k3xPyFc4	se
však	však	k9	však
vždy	vždy	k6eAd1	vždy
pečou	péct	k5eAaImIp3nP	péct
v	v	k7c6	v
troubě	trouba	k1gFnSc6	trouba
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lasagne	lasagnout	k5eAaImIp3nS	lasagnout
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
lasagní	lasagnět	k5eAaPmIp3nP	lasagnět
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lasagne	Lasagn	k1gInSc5	Lasagn
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
