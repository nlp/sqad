<s>
Smysl	smysl	k1gInSc1	smysl
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
organismu	organismus	k1gInSc2	organismus
přijímat	přijímat	k5eAaImF	přijímat
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
-	-	kIx~	-
např.	např.	kA	např.
koncentraci	koncentrace	k1gFnSc4	koncentrace
určité	určitý	k2eAgFnSc2d1	určitá
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
světla	světlo	k1gNnSc2	světlo
nebo	nebo	k8xC	nebo
charakteristiku	charakteristika	k1gFnSc4	charakteristika
vlnění	vlnění	k1gNnSc2	vlnění
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
