<s>
Smysl	smysl	k1gInSc1	smysl
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
organismu	organismus	k1gInSc2	organismus
přijímat	přijímat	k5eAaImF	přijímat
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
-	-	kIx~	-
např.	např.	kA	např.
koncentraci	koncentrace	k1gFnSc4	koncentrace
určité	určitý	k2eAgFnSc2d1	určitá
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
světla	světlo	k1gNnSc2	světlo
nebo	nebo	k8xC	nebo
charakteristiku	charakteristika	k1gFnSc4	charakteristika
vlnění	vlnění	k1gNnSc2	vlnění
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
přijímána	přijímat	k5eAaImNgFnS	přijímat
specializovaným	specializovaný	k2eAgInSc7d1	specializovaný
orgánem	orgán	k1gInSc7	orgán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
smyslový	smyslový	k2eAgInSc1d1	smyslový
orgán	orgán	k1gInSc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
či	či	k8xC	či
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
a	a	k8xC	a
stupeň	stupeň	k1gInSc1	stupeň
rozvinutí	rozvinutí	k1gNnSc2	rozvinutí
určitého	určitý	k2eAgInSc2d1	určitý
smyslu	smysl	k1gInSc2	smysl
u	u	k7c2	u
daného	daný	k2eAgInSc2d1	daný
biologického	biologický	k2eAgInSc2d1	biologický
druhu	druh	k1gInSc2	druh
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dravci	dravec	k1gMnPc1	dravec
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc1d1	dobrý
zrak	zrak	k1gInSc1	zrak
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
mají	mít	k5eAaImIp3nP	mít
výborný	výborný	k2eAgInSc4d1	výborný
čich	čich	k1gInSc4	čich
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
pět	pět	k4xCc1	pět
základních	základní	k2eAgInPc2d1	základní
smyslů	smysl	k1gInPc2	smysl
<g/>
:	:	kIx,	:
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
čich	čich	k1gInSc4	čich
<g/>
,	,	kIx,	,
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
hmat	hmat	k1gInSc4	hmat
a	a	k8xC	a
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
rozlišení	rozlišení	k1gNnSc4	rozlišení
znal	znát	k5eAaImAgMnS	znát
již	již	k9	již
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
a	a	k8xC	a
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
už	už	k9	už
i	i	k9	i
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
patrno	patrn	k2eAgNnSc1d1	patrno
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
spisu	spis	k1gInSc2	spis
"	"	kIx"	"
<g/>
O	o	k7c6	o
duši	duše	k1gFnSc6	duše
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pojetí	pojetí	k1gNnSc2	pojetí
pěti	pět	k4xCc2	pět
smyslů	smysl	k1gInPc2	smysl
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
fráze	fráze	k1gFnSc1	fráze
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
všech	všecek	k3xTgNnPc2	všecek
pět	pět	k4xCc4	pět
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
Ottova	Ottův	k2eAgFnSc1d1	Ottova
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
ovšem	ovšem	k9	ovšem
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgFnSc4d1	základní
pětici	pětice	k1gFnSc4	pětice
lze	lze	k6eAd1	lze
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
smysly	smysl	k1gInPc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgMnPc2	všecek
pět	pět	k4xCc1	pět
základních	základní	k2eAgInPc2d1	základní
smyslů	smysl	k1gInPc2	smysl
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
receptory	receptor	k1gInPc4	receptor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vnímat	vnímat	k5eAaImF	vnímat
okolí	okolí	k1gNnSc4	okolí
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
exteroreceptory	exteroreceptor	k1gInPc1	exteroreceptor
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
a	a	k8xC	a
čich	čich	k1gInSc4	čich
jsou	být	k5eAaImIp3nP	být
chemoreceptorové	chemoreceptorový	k2eAgInPc1d1	chemoreceptorový
smysly	smysl	k1gInPc1	smysl
<g/>
,	,	kIx,	,
sluch	sluch	k1gInSc1	sluch
a	a	k8xC	a
hmat	hmat	k1gInSc1	hmat
jsou	být	k5eAaImIp3nP	být
mechanoreceptorové	mechanoreceptor	k1gMnPc1	mechanoreceptor
a	a	k8xC	a
zrak	zrak	k1gInSc1	zrak
je	být	k5eAaImIp3nS	být
fotoreceptorový	fotoreceptorový	k2eAgInSc4d1	fotoreceptorový
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
smyslů	smysl	k1gInPc2	smysl
<g/>
:	:	kIx,	:
zrak	zrak	k1gInSc1	zrak
–	–	k?	–
orgán	orgán	k1gInSc1	orgán
oko	oko	k1gNnSc1	oko
sluch	sluch	k1gInSc1	sluch
–	–	k?	–
orgán	orgán	k1gInSc1	orgán
ucho	ucho	k1gNnSc1	ucho
hmat	hmat	k1gInSc1	hmat
–	–	k?	–
mechanoreceptory	mechanoreceptor	k1gInPc1	mechanoreceptor
<g/>
,	,	kIx,	,
termoreceptory	termoreceptor	k1gInPc1	termoreceptor
a	a	k8xC	a
nocireceptory	nocireceptor	k1gInPc1	nocireceptor
(	(	kIx(	(
<g/>
vnímání	vnímání	k1gNnSc1	vnímání
bolesti	bolest	k1gFnSc2	bolest
<g/>
)	)	kIx)	)
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
chuť	chuť	k1gFnSc4	chuť
–	–	k?	–
chuťové	chuťový	k2eAgInPc4d1	chuťový
pohárky	pohárek	k1gInPc4	pohárek
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
(	(	kIx(	(
<g/>
na	na	k7c6	na
jazyku	jazyk	k1gInSc6	jazyk
<g/>
)	)	kIx)	)
čich	čich	k1gInSc1	čich
–	–	k?	–
čichové	čichový	k2eAgFnPc1d1	čichová
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
nose	nos	k1gInSc6	nos
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
exteroreceptorů	exteroreceptor	k1gInPc2	exteroreceptor
<g/>
,	,	kIx,	,
interoreceptory	interoreceptor	k1gInPc1	interoreceptor
sledují	sledovat	k5eAaImIp3nP	sledovat
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
prostředí	prostředí	k1gNnSc4	prostředí
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
receptory	receptor	k1gInPc4	receptor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
sledují	sledovat	k5eAaImIp3nP	sledovat
kyselost	kyselost	k1gFnSc4	kyselost
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
vnímání	vnímání	k1gNnSc1	vnímání
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
receptorech	receptor	k1gInPc6	receptor
a	a	k8xC	a
využití	využití	k1gNnSc6	využití
setrvačných	setrvačný	k2eAgFnPc2d1	setrvačná
sil	síla	k1gFnPc2	síla
ve	v	k7c6	v
vestibulárním	vestibulární	k2eAgInSc6d1	vestibulární
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Umožňují	umožňovat	k5eAaImIp3nP	umožňovat
získat	získat	k5eAaPmF	získat
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
receptory	receptor	k1gInPc4	receptor
nejsou	být	k5eNaImIp3nP	být
smysly	smysl	k1gInPc4	smysl
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
bývají	bývat	k5eAaImIp3nP	bývat
ke	k	k7c3	k
smyslům	smysl	k1gInPc3	smysl
přiřazovány	přiřazován	k2eAgInPc4d1	přiřazován
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Smyslový	smyslový	k2eAgInSc4d1	smyslový
orgán	orgán	k1gInSc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
teploty	teplota	k1gFnSc2	teplota
–	–	k?	–
termoreceptory	termoreceptor	k1gInPc4	termoreceptor
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
Vnímání	vnímání	k1gNnSc2	vnímání
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
–	–	k?	–
mechanoreceptory	mechanoreceptor	k1gInPc1	mechanoreceptor
ve	v	k7c6	v
vestibulárním	vestibulární	k2eAgInSc6d1	vestibulární
systému	systém	k1gInSc6	systém
uvnitř	uvnitř	k7c2	uvnitř
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
ucha	ucho	k1gNnSc2	ucho
Vnímání	vnímání	k1gNnSc2	vnímání
času	čas	k1gInSc2	čas
–	–	k?	–
vnímání	vnímání	k1gNnSc1	vnímání
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
krátkodobé	krátkodobý	k2eAgInPc4d1	krátkodobý
časové	časový	k2eAgInPc4d1	časový
úseky	úsek	k1gInPc4	úsek
–	–	k?	–
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
času	čas	k1gInSc6	čas
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
nepřesná	přesný	k2eNgFnSc1d1	nepřesná
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
pouze	pouze	k6eAd1	pouze
orientační	orientační	k2eAgFnSc4d1	orientační
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
vnímání	vnímání	k1gNnSc1	vnímání
delšího	dlouhý	k2eAgInSc2d2	delší
časového	časový	k2eAgInSc2d1	časový
úseku	úsek	k1gInSc2	úsek
–	–	k?	–
přibližně	přibližně	k6eAd1	přibližně
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
smysl	smysl	k1gInSc1	smysl
je	být	k5eAaImIp3nS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
hypothalamem	hypothalam	k1gInSc7	hypothalam
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
a	a	k8xC	a
sítnicí	sítnice	k1gFnSc7	sítnice
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
nevědomá	vědomý	k2eNgFnSc1d1	nevědomá
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
smysl	smysl	k1gInSc1	smysl
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
hypothalamem	hypothalam	k1gInSc7	hypothalam
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
cirkadiánního	cirkadiánní	k2eAgInSc2d1	cirkadiánní
biorytmu	biorytmus	k1gInSc2	biorytmus
<g/>
.	.	kIx.	.
</s>
<s>
Různí	různý	k2eAgMnPc1d1	různý
živočichové	živočich	k1gMnPc1	živočich
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgInPc1d1	různý
smysly	smysl	k1gInPc1	smysl
–	–	k?	–
s	s	k7c7	s
odpovídajícími	odpovídající	k2eAgInPc7d1	odpovídající
receptory	receptor	k1gInPc7	receptor
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
–	–	k?	–
stejné	stejná	k1gFnSc2	stejná
jako	jako	k8xC	jako
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
někteří	některý	k3yIgMnPc1	některý
živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
žádnému	žádný	k3yNgInSc3	žádný
smyslu	smysl	k1gInSc3	smysl
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
–	–	k?	–
orientace	orientace	k1gFnSc1	orientace
některých	některý	k3yIgMnPc2	některý
ptáků	pták	k1gMnPc2	pták
nebo	nebo	k8xC	nebo
mořských	mořský	k2eAgFnPc2d1	mořská
želv	želva	k1gFnPc2	želva
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgInP	objevit
magnetoreceptory	magnetoreceptor	k1gInPc1	magnetoreceptor
i	i	k8xC	i
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
opic	opice	k1gFnPc2	opice
Vnímání	vnímání	k1gNnSc1	vnímání
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
–	–	k?	–
řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
elekt	elekt	k1gInSc1	elekt
<g/>
.	.	kIx.	.
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
paryby	paryba	k1gFnPc1	paryba
Vnímání	vnímání	k1gNnSc2	vnímání
tepelného	tepelný	k2eAgNnSc2d1	tepelné
záření	záření	k1gNnSc2	záření
–	–	k?	–
tepločivné	tepločivný	k2eAgFnSc2d1	tepločivný
jamky	jamka	k1gFnSc2	jamka
u	u	k7c2	u
hadů	had	k1gMnPc2	had
pro	pro	k7c4	pro
nalezení	nalezení	k1gNnSc4	nalezení
kořisti	kořist	k1gFnSc2	kořist
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
Rostliny	rostlina	k1gFnSc2	rostlina
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
Světlo	světlo	k1gNnSc4	světlo
–	–	k?	–
růst	růst	k1gInSc1	růst
nadzemních	nadzemní	k2eAgFnPc2d1	nadzemní
částí	část	k1gFnPc2	část
rostliny	rostlina	k1gFnSc2	rostlina
za	za	k7c7	za
světlem	světlo	k1gNnSc7	světlo
Gravitační	gravitační	k2eAgFnSc2d1	gravitační
pole	pole	k1gFnSc2	pole
–	–	k?	–
růst	růst	k1gInSc1	růst
kořenů	kořen	k1gInPc2	kořen
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
zemské	zemský	k2eAgFnSc2d1	zemská
tíže	tíž	k1gFnSc2	tíž
(	(	kIx(	(
<g/>
gravitace	gravitace	k1gFnSc1	gravitace
<g/>
)	)	kIx)	)
Čas	čas	k1gInSc1	čas
délky	délka	k1gFnSc2	délka
přibližně	přibližně	k6eAd1	přibližně
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
–	–	k?	–
řízení	řízení	k1gNnSc1	řízení
cirkadiánního	cirkadiánní	k2eAgInSc2d1	cirkadiánní
biorytmu	biorytmus	k1gInSc2	biorytmus
</s>
