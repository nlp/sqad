<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
politická	politický	k2eAgFnSc1d1	politická
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
od	od	k7c2	od
posledního	poslední	k2eAgNnSc2d1	poslední
rozšíření	rozšíření	k1gNnSc2	rozšíření
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
tvoří	tvořit	k5eAaImIp3nS	tvořit
28	[number]	k4	28
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
s	s	k7c7	s
510,3	[number]	k4	510,3
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
;	;	kIx,	;
přibližně	přibližně	k6eAd1	přibližně
7,3	[number]	k4	7,3
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
