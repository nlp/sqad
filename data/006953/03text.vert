<s>
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Song	song	k1gInSc1	song
Remains	Remains	k1gInSc1	Remains
the	the	k?	the
Same	Sam	k1gMnSc5	Sam
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
píseň	píseň	k1gFnSc4	píseň
od	od	k7c2	od
anglické	anglický	k2eAgFnSc2d1	anglická
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
úvodní	úvodní	k2eAgFnSc7d1	úvodní
písní	píseň	k1gFnSc7	píseň
alba	album	k1gNnSc2	album
Houses	Houses	k1gMnSc1	Houses
of	of	k?	of
the	the	k?	the
Holy	hola	k1gFnSc2	hola
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
vícestopá	vícestopý	k2eAgFnSc1d1	vícestopý
nahrávka	nahrávka	k1gFnSc1	nahrávka
nespoutané	spoutaný	k2eNgFnSc2d1	nespoutaná
kytary	kytara	k1gFnSc2	kytara
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Page	Pag	k1gFnSc2	Pag
a	a	k8xC	a
vokály	vokál	k1gInPc1	vokál
zpěváka	zpěvák	k1gMnSc2	zpěvák
Roberta	Robert	k1gMnSc2	Robert
Planta	planta	k1gFnSc1	planta
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
Plantův	Plantův	k2eAgInSc1d1	Plantův
hold	hold	k1gInSc1	hold
world	world	k1gMnSc1	world
music	music	k1gMnSc1	music
<g/>
,	,	kIx,	,
odrážející	odrážející	k2eAgMnSc1d1	odrážející
jeho	jeho	k3xOp3gNnSc4	jeho
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgFnSc1d1	univerzální
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgFnSc1d1	studiová
verze	verze	k1gFnSc1	verze
měla	mít	k5eAaImAgFnS	mít
mírně	mírně	k6eAd1	mírně
zrychlené	zrychlený	k2eAgInPc4d1	zrychlený
vokály	vokál	k1gInPc4	vokál
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
vyšších	vysoký	k2eAgFnPc2d2	vyšší
poloh	poloha	k1gFnPc2	poloha
Plantova	Plantův	k2eAgInSc2d1	Plantův
hlasu	hlas	k1gInSc2	hlas
<g/>
,	,	kIx,	,
než	než	k8xS	než
obvykle	obvykle	k6eAd1	obvykle
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
pracovní	pracovní	k2eAgInSc4d1	pracovní
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
The	The	k1gMnSc5	The
Overture	Overtur	k1gMnSc5	Overtur
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Úvod	úvod	k1gInSc1	úvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
Plant	planta	k1gFnPc2	planta
napsal	napsat	k5eAaPmAgMnS	napsat
text	text	k1gInSc4	text
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Campaign	Campaign	k1gMnSc1	Campaign
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Kampaň	kampaň	k1gFnSc1	kampaň
nebo	nebo	k8xC	nebo
Tažení	tažení	k1gNnSc1	tažení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
však	však	k9	však
kapela	kapela	k1gFnSc1	kapela
shodla	shodnout	k5eAaPmAgFnS	shodnout
na	na	k7c6	na
názvu	název	k1gInSc6	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Song	song	k1gInSc1	song
Remains	Remains	k1gInSc1	Remains
the	the	k?	the
Same	Sam	k1gMnSc5	Sam
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Píseň	píseň	k1gFnSc1	píseň
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
nese	nést	k5eAaImIp3nS	nést
některé	některý	k3yIgInPc4	některý
podobné	podobný	k2eAgInPc4d1	podobný
znaky	znak	k1gInPc4	znak
jako	jako	k8xS	jako
instrumentálka	instrumentálka	k1gFnSc1	instrumentálka
"	"	kIx"	"
<g/>
Tinker	Tinker	k1gMnSc1	Tinker
Tailor	tailor	k1gMnSc1	tailor
Soldier	Soldier	k1gMnSc1	Soldier
Sailor	Sailor	k1gMnSc1	Sailor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Page	Page	k1gFnSc4	Page
nahrál	nahrát	k5eAaBmAgMnS	nahrát
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Song	song	k1gInSc1	song
Remains	Remains	k1gInSc1	Remains
the	the	k?	the
Same	Sam	k1gMnSc5	Sam
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgNnP	být
uvedena	uvést	k5eAaPmNgNnP	uvést
na	na	k7c6	na
filmu	film	k1gInSc6	film
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
skupiny	skupina	k1gFnSc2	skupina
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
a	a	k8xC	a
současně	současně	k6eAd1	současně
bylo	být	k5eAaImAgNnS	být
vydáno	vydán	k2eAgNnSc1d1	vydáno
album	album	k1gNnSc1	album
soundtrack	soundtrack	k1gInSc1	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
titul	titul	k1gInSc1	titul
písně	píseň	k1gFnSc2	píseň
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
film	film	k1gInSc4	film
i	i	k9	i
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Jason	Jason	k1gInSc1	Jason
Bonham	Bonham	k1gInSc1	Bonham
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
zemřelého	zemřelý	k2eAgMnSc2d1	zemřelý
bubeníka	bubeník	k1gMnSc2	bubeník
skupiny	skupina	k1gFnSc2	skupina
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
Johna	John	k1gMnSc4	John
Bonhama	Bonham	k1gMnSc4	Bonham
<g/>
,	,	kIx,	,
nahrál	nahrát	k5eAaPmAgMnS	nahrát
cover	cover	k1gMnSc1	cover
verzi	verze	k1gFnSc4	verze
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
In	In	k1gFnSc2	In
the	the	k?	the
Name	Nam	k1gFnSc2	Nam
of	of	k?	of
My	my	k3xPp1nPc1	my
Father	Fathra	k1gFnPc2	Fathra
-	-	kIx~	-
The	The	k1gFnSc1	The
Zepset	Zepset	k1gInSc1	Zepset
Progressive	Progressiev	k1gFnSc2	Progressiev
metalová	metalový	k2eAgFnSc1d1	metalová
skupina	skupina	k1gFnSc1	skupina
Dream	Dream	k1gInSc4	Dream
Theater	Theater	k1gInSc4	Theater
vydala	vydat	k5eAaPmAgFnS	vydat
směs	směs	k1gFnSc1	směs
písní	píseň	k1gFnSc7	píseň
Led	led	k1gInSc4	led
Zeppelin	Zeppelin	k2eAgInSc4d1	Zeppelin
na	na	k7c6	na
svém	své	k1gNnSc6	své
EP	EP	kA	EP
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
A	a	k8xC	a
Change	change	k1gFnSc1	change
of	of	k?	of
Seasons	Seasonsa	k1gFnPc2	Seasonsa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
otvírala	otvírat	k5eAaImAgFnS	otvírat
část	část	k1gFnSc1	část
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Song	song	k1gInSc1	song
Remains	Remains	k1gInSc1	Remains
the	the	k?	the
Same	Sam	k1gMnSc5	Sam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Dread	Dread	k1gInSc4	Dread
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
paroduje	parodovat	k5eAaImIp3nS	parodovat
Led	led	k1gInSc4	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
,	,	kIx,	,
nahrála	nahrát	k5eAaBmAgFnS	nahrát
tuto	tento	k3xDgFnSc4	tento
píseň	píseň	k1gFnSc4	píseň
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
5.000.000	[number]	k4	5.000.000
<g/>
.	.	kIx.	.
</s>
<s>
Vydala	vydat	k5eAaPmAgFnS	vydat
též	též	k9	též
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
The	The	k1gFnSc2	The
Song	song	k1gInSc1	song
Remains	Remains	k1gInSc1	Remains
Insane	Insan	k1gMnSc5	Insan
jako	jako	k8xS	jako
slovní	slovní	k2eAgFnSc4d1	slovní
hříčku	hříčka	k1gFnSc4	hříčka
narážející	narážející	k2eAgFnSc2d1	narážející
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
:	:	kIx,	:
Dazed	Dazed	k1gInSc1	Dazed
and	and	k?	and
Confused	Confused	k1gInSc1	Confused
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Stories	Stories	k1gMnSc1	Stories
Behind	Behind	k1gMnSc1	Behind
Every	Evera	k1gFnSc2	Evera
Song	song	k1gInSc1	song
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Chris	Chris	k1gInSc1	Chris
Welch	Welch	k1gInSc4	Welch
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-56025-818-7	[number]	k4	1-56025-818-7
The	The	k1gFnSc7	The
Complete	Comple	k1gNnSc2	Comple
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Music	Music	k1gMnSc1	Music
of	of	k?	of
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
Dave	Dav	k1gInSc5	Dav
Lewis	Lewis	k1gFnPc7	Lewis
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7119-3528-9	[number]	k4	0-7119-3528-9
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gFnSc2	The
Song	song	k1gInSc1	song
Remains	Remains	k1gInSc1	Remains
the	the	k?	the
Same	Sam	k1gMnSc5	Sam
(	(	kIx(	(
<g/>
song	song	k1gInSc1	song
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
