<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
spoluautorem	spoluautor	k1gMnSc7	spoluautor
prvního	první	k4xOgInSc2	první
projektu	projekt	k1gInSc2	projekt
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bohumilem	Bohumil	k1gMnSc7	Bohumil
Beladou	Belada	k1gFnSc7	Belada
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
