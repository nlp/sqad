<s>
FK	FK	kA
Obolon	Obolon	k1gInSc1
Kyjev	Kyjev	k1gInSc1
</s>
<s>
FK	FK	kA
Obolon	Obolon	k1gInSc1
KyjevNázev	KyjevNázev	k1gFnSc1
</s>
<s>
FK	FK	kA
Obolon	Obolon	k1gInSc1
Kyjev	Kyjev	k1gInSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Pyvovary	Pyvovar	k1gInPc1
Země	zem	k1gFnSc2
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
Město	město	k1gNnSc1
</s>
<s>
Kyjev	Kyjev	k1gInSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
1992	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
2013	#num#	k4
Barvy	barva	k1gFnSc2
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Premier	Premier	k1gMnSc1
Liha	Lih	k1gInSc2
Stadion	stadion	k1gInSc1
</s>
<s>
Obolon	Obolon	k1gInSc1
Arena	Areen	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
30	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
5	#num#	k4
100	#num#	k4
Vedení	vedení	k1gNnPc2
Trenér	trenér	k1gMnSc1
</s>
<s>
Serhiy	Serhia	k1gFnPc1
Konyushenko	Konyushenka	k1gFnSc5
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
FK	FK	kA
Obolon	Obolon	k1gInSc1
Kyjev	Kyjev	k1gInSc1
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
<g/>
:	:	kIx,
Ф	Ф	k?
к	к	k?
«	«	k?
<g/>
О	О	k?
<g/>
»	»	k?
К	К	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
profesionální	profesionální	k2eAgInSc1d1
ukrajinský	ukrajinský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
sídlící	sídlící	k2eAgInSc1d1
ve	v	k7c6
městě	město	k1gNnSc6
Kyjev	Kyjev	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
historie	historie	k1gFnSc2
pravidelně	pravidelně	k6eAd1
hrával	hrávat	k5eAaImAgMnS
v	v	k7c6
ukrajinské	ukrajinský	k2eAgFnSc6d1
nejvyšší	vysoký	k2eAgFnSc6d3
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
,	,	kIx,
zanikl	zaniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrával	odehrávat	k5eAaImAgInS
klub	klub	k1gInSc1
na	na	k7c6
stadionu	stadion	k1gInSc6
Obolon	Obolon	k1gInSc1
Arena	Areno	k1gNnSc2
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
5	#num#	k4
100	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1992	#num#	k4
–	–	k?
FK	FK	kA
Zmina	Zmina	k?
Kyjev	Kyjev	k1gInSc1
(	(	kIx(
<g/>
Futbolnyj	Futbolnyj	k1gInSc1
klub	klub	k1gInSc1
Zmina	Zmina	k?
Kyjev	Kyjev	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
–	–	k?
FK	FK	kA
Zmina-Obolon	Zmina-Obolon	k1gInSc1
Kyjev	Kyjev	k1gInSc1
(	(	kIx(
<g/>
Futbolnyj	Futbolnyj	k1gInSc1
klub	klub	k1gInSc1
Zmina-Obolon	Zmina-Obolon	k1gInSc1
Kyjev	Kyjev	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1995	#num#	k4
–	–	k?
FK	FK	kA
Obolon	Obolon	k1gInSc1
Kyjev	Kyjev	k1gInSc1
(	(	kIx(
<g/>
Futbolnyj	Futbolnyj	k1gInSc1
klub	klub	k1gInSc1
Obolon	Obolon	k1gInSc1
Kyjev	Kyjev	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
–	–	k?
FK	FK	kA
Obolon	Obolon	k1gInSc1
PVO	PVO	kA
Kyjev	Kyjev	k1gInSc1
(	(	kIx(
<g/>
Futbolnyj	Futbolnyj	k1gInSc1
klub	klub	k1gInSc1
Obolon	Obolon	k1gInSc4
PVO	PVO	kA
Kyjev	Kyjev	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
–	–	k?
FK	FK	kA
Obolon	Obolon	k1gInSc1
Kyjev	Kyjev	k1gInSc1
(	(	kIx(
<g/>
Futbolnyj	Futbolnyj	k1gInSc1
klub	klub	k1gInSc1
Obolon	Obolon	k1gInSc1
Kyjev	Kyjev	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	Z	kA
-	-	kIx~
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
V	v	k7c6
-	-	kIx~
výhry	výhra	k1gFnPc4
<g/>
,	,	kIx,
R	R	kA
-	-	kIx~
remízy	remíz	k1gInPc1
<g/>
,	,	kIx,
P	P	kA
-	-	kIx~
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
-	-	kIx~
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
-	-	kIx~
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
-	-	kIx~
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
-	-	kIx~
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
AAFU	AAFU	kA
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
</s>
<s>
52214533217	#num#	k4
<g/>
+	+	kIx~
<g/>
1533	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
AAFU	AAFU	kA
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
</s>
<s>
53023436017	#num#	k4
<g/>
+	+	kIx~
<g/>
4373	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
34022996035	#num#	k4
<g/>
+	+	kIx~
<g/>
2575	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
330151143417	#num#	k4
<g/>
+	+	kIx~
<g/>
1756	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
33015784728	#num#	k4
<g/>
+	+	kIx~
<g/>
1952	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
C	C	kA
</s>
<s>
32620424518	#num#	k4
<g/>
+	+	kIx~
<g/>
2764	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Perša	Perš	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
234512172352-2927	234512172352-2927	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
32821435114	#num#	k4
<g/>
+	+	kIx~
<g/>
3767	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Perša	Perš	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
23418884926	#num#	k4
<g/>
+	+	kIx~
<g/>
2362	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Vyšča	Vyšč	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
13077163245-1328	13077163245-1328	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Vyšča	Vyšč	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
130118113435-141	130118113435-141	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Vyšča	Vyšč	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
13049171843-2521	13049171843-2521	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Perša	Perš	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
23422665116	#num#	k4
<g/>
+	+	kIx~
<g/>
3572	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Perša	Perš	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
23623494727	#num#	k4
<g/>
+	+	kIx~
<g/>
2073	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Perša	Perš	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
238226106742	#num#	k4
<g/>
+	+	kIx~
<g/>
2572	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Perša	Perš	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
23219677440	#num#	k4
<g/>
+	+	kIx~
<g/>
3463	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Premjer-liha	Premjer-liha	k1gFnSc1
</s>
<s>
13094172650-2431	13094172650-2431	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Premjer-liha	Premjer-liha	k1gFnSc1
</s>
<s>
13097142638-1234	13097142638-1234	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Premjer-liha	Premjer-liha	k1gFnSc1
</s>
<s>
13049171742-2521	13049171742-2521	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Perša	Perš	k2eAgFnSc1d1
liha	liha	k1gFnSc1
</s>
<s>
23457221928-922	23457221928-922	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obolon-	Obolon-	k?
<g/>
2	#num#	k4
Kyjev	Kyjev	k1gInSc1
</s>
<s>
Obolon-	Obolon-	k?
<g/>
2	#num#	k4
Kyjev	Kyjev	k1gInSc1
byl	být	k5eAaImAgInS
rezervní	rezervní	k2eAgInSc4d1
tým	tým	k1gInSc4
kyjevského	kyjevský	k2eAgInSc2d1
Obolonu	Obolon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšího	veliký	k2eAgInSc2d3
úspěchu	úspěch	k1gInSc2
dosáhl	dosáhnout	k5eAaPmAgInS
klub	klub	k1gInSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c4
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
<g/>
)	)	kIx)
umístil	umístit	k5eAaPmAgMnS
na	na	k7c4
2	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	Z	kA
-	-	kIx~
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
V	v	k7c6
-	-	kIx~
výhry	výhra	k1gFnPc4
<g/>
,	,	kIx,
R	R	kA
-	-	kIx~
remízy	remíz	k1gInPc1
<g/>
,	,	kIx,
P	P	kA
-	-	kIx~
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
-	-	kIx~
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
-	-	kIx~
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
-	-	kIx~
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
-	-	kIx~
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
(	(	kIx(
<g/>
1999	#num#	k4
–	–	k?
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
32618534415	#num#	k4
<g/>
+	+	kIx~
<g/>
2959	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
...	...	k?
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
334811153456-2235	334811153456-2235	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
330107134145-437	330107134145-437	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
33091292530-539	33091292530-539	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
Liha	Lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
32847172149-2819	32847172149-2819	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
32898113136-535	32898113136-535	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
32859142645-1924	32859142645-1924	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
33015695036	#num#	k4
<g/>
+	+	kIx~
<g/>
1451	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
33254232030-1019	33254232030-1019	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rezervní	rezervní	k2eAgNnSc1d1
mužstvo	mužstvo	k1gNnSc1
působilo	působit	k5eAaImAgNnS
v	v	k7c6
letech	let	k1gInPc6
2009	#num#	k4
–	–	k?
2012	#num#	k4
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
juniorské	juniorský	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Druha	druh	k1gMnSc4
liha	lihus	k1gMnSc4
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
32053122130-918	32053122130-918	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
О	О	k?
К	К	k?
(	(	kIx(
<g/>
UKR	UKR	kA
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
wildstat	wildstat	k1gInSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
О	О	k?
<g/>
2	#num#	k4
К	К	k?
(	(	kIx(
<g/>
UKR	UKR	kA
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
wildstat	wildstat	k1gInSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
