<s>
Želva	želva	k1gFnSc1	želva
obrovská	obrovský	k2eAgFnSc1d1	obrovská
(	(	kIx(	(
<g/>
Dipsochelys	Dipsochelysa	k1gFnPc2	Dipsochelysa
gigantea	gigantea	k6eAd1	gigantea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
suchozemská	suchozemský	k2eAgFnSc1d1	suchozemská
želva	želva	k1gFnSc1	želva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Seychelských	seychelský	k2eAgInPc6d1	seychelský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejdéle	dlouho	k6eAd3	dlouho
žijící	žijící	k2eAgMnPc4d1	žijící
tvory	tvor	k1gMnPc4	tvor
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
krunýře	krunýř	k1gInSc2	krunýř
dospělé	dospělý	k2eAgFnSc2d1	dospělá
želvy	želva	k1gFnSc2	želva
je	být	k5eAaImIp3nS	být
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
200	[number]	k4	200
kg	kg	kA	kg
a	a	k8xC	a
dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
až	až	k9	až
160	[number]	k4	160
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k8xC	tak
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
suchozemskou	suchozemský	k2eAgFnSc7d1	suchozemská
želvou	želva	k1gFnSc7	želva
<g/>
.	.	kIx.	.
</s>
<s>
Karapax	Karapax	k1gInSc1	Karapax
má	mít	k5eAaImIp3nS	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
vysoké	vysoký	k2eAgFnSc2d1	vysoká
kopule	kopule	k1gFnSc2	kopule
<g/>
.	.	kIx.	.
</s>
<s>
Mohutné	mohutný	k2eAgFnPc1d1	mohutná
nohy	noha	k1gFnPc1	noha
slouží	sloužit	k5eAaImIp3nP	sloužit
také	také	k9	také
jako	jako	k9	jako
opora	opora	k1gFnSc1	opora
těžkého	těžký	k2eAgNnSc2d1	těžké
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
želvě	želva	k1gFnSc3	želva
využít	využít	k5eAaPmF	využít
větví	větev	k1gFnSc7	větev
až	až	k9	až
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
od	od	k7c2	od
země	zem	k1gFnSc2	zem
jako	jako	k8xS	jako
zdroje	zdroj	k1gInSc2	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
pomalé	pomalý	k2eAgFnPc1d1	pomalá
a	a	k8xC	a
opatrné	opatrný	k2eAgFnPc1d1	opatrná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc4d1	schopna
značné	značný	k2eAgFnPc4d1	značná
rychlosti	rychlost	k1gFnPc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vynikajícími	vynikající	k2eAgMnPc7d1	vynikající
plavci	plavec	k1gMnPc7	plavec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
sběru	sběr	k1gInSc2	sběr
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
ostrovech	ostrov	k1gInPc6	ostrov
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
vyhubeny	vyhuben	k2eAgFnPc1d1	vyhubena
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
vyhubením	vyhubení	k1gNnSc7	vyhubení
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
požadováno	požadován	k2eAgNnSc4d1	požadováno
povolení	povolení	k1gNnSc4	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
<g/>
,	,	kIx,	,
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
oblastech	oblast	k1gFnPc6	oblast
Seychelských	seychelský	k2eAgInPc2d1	seychelský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
ostrova	ostrov	k1gInSc2	ostrov
Aldabra	Aldabro	k1gNnSc2	Aldabro
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
populace	populace	k1gFnSc1	populace
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Changuu	Changuus	k1gInSc2	Changuus
a	a	k8xC	a
chované	chovaný	k2eAgFnSc2d1	chovaná
populace	populace	k1gFnSc2	populace
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
ochranářských	ochranářský	k2eAgInPc6d1	ochranářský
parcích	park	k1gInPc6	park
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Mauricius	Mauricius	k1gMnSc1	Mauricius
a	a	k8xC	a
Rodrigues	Rodrigues	k1gMnSc1	Rodrigues
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
kamenité	kamenitý	k2eAgFnPc4d1	kamenitá
stepi	step	k1gFnPc4	step
a	a	k8xC	a
polopouště	polopoušť	k1gFnPc4	polopoušť
a	a	k8xC	a
travnatá	travnatý	k2eAgNnPc4d1	travnaté
území	území	k1gNnPc4	území
s	s	k7c7	s
křovinami	křovina	k1gFnPc7	křovina
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
individuálně	individuálně	k6eAd1	individuálně
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
stádech	stádo	k1gNnPc6	stádo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c6	na
otevřených	otevřený	k2eAgFnPc6d1	otevřená
pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrabávají	vyhrabávat	k5eAaImIp3nP	vyhrabávat
si	se	k3xPyFc3	se
podzemní	podzemní	k2eAgFnPc1d1	podzemní
nory	nora	k1gFnPc1	nora
nebo	nebo	k8xC	nebo
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
v	v	k7c6	v
bažinách	bažina	k1gFnPc6	bažina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
chladnou	chladný	k2eAgFnSc4d1	chladná
hlavu	hlava	k1gFnSc4	hlava
během	během	k7c2	během
horkého	horký	k2eAgInSc2d1	horký
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tráví	trávit	k5eAaImIp3nP	trávit
čas	čas	k1gInSc4	čas
hledáním	hledání	k1gNnSc7	hledání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
plody	plod	k1gInPc4	plod
a	a	k8xC	a
výhonky	výhonek	k1gInPc4	výhonek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
živočišnými	živočišný	k2eAgInPc7d1	živočišný
zbytky	zbytek	k1gInPc7	zbytek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
býložravci	býložravec	k1gMnPc1	býložravec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gFnPc3	on
dopřáno	dopřát	k5eAaPmNgNnS	dopřát
jen	jen	k6eAd1	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
jí	on	k3xPp3gFnSc7	on
přijímají	přijímat	k5eAaImIp3nP	přijímat
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
jsou	být	k5eAaImIp3nP	být
živeny	živit	k5eAaImNgInP	živit
často	často	k6eAd1	často
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
jablka	jablko	k1gNnPc1	jablko
a	a	k8xC	a
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
rostlinnými	rostlinný	k2eAgFnPc7d1	rostlinná
peletami	peleta	k1gFnPc7	peleta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
únorem	únor	k1gInSc7	únor
a	a	k8xC	a
květnem	květen	k1gInSc7	květen
samice	samice	k1gFnSc2	samice
kladou	klást	k5eAaImIp3nP	klást
9	[number]	k4	9
až	až	k9	až
25	[number]	k4	25
měkkých	měkký	k2eAgNnPc2d1	měkké
vajec	vejce	k1gNnPc2	vejce
do	do	k7c2	do
důlku	důlek	k1gInSc2	důlek
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
pak	pak	k6eAd1	pak
zahrabou	zahrabat	k5eAaPmIp3nP	zahrabat
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
půdu	půda	k1gFnSc4	půda
udušou	udusat	k5eAaPmIp3nP	udusat
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
méně	málo	k6eAd2	málo
než	než	k8xS	než
poloviny	polovina	k1gFnPc1	polovina
vajec	vejce	k1gNnPc2	vejce
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
mláďata	mládě	k1gNnPc1	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Snůšek	snůška	k1gFnPc2	snůška
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
za	za	k7c4	za
rok	rok	k1gInSc4	rok
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
6	[number]	k4	6
až	až	k9	až
7	[number]	k4	7
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
