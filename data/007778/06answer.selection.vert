<s>
Želva	želva	k1gFnSc1	želva
obrovská	obrovský	k2eAgFnSc1d1	obrovská
(	(	kIx(	(
<g/>
Dipsochelys	Dipsochelysa	k1gFnPc2	Dipsochelysa
gigantea	gigantea	k6eAd1	gigantea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
suchozemská	suchozemský	k2eAgFnSc1d1	suchozemská
želva	želva	k1gFnSc1	želva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Seychelských	seychelský	k2eAgInPc6d1	seychelský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
