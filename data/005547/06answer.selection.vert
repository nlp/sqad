<s>
Amyláza	amyláza	k1gFnSc1	amyláza
je	být	k5eAaImIp3nS	být
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
štěpení	štěpení	k1gNnSc4	štěpení
škrobu	škrob	k1gInSc2	škrob
na	na	k7c4	na
jednodušší	jednoduchý	k2eAgInPc4d2	jednodušší
sacharidy	sacharid	k1gInPc4	sacharid
<g/>
.	.	kIx.	.
</s>
