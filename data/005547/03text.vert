<s>
Amyláza	amyláza	k1gFnSc1	amyláza
je	být	k5eAaImIp3nS	být
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
štěpení	štěpení	k1gNnSc4	štěpení
škrobu	škrob	k1gInSc2	škrob
na	na	k7c4	na
jednodušší	jednoduchý	k2eAgInPc4d2	jednodušší
sacharidy	sacharid	k1gInPc4	sacharid
<g/>
.	.	kIx.	.
</s>
<s>
Amyláza	amyláza	k1gFnSc1	amyláza
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
hydrolázy	hydroláza	k1gFnPc4	hydroláza
<g/>
.	.	kIx.	.
</s>
<s>
Amyláza	amyláza	k1gFnSc1	amyláza
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
izolován	izolován	k2eAgInSc1d1	izolován
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
diastáza	diastáza	k1gFnSc1	diastáza
Anselmem	Anselmo	k1gNnSc7	Anselmo
Payenem	Payen	k1gInSc7	Payen
roku	rok	k1gInSc2	rok
1833	[number]	k4	1833
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
typy	typ	k1gInPc1	typ
amylázy	amyláza	k1gFnSc2	amyláza
:	:	kIx,	:
α	α	k?	α
<g/>
,	,	kIx,	,
β	β	k?	β
a	a	k8xC	a
γ	γ	k?	γ
<g/>
.	.	kIx.	.
α	α	k?	α
produkovaná	produkovaný	k2eAgFnSc1d1	produkovaná
slinnými	slinný	k2eAgFnPc7d1	slinná
žlázami	žláza	k1gFnPc7	žláza
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ptyalin	ptyalin	k1gInSc1	ptyalin
<g/>
.	.	kIx.	.
</s>
<s>
Ptyalin	ptyalin	k1gInSc4	ptyalin
obsažený	obsažený	k2eAgInSc4d1	obsažený
ve	v	k7c6	v
slinách	slina	k1gFnPc6	slina
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
potravou	potrava	k1gFnSc7	potrava
začne	začít	k5eAaPmIp3nS	začít
štěpit	štěpit	k5eAaImF	štěpit
škrob	škrob	k1gInSc4	škrob
a	a	k8xC	a
složené	složený	k2eAgInPc1d1	složený
sacharidy	sacharid	k1gInPc1	sacharid
<g/>
.	.	kIx.	.
α	α	k?	α
produkuje	produkovat	k5eAaImIp3nS	produkovat
také	také	k9	také
pankreas	pankreas	k1gInSc1	pankreas
(	(	kIx(	(
<g/>
slinivka	slinivka	k1gFnSc1	slinivka
břišní	břišní	k2eAgFnSc1d1	břišní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
dalším	další	k2eAgNnSc6d1	další
štěpení	štěpení	k1gNnSc6	štěpení
cukrů	cukr	k1gInPc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Amylázy	amyláza	k1gFnPc1	amyláza
(	(	kIx(	(
<g/>
α	α	k?	α
a	a	k8xC	a
β	β	k?	β
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
ve	v	k7c6	v
sladu	slad	k1gInSc6	slad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
během	během	k7c2	během
rmutování	rmutování	k1gNnSc2	rmutování
sladiny	sladina	k1gFnSc2	sladina
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
obilný	obilný	k2eAgInSc4d1	obilný
škrob	škrob	k1gInSc4	škrob
na	na	k7c4	na
dextrin	dextrin	k1gInSc4	dextrin
a	a	k8xC	a
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
cukry	cukr	k1gInPc1	cukr
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
maltózu	maltóza	k1gFnSc4	maltóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
později	pozdě	k6eAd2	pozdě
zkvasit	zkvasit	k5eAaPmF	zkvasit
na	na	k7c4	na
alkohol	alkohol	k1gInSc4	alkohol
<g/>
;	;	kIx,	;
podobně	podobně	k6eAd1	podobně
působí	působit	k5eAaImIp3nS	působit
sladové	sladový	k2eAgFnPc4d1	sladová
amylázy	amyláza	k1gFnPc4	amyláza
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
whisky	whisky	k1gFnSc2	whisky
<g/>
.	.	kIx.	.
α	α	k?	α
pracuje	pracovat	k5eAaImIp3nS	pracovat
při	při	k7c6	při
teplotním	teplotní	k2eAgNnSc6d1	teplotní
optimu	optimum	k1gNnSc6	optimum
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
při	při	k7c6	při
80	[number]	k4	80
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
inaktivuje	inaktivovat	k5eAaBmIp3nS	inaktivovat
<g/>
.	.	kIx.	.
</s>
<s>
Optimum	optimum	k1gNnSc1	optimum
β	β	k?	β
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
62	[number]	k4	62
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
inaktivuje	inaktivovat	k5eAaBmIp3nS	inaktivovat
se	se	k3xPyFc4	se
při	při	k7c6	při
75	[number]	k4	75
°	°	k?	°
<g/>
C.	C.	kA	C.
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
při	při	k7c6	při
rmutování	rmutování	k1gNnSc6	rmutování
sladina	sladina	k1gFnSc1	sladina
postupně	postupně	k6eAd1	postupně
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ukazatelem	ukazatel	k1gInSc7	ukazatel
aktivity	aktivita	k1gFnSc2	aktivita
amylázy	amyláza	k1gFnSc2	amyláza
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
diastatická	diastatický	k2eAgFnSc1d1	diastatický
mohutnost	mohutnost	k1gFnSc1	mohutnost
<g/>
.	.	kIx.	.
</s>
<s>
Sladové	sladový	k2eAgFnPc1d1	sladová
amylázy	amyláza	k1gFnPc1	amyláza
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
lihu	líh	k1gInSc2	líh
aj.	aj.	kA	aj.
lihovin	lihovina	k1gFnPc2	lihovina
z	z	k7c2	z
brambor	brambora	k1gFnPc2	brambora
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
rozštěpit	rozštěpit	k5eAaPmF	rozštěpit
i	i	k9	i
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
jen	jen	k9	jen
obilný	obilný	k2eAgInSc4d1	obilný
škrob	škrob	k1gInSc4	škrob
<g/>
.	.	kIx.	.
</s>
<s>
Gorvinovy	Gorvinův	k2eAgFnPc1d1	Gorvinův
stránky	stránka	k1gFnPc1	stránka
<g/>
:	:	kIx,	:
Užití	užití	k1gNnPc1	užití
sladu	slad	k1gInSc2	slad
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
</s>
