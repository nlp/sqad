<s>
Monako	Monako	k1gNnSc1	Monako
-	-	kIx~	-
Monacké	monacký	k2eAgNnSc1d1	Monacké
knížectví	knížectví	k1gNnSc1	knížectví
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Principauté	Principautý	k2eAgNnSc1d1	Principauté
de	de	k?	de
Monaco	Monaco	k1gNnSc1	Monaco
nebo	nebo	k8xC	nebo
Monaco	Monaco	k1gNnSc1	Monaco
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
na	na	k7c6	na
středomořském	středomořský	k2eAgNnSc6d1	středomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
francouzské	francouzský	k2eAgFnSc2d1	francouzská
riviery	riviera	k1gFnSc2	riviera
-	-	kIx~	-
Azurového	azurový	k2eAgNnSc2d1	azurové
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2,02	[number]	k4	2,02
km	km	kA	km
<g/>
2	[number]	k4	2
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
37	[number]	k4	37
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
nejmenší	malý	k2eAgInSc4d3	nejmenší
stát	stát	k1gInSc4	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
současně	současně	k6eAd1	současně
země	země	k1gFnSc1	země
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
hustotou	hustota	k1gFnSc7	hustota
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
jediným	jediný	k2eAgMnSc7d1	jediný
sousedem	soused	k1gMnSc7	soused
je	být	k5eAaImIp3nS	být
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Pevninská	pevninský	k2eAgFnSc1d1	pevninská
hranice	hranice	k1gFnSc1	hranice
činí	činit	k5eAaImIp3nS	činit
4,4	[number]	k4	4,4
km	km	kA	km
a	a	k8xC	a
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
pobřežím	pobřeží	k1gNnSc7	pobřeží
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
4,1	[number]	k4	4,1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgMnSc1d1	starý
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
bohatými	bohatý	k2eAgMnPc7d1	bohatý
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
turisty	turist	k1gMnPc7	turist
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
ekonomickým	ekonomický	k2eAgFnPc3d1	ekonomická
mocnostem	mocnost	k1gFnPc3	mocnost
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
tvořili	tvořit	k5eAaImAgMnP	tvořit
milionáři	milionář	k1gMnPc1	milionář
30	[number]	k4	30
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
byly	být	k5eAaImAgInP	být
například	například	k6eAd1	například
Ženeva	Ženeva	k1gFnSc1	Ženeva
nebo	nebo	k8xC	nebo
Curych	Curych	k1gInSc1	Curych
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Monaka	Monako	k1gNnSc2	Monako
založena	založen	k2eAgFnSc1d1	založena
fénická	fénický	k2eAgFnSc1d1	fénická
obchodní	obchodní	k2eAgFnSc1d1	obchodní
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Římané	Říman	k1gMnPc1	Říman
nazvali	nazvat	k5eAaBmAgMnP	nazvat
podle	podle	k7c2	podle
Héraklova	Héraklův	k2eAgInSc2d1	Héraklův
chrámu	chrám	k1gInSc2	chrám
Herculis	Herculis	k1gFnSc1	Herculis
Monoeci	Monoeec	k1gInSc3	Monoeec
portus	portus	k1gMnSc1	portus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
území	území	k1gNnPc4	území
Monaka	Monako	k1gNnSc2	Monako
dočasně	dočasně	k6eAd1	dočasně
Vizigóti	Vizigót	k1gMnPc1	Vizigót
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
Francké	francký	k2eAgFnSc3d1	Francká
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1162	[number]	k4	1162
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
janovští	janovský	k2eAgMnPc1d1	janovský
kupci	kupec	k1gMnPc1	kupec
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
právo	právo	k1gNnSc4	právo
skladu	sklad	k1gInSc2	sklad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
padla	padnout	k5eAaPmAgFnS	padnout
monacká	monacký	k2eAgFnSc1d1	monacká
pevnost	pevnost	k1gFnSc1	pevnost
do	do	k7c2	do
rukou	ruka	k1gFnSc7	ruka
Françoise	Françoise	k1gFnSc2	Françoise
Grimaldieho	Grimaldie	k1gMnSc2	Grimaldie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
<g/>
)	)	kIx)	)
využil	využít	k5eAaPmAgMnS	využít
lsti	lest	k1gFnPc4	lest
<g/>
,	,	kIx,	,
žádaje	žádat	k5eAaImSgMnS	žádat
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
pevnosti	pevnost	k1gFnSc2	pevnost
v	v	k7c6	v
rouše	roucho	k1gNnSc6	roucho
františkánského	františkánský	k2eAgMnSc2d1	františkánský
mnicha	mnich	k1gMnSc2	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
Grimaldiovci	Grimaldiovec	k1gMnPc1	Grimaldiovec
definitivně	definitivně	k6eAd1	definitivně
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1419	[number]	k4	1419
<g/>
,	,	kIx,	,
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
převážně	převážně	k6eAd1	převážně
pod	pod	k7c7	pod
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
ochranou	ochrana	k1gFnSc7	ochrana
(	(	kIx(	(
<g/>
vláda	vláda	k1gFnSc1	vláda
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
několika	několik	k4yIc2	několik
kratších	krátký	k2eAgNnPc2d2	kratší
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
bylo	být	k5eAaImAgNnS	být
Monako	Monako	k1gNnSc1	Monako
francouzskými	francouzský	k2eAgNnPc7d1	francouzské
revolučními	revoluční	k2eAgInPc7d1	revoluční
vojsky	vojsky	k6eAd1	vojsky
dobyto	dobyt	k2eAgNnSc1d1	dobyto
a	a	k8xC	a
pod	pod	k7c7	pod
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
správou	správa	k1gFnSc7	správa
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadále	nadále	k6eAd1	nadále
bude	být	k5eAaImBp3nS	být
Monako	Monako	k1gNnSc1	Monako
pod	pod	k7c7	pod
protektorátem	protektorát	k1gInSc7	protektorát
Sardinie	Sardinie	k1gFnSc2	Sardinie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
revolučním	revoluční	k2eAgInSc6d1	revoluční
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
přišlo	přijít	k5eAaPmAgNnS	přijít
Monako	Monako	k1gNnSc1	Monako
o	o	k7c4	o
95	[number]	k4	95
%	%	kIx~	%
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
když	když	k8xS	když
dvě	dva	k4xCgNnPc4	dva
monacká	monacký	k2eAgNnPc4d1	Monacké
města	město	k1gNnPc4	město
Menton	menton	k1gInSc1	menton
a	a	k8xC	a
Roquebrune	Roquebrun	k1gInSc5	Roquebrun
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
oddělila	oddělit	k5eAaPmAgFnS	oddělit
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnSc1d1	zbývající
část	část	k1gFnSc1	část
Monaka	Monako	k1gNnSc2	Monako
získala	získat	k5eAaPmAgFnS	získat
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgNnP	být
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
francouzsko-monacká	francouzskoonacký	k2eAgFnSc1d1	francouzsko-monacký
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
Monako	Monako	k1gNnSc1	Monako
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
těžkou	těžký	k2eAgFnSc7d1	těžká
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
situací	situace	k1gFnSc7	situace
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
územní	územní	k2eAgFnSc1d1	územní
ztráta	ztráta	k1gFnSc1	ztráta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
znamenala	znamenat	k5eAaImAgFnS	znamenat
i	i	k9	i
ztrátu	ztráta	k1gFnSc4	ztráta
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
<g/>
,	,	kIx,	,
především	především	k9	především
omezením	omezení	k1gNnSc7	omezení
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
citronů	citron	k1gInPc2	citron
<g/>
,	,	kIx,	,
pomerančů	pomeranč	k1gInPc2	pomeranč
a	a	k8xC	a
oliv	oliva	k1gFnPc2	oliva
<g/>
.	.	kIx.	.
</s>
<s>
Monacký	monacký	k2eAgMnSc1d1	monacký
kníže	kníže	k1gMnSc1	kníže
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Monacký	monacký	k2eAgMnSc1d1	monacký
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
situaci	situace	k1gFnSc4	situace
své	svůj	k3xOyFgFnSc2	svůj
malé	malý	k2eAgFnSc2d1	malá
země	zem	k1gFnSc2	zem
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
zlepšit	zlepšit	k5eAaPmF	zlepšit
a	a	k8xC	a
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
na	na	k7c6	na
území	území	k1gNnSc6	území
Monaka	Monako	k1gNnSc2	Monako
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
první	první	k4xOgFnPc1	první
přímořské	přímořský	k2eAgFnPc1d1	přímořská
lázně	lázeň	k1gFnPc1	lázeň
s	s	k7c7	s
kasinem	kasino	k1gNnSc7	kasino
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
rozkvět	rozkvět	k1gInSc1	rozkvět
pak	pak	k6eAd1	pak
přišel	přijít	k5eAaPmAgInS	přijít
se	s	k7c7	s
založením	založení	k1gNnSc7	založení
čtvrti	čtvrt	k1gFnSc2	čtvrt
Monte	Mont	k1gMnSc5	Mont
Carlo	Carla	k1gMnSc5	Carla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
kasino	kasino	k1gNnSc1	kasino
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
a	a	k8xC	a
napojením	napojení	k1gNnSc7	napojení
Monte	Mont	k1gInSc5	Mont
Carla	Carl	k1gMnSc4	Carl
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
bylo	být	k5eAaImAgNnS	být
Monako	Monako	k1gNnSc1	Monako
v	v	k7c6	v
celní	celní	k2eAgFnSc6d1	celní
unii	unie	k1gFnSc6	unie
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
vládl	vládnout	k5eAaImAgMnS	vládnout
kníže	kníže	k1gMnSc1	kníže
Rainier	Rainier	k1gMnSc1	Rainier
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
liberalizoval	liberalizovat	k5eAaImAgMnS	liberalizovat
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
posílil	posílit	k5eAaPmAgMnS	posílit
práva	právo	k1gNnPc4	právo
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
vládne	vládnout	k5eAaImIp3nS	vládnout
jeho	jeho	k3xOp3gNnSc1	jeho
syn	syn	k1gMnSc1	syn
Albert	Albert	k1gMnSc1	Albert
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
až	až	k9	až
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
stranou	stranou	k6eAd1	stranou
Národní	národní	k2eAgFnSc1d1	národní
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
UND	UND	kA	UND
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
má	mít	k5eAaImIp3nS	mít
absolutní	absolutní	k2eAgFnSc4d1	absolutní
většinu	většina	k1gFnSc4	většina
Campora	Campor	k1gMnSc2	Campor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
Monako	Monako	k1gNnSc1	Monako
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
je	být	k5eAaImIp3nS	být
suverénní	suverénní	k2eAgInSc1d1	suverénní
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
pět	pět	k4xCc4	pět
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
riviéře	riviéra	k1gFnSc6	riviéra
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc1d1	Malé
knížectví	knížectví	k1gNnSc1	knížectví
se	se	k3xPyFc4	se
prostírá	prostírat	k5eAaImIp3nS	prostírat
na	na	k7c6	na
Azurovém	azurový	k2eAgNnSc6d1	azurové
pobřeží	pobřeží	k1gNnSc6	pobřeží
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Côte	Côte	k1gInSc1	Côte
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Azur	azur	k1gInSc1	azur
<g/>
)	)	kIx)	)
jen	jen	k9	jen
8	[number]	k4	8
km	km	kA	km
od	od	k7c2	od
italské	italský	k2eAgFnSc2d1	italská
hranice	hranice	k1gFnSc2	hranice
na	na	k7c6	na
výběžku	výběžek	k1gInSc6	výběžek
Přímořských	přímořský	k2eAgFnPc2d1	přímořská
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
pouhých	pouhý	k2eAgInPc2d1	pouhý
202	[number]	k4	202
hektarů	hektar	k1gInPc2	hektar
s	s	k7c7	s
šířkou	šířka	k1gFnSc7	šířka
od	od	k7c2	od
340	[number]	k4	340
m	m	kA	m
do	do	k7c2	do
1	[number]	k4	1
700	[number]	k4	700
m.	m.	k?	m.
Od	od	k7c2	od
skalnatého	skalnatý	k2eAgNnSc2d1	skalnaté
pobřeží	pobřeží	k1gNnSc2	pobřeží
stoupá	stoupat	k5eAaImIp3nS	stoupat
terén	terén	k1gInSc1	terén
terasovitě	terasovitě	k6eAd1	terasovitě
k	k	k7c3	k
pohoří	pohoří	k1gNnSc3	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
linie	linie	k1gFnPc1	linie
vrcholků	vrcholek	k1gInPc2	vrcholek
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pár	pár	k4xCyI	pár
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
mezi	mezi	k7c7	mezi
cca	cca	kA	cca
1100	[number]	k4	1100
<g/>
-	-	kIx~	-
<g/>
1300	[number]	k4	1300
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
-	-	kIx~	-
ty	ty	k3xPp2nSc1	ty
už	už	k6eAd1	už
ale	ale	k8xC	ale
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
sráz	sráz	k1gInSc1	sráz
pohoří	pohoří	k1gNnSc2	pohoří
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
i	i	k9	i
pod	pod	k7c7	pod
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
hloubka	hloubka	k1gFnSc1	hloubka
moře	moře	k1gNnSc2	moře
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
monackého	monacký	k2eAgInSc2d1	monacký
přístavu	přístav	k1gInSc2	přístav
rapidně	rapidně	k6eAd1	rapidně
klesá	klesat	k5eAaImIp3nS	klesat
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
i	i	k9	i
kotvení	kotvení	k1gNnPc2	kotvení
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
ponorem	ponor	k1gInSc7	ponor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
vlastní	vlastní	k2eAgNnSc1d1	vlastní
Monako	Monako	k1gNnSc1	Monako
se	s	k7c7	s
starým	starý	k2eAgNnSc7d1	staré
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
knížecím	knížecí	k2eAgInSc7d1	knížecí
palácem	palác	k1gInSc7	palác
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
s	s	k7c7	s
neoromantickou	neoromantický	k2eAgFnSc7d1	neoromantická
katedrálou	katedrála	k1gFnSc7	katedrála
a	a	k8xC	a
oceánografickým	oceánografický	k2eAgNnSc7d1	Oceánografické
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
,	,	kIx,	,
založeným	založený	k2eAgNnSc7d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
Nové	Nové	k2eAgNnSc1d1	Nové
m.	m.	k?	m.
La	la	k1gNnSc1	la
Condamine	Condamin	k1gInSc5	Condamin
s	s	k7c7	s
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
bankami	banka	k1gFnPc7	banka
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
obchodní	obchodní	k2eAgMnSc1d1	obchodní
čtvrtí	čtvrtit	k5eAaImIp3nS	čtvrtit
<g/>
;	;	kIx,	;
luxusní	luxusní	k2eAgFnPc1d1	luxusní
lázně	lázeň	k1gFnPc1	lázeň
Monte	Mont	k1gInSc5	Mont
Carlo	Carlo	k1gNnSc1	Carlo
s	s	k7c7	s
hernou	herna	k1gFnSc7	herna
(	(	kIx(	(
<g/>
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1878	[number]	k4	1878
<g/>
-	-	kIx~	-
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
a	a	k8xC	a
kongresovým	kongresový	k2eAgNnSc7d1	Kongresové
centrem	centrum	k1gNnSc7	centrum
a	a	k8xC	a
městskou	městský	k2eAgFnSc7d1	městská
tratí	trať	k1gFnSc7	trať
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
na	na	k7c4	na
náspu	náspa	k1gFnSc4	náspa
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
Fontvieille	Fontvieille	k1gFnSc2	Fontvieille
s	s	k7c7	s
obytnými	obytný	k2eAgFnPc7d1	obytná
a	a	k8xC	a
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
stavbami	stavba	k1gFnPc7	stavba
<g/>
,	,	kIx,	,
přír	přír	k1gInSc1	přír
<g/>
.	.	kIx.	.
parkem	park	k1gInSc7	park
a	a	k8xC	a
sportovními	sportovní	k2eAgInPc7d1	sportovní
areály	areál	k1gInPc7	areál
i	i	k8xC	i
novým	nový	k2eAgInSc7d1	nový
jachtovým	jachtův	k2eAgInSc7d1	jachtův
přístavem	přístav	k1gInSc7	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Mírné	mírný	k2eAgNnSc1d1	mírné
přímořské	přímořský	k2eAgNnSc1d1	přímořské
podnebí	podnebí	k1gNnSc1	podnebí
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
růst	růst	k1gInSc4	růst
i	i	k9	i
velmi	velmi	k6eAd1	velmi
pestré	pestrý	k2eAgFnSc3d1	pestrá
mediteránní	mediteránní	k2eAgFnSc3d1	mediteránní
vegetaci	vegetace	k1gFnSc3	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
je	být	k5eAaImIp3nS	být
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
jediného	jediný	k2eAgNnSc2d1	jediné
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgMnSc7d1	hlavní
a	a	k8xC	a
i	i	k9	i
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
4	[number]	k4	4
obvody	obvod	k1gInPc4	obvod
<g/>
:	:	kIx,	:
Monaco-Ville	Monaco-Vill	k1gMnSc5	Monaco-Vill
<g/>
,	,	kIx,	,
Monte	Mont	k1gMnSc5	Mont
Carlo	Carla	k1gMnSc5	Carla
<g/>
,	,	kIx,	,
La	la	k0	la
Condamine	Condamin	k1gInSc5	Condamin
<g/>
,	,	kIx,	,
Fontvieille	Fontvieille	k1gFnSc5	Fontvieille
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
zastavěno	zastavěn	k2eAgNnSc1d1	zastavěno
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
půdy	půda	k1gFnSc2	půda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zástavbu	zástavba	k1gFnSc4	zástavba
Monaka	Monako	k1gNnSc2	Monako
je	být	k5eAaImIp3nS	být
takřka	takřka	k6eAd1	takřka
nalepená	nalepený	k2eAgFnSc1d1	nalepená
zástavba	zástavba	k1gFnSc1	zástavba
sousedních	sousední	k2eAgFnPc2d1	sousední
francouzských	francouzský	k2eAgFnPc2d1	francouzská
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
prochází	procházet	k5eAaImIp3nP	procházet
státní	státní	k2eAgFnPc1d1	státní
hranice	hranice	k1gFnPc1	hranice
ulicemi	ulice	k1gFnPc7	ulice
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
nějak	nějak	k6eAd1	nějak
viditelně	viditelně	k6eAd1	viditelně
vyznačena	vyznačit	k5eAaPmNgNnP	vyznačit
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
viditelně	viditelně	k6eAd1	viditelně
vyznačenou	vyznačený	k2eAgFnSc4d1	vyznačená
státní	státní	k2eAgFnSc4d1	státní
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Monakem	Monako	k1gNnSc7	Monako
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
má	mít	k5eAaImIp3nS	mít
teplé	teplý	k2eAgNnSc1d1	teplé
středomořské	středomořský	k2eAgNnSc1d1	středomořské
klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
oceánské	oceánský	k2eAgNnSc1d1	oceánské
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgNnSc1d1	vlhké
subtropické	subtropický	k2eAgNnSc1d1	subtropické
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
teplá	teplý	k2eAgNnPc4d1	teplé
a	a	k8xC	a
suchá	suchý	k2eAgNnPc4d1	suché
léta	léto	k1gNnPc4	léto
a	a	k8xC	a
mírné	mírný	k2eAgFnPc4d1	mírná
<g/>
,	,	kIx,	,
deštivé	deštivý	k2eAgFnPc4d1	deštivá
zimy	zima	k1gFnPc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Deštivé	deštivý	k2eAgNnSc1d1	deštivé
období	období	k1gNnSc1	období
přichází	přicházet	k5eAaImIp3nS	přicházet
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Chladných	chladný	k2eAgFnPc2d1	chladná
sezon	sezona	k1gFnPc2	sezona
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
s	s	k7c7	s
těmi	ten	k3xDgInPc7	ten
teplejšími	teplý	k2eAgInPc7d2	teplejší
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgNnSc1d1	letní
odpoledne	odpoledne	k1gNnSc1	odpoledne
nebývají	bývat	k5eNaImIp3nP	bývat
obzvlášť	obzvlášť	k6eAd1	obzvlášť
horká	horký	k2eAgFnSc1d1	horká
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
zřídka	zřídka	k6eAd1	zřídka
překročí	překročit	k5eAaPmIp3nS	překročit
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
a	a	k8xC	a
nejsušším	suchý	k2eAgInSc7d3	nejsušší
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
červenec	červenec	k1gInSc1	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgFnPc1d1	noční
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
mírné	mírný	k2eAgNnSc1d1	mírné
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
poměrně	poměrně	k6eAd1	poměrně
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
letních	letní	k2eAgNnPc6d1	letní
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
pohybujeme	pohybovat	k5eAaImIp1nP	pohybovat
mezi	mezi	k7c7	mezi
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
sníh	sníh	k1gInSc1	sníh
a	a	k8xC	a
námraza	námraza	k1gFnSc1	námraza
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
velmi	velmi	k6eAd1	velmi
vzácným	vzácný	k2eAgInSc7d1	vzácný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
maximálně	maximálně	k6eAd1	maximálně
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c4	za
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
je	být	k5eAaImIp3nS	být
Monako	Monako	k1gNnSc4	Monako
dědičnou	dědičný	k2eAgFnSc7d1	dědičná
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
-	-	kIx~	-
vláda	vláda	k1gFnSc1	vláda
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
mužského	mužský	k2eAgMnSc4d1	mužský
potomka	potomek	k1gMnSc4	potomek
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
kníže	kníže	k1gNnSc1wR	kníže
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
poradní	poradní	k2eAgInPc4d1	poradní
orgány	orgán	k1gInPc4	orgán
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
korunní	korunní	k2eAgFnSc4d1	korunní
radu	rada	k1gFnSc4	rada
(	(	kIx(	(
<g/>
11	[number]	k4	11
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
a	a	k8xC	a
státní	státní	k2eAgFnSc4d1	státní
radu	rada	k1gFnSc4	rada
(	(	kIx(	(
<g/>
12	[number]	k4	12
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
autoritou	autorita	k1gFnSc7	autorita
knížete	kníže	k1gNnSc2wR	kníže
jeden	jeden	k4xCgInSc1	jeden
státní	státní	k2eAgMnSc1d1	státní
ministr	ministr	k1gMnSc1	ministr
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
vládními	vládní	k2eAgMnPc7d1	vládní
rady	rada	k1gMnPc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
kníže	kníže	k1gMnSc1	kníže
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc7d1	národní
radou	rada	k1gFnSc7	rada
s	s	k7c7	s
18	[number]	k4	18
volenými	volený	k2eAgMnPc7d1	volený
poslanci	poslanec	k1gMnPc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničních	zahraniční	k2eAgInPc6d1	zahraniční
vztazích	vztah	k1gInPc6	vztah
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Monako	Monako	k1gNnSc1	Monako
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
právní	právní	k2eAgInSc1d1	právní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
podle	podle	k7c2	podle
francouzského	francouzský	k2eAgInSc2d1	francouzský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
Monako	Monako	k1gNnSc1	Monako
neoznačuje	označovat	k5eNaImIp3nS	označovat
za	za	k7c2	za
knížectví	knížectví	k1gNnSc2	knížectví
ale	ale	k8xC	ale
tzv.	tzv.	kA	tzv.
principát	principát	k1gInSc1	principát
-	-	kIx~	-
tedy	tedy	k9	tedy
(	(	kIx(	(
<g/>
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
)	)	kIx)	)
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
panovníkem	panovník	k1gMnSc7	panovník
je	být	k5eAaImIp3nS	být
kníže	kníže	k1gMnSc1	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc4	druhý
nejmenší	malý	k2eAgInSc4d3	nejmenší
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejhustěji	husto	k6eAd3	husto
osídlený	osídlený	k2eAgInSc1d1	osídlený
stát	stát	k1gInSc1	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
samosprávní	samosprávní	k2eAgFnSc2d1	samosprávní
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
státem	stát	k1gInSc7	stát
a	a	k8xC	a
městem	město	k1gNnSc7	město
Monako	Monako	k1gNnSc1	Monako
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc1	žádný
geografický	geografický	k2eAgInSc1d1	geografický
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
4	[number]	k4	4
historické	historický	k2eAgFnSc2d1	historická
městské	městský	k2eAgFnSc2d1	městská
čtvrti	čtvrt	k1gFnSc2	čtvrt
(	(	kIx(	(
<g/>
Monaco-Ville	Monaco-Vill	k1gMnSc5	Monaco-Vill
<g/>
,	,	kIx,	,
Monte	Mont	k1gMnSc5	Mont
Carlo	Carla	k1gMnSc5	Carla
<g/>
,	,	kIx,	,
La	la	k0	la
Condamine	Condamin	k1gInSc5	Condamin
<g/>
,	,	kIx,	,
Fontvieille	Fontvieille	k1gInSc1	Fontvieille
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Monaco-Ville	Monaco-Ville	k1gFnSc1	Monaco-Ville
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgFnSc1d1	původní
<g/>
,	,	kIx,	,
staré	starý	k2eAgNnSc1d1	staré
město	město	k1gNnSc1	město
známe	znát	k5eAaImIp1nP	znát
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
skála	skála	k1gFnSc1	skála
Monaka	Monako	k1gNnSc2	Monako
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Monte	Monte	k5eAaPmIp2nP	Monte
Carlo	Carlo	k1gNnSc1	Carlo
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
obytných	obytný	k2eAgFnPc2d1	obytná
a	a	k8xC	a
rekreačních	rekreační	k2eAgFnPc2d1	rekreační
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
Condamine	Condamin	k1gInSc5	Condamin
-	-	kIx~	-
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
část	část	k1gFnSc4	část
Monaka	Monako	k1gNnSc2	Monako
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
součást	součást	k1gFnSc4	součást
tvoří	tvořit	k5eAaImIp3nS	tvořit
přístav	přístav	k1gInSc1	přístav
Port	porta	k1gFnPc2	porta
Hercules	Herculesa	k1gFnPc2	Herculesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
10	[number]	k4	10
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
-	-	kIx~	-
Spojené	spojený	k2eAgInPc1d1	spojený
národy	národ	k1gInPc1	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
OBSE	OBSE	kA	OBSE
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
-	-	kIx~	-
37	[number]	k4	37
800	[number]	k4	800
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
-	-	kIx~	-
18	[number]	k4	18
948	[number]	k4	948
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
roční	roční	k2eAgInSc1d1	roční
přírůstek	přírůstek	k1gInSc1	přírůstek
-	-	kIx~	-
0,6	[number]	k4	0,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
Francouzi	Francouz	k1gMnPc1	Francouz
(	(	kIx(	(
<g/>
28,4	[number]	k4	28,4
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
tzv.	tzv.	kA	tzv.
Monegaskové	Monegasek	k1gMnPc1	Monegasek
(	(	kIx(	(
<g/>
21,6	[number]	k4	21,6
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
na	na	k7c6	na
území	území	k1gNnSc6	území
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
Italové	Ital	k1gMnPc1	Ital
(	(	kIx(	(
<g/>
18,7	[number]	k4	18,7
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Britové	Brit	k1gMnPc1	Brit
(	(	kIx(	(
<g/>
7,5	[number]	k4	7,5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Belgičané	Belgičan	k1gMnPc1	Belgičan
(	(	kIx(	(
<g/>
2,8	[number]	k4	2,8
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švýcaři	Švýcar	k1gMnPc1	Švýcar
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
1,2	[number]	k4	1,2
<g/>
%	%	kIx~	%
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
Monaka	Monako	k1gNnSc2	Monako
je	být	k5eAaImIp3nS	být
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Angličtinu	angličtina	k1gFnSc4	angličtina
využívá	využívat	k5eAaImIp3nS	využívat
americká	americký	k2eAgFnSc1d1	americká
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
<g/>
,	,	kIx,	,
kanadská	kanadský	k2eAgFnSc1d1	kanadská
a	a	k8xC	a
irská	irský	k2eAgFnSc1d1	irská
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tradičním	tradiční	k2eAgInSc7d1	tradiční
<g/>
,	,	kIx,	,
národním	národní	k2eAgInSc7d1	národní
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
Monégasque	Monégasque	k1gFnSc1	Monégasque
<g/>
,	,	kIx,	,
dialekt	dialekt	k1gInSc1	dialekt
románského	románský	k2eAgInSc2d1	románský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
se	se	k3xPyFc4	se
také	také	k9	také
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
oblasti	oblast	k1gFnSc6	oblast
Ligurie	Ligurie	k1gFnSc2	Ligurie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dialektem	dialekt	k1gInSc7	dialekt
se	se	k3xPyFc4	se
například	například	k6eAd1	například
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
pouliční	pouliční	k2eAgFnPc4d1	pouliční
reklamy	reklama	k1gFnPc4	reklama
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
<g/>
,	,	kIx,	,
opevněném	opevněný	k2eAgNnSc6d1	opevněné
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
83,2	[number]	k4	83,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
12,9	[number]	k4	12,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
judaismus	judaismus	k1gInSc1	judaismus
2,9	[number]	k4	2,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
0,8	[number]	k4	0,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc4d1	jiné
vyznání	vyznání	k1gNnSc4	vyznání
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
katolictví	katolictví	k1gNnSc1	katolictví
se	s	k7c7	s
svobodou	svoboda	k1gFnSc7	svoboda
jiných	jiný	k2eAgNnPc2d1	jiné
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pět	pět	k4xCc1	pět
římskokatolických	římskokatolický	k2eAgFnPc2d1	Římskokatolická
farností	farnost	k1gFnPc2	farnost
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
biskupství	biskupství	k1gNnSc1	biskupství
monackého	monacký	k2eAgMnSc2d1	monacký
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jeden	jeden	k4xCgInSc1	jeden
anglikánský	anglikánský	k2eAgInSc1d1	anglikánský
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
Avenue	avenue	k1gFnSc6	avenue
de	de	k?	de
Grande	grand	k1gMnSc5	grand
Bretagnee	Bretagneus	k1gMnSc5	Bretagneus
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gInSc2	Carl
<g/>
.	.	kIx.	.
</s>
<s>
Muslimská	muslimský	k2eAgFnSc1d1	muslimská
populace	populace	k1gFnSc1	populace
Monaka	Monako	k1gNnSc2	Monako
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
asi	asi	k9	asi
z	z	k7c2	z
280	[number]	k4	280
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
tohoto	tento	k3xDgNnSc2	tento
náboženství	náboženství	k1gNnSc2	náboženství
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
občany	občan	k1gMnPc4	občan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
obyvateli	obyvatel	k1gMnPc7	obyvatel
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
muslimských	muslimský	k2eAgMnPc2d1	muslimský
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
turecká	turecký	k2eAgFnSc1d1	turecká
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
žádná	žádný	k3yNgFnSc1	žádný
mešita	mešita	k1gFnSc1	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
odvětvím	odvětví	k1gNnSc7	odvětví
knížectví	knížectví	k1gNnSc2	knížectví
je	být	k5eAaImIp3nS	být
celoroční	celoroční	k2eAgInSc4d1	celoroční
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
i	i	k8xC	i
sektor	sektor	k1gInSc4	sektor
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
a	a	k8xC	a
pojišťovnictví	pojišťovnictví	k1gNnSc2	pojišťovnictví
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
příjmy	příjem	k1gInPc1	příjem
vytváří	vytvářit	k5eAaPmIp3nP	vytvářit
převážně	převážně	k6eAd1	převážně
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
přidané	přidaný	k2eAgFnSc2d1	přidaná
hodnoty	hodnota	k1gFnSc2	hodnota
a	a	k8xC	a
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
obratu	obrat	k1gInSc2	obrat
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
státní	státní	k2eAgInPc1d1	státní
monopoly	monopol	k1gInPc1	monopol
jako	jako	k8xC	jako
telefon	telefon	k1gInSc1	telefon
<g/>
,	,	kIx,	,
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
tabákových	tabákový	k2eAgInPc2d1	tabákový
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
vydávání	vydávání	k1gNnSc1	vydávání
známek	známka	k1gFnPc2	známka
aj.	aj.	kA	aj.
Přímé	přímý	k2eAgFnSc2d1	přímá
daně	daň	k1gFnSc2	daň
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
Monaka	Monako	k1gNnSc2	Monako
nevybírají	vybírat	k5eNaImIp3nP	vybírat
(	(	kIx(	(
<g/>
jen	jen	k9	jen
Francouzi	Francouz	k1gMnPc1	Francouz
podléhají	podléhat	k5eAaImIp3nP	podléhat
francouzským	francouzský	k2eAgInPc3d1	francouzský
daňovým	daňový	k2eAgInPc3d1	daňový
předpisům	předpis	k1gInPc3	předpis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
sektoru	sektor	k1gInSc6	sektor
převládají	převládat	k5eAaImIp3nP	převládat
malé	malý	k2eAgInPc1d1	malý
a	a	k8xC	a
střední	střední	k2eAgInPc1d1	střední
podniky	podnik	k1gInPc1	podnik
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
chemie	chemie	k1gFnSc2	chemie
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
farmacie	farmacie	k1gFnSc1	farmacie
a	a	k8xC	a
kosmetika	kosmetika	k1gFnSc1	kosmetika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
plastických	plastický	k2eAgFnPc2d1	plastická
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
je	být	k5eAaImIp3nS	být
Monako	Monako	k1gNnSc1	Monako
spojeno	spojen	k2eAgNnSc1d1	spojeno
měnovou	měnový	k2eAgFnSc4d1	měnová
<g/>
,	,	kIx,	,
hosp	hosp	k1gInSc1	hosp
<g/>
.	.	kIx.	.
a	a	k8xC	a
celní	celní	k2eAgFnSc7d1	celní
unií	unie	k1gFnSc7	unie
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
podle	podle	k7c2	podle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
představuje	představovat	k5eAaImIp3nS	představovat
asi	asi	k9	asi
870	[number]	k4	870
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
US	US	kA	US
$	$	kIx~	$
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
27	[number]	k4	27
188	[number]	k4	188
na	na	k7c4	na
1	[number]	k4	1
obyvatele	obyvatel	k1gMnSc4	obyvatel
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historickou	historický	k2eAgFnSc4d1	historická
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
dějinách	dějiny	k1gFnPc6	dějiny
sehrálo	sehrát	k5eAaPmAgNnS	sehrát
vyhlášené	vyhlášený	k2eAgNnSc1d1	vyhlášené
kasino	kasino	k1gNnSc1	kasino
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
Mentonu	menton	k1gInSc2	menton
a	a	k8xC	a
Roquebrune	Roquebrun	k1gInSc5	Roquebrun
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
Monaku	Monako	k1gNnSc3	Monako
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
přeorientovat	přeorientovat	k5eAaPmF	přeorientovat
na	na	k7c4	na
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgInP	být
výnosy	výnos	k1gInPc1	výnos
z	z	k7c2	z
kasina	kasino	k1gNnSc2	kasino
dostatečné	dostatečná	k1gFnSc2	dostatečná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
uvolněny	uvolněn	k2eAgFnPc1d1	uvolněna
některé	některý	k3yIgFnPc1	některý
daně	daň	k1gFnPc1	daň
pro	pro	k7c4	pro
místní	místní	k2eAgMnPc4d1	místní
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
kasino	kasino	k1gNnSc1	kasino
generovalo	generovat	k5eAaImAgNnS	generovat
většinu	většina	k1gFnSc4	většina
příjmů	příjem	k1gInPc2	příjem
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
%	%	kIx~	%
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
přebral	přebrat	k5eAaPmAgInS	přebrat
sektor	sektor	k1gInSc1	sektor
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
sídlí	sídlet	k5eAaImIp3nS	sídlet
mnoho	mnoho	k4c1	mnoho
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
finančních	finanční	k2eAgFnPc2d1	finanční
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
peněžních	peněžní	k2eAgInPc2d1	peněžní
ústavů	ústav	k1gInPc2	ústav
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
významnou	významný	k2eAgFnSc7d1	významná
složkou	složka	k1gFnSc7	složka
příjmů	příjem	k1gInPc2	příjem
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
orientovaný	orientovaný	k2eAgInSc4d1	orientovaný
pro	pro	k7c4	pro
bohatší	bohatý	k2eAgMnPc4d2	bohatší
i	i	k8xC	i
běžné	běžný	k2eAgMnPc4d1	běžný
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
Monako	Monako	k1gNnSc1	Monako
jen	jen	k8xS	jen
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
zastávka	zastávka	k1gFnSc1	zastávka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
delšího	dlouhý	k2eAgInSc2d2	delší
výletu	výlet	k1gInSc2	výlet
<g/>
.	.	kIx.	.
</s>
<s>
Viditelná	viditelný	k2eAgFnSc1d1	viditelná
je	být	k5eAaImIp3nS	být
i	i	k9	i
snaha	snaha	k1gFnSc1	snaha
Monaka	Monako	k1gNnSc2	Monako
hostovat	hostovat	k5eAaImF	hostovat
významné	významný	k2eAgFnPc4d1	významná
evropské	evropský	k2eAgFnPc4d1	Evropská
i	i	k8xC	i
celosvětové	celosvětový	k2eAgFnPc4d1	celosvětová
události	událost	k1gFnPc4	událost
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
vybrané	vybraný	k2eAgInPc1d1	vybraný
monacké	monacký	k2eAgInPc1d1	monacký
závody	závod	k1gInPc1	závod
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
delikátní	delikátní	k2eAgNnSc4d1	delikátní
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
evropskou	evropský	k2eAgFnSc4d1	Evropská
a	a	k8xC	a
americkou	americký	k2eAgFnSc4d1	americká
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
agenturu	agentura	k1gFnSc4	agentura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Monaka	Monako	k1gNnSc2	Monako
přivedena	přiveden	k2eAgFnSc1d1	přivedena
železnice	železnice	k1gFnSc1	železnice
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
monacký	monacký	k2eAgInSc1d1	monacký
přístav	přístav	k1gInSc1	přístav
doznal	doznat	k5eAaPmAgInS	doznat
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
úprav	úprava	k1gFnPc2	úprava
-	-	kIx~	-
přibyla	přibýt	k5eAaPmAgFnS	přibýt
dvě	dva	k4xCgNnPc4	dva
velká	velký	k2eAgNnPc4d1	velké
mola	molo	k1gNnPc4	molo
pro	pro	k7c4	pro
kotvení	kotvení	k1gNnSc4	kotvení
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
ponorem	ponor	k1gInSc7	ponor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
taktéž	taktéž	k?	taktéž
funguje	fungovat	k5eAaImIp3nS	fungovat
přívoz	přívoz	k1gInSc1	přívoz
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližším	blízký	k2eAgNnSc7d3	nejbližší
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
letištěm	letiště	k1gNnSc7	letiště
je	být	k5eAaImIp3nS	být
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Nice	Nice	k1gFnSc2	Nice
<g/>
,	,	kIx,	,
bohatší	bohatý	k2eAgMnPc1d2	bohatší
návštěvníci	návštěvník	k1gMnPc1	návštěvník
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
do	do	k7c2	do
Monaka	Monako	k1gNnSc2	Monako
využívají	využívat	k5eAaImIp3nP	využívat
vrtulníkové	vrtulníkový	k2eAgFnPc1d1	vrtulníková
přepravy	přeprava	k1gFnPc1	přeprava
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
chtějí	chtít	k5eAaImIp3nP	chtít
Monako	Monako	k1gNnSc4	Monako
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
několik	několik	k4yIc4	několik
výhledových	výhledový	k2eAgInPc2d1	výhledový
autokarů	autokar	k1gInPc2	autokar
a	a	k8xC	a
turistické	turistický	k2eAgFnSc2d1	turistická
vláčky	vláčka	k1gFnSc2	vláčka
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
turisté	turist	k1gMnPc1	turist
mohou	moct	k5eAaImIp3nP	moct
využít	využít	k5eAaPmF	využít
několikapatrové	několikapatrový	k2eAgNnSc4d1	několikapatrové
parkování	parkování	k1gNnSc4	parkování
u	u	k7c2	u
budovy	budova	k1gFnSc2	budova
Oceánografického	oceánografický	k2eAgNnSc2d1	Oceánografické
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Školství	školství	k1gNnSc1	školství
je	být	k5eAaImIp3nS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
podle	podle	k7c2	podle
italského	italský	k2eAgInSc2d1	italský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
výroba	výroba	k1gFnSc1	výroba
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
neexistuje	existovat	k5eNaImIp3nS	existovat
-	-	kIx~	-
potraviny	potravina	k1gFnPc1	potravina
se	se	k3xPyFc4	se
dováží	dovážet	k5eAaImIp3nP	dovážet
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
ceně	cena	k1gFnSc6	cena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zástavbě	zástavba	k1gFnSc6	zástavba
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zelené	zelený	k2eAgFnPc4d1	zelená
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
"	"	kIx"	"
-	-	kIx~	-
zeleň	zeleň	k1gFnSc1	zeleň
na	na	k7c6	na
střechách	střecha	k1gFnPc6	střecha
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
policii	policie	k1gFnSc4	policie
a	a	k8xC	a
gardu	garda	k1gFnSc4	garda
-	-	kIx~	-
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
případě	případ	k1gInSc6	případ
napadení	napadení	k1gNnPc2	napadení
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
dohody	dohoda	k1gFnSc2	dohoda
<g/>
)	)	kIx)	)
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Monako	Monako	k1gNnSc1	Monako
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgNnSc4d1	celé
pokryto	pokryt	k2eAgNnSc4d1	pokryto
sítí	sítí	k1gNnSc4	sítí
CCTV	CCTV	kA	CCTV
kamer	kamera	k1gFnPc2	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
velice	velice	k6eAd1	velice
profesionální	profesionální	k2eAgFnSc4d1	profesionální
<g/>
,	,	kIx,	,
přísnou	přísný	k2eAgFnSc4d1	přísná
a	a	k8xC	a
nesmlouvavou	smlouvavý	k2eNgFnSc4d1	nesmlouvavá
<g/>
;	;	kIx,	;
míra	míra	k1gFnSc1	míra
násilné	násilný	k2eAgFnSc2d1	násilná
kriminality	kriminalita	k1gFnSc2	kriminalita
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tamním	tamní	k2eAgNnSc6d1	tamní
kasinu	kasino	k1gNnSc6	kasino
mohou	moct	k5eAaImIp3nP	moct
jako	jako	k9	jako
krupiéři	krupiér	k1gMnPc1	krupiér
pracovat	pracovat	k5eAaImF	pracovat
pouze	pouze	k6eAd1	pouze
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Monaka	Monako	k1gNnSc2	Monako
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
naopak	naopak	k6eAd1	naopak
mají	mít	k5eAaImIp3nP	mít
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
daňových	daňový	k2eAgNnPc2d1	daňové
osvobození	osvobození	k1gNnPc2	osvobození
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
daňový	daňový	k2eAgInSc4d1	daňový
ráj	ráj	k1gInSc4	ráj
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
tisíce	tisíc	k4xCgInPc4	tisíc
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
francouzské	francouzský	k2eAgMnPc4d1	francouzský
občany	občan	k1gMnPc4	občan
a	a	k8xC	a
firmy	firma	k1gFnPc1	firma
se	se	k3xPyFc4	se
ale	ale	k9	ale
tato	tento	k3xDgNnPc1	tento
osvobození	osvobození	k1gNnPc1	osvobození
nevztahují	vztahovat	k5eNaImIp3nP	vztahovat
<g/>
.	.	kIx.	.
</s>
