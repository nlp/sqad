<s>
Pláč	pláč	k1gInSc1	pláč
je	být	k5eAaImIp3nS	být
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
jev	jev	k1gInSc1	jev
některých	některý	k3yIgFnPc2	některý
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
především	především	k9	především
smutku	smutek	k1gInSc2	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Plakat	plakat	k5eAaImF	plakat
lze	lze	k6eAd1	lze
ovšem	ovšem	k9	ovšem
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
bolesti	bolest	k1gFnSc3	bolest
nebo	nebo	k8xC	nebo
pocitům	pocit	k1gInPc3	pocit
vzteku	vztek	k1gInSc2	vztek
<g/>
,	,	kIx,	,
beznaděje	beznaděj	k1gFnSc2	beznaděj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
silné	silný	k2eAgFnPc4d1	silná
radosti	radost	k1gFnPc4	radost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pláč	pláč	k1gInSc4	pláč
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
mimika	mimika	k1gFnSc1	mimika
a	a	k8xC	a
proudění	proudění	k1gNnSc1	proudění
slz	slza	k1gFnPc2	slza
<g/>
.	.	kIx.	.
</s>
<s>
Slzy	slza	k1gFnPc1	slza
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
endorfin	endorfin	k1gInSc4	endorfin
tišící	tišící	k2eAgFnSc4d1	tišící
bolest	bolest	k1gFnSc4	bolest
a	a	k8xC	a
stresové	stresový	k2eAgInPc4d1	stresový
hormony	hormon	k1gInPc4	hormon
<g/>
,	,	kIx,	,
smyslem	smysl	k1gInSc7	smysl
pláče	pláč	k1gInSc2	pláč
je	být	k5eAaImIp3nS	být
zklidnění	zklidnění	k1gNnSc4	zklidnění
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Pláč	pláč	k1gInSc1	pláč
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
soucit	soucit	k1gInSc4	soucit
a	a	k8xC	a
lítost	lítost	k1gFnSc4	lítost
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
rovněž	rovněž	k9	rovněž
signální	signální	k2eAgFnSc4d1	signální
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každé	každý	k3xTgNnSc1	každý
ronění	ronění	k1gNnSc1	ronění
slz	slza	k1gFnPc2	slza
je	být	k5eAaImIp3nS	být
pláč	pláč	k1gInSc1	pláč
-	-	kIx~	-
pokud	pokud	k8xS	pokud
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
dojde	dojít	k5eAaPmIp3nS	dojít
při	při	k7c6	při
poranění	poranění	k1gNnSc1	poranění
nebo	nebo	k8xC	nebo
podráždění	podráždění	k1gNnSc1	podráždění
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
o	o	k7c4	o
pláč	pláč	k1gInSc4	pláč
nejedná	jednat	k5eNaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
savci	savec	k1gMnPc1	savec
mají	mít	k5eAaImIp3nP	mít
slzy	slza	k1gFnPc4	slza
ke	k	k7c3	k
zvlhčování	zvlhčování	k1gNnSc3	zvlhčování
povrchu	povrch	k1gInSc2	povrch
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
vyplavování	vyplavování	k1gNnSc2	vyplavování
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřejmě	zřejmě	k6eAd1	zřejmě
pouze	pouze	k6eAd1	pouze
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
plakat	plakat	k5eAaImF	plakat
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
pláči	pláč	k1gInSc3	pláč
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
jedince	jedinec	k1gMnSc2	jedinec
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
věku	věk	k1gInSc6	věk
a	a	k8xC	a
pohlaví	pohlaví	k1gNnSc6	pohlaví
jedince	jedinec	k1gMnSc2	jedinec
<g/>
:	:	kIx,	:
děti	dítě	k1gFnPc1	dítě
pláčou	plakat	k5eAaImIp3nP	plakat
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
průměrně	průměrně	k6eAd1	průměrně
pláčou	plakat	k5eAaImIp3nP	plakat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
staří	starý	k2eAgMnPc1d1	starý
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
práh	práh	k1gInSc4	práh
pláče	pláč	k1gInSc2	pláč
nižší	nízký	k2eAgFnSc4d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
společenská	společenský	k2eAgFnSc1d1	společenská
tolerance	tolerance	k1gFnSc1	tolerance
pláče	pláč	k1gInSc2	pláč
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
liší	lišit	k5eAaImIp3nP	lišit
-	-	kIx~	-
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
například	například	k6eAd1	například
muži	muž	k1gMnPc1	muž
snaží	snažit	k5eAaImIp3nP	snažit
pláč	pláč	k1gInSc4	pláč
potlačovat	potlačovat	k5eAaImF	potlačovat
a	a	k8xC	a
pláčou	plakat	k5eAaImIp3nP	plakat
jen	jen	k9	jen
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
žalostných	žalostný	k2eAgFnPc6d1	žalostná
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
některých	některý	k3yIgFnPc6	některý
duševních	duševní	k2eAgFnPc6d1	duševní
poruchách	porucha	k1gFnPc6	porucha
se	se	k3xPyFc4	se
dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
pláč	pláč	k1gInSc1	pláč
bez	bez	k7c2	bez
příčiny	příčina	k1gFnSc2	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hysterii	hysterie	k1gFnSc6	hysterie
může	moct	k5eAaImIp3nS	moct
pláč	pláč	k1gInSc4	pláč
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Slzy	slza	k1gFnPc1	slza
Hysterie	hysterie	k1gFnSc2	hysterie
Smutek	smutek	k1gInSc1	smutek
Smích	smích	k1gInSc1	smích
Bolest	bolest	k1gFnSc1	bolest
NAKONEČNÝ	NAKONEČNÝ	kA	NAKONEČNÝ
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
emoce	emoce	k1gFnPc1	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
763	[number]	k4	763
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
254	[number]	k4	254
<g/>
.	.	kIx.	.
</s>
<s>
VONDRÁČEK	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
;	;	kIx,	;
HOLUB	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Fantastické	fantastický	k2eAgInPc1d1	fantastický
a	a	k8xC	a
magické	magický	k2eAgInPc1d1	magický
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Columbus	Columbus	k1gMnSc1	Columbus
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7136	[number]	k4	7136
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
