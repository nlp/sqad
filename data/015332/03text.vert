<s>
Gyermekvasút	Gyermekvasút	k1gMnSc1
</s>
<s>
Gyermekvasút	Gyermekvasút	k1gMnSc1
</s>
<s>
Gyermekvasút	Gyermekvasút	k1gMnSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
<g/>
7	#num#	k4
</s>
<s>
Provozovatel	provozovatel	k1gMnSc1
dráhyMÁV	dráhyMÁV	k?
</s>
<s>
Technické	technický	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
11,2	11,2	k4
km	km	kA
</s>
<s>
Rozchod	rozchod	k1gInSc1
koleje	kolej	k1gFnSc2
<g/>
760	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
bosenský	bosenský	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Napájecí	napájecí	k2eAgNnSc1d1
soustavaNeelektrizováno	soustavaNeelektrizován	k2eAgNnSc1d1
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
20	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
hod	hod	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Průběh	průběh	k1gInSc1
trati	trať	k1gFnSc2
</s>
<s>
Legenda	legenda	k1gFnSc1
</s>
<s>
Stanice	stanice	k1gFnSc1
(	(	kIx(
<g/>
vzdálenost	vzdálenost	k1gFnSc1
od	od	k7c2
Széchenyi-hegy	Széchenyi-hega	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Széchenyi-hegy	Széchenyi-hega	k1gFnPc1
(	(	kIx(
<g/>
0	#num#	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Normafa	Normaf	k1gMnSc4
(	(	kIx(
<g/>
0,8	0,8	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Csillebérc	Csillebérc	k1gFnSc1
(	(	kIx(
<g/>
1,7	1,7	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Virágvölgy	Virágvölga	k1gFnPc1
(	(	kIx(
<g/>
3,0	3,0	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
János-hegy	János-hega	k1gFnPc1
(	(	kIx(
<g/>
4,5	4,5	k4
km	km	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
přestup	přestup	k1gInSc1
na	na	k7c4
visutou	visutý	k2eAgFnSc4d1
lanovku	lanovka	k1gFnSc4
(	(	kIx(
<g/>
Libegö	Libegö	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Vadaspark	Vadaspark	k1gInSc1
(	(	kIx(
<g/>
5,7	5,7	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Szépjuhászné	Szépjuhászný	k2eAgInPc4d1
(	(	kIx(
<g/>
6,7	6,7	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Hárs-hegy	Hárs-hega	k1gFnPc1
(	(	kIx(
<g/>
8,7	8,7	k4
km	km	kA
<g/>
)	)	kIx)
</s>
<s>
Hűvösvölgy	Hűvösvölga	k1gFnPc1
(	(	kIx(
<g/>
11.2	11.2	k4
km	km	kA
<g/>
)	)	kIx)
konečná	konečná	k1gFnSc1
tramvaje	tramvaj	k1gFnSc2
č.	č.	k?
56	#num#	k4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gyermekvasút	Gyermekvasút	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
:	:	kIx,
Dětská	dětský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
úzkorozchodná	úzkorozchodný	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
<g/>
,	,	kIx,
obsluhovaná	obsluhovaný	k2eAgFnSc1d1
výhradně	výhradně	k6eAd1
dětmi	dítě	k1gFnPc7
ve	v	k7c6
věku	věk	k1gInSc6
10	#num#	k4
až	až	k9
14	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Železnice	železnice	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
ojedinělá	ojedinělý	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
byla	být	k5eAaImAgFnS
vybudována	vybudován	k2eAgFnSc1d1
maďarskými	maďarský	k2eAgMnPc7d1
mládežníky	mládežník	k1gMnPc7
v	v	k7c6
letech	léto	k1gNnPc6
1948	#num#	k4
–	–	k?
1950	#num#	k4
a	a	k8xC
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
nesla	nést	k5eAaImAgFnS
název	název	k1gInSc4
Úttörővasút	Úttörővasúta	k1gFnPc2
(	(	kIx(
<g/>
Pionýrská	pionýrský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsluhu	obsluha	k1gFnSc4
této	tento	k3xDgFnSc2
tratě	trať	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
zásadně	zásadně	k6eAd1
děti	dítě	k1gFnPc1
ve	v	k7c6
věku	věk	k1gInSc6
10	#num#	k4
až	až	k9
14	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
chtěly	chtít	k5eAaImAgFnP
pracovat	pracovat	k5eAaImF
pro	pro	k7c4
MÁV	MÁV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
na	na	k7c6
starosti	starost	k1gFnSc6
vše	všechen	k3xTgNnSc1
od	od	k7c2
řízení	řízení	k1gNnSc2
vlakového	vlakový	k2eAgInSc2d1
provozu	provoz	k1gInSc2
přes	přes	k7c4
vypravování	vypravování	k1gNnSc4
vlaků	vlak	k1gInPc2
ze	z	k7c2
stanic	stanice	k1gFnPc2
až	až	k9
po	po	k7c4
kontrolu	kontrola	k1gFnSc4
jízdních	jízdní	k2eAgInPc2d1
dokladů	doklad	k1gInPc2
ve	v	k7c6
vlaku	vlak	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Velkou	velký	k2eAgFnSc7d1
zajímavostí	zajímavost	k1gFnSc7
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
198	#num#	k4
metrů	metr	k1gInPc2
dlouhý	dlouhý	k2eAgInSc1d1
tunel	tunel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
pro	pro	k7c4
větší	veliký	k2eAgNnSc4d2
vzrušení	vzrušení	k1gNnSc4
z	z	k7c2
jízdy	jízda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Staniční	staniční	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Hűvösvölgy	Hűvösvölga	k1gFnSc2
hostí	hostit	k5eAaImIp3nS
Gyermekvasutas	Gyermekvasutas	k1gInSc1
Muzeum	muzeum	k1gNnSc1
(	(	kIx(
<g/>
Dětské	dětský	k2eAgNnSc1d1
Železniční	železniční	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vystavuje	vystavovat	k5eAaImIp3nS
především	především	k6eAd1
exponáty	exponát	k1gInPc4
připomínající	připomínající	k2eAgFnSc3d1
historii	historie	k1gFnSc3
bývalé	bývalý	k2eAgFnSc2d1
pionýrské	pionýrský	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vozidla	vozidlo	k1gNnPc1
</s>
<s>
Na	na	k7c6
většině	většina	k1gFnSc6
vlaků	vlak	k1gInPc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
motorové	motorový	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
rumunské	rumunský	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
řady	řada	k1gFnSc2
Mk	Mk	k1gFnSc2
<g/>
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
pak	pak	k6eAd1
také	také	k9
dvě	dva	k4xCgFnPc1
unikátní	unikátní	k2eAgFnPc1d1
parní	parní	k2eAgFnPc1d1
lokomotivy	lokomotiva	k1gFnPc1
490	#num#	k4
039	#num#	k4
a	a	k8xC
490	#num#	k4
056	#num#	k4
ze	z	k7c2
série	série	k1gFnSc2
MÁV	MÁV	kA
490	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
a	a	k8xC
jízdné	jízdné	k1gNnSc1
</s>
<s>
Gyermekvasút	Gyermekvasút	k1gInSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
v	v	k7c6
rozlehlém	rozlehlý	k2eAgInSc6d1
parku	park	k1gInSc6
hornaté	hornatý	k2eAgFnPc4d1
části	část	k1gFnPc4
Budy	Bud	k2eAgFnPc4d1
ve	v	k7c6
XII	XII	kA
<g/>
.	.	kIx.
a	a	k8xC
II	II	kA
<g/>
.	.	kIx.
obvodě	obvod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
v	v	k7c6
provozu	provoz	k1gInSc6
denně	denně	k6eAd1
od	od	k7c2
9	#num#	k4
do	do	k7c2
16	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
jízdného	jízdné	k1gNnSc2
je	být	k5eAaImIp3nS
800	#num#	k4
forintů	forint	k1gInPc2
nebo	nebo	k8xC
1400	#num#	k4
forintů	forint	k1gInPc2
za	za	k7c4
zpáteční	zpáteční	k2eAgFnSc4d1
jízdenku	jízdenka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
počáteční	počáteční	k2eAgFnSc3d1
stanici	stanice	k1gFnSc3
Széchenyi-hegy	Széchenyi-hega	k1gFnSc2
se	se	k3xPyFc4
lze	lze	k6eAd1
dostat	dostat	k5eAaPmF
další	další	k2eAgFnSc7d1
železniční	železniční	k2eAgFnSc7d1
zajímavostí	zajímavost	k1gFnSc7
a	a	k8xC
to	ten	k3xDgNnSc1
Fogaskerekű	Fogaskerekű	k1gFnSc1
vasút	vasúta	k1gFnPc2
(	(	kIx(
<g/>
Ozubnicovou	ozubnicový	k2eAgFnSc7d1
dráhou	dráha	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
jezdí	jezdit	k5eAaImIp3nP
v	v	k7c6
15	#num#	k4
<g/>
minutových	minutový	k2eAgInPc6d1
intervalech	interval	k1gInPc6
mezi	mezi	k7c7
Széchenyi-hegy	Széchenyi-heg	k1gMnPc7
a	a	k8xC
Városmajor	Városmajora	k1gFnPc2
poblíž	poblíž	k7c2
rušné	rušný	k2eAgFnSc2d1
křižovatky	křižovatka	k1gFnSc2
Széll	Széll	k1gInSc1
Kálmán	Kálmán	k2eAgInSc1d1
tér	tér	k1gInSc4
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Moszkva	Moszkva	k1gFnSc1
tér	tér	k1gInSc1
-	-	kIx~
Moskevské	moskevský	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
Magyar	Magyar	k1gMnSc1
Államvasutak	Államvasutak	k1gMnSc1
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gyermekvasút	Gyermekvasúta	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
<g/>
)	)	kIx)
Gyermekvasut	Gyermekvasut	k2eAgInSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
|	|	kIx~
Železnice	železnice	k1gFnSc1
</s>
