<s>
Šakalí	šakalí	k2eAgNnPc4d1	šakalí
léta	léto	k1gNnPc4	léto
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
muzikálová	muzikálový	k2eAgFnSc1d1	muzikálová
komedie	komedie	k1gFnSc1	komedie
Jana	Jana	k1gFnSc1	Jana
Hřebejka	Hřebejka	k1gFnSc1	Hřebejka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
dejvického	dejvický	k2eAgInSc2d1	dejvický
hotelu	hotel	k1gInSc2	hotel
International	International	k1gFnSc2	International
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poklidného	poklidný	k2eAgInSc2d1	poklidný
maloměstského	maloměstský	k2eAgInSc2d1	maloměstský
života	život	k1gInSc2	život
nečekaně	nečekaně	k6eAd1	nečekaně
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
odnikud	odnikud	k6eAd1	odnikud
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
chuligán	chuligán	k1gMnSc1	chuligán
Bejby	Bejba	k1gFnSc2	Bejba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
svým	svůj	k3xOyFgNnSc7	svůj
výstředním	výstřední	k2eAgNnSc7d1	výstřední
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
kytarou	kytara	k1gFnSc7	kytara
a	a	k8xC	a
vášní	vášeň	k1gFnSc7	vášeň
pro	pro	k7c4	pro
rock	rock	k1gInSc4	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
roll	roll	k1gInSc1	roll
rozbouří	rozbouřit	k5eAaPmIp3nS	rozbouřit
stojaté	stojatý	k2eAgFnSc2d1	stojatá
vody	voda	k1gFnSc2	voda
nejen	nejen	k6eAd1	nejen
klukovského	klukovský	k2eAgInSc2d1	klukovský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
světa	svět	k1gInSc2	svět
dospělých	dospělý	k2eAgMnPc2d1	dospělý
<g/>
,	,	kIx,	,
snažících	snažící	k2eAgMnPc2d1	snažící
se	se	k3xPyFc4	se
nemyslet	myslet	k5eNaImF	myslet
na	na	k7c4	na
nelehkou	lehký	k2eNgFnSc4d1	nelehká
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
musejí	muset	k5eAaImIp3nP	muset
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Bejby	Bejb	k1gInPc4	Bejb
svým	svůj	k3xOyFgInSc7	svůj
nečekaným	čekaný	k2eNgInSc7d1	nečekaný
příchodem	příchod	k1gInSc7	příchod
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
do	do	k7c2	do
života	život	k1gInSc2	život
číšnického	číšnický	k2eAgMnSc2d1	číšnický
pomocníka	pomocník	k1gMnSc2	pomocník
Edy	Eda	k1gMnSc2	Eda
Drábka	Drábek	k1gMnSc2	Drábek
<g/>
,	,	kIx,	,
okrskáře	okrskář	k1gMnSc2	okrskář
Prokopa	Prokop	k1gMnSc2	Prokop
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
přezdívaného	přezdívaný	k2eAgMnSc2d1	přezdívaný
Kšanda	kšanda	k1gFnSc1	kšanda
<g/>
,	,	kIx,	,
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
i	i	k9	i
do	do	k7c2	do
života	život	k1gInSc2	život
barové	barový	k2eAgFnSc2d1	barová
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Milady	Milada	k1gFnSc2	Milada
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
arogantního	arogantní	k2eAgMnSc4d1	arogantní
estébáka	estébáka	k?	estébáka
Přemka	Přemek	k1gMnSc4	Přemek
<g/>
.	.	kIx.	.
</s>
<s>
Šakalí	šakalí	k2eAgNnPc1d1	šakalí
léta	léto	k1gNnPc1	léto
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sakali	Sakali	k1gFnSc1	Sakali
leta	leta	k6eAd1	leta
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc6	Databasa
Šakalí	šakalí	k2eAgNnPc1d1	šakalí
léta	léto	k1gNnPc1	léto
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
