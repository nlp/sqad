<p>
<s>
Vádí	vádí	k1gNnSc1	vádí
(	(	kIx(	(
<g/>
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
<g/>
,	,	kIx,	,
wadi	wadi	k1gNnSc2	wadi
<g/>
,	,	kIx,	,
nachal	nachat	k5eAaBmAgInS	nachat
v	v	k7c6	v
hebrejských	hebrejský	k2eAgInPc6d1	hebrejský
názvech	název	k1gInPc6	název
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vyschlá	vyschlý	k2eAgNnPc1d1	vyschlé
koryta	koryto	k1gNnPc1	koryto
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vádí	vádí	k1gNnSc1	vádí
je	být	k5eAaImIp3nS	být
údolí	údolí	k1gNnSc2	údolí
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
erozí	eroze	k1gFnSc7	eroze
občasného	občasný	k2eAgInSc2d1	občasný
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
v	v	k7c6	v
aridních	aridní	k2eAgFnPc6d1	aridní
(	(	kIx(	(
<g/>
suchých	suchý	k2eAgFnPc6d1	suchá
<g/>
)	)	kIx)	)
oblastech	oblast	k1gFnPc6	oblast
pouští	poušť	k1gFnPc2	poušť
a	a	k8xC	a
polopouští	polopoušť	k1gFnPc2	polopoušť
<g/>
.	.	kIx.	.
</s>
<s>
Vádí	vádí	k1gNnPc1	vádí
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
doby	doba	k1gFnSc2	doba
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
jimi	on	k3xPp3gMnPc7	on
protéká	protékat	k5eAaImIp3nS	protékat
nárazově	nárazově	k6eAd1	nárazově
jen	jen	k9	jen
po	po	k7c6	po
deštích	dešť	k1gInPc6	dešť
či	či	k8xC	či
ve	v	k7c6	v
vlhčím	vlhký	k2eAgNnSc6d2	vlhčí
období	období	k1gNnSc6	období
roku	rok	k1gInSc2	rok
a	a	k8xC	a
často	často	k6eAd1	často
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
strmé	strmý	k2eAgFnPc4d1	strmá
stěny	stěna	k1gFnPc4	stěna
ohraničující	ohraničující	k2eAgNnSc4d1	ohraničující
údolí	údolí	k1gNnSc4	údolí
a	a	k8xC	a
dno	dno	k1gNnSc4	dno
pokryje	pokrýt	k5eAaPmIp3nS	pokrýt
horninovou	horninový	k2eAgFnSc7d1	horninová
sutí	suť	k1gFnSc7	suť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
vádí	vádí	k1gNnPc2	vádí
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
v	v	k7c6	v
Jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
československá	československý	k2eAgFnSc1d1	Československá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
<g/>
:	:	kIx,	:
vádí	vádí	k1gNnSc1	vádí
<g/>
,	,	kIx,	,
VI	VI	kA	VI
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
ÚdolíDra	ÚdolíDra	k1gFnSc1	ÚdolíDra
–	–	k?	–
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
</s>
</p>
<p>
<s>
Mulúja	Mulúj	k2eAgFnSc1d1	Mulúj
–	–	k?	–
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
</s>
</p>
<p>
<s>
Vádí	vádí	k1gNnSc1	vádí
Hammámat	Hammámat	k1gFnSc2	Hammámat
–	–	k?	–
Egypt	Egypt	k1gInSc1	Egypt
</s>
</p>
<p>
<s>
Vádí	vádí	k1gNnSc1	vádí
Natrun	Natrun	k1gMnSc1	Natrun
–	–	k?	–
Egypt	Egypt	k1gInSc1	Egypt
</s>
</p>
<p>
<s>
Vádí	vádí	k1gNnSc1	vádí
al-Araba	al-Araba	k1gMnSc1	al-Araba
–	–	k?	–
Izrael	Izrael	k1gInSc1	Izrael
<g/>
/	/	kIx~	/
<g/>
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vádí	vádí	k1gNnSc2	vádí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vádí	vádí	k1gNnSc2	vádí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
