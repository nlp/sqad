<s>
Descartes	Descartes	k1gMnSc1	Descartes
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
vzdělané	vzdělaný	k2eAgFnSc6d1	vzdělaná
šlechtické	šlechtický	k2eAgFnSc6d1	šlechtická
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
radou	rada	k1gMnSc7	rada
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
parlamentu	parlament	k1gInSc2	parlament
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c4	v
Rennes	Rennes	k1gInSc4	Rennes
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
brzy	brzy	k6eAd1	brzy
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
nadaného	nadaný	k2eAgMnSc2d1	nadaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neduživého	duživý	k2eNgMnSc2d1	neduživý
chlapce	chlapec	k1gMnSc2	chlapec
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
jeho	jeho	k3xOp3gFnSc1	jeho
babička	babička	k1gFnSc1	babička
<g/>
.	.	kIx.	.
</s>
