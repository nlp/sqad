<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Broumovsko	Broumovsko	k1gNnSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
BroumovskoIUCN	BroumovskoIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Skalní	skalní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Starostová	Starostová	k1gFnSc1
a	a	k8xC
Starosta	Starosta	k1gMnSc1
v	v	k7c6
Adršpašských	adršpašský	k2eAgFnPc6d1
skaláchZákladní	skaláchZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
430	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Broumovsko	Broumovsko	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Broumov	Broumov	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
<g/>
28	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Broumovsko	Broumovsko	k1gNnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
62	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.broumovsko.ochranaprirody.cz	www.broumovsko.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Broumovsko	Broumovsko	k1gNnSc4
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
geomorfologickém	geomorfologický	k2eAgInSc6d1
celku	celek	k1gInSc6
Broumovská	broumovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
v	v	k7c6
turistickém	turistický	k2eAgInSc6d1
regionu	region	k1gInSc6
Kladské	kladský	k2eAgNnSc4d1
pomezí	pomezí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
43	#num#	k4
000	#num#	k4
ha	ha	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
mj.	mj.	kA
pískovcová	pískovcový	k2eAgNnPc1d1
skalní	skalní	k2eAgNnPc1d1
města	město	k1gNnPc1
a	a	k8xC
stolové	stolový	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
<g/>
:	:	kIx,
Adršpašsko-teplické	adršpašsko-teplický	k2eAgFnPc1d1
skály	skála	k1gFnPc1
<g/>
,	,	kIx,
Broumovské	broumovský	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
</s>
<s>
národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
<g/>
:	:	kIx,
Polické	polický	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
</s>
<s>
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
<g/>
:	:	kIx,
Ostaš	Ostaš	k1gInSc1
<g/>
,	,	kIx,
Křížová	Křížová	k1gFnSc1
cesta	cesta	k1gFnSc1
<g/>
,	,	kIx,
Farní	farní	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
</s>
<s>
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
<g/>
:	:	kIx,
Borek	borek	k1gInSc1
<g/>
,	,	kIx,
Kočičí	kočičí	k2eAgFnPc1d1
skály	skála	k1gFnPc1
<g/>
,	,	kIx,
Šafránová	šafránový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
<g/>
,	,	kIx,
Mořská	mořský	k2eAgFnSc1d1
transgrese	transgrese	k1gFnSc1
<g/>
,	,	kIx,
Pískovcové	pískovcový	k2eAgInPc1d1
sloupky	sloupek	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Broumovsko	Broumovsko	k1gNnSc4
na	na	k7c6
OpenStreetMap	OpenStreetMap	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Náchod	Náchod	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Broumovsko	Broumovsko	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Adršpašsko-teplické	adršpašsko-teplický	k2eAgFnPc1d1
skály	skála	k1gFnPc1
•	•	k?
Broumovské	broumovský	k2eAgFnPc1d1
stěny	stěna	k1gFnPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Babiččino	babiččin	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Polické	polický	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Dubno	Dubno	k6eAd1
–	–	k?
Česká	český	k2eAgFnSc1d1
Skalice	Skalice	k1gFnSc1
•	•	k?
Farní	farní	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Křížová	Křížová	k1gFnSc1
cesta	cesta	k1gFnSc1
•	•	k?
Ostaš	Ostaš	k1gMnSc1
•	•	k?
Peklo	peklo	k1gNnSc4
•	•	k?
Šestajovická	Šestajovický	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Zbytka	Zbytka	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Borek	borek	k1gInSc1
•	•	k?
Březinka	březinka	k1gFnSc1
•	•	k?
Halín	Halín	k1gInSc1
•	•	k?
Hustířanský	Hustířanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Kočičí	kočičí	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Louky	louka	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
Čermné	Čermný	k2eAgFnSc6d1
•	•	k?
Mořská	mořský	k2eAgFnSc1d1
transgrese	transgrese	k1gFnSc1
•	•	k?
Pískovcové	pískovcový	k2eAgInPc1d1
sloupky	sloupek	k1gInPc1
•	•	k?
Pod	pod	k7c7
Rýzmburkem	Rýzmburk	k1gInSc7
•	•	k?
Rašelina	rašelina	k1gFnSc1
•	•	k?
Stará	starý	k2eAgFnSc1d1
Metuje	Metuje	k1gFnSc1
•	•	k?
Šafránová	šafránový	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Tuří	tuří	k2eAgInSc4d1
rybník	rybník	k1gInSc4
Ptačí	ptačí	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Broumovsko	Broumovsko	k6eAd1
•	•	k?
Josefovské	josefovský	k2eAgFnPc4d1
louky	louka	k1gFnPc4
(	(	kIx(
<g/>
nestátní	státní	k2eNgFnSc4d1
<g/>
)	)	kIx)
Evropsky	evropsky	k6eAd1
významné	významný	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
</s>
<s>
Adršpašsko-teplické	adršpašsko-teplický	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Babiččino	babiččin	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
-	-	kIx~
Rýzmburk	Rýzmburk	k1gInSc1
•	•	k?
Broumovské	broumovský	k2eAgFnPc4d1
stěny	stěna	k1gFnPc4
•	•	k?
Březinka	březinka	k1gFnSc1
•	•	k?
Dubno	Dubno	k6eAd1
–	–	k?
Česká	český	k2eAgFnSc1d1
Skalice	Skalice	k1gFnSc1
•	•	k?
Halín	Halín	k1gInSc1
•	•	k?
Hustířanský	Hustířanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Josefov	Josefov	k1gInSc1
-	-	kIx~
pevnost	pevnost	k1gFnSc1
•	•	k?
Kozínek	Kozínek	k1gInSc1
•	•	k?
Metuje	Metuje	k1gFnSc1
a	a	k8xC
Dřevíč	Dřevíč	k1gFnSc1
•	•	k?
Peklo	peklo	k1gNnSc1
•	•	k?
Pevnost	pevnost	k1gFnSc1
Dobrošov	Dobrošov	k1gInSc1
•	•	k?
Pod	pod	k7c7
Rýzmburkem	Rýzmburk	k1gInSc7
•	•	k?
Řeřišný	Řeřišný	k2eAgInSc1d1
u	u	k7c2
Machova	Machův	k2eAgInSc2d1
•	•	k?
Stará	starat	k5eAaImIp3nS
Metuje	Metuje	k1gFnSc1
•	•	k?
Stárkovské	stárkovský	k2eAgFnPc4d1
bučiny	bučina	k1gFnPc4
•	•	k?
Vladivostok	Vladivostok	k1gInSc1
•	•	k?
Zbytka	Zbytka	k1gFnSc1
</s>
