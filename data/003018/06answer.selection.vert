<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
může	moct	k5eAaImIp3nS	moct
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
Taxonomie	taxonomie	k1gFnSc1	taxonomie
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
)	)	kIx)	)
–	–	k?	–
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
teorií	teorie	k1gFnSc7	teorie
a	a	k8xC	a
praxí	praxe	k1gFnSc7	praxe
klasifikace	klasifikace	k1gFnSc2	klasifikace
organismů	organismus	k1gInPc2	organismus
Taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
kategorie	kategorie	k1gFnSc1	kategorie
–	–	k?	–
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
hierarchické	hierarchický	k2eAgFnSc6d1	hierarchická
klasifikaci	klasifikace	k1gFnSc6	klasifikace
Fylogeneze	fylogeneze	k1gFnSc2	fylogeneze
–	–	k?	–
znamená	znamenat	k5eAaImIp3nS	znamenat
vývoj	vývoj	k1gInSc1	vývoj
druhů	druh	k1gInPc2	druh
organismů	organismus	k1gInPc2	organismus
Chemická	chemický	k2eAgFnSc1d1	chemická
klasifikace	klasifikace	k1gFnSc1	klasifikace
–	–	k?	–
základní	základní	k2eAgFnSc1d1	základní
taxonomie	taxonomie	k1gFnSc1	taxonomie
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
Flynnova	Flynnův	k2eAgFnSc1d1	Flynnova
taxonomie	taxonomie	k1gFnSc1	taxonomie
–	–	k?	–
klasifikace	klasifikace	k1gFnSc1	klasifikace
metody	metoda	k1gFnSc2	metoda
paralelismu	paralelismus	k1gInSc2	paralelismus
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
