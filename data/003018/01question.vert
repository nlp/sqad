<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
teorií	teorie	k1gFnSc7	teorie
a	a	k8xC	a
praxí	praxe	k1gFnSc7	praxe
klasifikace	klasifikace	k1gFnSc2	klasifikace
organismů	organismus	k1gInPc2	organismus
<g/>
?	?	kIx.	?
</s>
