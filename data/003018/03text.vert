<s>
Taxonomie	taxonomie	k1gFnSc1	taxonomie
může	moct	k5eAaImIp3nS	moct
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
<g/>
:	:	kIx,	:
Taxonomie	taxonomie	k1gFnSc1	taxonomie
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
)	)	kIx)	)
–	–	k?	–
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
teorií	teorie	k1gFnSc7	teorie
a	a	k8xC	a
praxí	praxe	k1gFnSc7	praxe
klasifikace	klasifikace	k1gFnSc2	klasifikace
organismů	organismus	k1gInPc2	organismus
Taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
kategorie	kategorie	k1gFnSc1	kategorie
–	–	k?	–
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
hierarchické	hierarchický	k2eAgFnSc6d1	hierarchická
klasifikaci	klasifikace	k1gFnSc6	klasifikace
Fylogeneze	fylogeneze	k1gFnSc2	fylogeneze
–	–	k?	–
znamená	znamenat	k5eAaImIp3nS	znamenat
vývoj	vývoj	k1gInSc1	vývoj
druhů	druh	k1gInPc2	druh
organismů	organismus	k1gInPc2	organismus
Chemická	chemický	k2eAgFnSc1d1	chemická
klasifikace	klasifikace	k1gFnSc1	klasifikace
–	–	k?	–
základní	základní	k2eAgFnSc1d1	základní
taxonomie	taxonomie	k1gFnSc1	taxonomie
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
Flynnova	Flynnův	k2eAgFnSc1d1	Flynnova
taxonomie	taxonomie	k1gFnSc1	taxonomie
–	–	k?	–
klasifikace	klasifikace	k1gFnSc1	klasifikace
metody	metoda	k1gFnSc2	metoda
paralelismu	paralelismus	k1gInSc2	paralelismus
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
úrovni	úroveň	k1gFnSc3	úroveň
provádění	provádění	k1gNnSc2	provádění
instrukcí	instrukce	k1gFnSc7	instrukce
Systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
obsahu	obsah	k1gInSc2	obsah
–	–	k?	–
klasifikace	klasifikace	k1gFnSc2	klasifikace
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
resp.	resp.	kA	resp.
v	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
informatice	informatika	k1gFnSc6	informatika
Folksonomie	Folksonomie	k1gFnSc2	Folksonomie
–	–	k?	–
prostředek	prostředek	k1gInSc1	prostředek
pro	pro	k7c4	pro
kategorizaci	kategorizace	k1gFnSc4	kategorizace
obsahu	obsah	k1gInSc2	obsah
internetu	internet	k1gInSc2	internet
Bloomova	Bloomův	k2eAgFnSc1d1	Bloomova
taxonomie	taxonomie	k1gFnSc1	taxonomie
–	–	k?	–
hodnocení	hodnocení	k1gNnSc2	hodnocení
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
cílů	cíl	k1gInPc2	cíl
a	a	k8xC	a
dovedností	dovednost	k1gFnPc2	dovednost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nastavili	nastavit	k5eAaPmAgMnP	nastavit
učitelé	učitel	k1gMnPc1	učitel
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
Nosologie	nosologie	k1gFnSc2	nosologie
–	–	k?	–
obor	obor	k1gInSc1	obor
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
klasifikací	klasifikace	k1gFnSc7	klasifikace
nemocí	nemoc	k1gFnPc2	nemoc
Taxonomie	taxonomie	k1gFnSc2	taxonomie
půd	půda	k1gFnPc2	půda
-	-	kIx~	-
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
klasifikací	klasifikace	k1gFnSc7	klasifikace
půd	půda	k1gFnPc2	půda
Taxon	taxon	k1gInSc1	taxon
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
organismů	organismus	k1gInPc2	organismus
Kategorizace	kategorizace	k1gFnSc2	kategorizace
–	–	k?	–
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
jsou	být	k5eAaImIp3nP	být
myšlenky	myšlenka	k1gFnPc1	myšlenka
a	a	k8xC	a
předměty	předmět	k1gInPc1	předmět
rozeznávané	rozeznávaný	k2eAgFnSc2d1	rozeznávaný
a	a	k8xC	a
rozlišované	rozlišovaný	k2eAgFnSc2d1	rozlišovaná
Ontologie	ontologie	k1gFnSc2	ontologie
(	(	kIx(	(
<g/>
informatika	informatika	k1gFnSc1	informatika
<g/>
)	)	kIx)	)
–	–	k?	–
výslovný	výslovný	k2eAgInSc4d1	výslovný
(	(	kIx(	(
<g/>
explicitní	explicitní	k2eAgInSc4d1	explicitní
<g/>
)	)	kIx)	)
a	a	k8xC	a
formalizovaný	formalizovaný	k2eAgInSc4d1	formalizovaný
popis	popis	k1gInSc4	popis
určité	určitý	k2eAgFnSc2d1	určitá
problematiky	problematika	k1gFnSc2	problematika
Klasifikace	klasifikace	k1gFnSc2	klasifikace
</s>
