<s>
Kimono	kimono	k1gNnSc1	kimono
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
着	着	k?	着
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
oděv	oděv	k1gInSc1	oděv
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
šat	šat	k1gInSc1	šat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
japonský	japonský	k2eAgInSc1d1	japonský
oděv	oděv	k1gInSc1	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
slovo	slovo	k1gNnSc1	slovo
kimono	kimono	k1gNnSc4	kimono
označovalo	označovat	k5eAaImAgNnS	označovat
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
typ	typ	k1gInSc4	typ
tradičního	tradiční	k2eAgMnSc2d1	tradiční
plně	plně	k6eAd1	plně
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
roucha	roucho	k1gNnSc2	roucho
<g/>
.	.	kIx.	.
</s>
<s>
Originální	originální	k2eAgInSc1d1	originální
a	a	k8xC	a
přesný	přesný	k2eAgInSc1d1	přesný
název	název	k1gInSc1	název
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
wafuku	wafuk	k1gMnSc3	wafuk
(	(	kIx(	(
<g/>
和	和	k?	和
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
oblečení	oblečení	k1gNnSc4	oblečení
japonského	japonský	k2eAgInSc2d1	japonský
stylu	styl	k1gInSc2	styl
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnPc1	kimono
jsou	být	k5eAaImIp3nP	být
rovně	rovně	k6eAd1	rovně
střižená	střižený	k2eAgNnPc1d1	střižené
roucha	roucho	k1gNnPc1	roucho
tvaru	tvar	k1gInSc2	tvar
T	T	kA	T
sahající	sahající	k2eAgFnSc1d1	sahající
po	po	k7c4	po
kotníky	kotník	k1gInPc4	kotník
<g/>
,	,	kIx,	,
s	s	k7c7	s
límcem	límec	k1gInSc7	límec
a	a	k8xC	a
širokými	široký	k2eAgInPc7d1	široký
<g/>
,	,	kIx,	,
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
rukávy	rukáv	k1gInPc7	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnPc1	kimono
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
kolem	kolem	k7c2	kolem
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
levou	levý	k2eAgFnSc7d1	levá
stranou	strana	k1gFnSc7	strana
přes	přes	k7c4	přes
pravou	pravá	k1gFnSc4	pravá
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
oblékání	oblékání	k1gNnSc1	oblékání
mrtvého	mrtvý	k1gMnSc2	mrtvý
při	při	k7c6	při
pohřbu	pohřeb	k1gInSc6	pohřeb
<g/>
)	)	kIx)	)
a	a	k8xC	a
upevněná	upevněný	k2eAgNnPc4d1	upevněné
širokým	široký	k2eAgInSc7d1	široký
pásem	pás	k1gInSc7	pás
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
obi	obi	k?	obi
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vázán	vázat	k5eAaImNgInS	vázat
vzadu	vzadu	k6eAd1	vzadu
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnPc1	kimono
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
nosí	nosit	k5eAaImIp3nS	nosit
s	s	k7c7	s
tradiční	tradiční	k2eAgFnSc7d1	tradiční
obuví	obuv	k1gFnSc7	obuv
(	(	kIx(	(
<g/>
především	především	k9	především
s	s	k7c7	s
dřeváky	dřevák	k1gMnPc7	dřevák
geta	geta	k6eAd1	geta
nebo	nebo	k8xC	nebo
sandály	sandál	k1gInPc1	sandál
zóri	zór	k1gFnSc2	zór
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
ponožkami	ponožka	k1gFnPc7	ponožka
<g/>
"	"	kIx"	"
s	s	k7c7	s
odděleným	oddělený	k2eAgInSc7d1	oddělený
palcem	palec	k1gInSc7	palec
–	–	k?	–
tabi	tab	k1gFnSc2	tab
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
kimono	kimono	k1gNnSc4	kimono
nosí	nosit	k5eAaImIp3nP	nosit
především	především	k9	především
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
nosí	nosit	k5eAaImIp3nP	nosit
svobodné	svobodný	k2eAgFnPc1d1	svobodná
ženy	žena	k1gFnPc1	žena
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
příležitostech	příležitost	k1gFnPc6	příležitost
kimono	kimono	k1gNnSc4	kimono
nazývané	nazývaný	k2eAgNnSc4d1	nazývané
furisode	furisod	k1gMnSc5	furisod
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
rukávy	rukáv	k1gInPc4	rukáv
až	až	k8xS	až
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
starších	starší	k1gMnPc2	starší
<g/>
,	,	kIx,	,
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
méně	málo	k6eAd2	málo
mužů	muž	k1gMnPc2	muž
nosí	nosit	k5eAaImIp3nS	nosit
kimono	kimono	k1gNnSc1	kimono
jako	jako	k8xC	jako
běžné	běžný	k2eAgNnSc1d1	běžné
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc4d1	tradiční
japonské	japonský	k2eAgNnSc4d1	Japonské
oblečení	oblečení	k1gNnSc4	oblečení
rovněž	rovněž	k9	rovněž
nosí	nosit	k5eAaImIp3nP	nosit
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
určitých	určitý	k2eAgNnPc2d1	určité
tradičních	tradiční	k2eAgNnPc2d1	tradiční
povolání	povolání	k1gNnPc2	povolání
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
šintoističtí	šintoistický	k2eAgMnPc1d1	šintoistický
a	a	k8xC	a
buddhističtí	buddhistický	k2eAgMnPc1d1	buddhistický
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
šintoistické	šintoistický	k2eAgFnPc1d1	šintoistická
kněžky	kněžka	k1gFnPc1	kněžka
miko	miko	k6eAd1	miko
a	a	k8xC	a
gejši	gejša	k1gFnPc1	gejša
a	a	k8xC	a
osoby	osoba	k1gFnPc1	osoba
věnující	věnující	k2eAgFnPc1d1	věnující
se	se	k3xPyFc4	se
tradičním	tradiční	k2eAgNnSc7d1	tradiční
uměním	umění	k1gNnSc7	umění
(	(	kIx(	(
<g/>
čajový	čajový	k2eAgInSc4d1	čajový
obřad	obřad	k1gInSc4	obřad
<g/>
,	,	kIx,	,
ikebana	ikebana	k1gFnSc1	ikebana
<g/>
,	,	kIx,	,
kaligrafie	kaligrafie	k1gFnSc1	kaligrafie
ap	ap	kA	ap
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
si	se	k3xPyFc3	se
často	často	k6eAd1	často
oblékají	oblékat	k5eAaImIp3nP	oblékat
kimono	kimono	k1gNnSc4	kimono
i	i	k9	i
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
a	a	k8xC	a
formálních	formální	k2eAgFnPc6d1	formální
příležitostech	příležitost	k1gFnPc6	příležitost
(	(	kIx(	(
<g/>
svatbách	svatba	k1gFnPc6	svatba
<g/>
,	,	kIx,	,
pohřbech	pohřeb	k1gInPc6	pohřeb
<g/>
,	,	kIx,	,
promocích	promoce	k1gFnPc6	promoce
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
druhy	druh	k1gInPc4	druh
kimon	kimono	k1gNnPc2	kimono
pak	pak	k6eAd1	pak
nosí	nosit	k5eAaImIp3nP	nosit
ženy	žena	k1gFnPc1	žena
i	i	k8xC	i
muži	muž	k1gMnPc1	muž
při	při	k7c6	při
určitých	určitý	k2eAgInPc6d1	určitý
druzích	druh	k1gInPc6	druh
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
kendó	kendó	k?	kendó
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgMnPc1d1	profesionální
zápasníci	zápasník	k1gMnPc1	zápasník
sumó	sumó	k?	sumó
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vídáni	vídán	k2eAgMnPc1d1	vídán
v	v	k7c6	v
kimonech	kimono	k1gNnPc6	kimono
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
vyžadováno	vyžadován	k2eAgNnSc4d1	vyžadováno
nošení	nošení	k1gNnSc4	nošení
tradičního	tradiční	k2eAgNnSc2d1	tradiční
japonského	japonský	k2eAgNnSc2d1	Japonské
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
kdykoliv	kdykoliv	k6eAd1	kdykoliv
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Nadšenci	nadšenec	k1gMnSc3	nadšenec
pro	pro	k7c4	pro
kimona	kimono	k1gNnPc4	kimono
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
lekce	lekce	k1gFnSc1	lekce
jak	jak	k6eAd1	jak
oblékat	oblékat	k5eAaImF	oblékat
a	a	k8xC	a
nosit	nosit	k5eAaImF	nosit
kimono	kimono	k1gNnSc4	kimono
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
vybírání	vybírání	k1gNnSc3	vybírání
vzorů	vzor	k1gInPc2	vzor
a	a	k8xC	a
látek	látka	k1gFnPc2	látka
odpovídajících	odpovídající	k2eAgInPc2d1	odpovídající
ročnímu	roční	k2eAgNnSc3d1	roční
období	období	k1gNnSc3	období
a	a	k8xC	a
příležitosti	příležitost	k1gFnSc3	příležitost
<g/>
,	,	kIx,	,
sladění	sladění	k1gNnSc3	sladění
spodních	spodní	k2eAgInPc2d1	spodní
dílů	díl	k1gInPc2	díl
kimona	kimono	k1gNnSc2	kimono
a	a	k8xC	a
doplňků	doplněk	k1gInPc2	doplněk
<g/>
,	,	kIx,	,
vrstvení	vrstvení	k1gNnSc1	vrstvení
spodních	spodní	k2eAgInPc2d1	spodní
dílů	díl	k1gInPc2	díl
kimona	kimono	k1gNnSc2	kimono
podle	podle	k7c2	podle
přesného	přesný	k2eAgInSc2d1	přesný
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
a	a	k8xC	a
vázání	vázání	k1gNnSc1	vázání
obi	obi	k?	obi
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
kluby	klub	k1gInPc4	klub
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
kulturu	kultura	k1gFnSc4	kultura
kimona	kimono	k1gNnSc2	kimono
jako	jako	k8xS	jako
Kimono	kimono	k1gNnSc4	kimono
de	de	k?	de
Ginza	Ginza	k1gFnSc1	Ginza
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
kimona	kimono	k1gNnPc4	kimono
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
tradičním	tradiční	k2eAgNnSc7d1	tradiční
čínským	čínský	k2eAgNnSc7d1	čínské
oblečením	oblečení	k1gNnSc7	oblečení
zvaným	zvaný	k2eAgNnSc7d1	zvané
hanfu	hanf	k1gInSc2	hanf
kvůli	kvůli	k7c3	kvůli
značnému	značný	k2eAgNnSc3d1	značné
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
propojení	propojení	k1gNnSc3	propojení
mezi	mezi	k7c7	mezi
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
Japonskem	Japonsko	k1gNnSc7	Japonsko
už	už	k6eAd1	už
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
Avšak	avšak	k8xC	avšak
samotná	samotný	k2eAgFnSc1d1	samotná
čínská	čínský	k2eAgFnSc1d1	čínská
móda	móda	k1gFnSc1	móda
přišla	přijít	k5eAaPmAgFnS	přijít
mezi	mezi	k7c4	mezi
Japonce	Japonec	k1gMnPc4	Japonec
až	až	k9	až
v	v	k7c4	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
překrývání	překrývání	k1gNnSc1	překrývání
límce	límec	k1gInSc2	límec
stalo	stát	k5eAaPmAgNnS	stát
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
ženskou	ženský	k2eAgFnSc4d1	ženská
módu	móda	k1gFnSc4	móda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
japonského	japonský	k2eAgNnSc2d1	Japonské
období	období	k1gNnSc2	období
Heian	Heiany	k1gInPc2	Heiany
(	(	kIx(	(
<g/>
794	[number]	k4	794
-	-	kIx~	-
1192	[number]	k4	1192
<g/>
)	)	kIx)	)
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
kimono	kimono	k1gNnSc1	kimono
více	hodně	k6eAd2	hodně
stylizované	stylizovaný	k2eAgNnSc1d1	stylizované
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
něj	on	k3xPp3gInSc4	on
pořád	pořád	k6eAd1	pořád
nosila	nosit	k5eAaImAgFnS	nosit
polosukně	polosukně	k6eAd1	polosukně
zvaná	zvaný	k2eAgFnSc1d1	zvaná
mo	mo	k?	mo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
Muromači	Muromač	k1gInSc6	Muromač
(	(	kIx(	(
<g/>
1392	[number]	k4	1392
-	-	kIx~	-
1573	[number]	k4	1573
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
nosit	nosit	k5eAaImF	nosit
kosode	kosod	k1gInSc5	kosod
-	-	kIx~	-
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
dříve	dříve	k6eAd2	dříve
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
kimona	kimono	k1gNnSc2	kimono
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k9	už
bez	bez	k7c2	bez
kalhot	kalhoty	k1gFnPc2	kalhoty
zvaných	zvaný	k2eAgFnPc2d1	zvaná
hakama	hakama	k1gNnSc4	hakama
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
dalo	dát	k5eAaPmAgNnS	dát
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
upevňování	upevňování	k1gNnSc4	upevňování
kimon	kimono	k1gNnPc2	kimono
pomocí	pomocí	k7c2	pomocí
pásu	pás	k1gInSc2	pás
obi	obi	k?	obi
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
Edo	Eda	k1gMnSc5	Eda
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
-	-	kIx~	-
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšiřoval	rozšiřovat	k5eAaImAgInS	rozšiřovat
trend	trend	k1gInSc1	trend
prodlužování	prodlužování	k1gNnSc2	prodlužování
rukávů	rukáv	k1gInPc2	rukáv
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
mezi	mezi	k7c7	mezi
svobodnými	svobodný	k2eAgFnPc7d1	svobodná
ženami	žena	k1gFnPc7	žena
obi	obi	k?	obi
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
širší	široký	k2eAgFnSc1d2	širší
<g/>
,	,	kIx,	,
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
novými	nový	k2eAgInPc7d1	nový
styly	styl	k1gInPc7	styl
vázání	vázání	k1gNnSc2	vázání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jak	jak	k6eAd1	jak
ženská	ženský	k2eAgFnSc1d1	ženská
tak	tak	k8xC	tak
mužská	mužský	k2eAgNnPc1d1	mužské
kimona	kimono	k1gNnPc1	kimono
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nezměněna	změněn	k2eNgFnSc1d1	nezměněna
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnSc2	kimono
pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
jsou	být	k5eAaImIp3nP	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
velikostech	velikost	k1gFnPc6	velikost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kimona	kimono	k1gNnPc1	kimono
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
stejně	stejně	k6eAd1	stejně
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přizpůsobována	přizpůsobován	k2eAgNnPc4d1	přizpůsobováno
různým	různý	k2eAgInPc3d1	různý
rozměrům	rozměr	k1gInPc3	rozměr
těla	tělo	k1gNnSc2	tělo
zahýbáním	zahýbání	k1gNnSc7	zahýbání
a	a	k8xC	a
skládáním	skládání	k1gNnSc7	skládání
<g/>
.	.	kIx.	.
</s>
<s>
Ideálně	ideálně	k6eAd1	ideálně
ušité	ušitý	k2eAgNnSc1d1	ušité
kimono	kimono	k1gNnSc1	kimono
má	mít	k5eAaImIp3nS	mít
rukávy	rukáv	k1gInPc4	rukáv
končící	končící	k2eAgInPc4d1	končící
u	u	k7c2	u
zápěstí	zápěstí	k1gNnSc2	zápěstí
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
ruce	ruka	k1gFnPc4	ruka
spuštěné	spuštěný	k2eAgFnPc4d1	spuštěná
<g/>
.	.	kIx.	.
</s>
<s>
Mužské	mužský	k2eAgNnSc1d1	mužské
kimono	kimono	k1gNnSc1	kimono
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
mužova	mužův	k2eAgFnSc1d1	mužova
postava	postava	k1gFnSc1	postava
a	a	k8xC	a
přebytečná	přebytečný	k2eAgFnSc1d1	přebytečná
délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
v	v	k7c6	v
pase	pas	k1gInSc6	pas
zapošívá	zapošívat	k5eAaImIp3nS	zapošívat
<g/>
.	.	kIx.	.
</s>
<s>
Ženské	ženská	k1gFnSc3	ženská
kimona	kimono	k1gNnSc2	kimono
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
asi	asi	k9	asi
o	o	k7c4	o
20	[number]	k4	20
cm	cm	kA	cm
než	než	k8xS	než
postava	postava	k1gFnSc1	postava
jeho	jeho	k3xOp3gFnSc2	jeho
majitelky	majitelka	k1gFnSc2	majitelka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
ohašori	ohašore	k1gFnSc4	ohašore
-	-	kIx~	-
sklad	sklad	k1gInSc4	sklad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
pod	pod	k7c7	pod
obi	obi	k?	obi
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnPc1	kimono
jsou	být	k5eAaImIp3nP	být
vyráběna	vyrábět	k5eAaImNgNnP	vyrábět
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
látky	látka	k1gFnSc2	látka
zvaného	zvaný	k2eAgInSc2d1	zvaný
tan	tan	k?	tan
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kusy	kus	k1gInPc1	kus
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
36	[number]	k4	36
cm	cm	kA	cm
široké	široký	k2eAgFnSc2d1	široká
a	a	k8xC	a
11,5	[number]	k4	11,5
metru	metr	k1gInSc2	metr
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
(	(	kIx(	(
<g/>
dostatečně	dostatečně	k6eAd1	dostatečně
pro	pro	k7c4	pro
kimono	kimono	k1gNnSc4	kimono
pro	pro	k7c4	pro
dospělého	dospělý	k1gMnSc4	dospělý
<g/>
)	)	kIx)	)
a	a	k8xC	a
kimono	kimono	k1gNnSc1	kimono
se	se	k3xPyFc4	se
šije	šít	k5eAaImIp3nS	šít
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
kusu	kus	k1gInSc2	kus
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Hotové	hotový	k2eAgNnSc1d1	hotové
kimono	kimono	k1gNnSc1	kimono
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
hlavních	hlavní	k2eAgInPc2d1	hlavní
pruhů	pruh	k1gInPc2	pruh
látky	látka	k1gFnSc2	látka
<g/>
:	:	kIx,	:
dva	dva	k4xCgMnPc1	dva
kryjí	krýt	k5eAaImIp3nP	krýt
tělo	tělo	k1gNnSc1	tělo
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
tvoří	tvořit	k5eAaImIp3nS	tvořit
rukávy	rukáv	k1gInPc1	rukáv
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
menší	malý	k2eAgInPc1d2	menší
pruhy	pruh	k1gInPc1	pruh
tvoří	tvořit	k5eAaImIp3nP	tvořit
úzký	úzký	k2eAgInSc4d1	úzký
přední	přední	k2eAgInSc4d1	přední
díl	díl	k1gInSc4	díl
a	a	k8xC	a
límec	límec	k1gInSc4	límec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
praní	praní	k1gNnSc6	praní
kimono	kimono	k1gNnSc1	kimono
obvykle	obvykle	k6eAd1	obvykle
rozpáráno	rozpárat	k5eAaPmNgNnS	rozpárat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
opět	opět	k6eAd1	opět
ručně	ručně	k6eAd1	ručně
sešito	sešit	k2eAgNnSc1d1	sešito
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kusy	kus	k1gInPc1	kus
látky	látka	k1gFnSc2	látka
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kimona	kimono	k1gNnSc2	kimono
v	v	k7c6	v
celku	celek	k1gInSc6	celek
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kimono	kimono	k1gNnSc4	kimono
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
přešito	přešit	k2eAgNnSc1d1	přešit
pro	pro	k7c4	pro
jiného	jiný	k2eAgMnSc4d1	jiný
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
rukávu	rukáv	k1gInSc2	rukáv
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
šíří	šíř	k1gFnSc7	šíř
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
středu	střed	k1gInSc2	střed
páteře	páteř	k1gFnSc2	páteř
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
rukávu	rukáv	k1gInSc2	rukáv
nemůže	moct	k5eNaImIp3nS	moct
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
délku	délka	k1gFnSc4	délka
dvounásobku	dvounásobek	k1gInSc2	dvounásobek
šíře	šíř	k1gFnSc2	šíř
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnPc1d1	tradiční
látky	látka	k1gFnPc1	látka
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kimona	kimono	k1gNnSc2	kimono
nebyly	být	k5eNaImAgFnP	být
širší	široký	k2eAgFnSc4d2	širší
než	než	k8xS	než
36	[number]	k4	36
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
páteře	páteř	k1gFnSc2	páteř
k	k	k7c3	k
zápěstí	zápěstí	k1gNnSc3	zápěstí
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
68	[number]	k4	68
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
materiály	materiál	k1gInPc1	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kimona	kimono	k1gNnSc2	kimono
jsou	být	k5eAaImIp3nP	být
tkány	tkát	k5eAaImNgFnP	tkát
v	v	k7c6	v
šíři	šíř	k1gFnSc6	šíř
42	[number]	k4	42
cm	cm	kA	cm
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyhovovaly	vyhovovat	k5eAaImAgInP	vyhovovat
současným	současný	k2eAgInPc3d1	současný
rozměrům	rozměr	k1gInPc3	rozměr
Japonců	Japonec	k1gMnPc2	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
vysocí	vysoký	k2eAgMnPc1d1	vysoký
nebo	nebo	k8xC	nebo
silní	silný	k2eAgMnPc1d1	silný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zápasníci	zápasník	k1gMnPc1	zápasník
sumó	sumó	k?	sumó
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
kimona	kimono	k1gNnPc4	kimono
šitá	šitý	k2eAgNnPc4d1	šité
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
z	z	k7c2	z
více	hodně	k6eAd2	hodně
kusů	kus	k1gInPc2	kus
látky	látka	k1gFnSc2	látka
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
látek	látka	k1gFnPc2	látka
utkaných	utkaný	k2eAgFnPc2d1	utkaná
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
kimona	kimono	k1gNnPc1	kimono
jsou	být	k5eAaImIp3nP	být
ručně	ručně	k6eAd1	ručně
šitá	šitý	k2eAgNnPc1d1	šité
a	a	k8xC	a
i	i	k9	i
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
ručně	ručně	k6eAd1	ručně
tkané	tkaný	k2eAgInPc4d1	tkaný
a	a	k8xC	a
dekorované	dekorovaný	k2eAgInPc4d1	dekorovaný
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
techniky	technika	k1gFnPc1	technika
jako	jako	k8xC	jako
barvení	barvení	k1gNnSc1	barvení
júzen	júzna	k1gFnPc2	júzna
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
aplikaci	aplikace	k1gFnSc4	aplikace
zdobení	zdobení	k1gNnPc2	zdobení
a	a	k8xC	a
vzorů	vzor	k1gInPc2	vzor
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
látce	látka	k1gFnSc6	látka
<g/>
.	.	kIx.	.
</s>
<s>
Opakující	opakující	k2eAgFnSc1d1	opakující
se	s	k7c7	s
vzory	vzor	k1gInPc7	vzor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
velkou	velký	k2eAgFnSc4d1	velká
plochu	plocha	k1gFnSc4	plocha
kimona	kimono	k1gNnSc2	kimono
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
zhotovovány	zhotovovat	k5eAaImNgInP	zhotovovat
technikou	technika	k1gFnSc7	technika
júzen	júzna	k1gFnPc2	júzna
a	a	k8xC	a
šablonou	šablona	k1gFnSc7	šablona
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
probíhaly	probíhat	k5eAaImAgFnP	probíhat
různé	různý	k2eAgFnPc1d1	různá
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
látkách	látka	k1gFnPc6	látka
<g/>
,	,	kIx,	,
stylech	styl	k1gInPc6	styl
i	i	k8xC	i
v	v	k7c6	v
doplňcích	doplněk	k1gInPc6	doplněk
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
obi	obi	k?	obi
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnPc1	kimono
a	a	k8xC	a
obi	obi	k?	obi
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
z	z	k7c2	z
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
hedvábného	hedvábný	k2eAgInSc2d1	hedvábný
brokátu	brokát	k1gInSc2	brokát
<g/>
,	,	kIx,	,
hedvábného	hedvábný	k2eAgInSc2d1	hedvábný
krepu	krep	k1gInSc2	krep
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
čirimen	čirimen	k1gInSc1	čirimen
<g/>
)	)	kIx)	)
a	a	k8xC	a
saténových	saténový	k2eAgFnPc2d1	saténová
tkanin	tkanina	k1gFnPc2	tkanina
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
rinzu	rinza	k1gFnSc4	rinza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
kimona	kimono	k1gNnPc1	kimono
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
široce	široko	k6eAd1	široko
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
levnějších	levný	k2eAgFnPc6d2	levnější
a	a	k8xC	a
na	na	k7c4	na
údržbu	údržba	k1gFnSc4	údržba
snazších	snadný	k2eAgFnPc2d2	snazší
materiálech	materiál	k1gInPc6	materiál
jakou	jaký	k3yRgFnSc4	jaký
je	on	k3xPp3gNnSc4	on
umělé	umělý	k2eAgNnSc4d1	umělé
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
,	,	kIx,	,
bavlněný	bavlněný	k2eAgInSc4d1	bavlněný
satén	satén	k1gInSc4	satén
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
polyester	polyester	k1gInSc1	polyester
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
syntetická	syntetický	k2eAgNnPc1d1	syntetické
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Hedvábí	hedvábí	k1gNnSc1	hedvábí
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
považovanou	považovaný	k2eAgFnSc4d1	považovaná
za	za	k7c4	za
ideální	ideální	k2eAgInSc4d1	ideální
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nutností	nutnost	k1gFnSc7	nutnost
pro	pro	k7c4	pro
formální	formální	k2eAgFnPc4d1	formální
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zvyku	zvyk	k1gInSc2	zvyk
jsou	být	k5eAaImIp3nP	být
tkané	tkaný	k2eAgInPc1d1	tkaný
vzory	vzor	k1gInPc1	vzor
a	a	k8xC	a
opakující	opakující	k2eAgInPc1d1	opakující
se	se	k3xPyFc4	se
barvené	barvený	k2eAgInPc1d1	barvený
vzory	vzor	k1gInPc1	vzor
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
neformální	formální	k2eNgInSc4d1	neformální
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgNnSc1d1	formální
kimono	kimono	k1gNnSc1	kimono
má	mít	k5eAaImIp3nS	mít
vzor	vzor	k1gInSc4	vzor
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
stylu	styl	k1gInSc6	styl
malovaný	malovaný	k2eAgInSc4d1	malovaný
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
nebo	nebo	k8xC	nebo
kolem	kolem	k7c2	kolem
okraje	okraj	k1gInSc2	okraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
Heian	Heiana	k1gFnPc2	Heiana
se	se	k3xPyFc4	se
kimono	kimono	k1gNnSc1	kimono
nosilo	nosit	k5eAaImAgNnS	nosit
s	s	k7c7	s
až	až	k6eAd1	až
12	[number]	k4	12
barevně	barevně	k6eAd1	barevně
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
vrstvami	vrstva	k1gFnPc7	vrstva
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
barevná	barevný	k2eAgFnSc1d1	barevná
kombinace	kombinace	k1gFnSc1	kombinace
měla	mít	k5eAaImAgFnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
a	a	k8xC	a
vzorec	vzorec	k1gInSc4	vzorec
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
kimono	kimono	k1gNnSc4	kimono
obvykle	obvykle	k6eAd1	obvykle
jeden	jeden	k4xCgInSc4	jeden
vrchní	vrchní	k2eAgInSc4d1	vrchní
díl	díl	k1gInSc4	díl
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
spodničku	spodnička	k1gFnSc4	spodnička
<g/>
.	.	kIx.	.
</s>
<s>
Vzor	vzor	k1gInSc1	vzor
kimona	kimono	k1gNnSc2	kimono
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
napovídat	napovídat	k5eAaBmF	napovídat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
nosit	nosit	k5eAaImF	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
kimono	kimono	k1gNnSc1	kimono
se	s	k7c7	s
vzorem	vzor	k1gInSc7	vzor
s	s	k7c7	s
motýli	motýl	k1gMnPc1	motýl
nebo	nebo	k8xC	nebo
květy	květ	k1gInPc4	květ
třešně	třešeň	k1gFnSc2	třešeň
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nosilo	nosit	k5eAaImAgNnS	nosit
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Vzory	vzor	k1gInPc1	vzor
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgInPc1d1	běžný
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
podzimním	podzimní	k2eAgInSc7d1	podzimní
motivem	motiv	k1gInSc7	motiv
je	být	k5eAaImIp3nS	být
červenohnědý	červenohnědý	k2eAgInSc1d1	červenohnědý
list	list	k1gInSc1	list
japonského	japonský	k2eAgInSc2d1	japonský
javoru	javor	k1gInSc2	javor
a	a	k8xC	a
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
mohou	moct	k5eAaImIp3nP	moct
motivy	motiv	k1gInPc4	motiv
znázorňovat	znázorňovat	k5eAaImF	znázorňovat
bambus	bambus	k1gInSc4	bambus
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnPc4	borovice
a	a	k8xC	a
květy	květ	k1gInPc4	květ
švestky	švestka	k1gFnSc2	švestka
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgNnPc1d1	staré
kimona	kimono	k1gNnPc1	kimono
se	se	k3xPyFc4	se
často	často	k6eAd1	často
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
"	"	kIx"	"
<g/>
recyklují	recyklovat	k5eAaBmIp3nP	recyklovat
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
upravují	upravovat	k5eAaImIp3nP	upravovat
se	se	k3xPyFc4	se
na	na	k7c6	na
haori	haor	k1gInSc6	haor
<g/>
,	,	kIx,	,
hijoku	hijok	k1gInSc6	hijok
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kimona	kimono	k1gNnPc1	kimono
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
podobných	podobný	k2eAgNnPc2d1	podobné
kimon	kimono	k1gNnPc2	kimono
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kabelek	kabelka	k1gFnPc2	kabelka
a	a	k8xC	a
podobných	podobný	k2eAgInPc2d1	podobný
doplňků	doplněk	k1gInPc2	doplněk
<g/>
,	,	kIx,	,
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
obalů	obal	k1gInPc2	obal
<g/>
,	,	kIx,	,
tašek	taška	k1gFnPc2	taška
a	a	k8xC	a
krabiček	krabička	k1gFnPc2	krabička
pro	pro	k7c4	pro
různý	různý	k2eAgInSc4d1	různý
obsah	obsah	k1gInSc4	obsah
-	-	kIx~	-
zvlášť	zvlášť	k6eAd1	zvlášť
na	na	k7c4	na
sladkosti	sladkost	k1gFnPc4	sladkost
pro	pro	k7c4	pro
čajové	čajový	k2eAgInPc4d1	čajový
obřady	obřad	k1gInPc4	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Kimono	kimono	k1gNnSc1	kimono
s	s	k7c7	s
poškozením	poškození	k1gNnSc7	poškození
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
pasu	pást	k5eAaImIp1nS	pást
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
nosit	nosit	k5eAaImF	nosit
s	s	k7c7	s
kalhotami	kalhoty	k1gFnPc7	kalhoty
hakama	hakamum	k1gNnSc2	hakamum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
skryla	skrýt	k5eAaPmAgFnS	skrýt
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zkušení	zkušený	k2eAgMnPc1d1	zkušený
řemeslníci	řemeslník	k1gMnPc1	řemeslník
pracně	pracně	k6eAd1	pracně
vybírali	vybírat	k5eAaImAgMnP	vybírat
hedvábné	hedvábný	k2eAgFnPc4d1	hedvábná
nitě	nit	k1gFnPc4	nit
ze	z	k7c2	z
starých	starý	k2eAgNnPc2d1	staré
kimon	kimono	k1gNnPc2	kimono
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tkali	tkát	k5eAaImAgMnP	tkát
látku	látka	k1gFnSc4	látka
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
heko	heko	k1gNnSc1	heko
obi	obi	k?	obi
pro	pro	k7c4	pro
mužská	mužský	k2eAgNnPc4d1	mužské
kimona	kimono	k1gNnPc4	kimono
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
používání	používání	k1gNnSc2	používání
"	"	kIx"	"
<g/>
recyklované	recyklovaný	k2eAgFnSc2d1	recyklovaná
příze	příz	k1gFnSc2	příz
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
saki-ori	sakiri	k6eAd1	saki-ori
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnPc1	kimono
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
drahá	drahý	k2eAgFnSc1d1	drahá
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
ženského	ženský	k2eAgNnSc2d1	ženské
kimona	kimono	k1gNnSc2	kimono
může	moct	k5eAaImIp3nS	moct
snadno	snadno	k6eAd1	snadno
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
10	[number]	k4	10
000	[number]	k4	000
US	US	kA	US
<g/>
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInPc1d1	kompletní
šaty	šat	k1gInPc1	šat
-	-	kIx~	-
kimono	kimono	k1gNnSc1	kimono
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnPc1d1	spodní
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
obi	obi	k?	obi
<g/>
,	,	kIx,	,
vázanky	vázanka	k1gFnPc1	vázanka
<g/>
,	,	kIx,	,
tabi	tabi	k1gNnSc1	tabi
<g/>
,	,	kIx,	,
obuv	obuv	k1gFnSc1	obuv
a	a	k8xC	a
doplňky	doplněk	k1gInPc1	doplněk
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
20	[number]	k4	20
000	[number]	k4	000
US	US	kA	US
<g/>
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
samotné	samotný	k2eAgNnSc1d1	samotné
obi	obi	k?	obi
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
kimon	kimono	k1gNnPc2	kimono
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
nadšenci	nadšenec	k1gMnPc7	nadšenec
nebo	nebo	k8xC	nebo
umělci	umělec	k1gMnPc1	umělec
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
tradičním	tradiční	k2eAgNnSc7d1	tradiční
uměním	umění	k1gNnSc7	umění
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Zruční	zručný	k2eAgMnPc1d1	zručný
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
šijí	šít	k5eAaImIp3nP	šít
vlastní	vlastní	k2eAgNnPc4d1	vlastní
kimona	kimono	k1gNnPc4	kimono
a	a	k8xC	a
spodní	spodní	k2eAgInPc4d1	spodní
díly	díl	k1gInPc4	díl
podle	podle	k7c2	podle
standardních	standardní	k2eAgInPc2d1	standardní
vzorů	vzor	k1gInPc2	vzor
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
recyklováním	recyklování	k1gNnPc3	recyklování
<g/>
"	"	kIx"	"
starých	starý	k2eAgNnPc2d1	staré
kimon	kimono	k1gNnPc2	kimono
<g/>
.	.	kIx.	.
</s>
<s>
Levnější	levný	k2eAgFnSc1d2	levnější
a	a	k8xC	a
strojově	strojově	k6eAd1	strojově
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
látky	látka	k1gFnPc1	látka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
tradiční	tradiční	k2eAgFnPc4d1	tradiční
<g/>
,	,	kIx,	,
ručně	ručně	k6eAd1	ručně
barvené	barvený	k2eAgNnSc4d1	barvené
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
i	i	k9	i
použitá	použitý	k2eAgNnPc1d1	Použité
kimona	kimono	k1gNnPc1	kimono
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
cenových	cenový	k2eAgFnPc6d1	cenová
relacích	relace	k1gFnPc6	relace
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
kupující	kupující	k2eAgNnSc4d1	kupující
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
v	v	k7c6	v
bazaru	bazar	k1gInSc6	bazar
či	či	k8xC	či
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
se	s	k7c7	s
starožitnostmi	starožitnost	k1gFnPc7	starožitnost
sehnat	sehnat	k5eAaPmF	sehnat
poměrně	poměrně	k6eAd1	poměrně
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
hedvábné	hedvábný	k2eAgNnSc4d1	hedvábné
kimono	kimono	k1gNnSc4	kimono
nebo	nebo	k8xC	nebo
brokátové	brokátový	k2eAgInPc1d1	brokátový
obi	obi	k?	obi
za	za	k7c4	za
pouhých	pouhý	k2eAgFnPc2d1	pouhá
2000	[number]	k4	2000
jenů	jen	k1gInPc2	jen
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
ženská	ženská	k1gFnSc1	ženská
obi	obi	k?	obi
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nákladnou	nákladný	k2eAgFnSc7d1	nákladná
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
bývají	bývat	k5eAaImIp3nP	bývat
dražší	drahý	k2eAgNnSc4d2	dražší
než	než	k8xS	než
kimono	kimono	k1gNnSc4	kimono
samotné	samotný	k2eAgNnSc4d1	samotné
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
výrobu	výroba	k1gFnSc4	výroba
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zkušených	zkušený	k2eAgMnPc2d1	zkušený
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1	mužská
obi	obi	k?	obi
<g/>
,	,	kIx,	,
i	i	k8xC	i
ta	ten	k3xDgFnSc1	ten
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
užší	úzký	k2eAgInPc4d2	užší
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgInPc4d2	kratší
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
ženská	ženská	k1gFnSc1	ženská
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnPc1	kimono
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
extrémně	extrémně	k6eAd1	extrémně
formálních	formální	k2eAgInPc2d1	formální
až	až	k9	až
po	po	k7c4	po
běžná	běžný	k2eAgNnPc4d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
formálnosti	formálnost	k1gFnSc2	formálnost
u	u	k7c2	u
ženských	ženský	k2eAgNnPc2d1	ženské
kimon	kimono	k1gNnPc2	kimono
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
určena	určit	k5eAaPmNgFnS	určit
vzorem	vzor	k1gInSc7	vzor
<g/>
,	,	kIx,	,
látkou	látka	k1gFnSc7	látka
a	a	k8xC	a
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
ženy	žena	k1gFnPc1	žena
nosí	nosit	k5eAaImIp3nP	nosit
kimono	kimono	k1gNnSc4	kimono
s	s	k7c7	s
delšími	dlouhý	k2eAgInPc7d2	delší
rukávy	rukáv	k1gInPc7	rukáv
značící	značící	k2eAgFnSc2d1	značící
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
svobodné	svobodný	k2eAgInPc1d1	svobodný
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
kimona	kimono	k1gNnPc1	kimono
bývají	bývat	k5eAaImIp3nP	bývat
propracovanější	propracovaný	k2eAgNnPc1d2	propracovanější
než	než	k8xS	než
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
formální	formální	k2eAgNnPc1d1	formální
kimona	kimono	k1gNnPc1	kimono
pro	pro	k7c4	pro
starší	starý	k2eAgFnPc4d2	starší
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Mužská	mužský	k2eAgNnPc1d1	mužské
kimona	kimono	k1gNnPc1	kimono
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
stejný	stejný	k2eAgInSc4d1	stejný
střih	střih	k1gInSc4	střih
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
tlumených	tlumený	k2eAgFnPc6d1	tlumená
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Formálnost	formálnost	k1gFnSc1	formálnost
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
určována	určovat	k5eAaImNgFnS	určovat
typem	typ	k1gInSc7	typ
a	a	k8xC	a
barvou	barva	k1gFnSc7	barva
doplňků	doplněk	k1gInPc2	doplněk
<g/>
,	,	kIx,	,
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
počtem	počet	k1gInSc7	počet
nebo	nebo	k8xC	nebo
absencí	absence	k1gFnSc7	absence
kamon	kamona	k1gFnPc2	kamona
(	(	kIx(	(
<g/>
rodinné	rodinný	k2eAgInPc1d1	rodinný
znaky	znak	k1gInPc1	znak
<g/>
)	)	kIx)	)
-	-	kIx~	-
5	[number]	k4	5
znaků	znak	k1gInPc2	znak
značí	značit	k5eAaImIp3nS	značit
extrémní	extrémní	k2eAgFnSc1d1	extrémní
formálnost	formálnost	k1gFnSc1	formálnost
<g/>
.	.	kIx.	.
</s>
<s>
Hedvábí	hedvábí	k1gNnSc1	hedvábí
je	být	k5eAaImIp3nS	být
nejvhodnějším	vhodný	k2eAgInSc7d3	nejvhodnější
a	a	k8xC	a
nejformálnějším	formální	k2eAgInSc7d3	formální
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnPc1	kimono
šitá	šitý	k2eAgNnPc1d1	šité
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
nebo	nebo	k8xC	nebo
polyesteru	polyester	k1gInSc2	polyester
jsou	být	k5eAaImIp3nP	být
neformální	formální	k2eNgFnPc1d1	neformální
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
moderních	moderní	k2eAgFnPc2d1	moderní
Japonek	Japonka	k1gFnPc2	Japonka
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
při	při	k7c6	při
oblékání	oblékání	k1gNnSc6	oblékání
kimona	kimono	k1gNnSc2	kimono
neobejde	obejde	k6eNd1	obejde
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
ženské	ženský	k2eAgNnSc1d1	ženské
kimono	kimono	k1gNnSc1	kimono
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
12	[number]	k4	12
a	a	k8xC	a
více	hodně	k6eAd2	hodně
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nP	nosit
<g/>
,	,	kIx,	,
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
a	a	k8xC	a
upevňují	upevňovat	k5eAaImIp3nP	upevňovat
předepsaným	předepsaný	k2eAgInSc7d1	předepsaný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
asistence	asistence	k1gFnSc1	asistence
licencovaného	licencovaný	k2eAgMnSc2d1	licencovaný
oblékače	oblékač	k1gMnSc2	oblékač
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
objednávají	objednávat	k5eAaImIp3nP	objednávat
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Oblékači	oblékač	k1gMnPc1	oblékač
kimona	kimono	k1gNnSc2	kimono
pracují	pracovat	k5eAaImIp3nP	pracovat
také	také	k9	také
v	v	k7c6	v
kadeřnických	kadeřnický	k2eAgInPc6d1	kadeřnický
salónech	salón	k1gInPc6	salón
a	a	k8xC	a
jezdí	jezdit	k5eAaImIp3nP	jezdit
i	i	k9	i
k	k	k7c3	k
zákazníkům	zákazník	k1gMnPc3	zákazník
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
odpovídajícího	odpovídající	k2eAgInSc2d1	odpovídající
typu	typ	k1gInSc2	typ
kimona	kimono	k1gNnSc2	kimono
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
symbolice	symbolika	k1gFnSc6	symbolika
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
věk	věk	k1gInSc4	věk
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
úroveň	úroveň	k1gFnSc4	úroveň
a	a	k8xC	a
stupeň	stupeň	k1gInSc4	stupeň
formálnosti	formálnost	k1gFnSc2	formálnost
dané	daný	k2eAgFnSc2d1	daná
příležitosti	příležitost	k1gFnSc2	příležitost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
黒	黒	k?	黒
<g/>
)	)	kIx)	)
černé	černý	k2eAgNnSc1d1	černé
kimono	kimono	k1gNnSc1	kimono
se	s	k7c7	s
vzorem	vzor	k1gInSc7	vzor
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
pasu	pas	k1gInSc2	pas
<g/>
.	.	kIx.	.
</s>
<s>
Kurotomesode	Kurotomesod	k1gMnSc5	Kurotomesod
je	být	k5eAaImIp3nS	být
nejformálnější	formální	k2eAgNnSc4d3	formální
kimono	kimono	k1gNnSc4	kimono
pro	pro	k7c4	pro
vdané	vdaný	k2eAgFnPc4d1	vdaná
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	on	k3xPp3gInPc4	on
nosí	nosit	k5eAaImIp3nP	nosit
matky	matka	k1gFnPc4	matka
nevěsty	nevěsta	k1gFnSc2	nevěsta
a	a	k8xC	a
ženicha	ženich	k1gMnSc2	ženich
na	na	k7c6	na
svatbách	svatba	k1gFnPc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Kurotomesode	Kurotomesod	k1gMnSc5	Kurotomesod
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
pět	pět	k4xCc4	pět
rodových	rodový	k2eAgInPc2d1	rodový
znaků	znak	k1gInPc2	znak
kamon	kamona	k1gFnPc2	kamona
vytištěných	vytištěný	k2eAgFnPc2d1	vytištěná
na	na	k7c6	na
rukávech	rukáv	k1gInPc6	rukáv
<g/>
,	,	kIx,	,
hrudi	hruď	k1gFnPc1	hruď
a	a	k8xC	a
zádech	zádech	k1gInSc1	zádech
kimona	kimono	k1gNnSc2	kimono
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
振	振	k?	振
<g/>
)	)	kIx)	)
furisode	furisod	k1gInSc5	furisod
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
vlající	vlající	k2eAgInPc4d1	vlající
rukávy	rukáv	k1gInPc4	rukáv
-	-	kIx~	-
rukávy	rukáv	k1gInPc4	rukáv
furisode	furisod	k1gInSc5	furisod
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
100	[number]	k4	100
-	-	kIx~	-
106	[number]	k4	106
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Furisode	Furisod	k1gMnSc5	Furisod
je	být	k5eAaImIp3nS	být
nejformálnější	formální	k2eAgNnSc4d3	formální
kimono	kimono	k1gNnSc4	kimono
pro	pro	k7c4	pro
svobodné	svobodný	k2eAgFnPc4d1	svobodná
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
s	s	k7c7	s
barevným	barevný	k2eAgInSc7d1	barevný
vzorem	vzor	k1gInSc7	vzor
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
roucha	roucho	k1gNnSc2	roucho
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
při	při	k7c6	při
svátku	svátek	k1gInSc6	svátek
dospělosti	dospělost	k1gFnSc2	dospělost
(	(	kIx(	(
<g/>
seidžin	seidžin	k1gInSc1	seidžin
šiki	šik	k1gFnSc2	šik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
promoci	promoce	k1gFnSc4	promoce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	on	k3xPp3gMnPc4	on
nosí	nosit	k5eAaImIp3nS	nosit
svobodné	svobodný	k2eAgMnPc4d1	svobodný
příbuzné	příbuzný	k1gMnPc4	příbuzný
nevěsty	nevěsta	k1gFnSc2	nevěsta
a	a	k8xC	a
ženicha	ženich	k1gMnSc2	ženich
při	při	k7c6	při
svatební	svatební	k2eAgFnSc6d1	svatební
hostině	hostina	k1gFnSc6	hostina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
色	色	k?	色
<g/>
)	)	kIx)	)
jednobarevné	jednobarevný	k2eAgNnSc1d1	jednobarevné
kimono	kimono	k1gNnSc1	kimono
se	s	k7c7	s
vzorem	vzor	k1gInSc7	vzor
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
pasu	pas	k1gInSc2	pas
<g/>
.	.	kIx.	.
</s>
<s>
Irotomesode	Irotomesod	k1gMnSc5	Irotomesod
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
formální	formální	k2eAgMnSc1d1	formální
než	než	k8xS	než
kurotomesode	kurotomesod	k1gMnSc5	kurotomesod
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nP	nosit
je	on	k3xPp3gFnPc4	on
vdané	vdaný	k2eAgFnPc4d1	vdaná
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
blízké	blízký	k2eAgMnPc4d1	blízký
příbuzné	příbuzný	k1gMnPc4	příbuzný
nevěsty	nevěsta	k1gFnSc2	nevěsta
a	a	k8xC	a
ženicha	ženich	k1gMnSc2	ženich
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Irotomesode	Irotomesod	k1gMnSc5	Irotomesod
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
tři	tři	k4xCgInPc4	tři
až	až	k9	až
pět	pět	k4xCc4	pět
kamon	kamona	k1gFnPc2	kamona
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
訪	訪	k?	訪
<g/>
)	)	kIx)	)
doslovně	doslovně	k6eAd1	doslovně
znamená	znamenat	k5eAaImIp3nS	znamenat
oblečení	oblečení	k1gNnSc4	oblečení
na	na	k7c4	na
návštěvy	návštěva	k1gFnPc4	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
vzorem	vzor	k1gInSc7	vzor
který	který	k3yIgInSc1	který
přechází	přecházet	k5eAaImIp3nS	přecházet
přes	přes	k7c4	přes
ramena	rameno	k1gNnPc4	rameno
<g/>
,	,	kIx,	,
lemy	lem	k1gInPc4	lem
a	a	k8xC	a
rukávy	rukáv	k1gInPc4	rukáv
<g/>
.	.	kIx.	.
</s>
<s>
Hómongi	Hómongi	k6eAd1	Hómongi
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgNnSc4d2	vyšší
postavení	postavení	k1gNnSc4	postavení
než	než	k8xS	než
jeho	jeho	k3xOp3gInSc1	jeho
blízký	blízký	k2eAgInSc1d1	blízký
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
Cukesage	Cukesage	k1gInSc1	Cukesage
<g/>
.	.	kIx.	.
</s>
<s>
Hómongi	Hómongi	k6eAd1	Hómongi
mohou	moct	k5eAaImIp3nP	moct
nosit	nosit	k5eAaImF	nosit
svobodné	svobodný	k2eAgFnPc1d1	svobodná
i	i	k8xC	i
vdané	vdaný	k2eAgFnPc1d1	vdaná
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	on	k3xPp3gInPc4	on
nosí	nosit	k5eAaImIp3nS	nosit
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
nevěsty	nevěsta	k1gFnSc2	nevěsta
s	s	k7c7	s
ženicha	ženich	k1gMnSc2	ženich
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
recepcích	recepce	k1gFnPc6	recepce
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nošeno	nosit	k5eAaImNgNnS	nosit
i	i	k9	i
při	při	k7c6	při
formálních	formální	k2eAgInPc6d1	formální
večírcích	večírek	k1gInPc6	večírek
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
付	付	k?	付
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
jednodušší	jednoduchý	k2eAgInSc1d2	jednodušší
vzor	vzor	k1gInSc1	vzor
který	který	k3yIgInSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
menší	malý	k2eAgFnSc4d2	menší
plochu	plocha	k1gFnSc4	plocha
-	-	kIx~	-
většinou	většina	k1gFnSc7	většina
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
pasu	pást	k5eAaImIp1nS	pást
-	-	kIx~	-
než	než	k8xS	než
formálnější	formální	k2eAgFnSc4d2	formálnější
Hómongi	Hómonge	k1gFnSc4	Hómonge
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
je	on	k3xPp3gNnSc4	on
nosit	nosit	k5eAaImF	nosit
i	i	k9	i
vdané	vdaný	k2eAgFnPc4d1	vdaná
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
色	色	k?	色
<g/>
)	)	kIx)	)
jednobarevné	jednobarevný	k2eAgNnSc1d1	jednobarevné
kimono	kimono	k1gNnSc1	kimono
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
nosit	nosit	k5eAaImF	nosit
svobodné	svobodný	k2eAgFnPc1d1	svobodná
i	i	k8xC	i
vdané	vdaný	k2eAgFnPc1d1	vdaná
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
při	při	k7c6	při
čajových	čajový	k2eAgInPc6d1	čajový
obřadech	obřad	k1gInPc6	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Barvené	barvený	k2eAgNnSc1d1	barvené
hedvábí	hedvábí	k1gNnSc1	hedvábí
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
vzor	vzor	k1gInSc4	vzor
(	(	kIx(	(
<g/>
rinzu	rinza	k1gFnSc4	rinza
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc1d1	podobný
žakardu	žakarda	k1gFnSc4	žakarda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgNnSc1	žádný
barevně	barevně	k6eAd1	barevně
odlišné	odlišný	k2eAgNnSc1d1	odlišné
vzorování	vzorování	k1gNnSc1	vzorování
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
小	小	k?	小
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
jemný	jemný	k2eAgInSc1d1	jemný
vzor	vzor	k1gInSc1	vzor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
kimona	kimono	k1gNnPc1	kimono
s	s	k7c7	s
jemným	jemný	k2eAgInSc7d1	jemný
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
vzorem	vzor	k1gInSc7	vzor
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
rouchu	roucho	k1gNnSc6	roucho
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
kimona	kimono	k1gNnSc2	kimono
je	být	k5eAaImIp3nS	být
neformální	formální	k2eNgNnSc1d1	neformální
<g/>
,	,	kIx,	,
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
s	s	k7c7	s
neformálním	formální	k2eNgNnSc7d1	neformální
obi	obi	k?	obi
neboli	neboli	k8xC	neboli
nagoja	nagoja	k1gMnSc1	nagoja
obi	obi	k?	obi
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
běžné	běžný	k2eAgNnSc4d1	běžné
"	"	kIx"	"
<g/>
vycházkové	vycházkový	k2eAgNnSc4d1	vycházkové
<g/>
"	"	kIx"	"
nošení	nošení	k1gNnSc4	nošení
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
ho	on	k3xPp3gNnSc4	on
nosit	nosit	k5eAaImF	nosit
svobodné	svobodný	k2eAgFnPc1d1	svobodná
i	i	k8xC	i
vdané	vdaný	k2eAgFnPc1d1	vdaná
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
江	江	k?	江
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
kimona	kimono	k1gNnSc2	kimono
komon	komona	k1gFnPc2	komona
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
drobnými	drobný	k2eAgFnPc7d1	drobná
tečkami	tečka	k1gFnPc7	tečka
v	v	k7c6	v
hustém	hustý	k2eAgInSc6d1	hustý
vzoru	vzor	k1gInSc6	vzor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
větší	veliký	k2eAgInPc4d2	veliký
obrazce	obrazec	k1gInPc4	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technika	k1gFnSc1	technika
barvení	barvení	k1gNnSc2	barvení
Edo	Eda	k1gMnSc5	Eda
komon	komon	k1gMnSc1	komon
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
samurajů	samuraj	k1gMnPc2	samuraj
z	z	k7c2	z
období	období	k1gNnSc2	období
Edo	Eda	k1gMnSc5	Eda
<g/>
.	.	kIx.	.
</s>
<s>
Kimono	kimono	k1gNnSc1	kimono
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
vzorem	vzor	k1gInSc7	vzor
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
jako	jako	k8xS	jako
iromudži	iromudž	k1gFnSc6	iromudž
a	a	k8xC	a
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
kamon	kamon	k1gInSc4	kamon
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nošeno	nosit	k5eAaImNgNnS	nosit
jako	jako	k9	jako
oblečení	oblečení	k1gNnSc3	oblečení
na	na	k7c4	na
návštěvy	návštěva	k1gFnPc4	návštěva
(	(	kIx(	(
<g/>
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
cukesage	cukesag	k1gFnSc2	cukesag
nebo	nebo	k8xC	nebo
hómongi	hómong	k1gFnSc2	hómong
<g/>
)	)	kIx)	)
Učikake	Učikake	k1gInSc1	Učikake
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
formální	formální	k2eAgNnSc4d1	formální
kimono	kimono	k1gNnSc4	kimono
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nosí	nosit	k5eAaImIp3nS	nosit
jen	jen	k9	jen
nevěsty	nevěsta	k1gFnPc4	nevěsta
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tanečnice	tanečnice	k1gFnPc1	tanečnice
či	či	k8xC	či
herci	herec	k1gMnPc1	herec
při	při	k7c6	při
uměleckých	umělecký	k2eAgNnPc6d1	umělecké
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Učikake	Učikake	k1gFnSc1	Učikake
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
bohatě	bohatě	k6eAd1	bohatě
zdobené	zdobený	k2eAgNnSc1d1	zdobené
a	a	k8xC	a
nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
vlastní	vlastní	k2eAgNnSc4d1	vlastní
kimono	kimono	k1gNnSc4	kimono
a	a	k8xC	a
obi	obi	k?	obi
jako	jako	k8xC	jako
plášť	plášť	k1gInSc1	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
ně	on	k3xPp3gMnPc4	on
obi	obi	k?	obi
nikdy	nikdy	k6eAd1	nikdy
neváže	vázat	k5eNaImIp3nS	vázat
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
táhnout	táhnout	k5eAaImF	táhnout
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
spodní	spodní	k2eAgInSc1d1	spodní
lem	lem	k1gInSc1	lem
zatížený	zatížený	k2eAgInSc1d1	zatížený
vycpáním	vycpání	k1gNnPc3	vycpání
<g/>
.	.	kIx.	.
</s>
<s>
Učikake	Učikake	k1gFnSc1	Učikake
nevěsty	nevěsta	k1gFnSc2	nevěsta
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
úplně	úplně	k6eAd1	úplně
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
barevné	barevný	k2eAgNnSc1d1	barevné
-	-	kIx~	-
častá	častý	k2eAgFnSc1d1	častá
základní	základní	k2eAgFnSc1d1	základní
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Susohiki	Susohiki	k6eAd1	Susohiki
nejčastěji	často	k6eAd3	často
nosí	nosit	k5eAaImIp3nP	nosit
gejši	gejša	k1gFnPc1	gejša
nebo	nebo	k8xC	nebo
tanečnice	tanečnice	k1gFnPc1	tanečnice
při	při	k7c6	při
vystoupeních	vystoupení	k1gNnPc6	vystoupení
tradičního	tradiční	k2eAgInSc2d1	tradiční
japonského	japonský	k2eAgInSc2d1	japonský
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
normálnímu	normální	k2eAgNnSc3d1	normální
kimonu	kimono	k1gNnSc3	kimono
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
vléct	vléct	k5eAaImF	vléct
po	po	k7c6	po
podlaze	podlaha	k1gFnSc6	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Susohiki	Susohiki	k6eAd1	Susohiki
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
vléct	vléct	k5eAaImF	vléct
spodní	spodní	k2eAgInSc4d1	spodní
lem	lem	k1gInSc4	lem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgNnSc1d1	běžné
kimono	kimono	k1gNnSc1	kimono
pro	pro	k7c4	pro
ženu	žena	k1gFnSc4	žena
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
přibližně	přibližně	k6eAd1	přibližně
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
1,6	[number]	k4	1,6
m	m	kA	m
<g/>
,	,	kIx,	,
susohiki	susohiki	k6eAd1	susohiki
může	moct	k5eAaImIp3nS	moct
měřit	měřit	k5eAaImF	měřit
až	až	k9	až
2	[number]	k4	2
m.	m.	k?	m.
Gejši	gejša	k1gFnSc2	gejša
a	a	k8xC	a
maiko	maiko	k6eAd1	maiko
proto	proto	k8xC	proto
nosí	nosit	k5eAaImIp3nS	nosit
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
venku	venku	k6eAd1	venku
sukni	sukně	k1gFnSc4	sukně
přizvednutou	přizvednutý	k2eAgFnSc4d1	přizvednutá
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
také	také	k6eAd1	také
mohou	moct	k5eAaImIp3nP	moct
ukázat	ukázat	k5eAaPmF	ukázat
své	svůj	k3xOyFgFnPc4	svůj
krásné	krásný	k2eAgFnPc4d1	krásná
"	"	kIx"	"
<g/>
spodničky	spodnička	k1gFnPc4	spodnička
<g/>
"	"	kIx"	"
nagadžuban	nagadžuban	k1gMnSc1	nagadžuban
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ženských	ženský	k2eAgNnPc2d1	ženské
kimon	kimono	k1gNnPc2	kimono
jsou	být	k5eAaImIp3nP	být
ta	ten	k3xDgNnPc1	ten
mužská	mužský	k2eAgNnPc1d1	mužské
mnohem	mnohem	k6eAd1	mnohem
jednodušší	jednoduchý	k2eAgNnPc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
maximálně	maximálně	k6eAd1	maximálně
pěti	pět	k4xCc2	pět
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
nepočítáme	počítat	k5eNaImIp1nP	počítat
<g/>
-li	i	k?	-li
obuv	obuv	k1gFnSc4	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Rukávy	rukáv	k1gInPc1	rukáv
mužského	mužský	k2eAgNnSc2d1	mužské
kimona	kimono	k1gNnSc2	kimono
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
centimetrů	centimetr	k1gInPc2	centimetr
na	na	k7c6	na
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
volných	volný	k2eAgNnPc2d1	volné
<g/>
.	.	kIx.	.
</s>
<s>
Ženská	ženský	k2eAgNnPc1d1	ženské
kimona	kimono	k1gNnPc1	kimono
mají	mít	k5eAaImIp3nP	mít
naopak	naopak	k6eAd1	naopak
rukávy	rukáv	k1gInPc1	rukáv
většinou	většinou	k6eAd1	většinou
otevřené	otevřený	k2eAgInPc1d1	otevřený
<g/>
.	.	kIx.	.
</s>
<s>
Rukávy	rukáv	k1gInPc1	rukáv
mužského	mužský	k2eAgNnSc2d1	mužské
kimona	kimono	k1gNnSc2	kimono
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nepřekážely	překážet	k5eNaImAgFnP	překážet
při	při	k7c6	při
vázáni	vázat	k5eAaImNgMnP	vázat
obi	obi	k?	obi
kolem	kolem	k7c2	kolem
pasu	pas	k1gInSc2	pas
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
ženského	ženský	k2eAgNnSc2d1	ženské
kimona	kimono	k1gNnSc2	kimono
mohou	moct	k5eAaImIp3nP	moct
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
volné	volný	k2eAgInPc4d1	volný
rukávy	rukáv	k1gInPc4	rukáv
viset	viset	k5eAaImF	viset
kolem	kolem	k7c2	kolem
obi	obi	k?	obi
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
překážely	překážet	k5eAaImAgFnP	překážet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgInPc7d1	mužský
kimony	kimono	k1gNnPc7	kimono
druh	druh	k1gInSc4	druh
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
kimono	kimono	k1gNnSc1	kimono
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tmavých	tmavý	k2eAgFnPc6d1	tmavá
tlumených	tlumený	k2eAgFnPc6d1	tlumená
barvách	barva	k1gFnPc6	barva
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
a	a	k8xC	a
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
matné	matný	k2eAgInPc1d1	matný
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
materiály	materiál	k1gInPc1	materiál
mají	mít	k5eAaImIp3nP	mít
jemný	jemný	k2eAgInSc4d1	jemný
vzor	vzor	k1gInSc4	vzor
a	a	k8xC	a
kimona	kimono	k1gNnSc2	kimono
pro	pro	k7c4	pro
denní	denní	k2eAgNnSc4d1	denní
nošení	nošení	k1gNnSc4	nošení
se	se	k3xPyFc4	se
často	často	k6eAd1	často
šijí	šít	k5eAaImIp3nP	šít
z	z	k7c2	z
texturovaných	texturovaný	k2eAgFnPc2d1	texturovaná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Kimona	kimono	k1gNnPc1	kimono
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
nošení	nošení	k1gNnSc4	nošení
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
jasnější	jasný	k2eAgFnPc4d2	jasnější
barvy	barva	k1gFnPc4	barva
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgFnSc1d2	světlejší
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Zápasníci	zápasník	k1gMnPc1	zápasník
sumo	suma	k1gFnSc5	suma
někdy	někdy	k6eAd1	někdy
nosí	nosit	k5eAaImIp3nS	nosit
i	i	k9	i
kimono	kimono	k1gNnSc4	kimono
v	v	k7c6	v
docela	docela	k6eAd1	docela
jasných	jasný	k2eAgFnPc6d1	jasná
barvách	barva	k1gFnPc6	barva
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
fuchsiová	fuchsiový	k2eAgFnSc1d1	fuchsiová
<g/>
.	.	kIx.	.
</s>
<s>
Nejformálnější	formální	k2eAgNnSc1d3	formální
mužské	mužský	k2eAgNnSc1d1	mužské
kimono	kimono	k1gNnSc1	kimono
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
černé	černá	k1gFnSc2	černá
s	s	k7c7	s
pěti	pět	k4xCc2	pět
kamon	kamona	k1gFnPc2	kamona
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
,	,	kIx,	,
rukávech	rukáv	k1gInPc6	rukáv
a	a	k8xC	a
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
formální	formální	k2eAgInSc1d1	formální
jsou	být	k5eAaImIp3nP	být
kimona	kimono	k1gNnPc4	kimono
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
kamon	kamona	k1gFnPc2	kamona
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nosí	nosit	k5eAaImIp3nS	nosit
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
spodními	spodní	k2eAgInPc7d1	spodní
díly	díl	k1gInPc7	díl
a	a	k8xC	a
doplňky	doplněk	k1gInPc7	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
každé	každý	k3xTgNnSc1	každý
kimono	kimono	k1gNnSc1	kimono
může	moct	k5eAaImIp3nS	moct
získat	získat	k5eAaPmF	získat
na	na	k7c6	na
formálnosti	formálnost	k1gFnSc6	formálnost
přidáním	přidání	k1gNnSc7	přidání
kalhot	kalhoty	k1gFnPc2	kalhoty
hakama	hakamum	k1gNnSc2	hakamum
a	a	k8xC	a
haori	haori	k1gNnSc2	haori
<g/>
.	.	kIx.	.
</s>
<s>
Nagadžuban	Nagadžuban	k1gInSc1	Nagadžuban
(	(	kIx(	(
<g/>
長	長	k?	長
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
džuban	džubany	k1gInPc2	džubany
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plášť	plášť	k1gInSc4	plášť
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kimona	kimono	k1gNnSc2	kimono
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nosí	nosit	k5eAaImIp3nP	nosit
ženy	žena	k1gFnPc1	žena
i	i	k8xC	i
muži	muž	k1gMnPc1	muž
pod	pod	k7c7	pod
vrchním	vrchní	k2eAgNnSc7d1	vrchní
rouchem	roucho	k1gNnSc7	roucho
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
hedvábná	hedvábný	k2eAgNnPc1d1	hedvábné
kimona	kimono	k1gNnPc1	kimono
jsou	být	k5eAaImIp3nP	být
náročná	náročný	k2eAgNnPc1d1	náročné
na	na	k7c4	na
údržbu	údržba	k1gFnSc4	údržba
a	a	k8xC	a
čištění	čištění	k1gNnSc4	čištění
<g/>
,	,	kIx,	,
nagadžuban	nagadžuban	k1gInSc4	nagadžuban
je	on	k3xPp3gMnPc4	on
chrání	chránit	k5eAaImIp3nS	chránit
od	od	k7c2	od
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
pokožkou	pokožka	k1gFnSc7	pokožka
nositele	nositel	k1gMnSc2	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
nagadžuban	nagadžubany	k1gInPc2	nagadžubany
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
pouze	pouze	k6eAd1	pouze
límec	límec	k1gInSc4	límec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nepřekrývá	překrývat	k5eNaImIp3nS	překrývat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
kimono	kimono	k1gNnSc4	kimono
<g/>
.	.	kIx.	.
</s>
<s>
Nagadžuban	Nagadžuban	k1gInSc1	Nagadžuban
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
vyměnitelný	vyměnitelný	k2eAgInSc1d1	vyměnitelný
límec	límec	k1gInSc1	límec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
vrchnímu	vrchní	k2eAgNnSc3d1	vrchní
kimonu	kimono	k1gNnSc3	kimono
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
šel	jít	k5eAaImAgMnS	jít
lehce	lehko	k6eAd1	lehko
vyměnit	vyměnit	k5eAaPmF	vyměnit
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
celý	celý	k2eAgInSc1d1	celý
oděv	oděv	k1gInSc1	oděv
prát	prát	k5eAaImF	prát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
nejformálnější	formální	k2eAgInSc4d3	formální
typ	typ	k1gInSc4	typ
Nagadžuban	Nagadžubana	k1gFnPc2	Nagadžubana
čistě	čistě	k6eAd1	čistě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
krásně	krásně	k6eAd1	krásně
zdobené	zdobený	k2eAgNnSc4d1	zdobené
a	a	k8xC	a
vzorované	vzorovaný	k2eAgNnSc4d1	vzorované
jako	jako	k8xC	jako
vnější	vnější	k2eAgNnSc4d1	vnější
kimono	kimono	k1gNnSc4	kimono
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
mužská	mužský	k2eAgNnPc1d1	mužské
kimona	kimono	k1gNnPc1	kimono
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
velmi	velmi	k6eAd1	velmi
nenápadné	nápadný	k2eNgInPc1d1	nenápadný
vzory	vzor	k1gInPc1	vzor
a	a	k8xC	a
tlumené	tlumený	k2eAgFnPc1d1	tlumená
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
nagadžuban	nagadžuban	k1gInSc1	nagadžuban
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
skrytě	skrytě	k6eAd1	skrytě
použít	použít	k5eAaPmF	použít
velmi	velmi	k6eAd1	velmi
nápadité	nápaditý	k2eAgInPc4d1	nápaditý
motivy	motiv	k1gInPc4	motiv
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Hadadžuban	Hadadžuban	k1gInSc1	Hadadžuban
(	(	kIx(	(
<g/>
肌	肌	k?	肌
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jemná	jemný	k2eAgFnSc1d1	jemná
spodnička	spodnička	k1gFnSc1	spodnička
vypadající	vypadající	k2eAgFnSc1d1	vypadající
jako	jako	k8xS	jako
zjednodušené	zjednodušený	k2eAgNnSc1d1	zjednodušené
kimono	kimono	k1gNnSc1	kimono
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
je	on	k3xPp3gFnPc4	on
ženy	žena	k1gFnPc4	žena
pod	pod	k7c4	pod
nagadžuban	nagadžuban	k1gInSc4	nagadžuban
<g/>
.	.	kIx.	.
</s>
<s>
Susojoke	Susojoke	k1gFnSc1	Susojoke
(	(	kIx(	(
<g/>
裾	裾	k?	裾
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jemná	jemný	k2eAgFnSc1d1	jemná
zavinovací	zavinovací	k2eAgFnSc1d1	zavinovací
sukně	sukně	k1gFnSc1	sukně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
pod	pod	k7c4	pod
nagadžuban	nagadžuban	k1gInSc4	nagadžuban
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
susojoke	susojoke	k1gInSc1	susojoke
a	a	k8xC	a
hadadžuban	hadadžuban	k1gInSc1	hadadžuban
spojen	spojit	k5eAaPmNgInS	spojit
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
kus	kus	k1gInSc4	kus
<g/>
.	.	kIx.	.
</s>
<s>
Geta	Geta	k1gFnSc1	Geta
(	(	kIx(	(
<g/>
下	下	k?	下
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dřeváky	dřevák	k1gInPc1	dřevák
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nosí	nosit	k5eAaImIp3nP	nosit
ženy	žena	k1gFnPc4	žena
i	i	k8xC	i
muži	muž	k1gMnPc1	muž
společně	společně	k6eAd1	společně
s	s	k7c7	s
jukata	jukat	k2eAgNnPc4d1	jukat
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
typ	typ	k1gInSc1	typ
dřeváků	dřevák	k1gMnPc2	dřevák
zvané	zvaný	k2eAgFnSc2d1	zvaná
okobo	okoba	k1gFnSc5	okoba
nosí	nosit	k5eAaImIp3nP	nosit
pouze	pouze	k6eAd1	pouze
gejši	gejša	k1gFnPc1	gejša
<g/>
.	.	kIx.	.
</s>
<s>
Hakama	Hakama	k1gFnSc1	Hakama
(	(	kIx(	(
<g/>
袴	袴	k?	袴
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dělená	dělený	k2eAgFnSc1d1	dělená
(	(	kIx(	(
<g/>
Umanori	Umanore	k1gFnSc4	Umanore
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nedělená	dělený	k2eNgFnSc1d1	nedělená
sukně	sukně	k1gFnSc1	sukně
(	(	kIx(	(
<g/>
Andon	Andon	k1gNnSc1	Andon
<g/>
)	)	kIx)	)
připomínající	připomínající	k2eAgFnPc1d1	připomínající
široké	široký	k2eAgFnPc1d1	široká
kalhoty	kalhoty	k1gFnPc1	kalhoty
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tradičně	tradičně	k6eAd1	tradičně
nosí	nosit	k5eAaImIp3nP	nosit
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
je	on	k3xPp3gInPc4	on
nosí	nosit	k5eAaImIp3nP	nosit
i	i	k9	i
ženy	žena	k1gFnPc1	žena
jako	jako	k8xC	jako
méně	málo	k6eAd2	málo
formální	formální	k2eAgInSc1d1	formální
obleční	obleční	k2eAgInSc1d1	obleční
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Hakama	Hakama	k1gFnSc1	Hakama
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
plisování	plisování	k1gNnSc1	plisování
-	-	kIx~	-
košiita	košiita	k1gFnSc1	košiita
(	(	kIx(	(
<g/>
pevná	pevný	k2eAgFnSc1d1	pevná
nebo	nebo	k8xC	nebo
vycpaná	vycpaný	k2eAgFnSc1d1	vycpaná
část	část	k1gFnSc1	část
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
zad	záda	k1gNnPc2	záda
nositele	nositel	k1gMnSc2	nositel
<g/>
)	)	kIx)	)
a	a	k8xC	a
himo	himo	k6eAd1	himo
(	(	kIx(	(
<g/>
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
pruhy	pruh	k1gInPc4	pruh
látky	látka	k1gFnPc1	látka
vázané	vázaný	k2eAgFnPc1d1	vázaná
v	v	k7c6	v
pase	pas	k1gInSc6	pas
přes	přes	k7c4	přes
obi	obi	k?	obi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hakama	Hakama	k1gFnSc1	Hakama
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nosí	nosit	k5eAaImIp3nS	nosit
při	při	k7c6	při
bojových	bojový	k2eAgInPc6d1	bojový
sportech	sport	k1gInPc6	sport
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
aikido	aikida	k1gFnSc5	aikida
<g/>
,	,	kIx,	,
kendó	kendó	k?	kendó
<g/>
,	,	kIx,	,
iaidó	iaidó	k?	iaidó
a	a	k8xC	a
naginatadó	naginatadó	k?	naginatadó
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
nosí	nosit	k5eAaImIp3nP	nosit
hakama	hakama	k1gNnSc4	hakama
při	při	k7c6	při
promocích	promoce	k1gFnPc6	promoce
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
velmi	velmi	k6eAd1	velmi
formálních	formální	k2eAgInPc2d1	formální
až	až	k9	až
po	po	k7c4	po
běžné	běžný	k2eAgNnSc4d1	běžné
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc4	rozdělení
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
vzoru	vzor	k1gInSc6	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Haori	Haori	k1gNnSc1	Haori
(	(	kIx(	(
<g/>
羽	羽	k?	羽
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kabátec	kabátec	k1gInSc4	kabátec
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
buď	buď	k8xC	buď
do	do	k7c2	do
pasu	pas	k1gInSc2	pas
nebo	nebo	k8xC	nebo
do	do	k7c2	do
půli	půle	k1gFnSc4	půle
stehen	stehno	k1gNnPc2	stehno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dodává	dodávat	k5eAaImIp3nS	dodávat
oblečení	oblečení	k1gNnSc4	oblečení
na	na	k7c6	na
formalitě	formalita	k1gFnSc6	formalita
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
nosili	nosit	k5eAaImAgMnP	nosit
haori	haori	k6eAd1	haori
výhradně	výhradně	k6eAd1	výhradně
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nezměnila	změnit	k5eNaPmAgFnS	změnit
móda	móda	k1gFnSc1	móda
na	na	k7c6	na
konci	konec	k1gInSc6	konec
období	období	k1gNnSc2	období
Meidži	Meidž	k1gFnSc3	Meidž
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	on	k3xPp3gInPc4	on
nosí	nosit	k5eAaImIp3nP	nosit
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ženská	ženský	k2eAgNnPc4d1	ženské
haori	haori	k1gNnPc4	haori
bývají	bývat	k5eAaImIp3nP	bývat
delší	dlouhý	k2eAgInPc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Haori-himo	Haoriimo	k6eAd1	Haori-himo
(	(	kIx(	(
<g/>
羽	羽	k?	羽
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pletená	pletený	k2eAgFnSc1d1	pletená
tkanice	tkanice	k1gFnSc1	tkanice
se	s	k7c7	s
střapci	střapec	k1gInPc7	střapec
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
svazuje	svazovat	k5eAaImIp3nS	svazovat
haori	haori	k6eAd1	haori
<g/>
.	.	kIx.	.
</s>
<s>
Nejformálnější	formální	k2eAgFnSc7d3	nejformálnější
barvou	barva	k1gFnSc7	barva
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Hijoku	Hijok	k1gInSc2	Hijok
(	(	kIx(	(
<g/>
ひ	ひ	k?	ひ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
spodního	spodní	k2eAgNnSc2d1	spodní
kimona	kimono	k1gNnSc2	kimono
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dřív	dříve	k6eAd2	dříve
nosily	nosit	k5eAaImAgFnP	nosit
ženy	žena	k1gFnPc1	žena
pod	pod	k7c7	pod
vlastním	vlastní	k2eAgNnSc7d1	vlastní
kimonem	kimono	k1gNnSc7	kimono
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	on	k3xPp3gInPc4	on
nosí	nosit	k5eAaImIp3nS	nosit
už	už	k6eAd1	už
jen	jen	k9	jen
při	při	k7c6	při
svatbách	svatba	k1gFnPc6	svatba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
jiných	jiný	k2eAgFnPc6d1	jiná
významných	významný	k2eAgFnPc6d1	významná
společenských	společenský	k2eAgFnPc6d1	společenská
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Kanzaši	Kanzasat	k5eAaPmIp1nSwK	Kanzasat
(	(	kIx(	(
<g/>
簪	簪	k?	簪
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
tradiční	tradiční	k2eAgFnPc4d1	tradiční
jehlice	jehlice	k1gFnPc4	jehlice
do	do	k7c2	do
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nosí	nosit	k5eAaImIp3nP	nosit
ženy	žena	k1gFnPc4	žena
ve	v	k7c6	v
složitých	složitý	k2eAgInPc6d1	složitý
účesech	účes	k1gInPc6	účes
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
často	často	k6eAd1	často
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
kimono	kimono	k1gNnSc4	kimono
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
materiály	materiál	k1gInPc1	materiál
jsou	být	k5eAaImIp3nP	být
stříbro	stříbro	k1gNnSc4	stříbro
či	či	k8xC	či
jiné	jiný	k2eAgInPc4d1	jiný
kovy	kov	k1gInPc4	kov
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
kombinované	kombinovaný	k2eAgFnSc2d1	kombinovaná
s	s	k7c7	s
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
lak	lak	k1gInSc1	lak
nebo	nebo	k8xC	nebo
želvovina	želvovina	k1gFnSc1	želvovina
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
však	však	k9	však
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
i	i	k9	i
kanzaši	kanzaš	k1gMnPc1	kanzaš
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
nových	nový	k2eAgInPc2d1	nový
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Kuši	kuše	k1gFnSc4	kuše
(	(	kIx(	(
<g/>
櫛	櫛	k?	櫛
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hřebínek	hřebínek	k1gInSc1	hřebínek
do	do	k7c2	do
vlasů	vlas	k1gInPc2	vlas
nošený	nošený	k2eAgInSc4d1	nošený
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
účesu	účes	k1gInSc6	účes
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
materiály	materiál	k1gInPc1	materiál
jsou	být	k5eAaImIp3nP	být
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
lak	lak	k1gInSc1	lak
nebo	nebo	k8xC	nebo
želvovina	želvovina	k1gFnSc1	želvovina
<g/>
.	.	kIx.	.
</s>
<s>
Kógai	Kógai	k1gNnSc1	Kógai
(	(	kIx(	(
<g/>
笄	笄	k?	笄
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jiný	jiný	k2eAgInSc1d1	jiný
typ	typ	k1gInSc1	typ
tradiční	tradiční	k2eAgFnSc2d1	tradiční
jehlice	jehlice	k1gFnSc2	jehlice
do	do	k7c2	do
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
používaný	používaný	k2eAgInSc1d1	používaný
pouze	pouze	k6eAd1	pouze
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jehlice	jehlice	k1gFnSc2	jehlice
kanzaši	kanzasat	k5eAaPmIp1nSwK	kanzasat
je	být	k5eAaImIp3nS	být
masivnější	masivní	k2eAgFnSc4d2	masivnější
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
konce	konec	k1gInPc1	konec
jsou	být	k5eAaImIp3nP	být
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
hřebínky	hřebínek	k1gInPc4	hřebínek
kuši	kuše	k1gFnSc4	kuše
<g/>
.	.	kIx.	.
</s>
<s>
Materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
želvovina	želvovina	k1gFnSc1	želvovina
nebo	nebo	k8xC	nebo
lakované	lakovaný	k2eAgNnSc1d1	lakované
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Obi	Obi	k?	Obi
(	(	kIx(	(
<g/>
帯	帯	k?	帯
<g/>
)	)	kIx)	)
obi	obi	k?	obi
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
ozdobného	ozdobný	k2eAgInSc2d1	ozdobný
pásu	pás	k1gInSc2	pás
nošeného	nošený	k2eAgInSc2d1	nošený
společně	společně	k6eAd1	společně
s	s	k7c7	s
kimonem	kimono	k1gNnSc7	kimono
muži	muž	k1gMnPc7	muž
i	i	k8xC	i
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Obiage	Obiage	k1gFnSc1	Obiage
(	(	kIx(	(
<g/>
帯	帯	k?	帯
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ozdobný	ozdobný	k2eAgInSc4d1	ozdobný
kus	kus	k1gInSc4	kus
látky	látka	k1gFnSc2	látka
podobný	podobný	k2eAgInSc4d1	podobný
šátku	šátek	k1gInSc2	šátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
horní	horní	k2eAgInSc4d1	horní
okraj	okraj	k1gInSc4	okraj
formálního	formální	k2eAgMnSc2d1	formální
obi	obi	k?	obi
<g/>
.	.	kIx.	.
</s>
<s>
Zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
tak	tak	k9	tak
všechny	všechen	k3xTgFnPc1	všechen
spodní	spodní	k2eAgFnPc1d1	spodní
tkanice	tkanice	k1gFnPc1	tkanice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
eventuálně	eventuálně	k6eAd1	eventuálně
vyčuhovat	vyčuhovat	k5eAaImF	vyčuhovat
zpod	zpod	k7c2	zpod
obi	obi	k?	obi
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
ještě	ještě	k6eAd1	ještě
posiluje	posilovat	k5eAaImIp3nS	posilovat
pevnost	pevnost	k1gFnSc4	pevnost
uvázaného	uvázaný	k2eAgInSc2d1	uvázaný
obi	obi	k?	obi
<g/>
.	.	kIx.	.
</s>
<s>
Obidžime	Obidžimat	k5eAaPmIp3nS	Obidžimat
(	(	kIx(	(
<g/>
帯	帯	k?	帯
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ozdobná	ozdobný	k2eAgFnSc1d1	ozdobná
tkanice	tkanice	k1gFnSc1	tkanice
vázaná	vázaný	k2eAgFnSc1d1	vázaná
přes	přes	k7c4	přes
formální	formální	k2eAgNnSc4d1	formální
obi	obi	k?	obi
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
účel	účel	k1gInSc4	účel
nejen	nejen	k6eAd1	nejen
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
praktický	praktický	k2eAgInSc1d1	praktický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obi	obi	k?	obi
drželo	držet	k5eAaImAgNnS	držet
pevně	pevně	k6eAd1	pevně
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
místo	místo	k1gNnSc1	místo
obidžime	obidžimat	k5eAaPmIp3nS	obidžimat
tzv.	tzv.	kA	tzv.
obidome	obidom	k1gInSc5	obidom
<g/>
,	,	kIx,	,
tkanice	tkanice	k1gFnPc1	tkanice
se	s	k7c7	s
speciální	speciální	k2eAgFnSc7d1	speciální
drobnou	drobný	k2eAgFnSc7d1	drobná
zdobenou	zdobený	k2eAgFnSc7d1	zdobená
sponou	spona	k1gFnSc7	spona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
spíná	spínat	k5eAaImIp3nS	spínat
vepředu	vepředu	k6eAd1	vepředu
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
tak	tak	k6eAd1	tak
i	i	k9	i
jako	jako	k9	jako
nenápadný	nápadný	k2eNgInSc1d1	nenápadný
doplněk	doplněk	k1gInSc1	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Obi-ita	Obita	k1gFnSc1	Obi-ita
(	(	kIx(	(
<g/>
帯	帯	k?	帯
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tenká	tenký	k2eAgFnSc1d1	tenká
pevná	pevný	k2eAgFnSc1d1	pevná
podložka	podložka	k1gFnSc1	podložka
potažená	potažený	k2eAgFnSc1d1	potažená
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
vkládaná	vkládaný	k2eAgFnSc1d1	vkládaná
pod	pod	k7c7	pod
ženská	ženský	k2eAgFnSc1d1	ženská
obi	obi	k?	obi
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
držela	držet	k5eAaImAgFnS	držet
tvar	tvar	k1gInSc4	tvar
obi	obi	k?	obi
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
říká	říkat	k5eAaImIp3nS	říkat
mae-ita	maeta	k1gFnSc1	mae-ita
<g/>
.	.	kIx.	.
</s>
<s>
Obimakura	Obimakura	k1gFnSc1	Obimakura
(	(	kIx(	(
<g/>
帯	帯	k?	帯
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
otaiko	otaika	k1gFnSc5	otaika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
bubínek	bubínek	k1gInSc1	bubínek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vycpávka	vycpávka	k1gFnSc1	vycpávka
podobná	podobný	k2eAgFnSc1d1	podobná
polštářku	polštářek	k1gInSc2	polštářek
(	(	kIx(	(
<g/>
makura	makura	k1gFnSc1	makura
<g/>
=	=	kIx~	=
<g/>
polštář	polštář	k1gInSc1	polštář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
vycpává	vycpávat	k5eAaImIp3nS	vycpávat
ozdobný	ozdobný	k2eAgInSc4d1	ozdobný
uzel	uzel	k1gInSc4	uzel
u	u	k7c2	u
formálního	formální	k2eAgMnSc2d1	formální
obi	obi	k?	obi
<g/>
.	.	kIx.	.
</s>
<s>
Datedžime	Datedžimat	k5eAaPmIp3nS	Datedžimat
(	(	kIx(	(
<g/>
伊	伊	k?	伊
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tenká	tenký	k2eAgFnSc1d1	tenká
pevná	pevný	k2eAgFnSc1d1	pevná
šerpa	šerpa	k1gFnSc1	šerpa
vázaná	vázaný	k2eAgFnSc1d1	vázaná
v	v	k7c6	v
bocích	bok	k1gInPc6	bok
pod	pod	k7c7	pod
obi	obi	k?	obi
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kimono	kimono	k1gNnSc1	kimono
drželo	držet	k5eAaImAgNnS	držet
pevně	pevně	k6eAd1	pevně
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Koši	koš	k1gInPc7	koš
himo	himo	k1gNnSc1	himo
(	(	kIx(	(
<g/>
腰	腰	k?	腰
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
tenké	tenký	k2eAgFnPc1d1	tenká
stuhy	stuha	k1gFnPc1	stuha
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
bocích	bok	k1gInPc6	bok
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
kimono	kimono	k1gNnSc1	kimono
při	při	k7c6	při
oblékání	oblékání	k1gNnSc6	oblékání
<g/>
.	.	kIx.	.
</s>
<s>
Tabi	Tabi	k1gNnSc1	Tabi
(	(	kIx(	(
<g/>
足	足	k?	足
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
kotníky	kotník	k1gInPc4	kotník
vysoké	vysoká	k1gFnSc2	vysoká
"	"	kIx"	"
<g/>
ponožky	ponožka	k1gFnPc1	ponožka
<g/>
"	"	kIx"	"
s	s	k7c7	s
odděleným	oddělený	k2eAgInSc7d1	oddělený
palcem	palec	k1gInSc7	palec
nošené	nošený	k2eAgNnSc4d1	nošené
společně	společně	k6eAd1	společně
se	s	k7c7	s
zóri	zóri	k1gNnSc7	zóri
či	či	k8xC	či
jinou	jiný	k2eAgFnSc7d1	jiná
tradiční	tradiční	k2eAgFnSc7d1	tradiční
obuví	obuv	k1gFnSc7	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
i	i	k9	i
jako	jako	k9	jako
"	"	kIx"	"
<g/>
podkolenky	podkolenka	k1gFnSc2	podkolenka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Waradži	Waradzat	k5eAaPmIp1nS	Waradzat
(	(	kIx(	(
<g/>
草	草	k?	草
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
slaměné	slaměný	k2eAgInPc4d1	slaměný
sandály	sandál	k1gInPc4	sandál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dnes	dnes	k6eAd1	dnes
většinou	většinou	k6eAd1	většinou
nosí	nosit	k5eAaImIp3nP	nosit
jenom	jenom	k9	jenom
mniši	mnich	k1gMnPc1	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Jukata	Jukata	k1gFnSc1	Jukata
(	(	kIx(	(
<g/>
浴	浴	k?	浴
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
neformální	formální	k2eNgNnSc4d1	neformální
letní	letní	k2eAgNnSc4d1	letní
kimono	kimono	k1gNnSc4	kimono
bez	bez	k7c2	bez
podšívky	podšívka	k1gFnSc2	podšívka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
vyrobené	vyrobený	k2eAgNnSc1d1	vyrobené
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
lnu	len	k1gInSc2	len
nebo	nebo	k8xC	nebo
konopné	konopný	k2eAgFnSc2d1	konopná
příze	příz	k1gFnSc2	příz
<g/>
.	.	kIx.	.
</s>
<s>
Jukata	Jukata	k1gFnSc1	Jukata
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
nosí	nosit	k5eAaImIp3nS	nosit
na	na	k7c4	na
venkovní	venkovní	k2eAgInPc4d1	venkovní
festivaly	festival	k1gInPc4	festival
a	a	k8xC	a
nosí	nosit	k5eAaImIp3nP	nosit
je	on	k3xPp3gNnSc4	on
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
všech	všecek	k3xTgFnPc2	všecek
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
onsenu	onsen	k1gInSc6	onsen
-	-	kIx~	-
termálních	termální	k2eAgFnPc6d1	termální
lázních	lázeň	k1gFnPc6	lázeň
-	-	kIx~	-
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
každé	každý	k3xTgFnPc1	každý
lázně	lázeň	k1gFnPc1	lázeň
mají	mít	k5eAaImIp3nP	mít
jukatu	jukata	k1gFnSc4	jukata
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
vzorem	vzor	k1gInSc7	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Zóri	Zóri	k1gNnSc1	Zóri
(	(	kIx(	(
<g/>
草	草	k?	草
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
látkové	látkový	k2eAgFnPc1d1	látková
<g/>
,	,	kIx,	,
kožené	kožený	k2eAgFnPc1d1	kožená
<g/>
,	,	kIx,	,
slaměné	slaměný	k2eAgFnPc1d1	slaměná
nebo	nebo	k8xC	nebo
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
plastové	plastový	k2eAgInPc4d1	plastový
sandály	sandál	k1gInPc4	sandál
<g/>
.	.	kIx.	.
</s>
<s>
Zóri	Zóri	k6eAd1	Zóri
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
zdobné	zdobný	k2eAgNnSc1d1	zdobné
<g/>
,	,	kIx,	,
se	s	k7c7	s
složitým	složitý	k2eAgNnSc7d1	složité
vyšíváním	vyšívání	k1gNnSc7	vyšívání
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
úplně	úplně	k6eAd1	úplně
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
je	on	k3xPp3gFnPc4	on
ženy	žena	k1gFnPc4	žena
i	i	k8xC	i
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Zóri	Zóri	k1gNnSc4	Zóri
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
ze	z	k7c2	z
slámy	sláma	k1gFnSc2	sláma
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
pásky	pásek	k1gInPc7	pásek
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
nejformálnější	formální	k2eAgFnSc1d3	nejformálnější
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
podobný	podobný	k2eAgInSc4d1	podobný
design	design	k1gInSc4	design
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
žabky	žabka	k1gFnSc2	žabka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
celé	celý	k2eAgNnSc4d1	celé
kimono	kimono	k1gNnSc4	kimono
při	při	k7c6	při
čištění	čištění	k1gNnSc6	čištění
úplně	úplně	k6eAd1	úplně
rozpáráno	rozpárat	k5eAaPmNgNnS	rozpárat
a	a	k8xC	a
potom	potom	k6eAd1	potom
znovu	znovu	k6eAd1	znovu
ručně	ručně	k6eAd1	ručně
sešito	sešit	k2eAgNnSc1d1	sešito
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tradiční	tradiční	k2eAgFnSc1d1	tradiční
metoda	metoda	k1gFnSc1	metoda
čištění	čištění	k1gNnSc2	čištění
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
arai	arai	k6eAd1	arai
hari	hari	k6eAd1	hari
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
při	při	k7c6	při
čištění	čištění	k1gNnSc6	čištění
kimona	kimono	k1gNnSc2	kimono
musely	muset	k5eAaImAgInP	muset
stehy	steh	k1gInPc1	steh
vyndavat	vyndavat	k5eAaImF	vyndavat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byla	být	k5eAaImAgNnP	být
kimona	kimono	k1gNnPc1	kimono
ručně	ručně	k6eAd1	ručně
šitá	šitý	k2eAgNnPc1d1	šité
<g/>
.	.	kIx.	.
</s>
<s>
Arai	Arai	k6eAd1	Arai
hari	hari	k6eAd1	hari
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
drahý	drahý	k2eAgInSc1d1	drahý
způsob	způsob	k1gInSc1	způsob
čištění	čištění	k1gNnSc2	čištění
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
klesající	klesající	k2eAgFnSc2d1	klesající
obliby	obliba	k1gFnSc2	obliba
kimon	kimono	k1gNnPc2	kimono
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
už	už	k6eAd1	už
existují	existovat	k5eAaImIp3nP	existovat
moderní	moderní	k2eAgInPc1d1	moderní
materiály	materiál	k1gInPc1	materiál
a	a	k8xC	a
nové	nový	k2eAgInPc1d1	nový
způsoby	způsob	k1gInPc1	způsob
čištění	čištění	k1gNnSc2	čištění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
čištění	čištění	k1gNnSc4	čištění
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
tradiční	tradiční	k2eAgNnSc1d1	tradiční
čištění	čištění	k1gNnSc1	čištění
stále	stále	k6eAd1	stále
používá	používat	k5eAaImIp3nS	používat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
obzvlášť	obzvlášť	k6eAd1	obzvlášť
cenných	cenný	k2eAgNnPc2d1	cenné
kimon	kimono	k1gNnPc2	kimono
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
kimono	kimono	k1gNnSc1	kimono
ušité	ušitý	k2eAgFnSc2d1	ušitá
zákazníkovi	zákazník	k1gMnSc3	zákazník
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
mu	on	k3xPp3gInSc3	on
bude	být	k5eAaImBp3nS	být
doručeno	doručit	k5eAaPmNgNnS	doručit
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
stahovacími	stahovací	k2eAgInPc7d1	stahovací
švy	šev	k1gInPc7	šev
kolem	kolem	k7c2	kolem
vnějších	vnější	k2eAgInPc2d1	vnější
lemů	lem	k1gInPc2	lem
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
švy	šev	k1gInPc1	šev
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
šicuke	šicuk	k1gInPc1	šicuk
ito	ito	k?	ito
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
šicuke	šicuke	k6eAd1	šicuke
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
před	před	k7c7	před
uložením	uložení	k1gNnSc7	uložení
přešívají	přešívat	k5eAaImIp3nP	přešívat
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
předcházet	předcházet	k5eAaImF	předcházet
mačkání	mačkání	k1gNnSc4	mačkání
a	a	k8xC	a
udržují	udržovat	k5eAaImIp3nP	udržovat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vrstvy	vrstva	k1gFnPc1	vrstva
kimona	kimono	k1gNnSc2	kimono
přesně	přesně	k6eAd1	přesně
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
tradičních	tradiční	k2eAgInPc2d1	tradiční
japonských	japonský	k2eAgInPc2d1	japonský
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
i	i	k9	i
u	u	k7c2	u
kimona	kimono	k1gNnSc2	kimono
jsou	být	k5eAaImIp3nP	být
specifické	specifický	k2eAgInPc1d1	specifický
způsoby	způsob	k1gInPc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
ho	on	k3xPp3gMnSc4	on
skládat	skládat	k5eAaImF	skládat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
skládání	skládání	k1gNnSc2	skládání
chrání	chránit	k5eAaImIp3nP	chránit
kimono	kimono	k1gNnSc4	kimono
při	při	k7c6	při
skladování	skladování	k1gNnSc6	skladování
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
jeho	jeho	k3xOp3gNnSc4	jeho
mačkání	mačkání	k1gNnSc4	mačkání
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
kimona	kimono	k1gNnPc1	kimono
skladují	skladovat	k5eAaImIp3nP	skladovat
zabalená	zabalený	k2eAgNnPc1d1	zabalené
v	v	k7c6	v
papíru	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
<s>
Kimono	kimono	k1gNnSc1	kimono
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
občas	občas	k6eAd1	občas
vyvětrat	vyvětrat	k5eAaPmF	vyvětrat
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
sezonně	sezonně	k6eAd1	sezonně
a	a	k8xC	a
nebo	nebo	k8xC	nebo
před	před	k7c7	před
a	a	k8xC	a
po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
nošení	nošení	k1gNnSc6	nošení
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
dnes	dnes	k6eAd1	dnes
svá	svůj	k3xOyFgNnPc4	svůj
kimona	kimono	k1gNnPc4	kimono
nechává	nechávat	k5eAaImIp3nS	nechávat
čistit	čistit	k5eAaImF	čistit
chemicky	chemicky	k6eAd1	chemicky
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
někdy	někdy	k6eAd1	někdy
velmi	velmi	k6eAd1	velmi
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
levnější	levný	k2eAgMnSc1d2	levnější
než	než	k8xS	než
arai	arai	k6eAd1	arai
hari	hari	k6eAd1	hari
a	a	k8xC	a
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
typy	typ	k1gInPc4	typ
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
barvení	barvení	k1gNnSc2	barvení
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
čištění	čištění	k1gNnSc4	čištění
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Obi	Obi	k?	Obi
(	(	kIx(	(
<g/>
pás	pás	k1gInSc1	pás
<g/>
)	)	kIx)	)
</s>
