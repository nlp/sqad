<p>
<s>
Anne	Anne	k1gFnSc1	Anne
Franková	Franková	k1gFnSc1	Franková
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Annelies	Anneliesa	k1gFnPc2	Anneliesa
Marie	Maria	k1gFnSc2	Maria
"	"	kIx"	"
<g/>
Anne	Anne	k1gFnSc1	Anne
<g/>
"	"	kIx"	"
Frank	Frank	k1gMnSc1	Frank
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1929	[number]	k4	1929
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
–	–	k?	–
únor	únor	k1gInSc4	únor
1945	[number]	k4	1945
koncentrační	koncentrační	k2eAgInSc4d1	koncentrační
tábor	tábor	k1gInSc4	tábor
Bergen-Belsen	Bergen-Belsen	k2eAgInSc4d1	Bergen-Belsen
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
narozená	narozený	k2eAgFnSc1d1	narozená
autorka	autorka	k1gFnSc1	autorka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
dívka	dívka	k1gFnSc1	dívka
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
ukrývající	ukrývající	k2eAgFnSc2d1	ukrývající
se	se	k3xPyFc4	se
před	před	k7c7	před
nacisty	nacista	k1gMnPc7	nacista
v	v	k7c6	v
zadním	zadní	k2eAgInSc6d1	zadní
traktu	trakt	k1gInSc6	trakt
jednoho	jeden	k4xCgInSc2	jeden
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
si	se	k3xPyFc3	se
psala	psát	k5eAaImAgFnS	psát
deník	deník	k1gInSc4	deník
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rodina	rodina	k1gFnSc1	rodina
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
do	do	k7c2	do
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
před	před	k7c7	před
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Nacismus	nacismus	k1gInSc1	nacismus
získával	získávat	k5eAaImAgInS	získávat
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
okupováno	okupován	k2eAgNnSc1d1	okupováno
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
pronásledování	pronásledování	k1gNnSc1	pronásledování
a	a	k8xC	a
persekuce	persekuce	k1gFnSc1	persekuce
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1942	[number]	k4	1942
ukryla	ukrýt	k5eAaPmAgFnS	ukrýt
v	v	k7c6	v
tajných	tajný	k2eAgFnPc6d1	tajná
místnostech	místnost	k1gFnPc6	místnost
kancelářské	kancelářský	k2eAgFnSc2d1	kancelářská
budovy	budova	k1gFnSc2	budova
Otto	Otto	k1gMnSc1	Otto
Franka	Frank	k1gMnSc4	Frank
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
skrývání	skrývání	k1gNnSc2	skrývání
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
prozrazena	prozradit	k5eAaPmNgFnS	prozradit
a	a	k8xC	a
deportována	deportovat	k5eAaBmNgFnS	deportovat
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
tyfus	tyfus	k1gInSc4	tyfus
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Bergen-Belsen	Bergen-Belsen	k1gInSc1	Bergen-Belsen
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Otto	Otto	k1gMnSc1	Otto
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
a	a	k8xC	a
našel	najít	k5eAaPmAgMnS	najít
její	její	k3xOp3gInSc4	její
deník	deník	k1gInSc4	deník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
unikátní	unikátní	k2eAgInSc1d1	unikátní
záznam	záznam	k1gInSc1	záznam
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vydat	vydat	k5eAaPmF	vydat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
kniha	kniha	k1gFnSc1	kniha
získala	získat	k5eAaPmAgFnS	získat
jméno	jméno	k1gNnSc4	jméno
Deník	deník	k1gInSc1	deník
Anne	Anne	k1gNnSc6	Anne
Frankové	Franková	k1gFnSc2	Franková
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc4	některý
vydání	vydání	k1gNnPc4	vydání
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Deník	deník	k1gInSc1	deník
Anny	Anna	k1gFnSc2	Anna
Frankové	Franková	k1gFnSc2	Franková
nebo	nebo	k8xC	nebo
Deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
<g/>
Deník	deník	k1gInSc1	deník
Anna	Anna	k1gFnSc1	Anna
Franková	Franková	k1gFnSc1	Franková
dostala	dostat	k5eAaPmAgFnS	dostat
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
třináctým	třináctý	k4xOgFnPc3	třináctý
narozeninám	narozeniny	k1gFnPc3	narozeniny
a	a	k8xC	a
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
události	událost	k1gFnPc4	událost
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
až	až	k9	až
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
záznamu	záznam	k1gInSc2	záznam
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
nizozemštiny	nizozemština	k1gFnSc2	nizozemština
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejčtenějších	čtený	k2eAgFnPc2d3	Nejčtenější
knih	kniha	k1gFnPc2	kniha
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Období	období	k1gNnSc1	období
zachycené	zachycený	k2eAgNnSc1d1	zachycené
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
ukrytím	ukrytí	k1gNnSc7	ukrytí
===	===	k?	===
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
třináctým	třináctý	k4xOgFnPc3	třináctý
narozeninám	narozeniny	k1gFnPc3	narozeniny
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
dostala	dostat	k5eAaPmAgFnS	dostat
Anne	Anne	k1gInSc4	Anne
malý	malý	k2eAgInSc4d1	malý
zápisník	zápisník	k1gInSc4	zápisník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
ukázala	ukázat	k5eAaPmAgFnS	ukázat
otci	otec	k1gMnSc6	otec
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
ve	v	k7c6	v
výloze	výloha	k1gFnSc6	výloha
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
na	na	k7c4	na
podpisy	podpis	k1gInPc4	podpis
a	a	k8xC	a
autogramy	autogram	k1gInPc4	autogram
s	s	k7c7	s
červeno-bílou	červenoílý	k2eAgFnSc7d1	červeno-bílá
čtverečkovanou	čtverečkovaný	k2eAgFnSc7d1	čtverečkovaná
vazbou	vazba	k1gFnSc7	vazba
a	a	k8xC	a
malým	malý	k2eAgInSc7d1	malý
zámečkem	zámeček	k1gInSc7	zámeček
na	na	k7c6	na
obálce	obálka	k1gFnSc6	obálka
<g/>
,	,	kIx,	,
Anne	Anne	k1gFnSc1	Anne
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
použije	použít	k5eAaPmIp3nS	použít
jako	jako	k9	jako
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
popisovat	popisovat	k5eAaImF	popisovat
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
by	by	kYmCp3nP	by
ráda	rád	k2eAgFnSc1d1	ráda
navštívila	navštívit	k5eAaPmAgFnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rané	raný	k2eAgInPc1d1	raný
zápisy	zápis	k1gInPc1	zápis
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
život	život	k1gInSc1	život
podobný	podobný	k2eAgInSc1d1	podobný
životu	život	k1gInSc2	život
každé	každý	k3xTgFnSc2	každý
dívky	dívka	k1gFnSc2	dívka
jejího	její	k3xOp3gInSc2	její
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
také	také	k9	také
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přišly	přijít	k5eAaPmAgFnP	přijít
po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
okupaci	okupace	k1gFnSc6	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
narážky	narážka	k1gFnPc1	narážka
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
náhodné	náhodný	k2eAgFnPc1d1	náhodná
a	a	k8xC	a
nedůrazné	důrazný	k2eNgFnPc1d1	nedůrazná
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Anne	Anne	k1gFnSc1	Anne
někdy	někdy	k6eAd1	někdy
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
detailech	detail	k1gInPc6	detail
útisku	útisk	k1gInSc2	útisk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
neustále	neustále	k6eAd1	neustále
vzrůstal	vzrůstat	k5eAaImAgInS	vzrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
psala	psát	k5eAaImAgFnS	psát
o	o	k7c6	o
žluté	žlutý	k2eAgFnSc6d1	žlutá
hvězdě	hvězda	k1gFnSc6	hvězda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
Židé	Žid	k1gMnPc1	Žid
nuceni	nutit	k5eAaImNgMnP	nutit
nosit	nosit	k5eAaImF	nosit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
přehledu	přehled	k1gInSc2	přehled
omezení	omezení	k1gNnSc2	omezení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zasahovala	zasahovat	k5eAaImAgFnS	zasahovat
do	do	k7c2	do
života	život	k1gInSc2	život
amsterodamské	amsterodamský	k2eAgFnSc2d1	Amsterodamská
židovské	židovský	k2eAgFnSc2d1	židovská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1942	[number]	k4	1942
dostala	dostat	k5eAaPmAgFnS	dostat
Edith	Edith	k1gInSc4	Edith
Franková	Franková	k1gFnSc1	Franková
předvolání	předvolání	k1gNnSc2	předvolání
od	od	k7c2	od
Ústředny	ústředna	k1gFnSc2	ústředna
pro	pro	k7c4	pro
židovské	židovský	k2eAgNnSc4d1	Židovské
vystěhovalectví	vystěhovalectví	k1gNnSc4	vystěhovalectví
(	(	kIx(	(
<g/>
Zentralstelle	Zentralstelle	k1gInSc1	Zentralstelle
für	für	k?	für
jüdische	jüdische	k1gInSc1	jüdische
Auswanderung	Auswanderung	k1gInSc1	Auswanderung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předvolání	předvolání	k1gNnSc1	předvolání
přikazovalo	přikazovat	k5eAaImAgNnS	přikazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
k	k	k7c3	k
přesídlení	přesídlení	k1gNnSc3	přesídlení
do	do	k7c2	do
pracovního	pracovní	k2eAgInSc2d1	pracovní
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Anně	Anna	k1gFnSc3	Anna
pak	pak	k6eAd1	pak
otec	otec	k1gMnSc1	otec
sdělil	sdělit	k5eAaPmAgMnS	sdělit
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Otto	Otto	k1gMnSc1	Otto
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
důvěryhodnými	důvěryhodný	k2eAgMnPc7d1	důvěryhodný
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
kterém	který	k3yQgNnSc6	který
již	již	k6eAd1	již
Edith	Edith	k1gInSc1	Edith
a	a	k8xC	a
Margot	Margot	k1gInSc1	Margot
krátce	krátce	k6eAd1	krátce
věděly	vědět	k5eAaImAgFnP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
schovat	schovat	k5eAaPmF	schovat
do	do	k7c2	do
místností	místnost	k1gFnPc2	místnost
nad	nad	k7c4	nad
a	a	k8xC	a
za	za	k7c4	za
prostory	prostora	k1gFnPc4	prostora
firmy	firma	k1gFnSc2	firma
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Prinsengracht	Prinsengrachta	k1gFnPc2	Prinsengrachta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Život	život	k1gInSc1	život
v	v	k7c6	v
zadním	zadní	k2eAgInSc6d1	zadní
traktu	trakt	k1gInSc6	trakt
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
ráno	ráno	k6eAd1	ráno
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1942	[number]	k4	1942
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
úkrytu	úkryt	k1gInSc2	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
zanechali	zanechat	k5eAaPmAgMnP	zanechat
nepořádek	nepořádek	k1gInSc4	nepořádek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyvolali	vyvolat	k5eAaPmAgMnP	vyvolat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
náhle	náhle	k6eAd1	náhle
<g/>
,	,	kIx,	,
a	a	k8xC	a
Otto	Otto	k1gMnSc1	Otto
Frank	Frank	k1gMnSc1	Frank
zanechal	zanechat	k5eAaPmAgMnS	zanechat
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
naznačoval	naznačovat	k5eAaImAgMnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odjeli	odjet	k5eAaPmAgMnP	odjet
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
Židé	Žid	k1gMnPc1	Žid
nesměli	smět	k5eNaImAgMnP	smět
použít	použít	k5eAaPmF	použít
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
šli	jít	k5eAaImAgMnP	jít
pěšky	pěšky	k6eAd1	pěšky
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
nového	nový	k2eAgInSc2d1	nový
domova	domov	k1gInSc2	domov
s	s	k7c7	s
několika	několik	k4yIc7	několik
vrstvami	vrstva	k1gFnPc7	vrstva
oblečení	oblečení	k1gNnPc2	oblečení
na	na	k7c4	na
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
neodvážili	odvážit	k5eNaPmAgMnP	odvážit
vzít	vzít	k5eAaPmF	vzít
si	se	k3xPyFc3	se
zavazadla	zavazadlo	k1gNnSc2	zavazadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgInSc1d1	zadní
trakt	trakt	k1gInSc1	trakt
byl	být	k5eAaImAgInS	být
prostor	prostor	k1gInSc4	prostor
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
přístupný	přístupný	k2eAgInSc4d1	přístupný
z	z	k7c2	z
chodby	chodba	k1gFnSc2	chodba
za	za	k7c7	za
kancelářemi	kancelář	k1gFnPc7	kancelář
firmy	firma	k1gFnSc2	firma
Opekta	Opekto	k1gNnSc2	Opekto
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
malé	malý	k2eAgFnPc1d1	malá
místnosti	místnost	k1gFnPc1	místnost
s	s	k7c7	s
přilehlou	přilehlý	k2eAgFnSc7d1	přilehlá
koupelnou	koupelna	k1gFnSc7	koupelna
a	a	k8xC	a
záchodem	záchod	k1gInSc7	záchod
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
prostorná	prostorný	k2eAgFnSc1d1	prostorná
místnost	místnost	k1gFnSc1	místnost
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
malé	malý	k2eAgFnPc4d1	malá
místnosti	místnost	k1gFnPc4	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
menší	malý	k2eAgFnSc2d2	menší
místnosti	místnost	k1gFnSc2	místnost
vedl	vést	k5eAaImAgInS	vést
žebřík	žebřík	k1gInSc1	žebřík
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
zadního	zadní	k2eAgInSc2d1	zadní
traktu	trakt	k1gInSc2	trakt
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
zakryt	zakrýt	k5eAaPmNgInS	zakrýt
regálem	regál	k1gInSc7	regál
s	s	k7c7	s
šanony	šanon	k1gInPc7	šanon
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
kostela	kostel	k1gInSc2	kostel
Westerkerk	Westerkerk	k1gInSc4	Westerkerk
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nevýznamná	významný	k2eNgFnSc1d1	nevýznamná
stará	starý	k2eAgFnSc1d1	stará
budova	budova	k1gFnSc1	budova
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
západní	západní	k2eAgFnSc4d1	západní
čtvrtě	čtvrt	k1gFnPc4	čtvrt
Amsterodamu	Amsterodam	k1gInSc2	Amsterodam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Victor	Victor	k1gMnSc1	Victor
Kugler	Kugler	k1gMnSc1	Kugler
(	(	kIx(	(
<g/>
český	český	k2eAgMnSc1d1	český
Němec	Němec	k1gMnSc1	Němec
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Vrchlabí	Vrchlabí	k1gNnSc2	Vrchlabí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
Kleiman	Kleiman	k1gMnSc1	Kleiman
<g/>
,	,	kIx,	,
Miep	Miep	k1gMnSc1	Miep
Giesová	Giesová	k1gFnSc1	Giesová
a	a	k8xC	a
Bep	Bep	k1gFnSc1	Bep
Voskuijlová	Voskuijlová	k1gFnSc1	Voskuijlová
byli	být	k5eAaImAgMnP	být
jedinými	jediný	k2eAgMnPc7d1	jediný
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
o	o	k7c6	o
ukrytých	ukrytý	k2eAgInPc6d1	ukrytý
lidech	lid	k1gInPc6	lid
věděli	vědět	k5eAaImAgMnP	vědět
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
Giesové	Giesová	k1gFnSc2	Giesová
Janem	Jan	k1gMnSc7	Jan
Giesem	Gies	k1gMnSc7	Gies
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
Voskuijlové	Voskuijlová	k1gFnSc2	Voskuijlová
Johannesem	Johannes	k1gMnSc7	Johannes
Hendrikem	Hendrik	k1gMnSc7	Hendrik
Voskuijlem	Voskuijl	k1gMnSc7	Voskuijl
jim	on	k3xPp3gMnPc3	on
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
jim	on	k3xPp3gMnPc3	on
jediný	jediný	k2eAgInSc4d1	jediný
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
vnějším	vnější	k2eAgInSc7d1	vnější
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
lidmi	člověk	k1gMnPc7	člověk
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
přinášeli	přinášet	k5eAaImAgMnP	přinášet
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
války	válka	k1gFnSc2	válka
a	a	k8xC	a
politické	politický	k2eAgFnSc3d1	politická
situaci	situace	k1gFnSc3	situace
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťovali	zajišťovat	k5eAaImAgMnP	zajišťovat
všechny	všechen	k3xTgFnPc4	všechen
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
,	,	kIx,	,
starali	starat	k5eAaImAgMnP	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
zásobovali	zásobovat	k5eAaImAgMnP	zásobovat
je	on	k3xPp3gNnSc4	on
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
těžší	těžký	k2eAgInSc1d2	těžší
úkol	úkol	k1gInSc1	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Anne	Anne	k1gFnSc1	Anne
psala	psát	k5eAaImAgFnS	psát
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
a	a	k8xC	a
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
nálady	nálada	k1gFnSc2	nálada
uprchlíků	uprchlík	k1gMnPc2	uprchlík
i	i	k8xC	i
během	během	k7c2	během
nejhorších	zlý	k2eAgInPc2d3	Nejhorší
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
vědomi	vědom	k2eAgMnPc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	on	k3xPp3gInPc4	on
chytí	chytit	k5eAaPmIp3nP	chytit
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
jim	on	k3xPp3gMnPc3	on
za	za	k7c4	za
skrývání	skrývání	k1gNnPc4	skrývání
Židů	Žid	k1gMnPc2	Žid
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
k	k	k7c3	k
Frankům	Franky	k1gInPc3	Franky
připojila	připojit	k5eAaPmAgFnS	připojit
rodina	rodina	k1gFnSc1	rodina
van	van	k1gInSc1	van
Pelse	Pelse	k1gFnSc1	Pelse
<g/>
:	:	kIx,	:
Hermann	Hermann	k1gMnSc1	Hermann
<g/>
,	,	kIx,	,
Auguste	August	k1gMnSc5	August
a	a	k8xC	a
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
Peter	Peter	k1gMnSc1	Peter
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
přibyl	přibýt	k5eAaPmAgMnS	přibýt
ještě	ještě	k9	ještě
Fritz	Fritz	k1gMnSc1	Fritz
Pfeffer	Pfeffer	k1gMnSc1	Pfeffer
<g/>
,	,	kIx,	,
zubař	zubař	k1gMnSc1	zubař
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Anne	Anne	k1gFnSc1	Anne
měla	mít	k5eAaImAgFnS	mít
radost	radost	k1gFnSc4	radost
z	z	k7c2	z
nových	nový	k2eAgMnPc2d1	nový
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgMnPc7	který
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
povídat	povídat	k5eAaImF	povídat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
napětí	napětí	k1gNnSc1	napětí
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
uvnitř	uvnitř	k7c2	uvnitř
skupiny	skupina	k1gFnSc2	skupina
nucené	nucený	k2eAgNnSc1d1	nucené
bydlet	bydlet	k5eAaImF	bydlet
v	v	k7c6	v
takových	takový	k3xDgFnPc6	takový
omezených	omezený	k2eAgFnPc6d1	omezená
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sdílela	sdílet	k5eAaImAgFnS	sdílet
pokoj	pokoj	k1gInSc4	pokoj
s	s	k7c7	s
Pfefferem	Pfeffero	k1gNnSc7	Pfeffero
<g/>
,	,	kIx,	,
shledala	shledat	k5eAaPmAgFnS	shledat
ho	on	k3xPp3gMnSc4	on
nesnesitelným	snesitelný	k2eNgMnSc7d1	nesnesitelný
<g/>
,	,	kIx,	,
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
konflikty	konflikt	k1gInPc4	konflikt
s	s	k7c7	s
Augustou	Augusta	k1gMnSc7	Augusta
van	van	k1gInSc4	van
Pelsovou	Pelsová	k1gFnSc7	Pelsová
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
pošetilou	pošetilý	k2eAgFnSc4d1	pošetilá
<g/>
.	.	kIx.	.
</s>
<s>
Annin	Annin	k2eAgInSc1d1	Annin
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
byl	být	k5eAaImAgInS	být
napjatý	napjatý	k2eAgInSc1d1	napjatý
a	a	k8xC	a
Anne	Anne	k1gFnSc1	Anne
psala	psát	k5eAaImAgFnS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
málo	málo	k6eAd1	málo
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
hádala	hádat	k5eAaImAgFnS	hádat
s	s	k7c7	s
Margot	Margot	k1gInSc1	Margot
<g/>
,	,	kIx,	,
psala	psát	k5eAaImAgFnS	psát
o	o	k7c6	o
neočekávaném	očekávaný	k2eNgNnSc6d1	neočekávané
poutu	pouto	k1gNnSc6	pouto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Citově	citově	k6eAd1	citově
nejblíže	blízce	k6eAd3	blízce
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
plachého	plachý	k2eAgMnSc2d1	plachý
a	a	k8xC	a
nemotorného	motorný	k2eNgMnSc2d1	nemotorný
Petera	Peter	k1gMnSc2	Peter
van	vana	k1gFnPc2	vana
Pelse	Pelse	k1gFnSc2	Pelse
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
našla	najít	k5eAaPmAgFnS	najít
spřízněnou	spřízněný	k2eAgFnSc4d1	spřízněná
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anne	Anne	k1gFnSc1	Anne
trávila	trávit	k5eAaImAgFnS	trávit
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
čtením	čtení	k1gNnSc7	čtení
a	a	k8xC	a
vzděláváním	vzdělávání	k1gNnSc7	vzdělávání
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
stále	stále	k6eAd1	stále
psala	psát	k5eAaImAgFnS	psát
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
deníku	deník	k1gInSc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Nejenže	nejenže	k6eAd1	nejenže
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
píše	psát	k5eAaImIp3nS	psát
také	také	k9	také
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
pocitech	pocit	k1gInPc6	pocit
<g/>
,	,	kIx,	,
plánech	plán	k1gInPc6	plán
<g/>
,	,	kIx,	,
o	o	k7c6	o
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgNnPc6	který
s	s	k7c7	s
nikým	nikdo	k3yNnSc7	nikdo
nemohla	moct	k5eNaImAgFnS	moct
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Psala	psát	k5eAaImAgFnS	psát
pravidelně	pravidelně	k6eAd1	pravidelně
až	až	k9	až
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
zápisu	zápis	k1gInSc2	zápis
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zatčení	zatčení	k1gNnSc1	zatčení
a	a	k8xC	a
koncentrační	koncentrační	k2eAgInSc1d1	koncentrační
tábor	tábor	k1gInSc1	tábor
==	==	k?	==
</s>
</p>
<p>
<s>
Ráno	ráno	k6eAd1	ráno
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
vnikly	vniknout	k5eAaPmAgFnP	vniknout
do	do	k7c2	do
zadního	zadní	k2eAgInSc2d1	zadní
traktu	trakt	k1gInSc2	trakt
jednotky	jednotka	k1gFnSc2	jednotka
zelené	zelený	k2eAgFnSc2d1	zelená
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
informátorem	informátor	k1gMnSc7	informátor
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
neví	vědět	k5eNaImIp3nS	vědět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgMnS	vést
je	on	k3xPp3gNnSc4	on
Oberscharführer	Oberscharführer	k1gMnSc1	Oberscharführer
Karl	Karl	k1gMnSc1	Karl
Silberbauer	Silberbauer	k1gMnSc1	Silberbauer
ze	z	k7c2	z
Sicherheitsdienst	Sicherheitsdienst	k1gMnSc1	Sicherheitsdienst
(	(	kIx(	(
<g/>
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
služba	služba	k1gFnSc1	služba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
byli	být	k5eAaImAgMnP	být
odvezeni	odvézt	k5eAaPmNgMnP	odvézt
k	k	k7c3	k
výslechu	výslech	k1gInSc3	výslech
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
Kugler	Kugler	k1gMnSc1	Kugler
a	a	k8xC	a
Johannes	Johannes	k1gMnSc1	Johannes
Kleiman	Kleiman	k1gMnSc1	Kleiman
byli	být	k5eAaImAgMnP	být
také	také	k9	také
odvezeni	odvezen	k2eAgMnPc1d1	odvezen
a	a	k8xC	a
následně	následně	k6eAd1	následně
uvězněni	uvězněn	k2eAgMnPc1d1	uvězněn
<g/>
,	,	kIx,	,
Miep	Miep	k1gMnSc1	Miep
Giesová	Giesová	k1gFnSc1	Giesová
a	a	k8xC	a
Bep	Bep	k1gFnSc1	Bep
Voskuijlová	Voskuijlová	k1gFnSc1	Voskuijlová
mohly	moct	k5eAaImAgFnP	moct
jít	jít	k5eAaImF	jít
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgFnP	vrátit
do	do	k7c2	do
zadního	zadní	k2eAgInSc2d1	zadní
traktu	trakt	k1gInSc2	trakt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
našly	najít	k5eAaPmAgFnP	najít
Annin	Annin	k2eAgInSc4d1	Annin
deník	deník	k1gInSc4	deník
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzaly	vzít	k5eAaPmAgFnP	vzít
ho	on	k3xPp3gNnSc4	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
několika	několik	k4yIc7	několik
alby	album	k1gNnPc7	album
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
Geisová	Geisová	k1gFnSc1	Geisová
vše	všechen	k3xTgNnSc4	všechen
chtěla	chtít	k5eAaImAgFnS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
skrýše	skrýš	k1gFnSc2	skrýš
byli	být	k5eAaImAgMnP	být
odvezeni	odvézt	k5eAaPmNgMnP	odvézt
do	do	k7c2	do
tranzitního	tranzitní	k2eAgInSc2d1	tranzitní
tábora	tábor	k1gInSc2	tábor
Westerbork	Westerbork	k1gInSc1	Westerbork
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
již	již	k6eAd1	již
prošlo	projít	k5eAaPmAgNnS	projít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
odvezena	odvézt	k5eAaPmNgFnS	odvézt
do	do	k7c2	do
vyhlazovacího	vyhlazovací	k2eAgInSc2d1	vyhlazovací
tábora	tábor	k1gInSc2	tábor
Osvětim	Osvětim	k1gFnSc4	Osvětim
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
dorazili	dorazit	k5eAaPmAgMnP	dorazit
po	po	k7c6	po
třídenní	třídenní	k2eAgFnSc6d1	třídenní
cestě	cesta	k1gFnSc6	cesta
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
podle	podle	k7c2	podle
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
již	již	k6eAd1	již
navzájem	navzájem	k6eAd1	navzájem
nemohli	moct	k5eNaImAgMnP	moct
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
1	[number]	k4	1
019	[number]	k4	019
deportovaných	deportovaný	k2eAgMnPc2d1	deportovaný
bylo	být	k5eAaImAgNnS	být
549	[number]	k4	549
lidí	člověk	k1gMnPc2	člověk
-	-	kIx~	-
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgFnPc2	všecek
dětí	dítě	k1gFnPc2	dítě
mladších	mladý	k2eAgFnPc2d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
-	-	kIx~	-
posláno	poslán	k2eAgNnSc1d1	Posláno
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
plynových	plynový	k2eAgFnPc2d1	plynová
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Anne	Anne	k1gFnSc1	Anne
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
věku	věk	k1gInSc2	věk
15	[number]	k4	15
let	léto	k1gNnPc2	léto
před	před	k7c7	před
třemi	tři	k4xCgInPc7	tři
měsíci	měsíc	k1gInPc7	měsíc
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
ze	z	k7c2	z
zadního	zadní	k2eAgInSc2d1	zadní
traktu	trakt	k1gInSc2	trakt
přežila	přežít	k5eAaPmAgFnS	přežít
selekci	selekce	k1gFnSc4	selekce
<g/>
.	.	kIx.	.
</s>
<s>
Domnívala	domnívat	k5eAaImAgFnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
ženami	žena	k1gFnPc7	žena
byla	být	k5eAaImAgFnS	být
Anne	Anne	k1gFnSc1	Anne
nucena	nucen	k2eAgFnSc1d1	nucena
se	se	k3xPyFc4	se
vysvléci	vysvléct	k5eAaPmF	vysvléct
a	a	k8xC	a
podrobit	podrobit	k5eAaPmF	podrobit
se	se	k3xPyFc4	se
desinfekci	desinfekce	k1gFnSc3	desinfekce
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jí	jíst	k5eAaImIp3nS	jíst
oholena	oholen	k2eAgFnSc1d1	oholena
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
na	na	k7c6	na
paži	paže	k1gFnSc6	paže
vytetováno	vytetován	k2eAgNnSc1d1	vytetováno
identifikační	identifikační	k2eAgNnSc1d1	identifikační
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
byly	být	k5eAaImAgFnP	být
využívány	využívat	k5eAaPmNgFnP	využívat
na	na	k7c4	na
otrocké	otrocký	k2eAgFnPc4d1	otrocká
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
byly	být	k5eAaImAgFnP	být
natlačeny	natlačit	k5eAaBmNgFnP	natlačit
do	do	k7c2	do
studených	studený	k2eAgFnPc2d1	studená
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
šířily	šířit	k5eAaImAgFnP	šířit
nemoci	nemoc	k1gFnPc1	nemoc
a	a	k8xC	a
Anne	Anne	k1gFnSc1	Anne
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
svrabem	svrab	k1gInSc7	svrab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byly	být	k5eAaImAgFnP	být
vybírány	vybírán	k2eAgFnPc1d1	vybírána
ženy	žena	k1gFnPc1	žena
pro	pro	k7c4	pro
přesun	přesun	k1gInSc4	přesun
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
Bergen-Belsen	Bergen-Belsna	k1gFnPc2	Bergen-Belsna
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
8	[number]	k4	8
000	[number]	k4	000
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Anne	Anne	k1gFnPc2	Anne
a	a	k8xC	a
Margot	Margota	k1gFnPc2	Margota
Frankových	Frankových	k2eAgFnSc2d1	Frankových
a	a	k8xC	a
Auguste	August	k1gMnSc5	August
van	vana	k1gFnPc2	vana
Pelsové	Pelsový	k2eAgNnSc1d1	Pelsový
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
transportováno	transportován	k2eAgNnSc1d1	transportováno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Edith	Edith	k1gInSc4	Edith
Franková	Franková	k1gFnSc1	Franková
zůstala	zůstat	k5eAaPmAgFnS	zůstat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1945	[number]	k4	1945
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
epidemie	epidemie	k1gFnSc2	epidemie
tyfu	tyf	k1gInSc2	tyf
<g/>
,	,	kIx,	,
zabila	zabít	k5eAaPmAgFnS	zabít
asi	asi	k9	asi
17	[number]	k4	17
000	[number]	k4	000
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Svědci	svědek	k1gMnPc1	svědek
později	pozdě	k6eAd2	pozdě
vypověděli	vypovědět	k5eAaPmAgMnP	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Margot	Margot	k1gInSc4	Margot
byla	být	k5eAaImAgFnS	být
tak	tak	k9	tak
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zabila	zabít	k5eAaPmAgFnS	zabít
pádem	pád	k1gInSc7	pád
z	z	k7c2	z
postele	postel	k1gFnSc2	postel
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
zemřela	zemřít	k5eAaPmAgFnS	zemřít
i	i	k9	i
Anne	Anne	k1gFnSc1	Anne
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovali	odhadovat	k5eAaImAgMnP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
osvobozením	osvobození	k1gNnSc7	osvobození
tábora	tábor	k1gInSc2	tábor
britskou	britský	k2eAgFnSc7d1	britská
armádou	armáda	k1gFnSc7	armáda
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
přesné	přesný	k2eAgNnSc1d1	přesné
datum	datum	k1gNnSc1	datum
jejího	její	k3xOp3gNnSc2	její
úmrtí	úmrtí	k1gNnSc2	úmrtí
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Anne	Anne	k1gFnSc1	Anne
zemřela	zemřít	k5eAaPmAgFnS	zemřít
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
odhadnuto	odhadnout	k5eAaPmNgNnS	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
110	[number]	k4	110
000	[number]	k4	000
židů	žid	k1gMnPc2	žid
deportovaných	deportovaný	k2eAgMnPc2d1	deportovaný
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
během	během	k7c2	během
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
jich	on	k3xPp3gFnPc2	on
přežilo	přežít	k5eAaPmAgNnS	přežít
jen	jen	k9	jen
5	[number]	k4	5
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vydání	vydání	k1gNnSc1	vydání
deníku	deník	k1gInSc2	deník
===	===	k?	===
</s>
</p>
<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Frank	Frank	k1gMnSc1	Frank
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Amsterodamu	Amsterodam	k1gInSc2	Amsterodam
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
dcery	dcera	k1gFnPc1	dcera
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Bergen-Belsenu	Bergen-Belsen	k2eAgFnSc4d1	Bergen-Belsen
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
stále	stále	k6eAd1	stále
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přežily	přežít	k5eAaPmAgFnP	přežít
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1945	[number]	k4	1945
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
zemřely	zemřít	k5eAaPmAgFnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
mu	on	k3xPp3gNnSc3	on
Miep	Miep	k1gInSc4	Miep
Giesová	Giesová	k1gFnSc1	Giesová
dala	dát	k5eAaPmAgFnS	dát
Annin	Annin	k2eAgInSc4d1	Annin
deník	deník	k1gInSc4	deník
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
jej	on	k3xPp3gNnSc4	on
nečetla	číst	k5eNaImAgFnS	číst
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
ho	on	k3xPp3gMnSc4	on
přečetl	přečíst	k5eAaPmAgMnS	přečíst
a	a	k8xC	a
shledal	shledat	k5eAaPmAgMnS	shledat
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
přesný	přesný	k2eAgInSc4d1	přesný
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
napsaný	napsaný	k2eAgInSc4d1	napsaný
záznam	záznam	k1gInSc4	záznam
části	část	k1gFnSc2	část
jejich	jejich	k3xOp3gInSc2	jejich
společného	společný	k2eAgInSc2d1	společný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Anne	Anne	k1gFnSc1	Anne
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
zmiňovala	zmiňovat	k5eAaImAgFnS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
chtěla	chtít	k5eAaImAgFnS	chtít
být	být	k5eAaImF	být
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
možné	možný	k2eAgNnSc4d1	možné
vydání	vydání	k1gNnSc4	vydání
jejího	její	k3xOp3gInSc2	její
deníku	deník	k1gInSc2	deník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anne	Anne	k1gFnSc1	Anne
začala	začít	k5eAaPmAgFnS	začít
svůj	svůj	k3xOyFgInSc4	svůj
deník	deník	k1gInSc4	deník
jako	jako	k8xC	jako
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
soukromé	soukromý	k2eAgNnSc4d1	soukromé
vyjádření	vyjádření	k1gNnSc4	vyjádření
svých	svůj	k3xOyFgFnPc2	svůj
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
napsala	napsat	k5eAaBmAgFnS	napsat
několikrát	několikrát	k6eAd1	několikrát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
nikomu	nikdo	k3yNnSc3	nikdo
nedovolila	dovolit	k5eNaPmAgFnS	dovolit
jej	on	k3xPp3gInSc4	on
číst	číst	k5eAaImF	číst
<g/>
.	.	kIx.	.
</s>
<s>
Otevřeně	otevřeně	k6eAd1	otevřeně
popisovala	popisovat	k5eAaImAgFnS	popisovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
společníky	společník	k1gMnPc4	společník
a	a	k8xC	a
situace	situace	k1gFnPc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
si	se	k3xPyFc3	se
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
psát	psát	k5eAaImF	psát
beletrii	beletrie	k1gFnSc4	beletrie
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1944	[number]	k4	1944
slyšela	slyšet	k5eAaImAgFnS	slyšet
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
Gerrita	Gerrita	k1gFnSc1	Gerrita
Bolkesteina	Bolkesteino	k1gNnSc2	Bolkesteino
-	-	kIx~	-
člena	člen	k1gMnSc2	člen
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
–	–	k?	–
ten	ten	k3xDgInSc1	ten
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
by	by	kYmCp3nS	by
uchovat	uchovat	k5eAaPmF	uchovat
k	k	k7c3	k
připomenutí	připomenutí	k1gNnSc3	připomenutí
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
za	za	k7c2	za
německého	německý	k2eAgInSc2d1	německý
útisku	útisk	k1gInSc2	útisk
<g/>
.	.	kIx.	.
</s>
<s>
Zmínil	zmínit	k5eAaPmAgInS	zmínit
se	se	k3xPyFc4	se
o	o	k7c6	o
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
dopisů	dopis	k1gInPc2	dopis
a	a	k8xC	a
deníků	deník	k1gInPc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Anne	Anne	k1gFnSc1	Anne
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
poslat	poslat	k5eAaPmF	poslat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
přijde	přijít	k5eAaPmIp3nS	přijít
vhodná	vhodný	k2eAgFnSc1d1	vhodná
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
upravovat	upravovat	k5eAaImF	upravovat
zápisky	zápiska	k1gFnPc4	zápiska
<g/>
,	,	kIx,	,
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
pasáže	pasáž	k1gFnPc4	pasáž
a	a	k8xC	a
jiné	jiné	k1gNnSc4	jiné
přepisovala	přepisovat	k5eAaImAgFnS	přepisovat
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
zápisník	zápisník	k1gInSc1	zápisník
doplnil	doplnit	k5eAaPmAgInS	doplnit
trhací	trhací	k2eAgInSc1d1	trhací
blok	blok	k1gInSc1	blok
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
pseudonymy	pseudonym	k1gInPc4	pseudonym
pro	pro	k7c4	pro
spolubydlící	spolubydlící	k1gMnPc4	spolubydlící
a	a	k8xC	a
pomocníky	pomocník	k1gMnPc4	pomocník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Van	vana	k1gFnPc2	vana
Pelsových	Pelsových	k2eAgFnSc2d1	Pelsových
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Hermann	Hermann	k1gMnSc1	Hermann
<g/>
,	,	kIx,	,
Petronella	Petronella	k1gMnSc1	Petronella
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
van	van	k1gInSc1	van
Daanovi	Daan	k1gMnSc3	Daan
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Pfeffer	Pfeffer	k1gMnSc1	Pfeffer
získal	získat	k5eAaPmAgMnS	získat
jméno	jméno	k1gNnSc4	jméno
Albert	Albert	k1gMnSc1	Albert
Düssell	Düssell	k1gMnSc1	Düssell
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
Frank	Frank	k1gMnSc1	Frank
použil	použít	k5eAaPmAgMnS	použít
původní	původní	k2eAgInSc4d1	původní
deník	deník	k1gInSc4	deník
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc4d1	známý
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
verze	verze	k1gFnSc1	verze
A	a	k9	a
<g/>
"	"	kIx"	"
a	a	k8xC	a
Anninu	Annin	k2eAgFnSc4d1	Annina
upravenou	upravený	k2eAgFnSc4d1	upravená
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
verze	verze	k1gFnSc1	verze
B	B	kA	B
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Odstranil	odstranit	k5eAaPmAgMnS	odstranit
některé	některý	k3yIgFnPc4	některý
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
hovořily	hovořit	k5eAaImAgFnP	hovořit
nepěkně	pěkně	k6eNd1	pěkně
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
ženě	žena	k1gFnSc6	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zmiňovaly	zmiňovat	k5eAaImAgFnP	zmiňovat
sexuální	sexuální	k2eAgFnPc4d1	sexuální
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Navrátil	navrátit	k5eAaPmAgMnS	navrátit
své	svůj	k3xOyFgFnSc3	svůj
rodině	rodina	k1gFnSc3	rodina
původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
lidí	člověk	k1gMnPc2	člověk
nechal	nechat	k5eAaPmAgMnS	nechat
pseudonymy	pseudonym	k1gInPc7	pseudonym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc4	dětství
==	==	k?	==
</s>
</p>
<p>
<s>
Anne	Anne	k1gFnSc1	Anne
Franková	Franková	k1gFnSc1	Franková
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1929	[number]	k4	1929
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
Otty	Otta	k1gMnSc2	Otta
Heinricha	Heinrich	k1gMnSc2	Heinrich
Franka	Frank	k1gMnSc2	Frank
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1889	[number]	k4	1889
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
a	a	k8xC	a
Edith	Edith	k1gMnSc1	Edith
Holländerové	Holländerová	k1gFnSc2	Holländerová
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Margot	Margot	k1gInSc1	Margot
Franková	Franková	k1gFnSc1	Franková
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1926	[number]	k4	1926
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gNnSc1	její
úředně	úředně	k6eAd1	úředně
vedené	vedený	k2eAgNnSc1d1	vedené
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Anneliese	Anneliese	k1gFnPc4	Anneliese
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
přátelé	přítel	k1gMnPc1	přítel
jí	on	k3xPp3gFnSc2	on
říkali	říkat	k5eAaImAgMnP	říkat
zkráceně	zkráceně	k6eAd1	zkráceně
"	"	kIx"	"
<g/>
Anne	Anne	k1gFnSc1	Anne
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
ji	on	k3xPp3gFnSc4	on
někdy	někdy	k6eAd1	někdy
nazýval	nazývat	k5eAaImAgInS	nazývat
"	"	kIx"	"
<g/>
Annelein	Annelein	k1gInSc1	Annelein
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Anička	Anička	k1gFnSc1	Anička
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
židovských	židovský	k2eAgMnPc2d1	židovský
a	a	k8xC	a
nežidovských	židovský	k2eNgMnPc2d1	nežidovský
spoluobčanů	spoluobčan	k1gMnPc2	spoluobčan
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
vyrostly	vyrůst	k5eAaPmAgFnP	vyrůst
s	s	k7c7	s
katolickými	katolický	k2eAgMnPc7d1	katolický
<g/>
,	,	kIx,	,
protestantskými	protestantský	k2eAgMnPc7d1	protestantský
a	a	k8xC	a
židovskými	židovský	k2eAgMnPc7d1	židovský
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Frankovi	Frankův	k2eAgMnPc1d1	Frankův
byli	být	k5eAaImAgMnP	být
reformní	reformní	k2eAgMnPc1d1	reformní
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
dodržovali	dodržovat	k5eAaImAgMnP	dodržovat
však	však	k9	však
mnohé	mnohý	k2eAgFnPc4d1	mnohá
tradice	tradice	k1gFnPc4	tradice
judaismu	judaismus	k1gInSc2	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Edith	Edith	k1gInSc1	Edith
Franková	Franková	k1gFnSc1	Franková
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
starala	starat	k5eAaImAgNnP	starat
o	o	k7c4	o
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Otto	Otto	k1gMnSc1	Otto
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
vyznamenaný	vyznamenaný	k2eAgMnSc1d1	vyznamenaný
německý	německý	k2eAgMnSc1d1	německý
důstojník	důstojník	k1gMnSc1	důstojník
z	z	k7c2	z
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
učené	učený	k2eAgNnSc4d1	učené
bádání	bádání	k1gNnSc4	bádání
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
velkou	velký	k2eAgFnSc4d1	velká
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1933	[number]	k4	1933
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
komunální	komunální	k2eAgFnSc2d1	komunální
volby	volba	k1gFnSc2	volba
a	a	k8xC	a
Hitlerova	Hitlerův	k2eAgFnSc1d1	Hitlerova
nacistická	nacistický	k2eAgFnSc1d1	nacistická
strana	strana	k1gFnSc1	strana
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
začaly	začít	k5eAaPmAgFnP	začít
antisemitské	antisemitský	k2eAgFnPc1d1	antisemitská
demonstrace	demonstrace	k1gFnPc1	demonstrace
a	a	k8xC	a
Frankovi	Frankův	k2eAgMnPc1d1	Frankův
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
obávat	obávat	k5eAaImF	obávat
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odešla	odejít	k5eAaPmAgFnS	odejít
Edith	Edith	k1gInSc4	Edith
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
do	do	k7c2	do
Cách	Cáchy	k1gFnPc2	Cáchy
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
Rose	Rosa	k1gFnSc3	Rosa
Holländerové	Holländerová	k1gFnSc3	Holländerová
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
Frank	Frank	k1gMnSc1	Frank
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostal	dostat	k5eAaPmAgMnS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
zařídit	zařídit	k5eAaPmF	zařídit
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
ubytování	ubytování	k1gNnSc4	ubytování
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Frank	Frank	k1gMnSc1	Frank
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Opekta	Opekt	k1gInSc2	Opekt
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prodávala	prodávat	k5eAaImAgFnS	prodávat
ovocný	ovocný	k2eAgInSc4d1	ovocný
extrakt	extrakt	k1gInSc4	extrakt
pektin	pektin	k1gInSc4	pektin
<g/>
,	,	kIx,	,
a	a	k8xC	a
našel	najít	k5eAaPmAgMnS	najít
byt	byt	k1gInSc4	byt
na	na	k7c4	na
Merwedeplein	Merwedeplein	k1gInSc4	Merwedeplein
(	(	kIx(	(
<g/>
náměstí	náměstí	k1gNnSc4	náměstí
Merwede	Merwed	k1gMnSc5	Merwed
<g/>
)	)	kIx)	)
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Amsterdamu	Amsterdam	k1gInSc2	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1934	[number]	k4	1934
přijela	přijet	k5eAaPmAgFnS	přijet
Edith	Edith	k1gInSc4	Edith
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
do	do	k7c2	do
montessoriovské	montessoriovská	k1gFnSc2	montessoriovská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Margot	Margota	k1gFnPc2	Margota
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
schopnosti	schopnost	k1gFnPc1	schopnost
v	v	k7c6	v
aritmetice	aritmetika	k1gFnSc6	aritmetika
a	a	k8xC	a
u	u	k7c2	u
Anne	Ann	k1gInSc2	Ann
nadání	nadání	k1gNnSc4	nadání
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
Otto	Otto	k1gMnSc1	Otto
Frank	Frank	k1gMnSc1	Frank
založil	založit	k5eAaPmAgMnS	založit
druhou	druhý	k4xOgFnSc4	druhý
společnost	společnost	k1gFnSc4	společnost
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hermannem	Hermann	k1gMnSc7	Hermann
van	vana	k1gFnPc2	vana
Pelsem	Pels	k1gMnSc7	Pels
<g/>
,	,	kIx,	,
řezníkem	řezník	k1gMnSc7	řezník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
utekl	utéct	k5eAaPmAgMnS	utéct
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
z	z	k7c2	z
Osnabrücku	Osnabrück	k1gInSc2	Osnabrück
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
Edithina	Edithin	k2eAgFnSc1d1	Edithin
matka	matka	k1gFnSc1	matka
přišla	přijít	k5eAaPmAgFnS	přijít
za	za	k7c7	za
Frankovými	Franková	k1gFnPc7	Franková
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
bylo	být	k5eAaImAgNnS	být
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
okupováno	okupován	k2eAgNnSc1d1	okupováno
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
okupační	okupační	k2eAgFnSc1d1	okupační
vláda	vláda	k1gFnSc1	vláda
začala	začít	k5eAaPmAgFnS	začít
perzekvovat	perzekvovat	k5eAaImF	perzekvovat
Židy	Žid	k1gMnPc4	Žid
přijetím	přijetí	k1gNnSc7	přijetí
omezujících	omezující	k2eAgInPc2d1	omezující
a	a	k8xC	a
diskriminačních	diskriminační	k2eAgInPc2d1	diskriminační
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Povinná	povinný	k2eAgFnSc1d1	povinná
registrace	registrace	k1gFnSc1	registrace
a	a	k8xC	a
segregace	segregace	k1gFnSc1	segregace
Židů	Žid	k1gMnPc2	Žid
následovala	následovat	k5eAaImAgFnS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Margot	Margot	k1gInSc1	Margot
a	a	k8xC	a
Anne	Ann	k1gFnPc1	Ann
měly	mít	k5eAaImAgFnP	mít
vynikající	vynikající	k2eAgInSc4d1	vynikající
prospěch	prospěch	k1gInSc4	prospěch
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
Židé	Žid	k1gMnPc1	Žid
směli	smět	k5eAaImAgMnP	smět
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
jen	jen	k9	jen
židovské	židovský	k2eAgFnPc1d1	židovská
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
zapsány	zapsat	k5eAaPmNgInP	zapsat
na	na	k7c4	na
židovské	židovský	k2eAgNnSc4d1	Židovské
lyceum	lyceum	k1gNnSc4	lyceum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpochybňování	zpochybňování	k1gNnPc4	zpochybňování
autentičnosti	autentičnost	k1gFnSc2	autentičnost
==	==	k?	==
</s>
</p>
<p>
<s>
Deník	deník	k1gInSc1	deník
Anny	Anna	k1gFnSc2	Anna
Frankové	Franková	k1gFnSc2	Franková
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gFnSc4	jeho
autentičnost	autentičnost	k1gFnSc4	autentičnost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpochybňována	zpochybňovat	k5eAaImNgFnS	zpochybňovat
některými	některý	k3yIgMnPc7	některý
autory	autor	k1gMnPc7	autor
a	a	k8xC	a
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc1	téma
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
řad	řada	k1gFnPc2	řada
napsáno	napsat	k5eAaPmNgNnS	napsat
několik	několik	k4yIc4	několik
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pravosti	pravost	k1gFnPc4	pravost
deníku	deník	k1gInSc2	deník
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
profesor	profesor	k1gMnSc1	profesor
Robert	Robert	k1gMnSc1	Robert
Faurisson	Faurisson	k1gMnSc1	Faurisson
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
Siegfriedem	Siegfried	k1gMnSc7	Siegfried
Verbekem	Verbek	k1gInSc7	Verbek
brožuru	brožura	k1gFnSc4	brožura
The	The	k1gFnSc2	The
"	"	kIx"	"
<g/>
Diary	Diara	k1gFnSc2	Diara
<g/>
"	"	kIx"	"
of	of	k?	of
Anne	Anne	k1gInSc1	Anne
Frank	Frank	k1gMnSc1	Frank
–	–	k?	–
A	a	k9	a
Critical	Critical	k1gMnSc1	Critical
Evaluation	Evaluation	k1gInSc1	Evaluation
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Deník	deník	k1gInSc1	deník
<g/>
"	"	kIx"	"
Anny	Anna	k1gFnSc2	Anna
Frankové	Franková	k1gFnSc2	Franková
–	–	k?	–
kritické	kritický	k2eAgNnSc4d1	kritické
zhodnocení	zhodnocení	k1gNnSc4	zhodnocení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
předmětem	předmět	k1gInSc7	předmět
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dokazování	dokazování	k1gNnSc1	dokazování
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
podvrh	podvrh	k1gInSc4	podvrh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1980	[number]	k4	1980
Nizozemský	nizozemský	k2eAgInSc1d1	nizozemský
státní	státní	k2eAgInSc1d1	státní
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
válečnou	válečný	k2eAgFnSc4d1	válečná
dokumentaci	dokumentace	k1gFnSc4	dokumentace
obdržel	obdržet	k5eAaPmAgMnS	obdržet
veškeré	veškerý	k3xTgInPc4	veškerý
originály	originál	k1gInPc4	originál
spisů	spis	k1gInPc2	spis
Anne	Anne	k1gFnPc2	Anne
Frankové	Franková	k1gFnSc2	Franková
a	a	k8xC	a
po	po	k7c6	po
sérii	série	k1gFnSc6	série
studií	studie	k1gFnPc2	studie
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
autentičnost	autentičnost	k1gFnSc4	autentičnost
deníku	deník	k1gInSc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
ústavu	ústav	k1gInSc2	ústav
přitom	přitom	k6eAd1	přitom
prozkoumali	prozkoumat	k5eAaPmAgMnP	prozkoumat
mezi	mezi	k7c7	mezi
jiným	jiný	k2eAgInSc7d1	jiný
rodinné	rodinný	k2eAgNnSc4d1	rodinné
zázemí	zázemí	k1gNnPc4	zázemí
<g/>
,	,	kIx,	,
okolnosti	okolnost	k1gFnPc4	okolnost
zatčení	zatčení	k1gNnSc2	zatčení
a	a	k8xC	a
deportace	deportace	k1gFnSc2	deportace
<g/>
,	,	kIx,	,
použité	použitý	k2eAgInPc1d1	použitý
písemné	písemný	k2eAgInPc1d1	písemný
materiály	materiál	k1gInPc1	materiál
a	a	k8xC	a
písmo	písmo	k1gNnSc1	písmo
Anne	Ann	k1gInSc2	Ann
Frankové	Franková	k1gFnSc2	Franková
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
všechny	všechen	k3xTgInPc4	všechen
záznamy	záznam	k1gInPc4	záznam
Anne	Anne	k1gFnSc3	Anne
Frankové	Franková	k1gFnSc2	Franková
společně	společně	k6eAd1	společně
s	s	k7c7	s
výsledky	výsledek	k1gInPc4	výsledek
svého	svůj	k3xOyFgInSc2	svůj
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
719	[number]	k4	719
<g/>
stránkové	stránkový	k2eAgFnSc6d1	stránková
publikaci	publikace	k1gFnSc6	publikace
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Anne	Ann	k1gMnSc2	Ann
Frank	Frank	k1gMnSc1	Frank
Diary	Diara	k1gMnSc2	Diara
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Critical	Critical	k1gMnSc1	Critical
Edition	Edition	k1gInSc1	Edition
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
amsterdamský	amsterdamský	k2eAgInSc1d1	amsterdamský
soud	soud	k1gInSc1	soud
zakázal	zakázat	k5eAaPmAgInS	zakázat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
budoucí	budoucí	k2eAgNnSc1d1	budoucí
zpochybňování	zpochybňování	k1gNnSc1	zpochybňování
jeho	jeho	k3xOp3gFnSc2	jeho
pravosti	pravost	k1gFnSc2	pravost
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
distribuci	distribuce	k1gFnSc4	distribuce
předem	předem	k6eAd1	předem
nevyžádaných	vyžádaný	k2eNgFnPc2d1	nevyžádaná
publikací	publikace	k1gFnPc2	publikace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pravost	pravost	k1gFnSc4	pravost
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
trestem	trest	k1gInSc7	trest
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
guldenů	gulden	k1gInPc2	gulden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
deník	deník	k1gInSc4	deník
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
notorickým	notorický	k2eAgMnPc3d1	notorický
antisemitům	antisemita	k1gMnPc3	antisemita
a	a	k8xC	a
zpochybňovačům	zpochybňovač	k1gMnPc3	zpochybňovač
holocaustu	holocaust	k1gInSc2	holocaust
jako	jako	k8xS	jako
takového	takový	k3xDgNnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
Faurisson	Faurisson	k1gMnSc1	Faurisson
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
tzv.	tzv.	kA	tzv.
plynování	plynování	k1gNnSc4	plynování
<g/>
"	"	kIx"	"
Židů	Žid	k1gMnPc2	Žid
"	"	kIx"	"
<g/>
gigantickým	gigantický	k2eAgInSc7d1	gigantický
politicko-finančním	politickoinanční	k2eAgInSc7d1	politicko-finanční
podvodem	podvod	k1gInSc7	podvod
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
měl	mít	k5eAaImAgInS	mít
prospěch	prospěch	k1gInSc4	prospěch
stát	stát	k5eAaImF	stát
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
sionismus	sionismus	k1gInSc4	sionismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
nekonzistentní	konzistentní	k2eNgFnSc1d1	nekonzistentní
a	a	k8xC	a
prostoupená	prostoupený	k2eAgFnSc1d1	prostoupená
manipulacemi	manipulace	k1gFnPc7	manipulace
s	s	k7c7	s
fakty	fakt	k1gInPc7	fakt
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
Richard	Richard	k1gMnSc1	Richard
Harwood	Harwooda	k1gFnPc2	Harwooda
<g/>
,	,	kIx,	,
zpochybňovač	zpochybňovač	k1gInSc4	zpochybňovač
holocaustu	holocaust	k1gInSc2	holocaust
a	a	k8xC	a
neonacista	neonacista	k1gMnSc1	neonacista
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
Anny	Anna	k1gFnSc2	Anna
Frankové	Franková	k1gFnSc2	Franková
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
cílem	cíl	k1gInSc7	cíl
neonacistů	neonacista	k1gMnPc2	neonacista
a	a	k8xC	a
antisemitů	antisemita	k1gMnPc2	antisemita
právě	právě	k6eAd1	právě
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
své	svůj	k3xOyFgFnSc2	svůj
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památka	památka	k1gFnSc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1988	[number]	k4	1988
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
počest	počest	k1gFnSc4	počest
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
poštovní	poštovní	k2eAgFnSc1d1	poštovní
známka	známka	k1gFnSc1	známka
o	o	k7c6	o
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
0,6	[number]	k4	0,6
šekelů	šekel	k1gInPc2	šekel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Anne	Anne	k1gInSc1	Anne
Frank-Fonds	Frank-Fonds	k1gInSc1	Frank-Fonds
(	(	kIx(	(
<g/>
Fond	fond	k1gInSc1	fond
Anny	Anna	k1gFnSc2	Anna
Frankové	Franková	k1gFnSc2	Franková
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Gesamtausgabe	Gesamtausgab	k1gMnSc5	Gesamtausgab
<g/>
.	.	kIx.	.
</s>
<s>
Tagebücher	Tagebüchra	k1gFnPc2	Tagebüchra
–	–	k?	–
Geschichten	Geschichtno	k1gNnPc2	Geschichtno
und	und	k?	und
Ereignisse	Ereignisse	k1gFnSc2	Ereignisse
aus	aus	k?	aus
dem	dem	k?	dem
Hinterhaus	Hinterhaus	k1gInSc1	Hinterhaus
–	–	k?	–
Erzählungen	Erzählungen	k1gInSc1	Erzählungen
–	–	k?	–
Briefe	Brief	k1gInSc5	Brief
–	–	k?	–
Fotos	Fotos	k1gInSc1	Fotos
und	und	k?	und
Dokumente	dokument	k1gInSc5	dokument
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Celkové	celkový	k2eAgNnSc1d1	celkové
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Deníky	deník	k1gInPc1	deník
-	-	kIx~	-
příběhy	příběh	k1gInPc1	příběh
a	a	k8xC	a
události	událost	k1gFnPc1	událost
ze	z	k7c2	z
zadního	zadní	k2eAgInSc2d1	zadní
domu	dům	k1gInSc2	dům
-	-	kIx~	-
vyprávění	vyprávění	k1gNnPc4	vyprávění
-	-	kIx~	-
dopisy	dopis	k1gInPc1	dopis
-	-	kIx~	-
fotografie	fotografia	k1gFnPc1	fotografia
a	a	k8xC	a
dokumenty	dokument	k1gInPc1	dokument
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Překlad	překlad	k1gInSc1	překlad
z	z	k7c2	z
nizozemštiny	nizozemština	k1gFnSc2	nizozemština
od	od	k7c2	od
Mirjam	Mirjam	k1gFnSc2	Mirjam
Presslerové	Presslerová	k1gFnSc2	Presslerová
<g/>
.	.	kIx.	.
</s>
<s>
Fischer-Verlag	Fischer-Verlag	k1gInSc1	Fischer-Verlag
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
816	[number]	k4	816
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
22304	[number]	k4	22304
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
COMAY	COMAY	kA	COMAY
<g/>
,	,	kIx,	,
Joan	Joan	k1gNnSc1	Joan
<g/>
;	;	kIx,	;
COHN-SHERBOK	COHN-SHERBOK	k1gFnPc1	COHN-SHERBOK
<g/>
,	,	kIx,	,
Lavinia	Lavinium	k1gNnPc1	Lavinium
<g/>
.	.	kIx.	.
</s>
<s>
Who	Who	k?	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Who	Who	k1gMnSc7	Who
in	in	k?	in
Jewish	Jewish	k1gInSc1	Jewish
History	Histor	k1gInPc1	Histor
<g/>
:	:	kIx,	:
After	After	k1gInSc1	After
the	the	k?	the
Period	perioda	k1gFnPc2	perioda
of	of	k?	of
the	the	k?	the
Old	Olda	k1gFnPc2	Olda
Testament	testament	k1gInSc1	testament
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
407	[number]	k4	407
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
415260305	[number]	k4	415260305
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
komiksovém	komiksový	k2eAgNnSc6d1	komiksové
vydání	vydání	k1gNnSc6	vydání
deníku	deník	k1gInSc2	deník
Anne	Ann	k1gFnSc2	Ann
Frankové	Frankové	k2eAgFnSc2d1	Frankové
(	(	kIx(	(
<g/>
Ernie	Ernie	k1gFnSc2	Ernie
Colón	colón	k1gInSc1	colón
a	a	k8xC	a
Sid	Sid	k1gMnSc1	Sid
Jacobson	Jacobson	k1gMnSc1	Jacobson
<g/>
:	:	kIx,	:
Anne	Anne	k1gFnSc1	Anne
Franková	Franková	k1gFnSc1	Franková
<g/>
.	.	kIx.	.
</s>
<s>
Komiksový	komiksový	k2eAgInSc1d1	komiksový
životopis	životopis	k1gInSc1	životopis
<g/>
)	)	kIx)	)
v	v	k7c6	v
jádu-magazínu	jáduagazín	k1gInSc6	jádu-magazín
</s>
</p>
<p>
<s>
Folman	Folman	k1gMnSc1	Folman
<g/>
,	,	kIx,	,
Ari	Ari	k1gMnSc1	Ari
<g/>
;	;	kIx,	;
Polonsky	Polonsky	k1gMnSc1	Polonsky
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
:	:	kIx,	:
Deník	deník	k1gInSc1	deník
Anne	Ann	k1gFnSc2	Ann
Frankové	Franková	k1gFnSc2	Franková
[	[	kIx(	[
<g/>
komiks	komiks	k1gInSc1	komiks
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Triáda	triáda	k1gFnSc1	triáda
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Bradyová	Bradyová	k1gFnSc1	Bradyová
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
Anne	Ann	k1gFnSc2	Ann
Frankové	Franková	k1gFnSc2	Franková
</s>
</p>
<p>
<s>
Jardin	Jardin	k2eAgInSc1d1	Jardin
Anne-Frank	Anne-Frank	k1gInSc1	Anne-Frank
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anne	Anne	k1gFnPc2	Anne
Franková	Franková	k1gFnSc1	Franková
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Anne	Anne	k1gFnSc1	Anne
Franková	Franková	k1gFnSc1	Franková
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Anne	Anne	k1gFnSc1	Anne
Franková	Franková	k1gFnSc1	Franková
</s>
</p>
<p>
<s>
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
Anne	Anne	k1gFnSc1	Anne
Franková	Franková	k1gFnSc1	Franková
<g/>
,	,	kIx,	,
patnáctiletá	patnáctiletý	k2eAgFnSc1d1	patnáctiletá
osobnost	osobnost	k1gFnSc1	osobnost
</s>
</p>
