<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Francie	Francie	k1gFnSc2	Francie
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
svislé	svislý	k2eAgFnSc2d1	svislá
trikolóry	trikolóra	k1gFnSc2	trikolóra
složené	složený	k2eAgFnSc6d1	složená
z	z	k7c2	z
modrého	modrý	k2eAgInSc2d1	modrý
<g/>
,	,	kIx,	,
bílého	bílý	k2eAgInSc2d1	bílý
a	a	k8xC	a
červeného	červený	k2eAgInSc2d1	červený
pruhu	pruh	k1gInSc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tradiční	tradiční	k2eAgFnSc4d1	tradiční
bílou	bílý	k2eAgFnSc4d1	bílá
královskou	královský	k2eAgFnSc4d1	královská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
červená	červené	k1gNnPc1	červené
a	a	k8xC	a
modrá	modré	k1gNnPc1	modré
jsou	být	k5eAaImIp3nP	být
barvy	barva	k1gFnPc1	barva
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
městskou	městský	k2eAgFnSc4d1	městská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kořeny	kořen	k2eAgFnPc1d1	Kořena
francouzské	francouzský	k2eAgFnPc1d1	francouzská
vlajky	vlajka	k1gFnPc1	vlajka
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
uznána	uznat	k5eAaPmNgFnS	uznat
za	za	k7c4	za
státní	státní	k2eAgInSc4d1	státní
symbol	symbol	k1gInSc4	symbol
Francie	Francie	k1gFnSc2	Francie
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1794	[number]	k4	1794
bylo	být	k5eAaImAgNnS	být
pořadí	pořadí	k1gNnSc1	pořadí
barev	barva	k1gFnPc2	barva
obráceno	obrátit	k5eAaPmNgNnS	obrátit
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
byla	být	k5eAaImAgFnS	být
navrácena	navrácen	k2eAgFnSc1d1	navrácena
bílá	bílý	k2eAgFnSc1d1	bílá
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
symbol	symbol	k1gInSc1	symbol
čistoty	čistota	k1gFnSc2	čistota
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
Bourbonů	bourbon	k1gInPc2	bourbon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
na	na	k7c6	na
barikádách	barikáda	k1gFnPc6	barikáda
rudá	rudý	k2eAgFnSc1d1	rudá
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
trikolóra	trikolóra	k1gFnSc1	trikolóra
přijata	přijmout	k5eAaPmNgFnS	přijmout
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předepsanými	předepsaný	k2eAgInPc7d1	předepsaný
odstíny	odstín	k1gInPc7	odstín
jsou	být	k5eAaImIp3nP	být
Pantone	Panton	k1gInSc5	Panton
Reflex	reflex	k1gInSc1	reflex
Blue	Blue	k1gNnPc2	Blue
pro	pro	k7c4	pro
modrou	modrý	k2eAgFnSc4d1	modrá
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
CMYK	CMYK	kA	CMYK
100,70	[number]	k4	100,70
<g/>
,0	,0	k4	,0
<g/>
,5	,5	k4	,5
<g/>
;	;	kIx,	;
RGB	RGB	kA	RGB
#	#	kIx~	#
<g/>
171796	[number]	k4	171796
<g/>
)	)	kIx)	)
a	a	k8xC	a
Red	Red	k1gFnSc1	Red
032	[number]	k4	032
pro	pro	k7c4	pro
červenou	červená	k1gFnSc4	červená
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
CMYK	CMYK	kA	CMYK
0,90	[number]	k4	0,90
<g/>
,86	,86	k4	,86
<g/>
,0	,0	k4	,0
<g/>
;	;	kIx,	;
RGB	RGB	kA	RGB
#	#	kIx~	#
<g/>
ED	ED	kA	ED
<g/>
2	[number]	k4	2
<g/>
E	E	kA	E
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Francie	Francie	k1gFnSc2	Francie
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Francie	Francie	k1gFnSc2	Francie
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Francie	Francie	k1gFnSc2	Francie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
