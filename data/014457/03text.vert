<s>
Lipicán	lipicán	k1gMnSc1
</s>
<s>
Lipicán	lipicán	k1gMnSc1
Moderní	moderní	k2eAgMnSc1d1
lipicánZákladní	lipicánZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
Slovinsko	Slovinsko	k1gNnSc1
Využití	využití	k1gNnSc1
</s>
<s>
jízdní	jízdní	k2eAgInPc4d1
<g/>
,	,	kIx,
drezura	drezura	k1gFnSc1
Tělesná	tělesný	k2eAgFnSc1d1
charakteristika	charakteristika	k1gFnSc1
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
450	#num#	k4
–	–	k?
550	#num#	k4
kg	kg	kA
Výška	výška	k1gFnSc1
†	†	k?
</s>
<s>
147	#num#	k4
–	–	k?
165	#num#	k4
cm	cm	kA
Tělesný	tělesný	k2eAgInSc4d1
rámec	rámec	k1gInSc4
</s>
<s>
Obdélníkový	obdélníkový	k2eAgInSc1d1
</s>
<s>
†	†	k?
výška	výška	k1gFnSc1
uváděna	uváděn	k2eAgFnSc1d1
v	v	k7c6
kohoutku	kohoutek	k1gInSc6
</s>
<s>
Lipicán	lipicán	k1gMnSc1
ve	v	k7c6
Španělské	španělský	k2eAgFnSc6d1
jezdecké	jezdecký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
</s>
<s>
Lipicán	lipicán	k1gMnSc1
v	v	k7c6
hřebčíně	hřebčín	k1gInSc6
v	v	k7c6
Lipici	Lipice	k1gFnSc6
</s>
<s>
Lipicán	lipicán	k1gMnSc1
(	(	kIx(
<g/>
slovinsky	slovinsky	k6eAd1
Lipicanec	Lipicanec	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
koňské	koňský	k2eAgNnSc1d1
plemeno	plemeno	k1gNnSc1
úzce	úzko	k6eAd1
spojené	spojený	k2eAgNnSc1d1
se	s	k7c7
Španělskou	španělský	k2eAgFnSc7d1
jezdeckou	jezdecký	k2eAgFnSc7d1
školou	škola	k1gFnSc7
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc4
chovu	chov	k1gInSc2
těchto	tento	k3xDgMnPc2
koní	kůň	k1gMnPc2
sahají	sahat	k5eAaImIp3nP
do	do	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
do	do	k7c2
slovinské	slovinský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
Lipica	Lipic	k1gInSc2
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
nese	nést	k5eAaImIp3nS
celé	celý	k2eAgNnSc4d1
plemeno	plemeno	k1gNnSc4
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
chovu	chov	k1gInSc2
</s>
<s>
Rok	rok	k1gInSc1
po	po	k7c6
založení	založení	k1gNnSc6
hřebčína	hřebčín	k1gInSc2
v	v	k7c6
Kladrubech	Kladruby	k1gInPc6
rozhodl	rozhodnout	k5eAaPmAgMnS
císař	císař	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
roku	rok	k1gInSc2
1580	#num#	k4
o	o	k7c6
založení	založení	k1gNnSc6
dalšího	další	k2eAgNnSc2d1
chovatelského	chovatelský	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
v	v	k7c6
Lipici	Lipice	k1gFnSc6
nedaleko	nedaleko	k7c2
Terstu	Terst	k1gInSc2
u	u	k7c2
Jadranu	Jadran	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rakouském	rakouský	k2eAgInSc6d1
Piberu	Piber	k1gInSc6
nedaleko	nedaleko	k7c2
Grazu	Graz	k1gInSc2
je	být	k5eAaImIp3nS
chovná	chovný	k2eAgFnSc1d1
základna	základna	k1gFnSc1
lipicánů	lipicán	k1gMnPc2
pro	pro	k7c4
Španělskou	španělský	k2eAgFnSc4d1
jezdeckou	jezdecký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
(	(	kIx(
<g/>
Spanische	Spanische	k1gInSc1
Hofreitschule	Hofreitschule	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
koně	kůň	k1gMnPc1
od	od	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
válečných	válečný	k2eAgNnPc2d1
let	léto	k1gNnPc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
chov	chov	k1gInSc1
přesunut	přesunout	k5eAaPmNgInS
do	do	k7c2
městečka	městečko	k1gNnSc2
Hostouň	Hostouň	k1gFnSc1
na	na	k7c6
Domažlicku	Domažlicko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgInSc4d2
počet	počet	k1gInSc4
lipicánů	lipicán	k1gMnPc2
najdeme	najít	k5eAaPmIp1nP
také	také	k9
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
v	v	k7c6
městečku	městečko	k1gNnSc6
Topoľčianky	Topoľčianka	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
a	a	k8xC
v	v	k7c6
Srbsku	Srbsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Vídeňští	vídeňský	k2eAgMnPc1d1
lipicáni	lipicán	k1gMnPc1
</s>
<s>
Hřebčín	hřebčín	k1gInSc1
v	v	k7c6
Piberu	Piber	k1gInSc6
dodává	dodávat	k5eAaImIp3nS
koně	kůň	k1gMnSc4
pro	pro	k7c4
vídeňskou	vídeňský	k2eAgFnSc4d1
Španělskou	španělský	k2eAgFnSc4d1
jezdeckou	jezdecký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladí	mladý	k2eAgMnPc1d1
hřebci	hřebec	k1gMnPc1
jsou	být	k5eAaImIp3nP
umístěni	umístit	k5eAaPmNgMnP
na	na	k7c4
alpské	alpský	k2eAgFnPc4d1
pastviny	pastvina	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
několik	několik	k4yIc1
let	léto	k1gNnPc2
sílí	sílet	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
vybraní	vybraný	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
pak	pak	k6eAd1
putují	putovat	k5eAaImIp3nP
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
k	k	k7c3
dalšímu	další	k2eAgInSc3d1
výcviku	výcvik	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pěti	pět	k4xCc7
až	až	k9
sedmi	sedm	k4xCc7
letech	léto	k1gNnPc6
jsou	být	k5eAaImIp3nP
připraveni	připravit	k5eAaPmNgMnP
k	k	k7c3
vystoupení	vystoupení	k1gNnSc3
před	před	k7c7
obecenstvem	obecenstvo	k1gNnSc7
formou	forma	k1gFnSc7
Morgenarbeit	Morgenarbeit	k2eAgInSc4d1
(	(	kIx(
<g/>
dopolední	dopolední	k2eAgInSc4d1
trénink	trénink	k1gInSc4
za	za	k7c4
poplatek	poplatek	k1gInSc4
přístupný	přístupný	k2eAgInSc4d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
)	)	kIx)
nebo	nebo	k8xC
večerních	večerní	k2eAgNnPc2d1
představení	představení	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
jsou	být	k5eAaImIp3nP
ovšem	ovšem	k9
na	na	k7c4
dlouhé	dlouhý	k2eAgInPc4d1
měsíce	měsíc	k1gInPc4
předem	předem	k6eAd1
vyprodaná	vyprodaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	dík	k1gInPc1
své	svůj	k3xOyFgFnSc2
odolnosti	odolnost	k1gFnSc2
a	a	k8xC
dlouhověkosti	dlouhověkost	k1gFnSc2
nejsou	být	k5eNaImIp3nP
výjimkou	výjimka	k1gFnSc7
koně	kůň	k1gMnPc1
vystupující	vystupující	k2eAgMnPc1d1
ve	v	k7c6
věku	věk	k1gInSc6
dvaceti	dvacet	k4xCc2
až	až	k8xS
pětadvaceti	pětadvacet	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
inventáře	inventář	k1gInSc2
vídeňské	vídeňský	k2eAgFnPc1d1
jezdecké	jezdecký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
jsou	být	k5eAaImIp3nP
i	i	k9
postroje	postroj	k1gInPc1
a	a	k8xC
barokní	barokní	k2eAgFnSc1d1
sedla	sednout	k5eAaPmAgFnS
stará	starý	k2eAgFnSc1d1
200	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
stále	stále	k6eAd1
plně	plně	k6eAd1
funkční	funkční	k2eAgNnPc1d1
a	a	k8xC
používaná	používaný	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
roku	rok	k1gInSc2
tráví	trávit	k5eAaImIp3nP
vystupující	vystupující	k2eAgMnPc1d1
koně	kůň	k1gMnPc1
na	na	k7c6
"	"	kIx"
<g/>
letním	letní	k2eAgInSc6d1
bytě	byt	k1gInSc6
<g/>
"	"	kIx"
v	v	k7c6
rakouském	rakouský	k2eAgInSc6d1
Heldenbergu	Heldenberg	k1gInSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
jsou	být	k5eAaImIp3nP
každoročně	každoročně	k6eAd1
převezeni	převézt	k5eAaPmNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
relaxovali	relaxovat	k5eAaBmAgMnP
a	a	k8xC
nabrali	nabrat	k5eAaPmAgMnP
síly	síla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
je	on	k3xPp3gInPc4
navštívit	navštívit	k5eAaPmF
a	a	k8xC
zhlédnout	zhlédnout	k5eAaPmF
občasné	občasný	k2eAgNnSc4d1
vystoupení	vystoupení	k1gNnSc4
těchto	tento	k3xDgFnPc2
překrásných	překrásný	k2eAgFnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
plemene	plemeno	k1gNnSc2
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
středně	středně	k6eAd1
velký	velký	k2eAgMnSc1d1
teplokrevník	teplokrevník	k1gMnSc1
<g/>
,	,	kIx,
zástupci	zástupce	k1gMnPc1
tohoto	tento	k3xDgNnSc2
plemene	plemeno	k1gNnSc2
mají	mít	k5eAaImIp3nP
většinou	většinou	k6eAd1
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
srsti	srst	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
kůň	kůň	k1gMnSc1
velmi	velmi	k6eAd1
mírumilovný	mírumilovný	k2eAgInSc4d1
<g/>
,	,	kIx,
hodný	hodný	k2eAgInSc4d1
<g/>
,	,	kIx,
vyniká	vynikat	k5eAaImIp3nS
svojí	svůj	k3xOyFgFnSc7
učenlivostí	učenlivost	k1gFnSc7
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
hodně	hodně	k6eAd1
používá	používat	k5eAaImIp3nS
v	v	k7c6
cirkusech	cirkus	k1gInPc6
a	a	k8xC
jezdeckých	jezdecký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
vhodný	vhodný	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
kůň	kůň	k1gMnSc1
jezdecký	jezdecký	k2eAgMnSc1d1
nebo	nebo	k8xC
kočárový	kočárový	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
tak	tak	k6eAd1
rychlý	rychlý	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
spíše	spíše	k9
vytrvalý	vytrvalý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
blízký	blízký	k2eAgMnSc1d1
příbuzný	příbuzný	k1gMnSc1
starokladrubského	starokladrubský	k2eAgMnSc2d1
koně	kůň	k1gMnSc2
(	(	kIx(
<g/>
Equus	Equus	k1gMnSc1
Bohemicus	Bohemicus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
u	u	k7c2
starokladrubského	starokladrubský	k2eAgInSc2d1
bělouše	bělouš	k1gInSc2
se	se	k3xPyFc4
hříbata	hříbě	k1gNnPc1
rodí	rodit	k5eAaImIp3nP
téměř	téměř	k6eAd1
černá	černý	k2eAgFnSc1d1
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
začínají	začínat	k5eAaImIp3nP
šednout	šednout	k5eAaImF
<g/>
,	,	kIx,
až	až	k8xS
srst	srst	k1gFnSc1
zbělá	zbělat	k5eAaPmIp3nS
–	–	k?
tento	tento	k3xDgInSc4
proces	proces	k1gInSc4
může	moct	k5eAaImIp3nS
trvat	trvat	k5eAaImF
i	i	k8xC
desítky	desítka	k1gFnPc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příležitostně	příležitostně	k6eAd1
se	se	k3xPyFc4
vyskytnou	vyskytnout	k5eAaPmIp3nP
i	i	k9
hnědáci	hnědák	k1gMnPc1
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
však	však	k9
nejsou	být	k5eNaImIp3nP
zařazeni	zařadit	k5eAaPmNgMnP
do	do	k7c2
chovu	chov	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
je	být	k5eAaImIp3nS
tradicí	tradice	k1gFnSc7
Španělské	španělský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
mít	mít	k5eAaImF
jednoho	jeden	k4xCgMnSc4
hnědáka	hnědák	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lipicán	lipicán	k1gMnSc1
dospívá	dospívat	k5eAaImIp3nS
pomalu	pomalu	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
výkonnost	výkonnost	k1gFnSc1
si	se	k3xPyFc3
drží	držet	k5eAaImIp3nS
do	do	k7c2
vysokého	vysoký	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Chovné	chovný	k2eAgFnPc1d1
linie	linie	k1gFnPc1
</s>
<s>
V	v	k7c6
chovu	chov	k1gInSc6
lipicána	lipicán	k1gMnSc2
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
šest	šest	k4xCc1
linií	linie	k1gFnPc2
podle	podle	k7c2
šesti	šest	k4xCc2
hlavních	hlavní	k2eAgMnPc2d1
hřebců	hřebec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
zakladatele	zakladatel	k1gMnPc4
slavných	slavný	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc1
<g/>
:	:	kIx,
</s>
<s>
Favory	Favory	k?
–	–	k?
plavák	plavák	k1gMnSc1
<g/>
,	,	kIx,
narozený	narozený	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1779	#num#	k4
v	v	k7c6
hřebčíně	hřebčín	k1gInSc6
v	v	k7c6
Kladrubech	Kladruby	k1gInPc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Maestoso	maestoso	k1gNnSc1
–	–	k?
kladrubský	kladrubský	k2eAgMnSc1d1
bělouš	bělouš	k1gMnSc1
<g/>
,	,	kIx,
narozený	narozený	k2eAgMnSc1d1
roku	rok	k1gInSc2
1773	#num#	k4
</s>
<s>
Conversano	Conversana	k1gFnSc5
–	–	k?
vraník	vraník	k1gMnSc1
<g/>
,	,	kIx,
narozený	narozený	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1767	#num#	k4
</s>
<s>
Neapolitano	Neapolitana	k1gFnSc5
–	–	k?
hnědák	hnědák	k1gMnSc1
<g/>
,	,	kIx,
narozený	narozený	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1790	#num#	k4
</s>
<s>
Pluto	Pluto	k1gMnSc1
–	–	k?
bělouš	bělouš	k1gMnSc1
španělského	španělský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
narozený	narozený	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1795	#num#	k4
</s>
<s>
Siglavy	Siglava	k1gFnPc1
–	–	k?
pravý	pravý	k2eAgMnSc1d1
arabský	arabský	k2eAgMnSc1d1
hřebec	hřebec	k1gMnSc1
<g/>
,	,	kIx,
nar	nar	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1810	#num#	k4
<g/>
,	,	kIx,
import	import	k1gInSc1
z	z	k7c2
Arábie	Arábie	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
DOLEŽAL	Doležal	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
DOLEŽALOVÁ	Doležalová	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
a	a	k8xC
kůň	kůň	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Dona	dona	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85463	#num#	k4
<g/>
-	-	kIx~
<g/>
52	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Skupina	skupina	k1gFnSc1
koní	kůň	k1gMnPc2
východních	východní	k2eAgInPc2d1
<g/>
,	,	kIx,
s.	s.	k?
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lipicán	lipicán	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Lipicán	lipicán	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Koně	kůň	k1gMnPc1
</s>
