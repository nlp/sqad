<s>
The	The	k?
Shard	Shard	k1gInSc1
</s>
<s>
The	The	k?
Shard	Shard	k1gInSc1
Účel	účel	k1gInSc1
stavby	stavba	k1gFnSc2
</s>
<s>
1-28	1-28	k4
kanceláře	kancelář	k1gFnSc2
</s>
<s>
29-33	29-33	k4
restaurace	restaurace	k1gFnSc1
</s>
<s>
34-52	34-52	k4
hotel	hotel	k1gInSc1
Shangri-La	Shangri-L	k1gInSc2
</s>
<s>
53-67	53-67	k4
byty	byt	k1gInPc7
</s>
<s>
68-72	68-72	k4
vyhlídkové	vyhlídkový	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
novofuturismus	novofuturismus	k1gInSc1
a	a	k8xC
high-tech	high-t	k1gInPc6
architektura	architektura	k1gFnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
Renzo	Renza	k1gFnSc5
Piano	piano	k1gNnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
Materiály	materiál	k1gInPc4
</s>
<s>
sklo	sklo	k1gNnSc1
<g/>
,	,	kIx,
kompozitní	kompozitní	k2eAgInSc1d1
materiál	materiál	k1gInSc1
<g/>
,	,	kIx,
ocel	ocel	k1gFnSc1
a	a	k8xC
beton	beton	k1gInSc1
Stavitel	stavitel	k1gMnSc1
</s>
<s>
Mace	Mace	k6eAd1
Pojmenováno	pojmenovat	k5eAaPmNgNnS
po	po	k7c6
</s>
<s>
střep	střep	k1gInSc1
Technické	technický	k2eAgInPc1d1
parametry	parametr	k1gInPc1
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc4
</s>
<s>
309,6	309,6	k4
m	m	kA
Výška	výška	k1gFnSc1
střechy	střecha	k1gFnSc2
</s>
<s>
304,1	304,1	k4
m	m	kA
Počet	počet	k1gInSc1
podlaží	podlaží	k1gNnSc2
</s>
<s>
72	#num#	k4
(	(	kIx(
<g/>
+	+	kIx~
dalších	další	k2eAgNnPc2d1
15	#num#	k4
tvoří	tvořit	k5eAaImIp3nS
skleněná	skleněný	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
<g/>
)	)	kIx)
Podlahová	podlahový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
</s>
<s>
110	#num#	k4
000	#num#	k4
m	m	kA
<g/>
2	#num#	k4
Výškový	výškový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3
budova	budova	k1gFnSc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
Předchůdce	předchůdce	k1gMnSc2
</s>
<s>
City	city	k1gNnSc1
of	of	k?
Capitals	Capitalsa	k1gFnPc2
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Mercury	Mercura	k1gFnPc1
City	City	k1gFnSc2
Tower	Towra	k1gFnPc2
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
London	London	k1gMnSc1
Bridge	Bridg	k1gFnSc2
Street	Street	k1gMnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc4
</s>
<s>
www.the-shard.com	www.the-shard.com	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Shard	Shard	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
doslovně	doslovně	k6eAd1
„	„	k?
<g/>
střep	střep	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
také	také	k9
jako	jako	k9
Shard	Shard	k1gMnSc1
of	of	k?
Glass	Glass	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Shard	Shard	k1gInSc1
London	London	k1gMnSc1
Bridge	Bridge	k1gInSc1
nebo	nebo	k8xC
London	London	k1gMnSc1
Bridge	Bridge	k1gFnSc2
Tower	Tower	k1gMnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
londýnský	londýnský	k2eAgInSc4d1
mrakodrap	mrakodrap	k1gInSc4
<g/>
,	,	kIx,
s	s	k7c7
310	#num#	k4
metry	metr	k1gInPc1
nejvyšší	vysoký	k2eAgInPc1d3
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
čtvrtá	čtvrtý	k4xOgFnSc1
nejvyšší	vysoký	k2eAgFnSc1d3
budova	budova	k1gFnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architektem	architekt	k1gMnSc7
stavby	stavba	k1gFnSc2
je	být	k5eAaImIp3nS
Ital	Ital	k1gMnSc1
Renzo	Renza	k1gFnSc5
Piano	piano	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
výstavby	výstavba	k1gFnSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
částky	částka	k1gFnPc4
450	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Odspoda	Odspoda	k?
budova	budova	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
kanceláře	kancelář	k1gFnPc4
<g/>
,	,	kIx,
ve	v	k7c6
vyšších	vysoký	k2eAgNnPc6d2
patrech	patro	k1gNnPc6
je	být	k5eAaImIp3nS
hotel	hotel	k1gInSc1
<g/>
,	,	kIx,
nad	nad	k7c7
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
byty	byt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnPc4d3
obyvatelná	obyvatelný	k2eAgNnPc4d1
patra	patro	k1gNnPc4
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
turistická	turistický	k2eAgFnSc1d1
vyhlídka	vyhlídka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
budovy	budova	k1gFnSc2
začala	začít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
místě	místo	k1gNnSc6
kancelářského	kancelářský	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
Southwark	Southwark	k1gInSc1
Towers	Towers	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
zbourán	zbourat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
2012	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgMnS
Shard	Shard	k1gMnSc1
London	London	k1gMnSc1
Bridge	Bridge	k1gNnSc7
plné	plný	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
a	a	k8xC
k	k	k7c3
otevření	otevření	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
katarského	katarský	k2eAgMnSc2d1
premiéra	premiér	k1gMnSc2
šajcha	šajch	k1gMnSc2
Hamáda	Hamáda	k1gFnSc1
Ibn	Ibn	k1gFnSc1
Džásima	Džásima	k1gFnSc1
Ibn	Ibn	k1gFnSc7
Džabera	Džaber	k1gMnSc2
as-Sáního	as-Sání	k1gMnSc2
a	a	k8xC
člena	člen	k1gMnSc2
britské	britský	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
vévody	vévoda	k1gMnSc2
Andrewa	Andrewus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Majitelem	majitel	k1gMnSc7
mrakodrapu	mrakodrap	k1gInSc2
je	být	k5eAaImIp3nS
emirát	emirát	k1gInSc1
Katar	katar	k1gInSc1
s	s	k7c7
95	#num#	k4
<g/>
%	%	kIx~
vlastnickým	vlastnický	k2eAgInSc7d1
podílem	podíl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BAR-HILLEL	BAR-HILLEL	k1gFnSc1
<g/>
,	,	kIx,
Mira	Mira	k1gFnSc1
<g/>
.	.	kIx.
£	£	k?
<g/>
28	#num#	k4
<g/>
bn	bn	k?
Shard	Shard	k1gMnSc1
of	of	k?
Glass	Glass	k1gInSc1
to	ten	k3xDgNnSc1
start	start	k1gInSc1
its	its	k?
ascent	ascent	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
Evening	Evening	k1gInSc4
Standard	standard	k1gInSc1
<g/>
.	.	kIx.
24	#num#	k4
February	Februara	k1gFnSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Work	Work	k1gMnSc1
starts	starts	k6eAd1
on	on	k3xPp3gMnSc1
Shard	Shard	k1gMnSc1
of	of	k?
Glass	Glass	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Civil	civil	k1gMnSc1
Engineer	Engineer	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
April	April	k1gInSc1
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
7	#num#	k4
July	Jula	k1gFnSc2
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
London	London	k1gMnSc1
Bridge	Bridge	k1gNnSc1
Tower	Tower	k1gMnSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Designbuild-network	Designbuild-network	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
15	#num#	k4
June	jun	k1gMnSc5
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Shard	Shard	k1gInSc1
funding	funding	k1gInSc1
crisis	crisis	k1gFnSc1
<g/>
:	:	kIx,
Tower	Tower	k1gMnSc1
finances	finances	k1gMnSc1
cast	cast	k1gMnSc1
shadow	shadow	k?
over	over	k1gMnSc1
project	project	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Architecture	Architectur	k1gMnSc5
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
10	#num#	k4
September	September	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
V	v	k7c6
Londýně	Londýn	k1gInSc6
otevírají	otevírat	k5eAaImIp3nP
mrakodrap	mrakodrap	k1gInSc4
Střep	střep	k1gInSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgFnSc4d3
budovu	budova	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
iDNS	iDNS	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
5.7	5.7	k4
<g/>
.2012	.2012	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
budov	budova	k1gFnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Seznam	seznam	k1gInSc1
nejvyšších	vysoký	k2eAgFnPc2d3
budov	budova	k1gFnPc2
světa	svět	k1gInSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normat	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
<g/>
}	}	kIx)
Významné	významný	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
a	a	k8xC
památníky	památník	k1gInPc4
v	v	k7c6
Londýně	Londýn	k1gInSc6
Významné	významný	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
a	a	k8xC
stavby	stavba	k1gFnSc2
</s>
<s>
1	#num#	k4
Canada	Canada	k1gFnSc1
Square	square	k1gInSc1
<g/>
30	#num#	k4
St	St	kA
Mary	Mary	k1gFnPc2
AxeBank	AxeBanka	k1gFnPc2
of	of	k?
EnglandBig	EnglandBiga	k1gFnPc2
BenBritská	BenBritský	k2eAgFnSc1d1
knihovnaBroadcasting	knihovnaBroadcasting	k1gInSc4
HouseBuckinghamský	HouseBuckinghamský	k2eAgInSc4d1
palácBudova	palácBudův	k2eAgFnSc1d1
pojišťovny	pojišťovna	k1gFnSc2
LloydsLondýnská	LloydsLondýnský	k2eAgFnSc1d1
radniceClarence	radniceClarence	k1gFnSc1
HouseElektrárna	HouseElektrárna	k1gFnSc1
BatterseaHampton	BatterseaHampton	k1gInSc1
Court	Court	k1gInSc1
PalaceKatedrála	PalaceKatedrál	k1gMnSc2
svatého	svatý	k2eAgMnSc2d1
PavlaKensingtonský	PavlaKensingtonský	k2eAgInSc4d1
palácKrálovská	palácKrálovský	k2eAgNnPc1d1
burzaKrálovská	burzaKrálovský	k2eAgNnPc1d1
greenwichská	greenwichský	k2eAgNnPc1d1
observatořKrálovský	observatořKrálovský	k2eAgMnSc1d1
soudní	soudní	k2eAgMnSc1d1
dvůrLambethský	dvůrLambethský	k1gMnSc1
palácMillennium	palácMillennium	k1gNnSc4
DomeMramorový	DomeMramorový	k2eAgMnSc1d1
obloukNeasdenský	obloukNeasdenský	k2eAgMnSc1d1
chrámRoyal	chrámRoyal	k1gMnSc1
Albert	Albert	k1gMnSc1
HallRoyal	HallRoyal	k1gMnSc1
Festival	festival	k1gInSc4
HallSt	HallSt	k2eAgInSc4d1
James	James	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
PalaceTowerTower	PalaceTowerTowra	k1gFnPc2
42	#num#	k4
<g/>
Věž	věž	k1gFnSc4
British	Britisha	k1gFnPc2
TelecomWestminsterské	TelecomWestminsterský	k2eAgFnSc2d1
opatstvíWestminsterský	opatstvíWestminsterský	k2eAgInSc4d1
palácThe	palácThe	k1gInSc4
Shard	Sharda	k1gFnPc2
Památníky	památník	k1gInPc7
</s>
<s>
Albertův	Albertův	k2eAgMnSc1d1
památníkKleopatřin	památníkKleopatřin	k2eAgMnSc1d1
obeliskNelsonův	obeliskNelsonův	k2eAgMnSc1d1
sloupPamátník	sloupPamátník	k1gMnSc1
Diany	Diana	k1gFnSc2
<g/>
,	,	kIx,
princezny	princezna	k1gFnSc2
z	z	k7c2
WalesuPamátník	WalesuPamátník	k1gMnSc1
Velkého	velký	k2eAgInSc2d1
požáru	požár	k1gInSc2
Londýna	Londýn	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1045328480	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2016000852	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
305441807	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2016000852	#num#	k4
</s>
