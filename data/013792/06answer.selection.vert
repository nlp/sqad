<s>
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1879	#num#	k4
Lysá	lysat	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1952	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
mezinárodně	mezinárodně	k6eAd1
uznávaný	uznávaný	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
klínopisec	klínopisec	k1gMnSc1
a	a	k8xC
orientalista	orientalista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
rozluštil	rozluštit	k5eAaPmAgInS
jazyk	jazyk	k1gInSc4
starověkých	starověký	k2eAgMnPc2d1
Chetitů	Chetit	k1gMnPc2
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
položil	položit	k5eAaPmAgMnS
tak	tak	k8xS,k8xC
základy	základ	k1gInPc4
oboru	obor	k1gInSc2
<g />
.	.	kIx.
</s>