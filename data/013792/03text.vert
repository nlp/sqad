<s>
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
</s>
<s>
Prof.	prof.	kA
PhDr.	PhDr.	kA
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgInSc4d1
ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
Narození	narození	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1879	#num#	k4
<g/>
Lysá	lysat	k5eAaImIp3nS
nad	nad	k7c4
LabemRakousko-Uhersko	LabemRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1952	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
73	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
evangelický	evangelický	k2eAgInSc1d1
hřbitov	hřbitov	k1gInSc1
v	v	k7c6
Lysé	Lysá	k1gFnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Bydliště	bydliště	k1gNnSc2
</s>
<s>
Ořechovka	ořechovka	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1
univerzitaHumboldtova	univerzitaHumboldtův	k2eAgFnSc1d1
univerzitaEvangelická	univerzitaEvangelický	k2eAgFnSc1d1
teologická	teologický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
orientalista	orientalista	k1gMnSc1
<g/>
,	,	kIx,
archeolog	archeolog	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
Hittitologist	Hittitologist	k1gMnSc1
<g/>
,	,	kIx,
asyriolog	asyriolog	k1gMnSc1
<g/>
,	,	kIx,
knihovník	knihovník	k1gMnSc1
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
a	a	k8xC
učitel	učitel	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
KarlovaVídeňská	KarlovaVídeňský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
Evangelická	evangelický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
(	(	kIx(
<g/>
1781	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1879	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
Českobratrská	českobratrský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
evangelická	evangelický	k2eAgFnSc1d1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
Rodiče	rodič	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
a	a	k8xC
Marie	Marie	k1gFnSc1
Tardyová	Tardyová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnSc3
<g/>
)	)	kIx)
Funkce	funkce	k1gFnSc1
</s>
<s>
děkan	děkan	k1gMnSc1
(	(	kIx(
<g/>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
<g/>
;	;	kIx,
1926	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
<g/>
rektor	rektor	k1gMnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
presbyter	presbyter	k1gMnSc1
(	(	kIx(
<g/>
Farní	farní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Českobratrské	českobratrský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
evangelické	evangelický	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
1	#num#	k4
-	-	kIx~
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
se	se	k3xPyFc4
svým	svůj	k3xOyFgMnSc7
žákem	žák	k1gMnSc7
Josefem	Josef	k1gMnSc7
Klímou	Klíma	k1gMnSc7
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1879	#num#	k4
Lysá	lysat	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
–	–	k?
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1952	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
mezinárodně	mezinárodně	k6eAd1
uznávaný	uznávaný	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
klínopisec	klínopisec	k1gMnSc1
a	a	k8xC
orientalista	orientalista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
rozluštil	rozluštit	k5eAaPmAgInS
jazyk	jazyk	k1gInSc4
starověkých	starověký	k2eAgMnPc2d1
Chetitů	Chetit	k1gMnPc2
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
položil	položit	k5eAaPmAgMnS
tak	tak	k8xS,k8xC
základy	základ	k1gInPc4
oboru	obor	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
chetitologie	chetitologie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
UK	UK	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
založil	založit	k5eAaPmAgMnS
a	a	k8xC
řídil	řídit	k5eAaImAgInS
Seminář	seminář	k1gInSc1
pro	pro	k7c4
klínopisné	klínopisný	k2eAgNnSc4d1
badání	badání	k1gNnSc4
a	a	k8xC
dějiny	dějiny	k1gFnPc4
starého	starý	k2eAgInSc2d1
Orientu	Orient	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1926	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
byl	být	k5eAaImAgMnS
děkanem	děkan	k1gMnSc7
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
UK	UK	kA
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
1939	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
rektorem	rektor	k1gMnSc7
UK	UK	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
odvážně	odvážně	k6eAd1
postavil	postavit	k5eAaPmAgMnS
německým	německý	k2eAgMnSc7d1
vojákům	voják	k1gMnPc3
zasahujícím	zasahující	k2eAgMnPc3d1
proti	proti	k7c3
českým	český	k2eAgMnPc3d1
studentům	student	k1gMnPc3
na	na	k7c6
půdě	půda	k1gFnSc6
Právnické	právnický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
atria	atrium	k1gNnSc2
právnické	právnický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
,	,	kIx,
postavou	postava	k1gFnSc7
vysoký	vysoký	k2eAgMnSc1d1
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
stál	stát	k5eAaImAgMnS
rozkročen	rozkročen	k2eAgMnSc1d1
na	na	k7c6
schodech	schod	k1gInPc6
a	a	k8xC
zařval	zařvat	k5eAaPmAgMnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Ven	ven	k6eAd1
darebáci	darebák	k1gMnPc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
akademická	akademický	k2eAgFnSc1d1
půda	půda	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
tehdy	tehdy	k6eAd1
opravdu	opravdu	k6eAd1
odešli	odejít	k5eAaPmAgMnP
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Lysé	Lysá	k1gFnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
jako	jako	k9
syn	syn	k1gMnSc1
evangelického	evangelický	k2eAgMnSc2d1
faráře	farář	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
na	na	k7c6
Akademickém	akademický	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
středoškolská	středoškolský	k2eAgNnPc4d1
studia	studio	k1gNnPc4
dokončil	dokončit	k5eAaPmAgMnS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Kolíně	Kolín	k1gInSc6
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gNnSc4
učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
nadšený	nadšený	k2eAgMnSc1d1
orientalista	orientalista	k1gMnSc1
Justin	Justin	k1gMnSc1
Václav	Václav	k1gMnSc1
Prášek	Prášek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
tehdy	tehdy	k6eAd1
povinné	povinný	k2eAgFnSc2d1
latiny	latina	k1gFnSc2
a	a	k8xC
klasické	klasický	k2eAgFnSc2d1
řečtiny	řečtina	k1gFnSc2
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgMnS
základům	základ	k1gInPc3
hebrejštiny	hebrejština	k1gFnSc2
a	a	k8xC
arabštiny	arabština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgNnPc6
studiích	studio	k1gNnPc6
pokračoval	pokračovat	k5eAaImAgMnS
na	na	k7c4
přání	přání	k1gNnSc4
otce	otec	k1gMnSc2
na	na	k7c6
bohoslovecké	bohoslovecký	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
vídeňské	vídeňský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
vydržel	vydržet	k5eAaPmAgMnS
pouhý	pouhý	k2eAgInSc4d1
jeden	jeden	k4xCgInSc4
semestr	semestr	k1gInSc4
a	a	k8xC
přestoupil	přestoupit	k5eAaPmAgMnS
na	na	k7c4
fakultu	fakulta	k1gFnSc4
filosofickou	filosofický	k2eAgFnSc4d1
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
zejména	zejména	k9
orientálním	orientální	k2eAgInPc3d1
jazykům	jazyk	k1gInPc3
<g/>
,	,	kIx,
především	především	k6eAd1
akkadštině	akkadština	k1gFnSc3
<g/>
,	,	kIx,
aramejštině	aramejština	k1gFnSc6
<g/>
,	,	kIx,
etiopštině	etiopština	k1gFnSc6
<g/>
,	,	kIx,
sanskrtu	sanskrt	k1gInSc6
a	a	k8xC
sumerštině	sumerština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dobu	doba	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
studia	studio	k1gNnSc2
zvládl	zvládnout	k5eAaPmAgInS
celkem	celkem	k6eAd1
deset	deset	k4xCc4
orientálních	orientální	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
získal	získat	k5eAaPmAgInS
doktorát	doktorát	k1gInSc4
a	a	k8xC
stipendium	stipendium	k1gNnSc4
na	na	k7c6
Berlínské	berlínský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
spolu	spolu	k6eAd1
s	s	k7c7
Oxfordskou	oxfordský	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
tehdy	tehdy	k6eAd1
představovala	představovat	k5eAaImAgFnS
špičkové	špičkový	k2eAgNnSc4d1
badatelské	badatelský	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
orientalistiky	orientalistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžištěm	těžiště	k1gNnSc7
jeho	jeho	k3xOp3gFnSc2
vědecké	vědecký	k2eAgFnSc2d1
práce	práce	k1gFnSc2
byl	být	k5eAaImAgInS
klínopis	klínopis	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
studium	studium	k1gNnSc1
sumerských	sumerský	k2eAgInPc2d1
a	a	k8xC
akkadských	akkadský	k2eAgInPc2d1
textů	text	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
odjel	odjet	k5eAaPmAgInS
s	s	k7c7
prof.	prof.	kA
Ernstem	Ernst	k1gMnSc7
Sellinem	Sellin	k1gInSc7
do	do	k7c2
Turecka	Turecko	k1gNnSc2
<g/>
,	,	kIx,
Sýrie	Sýrie	k1gFnSc2
<g/>
,	,	kIx,
Palestiny	Palestina	k1gFnSc2
a	a	k8xC
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
na	na	k7c4
vydávání	vydávání	k1gNnSc4
klínopisných	klínopisný	k2eAgInPc2d1
textů	text	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
Univerzitní	univerzitní	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
roku	rok	k1gInSc2
1909	#num#	k4
získal	získat	k5eAaPmAgMnS
definitivu	definitiva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
se	se	k3xPyFc4
i	i	k9
oženil	oženit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
při	při	k7c6
pobytu	pobyt	k1gInSc6
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
(	(	kIx(
<g/>
asi	asi	k9
150	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
Ankary	Ankara	k1gFnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německou	německý	k2eAgFnSc7d1
expedicí	expedice	k1gFnSc7
objeven	objeven	k2eAgInSc1d1
velký	velký	k2eAgInSc1d1
archív	archív	k1gInSc1
chetitských	chetitský	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
obsahoval	obsahovat	k5eAaImAgInS
množství	množství	k1gNnSc4
hliněných	hliněný	k2eAgFnPc2d1
tabulek	tabulka	k1gFnPc2
popsaných	popsaný	k2eAgInPc2d1
již	již	k6eAd1
známým	známý	k2eAgNnSc7d1
klínovým	klínový	k2eAgNnSc7d1
písmem	písmo	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
neznámou	známý	k2eNgFnSc7d1
řečí	řeč	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vzdorovala	vzdorovat	k5eAaImAgFnS
mnoha	mnoho	k4c2
badatelům	badatel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
sice	sice	k8xC
odveden	odveden	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
sloužil	sloužit	k5eAaImAgMnS
jako	jako	k9
písař	písař	k1gMnSc1
štábu	štáb	k1gInSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
předložil	předložit	k5eAaPmAgInS
rozluštění	rozluštění	k1gNnSc4
tohoto	tento	k3xDgInSc2
problému	problém	k1gInSc2
současně	současně	k6eAd1
s	s	k7c7
prvním	první	k4xOgInSc7
stručným	stručný	k2eAgInSc7d1
nárysem	nárys	k1gInSc7
mluvnice	mluvnice	k1gFnSc2
tohoto	tento	k3xDgInSc2
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgNnSc7
byl	být	k5eAaImAgInS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
jazyk	jazyk	k1gInSc1
chetitský	chetitský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
také	také	k9
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
chetitština	chetitština	k1gFnSc1
patří	patřit	k5eAaImIp3nS
ke	k	k7c3
skupině	skupina	k1gFnSc3
tzv.	tzv.	kA
indoevropských	indoevropský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
tedy	tedy	k9
příbuzná	příbuzný	k2eAgFnSc1d1
s	s	k7c7
řečtinou	řečtina	k1gFnSc7
<g/>
,	,	kIx,
latinou	latina	k1gFnSc7
<g/>
,	,	kIx,
indickými	indický	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
apod.	apod.	kA
Výsledky	výsledek	k1gInPc1
svého	svůj	k3xOyFgNnSc2
bádání	bádání	k1gNnSc2
publikoval	publikovat	k5eAaBmAgMnS
roku	rok	k1gInSc2
1917	#num#	k4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
stěžejní	stěžejní	k2eAgFnSc6d1
práci	práce	k1gFnSc6
Die	Die	k1gFnSc2
Sprache	Sprach	k1gFnSc2
der	drát	k5eAaImRp2nS
Hethiter	Hethiter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
v	v	k7c6
Praze	Praha	k1gFnSc6
s	s	k7c7
Vlastou	Vlasta	k1gFnSc7
Miladou	Milada	k1gFnSc7
Procházkovou	Procházková	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
měli	mít	k5eAaImAgMnP
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
dcery	dcera	k1gFnPc4
Olgu	Olga	k1gFnSc4
(	(	kIx(
<g/>
*	*	kIx~
19	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1914	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Helenu	Helena	k1gFnSc4
(	(	kIx(
<g/>
*	*	kIx~
7	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1917	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
profesorem	profesor	k1gMnSc7
klínopisu	klínopis	k1gInSc2
a	a	k8xC
dějin	dějiny	k1gFnPc2
starého	starý	k2eAgInSc2d1
Orientu	Orient	k1gInSc2
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1924	#num#	k4
získal	získat	k5eAaPmAgInS
peníze	peníz	k1gInPc4
na	na	k7c4
první	první	k4xOgFnSc4
českou	český	k2eAgFnSc4d1
expedici	expedice	k1gFnSc4
do	do	k7c2
Šech	šech	k1gMnSc1
Sadu	sada	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
dubnu	duben	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
začaly	začít	k5eAaPmAgInP
výkopy	výkop	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
kopal	kopat	k5eAaImAgMnS
na	na	k7c6
pahorku	pahorek	k1gInSc6
Tell	Tella	k1gFnPc2
Erfád	Erfáda	k1gFnPc2
(	(	kIx(
<g/>
severní	severní	k2eAgFnSc1d1
Sýrie	Sýrie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
vykopal	vykopat	k5eAaPmAgMnS
pozůstatky	pozůstatek	k1gInPc7
řeckých	řecký	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
keramiky	keramika	k1gFnSc2
<g/>
,	,	kIx,
terakotových	terakotový	k2eAgFnPc2d1
sošek	soška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
na	na	k7c6
další	další	k2eAgFnSc6d1
expedici	expedice	k1gFnSc6
(	(	kIx(
<g/>
Kaisarií	Kaisarie	k1gFnPc2
na	na	k7c6
kopci	kopec	k1gInSc6
Kültepe	Kültep	k1gInSc5
<g/>
)	)	kIx)
podařilo	podařit	k5eAaPmAgNnS
při	při	k7c6
vykopávkách	vykopávka	k1gFnPc6
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
objevit	objevit	k5eAaPmF
přes	přes	k7c4
1000	#num#	k4
hliněných	hliněný	k2eAgFnPc2d1
tabulek	tabulka	k1gFnPc2
<g/>
,	,	kIx,
popsaných	popsaný	k2eAgInPc2d1
klínovým	klínový	k2eAgNnSc7d1
písmem	písmo	k1gNnSc7
a	a	k8xC
obsahujících	obsahující	k2eAgFnPc6d1
smlouvy	smlouva	k1gFnPc1
a	a	k8xC
dopisy	dopis	k1gInPc1
asyrských	asyrský	k2eAgMnPc2d1
kupců	kupec	k1gMnPc2
z	z	k7c2
druhého	druhý	k4xOgNnSc2
tisíciletí	tisíciletí	k1gNnSc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
obchodní	obchodní	k2eAgInSc1d1
archiv	archiv	k1gInSc1
marně	marně	k6eAd1
hledaly	hledat	k5eAaImAgFnP
desítky	desítka	k1gFnPc1
vědců	vědec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potvrdilo	potvrdit	k5eAaPmAgNnS
se	s	k7c7
mj.	mj.	kA
i	i	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
Hrozný	hrozný	k2eAgMnSc1d1
zjistil	zjistit	k5eAaPmAgInS
již	již	k6eAd1
při	při	k7c6
studiu	studio	k1gNnSc6
klínových	klínový	k2eAgInPc2d1
textů	text	k1gInPc2
sumersko-babylonských	sumersko-babylonský	k2eAgInPc2d1
<g/>
:	:	kIx,
Už	už	k6eAd1
staří	starý	k2eAgMnPc1d1
Sumerové	Sumer	k1gMnPc1
3	#num#	k4
tisíce	tisíc	k4xCgInPc4
let	léto	k1gNnPc2
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
znali	znát	k5eAaImAgMnP
slad	slad	k1gInSc4
a	a	k8xC
uměli	umět	k5eAaImAgMnP
vařit	vařit	k5eAaImF
pivo	pivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
podle	podle	k7c2
celé	celý	k2eAgFnSc2d1
řady	řada	k1gFnSc2
receptů	recept	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
zajímavé	zajímavý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
jelikož	jelikož	k8xS
chtěl	chtít	k5eAaImAgMnS
kopat	kopat	k5eAaImF
i	i	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
kopce	kopec	k1gInSc2
Kültepe	Kültep	k1gInSc5
<g/>
,	,	kIx,
musel	muset	k5eAaImAgInS
odkoupit	odkoupit	k5eAaPmF
přilehlou	přilehlý	k2eAgFnSc4d1
louku	louka	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
majitele	majitel	k1gMnPc4
této	tento	k3xDgFnSc2
louky	louka	k1gFnSc2
nechal	nechat	k5eAaPmAgInS
zapsat	zapsat	k5eAaPmF
Československý	československý	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
těchto	tento	k3xDgFnPc6
vykopávkách	vykopávka	k1gFnPc6
bylo	být	k5eAaImAgNnS
nalezeno	naleznout	k5eAaPmNgNnS,k5eAaBmNgNnS
chetitské	chetitský	k2eAgNnSc1d1
město	město	k1gNnSc1
Kaneš	kanout	k5eAaImIp2nS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
1925	#num#	k4
se	se	k3xPyFc4
musel	muset	k5eAaImAgInS
vrátit	vrátit	k5eAaPmF
zpět	zpět	k6eAd1
do	do	k7c2
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
založil	založit	k5eAaPmAgInS
mezinárodní	mezinárodní	k2eAgInSc1d1
vědecký	vědecký	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Archiv	archiv	k1gInSc1
Orientální	orientální	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přednášel	přednášet	k5eAaImAgMnS
na	na	k7c6
řadě	řada	k1gFnSc6
univerzit	univerzita	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc1
přednášky	přednáška	k1gFnPc1
byly	být	k5eAaImAgFnP
velice	velice	k6eAd1
úspěšné	úspěšný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1939	#num#	k4
měl	mít	k5eAaImAgMnS
možnost	možnost	k1gFnSc4
emigrovat	emigrovat	k5eAaBmF
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc4
odmítl	odmítnout	k5eAaPmAgInS
<g/>
;	;	kIx,
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
rektorem	rektor	k1gMnSc7
Karlovy	Karlův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1940	#num#	k4
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
nabídnuto	nabídnout	k5eAaPmNgNnS
místo	místo	k1gNnSc1
na	na	k7c6
ministerstvu	ministerstvo	k1gNnSc6
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
také	také	k9
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
byl	být	k5eAaImAgInS
stižen	stižen	k2eAgInSc1d1
záchvatem	záchvat	k1gInSc7
mrtvice	mrtvice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vědecké	vědecký	k2eAgFnSc3d1
práci	práce	k1gFnSc3
se	se	k3xPyFc4
už	už	k6eAd1
nevrátil	vrátit	k5eNaPmAgMnS
ani	ani	k8xC
po	po	k7c6
osvobození	osvobození	k1gNnSc6
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
Státní	státní	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
byl	být	k5eAaImAgInS
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
členem	člen	k1gInSc7
nově	nově	k6eAd1
založené	založený	k2eAgFnSc2d1
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
Lysé	Lysá	k1gFnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
na	na	k7c6
evangelickém	evangelický	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
zrušení	zrušení	k1gNnSc6
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
byly	být	k5eAaImAgInP
jeho	jeho	k3xOp3gInPc1
ostatky	ostatek	k1gInPc1
přeneseny	přenesen	k2eAgInPc1d1
na	na	k7c4
obecní	obecní	k2eAgInSc4d1
hřbitov	hřbitov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
původní	původní	k2eAgInSc1d1
náhrobní	náhrobní	k2eAgInSc1d1
obelisk	obelisk	k1gInSc1
nyní	nyní	k6eAd1
stojí	stát	k5eAaImIp3nS
v	v	k7c6
areálu	areál	k1gInSc6
Evangelického	evangelický	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Expozice	expozice	k1gFnSc1
</s>
<s>
V	v	k7c6
jeho	jeho	k3xOp3gNnSc6
rodišti	rodiště	k1gNnSc6
Lysé	Lysá	k1gFnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc2
1951	#num#	k4
zřízeno	zřídit	k5eAaPmNgNnS
Muzeum	muzeum	k1gNnSc1
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc2d1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
dnes	dnes	k6eAd1
stálá	stálý	k2eAgFnSc1d1
expozice	expozice	k1gFnSc1
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc2d1
s	s	k7c7
mnoha	mnoho	k4c7
vzácnými	vzácný	k2eAgInPc7d1
a	a	k8xC
zajímavými	zajímavý	k2eAgInPc7d1
sbírkovými	sbírkový	k2eAgInPc7d1
předměty	předmět	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
biografiím	biografie	k1gFnPc3
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc4d1
</s>
<s>
Velhartická	Velhartický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Šárka	Šárka	k1gFnSc1
<g/>
:	:	kIx,
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
a	a	k8xC
100	#num#	k4
let	léto	k1gNnPc2
chetitologie	chetitologie	k1gFnSc2
/	/	kIx~
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
and	and	k?
100	#num#	k4
Years	Years	k1gInSc1
of	of	k?
Hittitology	Hittitolog	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Velhartická	Velhartický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Šárka	Šárka	k1gFnSc1
<g/>
:	:	kIx,
Dopisy	dopis	k1gInPc1
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc2d1
literárním	literární	k2eAgFnPc3d1
osobnostem	osobnost	k1gFnPc3
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Památník	památník	k1gInSc1
národního	národní	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Velhartická	Velhartický	k2eAgFnSc1d1
<g/>
,	,	kIx,
Šárka	Šárka	k1gFnSc1
<g/>
:	:	kIx,
Justin	Justin	k1gMnSc1
Václav	Václav	k1gMnSc1
Prášek	Prášek	k1gMnSc1
a	a	k8xC
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc7
české	český	k2eAgFnSc2d1
staroorientalistiky	staroorientalistika	k1gFnSc2
a	a	k8xC
klínopisného	klínopisný	k2eAgNnSc2d1
bádání	bádání	k1gNnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
–	–	k?
<g/>
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vavroušek	Vavroušek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
:	:	kIx,
Pán	pán	k1gMnSc1
chetitských	chetitský	k2eAgFnPc2d1
tabulek	tabulka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Universita	universita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Matouš	Matouš	k1gMnSc1
<g/>
,	,	kIx,
Lubor	Lubor	k1gMnSc1
<g/>
:	:	kIx,
Stopami	stopa	k1gFnPc7
zašlých	zašlý	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Melantrich	Melantrich	k1gInSc1
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ranoszek	Ranoszek	k1gInSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Akademik	akademik	k1gMnSc1
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Przegląd	Przegląd	k1gInSc1
Orientalistyczny	Orientalistyczna	k1gFnSc2
3	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Klíma	Klíma	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
In	In	k1gMnSc1
memoriam	memoriam	k6eAd1
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Juristic	Juristice	k1gFnPc2
Papyrology	Papyrolog	k1gMnPc4
VII	VII	kA
<g/>
/	/	kIx~
<g/>
VIII	VIII	kA
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
David	David	k1gMnSc1
<g/>
,	,	kIx,
Madeleine	Madeleine	k1gFnSc1
<g/>
:	:	kIx,
La	la	k1gNnSc1
vie	vie	k?
et	et	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
oeuvre	oeuvr	k1gInSc5
de	de	k?
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revue	revue	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Assyriologie	Assyriologie	k1gFnSc1
47	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zamarovský	Zamarovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
:	:	kIx,
Za	za	k7c7
tajemstvím	tajemství	k1gNnSc7
říše	říš	k1gFnSc2
Chetitů	Chetit	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
vědecká	vědecký	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
byla	být	k5eAaImAgFnS
neobyčejně	obyčejně	k6eNd1
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
a	a	k8xC
všestranná	všestranný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
první	první	k4xOgFnSc1
vědecká	vědecký	k2eAgFnSc1d1
práce	práce	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
a	a	k8xC
celý	celý	k2eAgInSc1d1
seznam	seznam	k1gInSc1
jeho	jeho	k3xOp3gFnPc2
vědeckých	vědecký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
obsahuje	obsahovat	k5eAaImIp3nS
přes	přes	k7c4
200	#num#	k4
titulů	titul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
pracích	práce	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
psal	psát	k5eAaImAgMnS
česky	česky	k6eAd1
<g/>
,	,	kIx,
německy	německy	k6eAd1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
a	a	k8xC
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
především	především	k9
Malé	Malé	k2eAgFnSc3d1
Asii	Asie	k1gFnSc3
a	a	k8xC
starověké	starověký	k2eAgFnSc3d1
Mezopotámii	Mezopotámie	k1gFnSc3
<g/>
,	,	kIx,
zabýval	zabývat	k5eAaImAgMnS
se	se	k3xPyFc4
však	však	k9
i	i	k9
starověkým	starověký	k2eAgInSc7d1
Egyptem	Egypt	k1gInSc7
<g/>
,	,	kIx,
Krétou	Kréta	k1gFnSc7
a	a	k8xC
Indií	Indie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gFnSc1
práce	práce	k1gFnSc1
v	v	k7c6
oboru	obor	k1gInSc6
klínopis	klínopis	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
sumerská	sumerský	k2eAgFnSc1d1
a	a	k8xC
akkadská	akkadský	k2eAgFnSc1d1
filologie	filologie	k1gFnSc1
a	a	k8xC
později	pozdě	k6eAd2
chetitologie	chetitologie	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
a	a	k8xC
je	být	k5eAaImIp3nS
mezinárodně	mezinárodně	k6eAd1
uznávaná	uznávaný	k2eAgFnSc1d1
a	a	k8xC
přijímaná	přijímaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
jeho	jeho	k3xOp3gInPc1
pozdější	pozdní	k2eAgInPc1d2
pokusy	pokus	k1gInPc1
o	o	k7c4
rozluštění	rozluštění	k1gNnSc4
celé	celý	k2eAgFnSc2d1
řady	řada	k1gFnSc2
jiných	jiný	k2eAgNnPc2d1
písem	písmo	k1gNnPc2
či	či	k8xC
jeho	jeho	k3xOp3gFnPc1
alternativní	alternativní	k2eAgFnPc1d1
Dějiny	dějiny	k1gFnPc1
Přední	přední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
odbornou	odborný	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
odmítnuty	odmítnut	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
před	před	k7c7
svou	svůj	k3xOyFgFnSc7
smrtí	smrt	k1gFnSc7
se	se	k3xPyFc4
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
stal	stát	k5eAaPmAgInS
v	v	k7c6
Československu	Československo	k1gNnSc6
předmětem	předmět	k1gInSc7
značné	značný	k2eAgFnSc2d1
glorifikace	glorifikace	k1gFnSc2
a	a	k8xC
mystifikace	mystifikace	k1gFnSc2
<g/>
,	,	kIx,
počínaje	počínaje	k7c7
Stopami	stopa	k1gFnPc7
zašlých	zašlý	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
Lubora	Lubor	k1gMnSc2
Matouše	Matouš	k1gMnSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
až	až	k9
po	po	k7c4
naprosto	naprosto	k6eAd1
nekritické	kritický	k2eNgFnPc4d1
popularizační	popularizační	k2eAgFnPc4d1
práce	práce	k1gFnPc4
René	René	k1gFnSc1
Kopeckého	Kopeckého	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3
práce	práce	k1gFnPc1
</s>
<s>
Zum	Zum	k?
Geldwesen	Geldwesen	k2eAgMnSc1d1
der	drát	k5eAaImRp2nS
Babylonier	Babylonier	k1gInSc1
<g/>
,	,	kIx,
Beiträge	Beiträge	k1gInSc1
zur	zur	k?
Assyriologie	Assyriologie	k1gFnSc2
IV	IV	kA
<g/>
,	,	kIx,
1902	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sumerisch-babylonische	Sumerisch-babylonische	k6eAd1
Mythen	Mythen	k2eAgInSc1d1
von	von	k1gInSc1
dem	dem	k?
Gotte	Gott	k1gInSc5
Ninrag	Ninrag	k1gMnSc1
(	(	kIx(
<g/>
Ninib	Ninib	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Wien	Wien	k1gInSc1
1903	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Das	Das	k?
Getreide	Getreid	k1gMnSc5
im	im	k?
alten	alten	k2eAgInSc1d1
Babylonien	Babylonien	k1gInSc1
<g/>
,	,	kIx,
Wien	Wien	k1gInSc1
1914	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Die	Die	k?
Sprache	Sprache	k1gInSc1
der	drát	k5eAaImRp2nS
Hethiter	Hethiter	k1gMnSc1
<g/>
,	,	kIx,
ihr	ihr	k?
Bau	Bau	k1gFnSc1
und	und	k?
ihre	ihrat	k5eAaPmIp3nS
Zugehörigkeit	Zugehörigkeit	k2eAgMnSc1d1
zum	zum	k?
indogermanischen	indogermanischen	k2eAgInSc1d1
Sprachstamm	Sprachstamm	k1gInSc1
<g/>
,	,	kIx,
Leipzig	Leipzig	k1gInSc1
1916	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Code	Code	k6eAd1
hittite	hittit	k1gInSc5
provenant	provenant	k1gInSc4
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Asie	Asie	k1gFnSc2
Mineure	Mineur	k1gMnSc5
(	(	kIx(
<g/>
vers	versa	k1gFnPc2
1350	#num#	k4
av	av	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
<g/>
-	-	kIx~
<g/>
C.	C.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Paris	Paris	k1gMnSc1
1922	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Les	les	k1gInSc4
inscriptions	inscriptions	k6eAd1
hittites	hittites	k1gMnSc1
hiéroglyphiques	hiéroglyphiques	k1gMnSc1
<g/>
,	,	kIx,
essai	essai	k6eAd1
de	de	k?
déchiffrement	déchiffrement	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1933	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3
dějiny	dějiny	k1gFnPc1
Přední	přední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc2
a	a	k8xC
Kréty	Kréta	k1gFnSc2
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Melantrich	Melantrich	k1gInSc1
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
Hrozného	hrozný	k2eAgInSc2d1
autobiografií	autobiografie	k1gFnSc7
</s>
<s>
V	v	k7c6
říši	říš	k1gFnSc6
půlměsíce	půlměsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesty	cesta	k1gFnPc1
a	a	k8xC
výkopy	výkop	k1gInPc1
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
S	s	k7c7
29	#num#	k4
původními	původní	k2eAgInPc7d1
snímky	snímek	k1gInPc7
<g/>
,	,	kIx,
1	#num#	k4
plánem	plán	k1gInSc7
a	a	k8xC
1	#num#	k4
mapou	mapa	k1gFnSc7
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
J.	J.	kA
R.	R.	kA
Vilímek	Vilímek	k1gMnSc1
1927	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rapport	Rapport	k1gInSc1
préliminaire	préliminair	k1gInSc5
sur	sur	k?
les	les	k1gInSc1
fouilles	fouilles	k1gMnSc1
tschécoslovaques	tschécoslovaques	k1gMnSc1
du	du	k?
Kultépé	Kultépý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syria	Syria	k1gFnSc1
VIII	VIII	kA
<g/>
,	,	kIx,
1927	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1
životopis	životopis	k1gInSc1
v	v	k7c6
kostce	kostka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venkov	venkov	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
pěti	pět	k4xCc6
sovětských	sovětský	k2eAgFnPc6d1
republikách	republika	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
–	–	k?
Moskva	Moskva	k1gFnSc1
II	II	kA
<g/>
,	,	kIx,
1937	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k8xS,k8xC
profesor	profesor	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
luští	luštit	k5eAaImIp3nS
staroorientální	staroorientální	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
Orient	Orient	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
mých	můj	k3xOp1gMnPc2
vědeckých	vědecký	k2eAgMnPc2d1
objevů	objev	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Melantrich	Melantrich	k1gInSc1
<g/>
,	,	kIx,
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fotografie	fotografia	k1gFnPc1
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
nu	nu	k9
NINDA-an	NINDA-an	k1gMnSc1
ē	ē	k2eAgMnPc1d1
<g/>
,	,	kIx,
wā	wā	k1gFnSc1
ekuteni	ekuten	k2eAgMnPc1d1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
chléb	chléb	k1gInSc1
budete	být	k5eAaImBp2nP
jísti	jíst	k5eAaImF
<g/>
,	,	kIx,
vodu	voda	k1gFnSc4
budete	být	k5eAaImBp2nP
píti	pít	k5eAaImF
<g/>
"	"	kIx"
<g/>
)	)	kIx)
–	–	k?
první	první	k4xOgFnSc1
chetitská	chetitský	k2eAgFnSc1d1
věta	věta	k1gFnSc1
rozluštěná	rozluštěný	k2eAgFnSc1d1
B.	B.	kA
Hrozným	hrozný	k2eAgInSc7d1
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
žák	žák	k1gMnSc1
Josef	Josef	k1gMnSc1
Klíma	Klíma	k1gMnSc1
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
Památník	památník	k1gInSc1
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc2d1
v	v	k7c6
Lysé	Lysá	k1gFnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Hrob	hrob	k1gInSc1
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc2d1
v	v	k7c6
Lysé	Lysá	k1gFnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Rodný	rodný	k2eAgInSc1d1
dům	dům	k1gInSc1
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc2d1
v	v	k7c6
Lysé	Lysá	k1gFnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Muzeum	muzeum	k1gNnSc1
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc2d1
po	po	k7c6
rekonstrukci	rekonstrukce	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Alena	Alena	k1gFnSc1
Plháková	Plháková	k1gFnSc1
<g/>
,	,	kIx,
Olga	Olga	k1gFnSc1
Pechová	Pechová	k1gFnSc1
<g/>
:	:	kIx,
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
Vladimíra	Vladimír	k1gMnSc2
Tardyho	Tardy	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2045	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
<g/>
1	#num#	k4
2	#num#	k4
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.libri.cz	www.libri.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
http://www.ff.cuni.cz/fakulta/o-fakulte/historie-fakulty/prehled-dekanu-ff-uk/hrozny/	http://www.ff.cuni.cz/fakulta/o-fakulte/historie-fakulty/prehled-dekanu-ff-uk/hrozny/	k?
<g/>
↑	↑	k?
http://www.cuni.cz/UK-2367.html	http://www.cuni.cz/UK-2367.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.facebook.com/FFabula/photos/a.613433355345466.1073741837.118230671532406/654656027889865/?type=1&	https://www.facebook.com/FFabula/photos/a.613433355345466.1073741837.118230671532406/654656027889865/?type=1&	k1gMnSc1
<g/>
↑	↑	k?
HNÍZDIL	Hnízdil	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc7
rod	rod	k1gInSc1
<g/>
,	,	kIx,
život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genealogické	genealogický	k2eAgInPc1d1
a	a	k8xC
heraldické	heraldický	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
XXXIV	XXXIV	kA
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
128	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MATOUŠ	Matouš	k1gMnSc1
<g/>
,	,	kIx,
Lubor	Lubor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopami	stopa	k1gFnPc7
zašlých	zašlý	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
práci	práce	k1gFnSc6
profesora	profesor	k1gMnSc2
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc2d1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1949	#num#	k4
<g/>
.	.	kIx.
54	#num#	k4
s.	s.	k?
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
René	René	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
<g/>
:	:	kIx,
život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Dar	dar	k1gInSc1
ibn	ibn	k?
Rushd	Rushd	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
152	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86149	#num#	k4
<g/>
-	-	kIx~
<g/>
74	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Čihař	čihař	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibliographie	Bibliographie	k1gFnSc1
des	des	k1gNnSc2
travaux	travaux	k1gInSc1
de	de	k?
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archiv	archiv	k1gInSc1
Orientální	orientální	k2eAgFnSc2d1
XVII	XVII	kA
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1949	#num#	k4
(	(	kIx(
<g/>
=	=	kIx~
Symbolae	Symbolae	k1gInSc1
Hrozný	hrozný	k2eAgInSc1d1
<g/>
,	,	kIx,
Pars	Pars	k1gInSc1
prima	prima	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doplněno	doplnit	k5eAaPmNgNnS
v	v	k7c4
Bibliografii	bibliografie	k1gFnSc4
č.	č.	k?
6	#num#	k4
PhDr.	PhDr.	kA
Jiřím	Jiří	k1gMnSc7
Proseckým	Prosecký	k2eAgNnSc7d1
s	s	k7c7
úvodní	úvodní	k2eAgFnSc7d1
životopisnou	životopisný	k2eAgFnSc7d1
poznámkou	poznámka	k1gFnSc7
o	o	k7c6
Bedřichu	Bedřich	k1gMnSc6
Hrozném	hrozný	k2eAgMnSc6d1
od	od	k7c2
doc.	doc.	kA
dr	dr	kA
<g/>
.	.	kIx.
Vladimíra	Vladimír	k1gMnSc2
Součka	Souček	k1gMnSc2
<g/>
,	,	kIx,
vydané	vydaný	k2eAgInPc4d1
OÚ	OÚ	kA
ČSAV	ČSAV	kA
<g/>
.	.	kIx.
</s>
<s>
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
René	René	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
-	-	kIx~
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Dar	dar	k1gInSc1
ibn	ibn	k?
Rushd	Rushd	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
156	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86149	#num#	k4
<g/>
-	-	kIx~
<g/>
74	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KUTNAR	KUTNAR	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
;	;	kIx,
MAREK	Marek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehledné	přehledný	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
českého	český	k2eAgMnSc2d1
a	a	k8xC
slovenského	slovenský	k2eAgNnSc2d1
dějepisectví	dějepisectví	k1gNnSc2
:	:	kIx,
od	od	k7c2
počátků	počátek	k1gInPc2
národní	národní	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
až	až	k9
do	do	k7c2
sklonku	sklonek	k1gInSc2
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
1065	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
252	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
-	-	kIx~
Česko	Česko	k1gNnSc1
:	:	kIx,
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ottovo	Ottův	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
823	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7360	#num#	k4
<g/>
-	-	kIx~
<g/>
796	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
246	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TOMEŠ	Tomeš	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnPc2
:	:	kIx,
I.	I.	kA
díl	díl	k1gInSc1
:	:	kIx,
A	a	k9
<g/>
–	–	k?
<g/>
J.	J.	kA
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
;	;	kIx,
Petr	Petr	k1gMnSc1
Meissner	Meissner	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
634	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
245	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
521	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VELHARTICKÁ	VELHARTICKÁ	kA
<g/>
,	,	kIx,
Šárka	Šárka	k1gFnSc1
<g/>
:	:	kIx,
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
a	a	k8xC
100	#num#	k4
let	léto	k1gNnPc2
chetitologie	chetitologie	k1gFnSc2
/	/	kIx~
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
and	and	k?
100	#num#	k4
Years	Years	k1gInSc1
of	of	k?
Hittitology	Hittitolog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VELHARTICKÁ	VELHARTICKÁ	kA
<g/>
,	,	kIx,
Šárka	Šárka	k1gFnSc1
<g/>
:	:	kIx,
Dopisy	dopis	k1gInPc1
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgMnSc2d1
literárním	literární	k2eAgFnPc3d1
osobnostem	osobnost	k1gFnPc3
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Památník	památník	k1gInSc1
národního	národní	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VELHARTICKÁ	VELHARTICKÁ	kA
<g/>
,	,	kIx,
Šárka	Šárka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Justin	Justin	k1gMnSc1
Václav	Václav	k1gMnSc1
Prášek	Prášek	k1gMnSc1
a	a	k8xC
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
<g/>
:	:	kIx,
Počátky	počátek	k1gInPc1
české	český	k2eAgFnSc2d1
staroorientalistiky	staroorientalistika	k1gFnSc2
a	a	k8xC
klínopisného	klínopisný	k2eAgNnSc2d1
bádání	bádání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-200-2938-6	978-80-200-2938-6	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
</s>
<s>
Domovská	domovský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Muzea	muzeum	k1gNnSc2
Bedřicha	Bedřich	k1gMnSc2
Hrozného	hrozný	k2eAgInSc2d1
</s>
<s>
KDO	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
BYL	být	k5eAaImAgMnS
KDO	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
v	v	k7c6
našich	náš	k3xOp1gFnPc6
dějinách	dějiny	k1gFnPc6
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c6
Hrozném	hrozný	k2eAgInSc6d1
na	na	k7c6
serveru	server	k1gInSc6
Nymburk-info	Nymburk-info	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
zmizelý	zmizelý	k2eAgMnSc1d1
při	při	k7c6
jeho	jeho	k3xOp3gFnSc6
reorganizaci	reorganizace	k1gFnSc6
<g/>
,	,	kIx,
kopie	kopie	k1gFnSc1
z	z	k7c2
Web	web	k1gInSc4
Archive	archiv	k1gInSc5
</s>
<s>
„	„	k?
<g/>
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
–	–	k?
fakta	faktum	k1gNnPc4
a	a	k8xC
fotografie	fotografia	k1gFnPc4
<g/>
“	“	k?
–	–	k?
článek	článek	k1gInSc1
převzatý	převzatý	k2eAgInSc1d1
patrně	patrně	k6eAd1
z	z	k7c2
nějakého	nějaký	k3yIgInSc2
staršího	starý	k2eAgInSc2d2
zdroje	zdroj	k1gInSc2
</s>
<s>
Dvě	dva	k4xCgFnPc1
fotografie	fotografia	k1gFnPc1
</s>
<s>
Další	další	k2eAgInPc1d1
obrázky	obrázek	k1gInPc1
</s>
<s>
Jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
se	s	k7c7
slavným	slavný	k2eAgInSc7d1
vědeckým	vědecký	k2eAgInSc7d1
objevem	objev	k1gInSc7
českého	český	k2eAgMnSc2d1
orientalisty	orientalista	k1gMnSc2
doopravdy	doopravdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
Prosecký	Prosecký	k2eAgMnSc1d1
k	k	k7c3
omylům	omyl	k1gInPc3
v	v	k7c6
článcích	článek	k1gInPc6
o	o	k7c6
Bedřichu	Bedřich	k1gMnSc6
Hrozném	hrozný	k2eAgInSc6d1
v	v	k7c6
časopise	časopis	k1gInSc6
Vesmír	vesmír	k1gInSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
a	a	k8xC
Chetité	Chetita	k1gMnPc1
Archivováno	archivován	k2eAgNnSc4d1
30	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Z	z	k7c2
archivu	archiv	k1gInSc2
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
<g/>
,	,	kIx,
pořad	pořad	k1gInSc1
Planetárium	planetárium	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rektoři	rektor	k1gMnPc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
1882	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
(	(	kIx(
<g/>
česká	český	k2eAgFnSc1d1
Karlo-Ferdinandova	Karlo-Ferdinandův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1882	#num#	k4
<g/>
–	–	k?
<g/>
1883	#num#	k4
<g/>
:	:	kIx,
Václav	Václava	k1gFnPc2
Vladivoj	Vladivoj	k1gInSc1
Tomek	Tomek	k1gMnSc1
•	•	k?
1883	#num#	k4
<g/>
–	–	k?
<g/>
1884	#num#	k4
<g/>
:	:	kIx,
Antonín	Antonín	k1gMnSc1
Randa	Randa	k1gMnSc1
•	•	k?
1884	#num#	k4
<g/>
–	–	k?
<g/>
1885	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Streng	Strenga	k1gFnPc2
•	•	k?
1885	#num#	k4
<g/>
–	–	k?
<g/>
1886	#num#	k4
<g/>
:	:	kIx,
Václav	Václava	k1gFnPc2
Vladivoj	Vladivoj	k1gInSc1
Tomek	Tomek	k1gMnSc1
•	•	k?
1886	#num#	k4
<g/>
–	–	k?
<g/>
1887	#num#	k4
<g/>
:	:	kIx,
Emil	Emil	k1gMnSc1
Ott	Ott	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1887	#num#	k4
<g/>
–	–	k?
<g/>
1888	#num#	k4
<g/>
:	:	kIx,
Vilém	Vilém	k1gMnSc1
Weiss	Weiss	k1gMnSc1
•	•	k?
1888	#num#	k4
<g/>
–	–	k?
<g/>
1889	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Studnička	Studnička	k1gMnSc1
•	•	k?
1889	#num#	k4
<g/>
–	–	k?
<g/>
1890	#num#	k4
<g/>
:	:	kIx,
Matouš	Matouš	k1gMnSc1
Talíř	Talíř	k1gMnSc1
•	•	k?
1890	#num#	k4
<g/>
–	–	k?
<g/>
1891	#num#	k4
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Tomsa	Tomsa	k1gFnSc1
•	•	k?
1891	#num#	k4
<g/>
–	–	k?
<g/>
1892	#num#	k4
<g/>
:	:	kIx,
Antonín	Antonín	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Frič	Frič	k1gMnSc1
•	•	k?
1892	#num#	k4
<g/>
–	–	k?
<g/>
1893	#num#	k4
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Pražák	Pražák	k1gMnSc1
•	•	k?
1893	#num#	k4
<g/>
–	–	k?
<g/>
1894	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Xaver	Xaver	k1gMnSc1
Kryštůfek	Kryštůfek	k1gMnSc1
•	•	k?
1894	#num#	k4
<g/>
–	–	k?
<g/>
1895	#num#	k4
<g/>
:	:	kIx,
Arnold	Arnold	k1gMnSc1
Spina	spina	k1gFnSc1
•	•	k?
1895	#num#	k4
<g/>
–	–	k?
<g/>
1896	#num#	k4
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Vrba	Vrba	k1gMnSc1
•	•	k?
1896	#num#	k4
<g/>
–	–	k?
<g/>
1897	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Jaromír	Jaromír	k1gMnSc1
Hanel	Hanel	k1gMnSc1
•	•	k?
1897	#num#	k4
<g/>
–	–	k?
<g/>
1898	#num#	k4
<g/>
:	:	kIx,
Eugen	Eugen	k2eAgInSc1d1
Kadeřávek	kadeřávek	k1gInSc1
•	•	k?
1898	#num#	k4
<g/>
–	–	k?
<g/>
1899	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Reinsberg	Reinsberg	k1gMnSc1
•	•	k?
1889	#num#	k4
<g/>
–	–	k?
<g/>
1900	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Gebauer	Gebaura	k1gFnPc2
•	•	k?
1900	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Stupecký	Stupecký	k2eAgMnSc1d1
•	•	k?
1901	#num#	k4
<g/>
–	–	k?
<g/>
1902	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Jan	Jan	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Sýkora	Sýkora	k1gMnSc1
•	•	k?
1902	#num#	k4
<g/>
–	–	k?
<g/>
1903	#num#	k4
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
Horbaczewski	Horbaczewsk	k1gFnSc2
•	•	k?
1903	#num#	k4
<g/>
–	–	k?
<g/>
1904	#num#	k4
<g/>
:	:	kIx,
Čeněk	Čeněk	k1gMnSc1
Strouhal	Strouhal	k1gMnSc1
•	•	k?
1904	#num#	k4
<g/>
–	–	k?
<g/>
1905	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Storch	Storcha	k1gFnPc2
•	•	k?
1905	#num#	k4
<g/>
–	–	k?
<g/>
1906	#num#	k4
<g/>
:	:	kIx,
Antonín	Antonín	k1gMnSc1
Vřešťál	Vřešťála	k1gFnPc2
•	•	k?
1906	#num#	k4
<g/>
–	–	k?
<g/>
1907	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Hlava	hlava	k1gFnSc1
•	•	k?
1907	#num#	k4
<g/>
–	–	k?
<g/>
1908	#num#	k4
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Goll	Golla	k1gFnPc2
•	•	k?
1908	#num#	k4
<g/>
–	–	k?
<g/>
1909	#num#	k4
<g/>
:	:	kIx,
Leopold	Leopold	k1gMnSc1
Heyrovský	Heyrovský	k2eAgMnSc1d1
•	•	k?
1909	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Král	Král	k1gMnSc1
•	•	k?
1910	#num#	k4
<g/>
–	–	k?
<g/>
1911	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Janošík	Janošík	k1gMnSc1
•	•	k?
1911	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Jaromír	Jaromír	k1gMnSc1
Čelakovský	Čelakovský	k2eAgMnSc1d1
•	•	k?
1912	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Vejdovský	Vejdovský	k2eAgMnSc1d1
•	•	k?
1913	#num#	k4
<g/>
–	–	k?
<g/>
1914	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Mareš	Mareš	k1gMnSc1
•	•	k?
1914	#num#	k4
<g/>
–	–	k?
<g/>
1915	#num#	k4
<g/>
:	:	kIx,
Kamil	Kamil	k1gMnSc1
Henner	Hennra	k1gFnPc2
•	•	k?
1915	#num#	k4
<g/>
–	–	k?
<g/>
1916	#num#	k4
<g/>
:	:	kIx,
Rudolf	Rudolf	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
•	•	k?
1916	#num#	k4
<g/>
–	–	k?
<g/>
1917	#num#	k4
<g/>
:	:	kIx,
Vítězslav	Vítězslav	k1gMnSc1
Janovský	janovský	k2eAgMnSc1d1
•	•	k?
1917	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
:	:	kIx,
Gabriel	Gabriel	k1gMnSc1
Pecháček	Pecháček	k1gMnSc1
•	•	k?
1918	#num#	k4
<g/>
–	–	k?
<g/>
1919	#num#	k4
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Hermann-Otavský	Hermann-Otavský	k2eAgMnSc1d1
•	•	k?
1919	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Zubatý	zubatý	k2eAgMnSc1d1
od	od	k7c2
1920	#num#	k4
(	(	kIx(
<g/>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
<g/>
)	)	kIx)
</s>
<s>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Mareš	Mareš	k1gMnSc1
•	•	k?
1921	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
<g/>
:	:	kIx,
Bohumil	Bohumil	k1gMnSc1
Němec	Němec	k1gMnSc1
•	•	k?
1922	#num#	k4
<g/>
–	–	k?
<g/>
1923	#num#	k4
<g/>
:	:	kIx,
Cyril	Cyril	k1gMnSc1
Horáček	Horáček	k1gMnSc1
•	•	k?
1923	#num#	k4
<g/>
–	–	k?
<g/>
1924	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Pastrnek	Pastrnky	k1gFnPc2
•	•	k?
1924	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
:	:	kIx,
Otakar	Otakar	k1gMnSc1
Kukula	Kukula	k1gFnSc1
•	•	k?
1925	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Petr	Petr	k1gMnSc1
•	•	k?
1926	#num#	k4
<g/>
–	–	k?
<g/>
1927	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Vančura	Vančura	k1gMnSc1
•	•	k?
1927	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
:	:	kIx,
Lubor	Lubor	k1gMnSc1
Niederle	Niederle	k1gFnSc2
•	•	k?
1928	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Slavík	Slavík	k1gMnSc1
•	•	k?
1929	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
:	:	kIx,
Jindřich	Jindřich	k1gMnSc1
Matiegka	Matiegek	k1gInSc2
•	•	k?
1930	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
1931	#num#	k4
<g/>
:	:	kIx,
August	August	k1gMnSc1
Miřička	Miřička	k1gMnSc1
•	•	k?
1931	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Pekař	Pekař	k1gMnSc1
•	•	k?
1932	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
<g/>
:	:	kIx,
Rudolf	Rudolf	k1gMnSc1
Kimla	Kimla	k1gMnSc1
•	•	k?
1933	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
:	:	kIx,
Karel	Karla	k1gFnPc2
Domin	domino	k1gNnPc2
•	•	k?
1934	#num#	k4
<g/>
–	–	k?
<g/>
1935	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Drachovský	Drachovský	k2eAgMnSc1d1
•	•	k?
1935	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
:	:	kIx,
Gustav	Gustav	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
•	•	k?
1936	#num#	k4
<g/>
–	–	k?
<g/>
1937	#num#	k4
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Weigner	Weignra	k1gFnPc2
•	•	k?
1937	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
:	:	kIx,
František	František	k1gMnSc1
Slavík	Slavík	k1gMnSc1
•	•	k?
1938	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
:	:	kIx,
Vilém	Viléma	k1gFnPc2
Funk	funk	k1gInSc1
•	•	k?
1939	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
:	:	kIx,
Bedřich	Bedřich	k1gMnSc1
Hrozný	hrozný	k2eAgMnSc1d1
•	•	k?
1945	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Bělehrádek	Bělehrádka	k1gFnPc2
•	•	k?
1946	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
:	:	kIx,
Bohumil	Bohumil	k1gMnSc1
Bydžovský	Bydžovský	k1gMnSc1
•	•	k?
1947	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Engliš	Engliš	k1gMnSc1
•	•	k?
1948	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Mukařovský	Mukařovský	k1gMnSc1
•	•	k?
1954	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
<g/>
:	:	kIx,
Miroslav	Miroslava	k1gFnPc2
Katětov	Katětov	k1gInSc1
•	•	k?
1958	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Procházka	Procházka	k1gMnSc1
•	•	k?
1966	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
:	:	kIx,
Oldřich	Oldřich	k1gMnSc1
Starý	Starý	k1gMnSc1
•	•	k?
1969	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Charvát	Charvát	k1gMnSc1
•	•	k?
1970	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
:	:	kIx,
Bedřich	Bedřich	k1gMnSc1
Švestka	Švestka	k1gMnSc1
•	•	k?
1976	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
:	:	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Češka	Češka	k1gFnSc1
•	•	k?
1990	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
<g/>
:	:	kIx,
Radim	Radim	k1gMnSc1
Palouš	Palouš	k1gMnSc1
•	•	k?
1994	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Malý	Malý	k1gMnSc1
•	•	k?
2000	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
•	•	k?
2006	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Hampl	Hampl	k1gMnSc1
•	•	k?
od	od	k7c2
2014	#num#	k4
<g/>
:	:	kIx,
Tomáš	Tomáš	k1gMnSc1
Zima	Zima	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1042884	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118775162	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1051	#num#	k4
5575	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83225277	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
32130155	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83225277	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Starověk	starověk	k1gInSc1
</s>
