<s>
Kotouč	kotouč	k1gInSc1	kotouč
hovorově	hovorově	k6eAd1	hovorově
pojmenováván	pojmenováván	k2eAgInSc1d1	pojmenováván
puk	puk	k1gInSc1	puk
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
touš	touš	k1gInSc1	touš
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drobný	drobný	k2eAgInSc4d1	drobný
předmět	předmět	k1gInSc4	předmět
tvaru	tvar	k1gInSc2	tvar
velmi	velmi	k6eAd1	velmi
plochého	plochý	k2eAgInSc2d1	plochý
válce	válec	k1gInSc2	válec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
při	při	k7c6	při
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
puk	puk	k1gInSc1	puk
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
puck	puck	k6eAd1	puck
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
byly	být	k5eAaImAgInP	být
spojeny	spojen	k2eAgInPc1d1	spojen
sice	sice	k8xC	sice
s	s	k7c7	s
postupně	postupně	k6eAd1	postupně
zakulacujícími	zakulacující	k2eAgInPc7d1	zakulacující
se	s	k7c7	s
dřevěnými	dřevěný	k2eAgFnPc7d1	dřevěná
destičkami	destička	k1gFnPc7	destička
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
jinými	jiný	k2eAgFnPc7d1	jiná
náhradami	náhrada	k1gFnPc7	náhrada
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
plechovek	plechovka	k1gFnPc2	plechovka
i	i	k8xC	i
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
koňského	koňský	k2eAgInSc2d1	koňský
trusu	trus	k1gInSc2	trus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnešní	dnešní	k2eAgInPc1d1	dnešní
hokejové	hokejový	k2eAgInPc1d1	hokejový
puky	puk	k1gInPc1	puk
bývají	bývat	k5eAaImIp3nP	bývat
zhotoveny	zhotoven	k2eAgInPc1d1	zhotoven
z	z	k7c2	z
vulkanizované	vulkanizovaný	k2eAgFnSc2d1	vulkanizovaná
pryže	pryž	k1gFnSc2	pryž
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
nad	nad	k7c7	nad
150	[number]	k4	150
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
stovek	stovka	k1gFnPc2	stovka
atmosfér	atmosféra	k1gFnPc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Puk	puk	k1gInSc1	puk
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
průměr	průměr	k1gInSc1	průměr
76,2	[number]	k4	76,2
milimetrů	milimetr	k1gInPc2	milimetr
(	(	kIx(	(
<g/>
3	[number]	k4	3
palce	palec	k1gInSc2	palec
anglické	anglický	k2eAgFnSc2d1	anglická
míry	míra	k1gFnSc2	míra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výšku	výška	k1gFnSc4	výška
25,4	[number]	k4	25,4
mm	mm	kA	mm
(	(	kIx(	(
<g/>
1	[number]	k4	1
palec	palec	k1gInSc1	palec
anglické	anglický	k2eAgFnSc2d1	anglická
míry	míra	k1gFnSc2	míra
<g/>
)	)	kIx)	)
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
od	od	k7c2	od
156	[number]	k4	156
do	do	k7c2	do
170	[number]	k4	170
gramů	gram	k1gInPc2	gram
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
tyto	tento	k3xDgInPc1	tento
parametry	parametr	k1gInPc1	parametr
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mírně	mírně	k6eAd1	mírně
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
třeba	třeba	k6eAd1	třeba
pro	pro	k7c4	pro
žákovské	žákovský	k2eAgFnPc4d1	žákovská
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rozměrů	rozměr	k1gInPc2	rozměr
musí	muset	k5eAaImIp3nS	muset
puk	puk	k1gInSc1	puk
zároveň	zároveň	k6eAd1	zároveň
vyhovovat	vyhovovat	k5eAaImF	vyhovovat
celé	celý	k2eAgFnSc3d1	celá
řadě	řada	k1gFnSc3	řada
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
vlastnosti	vlastnost	k1gFnSc6	vlastnost
(	(	kIx(	(
<g/>
elastičnost	elastičnost	k1gFnSc1	elastičnost
<g/>
,	,	kIx,	,
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
,	,	kIx,	,
odrazivá	odrazivý	k2eAgFnSc1d1	odrazivá
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
utkání	utkání	k1gNnSc4	utkání
jich	on	k3xPp3gInPc2	on
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dostatek	dostatek	k1gInSc1	dostatek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnohdy	mnohdy	k6eAd1	mnohdy
několik	několik	k4yIc1	několik
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
hledišti	hlediště	k1gNnSc6	hlediště
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
rychlosti	rychlost	k1gFnSc3	rychlost
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
někdy	někdy	k6eAd1	někdy
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
vystřelený	vystřelený	k2eAgInSc1d1	vystřelený
puk	puk	k1gInSc1	puk
(	(	kIx(	(
<g/>
160	[number]	k4	160
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k9	i
smrtelně	smrtelně	k6eAd1	smrtelně
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Puky	puk	k1gInPc1	puk
si	se	k3xPyFc3	se
někdy	někdy	k6eAd1	někdy
berou	brát	k5eAaImIp3nP	brát
jako	jako	k8xC	jako
suvenýr	suvenýr	k1gInSc4	suvenýr
někteří	některý	k3yIgMnPc1	některý
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
když	když	k8xS	když
vstřelí	vstřelit	k5eAaPmIp3nS	vstřelit
nějaký	nějaký	k3yIgInSc4	nějaký
významný	významný	k2eAgInSc4d1	významný
gól	gól	k1gInSc4	gól
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběny	vyráběn	k2eAgInPc1d1	vyráběn
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
suvenýry	suvenýr	k1gInPc1	suvenýr
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
puků	puk	k1gInPc2	puk
(	(	kIx(	(
<g/>
skleněné	skleněný	k2eAgNnSc4d1	skleněné
<g/>
,	,	kIx,	,
přívěšky	přívěšek	k1gInPc4	přívěšek
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
významné	významný	k2eAgFnPc1d1	významná
akce	akce	k1gFnPc1	akce
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
IIHF	IIHF	kA	IIHF
<g/>
)	)	kIx)	)
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
turnaje	turnaj	k1gInPc4	turnaj
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
s	s	k7c7	s
puky	puk	k1gInPc7	puk
valašského	valašský	k2eAgNnSc2d1	Valašské
výrobce	výrobce	k1gMnPc4	výrobce
Gufex	Gufex	k1gInSc1	Gufex
v	v	k7c6	v
Kateřinicích	Kateřinice	k1gFnPc6	Kateřinice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nezanechávají	zanechávat	k5eNaImIp3nP	zanechávat
veskrze	veskrze	k6eAd1	veskrze
žádné	žádný	k3yNgFnPc4	žádný
stopy	stopa	k1gFnPc4	stopa
na	na	k7c6	na
mantinelech	mantinel	k1gInPc6	mantinel
a	a	k8xC	a
plexisklech	plexisklo	k1gNnPc6	plexisklo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podvodním	podvodní	k2eAgInSc6d1	podvodní
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
puk	puk	k1gInSc1	puk
podobá	podobat	k5eAaImIp3nS	podobat
tomu	ten	k3xDgNnSc3	ten
z	z	k7c2	z
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
olověné	olověný	k2eAgNnSc1d1	olověné
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
teflonu	teflon	k1gInSc2	teflon
<g/>
,	,	kIx,	,
plastu	plast	k1gInSc2	plast
nebo	nebo	k8xC	nebo
gumy	guma	k1gFnSc2	guma
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
těžký	těžký	k2eAgInSc1d1	těžký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
bazénu	bazén	k1gInSc6	bazén
potopil	potopit	k5eAaPmAgMnS	potopit
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
šetrný	šetrný	k2eAgInSc4d1	šetrný
k	k	k7c3	k
bazénovým	bazénový	k2eAgFnPc3d1	bazénová
dlaždicím	dlaždice	k1gFnPc3	dlaždice
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Neúplný	úplný	k2eNgInSc4d1	neúplný
seznam	seznam	k1gInSc4	seznam
bývalých	bývalý	k2eAgMnPc2d1	bývalý
či	či	k8xC	či
současných	současný	k2eAgMnPc2d1	současný
výrobců	výrobce	k1gMnPc2	výrobce
puků	puk	k1gInPc2	puk
<g/>
:	:	kIx,	:
In	In	k1gFnPc2	In
Glas	Glas	k1gInSc1	Glas
Co	co	k3yRnSc1	co
<g/>
.	.	kIx.	.
</s>
<s>
Sher-Wood	Sher-Wood	k1gInSc1	Sher-Wood
Gufex	Gufex	k1gInSc4	Gufex
Rubena	ruben	k2eAgNnPc4d1	ruben
Vegum	Vegum	k1gNnSc4	Vegum
Dolné	Dolné	k2eAgFnSc1d1	Dolné
Vestenice	Vestenice	k1gFnSc1	Vestenice
Converse	Converse	k1gFnSc1	Converse
HockeyShot	HockeyShot	k1gInSc4	HockeyShot
Spalding	Spalding	k1gInSc1	Spalding
Xiamen	Xiamen	k1gInSc1	Xiamen
Yin	Yin	k1gMnSc2	Yin
Hua	Hua	k1gMnSc2	Hua
Silicone	Silicon	k1gInSc5	Silicon
Rubber	Rubber	k1gInSc1	Rubber
Products	Products	k1gInSc1	Products
Co	co	k3yRnSc1	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
</s>
<s>
Xiamen	Xiamen	k2eAgInSc1d1	Xiamen
Deng	Deng	k1gInSc1	Deng
Hong	Honga	k1gFnPc2	Honga
Silica	Silic	k1gInSc2	Silic
Gel	gel	k1gInSc4	gel
Product	Product	k1gInSc1	Product
Co	co	k3yQnSc1	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
</s>
<s>
Xiamen	Xiamen	k1gInSc1	Xiamen
Ijetech	Ijet	k1gInPc6	Ijet
Industry	Industra	k1gFnSc2	Industra
&	&	k?	&
Trade	Trad	k1gInSc5	Trad
Co	co	k3yInSc4	co
<g/>
.	.	kIx.	.
</s>
<s>
Ltd	ltd	kA	ltd
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Puk	puk	k1gInSc1	puk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
puk	puk	k1gInSc1	puk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
