<s>
Kotouč	kotouč	k1gInSc1	kotouč
hovorově	hovorově	k6eAd1	hovorově
pojmenováván	pojmenováván	k2eAgInSc1d1	pojmenováván
puk	puk	k1gInSc1	puk
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
touš	touš	k1gInSc1	touš
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
drobný	drobný	k2eAgInSc4d1	drobný
předmět	předmět	k1gInSc4	předmět
tvaru	tvar	k1gInSc2	tvar
velmi	velmi	k6eAd1	velmi
plochého	plochý	k2eAgInSc2d1	plochý
válce	válec	k1gInSc2	válec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
při	při	k7c6	při
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
