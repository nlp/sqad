<p>
<s>
Skauting	skauting	k1gInSc1	skauting
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
scout	scout	k1gMnSc1	scout
–	–	k?	–
zvěd	zvěd	k1gMnSc1	zvěd
<g/>
,	,	kIx,	,
průzkumník	průzkumník	k1gMnSc1	průzkumník
<g/>
,	,	kIx,	,
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
celosvětové	celosvětový	k2eAgNnSc1d1	celosvětové
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
výchova	výchova	k1gFnSc1	výchova
a	a	k8xC	a
sebevýchova	sebevýchova	k1gFnSc1	sebevýchova
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
skauti	skaut	k1gMnPc1	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
skauting	skauting	k1gInSc1	skauting
je	být	k5eAaImIp3nS	být
specifickou	specifický	k2eAgFnSc7d1	specifická
národní	národní	k2eAgFnSc7d1	národní
formou	forma	k1gFnSc7	forma
tohoto	tento	k3xDgNnSc2	tento
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
hnutí	hnutí	k1gNnSc2	hnutí
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
reprezentován	reprezentovat	k5eAaImNgMnS	reprezentovat
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
různorodých	různorodý	k2eAgInPc2d1	různorodý
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poslání	poslání	k1gNnPc1	poslání
a	a	k8xC	a
principy	princip	k1gInPc1	princip
==	==	k?	==
</s>
</p>
<p>
<s>
Posláním	poslání	k1gNnSc7	poslání
hnutí	hnutí	k1gNnSc2	hnutí
je	být	k5eAaImIp3nS	být
podporovat	podporovat	k5eAaImF	podporovat
rozvoj	rozvoj	k1gInSc4	rozvoj
osobnosti	osobnost	k1gFnSc2	osobnost
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
duchovních	duchovní	k2eAgFnPc2d1	duchovní
<g/>
,	,	kIx,	,
mravních	mravní	k2eAgFnPc2d1	mravní
<g/>
,	,	kIx,	,
intelektuálních	intelektuální	k2eAgFnPc2d1	intelektuální
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
a	a	k8xC	a
tělesných	tělesný	k2eAgFnPc2d1	tělesná
schopností	schopnost	k1gFnPc2	schopnost
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
připraveni	připraven	k2eAgMnPc1d1	připraven
plnit	plnit	k5eAaImF	plnit
povinnosti	povinnost	k1gFnPc4	povinnost
k	k	k7c3	k
sobě	se	k3xPyFc3	se
samým	samý	k3xTgMnPc3	samý
<g/>
,	,	kIx,	,
bližním	bližní	k1gMnPc3	bližní
<g/>
,	,	kIx,	,
vlasti	vlast	k1gFnSc3	vlast
<g/>
,	,	kIx,	,
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
celému	celý	k2eAgNnSc3d1	celé
lidskému	lidský	k2eAgNnSc3d1	lidské
společenství	společenství	k1gNnSc3	společenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
poslání	poslání	k1gNnSc1	poslání
je	být	k5eAaImIp3nS	být
blíže	blízce	k6eAd2	blízce
konkretizováno	konkretizován	k2eAgNnSc1d1	konkretizováno
třemi	tři	k4xCgInPc7	tři
principy	princip	k1gInPc7	princip
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
vyjádřeny	vyjádřit	k5eAaPmNgInP	vyjádřit
ve	v	k7c6	v
skautském	skautský	k2eAgInSc6d1	skautský
slibu	slib	k1gInSc6	slib
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
povinnost	povinnost	k1gFnSc1	povinnost
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
(	(	kIx(	(
<g/>
vyššímu	vysoký	k2eAgInSc3d2	vyšší
principu	princip	k1gInSc3	princip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
povinnost	povinnost	k1gFnSc4	povinnost
k	k	k7c3	k
bližním	bližní	k1gMnPc3	bližní
(	(	kIx(	(
<g/>
ostatním	ostatní	k2eAgMnPc3d1	ostatní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
povinnost	povinnost	k1gFnSc1	povinnost
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
<g/>
Poslání	poslání	k1gNnPc1	poslání
a	a	k8xC	a
principy	princip	k1gInPc1	princip
se	se	k3xPyFc4	se
skauting	skauting	k1gInSc1	skauting
snaží	snažit	k5eAaImIp3nS	snažit
naplňovat	naplňovat	k5eAaImF	naplňovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
skautské	skautský	k2eAgFnSc2d1	skautská
výchovné	výchovný	k2eAgFnSc2d1	výchovná
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
učení	učení	k1gNnSc4	učení
se	s	k7c7	s
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
,	,	kIx,	,
morální	morální	k2eAgInSc1d1	morální
kodex	kodex	k1gInSc1	kodex
vyjádřený	vyjádřený	k2eAgInSc1d1	vyjádřený
skautským	skautský	k2eAgInSc7d1	skautský
slibem	slib	k1gInSc7	slib
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
samosprávu	samospráva	k1gFnSc4	samospráva
malých	malý	k2eAgFnPc2d1	malá
skupin	skupina	k1gFnPc2	skupina
členů	člen	k1gMnPc2	člen
podobného	podobný	k2eAgInSc2d1	podobný
věku	věk	k1gInSc2	věk
(	(	kIx(	(
<g/>
družinový	družinový	k2eAgInSc1d1	družinový
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
podporovaných	podporovaný	k2eAgInPc2d1	podporovaný
dospělými	dospělý	k2eAgMnPc7d1	dospělý
<g/>
,	,	kIx,	,
individuální	individuální	k2eAgInSc4d1	individuální
program	program	k1gInSc4	program
osobního	osobní	k2eAgInSc2d1	osobní
růstu	růst	k1gInSc2	růst
(	(	kIx(	(
<g/>
např.	např.	kA	např.
skrz	skrz	k7c4	skrz
skautskou	skautský	k2eAgFnSc4d1	skautská
stezku	stezka	k1gFnSc4	stezka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
symbolických	symbolický	k2eAgInPc2d1	symbolický
rámců	rámec	k1gInPc2	rámec
a	a	k8xC	a
zapojení	zapojení	k1gNnSc2	zapojení
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
úrovních	úroveň	k1gFnPc6	úroveň
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povinnost	povinnost	k1gFnSc1	povinnost
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
===	===	k?	===
</s>
</p>
<p>
<s>
Různé	různý	k2eAgFnPc1d1	různá
skautské	skautský	k2eAgFnPc1d1	skautská
organizace	organizace	k1gFnPc1	organizace
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
principem	princip	k1gInSc7	princip
různě	různě	k6eAd1	různě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
slibu	slib	k1gInSc2	slib
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
organizaci	organizace	k1gFnSc6	organizace
WOSM	WOSM	kA	WOSM
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
organizace	organizace	k1gFnPc1	organizace
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
navázány	navázán	k2eAgInPc1d1	navázán
na	na	k7c4	na
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
náboženství	náboženství	k1gNnSc4	náboženství
nebo	nebo	k8xC	nebo
církev	církev	k1gFnSc4	církev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Německá	německý	k2eAgFnSc1d1	německá
asociace	asociace	k1gFnSc1	asociace
skautů	skaut	k1gMnPc2	skaut
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
nebo	nebo	k8xC	nebo
Skauti	skaut	k1gMnPc1	skaut
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Junák	junák	k1gMnSc1	junák
<g/>
)	)	kIx)	)
naopak	naopak	k6eAd1	naopak
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
povinností	povinnost	k1gFnSc7	povinnost
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
jako	jako	k8xS	jako
s	s	k7c7	s
povinností	povinnost	k1gFnSc7	povinnost
k	k	k7c3	k
hodnotám	hodnota	k1gFnPc3	hodnota
vyšším	vysoký	k2eAgFnPc3d2	vyšší
než	než	k8xS	než
materiálním	materiální	k2eAgFnPc3d1	materiální
bez	bez	k7c2	bez
odkazu	odkaz	k1gInSc2	odkaz
na	na	k7c4	na
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
Boha	bůh	k1gMnSc4	bůh
nebo	nebo	k8xC	nebo
náboženství	náboženství	k1gNnSc4	náboženství
a	a	k8xC	a
uznávají	uznávat	k5eAaImIp3nP	uznávat
širší	široký	k2eAgFnSc4d2	širší
škálu	škála	k1gFnSc4	škála
forem	forma	k1gFnPc2	forma
spirituality	spiritualita	k1gFnSc2	spiritualita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
i	i	k9	i
ve	v	k7c6	v
slibu	slib	k1gInSc6	slib
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
explicitní	explicitní	k2eAgFnSc2d1	explicitní
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c6	o
Bohu	bůh	k1gMnSc3	bůh
uvedena	uvést	k5eAaPmNgFnS	uvést
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
Pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
Láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
skauting	skauting	k1gInSc1	skauting
==	==	k?	==
</s>
</p>
<p>
<s>
Skauting	skauting	k1gInSc1	skauting
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
na	na	k7c6	na
základě	základ	k1gInSc6	základ
myšlenek	myšlenka	k1gFnPc2	myšlenka
zakladatele	zakladatel	k1gMnSc2	zakladatel
Roberta	Robert	k1gMnSc2	Robert
Baden-Powella	Baden-Powell	k1gMnSc2	Baden-Powell
<g/>
,	,	kIx,	,
významného	významný	k2eAgMnSc2d1	významný
britského	britský	k2eAgMnSc2d1	britský
generála	generál	k1gMnSc2	generál
<g/>
,	,	kIx,	,
hrdiny	hrdina	k1gMnSc2	hrdina
druhé	druhý	k4xOgFnSc2	druhý
búrské	búrský	k2eAgFnSc2d1	búrská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
šířilo	šířit	k5eAaImAgNnS	šířit
do	do	k7c2	do
dalších	další	k2eAgMnPc2d1	další
států	stát	k1gInPc2	stát
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
dalšími	další	k2eAgFnPc7d1	další
osobnostmi	osobnost	k1gFnPc7	osobnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ernest	Ernest	k1gMnSc1	Ernest
Thompson	Thompson	k1gMnSc1	Thompson
Seton	Seton	k1gMnSc1	Seton
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
skauting	skauting	k1gInSc1	skauting
existuje	existovat	k5eAaImIp3nS	existovat
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
216	[number]	k4	216
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
výjimek	výjimka	k1gFnPc2	výjimka
–	–	k?	–
vesměs	vesměs	k6eAd1	vesměs
totalitních	totalitní	k2eAgInPc2d1	totalitní
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc1	Laos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
má	mít	k5eAaImIp3nS	mít
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
zastřešujících	zastřešující	k2eAgFnPc2d1	zastřešující
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
pravidlem	pravidlem	k6eAd1	pravidlem
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
země	zem	k1gFnSc2	zem
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
organizace	organizace	k1gFnSc2	organizace
členskou	členský	k2eAgFnSc7d1	členská
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
skautských	skautský	k2eAgFnPc2d1	skautská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
vytvořit	vytvořit	k5eAaPmF	vytvořit
společnou	společný	k2eAgFnSc4d1	společná
koordinační	koordinační	k2eAgFnSc4d1	koordinační
skupinu	skupina	k1gFnSc4	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Skauting	skauting	k1gInSc1	skauting
–	–	k?	–
WOSM	WOSM	kA	WOSM
(	(	kIx(	(
<g/>
World	World	k1gInSc1	World
Organization	Organization	k1gInSc1	Organization
of	of	k?	of
the	the	k?	the
Scout	Scout	k2eAgInSc4d1	Scout
Movement	Movement	k1gInSc4	Movement
<g/>
)	)	kIx)	)
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
přes	přes	k7c4	přes
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
členů	člen	k1gMnPc2	člen
ze	z	k7c2	z
165	[number]	k4	165
států	stát	k1gInPc2	stát
a	a	k8xC	a
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dívčí	dívčí	k2eAgInSc1d1	dívčí
skauting	skauting	k1gInSc1	skauting
–	–	k?	–
WAGGGS	WAGGGS	kA	WAGGGS
(	(	kIx(	(
<g/>
World	World	k1gInSc1	World
Association	Association	k1gInSc1	Association
of	of	k?	of
Girl	girl	k1gFnSc2	girl
Guides	Guides	k1gMnSc1	Guides
and	and	k?	and
Girl	girl	k1gFnSc2	girl
Scouts	Scoutsa	k1gFnPc2	Scoutsa
<g/>
)	)	kIx)	)
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
asi	asi	k9	asi
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
členek	členka	k1gFnPc2	členka
ze	z	k7c2	z
145	[number]	k4	145
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skauting	skauting	k1gInSc1	skauting
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
–	–	k?	–
ISGF	ISGF	kA	ISGF
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
International	International	k1gMnSc1	International
Scout	Scout	k1gMnSc1	Scout
and	and	k?	and
Guide	Guid	k1gInSc5	Guid
Fellowship	Fellowship	k1gInSc1	Fellowship
<g/>
)	)	kIx)	)
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
dospělé	dospělý	k2eAgMnPc4d1	dospělý
skauty	skaut	k1gMnPc4	skaut
ze	z	k7c2	z
40	[number]	k4	40
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
hlavních	hlavní	k2eAgFnPc2d1	hlavní
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
spolupracujících	spolupracující	k2eAgFnPc2d1	spolupracující
organizací	organizace	k1gFnPc2	organizace
existují	existovat	k5eAaImIp3nP	existovat
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
sdružující	sdružující	k2eAgNnPc1d1	sdružující
skautská	skautský	k2eAgNnPc1d1	skautské
hnutí	hnutí	k1gNnPc1	hnutí
stojící	stojící	k2eAgNnPc1d1	stojící
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgInSc4d1	hlavní
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
organizace	organizace	k1gFnPc1	organizace
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
skautingu	skauting	k1gInSc3	skauting
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc3	jeho
hodnotám	hodnota	k1gFnPc3	hodnota
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
reprezentovány	reprezentován	k2eAgInPc4d1	reprezentován
výhradně	výhradně	k6eAd1	výhradně
dobrovolníky	dobrovolník	k1gMnPc7	dobrovolník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
federace	federace	k1gFnSc1	federace
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
skautů	skaut	k1gMnPc2	skaut
-	-	kIx~	-
World	World	k1gInSc1	World
Federation	Federation	k1gInSc1	Federation
of	of	k?	of
Independent	independent	k1gMnSc1	independent
Scouts	Scouts	k1gInSc1	Scouts
-	-	kIx~	-
WFIS	WFIS	kA	WFIS
-	-	kIx~	-
v	v	k7c6	v
r.	r.	kA	r.
2014	[number]	k4	2014
měla	mít	k5eAaImAgFnS	mít
bezmála	bezmála	k6eAd1	bezmála
200	[number]	k4	200
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
ve	v	k7c6	v
41	[number]	k4	41
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
asociace	asociace	k1gFnSc1	asociace
Skautů	skaut	k1gMnPc2	skaut
Evropy	Evropa	k1gFnSc2	Evropa
UIGSE	UIGSE	kA	UIGSE
–	–	k?	–
FSE	FSE	kA	FSE
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Union	union	k1gInSc1	union
Internationale	Internationale	k1gMnSc2	Internationale
des	des	k1gNnSc2	des
Guides	Guides	k1gMnSc1	Guides
et	et	k?	et
Scouts	Scouts	k1gInSc1	Scouts
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Europe	Europ	k1gInSc5	Europ
–	–	k?	–
Fédération	Fédération	k1gInSc1	Fédération
du	du	k?	du
Scoutisme	Scoutismus	k1gInSc5	Scoutismus
EuropéenMezinárodním	EuropéenMezinárodní	k2eAgInSc7d1	EuropéenMezinárodní
dnem	den	k1gInSc7	den
skautů	skaut	k1gMnPc2	skaut
je	být	k5eAaImIp3nS	být
svátek	svátek	k1gInSc1	svátek
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skautské	skautský	k2eAgInPc4d1	skautský
symboly	symbol	k1gInPc4	symbol
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skautský	skautský	k2eAgInSc1d1	skautský
pozdrav	pozdrav	k1gInSc1	pozdrav
===	===	k?	===
</s>
</p>
<p>
<s>
Skauti	skaut	k1gMnPc1	skaut
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
zdraví	zdravit	k5eAaImIp3nS	zdravit
zdviženou	zdvižený	k2eAgFnSc7d1	zdvižená
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
k	k	k7c3	k
rameni	rameno	k1gNnSc3	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Ruka	ruka	k1gFnSc1	ruka
je	být	k5eAaImIp3nS	být
obrácená	obrácený	k2eAgFnSc1d1	obrácená
dlaní	dlaň	k1gFnPc2	dlaň
vpřed	vpřed	k6eAd1	vpřed
u	u	k7c2	u
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
(	(	kIx(	(
<g/>
mládež	mládež	k1gFnSc1	mládež
většinou	většinou	k6eAd1	většinou
od	od	k7c2	od
12	[number]	k4	12
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
se	s	k7c7	s
vztyčenými	vztyčený	k2eAgInPc7d1	vztyčený
třemi	tři	k4xCgInPc7	tři
prsty	prst	k1gInPc7	prst
<g/>
,	,	kIx,	,
u	u	k7c2	u
světlušek	světluška	k1gFnPc2	světluška
a	a	k8xC	a
vlčat	vlče	k1gNnPc2	vlče
(	(	kIx(	(
<g/>
děti	dítě	k1gFnPc1	dítě
od	od	k7c2	od
6	[number]	k4	6
do	do	k7c2	do
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vztyčují	vztyčovat	k5eAaImIp3nP	vztyčovat
jen	jen	k9	jen
prsty	prst	k1gInPc4	prst
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
skautů	skaut	k1gMnPc2	skaut
tři	tři	k4xCgInPc1	tři
prsty	prst	k1gInPc1	prst
představují	představovat	k5eAaImIp3nP	představovat
tři	tři	k4xCgInPc4	tři
body	bod	k1gInPc4	bod
skautského	skautský	k2eAgInSc2d1	skautský
slibu	slib	k1gInSc2	slib
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
palec	palec	k1gInSc1	palec
překrývá	překrývat	k5eAaImIp3nS	překrývat
malíček	malíček	k1gInSc4	malíček
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
ochranu	ochrana	k1gFnSc4	ochrana
slabších	slabý	k2eAgMnPc2d2	slabší
silnějšími	silný	k2eAgInPc7d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
palce	palec	k1gInSc2	palec
a	a	k8xC	a
malíčku	malíček	k1gInSc2	malíček
vzniká	vznikat	k5eAaImIp3nS	vznikat
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
přátelství	přátelství	k1gNnSc4	přátelství
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
setkání	setkání	k1gNnSc6	setkání
se	se	k3xPyFc4	se
skauti	skaut	k1gMnPc1	skaut
také	také	k6eAd1	také
zdraví	zdraví	k1gNnSc6	zdraví
podáním	podání	k1gNnSc7	podání
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
se	s	k7c7	s
zaklesnutým	zaklesnutý	k2eAgInSc7d1	zaklesnutý
malíčkem	malíček	k1gInSc7	malíček
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgFnPc1	tři
verze	verze	k1gFnPc1	verze
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tento	tento	k3xDgInSc1	tento
pozdrav	pozdrav	k1gInSc1	pozdrav
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
levá	levý	k2eAgFnSc1d1	levá
ruka	ruka	k1gFnSc1	ruka
je	být	k5eAaImIp3nS	být
blíž	blízce	k6eAd2	blízce
k	k	k7c3	k
srdci	srdce	k1gNnSc3	srdce
a	a	k8xC	a
zaklesnutí	zaklesnutí	k1gNnSc3	zaklesnutí
malíčku	malíček	k1gInSc2	malíček
je	být	k5eAaImIp3nS	být
důkazem	důkaz	k1gInSc7	důkaz
propojení	propojení	k1gNnSc2	propojení
a	a	k8xC	a
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
rytíři	rytíř	k1gMnPc1	rytíř
po	po	k7c6	po
soubojích	souboj	k1gInPc6	souboj
podávali	podávat	k5eAaImAgMnP	podávat
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
levou	levý	k2eAgFnSc4d1	levá
ruku	ruka	k1gFnSc4	ruka
podat	podat	k5eAaPmF	podat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
důvěry	důvěra	k1gFnSc2	důvěra
sundali	sundat	k5eAaPmAgMnP	sundat
štít	štít	k1gInSc4	štít
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
zdravili	zdravit	k5eAaImAgMnP	zdravit
afričtí	africký	k2eAgMnPc1d1	africký
bojovníci	bojovník	k1gMnPc1	bojovník
<g/>
,	,	kIx,	,
podáním	podání	k1gNnSc7	podání
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
dávali	dávat	k5eAaImAgMnP	dávat
spolubojovníkům	spolubojovník	k1gMnPc3	spolubojovník
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
plně	plně	k6eAd1	plně
důvěřují	důvěřovat	k5eAaImIp3nP	důvěřovat
a	a	k8xC	a
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
členy	člen	k1gMnPc7	člen
stejné	stejný	k2eAgFnSc2d1	stejná
družiny	družina	k1gFnSc2	družina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
skauti	skaut	k1gMnPc1	skaut
se	se	k3xPyFc4	se
slovně	slovně	k6eAd1	slovně
zdraví	zdravit	k5eAaImIp3nS	zdravit
Nazdar	nazdar	k0	nazdar
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Morálka	morálka	k1gFnSc1	morálka
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Skautský	skautský	k2eAgInSc4d1	skautský
slib	slib	k1gInSc4	slib
a	a	k8xC	a
související	související	k2eAgFnPc4d1	související
morální	morální	k2eAgFnPc4d1	morální
zásady	zásada	k1gFnPc4	zásada
skautského	skautský	k2eAgInSc2d1	skautský
zákona	zákon	k1gInSc2	zákon
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
principů	princip	k1gInPc2	princip
stanovených	stanovený	k2eAgInPc2d1	stanovený
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
Robertem	Robert	k1gMnSc7	Robert
Baden-Powellem	Baden-Powell	k1gMnSc7	Baden-Powell
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
1	[number]	k4	1
<g/>
.	.	kIx.	.
princip	princip	k1gInSc1	princip
bývá	bývat	k5eAaImIp3nS	bývat
nejjasněji	jasně	k6eAd3	jasně
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
ve	v	k7c6	v
skautském	skautský	k2eAgInSc6d1	skautský
slibu	slib	k1gInSc6	slib
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jako	jako	k8xC	jako
služba	služba	k1gFnSc1	služba
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
vlasti	vlast	k1gFnSc3	vlast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
nějakým	nějaký	k3yIgInSc7	nějaký
jasným	jasný	k2eAgInSc7d1	jasný
způsobem	způsob	k1gInSc7	způsob
všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
principy	princip	k1gInPc4	princip
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
svých	svůj	k3xOyFgFnPc6	svůj
členských	členský	k2eAgFnPc6d1	členská
organizacích	organizace	k1gFnPc6	organizace
WOSM	WOSM	kA	WOSM
a	a	k8xC	a
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
požadavek	požadavek	k1gInSc1	požadavek
i	i	k8xC	i
dodržován	dodržovat	k5eAaImNgInS	dodržovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mají	mít	k5eAaImIp3nP	mít
s	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
principem	princip	k1gInSc7	princip
určité	určitý	k2eAgFnSc2d1	určitá
potíže	potíž	k1gFnSc2	potíž
v	v	k7c6	v
silně	silně	k6eAd1	silně
sekularizovaných	sekularizovaný	k2eAgInPc6d1	sekularizovaný
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnPc1	Francie
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
skauting	skauting	k1gInSc1	skauting
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
výrazně	výrazně	k6eAd1	výrazně
nenáboženský	náboženský	k2eNgInSc1d1	nenáboženský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
náboženský	náboženský	k2eAgInSc1d1	náboženský
požadavek	požadavek	k1gInSc1	požadavek
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
originálním	originální	k2eAgInSc7d1	originální
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
byl	být	k5eAaImAgInS	být
WOSM	WOSM	kA	WOSM
i	i	k8xC	i
WAGGGS	WAGGGS	kA	WAGGGS
akceptován	akceptován	k2eAgMnSc1d1	akceptován
<g/>
:	:	kIx,	:
Transcendentní	transcendentní	k2eAgFnSc1d1	transcendentní
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
níž	jenž	k3xRgFnSc3	jenž
nesou	nést	k5eAaImIp3nP	nést
skauti	skaut	k1gMnPc1	skaut
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
definovaná	definovaný	k2eAgFnSc1d1	definovaná
jako	jako	k8xC	jako
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
Pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
Láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vazby	vazba	k1gFnSc2	vazba
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
víru	víra	k1gFnSc4	víra
či	či	k8xC	či
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Limitem	limit	k1gInSc7	limit
skautských	skautský	k2eAgFnPc2d1	skautská
zásad	zásada	k1gFnPc2	zásada
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
jejich	jejich	k3xOp3gInSc4	jejich
obecný	obecný	k2eAgInSc4d1	obecný
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
velice	velice	k6eAd1	velice
individuální	individuální	k2eAgInSc4d1	individuální
výklad	výklad	k1gInSc4	výklad
i	i	k8xC	i
aplikaci	aplikace	k1gFnSc4	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Antisemitismus	antisemitismus	k1gInSc1	antisemitismus
a	a	k8xC	a
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
===	===	k?	===
</s>
</p>
<p>
<s>
Skauting	skauting	k1gInSc1	skauting
bývá	bývat	k5eAaImIp3nS	bývat
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
bratrství	bratrství	k1gNnSc2	bratrství
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
národnost	národnost	k1gFnSc4	národnost
nebo	nebo	k8xC	nebo
barvu	barva	k1gFnSc4	barva
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
skautingu	skauting	k1gInSc2	skauting
však	však	k9	však
opakovaně	opakovaně	k6eAd1	opakovaně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
situacím	situace	k1gFnPc3	situace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
tato	tento	k3xDgFnSc1	tento
idea	idea	k1gFnSc1	idea
skautingu	skauting	k1gInSc2	skauting
dramaticky	dramaticky	k6eAd1	dramaticky
selhávala	selhávat	k5eAaImAgFnS	selhávat
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
střet	střet	k1gInSc1	střet
dvou	dva	k4xCgFnPc2	dva
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
zásad	zásada	k1gFnPc2	zásada
<g/>
:	:	kIx,	:
bratrství	bratrství	k1gNnSc2	bratrství
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
politicky	politicky	k6eAd1	politicky
klidných	klidný	k2eAgFnPc6d1	klidná
dobách	doba	k1gFnPc6	doba
dokázali	dokázat	k5eAaPmAgMnP	dokázat
skauti	skaut	k1gMnPc1	skaut
obě	dva	k4xCgFnPc4	dva
hodnoty	hodnota	k1gFnPc4	hodnota
kombinovat	kombinovat	k5eAaImF	kombinovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
okamžicích	okamžik	k1gInPc6	okamžik
národního	národní	k2eAgMnSc2d1	národní
či	či	k8xC	či
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
sklon	sklon	k1gInSc4	sklon
podléhat	podléhat	k5eAaImF	podléhat
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
a	a	k8xC	a
vypjatému	vypjatý	k2eAgInSc3d1	vypjatý
politickému	politický	k2eAgInSc3d1	politický
diskurzu	diskurz	k1gInSc3	diskurz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
třicátých	třicátý	k4xOgNnPc2	třicátý
a	a	k8xC	a
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
způsobila	způsobit	k5eAaPmAgFnS	způsobit
nejednoznačnost	nejednoznačnost	k1gFnSc1	nejednoznačnost
skautských	skautský	k2eAgInPc2d1	skautský
ideálů	ideál	k1gInPc2	ideál
rozkol	rozkol	k1gInSc1	rozkol
mezi	mezi	k7c7	mezi
některými	některý	k3yIgFnPc7	některý
evropskými	evropský	k2eAgFnPc7d1	Evropská
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
skautských	skautský	k2eAgInPc2d1	skautský
oddílů	oddíl	k1gInPc2	oddíl
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
klerofašistické	klerofašistický	k2eAgFnSc2d1	klerofašistická
Hlinkovy	Hlinkův	k2eAgFnSc2d1	Hlinkova
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
ze	z	k7c2	z
skautů	skaut	k1gMnPc2	skaut
stávaly	stávat	k5eAaImAgInP	stávat
oddíly	oddíl	k1gInPc1	oddíl
Hitlerjugend	Hitlerjugenda	k1gFnPc2	Hitlerjugenda
<g/>
.	.	kIx.	.
</s>
<s>
Polští	polský	k2eAgMnPc1d1	polský
skauti	skaut	k1gMnPc1	skaut
se	se	k3xPyFc4	se
zapojovali	zapojovat	k5eAaImAgMnP	zapojovat
do	do	k7c2	do
pohraničních	pohraniční	k2eAgInPc2d1	pohraniční
bojů	boj	k1gInPc2	boj
proti	proti	k7c3	proti
Československu	Československo	k1gNnSc3	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
Junák	junák	k1gMnSc1	junák
se	se	k3xPyFc4	se
okupačním	okupační	k2eAgFnPc3d1	okupační
úřadům	úřada	k1gMnPc3	úřada
snažil	snažit	k5eAaImAgMnS	snažit
zalíbit	zalíbit	k5eAaPmF	zalíbit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
začal	začít	k5eAaPmAgMnS	začít
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
narůstajícího	narůstající	k2eAgInSc2d1	narůstající
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
ze	z	k7c2	z
skautských	skautský	k2eAgInPc2d1	skautský
oddílů	oddíl	k1gInPc2	oddíl
vylučovat	vylučovat	k5eAaImF	vylučovat
chlapce	chlapec	k1gMnPc4	chlapec
a	a	k8xC	a
děvčata	děvče	k1gNnPc4	děvče
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Plajner	Plajner	k1gMnSc1	Plajner
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vrcholných	vrcholný	k2eAgMnPc2d1	vrcholný
představitelů	představitel	k1gMnPc2	představitel
tuzemského	tuzemský	k2eAgMnSc2d1	tuzemský
Junáka	junák	k1gMnSc2	junák
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
ostatním	ostatní	k2eAgMnPc3d1	ostatní
oddílovým	oddílový	k2eAgMnPc3d1	oddílový
vedoucím	vedoucí	k1gMnPc3	vedoucí
doporučovat	doporučovat	k5eAaImF	doporučovat
ke	k	k7c3	k
čtení	čtení	k1gNnSc3	čtení
příručky	příručka	k1gFnSc2	příručka
Hitlerjugend	Hitlerjugenda	k1gFnPc2	Hitlerjugenda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pedofilní	pedofilní	k2eAgFnSc2d1	pedofilní
kauzy	kauza	k1gFnSc2	kauza
===	===	k?	===
</s>
</p>
<p>
<s>
Steně	Steně	k6eAd1	Steně
jako	jako	k8xC	jako
jiným	jiný	k2eAgFnPc3d1	jiná
výchovným	výchovný	k2eAgFnPc3d1	výchovná
a	a	k8xC	a
náboženským	náboženský	k2eAgFnPc3d1	náboženská
organizacím	organizace	k1gFnPc3	organizace
<g/>
,	,	kIx,	,
nevyhnuly	vyhnout	k5eNaPmAgFnP	vyhnout
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
skautům	skaut	k1gMnPc3	skaut
pedofilní	pedofilní	k2eAgMnPc1d1	pedofilní
skandály	skandál	k1gInPc4	skandál
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
tuzemských	tuzemský	k2eAgInPc2d1	tuzemský
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
kauza	kauza	k1gFnSc1	kauza
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
s	s	k7c7	s
přezdívkami	přezdívka	k1gFnPc7	přezdívka
Piškot	piškot	k1gInSc1	piškot
a	a	k8xC	a
Meluzín	Meluzín	k1gMnSc1	Meluzín
z	z	k7c2	z
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
postupně	postupně	k6eAd1	postupně
zneužili	zneužít	k5eAaPmAgMnP	zneužít
asi	asi	k9	asi
čtyři	čtyři	k4xCgFnPc4	čtyři
desítky	desítka	k1gFnPc4	desítka
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
oběti	oběť	k1gFnPc4	oběť
přinutili	přinutit	k5eAaPmAgMnP	přinutit
k	k	k7c3	k
diskrétnosti	diskrétnost	k1gFnSc3	diskrétnost
výzvou	výzva	k1gFnSc7	výzva
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
zásad	zásada	k1gFnPc2	zásada
skauta	skaut	k1gMnSc2	skaut
je	být	k5eAaImIp3nS	být
mlčení	mlčení	k1gNnSc4	mlčení
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
také	také	k9	také
případ	případ	k1gInSc1	případ
skautského	skautský	k2eAgMnSc2d1	skautský
vedoucího	vedoucí	k1gMnSc2	vedoucí
a	a	k8xC	a
bývalého	bývalý	k2eAgMnSc2d1	bývalý
pražského	pražský	k2eAgMnSc2d1	pražský
policisty	policista	k1gMnSc2	policista
Stejskala	Stejskal	k1gMnSc2	Stejskal
<g/>
.	.	kIx.	.
</s>
<s>
Příznivci	příznivec	k1gMnPc1	příznivec
skautingu	skauting	k1gInSc2	skauting
ale	ale	k8xC	ale
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
30	[number]	k4	30
let	léto	k1gNnPc2	léto
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
Junáku	junák	k1gMnSc6	junák
–	–	k?	–
českém	český	k2eAgInSc6d1	český
skautu	skaut	k1gMnSc3	skaut
takových	takový	k3xDgMnPc2	takový
případů	případ	k1gInPc2	případ
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
pět	pět	k4xCc1	pět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Diskriminace	diskriminace	k1gFnSc2	diskriminace
LGBTQ	LGBTQ	kA	LGBTQ
<g/>
+	+	kIx~	+
osob	osoba	k1gFnPc2	osoba
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
skautské	skautský	k2eAgFnPc1d1	skautská
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
majoritní	majoritní	k2eAgFnSc1d1	majoritní
americká	americký	k2eAgFnSc1d1	americká
Boy	boy	k1gMnSc1	boy
Scouts	Scoutsa	k1gFnPc2	Scoutsa
of	of	k?	of
America	America	k1gMnSc1	America
(	(	kIx(	(
<g/>
BSA	BSA	kA	BSA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
neumožňovaly	umožňovat	k5eNaImAgFnP	umožňovat
členství	členství	k1gNnSc4	členství
LGBT	LGBT	kA	LGBT
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
náboženským	náboženský	k2eAgNnSc7d1	náboženské
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
BSA	BSA	kA	BSA
neumožňovali	umožňovat	k5eNaImAgMnP	umožňovat
členství	členství	k1gNnSc4	členství
"	"	kIx"	"
<g/>
otevřeně	otevřeně	k6eAd1	otevřeně
se	se	k3xPyFc4	se
projevujícím	projevující	k2eAgMnPc3d1	projevující
<g/>
"	"	kIx"	"
homosexuálům	homosexuál	k1gMnPc3	homosexuál
<g/>
;	;	kIx,	;
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
těmto	tento	k3xDgNnPc3	tento
osobám	osoba	k1gFnPc3	osoba
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
působit	působit	k5eAaImF	působit
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jako	jako	k9	jako
pomáhající	pomáhající	k2eAgMnPc1d1	pomáhající
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
začali	začít	k5eAaPmAgMnP	začít
BSA	BSA	kA	BSA
přijímat	přijímat	k5eAaImF	přijímat
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
transgender	transgendra	k1gFnPc2	transgendra
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Český	český	k2eAgInSc1d1	český
skauting	skauting	k1gInSc1	skauting
==	==	k?	==
</s>
</p>
<p>
<s>
Skauting	skauting	k1gInSc1	skauting
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dominantní	dominantní	k2eAgFnSc7d1	dominantní
českou	český	k2eAgFnSc7d1	Česká
skautskou	skautský	k2eAgFnSc7d1	skautská
organizací	organizace	k1gFnSc7	organizace
je	být	k5eAaImIp3nS	být
Junák	junák	k1gMnSc1	junák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
<g/>
,	,	kIx,	,
z.	z.	k?	z.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
členů	člen	k1gInPc2	člen
překračujícím	překračující	k2eAgMnSc7d1	překračující
60	[number]	k4	60
tisíc	tisíc	k4xCgInSc4	tisíc
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakládající	zakládající	k2eAgFnSc7d1	zakládající
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
českou	český	k2eAgFnSc7d1	Česká
členskou	členský	k2eAgFnSc7d1	členská
organizací	organizace	k1gFnSc7	organizace
WOSM	WOSM	kA	WOSM
<g/>
,	,	kIx,	,
WAGGGS	WAGGGS	kA	WAGGGS
a	a	k8xC	a
ISGF	ISGF	kA	ISGF
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
byl	být	k5eAaImAgMnS	být
Antonín	Antonín	k1gMnSc1	Antonín
Benjamín	Benjamín	k1gMnSc1	Benjamín
Svojsík	Svojsík	k1gMnSc1	Svojsík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
Junáka	junák	k1gMnSc2	junák
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
skautských	skautský	k2eAgFnPc2d1	skautská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Svaz	svaz	k1gInSc1	svaz
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chronologický	chronologický	k2eAgInSc4d1	chronologický
předhled	předhled	k1gInSc4	předhled
českého	český	k2eAgInSc2d1	český
skautingu	skauting	k1gInSc2	skauting
===	===	k?	===
</s>
</p>
<p>
<s>
1911	[number]	k4	1911
A.	A.	kA	A.
B.	B.	kA	B.
Svojsík	Svojsík	k1gInSc1	Svojsík
se	se	k3xPyFc4	se
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	s	k7c7	s
skautingem	skauting	k1gInSc7	skauting
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1912	[number]	k4	1912
A.	A.	kA	A.
B.	B.	kA	B.
Svojsík	Svojsík	k1gMnSc1	Svojsík
vydal	vydat	k5eAaPmAgInS	vydat
knihu	kniha	k1gFnSc4	kniha
Základy	základ	k1gInPc4	základ
junáctví	junáctví	k1gNnSc2	junáctví
a	a	k8xC	a
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
první	první	k4xOgInSc4	první
skautský	skautský	k2eAgInSc4d1	skautský
tábor	tábor	k1gInSc4	tábor
nedaleko	nedaleko	k7c2	nedaleko
hradu	hrad	k1gInSc2	hrad
Lipnice	Lipnice	k1gFnSc2	Lipnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1914	[number]	k4	1914
Založen	založen	k2eAgInSc1d1	založen
spolek	spolek	k1gInSc1	spolek
Junák	junák	k1gMnSc1	junák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1915	[number]	k4	1915
Založen	založen	k2eAgInSc1d1	založen
dívčí	dívčí	k2eAgInSc1d1	dívčí
skauting	skauting	k1gInSc1	skauting
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
tábor	tábor	k1gInSc1	tábor
skautek	skautka	k1gFnPc2	skautka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
A.	A.	kA	A.
B.	B.	kA	B.
Svojsík	Svojsík	k1gMnSc1	Svojsík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
Sjednoceny	sjednocen	k2eAgFnPc1d1	sjednocena
skautské	skautský	k2eAgFnPc1d1	skautská
organizace	organizace	k1gFnPc1	organizace
ve	v	k7c6	v
svazu	svaz	k1gInSc6	svaz
Junák	junák	k1gMnSc1	junák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1940	[number]	k4	1940
Z	z	k7c2	z
některých	některý	k3yIgMnPc2	některý
junáckých	junácký	k2eAgMnPc2d1	junácký
oddílů	oddíl	k1gInPc2	oddíl
jsou	být	k5eAaImIp3nP	být
vylučováni	vylučován	k2eAgMnPc1d1	vylučován
chlapci	chlapec	k1gMnPc1	chlapec
a	a	k8xC	a
děvčata	děvče	k1gNnPc1	děvče
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1940	[number]	k4	1940
Junák	junák	k1gMnSc1	junák
rozpuštěn	rozpustit	k5eAaPmNgMnS	rozpustit
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
Junák	junák	k1gMnSc1	junák
obnoven	obnoven	k2eAgMnSc1d1	obnoven
ihned	ihned	k6eAd1	ihned
při	při	k7c6	při
Pražském	pražský	k2eAgNnSc6d1	Pražské
květnovém	květnový	k2eAgNnSc6d1	květnové
povstání	povstání	k1gNnSc6	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
Likvidace	likvidace	k1gFnSc1	likvidace
Junáka	junák	k1gMnSc2	junák
komunisty	komunista	k1gMnSc2	komunista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
Oficiální	oficiální	k2eAgInSc1d1	oficiální
zánik	zánik	k1gInSc1	zánik
Junáka	junák	k1gMnSc2	junák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Junák	junák	k1gMnSc1	junák
obnoven	obnoven	k2eAgInSc1d1	obnoven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
Junák	junák	k1gMnSc1	junák
opět	opět	k6eAd1	opět
rozpuštěn	rozpustit	k5eAaPmNgMnS	rozpustit
komunisty	komunista	k1gMnPc7	komunista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Junák	junák	k1gMnSc1	junák
potřetí	potřetí	k4xO	potřetí
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Chlapecký	chlapecký	k2eAgInSc1d1	chlapecký
kmen	kmen	k1gInSc1	kmen
Junáka	junák	k1gMnSc2	junák
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
WOSM	WOSM	kA	WOSM
<g/>
,	,	kIx,	,
dívčí	dívčí	k2eAgInSc1d1	dívčí
kmen	kmen	k1gInSc1	kmen
Junáka	junák	k1gMnSc2	junák
opětovně	opětovně	k6eAd1	opětovně
získal	získat	k5eAaPmAgMnS	získat
status	status	k1gInSc4	status
zakládajícího	zakládající	k2eAgMnSc2d1	zakládající
člena	člen	k1gMnSc2	člen
WAGGGS	WAGGGS	kA	WAGGGS
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Československa	Československo	k1gNnSc2	Československo
WOSM	WOSM	kA	WOSM
požadoval	požadovat	k5eAaImAgMnS	požadovat
nové	nový	k2eAgNnSc4d1	nové
přihlášení	přihlášení	k1gNnSc4	přihlášení
skautů	skaut	k1gMnPc2	skaut
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
následovnických	následovnický	k2eAgInPc2d1	následovnický
států	stát	k1gInPc2	stát
do	do	k7c2	do
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
skautského	skautský	k2eAgNnSc2d1	skautské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
Junák	junák	k1gMnSc1	junák
tak	tak	k8xS	tak
byl	být	k5eAaImAgInS	být
opětovně	opětovně	k6eAd1	opětovně
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
WOSM	WOSM	kA	WOSM
v	v	k7c6	v
r.	r.	kA	r.
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
Slovenský	slovenský	k2eAgInSc1d1	slovenský
skauting	skauting	k1gInSc1	skauting
v	v	k7c6	v
r.	r.	kA	r.
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Byl	být	k5eAaImAgMnS	být
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c4	na
Junák	junák	k1gMnSc1	junák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současné	současný	k2eAgFnPc1d1	současná
skautské	skautský	k2eAgFnPc1d1	skautská
organizace	organizace	k1gFnPc1	organizace
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Junák	junák	k1gMnSc1	junák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
</s>
</p>
<p>
<s>
SKAUT	Skaut	k1gInSc1	Skaut
–	–	k?	–
český	český	k2eAgInSc1d1	český
skauting	skauting	k1gInSc1	skauting
ABS	ABS	kA	ABS
</s>
</p>
<p>
<s>
YMCA	YMCA	kA	YMCA
skauti	skaut	k1gMnPc1	skaut
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
Pathfinder	Pathfindra	k1gFnPc2	Pathfindra
</s>
</p>
<p>
<s>
Svaz	svaz	k1gInSc1	svaz
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Skauti	skaut	k1gMnPc1	skaut
lesní	lesní	k2eAgFnSc2d1	lesní
moudrosti	moudrost	k1gFnSc2	moudrost
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
Skauti	skaut	k1gMnPc1	skaut
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skaut	Skaut	k1gInSc1	Skaut
–	–	k?	–
S.	S.	kA	S.
<g/>
S.	S.	kA	S.
<g/>
V.	V.	kA	V.
(	(	kIx(	(
<g/>
skauti	skaut	k1gMnPc1	skaut
a	a	k8xC	a
skautky	skautka	k1gFnPc1	skautka
vpřed	vpřed	k6eAd1	vpřed
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Harcerstwo	Harcerstwo	k1gMnSc1	Harcerstwo
Polskie	Polskie	k1gFnSc2	Polskie
w	w	k?	w
Republice	republika	k1gFnSc3	republika
Czeskiej	Czeskiej	k1gInSc4	Czeskiej
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Nagy	Naga	k1gFnPc1	Naga
<g/>
,	,	kIx,	,
László	László	k1gFnPc1	László
<g/>
.	.	kIx.	.
250	[number]	k4	250
miliónů	milión	k4xCgInPc2	milión
skautů	skaut	k1gMnPc2	skaut
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Junák	junák	k1gMnSc1	junák
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
154	[number]	k4	154
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86109	[number]	k4	86109
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skautem	Skaut	k1gInSc7	Skaut
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
zemědílech	zemědíl	k1gInPc6	zemědíl
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
2	[number]	k4	2
/	/	kIx~	/
Frederick	Frederick	k1gInSc1	Frederick
Russell	Russell	k1gMnSc1	Russell
Burnham	Burnham	k1gInSc1	Burnham
;	;	kIx,	;
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
a	a	k8xC	a
upravil	upravit	k5eAaPmAgMnS	upravit
H.	H.	kA	H.
Jost	Jost	k1gMnSc1	Jost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
grafická	grafický	k2eAgFnSc1d1	grafická
Unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
-	-	kIx~	-
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Vondráček	Vondráček	k1gMnSc1	Vondráček
<g/>
:	:	kIx,	:
Křesťanští	křesťanštět	k5eAaImIp3nP	křesťanštět
skauti	skaut	k1gMnPc1	skaut
a	a	k8xC	a
oddíly	oddíl	k1gInPc7	oddíl
očima	oko	k1gNnPc7	oko
skautů	skaut	k1gMnPc2	skaut
nekřesťanských	křesťanský	k2eNgInPc2d1	nekřesťanský
oddílů	oddíl	k1gInPc2	oddíl
<g/>
,	,	kIx,	,
Getsemany	Getsemany	k1gInPc1	Getsemany
č.	č.	k?	č.
212	[number]	k4	212
<g/>
,	,	kIx,	,
leden	leden	k1gInSc1	leden
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
skauting	skauting	k1gInSc1	skauting
</s>
</p>
<p>
<s>
Junák	junák	k1gMnSc1	junák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
</s>
</p>
<p>
<s>
Kontroverze	kontroverze	k1gFnPc1	kontroverze
a	a	k8xC	a
konflikty	konflikt	k1gInPc1	konflikt
skautingu	skauting	k1gInSc2	skauting
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
známých	známý	k2eAgMnPc2d1	známý
českých	český	k2eAgMnPc2d1	český
skautů	skaut	k1gMnPc2	skaut
</s>
</p>
<p>
<s>
Skautské	skautský	k2eAgInPc1d1	skautský
zákony	zákon	k1gInPc1	zákon
</s>
</p>
<p>
<s>
Tramping	tramping	k1gInSc1	tramping
</s>
</p>
<p>
<s>
Woodcraft	Woodcraft	k1gMnSc1	Woodcraft
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
skauting	skauting	k1gInSc1	skauting
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Skauting	skauting	k1gInSc1	skauting
</s>
</p>
<p>
<s>
Skauting	skauting	k1gInSc1	skauting
-	-	kIx~	-
stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
českém	český	k2eAgInSc6d1	český
i	i	k8xC	i
světovém	světový	k2eAgInSc6d1	světový
skautingu	skauting	k1gInSc6	skauting
</s>
</p>
<p>
<s>
Sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
skautingu	skauting	k1gInSc2	skauting
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
