<s>
Řehoř	Řehoř	k1gMnSc1
Barbarigo	Barbarigo	k1gMnSc1
</s>
<s>
Řehoř	Řehoř	k1gMnSc1
Barbarigo	Barbarigo	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1625	#num#	k4
<g/>
Benátky	Benátky	k1gFnPc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1697	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
71	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Padova	Padova	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Padua	Padua	k1gFnSc1
Cathedral	Cathedral	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Padovská	padovský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
diecézní	diecézní	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1657	#num#	k4
<g/>
)	)	kIx)
<g/>
katolický	katolický	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1657	#num#	k4
<g/>
)	)	kIx)
<g/>
kardinál	kardinál	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1660	#num#	k4
<g/>
)	)	kIx)
<g/>
diecézní	diecézní	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1664	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Řehoř	Řehoř	k1gMnSc1
Barbarigo	Barbarigo	k1gMnSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1625	#num#	k4
<g/>
,	,	kIx,
Benátky	Benátky	k1gFnPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1697	#num#	k4
Padova	Padova	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
obojí	oboj	k1gFnSc7
tehdy	tehdy	k6eAd1
Benátská	benátský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgFnSc1d1
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
kardinál	kardinál	k1gMnSc1
<g/>
,	,	kIx,
biskup	biskup	k1gMnSc1
<g/>
,	,	kIx,
katolický	katolický	k2eAgMnSc1d1
diplomat	diplomat	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
seminářů	seminář	k1gInPc2
a	a	k8xC
škol	škola	k1gFnPc2
výchovných	výchovný	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
mnohojazyčné	mnohojazyčný	k2eAgFnSc2d1
tiskárny	tiskárna	k1gFnSc2
<g/>
,	,	kIx,
horlivý	horlivý	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
a	a	k8xC
boží	boží	k2eAgMnSc1d1
služebník	služebník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1625	#num#	k4
do	do	k7c2
bohaté	bohatý	k2eAgFnSc2d1
a	a	k8xC
vážené	vážený	k2eAgFnSc2d1
benátské	benátský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
diplomatem	diplomat	k1gInSc7
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
diplomatickou	diplomatický	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
plánoval	plánovat	k5eAaImAgMnS
také	také	k9
pro	pro	k7c4
svého	svůj	k3xOyFgMnSc4
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
nechal	nechat	k5eAaPmAgMnS
vystudovat	vystudovat	k5eAaPmF
právo	právo	k1gNnSc4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Padově	Padova	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pramenů	pramen	k1gInPc2
patřil	patřit	k5eAaImAgInS
k	k	k7c3
nejlepším	dobrý	k2eAgInPc3d3
diplomatům	diplomat	k1gInPc3
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
kongresu	kongres	k1gInSc2
v	v	k7c6
Münsteru	Münster	k1gInSc6
na	na	k7c4
jednání	jednání	k1gNnSc4
o	o	k7c6
skončení	skončení	k1gNnSc6
náboženské	náboženský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dostávala	dostávat	k5eAaImAgFnS
podobu	podoba	k1gFnSc4
boje	boj	k1gInSc2
o	o	k7c4
evropskou	evropský	k2eAgFnSc4d1
nadvládu	nadvláda	k1gFnSc4
mezi	mezi	k7c7
Rakouskem	Rakousko	k1gNnSc7
a	a	k8xC
Francií	Francie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc1
jednání	jednání	k1gNnSc2
byly	být	k5eAaImAgInP
pak	pak	k6eAd1
stvrzeny	stvrzen	k2eAgInPc1d1
Vestfálskou	vestfálský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
o	o	k7c4
míru	míra	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konferenci	konference	k1gFnSc6
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
i	i	k9
s	s	k7c7
papežským	papežský	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
Fabiem	Fabius	k1gMnSc7
Chigi	Chigi	k1gNnSc2
(	(	kIx(
<g/>
pozdějším	pozdní	k2eAgMnSc7d2
papežem	papež	k1gMnSc7
Alexandrem	Alexandr	k1gMnSc7
VII	VII	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Benátek	Benátky	k1gFnPc2
byl	být	k5eAaImAgInS
zapsán	zapsat	k5eAaPmNgInS
do	do	k7c2
„	„	k?
<g/>
Rady	rada	k1gFnSc2
moudrých	moudrý	k2eAgInPc2d1
<g/>
“	“	k?
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
nedosahoval	dosahovat	k5eNaImAgMnS
požadovaného	požadovaný	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
1655	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
knězem	kněz	k1gMnSc7
v	v	k7c6
Padově	Padova	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1657	#num#	k4
zachvátila	zachvátit	k5eAaPmAgFnS
Řím	Řím	k1gInSc4
nákaza	nákaza	k1gFnSc1
moru	mor	k1gInSc2
a	a	k8xC
Řehoř	Řehoř	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
povolán	povolán	k2eAgInSc4d1
papežem	papež	k1gMnSc7
Alexandrem	Alexandr	k1gMnSc7
VII	VII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
jejž	jenž	k3xRgInSc4
znal	znát	k5eAaImAgMnS
již	již	k6eAd1
z	z	k7c2
Vestfálské	vestfálský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
<g/>
,	,	kIx,
povolán	povolat	k5eAaPmNgMnS
do	do	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gNnSc4
papež	papež	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
biskupem	biskup	k1gMnSc7
v	v	k7c6
Bergamu	Bergamo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
morové	morový	k2eAgFnSc2d1
epidemie	epidemie	k1gFnSc2
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
pomáhat	pomáhat	k5eAaImF
nemocným	mocný	k2eNgMnSc7d1,k2eAgMnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1660	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
kardinálem	kardinál	k1gMnSc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1664	#num#	k4
jako	jako	k8xS,k8xC
biskup	biskup	k1gMnSc1
přesídlil	přesídlit	k5eAaPmAgMnS
do	do	k7c2
Padovy	Padova	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zde	zde	k6eAd1
během	během	k7c2
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
založil	založit	k5eAaPmAgMnS
seminář	seminář	k1gInSc4
a	a	k8xC
školu	škola	k1gFnSc4
výchovných	výchovný	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
mnohojazyčnou	mnohojazyčný	k2eAgFnSc4d1
tiskárnu	tiskárna	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
což	což	k3yQnSc4,k3yRnSc4
byl	být	k5eAaImAgInS
uznáván	uznávat	k5eAaImNgInS
a	a	k8xC
obdivován	obdivovat	k5eAaImNgInS
mezi	mezi	k7c7
mocnáři	mocnář	k1gMnPc7
<g/>
,	,	kIx,
vědci	vědec	k1gMnPc7
i	i	k8xC
umělci	umělec	k1gMnPc1
<g/>
.	.	kIx.
<g/>
Socha	Socha	k1gMnSc1
svatého	svatý	k2eAgMnSc2d1
Řehoře	Řehoř	k1gMnSc2
Barbariga	Barbarig	k1gMnSc2
v	v	k7c6
kostele	kostel	k1gInSc6
svaté	svatý	k2eAgFnSc2d1
Marie	Maria	k1gFnSc2
Zobenigo	Zobenigo	k6eAd1
v	v	k7c6
BenátkáchVíce	BenátkáchVíce	k1gFnSc6
nežli	nežli	k8xS
biskupovi	biskupův	k2eAgMnPc1d1
se	se	k3xPyFc4
Řehoř	Řehoř	k1gMnSc1
Barbarigo	Barbarigo	k6eAd1
svým	svůj	k3xOyFgInSc7
způsobem	způsob	k1gInSc7
života	život	k1gInSc2
podobal	podobat	k5eAaImAgMnS
kajícímu	kající	k2eAgMnSc3d1
řeholníkovi	řeholník	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Padově	Padova	k1gFnSc6
byl	být	k5eAaImAgInS
pastýřem	pastýř	k1gMnSc7
33	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
během	během	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
byl	být	k5eAaImAgMnS
zároveň	zároveň	k6eAd1
papežským	papežský	k2eAgMnSc7d1
poradcem	poradce	k1gMnSc7
a	a	k8xC
zvolen	zvolit	k5eAaPmNgMnS
kardinálem	kardinál	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
smrti	smrt	k1gFnSc6
papeže	papež	k1gMnSc4
Alexandra	Alexandr	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
šířil	šířit	k5eAaImAgMnS
mezi	mezi	k7c7
kardinály	kardinál	k1gMnPc7
o	o	k7c4
sobě	se	k3xPyFc3
špatnou	špatný	k2eAgFnSc4d1
pověst	pověst	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
sám	sám	k3xTgMnSc1
nechtěl	chtít	k5eNaImAgMnS
převzít	převzít	k5eAaPmF
pontifikát	pontifikát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Papežem	Papež	k1gMnSc7
nakonec	nakonec	k6eAd1
zvolen	zvolen	k2eAgMnSc1d1
nebyl	být	k5eNaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
padovský	padovský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
a	a	k8xC
byl	být	k5eAaImAgMnS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
padovské	padovský	k2eAgFnSc6d1
katedrále	katedrála	k1gFnSc6
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1791	#num#	k4
ho	on	k3xPp3gInSc4
blahořečil	blahořečit	k5eAaImAgMnS
papež	papež	k1gMnSc1
Klement	Klement	k1gMnSc1
XIII	XIII	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
o	o	k7c4
téměř	téměř	k6eAd1
dvě	dva	k4xCgNnPc4
století	století	k1gNnPc4
později	pozdě	k6eAd2
ho	on	k3xPp3gMnSc4
papež	papež	k1gMnSc1
Jan	Jan	k1gMnSc1
XXIII	XXIII	kA
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1960	#num#	k4
svatořečil	svatořečit	k5eAaBmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
SALVADOROVÁ	SALVADOROVÁ	kA
<g/>
,	,	kIx,
Miranda	Mirando	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
cardinals	cardinalsa	k1gFnPc2
of	of	k?
the	the	k?
Holy	hola	k1gFnSc2
Roman	Roman	k1gMnSc1
Church	Church	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Floridan	Floridan	k1gInSc1
International	International	k1gFnSc2
University	universita	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
sv.	sv.	kA
Řehoř	Řehoř	k1gMnSc1
Barbarigo	Barbarigo	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noe	Noe	k1gMnSc1
TV	TV	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
CHLUMSKÝ	chlumský	k2eAgInSc1d1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
sv.	sv.	kA
Řehoř	Řehoř	k1gMnSc1
Barbarigo	Barbarigo	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Catholica	Catholica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
About	About	k1gMnSc1
Our	Our	k1gMnSc1
Patron	patron	k1gMnSc1
Saint	Saint	k1gMnSc1
Gregory	Gregor	k1gMnPc4
Barbarigo	Barbarigo	k1gNnSc4
Archivováno	archivovat	k5eAaBmNgNnS
3	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
St.	st.	kA
Gregory	Gregor	k1gMnPc4
Barbarigo	Barbarigo	k1gMnSc1
church	church	k1gMnSc1
Glasgow	Glasgow	k1gInSc1
-	-	kIx~
official	official	k1gInSc1
site	sit	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Meet	Meet	k1gInSc1
St.	st.	kA
Gregory	Gregor	k1gMnPc7
Barbarigo	Barbarigo	k6eAd1
<g/>
,	,	kIx,
Our	Our	k1gMnSc1
Patron	patron	k1gMnSc1
Saint	Saint	k1gMnSc1
Archivováno	archivován	k2eAgNnSc4d1
3	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
St.	st.	kA
Gregory	Gregor	k1gMnPc4
Barbarigo	Barbarigo	k1gNnSc1
Catholic	Catholice	k1gFnPc2
School	Schoola	k1gFnPc2
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Řehoř	Řehoř	k1gMnSc1
Barbarigo	Barbarigo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Podrobný	podrobný	k2eAgInSc1d1
životopis	životopis	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
119223155	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1022	#num#	k4
2967	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
98016208	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
20486022	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
98016208	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
</s>
