<s>
Svatý	svatý	k2eAgMnSc1d1
Řehoř	Řehoř	k1gMnSc1
Barbarigo	Barbarigo	k1gMnSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1625	#num#	k4
<g/>
,	,	kIx,
Benátky	Benátky	k1gFnPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1697	#num#	k4
Padova	Padova	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
obojí	oboj	k1gFnSc7
tehdy	tehdy	k6eAd1
Benátská	benátský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgFnSc1d1
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
kardinál	kardinál	k1gMnSc1
<g/>
,	,	kIx,
biskup	biskup	k1gMnSc1
<g/>
,	,	kIx,
katolický	katolický	k2eAgMnSc1d1
diplomat	diplomat	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
seminářů	seminář	k1gInPc2
a	a	k8xC
škol	škola	k1gFnPc2
výchovných	výchovný	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
mnohojazyčné	mnohojazyčný	k2eAgFnSc2d1
tiskárny	tiskárna	k1gFnSc2
<g/>
,	,	kIx,
horlivý	horlivý	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
a	a	k8xC
boží	boží	k2eAgMnSc1d1
služebník	služebník	k1gMnSc1
<g/>
.	.	kIx.
</s>