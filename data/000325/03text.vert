<s>
Praslovanština	praslovanština	k1gFnSc1	praslovanština
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
společným	společný	k2eAgInSc7d1	společný
prajazykem	prajazyk	k1gInSc7	prajazyk
dávných	dávný	k2eAgMnPc2d1	dávný
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
ještě	ještě	k9	ještě
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
slovanského	slovanský	k2eAgNnSc2d1	slovanské
stěhování	stěhování	k1gNnSc2	stěhování
z	z	k7c2	z
pravlasti	pravlast	k1gFnSc2	pravlast
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
slovanskými	slovanský	k2eAgInPc7d1	slovanský
kmeny	kmen	k1gInPc7	kmen
však	však	k8xC	však
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
začaly	začít	k5eAaPmAgInP	začít
růst	růst	k5eAaImF	růst
nářeční	nářeční	k2eAgInPc1d1	nářeční
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
jazyková	jazykový	k2eAgFnSc1d1	jazyková
jednota	jednota	k1gFnSc1	jednota
udržovala	udržovat	k5eAaImAgFnS	udržovat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
lze	lze	k6eAd1	lze
klást	klást	k5eAaImF	klást
na	na	k7c4	na
přelom	přelom	k1gInSc4	přelom
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
poslední	poslední	k2eAgFnPc1d1	poslední
jazykové	jazykový	k2eAgFnPc1d1	jazyková
změny	změna	k1gFnPc1	změna
společné	společný	k2eAgFnPc1d1	společná
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
Slovanstvo	Slovanstvo	k1gNnSc4	Slovanstvo
<g/>
.	.	kIx.	.
</s>
<s>
Praslovanština	praslovanština	k1gFnSc1	praslovanština
není	být	k5eNaImIp3nS	být
přímo	přímo	k6eAd1	přímo
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
písemnými	písemný	k2eAgFnPc7d1	písemná
památkami	památka	k1gFnPc7	památka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
metodami	metoda	k1gFnPc7	metoda
historické	historický	k2eAgFnSc2d1	historická
(	(	kIx(	(
<g/>
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
<g/>
)	)	kIx)	)
lingvistiky	lingvistika	k1gFnSc2	lingvistika
-	-	kIx~	-
studiem	studio	k1gNnSc7	studio
hláskových	hláskový	k2eAgInPc2d1	hláskový
<g/>
,	,	kIx,	,
morfologických	morfologický	k2eAgInPc2d1	morfologický
a	a	k8xC	a
lexikálních	lexikální	k2eAgFnPc2d1	lexikální
změn	změna	k1gFnPc2	změna
jazyků	jazyk	k1gInPc2	jazyk
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vzešlých	vzešlý	k2eAgInPc2d1	vzešlý
a	a	k8xC	a
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
mohly	moct	k5eAaImAgInP	moct
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
(	(	kIx(	(
<g/>
germánských	germánský	k2eAgFnPc2d1	germánská
<g/>
,	,	kIx,	,
keltských	keltský	k2eAgFnPc2d1	keltská
<g/>
,	,	kIx,	,
íránských	íránský	k2eAgFnPc2d1	íránská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
písemné	písemný	k2eAgNnSc4d1	písemné
zachycení	zachycení	k1gNnSc4	zachycení
pozdního	pozdní	k2eAgNnSc2d1	pozdní
stádia	stádium	k1gNnSc2	stádium
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
lze	lze	k6eAd1	lze
však	však	k9	však
považovat	považovat	k5eAaImF	považovat
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
rysů	rys	k1gInPc2	rys
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
jazyka	jazyk	k1gInSc2	jazyk
shodných	shodný	k2eAgInPc2d1	shodný
nebo	nebo	k8xC	nebo
velice	velice	k6eAd1	velice
blízkých	blízký	k2eAgInPc2d1	blízký
rysům	rys	k1gInPc3	rys
praslovanštiny	praslovanština	k1gFnPc1	praslovanština
získaným	získaný	k2eAgInSc7d1	získaný
lingvistickou	lingvistický	k2eAgFnSc7d1	lingvistická
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
větší	veliký	k2eAgFnSc7d2	veliký
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
utvořena	utvořit	k5eAaPmNgFnS	utvořit
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
uměle	uměle	k6eAd1	uměle
nápodobou	nápodoba	k1gFnSc7	nápodoba
vyspělejších	vyspělý	k2eAgInPc2d2	vyspělejší
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
především	především	k9	především
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
)	)	kIx)	)
a	a	k8xC	a
vymyká	vymykat	k5eAaImIp3nS	vymykat
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
soudobému	soudobý	k2eAgInSc3d1	soudobý
stavu	stav	k1gInSc3	stav
skladby	skladba	k1gFnSc2	skladba
v	v	k7c6	v
praslovanštině	praslovanština	k1gFnSc6	praslovanština
<g/>
.	.	kIx.	.
</s>
<s>
Praslovanština	praslovanština	k1gFnSc1	praslovanština
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
satemové	satemový	k2eAgFnSc3d1	satemový
skupině	skupina	k1gFnSc3	skupina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jazyky	jazyk	k1gInPc7	jazyk
indoíránskými	indoíránský	k2eAgInPc7d1	indoíránský
<g/>
,	,	kIx,	,
s	s	k7c7	s
arménštinou	arménština	k1gFnSc7	arménština
<g/>
,	,	kIx,	,
albánštinou	albánština	k1gFnSc7	albánština
a	a	k8xC	a
jazyky	jazyk	k1gInPc7	jazyk
baltskými	baltský	k2eAgInPc7d1	baltský
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
poslední	poslední	k2eAgFnSc7d1	poslední
skupinou	skupina	k1gFnSc7	skupina
jazyků	jazyk	k1gInPc2	jazyk
má	mít	k5eAaImIp3nS	mít
praslovanština	praslovanština	k1gFnSc1	praslovanština
nejužší	úzký	k2eAgFnSc2d3	nejužší
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
jazykovědě	jazykověda	k1gFnSc6	jazykověda
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
baltoslovanské	baltoslovanský	k2eAgFnSc6d1	baltoslovanská
jednotě	jednota	k1gFnSc6	jednota
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
rozpadem	rozpad	k1gInSc7	rozpad
měla	mít	k5eAaImAgFnS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
jazyková	jazykový	k2eAgFnSc1d1	jazyková
skupina	skupina	k1gFnSc1	skupina
baltská	baltský	k2eAgFnSc1d1	Baltská
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
samostatného	samostatný	k2eAgInSc2d1	samostatný
vývoje	vývoj	k1gInSc2	vývoj
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
se	se	k3xPyFc4	se
kladou	klást	k5eAaImIp3nP	klást
do	do	k7c2	do
širokého	široký	k2eAgNnSc2d1	široké
rozmezí	rozmezí	k1gNnSc2	rozmezí
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
700	[number]	k4	700
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
umísťován	umísťovat	k5eAaImNgInS	umísťovat
již	již	k6eAd1	již
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Praslovanština	praslovanština	k1gFnSc1	praslovanština
disponovala	disponovat	k5eAaBmAgFnS	disponovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
pozdní	pozdní	k2eAgFnSc6d1	pozdní
fázi	fáze	k1gFnSc6	fáze
11	[number]	k4	11
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jerů	jer	k1gInPc2	jer
<g/>
)	)	kIx)	)
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
i	i	k8xC	i
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
slavistice	slavistika	k1gFnSc6	slavistika
znázorňovány	znázorňovat	k5eAaImNgInP	znázorňovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
i	i	k9	i
<g/>
,	,	kIx,	,
ь	ь	k?	ь
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
ę	ę	k?	ę
<g/>
,	,	kIx,	,
ě	ě	k?	ě
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
ǫ	ǫ	k?	ǫ
<g/>
,	,	kIx,	,
ъ	ъ	k?	ъ
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
u.	u.	k?	u.
ь	ь	k?	ь
<g/>
,	,	kIx,	,
ъ	ъ	k?	ъ
-	-	kIx~	-
jery	jery	k1gNnSc1	jery
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc2d1	krátká
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
redukované	redukovaný	k2eAgFnPc4d1	redukovaná
<g/>
)	)	kIx)	)
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
polosamohlásky	polosamohláska	k1gFnPc1	polosamohláska
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
[	[	kIx(	[
<g/>
ɪ	ɪ	k?	ɪ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
[	[	kIx(	[
<g/>
ɯ	ɯ	k?	ɯ
<g/>
]	]	kIx)	]
ę	ę	k?	ę
<g/>
,	,	kIx,	,
ǫ	ǫ	k?	ǫ
-	-	kIx~	-
přední	přední	k2eAgFnSc2d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
nosové	nosový	k2eAgFnSc2d1	nosová
samohlásky	samohláska	k1gFnSc2	samohláska
ě	ě	k?	ě
-	-	kIx~	-
jať	jať	k1gNnSc1	jať
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
[	[	kIx(	[
<g/>
æ	æ	k?	æ
<g/>
]	]	kIx)	]
y	y	k?	y
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
[	[	kIx(	[
<g/>
ɨ	ɨ	k?	ɨ
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
ɯ	ɯ	k?	ɯ
<g/>
]	]	kIx)	]
existence	existence	k1gFnSc1	existence
slabikotvorných	slabikotvorný	k2eAgFnPc2d1	slabikotvorný
souhlásek	souhláska	k1gFnPc2	souhláska
l	l	kA	l
<g/>
̥	̥	k?	̥
<g/>
,	,	kIx,	,
ĺ	ĺ	k?	ĺ
<g/>
̥	̥	k?	̥
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
̥	̥	k?	̥
<g/>
,	,	kIx,	,
ŕ	ŕ	k?	ŕ
<g/>
̥	̥	k?	̥
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
praslovanštině	praslovanština	k1gFnSc6	praslovanština
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
někteří	některý	k3yIgMnPc1	některý
jazykovědci	jazykovědec	k1gMnPc1	jazykovědec
skupiny	skupina	k1gFnSc2	skupina
ъ	ъ	k?	ъ
<g/>
,	,	kIx,	,
ь	ь	k?	ь
<g/>
,	,	kIx,	,
ъ	ъ	k?	ъ
<g/>
,	,	kIx,	,
ь	ь	k?	ь
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
pozdní	pozdní	k2eAgFnSc2d1	pozdní
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
následující	následující	k2eAgFnSc7d1	následující
tabulkou	tabulka	k1gFnSc7	tabulka
<g/>
:	:	kIx,	:
párový	párový	k2eAgInSc1d1	párový
zápis	zápis	k1gInSc1	zápis
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
vždy	vždy	k6eAd1	vždy
označuje	označovat	k5eAaImIp3nS	označovat
neznělou	znělý	k2eNgFnSc4d1	neznělá
(	(	kIx(	(
<g/>
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
a	a	k8xC	a
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
znělou	znělý	k2eAgFnSc4d1	znělá
souhlásku	souhláska	k1gFnSc4	souhláska
c	c	k0	c
představuje	představovat	k5eAaImIp3nS	představovat
neznělou	znělý	k2eNgFnSc4d1	neznělá
afrikátu	afrikáta	k1gFnSc4	afrikáta
[	[	kIx(	[
<g/>
ʦ	ʦ	k?	ʦ
<g/>
]	]	kIx)	]
ʒ	ʒ	k?	ʒ
představuje	představovat	k5eAaImIp3nS	představovat
znělou	znělý	k2eAgFnSc4d1	znělá
afrikátu	afrikáta	k1gFnSc4	afrikáta
[	[	kIx(	[
<g/>
ʣ	ʣ	k?	ʣ
<g/>
]	]	kIx)	]
x	x	k?	x
představuje	představovat	k5eAaImIp3nS	představovat
neznělou	znělý	k2eNgFnSc4d1	neznělá
velární	velární	k2eAgFnSc4d1	velární
frikativu	frikativa	k1gFnSc4	frikativa
[	[	kIx(	[
<g/>
<g />
.	.	kIx.	.
</s>
<s>
x	x	k?	x
<g/>
]	]	kIx)	]
š	š	k?	š
<g/>
,	,	kIx,	,
č	č	k0	č
<g/>
,	,	kIx,	,
ž	ž	k?	ž
a	a	k8xC	a
ǯ	ǯ	k?	ǯ
představují	představovat	k5eAaImIp3nP	představovat
hlásky	hlásek	k1gInPc1	hlásek
[	[	kIx(	[
<g/>
ʃ	ʃ	k?	ʃ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ʧ	ʧ	k?	ʧ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ʒ	ʒ	k?	ʒ
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
ʤ	ʤ	k?	ʤ
<g/>
]	]	kIx)	]
'	'	kIx"	'
představuje	představovat	k5eAaImIp3nS	představovat
měkčící	měkčící	k2eAgNnSc4d1	měkčící
znaménko	znaménko	k1gNnSc4	znaménko
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
změkčení	změkčení	k1gNnSc2	změkčení
předchozí	předchozí	k2eAgFnSc2d1	předchozí
hlásky	hláska	k1gFnSc2	hláska
Ve	v	k7c6	v
slavistice	slavistika	k1gFnSc6	slavistika
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
transkripce	transkripce	k1gFnSc1	transkripce
praslovanských	praslovanský	k2eAgFnPc2d1	praslovanská
hlásek	hláska	k1gFnPc2	hláska
(	(	kIx(	(
<g/>
a	a	k8xC	a
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
představována	představovat	k5eAaImNgFnS	představovat
výše	vysoce	k6eAd2	vysoce
použitými	použitý	k2eAgInPc7d1	použitý
symboly	symbol	k1gInPc7	symbol
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
odpovídajícími	odpovídající	k2eAgInPc7d1	odpovídající
symboly	symbol	k1gInPc7	symbol
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
fonetické	fonetický	k2eAgFnSc6d1	fonetická
abecedě	abeceda	k1gFnSc6	abeceda
(	(	kIx(	(
<g/>
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Časování	časování	k1gNnSc2	časování
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
Praslovanská	praslovanský	k2eAgNnPc1d1	praslovanské
slovesa	sloveso	k1gNnPc1	sloveso
zdědila	zdědit	k5eAaPmAgNnP	zdědit
z	z	k7c2	z
praindoevropského	praindoevropský	k2eAgInSc2d1	praindoevropský
jazyka	jazyk	k1gInSc2	jazyk
3	[number]	k4	3
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Odvozené	odvozený	k2eAgInPc1d1	odvozený
slovesné	slovesný	k2eAgInPc1d1	slovesný
tvary	tvar	k1gInPc1	tvar
(	(	kIx(	(
<g/>
participia	participium	k1gNnPc1	participium
<g/>
)	)	kIx)	)
rozlišovaly	rozlišovat	k5eAaImAgInP	rozlišovat
tři	tři	k4xCgNnPc1	tři
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
singulár	singulár	k1gInSc1	singulár
<g/>
,	,	kIx,	,
duál	duál	k1gInSc1	duál
a	a	k8xC	a
plurál	plurál	k1gInSc1	plurál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
(	(	kIx(	(
<g/>
syntetické	syntetický	k2eAgInPc4d1	syntetický
<g/>
)	)	kIx)	)
minulé	minulý	k2eAgInPc4d1	minulý
časy	čas	k1gInPc4	čas
<g/>
:	:	kIx,	:
aorist	aorist	k1gInSc4	aorist
(	(	kIx(	(
<g/>
vyprávěcí	vyprávěcí	k2eAgInSc4d1	vyprávěcí
neprůvodní	průvodní	k2eNgInSc4d1	průvodní
děj	děj	k1gInSc4	děj
<g/>
)	)	kIx)	)
-	-	kIx~	-
asigmatický	asigmatický	k2eAgMnSc1d1	asigmatický
<g/>
,	,	kIx,	,
sigmatický	sigmatický	k2eAgMnSc1d1	sigmatický
kratší	krátký	k2eAgMnSc1d2	kratší
<g/>
,	,	kIx,	,
sigmatický	sigmatický	k2eAgMnSc1d1	sigmatický
delší	dlouhý	k2eAgNnSc4d2	delší
imperfektum	imperfektum	k1gNnSc4	imperfektum
-	-	kIx~	-
praslovanský	praslovanský	k2eAgInSc1d1	praslovanský
novotvar	novotvar	k1gInSc1	novotvar
od	od	k7c2	od
aoristu	aorist	k1gInSc2	aorist
-	-	kIx~	-
od	od	k7c2	od
nedokonavých	dokonavý	k2eNgNnPc2d1	nedokonavé
sloves	sloveso	k1gNnPc2	sloveso
Složené	složený	k2eAgFnPc1d1	složená
(	(	kIx(	(
<g/>
analytické	analytický	k2eAgFnPc1d1	analytická
<g/>
)	)	kIx)	)
minulé	minulý	k2eAgInPc4d1	minulý
časy	čas	k1gInPc4	čas
<g/>
:	:	kIx,	:
perfektum	perfektum	k1gNnSc1	perfektum
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
přítomný	přítomný	k1gMnSc1	přítomný
s	s	k7c7	s
minulým	minulý	k2eAgNnSc7d1	Minulé
příčestím	příčestí	k1gNnSc7	příčestí
<g/>
)	)	kIx)	)
plusquamperfektum	plusquamperfektum	k1gNnSc1	plusquamperfektum
(	(	kIx(	(
<g/>
participium	participium	k1gNnSc1	participium
+	+	kIx~	+
imperfektum	imperfektum	k1gNnSc1	imperfektum
či	či	k8xC	či
perfektum	perfektum	k1gNnSc1	perfektum
<g/>
)	)	kIx)	)
Přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
prézens	prézens	k1gInSc1	prézens
gnómický	gnómický	k2eAgInSc1d1	gnómický
-	-	kIx~	-
obecná	obecná	k1gFnSc1	obecná
pravda	pravda	k1gFnSc1	pravda
bez	bez	k7c2	bez
časového	časový	k2eAgNnSc2d1	časové
ohraničení	ohraničení	k1gNnSc2	ohraničení
prézens	prézens	k1gInSc1	prézens
historický	historický	k2eAgInSc1d1	historický
-	-	kIx~	-
minulá	minulý	k2eAgFnSc1d1	minulá
událost	událost	k1gFnSc1	událost
jako	jako	k9	jako
přítomnost	přítomnost	k1gFnSc4	přítomnost
Složený	složený	k2eAgInSc4d1	složený
budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
vznikal	vznikat	k5eAaImAgInS	vznikat
až	až	k9	až
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
fázích	fáze	k1gFnPc6	fáze
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
postoj	postoj	k1gInSc1	postoj
mluvčího	mluvčí	k1gMnSc2	mluvčí
indikativ	indikativ	k1gInSc4	indikativ
(	(	kIx(	(
<g/>
oznamovací	oznamovací	k2eAgFnPc4d1	oznamovací
<g/>
)	)	kIx)	)
z	z	k7c2	z
indoevropského	indoevropský	k2eAgInSc2d1	indoevropský
indikativu	indikativ	k1gInSc2	indikativ
imperativ	imperativ	k1gInSc1	imperativ
(	(	kIx(	(
<g/>
rozkazovací	rozkazovací	k2eAgFnSc1d1	rozkazovací
<g/>
)	)	kIx)	)
z	z	k7c2	z
indoevropského	indoevropský	k2eAgInSc2d1	indoevropský
přacího	přací	k2eAgInSc2d1	přací
způsobu	způsob	k1gInSc2	způsob
-	-	kIx~	-
tematický	tematický	k2eAgInSc1d1	tematický
<g/>
,	,	kIx,	,
atematický	atematický	k2eAgInSc1d1	atematický
kondicionál	kondicionál	k1gInSc1	kondicionál
(	(	kIx(	(
<g/>
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
<g/>
)	)	kIx)	)
-	-	kIx~	-
praslovanský	praslovanský	k2eAgInSc1d1	praslovanský
novotvar	novotvar	k1gInSc1	novotvar
z	z	k7c2	z
indoevropského	indoevropský	k2eAgInSc2d1	indoevropský
konjunktivu	konjunktiv	k1gInSc2	konjunktiv
Vid	vid	k1gInSc1	vid
se	se	k3xPyFc4	se
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
v	v	k7c6	v
praslovanštině	praslovanština	k1gFnSc6	praslovanština
teprve	teprve	k6eAd1	teprve
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Nedokonavost	nedokonavost	k1gFnSc1	nedokonavost
mohla	moct	k5eAaImAgFnS	moct
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
i	i	k9	i
u	u	k7c2	u
slovesa	sloveso	k1gNnSc2	sloveso
opětovná	opětovný	k2eAgNnPc4d1	opětovné
(	(	kIx(	(
<g/>
iterativa	iterativum	k1gNnPc4	iterativum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
činný	činný	k2eAgMnSc1d1	činný
(	(	kIx(	(
<g/>
aktivum	aktivum	k1gNnSc1	aktivum
<g/>
)	)	kIx)	)
trpný	trpný	k2eAgMnSc1d1	trpný
(	(	kIx(	(
<g/>
pasivum	pasivum	k1gNnSc1	pasivum
<g/>
)	)	kIx)	)
medium	medium	k1gNnSc1	medium
(	(	kIx(	(
<g/>
médium	médium	k1gNnSc1	médium
<g/>
)	)	kIx)	)
-	-	kIx~	-
agens	agens	k1gInSc1	agens
děje	děj	k1gInSc2	děj
je	být	k5eAaImIp3nS	být
činností	činnost	k1gFnSc7	činnost
zároveň	zároveň	k6eAd1	zároveň
zasahován	zasahován	k2eAgMnSc1d1	zasahován
Do	do	k7c2	do
historických	historický	k2eAgFnPc2d1	historická
fází	fáze	k1gFnPc2	fáze
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
médium	médium	k1gNnSc1	médium
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
<g/>
.	.	kIx.	.
infinitivní	infinitivní	k2eAgInSc4d1	infinitivní
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
minulý	minulý	k2eAgMnSc1d1	minulý
-0	-0	k4	-0
<g/>
,	,	kIx,	,
-i	-i	k?	-i
<g/>
,	,	kIx,	,
-ě	-ě	k?	-ě
<g/>
,	,	kIx,	,
-a	-a	k?	-a
<g/>
,	,	kIx,	,
-ova	-ova	k1gMnSc1	-ova
<g/>
,	,	kIx,	,
-nó	-nó	k?	-nó
přítomný	přítomný	k2eAgInSc1d1	přítomný
-i	-i	k?	-i
<g/>
,	,	kIx,	,
-e	-e	k?	-e
<g/>
,	,	kIx,	,
-ne	-ne	k?	-ne
<g/>
,	,	kIx,	,
-je	-je	k?	-je
Atematická	atematický	k2eAgNnPc1d1	atematický
slovesa	sloveso	k1gNnPc1	sloveso
nemají	mít	k5eNaImIp3nP	mít
prézentní	prézentní	k2eAgFnSc4d1	prézentní
kmenotvornou	kmenotvorný	k2eAgFnSc4d1	kmenotvorná
příponu	přípona	k1gFnSc4	přípona
<g/>
.	.	kIx.	.
infinitiv	infinitiv	k1gInSc4	infinitiv
-	-	kIx~	-
baltoslovanská	baltoslovanský	k2eAgFnSc1d1	baltoslovanská
koncovka	koncovka	k1gFnSc1	koncovka
-ti	-ti	k?	-ti
(	(	kIx(	(
<g/>
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
indoevropských	indoevropský	k2eAgInPc6d1	indoevropský
jazycích	jazyk	k1gInPc6	jazyk
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
tvoření	tvoření	k1gNnSc1	tvoření
<g/>
)	)	kIx)	)
supinum	supinum	k1gNnSc1	supinum
-	-	kIx~	-
praslovanská	praslovanský	k2eAgFnSc1d1	praslovanská
koncovka	koncovka	k1gFnSc1	koncovka
-tъ	-tъ	k?	-tъ
-	-	kIx~	-
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
pohybu	pohyb	k1gInSc2	pohyb
či	či	k8xC	či
vybídnutí	vybídnutí	k1gNnSc4	vybídnutí
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
→	→	k?	→
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
příslovečného	příslovečný	k2eAgNnSc2d1	příslovečné
určení	určení	k1gNnSc2	určení
účelu	účel	k1gInSc2	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
od	od	k7c2	od
dokonavých	dokonavý	k2eAgFnPc2d1	dokonavá
i	i	k8xC	i
nedokonavých	dokonavý	k2eNgFnPc2d1	nedokonavá
participium	participium	k1gNnSc4	participium
minulé	minulý	k2eAgFnSc2d1	minulá
činné	činný	k2eAgFnSc2d1	činná
(	(	kIx(	(
<g/>
od	od	k7c2	od
stejného	stejný	k2eAgNnSc2d1	stejné
participia	participium	k1gNnSc2	participium
v	v	k7c6	v
ie.	ie.	k?	ie.
<g/>
)	)	kIx)	)
přítomné	přítomný	k2eAgNnSc1d1	přítomné
činné	činný	k2eAgNnSc1d1	činné
(	(	kIx(	(
<g/>
od	od	k7c2	od
stejného	stejné	k1gNnSc2	stejné
v	v	k7c6	v
ie.	ie.	k?	ie.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
v	v	k7c6	v
sl.	sl.	k?	sl.
→	→	k?	→
přechodníky	přechodník	k1gInPc1	přechodník
přítomné	přítomný	k2eAgInPc1d1	přítomný
a	a	k8xC	a
zpřídavnělé	zpřídavnělý	k2eAgInPc1d1	zpřídavnělý
<g/>
)	)	kIx)	)
minulé	minulý	k2eAgFnPc1d1	minulá
trpné	trpný	k2eAgFnPc1d1	trpná
přítomné	přítomný	k2eAgFnPc1d1	přítomná
trpné	trpný	k2eAgFnPc1d1	trpná
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Skloňování	skloňování	k1gNnSc2	skloňování
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
Praslovanština	praslovanština	k1gFnSc1	praslovanština
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
staroslověnské	staroslověnský	k2eAgFnSc6d1	staroslověnská
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
dnešní	dnešní	k2eAgFnSc1d1	dnešní
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
disponovala	disponovat	k5eAaBmAgFnS	disponovat
7	[number]	k4	7
pády	pád	k1gInPc7	pád
(	(	kIx(	(
<g/>
nominativem	nominativ	k1gInSc7	nominativ
<g/>
,	,	kIx,	,
genitivem	genitiv	k1gInSc7	genitiv
<g/>
,	,	kIx,	,
dativem	dativ	k1gInSc7	dativ
<g/>
,	,	kIx,	,
akuzativem	akuzativ	k1gInSc7	akuzativ
<g/>
,	,	kIx,	,
vokativem	vokativ	k1gInSc7	vokativ
<g/>
,	,	kIx,	,
lokálem	lokál	k1gInSc7	lokál
a	a	k8xC	a
instrumentálem	instrumentál	k1gInSc7	instrumentál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
rozlišovala	rozlišovat	k5eAaImAgFnS	rozlišovat
jednotné	jednotný	k2eAgInPc4d1	jednotný
<g/>
,	,	kIx,	,
dvojné	dvojný	k2eAgInPc4d1	dvojný
a	a	k8xC	a
množné	množný	k2eAgInPc4d1	množný
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc4	rod
rozlišovala	rozlišovat	k5eAaImAgFnS	rozlišovat
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
,	,	kIx,	,
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
praslovanského	praslovanský	k2eAgNnSc2d1	praslovanské
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
u	u	k7c2	u
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
životnost	životnost	k1gFnSc1	životnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praslovanštině	praslovanština	k1gFnSc6	praslovanština
se	se	k3xPyFc4	se
skloňovalo	skloňovat	k5eAaImAgNnS	skloňovat
dle	dle	k7c2	dle
zakončení	zakončení	k1gNnSc2	zakončení
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějších	pozdní	k2eAgInPc6d2	pozdější
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
převládlo	převládnout	k5eAaPmAgNnS	převládnout
skloňování	skloňování	k1gNnSc1	skloňování
podle	podle	k7c2	podle
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
rodový	rodový	k2eAgInSc1d1	rodový
princip	princip	k1gInSc1	princip
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Deklinační	deklinační	k2eAgInPc1d1	deklinační
typy	typ	k1gInPc1	typ
(	(	kIx(	(
<g/>
kmeny	kmen	k1gInPc1	kmen
<g/>
)	)	kIx)	)
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
rodů	rod	k1gInPc2	rod
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
mužský	mužský	k2eAgInSc4d1	mužský
<g/>
:	:	kIx,	:
-u	-u	k?	-u
<g/>
,	,	kIx,	,
-t	-t	k?	-t
mužský	mužský	k2eAgInSc4d1	mužský
i	i	k8xC	i
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
:	:	kIx,	:
-i	-i	k?	-i
<g/>
,	,	kIx,	,
-a	-a	k?	-a
<g/>
,	,	kIx,	,
-ja	-ja	k?	-ja
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
:	:	kIx,	:
-o	-o	k?	-o
<g/>
,	,	kIx,	,
-jo	-jo	k?	-jo
<g/>
,	,	kIx,	,
-n	-n	k?	-n
střední	střední	k1gMnSc1	střední
<g/>
:	:	kIx,	:
-nt	-nt	k?	-nt
<g/>
,	,	kIx,	,
-s	-s	k?	-s
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
:	:	kIx,	:
-r	-r	k?	-r
<g/>
,	,	kIx,	,
-ъ	-ъ	k?	-ъ
-ъ	-ъ	k?	-ъ
(	(	kIx(	(
<g/>
-ú	-ú	k?	-ú
<g/>
)	)	kIx)	)
-n	-n	k?	-n
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
i	i	k8xC	i
střední	střední	k2eAgInSc4d1	střední
<g/>
)	)	kIx)	)
-nt	-nt	k?	-nt
-r	-r	k?	-r
-t	-t	k?	-t
-s	-s	k?	-s
<s>
</s>
V	v	k7c6	v
praslovanštině	praslovanština	k1gFnSc6	praslovanština
jsou	být	k5eAaImIp3nP	být
původní	původní	k2eAgFnPc4d1	původní
starobylé	starobylý	k2eAgFnPc4d1	starobylá
koncovky	koncovka	k1gFnPc4	koncovka
jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
pádech	pád	k1gInPc6	pád
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
jmenné	jmenný	k2eAgFnSc2d1	jmenná
deklinace	deklinace	k1gFnSc2	deklinace
(	(	kIx(	(
<g/>
shodné	shodný	k2eAgFnPc1d1	shodná
s	s	k7c7	s
podstatnými	podstatný	k2eAgNnPc7d1	podstatné
jmény	jméno	k1gNnPc7	jméno
<g/>
)	)	kIx)	)
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
složená	složený	k2eAgFnSc1d1	složená
deklinace	deklinace	k1gFnSc1	deklinace
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
praslovanskou	praslovanský	k2eAgFnSc4d1	praslovanská
inovaci	inovace	k1gFnSc4	inovace
<g/>
)	)	kIx)	)
připojením	připojení	k1gNnSc7	připojení
zájmene	zájmenout	k5eAaPmIp3nS	zájmenout
jь	jь	k?	jь
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ja	ja	k?	ja
(	(	kIx(	(
<g/>
ona	onen	k3xDgFnSc1	onen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
ono	onen	k3xDgNnSc1	onen
<g/>
)	)	kIx)	)
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
v	v	k7c6	v
postpozici	postpozice	k1gFnSc6	postpozice
v	v	k7c6	v
determinanční	determinanční	k2eAgFnSc6d1	determinanční
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Stupňování	stupňování	k1gNnSc1	stupňování
0	[number]	k4	0
m.	m.	k?	m.
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
ii	ii	k?	ii
<g/>
,	,	kIx,	,
f.	f.	k?	f.
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
iši	iši	k?	iši
<g/>
,	,	kIx,	,
n.	n.	k?	n.
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
m.	m.	k?	m.
<g/>
:	:	kIx,	:
ěi	ěi	k?	ěi
<g/>
,	,	kIx,	,
f.	f.	k?	f.
<g/>
:	:	kIx,	:
ějši	ějš	k1gFnSc2	ějš
<g/>
,	,	kIx,	,
n.	n.	k?	n.
<g/>
:	:	kIx,	:
ěje	ěje	k?	ěje
Bezrodá	bezrodý	k2eAgNnPc1d1	bezrodý
zájmena	zájmeno	k1gNnPc1	zájmeno
(	(	kIx(	(
<g/>
osobní	osobní	k2eAgNnPc1d1	osobní
<g/>
,	,	kIx,	,
tázací	tázací	k2eAgNnPc1d1	tázací
<g/>
,	,	kIx,	,
zvratné	zvratný	k2eAgNnSc1d1	zvratné
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
skloňování	skloňování	k1gNnSc4	skloňování
<g/>
.	.	kIx.	.
sь	sь	k?	sь
-	-	kIx~	-
dle	dle	k7c2	dle
"	"	kIx"	"
<g/>
tento	tento	k3xDgInSc1	tento
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
koncovky	koncovka	k1gFnPc1	koncovka
-i	-i	k?	-i
Zájmena	zájmeno	k1gNnSc2	zájmeno
rozlišující	rozlišující	k2eAgInSc4d1	rozlišující
rod	rod	k1gInSc4	rod
<g/>
:	:	kIx,	:
tъ	tъ	k?	tъ
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ta	k0	ta
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jь	jь	k?	jь
<g/>
,	,	kIx,	,
ja	ja	k?	ja
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
ono	onen	k3xDgNnSc4	onen
<g/>
)	)	kIx)	)
kyjь	kyjь	k?	kyjь
<g/>
,	,	kIx,	,
kaja	kaja	k1gMnSc1	kaja
<g/>
,	,	kIx,	,
koje	kojit	k5eAaImSgMnS	kojit
(	(	kIx(	(
<g/>
jaký	jaký	k3yIgMnSc1	jaký
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc4	jaký
<g/>
)	)	kIx)	)
Různé	různý	k2eAgFnPc1d1	různá
číslovky	číslovka	k1gFnPc1	číslovka
podléhají	podléhat	k5eAaImIp3nP	podléhat
deklinaci	deklinace	k1gFnSc3	deklinace
jmenné	jmenný	k2eAgFnSc3d1	jmenná
<g/>
,	,	kIx,	,
zájmenné	zájmenný	k2eAgFnSc3d1	zájmenná
i	i	k8xC	i
složené	složený	k2eAgFnSc3d1	složená
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
číslovky	číslovka	k1gFnPc1	číslovka
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
-	-	kIx~	-
původně	původně	k6eAd1	původně
číselná	číselný	k2eAgNnPc4d1	číselné
substantiva	substantivum	k1gNnPc4	substantivum
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
kmen	kmen	k1gInSc1	kmen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
jedinъ	jedinъ	k?	jedinъ
,	,	kIx,	,
jedina	jedina	k1gFnSc1	jedina
<g/>
,	,	kIx,	,
jedino	jedino	k1gNnSc1	jedino
-	-	kIx~	-
tъ	tъ	k?	tъ
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
2	[number]	k4	2
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
dъ	dъ	k?	dъ
<g/>
,	,	kIx,	,
dъ	dъ	k?	dъ
<g/>
,	,	kIx,	,
dъ	dъ	k?	dъ
3	[number]	k4	3
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
trъ	trъ	k?	trъ
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
tri	tri	k?	tri
<g/>
,	,	kIx,	,
tri	tri	k?	tri
-	-	kIx~	-
pl.	pl.	k?	pl.
=	=	kIx~	=
"	"	kIx"	"
<g/>
-i	-i	k?	-i
<g/>
"	"	kIx"	"
4	[number]	k4	4
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
četyre	četyr	k1gMnSc5	četyr
<g/>
,	,	kIx,	,
četyri	četyr	k1gMnSc5	četyr
<g/>
,	,	kIx,	,
četyri	četyri	k6eAd1	četyri
-	-	kIx~	-
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
-i	-i	k?	-i
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
5	[number]	k4	5
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
pěť	pěť	k?	pěť
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
-i	-i	k?	-i
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
6	[number]	k4	6
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
šesť	šesť	k1gFnSc1	šesť
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
-i	-i	k?	-i
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
...	...	k?	...
100	[number]	k4	100
-	-	kIx~	-
sъ	sъ	k?	sъ
-	-	kIx~	-
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
-o	-o	k?	-o
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1000	[number]	k4	1000
-	-	kIx~	-
tisošti	tisoštit	k5eAaPmRp2nS	tisoštit
(	(	kIx(	(
<g/>
tisešti	tiseštit	k5eAaPmRp2nS	tiseštit
<g/>
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
-ja	-ja	k?	-ja
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Řadové	řadový	k2eAgFnPc1d1	řadová
číslovky	číslovka	k1gFnPc1	číslovka
se	se	k3xPyFc4	se
skloňovaly	skloňovat	k5eAaImAgFnP	skloňovat
jako	jako	k8xS	jako
adjektiva	adjektivum	k1gNnSc2	adjektivum
složené	složený	k2eAgFnSc2d1	složená
deklinace	deklinace	k1gFnSc2	deklinace
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
tretijь	tretijь	k?	tretijь
-	-	kIx~	-
měkké	měkký	k2eAgNnSc1d1	měkké
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
-ь	-ь	k?	-ь
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
-	-	kIx~	-
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
se	se	k3xPyFc4	se
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druh	k1gMnPc1	druh
číslovky	číslovka	k1gFnSc2	číslovka
podléhají	podléhat	k5eAaImIp3nP	podléhat
měkké	měkký	k2eAgFnSc3d1	měkká
složené	složený	k2eAgFnSc3d1	složená
deklinaci	deklinace	k1gFnSc3	deklinace
(	(	kIx(	(
<g/>
utvořené	utvořený	k2eAgInPc1d1	utvořený
jsou	být	k5eAaImIp3nP	být
připojením	připojení	k1gNnSc7	připojení
zájmene	zájmenout	k5eAaPmIp3nS	zájmenout
jь	jь	k?	jь
<g/>
,	,	kIx,	,
ja	ja	k?	ja
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dъ	dъ	k?	dъ
<g/>
,	,	kIx,	,
dъ	dъ	k?	dъ
<g/>
,	,	kIx,	,
dъ	dъ	k?	dъ
obojь	obojь	k?	obojь
<g/>
,	,	kIx,	,
oboja	oboja	k1gMnSc1	oboja
<g/>
,	,	kIx,	,
obojo	obojo	k1gMnSc1	obojo
trojь	trojь	k?	trojь
<g/>
,	,	kIx,	,
troja	troja	k1gMnSc1	troja
<g/>
,	,	kIx,	,
trojo	trojo	k1gMnSc1	trojo
Vyšší	vysoký	k2eAgFnSc2d2	vyšší
druhové	druhový	k2eAgFnSc2d1	druhová
číslovky	číslovka	k1gFnSc2	číslovka
četvorь	četvorь	k?	četvorь
<g/>
,	,	kIx,	,
četvora	četvora	k1gFnSc1	četvora
<g/>
,	,	kIx,	,
četvoro	četvora	k1gFnSc5	četvora
-	-	kIx~	-
jako	jako	k9	jako
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
adj.	adj.	k?	adj.
jm	jm	k?	jm
<g/>
.	.	kIx.	.
tvaru	tvar	k1gInSc2	tvar
Většina	většina	k1gFnSc1	většina
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
dnešních	dnešní	k2eAgInPc2d1	dnešní
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
přejímek	přejímka	k1gFnPc2	přejímka
z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
nová	nový	k2eAgNnPc1d1	nové
slova	slovo	k1gNnPc1	slovo
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
slov	slovo	k1gNnPc2	slovo
zděděných	zděděný	k2eAgInPc2d1	zděděný
ze	z	k7c2	z
slovanského	slovanský	k2eAgInSc2d1	slovanský
prajazyka	prajazyk	k1gInSc2	prajazyk
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
do	do	k7c2	do
některých	některý	k3yIgInPc2	některý
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
změnila	změnit	k5eAaPmAgNnP	změnit
vybraná	vybraný	k2eAgNnPc1d1	vybrané
slova	slovo	k1gNnPc1	slovo
praslovanštiny	praslovanština	k1gFnSc2	praslovanština
(	(	kIx(	(
<g/>
praslovanský	praslovanský	k2eAgInSc4d1	praslovanský
tvar	tvar	k1gInSc4	tvar
jakožto	jakožto	k8xS	jakožto
předpokládaný	předpokládaný	k2eAgInSc4d1	předpokládaný
ale	ale	k8xC	ale
písemně	písemně	k6eAd1	písemně
nedoložený	doložený	k2eNgInSc1d1	nedoložený
je	být	k5eAaImIp3nS	být
označen	označen	k2eAgInSc1d1	označen
*	*	kIx~	*
<g/>
hvězdičkou	hvězdička	k1gFnSc7	hvězdička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
