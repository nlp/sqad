<s>
Praslovanština	praslovanština	k1gFnSc1	praslovanština
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
společným	společný	k2eAgInSc7d1	společný
prajazykem	prajazyk	k1gInSc7	prajazyk
dávných	dávný	k2eAgMnPc2d1	dávný
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
