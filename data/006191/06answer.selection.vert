<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Íránu	Írán	k1gInSc2	Írán
je	být	k5eAaImIp3nS	být
muslimského	muslimský	k2eAgNnSc2d1	muslimské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
90	[number]	k4	90
%	%	kIx~	%
Íránců	Íránec	k1gMnPc2	Íránec
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
šíitské	šíitský	k2eAgFnSc3d1	šíitská
větvi	větev	k1gFnSc3	větev
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
8	[number]	k4	8
%	%	kIx~	%
k	k	k7c3	k
sunnitské	sunnitský	k2eAgFnSc3d1	sunnitská
větvi	větev	k1gFnSc3	větev
<g/>
.	.	kIx.	.
</s>
