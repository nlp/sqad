<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
dva	dva	k4xCgInPc1	dva
výroční	výroční	k2eAgInPc1d1	výroční
festivaly	festival	k1gInPc1	festival
symfonické	symfonický	k2eAgFnSc2d1	symfonická
hudby	hudba	k1gFnSc2	hudba
–	–	k?	–
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Špilberk	Špilberk	k1gInSc1	Špilberk
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
