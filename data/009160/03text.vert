<p>
<s>
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
Hraběcí	hraběcí	k2eAgNnSc4d1	hraběcí
Nosticovo	Nosticův	k2eAgNnSc4d1	Nosticovo
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
Tylovo	Tylův	k2eAgNnSc1d1	Tylovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
je	on	k3xPp3gNnSc4	on
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Ovocném	ovocný	k2eAgInSc6d1	ovocný
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Budova	budova	k1gFnSc1	budova
===	===	k?	===
</s>
</p>
<p>
<s>
Budovu	budova	k1gFnSc4	budova
dal	dát	k5eAaPmAgMnS	dát
postavit	postavit	k5eAaPmF	postavit
jako	jako	k9	jako
scénu	scéna	k1gFnSc4	scéna
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
osvícený	osvícený	k2eAgMnSc1d1	osvícený
vlastenec	vlastenec	k1gMnSc1	vlastenec
hrabě	hrabě	k1gMnSc1	hrabě
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Nostic-Rieneck	Nostic-Rieneck	k1gMnSc1	Nostic-Rieneck
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
uspořádání	uspořádání	k1gNnSc2	uspořádání
prostor	prostor	k1gInSc4	prostor
divadla	divadlo	k1gNnSc2	divadlo
naskicoval	naskicovat	k5eAaPmAgMnS	naskicovat
hrabě	hrabě	k1gMnSc1	hrabě
Kašpar	Kašpar	k1gMnSc1	Kašpar
Heřman	Heřman	k1gMnSc1	Heřman
Künigel	Künigel	k1gMnSc1	Künigel
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
letech	let	k1gInPc6	let
1781	[number]	k4	1781
<g/>
–	–	k?	–
<g/>
1783	[number]	k4	1783
projektoval	projektovat	k5eAaBmAgInS	projektovat
a	a	k8xC	a
provedl	provést	k5eAaPmAgInS	provést
dvorní	dvorní	k2eAgMnSc1d1	dvorní
stavitel	stavitel	k1gMnSc1	stavitel
Anton	Anton	k1gMnSc1	Anton
Haffenecker	Haffenecker	k1gMnSc1	Haffenecker
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
klasicistních	klasicistní	k2eAgFnPc2d1	klasicistní
budov	budova	k1gFnPc2	budova
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
trvala	trvat	k5eAaImAgFnS	trvat
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavost	zajímavost	k1gFnSc1	zajímavost
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
budově	budova	k1gFnSc6	budova
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
údajně	údajně	k6eAd1	údajně
si	se	k3xPyFc3	se
hrabě	hrabě	k1gMnSc1	hrabě
Nostic	Nostice	k1gFnPc2	Nostice
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
miniaturu	miniatura	k1gFnSc4	miniatura
tohoto	tento	k3xDgNnSc2	tento
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
letním	letní	k2eAgNnSc6d1	letní
sídle	sídlo	k1gNnSc6	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dokonce	dokonce	k9	dokonce
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
budovy	budova	k1gFnPc1	budova
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
i	i	k9	i
propojeny	propojit	k5eAaPmNgFnP	propojit
podzemní	podzemní	k2eAgFnSc7d1	podzemní
chodbou	chodba	k1gFnSc7	chodba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
budova	budova	k1gFnSc1	budova
se	se	k3xPyFc4	se
odlišovala	odlišovat	k5eAaImAgFnS	odlišovat
od	od	k7c2	od
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
mělo	mít	k5eAaImAgNnS	mít
velká	velký	k2eAgNnPc1d1	velké
půlkruhově	půlkruhově	k6eAd1	půlkruhově
zakončená	zakončený	k2eAgNnPc1d1	zakončené
okna	okno	k1gNnPc1	okno
prostupující	prostupující	k2eAgNnPc1d1	prostupující
patry	patro	k1gNnPc7	patro
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
strany	strana	k1gFnSc2	strana
od	od	k7c2	od
havelského	havelský	k2eAgInSc2d1	havelský
kláštera	klášter	k1gInSc2	klášter
souměrná	souměrný	k2eAgFnSc1d1	souměrná
se	se	k3xPyFc4	se
zadním	zadní	k2eAgInSc7d1	zadní
traktem	trakt	k1gInSc7	trakt
do	do	k7c2	do
zbytku	zbytek	k1gInSc2	zbytek
Ovocného	ovocný	k2eAgInSc2d1	ovocný
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Čelní	čelní	k2eAgInSc1d1	čelní
rizalit	rizalit	k1gInSc1	rizalit
nebyl	být	k5eNaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
pater	patro	k1gNnPc2	patro
jako	jako	k8xC	jako
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
byl	být	k5eAaImAgMnS	být
uvnitř	uvnitř	k6eAd1	uvnitř
jeden	jeden	k4xCgInSc4	jeden
vysoký	vysoký	k2eAgInSc4d1	vysoký
salon	salon	k1gInSc4	salon
prostupující	prostupující	k2eAgInSc4d1	prostupující
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
patra	patro	k1gNnSc2	patro
až	až	k9	až
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgNnSc1d1	původní
hlediště	hlediště	k1gNnSc1	hlediště
mělo	mít	k5eAaImAgNnS	mít
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgNnPc4	tři
pořadí	pořadí	k1gNnPc2	pořadí
lóží	lóže	k1gFnPc2	lóže
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
galerii	galerie	k1gFnSc4	galerie
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
o	o	k7c4	o
patro	patro	k1gNnSc4	patro
nižší	nízký	k2eAgNnSc4d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
dnešní	dnešní	k2eAgNnSc1d1	dnešní
<g/>
.	.	kIx.	.
</s>
<s>
Disponovalo	disponovat	k5eAaBmAgNnS	disponovat
plochým	plochý	k2eAgInSc7d1	plochý
parterem	parter	k1gInSc7	parter
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
zvykem	zvyk	k1gInSc7	zvyk
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
plochý	plochý	k2eAgInSc4d1	plochý
strop	strop	k1gInSc4	strop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
došlo	dojít	k5eAaPmAgNnS	dojít
hned	hned	k6eAd1	hned
k	k	k7c3	k
několika	několik	k4yIc3	několik
menším	malý	k2eAgFnPc3d2	menší
i	i	k8xC	i
větším	veliký	k2eAgFnPc3d2	veliký
přestavbám	přestavba	k1gFnPc3	přestavba
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgMnPc1d3	nejvýraznější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
zaří	zaří	k6eAd1	zaří
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
(	(	kIx(	(
<g/>
navzdory	navzdory	k6eAd1	navzdory
časové	časový	k2eAgFnSc6d1	časová
tísni	tíseň	k1gFnSc6	tíseň
<g/>
)	)	kIx)	)
k	k	k7c3	k
pronikavé	pronikavý	k2eAgFnSc3d1	pronikavá
přestavbě	přestavba	k1gFnSc3	přestavba
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
hlediště	hlediště	k1gNnSc1	hlediště
bylo	být	k5eAaImAgNnS	být
zvýšeno	zvýšit	k5eAaPmNgNnS	zvýšit
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
patro	patro	k1gNnSc4	patro
a	a	k8xC	a
přistavěna	přistavět	k5eAaPmNgFnS	přistavět
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
hlediště	hlediště	k1gNnSc1	hlediště
bylo	být	k5eAaImAgNnS	být
vyzdobeno	vyzdoben	k2eAgNnSc1d1	vyzdobeno
do	do	k7c2	do
červeno-bílo-zlatých	červenoílolatý	k2eAgFnPc2d1	červeno-bílo-zlatý
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Parter	parter	k1gInSc1	parter
<g/>
,	,	kIx,	,
dřív	dříve	k6eAd2	dříve
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
propojen	propojit	k5eAaPmNgMnS	propojit
do	do	k7c2	do
jednoho	jeden	k4xCgMnSc2	jeden
svažitého	svažitý	k2eAgMnSc2d1	svažitý
<g/>
.	.	kIx.	.
</s>
<s>
Přízemní	přízemní	k2eAgNnSc1d1	přízemní
pořadí	pořadí	k1gNnSc1	pořadí
lóží	lóže	k1gFnPc2	lóže
bylo	být	k5eAaImAgNnS	být
propojedno	propojedno	k1gNnSc4	propojedno
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
velkou	velký	k2eAgFnSc4d1	velká
galerii	galerie	k1gFnSc4	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Portál	portál	k1gInSc1	portál
mezi	mezi	k7c7	mezi
hledištěm	hlediště	k1gNnSc7	hlediště
a	a	k8xC	a
jevištěm	jeviště	k1gNnSc7	jeviště
byl	být	k5eAaImAgInS	být
vyvýšený	vyvýšený	k2eAgInSc1d1	vyvýšený
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
jeviště	jeviště	k1gNnSc1	jeviště
získalo	získat	k5eAaPmAgNnS	získat
novou	nový	k2eAgFnSc4d1	nová
mašinérii	mašinérie	k1gFnSc4	mašinérie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
dostalo	dostat	k5eAaPmAgNnS	dostat
hlediště	hlediště	k1gNnSc1	hlediště
jeden	jeden	k4xCgInSc4	jeden
velký	velký	k2eAgInSc4d1	velký
centrální	centrální	k2eAgInSc4d1	centrální
lustr	lustr	k1gInSc4	lustr
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
otevřené	otevřený	k2eAgNnSc1d1	otevřené
divadlo	divadlo	k1gNnSc1	divadlo
zahajovalo	zahajovat	k5eAaImAgNnS	zahajovat
operou	opera	k1gFnSc7	opera
Němá	němý	k2eAgFnSc1d1	němá
z	z	k7c2	z
Poříčí	Poříčí	k1gNnSc2	Poříčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
a	a	k8xC	a
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
přestavbou	přestavba	k1gFnSc7	přestavba
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
byla	být	k5eAaImAgFnS	být
přestavba	přestavba	k1gFnSc1	přestavba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Přestavba	přestavba	k1gFnSc1	přestavba
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
architektem	architekt	k1gMnSc7	architekt
Karlem	Karel	k1gMnSc7	Karel
Brustem	Brust	k1gMnSc7	Brust
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Niklasem	Niklasma	k1gFnPc2	Niklasma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
svou	svůj	k3xOyFgFnSc7	svůj
citlivostí	citlivost	k1gFnSc7	citlivost
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
stylu	styl	k1gInSc3	styl
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
okna	okno	k1gNnPc1	okno
v	v	k7c6	v
exteriéru	exteriér	k1gInSc6	exteriér
byla	být	k5eAaImAgFnS	být
zazděna	zazděn	k2eAgFnSc1d1	zazděna
a	a	k8xC	a
proražena	proražen	k2eAgFnSc1d1	proražena
nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
patrech	patro	k1gNnPc6	patro
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Salon	salon	k1gInSc1	salon
v	v	k7c6	v
předním	přední	k2eAgInSc6d1	přední
razalitu	razalit	k1gInSc6	razalit
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Boční	boční	k2eAgFnPc1d1	boční
tříosé	tříosý	k2eAgFnPc1d1	tříosá
razality	razalita	k1gFnPc1	razalita
byly	být	k5eAaImAgFnP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
na	na	k7c4	na
pětiosé	pětiosý	k2eAgNnSc4d1	pětiosé
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
došlo	dojít	k5eAaPmAgNnS	dojít
prakticky	prakticky	k6eAd1	prakticky
k	k	k7c3	k
celkovému	celkový	k2eAgNnSc3d1	celkové
vybourání	vybourání	k1gNnSc3	vybourání
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
hmoty	hmota	k1gFnSc2	hmota
divadla	divadlo	k1gNnSc2	divadlo
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
upraven	upraven	k2eAgInSc4d1	upraven
půdorys	půdorys	k1gInSc4	půdorys
rozšířením	rozšíření	k1gNnSc7	rozšíření
chodeb	chodba	k1gFnPc2	chodba
a	a	k8xC	a
stavbou	stavba	k1gFnSc7	stavba
nových	nový	k2eAgNnPc2d1	nové
schodišť	schodiště	k1gNnPc2	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Hlediště	hlediště	k1gNnSc1	hlediště
divadla	divadlo	k1gNnSc2	divadlo
bylo	být	k5eAaImAgNnS	být
kompletně	kompletně	k6eAd1	kompletně
zbořeno	zbořen	k2eAgNnSc1d1	zbořeno
a	a	k8xC	a
zkráceno	zkrácen	k2eAgNnSc1d1	zkráceno
(	(	kIx(	(
<g/>
původní	původní	k2eAgNnSc1d1	původní
hlediště	hlediště	k1gNnSc1	hlediště
bylo	být	k5eAaImAgNnS	být
delší	dlouhý	k2eAgNnSc1d2	delší
<g/>
)	)	kIx)	)
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
zvětšení	zvětšení	k1gNnSc2	zvětšení
foyerů	foyer	k1gInPc2	foyer
a	a	k8xC	a
chodeb	chodba	k1gFnPc2	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
hlediště	hlediště	k1gNnSc1	hlediště
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
pater	patro	k1gNnPc2	patro
-	-	kIx~	-
tří	tři	k4xCgFnPc2	tři
pořadí	pořadí	k1gNnSc6	pořadí
lóží	lóže	k1gFnPc2	lóže
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
galerií	galerie	k1gFnPc2	galerie
se	s	k7c7	s
svažitým	svažitý	k2eAgInSc7d1	svažitý
parterem	parter	k1gInSc7	parter
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vyzdobeno	vyzdobit	k5eAaPmNgNnS	vyzdobit
opět	opět	k6eAd1	opět
červeno-bílo-zlatě	červenoílolatě	k6eAd1	červeno-bílo-zlatě
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
lóže	lóže	k1gFnPc1	lóže
byly	být	k5eAaImAgFnP	být
zmenšeny	zmenšit	k5eAaPmNgFnP	zmenšit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
navzdory	navzdory	k7c3	navzdory
zkrácení	zkrácení	k1gNnSc3	zkrácení
sálu	sál	k1gInSc2	sál
zvětšil	zvětšit	k5eAaPmAgMnS	zvětšit
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
na	na	k7c4	na
12	[number]	k4	12
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
25	[number]	k4	25
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
patro	patro	k1gNnSc4	patro
(	(	kIx(	(
<g/>
vč.	vč.	k?	vč.
středové	středový	k2eAgInPc1d1	středový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
přestavba	přestavba	k1gFnSc1	přestavba
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
velmi	velmi	k6eAd1	velmi
konzervativně	konzervativně	k6eAd1	konzervativně
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
zlepšení	zlepšení	k1gNnSc1	zlepšení
akustiky	akustika	k1gFnSc2	akustika
<g/>
,	,	kIx,	,
zvětšení	zvětšení	k1gNnSc1	zvětšení
kapacity	kapacita	k1gFnSc2	kapacita
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc4d2	veliký
komfort	komfort	k1gInSc4	komfort
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
a	a	k8xC	a
zlepšení	zlepšení	k1gNnSc4	zlepšení
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
především	především	k9	především
požárních	požární	k2eAgNnPc2d1	požární
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
přestavba	přestavba	k1gFnSc1	přestavba
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
přestavby	přestavba	k1gFnSc2	přestavba
bylo	být	k5eAaImAgNnS	být
celé	celý	k2eAgNnSc1d1	celé
divadlo	divadlo	k1gNnSc1	divadlo
prodlouženo	prodloužen	k2eAgNnSc1d1	prodlouženo
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Ovocného	ovocný	k2eAgInSc2d1	ovocný
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zvětšeny	zvětšen	k2eAgInPc4d1	zvětšen
prostory	prostor	k1gInPc4	prostor
pro	pro	k7c4	pro
účinkující	účinkující	k1gMnPc4	účinkující
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
přístavbou	přístavba	k1gFnSc7	přístavba
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zřízení	zřízení	k1gNnSc1	zřízení
postranních	postranní	k2eAgFnPc2d1	postranní
pavlačí	pavlač	k1gFnPc2	pavlač
s	s	k7c7	s
venkovními	venkovní	k2eAgNnPc7d1	venkovní
schodišti	schodiště	k1gNnPc7	schodiště
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xC	jako
únikové	únikový	k2eAgInPc1d1	únikový
východy	východ	k1gInPc1	východ
v	v	k7c6	v
případě	případ	k1gInSc6	případ
požáru	požár	k1gInSc2	požár
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
bývaly	bývat	k5eAaImAgFnP	bývat
především	především	k6eAd1	především
v	v	k7c6	v
divadlech	divadlo	k1gNnPc6	divadlo
velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
přestavbou	přestavba	k1gFnSc7	přestavba
byla	být	k5eAaImAgFnS	být
generální	generální	k2eAgFnSc1d1	generální
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1983	[number]	k4	1983
až	až	k9	až
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
došlo	dojít	k5eAaPmAgNnS	dojít
kromě	kromě	k7c2	kromě
restaurování	restaurování	k1gNnSc2	restaurování
interiérů	interiér	k1gInPc2	interiér
a	a	k8xC	a
exteriérů	exteriér	k1gInPc2	exteriér
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
technických	technický	k2eAgInPc2d1	technický
výtahů	výtah	k1gInPc2	výtah
<g/>
,	,	kIx,	,
k	k	k7c3	k
prohloubení	prohloubení	k1gNnSc3	prohloubení
divadla	divadlo	k1gNnSc2	divadlo
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vnikl	vniknout	k5eAaPmAgInS	vniknout
další	další	k2eAgInSc1d1	další
foyer	foyer	k1gInSc1	foyer
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
v	v	k7c6	v
parteru	parter	k1gInSc6	parter
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
provozní	provozní	k2eAgFnSc7d1	provozní
částí	část	k1gFnSc7	část
divadla	divadlo	k1gNnSc2	divadlo
vnikly	vniknout	k5eAaPmAgInP	vniknout
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
suterény	suterén	k1gInPc1	suterén
se	s	k7c7	s
zázemím	zázemí	k1gNnSc7	zázemí
pro	pro	k7c4	pro
účinkující	účinkující	k1gFnSc4	účinkující
i	i	k8xC	i
se	se	k3xPyFc4	se
sklady	sklad	k1gInPc1	sklad
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prostory	prostor	k1gInPc1	prostor
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
až	až	k9	až
pod	pod	k7c4	pod
volnou	volný	k2eAgFnSc4d1	volná
plochu	plocha	k1gFnSc4	plocha
Ovocného	ovocný	k2eAgInSc2d1	ovocný
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
postavena	postaven	k2eAgFnSc1d1	postavena
chodba	chodba	k1gFnSc1	chodba
spojující	spojující	k2eAgNnSc4d1	spojující
divadlo	divadlo	k1gNnSc4	divadlo
s	s	k7c7	s
Kolowratským	Kolowratský	k2eAgInSc7d1	Kolowratský
palácem	palác	k1gInSc7	palác
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
provozní	provozní	k2eAgNnSc1d1	provozní
zázemí	zázemí	k1gNnSc1	zázemí
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výzdoba	výzdoba	k1gFnSc1	výzdoba
===	===	k?	===
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
vyzdobena	vyzdoben	k2eAgFnSc1d1	vyzdobena
sloupy	sloup	k1gInPc7	sloup
a	a	k8xC	a
pilastry	pilastr	k1gInPc7	pilastr
z	z	k7c2	z
hnědého	hnědý	k2eAgInSc2d1	hnědý
sliveneckého	slivenecký	k2eAgInSc2d1	slivenecký
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
podlahy	podlaha	k1gFnSc2	podlaha
vestibulu	vestibul	k1gInSc2	vestibul
a	a	k8xC	a
foyer	foyer	k1gInSc1	foyer
jsou	být	k5eAaImIp3nP	být
vydlážděny	vydláždit	k5eAaPmNgInP	vydláždit
bílým	bílé	k1gNnSc7	bílé
a	a	k8xC	a
hnědým	hnědý	k2eAgInSc7d1	hnědý
mramorem	mramor	k1gInSc7	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Strop	strop	k1gInSc1	strop
jeviště	jeviště	k1gNnSc2	jeviště
má	mít	k5eAaImIp3nS	mít
malovanou	malovaný	k2eAgFnSc4d1	malovaná
geometrickou	geometrický	k2eAgFnSc4d1	geometrická
dekoraci	dekorace	k1gFnSc4	dekorace
s	s	k7c7	s
groteskními	groteskní	k2eAgInPc7d1	groteskní
motivy	motiv	k1gInPc7	motiv
v	v	k7c6	v
pompejském	pompejský	k2eAgInSc6d1	pompejský
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
úprav	úprava	k1gFnPc2	úprava
interiéru	interiér	k1gInSc2	interiér
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1859	[number]	k4	1859
a	a	k8xC	a
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Bergler	Bergler	k1gMnSc1	Bergler
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
namaloval	namalovat	k5eAaPmAgMnS	namalovat
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
oponu	opona	k1gFnSc4	opona
<g/>
,	,	kIx,	,
olejomalba	olejomalba	k1gFnSc1	olejomalba
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
Olymp	Olymp	k1gInSc4	Olymp
<g/>
,	,	kIx,	,
shromáždění	shromáždění	k1gNnSc4	shromáždění
řeckých	řecký	k2eAgMnPc2d1	řecký
bohů	bůh	k1gMnPc2	bůh
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
umění	umění	k1gNnSc2	umění
Apollónem	Apollón	k1gMnSc7	Apollón
ve	v	k7c6	v
slunečním	sluneční	k2eAgInSc6d1	sluneční
voze	vůz	k1gInSc6	vůz
taženém	tažený	k2eAgInSc6d1	tažený
trojspřežím	trojspřeží	k1gNnSc7	trojspřeží
běloušů	bělouš	k1gMnPc2	bělouš
<g/>
,	,	kIx,	,
bohové	bůh	k1gMnPc1	bůh
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
múzami	múza	k1gFnPc7	múza
účastní	účastnit	k5eAaImIp3nS	účastnit
produkce	produkce	k1gFnSc1	produkce
umění	umění	k1gNnSc2	umění
nebo	nebo	k8xC	nebo
jí	on	k3xPp3gFnSc7	on
přihlížejí	přihlížet	k5eAaImIp3nP	přihlížet
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
opony	opona	k1gFnSc2	opona
je	být	k5eAaImIp3nS	být
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
ve	v	k7c6	v
vstupním	vstupní	k2eAgInSc6d1	vstupní
vestibulu	vestibul	k1gInSc6	vestibul
nad	nad	k7c7	nad
pokladnou	pokladna	k1gFnSc7	pokladna
<g/>
,	,	kIx,	,
originál	originál	k1gInSc1	originál
opony	opona	k1gFnSc2	opona
se	se	k3xPyFc4	se
nezachoval	zachovat	k5eNaPmAgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1804	[number]	k4	1804
-	-	kIx~	-
1809	[number]	k4	1809
maloval	malovat	k5eAaImAgMnS	malovat
pro	pro	k7c4	pro
divadlo	divadlo	k1gNnSc4	divadlo
jevištní	jevištní	k2eAgFnSc2d1	jevištní
dekorace	dekorace	k1gFnSc2	dekorace
Karel	Karel	k1gMnSc1	Karel
Postl	Postl	k1gMnSc1	Postl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
foyer	foyer	k1gNnSc6	foyer
jsou	být	k5eAaImIp3nP	být
malované	malovaný	k2eAgInPc1d1	malovaný
portréty	portrét	k1gInPc1	portrét
a	a	k8xC	a
busty	busta	k1gFnPc1	busta
významných	významný	k2eAgMnPc2d1	významný
představitelů	představitel	k1gMnPc2	představitel
divadla	divadlo	k1gNnSc2	divadlo
<g/>
:	:	kIx,	:
portrét	portrét	k1gInSc1	portrét
dramatika	dramatik	k1gMnSc2	dramatik
<g/>
,	,	kIx,	,
herce	herec	k1gMnSc2	herec
a	a	k8xC	a
ředitele	ředitel	k1gMnSc2	ředitel
divadla	divadlo	k1gNnSc2	divadlo
Jana	Jan	k1gMnSc2	Jan
Nepomuka	Nepomuk	k1gMnSc2	Nepomuk
Štěpánka	Štěpánek	k1gMnSc2	Štěpánek
(	(	kIx(	(
<g/>
1783	[number]	k4	1783
–	–	k?	–
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Antonín	Antonín	k1gMnSc1	Antonín
Machek	Machek	k1gMnSc1	Machek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
portréty	portrét	k1gInPc1	portrét
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
a	a	k8xC	a
kapelníka	kapelník	k1gMnSc2	kapelník
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
Františka	František	k1gMnSc2	František
Škroupa	Škroup	k1gMnSc2	Škroup
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
,	,	kIx,	,
herce	herec	k1gMnSc4	herec
a	a	k8xC	a
pěvce	pěvec	k1gMnSc4	pěvec
Antonína	Antonín	k1gMnSc4	Antonín
Jelena	Jelen	k1gMnSc4	Jelen
v	v	k7c6	v
kostýmu	kostým	k1gInSc6	kostým
Maxe	Max	k1gMnSc2	Max
z	z	k7c2	z
Weberovy	Weberův	k2eAgFnSc2d1	Weberova
opery	opera	k1gFnSc2	opera
Čarostřelec	čarostřelec	k1gMnSc1	čarostřelec
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
zde	zde	k6eAd1	zde
visely	viset	k5eAaImAgInP	viset
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
portréty	portrét	k1gInPc1	portrét
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nezvěstné	zvěstný	k2eNgInPc1d1	nezvěstný
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kapelníka	kapelník	k1gMnSc2	kapelník
a	a	k8xC	a
houslisty	houslista	k1gMnSc2	houslista
Fridricha	Fridrich	k1gMnSc2	Fridrich
Wilhelma	Wilhelma	k1gFnSc1	Wilhelma
Pixise	Pixise	k1gFnSc1	Pixise
a	a	k8xC	a
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
Tekly	Tekla	k1gFnSc2	Tekla
Battkové-Podleské	Battkové-Podleský	k2eAgFnSc2d1	Battkové-Podleský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zahájení	zahájení	k1gNnSc1	zahájení
provozu	provoz	k1gInSc2	provoz
===	===	k?	===
</s>
</p>
<p>
<s>
Činnost	činnost	k1gFnSc1	činnost
zahájila	zahájit	k5eAaPmAgFnS	zahájit
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
Lessingova	Lessingův	k2eAgFnSc1d1	Lessingova
tragédie	tragédie	k1gFnSc1	tragédie
Emilia	Emilium	k1gNnSc2	Emilium
Galotti	Galotť	k1gFnSc2	Galotť
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
tehdy	tehdy	k6eAd1	tehdy
pojalo	pojmout	k5eAaPmAgNnS	pojmout
jeden	jeden	k4xCgMnSc1	jeden
tisíc	tisíc	k4xCgInPc2	tisíc
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Časem	čas	k1gInSc7	čas
byla	být	k5eAaImAgFnS	být
kapacita	kapacita	k1gFnSc1	kapacita
pro	pro	k7c4	pro
větší	veliký	k2eAgNnSc4d2	veliký
pohodlí	pohodlí	k1gNnSc4	pohodlí
diváků	divák	k1gMnPc2	divák
snížena	snížit	k5eAaPmNgFnS	snížit
na	na	k7c4	na
dnešních	dnešní	k2eAgNnPc2d1	dnešní
659	[number]	k4	659
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgMnS	mít
hrabě	hrabě	k1gMnSc1	hrabě
Nostic-Rieneck	Nostic-Rieneck	k1gMnSc1	Nostic-Rieneck
představu	představa	k1gFnSc4	představa
uvádět	uvádět	k5eAaImF	uvádět
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
německou	německý	k2eAgFnSc4d1	německá
činohru	činohra	k1gFnSc4	činohra
a	a	k8xC	a
italskou	italský	k2eAgFnSc4d1	italská
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
byl	být	k5eAaImAgInS	být
repertoár	repertoár	k1gInSc1	repertoár
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
i	i	k9	i
o	o	k7c4	o
českou	český	k2eAgFnSc4d1	Česká
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1785	[number]	k4	1785
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
veselohrou	veselohra	k1gFnSc7	veselohra
Gottlieba	Gottlieb	k1gMnSc2	Gottlieb
Stephanieho	Stephanie	k1gMnSc2	Stephanie
Odběhlec	odběhlec	k1gMnSc1	odběhlec
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
synovské	synovský	k2eAgFnSc2d1	synovská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
sám	sám	k3xTgMnSc1	sám
Mozart	Mozart	k1gMnSc1	Mozart
a	a	k8xC	a
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
osobně	osobně	k6eAd1	osobně
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
svoji	svůj	k3xOyFgFnSc4	svůj
Figarovu	Figarův	k2eAgFnSc4d1	Figarova
svatbu	svatba	k1gFnSc4	svatba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Nadšení	nadšení	k1gNnSc1	nadšení
Pražanů	Pražan	k1gMnPc2	Pražan
bylo	být	k5eAaImAgNnS	být
veliké	veliký	k2eAgNnSc1d1	veliké
<g/>
.	.	kIx.	.
</s>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
dojat	dojat	k2eAgInSc1d1	dojat
srdečným	srdečný	k2eAgNnSc7d1	srdečné
přijetím	přijetí	k1gNnSc7	přijetí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
složit	složit	k5eAaPmF	složit
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
operu	opera	k1gFnSc4	opera
Don	dona	k1gFnPc2	dona
Giovanni	Giovann	k1gMnPc1	Giovann
(	(	kIx(	(
<g/>
celý	celý	k2eAgInSc4d1	celý
název	název	k1gInSc4	název
zněl	znět	k5eAaImAgInS	znět
"	"	kIx"	"
<g/>
Il	Il	k1gFnSc2	Il
dissoluto	dissolout	k5eAaPmNgNnS	dissolout
punito	punit	k2eAgNnSc1d1	punit
ossia	ossium	k1gNnSc2	ossium
il	il	k?	il
Don	Don	k1gMnSc1	Don
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
literární	literární	k2eAgFnSc2d1	literární
předlohy	předloha	k1gFnSc2	předloha
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
premiéru	premiéra	k1gFnSc4	premiéra
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1787	[number]	k4	1787
opět	opět	k6eAd1	opět
sám	sám	k3xTgMnSc1	sám
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
tato	tento	k3xDgFnSc1	tento
opera	opera	k1gFnSc1	opera
divadlo	divadlo	k1gNnSc4	divadlo
proslavila	proslavit	k5eAaPmAgFnS	proslavit
a	a	k8xC	a
Mozartovými	Mozartův	k2eAgFnPc7d1	Mozartova
operami	opera	k1gFnPc7	opera
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
známým	známý	k2eAgInPc3d1	známý
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
kulturním	kulturní	k2eAgInSc6d1	kulturní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
jediným	jediné	k1gNnSc7	jediné
dosud	dosud	k6eAd1	dosud
existujícím	existující	k2eAgNnSc7d1	existující
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
působil	působit	k5eAaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
premiéra	premiéra	k1gFnSc1	premiéra
Mozartovy	Mozartův	k2eAgFnSc2d1	Mozartova
opery	opera	k1gFnSc2	opera
La	la	k1gNnSc2	la
clemenza	clemenz	k1gMnSc2	clemenz
di	di	k?	di
Tito	tento	k3xDgMnPc1	tento
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
korunovace	korunovace	k1gFnSc2	korunovace
císaře	císař	k1gMnSc2	císař
Leopolda	Leopold	k1gMnSc2	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
úspěchu	úspěch	k1gInSc3	úspěch
předchozích	předchozí	k2eAgFnPc2d1	předchozí
oper	opera	k1gFnPc2	opera
však	však	k9	však
již	již	k6eAd1	již
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
divadlo	divadlo	k1gNnSc4	divadlo
od	od	k7c2	od
hraběte	hrabě	k1gMnSc2	hrabě
Nostice	Nostice	k1gFnSc2	Nostice
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
čeští	český	k2eAgMnPc1d1	český
stavové	stavový	k2eAgNnSc4d1	stavové
a	a	k8xC	a
přejmenovali	přejmenovat	k5eAaPmAgMnP	přejmenovat
Nosticovo	Nosticův	k2eAgNnSc4d1	Nosticovo
divadlo	divadlo	k1gNnSc4	divadlo
na	na	k7c4	na
Stavovské	stavovský	k2eAgFnPc4d1	stavovská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
pravidelná	pravidelný	k2eAgNnPc4d1	pravidelné
česká	český	k2eAgNnPc4d1	české
představení	představení	k1gNnPc4	představení
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1826	[number]	k4	1826
zde	zde	k6eAd1	zde
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
opera	opera	k1gFnSc1	opera
Dráteník	dráteník	k1gMnSc1	dráteník
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Škroup	Škroup	k1gMnSc1	Škroup
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
i	i	k9	i
druhým	druhý	k4xOgMnSc7	druhý
kapelníkem	kapelník	k1gMnSc7	kapelník
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Škroup	Škroup	k1gMnSc1	Škroup
se	se	k3xPyFc4	se
také	také	k9	také
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c6	o
uvedení	uvedení	k1gNnSc6	uvedení
řady	řada	k1gFnSc2	řada
oper	opera	k1gFnPc2	opera
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
divadlech	divadlo	k1gNnPc6	divadlo
nehraných	hraný	k2eNgNnPc6d1	nehrané
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
zásluhou	zásluha	k1gFnSc7	zásluha
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
např.	např.	kA	např.
několik	několik	k4yIc4	několik
oper	opera	k1gFnPc2	opera
Richarda	Richard	k1gMnSc4	Richard
Wagnera	Wagner	k1gMnSc4	Wagner
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
se	se	k3xPyFc4	se
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
stalo	stát	k5eAaPmAgNnS	stát
Královské	královský	k2eAgNnSc4d1	královské
zemské	zemský	k2eAgNnSc4d1	zemské
německé	německý	k2eAgNnSc4d1	německé
divadlo	divadlo	k1gNnSc4	divadlo
s	s	k7c7	s
německým	německý	k2eAgInSc7d1	německý
souborem	soubor	k1gInSc7	soubor
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
V	v	k7c6	v
září	září	k1gNnSc6	září
1918	[number]	k4	1918
intendant	intendant	k1gMnSc1	intendant
Leopold	Leopold	k1gMnSc1	Leopold
Kramer	Kramer	k1gMnSc1	Kramer
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
s	s	k7c7	s
německým	německý	k2eAgInSc7d1	německý
souborem	soubor	k1gInSc7	soubor
nájemní	nájemní	k2eAgFnSc4d1	nájemní
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1920	[number]	k4	1920
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
protiněmeckých	protiněmecký	k2eAgInPc2d1	protiněmecký
nepokojů	nepokoj	k1gInPc2	nepokoj
však	však	k9	však
divadlo	divadlo	k1gNnSc4	divadlo
obsadil	obsadit	k5eAaPmAgMnS	obsadit
český	český	k2eAgInSc4d1	český
dav	dav	k1gInSc4	dav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
budovu	budova	k1gFnSc4	budova
předal	předat	k5eAaPmAgMnS	předat
Klubu	klub	k1gInSc2	klub
sólistů	sólista	k1gMnPc2	sólista
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Tato	tento	k3xDgFnSc1	tento
svévole	svévole	k1gFnSc1	svévole
byla	být	k5eAaImAgFnS	být
zhojena	zhojen	k2eAgFnSc1d1	zhojena
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
usnesení	usnesení	k1gNnSc1	usnesení
zemského	zemský	k2eAgInSc2d1	zemský
správního	správní	k2eAgInSc2d1	správní
výboru	výbor	k1gInSc2	výbor
divadlo	divadlo	k1gNnSc1	divadlo
převedlo	převést	k5eAaPmAgNnS	převést
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
nazvalo	nazvat	k5eAaBmAgNnS	nazvat
jej	on	k3xPp3gMnSc4	on
Stavovským	stavovský	k2eAgMnPc3d1	stavovský
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgNnP	být
zahájena	zahájit	k5eAaPmNgNnP	zahájit
představení	představení	k1gNnPc1	představení
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
Jiráskovou	Jiráskův	k2eAgFnSc7d1	Jiráskova
hrou	hra	k1gFnSc7	hra
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
divadelním	divadelní	k2eAgInSc7d1	divadelní
statutem	statut	k1gInSc7	statut
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1949	[number]	k4	1949
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc4	divadlo
i	i	k8xC	i
nové	nový	k2eAgNnSc4d1	nové
jméno	jméno	k1gNnSc4	jméno
–	–	k?	–
Tylovo	Tylův	k2eAgNnSc4d1	Tylovo
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
a	a	k8xC	a
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
prošlo	projít	k5eAaPmAgNnS	projít
divadlo	divadlo	k1gNnSc1	divadlo
zásadní	zásadní	k2eAgFnSc7d1	zásadní
přestavbou	přestavba	k1gFnSc7	přestavba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
podzemní	podzemní	k2eAgNnSc1d1	podzemní
podlaží	podlaží	k1gNnSc1	podlaží
a	a	k8xC	a
celé	celý	k2eAgNnSc1d1	celé
divadlo	divadlo	k1gNnSc1	divadlo
kompletně	kompletně	k6eAd1	kompletně
zrekonstruováno	zrekonstruovat	k5eAaPmNgNnS	zrekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zásadní	zásadní	k2eAgFnSc3d1	zásadní
změně	změna	k1gFnSc3	změna
barevnosti	barevnost	k1gFnSc3	barevnost
interiéru	interiér	k1gInSc2	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
bílo-zlato-červená	bílolato-červený	k2eAgFnSc1d1	bílo-zlato-červený
kombinace	kombinace	k1gFnSc1	kombinace
barev	barva	k1gFnPc2	barva
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
bílo-zlato-modrou	bílolatoodrý	k2eAgFnSc4d1	bílo-zlato-modrý
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
změnou	změna	k1gFnSc7	změna
barev	barva	k1gFnPc2	barva
stačil	stačit	k5eAaBmAgMnS	stačit
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
natočit	natočit	k5eAaBmF	natočit
řadu	řada	k1gFnSc4	řada
scén	scéna	k1gFnPc2	scéna
svého	svůj	k3xOyFgInSc2	svůj
slavného	slavný	k2eAgInSc2d1	slavný
filmu	film	k1gInSc2	film
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgNnSc1d1	podzemní
podlaží	podlaží	k1gNnSc1	podlaží
je	být	k5eAaImIp3nS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
šatna	šatna	k1gFnSc1	šatna
a	a	k8xC	a
občerstvení	občerstvení	k1gNnSc1	občerstvení
a	a	k8xC	a
technické	technický	k2eAgNnSc1d1	technické
zázemí	zázemí	k1gNnSc1	zázemí
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c4	pod
Ovocný	ovocný	k2eAgInSc4d1	ovocný
trh	trh	k1gInSc4	trh
<g/>
;	;	kIx,	;
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
provozních	provozní	k2eAgFnPc2d1	provozní
prostor	prostora	k1gFnPc2	prostora
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vstoupit	vstoupit	k5eAaPmF	vstoupit
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
samotného	samotný	k2eAgNnSc2d1	samotné
anebo	anebo	k8xC	anebo
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
Ovocného	ovocný	k2eAgInSc2d1	ovocný
trhu	trh	k1gInSc2	trh
výtahem	výtah	k1gInSc7	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
i	i	k8xC	i
moderní	moderní	k2eAgNnSc1d1	moderní
otočné	otočný	k2eAgNnSc1d1	otočné
jeviště	jeviště	k1gNnSc1	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
jeviště	jeviště	k1gNnSc1	jeviště
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
diváka	divák	k1gMnSc2	divák
sedícího	sedící	k2eAgMnSc2d1	sedící
v	v	k7c6	v
hledišti	hlediště	k1gNnSc6	hlediště
poměrně	poměrně	k6eAd1	poměrně
hluboké	hluboký	k2eAgInPc1d1	hluboký
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
jako	jako	k9	jako
Ovocný	ovocný	k2eAgInSc4d1	ovocný
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
lze	lze	k6eAd1	lze
i	i	k9	i
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
jeviště	jeviště	k1gNnSc4	jeviště
vstoupit	vstoupit	k5eAaPmF	vstoupit
zadním	zadní	k2eAgInSc7d1	zadní
vchodem	vchod	k1gInSc7	vchod
<g/>
.	.	kIx.	.
<g/>
Dvě	dva	k4xCgFnPc1	dva
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
lóže	lóže	k1gFnPc1	lóže
jsou	být	k5eAaImIp3nP	být
císařská	císařský	k2eAgFnSc1d1	císařská
a	a	k8xC	a
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
<g/>
.	.	kIx.	.
</s>
<s>
Císařská	císařský	k2eAgFnSc1d1	císařská
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
naproti	naproti	k7c3	naproti
jevišti	jeviště	k1gNnSc3	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
lóže	lóže	k1gFnSc1	lóže
je	být	k5eAaImIp3nS	být
napravo	napravo	k6eAd1	napravo
od	od	k7c2	od
jeviště	jeviště	k1gNnSc2	jeviště
<g/>
,	,	kIx,	,
výhled	výhled	k1gInSc1	výhled
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
nevalný	valný	k2eNgInSc1d1	nevalný
a	a	k8xC	a
lístek	lístek	k1gInSc1	lístek
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zakoupit	zakoupit	k5eAaPmF	zakoupit
nelze	lze	k6eNd1	lze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
neustále	neustále	k6eAd1	neustále
rezervována	rezervovat	k5eAaBmNgFnS	rezervovat
pro	pro	k7c4	pro
prezidenta	prezident	k1gMnSc4	prezident
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
hosty	host	k1gMnPc7	host
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
druhou	druhý	k4xOgFnSc4	druhý
scénou	scéna	k1gFnSc7	scéna
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
činoherního	činoherní	k2eAgInSc2d1	činoherní
souboru	soubor	k1gInSc2	soubor
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
opera	opera	k1gFnSc1	opera
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
provozuje	provozovat	k5eAaImIp3nS	provozovat
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
především	především	k6eAd1	především
mozartovský	mozartovský	k2eAgInSc4d1	mozartovský
repertoár	repertoár	k1gInSc4	repertoár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
menší	malý	k2eAgNnSc1d2	menší
Nosticovo	Nosticův	k2eAgNnSc1d1	Nosticovo
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zahradního	zahradní	k2eAgInSc2d1	zahradní
pavilónu	pavilón	k1gInSc2	pavilón
si	se	k3xPyFc3	se
hrabě	hrabě	k1gMnSc1	hrabě
Nostic	Nostice	k1gFnPc2	Nostice
dal	dát	k5eAaPmAgMnS	dát
vystavět	vystavět	k5eAaPmF	vystavět
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
svého	svůj	k3xOyFgInSc2	svůj
malostranského	malostranský	k2eAgInSc2d1	malostranský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Režisér	režisér	k1gMnSc1	režisér
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
natáčel	natáčet	k5eAaImAgMnS	natáčet
divadelní	divadelní	k2eAgFnSc2d1	divadelní
scény	scéna	k1gFnSc2	scéna
svého	svůj	k3xOyFgInSc2	svůj
filmu	film	k1gInSc2	film
Amadeus	Amadeus	k1gMnSc1	Amadeus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
české	český	k2eAgMnPc4d1	český
divadelníky	divadelník	k1gMnPc4	divadelník
spojené	spojený	k2eAgMnPc4d1	spojený
se	se	k3xPyFc4	se
Stavovským	stavovský	k2eAgNnSc7d1	Stavovské
divadlem	divadlo	k1gNnSc7	divadlo
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
osobnosti	osobnost	k1gFnPc4	osobnost
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
někdejší	někdejší	k2eAgMnSc1d1	někdejší
ředitel	ředitel	k1gMnSc1	ředitel
Jan	Jan	k1gMnSc1	Jan
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Kolowratský	Kolowratský	k2eAgInSc1d1	Kolowratský
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
divadlem	divadlo	k1gNnSc7	divadlo
spojen	spojit	k5eAaPmNgInS	spojit
podzemní	podzemní	k2eAgFnSc7d1	podzemní
chodbou	chodba	k1gFnSc7	chodba
a	a	k8xC	a
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
sídlí	sídlet	k5eAaImIp3nS	sídlet
komorní	komorní	k2eAgFnSc1d1	komorní
scéna	scéna	k1gFnSc1	scéna
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Kolowrat	Kolowrat	k1gInSc1	Kolowrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
W.	W.	kA	W.
A.	A.	kA	A.
Mozart	Mozart	k1gMnSc1	Mozart
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
premiéru	premiéra	k1gFnSc4	premiéra
své	svůj	k3xOyFgFnSc2	svůj
opery	opera	k1gFnSc2	opera
Don	dona	k1gFnPc2	dona
Giovanni	Giovann	k1gMnPc1	Giovann
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
socha	socha	k1gFnSc1	socha
ducha	duch	k1gMnSc2	duch
u	u	k7c2	u
divadla	divadlo	k1gNnSc2	divadlo
operní	operní	k2eAgFnSc4d1	operní
postavu	postava	k1gFnSc4	postava
Il	Il	k1gMnSc5	Il
Commendatore	Commendator	k1gMnSc5	Commendator
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
nové	nový	k2eAgNnSc4d1	nové
doby	doba	k1gFnPc4	doba
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
11	[number]	k4	11
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
333	[number]	k4	333
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Vlček	Vlček	k1gMnSc1	Vlček
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
památky	památka	k1gFnPc1	památka
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Str	str	kA	str
<g/>
.	.	kIx.	.
355	[number]	k4	355
<g/>
-	-	kIx~	-
<g/>
358	[number]	k4	358
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
včela	včela	k1gFnSc1	včela
<g/>
,	,	kIx,	,
měsíčník	měsíčník	k1gInSc1	měsíčník
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1834	[number]	k4	1834
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stavovské	stavovský	k2eAgFnSc2d1	stavovská
divadlo	divadlo	k1gNnSc4	divadlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
divadle	divadlo	k1gNnSc6	divadlo
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
</s>
</p>
<p>
<s>
TACE	TACE	kA	TACE
–	–	k?	–
entry	entra	k1gFnSc2	entra
in	in	k?	in
TACE	TACE	kA	TACE
database	database	k6eAd1	database
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
Romana	Roman	k1gMnSc2	Roman
Sikory	Sikora	k1gFnSc2	Sikora
na	na	k7c4	na
hru	hra	k1gFnSc4	hra
Dogwille	Dogwille	k1gNnSc2	Dogwille
<g/>
,	,	kIx,	,
inscenovanou	inscenovaný	k2eAgFnSc4d1	inscenovaná
ve	v	k7c6	v
Stavovském	stavovský	k2eAgNnSc6d1	Stavovské
divadle	divadlo	k1gNnSc6	divadlo
</s>
</p>
