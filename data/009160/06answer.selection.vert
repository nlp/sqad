<s>
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc4d1	původní
název	název	k1gInSc4	název
Hraběcí	hraběcí	k2eAgNnSc4d1	hraběcí
Nosticovo	Nosticův	k2eAgNnSc4d1	Nosticovo
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
Tylovo	Tylův	k2eAgNnSc1d1	Tylovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
je	on	k3xPp3gNnSc4	on
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Ovocném	ovocný	k2eAgInSc6d1	ovocný
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
