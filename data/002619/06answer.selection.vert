<s>
Nejzajímavější	zajímavý	k2eAgFnSc7d3	nejzajímavější
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
smyčka	smyčka	k1gFnSc1	smyčka
u	u	k7c2	u
Telgártu	Telgárt	k1gInSc2	Telgárt
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
2,3	[number]	k4	2,3
km	km	kA	km
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
níž	jenž	k3xRgFnSc2	jenž
trať	trať	k1gFnSc1	trať
překonává	překonávat	k5eAaImIp3nS	překonávat
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
31	[number]	k4	31
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
