<p>
<s>
Užovka	užovka	k1gFnSc1	užovka
hladká	hladký	k2eAgFnSc1d1	hladká
(	(	kIx(	(
<g/>
Coronella	Coronella	k1gFnSc1	Coronella
austriaca	austriaca	k1gFnSc1	austriaca
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
evropský	evropský	k2eAgMnSc1d1	evropský
nejedovatý	jedovatý	k2eNgMnSc1d1	nejedovatý
had	had	k1gMnSc1	had
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
uznávány	uznávat	k5eAaImNgInP	uznávat
tři	tři	k4xCgInPc1	tři
poddruhy	poddruh	k1gInPc1	poddruh
<g/>
:	:	kIx,	:
Coronella	Coronella	k1gFnSc1	Coronella
austriaca	austriaca	k1gFnSc1	austriaca
austriaca	austriaca	k1gFnSc1	austriaca
<g/>
,	,	kIx,	,
Coronella	Coronella	k1gFnSc1	Coronella
austriaca	austriaca	k1gFnSc1	austriaca
acutirostris	acutirostris	k1gFnSc1	acutirostris
a	a	k8xC	a
Coronella	Coronella	k1gFnSc1	Coronella
austriaca	austriac	k1gInSc2	austriac
fitzingeri	fitzinger	k1gFnSc2	fitzinger
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
Coronella	Coronell	k1gMnSc2	Coronell
austriaca	austriacus	k1gMnSc2	austriacus
acutirostris	acutirostris	k1gInSc1	acutirostris
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
Coronella	Coronello	k1gNnSc2	Coronello
austriaca	austriac	k1gInSc2	austriac
fitzingeri	fitzinger	k1gFnSc2	fitzinger
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
tohoto	tento	k3xDgInSc2	tento
poddruhu	poddruh	k1gInSc2	poddruh
je	být	k5eAaImIp3nS	být
však	však	k9	však
některými	některý	k3yIgInPc7	některý
autory	autor	k1gMnPc7	autor
zpochybňována	zpochybňovat	k5eAaImNgFnS	zpochybňovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
užovka	užovka	k1gFnSc1	užovka
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
rodí	rodit	k5eAaImIp3nS	rodit
živá	živý	k2eAgNnPc4d1	živé
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
také	také	k9	také
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
užovka	užovka	k1gFnSc1	užovka
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
rozmnožováním	rozmnožování	k1gNnSc7	rozmnožování
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
užovka	užovka	k1gFnSc1	užovka
je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
části	část	k1gFnSc2	část
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
severu	sever	k1gInSc2	sever
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
celého	celý	k2eAgNnSc2d1	celé
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
severu	sever	k1gInSc2	sever
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
až	až	k9	až
na	na	k7c4	na
64	[number]	k4	64
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc7d1	východní
hranicí	hranice	k1gFnSc7	hranice
rozšíření	rozšíření	k1gNnSc2	rozšíření
je	být	k5eAaImIp3nS	být
Kaspické	kaspický	k2eAgNnSc4d1	Kaspické
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
severozápad	severozápad	k1gInSc4	severozápad
Íránu	Írán	k1gInSc2	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
rovněž	rovněž	k9	rovněž
celé	celý	k2eAgNnSc1d1	celé
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Užovka	užovka	k1gFnSc1	užovka
hladká	hladký	k2eAgFnSc1d1	hladká
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
až	až	k9	až
60	[number]	k4	60
centimetrů	centimetr	k1gInPc2	centimetr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
však	však	k9	však
výjimečně	výjimečně	k6eAd1	výjimečně
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
75	[number]	k4	75
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
jedinci	jedinec	k1gMnPc1	jedinec
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
83	[number]	k4	83
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
asi	asi	k9	asi
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rekordní	rekordní	k2eAgMnSc1d1	rekordní
jedinec	jedinec	k1gMnSc1	jedinec
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
14	[number]	k4	14
let	let	k1gInSc4	let
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Má	mít	k5eAaImIp3nS	mít
plochou	plochý	k2eAgFnSc4d1	plochá
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
odlišitelná	odlišitelný	k2eAgFnSc1d1	odlišitelná
od	od	k7c2	od
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
ošupení	ošupení	k1gNnSc1	ošupení
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
užovek	užovka	k1gFnPc2	užovka
rodu	rod	k1gInSc2	rod
Natrix	Natrix	k1gInSc1	Natrix
hladké	hladký	k2eAgInPc1d1	hladký
a	a	k8xC	a
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
vlastně	vlastně	k9	vlastně
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k8xC	i
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
těla	tělo	k1gNnSc2	tělo
má	mít	k5eAaImIp3nS	mít
19	[number]	k4	19
řad	řada	k1gFnPc2	řada
šupin	šupina	k1gFnPc2	šupina
<g/>
,	,	kIx,	,
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
má	mít	k5eAaImIp3nS	mít
šupiny	šupina	k1gFnPc1	šupina
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
150	[number]	k4	150
až	až	k9	až
164	[number]	k4	164
břišních	břišní	k2eAgFnPc2d1	břišní
šupin	šupina	k1gFnPc2	šupina
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
152	[number]	k4	152
až	až	k9	až
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Rostrální	Rostrální	k2eAgInSc1d1	Rostrální
štítek	štítek	k1gInSc1	štítek
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
trojúhelníkovitý	trojúhelníkovitý	k2eAgInSc4d1	trojúhelníkovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hluboce	hluboko	k6eAd1	hluboko
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
štítku	štítek	k1gInSc2	štítek
internasálního	internasální	k2eAgInSc2d1	internasální
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
ho	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
<g/>
.	.	kIx.	.
</s>
<s>
Užovka	užovka	k1gFnSc1	užovka
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgMnSc1	jeden
(	(	kIx(	(
<g/>
vzácně	vzácně	k6eAd1	vzácně
dva	dva	k4xCgInPc1	dva
<g/>
)	)	kIx)	)
předoční	předoční	k2eAgInPc1d1	předoční
štítky	štítek	k1gInPc1	štítek
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
zaoční	zaočnět	k5eAaPmIp3nS	zaočnět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
očima	oko	k1gNnPc7	oko
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
páry	pár	k1gInPc4	pár
týlních	týlní	k2eAgInPc2d1	týlní
štítků	štítek	k1gInPc2	štítek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
pak	pak	k6eAd1	pak
jeden	jeden	k4xCgMnSc1	jeden
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Horních	horní	k2eAgInPc2d1	horní
retních	retní	k2eAgInPc2d1	retní
štítků	štítek	k1gInPc2	štítek
má	mít	k5eAaImIp3nS	mít
většinou	většina	k1gFnSc7	většina
7	[number]	k4	7
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Anální	anální	k2eAgInSc1d1	anální
štítek	štítek	k1gInSc1	štítek
pod	pod	k7c7	pod
ocasem	ocas	k1gInSc7	ocas
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc4	jeden
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
dva	dva	k4xCgMnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
54	[number]	k4	54
až	až	k9	až
70	[number]	k4	70
párů	pár	k1gInPc2	pár
podocasních	podocasní	k2eAgInPc2d1	podocasní
štítků	štítek	k1gInPc2	štítek
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
46	[number]	k4	46
až	až	k9	až
76	[number]	k4	76
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samci	Samek	k1gMnPc1	Samek
bývají	bývat	k5eAaImIp3nP	bývat
hnědí	hnědý	k2eAgMnPc1d1	hnědý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
do	do	k7c2	do
červena	červeno	k1gNnSc2	červeno
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
spíše	spíše	k9	spíše
šedohnědé	šedohnědý	k2eAgNnSc4d1	šedohnědé
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hlavou	hlava	k1gFnSc7	hlava
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
tmavá	tmavý	k2eAgFnSc1d1	tmavá
skvrna	skvrna	k1gFnSc1	skvrna
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
tmavý	tmavý	k2eAgInSc1d1	tmavý
pás	pás	k1gInSc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
se	se	k3xPyFc4	se
skvrny	skvrna	k1gFnPc1	skvrna
někdy	někdy	k6eAd1	někdy
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
užovka	užovka	k1gFnSc1	užovka
připomíná	připomínat	k5eAaImIp3nS	připomínat
zmiji	zmije	k1gFnSc4	zmije
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
zabíjena	zabíjen	k2eAgFnSc1d1	zabíjena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zmijí	zmije	k1gFnPc2	zmije
však	však	k9	však
její	její	k3xOp3gNnPc4	její
oči	oko	k1gNnPc4	oko
mají	mít	k5eAaImIp3nP	mít
kulatou	kulatý	k2eAgFnSc4d1	kulatá
zřítelnici	zřítelnice	k1gFnSc4	zřítelnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
čenichu	čenich	k1gInSc2	čenich
přes	přes	k7c4	přes
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
tmavý	tmavý	k2eAgInSc1d1	tmavý
pruh	pruh	k1gInSc1	pruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
ve	v	k7c6	v
slunných	slunný	k2eAgFnPc6d1	slunná
a	a	k8xC	a
kamenitých	kamenitý	k2eAgFnPc6d1	kamenitá
křovinatých	křovinatý	k2eAgFnPc6d1	křovinatá
stráních	stráň	k1gFnPc6	stráň
a	a	k8xC	a
ve	v	k7c6	v
skalách	skála	k1gFnPc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
rovněž	rovněž	k9	rovněž
různé	různý	k2eAgFnPc4d1	různá
hromady	hromada	k1gFnPc4	hromada
sutě	suť	k1gFnPc4	suť
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
suchá	suchý	k2eAgNnPc4d1	suché
a	a	k8xC	a
křovinatá	křovinatý	k2eAgNnPc4d1	křovinaté
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
především	především	k9	především
ještěrky	ještěrka	k1gFnPc4	ještěrka
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
ale	ale	k9	ale
uloví	ulovit	k5eAaPmIp3nP	ulovit
i	i	k9	i
slepýše	slepýš	k1gMnSc4	slepýš
nebo	nebo	k8xC	nebo
hada	had	k1gMnSc4	had
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
žere	žrát	k5eAaImIp3nS	žrát
i	i	k9	i
kobylky	kobylka	k1gFnPc4	kobylka
a	a	k8xC	a
další	další	k2eAgInSc4d1	další
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
denní	denní	k2eAgMnSc1d1	denní
živočich	živočich	k1gMnSc1	živočich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
po	po	k7c6	po
zimování	zimování	k1gNnSc6	zimování
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
živorodá	živorodý	k2eAgFnSc1d1	živorodá
<g/>
,	,	kIx,	,
březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
samice	samice	k1gFnSc1	samice
klade	klást	k5eAaImIp3nS	klást
živá	živý	k2eAgNnPc4d1	živé
mláďata	mládě	k1gNnPc4	mládě
12	[number]	k4	12
až	až	k8xS	až
20	[number]	k4	20
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgMnPc1d3	nejdelší
hadí	hadí	k2eAgMnPc1d1	hadí
novorozenci	novorozenec	k1gMnPc1	novorozenec
bývají	bývat	k5eAaImIp3nP	bývat
však	však	k9	však
až	až	k9	až
27	[number]	k4	27
centimetrů	centimetr	k1gInPc2	centimetr
dlouzí	dlouhý	k2eAgMnPc1d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
jich	on	k3xPp3gMnPc2	on
bývá	bývat	k5eAaImIp3nS	bývat
8	[number]	k4	8
až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
ohrožení	ohrožení	k1gNnSc6	ohrožení
vypouští	vypouštět	k5eAaImIp3nP	vypouštět
zapáchající	zapáchající	k2eAgFnSc4d1	zapáchající
tekutinu	tekutina	k1gFnSc4	tekutina
z	z	k7c2	z
kloaky	kloaka	k1gFnSc2	kloaka
a	a	k8xC	a
zuřivě	zuřivě	k6eAd1	zuřivě
kouše	kousat	k5eAaImIp3nS	kousat
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
je	být	k5eAaImIp3nS	být
také	také	k9	také
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
syčení	syčení	k1gNnSc1	syčení
<g/>
,	,	kIx,	,
nafukování	nafukování	k1gNnSc1	nafukování
krku	krk	k1gInSc2	krk
a	a	k8xC	a
zplošťování	zplošťování	k1gNnSc2	zplošťování
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněna	chráněn	k2eAgFnSc1d1	chráněna
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
svých	svůj	k3xOyFgNnPc6	svůj
vývojových	vývojový	k2eAgNnPc6d1	vývojové
stádiích	stádium	k1gNnPc6	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Chráněna	chráněn	k2eAgFnSc1d1	chráněna
jsou	být	k5eAaImIp3nP	být
jí	on	k3xPp3gFnSc3	on
užívaná	užívaný	k2eAgNnPc4d1	užívané
přirozená	přirozený	k2eAgNnPc4d1	přirozené
i	i	k8xC	i
umělá	umělý	k2eAgNnPc4d1	umělé
sídla	sídlo	k1gNnPc4	sídlo
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
biotop	biotop	k1gInSc4	biotop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Coronella	Coronell	k1gMnSc2	Coronell
austriaca	austriacus	k1gMnSc2	austriacus
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MORAVEC	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Plazi	plaz	k1gMnPc1	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
531	[number]	k4	531
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
2416	[number]	k4	2416
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Coronella	Coronella	k1gFnSc1	Coronella
austriaca	austriaca	k1gFnSc1	austriaca
–	–	k?	–
užovka	užovka	k1gFnSc1	užovka
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
s.	s.	k?	s.
283	[number]	k4	283
<g/>
–	–	k?	–
<g/>
302	[number]	k4	302
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příroda	příroda	k1gFnSc1	příroda
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
–	–	k?	–
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
a	a	k8xC	a
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
ROH	roh	k1gInSc1	roh
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
užovka	užovka	k1gFnSc1	užovka
hladká	hladkat	k5eAaImIp3nS	hladkat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
