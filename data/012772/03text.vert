<p>
<s>
Trest	trest	k1gInSc1	trest
propadnutí	propadnutí	k1gNnSc2	propadnutí
majetku	majetek	k1gInSc2	majetek
je	být	k5eAaImIp3nS	být
nejpřísnější	přísný	k2eAgInSc1d3	nejpřísnější
majetkový	majetkový	k2eAgInSc1d1	majetkový
trest	trest	k1gInSc1	trest
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
uložit	uložit	k5eAaPmF	uložit
jak	jak	k6eAd1	jak
osobě	osoba	k1gFnSc3	osoba
fyzické	fyzický	k2eAgFnSc3d1	fyzická
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
osobě	osoba	k1gFnSc3	osoba
právnické	právnický	k2eAgFnPc1d1	právnická
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trest	trest	k1gInSc1	trest
se	se	k3xPyFc4	se
ukládá	ukládat	k5eAaImIp3nS	ukládat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
závažného	závažný	k2eAgInSc2d1	závažný
úmyslného	úmyslný	k2eAgInSc2d1	úmyslný
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
spáchaného	spáchaný	k2eAgInSc2d1	spáchaný
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
získání	získání	k1gNnSc2	získání
majetkového	majetkový	k2eAgInSc2d1	majetkový
prospěchu	prospěch	k1gInSc2	prospěch
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
či	či	k8xC	či
někoho	někdo	k3yInSc4	někdo
dalšího	další	k2eAgMnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgInSc4	tento
trest	trest	k1gInSc4	trest
uložit	uložit	k5eAaPmF	uložit
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
trestu	trest	k1gInSc2	trest
<g/>
,	,	kIx,	,
nepodmíněného	podmíněný	k2eNgInSc2d1	nepodmíněný
trestu	trest	k1gInSc2	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
za	za	k7c4	za
závažný	závažný	k2eAgInSc4d1	závažný
úmyslný	úmyslný	k2eAgInSc4d1	úmyslný
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
trest	trest	k1gInSc4	trest
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInSc1d1	další
trest	trest	k1gInSc1	trest
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ukládat	ukládat	k5eAaImF	ukládat
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc1	trest
propadnutí	propadnutí	k1gNnSc2	propadnutí
majetku	majetek	k1gInSc2	majetek
upravuje	upravovat	k5eAaImIp3nS	upravovat
ustanovení	ustanovení	k1gNnSc1	ustanovení
§	§	k?	§
66	[number]	k4	66
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Františka	František	k1gMnSc2	František
Púryho	Púry	k1gMnSc2	Púry
"	"	kIx"	"
<g/>
trest	trest	k1gInSc1	trest
propadnutí	propadnutí	k1gNnSc2	propadnutí
majetku	majetek	k1gInSc2	majetek
představuje	představovat	k5eAaImIp3nS	představovat
poměrně	poměrně	k6eAd1	poměrně
přísnou	přísný	k2eAgFnSc4d1	přísná
sankci	sankce	k1gFnSc4	sankce
výrazně	výrazně	k6eAd1	výrazně
zasahující	zasahující	k2eAgFnSc4d1	zasahující
do	do	k7c2	do
majetkové	majetkový	k2eAgFnSc2d1	majetková
sféry	sféra	k1gFnSc2	sféra
pachatele	pachatel	k1gMnSc2	pachatel
<g/>
.	.	kIx.	.
</s>
<s>
Uložení	uložení	k1gNnSc1	uložení
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
trestu	trest	k1gInSc2	trest
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
výjimek	výjimka	k1gFnPc2	výjimka
z	z	k7c2	z
ústavní	ústavní	k2eAgFnSc2d1	ústavní
ochrany	ochrana	k1gFnSc2	ochrana
nedotknutelnosti	nedotknutelnost	k1gFnSc2	nedotknutelnost
majetku	majetek	k1gInSc2	majetek
jako	jako	k8xS	jako
základního	základní	k2eAgNnSc2d1	základní
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
11	[number]	k4	11
LPS	LPS	kA	LPS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uložit	uložit	k5eAaPmF	uložit
trest	trest	k1gInSc4	trest
propadnutí	propadnutí	k1gNnSc2	propadnutí
majetku	majetek	k1gInSc2	majetek
jen	jen	k9	jen
za	za	k7c4	za
relativně	relativně	k6eAd1	relativně
úzký	úzký	k2eAgInSc4d1	úzký
okruh	okruh	k1gInSc4	okruh
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
(	(	kIx(	(
<g/>
zločinů	zločin	k1gInPc2	zločin
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c2	za
dalších	další	k2eAgFnPc2d1	další
omezujících	omezující	k2eAgFnPc2d1	omezující
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Propadnutí	propadnutí	k1gNnSc3	propadnutí
majetku	majetek	k1gInSc2	majetek
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
může	moct	k5eAaImIp3nS	moct
trest	trest	k1gInSc4	trest
postihnout	postihnout	k5eAaPmF	postihnout
buď	buď	k8xC	buď
veškerý	veškerý	k3xTgInSc4	veškerý
majetek	majetek	k1gInSc4	majetek
pachatele	pachatel	k1gMnSc2	pachatel
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pohledávky	pohledávka	k1gFnSc2	pohledávka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
určí	určit	k5eAaPmIp3nS	určit
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
ale	ale	k9	ale
postihuje	postihovat	k5eAaImIp3nS	postihovat
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gInSc1	jeho
osobní	osobní	k2eAgInSc1d1	osobní
majetek	majetek	k1gInSc1	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Komplikací	komplikace	k1gFnSc7	komplikace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
společná	společný	k2eAgFnSc1d1	společná
trestná	trestný	k2eAgFnSc1d1	trestná
činnost	činnost	k1gFnSc1	činnost
manželů	manžel	k1gMnPc2	manžel
v	v	k7c6	v
různosti	různost	k1gFnSc6	různost
míry	míra	k1gFnSc2	míra
jejich	jejich	k3xOp3gInSc2	jejich
podílu	podíl	k1gInSc2	podíl
na	na	k7c6	na
trestné	trestný	k2eAgFnSc6d1	trestná
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
<g/>
Propadnutí	propadnutí	k1gNnSc1	propadnutí
majetku	majetek	k1gInSc2	majetek
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
na	na	k7c4	na
prostředky	prostředek	k1gInPc4	prostředek
nebo	nebo	k8xC	nebo
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
k	k	k7c3	k
uspokojení	uspokojení	k1gNnSc3	uspokojení
životních	životní	k2eAgFnPc2d1	životní
potřeb	potřeba	k1gFnPc2	potřeba
odsouzeného	odsouzený	k1gMnSc2	odsouzený
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
posuzovaných	posuzovaný	k2eAgFnPc2d1	posuzovaná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
či	či	k8xC	či
osob	osoba	k1gFnPc2	osoba
o	o	k7c4	o
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
výživu	výživa	k1gFnSc4	výživa
nebo	nebo	k8xC	nebo
výchovu	výchova	k1gFnSc4	výchova
je	být	k5eAaImIp3nS	být
odsouzený	odsouzený	k1gMnSc1	odsouzený
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
povinen	povinen	k2eAgMnSc1d1	povinen
pečovat	pečovat	k5eAaImF	pečovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Propadnutí	propadnutí	k1gNnSc1	propadnutí
majetku	majetek	k1gInSc2	majetek
se	se	k3xPyFc4	se
však	však	k9	však
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
na	na	k7c4	na
majetek	majetek	k1gInSc4	majetek
nabytý	nabytý	k2eAgInSc4d1	nabytý
trestnou	trestný	k2eAgFnSc4d1	trestná
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pachatel	pachatel	k1gMnSc1	pachatel
nemá	mít	k5eNaImIp3nS	mít
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
majetkové	majetkový	k2eAgNnSc4d1	majetkové
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
nabytý	nabytý	k2eAgInSc4d1	nabytý
majetek	majetek	k1gInSc4	majetek
je	být	k5eAaImIp3nS	být
pachateli	pachatel	k1gMnSc3	pachatel
zabaven	zabavit	k5eAaPmNgInS	zabavit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
trestu	trest	k1gInSc2	trest
propadnutí	propadnutí	k1gNnSc2	propadnutí
věci	věc	k1gFnSc2	věc
(	(	kIx(	(
<g/>
§	§	k?	§
70	[number]	k4	70
TrZ	TrZ	k1gFnPc2	TrZ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Propadlý	propadlý	k2eAgInSc1d1	propadlý
majetek	majetek	k1gInSc1	majetek
připadá	připadat	k5eAaImIp3nS	připadat
státu	stát	k1gInSc2	stát
a	a	k8xC	a
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
§	§	k?	§
347	[number]	k4	347
TrZ	TrZ	k1gFnSc2	TrZ
právo	právo	k1gNnSc1	právo
majetek	majetek	k1gInSc4	majetek
obviněného	obviněný	k1gMnSc4	obviněný
zajistit	zajistit	k5eAaPmF	zajistit
již	již	k9	již
v	v	k7c6	v
přípravném	přípravný	k2eAgNnSc6d1	přípravné
řízení	řízení	k1gNnSc6	řízení
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
obava	obava	k1gFnSc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
výkon	výkon	k1gInSc1	výkon
tohoto	tento	k3xDgInSc2	tento
trestu	trest	k1gInSc2	trest
bude	být	k5eAaImBp3nS	být
zmařen	zmařen	k2eAgMnSc1d1	zmařen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ztížen	ztížen	k2eAgInSc1d1	ztížen
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
zajistí	zajistit	k5eAaPmIp3nS	zajistit
majetek	majetek	k1gInSc4	majetek
obviněného	obviněný	k1gMnSc2	obviněný
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
nabyl	nabýt	k5eAaPmAgInS	nabýt
rozsudek	rozsudek	k1gInSc4	rozsudek
právní	právní	k2eAgFnSc2d1	právní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
uložil	uložit	k5eAaPmAgInS	uložit
trest	trest	k1gInSc1	trest
propadnutí	propadnutí	k1gNnSc2	propadnutí
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Propadnutí	propadnutí	k1gNnSc3	propadnutí
majetku	majetek	k1gInSc2	majetek
právnické	právnický	k2eAgFnSc2d1	právnická
osoby	osoba	k1gFnSc2	osoba
==	==	k?	==
</s>
</p>
<p>
<s>
Trest	trest	k1gInSc1	trest
propadnutí	propadnutí	k1gNnSc2	propadnutí
majetku	majetek	k1gInSc2	majetek
lze	lze	k6eAd1	lze
uložit	uložit	k5eAaPmF	uložit
i	i	k9	i
právnické	právnický	k2eAgFnSc3d1	právnická
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zvláště	zvláště	k6eAd1	zvláště
závažnému	závažný	k2eAgInSc3d1	závažný
trestnému	trestný	k2eAgInSc3d1	trestný
činu	čin	k1gInSc3	čin
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
obohacení	obohacení	k1gNnSc2	obohacení
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
či	či	k8xC	či
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgNnSc2d1	jiné
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
pokus	pokus	k1gInSc4	pokus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
čin	čin	k1gInSc1	čin
dokonaný	dokonaný	k2eAgInSc1d1	dokonaný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trest	trest	k1gInSc1	trest
lze	lze	k6eAd1	lze
uložit	uložit	k5eAaPmF	uložit
i	i	k9	i
právnickým	právnický	k2eAgFnPc3d1	právnická
osobám	osoba	k1gFnPc3	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
trest	trest	k1gInSc1	trest
uložen	uložit	k5eAaPmNgInS	uložit
nějakému	nějaký	k3yIgInSc3	nějaký
finančnímu	finanční	k2eAgInSc3d1	finanční
ústavu	ústav	k1gInSc3	ústav
(	(	kIx(	(
<g/>
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
si	se	k3xPyFc3	se
soud	soud	k1gInSc4	soud
vyžádat	vyžádat	k5eAaPmF	vyžádat
vyjádření	vyjádření	k1gNnSc4	vyjádření
banky	banka	k1gFnSc2	banka
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
u	u	k7c2	u
trestu	trest	k1gInSc2	trest
zrušení	zrušení	k1gNnSc4	zrušení
právnické	právnický	k2eAgFnSc2d1	právnická
osoby	osoba	k1gFnSc2	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Propadnutí	propadnutí	k1gNnSc1	propadnutí
majetku	majetek	k1gInSc2	majetek
postihuje	postihovat	k5eAaImIp3nS	postihovat
celý	celý	k2eAgInSc4d1	celý
majetek	majetek	k1gInSc4	majetek
právnické	právnický	k2eAgFnSc2d1	právnická
osoby	osoba	k1gFnSc2	osoba
nebo	nebo	k8xC	nebo
tu	tu	k6eAd1	tu
jeho	jeho	k3xOp3gFnSc4	jeho
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
soud	soud	k1gInSc1	soud
určí	určit	k5eAaPmIp3nS	určit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Propadnutí	propadnutí	k1gNnSc1	propadnutí
věci	věc	k1gFnSc2	věc
</s>
</p>
