<p>
<s>
Med	med	k1gInSc1	med
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
sladká	sladký	k2eAgFnSc1d1	sladká
a	a	k8xC	a
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
vytvářená	vytvářený	k2eAgFnSc1d1	vytvářená
včelami	včela	k1gFnPc7	včela
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
i	i	k9	i
jiným	jiný	k2eAgInSc7d1	jiný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
)	)	kIx)	)
sběrem	sběr	k1gInSc7	sběr
a	a	k8xC	a
zahušťováním	zahušťování	k1gNnSc7	zahušťování
sladkých	sladký	k2eAgFnPc2d1	sladká
šťáv	šťáva	k1gFnPc2	šťáva
–	–	k?	–
především	především	k9	především
nektaru	nektar	k1gInSc2	nektar
z	z	k7c2	z
květů	květ	k1gInPc2	květ
(	(	kIx(	(
<g/>
med	med	k1gInSc4	med
květový	květový	k2eAgInSc4d1	květový
<g/>
)	)	kIx)	)
a	a	k8xC	a
medovice	medovice	k1gFnSc1	medovice
(	(	kIx(	(
<g/>
med	med	k1gInSc1	med
medovicový	medovicový	k2eAgInSc1d1	medovicový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
například	například	k6eAd1	například
mšice	mšice	k1gFnPc1	mšice
<g/>
.	.	kIx.	.
</s>
<s>
Sběrem	sběr	k1gInSc7	sběr
a	a	k8xC	a
přetvořením	přetvoření	k1gNnSc7	přetvoření
medovice	medovice	k1gFnSc1	medovice
vzniká	vznikat	k5eAaImIp3nS	vznikat
medovicový	medovicový	k2eAgInSc4d1	medovicový
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
<s>
Tmavost	tmavost	k1gFnSc1	tmavost
medovicového	medovicový	k2eAgInSc2d1	medovicový
medu	med	k1gInSc2	med
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
barvivy	barvivo	k1gNnPc7	barvivo
obsaženými	obsažený	k2eAgFnPc7d1	obsažená
v	v	k7c6	v
míze	míza	k1gFnSc6	míza
lesních	lesní	k2eAgFnPc2d1	lesní
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Prvními	první	k4xOgFnPc7	první
doklady	doklad	k1gInPc4	doklad
o	o	k7c4	o
využívání	využívání	k1gNnSc4	využívání
medu	med	k1gInSc2	med
lidmi	člověk	k1gMnPc7	člověk
jsou	být	k5eAaImIp3nP	být
jeskynní	jeskynní	k2eAgFnPc1d1	jeskynní
malby	malba	k1gFnPc1	malba
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
malba	malba	k1gFnSc1	malba
z	z	k7c2	z
Pavoučí	pavoučí	k2eAgFnSc2d1	pavoučí
jeskyně	jeskyně	k1gFnSc2	jeskyně
(	(	kIx(	(
<g/>
Cueva	Cueva	k1gFnSc1	Cueva
de	de	k?	de
la	la	k1gNnSc1	la
Arañ	Arañ	k1gFnSc1	Arañ
<g/>
)	)	kIx)	)
u	u	k7c2	u
Bicorp	Bicorp	k1gMnSc1	Bicorp
poblíž	poblíž	k7c2	poblíž
Valencie	Valencie	k1gFnSc2	Valencie
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Španělsku	Španělsko	k1gNnSc6	Španělsko
objevená	objevený	k2eAgFnSc1d1	objevená
v	v	k7c4	v
r.	r.	kA	r.
1919	[number]	k4	1919
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
nejméně	málo	k6eAd3	málo
8000	[number]	k4	8000
let	léto	k1gNnPc2	léto
a	a	k8xC	a
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
člověka	člověk	k1gMnSc4	člověk
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
liánách	liána	k1gFnPc6	liána
nebo	nebo	k8xC	nebo
provazech	provaz	k1gInPc6	provaz
z	z	k7c2	z
trávy	tráva	k1gFnSc2	tráva
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
vybírá	vybírat	k5eAaImIp3nS	vybírat
do	do	k7c2	do
košíku	košík	k1gInSc2	košík
med	med	k1gInSc4	med
z	z	k7c2	z
úlu	úl	k1gInSc2	úl
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
skalní	skalní	k2eAgFnSc6d1	skalní
puklině	puklina	k1gFnSc6	puklina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kolem	kolem	k6eAd1	kolem
létají	létat	k5eAaImIp3nP	létat
obrovské	obrovský	k2eAgFnPc4d1	obrovská
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
jeskynní	jeskynní	k2eAgFnPc1d1	jeskynní
malby	malba	k1gFnPc1	malba
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
z	z	k7c2	z
centrální	centrální	k2eAgFnSc2d1	centrální
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
především	především	k9	především
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
med	med	k1gInSc1	med
včelám	včela	k1gFnPc3	včela
příležitostně	příležitostně	k6eAd1	příležitostně
vybírají	vybírat	k5eAaImIp3nP	vybírat
i	i	k9	i
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
opic	opice	k1gFnPc2	opice
a	a	k8xC	a
lidoopů	lidoop	k1gMnPc2	lidoop
(	(	kIx(	(
<g/>
paviáni	pavián	k1gMnPc1	pavián
<g/>
,	,	kIx,	,
makakové	makakové	k?	makakové
<g/>
,	,	kIx,	,
orangutani	orangutan	k1gMnPc1	orangutan
<g/>
,	,	kIx,	,
gorily	gorila	k1gFnPc1	gorila
<g/>
,	,	kIx,	,
šimpanzi	šimpanz	k1gMnPc1	šimpanz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
dokonce	dokonce	k9	dokonce
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
energeticky	energeticky	k6eAd1	energeticky
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgInSc4d1	bohatý
med	med	k1gInSc4	med
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
larvami	larva	k1gFnPc7	larva
včel	včela	k1gFnPc2	včela
(	(	kIx(	(
<g/>
coby	coby	k?	coby
zdroj	zdroj	k1gInSc1	zdroj
proteinů	protein	k1gInPc2	protein
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
potravy	potrava	k1gFnSc2	potrava
už	už	k6eAd1	už
pro	pro	k7c4	pro
první	první	k4xOgMnPc4	první
příslušníky	příslušník	k1gMnPc4	příslušník
rodu	rod	k1gInSc2	rod
Homo	Homo	k1gMnSc1	Homo
<g/>
.	.	kIx.	.
<g/>
Písemné	písemný	k2eAgFnSc2d1	písemná
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c6	o
medu	med	k1gInSc6	med
najdeme	najít	k5eAaPmIp1nP	najít
už	už	k9	už
v	v	k7c6	v
nejstarších	starý	k2eAgInPc6d3	nejstarší
dochovaných	dochovaný	k2eAgInPc6d1	dochovaný
záznamech	záznam	k1gInPc6	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
antických	antický	k2eAgFnPc2d1	antická
kultur	kultura	k1gFnPc2	kultura
byl	být	k5eAaImAgInS	být
med	med	k1gInSc1	med
používán	používat	k5eAaImNgInS	používat
ve	v	k7c6	v
výživě	výživa	k1gFnSc6	výživa
i	i	k9	i
jako	jako	k9	jako
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
medu	med	k1gInSc6	med
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
2100-2000	[number]	k4	2100-2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
z	z	k7c2	z
hliněné	hliněný	k2eAgFnSc2d1	hliněná
destičky	destička	k1gFnSc2	destička
ze	z	k7c2	z
starověkého	starověký	k2eAgNnSc2d1	starověké
sumerského	sumerský	k2eAgNnSc2d1	sumerské
města	město	k1gNnSc2	město
Nippur	Nippura	k1gFnPc2	Nippura
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
zaznamenán	zaznamenán	k2eAgInSc4d1	zaznamenán
návod	návod	k1gInSc4	návod
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ošetřit	ošetřit	k5eAaPmF	ošetřit
zranění	zranění	k1gNnSc4	zranění
-	-	kIx~	-
"	"	kIx"	"
<g/>
rozemlít	rozemlít	k5eAaPmF	rozemlít
na	na	k7c4	na
prášek	prášek	k1gInSc4	prášek
říční	říční	k2eAgInSc1d1	říční
prach	prach	k1gInSc1	prach
...	...	k?	...
(	(	kIx(	(
<g/>
chybějící	chybějící	k2eAgNnPc4d1	chybějící
slova	slovo	k1gNnPc4	slovo
<g/>
)	)	kIx)	)
uhníst	uhníst	k5eAaPmF	uhníst
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
medu	med	k1gInSc6	med
a	a	k8xC	a
přelít	přelít	k5eAaPmF	přelít
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
a	a	k8xC	a
cedrovým	cedrový	k2eAgInSc7d1	cedrový
olejem	olej	k1gInSc7	olej
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
Starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
pochází	pocházet	k5eAaImIp3nS	pocházet
Ebersův	Ebersův	k2eAgInSc1d1	Ebersův
papyrus	papyrus	k1gInSc1	papyrus
datovaný	datovaný	k2eAgInSc1d1	datovaný
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
asi	asi	k9	asi
1550	[number]	k4	1550
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
med	med	k1gInSc4	med
v	v	k7c6	v
147	[number]	k4	147
lékařských	lékařský	k2eAgInPc6d1	lékařský
receptech	recept	k1gInPc6	recept
pro	pro	k7c4	pro
léčení	léčení	k1gNnSc4	léčení
vnějších	vnější	k2eAgNnPc2d1	vnější
zranění	zranění	k1gNnPc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Med	med	k1gInSc1	med
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
rostlinnými	rostlinný	k2eAgFnPc7d1	rostlinná
<g/>
,	,	kIx,	,
živočišnými	živočišný	k2eAgInPc7d1	živočišný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
anorganickými	anorganický	k2eAgFnPc7d1	anorganická
ingrediencemi	ingredience	k1gFnPc7	ingredience
<g/>
)	)	kIx)	)
k	k	k7c3	k
ošetření	ošetření	k1gNnSc3	ošetření
zánětů	zánět	k1gInPc2	zánět
<g/>
,	,	kIx,	,
vředů	vřed	k1gInPc2	vřed
<g/>
,	,	kIx,	,
popálenin	popálenina	k1gFnPc2	popálenina
<g/>
,	,	kIx,	,
ran	rána	k1gFnPc2	rána
i	i	k8xC	i
v	v	k7c6	v
pooperačních	pooperační	k2eAgInPc6d1	pooperační
stavech	stav	k1gInPc6	stav
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
hojení	hojení	k1gNnSc2	hojení
(	(	kIx(	(
<g/>
při	při	k7c6	při
obřízce	obřízka	k1gFnSc6	obřízka
<g/>
,	,	kIx,	,
propíchávání	propíchávání	k1gNnSc1	propíchávání
uší	ucho	k1gNnPc2	ucho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
složitějších	složitý	k2eAgFnPc2d2	složitější
operací	operace	k1gFnPc2	operace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
čípku	čípek	k1gInSc2	čípek
<g/>
,	,	kIx,	,
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
plešatosti	plešatost	k1gFnSc3	plešatost
<g/>
,	,	kIx,	,
zánětu	zánět	k1gInSc3	zánět
spojivek	spojivka	k1gFnPc2	spojivka
nebo	nebo	k8xC	nebo
i	i	k9	i
uvolnění	uvolnění	k1gNnSc3	uvolnění
ztuhlých	ztuhlý	k2eAgInPc2d1	ztuhlý
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
podobně	podobně	k6eAd1	podobně
léčivý	léčivý	k2eAgMnSc1d1	léčivý
byl	být	k5eAaImAgInS	být
med	med	k1gInSc1	med
považován	považován	k2eAgInSc1d1	považován
i	i	k9	i
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ve	v	k7c6	v
Starověké	starověký	k2eAgFnSc6d1	starověká
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradiční	tradiční	k2eAgFnSc2d1	tradiční
čínské	čínský	k2eAgFnSc2d1	čínská
medicíny	medicína	k1gFnSc2	medicína
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vyváženou	vyvážený	k2eAgFnSc4d1	vyvážená
poživatinu	poživatina	k1gFnSc4	poživatina
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
jangovou	jangový	k2eAgFnSc4d1	jangová
ani	ani	k8xC	ani
jingovou	jingový	k2eAgFnSc4d1	jingový
<g/>
)	)	kIx)	)
schopnou	schopný	k2eAgFnSc4d1	schopná
dobře	dobře	k6eAd1	dobře
spojovat	spojovat	k5eAaImF	spojovat
různé	různý	k2eAgFnPc4d1	různá
další	další	k2eAgFnPc4d1	další
léčivé	léčivý	k2eAgFnPc4d1	léčivá
látky	látka	k1gFnPc4	látka
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
rámec	rámec	k1gInSc4	rámec
své	svůj	k3xOyFgFnSc2	svůj
nutriční	nutriční	k2eAgFnSc2d1	nutriční
hodnoty	hodnota	k1gFnSc2	hodnota
měl	mít	k5eAaImAgInS	mít
med	med	k1gInSc4	med
i	i	k9	i
zde	zde	k6eAd1	zde
mnohostranné	mnohostranný	k2eAgNnSc4d1	mnohostranné
lékařské	lékařský	k2eAgNnSc4d1	lékařské
využití	využití	k1gNnSc4	využití
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jako	jako	k8xS	jako
detoxikační	detoxikační	k2eAgNnSc1d1	detoxikační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
,	,	kIx,	,
k	k	k7c3	k
úlevě	úleva	k1gFnSc3	úleva
od	od	k7c2	od
bolestí	bolest	k1gFnPc2	bolest
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
nespavosti	nespavost	k1gFnSc3	nespavost
<g/>
,	,	kIx,	,
stresu	stres	k1gInSc3	stres
<g/>
,	,	kIx,	,
zánětům	zánět	k1gInPc3	zánět
žaludku	žaludek	k1gInSc2	žaludek
a	a	k8xC	a
vředům	vřed	k1gInPc3	vřed
<g/>
,	,	kIx,	,
zácpě	zácpa	k1gFnSc3	zácpa
<g/>
,	,	kIx,	,
kašli	kašel	k1gInSc3	kašel
<g/>
,	,	kIx,	,
oparům	opar	k1gInPc3	opar
aj.	aj.	kA	aj.
<g/>
S	s	k7c7	s
využíváním	využívání	k1gNnSc7	využívání
medu	med	k1gInSc2	med
šel	jít	k5eAaImAgMnS	jít
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
i	i	k8xC	i
rozvoj	rozvoj	k1gInSc4	rozvoj
včelařství	včelařství	k1gNnSc2	včelařství
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
záznamy	záznam	k1gInPc1	záznam
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
včel	včela	k1gFnPc2	včela
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
už	už	k6eAd1	už
z	z	k7c2	z
r.	r.	kA	r.
2450	[number]	k4	2450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
dovednost	dovednost	k1gFnSc1	dovednost
postupně	postupně	k6eAd1	postupně
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
šířila	šířit	k5eAaImAgFnS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
středomoří	středomoří	k1gNnSc2	středomoří
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
znalosti	znalost	k1gFnPc1	znalost
dostaly	dostat	k5eAaPmAgFnP	dostat
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1500	[number]	k4	1500
př.n.l.	př.n.l.	k?	př.n.l.
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
chovali	chovat	k5eAaImAgMnP	chovat
včely	včela	k1gFnPc4	včela
v	v	k7c6	v
úlech	úl	k1gInPc6	úl
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
750	[number]	k4	750
př.n.l.	př.n.l.	k?	př.n.l.
jsou	být	k5eAaImIp3nP	být
doklady	doklad	k1gInPc4	doklad
z	z	k7c2	z
Asýrie	Asýrie	k1gFnSc2	Asýrie
a	a	k8xC	a
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
technologie	technologie	k1gFnSc2	technologie
doputovala	doputovat	k5eAaPmAgFnS	doputovat
nejspíše	nejspíše	k9	nejspíše
po	po	k7c6	po
hedvábné	hedvábný	k2eAgFnSc6d1	hedvábná
stezce	stezka	k1gFnSc6	stezka
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
Perské	perský	k2eAgFnSc2d1	perská
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
se	se	k3xPyFc4	se
včela	včela	k1gFnSc1	včela
medonosná	medonosný	k2eAgFnSc1d1	medonosná
posledních	poslední	k2eAgInPc2d1	poslední
několik	několik	k4yIc4	několik
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
nevyskytovala	vyskytovat	k5eNaImAgFnS	vyskytovat
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přivezena	přivézt	k5eAaPmNgFnS	přivézt
kolonisty	kolonista	k1gMnPc7	kolonista
až	až	k8xS	až
v	v	k7c4	v
r.	r.	kA	r.
1622	[number]	k4	1622
<g/>
.	.	kIx.	.
</s>
<s>
Severoameričtí	severoamerický	k2eAgMnPc1d1	severoamerický
Indiáni	Indián	k1gMnPc1	Indián
jako	jako	k9	jako
sladidlo	sladidlo	k1gNnSc4	sladidlo
využívali	využívat	k5eAaPmAgMnP	využívat
tradičně	tradičně	k6eAd1	tradičně
javorový	javorový	k2eAgInSc4d1	javorový
syrup	syrup	k1gInSc4	syrup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
částech	část	k1gFnPc6	část
Ameriky	Amerika	k1gFnSc2	Amerika
však	však	k9	však
med	med	k1gInSc4	med
znám	znát	k5eAaImIp1nS	znát
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	on	k3xPp3gNnPc4	on
obývá	obývat	k5eAaImIp3nS	obývat
množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
med	med	k1gInSc4	med
produkujících	produkující	k2eAgFnPc2d1	produkující
včel	včela	k1gFnPc2	včela
<g/>
,	,	kIx,	,
zvláště	zvláště	k9	zvláště
rody	rod	k1gInPc1	rod
Melipona	Melipon	k1gMnSc2	Melipon
a	a	k8xC	a
Trigona	Trigon	k1gMnSc2	Trigon
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
zmínky	zmínka	k1gFnPc1	zmínka
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
od	od	k7c2	od
Mayů	May	k1gMnPc2	May
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
domestikovali	domestikovat	k5eAaBmAgMnP	domestikovat
včelu	včela	k1gFnSc4	včela
Melipona	Melipon	k1gMnSc2	Melipon
beecheii	beecheie	k1gFnSc6	beecheie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
včely	včela	k1gFnPc1	včela
nemají	mít	k5eNaImIp3nP	mít
žihadlo	žihadlo	k1gNnSc1	žihadlo
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
neagresivní	agresivní	k2eNgFnPc1d1	neagresivní
<g/>
,	,	kIx,	,
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
ale	ale	k8xC	ale
tvoří	tvořit	k5eAaImIp3nP	tvořit
méně	málo	k6eAd2	málo
medu	med	k1gInSc2	med
a	a	k8xC	a
pracněji	pracně	k6eAd2	pracně
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
-	-	kIx~	-
neukládají	ukládat	k5eNaImIp3nP	ukládat
med	med	k1gInSc4	med
do	do	k7c2	do
plástů	plást	k1gInPc2	plást
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
medových	medový	k2eAgInPc2d1	medový
pohárků	pohárek	k1gInPc2	pohárek
<g/>
.	.	kIx.	.
</s>
<s>
Med	med	k1gInSc1	med
je	být	k5eAaImIp3nS	být
řidší	řídký	k2eAgInSc1d2	řidší
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
pít	pít	k5eAaImF	pít
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
i	i	k9	i
senzorickými	senzorický	k2eAgFnPc7d1	senzorická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
velmi	velmi	k6eAd1	velmi
zdravý	zdravý	k2eAgMnSc1d1	zdravý
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
i	i	k9	i
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
med	med	k1gInSc1	med
znám	znám	k2eAgInSc1d1	znám
a	a	k8xC	a
používán	používán	k2eAgInSc1d1	používán
jako	jako	k8xS	jako
účinný	účinný	k2eAgInSc1d1	účinný
lék	lék	k1gInSc1	lék
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
<g/>
Mayové	May	k1gMnPc1	May
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
medu	med	k1gInSc6	med
zdatní	zdatný	k2eAgMnPc1d1	zdatný
a	a	k8xC	a
med	med	k1gInSc1	med
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgInSc7d1	důležitý
obchodním	obchodní	k2eAgInSc7d1	obchodní
artiklem	artikl	k1gInSc7	artikl
(	(	kIx(	(
<g/>
v	v	k7c6	v
Amazonii	Amazonie	k1gFnSc6	Amazonie
byli	být	k5eAaImAgMnP	být
obdobně	obdobně	k6eAd1	obdobně
zdatní	zdatný	k2eAgMnPc1d1	zdatný
Kayapové	Kayap	k1gMnPc1	Kayap
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
med	med	k1gInSc4	med
prodávali	prodávat	k5eAaImAgMnP	prodávat
okolním	okolní	k2eAgFnPc3d1	okolní
etnickým	etnický	k2eAgFnPc3d1	etnická
skupinám	skupina	k1gFnPc3	skupina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
významu	význam	k1gInSc6	význam
medu	med	k1gInSc2	med
pro	pro	k7c4	pro
Maye	May	k1gMnSc4	May
svědčí	svědčit	k5eAaImIp3nS	svědčit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
včely	včela	k1gFnPc1	včela
byly	být	k5eAaImAgFnP	být
uctívány	uctívat	k5eAaImNgFnP	uctívat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
symbolem	symbol	k1gInSc7	symbol
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
svého	svůj	k3xOyFgMnSc4	svůj
boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nahrazení	nahrazení	k1gNnSc4	nahrazení
medu	med	k1gInSc2	med
cukrem	cukr	k1gInSc7	cukr
===	===	k?	===
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
r.	r.	kA	r.
510	[number]	k4	510
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vládce	vládce	k1gMnSc1	vládce
Persie	Persie	k1gFnSc2	Persie
Dareios	Dareios	k1gMnSc1	Dareios
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
cukrové	cukrový	k2eAgFnSc6d1	cukrová
třtině	třtina	k1gFnSc6	třtina
jako	jako	k8xS	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
rákosu	rákos	k1gInSc3	rákos
dávajícím	dávající	k2eAgInSc7d1	dávající
med	med	k1gInSc4	med
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
není	být	k5eNaImIp3nS	být
vytvářen	vytvářet	k5eAaImNgInS	vytvářet
včelami	včela	k1gFnPc7	včela
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
med	med	k1gInSc1	med
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
nahrazen	nahradit	k5eAaPmNgInS	nahradit
cukrem	cukr	k1gInSc7	cukr
až	až	k6eAd1	až
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
staletí	staletí	k1gNnPc2	staletí
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
sladit	sladit	k5eAaPmF	sladit
cukrem	cukr	k1gInSc7	cukr
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
350	[number]	k4	350
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
cukr	cukr	k1gInSc1	cukr
objevil	objevit	k5eAaPmAgInS	objevit
až	až	k9	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
křižáckých	křižácký	k2eAgFnPc2d1	křižácká
výprav	výprava	k1gFnPc2	výprava
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xC	jako
nový	nový	k2eAgInSc1d1	nový
druh	druh	k1gInSc1	druh
luxusního	luxusní	k2eAgNnSc2d1	luxusní
koření	koření	k1gNnSc2	koření
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1480	[number]	k4	1480
se	se	k3xPyFc4	se
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
již	již	k6eAd1	již
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
středomořské	středomořský	k2eAgFnSc6d1	středomořská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cukr	cukr	k1gInSc1	cukr
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
příliš	příliš	k6eAd1	příliš
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
patřily	patřit	k5eAaImAgFnP	patřit
cukrovinky	cukrovinka	k1gFnPc1	cukrovinka
jen	jen	k9	jen
do	do	k7c2	do
jídelníčku	jídelníček	k1gInSc2	jídelníček
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Cukr	cukr	k1gInSc1	cukr
začal	začít	k5eAaPmAgInS	začít
skutečně	skutečně	k6eAd1	skutečně
zlevňovat	zlevňovat	k5eAaImF	zlevňovat
teprve	teprve	k6eAd1	teprve
až	až	k9	až
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
zámořského	zámořský	k2eAgNnSc2d1	zámořské
plantážnictví	plantážnictví	k1gNnSc2	plantážnictví
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Střední	střední	k2eAgFnSc3d1	střední
Americe	Amerika	k1gFnSc3	Amerika
a	a	k8xC	a
ke	k	k7c3	k
konečnému	konečný	k2eAgNnSc3d1	konečné
nahrazení	nahrazení	k1gNnSc3	nahrazení
medu	med	k1gInSc2	med
výrazně	výrazně	k6eAd1	výrazně
levnějším	levný	k2eAgInSc7d2	levnější
cukrem	cukr	k1gInSc7	cukr
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
řepného	řepný	k2eAgInSc2d1	řepný
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
zefektivnění	zefektivnění	k1gNnSc1	zefektivnění
jeho	jeho	k3xOp3gFnSc2	jeho
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
světová	světový	k2eAgFnSc1d1	světová
roční	roční	k2eAgFnSc1d1	roční
produkce	produkce	k1gFnSc1	produkce
medu	med	k1gInSc2	med
kolem	kolem	k7c2	kolem
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
<g/>
%	%	kIx~	%
roční	roční	k2eAgFnSc1d1	roční
produkce	produkce	k1gFnSc1	produkce
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc4	význam
a	a	k8xC	a
základní	základní	k2eAgFnPc4d1	základní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
včelstvo	včelstvo	k1gNnSc4	včelstvo
je	být	k5eAaImIp3nS	být
med	med	k1gInSc1	med
zásobou	zásoba	k1gFnSc7	zásoba
energeticky	energeticky	k6eAd1	energeticky
bohaté	bohatý	k2eAgFnSc2d1	bohatá
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
silné	silný	k2eAgNnSc1d1	silné
včelstvo	včelstvo	k1gNnSc1	včelstvo
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
a	a	k8xC	a
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
i	i	k9	i
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
řádově	řádově	k6eAd1	řádově
stovky	stovka	k1gFnPc4	stovka
kg	kg	kA	kg
medu	med	k1gInSc6	med
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
spotřeby	spotřeba	k1gFnSc2	spotřeba
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
<g/>
,	,	kIx,	,
na	na	k7c6	na
přečkání	přečkání	k1gNnSc6	přečkání
zimy	zima	k1gFnSc2	zima
včelstvu	včelstvo	k1gNnSc3	včelstvo
stačí	stačit	k5eAaBmIp3nS	stačit
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
kg	kg	kA	kg
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Med	med	k1gInSc1	med
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
mu	on	k3xPp3gMnSc3	on
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
cukrem	cukr	k1gInSc7	cukr
a	a	k8xC	a
jinými	jiný	k2eAgNnPc7d1	jiné
sladidly	sladidlo	k1gNnPc7	sladidlo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vhodné	vhodný	k2eAgFnPc4d1	vhodná
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
pro	pro	k7c4	pro
pečení	pečení	k1gNnSc4	pečení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapalný	kapalný	k2eAgInSc1d1	kapalný
med	med	k1gInSc1	med
se	se	k3xPyFc4	se
nekazí	kazit	k5eNaImIp3nS	kazit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
není	být	k5eNaImIp3nS	být
nepřiměřené	přiměřený	k2eNgNnSc1d1	nepřiměřené
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
nezralý	zralý	k2eNgInSc1d1	nezralý
med	med	k1gInSc1	med
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
koncentraci	koncentrace	k1gFnSc3	koncentrace
cukrů	cukr	k1gInPc2	cukr
ničí	ničit	k5eAaImIp3nS	ničit
bakterie	bakterie	k1gFnSc1	bakterie
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
zvaném	zvaný	k2eAgInSc6d1	zvaný
osmóza	osmóza	k1gFnSc1	osmóza
(	(	kIx(	(
<g/>
vysává	vysávat	k5eAaImIp3nS	vysávat
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ničí	ničit	k5eAaImIp3nS	ničit
jejich	jejich	k3xOp3gFnSc2	jejich
membrány	membrána	k1gFnSc2	membrána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
spory	spor	k1gInPc1	spor
některých	některý	k3yIgFnPc2	některý
bakterií	bakterie	k1gFnPc2	bakterie
osmóze	osmóza	k1gFnSc3	osmóza
odolávají	odolávat	k5eAaImIp3nP	odolávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
medu	med	k1gInSc6	med
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
uchytit	uchytit	k5eAaPmF	uchytit
ani	ani	k8xC	ani
kvasinky	kvasinka	k1gFnSc2	kvasinka
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsah	obsah	k1gInSc1	obsah
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
neupravený	upravený	k2eNgInSc1d1	neupravený
med	med	k1gInSc1	med
má	mít	k5eAaImIp3nS	mít
obsah	obsah	k1gInSc4	obsah
vody	voda	k1gFnSc2	voda
asi	asi	k9	asi
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
obsah	obsah	k1gInSc1	obsah
vody	voda	k1gFnSc2	voda
pod	pod	k7c7	pod
18	[number]	k4	18
%	%	kIx~	%
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
v	v	k7c6	v
medu	med	k1gInSc6	med
množit	množit	k5eAaImF	množit
prakticky	prakticky	k6eAd1	prakticky
žádný	žádný	k3yNgInSc4	žádný
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
skladování	skladování	k1gNnSc6	skladování
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc6d2	vyšší
<g/>
)	)	kIx)	)
teplotě	teplota	k1gFnSc6	teplota
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
díky	díky	k7c3	díky
sedimentaci	sedimentace	k1gFnSc3	sedimentace
(	(	kIx(	(
<g/>
usazování	usazování	k1gNnSc3	usazování
<g/>
)	)	kIx)	)
cukrů	cukr	k1gInPc2	cukr
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
medu	med	k1gInSc2	med
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
vytvořit	vytvořit	k5eAaPmF	vytvořit
tenká	tenký	k2eAgFnSc1d1	tenká
řidší	řídký	k2eAgFnSc1d2	řidší
vrstvička	vrstvička	k1gFnSc1	vrstvička
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
rozmnoží	rozmnožit	k5eAaPmIp3nP	rozmnožit
osmofilní	osmofilní	k2eAgInPc1d1	osmofilní
druhy	druh	k1gInPc1	druh
kvasinek	kvasinka	k1gFnPc2	kvasinka
(	(	kIx(	(
<g/>
zkvášejí	zkvášet	k5eAaImIp3nP	zkvášet
koncentrované	koncentrovaný	k2eAgFnPc1d1	koncentrovaná
roztoky	roztoka	k1gFnPc1	roztoka
cukrů	cukr	k1gInPc2	cukr
–	–	k?	–
nad	nad	k7c7	nad
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
medu	med	k1gInSc6	med
přirozeně	přirozeně	k6eAd1	přirozeně
obsaženy	obsažen	k2eAgInPc1d1	obsažen
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
kvašení	kvašení	k1gNnSc1	kvašení
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
především	především	k9	především
senzorické	senzorický	k2eAgNnSc1d1	senzorické
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
nutriční	nutriční	k2eAgFnPc4d1	nutriční
vlastnosti	vlastnost	k1gFnPc4	vlastnost
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Složení	složení	k1gNnSc1	složení
===	===	k?	===
</s>
</p>
<p>
<s>
Med	med	k1gInSc1	med
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc4	směs
cukrů	cukr	k1gInPc2	cukr
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Specifické	specifický	k2eAgNnSc1d1	specifické
složení	složení	k1gNnSc1	složení
medu	med	k1gInSc2	med
závisí	záviset	k5eAaImIp3nS	záviset
nejvíc	nejvíc	k6eAd1	nejvíc
na	na	k7c4	na
směsi	směs	k1gFnPc4	směs
květů	květ	k1gInPc2	květ
navštívených	navštívený	k2eAgInPc2d1	navštívený
včelami	včela	k1gFnPc7	včela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
med	med	k1gInSc1	med
produkovaly	produkovat	k5eAaImAgFnP	produkovat
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
lokality	lokalita	k1gFnSc2	lokalita
<g/>
,	,	kIx,	,
období	období	k1gNnSc2	období
<g/>
/	/	kIx~	/
<g/>
sezony	sezona	k1gFnSc2	sezona
i	i	k8xC	i
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
včelstev	včelstvo	k1gNnPc2	včelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
1	[number]	k4	1
litru	litr	k1gInSc2	litr
medu	med	k1gInSc2	med
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1,4	[number]	k4	1,4
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgNnSc1d1	typické
složení	složení	k1gNnSc1	složení
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rozborem	rozbor	k1gInSc7	rozbor
pylů	pyl	k1gInPc2	pyl
a	a	k8xC	a
výtrusů	výtrus	k1gInPc2	výtrus
(	(	kIx(	(
<g/>
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
v	v	k7c6	v
neupraveném	upravený	k2eNgInSc6d1	neupravený
medu	med	k1gInSc6	med
(	(	kIx(	(
<g/>
Melisopalynologie	Melisopalynologie	k1gFnSc1	Melisopalynologie
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
byl	být	k5eAaImAgInS	být
sesbírán	sesbírat	k5eAaPmNgInS	sesbírat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
včely	včela	k1gFnPc1	včela
přenášejí	přenášet	k5eAaImIp3nP	přenášet
elektrostatický	elektrostatický	k2eAgInSc4d1	elektrostatický
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
přitahovat	přitahovat	k5eAaImF	přitahovat
jiné	jiný	k2eAgFnPc1d1	jiná
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
techniky	technika	k1gFnPc1	technika
Melisopalynologie	Melisopalynologie	k1gFnSc2	Melisopalynologie
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
environmentálních	environmentální	k2eAgFnPc2d1	environmentální
studií	studie	k1gFnPc2	studie
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
částicového	částicový	k2eAgNnSc2d1	částicové
znečištění	znečištění	k1gNnSc2	znečištění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
zátěž	zátěž	k1gFnSc1	zátěž
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
medu	med	k1gInSc6	med
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
vzorky	vzorek	k1gInPc7	vzorek
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
překvapivě	překvapivě	k6eAd1	překvapivě
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rostliny	rostlina	k1gFnPc4	rostlina
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
květy	květ	k1gInPc1	květ
reprodukční	reprodukční	k2eAgInPc1d1	reprodukční
orgány	orgán	k1gInPc1	orgán
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
ochráněné	ochráněný	k2eAgNnSc1d1	ochráněné
před	před	k7c7	před
znečištěním	znečištění	k1gNnSc7	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
autentický	autentický	k2eAgInSc1d1	autentický
med	med	k1gInSc1	med
velmi	velmi	k6eAd1	velmi
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
cizorodých	cizorodý	k2eAgFnPc2d1	cizorodá
látek	látka	k1gFnPc2	látka
se	se	k3xPyFc4	se
do	do	k7c2	do
medu	med	k1gInSc2	med
dostává	dostávat	k5eAaImIp3nS	dostávat
až	až	k9	až
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsoby	způsob	k1gInPc1	způsob
těžby	těžba	k1gFnSc2	těžba
medu	med	k1gInSc2	med
==	==	k?	==
</s>
</p>
<p>
<s>
Plástečkový	Plástečkový	k2eAgInSc1d1	Plástečkový
med	med	k1gInSc1	med
–	–	k?	–
tento	tento	k3xDgInSc4	tento
med	med	k1gInSc4	med
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
včelaře	včelař	k1gMnSc2	včelař
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
specializované	specializovaný	k2eAgFnSc6d1	specializovaná
prodejně	prodejna	k1gFnSc6	prodejna
<g/>
.	.	kIx.	.
</s>
<s>
Kupujeme	kupovat	k5eAaImIp1nP	kupovat
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
plástečky	plásteček	k1gInPc4	plásteček
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc4	jejich
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
je	on	k3xPp3gMnPc4	on
zavěsíme	zavěsit	k5eAaPmIp1nP	zavěsit
<g/>
,	,	kIx,	,
otevřeme	otevřít	k5eAaPmIp1nP	otevřít
a	a	k8xC	a
necháme	nechat	k5eAaPmIp1nP	nechat
med	med	k1gInSc4	med
pozvolna	pozvolna	k6eAd1	pozvolna
vytékat	vytékat	k5eAaImF	vytékat
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
získáme	získat	k5eAaPmIp1nP	získat
med	med	k1gInSc4	med
šetrně	šetrně	k6eAd1	šetrně
a	a	k8xC	a
uchováme	uchovat	k5eAaPmIp1nP	uchovat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
většinu	většina	k1gFnSc4	většina
cenných	cenný	k2eAgFnPc2d1	cenná
přírodních	přírodní	k2eAgFnPc2d1	přírodní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapaný	kapaný	k2eAgInSc1d1	kapaný
med	med	k1gInSc1	med
–	–	k?	–
tento	tento	k3xDgInSc4	tento
med	med	k1gInSc4	med
včelař	včelař	k1gMnSc1	včelař
vytěží	vytěžit	k5eAaPmIp3nS	vytěžit
výše	vysoce	k6eAd2	vysoce
uvedeným	uvedený	k2eAgInSc7d1	uvedený
postupem	postup	k1gInSc7	postup
a	a	k8xC	a
prodává	prodávat	k5eAaImIp3nS	prodávat
ho	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
ve	v	k7c6	v
sklenicích	sklenice	k1gFnPc6	sklenice
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
náročnosti	náročnost	k1gFnSc3	náročnost
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
odkapáním	odkapání	k1gNnSc7	odkapání
se	se	k3xPyFc4	se
vytěží	vytěžit	k5eAaPmIp3nS	vytěžit
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
medu	med	k1gInSc2	med
obsaženého	obsažený	k2eAgInSc2d1	obsažený
v	v	k7c6	v
plástech	plást	k1gInPc6	plást
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kapaný	kapaný	k2eAgInSc1d1	kapaný
med	med	k1gInSc1	med
sice	sice	k8xC	sice
drahý	drahý	k2eAgInSc1d1	drahý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
hodnotný	hodnotný	k2eAgInSc1d1	hodnotný
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
léčebné	léčebný	k2eAgInPc4d1	léčebný
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
nejlépe	dobře	k6eAd3	dobře
hodí	hodit	k5eAaImIp3nS	hodit
tento	tento	k3xDgInSc4	tento
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stáčený	stáčený	k2eAgInSc1d1	stáčený
med	med	k1gInSc1	med
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
běžný	běžný	k2eAgInSc4d1	běžný
med	med	k1gInSc4	med
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kupujeme	kupovat	k5eAaImIp1nP	kupovat
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plástů	plást	k1gInPc2	plást
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
odstřeďováním	odstřeďování	k1gNnSc7	odstřeďování
v	v	k7c6	v
medometu	medomet	k1gInSc6	medomet
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
med	med	k1gInSc1	med
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
velmi	velmi	k6eAd1	velmi
hodnotný	hodnotný	k2eAgMnSc1d1	hodnotný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
většinu	většina	k1gFnSc4	většina
přírodních	přírodní	k2eAgFnPc2d1	přírodní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odstřeďování	odstřeďování	k1gNnSc6	odstřeďování
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
malé	malý	k2eAgFnPc1d1	malá
kapičky	kapička	k1gFnPc1	kapička
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
obsažených	obsažený	k2eAgFnPc2d1	obsažená
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
někteří	některý	k3yIgMnPc1	některý
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
plástečkový	plástečkový	k2eAgInSc4d1	plástečkový
nebo	nebo	k8xC	nebo
kapaný	kapaný	k2eAgInSc4d1	kapaný
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lisovaný	lisovaný	k2eAgInSc1d1	lisovaný
med	med	k1gInSc1	med
–	–	k?	–
je	být	k5eAaImIp3nS	být
med	med	k1gInSc4	med
vytlačovaný	vytlačovaný	k2eAgInSc4d1	vytlačovaný
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
z	z	k7c2	z
plástů	plást	k1gInPc2	plást
<g/>
.	.	kIx.	.
</s>
<s>
Lisování	lisování	k1gNnSc1	lisování
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
způsobem	způsob	k1gInSc7	způsob
získávání	získávání	k1gNnSc2	získávání
medu	med	k1gInSc2	med
<g/>
;	;	kIx,	;
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
medometu	medomet	k1gInSc2	medomet
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
téměř	téměř	k6eAd1	téměř
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
tuhý	tuhý	k2eAgInSc1d1	tuhý
a	a	k8xC	a
rosolovitý	rosolovitý	k2eAgInSc1d1	rosolovitý
vřesový	vřesový	k2eAgInSc1d1	vřesový
med	med	k1gInSc1	med
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
někdy	někdy	k6eAd1	někdy
kromě	kromě	k7c2	kromě
odstřeďování	odstřeďování	k1gNnSc2	odstřeďování
také	také	k9	také
lisuje	lisovat	k5eAaImIp3nS	lisovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tekutý	tekutý	k2eAgInSc1d1	tekutý
med	med	k1gInSc1	med
–	–	k?	–
med	med	k1gInSc4	med
horší	zlý	k2eAgFnSc2d2	horší
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
kombinací	kombinace	k1gFnSc7	kombinace
lisování	lisování	k1gNnSc2	lisování
a	a	k8xC	a
zahřívání	zahřívání	k1gNnSc2	zahřívání
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgNnSc2	tento
zpracování	zpracování	k1gNnSc2	zpracování
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
včelaře	včelař	k1gMnSc4	včelař
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
všechny	všechen	k3xTgInPc1	všechen
zbytky	zbytek	k1gInPc1	zbytek
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
nad	nad	k7c7	nad
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
zničí	zničit	k5eAaPmIp3nS	zničit
všechny	všechen	k3xTgFnPc4	všechen
cenné	cenný	k2eAgFnPc4d1	cenná
přírodní	přírodní	k2eAgFnPc4d1	přírodní
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
enzymy	enzym	k1gInPc4	enzym
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umělý	umělý	k2eAgInSc1d1	umělý
med	med	k1gInSc1	med
–	–	k?	–
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
umělý	umělý	k2eAgInSc1d1	umělý
med	med	k1gInSc1	med
lze	lze	k6eAd1	lze
nazvat	nazvat	k5eAaPmF	nazvat
cukrovou	cukrový	k2eAgFnSc7d1	cukrová
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
včelí	včelí	k2eAgInSc1d1	včelí
produkt	produkt	k1gInSc1	produkt
-	-	kIx~	-
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
invertního	invertní	k2eAgInSc2d1	invertní
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
sirupu	sirup	k1gInSc2	sirup
z	z	k7c2	z
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
svému	svůj	k3xOyFgInSc3	svůj
názvu	název	k1gInSc3	název
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
medem	med	k1gInSc7	med
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
.	.	kIx.	.
</s>
<s>
Umělý	umělý	k2eAgInSc1d1	umělý
med	med	k1gInSc1	med
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
prodává	prodávat	k5eAaImIp3nS	prodávat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
krém	krém	k1gInSc1	krém
z	z	k7c2	z
invertního	invertní	k2eAgInSc2d1	invertní
cukru	cukr	k1gInSc2	cukr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
Med	med	k1gInSc1	med
na	na	k7c6	na
pečení	pečení	k1gNnSc6	pečení
-	-	kIx~	-
vhodný	vhodný	k2eAgMnSc1d1	vhodný
též	též	k9	též
do	do	k7c2	do
čaje	čaj	k1gInSc2	čaj
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Med	med	k1gInSc4	med
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
==	==	k?	==
</s>
</p>
<p>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
kuchyni	kuchyně	k1gFnSc6	kuchyně
používat	používat	k5eAaImF	používat
třtinový	třtinový	k2eAgInSc4d1	třtinový
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
řepný	řepný	k2eAgInSc4d1	řepný
cukr	cukr	k1gInSc4	cukr
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc7d1	hlavní
sladidlem	sladidlo	k1gNnSc7	sladidlo
byl	být	k5eAaImAgInS	být
med	med	k1gInSc1	med
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
zejména	zejména	k9	zejména
do	do	k7c2	do
kaší	kaše	k1gFnPc2	kaše
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
nápojů	nápoj	k1gInPc2	nápoj
(	(	kIx(	(
<g/>
medovina	medovina	k1gFnSc1	medovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
masitých	masitý	k2eAgNnPc2d1	masité
jídel	jídlo	k1gNnPc2	jídlo
<g/>
,	,	kIx,	,
na	na	k7c6	na
některé	některý	k3yIgFnSc6	některý
věci	věc	k1gFnSc6	věc
se	se	k3xPyFc4	se
však	však	k9	však
nehodil	hodit	k5eNaPmAgMnS	hodit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
piškot	piškot	k1gInSc4	piškot
a	a	k8xC	a
cukrovinky	cukrovinka	k1gFnPc4	cukrovinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dost	dost	k6eAd1	dost
časté	častý	k2eAgNnSc1d1	časté
bylo	být	k5eAaImAgNnS	být
spojení	spojení	k1gNnSc1	spojení
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
pepře	pepř	k1gInSc2	pepř
(	(	kIx(	(
<g/>
peprník	peprník	k1gInSc1	peprník
<g/>
→	→	k?	→
<g/>
perník	perník	k1gInSc1	perník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zázvoru	zázvor	k1gInSc2	zázvor
a	a	k8xC	a
šafránu	šafrán	k1gInSc2	šafrán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgInPc1d1	zdravotní
účinky	účinek	k1gInPc1	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c4	po
2700	[number]	k4	2700
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
med	med	k1gInSc4	med
užíván	užíván	k2eAgInSc4d1	užíván
pro	pro	k7c4	pro
léčení	léčení	k1gNnSc4	léčení
chorob	choroba	k1gFnPc2	choroba
včetně	včetně	k7c2	včetně
tropických	tropický	k2eAgMnPc2d1	tropický
až	až	k6eAd1	až
do	do	k7c2	do
plného	plný	k2eAgNnSc2d1	plné
pochopení	pochopení	k1gNnSc2	pochopení
příčin	příčina	k1gFnPc2	příčina
infekce	infekce	k1gFnSc2	infekce
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
dobách	doba	k1gFnPc6	doba
a	a	k8xC	a
nasazení	nasazení	k1gNnSc4	nasazení
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgNnSc1d1	lidové
používání	používání	k1gNnSc1	používání
medu	med	k1gInSc2	med
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostalo	dostat	k5eAaPmAgNnS	dostat
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
<g/>
:	:	kIx,	:
med	med	k1gInSc1	med
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
antibakteriální	antibakteriální	k2eAgMnSc1d1	antibakteriální
a	a	k8xC	a
antiseptický	antiseptický	k2eAgMnSc1d1	antiseptický
činitel	činitel	k1gMnSc1	činitel
<g/>
.	.	kIx.	.
</s>
<s>
Antibakteriální	antibakteriální	k2eAgFnPc1d1	antibakteriální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
medu	med	k1gInSc2	med
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
nízké	nízký	k2eAgFnSc2d1	nízká
aktivity	aktivita	k1gFnSc2	aktivita
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
medu	med	k1gInSc6	med
způsobující	způsobující	k2eAgFnSc4d1	způsobující
osmózu	osmóza	k1gFnSc4	osmóza
<g/>
,	,	kIx,	,
efekt	efekt	k1gInSc4	efekt
peroxidu	peroxid	k1gInSc2	peroxid
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
kyselosti	kyselost	k1gFnSc2	kyselost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osmóza	osmóza	k1gFnSc1	osmóza
===	===	k?	===
</s>
</p>
<p>
<s>
Med	med	k1gInSc1	med
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
nasycený	nasycený	k2eAgInSc4d1	nasycený
roztok	roztok	k1gInSc4	roztok
dvou	dva	k4xCgInPc2	dva
monosacharidů	monosacharid	k1gInPc2	monosacharid
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
má	mít	k5eAaImIp3nS	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
vodní	vodní	k2eAgFnSc4d1	vodní
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
vodních	vodní	k2eAgFnPc2d1	vodní
molekul	molekula	k1gFnPc2	molekula
je	být	k5eAaImIp3nS	být
vázána	vázán	k2eAgFnSc1d1	vázána
cukry	cukr	k1gInPc7	cukr
a	a	k8xC	a
jen	jen	k9	jen
zbývající	zbývající	k2eAgMnPc1d1	zbývající
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
mikroorganismy	mikroorganismus	k1gInPc4	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nízkému	nízký	k2eAgInSc3d1	nízký
počtu	počet	k1gInSc3	počet
volných	volný	k2eAgFnPc2d1	volná
molekul	molekula	k1gFnPc2	molekula
vody	voda	k1gFnSc2	voda
nemají	mít	k5eNaImIp3nP	mít
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
vhodné	vhodný	k2eAgFnSc2d1	vhodná
podmínky	podmínka	k1gFnSc2	podmínka
k	k	k7c3	k
množení	množení	k1gNnSc3	množení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Peroxid	peroxid	k1gInSc1	peroxid
vodíku	vodík	k1gInSc2	vodík
===	===	k?	===
</s>
</p>
<p>
<s>
Vlivem	vliv	k1gInSc7	vliv
přítomného	přítomný	k2eAgInSc2d1	přítomný
enzymu	enzym	k1gInSc2	enzym
glukózaoxidáze	glukózaoxidáze	k1gFnSc2	glukózaoxidáze
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
medu	med	k1gInSc6	med
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
peroxidu	peroxid	k1gInSc2	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
glukóza	glukóza	k1gFnSc1	glukóza
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
→	→	k?	→
glukonová	glukonový	k2eAgFnSc1d1	glukonový
kyselina	kyselina	k1gFnSc1	kyselina
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
Peroxid	peroxid	k1gInSc4	peroxid
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
medu	med	k1gInSc6	med
je	být	k5eAaImIp3nS	být
aktivován	aktivovat	k5eAaBmNgInS	aktivovat
ředěním	ředění	k1gNnSc7	ředění
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lékařského	lékařský	k2eAgInSc2d1	lékařský
peroxidu	peroxid	k1gInSc2	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
3	[number]	k4	3
%	%	kIx~	%
objemu	objem	k1gInSc2	objem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
jen	jen	k9	jen
1	[number]	k4	1
mmol	mmolum	k1gNnPc2	mmolum
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
<s>
Železo	železo	k1gNnSc1	železo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
medu	med	k1gInSc6	med
oxidováno	oxidován	k2eAgNnSc1d1	oxidováno
kyslíkem	kyslík	k1gInSc7	kyslík
volné	volný	k2eAgInPc4d1	volný
radikály	radikál	k1gInPc4	radikál
a	a	k8xC	a
uvolňováno	uvolňován	k2eAgNnSc1d1	uvolňováno
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
peroxidu	peroxid	k1gInSc2	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
jako	jako	k8xS	jako
tekutý	tekutý	k2eAgInSc1d1	tekutý
obvaz	obvaz	k1gInSc1	obvaz
rány	rána	k1gFnSc2	rána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
peroxid	peroxid	k1gInSc1	peroxid
vodíku	vodík	k1gInSc2	vodík
produkován	produkovat	k5eAaImNgInS	produkovat
smísením	smísení	k1gNnSc7	smísení
s	s	k7c7	s
tělesným	tělesný	k2eAgInSc7d1	tělesný
potem	pot	k1gInSc7	pot
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
peroxid	peroxid	k1gInSc1	peroxid
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
antiseptikum	antiseptikum	k1gNnSc1	antiseptikum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
3	[number]	k4	3
<g/>
%	%	kIx~	%
lékařského	lékařský	k2eAgInSc2d1	lékařský
roztoku	roztok	k1gInSc2	roztok
<g/>
,	,	kIx,	,
pomalé	pomalý	k2eAgNnSc1d1	pomalé
uvolňování	uvolňování	k1gNnSc1	uvolňování
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
poškození	poškození	k1gNnSc4	poškození
okolní	okolní	k2eAgFnSc2d1	okolní
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kyselost	kyselost	k1gFnSc4	kyselost
===	===	k?	===
</s>
</p>
<p>
<s>
pH	ph	kA	ph
medu	med	k1gInSc2	med
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
mezi	mezi	k7c7	mezi
3,2	[number]	k4	3,2
a	a	k8xC	a
4,5	[number]	k4	4,5
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
relativně	relativně	k6eAd1	relativně
vysoký	vysoký	k2eAgInSc1d1	vysoký
stupeň	stupeň	k1gInSc1	stupeň
kyselosti	kyselost	k1gFnSc2	kyselost
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
růstu	růst	k1gInSc2	růst
bakterií	bakterie	k1gFnPc2	bakterie
způsobujících	způsobující	k2eAgFnPc2d1	způsobující
infekci	infekce	k1gFnSc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Kyselost	kyselost	k1gFnSc1	kyselost
také	také	k9	také
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
med	med	k1gInSc1	med
květový	květový	k2eAgInSc1d1	květový
má	mít	k5eAaImIp3nS	mít
všeobecně	všeobecně	k6eAd1	všeobecně
nižší	nízký	k2eAgFnSc4d2	nižší
pH	ph	kA	ph
než	než	k8xS	než
med	med	k1gInSc4	med
medovicový	medovicový	k2eAgInSc4d1	medovicový
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nutriční	nutriční	k2eAgInPc1d1	nutriční
efekty	efekt	k1gInPc1	efekt
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
relativně	relativně	k6eAd1	relativně
nových	nový	k2eAgInPc2d1	nový
poznatků	poznatek	k1gInPc2	poznatek
má	mít	k5eAaImIp3nS	mít
med	med	k1gInSc4	med
znatelné	znatelný	k2eAgInPc4d1	znatelný
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
efekty	efekt	k1gInPc4	efekt
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
užívání	užívání	k1gNnSc6	užívání
<g/>
.	.	kIx.	.
</s>
<s>
Med	med	k1gInSc1	med
totiž	totiž	k9	totiž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejen	nejen	k6eAd1	nejen
mnoho	mnoho	k4c1	mnoho
uhličitanů	uhličitan	k1gInPc2	uhličitan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nezřídka	nezřídka	k6eAd1	nezřídka
polyfenoly	polyfenol	k1gInPc4	polyfenol
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
antioxidanty	antioxidant	k1gInPc1	antioxidant
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Antioxidanty	antioxidant	k1gInPc1	antioxidant
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
výživný	výživný	k2eAgInSc1d1	výživný
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
působení	působení	k1gNnSc4	působení
negativních	negativní	k2eAgInPc2d1	negativní
důsledků	důsledek	k1gInPc2	důsledek
stresu	stres	k1gInSc2	stres
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
antioxidanty	antioxidant	k1gInPc1	antioxidant
v	v	k7c6	v
medu	med	k1gInSc6	med
příznivě	příznivě	k6eAd1	příznivě
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
rakovině	rakovina	k1gFnSc3	rakovina
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimoto	mimoto	k6eAd1	mimoto
<g/>
,	,	kIx,	,
med	med	k1gInSc1	med
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
také	také	k9	také
za	za	k7c4	za
zvyšování	zvyšování	k1gNnSc4	zvyšování
populace	populace	k1gFnSc2	populace
probiotických	probiotický	k2eAgFnPc2d1	probiotická
bakterií	bakterie	k1gFnPc2	bakterie
ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
zvýšení	zvýšení	k1gNnSc4	zvýšení
obranyschopnosti	obranyschopnost	k1gFnSc2	obranyschopnost
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
zlepšení	zlepšení	k1gNnSc4	zlepšení
trávení	trávení	k1gNnSc1	trávení
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
cholesterolu	cholesterol	k1gInSc2	cholesterol
a	a	k8xC	a
již	již	k6eAd1	již
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
prevence	prevence	k1gFnSc1	prevence
proti	proti	k7c3	proti
rakovině	rakovina	k1gFnSc3	rakovina
střeva	střevo	k1gNnSc2	střevo
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Laboratorně	laboratorně	k6eAd1	laboratorně
byly	být	k5eAaImAgInP	být
prokázány	prokázán	k2eAgInPc1d1	prokázán
antibakteriální	antibakteriální	k2eAgInPc1d1	antibakteriální
účinky	účinek	k1gInPc1	účinek
proti	proti	k7c3	proti
mnoha	mnoho	k4c3	mnoho
druhům	druh	k1gInPc3	druh
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
rodů	rod	k1gInPc2	rod
Salmonella	Salmonella	k1gMnSc1	Salmonella
<g/>
,	,	kIx,	,
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
<g/>
,	,	kIx,	,
Shigella	Shigella	k1gMnSc1	Shigella
<g/>
,	,	kIx,	,
bakteriím	bakterie	k1gFnPc3	bakterie
Helicobacter	Helicobactra	k1gFnPc2	Helicobactra
pilori	pilori	k6eAd1	pilori
<g/>
,	,	kIx,	,
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
<g/>
,	,	kIx,	,
Leptosperum	Leptosperum	k1gNnSc1	Leptosperum
polygalifolium	polygalifolium	k1gNnSc1	polygalifolium
<g/>
,	,	kIx,	,
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	coli	k1gNnSc2	coli
<g/>
,	,	kIx,	,
Bacillus	Bacillus	k1gMnSc1	Bacillus
anthracis	anthracis	k1gFnSc2	anthracis
<g/>
,	,	kIx,	,
Corynebacterium	Corynebacterium	k1gNnSc4	Corynebacterium
diptheriae	diptheria	k1gMnSc2	diptheria
<g/>
,	,	kIx,	,
Klebsiella	Klebsiell	k1gMnSc2	Klebsiell
pneumoniae	pneumonia	k1gMnSc2	pneumonia
<g/>
,	,	kIx,	,
Haemophilus	Haemophilus	k1gInSc4	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
<g/>
,	,	kIx,	,
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
tuberculosis	tuberculosis	k1gFnSc2	tuberculosis
a	a	k8xC	a
mnohých	mnohý	k2eAgNnPc6d1	mnohé
dalších	další	k2eAgNnPc6d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
protivirové	protivirový	k2eAgNnSc1d1	protivirové
(	(	kIx(	(
<g/>
Rubella	Rubella	k1gFnSc1	Rubella
a	a	k8xC	a
Herpes	herpes	k1gInSc1	herpes
virus	virus	k1gInSc1	virus
<g/>
)	)	kIx)	)
a	a	k8xC	a
protiparazitární	protiparazitární	k2eAgInPc1d1	protiparazitární
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Leishmania	Leishmanium	k1gNnSc2	Leishmanium
a	a	k8xC	a
Echinococcus	Echinococcus	k1gInSc1	Echinococcus
<g/>
,	,	kIx,	,
též	též	k9	též
Caenorhabditis	Caenorhabditis	k1gFnSc1	Caenorhabditis
elegans	elegansa	k1gFnPc2	elegansa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gInPc1	jeho
antimutagenní	antimutagenní	k2eAgInPc1d1	antimutagenní
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
též	též	k6eAd1	též
zjištěny	zjistit	k5eAaPmNgInP	zjistit
jeho	jeho	k3xOp3gInPc1	jeho
protiplísňové	protiplísňový	k2eAgInPc1d1	protiplísňový
účinky	účinek	k1gInPc1	účinek
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
různým	různý	k2eAgInPc3d1	různý
druhům	druh	k1gInPc3	druh
kvasinek	kvasinka	k1gFnPc2	kvasinka
a	a	k8xC	a
rodům	rod	k1gInPc3	rod
Epidermophyton	Epidermophyton	k1gInSc1	Epidermophyton
<g/>
,	,	kIx,	,
Microsporum	Microsporum	k1gInSc1	Microsporum
a	a	k8xC	a
Thrichophyton	Thrichophyton	k1gInSc1	Thrichophyton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pokusů	pokus	k1gInPc2	pokus
na	na	k7c6	na
krysách	krysa	k1gFnPc6	krysa
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
byly	být	k5eAaImAgInP	být
implantovány	implantován	k2eAgInPc1d1	implantován
nádory	nádor	k1gInPc1	nádor
před	před	k7c7	před
a	a	k8xC	a
po-operativně	poperativně	k6eAd1	po-operativně
ošetřené	ošetřený	k2eAgNnSc4d1	ošetřené
medem	med	k1gInSc7	med
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
na	na	k7c4	na
možné	možný	k2eAgInPc4d1	možný
protirakovinové	protirakovinový	k2eAgInPc4d1	protirakovinový
účinky	účinek	k1gInPc4	účinek
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
studiích	studio	k1gNnPc6	studio
prokázány	prokázat	k5eAaPmNgInP	prokázat
výrazné	výrazný	k2eAgInPc1d1	výrazný
antioxidační	antioxidační	k2eAgInPc1d1	antioxidační
účinky	účinek	k1gInPc1	účinek
(	(	kIx(	(
<g/>
při	při	k7c6	při
podávání	podávání	k1gNnSc6	podávání
1,2	[number]	k4	1,2
<g/>
g	g	kA	g
medu	med	k1gInSc2	med
na	na	k7c6	na
kg	kg	kA	kg
váhy	váha	k1gFnSc2	váha
-	-	kIx~	-
tzn.	tzn.	kA	tzn.
pro	pro	k7c4	pro
70	[number]	k4	70
kg	kg	kA	kg
člověka	člověk	k1gMnSc4	člověk
to	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
84	[number]	k4	84
g	g	kA	g
medu	med	k1gInSc2	med
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
antibakteriální	antibakteriální	k2eAgFnSc1d1	antibakteriální
(	(	kIx(	(
<g/>
dieta	dieta	k1gFnSc1	dieta
obohacená	obohacený	k2eAgFnSc1d1	obohacená
30	[number]	k4	30
<g/>
ml	ml	kA	ml
medu	med	k1gInSc2	med
3	[number]	k4	3
<g/>
x	x	k?	x
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
i	i	k9	i
protizánětlivé	protizánětlivý	k2eAgInPc1d1	protizánětlivý
účinky	účinek	k1gInPc1	účinek
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
g	g	kA	g
medu	med	k1gInSc2	med
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
s	s	k7c7	s
obézními	obézní	k2eAgMnPc7d1	obézní
i	i	k8xC	i
zdravými	zdravý	k2eAgMnPc7d1	zdravý
lidmi	člověk	k1gMnPc7	člověk
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
konzumace	konzumace	k1gFnSc1	konzumace
medu	med	k1gInSc2	med
(	(	kIx(	(
<g/>
70	[number]	k4	70
g	g	kA	g
denně	denně	k6eAd1	denně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
mírně	mírně	k6eAd1	mírně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
hodnoty	hodnota	k1gFnPc4	hodnota
rizikových	rizikový	k2eAgInPc2d1	rizikový
faktorů	faktor	k1gInPc2	faktor
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
kardiovaskulárních	kardiovaskulární	k2eAgInPc2d1	kardiovaskulární
a	a	k8xC	a
metabolických	metabolický	k2eAgInPc2d1	metabolický
problémů	problém	k1gInPc2	problém
(	(	kIx(	(
<g/>
vylepšuje	vylepšovat	k5eAaImIp3nS	vylepšovat
lipoproteinový	lipoproteinový	k2eAgInSc4d1	lipoproteinový
profil	profil	k1gInSc4	profil
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
hladinu	hladina	k1gFnSc4	hladina
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
na	na	k7c4	na
lačno	lačno	k1gNnSc4	lačno
<g/>
)	)	kIx)	)
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Skladování	skladování	k1gNnPc1	skladování
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
konzumace	konzumace	k1gFnSc2	konzumace
medu	med	k1gInSc2	med
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
pozitivním	pozitivní	k2eAgInPc3d1	pozitivní
účinkům	účinek	k1gInPc3	účinek
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
příznivých	příznivý	k2eAgInPc2d1	příznivý
účinků	účinek	k1gInPc2	účinek
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
med	med	k1gInSc1	med
nebyl	být	k5eNaImAgInS	být
vystavován	vystavován	k2eAgMnSc1d1	vystavován
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
skladován	skladovat	k5eAaImNgMnS	skladovat
ideálně	ideálně	k6eAd1	ideálně
v	v	k7c6	v
temnu	temno	k1gNnSc6	temno
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
testování	testování	k1gNnSc6	testování
jeho	jeho	k3xOp3gFnSc2	jeho
antibakteriální	antibakteriální	k2eAgFnSc2d1	antibakteriální
aktivity	aktivita	k1gFnSc2	aktivita
proti	proti	k7c3	proti
zlatému	zlatý	k2eAgInSc3d1	zlatý
stafylokoku	stafylokok	k1gInSc3	stafylokok
se	se	k3xPyFc4	se
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
:	:	kIx,	:
po	po	k7c4	po
15	[number]	k4	15
měsících	měsíc	k1gInPc6	měsíc
skladování	skladování	k1gNnSc6	skladování
květového	květový	k2eAgInSc2d1	květový
medu	med	k1gInSc2	med
na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
klesla	klesnout	k5eAaPmAgFnS	klesnout
jeho	jeho	k3xOp3gFnSc4	jeho
peroxidová	peroxidový	k2eAgFnSc1d1	peroxidová
antibakteriální	antibakteriální	k2eAgFnSc1d1	antibakteriální
aktivita	aktivita	k1gFnSc1	aktivita
na	na	k7c4	na
19	[number]	k4	19
<g/>
%	%	kIx~	%
výchozí	výchozí	k2eAgFnSc2d1	výchozí
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
po	po	k7c6	po
skladování	skladování	k1gNnSc6	skladování
v	v	k7c6	v
temnu	temno	k1gNnSc6	temno
na	na	k7c4	na
48	[number]	k4	48
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Neperoxidová	Neperoxidový	k2eAgFnSc1d1	Neperoxidový
antibakteriální	antibakteriální	k2eAgFnSc1d1	antibakteriální
aktivita	aktivita	k1gFnSc1	aktivita
klesla	klesnout	k5eAaPmAgFnS	klesnout
při	při	k7c6	při
skladování	skladování	k1gNnSc6	skladování
na	na	k7c6	na
světla	světlo	k1gNnPc4	světlo
na	na	k7c6	na
76	[number]	k4	76
<g/>
%	%	kIx~	%
a	a	k8xC	a
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
na	na	k7c4	na
86	[number]	k4	86
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
si	se	k3xPyFc3	se
lépe	dobře	k6eAd2	dobře
vedl	vést	k5eAaImAgMnS	vést
medovicový	medovicový	k2eAgInSc4d1	medovicový
med	med	k1gInSc4	med
(	(	kIx(	(
<g/>
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
též	též	k9	též
lesní	lesní	k2eAgNnSc1d1	lesní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
pokles	pokles	k1gInSc4	pokles
mírnější	mírný	k2eAgInSc4d2	mírnější
a	a	k8xC	a
při	při	k7c6	při
skladování	skladování	k1gNnSc6	skladování
v	v	k7c6	v
temnu	temno	k1gNnSc6	temno
bylo	být	k5eAaImAgNnS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
70	[number]	k4	70
<g/>
%	%	kIx~	%
jeho	jeho	k3xOp3gFnSc2	jeho
výchozí	výchozí	k2eAgFnSc2d1	výchozí
peroxidové	peroxidový	k2eAgFnSc2d1	peroxidová
antibakteriální	antibakteriální	k2eAgFnSc2d1	antibakteriální
aktivity	aktivita	k1gFnSc2	aktivita
a	a	k8xC	a
80	[number]	k4	80
<g/>
%	%	kIx~	%
neperoxidové	neperoxid	k1gMnPc1	neperoxid
<g/>
.	.	kIx.	.
<g/>
Ještě	ještě	k6eAd1	ještě
horší	zlý	k2eAgInPc1d2	horší
účinky	účinek	k1gInPc1	účinek
má	mít	k5eAaImIp3nS	mít
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
15	[number]	k4	15
minutách	minuta	k1gFnPc6	minuta
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
70	[number]	k4	70
<g/>
°	°	k?	°
<g/>
C	C	kA	C
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
u	u	k7c2	u
květového	květový	k2eAgInSc2d1	květový
medu	med	k1gInSc2	med
utlumení	utlumení	k1gNnSc2	utlumení
peroxidové	peroxidový	k2eAgFnSc2d1	peroxidová
antibakteriální	antibakteriální	k2eAgFnSc2d1	antibakteriální
aktivity	aktivita	k1gFnSc2	aktivita
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
Staph	Staph	k1gInSc1	Staph
<g/>
.	.	kIx.	.
aureus	aureus	k1gInSc1	aureus
<g/>
)	)	kIx)	)
na	na	k7c4	na
8	[number]	k4	8
<g/>
%	%	kIx~	%
a	a	k8xC	a
neperoxidové	peroxidový	k2eNgNnSc4d1	peroxidový
na	na	k7c4	na
86	[number]	k4	86
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Medovicový	medovicový	k2eAgInSc4d1	medovicový
med	med	k1gInSc4	med
si	se	k3xPyFc3	se
opět	opět	k6eAd1	opět
vedl	vést	k5eAaImAgMnS	vést
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
experimentu	experiment	k1gInSc6	experiment
jeho	jeho	k3xOp3gFnSc1	jeho
antibakteriální	antibakteriální	k2eAgFnSc1d1	antibakteriální
aktivita	aktivita	k1gFnSc1	aktivita
klesla	klesnout	k5eAaPmAgFnS	klesnout
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
78	[number]	k4	78
<g/>
%	%	kIx~	%
maxima	maximum	k1gNnSc2	maximum
(	(	kIx(	(
<g/>
peroxidová	peroxidový	k2eAgFnSc1d1	peroxidová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
94	[number]	k4	94
<g/>
%	%	kIx~	%
maxima	maximum	k1gNnSc2	maximum
(	(	kIx(	(
<g/>
neperoxidová	peroxidový	k2eNgFnSc1d1	peroxidový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Podobně	podobně	k6eAd1	podobně
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
měření	měření	k1gNnSc4	měření
jeho	jeho	k3xOp3gFnSc2	jeho
antioxidační	antioxidační	k2eAgFnSc2d1	antioxidační
kapacity	kapacita	k1gFnSc2	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zaměření	zaměření	k1gNnSc6	zaměření
na	na	k7c6	na
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
málo	málo	k6eAd1	málo
stabilních	stabilní	k2eAgFnPc2d1	stabilní
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
glukózu	glukóza	k1gFnSc4	glukóza
oxidázu	oxidáza	k1gFnSc4	oxidáza
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
degradace	degradace	k1gFnSc1	degradace
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
70	[number]	k4	70
<g/>
°	°	k?	°
<g/>
C	C	kA	C
následující	následující	k2eAgInPc1d1	následující
-	-	kIx~	-
za	za	k7c4	za
pouhou	pouhý	k2eAgFnSc4d1	pouhá
1	[number]	k4	1
minutu	minuta	k1gFnSc4	minuta
klesl	klesnout	k5eAaPmAgInS	klesnout
její	její	k3xOp3gInSc1	její
antioxidační	antioxidační	k2eAgInSc1d1	antioxidační
potenciál	potenciál	k1gInSc1	potenciál
na	na	k7c4	na
80	[number]	k4	80
<g/>
%	%	kIx~	%
výchozího	výchozí	k2eAgInSc2d1	výchozí
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
po	po	k7c6	po
5-10	[number]	k4	5-10
minutách	minuta	k1gFnPc6	minuta
na	na	k7c6	na
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
teplotu	teplota	k1gFnSc4	teplota
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
teplotu	teplota	k1gFnSc4	teplota
50	[number]	k4	50
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
poklesu	pokles	k1gInSc3	pokles
atioxidační	atioxidační	k2eAgFnSc2d1	atioxidační
kapacity	kapacita	k1gFnSc2	kapacita
ani	ani	k8xC	ani
po	po	k7c6	po
1	[number]	k4	1
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Med	med	k1gInSc1	med
tedy	tedy	k9	tedy
lze	lze	k6eAd1	lze
dávat	dávat	k5eAaImF	dávat
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
teplého	teplý	k2eAgInSc2d1	teplý
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
jej	on	k3xPp3gInSc4	on
dávat	dávat	k5eAaImF	dávat
do	do	k7c2	do
horkého	horký	k2eAgInSc2d1	horký
či	či	k8xC	či
vřelého	vřelý	k2eAgInSc2d1	vřelý
čaje	čaj	k1gInSc2	čaj
(	(	kIx(	(
<g/>
či	či	k8xC	či
jídla	jídlo	k1gNnSc2	jídlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pastování	pastování	k1gNnSc6	pastování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
většinou	většinou	k6eAd1	většinou
trvá	trvat	k5eAaImIp3nS	trvat
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
použít	použít	k5eAaPmF	použít
teplotu	teplota	k1gFnSc4	teplota
ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgFnSc4d2	nižší
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
===	===	k?	===
Užití	užití	k1gNnSc3	užití
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
medicíně	medicína	k1gFnSc6	medicína
===	===	k?	===
</s>
</p>
<p>
<s>
Časté	častý	k2eAgNnSc1d1	časté
užívání	užívání	k1gNnSc1	užívání
medu	med	k1gInSc2	med
jako	jako	k8xS	jako
protibakteriálního	protibakteriální	k2eAgInSc2d1	protibakteriální
činitele	činitel	k1gInSc2	činitel
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnPc4	jeho
užití	užití	k1gNnSc1	užití
pro	pro	k7c4	pro
krytí	krytí	k1gNnSc4	krytí
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
popálenin	popálenina	k1gFnPc2	popálenina
a	a	k8xC	a
vředů	vřed	k1gInPc2	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
užití	užití	k1gNnSc1	užití
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
užívání	užívání	k1gNnSc1	užívání
medu	med	k1gInSc2	med
redukuje	redukovat	k5eAaBmIp3nS	redukovat
otoky	otok	k1gInPc4	otok
a	a	k8xC	a
jizvy	jizva	k1gFnPc4	jizva
<g/>
;	;	kIx,	;
také	také	k9	také
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
např.	např.	kA	např.
bavlněnému	bavlněný	k2eAgInSc3d1	bavlněný
obvazu	obvaz	k1gInSc3	obvaz
nalepit	nalepit	k5eAaPmF	nalepit
se	se	k3xPyFc4	se
na	na	k7c4	na
léčené	léčený	k2eAgNnSc4d1	léčené
zranění	zranění	k1gNnSc4	zranění
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
metodě	metoda	k1gFnSc6	metoda
léčení	léčení	k1gNnSc4	léčení
slabých	slabý	k2eAgFnPc2d1	slabá
forem	forma	k1gFnPc2	forma
zánětů	zánět	k1gInPc2	zánět
spojivek	spojivka	k1gFnPc2	spojivka
pomocí	pomocí	k7c2	pomocí
kapky	kapka	k1gFnSc2	kapka
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
kápnutého	kápnutý	k2eAgMnSc2d1	kápnutý
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
antiseptických	antiseptický	k2eAgFnPc2d1	antiseptická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
med	med	k1gInSc4	med
(	(	kIx(	(
<g/>
speciálně	speciálně	k6eAd1	speciálně
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
kombinován	kombinován	k2eAgMnSc1d1	kombinován
s	s	k7c7	s
citrónem	citrón	k1gInSc7	citrón
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
užíván	užívat	k5eAaImNgMnS	užívat
orálně	orálně	k6eAd1	orálně
pacienty	pacient	k1gMnPc4	pacient
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
sužováni	sužován	k2eAgMnPc1d1	sužován
Pharyngitídou	Pharyngitída	k1gFnSc7	Pharyngitída
a	a	k8xC	a
Laryngitídou	Laryngitída	k1gFnSc7	Laryngitída
<g/>
,	,	kIx,	,
ke	k	k7c3	k
zmírnění	zmírnění	k1gNnSc3	zmírnění
příznaků	příznak	k1gInPc2	příznak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
všeobecně	všeobecně	k6eAd1	všeobecně
přijímané	přijímaný	k2eAgFnSc3d1	přijímaná
myšlence	myšlenka	k1gFnSc3	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
med	med	k1gInSc4	med
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
alergie	alergie	k1gFnSc1	alergie
<g/>
,	,	kIx,	,
kontrolované	kontrolovaný	k2eAgFnPc1d1	kontrolovaná
studie	studie	k1gFnPc1	studie
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
med	med	k1gInSc1	med
není	být	k5eNaImIp3nS	být
více	hodně	k6eAd2	hodně
efektivní	efektivní	k2eAgFnSc1d1	efektivní
než	než	k8xS	než
placebo	placebo	k1gNnSc1	placebo
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sezónních	sezónní	k2eAgFnPc2d1	sezónní
alergií	alergie	k1gFnPc2	alergie
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
způsobených	způsobený	k2eAgFnPc2d1	způsobená
pylem	pyl	k1gInSc7	pyl
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
trav	tráva	k1gFnPc2	tráva
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
včely	včela	k1gFnPc1	včela
většinou	většinou	k6eAd1	většinou
nesbírají	sbírat	k5eNaImIp3nP	sbírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Možné	možný	k2eAgInPc4d1	možný
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Kontaminovaný	kontaminovaný	k2eAgInSc4d1	kontaminovaný
med	med	k1gInSc4	med
====	====	k?	====
</s>
</p>
<p>
<s>
Kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
med	med	k1gInSc1	med
žádné	žádný	k3yNgFnSc2	žádný
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
bakterie	bakterie	k1gFnSc2	bakterie
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Případná	případný	k2eAgFnSc1d1	případná
kontaminace	kontaminace	k1gFnSc1	kontaminace
bývá	bývat	k5eAaImIp3nS	bývat
ze	z	k7c2	z
špinavých	špinavý	k2eAgFnPc2d1	špinavá
nádob	nádoba	k1gFnPc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
med	med	k1gInSc4	med
<g/>
,	,	kIx,	,
kukuřičný	kukuřičný	k2eAgInSc4d1	kukuřičný
sirup	sirup	k1gInSc4	sirup
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
sladidla	sladidlo	k1gNnPc4	sladidlo
tvoří	tvořit	k5eAaImIp3nS	tvořit
potenciální	potenciální	k2eAgNnSc1d1	potenciální
a	a	k8xC	a
akutní	akutní	k2eAgNnSc1d1	akutní
riziko	riziko	k1gNnSc1	riziko
pro	pro	k7c4	pro
kojence	kojenec	k1gMnSc4	kojenec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
K	k	k7c3	k
několika	několik	k4yIc2	několik
bakteriím	bakterie	k1gFnPc3	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
znečištěném	znečištěný	k2eAgInSc6d1	znečištěný
medu	med	k1gInSc6	med
přežijí	přežít	k5eAaPmIp3nP	přežít
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
Clostridium	Clostridium	k1gNnSc4	Clostridium
botulinum	botulinum	k1gInSc1	botulinum
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
bakterie	bakterie	k1gFnPc1	bakterie
produkují	produkovat	k5eAaImIp3nP	produkovat
botulotoxin	botulotoxin	k1gInSc4	botulotoxin
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
neškodné	škodný	k2eNgFnSc2d1	neškodná
kvůli	kvůli	k7c3	kvůli
vyšší	vysoký	k2eAgFnSc3d2	vyšší
kyselosti	kyselost	k1gFnSc3	kyselost
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
žaludeční	žaludeční	k2eAgFnSc2d1	žaludeční
šťávy	šťáva	k1gFnSc2	šťáva
kojence	kojenec	k1gMnSc2	kojenec
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
kyselé	kyselý	k2eAgInPc4d1	kyselý
<g/>
,	,	kIx,	,
konzumace	konzumace	k1gFnSc1	konzumace
medu	med	k1gInSc2	med
obsahujícího	obsahující	k2eAgInSc2d1	obsahující
výtrusy	výtrus	k1gInPc4	výtrus
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
až	až	k9	až
botulismus	botulismus	k1gInSc4	botulismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
doporučeno	doporučen	k2eAgNnSc1d1	doporučeno
nepodávat	podávat	k5eNaImF	podávat
žádný	žádný	k3yNgInSc4	žádný
med	med	k1gInSc4	med
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgNnSc4d1	jiné
sladidlo	sladidlo	k1gNnSc4	sladidlo
dětem	dítě	k1gFnPc3	dítě
mladším	mladý	k2eAgFnPc3d2	mladší
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Až	až	k9	až
když	když	k8xS	když
dítě	dítě	k1gNnSc1	dítě
začne	začít	k5eAaPmIp3nS	začít
přijímat	přijímat	k5eAaImF	přijímat
tuhou	tuhý	k2eAgFnSc4d1	tuhá
stravu	strava	k1gFnSc4	strava
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
být	být	k5eAaImF	být
žaludeční	žaludeční	k2eAgFnPc1d1	žaludeční
šťávy	šťáva	k1gFnPc1	šťáva
dost	dost	k6eAd1	dost
kyselé	kyselý	k2eAgInPc4d1	kyselý
na	na	k7c4	na
prevenci	prevence	k1gFnSc4	prevence
růstu	růst	k1gInSc2	růst
spór	spóra	k1gFnPc2	spóra
(	(	kIx(	(
<g/>
výtrusů	výtrus	k1gInPc2	výtrus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Toxický	toxický	k2eAgInSc4d1	toxický
med	med	k1gInSc4	med
====	====	k?	====
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
jedovaté	jedovatý	k2eAgInPc1d1	jedovatý
medy	med	k1gInPc1	med
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
sbírán	sbírat	k5eAaImNgInS	sbírat
z	z	k7c2	z
květů	květ	k1gInPc2	květ
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toxické	toxický	k2eAgInPc1d1	toxický
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
medy	med	k1gInPc1	med
z	z	k7c2	z
rododendronů	rododendron	k1gInPc2	rododendron
<g/>
.	.	kIx.	.
</s>
<s>
Rododendrony	rododendron	k1gInPc1	rododendron
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
další	další	k2eAgFnPc1d1	další
rostliny	rostlina	k1gFnPc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Ericaceae	Ericacea	k1gFnSc2	Ericacea
(	(	kIx(	(
<g/>
vřesovcovité	vřesovcovitý	k2eAgFnSc2d1	vřesovcovitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
grayanotoxin	grayanotoxina	k1gFnPc2	grayanotoxina
(	(	kIx(	(
<g/>
známý	známý	k1gMnSc1	známý
též	též	k9	též
jako	jako	k9	jako
andromedotoxin	andromedotoxina	k1gFnPc2	andromedotoxina
<g/>
,	,	kIx,	,
acetylandromedol	acetylandromedola	k1gFnPc2	acetylandromedola
nebo	nebo	k8xC	nebo
rhodotoxin	rhodotoxina	k1gFnPc2	rhodotoxina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
až	až	k9	až
do	do	k7c2	do
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
grayanotoxinu	grayanotoxin	k1gInSc2	grayanotoxin
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
toxické	toxický	k2eAgFnPc4d1	toxická
úrovně	úroveň	k1gFnPc4	úroveň
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednodruhový	jednodruhový	k2eAgInSc4d1	jednodruhový
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požití	požití	k1gNnSc6	požití
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
g	g	kA	g
takového	takový	k3xDgInSc2	takový
medu	med	k1gInSc2	med
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
tzv.	tzv.	kA	tzv.
nemoc	nemoc	k1gFnSc1	nemoc
šíleného	šílený	k2eAgInSc2d1	šílený
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
<s>
Popsané	popsaný	k2eAgInPc1d1	popsaný
příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
nízký	nízký	k2eAgInSc4d1	nízký
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
srdečního	srdeční	k2eAgInSc2d1	srdeční
rytmu	rytmus	k1gInSc2	rytmus
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc1	nevolnost
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
se	s	k7c7	s
zvracením	zvracení	k1gNnSc7	zvracení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocení	pocení	k1gNnSc1	pocení
<g/>
,	,	kIx,	,
motání	motání	k1gNnSc1	motání
hlavy	hlava	k1gFnSc2	hlava
až	až	k9	až
poruchy	porucha	k1gFnPc1	porucha
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
zastřené	zastřený	k2eAgNnSc4d1	zastřené
vidění	vidění	k1gNnSc4	vidění
či	či	k8xC	či
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
slinění	slinění	k1gNnSc4	slinění
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
nastávají	nastávat	k5eAaImIp3nP	nastávat
většinou	většina	k1gFnSc7	většina
mezi	mezi	k7c7	mezi
20	[number]	k4	20
minutami	minuta	k1gFnPc7	minuta
až	až	k9	až
3	[number]	k4	3
hodinami	hodina	k1gFnPc7	hodina
po	po	k7c4	po
požití	požití	k1gNnSc4	požití
a	a	k8xC	a
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
1-2	[number]	k4	1-2
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgInPc4	takový
medy	med	k1gInPc4	med
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
záměrně	záměrně	k6eAd1	záměrně
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
domnělé	domnělý	k2eAgInPc4d1	domnělý
léčivé	léčivý	k2eAgInPc4d1	léčivý
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
afrodiziakálních	afrodiziakální	k2eAgFnPc2d1	afrodiziakální
<g/>
)	)	kIx)	)
produkovány	produkován	k2eAgFnPc4d1	produkována
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
oblast	oblast	k1gFnSc1	oblast
Kulung	Kulunga	k1gFnPc2	Kulunga
v	v	k7c6	v
Nepálu	Nepál	k1gInSc6	Nepál
nebo	nebo	k8xC	nebo
ostrova	ostrov	k1gInSc2	ostrov
Réunion	Réunion	k1gInSc4	Réunion
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
mají	mít	k5eAaImIp3nP	mít
prý	prý	k9	prý
halucinogenní	halucinogenní	k2eAgInPc1d1	halucinogenní
účinky	účinek	k1gInPc1	účinek
a	a	k8xC	a
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
se	se	k3xPyFc4	se
v	v	k7c6	v
r.	r.	kA	r.
2018	[number]	k4	2018
prodávaly	prodávat	k5eAaImAgInP	prodávat
za	za	k7c2	za
horentních	horentní	k2eAgInPc2d1	horentní
$	$	kIx~	$
<g/>
199	[number]	k4	199
za	za	k7c4	za
250	[number]	k4	250
<g/>
g.	g.	k?	g.
</s>
</p>
<p>
<s>
==	==	k?	==
Strdí	strdí	k1gNnSc2	strdí
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
staročeských	staročeský	k2eAgInPc6d1	staročeský
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
med	med	k1gInSc1	med
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
plást	plást	k1gInSc4	plást
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
plástev	plástev	k1gFnSc1	plástev
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
favus	favus	k1gInSc1	favus
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
honeycomb	honeycomb	k1gInSc1	honeycomb
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
slovem	slovo	k1gNnSc7	slovo
strdí	strdí	k1gNnSc2	strdí
(	(	kIx(	(
<g/>
střední	střední	k2eAgInSc1d1	střední
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
strdí	strdí	k1gNnSc1	strdí
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
nesklonné	sklonný	k2eNgInPc1d1	nesklonný
<g/>
,	,	kIx,	,
též	též	k9	též
ta	ten	k3xDgFnSc1	ten
stred	stred	k1gMnSc1	stred
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
strda	strda	k1gMnSc1	strda
<g/>
)	)	kIx)	)
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
všeslovanského	všeslovanský	k2eAgInSc2d1	všeslovanský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
včelařské	včelařský	k2eAgFnSc6d1	včelařská
terminologii	terminologie	k1gFnSc6	terminologie
se	se	k3xPyFc4	se
jako	jako	k9	jako
strdí	strdí	k1gNnSc1	strdí
označuje	označovat	k5eAaImIp3nS	označovat
medem	med	k1gInSc7	med
zanesené	zanesený	k2eAgNnSc1d1	zanesené
včelí	včelí	k2eAgNnSc1d1	včelí
dílo	dílo	k1gNnSc1	dílo
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgFnPc2d1	žijící
včel	včela	k1gFnPc2	včela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Olomoucké	olomoucký	k2eAgFnSc6d1	olomoucká
bibli	bible	k1gFnSc6	bible
se	se	k3xPyFc4	se
ve	v	k7c6	v
verši	verš	k1gInSc6	verš
16,14	[number]	k4	16,14
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Numeri	Numer	k1gFnSc2	Numer
překládá	překládat	k5eAaImIp3nS	překládat
"	"	kIx"	"
<g/>
ješto	ješto	k1gNnSc1	ješto
(	(	kIx(	(
<g/>
země	zem	k1gFnPc1	zem
<g/>
)	)	kIx)	)
ploue	ploue	k6eAd1	ploue
potoky	potok	k1gInPc4	potok
mléka	mléko	k1gNnSc2	mléko
i	i	k8xC	i
strdi	strdi	k?	strdi
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
překládá	překládat	k5eAaImIp3nS	překládat
spojení	spojení	k1gNnSc1	spojení
favus	favus	k1gInSc1	favus
distillans	distillans	k6eAd1	distillans
z	z	k7c2	z
verše	verš	k1gInSc2	verš
4,11	[number]	k4	4,11
z	z	k7c2	z
Písně	píseň	k1gFnSc2	píseň
písní	píseň	k1gFnPc2	píseň
slovy	slovo	k1gNnPc7	slovo
plást	plást	k1gInSc1	plást
strdí	strdí	k1gNnSc6	strdí
kapagiczy	kapagicza	k1gFnSc2	kapagicza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
drážďanské	drážďanský	k2eAgFnSc6d1	Drážďanská
je	být	k5eAaImIp3nS	být
formulace	formulace	k1gFnSc1	formulace
z	z	k7c2	z
Ezechiela	Ezechiel	k1gMnSc2	Ezechiel
(	(	kIx(	(
<g/>
20,15	[number]	k4	20,15
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
jížto	jenžto	k3yRgFnSc2	jenžto
(	(	kIx(	(
<g/>
zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
sem	sem	k6eAd1	sem
jim	on	k3xPp3gMnPc3	on
dal	dát	k5eAaPmAgMnS	dát
plowucy	plowuca	k1gFnPc4	plowuca
(	(	kIx(	(
<g/>
plovúcí	plovúký	k2eAgMnPc1d1	plovúký
<g/>
)	)	kIx)	)
mlékem	mléko	k1gNnSc7	mléko
a	a	k8xC	a
strdí	strdí	k1gNnSc1	strdí
<g/>
,	,	kIx,	,
natczituu	natczituu	k6eAd1	natczituu
ze	z	k7c2	z
všelikých	všeliký	k3yIgFnPc2	všeliký
zemí	zem	k1gFnPc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
strdím	strdí	k1gNnSc7	strdí
oplývající	oplývající	k2eAgMnSc1d1	oplývající
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
tvé	tvůj	k3xOp2gNnSc1	tvůj
strdí	strdí	k1gNnSc1	strdí
plowuczye	plowuczy	k1gInSc2	plowuczy
milosti	milost	k1gFnSc2	milost
nepoznal	poznat	k5eNaPmAgMnS	poznat
sem	sem	k6eAd1	sem
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
překladech	překlad	k1gInPc6	překlad
ze	z	k7c2	z
svatého	svatý	k2eAgMnSc2d1	svatý
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
starých	starý	k2eAgInPc6d1	starý
rukopisech	rukopis	k1gInPc6	rukopis
<g/>
,	,	kIx,	,
lékařských	lékařský	k2eAgInPc2d1	lékařský
<g/>
,	,	kIx,	,
duchovních	duchovní	k2eAgInPc2d1	duchovní
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Formulaci	formulace	k1gFnSc4	formulace
z	z	k7c2	z
biblických	biblický	k2eAgFnPc2d1	biblická
knih	kniha	k1gFnPc2	kniha
Numeri	Numer	k1gFnSc2	Numer
a	a	k8xC	a
Ezechiel	Ezechiel	k1gMnSc1	Ezechiel
o	o	k7c6	o
"	"	kIx"	"
<g/>
zemi	zem	k1gFnSc6	zem
oplývající	oplývající	k2eAgFnSc6d1	oplývající
mlékem	mléko	k1gNnSc7	mléko
a	a	k8xC	a
strdím	strdí	k1gNnSc7	strdí
<g/>
"	"	kIx"	"
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
Bohem	bůh	k1gMnSc7	bůh
vyvolenému	vyvolený	k2eAgInSc3d1	vyvolený
národu	národ	k1gInSc3	národ
převzala	převzít	k5eAaPmAgFnS	převzít
začátkem	začátkem	k7c2	začátkem
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k8xC	i
Kosmova	Kosmův	k2eAgFnSc1d1	Kosmova
kronika	kronika	k1gFnSc1	kronika
a	a	k8xC	a
vztáhla	vztáhnout	k5eAaPmAgFnS	vztáhnout
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
příchodu	příchod	k1gInSc3	příchod
praotce	praotec	k1gMnSc2	praotec
Boema	Boem	k1gMnSc2	Boem
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nepříliš	příliš	k6eNd1	příliš
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
archetypálního	archetypální	k2eAgInSc2d1	archetypální
příběhu	příběh	k1gInSc2	příběh
o	o	k7c6	o
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
země	zem	k1gFnSc2	zem
zaslíbené	zaslíbená	k1gFnSc2	zaslíbená
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
Kosma	Kosma	k1gMnSc1	Kosma
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
kmene	kmen	k1gInSc2	kmen
Boémů	Boém	k1gMnPc2	Boém
po	po	k7c6	po
biblickém	biblický	k2eAgInSc6d1	biblický
vzoru	vzor	k1gInSc6	vzor
"	"	kIx"	"
<g/>
v	v	k7c4	v
čas	čas	k1gInSc4	čas
potopy	potopa	k1gFnSc2	potopa
lidu	lid	k1gInSc2	lid
zbavená	zbavený	k2eAgNnPc1d1	zbavené
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
Kosmas	Kosmas	k1gMnSc1	Kosmas
důkladně	důkladně	k6eAd1	důkladně
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
Biblí	bible	k1gFnSc7	bible
a	a	k8xC	a
Vergiliovou	Vergiliův	k2eAgFnSc7d1	Vergiliova
Aeneidou	Aeneida	k1gFnSc7	Aeneida
<g/>
,	,	kIx,	,
mnohde	mnohde	k6eAd1	mnohde
slovo	slovo	k1gNnSc1	slovo
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc4	příběh
dále	daleko	k6eAd2	daleko
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
Hájkova	Hájkův	k2eAgFnSc1d1	Hájkova
kronika	kronika	k1gFnSc1	kronika
a	a	k8xC	a
Jiráskovy	Jiráskův	k2eAgFnPc1d1	Jiráskova
Staré	Staré	k2eAgFnPc1d1	Staré
pověsti	pověst	k1gFnPc1	pověst
české	český	k2eAgFnPc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Strdí	strdí	k1gNnSc1	strdí
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
již	již	k6eAd1	již
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
verzi	verze	k1gFnSc6	verze
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
v	v	k7c6	v
novějších	nový	k2eAgInPc6d2	novější
překladech	překlad	k1gInPc6	překlad
uváděné	uváděný	k2eAgFnSc2d1	uváděná
jako	jako	k8xC	jako
med	med	k1gInSc1	med
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
ona	onen	k3xDgFnSc1	onen
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jsem	být	k5eAaImIp1nS	být
vám	vy	k3xPp2nPc3	vy
–	–	k?	–
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
pamatuji	pamatovat	k5eAaImIp1nS	pamatovat
–	–	k?	–
častokráte	častokráte	k?	častokráte
sliboval	slibovat	k5eAaImAgMnS	slibovat
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
nikomu	nikdo	k3yNnSc3	nikdo
nepoddaná	poddaný	k2eNgFnSc1d1	nepoddaná
<g/>
,	,	kIx,	,
zvěře	zvěř	k1gFnSc2	zvěř
a	a	k8xC	a
ptactva	ptactvo	k1gNnSc2	ptactvo
plná	plný	k2eAgFnSc1d1	plná
<g/>
,	,	kIx,	,
sladkým	sladký	k2eAgInSc7d1	sladký
medem	med	k1gInSc7	med
a	a	k8xC	a
mlékem	mléko	k1gNnSc7	mléko
vlhnoucí	vlhnoucí	k2eAgFnSc2d1	vlhnoucí
<g/>
,	,	kIx,	,
a	a	k8xC	a
jak	jak	k8xC	jak
sami	sám	k3xTgMnPc1	sám
pozorujete	pozorovat	k5eAaImIp2nP	pozorovat
<g/>
,	,	kIx,	,
podnebím	podnebí	k1gNnSc7	podnebí
k	k	k7c3	k
obývání	obývání	k1gNnSc3	obývání
příjemná	příjemný	k2eAgFnSc1d1	příjemná
<g/>
.	.	kIx.	.
</s>
<s>
Vody	voda	k1gFnPc1	voda
jsou	být	k5eAaImIp3nP	být
všude	všude	k6eAd1	všude
hojné	hojný	k2eAgFnPc1d1	hojná
a	a	k8xC	a
nad	nad	k7c4	nad
obyčej	obyčej	k1gInSc4	obyčej
rybnaté	rybnatý	k2eAgFnPc1d1	rybnatá
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Kvalita	kvalita	k1gFnSc1	kvalita
medů	med	k1gInPc2	med
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
med	med	k1gInSc1	med
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
produkují	produkovat	k5eAaImIp3nP	produkovat
domácí	domácí	k2eAgMnPc1d1	domácí
včelaři	včelař	k1gMnPc1	včelař
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejkvalitnějším	kvalitní	k2eAgInPc3d3	nejkvalitnější
výrobkům	výrobek	k1gInPc3	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
vydaná	vydaný	k2eAgNnPc1d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Českým	český	k2eAgInSc7d1	český
svazem	svaz	k1gInSc7	svaz
včelařů	včelař	k1gMnPc2	včelař
pro	pro	k7c4	pro
regionální	regionální	k2eAgFnSc4d1	regionální
známku	známka	k1gFnSc4	známka
Český	český	k2eAgInSc4d1	český
med	med	k1gInSc4	med
jsou	být	k5eAaImIp3nP	být
přísnější	přísný	k2eAgNnPc1d2	přísnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
evropská	evropský	k2eAgFnSc1d1	Evropská
směrnice	směrnice	k1gFnSc1	směrnice
pro	pro	k7c4	pro
med	med	k1gInSc4	med
i	i	k8xC	i
česká	český	k2eAgFnSc1d1	Česká
vyhláška	vyhláška	k1gFnSc1	vyhláška
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českých	český	k2eAgInPc6d1	český
obchodech	obchod	k1gInPc6	obchod
se	se	k3xPyFc4	se
však	však	k9	však
objevují	objevovat	k5eAaImIp3nP	objevovat
medy	med	k1gInPc1	med
či	či	k8xC	či
směsi	směs	k1gFnPc1	směs
medů	med	k1gInPc2	med
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
obsah	obsah	k1gInSc1	obsah
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
nedobrý	dobrý	k2eNgInSc1d1	nedobrý
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jen	jen	k9	jen
málo	málo	k1gNnSc4	málo
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
výrobky	výrobek	k1gInPc1	výrobek
jsou	být	k5eAaImIp3nP	být
směsicí	směsice	k1gFnSc7	směsice
škrobu	škrob	k1gInSc2	škrob
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
sirupů	sirup	k1gInPc2	sirup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
"	"	kIx"	"
<g/>
medech	med	k1gInPc6	med
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc4d1	velké
procento	procento	k1gNnSc4	procento
hydroxymethylfurfuralu	hydroxymethylfurfural	k1gInSc2	hydroxymethylfurfural
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
výrobek	výrobek	k1gInSc4	výrobek
byl	být	k5eAaImAgMnS	být
prakticky	prakticky	k6eAd1	prakticky
uvařen	uvařit	k5eAaPmNgMnS	uvařit
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
znehodnocen	znehodnocen	k2eAgMnSc1d1	znehodnocen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozborech	rozbor	k1gInPc6	rozbor
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
některé	některý	k3yIgInPc1	některý
dovozové	dovozový	k2eAgInPc1d1	dovozový
medy	med	k1gInPc1	med
dokonce	dokonce	k9	dokonce
i	i	k9	i
těžké	těžký	k2eAgInPc4d1	těžký
kovy	kov	k1gInPc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
odhalil	odhalit	k5eAaPmAgInS	odhalit
časopis	časopis	k1gInSc1	časopis
dTest	dTest	k5eAaPmF	dTest
medy	med	k1gInPc4	med
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
šizeny	šidit	k5eAaImNgInP	šidit
sirupy	sirup	k1gInPc1	sirup
a	a	k8xC	a
které	který	k3yIgNnSc1	který
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
pesticidy	pesticid	k1gInPc1	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
objevila	objevit	k5eAaPmAgFnS	objevit
kauza	kauza	k1gFnSc1	kauza
falšovaného	falšovaný	k2eAgInSc2d1	falšovaný
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Produkce	produkce	k1gFnSc1	produkce
medu	med	k1gInSc2	med
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
činila	činit	k5eAaImAgFnS	činit
světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
medu	med	k1gInSc2	med
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
Čína	Čína	k1gFnSc1	Čína
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
31	[number]	k4	31
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
čtyři	čtyři	k4xCgMnPc1	čtyři
největší	veliký	k2eAgMnPc1d3	veliký
producenti	producent	k1gMnPc1	producent
–	–	k?	–
Turecko	Turecko	k1gNnSc4	Turecko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
celkem	celkem	k6eAd1	celkem
vyprodukovaly	vyprodukovat	k5eAaPmAgFnP	vyprodukovat
22	[number]	k4	22
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Knollerová	Knollerová	k1gFnSc5	Knollerová
<g/>
,	,	kIx,	,
Rasso	Rassa	k1gFnSc5	Rassa
-	-	kIx~	-
Knížka	knížka	k1gFnSc1	knížka
o	o	k7c6	o
medu	med	k1gInSc6	med
<g/>
;	;	kIx,	;
Granit	granit	k1gInSc1	granit
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
;	;	kIx,	;
str	str	kA	str
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
80-85805-43-X	[number]	k4	80-85805-43-X
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
med	med	k1gInSc4	med
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Med	med	k1gInSc1	med
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
med	med	k1gInSc1	med
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Med	med	k1gInSc1	med
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc1	zpracování
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
medu	med	k1gInSc2	med
</s>
</p>
<p>
<s>
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
včelařský	včelařský	k2eAgInSc1d1	včelařský
v	v	k7c6	v
Dole	dol	k1gInSc6	dol
</s>
</p>
<p>
<s>
O	o	k7c6	o
medu	med	k1gInSc6	med
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
medu	med	k1gInSc6	med
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
tipy	tip	k1gInPc1	tip
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
med	med	k1gInSc4	med
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
<g/>
,	,	kIx,	,
na	na	k7c4	na
co	co	k3yQnSc4	co
si	se	k3xPyFc3	se
dávat	dávat	k5eAaImF	dávat
pozor	pozor	k1gInSc4	pozor
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
..	..	k?	..
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lesní	lesní	k2eAgInSc1d1	lesní
med	med	k1gInSc1	med
neexistuje	existovat	k5eNaImIp3nS	existovat
O	o	k7c6	o
medu	med	k1gInSc6	med
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
správném	správný	k2eAgNnSc6d1	správné
označování	označování	k1gNnSc6	označování
</s>
</p>
