<s>
Med	med	k1gInSc1	med
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
sladká	sladký	k2eAgFnSc1d1	sladká
a	a	k8xC	a
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
vytvářená	vytvářený	k2eAgFnSc1d1	vytvářená
včelami	včela	k1gFnPc7	včela
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
i	i	k9	i
jiným	jiný	k2eAgInSc7d1	jiný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
)	)	kIx)	)
sběrem	sběr	k1gInSc7	sběr
a	a	k8xC	a
zahušťováním	zahušťování	k1gNnSc7	zahušťování
sladkých	sladký	k2eAgFnPc2d1	sladká
šťáv	šťáva	k1gFnPc2	šťáva
–	–	k?	–
především	především	k9	především
nektaru	nektar	k1gInSc2	nektar
z	z	k7c2	z
květů	květ	k1gInPc2	květ
(	(	kIx(	(
<g/>
med	med	k1gInSc4	med
květový	květový	k2eAgInSc4d1	květový
<g/>
)	)	kIx)	)
a	a	k8xC	a
medovice	medovice	k1gFnSc1	medovice
(	(	kIx(	(
<g/>
med	med	k1gInSc1	med
medovicový	medovicový	k2eAgInSc1d1	medovicový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
například	například	k6eAd1	například
mšice	mšice	k1gFnPc1	mšice
<g/>
.	.	kIx.	.
</s>
