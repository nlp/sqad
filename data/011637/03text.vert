<p>
<s>
SQ-Tracker	SQ-Tracker	k1gInSc1	SQ-Tracker
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc1d1	hudební
editor	editor	k1gInSc1	editor
pro	pro	k7c4	pro
počítače	počítač	k1gMnSc4	počítač
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc4	Spectrum
a	a	k8xC	a
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Didaktik	didaktika	k1gFnPc2	didaktika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
skládat	skládat	k5eAaImF	skládat
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
hudební	hudební	k2eAgInSc4d1	hudební
čip	čip	k1gInSc4	čip
AY-	AY-	k1gFnPc4	AY-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8912	[number]	k4	8912
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
program	program	k1gInSc4	program
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Jiří	Jiří	k1gMnSc1	Jiří
Koudelka	Koudelka	k1gMnSc1	Koudelka
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
George	Georg	k1gInSc2	Georg
K.	K.	kA	K.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatelem	vydavatel	k1gMnSc7	vydavatel
programu	program	k1gInSc2	program
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc1	společnost
Proxima	Proxima	k1gFnSc1	Proxima
-	-	kIx~	-
Software	software	k1gInSc1	software
v.	v.	k?	v.
o.	o.	k?	o.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skládání	skládání	k1gNnSc1	skládání
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
trackerovým	trackerův	k2eAgInSc7d1	trackerův
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
zápisem	zápis	k1gInSc7	zápis
not	nota	k1gFnPc2	nota
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
jsou	být	k5eAaImIp3nP	být
přiřazeny	přiřazen	k2eAgFnPc1d1	přiřazena
patterny	patterna	k1gFnPc1	patterna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
pozici	pozice	k1gFnSc6	pozice
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
přehrávány	přehráván	k2eAgFnPc1d1	přehrávána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
podobný	podobný	k2eAgInSc4d1	podobný
program	program	k1gInSc4	program
Soundtracker	Soundtrackra	k1gFnPc2	Soundtrackra
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mít	mít	k5eAaImF	mít
patterny	patterna	k1gFnPc4	patterna
různých	různý	k2eAgFnPc2d1	různá
délek	délka	k1gFnPc2	délka
<g/>
,	,	kIx,	,
patterny	patterna	k1gFnPc1	patterna
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
psát	psát	k5eAaImF	psát
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
kanál	kanál	k1gInSc4	kanál
nezávisle	závisle	k6eNd1	závisle
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
pozice	pozice	k1gFnSc2	pozice
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
umístit	umístit	k5eAaPmF	umístit
pouze	pouze	k6eAd1	pouze
patterny	patterna	k1gFnPc4	patterna
stejné	stejný	k2eAgFnSc2d1	stejná
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
hudba	hudba	k1gFnSc1	hudba
dohraje	dohrát	k5eAaPmIp3nS	dohrát
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
opakována	opakovat	k5eAaImNgFnS	opakovat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
od	od	k7c2	od
jiné	jiný	k2eAgFnSc2d1	jiná
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
pozice	pozice	k1gFnSc1	pozice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přehrávána	přehrávat	k5eAaImNgFnS	přehrávat
jinou	jiný	k2eAgFnSc7d1	jiná
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
pattern	pattern	k1gInSc1	pattern
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přehráván	přehrávat	k5eAaImNgMnS	přehrávat
současně	současně	k6eAd1	současně
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
i	i	k8xC	i
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
kanálech	kanál	k1gInPc6	kanál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
programu	program	k1gInSc2	program
jsou	být	k5eAaImIp3nP	být
SQ-Compiler	SQ-Compiler	k1gInSc4	SQ-Compiler
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
napsanou	napsaný	k2eAgFnSc4d1	napsaná
hudbu	hudba	k1gFnSc4	hudba
zkompilovat	zkompilovat	k5eAaPmF	zkompilovat
a	a	k8xC	a
používat	používat	k5eAaImF	používat
ve	v	k7c6	v
vlastních	vlastní	k2eAgInPc6d1	vlastní
programech	program	k1gInPc6	program
a	a	k8xC	a
SQ-Linker	SQ-Linker	k1gInSc1	SQ-Linker
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yRgNnSc2	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spojit	spojit	k5eAaPmF	spojit
více	hodně	k6eAd2	hodně
zkompilovaných	zkompilovaný	k2eAgFnPc2d1	zkompilovaná
skladeb	skladba	k1gFnPc2	skladba
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ZX	ZX	kA	ZX
Spectru	Spectr	k1gInSc2	Spectr
128K	[number]	k4	128K
jsou	být	k5eAaImIp3nP	být
SQ-Compiler	SQ-Compiler	k1gInSc1	SQ-Compiler
a	a	k8xC	a
SQ-Linker	SQ-Linker	k1gInSc1	SQ-Linker
použitelné	použitelný	k2eAgFnSc2d1	použitelná
jako	jako	k8xC	jako
funkce	funkce	k1gFnSc2	funkce
hudebního	hudební	k2eAgInSc2d1	hudební
editoru	editor	k1gInSc2	editor
<g/>
,	,	kIx,	,
na	na	k7c6	na
ZX	ZX	kA	ZX
Spectru	Spectr	k1gInSc2	Spectr
48K	[number]	k4	48K
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
je	on	k3xPp3gInPc4	on
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
samostatné	samostatný	k2eAgInPc4d1	samostatný
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
linkeru	linker	k1gInSc2	linker
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
souboru	soubor	k1gInSc2	soubor
spojit	spojit	k5eAaPmF	spojit
až	až	k9	až
28	[number]	k4	28
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
společnosti	společnost	k1gFnSc2	společnost
Proxima	Proximum	k1gNnSc2	Proximum
-	-	kIx~	-
Software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
v.	v.	k?	v.
o.	o.	k?	o.
s.	s.	k?	s.
byl	být	k5eAaImAgInS	být
i	i	k9	i
nezávislý	závislý	k2eNgInSc1d1	nezávislý
komplet	komplet	k1gInSc1	komplet
SQ-Demo	SQ-Dema	k1gFnSc5	SQ-Dema
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
demoverzi	demoverze	k1gFnSc4	demoverze
hudebního	hudební	k2eAgInSc2d1	hudební
editoru	editor	k1gInSc2	editor
SQ-Tracker	SQ-Trackra	k1gFnPc2	SQ-Trackra
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
hudeb	hudba	k1gFnPc2	hudba
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
samplů	sampl	k1gInPc2	sampl
a	a	k8xC	a
zvukových	zvukový	k2eAgInPc2d1	zvukový
ornamentů	ornament	k1gInPc2	ornament
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
obsažen	obsažen	k2eAgInSc1d1	obsažen
konvertor	konvertor	k1gInSc1	konvertor
skladeb	skladba	k1gFnPc2	skladba
ze	z	k7c2	z
Soundtrackeru	Soundtracker	k1gInSc2	Soundtracker
do	do	k7c2	do
SQ-Trackeru	SQ-Tracker	k1gInSc2	SQ-Tracker
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
hudebně	hudebně	k6eAd1	hudebně
grafická	grafický	k2eAgFnSc1d1	grafická
dema	dema	k1gFnSc1	dema
<g/>
:	:	kIx,	:
SQ-Demo	SQ-Dema	k1gFnSc5	SQ-Dema
a	a	k8xC	a
Duckmania	Duckmanium	k1gNnPc1	Duckmanium
<g/>
.	.	kIx.	.
<g/>
Program	program	k1gInSc1	program
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
hudbu	hudba	k1gFnSc4	hudba
ukládat	ukládat	k5eAaImF	ukládat
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
kazetu	kazeta	k1gFnSc4	kazeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
diskový	diskový	k2eAgInSc4d1	diskový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
diskové	diskový	k2eAgFnPc1d1	disková
operace	operace	k1gFnPc1	operace
jsou	být	k5eAaImIp3nP	být
vykonávané	vykonávaný	k2eAgFnPc1d1	vykonávaná
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Basicu	Basicus	k1gInSc2	Basicus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgInPc4d1	možný
diskové	diskový	k2eAgInPc4d1	diskový
příkazy	příkaz	k1gInPc4	příkaz
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
diskový	diskový	k2eAgInSc4d1	diskový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
českých	český	k2eAgFnPc6d1	Česká
hudebnících	hudebník	k1gMnPc6	hudebník
činných	činný	k2eAgMnPc2d1	činný
na	na	k7c6	na
ZX	ZX	kA	ZX
Spectru	Spectr	k1gInSc2	Spectr
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
společně	společně	k6eAd1	společně
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
<g/>
,	,	kIx,	,
známých	známý	k2eAgInPc2d1	známý
pod	pod	k7c7	pod
jmény	jméno	k1gNnPc7	jméno
Scalex	Scalex	k1gInSc1	Scalex
a	a	k8xC	a
Qjeta	Qjeta	k1gFnSc1	Qjeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Soundtracker	Soundtracker	k1gMnSc1	Soundtracker
</s>
</p>
<p>
<s>
Sample	Sample	k6eAd1	Sample
Tracker	Tracker	k1gInSc1	Tracker
</s>
</p>
<p>
<s>
Wham	Wham	k6eAd1	Wham
<g/>
!	!	kIx.	!
</s>
<s>
128	[number]	k4	128
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
SQ-Tracker	SQ-Tracker	k1gInSc1	SQ-Tracker
na	na	k7c4	na
World	World	k1gInSc4	World
of	of	k?	of
Spectrum	Spectrum	k1gNnSc1	Spectrum
</s>
</p>
<p>
<s>
SQ-Compiler	SQ-Compiler	k1gInSc1	SQ-Compiler
na	na	k7c4	na
World	World	k1gInSc4	World
of	of	k?	of
Spectrum	Spectrum	k1gNnSc5	Spectrum
</s>
</p>
<p>
<s>
SQ-Tracker	SQ-Tracker	k1gInSc1	SQ-Tracker
na	na	k7c4	na
ZX	ZX	kA	ZX
Tunes	Tunes	k1gMnSc1	Tunes
</s>
</p>
<p>
<s>
SQ-Tracker	SQ-Tracker	k1gInSc1	SQ-Tracker
na	na	k7c4	na
Pouët	Pouët	k1gInSc4	Pouët
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
</s>
</p>
<p>
<s>
SQ	SQ	kA	SQ
Tracker	Tracker	k1gMnSc1	Tracker
na	na	k7c6	na
SpeccyWiki	SpeccyWik	k1gFnSc6	SpeccyWik
</s>
</p>
<p>
<s>
Something	Something	k1gInSc1	Something
for	forum	k1gNnPc2	forum
music	musice	k1gFnPc2	musice
makers	makersa	k1gFnPc2	makersa
composing	composing	k1gInSc1	composing
in	in	k?	in
SQ-TRACKER	SQ-TRACKER	k1gFnSc2	SQ-TRACKER
</s>
</p>
<p>
<s>
fotografie	fotografie	k1gFnSc1	fotografie
SQ-Trackeru	SQ-Tracker	k1gInSc2	SQ-Tracker
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc4	Spectrum
+3	+3	k4	+3
při	při	k7c6	při
koncertu	koncert	k1gInSc6	koncert
skupiny	skupina	k1gFnSc2	skupina
AY	AY	kA	AY
Riders	Riders	k1gInSc1	Riders
</s>
</p>
<p>
<s>
http://zxspectrum48.i-demo.pl/aymusic.html	[url]	k1gInSc1	http://zxspectrum48.i-demo.pl/aymusic.html
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
ukázku	ukázka	k1gFnSc4	ukázka
programů	program	k1gInPc2	program
Soundtracker	Soundtrackero	k1gNnPc2	Soundtrackero
<g/>
,	,	kIx,	,
SQ-Tracker	SQ-Tracker	k1gMnSc1	SQ-Tracker
a	a	k8xC	a
Sample	Sample	k1gMnSc1	Sample
Tracker	Tracker	k1gMnSc1	Tracker
</s>
</p>
