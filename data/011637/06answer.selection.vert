<s>
SQ-Tracker	SQ-Tracker	k1gInSc1	SQ-Tracker
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc1d1	hudební
editor	editor	k1gInSc1	editor
pro	pro	k7c4	pro
počítače	počítač	k1gMnSc4	počítač
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc4	Spectrum
a	a	k8xC	a
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Didaktik	didaktika	k1gFnPc2	didaktika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
skládat	skládat	k5eAaImF	skládat
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
hudební	hudební	k2eAgInSc4d1	hudební
čip	čip	k1gInSc4	čip
AY-	AY-	k1gFnPc4	AY-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8912	[number]	k4	8912
<g/>
.	.	kIx.	.
</s>
