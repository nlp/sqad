<s>
Kost	kost	k1gFnSc1	kost
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
os	osa	k1gFnPc2	osa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
<g/>
,	,	kIx,	,
mineralizovaná	mineralizovaný	k2eAgFnSc1d1	mineralizovaná
struktura	struktura	k1gFnSc1	struktura
sloužící	sloužící	k2eAgFnSc1d1	sloužící
jako	jako	k8xC	jako
mechanická	mechanický	k2eAgFnSc1d1	mechanická
ochrana	ochrana	k1gFnSc1	ochrana
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
opora	opora	k1gFnSc1	opora
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
upínají	upínat	k5eAaImIp3nP	upínat
svaly	sval	k1gInPc1	sval
a	a	k8xC	a
šlachy	šlacha	k1gFnPc1	šlacha
<g/>
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
kost	kost	k1gFnSc1	kost
tvořena	tvořit	k5eAaImNgFnS	tvořit
kostní	kostní	k2eAgFnSc7d1	kostní
tkání	tkáň	k1gFnSc7	tkáň
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
u	u	k7c2	u
vyšších	vysoký	k2eAgMnPc2d2	vyšší
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
:	:	kIx,	:
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
,	,	kIx,	,
plazů	plaz	k1gMnPc2	plaz
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
všech	všecek	k3xTgFnPc2	všecek
kostí	kost	k1gFnPc2	kost
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kostra	kostra	k1gFnSc1	kostra
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
kostra	kostra	k1gFnSc1	kostra
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
209	[number]	k4	209
až	až	k9	až
214	[number]	k4	214
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
osifikace	osifikace	k1gFnSc2	osifikace
<g/>
.	.	kIx.	.
</s>
<s>
Kosti	kost	k1gFnPc1	kost
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
vaziva	vazivo	k1gNnSc2	vazivo
(	(	kIx(	(
<g/>
intramembranózně	intramembranózně	k6eAd1	intramembranózně
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
chrupavky	chrupavka	k1gFnPc4	chrupavka
(	(	kIx(	(
<g/>
enchondrálně	enchondrálně	k6eAd1	enchondrálně
<g/>
)	)	kIx)	)
procesem	proces	k1gInSc7	proces
zvaným	zvaný	k2eAgInSc7d1	zvaný
kostnatění	kostnatění	k1gNnSc4	kostnatění
čili	čili	k8xC	čili
osifikace	osifikace	k1gFnPc4	osifikace
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
díky	díky	k7c3	díky
okostici	okostice	k1gFnSc3	okostice
<g/>
,	,	kIx,	,
růst	růst	k1gInSc4	růst
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
do	do	k7c2	do
určitého	určitý	k2eAgNnSc2d1	určité
stadia	stadion	k1gNnSc2	stadion
vývoje	vývoj	k1gInSc2	vývoj
jedince	jedinec	k1gMnPc4	jedinec
díky	díky	k7c3	díky
růstové	růstový	k2eAgFnSc3d1	růstová
ploténce	ploténka	k1gFnSc3	ploténka
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rostou	růst	k5eAaImIp3nP	růst
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zárodečného	zárodečný	k2eAgInSc2d1	zárodečný
vývoje	vývoj	k1gInSc2	vývoj
obratlovců	obratlovec	k1gMnPc2	obratlovec
nejprve	nejprve	k6eAd1	nejprve
vzniká	vznikat	k5eAaImIp3nS	vznikat
nezralá	zralý	k2eNgFnSc1d1	nezralá
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
kost	kost	k1gFnSc1	kost
bez	bez	k7c2	bez
stop	stopa	k1gFnPc2	stopa
lamelární	lamelární	k2eAgFnSc2d1	lamelární
struktury	struktura	k1gFnSc2	struktura
<g/>
;	;	kIx,	;
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
kostních	kostní	k2eAgFnPc2d1	kostní
buněk	buňka	k1gFnPc2	buňka
obklopených	obklopený	k2eAgFnPc2d1	obklopená
neuspořádanými	uspořádaný	k2eNgNnPc7d1	neuspořádané
vlákny	vlákno	k1gNnPc7	vlákno
kolagenu	kolagen	k1gInSc2	kolagen
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
postupně	postupně	k6eAd1	postupně
vzniká	vznikat	k5eAaImIp3nS	vznikat
kost	kost	k1gFnSc4	kost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
uspořádanou	uspořádaný	k2eAgFnSc4d1	uspořádaná
stavbu	stavba	k1gFnSc4	stavba
–	–	k?	–
tzv.	tzv.	kA	tzv.
lamelární	lamelární	k2eAgFnSc1d1	lamelární
kost	kost	k1gFnSc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
lamelární	lamelární	k2eAgFnSc1d1	lamelární
kost	kost	k1gFnSc1	kost
následně	následně	k6eAd1	následně
zraje	zrát	k5eAaImIp3nS	zrát
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
přímým	přímý	k2eAgNnSc7d1	přímé
kostnatěním	kostnatění	k1gNnSc7	kostnatění
z	z	k7c2	z
jistých	jistý	k2eAgNnPc2d1	jisté
osifikačních	osifikační	k2eAgNnPc2d1	osifikační
jader	jádro	k1gNnPc2	jádro
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přes	přes	k7c4	přes
chrupavčité	chrupavčitý	k2eAgNnSc4d1	chrupavčité
stadium	stadium	k1gNnSc4	stadium
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidských	lidský	k2eAgFnPc2d1	lidská
kostí	kost	k1gFnPc2	kost
vzniká	vznikat	k5eAaImIp3nS	vznikat
"	"	kIx"	"
<g/>
přes	přes	k7c4	přes
chrupavku	chrupavka	k1gFnSc4	chrupavka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
některé	některý	k3yIgFnPc4	některý
dermální	dermální	k2eAgFnPc4d1	dermální
kosti	kost	k1gFnPc4	kost
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
kosti	kost	k1gFnPc1	kost
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
hrudní	hrudní	k2eAgInSc1d1	hrudní
koš	koš	k1gInSc1	koš
<g/>
)	)	kIx)	)
a	a	k8xC	a
sezamoidní	sezamoidní	k2eAgFnPc1d1	sezamoidní
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgInSc1d1	vznikající
intramembranózně	intramembranózně	k6eAd1	intramembranózně
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
osifikací	osifikace	k1gFnPc2	osifikace
vazivové	vazivový	k2eAgFnSc2d1	vazivová
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Kostní	kostní	k2eAgFnSc1d1	kostní
tkáň	tkáň	k1gFnSc1	tkáň
je	být	k5eAaImIp3nS	být
typem	typ	k1gInSc7	typ
pojiva	pojivo	k1gNnSc2	pojivo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
mineralizací	mineralizace	k1gFnSc7	mineralizace
mezibuněčné	mezibuněčný	k2eAgFnSc2d1	mezibuněčná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Minerální	minerální	k2eAgFnSc4d1	minerální
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
až	až	k9	až
65	[number]	k4	65
<g/>
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
submikroskopické	submikroskopický	k2eAgInPc1d1	submikroskopický
krystaly	krystal	k1gInPc1	krystal
fosforečnanu	fosforečnan	k1gInSc2	fosforečnan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
,	,	kIx,	,
hydroxyapatitu	hydroxyapatit	k1gInSc2	hydroxyapatit
<g/>
.	.	kIx.	.
</s>
<s>
Krystaly	krystal	k1gInPc1	krystal
jsou	být	k5eAaImIp3nP	být
vázány	vázán	k2eAgMnPc4d1	vázán
na	na	k7c4	na
kolagenní	kolagenní	k2eAgNnPc4d1	kolagenní
vlákna	vlákno	k1gNnPc4	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Mezibuněčnou	mezibuněčný	k2eAgFnSc4d1	mezibuněčná
hmotu	hmota	k1gFnSc4	hmota
produkují	produkovat	k5eAaImIp3nP	produkovat
buňky	buňka	k1gFnPc1	buňka
osteoblasty	osteoblast	k1gInPc4	osteoblast
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
organelovou	organelový	k2eAgFnSc4d1	organelový
výbavu	výbava	k1gFnSc4	výbava
a	a	k8xC	a
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
výběžky	výběžek	k1gInPc4	výběžek
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
osteoblasty	osteoblast	k1gInPc7	osteoblast
a	a	k8xC	a
s	s	k7c7	s
cévou	céva	k1gFnSc7	céva
přivádějící	přivádějící	k2eAgFnSc2d1	přivádějící
živiny	živina	k1gFnSc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Nemineralizovaná	mineralizovaný	k2eNgFnSc1d1	mineralizovaný
kostní	kostní	k2eAgFnSc1d1	kostní
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ossein	ossein	k1gInSc1	ossein
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
ukládání	ukládání	k1gNnSc3	ukládání
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
osteoblast	osteoblast	k1gInSc1	osteoblast
obklopí	obklopit	k5eAaPmIp3nS	obklopit
vyprodukovanou	vyprodukovaný	k2eAgFnSc7d1	vyprodukovaná
mezibuněčnou	mezibuněčný	k2eAgFnSc7d1	mezibuněčná
hmotou	hmota	k1gFnSc7	hmota
<g/>
,	,	kIx,	,
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
na	na	k7c4	na
osteocyt	osteocyt	k1gInSc4	osteocyt
-	-	kIx~	-
buňku	buňka	k1gFnSc4	buňka
uzavřenou	uzavřený	k2eAgFnSc7d1	uzavřená
okolní	okolní	k2eAgFnSc7d1	okolní
kostí	kost	k1gFnSc7	kost
<g/>
,	,	kIx,	,
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
spojenou	spojený	k2eAgFnSc4d1	spojená
pouze	pouze	k6eAd1	pouze
výběžky	výběžek	k1gInPc1	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Osteoklasty	osteoklast	k1gInPc1	osteoklast
jsou	být	k5eAaImIp3nP	být
obrovské	obrovský	k2eAgInPc1d1	obrovský
(	(	kIx(	(
<g/>
i	i	k9	i
100	[number]	k4	100
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohojaderné	mnohojaderný	k2eAgFnPc1d1	mnohojaderná
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
naopak	naopak	k6eAd1	naopak
kostní	kostní	k2eAgFnSc4d1	kostní
tkáň	tkáň	k1gFnSc4	tkáň
odbourávají	odbourávat	k5eAaImIp3nP	odbourávat
<g/>
.	.	kIx.	.
</s>
<s>
Produkují	produkovat	k5eAaImIp3nP	produkovat
kyselou	kyselý	k2eAgFnSc4d1	kyselá
fosfatázu	fosfatáza	k1gFnSc4	fosfatáza
a	a	k8xC	a
kolagenázu	kolagenáza	k1gFnSc4	kolagenáza
a	a	k8xC	a
resorbují	resorbovat	k5eAaBmIp3nP	resorbovat
vytvořenou	vytvořený	k2eAgFnSc4d1	vytvořená
kostní	kostní	k2eAgFnSc4d1	kostní
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přestavbu	přestavba	k1gFnSc4	přestavba
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Makroskopicky	makroskopicky	k6eAd1	makroskopicky
lze	lze	k6eAd1	lze
kost	kost	k1gFnSc4	kost
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
kompaktní	kompaktní	k2eAgInPc4d1	kompaktní
a	a	k8xC	a
spongiózní	spongiózní	k2eAgMnPc4d1	spongiózní
(	(	kIx(	(
<g/>
houbovitou	houbovitý	k2eAgFnSc7d1	houbovitá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
v	v	k7c6	v
mikroskopu	mikroskop	k1gInSc6	mikroskop
však	však	k9	však
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
základní	základní	k2eAgFnSc4d1	základní
histologickou	histologický	k2eAgFnSc4d1	histologická
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Histologicky	histologicky	k6eAd1	histologicky
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
:	:	kIx,	:
primární	primární	k2eAgFnSc4d1	primární
(	(	kIx(	(
<g/>
nezralou	zralý	k2eNgFnSc4d1	nezralá
<g/>
,	,	kIx,	,
vláknitou	vláknitý	k2eAgFnSc4d1	vláknitá
kost	kost	k1gFnSc4	kost
<g/>
)	)	kIx)	)
a	a	k8xC	a
sekundární	sekundární	k2eAgFnSc4d1	sekundární
(	(	kIx(	(
<g/>
zralou	zralý	k2eAgFnSc4d1	zralá
<g/>
,	,	kIx,	,
lamelární	lamelární	k2eAgFnSc4d1	lamelární
kost	kost	k1gFnSc4	kost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
stejné	stejný	k2eAgFnPc4d1	stejná
stavební	stavební	k2eAgFnPc4d1	stavební
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
uspořádání	uspořádání	k1gNnSc6	uspořádání
kolageních	kolagení	k1gNnPc6	kolagení
vláken	vlákna	k1gFnPc2	vlákna
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
nezralé	zralý	k2eNgFnSc6d1	nezralá
vláknité	vláknitý	k2eAgFnSc6d1	vláknitá
kosti	kost	k1gFnSc6	kost
probíhají	probíhat	k5eAaImIp3nP	probíhat
vlákna	vlákno	k1gNnPc4	vlákno
náhodně	náhodně	k6eAd1	náhodně
<g/>
,	,	kIx,	,
neuspořádaně	uspořádaně	k6eNd1	uspořádaně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zralé	zralý	k2eAgFnSc6d1	zralá
kosti	kost	k1gFnSc6	kost
jsou	být	k5eAaImIp3nP	být
uspořádána	uspořádat	k5eAaPmNgNnP	uspořádat
do	do	k7c2	do
lamel	lamela	k1gFnPc2	lamela
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
každé	každý	k3xTgFnSc2	každý
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
růstu	růst	k1gInSc2	růst
i	i	k8xC	i
hojení	hojení	k1gNnSc2	hojení
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
nejprve	nejprve	k6eAd1	nejprve
nezralá	zralý	k2eNgFnSc1d1	nezralá
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
(	(	kIx(	(
<g/>
fibrilární	fibrilární	k2eAgFnSc1d1	fibrilární
<g/>
)	)	kIx)	)
kost	kost	k1gFnSc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
dočasná	dočasný	k2eAgFnSc1d1	dočasná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
zralou	zralý	k2eAgFnSc7d1	zralá
sekundární	sekundární	k2eAgFnSc7d1	sekundární
kostí	kost	k1gFnSc7	kost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
fibrilární	fibrilární	k2eAgFnSc4d1	fibrilární
kost	kost	k1gFnSc4	kost
nacházíme	nacházet	k5eAaImIp1nP	nacházet
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
úponu	úpon	k1gInSc2	úpon
šlach	šlacha	k1gFnPc2	šlacha
na	na	k7c4	na
tuberositates	tuberositates	k1gInSc4	tuberositates
ossium	ossium	k1gNnSc4	ossium
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
stavbu	stavba	k1gFnSc4	stavba
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
zubní	zubní	k2eAgInSc4d1	zubní
cement	cement	k1gInSc4	cement
<g/>
.	.	kIx.	.
</s>
<s>
Kost	kost	k1gFnSc1	kost
lamelární	lamelární	k2eAgFnSc1d1	lamelární
(	(	kIx(	(
<g/>
vrstevnatá	vrstevnatý	k2eAgFnSc1d1	vrstevnatá
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
častěji	často	k6eAd2	často
než	než	k8xS	než
kost	kost	k1gFnSc1	kost
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kostní	kostní	k2eAgFnSc1d1	kostní
tkáň	tkáň	k1gFnSc1	tkáň
dospělých	dospělí	k1gMnPc2	dospělí
má	mít	k5eAaImIp3nS	mít
kolagenní	kolagenní	k2eAgFnSc1d1	kolagenní
vlákna	vlákna	k1gFnSc1	vlákna
charakteristicky	charakteristicky	k6eAd1	charakteristicky
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
do	do	k7c2	do
lamel	lamela	k1gFnPc2	lamela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
paralelně	paralelně	k6eAd1	paralelně
jedna	jeden	k4xCgFnSc1	jeden
vedle	vedle	k7c2	vedle
druhé	druhý	k4xOgFnSc2	druhý
nebo	nebo	k8xC	nebo
koncentricky	koncentricky	k6eAd1	koncentricky
kolem	kolem	k7c2	kolem
kanálku	kanálek	k1gInSc2	kanálek
s	s	k7c7	s
cévami	céva	k1gFnPc7	céva
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolagenní	kolagenní	k2eAgNnPc1d1	kolagenní
vlákna	vlákno	k1gNnPc1	vlákno
tvoří	tvořit	k5eAaImIp3nP	tvořit
po	po	k7c6	po
délce	délka	k1gFnSc6	délka
osteonu	osteon	k1gInSc2	osteon
šroubovici	šroubovice	k1gFnSc4	šroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
závitu	závit	k1gInSc2	závit
této	tento	k3xDgFnSc2	tento
šroubovice	šroubovice	k1gFnSc2	šroubovice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
lamelách	lamela	k1gFnPc6	lamela
natolik	natolik	k6eAd1	natolik
různá	různý	k2eAgFnSc1d1	různá
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
bodě	bod	k1gInSc6	bod
jsou	být	k5eAaImIp3nP	být
kolagenní	kolagenní	k2eAgNnPc1d1	kolagenní
vlákna	vlákno	k1gNnPc1	vlákno
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
lamelách	lamela	k1gFnPc6	lamela
vzájemně	vzájemně	k6eAd1	vzájemně
orientována	orientován	k2eAgFnSc1d1	orientována
přibližně	přibližně	k6eAd1	přibližně
kolmo	kolmo	k6eAd1	kolmo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lamelami	lamela	k1gFnPc7	lamela
leží	ležet	k5eAaImIp3nS	ležet
vrstvy	vrstva	k1gFnPc4	vrstva
osteocytů	osteocyt	k1gInPc2	osteocyt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
komůrkách	komůrka	k1gFnPc6	komůrka
zvápenatělé	zvápenatělý	k2eAgFnSc2d1	zvápenatělá
kostní	kostní	k2eAgInSc4d1	kostní
matrix	matrix	k1gInSc4	matrix
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
lakuny	lakuna	k1gFnPc1	lakuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
lamele	lamela	k1gFnSc6	lamela
probíhají	probíhat	k5eAaImIp3nP	probíhat
kolagenní	kolagenní	k2eAgNnPc1d1	kolagenní
vlákna	vlákno	k1gNnPc1	vlákno
paralelně	paralelně	k6eAd1	paralelně
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Okostice	okostice	k1gFnSc2	okostice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
kost	kost	k1gFnSc1	kost
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
pevnou	pevný	k2eAgFnSc7d1	pevná
vazivovou	vazivový	k2eAgFnSc7d1	vazivová
blánou	blána	k1gFnSc7	blána
<g/>
,	,	kIx,	,
okosticí	okostice	k1gFnSc7	okostice
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
periost	periost	k1gInSc1	periost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
fibroblastů	fibroblast	k1gInPc2	fibroblast
<g/>
,	,	kIx,	,
elastických	elastický	k2eAgNnPc2d1	elastické
a	a	k8xC	a
kolagenních	kolagenní	k2eAgNnPc2d1	kolagenní
vláken	vlákno	k1gNnPc2	vlákno
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
cévní	cévní	k2eAgFnPc4d1	cévní
pleteně	pleteň	k1gFnPc4	pleteň
a	a	k8xC	a
nervová	nervový	k2eAgNnPc1d1	nervové
zakončení	zakončení	k1gNnPc1	zakončení
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
nemalé	malý	k2eNgNnSc1d1	nemalé
množství	množství	k1gNnSc1	množství
osteoblastů	osteoblast	k1gInPc2	osteoblast
a	a	k8xC	a
osteoklastů	osteoklast	k1gInPc2	osteoklast
<g/>
.	.	kIx.	.
</s>
<s>
Okostice	okostice	k1gFnPc4	okostice
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
cévní	cévní	k2eAgNnSc1d1	cévní
zásobení	zásobení	k1gNnSc1	zásobení
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
upínají	upínat	k5eAaImIp3nP	upínat
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
svaly	sval	k1gInPc4	sval
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
růst	růst	k1gInSc4	růst
kostí	kost	k1gFnPc2	kost
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
hojení	hojení	k1gNnSc2	hojení
zlomenin	zlomenina	k1gFnPc2	zlomenina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kostní	kostní	k2eAgFnSc4d1	kostní
dřeň	dřeň	k1gFnSc4	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
uvnitř	uvnitř	k7c2	uvnitř
kostí	kost	k1gFnPc2	kost
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
hemopoetickou	hemopoetický	k2eAgFnSc7d1	hemopoetický
tkání	tkáň	k1gFnSc7	tkáň
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
krvetvorba	krvetvorba	k1gFnSc1	krvetvorba
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
kostní	kostní	k2eAgFnSc3d1	kostní
dřeni	dřeň	k1gFnSc3	dřeň
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
(	(	kIx(	(
<g/>
medulla	medulla	k6eAd1	medulla
ossium	ossium	k1gNnSc1	ossium
rubra	rubrum	k1gNnSc2	rubrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
(	(	kIx(	(
<g/>
medulla	medulla	k6eAd1	medulla
ossium	ossium	k1gNnSc1	ossium
flava	flavo	k1gNnSc2	flavo
<g/>
)	)	kIx)	)
a	a	k8xC	a
šedá	šedý	k2eAgFnSc1d1	šedá
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
(	(	kIx(	(
<g/>
medulla	medulla	k6eAd1	medulla
ossium	ossium	k1gNnSc1	ossium
grisea	grise	k1gInSc2	grise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
jedinců	jedinec	k1gMnPc2	jedinec
se	se	k3xPyFc4	se
uvnitř	uvnitř	k7c2	uvnitř
všech	všecek	k3xTgFnPc2	všecek
kostí	kost	k1gFnPc2	kost
nachází	nacházet	k5eAaImIp3nS	nacházet
červená	červený	k2eAgFnSc1d1	červená
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
nahrazována	nahrazovat	k5eAaImNgFnS	nahrazovat
žlutou	žlutý	k2eAgFnSc7d1	žlutá
kostní	kostní	k2eAgFnSc7d1	kostní
dření	dřeň	k1gFnSc7	dřeň
a	a	k8xC	a
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
plochých	plochý	k2eAgFnPc6d1	plochá
kostech	kost	k1gFnPc6	kost
<g/>
,	,	kIx,	,
hrudní	hrudní	k2eAgFnPc1d1	hrudní
kosti	kost	k1gFnPc1	kost
<g/>
,	,	kIx,	,
žebrech	žebr	k1gInPc6	žebr
<g/>
,	,	kIx,	,
lebečních	lebeční	k2eAgFnPc6d1	lebeční
kostech	kost	k1gFnPc6	kost
<g/>
,	,	kIx,	,
pánvi	pánev	k1gFnSc6	pánev
a	a	k8xC	a
stehenní	stehenní	k2eAgFnSc6d1	stehenní
kosti	kost	k1gFnSc6	kost
<g/>
.	.	kIx.	.
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
tukovou	tukový	k2eAgFnSc7d1	tuková
tkání	tkáň	k1gFnSc7	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Šedá	šedý	k2eAgFnSc1d1	šedá
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
je	být	k5eAaImIp3nS	být
degradovaná	degradovaný	k2eAgFnSc1d1	degradovaná
žlutá	žlutý	k2eAgFnSc1d1	žlutá
dřeň	dřeň	k1gFnSc1	dřeň
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
ji	on	k3xPp3gFnSc4	on
najít	najít	k5eAaPmF	najít
u	u	k7c2	u
starých	starý	k2eAgMnPc2d1	starý
<g/>
,	,	kIx,	,
podvyživených	podvyživený	k2eAgMnPc2d1	podvyživený
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Řídnutí	řídnutí	k1gNnSc1	řídnutí
kostí	kost	k1gFnPc2	kost
nastává	nastávat	k5eAaImIp3nS	nastávat
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
menstruačního	menstruační	k2eAgNnSc2d1	menstruační
období	období	k1gNnSc2	období
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
osteoporóza	osteoporóza	k1gFnSc1	osteoporóza
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
se	se	k3xPyFc4	se
kosti	kost	k1gFnPc1	kost
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgFnSc6d1	krátká
<g/>
,	,	kIx,	,
ploché	plochý	k2eAgFnSc6d1	plochá
a	a	k8xC	a
nepravidelné	pravidelný	k2eNgFnSc6d1	nepravidelná
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
kostí	kost	k1gFnSc7	kost
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
kost	kost	k1gFnSc1	kost
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
longum	longum	k1gInSc1	longum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
kosti	kost	k1gFnPc1	kost
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
protáhlý	protáhlý	k2eAgInSc1d1	protáhlý
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
prostředek	prostředek	k1gInSc1	prostředek
kosti	kost	k1gFnSc2	kost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
diafýza	diafýza	k1gFnSc1	diafýza
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
konce	konec	k1gInPc1	konec
kosti	kost	k1gFnSc2	kost
pak	pak	k6eAd1	pak
epifýzy	epifýza	k1gFnSc2	epifýza
<g/>
.	.	kIx.	.
</s>
<s>
Kost	kost	k1gFnSc1	kost
není	být	k5eNaImIp3nS	být
kompaktní	kompaktní	k2eAgFnSc7d1	kompaktní
tkání	tkáň	k1gFnSc7	tkáň
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
kosti	kost	k1gFnSc2	kost
je	být	k5eAaImIp3nS	být
tenká	tenký	k2eAgFnSc1d1	tenká
vrstva	vrstva	k1gFnSc1	vrstva
skutečně	skutečně	k6eAd1	skutečně
kompaktní	kompaktní	k2eAgFnSc2d1	kompaktní
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
substantia	substantius	k1gMnSc2	substantius
compacta	compact	k1gMnSc2	compact
<g/>
.	.	kIx.	.
</s>
<s>
Epifýzy	epifýza	k1gFnPc1	epifýza
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
spongiózní	spongiózní	k2eAgFnSc7d1	spongiózní
kostí	kost	k1gFnSc7	kost
(	(	kIx(	(
<g/>
substantia	substantia	k1gFnSc1	substantia
spongiosa	spongiosa	k1gFnSc1	spongiosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
trámci	trámec	k1gInPc7	trámec
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
největšího	veliký	k2eAgNnSc2d3	veliký
zatížení	zatížení	k1gNnSc2	zatížení
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Kost	kost	k1gFnSc1	kost
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
odlehčená	odlehčený	k2eAgFnSc1d1	odlehčená
<g/>
.	.	kIx.	.
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1	uspořádání
trámců	trámec	k1gInPc2	trámec
tvoří	tvořit	k5eAaImIp3nS	tvořit
architektoniku	architektonika	k1gFnSc4	architektonika
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
kosti	kost	k1gFnSc2	kost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dřeňová	dřeňový	k2eAgFnSc1d1	dřeňová
dutina	dutina	k1gFnSc1	dutina
(	(	kIx(	(
<g/>
cavum	cavum	k1gInSc1	cavum
medullare	medullar	k1gMnSc5	medullar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
vyplněná	vyplněný	k2eAgFnSc1d1	vyplněná
žlutou	žlutý	k2eAgFnSc7d1	žlutá
kostní	kostní	k2eAgFnSc7d1	kostní
dření	dřeň	k1gFnSc7	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Epifýzy	epifýza	k1gFnPc1	epifýza
nejsou	být	k5eNaImIp3nP	být
kryté	krytý	k2eAgInPc1d1	krytý
periostem	periost	k1gInSc7	periost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hyalinní	hyalinní	k2eAgFnSc7d1	hyalinní
kloubní	kloubní	k2eAgFnSc7d1	kloubní
chrupavkou	chrupavka	k1gFnSc7	chrupavka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
diafýzou	diafýza	k1gFnSc7	diafýza
a	a	k8xC	a
epifýzou	epifýza	k1gFnSc7	epifýza
se	se	k3xPyFc4	se
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
růstu	růst	k1gInSc2	růst
nachází	nacházet	k5eAaImIp3nS	nacházet
epifyzodiafyzární	epifyzodiafyzární	k2eAgFnSc1d1	epifyzodiafyzární
ploténka	ploténka	k1gFnSc1	ploténka
<g/>
,	,	kIx,	,
chrupavčitá	chrupavčitý	k2eAgFnSc1d1	chrupavčitá
destička	destička	k1gFnSc1	destička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
růst	růst	k1gInSc4	růst
kostí	kost	k1gFnPc2	kost
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Krátká	krátký	k2eAgFnSc1d1	krátká
kost	kost	k1gFnSc1	kost
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
breve	breve	k1gNnSc2	breve
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stavebně	stavebně	k6eAd1	stavebně
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
všechny	všechen	k3xTgInPc4	všechen
rozměry	rozměr	k1gInPc4	rozměr
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgInPc4d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
obratel	obratel	k1gInSc4	obratel
nebo	nebo	k8xC	nebo
kopytní	kopytní	k2eAgFnSc4d1	kopytní
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Plochá	plochý	k2eAgFnSc1d1	plochá
kost	kost	k1gFnSc1	kost
(	(	kIx(	(
<g/>
os	osa	k1gFnPc2	osa
planum	planum	k1gInSc1	planum
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
zevní	zevní	k2eAgFnSc6d1	zevní
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
části	část	k1gFnSc6	část
různě	různě	k6eAd1	různě
silnou	silný	k2eAgFnSc4d1	silná
kompaktní	kompaktní	k2eAgFnSc4d1	kompaktní
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
je	být	k5eAaImIp3nS	být
spongiózní	spongiózní	k2eAgFnSc1d1	spongiózní
kost	kost	k1gFnSc1	kost
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
trámci	trámec	k1gInPc7	trámec
<g/>
.	.	kIx.	.
<g/>
Tyto	tento	k3xDgFnPc1	tento
prostory	prostora	k1gFnPc1	prostora
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
do	do	k7c2	do
pozdního	pozdní	k2eAgInSc2d1	pozdní
věku	věk	k1gInSc2	věk
zaplněny	zaplněn	k2eAgInPc1d1	zaplněn
červenou	červený	k2eAgFnSc7d1	červená
kostní	kostní	k2eAgFnSc7d1	kostní
dření	dřeň	k1gFnSc7	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
také	také	k9	také
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
pneumatizaci	pneumatizace	k1gFnSc3	pneumatizace
-	-	kIx~	-
vytvoření	vytvoření	k1gNnSc1	vytvoření
dutiny	dutina	k1gFnSc2	dutina
vystlané	vystlaný	k2eAgNnSc1d1	vystlané
sliznicí	sliznice	k1gFnSc7	sliznice
<g/>
,	,	kIx,	,
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
čelní	čelní	k2eAgFnSc6d1	čelní
a	a	k8xC	a
čichové	čichový	k2eAgFnSc6d1	čichová
kosti	kost	k1gFnSc6	kost
a	a	k8xC	a
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
skot	skot	k1gInSc1	skot
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozsah	rozsah	k1gInSc4	rozsah
pneumatizace	pneumatizace	k1gFnSc2	pneumatizace
větší	veliký	k2eAgFnSc2d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ploché	plochý	k2eAgFnPc4d1	plochá
kosti	kost	k1gFnPc4	kost
patří	patřit	k5eAaImIp3nP	patřit
kosti	kost	k1gFnPc1	kost
neurokrania	neurokranium	k1gNnSc2	neurokranium
<g/>
,	,	kIx,	,
lopatka	lopatka	k1gFnSc1	lopatka
<g/>
,	,	kIx,	,
žebra	žebro	k1gNnPc1	žebro
nebo	nebo	k8xC	nebo
pánev	pánev	k1gFnSc1	pánev
<g/>
.	.	kIx.	.
</s>
