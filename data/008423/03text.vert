<p>
<s>
Výkřik	výkřik	k1gInSc1	výkřik
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Křik	křik	k1gInSc1	křik
<g/>
,	,	kIx,	,
norsky	norsky	k6eAd1	norsky
Skrik	Skrik	k1gInSc1	Skrik
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
expresionistický	expresionistický	k2eAgInSc4d1	expresionistický
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
dílo	dílo	k1gNnSc4	dílo
norského	norský	k2eAgMnSc2d1	norský
malíře	malíř	k1gMnSc2	malíř
Edvarda	Edvard	k1gMnSc2	Edvard
Muncha	Munch	k1gMnSc2	Munch
<g/>
.	.	kIx.	.
</s>
<s>
Zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
výkřikem	výkřik	k1gInSc7	výkřik
znetvořenou	znetvořený	k2eAgFnSc4d1	znetvořená
postavu	postava	k1gFnSc4	postava
s	s	k7c7	s
lysou	lysý	k2eAgFnSc7d1	Lysá
lebkou	lebka	k1gFnSc7	lebka
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
stylizované	stylizovaný	k2eAgFnSc2d1	stylizovaná
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
krvavého	krvavý	k2eAgNnSc2d1	krvavé
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
"	"	kIx"	"
<g/>
krvavého	krvavý	k2eAgNnSc2d1	krvavé
nebe	nebe	k1gNnSc2	nebe
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
trojvariace	trojvariace	k1gFnSc2	trojvariace
Beznaděj	beznaděj	k1gFnSc1	beznaděj
–	–	k?	–
Výkřik	výkřik	k1gInSc1	výkřik
–	–	k?	–
Úzkost	úzkost	k1gFnSc1	úzkost
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
verzích	verze	k1gFnPc6	verze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikaly	vznikat	k5eAaImAgFnP	vznikat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
litografická	litografický	k2eAgFnSc1d1	litografická
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
verze	verze	k1gFnPc1	verze
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
norská	norský	k2eAgFnSc1d1	norská
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
Munchovo	Munchův	k2eAgNnSc1d1	Munchovo
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
a	a	k8xC	a
soukromý	soukromý	k2eAgMnSc1d1	soukromý
sběratel	sběratel	k1gMnSc1	sběratel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
několikrát	několikrát	k6eAd1	několikrát
terčem	terč	k1gInSc7	terč
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
zlodějů	zloděj	k1gMnPc2	zloděj
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
verzí	verze	k1gFnPc2	verze
byla	být	k5eAaImAgFnS	být
ukradena	ukrást	k5eAaPmNgFnS	ukrást
z	z	k7c2	z
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
nalezena	naleznout	k5eAaPmNgFnS	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
ukradena	ukrást	k5eAaPmNgFnS	ukrást
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Madonou	Madona	k1gFnSc7	Madona
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
nalezena	nalezen	k2eAgNnPc1d1	Nalezeno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
a	a	k8xC	a
po	po	k7c6	po
drobných	drobný	k2eAgFnPc6d1	drobná
opravách	oprava	k1gFnPc6	oprava
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
obrazu	obraz	k1gInSc2	obraz
ze	z	k7c2	z
soukromé	soukromý	k2eAgFnSc2d1	soukromá
sbírky	sbírka	k1gFnSc2	sbírka
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
Pettera	Petter	k1gMnSc2	Petter
Olsona	Olson	k1gMnSc2	Olson
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
prodána	prodat	k5eAaPmNgFnS	prodat
za	za	k7c4	za
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
aukci	aukce	k1gFnSc4	aukce
v	v	k7c6	v
newyorském	newyorský	k2eAgInSc6d1	newyorský
aukčním	aukční	k2eAgInSc6d1	aukční
domě	dům	k1gInSc6	dům
Sotheby	Sotheba	k1gFnSc2	Sotheba
<g/>
'	'	kIx"	'
<g/>
s.	s.	k?	s.
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nominálně	nominálně	k6eAd1	nominálně
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
za	za	k7c4	za
jakou	jaký	k3yIgFnSc4	jaký
byl	být	k5eAaImAgMnS	být
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
aukci	aukce	k1gFnSc6	aukce
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
obraz	obraz	k1gInSc1	obraz
prodán	prodat	k5eAaPmNgInS	prodat
<g/>
,	,	kIx,	,
při	při	k7c6	při
započtení	započtení	k1gNnSc6	započtení
inflace	inflace	k1gFnSc2	inflace
by	by	kYmCp3nS	by
však	však	k9	však
Výkřik	výkřik	k1gInSc1	výkřik
byl	být	k5eAaImAgInS	být
až	až	k9	až
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
inspirace	inspirace	k1gFnSc1	inspirace
==	==	k?	==
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
zásadní	zásadní	k2eAgFnSc7d1	zásadní
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
obrazu	obraz	k1gInSc2	obraz
se	se	k3xPyFc4	se
Munchovi	Munch	k1gMnSc3	Munch
stala	stát	k5eAaPmAgFnS	stát
příhoda	příhoda	k1gFnSc1	příhoda
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
specifický	specifický	k2eAgInSc1d1	specifický
přírodní	přírodní	k2eAgInSc1d1	přírodní
úkaz	úkaz	k1gInSc1	úkaz
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
si	se	k3xPyFc3	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
deníku	deník	k1gInSc2	deník
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
toto	tento	k3xDgNnSc1	tento
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Z	z	k7c2	z
poznámky	poznámka	k1gFnSc2	poznámka
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
důležitý	důležitý	k2eAgInSc1d1	důležitý
motiv	motiv	k1gInSc1	motiv
krvavého	krvavý	k2eAgNnSc2d1	krvavé
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
meteorologický	meteorologický	k2eAgInSc1d1	meteorologický
jev	jev	k1gInSc1	jev
běžně	běžně	k6eAd1	běžně
viditelný	viditelný	k2eAgInSc1d1	viditelný
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
ho	on	k3xPp3gInSc4	on
zachytili	zachytit	k5eAaPmAgMnP	zachytit
již	již	k6eAd1	již
romantičtí	romantický	k2eAgMnPc1d1	romantický
krajináři	krajinář	k1gMnPc1	krajinář
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
mimoto	mimoto	k6eAd1	mimoto
objevila	objevit	k5eAaPmAgFnS	objevit
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Munch	Munch	k1gInSc1	Munch
mohl	moct	k5eAaImAgInS	moct
sledovat	sledovat	k5eAaImF	sledovat
jev	jev	k1gInSc1	jev
erupcí	erupce	k1gFnPc2	erupce
sopky	sopka	k1gFnSc2	sopka
Krakatoa	Krakatoum	k1gNnSc2	Krakatoum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
viditelný	viditelný	k2eAgMnSc1d1	viditelný
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1883	[number]	k4	1883
a	a	k8xC	a
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
obraz	obraz	k1gInSc1	obraz
zachycující	zachycující	k2eAgInSc1d1	zachycující
bytostný	bytostný	k2eAgInSc4d1	bytostný
strach	strach	k1gInSc4	strach
v	v	k7c6	v
nádheře	nádhera	k1gFnSc6	nádhera
přírodní	přírodní	k2eAgFnSc2d1	přírodní
scenérie	scenérie	k1gFnSc2	scenérie
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Munch	Munch	k1gMnSc1	Munch
už	už	k6eAd1	už
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
a	a	k8xC	a
nazval	nazvat	k5eAaBmAgMnS	nazvat
jej	on	k3xPp3gNnSc4	on
Beznaděj	beznaděj	k1gFnSc1	beznaděj
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Thielska	Thielska	k1gFnSc1	Thielska
Galleriet	Galleriet	k1gMnSc1	Galleriet
<g/>
)	)	kIx)	)
a	a	k8xC	a
krajinou	krajina	k1gFnSc7	krajina
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
identický	identický	k2eAgMnSc1d1	identický
s	s	k7c7	s
Výkřikem	výkřik	k1gInSc7	výkřik
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
postavu	postava	k1gFnSc4	postava
z	z	k7c2	z
profilu	profil	k1gInSc2	profil
opírající	opírající	k2eAgFnSc1d1	opírající
se	se	k3xPyFc4	se
o	o	k7c6	o
zábradlí	zábradlí	k1gNnSc6	zábradlí
a	a	k8xC	a
sledující	sledující	k2eAgFnSc4d1	sledující
hloubku	hloubka	k1gFnSc4	hloubka
fjordu	fjord	k1gInSc2	fjord
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
z	z	k7c2	z
Nice	Nice	k1gFnSc2	Nice
navracel	navracet	k5eAaImAgMnS	navracet
přes	přes	k7c4	přes
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
prohluboval	prohlubovat	k5eAaImAgInS	prohlubovat
pocit	pocit	k1gInSc1	pocit
osamocení	osamocení	k1gNnPc2	osamocení
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
patrno	patrn	k2eAgNnSc1d1	patrno
na	na	k7c6	na
Večeru	večer	k1gInSc6	večer
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Karla	Karel	k1gMnSc2	Karel
Johana	Johan	k1gMnSc2	Johan
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
v	v	k7c6	v
Asgardstadtu	Asgardstadt	k1gInSc6	Asgardstadt
vzniká	vznikat	k5eAaImIp3nS	vznikat
obraz	obraz	k1gInSc1	obraz
Bouře	bouř	k1gFnSc2	bouř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
volnější	volný	k2eAgInSc1d2	volnější
rukopis	rukopis	k1gInSc1	rukopis
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
Böcklina	Böcklina	k1gFnSc1	Böcklina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
na	na	k7c6	na
společnicích	společnice	k1gFnPc6	společnice
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
gesto	gesto	k1gNnSc1	gesto
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
také	také	k9	také
na	na	k7c6	na
Výkřiku	výkřik	k1gInSc6	výkřik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
konečně	konečně	k6eAd1	konečně
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
jako	jako	k8xC	jako
Bouři	bouře	k1gFnSc4	bouře
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Výkřik	výkřik	k1gInSc1	výkřik
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
svůj	svůj	k3xOyFgInSc4	svůj
předobraz	předobraz	k1gInSc4	předobraz
v	v	k7c6	v
pohledu	pohled	k1gInSc6	pohled
na	na	k7c6	na
Oslo	Oslo	k1gNnSc6	Oslo
z	z	k7c2	z
kopce	kopec	k1gInSc2	kopec
Ekeberg	Ekeberg	k1gInSc1	Ekeberg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
přišel	přijít	k5eAaPmAgMnS	přijít
kunsthistorik	kunsthistorik	k1gMnSc1	kunsthistorik
Robert	Robert	k1gMnSc1	Robert
Rosenblum	Rosenblum	k1gInSc4	Rosenblum
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
podoba	podoba	k1gFnSc1	podoba
dominantní	dominantní	k2eAgFnSc2d1	dominantní
figury	figura	k1gFnSc2	figura
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
peruánskou	peruánský	k2eAgFnSc7d1	peruánská
mumií	mumie	k1gFnSc7	mumie
vystavovanou	vystavovaný	k2eAgFnSc7d1	vystavovaná
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
Trocadéru	Trocadér	k1gInSc6	Trocadér
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
mumii	mumie	k1gFnSc4	mumie
také	také	k9	také
použil	použít	k5eAaPmAgInS	použít
několikrát	několikrát	k6eAd1	několikrát
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Odkud	odkud	k6eAd1	odkud
přicházíme	přicházet	k5eAaImIp1nP	přicházet
<g/>
?	?	kIx.	?
</s>
<s>
Co	co	k3yInSc4	co
jsme	být	k5eAaImIp1nP	být
<g/>
?	?	kIx.	?
</s>
<s>
Kam	kam	k6eAd1	kam
jdeme	jít	k5eAaImIp1nP	jít
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
Munchův	Munchův	k2eAgMnSc1d1	Munchův
přítel	přítel	k1gMnSc1	přítel
Paul	Paul	k1gMnSc1	Paul
Gauguin	Gauguin	k1gMnSc1	Gauguin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
obrazu	obraz	k1gInSc6	obraz
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
upoutá	upoutat	k5eAaPmIp3nS	upoutat
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svým	svůj	k3xOyFgMnPc3	svůj
umístění	umístění	k1gNnSc4	umístění
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
komponován	komponován	k2eAgInSc1d1	komponován
středově	středově	k6eAd1	středově
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
si	se	k3xPyFc3	se
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
takové	takový	k3xDgFnSc2	takový
kompozice	kompozice	k1gFnSc2	kompozice
autor	autor	k1gMnSc1	autor
zahrával	zahrávat	k5eAaImAgMnS	zahrávat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
napovídá	napovídat	k5eAaBmIp3nS	napovídat
druhá	druhý	k4xOgFnSc1	druhý
strana	strana	k1gFnSc1	strana
obrazu	obraz	k1gInSc2	obraz
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
pokusem	pokus	k1gInSc7	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Posunutím	posunutí	k1gNnSc7	posunutí
poněkud	poněkud	k6eAd1	poněkud
dolů	dolů	k6eAd1	dolů
se	se	k3xPyFc4	se
lebka	lebka	k1gFnSc1	lebka
postavy	postava	k1gFnSc2	postava
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
průsečík	průsečík	k1gInSc4	průsečík
dvou	dva	k4xCgInPc2	dva
podstatných	podstatný	k2eAgInPc2d1	podstatný
kompozičních	kompoziční	k2eAgInPc2d1	kompoziční
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
na	na	k7c6	na
pohybové	pohybový	k2eAgFnSc6d1	pohybová
křivce	křivka	k1gFnSc6	křivka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
celou	celý	k2eAgFnSc7d1	celá
přírodou	příroda	k1gFnSc7	příroda
z	z	k7c2	z
krvavého	krvavý	k2eAgNnSc2d1	krvavé
nebe	nebe	k1gNnSc2	nebe
přes	přes	k7c4	přes
vlny	vlna	k1gFnPc4	vlna
až	až	k6eAd1	až
k	k	k7c3	k
figuře	figura	k1gFnSc3	figura
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
ostré	ostrý	k2eAgFnSc6d1	ostrá
diagonále	diagonála	k1gFnSc6	diagonála
zábradlí	zábradlí	k1gNnSc2	zábradlí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
za	za	k7c7	za
postavou	postava	k1gFnSc7	postava
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
postavy	postava	k1gFnPc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Rozpoložení	rozpoložení	k1gNnPc1	rozpoložení
těchto	tento	k3xDgFnPc2	tento
tří	tři	k4xCgFnPc2	tři
postav	postava	k1gFnPc2	postava
svádí	svádět	k5eAaImIp3nP	svádět
nezasvěcené	zasvěcený	k2eNgMnPc4d1	nezasvěcený
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
chápat	chápat	k5eAaImF	chápat
obraz	obraz	k1gInSc1	obraz
jako	jako	k8xC	jako
výjev	výjev	k1gInSc1	výjev
vystrašené	vystrašený	k2eAgFnSc2d1	vystrašená
utíkající	utíkající	k2eAgFnSc2d1	utíkající
ženy	žena	k1gFnSc2	žena
před	před	k7c7	před
dvěma	dva	k4xCgMnPc7	dva
pronásledovateli	pronásledovatel	k1gMnPc7	pronásledovatel
<g/>
.	.	kIx.	.
<g/>
Dost	dost	k6eAd1	dost
možná	možná	k9	možná
nejproblémovější	problémový	k2eAgFnSc7d3	nejproblémovější
částí	část	k1gFnSc7	část
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
krvavé	krvavý	k2eAgNnSc1d1	krvavé
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
musel	muset	k5eAaImAgInS	muset
Munch	Munch	k1gInSc4	Munch
řešit	řešit	k5eAaImF	řešit
jestli	jestli	k8xS	jestli
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
mraky	mrak	k1gInPc4	mrak
"	"	kIx"	"
<g/>
jako	jako	k8xS	jako
krev	krev	k1gFnSc4	krev
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
krev	krev	k1gFnSc1	krev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
jakési	jakýsi	k3yIgNnSc1	jakýsi
oko	oko	k1gNnSc1	oko
uragánu	uragán	k1gInSc2	uragán
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
patrnější	patrný	k2eAgFnSc7d2	patrnější
na	na	k7c6	na
litografické	litografický	k2eAgFnSc6d1	litografická
verzi	verze	k1gFnSc6	verze
obrazu	obraz	k1gInSc2	obraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Munch	Munch	k1gInSc1	Munch
chápal	chápat	k5eAaImAgInS	chápat
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
"	"	kIx"	"
<g/>
otevřené	otevřený	k2eAgFnPc1d1	otevřená
rány	rána	k1gFnPc1	rána
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
sobě	se	k3xPyFc3	se
nesl	nést	k5eAaImAgInS	nést
už	už	k9	už
od	od	k7c2	od
nizzejských	nizzejský	k2eAgFnPc2d1	nizzejský
dob	doba	k1gFnPc2	doba
hluboké	hluboký	k2eAgFnSc2d1	hluboká
deprese	deprese	k1gFnSc2	deprese
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
svým	svůj	k3xOyFgNnSc7	svůj
tématem	téma	k1gNnSc7	téma
lze	lze	k6eAd1	lze
chápat	chápat	k5eAaImF	chápat
oko	oko	k1gNnSc4	oko
jako	jako	k8xS	jako
mutaci	mutace	k1gFnSc4	mutace
tradičního	tradiční	k2eAgInSc2d1	tradiční
symbolu	symbol	k1gInSc2	symbol
okna	okno	k1gNnSc2	okno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
tvořilo	tvořit	k5eAaImAgNnS	tvořit
tak	tak	k6eAd1	tak
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
Večeru	večer	k1gInSc6	večer
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Karla	Karel	k1gMnSc2	Karel
Johana	Johan	k1gMnSc2	Johan
a	a	k8xC	a
v	v	k7c6	v
Bouři	bouř	k1gFnSc6	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mutace	mutace	k1gFnSc1	mutace
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ikonografie	ikonografie	k1gFnSc2	ikonografie
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
<g/>
Munch	Munch	k1gMnSc1	Munch
považoval	považovat	k5eAaImAgMnS	považovat
obraz	obraz	k1gInSc4	obraz
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
výrazové	výrazový	k2eAgNnSc4d1	výrazové
maximum	maximum	k1gNnSc4	maximum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
malíř	malíř	k1gMnSc1	malíř
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
napětí	napětí	k1gNnSc4	napětí
mnohem	mnohem	k6eAd1	mnohem
lyričtěji	lyricky	k6eAd2	lyricky
–	–	k?	–
tak	tak	k6eAd1	tak
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
další	další	k2eAgInSc4d1	další
obraz	obraz	k1gInSc4	obraz
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
možná	možná	k9	možná
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
text	text	k1gInSc1	text
"	"	kIx"	"
<g/>
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
namalováno	namalovat	k5eAaPmNgNnS	namalovat
jen	jen	k9	jen
šílencem	šílenec	k1gMnSc7	šílenec
<g/>
"	"	kIx"	"
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
Výkřiku	výkřik	k1gInSc2	výkřik
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgNnSc2	který
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ho	on	k3xPp3gMnSc4	on
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
kulturu	kultura	k1gFnSc4	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Výkřik	výkřik	k1gInSc1	výkřik
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
dopadem	dopad	k1gInSc7	dopad
na	na	k7c6	na
popkulturu	popkulturu	k?	popkulturu
rovná	rovnat	k5eAaImIp3nS	rovnat
Moně	Moň	k1gMnSc2	Moň
Lise	Lisa	k1gFnSc3	Lisa
malíře	malíř	k1gMnSc2	malíř
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinciho	Vinci	k1gMnSc2	Vinci
<g/>
,	,	kIx,	,
Whistlerově	Whistlerův	k2eAgFnSc6d1	Whistlerova
matce	matka	k1gFnSc6	matka
Jamese	Jamese	k1gFnSc2	Jamese
McNeila	McNeil	k1gMnSc2	McNeil
Whistlera	Whistler	k1gMnSc2	Whistler
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Americké	americký	k2eAgFnSc6d1	americká
gotice	gotika	k1gFnSc6	gotika
Granta	Grant	k1gMnSc2	Grant
Wooda	Wood	k1gMnSc2	Wood
<g/>
.	.	kIx.	.
<g/>
Už	už	k6eAd1	už
Munchův	Munchův	k2eAgMnSc1d1	Munchův
přítel	přítel	k1gMnSc1	přítel
Stanisław	Stanisław	k1gMnSc1	Stanisław
Przybyszewski	Przybyszewske	k1gFnSc4	Przybyszewske
napsal	napsat	k5eAaBmAgMnS	napsat
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
obrazu	obraz	k1gInSc2	obraz
román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložen	přeložit	k5eAaPmNgInS	přeložit
jako	jako	k9	jako
Křik	křik	k1gInSc1	křik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
Výkřik	výkřik	k1gInSc1	výkřik
pro	pro	k7c4	pro
popkulturu	popkulturu	k?	popkulturu
až	až	k8xS	až
kultovního	kultovní	k2eAgInSc2d1	kultovní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
užit	užít	k5eAaPmNgInS	užít
na	na	k7c6	na
přebalech	přebal	k1gInPc6	přebal
několika	několik	k4yIc2	několik
verzí	verze	k1gFnPc2	verze
knihy	kniha	k1gFnSc2	kniha
Prvotní	prvotní	k2eAgInSc4d1	prvotní
výkřik	výkřik	k1gInSc4	výkřik
Arthura	Arthur	k1gMnSc2	Arthur
Janova	Janov	k1gInSc2	Janov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
popartový	popartový	k2eAgMnSc1d1	popartový
umělec	umělec	k1gMnSc1	umělec
Andy	Anda	k1gFnSc2	Anda
Warhol	Warhol	k1gInSc1	Warhol
sérii	série	k1gFnSc4	série
sítotiskových	sítotiskový	k2eAgFnPc2d1	sítotisková
verzí	verze	k1gFnPc2	verze
Munchových	Munchův	k2eAgInPc2d1	Munchův
obrazů	obraz	k1gInPc2	obraz
včetně	včetně	k7c2	včetně
Výkřiku	výkřik	k1gInSc2	výkřik
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
totiž	totiž	k9	totiž
masovou	masový	k2eAgFnSc7d1	masová
produkcí	produkce	k1gFnSc7	produkce
sejmout	sejmout	k5eAaPmF	sejmout
posvátnou	posvátný	k2eAgFnSc4d1	posvátná
auru	aura	k1gFnSc4	aura
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Masovou	masový	k2eAgFnSc4d1	masová
produkci	produkce	k1gFnSc4	produkce
započal	započnout	k5eAaPmAgInS	započnout
už	už	k6eAd1	už
sám	sám	k3xTgInSc1	sám
Munch	Munch	k1gInSc1	Munch
vytvořením	vytvoření	k1gNnSc7	vytvoření
jednoduše	jednoduše	k6eAd1	jednoduše
kopírovatelné	kopírovatelný	k2eAgFnSc2d1	kopírovatelná
litografické	litografický	k2eAgFnSc2d1	litografická
verze	verze	k1gFnSc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Výkřik	výkřik	k1gInSc4	výkřik
také	také	k9	také
ironizoval	ironizovat	k5eAaImAgMnS	ironizovat
postmodernista	postmodernista	k1gMnSc1	postmodernista
Erró	Erró	k1gMnSc1	Erró
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
akrylových	akrylový	k2eAgFnPc6d1	akrylová
malbách	malba	k1gFnPc6	malba
Druhý	druhý	k4xOgInSc1	druhý
výkřik	výkřik	k1gInSc1	výkřik
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ding	Ding	k1gMnSc1	Ding
Dong	dong	k1gInSc1	dong
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
exemplářů	exemplář	k1gInPc2	exemplář
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
se	se	k3xPyFc4	se
Výkřik	výkřik	k1gInSc1	výkřik
těší	těšit	k5eAaImIp3nS	těšit
neutuchající	utuchající	k2eNgNnSc1d1	neutuchající
široké	široký	k2eAgNnSc1d1	široké
známostí	známost	k1gFnSc7	známost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
reklamách	reklama	k1gFnPc6	reklama
<g/>
,	,	kIx,	,
v	v	k7c6	v
animovaných	animovaný	k2eAgInPc6d1	animovaný
seriálech	seriál	k1gInPc6	seriál
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
filmu	film	k1gInSc6	film
a	a	k8xC	a
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Psychotický	psychotický	k2eAgInSc1d1	psychotický
zabiják	zabiják	k1gInSc1	zabiják
v	v	k7c6	v
kultovním	kultovní	k2eAgInSc6d1	kultovní
hororovém	hororový	k2eAgInSc6d1	hororový
filmu	film	k1gInSc6	film
Wese	Wes	k1gInSc2	Wes
Cravena	Craven	k2eAgFnSc1d1	Cravena
Vřískot	vřískot	k1gInSc1	vřískot
nosí	nosit	k5eAaImIp3nS	nosit
halloweenovou	halloweenový	k2eAgFnSc4d1	halloweenový
masku	maska	k1gFnSc4	maska
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
podobností	podobnost	k1gFnSc7	podobnost
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
rozebíral	rozebírat	k5eAaImAgInS	rozebírat
třeba	třeba	k6eAd1	třeba
Tony	Tony	k1gMnSc5	Tony
Magistrale	Magistral	k1gMnSc5	Magistral
<g/>
.	.	kIx.	.
</s>
<s>
Reprodukce	reprodukce	k1gFnSc1	reprodukce
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
také	také	k9	také
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
spotřebních	spotřební	k2eAgInPc2d1	spotřební
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
pravidelně	pravidelně	k6eAd1	pravidelně
základem	základ	k1gInSc7	základ
parodií	parodie	k1gFnPc2	parodie
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
témat	téma	k1gNnPc2	téma
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
serveru	server	k1gInSc6	server
Vesjolyje	Vesjolyje	k1gFnSc2	Vesjolyje
Kartinki	Kartink	k1gFnSc2	Kartink
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gFnSc2	The
Scream	Scream	k1gInSc1	Scream
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
WITTLICH	WITTLICH	kA	WITTLICH
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Noam	Noam	k1gInSc1	Noam
Galai	Gala	k1gFnSc2	Gala
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Výkřik	výkřik	k1gInSc1	výkřik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
