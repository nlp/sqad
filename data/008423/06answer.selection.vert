<s>
Výkřik	výkřik	k1gInSc1	výkřik
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Křik	křik	k1gInSc1	křik
<g/>
,	,	kIx,	,
norsky	norsky	k6eAd1	norsky
Skrik	Skrik	k1gInSc1	Skrik
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
expresionistický	expresionistický	k2eAgInSc4d1	expresionistický
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
dílo	dílo	k1gNnSc4	dílo
norského	norský	k2eAgMnSc2d1	norský
malíře	malíř	k1gMnSc2	malíř
Edvarda	Edvard	k1gMnSc2	Edvard
Muncha	Munch	k1gMnSc2	Munch
<g/>
.	.	kIx.	.
</s>
