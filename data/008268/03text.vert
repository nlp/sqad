<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
o	o	k7c6	o
prevenci	prevence	k1gFnSc6	prevence
a	a	k8xC	a
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
násilí	násilí	k1gNnSc3	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
a	a	k8xC	a
domácímu	domácí	k2eAgNnSc3d1	domácí
násilí	násilí	k1gNnSc3	násilí
(	(	kIx(	(
<g/>
Istanbulská	istanbulský	k2eAgFnSc1d1	Istanbulská
úmluva	úmluva	k1gFnSc1	úmluva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
smlouva	smlouva	k1gFnSc1	smlouva
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
posuzující	posuzující	k2eAgFnPc4d1	posuzující
především	především	k6eAd1	především
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
násilí	násilí	k1gNnSc2	násilí
vůči	vůči	k7c3	vůči
ženám	žena	k1gFnPc3	žena
jako	jako	k8xS	jako
formu	forma	k1gFnSc4	forma
historicky	historicky	k6eAd1	historicky
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
podmíněné	podmíněný	k2eAgFnPc4d1	podmíněná
diskriminace	diskriminace	k1gFnPc4	diskriminace
<g/>
.	.	kIx.	.
</s>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
prevenci	prevence	k1gFnSc4	prevence
<g/>
,	,	kIx,	,
ochranu	ochrana	k1gFnSc4	ochrana
obětí	oběť	k1gFnPc2	oběť
a	a	k8xC	a
stíhání	stíhání	k1gNnSc1	stíhání
pachatelů	pachatel	k1gMnPc2	pachatel
genderově	genderově	k6eAd1	genderově
podmíněného	podmíněný	k2eAgNnSc2d1	podmíněné
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
také	také	k9	také
nově	nově	k6eAd1	nově
definuje	definovat	k5eAaBmIp3nS	definovat
některé	některý	k3yIgInPc4	některý
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
mrzačení	mrzačení	k1gNnSc1	mrzačení
ženského	ženský	k2eAgInSc2d1	ženský
genitálu	genitál	k1gInSc2	genitál
<g/>
,	,	kIx,	,
vynucený	vynucený	k2eAgInSc1d1	vynucený
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
pronásledování	pronásledování	k1gNnSc1	pronásledování
(	(	kIx(	(
<g/>
stalking	stalking	k1gInSc1	stalking
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vynucený	vynucený	k2eAgInSc1d1	vynucený
potrat	potrat	k1gInSc4	potrat
a	a	k8xC	a
vynucenou	vynucený	k2eAgFnSc4d1	vynucená
sterilizaci	sterilizace	k1gFnSc4	sterilizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
požaduje	požadovat	k5eAaImIp3nS	požadovat
po	po	k7c6	po
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gMnPc4	on
zavedly	zavést	k5eAaPmAgFnP	zavést
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
trestního	trestní	k2eAgInSc2d1	trestní
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
úmluvu	úmluva	k1gFnSc4	úmluva
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
k	k	k7c3	k
březnu	březen	k1gInSc3	březen
2017	[number]	k4	2017
připojilo	připojit	k5eAaPmAgNnS	připojit
dalších	další	k2eAgInPc2d1	další
21	[number]	k4	21
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
platnost	platnost	k1gFnSc1	platnost
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
úmluvy	úmluva	k1gFnSc2	úmluva
==	==	k?	==
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Rada	rada	k1gFnSc1	rada
Evropy	Evropa	k1gFnSc2	Evropa
začala	začít	k5eAaPmAgFnS	začít
zasazovat	zasazovat	k5eAaImF	zasazovat
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
žen	žena	k1gFnPc2	žena
před	před	k7c7	před
genderově	genderově	k6eAd1	genderově
motivovaným	motivovaný	k2eAgNnSc7d1	motivované
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
Výbor	výbor	k1gInSc1	výbor
ministrů	ministr	k1gMnPc2	ministr
vydal	vydat	k5eAaPmAgInS	vydat
několik	několik	k4yIc4	několik
doporučení	doporučení	k1gNnPc2	doporučení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dalo	dát	k5eAaPmAgNnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
celoevropské	celoevropský	k2eAgFnSc6d1	celoevropská
kampani	kampaň	k1gFnSc6	kampaň
za	za	k7c4	za
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
násilí	násilí	k1gNnSc3	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
včetně	včetně	k7c2	včetně
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Zásadně	zásadně	k6eAd1	zásadně
odmítavý	odmítavý	k2eAgInSc4d1	odmítavý
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
násilí	násilí	k1gNnSc3	násilí
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
i	i	k9	i
Evropský	evropský	k2eAgInSc1d1	evropský
parlament	parlament	k1gInSc1	parlament
přijetím	přijetí	k1gNnSc7	přijetí
řady	řada	k1gFnSc2	řada
rezolucí	rezoluce	k1gFnPc2	rezoluce
a	a	k8xC	a
doporučení	doporučení	k1gNnSc1	doporučení
vyzývající	vyzývající	k2eAgNnSc1d1	vyzývající
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
právně	právně	k6eAd1	právně
závazných	závazný	k2eAgFnPc2d1	závazná
norem	norma	k1gFnPc2	norma
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
prevence	prevence	k1gFnSc2	prevence
<g/>
,	,	kIx,	,
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
stíhání	stíhání	k1gNnSc2	stíhání
nejzávažnějších	závažný	k2eAgFnPc2d3	nejzávažnější
a	a	k8xC	a
nejrozšířenějších	rozšířený	k2eAgFnPc2d3	nejrozšířenější
forem	forma	k1gFnPc2	forma
genderově	genderově	k6eAd1	genderově
podmíněného	podmíněný	k2eAgNnSc2d1	podmíněné
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnPc1d1	národní
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
studie	studie	k1gFnPc1	studie
a	a	k8xC	a
průzkumy	průzkum	k1gInPc1	průzkum
následně	následně	k6eAd1	následně
poukázaly	poukázat	k5eAaPmAgInP	poukázat
na	na	k7c4	na
závažnost	závažnost	k1gFnSc4	závažnost
tohoto	tento	k3xDgInSc2	tento
problému	problém	k1gInSc2	problém
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Kampaň	kampaň	k1gFnSc1	kampaň
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
odhalila	odhalit	k5eAaPmAgFnS	odhalit
značně	značně	k6eAd1	značně
rozdílné	rozdílný	k2eAgFnPc4d1	rozdílná
reakce	reakce	k1gFnPc4	reakce
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
na	na	k7c4	na
násilí	násilí	k1gNnSc4	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
a	a	k8xC	a
domácí	domácí	k2eAgNnSc4d1	domácí
násilí	násilí	k1gNnSc4	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
evidentní	evidentní	k2eAgFnSc1d1	evidentní
potřeba	potřeba	k1gFnSc1	potřeba
harmonizace	harmonizace	k1gFnSc2	harmonizace
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
oběti	oběť	k1gFnPc1	oběť
mohly	moct	k5eAaImAgFnP	moct
požívat	požívat	k5eAaImF	požívat
stejné	stejný	k2eAgFnPc4d1	stejná
úrovně	úroveň	k1gFnPc4	úroveň
ochrany	ochrana	k1gFnSc2	ochrana
napříč	napříč	k7c7	napříč
celou	celý	k2eAgFnSc7d1	celá
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
vůle	vůle	k1gFnSc1	vůle
jednat	jednat	k5eAaImF	jednat
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Ministři	ministr	k1gMnPc1	ministr
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
začali	začít	k5eAaPmAgMnP	začít
diskutovat	diskutovat	k5eAaImF	diskutovat
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
posílení	posílení	k1gNnSc1	posílení
ochrany	ochrana	k1gFnSc2	ochrana
obětí	oběť	k1gFnPc2	oběť
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
násilí	násilí	k1gNnSc4	násilí
partnerského	partnerský	k2eAgInSc2d1	partnerský
<g/>
.	.	kIx.	.
<g/>
Vědoma	vědom	k2eAgFnSc1d1	vědoma
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnSc2	svůj
role	role	k1gFnSc2	role
<g/>
,	,	kIx,	,
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
Rada	rada	k1gFnSc1	rada
Evropy	Evropa	k1gFnSc2	Evropa
vytvořit	vytvořit	k5eAaPmF	vytvořit
jednotné	jednotný	k2eAgInPc4d1	jednotný
standardy	standard	k1gInPc4	standard
pro	pro	k7c4	pro
prevenci	prevence	k1gFnSc4	prevence
a	a	k8xC	a
potírání	potírání	k1gNnSc4	potírání
násilí	násilí	k1gNnSc2	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
a	a	k8xC	a
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Výbor	výbor	k1gInSc4	výbor
ministrů	ministr	k1gMnPc2	ministr
zřídil	zřídit	k5eAaPmAgMnS	zřídit
expertní	expertní	k2eAgFnSc4d1	expertní
skupinu	skupina	k1gFnSc4	skupina
pověřenou	pověřený	k2eAgFnSc7d1	pověřená
přípravou	příprava	k1gFnSc7	příprava
návrhu	návrh	k1gInSc2	návrh
úmluvy	úmluva	k1gFnSc2	úmluva
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
CAHVIO	CAHVIO	kA	CAHVIO
-	-	kIx~	-
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
prevenci	prevence	k1gFnSc4	prevence
a	a	k8xC	a
boj	boj	k1gInSc4	boj
s	s	k7c7	s
násilím	násilí	k1gNnSc7	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
a	a	k8xC	a
domácím	domácí	k2eAgNnSc7d1	domácí
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
návrh	návrh	k1gInSc4	návrh
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přijetí	přijetí	k1gNnSc1	přijetí
úmluvy	úmluva	k1gFnSc2	úmluva
==	==	k?	==
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
Výborem	výbor	k1gInSc7	výbor
ministrů	ministr	k1gMnPc2	ministr
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
a	a	k8xC	a
k	k	k7c3	k
podpisům	podpis	k1gInPc3	podpis
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
121	[number]	k4	121
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
Výboru	výbor	k1gInSc2	výbor
ministrů	ministr	k1gMnPc2	ministr
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
platnost	platnost	k1gFnSc1	platnost
úmluva	úmluva	k1gFnSc1	úmluva
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
po	po	k7c6	po
ratifikaci	ratifikace	k1gFnSc6	ratifikace
prvními	první	k4xOgInPc7	první
deseti	deset	k4xCc7	deset
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2018	[number]	k4	2018
ji	on	k3xPp3gFnSc4	on
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
33	[number]	k4	33
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
,	,	kIx,	,
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Luxembursko	Luxembursko	k1gNnSc1	Luxembursko
<g/>
,	,	kIx,	,
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Monako	Monako	k1gNnSc1	Monako
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
12	[number]	k4	12
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
zatím	zatím	k6eAd1	zatím
jen	jen	k6eAd1	jen
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Podpis	podpis	k1gInSc1	podpis
úmluvy	úmluva	k1gFnSc2	úmluva
dosud	dosud	k6eAd1	dosud
odmítají	odmítat	k5eAaImIp3nP	odmítat
pouze	pouze	k6eAd1	pouze
Rusko	Rusko	k1gNnSc4	Rusko
a	a	k8xC	a
Ázerbájdžán	Ázerbájdžán	k1gInSc4	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Proces	proces	k1gInSc1	proces
přijímání	přijímání	k1gNnSc2	přijímání
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
přijetí	přijetí	k1gNnSc4	přijetí
smlouvy	smlouva	k1gFnSc2	smlouva
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zasazovala	zasazovat	k5eAaImAgFnS	zasazovat
Česká	český	k2eAgFnSc1d1	Česká
ženská	ženský	k2eAgFnSc1d1	ženská
lobby	lobby	k1gFnSc1	lobby
společně	společně	k6eAd1	společně
s	s	k7c7	s
členskými	členský	k2eAgFnPc7d1	členská
organizacemi	organizace	k1gFnPc7	organizace
(	(	kIx(	(
<g/>
proFem	proFem	k1gInSc1	proFem
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
Rosa	Rosa	k1gFnSc1	Rosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc1	přijetí
úmluvy	úmluva	k1gFnSc2	úmluva
rovněž	rovněž	k9	rovněž
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
podporoval	podporovat	k5eAaImAgMnS	podporovat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
rovné	rovný	k2eAgFnPc4d1	rovná
příležitosti	příležitost	k1gFnPc4	příležitost
a	a	k8xC	a
legislativu	legislativa	k1gFnSc4	legislativa
Jiří	Jiří	k1gMnSc1	Jiří
Dienstbier	Dienstbier	k1gMnSc1	Dienstbier
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
přijal	přijmout	k5eAaPmAgInS	přijmout
za	za	k7c4	za
vládu	vláda	k1gFnSc4	vláda
petici	petice	k1gFnSc4	petice
organizace	organizace	k1gFnSc2	organizace
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
za	za	k7c4	za
její	její	k3xOp3gNnSc4	její
přijetí	přijetí	k1gNnSc4	přijetí
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
nakonec	nakonec	k6eAd1	nakonec
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
)	)	kIx)	)
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Podpis	podpis	k1gInSc4	podpis
připojil	připojit	k5eAaPmAgMnS	připojit
její	její	k3xOp3gMnSc1	její
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
při	při	k7c6	při
Radě	rada	k1gFnSc6	rada
Evropy	Evropa	k1gFnSc2	Evropa
Emil	Emil	k1gMnSc1	Emil
Ruffer	Ruffer	k1gMnSc1	Ruffer
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
ratifikace	ratifikace	k1gFnSc1	ratifikace
smlouvy	smlouva	k1gFnSc2	smlouva
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cíle	cíl	k1gInPc1	cíl
úmluvy	úmluva	k1gFnSc2	úmluva
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Prevence	prevence	k1gFnSc1	prevence
===	===	k?	===
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
ovlivnění	ovlivnění	k1gNnSc2	ovlivnění
a	a	k8xC	a
změnu	změna	k1gFnSc4	změna
kulturních	kulturní	k2eAgInPc2d1	kulturní
vzorců	vzorec	k1gInPc2	vzorec
chování	chování	k1gNnSc2	chování
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
namířena	namířen	k2eAgFnSc1d1	namířena
proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
zvykům	zvyk	k1gInPc3	zvyk
a	a	k8xC	a
tradicím	tradice	k1gFnPc3	tradice
podporujících	podporující	k2eAgFnPc2d1	podporující
vnímání	vnímání	k1gNnPc2	vnímání
ženy	žena	k1gFnSc2	žena
jako	jako	k8xC	jako
méněcenné	méněcenný	k2eAgFnSc2d1	méněcenná
bytosti	bytost	k1gFnSc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Bojuje	bojovat	k5eAaImIp3nS	bojovat
proti	proti	k7c3	proti
stereotypnímu	stereotypní	k2eAgNnSc3d1	stereotypní
vnímání	vnímání	k1gNnSc3	vnímání
ženských	ženský	k2eAgFnPc2d1	ženská
a	a	k8xC	a
mužských	mužský	k2eAgFnPc2d1	mužská
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
toho	ten	k3xDgMnSc4	ten
chce	chtít	k5eAaImIp3nS	chtít
pomocí	pomocí	k7c2	pomocí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
školení	školení	k1gNnSc1	školení
odborníků	odborník	k1gMnPc2	odborník
pracujících	pracující	k1gMnPc2	pracující
s	s	k7c7	s
oběťmi	oběť	k1gFnPc7	oběť
</s>
</p>
<p>
<s>
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
osvětových	osvětový	k2eAgFnPc2d1	osvětová
kampaní	kampaň	k1gFnPc2	kampaň
</s>
</p>
<p>
<s>
zahrnutí	zahrnutí	k1gNnSc1	zahrnutí
téma	téma	k1gNnSc1	téma
genderové	genderová	k1gFnSc2	genderová
problematiky	problematika	k1gFnSc2	problematika
do	do	k7c2	do
školních	školní	k2eAgFnPc2d1	školní
osnov	osnova	k1gFnPc2	osnova
</s>
</p>
<p>
<s>
zajištění	zajištění	k1gNnSc1	zajištění
terapeutických	terapeutický	k2eAgInPc2d1	terapeutický
programů	program	k1gInPc2	program
pro	pro	k7c4	pro
pachatele	pachatel	k1gMnSc4	pachatel
domácího	domácí	k2eAgMnSc4d1	domácí
a	a	k8xC	a
sexuálně	sexuálně	k6eAd1	sexuálně
orientovaného	orientovaný	k2eAgNnSc2d1	orientované
násilí	násilí	k1gNnSc2	násilí
</s>
</p>
<p>
<s>
těsné	těsný	k2eAgFnSc2d1	těsná
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
nevládními	vládní	k2eNgFnPc7d1	nevládní
neziskovými	ziskový	k2eNgFnPc7d1	nezisková
organizacemi	organizace	k1gFnPc7	organizace
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
selhání	selhání	k1gNnSc2	selhání
preventivních	preventivní	k2eAgNnPc2d1	preventivní
opatření	opatření	k1gNnPc2	opatření
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc4d1	nezbytný
podniknout	podniknout	k5eAaPmF	podniknout
veškerá	veškerý	k3xTgNnPc4	veškerý
opatření	opatření	k1gNnPc4	opatření
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
obětí	oběť	k1gFnPc2	oběť
před	před	k7c7	před
násilím	násilí	k1gNnSc7	násilí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
pachatele	pachatel	k1gMnSc2	pachatel
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
zejména	zejména	k9	zejména
zavedením	zavedení	k1gNnSc7	zavedení
legální	legální	k2eAgFnSc2d1	legální
možnosti	možnost	k1gFnSc2	možnost
dočasného	dočasný	k2eAgNnSc2d1	dočasné
vykázání	vykázání	k1gNnSc2	vykázání
pachatele	pachatel	k1gMnSc2	pachatel
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
ze	z	k7c2	z
společného	společný	k2eAgNnSc2d1	společné
obydlí	obydlí	k1gNnSc2	obydlí
<g/>
,	,	kIx,	,
zavedením	zavedení	k1gNnSc7	zavedení
nepřetržitých	přetržitý	k2eNgFnPc2d1	nepřetržitá
tísňových	tísňový	k2eAgFnPc2d1	tísňová
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
dostatečným	dostatečný	k2eAgInSc7d1	dostatečný
počtem	počet	k1gInSc7	počet
dostupných	dostupný	k2eAgInPc2d1	dostupný
prozatímních	prozatímní	k2eAgInPc2d1	prozatímní
útulků	útulek	k1gInPc2	útulek
<g/>
,	,	kIx,	,
poskytováním	poskytování	k1gNnSc7	poskytování
potřebných	potřebný	k2eAgFnPc2d1	potřebná
srozumitelných	srozumitelný	k2eAgFnPc2d1	srozumitelná
informací	informace	k1gFnPc2	informace
pro	pro	k7c4	pro
traumatizované	traumatizovaný	k2eAgFnPc4d1	traumatizovaná
oběti	oběť	k1gFnPc4	oběť
<g/>
,	,	kIx,	,
zřízením	zřízení	k1gNnSc7	zřízení
specializovaných	specializovaný	k2eAgNnPc2d1	specializované
center	centrum	k1gNnPc2	centrum
pro	pro	k7c4	pro
oběti	oběť	k1gFnPc4	oběť
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
dostatečným	dostatečný	k2eAgNnSc7d1	dostatečné
informováním	informování	k1gNnSc7	informování
obětí	oběť	k1gFnPc2	oběť
o	o	k7c6	o
jejich	jejich	k3xOp3gNnPc6	jejich
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
dostupné	dostupný	k2eAgNnSc1d1	dostupné
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stíhání	stíhání	k1gNnSc2	stíhání
===	===	k?	===
</s>
</p>
<p>
<s>
Strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
povinny	povinen	k2eAgInPc1d1	povinen
definovat	definovat	k5eAaBmF	definovat
nové	nový	k2eAgInPc4d1	nový
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
trestní	trestní	k2eAgInSc1d1	trestní
řád	řád	k1gInSc1	řád
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
fyzické	fyzický	k2eAgNnSc1d1	fyzické
týrání	týrání	k1gNnSc1	týrání
<g/>
,	,	kIx,	,
psychické	psychický	k2eAgNnSc1d1	psychické
týrání	týrání	k1gNnSc1	týrání
<g/>
,	,	kIx,	,
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
pronásledování	pronásledování	k1gNnSc4	pronásledování
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgNnSc4d1	sexuální
násilí	násilí	k1gNnSc4	násilí
a	a	k8xC	a
znásilnění	znásilnění	k1gNnSc4	znásilnění
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgNnSc4d1	sexuální
obtěžování	obtěžování	k1gNnSc4	obtěžování
<g/>
,	,	kIx,	,
vynucený	vynucený	k2eAgInSc1d1	vynucený
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
mrzačení	mrzačení	k1gNnSc1	mrzačení
ženského	ženský	k2eAgInSc2d1	ženský
genitálu	genitál	k1gInSc2	genitál
<g/>
,	,	kIx,	,
vynucený	vynucený	k2eAgInSc1d1	vynucený
potrat	potrat	k1gInSc1	potrat
a	a	k8xC	a
nucená	nucený	k2eAgFnSc1d1	nucená
sterilizace	sterilizace	k1gFnSc1	sterilizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
mají	mít	k5eAaImIp3nP	mít
státy	stát	k1gInPc1	stát
povinnost	povinnost	k1gFnSc4	povinnost
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kulturní	kulturní	k2eAgFnPc1d1	kulturní
zvyklosti	zvyklost	k1gFnPc1	zvyklost
a	a	k8xC	a
obyčeje	obyčej	k1gInPc1	obyčej
nesloužily	sloužit	k5eNaImAgInP	sloužit
jako	jako	k8xC	jako
záminka	záminka	k1gFnSc1	záminka
k	k	k7c3	k
ospravedlňování	ospravedlňování	k1gNnSc3	ospravedlňování
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
chování	chování	k1gNnSc2	chování
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
tak	tak	k6eAd1	tak
zvaných	zvaný	k2eAgInPc2d1	zvaný
zločinů	zločin	k1gInPc2	zločin
ze	z	k7c2	z
cti	čest	k1gFnSc2	čest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mají	mít	k5eAaImIp3nP	mít
povinnost	povinnost	k1gFnSc4	povinnost
zajistit	zajistit	k5eAaPmF	zajistit
řádné	řádný	k2eAgNnSc4d1	řádné
vyšetření	vyšetření	k1gNnSc4	vyšetření
všech	všecek	k3xTgInPc2	všecek
projevů	projev	k1gInPc2	projev
násilí	násilí	k1gNnSc2	násilí
vůči	vůči	k7c3	vůči
ženám	žena	k1gFnPc3	žena
a	a	k8xC	a
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
myšlence	myšlenka	k1gFnSc6	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádná	žádný	k3yNgFnSc1	žádný
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
úřad	úřad	k1gInSc1	úřad
či	či	k8xC	či
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
nemůže	moct	k5eNaImIp3nS	moct
zvítězit	zvítězit	k5eAaPmF	zvítězit
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
násilí	násilí	k1gNnSc3	násilí
páchaném	páchaný	k2eAgInSc6d1	páchaný
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
osamoceně	osamoceně	k6eAd1	osamoceně
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
motivuje	motivovat	k5eAaBmIp3nS	motivovat
ke	k	k7c3	k
koordinovanému	koordinovaný	k2eAgNnSc3d1	koordinované
a	a	k8xC	a
promyšlenému	promyšlený	k2eAgNnSc3d1	promyšlené
úsilí	úsilí	k1gNnSc3	úsilí
všech	všecek	k3xTgInPc2	všecek
orgánů	orgán	k1gInPc2	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
i	i	k8xC	i
nevládních	vládní	k2eNgFnPc2d1	nevládní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyslat	vyslat	k5eAaPmF	vyslat
společnosti	společnost	k1gFnPc4	společnost
jasný	jasný	k2eAgInSc4d1	jasný
signál	signál	k1gInSc4	signál
jako	jako	k8xS	jako
celku	celek	k1gInSc3	celek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
násilí	násilí	k1gNnSc1	násilí
není	být	k5eNaImIp3nS	být
adekvátním	adekvátní	k2eAgNnSc7d1	adekvátní
řešením	řešení	k1gNnSc7	řešení
žádného	žádný	k3yNgInSc2	žádný
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
musí	muset	k5eAaImIp3nS	muset
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
násilí	násilí	k1gNnSc4	násilí
proti	proti	k7c3	proti
ženám	žena	k1gFnPc3	žena
a	a	k8xC	a
domácí	domácí	k2eAgNnSc1d1	domácí
násilí	násilí	k1gNnSc1	násilí
nebude	být	k5eNaImBp3nS	být
nikdy	nikdy	k6eAd1	nikdy
tolerováno	tolerován	k2eAgNnSc1d1	tolerováno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Monitorovací	monitorovací	k2eAgInSc1d1	monitorovací
mechanismus	mechanismus	k1gInSc1	mechanismus
===	===	k?	===
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
má	mít	k5eAaImIp3nS	mít
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
vlastní	vlastní	k2eAgInSc4d1	vlastní
monitorovací	monitorovací	k2eAgInSc4d1	monitorovací
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
podpora	podpora	k1gFnSc1	podpora
zavádění	zavádění	k1gNnSc2	zavádění
smlouvy	smlouva	k1gFnSc2	smlouva
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
pilířích	pilíř	k1gInPc6	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
stojí	stát	k5eAaImIp3nS	stát
patnáctičlenná	patnáctičlenný	k2eAgFnSc1d1	patnáctičlenná
skupina	skupina	k1gFnSc1	skupina
expertů	expert	k1gMnPc2	expert
proti	proti	k7c3	proti
násilí	násilí	k1gNnSc3	násilí
vůči	vůči	k7c3	vůči
ženám	žena	k1gFnPc3	žena
a	a	k8xC	a
domácímu	domácí	k2eAgNnSc3d1	domácí
násilí	násilí	k1gNnSc3	násilí
(	(	kIx(	(
<g/>
GREVIO	GREVIO	kA	GREVIO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastupujících	zastupující	k2eAgFnPc2d1	zastupující
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
regiony	region	k1gInPc4	region
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1	aktuální
poměr	poměr	k1gInSc1	poměr
zastoupení	zastoupení	k1gNnSc2	zastoupení
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
GREVIO	GREVIO	kA	GREVIO
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
případný	případný	k2eAgInSc1d1	případný
odborný	odborný	k2eAgInSc1d1	odborný
doprovod	doprovod	k1gInSc1	doprovod
je	být	k5eAaImIp3nS	být
vybaven	vybaven	k2eAgInSc1d1	vybaven
výjimkami	výjimka	k1gFnPc7	výjimka
z	z	k7c2	z
omezení	omezení	k1gNnSc2	omezení
svobody	svoboda	k1gFnSc2	svoboda
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
celních	celní	k2eAgFnPc2d1	celní
a	a	k8xC	a
devizových	devizový	k2eAgFnPc2d1	devizová
kontrol	kontrola	k1gFnPc2	kontrola
i	i	k8xC	i
doživotní	doživotní	k2eAgFnSc7d1	doživotní
imunitou	imunita	k1gFnSc7	imunita
s	s	k7c7	s
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
jejich	jejich	k3xOp3gFnSc2	jejich
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
omezena	omezit	k5eAaPmNgFnS	omezit
Generálním	generální	k2eAgInSc7d1	generální
tajemníkem	tajemník	k1gInSc7	tajemník
rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
je	být	k5eAaImIp3nS	být
Výbor	výbor	k1gInSc1	výbor
smluvních	smluvní	k2eAgInPc2d1	smluvní
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
the	the	k?	the
Committee	Committee	k1gInSc1	Committee
of	of	k?	of
the	the	k?	the
Parties	Parties	k1gInSc1	Parties
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
sestaven	sestavit	k5eAaPmNgInS	sestavit
z	z	k7c2	z
politických	politický	k2eAgMnPc2d1	politický
zástupců	zástupce	k1gMnPc2	zástupce
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
smluvních	smluvní	k2eAgFnPc2d1	smluvní
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Společenská	společenský	k2eAgFnSc1d1	společenská
diskuse	diskuse	k1gFnSc1	diskuse
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Podpora	podpora	k1gFnSc1	podpora
Úmluvě	úmluva	k1gFnSc3	úmluva
od	od	k7c2	od
České	český	k2eAgFnSc2d1	Česká
ženské	ženský	k2eAgFnSc2d1	ženská
lobby	lobby	k1gFnSc2	lobby
a	a	k8xC	a
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gFnPc2	International
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
ženská	ženská	k1gFnSc1	ženská
lobby	lobby	k1gFnSc7	lobby
ratifikaci	ratifikace	k1gFnSc4	ratifikace
Instanbulské	Instanbulský	k2eAgFnSc2d1	Instanbulský
úmluvy	úmluva	k1gFnSc2	úmluva
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
podporuje	podporovat	k5eAaImIp3nS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
její	její	k3xOp3gFnSc2	její
ředitelky	ředitelka	k1gFnSc2	ředitelka
Hany	Hana	k1gFnSc2	Hana
Stelzerové	Stelzerová	k1gFnSc2	Stelzerová
"	"	kIx"	"
<g/>
Úmluva	úmluva	k1gFnSc1	úmluva
popisuje	popisovat	k5eAaImIp3nS	popisovat
nástroje	nástroj	k1gInPc4	nástroj
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
dobré	dobrý	k2eAgFnSc2d1	dobrá
praxe	praxe	k1gFnSc2	praxe
odborníků	odborník	k1gMnPc2	odborník
a	a	k8xC	a
odbornic	odbornice	k1gFnPc2	odbornice
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
problematiku	problematika	k1gFnSc4	problematika
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
komplexnímu	komplexní	k2eAgInSc3d1	komplexní
přístupu	přístup	k1gInSc3	přístup
žádná	žádný	k3yNgFnSc1	žádný
oběť	oběť	k1gFnSc4	oběť
nezůstane	zůstat	k5eNaPmIp3nS	zůstat
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Dle	dle	k7c2	dle
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Šaldové	Šaldová	k1gFnSc2	Šaldová
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
advokačního	advokační	k2eAgNnSc2d1	advokační
oddělení	oddělení	k1gNnSc2	oddělení
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
ČR	ČR	kA	ČR
z	z	k7c2	z
výzkumu	výzkum	k1gInSc2	výzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
zadaného	zadaný	k2eAgInSc2d1	zadaný
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gMnPc2	International
a	a	k8xC	a
Českou	český	k2eAgFnSc7d1	Česká
ženskou	ženský	k2eAgFnSc7d1	ženská
lobby	lobby	k1gFnSc7	lobby
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
přes	přes	k7c4	přes
70	[number]	k4	70
<g/>
%	%	kIx~	%
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
současná	současný	k2eAgFnSc1d1	současná
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
násilí	násilí	k1gNnSc2	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
silněji	silně	k6eAd2	silně
vnímají	vnímat	k5eAaImIp3nP	vnímat
lidé	člověk	k1gMnPc1	člověk
potřebu	potřeba	k1gFnSc4	potřeba
zesílení	zesílení	k1gNnSc4	zesílení
prevence	prevence	k1gFnSc2	prevence
-	-	kIx~	-
po	po	k7c6	po
větším	veliký	k2eAgNnSc6d2	veliký
zaměření	zaměření	k1gNnSc6	zaměření
ČR	ČR	kA	ČR
na	na	k7c6	na
prevenci	prevence	k1gFnSc6	prevence
násilí	násilí	k1gNnSc2	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
volá	volat	k5eAaImIp3nS	volat
84	[number]	k4	84
<g/>
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
dotázaných	dotázaný	k2eAgMnPc2d1	dotázaný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
lepší	dobrý	k2eAgInSc4d2	lepší
právní	právní	k2eAgInSc4d1	právní
rámec	rámec	k1gInSc4	rámec
si	se	k3xPyFc3	se
přeje	přát	k5eAaImIp3nS	přát
i	i	k9	i
většina	většina	k1gFnSc1	většina
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Češek	Češka	k1gFnPc2	Češka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Odmítavé	odmítavý	k2eAgFnPc4d1	odmítavá
reakce	reakce	k1gFnPc4	reakce
některých	některý	k3yIgMnPc2	některý
politiků	politik	k1gMnPc2	politik
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
politiků	politik	k1gMnPc2	politik
nesouhlasný	souhlasný	k2eNgInSc4d1	nesouhlasný
postoj	postoj	k1gInSc4	postoj
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
například	například	k6eAd1	například
ministři	ministr	k1gMnPc1	ministr
vlády	vláda	k1gFnSc2	vláda
za	za	k7c2	za
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
Pavel	Pavel	k1gMnSc1	Pavel
Bělobrádek	Bělobrádek	k1gInSc1	Bělobrádek
a	a	k8xC	a
Marian	Marian	k1gMnSc1	Marian
Jurečka	Jurečka	k1gMnSc1	Jurečka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
zákon	zákon	k1gInSc1	zákon
pro	pro	k7c4	pro
naši	náš	k3xOp1gFnSc4	náš
republiku	republika	k1gFnSc4	republika
nepřináší	přinášet	k5eNaImIp3nS	přinášet
nic	nic	k3yNnSc1	nic
nového	nový	k2eAgNnSc2d1	nové
mimo	mimo	k7c4	mimo
zavedení	zavedení	k1gNnSc4	zavedení
prvků	prvek	k1gInPc2	prvek
tzv.	tzv.	kA	tzv.
genderové	genderová	k1gFnSc2	genderová
ideologie	ideologie	k1gFnSc2	ideologie
do	do	k7c2	do
školní	školní	k2eAgFnSc2d1	školní
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
snahy	snaha	k1gFnPc1	snaha
vnímají	vnímat	k5eAaImIp3nP	vnímat
spíše	spíše	k9	spíše
jako	jako	k9	jako
útok	útok	k1gInSc4	útok
na	na	k7c4	na
tradiční	tradiční	k2eAgFnSc4d1	tradiční
podobu	podoba	k1gFnSc4	podoba
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
definování	definování	k1gNnSc2	definování
třetího	třetí	k4xOgNnSc2	třetí
pohlaví	pohlaví	k1gNnSc2	pohlaví
a	a	k8xC	a
projev	projev	k1gInSc1	projev
sociálního	sociální	k2eAgNnSc2d1	sociální
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Pavla	Pavel	k1gMnSc2	Pavel
Bělobrádka	Bělobrádka	k1gFnSc1	Bělobrádka
již	již	k9	již
zákony	zákon	k1gInPc4	zákon
proti	proti	k7c3	proti
domácímu	domácí	k2eAgNnSc3d1	domácí
násilí	násilí	k1gNnSc3	násilí
máme	mít	k5eAaImIp1nP	mít
<g/>
,	,	kIx,	,
jen	jen	k9	jen
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
využívány	využívat	k5eAaImNgInP	využívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Námitky	námitka	k1gFnPc4	námitka
odborné	odborný	k2eAgFnSc2d1	odborná
veřejnosti	veřejnost	k1gFnSc2	veřejnost
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
advokacie	advokacie	k1gFnSc2	advokacie
===	===	k?	===
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
přijetí	přijetí	k1gNnSc3	přijetí
Istanbulské	istanbulský	k2eAgFnSc2d1	Istanbulská
úmluvy	úmluva	k1gFnSc2	úmluva
se	se	k3xPyFc4	se
postavily	postavit	k5eAaPmAgFnP	postavit
Unie	unie	k1gFnPc1	unie
rodinných	rodinný	k2eAgMnPc2d1	rodinný
advokátů	advokát	k1gMnPc2	advokát
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
advokátní	advokátní	k2eAgFnSc1d1	advokátní
komora	komora	k1gFnSc1	komora
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
největší	veliký	k2eAgNnSc4d3	veliký
riziko	riziko	k1gNnSc4	riziko
pro	pro	k7c4	pro
advokacii	advokacie	k1gFnSc4	advokacie
je	být	k5eAaImIp3nS	být
vnímán	vnímán	k2eAgInSc1d1	vnímán
článek	článek	k1gInSc1	článek
28	[number]	k4	28
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
"	"	kIx"	"
<g/>
strany	strana	k1gFnPc1	strana
učiní	učinit	k5eAaPmIp3nP	učinit
nezbytná	zbytný	k2eNgNnPc4d1	nezbytné
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pravidla	pravidlo	k1gNnPc1	pravidlo
důvěrnosti	důvěrnost	k1gFnSc2	důvěrnost
vyplývající	vyplývající	k2eAgInSc4d1	vyplývající
ze	z	k7c2	z
zákonů	zákon	k1gInPc2	zákon
dané	daný	k2eAgFnSc2d1	daná
země	zem	k1gFnSc2	zem
nebránila	bránit	k5eNaImAgFnS	bránit
pracovníkům	pracovník	k1gMnPc3	pracovník
určitých	určitý	k2eAgFnPc2d1	určitá
profesí	profes	k1gFnPc2	profes
<g/>
...	...	k?	...
<g />
.	.	kIx.	.
</s>
<s>
ohlásit	ohlásit	k5eAaPmF	ohlásit
kompetentním	kompetentní	k2eAgFnPc3d1	kompetentní
organizacím	organizace	k1gFnPc3	organizace
či	či	k8xC	či
úřadům	úřad	k1gInPc3	úřad
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
důvodné	důvodný	k2eAgNnSc4d1	důvodné
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spáchání	spáchání	k1gNnSc3	spáchání
závažného	závažný	k2eAgInSc2d1	závažný
násilného	násilný	k2eAgInSc2d1	násilný
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
spadajícího	spadající	k2eAgInSc2d1	spadající
do	do	k7c2	do
působnosti	působnost	k1gFnSc2	působnost
této	tento	k3xDgFnSc2	tento
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
,	,	kIx,	,
a	a	k8xC	a
lze	lze	k6eAd1	lze
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
spáchány	spáchat	k5eAaPmNgInP	spáchat
další	další	k2eAgInPc1d1	další
závažné	závažný	k2eAgInPc1d1	závažný
násilné	násilný	k2eAgInPc1d1	násilný
činy	čin	k1gInPc1	čin
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
České	český	k2eAgFnSc2d1	Česká
advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
proto	proto	k8xC	proto
hrozí	hrozit	k5eAaImIp3nS	hrozit
prolomení	prolomení	k1gNnSc1	prolomení
advokátní	advokátní	k2eAgFnSc2d1	advokátní
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnPc1d1	další
rizika	riziko	k1gNnPc1	riziko
skýtají	skýtat	k5eAaImIp3nP	skýtat
podle	podle	k7c2	podle
advokátů	advokát	k1gMnPc2	advokát
tyto	tento	k3xDgInPc4	tento
články	článek	k1gInPc4	článek
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
bývalé	bývalý	k2eAgFnSc2d1	bývalá
ministryně	ministryně	k1gFnSc2	ministryně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
JUDr.	JUDr.	kA	JUDr.
Daniely	Daniela	k1gFnSc2	Daniela
Kovářové	Kovářová	k1gFnSc2	Kovářová
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Čl	čl	kA	čl
<g/>
.	.	kIx.	.
35	[number]	k4	35
-	-	kIx~	-
"	"	kIx"	"
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
kriminalizací	kriminalizace	k1gFnSc7	kriminalizace
všech	všecek	k3xTgFnPc2	všecek
forem	forma	k1gFnPc2	forma
fyzického	fyzický	k2eAgNnSc2d1	fyzické
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
výchovného	výchovný	k2eAgInSc2d1	výchovný
pohlavku	pohlavek	k1gInSc2	pohlavek
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
rodič	rodič	k1gMnSc1	rodič
trestá	trestat	k5eAaImIp3nS	trestat
dítě	dítě	k1gNnSc4	dítě
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čl	čl	kA	čl
<g/>
.	.	kIx.	.
36	[number]	k4	36
-	-	kIx~	-
"	"	kIx"	"
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
přenesením	přenesení	k1gNnSc7	přenesení
důkazního	důkazní	k2eAgNnSc2d1	důkazní
břemene	břemeno	k1gNnSc2	břemeno
u	u	k7c2	u
sexuálního	sexuální	k2eAgInSc2d1	sexuální
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
ze	z	k7c2	z
státu	stát	k1gInSc2	stát
na	na	k7c4	na
obviněného	obviněný	k1gMnSc4	obviněný
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
prokazovat	prokazovat	k5eAaImF	prokazovat
výslovný	výslovný	k2eAgInSc1d1	výslovný
souhlas	souhlas	k1gInSc1	souhlas
poskytnutý	poskytnutý	k2eAgInSc1d1	poskytnutý
poškozeným	poškozený	k1gMnSc7	poškozený
<g/>
/	/	kIx~	/
<g/>
nou	nou	k?	nou
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čl	čl	kA	čl
<g/>
.	.	kIx.	.
40	[number]	k4	40
-	-	kIx~	-
"	"	kIx"	"
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
novým	nový	k2eAgInSc7d1	nový
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
verbálního	verbální	k2eAgNnSc2d1	verbální
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
obtěžování	obtěžování	k1gNnSc2	obtěžování
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čl	čl	kA	čl
<g/>
.	.	kIx.	.
46	[number]	k4	46
-	-	kIx~	-
"	"	kIx"	"
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
zavedením	zavedení	k1gNnSc7	zavedení
nové	nový	k2eAgFnSc2d1	nová
přitěžující	přitěžující	k2eAgFnSc2d1	přitěžující
okolnosti	okolnost	k1gFnSc2	okolnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
poškozeným	poškozený	k1gMnSc7	poškozený
bývalý	bývalý	k2eAgMnSc1d1	bývalý
manžel	manžel	k1gMnSc1	manžel
pachatele	pachatel	k1gMnSc2	pachatel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čl	čl	kA	čl
<g/>
.	.	kIx.	.
48	[number]	k4	48
-	-	kIx~	-
"	"	kIx"	"
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
vyloučením	vyloučení	k1gNnSc7	vyloučení
mediace	mediace	k1gFnSc2	mediace
a	a	k8xC	a
alternativního	alternativní	k2eAgNnSc2d1	alternativní
řešení	řešení	k1gNnSc2	řešení
sporu	spor	k1gInSc2	spor
u	u	k7c2	u
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
spojeného	spojený	k2eAgInSc2d1	spojený
s	s	k7c7	s
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
rozšířit	rozšířit	k5eAaPmF	rozšířit
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
vině	vina	k1gFnSc6	vina
a	a	k8xC	a
trestu	trest	k1gInSc6	trest
i	i	k8xC	i
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
skutky	skutek	k1gInPc4	skutek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čl	čl	kA	čl
<g/>
.	.	kIx.	.
54	[number]	k4	54
-	-	kIx~	-
"	"	kIx"	"
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
zavedením	zavedení	k1gNnSc7	zavedení
nové	nový	k2eAgFnSc2d1	nová
kategorie	kategorie	k1gFnSc2	kategorie
důkazů	důkaz	k1gInPc2	důkaz
relevantních	relevantní	k2eAgInPc2d1	relevantní
a	a	k8xC	a
nezbytných	nezbytný	k2eAgInPc2d1	nezbytný
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
uplatnění	uplatnění	k1gNnSc1	uplatnění
jiných	jiný	k2eAgInPc2d1	jiný
důkazů	důkaz	k1gInPc2	důkaz
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
o	o	k7c6	o
sexuálním	sexuální	k2eAgInSc6d1	sexuální
či	či	k8xC	či
domácím	domácí	k2eAgInSc6d1	domácí
násilí	násilí	k1gNnSc2	násilí
omezeno	omezen	k2eAgNnSc1d1	omezeno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čl	čl	kA	čl
<g/>
.	.	kIx.	.
58	[number]	k4	58
-	-	kIx~	-
"	"	kIx"	"
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
zavedením	zavedení	k1gNnSc7	zavedení
nové	nový	k2eAgFnSc2d1	nová
kategorie	kategorie	k1gFnSc2	kategorie
důkazů	důkaz	k1gInPc2	důkaz
relevantních	relevantní	k2eAgInPc2d1	relevantní
a	a	k8xC	a
nezbytných	zbytný	k2eNgInPc2d1	zbytný
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
uplatnění	uplatnění	k1gNnSc1	uplatnění
jiných	jiný	k2eAgInPc2d1	jiný
důkazů	důkaz	k1gInPc2	důkaz
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
o	o	k7c6	o
sexuálním	sexuální	k2eAgInSc6d1	sexuální
či	či	k8xC	či
domácím	domácí	k2eAgInSc6d1	domácí
násilí	násilí	k1gNnSc2	násilí
omezeno	omezen	k2eAgNnSc1d1	omezeno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
JUDr.	JUDr.	kA	JUDr.
Bc.	Bc.	k1gFnPc1	Bc.
Daniel	Daniel	k1gMnSc1	Daniel
Bartoň	Bartoň	k1gMnSc1	Bartoň
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
LL	LL	kA	LL
<g/>
.	.	kIx.	.
<g/>
M.	M.	kA	M.
(	(	kIx(	(
<g/>
advokát	advokát	k1gMnSc1	advokát
a	a	k8xC	a
přednášející	přednášející	k1gMnSc1	přednášející
na	na	k7c6	na
Evangelické	evangelický	k2eAgFnSc6d1	evangelická
teologické	teologický	k2eAgFnSc6d1	teologická
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
<g/>
)	)	kIx)	)
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
analýze	analýza	k1gFnSc6	analýza
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
Istanbulská	istanbulský	k2eAgFnSc1d1	Istanbulská
úmluva	úmluva	k1gFnSc1	úmluva
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
standardní	standardní	k2eAgFnSc7d1	standardní
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
lidskoprávní	lidskoprávní	k2eAgFnSc7d1	lidskoprávní
úmluvou	úmluva	k1gFnSc7	úmluva
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
při	při	k7c6	při
zohlednění	zohlednění	k1gNnSc6	zohlednění
běžných	běžný	k2eAgNnPc2d1	běžné
pravidel	pravidlo	k1gNnPc2	pravidlo
právní	právní	k2eAgFnSc1d1	právní
interpretace	interpretace	k1gFnSc1	interpretace
nijak	nijak	k6eAd1	nijak
zásadně	zásadně	k6eAd1	zásadně
nevybočuje	vybočovat	k5eNaImIp3nS	vybočovat
ze	z	k7c2	z
současného	současný	k2eAgInSc2d1	současný
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
lidskoprávního	lidskoprávní	k2eAgInSc2d1	lidskoprávní
diskurzu	diskurz	k1gInSc2	diskurz
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
K	k	k7c3	k
problematice	problematika	k1gFnSc3	problematika
prolomení	prolomení	k1gNnSc2	prolomení
advokátní	advokátní	k2eAgFnSc2d1	advokátní
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
článek	článek	k1gInSc1	článek
28	[number]	k4	28
Istanbulské	istanbulský	k2eAgFnPc4d1	Istanbulská
úmluvy	úmluva	k1gFnPc4	úmluva
není	být	k5eNaImIp3nS	být
o	o	k7c6	o
,	,	kIx,	,
<g/>
ohlašovací	ohlašovací	k2eAgFnSc2d1	ohlašovací
povinnosti	povinnost	k1gFnSc2	povinnost
odborníků	odborník	k1gMnPc2	odborník
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jevit	jevit	k5eAaImF	jevit
z	z	k7c2	z
nevhodného	vhodný	k2eNgInSc2d1	nevhodný
českého	český	k2eAgInSc2d1	český
překladu	překlad	k1gInSc2	překlad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
takovém	takový	k3xDgInSc6	takový
nastavení	nastavení	k1gNnSc6	nastavení
vnitrostátních	vnitrostátní	k2eAgNnPc2d1	vnitrostátní
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
některým	některý	k3yIgMnPc3	některý
profesionálům	profesionál	k1gMnPc3	profesionál
(	(	kIx(	(
<g/>
neadvokátům	neadvokát	k1gMnPc3	neadvokát
<g/>
)	)	kIx)	)
překazit	překazit	k5eAaPmF	překazit
páchání	páchání	k1gNnSc4	páchání
závažných	závažný	k2eAgInPc2d1	závažný
násilných	násilný	k2eAgInPc2d1	násilný
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ustanovení	ustanovení	k1gNnSc1	ustanovení
míří	mířit	k5eAaImIp3nS	mířit
na	na	k7c4	na
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
takové	takový	k3xDgNnSc4	takový
nastavení	nastavení	k1gNnSc4	nastavení
profesní	profesní	k2eAgFnSc2d1	profesní
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
překažení	překažení	k1gNnSc1	překažení
spáchání	spáchání	k1gNnSc3	spáchání
takových	takový	k3xDgInPc2	takový
činů	čin	k1gInPc2	čin
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
není	být	k5eNaImIp3nS	být
případ	případ	k1gInSc1	případ
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
K	k	k7c3	k
mediaci	mediace	k1gFnSc3	mediace
a	a	k8xC	a
článku	článek	k1gInSc3	článek
48	[number]	k4	48
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Istanbulská	istanbulský	k2eAgFnSc1d1	Istanbulská
úmluva	úmluva	k1gFnSc1	úmluva
mediaci	mediace	k1gFnSc4	mediace
nezakazuje	zakazovat	k5eNaImIp3nS	zakazovat
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
formuluje	formulovat	k5eAaImIp3nS	formulovat
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
mediace	mediace	k1gFnSc2	mediace
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
dobrovolnosti	dobrovolnost	k1gFnSc6	dobrovolnost
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Kritické	kritický	k2eAgInPc1d1	kritický
hlasy	hlas	k1gInPc1	hlas
odborné	odborný	k2eAgFnSc2d1	odborná
veřejnosti	veřejnost	k1gFnSc2	veřejnost
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
manželských	manželský	k2eAgMnPc2d1	manželský
a	a	k8xC	a
rodinných	rodinný	k2eAgMnPc2d1	rodinný
poradců	poradce	k1gMnPc2	poradce
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Mgr.	Mgr.	kA	Mgr.
Pavla	Pavel	k1gMnSc2	Pavel
Rataje	Rataj	k1gMnSc2	Rataj
<g/>
,	,	kIx,	,
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Asociace	asociace	k1gFnSc2	asociace
manželských	manželský	k2eAgMnPc2d1	manželský
a	a	k8xC	a
rodinných	rodinný	k2eAgMnPc2d1	rodinný
poradců	poradce	k1gMnPc2	poradce
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zohlednit	zohlednit	k5eAaPmF	zohlednit
<g/>
,	,	kIx,	,
že	že	k8xS	že
násilí	násilí	k1gNnSc1	násilí
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
posuzovat	posuzovat	k5eAaImF	posuzovat
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
reagující	reagující	k2eAgNnSc4d1	reagující
na	na	k7c4	na
emoce	emoce	k1gFnPc4	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
chování	chování	k1gNnSc1	chování
nebývá	bývat	k5eNaImIp3nS	bývat
racionální	racionální	k2eAgNnSc1d1	racionální
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
ovlivnitelné	ovlivnitelný	k2eAgFnPc1d1	ovlivnitelná
výchovou	výchova	k1gFnSc7	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Emocí	emoce	k1gFnSc7	emoce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
násilí	násilí	k1gNnSc4	násilí
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
horší	zlý	k2eAgNnSc4d2	horší
než	než	k8xS	než
fyzické	fyzický	k2eAgNnSc4d1	fyzické
násilí	násilí	k1gNnSc4	násilí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
násilí	násilí	k1gNnSc4	násilí
psychické	psychický	k2eAgNnSc4d1	psychické
<g/>
.	.	kIx.	.
</s>
<s>
Debata	debata	k1gFnSc1	debata
o	o	k7c6	o
trestání	trestání	k1gNnSc6	trestání
fyzického	fyzický	k2eAgNnSc2d1	fyzické
násilí	násilí	k1gNnSc2	násilí
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
debatou	debata	k1gFnSc7	debata
velmi	velmi	k6eAd1	velmi
povrchní	povrchní	k2eAgFnPc1d1	povrchní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
ptát	ptát	k5eAaImF	ptát
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
a	a	k8xC	a
jak	jak	k6eAd1	jak
emoce	emoce	k1gFnPc1	emoce
vznikají	vznikat	k5eAaImIp3nP	vznikat
a	a	k8xC	a
co	co	k3yQnSc1	co
je	on	k3xPp3gMnPc4	on
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
násilí	násilí	k1gNnSc3	násilí
dochází	docházet	k5eAaImIp3nS	docházet
především	především	k9	především
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
a	a	k8xC	a
chybný	chybný	k2eAgInSc1d1	chybný
není	být	k5eNaImIp3nS	být
systém	systém	k1gInSc1	systém
hlášení	hlášení	k1gNnSc2	hlášení
a	a	k8xC	a
trestů	trest	k1gInPc2	trest
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedostatek	nedostatek	k1gInSc1	nedostatek
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
neporozumění	neporozumění	k1gNnSc3	neporozumění
procesům	proces	k1gInPc3	proces
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
špatná	špatný	k2eAgFnSc1d1	špatná
komunikace	komunikace	k1gFnSc1	komunikace
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
</s>
</p>
<p>
<s>
===	===	k?	===
Církevní	církevní	k2eAgInPc1d1	církevní
kruhy	kruh	k1gInPc1	kruh
===	===	k?	===
</s>
</p>
<p>
<s>
Kritika	kritika	k1gFnSc1	kritika
Istanbulské	istanbulský	k2eAgFnSc2d1	Istanbulská
úmluvy	úmluva	k1gFnSc2	úmluva
zaznívá	zaznívat	k5eAaImIp3nS	zaznívat
zejména	zejména	k9	zejména
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
národoveckých	národovecký	k2eAgInPc2d1	národovecký
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
jejímu	její	k3xOp3gNnSc3	její
přijetí	přijetí	k1gNnSc3	přijetí
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
postavila	postavit	k5eAaPmAgFnS	postavit
celá	celý	k2eAgFnSc1d1	celá
Česká	český	k2eAgFnSc1d1	Česká
biskupská	biskupský	k2eAgFnSc1d1	biskupská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
Řeckokatolická	řeckokatolický	k2eAgFnSc1d1	řeckokatolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
Jednota	jednota	k1gFnSc1	jednota
bratrská	bratrský	k2eAgFnSc1d1	bratrská
<g/>
,	,	kIx,	,
Církev	církev	k1gFnSc1	církev
adventistů	adventista	k1gMnPc2	adventista
sedmého	sedmý	k4xOgInSc2	sedmý
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
Evangelická	evangelický	k2eAgFnSc1d1	evangelická
církev	církev	k1gFnSc1	církev
metodistická	metodistický	k2eAgFnSc1d1	metodistická
<g/>
,	,	kIx,	,
Slezská	slezský	k2eAgFnSc1d1	Slezská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
augsburského	augsburský	k2eAgNnSc2d1	Augsburské
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
Církev	církev	k1gFnSc1	církev
bratrská	bratrský	k2eAgFnSc1d1	bratrská
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
a	a	k8xC	a
spoustu	spousta	k1gFnSc4	spousta
reakcí	reakce	k1gFnPc2	reakce
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
kázání	kázání	k1gNnSc4	kázání
Mons	Monsa	k1gFnPc2	Monsa
<g/>
.	.	kIx.	.
</s>
<s>
Petra	Petr	k1gMnSc2	Petr
Piťhy	Piťha	k1gMnSc2	Piťha
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc4	Vít
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
svátku	svátek	k1gInSc2	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
úmluvu	úmluva	k1gFnSc4	úmluva
ostře	ostro	k6eAd1	ostro
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
ženská	ženská	k1gFnSc1	ženská
lobby	lobby	k1gFnSc2	lobby
podala	podat	k5eAaPmAgFnS	podat
trestní	trestní	k2eAgNnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
pro	pro	k7c4	pro
úmyslné	úmyslný	k2eAgNnSc4d1	úmyslné
šíření	šíření	k1gNnSc4	šíření
poplašné	poplašný	k2eAgFnSc2d1	poplašná
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Piťhu	Piťh	k1gInSc2	Piťh
jménem	jméno	k1gNnSc7	jméno
celé	celý	k2eAgFnSc2d1	celá
České	český	k2eAgFnSc2d1	Česká
biskupské	biskupský	k2eAgFnSc2d1	biskupská
konference	konference	k1gFnSc2	konference
podpořil	podpořit	k5eAaPmAgMnS	podpořit
kardinál	kardinál	k1gMnSc1	kardinál
Duka	Duka	k1gMnSc1	Duka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naopak	naopak	k6eAd1	naopak
Synodní	synodní	k2eAgFnSc1d1	synodní
rada	rada	k1gFnSc1	rada
Českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgNnSc1d1	evangelické
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ratifikaci	ratifikace	k1gFnSc4	ratifikace
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
čeští	český	k2eAgMnPc1d1	český
katoličtí	katolický	k2eAgMnPc1d1	katolický
biskupové	biskup	k1gMnPc1	biskup
vydali	vydat	k5eAaPmAgMnP	vydat
pastýřský	pastýřský	k2eAgInSc4d1	pastýřský
list	list	k1gInSc4	list
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
rozmělňování	rozmělňování	k1gNnPc1	rozmělňování
přirozené	přirozený	k2eAgFnSc2d1	přirozená
identity	identita	k1gFnSc2	identita
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Informační	informační	k2eAgFnSc1d1	informační
kampaň	kampaň	k1gFnSc1	kampaň
Úřadu	úřad	k1gInSc2	úřad
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
nejčastější	častý	k2eAgFnPc4d3	nejčastější
dezinformace	dezinformace	k1gFnPc4	dezinformace
a	a	k8xC	a
mýty	mýtus	k1gInPc4	mýtus
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
září	září	k1gNnSc6	září
2018	[number]	k4	2018
Úřad	úřad	k1gInSc1	úřad
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
informativní	informativní	k2eAgFnSc4d1	informativní
brožuru	brožura	k1gFnSc4	brožura
o	o	k7c6	o
Istanbulské	istanbulský	k2eAgFnSc6d1	Istanbulská
úmluvě	úmluva	k1gFnSc6	úmluva
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
také	také	k9	také
odborníci	odborník	k1gMnPc1	odborník
a	a	k8xC	a
odbornice	odbornice	k1gFnPc1	odbornice
z	z	k7c2	z
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
prevenci	prevence	k1gFnSc4	prevence
domácího	domácí	k2eAgNnSc2d1	domácí
násilí	násilí	k1gNnSc2	násilí
a	a	k8xC	a
násilí	násilí	k1gNnSc2	násilí
na	na	k7c6	na
ženách	žena	k1gFnPc6	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2018	[number]	k4	2018
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
hlasů	hlas	k1gInPc2	hlas
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
že	že	k8xS	že
Istanbulská	istanbulský	k2eAgFnSc1d1	Istanbulská
úmluva	úmluva	k1gFnSc1	úmluva
je	být	k5eAaImIp3nS	být
neslučitelná	slučitelný	k2eNgFnSc1d1	neslučitelná
s	s	k7c7	s
Ústavou	ústava	k1gFnSc7	ústava
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
soudců	soudce	k1gMnPc2	soudce
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokument	dokument	k1gInSc1	dokument
stírá	stírat	k5eAaImIp3nS	stírat
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
domácímu	domácí	k2eAgNnSc3d1	domácí
násilí	násilí	k1gNnSc3	násilí
pouze	pouze	k6eAd1	pouze
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2019	[number]	k4	2019
Parlament	parlament	k1gInSc1	parlament
Slovenska	Slovensko	k1gNnSc2	Slovensko
drtivou	drtivý	k2eAgFnSc7d1	drtivá
většinou	většina	k1gFnSc7	většina
(	(	kIx(	(
<g/>
101	[number]	k4	101
hlasů	hlas	k1gInPc2	hlas
ze	z	k7c2	z
133	[number]	k4	133
přítomných	přítomný	k1gMnPc2	přítomný
<g/>
)	)	kIx)	)
podpořil	podpořit	k5eAaPmAgInS	podpořit
návrh	návrh	k1gInSc1	návrh
SNS	SNS	kA	SNS
na	na	k7c6	na
zastavení	zastavení	k1gNnSc6	zastavení
ratifikace	ratifikace	k1gFnSc2	ratifikace
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
základních	základní	k2eAgFnPc2d1	základní
svobod	svoboda	k1gFnPc2	svoboda
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
sociální	sociální	k2eAgFnSc1d1	sociální
charta	charta	k1gFnSc1	charta
</s>
</p>
<p>
<s>
Listina	listina	k1gFnSc1	listina
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
svobod	svoboda	k1gFnPc2	svoboda
</s>
</p>
<p>
<s>
Charta	charta	k1gFnSc1	charta
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
EU	EU	kA	EU
</s>
</p>
<p>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
úmluvy	úmluva	k1gFnSc2	úmluva
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
úmluvy	úmluva	k1gFnSc2	úmluva
česky	česky	k6eAd1	česky
</s>
</p>
<p>
<s>
Sborník	sborník	k1gInSc1	sborník
#	#	kIx~	#
<g/>
ZaIstanbul	ZaIstanbul	k1gInSc1	ZaIstanbul
</s>
</p>
