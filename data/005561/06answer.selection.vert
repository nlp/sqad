<s>
Největším	veliký	k2eAgNnSc7d3	veliký
skotským	skotský	k2eAgNnSc7d1	skotské
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Glasgow	Glasgow	k1gNnSc1	Glasgow
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
kdysi	kdysi	k6eAd1	kdysi
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
předních	přední	k2eAgFnPc2d1	přední
světových	světový	k2eAgFnPc2d1	světová
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
metropolí	metropol	k1gFnPc2	metropol
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
velké	velký	k2eAgFnSc2d1	velká
glasgowské	glasgowský	k2eAgFnSc2d1	Glasgowská
konurbace	konurbace	k1gFnSc2	konurbace
dominující	dominující	k2eAgInSc1d1	dominující
Lowlands	Lowlands	k1gInSc1	Lowlands
<g/>
.	.	kIx.	.
</s>
