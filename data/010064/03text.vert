<p>
<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
(	(	kIx(	(
<g/>
Beta	beta	k1gNnSc1	beta
vulgaris	vulgaris	k1gFnSc2	vulgaris
var.	var.	k?	var.
altissima	altissima	k1gFnSc1	altissima
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dvouletá	dvouletý	k2eAgFnSc1d1	dvouletá
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
plodina	plodina	k1gFnSc1	plodina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
laskavcovitých	laskavcovitý	k2eAgFnPc2d1	laskavcovitý
<g/>
.	.	kIx.	.
<g/>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
jeden	jeden	k4xCgInSc1	jeden
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
řazená	řazený	k2eAgFnSc1d1	řazená
mezi	mezi	k7c4	mezi
okopaniny	okopanina	k1gFnPc4	okopanina
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
sklízené	sklízený	k2eAgFnSc2d1	sklízená
masy	masa	k1gFnSc2	masa
(	(	kIx(	(
<g/>
cca	cca	kA	cca
80	[number]	k4	80
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ukryta	ukryt	k2eAgFnSc1d1	ukryta
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zásobního	zásobní	k2eAgInSc2d1	zásobní
kořenu	kořen	k1gInSc2	kořen
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
bulvy	bulva	k1gFnSc2	bulva
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
vyrůstající	vyrůstající	k2eAgInPc1d1	vyrůstající
z	z	k7c2	z
bulvy	bulva	k1gFnSc2	bulva
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
chrást	chrást	k1gFnSc4	chrást
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sklizni	sklizeň	k1gFnSc6	sklizeň
se	se	k3xPyFc4	se
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
technologiích	technologie	k1gFnPc6	technologie
chrást	chrásta	k1gFnPc2	chrásta
rozřeže	rozřezat	k5eAaPmIp3nS	rozřezat
a	a	k8xC	a
rozmete	rozmést	k5eAaPmIp3nS	rozmést
do	do	k7c2	do
plochy	plocha	k1gFnSc2	plocha
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sklizené	sklizený	k2eAgFnPc1d1	sklizená
bulvy	bulva	k1gFnPc1	bulva
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgFnP	využívat
v	v	k7c6	v
cukrovarnickém	cukrovarnický	k2eAgInSc6d1	cukrovarnický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
lihu	líh	k1gInSc2	líh
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
omezeně	omezeně	k6eAd1	omezeně
jako	jako	k8xS	jako
krmivo	krmivo	k1gNnSc1	krmivo
pro	pro	k7c4	pro
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Napadení	napadení	k1gNnPc2	napadení
háďátkem	háďátko	k1gNnSc7	háďátko
==	==	k?	==
</s>
</p>
<p>
<s>
Řepa	Řepa	k1gMnSc1	Řepa
bývá	bývat	k5eAaImIp3nS	bývat
častým	častý	k2eAgMnSc7d1	častý
hostitelem	hostitel	k1gMnSc7	hostitel
parazita	parazit	k1gMnSc2	parazit
háďátka	háďátko	k1gNnSc2	háďátko
řepného	řepný	k2eAgNnSc2d1	řepné
<g/>
.	.	kIx.	.
</s>
<s>
Napadá	napadat	k5eAaImIp3nS	napadat
především	především	k9	především
řepné	řepný	k2eAgFnPc4d1	řepná
bulvy	bulva	k1gFnPc4	bulva
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
při	při	k7c6	při
napadení	napadení	k1gNnSc6	napadení
háďátkem	háďátko	k1gNnSc7	háďátko
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mnoho	mnoho	k4c1	mnoho
postranních	postranní	k2eAgInPc2d1	postranní
kořínků	kořínek	k1gInPc2	kořínek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hladové	hladový	k2eAgInPc1d1	hladový
kořeny	kořen	k1gInPc1	kořen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měla	mít	k5eAaImAgFnS	mít
dostatek	dostatek	k1gInSc4	dostatek
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
brzdí	brzdit	k5eAaImIp3nS	brzdit
růst	růst	k1gInSc1	růst
(	(	kIx(	(
<g/>
snížení	snížení	k1gNnSc1	snížení
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
výnosu	výnos	k1gInSc2	výnos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
byla	být	k5eAaImAgFnS	být
pěstována	pěstován	k2eAgFnSc1d1	pěstována
jako	jako	k8xC	jako
krmivo	krmivo	k1gNnSc1	krmivo
již	již	k9	již
od	od	k7c2	od
antických	antický	k2eAgFnPc2d1	antická
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
použití	použití	k1gNnSc1	použití
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
cukru	cukr	k1gInSc2	cukr
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
relativně	relativně	k6eAd1	relativně
nedávno	nedávno	k6eAd1	nedávno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1590	[number]	k4	1590
poprvé	poprvé	k6eAd1	poprvé
extrahoval	extrahovat	k5eAaBmAgMnS	extrahovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
botanik	botanik	k1gMnSc1	botanik
Olivier	Olivier	k1gMnSc1	Olivier
de	de	k?	de
Serres	Serres	k1gMnSc1	Serres
z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
sladký	sladký	k2eAgInSc4d1	sladký
sirup	sirup	k1gInSc4	sirup
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
postup	postup	k1gInSc1	postup
však	však	k9	však
nenašel	najít	k5eNaPmAgInS	najít
praktické	praktický	k2eAgNnSc4d1	praktické
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
další	další	k2eAgMnPc1d1	další
se	se	k3xPyFc4	se
extrakcí	extrakce	k1gFnSc7	extrakce
cukru	cukr	k1gInSc2	cukr
zabýval	zabývat	k5eAaImAgMnS	zabývat
pruský	pruský	k2eAgMnSc1d1	pruský
chemik	chemik	k1gMnSc1	chemik
Andreas	Andreas	k1gMnSc1	Andreas
Sigismund	Sigismund	k1gMnSc1	Sigismund
Marggraf	Marggraf	k1gMnSc1	Marggraf
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
k	k	k7c3	k
extrakci	extrakce	k1gFnSc3	extrakce
cukru	cukr	k1gInSc2	cukr
použil	použít	k5eAaPmAgInS	použít
ethanol	ethanol	k1gInSc1	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeho	jeho	k3xOp3gInSc1	jeho
postup	postup	k1gInSc1	postup
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
k	k	k7c3	k
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
produkci	produkce	k1gFnSc3	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Marggrafových	Marggrafův	k2eAgMnPc2d1	Marggrafův
bývalých	bývalý	k2eAgMnPc2d1	bývalý
studentů	student	k1gMnPc2	student
Franz	Franz	k1gMnSc1	Franz
Karl	Karl	k1gMnSc1	Karl
Achard	Achard	k1gMnSc1	Achard
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaImF	věnovat
šlechtění	šlechtění	k1gNnSc4	šlechtění
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
selektivního	selektivní	k2eAgNnSc2d1	selektivní
křížení	křížení	k1gNnSc2	křížení
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
obsahu	obsah	k1gInSc2	obsah
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
%	%	kIx~	%
sacharózy	sacharóza	k1gFnPc1	sacharóza
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
20	[number]	k4	20
%	%	kIx~	%
u	u	k7c2	u
moderních	moderní	k2eAgFnPc2d1	moderní
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
cukrovar	cukrovar	k1gInSc1	cukrovar
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
cukru	cukr	k1gInSc2	cukr
z	z	k7c2	z
řepy	řepa	k1gFnSc2	řepa
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Fridricha	Fridrich	k1gMnSc2	Fridrich
Viléma	Vilém	k1gMnSc2	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Pruského	pruský	k2eAgInSc2d1	pruský
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Cunern	Cunerna	k1gFnPc2	Cunerna
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Konary	Konara	k1gFnPc4	Konara
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obrovskému	obrovský	k2eAgInSc3d1	obrovský
rozvoji	rozvoj	k1gInSc3	rozvoj
cukrovarnictví	cukrovarnictví	k1gNnSc2	cukrovarnictví
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
francouzské	francouzský	k2eAgNnSc1d1	francouzské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
zavedlo	zavést	k5eAaPmAgNnS	zavést
námořní	námořní	k2eAgFnSc4d1	námořní
blokádu	blokáda	k1gFnSc4	blokáda
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
znemožněn	znemožněn	k2eAgInSc4d1	znemožněn
dovoz	dovoz	k1gInSc4	dovoz
třtinového	třtinový	k2eAgInSc2d1	třtinový
cukru	cukr	k1gInSc2	cukr
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
již	již	k6eAd1	již
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
cukrovarů	cukrovar	k1gInPc2	cukrovar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
cukrovar	cukrovar	k1gInSc1	cukrovar
na	na	k7c4	na
řepu	řepa	k1gFnSc4	řepa
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
cukru	cukr	k1gInSc2	cukr
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
cukru	cukr	k1gInSc2	cukr
se	se	k3xPyFc4	se
sklizená	sklizený	k2eAgFnSc1d1	sklizená
řepa	řepa	k1gFnSc1	řepa
nejprve	nejprve	k6eAd1	nejprve
pere	prát	k5eAaImIp3nS	prát
a	a	k8xC	a
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
,	,	kIx,	,
řeže	řezat	k5eAaImIp3nS	řezat
na	na	k7c4	na
úzké	úzký	k2eAgInPc4d1	úzký
proužky	proužek	k1gInPc4	proužek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
řízky	řízek	k1gInPc1	řízek
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ukládá	ukládat	k5eAaImIp3nS	ukládat
do	do	k7c2	do
difuzérů	difuzér	k1gInPc2	difuzér
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
cukr	cukr	k1gInSc4	cukr
vyluhuje	vyluhovat	k5eAaImIp3nS	vyluhovat
vodou	voda	k1gFnSc7	voda
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Vyluhované	vyluhovaný	k2eAgInPc1d1	vyluhovaný
řízky	řízek	k1gInPc1	řízek
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
pro	pro	k7c4	pro
krmné	krmný	k2eAgInPc4d1	krmný
i	i	k8xC	i
potravinářské	potravinářský	k2eAgInPc4d1	potravinářský
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
získané	získaný	k2eAgFnSc2d1	získaná
cukerné	cukerný	k2eAgFnSc2d1	cukerná
šťávy	šťáva	k1gFnSc2	šťáva
je	být	k5eAaImIp3nS	být
získáván	získáván	k2eAgInSc1d1	získáván
surový	surový	k2eAgInSc1d1	surový
(	(	kIx(	(
<g/>
hnědý	hnědý	k2eAgInSc1d1	hnědý
cukr	cukr	k1gInSc1	cukr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rafinaci	rafinace	k1gFnSc6	rafinace
cukru	cukr	k1gInSc2	cukr
se	se	k3xPyFc4	se
cukerná	cukerný	k2eAgFnSc1d1	cukerná
šťáva	šťáva	k1gFnSc1	šťáva
čistí	čistit	k5eAaImIp3nS	čistit
<g/>
,	,	kIx,	,
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
různých	různý	k2eAgFnPc2d1	různá
přísad	přísada	k1gFnPc2	přísada
<g/>
,	,	kIx,	,
nečistot	nečistota	k1gFnPc2	nečistota
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
bělí	bělet	k5eAaImIp3nS	bělet
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
surové	surový	k2eAgFnSc2d1	surová
šťávy	šťáva	k1gFnSc2	šťáva
je	být	k5eAaImIp3nS	být
přidáváno	přidáván	k2eAgNnSc1d1	přidáváno
vápenné	vápenný	k2eAgNnSc1d1	vápenné
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
šťáva	šťáva	k1gFnSc1	šťáva
smísená	smísený	k2eAgFnSc1d1	smísená
s	s	k7c7	s
vápenným	vápenný	k2eAgNnSc7d1	vápenné
mlékem	mléko	k1gNnSc7	mléko
se	se	k3xPyFc4	se
v	v	k7c6	v
saturátoru	saturátor	k1gInSc6	saturátor
čeří	čeřit	k5eAaImIp3nS	čeřit
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Vysrážené	vysrážený	k2eAgNnSc4d1	vysrážené
vápno	vápno	k1gNnSc4	vápno
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
naváže	navázat	k5eAaPmIp3nS	navázat
nečistoty	nečistota	k1gFnPc1	nečistota
a	a	k8xC	a
v	v	k7c6	v
kalolisu	kalolis	k1gInSc6	kalolis
je	být	k5eAaImIp3nS	být
odfiltrováno	odfiltrovat	k5eAaPmNgNnS	odfiltrovat
od	od	k7c2	od
šťávy	šťáva	k1gFnSc2	šťáva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odváženo	odvážen	k2eAgNnSc1d1	odváženo
na	na	k7c4	na
skládku	skládka	k1gFnSc4	skládka
<g/>
.	.	kIx.	.
</s>
<s>
Vysrážené	vysrážený	k2eAgNnSc4d1	vysrážené
vápno	vápno	k1gNnSc4	vápno
z	z	k7c2	z
kalolisu	kalolis	k1gInSc2	kalolis
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
šáma	šáma	k1gFnSc1	šáma
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
k	k	k7c3	k
vápnění	vápnění	k1gNnSc3	vápnění
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Čeření	čeření	k1gNnSc1	čeření
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
čeření	čeření	k1gNnSc2	čeření
bývá	bývat	k5eAaImIp3nS	bývat
součástí	součást	k1gFnSc7	součást
rafinerie	rafinerie	k1gFnSc2	rafinerie
vápenka	vápenka	k1gFnSc1	vápenka
na	na	k7c6	na
pálení	pálení	k1gNnSc6	pálení
vápence	vápenec	k1gInSc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Vypálené	vypálený	k2eAgNnSc1d1	vypálené
vápno	vápno	k1gNnSc1	vápno
se	se	k3xPyFc4	se
uhasí	uhasit	k5eAaPmIp3nS	uhasit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vápenného	vápenný	k2eAgNnSc2d1	vápenné
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
část	část	k1gFnSc4	část
spalin	spaliny	k1gFnPc2	spaliny
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
je	být	k5eAaImIp3nS	být
používán	používán	k2eAgInSc1d1	používán
k	k	k7c3	k
vysrážení	vysrážení	k1gNnSc3	vysrážení
vápna	vápno	k1gNnSc2	vápno
při	při	k7c6	při
čeření	čeření	k1gNnSc6	čeření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
čeření	čeření	k1gNnSc6	čeření
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
za	za	k7c2	za
sníženého	snížený	k2eAgInSc2d1	snížený
tlaku	tlak	k1gInSc2	tlak
šťáva	šťáva	k1gFnSc1	šťáva
vaří	vařit	k5eAaImIp3nS	vařit
v	v	k7c6	v
odparkách	odparka	k1gFnPc6	odparka
a	a	k8xC	a
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
se	se	k3xPyFc4	se
vody	voda	k1gFnPc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
100	[number]	k4	100
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
Šťáva	Šťáva	k1gMnSc1	Šťáva
se	se	k3xPyFc4	se
zahušťuje	zahušťovat	k5eAaImIp3nS	zahušťovat
a	a	k8xC	a
cukr	cukr	k1gInSc1	cukr
se	se	k3xPyFc4	se
sráží	srážet	k5eAaImIp3nS	srážet
v	v	k7c6	v
krystalech	krystal	k1gInPc6	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Zahuštěná	zahuštěný	k2eAgFnSc1d1	zahuštěná
"	"	kIx"	"
<g/>
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
"	"	kIx"	"
štáva	štáva	k1gFnSc1	štáva
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
do	do	k7c2	do
odstředivky	odstředivka	k1gFnSc2	odstředivka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sítu	síto	k1gNnSc6	síto
odstředivky	odstředivka	k1gFnSc2	odstředivka
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
krystalky	krystalka	k1gFnPc1	krystalka
cukru	cukr	k1gInSc2	cukr
od	od	k7c2	od
"	"	kIx"	"
<g/>
zadního	zadní	k2eAgInSc2d1	zadní
cukru	cukr	k1gInSc2	cukr
<g/>
"	"	kIx"	"
–	–	k?	–
tekuté	tekutý	k2eAgFnSc2d1	tekutá
melasy	melasa	k1gFnSc2	melasa
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
vlhká	vlhký	k2eAgFnSc1d1	vlhká
cukrovina	cukrovina	k1gFnSc1	cukrovina
se	se	k3xPyFc4	se
na	na	k7c6	na
sítech	síto	k1gNnPc6	síto
třídí	třídit	k5eAaImIp3nP	třídit
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
krystallů	krystall	k1gMnPc2	krystall
"	"	kIx"	"
<g/>
cukr	cukr	k1gInSc4	cukr
práškový	práškový	k2eAgInSc4d1	práškový
<g/>
"	"	kIx"	"
až	až	k9	až
"	"	kIx"	"
<g/>
cukr	cukr	k1gInSc1	cukr
krystal	krystal	k1gInSc1	krystal
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
plní	plnit	k5eAaImIp3nS	plnit
do	do	k7c2	do
forem	forma	k1gFnPc2	forma
nebo	nebo	k8xC	nebo
suší	suš	k1gFnPc2	suš
<g/>
.	.	kIx.	.
</s>
<s>
Melasa	melasa	k1gFnSc1	melasa
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
páchne	páchnout	k5eAaImIp3nS	páchnout
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ke	k	k7c3	k
krmným	krmný	k2eAgInPc3d1	krmný
účelům	účel	k1gInPc3	účel
nebo	nebo	k8xC	nebo
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
lihu	líh	k1gInSc2	líh
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
tzv.	tzv.	kA	tzv.
surového	surový	k2eAgNnSc2d1	surové
(	(	kIx(	(
<g/>
hnědého	hnědý	k2eAgMnSc2d1	hnědý
<g/>
)	)	kIx)	)
cukru	cukr	k1gInSc2	cukr
se	se	k3xPyFc4	se
při	při	k7c6	při
postupu	postup	k1gInSc6	postup
výroby	výroba	k1gFnSc2	výroba
neprovádí	provádět	k5eNaImIp3nS	provádět
čeření	čeření	k1gNnSc1	čeření
a	a	k8xC	a
zbavování	zbavování	k1gNnSc1	zbavování
příměsí	příměs	k1gFnPc2	příměs
a	a	k8xC	a
nešistot	nešistota	k1gFnPc2	nešistota
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
po	po	k7c6	po
převezení	převezení	k1gNnSc6	převezení
surového	surový	k2eAgInSc2d1	surový
cukru	cukr	k1gInSc2	cukr
do	do	k7c2	do
rafinérie	rafinérie	k1gFnSc2	rafinérie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
surový	surový	k2eAgInSc4d1	surový
cukr	cukr	k1gInSc4	cukr
znovu	znovu	k6eAd1	znovu
rozpouštěn	rozpouštěn	k2eAgMnSc1d1	rozpouštěn
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
proces	proces	k1gInSc4	proces
čeření	čeření	k1gNnSc2	čeření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dotační	dotační	k2eAgFnSc2d1	dotační
politiky	politika	k1gFnSc2	politika
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
ČR	ČR	kA	ČR
do	do	k7c2	do
EU	EU	kA	EU
ke	k	k7c3	k
kolapsu	kolaps	k1gInSc3	kolaps
českého	český	k2eAgNnSc2d1	české
cukrovarnictví	cukrovarnictví	k1gNnSc2	cukrovarnictví
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
rušení	rušení	k1gNnSc3	rušení
a	a	k8xC	a
bourání	bourání	k1gNnSc4	bourání
i	i	k9	i
moderních	moderní	k2eAgInPc2d1	moderní
cukrovarů	cukrovar	k1gInPc2	cukrovar
(	(	kIx(	(
<g/>
Hrochův	Hrochův	k2eAgInSc1d1	Hrochův
Týnec	Týnec	k1gInSc1	Týnec
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zmenšení	zmenšení	k1gNnSc3	zmenšení
osevní	osevní	k2eAgFnSc2d1	osevní
plochy	plocha	k1gFnSc2	plocha
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
však	však	k9	však
díky	díky	k7c3	díky
postupnému	postupný	k2eAgNnSc3d1	postupné
zvyšování	zvyšování	k1gNnSc3	zvyšování
výnosu	výnos	k1gInSc2	výnos
cukru	cukr	k1gInSc2	cukr
z	z	k7c2	z
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
plodin	plodina	k1gFnPc2	plodina
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
až	až	k9	až
na	na	k7c4	na
hodnoty	hodnota	k1gFnPc4	hodnota
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
produkci	produkce	k1gFnSc4	produkce
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
regulace	regulace	k1gFnSc2	regulace
pěstování	pěstování	k1gNnSc2	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
EU	EU	kA	EU
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
další	další	k2eAgFnSc4d1	další
nárust	nárust	k1gFnSc4	nárust
produkce	produkce	k1gFnSc2	produkce
této	tento	k3xDgFnSc2	tento
plodiny	plodina	k1gFnSc2	plodina
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
