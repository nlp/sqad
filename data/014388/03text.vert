<s>
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
O	o	k7c6
hudebním	hudební	k2eAgNnSc6d1
albu	album	k1gNnSc6
rockové	rockový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Synkopy	synkopa	k1gFnSc2
61	#num#	k4
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Formule	formule	k1gFnSc1
1	#num#	k4
(	(	kIx(
<g/>
album	album	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
Nadřazené	nadřazený	k2eAgInPc1d1
odvětví	odvětví	k1gNnPc2
</s>
<s>
není	být	k5eNaImIp3nS
Disciplíny	disciplína	k1gFnPc4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
jezdců	jezdec	k1gInPc2
Pohár	pohár	k1gInSc4
konstruktérů	konstruktér	k1gMnPc2
Vrcholné	vrcholný	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
Pohár	pohár	k1gInSc4
konstruktérů	konstruktér	k1gMnPc2
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
Mezinárodní	mezinárodní	k2eAgFnSc2d1
federace	federace	k1gFnSc2
Název	název	k1gInSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
automobilová	automobilový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Založena	založen	k2eAgFnSc1d1
</s>
<s>
1904	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.formula1.com	www.formula1.com	k1gInSc1
Národní	národní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Název	název	k1gInSc1
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1
automotoklub	automotoklub	k1gInSc1
ČR	ČR	kA
Založen	založit	k5eAaPmNgInS
</s>
<s>
1993	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.uamk-cr.cz	www.uamk-cr.cz	k1gMnSc1
</s>
<s>
Organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
současné	současný	k2eAgFnSc2d1
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bližší	blízký	k2eAgInPc1d2
detaily	detail	k1gInPc1
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
F	F	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
série	série	k1gFnSc1
závodů	závod	k1gInPc2
formulí	formule	k1gFnPc2
<g/>
,	,	kIx,
založena	založen	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nahradila	nahradit	k5eAaPmAgFnS
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
za	za	k7c4
královskou	královský	k2eAgFnSc4d1
disciplínu	disciplína	k1gFnSc4
automobilového	automobilový	k2eAgInSc2d1
sportu	sport	k1gInSc2
pro	pro	k7c4
takzvané	takzvaný	k2eAgInPc4d1
monoposty	monopost	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
pod	pod	k7c4
Mezinárodní	mezinárodní	k2eAgFnSc4d1
automobilovou	automobilový	k2eAgFnSc4d1
federaci	federace	k1gFnSc4
(	(	kIx(
<g/>
FIA	FIA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
organizační	organizační	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
F1	F1	k1gFnSc2
sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
série	série	k1gFnSc2
závodů	závod	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
označovány	označovat	k5eAaImNgFnP
jako	jako	k8xC,k8xS
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěží	soutěž	k1gFnPc2
se	se	k3xPyFc4
na	na	k7c6
uzavřených	uzavřený	k2eAgInPc6d1
autodromech	autodrom	k1gInPc6
či	či	k8xC
tratích	trať	k1gFnPc6
a	a	k8xC
městských	městský	k2eAgInPc6d1
okruzích	okruh	k1gInPc6
různých	různý	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
a	a	k8xC
délek	délka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozy	vůz	k1gInPc7
jsou	být	k5eAaImIp3nP
jednomístné	jednomístný	k2eAgInPc1d1
<g/>
,	,	kIx,
k	k	k7c3
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
speciálně	speciálně	k6eAd1
vyrobené	vyrobený	k2eAgFnSc3d1
<g/>
,	,	kIx,
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
technické	technický	k2eAgInPc1d1
parametry	parametr	k1gInPc1
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nP
vzhledem	vzhled	k1gInSc7
k	k	k7c3
pravidlům	pravidlo	k1gNnPc3
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
upravují	upravovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
se	se	k3xPyFc4
pořádá	pořádat	k5eAaImIp3nS
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
jezdců	jezdec	k1gMnPc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
pohár	pohár	k1gInSc4
konstruktérů	konstruktér	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Evropa	Evropa	k1gFnSc1
je	být	k5eAaImIp3nS
tradičním	tradiční	k2eAgNnSc7d1
operačním	operační	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
Formule	formule	k1gFnSc1
1	#num#	k4
a	a	k8xC
nejvýznamnějším	významný	k2eAgInSc7d3
trhem	trh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
si	se	k3xPyFc3
získala	získat	k5eAaPmAgFnS
tato	tento	k3xDgFnSc1
sportovní	sportovní	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
na	na	k7c6
popularitě	popularita	k1gFnSc6
a	a	k8xC
prosadila	prosadit	k5eAaPmAgFnS
se	se	k3xPyFc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
částech	část	k1gFnPc6
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejnovější	nový	k2eAgMnPc4d3
pořadatele	pořadatel	k1gMnPc4
se	se	k3xPyFc4
zařadili	zařadit	k5eAaPmAgMnP
Bahrajn	Bahrajn	k1gMnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Malajsie	Malajsie	k1gFnSc1
a	a	k8xC
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
novými	nový	k2eAgMnPc7d1
kandidáty	kandidát	k1gMnPc7
na	na	k7c4
pořádání	pořádání	k1gNnSc4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
jsou	být	k5eAaImIp3nP
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
Dubaj	Dubaj	k1gFnSc1
a	a	k8xC
Indie	Indie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
zájmu	zájem	k1gInSc6
FIA	FIA	kA
je	být	k5eAaImIp3nS
rozšíření	rozšíření	k1gNnSc1
aktivit	aktivita	k1gFnPc2
směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
a	a	k8xC
do	do	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
nespočet	nespočet	k1gInSc4
Velkých	velký	k2eAgFnPc2d1
cen	cena	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
Formule	formule	k1gFnSc1
1	#num#	k4
netěší	těšit	k5eNaImIp3nS
takové	takový	k3xDgFnSc3
popularitě	popularita	k1gFnSc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motorismus	motorismus	k1gInSc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
tří	tři	k4xCgFnPc2
sérií	série	k1gFnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
ovlivňován	ovlivňovat	k5eAaImNgInS
politickým	politický	k2eAgInSc7d1
a	a	k8xC
komerčním	komerční	k2eAgInSc7d1
bojem	boj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
F1	F1	k4
je	být	k5eAaImIp3nS
řízena	řízen	k2eAgFnSc1d1
„	„	k?
<g/>
Fédération	Fédération	k1gInSc1
Internationale	Internationale	k1gFnSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Automobile	automobil	k1gInSc6
<g/>
“	“	k?
(	(	kIx(
<g/>
Mezinárodní	mezinárodní	k2eAgNnSc1d1
automobilovou	automobilový	k2eAgFnSc7d1
federací	federace	k1gFnSc7
-	-	kIx~
FIA	FIA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Place	plac	k1gInSc6
de	de	k?
la	la	k1gNnSc2
Concorde	Concord	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současným	současný	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
je	být	k5eAaImIp3nS
Jean	Jean	k1gMnSc1
Todt	Todt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finanční	finanční	k2eAgInPc1d1
a	a	k8xC
obchodní	obchodní	k2eAgInPc1d1
toky	tok	k1gInPc1
jsou	být	k5eAaImIp3nP
řízeny	řídit	k5eAaImNgInP
Formula	Formulum	k1gNnSc2
One	One	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
spadajíci	spadajík	k1gMnPc1
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
společnosti	společnost	k1gFnSc2
SLEC	SLEC	kA
Holdings	Holdings	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
prodala	prodat	k5eAaPmAgFnS
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
část	část	k1gFnSc1
akcií	akcie	k1gFnPc2
CVC	CVC	kA
Capital	Capital	k1gMnSc1
Partners	Partners	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc4
a	a	k8xC
televizní	televizní	k2eAgNnPc4d1
práva	právo	k1gNnPc4
kontroluje	kontrolovat	k5eAaImIp3nS
Formula	Formula	k1gFnSc1
One	One	k1gFnSc2
Management	management	k1gInSc1
a	a	k8xC
tu	tu	k6eAd1
ovládá	ovládat	k5eAaImIp3nS
Chase	chasa	k1gFnSc3
Carey	Carea	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
společnosti	společnost	k1gFnSc2
Liberty	Libert	k1gInPc1
media	medium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
s	s	k7c7
vozem	vůz	k1gInSc7
Mercedes-Benz	Mercedes-Benz	k1gMnSc1
W196	W196	k1gMnSc1
při	při	k7c6
exhibici	exhibice	k1gFnSc6
na	na	k7c6
Nürburgringu	Nürburgring	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Historie	historie	k1gFnSc1
Formule	formule	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInPc1
automobilové	automobilový	k2eAgInPc1d1
závody	závod	k1gInPc1
byly	být	k5eAaImAgInP
pořádány	pořádat	k5eAaImNgInP
ve	v	k7c6
Francii	Francie	k1gFnSc6
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těch	ten	k3xDgInPc2
se	se	k3xPyFc4
zúčastňovaly	zúčastňovat	k5eAaImAgInP
automobily	automobil	k1gInPc1
poháněné	poháněný	k2eAgInPc1d1
benzínem	benzín	k1gInSc7
<g/>
,	,	kIx,
parou	para	k1gFnSc7
i	i	k8xC
elektřinou	elektřina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
století	století	k1gNnSc2
se	se	k3xPyFc4
již	již	k6eAd1
začaly	začít	k5eAaPmAgInP
automobily	automobil	k1gInPc1
v	v	k7c6
soutěžích	soutěž	k1gFnPc6
rozdělovat	rozdělovat	k5eAaImF
do	do	k7c2
kategorií	kategorie	k1gFnPc2
podle	podle	k7c2
hmotnosti	hmotnost	k1gFnSc2
(	(	kIx(
<g/>
což	což	k9
byla	být	k5eAaImAgFnS
vlastně	vlastně	k9
první	první	k4xOgNnPc1
technická	technický	k2eAgNnPc1d1
omezení	omezení	k1gNnPc1
konstrukce	konstrukce	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
soutěžily	soutěžit	k5eAaImAgFnP
prakticky	prakticky	k6eAd1
výhradně	výhradně	k6eAd1
automobily	automobil	k1gInPc1
se	s	k7c7
spalovacími	spalovací	k2eAgInPc7d1
motory	motor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímým	přímý	k2eAgNnSc7d1
předchůdcem	předchůdce	k1gMnSc7
Velkých	velký	k2eAgFnPc2d1
cen	cena	k1gFnPc2
byly	být	k5eAaImAgInP
závody	závod	k1gInPc1
o	o	k7c4
Pohár	pohár	k1gInSc4
Gordona	Gordon	k1gMnSc2
Bennetta	Bennett	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
vypsal	vypsat	k5eAaPmAgMnS
americký	americký	k2eAgMnSc1d1
vydavatel	vydavatel	k1gMnSc1
novin	novina	k1gFnPc2
a	a	k8xC
proběhly	proběhnout	k5eAaPmAgInP
v	v	k7c6
letech	let	k1gInPc6
1900	#num#	k4
<g/>
–	–	k?
<g/>
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc4
Velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
uspořádal	uspořádat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
francouzský	francouzský	k2eAgInSc1d1
autoklub	autoklub	k1gInSc1
(	(	kIx(
<g/>
Automobile	automobil	k1gInSc6
Club	club	k1gInSc4
de	de	k?
France	Franc	k1gMnSc2
<g/>
)	)	kIx)
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
l	l	kA
<g/>
'	'	kIx"
<g/>
ACF	ACF	kA
(	(	kIx(
<g/>
Velká	velký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
francouzského	francouzský	k2eAgInSc2d1
autoklubu	autoklub	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgInSc6
roce	rok	k1gInSc6
době	době	k6eAd1
platilo	platit	k5eAaImAgNnS
jediné	jediný	k2eAgNnSc1d1
omezení	omezení	k1gNnSc1
<g/>
:	:	kIx,
Vůz	vůz	k1gInSc1
nesmí	smět	k5eNaImIp3nS
mít	mít	k5eAaImF
hmotnost	hmotnost	k1gFnSc4
vyšší	vysoký	k2eAgFnSc4d2
než	než	k8xS
1000	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
přibývalo	přibývat	k5eAaImAgNnS
technických	technický	k2eAgNnPc2d1
omezení	omezení	k1gNnPc2
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
byla	být	k5eAaImAgFnS
omezena	omezit	k5eAaPmNgFnS
maximální	maximální	k2eAgFnSc1d1
spotřeba	spotřeba	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1908	#num#	k4
byla	být	k5eAaImAgFnS
určena	určit	k5eAaPmNgFnS
minimální	minimální	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
a	a	k8xC
plocha	plocha	k1gFnSc1
pístů	píst	k1gInPc2
motoru	motor	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1909	#num#	k4
<g/>
–	–	k?
<g/>
1911	#num#	k4
se	se	k3xPyFc4
Velká	velký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
nekonala	konat	k5eNaImAgFnS
pro	pro	k7c4
nezájem	nezájem	k1gInSc4
automobilek	automobilka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závody	závod	k1gInPc1
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
byly	být	k5eAaImAgFnP
příliš	příliš	k6eAd1
nákladné	nákladný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
konaly	konat	k5eAaImAgFnP
ještě	ještě	k9
tři	tři	k4xCgFnPc1
Velké	velký	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
dalšími	další	k2eAgNnPc7d1
pravidly	pravidlo	k1gNnPc7
ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
série	série	k1gFnSc1
přejmenovala	přejmenovat	k5eAaPmAgFnS
na	na	k7c4
Formule	formule	k1gFnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
<g/>
;	;	kIx,
ve	v	k7c6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
prvních	první	k4xOgInPc2
pět	pět	k4xCc1
ročníků	ročník	k1gInPc2
evropského	evropský	k2eAgInSc2d1
šampionátu	šampionát	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
nazván	nazván	k2eAgMnSc1d1
Grandes	Grandes	k1gMnSc1
Epreuves	Epreuves	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Hned	hned	k6eAd1
v	v	k7c6
prvních	první	k4xOgInPc6
poválečných	poválečný	k2eAgInPc6d1
letech	let	k1gInPc6
byla	být	k5eAaImAgFnS
ustanovena	ustanoven	k2eAgFnSc1d1
Formule	formule	k1gFnSc1
A	A	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
základem	základ	k1gInSc7
poválečných	poválečný	k2eAgInPc2d1
závodů	závod	k1gInPc2
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
prozatím	prozatím	k6eAd1
neměly	mít	k5eNaImAgInP
status	status	k1gInSc4
mistrovství	mistrovství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
závodů	závod	k1gInPc2
bylo	být	k5eAaImAgNnS
zorganizováno	zorganizovat	k5eAaPmNgNnS
již	již	k9
v	v	k7c6
období	období	k1gNnSc6
před	před	k7c7
druhou	druhý	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myšlenka	myšlenka	k1gFnSc1
uspořádat	uspořádat	k5eAaPmF
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
spatřila	spatřit	k5eAaPmAgFnS
světlo	světlo	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
byla	být	k5eAaImAgFnS
Formule	formule	k1gFnSc1
A	a	k9
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
<g/>
,	,	kIx,
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
i	i	k9
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
vznikla	vzniknout	k5eAaPmAgFnS
nižší	nízký	k2eAgFnSc1d2
série	série	k1gFnSc1
Formule	formule	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
automobilů	automobil	k1gInPc2
se	se	k3xPyFc4
tento	tento	k3xDgInSc4
rok	rok	k1gInSc4
poprvé	poprvé	k6eAd1
a	a	k8xC
naposledy	naposledy	k6eAd1
jela	jet	k5eAaImAgFnS
také	také	k9
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
sezóně	sezóna	k1gFnSc6
1949	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
kongresu	kongres	k1gInSc6
zástupců	zástupce	k1gMnPc2
pořadatelů	pořadatel	k1gMnPc2
vybralo	vybrat	k5eAaPmAgNnS
7	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
započítávaných	započítávaný	k2eAgInPc2d1
do	do	k7c2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
a	a	k8xC
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
automobilů	automobil	k1gInPc2
bylo	být	k5eAaImAgNnS
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
závody	závod	k1gInPc4
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
závodem	závod	k1gInSc7
byla	být	k5eAaImAgFnS
Velká	velký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
soutěžilo	soutěžit	k5eAaImAgNnS
o	o	k7c4
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
mezi	mezi	k7c4
piloty	pilota	k1gFnPc4
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
se	se	k3xPyFc4
rozdělují	rozdělovat	k5eAaImIp3nP
body	bod	k1gInPc1
i	i	k9
mezi	mezi	k7c4
konstruktéry	konstruktér	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInPc4d1
šampionáty	šampionát	k1gInPc4
pro	pro	k7c4
vozy	vůz	k1gInPc4
F1	F1	k1gMnPc2
se	se	k3xPyFc4
pořádaly	pořádat	k5eAaImAgFnP
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1960	#num#	k4
a	a	k8xC
1970	#num#	k4
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
Velké	velký	k2eAgFnSc3d1
Británii	Británie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počátečních	počáteční	k2eAgInPc6d1
letech	let	k1gInPc6
byla	být	k5eAaImAgFnS
organizována	organizován	k2eAgFnSc1d1
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
do	do	k7c2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
nezapočítávaly	započítávat	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
57	#num#	k4
ročníků	ročník	k1gInPc2
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
768	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
<g/>
,	,	kIx,
po	po	k7c6
skončení	skončení	k1gNnSc6
ročníku	ročník	k1gInSc2
2017	#num#	k4
jejich	jejich	k3xOp3gFnSc4
počet	počet	k1gInSc1
vzroste	vzrůst	k5eAaPmIp3nS
na	na	k7c4
976	#num#	k4
–	–	k?
k	k	k7c3
nim	on	k3xPp3gInPc3
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
přidat	přidat	k5eAaPmF
dalších	další	k2eAgFnPc2d1
368	#num#	k4
závodů	závod	k1gInPc2
nezapočítávaných	započítávaný	k2eNgInPc2d1
do	do	k7c2
klasifikace	klasifikace	k1gFnSc2
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
období	období	k1gNnSc6
1946	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
byl	být	k5eAaImAgInS
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
nejznámějším	známý	k2eAgInSc7d3
závodem	závod	k1gInSc7
Race	Race	k1gNnSc4
of	of	k?
Champions	Champions	k1gInSc1
organizovaný	organizovaný	k2eAgInSc1d1
v	v	k7c4
Brands	Brands	k1gInSc4
Hatch	Hatcha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vinou	vinou	k7c2
rostoucích	rostoucí	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
na	na	k7c4
provoz	provoz	k1gInSc4
týmů	tým	k1gInPc2
a	a	k8xC
organizací	organizace	k1gFnPc2
závodů	závod	k1gInPc2
se	se	k3xPyFc4
poslední	poslední	k2eAgInSc1d1
závod	závod	k1gInSc1
nezapočítávaný	započítávaný	k2eNgInSc1d1
do	do	k7c2
MS	MS	kA
uskutečnil	uskutečnit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počínaje	počínaje	k7c7
rokem	rok	k1gInSc7
1984	#num#	k4
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgInPc1
uskutečněné	uskutečněný	k2eAgInPc1d1
závody	závod	k1gInPc1
F1	F1	k1gMnSc4
součástí	součást	k1gFnSc7
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Roky	rok	k1gInPc1
formování	formování	k1gNnSc2
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc1
s	s	k7c7
vozem	vůz	k1gInSc7
Lotus	Lotus	kA
<g/>
–	–	k?
<g/>
Climax	Climax	k1gInSc1
při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Německa	Německo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
na	na	k7c6
trati	trať	k1gFnSc6
Nürburgring	Nürburgring	k1gInSc4
</s>
<s>
První	první	k4xOgInSc1
šampionát	šampionát	k1gInSc1
Formule	formule	k1gFnSc2
1	#num#	k4
byl	být	k5eAaImAgInS
ve	v	k7c6
znamení	znamení	k1gNnSc6
nadvlády	nadvláda	k1gFnSc2
vozů	vůz	k1gInPc2
Alfa	alfa	k1gNnSc1
Romeo	Romeo	k1gMnSc1
a	a	k8xC
prvním	první	k4xOgMnSc6
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Giuseppe	Giusepp	k1gInSc5
Farina	Farin	k2eAgNnPc1d1
na	na	k7c6
voze	vůz	k1gInSc6
Alfa	alfa	k1gFnSc1
Romeo	Romeo	k1gMnSc1
158	#num#	k4
před	před	k7c7
svými	svůj	k3xOyFgMnPc7
stájovými	stájový	k2eAgMnPc7d1
kolegy	kolega	k1gMnPc7
Fangiem	Fangius	k1gMnSc7
a	a	k8xC
Fagiolim	Fagioli	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
právě	právě	k6eAd1
Fangio	Fangio	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
získal	získat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
titul	titul	k1gInSc4
ve	v	k7c6
vylepšeném	vylepšený	k2eAgInSc6d1
modelu	model	k1gInSc6
Alfa	alfa	k1gFnSc1
Romeo	Romeo	k1gMnSc1
159	#num#	k4
<g/>
,	,	kIx,
další	další	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
tituly	titul	k1gInPc4
vybojoval	vybojovat	k5eAaPmAgInS
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
šesti	šest	k4xCc6
letech	léto	k1gNnPc6
v	v	k7c6
různých	různý	k2eAgInPc6d1
vozech	vůz	k1gInPc6
<g/>
:	:	kIx,
Mercedes-Benz	Mercedes-Benz	k1gMnSc1
<g/>
,	,	kIx,
Maserati	Maserat	k1gMnPc1
a	a	k8xC
Ferrari	Ferrari	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
nadvládu	nadvláda	k1gFnSc4
přerušil	přerušit	k5eAaPmAgMnS
jen	jen	k9
Alberto	Alberta	k1gFnSc5
Ascari	Ascari	k1gNnPc7
na	na	k7c4
Ferrari	Ferrari	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
jezdcem	jezdec	k1gMnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
titul	titul	k1gInSc4
obhájit	obhájit	k5eAaPmF
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
prozatím	prozatím	k6eAd1
posledním	poslední	k2eAgMnSc7d1
italským	italský	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stirling	Stirling	k1gInSc1
Moss	Moss	k1gInSc4
dokázal	dokázat	k5eAaPmAgInS
zvítězit	zvítězit	k5eAaPmF
v	v	k7c6
mnoha	mnoho	k4c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
nikdy	nikdy	k6eAd1
nezískal	získat	k5eNaPmAgMnS
titul	titul	k1gInSc4
a	a	k8xC
často	často	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
přezdívalo	přezdívat	k5eAaImAgNnS
nekorunovaný	korunovaný	k2eNgMnSc1d1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
prvními	první	k4xOgFnPc7
technickými	technický	k2eAgFnPc7d1
inovacemi	inovace	k1gFnPc7
přišel	přijít	k5eAaPmAgMnS
Cooper	Cooper	k1gMnSc1
znovuzavedením	znovuzavedení	k1gNnSc7
umístění	umístění	k1gNnSc4
motoru	motor	k1gInSc2
veprostřed	veprostřed	k6eAd1
(	(	kIx(
<g/>
na	na	k7c6
základě	základ	k1gInSc6
pokusu	pokus	k1gInSc2
konstruktéra	konstruktér	k1gMnSc2
firmy	firma	k1gFnSc2
Auto	auto	k1gNnSc1
Union	union	k1gInSc1
Ferdinanda	Ferdinand	k1gMnSc2
Porsche	Porsche	k1gNnSc2
během	během	k7c2
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přirozené	přirozený	k2eAgFnSc2d1
evoluce	evoluce	k1gFnSc2
úspěšného	úspěšný	k2eAgInSc2d1
projektu	projekt	k1gInSc2
z	z	k7c2
Formule	formule	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc4
<g/>
,	,	kIx,
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
z	z	k7c2
let	léto	k1gNnPc2
1959	#num#	k4
a	a	k8xC
1960	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
pilotem	pilot	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
triumfovat	triumfovat	k5eAaBmF
i	i	k9
na	na	k7c6
voze	vůz	k1gInSc6
vlastní	vlastní	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
potvrdil	potvrdit	k5eAaPmAgMnS
tak	tak	k6eAd1
úspěšnost	úspěšnost	k1gFnSc4
nové	nový	k2eAgFnSc2d1
koncepce	koncepce	k1gFnSc2
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
již	již	k9
všechny	všechen	k3xTgInPc1
vozy	vůz	k1gInPc1
nastupují	nastupovat	k5eAaImIp3nP
s	s	k7c7
umístěním	umístění	k1gNnSc7
motoru	motor	k1gInSc2
veprostřed	veprostřed	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Mike	Mike	k6eAd1
Hawthorn	Hawthorn	k1gInSc1
s	s	k7c7
Ferrari	Ferrari	k1gMnSc7
při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Argentiny	Argentina	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
</s>
<s>
Prvním	první	k4xOgInSc7
pilotem	pilot	k1gInSc7
britské	britský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
Mike	Mike	k1gNnPc2
Hawthorn	Hawthorna	k1gFnPc2
s	s	k7c7
vozem	vůz	k1gInSc7
Ferrari	Ferrari	k1gMnSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
vozy	vůz	k1gInPc1
Vanwall	Vanwalla	k1gFnPc2
získávají	získávat	k5eAaImIp3nP
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
jako	jako	k8xS,k8xC
první	první	k4xOgInSc4
pohár	pohár	k1gInSc4
konstruktérů	konstruktér	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Génius	génius	k1gMnSc1
Colin	Colin	k1gMnSc1
Chapman	Chapman	k1gMnSc1
<g/>
,	,	kIx,
projektant	projektant	k1gMnSc1
a	a	k8xC
pozdější	pozdní	k2eAgMnSc1d2
zakladatel	zakladatel	k1gMnSc1
stáje	stáj	k1gFnSc2
Lotus	Lotus	kA
<g/>
,	,	kIx,
konstruoval	konstruovat	k5eAaImAgMnS
vozy	vůz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
dominovaly	dominovat	k5eAaImAgFnP
formuli	formule	k1gFnSc4
1	#num#	k4
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
dekádě	dekáda	k1gFnSc6
její	její	k3xOp3gFnSc2
historie	historie	k1gFnSc2
až	až	k9
do	do	k7c2
počátku	počátek	k1gInSc2
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokázal	dokázat	k5eAaPmAgMnS
vyzdvihnout	vyzdvihnout	k5eAaPmF
takové	takový	k3xDgFnPc4
piloty	pilota	k1gFnPc4
<g/>
,	,	kIx,
jakými	jaký	k3yIgFnPc7,k3yRgFnPc7,k3yQgFnPc7
byli	být	k5eAaImAgMnP
Jim	on	k3xPp3gMnPc3
Clark	Clark	k1gInSc1
<g/>
,	,	kIx,
Jackie	Jackie	k1gFnSc1
Stewart	Stewart	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Surtees	Surtees	k1gMnSc1
<g/>
,	,	kIx,
Jack	Jack	k1gMnSc1
Brabham	Brabham	k1gInSc1
<g/>
,	,	kIx,
Graham	Graham	k1gMnSc1
Hill	Hill	k1gMnSc1
a	a	k8xC
Denny	Denna	k1gFnPc1
Hulme	houlit	k5eAaImRp1nP
<g/>
;	;	kIx,
v	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
také	také	k9
britští	britský	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
získali	získat	k5eAaPmAgMnP
jasnou	jasný	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
motoristické	motoristický	k2eAgFnSc6d1
disciplíně	disciplína	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
vyhráli	vyhrát	k5eAaPmAgMnP
12	#num#	k4
světových	světový	k2eAgInPc2d1
titulů	titul	k1gInPc2
v	v	k7c4
období	období	k1gNnSc4
mezi	mezi	k7c4
roky	rok	k1gInPc4
1962	#num#	k4
a	a	k8xC
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
Lotus	Lotus	kA
vyprojektoval	vyprojektovat	k5eAaPmAgMnS
první	první	k4xOgInSc4
vůz	vůz	k1gInSc4
se	s	k7c7
samostatnou	samostatný	k2eAgFnSc7d1
skořepinovou	skořepinový	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
namísto	namísto	k7c2
klasického	klasický	k2eAgInSc2d1
trubkového	trubkový	k2eAgInSc2d1
rámu	rám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
přišel	přijít	k5eAaPmAgInS
Lotus	Lotus	kA
tentokrát	tentokrát	k6eAd1
s	s	k7c7
novým	nový	k2eAgNnSc7d1
zbarvením	zbarvení	k1gNnSc7
vozu	vůz	k1gInSc2
<g/>
,	,	kIx,
opustil	opustit	k5eAaPmAgInS
tradiční	tradiční	k2eAgFnSc4d1
zelenou	zelená	k1gFnSc4
pro	pro	k7c4
britské	britský	k2eAgInPc4d1
vozy	vůz	k1gInPc4
a	a	k8xC
představil	představit	k5eAaPmAgInS
monopost	monopost	k1gInSc1
vyvedený	vyvedený	k2eAgInSc1d1
v	v	k7c6
červeno-zlato-bílých	červeno-zlato-bílý	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
svého	své	k1gNnSc2
sponzora	sponzor	k1gMnSc2
–	–	k?
odstartovala	odstartovat	k5eAaPmAgFnS
tak	tak	k9
éra	éra	k1gFnSc1
sponzorství	sponzorství	k1gNnSc2
ve	v	k7c6
formuli	formule	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
let	léto	k1gNnPc2
šedesátých	šedesátý	k4xOgNnPc2
a	a	k8xC
začátkem	začátkem	k7c2
sedmdesátých	sedmdesátý	k4xOgNnPc2
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
také	také	k9
vzhled	vzhled	k1gInSc1
vozů	vůz	k1gInPc2
a	a	k8xC
aerodynamika	aerodynamika	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
až	až	k9
doposud	doposud	k6eAd1
byla	být	k5eAaImAgFnS
pojmem	pojem	k1gInSc7
neznámým	známý	k2eNgInSc7d1
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
na	na	k7c6
významnosti	významnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vozech	vůz	k1gInPc6
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
první	první	k4xOgNnPc4
křidélka	křidélko	k1gNnPc4
a	a	k8xC
aerodynamické	aerodynamický	k2eAgFnPc4d1
plošky	ploška	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
FISA	FISA	kA
(	(	kIx(
<g/>
Fédération	Fédération	k1gInSc1
Internationale	Internationale	k1gFnSc2
du	du	k?
Sport	sport	k1gInSc1
Automobile	automobil	k1gInSc5
<g/>
)	)	kIx)
jejímž	jejíž	k3xOyRp3gMnSc7
prezidentem	prezident	k1gMnSc7
byl	být	k5eAaImAgMnS
Jean	Jean	k1gMnSc1
Marie	Maria	k1gFnSc2
Balestre	Balestr	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známé	známý	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
především	především	k9
léta	léto	k1gNnSc2
se	se	k3xPyFc4
táhnoucí	táhnoucí	k2eAgInPc1d1
spory	spor	k1gInPc1
s	s	k7c7
konkurenční	konkurenční	k2eAgFnSc7d1
FOCA	FOCA	kA
(	(	kIx(
<g/>
Formula	Formula	k1gFnSc1
One	One	k1gFnSc2
Constructors	Constructorsa	k1gFnPc2
Association	Association	k1gInSc1
<g/>
,	,	kIx,
řízenou	řízený	k2eAgFnSc7d1
Bernie	Bernie	k1gFnSc1
Ecclestonem	Eccleston	k1gInSc7
<g/>
)	)	kIx)
o	o	k7c4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
tokem	tok	k1gInSc7
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
plynoucích	plynoucí	k2eAgFnPc2d1
především	především	k9
z	z	k7c2
prodeje	prodej	k1gInSc2
televizních	televizní	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Změny	změna	k1gFnPc1
po	po	k7c6
zákazu	zákaz	k1gInSc6
přeplňování	přeplňování	k1gNnSc2
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
054	#num#	k4
V10	V10	k1gFnSc2
–	–	k?
motor	motor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
používalo	používat	k5eAaImAgNnS
Ferrari	ferrari	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
vývoje	vývoj	k1gInSc2
motorů	motor	k1gInPc2
často	často	k6eAd1
experimentuje	experimentovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
statistiky	statistika	k1gFnPc1
jsou	být	k5eAaImIp3nP
přesné	přesný	k2eAgFnPc1d1
a	a	k8xC
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
nejúspěšnějšími	úspěšný	k2eAgInPc7d3
motory	motor	k1gInPc7
jsou	být	k5eAaImIp3nP
Renault	renault	k1gInSc4
a	a	k8xC
Ferrari	Ferrari	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Renault	renault	k1gInSc1
získal	získat	k5eAaPmAgInS
jako	jako	k9
dodavatel	dodavatel	k1gMnSc1
motorů	motor	k1gInPc2
pro	pro	k7c4
jiné	jiný	k2eAgFnPc4d1
stáje	stáj	k1gFnPc4
v	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
celkem	celkem	k6eAd1
šestkrát	šestkrát	k6eAd1
pohár	pohár	k1gInSc4
konstruktérů	konstruktér	k1gMnPc2
a	a	k8xC
pětkrát	pětkrát	k6eAd1
dovezl	dovézt	k5eAaPmAgMnS
pilota	pilot	k1gMnSc4
k	k	k7c3
titulu	titul	k1gInSc3
<g/>
;	;	kIx,
navíc	navíc	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
již	již	k6eAd1
s	s	k7c7
vlastním	vlastní	k2eAgInSc7d1
vozem	vůz	k1gInSc7
získal	získat	k5eAaPmAgInS
jak	jak	k8xS,k8xC
titul	titul	k1gInSc1
mezi	mezi	k7c7
jezdci	jezdec	k1gInPc7
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
pohár	pohár	k1gInSc4
konstruktérů	konstruktér	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Renault	renault	k1gInSc1
šel	jít	k5eAaImAgInS
ve	v	k7c6
vývoji	vývoj	k1gInSc6
ještě	ještě	k6eAd1
dále	daleko	k6eAd2
a	a	k8xC
nasadil	nasadit	k5eAaPmAgMnS
desetiválcový	desetiválcový	k2eAgInSc4d1
motor	motor	k1gInSc4
s	s	k7c7
úhlem	úhel	k1gInSc7
rozevření	rozevření	k1gNnPc2
111	#num#	k4
<g/>
°	°	k?
ve	v	k7c6
snaze	snaha	k1gFnSc6
snížit	snížit	k5eAaPmF
těžiště	těžiště	k1gNnSc4
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
upravený	upravený	k2eAgInSc1d1
motor	motor	k1gInSc1
používal	používat	k5eAaImAgInS
ve	v	k7c6
voze	vůz	k1gInSc6
R23	R23	k1gFnSc2
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2003	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gMnSc1
hlavní	hlavní	k2eAgMnSc1d1
soupeř	soupeř	k1gMnSc1
Ferrari	Ferrari	k1gMnSc1
byl	být	k5eAaImAgMnS
úspěšnější	úspěšný	k2eAgMnSc1d2
s	s	k7c7
klasickým	klasický	k2eAgNnSc7d1
rozevřením	rozevření	k1gNnSc7
válců	válec	k1gInPc2
90	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Renault	renault	k1gInSc1
se	se	k3xPyFc4
proto	proto	k8xC
v	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
vrátil	vrátit	k5eAaPmAgInS
k	k	k7c3
tradičnějšímu	tradiční	k2eAgNnSc3d2
rozevření	rozevření	k1gNnSc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
s	s	k7c7
úhlem	úhel	k1gInSc7
72	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferrari	ferrari	k1gNnSc1
slavilo	slavit	k5eAaImAgNnS
úspěch	úspěch	k1gInSc4
především	především	k9
v	v	k7c6
letech	let	k1gInPc6
1999	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
získalo	získat	k5eAaPmAgNnS
šest	šest	k4xCc1
pohárů	pohár	k1gInPc2
konstruktérů	konstruktér	k1gMnPc2
a	a	k8xC
pět	pět	k4xCc4
titulů	titul	k1gInPc2
mezi	mezi	k7c7
jezdci	jezdec	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezóna	sezóna	k1gFnSc1
2005	#num#	k4
byla	být	k5eAaImAgFnS
také	také	k9
koncem	koncem	k7c2
éry	éra	k1gFnSc2
desetiválcových	desetiválcový	k2eAgInPc2d1
motorů	motor	k1gInPc2
v	v	k7c6
F	F	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Desetiválcové	desetiválcový	k2eAgInPc1d1
motory	motor	k1gInPc1
jsou	být	k5eAaImIp3nP
nejpoužívanější	používaný	k2eAgFnSc7d3
konfigurací	konfigurace	k1gFnSc7
od	od	k7c2
zákazu	zákaz	k1gInSc2
motorů	motor	k1gInPc2
s	s	k7c7
turbo	turba	k1gFnSc5
přeplňováním	přeplňování	k1gNnPc3
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
sezóny	sezóna	k1gFnSc2
2006	#num#	k4
většina	většina	k1gFnSc1
týmů	tým	k1gInPc2
nastoupila	nastoupit	k5eAaPmAgFnS
s	s	k7c7
osmiválcovými	osmiválcový	k2eAgInPc7d1
motory	motor	k1gInPc7
podle	podle	k7c2
nových	nový	k2eAgFnPc2d1
regulí	regule	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimku	výjimek	k1gInSc2
získal	získat	k5eAaPmAgInS
pouze	pouze	k6eAd1
tým	tým	k1gInSc1
Minardi	Minard	k1gMnPc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
nesehnal	sehnat	k5eNaPmAgInS
dostatek	dostatek	k1gInSc1
finančních	finanční	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
na	na	k7c4
pořízení	pořízení	k1gNnSc4
motorů	motor	k1gInPc2
V	v	k7c4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
mohl	moct	k5eAaImAgInS
používat	používat	k5eAaImF
omezené	omezený	k2eAgInPc4d1
motory	motor	k1gInPc4
V	V	kA
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Honda	Honda	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
představila	představit	k5eAaPmAgFnS
jako	jako	k9
nový	nový	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
odkoupila	odkoupit	k5eAaPmAgFnS
akcie	akcie	k1gFnSc1
stáje	stáj	k1gFnSc2
BAR	bar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peter	Peter	k1gMnSc1
Sauber	Sauber	k1gMnSc1
prodal	prodat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
stáj	stáj	k1gFnSc4
německému	německý	k2eAgInSc3d1
koncernu	koncern	k1gInSc3
BMW	BMW	kA
a	a	k8xC
stáj	stáj	k1gFnSc1
Jordan	Jordan	k1gMnSc1
koupil	koupit	k5eAaPmAgMnS
ruský	ruský	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
Alex	Alex	k1gMnSc1
Shnaider	Shnaider	k1gMnSc1
a	a	k8xC
tým	tým	k1gInSc1
přejmenoval	přejmenovat	k5eAaPmAgInS
na	na	k7c4
Midland	Midland	k1gInSc4
(	(	kIx(
<g/>
ještě	ještě	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
byl	být	k5eAaImAgInS
tým	tým	k1gInSc1
prodán	prodán	k2eAgInSc1d1
nizozemské	nizozemský	k2eAgFnSc3d1
firmě	firma	k1gFnSc3
Spyker	Spykra	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minardi	Minard	k1gMnPc1
získal	získat	k5eAaPmAgInS
Red	Red	k1gMnSc1
Bull	bulla	k1gFnPc2
Racing	Racing	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
prvním	první	k4xOgMnSc7
majitelem	majitel	k1gMnSc7
dvou	dva	k4xCgInPc6
stájí	stáj	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Aguri	Aguri	k6eAd1
Suzuki	suzuki	k1gNnSc1
<g/>
,	,	kIx,
japonský	japonský	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
F	F	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
koupil	koupit	k5eAaPmAgMnS
ve	v	k7c6
dražbě	dražba	k1gFnSc6
zbytky	zbytek	k1gInPc4
stáje	stáj	k1gFnSc2
Arrows	Arrowsa	k1gFnPc2
<g/>
,	,	kIx,
zkrachovalé	zkrachovalý	k2eAgInPc1d1
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
založil	založit	k5eAaPmAgMnS
stáj	stáj	k1gFnSc4
Super	super	k2eAgFnSc2d1
Aguri	Agur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgFnPc1d1
formace	formace	k1gFnPc1
přinesly	přinést	k5eAaPmAgFnP
i	i	k9
spoustu	spoustu	k6eAd1
dohadů	dohad	k1gInPc2
<g/>
,	,	kIx,
změn	změna	k1gFnPc2
a	a	k8xC
polemik	polemika	k1gFnPc2
především	především	k6eAd1
okolo	okolo	k7c2
používání	používání	k1gNnSc2
totožných	totožný	k2eAgNnPc2d1
šasi	šasi	k1gNnPc2
dvěma	dva	k4xCgInPc7
týmy	tým	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezóna	sezóna	k1gFnSc1
2007	#num#	k4
přinesla	přinést	k5eAaPmAgFnS
další	další	k2eAgFnPc4d1
změny	změna	k1gFnPc4
<g/>
,	,	kIx,
tou	ten	k3xDgFnSc7
největší	veliký	k2eAgFnSc7d3
bylo	být	k5eAaImAgNnS
zmrazení	zmrazení	k1gNnSc1
vývoje	vývoj	k1gInSc2
motorů	motor	k1gInPc2
–	–	k?
zákaz	zákaz	k1gInSc1
experimentů	experiment	k1gInPc2
se	s	k7c7
základními	základní	k2eAgInPc7d1
parametry	parametr	k1gInPc7
motoru	motor	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
definují	definovat	k5eAaBmIp3nP
odlitek	odlitek	k1gInSc4
bloku	blok	k1gInSc2
<g/>
,	,	kIx,
klikový	klikový	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
<g/>
,	,	kIx,
počet	počet	k1gInSc1
válců	válec	k1gInPc2
<g/>
,	,	kIx,
zdvihový	zdvihový	k2eAgInSc1d1
objem	objem	k1gInSc1
<g/>
,	,	kIx,
vrtání	vrtání	k1gNnSc1
<g/>
,	,	kIx,
zdvih	zdvih	k1gInSc1
atd.	atd.	kA
Naopak	naopak	k6eAd1
připouští	připouštět	k5eAaImIp3nS
se	se	k3xPyFc4
vývoj	vývoj	k1gInSc1
tvaru	tvar	k1gInSc2
spalovací	spalovací	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
,	,	kIx,
ventilových	ventilový	k2eAgInPc2d1
rozvodů	rozvod	k1gInPc2
<g/>
,	,	kIx,
sání	sání	k1gNnPc2
nebo	nebo	k8xC
trysek	tryska	k1gFnPc2
pro	pro	k7c4
vstřikování	vstřikování	k1gNnSc4
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rekordy	rekord	k1gInPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
se	se	k3xPyFc4
německý	německý	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
Michael	Michael	k1gMnSc1
Schumacher	Schumachra	k1gFnPc2
stal	stát	k5eAaPmAgMnS
rekordmanem	rekordman	k1gMnSc7
v	v	k7c6
počtu	počet	k1gInSc6
titulů	titul	k1gInPc2
mezi	mezi	k7c7
jezdci	jezdec	k1gInPc7
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rekord	rekord	k1gInSc4
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
dorvnán	dorvnat	k5eAaImNgMnS,k5eAaPmNgMnS
Lewisem	Lewis	k1gInSc7
Hamiltonem	Hamilton	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
Scuderria	Scuderrium	k1gNnSc2
Ferrari	ferrari	k1gNnPc7
získal	získat	k5eAaPmAgInS
nejvíce	nejvíce	k6eAd1,k6eAd3
pohárů	pohár	k1gInPc2
konstruktérů	konstruktér	k1gMnPc2
(	(	kIx(
<g/>
dohromady	dohromady	k6eAd1
16	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakušan	Rakušan	k1gMnSc1
Jochen	Jochen	k2eAgInSc1d1
Rindt	Rindt	k1gInSc1
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
šampiónem	šampión	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
in	in	k?
memoriam	memoriam	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2010	#num#	k4
se	se	k3xPyFc4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
stal	stát	k5eAaPmAgMnS
nejmladším	mladý	k2eAgMnSc7d3
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
F1	F1	k1gFnSc1
(	(	kIx(
<g/>
23	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
134	#num#	k4
dní	den	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vozy	vůz	k1gInPc1
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgInSc1d3
vůz	vůz	k1gInSc1
roku	rok	k1gInSc2
1951	#num#	k4
Alfa	alfa	k1gNnPc2
Romeo	Romeo	k1gMnSc1
159	#num#	k4
</s>
<s>
Moderní	moderní	k2eAgInPc1d1
vozy	vůz	k1gInPc1
F1	F1	k1gFnSc2
jsou	být	k5eAaImIp3nP
monoposty	monopost	k1gInPc4
s	s	k7c7
motorem	motor	k1gInSc7
uloženým	uložený	k2eAgInSc7d1
uprostřed	uprostřed	k7c2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skořepina	skořepina	k1gFnSc1
celého	celý	k2eAgInSc2d1
monopostu	monopost	k1gInSc2
je	být	k5eAaImIp3nS
konstruována	konstruovat	k5eAaImNgFnS
z	z	k7c2
kompozitu	kompozitum	k1gNnSc3
uhlíkových	uhlíkový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
docílilo	docílit	k5eAaPmAgNnS
nejnižší	nízký	k2eAgFnSc3d3
hmotnosti	hmotnost	k1gFnSc3
a	a	k8xC
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
vůz	vůz	k1gInSc4
vyvažovat	vyvažovat	k5eAaImF
posouváním	posouvání	k1gNnSc7
těžiště	těžiště	k1gNnSc2
a	a	k8xC
vyvažovacích	vyvažovací	k2eAgNnPc2d1
závaží	závaží	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
vozu	vůz	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
s	s	k7c7
pilotem	pilot	k1gMnSc7
a	a	k8xC
palivem	palivo	k1gNnSc7
<g/>
,	,	kIx,
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
po	po	k7c6
závodě	závod	k1gInSc6
méně	málo	k6eAd2
než	než	k8xS
620	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s>
Vozy	vůz	k1gInPc1
F1	F1	k1gFnSc2
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgFnPc1d1
projíždět	projíždět	k5eAaImF
zatáčky	zatáčka	k1gFnPc4
ve	v	k7c6
vysokých	vysoký	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
a	a	k8xC
to	ten	k3xDgNnSc1
díky	díky	k7c3
aerodynamickému	aerodynamický	k2eAgInSc3d1
přítlaku	přítlak	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aerodynamický	aerodynamický	k2eAgInSc1d1
přítlak	přítlak	k1gInSc1
funguje	fungovat	k5eAaImIp3nS
na	na	k7c6
principu	princip	k1gInSc6
obráceného	obrácený	k2eAgNnSc2d1
křídla	křídlo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
používají	používat	k5eAaImIp3nP
letadla	letadlo	k1gNnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
létat	létat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
šedesátých	šedesátý	k4xOgNnPc2
a	a	k8xC
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
na	na	k7c6
vozech	vůz	k1gInPc6
objevila	objevit	k5eAaPmAgNnP
křídla	křídlo	k1gNnPc4
s	s	k7c7
obráceným	obrácený	k2eAgInSc7d1
efektem	efekt	k1gInSc7
než	než	k8xS
u	u	k7c2
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
obrátí	obrátit	k5eAaPmIp3nS
i	i	k9
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tlačí	tlačit	k5eAaImIp3nS
letadlo	letadlo	k1gNnSc4
do	do	k7c2
vzduchu	vzduch	k1gInSc2
a	a	k8xC
bude	být	k5eAaImBp3nS
přitlačovat	přitlačovat	k5eAaImF
auto	auto	k1gNnSc4
k	k	k7c3
zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzduch	vzduch	k1gInSc1
proudící	proudící	k2eAgInSc4d1
pod	pod	k7c7
křídlem	křídlo	k1gNnSc7
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
rychlejší	rychlý	k2eAgFnPc1d2
než	než	k8xS
ten	ten	k3xDgInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
proudí	proudit	k5eAaImIp3nS,k5eAaPmIp3nS
nad	nad	k7c7
křídlem	křídlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
se	se	k3xPyFc4
toho	ten	k3xDgMnSc4
docílilo	docílit	k5eAaPmAgNnS
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
křídlo	křídlo	k1gNnSc1
zkonstruováno	zkonstruovat	k5eAaPmNgNnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
spodní	spodní	k2eAgFnSc1d1
část	část	k1gFnSc1
křídla	křídlo	k1gNnSc2
tvořila	tvořit	k5eAaImAgFnS
větší	veliký	k2eAgFnSc1d2
vzdálenost	vzdálenost	k1gFnSc1
než	než	k8xS
ta	ten	k3xDgFnSc1
horní	horní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Renault	renault	k1gInSc1
RS10	RS10	k1gMnSc2
jezdce	jezdec	k1gMnSc2
René	René	k1gMnSc1
Arnouxe	Arnouxe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Renault	renault	k1gInSc1
byl	být	k5eAaImAgInS
první	první	k4xOgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
přišel	přijít	k5eAaPmAgInS
s	s	k7c7
turbomotorem	turbomotor	k1gInSc7
</s>
<s>
Pneumatiky	pneumatika	k1gFnPc1
</s>
<s>
Dalším	další	k2eAgInSc7d1
důležitým	důležitý	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
pro	pro	k7c4
rychlý	rychlý	k2eAgInSc4d1
a	a	k8xC
plynulý	plynulý	k2eAgInSc4d1
průjezd	průjezd	k1gInSc4
zatáčkou	zatáčka	k1gFnSc7
jsou	být	k5eAaImIp3nP
pneumatiky	pneumatika	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
formuli	formule	k1gFnSc6
1	#num#	k4
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
nepoužívaly	používat	k5eNaImAgFnP
hladké	hladký	k2eAgFnPc1d1
pneumatiky	pneumatika	k1gFnPc1
„	„	k?
<g/>
slicky	slicka	k1gFnSc2
<g/>
“	“	k?
tak	tak	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
automobilových	automobilový	k2eAgFnPc6d1
sériích	série	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
pneumatiky	pneumatika	k1gFnPc1
s	s	k7c7
drážkami	drážka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
sezónu	sezóna	k1gFnSc4
2009	#num#	k4
však	však	k8xC
pravidla	pravidlo	k1gNnPc4
doznala	doznat	k5eAaPmAgFnS
mnoho	mnoho	k4c4
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
je	být	k5eAaImIp3nS
dovoleno	dovolen	k2eAgNnSc4d1
hladké	hladký	k2eAgFnPc4d1
pneumatiky	pneumatika	k1gFnPc4
opět	opět	k6eAd1
používat	používat	k5eAaImF
<g/>
.	.	kIx.
<g/>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
pneumatiky	pneumatika	k1gFnPc1
značky	značka	k1gFnSc2
Pirelli	Pirelle	k1gFnSc4
a	a	k8xC
to	ten	k3xDgNnSc4
v	v	k7c6
několika	několik	k4yIc2
sadách	sada	k1gFnPc6
(	(	kIx(
<g/>
specifikacích	specifikace	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
barevně	barevně	k6eAd1
označeny	označit	k5eAaPmNgInP
podle	podle	k7c2
své	svůj	k3xOyFgFnSc2
tvrdosti	tvrdost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soft	Soft	k?
-	-	kIx~
červená	červená	k1gFnSc1
<g/>
,	,	kIx,
Medium	medium	k1gNnSc1
-	-	kIx~
žlutá	žlutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Hard	Hard	k1gInSc1
-	-	kIx~
bílá	bílý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc7d1
výjimkou	výjimka	k1gFnSc7
pneumatik	pneumatika	k1gFnPc2
bez	bez	k7c2
vzorku	vzorek	k1gInSc2
<g/>
,	,	kIx,
takzvaných	takzvaný	k2eAgInPc2d1
"	"	kIx"
<g/>
slicků	slick	k1gInPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
pneumatiky	pneumatika	k1gFnPc1
do	do	k7c2
mokra	mokro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
intermediate	intermediat	k1gInSc5
pneumatiku	pneumatika	k1gFnSc4
(	(	kIx(
<g/>
zelená	zelený	k2eAgFnSc1d1
<g/>
)	)	kIx)
do	do	k7c2
přechodných	přechodný	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
(	(	kIx(
<g/>
mrholení	mrholení	k1gNnSc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
okruhu	okruh	k1gInSc2
mokrá	mokrat	k5eAaImIp3nS
<g/>
,	,	kIx,
část	část	k1gFnSc1
suchá	suchý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
pneumatiku	pneumatika	k1gFnSc4
wet	wet	k?
(	(	kIx(
<g/>
modrá	modrý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
do	do	k7c2
deště	dešť	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
zmíněné	zmíněný	k2eAgFnPc1d1
pneumatiky	pneumatika	k1gFnPc1
mají	mít	k5eAaImIp3nP
vzorek	vzorek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
karbonové	karbonový	k2eAgFnPc1d1
brzdy	brzda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
jednak	jednak	k8xC
lehčí	lehčit	k5eAaImIp3nP
a	a	k8xC
pak	pak	k6eAd1
účinnější	účinný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
tyto	tento	k3xDgFnPc1
komponenty	komponenta	k1gFnPc1
vhodně	vhodně	k6eAd1
kombinované	kombinovaný	k2eAgFnPc1d1
mají	mít	k5eAaImIp3nP
vliv	vliv	k1gInSc4
na	na	k7c4
jízdní	jízdní	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Motor	motor	k1gInSc1
</s>
<s>
Motory	motor	k1gInPc4
používané	používaný	k2eAgInPc4d1
v	v	k7c6
sezonách	sezona	k1gFnPc6
2006	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
jsou	být	k5eAaImIp3nP
osmiválcové	osmiválcový	k2eAgInPc1d1
s	s	k7c7
úhlem	úhel	k1gInSc7
rozevření	rozevření	k1gNnPc2
90	#num#	k4
<g/>
°	°	k?
o	o	k7c6
maximálním	maximální	k2eAgInSc6d1
objemu	objem	k1gInSc6
2,4	2,4	k4
l.	l.	k?
s	s	k7c7
výkonem	výkon	k1gInSc7
750	#num#	k4
koní	kůň	k1gMnPc2
při	při	k7c6
limitovaných	limitovaný	k2eAgInPc6d1
18	#num#	k4
000	#num#	k4
otáčkách	otáčka	k1gFnPc6
za	za	k7c4
minutu	minuta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palivo	palivo	k1gNnSc1
používané	používaný	k2eAgNnSc1d1
v	v	k7c6
motorech	motor	k1gInPc6
F1	F1	k1gFnSc2
je	být	k5eAaImIp3nS
svým	svůj	k3xOyFgNnSc7
složením	složení	k1gNnSc7
téměř	téměř	k6eAd1
totožné	totožný	k2eAgNnSc4d1
jako	jako	k8xC,k8xS
běžné	běžný	k2eAgNnSc4d1
palivo	palivo	k1gNnSc4
pro	pro	k7c4
komerční	komerční	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
předešlých	předešlý	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
hojně	hojně	k6eAd1
používaly	používat	k5eAaImAgFnP
nebezpečné	bezpečný	k2eNgFnPc1d1
směsi	směs	k1gFnPc1
na	na	k7c6
bázi	báze	k1gFnSc6
metanolu	metanol	k1gInSc2
nebo	nebo	k8xC
směs	směs	k1gFnSc4
benzínu	benzín	k1gInSc2
a	a	k8xC
toluenu	toluen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vysokootáčkovém	vysokootáčkový	k2eAgInSc6d1
motoru	motor	k1gInSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
speciální	speciální	k2eAgInSc1d1
olej	olej	k1gInSc1
s	s	k7c7
podobnou	podobný	k2eAgFnSc7d1
viskozitou	viskozita	k1gFnSc7
jakou	jaký	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
má	můj	k3xOp1gFnSc1
voda	voda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
nově	nově	k6eAd1
používají	používat	k5eAaImIp3nP
šestiválcové	šestiválcový	k2eAgInPc4d1
motory	motor	k1gInPc4
o	o	k7c6
objemu	objem	k1gInSc6
1,6	1,6	k4
l.	l.	k?
s	s	k7c7
turbodmychadlem	turbodmychadlo	k1gNnSc7
a	a	k8xC
ERS	ERS	kA
<g/>
.	.	kIx.
</s>
<s>
Převodovka	převodovka	k1gFnSc1
</s>
<s>
Převodovka	převodovka	k1gFnSc1
je	být	k5eAaImIp3nS
připevněna	připevnit	k5eAaPmNgFnS
k	k	k7c3
zadní	zadní	k2eAgFnSc3d1
části	část	k1gFnSc3
motoru	motor	k1gInSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
přenášet	přenášet	k5eAaImF
výkon	výkon	k1gInSc4
motoru	motor	k1gInSc2
na	na	k7c4
kola	kolo	k1gNnPc4
co	co	k9
nejhladším	hladký	k2eAgInSc7d3
a	a	k8xC
nejefektivnějším	efektivní	k2eAgInSc7d3
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInPc1d1
vozy	vůz	k1gInPc1
F1	F1	k1gFnSc2
mají	mít	k5eAaImIp3nP
osmirychlostní	osmirychlostní	k2eAgFnPc4d1
převodovky	převodovka	k1gFnPc4
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
byly	být	k5eAaImAgFnP
převodovky	převodovka	k1gFnPc1
sedmirychlostní	sedmirychlostní	k2eAgFnPc1d1
<g/>
)	)	kIx)
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
ovládání	ovládání	k1gNnSc1
je	být	k5eAaImIp3nS
polosamočinné	polosamočinný	k2eAgNnSc1d1
pomocí	pomocí	k7c2
elektrohydraulického	elektrohydraulický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravidla	pravidlo	k1gNnPc1
zakazují	zakazovat	k5eAaImIp3nP
samočinné	samočinný	k2eAgFnPc4d1
převodovky	převodovka	k1gFnPc4
i	i	k8xC
rychlé	rychlý	k2eAgInPc4d1
systémy	systém	k1gInPc4
dvojspojkových	dvojspojkový	k2eAgFnPc2d1
převodovek	převodovka	k1gFnPc2
DSG	DSG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změna	změna	k1gFnSc1
převodového	převodový	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
je	být	k5eAaImIp3nS
možná	možná	k9
jen	jen	k9
po	po	k7c6
přímém	přímý	k2eAgInSc6d1
pokynu	pokyn	k1gInSc6
od	od	k7c2
jezdce	jezdec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revolucí	revoluce	k1gFnPc2
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
třech	tři	k4xCgNnPc6
letech	léto	k1gNnPc6
je	být	k5eAaImIp3nS
používání	používání	k1gNnSc1
bezprodlevového	bezprodlevový	k2eAgNnSc2d1
řazení	řazení	k1gNnSc2
nebo	nebo	k8xC
také	také	k9
plynulého	plynulý	k2eAgNnSc2d1
řazení	řazení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejde	jít	k5eNaImIp3nS
ovšem	ovšem	k9
o	o	k7c4
plynulou	plynulý	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
převodových	převodový	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k9
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
u	u	k7c2
převodovek	převodovka	k1gFnPc2
s	s	k7c7
variátorem	variátor	k1gInSc7
(	(	kIx(
<g/>
VGT	VGT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
zakázané	zakázaný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
převodovky	převodovka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
změnu	změna	k1gFnSc4
rychlostního	rychlostní	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
spojení	spojení	k1gNnSc2
motoru	motor	k1gInSc2
s	s	k7c7
koly	kolo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovéto	takovýto	k3xDgNnSc1
řazení	řazení	k1gNnSc1
má	mít	k5eAaImIp3nS
výhodu	výhoda	k1gFnSc4
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
nezpůsobuje	způsobovat	k5eNaImIp3nS
rázy	ráz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
zatěžovaly	zatěžovat	k5eAaImAgFnP
jak	jak	k6eAd1
vůz	vůz	k1gInSc4
<g/>
,	,	kIx,
tak	tak	k9
jezdce	jezdec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
systém	systém	k1gInSc1
řazení	řazení	k1gNnSc2
musí	muset	k5eAaImIp3nS
pracovat	pracovat	k5eAaImF
s	s	k7c7
vyšším	vysoký	k2eAgNnSc7d2
mechanickým	mechanický	k2eAgNnSc7d1
namáháním	namáhání	k1gNnSc7
a	a	k8xC
větším	veliký	k2eAgNnSc7d2
namáháním	namáhání	k1gNnSc7
elektronických	elektronický	k2eAgInPc2d1
i	i	k8xC
hydraulických	hydraulický	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
brát	brát	k5eAaImF
v	v	k7c4
úvahu	úvaha	k1gFnSc4
i	i	k9
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
zlomku	zlomek	k1gInSc6
sekundy	sekunda	k1gFnSc2
jsou	být	k5eAaImIp3nP
vlastně	vlastně	k9
zařazeny	zařadit	k5eAaPmNgInP
dva	dva	k4xCgInPc1
rychlostní	rychlostní	k2eAgInPc1d1
stupně	stupeň	k1gInPc1
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
řazení	řazení	k1gNnSc2
stále	stále	k6eAd1
více	hodně	k6eAd2
osvědčuje	osvědčovat	k5eAaImIp3nS
a	a	k8xC
přináší	přinášet	k5eAaImIp3nS
úsporu	úspora	k1gFnSc4
až	až	k6eAd1
0,3	0,3	k4
<g/>
s	s	k7c7
na	na	k7c6
kolo	kolo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
monoposty	monopost	k1gInPc1
F1	F1	k1gFnSc2
mohou	moct	k5eAaImIp3nP
dosáhnout	dosáhnout	k5eAaPmF
rychlosti	rychlost	k1gFnPc4
přes	přes	k7c4
350	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
na	na	k7c6
okruhu	okruh	k1gInSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nejrychlejším	rychlý	k2eAgInSc7d3
okruhem	okruh	k1gInSc7
v	v	k7c6
celém	celý	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
je	být	k5eAaImIp3nS
Monza	Monza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgInPc6
měsících	měsíc	k1gInPc6
roku	rok	k1gInSc2
2006	#num#	k4
Honda	honda	k1gFnSc1
podrobila	podrobit	k5eAaPmAgFnS
svůj	svůj	k3xOyFgInSc4
monopost	monopost	k1gInSc4
rychlostnímu	rychlostní	k2eAgMnSc3d1
testu	test	k1gMnSc3
v	v	k7c6
Bonneville	Bonnevilla	k1gFnSc6
Speedway	Speedwaa	k1gFnSc2
a	a	k8xC
v	v	k7c6
Poušti	poušť	k1gFnSc6
Mojave	Mojav	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vozem	vůz	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
odpovídal	odpovídat	k5eAaImAgInS
pravidlům	pravidlo	k1gNnPc3
Formule	formule	k1gFnSc1
1	#num#	k4
<g/>
,	,	kIx,
překonali	překonat	k5eAaPmAgMnP
rychlost	rychlost	k1gFnSc4
416	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Ovšem	ovšem	k9
během	během	k7c2
závodů	závod	k1gInPc2
je	být	k5eAaImIp3nS
max	max	kA
<g/>
.	.	kIx.
rychlost	rychlost	k1gFnSc1
kolem	kolem	k7c2
340	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Velké	velký	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
</s>
<s>
Počet	počet	k1gInSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezónách	sezóna	k1gFnPc6
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
variabilní	variabilní	k2eAgInSc1d1
<g/>
,	,	kIx,
od	od	k7c2
7	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
až	až	k6eAd1
po	po	k7c4
19	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
budoucna	budoucno	k1gNnSc2
se	se	k3xPyFc4
plánuje	plánovat	k5eAaImIp3nS
až	až	k9
22	#num#	k4
závodů	závod	k1gInPc2
v	v	k7c6
jedné	jeden	k4xCgFnSc6
sezóně	sezóna	k1gFnSc6
<g/>
,	,	kIx,
i	i	k8xC
přesto	přesto	k8xC
měl	mít	k5eAaImAgMnS
fanoušek	fanoušek	k1gMnSc1
tohoto	tento	k3xDgInSc2
motoristického	motoristický	k2eAgInSc2d1
sportu	sport	k1gInSc2
v	v	k7c6
minulosti	minulost	k1gFnSc6
mnohem	mnohem	k6eAd1
víc	hodně	k6eAd2
příležitostí	příležitost	k1gFnSc7
spatřit	spatřit	k5eAaPmF
závodit	závodit	k5eAaImF
své	svůj	k3xOyFgInPc4
idoly	idol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
jen	jen	k9
7	#num#	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
se	s	k7c7
zvláštním	zvláštní	k2eAgInSc7d1
statutem	statut	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
umožňoval	umožňovat	k5eAaImAgInS
výsledek	výsledek	k1gInSc4
započítávat	započítávat	k5eAaImF
do	do	k7c2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
tom	ten	k3xDgInSc6
samém	samý	k3xTgInSc6
roce	rok	k1gInSc6
jelo	jet	k5eAaImAgNnS
dalších	další	k2eAgNnPc2d1
16	#num#	k4
závodů	závod	k1gInPc2
vypsaných	vypsaný	k2eAgInPc2d1
pro	pro	k7c4
formule	formule	k1gFnSc2
1	#num#	k4
a	a	k8xC
dalších	další	k2eAgInPc2d1
11	#num#	k4
závodů	závod	k1gInPc2
jihoamerické	jihoamerický	k2eAgFnSc2d1
série	série	k1gFnSc2
Formule	formule	k1gFnSc1
Libre	Libr	k1gMnSc5
<g/>
,	,	kIx,
kde	kde	k6eAd1
startovali	startovat	k5eAaBmAgMnP
i	i	k9
Alberto	Alberta	k1gFnSc5
Ascari	Ascar	k1gFnSc5
<g/>
,	,	kIx,
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Fangio	Fangio	k1gMnSc1
<g/>
,	,	kIx,
Giuseppe	Giusepp	k1gInSc5
Farina	Farin	k2eAgMnSc4d1
<g/>
,	,	kIx,
Luigi	Luige	k1gFnSc4
Fagioli	Fagiole	k1gFnSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
hvězdy	hvězda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
se	se	k3xPyFc4
kromě	kromě	k7c2
10	#num#	k4
závodů	závod	k1gInPc2
pro	pro	k7c4
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
jelo	jet	k5eAaImAgNnS
i	i	k9
34	#num#	k4
závodů	závod	k1gInPc2
mimo	mimo	k7c4
šampionát	šampionát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
začal	začít	k5eAaPmAgInS
zvyšovat	zvyšovat	k5eAaImF
počet	počet	k1gInSc4
GP	GP	kA
započítávaných	započítávaný	k2eAgInPc2d1
do	do	k7c2
MS	MS	kA
a	a	k8xC
opadával	opadávat	k5eAaImAgInS
počet	počet	k1gInSc1
závodů	závod	k1gInPc2
bez	bez	k7c2
statutu	statut	k1gInSc2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
ale	ale	k8xC
mistrovství	mistrovství	k1gNnSc1
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
Tasmánský	tasmánský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
jezdilo	jezdit	k5eAaImAgNnS
jen	jen	k9
v	v	k7c6
závodech	závod	k1gInPc6
se	s	k7c7
statutem	statut	k1gInSc7
MS	MS	kA
plus	plus	k1gInSc1
v	v	k7c6
závodu	závod	k1gInSc6
šampiónů	šampión	k1gMnPc2
v	v	k7c4
Brands	Brands	k1gInSc4
Hatch	Hatcha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jack	Jack	k1gInSc1
Brabham	Brabham	k1gInSc1
při	při	k7c6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Německa	Německo	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brabham	Brabham	k1gInSc1
byl	být	k5eAaImAgInS
jediným	jediný	k2eAgInSc7d1
pilotem	pilot	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
dokázal	dokázat	k5eAaPmAgInS
triumfovat	triumfovat	k5eAaBmF
na	na	k7c6
voze	vůz	k1gInSc6
vlastní	vlastní	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
</s>
<s>
Šest	šest	k4xCc1
ze	z	k7c2
sedmi	sedm	k4xCc2
závodů	závod	k1gInPc2
prvního	první	k4xOgInSc2
ročníku	ročník	k1gInSc2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
mimoevropským	mimoevropský	k2eAgInSc7d1
závodem	závod	k1gInSc7
bylo	být	k5eAaImAgNnS
500	#num#	k4
mil	míle	k1gFnPc2
v	v	k7c6
Indianapolis	Indianapolis	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
zařazeny	zařadit	k5eAaPmNgInP
do	do	k7c2
kalendáře	kalendář	k1gInSc2
v	v	k7c6
prvním	první	k4xOgNnSc6
desetiletí	desetiletí	k1gNnSc6
MS	MS	kA
a	a	k8xC
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
jakýmsi	jakýsi	k3yIgInSc7
mostem	most	k1gInSc7
mezi	mezi	k7c7
dvěma	dva	k4xCgNnPc7
naprosto	naprosto	k6eAd1
odlišnými	odlišný	k2eAgInPc7d1
světy	svět	k1gInPc7
s	s	k7c7
různou	různý	k2eAgFnSc7d1
mentalitou	mentalita	k1gFnSc7
a	a	k8xC
také	také	k9
s	s	k7c7
odlišným	odlišný	k2eAgNnSc7d1
technickým	technický	k2eAgNnSc7d1
vybavením	vybavení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
zájem	zájem	k1gInSc1
amerických	americký	k2eAgMnPc2d1
pilotů	pilot	k1gMnPc2
o	o	k7c4
start	start	k1gInSc4
v	v	k7c6
evropských	evropský	k2eAgFnPc6d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
byl	být	k5eAaImAgMnS
nulový	nulový	k2eAgMnSc1d1
a	a	k8xC
pokusy	pokus	k1gInPc1
Evropanů	Evropan	k1gMnPc2
o	o	k7c4
500	#num#	k4
mil	míle	k1gFnPc2
byly	být	k5eAaImAgFnP
sporadické	sporadický	k2eAgFnPc1d1
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
do	do	k7c2
kalendáře	kalendář	k1gInSc2
zařazena	zařadit	k5eAaPmNgFnS
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
společně	společně	k6eAd1
se	s	k7c7
závodem	závod	k1gInSc7
500	#num#	k4
mil	míle	k1gFnPc2
v	v	k7c6
Indianapolis	Indianapolis	k1gFnSc6
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
tento	tento	k3xDgInSc1
pokus	pokus	k1gInSc1
nevyprovokoval	vyprovokovat	k5eNaPmAgInS
u	u	k7c2
Američanů	Američan	k1gMnPc2
zájem	zájem	k1gInSc1
o	o	k7c6
formuli	formule	k1gFnSc6
1	#num#	k4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
jezdí	jezdit	k5eAaImIp3nS
Grand	grand	k1gMnSc1
prix	prix	k1gInSc1
USA	USA	kA
jen	jen	k9
pro	pro	k7c4
vozy	vůz	k1gInPc4
F	F	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argentina	Argentina	k1gFnSc1
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc1
hostila	hostit	k5eAaImAgFnS
závody	závod	k1gInPc1
formule	formule	k1gFnSc1
1	#num#	k4
na	na	k7c6
jihoamerickém	jihoamerický	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
<g/>
,	,	kIx,
Maroko	Maroko	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
pak	pak	k8xC
první	první	k4xOgInSc4
na	na	k7c6
africké	africký	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
závodem	závod	k1gInSc7
v	v	k7c6
Asii	Asie	k1gFnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Japonska	Japonsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
a	a	k8xC
do	do	k7c2
Oceánie	Oceánie	k1gFnSc2
zavítal	zavítat	k5eAaPmAgInS
kolotoč	kolotoč	k1gInSc1
formule	formule	k1gFnSc2
1	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
australskou	australský	k2eAgFnSc4d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
17	#num#	k4
GP	GP	kA
roku	rok	k1gInSc2
2007	#num#	k4
je	být	k5eAaImIp3nS
zastoupena	zastoupen	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
i	i	k8xC
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
Oceánie	Oceánie	k1gFnSc1
<g/>
,	,	kIx,
schází	scházet	k5eAaImIp3nS
jen	jen	k9
Afrika	Afrika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nový	nový	k2eAgInSc1d1
okruh	okruh	k1gInSc1
v	v	k7c6
Soči	Soči	k1gNnSc6
</s>
<s>
Každá	každý	k3xTgFnSc1
země	země	k1gFnSc1
může	moct	k5eAaImIp3nS
uspořádat	uspořádat	k5eAaPmF
pod	pod	k7c7
svým	svůj	k3xOyFgNnSc7
jménem	jméno	k1gNnSc7
jen	jen	k9
jednu	jeden	k4xCgFnSc4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
v	v	k7c6
jedné	jeden	k4xCgFnSc6
zemi	zem	k1gFnSc6
koná	konat	k5eAaImIp3nS
více	hodně	k6eAd2
závodů	závod	k1gInPc2
(	(	kIx(
<g/>
Itálie	Itálie	k1gFnSc1
a	a	k8xC
Německo	Německo	k1gNnSc1
po	po	k7c6
dvou	dva	k4xCgFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
druhá	druhý	k4xOgFnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
pojmenována	pojmenován	k2eAgNnPc4d1
odlišně	odlišně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
Itálii	Itálie	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
Monze	Monza	k1gFnSc6
jezdila	jezdit	k5eAaImAgFnS
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Itálie	Itálie	k1gFnSc2
a	a	k8xC
v	v	k7c6
Imole	Imol	k1gInSc6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
San	San	k1gMnSc1
Marina	Marina	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
Německu	Německo	k1gNnSc6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Německa	Německo	k1gNnSc2
a	a	k8xC
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monza	Monza	k1gFnSc1
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgInSc7d1
okruhem	okruh	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
hostí	hostit	k5eAaImIp3nS
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Itálie	Itálie	k1gFnSc2
od	od	k7c2
samého	samý	k3xTgInSc2
začátku	začátek	k1gInSc2
a	a	k8xC
v	v	k7c6
kalendáři	kalendář	k1gInSc6
chyběl	chybět	k5eAaImAgInS
jen	jen	k9
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Itálie	Itálie	k1gFnSc2
uskutečnila	uskutečnit	k5eAaPmAgFnS
na	na	k7c6
okruhu	okruh	k1gInSc6
v	v	k7c6
Imole	Imola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Země	země	k1gFnSc1
označené	označený	k2eAgNnSc1d1
zelenou	zelený	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
hostí	hostit	k5eAaImIp3nP
velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
F	F	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tmavě	tmavě	k6eAd1
šedé	šedá	k1gFnSc6
označených	označený	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
se	se	k3xPyFc4
jely	jet	k5eAaImAgFnP
GP	GP	kA
v	v	k7c6
minulosti	minulost	k1gFnSc6
</s>
<s>
Současnou	současný	k2eAgFnSc4d1
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
snahou	snaha	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
jednotlivé	jednotlivý	k2eAgInPc1d1
státy	stát	k1gInPc1
pořádaly	pořádat	k5eAaImAgInP
jen	jen	k6eAd1
jednu	jeden	k4xCgFnSc4
velkou	velký	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
a	a	k8xC
pokud	pokud	k8xS
možno	možno	k6eAd1
střídaly	střídat	k5eAaImAgInP
různé	různý	k2eAgInPc1d1
okruhy	okruh	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Itálie	Itálie	k1gFnSc2
se	se	k3xPyFc4
proto	proto	k8xC
uskuteční	uskutečnit	k5eAaPmIp3nS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
v	v	k7c6
Monze	Monza	k1gFnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
by	by	kYmCp3nP
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
na	na	k7c6
zmodernizovaném	zmodernizovaný	k2eAgInSc6d1
okruhu	okruh	k1gInSc6
v	v	k7c6
Imole	Imola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nürburgring	Nürburgring	k1gInSc4
a	a	k8xC
Hockenheim	Hockenheim	k1gInSc4
budou	být	k5eAaImBp3nP
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
alternovat	alternovat	k5eAaImF
v	v	k7c4
pořádání	pořádání	k1gNnSc4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Bahrajnu	Bahrajn	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
prvním	první	k4xOgInSc7
závodem	závod	k1gInSc7
na	na	k7c6
Středním	střední	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Bahrajnu	Bahrajn	k1gInSc2
stejně	stejně	k6eAd1
jako	jako	k9
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Číny	Čína	k1gFnSc2
a	a	k8xC
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Turecka	Turecko	k1gNnSc2
prezentovaly	prezentovat	k5eAaBmAgFnP
nový	nový	k2eAgInSc4d1
směr	směr	k1gInSc4
vývoje	vývoj	k1gInSc2
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Soči	Soči	k1gNnSc6
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formule	formule	k1gFnSc1
1	#num#	k4
se	se	k3xPyFc4
také	také	k9
vrátila	vrátit	k5eAaPmAgFnS
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
na	na	k7c4
okruh	okruh	k1gInSc4
Red	Red	k1gFnSc2
Bull	bulla	k1gFnPc2
Ring	ring	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
známý	známý	k2eAgInSc1d1
jako	jako	k9
A	a	k9
<g/>
1	#num#	k4
<g/>
-Ring	-Ringa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
sezónu	sezóna	k1gFnSc4
2018	#num#	k4
se	se	k3xPyFc4
po	po	k7c6
deseti	deset	k4xCc6
letech	léto	k1gNnPc6
vrátila	vrátit	k5eAaPmAgFnS
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
jezdí	jezdit	k5eAaImIp3nS
na	na	k7c6
okruhu	okruh	k1gInSc6
Paul	Paul	k1gMnSc1
Ricard	Ricard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
a	a	k8xC
sponzorství	sponzorství	k1gNnSc1
</s>
<s>
Lotus	Lotus	kA
v	v	k7c6
barvách	barva	k1gFnPc6
svého	své	k1gNnSc2
sponzora	sponzor	k1gMnSc2
</s>
<s>
Před	před	k7c7
rokem	rok	k1gInSc7
1968	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
masivní	masivní	k2eAgInSc4d1
vstup	vstup	k1gInSc4
sponzorů	sponzor	k1gMnPc2
do	do	k7c2
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
závodily	závodit	k5eAaImAgInP
v	v	k7c6
oficiálních	oficiální	k2eAgFnPc6d1
národních	národní	k2eAgFnPc6d1
barvách	barva	k1gFnPc6
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gFnPc6
kapotách	kapota	k1gFnPc6
se	se	k3xPyFc4
sponzorské	sponzorský	k2eAgInPc1d1
nápisy	nápis	k1gInPc1
objevovaly	objevovat	k5eAaImAgInP
spíše	spíše	k9
sporadicky	sporadicky	k6eAd1
a	a	k8xC
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
především	především	k9
o	o	k7c4
dodavatele	dodavatel	k1gMnPc4
komponentů	komponent	k1gInPc2
(	(	kIx(
<g/>
pneumatiky	pneumatika	k1gFnPc1
<g/>
,	,	kIx,
palivo	palivo	k1gNnSc1
<g/>
,	,	kIx,
olej	olej	k1gInSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
přišel	přijít	k5eAaPmAgMnS
Lotus	Lotus	kA
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
první	první	k4xOgInSc1
tým	tým	k1gInSc1
v	v	k7c6
barvách	barva	k1gFnPc6
svého	své	k1gNnSc2
sponzora	sponzor	k1gMnSc2
(	(	kIx(
<g/>
červeno	červeno	k1gNnSc1
<g/>
,	,	kIx,
zlato	zlato	k1gNnSc1
<g/>
,	,	kIx,
bílý	bílý	k2eAgInSc1d1
<g/>
)	)	kIx)
„	„	k?
<g/>
Imperial	Imperial	k1gMnSc1
Tobacco	Tobacco	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
následně	následně	k6eAd1
pak	pak	k6eAd1
ve	v	k7c4
známé	známý	k2eAgNnSc4d1
zlato	zlato	k1gNnSc4
černé	černá	k1gFnSc2
kombinaci	kombinace	k1gFnSc4
<g/>
,	,	kIx,
toto	tento	k3xDgNnSc1
zbarvení	zbarvení	k1gNnSc1
tak	tak	k9
nahradilo	nahradit	k5eAaPmAgNnS
zelenou	zelený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
(	(	kIx(
<g/>
British	British	k1gInSc1
racing	racing	k1gInSc1
green	green	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
jezdily	jezdit	k5eAaImAgInP
britské	britský	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgInPc6d1
letech	let	k1gInPc6
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
jezdci	jezdec	k1gMnPc1
<g/>
,	,	kIx,
okruhy	okruh	k1gInPc1
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
názvy	název	k1gInPc1
jednotlivých	jednotlivý	k2eAgInPc2d1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
byly	být	k5eAaImAgFnP
ovlivněné	ovlivněný	k2eAgMnPc4d1
mohutným	mohutný	k2eAgInSc7d1
nástupem	nástup	k1gInSc7
reklamy	reklama	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
z	z	k7c2
oblasti	oblast	k1gFnSc2
tabákového	tabákový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přijímáním	přijímání	k1gNnSc7
legislativy	legislativa	k1gFnSc2
zakazující	zakazující	k2eAgFnSc2d1
propagaci	propagace	k1gFnSc4
kouření	kouření	k1gNnSc2
začínají	začínat	k5eAaImIp3nP
do	do	k7c2
formule	formule	k1gFnSc2
1	#num#	k4
proudit	proudit	k5eAaPmF,k5eAaImF
prostředky	prostředek	k1gInPc4
i	i	k9
z	z	k7c2
jiných	jiný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týmy	tým	k1gInPc7
pomalu	pomalu	k6eAd1
začínají	začínat	k5eAaImIp3nP
eliminovat	eliminovat	k5eAaBmF
nápisy	nápis	k1gInPc4
propagující	propagující	k2eAgInPc4d1
tabákové	tabákový	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
prvními	první	k4xOgInPc7
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
stáj	stáj	k1gFnSc1
Williams	Williamsa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
následována	následován	k2eAgFnSc1d1
vozy	vůz	k1gInPc4
McLaren	McLarno	k1gNnPc2
a	a	k8xC
Ferrari	ferrari	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
ve	v	k7c6
Formuli	formule	k1gFnSc6
1	#num#	k4
podporovat	podporovat	k5eAaImF
i	i	k9
jen	jen	k9
jednoho	jeden	k4xCgMnSc4
pilota	pilot	k1gMnSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
v	v	k7c6
případě	případ	k1gInSc6
italského	italský	k2eAgMnSc2d1
jezdce	jezdec	k1gMnSc2
Andrey	Andrea	k1gFnSc2
de	de	k?
Adamiche	Adamich	k1gMnSc2
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
podporovala	podporovat	k5eAaImAgFnS
malá	malý	k2eAgFnSc1d1
keramická	keramický	k2eAgFnSc1d1
dílna	dílna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnPc1
Ital	Ital	k1gMnSc1
<g/>
,	,	kIx,
Vittorio	Vittorio	k1gMnSc1
Brambilla	Brambilla	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
podporován	podporovat	k5eAaImNgInS
soukromou	soukromý	k2eAgFnSc7d1
firmou	firma	k1gFnSc7
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
rodiště	rodiště	k1gNnSc2
<g/>
,	,	kIx,
Monzy	Monza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Také	také	k9
Wilson	Wilson	k1gInSc1
Fittipaldi	Fittipald	k1gMnPc1
pojmenoval	pojmenovat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
tým	tým	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
ho	on	k3xPp3gMnSc4
v	v	k7c6
polovině	polovina	k1gFnSc6
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
založil	založit	k5eAaPmAgMnS
<g/>
,	,	kIx,
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
sponzorovi	sponzor	k1gMnSc6
Copersucar	Copersucar	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
brazilská	brazilský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
na	na	k7c4
výrobu	výroba	k1gFnSc4
cukru	cukr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
ještě	ještě	k6eAd1
začátkem	začátkem	k7c2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
několikanásobný	několikanásobný	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
Niki	Nik	k1gFnSc2
Lauda	Lauda	k1gMnSc1
zpopularizoval	zpopularizovat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
oblíbenou	oblíbený	k2eAgFnSc4d1
značku	značka	k1gFnSc4
mléka	mléko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Historie	historie	k1gFnSc1
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
jezdců	jezdec	k1gMnPc2
F1	F1	k1gFnSc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
konstruktérů	konstruktér	k1gMnPc2
F1	F1	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
jezdců	jezdec	k1gMnPc2
Formule	formule	k1gFnSc2
1	#num#	k4
</s>
<s>
Seznam	seznam	k1gInSc1
konstruktérů	konstruktér	k1gMnPc2
Formule	formule	k1gFnSc2
1	#num#	k4
</s>
<s>
Formule	formule	k1gFnSc1
1	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
</s>
<s>
Organizace	organizace	k1gFnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
automobilová	automobilový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
–	–	k?
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
organizuje	organizovat	k5eAaBmIp3nS
závody	závod	k1gInPc4
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidentem	prezident	k1gMnSc7
je	být	k5eAaImIp3nS
Jean	Jean	k1gMnSc1
Todt	Todt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Formula	Formula	k1gFnSc1
One	One	k1gFnSc2
Management	management	k1gInSc1
–	–	k?
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
prezidentem	prezident	k1gMnSc7
je	být	k5eAaImIp3nS
Bernie	Bernie	k1gFnSc1
Ecclestone	Eccleston	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
Manufacturers	Manufacturers	k1gInSc1
Association	Association	k1gInSc1
–	–	k?
asociace	asociace	k1gFnSc2
týmů	tým	k1gInPc2
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
květnu	květen	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc4
World	World	k1gMnSc1
Championship	Championship	k1gMnSc1
–	–	k?
alternativní	alternativní	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
nikdy	nikdy	k6eAd1
nezačalo	začít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Americké	americký	k2eAgFnPc1d1
série	série	k1gFnPc1
</s>
<s>
IndyCar	IndyCar	k1gMnSc1
Series	Series	k1gMnSc1
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
od	od	k7c2
1996	#num#	k4
</s>
<s>
Indy	Indus	k1gInPc1
Lights	Lights	k1gInSc1
–	–	k?
CART	CART	kA
American	American	k1gInSc1
Racing	Racing	k1gInSc1
Series	Series	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
CART	CART	kA
Indy	Indus	k1gInPc1
Lights	Lightsa	k1gFnPc2
Series	Seriesa	k1gFnPc2
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
IRL	IRL	kA
Pro	pro	k7c4
Series	Series	k1gInSc4
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
IRL	IRL	kA
Indy	Indus	k1gInPc7
Pro	pro	k7c4
Series	Series	k1gInSc4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
IRL	IRL	kA
Indy	Indus	k1gInPc7
Lights	Lightsa	k1gFnPc2
od	od	k7c2
2008	#num#	k4
po	po	k7c4
současnost	současnost	k1gFnSc4
</s>
<s>
Champ	Champ	k1gMnSc1
Car	car	k1gMnSc1
–	–	k?
CART	CART	kA
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Champ	Champ	k1gMnSc1
Car	car	k1gMnSc1
World	World	k1gMnSc1
Series	Series	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Předválečné	předválečný	k2eAgInPc1d1
závody	závod	k1gInPc1
</s>
<s>
Formule	formule	k1gFnSc1
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
od	od	k7c2
1921	#num#	k4
do	do	k7c2
1945	#num#	k4
</s>
<s>
Formule	formule	k1gFnSc1
Libre	Libr	k1gInSc5
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
od	od	k7c2
1928	#num#	k4
do	do	k7c2
poloviny	polovina	k1gFnSc2
padesátých	padesátý	k4xOgInPc2
let	léto	k1gNnPc2
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
série	série	k1gFnSc1
</s>
<s>
Formule	formule	k1gFnSc1
2	#num#	k4
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
od	od	k7c2
1948	#num#	k4
do	do	k7c2
1984	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
2009	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
</s>
<s>
Formule	formule	k1gFnSc1
3000	#num#	k4
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
od	od	k7c2
1985	#num#	k4
do	do	k7c2
2004	#num#	k4
</s>
<s>
GP2	GP2	k4
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
od	od	k7c2
2005	#num#	k4
</s>
<s>
GP3	GP3	k4
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
od	od	k7c2
2010	#num#	k4
</s>
<s>
A1	A1	k4
Grand	grand	k1gMnSc1
Prix	Prix	k1gInSc1
–	–	k?
Pohár	pohár	k1gInSc1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
2005	#num#	k4
do	do	k7c2
2009	#num#	k4
</s>
<s>
Formule	formule	k1gFnSc1
3	#num#	k4
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
od	od	k7c2
1950	#num#	k4
</s>
<s>
Formule	formule	k1gFnSc1
Junior	junior	k1gMnSc1
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
od	od	k7c2
1958	#num#	k4
do	do	k7c2
1964	#num#	k4
</s>
<s>
World	World	k6eAd1
Series	Series	k1gInSc1
by	by	k9
Renault	renault	k1gInSc1
–	–	k?
Formule	formule	k1gFnSc1
Nissan	nissan	k1gInSc1
2000	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
do	do	k7c2
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
World	World	k1gMnSc1
Series	Series	k1gMnSc1
by	by	kYmCp3nS
Nissan	nissan	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
World	World	k1gMnSc1
Series	Series	k1gMnSc1
by	by	kYmCp3nS
Renault	renault	k1gInSc4
od	od	k7c2
2005	#num#	k4
po	po	k7c4
současnost	současnost	k1gFnSc4
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xS,k8xC
Formule	formule	k1gFnSc1
Renault	renault	k1gInSc1
3.5	3.5	k4
</s>
<s>
Formule	formule	k1gFnSc1
Nippon	Nippona	k1gFnPc2
–	–	k?
mezinárodní	mezinárodní	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
Japonska	Japonsko	k1gNnSc2
pro	pro	k7c4
vozy	vůz	k1gInPc4
Formule	formule	k1gFnSc2
2000	#num#	k4
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Formule	formule	k1gFnSc1
2	#num#	k4
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Formule	formule	k1gFnSc1
3000	#num#	k4
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
od	od	k7c2
1996	#num#	k4
po	po	k7c4
současnost	současnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Eurocup	Eurocup	k1gMnSc1
Formule	formule	k1gFnSc2
Renault	renault	k1gInSc1
2.0	2.0	k4
–	–	k?
Rencontres	Rencontres	k1gMnSc1
Internationales	Internationales	k1gMnSc1
de	de	k?
Formule	formule	k1gFnSc1
Renault	renault	k1gInSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Eurocup	Eurocup	k1gMnSc1
Formule	formule	k1gFnSc2
Renault	renault	k1gInSc1
2.0	2.0	k4
od	od	k7c2
1993	#num#	k4
po	po	k7c4
současnost	současnost	k1gFnSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kříž	Kříž	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Seriál	seriál	k1gInSc1
<g/>
:	:	kIx,
Finanční	finanční	k2eAgNnSc1d1
ohlédnutí	ohlédnutí	k1gNnSc1
za	za	k7c4
F1	F1	k1gFnSc4
-	-	kIx~
V.	V.	kA
díl	díl	k1gInSc1
<g/>
,	,	kIx,
on-line	on-lin	k1gInSc5
text	text	k1gInSc1
<g/>
,	,	kIx,
dostupný	dostupný	k2eAgInSc1d1
z	z	k7c2
.	.	kIx.
</s>
<s desamb="1">
Ověřeno	ověřen	k2eAgNnSc1d1
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
B.	B.	kA
Hanzelka	Hanzelka	k1gMnSc1
:	:	kIx,
Vozy	vůz	k1gInPc1
velkých	velký	k2eAgFnPc2d1
cen	cena	k1gFnPc2
<g/>
,	,	kIx,
SNTL	SNTL	kA
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
↑	↑	k?
Formule	formule	k1gFnSc1
1	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
královský	královský	k2eAgInSc1d1
závod	závod	k1gInSc1
jel	jet	k5eAaImAgInS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Garáž	garáž	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Formule	formule	k1gFnSc2
1	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Formule	formule	k1gFnSc1
1	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Formula	Formula	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Současná	současný	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
na	na	k7c6
webu	web	k1gInSc6
FIA	FIA	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
grandprix	grandprix	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
–	–	k?
Zpravodajství	zpravodajství	k1gNnSc2
v	v	k7c6
angličtině	angličtina	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
johnnybet	johnnybet	k5eAaPmF,k5eAaImF,k5eAaBmF
<g/>
.	.	kIx.
<g/>
com	com	k?
-	-	kIx~
Kde	kde	k6eAd1
sledovat	sledovat	k5eAaImF
Formuli	formule	k1gFnSc4
1	#num#	k4
<g/>
?	?	kIx.
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
f	f	k?
<g/>
1	#num#	k4
<g/>
sports	sports	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Zpravodajství	zpravodajství	k1gNnSc2
o	o	k7c6
F1	F1	k1gFnSc6
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
gpf	gpf	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Zpravodajství	zpravodajství	k1gNnSc2
o	o	k7c6
F1	F1	k1gFnSc6
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
vozů	vůz	k1gInPc2
Formule	formule	k1gFnSc2
1	#num#	k4
</s>
<s>
1950	#num#	k4
•	•	k?
1951	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1953	#num#	k4
•	•	k?
1954	#num#	k4
•	•	k?
1955	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1957	#num#	k4
•	•	k?
1958	#num#	k4
•	•	k?
1959	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1961	#num#	k4
•	•	k?
1962	#num#	k4
•	•	k?
1963	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1965	#num#	k4
•	•	k?
1966	#num#	k4
•	•	k?
1967	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1969	#num#	k4
•	•	k?
1970	#num#	k4
•	•	k?
1971	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1973	#num#	k4
•	•	k?
1974	#num#	k4
•	•	k?
1975	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1976	#num#	k4
•	•	k?
1977	#num#	k4
•	•	k?
1978	#num#	k4
•	•	k?
1979	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1981	#num#	k4
•	•	k?
1982	#num#	k4
•	•	k?
1983	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1985	#num#	k4
•	•	k?
1986	#num#	k4
•	•	k?
1987	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1989	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1991	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1993	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1995	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
1997	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2001	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2003	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2006	#num#	k4
•	•	k?
2007	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2017	#num#	k4
•	•	k?
2018	#num#	k4
•	•	k?
2019	#num#	k4
•	•	k?
2020	#num#	k4
•	•	k?
2021	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20061004005	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4267570-4	4267570-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2012002495	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
245922962	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2012002495	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Formule	formule	k1gFnSc1
1	#num#	k4
</s>
