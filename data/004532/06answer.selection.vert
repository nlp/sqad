<s>
Počátky	počátek	k1gInPc1	počátek
vědecké	vědecký	k2eAgFnSc2d1	vědecká
psychologie	psychologie	k1gFnSc2	psychologie
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
datovány	datovat	k5eAaImNgInP	datovat
rokem	rok	k1gInSc7	rok
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Wundt	Wundt	k1gMnSc1	Wundt
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgFnSc4	první
psychologickou	psychologický	k2eAgFnSc4d1	psychologická
laboratoř	laboratoř	k1gFnSc4	laboratoř
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
