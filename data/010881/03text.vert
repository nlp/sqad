<p>
<s>
Air	Air	k?	Air
Marshal	Marshal	k1gMnSc1	Marshal
William	William	k1gInSc4	William
Avery	Avera	k1gFnPc4	Avera
"	"	kIx"	"
<g/>
Billy	Bill	k1gMnPc4	Bill
<g/>
"	"	kIx"	"
Bishop	Bishop	k1gInSc1	Bishop
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Owen	Owen	k1gInSc1	Owen
Sound	Sound	k1gInSc1	Sound
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Palm	Palm	k1gMnSc1	Palm
Beach	Beach	k1gMnSc1	Beach
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
stíhací	stíhací	k2eAgMnSc1d1	stíhací
pilot	pilot	k1gMnSc1	pilot
<g/>
,	,	kIx,	,
letecké	letecký	k2eAgNnSc4d1	letecké
eso	eso	k1gNnSc4	eso
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc4d1	oficiální
zdroje	zdroj	k1gInPc4	zdroj
mu	on	k3xPp3gMnSc3	on
připisují	připisovat	k5eAaImIp3nP	připisovat
72	[number]	k4	72
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
stíhacím	stíhací	k2eAgMnSc7d1	stíhací
pilotem	pilot	k1gMnSc7	pilot
celého	celý	k2eAgNnSc2d1	celé
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
po	po	k7c6	po
René	René	k1gFnSc6	René
Fonckovi	Foncek	k1gMnSc3	Foncek
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
pilotem	pilot	k1gInSc7	pilot
spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
</s>
</p>
<p>
<s>
Croix	Croix	k1gInSc1	Croix
de	de	k?	de
guerre	guerr	k1gMnSc5	guerr
</s>
</p>
<p>
<s>
Viktoriin	Viktoriin	k2eAgInSc1d1	Viktoriin
kříž	kříž	k1gInSc1	kříž
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
lázně	lázeň	k1gFnSc2	lázeň
</s>
</p>
<p>
<s>
DSO	DSO	kA	DSO
&	&	k?	&
Bar	bar	k1gInSc1	bar
</s>
</p>
<p>
<s>
Military	Militara	k1gFnPc1	Militara
Cross	Crossa	k1gFnPc2	Crossa
</s>
</p>
<p>
<s>
DFC	DFC	kA	DFC
</s>
</p>
<p>
<s>
Canadian	Canadian	k1gMnSc1	Canadian
Efficiency	Efficienca	k1gFnSc2	Efficienca
Decoration	Decoration	k1gInSc1	Decoration
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Baker	Baker	k1gMnSc1	Baker
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Avery	Avera	k1gFnPc4	Avera
"	"	kIx"	"
<g/>
Billy	Bill	k1gMnPc4	Bill
<g/>
"	"	kIx"	"
Bishop	Bishop	k1gInSc1	Bishop
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
and	and	k?	and
the	the	k?	the
Aircraft	Aircraft	k1gInSc1	Aircraft
He	he	k0	he
Flew	Flew	k1gFnSc4	Flew
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Outline	Outlin	k1gInSc5	Outlin
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
871547	[number]	k4	871547
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barker	Barker	k1gMnSc1	Barker
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gMnSc1	Ralph
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Royal	Royal	k1gInSc1	Royal
Flying	Flying	k1gInSc1	Flying
Corps	corps	k1gInSc1	corps
in	in	k?	in
World	World	k1gInSc1	World
War	War	k1gMnSc1	War
I.	I.	kA	I.
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Constable	Constable	k1gMnSc1	Constable
and	and	k?	and
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
84119	[number]	k4	84119
<g/>
-	-	kIx~	-
<g/>
470	[number]	k4	470
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bishop	Bishop	k1gInSc1	Bishop
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Avery	Avera	k1gFnSc2	Avera
<g/>
.	.	kIx.	.
</s>
<s>
Winged	Winged	k1gMnSc1	Winged
Warfare	Warfar	k1gMnSc5	Warfar
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Crécy	Créca	k1gFnPc1	Créca
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
originally	originalla	k1gFnSc2	originalla
published	published	k1gInSc1	published
in	in	k?	in
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
947554	[number]	k4	947554
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Buzzell	Buzzell	k1gInSc1	Buzzell
<g/>
,	,	kIx,	,
Nora	Nora	k1gFnSc1	Nora
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Register	registrum	k1gNnPc2	registrum
of	of	k?	of
the	the	k?	the
Victoria	Victorium	k1gNnSc2	Victorium
Cross	Crossa	k1gFnPc2	Crossa
Third	Thirda	k1gFnPc2	Thirda
Edition	Edition	k1gInSc1	Edition
<g/>
.	.	kIx.	.
</s>
<s>
Cheltenham	Cheltenham	k6eAd1	Cheltenham
<g/>
,	,	kIx,	,
Gloucestershire	Gloucestershir	k1gMnSc5	Gloucestershir
<g/>
,	,	kIx,	,
UK	UK	kA	UK
<g/>
:	:	kIx,	:
This	This	k1gInSc1	This
England	Englanda	k1gFnPc2	Englanda
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
906324	[number]	k4	906324
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Harvey	Harvea	k1gFnPc1	Harvea
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Monuments	Monuments	k6eAd1	Monuments
to	ten	k3xDgNnSc4	ten
Courage	Courage	k1gNnSc4	Courage
<g/>
.	.	kIx.	.
</s>
<s>
Uckfield	Uckfield	k1gMnSc1	Uckfield
<g/>
,	,	kIx,	,
UK	UK	kA	UK
<g/>
:	:	kIx,	:
Naval	navalit	k5eAaPmRp2nS	navalit
&	&	k?	&
Military	Militara	k1gFnSc2	Militara
Press	Pressa	k1gFnPc2	Pressa
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
84342	[number]	k4	84342
<g/>
-	-	kIx~	-
<g/>
356	[number]	k4	356
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
McCaffrey	McCaffrea	k1gFnPc1	McCaffrea
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
<g/>
.	.	kIx.	.
</s>
<s>
Billy	Bill	k1gMnPc4	Bill
Bishop	Bishop	k1gInSc1	Bishop
<g/>
:	:	kIx,	:
Canadian	Canadian	k1gMnSc1	Canadian
Hero	Hero	k1gMnSc1	Hero
<g/>
.	.	kIx.	.
</s>
<s>
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
:	:	kIx,	:
James	James	k1gMnSc1	James
Lorimer	Lorimer	k1gMnSc1	Lorimer
&	&	k?	&
Company	Compana	k1gFnSc2	Compana
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
55028	[number]	k4	55028
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
95	[number]	k4	95
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shores	Shores	k1gMnSc1	Shores
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
<g/>
,	,	kIx,	,
Franks	Franks	k1gInSc1	Franks
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
R.	R.	kA	R.
a	a	k8xC	a
Guest	Guest	k1gMnSc1	Guest
Russell	Russell	k1gMnSc1	Russell
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
Complete	Comple	k1gNnSc2	Comple
Record	Recorda	k1gFnPc2	Recorda
of	of	k?	of
the	the	k?	the
Fighter	fighter	k1gMnSc1	fighter
Aces	Aces	k1gInSc1	Aces
and	and	k?	and
Units	Units	k1gInSc1	Units
of	of	k?	of
the	the	k?	the
British	British	k1gInSc1	British
Empire	empir	k1gInSc5	empir
Air	Air	k1gFnPc7	Air
Forces	Forces	k1gMnSc1	Forces
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Grub	Grub	k1gMnSc1	Grub
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Urwin	Urwin	k1gInSc1	Urwin
<g/>
,	,	kIx,	,
Gregory	Gregor	k1gMnPc4	Gregor
J.W.	J.W.	k1gFnPc2	J.W.
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
Without	Without	k1gMnSc1	Without
Fear	Fear	k1gMnSc1	Fear
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Combat	Combat	k1gMnSc2	Combat
story	story	k1gFnSc2	story
of	of	k?	of
Lieutenant	Lieutenant	k1gMnSc1	Lieutenant
Colonel	Colonel	k1gMnSc1	Colonel
William	William	k1gInSc4	William
Avery	Avera	k1gFnSc2	Avera
Bishop	Bishop	k1gInSc1	Bishop
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Air	Air	k1gFnSc1	Air
Classics	Classics	k1gInSc1	Classics
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
,	,	kIx,	,
No	no	k9	no
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
,	,	kIx,	,
září	zářit	k5eAaImIp3nS	zářit
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Billy	Bill	k1gMnPc7	Bill
Bishop	Bishop	k1gInSc4	Bishop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Billy	Bill	k1gMnPc7	Bill
Bishop	Bishop	k1gInSc4	Bishop
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Billy	Bill	k1gMnPc4	Bill
Bishop	Bishop	k1gInSc1	Bishop
www.worldwar1.com	www.worldwar1.com	k1gInSc4	www.worldwar1.com
</s>
</p>
<p>
<s>
Billy	Bill	k1gMnPc7	Bill
Bishop	Bishop	k1gInSc4	Bishop
na	na	k7c4	na
www.theaerodrome.com	www.theaerodrome.com	k1gInSc4	www.theaerodrome.com
</s>
</p>
<p>
<s>
Billy	Bill	k1gMnPc4	Bill
Bishop	Bishop	k1gInSc1	Bishop
<g/>
,	,	kIx,	,
biografie	biografie	k1gFnSc1	biografie
</s>
</p>
<p>
<s>
www.billybishop.org	www.billybishop.org	k1gInSc1	www.billybishop.org
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
</s>
</p>
