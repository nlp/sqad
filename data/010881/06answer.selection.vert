<s>
Oficiální	oficiální	k2eAgInPc4d1	oficiální
zdroje	zdroj	k1gInPc4	zdroj
mu	on	k3xPp3gMnSc3	on
připisují	připisovat	k5eAaImIp3nP	připisovat
72	[number]	k4	72
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
stíhacím	stíhací	k2eAgMnSc7d1	stíhací
pilotem	pilot	k1gMnSc7	pilot
celého	celý	k2eAgNnSc2d1	celé
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
po	po	k7c6	po
René	René	k1gFnSc6	René
Fonckovi	Foncek	k1gMnSc3	Foncek
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
pilotem	pilot	k1gInSc7	pilot
spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
