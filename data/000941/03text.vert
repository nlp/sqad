<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1879	[number]	k4	1879
Ulm	Ulm	k1gFnPc2	Ulm
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1955	[number]	k4	1955
Princeton	Princeton	k1gInSc1	Princeton
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
vědců	vědec	k1gMnPc2	vědec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
vědce	vědec	k1gMnSc4	vědec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Newtonem	newton	k1gInSc7	newton
za	za	k7c4	za
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
fyzika	fyzik	k1gMnSc4	fyzik
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
příspěvky	příspěvek	k1gInPc4	příspěvek
fyzice	fyzika	k1gFnSc3	fyzika
patří	patřit	k5eAaImIp3nS	patřit
speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
myšlenka	myšlenka	k1gFnSc1	myšlenka
kvantování	kvantování	k1gNnSc2	kvantování
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
fotoefektu	fotoefekt	k1gInSc2	fotoefekt
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
Brownova	Brownův	k2eAgInSc2d1	Brownův
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
a	a	k8xC	a
snad	snad	k9	snad
nejvíce	nejvíce	k6eAd1	nejvíce
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
doposud	doposud	k6eAd1	doposud
nejlépe	dobře	k6eAd3	dobře
popisuje	popisovat	k5eAaImIp3nS	popisovat
vesmír	vesmír	k1gInSc1	vesmír
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
měřítkách	měřítko	k1gNnPc6	měřítko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
na	na	k7c6	na
statistické	statistický	k2eAgFnSc6d1	statistická
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc6d1	kvantová
statistice	statistika	k1gFnSc6	statistika
(	(	kIx(	(
<g/>
Boseho-Einsteinovo	Boseho-Einsteinův	k2eAgNnSc1d1	Boseho-Einsteinův
rozdělení	rozdělení	k1gNnSc1	rozdělení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diskusi	diskuse	k1gFnSc4	diskuse
o	o	k7c4	o
interpretaci	interpretace	k1gFnSc4	interpretace
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
(	(	kIx(	(
<g/>
diskuse	diskuse	k1gFnSc1	diskuse
s	s	k7c7	s
Bohrem	Bohro	k1gNnSc7	Bohro
<g/>
,	,	kIx,	,
EPR	EPR	kA	EPR
paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Leó	Leó	k1gMnSc7	Leó
Szilárdem	Szilárd	k1gInSc7	Szilárd
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
chladničky	chladnička	k1gFnSc2	chladnička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgMnS	být
Einstein	Einstein	k1gMnSc1	Einstein
oceněn	ocenit	k5eAaPmNgMnS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
"	"	kIx"	"
<g/>
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
fotoefektu	fotoefekt	k1gInSc2	fotoefekt
a	a	k8xC	a
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgFnPc1d1	významná
byly	být	k5eAaImAgFnP	být
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
jeho	jeho	k3xOp3gFnPc4	jeho
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
práce	práce	k1gFnPc4	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
řadě	řada	k1gFnSc6	řada
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
obecná	obecná	k1gFnSc1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
udělení	udělení	k1gNnSc6	udělení
ceny	cena	k1gFnSc2	cena
ještě	ještě	k6eAd1	ještě
nedoceněná	doceněný	k2eNgFnSc1d1	nedoceněná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zformuloval	zformulovat	k5eAaPmAgInS	zformulovat
obecnou	obecný	k2eAgFnSc4d1	obecná
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
známým	známý	k2eAgMnSc7d1	známý
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vědce	vědec	k1gMnPc4	vědec
nevídaný	vídaný	k2eNgInSc1d1	nevídaný
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
jeho	jeho	k3xOp3gMnPc2	jeho
sláva	sláva	k1gFnSc1	sláva
zastínila	zastínit	k5eAaPmAgFnS	zastínit
ostatní	ostatní	k1gNnSc4	ostatní
vědce	vědec	k1gMnSc2	vědec
a	a	k8xC	a
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc7d1	vysoká
inteligencí	inteligence	k1gFnSc7	inteligence
nebo	nebo	k8xC	nebo
zkrátka	zkrátka	k6eAd1	zkrátka
génia	génius	k1gMnSc4	génius
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvář	tvář	k1gFnSc1	tvář
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
ho	on	k3xPp3gMnSc4	on
časopis	časopis	k1gInSc1	časopis
Time	Time	k1gNnSc3	Time
vybral	vybrat	k5eAaPmAgInS	vybrat
jako	jako	k9	jako
Osobnost	osobnost	k1gFnSc4	osobnost
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
často	často	k6eAd1	často
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
jeho	jeho	k3xOp3gNnSc2	jeho
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
reklamách	reklama	k1gFnPc6	reklama
a	a	k8xC	a
obchodu	obchod	k1gInSc6	obchod
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
k	k	k7c3	k
registraci	registrace	k1gFnSc3	registrace
obchodní	obchodní	k2eAgFnSc2d1	obchodní
známky	známka	k1gFnSc2	známka
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
počest	počest	k1gFnPc6	počest
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenovány	pojmenovat	k5eAaPmNgFnP	pojmenovat
fotochemická	fotochemický	k2eAgFnSc1d1	fotochemická
jednotka	jednotka	k1gFnSc1	jednotka
einstein	einstein	k1gInSc4	einstein
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
einsteinium	einsteinium	k1gNnSc1	einsteinium
a	a	k8xC	a
planetka	planetka	k1gFnSc1	planetka
2001	[number]	k4	2001
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Raná	raný	k2eAgNnPc4d1	rané
léta	léto	k1gNnPc4	léto
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
univerzita	univerzita	k1gFnSc1	univerzita
===	===	k?	===
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
v	v	k7c6	v
Ulmu	Ulmus	k1gInSc6	Ulmus
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
Württembersku	Württembersko	k1gNnSc6	Württembersko
<g/>
,	,	kIx,	,
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Stuttgartu	Stuttgart	k1gInSc2	Stuttgart
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
byli	být	k5eAaImAgMnP	být
Hermann	Hermann	k1gMnSc1	Hermann
Einstein	Einstein	k1gMnSc1	Einstein
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
elektrochemik	elektrochemik	k1gMnSc1	elektrochemik
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Pauline	Paulin	k1gInSc5	Paulin
rozená	rozený	k2eAgFnSc1d1	rozená
Kochová	Kochová	k1gFnSc1	Kochová
(	(	kIx(	(
<g/>
nepřechýleně	přechýleně	k6eNd1	přechýleně
Koch	Koch	k1gMnSc1	Koch
<g/>
,	,	kIx,	,
1858	[number]	k4	1858
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
katolickou	katolický	k2eAgFnSc4d1	katolická
obecnou	obecný	k2eAgFnSc4d1	obecná
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
bral	brát	k5eAaImAgMnS	brát
hodiny	hodina	k1gFnPc4	hodina
houslí	housle	k1gFnPc2	housle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Albertovi	Albert	k1gMnSc3	Albert
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
kapesní	kapesní	k2eAgInSc4d1	kapesní
kompas	kompas	k1gInSc4	kompas
a	a	k8xC	a
Einstein	Einstein	k1gMnSc1	Einstein
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc4	něco
v	v	k7c6	v
"	"	kIx"	"
<g/>
prázdném	prázdný	k2eAgInSc6d1	prázdný
<g/>
"	"	kIx"	"
prostoru	prostor	k1gInSc6	prostor
musí	muset	k5eAaImIp3nS	muset
působit	působit	k5eAaImF	působit
na	na	k7c4	na
střelku	střelka	k1gFnSc4	střelka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tuto	tento	k3xDgFnSc4	tento
zkušenost	zkušenost	k1gFnSc4	zkušenost
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xC	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Stavěl	stavět	k5eAaImAgMnS	stavět
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
modely	model	k1gInPc1	model
a	a	k8xC	a
mechanická	mechanický	k2eAgNnPc1d1	mechanické
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
otcova	otcův	k2eAgFnSc1d1	otcova
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Einstein	Einstein	k1gMnSc1	Einstein
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
z	z	k7c2	z
Mnichova	Mnichov	k1gInSc2	Mnichov
do	do	k7c2	do
Pavie	Pavie	k1gFnSc2	Pavie
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
na	na	k7c4	na
přijímací	přijímací	k2eAgFnPc4d1	přijímací
zkoušky	zkouška	k1gFnPc4	zkouška
na	na	k7c4	na
Spolkovou	spolkový	k2eAgFnSc4d1	spolková
vysokou	vysoký	k2eAgFnSc4d1	vysoká
technickou	technický	k2eAgFnSc4d1	technická
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
Eidgenössische	Eidgenössische	k1gFnSc1	Eidgenössische
Technische	Technische	k1gFnSc1	Technische
Hochschule	Hochschule	k1gFnSc1	Hochschule
<g/>
,	,	kIx,	,
ETH	ETH	kA	ETH
<g/>
)	)	kIx)	)
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zkouškách	zkouška	k1gFnPc6	zkouška
z	z	k7c2	z
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
matematiky	matematika	k1gFnSc2	matematika
sice	sice	k8xC	sice
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
na	na	k7c4	na
výbornou	výborná	k1gFnSc4	výborná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezvládl	zvládnout	k5eNaPmAgMnS	zvládnout
obecnou	obecný	k2eAgFnSc4d1	obecná
část	část	k1gFnSc4	část
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
dokončit	dokončit	k5eAaPmF	dokončit
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
přihlásit	přihlásit	k5eAaPmF	přihlásit
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
proto	proto	k8xC	proto
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Kantonální	kantonální	k2eAgFnSc6d1	kantonální
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Aarau	Aaraus	k1gInSc6	Aaraus
a	a	k8xC	a
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
s	s	k7c7	s
výborným	výborný	k2eAgInSc7d1	výborný
prospěchem	prospěch	k1gInSc7	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
pokusu	pokus	k1gInSc6	pokus
již	již	k6eAd1	již
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
curyšskou	curyšský	k2eAgFnSc4d1	curyšská
Polytechniku	polytechnika	k1gFnSc4	polytechnika
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
zřekl	zřeknout	k5eAaPmAgInS	zřeknout
německého	německý	k2eAgNnSc2d1	německé
občanství	občanství	k1gNnSc2	občanství
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
bez	bez	k7c2	bez
státní	státní	k2eAgFnSc2d1	státní
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ETH	ETH	kA	ETH
dokončil	dokončit	k5eAaPmAgInS	dokončit
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
se	se	k3xPyFc4	se
Einstein	Einstein	k1gMnSc1	Einstein
stal	stát	k5eAaPmAgMnS	stát
švýcarským	švýcarský	k2eAgMnSc7d1	švýcarský
občanem	občan	k1gMnSc7	občan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
práce	práce	k1gFnSc1	práce
a	a	k8xC	a
doktorát	doktorát	k1gInSc1	doktorát
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zakončení	zakončení	k1gNnSc6	zakončení
studií	studie	k1gFnPc2	studie
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
nemohl	moct	k5eNaImAgMnS	moct
Einstein	Einstein	k1gMnSc1	Einstein
najít	najít	k5eAaPmF	najít
žádné	žádný	k3yNgNnSc4	žádný
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
učitelské	učitelský	k2eAgNnSc4d1	učitelské
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
mladická	mladický	k2eAgFnSc1d1	mladická
drzost	drzost	k1gFnSc1	drzost
rozčilovala	rozčilovat	k5eAaImAgFnS	rozčilovat
většinu	většina	k1gFnSc4	většina
jeho	jeho	k3xOp3gMnPc2	jeho
profesorů	profesor	k1gMnPc2	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
spolustudentů	spolustudent	k1gMnPc2	spolustudent
mu	on	k3xPp3gNnSc3	on
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
pomohl	pomoct	k5eAaPmAgMnS	pomoct
získat	získat	k5eAaPmF	získat
místo	místo	k1gNnSc4	místo
technického	technický	k2eAgMnSc2d1	technický
asistenta	asistent	k1gMnSc2	asistent
na	na	k7c6	na
Švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
patentovém	patentový	k2eAgInSc6d1	patentový
úřadě	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
tam	tam	k6eAd1	tam
posuzoval	posuzovat	k5eAaImAgMnS	posuzovat
význam	význam	k1gInSc4	význam
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
přihlášených	přihlášený	k2eAgInPc2d1	přihlášený
k	k	k7c3	k
udělení	udělení	k1gNnSc3	udělení
patentů	patent	k1gInPc2	patent
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgMnPc6	který
byla	být	k5eAaImAgFnS	být
nutná	nutný	k2eAgFnSc1d1	nutná
znalost	znalost	k1gFnSc1	znalost
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
učil	učit	k5eAaImAgInS	učit
rozeznávat	rozeznávat	k5eAaImF	rozeznávat
samotnou	samotný	k2eAgFnSc4d1	samotná
podstatu	podstata	k1gFnSc4	podstata
přihlášek	přihláška	k1gFnPc2	přihláška
navzdory	navzdory	k7c3	navzdory
jejich	jejich	k3xOp3gInSc3	jejich
občasnému	občasný	k2eAgInSc3d1	občasný
nedostatečnému	dostatečný	k2eNgInSc3d1	nedostatečný
popisu	popis	k1gInSc3	popis
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgNnSc6	tento
posuzování	posuzování	k1gNnSc6	posuzování
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
opravoval	opravovat	k5eAaImAgMnS	opravovat
chyby	chyba	k1gFnPc4	chyba
v	v	k7c6	v
návrzích	návrh	k1gInPc6	návrh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
získal	získat	k5eAaPmAgMnS	získat
Einstein	Einstein	k1gMnSc1	Einstein
na	na	k7c6	na
patentovém	patentový	k2eAgInSc6d1	patentový
úřadě	úřad	k1gInSc6	úřad
stálé	stálý	k2eAgNnSc4d1	stálé
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
získal	získat	k5eAaPmAgMnS	získat
Einstein	Einstein	k1gMnSc1	Einstein
na	na	k7c6	na
Spolkové	spolkový	k2eAgFnSc6d1	spolková
vysoké	vysoký	k2eAgFnSc6d1	vysoká
technické	technický	k2eAgFnSc6d1	technická
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
doktorát	doktorát	k1gInSc4	doktorát
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
"	"	kIx"	"
<g/>
O	o	k7c6	o
novém	nový	k2eAgNnSc6d1	nové
určení	určení	k1gNnSc6	určení
molekulárních	molekulární	k2eAgInPc2d1	molekulární
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
napsal	napsat	k5eAaBmAgMnS	napsat
čtyři	čtyři	k4xCgInPc4	čtyři
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
moderní	moderní	k2eAgFnSc2d1	moderní
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
odkazoval	odkazovat	k5eAaImAgInS	odkazovat
na	na	k7c4	na
odbornou	odborný	k2eAgFnSc4d1	odborná
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
své	svůj	k3xOyFgFnPc4	svůj
teorie	teorie	k1gFnPc4	teorie
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
s	s	k7c7	s
vědeckými	vědecký	k2eAgMnPc7d1	vědecký
kolegy	kolega	k1gMnPc7	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Témata	téma	k1gNnPc4	téma
těchto	tento	k3xDgInPc2	tento
článků	článek	k1gInPc2	článek
byla	být	k5eAaImAgFnS	být
Brownův	Brownův	k2eAgInSc4d1	Brownův
pohyb	pohyb	k1gInSc4	pohyb
<g/>
,	,	kIx,	,
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnPc4d1	speciální
teorie	teorie	k1gFnPc4	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
ekvivalence	ekvivalence	k1gFnSc2	ekvivalence
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
získal	získat	k5eAaPmAgMnS	získat
Einstein	Einstein	k1gMnSc1	Einstein
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
kousek	kousek	k1gInSc1	kousek
ironie	ironie	k1gFnSc2	ironie
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Einstein	Einstein	k1gMnSc1	Einstein
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
mnohem	mnohem	k6eAd1	mnohem
známější	známý	k2eAgFnSc1d2	známější
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
je	být	k5eAaImIp3nS	být
záležitost	záležitost	k1gFnSc1	záležitost
kvantové	kvantový	k2eAgFnSc2d1	kvantová
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
Einstein	Einstein	k1gMnSc1	Einstein
poněkud	poněkud	k6eAd1	poněkud
rozčarovaný	rozčarovaný	k2eAgMnSc1d1	rozčarovaný
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
články	článek	k1gInPc1	článek
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
přesto	přesto	k6eAd1	přesto
hodny	hoden	k2eAgInPc1d1	hoden
zaznamenání	zaznamenání	k1gNnPc4	zaznamenání
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Einstein	Einstein	k1gMnSc1	Einstein
převzal	převzít	k5eAaPmAgMnS	převzít
odvážně	odvážně	k6eAd1	odvážně
myšlenky	myšlenka	k1gFnPc4	myšlenka
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
dovedl	dovést	k5eAaPmAgMnS	dovést
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jejich	jejich	k3xOp3gInPc2	jejich
logických	logický	k2eAgInPc2d1	logický
důsledků	důsledek	k1gInPc2	důsledek
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
výsledky	výsledek	k1gInPc4	výsledek
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
vědce	vědec	k1gMnSc2	vědec
zneklidňovaly	zneklidňovat	k5eAaImAgFnP	zneklidňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
články	článek	k1gInPc1	článek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
odeslal	odeslat	k5eAaPmAgMnS	odeslat
Einstein	Einstein	k1gMnSc1	Einstein
do	do	k7c2	do
odborného	odborný	k2eAgInSc2d1	odborný
časopisu	časopis	k1gInSc2	časopis
Annalen	Annalen	k2eAgInSc1d1	Annalen
der	drát	k5eAaImRp2nS	drát
Physik	Physik	k1gInSc4	Physik
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nazýván	nazývat	k5eAaImNgInS	nazývat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
zázračný	zázračný	k2eAgInSc1d1	zázračný
rok	rok	k1gInSc1	rok
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Annus	Annus	k1gInSc1	Annus
Mirabilis	Mirabilis	k1gFnSc2	Mirabilis
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
zázračný	zázračný	k2eAgInSc1d1	zázračný
rok	rok	k1gInSc1	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
unie	unie	k1gFnSc1	unie
pro	pro	k7c4	pro
čistou	čistý	k2eAgFnSc4d1	čistá
a	a	k8xC	a
aplikovanou	aplikovaný	k2eAgFnSc4d1	aplikovaná
fyziku	fyzika	k1gFnSc4	fyzika
(	(	kIx(	(
<g/>
IUPAP	IUPAP	kA	IUPAP
<g/>
)	)	kIx)	)
proto	proto	k8xC	proto
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
připomenutí	připomenutí	k1gNnSc4	připomenutí
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
rok	rok	k1gInSc1	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jejich	jejich	k3xOp3gNnSc1	jejich
sté	stý	k4xOgNnSc1	stý
výročí	výročí	k1gNnSc1	výročí
<g/>
,	,	kIx,	,
světovým	světový	k2eAgInSc7d1	světový
rokem	rok	k1gInSc7	rok
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Brownův	Brownův	k2eAgInSc1d1	Brownův
pohyb	pohyb	k1gInSc1	pohyb
====	====	k?	====
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
článek	článek	k1gInSc1	článek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
O	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
–	–	k?	–
potřebném	potřebné	k1gNnSc6	potřebné
pro	pro	k7c4	pro
molekulární	molekulární	k2eAgFnSc4d1	molekulární
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
teorii	teorie	k1gFnSc4	teorie
tepla	teplo	k1gNnSc2	teplo
–	–	k?	–
malých	malý	k2eAgFnPc2d1	malá
částic	částice	k1gFnPc2	částice
umístěných	umístěný	k2eAgFnPc2d1	umístěná
v	v	k7c6	v
klidné	klidný	k2eAgFnSc6d1	klidná
kapalině	kapalina	k1gFnSc6	kapalina
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
studiem	studio	k1gNnSc7	studio
Brownova	Brownův	k2eAgInSc2d1	Brownův
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
tehdy	tehdy	k6eAd1	tehdy
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
kinetické	kinetický	k2eAgFnSc2d1	kinetická
teorie	teorie	k1gFnSc2	teorie
tekutin	tekutina	k1gFnPc2	tekutina
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
odolával	odolávat	k5eAaImAgMnS	odolávat
uspokojujícímu	uspokojující	k2eAgNnSc3d1	uspokojující
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
empirický	empirický	k2eAgInSc1d1	empirický
důkaz	důkaz	k1gInSc1	důkaz
reality	realita	k1gFnSc2	realita
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Dodal	dodat	k5eAaPmAgMnS	dodat
také	také	k9	také
důvěryhodnost	důvěryhodnost	k1gFnSc4	důvěryhodnost
statistické	statistický	k2eAgFnSc3d1	statistická
mechanice	mechanika	k1gFnSc3	mechanika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
také	také	k9	také
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Než	než	k8xS	než
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
atomy	atom	k1gInPc1	atom
uznávány	uznáván	k2eAgInPc1d1	uznáván
jako	jako	k8xS	jako
užitečná	užitečný	k2eAgFnSc1d1	užitečná
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fyzikové	fyzik	k1gMnPc1	fyzik
a	a	k8xC	a
chemikové	chemik	k1gMnPc1	chemik
se	se	k3xPyFc4	se
vášnivě	vášnivě	k6eAd1	vášnivě
přeli	přít	k5eAaImAgMnP	přít
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vůbec	vůbec	k9	vůbec
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
statistický	statistický	k2eAgInSc1d1	statistický
popis	popis	k1gInSc1	popis
chování	chování	k1gNnSc2	chování
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
experimentátorům	experimentátor	k1gMnPc3	experimentátor
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
spočítat	spočítat	k5eAaPmF	spočítat
atomy	atom	k1gInPc4	atom
prostým	prostý	k2eAgInSc7d1	prostý
pohledem	pohled	k1gInSc7	pohled
do	do	k7c2	do
obyčejného	obyčejný	k2eAgInSc2d1	obyčejný
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Ostwald	Ostwald	k1gMnSc1	Ostwald
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
odpůrců	odpůrce	k1gMnPc2	odpůrce
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
Arnoldu	Arnold	k1gMnSc3	Arnold
Sommerfeldovi	Sommerfeld	k1gMnSc3	Sommerfeld
<g/>
,	,	kIx,	,
že	že	k8xS	že
změnil	změnit	k5eAaPmAgInS	změnit
názor	názor	k1gInSc4	názor
právě	právě	k9	právě
díky	díky	k7c3	díky
Einsteinově	Einsteinův	k2eAgFnSc3d1	Einsteinova
kompletnímu	kompletní	k2eAgNnSc3d1	kompletní
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
Brownova	Brownův	k2eAgInSc2d1	Brownův
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
====	====	k?	====
</s>
</p>
<p>
<s>
Druhý	druhý	k4xOgInSc1	druhý
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
"	"	kIx"	"
<g/>
O	o	k7c6	o
heuristickém	heuristický	k2eAgNnSc6d1	heuristické
hledisku	hledisko	k1gNnSc6	hledisko
dotýkajícím	dotýkající	k2eAgNnSc6d1	dotýkající
se	se	k3xPyFc4	se
vznikem	vznik	k1gInSc7	vznik
a	a	k8xC	a
přeměnou	přeměna	k1gFnSc7	přeměna
světla	světlo	k1gNnSc2	světlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
myšlenku	myšlenka	k1gFnSc4	myšlenka
světelných	světelný	k2eAgNnPc2d1	světelné
kvant	kvantum	k1gNnPc2	kvantum
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
fotony	foton	k1gInPc4	foton
<g/>
)	)	kIx)	)
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
takových	takový	k3xDgInPc2	takový
jevů	jev	k1gInPc2	jev
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
světelného	světelný	k2eAgNnSc2d1	světelné
kvanta	kvantum	k1gNnSc2	kvantum
přišla	přijít	k5eAaPmAgFnS	přijít
z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
odvození	odvození	k1gNnSc2	odvození
záření	záření	k1gNnSc2	záření
absolutně	absolutně	k6eAd1	absolutně
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
Maxem	Max	k1gMnSc7	Max
Planckem	Planck	k1gInSc7	Planck
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
energie	energie	k1gFnSc1	energie
záření	záření	k1gNnSc2	záření
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
nebo	nebo	k8xC	nebo
vyzářena	vyzářit	k5eAaPmNgFnS	vyzářit
jen	jen	k6eAd1	jen
po	po	k7c6	po
celých	celý	k2eAgFnPc6d1	celá
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgNnPc2d1	nazývané
kvanta	kvantum	k1gNnSc2	kvantum
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
vlastně	vlastně	k9	vlastně
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
balíčků	balíček	k1gInPc2	balíček
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
záhadný	záhadný	k2eAgInSc4d1	záhadný
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
vysvětlen	vysvětlen	k2eAgInSc4d1	vysvětlen
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Představa	představa	k1gFnSc1	představa
světla	světlo	k1gNnSc2	světlo
jako	jako	k8xS	jako
kvant	kvantum	k1gNnPc2	kvantum
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
teorií	teorie	k1gFnSc7	teorie
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přirozeně	přirozeně	k6eAd1	přirozeně
vyplývala	vyplývat	k5eAaImAgFnS	vyplývat
z	z	k7c2	z
Maxwellových	Maxwellův	k2eAgFnPc2d1	Maxwellova
rovnic	rovnice	k1gFnPc2	rovnice
pro	pro	k7c4	pro
elektromagnetismus	elektromagnetismus	k1gInSc4	elektromagnetismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
představou	představa	k1gFnSc7	představa
o	o	k7c6	o
nekonečné	konečný	k2eNgFnSc6d1	nekonečná
dělitelnosti	dělitelnost	k1gFnSc6	dělitelnost
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
fyzikálních	fyzikální	k2eAgInPc6d1	fyzikální
systémech	systém	k1gInPc6	systém
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
experimenty	experiment	k1gInPc1	experiment
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Einsteinovy	Einsteinův	k2eAgFnPc1d1	Einsteinova
rovnice	rovnice	k1gFnPc1	rovnice
pro	pro	k7c4	pro
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
byly	být	k5eAaImAgInP	být
přesné	přesný	k2eAgInPc1d1	přesný
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
nebylo	být	k5eNaImAgNnS	být
běžně	běžně	k6eAd1	běžně
uznáváno	uznáván	k2eAgNnSc1d1	uznáváno
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
fotoelektrickém	fotoelektrický	k2eAgInSc6d1	fotoelektrický
jevu	jev	k1gInSc6	jev
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
většina	většina	k1gFnSc1	většina
fyziků	fyzik	k1gMnPc2	fyzik
už	už	k6eAd1	už
myslela	myslet	k5eAaImAgFnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
rovnice	rovnice	k1gFnSc1	rovnice
(	(	kIx(	(
<g/>
hf	hf	k?	hf
=	=	kIx~	=
Wv	Wv	k1gMnSc1	Wv
+	+	kIx~	+
Ek	Ek	k1gMnSc1	Ek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
h	h	k?	h
je	být	k5eAaImIp3nS	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
f	f	k?	f
frekvence	frekvence	k1gFnPc4	frekvence
dopadajícího	dopadající	k2eAgInSc2d1	dopadající
fotonu	foton	k1gInSc2	foton
<g/>
,	,	kIx,	,
Wv	Wv	k1gFnSc2	Wv
výstupní	výstupní	k2eAgFnSc2d1	výstupní
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
Ek	Ek	k1gFnSc7	Ek
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
vyraženého	vyražený	k2eAgInSc2d1	vyražený
elektronu	elektron	k1gInSc2	elektron
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
správná	správný	k2eAgFnSc1d1	správná
a	a	k8xC	a
světelná	světelný	k2eAgNnPc1d1	světelné
kvanta	kvantum	k1gNnPc1	kvantum
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
světelného	světelný	k2eAgNnSc2d1	světelné
kvanta	kvantum	k1gNnSc2	kvantum
byla	být	k5eAaImAgFnS	být
předzvěstí	předzvěst	k1gFnSc7	předzvěst
vlnově-částicové	vlnově-částicový	k2eAgFnPc1d1	vlnově-částicový
duality	dualita	k1gFnPc1	dualita
<g/>
,	,	kIx,	,
představy	představa	k1gFnPc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
systémy	systém	k1gInPc1	systém
mohou	moct	k5eAaImIp3nP	moct
vykazovat	vykazovat	k5eAaImF	vykazovat
jak	jak	k6eAd1	jak
vlnové	vlnový	k2eAgFnPc4d1	vlnová
<g/>
,	,	kIx,	,
tak	tak	k9	tak
částicové	částicový	k2eAgFnPc4d1	částicová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xC	jako
základní	základní	k2eAgInSc4d1	základní
princip	princip	k1gInSc4	princip
tvůrci	tvůrce	k1gMnSc3	tvůrce
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
úplně	úplně	k6eAd1	úplně
vysvětlený	vysvětlený	k2eAgInSc1d1	vysvětlený
až	až	k9	až
po	po	k7c6	po
dozrání	dozrání	k1gNnSc6	dozrání
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Speciální	speciální	k2eAgFnSc1d1	speciální
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
====	====	k?	====
</s>
</p>
<p>
<s>
Einsteinův	Einsteinův	k2eAgInSc4d1	Einsteinův
třetí	třetí	k4xOgInSc4	třetí
článek	článek	k1gInSc4	článek
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
"	"	kIx"	"
<g/>
O	o	k7c6	o
elektrodynamice	elektrodynamika	k1gFnSc6	elektrodynamika
pohybujících	pohybující	k2eAgNnPc2d1	pohybující
se	se	k3xPyFc4	se
těles	těleso	k1gNnPc2	těleso
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Einstein	Einstein	k1gMnSc1	Einstein
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgInS	napsat
Milevě	Mileva	k1gFnSc3	Mileva
o	o	k7c6	o
"	"	kIx"	"
<g/>
naší	náš	k3xOp1gFnSc3	náš
práci	práce	k1gFnSc3	práce
o	o	k7c6	o
relativitě	relativita	k1gFnSc6	relativita
pohybu	pohyb	k1gInSc2	pohyb
<g/>
"	"	kIx"	"
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
úvahám	úvaha	k1gFnPc3	úvaha
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Mileva	Mileva	k1gFnSc1	Mileva
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
práci	práce	k1gFnSc6	práce
podíl	podíl	k1gInSc4	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
představil	představit	k5eAaPmAgInS	představit
speciální	speciální	k2eAgFnSc4d1	speciální
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
teorii	teorie	k1gFnSc4	teorie
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
<g/>
,	,	kIx,	,
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
konzistentní	konzistentní	k2eAgInSc4d1	konzistentní
s	s	k7c7	s
elektromagnetismem	elektromagnetismus	k1gInSc7	elektromagnetismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vynechávala	vynechávat	k5eAaImAgFnS	vynechávat
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
relativita	relativita	k1gFnSc1	relativita
složila	složit	k5eAaPmAgFnS	složit
skládanku	skládanka	k1gFnSc4	skládanka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
Michelsonova-Morleyova	Michelsonova-Morleyův	k2eAgInSc2d1	Michelsonova-Morleyův
experimentu	experiment	k1gInSc2	experiment
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
světelné	světelný	k2eAgFnPc1d1	světelná
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
šířit	šířit	k5eAaImF	šířit
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
za	za	k7c2	za
jaké	jaký	k3yIgFnSc2	jaký
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
éter	éter	k1gInSc1	éter
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
ne	ne	k9	ne
relativní	relativní	k2eAgNnSc1d1	relativní
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
newtonovské	newtonovský	k2eAgFnSc6d1	newtonovská
fyzice	fyzika	k1gFnSc6	fyzika
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
se	se	k3xPyFc4	se
George	George	k1gInSc1	George
Fitzgerald	Fitzgeralda	k1gFnPc2	Fitzgeralda
domníval	domnívat	k5eAaImAgInS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledky	výsledek	k1gInPc4	výsledek
Michelsonova-Morleyova	Michelsonova-Morleyův	k2eAgInSc2d1	Michelsonova-Morleyův
experimentu	experiment	k1gInSc2	experiment
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vysvětleny	vysvětlit	k5eAaPmNgFnP	vysvětlit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
pohybující	pohybující	k2eAgFnPc1d1	pohybující
se	se	k3xPyFc4	se
objekty	objekt	k1gInPc1	objekt
zkráceny	zkrácen	k2eAgInPc1d1	zkrácen
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jejich	jejich	k3xOp3gInSc2	jejich
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rovnic	rovnice	k1gFnPc2	rovnice
Einsteinova	Einsteinův	k2eAgInSc2d1	Einsteinův
článku	článek	k1gInSc2	článek
–	–	k?	–
Lorentzovy	Lorentzův	k2eAgFnSc2d1	Lorentzova
transformace	transformace	k1gFnSc2	transformace
–	–	k?	–
byly	být	k5eAaImAgFnP	být
světu	svět	k1gInSc3	svět
představeny	představit	k5eAaPmNgFnP	představit
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
holandským	holandský	k2eAgMnSc7d1	holandský
fyzikem	fyzik	k1gMnSc7	fyzik
Hendrikem	Hendrik	k1gMnSc7	Hendrik
Lorentzem	Lorentz	k1gMnSc7	Lorentz
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dal	dát	k5eAaPmAgInS	dát
Fitzgeraldovým	Fitzgeraldův	k2eAgNnSc7d1	Fitzgeraldův
domněnkám	domněnka	k1gFnPc3	domněnka
matematickou	matematický	k2eAgFnSc4d1	matematická
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Einstein	Einstein	k1gMnSc1	Einstein
odhalil	odhalit	k5eAaPmAgMnS	odhalit
podstatu	podstata	k1gFnSc4	podstata
této	tento	k3xDgFnSc2	tento
geometrické	geometrický	k2eAgFnSc2d1	geometrická
podivnosti	podivnost	k1gFnSc2	podivnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
pramenilo	pramenit	k5eAaImAgNnS	pramenit
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
axiomů	axiom	k1gInPc2	axiom
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
byla	být	k5eAaImAgFnS	být
stará	starý	k2eAgFnSc1d1	stará
Galileova	Galileův	k2eAgFnSc1d1	Galileova
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
přírodní	přírodní	k2eAgInPc1d1	přírodní
zákony	zákon	k1gInPc1	zákon
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
stejné	stejný	k2eAgFnPc1d1	stejná
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
konstantní	konstantní	k2eAgFnSc7d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
byl	být	k5eAaImAgInS	být
princip	princip	k1gInSc1	princip
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
relativita	relativita	k1gFnSc1	relativita
měla	mít	k5eAaImAgFnS	mít
několik	několik	k4yIc4	několik
revolučních	revoluční	k2eAgInPc2d1	revoluční
důsledků	důsledek	k1gInPc2	důsledek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc6	on
padla	padnout	k5eAaImAgFnS	padnout
představa	představa	k1gFnSc1	představa
absolutního	absolutní	k2eAgInSc2d1	absolutní
času	čas	k1gInSc2	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
nazvána	nazván	k2eAgFnSc1d1	nazvána
speciální	speciální	k2eAgFnSc1d1	speciální
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odlišila	odlišit	k5eAaPmAgFnS	odlišit
od	od	k7c2	od
její	její	k3xOp3gFnSc2	její
následovnice	následovnice	k1gFnSc2	následovnice
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
sobě	se	k3xPyFc3	se
rovné	rovný	k2eAgMnPc4d1	rovný
i	i	k8xC	i
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
zrychlují	zrychlovat	k5eAaImIp3nP	zrychlovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
doslova	doslova	k6eAd1	doslova
oplývá	oplývat	k5eAaImIp3nS	oplývat
paradoxy	paradox	k1gInPc4	paradox
a	a	k8xC	a
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dává	dávat	k5eAaImIp3nS	dávat
jen	jen	k9	jen
málo	málo	k1gNnSc1	málo
smyslu	smysl	k1gInSc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Mohla	moct	k5eAaImAgFnS	moct
Einsteina	Einstein	k1gMnSc4	Einstein
skutečně	skutečně	k6eAd1	skutečně
zesměšnit	zesměšnit	k5eAaPmF	zesměšnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
jejími	její	k3xOp3gInPc7	její
zdánlivými	zdánlivý	k2eAgInPc7d1	zdánlivý
protiklady	protiklad	k1gInPc7	protiklad
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
vyřešit	vyřešit	k5eAaPmF	vyřešit
její	její	k3xOp3gInPc4	její
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Ekvivalence	ekvivalence	k1gFnSc2	ekvivalence
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
====	====	k?	====
</s>
</p>
<p>
<s>
Čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
"	"	kIx"	"
<g/>
Závisí	záviset	k5eAaImIp3nS	záviset
setrvačnost	setrvačnost	k1gFnSc4	setrvačnost
tělesa	těleso	k1gNnSc2	těleso
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
energii	energie	k1gFnSc4	energie
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
představil	představit	k5eAaPmAgInS	představit
další	další	k2eAgNnSc4d1	další
odvození	odvození	k1gNnSc4	odvození
z	z	k7c2	z
relativistických	relativistický	k2eAgInPc2d1	relativistický
axiomů	axiom	k1gInPc2	axiom
–	–	k?	–
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
energií	energie	k1gFnSc7	energie
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
původně	původně	k6eAd1	původně
zapsal	zapsat	k5eAaPmAgMnS	zapsat
jako	jako	k8xC	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
=	=	kIx~	=
<g/>
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
odvození	odvození	k1gNnSc1	odvození
přepíše	přepsat	k5eAaPmIp3nS	přepsat
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
notoricky	notoricky	k6eAd1	notoricky
známá	známý	k2eAgFnSc1d1	známá
rovnice	rovnice	k1gFnSc1	rovnice
</s>
</p>
<p>
<s>
E	E	kA	E
=	=	kIx~	=
mc2	mc2	k4	mc2
<g/>
,	,	kIx,	,
že	že	k8xS	že
energie	energie	k1gFnSc1	energie
hmoty	hmota	k1gFnSc2	hmota
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
její	její	k3xOp3gFnSc3	její
hmotnosti	hmotnost	k1gFnSc3	hmotnost
vynásobené	vynásobený	k2eAgNnSc1d1	vynásobené
čtvercem	čtverec	k1gInSc7	čtverec
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
považoval	považovat	k5eAaImAgMnS	považovat
tuto	tento	k3xDgFnSc4	tento
rovnici	rovnice	k1gFnSc4	rovnice
za	za	k7c4	za
vrcholně	vrcholně	k6eAd1	vrcholně
důležitou	důležitý	k2eAgFnSc4d1	důležitá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžké	těžký	k2eAgFnPc1d1	těžká
částice	částice	k1gFnPc1	částice
mají	mít	k5eAaImIp3nP	mít
kromě	kromě	k7c2	kromě
kinetické	kinetický	k2eAgFnSc2d1	kinetická
a	a	k8xC	a
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
ještě	ještě	k9	ještě
zbytkovou	zbytkový	k2eAgFnSc4d1	zbytková
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
toto	tento	k3xDgNnSc4	tento
zjištění	zjištění	k1gNnSc4	zjištění
prostě	prostě	k6eAd1	prostě
odmítala	odmítat	k5eAaImAgFnS	odmítat
jako	jako	k9	jako
kuriozitu	kuriozita	k1gFnSc4	kuriozita
až	až	k9	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
hmotností	hmotnost	k1gFnSc7	hmotnost
a	a	k8xC	a
energií	energie	k1gFnSc7	energie
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
objasnění	objasnění	k1gNnSc3	objasnění
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jaderné	jaderný	k2eAgFnPc1d1	jaderná
zbraně	zbraň	k1gFnPc1	zbraň
mohou	moct	k5eAaImIp3nP	moct
produkovat	produkovat	k5eAaImF	produkovat
tak	tak	k6eAd1	tak
ohromné	ohromný	k2eAgNnSc1d1	ohromné
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Měřením	měření	k1gNnSc7	měření
hmotnosti	hmotnost	k1gFnSc2	hmotnost
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
vydělením	vydělení	k1gNnSc7	vydělení
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
snadno	snadno	k6eAd1	snadno
spočítat	spočítat	k5eAaPmF	spočítat
vazebná	vazebný	k2eAgFnSc1d1	vazebná
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
uvězněná	uvězněný	k2eAgFnSc1d1	uvězněná
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
atomových	atomový	k2eAgNnPc6d1	atomové
jádrech	jádro	k1gNnPc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
nám	my	k3xPp1nPc3	my
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vypočíst	vypočíst	k5eAaPmF	vypočíst
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc1	kolik
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
při	při	k7c6	při
přeměně	přeměna	k1gFnSc6	přeměna
jednoho	jeden	k4xCgNnSc2	jeden
jádra	jádro	k1gNnSc2	jádro
v	v	k7c4	v
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rozštěpení	rozštěpení	k1gNnSc2	rozštěpení
jádra	jádro	k1gNnSc2	jádro
uranu	uran	k1gInSc2	uran
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
ohromující	ohromující	k2eAgNnSc1d1	ohromující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Umberta	Umbert	k1gMnSc2	Umbert
Bartocciho	Bartocci	k1gMnSc2	Bartocci
<g/>
,	,	kIx,	,
historika	historik	k1gMnSc2	historik
matematiky	matematika	k1gFnSc2	matematika
z	z	k7c2	z
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Perugii	Perugie	k1gFnSc6	Perugie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
slavná	slavný	k2eAgFnSc1d1	slavná
rovnice	rovnice	k1gFnSc1	rovnice
poprvé	poprvé	k6eAd1	poprvé
publikována	publikovat	k5eAaBmNgFnS	publikovat
již	již	k9	již
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
před	před	k7c7	před
tím	ten	k3xDgMnSc7	ten
Olintem	Olint	k1gMnSc7	Olint
De	De	k?	De
Prettoem	Pretto	k1gMnSc7	Pretto
<g/>
,	,	kIx,	,
průmyslníkem	průmyslník	k1gMnSc7	průmyslník
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
Vicenzy	Vicenza	k1gFnSc2	Vicenza
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
není	být	k5eNaImIp3nS	být
obecně	obecně	k6eAd1	obecně
přijímáno	přijímat	k5eAaImNgNnS	přijímat
jako	jako	k8xC	jako
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
nebo	nebo	k8xC	nebo
důležité	důležitý	k2eAgFnPc1d1	důležitá
<g/>
,	,	kIx,	,
De	De	k?	De
Pretto	Pretto	k1gNnSc4	Pretto
mohl	moct	k5eAaImAgInS	moct
rovnici	rovnice	k1gFnSc4	rovnice
publikovat	publikovat	k5eAaBmF	publikovat
již	již	k6eAd1	již
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc4	ten
až	až	k6eAd1	až
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
ji	on	k3xPp3gFnSc4	on
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zralá	zralý	k2eAgNnPc4d1	zralé
léta	léto	k1gNnPc4	léto
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
byl	být	k5eAaImAgMnS	být
Einstein	Einstein	k1gMnSc1	Einstein
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
revizora	revizor	k1gMnSc4	revizor
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
bylo	být	k5eAaImAgNnS	být
Einsteinovi	Einstein	k1gMnSc3	Einstein
uděleno	udělen	k2eAgNnSc1d1	uděleno
oprávnění	oprávnění	k1gNnSc1	oprávnění
učit	učit	k5eAaImF	učit
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
jako	jako	k8xC	jako
soukromý	soukromý	k2eAgMnSc1d1	soukromý
docent	docent	k1gMnSc1	docent
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Einsteinův	Einsteinův	k2eAgMnSc1d1	Einsteinův
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Eduard	Eduard	k1gMnSc1	Eduard
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pražský	pražský	k2eAgInSc1d1	pražský
pobyt	pobyt	k1gInSc1	pobyt
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
se	se	k3xPyFc4	se
Einstein	Einstein	k1gMnSc1	Einstein
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
německé	německý	k2eAgFnSc6d1	německá
univerzitě	univerzita	k1gFnSc6	univerzita
–	–	k?	–
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
semestry	semestr	k1gInPc4	semestr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Marcelem	Marcel	k1gMnSc7	Marcel
Grossmannem	Grossmann	k1gMnSc7	Grossmann
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
bydlel	bydlet	k5eAaImAgMnS	bydlet
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
září	září	k1gNnSc2	září
1911	[number]	k4	1911
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Milevou	Mileva	k1gFnSc7	Mileva
v	v	k7c6	v
Třebízského	Třebízského	k2eAgFnSc6d1	Třebízského
ulici	ulice	k1gFnSc6	ulice
číslo	číslo	k1gNnSc1	číslo
1215	[number]	k4	1215
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Lesnická	lesnický	k2eAgFnSc1d1	lesnická
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Mileva	Mileva	k1gFnSc1	Mileva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nebyli	být	k5eNaImAgMnP	být
moc	moc	k6eAd1	moc
šťastní	šťastný	k2eAgMnPc1d1	šťastný
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
tu	tu	k6eAd1	tu
dlouho	dlouho	k6eAd1	dlouho
nepobyla	pobýt	k5eNaPmAgFnS	pobýt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1912	[number]	k4	1912
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
do	do	k7c2	do
Curychu	Curych	k1gInSc2	Curych
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
pražského	pražský	k2eAgInSc2d1	pražský
pobytu	pobyt	k1gInSc2	pobyt
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
salón	salón	k1gInSc4	salón
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
jednorožce	jednorožec	k1gMnSc2	jednorožec
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
začal	začít	k5eAaPmAgMnS	začít
Einstein	Einstein	k1gMnSc1	Einstein
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
času	čas	k1gInSc6	čas
jako	jako	k8xC	jako
o	o	k7c6	o
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
rozměru	rozměr	k1gInSc6	rozměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Berlínský	berlínský	k2eAgInSc4d1	berlínský
pobyt	pobyt	k1gInSc4	pobyt
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Einstein	Einstein	k1gMnSc1	Einstein
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
Pruské	pruský	k2eAgFnSc2d1	pruská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pacifismus	pacifismus	k1gInSc1	pacifismus
a	a	k8xC	a
židovský	židovský	k2eAgInSc4d1	židovský
původ	původ	k1gInSc4	původ
byl	být	k5eAaImAgMnS	být
trnem	trn	k1gInSc7	trn
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
německým	německý	k2eAgMnPc3d1	německý
nacionalistům	nacionalista	k1gMnPc3	nacionalista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mu	on	k3xPp3gNnSc3	on
navíc	navíc	k6eAd1	navíc
záviděli	závidět	k5eAaImAgMnP	závidět
jeho	jeho	k3xOp3gFnSc4	jeho
slávu	sláva	k1gFnSc4	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
osoba	osoba	k1gFnSc1	osoba
se	se	k3xPyFc4	se
tak	tak	k9	tak
poprvé	poprvé	k6eAd1	poprvé
stala	stát	k5eAaPmAgFnS	stát
terčem	terč	k1gInSc7	terč
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
zdiskreditovat	zdiskreditovat	k5eAaPmF	zdiskreditovat
jeho	jeho	k3xOp3gFnSc1	jeho
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1917	[number]	k4	1917
až	až	k9	až
1933	[number]	k4	1933
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
ústavu	ústav	k1gInSc2	ústav
císaře	císař	k1gMnSc2	císař
Viléma	Vilém	k1gMnSc2	Vilém
(	(	kIx(	(
<g/>
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
<g/>
)	)	kIx)	)
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
obdržel	obdržet	k5eAaPmAgInS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
učinil	učinit	k5eAaPmAgInS	učinit
objevy	objev	k1gInPc4	objev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nejvíce	hodně	k6eAd3	hodně
otřásly	otřást	k5eAaPmAgFnP	otřást
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1915	[number]	k4	1915
Einstein	Einstein	k1gMnSc1	Einstein
přednesl	přednést	k5eAaPmAgMnS	přednést
na	na	k7c6	na
Pruské	pruský	k2eAgFnSc6d1	pruská
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
sérii	série	k1gFnSc4	série
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
popsal	popsat	k5eAaPmAgMnS	popsat
svou	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
přednáška	přednáška	k1gFnSc1	přednáška
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
jeho	on	k3xPp3gMnSc4	on
rovnicemi	rovnice	k1gFnPc7	rovnice
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
zvanými	zvaný	k2eAgInPc7d1	zvaný
Einsteinovy	Einsteinův	k2eAgFnPc1d1	Einsteinova
rovnice	rovnice	k1gFnPc1	rovnice
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nahradily	nahradit	k5eAaPmAgFnP	nahradit
Newtonův	Newtonův	k2eAgInSc4d1	Newtonův
gravitační	gravitační	k2eAgInSc4d1	gravitační
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
sobě	se	k3xPyFc3	se
rovné	rovný	k2eAgFnPc4d1	rovná
všechny	všechen	k3xTgMnPc4	všechen
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jen	jen	k9	jen
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
přímočaře	přímočaro	k6eAd1	přímočaro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
relativitě	relativita	k1gFnSc6	relativita
už	už	k6eAd1	už
gravitace	gravitace	k1gFnSc2	gravitace
není	být	k5eNaImIp3nS	být
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Newtonově	Newtonův	k2eAgInSc6d1	Newtonův
gravitačním	gravitační	k2eAgInSc6d1	gravitační
zákoně	zákon	k1gInSc6	zákon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouhý	pouhý	k2eAgInSc1d1	pouhý
důsledek	důsledek	k1gInSc1	důsledek
zprohýbaného	zprohýbaný	k2eAgInSc2d1	zprohýbaný
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
kosmologie	kosmologie	k1gFnSc2	kosmologie
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
vědcům	vědec	k1gMnPc3	vědec
nástroje	nástroj	k1gInPc4	nástroj
k	k	k7c3	k
porozumění	porozumění	k1gNnSc3	porozumění
mnoha	mnoho	k4c2	mnoho
vlastností	vlastnost	k1gFnPc2	vlastnost
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
mnoho	mnoho	k6eAd1	mnoho
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
až	až	k9	až
po	po	k7c6	po
Einsteinově	Einsteinův	k2eAgFnSc6d1	Einsteinova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
celou	celý	k2eAgFnSc4d1	celá
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teorie	teorie	k1gFnSc1	teorie
byla	být	k5eAaImAgFnS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
pomocí	pomocí	k7c2	pomocí
teoretických	teoretický	k2eAgFnPc2d1	teoretická
úvah	úvaha	k1gFnPc2	úvaha
a	a	k8xC	a
racionálních	racionální	k2eAgFnPc2d1	racionální
analýz	analýza	k1gFnPc2	analýza
a	a	k8xC	a
nebyla	být	k5eNaImAgFnS	být
zprvu	zprvu	k6eAd1	zprvu
podepřena	podepřen	k2eAgFnSc1d1	podepřena
pokusy	pokus	k1gInPc7	pokus
a	a	k8xC	a
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
u	u	k7c2	u
vědců	vědec	k1gMnPc2	vědec
skepticismus	skepticismus	k1gInSc4	skepticismus
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
rovnice	rovnice	k1gFnPc1	rovnice
ale	ale	k9	ale
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
předpovědi	předpověď	k1gFnPc1	předpověď
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
následné	následný	k2eAgNnSc4d1	následné
testování	testování	k1gNnSc4	testování
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
velkým	velký	k2eAgInSc7d1	velký
testem	test	k1gInSc7	test
bylo	být	k5eAaImAgNnS	být
měření	měření	k1gNnSc1	měření
ohybu	ohyb	k1gInSc2	ohyb
paprsků	paprsek	k1gInPc2	paprsek
ze	z	k7c2	z
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
hvězd	hvězda	k1gFnPc2	hvězda
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
gravitační	gravitační	k2eAgFnSc1d1	gravitační
čočka	čočka	k1gFnSc1	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
měření	měření	k1gNnSc1	měření
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
provedeno	provést	k5eAaPmNgNnS	provést
jen	jen	k9	jen
během	během	k7c2	během
slunečního	sluneční	k2eAgNnSc2d1	sluneční
zatmění	zatmění	k1gNnSc2	zatmění
<g/>
.	.	kIx.	.
</s>
<s>
Úkolu	úkol	k1gInSc3	úkol
se	se	k3xPyFc4	se
zhostil	zhostit	k5eAaPmAgMnS	zhostit
Arthur	Arthur	k1gMnSc1	Arthur
Eddington	Eddington	k1gInSc1	Eddington
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
měření	měření	k1gNnSc1	měření
teorii	teorie	k1gFnSc4	teorie
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1919	[number]	k4	1919
noviny	novina	k1gFnPc4	novina
The	The	k1gFnSc2	The
Times	Timesa	k1gFnPc2	Timesa
o	o	k7c4	o
potvrzení	potvrzení	k1gNnSc4	potvrzení
vydaly	vydat	k5eAaPmAgInP	vydat
článek	článek	k1gInSc4	článek
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
slavná	slavný	k2eAgFnSc1d1	slavná
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
teorie	teorie	k1gFnSc1	teorie
stala	stát	k5eAaPmAgFnS	stát
terčem	terč	k1gInSc7	terč
všemožných	všemožný	k2eAgInPc2d1	všemožný
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
provedená	provedený	k2eAgNnPc1d1	provedené
měření	měření	k1gNnSc1	měření
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
zatím	zatím	k6eAd1	zatím
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
až	až	k9	až
experiment	experiment	k1gInSc4	experiment
OPERA	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
prvotních	prvotní	k2eAgNnPc2d1	prvotní
měření	měření	k1gNnPc2	měření
zachycena	zachytit	k5eAaPmNgFnS	zachytit
neutrina	neutrino	k1gNnSc2	neutrino
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
popíralo	popírat	k5eAaImAgNnS	popírat
Einsteinovu	Einsteinův	k2eAgFnSc4d1	Einsteinova
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nic	nic	k3yNnSc1	nic
není	být	k5eNaImIp3nS	být
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvotní	prvotní	k2eAgFnSc6d1	prvotní
mediální	mediální	k2eAgFnSc6d1	mediální
senzaci	senzace	k1gFnSc6	senzace
sami	sám	k3xTgMnPc1	sám
experimentátoři	experimentátor	k1gMnPc1	experimentátor
<g/>
,	,	kIx,	,
od	od	k7c2	od
začátků	začátek	k1gInPc2	začátek
skeptičtí	skeptický	k2eAgMnPc1d1	skeptický
k	k	k7c3	k
naměřeným	naměřený	k2eAgMnPc3d1	naměřený
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
,	,	kIx,	,
provedli	provést	k5eAaPmAgMnP	provést
úpravu	úprava	k1gFnSc4	úprava
metodiky	metodika	k1gFnSc2	metodika
<g/>
,	,	kIx,	,
opakovanou	opakovaný	k2eAgFnSc4d1	opakovaná
analýzu	analýza	k1gFnSc4	analýza
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
nová	nový	k2eAgNnPc4d1	nové
měření	měření	k1gNnPc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
prokázala	prokázat	k5eAaPmAgFnS	prokázat
chybu	chyba	k1gFnSc4	chyba
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
procesu	proces	k1gInSc6	proces
měření	měření	k1gNnSc2	měření
a	a	k8xC	a
zneplatnila	zneplatnit	k5eAaImAgFnS	zneplatnit
výsledky	výsledek	k1gInPc4	výsledek
experimentů	experiment	k1gInPc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovaná	pozorovaný	k2eAgNnPc1d1	pozorované
neutrina	neutrino	k1gNnPc1	neutrino
tedy	tedy	k9	tedy
nejsou	být	k5eNaImIp3nP	být
rychlejší	rychlý	k2eAgFnPc1d2	rychlejší
než	než	k8xS	než
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
revoluce	revoluce	k1gFnSc1	revoluce
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
nekoná	konat	k5eNaImIp3nS	konat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgMnPc2d1	významný
fyziků	fyzik	k1gMnPc2	fyzik
a	a	k8xC	a
filozofů	filozof	k1gMnPc2	filozof
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
nebylo	být	k5eNaImAgNnS	být
o	o	k7c6	o
správnosti	správnost	k1gFnSc6	správnost
teorie	teorie	k1gFnSc2	teorie
přesvědčeno	přesvědčit	k5eAaPmNgNnS	přesvědčit
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
důvody	důvod	k1gInPc1	důvod
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgInP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
s	s	k7c7	s
Einsteinovými	Einsteinův	k2eAgFnPc7d1	Einsteinova
interpretacemi	interpretace	k1gFnPc7	interpretace
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
jen	jen	k9	jen
prostě	prostě	k9	prostě
nemohli	moct	k5eNaImAgMnP	moct
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
bez	bez	k7c2	bez
absolutna	absolutno	k1gNnSc2	absolutno
nemůže	moct	k5eNaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Einsteina	Einstein	k1gMnSc2	Einstein
zkrátka	zkrátka	k6eAd1	zkrátka
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
neporozumělo	porozumět	k5eNaPmAgNnS	porozumět
použité	použitý	k2eAgFnSc3d1	použitá
matematice	matematika	k1gFnSc3	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
veřejná	veřejný	k2eAgFnSc1d1	veřejná
sláva	sláva	k1gFnSc1	sláva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
následovala	následovat	k5eAaImAgFnS	následovat
po	po	k7c6	po
zmíněném	zmíněný	k2eAgNnSc6d1	zmíněné
zatmění	zatmění	k1gNnSc6	zatmění
<g/>
,	,	kIx,	,
přinesla	přinést	k5eAaPmAgFnS	přinést
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
jeho	jeho	k3xOp3gMnPc2	jeho
odpůrců	odpůrce	k1gMnPc2	odpůrce
nevoli	nevole	k1gFnSc4	nevole
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
až	až	k6eAd1	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
kvantové	kvantový	k2eAgFnSc3d1	kvantová
fyzice	fyzika	k1gFnSc3	fyzika
byl	být	k5eAaImAgInS	být
také	také	k9	také
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgInSc6	první
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
před	před	k7c7	před
Maxem	Max	k1gMnSc7	Max
Planckem	Planck	k1gInSc7	Planck
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
revoluční	revoluční	k2eAgFnSc1d1	revoluční
věc	věc	k1gFnSc1	věc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
myšlenka	myšlenka	k1gFnSc1	myšlenka
světelného	světelný	k2eAgNnSc2d1	světelné
kvanta	kvantum	k1gNnSc2	kvantum
zcela	zcela	k6eAd1	zcela
změnila	změnit	k5eAaPmAgFnS	změnit
klasické	klasický	k2eAgNnSc4d1	klasické
chápání	chápání	k1gNnSc4	chápání
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
na	na	k7c4	na
shromáždění	shromáždění	k1gNnSc4	shromáždění
fyziků	fyzik	k1gMnPc2	fyzik
Einstein	Einstein	k1gMnSc1	Einstein
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
najít	najít	k5eAaPmF	najít
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
vln	vlna	k1gFnPc2	vlna
a	a	k8xC	a
částic	částice	k1gFnPc2	částice
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Einstein	Einstein	k1gMnSc1	Einstein
stal	stát	k5eAaPmAgMnS	stát
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
postavou	postava	k1gFnSc7	postava
na	na	k7c6	na
známém	známý	k2eAgInSc6d1	známý
semináři	seminář	k1gInSc6	seminář
na	na	k7c6	na
Berlínské	berlínský	k2eAgFnSc6d1	Berlínská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
pravidelně	pravidelně	k6eAd1	pravidelně
každý	každý	k3xTgInSc1	každý
týden	týden	k1gInSc1	týden
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1921	[number]	k4	1921
Einstein	Einstein	k1gMnSc1	Einstein
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
přednášet	přednášet	k5eAaImF	přednášet
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
nové	nový	k2eAgFnSc6d1	nová
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
rok	rok	k1gInSc4	rok
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
nejznámější	známý	k2eAgInSc1d3	nejznámější
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
relativitu	relativita	k1gFnSc4	relativita
<g/>
,	,	kIx,	,
cenu	cena	k1gFnSc4	cena
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
dřívější	dřívější	k2eAgFnSc4d1	dřívější
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
fotoelektrickém	fotoelektrický	k2eAgInSc6d1	fotoelektrický
jevu	jev	k1gInSc6	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
relativita	relativita	k1gFnSc1	relativita
stále	stále	k6eAd1	stále
příliš	příliš	k6eAd1	příliš
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
odměněn	odměnit	k5eAaPmNgInS	odměnit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
komise	komise	k1gFnSc1	komise
Švédské	švédský	k2eAgFnSc2d1	švédská
královské	královský	k2eAgFnSc2d1	královská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
méně	málo	k6eAd2	málo
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
starší	starší	k1gMnSc1	starší
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
politického	politický	k2eAgNnSc2d1	politické
hlediska	hledisko	k1gNnSc2	hledisko
lepší	dobrý	k2eAgFnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kodaňská	kodaňský	k2eAgFnSc1d1	Kodaňská
interpretace	interpretace	k1gFnSc1	interpretace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
původní	původní	k2eAgFnSc1d1	původní
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
novou	nový	k2eAgFnSc7d1	nová
kvantovou	kvantový	k2eAgFnSc7d1	kvantová
mechanikou	mechanika	k1gFnSc7	mechanika
<g/>
,	,	kIx,	,
Einstein	Einstein	k1gMnSc1	Einstein
zavrhl	zavrhnout	k5eAaPmAgMnS	zavrhnout
kodaňskou	kodaňský	k2eAgFnSc4d1	Kodaňská
interpretaci	interpretace	k1gFnSc4	interpretace
nových	nový	k2eAgFnPc2d1	nová
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nelíbila	líbit	k5eNaImAgFnS	líbit
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
pravděpodobnostní	pravděpodobnostní	k2eAgInSc4d1	pravděpodobnostní
a	a	k8xC	a
nenázorný	názorný	k2eNgInSc4d1	nenázorný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
fyzikálním	fyzikální	k2eAgInPc3d1	fyzikální
jevům	jev	k1gInPc3	jev
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lidstvo	lidstvo	k1gNnSc1	lidstvo
mělo	mít	k5eAaImAgNnS	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hledal	hledat	k5eAaImAgMnS	hledat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
ucelenější	ucelený	k2eAgFnSc1d2	ucelenější
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rozuměj	rozumět	k5eAaImRp2nS	rozumět
deterministické	deterministický	k2eAgNnSc4d1	deterministické
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
taková	takový	k3xDgFnSc1	takový
fyzika	fyzika	k1gFnSc1	fyzika
musí	muset	k5eAaImIp3nS	muset
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
už	už	k9	už
dříve	dříve	k6eAd2	dříve
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
úspěchům	úspěch	k1gInPc3	úspěch
s	s	k7c7	s
atomy	atom	k1gInPc7	atom
<g/>
,	,	kIx,	,
fotony	foton	k1gInPc7	foton
a	a	k8xC	a
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechtěl	chtít	k5eNaImAgMnS	chtít
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
víry	víra	k1gFnSc2	víra
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
napsal	napsat	k5eAaBmAgMnS	napsat
Maxu	Maxa	k1gMnSc4	Maxa
Bornovi	Born	k1gMnSc3	Born
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
je	být	k5eAaImIp3nS	být
jistě	jistě	k6eAd1	jistě
velkolepá	velkolepý	k2eAgFnSc1d1	velkolepá
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
hlas	hlas	k1gInSc1	hlas
mi	já	k3xPp1nSc3	já
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravé	pravý	k2eAgNnSc1d1	pravé
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
teorie	teorie	k1gFnSc1	teorie
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
mnohé	mnohé	k1gNnSc4	mnohé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neposunuje	posunovat	k5eNaImIp3nS	posunovat
nás	my	k3xPp1nPc4	my
ani	ani	k8xC	ani
o	o	k7c4	o
kousek	kousek	k1gInSc4	kousek
blíž	blízce	k6eAd2	blízce
k	k	k7c3	k
Jeho	jeho	k3xOp3gNnSc3	jeho
tajemství	tajemství	k1gNnSc3	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
On	on	k3xPp3gMnSc1	on
v	v	k7c6	v
kostky	kostka	k1gFnSc2	kostka
nehraje	hrát	k5eNaImIp3nS	hrát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
pravděpodobnostních	pravděpodobnostní	k2eAgFnPc2d1	pravděpodobnostní
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
Einstein	Einstein	k1gMnSc1	Einstein
také	také	k9	také
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
pracích	práce	k1gFnPc6	práce
o	o	k7c6	o
Brownově	Brownův	k2eAgInSc6d1	Brownův
pohybu	pohyb	k1gInSc6	pohyb
a	a	k8xC	a
fotoelektřině	fotoelektřina	k1gFnSc6	fotoelektřina
využíval	využívat	k5eAaImAgInS	využívat
statistické	statistický	k2eAgFnPc4d1	statistická
analýzy	analýza	k1gFnPc4	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc2	který
publikoval	publikovat	k5eAaBmAgMnS	publikovat
před	před	k7c7	před
plodným	plodný	k2eAgInSc7d1	plodný
rokem	rok	k1gInSc7	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
objevil	objevit	k5eAaPmAgMnS	objevit
Gibbsovy	Gibbsův	k2eAgInPc4d1	Gibbsův
soubory	soubor	k1gInPc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
realita	realita	k1gFnSc1	realita
se	se	k3xPyFc4	se
v	v	k7c6	v
základu	základ	k1gInSc6	základ
chová	chovat	k5eAaImIp3nS	chovat
náhodně	náhodně	k6eAd1	náhodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Boseho-Einsteinova	Boseho-Einsteinův	k2eAgFnSc1d1	Boseho-Einsteinova
statistika	statistika	k1gFnSc1	statistika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
Einstein	Einstein	k1gMnSc1	Einstein
dostal	dostat	k5eAaPmAgMnS	dostat
krátký	krátký	k2eAgInSc4d1	krátký
článek	článek	k1gInSc4	článek
mladého	mladý	k2eAgMnSc2d1	mladý
indického	indický	k2eAgMnSc2d1	indický
fyzika	fyzik	k1gMnSc2	fyzik
Boseho	Bose	k1gMnSc2	Bose
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
popisoval	popisovat	k5eAaImAgMnS	popisovat
světlo	světlo	k1gNnSc4	světlo
jako	jako	k8xS	jako
plyn	plyn	k1gInSc4	plyn
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
Einsteina	Einstein	k1gMnSc4	Einstein
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
publikaci	publikace	k1gFnSc6	publikace
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejná	stejný	k2eAgFnSc1d1	stejná
statistika	statistika	k1gFnSc1	statistika
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
na	na	k7c4	na
atomy	atom	k1gInPc4	atom
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
německy	německy	k6eAd1	německy
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
lingua	lingu	k2eAgFnSc1d1	lingua
franca	franca	k1gFnSc1	franca
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
)	)	kIx)	)
psaný	psaný	k2eAgInSc1d1	psaný
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
popsal	popsat	k5eAaPmAgMnS	popsat
Boseho	Bose	k1gMnSc2	Bose
model	model	k1gInSc1	model
a	a	k8xC	a
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
jeho	jeho	k3xOp3gInPc4	jeho
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Boseho-Einsteinovo	Boseho-Einsteinův	k2eAgNnSc1d1	Boseho-Einsteinův
rozdělení	rozdělení	k1gNnSc1	rozdělení
popisuje	popisovat	k5eAaImIp3nS	popisovat
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
soubor	soubor	k1gInSc1	soubor
těchto	tento	k3xDgFnPc2	tento
identických	identický	k2eAgFnPc2d1	identická
částic	částice	k1gFnPc2	částice
známých	známý	k2eAgFnPc2d1	známá
jako	jako	k9	jako
bosony	bosona	k1gFnPc1	bosona
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
také	také	k9	také
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
Erwinu	Erwin	k1gMnSc3	Erwin
Schrödingerovi	Schrödinger	k1gMnSc3	Schrödinger
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
kvantového	kvantový	k2eAgNnSc2d1	kvantové
Boltzmannova	Boltzmannův	k2eAgNnSc2d1	Boltzmannovo
rozdělení	rozdělení	k1gNnSc2	rozdělení
modelující	modelující	k2eAgInSc4d1	modelující
plyn	plyn	k1gInSc4	plyn
pomocí	pomocí	k7c2	pomocí
směsi	směs	k1gFnSc2	směs
klasické	klasický	k2eAgFnSc2d1	klasická
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
však	však	k9	však
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
důležité	důležitý	k2eAgNnSc1d1	důležité
jako	jako	k8xC	jako
Boseho-Einsteinův	Boseho-Einsteinův	k2eAgInSc1d1	Boseho-Einsteinův
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
článek	článek	k1gInSc4	článek
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozdější	pozdní	k2eAgNnPc4d2	pozdější
léta	léto	k1gNnPc4	léto
==	==	k?	==
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
někdejší	někdejší	k2eAgMnSc1d1	někdejší
student	student	k1gMnSc1	student
Leó	Leó	k1gMnSc1	Leó
Szilárd	Szilárd	k1gMnSc1	Szilárd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
spoluvynalezli	spoluvynaleznout	k5eAaPmAgMnP	spoluvynaleznout
unikátní	unikátní	k2eAgInSc4d1	unikátní
typ	typ	k1gInSc4	typ
chladničky	chladnička	k1gFnSc2	chladnička
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
též	též	k9	též
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
chladnička	chladnička	k1gFnSc1	chladnička
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1930	[number]	k4	1930
ji	on	k3xPp3gFnSc4	on
tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
vědci	vědec	k1gMnPc1	vědec
patentovali	patentovat	k5eAaBmAgMnP	patentovat
pod	pod	k7c7	pod
č.	č.	k?	č.
US	US	kA	US
<g/>
1781541	[number]	k4	1781541
<g/>
.	.	kIx.	.
</s>
<s>
Patent	patent	k1gInSc1	patent
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
termodynamický	termodynamický	k2eAgInSc4d1	termodynamický
cyklus	cyklus	k1gInSc4	cyklus
chladící	chladící	k2eAgInPc4d1	chladící
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
pohybujících	pohybující	k2eAgFnPc2d1	pohybující
se	se	k3xPyFc4	se
součástek	součástka	k1gFnPc2	součástka
při	při	k7c6	při
konstantním	konstantní	k2eAgInSc6d1	konstantní
tlaku	tlak	k1gInSc6	tlak
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
spotřebovávající	spotřebovávající	k2eAgNnSc4d1	spotřebovávající
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Chladicí	chladicí	k2eAgInSc1d1	chladicí
cyklus	cyklus	k1gInSc1	cyklus
využíval	využívat	k5eAaPmAgInS	využívat
čpavek	čpavek	k1gInSc4	čpavek
<g/>
,	,	kIx,	,
butan	butan	k1gInSc4	butan
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
získal	získat	k5eAaPmAgMnS	získat
moc	moc	k6eAd1	moc
<g/>
,	,	kIx,	,
nenávist	nenávist	k1gFnSc1	nenávist
k	k	k7c3	k
Einsteinovi	Einstein	k1gMnSc3	Einstein
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nové	nový	k2eAgFnPc4d1	nová
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
národně-socialistickým	národněocialistický	k2eAgInSc7d1	národně-socialistický
režimem	režim	k1gInSc7	režim
nařčen	nařčen	k2eAgMnSc1d1	nařčen
z	z	k7c2	z
tvoření	tvoření	k1gNnSc2	tvoření
"	"	kIx"	"
<g/>
židovské	židovský	k2eAgFnSc2d1	židovská
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
"	"	kIx"	"
proti	proti	k7c3	proti
"	"	kIx"	"
<g/>
německé	německý	k2eAgFnSc3d1	německá
<g/>
,	,	kIx,	,
árijské	árijský	k2eAgFnSc3d1	árijská
fyzice	fyzika	k1gFnSc3	fyzika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
(	(	kIx(	(
<g/>
zmiňme	zminout	k5eAaPmRp1nP	zminout
laureáty	laureát	k1gMnPc7	laureát
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
Johanna	Johanna	k1gFnSc1	Johanna
Starka	starka	k1gFnSc1	starka
a	a	k8xC	a
Philippa	Philippa	k1gFnSc1	Philippa
Lenarda	Lenarda	k1gFnSc1	Lenarda
<g/>
)	)	kIx)	)
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
pokusech	pokus	k1gInPc6	pokus
zdiskreditovat	zdiskreditovat	k5eAaPmF	zdiskreditovat
jeho	jeho	k3xOp3gFnPc4	jeho
teorie	teorie	k1gFnPc4	teorie
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
zakázat	zakázat	k5eAaPmF	zakázat
ty	ten	k3xDgMnPc4	ten
německé	německý	k2eAgMnPc4d1	německý
fyziky	fyzik	k1gMnPc4	fyzik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gNnSc4	on
vyučovali	vyučovat	k5eAaImAgMnP	vyučovat
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Werner	Werner	k1gMnSc1	Werner
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgInS	získat
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
trvalému	trvalý	k2eAgInSc3d1	trvalý
pobytu	pobyt	k1gInSc3	pobyt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
FBI	FBI	kA	FBI
ho	on	k3xPp3gInSc4	on
sledovala	sledovat	k5eAaImAgFnS	sledovat
a	a	k8xC	a
prohledávala	prohledávat	k5eAaImAgFnS	prohledávat
mu	on	k3xPp3gNnSc3	on
i	i	k9	i
koš	koš	k1gInSc1	koš
na	na	k7c4	na
odpadky	odpadek	k1gInPc4	odpadek
<g/>
.	.	kIx.	.
<g/>
Einstein	Einstein	k1gMnSc1	Einstein
strávil	strávit	k5eAaPmAgMnS	strávit
posledních	poslední	k2eAgNnPc2d1	poslední
40	[number]	k4	40
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
v	v	k7c6	v
pokusech	pokus	k1gInPc6	pokus
sjednotit	sjednotit	k5eAaPmF	sjednotit
gravitaci	gravitace	k1gFnSc4	gravitace
a	a	k8xC	a
elektromagnetismus	elektromagnetismus	k1gInSc4	elektromagnetismus
nějakým	nějaký	k3yIgNnSc7	nějaký
důvtipným	důvtipný	k2eAgNnSc7d1	důvtipné
poznáním	poznání	k1gNnSc7	poznání
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Hledal	hledat	k5eAaImAgMnS	hledat
klasické	klasický	k2eAgNnSc4d1	klasické
spojení	spojení	k1gNnSc4	spojení
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgNnPc4d2	vyšší
studia	studio	k1gNnPc4	studio
(	(	kIx(	(
<g/>
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Advanced	Advanced	k1gInSc1	Advanced
Study	stud	k1gInPc1	stud
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
přijal	přijmout	k5eAaPmAgMnS	přijmout
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
na	na	k7c6	na
nově	nově	k6eAd1	nově
založeném	založený	k2eAgInSc6d1	založený
Institutu	institut	k1gInSc6	institut
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgNnPc4d2	vyšší
studia	studio	k1gNnPc4	studio
(	(	kIx(	(
<g/>
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Advanced	Advanced	k1gInSc1	Advanced
Study	stud	k1gInPc1	stud
<g/>
)	)	kIx)	)
na	na	k7c6	na
Princetonské	Princetonský	k2eAgFnSc6d1	Princetonská
univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
americkým	americký	k2eAgMnSc7d1	americký
občanem	občan	k1gMnSc7	občan
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
si	se	k3xPyFc3	se
uchoval	uchovat	k5eAaPmAgMnS	uchovat
své	svůj	k3xOyFgNnSc4	svůj
švýcarské	švýcarský	k2eAgNnSc4d1	švýcarské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
v	v	k7c6	v
Institutu	institut	k1gInSc6	institut
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgNnPc4d2	vyšší
studia	studio	k1gNnPc4	studio
byla	být	k5eAaImAgFnS	být
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
sjednocení	sjednocení	k1gNnSc4	sjednocení
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nazýval	nazývat	k5eAaImAgInS	nazývat
Velkou	velký	k2eAgFnSc7d1	velká
sjednocující	sjednocující	k2eAgFnSc7d1	sjednocující
teorií	teorie	k1gFnSc7	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
trávil	trávit	k5eAaImAgMnS	trávit
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
hledáním	hledání	k1gNnSc7	hledání
sjednocení	sjednocení	k1gNnSc2	sjednocení
fundamentálních	fundamentální	k2eAgFnPc2d1	fundamentální
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
popisoval	popisovat	k5eAaImAgInS	popisovat
všechny	všechen	k3xTgFnPc4	všechen
síly	síla	k1gFnPc4	síla
jako	jako	k8xC	jako
různé	různý	k2eAgInPc4d1	různý
projevy	projev	k1gInPc4	projev
jedné	jeden	k4xCgFnSc2	jeden
jediné	jediný	k2eAgFnSc2d1	jediná
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pokus	pokus	k1gInSc1	pokus
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
nezdaru	nezdar	k1gInSc3	nezdar
už	už	k6eAd1	už
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
slabá	slabý	k2eAgFnSc1d1	slabá
interakce	interakce	k1gFnSc1	interakce
nebyly	být	k5eNaImAgFnP	být
samostatně	samostatně	k6eAd1	samostatně
pochopeny	pochopen	k2eAgFnPc1d1	pochopena
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
15	[number]	k4	15
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
toto	tento	k3xDgNnSc1	tento
hledání	hledání	k1gNnSc1	hledání
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
ústředním	ústřední	k2eAgInSc7d1	ústřední
problémem	problém	k1gInSc7	problém
moderní	moderní	k2eAgFnSc2d1	moderní
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
ho	on	k3xPp3gMnSc4	on
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
zejména	zejména	k9	zejména
teorie	teorie	k1gFnSc1	teorie
superstrun	superstruna	k1gFnPc2	superstruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zobecněná	zobecněný	k2eAgFnSc1d1	zobecněná
teorie	teorie	k1gFnSc1	teorie
===	===	k?	===
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
Einstein	Einstein	k1gMnSc1	Einstein
začal	začít	k5eAaPmAgMnS	začít
tvořit	tvořit	k5eAaImF	tvořit
zobecněnou	zobecněný	k2eAgFnSc4d1	zobecněná
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
s	s	k7c7	s
univerzálními	univerzální	k2eAgInPc7d1	univerzální
zákony	zákon	k1gInPc7	zákon
gravitace	gravitace	k1gFnSc2	gravitace
a	a	k8xC	a
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
sjednocení	sjednocení	k1gNnSc4	sjednocení
a	a	k8xC	a
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
fundamentálních	fundamentální	k2eAgFnPc2d1	fundamentální
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
popsal	popsat	k5eAaPmAgMnS	popsat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
článku	článek	k1gInSc6	článek
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Scientific	Scientific	k1gMnSc1	Scientific
American	American	k1gMnSc1	American
<g/>
.	.	kIx.	.
</s>
<s>
Postupoval	postupovat	k5eAaImAgMnS	postupovat
s	s	k7c7	s
vírou	víra	k1gFnSc7	víra
v	v	k7c4	v
jediné	jediný	k2eAgNnSc4d1	jediné
statistické	statistický	k2eAgNnSc4d1	statistické
měření	měření	k1gNnSc4	měření
odchylky	odchylka	k1gFnSc2	odchylka
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
soubor	soubor	k1gInSc4	soubor
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
prošetřoval	prošetřovat	k5eAaImAgMnS	prošetřovat
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
a	a	k8xC	a
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
nekonečný	konečný	k2eNgInSc4d1	nekonečný
dosah	dosah	k1gInSc4	dosah
a	a	k8xC	a
dodržují	dodržovat	k5eAaImIp3nP	dodržovat
zákon	zákon	k1gInSc4	zákon
převrácených	převrácený	k2eAgInPc2d1	převrácený
čtverců	čtverec	k1gInPc2	čtverec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
zobecněná	zobecněný	k2eAgFnSc1d1	zobecněná
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc2	gravitace
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgNnSc1d1	univerzální
matematické	matematický	k2eAgNnSc1d1	matematické
pojetí	pojetí	k1gNnSc1	pojetí
teorie	teorie	k1gFnSc2	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
logiky	logika	k1gFnSc2	logika
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
zredukovat	zredukovat	k5eAaPmF	zredukovat
různé	různý	k2eAgInPc4d1	různý
jevy	jev	k1gInPc4	jev
do	do	k7c2	do
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nám	my	k3xPp1nPc3	my
důvěrně	důvěrně	k6eAd1	důvěrně
známé	známý	k2eAgFnPc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
sjednotit	sjednotit	k5eAaPmF	sjednotit
gravitaci	gravitace	k1gFnSc4	gravitace
a	a	k8xC	a
elektromagnetismus	elektromagnetismus	k1gInSc4	elektromagnetismus
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
neotřelému	otřelý	k2eNgNnSc3d1	neotřelé
porozumění	porozumění	k1gNnSc3	porozumění
kvantové	kvantový	k2eAgFnSc3d1	kvantová
mechanice	mechanika	k1gFnSc3	mechanika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
strukturu	struktura	k1gFnSc4	struktura
čtyřrozměrného	čtyřrozměrný	k2eAgNnSc2d1	čtyřrozměrné
časového	časový	k2eAgNnSc2d1	časové
kontinua	kontinuum	k1gNnSc2	kontinuum
vyjádřeného	vyjádřený	k2eAgInSc2d1	vyjádřený
axiomy	axiom	k1gInPc7	axiom
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
byly	být	k5eAaImAgInP	být
reprezentovány	reprezentován	k2eAgInPc1d1	reprezentován
vektory	vektor	k1gInPc1	vektor
s	s	k7c7	s
pěti	pět	k4xCc7	pět
složkami	složka	k1gFnPc7	složka
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
výzkumu	výzkum	k1gInSc6	výzkum
staly	stát	k5eAaPmAgFnP	stát
omezenou	omezený	k2eAgFnSc7d1	omezená
oblastí	oblast	k1gFnSc7	oblast
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
byla	být	k5eAaImAgFnS	být
síla	síla	k1gFnSc1	síla
pole	pole	k1gFnSc1	pole
nebo	nebo	k8xC	nebo
hustota	hustota	k1gFnSc1	hustota
energie	energie	k1gFnSc2	energie
obzvláště	obzvláště	k6eAd1	obzvláště
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Subatomární	Subatomární	k2eAgFnSc2d1	Subatomární
částice	částice	k1gFnSc2	částice
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
objekty	objekt	k1gInPc4	objekt
vložené	vložený	k2eAgInPc4d1	vložený
do	do	k7c2	do
sjednoceného	sjednocený	k2eAgNnSc2d1	sjednocené
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
ovlivňující	ovlivňující	k2eAgNnSc1d1	ovlivňující
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
existující	existující	k2eAgInSc4d1	existující
jakožto	jakožto	k8xS	jakožto
podstatné	podstatný	k2eAgFnPc4d1	podstatná
součásti	součást	k1gFnPc4	součást
sjednoceného	sjednocený	k2eAgNnSc2d1	sjednocené
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
také	také	k9	také
hledal	hledat	k5eAaImAgMnS	hledat
přirozené	přirozený	k2eAgNnSc4d1	přirozené
zobecnění	zobecnění	k1gNnSc4	zobecnění
symetrických	symetrický	k2eAgFnPc2d1	symetrická
tenzorových	tenzorový	k2eAgFnPc2d1	tenzorová
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
kombinaci	kombinace	k1gFnSc4	kombinace
dvou	dva	k4xCgFnPc2	dva
složek	složka	k1gFnPc2	složka
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jejich	jejich	k3xOp3gFnPc1	jejich
symetrické	symetrický	k2eAgFnPc1d1	symetrická
a	a	k8xC	a
antisymetrické	antisymetrický	k2eAgFnPc1d1	antisymetrická
části	část	k1gFnPc1	část
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumal	zkoumat	k5eAaImAgInS	zkoumat
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
rovnice	rovnice	k1gFnPc4	rovnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
odvodit	odvodit	k5eAaPmF	odvodit
z	z	k7c2	z
variačního	variační	k2eAgInSc2d1	variační
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stával	stávat	k5eAaImAgInS	stávat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
výzkumu	výzkum	k1gInSc6	výzkum
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
izolovaným	izolovaný	k2eAgMnSc7d1	izolovaný
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
úsilí	úsilí	k1gNnSc3	úsilí
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
šílený	šílený	k2eAgMnSc1d1	šílený
vědec	vědec	k1gMnSc1	vědec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
konstrukci	konstrukce	k1gFnSc6	konstrukce
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
teorie	teorie	k1gFnSc2	teorie
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
byly	být	k5eAaImAgFnP	být
zcela	zcela	k6eAd1	zcela
neúspěšné	úspěšný	k2eNgFnPc1d1	neúspěšná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
Einstein	Einstein	k1gMnSc1	Einstein
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
komisi	komise	k1gFnSc6	komise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
založila	založit	k5eAaPmAgFnS	založit
židovskými	židovská	k1gFnPc7	židovská
občany	občan	k1gMnPc4	občan
podporovanou	podporovaný	k2eAgFnSc4d1	podporovaná
soukromou	soukromý	k2eAgFnSc4d1	soukromá
a	a	k8xC	a
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
Brandeisovu	Brandeisův	k2eAgFnSc4d1	Brandeisův
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
vydal	vydat	k5eAaPmAgInS	vydat
revidovanou	revidovaný	k2eAgFnSc4d1	revidovaná
sjednocenou	sjednocený	k2eAgFnSc4d1	sjednocená
teorii	teorie	k1gFnSc4	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Einstein	Einstein	k1gMnSc1	Einstein
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Milevou	Mileva	k1gFnSc7	Mileva
Marićovou	Marićový	k2eAgFnSc7d1	Marićový
<g/>
,	,	kIx,	,
srbskou	srbský	k2eAgFnSc7d1	Srbská
studentkou	studentka	k1gFnSc7	studentka
a	a	k8xC	a
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Nikoly	Nikola	k1gMnSc2	Nikola
Tesly	Tesla	k1gMnSc2	Tesla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
Aspergerův	Aspergerův	k2eAgInSc1d1	Aspergerův
syndrom	syndrom	k1gInSc1	syndrom
jako	jako	k8xS	jako
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgNnPc2	tento
let	léto	k1gNnPc2	léto
Einstein	Einstein	k1gMnSc1	Einstein
probíral	probírat	k5eAaImAgMnS	probírat
své	svůj	k3xOyFgInPc4	svůj
vědecké	vědecký	k2eAgInPc4d1	vědecký
zájmy	zájem	k1gInPc4	zájem
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
blízkých	blízký	k2eAgMnPc2d1	blízký
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Milevy	Mileva	k1gFnSc2	Mileva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
první	první	k4xOgNnSc1	první
dítě	dítě	k1gNnSc1	dítě
Einsteina	Einstein	k1gMnSc2	Einstein
a	a	k8xC	a
Milevy	Mileva	k1gFnPc1	Mileva
Marićové	Marićová	k1gFnSc2	Marićová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1902	[number]	k4	1902
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
nemanželská	manželský	k2eNgFnSc1d1	nemanželská
dcera	dcera	k1gFnSc1	dcera
Lieserl	Lieserl	k1gMnSc1	Lieserl
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Osud	osud	k1gInSc1	osud
Lieserl	Lieserla	k1gFnPc2	Lieserla
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
záškrt	záškrt	k1gInSc4	záškrt
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k2eAgMnPc2d1	jiný
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
k	k	k7c3	k
adopci	adopce	k1gFnSc3	adopce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
s	s	k7c7	s
Milevou	Mileva	k1gFnSc7	Mileva
Marićovou	Marićový	k2eAgFnSc7d1	Marićový
oženil	oženit	k5eAaPmAgMnS	oženit
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
osobním	osobní	k2eAgNnSc7d1	osobní
a	a	k8xC	a
intelektuálním	intelektuální	k2eAgNnSc7d1	intelektuální
partnerstvím	partnerství	k1gNnSc7	partnerství
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Mileva	Mileva	k1gFnSc1	Mileva
byla	být	k5eAaImAgFnS	být
matematičkou	matematička	k1gFnSc7	matematička
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
popisoval	popisovat	k5eAaImAgMnS	popisovat
Milevu	Mileva	k1gFnSc4	Mileva
zamilovaně	zamilovaně	k6eAd1	zamilovaně
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
rovná	rovnat	k5eAaImIp3nS	rovnat
a	a	k8xC	a
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mileviným	Milevin	k2eAgInSc7d1	Milevin
původním	původní	k2eAgInSc7d1	původní
snem	sen	k1gInSc7	sen
byla	být	k5eAaImAgFnS	být
medicína	medicína	k1gFnSc1	medicína
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
její	její	k3xOp3gFnSc1	její
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
matematice	matematika	k1gFnSc3	matematika
a	a	k8xC	a
fyzice	fyzika	k1gFnSc3	fyzika
nakonec	nakonec	k6eAd1	nakonec
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pátou	pátý	k4xOgFnSc7	pátý
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
curyšské	curyšský	k2eAgFnSc6d1	curyšská
polytechnice	polytechnika	k1gFnSc6	polytechnika
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
budoucím	budoucí	k2eAgMnSc7d1	budoucí
manželem	manžel	k1gMnSc7	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
dodnes	dodnes	k6eAd1	dodnes
spekulují	spekulovat	k5eAaImIp3nP	spekulovat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
Mileva	Mileva	k1gFnSc1	Mileva
také	také	k9	také
významně	významně	k6eAd1	významně
nepřispěla	přispět	k5eNaPmAgFnS	přispět
k	k	k7c3	k
formulaci	formulace	k1gFnSc3	formulace
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Vědec	vědec	k1gMnSc1	vědec
Abraham	Abraham	k1gMnSc1	Abraham
Joffe	Joff	k1gInSc5	Joff
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
oběma	dva	k4xCgInPc7	dva
manžely	manžel	k1gMnPc7	manžel
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
původním	původní	k2eAgInSc6d1	původní
rukopisu	rukopis	k1gInSc6	rukopis
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
viděl	vidět	k5eAaImAgInS	vidět
podpis	podpis	k1gInSc1	podpis
Einstein-Marity	Einstein-Marita	k1gFnSc2	Einstein-Marita
<g/>
.	.	kIx.	.
</s>
<s>
Marity	Marita	k1gFnPc4	Marita
znamená	znamenat	k5eAaImIp3nS	znamenat
Marićovou	Marićová	k1gFnSc4	Marićová
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
historici	historik	k1gMnPc1	historik
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Galina	Galina	k1gFnSc1	Galina
Weinsteinová	Weinsteinová	k1gFnSc1	Weinsteinová
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
jen	jen	k9	jen
o	o	k7c4	o
nedorozumění	nedorozumění	k1gNnSc4	nedorozumění
a	a	k8xC	a
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
dopisy	dopis	k1gInPc1	dopis
Einsteina	Einstein	k1gMnSc2	Einstein
Milevě	Mileva	k1gFnSc6	Mileva
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgFnPc1d1	plná
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
nápadů	nápad	k1gInPc2	nápad
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Mileviny	Milevin	k2eAgInPc1d1	Milevin
dopisy	dopis	k1gInPc1	dopis
manželovi	manžel	k1gMnSc3	manžel
podobné	podobný	k2eAgFnSc2d1	podobná
myšlenky	myšlenka	k1gFnSc2	myšlenka
postrádají	postrádat	k5eAaImIp3nP	postrádat
<g/>
.	.	kIx.	.
</s>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Einsteinům	Einstein	k1gMnPc3	Einstein
jejich	jejich	k3xOp3gMnSc1	jejich
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
Hans	Hans	k1gMnSc1	Hans
Albert	Albert	k1gMnSc1	Albert
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Albert	Albert	k1gMnSc1	Albert
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
univerzitním	univerzitní	k2eAgMnSc7d1	univerzitní
profesorem	profesor	k1gMnSc7	profesor
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
uznávaným	uznávaný	k2eAgMnSc7d1	uznávaný
vědcem	vědec	k1gMnSc7	vědec
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
hydrauliky	hydraulika	k1gFnSc2	hydraulika
a	a	k8xC	a
transportu	transport	k1gInSc2	transport
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Eduard	Eduard	k1gMnSc1	Eduard
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
otcem	otec	k1gMnSc7	otec
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
psychiatrii	psychiatrie	k1gFnSc4	psychiatrie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
propukla	propuknout	k5eAaPmAgFnS	propuknout
schizofrenie	schizofrenie	k1gFnSc1	schizofrenie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
starala	starat	k5eAaImAgFnS	starat
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
na	na	k7c6	na
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
klinice	klinika	k1gFnSc6	klinika
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1965	[number]	k4	1965
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1919	[number]	k4	1919
se	se	k3xPyFc4	se
Einstein	Einstein	k1gMnSc1	Einstein
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
s	s	k7c7	s
Milevou	Mileva	k1gFnSc7	Mileva
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestřenicí	sestřenice	k1gFnSc7	sestřenice
Elsou	Elsa	k1gFnSc7	Elsa
Löwenthalovou	Löwenthalová	k1gFnSc7	Löwenthalová
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Einsteinovou	Einsteinová	k1gFnSc7	Einsteinová
(	(	kIx(	(
<g/>
Löwenthalová	Löwenthalová	k1gFnSc1	Löwenthalová
bylo	být	k5eAaImAgNnS	být
příjmení	příjmení	k1gNnSc1	příjmení
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
prvním	první	k4xOgNnSc6	první
manželovi	manžel	k1gMnSc6	manžel
Maxovi	Max	k1gMnSc6	Max
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elsa	Elsa	k1gFnSc1	Elsa
byla	být	k5eAaImAgFnS	být
Albertovou	Albertová	k1gFnSc7	Albertová
první	první	k4xOgFnSc1	první
sestřenicí	sestřenice	k1gFnSc7	sestřenice
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
sestřenicí	sestřenice	k1gFnSc7	sestřenice
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
starší	starší	k1gMnSc1	starší
než	než	k8xS	než
Albert	Albert	k1gMnSc1	Albert
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
mu	on	k3xPp3gMnSc3	on
se	se	k3xPyFc4	se
uzdravit	uzdravit	k5eAaPmF	uzdravit
po	po	k7c6	po
částečném	částečný	k2eAgInSc6d1	částečný
nervovém	nervový	k2eAgInSc6d1	nervový
kolapsu	kolaps	k1gInSc6	kolaps
kombinovaném	kombinovaný	k2eAgInSc6d1	kombinovaný
s	s	k7c7	s
žaludečními	žaludeční	k2eAgFnPc7d1	žaludeční
obtížemi	obtíž	k1gFnPc7	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
manželství	manželství	k1gNnSc1	manželství
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
Einstein	Einstein	k1gMnSc1	Einstein
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Elsou	Elsa	k1gFnSc7	Elsa
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Kitano	Kitana	k1gFnSc5	Kitana
Maru	Maru	k1gFnPc3	Maru
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
přivedla	přivést	k5eAaPmAgFnS	přivést
také	také	k9	také
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
asijských	asijský	k2eAgInPc2d1	asijský
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
do	do	k7c2	do
Singapuru	Singapur	k1gInSc2	Singapur
<g/>
,	,	kIx,	,
Hongkongu	Hongkong	k1gInSc2	Hongkong
a	a	k8xC	a
Šanghaje	Šanghaj	k1gFnSc2	Šanghaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Princetonu	Princeton	k1gInSc6	Princeton
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Zanechal	zanechat	k5eAaPmAgInS	zanechat
zobecněnou	zobecněný	k2eAgFnSc4d1	zobecněná
teorii	teorie	k1gFnSc4	teorie
gravitace	gravitace	k1gFnSc2	gravitace
nevyřešenou	vyřešený	k2eNgFnSc4d1	nevyřešená
<g/>
.	.	kIx.	.
</s>
<s>
Kremace	kremace	k1gFnSc1	kremace
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
proběhla	proběhnout	k5eAaPmAgNnP	proběhnout
ještě	ještě	k9	ještě
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
v	v	k7c6	v
Trentonu	Trenton	k1gInSc6	Trenton
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
popel	popel	k1gInSc1	popel
byl	být	k5eAaImAgInS	být
rozptýlen	rozptýlit	k5eAaPmNgInS	rozptýlit
na	na	k7c6	na
utajeném	utajený	k2eAgNnSc6d1	utajené
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
mozek	mozek	k1gInSc1	mozek
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
zachoval	zachovat	k5eAaPmAgMnS	zachovat
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Thomase	Thomas	k1gMnSc2	Thomas
Stolze	stolze	k6eAd1	stolze
Harveye	Harvey	k1gMnSc4	Harvey
<g/>
,	,	kIx,	,
patologa	patolog	k1gMnSc4	patolog
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prováděl	provádět	k5eAaImAgInS	provádět
pitvu	pitva	k1gFnSc4	pitva
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Harvey	Harvea	k1gFnPc1	Harvea
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
neshledal	shledat	k5eNaPmAgMnS	shledat
nic	nic	k3yNnSc1	nic
nenormálního	normální	k2eNgNnSc2d1	nenormální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
další	další	k2eAgFnPc1d1	další
analýzy	analýza	k1gFnPc1	analýza
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
týmem	tým	k1gInSc7	tým
na	na	k7c6	na
McMasterově	McMasterův	k2eAgFnSc6d1	McMasterův
Univerzitě	univerzita	k1gFnSc6	univerzita
odhalily	odhalit	k5eAaPmAgFnP	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
Einsteinovi	Einstein	k1gMnSc3	Einstein
chyběla	chybět	k5eAaImAgFnS	chybět
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
její	její	k3xOp3gFnSc3	její
kompenzaci	kompenzace	k1gFnSc3	kompenzace
byl	být	k5eAaImAgInS	být
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
temenní	temenní	k2eAgInSc1d1	temenní
lalok	lalok	k1gInSc1	lalok
o	o	k7c4	o
15	[number]	k4	15
%	%	kIx~	%
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
obyčejně	obyčejně	k6eAd1	obyčejně
bývá	bývat	k5eAaImIp3nS	bývat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
mozku	mozek	k1gInSc2	mozek
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
matematické	matematický	k2eAgNnSc4d1	matematické
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
,	,	kIx,	,
vizuálně	vizuálně	k6eAd1	vizuálně
prostorové	prostorový	k2eAgNnSc4d1	prostorové
vnímání	vnímání	k1gNnSc4	vnímání
a	a	k8xC	a
představy	představa	k1gFnPc4	představa
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnost	osobnost	k1gFnSc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
byl	být	k5eAaImAgMnS	být
respektovaný	respektovaný	k2eAgInSc4d1	respektovaný
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
přátelskost	přátelskost	k1gFnSc4	přátelskost
zakořeněnou	zakořeněný	k2eAgFnSc4d1	zakořeněná
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
pacifismu	pacifismus	k1gInSc6	pacifismus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
skromný	skromný	k2eAgMnSc1d1	skromný
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
schopností	schopnost	k1gFnSc7	schopnost
týkalo	týkat	k5eAaImAgNnS	týkat
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
jasné	jasný	k2eAgInPc4d1	jasný
názory	názor	k1gInPc4	názor
na	na	k7c4	na
módu	móda	k1gFnSc4	móda
–	–	k?	–
například	například	k6eAd1	například
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
svůj	svůj	k3xOyFgInSc4	svůj
šatník	šatník	k1gInSc4	šatník
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nemusel	muset	k5eNaImAgMnS	muset
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
vzít	vzít	k5eAaPmF	vzít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Nejedl	jíst	k5eNaImAgMnS	jíst
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
těla	tělo	k1gNnSc2	tělo
zabitých	zabitý	k2eAgNnPc2d1	zabité
zvířat	zvíře	k1gNnPc2	zvíře
–	–	k?	–
hlásil	hlásit	k5eAaImAgMnS	hlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
vegetariánství	vegetariánství	k1gNnSc3	vegetariánství
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k1gMnSc1	známý
je	být	k5eAaImIp3nS	být
Einsteinův	Einsteinův	k2eAgInSc4d1	Einsteinův
citát	citát	k1gInSc4	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nic	nic	k3yNnSc1	nic
nebude	být	k5eNaImBp3nS	být
lidskému	lidský	k2eAgNnSc3d1	lidské
zdraví	zdravit	k5eAaImIp3nS	zdravit
prospěšnější	prospěšný	k2eAgNnSc1d2	prospěšnější
a	a	k8xC	a
nic	nic	k3yNnSc1	nic
nezvýší	zvýšit	k5eNaPmIp3nS	zvýšit
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
zachování	zachování	k1gNnSc4	zachování
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
vegetariánskou	vegetariánský	k2eAgFnSc4d1	vegetariánská
stravu	strava	k1gFnSc4	strava
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Občas	občas	k6eAd1	občas
měl	mít	k5eAaImAgInS	mít
hravý	hravý	k2eAgInSc1d1	hravý
smysl	smysl	k1gInSc1	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
a	a	k8xC	a
vyžíval	vyžívat	k5eAaImAgMnS	vyžívat
se	se	k3xPyFc4	se
v	v	k7c4	v
hraní	hraní	k1gNnSc4	hraní
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
v	v	k7c6	v
jachtingu	jachting	k1gInSc6	jachting
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
typickým	typický	k2eAgInPc3d1	typický
"	"	kIx"	"
<g/>
roztržitým	roztržitý	k2eAgMnSc7d1	roztržitý
profesorem	profesor	k1gMnSc7	profesor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zapomínal	zapomínat	k5eAaImAgMnS	zapomínat
na	na	k7c6	na
každodenní	každodenní	k2eAgFnSc6d1	každodenní
věci	věc	k1gFnSc6	věc
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
klíče	klíč	k1gInPc4	klíč
<g/>
,	,	kIx,	,
a	a	k8xC	a
soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
tak	tak	k9	tak
mocně	mocně	k6eAd1	mocně
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedbal	nedbat	k5eAaImAgInS	nedbat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženské	náboženský	k2eAgInPc1d1	náboženský
názory	názor	k1gInPc1	názor
===	===	k?	===
</s>
</p>
<p>
<s>
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
náboženský	náboženský	k2eAgInSc1d1	náboženský
názor	názor	k1gInSc1	názor
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
deistický	deistický	k2eAgInSc4d1	deistický
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
prohlášeních	prohlášení	k1gNnPc6	prohlášení
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
označoval	označovat	k5eAaImAgInS	označovat
za	za	k7c4	za
agnostika	agnostik	k1gMnSc4	agnostik
<g/>
.	.	kIx.	.
</s>
<s>
Věřil	věřit	k5eAaImAgMnS	věřit
prý	prý	k9	prý
"	"	kIx"	"
<g/>
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
v	v	k7c6	v
harmonii	harmonie	k1gFnSc6	harmonie
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
,	,	kIx,	,
ne	ne	k9	ne
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
osud	osud	k1gInSc4	osud
a	a	k8xC	a
činy	čin	k1gInPc4	čin
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
názor	názor	k1gInSc4	názor
spíše	spíše	k9	spíše
panteistický	panteistický	k2eAgInSc4d1	panteistický
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
chtěl	chtít	k5eAaImAgMnS	chtít
"	"	kIx"	"
<g/>
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Bůh	bůh	k1gMnSc1	bůh
stvořil	stvořit	k5eAaPmAgMnS	stvořit
svět	svět	k1gInSc4	svět
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgInS	být
tázán	tázán	k2eAgInSc1d1	tázán
na	na	k7c4	na
náboženské	náboženský	k2eAgFnPc4d1	náboženská
otázky	otázka	k1gFnPc4	otázka
Martinem	Martin	k1gMnSc7	Martin
Buberem	Buber	k1gMnSc7	Buber
<g/>
,	,	kIx,	,
Einstein	Einstein	k1gMnSc1	Einstein
zvolal	zvolat	k5eAaPmAgMnS	zvolat
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
fyzikové	fyzik	k1gMnPc1	fyzik
se	se	k3xPyFc4	se
jen	jen	k9	jen
snažíme	snažit	k5eAaImIp1nP	snažit
obtáhnout	obtáhnout	k5eAaPmF	obtáhnout
čáry	čára	k1gFnPc4	čára
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
On	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
A	a	k9	a
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Buddhismus	buddhismus	k1gInSc1	buddhismus
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
očekáváno	očekávat	k5eAaImNgNnS	očekávat
ve	v	k7c6	v
vesmírném	vesmírný	k2eAgNnSc6d1	vesmírné
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
:	:	kIx,	:
Převýší	převýšit	k5eAaPmIp3nP	převýšit
osobního	osobní	k2eAgMnSc4d1	osobní
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
vyvaruje	vyvarovat	k5eAaPmIp3nS	vyvarovat
se	se	k3xPyFc4	se
dogmat	dogma	k1gNnPc2	dogma
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
<g/>
;	;	kIx,	;
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
jak	jak	k6eAd1	jak
přírodou	příroda	k1gFnSc7	příroda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
duchovnem	duchovno	k1gNnSc7	duchovno
<g/>
,	,	kIx,	,
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
posvátném	posvátný	k2eAgInSc6d1	posvátný
smyslu	smysl	k1gInSc6	smysl
a	a	k8xC	a
povstane	povstat	k5eAaPmIp3nS	povstat
ze	z	k7c2	z
zkušeností	zkušenost	k1gFnPc2	zkušenost
všech	všecek	k3xTgMnPc2	všecek
<g />
.	.	kIx.	.
</s>
<s>
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgFnPc2d1	přírodní
i	i	k8xC	i
duchovních	duchovní	k2eAgFnPc2d1	duchovní
<g/>
,	,	kIx,	,
jako	jako	k9	jako
jejich	jejich	k3xOp3gNnSc4	jejich
smysluplné	smysluplný	k2eAgNnSc4d1	smysluplné
sjednocení	sjednocení	k1gNnSc4	sjednocení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
pak	pak	k6eAd1	pak
shrnul	shrnout	k5eAaPmAgMnS	shrnout
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mé	můj	k3xOp1gNnSc1	můj
náboženství	náboženství	k1gNnSc1	náboženství
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
pokorného	pokorný	k2eAgInSc2d1	pokorný
obdivu	obdiv	k1gInSc2	obdiv
neomezeného	omezený	k2eNgMnSc2d1	neomezený
vyššího	vysoký	k2eAgMnSc2d2	vyšší
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
jako	jako	k9	jako
drobné	drobný	k2eAgInPc4d1	drobný
detaily	detail	k1gInPc4	detail
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsme	být	k5eAaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
postřehnout	postřehnout	k5eAaPmF	postřehnout
svou	svůj	k3xOyFgFnSc4	svůj
chatrnou	chatrný	k2eAgFnSc4d1	chatrná
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
vetchou	vetchý	k2eAgFnSc7d1	vetchá
myslí	mysl	k1gFnSc7	mysl
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
dopis	dopis	k1gInSc1	dopis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
náboženství	náboženství	k1gNnSc6	náboženství
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
pošetilou	pošetilý	k2eAgFnSc4d1	pošetilá
pověru	pověra	k1gFnSc4	pověra
Einstein	Einstein	k1gMnSc1	Einstein
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
být	být	k5eAaImF	být
označován	označován	k2eAgMnSc1d1	označován
za	za	k7c4	za
ateistu	ateista	k1gMnSc4	ateista
a	a	k8xC	a
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
"	"	kIx"	"
<g/>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
mnou	já	k3xPp1nSc7	já
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
takzvaných	takzvaný	k2eAgMnPc2d1	takzvaný
ateistů	ateista	k1gMnPc2	ateista
je	být	k5eAaImIp3nS	být
pocit	pocit	k1gInSc4	pocit
naprosté	naprostý	k2eAgFnSc2d1	naprostá
pokory	pokora	k1gFnSc2	pokora
před	před	k7c7	před
neuchopitelným	uchopitelný	k2eNgNnSc7d1	neuchopitelné
tajemstvím	tajemství	k1gNnSc7	tajemství
a	a	k8xC	a
harmonií	harmonie	k1gFnSc7	harmonie
kosmu	kosmos	k1gInSc2	kosmos
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Politické	politický	k2eAgInPc1d1	politický
názory	názor	k1gInPc1	názor
a	a	k8xC	a
angažmá	angažmá	k1gNnSc1	angažmá
===	===	k?	===
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
pacifistu	pacifista	k1gMnSc4	pacifista
a	a	k8xC	a
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
za	za	k7c4	za
socialistu	socialista	k1gMnSc4	socialista
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Věřím	věřit	k5eAaImIp1nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gándhího	Gándhí	k1gMnSc4	Gándhí
názory	názor	k1gInPc4	názor
byly	být	k5eAaImAgFnP	být
ty	ten	k3xDgFnPc1	ten
nejosvícenější	osvícený	k2eAgFnPc1d3	nejosvícenější
z	z	k7c2	z
názorů	názor	k1gInPc2	názor
všech	všecek	k3xTgMnPc2	všecek
politiků	politik	k1gMnPc2	politik
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
pokoušet	pokoušet	k5eAaImF	pokoušet
dělat	dělat	k5eAaImF	dělat
věci	věc	k1gFnPc4	věc
v	v	k7c6	v
jeho	jeho	k3xOp3gMnSc6	jeho
duchu	duch	k1gMnSc6	duch
<g/>
:	:	kIx,	:
nepoužívat	používat	k5eNaImF	používat
násilí	násilí	k1gNnSc4	násilí
v	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
naši	náš	k3xOp1gFnSc4	náš
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neúčastnit	účastnit	k5eNaImF	účastnit
se	se	k3xPyFc4	se
ničeho	nic	k3yNnSc2	nic
<g/>
,	,	kIx,	,
o	o	k7c6	o
čem	co	k3yRnSc6	co
si	se	k3xPyFc3	se
myslíme	myslet	k5eAaImIp1nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zlé	zlý	k2eAgNnSc1d1	zlé
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Einsteinovy	Einsteinův	k2eAgInPc4d1	Einsteinův
názory	názor	k1gInPc4	názor
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
otázky	otázka	k1gFnPc4	otázka
včetně	včetně	k7c2	včetně
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
mccarthismu	mccarthismus	k1gInSc2	mccarthismus
a	a	k8xC	a
rasismu	rasismus	k1gInSc2	rasismus
byly	být	k5eAaImAgInP	být
totalitními	totalitní	k2eAgFnPc7d1	totalitní
režimy	režim	k1gInPc1	režim
zatajovány	zatajovat	k5eAaImNgInP	zatajovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
selektivně	selektivně	k6eAd1	selektivně
využívány	využívat	k5eAaPmNgInP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
byl	být	k5eAaImAgMnS	být
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
liberální	liberální	k2eAgFnSc2d1	liberální
Německé	německý	k2eAgFnSc2d1	německá
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
FBI	FBI	kA	FBI
měla	mít	k5eAaImAgFnS	mít
složku	složka	k1gFnSc4	složka
zvící	zvící	k2eAgFnSc4d1	zvící
1	[number]	k4	1
427	[number]	k4	427
stran	strana	k1gFnPc2	strana
o	o	k7c6	o
jeho	jeho	k3xOp3gFnPc6	jeho
aktivitách	aktivita	k1gFnPc6	aktivita
a	a	k8xC	a
doporučila	doporučit	k5eAaPmAgFnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
zamezeno	zamezit	k5eAaPmNgNnS	zamezit
přistěhovat	přistěhovat	k5eAaPmF	přistěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
USA	USA	kA	USA
podle	podle	k7c2	podle
zákonu	zákon	k1gInSc3	zákon
o	o	k7c4	o
vyloučení	vyloučení	k1gNnSc4	vyloučení
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Einstein	Einstein	k1gMnSc1	Einstein
"	"	kIx"	"
<g/>
věří	věřit	k5eAaImIp3nS	věřit
v	v	k7c4	v
<g/>
,	,	kIx,	,
radí	radit	k5eAaImIp3nS	radit
<g/>
,	,	kIx,	,
chrání	chránit	k5eAaImIp3nS	chránit
nebo	nebo	k8xC	nebo
učí	učit	k5eAaImIp3nS	učit
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
dovolila	dovolit	k5eAaPmAgFnS	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nerušeně	nerušeně	k6eAd1	nerušeně
přikradla	přikrást	k5eAaPmAgFnS	přikrást
anarchie	anarchie	k1gFnSc1	anarchie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c4	v
vládu	vláda	k1gFnSc4	vláda
pouze	pouze	k6eAd1	pouze
podle	podle	k7c2	podle
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
proti	proti	k7c3	proti
tyranským	tyranský	k2eAgFnPc3d1	tyranská
formám	forma	k1gFnPc3	forma
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
(	(	kIx(	(
<g/>
a	a	k8xC	a
svému	svůj	k3xOyFgInSc3	svůj
židovskému	židovský	k2eAgInSc3d1	židovský
původu	původ	k1gInSc3	původ
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
i	i	k9	i
proti	proti	k7c3	proti
nacistickému	nacistický	k2eAgInSc3d1	nacistický
režimu	režim	k1gInSc3	režim
a	a	k8xC	a
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
nastolení	nastolení	k1gNnSc6	nastolení
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
podporoval	podporovat	k5eAaImAgInS	podporovat
konstrukci	konstrukce	k1gFnSc4	konstrukce
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajistil	zajistit	k5eAaPmAgMnS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hitler	Hitler	k1gMnSc1	Hitler
ji	on	k3xPp3gFnSc4	on
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
poslal	poslat	k5eAaPmAgMnS	poslat
ještě	ještě	k9	ještě
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
dopis	dopis	k1gInSc1	dopis
prezidentovi	prezident	k1gMnSc3	prezident
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
(	(	kIx(	(
<g/>
dopis	dopis	k1gInSc1	dopis
byl	být	k5eAaImAgInS	být
spíše	spíše	k9	spíše
napsaný	napsaný	k2eAgInSc1d1	napsaný
Leó	Leó	k1gFnSc4	Leó
Szilárdem	Szilárd	k1gInSc7	Szilárd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
ho	on	k3xPp3gMnSc4	on
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
spustit	spustit	k5eAaPmF	spustit
program	program	k1gInSc4	program
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
pak	pak	k6eAd1	pak
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
lze	lze	k6eAd1	lze
uran	uran	k1gInSc4	uran
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
projektem	projekt	k1gInSc7	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
protestoval	protestovat	k5eAaBmAgMnS	protestovat
také	také	k9	také
proti	proti	k7c3	proti
inscenovaným	inscenovaný	k2eAgFnPc3d1	inscenovaná
tzv.	tzv.	kA	tzv.
politickým	politický	k2eAgMnPc3d1	politický
procesům	proces	k1gInPc3	proces
v	v	k7c6	v
totalitním	totalitní	k2eAgNnSc6d1	totalitní
Československu	Československo	k1gNnSc6	Československo
–	–	k?	–
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
justičním	justiční	k2eAgFnPc3d1	justiční
vraždám	vražda	k1gFnPc3	vražda
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
v	v	k7c6	v
telegramu	telegram	k1gInSc6	telegram
zaslaném	zaslaný	k2eAgInSc6d1	zaslaný
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1950	[number]	k4	1950
jeho	jeho	k3xOp3gMnSc3	jeho
představiteli	představitel	k1gMnSc3	představitel
Klementu	Klement	k1gMnSc3	Klement
Gottwaldovi	Gottwald	k1gMnSc3	Gottwald
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Prosím	prosit	k5eAaImIp1nS	prosit
Vás	vy	k3xPp2nPc4	vy
o	o	k7c4	o
nevykonání	nevykonání	k1gNnSc4	nevykonání
rozsudku	rozsudek	k1gInSc2	rozsudek
vyneseného	vynesený	k2eAgMnSc4d1	vynesený
nad	nad	k7c7	nad
Miladou	Milada	k1gFnSc7	Milada
Horákovou	Horáková	k1gFnSc7	Horáková
<g/>
,	,	kIx,	,
Závišem	Záviš	k1gMnSc7	Záviš
Kalandrou	Kalandra	k1gFnSc7	Kalandra
<g/>
,	,	kIx,	,
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Peclem	Pecl	k1gMnSc7	Pecl
a	a	k8xC	a
Janem	Jan	k1gMnSc7	Jan
Buchalem	Buchal	k1gMnSc7	Buchal
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
oběťmi	oběť	k1gFnPc7	oběť
nacismu	nacismus	k1gInSc2	nacismus
<g/>
,	,	kIx,	,
vězni	vězeň	k1gMnPc1	vězeň
německých	německý	k2eAgInPc2d1	německý
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
hluboce	hluboko	k6eAd1	hluboko
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
Einstein	Einstein	k1gMnSc1	Einstein
lobboval	lobbovat	k5eAaImAgMnS	lobbovat
za	za	k7c4	za
jaderné	jaderný	k2eAgNnSc4d1	jaderné
odzbrojení	odzbrojení	k1gNnSc4	odzbrojení
a	a	k8xC	a
světovou	světový	k2eAgFnSc4d1	světová
vládu	vláda	k1gFnSc4	vláda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
bojovat	bojovat	k5eAaImF	bojovat
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
čtvrté	čtvrtá	k1gFnSc6	čtvrtá
to	ten	k3xDgNnSc1	ten
budou	být	k5eAaImBp3nP	být
klacky	klacek	k1gInPc1	klacek
a	a	k8xC	a
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
podporoval	podporovat	k5eAaImAgMnS	podporovat
sionismus	sionismus	k1gInSc4	sionismus
a	a	k8xC	a
židovské	židovský	k2eAgNnSc4d1	Židovské
osídlení	osídlení	k1gNnSc4	osídlení
Palestiny	Palestina	k1gFnSc2	Palestina
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
při	při	k7c6	při
zakládání	zakládání	k1gNnSc6	zakládání
Hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
vydala	vydat	k5eAaPmAgFnS	vydat
svazek	svazek	k1gInSc4	svazek
O	o	k7c6	o
sionismu	sionismus	k1gInSc6	sionismus
<g/>
:	:	kIx,	:
Proslovy	proslov	k1gInPc4	proslov
a	a	k8xC	a
přednášky	přednáška	k1gFnPc4	přednáška
profesora	profesor	k1gMnSc2	profesor
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
a	a	k8xC	a
které	který	k3yRgInPc4	který
Einstein	Einstein	k1gMnSc1	Einstein
odkázal	odkázat	k5eAaPmAgMnS	odkázat
své	svůj	k3xOyFgInPc4	svůj
dokumenty	dokument	k1gInPc4	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
opovrhoval	opovrhovat	k5eAaImAgMnS	opovrhovat
nacionalismem	nacionalismus	k1gInSc7	nacionalismus
a	a	k8xC	a
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
pochyby	pochyba	k1gFnPc4	pochyba
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
židovský	židovský	k2eAgInSc1d1	židovský
stát	stát	k1gInSc1	stát
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
řešením	řešení	k1gNnSc7	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
si	se	k3xPyFc3	se
představoval	představovat	k5eAaImAgMnS	představovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
Arabové	Arab	k1gMnPc1	Arab
žít	žít	k5eAaImF	žít
spolu	spolu	k6eAd1	spolu
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Chajima	Chajim	k1gMnSc4	Chajim
Weizmanna	Weizmann	k1gMnSc4	Weizmann
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
izraelským	izraelský	k2eAgMnSc7d1	izraelský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Einstein	Einstein	k1gMnSc1	Einstein
nabídku	nabídka	k1gFnSc4	nabídka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
hluboce	hluboko	k6eAd1	hluboko
dojat	dojmout	k5eAaPmNgInS	dojmout
nabídkou	nabídka	k1gFnSc7	nabídka
od	od	k7c2	od
našeho	náš	k3xOp1gInSc2	náš
státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
smutný	smutný	k2eAgMnSc1d1	smutný
a	a	k8xC	a
zahanbený	zahanbený	k2eAgMnSc1d1	zahanbený
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nemohu	moct	k5eNaImIp1nS	moct
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
předmětnými	předmětný	k2eAgFnPc7d1	předmětná
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mi	já	k3xPp1nSc3	já
chybí	chybět	k5eAaImIp3nS	chybět
jak	jak	k6eAd1	jak
přirozené	přirozený	k2eAgFnPc4d1	přirozená
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zkušenosti	zkušenost	k1gFnPc1	zkušenost
ke	k	k7c3	k
správnému	správný	k2eAgNnSc3d1	správné
jednání	jednání	k1gNnSc3	jednání
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
vykonávání	vykonávání	k1gNnSc3	vykonávání
úředních	úřední	k2eAgFnPc2d1	úřední
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
Schweitzerem	Schweitzero	k1gNnSc7	Schweitzero
a	a	k8xC	a
Bertrandem	Bertrando	k1gNnSc7	Bertrando
Russellem	Russell	k1gInSc7	Russell
bojoval	bojovat	k5eAaImAgInS	bojovat
proti	proti	k7c3	proti
jaderným	jaderný	k2eAgInPc3d1	jaderný
testům	test	k1gInPc3	test
a	a	k8xC	a
bombám	bomba	k1gFnPc3	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mírovou	mírový	k2eAgFnSc7d1	mírová
skupinou	skupina	k1gFnSc7	skupina
Pugwash	Pugwash	k1gMnSc1	Pugwash
Conferences	Conferences	k1gMnSc1	Conferences
on	on	k3xPp3gMnSc1	on
Science	Science	k1gFnSc1	Science
and	and	k?	and
World	World	k1gInSc4	World
Affairs	Affairsa	k1gFnPc2	Affairsa
a	a	k8xC	a
Bertrandem	Bertrando	k1gNnSc7	Bertrando
Russellem	Russell	k1gInSc7	Russell
vydal	vydat	k5eAaPmAgMnS	vydat
Russelův-Einsteinův	Russelův-Einsteinův	k2eAgInSc4d1	Russelův-Einsteinův
manifest	manifest	k1gInSc4	manifest
a	a	k8xC	a
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
několik	několik	k4yIc4	několik
konferencí	konference	k1gFnPc2	konference
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popularita	popularita	k1gFnSc1	popularita
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
dopad	dopad	k1gInSc1	dopad
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zábava	zábava	k1gFnSc1	zábava
===	===	k?	===
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
témata	téma	k1gNnPc4	téma
mnoha	mnoho	k4c2	mnoho
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
a	a	k8xC	a
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
filmů	film	k1gInPc2	film
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
Roega	Roega	k1gFnSc1	Roega
Nedůležitost	Nedůležitost	k1gFnSc1	Nedůležitost
<g/>
,	,	kIx,	,
Freda	Freda	k1gMnSc1	Freda
Schepisi	Schepise	k1gFnSc4	Schepise
I.Q.	I.Q.	k1gFnSc2	I.Q.
a	a	k8xC	a
románu	román	k1gInSc2	román
Alana	Alan	k1gMnSc2	Alan
Lightmana	Lightman	k1gMnSc2	Lightman
Einsteinovy	Einsteinův	k2eAgInPc1d1	Einsteinův
sny	sen	k1gInPc1	sen
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
námětem	námět	k1gInSc7	námět
opery	opera	k1gFnSc2	opera
Philipa	Philip	k1gMnSc2	Philip
Glasse	Glass	k1gMnSc2	Glass
Einstein	Einstein	k1gMnSc1	Einstein
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používán	používat	k5eAaImNgInS	používat
jako	jako	k9	jako
model	model	k1gInSc1	model
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
excentrické	excentrický	k2eAgMnPc4d1	excentrický
fiktivní	fiktivní	k2eAgMnPc4d1	fiktivní
vědce	vědec	k1gMnPc4	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vlastní	vlastní	k2eAgInSc1d1	vlastní
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
odlišný	odlišný	k2eAgInSc4d1	odlišný
účes	účes	k1gInSc4	účes
prozrazovaly	prozrazovat	k5eAaImAgInP	prozrazovat
excentricitu	excentricita	k1gFnSc4	excentricita
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
bláznovství	bláznovství	k1gNnSc1	bláznovství
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
proto	proto	k8xC	proto
často	často	k6eAd1	často
kopírované	kopírovaný	k2eAgFnPc1d1	kopírovaná
a	a	k8xC	a
přeháněné	přeháněný	k2eAgFnPc1d1	přeháněná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
Einsteinových	Einsteinových	k2eAgNnPc2d1	Einsteinových
72	[number]	k4	72
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc2	on
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
neznámý	známý	k2eNgMnSc1d1	neznámý
fotograf	fotograf	k1gMnSc1	fotograf
přemluvit	přemluvit	k5eAaPmF	přemluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
usmál	usmát	k5eAaPmAgMnS	usmát
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Einstein	Einstein	k1gMnSc1	Einstein
tohle	tenhle	k3xDgNnSc4	tenhle
dělal	dělat	k5eAaImAgMnS	dělat
pro	pro	k7c4	pro
fotografy	fotograf	k1gMnPc4	fotograf
již	již	k6eAd1	již
mnohokrát	mnohokrát	k6eAd1	mnohokrát
<g/>
,	,	kIx,	,
vyplázl	vypláznout	k5eAaPmAgMnS	vypláznout
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Obrázek	obrázek	k1gInSc1	obrázek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ikonou	ikona	k1gFnSc7	ikona
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
kultuře	kultura	k1gFnSc6	kultura
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
kontrast	kontrast	k1gInSc4	kontrast
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
geniální	geniální	k2eAgMnSc1d1	geniální
vědec	vědec	k1gMnSc1	vědec
je	být	k5eAaImIp3nS	být
zachycen	zachytit	k5eAaPmNgMnS	zachytit
v	v	k7c6	v
humorném	humorný	k2eAgInSc6d1	humorný
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Yahoo	Yahoo	k1gMnSc1	Yahoo
Serious	Serious	k1gMnSc1	Serious
<g/>
,	,	kIx,	,
australský	australský	k2eAgMnSc1d1	australský
filmař	filmař	k1gMnSc1	filmař
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
tuto	tento	k3xDgFnSc4	tento
fotografii	fotografia	k1gFnSc4	fotografia
jako	jako	k8xC	jako
inspiraci	inspirace	k1gFnSc4	inspirace
k	k	k7c3	k
záměrně	záměrně	k6eAd1	záměrně
anachronickému	anachronický	k2eAgInSc3d1	anachronický
filmu	film	k1gInSc3	film
Mladý	mladý	k2eAgMnSc1d1	mladý
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
známka	známka	k1gFnSc1	známka
jména	jméno	k1gNnSc2	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
obchodní	obchodní	k2eAgFnSc1d1	obchodní
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vlastní	vlastní	k2eAgMnSc1d1	vlastní
The	The	k1gMnSc1	The
Roger	Rogra	k1gFnPc2	Rogra
Richman	Richman	k1gMnSc1	Richman
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc2	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
řídí	řídit	k5eAaImIp3nS	řídit
komerční	komerční	k2eAgNnPc4d1	komerční
použití	použití	k1gNnPc4	použití
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Reklamy	reklama	k1gFnPc1	reklama
a	a	k8xC	a
inzerce	inzerce	k1gFnPc1	inzerce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
podobu	podoba	k1gFnSc4	podoba
a	a	k8xC	a
obrázek	obrázek	k1gInSc4	obrázek
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
licencovány	licencovat	k5eAaBmNgInP	licencovat
touto	tento	k3xDgFnSc7	tento
agenturou	agentura	k1gFnSc7	agentura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
případě	případ	k1gInSc6	případ
agentura	agentura	k1gFnSc1	agentura
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Hebrejskou	hebrejský	k2eAgFnSc4d1	hebrejská
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Einstein	Einstein	k1gMnSc1	Einstein
aktivně	aktivně	k6eAd1	aktivně
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
instituce	instituce	k1gFnSc1	instituce
má	mít	k5eAaImIp3nS	mít
zisk	zisk	k1gInSc4	zisk
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
licenčních	licenční	k2eAgInPc2d1	licenční
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
agentura	agentura	k1gFnSc1	agentura
může	moct	k5eAaImIp3nS	moct
zcela	zcela	k6eAd1	zcela
zabránit	zabránit	k5eAaPmF	zabránit
užití	užití	k1gNnSc3	užití
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neshodoval	shodovat	k5eNaImAgMnS	shodovat
s	s	k7c7	s
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
představou	představa	k1gFnSc7	představa
této	tento	k3xDgFnSc2	tento
obchodní	obchodní	k2eAgFnSc2d1	obchodní
známky	známka	k1gFnSc2	známka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
agentury	agentura	k1gFnSc2	agentura
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
Albertu	Albert	k1gMnSc3	Albert
Einsteinovi	Einstein	k1gMnSc3	Einstein
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Kdykoli	kdykoli	k6eAd1	kdykoli
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
'	'	kIx"	'
<g/>
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
'	'	kIx"	'
kopírováno	kopírován	k2eAgNnSc1d1	kopírováno
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
vždy	vždy	k6eAd1	vždy
být	být	k5eAaImF	být
symbol	symbol	k1gInSc4	symbol
TM	TM	kA	TM
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Památka	památka	k1gFnSc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972	[number]	k4	1972
až	až	k9	až
1984	[number]	k4	1984
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
bankovky	bankovka	k1gFnSc2	bankovka
o	o	k7c6	o
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
5	[number]	k4	5
izraelských	izraelský	k2eAgFnPc2d1	izraelská
lir	lira	k1gFnPc2	lira
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
aversní	aversní	k2eAgFnSc6d1	aversní
straně	strana	k1gFnSc6	strana
byl	být	k5eAaImAgInS	být
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
portrét	portrét	k1gInSc1	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
reversní	reversní	k2eAgFnSc6d1	reversní
straně	strana	k1gFnSc6	strana
bankovky	bankovka	k1gFnSc2	bankovka
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazen	k2eAgInSc1d1	vyobrazen
reaktor	reaktor	k1gInSc1	reaktor
jaderného	jaderný	k2eAgNnSc2d1	jaderné
výzkumného	výzkumný	k2eAgNnSc2d1	výzkumné
centra	centrum	k1gNnSc2	centrum
Sorek	Sorky	k1gFnPc2	Sorky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
pár	pár	k4xCyI	pár
vlaků	vlak	k1gInPc2	vlak
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
350	[number]	k4	350
<g/>
/	/	kIx~	/
<g/>
353	[number]	k4	353
<g/>
)	)	kIx)	)
jezdící	jezdící	k2eAgFnSc2d1	jezdící
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Domažlice	Domažlice	k1gFnPc1	Domažlice
-	-	kIx~	-
Regensburg	Regensburg	k1gInSc1	Regensburg
-	-	kIx~	-
Mnichov	Mnichov	k1gInSc1	Mnichov
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
letech	let	k1gInPc6	let
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
otevřeno	otevřen	k2eAgNnSc1d1	otevřeno
Einsteinovo	Einsteinův	k2eAgNnSc1d1	Einsteinovo
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einsteinovo	Einsteinův	k2eAgNnSc1d1	Einsteinovo
muzeum	muzeum	k1gNnSc1	muzeum
je	být	k5eAaImIp3nS	být
též	též	k9	též
v	v	k7c6	v
Landau	Landaus	k1gInSc6	Landaus
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
hádanka	hádanka	k1gFnSc1	hádanka
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c4	o
Albertu	Alberta	k1gFnSc4	Alberta
EinsteinoviCOMAY	EinsteinoviCOMAY	k1gFnSc2	EinsteinoviCOMAY
<g/>
,	,	kIx,	,
Joan	Joan	k1gNnSc1	Joan
<g/>
;	;	kIx,	;
COHN-SHERBOK	COHN-SHERBOK	k1gFnPc1	COHN-SHERBOK
<g/>
,	,	kIx,	,
Lavinia	Lavinium	k1gNnPc1	Lavinium
<g/>
.	.	kIx.	.
</s>
<s>
Who	Who	k?	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Who	Who	k1gMnSc7	Who
in	in	k?	in
Jewish	Jewish	k1gInSc1	Jewish
History	Histor	k1gInPc1	Histor
<g/>
:	:	kIx,	:
After	After	k1gInSc1	After
the	the	k?	the
Period	perioda	k1gFnPc2	perioda
of	of	k?	of
the	the	k?	the
Old	Olda	k1gFnPc2	Olda
Testament	testament	k1gInSc1	testament
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
407	[number]	k4	407
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
415260305	[number]	k4	415260305
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Clark	Clark	k1gInSc1	Clark
<g/>
,	,	kIx,	,
Ronald	Ronald	k1gMnSc1	Ronald
W.	W.	kA	W.
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Life	Lif	k1gFnSc2	Lif
and	and	k?	and
Times	Times	k1gMnSc1	Times
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-380-44123-3	[number]	k4	0-380-44123-3
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pais	Pais	k1gInSc1	Pais
<g/>
,	,	kIx,	,
Abraham	Abraham	k1gMnSc1	Abraham
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Subtle	Subtle	k1gFnSc1	Subtle
is	is	k?	is
the	the	k?	the
Lord	lord	k1gMnSc1	lord
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Science	Science	k1gFnSc1	Science
and	and	k?	and
the	the	k?	the
Life	Lif	k1gFnSc2	Lif
of	of	k?	of
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-19-520438-7	[number]	k4	0-19-520438-7
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hart	Hart	k1gMnSc1	Hart
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
M.	M.	kA	M.
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
One	One	k1gMnSc1	One
Hundred	Hundred	k1gMnSc1	Hundred
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Ranking	Ranking	k1gInSc1	Ranking
of	of	k?	of
the	the	k?	the
Most	most	k1gInSc1	most
Influential	Influential	k1gMnSc1	Influential
Persons	Personsa	k1gFnPc2	Personsa
in	in	k?	in
History	Histor	k1gInPc1	Histor
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-8065-1350-0	[number]	k4	0-8065-1350-0
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Levenson	Levenson	k1gMnSc1	Levenson
<g/>
,	,	kIx,	,
T.	T.	kA	T.
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7252-101-2	[number]	k4	80-7252-101-2
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neffe	Neff	k1gMnSc5	Neff
<g/>
,	,	kIx,	,
Jürgen	Jürgen	k1gInSc1	Jürgen
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Životopis	životopis	k1gInSc1	životopis
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7203-742-0	[number]	k4	80-7203-742-0
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
15	[number]	k4	15
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
:	:	kIx,	:
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
–	–	k?	–
<g/>
Enz	Enz	k1gMnSc1	Enz
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
467	[number]	k4	467
<g/>
–	–	k?	–
<g/>
610	[number]	k4	610
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
504	[number]	k4	504
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
551	[number]	k4	551
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
Isaacson	Isaacson	k1gMnSc1	Isaacson
<g/>
,	,	kIx,	,
Einsten	Einsten	k2eAgMnSc1d1	Einsten
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
Audioteka	Audioteka	k1gFnSc1	Audioteka
<g/>
,	,	kIx,	,
načetl	načíst	k5eAaBmAgMnS	načíst
Vladislav	Vladislav	k1gMnSc1	Vladislav
BenešPublikace	BenešPublikace	k1gFnSc2	BenešPublikace
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
</s>
</p>
<p>
<s>
anglickyIdeas	anglickyIdeas	k1gInSc1	anglickyIdeas
&	&	k?	&
Opinions	Opinions	k1gInSc1	Opinions
ISBN	ISBN	kA	ISBN
0-517-00393-7	[number]	k4	0-517-00393-7
</s>
</p>
<p>
<s>
World	World	k6eAd1	World
As	as	k9	as
I	i	k9	i
See	See	k1gMnSc1	See
It	It	k1gFnSc2	It
ISBN	ISBN	kA	ISBN
0-8065-0711-X	[number]	k4	0-8065-0711-X
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
1949	[number]	k4	1949
</s>
</p>
<p>
<s>
Relativity	relativita	k1gFnPc1	relativita
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Special	Special	k1gMnSc1	Special
and	and	k?	and
General	General	k1gMnSc1	General
Theory	Theora	k1gFnSc2	Theora
ISBN	ISBN	kA	ISBN
0-517-88441-0	[number]	k4	0-517-88441-0
(	(	kIx(	(
<g/>
Project	Project	k1gMnSc1	Project
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
E-text	Eext	k1gMnSc1	E-text
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Electrodynamics	Electrodynamics	k1gInSc1	Electrodynamics
of	of	k?	of
Moving	Moving	k1gInSc1	Moving
Bodies	Bodies	k1gInSc1	Bodies
<g/>
"	"	kIx"	"
Annalen	Annalen	k2eAgInSc1d1	Annalen
der	drát	k5eAaImRp2nS	drát
Physik	Physik	k1gMnSc1	Physik
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Does	Does	k1gInSc1	Does
the	the	k?	the
Inertia	Inertia	k1gFnSc1	Inertia
of	of	k?	of
a	a	k8xC	a
Body	bod	k1gInPc4	bod
Depend	Dependa	k1gFnPc2	Dependa
Upon	Upona	k1gFnPc2	Upona
Its	Its	k1gFnSc2	Its
Energy	Energ	k1gMnPc4	Energ
Content	Content	k1gInSc4	Content
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Annalen	Annalen	k2eAgInSc4d1	Annalen
der	drát	k5eAaImRp2nS	drát
Physik	Physik	k1gMnSc1	Physik
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Inaugural	Inaugural	k1gMnSc5	Inaugural
Lecture	Lectur	k1gMnSc5	Lectur
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Prussian	Prussian	k1gInSc1	Prussian
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Generalized	Generalized	k1gMnSc1	Generalized
Theory	Theora	k1gFnSc2	Theora
of	of	k?	of
Gravitation	Gravitation	k1gInSc1	Gravitation
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Duben	duben	k1gInSc1	duben
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
<g/>
českyEINSTEIN	českyEINSTEIN	k?	českyEINSTEIN
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
;	;	kIx,	;
INFELD	INFELD	kA	INFELD
<g/>
,	,	kIx,	,
Leopold	Leopold	k1gMnSc1	Leopold
<g/>
.	.	kIx.	.
</s>
<s>
Fysika	fysika	k1gFnSc1	fysika
jako	jako	k8xS	jako
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
285	[number]	k4	285
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
S.	S.	kA	S.
Morgan	morgan	k1gMnSc1	morgan
Friedman	Friedman	k1gMnSc1	Friedman
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
Online	Onlin	k1gInSc5	Onlin
–	–	k?	–
Vyčerpávající	vyčerpávající	k2eAgInSc4d1	vyčerpávající
seznam	seznam	k1gInSc4	seznam
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
texty	text	k1gInPc4	text
o	o	k7c6	o
Albertu	Albert	k1gMnSc6	Albert
Einsteinovi	Einstein	k1gMnSc6	Einstein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
online	onlinout	k5eAaPmIp3nS	onlinout
Einsteinův	Einsteinův	k2eAgInSc4d1	Einsteinův
archiv	archiv	k1gInSc4	archiv
–	–	k?	–
3	[number]	k4	3
000	[number]	k4	000
dokumentů	dokument	k1gInPc2	dokument
</s>
</p>
<p>
<s>
Žijící	žijící	k2eAgMnSc1d1	žijící
Einstein	Einstein	k1gMnSc1	Einstein
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Planckova	Planckův	k2eAgInSc2d1	Planckův
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
institutu	institut	k1gInSc2	institut
</s>
</p>
<p>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Amerického	americký	k2eAgInSc2d1	americký
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
institutu	institut	k1gInSc2	institut
</s>
</p>
<p>
<s>
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
univerzita	univerzita	k1gFnSc1	univerzita
–	–	k?	–
Archiv	archiv	k1gInSc1	archiv
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
</s>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
Kalifornském	kalifornský	k2eAgInSc6d1	kalifornský
technologickém	technologický	k2eAgInSc6d1	technologický
institutu	institut	k1gInSc6	institut
</s>
</p>
<p>
<s>
MacTutorova	MacTutorův	k2eAgFnSc1d1	MacTutorův
historie	historie	k1gFnSc1	historie
Einsteina	Einstein	k1gMnSc2	Einstein
</s>
</p>
<p>
<s>
Významné	významný	k2eAgInPc4d1	významný
okamžiky	okamžik	k1gInPc4	okamžik
v	v	k7c6	v
Einsteinově	Einsteinův	k2eAgInSc6d1	Einsteinův
životě	život	k1gInSc6	život
od	od	k7c2	od
Jürgena	Jürgen	k1gMnSc2	Jürgen
Schmidhubera	Schmidhuber	k1gMnSc2	Schmidhuber
</s>
</p>
<p>
<s>
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
dopis	dopis	k1gInSc1	dopis
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
</s>
</p>
<p>
<s>
FBI	FBI	kA	FBI
files	files	k1gMnSc1	files
–	–	k?	–
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
ohledně	ohledně	k7c2	ohledně
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvukové	zvukový	k2eAgInPc1d1	zvukový
úryvky	úryvek	k1gInPc1	úryvek
známých	známý	k2eAgInPc2d1	známý
proslovů	proslov	k1gInPc2	proslov
<g/>
:	:	kIx,	:
Atomová	atomový	k2eAgFnSc1d1	atomová
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
Závody	závod	k1gInPc1	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
(	(	kIx(	(
<g/>
Z	z	k7c2	z
archivu	archiv	k1gInSc2	archiv
časopisu	časopis	k1gInSc2	časopis
Time	Tim	k1gFnSc2	Tim
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOVA	nova	k1gFnSc1	nova
–	–	k?	–
Einstein	Einstein	k1gMnSc1	Einstein
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Big	Big	k1gFnSc7	Big
Idea	idea	k1gFnSc1	idea
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
rodiny	rodina	k1gFnSc2	rodina
</s>
</p>
<p>
<s>
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
hlas	hlas	k1gInSc1	hlas
v	v	k7c6	v
Archivu	archiv	k1gInSc6	archiv
rozhlasu	rozhlas	k1gInSc2	rozhlas
</s>
</p>
