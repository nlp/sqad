<s>
Charles	Charles	k1gMnSc1	Charles
Milles	Milles	k1gMnSc1	Milles
Manson	Manson	k1gMnSc1	Manson
(	(	kIx(	(
<g/>
*	*	kIx~	*
jako	jako	k8xS	jako
Charles	Charles	k1gMnSc1	Charles
Milles	Milles	k1gMnSc1	Milles
Maddox	Maddox	k1gInSc4	Maddox
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1934	[number]	k4	1934
Cincinnati	Cincinnati	k1gFnPc2	Cincinnati
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
americký	americký	k2eAgMnSc1d1	americký
zločinec	zločinec	k1gMnSc1	zločinec
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
komunity	komunita	k1gFnSc2	komunita
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Manson	Manson	k1gNnSc1	Manson
Family	Famila	k1gFnSc2	Famila
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Kathleen	Kathlena	k1gFnPc2	Kathlena
Maddoxová	Maddoxový	k2eAgFnSc1d1	Maddoxová
nebyla	být	k5eNaImAgFnS	být
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Kathleen	Kathleen	k1gInSc4	Kathleen
byla	být	k5eAaImAgFnS	být
alkoholička	alkoholička	k1gFnSc1	alkoholička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zařídit	zařídit	k5eAaPmF	zařídit
rodinné	rodinný	k2eAgNnSc4d1	rodinné
zázemí	zázemí	k1gNnSc4	zázemí
a	a	k8xC	a
chvíli	chvíle	k1gFnSc4	chvíle
po	po	k7c4	po
narození	narození	k1gNnSc4	narození
Charlese	Charles	k1gMnSc2	Charles
byla	být	k5eAaImAgFnS	být
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
za	za	k7c4	za
přepadení	přepadení	k1gNnSc4	přepadení
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
tak	tak	k6eAd1	tak
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
13	[number]	k4	13
let	léto	k1gNnPc2	léto
trávil	trávit	k5eAaImAgMnS	trávit
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
v	v	k7c6	v
nápravných	nápravný	k2eAgNnPc6d1	nápravné
zařízeních	zařízení	k1gNnPc6	zařízení
a	a	k8xC	a
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
za	za	k7c7	za
zločiny	zločin	k1gInPc7	zločin
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
krádeže	krádež	k1gFnPc1	krádež
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
přepadení	přepadení	k1gNnSc1	přepadení
<g/>
,	,	kIx,	,
podvody	podvod	k1gInPc1	podvod
a	a	k8xC	a
padělání	padělání	k1gNnSc1	padělání
kreditních	kreditní	k2eAgFnPc2d1	kreditní
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
pasák	pasák	k1gMnSc1	pasák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavou	hlava	k1gFnSc7	hlava
skupiny	skupina	k1gFnSc2	skupina
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Rodina	rodina	k1gFnSc1	rodina
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Family	Famila	k1gFnSc2	Famila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
posléze	posléze	k6eAd1	posléze
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
několik	několik	k4yIc1	několik
brutálních	brutální	k2eAgFnPc2d1	brutální
vražd	vražda	k1gFnPc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
vraždu	vražda	k1gFnSc4	vražda
Sharon	Sharon	k1gInSc4	Sharon
Tate	Tat	k1gInSc2	Tat
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
režiséra	režisér	k1gMnSc2	režisér
Romana	Roman	k1gMnSc2	Roman
Polanského	Polanský	k1gMnSc2	Polanský
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
osmém	osmý	k4xOgInSc6	osmý
měsíci	měsíc	k1gInSc6	měsíc
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Manson	Manson	k1gInSc1	Manson
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
případu	případ	k1gInSc6	případ
známém	známý	k2eAgInSc6d1	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Tate-La	Tate-L	k2eAgFnSc1d1	Tate-L
Bianca	Bianca	k1gFnSc1	Bianca
case	casat	k5eAaPmIp3nS	casat
<g/>
"	"	kIx"	"
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
zákonů	zákon	k1gInPc2	zákon
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
doživotí	doživotí	k1gNnSc6	doživotí
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
mu	on	k3xPp3gMnSc3	on
nebylo	být	k5eNaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
osobně	osobně	k6eAd1	osobně
spáchal	spáchat	k5eAaPmAgInS	spáchat
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
podvanácté	podvanácté	k4xO	podvanácté
zamítnuta	zamítnut	k2eAgFnSc1d1	zamítnuta
žádost	žádost	k1gFnSc1	žádost
o	o	k7c4	o
podmíněné	podmíněný	k2eAgNnSc4d1	podmíněné
propuštění	propuštění	k1gNnSc4	propuštění
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
šanci	šance	k1gFnSc4	šance
má	mít	k5eAaImIp3nS	mít
až	až	k9	až
po	po	k7c6	po
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
trest	trest	k1gInSc4	trest
si	se	k3xPyFc3	se
odpykává	odpykávat	k5eAaImIp3nS	odpykávat
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
Corcoran	Corcorana	k1gFnPc2	Corcorana
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
hudebníkem	hudebník	k1gMnSc7	hudebník
<g/>
,	,	kIx,	,
natočil	natočit	k5eAaBmAgMnS	natočit
několik	několik	k4yIc4	několik
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
první	první	k4xOgMnPc1	první
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
LIE	LIE	kA	LIE
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Love	lov	k1gInSc5	lov
&	&	k?	&
Terror	Terror	k1gMnSc1	Terror
Cult	Cult	k1gMnSc1	Cult
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
ho	on	k3xPp3gMnSc4	on
naučil	naučit	k5eAaPmAgMnS	naučit
hrát	hrát	k5eAaImF	hrát
jeho	jeho	k3xOp3gMnSc1	jeho
spoluvězeň	spoluvězeň	k1gMnSc1	spoluvězeň
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
uvěznění	uvěznění	k1gNnSc1	uvěznění
doslova	doslova	k6eAd1	doslova
ikonou	ikona	k1gFnSc7	ikona
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Hudebník	hudebník	k1gMnSc1	hudebník
Marilyn	Marilyn	k1gFnSc2	Marilyn
Manson	Manson	k1gMnSc1	Manson
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
pseudonym	pseudonym	k1gInSc4	pseudonym
složen	složit	k5eAaPmNgMnS	složit
z	z	k7c2	z
kombinace	kombinace	k1gFnSc2	kombinace
jména	jméno	k1gNnSc2	jméno
slavné	slavný	k2eAgFnSc2d1	slavná
americké	americký	k2eAgFnSc2d1	americká
herečky	herečka	k1gFnSc2	herečka
a	a	k8xC	a
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
a	a	k8xC	a
Mansona	Mansona	k1gFnSc1	Mansona
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svými	svůj	k3xOyFgMnPc7	svůj
tričky	tričko	k1gNnPc7	tričko
s	s	k7c7	s
Mansonovou	Mansonový	k2eAgFnSc7d1	Mansonový
tváří	tvář	k1gFnSc7	tvář
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Charlie	Charlie	k1gMnSc1	Charlie
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Surf	surf	k1gInSc1	surf
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c4	na
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Spaghetti	spaghetti	k1gInPc2	spaghetti
Incident	incident	k1gInSc1	incident
<g/>
?	?	kIx.	?
</s>
<s>
kapely	kapela	k1gFnSc2	kapela
Guns	Gunsa	k1gFnPc2	Gunsa
N	N	kA	N
<g/>
'	'	kIx"	'
Roses	Roses	k1gMnSc1	Roses
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Manson	Manson	k1gInSc1	Manson
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
soudních	soudní	k2eAgInPc6d1	soudní
sporech	spor	k1gInPc6	spor
údajně	údajně	k6eAd1	údajně
výtěžky	výtěžek	k1gInPc4	výtěžek
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
triček	tričko	k1gNnPc2	tričko
a	a	k8xC	a
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
putují	putovat	k5eAaImIp3nP	putovat
k	k	k7c3	k
pozůstalým	pozůstalý	k1gMnPc3	pozůstalý
po	po	k7c6	po
W.	W.	kA	W.
Frykowském	Frykowské	k1gNnSc6	Frykowské
<g/>
.	.	kIx.	.
</s>
<s>
Manson	Manson	k1gInSc1	Manson
byl	být	k5eAaImAgInS	být
také	také	k9	také
přítelem	přítel	k1gMnSc7	přítel
Dennise	Dennise	k1gFnSc2	Dennise
Wilsona	Wilsona	k1gFnSc1	Wilsona
z	z	k7c2	z
The	The	k1gFnPc2	The
Beach	Beacha	k1gFnPc2	Beacha
Boys	boy	k1gMnPc2	boy
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
textu	text	k1gInSc2	text
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Never	Never	k1gInSc1	Never
Learn	Learn	k1gNnSc1	Learn
Not	nota	k1gFnPc2	nota
To	to	k9	to
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Trent	Trent	k1gMnSc1	Trent
Reznor	Reznor	k1gMnSc1	Reznor
(	(	kIx(	(
<g/>
NIN	Nina	k1gFnPc2	Nina
<g/>
)	)	kIx)	)
nahrál	nahrát	k5eAaBmAgMnS	nahrát
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
alb	album	k1gNnPc2	album
v	v	k7c6	v
domě	dům	k1gInSc6	dům
na	na	k7c4	na
10050	[number]	k4	10050
Cielo	Cielo	k1gNnSc4	Cielo
Drive	drive	k1gInSc1	drive
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
zavražděni	zavraždit	k5eAaPmNgMnP	zavraždit
Sharon	Sharon	k1gMnSc1	Sharon
Tate	Tat	k1gMnSc2	Tat
<g/>
,	,	kIx,	,
Abigail	Abigail	k1gInSc1	Abigail
Folgerová	Folgerová	k1gFnSc1	Folgerová
<g/>
,	,	kIx,	,
Wojciech	Wojciech	k1gInSc1	Wojciech
(	(	kIx(	(
<g/>
Wojtek	Wojtek	k1gInSc1	Wojtek
<g/>
)	)	kIx)	)
Frykowski	Frykowski	k1gNnSc1	Frykowski
<g/>
,	,	kIx,	,
Jay	Jay	k1gFnSc1	Jay
Sebring	Sebring	k1gInSc1	Sebring
a	a	k8xC	a
Steven	Steven	k2eAgInSc1d1	Steven
Parent	Parent	k1gInSc1	Parent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
dům	dům	k1gInSc1	dům
zbořen	zbořit	k5eAaPmNgInS	zbořit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Reznor	Reznor	k1gMnSc1	Reznor
si	se	k3xPyFc3	se
z	z	k7c2	z
domu	dům	k1gInSc2	dům
odnesl	odnést	k5eAaPmAgMnS	odnést
vstupní	vstupní	k2eAgFnPc4d1	vstupní
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
ty	ten	k3xDgFnPc1	ten
samé	samý	k3xTgFnPc1	samý
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
Susan	Susan	k1gInSc4	Susan
Atkinsová	Atkinsová	k1gFnSc1	Atkinsová
napsala	napsat	k5eAaBmAgFnS	napsat
krví	krev	k1gFnSc7	krev
Tateové	Tateové	k2eAgNnSc1d1	Tateové
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
pig	pig	k?	pig
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prase	prase	k1gNnSc1	prase
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dveře	dveře	k1gFnPc1	dveře
Reznor	Reznora	k1gFnPc2	Reznora
údajně	údajně	k6eAd1	údajně
používal	používat	k5eAaImAgMnS	používat
jako	jako	k9	jako
vstupní	vstupní	k2eAgFnPc4d1	vstupní
dveře	dveře	k1gFnPc4	dveře
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
nahrávacímu	nahrávací	k2eAgNnSc3d1	nahrávací
studiu	studio	k1gNnSc3	studio
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Le	Le	k1gFnSc1	Le
Pig	Pig	k1gMnSc1	Pig
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
deska	deska	k1gFnSc1	deska
The	The	k1gMnSc1	The
Downward	Downward	k1gMnSc1	Downward
Spiral	Spiral	k1gMnSc1	Spiral
<g/>
.	.	kIx.	.
</s>
<s>
Norská	norský	k2eAgNnPc1d1	norské
aggrotech	aggrot	k1gInPc6	aggrot
kapela	kapela	k1gFnSc1	kapela
Combichrist	Combichrist	k1gInSc1	Combichrist
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
God	God	k1gFnSc1	God
Bless	Blessa	k1gFnPc2	Blessa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
žehnej	žehnat	k5eAaImRp2nS	žehnat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
sériových	sériový	k2eAgMnPc2d1	sériový
vrahů	vrah	k1gMnPc2	vrah
a	a	k8xC	a
teroristů	terorista	k1gMnPc2	terorista
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
Charles	Charles	k1gMnSc1	Charles
Manson	Manson	k1gMnSc1	Manson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
Charles	Charles	k1gMnSc1	Charles
Manson	Manson	k1gMnSc1	Manson
Superstar	superstar	k1gFnSc1	superstar
<g/>
.	.	kIx.	.
</s>
