<s>
Písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
vizuální	vizuální	k2eAgInSc4d1	vizuální
zápis	zápis	k1gInSc4	zápis
jazyka	jazyk	k1gInSc2	jazyk
standardizovanými	standardizovaný	k2eAgInPc7d1	standardizovaný
symboly	symbol	k1gInPc7	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
typy	typ	k1gInPc1	typ
písma	písmo	k1gNnSc2	písmo
jsou	být	k5eAaImIp3nP	být
piktogramy	piktogram	k1gInPc1	piktogram
nebo	nebo	k8xC	nebo
ideogramy	ideogram	k1gInPc1	ideogram
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
písem	písmo	k1gNnPc2	písmo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
3	[number]	k4	3
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
logografické	logografický	k2eAgFnSc2d1	logografický
(	(	kIx(	(
<g/>
slovní	slovní	k2eAgFnSc2d1	slovní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sylabické	sylabický	k2eAgNnSc4d1	sylabické
(	(	kIx(	(
<g/>
slabičné	slabičný	k2eAgNnSc4d1	slabičné
<g/>
)	)	kIx)	)
a	a	k8xC	a
alfabetické	alfabetický	k2eAgFnSc2d1	alfabetická
(	(	kIx(	(
<g/>
abecední	abecední	k2eAgFnSc2d1	abecední
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1	obecné
slovo	slovo	k1gNnSc1	slovo
pro	pro	k7c4	pro
symboly	symbol	k1gInPc4	symbol
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
je	být	k5eAaImIp3nS	být
grafém	grafém	k1gInSc1	grafém
(	(	kIx(	(
<g/>
znak	znak	k1gInSc1	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
(	(	kIx(	(
<g/>
též	též	k9	též
Glyf	Glyf	k1gInSc1	Glyf
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
grafická	grafický	k2eAgFnSc1d1	grafická
reprezentace	reprezentace	k1gFnSc1	reprezentace
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Glyfy	Glyf	k1gInPc1	Glyf
většiny	většina	k1gFnSc2	většina
písem	písmo	k1gNnPc2	písmo
tvoří	tvořit	k5eAaImIp3nS	tvořit
linie	linie	k1gFnSc1	linie
(	(	kIx(	(
<g/>
čáry	čára	k1gFnPc1	čára
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
lineární	lineární	k2eAgInPc1d1	lineární
(	(	kIx(	(
<g/>
čárové	čárový	k2eAgInPc1d1	čárový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
glyfy	glyf	k1gInPc1	glyf
nelineárních	lineární	k2eNgNnPc2d1	nelineární
písem	písmo	k1gNnPc2	písmo
tvořené	tvořený	k2eAgInPc1d1	tvořený
jinými	jiný	k2eAgInPc7d1	jiný
typy	typ	k1gInPc7	typ
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Písmu	písmo	k1gNnSc3	písmo
předchází	předcházet	k5eAaImIp3nP	předcházet
paleolitické	paleolitický	k2eAgInPc1d1	paleolitický
záznamy	záznam	k1gInPc1	záznam
počtu	počet	k1gInSc2	počet
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vlčí	vlčí	k2eAgInSc4d1	vlčí
radius	radius	k1gInSc4	radius
<g/>
)	)	kIx)	)
či	či	k8xC	či
snad	snad	k9	snad
i	i	k9	i
symbolů	symbol	k1gInPc2	symbol
pomocí	pomocí	k7c2	pomocí
zářezů	zářez	k1gInPc2	zářez
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
písmem	písmo	k1gNnSc7	písmo
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
jen	jen	k9	jen
proto-písmem	protoísm	k1gInSc7	proto-písm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
na	na	k7c6	na
želvích	želví	k2eAgInPc6d1	želví
krunýřích	krunýř	k1gInPc6	krunýř
nalezených	nalezený	k2eAgInPc6d1	nalezený
v	v	k7c4	v
Džia-chu	Džiaha	k1gFnSc4	Džia-cha
(	(	kIx(	(
<g/>
Jiahu	Jiaha	k1gFnSc4	Jiaha
<g/>
)	)	kIx)	)
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Che-nan	Cheany	k1gInPc2	Che-nany
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
datované	datovaný	k2eAgInPc1d1	datovaný
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc1	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
znalců	znalec	k1gMnPc2	znalec
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
spojovat	spojovat	k5eAaImF	spojovat
s	s	k7c7	s
pozdějšími	pozdní	k2eAgInPc7d2	pozdější
nejstaršími	starý	k2eAgInPc7d3	nejstarší
čínskými	čínský	k2eAgInPc7d1	čínský
hieroglyfy	hieroglyf	k1gInPc7	hieroglyf
na	na	k7c6	na
věšteckých	věštecký	k2eAgFnPc6d1	Věštecká
kostkách	kostka	k1gFnPc6	kostka
Džia-ku-wen	Džiauna	k1gFnPc2	Džia-ku-wna
(	(	kIx(	(
<g/>
Jiaguwen	Jiaguwna	k1gFnPc2	Jiaguwna
<g/>
)	)	kIx)	)
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
čínské	čínský	k2eAgNnSc1d1	čínské
písmo	písmo	k1gNnSc1	písmo
by	by	kYmCp3nS	by
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgNnSc1d3	nejstarší
písmo	písmo	k1gNnSc1	písmo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
výhrady	výhrada	k1gFnSc2	výhrada
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
dosud	dosud	k6eAd1	dosud
užívaného	užívaný	k2eAgNnSc2d1	užívané
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
mladší	mladý	k2eAgInPc1d2	mladší
neolitické	neolitický	k2eAgInPc1d1	neolitický
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
pravděpodobněji	pravděpodobně	k6eAd2	pravděpodobně
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
na	na	k7c4	na
více	hodně	k6eAd2	hodně
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
Dadiwan	Dadiwan	k1gInSc1	Dadiwan
<g/>
,	,	kIx,	,
Damaidi	Damaid	k1gMnPc1	Damaid
<g/>
,	,	kIx,	,
Banpo	Banpa	k1gFnSc5	Banpa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
nejstarší	starý	k2eAgNnSc4d3	nejstarší
známé	známý	k2eAgNnSc4d1	známé
písmo	písmo	k1gNnSc4	písmo
či	či	k8xC	či
proto-písmo	protoísmo	k6eAd1	proto-písmo
euroazijskoafrické	euroazijskoafrický	k2eAgFnSc6d1	euroazijskoafrický
oblasti	oblast	k1gFnSc6	oblast
považováno	považován	k2eAgNnSc1d1	považováno
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
nalezené	nalezený	k2eAgNnSc4d1	nalezené
písmo	písmo	k1gNnSc4	písmo
staroevropské	staroevropský	k2eAgFnSc2d1	staroevropský
podunajské	podunajský	k2eAgFnSc2d1	Podunajská
civilizace	civilizace	k1gFnSc2	civilizace
Vinča	Vinč	k1gInSc2	Vinč
<g/>
,	,	kIx,	,
datované	datovaný	k2eAgNnSc1d1	datované
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Bylo	být	k5eAaImAgNnS	být
rozeznáno	rozeznat	k5eAaPmNgNnS	rozeznat
již	již	k6eAd1	již
asi	asi	k9	asi
230	[number]	k4	230
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
mladší	mladý	k2eAgInPc1d2	mladší
nálezy	nález	k1gInPc1	nález
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Dispilio	Dispilio	k6eAd1	Dispilio
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
známé	známý	k2eAgNnSc1d1	známé
zaniklé	zaniklý	k2eAgNnSc1d1	zaniklé
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
logofonetické	logofonetický	k2eAgNnSc1d1	logofonetický
klínové	klínový	k2eAgNnSc1d1	klínové
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
piktogramů	piktogram	k1gInPc2	piktogram
u	u	k7c2	u
Sumerů	Sumer	k1gInPc2	Sumer
koncem	koncem	k7c2	koncem
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
více	hodně	k6eAd2	hodně
fonetickým	fonetický	k2eAgInPc3d1	fonetický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
bylo	být	k5eAaImAgNnS	být
následováno	následovat	k5eAaImNgNnS	následovat
logofonetickým	logofonetický	k2eAgNnSc7d1	logofonetický
písmem	písmo	k1gNnSc7	písmo
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
(	(	kIx(	(
<g/>
egyptské	egyptský	k2eAgInPc1d1	egyptský
hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
<g/>
)	)	kIx)	)
a	a	k8xC	a
písmem	písmo	k1gNnSc7	písmo
Induské	Induský	k2eAgFnSc6d1	Induský
dolině	dolina	k1gFnSc6	dolina
(	(	kIx(	(
<g/>
harappské	harappský	k2eAgNnSc1d1	harappský
písmo	písmo	k1gNnSc1	písmo
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
písmo	písmo	k1gNnSc1	písmo
objevilo	objevit	k5eAaPmAgNnS	objevit
mnohokrát	mnohokrát	k6eAd1	mnohokrát
<g/>
,	,	kIx,	,
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
civilizacemi	civilizace	k1gFnPc7	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
lineární	lineární	k2eAgNnSc1d1	lineární
písmo	písmo	k1gNnSc1	písmo
B	B	kA	B
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
znaky	znak	k1gInPc4	znak
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
,	,	kIx,	,
slabiky	slabika	k1gFnPc4	slabika
i	i	k8xC	i
logogramy	logogram	k1gInPc4	logogram
<g/>
.	.	kIx.	.
</s>
<s>
Mayská	mayský	k2eAgFnSc1d1	mayská
literatura	literatura	k1gFnSc1	literatura
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
kombinované	kombinovaný	k2eAgNnSc4d1	kombinované
logosylabické	logosylabický	k2eAgNnSc4d1	logosylabický
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Fénické	fénický	k2eAgNnSc1d1	fénické
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
egyptského	egyptský	k2eAgNnSc2d1	egyptské
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgInSc7	první
fonetickým	fonetický	k2eAgNnSc7d1	fonetické
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
základem	základ	k1gInSc7	základ
abeced	abeceda	k1gFnPc2	abeceda
(	(	kIx(	(
<g/>
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
<g/>
,	,	kIx,	,
řecké	řecký	k2eAgFnSc2d1	řecká
<g/>
,	,	kIx,	,
latinské	latinský	k2eAgFnSc2d1	Latinská
<g/>
,	,	kIx,	,
arabské	arabský	k2eAgFnSc2d1	arabská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teokratickém	teokratický	k2eAgInSc6d1	teokratický
Egyptě	Egypt	k1gInSc6	Egypt
ho	on	k3xPp3gMnSc4	on
podle	podle	k7c2	podle
mytologie	mytologie	k1gFnSc2	mytologie
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
bůh	bůh	k1gMnSc1	bůh
Thovt	Thovt	k1gMnSc1	Thovt
(	(	kIx(	(
<g/>
hiera	hiera	k1gMnSc1	hiera
glyfé	glyfá	k1gFnSc2	glyfá
-	-	kIx~	-
"	"	kIx"	"
<g/>
posvátný	posvátný	k2eAgInSc1d1	posvátný
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
Písmo	písmo	k1gNnSc4	písmo
svaté	svatá	k1gFnSc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
písma	písmo	k1gNnSc2	písmo
je	být	k5eAaImIp3nS	být
také	také	k9	také
úzce	úzko	k6eAd1	úzko
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
dostupností	dostupnost	k1gFnSc7	dostupnost
vhodného	vhodný	k2eAgInSc2d1	vhodný
materiálu	materiál	k1gInSc2	materiál
na	na	k7c4	na
zaznamenání	zaznamenání	k1gNnSc4	zaznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
bylo	být	k5eAaImAgNnS	být
zaznamenáváno	zaznamenávat	k5eAaImNgNnS	zaznamenávat
na	na	k7c4	na
pevný	pevný	k2eAgInSc4d1	pevný
materiál	materiál	k1gInSc4	materiál
(	(	kIx(	(
<g/>
kamen	kamna	k1gNnPc2	kamna
<g/>
,	,	kIx,	,
keramika	keramika	k1gFnSc1	keramika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
ohebný	ohebný	k2eAgInSc4d1	ohebný
(	(	kIx(	(
<g/>
papyrus	papyrus	k1gInSc4	papyrus
<g/>
,	,	kIx,	,
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
pergamen	pergamen	k1gInSc4	pergamen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
státním	státní	k2eAgInSc7d1	státní
monopolem	monopol	k1gInSc7	monopol
či	či	k8xC	či
i	i	k9	i
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Jednotnosti	jednotnost	k1gFnPc1	jednotnost
písma	písmo	k1gNnSc2	písmo
bylo	být	k5eAaImAgNnS	být
dosahováno	dosahovat	k5eAaImNgNnS	dosahovat
centrální	centrální	k2eAgFnSc7d1	centrální
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
čínský	čínský	k2eAgMnSc1d1	čínský
císař	císař	k1gMnSc1	císař
Čchin	Čchin	k1gMnSc1	Čchin
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-chuang-ti	huang-ť	k1gFnSc2	-chuang-ť
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
první	první	k4xOgMnSc1	první
středověký	středověký	k2eAgMnSc1d1	středověký
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
zavedl	zavést	k5eAaPmAgMnS	zavést
karolinské	karolinský	k2eAgFnPc4d1	Karolinská
minuskuly	minuskula	k1gFnPc4	minuskula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlaholice	hlaholice	k1gFnSc1	hlaholice
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
knížetem	kníže	k1gMnSc7	kníže
Rostislavem	Rostislav	k1gMnSc7	Rostislav
(	(	kIx(	(
<g/>
po	po	k7c4	po
svolení	svolení	k1gNnSc4	svolení
od	od	k7c2	od
byzantského	byzantský	k2eAgMnSc2d1	byzantský
císaře	císař	k1gMnSc2	císař
Michala	Michal	k1gMnSc2	Michal
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dané	daný	k2eAgNnSc1d1	dané
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
úzce	úzko	k6eAd1	úzko
spjaty	spjat	k2eAgFnPc1d1	spjata
dějiny	dějiny	k1gFnPc1	dějiny
pedagogiky	pedagogika	k1gFnSc2	pedagogika
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
uměle	uměle	k6eAd1	uměle
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
jazyky	jazyk	k1gInPc4	jazyk
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
inuktitutština	inuktitutština	k1gFnSc1	inuktitutština
či	či	k8xC	či
čerokézština	čerokézština	k1gFnSc1	čerokézština
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
důvodů	důvod	k1gInPc2	důvod
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
fonetická	fonetický	k2eAgFnSc1d1	fonetická
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
,	,	kIx,	,
Comenia	Comenium	k1gNnSc2	Comenium
Script	Scripta	k1gFnPc2	Scripta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Logogram	Logogram	k1gInSc1	Logogram
<g/>
.	.	kIx.	.
</s>
<s>
Logografická	Logografický	k2eAgNnPc1d1	Logografický
písma	písmo	k1gNnPc1	písmo
jsou	být	k5eAaImIp3nP	být
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
logogramů	logogram	k1gInPc2	logogram
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
každý	každý	k3xTgInSc1	každý
znak	znak	k1gInSc4	znak
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
jedno	jeden	k4xCgNnSc1	jeden
slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
morfém	morfém	k1gInSc1	morfém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
nutnost	nutnost	k1gFnSc1	nutnost
memorování	memorování	k1gNnSc2	memorování
(	(	kIx(	(
<g/>
učení	učení	k1gNnSc2	učení
se	se	k3xPyFc4	se
nazpaměť	nazpaměť	k6eAd1	nazpaměť
<g/>
)	)	kIx)	)
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
logogramů	logogram	k1gInPc2	logogram
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
významů	význam	k1gInPc2	význam
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnPc4d1	hlavní
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
logografických	logografický	k2eAgInPc2d1	logografický
systémů	systém	k1gInPc2	systém
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
alfabetickými	alfabetický	k2eAgInPc7d1	alfabetický
(	(	kIx(	(
<g/>
abecedními	abecední	k2eAgInPc7d1	abecední
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
protože	protože	k8xS	protože
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
obsažený	obsažený	k2eAgInSc1d1	obsažený
v	v	k7c6	v
symbolu	symbol	k1gInSc6	symbol
<g/>
,	,	kIx,	,
stejný	stejný	k2eAgInSc1d1	stejný
logografický	logografický	k2eAgInSc1d1	logografický
systém	systém	k1gInSc1	systém
může	moct	k5eAaImIp3nS	moct
teoreticky	teoreticky	k6eAd1	teoreticky
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
na	na	k7c6	na
reprezentaci	reprezentace	k1gFnSc6	reprezentace
různých	různý	k2eAgInPc2d1	různý
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k9	jen
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgInPc4d1	blízký
jazyky	jazyk	k1gInPc4	jazyk
jako	jako	k8xC	jako
dialekty	dialekt	k1gInPc4	dialekt
čínštiny	čínština	k1gFnSc2	čínština
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
syntaktická	syntaktický	k2eAgNnPc1d1	syntaktické
omezení	omezení	k1gNnPc1	omezení
redukují	redukovat	k5eAaBmIp3nP	redukovat
portabilitu	portabilita	k1gFnSc4	portabilita
(	(	kIx(	(
<g/>
přenositelnost	přenositelnost	k1gFnSc4	přenositelnost
<g/>
)	)	kIx)	)
daného	daný	k2eAgInSc2d1	daný
logografického	logografický	k2eAgInSc2d1	logografický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Korejština	korejština	k1gFnSc1	korejština
i	i	k8xC	i
japonština	japonština	k1gFnSc1	japonština
používají	používat	k5eAaImIp3nP	používat
čínské	čínský	k2eAgInPc1d1	čínský
logogramy	logogram	k1gInPc1	logogram
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
písmech	písmo	k1gNnPc6	písmo
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
symbolů	symbol	k1gInPc2	symbol
má	mít	k5eAaImIp3nS	mít
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
od	od	k7c2	od
čínštiny	čínština	k1gFnSc2	čínština
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
čínský	čínský	k2eAgInSc1d1	čínský
text	text	k1gInSc1	text
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
japonského	japonský	k2eAgMnSc4d1	japonský
nebo	nebo	k8xC	nebo
korejského	korejský	k2eAgMnSc4d1	korejský
čtenáře	čtenář	k1gMnSc4	čtenář
lehce	lehko	k6eAd1	lehko
srozumitelný	srozumitelný	k2eAgMnSc1d1	srozumitelný
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
většina	většina	k1gFnSc1	většina
jazyků	jazyk	k1gInPc2	jazyk
nepoužívá	používat	k5eNaImIp3nS	používat
kompletní	kompletní	k2eAgNnPc4d1	kompletní
logografická	logografický	k2eAgNnPc4d1	logografické
písma	písmo	k1gNnPc4	písmo
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
jazyků	jazyk	k1gInPc2	jazyk
používá	používat	k5eAaImIp3nS	používat
nějaké	nějaký	k3yIgInPc4	nějaký
logogramy	logogram	k1gInPc4	logogram
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
příklad	příklad	k1gInSc1	příklad
moderních	moderní	k2eAgInPc2d1	moderní
západních	západní	k2eAgInPc2d1	západní
logogramů	logogram	k1gInPc2	logogram
jsou	být	k5eAaImIp3nP	být
arabské	arabský	k2eAgFnPc1d1	arabská
číslice	číslice	k1gFnPc1	číslice
-	-	kIx~	-
každý	každý	k3xTgMnSc1	každý
uživatel	uživatel	k1gMnSc1	uživatel
těchto	tento	k3xDgInPc2	tento
symbolů	symbol	k1gInPc2	symbol
rozumí	rozumět	k5eAaImIp3nS	rozumět
co	co	k9	co
znamená	znamenat	k5eAaImIp3nS	znamenat
1	[number]	k4	1
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
různě	různě	k6eAd1	různě
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
one	one	k?	one
<g/>
,	,	kIx,	,
eins	eins	k1gInSc1	eins
<g/>
,	,	kIx,	,
uno	uno	k?	uno
nebo	nebo	k8xC	nebo
ichi	ich	k1gFnSc2	ich
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
západní	západní	k2eAgInPc1d1	západní
logogramy	logogram	k1gInPc1	logogram
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ampersand	ampersand	k1gInSc4	ampersand
&	&	k?	&
(	(	kIx(	(
<g/>
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
spojku	spojka	k1gFnSc4	spojka
a	a	k8xC	a
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
et	et	k?	et
<g/>
)	)	kIx)	)
a	a	k8xC	a
zavináč	zavináč	k1gInSc4	zavináč
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
at	at	k?	at
sign	signum	k1gNnPc2	signum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
@	@	kIx~	@
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kontextech	kontext	k1gInPc6	kontext
pro	pro	k7c4	pro
při	pře	k1gFnSc4	pře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
korespondenci	korespondence	k1gFnSc6	korespondence
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zavedly	zavést	k5eAaPmAgInP	zavést
logogramy	logogram	k1gInPc4	logogram
nálady	nálada	k1gFnSc2	nálada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiných	jiný	k2eAgInPc2d1	jiný
významů	význam	k1gInPc2	význam
-	-	kIx~	-
emotikony	emotikon	k1gInPc1	emotikon
a	a	k8xC	a
"	"	kIx"	"
<g/>
smajlíci	smajlíci	k?	smajlíci
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Logogramy	Logogram	k1gInPc1	Logogram
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývají	nazývat	k5eAaImIp3nP	nazývat
ideogramy	ideogram	k1gInPc1	ideogram
-	-	kIx~	-
slovo	slovo	k1gNnSc1	slovo
označující	označující	k2eAgInPc1d1	označující
symboly	symbol	k1gInPc1	symbol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
graficky	graficky	k6eAd1	graficky
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
abstraktní	abstraktní	k2eAgFnPc4d1	abstraktní
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lingvisté	lingvista	k1gMnPc1	lingvista
se	se	k3xPyFc4	se
tomuto	tento	k3xDgNnSc3	tento
použití	použití	k1gNnSc3	použití
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
sémanticko-fonetické	sémantickoonetický	k2eAgInPc1d1	sémanticko-fonetický
(	(	kIx(	(
<g/>
fonetika	fonetika	k1gFnSc1	fonetika
<g/>
=	=	kIx~	=
<g/>
hláskosloví	hláskosloví	k1gNnSc1	hláskosloví
<g/>
)	)	kIx)	)
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
symboly	symbol	k1gInPc1	symbol
obsahující	obsahující	k2eAgInSc1d1	obsahující
element	element	k1gInSc4	element
reprezentující	reprezentující	k2eAgInSc4d1	reprezentující
význam	význam	k1gInSc4	význam
a	a	k8xC	a
element	element	k1gInSc4	element
reprezentující	reprezentující	k2eAgFnSc1d1	reprezentující
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lingvisté	lingvista	k1gMnPc1	lingvista
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
mezi	mezi	k7c7	mezi
lexigrafií	lexigrafie	k1gFnSc7	lexigrafie
a	a	k8xC	a
ideografií	ideografie	k1gFnSc7	ideografie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
symboly	symbol	k1gInPc4	symbol
v	v	k7c6	v
lexigrafiích	lexigrafie	k1gFnPc6	lexigrafie
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
symboly	symbol	k1gInPc1	symbol
v	v	k7c6	v
ideografiích	ideografie	k1gFnPc6	ideografie
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
slova	slovo	k1gNnPc1	slovo
nebo	nebo	k8xC	nebo
morfémy	morfém	k1gInPc1	morfém
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc3d3	nejdůležitější
(	(	kIx(	(
<g/>
a	a	k8xC	a
jediné	jediný	k2eAgNnSc1d1	jediné
přežívající	přežívající	k2eAgNnSc1d1	přežívající
<g/>
)	)	kIx)	)
moderní	moderní	k2eAgNnSc1d1	moderní
logografické	logografický	k2eAgNnSc1d1	logografické
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
čínské	čínský	k2eAgNnSc1d1	čínské
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
stupněm	stupeň	k1gInSc7	stupeň
modifikace	modifikace	k1gFnSc2	modifikace
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
<g/>
,	,	kIx,	,
japonštině	japonština	k1gFnSc6	japonština
<g/>
,	,	kIx,	,
korejštině	korejština	k1gFnSc6	korejština
<g/>
,	,	kIx,	,
vietnamštině	vietnamština	k1gFnSc6	vietnamština
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
východoasijských	východoasijský	k2eAgInPc6d1	východoasijský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Starověké	starověký	k2eAgInPc1d1	starověký
egyptské	egyptský	k2eAgInPc1d1	egyptský
hieroglyfy	hieroglyf	k1gInPc1	hieroglyf
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k8xC	i
písmo	písmo	k1gNnSc4	písmo
Mayů	May	k1gMnPc2	May
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
logografické	logografický	k2eAgInPc4d1	logografický
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
seznam	seznam	k1gInSc1	seznam
logografických	logografický	k2eAgNnPc2d1	logografické
písem	písmo	k1gNnPc2	písmo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
článek	článek	k1gInSc4	článek
Seznam	seznam	k1gInSc4	seznam
písem	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
lze	lze	k6eAd1	lze
za	za	k7c4	za
obdobu	obdoba	k1gFnSc4	obdoba
logogramů	logogram	k1gInPc2	logogram
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
interpunkční	interpunkční	k2eAgNnPc4d1	interpunkční
znaménka	znaménko	k1gNnPc4	znaménko
včetně	včetně	k7c2	včetně
mezery	mezera	k1gFnSc2	mezera
(	(	kIx(	(
<g/>
rozdělující	rozdělující	k2eAgNnPc4d1	rozdělující
slova	slovo	k1gNnPc4	slovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neznamenají	znamenat	k5eNaImIp3nP	znamenat
sice	sice	k8xC	sice
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
písem	písmo	k1gNnPc2	písmo
a	a	k8xC	a
nesou	nést	k5eAaImIp3nP	nést
mezinárodně	mezinárodně	k6eAd1	mezinárodně
srozumitelnou	srozumitelný	k2eAgFnSc4d1	srozumitelná
informaci	informace	k1gFnSc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Slabičná	slabičný	k2eAgNnPc4d1	slabičné
písma	písmo	k1gNnPc4	písmo
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
sylabarie	sylabarie	k1gFnPc4	sylabarie
a	a	k8xC	a
abugidy	abugida	k1gFnPc4	abugida
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
seznam	seznam	k1gInSc1	seznam
slabičných	slabičný	k2eAgNnPc2d1	slabičné
písem	písmo	k1gNnPc2	písmo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
článek	článek	k1gInSc4	článek
Seznam	seznam	k1gInSc4	seznam
písem	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Sylabarie	Sylabarie	k1gFnSc1	Sylabarie
je	být	k5eAaImIp3nS	být
sada	sada	k1gFnSc1	sada
napsaných	napsaný	k2eAgInPc2d1	napsaný
symbolů	symbol	k1gInPc2	symbol
reprezentujících	reprezentující	k2eAgInPc2d1	reprezentující
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
aproximujících	aproximující	k2eAgInPc2d1	aproximující
<g/>
)	)	kIx)	)
slabiky	slabika	k1gFnPc1	slabika
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
v	v	k7c6	v
sylabarii	sylabarie	k1gFnSc6	sylabarie
typicky	typicky	k6eAd1	typicky
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
zvuk	zvuk	k1gInSc1	zvuk
souhlásky	souhláska	k1gFnSc2	souhláska
následovaný	následovaný	k2eAgInSc1d1	následovaný
zvukem	zvuk	k1gInSc7	zvuk
samohlásky	samohláska	k1gFnSc2	samohláska
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
samotnou	samotný	k2eAgFnSc7d1	samotná
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
sylabarii	sylabarie	k1gFnSc6	sylabarie
není	být	k5eNaImIp3nS	být
systematická	systematický	k2eAgFnSc1d1	systematická
grafická	grafický	k2eAgFnSc1d1	grafická
podobnost	podobnost	k1gFnSc1	podobnost
mezi	mezi	k7c7	mezi
foneticky	foneticky	k6eAd1	foneticky
souvisejícími	související	k2eAgInPc7d1	související
znaky	znak	k1gInPc7	znak
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
grafickou	grafický	k2eAgFnSc4d1	grafická
podobnost	podobnost	k1gFnSc4	podobnost
pro	pro	k7c4	pro
samohlásky	samohláska	k1gFnPc4	samohláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
např.	např.	kA	např.
znaky	znak	k1gInPc7	znak
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
ke	k	k7c3	k
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ka	ka	k?	ka
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
ko	ko	k?	ko
<g/>
"	"	kIx"	"
nemají	mít	k5eNaImIp3nP	mít
podobnost	podobnost	k1gFnSc4	podobnost
na	na	k7c6	na
indikaci	indikace	k1gFnSc6	indikace
jejich	jejich	k3xOp3gFnSc2	jejich
společné	společný	k2eAgFnSc2d1	společná
souhlásky	souhláska	k1gFnSc2	souhláska
"	"	kIx"	"
<g/>
k	k	k7c3	k
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sylabarie	Sylabarie	k1gFnPc4	Sylabarie
jsou	být	k5eAaImIp3nP	být
nejvhodnější	vhodný	k2eAgMnPc1d3	nejvhodnější
pro	pro	k7c4	pro
jazyky	jazyk	k1gInPc4	jazyk
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
slabikovou	slabikový	k2eAgFnSc7d1	slabiková
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
typické	typický	k2eAgInPc4d1	typický
např.	např.	kA	např.
pro	pro	k7c4	pro
východní	východní	k2eAgFnSc4d1	východní
Asii	Asie	k1gFnSc4	Asie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
korejština	korejština	k1gFnSc1	korejština
nebo	nebo	k8xC	nebo
japonština	japonština	k1gFnSc1	japonština
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc4d1	jiný
příklady	příklad	k1gInPc4	příklad
jazyků	jazyk	k1gInPc2	jazyk
používajících	používající	k2eAgInPc2d1	používající
slabikové	slabikový	k2eAgNnSc4d1	slabikové
psaní	psaní	k1gNnSc4	psaní
jsou	být	k5eAaImIp3nP	být
mykénská	mykénský	k2eAgFnSc1d1	mykénská
řečtina	řečtina	k1gFnSc1	řečtina
(	(	kIx(	(
<g/>
lineární	lineární	k2eAgNnSc1d1	lineární
písmo	písmo	k1gNnSc1	písmo
B	B	kA	B
<g/>
)	)	kIx)	)
a	a	k8xC	a
písma	písmo	k1gNnSc2	písmo
původních	původní	k2eAgInPc2d1	původní
amerických	americký	k2eAgInPc2d1	americký
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
abeceda	abeceda	k1gFnSc1	abeceda
kmene	kmen	k1gInSc2	kmen
Čerokíové	Čerokíová	k1gFnSc2	Čerokíová
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jazyky	jazyk	k1gInPc1	jazyk
starověkého	starověký	k2eAgInSc2d1	starověký
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
používaly	používat	k5eAaImAgFnP	používat
formy	forma	k1gFnPc1	forma
klínového	klínový	k2eAgNnSc2d1	klínové
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
sylabarií	sylabarie	k1gFnSc7	sylabarie
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
neslabikovými	slabikový	k2eNgInPc7d1	slabikový
elementy	element	k1gInPc7	element
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jazyky	jazyk	k1gInPc1	jazyk
-	-	kIx~	-
například	například	k6eAd1	například
indoevropské	indoevropský	k2eAgNnSc1d1	indoevropské
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
angličtina	angličtina	k1gFnSc1	angličtina
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
-	-	kIx~	-
užívají	užívat	k5eAaImIp3nP	užívat
komplexní	komplexní	k2eAgFnPc4d1	komplexní
slabikové	slabikový	k2eAgFnPc4d1	slabiková
struktury	struktura	k1gFnPc4	struktura
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
velkou	velký	k2eAgFnSc7d1	velká
zásobou	zásoba	k1gFnSc7	zásoba
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
zejména	zejména	k9	zejména
složených	složený	k2eAgFnPc2d1	složená
skupin	skupina	k1gFnPc2	skupina
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
jazycích	jazyk	k1gInPc6	jazyk
slabičné	slabičný	k2eAgNnSc1d1	slabičné
písmo	písmo	k1gNnSc4	písmo
nevhodné	vhodný	k2eNgNnSc4d1	nevhodné
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
možná	možný	k2eAgFnSc1d1	možná
slabika	slabika	k1gFnSc1	slabika
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
mít	mít	k5eAaImF	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
možných	možný	k2eAgFnPc2d1	možná
slabik	slabika	k1gFnPc2	slabika
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
není	být	k5eNaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
asi	asi	k9	asi
sto	sto	k4xCgNnSc1	sto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
evropských	evropský	k2eAgInPc6d1	evropský
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
mnoho	mnoho	k4c4	mnoho
tisíc	tisíc	k4xCgInSc4	tisíc
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
písmo	písmo	k1gNnSc4	písmo
by	by	kYmCp3nP	by
tak	tak	k9	tak
svou	svůj	k3xOyFgFnSc7	svůj
složitostí	složitost	k1gFnSc7	složitost
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
podobné	podobný	k2eAgNnSc1d1	podobné
čínskému	čínský	k2eAgNnSc3d1	čínské
písmu	písmo	k1gNnSc3	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Abugida	Abugida	k1gFnSc1	Abugida
oproti	oproti	k7c3	oproti
sylabarii	sylabarie	k1gFnSc3	sylabarie
má	mít	k5eAaImIp3nS	mít
grafémy	grafém	k1gInPc4	grafém
typicky	typicky	k6eAd1	typicky
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
slabiku	slabika	k1gFnSc4	slabika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znaky	znak	k1gInPc4	znak
reprezentující	reprezentující	k2eAgInPc4d1	reprezentující
související	související	k2eAgInPc4d1	související
zvuky	zvuk	k1gInPc4	zvuk
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
i	i	k9	i
graficky	graficky	k6eAd1	graficky
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
to	ten	k3xDgNnSc4	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
společná	společný	k2eAgFnSc1d1	společná
souhlásková	souhláskový	k2eAgFnSc1d1	souhlásková
báze	báze	k1gFnSc1	báze
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
jednotným	jednotný	k2eAgInSc7d1	jednotný
způsobem	způsob	k1gInSc7	způsob
reprezentace	reprezentace	k1gFnSc2	reprezentace
samohlásky	samohláska	k1gFnSc2	samohláska
v	v	k7c6	v
slabice	slabika	k1gFnSc6	slabika
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jednotná	jednotný	k2eAgFnSc1d1	jednotná
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
ligatura	ligatura	k1gFnSc1	ligatura
spojuje	spojovat	k5eAaImIp3nS	spojovat
původně	původně	k6eAd1	původně
samostatná	samostatný	k2eAgNnPc4d1	samostatné
písmena	písmeno	k1gNnPc4	písmeno
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
<g/>
)	)	kIx)	)
složky	složka	k1gFnPc4	složka
výsledný	výsledný	k2eAgInSc4d1	výsledný
slabičný	slabičný	k2eAgInSc4d1	slabičný
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Čistým	čistý	k2eAgInSc7d1	čistý
příkladem	příklad	k1gInSc7	příklad
takového	takový	k3xDgInSc2	takový
systému	systém	k1gInSc2	systém
písma	písmo	k1gNnSc2	písmo
je	být	k5eAaImIp3nS	být
korejské	korejský	k2eAgNnSc1d1	korejské
písmo	písmo	k1gNnSc1	písmo
hangul	hangul	k1gInSc1	hangul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
skládá	skládat	k5eAaImIp3nS	skládat
znaky	znak	k1gInPc4	znak
pro	pro	k7c4	pro
slabiky	slabika	k1gFnPc4	slabika
z	z	k7c2	z
písmen	písmeno	k1gNnPc2	písmeno
hláskové	hláskový	k2eAgFnSc2d1	hlásková
abecedy	abeceda	k1gFnSc2	abeceda
jamo	jamo	k1gNnSc1	jamo
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
typ	typ	k1gInSc1	typ
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
ho	on	k3xPp3gInSc4	on
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
nynější	nynější	k2eAgInPc4d1	nynější
jazyky	jazyk	k1gInPc4	jazyk
buď	buď	k8xC	buď
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
pro	pro	k7c4	pro
přepis	přepis	k1gInSc4	přepis
pro	pro	k7c4	pro
cizince	cizinec	k1gMnSc4	cizinec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pchin-jin	pchinin	k1gInSc1	pchin-jin
pro	pro	k7c4	pro
čínštinu	čínština	k1gFnSc4	čínština
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
samostatné	samostatný	k2eAgInPc4d1	samostatný
znaky	znak	k1gInPc4	znak
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
hlásky	hlásek	k1gInPc4	hlásek
<g/>
,	,	kIx,	,
elementární	elementární	k2eAgInPc4d1	elementární
prvky	prvek	k1gInPc4	prvek
mluvené	mluvený	k2eAgFnSc2d1	mluvená
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Znakově	znakově	k6eAd1	znakově
<g/>
,	,	kIx,	,
graficky	graficky	k6eAd1	graficky
je	být	k5eAaImIp3nS	být
nejjednodušší	jednoduchý	k2eAgFnSc7d3	nejjednodušší
možností	možnost	k1gFnSc7	možnost
<g/>
,	,	kIx,	,
postačí	postačit	k5eAaPmIp3nS	postačit
jen	jen	k9	jen
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
písmen	písmeno	k1gNnPc2	písmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
úsporu	úspora	k1gFnSc4	úspora
znaků	znak	k1gInPc2	znak
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jejich	jejich	k3xOp3gFnPc1	jejich
kombinace	kombinace	k1gFnPc1	kombinace
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc1d1	zvaná
spřežky	spřežka	k1gFnPc1	spřežka
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
spřežka	spřežka	k1gFnSc1	spřežka
"	"	kIx"	"
<g/>
ch	ch	k0	ch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
cz	cz	k?	cz
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
č	č	k0	č
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
spřežky	spřežka	k1gFnPc1	spřežka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
z	z	k7c2	z
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
dvou	dva	k4xCgInPc2	dva
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
např.	např.	kA	např.
německé	německý	k2eAgInPc1d1	německý
"	"	kIx"	"
<g/>
tsch	tsch	k1gInSc1	tsch
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
hlásku	hláska	k1gFnSc4	hláska
č.	č.	k?	č.
Jinou	jiný	k2eAgFnSc7d1	jiná
častou	častý	k2eAgFnSc7d1	častá
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
opatření	opatření	k1gNnSc1	opatření
základních	základní	k2eAgInPc2d1	základní
latinských	latinský	k2eAgInPc2d1	latinský
znaků	znak	k1gInPc2	znak
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
háčky	háček	k1gMnPc4	háček
a	a	k8xC	a
čárky	čárka	k1gFnPc4	čárka
nad	nad	k7c7	nad
základním	základní	k2eAgInSc7d1	základní
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
hlásková	hláskový	k2eAgNnPc4d1	hláskové
písma	písmo	k1gNnPc4	písmo
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
mezi	mezi	k7c7	mezi
klínovými	klínový	k2eAgNnPc7d1	klínové
písmy	písmo	k1gNnPc7	písmo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ugaritské	ugaritský	k2eAgFnPc1d1	ugaritská
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
většina	většina	k1gFnSc1	většina
klínových	klínový	k2eAgNnPc2d1	klínové
písem	písmo	k1gNnPc2	písmo
byla	být	k5eAaImAgFnS	být
slabičná	slabičný	k2eAgFnSc1d1	slabičná
nebo	nebo	k8xC	nebo
poloslabičná	poloslabičný	k2eAgFnSc1d1	poloslabičný
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgFnPc1d3	nejrozšířenější
jsou	být	k5eAaImIp3nP	být
písma	písmo	k1gNnPc1	písmo
založená	založený	k2eAgNnPc1d1	založené
na	na	k7c6	na
písmu	písmo	k1gNnSc6	písmo
starořímském	starořímský	k2eAgNnSc6d1	starořímské
(	(	kIx(	(
<g/>
latince	latinka	k1gFnSc6	latinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
<g/>
,	,	kIx,	,
písmu	písmo	k1gNnSc6	písmo
starořeckém	starořecký	k2eAgNnSc6d1	starořecké
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c4	na
písmo	písmo	k1gNnSc4	písmo
fénické	fénický	k2eAgNnSc4d1	fénické
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fénického	fénický	k2eAgInSc2d1	fénický
základu	základ	k1gInSc2	základ
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k8xC	i
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgNnPc2d1	ostatní
hláskových	hláskový	k2eAgNnPc2d1	hláskové
písem	písmo	k1gNnPc2	písmo
-	-	kIx~	-
jak	jak	k8xS	jak
písma	písmo	k1gNnPc1	písmo
semitská	semitský	k2eAgNnPc1d1	semitské
(	(	kIx(	(
<g/>
arabské	arabský	k2eAgInPc1d1	arabský
<g/>
,	,	kIx,	,
hebrejské	hebrejský	k2eAgInPc1d1	hebrejský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
která	který	k3yQgFnSc1	který
navázala	navázat	k5eAaPmAgFnS	navázat
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
islámu	islám	k1gInSc2	islám
i	i	k9	i
písma	písmo	k1gNnPc4	písmo
středoasijská	středoasijský	k2eAgNnPc4d1	středoasijské
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
písma	písmo	k1gNnPc4	písmo
jihoasijská	jihoasijský	k2eAgNnPc4d1	jihoasijské
<g/>
,	,	kIx,	,
např.	např.	kA	např.
indická	indický	k2eAgFnSc1d1	indická
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
jazyky	jazyk	k1gInPc4	jazyk
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
písem	písmo	k1gNnPc2	písmo
více	hodně	k6eAd2	hodně
-	-	kIx~	-
původní	původní	k2eAgFnPc4d1	původní
(	(	kIx(	(
<g/>
starobylé	starobylý	k2eAgFnPc4d1	starobylá
<g/>
)	)	kIx)	)
a	a	k8xC	a
novodobé	novodobý	k2eAgNnSc4d1	novodobé
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
latince	latinka	k1gFnSc6	latinka
nebo	nebo	k8xC	nebo
na	na	k7c6	na
cyrilici	cyrilice	k1gFnSc6	cyrilice
(	(	kIx(	(
<g/>
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
azbuka	azbuka	k1gFnSc1	azbuka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
seřazovány	seřazovat	k5eAaImNgInP	seřazovat
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
jazyky	jazyk	k1gInPc4	jazyk
do	do	k7c2	do
vzorových	vzorový	k2eAgFnPc2d1	vzorová
řad	řada	k1gFnPc2	řada
s	s	k7c7	s
ustáleným	ustálený	k2eAgNnSc7d1	ustálené
pořadím	pořadí	k1gNnSc7	pořadí
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
hlásek	hlásek	k1gInSc4	hlásek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
nazvaných	nazvaný	k2eAgMnPc2d1	nazvaný
podle	podle	k7c2	podle
prvních	první	k4xOgInPc2	první
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
např.	např.	kA	např.
česky	česky	k6eAd1	česky
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
,	,	kIx,	,
řazení	řazení	k1gNnSc1	řazení
cyrilice	cyrilice	k1gFnSc2	cyrilice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
nazýváno	nazývat	k5eAaImNgNnS	nazývat
azbuka	azbuka	k1gFnSc1	azbuka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
názvy	název	k1gInPc1	název
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
pro	pro	k7c4	pro
samotné	samotný	k2eAgNnSc4d1	samotné
písmo	písmo	k1gNnSc4	písmo
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hláskových	hláskový	k2eAgInPc2d1	hláskový
znaků	znak	k1gInPc2	znak
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jednak	jednak	k8xC	jednak
logogramy	logogram	k1gInPc1	logogram
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
číslice	číslice	k1gFnSc1	číslice
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
pomocné	pomocný	k2eAgInPc4d1	pomocný
(	(	kIx(	(
<g/>
organizační	organizační	k2eAgInPc4d1	organizační
<g/>
)	)	kIx)	)
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
interpunkční	interpunkční	k2eAgFnSc4d1	interpunkční
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mezera	mezera	k1gFnSc1	mezera
<g/>
,	,	kIx,	,
tečka	tečka	k1gFnSc1	tečka
<g/>
,	,	kIx,	,
pomlčka	pomlčka	k1gFnSc1	pomlčka
<g/>
,	,	kIx,	,
apostrof	apostrof	k1gInSc1	apostrof
<g/>
,	,	kIx,	,
rovnítko	rovnítko	k1gNnSc1	rovnítko
či	či	k8xC	či
vlnovka	vlnovka	k1gFnSc1	vlnovka
<g/>
.	.	kIx.	.
</s>
<s>
Interpunkční	interpunkční	k2eAgNnSc1d1	interpunkční
využití	využití	k1gNnSc1	využití
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
rozlišení	rozlišení	k1gNnSc4	rozlišení
stejného	stejný	k2eAgNnSc2d1	stejné
písma	písmo	k1gNnSc2	písmo
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
varianty	varianta	k1gFnPc4	varianta
-	-	kIx~	-
"	"	kIx"	"
<g/>
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
minuskule	minuskule	k1gFnSc2	minuskule
a	a	k8xC	a
majuskule	majuskule	k1gFnSc2	majuskule
(	(	kIx(	(
<g/>
verzálky	verzálka	k1gFnSc2	verzálka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
ručního	ruční	k2eAgNnSc2d1	ruční
psaní	psaní	k1gNnSc2	psaní
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
ještě	ještě	k9	ještě
verze	verze	k1gFnPc1	verze
psací	psací	k2eAgFnPc1d1	psací
obou	dva	k4xCgFnPc2	dva
variant	varianta	k1gFnPc2	varianta
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Logografickým	Logografický	k2eAgInSc7d1	Logografický
(	(	kIx(	(
<g/>
významovým	významový	k2eAgInSc7d1	významový
<g/>
)	)	kIx)	)
principem	princip	k1gInSc7	princip
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
řídí	řídit	k5eAaImIp3nS	řídit
některá	některý	k3yIgNnPc4	některý
významová	významový	k2eAgNnPc4d1	významové
rozlišení	rozlišení	k1gNnPc4	rozlišení
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
či	či	k8xC	či
pozice	pozice	k1gFnSc2	pozice
znaků	znak	k1gInPc2	znak
pro	pro	k7c4	pro
méně	málo	k6eAd2	málo
běžné	běžný	k2eAgInPc4d1	běžný
odborné	odborný	k2eAgInPc4d1	odborný
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
mocniny	mocnina	k1gFnPc4	mocnina
nebo	nebo	k8xC	nebo
indexy	index	k1gInPc4	index
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
hláskové	hláskový	k2eAgInPc1d1	hláskový
znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
významy	význam	k1gInPc4	význam
jako	jako	k8xC	jako
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
,	,	kIx,	,
značky	značka	k1gFnPc1	značka
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
C	C	kA	C
-	-	kIx~	-
uhlík	uhlík	k1gInSc4	uhlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
násobků	násobek	k1gInPc2	násobek
či	či	k8xC	či
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
l	l	kA	l
<g/>
"	"	kIx"	"
=	=	kIx~	=
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
m	m	kA	m
<g/>
"	"	kIx"	"
=	=	kIx~	=
metr	metr	k1gInSc4	metr
nebo	nebo	k8xC	nebo
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kPa	kPa	k?	kPa
<g/>
"	"	kIx"	"
=	=	kIx~	=
kilopascal	kilopascat	k5eAaPmAgMnS	kilopascat
<g/>
.	.	kIx.	.
</s>
<s>
Návrhem	návrh	k1gInSc7	návrh
různých	různý	k2eAgFnPc2d1	různá
grafických	grafický	k2eAgFnPc2d1	grafická
podob	podoba	k1gFnPc2	podoba
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
písmen	písmeno	k1gNnPc2	písmeno
či	či	k8xC	či
celých	celý	k2eAgFnPc2d1	celá
abeced	abeceda	k1gFnPc2	abeceda
respektive	respektive	k9	respektive
znakových	znakový	k2eAgFnPc2d1	znaková
sad	sada	k1gFnPc2	sada
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
výtvarný	výtvarný	k2eAgInSc1d1	výtvarný
obor	obor	k1gInSc1	obor
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
typografie	typografia	k1gFnSc2	typografia
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
