<p>
<s>
Salvador	Salvador	k1gMnSc1	Salvador
Felip	Felip	k1gMnSc1	Felip
Jacint	Jacinta	k1gFnPc2	Jacinta
Dalí	Dalí	k1gMnSc1	Dalí
i	i	k8xC	i
Domè	Domè	k1gMnSc1	Domè
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
Figueres	Figueres	k1gInSc1	Figueres
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1989	[number]	k4	1989
Figueres	Figueresa	k1gFnPc2	Figueresa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
katalánský	katalánský	k2eAgMnSc1d1	katalánský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svými	svůj	k3xOyFgInPc7	svůj
surrealistickými	surrealistický	k2eAgInPc7d1	surrealistický
díly	díl	k1gInPc7	díl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pařížské	pařížský	k2eAgMnPc4d1	pařížský
surrealisty	surrealista	k1gMnPc4	surrealista
byl	být	k5eAaImAgMnS	být
přijatý	přijatý	k2eAgMnSc1d1	přijatý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
po	po	k7c6	po
natočení	natočení	k1gNnSc6	natočení
filmu	film	k1gInSc2	film
Andaluský	andaluský	k2eAgMnSc1d1	andaluský
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Luisem	Luis	k1gMnSc7	Luis
Buñ	Buñ	k1gMnSc7	Buñ
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
rozešel	rozejít	k5eAaPmAgMnS	rozejít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
komerčního	komerční	k2eAgMnSc4d1	komerční
umělce	umělec	k1gMnSc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
snové	snový	k2eAgFnSc6d1	snová
imaginaci	imaginace	k1gFnSc6	imaginace
<g/>
.	.	kIx.	.
</s>
<s>
Předměty	předmět	k1gInPc1	předmět
každodennosti	každodennost	k1gFnSc2	každodennost
na	na	k7c6	na
nich	on	k3xPp3gNnPc6	on
dostávají	dostávat	k5eAaImIp3nP	dostávat
nezvyklé	zvyklý	k2eNgFnPc4d1	nezvyklá
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
např.	např.	kA	např.
rozteklé	rozteklý	k2eAgFnPc4d1	rozteklá
hodinky	hodinka	k1gFnPc4	hodinka
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Persistence	persistence	k1gFnSc2	persistence
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
po	po	k7c4	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
jsou	být	k5eAaImIp3nP	být
klasičtější	klasický	k2eAgInPc1d2	klasičtější
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Dalího	Dalí	k1gMnSc2	Dalí
konverze	konverze	k1gFnSc2	konverze
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
mívají	mívat	k5eAaImIp3nP	mívat
náboženské	náboženský	k2eAgInPc1d1	náboženský
náměty	námět	k1gInPc1	námět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
pečlivým	pečlivý	k2eAgNnSc7d1	pečlivé
kresebným	kresebný	k2eAgNnSc7d1	kresebné
zpracováním	zpracování	k1gNnSc7	zpracování
a	a	k8xC	a
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
realistický	realistický	k2eAgInSc4d1	realistický
detail	detail	k1gInSc4	detail
<g/>
.	.	kIx.	.
</s>
<s>
Používal	používat	k5eAaImAgMnS	používat
výrazné	výrazný	k2eAgFnPc4d1	výrazná
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
malby	malba	k1gFnSc2	malba
a	a	k8xC	a
kresby	kresba	k1gFnSc2	kresba
se	se	k3xPyFc4	se
Dalí	Dalí	k1gMnSc1	Dalí
také	také	k9	také
zabýval	zabývat	k5eAaImAgMnS	zabývat
grafikou	grafika	k1gFnSc7	grafika
<g/>
,	,	kIx,	,
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
sochy	socha	k1gFnPc4	socha
<g/>
,	,	kIx,	,
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
parfém	parfém	k1gInSc4	parfém
<g/>
,	,	kIx,	,
designoval	designovat	k5eAaPmAgMnS	designovat
šperky	šperk	k1gInPc4	šperk
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
kostýmy	kostým	k1gInPc4	kostým
a	a	k8xC	a
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
rovněž	rovněž	k9	rovněž
libreto	libreto	k1gNnSc4	libreto
k	k	k7c3	k
baletu	balet	k1gInSc3	balet
a	a	k8xC	a
několik	několik	k4yIc1	několik
autobiografických	autobiografický	k2eAgFnPc2d1	autobiografická
a	a	k8xC	a
beletristických	beletristický	k2eAgFnPc2d1	beletristická
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tvorbu	tvorba	k1gFnSc4	tvorba
formuloval	formulovat	k5eAaImAgMnS	formulovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
studiích	studie	k1gFnPc6	studie
a	a	k8xC	a
manifestech	manifest	k1gInPc6	manifest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Dalí	Dalí	k1gMnSc1	Dalí
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Figueres	Figueresa	k1gFnPc2	Figueresa
v	v	k7c6	v
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zhruba	zhruba	k6eAd1	zhruba
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
gastroenteritidu	gastroenteritida	k1gFnSc4	gastroenteritida
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíše	nejspíše	k9	nejspíše
právě	právě	k9	právě
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byli	být	k5eAaImAgMnP	být
Salvadorovi	Salvadorův	k2eAgMnPc1d1	Salvadorův
rodiče	rodič	k1gMnPc1	rodič
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pouhou	pouhý	k2eAgFnSc7d1	pouhá
reinkarnací	reinkarnace	k1gFnSc7	reinkarnace
svého	svůj	k3xOyFgMnSc2	svůj
vlastního	vlastní	k2eAgMnSc2d1	vlastní
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
nakonec	nakonec	k6eAd1	nakonec
sám	sám	k3xTgMnSc1	sám
uvěřil	uvěřit	k5eAaPmAgMnS	uvěřit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
například	například	k6eAd1	například
v	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
Portrét	portrét	k1gInSc1	portrét
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
bratra	bratr	k1gMnSc2	bratr
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
projevoval	projevovat	k5eAaImAgMnS	projevovat
vlohy	vloha	k1gFnPc4	vloha
pro	pro	k7c4	pro
malířství	malířství	k1gNnSc4	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jako	jako	k8xC	jako
osmiletému	osmiletý	k2eAgInSc3d1	osmiletý
mu	on	k3xPp3gMnSc3	on
rodiče	rodič	k1gMnPc1	rodič
zřídili	zřídit	k5eAaPmAgMnP	zřídit
malý	malý	k2eAgInSc4d1	malý
ateliér	ateliér	k1gInSc4	ateliér
v	v	k7c6	v
podkroví	podkroví	k1gNnSc6	podkroví
bytu	byt	k1gInSc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
školení	školení	k1gNnPc1	školení
v	v	k7c6	v
kresbě	kresba	k1gFnSc6	kresba
získal	získat	k5eAaPmAgMnS	získat
Dalí	Dalí	k1gMnSc1	Dalí
na	na	k7c6	na
soukromém	soukromý	k2eAgNnSc6d1	soukromé
gymnáziu	gymnázium	k1gNnSc6	gymnázium
maristů	marist	k1gInPc2	marist
(	(	kIx(	(
<g/>
Societas	Societas	k1gInSc1	Societas
Mariae	Maria	k1gInSc2	Maria
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
impresionismem	impresionismus	k1gInSc7	impresionismus
a	a	k8xC	a
pointilismem	pointilismus	k1gInSc7	pointilismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
mu	on	k3xPp3gMnSc3	on
místní	místní	k2eAgNnSc1d1	místní
divadlo	divadlo	k1gNnSc1	divadlo
uspořádalo	uspořádat	k5eAaPmAgNnS	uspořádat
výstavu	výstava	k1gFnSc4	výstava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
mezi	mezi	k7c4	mezi
kritiky	kritik	k1gMnPc4	kritik
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
dějiny	dějiny	k1gFnPc4	dějiny
malířství	malířství	k1gNnSc2	malířství
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
regionálním	regionální	k2eAgInSc6d1	regionální
časopise	časopis	k1gInSc6	časopis
články	článek	k1gInPc4	článek
o	o	k7c6	o
starých	starý	k2eAgMnPc6d1	starý
mistrech	mistr	k1gMnPc6	mistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
jeho	jeho	k3xOp3gMnPc7	jeho
přáteli	přítel	k1gMnPc7	přítel
Federico	Federico	k1gMnSc1	Federico
García	García	k1gMnSc1	García
Lorca	Lorca	k1gMnSc1	Lorca
a	a	k8xC	a
Luis	Luisa	k1gFnPc2	Luisa
Buñ	Buñ	k1gFnSc2	Buñ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
však	však	k9	však
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
kritiku	kritika	k1gFnSc4	kritika
vyučujících	vyučující	k2eAgInPc2d1	vyučující
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
na	na	k7c4	na
rok	rok	k1gInSc4	rok
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
na	na	k7c4	na
Akademii	akademie	k1gFnSc4	akademie
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1926	[number]	k4	1926
však	však	k9	však
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
vyloučený	vyloučený	k2eAgInSc1d1	vyloučený
definitivně	definitivně	k6eAd1	definitivně
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
se	se	k3xPyFc4	se
podrobit	podrobit	k5eAaPmF	podrobit
zkouškám	zkouška	k1gFnPc3	zkouška
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
považoval	považovat	k5eAaImAgInS	považovat
examinátory	examinátor	k1gMnPc4	examinátor
za	za	k7c4	za
nekompetentní	kompetentní	k2eNgNnSc4d1	nekompetentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1927	[number]	k4	1927
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgMnSc7	ten
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
tety	teta	k1gFnSc2	teta
týden	týden	k1gInSc1	týden
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Navštívil	navštívit	k5eAaPmAgInS	navštívit
Louvre	Louvre	k1gInSc4	Louvre
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
ho	on	k3xPp3gMnSc4	on
i	i	k9	i
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Dalí	Dalí	k1gMnSc1	Dalí
snažil	snažit	k5eAaImAgMnS	snažit
formulovat	formulovat	k5eAaImF	formulovat
své	svůj	k3xOyFgInPc4	svůj
estetické	estetický	k2eAgInPc4d1	estetický
názory	názor	k1gInPc4	názor
i	i	k9	i
písemně	písemně	k6eAd1	písemně
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
publikoval	publikovat	k5eAaBmAgMnS	publikovat
esej	esej	k1gFnSc4	esej
o	o	k7c6	o
svatém	svatý	k2eAgMnSc6d1	svatý
Šebestiánovi	Šebestián	k1gMnSc6	Šebestián
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
si	se	k3xPyFc3	se
psal	psát	k5eAaImAgInS	psát
s	s	k7c7	s
Lorcou	Lorca	k1gFnSc7	Lorca
a	a	k8xC	a
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
už	už	k6eAd1	už
objevuje	objevovat	k5eAaImIp3nS	objevovat
surrealistická	surrealistický	k2eAgFnSc1d1	surrealistická
tematika	tematika	k1gFnSc1	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
literáty	literát	k1gMnPc7	literát
Lluísem	Lluís	k1gMnSc7	Lluís
Montanyà	Montanyà	k1gMnSc7	Montanyà
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sebastianem	Sebastian	k1gMnSc7	Sebastian
Gaschem	Gasch	k1gMnSc7	Gasch
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
vydali	vydat	k5eAaPmAgMnP	vydat
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
Žlutý	žlutý	k2eAgInSc1d1	žlutý
manifest	manifest	k1gInSc1	manifest
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
za	za	k7c4	za
odkaz	odkaz	k1gInSc4	odkaz
avantgardních	avantgardní	k2eAgInPc2d1	avantgardní
směrů	směr	k1gInPc2	směr
jako	jako	k8xC	jako
byly	být	k5eAaImAgFnP	být
kubismus	kubismus	k1gInSc4	kubismus
<g/>
,	,	kIx,	,
futurismus	futurismus	k1gInSc4	futurismus
a	a	k8xC	a
dadaismus	dadaismus	k1gInSc4	dadaismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Surrealista	surrealista	k1gMnSc1	surrealista
===	===	k?	===
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1929	[number]	k4	1929
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Dalího	Dalí	k1gMnSc4	Dalí
důležitý	důležitý	k2eAgInSc1d1	důležitý
z	z	k7c2	z
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
i	i	k8xC	i
osobního	osobní	k2eAgNnSc2d1	osobní
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
obrazů	obraz	k1gInPc2	obraz
surrealistických	surrealistický	k2eAgMnPc2d1	surrealistický
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Hans	Hans	k1gMnSc1	Hans
Arp	Arp	k1gMnSc1	Arp
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
,	,	kIx,	,
Yves	Yves	k1gInSc1	Yves
Tanguy	Tangua	k1gFnSc2	Tangua
a	a	k8xC	a
Joan	Joan	k1gMnSc1	Joan
Miró	Miró	k1gMnSc1	Miró
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
ve	v	k7c6	v
stejném	stejné	k1gNnSc6	stejné
duchu	duch	k1gMnSc3	duch
maloval	malovat	k5eAaImAgMnS	malovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
onoho	onen	k3xDgInSc2	onen
roku	rok	k1gInSc2	rok
spadá	spadat	k5eAaPmIp3nS	spadat
jeho	jeho	k3xOp3gNnSc1	jeho
druhý	druhý	k4xOgInSc1	druhý
pařížský	pařížský	k2eAgInSc1d1	pařížský
pobyt	pobyt	k1gInSc1	pobyt
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgInSc1d1	spojený
hlavně	hlavně	k6eAd1	hlavně
s	s	k7c7	s
natáčením	natáčení	k1gNnSc7	natáčení
Andaluského	andaluský	k2eAgMnSc2d1	andaluský
psa	pes	k1gMnSc2	pes
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Luisem	Luis	k1gMnSc7	Luis
Buñ	Buñ	k1gMnSc7	Buñ
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
znamenal	znamenat	k5eAaImAgInS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
Dalí	Dalí	k1gMnSc1	Dalí
přijatý	přijatý	k2eAgInSc4d1	přijatý
mezi	mezi	k7c4	mezi
pařížské	pařížský	k2eAgMnPc4d1	pařížský
surrealisty	surrealista	k1gMnPc4	surrealista
<g/>
.	.	kIx.	.
</s>
<s>
Poznal	poznat	k5eAaPmAgMnS	poznat
tak	tak	k9	tak
André	André	k1gMnSc4	André
Bretona	Breton	k1gMnSc4	Breton
a	a	k8xC	a
Tristana	Tristan	k1gMnSc4	Tristan
Tzaru	Tzara	k1gMnSc4	Tzara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
měl	mít	k5eAaImAgMnS	mít
Dalí	Dalí	k1gMnSc1	Dalí
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
první	první	k4xOgFnSc4	první
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
výstavu	výstava	k1gFnSc4	výstava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Galerii	galerie	k1gFnSc6	galerie
Goemans	Goemansa	k1gFnPc2	Goemansa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1929	[number]	k4	1929
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
Cadaqués	Cadaquésa	k1gFnPc2	Cadaquésa
přijeli	přijet	k5eAaPmAgMnP	přijet
navštívit	navštívit	k5eAaPmF	navštívit
Buñ	Buñ	k1gFnSc4	Buñ
<g/>
,	,	kIx,	,
René	René	k1gMnSc5	René
Magritte	Magritt	k1gMnSc5	Magritt
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Eluard	Eluard	k1gMnSc1	Eluard
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Galou	Gala	k1gMnSc7	Gala
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
Ruskou	Ruska	k1gFnSc7	Ruska
<g/>
.	.	kIx.	.
</s>
<s>
Dalí	Dalí	k6eAd1	Dalí
a	a	k8xC	a
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
starší	starý	k2eAgNnSc4d2	starší
Gala	gala	k1gNnSc4	gala
(	(	kIx(	(
<g/>
†	†	k?	†
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
navázali	navázat	k5eAaPmAgMnP	navázat
poměr	poměr	k1gInSc4	poměr
<g/>
;	;	kIx,	;
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
důsledku	důsledek	k1gInSc6	důsledek
se	se	k3xPyFc4	se
Gala	Gala	k1gMnSc1	Gala
s	s	k7c7	s
Eluardem	Eluard	k1gMnSc7	Eluard
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1934	[number]	k4	1934
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
Dalího	Dalí	k1gMnSc4	Dalí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
malíře	malíř	k1gMnSc2	malíř
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
znamenalo	znamenat	k5eAaImAgNnS	znamenat
rozchod	rozchod	k1gInSc4	rozchod
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
nemanželský	manželský	k2eNgInSc1d1	nemanželský
poměr	poměr	k1gInSc1	poměr
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
akceptovat	akceptovat	k5eAaBmF	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
Gala	gala	k2eAgFnSc1d1	gala
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Dalího	Dalí	k1gMnSc2	Dalí
inspirací	inspirace	k1gFnPc2	inspirace
–	–	k?	–
často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
portrétoval	portrétovat	k5eAaImAgInS	portrétovat
–	–	k?	–
a	a	k8xC	a
především	především	k6eAd1	především
se	se	k3xPyFc4	se
starala	starat	k5eAaImAgFnS	starat
o	o	k7c4	o
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
záležitosti	záležitost	k1gFnPc4	záležitost
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
začal	začít	k5eAaPmAgMnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
svou	svůj	k3xOyFgFnSc4	svůj
paranoicko-kritickou	paranoickoritický	k2eAgFnSc4d1	paranoicko-kritická
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
tehdy	tehdy	k6eAd1	tehdy
texty	text	k1gInPc4	text
Shnilý	shnilý	k2eAgMnSc1d1	shnilý
osel	osel	k1gMnSc1	osel
a	a	k8xC	a
Viditelná	viditelný	k2eAgFnSc1d1	viditelná
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
ji	on	k3xPp3gFnSc4	on
interpretoval	interpretovat	k5eAaBmAgMnS	interpretovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Neustálé	neustálý	k2eAgFnPc4d1	neustálá
<g/>
,	,	kIx,	,
nezadržitelné	zadržitelný	k2eNgFnPc4d1	nezadržitelná
proměny	proměna	k1gFnPc4	proměna
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
předmět	předmět	k1gInSc1	předmět
duchovního	duchovní	k2eAgInSc2d1	duchovní
zájmu	zájem	k1gInSc2	zájem
pro	pro	k7c4	pro
paranoika	paranoik	k1gMnSc4	paranoik
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
,	,	kIx,	,
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
paranoik	paranoik	k1gMnSc1	paranoik
pokládá	pokládat	k5eAaImIp3nS	pokládat
všechny	všechen	k3xTgInPc4	všechen
jevy	jev	k1gInPc4	jev
vnějšího	vnější	k2eAgInSc2d1	vnější
světa	svět	k1gInSc2	svět
za	za	k7c4	za
nestálé	stálý	k2eNgNnSc4d1	nestálé
<g/>
,	,	kIx,	,
přecházející	přecházející	k2eAgNnPc4d1	přecházející
rychle	rychle	k6eAd1	rychle
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
stavu	stav	k1gInSc2	stav
k	k	k7c3	k
druhému	druhý	k4xOgNnSc3	druhý
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
Rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
paranoik	paranoik	k1gMnSc1	paranoik
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
toto	tento	k3xDgNnSc4	tento
vidění	vidění	k1gNnSc4	vidění
světa	svět	k1gInSc2	svět
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
i	i	k9	i
ostatním	ostatní	k2eAgMnPc3d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
mezi	mezi	k7c7	mezi
Buñ	Buñ	k1gFnSc7	Buñ
a	a	k8xC	a
Dalím	Dalí	k1gNnSc7	Dalí
jejich	jejich	k3xOp3gInSc4	jejich
druhý	druhý	k4xOgInSc4	druhý
film	film	k1gInSc4	film
Zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
u	u	k7c2	u
pravicových	pravicový	k2eAgMnPc2d1	pravicový
extremistů	extremista	k1gMnPc2	extremista
vlnu	vlna	k1gFnSc4	vlna
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
vandalismu	vandalismus	k1gInSc2	vandalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
Dalí	Dalí	k1gFnSc4	Dalí
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
sběratelských	sběratelský	k2eAgInPc6d1	sběratelský
kruzích	kruh	k1gInPc6	kruh
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
první	první	k4xOgFnPc1	první
surrealistické	surrealistický	k2eAgFnPc1d1	surrealistická
výstavy	výstava	k1gFnPc1	výstava
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
skupina	skupina	k1gFnSc1	skupina
sběratelů	sběratel	k1gMnPc2	sběratel
s	s	k7c7	s
názvem	název	k1gInSc7	název
Zodiaque	Zodiaqu	k1gFnSc2	Zodiaqu
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
jeho	jeho	k3xOp3gInPc2	jeho
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
Dalí	Dalí	k1gMnSc1	Dalí
věnoval	věnovat	k5eAaPmAgMnS	věnovat
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
mu	on	k3xPp3gNnSc3	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
práce	práce	k1gFnSc1	práce
Láska	láska	k1gFnSc1	láska
a	a	k8xC	a
paměť	paměť	k1gFnSc1	paměť
a	a	k8xC	a
článek	článek	k1gInSc1	článek
o	o	k7c6	o
jedlé	jedlý	k2eAgFnSc6d1	jedlá
kráse	krása	k1gFnSc6	krása
a	a	k8xC	a
secesní	secesní	k2eAgFnSc3d1	secesní
architektuře	architektura	k1gFnSc3	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
k	k	k7c3	k
filmu	film	k1gInSc3	film
Babaouo	Babaouo	k6eAd1	Babaouo
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
začal	začít	k5eAaPmAgMnS	začít
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
obrazech	obraz	k1gInPc6	obraz
tematizovat	tematizovat	k5eAaImF	tematizovat
díla	dílo	k1gNnPc4	dílo
Arnolda	Arnold	k1gMnSc2	Arnold
Böcklina	Böcklin	k2eAgMnSc2d1	Böcklin
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Vermeera	Vermeer	k1gMnSc2	Vermeer
<g/>
;	;	kIx,	;
namaloval	namalovat	k5eAaPmAgMnS	namalovat
několik	několik	k4yIc4	několik
perzifláží	perzifláž	k1gFnPc2	perzifláž
slavného	slavný	k2eAgInSc2d1	slavný
Miletova	Miletův	k2eAgInSc2d1	Miletův
obrazu	obraz	k1gInSc2	obraz
Klekání	klekání	k1gNnSc2	klekání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
namaloval	namalovat	k5eAaPmAgInS	namalovat
a	a	k8xC	a
rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
na	na	k7c6	na
Salonu	salon	k1gInSc6	salon
nezávislých	závislý	k2eNgInPc2d1	nezávislý
vystavil	vystavit	k5eAaPmAgInS	vystavit
obraz	obraz	k1gInSc1	obraz
Záhada	záhada	k1gFnSc1	záhada
Viléma	Vilém	k1gMnSc2	Vilém
Tella	Tell	k1gMnSc2	Tell
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
roztržce	roztržka	k1gFnSc3	roztržka
s	s	k7c7	s
pařížskými	pařížský	k2eAgMnPc7d1	pařížský
surrealisty	surrealista	k1gMnPc7	surrealista
<g/>
.	.	kIx.	.
</s>
<s>
Tell	Tell	k1gMnSc1	Tell
zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
má	mít	k5eAaImIp3nS	mít
Leninův	Leninův	k2eAgInSc4d1	Leninův
obličej	obličej	k1gInSc4	obličej
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
rozlítilo	rozlítit	k5eAaPmAgNnS	rozlítit
Bretona	Breton	k1gMnSc4	Breton
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zničit	zničit	k5eAaPmF	zničit
a	a	k8xC	a
Dalího	Dalí	k1gMnSc4	Dalí
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
kontrarevolucionáře	kontrarevolucionář	k1gMnSc4	kontrarevolucionář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Galou	Gala	k1gMnSc7	Gala
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
otevření	otevření	k1gNnSc4	otevření
své	svůj	k3xOyFgFnSc2	svůj
první	první	k4xOgFnSc2	první
samostatné	samostatný	k2eAgFnSc2d1	samostatná
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
přednášky	přednáška	k1gFnSc2	přednáška
na	na	k7c6	na
londýnské	londýnský	k2eAgFnSc6d1	londýnská
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
surrealistické	surrealistický	k2eAgFnSc6d1	surrealistická
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
málem	málem	k6eAd1	málem
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potápěčském	potápěčský	k2eAgInSc6d1	potápěčský
obleku	oblek	k1gInSc6	oblek
s	s	k7c7	s
helmou	helma	k1gFnSc7	helma
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
ponor	ponor	k1gInSc1	ponor
do	do	k7c2	do
nevědomí	nevědomí	k1gNnSc2	nevědomí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
svou	svůj	k3xOyFgFnSc4	svůj
paranoicko-kritickou	paranoickoritický	k2eAgFnSc4d1	paranoicko-kritická
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
začal	začít	k5eAaPmAgInS	začít
dusit	dusit	k5eAaImF	dusit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
ho	on	k3xPp3gMnSc4	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
básník	básník	k1gMnSc1	básník
David	David	k1gMnSc1	David
Gascoyne	Gascoyn	k1gInSc5	Gascoyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Narcisovu	Narcisův	k2eAgFnSc4d1	Narcisova
proměnu	proměna	k1gFnSc4	proměna
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
doprovodil	doprovodit	k5eAaPmAgMnS	doprovodit
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
básní	báseň	k1gFnSc7	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
ukázal	ukázat	k5eAaPmAgInS	ukázat
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
Londýna	Londýn	k1gInSc2	Londýn
Sigmundu	Sigmund	k1gMnSc3	Sigmund
Freudovi	Freuda	k1gMnSc3	Freuda
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sice	sice	k8xC	sice
významně	významně	k6eAd1	významně
surrealisty	surrealista	k1gMnPc7	surrealista
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc4	jejich
umělecký	umělecký	k2eAgInSc4d1	umělecký
program	program	k1gInSc4	program
neakceptoval	akceptovat	k5eNaBmAgMnS	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
Dalí	Dalí	k1gMnSc1	Dalí
pak	pak	k6eAd1	pak
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
několik	několik	k4yIc4	několik
Freudových	Freudový	k2eAgInPc2d1	Freudový
portrétů	portrét	k1gInPc2	portrét
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
surrealisty	surrealista	k1gMnPc7	surrealista
se	se	k3xPyFc4	se
Dalí	Dalí	k1gMnSc1	Dalí
definitivně	definitivně	k6eAd1	definitivně
rozešel	rozejít	k5eAaPmAgMnS	rozejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Vadila	vadit	k5eAaImAgFnS	vadit
jim	on	k3xPp3gMnPc3	on
jeho	jeho	k3xOp3gFnSc1	jeho
posedlost	posedlost	k1gFnSc1	posedlost
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
mystickou	mystický	k2eAgFnSc4d1	mystická
osobnost	osobnost	k1gFnSc4	osobnost
ztělesňující	ztělesňující	k2eAgFnSc4d1	ztělesňující
absolutní	absolutní	k2eAgNnSc1d1	absolutní
zlo	zlo	k1gNnSc1	zlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
zženštilou	zženštilý	k2eAgFnSc4d1	zženštilá
<g/>
,	,	kIx,	,
sebedestruktivní	sebedestruktivní	k2eAgFnSc4d1	sebedestruktivní
a	a	k8xC	a
paranoidní	paranoidní	k2eAgFnSc4d1	paranoidní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgMnSc6	tento
duchu	duch	k1gMnSc6	duch
namaloval	namalovat	k5eAaPmAgMnS	namalovat
několik	několik	k4yIc1	několik
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Breton	Breton	k1gMnSc1	Breton
také	také	k9	také
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
komerční	komerční	k2eAgFnSc7d1	komerční
stránkou	stránka	k1gFnSc7	stránka
Dalího	Dalí	k1gMnSc2	Dalí
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
ho	on	k3xPp3gMnSc4	on
Avida	Avid	k1gMnSc4	Avid
Dollars	Dollars	k1gInSc4	Dollars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
v	v	k7c6	v
USA	USA	kA	USA
===	===	k?	===
</s>
</p>
<p>
<s>
New	New	k?	New
York	York	k1gInSc1	York
se	se	k3xPyFc4	se
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
stal	stát	k5eAaPmAgMnS	stát
pro	pro	k7c4	pro
Dalího	Dalí	k1gMnSc4	Dalí
a	a	k8xC	a
Galu	Gala	k1gMnSc4	Gala
místem	místo	k1gNnSc7	místo
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
Dalí	Dalí	k1gFnSc2	Dalí
i	i	k9	i
nadále	nadále	k6eAd1	nadále
maloval	malovat	k5eAaImAgMnS	malovat
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jeho	jeho	k3xOp3gFnPc4	jeho
autobiografie	autobiografie	k1gFnPc4	autobiografie
Tajný	tajný	k2eAgInSc1d1	tajný
život	život	k1gInSc1	život
Salvadora	Salvador	k1gMnSc2	Salvador
Dalího	Dalí	k1gMnSc2	Dalí
<g/>
,	,	kIx,	,
za	za	k7c4	za
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
román	román	k1gInSc1	román
Skryté	skrytý	k2eAgFnSc2d1	skrytá
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
intriky	intrika	k1gFnPc4	intrika
a	a	k8xC	a
milostné	milostný	k2eAgFnPc4d1	milostná
pletky	pletka	k1gFnPc4	pletka
dekadentních	dekadentní	k2eAgMnPc2d1	dekadentní
aristokratů	aristokrat	k1gMnPc2	aristokrat
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
společně	společně	k6eAd1	společně
krajanem	krajan	k1gMnSc7	krajan
Joanem	Joan	k1gMnSc7	Joan
Miró	Miró	k1gMnSc7	Miró
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
kreslil	kreslit	k5eAaImAgMnS	kreslit
skici	skica	k1gFnSc2	skica
pro	pro	k7c4	pro
Walta	Walt	k1gMnSc4	Walt
Disneye	Disney	k1gMnSc4	Disney
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
snových	snový	k2eAgFnPc6d1	snová
sekvencích	sekvence	k1gFnPc6	sekvence
do	do	k7c2	do
Hitchcockova	Hitchcockov	k1gInSc2	Hitchcockov
filmu	film	k1gInSc2	film
Rozdvojená	rozdvojený	k2eAgFnSc1d1	rozdvojená
duše	duše	k1gFnSc1	duše
<g/>
.	.	kIx.	.
</s>
<s>
Zkáza	zkáza	k1gFnSc1	zkáza
Hirošimy	Hirošima	k1gFnSc2	Hirošima
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
atomovou	atomový	k2eAgFnSc7d1	atomová
bombou	bomba	k1gFnSc7	bomba
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
ho	on	k3xPp3gMnSc4	on
přivedla	přivést	k5eAaPmAgFnS	přivést
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nukleárnímu	nukleární	k2eAgInSc3d1	nukleární
mysticismu	mysticismus	k1gInSc3	mysticismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
Dalí	Dalí	k1gMnSc1	Dalí
společně	společně	k6eAd1	společně
s	s	k7c7	s
Galou	Gala	k1gMnSc7	Gala
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
rybářské	rybářský	k2eAgFnSc6d1	rybářská
vesnici	vesnice	k1gFnSc6	vesnice
Port	port	k1gInSc1	port
Lligat	Lligat	k2eAgInSc1d1	Lligat
u	u	k7c2	u
Cadaqués	Cadaquésa	k1gFnPc2	Cadaquésa
<g/>
.	.	kIx.	.
</s>
<s>
Dalí	Dalí	k6eAd1	Dalí
zde	zde	k6eAd1	zde
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
prožil	prožít	k5eAaPmAgMnS	prožít
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
zbývajícího	zbývající	k2eAgInSc2d1	zbývající
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zimy	zima	k1gFnSc2	zima
trávil	trávit	k5eAaImAgMnS	trávit
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Život	život	k1gInSc1	život
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Výbuch	výbuch	k1gInSc1	výbuch
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
způsobil	způsobit	k5eAaPmAgInS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnou	já	k3xPp1nSc7	já
pronikl	proniknout	k5eAaPmAgInS	proniknout
seismický	seismický	k2eAgInSc4d1	seismický
otřes	otřes	k1gInSc4	otřes
<g/>
.	.	kIx.	.
</s>
<s>
Skvělá	skvělý	k2eAgFnSc1d1	skvělá
inspirace	inspirace	k1gFnSc1	inspirace
mi	já	k3xPp1nSc3	já
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
disponuji	disponovat	k5eAaBmIp1nS	disponovat
nezvyklou	zvyklý	k2eNgFnSc7d1	nezvyklá
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mi	já	k3xPp1nSc3	já
pomůže	pomoct	k5eAaPmIp3nS	pomoct
proniknout	proniknout	k5eAaPmF	proniknout
k	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
:	:	kIx,	:
mysticismem	mysticismus	k1gInSc7	mysticismus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Takto	takto	k6eAd1	takto
Dalí	Dalí	k1gMnSc1	Dalí
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
svůj	svůj	k3xOyFgInSc4	svůj
příklon	příklon	k1gInSc4	příklon
k	k	k7c3	k
mysticismu	mysticismus	k1gInSc3	mysticismus
a	a	k8xC	a
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1949	[number]	k4	1949
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
soukromé	soukromý	k2eAgFnSc6d1	soukromá
audienci	audience	k1gFnSc6	audience
přijal	přijmout	k5eAaPmAgMnS	přijmout
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Dalí	Dalí	k1gMnSc1	Dalí
mu	on	k3xPp3gMnSc3	on
při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
věnoval	věnovat	k5eAaImAgMnS	věnovat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
obrazů	obraz	k1gInPc2	obraz
s	s	k7c7	s
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
maloval	malovat	k5eAaImAgMnS	malovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
verzi	verze	k1gFnSc3	verze
Madony	Madona	k1gFnSc2	Madona
z	z	k7c2	z
Port	porta	k1gFnPc2	porta
Lligatu	Lligat	k1gInSc2	Lligat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
teoretické	teoretický	k2eAgFnSc6d1	teoretická
stránce	stránka	k1gFnSc6	stránka
našel	najít	k5eAaPmAgMnS	najít
jeho	jeho	k3xOp3gInSc4	jeho
obrat	obrat	k1gInSc4	obrat
k	k	k7c3	k
mysticismu	mysticismus	k1gInSc3	mysticismus
odraz	odraz	k1gInSc1	odraz
v	v	k7c6	v
Mystickém	mystický	k2eAgInSc6d1	mystický
manifestu	manifest	k1gInSc6	manifest
<g/>
,	,	kIx,	,
vyšlém	vyšlý	k2eAgInSc6d1	vyšlý
roku	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
prezentaci	prezentace	k1gFnSc3	prezentace
jeho	jeho	k3xOp3gInPc2	jeho
názorů	názor	k1gInPc2	názor
poskytnut	poskytnut	k2eAgInSc4d1	poskytnut
prostor	prostor	k1gInSc4	prostor
na	na	k7c6	na
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
v	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Dalí	Dalí	k1gFnSc2	Dalí
věnoval	věnovat	k5eAaPmAgInS	věnovat
i	i	k8xC	i
jiným	jiný	k2eAgFnPc3d1	jiná
uměleckým	umělecký	k2eAgFnPc3d1	umělecká
aktivitám	aktivita	k1gFnPc3	aktivita
než	než	k8xS	než
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
začal	začít	k5eAaPmAgInS	začít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Descharnesem	Descharnes	k1gMnSc7	Descharnes
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
filmu	film	k1gInSc6	film
Podivuhodný	podivuhodný	k2eAgInSc4d1	podivuhodný
příběh	příběh	k1gInSc4	příběh
krajkářky	krajkářka	k1gFnSc2	krajkářka
a	a	k8xC	a
nosorožce	nosorožec	k1gMnSc2	nosorožec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
inspirovaný	inspirovaný	k2eAgMnSc1d1	inspirovaný
Dalího	Dalí	k1gMnSc4	Dalí
zaujetím	zaujetí	k1gNnSc7	zaujetí
pro	pro	k7c4	pro
Jana	Jan	k1gMnSc4	Jan
Vermeera	Vermeer	k1gMnSc4	Vermeer
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
Krajkářka	krajkářka	k1gFnSc1	krajkářka
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
dokončen	dokončit	k5eAaPmNgInS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
premiéru	premiéra	k1gFnSc4	premiéra
Galin	Galin	k2eAgInSc1d1	Galin
balet	balet	k1gInSc1	balet
v	v	k7c6	v
choreografii	choreografie	k1gFnSc6	choreografie
Maurice	Maurika	k1gFnSc6	Maurika
Béjarta	Béjarta	k1gFnSc1	Béjarta
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
napsal	napsat	k5eAaBmAgMnS	napsat
Dalí	Dalí	k1gMnSc1	Dalí
libreto	libreto	k1gNnSc4	libreto
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
ho	on	k3xPp3gNnSc4	on
scénograficky	scénograficky	k6eAd1	scénograficky
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
práci	práce	k1gFnSc3	práce
Le	Le	k1gFnSc3	Le
Mythe	mythus	k1gInSc5	mythus
Tragique	Tragiqu	k1gInPc1	Tragiqu
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Angélus	Angélus	k1gMnSc1	Angélus
de	de	k?	de
Millet	Millet	k1gInSc1	Millet
(	(	kIx(	(
<g/>
Tragický	tragický	k2eAgInSc1d1	tragický
mýtus	mýtus	k1gInSc1	mýtus
Miletova	Miletův	k2eAgNnSc2d1	Miletův
Klekání	klekání	k1gNnSc2	klekání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
analýzu	analýza	k1gFnSc4	analýza
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
viděl	vidět	k5eAaImAgInS	vidět
nikoliv	nikoliv	k9	nikoliv
výraz	výraz	k1gInSc1	výraz
zbožnosti	zbožnost	k1gFnSc2	zbožnost
venkovanů	venkovan	k1gMnPc2	venkovan
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jejich	jejich	k3xOp3gFnSc4	jejich
potlačenou	potlačený	k2eAgFnSc4d1	potlačená
sexualitu	sexualita	k1gFnSc4	sexualita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
pak	pak	k6eAd1	pak
vydal	vydat	k5eAaPmAgInS	vydat
Deník	deník	k1gInSc1	deník
génia	génius	k1gMnSc2	génius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některými	některý	k3yIgInPc7	některý
svými	svůj	k3xOyFgFnPc7	svůj
instalacemi	instalace	k1gFnPc7	instalace
Dalí	Dalí	k1gFnSc2	Dalí
veřejnost	veřejnost	k1gFnSc4	veřejnost
šokoval	šokovat	k5eAaBmAgMnS	šokovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
když	když	k8xS	když
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1958	[number]	k4	1958
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
happeningu	happening	k1gInSc6	happening
vystavil	vystavit	k5eAaPmAgInS	vystavit
patnáctimetrový	patnáctimetrový	k2eAgInSc1d1	patnáctimetrový
bochník	bochník	k1gInSc1	bochník
chleba	chléb	k1gInSc2	chléb
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
představil	představit	k5eAaPmAgMnS	představit
svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
<g/>
:	:	kIx,	:
ovovciped	ovovciped	k1gMnSc1	ovovciped
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
s	s	k7c7	s
holografií	holografie	k1gFnSc7	holografie
a	a	k8xC	a
zabýval	zabývat	k5eAaImAgMnS	zabývat
se	se	k3xPyFc4	se
možnostmi	možnost	k1gFnPc7	možnost
třídimenzionálního	třídimenzionální	k2eAgNnSc2d1	třídimenzionální
zobrazování	zobrazování	k1gNnSc2	zobrazování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k9	už
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
byla	být	k5eAaImAgNnP	být
otevřena	otevřít	k5eAaPmNgNnP	otevřít
dvě	dva	k4xCgFnPc4	dva
jeho	jeho	k3xOp3gNnPc2	jeho
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
v	v	k7c6	v
Clevelandu	Cleveland	k1gInSc6	Cleveland
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
přemístěno	přemístit	k5eAaPmNgNnS	přemístit
do	do	k7c2	do
Saint	Sainta	k1gFnPc2	Sainta
Petersburgu	Petersburg	k1gInSc2	Petersburg
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
ve	v	k7c4	v
Figueras	Figueras	k1gInSc4	Figueras
<g/>
.	.	kIx.	.
</s>
<s>
Dalího	Dalí	k1gMnSc2	Dalí
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
světového	světový	k2eAgNnSc2d1	světové
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
byl	být	k5eAaImAgInS	být
zhodnocen	zhodnotit	k5eAaPmNgInS	zhodnotit
několika	několik	k4yIc7	několik
oceněními	ocenění	k1gNnPc7	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
akademie	akademie	k1gFnSc2	akademie
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
za	za	k7c4	za
člena	člen	k1gMnSc4	člen
Académie	Académie	k1gFnSc2	Académie
des	des	k1gNnSc7	des
beaux-arts	beauxrtsa	k1gFnPc2	beaux-artsa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Gala	gala	k1gNnSc3	gala
<g/>
,	,	kIx,	,
mu	on	k3xPp3gNnSc3	on
král	král	k1gMnSc1	král
Juan	Juan	k1gMnSc1	Juan
Carlos	Carlos	k1gMnSc1	Carlos
I.	I.	kA	I.
udělil	udělit	k5eAaPmAgMnS	udělit
titul	titul	k1gInSc4	titul
markýz	markýza	k1gFnPc2	markýza
z	z	k7c2	z
Púbolu	Púbol	k1gInSc2	Púbol
podle	podle	k7c2	podle
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
koupil	koupit	k5eAaPmAgMnS	koupit
Gale	Gale	k1gMnSc1	Gale
a	a	k8xC	a
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Dalí	Dalí	k1gMnSc1	Dalí
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
Ocas	ocas	k1gInSc4	ocas
vlaštovky	vlaštovka	k1gFnSc2	vlaštovka
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
matematické	matematický	k2eAgFnSc2d1	matematická
teorie	teorie	k1gFnSc2	teorie
katastrof	katastrofa	k1gFnPc2	katastrofa
René	René	k1gFnSc2	René
Thoma	Thomum	k1gNnSc2	Thomum
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
koncem	konec	k1gInSc7	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
Dalí	Dalí	k2eAgFnPc4d1	Dalí
těžké	těžký	k2eAgFnPc4d1	těžká
popáleniny	popálenina	k1gFnPc4	popálenina
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
způsobený	způsobený	k2eAgInSc1d1	způsobený
zkratem	zkrat	k1gInSc7	zkrat
elektřiny	elektřina	k1gFnSc2	elektřina
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
ložnici	ložnice	k1gFnSc6	ložnice
v	v	k7c6	v
Púbolu	Púbol	k1gInSc6	Púbol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyléčení	vyléčení	k1gNnSc6	vyléčení
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
věže	věž	k1gFnSc2	věž
vedle	vedle	k7c2	vedle
svého	svůj	k3xOyFgNnSc2	svůj
muzea	muzeum	k1gNnSc2	muzeum
ve	v	k7c4	v
Figueres	Figueres	k1gInSc4	Figueres
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazval	nazvat	k5eAaBmAgMnS	nazvat
Torre	torr	k1gInSc5	torr
Galatea	Galate	k2eAgMnSc4d1	Galate
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1989	[number]	k4	1989
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgNnPc4d1	srdeční
selhání	selhání	k1gNnPc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnPc2	jeho
přání	přání	k1gNnPc2	přání
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
nabalzamováno	nabalzamován	k2eAgNnSc1d1	nabalzamován
a	a	k8xC	a
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
vystavováno	vystavován	k2eAgNnSc4d1	vystavováno
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
tamtéž	tamtéž	k6eAd1	tamtéž
v	v	k7c6	v
podlaze	podlaha	k1gFnSc6	podlaha
atria	atrium	k1gNnSc2	atrium
<g/>
.	.	kIx.	.
</s>
<s>
Dalí	Dalí	k1gMnSc1	Dalí
odkázal	odkázat	k5eAaPmAgMnS	odkázat
veškeré	veškerý	k3xTgNnSc4	veškerý
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
majetek	majetek	k1gInSc1	majetek
španělskému	španělský	k2eAgInSc3d1	španělský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstavy	výstava	k1gFnPc1	výstava
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
1925	[number]	k4	1925
Galerie	galerie	k1gFnSc1	galerie
Dalmau	Dalmaus	k1gInSc2	Dalmaus
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
samostatná	samostatný	k2eAgFnSc1d1	samostatná
výstava	výstava	k1gFnSc1	výstava
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1929	[number]	k4	1929
Galerie	galerie	k1gFnSc1	galerie
Goemans	Goemansa	k1gFnPc2	Goemansa
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
samostatná	samostatný	k2eAgFnSc1d1	samostatná
výstava	výstava	k1gFnSc1	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1934	[number]	k4	1934
Salon	salon	k1gInSc1	salon
des	des	k1gNnSc2	des
Indépendants	Indépendantsa	k1gFnPc2	Indépendantsa
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
(	(	kIx(	(
<g/>
vystavil	vystavit	k5eAaPmAgInS	vystavit
Záhadu	záhada	k1gFnSc4	záhada
Viléma	Vilém	k1gMnSc2	Vilém
Tella	Tell	k1gMnSc2	Tell
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
<g/>
Julien	Julien	k2eAgMnSc1d1	Julien
Levy	Levy	k?	Levy
Gallery	Galler	k1gInPc4	Galler
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc4	York
<g/>
.1936	.1936	k4	.1936
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
surrealistická	surrealistický	k2eAgFnSc1d1	surrealistická
výstava	výstava	k1gFnSc1	výstava
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
surrealistická	surrealistický	k2eAgFnSc1d1	surrealistická
výstava	výstava	k1gFnSc1	výstava
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1941	[number]	k4	1941
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Joanem	Joan	k1gMnSc7	Joan
Miró	Miró	k1gMnSc7	Miró
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1952	[number]	k4	1952
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
National	National	k1gFnSc1	National
Gallery	Galler	k1gInPc1	Galler
of	of	k?	of
Art	Art	k1gFnPc2	Art
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
The	The	k1gFnSc1	The
Contemporary	Contemporara	k1gFnSc2	Contemporara
Art	Art	k1gFnSc2	Art
Gallery	Galler	k1gInPc1	Galler
Seibu	Seiba	k1gFnSc4	Seiba
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc4	Tokio
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
retrospektiva	retrospektiva	k1gFnSc1	retrospektiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Guggenheimovo	Guggenheimův	k2eAgNnSc1d1	Guggenheimovo
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
hyperstereoskopická	hyperstereoskopický	k2eAgNnPc1d1	hyperstereoskopický
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
Centre	centr	k1gInSc5	centr
Georges	Georges	k1gMnSc1	Georges
Pompidou	Pompida	k1gFnSc7	Pompida
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
a	a	k8xC	a
Tate	Tate	k1gFnSc1	Tate
Britain	Britain	k1gInSc1	Britain
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
putovní	putovní	k2eAgFnSc1d1	putovní
retrospektiva	retrospektiva	k1gFnSc1	retrospektiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Palazzo	Palazza	k1gFnSc5	Palazza
dei	dei	k?	dei
Diamanti	Diamant	k1gMnPc1	Diamant
<g/>
,	,	kIx,	,
Ferrara	Ferrara	k1gFnSc1	Ferrara
<g/>
,	,	kIx,	,
retrospektiva	retrospektiva	k1gFnSc1	retrospektiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Londra	Londra	k1gFnSc1	Londra
Palace	Palace	k1gFnSc1	Palace
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
,	,	kIx,	,
litografie	litografie	k1gFnPc1	litografie
<g/>
,	,	kIx,	,
rytiny	rytina	k1gFnPc1	rytina
<g/>
,	,	kIx,	,
plastiky	plastika	k1gFnPc1	plastika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Stuttgart	Stuttgart	k1gInSc4	Stuttgart
a	a	k8xC	a
Curych	Curych	k1gInSc1	Curych
<g/>
,	,	kIx,	,
retrospektivy	retrospektiva	k1gFnPc1	retrospektiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Muzeum	muzeum	k1gNnSc1	muzeum
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
Montreal	Montreal	k1gInSc1	Montreal
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Dalí	Dalí	k1gMnSc1	Dalí
byl	být	k5eAaImAgMnS	být
umělcem	umělec	k1gMnSc7	umělec
obrovského	obrovský	k2eAgInSc2d1	obrovský
talentu	talent	k1gInSc2	talent
a	a	k8xC	a
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
arabské	arabský	k2eAgInPc4d1	arabský
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
předkové	předek	k1gMnPc1	předek
byli	být	k5eAaImAgMnP	být
potomky	potomek	k1gMnPc4	potomek
Maurů	Maur	k1gMnPc2	Maur
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c4	na
Iberský	iberský	k2eAgInSc4d1	iberský
poloostrov	poloostrov	k1gInSc4	poloostrov
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
roku	rok	k1gInSc2	rok
711	[number]	k4	711
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
Dalí	Dalí	k1gNnSc6	Dalí
spatřoval	spatřovat	k5eAaImAgInS	spatřovat
příčinu	příčina	k1gFnSc4	příčina
své	svůj	k3xOyFgFnSc2	svůj
lásky	láska	k1gFnSc2	láska
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
přehnané	přehnaný	k2eAgFnPc4d1	přehnaná
<g/>
,	,	kIx,	,
svoji	svůj	k3xOyFgFnSc4	svůj
vášeň	vášeň	k1gFnSc4	vášeň
pro	pro	k7c4	pro
luxus	luxus	k1gInSc4	luxus
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
pro	pro	k7c4	pro
orientální	orientální	k2eAgNnSc4d1	orientální
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Dalího	Dalí	k1gMnSc2	Dalí
výstřední	výstřední	k2eAgMnSc1d1	výstřední
a	a	k8xC	a
teatrální	teatrální	k2eAgNnSc1d1	teatrální
chování	chování	k1gNnSc1	chování
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
zastínilo	zastínit	k5eAaPmAgNnS	zastínit
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
dělal	dělat	k5eAaImAgMnS	dělat
neobvyklé	obvyklý	k2eNgFnPc4d1	neobvyklá
věci	věc	k1gFnPc4	věc
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
upoutal	upoutat	k5eAaPmAgMnS	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
občas	občas	k6eAd1	občas
unavovalo	unavovat	k5eAaImAgNnS	unavovat
jak	jak	k8xS	jak
jeho	on	k3xPp3gMnSc2	on
obdivovatele	obdivovatel	k1gMnSc2	obdivovatel
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
jeho	jeho	k3xOp3gFnSc2	jeho
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
vedla	vést	k5eAaImAgFnS	vést
tato	tento	k3xDgFnSc1	tento
cílená	cílený	k2eAgFnSc1d1	cílená
pozornost	pozornost	k1gFnSc1	pozornost
k	k	k7c3	k
uznání	uznání	k1gNnSc3	uznání
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
širokou	široký	k2eAgFnSc7d1	široká
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
prodeje	prodej	k1gInSc2	prodej
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalího	Dalí	k2eAgNnSc2d1	Dalí
díla	dílo	k1gNnSc2	dílo
jsou	být	k5eAaImIp3nP	být
proslulá	proslulý	k2eAgFnSc1d1	proslulá
překvapující	překvapující	k2eAgFnSc7d1	překvapující
kombinací	kombinace	k1gFnSc7	kombinace
bizarních	bizarní	k2eAgInPc2d1	bizarní
snových	snový	k2eAgInPc2d1	snový
obrazů	obraz	k1gInPc2	obraz
s	s	k7c7	s
prvotřídním	prvotřídní	k2eAgInSc7d1	prvotřídní
kreslířským	kreslířský	k2eAgInSc7d1	kreslířský
a	a	k8xC	a
malířským	malířský	k2eAgInSc7d1	malířský
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
Dalí	Dalí	k1gMnSc1	Dalí
nacházel	nacházet	k5eAaImAgMnS	nacházet
inspiraci	inspirace	k1gFnSc4	inspirace
v	v	k7c6	v
pracích	práce	k1gFnPc6	práce
starých	starý	k2eAgMnPc2d1	starý
renesančních	renesanční	k2eAgMnPc2d1	renesanční
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
Persistenci	persistence	k1gFnSc3	persistence
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
dokončil	dokončit	k5eAaPmAgMnS	dokončit
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
Dalí	Dalí	k2eAgFnPc1d1	Dalí
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
namaloval	namalovat	k5eAaPmAgMnS	namalovat
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
1500	[number]	k4	1500
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umělecký	umělecký	k2eAgInSc1d1	umělecký
repertoár	repertoár	k1gInSc1	repertoár
Salvadora	Salvador	k1gMnSc2	Salvador
Dalího	Dalí	k1gMnSc2	Dalí
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
i	i	k9	i
film	film	k1gInSc4	film
a	a	k8xC	a
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Waltem	Walt	k1gInSc7	Walt
Disneym	Disneym	k1gInSc1	Disneym
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
krátkém	krátký	k2eAgInSc6d1	krátký
animovaném	animovaný	k2eAgInSc6d1	animovaný
filmu	film	k1gInSc6	film
Destino	Destin	k2eAgNnSc1d1	Destino
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
uvedení	uvedení	k1gNnSc6	uvedení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
dávno	dávno	k6eAd1	dávno
po	po	k7c4	po
Dalího	Dalí	k1gMnSc4	Dalí
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
nominován	nominován	k2eAgMnSc1d1	nominován
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Americké	americký	k2eAgFnSc2d1	americká
filmové	filmový	k2eAgFnSc2d1	filmová
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Luisem	Luiso	k1gNnSc7	Luiso
Buñ	Buñ	k1gMnSc2	Buñ
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
scénáři	scénář	k1gInSc6	scénář
surrealistického	surrealistický	k2eAgInSc2d1	surrealistický
filmu	film	k1gInSc2	film
Andaluský	andaluský	k2eAgMnSc1d1	andaluský
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
let	léto	k1gNnPc2	léto
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
projektech	projekt	k1gInPc6	projekt
s	s	k7c7	s
fotografem	fotograf	k1gMnSc7	fotograf
Philippem	Philipp	k1gMnSc7	Philipp
Halsmanem	Halsman	k1gMnSc7	Halsman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
realizovat	realizovat	k5eAaBmF	realizovat
některé	některý	k3yIgFnPc4	některý
jeho	jeho	k3xOp3gFnPc4	jeho
myšlenky	myšlenka	k1gFnPc4	myšlenka
fotograficky	fotograficky	k6eAd1	fotograficky
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
úspěchy	úspěch	k1gInPc4	úspěch
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
plastiky	plastika	k1gFnSc2	plastika
patří	patřit	k5eAaImIp3nS	patřit
muzeum	muzeum	k1gNnSc1	muzeum
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Port	port	k1gInSc1	port
Lligat	Lligat	k2eAgInSc1d1	Lligat
u	u	k7c2	u
Cadaqués	Cadaquésa	k1gFnPc2	Cadaquésa
společně	společně	k6eAd1	společně
se	s	k7c7	s
surrealistickým	surrealistický	k2eAgInSc7d1	surrealistický
pavilonem	pavilon	k1gInSc7	pavilon
Dream	Dream	k1gInSc1	Dream
of	of	k?	of
Venus	Venus	k1gInSc1	Venus
(	(	kIx(	(
<g/>
Venušin	Venušin	k2eAgInSc1d1	Venušin
sen	sen	k1gInSc1	sen
<g/>
)	)	kIx)	)
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
ukrýval	ukrývat	k5eAaImAgInS	ukrývat
množství	množství	k1gNnSc4	množství
neobvyklých	obvyklý	k2eNgFnPc2d1	neobvyklá
plastik	plastika	k1gFnPc2	plastika
a	a	k8xC	a
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
ortodoxních	ortodoxní	k2eAgInPc2d1	ortodoxní
uměleckých	umělecký	k2eAgInPc2d1	umělecký
počinů	počin	k1gInPc2	počin
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimiž	jenž	k3xRgInPc7	jenž
Dalí	Dalí	k1gMnSc1	Dalí
stál	stát	k5eAaImAgInS	stát
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
lidská	lidský	k2eAgFnSc1d1	lidská
osobnost	osobnost	k1gFnSc1	osobnost
jako	jako	k8xC	jako
taková	takový	k3xDgFnSc1	takový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
potkal	potkat	k5eAaPmAgMnS	potkat
Dalí	Dalí	k1gMnSc1	Dalí
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
francouzském	francouzský	k2eAgInSc6d1	francouzský
nočním	noční	k2eAgInSc6d1	noční
klubu	klub	k1gInSc6	klub
modelku	modelka	k1gFnSc4	modelka
Amandu	Amanda	k1gFnSc4	Amanda
Learovou	Learová	k1gFnSc4	Learová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
spíše	spíše	k9	spíše
známá	známá	k1gFnSc1	známá
jako	jako	k8xC	jako
transvestita	transvestita	k1gMnSc1	transvestita
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Peki	Pek	k1gFnSc2	Pek
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Learová	Learová	k1gFnSc1	Learová
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
stala	stát	k5eAaPmAgFnS	stát
Daliho	Dali	k1gMnSc4	Dali
chráněnkou	chráněnka	k1gFnSc7	chráněnka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
múzou	múza	k1gFnSc7	múza
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
aféře	aféra	k1gFnSc6	aféra
posléze	posléze	k6eAd1	posléze
napsala	napsat	k5eAaBmAgFnS	napsat
autorizovanou	autorizovaný	k2eAgFnSc4d1	autorizovaná
biografii	biografie	k1gFnSc4	biografie
My	my	k3xPp1nPc1	my
Live	Liv	k1gFnPc4	Liv
With	With	k1gInSc4	With
Dalí	Dalí	k1gNnSc1	Dalí
(	(	kIx(	(
<g/>
Můj	můj	k3xOp1gInSc4	můj
život	život	k1gInSc4	život
s	s	k7c7	s
Dalím	Dalí	k1gNnSc7	Dalí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
ohromen	ohromit	k5eAaPmNgInS	ohromit
chlapeckým	chlapecký	k2eAgInSc7d1	chlapecký
vzhledem	vzhled	k1gInSc7	vzhled
Learové	Learová	k1gFnSc2	Learová
<g/>
,	,	kIx,	,
stál	stát	k5eAaImAgInS	stát
i	i	k9	i
za	za	k7c7	za
její	její	k3xOp3gFnSc7	její
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
přeměnou	přeměna	k1gFnSc7	přeměna
z	z	k7c2	z
modelky	modelka	k1gFnSc2	modelka
na	na	k7c4	na
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnPc3	jeho
radám	rada	k1gFnPc3	rada
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sebeprezentace	sebeprezentace	k1gFnSc2	sebeprezentace
a	a	k8xC	a
tajuplným	tajuplný	k2eAgInPc3d1	tajuplný
příběhům	příběh	k1gInPc3	příběh
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
původu	původ	k1gInSc6	původ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Dalí	Dalí	k1gMnSc1	Dalí
sám	sám	k3xTgMnSc1	sám
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
šířit	šířit	k5eAaImF	šířit
<g/>
,	,	kIx,	,
vzala	vzít	k5eAaPmAgFnS	vzít
Learová	Learová	k1gFnSc1	Learová
hudební	hudební	k2eAgFnSc4d1	hudební
scénu	scéna	k1gFnSc4	scéna
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
Dalím	Dalí	k1gNnSc7	Dalí
spojeni	spojit	k5eAaPmNgMnP	spojit
"	"	kIx"	"
<g/>
duchovním	duchovní	k2eAgInSc7d1	duchovní
svazkem	svazek	k1gInSc7	svazek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Learová	Learová	k1gFnSc1	Learová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c2	za
Dalího	Dalí	k2eAgNnSc2d1	Dalí
soukromého	soukromý	k2eAgNnSc2d1	soukromé
"	"	kIx"	"
<g/>
Frankensteina	Frankensteino	k1gNnSc2	Frankensteino
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
převzala	převzít	k5eAaPmAgFnS	převzít
místo	místo	k7c2	místo
jeho	jeho	k3xOp3gFnSc2	jeho
dřívější	dřívější	k2eAgFnSc2d1	dřívější
múzy	múza	k1gFnSc2	múza
Ultra	ultra	k2eAgFnPc2d1	ultra
Violet	Violeta	k1gFnPc2	Violeta
(	(	kIx(	(
<g/>
Isabelle	Isabelle	k1gFnSc1	Isabelle
Collin	Collin	k1gInSc1	Collin
Dufresne	Dufresne	k1gFnSc1	Dufresne
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
Dalího	Dalí	k2eAgInSc2d1	Dalí
opustila	opustit	k5eAaPmAgFnS	opustit
kvůli	kvůli	k7c3	kvůli
Andymu	Andym	k1gInSc3	Andym
Warholovi	Warhol	k1gMnSc6	Warhol
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
studiu	studio	k1gNnSc6	studio
Factory	Factor	k1gInPc1	Factor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sbírky	sbírka	k1gFnSc2	sbírka
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
kolekci	kolekce	k1gFnSc4	kolekce
Dalího	Dalí	k1gMnSc2	Dalí
prací	práce	k1gFnSc7	práce
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
–	–	k?	–
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
Dalí	Dalí	k1gFnSc2	Dalí
Theatre	Theatr	k1gInSc5	Theatr
and	and	k?	and
Museum	museum	k1gNnSc1	museum
v	v	k7c6	v
katalánském	katalánský	k2eAgInSc6d1	katalánský
Figueres	Figueres	k1gInSc1	Figueres
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
kolekce	kolekce	k1gFnSc1	kolekce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sbírky	sbírka	k1gFnPc4	sbírka
manželů	manžel	k1gMnPc2	manžel
Reynoldse	Reynoldse	k1gFnSc2	Reynoldse
and	and	k?	and
Eleanor	Eleanora	k1gFnPc2	Eleanora
Morseových	Morseův	k2eAgFnPc2d1	Morseova
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
v	v	k7c4	v
Salvador	Salvador	k1gInSc4	Salvador
Dalí	Dalí	k2eAgNnSc1d1	Dalí
Muzeum	muzeum	k1gNnSc1	muzeum
ve	v	k7c6	v
floridském	floridský	k2eAgInSc6d1	floridský
St.	st.	kA	st.
Petersburgu	Petersburg	k1gInSc6	Petersburg
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
1500	[number]	k4	1500
děl	dělo	k1gNnPc2	dělo
tohoto	tento	k3xDgMnSc2	tento
slavného	slavný	k2eAgMnSc2d1	slavný
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
významné	významný	k2eAgFnPc1d1	významná
kolekce	kolekce	k1gFnPc1	kolekce
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
také	také	k9	také
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
Museo	Museo	k6eAd1	Museo
Reina	Rein	k2eAgFnSc1d1	Reina
Sofía	Sofía	k1gFnSc1	Sofía
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
a	a	k8xC	a
v	v	k7c6	v
Galerii	galerie	k1gFnSc6	galerie
Salvadora	Salvador	k1gMnSc2	Salvador
Dalího	Dalí	k1gMnSc2	Dalí
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Pacific	Pacifice	k1gInPc2	Pacifice
Palisades	Palisadesa	k1gFnPc2	Palisadesa
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
sbírku	sbírka	k1gFnSc4	sbírka
jeho	jeho	k3xOp3gFnPc2	jeho
kreseb	kresba	k1gFnPc2	kresba
a	a	k8xC	a
soch	socha	k1gFnPc2	socha
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
pařížské	pařížský	k2eAgNnSc1d1	pařížské
muzeum	muzeum	k1gNnSc1	muzeum
Espace	Espace	k1gFnSc2	Espace
Dalí	Dalí	k1gFnSc2	Dalí
na	na	k7c6	na
Montmartru	Montmartr	k1gInSc6	Montmartr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
přes	přes	k7c4	přes
500	[number]	k4	500
Dalího	Dalí	k2eAgInSc2d1	Dalí
děl	dělo	k1gNnPc2	dělo
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
akce	akce	k1gFnSc2	akce
Dalí	Dalí	k2eAgFnSc2d1	Dalí
Universe	Universe	k1gFnSc2	Universe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lauryssensovo	Lauryssensův	k2eAgNnSc4d1	Lauryssensův
svědectví	svědectví	k1gNnSc4	svědectví
==	==	k?	==
</s>
</p>
<p>
<s>
Stan	stan	k1gInSc1	stan
Lauryssens	Lauryssens	k1gInSc1	Lauryssens
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
belgický	belgický	k2eAgMnSc1d1	belgický
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
bydlel	bydlet	k5eAaImAgMnS	bydlet
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Cadaqués	Cadaquésa	k1gFnPc2	Cadaquésa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
Dalího	Dalí	k2eAgInSc2d1	Dalí
sousedem	soused	k1gMnSc7	soused
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
setkání	setkání	k1gNnSc6	setkání
mu	on	k3xPp3gMnSc3	on
velmi	velmi	k6eAd1	velmi
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Lauryssens	Lauryssens	k6eAd1	Lauryssens
své	svůj	k3xOyFgFnPc4	svůj
zkušenosti	zkušenost	k1gFnPc4	zkušenost
literárně	literárně	k6eAd1	literárně
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
v	v	k7c6	v
románu	román	k1gInSc6	román
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
-	-	kIx~	-
skutečný	skutečný	k2eAgInSc1d1	skutečný
příběh	příběh	k1gInSc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnPc1	právo
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
prodala	prodat	k5eAaPmAgFnS	prodat
do	do	k7c2	do
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
30	[number]	k4	30
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
natočen	natočit	k5eAaBmNgInS	natočit
i	i	k9	i
film	film	k1gInSc1	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Dali	dát	k5eAaPmAgMnP	dát
&	&	k?	&
I	i	k8xC	i
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Surreal	Surreal	k1gInSc4	Surreal
Story	story	k1gFnSc2	story
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
ještě	ještě	k9	ještě
nebyl	být	k5eNaImAgInS	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
uveden	uveden	k2eAgMnSc1d1	uveden
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dalí	Dalí	k2eAgFnSc2d1	Dalí
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
Dalího	Dalí	k2eAgInSc2d1	Dalí
dílo	dílo	k1gNnSc1	dílo
mezi	mezi	k7c7	mezi
umělci	umělec	k1gMnPc7	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
surrealismus	surrealismus	k1gInSc4	surrealismus
sledovali	sledovat	k5eAaImAgMnP	sledovat
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
známé	známý	k2eAgFnPc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Revue	revue	k1gFnSc2	revue
Devětsilu	Devětsil	k1gInSc2	Devětsil
<g/>
,	,	kIx,	,
vydávaného	vydávaný	k2eAgInSc2d1	vydávaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
vycházely	vycházet	k5eAaImAgFnP	vycházet
reprodukce	reprodukce	k1gFnPc4	reprodukce
Kataláncových	Kataláncův	k2eAgMnPc2d1	Kataláncův
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
Spolek	spolek	k1gInSc1	spolek
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
Mánes	Mánes	k1gMnSc1	Mánes
výstavu	výstava	k1gFnSc4	výstava
s	s	k7c7	s
názvem	název	k1gInSc7	název
Poesie	poesie	k1gFnSc1	poesie
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
konfrontaci	konfrontace	k1gFnSc6	konfrontace
českého	český	k2eAgNnSc2d1	české
surrealistického	surrealistický	k2eAgNnSc2d1	surrealistické
umění	umění	k1gNnSc2	umění
se	s	k7c7	s
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgMnS	být
kromě	kromě	k7c2	kromě
mnoha	mnoho	k4c2	mnoho
jiných	jiná	k1gFnPc2	jiná
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
i	i	k9	i
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
tak	tak	k6eAd1	tak
předběhla	předběhnout	k5eAaPmAgFnS	předběhnout
analogické	analogický	k2eAgInPc4d1	analogický
podniky	podnik	k1gInPc4	podnik
uspořádané	uspořádaný	k2eAgInPc4d1	uspořádaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
a	a	k8xC	a
1938	[number]	k4	1938
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
čeští	český	k2eAgMnPc1d1	český
umělci	umělec	k1gMnPc1	umělec
tvořili	tvořit	k5eAaImAgMnP	tvořit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pod	pod	k7c7	pod
přímým	přímý	k2eAgInSc7d1	přímý
Dalího	Dalí	k2eAgInSc2d1	Dalí
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
malířů	malíř	k1gMnPc2	malíř
Bohdana	Bohdan	k1gMnSc2	Bohdan
Laciny	Lacina	k1gMnSc2	Lacina
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
Zykmunda	Zykmund	k1gMnSc2	Zykmund
a	a	k8xC	a
fotografa	fotograf	k1gMnSc2	fotograf
Miroslava	Miroslav	k1gMnSc2	Miroslav
Háka	Hák	k1gMnSc2	Hák
<g/>
,	,	kIx,	,
zaujatých	zaujatý	k2eAgFnPc2d1	zaujatá
Dalího	Dalí	k1gMnSc4	Dalí
paraniocko-kritickou	paraniockoritický	k2eAgFnSc7d1	paraniocko-kritický
metodou	metoda	k1gFnSc7	metoda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc4d1	možné
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
nebo	nebo	k8xC	nebo
literaturu	literatura	k1gFnSc4	literatura
spojenou	spojený	k2eAgFnSc4d1	spojená
se	s	k7c7	s
surrealismem	surrealismus	k1gInSc7	surrealismus
veřejně	veřejně	k6eAd1	veřejně
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
.	.	kIx.	.
</s>
<s>
Ilegální	ilegální	k2eAgFnSc1d1	ilegální
skupina	skupina	k1gFnSc1	skupina
kolem	kolem	k7c2	kolem
Vratislava	Vratislav	k1gMnSc2	Vratislav
Effenbergera	Effenberger	k1gMnSc2	Effenberger
proto	proto	k8xC	proto
vydávala	vydávat	k5eAaPmAgFnS	vydávat
samizdaty	samizdat	k1gInPc4	samizdat
s	s	k7c7	s
názvem	název	k1gInSc7	název
Objekt	objekt	k1gInSc1	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
z	z	k7c2	z
října	říjen	k1gInSc2	říjen
1953	[number]	k4	1953
představoval	představovat	k5eAaImAgMnS	představovat
antologii	antologie	k1gFnSc4	antologie
surrealistické	surrealistický	k2eAgFnSc2d1	surrealistická
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
byl	být	k5eAaImAgMnS	být
Dalí	Dalí	k1gMnSc1	Dalí
rovněž	rovněž	k9	rovněž
zastoupen	zastoupen	k2eAgMnSc1d1	zastoupen
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
okruhu	okruh	k1gInSc2	okruh
kolem	kolem	k7c2	kolem
Effenbergera	Effenbergero	k1gNnSc2	Effenbergero
<g/>
,	,	kIx,	,
manželé	manžel	k1gMnPc1	manžel
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Medek	Medek	k1gMnSc1	Medek
a	a	k8xC	a
Emila	Emil	k1gMnSc2	Emil
Medková	Medková	k1gFnSc1	Medková
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
navazovali	navazovat	k5eAaImAgMnP	navazovat
na	na	k7c4	na
magický	magický	k2eAgInSc4d1	magický
realismus	realismus	k1gInSc4	realismus
Dalího	Dalí	k1gMnSc2	Dalí
i	i	k8xC	i
René	René	k1gMnSc1	René
Magritta	Magritta	k1gMnSc1	Magritta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tytéž	týž	k3xTgMnPc4	týž
umělce	umělec	k1gMnPc4	umělec
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
i	i	k9	i
část	část	k1gFnSc1	část
fotografické	fotografický	k2eAgFnSc2d1	fotografická
tvorby	tvorba	k1gFnSc2	tvorba
Evy	Eva	k1gFnSc2	Eva
Fukové	Fuková	k1gFnSc2	Fuková
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dalího	Dalí	k1gMnSc2	Dalí
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
obrazech	obraz	k1gInPc6	obraz
Evy	Eva	k1gFnSc2	Eva
Švankmajerové	Švankmajerová	k1gFnSc2	Švankmajerová
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
výstava	výstava	k1gFnSc1	výstava
Dalího	Dalí	k1gMnSc2	Dalí
obrazů	obraz	k1gInPc2	obraz
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Galerii	galerie	k1gFnSc6	galerie
D	D	kA	D
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Portheimka	Portheimka	k1gFnSc1	Portheimka
Galerie	galerie	k1gFnSc2	galerie
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sérii	série	k1gFnSc4	série
portrétů	portrét	k1gInPc2	portrét
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
slavného	slavný	k2eAgMnSc2d1	slavný
Katalánce	Katalánec	k1gMnSc2	Katalánec
fotograf	fotograf	k1gMnSc1	fotograf
Václav	Václav	k1gMnSc1	Václav
Chochola	Chochola	k1gFnSc1	Chochola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pasáži	pasáž	k1gFnSc6	pasáž
Jiřího	Jiří	k1gMnSc2	Jiří
Grossmanna	Grossmann	k1gMnSc2	Grossmann
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
plastika	plastika	k1gFnSc1	plastika
Dalího	Dalí	k1gMnSc2	Dalí
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Jana	Jan	k1gMnSc2	Jan
Bernata	Bernat	k1gMnSc2	Bernat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pražské	pražský	k2eAgMnPc4d1	pražský
Gallery	Galler	k1gMnPc4	Galler
of	of	k?	of
Art	Art	k1gFnSc1	Art
Prague	Prague	k1gFnSc1	Prague
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
expozice	expozice	k1gFnSc1	expozice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
Dalího	Dalí	k2eAgInSc2d1	Dalí
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Carré	Carrý	k2eAgInPc1d1	Carrý
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Art	Art	k1gMnSc1	Art
(	(	kIx(	(
<g/>
Salvador	Salvador	k1gMnSc1	Salvador
Dali	dát	k5eAaPmAgMnP	dát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Thiollet	Thiollet	k1gInSc1	Thiollet
<g/>
,	,	kIx,	,
Anagramme	Anagramme	k1gMnSc1	Anagramme
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-2-35035-189-6	[number]	k4	978-2-35035-189-6
</s>
</p>
<p>
<s>
Tajný	tajný	k2eAgInSc1d1	tajný
život	život	k1gInSc1	život
Salvadora	Salvador	k1gMnSc2	Salvador
Dalího	Dalí	k1gMnSc2	Dalí
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gInSc4	Salvador
Dalí	Dal	k1gFnPc2	Dal
<g/>
,	,	kIx,	,
Slovart	Slovarta	k1gFnPc2	Slovarta
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7209-582-X	[number]	k4	80-7209-582-X
</s>
</p>
<p>
<s>
GIBSON	GIBSON	kA	GIBSON
<g/>
,	,	kIx,	,
Ian	Ian	k1gMnSc1	Ian
<g/>
.	.	kIx.	.
</s>
<s>
Lorca	Lorca	k1gFnSc1	Lorca
-	-	kIx~	-
Dalí	Dalí	k1gFnSc1	Dalí
<g/>
:	:	kIx,	:
marná	marný	k2eAgFnSc1d1	marná
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Miluše	Miluše	k1gFnSc2	Miluše
Válková	Válková	k1gFnSc1	Válková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
340	[number]	k4	340
s.	s.	k?	s.
<g/>
,	,	kIx,	,
24	[number]	k4	24
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7209	[number]	k4	7209
<g/>
-	-	kIx~	-
<g/>
509	[number]	k4	509
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československý	československý	k2eAgMnSc1d1	československý
malíř	malíř	k1gMnSc1	malíř
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
MIRO	Mira	k1gFnSc5	Mira
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-904239-2-3	[number]	k4	978-80-904239-2-3
</s>
</p>
<p>
<s>
Matthes	Matthes	k1gMnSc1	Matthes
<g/>
,	,	kIx,	,
Axel	Axel	k1gMnSc1	Axel
–	–	k?	–
Stegmann	Stegmann	k1gMnSc1	Stegmann
<g/>
,	,	kIx,	,
Tilbert	Tilbert	k1gMnSc1	Tilbert
Diego	Diego	k1gMnSc1	Diego
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
–	–	k?	–
Unabhängigkeitserklärung	Unabhängigkeitserklärung	k1gMnSc1	Unabhängigkeitserklärung
der	drát	k5eAaImRp2nS	drát
Phantasie	Phantasie	k1gFnSc2	Phantasie
und	und	k?	und
Erklärung	Erklärung	k1gMnSc1	Erklärung
der	drát	k5eAaImRp2nS	drát
Rechte	Recht	k1gInSc5	Recht
des	des	k1gNnSc7	des
Menschen	Menschen	k1gInSc1	Menschen
auf	auf	k?	auf
seine	seinout	k5eAaPmIp3nS	seinout
Verrücktheit	Verrücktheit	k1gInSc1	Verrücktheit
<g/>
.	.	kIx.	.
</s>
<s>
Gesammelte	Gesammelit	k5eAaPmRp2nP	Gesammelit
Schriften	Schriften	k2eAgInSc4d1	Schriften
<g/>
,	,	kIx,	,
Rogner	Rogner	k1gInSc1	Rogner
&	&	k?	&
Bernhard	Bernhard	k1gInSc1	Bernhard
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Surrealismus	surrealismus	k1gInSc1	surrealismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k2eAgMnSc1d1	Dalí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Dalího	Dalí	k2eAgNnSc2d1	Dalí
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
St.	st.	kA	st.
Petersburgu	Petersburg	k1gInSc6	Petersburg
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Portréty	portrét	k1gInPc1	portrét
Salvadora	Salvador	k1gMnSc2	Salvador
Dalího	Dalí	k1gMnSc2	Dalí
od	od	k7c2	od
Václava	Václav	k1gMnSc2	Václav
Chocholy	chochol	k1gInPc4	chochol
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
na	na	k7c4	na
Artmuseum	Artmuseum	k1gInSc4	Artmuseum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
životopis	životopis	k1gInSc1	životopis
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Nadace	nadace	k1gFnSc2	nadace
Salvadora	Salvador	k1gMnSc4	Salvador
Dalí	Dalí	k1gFnSc2	Dalí
s	s	k7c7	s
reprodukcemi	reprodukce	k1gFnPc7	reprodukce
jeho	jeho	k3xOp3gMnPc2	jeho
obrazů	obraz	k1gInPc2	obraz
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
Dalímu	Dalí	k1gMnSc3	Dalí
včetně	včetně	k7c2	včetně
reprodukcí	reprodukce	k1gFnPc2	reprodukce
obrazů	obraz	k1gInPc2	obraz
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
