<s>
Tučňák	tučňák	k1gMnSc1
královský	královský	k2eAgMnSc1d1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vysoký	vysoký	k2eAgInSc1d1
až	až	k9
75	[number]	k4
cm	cm	kA
<g/>
,	,	kIx,
a	a	k8xC
vážit	vážit	k5eAaImF
až	až	k9
8	[number]	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s>
Tučňák	tučňák	k1gMnSc1
královský	královský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Eudyptes	Eudyptes	k1gMnSc1
schlegeli	schleget	k5eAaPmAgMnP
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velký	velký	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
čeledi	čeleď	k1gFnSc2
tučnákovitých	tučnákovitý	k2eAgFnPc2d1
obývající	obývající	k2eAgFnPc4d1
vody	voda	k1gFnPc4
obklopující	obklopující	k2eAgFnSc4d1
Antarktidu	Antarktida	k1gFnSc4
<g/>
.	.	kIx.
</s>