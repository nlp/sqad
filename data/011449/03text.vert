<p>
<s>
Delivered	Delivered	k1gInSc1	Delivered
ex	ex	k6eAd1	ex
Ship	Ship	k1gInSc1	Ship
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
DES	des	k1gNnSc2	des
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
s	s	k7c7	s
dodáním	dodání	k1gNnSc7	dodání
z	z	k7c2	z
lodi	loď	k1gFnSc2	loď
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
doložka	doložka	k1gFnSc1	doložka
Incoterms	Incotermsa	k1gFnPc2	Incotermsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doložka	doložka	k1gFnSc1	doložka
DES	des	k1gNnSc2	des
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kupující	kupující	k1gMnSc1	kupující
přebírá	přebírat	k5eAaImIp3nS	přebírat
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
a	a	k8xC	a
rizika	riziko	k1gNnPc4	riziko
za	za	k7c4	za
doručené	doručený	k2eAgNnSc4d1	doručené
zboží	zboží	k1gNnSc4	zboží
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
prodávající	prodávající	k2eAgNnSc4d1	prodávající
dá	dát	k5eAaPmIp3nS	dát
zboží	zboží	k1gNnSc4	zboží
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
ve	v	k7c6	v
sjednaném	sjednaný	k2eAgInSc6d1	sjednaný
přístavu	přístav	k1gInSc6	přístav
<g/>
,	,	kIx,	,
na	na	k7c6	na
obvyklém	obvyklý	k2eAgNnSc6d1	obvyklé
vykládacím	vykládací	k2eAgNnSc6d1	vykládací
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
vykládkou	vykládka	k1gFnSc7	vykládka
<g/>
.	.	kIx.	.
</s>
<s>
Povinností	povinnost	k1gFnSc7	povinnost
prodávajícího	prodávající	k2eAgMnSc2d1	prodávající
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
uvědomit	uvědomit	k5eAaPmF	uvědomit
kupujícího	kupující	k1gMnSc4	kupující
včas	včas	k6eAd1	včas
o	o	k7c6	o
předpokládaném	předpokládaný	k2eAgInSc6d1	předpokládaný
dni	den	k1gInSc6	den
příjezdu	příjezd	k1gInSc6	příjezd
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
zboží	zboží	k1gNnSc1	zboží
naloženo	naložen	k2eAgNnSc1d1	naloženo
a	a	k8xC	a
dodat	dodat	k5eAaPmF	dodat
mu	on	k3xPp3gMnSc3	on
včas	včas	k6eAd1	včas
konosament	konosament	k1gInSc4	konosament
<g/>
,	,	kIx,	,
vydací	vydací	k2eAgInSc4d1	vydací
list	list	k1gInSc4	list
<g/>
,	,	kIx,	,
osvědčení	osvědčení	k1gNnSc4	osvědčení
o	o	k7c6	o
původu	původ	k1gInSc6	původ
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
konzulární	konzulární	k2eAgFnSc4d1	konzulární
fakturu	faktura	k1gFnSc4	faktura
nebo	nebo	k8xC	nebo
další	další	k2eAgInPc4d1	další
potřebné	potřebný	k2eAgInPc4d1	potřebný
doklady	doklad	k1gInPc4	doklad
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mu	on	k3xPp3gMnSc3	on
případně	případně	k6eAd1	případně
poskytnout	poskytnout	k5eAaPmF	poskytnout
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
obstarávání	obstarávání	k1gNnSc6	obstarávání
dokladů	doklad	k1gInPc2	doklad
dalších	další	k2eAgInPc2d1	další
vystavovaných	vystavovaný	k2eAgInPc2d1	vystavovaný
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nalodění	nalodění	k1gNnSc2	nalodění
či	či	k8xC	či
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
