<s>
kupující	kupující	k1gMnSc1	kupující
přebírá	přebírat	k5eAaImIp3nS	přebírat
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
a	a	k8xC	a
rizika	riziko	k1gNnPc4	riziko
za	za	k7c4	za
doručené	doručený	k2eAgNnSc4d1	doručené
zboží	zboží	k1gNnSc4	zboží
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
prodávající	prodávající	k2eAgNnSc4d1	prodávající
dá	dát	k5eAaPmIp3nS	dát
zboží	zboží	k1gNnSc4	zboží
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
ve	v	k7c6	v
sjednaném	sjednaný	k2eAgInSc6d1	sjednaný
přístavu	přístav	k1gInSc6	přístav
</s>
