<s>
Uzyn	Uzyn	k1gMnSc1
</s>
<s>
Uzyn	Uzyn	k1gInSc1
У	У	k?
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
2	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
30	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
2	#num#	k4
Stát	stát	k1gInSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
oblast	oblast	k1gFnSc1
</s>
<s>
Kyjevská	kyjevský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Uzyn	Uzyn	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
67,00	67,00	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
12	#num#	k4
146	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
181,3	181,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
в	в	k?
<g/>
.	.	kIx.
Ж	Ж	k?
16	#num#	k4
<g/>
/	/	kIx~
<g/>
109161	#num#	k4
м	м	k?
У	У	k?
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
4463	#num#	k4
PSČ	PSČ	kA
</s>
<s>
09160-09163	09160-09163	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Uzyn	Uzyn	k1gNnSc1
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
У	У	k?
<g/>
;	;	kIx,
rusky	rusky	k6eAd1
У	У	k?
–	–	k?
Uzin	Uzin	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
Kyjevské	kyjevský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
jižně	jižně	k6eAd1
od	od	k7c2
Kyjeva	Kyjev	k1gInSc2
a	a	k8xC
východně	východně	k6eAd1
od	od	k7c2
Bílé	bílý	k2eAgFnSc2d1
Cerekve	Cerekev	k1gFnSc2
<g/>
,	,	kIx,
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
v	v	k7c6
něm	on	k3xPp3gMnSc6
žilo	žít	k5eAaImAgNnS
přes	přes	k7c4
dvanáct	dvanáct	k4xCc4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
celostátně	celostátně	k6eAd1
významný	významný	k2eAgInSc4d1
cukrovar	cukrovar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Rodáci	rodák	k1gMnPc1
</s>
<s>
Pavlo	Pavla	k1gFnSc5
Romanovyč	Romanovyč	k1gFnSc1
Popovyč	Popovyč	k1gMnSc1
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
sovětský	sovětský	k2eAgMnSc1d1
kosmonaut	kosmonaut	k1gMnSc1
ukrajinské	ukrajinský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kyjevská	kyjevský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Rajóny	rajón	k1gInPc1
</s>
<s>
Bilocerkevský	Bilocerkevský	k2eAgInSc1d1
•	•	k?
Boryspilský	Boryspilský	k2eAgInSc1d1
•	•	k?
Brovarský	Brovarský	k2eAgInSc1d1
•	•	k?
Bučský	Bučský	k2eAgInSc1d1
•	•	k?
Fastivský	Fastivský	k2eAgInSc1d1
•	•	k?
Obuchivský	Obuchivský	k2eAgInSc1d1
•	•	k?
Vyšhorodský	Vyšhorodský	k2eAgInSc1d1
Města	město	k1gNnSc2
</s>
<s>
Berezaň	Berezanit	k5eAaPmRp2nS
•	•	k?
Bila	bít	k5eAaImAgFnS
Cerkva	Cerkva	k1gFnSc1
•	•	k?
Bohuslav	Bohuslav	k1gMnSc1
•	•	k?
Bojarka	bojarka	k1gFnSc1
•	•	k?
Boryspil	Boryspil	k1gFnSc1
•	•	k?
Brovary	Brovar	k1gInPc1
•	•	k?
Buča	Buč	k1gInSc2
•	•	k?
Černobyl	Černobyl	k1gInSc1
•	•	k?
Fastiv	Fastiv	k1gInSc1
•	•	k?
Irpiň	Irpiň	k1gFnSc1
•	•	k?
Jahotyn	Jahotyn	k1gInSc1
•	•	k?
Kaharlyk	Kaharlyk	k1gInSc1
•	•	k?
Myronivka	Myronivka	k1gFnSc1
•	•	k?
Obuchiv	Obuchiva	k1gFnPc2
•	•	k?
Perejaslav	Perejaslav	k1gMnSc1
•	•	k?
Pripjať	Pripjať	k1gMnSc1
•	•	k?
Ržyščiv	Ržyščiv	k1gMnSc1
•	•	k?
Skvyra	Skvyra	k1gMnSc1
•	•	k?
Slavutyč	Slavutyč	k1gMnSc1
•	•	k?
Tarašča	Tarašča	k1gMnSc1
•	•	k?
Tetijiv	Tetijivo	k1gNnPc2
•	•	k?
Ukrajinka	Ukrajinka	k1gFnSc1
•	•	k?
Uzyn	Uzyn	k1gInSc1
•	•	k?
Vasylkiv	Vasylkiva	k1gFnPc2
•	•	k?
Vyšhorod	Vyšhorod	k1gInSc1
•	•	k?
Vyšneve	Vyšneev	k1gFnSc2
Sídla	sídlo	k1gNnSc2
městského	městský	k2eAgInSc2d1
typu	typ	k1gInSc2
</s>
<s>
Babynci	Babynek	k1gMnPc1
•	•	k?
Baryšivka	Baryšivka	k1gFnSc1
•	•	k?
Boroďanka	Boroďanka	k1gFnSc1
•	•	k?
Borova	Borov	k1gInSc2
•	•	k?
Čabany	Čabana	k1gFnSc2
•	•	k?
Doslidnycke	Doslidnyck	k1gFnSc2
•	•	k?
Dymer	Dymer	k1gMnSc1
•	•	k?
Hlevacha	Hlevacha	k1gMnSc1
•	•	k?
Hostomel	Hostomel	k1gMnSc1
•	•	k?
Hrebinky	Hrebinka	k1gFnSc2
•	•	k?
Ivankiv	Ivankiva	k1gFnPc2
•	•	k?
Kalynivka	Kalynivka	k1gFnSc1
(	(	kIx(
<g/>
Brovarský	Brovarský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kalynivka	Kalynivka	k1gFnSc1
(	(	kIx(
<g/>
Fastivský	Fastivský	k2eAgInSc1d1
rajón	rajón	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kalyta	Kalyta	k1gFnSc1
•	•	k?
Klavdijevo-Tarasove	Klavdijevo-Tarasov	k1gInSc5
•	•	k?
Kodra	Kodr	k1gMnSc2
•	•	k?
Kocjubynske	Kocjubynsk	k1gMnSc2
•	•	k?
Kozyn	Kozyn	k1gMnSc1
•	•	k?
Kožanka	Kožanka	k1gFnSc1
•	•	k?
Krasjatyči	Krasjatyč	k1gFnSc6
•	•	k?
Makariv	Makariva	k1gFnPc2
•	•	k?
Nemišajeve	Nemišajeev	k1gFnSc2
•	•	k?
Piskivka	Piskivka	k1gFnSc1
•	•	k?
Rokytne	Rokytne	k1gFnSc2
•	•	k?
Stavyšče	Stavyšč	k1gFnSc2
•	•	k?
Terezyne	Terezyn	k1gInSc5
•	•	k?
Velyka	Velyka	k1gFnSc1
Dymerka	Dymerka	k1gFnSc1
•	•	k?
Volodarka	Volodarka	k1gFnSc1
•	•	k?
Vorzel	Vorzel	k1gFnSc1
•	•	k?
Zhurivka	Zhurivka	k1gFnSc1
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
Administrativním	administrativní	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
Kyjev	Kyjev	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nepatří	patřit	k5eNaImIp3nS
do	do	k7c2
Kyjevské	kyjevský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
je	být	k5eAaImIp3nS
městem	město	k1gNnSc7
se	s	k7c7
speciálním	speciální	k2eAgInSc7d1
statutem	statut	k1gInSc7
na	na	k7c6
úrovni	úroveň	k1gFnSc6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
