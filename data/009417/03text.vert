<p>
<s>
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Bangladéšská	bangladéšský	k2eAgFnSc1d1	Bangladéšská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
bengálsky	bengálsky	k6eAd1	bengálsky
গ	গ	k?	গ
<g/>
্	্	k?	্
<g/>
র	র	k?	র
<g/>
া	া	k?	া
<g/>
ত	ত	k?	ত
<g/>
্	্	k?	্
<g/>
ত	ত	k?	ত
<g/>
্	্	k?	্
<g/>
র	র	k?	র
<g/>
ী	ী	k?	ী
ব	ব	k?	ব
<g/>
া	া	k?	া
<g/>
ং	ং	k?	ং
<g/>
ল	ল	k?	ল
<g/>
া	া	k?	া
<g/>
দ	দ	k?	দ
<g/>
ে	ে	k?	ে
<g/>
শ	শ	k?	শ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
hovořeno	hovořen	k2eAgNnSc1d1	hovořeno
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
rodě	rod	k1gInSc6	rod
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc1d1	jediné
přípustné	přípustný	k2eAgNnSc1d1	přípustné
skloňování	skloňování	k1gNnSc1	skloňování
podle	podle	k7c2	podle
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
Myanmar	Myanmar	k1gInSc1	Myanmar
(	(	kIx(	(
<g/>
Barma	Barma	k1gFnSc1	Barma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Dháka	Dháka	k1gFnSc1	Dháka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
158	[number]	k4	158
milionů	milion	k4xCgInPc2	milion
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
chudá	chudý	k2eAgFnSc1d1	chudá
rozvojová	rozvojový	k2eAgFnSc1d1	rozvojová
země	země	k1gFnSc1	země
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
nejlidnatějším	lidnatý	k2eAgInPc3d3	nejlidnatější
státům	stát	k1gInPc3	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
bengálština	bengálština	k1gFnSc1	bengálština
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
bengálská	bengálský	k2eAgFnSc1d1	bengálská
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Bengálsko	Bengálsko	k1gNnSc1	Bengálsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
tvořil	tvořit	k5eAaImAgInS	tvořit
součást	součást	k1gFnSc4	součást
Maurjovské	Maurjovský	k2eAgFnSc2d1	Maurjovský
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zemí	zem	k1gFnSc7	zem
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
šíří	šířit	k5eAaImIp3nS	šířit
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Bengálsko	Bengálsko	k1gNnSc1	Bengálsko
britskou	britský	k2eAgFnSc7d1	britská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
vládli	vládnout	k5eAaImAgMnP	vládnout
hinduističtí	hinduistický	k2eAgMnPc1d1	hinduistický
vládci	vládce	k1gMnPc1	vládce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
muslimy	muslim	k1gMnPc7	muslim
a	a	k8xC	a
hinduisty	hinduista	k1gMnPc7	hinduista
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bengálský	bengálský	k2eAgInSc1d1	bengálský
hladomor	hladomor	k1gInSc1	hladomor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
zahubil	zahubit	k5eAaPmAgMnS	zahubit
až	až	k9	až
třetinu	třetina	k1gFnSc4	třetina
populace	populace	k1gFnSc2	populace
Bengálska	Bengálsko	k1gNnSc2	Bengálsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
bylo	být	k5eAaImAgNnS	být
Bengálsko	Bengálsko	k1gNnSc1	Bengálsko
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
hinduistickou	hinduistický	k2eAgFnSc4d1	hinduistická
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
muslimskou	muslimský	k2eAgFnSc4d1	muslimská
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
Bengálsko	Bengálsko	k1gNnSc1	Bengálsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
opět	opět	k6eAd1	opět
sjednoceno	sjednotit	k5eAaPmNgNnS	sjednotit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hladomoru	hladomor	k1gInSc6	hladomor
v	v	k7c6	v
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
zahynuly	zahynout	k5eAaPmAgInP	zahynout
až	až	k6eAd1	až
tři	tři	k4xCgInPc1	tři
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
nechali	nechat	k5eAaPmAgMnP	nechat
Britové	Brit	k1gMnPc1	Brit
Britskou	britský	k2eAgFnSc4d1	britská
Indii	Indie	k1gFnSc4	Indie
svému	svůj	k3xOyFgInSc3	svůj
osudu	osud	k1gInSc3	osud
<g/>
.	.	kIx.	.
</s>
<s>
Bengálsko	Bengálsko	k1gNnSc1	Bengálsko
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
podle	podle	k7c2	podle
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
hinduistický	hinduistický	k2eAgInSc1d1	hinduistický
západ	západ	k1gInSc1	západ
připadl	připadnout	k5eAaPmAgInS	připadnout
Indii	Indie	k1gFnSc4	Indie
a	a	k8xC	a
muslimský	muslimský	k2eAgInSc4d1	muslimský
východ	východ	k1gInSc4	východ
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Východní	východní	k2eAgInSc4d1	východní
Pákistán	Pákistán	k1gInSc4	Pákistán
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
Islámské	islámský	k2eAgFnSc2d1	islámská
republiky	republika	k1gFnSc2	republika
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInSc1d1	západní
Pákistán	Pákistán	k1gInSc1	Pákistán
byl	být	k5eAaImAgInS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
od	od	k7c2	od
Východního	východní	k2eAgInSc2d1	východní
přes	přes	k7c4	přes
1500	[number]	k4	1500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
Lidová	lidový	k2eAgFnSc1d1	lidová
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
autonomii	autonomie	k1gFnSc4	autonomie
Bengálska	Bengálsko	k1gNnSc2	Bengálsko
<g/>
.	.	kIx.	.
</s>
<s>
Pákistánská	pákistánský	k2eAgFnSc1d1	pákistánská
vláda	vláda	k1gFnSc1	vláda
ale	ale	k8xC	ale
autonomii	autonomie	k1gFnSc4	autonomie
nezavedla	zavést	k5eNaPmAgFnS	zavést
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
poslala	poslat	k5eAaPmAgFnS	poslat
do	do	k7c2	do
východního	východní	k2eAgInSc2d1	východní
Bengálska	Bengálsko	k1gNnPc1	Bengálsko
své	svůj	k3xOyFgNnSc4	svůj
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
statisíce	statisíce	k1gInPc4	statisíce
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
východní	východní	k2eAgNnSc1d1	východní
Bengálsko	Bengálsko	k1gNnSc1	Bengálsko
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
a	a	k8xC	a
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1971	[number]	k4	1971
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
premiérem	premiér	k1gMnSc7	premiér
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vůdce	vůdce	k1gMnSc1	vůdce
Lidové	lidový	k2eAgFnSc2d1	lidová
ligy	liga	k1gFnSc2	liga
šejch	šejch	k1gMnSc1	šejch
Mudžíbur	Mudžíbur	k1gMnSc1	Mudžíbur
Rahmán	Rahmán	k2eAgMnSc1d1	Rahmán
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
při	při	k7c6	při
převratu	převrat	k1gInSc6	převrat
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
demokratického	demokratický	k2eAgInSc2d1	demokratický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
dominují	dominovat	k5eAaImIp3nP	dominovat
Národní	národní	k2eAgFnSc1d1	národní
strana	strana	k1gFnSc1	strana
vedená	vedený	k2eAgFnSc1d1	vedená
Chálid	Chálid	k1gInSc4	Chálid
Ziovou	Ziova	k1gFnSc7	Ziova
a	a	k8xC	a
Awami	Awa	k1gFnPc7	Awa
League	Leagu	k1gFnSc2	Leagu
vedená	vedený	k2eAgFnSc1d1	vedená
Šejch	šejch	k1gMnSc1	šejch
Hasínvou	Hasínva	k1gFnSc7	Hasínva
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Mudžíbura	Mudžíbur	k1gMnSc2	Mudžíbur
Rahmána	Rahmán	k1gMnSc2	Rahmán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
vláda	vláda	k1gFnSc1	vláda
Šejch	šejch	k1gMnSc1	šejch
Hasínové	Hasínové	k2eAgInSc1d1	Hasínové
trestní	trestní	k2eAgInSc1d1	trestní
tribunál	tribunál	k1gInSc1	tribunál
stíhající	stíhající	k2eAgFnSc2d1	stíhající
pachatele	pachatel	k1gMnSc4	pachatel
zločinů	zločin	k1gInPc2	zločin
z	z	k7c2	z
období	období	k1gNnSc2	období
Bangladéšské	bangladéšský	k2eAgFnSc2d1	Bangladéšská
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
jsou	být	k5eAaImIp3nP	být
stíháni	stíhán	k2eAgMnPc1d1	stíhán
především	především	k9	především
bangladéšští	bangladéšský	k2eAgMnPc1d1	bangladéšský
islamisté	islamista	k1gMnPc1	islamista
a	a	k8xC	a
představitelé	představitel	k1gMnPc1	představitel
opoziční	opoziční	k2eAgFnSc2d1	opoziční
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Human	Human	k1gMnSc1	Human
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watch	k1gMnSc1	Watch
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
nezaujatosti	nezaujatost	k1gFnSc6	nezaujatost
soudního	soudní	k2eAgInSc2d1	soudní
tribunálu	tribunál	k1gInSc2	tribunál
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
řek	řeka	k1gFnPc2	řeka
Gangy	Ganga	k1gFnSc2	Ganga
a	a	k8xC	a
Brahmaputry	Brahmaputr	k1gInPc4	Brahmaputr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přinášejí	přinášet	k5eAaImIp3nP	přinášet
úrodné	úrodný	k2eAgInPc1d1	úrodný
náplavy	náplav	k1gInPc1	náplav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ničivé	ničivý	k2eAgFnPc4d1	ničivá
záplavy	záplava	k1gFnPc4	záplava
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Bengálského	bengálský	k2eAgInSc2d1	bengálský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
povrch	povrch	k6eAd1wR	povrch
zvedá	zvedat	k5eAaImIp3nS	zvedat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Čittágongské	Čittágongský	k2eAgFnPc1d1	Čittágongský
hory	hora	k1gFnPc1	hora
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnPc1	podnebí
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
monzuny	monzun	k1gInPc4	monzun
<g/>
,	,	kIx,	,
přinášející	přinášející	k2eAgFnPc4d1	přinášející
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
a	a	k8xC	a
vydatné	vydatný	k2eAgFnPc4d1	vydatná
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
ničivé	ničivý	k2eAgFnPc1d1	ničivá
povodně	povodeň	k1gFnPc1	povodeň
a	a	k8xC	a
tajfuny	tajfun	k1gInPc1	tajfun
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pravidelně	pravidelně	k6eAd1	pravidelně
připravují	připravovat	k5eAaImIp3nP	připravovat
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
o	o	k7c4	o
obydlí	obydlí	k1gNnSc4	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Cyklón	cyklón	k1gInSc1	cyklón
Bhola	Bholo	k1gNnSc2	Bholo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
zahubil	zahubit	k5eAaPmAgInS	zahubit
až	až	k9	až
500	[number]	k4	500
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Cyklón	cyklón	k1gInSc1	cyklón
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
usmrtil	usmrtit	k5eAaPmAgInS	usmrtit
přes	přes	k7c4	přes
140	[number]	k4	140
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
odkázána	odkázat	k5eAaPmNgFnS	odkázat
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
potravinovou	potravinový	k2eAgFnSc4d1	potravinová
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
Bangladéš	Bangladéš	k1gInSc4	Bangladéš
jako	jako	k8xC	jako
stát	stát	k1gInSc4	stát
s	s	k7c7	s
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
má	mít	k5eAaImIp3nS	mít
vést	vést	k5eAaImF	vést
volený	volený	k2eAgMnSc1d1	volený
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
období	období	k1gNnSc1	období
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
stavu	stav	k1gInSc2	stav
ale	ale	k8xC	ale
znemožnilo	znemožnit	k5eAaPmAgNnS	znemožnit
toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
státu	stát	k1gInSc2	stát
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
8	[number]	k4	8
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
bengálsky	bengálsky	k6eAd1	bengálsky
ব	ব	k?	ব
<g/>
ি	ি	k?	ি
<g/>
ভ	ভ	k?	ভ
<g/>
া	া	k?	া
<g/>
গ	গ	k?	গ
bibhága	bibhága	k1gFnSc1	bibhága
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
ব	ব	k?	ব
<g/>
ি	ি	k?	ি
<g/>
ভ	ভ	k?	ভ
<g/>
া	া	k?	া
<g/>
গ	গ	k?	গ
<g/>
ে	ে	k?	ে
bibhágé	bibhágá	k1gFnSc2	bibhágá
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
division	division	k1gInSc1	division
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každé	každý	k3xTgNnSc1	každý
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
podle	podle	k7c2	podle
svého	svůj	k3xOyFgNnSc2	svůj
správního	správní	k2eAgNnSc2d1	správní
střediska	středisko	k1gNnSc2	středisko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ঢ	ঢ	k?	ঢ
<g/>
া	া	k?	া
<g/>
ক	ক	k?	ক
<g/>
া	া	k?	া
/	/	kIx~	/
Dháka	Dháka	k1gMnSc1	Dháka
/	/	kIx~	/
Dhaka	Dhaka	k1gMnSc1	Dhaka
</s>
</p>
<p>
<s>
চ	চ	k?	চ
<g/>
্	্	k?	্
<g/>
ট	ট	k?	ট
<g/>
্	্	k?	্
<g/>
র	র	k?	র
<g/>
া	া	k?	া
<g/>
ম	ম	k?	ম
/	/	kIx~	/
Čattagrám	Čattagra	k1gFnPc3	Čattagra
/	/	kIx~	/
Chittagong	Chittagong	k1gInSc1	Chittagong
</s>
</p>
<p>
<s>
র	র	k?	র
<g/>
া	া	k?	া
<g/>
জ	জ	k?	জ
<g/>
া	া	k?	া
<g/>
হ	হ	k?	হ
<g/>
ী	ী	k?	ী
/	/	kIx~	/
Rádžašáhí	Rádžašáhí	k2eAgFnSc4d1	Rádžašáhí
/	/	kIx~	/
Rajshahi	Rajshahe	k1gFnSc4	Rajshahe
</s>
</p>
<p>
<s>
ব	ব	k?	ব
<g/>
ি	ি	k?	ি
<g/>
শ	শ	k?	শ
<g/>
া	া	k?	া
<g/>
ল	ল	k?	ল
/	/	kIx~	/
Barišál	Barišál	k1gMnSc1	Barišál
/	/	kIx~	/
Barisal	Barisal	k1gMnSc1	Barisal
</s>
</p>
<p>
<s>
খ	খ	k?	খ
<g/>
ু	ু	k?	ু
<g/>
ল	ল	k?	ল
<g/>
া	া	k?	া
/	/	kIx~	/
Khulaná	Khulaný	k2eAgFnSc1d1	Khulaný
/	/	kIx~	/
Khulna	Khulna	k1gFnSc1	Khulna
</s>
</p>
<p>
<s>
স	স	k?	স
<g/>
ি	ি	k?	ি
<g/>
ল	ল	k?	ল
<g/>
ে	ে	k?	ে
<g/>
ট	ট	k?	ট
/	/	kIx~	/
Silét	Silét	k1gMnSc1	Silét
/	/	kIx~	/
Sylhet	Sylhet	k1gMnSc1	Sylhet
</s>
</p>
<p>
<s>
র	র	k?	র
<g/>
ং	ং	k?	ং
<g/>
প	প	k?	প
<g/>
ু	ু	k?	ু
<g/>
র	র	k?	র
/	/	kIx~	/
Rangpur	Rangpur	k1gMnSc1	Rangpur
/	/	kIx~	/
Rangpur	Rangpur	k1gMnSc1	Rangpur
</s>
</p>
<p>
<s>
ম	ম	k?	ম
<g/>
়	়	k?	়
<g/>
ম	ম	k?	ম
<g/>
ি	ি	k?	ি
<g/>
ং	ং	k?	ং
<g/>
হ	হ	k?	হ
/	/	kIx~	/
Mymensingh	Mymensingh	k1gMnSc1	Mymensingh
/	/	kIx~	/
Mymensingh	Mymensingh	k1gMnSc1	Mymensingh
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Relativně	relativně	k6eAd1	relativně
vysoké	vysoký	k2eAgInPc1d1	vysoký
výnosy	výnos	k1gInPc1	výnos
zemědělství	zemědělství	k1gNnSc2	zemědělství
neuživí	uživit	k5eNaPmIp3nP	uživit
vysoký	vysoký	k2eAgInSc4d1	vysoký
přírůstek	přírůstek	k1gInSc4	přírůstek
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podvýživa	podvýživa	k1gFnSc1	podvýživa
je	být	k5eAaImIp3nS	být
běžným	běžný	k2eAgInSc7d1	běžný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejchudším	chudý	k2eAgNnPc3d3	nejchudší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
podle	podle	k7c2	podle
FAOSTAT	FAOSTAT	kA	FAOSTAT
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
producentům	producent	k1gMnPc3	producent
juty	juta	k1gFnSc2	juta
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ryb	ryba	k1gFnPc2	ryba
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tropického	tropický	k2eAgNnSc2d1	tropické
ovoce	ovoce	k1gNnSc2	ovoce
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brambor	brambor	k1gInSc1	brambor
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
a	a	k8xC	a
čaje	čaj	k1gInSc2	čaj
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
-	-	kIx~	-
číslo	číslo	k1gNnSc1	číslo
uvádí	uvádět	k5eAaImIp3nS	uvádět
velikost	velikost	k1gFnSc4	velikost
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
srovnání	srovnání	k1gNnSc6	srovnání
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
plodinami	plodina	k1gFnPc7	plodina
jsou	být	k5eAaImIp3nP	být
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc2	pšenice
a	a	k8xC	a
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
z	z	k7c2	z
technických	technický	k2eAgFnPc2d1	technická
plodin	plodina	k1gFnPc2	plodina
bavlník	bavlník	k1gInSc1	bavlník
a	a	k8xC	a
juta	juta	k1gFnSc1	juta
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pytlovina	pytlovina	k1gFnSc1	pytlovina
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
drůbež	drůbež	k1gFnSc1	drůbež
a	a	k8xC	a
vodní	vodní	k2eAgMnPc1d1	vodní
buvoli	buvol	k1gMnPc1	buvol
<g/>
,	,	kIx,	,
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
říčních	říční	k2eAgInPc2d1	říční
veletoků	veletok	k1gInPc2	veletok
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
a	a	k8xC	a
silnice	silnice	k1gFnPc1	silnice
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Specifickým	specifický	k2eAgInSc7d1	specifický
druhem	druh	k1gInSc7	druh
příjmů	příjem	k1gInPc2	příjem
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
příjmech	příjem	k1gInPc6	příjem
z	z	k7c2	z
oděvního	oděvní	k2eAgInSc2d1	oděvní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
peníze	peníz	k1gInPc4	peníz
plynoucí	plynoucí	k2eAgInPc4d1	plynoucí
z	z	k7c2	z
OSN	OSN	kA	OSN
za	za	k7c2	za
vysílání	vysílání	k1gNnSc2	vysílání
bangladéšských	bangladéšský	k2eAgMnPc2d1	bangladéšský
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
policistů	policista	k1gMnPc2	policista
do	do	k7c2	do
mírových	mírový	k2eAgFnPc2d1	mírová
misí	mise	k1gFnPc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
přispívá	přispívat	k5eAaImIp3nS	přispívat
vládě	vláda	k1gFnSc3	vláda
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
nejen	nejen	k6eAd1	nejen
kapitační	kapitační	k2eAgFnPc1d1	kapitační
platby	platba	k1gFnPc1	platba
za	za	k7c7	za
vojáky	voják	k1gMnPc7	voják
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1000	[number]	k4	1000
USD	USD	kA	USD
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
za	za	k7c4	za
používání	používání	k1gNnSc4	používání
vojenské	vojenský	k2eAgFnSc2d1	vojenská
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
nábytku	nábytek	k1gInSc2	nábytek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
75	[number]	k4	75
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
z	z	k7c2	z
44	[number]	k4	44
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
160	[number]	k4	160
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
Dháky	Dháka	k1gFnSc2	Dháka
žije	žít	k5eAaImIp3nS	žít
až	až	k9	až
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejhustěji	husto	k6eAd3	husto
osídleným	osídlený	k2eAgInSc7d1	osídlený
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
pomineme	pominout	k5eAaPmIp1nP	pominout
malé	malý	k2eAgFnPc1d1	malá
ostrovní	ostrovní	k2eAgFnPc1d1	ostrovní
země	zem	k1gFnPc1	zem
a	a	k8xC	a
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
na	na	k7c6	na
území	území	k1gNnSc6	území
jen	jen	k9	jen
o	o	k7c6	o
málo	málo	k6eAd1	málo
větším	veliký	k2eAgMnSc6d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
prvorepublikové	prvorepublikový	k2eAgNnSc1d1	prvorepublikové
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
tísní	tísnit	k5eAaImIp3nS	tísnit
více	hodně	k6eAd2	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
bezmála	bezmála	k6eAd1	bezmála
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
obyvatel	obyvatel	k1gMnPc2	obyvatel
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
nakupení	nakupení	k1gNnSc1	nakupení
populace	populace	k1gFnSc2	populace
představuje	představovat	k5eAaImIp3nS	představovat
problém	problém	k1gInSc4	problém
při	při	k7c6	při
častých	častý	k2eAgFnPc6d1	častá
přírodních	přírodní	k2eAgFnPc6d1	přírodní
katastrofách	katastrofa	k1gFnPc6	katastrofa
a	a	k8xC	a
také	také	k9	také
velmi	velmi	k6eAd1	velmi
zatěžuje	zatěžovat	k5eAaImIp3nS	zatěžovat
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bengálci	Bengálec	k1gMnPc1	Bengálec
tvoří	tvořit	k5eAaImIp3nP	tvořit
drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
populace	populace	k1gFnSc2	populace
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
87	[number]	k4	87
%	%	kIx~	%
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
velká	velký	k2eAgFnSc1d1	velká
menšina	menšina	k1gFnSc1	menšina
hinduistů	hinduista	k1gMnPc2	hinduista
(	(	kIx(	(
<g/>
asi	asi	k9	asi
12	[number]	k4	12
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
regionu	region	k1gInSc6	region
Čittágongských	Čittágongský	k2eAgFnPc2d1	Čittágongský
hor	hora	k1gFnPc2	hora
žijí	žít	k5eAaImIp3nP	žít
domorodé	domorodý	k2eAgInPc1d1	domorodý
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
převážně	převážně	k6eAd1	převážně
buddhismus	buddhismus	k1gInSc4	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
žije	žít	k5eAaImIp3nS	žít
též	též	k9	též
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
tisíc	tisíc	k4xCgInPc2	tisíc
muslimských	muslimský	k2eAgInPc2d1	muslimský
Rohingyů	Rohingy	k1gInPc2	Rohingy
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
před	před	k7c7	před
pronásledováním	pronásledování	k1gNnSc7	pronásledování
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
Myanmaru	Myanmar	k1gInSc6	Myanmar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
exilu	exil	k1gInSc6	exil
dnes	dnes	k6eAd1	dnes
působí	působit	k5eAaImIp3nS	působit
feministická	feministický	k2eAgFnSc1d1	feministická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Taslima	Taslima	k1gFnSc1	Taslima
Nasrinová	Nasrinová	k1gFnSc1	Nasrinová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
vyšla	vyjít	k5eAaPmAgFnS	vyjít
její	její	k3xOp3gFnSc1	její
kniha	kniha	k1gFnSc1	kniha
Lajja	Lajja	k1gFnSc1	Lajja
(	(	kIx(	(
<g/>
Stud	stud	k1gInSc1	stud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
osudu	osud	k1gInSc6	osud
hinduistické	hinduistický	k2eAgFnSc2d1	hinduistická
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
si	se	k3xPyFc3	se
vylévali	vylévat	k5eAaImAgMnP	vylévat
zlost	zlost	k1gFnSc4	zlost
muslimští	muslimský	k2eAgMnPc1d1	muslimský
fundamentalisté	fundamentalista	k1gMnPc1	fundamentalista
po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
jedné	jeden	k4xCgFnSc2	jeden
mešity	mešita	k1gFnSc2	mešita
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
muslimy	muslim	k1gMnPc4	muslim
často	často	k6eAd1	často
veřejně	veřejně	k6eAd1	veřejně
pálena	pálen	k2eAgFnSc1d1	pálena
<g/>
,	,	kIx,	,
bangladéšská	bangladéšský	k2eAgFnSc1d1	Bangladéšská
vláda	vláda	k1gFnSc1	vláda
spisovatelku	spisovatelka	k1gFnSc4	spisovatelka
oficiálně	oficiálně	k6eAd1	oficiálně
obvinila	obvinit	k5eAaPmAgFnS	obvinit
z	z	k7c2	z
rouhání	rouhání	k1gNnSc2	rouhání
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jí	jíst	k5eAaImIp3nS	jíst
zabaven	zabaven	k2eAgInSc4d1	zabaven
pas	pas	k1gInSc4	pas
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
knihy	kniha	k1gFnPc1	kniha
zakázány	zakázat	k5eAaPmNgFnP	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Muslimští	muslimský	k2eAgMnPc1d1	muslimský
duchovní	duchovní	k1gMnSc1	duchovní
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
fatvu	fatva	k1gFnSc4	fatva
za	za	k7c4	za
kritiku	kritika	k1gFnSc4	kritika
Koránu	korán	k1gInSc2	korán
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc3	International
<g/>
,	,	kIx,	,
PEN	PEN	kA	PEN
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
vyslance	vyslanec	k1gMnSc2	vyslanec
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
vycestovala	vycestovat	k5eAaPmAgFnS	vycestovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
z	z	k7c2	z
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
azyl	azyl	k1gInSc4	azyl
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
<g/>
Národním	národní	k2eAgMnSc7d1	národní
básníkem	básník	k1gMnSc7	básník
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
je	být	k5eAaImIp3nS	být
Kazi	Kazi	k1gFnSc1	Kazi
Nazrul	Nazrul	k1gInSc4	Nazrul
Islam	Islam	k1gInSc1	Islam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významným	významný	k2eAgMnSc7d1	významný
architektem	architekt	k1gMnSc7	architekt
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
byl	být	k5eAaImAgMnS	být
Fazlur	Fazlur	k1gMnSc1	Fazlur
Rahman	Rahman	k1gMnSc1	Rahman
Khan	Khan	k1gMnSc1	Khan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Věda	věda	k1gFnSc1	věda
===	===	k?	===
</s>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
bangladéšským	bangladéšský	k2eAgMnSc7d1	bangladéšský
vědcem	vědec	k1gMnSc7	vědec
je	být	k5eAaImIp3nS	být
ekonom	ekonom	k1gMnSc1	ekonom
Muhammad	Muhammad	k1gInSc1	Muhammad
Yunus	Yunus	k1gInSc1	Yunus
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
konceptem	koncept	k1gInSc7	koncept
mikroúvěru	mikroúvěr	k1gInSc2	mikroúvěr
(	(	kIx(	(
<g/>
poskytování	poskytování	k1gNnSc4	poskytování
drobných	drobný	k2eAgFnPc2d1	drobná
půjček	půjčka	k1gFnPc2	půjčka
chudým	chudý	k1gMnPc3	chudý
podnikatelům	podnikatel	k1gMnPc3	podnikatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
málo	málo	k6eAd1	málo
kredibilní	kredibilní	k2eAgFnPc1d1	kredibilní
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
získali	získat	k5eAaPmAgMnP	získat
úvěr	úvěr	k1gInSc4	úvěr
od	od	k7c2	od
tradiční	tradiční	k2eAgFnSc2d1	tradiční
banky	banka	k1gFnSc2	banka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
obdržel	obdržet	k5eAaPmAgInS	obdržet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
bankou	banka	k1gFnSc7	banka
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Grameen	Gramena	k1gFnPc2	Gramena
Bank	bank	k1gInSc1	bank
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
ho	on	k3xPp3gInSc4	on
bangladéšská	bangladéšský	k2eAgFnSc1d1	Bangladéšská
vláda	vláda	k1gFnSc1	vláda
donutila	donutit	k5eAaPmAgFnS	donutit
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Bangladéši	Bangladéš	k1gInSc6	Bangladéš
je	být	k5eAaImIp3nS	být
kriket	kriket	k1gInSc1	kriket
<g/>
.	.	kIx.	.
</s>
<s>
Bangladéšská	bangladéšský	k2eAgFnSc1d1	Bangladéšská
kriketová	kriketový	k2eAgFnSc1d1	kriketová
reprezentace	reprezentace	k1gFnSc1	reprezentace
došla	dojít	k5eAaPmAgFnS	dojít
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
nejdál	daleko	k6eAd3	daleko
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Národním	národní	k2eAgInSc7d1	národní
sportem	sport	k1gInSc7	sport
je	on	k3xPp3gNnSc4	on
kabadi	kabad	k1gMnPc1	kabad
<g/>
.	.	kIx.	.
</s>
<s>
Šachista	šachista	k1gMnSc1	šachista
Niaz	Niaz	k1gMnSc1	Niaz
Murshed	Murshed	k1gMnSc1	Murshed
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
šachovým	šachový	k2eAgMnSc7d1	šachový
velmistrem	velmistr	k1gMnSc7	velmistr
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
poprvé	poprvé	k6eAd1	poprvé
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
žádnou	žádný	k3yNgFnSc4	žádný
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FILIPSKÝ	FILIPSKÝ	kA	FILIPSKÝ
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
,	,	kIx,	,
Cejlonu	Cejlon	k1gInSc2	Cejlon
<g/>
,	,	kIx,	,
Malediv	Maledivy	k1gFnPc2	Maledivy
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
Šrí	Šrí	k1gFnSc1	Šrí
Lanky	lanko	k1gNnPc7	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
768	[number]	k4	768
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KNOTKOVÁ	Knotková	k1gFnSc1	Knotková
<g/>
,	,	kIx,	,
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
.	.	kIx.	.
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
290	[number]	k4	290
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bangladéš	Bangladéš	k1gInSc4	Bangladéš
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
bangladéšské	bangladéšský	k2eAgFnSc2d1	Bangladéšská
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
Bangladesh	Bangladesh	k1gInSc1	Bangladesh
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Bangladesh	Bangladesh	k1gInSc1	Bangladesh
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
South	South	k1gMnSc1	South
and	and	k?	and
Central	Central	k1gMnSc1	Central
Asian	Asian	k1gMnSc1	Asian
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Bangladesh	Bangladesh	k1gInSc1	Bangladesh
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2010-05-24	[number]	k4	2010-05-24
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Bangladesh	Bangladesh	k1gInSc1	Bangladesh
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-08	[number]	k4	2011-07-08
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-03-25	[number]	k4	2011-03-25
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HUSAIN	HUSAIN	kA	HUSAIN
<g/>
,	,	kIx,	,
Syed	Syed	k1gInSc1	Syed
Sajjad	Sajjad	k1gInSc1	Sajjad
<g/>
;	;	kIx,	;
TINKER	TINKER	kA	TINKER
<g/>
,	,	kIx,	,
Hugh	Hugh	k1gMnSc1	Hugh
Russell	Russell	k1gMnSc1	Russell
<g/>
.	.	kIx.	.
</s>
<s>
Bangladesh	Bangladesh	k1gInSc1	Bangladesh
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pomáhající	pomáhající	k2eAgFnSc1d1	pomáhající
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
helpforlife	helpforlif	k1gInSc5	helpforlif
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
