<p>
<s>
Jestřáb	jestřáb	k1gMnSc1	jestřáb
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Accipiter	Accipiter	k1gInSc1	Accipiter
gentilis	gentilis	k1gInSc1	gentilis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
dravce	dravec	k1gMnSc2	dravec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
jestřábovitých	jestřábovitý	k2eAgFnPc2d1	jestřábovitý
(	(	kIx(	(
<g/>
Accipitridae	Accipitridae	k1gFnPc2	Accipitridae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	s	k7c7	s
8	[number]	k4	8
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
A.	A.	kA	A.
g.	g.	k?	g.
gentilis	gentilis	k1gInSc1	gentilis
(	(	kIx(	(
<g/>
Linnaeus	Linnaeus	k1gInSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
–	–	k?	–
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
A.	A.	kA	A.
g.	g.	k?	g.
arrigonii	arrigonie	k1gFnSc6	arrigonie
(	(	kIx(	(
<g/>
Kleinschmidt	Kleinschmidt	k1gInSc1	Kleinschmidt
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
–	–	k?	–
Korsika	Korsika	k1gFnSc1	Korsika
<g/>
,	,	kIx,	,
Sardinie	Sardinie	k1gFnSc1	Sardinie
</s>
</p>
<p>
<s>
A.	A.	kA	A.
g.	g.	k?	g.
buteoides	buteoides	k1gMnSc1	buteoides
(	(	kIx(	(
<g/>
Menzbier	Menzbier	k1gMnSc1	Menzbier
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
–	–	k?	–
severní	severní	k2eAgFnSc1d1	severní
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc1	Asie
od	od	k7c2	od
Švédska	Švédsko	k1gNnSc2	Švédsko
východně	východně	k6eAd1	východně
po	po	k7c4	po
řeku	řeka	k1gFnSc4	řeka
Lenu	Lena	k1gFnSc4	Lena
</s>
</p>
<p>
<s>
A.	A.	kA	A.
g.	g.	k?	g.
albidus	albidus	k1gMnSc1	albidus
(	(	kIx(	(
<g/>
Menzbier	Menzbier	k1gMnSc1	Menzbier
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
–	–	k?	–
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
Sibiř	Sibiř	k1gFnSc1	Sibiř
</s>
</p>
<p>
<s>
A.	A.	kA	A.
g.	g.	k?	g.
schvedowi	schvedow	k1gFnSc2	schvedow
(	(	kIx(	(
<g/>
Menzbier	Menzbier	k1gMnSc1	Menzbier
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
–	–	k?	–
Asie	Asie	k1gFnSc2	Asie
od	od	k7c2	od
Uralu	Ural	k1gInSc2	Ural
východně	východně	k6eAd1	východně
po	po	k7c4	po
Sachalin	Sachalin	k1gInSc4	Sachalin
a	a	k8xC	a
Kurily	Kurily	k1gFnPc4	Kurily
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
po	po	k7c4	po
střední	střední	k2eAgFnSc4d1	střední
Čínu	Čína	k1gFnSc4	Čína
</s>
</p>
<p>
<s>
A.	A.	kA	A.
g.	g.	k?	g.
fujiyamae	fujiyamae	k1gInSc1	fujiyamae
(	(	kIx(	(
<g/>
Swann	Swann	k1gInSc1	Swann
&	&	k?	&
Hartert	Hartert	k1gInSc1	Hartert
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
–	–	k?	–
Japonsko	Japonsko	k1gNnSc1	Japonsko
</s>
</p>
<p>
<s>
A.	A.	kA	A.
g.	g.	k?	g.
atricapillus	atricapillus	k1gMnSc1	atricapillus
(	(	kIx(	(
<g/>
Wilson	Wilson	k1gMnSc1	Wilson
<g/>
,	,	kIx,	,
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
A.	A.	kA	A.
g.	g.	k?	g.
laingi	laing	k1gFnSc2	laing
(	(	kIx(	(
<g/>
Taverner	Taverner	k1gMnSc1	Taverner
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
–	–	k?	–
Haida	Haida	k1gFnSc1	Haida
Gwaii	Gwaie	k1gFnSc4	Gwaie
<g/>
,	,	kIx,	,
Vancouver	Vancouver	k1gInSc4	Vancouver
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jestřáb	jestřáb	k1gMnSc1	jestřáb
lesní	lesní	k2eAgMnSc1d1	lesní
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
druh	druh	k1gInSc1	druh
rodu	rod	k1gInSc2	rod
Accipiter	Accipitra	k1gFnPc2	Accipitra
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
velikosti	velikost	k1gFnSc3	velikost
káně	káně	k1gFnSc2	káně
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
58	[number]	k4	58
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
108	[number]	k4	108
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
menší	malý	k2eAgMnSc1d2	menší
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
49	[number]	k4	49
<g/>
–	–	k?	–
<g/>
56	[number]	k4	56
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
105	[number]	k4	105
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
samce	samec	k1gInSc2	samec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
517	[number]	k4	517
<g/>
–	–	k?	–
<g/>
1170	[number]	k4	1170
g	g	kA	g
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
820	[number]	k4	820
<g/>
–	–	k?	–
<g/>
1509	[number]	k4	1509
g.	g.	k?	g.
Má	mít	k5eAaImIp3nS	mít
krátká	krátký	k2eAgNnPc4d1	krátké
zakulacená	zakulacený	k2eAgNnPc4d1	zakulacené
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc4d1	umožňující
rychlé	rychlý	k2eAgNnSc4d1	rychlé
manévrování	manévrování	k1gNnSc4	manévrování
a	a	k8xC	a
prudké	prudký	k2eAgFnSc2d1	prudká
otočky	otočka	k1gFnSc2	otočka
v	v	k7c6	v
hustém	hustý	k2eAgInSc6d1	hustý
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Svrchu	svrchu	k6eAd1	svrchu
je	být	k5eAaImIp3nS	být
modrošedý	modrošedý	k2eAgInSc1d1	modrošedý
(	(	kIx(	(
<g/>
samec	samec	k1gInSc1	samec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
šedohnědý	šedohnědý	k2eAgMnSc1d1	šedohnědý
(	(	kIx(	(
<g/>
samice	samice	k1gFnPc1	samice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zespodu	zespodu	k6eAd1	zespodu
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
černě	černě	k6eAd1	černě
proužkovaný	proužkovaný	k2eAgInSc1d1	proužkovaný
<g/>
.	.	kIx.	.
</s>
<s>
Temeno	temeno	k1gNnSc1	temeno
a	a	k8xC	a
tváře	tvář	k1gFnPc4	tvář
tmavé	tmavý	k2eAgFnPc4d1	tmavá
<g/>
,	,	kIx,	,
kontrastující	kontrastující	k2eAgFnPc4d1	kontrastující
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
bílým	bílý	k2eAgInSc7d1	bílý
nadočním	nadoční	k2eAgInSc7d1	nadoční
proužkem	proužek	k1gInSc7	proužek
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
pták	pták	k1gMnSc1	pták
je	být	k5eAaImIp3nS	být
svrchu	svrchu	k6eAd1	svrchu
hnědý	hnědý	k2eAgInSc1d1	hnědý
a	a	k8xC	a
zespodu	zespodu	k6eAd1	zespodu
béžově	béžově	k6eAd1	béžově
bílý	bílý	k2eAgInSc1d1	bílý
s	s	k7c7	s
řídkým	řídký	k2eAgNnSc7d1	řídké
hnědým	hnědý	k2eAgNnSc7d1	hnědé
čárkováním	čárkování	k1gNnSc7	čárkování
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
duhovka	duhovka	k1gFnSc1	duhovka
dospělého	dospělý	k2eAgMnSc2d1	dospělý
oranžová	oranžový	k2eAgFnSc1d1	oranžová
až	až	k6eAd1	až
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
mladého	mladý	k2eAgMnSc4d1	mladý
bledě	bledě	k6eAd1	bledě
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zaměněn	zaměnit	k5eAaPmNgInS	zaměnit
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrazně	výrazně	k6eAd1	výrazně
menším	malý	k2eAgMnSc7d2	menší
krahujcem	krahujec	k1gMnSc7	krahujec
obecným	obecný	k2eAgMnSc7d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letu	let	k1gInSc6	let
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
o	o	k7c4	o
něco	něco	k3yInSc4	něco
delšími	dlouhý	k2eAgNnPc7d2	delší
a	a	k8xC	a
špičatějšími	špičatý	k2eAgNnPc7d2	špičatější
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
oblejšími	oblý	k2eAgInPc7d2	oblejší
cípy	cíp	k1gInPc7	cíp
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
širším	široký	k2eAgInSc7d2	širší
kořenem	kořen	k1gInSc7	kořen
ocasu	ocas	k1gInSc2	ocas
a	a	k8xC	a
více	hodně	k6eAd2	hodně
vyčnívající	vyčnívající	k2eAgFnSc7d1	vyčnívající
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
vsedě	vsedě	k6eAd1	vsedě
jsou	být	k5eAaImIp3nP	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
rozpoznávacím	rozpoznávací	k2eAgInSc7d1	rozpoznávací
znakem	znak	k1gInSc7	znak
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
jestřába	jestřáb	k1gMnSc2	jestřáb
lesního	lesní	k2eAgMnSc2d1	lesní
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
<g/>
x	x	k?	x
silnější	silný	k2eAgInPc1d2	silnější
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k7c4	mimo
hnízdní	hnízdní	k2eAgNnSc4d1	hnízdní
období	období	k1gNnSc4	období
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
neozývá	ozývat	k5eNaImIp3nS	ozývat
<g/>
,	,	kIx,	,
na	na	k7c6	na
hnízdišti	hnízdiště	k1gNnSc6	hnízdiště
vydává	vydávat	k5eAaPmIp3nS	vydávat
pronikavé	pronikavý	k2eAgNnSc1d1	pronikavé
"	"	kIx"	"
<g/>
kekekekekekekekeke	kekekekekekekekeke	k1gNnSc1	kekekekekekekekeke
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Jestřáb	jestřáb	k1gMnSc1	jestřáb
lesní	lesní	k2eAgMnSc1d1	lesní
má	mít	k5eAaImIp3nS	mít
holarktický	holarktický	k2eAgInSc1d1	holarktický
typ	typ	k1gInSc1	typ
rozšíření	rozšíření	k1gNnSc2	rozšíření
v	v	k7c6	v
lesním	lesní	k2eAgNnSc6d1	lesní
pásmu	pásmo	k1gNnSc6	pásmo
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
stálý	stálý	k2eAgInSc4d1	stálý
nebo	nebo	k8xC	nebo
potulný	potulný	k2eAgInSc4d1	potulný
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
nejsevernější	severní	k2eAgFnPc1d3	nejsevernější
populace	populace	k1gFnPc1	populace
jsou	být	k5eAaImIp3nP	být
zčásti	zčásti	k6eAd1	zčásti
tažné	tažný	k2eAgFnPc1d1	tažná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
poklesu	pokles	k1gInSc3	pokles
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
zapříčiněnému	zapříčiněný	k2eAgMnSc3d1	zapříčiněný
hlavně	hlavně	k9	hlavně
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
pronásledováním	pronásledování	k1gNnSc7	pronásledování
a	a	k8xC	a
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
také	také	k9	také
používáním	používání	k1gNnSc7	používání
pesticidů	pesticid	k1gInPc2	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
evropská	evropský	k2eAgFnSc1d1	Evropská
populace	populace	k1gFnSc1	populace
mírně	mírně	k6eAd1	mírně
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
hnízdilo	hnízdit	k5eAaImAgNnS	hnízdit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
160	[number]	k4	160
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
skrytě	skrytě	k6eAd1	skrytě
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
vzrostlých	vzrostlý	k2eAgInPc6d1	vzrostlý
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
porostech	porost	k1gInPc6	porost
<g/>
,	,	kIx,	,
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
občas	občas	k6eAd1	občas
zaletuje	zaletovat	k5eAaPmIp3nS	zaletovat
i	i	k9	i
do	do	k7c2	do
otevřené	otevřený	k2eAgFnSc2d1	otevřená
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
v	v	k7c6	v
lesnaté	lesnatý	k2eAgFnSc6d1	lesnatá
kulturní	kulturní	k2eAgFnSc6d1	kulturní
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nesouvislých	souvislý	k2eNgInPc6d1	nesouvislý
lesích	les	k1gInPc6	les
v	v	k7c6	v
podhůřích	podhůří	k1gNnPc6	podhůří
a	a	k8xC	a
v	v	k7c6	v
lužních	lužní	k2eAgInPc6d1	lužní
lesích	les	k1gInPc6	les
než	než	k8xS	než
uvnitř	uvnitř	k7c2	uvnitř
hlubokých	hluboký	k2eAgInPc2d1	hluboký
lesů	les	k1gInPc2	les
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
do	do	k7c2	do
1200	[number]	k4	1200
<g/>
–	–	k?	–
<g/>
1300	[number]	k4	1300
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
do	do	k7c2	do
1150	[number]	k4	1150
m.	m.	k?	m.
Celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
zákonnou	zákonný	k2eAgFnSc4d1	zákonná
ochranu	ochrana	k1gFnSc4	ochrana
nadále	nadále	k6eAd1	nadále
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
89	[number]	k4	89
v	v	k7c6	v
ČR	ČR	kA	ČR
hnízdilo	hnízdit	k5eAaImAgNnS	hnízdit
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2800	[number]	k4	2800
párů	pár	k1gInPc2	pár
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
1800	[number]	k4	1800
<g/>
–	–	k?	–
<g/>
2500	[number]	k4	2500
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
požírá	požírat	k5eAaImIp3nS	požírat
i	i	k9	i
plazy	plaz	k1gMnPc4	plaz
<g/>
,	,	kIx,	,
bezobratlé	bezobratlí	k1gMnPc4	bezobratlí
a	a	k8xC	a
mršiny	mršina	k1gFnPc4	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
dle	dle	k7c2	dle
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
a	a	k8xC	a
místní	místní	k2eAgFnSc2d1	místní
nabídky	nabídka	k1gFnSc2	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
loví	lovit	k5eAaImIp3nP	lovit
hlavně	hlavně	k9	hlavně
lesní	lesní	k2eAgInPc1d1	lesní
druhy	druh	k1gInPc1	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
postupně	postupně	k6eAd1	postupně
roste	růst	k5eAaImIp3nS	růst
podíl	podíl	k1gInSc1	podíl
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
drobných	drobný	k2eAgMnPc2d1	drobný
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
a	a	k8xC	a
přibývá	přibývat	k5eAaImIp3nS	přibývat
též	též	k9	též
ptáků	pták	k1gMnPc2	pták
otevřené	otevřený	k2eAgFnSc2d1	otevřená
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
hnízdech	hnízdo	k1gNnPc6	hnízdo
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
1539	[number]	k4	1539
kusů	kus	k1gInPc2	kus
kořisti	kořist	k1gFnSc2	kořist
94	[number]	k4	94
%	%	kIx~	%
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
sojka	sojka	k1gFnSc1	sojka
(	(	kIx(	(
<g/>
19	[number]	k4	19
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
holubovití	holubovitý	k2eAgMnPc1d1	holubovitý
(	(	kIx(	(
<g/>
18	[number]	k4	18
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drozdovití	drozdovitý	k2eAgMnPc1d1	drozdovitý
(	(	kIx(	(
<g/>
14	[number]	k4	14
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
datlovití	datlovitý	k2eAgMnPc1d1	datlovitý
(	(	kIx(	(
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hnízdech	hnízdo	k1gNnPc6	hnízdo
v	v	k7c6	v
ČR	ČR	kA	ČR
byli	být	k5eAaImAgMnP	být
zjištěni	zjištěn	k2eAgMnPc1d1	zjištěn
holubovití	holubovitý	k2eAgMnPc1d1	holubovitý
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
holub	holub	k1gMnSc1	holub
hřivnáč	hřivnáč	k1gMnSc1	hřivnáč
a	a	k8xC	a
holub	holub	k1gMnSc1	holub
domácí	domácí	k1gMnSc1	domácí
<g/>
;	;	kIx,	;
30,1	[number]	k4	30,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krkavcovití	krkavcovitý	k2eAgMnPc1d1	krkavcovitý
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
sojka	sojka	k1gFnSc1	sojka
<g/>
;	;	kIx,	;
16,9	[number]	k4	16,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drozdovití	drozdovitý	k2eAgMnPc1d1	drozdovitý
(	(	kIx(	(
<g/>
nejvíce	hodně	k6eAd3	hodně
drozd	drozd	k1gMnSc1	drozd
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
<g/>
;	;	kIx,	;
21,9	[number]	k4	21,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
pěvci	pěvec	k1gMnPc1	pěvec
(	(	kIx(	(
<g/>
5,9	[number]	k4	5,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dravci	dravec	k1gMnPc7	dravec
(	(	kIx(	(
<g/>
2,3	[number]	k4	2,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sovy	sova	k1gFnSc2	sova
(	(	kIx(	(
<g/>
3,7	[number]	k4	3,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veverka	veverka	k1gFnSc1	veverka
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
drobné	drobný	k2eAgFnSc2d1	drobná
lovné	lovný	k2eAgFnSc2d1	lovná
zvěře	zvěř	k1gFnSc2	zvěř
(	(	kIx(	(
<g/>
kachna	kachna	k1gFnSc1	kachna
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
,	,	kIx,	,
koroptev	koroptev	k1gFnSc1	koroptev
polní	polní	k2eAgFnSc1d1	polní
<g/>
,	,	kIx,	,
bažant	bažant	k1gMnSc1	bažant
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
,	,	kIx,	,
zajíc	zajíc	k1gMnSc1	zajíc
polní	polní	k2eAgMnSc1d1	polní
<g/>
)	)	kIx)	)
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
tvořili	tvořit	k5eAaImAgMnP	tvořit
potravu	potrava	k1gFnSc4	potrava
jestřábů	jestřáb	k1gMnPc2	jestřáb
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hnízdního	hnízdní	k2eAgNnSc2d1	hnízdní
období	období	k1gNnSc2	období
z	z	k7c2	z
87	[number]	k4	87
%	%	kIx~	%
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
ze	z	k7c2	z
13	[number]	k4	13
%	%	kIx~	%
savci	savec	k1gMnPc1	savec
<g/>
;	;	kIx,	;
z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
převažovali	převažovat	k5eAaImAgMnP	převažovat
holub	holub	k1gMnSc1	holub
domácí	domácí	k1gMnSc1	domácí
<g/>
,	,	kIx,	,
holub	holub	k1gMnSc1	holub
hřivnáč	hřivnáč	k1gMnSc1	hřivnáč
<g/>
,	,	kIx,	,
vrána	vrána	k1gFnSc1	vrána
<g/>
,	,	kIx,	,
havran	havran	k1gMnSc1	havran
<g/>
,	,	kIx,	,
straka	straka	k1gMnSc1	straka
<g/>
,	,	kIx,	,
sojka	sojka	k1gFnSc1	sojka
<g/>
,	,	kIx,	,
drozd	drozd	k1gMnSc1	drozd
brávník	brávník	k1gMnSc1	brávník
<g/>
,	,	kIx,	,
drozd	drozd	k1gMnSc1	drozd
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
a	a	k8xC	a
kos	kos	k1gMnSc1	kos
černý	černý	k1gMnSc1	černý
<g/>
,	,	kIx,	,
ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
veverka	veverka	k1gFnSc1	veverka
popelavá	popelavý	k2eAgFnSc1d1	popelavá
a	a	k8xC	a
králík	králík	k1gMnSc1	králík
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původnějších	původní	k2eAgFnPc6d2	původnější
podmínkách	podmínka	k1gFnPc6	podmínka
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
potravou	potrava	k1gFnSc7	potrava
lesní	lesní	k2eAgMnPc1d1	lesní
kurové	kur	k1gMnPc1	kur
<g/>
.	.	kIx.	.
<g/>
Denní	denní	k2eAgFnSc1d1	denní
spotřeba	spotřeba	k1gFnSc1	spotřeba
potravy	potrava	k1gFnSc2	potrava
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
14	[number]	k4	14
%	%	kIx~	%
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
volnějších	volný	k2eAgNnPc6d2	volnější
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
i	i	k9	i
na	na	k7c6	na
otevřených	otevřený	k2eAgNnPc6d1	otevřené
prostranstvích	prostranství	k1gNnPc6	prostranství
mimo	mimo	k6eAd1	mimo
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
vyhlíží	vyhlížet	k5eAaImIp3nS	vyhlížet
za	za	k7c2	za
letu	let	k1gInSc2	let
nevysoko	vysoko	k6eNd1	vysoko
nad	nad	k7c7	nad
porostem	porost	k1gInSc7	porost
nebo	nebo	k8xC	nebo
vsedě	vsedě	k6eAd1	vsedě
z	z	k7c2	z
pozorovatelny	pozorovatelna	k1gFnSc2	pozorovatelna
a	a	k8xC	a
chytá	chytat	k5eAaImIp3nS	chytat
ji	on	k3xPp3gFnSc4	on
po	po	k7c6	po
prudkém	prudký	k2eAgInSc6d1	prudký
výpadu	výpad	k1gInSc6	výpad
do	do	k7c2	do
napřažených	napřažený	k2eAgInPc2d1	napřažený
pařátů	pařát	k1gInPc2	pařát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jednotlivě	jednotlivě	k6eAd1	jednotlivě
a	a	k8xC	a
teritoriálně	teritoriálně	k6eAd1	teritoriálně
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
stálé	stálý	k2eAgMnPc4d1	stálý
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
udržují	udržovat	k5eAaImIp3nP	udržovat
i	i	k9	i
stejný	stejný	k2eAgInSc4d1	stejný
domovský	domovský	k2eAgInSc4d1	domovský
okrsek	okrsek	k1gInSc4	okrsek
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
koncem	koncem	k7c2	koncem
zimy	zima	k1gFnSc2	zima
nebo	nebo	k8xC	nebo
již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
předvádí	předvádět	k5eAaImIp3nS	předvádět
nad	nad	k7c7	nad
hnízdištěm	hnízdiště	k1gNnSc7	hnízdiště
svatební	svatební	k2eAgFnSc2d1	svatební
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yIgFnPc2	který
několikrát	několikrát	k6eAd1	několikrát
mocně	mocně	k6eAd1	mocně
udeří	udeřit	k5eAaPmIp3nS	udeřit
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
následně	následně	k6eAd1	následně
kolmo	kolmo	k6eAd1	kolmo
vzletí	vzletět	k5eAaPmIp3nS	vzletět
s	s	k7c7	s
roztaženými	roztažený	k2eAgNnPc7d1	roztažené
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vysokých	vysoký	k2eAgInPc6d1	vysoký
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
u	u	k7c2	u
kmene	kmen	k1gInSc2	kmen
i	i	k9	i
v	v	k7c6	v
rozsoše	rozsocha	k1gFnSc6	rozsocha
silnějších	silný	k2eAgFnPc2d2	silnější
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
odlehlejších	odlehlý	k2eAgFnPc6d2	odlehlejší
částech	část	k1gFnPc6	část
starých	starý	k2eAgInPc2d1	starý
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
zcela	zcela	k6eAd1	zcela
nekryto	kryt	k2eNgNnSc1d1	kryt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
bylo	být	k5eAaImAgNnS	být
nejčastěji	často	k6eAd3	často
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
na	na	k7c6	na
jehličnanech	jehličnan	k1gInPc6	jehličnan
(	(	kIx(	(
<g/>
smrku	smrk	k1gInSc6	smrk
<g/>
,	,	kIx,	,
jedli	jedle	k1gFnSc6	jedle
<g/>
,	,	kIx,	,
borovici	borovice	k1gFnSc6	borovice
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
m.	m.	k?	m.
V	v	k7c6	v
revíru	revír	k1gInSc6	revír
obvykle	obvykle	k6eAd1	obvykle
buduje	budovat	k5eAaImIp3nS	budovat
několik	několik	k4yIc1	několik
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nepravidelně	pravidelně	k6eNd1	pravidelně
střídá	střídat	k5eAaImIp3nS	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Stavebním	stavební	k2eAgInSc7d1	stavební
materiálem	materiál	k1gInSc7	materiál
jsou	být	k5eAaImIp3nP	být
větve	větev	k1gFnPc1	větev
<g/>
,	,	kIx,	,
kotlinka	kotlinka	k1gFnSc1	kotlinka
je	být	k5eAaImIp3nS	být
vystlána	vystlat	k5eAaPmNgFnS	vystlat
trávou	tráva	k1gFnSc7	tráva
<g/>
,	,	kIx,	,
stonky	stonek	k1gInPc4	stonek
nebo	nebo	k8xC	nebo
kousky	kousek	k1gInPc4	kousek
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
staví	stavit	k5eAaImIp3nP	stavit
oba	dva	k4xCgMnPc1	dva
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
1	[number]	k4	1
<g/>
x	x	k?	x
ročně	ročně	k6eAd1	ročně
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Snůška	snůška	k1gFnSc1	snůška
čítá	čítat	k5eAaImIp3nS	čítat
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
světle	světlo	k1gNnSc6	světlo
nazelenale	nazelenale	k6eAd1	nazelenale
modrých	modrý	k2eAgFnPc2d1	modrá
nebo	nebo	k8xC	nebo
světle	světlo	k1gNnSc6	světlo
šedých	šedý	k2eAgNnPc2d1	šedé
vajec	vejce	k1gNnPc2	vejce
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
57,2	[number]	k4	57,2
×	×	k?	×
45,1	[number]	k4	45,1
mm	mm	kA	mm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
kolem	kolem	k7c2	kolem
51	[number]	k4	51
g.	g.	k?	g.
Snášena	snášen	k2eAgNnPc1d1	snášeno
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
dvoudenních	dvoudenní	k2eAgInPc6d1	dvoudenní
intervalech	interval	k1gInPc6	interval
a	a	k8xC	a
sezení	sezení	k1gNnSc1	sezení
začíná	začínat	k5eAaImIp3nS	začínat
nejspíš	nejspíš	k9	nejspíš
od	od	k7c2	od
snesení	snesení	k1gNnSc2	snesení
prvního	první	k4xOgNnSc2	první
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nP	sedit
převážně	převážně	k6eAd1	převážně
samice	samice	k1gFnPc1	samice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
samec	samec	k1gMnSc1	samec
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
x	x	k?	x
denně	denně	k6eAd1	denně
přináší	přinášet	k5eAaImIp3nS	přinášet
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
střídá	střídat	k5eAaImIp3nS	střídat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
proletět	proletět	k5eAaPmF	proletět
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
hnízdo	hnízdo	k1gNnSc4	hnízdo
si	se	k3xPyFc3	se
prudce	prudko	k6eAd1	prudko
brání	bránit	k5eAaImIp3nP	bránit
a	a	k8xC	a
útočí	útočit	k5eAaImIp3nP	útočit
přitom	přitom	k6eAd1	přitom
i	i	k9	i
na	na	k7c4	na
větší	veliký	k2eAgInPc4d2	veliký
druhy	druh	k1gInPc4	druh
dravců	dravec	k1gMnPc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
prvních	první	k4xOgInPc2	první
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
dnů	den	k1gInPc2	den
zahřívána	zahříván	k2eAgFnSc1d1	zahřívána
samicí	samice	k1gFnSc7	samice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
trhá	trhat	k5eAaImIp3nS	trhat
potravu	potrava	k1gFnSc4	potrava
přinášenou	přinášený	k2eAgFnSc4d1	přinášená
samcem	samec	k1gInSc7	samec
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
loví	lovit	k5eAaImIp3nP	lovit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc1	mládě
krmena	krmen	k2eAgNnPc1d1	krmeno
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
x.	x.	k?	x.
Hnízdo	hnízdo	k1gNnSc4	hnízdo
opouštějí	opouštět	k5eAaImIp3nP	opouštět
po	po	k7c6	po
36	[number]	k4	36
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
43	[number]	k4	43
dnech	den	k1gInPc6	den
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vzletnosti	vzletnost	k1gFnPc1	vzletnost
a	a	k8xC	a
krmena	krmen	k2eAgNnPc1d1	krmeno
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
do	do	k7c2	do
stáří	stáří	k1gNnSc2	stáří
70	[number]	k4	70
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
z	z	k7c2	z
hnízdního	hnízdní	k2eAgInSc2d1	hnízdní
revíru	revír	k1gInSc2	revír
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
zpravidla	zpravidla	k6eAd1	zpravidla
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1	[number]	k4	1
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdní	hnízdní	k2eAgFnSc1d1	hnízdní
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
je	být	k5eAaImIp3nS	být
73	[number]	k4	73
%	%	kIx~	%
<g/>
,	,	kIx,	,
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
ztrát	ztráta	k1gFnPc2	ztráta
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
přímým	přímý	k2eAgNnSc7d1	přímé
pronásledováním	pronásledování	k1gNnSc7	pronásledování
(	(	kIx(	(
<g/>
vybírání	vybírání	k1gNnSc1	vybírání
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
,	,	kIx,	,
odstřel	odstřel	k1gInSc1	odstřel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
80	[number]	k4	80
%	%	kIx~	%
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
známý	známý	k2eAgInSc1d1	známý
věk	věk	k1gInSc1	věk
okroužkovaného	okroužkovaný	k2eAgMnSc2d1	okroužkovaný
ptáka	pták	k1gMnSc2	pták
je	být	k5eAaImIp3nS	být
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Jestřáb	jestřáb	k1gMnSc1	jestřáb
lesní	lesní	k2eAgMnSc1d1	lesní
je	být	k5eAaImIp3nS	být
myslivci	myslivec	k1gMnPc1	myslivec
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
škodnou	škodný	k2eAgFnSc4d1	škodná
zvěř	zvěř	k1gFnSc4	zvěř
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
proto	proto	k8xC	proto
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
intenzivně	intenzivně	k6eAd1	intenzivně
pronásledován	pronásledován	k2eAgMnSc1d1	pronásledován
a	a	k8xC	a
odchytáván	odchytáván	k2eAgMnSc1d1	odchytáván
do	do	k7c2	do
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
pastí	past	k1gFnPc2	past
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
jestřábích	jestřábí	k2eAgInPc2d1	jestřábí
košů	koš	k1gInPc2	koš
<g/>
.	.	kIx.	.
</s>
<s>
Negativní	negativní	k2eAgInSc1d1	negativní
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
populace	populace	k1gFnPc4	populace
drobné	drobný	k2eAgFnSc2d1	drobná
zvěře	zvěř	k1gFnSc2	zvěř
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
vědecky	vědecky	k6eAd1	vědecky
prokázán	prokázat	k5eAaPmNgInS	prokázat
<g/>
,	,	kIx,	,
určité	určitý	k2eAgFnPc4d1	určitá
škody	škoda	k1gFnPc4	škoda
může	moct	k5eAaImIp3nS	moct
místy	místy	k6eAd1	místy
působit	působit	k5eAaImF	působit
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
drůbeži	drůbež	k1gFnSc6	drůbež
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
předcházet	předcházet	k5eAaImF	předcházet
preventivní	preventivní	k2eAgFnSc7d1	preventivní
ochranou	ochrana	k1gFnSc7	ochrana
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
druhy	druh	k1gInPc4	druh
často	často	k6eAd1	často
používané	používaný	k2eAgInPc4d1	používaný
v	v	k7c6	v
sokolnictví	sokolnictví	k1gNnSc6	sokolnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
zvláště	zvláště	k6eAd1	zvláště
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jestřáb	jestřáb	k1gMnSc1	jestřáb
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
jestřáb	jestřáb	k1gMnSc1	jestřáb
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Accipiter	Accipiter	k1gInSc1	Accipiter
gentilis	gentilis	k1gInSc1	gentilis
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Biolib	Biolib	k1gMnSc1	Biolib
</s>
</p>
<p>
<s>
Příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jestřáb	jestřáb	k1gMnSc1	jestřáb
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
<p>
<s>
Webkamera	Webkamera	k1gFnSc1	Webkamera
JEstřába	jestřáb	k1gMnSc2	jestřáb
lesního	lesní	k2eAgMnSc2d1	lesní
<g/>
:	:	kIx,	:
https://web.archive.org/web/20160516025827/http://www.zoocam.info/jestrab-lesni-webkamera/	[url]	k4	https://web.archive.org/web/20160516025827/http://www.zoocam.info/jestrab-lesni-webkamera/
</s>
</p>
