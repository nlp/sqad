<p>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
ženy	žena	k1gFnPc4	žena
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
viz	vidět	k5eAaImRp2nS	vidět
Marie	Maria	k1gFnSc2	Maria
Anna	Anna	k1gFnSc1	Anna
Španělská	španělský	k2eAgFnSc1d1	španělská
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Španělská	španělský	k2eAgFnSc1d1	španělská
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1606	[number]	k4	1606
<g/>
,	,	kIx,	,
u	u	k7c2	u
Madridu	Madrid	k1gInSc2	Madrid
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1646	[number]	k4	1646
<g/>
,	,	kIx,	,
Linz	Linz	k1gInSc1	Linz
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
španělského	španělský	k2eAgMnSc4d1	španělský
krále	král	k1gMnSc4	král
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Markéty	Markéta	k1gFnSc2	Markéta
a	a	k8xC	a
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
a	a	k8xC	a
císaře	císař	k1gMnSc2	císař
římského	římský	k2eAgMnSc2d1	římský
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
uherského	uherský	k2eAgMnSc2d1	uherský
a	a	k8xC	a
českého	český	k2eAgMnSc2d1	český
a	a	k8xC	a
markraběte	markrabě	k1gMnSc2	markrabě
moravského	moravský	k2eAgMnSc2d1	moravský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plánované	plánovaný	k2eAgInPc4d1	plánovaný
sňatky	sňatek	k1gInPc4	sňatek
==	==	k?	==
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
roku	rok	k1gInSc2	rok
1606	[number]	k4	1606
jako	jako	k8xC	jako
druhorozené	druhorozený	k2eAgNnSc4d1	druhorozené
dítě	dítě	k1gNnSc4	dítě
(	(	kIx(	(
<g/>
prvorozená	prvorozený	k2eAgFnSc1d1	prvorozená
byla	být	k5eAaImAgFnS	být
Anna	Anna	k1gFnSc1	Anna
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
<g/>
)	)	kIx)	)
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
byla	být	k5eAaImAgFnS	být
zaslíbena	zaslíben	k2eAgFnSc1d1	zaslíbena
arcivévodovi	arcivévoda	k1gMnSc3	arcivévoda
Janu	Jan	k1gMnSc3	Jan
Karlovi	Karel	k1gMnSc3	Karel
<g/>
,	,	kIx,	,
synovi	syn	k1gMnSc3	syn
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Jan	Jan	k1gMnSc1	Jan
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1619	[number]	k4	1619
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
pojmout	pojmout	k5eAaPmF	pojmout
za	za	k7c4	za
chotě	choť	k1gMnSc4	choť
císařova	císařův	k2eAgMnSc4d1	císařův
mladšího	mladý	k2eAgMnSc4d2	mladší
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
plánovaní	plánovaný	k2eAgMnPc1d1	plánovaný
manželé	manžel	k1gMnPc1	manžel
byli	být	k5eAaImAgMnP	být
bratranec	bratranec	k1gMnSc1	bratranec
a	a	k8xC	a
sestřenice	sestřenice	k1gFnSc1	sestřenice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
habsburském	habsburský	k2eAgInSc6d1	habsburský
rodě	rod	k1gInSc6	rod
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
nebylo	být	k5eNaImAgNnS	být
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
nic	nic	k3yNnSc1	nic
neobvyklého	obvyklý	k2eNgNnSc2d1	neobvyklé
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
sňatek	sňatek	k1gInSc1	sňatek
byl	být	k5eAaImAgInS	být
odložen	odložit	k5eAaPmNgInS	odložit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
o	o	k7c4	o
Marii	Maria	k1gFnSc4	Maria
Annu	Anna	k1gFnSc4	Anna
začal	začít	k5eAaPmAgMnS	začít
ucházet	ucházet	k5eAaImF	ucházet
anglický	anglický	k2eAgMnSc1d1	anglický
a	a	k8xC	a
skotský	skotský	k2eAgMnSc1d1	skotský
korunní	korunní	k2eAgMnSc1d1	korunní
princ	princ	k1gMnSc1	princ
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
dokonce	dokonce	k9	dokonce
přijel	přijet	k5eAaPmAgMnS	přijet
za	za	k7c7	za
uvažovanou	uvažovaný	k2eAgFnSc7d1	uvažovaná
nevěstou	nevěsta	k1gFnSc7	nevěsta
do	do	k7c2	do
Španěl	Španěly	k1gInPc2	Španěly
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
píseň	píseň	k1gFnSc1	píseň
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
líčí	líčit	k5eAaImIp3nS	líčit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Carlos	Carlos	k1gMnSc1	Carlos
Estuardo	Estuardo	k1gNnSc1	Estuardo
soy	soy	k?	soy
<g/>
,	,	kIx,	,
Que	Que	k1gMnSc1	Que
siendo	siendo	k6eAd1	siendo
amor	amor	k1gMnSc1	amor
mi	já	k3xPp1nSc3	já
guia	guia	k6eAd1	guia
<g/>
,	,	kIx,	,
A	a	k8xC	a
cielo	cielo	k1gNnSc1	cielo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Espana	Espana	k1gFnSc1	Espana
voy	voy	k?	voy
<g/>
,	,	kIx,	,
Per	pero	k1gNnPc2	pero
ver	ver	k?	ver
estrella	estrell	k1gMnSc2	estrell
Maria	Mario	k1gMnSc2	Mario
(	(	kIx(	(
<g/>
Jsem	být	k5eAaImIp1nS	být
Karel	Karel	k1gMnSc1	Karel
Stuart	Stuarta	k1gFnPc2	Stuarta
<g/>
,	,	kIx,	,
láskou	láska	k1gFnSc7	láska
přiveden	přiveden	k2eAgInSc1d1	přiveden
zdaleka	zdaleka	k6eAd1	zdaleka
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
španělské	španělský	k2eAgNnSc4d1	španělské
nebe	nebe	k1gNnSc4	nebe
přišel	přijít	k5eAaPmAgInS	přijít
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
spatřil	spatřit	k5eAaPmAgMnS	spatřit
Marii	Maria	k1gFnSc4	Maria
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
uskutečněn	uskutečněn	k2eAgInSc1d1	uskutečněn
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ženichův	ženichův	k2eAgMnSc1d1	ženichův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
Stuart	Stuart	k1gInSc1	Stuart
<g/>
,	,	kIx,	,
požadoval	požadovat	k5eAaImAgInS	požadovat
za	za	k7c4	za
nevěstino	nevěstin	k2eAgNnSc4d1	nevěstino
věno	věno	k1gNnSc4	věno
území	území	k1gNnSc2	území
Falce	Falc	k1gFnSc2	Falc
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
obsazené	obsazený	k2eAgNnSc1d1	obsazené
španělským	španělský	k2eAgNnSc7d1	španělské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
Falc	Falc	k1gFnSc4	Falc
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jeho	jeho	k3xOp3gMnSc3	jeho
zeti	zeť	k1gMnSc3	zeť
<g/>
,	,	kIx,	,
českému	český	k2eAgMnSc3d1	český
"	"	kIx"	"
<g/>
zimnímu	zimní	k2eAgMnSc3d1	zimní
králi	král	k1gMnSc3	král
<g/>
"	"	kIx"	"
Fridrichu	Fridrich	k1gMnSc3	Fridrich
Falckému	falcký	k2eAgMnSc3d1	falcký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
1626	[number]	k4	1626
byl	být	k5eAaImAgInS	být
kontakt	kontakt	k1gInSc4	kontakt
o	o	k7c6	o
svatbě	svatba	k1gFnSc6	svatba
mezi	mezi	k7c7	mezi
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
Madridem	Madrid	k1gInSc7	Madrid
přerušen	přerušit	k5eAaPmNgInS	přerušit
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c2	za
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Zpovědník	zpovědník	k1gMnSc1	zpovědník
budoucí	budoucí	k2eAgFnSc2d1	budoucí
císařovny	císařovna	k1gFnSc2	císařovna
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
však	však	k9	však
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
trvaly	trvat	k5eAaImAgInP	trvat
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
vídeňským	vídeňský	k2eAgInSc7d1	vídeňský
královským	královský	k2eAgInSc7d1	královský
dvorem	dvůr	k1gInSc7	dvůr
a	a	k8xC	a
Madridem	Madrid	k1gInSc7	Madrid
o	o	k7c6	o
výši	výše	k1gFnSc6	výše
věna	věno	k1gNnSc2	věno
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1629	[number]	k4	1629
vyrazil	vyrazit	k5eAaPmAgInS	vyrazit
nevěstin	nevěstin	k2eAgInSc1d1	nevěstin
svatební	svatební	k2eAgInSc1d1	svatební
průvod	průvod	k1gInSc1	průvod
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
doznívajícím	doznívající	k2eAgFnPc3d1	doznívající
válečných	válečný	k2eAgNnPc2d1	válečné
bojům	boj	k1gInPc3	boj
dorazil	dorazit	k5eAaPmAgMnS	dorazit
až	až	k9	až
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1631	[number]	k4	1631
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prý	prý	k9	prý
šťastné	šťastný	k2eAgNnSc1d1	šťastné
patnáctileté	patnáctiletý	k2eAgNnSc1d1	patnáctileté
manželství	manželství	k1gNnSc1	manželství
ženicha	ženich	k1gMnSc2	ženich
a	a	k8xC	a
nevěsty	nevěsta	k1gFnSc2	nevěsta
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1631	[number]	k4	1631
svatbou	svatba	k1gFnSc7	svatba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Královna	královna	k1gFnSc1	královna
a	a	k8xC	a
císařovna	císařovna	k1gFnSc1	císařovna
==	==	k?	==
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
španělským	španělský	k2eAgInSc7d1	španělský
doprovodem	doprovod	k1gInSc7	doprovod
zřídila	zřídit	k5eAaPmAgFnS	zřídit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
rovnat	rovnat	k5eAaImF	rovnat
dvoru	dvůr	k1gInSc3	dvůr
císařovny	císařovna	k1gFnSc2	císařovna
Eleonory	Eleonora	k1gFnSc2	Eleonora
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc2	její
tchyně	tchyně	k1gFnSc2	tchyně
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
velmi	velmi	k6eAd1	velmi
zdatně	zdatně	k6eAd1	zdatně
plnila	plnit	k5eAaImAgFnS	plnit
reprezentativní	reprezentativní	k2eAgFnPc4d1	reprezentativní
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
nebyl	být	k5eNaImAgMnS	být
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
přítomen	přítomen	k2eAgMnSc1d1	přítomen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vedla	vést	k5eAaImAgNnP	vést
úspěšná	úspěšný	k2eAgNnPc1d1	úspěšné
jednání	jednání	k1gNnPc1	jednání
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
německým	německý	k2eAgMnSc7d1	německý
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
za	za	k7c2	za
celé	celá	k1gFnSc2	celá
manželství	manželství	k1gNnSc2	manželství
porodila	porodit	k5eAaPmAgFnS	porodit
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
zřejmě	zřejmě	k6eAd1	zřejmě
vedl	vést	k5eAaImAgMnS	vést
spořádaný	spořádaný	k2eAgInSc4d1	spořádaný
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
milenky	milenka	k1gFnSc2	milenka
nebo	nebo	k8xC	nebo
levobočky	levobočka	k1gFnSc2	levobočka
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
nechal	nechat	k5eAaPmAgMnS	nechat
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
korunovat	korunovat	k5eAaBmF	korunovat
českým	český	k2eAgMnPc3d1	český
<g/>
,	,	kIx,	,
uherským	uherský	k2eAgMnPc3d1	uherský
a	a	k8xC	a
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
byla	být	k5eAaImAgFnS	být
tragickou	tragický	k2eAgFnSc7d1	tragická
obětí	oběť	k1gFnSc7	oběť
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
byl	být	k5eAaImAgInS	být
nucen	nucen	k2eAgInSc1d1	nucen
celý	celý	k2eAgInSc1d1	celý
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
dvůr	dvůr	k1gInSc1	dvůr
utéct	utéct	k5eAaPmF	utéct
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
před	před	k7c7	před
Švédy	Švéd	k1gMnPc7	Švéd
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k8xC	jako
jiným	jiné	k1gNnSc7	jiné
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gInSc7	její
azylem	azyl	k1gInSc7	azyl
nejprve	nejprve	k6eAd1	nejprve
Štýrský	štýrský	k2eAgInSc4d1	štýrský
Hradec	Hradec	k1gInSc4	Hradec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Linz	Linza	k1gFnPc2	Linza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1646	[number]	k4	1646
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
následkům	následek	k1gInPc3	následek
otravy	otrava	k1gFnSc2	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
právě	právě	k6eAd1	právě
čekala	čekat	k5eAaImAgFnS	čekat
šesté	šestý	k4xOgNnSc4	šestý
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Marii	Maria	k1gFnSc4	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
císařským	císařský	k2eAgInSc7d1	císařský
řezem	řez	k1gInSc7	řez
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
byly	být	k5eAaImAgFnP	být
pohřbeny	pohřbít	k5eAaPmNgFnP	pohřbít
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
ještě	ještě	k9	ještě
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
stala	stát	k5eAaPmAgFnS	stát
Marie	Marie	k1gFnSc1	Marie
Leopoldina	Leopoldina	k1gFnSc1	Leopoldina
Tyrolská	tyrolský	k2eAgFnSc1d1	tyrolská
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
–	–	k?	–
<g/>
1649	[number]	k4	1649
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
sňatek	sňatek	k1gInSc4	sňatek
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1651	[number]	k4	1651
opět	opět	k6eAd1	opět
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
s	s	k7c7	s
Eleonorou	Eleonora	k1gFnSc7	Eleonora
Magdalenou	Magdalena	k1gFnSc7	Magdalena
de	de	k?	de
Gonzaga	Gonzaga	k1gFnSc1	Gonzaga
(	(	kIx(	(
<g/>
1630	[number]	k4	1630
<g/>
–	–	k?	–
<g/>
1686	[number]	k4	1686
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1633	[number]	k4	1633
<g/>
–	–	k?	–
<g/>
1654	[number]	k4	1654
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
1635	[number]	k4	1635
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1649	[number]	k4	1649
španělský	španělský	k2eAgMnSc1d1	španělský
a	a	k8xC	a
portugalský	portugalský	k2eAgMnSc1d1	portugalský
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
August	August	k1gMnSc1	August
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
<g/>
–	–	k?	–
<g/>
1639	[number]	k4	1639
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Tomáš	Tomáš	k1gMnSc1	Tomáš
(	(	kIx(	(
<g/>
1638	[number]	k4	1638
<g/>
–	–	k?	–
<g/>
1639	[number]	k4	1639
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1640	[number]	k4	1640
<g/>
–	–	k?	–
<g/>
1705	[number]	k4	1705
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
∞	∞	k?	∞
</s>
</p>
<p>
<s>
1666	[number]	k4	1666
infantka	infantka	k1gFnSc1	infantka
Markéta	Markéta	k1gFnSc1	Markéta
Tereza	Tereza	k1gFnSc1	Tereza
(	(	kIx(	(
<g/>
1651	[number]	k4	1651
<g/>
–	–	k?	–
<g/>
1673	[number]	k4	1673
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Leopoldovy	Leopoldův	k2eAgFnPc1d1	Leopoldova
sestry	sestra	k1gFnPc1	sestra
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
</s>
</p>
<p>
<s>
1673	[number]	k4	1673
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Klaudie	Klaudie	k1gFnSc1	Klaudie
Felicitas	Felicitas	k1gFnSc1	Felicitas
(	(	kIx(	(
<g/>
1653	[number]	k4	1653
<g/>
–	–	k?	–
<g/>
1676	[number]	k4	1676
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Karla	Karel	k1gMnSc2	Karel
Tyrolského	tyrolský	k2eAgMnSc2d1	tyrolský
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Anny	Anna	k1gFnSc2	Anna
Medicejské	Medicejský	k2eAgFnSc2d1	Medicejská
</s>
</p>
<p>
<s>
1676	[number]	k4	1676
princezna	princezna	k1gFnSc1	princezna
Eleonora	Eleonora	k1gFnSc1	Eleonora
Magdalena	Magdalena	k1gFnSc1	Magdalena
(	(	kIx(	(
<g/>
1655	[number]	k4	1655
<g/>
–	–	k?	–
<g/>
1720	[number]	k4	1720
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
falckého	falcký	k2eAgMnSc2d1	falcký
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Filipa	Filip	k1gMnSc2	Filip
Viléma	Vilém	k1gMnSc2	Vilém
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Alžběty	Alžběta	k1gFnSc2	Alžběta
Amálie	Amálie	k1gFnSc2	Amálie
Hesenské	hesenský	k2eAgFnSc2d1	Hesenská
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
†	†	k?	†
1646	[number]	k4	1646
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
FIDLER	FIDLER	kA	FIDLER
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
:	:	kIx,	:
České	český	k2eAgFnSc2d1	Česká
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7200	[number]	k4	7200
<g/>
-	-	kIx~	-
<g/>
849	[number]	k4	849
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
82	[number]	k4	82
<g/>
–	–	k?	–
<g/>
84	[number]	k4	84
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marie	Maria	k1gFnSc2	Maria
Anna	Anna	k1gFnSc1	Anna
Španělská	španělský	k2eAgFnSc1d1	španělská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
