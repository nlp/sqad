<s>
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Španělská	španělský	k2eAgFnSc1d1	španělská
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1606	[number]	k4	1606
<g/>
,	,	kIx,	,
u	u	k7c2	u
Madridu	Madrid	k1gInSc2	Madrid
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1646	[number]	k4	1646
<g/>
,	,	kIx,	,
Linz	Linz	k1gInSc1	Linz
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
španělského	španělský	k2eAgMnSc4d1	španělský
krále	král	k1gMnSc4	král
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Markéty	Markéta	k1gFnSc2	Markéta
a	a	k8xC	a
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
a	a	k8xC	a
císaře	císař	k1gMnSc2	císař
římského	římský	k2eAgMnSc2d1	římský
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
uherského	uherský	k2eAgMnSc2d1	uherský
a	a	k8xC	a
českého	český	k2eAgMnSc2d1	český
a	a	k8xC	a
markraběte	markrabě	k1gMnSc2	markrabě
moravského	moravský	k2eAgMnSc2d1	moravský
<g/>
.	.	kIx.	.
</s>
