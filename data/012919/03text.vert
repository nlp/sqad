<p>
<s>
Schönbrunn	Schönbrunn	k1gInSc1	Schönbrunn
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
barokního	barokní	k2eAgInSc2d1	barokní
zámku	zámek	k1gInSc2	zámek
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Schönbrunn	Schönbrunn	k1gInSc1	Schönbrunn
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
letní	letní	k2eAgFnSc7d1	letní
rezidencí	rezidence	k1gFnSc7	rezidence
rakouských	rakouský	k2eAgMnPc2d1	rakouský
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
bydlela	bydlet	k5eAaImAgFnS	bydlet
císařská	císařský	k2eAgFnSc1d1	císařská
rodina	rodina	k1gFnSc1	rodina
v	v	k7c6	v
Hofburgu	Hofburg	k1gInSc6	Hofburg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
zámku	zámek	k1gInSc2	zámek
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
zahradní	zahradní	k2eAgFnPc4d1	zahradní
prostory	prostora	k1gFnPc4	prostora
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zámkem	zámek	k1gInSc7	zámek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
zařazeny	zařadit	k5eAaPmNgInP	zařadit
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
vystaven	vystaven	k2eAgInSc1d1	vystaven
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
císař	císař	k1gMnSc1	císař
Matyáš	Matyáš	k1gMnSc1	Matyáš
<g/>
,	,	kIx,	,
tryskal	tryskat	k5eAaImAgMnS	tryskat
zde	zde	k6eAd1	zde
tehdy	tehdy	k6eAd1	tehdy
pramen	pramen	k1gInSc4	pramen
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgNnSc2	který
císař	císař	k1gMnSc1	císař
vztyčil	vztyčit	k5eAaPmAgMnS	vztyčit
sochu	socha	k1gFnSc4	socha
nymfy	nymfa	k1gFnSc2	nymfa
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
jej	on	k3xPp3gInSc4	on
Schöner	Schöner	k1gInSc4	Schöner
Brunnen	Brunnen	k2eAgInSc4d1	Brunnen
(	(	kIx(	(
<g/>
krásný	krásný	k2eAgInSc4d1	krásný
pramen	pramen	k1gInSc4	pramen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
zde	zde	k6eAd1	zde
vystavit	vystavit	k5eAaPmF	vystavit
i	i	k9	i
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámeček	zámeček	k1gInSc4	zámeček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
těžce	těžce	k6eAd1	těžce
poškozen	poškodit	k5eAaPmNgInS	poškodit
Turky	turek	k1gInPc7	turek
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
obléhání	obléhání	k1gNnSc6	obléhání
Vídně	Vídeň	k1gFnSc2	Vídeň
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgMnS	začít
Johann	Johann	k1gMnSc1	Johann
Bernhard	Bernhard	k1gMnSc1	Bernhard
Fischer	Fischer	k1gMnSc1	Fischer
von	von	k1gInSc4	von
Erlach	Erlach	k1gInSc4	Erlach
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
stavět	stavět	k5eAaImF	stavět
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
barokní	barokní	k2eAgFnSc4d1	barokní
rezidenci	rezidence	k1gFnSc4	rezidence
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
překonat	překonat	k5eAaPmF	překonat
Versailles	Versailles	k1gFnSc1	Versailles
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
měl	mít	k5eAaImAgMnS	mít
důležitější	důležitý	k2eAgInPc4d2	Důležitější
problémy	problém	k1gInPc4	problém
než	než	k8xS	než
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
Versailles	Versailles	k1gFnPc7	Versailles
a	a	k8xC	a
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
zámku	zámek	k1gInSc2	zámek
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
za	za	k7c4	za
Marie	Maria	k1gFnPc4	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
architekt	architekt	k1gMnSc1	architekt
Nicolo	Nicola	k1gFnSc5	Nicola
Pacassi	Pacasse	k1gFnSc3	Pacasse
plány	plán	k1gInPc4	plán
realizoval	realizovat	k5eAaBmAgMnS	realizovat
v	v	k7c6	v
redukovaném	redukovaný	k2eAgNnSc6d1	redukované
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
tu	ten	k3xDgFnSc4	ten
narodil	narodit	k5eAaPmAgMnS	narodit
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svá	svůj	k3xOyFgNnPc4	svůj
poslední	poslední	k2eAgNnPc4d1	poslední
léta	léto	k1gNnPc4	léto
trávil	trávit	k5eAaImAgMnS	trávit
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zámku	zámek	k1gInSc6	zámek
a	a	k8xC	a
také	také	k9	také
zde	zde	k6eAd1	zde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
poslední	poslední	k2eAgMnSc1d1	poslední
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
zámek	zámek	k1gInSc1	zámek
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
nové	nový	k2eAgFnSc2d1	nová
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nS	patřit
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
historickému	historický	k2eAgInSc3d1	historický
významu	význam	k1gInSc3	význam
<g/>
,	,	kIx,	,
poloze	poloha	k1gFnSc3	poloha
a	a	k8xC	a
velkolepému	velkolepý	k2eAgNnSc3d1	velkolepé
architektonickému	architektonický	k2eAgNnSc3d1	architektonické
vybavení	vybavení	k1gNnSc3	vybavení
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
pamětihodnosti	pamětihodnost	k1gFnPc4	pamětihodnost
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Wagenburg	Wagenburg	k1gInSc1	Wagenburg
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
kočárů	kočár	k1gInPc2	kočár
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
kočárů	kočár	k1gInPc2	kočár
Wagenburg	Wagenburg	k1gInSc4	Wagenburg
v	v	k7c6	v
Schönbrunnu	Schönbrunn	k1gInSc6	Schönbrunn
představuje	představovat	k5eAaImIp3nS	představovat
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
jádro	jádro	k1gNnSc4	jádro
vozového	vozový	k2eAgInSc2d1	vozový
parku	park	k1gInSc2	park
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
monarchie	monarchie	k1gFnSc2	monarchie
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
na	na	k7c4	na
stovku	stovka	k1gFnSc4	stovka
kočárů	kočár	k1gInPc2	kočár
<g/>
,	,	kIx,	,
saní	saně	k1gFnPc2	saně
<g/>
,	,	kIx,	,
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
nosítek	nosítka	k1gNnPc2	nosítka
s	s	k7c7	s
veškerými	veškerý	k3xTgInPc7	veškerý
postroji	postroj	k1gInPc7	postroj
<g/>
,	,	kIx,	,
řemením	řemení	k1gNnSc7	řemení
<g/>
,	,	kIx,	,
čabrakami	čabraka	k1gFnPc7	čabraka
a	a	k8xC	a
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zámek	zámek	k1gInSc1	zámek
Schönbrunn	Schönbrunn	k1gNnSc1	Schönbrunn
==	==	k?	==
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
má	mít	k5eAaImIp3nS	mít
1441	[number]	k4	1441
místností	místnost	k1gFnPc2	místnost
a	a	k8xC	a
200	[number]	k4	200
kuchyní	kuchyně	k1gFnPc2	kuchyně
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
zpřístupněno	zpřístupnit	k5eAaPmNgNnS	zpřístupnit
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
vybavení	vybavení	k1gNnSc1	vybavení
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
rokoka	rokoko	k1gNnSc2	rokoko
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
bílé	bílý	k2eAgFnPc1d1	bílá
plochy	plocha	k1gFnPc1	plocha
s	s	k7c7	s
ornamenty	ornament	k1gInPc7	ornament
ze	z	k7c2	z
14	[number]	k4	14
<g/>
karátového	karátový	k2eAgNnSc2d1	karátové
lístkového	lístkový	k2eAgNnSc2d1	lístkové
zlata	zlato	k1gNnSc2	zlato
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
české	český	k2eAgInPc4d1	český
křišťálové	křišťálový	k2eAgInPc4d1	křišťálový
lustry	lustr	k1gInPc4	lustr
a	a	k8xC	a
kachlová	kachlový	k2eAgNnPc4d1	kachlové
kamna	kamna	k1gNnPc4	kamna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místnosti	místnost	k1gFnPc1	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zařízeny	zařídit	k5eAaPmNgInP	zařídit
stroze	stroze	k6eAd1	stroze
a	a	k8xC	a
prostě	prostě	k6eAd1	prostě
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
reprezentativní	reprezentativní	k2eAgFnPc4d1	reprezentativní
prostory	prostora	k1gFnPc4	prostora
a	a	k8xC	a
místnosti	místnost	k1gFnPc4	místnost
pro	pro	k7c4	pro
hosty	host	k1gMnPc4	host
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vyzdobeny	vyzdobit	k5eAaPmNgFnP	vyzdobit
velmi	velmi	k6eAd1	velmi
honosně	honosně	k6eAd1	honosně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zrcadlovém	zrcadlový	k2eAgInSc6d1	zrcadlový
sále	sál	k1gInSc6	sál
hrál	hrát	k5eAaImAgMnS	hrát
jako	jako	k9	jako
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
zázračné	zázračný	k2eAgNnSc4d1	zázračné
dítě	dítě	k1gNnSc4	dítě
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Čínském	čínský	k2eAgInSc6d1	čínský
oválném	oválný	k2eAgInSc6d1	oválný
kabinetě	kabinet	k1gInSc6	kabinet
pořádala	pořádat	k5eAaImAgFnS	pořádat
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
tajné	tajný	k2eAgFnSc2d1	tajná
konference	konference	k1gFnSc2	konference
se	s	k7c7	s
státním	státní	k2eAgMnSc7d1	státní
kancléřem	kancléř	k1gMnSc7	kancléř
hrabětem	hrabě	k1gMnSc7	hrabě
Kounicem	Kounic	k1gMnSc7	Kounic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
zvaném	zvaný	k2eAgInSc6d1	zvaný
Vieux-Lacque	Vieux-Lacque	k1gNnSc3	Vieux-Lacque
zasedal	zasedat	k5eAaImAgInS	zasedat
Napoleon	napoleon	k1gInSc1	napoleon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Modrém	modrý	k2eAgInSc6d1	modrý
čínském	čínský	k2eAgInSc6d1	čínský
salónu	salón	k1gInSc6	salón
podepsal	podepsat	k5eAaPmAgMnS	podepsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
svoji	svůj	k3xOyFgFnSc4	svůj
abdikaci	abdikace	k1gFnSc4	abdikace
(	(	kIx(	(
<g/>
konec	konec	k1gInSc4	konec
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Milionový	milionový	k2eAgInSc1d1	milionový
pokoj	pokoj	k1gInSc1	pokoj
"	"	kIx"	"
<g/>
Millionenzimmer	Millionenzimmer	k1gInSc1	Millionenzimmer
<g/>
"	"	kIx"	"
obložený	obložený	k2eAgInSc1d1	obložený
růžovým	růžový	k2eAgNnSc7d1	růžové
dřevem	dřevo	k1gNnSc7	dřevo
s	s	k7c7	s
drahocennými	drahocenný	k2eAgFnPc7d1	drahocenná
miniaturami	miniatura	k1gFnPc7	miniatura
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Persie	Persie	k1gFnSc2	Persie
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejkrásnějším	krásný	k2eAgInPc3d3	nejkrásnější
rokokovým	rokokový	k2eAgInPc3d1	rokokový
prostorům	prostor	k1gInPc3	prostor
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
galerii	galerie	k1gFnSc6	galerie
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1814	[number]	k4	1814
<g/>
–	–	k?	–
<g/>
1815	[number]	k4	1815
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
kongres	kongres	k1gInSc1	kongres
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
pořádají	pořádat	k5eAaImIp3nP	pořádat
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
příležitostech	příležitost	k1gFnPc6	příležitost
státní	státní	k2eAgFnSc2d1	státní
audience	audience	k1gFnSc2	audience
<g/>
.	.	kIx.	.
</s>
<s>
Schönbrunn	Schönbrunn	k1gMnSc1	Schönbrunn
sloužil	sloužit	k5eAaImAgMnS	sloužit
rakouským	rakouský	k2eAgMnSc7d1	rakouský
císařům	císař	k1gMnPc3	císař
(	(	kIx(	(
<g/>
Habsburkům	Habsburk	k1gMnPc3	Habsburk
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
zimní	zimní	k2eAgNnSc1d1	zimní
sídlo	sídlo	k1gNnSc4	sídlo
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
Hofburg	Hofburg	k1gInSc1	Hofburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Schönbrunnu	Schönbrunn	k1gInSc6	Schönbrunn
==	==	k?	==
</s>
</p>
<p>
<s>
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
Schönbrunnu	Schönbrunn	k1gInSc6	Schönbrunn
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
existující	existující	k2eAgFnSc7d1	existující
zoologickou	zoologický	k2eAgFnSc7d1	zoologická
zahradou	zahrada	k1gFnSc7	zahrada
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
jedinou	jediný	k2eAgFnSc7d1	jediná
zachovanou	zachovaný	k2eAgFnSc7d1	zachovaná
barokní	barokní	k2eAgFnSc7d1	barokní
menažérií	menažérie	k1gFnSc7	menažérie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
historické	historický	k2eAgNnSc1d1	historické
zoo	zoo	k1gNnSc1	zoo
má	mít	k5eAaImIp3nS	mít
zajímavou	zajímavý	k2eAgFnSc4d1	zajímavá
minulost	minulost	k1gFnSc4	minulost
a	a	k8xC	a
současně	současně	k6eAd1	současně
má	mít	k5eAaImIp3nS	mít
plnit	plnit	k5eAaImF	plnit
důležité	důležitý	k2eAgInPc4d1	důležitý
úkoly	úkol	k1gInPc4	úkol
i	i	k9	i
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1752	[number]	k4	1752
zavedl	zavést	k5eAaPmAgMnS	zavést
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgMnPc4	svůj
šlechtické	šlechtický	k2eAgMnPc4d1	šlechtický
hosty	host	k1gMnPc4	host
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
nově	nově	k6eAd1	nově
zřízené	zřízený	k2eAgFnSc2d1	zřízená
menažérie	menažérie	k1gFnSc2	menažérie
v	v	k7c6	v
zámeckém	zámecký	k2eAgInSc6d1	zámecký
parku	park	k1gInSc6	park
letní	letní	k2eAgFnSc2d1	letní
rezidence	rezidence	k1gFnSc2	rezidence
Schönbrunn	Schönbrunna	k1gFnPc2	Schönbrunna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Schönbrunnu	Schönbrunn	k1gInSc6	Schönbrunn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1906	[number]	k4	1906
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
první	první	k4xOgNnSc4	první
slůně	slůně	k1gNnSc4	slůně
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byli	být	k5eAaImAgMnP	být
zoologové	zoolog	k1gMnPc1	zoolog
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sloni	slon	k1gMnPc1	slon
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
nerozmnožují	rozmnožovat	k5eNaImIp3nP	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Motto	motto	k1gNnSc1	motto
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
dodnes	dodnes	k6eAd1	dodnes
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Schönbrunn	Schönbrunn	k1gInSc1	Schönbrunn
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zoologická	zoologický	k2eAgFnSc1d1	zoologická
zahrada	zahrada	k1gFnSc1	zahrada
se	s	k7c7	s
šťastnými	šťastný	k2eAgNnPc7d1	šťastné
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Gloriette	Gloriett	k1gMnSc5	Gloriett
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
překrásné	překrásný	k2eAgFnSc6d1	překrásná
stavbě	stavba	k1gFnSc6	stavba
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nS	nacházet
kavárna	kavárna	k1gFnSc1	kavárna
s	s	k7c7	s
vynikajícím	vynikající	k2eAgNnSc7d1	vynikající
pečivem	pečivo	k1gNnSc7	pečivo
a	a	k8xC	a
velkolepým	velkolepý	k2eAgInSc7d1	velkolepý
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
dopoledne	dopoledne	k1gNnSc2	dopoledne
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
pořádají	pořádat	k5eAaImIp3nP	pořádat
pozdní	pozdní	k2eAgFnPc1d1	pozdní
snídaně	snídaně	k1gFnPc1	snídaně
s	s	k7c7	s
živou	živý	k2eAgFnSc7d1	živá
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
od	od	k7c2	od
klasiky	klasika	k1gFnSc2	klasika
po	po	k7c4	po
jazz	jazz	k1gInSc4	jazz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gloriette	Glorietit	k5eAaImRp2nP	Glorietit
<g/>
,	,	kIx,	,
připomínka	připomínka	k1gFnSc1	připomínka
vítězství	vítězství	k1gNnSc2	vítězství
Rakouska	Rakousko	k1gNnSc2	Rakousko
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kolína	Kolín	k1gInSc2	Kolín
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
připomíná	připomínat	k5eAaImIp3nS	připomínat
nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
stavby	stavba	k1gFnSc2	stavba
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
JOSEPHO	JOSEPHO	kA	JOSEPHO
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
AUGUSTO	Augusta	k1gMnSc5	Augusta
ET	ET	kA	ET
MARIA	Mario	k1gMnSc2	Mario
THERESIA	THERESIA	kA	THERESIA
IMPERANTIB	IMPERANTIB	kA	IMPERANTIB
<g/>
.	.	kIx.	.
</s>
<s>
MDCCLXXV	MDCCLXXV	kA	MDCCLXXV
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Postavena	postaven	k2eAgFnSc1d1	postavena
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
císařovny	císařovna	k1gFnSc2	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
lidé	člověk	k1gMnPc1	člověk
dokázali	dokázat	k5eAaPmAgMnP	dokázat
ocenit	ocenit	k5eAaPmF	ocenit
krásný	krásný	k2eAgInSc4d1	krásný
výhled	výhled	k1gInSc4	výhled
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
dvacet	dvacet	k4xCc4	dvacet
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
terasa	terasa	k1gFnSc1	terasa
(	(	kIx(	(
<g/>
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
točitých	točitý	k2eAgInPc6d1	točitý
schodech	schod	k1gInPc6	schod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zahradní	zahradní	k2eAgInSc1d1	zahradní
labyrint	labyrint	k1gInSc1	labyrint
v	v	k7c6	v
Schönbrunnu	Schönbrunn	k1gInSc6	Schönbrunn
(	(	kIx(	(
<g/>
Irrgarten	Irrgarten	k2eAgMnSc1d1	Irrgarten
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
zákoutích	zákoutí	k1gNnPc6	zákoutí
mohou	moct	k5eAaImIp3nP	moct
návštěvníci	návštěvník	k1gMnPc1	návštěvník
objevit	objevit	k5eAaPmF	objevit
symboly	symbol	k1gInPc4	symbol
dvanácti	dvanáct	k4xCc2	dvanáct
znamení	znamení	k1gNnSc6	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
sledovat	sledovat	k5eAaImF	sledovat
z	z	k7c2	z
vyvýšeného	vyvýšený	k2eAgNnSc2d1	vyvýšené
místa	místo	k1gNnSc2	místo
bloudění	bloudění	k1gNnSc2	bloudění
ostatních	ostatní	k2eAgMnPc2d1	ostatní
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Labyrint	labyrint	k1gInSc1	labyrint
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1698	[number]	k4	1698
a	a	k8xC	a
1740	[number]	k4	1740
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
ho	on	k3xPp3gMnSc4	on
tvořily	tvořit	k5eAaImAgFnP	tvořit
čtyři	čtyři	k4xCgFnPc4	čtyři
různé	různý	k2eAgFnPc4d1	různá
části	část	k1gFnPc4	část
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
pavilonem	pavilon	k1gInSc7	pavilon
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
celé	celý	k2eAgNnSc4d1	celé
bludiště	bludiště	k1gNnSc4	bludiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zmenšoval	zmenšovat	k5eAaImAgMnS	zmenšovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
vymýcena	vymýcen	k2eAgFnSc1d1	vymýcena
i	i	k8xC	i
poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
nový	nový	k2eAgInSc1d1	nový
labyrint	labyrint	k1gInSc1	labyrint
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
rozlohou	rozloha	k1gFnSc7	rozloha
1	[number]	k4	1
715	[number]	k4	715
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
autoři	autor	k1gMnPc1	autor
snažili	snažit	k5eAaImAgMnP	snažit
co	co	k9	co
možná	možná	k9	možná
nejvíce	nejvíce	k6eAd1	nejvíce
držet	držet	k5eAaImF	držet
historické	historický	k2eAgFnPc4d1	historická
předlohy	předloha	k1gFnPc4	předloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Palmenhaus	Palmenhaus	k1gInSc4	Palmenhaus
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pavilonu	pavilon	k1gInSc6	pavilon
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc1	rostlina
z	z	k7c2	z
pralesů	prales	k1gInPc2	prales
všech	všecek	k3xTgInPc2	všecek
kontinentů	kontinent	k1gInPc2	kontinent
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
poslední	poslední	k2eAgFnSc1d1	poslední
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
expozice	expozice	k1gFnSc1	expozice
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
působivé	působivý	k2eAgFnPc1d1	působivá
noční	noční	k2eAgFnPc1d1	noční
prohlídky	prohlídka	k1gFnPc1	prohlídka
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
navštívit	navštívit	k5eAaPmF	navštívit
jinak	jinak	k6eAd1	jinak
nepřístupnou	přístupný	k2eNgFnSc4d1	nepřístupná
galerii	galerie	k1gFnSc4	galerie
pod	pod	k7c7	pod
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
široká	široký	k2eAgFnSc1d1	široká
113	[number]	k4	113
m	m	kA	m
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
až	až	k9	až
28	[number]	k4	28
m	m	kA	m
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	let	k1gInPc6	let
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1882	[number]	k4	1882
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Franze	Franze	k1gFnSc1	Franze
Xavera	Xavera	k1gFnSc1	Xavera
Segenschmida	Segenschmida	k1gFnSc1	Segenschmida
v	v	k7c6	v
historizujícím	historizující	k2eAgInSc6d1	historizující
stylu	styl	k1gInSc6	styl
jako	jako	k8xS	jako
impozantní	impozantní	k2eAgFnSc1d1	impozantní
konstrukce	konstrukce	k1gFnSc1	konstrukce
ze	z	k7c2	z
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
stavba	stavba	k1gFnSc1	stavba
simuluje	simulovat	k5eAaImIp3nS	simulovat
tři	tři	k4xCgFnPc4	tři
klimatické	klimatický	k2eAgFnPc4d1	klimatická
zóny	zóna	k1gFnPc4	zóna
<g/>
:	:	kIx,	:
zimní	zimní	k2eAgFnSc4d1	zimní
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
,	,	kIx,	,
středomořskou	středomořský	k2eAgFnSc4d1	středomořská
a	a	k8xC	a
tropickou	tropický	k2eAgFnSc4d1	tropická
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Wüstenhaus	Wüstenhaus	k1gInSc1	Wüstenhaus
(	(	kIx(	(
<g/>
pouštní	pouštní	k2eAgInSc1d1	pouštní
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
s	s	k7c7	s
historickými	historický	k2eAgFnPc7d1	historická
slunečními	sluneční	k2eAgFnPc7d1	sluneční
hodinami	hodina	k1gFnPc7	hodina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
–	–	k?	–
proti	proti	k7c3	proti
palmovému	palmový	k2eAgInSc3d1	palmový
skleníku	skleník	k1gInSc3	skleník
Palmenhaus	Palmenhaus	k1gInSc4	Palmenhaus
v	v	k7c6	v
schönbrunnském	schönbrunnský	k2eAgInSc6d1	schönbrunnský
zámeckém	zámecký	k2eAgInSc6d1	zámecký
parku	park	k1gInSc6	park
–	–	k?	–
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
probudila	probudit	k5eAaPmAgFnS	probudit
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnohaletém	mnohaletý	k2eAgNnSc6d1	mnohaleté
uzavření	uzavření	k1gNnSc6	uzavření
a	a	k8xC	a
generální	generální	k2eAgFnSc3d1	generální
opravě	oprava	k1gFnSc3	oprava
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgInSc1d1	bývalý
Motýlí	motýlí	k2eAgInSc1d1	motýlí
dům	dům	k1gInSc1	dům
"	"	kIx"	"
<g/>
Schmetterlinghaus	Schmetterlinghaus	k1gInSc1	Schmetterlinghaus
<g/>
"	"	kIx"	"
znovu	znovu	k6eAd1	znovu
otevřen	otevřít	k5eAaPmNgInS	otevřít
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jako	jako	k8xS	jako
Pouštní	pouštní	k2eAgInSc1d1	pouštní
dům	dům	k1gInSc1	dům
"	"	kIx"	"
<g/>
Wüstenhaus	Wüstenhaus	k1gInSc1	Wüstenhaus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
botanického	botanický	k2eAgNnSc2d1	botanické
hlediska	hledisko	k1gNnSc2	hledisko
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
sbírka	sbírka	k1gFnSc1	sbírka
kaktusů	kaktus	k1gInPc2	kaktus
státní	státní	k2eAgFnSc2d1	státní
botanické	botanický	k2eAgFnSc2d1	botanická
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
zpřístupněna	zpřístupnit	k5eAaPmNgFnS	zpřístupnit
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dokonale	dokonale	k6eAd1	dokonale
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
poutavé	poutavý	k2eAgFnSc2d1	poutavá
koncepce	koncepce	k1gFnSc2	koncepce
zahrnující	zahrnující	k2eAgMnPc4d1	zahrnující
i	i	k8xC	i
zástupce	zástupce	k1gMnPc4	zástupce
příslušné	příslušný	k2eAgFnSc2d1	příslušná
fauny	fauna	k1gFnSc2	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
návštěvník	návštěvník	k1gMnSc1	návštěvník
zhlédne	zhlédnout	k5eAaPmIp3nS	zhlédnout
ukázky	ukázka	k1gFnPc4	ukázka
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
pouštních	pouštní	k2eAgFnPc6d1	pouštní
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Schönbrunn	Schönbrunna	k1gFnPc2	Schönbrunna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Palace	Palace	k1gFnSc1	Palace
and	and	k?	and
Gardens	Gardens	k1gInSc1	Gardens
of	of	k?	of
Schönbrunn	Schönbrunn	k1gInSc1	Schönbrunn
–	–	k?	–
UNESCO	UNESCO	kA	UNESCO
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
