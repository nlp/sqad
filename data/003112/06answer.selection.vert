<s>
Melanin	melanin	k1gInSc1	melanin
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
hnědý	hnědý	k2eAgInSc4d1	hnědý
až	až	k8xS	až
černý	černý	k2eAgInSc4d1	černý
pigment	pigment	k1gInSc4	pigment
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
živočichů	živočich	k1gMnPc2	živočich
i	i	k8xC	i
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
