<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hnědý	hnědý	k2eAgInSc1d1	hnědý
až	až	k8xS	až
černý	černý	k2eAgInSc1d1	černý
pigment	pigment	k1gInSc1	pigment
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
živočichů	živočich	k1gMnPc2	živočich
i	i	k8xC	i
prvoků	prvok	k1gMnPc2	prvok
<g/>
?	?	kIx.	?
</s>
