<s>
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
tištěná	tištěný	k2eAgFnSc1d1	tištěná
bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
biblických	biblický	k2eAgInPc2d1	biblický
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
aramejštiny	aramejština	k1gFnSc2	aramejština
a	a	k8xC	a
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
)	)	kIx)	)
přeložili	přeložit	k5eAaPmAgMnP	přeložit
překladatelé	překladatel	k1gMnPc1	překladatel
a	a	k8xC	a
teologové	teolog	k1gMnPc1	teolog
původní	původní	k2eAgFnSc2d1	původní
Jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
dostala	dostat	k5eAaPmAgFnS	dostat
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
vytištění	vytištění	k1gNnSc2	vytištění
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
byly	být	k5eAaImAgFnP	být
jihomoravské	jihomoravský	k2eAgFnPc4d1	Jihomoravská
Kralice	Kralice	k1gFnPc4	Kralice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
bible	bible	k1gFnSc2	bible
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
z	z	k7c2	z
latinské	latinský	k2eAgFnSc2d1	Latinská
Vulgáty	Vulgáta	k1gFnSc2	Vulgáta
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ivančicko-kralická	ivančickoralický	k2eAgFnSc1d1	ivančicko-kralický
bratrská	bratrský	k2eAgFnSc1d1	bratrská
tiskárna	tiskárna	k1gFnSc1	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
kladli	klást	k5eAaImAgMnP	klást
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
činnosti	činnost	k1gFnSc2	činnost
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
svých	svůj	k3xOyFgMnPc2	svůj
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
proto	proto	k8xC	proto
ocenili	ocenit	k5eAaPmAgMnP	ocenit
význam	význam	k1gInSc4	význam
knihtisku	knihtisk	k1gInSc2	knihtisk
velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
tisky	tisk	k1gInPc1	tisk
vynikají	vynikat	k5eAaImIp3nP	vynikat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
typografickou	typografický	k2eAgFnSc7d1	typografická
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Jednota	jednota	k1gFnSc1	jednota
bratrská	bratrský	k2eAgFnSc1d1	bratrská
byla	být	k5eAaImAgFnS	být
zakázaným	zakázaný	k2eAgNnSc7d1	zakázané
náboženským	náboženský	k2eAgNnSc7d1	náboženské
uskupením	uskupení	k1gNnSc7	uskupení
<g/>
,	,	kIx,	,
také	také	k9	také
jejich	jejich	k3xOp3gFnSc1	jejich
tiskárna	tiskárna	k1gFnSc1	tiskárna
působila	působit	k5eAaImAgFnS	působit
tajně	tajně	k6eAd1	tajně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
utajení	utajení	k1gNnSc2	utajení
se	se	k3xPyFc4	se
ve	v	k7c6	v
vydaných	vydaný	k2eAgFnPc6d1	vydaná
knihách	kniha	k1gFnPc6	kniha
nepoužívalo	používat	k5eNaImAgNnS	používat
označení	označení	k1gNnSc1	označení
místa	místo	k1gNnSc2	místo
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
jen	jen	k9	jen
krycí	krycí	k2eAgNnSc1d1	krycí
označení	označení	k1gNnSc1	označení
in	in	k?	in
insula	insula	k1gFnSc1	insula
hortensi	hortense	k1gFnSc4	hortense
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Tajná	tajný	k2eAgFnSc1d1	tajná
bratrská	bratrský	k2eAgFnSc1d1	bratrská
tiskárna	tiskárna	k1gFnSc1	tiskárna
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1503	[number]	k4	1503
<g/>
)	)	kIx)	)
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1518	[number]	k4	1518
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1562	[number]	k4	1562
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
v	v	k7c6	v
Ivančicích	Ivančice	k1gFnPc6	Ivančice
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1578	[number]	k4	1578
na	na	k7c6	na
tvrzi	tvrz	k1gFnSc6	tvrz
v	v	k7c6	v
Kralicích	Kralice	k1gFnPc6	Kralice
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
Jana	Jan	k1gMnSc2	Jan
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
Náměšti	Náměšť	k1gFnSc6	Náměšť
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
posledních	poslední	k2eAgNnPc6d1	poslední
místech	místo	k1gNnPc6	místo
působil	působit	k5eAaImAgMnS	působit
významný	významný	k2eAgMnSc1d1	významný
bratrský	bratrský	k2eAgMnSc1d1	bratrský
tiskař	tiskař	k1gMnSc1	tiskař
Zachariáš	Zachariáš	k1gMnSc1	Zachariáš
Šolín	Šolín	k1gMnSc1	Šolín
<g/>
.	.	kIx.	.
</s>
<s>
Iniciátorem	iniciátor	k1gMnSc7	iniciátor
překladu	překlad	k1gInSc2	překlad
byl	být	k5eAaImAgMnS	být
bratrský	bratrský	k2eAgMnSc1d1	bratrský
biskup	biskup	k1gMnSc1	biskup
Jan	Jan	k1gMnSc1	Jan
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c4	na
erasmovskou	erasmovský	k2eAgFnSc4d1	erasmovský
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
snahu	snaha	k1gFnSc4	snaha
přeložit	přeložit	k5eAaPmF	přeložit
Písmo	písmo	k1gNnSc4	písmo
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Nový	nový	k2eAgInSc4d1	nový
zákon	zákon	k1gInSc4	zákon
podle	podle	k7c2	podle
vydání	vydání	k1gNnSc2	vydání
Theodora	Theodor	k1gMnSc2	Theodor
Bezy	bez	k1gInPc4	bez
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
i	i	k9	i
latinským	latinský	k2eAgInSc7d1	latinský
překladem	překlad	k1gInSc7	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
se	se	k3xPyFc4	se
řídil	řídit	k5eAaImAgInS	řídit
především	především	k6eAd1	především
Bezovým	bezový	k2eAgInSc7d1	bezový
latinským	latinský	k2eAgInSc7d1	latinský
textem	text	k1gInSc7	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
značně	značně	k6eAd1	značně
přihlížel	přihlížet	k5eAaImAgInS	přihlížet
i	i	k9	i
k	k	k7c3	k
řeckému	řecký	k2eAgInSc3d1	řecký
originálu	originál	k1gInSc3	originál
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
překlad	překlad	k1gInSc4	překlad
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
vyšel	vyjít	k5eAaPmAgInS	vyjít
celkem	celkem	k6eAd1	celkem
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
z	z	k7c2	z
jazyku	jazyk	k1gInSc3	jazyk
řeckého	řecký	k2eAgNnSc2d1	řecké
<g/>
)	)	kIx)	)
vnově	vnově	k?	vnově
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložený	přeložený	k2eAgInSc1d1	přeložený
Léta	léto	k1gNnSc2	léto
Páně	páně	k2eAgNnSc2d1	páně
1564	[number]	k4	1564
v	v	k7c6	v
Ivančicích	Ivančice	k1gFnPc6	Ivančice
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
vnově	vnově	k?	vnově
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložený	přeložený	k2eAgInSc1d1	přeložený
<g/>
,	,	kIx,	,
secunda	secunda	k1gFnSc1	secunda
editio	editio	k1gMnSc1	editio
diligenter	diligenter	k1gMnSc1	diligenter
recognita	recognita	k1gFnSc1	recognita
anno	anno	k6eAd1	anno
1568	[number]	k4	1568
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
latiny	latina	k1gFnSc2	latina
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
pečlivě	pečlivě	k6eAd1	pečlivě
přehlédnuté	přehlédnutý	k2eAgFnPc1d1	přehlédnutá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1568	[number]	k4	1568
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
začal	začít	k5eAaPmAgInS	začít
také	také	k9	také
používat	používat	k5eAaImF	používat
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
komentáře	komentář	k1gInPc4	komentář
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
příbuzná	příbuzný	k2eAgNnPc4d1	příbuzné
biblická	biblický	k2eAgNnPc4d1	biblické
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
překladových	překladový	k2eAgFnPc2d1	překladová
variant	varianta	k1gFnPc2	varianta
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
přímo	přímo	k6eAd1	přímo
výklad	výklad	k1gInSc1	výklad
exegetický	exegetický	k2eAgInSc1d1	exegetický
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
přeložit	přeložit	k5eAaPmF	přeložit
i	i	k9	i
Starý	starý	k2eAgInSc4d1	starý
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
než	než	k8xS	než
se	se	k3xPyFc4	se
tohoto	tento	k3xDgInSc2	tento
úkolu	úkol	k1gInSc2	úkol
stačil	stačit	k5eAaBmAgMnS	stačit
ujmout	ujmout	k5eAaPmF	ujmout
<g/>
,	,	kIx,	,
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
ho	on	k3xPp3gMnSc4	on
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
koordinoval	koordinovat	k5eAaBmAgInS	koordinovat
a	a	k8xC	a
vedl	vést	k5eAaImAgInS	vést
Blahoslavův	Blahoslavův	k2eAgMnSc1d1	Blahoslavův
odchovanec	odchovanec	k1gMnSc1	odchovanec
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Ondřej	Ondřej	k1gMnSc1	Ondřej
Štefan	Štefan	k1gMnSc1	Štefan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
týmu	tým	k1gInSc6	tým
překladatelů	překladatel	k1gMnPc2	překladatel
(	(	kIx(	(
<g/>
filologů	filolog	k1gMnPc2	filolog
a	a	k8xC	a
teologů	teolog	k1gMnPc2	teolog
současně	současně	k6eAd1	současně
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nacházeli	nacházet	k5eAaImAgMnP	nacházet
<g/>
:	:	kIx,	:
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Kaménka	Kaménko	k1gNnSc2	Kaménko
(	(	kIx(	(
<g/>
hebraista	hebraista	k1gMnSc1	hebraista
<g/>
)	)	kIx)	)
Lukáš	Lukáš	k1gMnSc1	Lukáš
Helic	Helic	k1gMnSc1	Helic
(	(	kIx(	(
<g/>
pokřtěný	pokřtěný	k2eAgMnSc1d1	pokřtěný
Žid	Žid	k1gMnSc1	Žid
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Eneáš	Eneáš	k1gMnSc1	Eneáš
(	(	kIx(	(
<g/>
senior	senior	k1gMnSc1	senior
Jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
)	)	kIx)	)
Izaiáš	Izaiáš	k1gFnSc2	Izaiáš
Cibulka	Cibulka	k1gMnSc1	Cibulka
(	(	kIx(	(
<g/>
konsenior	konsenior	k1gMnSc1	konsenior
a	a	k8xC	a
správce	správce	k1gMnSc1	správce
kralického	kralický	k2eAgInSc2d1	kralický
sboru	sbor	k1gInSc2	sbor
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
Strejc	Strejc	k1gMnSc1	Strejc
(	(	kIx(	(
<g/>
konsenior	konsenior	k1gMnSc1	konsenior
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Efraim	Efraim	k1gMnSc1	Efraim
Pavel	Pavel	k1gMnSc1	Pavel
Jessen	Jessen	k2eAgMnSc1d1	Jessen
Jan	Jan	k1gMnSc1	Jan
Kapito	Kapit	k2eAgNnSc4d1	Kapito
(	(	kIx(	(
<g/>
Hlaváč	hlaváč	k1gInSc4	hlaváč
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
Nový	nový	k2eAgInSc4d1	nový
zákon	zákon	k1gInSc4	zákon
byl	být	k5eAaImAgInS	být
převzat	převzat	k2eAgInSc1d1	převzat
podrobně	podrobně	k6eAd1	podrobně
zrevidovaný	zrevidovaný	k2eAgInSc1d1	zrevidovaný
překlad	překlad	k1gInSc1	překlad
Blahoslavův	Blahoslavův	k2eAgInSc1d1	Blahoslavův
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavův	Blahoslavův	k2eAgInSc1d1	Blahoslavův
překlad	překlad	k1gInSc1	překlad
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
revidoval	revidovat	k5eAaImAgMnS	revidovat
senior	senior	k1gMnSc1	senior
Jan	Jan	k1gMnSc1	Jan
Němčanský	Němčanský	k2eAgMnSc1d1	Němčanský
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
pak	pak	k6eAd1	pak
Zachariáš	Zachariáš	k1gMnPc1	Zachariáš
Ariston	ariston	k1gInSc4	ariston
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
Kralické	kralický	k2eAgFnSc2d1	Kralická
bible	bible	k1gFnSc2	bible
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
šesti	šest	k4xCc6	šest
svazcích	svazek	k1gInPc6	svazek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
šestidílka	šestidílek	k1gMnSc2	šestidílek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
textu	text	k1gInSc2	text
do	do	k7c2	do
šesti	šest	k4xCc2	šest
svazků	svazek	k1gInPc2	svazek
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejednalo	jednat	k5eNaImAgNnS	jednat
jen	jen	k9	jen
o	o	k7c4	o
biblický	biblický	k2eAgInSc4d1	biblický
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c4	o
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
komentáře	komentář	k1gInPc4	komentář
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
text	text	k1gInSc1	text
značně	značně	k6eAd1	značně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Komentáře	komentář	k1gInPc1	komentář
a	a	k8xC	a
vsuvky	vsuvka	k1gFnPc1	vsuvka
byly	být	k5eAaImAgFnP	být
následujícího	následující	k2eAgInSc2d1	následující
druhu	druh	k1gInSc2	druh
<g/>
:	:	kIx,	:
Menším	menšit	k5eAaImIp1nS	menšit
písmem	písmo	k1gNnSc7	písmo
slova	slovo	k1gNnSc2	slovo
dodaná	dodaná	k1gFnSc1	dodaná
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
překladů	překlad	k1gInPc2	překlad
nebo	nebo	k8xC	nebo
nově	nova	k1gFnSc3	nova
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
srozumitelnost	srozumitelnost	k1gFnSc4	srozumitelnost
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
okraji	okraj	k1gInSc6	okraj
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
název	název	k1gInSc4	název
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
nadpisy	nadpis	k1gInPc1	nadpis
byly	být	k5eAaImAgInP	být
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
vybavené	vybavený	k2eAgInPc1d1	vybavený
ozdobnou	ozdobný	k2eAgFnSc7d1	ozdobná
iniciálou	iniciála	k1gFnSc7	iniciála
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
textu	text	k1gInSc2	text
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
komentář	komentář	k1gInSc4	komentář
<g/>
,	,	kIx,	,
oddělený	oddělený	k2eAgInSc4d1	oddělený
od	od	k7c2	od
samotného	samotný	k2eAgInSc2d1	samotný
biblického	biblický	k2eAgInSc2d1	biblický
textu	text	k1gInSc2	text
tenkou	tenký	k2eAgFnSc7d1	tenká
linkou	linka	k1gFnSc7	linka
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
sumářem	sumář	k1gInSc7	sumář
<g/>
,	,	kIx,	,
menší	malý	k2eAgInPc4d2	menší
sumáře	sumář	k1gInPc4	sumář
měly	mít	k5eAaImAgFnP	mít
také	také	k6eAd1	také
všechny	všechen	k3xTgFnPc1	všechen
kapitoly	kapitola	k1gFnPc1	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
paralelní	paralelní	k2eAgNnPc4d1	paralelní
biblická	biblický	k2eAgNnPc4d1	biblické
místa	místo	k1gNnPc4	místo
Výkladový	výkladový	k2eAgInSc4d1	výkladový
komentář	komentář	k1gInSc4	komentář
obsahující	obsahující	k2eAgFnSc2d1	obsahující
různé	různý	k2eAgFnSc2d1	různá
varianty	varianta	k1gFnSc2	varianta
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
překladů	překlad	k1gInPc2	překlad
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
<g/>
,	,	kIx,	,
řeckého	řecký	k2eAgInSc2d1	řecký
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
<g/>
,	,	kIx,	,
blíže	blízce	k6eAd2	blízce
neoznačeného	označený	k2eNgInSc2d1	neoznačený
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
také	také	k9	také
reálie	reálie	k1gFnSc2	reálie
<g/>
,	,	kIx,	,
užitečné	užitečný	k2eAgFnSc2d1	užitečná
k	k	k7c3	k
pochopení	pochopení	k1gNnSc3	pochopení
biblického	biblický	k2eAgInSc2d1	biblický
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kralické	kralický	k2eAgFnSc6d1	Kralická
bibli	bible	k1gFnSc6	bible
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
objevuje	objevovat	k5eAaImIp3nS	objevovat
dělení	dělení	k1gNnSc1	dělení
kapitol	kapitola	k1gFnPc2	kapitola
na	na	k7c4	na
verše	verš	k1gInPc4	verš
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
Roberta	Robert	k1gMnSc2	Robert
Stephana	Stephan	k1gMnSc2	Stephan
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1551	[number]	k4	1551
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
knih	kniha	k1gFnPc2	kniha
Mojžíšových	Mojžíšová	k1gFnPc2	Mojžíšová
(	(	kIx(	(
<g/>
Pentateuch	pentateuch	k1gInSc1	pentateuch
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
,	,	kIx,	,
Exodus	Exodus	k1gInSc1	Exodus
<g/>
,	,	kIx,	,
Leviticus	Leviticus	k1gInSc1	Leviticus
<g/>
,	,	kIx,	,
Numeri	Numeri	k1gNnSc1	Numeri
<g/>
,	,	kIx,	,
Deuteronomium	Deuteronomium	k1gNnSc1	Deuteronomium
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1579	[number]	k4	1579
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgFnPc1d1	historická
knihy	kniha	k1gFnPc1	kniha
<g/>
:	:	kIx,	:
Jozue	Jozue	k1gMnSc1	Jozue
<g/>
,	,	kIx,	,
Soudců	soudce	k1gMnPc2	soudce
<g/>
,	,	kIx,	,
Rút	Rút	k1gFnSc1	Rút
<g/>
,	,	kIx,	,
4	[number]	k4	4
knihy	kniha	k1gFnSc2	kniha
královské	královský	k2eAgFnSc2d1	královská
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
Samuelova	Samuelův	k2eAgFnSc1d1	Samuelova
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
královská	královský	k2eAgFnSc1d1	královská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Paralipomenon	paralipomenon	k1gNnSc1	paralipomenon
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
Ezdrášova	Ezdrášův	k2eAgFnSc1d1	Ezdrášova
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Ezdráš	Ezdráš	k1gFnSc2	Ezdráš
a	a	k8xC	a
Nehemjáš	Nehemjáš	k1gFnSc2	Nehemjáš
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1580	[number]	k4	1580
<g/>
.	.	kIx.	.
</s>
<s>
Básnické	básnický	k2eAgFnPc1d1	básnická
knihy	kniha	k1gFnPc1	kniha
<g/>
:	:	kIx,	:
Jób	Jób	k1gFnPc1	Jób
<g/>
,	,	kIx,	,
Žalmy	žalm	k1gInPc1	žalm
<g/>
,	,	kIx,	,
Přísloví	přísloví	k1gNnPc1	přísloví
<g/>
,	,	kIx,	,
Kazatel	kazatel	k1gMnSc1	kazatel
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
písní	píseň	k1gFnPc2	píseň
<g/>
;	;	kIx,	;
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1582	[number]	k4	1582
<g/>
.	.	kIx.	.
</s>
<s>
Prorocké	prorocký	k2eAgFnPc1d1	prorocká
knihy	kniha	k1gFnPc1	kniha
<g/>
:	:	kIx,	:
Izajáš	Izajáš	k1gMnSc1	Izajáš
<g/>
,	,	kIx,	,
Jeremjáš	Jeremjáš	k1gMnSc1	Jeremjáš
<g/>
,	,	kIx,	,
Ezechiel	Ezechiel	k1gMnSc1	Ezechiel
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
,	,	kIx,	,
Ozeáš	Ozeáš	k1gMnSc1	Ozeáš
<g/>
,	,	kIx,	,
Jóel	Jóel	k1gMnSc1	Jóel
<g/>
,	,	kIx,	,
Ámos	Ámos	k1gMnSc1	Ámos
<g/>
,	,	kIx,	,
Abdijáš	Abdijáš	k1gMnSc1	Abdijáš
<g/>
,	,	kIx,	,
Jonáš	Jonáš	k1gMnSc1	Jonáš
<g/>
,	,	kIx,	,
Micheáš	Micheáš	k1gMnSc1	Micheáš
<g/>
,	,	kIx,	,
Nahum	Nahum	k1gInSc1	Nahum
<g/>
,	,	kIx,	,
Abakuk	Abakuk	k1gMnSc1	Abakuk
<g/>
,	,	kIx,	,
Sofonjáš	Sofonjáš	k1gMnSc1	Sofonjáš
<g/>
,	,	kIx,	,
Ageus	Ageus	k1gMnSc1	Ageus
<g/>
,	,	kIx,	,
Zacharjáš	Zacharjáš	k1gMnSc1	Zacharjáš
<g/>
,	,	kIx,	,
Malachjáš	Malachjáš	k1gMnSc1	Malachjáš
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
vyšel	vyjít	k5eAaPmAgInS	vyjít
roku	rok	k1gInSc2	rok
1587	[number]	k4	1587
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Apokryfy	apokryf	k1gInPc1	apokryf
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jak	jak	k8xS	jak
apokryfy	apokryf	k1gInPc1	apokryf
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
deuterokanonické	deuterokanonický	k2eAgFnPc1d1	deuterokanonická
knihy	kniha	k1gFnPc1	kniha
<g/>
:	:	kIx,	:
Tobijáš	Tobijáš	k1gFnSc1	Tobijáš
<g/>
,	,	kIx,	,
Modlitba	modlitba	k1gFnSc1	modlitba
Manassesova	Manassesův	k2eAgFnSc1d1	Manassesova
<g/>
,	,	kIx,	,
Júdit	Júdit	k1gMnSc1	Júdit
<g/>
,	,	kIx,	,
Báruch	Báruch	k1gMnSc1	Báruch
<g/>
,	,	kIx,	,
přídavky	přídavka	k1gFnPc4	přídavka
k	k	k7c3	k
Danielovi	Daniel	k1gMnSc3	Daniel
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
<g />
.	.	kIx.	.
</s>
<s>
Ezdrášova	Ezdrášův	k2eAgFnSc1d1	Ezdrášova
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
Ezdrášova	Ezdrášův	k2eAgFnSc1d1	Ezdrášova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přídavky	přídavek	k1gInPc4	přídavek
k	k	k7c3	k
Ester	Ester	k1gFnSc3	Ester
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
Makabejská	makabejský	k2eAgFnSc1d1	Makabejská
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
Moudrosti	moudrost	k1gFnSc2	moudrost
a	a	k8xC	a
Sírachovec	Sírachovec	k1gMnSc1	Sírachovec
<g/>
;	;	kIx,	;
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
roku	rok	k1gInSc2	rok
1588	[number]	k4	1588
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgFnPc2d1	moderní
vydání	vydání	k1gNnSc1	vydání
Bible	bible	k1gFnSc2	bible
kralické	kralický	k2eAgNnSc1d1	kralické
tyto	tento	k3xDgFnPc4	tento
knihy	kniha	k1gFnPc4	kniha
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
dnešním	dnešní	k2eAgInSc7d1	dnešní
protestantským	protestantský	k2eAgInSc7d1	protestantský
územ	úzus	k1gInSc7	úzus
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
<g/>
)	)	kIx)	)
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
datovaný	datovaný	k2eAgInSc1d1	datovaný
nejednotně	jednotně	k6eNd1	jednotně
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1593	[number]	k4	1593
a	a	k8xC	a
1594	[number]	k4	1594
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1596	[number]	k4	1596
vyšla	vyjít	k5eAaPmAgFnS	vyjít
Kralická	kralický	k2eAgFnSc1d1	Kralická
bible	bible	k1gFnSc1	bible
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
jednosvazkovém	jednosvazkový	k2eAgNnSc6d1	jednosvazkové
vydání	vydání	k1gNnSc6	vydání
bez	bez	k7c2	bez
výkladového	výkladový	k2eAgInSc2d1	výkladový
aparátu	aparát	k1gInSc2	aparát
<g/>
,	,	kIx,	,
jen	jen	k9	jen
s	s	k7c7	s
rejstříkem	rejstřík	k1gInSc7	rejstřík
starozákonních	starozákonní	k2eAgInPc2d1	starozákonní
citátů	citát	k1gInPc2	citát
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
<g/>
,	,	kIx,	,
s	s	k7c7	s
výkladem	výklad	k1gInSc7	výklad
cizích	cizí	k2eAgNnPc2d1	cizí
jmen	jméno	k1gNnPc2	jméno
a	a	k8xC	a
s	s	k7c7	s
rejstříkem	rejstřík	k1gInSc7	rejstřík
liturgických	liturgický	k2eAgNnPc2d1	liturgické
čtení	čtení	k1gNnPc2	čtení
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Revizi	revize	k1gFnSc4	revize
měli	mít	k5eAaImAgMnP	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
Samuel	Samuel	k1gMnSc1	Samuel
Sušický	sušický	k2eAgMnSc1d1	sušický
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
Felin	Felin	k1gMnSc1	Felin
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Kaménka	Kaménko	k1gNnSc2	Kaménko
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Strejc	Strejc	k1gMnSc1	Strejc
<g/>
.	.	kIx.	.
</s>
<s>
Rejstříky	rejstřík	k1gInPc1	rejstřík
zhotovené	zhotovený	k2eAgInPc1d1	zhotovený
dříve	dříve	k6eAd2	dříve
Janem	Jan	k1gMnSc7	Jan
Aquinem	Aquin	k1gMnSc7	Aquin
zrevidoval	zrevidovat	k5eAaPmAgMnS	zrevidovat
Samuel	Samuel	k1gMnSc1	Samuel
Sušický	sušický	k2eAgMnSc1d1	sušický
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Albín	Albín	k1gMnSc1	Albín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vyšel	vyjít	k5eAaPmAgInS	vyjít
samostatně	samostatně	k6eAd1	samostatně
i	i	k9	i
kapesní	kapesní	k2eAgInSc1d1	kapesní
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
stránku	stránka	k1gFnSc4	stránka
tohoto	tento	k3xDgNnSc2	tento
jednosvazkového	jednosvazkový	k2eAgNnSc2d1	jednosvazkové
vydání	vydání	k1gNnSc2	vydání
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
Václav	Václav	k1gMnSc1	Václav
Elam	Elam	k1gInSc4	Elam
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
později	pozdě	k6eAd2	pozdě
vedl	vést	k5eAaImAgInS	vést
celou	celý	k2eAgFnSc4d1	celá
kralickou	kralický	k2eAgFnSc4d1	Kralická
tiskárnu	tiskárna	k1gFnSc4	tiskárna
a	a	k8xC	a
bratrský	bratrský	k2eAgInSc4d1	bratrský
sbor	sbor	k1gInSc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
vydáních	vydání	k1gNnPc6	vydání
Kralické	kralický	k2eAgFnSc2d1	Kralická
bible	bible	k1gFnSc2	bible
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
měnil	měnit	k5eAaImAgMnS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
měnila	měnit	k5eAaImAgFnS	měnit
a	a	k8xC	a
zdokonalovala	zdokonalovat	k5eAaImAgFnS	zdokonalovat
předloha	předloha	k1gFnSc1	předloha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
měli	mít	k5eAaImAgMnP	mít
překladatelé	překladatel	k1gMnPc1	překladatel
a	a	k8xC	a
revizoři	revizor	k1gMnPc1	revizor
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
prameny	pramen	k1gInPc4	pramen
a	a	k8xC	a
předlohy	předloha	k1gFnSc2	předloha
však	však	k9	však
překladatelé	překladatel	k1gMnPc1	překladatel
většinou	většinou	k6eAd1	většinou
neuvádějí	uvádět	k5eNaImIp3nP	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
u	u	k7c2	u
pátého	pátý	k4xOgInSc2	pátý
dílu	díl	k1gInSc2	díl
(	(	kIx(	(
<g/>
apokryfů	apokryf	k1gInPc2	apokryf
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
udána	udán	k2eAgFnSc1d1	udána
Antverpská	antverpský	k2eAgFnSc1d1	Antverpská
polyglota	polyglota	k1gFnSc1	polyglota
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1572	[number]	k4	1572
a	a	k8xC	a
pro	pro	k7c4	pro
knihy	kniha	k1gFnPc4	kniha
Ezdrášovy	Ezdrášův	k2eAgFnPc4d1	Ezdrášova
řecká	řecký	k2eAgFnSc1d1	řecká
bible	bible	k1gFnSc2	bible
vytištěná	vytištěný	k2eAgFnSc1d1	vytištěná
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1545	[number]	k4	1545
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1613	[number]	k4	1613
byla	být	k5eAaImAgFnS	být
jednosvazková	jednosvazkový	k2eAgFnSc1d1	jednosvazková
Kralická	kralický	k2eAgFnSc1d1	Kralická
bible	bible	k1gFnSc1	bible
vydána	vydán	k2eAgFnSc1d1	vydána
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
po	po	k7c6	po
předcházející	předcházející	k2eAgFnSc6d1	předcházející
revizi	revize	k1gFnSc6	revize
<g/>
;	;	kIx,	;
též	též	k9	též
bez	bez	k7c2	bez
poznámkového	poznámkový	k2eAgInSc2d1	poznámkový
aparátu	aparát	k1gInSc2	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nedostávalo	dostávat	k5eNaImAgNnS	dostávat
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
knih	kniha	k1gFnPc2	kniha
z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
vydání	vydání	k1gNnSc2	vydání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
vydat	vydat	k5eAaPmF	vydat
bibli	bible	k1gFnSc3	bible
v	v	k7c6	v
poněkud	poněkud	k6eAd1	poněkud
větším	veliký	k2eAgInSc6d2	veliký
a	a	k8xC	a
čitelnějším	čitelný	k2eAgInSc6d2	čitelnější
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
předchozí	předchozí	k2eAgFnSc1d1	předchozí
jednosvazková	jednosvazkový	k2eAgFnSc1d1	jednosvazková
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vydání	vydání	k1gNnSc1	vydání
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
poslední	poslední	k2eAgNnSc1d1	poslední
kralické	kralický	k2eAgNnSc1d1	kralické
vydání	vydání	k1gNnSc1	vydání
<g/>
"	"	kIx"	"
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
)	)	kIx)	)
musela	muset	k5eAaImAgFnS	muset
Jednota	jednota	k1gFnSc1	jednota
bratrská	bratrský	k2eAgFnSc1d1	bratrská
ukončit	ukončit	k5eAaPmF	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
vydání	vydání	k1gNnSc1	vydání
Kralické	kralický	k2eAgFnSc2d1	Kralická
bible	bible	k1gFnSc2	bible
se	se	k3xPyFc4	se
řídila	řídit	k5eAaImAgFnS	řídit
třetím	třetí	k4xOgNnSc7	třetí
<g/>
/	/	kIx~	/
<g/>
posledním	poslední	k2eAgNnSc7d1	poslední
autentickým	autentický	k2eAgNnSc7d1	autentické
vydáním	vydání	k1gNnSc7	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
silné	silný	k2eAgFnSc3d1	silná
protireformaci	protireformace	k1gFnSc3	protireformace
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
bible	bible	k1gFnPc1	bible
vydávány	vydáván	k2eAgFnPc1d1	vydávána
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
a	a	k8xC	a
pašovány	pašovat	k5eAaImNgFnP	pašovat
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Legálně	legálně	k6eAd1	legálně
tu	tu	k6eAd1	tu
mohl	moct	k5eAaImAgInS	moct
tento	tento	k3xDgInSc4	tento
překlad	překlad	k1gInSc4	překlad
Bible	bible	k1gFnSc2	bible
znovu	znovu	k6eAd1	znovu
vyjít	vyjít	k5eAaPmF	vyjít
až	až	k9	až
po	po	k7c6	po
250	[number]	k4	250
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
1722	[number]	k4	1722
v	v	k7c6	v
Halle	Halla	k1gFnSc6	Halla
nad	nad	k7c7	nad
Sálou	Sála	k1gFnSc7	Sála
(	(	kIx(	(
<g/>
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
pietistů	pietista	k1gMnPc2	pietista
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
1745	[number]	k4	1745
s	s	k7c7	s
mnoha	mnoho	k4c2	mnoho
tiskovými	tiskový	k2eAgFnPc7d1	tisková
chybami	chyba	k1gFnPc7	chyba
<g/>
;	;	kIx,	;
1766	[number]	k4	1766
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c6	o
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1722	[number]	k4	1722
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
srovnávána	srovnávat	k5eAaImNgFnS	srovnávat
s	s	k7c7	s
první	první	k4xOgFnSc7	první
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
třetí	třetí	k4xOgFnSc7	třetí
autentickou	autentický	k2eAgFnSc7d1	autentická
verzí	verze	k1gFnSc7	verze
<g/>
;	;	kIx,	;
1787	[number]	k4	1787
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
;	;	kIx,	;
1808	[number]	k4	1808
Palkovičovo	Palkovičův	k2eAgNnSc4d1	Palkovičův
bratislavské	bratislavský	k2eAgNnSc4d1	Bratislavské
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
přetiskující	přetiskující	k2eAgInSc4d1	přetiskující
text	text	k1gInSc4	text
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1766	[number]	k4	1766
s	s	k7c7	s
několika	několik	k4yIc7	několik
změnami	změna	k1gFnPc7	změna
<g/>
;	;	kIx,	;
1863	[number]	k4	1863
Biblia	Biblium	k1gNnSc2	Biblium
sacra	sacro	k1gNnSc2	sacro
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
Biblí	bible	k1gFnSc7	bible
svaté	svatá	k1gFnSc2	svatá
k	k	k7c3	k
tisícileté	tisíciletý	k2eAgFnSc3d1	tisíciletá
jubilejní	jubilejní	k2eAgFnSc3d1	jubilejní
slavnosti	slavnost	k1gFnSc3	slavnost
obrácení	obrácení	k1gNnSc2	obrácení
Slovanův	Slovanův	k2eAgInSc1d1	Slovanův
na	na	k7c4	na
víru	víra	k1gFnSc4	víra
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
od	od	k7c2	od
Josefa	Josef	k1gMnSc4	Josef
Růžičky	Růžička	k1gMnSc2	Růžička
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
vydání	vydání	k1gNnSc4	vydání
byla	být	k5eAaImAgFnS	být
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
byla	být	k5eAaImAgFnS	být
Kralická	kralický	k2eAgFnSc1d1	Kralická
bible	bible	k1gFnSc1	bible
vydávána	vydáván	k2eAgFnSc1d1	vydávána
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
novodobých	novodobý	k2eAgFnPc6d1	novodobá
reedicích	reedice	k1gFnPc6	reedice
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
revidované	revidovaný	k2eAgFnSc6d1	revidovaná
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Revidované	revidovaný	k2eAgNnSc4d1	revidované
znění	znění	k1gNnSc4	znění
připravil	připravit	k5eAaPmAgMnS	připravit
Jan	Jan	k1gMnSc1	Jan
Karafiát	Karafiát	k1gMnSc1	Karafiát
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Biblí	bible	k1gFnSc7	bible
svatá	svatat	k5eAaImIp3nS	svatat
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
původního	původní	k2eAgNnSc2d1	původní
vydání	vydání	k1gNnSc2	vydání
kralického	kralický	k2eAgInSc2d1	kralický
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1579	[number]	k4	1579
<g/>
-	-	kIx~	-
<g/>
1593	[number]	k4	1593
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
biblická	biblický	k2eAgFnSc1d1	biblická
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
819	[number]	k4	819
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
bez	bez	k7c2	bez
apokryfů	apokryf	k1gInPc2	apokryf
<g/>
.	.	kIx.	.
</s>
<s>
Přepsáno	přepsán	k2eAgNnSc1d1	přepsáno
novodobým	novodobý	k2eAgInSc7d1	novodobý
pravopisem	pravopis	k1gInSc7	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
původních	původní	k2eAgFnPc2d1	původní
poznámek	poznámka	k1gFnPc2	poznámka
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
Pána	pán	k1gMnSc2	pán
a	a	k8xC	a
Spasitele	spasitel	k1gMnSc4	spasitel
našeho	náš	k3xOp1gMnSc4	náš
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Krista	k1gFnSc1	Krista
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
vydání	vydání	k1gNnSc2	vydání
kralického	kralický	k2eAgInSc2d1	kralický
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1593	[number]	k4	1593
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
biblická	biblický	k2eAgFnSc1d1	biblická
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
279	[number]	k4	279
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Přepsáno	přepsán	k2eAgNnSc1d1	přepsáno
novodobým	novodobý	k2eAgInSc7d1	novodobý
pravopisem	pravopis	k1gInSc7	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
původních	původní	k2eAgFnPc2d1	původní
poznámek	poznámka	k1gFnPc2	poznámka
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Apokryfy	apokryf	k1gInPc7	apokryf
<g/>
:	:	kIx,	:
Biblí	bible	k1gFnSc7	bible
české	český	k2eAgMnPc4d1	český
díl	díl	k1gInSc4	díl
pátý	pátý	k4xOgMnSc1	pátý
z	z	k7c2	z
r.	r.	kA	r.
1588	[number]	k4	1588
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
kralickém	kralický	k2eAgNnSc6d1	kralické
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rada	rada	k1gFnSc1	rada
Jednoty	jednota	k1gFnSc2	jednota
českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
272	[number]	k4	272
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Kralický	kralický	k2eAgInSc4d1	kralický
text	text	k1gInSc4	text
novodobým	novodobý	k2eAgInSc7d1	novodobý
pravopisem	pravopis	k1gInSc7	pravopis
přepsal	přepsat	k5eAaPmAgInS	přepsat
a	a	k8xC	a
doslovem	doslov	k1gInSc7	doslov
opatřil	opatřit	k5eAaPmAgMnS	opatřit
Pavel	Pavel	k1gMnSc1	Pavel
Josef	Josef	k1gMnSc1	Josef
Chráska	Chrásek	k1gMnSc2	Chrásek
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
<g/>
:	:	kIx,	:
šestidílná	šestidílný	k2eAgFnSc1d1	šestidílná
<g/>
:	:	kIx,	:
kompletní	kompletní	k2eAgNnSc1d1	kompletní
vydání	vydání	k1gNnSc1	vydání
s	s	k7c7	s
původními	původní	k2eAgFnPc7d1	původní
poznámkami	poznámka	k1gFnPc7	poznámka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
biblická	biblický	k2eAgFnSc1d1	biblická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
2813	[number]	k4	2813
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87287	[number]	k4	87287
<g/>
-	-	kIx~	-
<g/>
78	[number]	k4	78
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
šestidílná	šestidílný	k2eAgFnSc1d1	šestidílná
verze	verze	k1gFnSc1	verze
Kralické	kralický	k2eAgFnSc2d1	Kralická
bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Paderbornu	Paderborn	k1gInSc6	Paderborn
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Schoningh	Schoningha	k1gFnPc2	Schoningha
<g/>
.	.	kIx.	.
</s>
<s>
Srovnání	srovnání	k1gNnSc1	srovnání
doslovné	doslovný	k2eAgFnSc2d1	doslovná
transkripce	transkripce	k1gFnSc2	transkripce
z	z	k7c2	z
vydání	vydání	k1gNnSc2	vydání
1613	[number]	k4	1613
se	s	k7c7	s
současným	současný	k2eAgInSc7d1	současný
způsobem	způsob	k1gInSc7	způsob
přepisu	přepis	k1gInSc2	přepis
kralického	kralický	k2eAgInSc2d1	kralický
textu	text	k1gInSc2	text
<g/>
:	:	kIx,	:
</s>
