<s>
Index	index	k1gInSc1
tělesné	tělesný	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
označovaný	označovaný	k2eAgInSc4d1
zkratkou	zkratka	k1gFnSc7
BMI	BMI	kA
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
body	bod	k1gInPc7
mass	mass	k6eAd1
index	index	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
číslo	číslo	k1gNnSc4
používané	používaný	k2eAgNnSc4d1
jako	jako	k8xS,k8xC
indikátor	indikátor	k1gInSc4
podváhy	podváha	k1gFnSc2
<g/>
,	,	kIx,
normální	normální	k2eAgFnSc2d1
tělesné	tělesný	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
<g/>
,	,	kIx,
nadváhy	nadváha	k1gFnSc2
a	a	k8xC
obezity	obezita	k1gFnSc2
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgNnSc1d1
statistické	statistický	k2eAgNnSc1d1
porovnávání	porovnávání	k1gNnSc1
tělesné	tělesný	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
lidí	člověk	k1gMnPc2
s	s	k7c7
různou	různý	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
<g/>
.	.	kIx.
</s>