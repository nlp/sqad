<s>
Velehrad	Velehrad	k1gInSc4	Velehrad
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
ve	v	k7c6	v
(	(	kIx(	(
<g/>
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
asi	asi	k9	asi
1,3	[number]	k4	1,3
tisíce	tisíc	k4xCgInPc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
poutních	poutní	k2eAgNnPc2d1	poutní
míst	místo	k1gNnPc2	místo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
střediskem	středisko	k1gNnSc7	středisko
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
Veligradem	Veligrad	k1gInSc7	Veligrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejstarší	starý	k2eAgNnSc4d3	nejstarší
cisterciácké	cisterciácký	k2eAgNnSc4d1	cisterciácké
opatství	opatství	k1gNnSc4	opatství
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
náleží	náležet	k5eAaImIp3nP	náležet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
jezuitům	jezuita	k1gMnPc3	jezuita
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
cílem	cíl	k1gInSc7	cíl
Národní	národní	k2eAgFnSc2d1	národní
pouti	pouť	k1gFnSc2	pouť
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
svátku	svátek	k1gInSc2	svátek
svatých	svatý	k1gMnPc2	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
tři	tři	k4xCgInPc1	tři
katolické	katolický	k2eAgInPc1d1	katolický
řády	řád	k1gInPc1	řád
<g/>
:	:	kIx,	:
kromě	kromě	k7c2	kromě
už	už	k6eAd1	už
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
jezuitů	jezuita	k1gMnPc2	jezuita
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
ještě	ještě	k9	ještě
Kongregace	kongregace	k1gFnSc1	kongregace
sester	sestra	k1gFnPc2	sestra
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc4	Metoděj
a	a	k8xC	a
slovenská	slovenský	k2eAgFnSc1d1	slovenská
provincie	provincie	k1gFnSc1	provincie
Kongregácie	Kongregácie	k1gFnSc2	Kongregácie
sestier	sestira	k1gFnPc2	sestira
Božského	božský	k2eAgNnSc2d1	božské
Vykupiteľa	Vykupiteľum	k1gNnSc2	Vykupiteľum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
dosud	dosud	k6eAd1	dosud
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
střediskem	středisko	k1gNnSc7	středisko
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
sídlo	sídlo	k1gNnSc1	sídlo
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
není	být	k5eNaImIp3nS	být
nikde	nikde	k6eAd1	nikde
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
doklad	doklad	k1gInSc1	doklad
pro	pro	k7c4	pro
jméno	jméno	k1gNnSc4	jméno
Velehrad	Velehrad	k1gInSc4	Velehrad
je	být	k5eAaImIp3nS	být
až	až	k9	až
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
biskupa	biskup	k1gMnSc2	biskup
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Zdíka	Zdík	k1gMnSc2	Zdík
z	z	k7c2	z
r.	r.	kA	r.
1131	[number]	k4	1131
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
bližší	blízký	k2eAgFnSc2d2	bližší
identifikace	identifikace	k1gFnSc2	identifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
na	na	k7c4	na
Velehrad	Velehrad	k1gInSc4	Velehrad
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Národní	národní	k2eAgFnSc2d1	národní
poutě	pouť	k1gFnSc2	pouť
navzdory	navzdory	k7c3	navzdory
obstrukcím	obstrukce	k1gFnPc3	obstrukce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
státních	státní	k2eAgInPc2d1	státní
orgánů	orgán	k1gInPc2	orgán
sjelo	sjet	k5eAaPmAgNnS	sjet
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
za	za	k7c2	za
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
zvykem	zvyk	k1gInSc7	zvyk
(	(	kIx(	(
<g/>
důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
kulaté	kulatý	k2eAgNnSc4d1	kulaté
1100	[number]	k4	1100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
sv.	sv.	kA	sv.
Metoděje	Metoděj	k1gMnSc4	Metoděj
a	a	k8xC	a
okolnost	okolnost	k1gFnSc4	okolnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
udělit	udělit	k5eAaPmF	udělit
velehradské	velehradský	k2eAgFnSc3d1	Velehradská
bazilice	bazilika	k1gFnSc3	bazilika
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
růži	růže	k1gFnSc4	růže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Projev	projev	k1gInSc1	projev
ministra	ministr	k1gMnSc2	ministr
kultury	kultura	k1gFnSc2	kultura
Klusáka	Klusák	k1gMnSc2	Klusák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
označil	označit	k5eAaPmAgMnS	označit
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc4	Metoděj
za	za	k7c4	za
první	první	k4xOgMnPc4	první
komunisty	komunista	k1gMnPc4	komunista
<g/>
,	,	kIx,	,
důsledně	důsledně	k6eAd1	důsledně
vynechával	vynechávat	k5eAaImAgMnS	vynechávat
při	při	k7c6	při
řeči	řeč	k1gFnSc6	řeč
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
přídomek	přídomek	k1gInSc4	přídomek
svatý	svatý	k1gMnSc1	svatý
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
dát	dát	k5eAaPmF	dát
celé	celý	k2eAgFnSc2d1	celá
pouti	pouť	k1gFnSc2	pouť
"	"	kIx"	"
<g/>
ten	ten	k3xDgInSc4	ten
správný	správný	k2eAgInSc4d1	správný
politický	politický	k2eAgInSc4d1	politický
rozměr	rozměr	k1gInSc4	rozměr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
ministr	ministr	k1gMnSc1	ministr
z	z	k7c2	z
tribuny	tribuna	k1gFnSc2	tribuna
vypískán	vypískán	k2eAgMnSc1d1	vypískán
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
akce	akce	k1gFnSc1	akce
přerostla	přerůst	k5eAaPmAgFnS	přerůst
v	v	k7c6	v
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
demonstrací	demonstrace	k1gFnPc2	demonstrace
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
za	za	k7c4	za
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Velehrad	Velehrad	k1gInSc4	Velehrad
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
Celostátní	celostátní	k2eAgNnSc1d1	celostátní
setkání	setkání	k1gNnSc1	setkání
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
asi	asi	k9	asi
8000	[number]	k4	8000
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
Velehradě	Velehrad	k1gInSc6	Velehrad
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Velehradský	velehradský	k2eAgInSc4d1	velehradský
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Velehradský	velehradský	k2eAgInSc1d1	velehradský
klášter	klášter	k1gInSc1	klášter
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc1	první
cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
klášter	klášter	k1gInSc1	klášter
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
bazilika	bazilika	k1gFnSc1	bazilika
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
,	,	kIx,	,
vyznamenaná	vyznamenaný	k2eAgFnSc1d1	vyznamenaná
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
růží	růž	k1gFnSc7	růž
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
poutní	poutní	k2eAgFnSc7d1	poutní
bazilikou	bazilika	k1gFnSc7	bazilika
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
biskupa	biskup	k1gMnSc2	biskup
Roberta	Robert	k1gMnSc2	Robert
klášter	klášter	k1gInSc1	klášter
založil	založit	k5eAaPmAgMnS	založit
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jindřich	Jindřich	k1gMnSc1	Jindřich
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vesnice	vesnice	k1gFnSc2	vesnice
Veligradu	Veligrad	k1gInSc2	Veligrad
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vybudován	vybudován	k2eAgMnSc1d1	vybudován
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
románském	románský	k2eAgInSc6d1	románský
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
rané	raný	k2eAgFnSc2d1	raná
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
náleží	náležet	k5eAaImIp3nS	náležet
jezuitům	jezuita	k1gMnPc3	jezuita
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1421	[number]	k4	1421
klášter	klášter	k1gInSc4	klášter
vypálili	vypálit	k5eAaPmAgMnP	vypálit
moravští	moravský	k2eAgMnPc1d1	moravský
husité	husita	k1gMnPc1	husita
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
obnově	obnova	k1gFnSc3	obnova
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
mezitím	mezitím	k6eAd1	mezitím
byl	být	k5eAaImAgInS	být
obydlen	obydlet	k5eAaPmNgInS	obydlet
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
opatský	opatský	k2eAgInSc1d1	opatský
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
zpustošen	zpustošit	k5eAaPmNgInS	zpustošit
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
sedmihradskými	sedmihradský	k2eAgNnPc7d1	sedmihradské
vojsky	vojsko	k1gNnPc7	vojsko
(	(	kIx(	(
<g/>
1623	[number]	k4	1623
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
povstalci	povstalec	k1gMnPc1	povstalec
z	z	k7c2	z
Valašska	Valašsko	k1gNnSc2	Valašsko
(	(	kIx(	(
<g/>
1626	[number]	k4	1626
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
událostech	událost	k1gFnPc6	událost
následovala	následovat	k5eAaImAgFnS	následovat
další	další	k2eAgFnSc1d1	další
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc1d1	barokní
přestavba	přestavba	k1gFnSc1	přestavba
(	(	kIx(	(
<g/>
1629	[number]	k4	1629
<g/>
-	-	kIx~	-
<g/>
1635	[number]	k4	1635
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1681	[number]	k4	1681
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
ke	k	k7c3	k
zničujícímu	zničující	k2eAgInSc3d1	zničující
požáru	požár	k1gInSc3	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
přestavbu	přestavba	k1gFnSc4	přestavba
konventu	konvent	k1gInSc2	konvent
a	a	k8xC	a
následně	následně	k6eAd1	následně
(	(	kIx(	(
<g/>
a	a	k8xC	a
zejména	zejména	k9	zejména
<g/>
)	)	kIx)	)
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Josefínských	josefínský	k2eAgFnPc2d1	josefínská
reforem	reforma	k1gFnPc2	reforma
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
kostel	kostel	k1gInSc1	kostel
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
farní	farní	k2eAgInSc1d1	farní
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ale	ale	k8xC	ale
fara	fara	k1gFnSc1	fara
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
klášter	klášter	k1gInSc4	klášter
získali	získat	k5eAaPmAgMnP	získat
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zřídili	zřídit	k5eAaPmAgMnP	zřídit
své	svůj	k3xOyFgInPc4	svůj
vyučovací	vyučovací	k2eAgInPc4d1	vyučovací
ústavy	ústav	k1gInPc4	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jejich	jejich	k3xOp3gNnSc2	jejich
panování	panování	k1gNnSc2	panování
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
dvoje	dvoje	k4xRgFnPc1	dvoje
vykopávky	vykopávka	k1gFnPc1	vykopávka
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
-	-	kIx~	-
<g/>
1906	[number]	k4	1906
a	a	k8xC	a
1936	[number]	k4	1936
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
vysušení	vysušení	k1gNnSc1	vysušení
a	a	k8xC	a
odvodnění	odvodnění	k1gNnSc1	odvodnění
klášterního	klášterní	k2eAgNnSc2d1	klášterní
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
provedli	provést	k5eAaPmAgMnP	provést
jezuité	jezuita	k1gMnPc1	jezuita
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
opravy	oprava	k1gFnSc2	oprava
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Akce	akce	k1gFnSc2	akce
K	k	k7c3	k
přepaden	přepaden	k2eAgInSc1d1	přepaden
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
veškerá	veškerý	k3xTgFnSc1	veškerý
jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
jezuité	jezuita	k1gMnPc1	jezuita
byli	být	k5eAaImAgMnP	být
odvlečeni	odvléct	k5eAaPmNgMnP	odvléct
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
jezuité	jezuita	k1gMnPc1	jezuita
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
vrátili	vrátit	k5eAaPmAgMnP	vrátit
a	a	k8xC	a
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
Velehrad	Velehrad	k1gInSc1	Velehrad
navštívil	navštívit	k5eAaPmAgMnS	navštívit
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
jej	on	k3xPp3gMnSc4	on
František	František	k1gMnSc1	František
kardinál	kardinál	k1gMnSc1	kardinál
Tomášek	Tomášek	k1gMnSc1	Tomášek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
památku	památka	k1gFnSc4	památka
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
před	před	k7c7	před
bazilikou	bazilika	k1gFnSc7	bazilika
vztyčen	vztyčen	k2eAgInSc1d1	vztyčen
velký	velký	k2eAgInSc1d1	velký
kříž	kříž	k1gInSc1	kříž
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
papežským	papežský	k2eAgInSc7d1	papežský
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bazilika	bazilika	k1gFnSc1	bazilika
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
nově	nově	k6eAd1	nově
založeného	založený	k2eAgInSc2d1	založený
kláštera	klášter	k1gInSc2	klášter
byl	být	k5eAaImAgInS	být
i	i	k9	i
klášterní	klášterní	k2eAgInSc1d1	klášterní
"	"	kIx"	"
<g/>
kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
biskupem	biskup	k1gMnSc7	biskup
Robertem	Robert	k1gMnSc7	Robert
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
výstavby	výstavba	k1gFnSc2	výstavba
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1228	[number]	k4	1228
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dnešní	dnešní	k2eAgNnSc1d1	dnešní
plné	plný	k2eAgNnSc1d1	plné
jméno	jméno	k1gNnSc1	jméno
dnes	dnes	k6eAd1	dnes
zní	znět	k5eAaImIp3nS	znět
"	"	kIx"	"
<g/>
bazilika	bazilika	k1gFnSc1	bazilika
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc4	Metoděj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současnou	současný	k2eAgFnSc4d1	současná
podobu	podoba	k1gFnSc4	podoba
bazilika	bazilika	k1gFnSc1	bazilika
získala	získat	k5eAaPmAgFnS	získat
při	při	k7c6	při
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
přestavbách	přestavba	k1gFnPc6	přestavba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
následovaly	následovat	k5eAaImAgFnP	následovat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1681	[number]	k4	1681
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
nim	on	k3xPp3gInPc3	on
jí	on	k3xPp3gFnSc3	on
však	však	k9	však
zůstal	zůstat	k5eAaPmAgInS	zůstat
uchován	uchován	k2eAgInSc1d1	uchován
románsko-gotický	románskootický	k2eAgInSc1d1	románsko-gotický
charakter	charakter	k1gInSc1	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přestavby	přestavba	k1gFnSc2	přestavba
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
14	[number]	k4	14
samostatných	samostatný	k2eAgFnPc2d1	samostatná
pobočných	pobočný	k2eAgFnPc2d1	pobočná
kaplí	kaple	k1gFnPc2	kaple
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zbořena	zbořen	k2eAgFnSc1d1	zbořena
předsíň	předsíň	k1gFnSc1	předsíň
a	a	k8xC	a
západní	západní	k2eAgNnSc1d1	západní
průčelí	průčelí	k1gNnSc1	průčelí
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
baziliky	bazilika	k1gFnSc2	bazilika
tak	tak	k6eAd1	tak
klesla	klesnout	k5eAaPmAgFnS	klesnout
ze	z	k7c2	z
100	[number]	k4	100
na	na	k7c4	na
86	[number]	k4	86
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
byla	být	k5eAaImAgFnS	být
předělána	předělán	k2eAgFnSc1d1	předělána
střecha	střecha	k1gFnSc1	střecha
a	a	k8xC	a
věže	věž	k1gFnPc1	věž
baziliky	bazilika	k1gFnSc2	bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Podlaha	podlaha	k1gFnSc1	podlaha
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
okolního	okolní	k2eAgInSc2d1	okolní
terénu	terén	k1gInSc2	terén
(	(	kIx(	(
<g/>
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnPc2	staletí
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
okolí	okolí	k1gNnSc2	okolí
asi	asi	k9	asi
o	o	k7c4	o
2	[number]	k4	2
metry	metr	k1gInPc4	metr
<g/>
)	)	kIx)	)
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
vybudovány	vybudován	k2eAgFnPc4d1	vybudována
krypty	krypta	k1gFnPc4	krypta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1719	[number]	k4	1719
byly	být	k5eAaImAgFnP	být
opravy	oprava	k1gFnPc1	oprava
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
a	a	k8xC	a
dodána	dodán	k2eAgFnSc1d1	dodána
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
výzdoba	výzdoba	k1gFnSc1	výzdoba
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
podíleli	podílet	k5eAaImAgMnP	podílet
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
štukatér	štukatér	k1gMnSc1	štukatér
Baldassare	Baldassar	k1gMnSc5	Baldassar
Fontana	Fontana	k1gFnSc1	Fontana
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
Carl	Carl	k1gMnSc1	Carl
Johann	Johann	k1gMnSc1	Johann
Steinhaüser	Steinhaüser	k1gMnSc1	Steinhaüser
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
Ignác	Ignác	k1gMnSc1	Ignác
Raab	Raab	k1gMnSc1	Raab
<g/>
.	.	kIx.	.
</s>
<s>
Přebudování	přebudování	k1gNnSc1	přebudování
kostela	kostel	k1gInSc2	kostel
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1735	[number]	k4	1735
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
druhým	druhý	k4xOgNnSc7	druhý
svěcením	svěcení	k1gNnSc7	svěcení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
udělil	udělit	k5eAaPmAgInS	udělit
kostelu	kostel	k1gInSc3	kostel
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
titul	titul	k1gInSc1	titul
basilika	basilika	k1gFnSc1	basilika
minor	minor	k2eAgFnSc1d1	minor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1985	[number]	k4	1985
obdržela	obdržet	k5eAaPmAgFnS	obdržet
bazilika	bazilika	k1gFnSc1	bazilika
od	od	k7c2	od
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
růži	růže	k1gFnSc4	růže
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Stojanovo	Stojanův	k2eAgNnSc1d1	Stojanovo
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
zřídili	zřídit	k5eAaPmAgMnP	zřídit
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
kolej	kolej	k1gFnSc1	kolej
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
řádové	řádový	k2eAgNnSc1d1	řádové
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
vychovávat	vychovávat	k5eAaImF	vychovávat
kněze	kněz	k1gMnPc4	kněz
a	a	k8xC	a
misionáře	misionář	k1gMnPc4	misionář
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
pobočkou	pobočka	k1gFnSc7	pobočka
Arcibiskupského	arcibiskupský	k2eAgNnSc2d1	Arcibiskupské
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Praze-Bubenči	Praze-Bubenči	k1gFnSc6	Praze-Bubenči
<g/>
.	.	kIx.	.
</s>
<s>
Obrovskou	obrovský	k2eAgFnSc4d1	obrovská
prestiž	prestiž	k1gFnSc4	prestiž
mu	on	k3xPp3gMnSc3	on
přinesl	přinést	k5eAaPmAgMnS	přinést
rok	rok	k1gInSc4	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XV	XV	kA	XV
<g/>
.	.	kIx.	.
povýšil	povýšit	k5eAaPmAgInS	povýšit
kolej	kolej	k1gFnSc4	kolej
příslušející	příslušející	k2eAgFnSc4d1	příslušející
k	k	k7c3	k
ústavu	ústav	k1gInSc3	ústav
na	na	k7c4	na
Papežský	papežský	k2eAgInSc4d1	papežský
ústav	ústav	k1gInSc4	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Kolej	kolej	k1gFnSc1	kolej
tak	tak	k9	tak
přijala	přijmout	k5eAaPmAgFnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Papežský	papežský	k2eAgInSc1d1	papežský
misijní	misijní	k2eAgInSc1d1	misijní
ústav	ústav	k1gInSc1	ústav
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc4	Metoděj
pro	pro	k7c4	pro
misie	misie	k1gFnPc4	misie
slovanské	slovanský	k2eAgFnPc4d1	Slovanská
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
osamostatnilo	osamostatnit	k5eAaPmAgNnS	osamostatnit
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Řádové	řádový	k2eAgNnSc1d1	řádové
československé	československý	k2eAgNnSc1d1	Československé
gymnázium	gymnázium	k1gNnSc1	gymnázium
Papežské	papežský	k2eAgFnSc2d1	Papežská
koleje	kolej	k1gFnSc2	kolej
Tovaryšstva	tovaryšstvo	k1gNnSc2	tovaryšstvo
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
nacisty	nacista	k1gMnPc7	nacista
a	a	k8xC	a
areál	areál	k1gInSc4	areál
kláštera	klášter	k1gInSc2	klášter
využívala	využívat	k5eAaImAgFnS	využívat
Hitlerjugend	Hitlerjugend	k1gInSc4	Hitlerjugend
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byly	být	k5eAaImAgInP	být
oba	dva	k4xCgInPc1	dva
ústavy	ústav	k1gInPc1	ústav
obnoveny	obnoven	k2eAgInPc1d1	obnoven
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
nadlouho	nadlouho	k6eAd1	nadlouho
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
koleje	kolej	k1gFnSc2	kolej
i	i	k8xC	i
gymnázia	gymnázium	k1gNnSc2	gymnázium
byla	být	k5eAaImAgFnS	být
násilně	násilně	k6eAd1	násilně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Akce	akce	k1gFnSc2	akce
K	k	k7c3	k
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
na	na	k7c4	na
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
přepaden	přepadnout	k5eAaPmNgInS	přepadnout
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
zabrala	zabrat	k5eAaPmAgFnS	zabrat
<g/>
,	,	kIx,	,
ústavy	ústav	k1gInPc1	ústav
jediným	jediný	k2eAgNnSc7d1	jediné
prohlášením	prohlášení	k1gNnSc7	prohlášení
zlikvidovala	zlikvidovat	k5eAaPmAgFnS	zlikvidovat
a	a	k8xC	a
jezuity	jezuita	k1gMnPc4	jezuita
odvezla	odvézt	k5eAaPmAgFnS	odvézt
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
zde	zde	k6eAd1	zde
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
výuku	výuka	k1gFnSc4	výuka
2004	[number]	k4	2004
církevní	církevní	k2eAgFnSc4d1	církevní
Stojanovo	Stojanův	k2eAgNnSc1d1	Stojanovo
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Janem	Jan	k1gMnSc7	Jan
Graubnerem	Graubner	k1gMnSc7	Graubner
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Zjevení	zjevení	k1gNnSc2	zjevení
Páně	páně	k2eAgNnSc2d1	páně
(	(	kIx(	(
<g/>
Velehrad	Velehrad	k1gInSc4	Velehrad
<g/>
)	)	kIx)	)
-	-	kIx~	-
zvaný	zvaný	k2eAgInSc1d1	zvaný
Cyrilka	Cyrilka	k1gFnSc1	Cyrilka
<g/>
,	,	kIx,	,
neb	neb	k8xC	neb
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
ústní	ústní	k2eAgFnSc2d1	ústní
tradice	tradice	k1gFnSc2	tradice
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
Cyrilova	Cyrilův	k2eAgNnSc2d1	Cyrilovo
kázání	kázání	k1gNnSc2	kázání
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
Sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1703	[number]	k4	1703
od	od	k7c2	od
neznámého	známý	k2eNgMnSc2d1	neznámý
autora	autor	k1gMnSc2	autor
stojí	stát	k5eAaImIp3nS	stát
při	při	k7c6	při
silnici	silnice	k1gFnSc6	silnice
ze	z	k7c2	z
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
na	na	k7c4	na
Velehrad	Velehrad	k1gInSc4	Velehrad
u	u	k7c2	u
křižovatky	křižovatka	k1gFnSc2	křižovatka
na	na	k7c4	na
Modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Kristus	Kristus	k1gMnSc1	Kristus
je	být	k5eAaImIp3nS	být
vytesán	vytesat	k5eAaPmNgInS	vytesat
v	v	k7c6	v
poněkud	poněkud	k6eAd1	poněkud
menší	malý	k2eAgFnSc6d2	menší
než	než	k8xS	než
v	v	k7c6	v
životní	životní	k2eAgFnSc6d1	životní
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
svázanýma	svázaný	k2eAgFnPc7d1	svázaná
rukama	ruka	k1gFnPc7	ruka
připoutaná	připoutaný	k2eAgFnSc1d1	připoutaná
k	k	k7c3	k
nízkému	nízký	k2eAgInSc3d1	nízký
sloupku	sloupek	k1gInSc3	sloupek
představuje	představovat	k5eAaImIp3nS	představovat
"	"	kIx"	"
<g/>
muže	muž	k1gMnSc2	muž
bolesti	bolest	k1gFnSc2	bolest
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
stojící	stojící	k2eAgFnSc1d1	stojící
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
předklášteří	předklášteří	k1gNnSc2	předklášteří
(	(	kIx(	(
<g/>
před	před	k7c7	před
západní	západní	k2eAgFnSc7d1	západní
klášterní	klášterní	k2eAgFnSc7d1	klášterní
branou	brána	k1gFnSc7	brána
<g/>
)	)	kIx)	)
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
opata	opat	k1gMnSc2	opat
Petra	Petr	k1gMnSc2	Petr
Silaveckého	Silavecký	k2eAgMnSc2d1	Silavecký
velehradský	velehradský	k2eAgInSc4d1	velehradský
cisterciácký	cisterciácký	k2eAgInSc4d1	cisterciácký
konvrše	konvrše	k?	konvrše
Bernard	Bernard	k1gMnSc1	Bernard
Giermek	Giermek	k1gMnSc1	Giermek
<g/>
.	.	kIx.	.
</s>
<s>
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
socha	socha	k1gFnSc1	socha
na	na	k7c6	na
sloupu	sloup	k1gInSc6	sloup
s	s	k7c7	s
korintskou	korintský	k2eAgFnSc7d1	Korintská
hlavicí	hlavice	k1gFnSc7	hlavice
byla	být	k5eAaImAgFnS	být
opatem	opat	k1gMnSc7	opat
posvěcena	posvěcen	k2eAgFnSc1d1	posvěcena
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1681	[number]	k4	1681
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1715	[number]	k4	1715
byl	být	k5eAaImAgInS	být
sloup	sloup	k1gInSc1	sloup
upraven	upravit	k5eAaPmNgInS	upravit
v	v	k7c4	v
mohutnější	mohutný	k2eAgNnSc4d2	mohutnější
sousoší	sousoší	k1gNnSc4	sousoší
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
poděkování	poděkování	k1gNnSc4	poděkování
za	za	k7c4	za
šťastně	šťastně	k6eAd1	šťastně
přestálou	přestálý	k2eAgFnSc4d1	přestálá
morovou	morový	k2eAgFnSc4d1	morová
epidemii	epidemie	k1gFnSc4	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
sloup	sloup	k1gInSc1	sloup
s	s	k7c7	s
Immaculatou	Immaculatý	k2eAgFnSc7d1	Immaculatý
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c4	na
čtyřboký	čtyřboký	k2eAgInSc4d1	čtyřboký
podklad	podklad	k1gInSc4	podklad
z	z	k7c2	z
kvádrového	kvádrový	k2eAgNnSc2d1	kvádrové
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
dopředu	dopředu	k6eAd1	dopředu
otevřený	otevřený	k2eAgInSc1d1	otevřený
hrob	hrob	k1gInSc1	hrob
jako	jako	k8xC	jako
jeskyně	jeskyně	k1gFnSc1	jeskyně
s	s	k7c7	s
ležící	ležící	k2eAgFnSc7d1	ležící
postavou	postava	k1gFnSc7	postava
sv.	sv.	kA	sv.
Rozálie	Rozálie	k1gFnPc4	Rozálie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
sousoší	sousoší	k1gNnSc1	sousoší
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
"	"	kIx"	"
<g/>
Rozárkou	Rozárka	k1gFnSc7	Rozárka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
římse	římsa	k1gFnSc6	římsa
podkladu	podklad	k1gInSc2	podklad
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
soklu	sokl	k1gInSc2	sokl
dvě	dva	k4xCgFnPc4	dva
postavy	postava	k1gFnPc1	postava
stojících	stojící	k2eAgMnPc2d1	stojící
andělů	anděl	k1gMnPc2	anděl
a	a	k8xC	a
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
římse	římsa	k1gFnSc6	římsa
tři	tři	k4xCgFnPc4	tři
kartuše	kartuš	k1gFnPc4	kartuš
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc6	jenž
větší	veliký	k2eAgMnSc1d2	veliký
střední	střední	k1gMnSc1	střední
nese	nést	k5eAaImIp3nS	nést
plastickou	plastický	k2eAgFnSc4d1	plastická
podobu	podoba	k1gFnSc4	podoba
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
tváře	tvář	k1gFnSc2	tvář
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Salvator	Salvator	k1gInSc1	Salvator
mundi	mund	k1gMnPc1	mund
adiuva	adiuva	k6eAd1	adiuva
nos	nos	k1gInSc4	nos
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejspodnější	spodní	k2eAgFnSc4d3	nejspodnější
partii	partie	k1gFnSc4	partie
tvoří	tvořit	k5eAaImIp3nS	tvořit
balustráda	balustráda	k1gFnSc1	balustráda
s	s	k7c7	s
nárožními	nárožní	k2eAgFnPc7d1	nárožní
sochami	socha	k1gFnPc7	socha
čtyř	čtyři	k4xCgMnPc2	čtyři
patronů	patron	k1gMnPc2	patron
proti	proti	k7c3	proti
moru	mor	k1gInSc3	mor
-	-	kIx~	-
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Šebestiána	Šebestián	k1gMnSc2	Šebestián
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Rocha	Rocha	k1gFnSc1	Rocha
a	a	k8xC	a
sv.	sv.	kA	sv.
Bernarda	Bernard	k1gMnSc2	Bernard
<g/>
.	.	kIx.	.
</s>
<s>
Sloup	sloup	k1gInSc1	sloup
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
U	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
č.	č.	k?	č.
428	[number]	k4	428
asi	asi	k9	asi
3,5	[number]	k4	3,5
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Modrá	modrat	k5eAaImIp3nS	modrat
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
katastrů	katastr	k1gInPc2	katastr
Jankovic	Jankovice	k1gFnPc2	Jankovice
a	a	k8xC	a
Velehradu	Velehrad	k1gInSc2	Velehrad
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
-	-	kIx~	-
kamenný	kamenný	k2eAgInSc1d1	kamenný
blok	blok	k1gInSc1	blok
z	z	k7c2	z
hrubozrnného	hrubozrnný	k2eAgInSc2d1	hrubozrnný
pískovce	pískovec	k1gInSc2	pískovec
"	"	kIx"	"
<g/>
Králův	Králův	k2eAgInSc1d1	Králův
stůl	stůl	k1gInSc1	stůl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
opředený	opředený	k2eAgInSc1d1	opředený
pověstmi	pověst	k1gFnPc7	pověst
<g/>
,	,	kIx,	,
pokládaný	pokládaný	k2eAgInSc1d1	pokládaný
za	za	k7c4	za
megalitický	megalitický	k2eAgInSc4d1	megalitický
dolmen	dolmen	k1gInSc4	dolmen
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
kameni	kámen	k1gInSc6	kámen
existuje	existovat	k5eAaImIp3nS	existovat
zmínka	zmínka	k1gFnSc1	zmínka
už	už	k6eAd1	už
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1228	[number]	k4	1228
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
požehnána	požehnán	k2eAgFnSc1d1	požehnána
Poutní	poutní	k2eAgFnSc1d1	poutní
cesta	cesta	k1gFnSc1	cesta
růžence	růženec	k1gInSc2	růženec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Staré	Staré	k2eAgNnSc4d1	Staré
Město	město	k1gNnSc4	město
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
s	s	k7c7	s
Velehradem	Velehrad	k1gInSc7	Velehrad
<g/>
.	.	kIx.	.
</s>
<s>
Poutní	poutní	k2eAgFnSc4d1	poutní
cestu	cesta	k1gFnSc4	cesta
požehnal	požehnat	k5eAaPmAgMnS	požehnat
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Jan	Jan	k1gMnSc1	Jan
Graubner	Graubner	k1gMnSc1	Graubner
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architektů	architekt	k1gMnPc2	architekt
Klementa	Klement	k1gMnSc2	Klement
a	a	k8xC	a
Todorova	Todorův	k2eAgMnSc2d1	Todorův
byly	být	k5eAaImAgFnP	být
vydlážděny	vydlážděn	k2eAgFnPc4d1	vydlážděna
kamenné	kamenný	k2eAgFnPc4d1	kamenná
plochy	plocha	k1gFnPc4	plocha
pro	pro	k7c4	pro
vztyčení	vztyčení	k1gNnPc4	vztyčení
kamenných	kamenný	k2eAgNnPc2d1	kamenné
zastavení	zastavení	k1gNnPc2	zastavení
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgNnPc2	tento
zastavení	zastavení	k1gNnPc2	zastavení
je	být	k5eAaImIp3nS	být
dvacet	dvacet	k4xCc1	dvacet
a	a	k8xC	a
nesou	nést	k5eAaImIp3nP	nést
reliéfy	reliéf	k1gInPc4	reliéf
zobrazující	zobrazující	k2eAgNnPc4d1	zobrazující
růžencová	růžencový	k2eAgNnPc4d1	Růžencové
tajemství	tajemství	k1gNnPc4	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
výtvarníka	výtvarník	k1gMnSc2	výtvarník
Milivoje	Milivoj	k1gInSc2	Milivoj
Husáka	Husák	k1gMnSc2	Husák
z	z	k7c2	z
Lelekovic	Lelekovice	k1gFnPc2	Lelekovice
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
vytesal	vytesat	k5eAaPmAgMnS	vytesat
kameník	kameník	k1gMnSc1	kameník
Josef	Josef	k1gMnSc1	Josef
Kozel	Kozel	k1gMnSc1	Kozel
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
byly	být	k5eAaImAgInP	být
schváleny	schválit	k5eAaPmNgInP	schválit
liturgickou	liturgický	k2eAgFnSc7d1	liturgická
komisí	komise	k1gFnSc7	komise
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
zbudování	zbudování	k1gNnSc6	zbudování
cesty	cesta	k1gFnSc2	cesta
zaštítila	zaštítit	k5eAaPmAgFnS	zaštítit
Matice	matice	k1gFnSc1	matice
velehradská	velehradský	k2eAgFnSc1d1	Velehradská
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
:	:	kIx,	:
Poutní	poutní	k2eAgInSc1d1	poutní
dům	dům	k1gInSc1	dům
Velehrad	Velehrad	k1gInSc1	Velehrad
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Umístěn	umístěn	k2eAgMnSc1d1	umístěn
na	na	k7c4	na
Via	via	k7c4	via
delle	delle	k6eAd1	delle
Fornaci	Fornace	k1gFnSc4	Fornace
200	[number]	k4	200
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
chůze	chůze	k1gFnSc2	chůze
od	od	k7c2	od
baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Oslavný	oslavný	k2eAgInSc1d1	oslavný
sloup	sloup	k1gInSc1	sloup
Marca	Marcus	k1gMnSc2	Marcus
Aurelia	Aurelius	k1gMnSc2	Aurelius
<g/>
,	,	kIx,	,
odhalený	odhalený	k2eAgInSc1d1	odhalený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
196	[number]	k4	196
po	po	k7c6	po
Kr.	Kr.	k1gFnSc6	Kr.
<g/>
,	,	kIx,	,
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
válečné	válečný	k2eAgNnSc1d1	válečné
tažení	tažení	k1gNnSc1	tažení
římských	římský	k2eAgNnPc2d1	římské
vojsk	vojsko	k1gNnPc2	vojsko
proti	proti	k7c3	proti
moravským	moravský	k2eAgMnPc3d1	moravský
Kvádům	Kvád	k1gMnPc3	Kvád
<g/>
.	.	kIx.	.
</s>
<s>
Legie	legie	k1gFnSc1	legie
i	i	k9	i
s	s	k7c7	s
jistým	jistý	k2eAgInSc7d1	jistý
počtem	počet	k1gInSc7	počet
lodí	loď	k1gFnPc2	loď
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
tábora	tábor	k1gInSc2	tábor
Carnuntum	Carnuntum	k1gNnSc1	Carnuntum
a	a	k8xC	a
postupovala	postupovat	k5eAaImAgFnS	postupovat
po	po	k7c6	po
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
Moravy	Morava	k1gFnSc2	Morava
severně	severně	k6eAd1	severně
proti	proti	k7c3	proti
Kvádům	Kvád	k1gMnPc3	Kvád
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
výjev	výjev	k1gInSc4	výjev
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
znázorňovat	znázorňovat	k5eAaImF	znázorňovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
kvádskou	kvádský	k2eAgFnSc4d1	kvádský
aglomeraci	aglomerace	k1gFnSc4	aglomerace
<g/>
,	,	kIx,	,
možného	možný	k2eAgMnSc2d1	možný
předchůdce	předchůdce	k1gMnSc2	předchůdce
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
slovanského	slovanský	k2eAgInSc2d1	slovanský
Veligradu	Veligrad	k1gInSc2	Veligrad
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
či	či	k8xC	či
Mikulčice	Mikulčice	k1gFnPc1	Mikulčice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
opevnění	opevnění	k1gNnSc6	opevnění
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
velké	velký	k2eAgFnSc2d1	velká
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
postup	postup	k1gInSc4	postup
římských	římský	k2eAgFnPc2d1	římská
legií	legie	k1gFnPc2	legie
po	po	k7c6	po
jantarové	jantarový	k2eAgFnSc6d1	jantarová
stezce	stezka	k1gFnSc6	stezka
atd.	atd.	kA	atd.
·	·	k?	·
Pastorační	pastorační	k2eAgInSc4d1	pastorační
dům	dům	k1gInSc4	dům
Velehrad	Velehrad	k1gInSc1	Velehrad
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poutní	poutní	k2eAgInSc1d1	poutní
dům	dům	k1gInSc1	dům
sloužící	sloužící	k2eAgInSc1d1	sloužící
k	k	k7c3	k
pastoraci	pastorace	k1gFnSc6	pastorace
bychom	by	kYmCp1nP	by
našli	najít	k5eAaPmAgMnP	najít
v	v	k7c6	v
malebném	malebný	k2eAgNnSc6d1	malebné
horském	horský	k2eAgNnSc6d1	horské
údolí	údolí	k1gNnSc6	údolí
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
St.	st.	kA	st.
Martin	Martin	k1gInSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
1310	[number]	k4	1310
metrech	metr	k1gInPc6	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
blízko	blízko	k7c2	blízko
Walsbergu	Walsberg	k1gInSc2	Walsberg
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholky	vrcholek	k1gInPc1	vrcholek
obklopující	obklopující	k2eAgInSc1d1	obklopující
pastorační	pastorační	k2eAgInSc4d1	pastorační
dům	dům	k1gInSc4	dům
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výšky	výška	k1gFnPc4	výška
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2100	[number]	k4	2100
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Účelem	účel	k1gInSc7	účel
domu	dům	k1gInSc2	dům
je	být	k5eAaImIp3nS	být
ubytování	ubytování	k1gNnSc4	ubytování
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
bohoslovců	bohoslovec	k1gMnPc2	bohoslovec
<g/>
,	,	kIx,	,
kněžím	kněz	k1gMnPc3	kněz
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nP	sloužit
také	také	k9	také
školám	škola	k1gFnPc3	škola
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
spousta	spousta	k1gFnSc1	spousta
nádherných	nádherný	k2eAgFnPc2d1	nádherná
turistických	turistický	k2eAgFnPc2d1	turistická
stezek	stezka	k1gFnPc2	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Malebné	malebný	k2eAgInPc1d1	malebný
vrcholky	vrcholek	k1gInPc1	vrcholek
Dolomit	Dolomity	k1gInPc2	Dolomity
obklopující	obklopující	k2eAgFnSc4d1	obklopující
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
úplně	úplně	k6eAd1	úplně
vybízejí	vybízet	k5eAaImIp3nP	vybízet
k	k	k7c3	k
turistice	turistika	k1gFnSc3	turistika
a	a	k8xC	a
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
k	k	k7c3	k
lyžování	lyžování	k1gNnSc3	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Pastorační	pastorační	k2eAgInSc1d1	pastorační
dům	dům	k1gInSc1	dům
ve	v	k7c6	v
St.	st.	kA	st.
Martin	Martin	k1gMnSc1	Martin
in	in	k?	in
Gsies	Gsies	k1gInSc1	Gsies
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
:	:	kIx,	:
Velehrad	Velehrad	k1gInSc1	Velehrad
v	v	k7c4	v
Notting	Notting	k1gInSc4	Notting
Hillu	Hill	k1gInSc2	Hill
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
české	český	k2eAgNnSc1d1	české
a	a	k8xC	a
slovenské	slovenský	k2eAgNnSc1d1	slovenské
krajanské	krajanský	k2eAgNnSc1d1	krajanské
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
činné	činný	k2eAgFnPc1d1	činná
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
