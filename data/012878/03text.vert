<p>
<s>
Octomilka	octomilka	k1gFnSc1	octomilka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Drosophila	Drosophila	k1gFnSc1	Drosophila
melanogaster	melanogaster	k1gMnSc1	melanogaster
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čeleď	čeleď	k1gFnSc4	čeleď
octomilkovití	octomilkovitý	k2eAgMnPc1d1	octomilkovitý
<g/>
,	,	kIx,	,
řád	řád	k1gInSc4	řád
dvoukřídlí	dvoukřídlí	k1gMnPc1	dvoukřídlí
(	(	kIx(	(
<g/>
Diptera	Dipter	k1gMnSc2	Dipter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druhový	k2eAgNnSc1d1	druhové
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
černobřichá	černobřichat	k5eAaImIp3nS	černobřichat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Drosophily	Drosophil	k1gInPc1	Drosophil
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
banánové	banánový	k2eAgFnSc2d1	banánová
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
ovocné	ovocný	k2eAgFnPc1d1	ovocná
mušky	muška	k1gFnPc1	muška
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaPmNgInP	využívat
jako	jako	k9	jako
laboratorní	laboratorní	k2eAgNnPc1d1	laboratorní
zvířata	zvíře	k1gNnPc1	zvíře
nebo	nebo	k8xC	nebo
krmivo	krmivo	k1gNnSc1	krmivo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
jako	jako	k9	jako
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
modelové	modelový	k2eAgInPc4d1	modelový
organismy	organismus	k1gInPc4	organismus
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
a	a	k8xC	a
v	v	k7c6	v
genetických	genetický	k2eAgFnPc6d1	genetická
studiích	studie	k1gFnPc6	studie
<g/>
,	,	kIx,	,
fyziologii	fyziologie	k1gFnSc3	fyziologie
a	a	k8xC	a
evoluční	evoluční	k2eAgFnSc3d1	evoluční
biologii	biologie	k1gFnSc3	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
vrtulovití	vrtulovitý	k2eAgMnPc1d1	vrtulovitý
(	(	kIx(	(
<g/>
Tephritidae	Tephritidae	k1gNnSc7	Tephritidae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
označování	označování	k1gNnPc1	označování
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ovocné	ovocný	k2eAgFnPc1d1	ovocná
mušky	muška	k1gFnPc1	muška
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nedorozuměním	nedorozumění	k1gNnPc3	nedorozumění
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
čeleď	čeleď	k1gFnSc1	čeleď
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
škůdcům	škůdce	k1gMnPc3	škůdce
na	na	k7c6	na
pěstovaném	pěstovaný	k2eAgNnSc6d1	pěstované
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
laboratorní	laboratorní	k2eAgNnSc4d1	laboratorní
a	a	k8xC	a
chovatelské	chovatelský	k2eAgNnSc4d1	chovatelské
(	(	kIx(	(
<g/>
krmivo	krmivo	k1gNnSc4	krmivo
<g/>
)	)	kIx)	)
účely	účel	k1gInPc1	účel
se	se	k3xPyFc4	se
z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
bezkřídlá	bezkřídlý	k2eAgFnSc1d1	bezkřídlá
mutace	mutace	k1gFnSc1	mutace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
(	(	kIx(	(
<g/>
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
)	)	kIx)	)
forma	forma	k1gFnSc1	forma
octomilky	octomilka	k1gFnSc2	octomilka
obecné	obecný	k2eAgFnSc2d1	obecná
má	mít	k5eAaImIp3nS	mít
jasně	jasně	k6eAd1	jasně
červené	červený	k2eAgNnSc4d1	červené
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
kvasícím	kvasící	k2eAgNnSc6d1	kvasící
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
,	,	kIx,	,
marmeládách	marmeláda	k1gFnPc6	marmeláda
<g/>
,	,	kIx,	,
ovocných	ovocný	k2eAgFnPc6d1	ovocná
šťávách	šťáva	k1gFnPc6	šťáva
apod.	apod.	kA	apod.
Beznohé	beznohý	k2eAgFnSc2d1	beznohá
larvy	larva	k1gFnSc2	larva
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
mm	mm	kA	mm
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
hnijící	hnijící	k2eAgFnSc6d1	hnijící
dužnině	dužnina	k1gFnSc6	dužnina
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
octomilky	octomilka	k1gFnSc2	octomilka
==	==	k?	==
</s>
</p>
<p>
<s>
Octomilky	octomilka	k1gFnPc1	octomilka
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
chovatelé	chovatel	k1gMnPc1	chovatel
říkají	říkat	k5eAaImIp3nP	říkat
"	"	kIx"	"
<g/>
vinné	vinný	k2eAgFnPc1d1	vinná
mušky	muška	k1gFnPc1	muška
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
zdrojem	zdroj	k1gInSc7	zdroj
potravy	potrava	k1gFnSc2	potrava
pro	pro	k7c4	pro
dravý	dravý	k2eAgInSc4d1	dravý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
žáby	žába	k1gFnPc4	žába
a	a	k8xC	a
mláďata	mládě	k1gNnPc4	mládě
malých	malý	k2eAgInPc2d1	malý
druhů	druh	k1gInPc2	druh
ještěrů	ještěr	k1gMnPc2	ještěr
<g/>
.	.	kIx.	.
</s>
<s>
Chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
dvě	dva	k4xCgFnPc1	dva
formy	forma	k1gFnPc1	forma
octomilek	octomilka	k1gFnPc2	octomilka
–	–	k?	–
klasická	klasický	k2eAgFnSc1d1	klasická
<g/>
,	,	kIx,	,
okřídlená	okřídlený	k2eAgFnSc1d1	okřídlená
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	s	k7c7	s
zakrnělými	zakrnělý	k2eAgNnPc7d1	zakrnělé
křídly	křídlo	k1gNnPc7	křídlo
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
častěji	často	k6eAd2	často
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgMnSc1d2	vhodnější
jak	jak	k8xS	jak
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
tak	tak	k8xC	tak
manipulaci	manipulace	k1gFnSc4	manipulace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optimální	optimální	k2eAgFnSc1d1	optimální
teplota	teplota	k1gFnSc1	teplota
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
chovat	chovat	k5eAaImF	chovat
i	i	k9	i
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
vývoj	vývoj	k1gInSc1	vývoj
se	se	k3xPyFc4	se
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
vyšší	vysoký	k2eAgFnSc6d2	vyšší
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
degenerovat	degenerovat	k5eAaBmF	degenerovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
optimální	optimální	k2eAgFnSc6d1	optimální
teplotě	teplota	k1gFnSc6	teplota
trvá	trvat	k5eAaImIp3nS	trvat
jejich	jejich	k3xOp3gInSc1	jejich
vývojový	vývojový	k2eAgInSc1d1	vývojový
cyklus	cyklus	k1gInSc1	cyklus
8-10	[number]	k4	8-10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vajíček	vajíčko	k1gNnPc2	vajíčko
se	se	k3xPyFc4	se
larvy	larva	k1gFnPc1	larva
líhnou	líhnout	k5eAaImIp3nP	líhnout
do	do	k7c2	do
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
za	za	k7c7	za
4	[number]	k4	4
dny	den	k1gInPc7	den
se	se	k3xPyFc4	se
2	[number]	k4	2
<g/>
x	x	k?	x
svlékají	svlékat	k5eAaImIp3nP	svlékat
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
se	se	k3xPyFc4	se
zakuklí	zakuklit	k5eAaPmIp3nS	zakuklit
</s>
</p>
<p>
<s>
a	a	k8xC	a
za	za	k7c7	za
4	[number]	k4	4
dny	den	k1gInPc7	den
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
jedinou	jediný	k2eAgFnSc7d1	jediná
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
životnost	životnost	k1gFnSc1	životnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Embryonální	embryonální	k2eAgInSc4d1	embryonální
vývoj	vývoj	k1gInSc4	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Tělní	tělní	k2eAgFnSc1d1	tělní
segmentace	segmentace	k1gFnSc1	segmentace
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
živočichů	živočich	k1gMnPc2	živočich
asi	asi	k9	asi
nejrozvinutější	rozvinutý	k2eAgMnSc1d3	nejrozvinutější
u	u	k7c2	u
členovců	členovec	k1gMnPc2	členovec
a	a	k8xC	a
podrobně	podrobně	k6eAd1	podrobně
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
octomilky	octomilka	k1gFnSc2	octomilka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mouchy	moucha	k1gFnPc4	moucha
<g/>
"	"	kIx"	"
rodu	rod	k1gInSc2	rod
Drosophila	Drosophila	k1gFnSc1	Drosophila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Octomilky	octomilka	k1gFnPc1	octomilka
jsou	být	k5eAaImIp3nP	být
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
modelovým	modelový	k2eAgInSc7d1	modelový
organismem	organismus	k1gInSc7	organismus
genetiků	genetik	k1gMnPc2	genetik
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
průběh	průběh	k1gInSc1	průběh
segmentace	segmentace	k1gFnSc2	segmentace
popsán	popsat	k5eAaPmNgInS	popsat
právě	právě	k9	právě
z	z	k7c2	z
molekulárního	molekulární	k2eAgNnSc2d1	molekulární
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc4d1	výrazná
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
segmentaci	segmentace	k1gFnSc6	segmentace
těla	tělo	k1gNnSc2	tělo
hmyzu	hmyz	k1gInSc2	hmyz
gen	gen	k1gInSc1	gen
bicoid	bicoid	k1gInSc1	bicoid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
oplození	oplození	k1gNnSc6	oplození
vajíčka	vajíčko	k1gNnSc2	vajíčko
rozlišit	rozlišit	k5eAaPmF	rozlišit
budoucí	budoucí	k2eAgFnSc4d1	budoucí
přední	přední	k2eAgFnSc4d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
embrya	embryo	k1gNnSc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
morfogen	morfogen	k1gInSc1	morfogen
řídí	řídit	k5eAaImIp3nS	řídit
spouštění	spouštění	k1gNnSc4	spouštění
dalších	další	k2eAgInPc2d1	další
genů	gen	k1gInPc2	gen
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
tělních	tělní	k2eAgInPc6d1	tělní
článcích	článek	k1gInPc6	článek
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
geny	gen	k1gInPc1	gen
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
gap	gap	k?	gap
geny	gen	k1gInPc1	gen
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
spouští	spouštět	k5eAaImIp3nP	spouštět
čím	čí	k3xOyQgNnSc7	čí
dál	daleko	k6eAd2	daleko
jemnější	jemný	k2eAgFnSc1d2	jemnější
kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
oblastmi	oblast	k1gFnPc7	oblast
embrya	embryo	k1gNnSc2	embryo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Drosophila	Drosophil	k1gMnSc2	Drosophil
melanogaster	melanogastra	k1gFnPc2	melanogastra
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
