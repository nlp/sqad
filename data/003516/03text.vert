<s>
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
e-mail	eail	k1gInSc1	e-mail
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
mail	mail	k1gInSc1	mail
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
záměnu	záměna	k1gFnSc4	záměna
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
email	email	k1gInSc1	email
–	–	k?	–
nátěrová	nátěrový	k2eAgFnSc1d1	nátěrová
hmota	hmota	k1gFnSc1	hmota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
odesílání	odesílání	k1gNnSc2	odesílání
<g/>
,	,	kIx,	,
doručování	doručování	k1gNnSc2	doručování
a	a	k8xC	a
přijímání	přijímání	k1gNnSc2	přijímání
zpráv	zpráva	k1gFnPc2	zpráva
přes	přes	k7c4	přes
elektronické	elektronický	k2eAgInPc4d1	elektronický
komunikační	komunikační	k2eAgInPc4d1	komunikační
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
E-mailové	Eailové	k2eAgFnPc1d1	E-mailové
služby	služba	k1gFnPc1	služba
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
mezi	mezi	k7c4	mezi
veřejnost	veřejnost	k1gFnSc4	veřejnost
až	až	k9	až
díky	díky	k7c3	díky
vzniku	vznik	k1gInSc3	vznik
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
volných	volný	k2eAgFnPc2d1	volná
e-mailových	eailův	k2eAgFnPc2d1	e-mailova
služeb	služba	k1gFnPc2	služba
Hotmail	Hotmail	k1gMnSc1	Hotmail
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
celosvětovém	celosvětový	k2eAgInSc6d1	celosvětový
úspěchu	úspěch	k1gInSc6	úspěch
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
firma	firma	k1gFnSc1	firma
Microsoft	Microsoft	kA	Microsoft
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
další	další	k2eAgFnPc4d1	další
nyní	nyní	k6eAd1	nyní
známé	známý	k2eAgFnPc4d1	známá
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
e-mail	eail	k1gInSc1	e-mail
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
internetový	internetový	k2eAgInSc4d1	internetový
systém	systém	k1gInSc4	systém
elektronické	elektronický	k2eAgFnSc2d1	elektronická
pošty	pošta	k1gFnSc2	pošta
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
protokolu	protokol	k1gInSc6	protokol
SMTP	SMTP	kA	SMTP
(	(	kIx(	(
<g/>
Simple	Simple	k1gFnSc1	Simple
Mail	mail	k1gInSc1	mail
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
intranetové	intranetový	k2eAgInPc4d1	intranetový
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
posílat	posílat	k5eAaImF	posílat
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
zprávy	zpráva	k1gFnSc2	zpráva
uživatelům	uživatel	k1gMnPc3	uživatel
uvnitř	uvnitř	k7c2	uvnitř
jedné	jeden	k4xCgFnSc2	jeden
společnosti	společnost	k1gFnSc2	společnost
nebo	nebo	k8xC	nebo
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
tyto	tento	k3xDgInPc1	tento
systémy	systém	k1gInPc1	systém
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
nestandardní	standardní	k2eNgInPc1d1	nestandardní
protokoly	protokol	k1gInPc1	protokol
<g/>
,	,	kIx,	,
mívají	mívat	k5eAaImIp3nP	mívat
ovšem	ovšem	k9	ovšem
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jim	on	k3xPp3gMnPc3	on
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
posílat	posílat	k5eAaImF	posílat
a	a	k8xC	a
přijímat	přijímat	k5eAaImF	přijímat
e-maily	eail	k1gInPc4	e-mail
z	z	k7c2	z
internetu	internet	k1gInSc2	internet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
širokému	široký	k2eAgNnSc3d1	široké
rozšíření	rozšíření	k1gNnSc3	rozšíření
e-mailu	eail	k1gInSc2	e-mail
přispěl	přispět	k5eAaPmAgInS	přispět
zejména	zejména	k9	zejména
internet	internet	k1gInSc1	internet
<g/>
.	.	kIx.	.
</s>
<s>
E-mail	eail	k1gInSc1	e-mail
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
internet	internet	k1gInSc1	internet
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
staré	starý	k2eAgInPc1d1	starý
e-mailové	eailové	k2eAgInPc1d1	e-mailové
systémy	systém	k1gInPc1	systém
byly	být	k5eAaImAgInP	být
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
nástrojem	nástroj	k1gInSc7	nástroj
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
pošta	pošta	k1gFnSc1	pošta
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
jako	jako	k8xS	jako
způsob	způsob	k1gInSc1	způsob
komunikace	komunikace	k1gFnSc2	komunikace
více	hodně	k6eAd2	hodně
uživatelů	uživatel	k1gMnPc2	uživatel
mainframového	mainframový	k2eAgInSc2d1	mainframový
počítače	počítač	k1gInSc2	počítač
se	s	k7c7	s
sdílením	sdílení	k1gNnSc7	sdílení
času	čas	k1gInSc2	čas
<g/>
;	;	kIx,	;
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
historie	historie	k1gFnSc1	historie
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
systémy	systém	k1gInPc7	systém
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
schopností	schopnost	k1gFnSc7	schopnost
byly	být	k5eAaImAgFnP	být
Q32	Q32	k1gFnPc4	Q32
od	od	k7c2	od
SDC	SDC	kA	SDC
a	a	k8xC	a
CTSS	CTSS	kA	CTSS
z	z	k7c2	z
MIT	MIT	kA	MIT
<g/>
.	.	kIx.	.
</s>
<s>
E-mail	eail	k1gInSc1	e-mail
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	s	k7c7	s
síťovým	síťový	k2eAgInSc7d1	síťový
e-mailem	eail	k1gInSc7	e-mail
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
uživatelem	uživatel	k1gMnSc7	uživatel
posílání	posílání	k1gNnSc4	posílání
zpráv	zpráva	k1gFnPc2	zpráva
mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
počítači	počítač	k1gInPc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Raná	raný	k2eAgFnSc1d1	raná
historie	historie	k1gFnSc1	historie
síťového	síťový	k2eAgInSc2d1	síťový
e-mailu	eail	k1gInSc2	e-mail
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
;	;	kIx,	;
systémy	systém	k1gInPc1	systém
AUTODIN	AUTODIN	kA	AUTODIN
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
přenos	přenos	k1gInSc4	přenos
elektronických	elektronický	k2eAgFnPc2d1	elektronická
textových	textový	k2eAgFnPc2d1	textová
zpráv	zpráva	k1gFnPc2	zpráva
mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
počítači	počítač	k1gInPc7	počítač
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc4	něco
podobného	podobný	k2eAgNnSc2d1	podobné
měl	mít	k5eAaImAgMnS	mít
už	už	k9	už
systém	systém	k1gInSc4	systém
SAGE	SAGE	kA	SAGE
<g/>
.	.	kIx.	.
</s>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
síť	síť	k1gFnSc1	síť
ARPANET	ARPANET	kA	ARPANET
měla	mít	k5eAaImAgFnS	mít
hlavní	hlavní	k2eAgNnSc4d1	hlavní
slovo	slovo	k1gNnSc4	slovo
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
elektronické	elektronický	k2eAgFnSc2d1	elektronická
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
experimenty	experiment	k1gInPc4	experiment
s	s	k7c7	s
přenosem	přenos	k1gInSc7	přenos
e-mailů	eail	k1gInPc2	e-mail
mezi	mezi	k7c4	mezi
systémy	systém	k1gInPc4	systém
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
ARPANETu	ARPANETus	k1gInSc2	ARPANETus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Ray	Ray	k?	Ray
Tomlinson	Tomlinson	k1gNnSc1	Tomlinson
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
používat	používat	k5eAaImF	používat
znak	znak	k1gInSc4	znak
@	@	kIx~	@
na	na	k7c6	na
oddělení	oddělení	k1gNnSc6	oddělení
jména	jméno	k1gNnSc2	jméno
uživatele	uživatel	k1gMnSc2	uživatel
od	od	k7c2	od
názvu	název	k1gInSc2	název
stroje	stroj	k1gInSc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
původcem	původce	k1gMnSc7	původce
elektronické	elektronický	k2eAgFnSc2d1	elektronická
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přehnaný	přehnaný	k2eAgInSc1d1	přehnaný
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jeho	jeho	k3xOp3gInPc4	jeho
programy	program	k1gInPc4	program
SNDMSG	SNDMSG	kA	SNDMSG
a	a	k8xC	a
READMAIL	READMAIL	kA	READMAIL
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
vývoji	vývoj	k1gInSc6	vývoj
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
ARPANET	ARPANET	kA	ARPANET
významně	významně	k6eAd1	významně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
popularitu	popularita	k1gFnSc4	popularita
e-mailu	eail	k1gInSc2	e-mail
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
stal	stát	k5eAaPmAgMnS	stát
trhákem	trhák	k1gInSc7	trhák
jako	jako	k8xS	jako
aplikace	aplikace	k1gFnSc1	aplikace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ARPANETu	ARPANETus	k1gInSc2	ARPANETus
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
elektronická	elektronický	k2eAgFnSc1d1	elektronická
pošta	pošta	k1gFnSc1	pošta
stávala	stávat	k5eAaImAgFnS	stávat
všeobecně	všeobecně	k6eAd1	všeobecně
známou	známý	k2eAgFnSc4d1	známá
<g/>
,	,	kIx,	,
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
popularita	popularita	k1gFnSc1	popularita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
i	i	k9	i
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
mezi	mezi	k7c4	mezi
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
neměli	mít	k5eNaImAgMnP	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
ARPANETu	ARPANETum	k1gNnSc3	ARPANETum
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
několik	několik	k4yIc1	několik
protokolů	protokol	k1gInPc2	protokol
na	na	k7c4	na
doručování	doručování	k1gNnSc4	doručování
pošty	pošta	k1gFnSc2	pošta
mezi	mezi	k7c7	mezi
skupinami	skupina	k1gFnPc7	skupina
počítačů	počítač	k1gMnPc2	počítač
se	s	k7c7	s
sdíleným	sdílený	k2eAgInSc7d1	sdílený
časem	čas	k1gInSc7	čas
pomocí	pomocí	k7c2	pomocí
alternativních	alternativní	k2eAgInPc2d1	alternativní
přenosových	přenosový	k2eAgInPc2d1	přenosový
systémů	systém	k1gInPc2	systém
jako	jako	k8xS	jako
UUCP	UUCP	kA	UUCP
a	a	k8xC	a
e-mailový	eailový	k2eAgInSc1d1	e-mailový
systém	systém	k1gInSc1	systém
VNET	VNET	kA	VNET
od	od	k7c2	od
IBM	IBM	kA	IBM
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
nebyly	být	k5eNaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
počítače	počítač	k1gInPc1	počítač
nebo	nebo	k8xC	nebo
sítě	síť	k1gFnPc1	síť
navzájem	navzájem	k6eAd1	navzájem
síťově	síťově	k6eAd1	síťově
propojené	propojený	k2eAgFnPc1d1	propojená
<g/>
,	,	kIx,	,
e-mailové	eailové	k2eAgFnPc1d1	e-mailové
adresy	adresa	k1gFnPc1	adresa
musely	muset	k5eAaImAgFnP	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
"	"	kIx"	"
<g/>
cestu	cesta	k1gFnSc4	cesta
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
trasu	trasa	k1gFnSc4	trasa
mezi	mezi	k7c7	mezi
počítačem	počítač	k1gInSc7	počítač
odesílatele	odesílatel	k1gMnSc2	odesílatel
a	a	k8xC	a
příjemce	příjemce	k1gMnSc2	příjemce
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
posílat	posílat	k5eAaImF	posílat
e-maily	eail	k1gInPc4	e-mail
mezi	mezi	k7c7	mezi
více	hodně	k6eAd2	hodně
sítěmi	síť	k1gFnPc7	síť
<g/>
.	.	kIx.	.
</s>
<s>
Cestu	cesta	k1gFnSc4	cesta
specifikovala	specifikovat	k5eAaBmAgFnS	specifikovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
bang	bang	k1gMnSc1	bang
path	path	k1gMnSc1	path
<g/>
"	"	kIx"	"
adresa	adresa	k1gFnSc1	adresa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
již	již	k6eAd1	již
specifikovala	specifikovat	k5eAaBmAgFnS	specifikovat
skoky	skok	k1gInPc4	skok
(	(	kIx(	(
<g/>
hops	hops	k0	hops
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
lokacemi	lokace	k1gFnPc7	lokace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
považované	považovaný	k2eAgInPc1d1	považovaný
za	za	k7c4	za
dostupné	dostupný	k2eAgNnSc4d1	dostupné
adresátovi	adresátův	k2eAgMnPc5d1	adresátův
<g/>
.	.	kIx.	.
</s>
<s>
Nazývala	nazývat	k5eAaImAgFnS	nazývat
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
skok	skok	k1gInSc4	skok
znak	znak	k1gInSc1	znak
"	"	kIx"	"
<g/>
bang	bang	k1gInSc1	bang
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
například	například	k6eAd1	například
cesta	cesta	k1gFnSc1	cesta
...	...	k?	...
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
bigsite	bigsit	k1gInSc5	bigsit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
foovax	foovax	k1gInSc1	foovax
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
barbox	barbox	k1gInSc1	barbox
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
ja	ja	k?	ja
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pošta	pošta	k1gFnSc1	pošta
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
směrovat	směrovat	k5eAaImF	směrovat
stroji	stroj	k1gInSc3	stroj
bigsite	bigsit	k1gInSc5	bigsit
(	(	kIx(	(
<g/>
předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
dobře	dobře	k6eAd1	dobře
známá	známý	k2eAgFnSc1d1	známá
lokace	lokace	k1gFnSc1	lokace
přístupná	přístupný	k2eAgFnSc1d1	přístupná
každému	každý	k3xTgNnSc3	každý
<g/>
)	)	kIx)	)
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
přes	přes	k7c4	přes
stroj	stroj	k1gInSc4	stroj
foovax	foovax	k1gInSc1	foovax
uživatelskému	uživatelský	k2eAgInSc3d1	uživatelský
účtu	účet	k1gInSc3	účet
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
barbox	barbox	k1gInSc1	barbox
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
mailové	mailový	k2eAgInPc4d1	mailový
programy	program	k1gInPc4	program
s	s	k7c7	s
automatickým	automatický	k2eAgNnSc7d1	automatické
směrováním	směrování	k1gNnSc7	směrování
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
často	často	k6eAd1	často
uveřejňovali	uveřejňovat	k5eAaImAgMnP	uveřejňovat
složené	složený	k2eAgInPc4d1	složený
bang	bang	k1gInSc4	bang
adresy	adresa	k1gFnSc2	adresa
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
{	{	kIx(	{
}	}	kIx)	}
konvence	konvence	k1gFnPc4	konvence
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Glob	Glob	k1gInSc1	Glob
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c7	mezi
složenými	složený	k2eAgFnPc7d1	složená
závorkami	závorka	k1gFnPc7	závorka
byly	být	k5eAaImAgFnP	být
uvedené	uvedený	k2eAgFnPc4d1	uvedená
cesty	cesta	k1gFnPc4	cesta
některých	některý	k3yIgInPc2	některý
velkých	velký	k2eAgInPc2d1	velký
strojů	stroj	k1gInPc2	stroj
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
potenciální	potenciální	k2eAgMnSc1d1	potenciální
odesílatel	odesílatel	k1gMnSc1	odesílatel
bude	být	k5eAaImBp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
spolehlivě	spolehlivě	k6eAd1	spolehlivě
doručit	doručit	k5eAaPmF	doručit
poštu	pošta	k1gFnSc4	pošta
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
...	...	k?	...
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
seismo	seismo	k6eAd1	seismo
<g/>
,	,	kIx,	,
ut-sally	utall	k1gInPc1	ut-sall
<g/>
,	,	kIx,	,
ihnp	ihnp	k1gInSc1	ihnp
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
rice	rice	k6eAd1	rice
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
beta	beta	k1gNnSc1	beta
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
gamma	gamma	k1gFnSc1	gamma
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
ja	ja	k?	ja
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bang	Bang	k1gInSc1	Bang
cesty	cesta	k1gFnSc2	cesta
s	s	k7c7	s
8	[number]	k4	8
až	až	k8xS	až
10	[number]	k4	10
skoky	skok	k1gInPc7	skok
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
ničím	ničit	k5eAaImIp1nS	ničit
výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
<g/>
.	.	kIx.	.
</s>
<s>
Vytáčená	vytáčený	k2eAgFnSc1d1	vytáčená
UUCP	UUCP	kA	UUCP
spojení	spojení	k1gNnSc4	spojení
pracující	pracující	k1gFnSc2	pracující
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
týden	týden	k1gInSc4	týden
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
přenosy	přenos	k1gInPc4	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Bang	Bang	k1gInSc1	Bang
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
volily	volit	k5eAaImAgInP	volit
často	často	k6eAd1	často
podle	podle	k7c2	podle
času	čas	k1gInSc2	čas
přenosu	přenos	k1gInSc2	přenos
a	a	k8xC	a
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
zprávy	zpráva	k1gFnPc1	zpráva
často	často	k6eAd1	často
ztrácely	ztrácet	k5eAaImAgFnP	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
E-mailová	Eailový	k2eAgFnSc1d1	E-mailová
adresa	adresa	k1gFnSc1	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
počítači	počítač	k1gInPc7	počítač
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
se	se	k3xPyFc4	se
vyměňují	vyměňovat	k5eAaImIp3nP	vyměňovat
zprávy	zpráva	k1gFnPc1	zpráva
pomocí	pomocí	k7c2	pomocí
Simple	Simple	k1gFnSc2	Simple
Mail	mail	k1gInSc1	mail
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocola	k1gFnPc2	Protocola
a	a	k8xC	a
softwaru	software	k1gInSc2	software
typu	typ	k1gInSc2	typ
MTA	mta	k0	mta
jako	jako	k8xS	jako
např.	např.	kA	např.
Sendmail	Sendmail	k1gInSc1	Sendmail
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
mívají	mívat	k5eAaImIp3nP	mívat
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
počítači	počítač	k1gInSc6	počítač
nainstalován	nainstalován	k2eAgInSc1d1	nainstalován
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
e-mailový	eailový	k2eAgMnSc1d1	e-mailový
klient	klient	k1gMnSc1	klient
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
stahuje	stahovat	k5eAaImIp3nS	stahovat
zprávy	zpráva	k1gFnPc4	zpráva
z	z	k7c2	z
poštovního	poštovní	k2eAgInSc2d1	poštovní
serveru	server	k1gInSc2	server
použitím	použití	k1gNnSc7	použití
protokolů	protokol	k1gInPc2	protokol
POP	pop	k1gInSc1	pop
nebo	nebo	k8xC	nebo
IMAP	IMAP	kA	IMAP
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
velkých	velký	k2eAgFnPc2d1	velká
společností	společnost	k1gFnPc2	společnost
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
použití	použití	k1gNnSc4	použití
některého	některý	k3yIgInSc2	některý
komerčního	komerční	k2eAgInSc2d1	komerční
protokolu	protokol	k1gInSc2	protokol
jako	jako	k8xC	jako
např.	např.	kA	např.
Lotus	Lotus	kA	Lotus
Notes	notes	k1gInSc4	notes
nebo	nebo	k8xC	nebo
Microsoft	Microsoft	kA	Microsoft
Exchange	Exchange	k1gInSc1	Exchange
Server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ukládat	ukládat	k5eAaImF	ukládat
e-maily	eail	k1gInPc4	e-mail
buď	buď	k8xC	buď
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
klienta	klient	k1gMnSc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgInPc1d1	standardní
formáty	formát	k1gInPc1	formát
pro	pro	k7c4	pro
mailové	mailový	k2eAgFnPc4d1	mailová
schránky	schránka	k1gFnPc4	schránka
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Maildir	Maildir	k1gInSc4	Maildir
a	a	k8xC	a
mbox	mbox	k1gInSc4	mbox
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
e-mailových	eailův	k2eAgMnPc2d1	e-mailův
klientů	klient	k1gMnPc2	klient
používá	používat	k5eAaImIp3nS	používat
vlastní	vlastní	k2eAgInSc4d1	vlastní
formát	formát	k1gInSc4	formát
a	a	k8xC	a
na	na	k7c4	na
konverzi	konverze	k1gFnSc4	konverze
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
formáty	formát	k1gInPc7	formát
je	být	k5eAaImIp3nS	být
potřebný	potřebný	k2eAgInSc1d1	potřebný
speciální	speciální	k2eAgInSc1d1	speciální
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
uživatelé	uživatel	k1gMnPc1	uživatel
nepoužívají	používat	k5eNaImIp3nP	používat
e-mailového	eailový	k2eAgMnSc4d1	e-mailový
klienta	klient	k1gMnSc4	klient
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přistupují	přistupovat	k5eAaImIp3nP	přistupovat
ke	k	k7c3	k
zprávám	zpráva	k1gFnPc3	zpráva
umístěným	umístěný	k2eAgFnPc3d1	umístěná
na	na	k7c6	na
poštovním	poštovní	k2eAgInSc6d1	poštovní
serveru	server	k1gInSc6	server
přes	přes	k7c4	přes
webové	webový	k2eAgNnSc4d1	webové
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
u	u	k7c2	u
freemailových	freemailův	k2eAgFnPc2d1	freemailův
(	(	kIx(	(
<g/>
bezplatných	bezplatný	k2eAgFnPc2d1	bezplatná
<g/>
)	)	kIx)	)
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
posílání	posílání	k1gNnSc6	posílání
pošty	pošta	k1gFnSc2	pošta
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zaručen	zaručit	k5eAaPmNgInS	zaručit
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
přenos	přenos	k1gInSc1	přenos
zprávy	zpráva	k1gFnSc2	zpráva
i	i	k8xC	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dočasného	dočasný	k2eAgInSc2d1	dočasný
výpadku	výpadek	k1gInSc2	výpadek
cílového	cílový	k2eAgInSc2d1	cílový
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
programu	program	k1gInSc2	program
typu	typ	k1gInSc2	typ
e-mailového	eailový	k2eAgMnSc4d1	e-mailový
klienta	klient	k1gMnSc4	klient
nebo	nebo	k8xC	nebo
v	v	k7c6	v
obdobném	obdobný	k2eAgInSc6d1	obdobný
formuláři	formulář	k1gInSc6	formulář
webového	webový	k2eAgNnSc2d1	webové
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
pomocí	pomocí	k7c2	pomocí
Simple	Simple	k1gFnSc2	Simple
Mail	mail	k1gInSc1	mail
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
SMTP	SMTP	kA	SMTP
<g/>
)	)	kIx)	)
pošle	poslat	k5eAaPmIp3nS	poslat
zprávu	zpráva	k1gFnSc4	zpráva
programu	program	k1gInSc2	program
Mail	mail	k1gInSc1	mail
Transfer	transfer	k1gInSc1	transfer
Agent	agent	k1gMnSc1	agent
(	(	kIx(	(
<g/>
MTA	mta	k0	mta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
smtp	smtp	k1gInSc1	smtp
<g/>
.	.	kIx.	.
<g/>
a.	a.	k?	a.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
běžet	běžet	k5eAaImF	běžet
buď	buď	k8xC	buď
na	na	k7c6	na
samostatném	samostatný	k2eAgInSc6d1	samostatný
smtp	smtp	k1gMnSc1	smtp
poštovním	poštovní	k1gMnSc7	poštovní
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
odesílatele	odesílatel	k1gMnSc2	odesílatel
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
MTA	mta	k0	mta
zjistí	zjistit	k5eAaPmIp3nS	zjistit
z	z	k7c2	z
uvedených	uvedený	k2eAgFnPc2d1	uvedená
cílových	cílový	k2eAgFnPc2d1	cílová
adres	adresa	k1gFnPc2	adresa
název	název	k1gInSc4	název
domény	doména	k1gFnPc4	doména
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
adresy	adresa	k1gFnSc2	adresa
za	za	k7c7	za
zavináčem	zavináč	k1gInSc7	zavináč
<g/>
)	)	kIx)	)
a	a	k8xC	a
tyto	tento	k3xDgFnPc4	tento
domény	doména	k1gFnPc4	doména
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
v	v	k7c4	v
Domain	Domain	k1gInSc4	Domain
Name	Name	k1gNnSc2	Name
System	Syst	k1gMnSc7	Syst
(	(	kIx(	(
<g/>
DNS	DNS	kA	DNS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjistil	zjistit	k5eAaPmAgInS	zjistit
mail	mail	k1gInSc1	mail
exchange	exchang	k1gFnSc2	exchang
servery	server	k1gInPc1	server
přijímající	přijímající	k2eAgFnSc4d1	přijímající
poštu	pošta	k1gFnSc4	pošta
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
doménu	doména	k1gFnSc4	doména
<g/>
.	.	kIx.	.
</s>
<s>
DNS	DNS	kA	DNS
server	server	k1gInSc1	server
domény	doména	k1gFnSc2	doména
b.	b.	k?	b.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ns	ns	k?	ns
<g/>
.	.	kIx.	.
<g/>
b.	b.	k?	b.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
MX	MX	kA	MX
záznamem	záznam	k1gInSc7	záznam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvede	uvést	k5eAaPmIp3nS	uvést
mail	mail	k1gInSc1	mail
exchange	exchange	k6eAd1	exchange
server	server	k1gInSc1	server
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
doménu	doména	k1gFnSc4	doména
<g/>
.	.	kIx.	.
</s>
<s>
MTA	mta	k0	mta
server	server	k1gInSc1	server
(	(	kIx(	(
<g/>
např.	např.	kA	např.
smtp	smtp	k1gInSc1	smtp
<g/>
.	.	kIx.	.
<g/>
a.	a.	k?	a.
<g/>
org	org	k?	org
<g/>
)	)	kIx)	)
odešle	odeslat	k5eAaPmIp3nS	odeslat
zprávu	zpráva	k1gFnSc4	zpráva
na	na	k7c4	na
mail	mail	k1gInSc4	mail
exchange	exchangat	k5eAaPmIp3nS	exchangat
server	server	k1gInSc1	server
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mx	mx	k?	mx
<g/>
.	.	kIx.	.
<g/>
b.	b.	k?	b.
<g/>
org	org	k?	org
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
protokolu	protokol	k1gInSc2	protokol
SMTP	SMTP	kA	SMTP
<g/>
.	.	kIx.	.
</s>
<s>
Domény	doména	k1gFnPc1	doména
obvykle	obvykle	k6eAd1	obvykle
mají	mít	k5eAaImIp3nP	mít
záložní	záložní	k2eAgNnSc4d1	záložní
(	(	kIx(	(
<g/>
backup	backup	k1gInSc4	backup
<g/>
)	)	kIx)	)
mail	mail	k1gInSc1	mail
exchange	exchange	k1gNnSc1	exchange
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
můžou	můžou	k?	můžou
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
přijímaní	přijímaný	k2eAgMnPc1d1	přijímaný
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
hlavní	hlavní	k2eAgInSc1d1	hlavní
mail	mail	k1gInSc1	mail
exchange	exchangat	k5eAaPmIp3nS	exchangat
server	server	k1gInSc4	server
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zprávu	zpráva	k1gFnSc4	zpráva
doručit	doručit	k5eAaPmF	doručit
<g/>
,	,	kIx,	,
MTA	mta	k0	mta
příjemce	příjemce	k1gMnSc2	příjemce
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
musí	muset	k5eAaImIp3nP	muset
odeslat	odeslat	k5eAaPmF	odeslat
zpět	zpět	k6eAd1	zpět
odesílateli	odesílatel	k1gMnSc3	odesílatel
zprávu	zpráva	k1gFnSc4	zpráva
(	(	kIx(	(
<g/>
bounce	bounec	k1gInPc1	bounec
message	messag	k1gFnSc2	messag
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Mail	mail	k1gInSc1	mail
exchange	exchangat	k5eAaPmIp3nS	exchangat
server	server	k1gInSc1	server
zprávu	zpráva	k1gFnSc4	zpráva
doručí	doručit	k5eAaPmIp3nS	doručit
do	do	k7c2	do
schránky	schránka	k1gFnSc2	schránka
adresáta	adresát	k1gMnSc2	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
schránky	schránka	k1gFnSc2	schránka
adresáta	adresát	k1gMnSc4	adresát
si	se	k3xPyFc3	se
zprávu	zpráva	k1gFnSc4	zpráva
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
pomocí	pomocí	k7c2	pomocí
protokolu	protokol	k1gInSc2	protokol
POP	pop	k1gMnSc1	pop
(	(	kIx(	(
<g/>
POP	pop	k1gMnSc1	pop
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
IMAP	IMAP	kA	IMAP
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
adresátovi	adresátův	k2eAgMnPc1d1	adresátův
umožní	umožnit	k5eAaPmIp3nP	umožnit
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
poštovní	poštovní	k2eAgMnSc1d1	poštovní
klient	klient	k1gMnSc1	klient
příjemce	příjemce	k1gMnSc1	příjemce
nebo	nebo	k8xC	nebo
webová	webový	k2eAgFnSc1d1	webová
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Bývalo	bývat	k5eAaImAgNnS	bývat
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
že	že	k8xS	že
kterýkoliv	kterýkoliv	k3yIgInSc1	kterýkoliv
MTA	mta	k0	mta
přijímal	přijímat	k5eAaImAgMnS	přijímat
zprávy	zpráva	k1gFnSc2	zpráva
pro	pro	k7c4	pro
kteréhokoli	kterýkoli	k3yIgMnSc4	kterýkoli
uživatele	uživatel	k1gMnSc4	uživatel
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zprávu	zpráva	k1gFnSc4	zpráva
doručil	doručit	k5eAaPmAgMnS	doručit
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
MTA	mta	k0	mta
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
open	open	k1gInSc4	open
mail	mail	k1gInSc1	mail
relays	relays	k1gInSc1	relays
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgNnP	být
síťová	síťový	k2eAgNnPc1d1	síťové
spojení	spojení	k1gNnPc1	spojení
nespolehlivá	spolehlivý	k2eNgNnPc1d1	nespolehlivé
a	a	k8xC	a
nepermanentní	permanentní	k2eNgNnPc1d1	permanentní
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
MTA	mta	k0	mta
nemohl	moct	k5eNaImAgMnS	moct
doručit	doručit	k5eAaPmF	doručit
zprávu	zpráva	k1gFnSc4	zpráva
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
ji	on	k3xPp3gFnSc4	on
alespoň	alespoň	k9	alespoň
poslat	poslat	k5eAaPmF	poslat
agentovi	agent	k1gMnSc3	agent
bližšímu	bližší	k1gNnSc3	bližší
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
větší	veliký	k2eAgFnSc4d2	veliký
šanci	šance	k1gFnSc4	šance
ji	on	k3xPp3gFnSc4	on
doručit	doručit	k5eAaPmF	doručit
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
mechanizmus	mechanizmus	k1gInSc1	mechanizmus
byl	být	k5eAaImAgInS	být
zneužitelný	zneužitelný	k2eAgInSc1d1	zneužitelný
lidmi	člověk	k1gMnPc7	člověk
posílající	posílající	k2eAgFnSc4d1	posílající
nevyžádanou	vyžádaný	k2eNgFnSc4d1	nevyžádaná
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
poštu	pošta	k1gFnSc4	pošta
(	(	kIx(	(
<g/>
spam	spam	k6eAd1	spam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
ze	z	k7c2	z
současných	současný	k2eAgInPc2d1	současný
MTA	mta	k0	mta
jsou	být	k5eAaImIp3nP	být
open	open	k1gInSc4	open
mail	mail	k1gInSc1	mail
relays	relays	k1gInSc1	relays
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
přijímají	přijímat	k5eAaImIp3nP	přijímat
poštu	pošta	k1gFnSc4	pošta
pro	pro	k7c4	pro
známé	známý	k2eAgMnPc4d1	známý
uživatele	uživatel	k1gMnPc4	uživatel
resp.	resp.	kA	resp.
domény	doména	k1gFnSc2	doména
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
open	open	k1gNnSc4	open
relays	relaysa	k1gFnPc2	relaysa
jsou	být	k5eAaImIp3nP	být
rychle	rychle	k6eAd1	rychle
odhalené	odhalený	k2eAgMnPc4d1	odhalený
a	a	k8xC	a
zneužité	zneužitý	k2eAgMnPc4d1	zneužitý
spammery	spammer	k1gMnPc4	spammer
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
neustále	neustále	k6eAd1	neustále
prohledávají	prohledávat	k5eAaImIp3nP	prohledávat
(	(	kIx(	(
<g/>
skenují	skenovat	k5eAaImIp3nP	skenovat
<g/>
)	)	kIx)	)
IP	IP	kA	IP
rozsahy	rozsah	k1gInPc1	rozsah
celého	celý	k2eAgInSc2d1	celý
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
e-mail	eail	k1gInSc4	e-mail
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
přenos	přenos	k1gInSc1	přenos
7	[number]	k4	7
<g/>
bitové	bitový	k2eAgFnPc4d1	bitová
ASCII	ascii	kA	ascii
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
e-mailových	eailův	k2eAgInPc2d1	e-mailův
přenosů	přenos	k1gInPc2	přenos
8	[number]	k4	8
<g/>
bitových	bitový	k2eAgFnPc2d1	bitová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k8xC	ale
nelze	lze	k6eNd1	lze
zaručit	zaručit	k5eAaPmF	zaručit
bezproblémovost	bezproblémovost	k1gFnSc4	bezproblémovost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byla	být	k5eAaImAgFnS	být
elektronická	elektronický	k2eAgFnSc1d1	elektronická
pošta	pošta	k1gFnSc1	pošta
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
standard	standard	k1gInSc4	standard
MIME	mim	k1gMnSc5	mim
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
kódování	kódování	k1gNnSc1	kódování
vkládaných	vkládaný	k2eAgInPc2d1	vkládaný
HTML	HTML	kA	HTML
a	a	k8xC	a
binárních	binární	k2eAgFnPc2d1	binární
příloh	příloha	k1gFnPc2	příloha
<g/>
,	,	kIx,	,
obrázků	obrázek	k1gInPc2	obrázek
<g/>
,	,	kIx,	,
zvuků	zvuk	k1gInPc2	zvuk
a	a	k8xC	a
videí	video	k1gNnPc2	video
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
sled	sled	k1gInSc1	sled
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Jana	Jana	k1gFnSc1	Jana
napíše	napsat	k5eAaPmIp3nS	napsat
e-mail	eail	k1gInSc4	e-mail
Petrovi	Petr	k1gMnSc3	Petr
<g/>
:	:	kIx,	:
Jana	Jana	k1gFnSc1	Jana
napíše	napsat	k5eAaPmIp3nS	napsat
zprávu	zpráva	k1gFnSc4	zpráva
pomocí	pomocí	k7c2	pomocí
svého	svůj	k3xOyFgMnSc2	svůj
e-mailového	eailový	k2eAgMnSc2d1	e-mailový
klienta	klient	k1gMnSc2	klient
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
formuláři	formulář	k1gInSc6	formulář
webového	webový	k2eAgNnSc2d1	webové
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
také	také	k9	také
napíše	napsat	k5eAaPmIp3nS	napsat
nebo	nebo	k8xC	nebo
zvolí	zvolit	k5eAaPmIp3nS	zvolit
z	z	k7c2	z
adresáře	adresář	k1gInSc2	adresář
e-mailovou	eailový	k2eAgFnSc4d1	e-mailová
adresu	adresa	k1gFnSc4	adresa
svého	svůj	k3xOyFgMnSc2	svůj
adresáta	adresát	k1gMnSc2	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
klikne	kliknout	k5eAaPmIp3nS	kliknout
na	na	k7c4	na
tlačítko	tlačítko	k1gNnSc4	tlačítko
"	"	kIx"	"
<g/>
Odeslat	odeslat	k5eAaPmF	odeslat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Programy	program	k1gInPc4	program
poštovní	poštovní	k1gMnSc1	poštovní
klient	klient	k1gMnSc1	klient
<g/>
,	,	kIx,	,
MTA	mta	k0	mta
server	server	k1gInSc1	server
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
informace	informace	k1gFnSc2	informace
od	od	k7c2	od
DNS	DNS	kA	DNS
serveru	server	k1gInSc2	server
a	a	k8xC	a
mail	mail	k1gInSc4	mail
exchange	exchangat	k5eAaPmIp3nS	exchangat
server	server	k1gInSc1	server
a	a	k8xC	a
poštovní	poštovní	k2eAgMnSc1d1	poštovní
klient	klient	k1gMnSc1	klient
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
adresáta	adresát	k1gMnSc2	adresát
se	se	k3xPyFc4	se
postarají	postarat	k5eAaPmIp3nP	postarat
o	o	k7c4	o
doručení	doručení	k1gNnSc4	doručení
zprávy	zpráva	k1gFnSc2	zpráva
adresátovi	adresát	k1gMnSc3	adresát
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
odesílateli	odesílatel	k1gMnPc7	odesílatel
(	(	kIx(	(
<g/>
Janě	Jana	k1gFnSc3	Jana
<g/>
)	)	kIx)	)
zašlou	zaslat	k5eAaPmIp3nP	zaslat
chybovou	chybový	k2eAgFnSc4d1	chybová
<g/>
,	,	kIx,	,
varovnou	varovný	k2eAgFnSc4d1	varovná
nebo	nebo	k8xC	nebo
potvrzující	potvrzující	k2eAgFnSc4d1	potvrzující
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
klikne	kliknout	k5eAaPmIp3nS	kliknout
na	na	k7c4	na
tlačítko	tlačítko	k1gNnSc4	tlačítko
"	"	kIx"	"
<g/>
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
novou	nový	k2eAgFnSc4d1	nová
poštu	pošta	k1gFnSc4	pošta
<g/>
"	"	kIx"	"
ve	v	k7c6	v
svém	svůj	k3xOyFgMnSc6	svůj
e-mailovém	eailový	k2eAgMnSc6d1	e-mailový
klientu	klient	k1gMnSc6	klient
(	(	kIx(	(
<g/>
popřípadě	popřípadě	k6eAd1	popřípadě
má	mít	k5eAaImIp3nS	mít
poštovní	poštovní	k2eAgMnSc1d1	poštovní
klient	klient	k1gMnSc1	klient
nastavený	nastavený	k2eAgMnSc1d1	nastavený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
program	program	k1gInSc1	program
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
poštu	pošta	k1gFnSc4	pošta
stahuje	stahovat	k5eAaImIp3nS	stahovat
sám	sám	k3xTgMnSc1	sám
<g/>
)	)	kIx)	)
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
vyzvedne	vyzvednout	k5eAaPmIp3nS	vyzvednout
poštu	pošta	k1gFnSc4	pošta
pomocí	pomocí	k7c2	pomocí
protokolu	protokol	k1gInSc2	protokol
POP	pop	k1gMnSc1	pop
(	(	kIx(	(
<g/>
POP	pop	k1gMnSc1	pop
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Popřípadě	popřípadě	k6eAd1	popřípadě
může	moct	k5eAaImIp3nS	moct
Petr	Petr	k1gMnSc1	Petr
pomocí	pomocí	k7c2	pomocí
webové	webový	k2eAgFnSc2d1	webová
služby	služba	k1gFnSc2	služba
nebo	nebo	k8xC	nebo
příslušně	příslušně	k6eAd1	příslušně
nastaveného	nastavený	k2eAgMnSc2d1	nastavený
poštovního	poštovní	k1gMnSc2	poštovní
klienta	klient	k1gMnSc2	klient
prohlížet	prohlížet	k5eAaImF	prohlížet
doručené	doručený	k2eAgInPc4d1	doručený
e-maily	eail	k1gInPc4	e-mail
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
schránce	schránka	k1gFnSc6	schránka
na	na	k7c4	na
mail	mail	k1gInSc4	mail
exchange	exchang	k1gFnSc2	exchang
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Internetové	internetový	k2eAgFnPc1d1	internetová
e-mailové	eailové	k2eAgFnPc1d1	e-mailové
zprávy	zpráva	k1gFnPc1	zpráva
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Hlavička	hlavička	k1gFnSc1	hlavička
–	–	k?	–
předmět	předmět	k1gInSc4	předmět
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
odesílatel	odesílatel	k1gMnSc1	odesílatel
<g/>
,	,	kIx,	,
příjemce	příjemce	k1gMnSc1	příjemce
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
e-mailu	eail	k1gInSc6	e-mail
Tělo	tělo	k1gNnSc1	tělo
–	–	k?	–
Samotná	samotný	k2eAgFnSc1d1	samotná
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
obyčejně	obyčejně	k6eAd1	obyčejně
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
blok	blok	k1gInSc1	blok
s	s	k7c7	s
podpisem	podpis	k1gInSc7	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
e-mailu	eail	k1gInSc2	e-mail
začíná	začínat	k5eAaImIp3nS	začínat
zpravidla	zpravidla	k6eAd1	zpravidla
pozdravem	pozdrav	k1gInSc7	pozdrav
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
e-mailu	eail	k1gInSc3	e-mail
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přikládat	přikládat	k5eAaImF	přikládat
jako	jako	k9	jako
přílohy	příloh	k1gInPc4	příloh
i	i	k8xC	i
obrázky	obrázek	k1gInPc4	obrázek
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
soubory	soubor	k1gInPc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
bývá	bývat	k5eAaImIp3nS	bývat
doručování	doručování	k1gNnSc1	doručování
menších	malý	k2eAgInPc2d2	menší
souborů	soubor	k1gInPc2	soubor
typu	typ	k1gInSc2	typ
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
zprávě	zpráva	k1gFnSc3	zpráva
přiložen	přiložen	k2eAgInSc4d1	přiložen
velký	velký	k2eAgInSc4d1	velký
soubor	soubor	k1gInSc4	soubor
nebo	nebo	k8xC	nebo
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
souborů	soubor	k1gInPc2	soubor
nebo	nebo	k8xC	nebo
soubor	soubor	k1gInSc1	soubor
typu	typ	k1gInSc2	typ
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
infikován	infikovat	k5eAaBmNgInS	infikovat
virem	vir	k1gInSc7	vir
nebo	nebo	k8xC	nebo
červem	červ	k1gMnSc7	červ
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
taková	takový	k3xDgFnSc1	takový
zpráva	zpráva	k1gFnSc1	zpráva
neprojde	projít	k5eNaPmIp3nS	projít
ochrannými	ochranný	k2eAgInPc7d1	ochranný
filtry	filtr	k1gInPc7	filtr
na	na	k7c6	na
doručovací	doručovací	k2eAgFnSc6d1	doručovací
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavičky	hlavička	k1gFnPc1	hlavička
obvykle	obvykle	k6eAd1	obvykle
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
alespoň	alespoň	k9	alespoň
4	[number]	k4	4
pole	pole	k1gNnSc2	pole
<g/>
:	:	kIx,	:
Od	od	k7c2	od
(	(	kIx(	(
<g/>
From	Froma	k1gFnPc2	Froma
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
e-mailová	eailový	k2eAgFnSc1d1	e-mailová
adresa	adresa	k1gFnSc1	adresa
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
i	i	k9	i
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
odesílatele	odesílatel	k1gMnSc2	odesílatel
zprávy	zpráva	k1gFnSc2	zpráva
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
program	program	k1gInSc4	program
automaticky	automaticky	k6eAd1	automaticky
<g/>
)	)	kIx)	)
Komu	kdo	k3yRnSc3	kdo
(	(	kIx(	(
<g/>
To	to	k9	to
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
e-mailová	eailový	k2eAgFnSc1d1	e-mailová
adresa	adresa	k1gFnSc1	adresa
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
i	i	k9	i
jméno	jméno	k1gNnSc4	jméno
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
příjemce	příjemce	k1gMnSc4	příjemce
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
adresátů	adresát	k1gMnPc2	adresát
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
současně	současně	k6eAd1	současně
(	(	kIx(	(
<g/>
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
odesílatel	odesílatel	k1gMnSc1	odesílatel
<g/>
)	)	kIx)	)
Předmět	předmět	k1gInSc1	předmět
(	(	kIx(	(
<g/>
Subject	Subject	k1gInSc1	Subject
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
stručný	stručný	k2eAgInSc4d1	stručný
popis	popis	k1gInSc4	popis
obsahu	obsah	k1gInSc2	obsah
zprávy	zpráva	k1gFnSc2	zpráva
(	(	kIx(	(
<g/>
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
odesílatel	odesílatel	k1gMnSc1	odesílatel
<g/>
,	,	kIx,	,
nepovinně	povinně	k6eNd1	povinně
<g/>
)	)	kIx)	)
Datum	datum	k1gNnSc1	datum
(	(	kIx(	(
<g/>
Date	Date	k1gFnSc1	Date
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
místní	místní	k2eAgNnSc4d1	místní
datum	datum	k1gNnSc4	datum
a	a	k8xC	a
čas	čas	k1gInSc4	čas
<g />
.	.	kIx.	.
</s>
<s>
odeslání	odeslání	k1gNnSc1	odeslání
zprávy	zpráva	k1gFnSc2	zpráva
(	(	kIx(	(
<g/>
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
program	program	k1gInSc4	program
automaticky	automaticky	k6eAd1	automaticky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
v	v	k7c6	v
hlavičce	hlavička	k1gFnSc6	hlavička
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
příjemce	příjemce	k1gMnSc1	příjemce
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgNnPc4d1	podobné
záhlaví	záhlaví	k1gNnPc4	záhlaví
na	na	k7c6	na
konvenčním	konvenční	k2eAgInSc6d1	konvenční
dopisu	dopis	k1gInSc6	dopis
–	–	k?	–
skutečná	skutečný	k2eAgFnSc1d1	skutečná
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
komu	kdo	k3yRnSc3	kdo
byla	být	k5eAaImAgFnS	být
zpráva	zpráva	k1gFnSc1	zpráva
adresována	adresovat	k5eAaBmNgFnS	adresovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odstraněná	odstraněný	k2eAgNnPc4d1	odstraněné
mailovým	mailový	k2eAgInSc7d1	mailový
serverem	server	k1gInSc7	server
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
správné	správný	k2eAgFnSc3d1	správná
mailové	mailový	k2eAgFnSc3d1	mailová
schránce	schránka	k1gFnSc3	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
"	"	kIx"	"
<g/>
Od	od	k7c2	od
<g/>
"	"	kIx"	"
nemusí	muset	k5eNaImIp3nP	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
adresu	adresa	k1gFnSc4	adresa
skutečného	skutečný	k2eAgMnSc2d1	skutečný
odesílatele	odesílatel	k1gMnSc2	odesílatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
to	ten	k3xDgNnSc4	ten
zfalšovat	zfalšovat	k5eAaPmF	zfalšovat
a	a	k8xC	a
zpráva	zpráva	k1gFnSc1	zpráva
potom	potom	k6eAd1	potom
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přišla	přijít	k5eAaPmAgFnS	přijít
z	z	k7c2	z
uvedené	uvedený	k2eAgFnSc2d1	uvedená
adresy	adresa	k1gFnSc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
e-mail	eail	k1gInSc4	e-mail
digitálně	digitálně	k6eAd1	digitálně
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
od	od	k7c2	od
koho	kdo	k3yQnSc2	kdo
zpráva	zpráva	k1gFnSc1	zpráva
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
běžné	běžný	k2eAgFnPc1d1	běžná
součásti	součást	k1gFnPc1	součást
hlavičky	hlavička	k1gFnSc2	hlavička
<g/>
:	:	kIx,	:
Kopie	kopie	k1gFnSc1	kopie
(	(	kIx(	(
<g/>
Cc	Cc	k1gFnSc1	Cc
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
carbon	carbon	k1gInSc1	carbon
copy	cop	k1gInPc1	cop
–	–	k?	–
kopie	kopie	k1gFnSc2	kopie
(	(	kIx(	(
<g/>
carbon	carbon	k1gInSc1	carbon
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
psací	psací	k2eAgInPc1d1	psací
stroje	stroj	k1gInPc1	stroj
používají	používat	k5eAaImIp3nP	používat
"	"	kIx"	"
<g/>
kopírák	kopírák	k1gInSc1	kopírák
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
carbon	carbon	k1gMnSc1	carbon
paper	paper	k1gMnSc1	paper
<g/>
)	)	kIx)	)
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
kopií	kopie	k1gFnPc2	kopie
dopisů	dopis	k1gInPc2	dopis
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
odesílatel	odesílatel	k1gMnSc1	odesílatel
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
nepovinná	povinný	k2eNgFnSc1d1	nepovinná
položka	položka	k1gFnSc1	položka
<g/>
)	)	kIx)	)
Slepá	slepý	k2eAgFnSc1d1	slepá
čili	čili	k8xC	čili
skrytá	skrytý	k2eAgFnSc1d1	skrytá
kopie	kopie	k1gFnSc1	kopie
(	(	kIx(	(
<g/>
Bcc	Bcc	k1gFnSc1	Bcc
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
blind	blind	k1gInSc1	blind
carbon	carbon	k1gInSc1	carbon
copy	cop	k1gInPc1	cop
–	–	k?	–
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
kopie	kopie	k1gFnSc1	kopie
(	(	kIx(	(
<g/>
adresát	adresát	k1gMnSc1	adresát
bude	být	k5eAaImBp3nS	být
vidět	vidět	k5eAaImF	vidět
osoby	osoba	k1gFnPc4	osoba
uvedené	uvedený	k2eAgFnPc4d1	uvedená
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
"	"	kIx"	"
<g/>
Komu	kdo	k3yInSc3	kdo
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Cc	Cc	k1gFnSc1	Cc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
adresy	adresa	k1gFnPc1	adresa
v	v	k7c6	v
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Bcc	Bcc	k?	Bcc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
odesílatel	odesílatel	k1gMnSc1	odesílatel
<g/>
,	,	kIx,	,
nepovinná	povinný	k2eNgFnSc1d1	nepovinná
položka	položka	k1gFnSc1	položka
<g/>
)	)	kIx)	)
Received	Received	k1gInSc1	Received
<g/>
:	:	kIx,	:
přijato	přijmout	k5eAaPmNgNnS	přijmout
–	–	k?	–
trasové	trasový	k2eAgFnPc1d1	trasová
informace	informace	k1gFnPc1	informace
vytvořené	vytvořený	k2eAgInPc4d1	vytvořený
servery	server	k1gInPc4	server
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
zpráva	zpráva	k1gFnSc1	zpráva
prošla	projít	k5eAaPmAgFnS	projít
(	(	kIx(	(
<g/>
automatické	automatický	k2eAgInPc1d1	automatický
zápisy	zápis	k1gInPc1	zápis
serverů	server	k1gInPc2	server
<g/>
)	)	kIx)	)
Content-type	Contentyp	k1gInSc5	Content-typ
<g/>
:	:	kIx,	:
druh	druh	k1gInSc1	druh
obsahu	obsah	k1gInSc2	obsah
–	–	k?	–
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zpráva	zpráva	k1gFnSc1	zpráva
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
MIME	mim	k1gMnSc5	mim
typ	typ	k1gInSc1	typ
(	(	kIx(	(
<g/>
automatický	automatický	k2eAgInSc1d1	automatický
zápis	zápis	k1gInSc1	zápis
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
E-mailová	Eailový	k2eAgFnSc1d1	E-mailová
adresa	adresa	k1gFnSc1	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
uživatel	uživatel	k1gMnSc1	uživatel
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
zpráv	zpráva	k1gFnPc2	zpráva
svoji	svůj	k3xOyFgFnSc4	svůj
e-mailovou	eailový	k2eAgFnSc4d1	e-mailová
adresu	adresa	k1gFnSc4	adresa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
identifikuje	identifikovat	k5eAaBmIp3nS	identifikovat
jeho	jeho	k3xOp3gFnSc4	jeho
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
poštovní	poštovní	k2eAgFnSc4d1	poštovní
schránku	schránka	k1gFnSc4	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
fyzicky	fyzicky	k6eAd1	fyzicky
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
nějakém	nějaký	k3yIgInSc6	nějaký
internetovém	internetový	k2eAgInSc6d1	internetový
serveru	server	k1gInSc6	server
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnPc1d1	populární
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
servery	server	k1gInPc1	server
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nabízejí	nabízet	k5eAaImIp3nP	nabízet
e-mailovou	eailový	k2eAgFnSc4d1	e-mailová
schránku	schránka	k1gFnSc4	schránka
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
s	s	k7c7	s
webovým	webový	k2eAgNnSc7d1	webové
rozhraním	rozhraní	k1gNnSc7	rozhraní
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
GMail	GMail	k1gInSc1	GMail
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odesílání	odesílání	k1gNnSc4	odesílání
zpráv	zpráva	k1gFnPc2	zpráva
není	být	k5eNaImIp3nS	být
vlastní	vlastní	k2eAgFnSc1d1	vlastní
e-mailová	eailový	k2eAgFnSc1d1	e-mailová
adresa	adresa	k1gFnSc1	adresa
nutná	nutný	k2eAgFnSc1d1	nutná
<g/>
,	,	kIx,	,
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
elektronické	elektronický	k2eAgFnPc4d1	elektronická
pohlednice	pohlednice	k1gFnPc4	pohlednice
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vlastní	vlastní	k2eAgFnSc2d1	vlastní
e-mailové	eailové	k2eAgFnSc2d1	e-mailové
adresy	adresa	k1gFnSc2	adresa
lze	lze	k6eAd1	lze
přes	přes	k7c4	přes
webové	webový	k2eAgNnSc4d1	webové
rozhraní	rozhraní	k1gNnSc4	rozhraní
zasílat	zasílat	k5eAaImF	zasílat
také	také	k9	také
klasické	klasický	k2eAgFnPc4d1	klasická
e-mailové	eailové	k2eAgFnPc4d1	e-mailové
zprávy	zpráva	k1gFnPc4	zpráva
i	i	k9	i
s	s	k7c7	s
přílohou	příloha	k1gFnSc7	příloha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Sendik	Sendik	k1gInSc1	Sendik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgFnPc1d1	populární
jsou	být	k5eAaImIp3nP	být
diskuzní	diskuzní	k2eAgFnPc1d1	diskuzní
skupiny	skupina	k1gFnPc1	skupina
zvané	zvaný	k2eAgFnSc2d1	zvaná
e-mailové	eailové	k2eAgFnSc2d1	e-mailové
konference	konference	k1gFnSc2	konference
(	(	kIx(	(
<g/>
mailinglisty	mailinglista	k1gMnSc2	mailinglista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
hromadná	hromadný	k2eAgFnSc1d1	hromadná
výměna	výměna	k1gFnSc1	výměna
e-mailů	eail	k1gInPc2	e-mail
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
účastníky	účastník	k1gMnPc7	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Elektronický	elektronický	k2eAgInSc4d1	elektronický
podpis	podpis	k1gInSc4	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc7d3	nejjednodušší
formou	forma	k1gFnSc7	forma
elektronického	elektronický	k2eAgInSc2d1	elektronický
podpisu	podpis	k1gInSc2	podpis
je	být	k5eAaImIp3nS	být
prosté	prostý	k2eAgNnSc1d1	prosté
uvedení	uvedení	k1gNnSc1	uvedení
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
identifikačních	identifikační	k2eAgInPc2d1	identifikační
a	a	k8xC	a
kontaktních	kontaktní	k2eAgInPc2d1	kontaktní
údajů	údaj	k1gInPc2	údaj
na	na	k7c6	na
konci	konec	k1gInSc6	konec
těla	tělo	k1gNnSc2	tělo
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
však	však	k9	však
pojmem	pojem	k1gInSc7	pojem
elektronický	elektronický	k2eAgInSc1d1	elektronický
podpis	podpis	k1gInSc1	podpis
míní	mínit	k5eAaImIp3nS	mínit
sofistikovanější	sofistikovaný	k2eAgInSc1d2	sofistikovanější
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pomocí	pomocí	k7c2	pomocí
speciálního	speciální	k2eAgInSc2d1	speciální
kódu	kód	k1gInSc2	kód
připojeného	připojený	k2eAgInSc2d1	připojený
ke	k	k7c3	k
zprávě	zpráva	k1gFnSc3	zpráva
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ověřit	ověřit	k5eAaPmF	ověřit
jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
zprávu	zpráva	k1gFnSc4	zpráva
skutečně	skutečně	k6eAd1	skutečně
odeslal	odeslat	k5eAaPmAgMnS	odeslat
(	(	kIx(	(
<g/>
samotný	samotný	k2eAgInSc4d1	samotný
údaj	údaj	k1gInSc4	údaj
v	v	k7c6	v
položce	položka	k1gFnSc6	položka
From	Froma	k1gFnPc2	Froma
totiž	totiž	k9	totiž
není	být	k5eNaImIp3nS	být
spolehlivý	spolehlivý	k2eAgMnSc1d1	spolehlivý
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsah	obsah	k1gInSc1	obsah
zprávy	zpráva	k1gFnSc2	zpráva
(	(	kIx(	(
<g/>
tělo	tělo	k1gNnSc1	tělo
zprávy	zpráva	k1gFnSc2	zpráva
a	a	k8xC	a
přílohy	příloha	k1gFnSc2	příloha
<g/>
)	)	kIx)	)
nebyl	být	k5eNaImAgMnS	být
mezi	mezi	k7c7	mezi
odesláním	odeslání	k1gNnSc7	odeslání
a	a	k8xC	a
přijetím	přijetí	k1gNnSc7	přijetí
zprávy	zpráva	k1gFnSc2	zpráva
změněn	změněn	k2eAgMnSc1d1	změněn
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
řečeno	řečen	k2eAgNnSc1d1	řečeno
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektronický	elektronický	k2eAgInSc4d1	elektronický
dokument	dokument	k1gInSc4	dokument
skutečně	skutečně	k6eAd1	skutečně
podepsala	podepsat	k5eAaPmAgFnS	podepsat
daná	daný	k2eAgFnSc1d1	daná
osoba	osoba	k1gFnSc1	osoba
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
žádným	žádný	k3yNgFnPc3	žádný
dodatečným	dodatečný	k2eAgFnPc3d1	dodatečná
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
osobních	osobní	k2eAgNnPc2d1	osobní
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
e-mail	eail	k1gInSc4	e-mail
nezaručuje	zaručovat	k5eNaImIp3nS	zaručovat
soukromí	soukromí	k1gNnSc1	soukromí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
e-mailové	eailové	k2eAgFnPc1d1	e-mailové
zprávy	zpráva	k1gFnPc1	zpráva
všeobecně	všeobecně	k6eAd1	všeobecně
nejsou	být	k5eNaImIp3nP	být
šifrované	šifrovaný	k2eAgFnPc1d1	šifrovaná
<g/>
;	;	kIx,	;
e-mailové	eailové	k2eAgFnPc1d1	e-mailové
zprávy	zpráva	k1gFnPc1	zpráva
musí	muset	k5eAaImIp3nP	muset
projít	projít	k5eAaPmF	projít
cizími	cizí	k2eAgInPc7d1	cizí
počítači	počítač	k1gInPc7	počítač
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
cílový	cílový	k2eAgInSc4d1	cílový
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
je	být	k5eAaImIp3nS	být
cestou	cesta	k1gFnSc7	cesta
zachytit	zachytit	k5eAaPmF	zachytit
a	a	k8xC	a
přečíst	přečíst	k5eAaPmF	přečíst
si	se	k3xPyFc3	se
cizí	cizí	k2eAgFnSc4d1	cizí
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
poskytovatelů	poskytovatel	k1gMnPc2	poskytovatel
internetového	internetový	k2eAgNnSc2d1	internetové
připojení	připojení	k1gNnSc2	připojení
(	(	kIx(	(
<g/>
Internet	Internet	k1gInSc1	Internet
service	service	k1gFnSc2	service
provider	providra	k1gFnPc2	providra
<g/>
)	)	kIx)	)
ukládá	ukládat	k5eAaImIp3nS	ukládat
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
servery	server	k1gInPc4	server
kopie	kopie	k1gFnSc2	kopie
vašich	váš	k3xOp2gFnPc2	váš
e-mailových	eailův	k2eAgFnPc2d1	e-mailova
zpráv	zpráva	k1gFnPc2	zpráva
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	on	k3xPp3gMnPc4	on
doručí	doručit	k5eAaPmIp3nS	doručit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zálohy	záloha	k1gFnPc1	záloha
můžou	můžou	k?	můžou
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c6	na
serveru	server	k1gInSc6	server
až	až	k9	až
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
schránce	schránka	k1gFnSc6	schránka
vymažete	vymazat	k5eAaPmIp2nP	vymazat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
kryptografické	kryptografický	k2eAgFnPc1d1	kryptografická
(	(	kIx(	(
<g/>
šifrovací	šifrovací	k2eAgFnPc1d1	šifrovací
<g/>
)	)	kIx)	)
aplikace	aplikace	k1gFnPc1	aplikace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
tyto	tento	k3xDgInPc4	tento
nedostatky	nedostatek	k1gInPc4	nedostatek
řešit	řešit	k5eAaImF	řešit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
virtuální	virtuální	k2eAgFnSc2d1	virtuální
privátní	privátní	k2eAgFnSc2d1	privátní
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
šifrování	šifrování	k1gNnSc2	šifrování
zpráv	zpráva	k1gFnPc2	zpráva
pomocí	pomocí	k7c2	pomocí
PGP	PGP	kA	PGP
nebo	nebo	k8xC	nebo
GNU	gnu	k1gMnSc1	gnu
Privacy	Privaca	k1gFnSc2	Privaca
Guard	Guard	k1gMnSc1	Guard
<g/>
,	,	kIx,	,
šifrovaná	šifrovaný	k2eAgFnSc1d1	šifrovaná
komunikace	komunikace	k1gFnSc1	komunikace
s	s	k7c7	s
e-mailovým	eailův	k2eAgInSc7d1	e-mailův
serverem	server	k1gInSc7	server
pomocí	pomocí	k7c2	pomocí
Transport	transporta	k1gFnPc2	transporta
Layer	Layra	k1gFnPc2	Layra
Security	Securita	k1gFnSc2	Securita
a	a	k8xC	a
Secure	Secur	k1gMnSc5	Secur
Sockets	Socketsa	k1gFnPc2	Socketsa
Layer	Layer	k1gInSc1	Layer
a	a	k8xC	a
šifrované	šifrovaný	k2eAgNnSc1d1	šifrované
autentifikační	autentifikační	k2eAgNnSc1d1	autentifikační
schéma	schéma	k1gNnSc1	schéma
jako	jako	k8xC	jako
Simple	Simple	k1gFnPc3	Simple
Authentication	Authentication	k1gInSc1	Authentication
and	and	k?	and
Security	Securita	k1gFnSc2	Securita
Layer	Layra	k1gFnPc2	Layra
<g/>
.	.	kIx.	.
</s>
<s>
Šifrovací	šifrovací	k2eAgFnPc1d1	šifrovací
aplikace	aplikace	k1gFnPc1	aplikace
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
funkčně	funkčně	k6eAd1	funkčně
propojené	propojený	k2eAgFnSc6d1	propojená
s	s	k7c7	s
aplikacemi	aplikace	k1gFnPc7	aplikace
vytvářejícími	vytvářející	k2eAgFnPc7d1	vytvářející
elektronický	elektronický	k2eAgInSc4d1	elektronický
podpis	podpis	k1gInSc4	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Užitečnost	užitečnost	k1gFnSc1	užitečnost
a	a	k8xC	a
použitelnost	použitelnost	k1gFnSc1	použitelnost
elektronické	elektronický	k2eAgFnSc2d1	elektronická
pošty	pošta	k1gFnSc2	pošta
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
dva	dva	k4xCgInPc1	dva
fenomény	fenomén	k1gInPc1	fenomén
<g/>
:	:	kIx,	:
spam	spam	k6eAd1	spam
a	a	k8xC	a
e-mailoví	eailový	k2eAgMnPc1d1	e-mailový
červi	červ	k1gMnPc1	červ
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
stává	stávat	k5eAaImIp3nS	stávat
nevyžádaná	vyžádaný	k2eNgFnSc1d1	nevyžádaná
obtěžující	obtěžující	k2eAgFnSc1d1	obtěžující
pošta	pošta	k1gFnSc1	pošta
zvaná	zvaný	k2eAgFnSc1d1	zvaná
spam	spa	k1gNnSc7	spa
(	(	kIx(	(
<g/>
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
především	především	k9	především
různých	různý	k2eAgFnPc2d1	různá
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
inzerátů	inzerát	k1gInPc2	inzerát
<g/>
,	,	kIx,	,
formulářů	formulář	k1gInPc2	formulář
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yRgNnSc3	který
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
být	být	k5eAaImF	být
opatrný	opatrný	k2eAgMnSc1d1	opatrný
při	při	k7c6	při
zveřejňování	zveřejňování	k1gNnSc6	zveřejňování
e-mailové	eailové	k2eAgFnSc2d1	e-mailové
adresy	adresa	k1gFnSc2	adresa
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
vaši	váš	k3xOp2gFnSc4	váš
adresu	adresa	k1gFnSc4	adresa
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
chcete	chtít	k5eAaImIp2nP	chtít
je	on	k3xPp3gMnPc4	on
dobré	dobrý	k2eAgNnSc1d1	dobré
nahradit	nahradit	k5eAaPmF	nahradit
"	"	kIx"	"
<g/>
zavináč	zavináč	k1gInSc1	zavináč
–	–	k?	–
@	@	kIx~	@
<g/>
"	"	kIx"	"
například	například	k6eAd1	například
–	–	k?	–
(	(	kIx(	(
<g/>
at	at	k?	at
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spam	Spam	k6eAd1	Spam
je	být	k5eAaImIp3nS	být
nevyžádaná	vyžádaný	k2eNgFnSc1d1	nevyžádaná
reklamní	reklamní	k2eAgFnSc1d1	reklamní
pošta	pošta	k1gFnSc1	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgInPc1d1	nízký
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
odeslání	odeslání	k1gNnSc4	odeslání
zprávy	zpráva	k1gFnSc2	zpráva
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
spammerům	spammer	k1gInPc3	spammer
odeslat	odeslat	k5eAaPmF	odeslat
stovky	stovka	k1gFnPc4	stovka
miliónů	milión	k4xCgInPc2	milión
elektronických	elektronický	k2eAgFnPc2d1	elektronická
zpráv	zpráva	k1gFnPc2	zpráva
denně	denně	k6eAd1	denně
pomocí	pomocí	k7c2	pomocí
laciného	laciný	k2eAgNnSc2d1	laciné
internetového	internetový	k2eAgNnSc2d1	internetové
připojení	připojení	k1gNnSc2	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Stovky	stovka	k1gFnPc1	stovka
aktivních	aktivní	k2eAgMnPc2d1	aktivní
spammerů	spammer	k1gMnPc2	spammer
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
přetížení	přetížení	k1gNnPc4	přetížení
počítačů	počítač	k1gMnPc2	počítač
v	v	k7c6	v
internetu	internet	k1gInSc6	internet
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
takto	takto	k6eAd1	takto
dostávají	dostávat	k5eAaImIp3nP	dostávat
desítky	desítka	k1gFnPc1	desítka
či	či	k8xC	či
stovky	stovka	k1gFnPc1	stovka
nevyžádaných	vyžádaný	k2eNgInPc2d1	nevyžádaný
e-mailů	eail	k1gInPc2	e-mail
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
typem	typ	k1gInSc7	typ
e-mailových	eailův	k2eAgFnPc2d1	e-mailova
zpráv	zpráva	k1gFnPc2	zpráva
jsou	být	k5eAaImIp3nP	být
takzvané	takzvaný	k2eAgInPc1d1	takzvaný
hoaxy	hoax	k1gInPc1	hoax
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
bludné	bludný	k2eAgFnPc1d1	bludná
a	a	k8xC	a
zplanělé	zplanělý	k2eAgFnPc1d1	zplanělá
zprávy	zpráva	k1gFnPc1	zpráva
kolující	kolující	k2eAgFnPc1d1	kolující
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
E-mailoví	Eailový	k2eAgMnPc1d1	E-mailový
červi	červ	k1gMnPc1	červ
a	a	k8xC	a
viry	vira	k1gFnPc1	vira
používají	používat	k5eAaImIp3nP	používat
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
poštu	pošta	k1gFnSc4	pošta
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
šířit	šířit	k5eAaImF	šířit
do	do	k7c2	do
ostatních	ostatní	k2eAgInPc2d1	ostatní
zranitelných	zranitelný	k2eAgInPc2d1	zranitelný
počítačů	počítač	k1gInPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
první	první	k4xOgMnSc1	první
e-mailový	eailový	k2eAgMnSc1d1	e-mailový
červ	červ	k1gMnSc1	červ
(	(	kIx(	(
<g/>
Morris	Morris	k1gFnSc1	Morris
worm	worm	k1gMnSc1	worm
<g/>
)	)	kIx)	)
infikoval	infikovat	k5eAaBmAgInS	infikovat
UNIXové	unixový	k2eAgInPc4d1	unixový
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
týká	týkat	k5eAaImIp3nS	týkat
především	především	k9	především
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
faktorů	faktor	k1gInPc2	faktor
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uživatelé	uživatel	k1gMnPc1	uživatel
dostávají	dostávat	k5eAaImIp3nP	dostávat
více	hodně	k6eAd2	hodně
nevyžádané	vyžádaný	k2eNgFnPc1d1	nevyžádaná
pošty	pošta	k1gFnPc1	pošta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
snižuje	snižovat	k5eAaImIp3nS	snižovat
použitelnost	použitelnost	k1gFnSc4	použitelnost
e-mailu	eail	k1gInSc2	e-mail
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
technologických	technologický	k2eAgFnPc2d1	technologická
iniciativ	iniciativa	k1gFnPc2	iniciativa
(	(	kIx(	(
<g/>
Stopping	Stopping	k1gInSc1	Stopping
e-mail	eail	k1gInSc1	e-mail
abuse	abusus	k1gInSc5	abusus
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zmírnit	zmírnit	k5eAaPmF	zmírnit
dopad	dopad	k1gInSc4	dopad
spamových	spamův	k2eAgInPc2d1	spamův
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
Kongresem	kongres	k1gInSc7	kongres
schválen	schválit	k5eAaPmNgInS	schválit
zákon	zákon	k1gInSc1	zákon
na	na	k7c4	na
regulaci	regulace	k1gFnSc4	regulace
takového	takový	k3xDgInSc2	takový
e-mailu	eail	k1gInSc2	e-mail
(	(	kIx(	(
<g/>
Can	Can	k1gFnSc1	Can
Spam	Spam	k1gInSc1	Spam
Act	Act	k1gFnSc1	Act
of	of	k?	of
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
obrany	obrana	k1gFnSc2	obrana
před	před	k7c7	před
spamem	spam	k1gInSc7	spam
je	být	k5eAaImIp3nS	být
obrana	obrana	k1gFnSc1	obrana
technologická	technologický	k2eAgFnSc1d1	technologická
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
e-mailových	eailův	k2eAgFnPc2d1	e-mailova
adres	adresa	k1gFnPc2	adresa
je	být	k5eAaImIp3nS	být
web	web	k1gInSc4	web
a	a	k8xC	a
určitým	určitý	k2eAgInSc7d1	určitý
specializovaným	specializovaný	k2eAgInSc7d1	specializovaný
programem	program	k1gInSc7	program
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
hromadně	hromadně	k6eAd1	hromadně
extrahovat	extrahovat	k5eAaBmF	extrahovat
elektronické	elektronický	k2eAgFnSc2d1	elektronická
adresy	adresa	k1gFnSc2	adresa
vyskytující	vyskytující	k2eAgFnSc6d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
textu	text	k1gInSc6	text
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zabránění	zabránění	k1gNnSc3	zabránění
těmto	tento	k3xDgInPc3	tento
automatizovaným	automatizovaný	k2eAgInPc3d1	automatizovaný
útokům	útok	k1gInPc3	útok
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
e-mailová	eailový	k2eAgFnSc1d1	e-mailová
adresa	adresa	k1gFnSc1	adresa
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pochopitelné	pochopitelný	k2eAgNnSc1d1	pochopitelné
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nepochopitelné	pochopitelný	k2eNgNnSc1d1	nepochopitelné
stroji	stroj	k1gInPc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
adresa	adresa	k1gFnSc1	adresa
"	"	kIx"	"
<g/>
eva@domena.cz	eva@domena.cz	k1gInSc1	eva@domena.cz
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
zapíše	zapsat	k5eAaPmIp3nS	zapsat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
eva	eva	k?	eva
zavináč	zavináč	k1gInSc1	zavináč
doména	doména	k1gFnSc1	doména
tečka	tečka	k1gFnSc1	tečka
cz	cz	k?	cz
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
"	"	kIx"	"
<g/>
eva	eva	k?	eva
AT	AT	kA	AT
doména	doména	k1gFnSc1	doména
DOT	DOT	kA	DOT
cz	cz	k?	cz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vygeneruje	vygenerovat	k5eAaPmIp3nS	vygenerovat
obrázek	obrázek	k1gInSc1	obrázek
obsahující	obsahující	k2eAgFnSc4d1	obsahující
e-mailovou	eailový	k2eAgFnSc4d1	e-mailová
adresu	adresa	k1gFnSc4	adresa
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
uvede	uvést	k5eAaPmIp3nS	uvést
namísto	namísto	k7c2	namísto
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
programů	program	k1gInPc2	program
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
samostatné	samostatný	k2eAgFnPc1d1	samostatná
aplikace	aplikace	k1gFnPc1	aplikace
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc1	součást
e-mailových	eailův	k2eAgMnPc2d1	e-mailův
klientů	klient	k1gMnPc2	klient
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
programy	program	k1gInPc1	program
na	na	k7c6	na
serverech	server	k1gInPc6	server
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
různých	různý	k2eAgNnPc2d1	různé
kritérií	kritérion	k1gNnPc2	kritérion
alespoň	alespoň	k9	alespoň
část	část	k1gFnSc1	část
spamu	spamat	k5eAaPmIp1nS	spamat
odfiltrovat	odfiltrovat	k5eAaPmF	odfiltrovat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
programy	program	k1gInPc4	program
pro	pro	k7c4	pro
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
e-mailovou	eailový	k2eAgFnSc4d1	e-mailová
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
:	:	kIx,	:
Enigmail	Enigmail	k1gInSc1	Enigmail
–	–	k?	–
velice	velice	k6eAd1	velice
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
doplněk	doplněk	k1gInSc4	doplněk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
do	do	k7c2	do
poštovního	poštovní	k2eAgMnSc2d1	poštovní
klienta	klient	k1gMnSc2	klient
Thunderbirdu	Thunderbird	k1gInSc2	Thunderbird
přidá	přidat	k5eAaPmIp3nS	přidat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
šifrovanou	šifrovaný	k2eAgFnSc7d1	šifrovaná
poštou	pošta	k1gFnSc7	pošta
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
posílat	posílat	k5eAaImF	posílat
poštu	pošta	k1gFnSc4	pošta
v	v	k7c6	v
šifrované	šifrovaný	k2eAgFnSc6d1	šifrovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vaše	váš	k3xOp2gFnPc1	váš
zprávu	zpráva	k1gFnSc4	zpráva
přečtou	přečíst	k5eAaPmIp3nP	přečíst
pouze	pouze	k6eAd1	pouze
prověřené	prověřený	k2eAgFnPc4d1	prověřená
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Safetica	Safetic	k2eAgFnSc1d1	Safetic
Free	Free	k1gFnSc1	Free
–	–	k?	–
dříve	dříve	k6eAd2	dříve
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
DisCryptor	DisCryptor	k1gMnSc1	DisCryptor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bezplatný	bezplatný	k2eAgInSc4d1	bezplatný
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
počítačového	počítačový	k2eAgNnSc2d1	počítačové
soukromí	soukromí	k1gNnSc2	soukromí
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
zašifrovat	zašifrovat	k5eAaPmF	zašifrovat
soubory	soubor	k1gInPc4	soubor
<g/>
,	,	kIx,	,
složky	složka	k1gFnPc4	složka
a	a	k8xC	a
přílohy	příloh	k1gInPc4	příloh
e-mailů	eail	k1gInPc2	e-mail
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
pokročilých	pokročilý	k2eAgFnPc2d1	pokročilá
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
datová	datový	k2eAgFnSc1d1	datová
skartovačka	skartovačka	k1gFnSc1	skartovačka
pro	pro	k7c4	pro
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
odstranění	odstranění	k1gNnSc4	odstranění
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
je	být	k5eAaImIp3nS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
českou	český	k2eAgFnSc7d1	Česká
společností	společnost	k1gFnSc7	společnost
Safetica	Safetica	k1gMnSc1	Safetica
Technologies	Technologies	k1gMnSc1	Technologies
<g/>
.	.	kIx.	.
</s>
<s>
Vigenè	Vigenè	k?	Vigenè
šifra	šifra	k1gFnSc1	šifra
–	–	k?	–
program	program	k1gInSc1	program
zašifruje	zašifrovat	k5eAaPmIp3nS	zašifrovat
i	i	k8xC	i
dešifruje	dešifrovat	k5eAaBmIp3nS	dešifrovat
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
napíšete	napsat	k5eAaPmIp2nP	napsat
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
k	k	k7c3	k
nejjednoduššímu	jednoduchý	k2eAgInSc3d3	nejjednodušší
způsobu	způsob	k1gInSc3	způsob
šifrování	šifrování	k1gNnSc2	šifrování
e-mailové	eailové	k2eAgFnSc2d1	e-mailové
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Musíme	muset	k5eAaImIp1nP	muset
mít	mít	k5eAaImF	mít
ale	ale	k9	ale
na	na	k7c6	na
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
šifra	šifra	k1gFnSc1	šifra
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
už	už	k6eAd1	už
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
nerozbitným	rozbitný	k2eNgInSc7d1	nerozbitný
kódem	kód	k1gInSc7	kód
jako	jako	k8xS	jako
nejmodernější	moderní	k2eAgInPc1d3	nejmodernější
algoritmy	algoritmus	k1gInPc1	algoritmus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Tor	Tor	k1gMnSc1	Tor
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
ochránit	ochránit	k5eAaPmF	ochránit
soukromí	soukromí	k1gNnSc4	soukromí
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
uživatelů	uživatel	k1gMnPc2	uživatel
e-mailů	eail	k1gInPc2	e-mail
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
zabránit	zabránit	k5eAaPmF	zabránit
sledování	sledování	k1gNnSc3	sledování
komunikace	komunikace	k1gFnSc2	komunikace
probíhající	probíhající	k2eAgFnSc2d1	probíhající
skrze	skrze	k?	skrze
protokol	protokol	k1gInSc1	protokol
TCP	TCP	kA	TCP
<g/>
.	.	kIx.	.
</s>
<s>
Tor	Tor	k1gMnSc1	Tor
rozebere	rozebrat	k5eAaPmIp3nS	rozebrat
internetové	internetový	k2eAgNnSc4d1	internetové
připojení	připojení	k1gNnSc4	připojení
uživatele	uživatel	k1gMnSc2	uživatel
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
anonymní	anonymní	k2eAgInSc1d1	anonymní
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
lze	lze	k6eAd1	lze
také	také	k9	také
využít	využít	k5eAaPmF	využít
i	i	k9	i
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
freemailovým	freemailův	k2eAgFnPc3d1	freemailův
službám	služba	k1gFnPc3	služba
skrze	skrze	k?	skrze
jejich	jejich	k3xOp3gNnSc7	jejich
zranitelné	zranitelný	k2eAgNnSc1d1	zranitelné
webové	webový	k2eAgNnSc1d1	webové
rozhraní	rozhraní	k1gNnSc1	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
Email	email	k1gInSc1	email
(	(	kIx(	(
<g/>
Seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
mail	mail	k1gInSc1	mail
(	(	kIx(	(
<g/>
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Outlook	Outlook	k1gInSc1	Outlook
(	(	kIx(	(
<g/>
Microsoft	Microsoft	kA	Microsoft
<g/>
)	)	kIx)	)
Gmail	Gmail	k1gMnSc1	Gmail
(	(	kIx(	(
<g/>
Google	Google	k1gInSc1	Google
<g/>
)	)	kIx)	)
Internet	Internet	k1gInSc1	Internet
Simple	Simple	k1gFnSc2	Simple
Mail	mail	k1gInSc1	mail
Transfer	transfer	k1gInSc1	transfer
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
SMTP	SMTP	kA	SMTP
<g/>
)	)	kIx)	)
Post	post	k1gInSc1	post
Office	Office	kA	Office
<g />
.	.	kIx.	.
</s>
<s>
Protocol	Protocol	k1gInSc1	Protocol
version	version	k1gInSc1	version
3	[number]	k4	3
(	(	kIx(	(
<g/>
POP	pop	k1gInSc1	pop
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Internet	Internet	k1gInSc1	Internet
Message	Messag	k1gFnSc2	Messag
Access	Accessa	k1gFnPc2	Accessa
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
IMAP	IMAP	kA	IMAP
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
e-mail	eail	k1gInSc1	e-mail
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
e-mail	eail	k1gInSc1	e-mail
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
E-mail	eail	k1gInSc1	e-mail
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
terminologické	terminologický	k2eAgFnSc6d1	terminologická
databázi	databáze	k1gFnSc6	databáze
knihovnictví	knihovnictví	k1gNnSc2	knihovnictví
a	a	k8xC	a
informační	informační	k2eAgFnSc2d1	informační
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
TDKIV	TDKIV	kA	TDKIV
<g/>
)	)	kIx)	)
</s>
