<s>
V	v	k7c6
této	tento	k3xDgFnSc6
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
úplně	úplně	k6eAd1
poražena	poražen	k2eAgFnSc1d1
byly	být	k5eAaImAgInP
v	v	k7c6
Londýně	Londýn	k1gInSc6
obnoveny	obnoven	k2eAgInPc4d1
rozhovory	rozhovor	k1gInPc4
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1913	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1913	#num#	k4
starý	starý	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
podepsána	podepsat	k5eAaPmNgFnS
Londýnská	londýnský	k2eAgFnSc1d1
mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
základě	základ	k1gInSc6
přišla	přijít	k5eAaPmAgFnS
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
o	o	k7c4
všechna	všechen	k3xTgNnPc4
území	území	k1gNnPc4
v	v	k7c6
Mysii	Mysie	k1gFnSc6
kromě	kromě	k7c2
oblastí	oblast	k1gFnPc2
v	v	k7c6
Albánii	Albánie	k1gFnSc6
<g/>
.	.	kIx.
</s>