<s>
První	první	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Balkánské	balkánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
ilustrační	ilustrační	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1912	#num#	k4
-	-	kIx~
30	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1913	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Balkán	Balkán	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Vítězství	vítězství	k1gNnSc1
Balkánského	balkánský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
londýnská	londýnský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Balkánský	balkánský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
:	:	kIx,
Bulharsko	Bulharsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
Řecko	Řecko	k1gNnSc4
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Nazim	Nazim	k1gMnSc1
Paša	paša	k1gMnSc1
<g/>
,	,	kIx,
Zeki	Zek	k1gMnPc1
Paša	paša	k1gMnSc1
<g/>
,	,	kIx,
Esad	Esad	k1gMnSc1
Paša	paša	k1gMnSc1
<g/>
,	,	kIx,
Abdullah	Abdullah	k1gMnSc1
Paša	paša	k1gMnSc1
<g/>
,	,	kIx,
Ali	Ali	k1gMnSc1
Rizah	Rizah	k1gMnSc1
Paša	paša	k1gMnSc1
<g/>
,	,	kIx,
Hasan	Hasan	k1gMnSc1
Tahsin	Tahsin	k1gMnSc1
Paša	paša	k1gMnSc1
</s>
<s>
Mihail	Mihait	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
Savov	Savov	k1gInSc1
Ivan	Ivan	k1gMnSc1
Fičev	Fičev	k1gFnSc1
<g/>
,	,	kIx,
Vasil	Vasil	k1gMnSc1
Kutinčev	Kutinčev	k1gMnSc1
<g/>
,	,	kIx,
Nikola	Nikola	k1gMnSc1
Ivanov	Ivanov	k1gInSc1
<g/>
,	,	kIx,
Radko	Radka	k1gFnSc5
Dimitriev	Dimitriev	k1gFnSc1
<g/>
,	,	kIx,
Stiliyan	Stiliyan	k1gInSc1
Kovačev	Kovačev	k1gFnSc2
Georgi	Georg	k1gFnSc2
Todorov	Todorovo	k1gNnPc2
Radomir	Radomir	k1gMnSc1
Putnik	Putnik	k1gMnSc1
<g/>
,	,	kIx,
Petar	Petar	k1gMnSc1
Bojović	Bojović	k1gMnSc1
<g/>
,	,	kIx,
Stepa	Stepa	k1gFnSc1
Stepanović	Stepanović	k1gMnSc1
<g/>
,	,	kIx,
Božidar	Božidar	k1gMnSc1
Janković	Janković	k1gFnSc2
Konstantin	Konstantin	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
Panagiotis	Panagiotis	k1gFnSc1
Danglis	Danglis	k1gFnSc1
<g/>
,	,	kIx,
Pavlos	Pavlos	k1gMnSc1
Kountouriotis	Kountouriotis	k1gFnSc2
Nikola	Nikola	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
Princ	princa	k1gFnPc2
Danilo	danit	k5eAaImAgNnS
<g/>
,	,	kIx,
Mitar	Mitar	k1gMnSc1
Martinović	Martinović	k1gMnSc1
<g/>
,	,	kIx,
Janko	Janko	k1gMnSc1
Vukotić	Vukotić	k1gMnSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
336	#num#	k4
742	#num#	k4
vojáků	voják	k1gMnPc2
zpočátku	zpočátku	k6eAd1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
350	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
Srbsko	Srbsko	k1gNnSc4
230	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
Řecko	Řecko	k1gNnSc4
125	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
44	#num#	k4
500	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
749	#num#	k4
500	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
:	:	kIx,
<g/>
50	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
<g/>
100	#num#	k4
000	#num#	k4
zraněných	zraněný	k2eAgInPc2d1
<g/>
115	#num#	k4
000	#num#	k4
zajatých	zajatý	k2eAgInPc2d1
<g/>
75	#num#	k4
000	#num#	k4
zemřelo	zemřít	k5eAaPmAgNnS
na	na	k7c4
nemoci	nemoc	k1gFnPc4
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
340	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
zraněných	zraněný	k1gMnPc2
nebo	nebo	k8xC
zajatých	zajatý	k1gMnPc2
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
8	#num#	k4
840	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
4	#num#	k4
926	#num#	k4
pohřešovaných	pohřešovaný	k2eAgInPc2d1
<g/>
36	#num#	k4
877	#num#	k4
zraněných	zraněný	k2eAgInPc2d1
<g/>
10	#num#	k4
995	#num#	k4
zesnulých	zesnulý	k2eAgMnPc2d1
</s>
<s>
Řecko	Řecko	k1gNnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
373	#num#	k4
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
v	v	k7c6
boji	boj	k1gInSc6
nebo	nebo	k8xC
zemřelo	zemřít	k5eAaPmAgNnS
na	na	k7c4
zranění	zranění	k1gNnSc4
<g/>
9	#num#	k4
295	#num#	k4
zraněných	zraněný	k2eAgMnPc2d1
<g/>
~	~	kIx~
<g/>
1	#num#	k4
558	#num#	k4
mrtvých	mrtvý	k1gMnPc2
na	na	k7c4
nemoc	nemoc	k1gFnSc4
nebo	nebo	k8xC
při	při	k7c6
nehodách	nehoda	k1gFnPc6
(	(	kIx(
<g/>
vč.	vč.	k?
2	#num#	k4
<g/>
.	.	kIx.
balk	balk	k1gInSc1
<g/>
.	.	kIx.
války	válka	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
<g/>
:	:	kIx,
<g/>
5	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
<g/>
18	#num#	k4
000	#num#	k4
zraněných	zraněný	k2eAgMnPc2d1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
unknown	unknown	k1gMnSc1
dead	dead	k1gMnSc1
of	of	k?
disease	disease	k6eAd1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
2	#num#	k4
836	#num#	k4
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
v	v	k7c6
boji	boj	k1gInSc6
nebo	nebo	k8xC
zemřelo	zemřít	k5eAaPmAgNnS
na	na	k7c4
nemoci	nemoc	k1gFnPc4
6	#num#	k4
602	#num#	k4
zraněných	zraněný	k1gMnPc2
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
min	min	kA
<g/>
.	.	kIx.
cca	cca	kA
108	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
zraněných	zraněný	k1gMnPc2
</s>
<s>
První	první	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
1912	#num#	k4
-	-	kIx~
květen	květen	k1gInSc4
1913	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
balkánských	balkánský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vypukla	vypuknout	k5eAaPmAgFnS
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Balkánské	balkánský	k2eAgFnPc1d1
země	zem	k1gFnPc1
(	(	kIx(
<g/>
Bulharské	bulharský	k2eAgNnSc1d1
carství	carství	k1gNnSc1
<g/>
,	,	kIx,
Srbské	srbský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Řecké	řecký	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Černohorské	Černohorské	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c6
ní	on	k3xPp3gFnSc6
vyhlásily	vyhlásit	k5eAaPmAgInP
válku	válka	k1gFnSc4
Osmanské	osmanský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
s	s	k7c7
cílem	cíl	k1gInSc7
dobýt	dobýt	k5eAaPmF
její	její	k3xOp3gNnSc4
balkánské	balkánský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Diplomatická	diplomatický	k2eAgFnSc1d1
a	a	k8xC
politická	politický	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
války	válka	k1gFnSc2
</s>
<s>
Bulharský	bulharský	k2eAgMnSc1d1
car	car	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
pochopil	pochopit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
země	země	k1gFnSc1
není	být	k5eNaImIp3nS
schopna	schopen	k2eAgFnSc1d1
sama	sám	k3xTgFnSc1
vyřešit	vyřešit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
otázky	otázka	k1gFnPc4
budoucnosti	budoucnost	k1gFnSc2
cestou	cestou	k7c2
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všem	všecek	k3xTgNnSc6
je	být	k5eAaImIp3nS
podřízena	podřízen	k2eAgFnSc1d1
Osmanské	osmanský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
,	,	kIx,
plánem	plán	k1gInSc7
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc2
,	,	kIx,
Ruska	Rusko	k1gNnSc2
a	a	k8xC
sousedním	sousední	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
-	-	kIx~
Srbsku	Srbsko	k1gNnSc6
<g/>
,	,	kIx,
Černé	Černá	k1gFnSc6
Hoře	hora	k1gFnSc6
<g/>
,	,	kIx,
Rumunsku	Rumunsko	k1gNnSc6
a	a	k8xC
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
vyformovaly	vyformovat	k5eAaPmAgFnP
dvě	dva	k4xCgNnPc4
seskupení	seskupení	k1gNnPc4
<g/>
:	:	kIx,
</s>
<s>
Centrální	centrální	k2eAgFnPc1d1
mocnosti	mocnost	k1gFnPc1
<g/>
:	:	kIx,
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
<g/>
,	,	kIx,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
<g/>
,	,	kIx,
Italské	italský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Trojdohoda	Trojdohoda	k1gFnSc1
<g/>
:	:	kIx,
Anglie	Anglie	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
Centrální	centrální	k2eAgFnPc1d1
mocnosti	mocnost	k1gFnPc1
<g/>
,	,	kIx,
ovládané	ovládaný	k2eAgFnPc1d1
Německem	Německo	k1gNnSc7
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgFnP
získat	získat	k5eAaPmF
si	se	k3xPyFc3
Osmanskou	osmanský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharsko	Bulharsko	k1gNnSc1
se	se	k3xPyFc4
tedy	tedy	k9
nemohlo	moct	k5eNaImAgNnS
spoléhat	spoléhat	k5eAaImF
na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
se	se	k3xPyFc4
car	car	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
rozhodl	rozhodnout	k5eAaPmAgMnS
pro	pro	k7c4
pomoc	pomoc	k1gFnSc4
od	od	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1910	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
ministerským	ministerský	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
a	a	k8xC
ministrem	ministr	k1gMnSc7
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
Bulharska	Bulharsko	k1gNnSc2
navštívil	navštívit	k5eAaPmAgInS
Petrohrad	Petrohrad	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
tam	tam	k6eAd1
s	s	k7c7
ruským	ruský	k2eAgMnSc7d1
carem	car	k1gMnSc7
Mikulášem	Mikuláš	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
podepsal	podepsat	k5eAaPmAgMnS
vojenskou	vojenský	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
však	však	k9
nebyla	být	k5eNaImAgFnS
podepsána	podepsat	k5eAaPmNgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
ruský	ruský	k2eAgMnSc1d1
car	car	k1gMnSc1
obával	obávat	k5eAaImAgMnS
o	o	k7c4
ztrátu	ztráta	k1gFnSc4
vlivu	vliv	k1gInSc2
a	a	k8xC
důležitých	důležitý	k2eAgInPc2d1
průlivů	průliv	k1gInPc2
Bospor	Bospor	k1gInSc1
a	a	k8xC
Dardanely	Dardanely	k1gFnPc1
na	na	k7c6
Balkánském	balkánský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
proto	proto	k8xC
doporučilo	doporučit	k5eAaPmAgNnS
Bulharsku	Bulharsko	k1gNnSc6
uzavřít	uzavřít	k5eAaPmF
svaz	svaz	k1gInSc1
se	s	k7c7
Srbskem	Srbsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Car	car	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
se	se	k3xPyFc4
však	však	k9
potřeboval	potřebovat	k5eAaImAgInS
zajistit	zajistit	k5eAaPmF
a	a	k8xC
v	v	k7c6
zemi	zem	k1gFnSc6
vytvořit	vytvořit	k5eAaPmF
takovou	takový	k3xDgFnSc4
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
Rusku	Ruska	k1gFnSc4
vyhovovala	vyhovovat	k5eAaImAgFnS
a	a	k8xC
měla	mít	k5eAaImAgFnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
plnou	plný	k2eAgFnSc4d1
důvěru	důvěra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1911	#num#	k4
vláda	vláda	k1gFnSc1
Alexandra	Alexandra	k1gFnSc1
Malinova	Malinův	k2eAgFnSc1d1
podala	podat	k5eAaPmAgFnS
demisi	demise	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
postu	post	k1gInSc6
předsedy	předseda	k1gMnSc2
vlády	vláda	k1gFnSc2
ho	on	k3xPp3gMnSc4
nahradil	nahradit	k5eAaPmAgMnS
Ivan	Ivan	k1gMnSc1
Gešov	Gešov	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
parlamentu	parlament	k1gInSc6
předsedal	předsedat	k5eAaImAgMnS
dvěma	dva	k4xCgFnPc3
rusofilským	rusofilský	k2eAgFnPc3d1
stranám	strana	k1gFnPc3
-	-	kIx~
Národní	národní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
a	a	k8xC
progresivním-liberální	progresivním-liberální	k2eAgFnSc3d1
straně	strana	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
roku	rok	k1gInSc2
1911	#num#	k4
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc4d1
shromáždění	shromáždění	k1gNnSc1
odhlasovalo	odhlasovat	k5eAaPmAgNnS
změnu	změna	k1gFnSc4
článku	článek	k1gInSc2
17	#num#	k4
Tărnovskej	Tărnovskej	k1gFnSc2
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
umožňoval	umožňovat	k5eAaImAgInS
carovi	car	k1gMnSc3
Ferdinandovi	Ferdinand	k1gMnSc3
I.	I.	kA
a	a	k8xC
vládě	vláda	k1gFnSc3
uzavírat	uzavírat	k5eAaPmF,k5eAaImF
mezinárodní	mezinárodní	k2eAgFnPc4d1
smlouvy	smlouva	k1gFnPc4
bez	bez	k7c2
vědomí	vědomí	k1gNnSc2
bulharského	bulharský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
oslabila	oslabit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
možnost	možnost	k1gFnSc1
kontroly	kontrola	k1gFnSc2
situace	situace	k1gFnSc1
na	na	k7c6
Balkánském	balkánský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
hlouběji	hluboko	k6eAd2
zajímat	zajímat	k5eAaImF
o	o	k7c4
situaci	situace	k1gFnSc4
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
Rusko	Rusko	k1gNnSc1
muselo	muset	k5eAaImAgNnS
urychleně	urychleně	k6eAd1
zaujmout	zaujmout	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
pozici	pozice	k1gFnSc4
a	a	k8xC
začalo	začít	k5eAaPmAgNnS
podporovat	podporovat	k5eAaImF
protiturecké	protiturecký	k2eAgFnPc4d1
nálady	nálada	k1gFnPc4
v	v	k7c6
balkánských	balkánský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1911	#num#	k4
vypukla	vypuknout	k5eAaPmAgFnS
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Itálií	Itálie	k1gFnSc7
a	a	k8xC
Osmanskou	osmanský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
Balkánského	balkánský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
</s>
<s>
Balkánský	balkánský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
na	na	k7c6
konci	konec	k1gInSc6
První	první	k4xOgFnSc2
balkánské	balkánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1912	#num#	k4
podepsali	podepsat	k5eAaPmAgMnP
Bulharsko	Bulharsko	k1gNnSc4
a	a	k8xC
Srbsko	Srbsko	k1gNnSc4
tzv.	tzv.	kA
Smlouvu	smlouva	k1gFnSc4
a	a	k8xC
přátelství	přátelství	k1gNnSc4
a	a	k8xC
spojení	spojení	k1gNnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
doplněna	doplnit	k5eAaPmNgFnS
o	o	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
konvenci	konvence	k1gFnSc4
(	(	kIx(
<g/>
úmluvu	úmluva	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smlouva	smlouva	k1gFnSc1
měla	mít	k5eAaImAgFnS
tajný	tajný	k2eAgInSc4d1
dodatek	dodatek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
určoval	určovat	k5eAaImAgInS
rozdělení	rozdělení	k1gNnSc4
osvobozených	osvobozený	k2eAgNnPc2d1
území	území	k1gNnPc2
Bulharska	Bulharsko	k1gNnSc2
(	(	kIx(
<g/>
Thrákie	Thrákie	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
případě	případ	k1gInSc6
úspěchu	úspěch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdělení	rozdělení	k1gNnSc1
území	území	k1gNnSc1
Makedonie	Makedonie	k1gFnSc2
bylo	být	k5eAaImAgNnS
těžším	těžký	k2eAgInSc7d2
oříškem	oříšek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
území	území	k1gNnSc1
se	se	k3xPyFc4
dělilo	dělit	k5eAaImAgNnS
na	na	k7c4
spornou	sporný	k2eAgFnSc4d1
a	a	k8xC
nespornou	sporný	k2eNgFnSc4d1
zónu	zóna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastníci	účastník	k1gMnPc1
podpisového	podpisový	k2eAgInSc2d1
aktu	akt	k1gInSc2
se	se	k3xPyFc4
dohodli	dohodnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
nesporná	sporný	k2eNgFnSc1d1
zóna	zóna	k1gFnSc1
připadne	připadnout	k5eAaPmIp3nS
Bulharsku	Bulharsko	k1gNnSc6
a	a	k8xC
o	o	k7c4
spornou	sporný	k2eAgFnSc4d1
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
vést	vést	k5eAaImF
další	další	k2eAgInPc4d1
rozhovory	rozhovor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
ani	ani	k8xC
v	v	k7c6
těchto	tento	k3xDgInPc6
rozhovorech	rozhovor	k1gInPc6
nedosáhnou	dosáhnout	k5eNaPmIp3nP
konsensu	konsens	k1gInSc3
<g/>
,	,	kIx,
navštíví	navštívit	k5eAaPmIp3nS
ruského	ruský	k2eAgMnSc4d1
cara	car	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
otázce	otázka	k1gFnSc6
rozhodne	rozhodnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharská	bulharský	k2eAgFnSc1d1
diplomacie	diplomacie	k1gFnSc1
se	se	k3xPyFc4
tímto	tento	k3xDgInSc7
krokem	krok	k1gInSc7
dopustila	dopustit	k5eAaPmAgFnS
rozdělení	rozdělení	k1gNnSc4
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
se	se	k3xPyFc4
Bulharsko	Bulharsko	k1gNnSc1
se	s	k7c7
Srbskem	Srbsko	k1gNnSc7
dohodli	dohodnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
vypukne	vypuknout	k5eAaPmIp3nS
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
Bulharsko	Bulharsko	k1gNnSc1
přispěje	přispět	k5eAaPmIp3nS
nejméně	málo	k6eAd3
200	#num#	k4
000	#num#	k4
vojskem	vojsko	k1gNnSc7
a	a	k8xC
Srbsko	Srbsko	k1gNnSc1
150	#num#	k4
000	#num#	k4
armádou	armáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
dohodou	dohoda	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
úmluvy	úmluva	k1gFnSc2
mezi	mezi	k7c7
Sofií	Sofia	k1gFnSc7
a	a	k8xC
Bělehradem	Bělehrad	k1gInSc7
byl	být	k5eAaImAgMnS
upevněn	upevněn	k2eAgInSc4d1
základ	základ	k1gInSc4
Balkánského	balkánský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1912	#num#	k4
Bulharsko	Bulharsko	k1gNnSc1
podepsalo	podepsat	k5eAaPmAgNnS
smlouvu	smlouva	k1gFnSc4
také	také	k6eAd1
s	s	k7c7
Řeckem	Řecko	k1gNnSc7
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
zavázalo	zavázat	k5eAaPmAgNnS
účastnit	účastnit	k5eAaImF
se	se	k3xPyFc4
války	válka	k1gFnSc2
v	v	k7c6
počtu	počet	k1gInSc6
300	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecko	Řecko	k1gNnSc1
mělo	mít	k5eAaImAgNnS
přispět	přispět	k5eAaPmF
150	#num#	k4
000	#num#	k4
armádou	armáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharsko	Bulharsko	k1gNnSc1
se	se	k3xPyFc4
však	však	k9
dopustilo	dopustit	k5eAaPmAgNnS
vážné	vážný	k2eAgFnPc4d1
chyby	chyba	k1gFnPc4
<g/>
,	,	kIx,
když	když	k8xS
neotevřelo	otevřít	k5eNaPmAgNnS
otázku	otázka	k1gFnSc4
rozdělení	rozdělení	k1gNnSc2
tureckých	turecký	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
případě	případ	k1gInSc6
vítězství	vítězství	k1gNnSc2
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
tak	tak	k9
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
ve	v	k7c6
válce	válka	k1gFnSc6
poraženo	poražen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
k	k	k7c3
Bulharsku	Bulharsko	k1gNnSc3
a	a	k8xC
Srbsku	Srbsko	k1gNnSc3
připojilo	připojit	k5eAaPmAgNnS
i	i	k9
Černohorské	Černohorské	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jejím	její	k3xOp3gInSc6
připojení	připojený	k2eAgMnPc1d1
členové	člen	k1gMnPc1
svazu	svaz	k1gInSc2
upřesnili	upřesnit	k5eAaPmAgMnP
plán	plán	k1gInSc4
postupu	postup	k1gInSc2
vojenských	vojenský	k2eAgInPc2d1
pochodů	pochod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharsko	Bulharsko	k1gNnSc1
v	v	k7c6
nich	on	k3xPp3gMnPc6
mělo	mít	k5eAaImAgNnS
nejdůležitější	důležitý	k2eAgFnSc4d3
roli	role	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
uskutečnit	uskutečnit	k5eAaPmF
hlavní	hlavní	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srbská	srbský	k2eAgFnSc1d1
a	a	k8xC
řecká	řecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
zatím	zatím	k6eAd1
rozmístila	rozmístit	k5eAaPmAgFnS
na	na	k7c6
území	území	k1gNnSc6
Makedonie	Makedonie	k1gFnSc2
a	a	k8xC
Albánie	Albánie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
spojila	spojit	k5eAaPmAgFnS
s	s	k7c7
armádou	armáda	k1gFnSc7
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
však	však	k9
vyskytl	vyskytnout	k5eAaPmAgInS
problém	problém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Řecko	Řecko	k1gNnSc1
chtělo	chtít	k5eAaImAgNnS
osvobodit	osvobodit	k5eAaPmF
území	území	k1gNnSc4
Makedonie	Makedonie	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
tak	tak	k6eAd1
zajistilo	zajistit	k5eAaPmAgNnS
vliv	vliv	k1gInSc4
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
s	s	k7c7
čímž	což	k3yRnSc7,k3yQnSc7
Bulharsko	Bulharsko	k1gNnSc1
nesouhlasilo	souhlasit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Bezprostřední	bezprostřední	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
na	na	k7c4
válku	válka	k1gFnSc4
</s>
<s>
V	v	k7c6
zemi	zem	k1gFnSc6
se	se	k3xPyFc4
všichni	všechen	k3xTgMnPc1
kromě	kromě	k7c2
členů	člen	k1gMnPc2
Strany	strana	k1gFnSc2
úzkých	úzký	k2eAgMnPc2d1
socialistů	socialist	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
po	po	k7c6
celém	celý	k2eAgNnSc6d1
Bulharsku	Bulharsko	k1gNnSc6
organizovali	organizovat	k5eAaBmAgMnP
protivojenskou	protivojenský	k2eAgFnSc4d1
propagandu	propaganda	k1gFnSc4
<g/>
,	,	kIx,
připravovali	připravovat	k5eAaImAgMnP
na	na	k7c4
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
roku	rok	k1gInSc2
1912	#num#	k4
se	se	k3xPyFc4
situace	situace	k1gFnSc1
na	na	k7c6
Balkáně	Balkán	k1gInSc6
zostřila	zostřit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
začali	začít	k5eAaPmAgMnP
mobilizaci	mobilizace	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
vojska	vojsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1912	#num#	k4
dalo	dát	k5eAaPmAgNnS
Bulharsko	Bulharsko	k1gNnSc1
Osmanské	osmanský	k2eAgNnSc1d1
říši	říš	k1gFnSc3
ultimátum	ultimátum	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yIgMnPc3,k3yQgMnPc3
chtělo	chtít	k5eAaImAgNnS
dosáhnout	dosáhnout	k5eAaPmF
pro	pro	k7c4
evropské	evropský	k2eAgNnSc4d1
území	území	k1gNnSc4
bulharského	bulharský	k2eAgNnSc2d1
carství	carství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpovědí	odpověď	k1gFnSc7
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
na	na	k7c4
ultimátum	ultimátum	k1gNnSc4
však	však	k9
bylo	být	k5eAaImAgNnS
zrušení	zrušení	k1gNnSc1
diplomatických	diplomatický	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
s	s	k7c7
Bulharskem	Bulharsko	k1gNnSc7
a	a	k8xC
vyhlášení	vyhlášení	k1gNnSc1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
války	válka	k1gFnSc2
</s>
<s>
První	první	k4xOgFnSc1
etapa	etapa	k1gFnSc1
</s>
<s>
Řecký	řecký	k2eAgInSc1d1
polní	polní	k2eAgInSc1d1
kánon	kánon	k1gInSc1
ráže	ráže	k1gFnSc2
75	#num#	k4
<g/>
mm	mm	kA
</s>
<s>
Předběžné	předběžný	k2eAgNnSc1d1
rozložení	rozložení	k1gNnSc1
sil	síla	k1gFnPc2
nahrávalo	nahrávat	k5eAaImAgNnS
vojskům	vojsko	k1gNnPc3
Osmanské	osmanský	k2eAgFnPc4d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharská	bulharský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
cara	car	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
I.	I.	kA
se	se	k3xPyFc4
však	však	k9
nevzdala	vzdát	k5eNaPmAgFnS
a	a	k8xC
rozdělená	rozdělený	k2eAgFnSc1d1
na	na	k7c4
tři	tři	k4xCgFnPc4
části	část	k1gFnPc4
postupovala	postupovat	k5eAaImAgFnS
k	k	k7c3
Cařihradu	Cařihrad	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
část	část	k1gFnSc1
vojska	vojsko	k1gNnSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
generála	generál	k1gMnSc2
Vasila	Vasil	k1gMnSc2
Kutničeva	Kutničev	k1gMnSc2
ve	v	k7c6
dnech	den	k1gInPc6
10	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1912	#num#	k4
porazila	porazit	k5eAaPmAgFnS
střed	střed	k1gInSc4
tureckých	turecký	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
s	s	k7c7
generálem	generál	k1gMnSc7
Nikolou	Nikola	k1gMnSc7
Ivanovem	Ivanov	k1gInSc7
obsadila	obsadit	k5eAaPmAgFnS
pevnost	pevnost	k1gFnSc1
Odrin	Odrin	k1gMnSc1
a	a	k8xC
třetí	třetí	k4xOgMnPc1
(	(	kIx(
<g/>
generál	generál	k1gMnSc1
Radko	Radka	k1gFnSc5
Dmitriev	Dmitrivo	k1gNnPc2
<g/>
)	)	kIx)
překvapil	překvapit	k5eAaPmAgInS
turecká	turecký	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
u	u	k7c2
města	město	k1gNnSc2
Lozengrad	Lozengrada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěšný	úspěšný	k2eAgInSc1d1
postup	postup	k1gInSc1
bulharské	bulharský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
donutil	donutit	k5eAaPmAgMnS
protivníka	protivník	k1gMnSc4
stáhnout	stáhnout	k5eAaPmF
se	se	k3xPyFc4
k	k	k7c3
Burgasu	Burgas	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	s	k7c7
první	první	k4xOgFnSc2
a	a	k8xC
třetí	třetí	k4xOgFnSc1
armáda	armáda	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
40	#num#	k4
km	km	kA
od	od	k7c2
Cařihradu	Cařihrad	k1gInSc2
<g/>
,	,	kIx,
vojska	vojsko	k1gNnSc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
chtěla	chtít	k5eAaImAgFnS
podepsat	podepsat	k5eAaPmF
příměří	příměří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharská	bulharský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
však	však	k9
nesouhlasila	souhlasit	k5eNaImAgFnS
a	a	k8xC
podnikla	podniknout	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
vpád	vpád	k1gInSc4
na	na	k7c4
osmanské	osmanský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
však	však	k9
skončil	skončit	k5eAaPmAgInS
neúspěchem	neúspěch	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1912	#num#	k4
podepsána	podepsán	k2eAgFnSc1d1
mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zároveň	zároveň	k6eAd1
s	s	k7c7
úspěšným	úspěšný	k2eAgInSc7d1
postupem	postup	k1gInSc7
ostatních	ostatní	k2eAgFnPc2d1
bulharských	bulharský	k2eAgFnPc2d1
armád	armáda	k1gFnPc2
směrem	směr	k1gInSc7
k	k	k7c3
Cařihradu	Cařihrad	k1gInSc3
<g/>
,	,	kIx,
armáda	armáda	k1gFnSc1
protivníka	protivník	k1gMnSc2
přišla	přijít	k5eAaPmAgFnS
o	o	k7c4
dalších	další	k2eAgInPc2d1
12	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dařilo	dařit	k5eAaImAgNnS
se	se	k3xPyFc4
i	i	k9
postupu	postup	k1gInSc3
srbské	srbský	k2eAgFnSc2d1
<g/>
,	,	kIx,
řecké	řecký	k2eAgFnSc3d1
a	a	k8xC
černohorské	černohorský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Makedonie	Makedonie	k1gFnSc1
a	a	k8xC
Albánie	Albánie	k1gFnSc1
postupně	postupně	k6eAd1
Turky	Turek	k1gMnPc4
ničila	ničit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
méně	málo	k6eAd2
než	než	k8xS
měsíc	měsíc	k1gInSc4
byla	být	k5eAaImAgFnS
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
poražena	poražen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1912	#num#	k4
začaly	začít	k5eAaPmAgInP
v	v	k7c6
Londýně	Londýn	k1gInSc6
mírové	mírový	k2eAgInPc4d1
rozhovory	rozhovor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmínky	podmínka	k1gFnPc1
spojeneckých	spojenecký	k2eAgInPc2d1
států	stát	k1gInPc2
pro	pro	k7c4
Osmanskou	osmanský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
byly	být	k5eAaImAgFnP
jasně	jasně	k6eAd1
stanoveny	stanoven	k2eAgInPc1d1
-	-	kIx~
měla	mít	k5eAaImAgFnS
se	se	k3xPyFc4
vzdát	vzdát	k5eAaPmF
všech	všecek	k3xTgNnPc2
území	území	k1gNnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Mysie	Mysie	k1gFnSc2
a	a	k8xC
ostrovů	ostrov	k1gInPc2
v	v	k7c6
Egejském	egejský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
však	však	k9
nechtěli	chtít	k5eNaImAgMnP
o	o	k7c4
tato	tento	k3xDgNnPc4
území	území	k1gNnPc4
v	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
přijít	přijít	k5eAaPmF
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
se	se	k3xPyFc4
však	však	k9
vzdávali	vzdávat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1913	#num#	k4
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
Cařihradu	Cařihrad	k1gInSc2
uskutečnil	uskutečnit	k5eAaPmAgInS
převrat	převrat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
moci	moc	k1gFnSc3
se	se	k3xPyFc4
dostala	dostat	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
podporovaná	podporovaný	k2eAgFnSc1d1
Německem	Německo	k1gNnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
zájem	zájem	k1gInSc4
na	na	k7c4
pokračování	pokračování	k1gNnSc4
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
etapa	etapa	k1gFnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
osmanská	osmanský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
snažila	snažit	k5eAaImAgFnS
ve	v	k7c6
válce	válka	k1gFnSc6
o	o	k7c4
obrat	obrat	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc1
plány	plán	k1gInPc1
byly	být	k5eAaImAgInP
však	však	k9
vítězstvím	vítězství	k1gNnSc7
bulharské	bulharský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
na	na	k7c4
Gallipoli	Gallipoli	k1gNnSc4
velmi	velmi	k6eAd1
brzy	brzy	k6eAd1
překaženy	překažen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rukou	ruka	k1gFnPc6
Turků	turek	k1gInPc2
zbyly	zbýt	k5eAaPmAgInP
už	už	k9
jen	jen	k6eAd1
pevnosti	pevnost	k1gFnSc3
Odrin	Odrina	k1gFnPc2
<g/>
,	,	kIx,
Janina	Janin	k2eAgFnSc1d1
a	a	k8xC
Skadar	Skadar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dnech	den	k1gInPc6
11	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1913	#num#	k4
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
nejdůležitější	důležitý	k2eAgFnSc1d3
bitva	bitva	k1gFnSc1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
předtím	předtím	k6eAd1
obléhala	obléhat	k5eAaImAgFnS
105	#num#	k4
000	#num#	k4
bulharská	bulharský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
za	za	k7c2
pomoci	pomoc	k1gFnSc2
35	#num#	k4
000	#num#	k4
armády	armáda	k1gFnSc2
Srbů	Srb	k1gMnPc2
pevnost	pevnost	k1gFnSc1
Odrin	Odrin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turecký	turecký	k2eAgInSc1d1
velitel	velitel	k1gMnSc1
Šukri	Šukr	k1gMnPc1
Paša	paša	k1gMnSc1
se	s	k7c7
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1913	#num#	k4
Bulharům	Bulhar	k1gMnPc3
vzdal	vzdát	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Výsledek	výsledek	k1gInSc1
války	válka	k1gFnSc2
</s>
<s>
Teritoriální	teritoriální	k2eAgFnPc1d1
změny	změna	k1gFnPc1
po	po	k7c6
podpisu	podpis	k1gInSc6
Londýnské	londýnský	k2eAgFnSc2d1
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
(	(	kIx(
<g/>
nahoře	nahoře	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
Bukurešťské	bukurešťský	k2eAgFnSc6d1
mírové	mírový	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
(	(	kIx(
<g/>
dole	dole	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
úplně	úplně	k6eAd1
poražena	poražen	k2eAgFnSc1d1
byly	být	k5eAaImAgInP
v	v	k7c6
Londýně	Londýn	k1gInSc6
obnoveny	obnoven	k2eAgInPc4d1
rozhovory	rozhovor	k1gInPc4
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1913	#num#	k4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1913	#num#	k4
starý	starý	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
podepsána	podepsat	k5eAaPmNgFnS
Londýnská	londýnský	k2eAgFnSc1d1
mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
základě	základ	k1gInSc6
přišla	přijít	k5eAaPmAgFnS
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
o	o	k7c4
všechna	všechen	k3xTgNnPc4
území	území	k1gNnPc4
v	v	k7c6
Mysii	Mysie	k1gFnSc6
kromě	kromě	k7c2
oblastí	oblast	k1gFnPc2
v	v	k7c6
Albánii	Albánie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Rozpad	rozpad	k1gInSc1
Balkánského	balkánský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
</s>
<s>
Rozpory	rozpor	k1gInPc1
mezi	mezi	k7c7
spojenci	spojenec	k1gMnPc7
balkánských	balkánský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
se	se	k3xPyFc4
neprojevily	projevit	k5eNaPmAgInP
hned	hned	k6eAd1
po	po	k7c6
ukončení	ukončení	k1gNnSc6
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
tu	tu	k6eAd1
i	i	k9
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
však	však	k9
nenarušily	narušit	k5eNaPmAgFnP
vítězný	vítězný	k2eAgInSc4d1
postup	postup	k1gInSc4
Bulharska	Bulharsko	k1gNnSc2
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
kroků	krok	k1gInPc2
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
i	i	k9
přes	přes	k7c4
nevyřízené	vyřízený	k2eNgFnPc4d1
otázky	otázka	k1gFnPc4
rozdělení	rozdělení	k1gNnSc2
území	území	k1gNnSc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
po	po	k7c6
případném	případný	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
Balkánského	balkánský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
táhl	táhnout	k5eAaImAgInS
ještě	ještě	k6eAd1
od	od	k7c2
uzavření	uzavření	k1gNnSc2
řecko-bulharské	řecko-bulharský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
postupně	postupně	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
chce	chtít	k5eAaImIp3nS
zachovat	zachovat	k5eAaPmF
území	území	k1gNnSc4
kolem	kolem	k7c2
města	město	k1gNnSc2
Soluň	Soluň	k1gFnSc1
a	a	k8xC
část	část	k1gFnSc1
Makedonie	Makedonie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
zprvu	zprvu	k6eAd1
přiznalo	přiznat	k5eAaPmAgNnS
Bulharsku	Bulharsko	k1gNnSc3
právo	právo	k1gNnSc4
na	na	k7c4
větší	veliký	k2eAgFnSc4d2
část	část	k1gFnSc4
Makedonie	Makedonie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
průběhu	průběh	k1gInSc6
války	válka	k1gFnSc2
však	však	k9
svůj	svůj	k3xOyFgInSc4
názor	názor	k1gInSc4
změnilo	změnit	k5eAaPmAgNnS
a	a	k8xC
prosadilo	prosadit	k5eAaPmAgNnS
návrh	návrh	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zakládal	zakládat	k5eAaImAgInS
na	na	k7c6
principu	princip	k1gInSc6
okupace	okupace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
země	země	k1gFnSc1
si	se	k3xPyFc3
mohla	moct	k5eAaImAgFnS
připojit	připojit	k5eAaPmF
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
území	území	k1gNnSc4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
je	být	k5eAaImIp3nS
vojsky	vojsky	k6eAd1
okupováno	okupován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Napětí	napětí	k1gNnSc1
do	do	k7c2
sporu	spor	k1gInSc2
přineslo	přinést	k5eAaPmAgNnS
Rumunsko	Rumunsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
mírových	mírový	k2eAgInPc2d1
rozhovorů	rozhovor	k1gInPc2
v	v	k7c6
Londýně	Londýn	k1gInSc6
chtělo	chtít	k5eAaImAgNnS
kompenzaci	kompenzace	k1gFnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
připojení	připojení	k1gNnSc6
nových	nový	k2eAgNnPc2d1
území	území	k1gNnPc2
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
neutralitu	neutralita	k1gFnSc4
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
však	však	k9
mohlo	moct	k5eAaImAgNnS
poskytnout	poskytnout	k5eAaPmF
pouze	pouze	k6eAd1
Bulharsko	Bulharsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požadavky	požadavek	k1gInPc1
jeho	jeho	k3xOp3gMnPc2
spojenců	spojenec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
nechtěli	chtít	k5eNaImAgMnP
ustoupit	ustoupit	k5eAaPmF
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
otázce	otázka	k1gFnSc6
dělení	dělení	k1gNnSc2
území	území	k1gNnSc2
Makedonie	Makedonie	k1gFnSc1
<g/>
)	)	kIx)
mu	on	k3xPp3gMnSc3
škodily	škodit	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Car	car	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
nechtěl	chtít	k5eNaImAgMnS
v	v	k7c6
požadavku	požadavek	k1gInSc6
Ivana	Ivan	k1gMnSc2
Gešova	Gešovo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
vyzýval	vyzývat	k5eAaImAgMnS
k	k	k7c3
umírněnému	umírněný	k2eAgInSc3d1
postupu	postup	k1gInSc3
<g/>
,	,	kIx,
ustoupit	ustoupit	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
proruské	proruský	k2eAgNnSc4d1
vláda	vláda	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
se	s	k7c7
Stojanem	stojan	k1gInSc7
Danevem	Danev	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
si	se	k3xPyFc3
měla	mít	k5eAaImAgFnS
v	v	k7c6
této	tento	k3xDgFnSc6
těžké	těžký	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
získat	získat	k5eAaPmF
důvěru	důvěra	k1gFnSc4
Petrohradu	Petrohrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dva	dva	k4xCgInPc4
dny	den	k1gInPc4
po	po	k7c6
podpisu	podpis	k1gInSc6
Londýnské	londýnský	k2eAgFnSc2d1
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1913	#num#	k4
Srbsko	Srbsko	k1gNnSc1
a	a	k8xC
Řecko	Řecko	k1gNnSc1
podepsaly	podepsat	k5eAaPmAgInP
úmluvu	úmluva	k1gFnSc4
<g/>
,	,	kIx,
kterým	který	k3yQgNnSc7,k3yIgNnSc7,k3yRgNnSc7
se	se	k3xPyFc4
zavázaly	zavázat	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
společnou	společný	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
konečném	konečný	k2eAgInSc6d1
důsledku	důsledek	k1gInSc6
pro	pro	k7c4
Bulharsko	Bulharsko	k1gNnSc4
znamenalo	znamenat	k5eAaImAgNnS
obsazení	obsazení	k1gNnSc4
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
smlouvou	smlouva	k1gFnSc7
si	se	k3xPyFc3
jeho	jeho	k3xOp3gFnSc4
bývalí	bývalý	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
zajistili	zajistit	k5eAaPmAgMnP
společný	společný	k2eAgInSc4d1
postup	postup	k1gInSc4
proti	proti	k7c3
Bulharsku	Bulharsko	k1gNnSc3
<g/>
,	,	kIx,
včetně	včetně	k7c2
použití	použití	k1gNnSc2
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
Trojdohody	Trojdohoda	k1gFnSc2
(	(	kIx(
<g/>
zejména	zejména	k9
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
<g/>
)	)	kIx)
nechtěli	chtít	k5eNaImAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vypukl	vypuknout	k5eAaPmAgInS
další	další	k2eAgInSc4d1
ozbrojený	ozbrojený	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
a	a	k8xC
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
byly	být	k5eAaImAgInP
opačného	opačný	k2eAgInSc2d1
názoru	názor	k1gInSc2
a	a	k8xC
chtěly	chtít	k5eAaImAgFnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Balkánský	balkánský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
definitivně	definitivně	k6eAd1
rozpadl	rozpadnout	k5eAaPmAgInS
<g/>
,	,	kIx,
protože	protože	k8xS
vznikl	vzniknout	k5eAaPmAgInS
pod	pod	k7c7
záštitou	záštita	k1gFnSc7
ruské	ruský	k2eAgFnSc2d1
diplomacie	diplomacie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
zhoršovala	zhoršovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulharská	bulharský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
se	se	k3xPyFc4
z	z	k7c2
Thrákie	Thrákie	k1gFnSc2
přesunula	přesunout	k5eAaPmAgFnS
k	k	k7c3
západní	západní	k2eAgFnSc3d1
hranici	hranice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1913	#num#	k4
car	car	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
svůj	svůj	k3xOyFgInSc4
krok	krok	k1gInSc4
oznámil	oznámit	k5eAaPmAgMnS
vládě	vláda	k1gFnSc3
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgInS
rozkaz	rozkaz	k1gInSc1
k	k	k7c3
napadení	napadení	k1gNnSc3
bývalých	bývalý	k2eAgMnPc2d1
spojenců	spojenec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
však	však	k9
již	již	k6eAd1
velmi	velmi	k6eAd1
pozdě	pozdě	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
Srbsko	Srbsko	k1gNnSc4
a	a	k8xC
Řecko	Řecko	k1gNnSc4
již	již	k6eAd1
obdržely	obdržet	k5eAaPmAgFnP
svůj	svůj	k3xOyFgInSc4
podíl	podíl	k1gInSc4
a	a	k8xC
nechtěly	chtít	k5eNaImAgFnP
skončit	skončit	k5eAaPmF
započatou	započatý	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Erickson	Erickson	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
52	#num#	k4
<g/>
↑	↑	k?
Hall	Hall	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
16	#num#	k4
<g/>
↑	↑	k?
Hall	Hall	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
18	#num#	k4
<g/>
↑	↑	k?
Erickson	Erickson	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
70	#num#	k4
<g/>
↑	↑	k?
Erickson	Ericksona	k1gFnPc2
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
691	#num#	k4
2	#num#	k4
Erickson	Ericksona	k1gFnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
329	#num#	k4
<g/>
↑	↑	k?
http://www.bulgarianartillery.it/Bulgarian%20Artillery%201/T_OOB/Troops%20losses_1912-13.htm	http://www.bulgarianartillery.it/Bulgarian%20Artillery%201/T_OOB/Troops%20losses_1912-13.htma	k1gFnPc2
<g/>
↑	↑	k?
Hellenic	Hellenice	k1gFnPc2
Army	Arma	k1gFnSc2
General	General	k1gMnSc1
staff	staff	k1gMnSc1
<g/>
:	:	kIx,
A	a	k9
concise	concise	k1gFnSc1
history	histor	k1gInPc1
of	of	k?
the	the	k?
Balkan	Balkan	k1gInSc1
Wars	Wars	k1gInSc1
<g/>
,	,	kIx,
page	page	k1gInSc1
287	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Β	Β	k?
ε	ε	k?
3	#num#	k4
<g/>
,	,	kIx,
Ο	Ο	k?
Β	Β	k?
Π	Π	k?
<g/>
,	,	kIx,
Β	Β	k?
Κ	Κ	k?
and	and	k?
Χ	Χ	k?
Κ	Κ	k?
<g/>
,	,	kIx,
translation	translation	k1gInSc4
by	by	kYmCp3nS
Ι	Ι	k?
Π	Π	k?
<g/>
,	,	kIx,
CDRSEE	CDRSEE	kA
<g/>
,	,	kIx,
Thessaloniki	Thessaloniki	k1gNnSc2
2005	#num#	k4
<g/>
,	,	kIx,
page	page	k1gFnSc1
120	#num#	k4
<g/>
,	,	kIx,
<g/>
(	(	kIx(
<g/>
Greek	Greek	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gMnSc1
from	from	k1gMnSc1
http://www.cdsee.org	http://www.cdsee.org	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
První	první	k4xOgFnSc1
balkánská	balkánský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
