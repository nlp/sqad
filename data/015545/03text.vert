<s>
Lukovské	Lukovský	k2eAgNnSc1d1
podhradí	podhradí	k1gNnSc1
</s>
<s>
Lukovské	Lukovské	k2eAgFnSc1d1
podhradíForma	podhradíForma	k1gFnSc1
</s>
<s>
sdružení	sdružení	k1gNnSc1
právnických	právnický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
Předseda	předseda	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Lubomír	Lubomír	k1gMnSc1
Doležel	doležet	k5eAaPmAgMnS
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Fryšták	Fryšták	k1gMnSc1
Datum	datum	k1gInSc4
založení	založení	k1gNnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2003	#num#	k4
Poloha	poloha	k1gFnSc1
Kraj	kraj	k7c2
</s>
<s>
Zlínský	zlínský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Zlín	Zlín	k1gInSc1
Kontakt	kontakt	k1gInSc1
E-mail	e-mail	k1gInSc4
</s>
<s>
starosta@frystak.cz	starosta@frystak.cz	k1gInSc1
Web	web	k1gInSc1
</s>
<s>
www.frystak.cz	www.frystak.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Lukovské	Lukovský	k2eAgNnSc1d1
podhradí	podhradí	k1gNnSc1
je	být	k5eAaImIp3nS
sdružení	sdružení	k1gNnSc4
právnických	právnický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
v	v	k7c6
okresu	okres	k1gInSc6
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
sídlem	sídlo	k1gNnSc7
je	být	k5eAaImIp3nS
Fryšták	Fryšták	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
vzájemná	vzájemný	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
,	,	kIx,
ekologických	ekologický	k2eAgInPc2d1
projektů	projekt	k1gInPc2
a	a	k8xC
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
rozvíjení	rozvíjení	k1gNnSc2
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
obnovy	obnova	k1gFnSc2
tradic	tradice	k1gFnPc2
a	a	k8xC
lidových	lidový	k2eAgNnPc2d1
řemesel	řemeslo	k1gNnPc2
<g/>
,	,	kIx,
dopravní	dopravní	k2eAgFnSc2d1
obslužnosti	obslužnost	k1gFnSc2
a	a	k8xC
sportovních	sportovní	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdružuje	sdružovat	k5eAaImIp3nS
celkem	celkem	k6eAd1
8	#num#	k4
obcí	obec	k1gFnPc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
sdružené	sdružený	k2eAgFnPc1d1
v	v	k7c6
mikroregionu	mikroregion	k1gInSc6
</s>
<s>
Kašava	Kašava	k1gFnSc1
</s>
<s>
Lukov	Lukov	k1gInSc1
</s>
<s>
Lukoveček	Lukoveček	k1gMnSc1
</s>
<s>
Fryšták	Fryšták	k1gMnSc1
</s>
<s>
Držková	držkový	k2eAgFnSc1d1
</s>
<s>
Vlčková	Vlčková	k1gFnSc1
</s>
<s>
Racková	Racková	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
