<s>
Sunnité	sunnita	k1gMnPc1	sunnita
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
po	po	k7c6	po
Mohamedově	Mohamedův	k2eAgFnSc6d1	Mohamedova
smrti	smrt	k1gFnSc6	smrt
uznali	uznat	k5eAaPmAgMnP	uznat
za	za	k7c4	za
nástupce	nástupce	k1gMnSc4	nástupce
Abú	abú	k1gMnSc4	abú
Bakra	Bakr	k1gMnSc4	Bakr
a	a	k8xC	a
oddělili	oddělit	k5eAaPmAgMnP	oddělit
se	se	k3xPyFc4	se
tak	tak	k9	tak
od	od	k7c2	od
šíitů	šíita	k1gMnPc2	šíita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
za	za	k7c4	za
nástupce	nástupce	k1gMnSc4	nástupce
považovali	považovat	k5eAaImAgMnP	považovat
člena	člen	k1gMnSc4	člen
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
-	-	kIx~	-
zetě	zeť	k1gMnSc2	zeť
Alího	Alí	k1gMnSc2	Alí
<g/>
.	.	kIx.	.
</s>
