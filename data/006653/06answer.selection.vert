<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k1gMnPc4	domácí
vždy	vždy	k6eAd1	vždy
sloužila	sloužit	k5eAaImAgFnS	sloužit
člověku	člověk	k1gMnSc6	člověk
především	především	k9	především
jako	jako	k9	jako
lovec	lovec	k1gMnSc1	lovec
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
také	také	k9	také
jako	jako	k9	jako
společník	společník	k1gMnSc1	společník
a	a	k8xC	a
mazlíček	mazlíček	k1gMnSc1	mazlíček
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
