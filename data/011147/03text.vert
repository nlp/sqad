<p>
<s>
Panhard	Panhard	k1gMnSc1	Panhard
&	&	k?	&
Levassor	Levassor	k1gMnSc1	Levassor
Dynamic	Dynamic	k1gMnSc1	Dynamic
je	být	k5eAaImIp3nS	být
čtyřdveřová	čtyřdveřový	k2eAgFnSc1d1	čtyřdveřová
limuzína	limuzína	k1gFnSc1	limuzína
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
představila	představit	k5eAaPmAgFnS	představit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
automobilka	automobilka	k1gFnSc1	automobilka
Panhard	Panhard	k1gMnSc1	Panhard
&	&	k?	&
Levassor	Levassor	k1gMnSc1	Levassor
jako	jako	k8xS	jako
nástupce	nástupce	k1gMnSc1	nástupce
modelů	model	k1gInPc2	model
CS	CS	kA	CS
a	a	k8xC	a
DS	DS	kA	DS
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
měl	mít	k5eAaImAgInS	mít
elegantní	elegantní	k2eAgFnSc4d1	elegantní
proudnicovou	proudnicový	k2eAgFnSc4d1	proudnicová
karosérii	karosérie	k1gFnSc4	karosérie
s	s	k7c7	s
částečně	částečně	k6eAd1	částečně
zakrytými	zakrytý	k2eAgNnPc7d1	zakryté
koly	kolo	k1gNnPc7	kolo
<g/>
,	,	kIx,	,
trojdílné	trojdílný	k2eAgNnSc4d1	trojdílné
čelní	čelní	k2eAgNnSc4d1	čelní
okno	okno	k1gNnSc4	okno
použité	použitý	k2eAgNnSc4d1	Použité
už	už	k6eAd1	už
u	u	k7c2	u
vozů	vůz	k1gInPc2	vůz
CS	CS	kA	CS
verze	verze	k1gFnSc1	verze
Panoramique	Panoramique	k1gFnSc1	Panoramique
a	a	k8xC	a
v	v	k7c6	v
předních	přední	k2eAgInPc6d1	přední
blatnících	blatník	k1gInPc6	blatník
integrované	integrovaný	k2eAgInPc4d1	integrovaný
kryté	krytý	k2eAgInPc4d1	krytý
světlomety	světlomet	k1gInPc4	světlomet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prototyp	prototyp	k1gInSc1	prototyp
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Dynamic	Dynamic	k1gMnSc1	Dynamic
20	[number]	k4	20
CV	CV	kA	CV
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1936	[number]	k4	1936
byl	být	k5eAaImAgInS	být
osazen	osadit	k5eAaPmNgInS	osadit
řadovým	řadový	k2eAgInSc7d1	řadový
šestiválcem	šestiválec	k1gInSc7	šestiválec
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
3	[number]	k4	3
485	[number]	k4	485
cm3	cm3	k4	cm3
(	(	kIx(	(
<g/>
20	[number]	k4	20
CV	CV	kA	CV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
mechanických	mechanický	k2eAgFnPc2d1	mechanická
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
Louis	Louis	k1gMnSc1	Louis
Delagarde	Delagard	k1gMnSc5	Delagard
<g/>
,	,	kIx,	,
karosérii	karosérie	k1gFnSc4	karosérie
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
Louis	Louis	k1gMnSc1	Louis
Bionier	Bionier	k1gMnSc1	Bionier
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
1936	[number]	k4	1936
sériově	sériově	k6eAd1	sériově
vyráběný	vyráběný	k2eAgInSc4d1	vyráběný
model	model	k1gInSc4	model
Dynamic	Dynamice	k1gInPc2	Dynamice
130	[number]	k4	130
používal	používat	k5eAaImAgInS	používat
bezventilový	bezventilový	k2eAgInSc1d1	bezventilový
šoupátkový	šoupátkový	k2eAgInSc1d1	šoupátkový
řadový	řadový	k2eAgInSc1d1	řadový
šestiválec	šestiválec	k1gInSc1	šestiválec
2	[number]	k4	2
516	[number]	k4	516
cm3	cm3	k4	cm3
(	(	kIx(	(
<g/>
14	[number]	k4	14
CV	CV	kA	CV
<g/>
)	)	kIx)	)
z	z	k7c2	z
předcházejícího	předcházející	k2eAgInSc2d1	předcházející
vozu	vůz	k1gInSc2	vůz
CS	CS	kA	CS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nejslabší	slabý	k2eAgFnSc6d3	nejslabší
verzi	verze	k1gFnSc6	verze
130	[number]	k4	130
byly	být	k5eAaImAgFnP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
šestimístné	šestimístný	k2eAgFnPc1d1	šestimístná
limuzíny	limuzína	k1gFnPc1	limuzína
bez	bez	k7c2	bez
zavazadlového	zavazadlový	k2eAgInSc2d1	zavazadlový
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
čtyřmístné	čtyřmístný	k2eAgFnSc2d1	čtyřmístná
limuzíny	limuzína	k1gFnSc2	limuzína
se	s	k7c7	s
zavazadlovým	zavazadlový	k2eAgInSc7d1	zavazadlový
prostorem	prostor	k1gInSc7	prostor
vzadu	vzadu	k6eAd1	vzadu
(	(	kIx(	(
<g/>
Berline	Berlin	k1gInSc5	Berlin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
karosérie	karosérie	k1gFnPc1	karosérie
kupé	kupé	k1gNnSc2	kupé
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
verze	verze	k1gFnSc1	verze
kabriolet	kabriolet	k1gInSc4	kabriolet
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
jen	jen	k9	jen
358	[number]	k4	358
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
vozy	vůz	k1gInPc1	vůz
získaly	získat	k5eAaPmAgInP	získat
četná	četný	k2eAgNnPc4d1	četné
ocenění	ocenění	k1gNnSc4	ocenění
v	v	k7c6	v
soutěžích	soutěž	k1gFnPc6	soutěž
elegance	elegance	k1gFnSc2	elegance
<g/>
,	,	kIx,	,
unikátní	unikátní	k2eAgFnSc2d1	unikátní
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
řidič	řidič	k1gMnSc1	řidič
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
vychutnat	vychutnat	k5eAaPmF	vychutnat
výhled	výhled	k1gInSc4	výhled
z	z	k7c2	z
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
seděl	sedět	k5eAaImAgMnS	sedět
za	za	k7c7	za
volantem	volant	k1gInSc7	volant
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
vpředu	vpředu	k6eAd1	vpředu
uprostřed	uprostřed	k7c2	uprostřed
<g/>
.	.	kIx.	.
<g/>
Paralelně	paralelně	k6eAd1	paralelně
byl	být	k5eAaImAgInS	být
vyráběn	vyráběn	k2eAgInSc4d1	vyráběn
model	model	k1gInSc4	model
Dynamic	Dynamice	k1gFnPc2	Dynamice
140	[number]	k4	140
<g/>
,	,	kIx,	,
používající	používající	k2eAgInSc1d1	používající
motor	motor	k1gInSc1	motor
z	z	k7c2	z
modelu	model	k1gInSc2	model
CS	CS	kA	CS
Spécial	Spécial	k1gInSc1	Spécial
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
2	[number]	k4	2
861	[number]	k4	861
cm3	cm3	k4	cm3
(	(	kIx(	(
<g/>
16	[number]	k4	16
CV	CV	kA	CV
<g/>
)	)	kIx)	)
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
70	[number]	k4	70
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
51	[number]	k4	51
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc4	typ
si	se	k3xPyFc3	se
zákazníci	zákazník	k1gMnPc1	zákazník
objednávali	objednávat	k5eAaImAgMnP	objednávat
nejčastěji	často	k6eAd3	často
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ukončení	ukončení	k1gNnSc2	ukončení
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
začátku	začátek	k1gInSc2	začátek
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
2	[number]	k4	2
230	[number]	k4	230
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
vozy	vůz	k1gInPc1	vůz
série	série	k1gFnSc2	série
produkované	produkovaný	k2eAgFnSc2d1	produkovaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
už	už	k6eAd1	už
měly	mít	k5eAaImAgInP	mít
opět	opět	k6eAd1	opět
volant	volant	k1gInSc4	volant
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
také	také	k6eAd1	také
skončila	skončit	k5eAaPmAgFnS	skončit
výroba	výroba	k1gFnSc1	výroba
posledního	poslední	k2eAgInSc2d1	poslední
osobního	osobní	k2eAgInSc2d1	osobní
automobilu	automobil	k1gInSc2	automobil
používající	používající	k2eAgInSc1d1	používající
bezventilový	bezventilový	k2eAgInSc1d1	bezventilový
šoupátkový	šoupátkový	k2eAgInSc1d1	šoupátkový
motor	motor	k1gInSc1	motor
licence	licence	k1gFnSc2	licence
Knight	Knighta	k1gFnPc2	Knighta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
model	model	k1gInSc1	model
Dynamic	Dynamice	k1gInPc2	Dynamice
160	[number]	k4	160
jako	jako	k8xC	jako
nástupce	nástupce	k1gMnSc1	nástupce
typu	typ	k1gInSc2	typ
DS	DS	kA	DS
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
motoru	motor	k1gInSc2	motor
byl	být	k5eAaImAgInS	být
3	[number]	k4	3
834	[number]	k4	834
cm3	cm3	k4	cm3
(	(	kIx(	(
<g/>
22	[number]	k4	22
CV	CV	kA	CV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
153	[number]	k4	153
těchto	tento	k3xDgInPc2	tento
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Panhard	Panhard	k1gMnSc1	Panhard
&	&	k?	&
Levassor	Levassor	k1gMnSc1	Levassor
Dynamic	Dynamic	k1gMnSc1	Dynamic
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
Vermeylen	Vermeylen	k2eAgMnSc1d1	Vermeylen
<g/>
:	:	kIx,	:
Panhard	Panhard	k1gMnSc1	Panhard
&	&	k?	&
Levassor	Levassor	k1gMnSc1	Levassor
entre	entr	k1gInSc5	entr
tradition	tradition	k1gInSc4	tradition
et	et	k?	et
modernité	modernitý	k2eAgFnSc2d1	modernitý
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ETAI	ETAI	kA	ETAI
<g/>
,	,	kIx,	,
Boulogne-Billancourt	Boulogne-Billancourt	k1gInSc1	Boulogne-Billancourt
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
2-7268-9406-2	[number]	k4	2-7268-9406-2
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Panhard	Panharda	k1gFnPc2	Panharda
&	&	k?	&
Levassor	Levassor	k1gInSc1	Levassor
Dynamic	Dynamice	k1gFnPc2	Dynamice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
fotografie	fotografie	k1gFnSc1	fotografie
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
vozů	vůz	k1gInPc2	vůz
</s>
</p>
