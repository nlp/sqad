<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc6	první
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
jediným	jediný	k2eAgMnSc7d1	jediný
Slovákem	Slovák	k1gMnSc7	Slovák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
Oscar	Oscar	k1gMnSc1	Oscar
<g/>
?	?	kIx.	?
</s>
