<s>
Pyladés	Pyladés	k6eAd1
</s>
<s>
Pyladés	Pyladés	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Élektra	Élektra	k1gFnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
MedonStrophius	MedonStrophius	k1gInSc4
Rodiče	rodič	k1gMnPc1
</s>
<s>
Strofios	Strofios	k1gInSc1
a	a	k8xC
Anaxibia	Anaxibia	k1gFnSc1
<g/>
,	,	kIx,
Cyndragora	Cyndragora	k1gFnSc1
a	a	k8xC
Astyoché	Astyochý	k2eAgFnPc1d1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Astydameia	Astydameia	k1gFnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pyladés	Pyladés	k1gInSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Π	Π	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Pylades	Pylades	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
řecké	řecký	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
syn	syn	k1gMnSc1
fóckého	fócký	k2eAgMnSc2d1
krále	král	k1gMnSc2
Strofia	Strofius	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Astyochey	Astyochea	k1gFnSc2
(	(	kIx(
<g/>
zvané	zvaný	k2eAgNnSc1d1
též	též	k9
Anaxibia	Anaxibia	k2gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgInS
vychováván	vychovávat	k5eAaImNgInS
společně	společně	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
přítelem	přítel	k1gMnSc7
Orestem	Orestes	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
po	po	k7c4
zavraždění	zavraždění	k1gNnSc4
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Agamemnona	Agamemnon	k1gMnSc2
<g/>
,	,	kIx,
mykénského	mykénský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
,	,	kIx,
našel	najít	k5eAaPmAgMnS
útočiště	útočiště	k1gNnSc4
u	u	k7c2
krále	král	k1gMnSc2
Strofia	Strofius	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
obou	dva	k4xCgMnPc2
hochů	hoch	k1gMnPc2
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
velcí	velký	k2eAgMnPc1d1
přátelé	přítel	k1gMnPc1
na	na	k7c4
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Orestés	Orestés	k1gInSc1
dospěl	dochvít	k5eAaPmAgInS
a	a	k8xC
měl	mít	k5eAaImAgInS
promyšlenou	promyšlený	k2eAgFnSc4d1
pomstu	pomsta	k1gFnSc4
vrahům	vrah	k1gMnPc3
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pyladés	Pyladés	k1gInSc4
ho	on	k3xPp3gInSc4
doprovodil	doprovodit	k5eAaPmAgInS
do	do	k7c2
Mykén	Mykény	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Orestés	Orestés	k1gInSc1
zabil	zabít	k5eAaPmAgInS
zločinného	zločinný	k2eAgMnSc4d1
Aigistha	Aigisth	k1gMnSc4
i	i	k8xC
svou	svůj	k3xOyFgFnSc4
matku	matka	k1gFnSc4
Klytaimnéstru	Klytaimnéstra	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zradila	zradit	k5eAaPmAgFnS
manžela	manžel	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
vraždu	vražda	k1gFnSc4
zosnovala	zosnovat	k5eAaPmAgFnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
milencem	milenec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orestés	Orestés	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
novým	nový	k2eAgMnSc7d1
mykénským	mykénský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
lid	lid	k1gInSc1
považoval	považovat	k5eAaImAgInS
pomstu	pomsta	k1gFnSc4
a	a	k8xC
vraždu	vražda	k1gFnSc4
za	za	k7c4
pochopitelnou	pochopitelný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Oresta	Orestes	k1gMnSc4
však	však	k9
pronásledovaly	pronásledovat	k5eAaImAgInP
za	za	k7c4
hrůzný	hrůzný	k2eAgInSc4d1
čin	čin	k1gInSc4
Erínye	Eríny	k1gFnSc2
<g/>
,	,	kIx,
bohyně	bohyně	k1gFnSc2
pomsty	pomsta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
očištění	očištění	k1gNnSc4
musel	muset	k5eAaImAgInS
podle	podle	k7c2
výkladu	výklad	k1gInSc2
věštkyně	věštkyně	k1gFnSc2
podniknout	podniknout	k5eAaPmF
dlouhou	dlouhý	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
daleké	daleký	k2eAgMnPc4d1
Tauridy	Taurid	k1gInPc4
a	a	k8xC
tam	tam	k6eAd1
se	se	k3xPyFc4
kát	kát	k5eAaImF
u	u	k7c2
oltáře	oltář	k1gInSc2
bohyně	bohyně	k1gFnSc2
Artemidy	Artemida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pyladés	Pyladés	k1gInSc1
podnikl	podniknout	k5eAaPmAgInS
výpravu	výprava	k1gFnSc4
s	s	k7c7
ním	on	k3xPp3gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgMnPc1
však	však	k9
byli	být	k5eAaImAgMnP
hned	hned	k6eAd1
zajati	zajat	k2eAgMnPc1d1
a	a	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
obětován	obětovat	k5eAaBmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pyladés	Pyladés	k1gInSc1
okamžitě	okamžitě	k6eAd1
nabídl	nabídnout	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
za	za	k7c4
život	život	k1gInSc4
přítelův	přítelův	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
okamžiku	okamžik	k1gInSc6
je	být	k5eAaImIp3nS
však	však	k9
oba	dva	k4xCgMnPc1
zachránila	zachránit	k5eAaPmAgFnS
kněžka	kněžka	k1gFnSc1
Ífigeneia	Ífigeneia	k1gFnSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
brzy	brzy	k6eAd1
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
nejstarší	starý	k2eAgFnSc1d3
Orestova	Orestův	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
kdysi	kdysi	k6eAd1
dávno	dávno	k6eAd1
unesla	unést	k5eAaPmAgFnS
bohyně	bohyně	k1gFnSc1
Artemis	Artemis	k1gFnSc1
<g/>
,	,	kIx,
uchránivši	uchránit	k5eAaPmDgFnS
ji	on	k3xPp3gFnSc4
před	před	k7c7
obětováním	obětování	k1gNnSc7
pro	pro	k7c4
zdar	zdar	k1gInSc4
výpravy	výprava	k1gFnSc2
jejich	jejich	k3xOp3gMnSc2
otce	otec	k1gMnSc2
Agamemnona	Agamemnon	k1gMnSc2
do	do	k7c2
války	válka	k1gFnSc2
proti	proti	k7c3
Tróji	Trója	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
vše	všechen	k3xTgNnSc1
vysvětlilo	vysvětlit	k5eAaPmAgNnS
<g/>
,	,	kIx,
Ífigeneia	Ífigeneia	k1gFnSc1
lstí	lest	k1gFnPc2
zajistila	zajistit	k5eAaPmAgFnS
propuštění	propuštění	k1gNnPc4
obou	dva	k4xCgMnPc2
mladíků	mladík	k1gMnPc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
nimi	on	k3xPp3gInPc7
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
do	do	k7c2
vlasti	vlast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orestés	Orestés	k1gInSc1
znovu	znovu	k6eAd1
získal	získat	k5eAaPmAgInS
mykénský	mykénský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
a	a	k8xC
Pyladés	Pyladés	k1gInSc4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
sestrou	sestra	k1gFnSc7
Élektrou	Élektra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pyladés	Pyladés	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
symbolem	symbol	k1gInSc7
obětavého	obětavý	k2eAgNnSc2d1
přátelství	přátelství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odraz	odraz	k1gInSc1
v	v	k7c6
umění	umění	k1gNnSc6
</s>
<s>
Postava	postava	k1gFnSc1
věrného	věrný	k2eAgMnSc2d1
přítele	přítel	k1gMnSc2
Pylada	Pylades	k1gMnSc2
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
všude	všude	k6eAd1
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
zpracovány	zpracován	k2eAgInPc4d1
Orestovy	Orestův	k2eAgInPc4d1
osudy	osud	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Aischylova	Aischylův	k2eAgFnSc1d1
tragédie	tragédie	k1gFnSc1
Oresteia	Oresteia	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
r.	r.	kA
458	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Sofoklova	Sofoklův	k2eAgFnSc1d1
a	a	k8xC
Eurípidova	Eurípidův	k2eAgFnSc1d1
Élektra	Élektra	k1gFnSc1
<g/>
,	,	kIx,
Eurípidova	Eurípidův	k2eAgFnSc1d1
Ífigenie	Ífigenie	k1gFnSc1
na	na	k7c6
Tauridě	Taurida	k1gFnSc6
a	a	k8xC
Orestés	Orestés	k1gInSc4
(	(	kIx(
<g/>
z	z	k7c2
roku	rok	k1gInSc2
408	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
pozoruhodné	pozoruhodný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
římské	římský	k2eAgNnSc1d1
sousoší	sousoší	k1gNnSc1
Orestés	Orestésa	k1gFnPc2
a	a	k8xC
Pyladés	Pyladésa	k1gFnPc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
v	v	k7c6
pařížském	pařížský	k2eAgInSc6d1
Louvru	Louvre	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Slovník	slovník	k1gInSc1
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
nakl	naknout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1974	#num#	k4
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Zamarovský	Zamarovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Bohové	bůh	k1gMnPc1
a	a	k8xC
hrdinové	hrdina	k1gMnPc1
antických	antický	k2eAgFnPc2d1
bájí	báj	k1gFnPc2
</s>
<s>
Graves	Graves	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
,	,	kIx,
Řecké	řecký	k2eAgInPc4d1
mýty	mýtus	k1gInPc4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7309-153-4	80-7309-153-4	k4
</s>
<s>
Houtzager	Houtzager	k1gInSc1
<g/>
,	,	kIx,
Guus	Guus	k1gInSc1
<g/>
,	,	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
řecké	řecký	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7234-287-8	80-7234-287-8	k4
</s>
<s>
Gerhard	Gerhard	k1gMnSc1
Löwe	Löw	k1gInSc2
<g/>
,	,	kIx,
Heindrich	Heindrich	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Stoll	Stoll	k1gMnSc1
<g/>
,	,	kIx,
ABC	ABC	kA
Antiky	antika	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
316449885	#num#	k4
</s>
