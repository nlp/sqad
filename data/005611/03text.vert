<s>
Včela	včela	k1gFnSc1	včela
medonosná	medonosný	k2eAgFnSc1d1	medonosná
Apis	Apis	k1gInSc4	Apis
mellifera	mellifer	k1gMnSc2	mellifer
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
blanokřídlý	blanokřídlý	k2eAgInSc4d1	blanokřídlý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
zástupců	zástupce	k1gMnPc2	zástupce
společenského	společenský	k2eAgInSc2d1	společenský
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
extrémní	extrémní	k2eAgFnSc7d1	extrémní
geografickou	geografický	k2eAgFnSc7d1	geografická
izolací	izolace	k1gFnSc7	izolace
(	(	kIx(	(
<g/>
alopatrická	alopatrický	k2eAgFnSc1d1	alopatrická
speciace	speciace	k1gFnSc1	speciace
<g/>
)	)	kIx)	)
od	od	k7c2	od
mateřské	mateřský	k2eAgFnSc2d1	mateřská
populace	populace	k1gFnSc2	populace
včely	včela	k1gFnSc2	včela
východní	východní	k2eAgFnSc2d1	východní
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
evolučně	evolučně	k6eAd1	evolučně
"	"	kIx"	"
<g/>
nedávné	dávný	k2eNgFnSc6d1	nedávná
<g/>
"	"	kIx"	"
–	–	k?	–
asi	asi	k9	asi
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
lety	let	k1gInPc7	let
(	(	kIx(	(
<g/>
Oldroyd	Oldroyd	k1gInSc1	Oldroyd
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
původního	původní	k2eAgNnSc2d1	původní
rozšíření	rozšíření	k1gNnSc2	rozšíření
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
a	a	k8xC	a
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
byla	být	k5eAaImAgFnS	být
přivezena	přivézt	k5eAaPmNgFnS	přivézt
až	až	k9	až
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
na	na	k7c4	na
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
až	až	k6eAd1	až
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
hnízda	hnízdo	k1gNnPc4	hnízdo
staví	stavit	k5eAaImIp3nS	stavit
na	na	k7c6	na
chráněných	chráněný	k2eAgNnPc6d1	chráněné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
plásty	plást	k1gInPc4	plást
umístěny	umístěn	k2eAgInPc4d1	umístěn
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
druhy	druh	k1gInPc7	druh
včel	včela	k1gFnPc2	včela
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
hospodářsky	hospodářsky	k6eAd1	hospodářsky
využívána	využívat	k5eAaPmNgFnS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Včela	včela	k1gFnSc1	včela
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
kmene	kmen	k1gInSc2	kmen
členovců	členovec	k1gMnPc2	členovec
(	(	kIx(	(
<g/>
Arthropoda	Arthropoda	k1gMnSc1	Arthropoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
tělo	tělo	k1gNnSc1	tělo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
–	–	k?	–
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
hrudi	hruď	k1gFnSc2	hruď
a	a	k8xC	a
zadečku	zadeček	k1gInSc2	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
oddělené	oddělený	k2eAgFnPc4d1	oddělená
zúžením	zúžení	k1gNnSc7	zúžení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohyblivost	pohyblivost	k1gFnSc4	pohyblivost
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
(	(	kIx(	(
<g/>
caput	caput	k1gInSc1	caput
<g/>
)	)	kIx)	)
včely	včela	k1gFnSc2	včela
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
hrudí	hruď	k1gFnSc7	hruď
spojena	spojit	k5eAaPmNgFnS	spojit
tenkým	tenký	k2eAgNnSc7d1	tenké
zúžením	zúžení	k1gNnSc7	zúžení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
jí	on	k3xPp3gFnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Oporu	opor	k1gInSc2	opor
svalstvu	svalstvo	k1gNnSc3	svalstvo
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
kostra	kostra	k1gFnSc1	kostra
(	(	kIx(	(
<g/>
tentorium	tentorium	k1gNnSc1	tentorium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
boku	bok	k1gInSc6	bok
temene	temeno	k1gNnSc2	temeno
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
vertex	vertex	k1gInSc1	vertex
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
včela	včela	k1gFnSc1	včela
dvě	dva	k4xCgFnPc4	dva
složené	složený	k2eAgInPc1d1	složený
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
oculi	ocunout	k5eAaImAgMnP	ocunout
compositi	composit	k5eAaPmF	composit
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
temena	temeno	k1gNnSc2	temeno
tři	tři	k4xCgFnPc4	tři
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
ocelli	ocellit	k5eAaPmRp2nS	ocellit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
rozmístěné	rozmístěný	k2eAgInPc1d1	rozmístěný
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vrchol	vrchol	k1gInSc1	vrchol
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
hlavy	hlava	k1gFnSc2	hlava
směřuje	směřovat	k5eAaImIp3nS	směřovat
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
spodního	spodní	k2eAgNnSc2d1	spodní
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
oka	oko	k1gNnSc2	oko
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
středem	středem	k7c2	středem
temena	temeno	k1gNnSc2	temeno
tenká	tenký	k2eAgFnSc1d1	tenká
brázdička	brázdička	k1gFnSc1	brázdička
(	(	kIx(	(
<g/>
carina	carina	k1gFnSc1	carina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
končící	končící	k2eAgInPc1d1	končící
u	u	k7c2	u
kořenů	kořen	k1gInPc2	kořen
tykadel	tykadlo	k1gNnPc2	tykadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tykadla	tykadlo	k1gNnPc1	tykadlo
(	(	kIx(	(
<g/>
antennae	antenna	k1gFnSc2	antenna
<g/>
)	)	kIx)	)
včel	včela	k1gFnPc2	včela
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc1	dva
článkovitá	článkovitý	k2eAgNnPc1d1	článkovité
ústrojí	ústrojí	k1gNnPc1	ústrojí
umístěná	umístěný	k2eAgNnPc1d1	umístěné
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
jamkách	jamka	k1gFnPc6	jamka
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
přední	přední	k2eAgFnSc2d1	přední
strany	strana	k1gFnSc2	strana
hlavy	hlava	k1gFnSc2	hlava
nad	nad	k7c7	nad
čelním	čelní	k2eAgInSc7d1	čelní
štítem	štít	k1gInSc7	štít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tykadlech	tykadlo	k1gNnPc6	tykadlo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yQgFnPc2	který
včela	včela	k1gFnSc1	včela
dovede	dovést	k5eAaPmIp3nS	dovést
vnímat	vnímat	k5eAaImF	vnímat
čichová	čichový	k2eAgNnPc4d1	čichové
a	a	k8xC	a
hmatová	hmatový	k2eAgNnPc4d1	hmatové
podráždění	podráždění	k1gNnPc4	podráždění
<g/>
.	.	kIx.	.
</s>
<s>
Tykadla	tykadlo	k1gNnPc4	tykadlo
dělnic	dělnice	k1gFnPc2	dělnice
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
12	[number]	k4	12
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
u	u	k7c2	u
trubce	trubec	k1gMnSc2	trubec
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgMnSc1d3	nejdelší
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc1d1	základní
článek	článek	k1gInSc1	článek
tykadla	tykadlo	k1gNnSc2	tykadlo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
násadec	násadec	k1gInSc1	násadec
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
tykadlo	tykadlo	k1gNnSc1	tykadlo
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
upnuté	upnutý	k2eAgFnSc6d1	upnutá
<g/>
.	.	kIx.	.
</s>
<s>
Násadec	násadec	k1gInSc1	násadec
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
krátkým	krátký	k2eAgNnSc7d1	krátké
válcovitým	válcovitý	k2eAgNnSc7d1	válcovité
kolínkem	kolínko	k1gNnSc7	kolínko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ho	on	k3xPp3gMnSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
články	článek	k1gInPc7	článek
tykadla	tykadlo	k1gNnSc2	tykadlo
tzv.	tzv.	kA	tzv.
bičíky	bičík	k1gInPc1	bičík
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
samiček	samička	k1gFnPc2	samička
se	se	k3xPyFc4	se
bičíky	bičík	k1gInPc1	bičík
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
10	[number]	k4	10
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
u	u	k7c2	u
trubce	trubec	k1gMnSc2	trubec
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
včelí	včelí	k2eAgFnSc2d1	včelí
matky	matka	k1gFnSc2	matka
má	mít	k5eAaImIp3nS	mít
srdcovitý	srdcovitý	k2eAgInSc4d1	srdcovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
dělnice	dělnice	k1gFnPc4	dělnice
trojúhelníkový	trojúhelníkový	k2eAgMnSc1d1	trojúhelníkový
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
trubce	trubec	k1gMnSc2	trubec
je	být	k5eAaImIp3nS	být
kruhovitá	kruhovitý	k2eAgFnSc1d1	kruhovitá
<g/>
.	.	kIx.	.
</s>
<s>
Hruď	hruď	k1gFnSc1	hruď
(	(	kIx(	(
<g/>
thorax	thorax	k1gInSc1	thorax
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xC	jako
nosič	nosič	k1gInSc4	nosič
orgánů	orgán	k1gInPc2	orgán
pohybu	pohyb	k1gInSc2	pohyb
–	–	k?	–
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
její	její	k3xOp3gFnSc1	její
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
a	a	k8xC	a
vnější	vnější	k2eAgFnSc1d1	vnější
stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
mohutné	mohutný	k2eAgNnSc1d1	mohutné
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
larválním	larvální	k2eAgNnSc6d1	larvální
vývojovém	vývojový	k2eAgNnSc6d1	vývojové
stádiu	stádium	k1gNnSc6	stádium
má	mít	k5eAaImIp3nS	mít
včela	včela	k1gFnSc1	včela
3	[number]	k4	3
hrudní	hrudní	k2eAgInPc1d1	hrudní
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
dospělá	dospělý	k2eAgFnSc1d1	dospělá
včela	včela	k1gFnSc1	včela
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
hruď	hruď	k1gFnSc4	hruď
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
4	[number]	k4	4
hrudních	hrudní	k2eAgInPc2d1	hrudní
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
přestavbě	přestavba	k1gFnSc6	přestavba
orgánů	orgán	k1gInPc2	orgán
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
kukly	kukla	k1gFnSc2	kukla
se	se	k3xPyFc4	se
první	první	k4xOgInSc1	první
břišní	břišní	k2eAgInSc1d1	břišní
článek	článek	k1gInSc1	článek
přesunul	přesunout	k5eAaPmAgInS	přesunout
k	k	k7c3	k
hrudi	hruď	k1gFnSc3	hruď
a	a	k8xC	a
využil	využít	k5eAaPmAgMnS	využít
se	se	k3xPyFc4	se
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
její	její	k3xOp3gFnSc2	její
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
hrudní	hrudní	k2eAgInPc1d1	hrudní
články	článek	k1gInPc1	článek
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
<g/>
:	:	kIx,	:
předohruď	předohruď	k1gFnSc1	předohruď
(	(	kIx(	(
<g/>
prothroax	prothroax	k1gInSc1	prothroax
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
středohruď	středohruď	k1gFnSc1	středohruď
(	(	kIx(	(
<g/>
mesothroax	mesothroax	k1gInSc1	mesothroax
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zadohruď	zadohruď	k1gFnSc1	zadohruď
(	(	kIx(	(
<g/>
metathroax	metathroax	k1gInSc1	metathroax
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesunutý	přesunutý	k2eAgInSc1d1	přesunutý
kroužek	kroužek	k1gInSc1	kroužek
(	(	kIx(	(
<g/>
propodeum	propodeum	k1gInSc1	propodeum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Včela	včela	k1gFnSc1	včela
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
páry	pára	k1gFnSc2	pára
nohou	noha	k1gFnPc2	noha
(	(	kIx(	(
<g/>
pedes	pedes	k1gInSc1	pedes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Včele	včela	k1gFnSc3	včela
slouží	sloužit	k5eAaImIp3nP	sloužit
nohy	noha	k1gFnPc1	noha
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
<g/>
,	,	kIx,	,
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
řetízků	řetízek	k1gInPc2	řetízek
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
včelami	včela	k1gFnPc7	včela
<g/>
,	,	kIx,	,
k	k	k7c3	k
předávání	předávání	k1gNnSc3	předávání
voskových	voskový	k2eAgFnPc2d1	vosková
šupinek	šupinka	k1gFnPc2	šupinka
<g/>
,	,	kIx,	,
sběru	sběr	k1gInSc2	sběr
a	a	k8xC	a
ukládání	ukládání	k1gNnSc2	ukládání
pylu	pyl	k1gInSc2	pyl
<g/>
,	,	kIx,	,
čištění	čištění	k1gNnSc3	čištění
tykadel	tykadlo	k1gNnPc2	tykadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nesou	nést	k5eAaImIp3nP	nést
též	též	k9	též
chemické	chemický	k2eAgInPc1d1	chemický
a	a	k8xC	a
mechanické	mechanický	k2eAgInPc1d1	mechanický
receptory	receptor	k1gInPc1	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
ústrojí	ústrojí	k1gNnSc2	ústrojí
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
páru	pár	k1gInSc6	pár
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgInPc4d1	zvaný
košíčky	košíček	k1gInPc4	košíček
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgInPc2	který
sbírá	sbírat	k5eAaImIp3nS	sbírat
pyl	pyl	k1gInSc1	pyl
<g/>
.	.	kIx.	.
</s>
<s>
Články	článek	k1gInPc1	článek
nohou	noha	k1gFnPc2	noha
<g/>
:	:	kIx,	:
kyčel	kyčel	k1gFnSc1	kyčel
(	(	kIx(	(
<g/>
coxa	coxa	k1gFnSc1	coxa
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
příkyčlí	příkyčlí	k1gNnSc1	příkyčlí
(	(	kIx(	(
<g/>
subcoxa	subcoxa	k1gFnSc1	subcoxa
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
stehno	stehno	k1gNnSc1	stehno
(	(	kIx(	(
<g/>
femur	femur	k1gMnSc1	femur
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
holeň	holeň	k1gFnSc1	holeň
a	a	k8xC	a
článkované	článkovaný	k2eAgNnSc1d1	článkované
chodidlo	chodidlo	k1gNnSc1	chodidlo
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc1d1	poslední
článek	článek	k1gInSc1	článek
nese	nést	k5eAaImIp3nS	nést
drápky	drápek	k1gInPc4	drápek
a	a	k8xC	a
polštářky	polštářek	k1gInPc4	polštářek
<g/>
)	)	kIx)	)
Včela	včela	k1gFnSc1	včela
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
boku	bok	k1gInSc6	bok
hrudní	hrudní	k2eAgFnSc2d1	hrudní
části	část	k1gFnSc2	část
jeden	jeden	k4xCgMnSc1	jeden
pár	pár	k4xCyI	pár
blanitých	blanitý	k2eAgNnPc2d1	blanité
křídel	křídlo	k1gNnPc2	křídlo
(	(	kIx(	(
<g/>
alae	alae	k1gInSc1	alae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
množstvím	množství	k1gNnSc7	množství
drobných	drobný	k2eAgInPc2d1	drobný
chloupků	chloupek	k1gInPc2	chloupek
<g/>
,	,	kIx,	,
okem	oke	k1gNnSc7	oke
běžně	běžně	k6eAd1	běžně
neviditelných	viditelný	k2eNgMnPc2d1	Neviditelný
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
nejsou	být	k5eNaImIp3nP	být
končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
vychlípeniny	vychlípenina	k1gFnSc2	vychlípenina
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
kukly	kukla	k1gFnSc2	kukla
pronikly	proniknout	k5eAaPmAgFnP	proniknout
vzdušnice	vzdušnice	k1gFnPc1	vzdušnice
a	a	k8xC	a
nervy	nerv	k1gInPc1	nerv
a	a	k8xC	a
kolem	kolem	k7c2	kolem
nichž	jenž	k3xRgFnPc2	jenž
proudí	proudit	k5eAaPmIp3nS	proudit
do	do	k7c2	do
křídel	křídlo	k1gNnPc2	křídlo
i	i	k9	i
hemolymfa	hemolymf	k1gMnSc2	hemolymf
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgNnPc4d1	přední
křídla	křídlo	k1gNnPc4	křídlo
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
zadní	zadní	k2eAgInPc1d1	zadní
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
dvou	dva	k4xCgFnPc2	dva
určitých	určitý	k2eAgFnPc2d1	určitá
žilek	žilka	k1gFnPc2	žilka
na	na	k7c6	na
křídle	křídlo	k1gNnSc6	křídlo
včely	včela	k1gFnSc2	včela
včelaři	včelař	k1gMnPc1	včelař
vypočítávají	vypočítávat	k5eAaImIp3nP	vypočítávat
tzv.	tzv.	kA	tzv.
loketní	loketní	k2eAgInSc4d1	loketní
index	index	k1gInSc4	index
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
poznají	poznat	k5eAaPmIp3nP	poznat
<g/>
,	,	kIx,	,
o	o	k7c4	o
jaké	jaký	k3yIgNnSc4	jaký
geografické	geografický	k2eAgNnSc4d1	geografické
plemeno	plemeno	k1gNnSc4	plemeno
včely	včela	k1gFnSc2	včela
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
křídel	křídlo	k1gNnPc2	křídlo
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
sledovat	sledovat	k5eAaImF	sledovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
kmity	kmit	k1gInPc4	kmit
<g/>
.	.	kIx.	.
</s>
<s>
Včela	včela	k1gFnSc1	včela
mávne	mávnout	k5eAaPmIp3nS	mávnout
křídly	křídlo	k1gNnPc7	křídlo
přesně	přesně	k6eAd1	přesně
230	[number]	k4	230
krát	krát	k6eAd1	krát
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nese	nést	k5eAaImIp3nS	nést
pyl	pyl	k1gInSc1	pyl
jen	jen	k9	jen
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
úhel	úhel	k1gInSc4	úhel
mávání	mávání	k1gNnSc2	mávání
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
koordinace	koordinace	k1gFnSc1	koordinace
činnosti	činnost	k1gFnSc2	činnost
předního	přední	k2eAgMnSc2d1	přední
a	a	k8xC	a
zadního	zadní	k2eAgNnSc2d1	zadní
křídla	křídlo	k1gNnSc2	křídlo
je	být	k5eAaImIp3nS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
háčky	háček	k1gMnPc7	háček
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gNnPc2	on
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
předním	přední	k2eAgInSc6d1	přední
okraji	okraj	k1gInSc6	okraj
zadního	zadní	k2eAgNnSc2d1	zadní
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
zapadají	zapadat	k5eAaPmIp3nP	zapadat
při	při	k7c6	při
letu	let	k1gInSc6	let
včely	včela	k1gFnSc2	včela
do	do	k7c2	do
žlábku	žlábek	k1gInSc2	žlábek
na	na	k7c6	na
zadním	zadní	k2eAgInSc6d1	zadní
okraji	okraj	k1gInSc6	okraj
předního	přední	k2eAgNnSc2d1	přední
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
tak	tak	k9	tak
souvislou	souvislý	k2eAgFnSc4d1	souvislá
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
letu	let	k1gInSc2	let
a	a	k8xC	a
návratu	návrat	k1gInSc3	návrat
křídel	křídlo	k1gNnPc2	křídlo
do	do	k7c2	do
normální	normální	k2eAgFnSc2d1	normální
polohy	poloha	k1gFnSc2	poloha
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
se	se	k3xPyFc4	se
háčky	háček	k1gInPc7	háček
posunutím	posunutí	k1gNnSc7	posunutí
předního	přední	k2eAgNnSc2d1	přední
křídla	křídlo	k1gNnSc2	křídlo
přes	přes	k7c4	přes
zadní	zadní	k2eAgNnPc4d1	zadní
samy	sám	k3xTgFnPc1	sám
vypnou	vypnout	k5eAaPmIp3nP	vypnout
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
křídel	křídlo	k1gNnPc2	křídlo
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
osmičky	osmička	k1gFnSc2	osmička
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
hrudní	hrudní	k2eAgInPc1d1	hrudní
svaly	sval	k1gInPc1	sval
<g/>
.	.	kIx.	.
</s>
<s>
Včela	včela	k1gFnSc1	včela
dokáže	dokázat	k5eAaPmIp3nS	dokázat
letět	letět	k5eAaImF	letět
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
stát	stát	k5eAaImF	stát
za	za	k7c2	za
letu	let	k1gInSc2	let
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
když	když	k8xS	když
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
pyl	pyl	k1gInSc1	pyl
do	do	k7c2	do
rousků	rousek	k1gInPc2	rousek
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
dokáže	dokázat	k5eAaPmIp3nS	dokázat
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
podložce	podložka	k1gFnSc6	podložka
a	a	k8xC	a
pohybem	pohyb	k1gInSc7	pohyb
křídel	křídlo	k1gNnPc2	křídlo
účinně	účinně	k6eAd1	účinně
větrat	větrat	k5eAaImF	větrat
úl	úl	k1gInSc4	úl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zadečku	zadeček	k1gInSc6	zadeček
(	(	kIx(	(
<g/>
abdomen	abdomen	k1gInSc1	abdomen
<g/>
)	)	kIx)	)
včely	včela	k1gFnPc1	včela
jsou	být	k5eAaImIp3nP	být
uložené	uložený	k2eAgInPc1d1	uložený
zažívací	zažívací	k2eAgInPc1d1	zažívací
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
medový	medový	k2eAgInSc1d1	medový
váček	váček	k1gInSc1	váček
<g/>
,	,	kIx,	,
jedová	jedový	k2eAgFnSc1d1	jedová
žláza	žláza	k1gFnSc1	žláza
<g/>
,	,	kIx,	,
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
vaky	vak	k1gInPc1	vak
a	a	k8xC	a
žihadlo	žihadlo	k1gNnSc1	žihadlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohyblivě	pohyblivě	k6eAd1	pohyblivě
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
hrudní	hrudní	k2eAgFnSc7d1	hrudní
částí	část	k1gFnSc7	část
<g/>
.	.	kIx.	.
</s>
<s>
Žihadlo	žihadlo	k1gNnSc1	žihadlo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zadečku	zadeček	k1gInSc2	zadeček
je	být	k5eAaImIp3nS	být
duté	dutý	k2eAgNnSc1d1	duté
<g/>
,	,	kIx,	,
napojené	napojený	k2eAgNnSc1d1	napojené
na	na	k7c4	na
jedový	jedový	k2eAgInSc4d1	jedový
váček	váček	k1gInSc4	váček
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žihadle	žihadlo	k1gNnSc6	žihadlo
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zpětné	zpětný	k2eAgInPc1d1	zpětný
háčky	háček	k1gInPc1	háček
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
po	po	k7c6	po
bodnutí	bodnutí	k1gNnSc6	bodnutí
jeho	on	k3xPp3gNnSc2	on
vytažení	vytažení	k1gNnSc2	vytažení
z	z	k7c2	z
rány	rána	k1gFnSc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Včela	včela	k1gFnSc1	včela
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
po	po	k7c6	po
bodnutí	bodnutí	k1gNnSc6	bodnutí
vytrhne	vytrhnout	k5eAaPmIp3nS	vytrhnout
žihadlo	žihadlo	k1gNnSc4	žihadlo
i	i	k9	i
s	s	k7c7	s
jedovým	jedový	k2eAgInSc7d1	jedový
váčkem	váček	k1gInSc7	váček
a	a	k8xC	a
následně	následně	k6eAd1	následně
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vytržení	vytržení	k1gNnSc6	vytržení
žihadla	žihadlo	k1gNnSc2	žihadlo
dochází	docházet	k5eAaImIp3nS	docházet
ještě	ještě	k9	ještě
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
k	k	k7c3	k
vypouštění	vypouštění	k1gNnSc3	vypouštění
jedu	jed	k1gInSc2	jed
do	do	k7c2	do
rány	rána	k1gFnSc2	rána
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
účinek	účinek	k1gInSc4	účinek
bodnutí	bodnutí	k1gNnSc2	bodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
žihadlo	žihadlo	k1gNnSc4	žihadlo
v	v	k7c6	v
ráně	rána	k1gFnSc6	rána
<g/>
,	,	kIx,	,
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
ho	on	k3xPp3gNnSc2	on
nesmí	smět	k5eNaImIp3nP	smět
vytahovat	vytahovat	k5eAaImF	vytahovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ho	on	k3xPp3gMnSc4	on
uchopil	uchopit	k5eAaPmAgInS	uchopit
za	za	k7c4	za
jedový	jedový	k2eAgInSc4d1	jedový
váček	váček	k1gInSc4	váček
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
vymáčkl	vymáčknout	k5eAaPmAgInS	vymáčknout
zbytek	zbytek	k1gInSc4	zbytek
jedu	jed	k1gInSc2	jed
ve	v	k7c6	v
váčku	váček	k1gInSc6	váček
do	do	k7c2	do
rány	rána	k1gFnSc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Žihadlo	žihadlo	k1gNnSc1	žihadlo
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
vyškrábnout	vyškrábnout	k5eAaPmF	vyškrábnout
tenkým	tenký	k2eAgInSc7d1	tenký
předmětem	předmět	k1gInSc7	předmět
(	(	kIx(	(
<g/>
nehtem	nehet	k1gInSc7	nehet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčko	vajíčko	k1gNnSc1	vajíčko
Larva	larva	k1gFnSc1	larva
(	(	kIx(	(
<g/>
nymfa	nymfa	k1gFnSc1	nymfa
<g/>
)	)	kIx)	)
Předkukla	Předkukla	k1gMnSc1	Předkukla
Kukla	Kukla	k1gMnSc1	Kukla
Dospělec	dospělec	k1gMnSc1	dospělec
(	(	kIx(	(
<g/>
imago	imago	k1gMnSc1	imago
<g/>
)	)	kIx)	)
Vajíčko	vajíčko	k1gNnSc1	vajíčko
pokládá	pokládat	k5eAaImIp3nS	pokládat
matka	matka	k1gFnSc1	matka
včely	včela	k1gFnSc2	včela
medonosné	medonosný	k2eAgFnSc2d1	medonosná
do	do	k7c2	do
dělničí	dělničit	k5eAaImIp3nP	dělničit
či	či	k8xC	či
trubčí	trubčí	k2eAgFnPc1d1	trubčí
buňky	buňka	k1gFnPc1	buňka
plástu	plást	k1gInSc2	plást
nebo	nebo	k8xC	nebo
do	do	k7c2	do
mateří	mateří	k2eAgFnSc2d1	mateří
misky	miska	k1gFnSc2	miska
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
včelí	včelí	k2eAgFnSc2d1	včelí
buňky	buňka	k1gFnSc2	buňka
a	a	k8xC	a
mateří	mateří	k2eAgFnSc2d1	mateří
misky	miska	k1gFnSc2	miska
klade	klást	k5eAaImIp3nS	klást
vajíčka	vajíčko	k1gNnPc4	vajíčko
oplozená	oplozený	k2eAgNnPc4d1	oplozené
<g/>
,	,	kIx,	,
do	do	k7c2	do
trubčích	trubčí	k2eAgNnPc2d1	trubčí
vajíčka	vajíčko	k1gNnSc2	vajíčko
neoplozená	oplozený	k2eNgFnSc1d1	neoplozená
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčko	vajíčko	k1gNnSc1	vajíčko
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgFnPc4d1	bílá
tyčinkovité	tyčinkovitý	k2eAgFnPc4d1	tyčinkovitá
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
zakřivené	zakřivený	k2eAgFnPc1d1	zakřivená
<g/>
,	,	kIx,	,
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1,3	[number]	k4	1,3
<g/>
–	–	k?	–
<g/>
1,8	[number]	k4	1,8
mm	mm	kA	mm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
kolem	kolem	k7c2	kolem
0,130	[number]	k4	0,130
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Larva	larva	k1gFnSc1	larva
vylíhlá	vylíhlý	k2eAgFnSc1d1	vylíhlá
z	z	k7c2	z
vajíčka	vajíčko	k1gNnSc2	vajíčko
není	být	k5eNaImIp3nS	být
ještě	ještě	k6eAd1	ještě
vůbec	vůbec	k9	vůbec
podobná	podobný	k2eAgFnSc1d1	podobná
včele	včela	k1gFnSc3	včela
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
projít	projít	k5eAaPmF	projít
celou	celý	k2eAgFnSc7d1	celá
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
proměnou	proměna	k1gFnSc7	proměna
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
stane	stanout	k5eAaPmIp3nS	stanout
dospělec	dospělec	k1gMnSc1	dospělec
(	(	kIx(	(
<g/>
imago	imago	k1gMnSc1	imago
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
larvy	larva	k1gFnSc2	larva
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
13	[number]	k4	13
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Pokožka	pokožka	k1gFnSc1	pokožka
larvy	larva	k1gFnSc2	larva
má	mít	k5eAaImIp3nS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
lesklou	lesklý	k2eAgFnSc4d1	lesklá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Larvy	larva	k1gFnPc1	larva
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
hodinách	hodina	k1gFnPc6	hodina
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
přibližně	přibližně	k6eAd1	přibližně
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
2,0	[number]	k4	2,0
mm	mm	kA	mm
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hlavě	hlava	k1gFnSc3	hlava
a	a	k8xC	a
zadečku	zadeček	k1gInSc2	zadeček
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
jsou	být	k5eAaImIp3nP	být
larvy	larva	k1gFnPc1	larva
stočené	stočený	k2eAgFnPc1d1	stočená
bříškem	bříšek	k1gInSc7	bříšek
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
růstu	růst	k1gInSc2	růst
se	se	k3xPyFc4	se
napřimují	napřimovat	k5eAaImIp3nP	napřimovat
a	a	k8xC	a
vyplní	vyplnit	k5eAaPmIp3nP	vyplnit
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
objem	objem	k1gInSc4	objem
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Předkukla	Předkuknout	k5eAaPmAgFnS	Předkuknout
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
mateřské	mateřský	k2eAgFnSc3d1	mateřská
buňce	buňka	k1gFnSc3	buňka
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
matečníku	matečník	k1gInSc2	matečník
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
zámotek	zámotek	k1gInSc4	zámotek
jen	jen	k9	jen
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
a	a	k8xC	a
ve	v	k7c6	v
vrchní	vrchní	k2eAgFnSc6d1	vrchní
části	část	k1gFnSc6	část
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
zámotku	zámotek	k1gInSc2	zámotek
se	se	k3xPyFc4	se
larva	larva	k1gFnSc1	larva
zapřádá	zapřádat	k5eAaImIp3nS	zapřádat
po	po	k7c6	po
zavíčkování	zavíčkování	k1gNnSc6	zavíčkování
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vnější	vnější	k2eAgFnPc1d1	vnější
části	část	k1gFnPc1	část
těla	tělo	k1gNnSc2	tělo
předkukly	předkuknout	k5eAaPmAgFnP	předkuknout
už	už	k6eAd1	už
začínají	začínat	k5eAaImIp3nP	začínat
narůstat	narůstat	k5eAaImF	narůstat
do	do	k7c2	do
tvarů	tvar	k1gInPc2	tvar
podobných	podobný	k2eAgInPc2d1	podobný
dospělé	dospělý	k2eAgInPc4d1	dospělý
včele	včela	k1gFnSc3	včela
<g/>
.	.	kIx.	.
</s>
<s>
Kukla	kukla	k1gFnSc1	kukla
je	být	k5eAaImIp3nS	být
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
stádiem	stádium	k1gNnSc7	stádium
vývoje	vývoj	k1gInSc2	vývoj
včely	včela	k1gFnSc2	včela
<g/>
.	.	kIx.	.
</s>
<s>
Stádium	stádium	k1gNnSc1	stádium
kukly	kukla	k1gFnSc2	kukla
trvá	trvat	k5eAaImIp3nS	trvat
u	u	k7c2	u
matky	matka	k1gFnSc2	matka
5	[number]	k4	5
dní	den	k1gInPc2	den
a	a	k8xC	a
u	u	k7c2	u
dělnic	dělnice	k1gFnPc2	dělnice
a	a	k8xC	a
trubců	trubec	k1gMnPc2	trubec
8	[number]	k4	8
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
kukla	kukla	k1gFnSc1	kukla
nehýbe	hýbat	k5eNaImIp3nS	hýbat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
natočená	natočený	k2eAgFnSc1d1	natočená
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
hlavou	hlavý	k2eAgFnSc4d1	hlavá
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
otvoru	otvor	k1gInSc3	otvor
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
kukly	kukla	k1gFnSc2	kukla
nastává	nastávat	k5eAaImIp3nS	nastávat
přeměna	přeměna	k1gFnSc1	přeměna
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
orgánu	orgán	k1gInSc3	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Živiny	živina	k1gFnSc2	živina
potřebné	potřebný	k2eAgFnSc2d1	potřebná
pro	pro	k7c4	pro
látkovou	látkový	k2eAgFnSc4d1	látková
přeměnu	přeměna	k1gFnSc4	přeměna
a	a	k8xC	a
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
nových	nový	k2eAgInPc2d1	nový
orgánů	orgán	k1gInPc2	orgán
čerpá	čerpat	k5eAaImIp3nS	čerpat
ze	z	k7c2	z
zásob	zásoba	k1gFnPc2	zásoba
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
glykogenu	glykogen	k1gInSc2	glykogen
a	a	k8xC	a
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
si	se	k3xPyFc3	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
v	v	k7c6	v
larválním	larvální	k2eAgNnSc6d1	larvální
stádiu	stádium	k1gNnSc6	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Dospělec	dospělec	k1gMnSc1	dospělec
vylézá	vylézat	k5eAaImIp3nS	vylézat
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
po	po	k7c6	po
vykousání	vykousání	k1gNnSc6	vykousání
jejího	její	k3xOp3gNnSc2	její
víčka	víčko	k1gNnSc2	víčko
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
vývoj	vývoj	k1gInSc1	vývoj
matky	matka	k1gFnSc2	matka
trvá	trvat	k5eAaImIp3nS	trvat
16	[number]	k4	16
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
dělnice	dělnice	k1gFnPc1	dělnice
21	[number]	k4	21
dní	den	k1gInPc2	den
a	a	k8xC	a
trubce	trubka	k1gFnSc6	trubka
24	[number]	k4	24
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
včelstvo	včelstvo	k1gNnSc4	včelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Včelstvo	včelstvo	k1gNnSc1	včelstvo
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
složeno	složen	k2eAgNnSc1d1	složeno
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
mnoha	mnoho	k4c2	mnoho
dělnic	dělnice	k1gFnPc2	dělnice
a	a	k8xC	a
určitého	určitý	k2eAgNnSc2d1	určité
množství	množství	k1gNnSc2	množství
trubců	trubec	k1gMnPc2	trubec
<g/>
,	,	kIx,	,
závisejícího	závisející	k2eAgInSc2d1	závisející
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
včelstva	včelstvo	k1gNnSc2	včelstvo
<g/>
,	,	kIx,	,
dostupnosti	dostupnost	k1gFnSc2	dostupnost
bílkovinné	bílkovinný	k2eAgFnSc2d1	bílkovinná
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
pylu	pyl	k1gInSc2	pyl
<g/>
)	)	kIx)	)
a	a	k8xC	a
roční	roční	k2eAgFnSc6d1	roční
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
je	být	k5eAaImIp3nS	být
odchov	odchov	k1gInSc1	odchov
trubců	trubec	k1gMnPc2	trubec
podmíněn	podmínit	k5eAaPmNgInS	podmínit
i	i	k9	i
genetickými	genetický	k2eAgFnPc7d1	genetická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
a	a	k8xC	a
stářím	stáří	k1gNnSc7	stáří
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
členové	člen	k1gMnPc1	člen
včelstva	včelstvo	k1gNnSc2	včelstvo
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
sobě	se	k3xPyFc3	se
závislí	závislý	k2eAgMnPc1d1	závislý
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
bez	bez	k7c2	bez
ostatních	ostatní	k2eAgMnPc2d1	ostatní
nedovede	dovést	k5eNaPmIp3nS	dovést
plnit	plnit	k5eAaImF	plnit
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
následně	následně	k6eAd1	následně
zahyne	zahynout	k5eAaPmIp3nS	zahynout
(	(	kIx(	(
<g/>
např.	např.	kA	např.
osamělá	osamělý	k2eAgFnSc1d1	osamělá
dělnice	dělnice	k1gFnSc1	dělnice
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
či	či	k8xC	či
trubec	trubec	k1gMnSc1	trubec
nebo	nebo	k8xC	nebo
plod	plod	k1gInSc1	plod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
včelami	včela	k1gFnPc7	včela
funguje	fungovat	k5eAaImIp3nS	fungovat
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
dělba	dělba	k1gFnSc1	dělba
práce	práce	k1gFnSc2	práce
<g/>
:	:	kIx,	:
</s>
<s>
Matka	matka	k1gFnSc1	matka
–	–	k?	–
její	její	k3xOp3gFnSc7	její
úlohou	úloha	k1gFnSc7	úloha
je	být	k5eAaImIp3nS	být
klást	klást	k5eAaImF	klást
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
obnovu	obnova	k1gFnSc4	obnova
včelstva	včelstvo	k1gNnSc2	včelstvo
</s>
<s>
Trubec	trubec	k1gMnSc1	trubec
–	–	k?	–
jeho	jeho	k3xOp3gFnSc7	jeho
úlohou	úloha	k1gFnSc7	úloha
je	být	k5eAaImIp3nS	být
oplodnit	oplodnit	k5eAaPmF	oplodnit
mladé	mladý	k2eAgFnPc4d1	mladá
matky	matka	k1gFnPc4	matka
<g/>
;	;	kIx,	;
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zahřívat	zahřívat	k5eAaImF	zahřívat
včelí	včelí	k2eAgInSc4d1	včelí
plod	plod	k1gInSc4	plod
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
vyšší	vysoký	k2eAgFnSc3d2	vyšší
tělesné	tělesný	k2eAgFnSc3d1	tělesná
teplotě	teplota	k1gFnSc3	teplota
<g/>
,	,	kIx,	,
než	než	k8xS	než
jakou	jaký	k3yRgFnSc4	jaký
mají	mít	k5eAaImIp3nP	mít
<g />
.	.	kIx.	.
</s>
<s>
dělnice	dělnice	k1gFnPc1	dělnice
Dělnice	dělnice	k1gFnPc1	dělnice
–	–	k?	–
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
pro	pro	k7c4	pro
včelstvo	včelstvo	k1gNnSc4	včelstvo
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
potřebné	potřebný	k2eAgFnPc4d1	potřebná
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
a	a	k8xC	a
přinášení	přinášení	k1gNnSc1	přinášení
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
nektar	nektar	k1gInSc1	nektar
<g/>
,	,	kIx,	,
medovice	medovice	k1gFnSc1	medovice
<g/>
,	,	kIx,	,
pyl	pyl	k1gInSc1	pyl
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpracovávání	zpracovávání	k1gNnSc1	zpracovávání
medu	med	k1gInSc2	med
z	z	k7c2	z
nektaru	nektar	k1gInSc2	nektar
a	a	k8xC	a
medovice	medovice	k1gFnSc2	medovice
<g/>
,	,	kIx,	,
konzervování	konzervování	k1gNnSc1	konzervování
pylu	pyl	k1gInSc2	pyl
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
plástů	plást	k1gInPc2	plást
<g/>
,	,	kIx,	,
krmení	krmení	k1gNnSc1	krmení
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
trubců	trubec	k1gMnPc2	trubec
a	a	k8xC	a
plodu	plod	k1gInSc2	plod
<g/>
,	,	kIx,	,
střežení	střežení	k1gNnSc4	střežení
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
úlu	úl	k1gInSc2	úl
<g/>
,	,	kIx,	,
úklid	úklid	k1gInSc1	úklid
a	a	k8xC	a
čištění	čištění	k1gNnSc1	čištění
<g/>
,	,	kIx,	,
větrání	větrání	k1gNnSc1	větrání
a	a	k8xC	a
udržování	udržování	k1gNnSc1	udržování
správné	správný	k2eAgFnSc2d1	správná
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
úlu	úl	k1gInSc6	úl
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
činností	činnost	k1gFnPc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
včelí	včelí	k2eAgFnSc1d1	včelí
samička	samička	k1gFnSc1	samička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
včelstva	včelstvo	k1gNnSc2	včelstvo
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
orgány	orgán	k1gInPc1	orgán
(	(	kIx(	(
<g/>
dělnice	dělnice	k1gFnPc1	dělnice
je	on	k3xPp3gMnPc4	on
mají	mít	k5eAaImIp3nP	mít
zakrnělé	zakrnělý	k2eAgFnPc1d1	zakrnělá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dělnic	dělnice	k1gFnPc2	dělnice
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
svými	svůj	k3xOyFgInPc7	svůj
většími	veliký	k2eAgInPc7d2	veliký
rozměry	rozměr	k1gInPc7	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Matky	matka	k1gFnPc1	matka
měří	měřit	k5eAaImIp3nP	měřit
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
mm	mm	kA	mm
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
175	[number]	k4	175
<g/>
–	–	k?	–
<g/>
240	[number]	k4	240
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Oplozená	oplozený	k2eAgFnSc1d1	oplozená
matka	matka	k1gFnSc1	matka
váží	vážit	k5eAaImIp3nS	vážit
225	[number]	k4	225
<g/>
–	–	k?	–
<g/>
290	[number]	k4	290
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dělnice	dělnice	k1gFnSc2	dělnice
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
orgány	orgán	k1gInPc4	orgán
uzpůsobené	uzpůsobený	k2eAgInPc4d1	uzpůsobený
ke	k	k7c3	k
sběru	sběr	k1gInSc3	sběr
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pylové	pylový	k2eAgInPc1d1	pylový
košíčky	košíček	k1gInPc1	košíček
<g/>
,	,	kIx,	,
voskotvorné	voskotvorný	k2eAgFnPc1d1	voskotvorný
žlázy	žláza	k1gFnPc1	žláza
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
hltanové	hltanový	k2eAgFnPc1d1	hltanová
žlázy	žláza	k1gFnPc1	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dělnic	dělnice	k1gFnPc2	dělnice
nepodílí	podílet	k5eNaImIp3nS	podílet
na	na	k7c6	na
pracích	práce	k1gFnPc6	práce
v	v	k7c6	v
úlu	úl	k1gInSc6	úl
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc1	svůj
žihadlo	žihadlo	k1gNnSc1	žihadlo
má	mít	k5eAaImIp3nS	mít
uzpůsobené	uzpůsobený	k2eAgNnSc1d1	uzpůsobené
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
matkou	matka	k1gFnSc7	matka
<g/>
;	;	kIx,	;
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c6	na
žihadle	žihadlo	k1gNnSc6	žihadlo
protiháčky	protiháček	k1gInPc4	protiháček
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
po	po	k7c6	po
bodnutí	bodnutí	k1gNnSc6	bodnutí
nehyne	hynout	k5eNaImIp3nS	hynout
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
dělnic	dělnice	k1gFnPc2	dělnice
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
její	její	k3xOp3gNnSc1	její
žihadlo	žihadlo	k1gNnSc1	žihadlo
funguje	fungovat	k5eAaImIp3nS	fungovat
zejména	zejména	k9	zejména
jako	jako	k8xC	jako
kladélko	kladélko	k1gNnSc4	kladélko
ke	k	k7c3	k
kladení	kladení	k1gNnSc3	kladení
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
matku	matka	k1gFnSc4	matka
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
dělnice	dělnice	k1gFnPc1	dělnice
soustavně	soustavně	k6eAd1	soustavně
starat	starat	k5eAaImF	starat
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
ji	on	k3xPp3gFnSc4	on
krmí	krmit	k5eAaImIp3nS	krmit
tzv.	tzv.	kA	tzv.
mateří	mateří	k2eAgFnSc7d1	mateří
kašičkou	kašička	k1gFnSc7	kašička
(	(	kIx(	(
<g/>
výměšek	výměšek	k1gInSc1	výměšek
hltanových	hltanový	k2eAgFnPc2d1	hltanová
žláz	žláza	k1gFnPc2	žláza
mladých	mladý	k2eAgFnPc2d1	mladá
včel	včela	k1gFnPc2	včela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bohatou	bohatý	k2eAgFnSc4d1	bohatá
na	na	k7c4	na
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
matky	matka	k1gFnSc2	matka
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k8xS	jako
vývoj	vývoj	k1gInSc1	vývoj
dělnice	dělnice	k1gFnSc2	dělnice
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
larvička	larvička	k1gFnSc1	larvička
matky	matka	k1gFnSc2	matka
dostává	dostávat	k5eAaImIp3nS	dostávat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
larválního	larvální	k2eAgInSc2d1	larvální
vývoje	vývoj	k1gInSc2	vývoj
mateří	mateří	k2eAgFnSc4d1	mateří
kašičku	kašička	k1gFnSc4	kašička
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
než	než	k8xS	než
dělnice	dělnice	k1gFnPc1	dělnice
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
na	na	k7c4	na
rojení	rojení	k1gNnSc4	rojení
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
při	při	k7c6	při
tiché	tichý	k2eAgFnSc6d1	tichá
výměně	výměna	k1gFnSc6	výměna
<g/>
)	)	kIx)	)
položí	položit	k5eAaPmIp3nS	položit
matka	matka	k1gFnSc1	matka
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
mateřské	mateřský	k2eAgFnSc2d1	mateřská
misky	miska	k1gFnSc2	miska
(	(	kIx(	(
<g/>
matečníku	matečník	k1gInSc2	matečník
<g/>
)	)	kIx)	)
oplodněné	oplodněný	k2eAgNnSc1d1	oplodněné
vajíčko	vajíčko	k1gNnSc1	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
miska	miska	k1gFnSc1	miska
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
kulového	kulový	k2eAgInSc2d1	kulový
vrchlíku	vrchlík	k1gInSc2	vrchlík
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
9	[number]	k4	9
mm	mm	kA	mm
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
postavena	postavit	k5eAaPmNgFnS	postavit
dnem	den	k1gInSc7	den
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
plástu	plást	k1gInSc2	plást
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
včelaři	včelař	k1gMnPc1	včelař
odchovávají	odchovávat	k5eAaImIp3nP	odchovávat
"	"	kIx"	"
<g/>
umělé	umělý	k2eAgFnPc1d1	umělá
<g/>
"	"	kIx"	"
matky	matka	k1gFnPc1	matka
<g/>
,	,	kIx,	,
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
si	se	k3xPyFc3	se
tyto	tento	k3xDgFnPc4	tento
misky	miska	k1gFnPc4	miska
sami	sám	k3xTgMnPc1	sám
z	z	k7c2	z
včelího	včelí	k2eAgInSc2d1	včelí
vosku	vosk	k1gInSc2	vosk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
používají	používat	k5eAaImIp3nP	používat
misky	miska	k1gFnPc4	miska
sériově	sériově	k6eAd1	sériově
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
položení	položení	k1gNnSc6	položení
vajíčka	vajíčko	k1gNnSc2	vajíčko
do	do	k7c2	do
mateřské	mateřský	k2eAgFnSc2d1	mateřská
misky	miska	k1gFnSc2	miska
vystaví	vystavit	k5eAaPmIp3nP	vystavit
dělnice	dělnice	k1gFnPc1	dělnice
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
misky	miska	k1gFnSc2	miska
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
buňku	buňka	k1gFnSc4	buňka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
žaludu	žalud	k1gInSc2	žalud
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vajíčka	vajíčko	k1gNnSc2	vajíčko
se	se	k3xPyFc4	se
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
zárodečného	zárodečný	k2eAgInSc2d1	zárodečný
vývoje	vývoj	k1gInSc2	vývoj
vylíhne	vylíhnout	k5eAaPmIp3nS	vylíhnout
larvička	larvička	k1gFnSc1	larvička
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
5	[number]	k4	5
dní	den	k1gInPc2	den
zaplní	zaplnit	k5eAaPmIp3nP	zaplnit
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
buňku	buňka	k1gFnSc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
larválního	larvální	k2eAgInSc2d1	larvální
vývoje	vývoj	k1gInSc2	vývoj
dělnice	dělnice	k1gFnSc2	dělnice
buňku	buňka	k1gFnSc4	buňka
zavíčkují	zavíčkovat	k5eAaPmIp3nP	zavíčkovat
<g/>
,	,	kIx,	,
larva	larva	k1gFnSc1	larva
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc7	on
na	na	k7c4	na
8	[number]	k4	8
dní	den	k1gInPc2	den
zakuklí	zakuklit	k5eAaPmIp3nS	zakuklit
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
z	z	k7c2	z
kukly	kukla	k1gFnSc2	kukla
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
dospělou	dospělý	k2eAgFnSc4d1	dospělá
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
líhne	líhnout	k5eAaImIp3nS	líhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
od	od	k7c2	od
položení	položení	k1gNnSc2	položení
vajíčka	vajíčko	k1gNnSc2	vajíčko
do	do	k7c2	do
mateřské	mateřský	k2eAgFnSc2d1	mateřská
misky	miska	k1gFnSc2	miska
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
pomocí	pomocí	k7c2	pomocí
kusadel	kusadla	k1gNnPc2	kusadla
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vykouše	vykousat	k5eAaPmIp3nS	vykousat
ve	v	k7c6	v
víčku	víčko	k1gNnSc6	víčko
otvor	otvor	k1gInSc4	otvor
<g/>
,	,	kIx,	,
a	a	k8xC	a
víčko	víčko	k1gNnSc1	víčko
odklopí	odklopit	k5eAaPmIp3nS	odklopit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
otvor	otvor	k1gInSc1	otvor
si	se	k3xPyFc3	se
dovede	dovést	k5eAaPmIp3nS	dovést
vykousat	vykousat	k5eAaPmF	vykousat
přesně	přesně	k6eAd1	přesně
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jím	on	k3xPp3gNnSc7	on
akorát	akorát	k6eAd1	akorát
prolezla	prolézt	k5eAaPmAgFnS	prolézt
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
otvoru	otvor	k1gInSc2	otvor
včelař	včelař	k1gMnSc1	včelař
může	moct	k5eAaImIp3nS	moct
poznat	poznat	k5eAaPmF	poznat
velikost	velikost	k1gFnSc4	velikost
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
jí	on	k3xPp3gFnSc2	on
přijdou	přijít	k5eAaPmIp3nP	přijít
dělnice	dělnice	k1gFnPc1	dělnice
nakrmit	nakrmit	k5eAaPmF	nakrmit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nouze	nouze	k1gFnSc2	nouze
se	se	k3xPyFc4	se
jde	jít	k5eAaImIp3nS	jít
matka	matka	k1gFnSc1	matka
sama	sám	k3xTgMnSc4	sám
nakrmit	nakrmit	k5eAaPmF	nakrmit
sladinou	sladina	k1gFnSc7	sladina
(	(	kIx(	(
<g/>
medem	med	k1gInSc7	med
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
v	v	k7c6	v
úlu	úl	k1gInSc6	úl
nalezne	naleznout	k5eAaPmIp3nS	naleznout
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
další	další	k2eAgFnSc4d1	další
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
souboj	souboj	k1gInSc4	souboj
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
ta	ten	k3xDgFnSc1	ten
slabší	slabý	k2eAgFnSc1d2	slabší
zpravidla	zpravidla	k6eAd1	zpravidla
zahyne	zahynout	k5eAaPmIp3nS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
vítězící	vítězící	k2eAgFnSc1d1	vítězící
matka	matka	k1gFnSc1	matka
poraněná	poraněný	k2eAgFnSc1d1	poraněná
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dokonce	dokonce	k9	dokonce
zahynou	zahynout	k5eAaPmIp3nP	zahynout
obě	dva	k4xCgFnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Rojení	rojení	k1gNnSc1	rojení
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc4d1	přirozený
způsob	způsob	k1gInSc4	způsob
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
včelstev	včelstvo	k1gNnPc2	včelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rojení	rojení	k1gNnSc6	rojení
se	se	k3xPyFc4	se
včelstvo	včelstvo	k1gNnSc1	včelstvo
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgFnPc4d1	zastoupena
včely	včela	k1gFnPc4	včela
všech	všecek	k3xTgFnPc2	všecek
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
včetně	včetně	k7c2	včetně
trubců	trubec	k1gMnPc2	trubec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prvním	první	k4xOgInSc7	první
rojem	roj	k1gInSc7	roj
vyletí	vyletět	k5eAaPmIp3nS	vyletět
i	i	k9	i
stará	starý	k2eAgFnSc1d1	stará
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Roj	roj	k1gInSc1	roj
se	se	k3xPyFc4	se
zabydlí	zabydlet	k5eAaPmIp3nS	zabydlet
v	v	k7c6	v
předem	předem	k6eAd1	předem
vyhlédnuté	vyhlédnutý	k2eAgFnSc6d1	vyhlédnutá
dutině	dutina	k1gFnSc6	dutina
a	a	k8xC	a
postaví	postavit	k5eAaPmIp3nP	postavit
si	se	k3xPyFc3	se
tam	tam	k6eAd1	tam
nové	nový	k2eAgInPc1d1	nový
plásty	plást	k1gInPc1	plást
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úle	úl	k1gInSc6	úl
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zavíčkované	zavíčkovaný	k2eAgFnPc1d1	zavíčkovaná
mateřské	mateřský	k2eAgFnPc1d1	mateřská
buňky	buňka	k1gFnPc1	buňka
(	(	kIx(	(
<g/>
matečníky	matečník	k1gInPc1	matečník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
mladé	mladý	k2eAgFnPc4d1	mladá
matky	matka	k1gFnPc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgNnSc1d1	silné
včelstvo	včelstvo	k1gNnSc1	včelstvo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyrojit	vyrojit	k5eAaPmF	vyrojit
i	i	k9	i
vícekrát	vícekrát	k6eAd1	vícekrát
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
produkuje	produkovat	k5eAaImIp3nS	produkovat
feromon	feromon	k1gInSc1	feromon
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
včelařství	včelařství	k1gNnSc6	včelařství
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
mateří	mateří	k2eAgFnSc1d1	mateří
látka	látka	k1gFnSc1	látka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
dělnice	dělnice	k1gFnPc1	dělnice
tento	tento	k3xDgInSc4	tento
feromon	feromon	k1gInSc4	feromon
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
olizují	olizovat	k5eAaImIp3nP	olizovat
a	a	k8xC	a
předávají	předávat	k5eAaImIp3nP	předávat
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
dál	daleko	k6eAd2	daleko
jedna	jeden	k4xCgFnSc1	jeden
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
chemická	chemický	k2eAgFnSc1d1	chemická
informace	informace	k1gFnSc1	informace
tak	tak	k6eAd1	tak
ve	v	k7c6	v
včelstvu	včelstvo	k1gNnSc6	včelstvo
neustále	neustále	k6eAd1	neustále
koluje	kolovat	k5eAaImIp3nS	kolovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
matka	matka	k1gFnSc1	matka
ve	v	k7c6	v
včelstvu	včelstvo	k1gNnSc6	včelstvo
chybět	chybět	k5eAaImF	chybět
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
feromon	feromon	k1gInSc1	feromon
přestane	přestat	k5eAaPmIp3nS	přestat
kolovat	kolovat	k5eAaImF	kolovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
včelstvo	včelstvo	k1gNnSc1	včelstvo
tak	tak	k6eAd1	tak
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
osiřelé	osiřelý	k2eAgNnSc1d1	osiřelé
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
začne	začít	k5eAaPmIp3nS	začít
zakládat	zakládat	k5eAaImF	zakládat
nouzové	nouzový	k2eAgInPc4d1	nouzový
matečníky	matečník	k1gInPc4	matečník
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
je	on	k3xPp3gNnSc4	on
mohlo	moct	k5eAaImAgNnS	moct
založit	založit	k5eAaPmF	založit
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
ve	v	k7c6	v
včelstvu	včelstvo	k1gNnSc6	včelstvo
existovat	existovat	k5eAaImF	existovat
otevřený	otevřený	k2eAgInSc4d1	otevřený
plod	plod	k1gInSc4	plod
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
larvičky	larvička	k1gFnPc1	larvička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ve	v	k7c6	v
včelstvu	včelstvo	k1gNnSc6	včelstvo
otevřený	otevřený	k2eAgInSc4d1	otevřený
plod	plod	k1gInSc4	plod
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
včelstvo	včelstvo	k1gNnSc1	včelstvo
odsouzené	odsouzený	k2eAgNnSc1d1	odsouzené
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Včelař	včelař	k1gMnSc1	včelař
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
včelstva	včelstvo	k1gNnSc2	včelstvo
přidá	přidat	k5eAaPmIp3nS	přidat
plást	plást	k1gInSc1	plást
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
plodem	plod	k1gInSc7	plod
z	z	k7c2	z
jiného	jiný	k2eAgNnSc2d1	jiné
včelstva	včelstvo	k1gNnSc2	včelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Lepším	dobrý	k2eAgNnSc7d2	lepší
řešením	řešení	k1gNnSc7	řešení
je	být	k5eAaImIp3nS	být
přidání	přidání	k1gNnSc4	přidání
cizího	cizí	k2eAgInSc2d1	cizí
zavíčkovaného	zavíčkovaný	k2eAgInSc2d1	zavíčkovaný
matečníku	matečník	k1gInSc2	matečník
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
vylíhnutím	vylíhnutí	k1gNnSc7	vylíhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yIgFnSc4	který
ve	v	k7c6	v
včelstvu	včelstvo	k1gNnSc6	včelstvo
matka	matka	k1gFnSc1	matka
neklade	klást	k5eNaImIp3nS	klást
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nepříznivě	příznivě	k6eNd1	příznivě
projeví	projevit	k5eAaPmIp3nS	projevit
zeslábnutím	zeslábnutí	k1gNnSc7	zeslábnutí
včelstva	včelstvo	k1gNnSc2	včelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zkušení	zkušený	k2eAgMnPc1d1	zkušený
včelaři	včelař	k1gMnPc1	včelař
takovému	takový	k3xDgNnSc3	takový
včelstvu	včelstvo	k1gNnSc3	včelstvo
přidávají	přidávat	k5eAaImIp3nP	přidávat
oplozenou	oplozený	k2eAgFnSc4d1	oplozená
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
procedura	procedura	k1gFnSc1	procedura
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
s	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
<g/>
,	,	kIx,	,
že	že	k8xS	že
včelstvo	včelstvo	k1gNnSc1	včelstvo
matku	matka	k1gFnSc4	matka
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
a	a	k8xC	a
úspěch	úspěch	k1gInSc1	úspěch
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
zkušenostech	zkušenost	k1gFnPc6	zkušenost
včelaře	včelař	k1gMnSc2	včelař
<g/>
.	.	kIx.	.
</s>
<s>
Dělnice	dělnice	k1gFnSc1	dělnice
tvoří	tvořit	k5eAaImIp3nS	tvořit
ve	v	k7c6	v
včelstvu	včelstvo	k1gNnSc6	včelstvo
nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
složku	složka	k1gFnSc4	složka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
samičky	samička	k1gFnPc1	samička
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
nevyvinuté	vyvinutý	k2eNgInPc4d1	nevyvinutý
pohlavní	pohlavní	k2eAgInPc4d1	pohlavní
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
spářit	spářit	k5eAaPmF	spářit
s	s	k7c7	s
trubci	trubec	k1gMnPc7	trubec
<g/>
.	.	kIx.	.
</s>
<s>
Dělnice	dělnice	k1gFnPc1	dělnice
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
mm	mm	kA	mm
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
100	[number]	k4	100
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Líhnou	líhnout	k5eAaImIp3nP	líhnout
se	se	k3xPyFc4	se
z	z	k7c2	z
oplodněných	oplodněný	k2eAgNnPc2d1	oplodněné
vajíček	vajíčko	k1gNnPc2	vajíčko
v	v	k7c6	v
dělničích	dělničí	k2eAgFnPc6d1	dělničí
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c4	na
1	[number]	k4	1
dm	dm	kA	dm
<g/>
2	[number]	k4	2
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
hlavní	hlavní	k2eAgFnSc2d1	hlavní
snůšky	snůška	k1gFnSc2	snůška
se	se	k3xPyFc4	se
v	v	k7c6	v
úlu	úl	k1gInSc6	úl
nachází	nacházet	k5eAaImIp3nS	nacházet
dělnic	dělnice	k1gFnPc2	dělnice
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zimního	zimní	k2eAgInSc2d1	zimní
klidu	klid	k1gInSc2	klid
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Dělnice	dělnice	k1gFnPc1	dělnice
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
z	z	k7c2	z
oplozeného	oplozený	k2eAgNnSc2d1	oplozené
vajíčka	vajíčko	k1gNnSc2	vajíčko
21	[number]	k4	21
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
položení	položení	k1gNnSc2	položení
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
začnou	začít	k5eAaPmIp3nP	začít
sílit	sílit	k5eAaImF	sílit
a	a	k8xC	a
po	po	k7c6	po
2-3	[number]	k4	2-3
hodinách	hodina	k1gFnPc6	hodina
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
stávají	stávat	k5eAaImIp3nP	stávat
tzv.	tzv.	kA	tzv.
včely	včela	k1gFnSc2	včela
čističky	čistička	k1gFnSc2	čistička
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
vyčistit	vyčistit	k5eAaPmF	vyčistit
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
připravit	připravit	k5eAaPmF	připravit
je	on	k3xPp3gInPc4	on
matce	matka	k1gFnSc3	matka
k	k	k7c3	k
zakladení	zakladení	k1gNnSc3	zakladení
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
včely	včela	k1gFnSc2	včela
v	v	k7c6	v
úlu	úl	k1gInSc6	úl
probíhá	probíhat	k5eAaImIp3nS	probíhat
různě	různě	k6eAd1	různě
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
okolních	okolní	k2eAgFnPc6d1	okolní
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
na	na	k7c6	na
potřebách	potřeba	k1gFnPc6	potřeba
včelstva	včelstvo	k1gNnSc2	včelstvo
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
mladé	mladý	k2eAgFnPc1d1	mladá
včely	včela	k1gFnPc1	včela
(	(	kIx(	(
<g/>
mladušky	mladuška	k1gFnPc1	mladuška
<g/>
)	)	kIx)	)
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
začínají	začínat	k5eAaImIp3nP	začínat
krmit	krmit	k5eAaImF	krmit
včelí	včelí	k2eAgInSc4d1	včelí
plod	plod	k1gInSc4	plod
mateří	mateří	k2eAgFnSc7d1	mateří
kašičkou	kašička	k1gFnSc7	kašička
smísenou	smísený	k2eAgFnSc4d1	smísená
s	s	k7c7	s
medem	med	k1gInSc7	med
a	a	k8xC	a
pylem	pyl	k1gInSc7	pyl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
stádiu	stádium	k1gNnSc6	stádium
se	se	k3xPyFc4	se
dělnicím	dělnice	k1gFnPc3	dělnice
aktivují	aktivovat	k5eAaBmIp3nP	aktivovat
voskotvorné	voskotvorný	k2eAgFnSc2d1	voskotvorný
žlázy	žláza	k1gFnSc2	žláza
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
stávají	stávat	k5eAaImIp3nP	stávat
stavitelky	stavitelka	k1gFnPc1	stavitelka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
staví	stavit	k5eAaImIp3nP	stavit
včelí	včelí	k2eAgNnSc4d1	včelí
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
stádiu	stádium	k1gNnSc6	stádium
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
ze	z	k7c2	z
stavitelek	stavitelka	k1gFnPc2	stavitelka
stávají	stávat	k5eAaImIp3nP	stávat
strážkyně	strážkyně	k1gFnPc1	strážkyně
česna	česno	k1gNnSc2	česno
(	(	kIx(	(
<g/>
úlová	úlový	k2eAgNnPc1d1	úlové
dvířka	dvířka	k1gNnPc1	dvířka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
bránit	bránit	k5eAaImF	bránit
před	před	k7c7	před
vetřelci	vetřelec	k1gMnPc7	vetřelec
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
snůška	snůška	k1gFnSc1	snůška
<g/>
,	,	kIx,	,
i	i	k9	i
před	před	k7c7	před
jinými	jiný	k2eAgFnPc7d1	jiná
včelami	včela	k1gFnPc7	včela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
stádiu	stádium	k1gNnSc6	stádium
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
mladušek	mladuška	k1gFnPc2	mladuška
stávají	stávat	k5eAaImIp3nP	stávat
létavky	létavka	k1gFnPc1	létavka
<g/>
,	,	kIx,	,
vylétají	vylétat	k5eAaImIp3nP	vylétat
z	z	k7c2	z
úlu	úl	k1gInSc2	úl
a	a	k8xC	a
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
se	se	k3xPyFc4	se
do	do	k7c2	do
shánění	shánění	k1gNnSc2	shánění
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
činnost	činnost	k1gFnSc1	činnost
včelí	včelí	k2eAgNnSc4d1	včelí
tělíčko	tělíčko	k1gNnSc4	tělíčko
vysiluje	vysilovat	k5eAaImIp3nS	vysilovat
a	a	k8xC	a
po	po	k7c6	po
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
týdnech	týden	k1gInPc6	týden
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
znát	znát	k5eAaImF	znát
velké	velký	k2eAgNnSc4d1	velké
opotřebení	opotřebení	k1gNnSc4	opotřebení
(	(	kIx(	(
<g/>
vybledlé	vybledlý	k2eAgFnSc2d1	vybledlá
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
a	a	k8xC	a
včela	včela	k1gFnSc1	včela
uhyne	uhynout	k5eAaPmIp3nS	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
uhynou	uhynout	k5eAaPmIp3nP	uhynout
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
použijí	použít	k5eAaPmIp3nP	použít
své	svůj	k3xOyFgNnSc4	svůj
žihadlo	žihadlo	k1gNnSc4	žihadlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
po	po	k7c4	po
bodnutí	bodnutí	k1gNnSc4	bodnutí
vytrhnou	vytrhnout	k5eAaPmIp3nP	vytrhnout
i	i	k9	i
s	s	k7c7	s
jedovou	jedový	k2eAgFnSc7d1	jedová
žlázou	žláza	k1gFnSc7	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
tzv.	tzv.	kA	tzv.
dlouhověké	dlouhověký	k2eAgFnPc4d1	dlouhověká
včely	včela	k1gFnPc4	včela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
přečkat	přečkat	k5eAaPmF	přečkat
zimu	zima	k1gFnSc4	zima
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
i	i	k9	i
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
dělnic	dělnice	k1gFnPc2	dělnice
se	se	k3xPyFc4	se
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
hlavně	hlavně	k9	hlavně
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
včelstvu	včelstvo	k1gNnSc6	včelstvo
potřeba	potřeba	k6eAd1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
uvedené	uvedený	k2eAgInPc1d1	uvedený
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
příkladem	příklad	k1gInSc7	příklad
získaným	získaný	k2eAgInSc7d1	získaný
z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
označených	označený	k2eAgFnPc2d1	označená
dělnic	dělnice	k1gFnPc2	dělnice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
včelstvo	včelstvo	k1gNnSc1	včelstvo
osiří	osiřet	k5eAaPmIp3nS	osiřet
<g/>
,	,	kIx,	,
a	a	k8xC	a
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
si	se	k3xPyFc3	se
odchovat	odchovat	k5eAaPmF	odchovat
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
krmení	krmení	k1gNnSc3	krmení
dělnic	dělnice	k1gFnPc2	dělnice
mateří	mateří	k2eAgFnSc7d1	mateří
kašičkou	kašička	k1gFnSc7	kašička
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
aktivaci	aktivace	k1gFnSc3	aktivace
jejich	jejich	k3xOp3gInPc2	jejich
vaječníků	vaječník	k1gInPc2	vaječník
a	a	k8xC	a
takovým	takový	k3xDgFnPc3	takový
včelám	včela	k1gFnPc3	včela
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
trubčice	trubčice	k1gFnPc1	trubčice
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
se	se	k3xPyFc4	se
spářit	spářit	k5eAaPmF	spářit
s	s	k7c7	s
trubcem	trubec	k1gMnSc7	trubec
<g/>
,	,	kIx,	,
kladou	klást	k5eAaImIp3nP	klást
vajíčka	vajíčko	k1gNnPc1	vajíčko
neoplodněná	oplodněný	k2eNgNnPc1d1	neoplodněné
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
pouze	pouze	k6eAd1	pouze
trubci	trubec	k1gMnPc1	trubec
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
včelstvo	včelstvo	k1gNnSc1	včelstvo
je	být	k5eAaImIp3nS	být
odsouzené	odsouzená	k1gFnPc4	odsouzená
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tyto	tento	k3xDgFnPc1	tento
trubčice	trubčice	k1gFnPc1	trubčice
již	již	k6eAd1	již
nepřijmou	přijmout	k5eNaPmIp3nP	přijmout
žádnou	žádný	k3yNgFnSc4	žádný
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
sice	sice	k8xC	sice
postupy	postup	k1gInPc1	postup
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
lze	lze	k6eAd1	lze
někdy	někdy	k6eAd1	někdy
takové	takový	k3xDgNnSc4	takový
včelstvo	včelstvo	k1gNnSc4	včelstvo
zachránit	zachránit	k5eAaPmF	zachránit
přidáním	přidání	k1gNnSc7	přidání
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zkušení	zkušený	k2eAgMnPc1d1	zkušený
včelaři	včelař	k1gMnPc1	včelař
to	ten	k3xDgNnSc4	ten
nedělají	dělat	k5eNaImIp3nP	dělat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
věková	věkový	k2eAgFnSc1d1	věková
struktura	struktura	k1gFnSc1	struktura
těchto	tento	k3xDgNnPc2	tento
včelstev	včelstvo	k1gNnPc2	včelstvo
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
takto	takto	k6eAd1	takto
zachráněné	zachráněný	k2eAgNnSc1d1	zachráněné
včelstvo	včelstvo	k1gNnSc1	včelstvo
stejně	stejně	k6eAd1	stejně
zaostává	zaostávat	k5eAaImIp3nS	zaostávat
<g/>
.	.	kIx.	.
</s>
<s>
Včelaři	včelař	k1gMnPc1	včelař
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
stavu	stav	k1gInSc3	stav
nenechat	nechat	k5eNaPmF	nechat
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přece	přece	k9	přece
jen	jen	k9	jen
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
taková	takový	k3xDgNnPc1	takový
včelstva	včelstvo	k1gNnPc1	včelstvo
raději	rád	k6eAd2	rád
likvidují	likvidovat	k5eAaBmIp3nP	likvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Trubčí	trubčí	k2eAgFnPc1d1	trubčí
larvy	larva	k1gFnPc1	larva
v	v	k7c6	v
dělničích	dělničí	k2eAgFnPc6d1	dělničí
buňkách	buňka	k1gFnPc6	buňka
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
se	se	k3xPyFc4	se
nevejdou	vejít	k5eNaPmIp3nP	vejít
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
včely	včela	k1gFnPc4	včela
tyto	tento	k3xDgFnPc4	tento
buňky	buňka	k1gFnPc1	buňka
protahují	protahovat	k5eAaImIp3nP	protahovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavíčkování	zavíčkování	k1gNnSc6	zavíčkování
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
buňky	buňka	k1gFnPc4	buňka
nevzhledné	vzhledný	k2eNgFnPc4d1	nevzhledná
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
hrboplod	hrboplod	k1gInSc1	hrboplod
<g/>
.	.	kIx.	.
</s>
<s>
Trubec	trubec	k1gMnSc1	trubec
je	být	k5eAaImIp3nS	být
včelí	včelí	k2eAgMnSc1d1	včelí
sameček	sameček	k1gMnSc1	sameček
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
mm	mm	kA	mm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
okolo	okolo	k7c2	okolo
220	[number]	k4	220
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dělnic	dělnice	k1gFnPc2	dělnice
a	a	k8xC	a
matky	matka	k1gFnSc2	matka
nemá	mít	k5eNaImIp3nS	mít
žihadlo	žihadlo	k1gNnSc1	žihadlo
<g/>
.	.	kIx.	.
</s>
<s>
Trubci	trubec	k1gMnPc1	trubec
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
z	z	k7c2	z
neoplozených	oplozený	k2eNgFnPc2d1	neoplozená
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
matka	matka	k1gFnSc1	matka
naklade	naklást	k5eAaPmIp3nS	naklást
do	do	k7c2	do
trubčích	trubčí	k2eAgFnPc2d1	trubčí
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
odlišujících	odlišující	k2eAgMnPc2d1	odlišující
se	se	k3xPyFc4	se
od	od	k7c2	od
dělničích	dělničí	k2eAgFnPc2d1	dělničí
svou	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Trubčí	trubčí	k2eAgFnPc1d1	trubčí
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
dělničí	dělničit	k5eAaImIp3nS	dělničit
a	a	k8xC	a
na	na	k7c4	na
1	[number]	k4	1
dm	dm	kA	dm
<g/>
2	[number]	k4	2
trubčích	trubčí	k2eAgFnPc2d1	trubčí
buněk	buňka	k1gFnPc2	buňka
jich	on	k3xPp3gFnPc2	on
připadá	připadat	k5eAaPmIp3nS	připadat
270	[number]	k4	270
<g/>
.	.	kIx.	.
</s>
<s>
Trubci	trubec	k1gMnPc1	trubec
žijí	žít	k5eAaImIp3nP	žít
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
včelstvu	včelstvo	k1gNnSc6	včelstvo
jich	on	k3xPp3gInPc2	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
až	až	k8xS	až
pár	pár	k4xCyI	pár
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejčastější	častý	k2eAgNnSc1d3	nejčastější
množství	množství	k1gNnSc1	množství
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
500	[number]	k4	500
trubců	trubec	k1gMnPc2	trubec
<g/>
.	.	kIx.	.
</s>
<s>
Včelaři	včelař	k1gMnPc1	včelař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chovají	chovat	k5eAaImIp3nP	chovat
matky	matka	k1gFnPc4	matka
<g/>
,	,	kIx,	,
záměrně	záměrně	k6eAd1	záměrně
ve	v	k7c6	v
včelstvech	včelstvo	k1gNnPc6	včelstvo
odchovávají	odchovávat	k5eAaImIp3nP	odchovávat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
trubců	trubec	k1gMnPc2	trubec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zvýšili	zvýšit	k5eAaPmAgMnP	zvýšit
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
oplodnění	oplodnění	k1gNnSc4	oplodnění
matky	matka	k1gFnSc2	matka
trubci	trubec	k1gMnPc7	trubec
s	s	k7c7	s
kvalitnějšími	kvalitní	k2eAgFnPc7d2	kvalitnější
genetickými	genetický	k2eAgFnPc7d1	genetická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
dělnice	dělnice	k1gFnSc2	dělnice
z	z	k7c2	z
úlu	úl	k1gInSc2	úl
trubce	trubec	k1gMnSc2	trubec
vyženou	vyhnat	k5eAaPmIp3nP	vyhnat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
přerušení	přerušení	k1gNnSc2	přerušení
snůšky	snůška	k1gFnSc2	snůška
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
déle	dlouho	k6eAd2	dlouho
trvajícího	trvající	k2eAgNnSc2d1	trvající
nepříznivého	příznivý	k2eNgNnSc2d1	nepříznivé
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
trubci	trubec	k1gMnPc1	trubec
v	v	k7c6	v
úlech	úl	k1gInPc6	úl
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jen	jen	k9	jen
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
včel	včela	k1gFnPc2	včela
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
<g/>
:	:	kIx,	:
bílkoviné	bílkoviný	k2eAgFnSc2d1	bílkoviný
složky	složka	k1gFnSc2	složka
–	–	k?	–
pyl	pyl	k1gInSc1	pyl
květů	květ	k1gInPc2	květ
glycidové	glycidový	k2eAgFnSc2d1	glycidová
složky	složka	k1gFnSc2	složka
–	–	k?	–
nektar	nektar	k1gInSc1	nektar
hmyzosnubných	hmyzosnubný	k2eAgInPc2d1	hmyzosnubný
květů	květ	k1gInPc2	květ
a	a	k8xC	a
medovice	medovice	k1gFnSc2	medovice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
odpadní	odpadní	k2eAgInSc4d1	odpadní
produkt	produkt	k1gInSc4	produkt
metabolismu	metabolismus	k1gInSc2	metabolismus
mer	mer	k?	mer
a	a	k8xC	a
mšic	mšice	k1gFnPc2	mšice
<g/>
.	.	kIx.	.
</s>
<s>
Sběrem	sběr	k1gInSc7	sběr
a	a	k8xC	a
shromažďováním	shromažďování	k1gNnSc7	shromažďování
zásob	zásoba	k1gFnPc2	zásoba
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
výhradně	výhradně	k6eAd1	výhradně
dělnice-létavky	dělniceétavka	k1gFnPc1	dělnice-létavka
<g/>
.	.	kIx.	.
</s>
<s>
Nektar	nektar	k1gInSc4	nektar
a	a	k8xC	a
medovici	medovice	k1gFnSc4	medovice
sbírají	sbírat	k5eAaImIp3nP	sbírat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
volátka	volátko	k1gNnSc2	volátko
–	–	k?	–
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
části	část	k1gFnSc2	část
trávící	trávící	k2eAgFnSc2d1	trávící
trubice	trubice	k1gFnSc2	trubice
umístěné	umístěný	k2eAgFnSc2d1	umístěná
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
zadečku	zadeček	k1gInSc2	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Pyl	pyl	k1gInSc4	pyl
městnají	městnat	k5eAaImIp3nP	městnat
do	do	k7c2	do
košíčků	košíček	k1gInPc2	košíček
na	na	k7c6	na
holenním	holenní	k2eAgInSc6d1	holenní
článku	článek	k1gInSc6	článek
třetího	třetí	k4xOgInSc2	třetí
páru	pár	k1gInSc2	pár
nohou	noha	k1gFnSc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Přinesený	přinesený	k2eAgInSc1d1	přinesený
pyl	pyl	k1gInSc1	pyl
je	být	k5eAaImIp3nS	být
létavkami	létavka	k1gFnPc7	létavka
městnán	městnán	k2eAgInSc1d1	městnán
do	do	k7c2	do
plástových	plástový	k2eAgFnPc2d1	plástová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Úlové	úlový	k2eAgFnPc4d1	úlová
včely	včela	k1gFnPc4	včela
(	(	kIx(	(
<g/>
krmičky	krmička	k1gFnPc4	krmička
a	a	k8xC	a
kojičky	kojička	k1gFnPc4	kojička
<g/>
)	)	kIx)	)
uskladněný	uskladněný	k2eAgInSc1d1	uskladněný
pyl	pyl	k1gInSc1	pyl
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
odebírají	odebírat	k5eAaImIp3nP	odebírat
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
enzymu	enzym	k1gInSc2	enzym
hltanových	hltanový	k2eAgFnPc2d1	hltanová
žláz	žláza	k1gFnPc2	žláza
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
těle	tělo	k1gNnSc6	tělo
na	na	k7c4	na
bílkovinnou	bílkovinný	k2eAgFnSc4d1	bílkovinná
složku	složka	k1gFnSc4	složka
potravy	potrava	k1gFnSc2	potrava
důležitou	důležitý	k2eAgFnSc4d1	důležitá
pro	pro	k7c4	pro
krmení	krmení	k1gNnSc4	krmení
plodu	plod	k1gInSc2	plod
–	–	k?	–
larev	larva	k1gFnPc2	larva
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
natrávenou	natrávený	k2eAgFnSc7d1	natrávená
a	a	k8xC	a
upravenou	upravený	k2eAgFnSc7d1	upravená
potravou	potrava	k1gFnSc7	potrava
je	být	k5eAaImIp3nS	být
plod	plod	k1gInSc1	plod
krmen	krmen	k2eAgInSc1d1	krmen
včelami	včela	k1gFnPc7	včela
krmičkami	krmička	k1gFnPc7	krmička
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
krmného	krmný	k2eAgInSc2d1	krmný
produktu	produkt	k1gInSc2	produkt
je	být	k5eAaImIp3nS	být
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
a	a	k8xC	a
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
činiteli	činitel	k1gMnPc7	činitel
vývoj	vývoj	k1gInSc4	vývoj
larev	larva	k1gFnPc2	larva
v	v	k7c4	v
dělnice	dělnice	k1gFnPc4	dělnice
<g/>
,	,	kIx,	,
trubce	trubec	k1gMnPc4	trubec
či	či	k8xC	či
novou	nový	k2eAgFnSc4d1	nová
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Larva	larva	k1gFnSc1	larva
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
matečníku	matečník	k1gInSc6	matečník
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
živena	živit	k5eAaImNgFnS	živit
produktem	produkt	k1gInSc7	produkt
o	o	k7c6	o
vysoké	vysoký	k2eAgFnSc6d1	vysoká
biologické	biologický	k2eAgFnSc6d1	biologická
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Krmivo	krmivo	k1gNnSc1	krmivo
za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
produkované	produkovaný	k2eAgInPc4d1	produkovaný
včelami	včela	k1gFnPc7	včela
kojičkami	kojička	k1gFnPc7	kojička
nese	nést	k5eAaImIp3nS	nést
případné	případný	k2eAgNnSc1d1	případné
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
mateří	mateří	k2eAgFnSc1d1	mateří
kašička	kašička	k1gFnSc1	kašička
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příletu	přílet	k1gInSc6	přílet
do	do	k7c2	do
úlu	úl	k1gInSc2	úl
předávají	předávat	k5eAaImIp3nP	předávat
létavky	létavka	k1gFnPc4	létavka
nasbíraný	nasbíraný	k2eAgInSc4d1	nasbíraný
nektar	nektar	k1gInSc4	nektar
resp.	resp.	kA	resp.
medovici	medovice	k1gFnSc4	medovice
z	z	k7c2	z
volátka	volátko	k1gNnSc2	volátko
do	do	k7c2	do
volátka	volátko	k1gNnSc2	volátko
úlovým	úlový	k2eAgFnPc3d1	úlová
včelám	včela	k1gFnPc3	včela
<g/>
.	.	kIx.	.
</s>
<s>
Úlové	úlový	k2eAgFnPc4d1	úlová
včely	včela	k1gFnPc4	včela
obdržený	obdržený	k2eAgInSc4d1	obdržený
nektar	nektar	k1gInSc4	nektar
zahušťují	zahušťovat	k5eAaImIp3nP	zahušťovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomocí	pomocí	k7c2	pomocí
ústních	ústní	k2eAgInPc2d1	ústní
orgánů	orgán	k1gInPc2	orgán
vystavují	vystavovat	k5eAaImIp3nP	vystavovat
kapku	kapka	k1gFnSc4	kapka
nektaru	nektar	k1gInSc2	nektar
teplému	teplý	k2eAgNnSc3d1	teplé
a	a	k8xC	a
větranému	větraný	k2eAgNnSc3d1	větrané
prostředí	prostředí	k1gNnSc3	prostředí
úlu	úl	k1gInSc2	úl
<g/>
.	.	kIx.	.
</s>
<s>
Zahuštěnou	zahuštěný	k2eAgFnSc4d1	zahuštěná
sladinu	sladina	k1gFnSc4	sladina
ukládají	ukládat	k5eAaImIp3nP	ukládat
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
plástů	plást	k1gInPc2	plást
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
odpařováním	odpařování	k1gNnSc7	odpařování
vody	voda	k1gFnSc2	voda
uložená	uložený	k2eAgFnSc1d1	uložená
sladina	sladina	k1gFnSc1	sladina
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
postupně	postupně	k6eAd1	postupně
v	v	k7c4	v
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
<s>
Glycidová	glycidový	k2eAgFnSc1d1	glycidová
složka	složka	k1gFnSc1	složka
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
čerpána	čerpat	k5eAaImNgFnS	čerpat
z	z	k7c2	z
takto	takto	k6eAd1	takto
nashromážděných	nashromážděný	k2eAgFnPc2d1	nashromážděná
zásob	zásoba	k1gFnPc2	zásoba
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
a	a	k8xC	a
trubci	trubec	k1gMnPc1	trubec
se	se	k3xPyFc4	se
nechají	nechat	k5eAaPmIp3nP	nechat
krmit	krmit	k5eAaImF	krmit
kojičkami	kojička	k1gFnPc7	kojička
<g/>
.	.	kIx.	.
</s>
<s>
Propolis	propolis	k1gInSc1	propolis
je	být	k5eAaImIp3nS	být
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
pryskyřičná	pryskyřičný	k2eAgFnSc1d1	pryskyřičná
substance	substance	k1gFnSc1	substance
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
včely	včela	k1gFnPc1	včela
sbírají	sbírat	k5eAaImIp3nP	sbírat
na	na	k7c6	na
pupenech	pupen	k1gInPc6	pupen
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vysoký	vysoký	k2eAgInSc4d1	vysoký
podíl	podíl	k1gInSc4	podíl
látek	látka	k1gFnPc2	látka
bránících	bránící	k2eAgFnPc2d1	bránící
rozvoji	rozvoj	k1gInSc3	rozvoj
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
ji	on	k3xPp3gFnSc4	on
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
tmelení	tmelení	k1gNnSc3	tmelení
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
stěn	stěna	k1gFnPc2	stěna
úlu	úl	k1gInSc2	úl
<g/>
,	,	kIx,	,
ucpávání	ucpávání	k1gNnSc1	ucpávání
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
štěrbin	štěrbina	k1gFnPc2	štěrbina
a	a	k8xC	a
mumifikaci	mumifikace	k1gFnSc4	mumifikace
mrtvých	mrtvý	k2eAgNnPc2d1	mrtvé
těl	tělo	k1gNnPc2	tělo
vetřelců	vetřelec	k1gMnPc2	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Včelí	včelí	k2eAgInSc1d1	včelí
vosk	vosk	k1gInSc1	vosk
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
odolný	odolný	k2eAgInSc1d1	odolný
ester	ester	k1gInSc1	ester
jednofunkčních	jednofunkční	k2eAgInPc2d1	jednofunkční
alkoholů	alkohol	k1gInPc2	alkohol
a	a	k8xC	a
vyšších	vysoký	k2eAgFnPc2d2	vyšší
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladé	k1gNnSc1	mladé
úlové	úlový	k2eAgFnSc2d1	úlová
včely	včela	k1gFnSc2	včela
ho	on	k3xPp3gMnSc4	on
produkují	produkovat	k5eAaImIp3nP	produkovat
jako	jako	k8xS	jako
výpotek	výpotek	k1gInSc4	výpotek
tzv.	tzv.	kA	tzv.
zrcátek	zrcátko	k1gNnPc2	zrcátko
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
článků	článek	k1gInPc2	článek
zadečku	zadeček	k1gInSc2	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Včelí	včelí	k2eAgInSc1d1	včelí
roj	roj	k1gInSc1	roj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
volně	volně	k6eAd1	volně
usadí	usadit	k5eAaPmIp3nS	usadit
např.	např.	kA	např.
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
zahájí	zahájit	k5eAaPmIp3nS	zahájit
neprodleně	prodleně	k6eNd1	prodleně
výstavbu	výstavba	k1gFnSc4	výstavba
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
se	se	k3xPyFc4	se
zavěsí	zavěsit	k5eAaPmIp3nP	zavěsit
na	na	k7c4	na
strop	strop	k1gInSc4	strop
dutiny	dutina	k1gFnSc2	dutina
a	a	k8xC	a
nožkami	nožka	k1gFnPc7	nožka
zaklesnuté	zaklesnutý	k2eAgFnSc2d1	zaklesnutá
jedna	jeden	k4xCgFnSc1	jeden
do	do	k7c2	do
druhé	druhý	k4xOgNnSc4	druhý
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
jakýsi	jakýsi	k3yIgInSc1	jakýsi
živý	živý	k2eAgInSc1d1	živý
závěs	závěs	k1gInSc1	závěs
<g/>
.	.	kIx.	.
</s>
<s>
Voskové	voskový	k2eAgInPc1d1	voskový
výpotky	výpotek	k1gInPc1	výpotek
promíchávají	promíchávat	k5eAaImIp3nP	promíchávat
se	s	k7c7	s
slinami	slina	k1gFnPc7	slina
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
modelují	modelovat	k5eAaImIp3nP	modelovat
plást	plást	k1gInSc4	plást
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
svislá	svislý	k2eAgFnSc1d1	svislá
tenká	tenký	k2eAgFnSc1d1	tenká
vosková	voskový	k2eAgFnSc1d1	vosková
stěna	stěna	k1gFnSc1	stěna
osázená	osázený	k2eAgFnSc1d1	osázená
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
hustou	hustý	k2eAgFnSc7d1	hustá
sítí	síť	k1gFnSc7	síť
šestistěnných	šestistěnný	k2eAgFnPc2d1	šestistěnná
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
v	v	k7c6	v
umělém	umělý	k2eAgNnSc6d1	umělé
prostředí	prostředí	k1gNnSc6	prostředí
úlu	úl	k1gInSc2	úl
stavějí	stavět	k5eAaImIp3nP	stavět
buňky	buňka	k1gFnPc1	buňka
na	na	k7c6	na
předem	předem	k6eAd1	předem
vylisovaných	vylisovaný	k2eAgInPc6d1	vylisovaný
voskových	voskový	k2eAgInPc6d1	voskový
polotovarech	polotovar	k1gInPc6	polotovar
(	(	kIx(	(
<g/>
mezistěnách	mezistěna	k1gFnPc6	mezistěna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezistěny	mezistěna	k1gFnPc4	mezistěna
dodává	dodávat	k5eAaImIp3nS	dodávat
do	do	k7c2	do
úlu	úl	k1gInSc2	úl
včelař	včelař	k1gMnSc1	včelař
v	v	k7c6	v
potřebném	potřebný	k2eAgNnSc6d1	potřebné
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
mezistěny	mezistěna	k1gFnSc2	mezistěna
je	být	k5eAaImIp3nS	být
limitována	limitovat	k5eAaBmNgFnS	limitovat
rozměrem	rozměr	k1gInSc7	rozměr
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
rámku	rámek	k1gInSc2	rámek
–	–	k?	–
rámkovou	rámkový	k2eAgFnSc7d1	rámková
mírou	míra	k1gFnSc7	míra
<g/>
.	.	kIx.	.
</s>
<s>
Voskový	voskový	k2eAgInSc1d1	voskový
plást	plást	k1gInSc1	plást
je	být	k5eAaImIp3nS	být
pozoruhodný	pozoruhodný	k2eAgMnSc1d1	pozoruhodný
svojí	svůj	k3xOyFgFnSc7	svůj
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
dutý	dutý	k2eAgInSc1d1	dutý
šestistěn	šestistěn	k2eAgInSc1d1	šestistěn
trubkovitého	trubkovitý	k2eAgInSc2d1	trubkovitý
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
buňka	buňka	k1gFnSc1	buňka
<g/>
)	)	kIx)	)
až	až	k9	až
15	[number]	k4	15
mm	mm	kA	mm
hluboký	hluboký	k2eAgInSc4d1	hluboký
<g/>
.	.	kIx.	.
</s>
<s>
Šestiboký	šestiboký	k2eAgInSc1d1	šestiboký
tvar	tvar	k1gInSc1	tvar
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stěny	stěna	k1gFnPc1	stěna
buňky	buňka	k1gFnSc2	buňka
jsou	být	k5eAaImIp3nP	být
společné	společný	k2eAgInPc1d1	společný
buňkám	buňka	k1gFnPc3	buňka
sousedním	sousední	k2eAgInPc3d1	sousední
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
do	do	k7c2	do
sebe	se	k3xPyFc2	se
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
zapadají	zapadat	k5eAaImIp3nP	zapadat
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
buňky	buňka	k1gFnSc2	buňka
je	být	k5eAaImIp3nS	být
uzavřené	uzavřený	k2eAgNnSc1d1	uzavřené
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
společné	společný	k2eAgFnSc6d1	společná
s	s	k7c7	s
buňkami	buňka	k1gFnPc7	buňka
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
plástu	plást	k1gInSc2	plást
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příčném	příčný	k2eAgInSc6d1	příčný
řezu	řez	k1gInSc6	řez
se	se	k3xPyFc4	se
plást	plást	k1gInSc1	plást
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
stromečková	stromečkový	k2eAgFnSc1d1	stromečková
struktura	struktura	k1gFnSc1	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc4d1	střední
osu	osa	k1gFnSc4	osa
tvoří	tvořit	k5eAaImIp3nS	tvořit
dna	dno	k1gNnSc2	dno
buněk	buňka	k1gFnPc2	buňka
resp.	resp.	kA	resp.
mezistěna	mezistěna	k1gFnSc1	mezistěna
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
buňky	buňka	k1gFnPc1	buňka
vybíhají	vybíhat	k5eAaImIp3nP	vybíhat
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
vzhůru	vzhůru	k6eAd1	vzhůru
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
úhlu	úhel	k1gInSc6	úhel
asi	asi	k9	asi
5	[number]	k4	5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Nevodorovný	vodorovný	k2eNgInSc1d1	vodorovný
sklon	sklon	k1gInSc1	sklon
přispívá	přispívat	k5eAaImIp3nS	přispívat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
adhezí	adheze	k1gFnSc7	adheze
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsah	obsah	k1gInSc1	obsah
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
řídká	řídký	k2eAgFnSc1d1	řídká
sladina	sladina	k1gFnSc1	sladina
<g/>
,	,	kIx,	,
med	med	k1gInSc1	med
či	či	k8xC	či
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
nemohou	moct	k5eNaImIp3nP	moct
vytéci	vytéct	k5eAaPmF	vytéct
<g/>
.	.	kIx.	.
</s>
<s>
Vodu	voda	k1gFnSc4	voda
přinášejí	přinášet	k5eAaImIp3nP	přinášet
včely	včela	k1gFnPc1	včela
létavky	létavka	k1gFnSc2	létavka
z	z	k7c2	z
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
vodě	voda	k1gFnSc6	voda
z	z	k7c2	z
lidského	lidský	k2eAgNnSc2d1	lidské
hlediska	hledisko	k1gNnSc2	hledisko
nekvalitní	kvalitní	k2eNgNnSc1d1	nekvalitní
(	(	kIx(	(
<g/>
močůvka	močůvka	k1gFnSc1	močůvka
<g/>
,	,	kIx,	,
okraje	okraj	k1gInPc4	okraj
zakalených	zakalený	k2eAgFnPc2d1	zakalená
louží	louž	k1gFnPc2	louž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přinesená	přinesený	k2eAgFnSc1d1	přinesená
voda	voda	k1gFnSc1	voda
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
ředění	ředění	k1gNnSc3	ředění
medu	med	k1gInSc2	med
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
na	na	k7c4	na
krmivo	krmivo	k1gNnSc4	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
odpařující	odpařující	k2eAgFnSc1d1	odpařující
se	se	k3xPyFc4	se
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
při	při	k7c6	při
zahušťování	zahušťování	k1gNnSc6	zahušťování
sladiny	sladina	k1gFnSc2	sladina
a	a	k8xC	a
dozrávání	dozrávání	k1gNnSc2	dozrávání
medu	med	k1gInSc2	med
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
důležitým	důležitý	k2eAgInSc7d1	důležitý
činitelem	činitel	k1gInSc7	činitel
při	při	k7c6	při
regulaci	regulace	k1gFnSc6	regulace
úlového	úlový	k2eAgNnSc2d1	úlové
mikroklimatu	mikroklima	k1gNnSc2	mikroklima
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
účinky	účinek	k1gInPc1	účinek
odpařující	odpařující	k2eAgFnSc2d1	odpařující
se	se	k3xPyFc4	se
vody	voda	k1gFnSc2	voda
včely	včela	k1gFnSc2	včela
využívají	využívat	k5eAaPmIp3nP	využívat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
aktivním	aktivní	k2eAgNnSc7d1	aktivní
větráním	větrání	k1gNnSc7	větrání
(	(	kIx(	(
<g/>
víření	víření	k1gNnSc1	víření
křídel	křídlo	k1gNnPc2	křídlo
na	na	k7c4	na
česně	česně	k6eAd1	česně
<g/>
)	)	kIx)	)
regulují	regulovat	k5eAaImIp3nP	regulovat
vlhkost	vlhkost	k1gFnSc4	vlhkost
a	a	k8xC	a
teplotu	teplota	k1gFnSc4	teplota
úlového	úlový	k2eAgNnSc2d1	úlové
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
vidí	vidět	k5eAaImIp3nP	vidět
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
soudilo	soudit	k5eAaImAgNnS	soudit
<g/>
.	.	kIx.	.
</s>
<s>
Včela	včela	k1gFnSc1	včela
medonosná	medonosný	k2eAgFnSc1d1	medonosná
jako	jako	k9	jako
sociální	sociální	k2eAgInSc4d1	sociální
hmyz	hmyz	k1gInSc4	hmyz
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
rozvinutou	rozvinutý	k2eAgFnSc7d1	rozvinutá
schopností	schopnost	k1gFnSc7	schopnost
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
informuje	informovat	k5eAaBmIp3nS	informovat
včela	včela	k1gFnSc1	včela
průzkumnice	průzkumnice	k1gFnSc2	průzkumnice
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
nového	nový	k2eAgInSc2d1	nový
zdroje	zdroj	k1gInSc2	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
:	:	kIx,	:
Děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
tak	tak	k9	tak
pomocí	pomocí	k7c2	pomocí
včelího	včelí	k2eAgInSc2d1	včelí
tance	tanec	k1gInSc2	tanec
<g/>
:	:	kIx,	:
Na	na	k7c6	na
svislé	svislý	k2eAgFnSc6d1	svislá
ploše	plocha	k1gFnSc6	plocha
plástu	plást	k1gInSc2	plást
včela	včela	k1gFnSc1	včela
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
určitým	určitý	k2eAgInSc7d1	určitý
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
bzučí	bzučet	k5eAaImIp3nS	bzučet
křídly	křídlo	k1gNnPc7	křídlo
a	a	k8xC	a
vrtí	vrtit	k5eAaImIp3nS	vrtit
zadečkem	zadeček	k1gInSc7	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
vrtivého	vrtivý	k2eAgInSc2d1	vrtivý
přímého	přímý	k2eAgInSc2d1	přímý
běhu	běh	k1gInSc2	běh
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
po	po	k7c6	po
elipse	elipsa	k1gFnSc6	elipsa
k	k	k7c3	k
výchozímu	výchozí	k2eAgInSc3d1	výchozí
bodu	bod	k1gInSc3	bod
a	a	k8xC	a
opět	opět	k6eAd1	opět
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
<g/>
.	.	kIx.	.
</s>
<s>
Vektor	vektor	k1gInSc1	vektor
vrtivého	vrtivý	k2eAgInSc2d1	vrtivý
běhu	běh	k1gInSc2	běh
svírá	svírat	k5eAaImIp3nS	svírat
se	s	k7c7	s
svislicí	svislice	k1gFnSc7	svislice
plástu	plást	k1gInSc2	plást
úhel	úhel	k1gInSc1	úhel
shodný	shodný	k2eAgInSc1d1	shodný
s	s	k7c7	s
úhlem	úhel	k1gInSc7	úhel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
svírá	svírat	k5eAaImIp3nS	svírat
směr	směr	k1gInSc4	směr
k	k	k7c3	k
potravě	potrava	k1gFnSc3	potrava
a	a	k8xC	a
poloha	poloha	k1gFnSc1	poloha
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tanci	tanec	k1gInSc6	tanec
je	být	k5eAaImIp3nS	být
včela	včela	k1gFnSc1	včela
průzkumnice	průzkumnice	k1gFnSc2	průzkumnice
obklopena	obklopen	k2eAgFnSc1d1	obklopena
tzv.	tzv.	kA	tzv.
rekrutkami	rekrutka	k1gFnPc7	rekrutka
<g/>
.	.	kIx.	.
</s>
<s>
Rekrutky	Rekrutka	k1gFnPc1	Rekrutka
její	její	k3xOp3gInSc4	její
taneček	taneček	k1gInSc4	taneček
sledují	sledovat	k5eAaImIp3nP	sledovat
a	a	k8xC	a
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
vyrážejí	vyrážet	k5eAaImIp3nP	vyrážet
z	z	k7c2	z
úlu	úl	k1gInSc2	úl
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
byly	být	k5eAaImAgInP	být
takto	takto	k6eAd1	takto
informovány	informovat	k5eAaBmNgInP	informovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
slunce	slunce	k1gNnSc1	slunce
za	za	k7c7	za
mraky	mrak	k1gInPc7	mrak
<g/>
,	,	kIx,	,
včely	včela	k1gFnPc4	včela
vnímají	vnímat	k5eAaImIp3nP	vnímat
rozptýlené	rozptýlený	k2eAgNnSc4d1	rozptýlené
polarizované	polarizovaný	k2eAgNnSc4d1	polarizované
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
určí	určit	k5eAaPmIp3nS	určit
polohu	poloha	k1gFnSc4	poloha
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgInPc4d1	uvedený
jevy	jev	k1gInPc4	jev
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
německý	německý	k2eAgMnSc1d1	německý
zoolog	zoolog	k1gMnSc1	zoolog
Karl	Karl	k1gMnSc1	Karl
von	von	k1gInSc4	von
Frisch	Frischa	k1gFnPc2	Frischa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
způsob	způsob	k1gInSc1	způsob
komunikace	komunikace	k1gFnSc2	komunikace
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
především	především	k6eAd1	především
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
chemických	chemický	k2eAgInPc2d1	chemický
podnětů	podnět	k1gInPc2	podnět
pomocí	pomocí	k7c2	pomocí
feromonů	feromon	k1gInPc2	feromon
<g/>
:	:	kIx,	:
Feromony	feromon	k1gInPc1	feromon
jedové	jedový	k2eAgFnSc2d1	jedová
žlázy	žláza	k1gFnSc2	žláza
–	–	k?	–
Nápadný	nápadný	k2eAgInSc1d1	nápadný
i	i	k8xC	i
člověkem	člověk	k1gMnSc7	člověk
vnímatelný	vnímatelný	k2eAgInSc4d1	vnímatelný
je	být	k5eAaImIp3nS	být
chemický	chemický	k2eAgInSc4d1	chemický
signál	signál	k1gInSc4	signál
k	k	k7c3	k
hromadnému	hromadný	k2eAgInSc3d1	hromadný
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vbodnutí	vbodnutí	k1gNnSc6	vbodnutí
je	být	k5eAaImIp3nS	být
žihadlo	žihadlo	k1gNnSc1	žihadlo
včely	včela	k1gFnSc2	včela
vytrženo	vytrhnout	k5eAaPmNgNnS	vytrhnout
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
obnažení	obnažení	k1gNnSc3	obnažení
výstelky	výstelka	k1gFnSc2	výstelka
jedové	jedový	k2eAgFnSc2d1	jedová
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
včelího	včelí	k2eAgNnSc2d1	včelí
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
syntetizovaná	syntetizovaný	k2eAgFnSc1d1	syntetizovaná
směs	směs	k1gFnSc1	směs
feromonů	feromon	k1gInPc2	feromon
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
vůně	vůně	k1gFnSc1	vůně
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
ostatní	ostatní	k2eAgFnPc4d1	ostatní
včely	včela	k1gFnPc4	včela
k	k	k7c3	k
agresi	agrese	k1gFnSc3	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Feromony	feromon	k1gInPc1	feromon
Nasanovy	Nasanův	k2eAgFnSc2d1	Nasanův
žlázy	žláza	k1gFnSc2	žláza
–	–	k?	–
Nasanova	Nasanův	k2eAgFnSc1d1	Nasanův
žláza	žláza	k1gFnSc1	žláza
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
ukončení	ukončení	k1gNnSc2	ukončení
zadečku	zadeček	k1gInSc2	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
včely	včela	k1gFnPc4	včela
dělnice	dělnice	k1gFnSc2	dělnice
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
snůšky	snůška	k1gFnSc2	snůška
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
úlu	úl	k1gInSc2	úl
zvedají	zvedat	k5eAaImIp3nP	zvedat
zadečky	zadeček	k1gInPc1	zadeček
a	a	k8xC	a
vířením	víření	k1gNnPc3	víření
křídel	křídlo	k1gNnPc2	křídlo
rozptylují	rozptylovat	k5eAaImIp3nP	rozptylovat
feromon	feromon	k1gInSc4	feromon
přitahující	přitahující	k2eAgFnSc2d1	přitahující
ostatní	ostatní	k2eAgFnSc2d1	ostatní
včely	včela	k1gFnSc2	včela
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
takto	takto	k6eAd1	takto
lze	lze	k6eAd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
časté	častý	k2eAgNnSc4d1	časté
zalétávání	zalétávání	k1gNnSc4	zalétávání
včel	včela	k1gFnPc2	včela
do	do	k7c2	do
cizího	cizí	k2eAgInSc2d1	cizí
úlu	úl	k1gInSc2	úl
<g/>
.	.	kIx.	.
</s>
<s>
Feromon	feromon	k1gInSc1	feromon
"	"	kIx"	"
<g/>
mateří	mateří	k2eAgFnSc1d1	mateří
látka	látka	k1gFnSc1	látka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
nezaměnit	zaměnit	k5eNaPmF	zaměnit
s	s	k7c7	s
mateří	mateří	k2eAgFnSc7d1	mateří
kašičkou	kašička	k1gFnSc7	kašička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyústění	vyústění	k1gNnSc4	vyústění
žlázy	žláza	k1gFnSc2	žláza
produkující	produkující	k2eAgFnSc4d1	produkující
mateří	mateří	k2eAgFnSc4d1	mateří
látku	látka	k1gFnSc4	látka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kusadel	kusadla	k1gNnPc2	kusadla
(	(	kIx(	(
<g/>
mandibuly	mandibula	k1gFnSc2	mandibula
<g/>
)	)	kIx)	)
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Feromon	feromon	k1gInSc1	feromon
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Uvedeny	uvést	k5eAaPmNgFnP	uvést
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
<g/>
:	:	kIx,	:
Přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
dělnice	dělnice	k1gFnPc1	dělnice
pečovatelky	pečovatelka	k1gFnSc2	pečovatelka
a	a	k8xC	a
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
soudržnost	soudržnost	k1gFnSc4	soudržnost
včelstva	včelstvo	k1gNnSc2	včelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
při	při	k7c6	při
krmení	krmení	k1gNnSc6	krmení
matky	matka	k1gFnSc2	matka
přejímají	přejímat	k5eAaImIp3nP	přejímat
mateří	mateří	k2eAgFnSc4d1	mateří
látku	látka	k1gFnSc4	látka
a	a	k8xC	a
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
včelami	včela	k1gFnPc7	včela
ji	on	k3xPp3gFnSc4	on
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
sexuální	sexuální	k2eAgMnSc1d1	sexuální
atraktant	atraktant	k1gMnSc1	atraktant
<g/>
;	;	kIx,	;
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
trubce	trubec	k1gMnPc4	trubec
ke	k	k7c3	k
kopulaci	kopulace	k1gFnSc3	kopulace
při	při	k7c6	při
snubním	snubní	k2eAgInSc6d1	snubní
letu	let	k1gInSc6	let
Nedostatek	nedostatek	k1gInSc1	nedostatek
feromonu	feromon	k1gInSc2	feromon
<g/>
:	:	kIx,	:
při	při	k7c6	při
uhynutí	uhynutí	k1gNnSc6	uhynutí
matky	matka	k1gFnSc2	matka
vede	vést	k5eAaImIp3nS	vést
včely	včela	k1gFnPc4	včela
k	k	k7c3	k
naražení	naražení	k1gNnSc3	naražení
nouzových	nouzový	k2eAgInPc2d1	nouzový
matečníků	matečník	k1gInPc2	matečník
pro	pro	k7c4	pro
výchovu	výchova	k1gFnSc4	výchova
matky	matka	k1gFnSc2	matka
náhradní	náhradní	k2eAgFnSc1d1	náhradní
snížením	snížení	k1gNnSc7	snížení
produkce	produkce	k1gFnSc2	produkce
u	u	k7c2	u
staré	starý	k2eAgFnSc2d1	stará
matky	matka	k1gFnSc2	matka
nastartuje	nastartovat	k5eAaPmIp3nS	nastartovat
přípravy	příprava	k1gFnPc4	příprava
k	k	k7c3	k
rojení	rojení	k1gNnSc3	rojení
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
matečníky	matečník	k1gInPc1	matečník
a	a	k8xC	a
výchova	výchova	k1gFnSc1	výchova
nových	nový	k2eAgFnPc2d1	nová
matek	matka	k1gFnPc2	matka
<g/>
)	)	kIx)	)
Obranný	obranný	k2eAgInSc1d1	obranný
pud	pud	k1gInSc1	pud
včel	včela	k1gFnPc2	včela
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
podmínek	podmínka	k1gFnPc2	podmínka
přežití	přežití	k1gNnSc2	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnPc1	včela
musejí	muset	k5eAaImIp3nP	muset
uhlídat	uhlídat	k5eAaImF	uhlídat
shromážděné	shromážděný	k2eAgFnPc4d1	shromážděná
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
samotnou	samotný	k2eAgFnSc4d1	samotná
existenci	existence	k1gFnSc4	existence
včelstva	včelstvo	k1gNnSc2	včelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
včel	včela	k1gFnPc2	včela
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
vnímají	vnímat	k5eAaImIp3nP	vnímat
<g/>
-li	i	k?	-li
včely	včela	k1gFnPc1	včela
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
situaci	situace	k1gFnSc4	situace
jako	jako	k8xC	jako
ohrožení	ohrožení	k1gNnSc4	ohrožení
zásob	zásoba	k1gFnPc2	zásoba
či	či	k8xC	či
existenci	existence	k1gFnSc4	existence
včelstva	včelstvo	k1gNnSc2	včelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
včelí	včelí	k2eAgInSc4d1	včelí
jed	jed	k1gInSc4	jed
<g/>
.	.	kIx.	.
</s>
<s>
Pohotovost	pohotovost	k1gFnSc1	pohotovost
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
drží	držet	k5eAaImIp3nS	držet
tzv.	tzv.	kA	tzv.
strážkyně	strážkyně	k1gFnSc1	strážkyně
na	na	k7c6	na
česně	česno	k1gNnSc6	česno
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
vibrace	vibrace	k1gFnPc4	vibrace
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
úlu	úl	k1gInSc2	úl
<g/>
.	.	kIx.	.
</s>
<s>
Zkušení	zkušený	k2eAgMnPc1d1	zkušený
včelaři	včelař	k1gMnPc1	včelař
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kritická	kritický	k2eAgFnSc1d1	kritická
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
česna	česno	k1gNnSc2	česno
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5	[number]	k4	5
m.	m.	k?	m.
Při	při	k7c6	při
přiblížení	přiblížení	k1gNnSc6	přiblížení
pod	pod	k7c4	pod
tuto	tento	k3xDgFnSc4	tento
hranici	hranice	k1gFnSc4	hranice
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
útok	útok	k1gInSc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
úlem	úl	k1gInSc7	úl
vyprovokuje	vyprovokovat	k5eAaPmIp3nS	vyprovokovat
k	k	k7c3	k
obranné	obranný	k2eAgFnSc3d1	obranná
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
nalétávají	nalétávat	k5eAaImIp3nP	nalétávat
<g/>
,	,	kIx,	,
varovně	varovně	k6eAd1	varovně
bzučí	bzučet	k5eAaImIp3nS	bzučet
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dojde	dojít	k5eAaPmIp3nS	dojít
na	na	k7c4	na
žihadlo	žihadlo	k1gNnSc4	žihadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
vpichu	vpich	k1gInSc6	vpich
je	být	k5eAaImIp3nS	být
cítit	cítit	k5eAaImF	cítit
zřetelnou	zřetelný	k2eAgFnSc4d1	zřetelná
nakyslou	nakyslý	k2eAgFnSc4d1	nakyslá
vůni	vůně	k1gFnSc4	vůně
včelího	včelí	k2eAgInSc2d1	včelí
jedu	jed	k1gInSc2	jed
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k8xC	i
feromonu	feromon	k1gInSc2	feromon
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
komunikace	komunikace	k1gFnSc2	komunikace
včel	včela	k1gFnPc2	včela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
signál	signál	k1gInSc4	signál
i	i	k9	i
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgFnPc4d1	ostatní
včely	včela	k1gFnPc4	včela
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
příčiny	příčina	k1gFnPc1	příčina
provokující	provokující	k2eAgFnSc4d1	provokující
agresivitu	agresivita	k1gFnSc4	agresivita
včel	včela	k1gFnPc2	včela
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
obtěžování	obtěžování	k1gNnSc4	obtěžování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
,	,	kIx,	,
vos	vosa	k1gFnPc2	vosa
<g/>
,	,	kIx,	,
ptáku	pták	k1gMnSc6	pták
<g/>
,	,	kIx,	,
hlodavců	hlodavec	k1gMnPc2	hlodavec
sečení	sečení	k1gNnSc2	sečení
trávy	tráva	k1gFnSc2	tráva
křovinořezem	křovinořez	k1gInSc7	křovinořez
<g/>
,	,	kIx,	,
broušení	broušení	k1gNnSc4	broušení
kosy	kosa	k1gFnSc2	kosa
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
pohyb	pohyb	k1gInSc1	pohyb
koní	kůň	k1gMnPc2	kůň
či	či	k8xC	či
jiných	jiný	k2eAgNnPc2d1	jiné
velkých	velký	k2eAgNnPc2d1	velké
zvířat	zvíře	k1gNnPc2	zvíře
Nejcitlivější	citlivý	k2eAgFnSc2d3	nejcitlivější
na	na	k7c6	na
rušení	rušení	k1gNnSc6	rušení
jsou	být	k5eAaImIp3nP	být
včely	včela	k1gFnPc4	včela
za	za	k7c2	za
časného	časný	k2eAgNnSc2d1	časné
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
obdobích	období	k1gNnPc6	období
mimo	mimo	k7c4	mimo
snůšku	snůška	k1gFnSc4	snůška
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
rojení	rojení	k1gNnSc6	rojení
<g/>
,	,	kIx,	,
jakkoliv	jakkoliv	k6eAd1	jakkoliv
letící	letící	k2eAgMnSc1d1	letící
a	a	k8xC	a
hučící	hučící	k2eAgNnSc1d1	hučící
včelstvo	včelstvo	k1gNnSc1	včelstvo
působí	působit	k5eAaImIp3nS	působit
impozantním	impozantní	k2eAgInSc7d1	impozantní
až	až	k8xS	až
hrozivým	hrozivý	k2eAgInSc7d1	hrozivý
dojmem	dojem	k1gInSc7	dojem
<g/>
,	,	kIx,	,
obavy	obava	k1gFnPc1	obava
nejsou	být	k5eNaImIp3nP	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pozoruhodné	pozoruhodný	k2eAgInPc4d1	pozoruhodný
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
chorobám	choroba	k1gFnPc3	choroba
nemají	mít	k5eNaImIp3nP	mít
včely	včela	k1gFnPc4	včela
příliš	příliš	k6eAd1	příliš
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
genetickou	genetický	k2eAgFnSc4d1	genetická
výbavu	výbava	k1gFnSc4	výbava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nedostatek	nedostatek	k1gInSc1	nedostatek
je	být	k5eAaImIp3nS	být
kompenzován	kompenzovat	k5eAaBmNgInS	kompenzovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
organizovaností	organizovanost	k1gFnSc7	organizovanost
a	a	k8xC	a
účelným	účelný	k2eAgNnSc7d1	účelné
chováním	chování	k1gNnSc7	chování
celého	celý	k2eAgNnSc2d1	celé
hmyzího	hmyzí	k2eAgNnSc2d1	hmyzí
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
"	"	kIx"	"
<g/>
hygienických	hygienický	k2eAgInPc2d1	hygienický
návyků	návyk	k1gInPc2	návyk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čistící	čistící	k2eAgInSc1d1	čistící
pud	pud	k1gInSc1	pud
<g/>
,	,	kIx,	,
desinfekce	desinfekce	k1gFnSc1	desinfekce
propolisem	propolis	k1gInSc7	propolis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
obranné	obranný	k2eAgInPc1d1	obranný
mechanismy	mechanismus	k1gInPc1	mechanismus
včel	včela	k1gFnPc2	včela
selhávají	selhávat	k5eAaImIp3nP	selhávat
v	v	k7c6	v
konfrontaci	konfrontace	k1gFnSc6	konfrontace
se	s	k7c7	s
zavlečenými	zavlečený	k2eAgMnPc7d1	zavlečený
parazity	parazit	k1gMnPc7	parazit
(	(	kIx(	(
<g/>
kleštík	kleštík	k1gInSc1	kleštík
včelí	včelí	k2eAgFnSc1d1	včelí
Varroa	Varroa	k1gFnSc1	Varroa
destructor	destructor	k1gMnSc1	destructor
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pesticidy	pesticid	k1gInPc1	pesticid
a	a	k8xC	a
insekticidy	insekticid	k1gInPc1	insekticid
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
jsou	být	k5eAaImIp3nP	být
odkázány	odkázat	k5eAaPmNgFnP	odkázat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
včelstvo	včelstvo	k1gNnSc1	včelstvo
nemůže	moct	k5eNaImIp3nS	moct
dlouho	dlouho	k6eAd1	dlouho
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
virózy	viróza	k1gFnPc1	viróza
<g/>
:	:	kIx,	:
virus	virus	k1gInSc1	virus
deformovaných	deformovaný	k2eAgNnPc2d1	deformované
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
Deformed	Deformed	k1gInSc1	Deformed
Wing	Wing	k1gInSc1	Wing
Virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
DWV	DWV	kA	DWV
<g/>
)	)	kIx)	)
virus	virus	k1gInSc1	virus
pytlíčkovitého	pytlíčkovitý	k2eAgInSc2d1	pytlíčkovitý
plodu	plod	k1gInSc2	plod
<g/>
,	,	kIx,	,
Sacbrood	Sacbrood	k1gInSc1	Sacbrood
Virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
SBV	SBV	kA	SBV
<g/>
)	)	kIx)	)
virus	virus	k1gInSc1	virus
černání	černání	k1gNnSc2	černání
matečníků	matečník	k1gInPc2	matečník
Black	Blacko	k1gNnPc2	Blacko
Queen	Queno	k1gNnPc2	Queno
Cell	cello	k1gNnPc2	cello
Virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
BQCV	BQCV	kA	BQCV
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
kašmírský	kašmírský	k2eAgInSc1d1	kašmírský
virus	virus	k1gInSc1	virus
Kashmir	Kashmira	k1gFnPc2	Kashmira
Bee	Bee	k1gFnPc2	Bee
Virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
KBV	KBV	kA	KBV
<g/>
)	)	kIx)	)
mor	mor	k1gInSc1	mor
včelího	včelí	k2eAgInSc2d1	včelí
plodu	plod	k1gInSc2	plod
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
hniloba	hniloba	k1gFnSc1	hniloba
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
zvápenatění	zvápenatění	k1gNnSc6	zvápenatění
včelího	včelí	k2eAgInSc2d1	včelí
plodu	plod	k1gInSc2	plod
varroáza	varroáza	k1gFnSc1	varroáza
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
viróza	viróza	k1gFnSc1	viróza
virus	virus	k1gInSc1	virus
akutní	akutní	k2eAgFnSc2d1	akutní
paralýzy	paralýza	k1gFnSc2	paralýza
včel	včela	k1gFnPc2	včela
Acute	Acut	k1gMnSc5	Acut
(	(	kIx(	(
<g/>
Bee	Bee	k1gMnSc6	Bee
<g/>
)	)	kIx)	)
Paralysis	Paralysis	k1gFnSc2	Paralysis
Virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
ABPV	ABPV	kA	ABPV
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
virus	virus	k1gInSc1	virus
chronické	chronický	k2eAgFnSc2d1	chronická
paralýzy	paralýza	k1gFnSc2	paralýza
včel	včela	k1gFnPc2	včela
Chronic	Chronice	k1gFnPc2	Chronice
(	(	kIx(	(
<g/>
Bee	Bee	k1gFnSc1	Bee
<g/>
)	)	kIx)	)
Paralysis	Paralysis	k1gInSc1	Paralysis
Virus	virus	k1gInSc1	virus
(	(	kIx(	(
<g/>
CBPV	CBPV	kA	CBPV
<g/>
)	)	kIx)	)
nosemóza	nosemóza	k1gFnSc1	nosemóza
akarapidóza	akarapidóza	k1gFnSc1	akarapidóza
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
varroáza	varroáza	k1gFnSc1	varroáza
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
otrava	otrava	k1gFnSc1	otrava
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
–	–	k?	–
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
choroby	choroba	k1gFnPc4	choroba
podle	podle	k7c2	podle
veterinárního	veterinární	k2eAgInSc2d1	veterinární
zákona	zákon	k1gInSc2	zákon
majka	majka	k1gFnSc1	majka
fialová	fialový	k2eAgFnSc1d1	fialová
lesknáček	lesknáček	k1gInSc4	lesknáček
úlový	úlový	k2eAgInSc4d1	úlový
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
ČR	ČR	kA	ČR
nebyl	být	k5eNaImAgInS	být
dosud	dosud	k6eAd1	dosud
potvrzen	potvrzen	k2eAgInSc1d1	potvrzen
<g/>
)	)	kIx)	)
včelomorka	včelomorka	k1gFnSc1	včelomorka
obecná	obecná	k1gFnSc1	obecná
zavíječ	zavíječ	k1gInSc4	zavíječ
voskový	voskový	k2eAgInSc1d1	voskový
zavíječ	zavíječ	k1gInSc1	zavíječ
malý	malý	k2eAgInSc1d1	malý
Carl	Carl	k1gInSc4	Carl
Linné	Linné	k2eAgInSc4d1	Linné
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
včelu	včela	k1gFnSc4	včela
medonosnou	medonosný	k2eAgFnSc4d1	medonosná
jako	jako	k9	jako
včelu	včela	k1gFnSc4	včela
med	med	k1gInSc4	med
nosící	nosící	k2eAgNnSc1d1	nosící
(	(	kIx(	(
<g/>
mellifera	mellifera	k1gFnSc1	mellifera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
poznal	poznat	k5eAaPmAgMnS	poznat
určitou	určitý	k2eAgFnSc4d1	určitá
nepřesnost	nepřesnost	k1gFnSc4	nepřesnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
včely	včela	k1gFnPc4	včela
nosí	nosit	k5eAaImIp3nS	nosit
jen	jen	k9	jen
nektar	nektar	k1gInSc1	nektar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
úlu	úl	k1gInSc6	úl
zpracují	zpracovat	k5eAaPmIp3nP	zpracovat
na	na	k7c4	na
med	med	k1gInSc4	med
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgInS	navrhnout
proto	proto	k8xC	proto
nový	nový	k2eAgInSc4d1	nový
název	název	k1gInSc4	název
Apis	Apisa	k1gFnPc2	Apisa
mellifica	mellific	k1gInSc2	mellific
L.	L.	kA	L.
Podle	podle	k7c2	podle
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
zoologických	zoologický	k2eAgNnPc2d1	zoologické
pravidel	pravidlo	k1gNnPc2	pravidlo
je	být	k5eAaImIp3nS	být
platným	platný	k2eAgNnSc7d1	platné
jménem	jméno	k1gNnSc7	jméno
jméno	jméno	k1gNnSc1	jméno
Apis	Apisa	k1gFnPc2	Apisa
mellifera	mellifero	k1gNnSc2	mellifero
<g/>
.	.	kIx.	.
</s>
<s>
Včely	včela	k1gFnSc2	včela
lidé	člověk	k1gMnPc1	člověk
chovají	chovat	k5eAaImIp3nP	chovat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
získávání	získávání	k1gNnSc1	získávání
včelích	včelí	k2eAgInPc2d1	včelí
produktů	produkt	k1gInPc2	produkt
(	(	kIx(	(
<g/>
med	med	k1gInSc1	med
<g/>
,	,	kIx,	,
vosk	vosk	k1gInSc1	vosk
<g/>
,	,	kIx,	,
propolis	propolis	k1gInSc1	propolis
<g/>
,	,	kIx,	,
mateří	mateří	k2eAgFnSc1d1	mateří
kašička	kašička	k1gFnSc1	kašička
<g/>
,	,	kIx,	,
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
pyl	pyl	k1gInSc1	pyl
<g/>
)	)	kIx)	)
opylovací	opylovací	k2eAgFnSc1d1	opylovací
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
totiž	totiž	k9	totiž
plodiny	plodina	k1gFnPc1	plodina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ovocné	ovocný	k2eAgInPc1d1	ovocný
stromy	strom	k1gInPc1	strom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bez	bez	k7c2	bez
opylení	opylení	k1gNnSc2	opylení
včelami	včela	k1gFnPc7	včela
není	být	k5eNaImIp3nS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
úroda	úroda	k1gFnSc1	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Včelaři	včelař	k1gMnPc1	včelař
proto	proto	k8xC	proto
přisunují	přisunovat	k5eAaImIp3nP	přisunovat
včely	včela	k1gFnPc4	včela
v	v	k7c6	v
kočovných	kočovný	k2eAgInPc6d1	kočovný
vozech	vůz	k1gInPc6	vůz
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
kvetoucích	kvetoucí	k2eAgInPc2d1	kvetoucí
porostů	porost	k1gInPc2	porost
v	v	k7c6	v
době	doba	k1gFnSc6	doba
květu	květ	k1gInSc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
opylováno	opylovat	k5eAaImNgNnS	opylovat
asi	asi	k9	asi
85	[number]	k4	85
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
;	;	kIx,	;
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
85	[number]	k4	85
%	%	kIx~	%
včelami	včela	k1gFnPc7	včela
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
U	u	k7c2	u
ovocných	ovocný	k2eAgInPc2d1	ovocný
stromů	strom	k1gInPc2	strom
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
květů	květ	k1gInPc2	květ
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Seznam	seznam	k1gInSc1	seznam
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
navštěvovaných	navštěvovaný	k2eAgFnPc2d1	navštěvovaná
včelami	včela	k1gFnPc7	včela
čítá	čítat	k5eAaImIp3nS	čítat
na	na	k7c6	na
170	[number]	k4	170
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
opylování	opylování	k1gNnSc2	opylování
včelami	včela	k1gFnPc7	včela
těžko	těžko	k6eAd1	těžko
obešly	obejít	k5eAaPmAgInP	obejít
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
skupinu	skupina	k1gFnSc4	skupina
40	[number]	k4	40
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
<g/>
:	:	kIx,	:
90,0	[number]	k4	90,0
%	%	kIx~	%
Třešeň	třešeň	k1gFnSc1	třešeň
<g/>
:	:	kIx,	:
98,5	[number]	k4	98,5
%	%	kIx~	%
Hrušeň	hrušeň	k1gFnSc1	hrušeň
<g/>
:	:	kIx,	:
92,0	[number]	k4	92,0
%	%	kIx~	%
Angrešt	angrešt	k1gInSc1	angrešt
<g/>
:	:	kIx,	:
73,3	[number]	k4	73,3
%	%	kIx~	%
Višeň	višeň	k1gFnSc1	višeň
samosprašná	samosprašný	k2eAgFnSc1d1	samosprašná
<g/>
:	:	kIx,	:
58,3	[number]	k4	58,3
%	%	kIx~	%
Slivoň	slivoň	k1gFnSc1	slivoň
samosprašná	samosprašný	k2eAgFnSc1d1	samosprašná
<g/>
:	:	kIx,	:
42,9	[number]	k4	42,9
%	%	kIx~	%
cizosprašné	cizosprašný	k2eAgFnSc2d1	cizosprašná
<g/>
:	:	kIx,	:
96,4	[number]	k4	96,4
%	%	kIx~	%
řepka	řepka	k1gFnSc1	řepka
olejka	olejka	k1gFnSc1	olejka
<g/>
:	:	kIx,	:
nárůst	nárůst	k1gInSc1	nárůst
výnosu	výnos	k1gInSc2	výnos
při	při	k7c6	při
opylení	opylení	k1gNnSc6	opylení
včelami	včela	k1gFnPc7	včela
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
30	[number]	k4	30
%	%	kIx~	%
Důležitým	důležitý	k2eAgInSc7d1	důležitý
opylovačem	opylovač	k1gInSc7	opylovač
je	být	k5eAaImIp3nS	být
i	i	k9	i
čmelák	čmelák	k1gMnSc1	čmelák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
opyluje	opylovat	k5eAaImIp3nS	opylovat
lépe	dobře	k6eAd2	dobře
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Úloha	úloha	k1gFnSc1	úloha
včel	včela	k1gFnPc2	včela
pro	pro	k7c4	pro
opylování	opylování	k1gNnSc4	opylování
se	se	k3xPyFc4	se
přeceňuje	přeceňovat	k5eAaImIp3nS	přeceňovat
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
nezastupitelná	zastupitelný	k2eNgFnSc1d1	nezastupitelná
<g/>
.	.	kIx.	.
</s>
<s>
Biologický	biologický	k2eAgInSc1d1	biologický
druh	druh	k1gInSc1	druh
včela	včela	k1gFnSc1	včela
medonosná	medonosný	k2eAgFnSc1d1	medonosná
(	(	kIx(	(
<g/>
Apis	Apis	k1gInSc1	Apis
melifera	melifero	k1gNnSc2	melifero
Linnaeus	Linnaeus	k1gInSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozčleněn	rozčleněn	k2eAgMnSc1d1	rozčleněn
do	do	k7c2	do
početné	početný	k2eAgFnSc2d1	početná
škály	škála	k1gFnSc2	škála
poddruhů	poddruh	k1gInPc2	poddruh
</s>
