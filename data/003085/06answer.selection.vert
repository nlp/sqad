<s>
Kakaduové	kakadu	k1gMnPc1	kakadu
jsou	být	k5eAaImIp3nP	být
ptáci	pták	k1gMnPc1	pták
vysloveně	vysloveně	k6eAd1	vysloveně
společenští	společenský	k2eAgMnPc1d1	společenský
<g/>
,	,	kIx,	,
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
hejna	hejno	k1gNnPc4	hejno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
.	.	kIx.	.
</s>
