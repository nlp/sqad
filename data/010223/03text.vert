<p>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS	Jevgenít
Babič	Babič	k1gInSc1	Babič
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
:	:	kIx,	:
Е	Е	k?	Е
М	М	k?	М
Б	Б	k?	Б
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sovětský	sovětský	k2eAgMnSc1d1	sovětský
reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
hokejový	hokejový	k2eAgMnSc1d1	hokejový
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Ruské	ruský	k2eAgFnSc2d1	ruská
a	a	k8xC	a
sovětské	sovětský	k2eAgFnSc2d1	sovětská
hokejové	hokejový	k2eAgFnSc2d1	hokejová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
členem	člen	k1gInSc7	člen
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
reprezentací	reprezentace	k1gFnSc7	reprezentace
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
získal	získat	k5eAaPmAgMnS	získat
jednu	jeden	k4xCgFnSc4	jeden
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
jednoho	jeden	k4xCgNnSc2	jeden
zlata	zlato	k1gNnSc2	zlato
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvou	dva	k4xCgNnPc2	dva
stříber	stříbro	k1gNnPc2	stříbro
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
a	a	k8xC	a
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
z	z	k7c2	z
MS.	MS.	k1gFnSc2	MS.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
oběšením	oběšení	k1gNnPc3	oběšení
<g/>
.	.	kIx.	.
</s>
</p>
