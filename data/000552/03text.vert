<s>
Červenec	červenec	k1gInSc1	červenec
je	být	k5eAaImIp3nS	být
sedmý	sedmý	k4xOgInSc1	sedmý
měsíc	měsíc	k1gInSc1	měsíc
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
měl	mít	k5eAaImAgInS	mít
31	[number]	k4	31
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
římském	římský	k2eAgInSc6d1	římský
kalendáři	kalendář	k1gInSc6	kalendář
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
Quintilis	Quintilis	k1gFnSc2	Quintilis
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Pátý	pátý	k4xOgInSc4	pátý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
44	[number]	k4	44
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
a	a	k8xC	a
poté	poté	k6eAd1	poté
římský	římský	k2eAgInSc1d1	římský
senát	senát	k1gInSc1	senát
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Marka	Marek	k1gMnSc2	Marek
Antonia	Antonio	k1gMnSc2	Antonio
změnil	změnit	k5eAaPmAgMnS	změnit
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Julia	Julius	k1gMnSc2	Julius
Caesara	Caesar	k1gMnSc2	Caesar
název	název	k1gInSc1	název
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
Quintilis	Quintilis	k1gFnSc1	Quintilis
na	na	k7c6	na
Julius	Julius	k1gMnSc1	Julius
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
měsíc	měsíc	k1gInSc1	měsíc
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
červenec	červenec	k1gInSc1	červenec
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
původně	původně	k6eAd1	původně
malý	malý	k2eAgInSc1d1	malý
červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
přípona	přípona	k1gFnSc1	přípona
zde	zde	k6eAd1	zde
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
následnost	následnost	k1gFnSc1	následnost
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc1	červenec
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
Etymologie	etymologie	k1gFnPc1	etymologie
měsíce	měsíc	k1gInSc2	měsíc
červenec	červenec	k1gInSc1	červenec
<g/>
:	:	kIx,	:
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
červen	červen	k2eAgInSc4d1	červen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začíná	začínat	k5eAaImIp3nS	začínat
zrát	zrát	k5eAaImF	zrát
první	první	k4xOgNnPc1	první
ovoce	ovoce	k1gNnPc1	ovoce
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Červenec	červenec	k1gInSc1	červenec
označuje	označovat	k5eAaImIp3nS	označovat
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
červené	červený	k2eAgNnSc1d1	červené
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ještě	ještě	k9	ještě
jen	jen	k9	jen
začalo	začít	k5eAaPmAgNnS	začít
zrát	zrát	k5eAaImF	zrát
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
zralé	zralý	k2eAgFnPc1d1	zralá
<g/>
.	.	kIx.	.
</s>
<s>
Červenec	červenec	k1gInSc1	červenec
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
slangový	slangový	k2eAgInSc4d1	slangový
název	název	k1gInSc4	název
pro	pro	k7c4	pro
načervenalou	načervenalý	k2eAgFnSc4d1	načervenalá
rudu	ruda	k1gFnSc4	ruda
<g/>
,	,	kIx,	,
wurtzit	wurtzit	k1gInSc4	wurtzit
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
červenec	červenec	k1gInSc1	červenec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
červenec	červenec	k1gInSc1	červenec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
