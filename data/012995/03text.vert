<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Cikánka	cikánka	k1gFnSc1	cikánka
II	II	kA	II
je	být	k5eAaImIp3nS	být
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
území	území	k1gNnSc4	území
hl.	hl.	k?	hl.
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Katastrálně	katastrálně	k6eAd1	katastrálně
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
městskou	městský	k2eAgFnSc4d1	městská
část	část	k1gFnSc4	část
Praha	Praha	k1gFnSc1	Praha
16	[number]	k4	16
-	-	kIx~	-
Radotín	Radotína	k1gFnPc2	Radotína
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
nepoužívané	používaný	k2eNgFnSc6d1	nepoužívaná
části	část	k1gFnSc6	část
lomu	lom	k1gInSc2	lom
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
součástí	součást	k1gFnSc7	součást
Sliveneckých	slivenecký	k2eAgInPc2d1	slivenecký
lomů	lom	k1gInPc2	lom
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
známých	známá	k1gFnPc2	známá
již	již	k6eAd1	již
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
památky	památka	k1gFnSc2	památka
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
0,39	[number]	k4	0,39
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
mezi	mezi	k7c7	mezi
297	[number]	k4	297
až	až	k9	až
313	[number]	k4	313
m.	m.	k?	m.
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Cikánku	cikánka	k1gFnSc4	cikánka
II	II	kA	II
za	za	k7c4	za
přírodní	přírodní	k2eAgFnSc4d1	přírodní
památku	památka	k1gFnSc4	památka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
plán	plán	k1gInSc1	plán
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
památku	památka	k1gFnSc4	památka
Cikánka	cikánka	k1gFnSc1	cikánka
II	II	kA	II
je	být	k5eAaImIp3nS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c4	na
období	období	k1gNnSc4	období
2010	[number]	k4	2010
–	–	k?	–
2024	[number]	k4	2024
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
lomy	lom	k1gInPc1	lom
chráněny	chráněn	k2eAgInPc1d1	chráněn
díky	díky	k7c3	díky
významným	významný	k2eAgMnPc3d1	významný
paleontologickým	paleontologický	k2eAgMnPc3d1	paleontologický
nálezům	nález	k1gInPc3	nález
ve	v	k7c6	v
skalních	skalní	k2eAgFnPc6d1	skalní
vrstvách	vrstva	k1gFnPc6	vrstva
na	na	k7c6	na
přechodech	přechod	k1gInPc6	přechod
Lochkovských	Lochkovský	k2eAgNnPc2d1	Lochkovské
souvrství	souvrství	k1gNnPc2	souvrství
a	a	k8xC	a
Sliveneckých	slivenecký	k2eAgNnPc2d1	slivenecký
<g/>
/	/	kIx~	/
<g/>
Pražských	pražský	k2eAgNnPc2d1	Pražské
souvrství	souvrství	k1gNnPc2	souvrství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lokalita	lokalita	k1gFnSc1	lokalita
==	==	k?	==
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Cikánka	cikánka	k1gFnSc1	cikánka
II	II	kA	II
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
Zadní	zadní	k2eAgFnSc7d1	zadní
Kopaninou	kopanina	k1gFnSc7	kopanina
a	a	k8xC	a
Lochkovem	Lochkov	k1gInSc7	Lochkov
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
dobývacího	dobývací	k2eAgInSc2d1	dobývací
prostoru	prostor	k1gInSc2	prostor
Radotínsko	Radotínsko	k1gNnSc4	Radotínsko
–	–	k?	–
Chuchelské	chuchelský	k2eAgInPc1d1	chuchelský
háje	háj	k1gInPc1	háj
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
lomů	lom	k1gInPc2	lom
opěrných	opěrný	k2eAgInPc2d1	opěrný
profilů	profil	k1gInPc2	profil
lochkov	lochkov	k1gInSc4	lochkov
–	–	k?	–
prag	praga	k1gFnPc2	praga
<g/>
,	,	kIx,	,
situovaných	situovaný	k2eAgInPc2d1	situovaný
ve	v	k7c6	v
skálách	skála	k1gFnPc6	skála
severně	severně	k6eAd1	severně
od	od	k7c2	od
lomu	lom	k1gInSc2	lom
Cikánka	Cikánek	k1gMnSc2	Cikánek
I	i	k8xC	i
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
svahu	svah	k1gInSc6	svah
Radotínského	radotínský	k2eAgInSc2d1	radotínský
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Cikánky	cikánka	k1gFnSc2	cikánka
II	II	kA	II
se	se	k3xPyFc4	se
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
těžil	těžit	k5eAaImAgMnS	těžit
růžový	růžový	k2eAgInSc4d1	růžový
slivenecký	slivenecký	k2eAgInSc4d1	slivenecký
vápenec	vápenec	k1gInSc4	vápenec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgMnSc7d3	nejčastější
okrasným	okrasný	k2eAgInSc7d1	okrasný
kamenem	kámen	k1gInSc7	kámen
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
lomy	lom	k1gInPc1	lom
patřily	patřit	k5eAaImAgInP	patřit
rytířskému	rytířský	k2eAgInSc3d1	rytířský
řádu	řád	k1gInSc3	řád
Křížovníků	křížovník	k1gMnPc2	křížovník
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vápenec	vápenec	k1gInSc1	vápenec
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
těžil	těžit	k5eAaImAgMnS	těžit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Přírodovědce	přírodovědec	k1gMnSc4	přírodovědec
zaujaly	zaujmout	k5eAaPmAgFnP	zaujmout
lomy	lom	k1gInPc4	lom
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Křížovníků	křížovník	k1gMnPc2	křížovník
byly	být	k5eAaImAgFnP	být
lomy	lom	k1gInPc4	lom
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
poměry	poměr	k1gInPc1	poměr
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
lomech	lom	k1gInPc6	lom
Cikánka	cikánka	k1gFnSc1	cikánka
II	II	kA	II
je	být	k5eAaImIp3nS	být
znatelné	znatelný	k2eAgNnSc4d1	znatelné
rozhraní	rozhraní	k1gNnSc4	rozhraní
dvou	dva	k4xCgFnPc2	dva
geologicky	geologicky	k6eAd1	geologicky
odlišných	odlišný	k2eAgFnPc2d1	odlišná
částí	část	k1gFnPc2	část
na	na	k7c6	na
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
straně	strana	k1gFnSc6	strana
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranice	hranice	k1gFnSc1	hranice
lochkovských	lochkovský	k2eAgNnPc2d1	lochkovský
souvrství	souvrství	k1gNnPc2	souvrství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Barrandienu	barrandien	k1gInSc2	barrandien
(	(	kIx(	(
<g/>
nejspodnější	spodní	k2eAgInSc1d3	nejspodnější
devon	devon	k1gInSc1	devon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
Pražské	pražský	k2eAgNnSc1d1	Pražské
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgInSc1d1	spodní
devon	devon	k1gInSc1	devon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Profil	profil	k1gInSc1	profil
vrstev	vrstva	k1gFnPc2	vrstva
lochkov	lochkov	k1gInSc4	lochkov
–	–	k?	–
prag	prag	k1gInSc1	prag
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
konodontové	konodontový	k2eAgFnPc4d1	konodontový
a	a	k8xC	a
tentakulitové	tentakulitový	k2eAgFnPc4d1	tentakulitový
zóny	zóna	k1gFnPc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
odkrytém	odkrytý	k2eAgNnSc6d1	odkryté
rozhraní	rozhraní	k1gNnSc6	rozhraní
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílela	podílet	k5eAaImAgFnS	podílet
jednak	jednak	k8xC	jednak
těžba	těžba	k1gFnSc1	těžba
mramoru	mramor	k1gInSc2	mramor
a	a	k8xC	a
jednak	jednak	k8xC	jednak
i	i	k9	i
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
vějířovitým	vějířovitý	k2eAgInSc7d1	vějířovitý
průběhem	průběh	k1gInSc7	průběh
radiálních	radiální	k2eAgFnPc2d1	radiální
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
trilobity	trilobit	k1gMnPc4	trilobit
Platyscutellum	Platyscutellum	k1gNnSc1	Platyscutellum
formosum	formosum	k1gInSc1	formosum
slivenecense	slivenecense	k1gFnSc1	slivenecense
<g/>
,	,	kIx,	,
Crotalocephalus	Crotalocephalus	k1gInSc1	Crotalocephalus
albertii	albertie	k1gFnSc4	albertie
<g/>
,	,	kIx,	,
Pragoproetus	Pragoproetus	k1gInSc4	Pragoproetus
pragensis	pragensis	k1gFnPc2	pragensis
<g/>
,	,	kIx,	,
ramenonožce	ramenonožec	k1gMnPc4	ramenonožec
Dalejodiscus	Dalejodiscus	k1gMnSc1	Dalejodiscus
subcomitans	subcomitans	k6eAd1	subcomitans
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
lomu	lom	k1gInSc2	lom
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
facie	facie	k1gFnSc2	facie
řeporyjských	řeporyjský	k2eAgInPc2d1	řeporyjský
vápenců	vápenec	k1gInPc2	vápenec
pražského	pražský	k2eAgNnSc2d1	Pražské
souvrství	souvrství	k1gNnSc2	souvrství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Před	před	k7c7	před
antropogenními	antropogenní	k2eAgInPc7d1	antropogenní
vlivy	vliv	k1gInPc7	vliv
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc4	oblast
Cikánky	cikánka	k1gFnSc2	cikánka
II	II	kA	II
historicky	historicky	k6eAd1	historicky
zřejmě	zřejmě	k6eAd1	zřejmě
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
šipákovou	šipákův	k2eAgFnSc7d1	šipákův
lesostepí	lesostep	k1gFnSc7	lesostep
a	a	k8xC	a
stepní	stepní	k2eAgNnPc4d1	stepní
společenstva	společenstvo	k1gNnPc4	společenstvo
pokrývala	pokrývat	k5eAaImAgFnS	pokrývat
nejvíce	hodně	k6eAd3	hodně
vystavované	vystavovaný	k2eAgFnPc4d1	vystavovaná
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
nebyla	být	k5eNaImAgFnS	být
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
prozkoumána	prozkoumán	k2eAgFnSc1d1	prozkoumána
a	a	k8xC	a
údaje	údaj	k1gInPc4	údaj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
o	o	k7c4	o
vegetaci	vegetace	k1gFnSc4	vegetace
daného	daný	k2eAgNnSc2d1	dané
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nepřesné	přesný	k2eNgInPc1d1	nepřesný
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
souhrnně	souhrnně	k6eAd1	souhrnně
pro	pro	k7c4	pro
památky	památka	k1gFnPc4	památka
Cikánky	cikánka	k1gFnSc2	cikánka
I	I	kA	I
<g/>
+	+	kIx~	+
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc4	co
mají	mít	k5eAaImIp3nP	mít
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
jistě	jistě	k6eAd1	jistě
společné	společný	k2eAgFnPc1d1	společná
jsou	být	k5eAaImIp3nP	být
dřeviny	dřevina	k1gFnPc1	dřevina
zarůstající	zarůstající	k2eAgNnSc1d1	zarůstající
dno	dno	k1gNnSc1	dno
a	a	k8xC	a
svahy	svah	k1gInPc1	svah
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
na	na	k7c6	na
lomových	lomový	k2eAgFnPc6d1	Lomová
stěnách	stěna	k1gFnPc6	stěna
xerotermní	xerotermní	k2eAgFnSc2d1	xerotermní
traviny	travina	k1gFnSc2	travina
(	(	kIx(	(
<g/>
Festucion	Festucion	k1gInSc1	Festucion
valesiacae	valesiaca	k1gInSc2	valesiaca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
další	další	k2eAgNnPc1d1	další
xerotermní	xerotermní	k2eAgNnPc1d1	xerotermní
společenstva	společenstvo	k1gNnPc1	společenstvo
(	(	kIx(	(
<g/>
Carpinion	Carpinion	k1gInSc4	Carpinion
betuli	betule	k1gFnSc4	betule
–	–	k?	–
Hercynské	hercynský	k2eAgFnSc2d1	hercynská
dubohabřiny	dubohabřina	k1gFnSc2	dubohabřina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
průzkum	průzkum	k1gInSc1	průzkum
fauny	fauna	k1gFnSc2	fauna
se	se	k3xPyFc4	se
prováděl	provádět	k5eAaImAgInS	provádět
také	také	k9	také
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Cikánky	cikánka	k1gFnSc2	cikánka
I	I	kA	I
+	+	kIx~	+
II	II	kA	II
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
druhy	druh	k1gInPc1	druh
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
památce	památka	k1gFnSc6	památka
Cikánka	Cikánek	k1gMnSc2	Cikánek
II	II	kA	II
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
blízkosti	blízkost	k1gFnSc6	blízkost
obou	dva	k4xCgNnPc2	dva
území	území	k1gNnPc2	území
pravděpodobné	pravděpodobný	k2eAgFnPc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Cikánka	cikánka	k1gFnSc1	cikánka
II	II	kA	II
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
hojném	hojný	k2eAgInSc6d1	hojný
počtu	počet	k1gInSc6	počet
41	[number]	k4	41
druhů	druh	k1gInPc2	druh
střevlíkovitých	střevlíkovitý	k2eAgMnPc2d1	střevlíkovitý
brouků	brouk	k1gMnPc2	brouk
<g/>
:	:	kIx,	:
Harpalus	Harpalus	k1gMnSc1	Harpalus
caspius	caspius	k1gMnSc1	caspius
roubali	roubat	k5eAaImAgMnP	roubat
<g/>
,	,	kIx,	,
Panagaeus	Panagaeus	k1gMnSc1	Panagaeus
bipustulatus	bipustulatus	k1gMnSc1	bipustulatus
<g/>
,	,	kIx,	,
Ophonus	Ophonus	k1gMnSc1	Ophonus
cordatus	cordatus	k1gMnSc1	cordatus
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
2	[number]	k4	2
druhy	druh	k1gInPc7	druh
měkkýšů	měkkýš	k1gMnPc2	měkkýš
<g/>
:	:	kIx,	:
Granaria	Granarium	k1gNnSc2	Granarium
frumentum	frumentum	k1gNnSc1	frumentum
<g/>
,	,	kIx,	,
Pupilla	Pupilla	k1gFnSc1	Pupilla
sterri	sterr	k1gFnSc2	sterr
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgFnSc3d1	chráněná
prskavce	prskavka	k1gFnSc3	prskavka
<g/>
:	:	kIx,	:
menší	malý	k2eAgInSc1d2	menší
(	(	kIx(	(
<g/>
Brachinus	Brachinus	k1gInSc1	Brachinus
explodens	explodens	k1gInSc1	explodens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
větší	veliký	k2eAgInSc1d2	veliký
(	(	kIx(	(
<g/>
Brachinus	Brachinus	k1gInSc1	Brachinus
crepitans	crepitans	k1gInSc1	crepitans
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
802	[number]	k4	802
druhů	druh	k1gInPc2	druh
motýlů	motýl	k1gMnPc2	motýl
např.	např.	kA	např.
Hamearis	Hamearis	k1gFnSc1	Hamearis
lucina	lucina	k1gFnSc1	lucina
(	(	kIx(	(
<g/>
pestrobarvec	pestrobarvec	k1gInSc1	pestrobarvec
petrklíčový	petrklíčový	k2eAgInSc1d1	petrklíčový
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
zařazené	zařazený	k2eAgInPc4d1	zařazený
na	na	k7c6	na
Červeném	červený	k2eAgInSc6d1	červený
seznamu	seznam	k1gInSc6	seznam
<g/>
:	:	kIx,	:
Hemaris	Hemaris	k1gFnSc1	Hemaris
tityus	tityus	k1gMnSc1	tityus
(	(	kIx(	(
<g/>
dlouhozobka	dlouhozobka	k1gFnSc1	dlouhozobka
chrastavcová	chrastavcový	k2eAgFnSc1d1	chrastavcový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hemaris	Hemaris	k1gFnSc1	Hemaris
fuciformis	fuciformis	k1gFnSc1	fuciformis
(	(	kIx(	(
<g/>
dlouhozobka	dlouhozobka	k1gFnSc1	dlouhozobka
zimolezová	zimolezový	k2eAgFnSc1d1	zimolezová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hyles	Hyles	k1gMnSc1	Hyles
euphorbiae	euphorbia	k1gFnSc2	euphorbia
(	(	kIx(	(
<g/>
lišaj	lišaj	k1gMnSc1	lišaj
pryšcový	pryšcový	k2eAgMnSc1d1	pryšcový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Setina	setina	k1gFnSc1	setina
roscida	roscida	k1gFnSc1	roscida
(	(	kIx(	(
<g/>
lišejníkovec	lišejníkovec	k1gInSc1	lišejníkovec
malý	malý	k2eAgInSc1d1	malý
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Leptidea	Leptide	k2eAgFnSc1d1	Leptide
sinapis	sinapis	k1gFnSc1	sinapis
(	(	kIx(	(
<g/>
bělásek	bělásek	k1gMnSc1	bělásek
hrachorový	hrachorový	k2eAgMnSc1d1	hrachorový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Plebejus	Plebejus	k1gInSc1	Plebejus
idas	idas	k1gInSc1	idas
(	(	kIx(	(
<g/>
modrásek	modrásek	k1gMnSc1	modrásek
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polyommatus	Polyommatus	k1gMnSc1	Polyommatus
daphis	daphis	k1gFnSc2	daphis
(	(	kIx(	(
<g/>
modrásek	modrásek	k1gMnSc1	modrásek
hnědoskvrnný	hnědoskvrnný	k2eAgMnSc1d1	hnědoskvrnný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hipparchia	Hipparchia	k1gFnSc1	Hipparchia
semele	semlít	k5eAaPmIp3nS	semlít
(	(	kIx(	(
<g/>
okáč	okáč	k1gMnSc1	okáč
metlicový	metlicový	k2eAgMnSc1d1	metlicový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pyrgus	Pyrgus	k1gMnSc1	Pyrgus
carthami	cartha	k1gFnPc7	cartha
(	(	kIx(	(
<g/>
soumračník	soumračník	k1gMnSc1	soumračník
proskurníkový	proskurníkový	k2eAgMnSc1d1	proskurníkový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thymelicus	Thymelicus	k1gMnSc1	Thymelicus
acteon	acteon	k1gMnSc1	acteon
(	(	kIx(	(
<g/>
soumračník	soumračník	k1gMnSc1	soumračník
žlutoskvrný	žlutoskvrný	k2eAgMnSc1d1	žlutoskvrný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iphlicides	Iphlicides	k1gMnSc1	Iphlicides
podalirius	podalirius	k1gMnSc1	podalirius
(	(	kIx(	(
<g/>
otakárek	otakárek	k1gMnSc1	otakárek
ovocný	ovocný	k2eAgMnSc1d1	ovocný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
Cikánky	cikánka	k1gFnSc2	cikánka
II	II	kA	II
nebylo	být	k5eNaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
a	a	k8xC	a
pás	pás	k1gInSc1	pás
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
50	[number]	k4	50
m	m	kA	m
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
předmětů	předmět	k1gInPc2	předmět
ochrany	ochrana	k1gFnSc2	ochrana
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
skutečné	skutečný	k2eAgFnSc2d1	skutečná
rozloze	rozloha	k1gFnSc6	rozloha
památky	památka	k1gFnPc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
je	být	k5eAaImIp3nS	být
Cikánka	cikánka	k1gFnSc1	cikánka
II	II	kA	II
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
jako	jako	k8xS	jako
zvláště	zvláště	k6eAd1	zvláště
chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
díky	díky	k7c3	díky
jejímu	její	k3xOp3gInSc3	její
odkrytému	odkrytý	k2eAgInSc3d1	odkrytý
geologickému	geologický	k2eAgInSc3d1	geologický
profilu	profil	k1gInSc3	profil
na	na	k7c4	na
JZ	JZ	kA	JZ
lokality	lokalita	k1gFnSc2	lokalita
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
studovat	studovat	k5eAaImF	studovat
strukturu	struktura	k1gFnSc4	struktura
jejích	její	k3xOp3gFnPc2	její
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
nacházejí	nacházet	k5eAaImIp3nP	nacházet
paleontologické	paleontologický	k2eAgInPc4d1	paleontologický
nálezy	nález	k1gInPc4	nález
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
vzniku	vznik	k1gInSc2	vznik
ochrany	ochrana	k1gFnSc2	ochrana
této	tento	k3xDgFnSc2	tento
památky	památka	k1gFnSc2	památka
je	být	k5eAaImIp3nS	být
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
těžba	těžba	k1gFnSc1	těžba
v	v	k7c6	v
blízkých	blízký	k2eAgInPc6d1	blízký
lomech	lom	k1gInPc6	lom
pro	pro	k7c4	pro
cementářské	cementářský	k2eAgInPc4d1	cementářský
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
nedaleká	daleký	k2eNgFnSc1d1	nedaleká
výstavba	výstavba	k1gFnSc1	výstavba
cementárny	cementárna	k1gFnSc2	cementárna
pro	pro	k7c4	pro
okolní	okolní	k2eAgInPc4d1	okolní
lomy	lom	k1gInPc4	lom
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c4	před
závaly	zával	k1gInPc4	zával
a	a	k8xC	a
výskyt	výskyt	k1gInSc4	výskyt
několika	několik	k4yIc2	několik
vzácně	vzácně	k6eAd1	vzácně
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
negativní	negativní	k2eAgInSc1d1	negativní
vliv	vliv	k1gInSc1	vliv
působící	působící	k2eAgInSc1d1	působící
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
chráněnou	chráněný	k2eAgFnSc4d1	chráněná
oblast	oblast	k1gFnSc4	oblast
mají	mít	k5eAaImIp3nP	mít
zarůstající	zarůstající	k2eAgFnPc4d1	zarůstající
dřeviny	dřevina	k1gFnPc4	dřevina
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
lomů	lom	k1gInPc2	lom
pro	pro	k7c4	pro
porosty	porost	k1gInPc4	porost
cenných	cenný	k2eAgInPc2d1	cenný
xerotermních	xerotermní	k2eAgInPc2d1	xerotermní
trávníků	trávník	k1gInPc2	trávník
a	a	k8xC	a
zahalující	zahalující	k2eAgInPc4d1	zahalující
monocenotické	monocenotický	k2eAgInPc4d1	monocenotický
porosty	porost	k1gInPc4	porost
pod	pod	k7c7	pod
lomovou	lomový	k2eAgFnSc7d1	Lomová
stěnou	stěna	k1gFnSc7	stěna
se	s	k7c7	s
stratigrafickým	stratigrafický	k2eAgInSc7d1	stratigrafický
profilem	profil	k1gInSc7	profil
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
na	na	k7c4	na
léta	léto	k1gNnPc4	léto
2010	[number]	k4	2010
–	–	k?	–
2024	[number]	k4	2024
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Žádné	žádný	k3yNgInPc4	žádný
písemné	písemný	k2eAgInPc4d1	písemný
doklady	doklad	k1gInPc4	doklad
o	o	k7c6	o
dřívějším	dřívější	k2eAgInSc6d1	dřívější
managementu	management	k1gInSc6	management
přírodní	přírodní	k2eAgFnPc1d1	přírodní
památky	památka	k1gFnPc1	památka
nebyly	být	k5eNaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
ani	ani	k9	ani
v	v	k7c6	v
rezervační	rezervační	k2eAgFnSc6d1	rezervační
knize	kniha	k1gFnSc6	kniha
Ústředního	ústřední	k2eAgInSc2d1	ústřední
seznamu	seznam	k1gInSc2	seznam
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
ani	ani	k8xC	ani
ve	v	k7c6	v
výpisu	výpis	k1gInSc6	výpis
z	z	k7c2	z
rezervační	rezervační	k2eAgFnSc2d1	rezervační
knihy	kniha	k1gFnSc2	kniha
na	na	k7c6	na
Magistrátu	magistrát	k1gInSc6	magistrát
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
pruhové	pruhový	k2eAgNnSc1d1	pruhové
značení	značení	k1gNnSc1	značení
i	i	k8xC	i
označení	označení	k1gNnPc4	označení
přírodní	přírodní	k2eAgFnPc4d1	přírodní
památky	památka	k1gFnPc4	památka
cedulemi	cedule	k1gFnPc7	cedule
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
značení	značení	k1gNnSc1	značení
instalováno	instalovat	k5eAaBmNgNnS	instalovat
<g/>
,	,	kIx,	,
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
však	však	k9	však
hranici	hranice	k1gFnSc4	hranice
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
podle	podle	k7c2	podle
vyhlašovacího	vyhlašovací	k2eAgInSc2d1	vyhlašovací
předpisu	předpis	k1gInSc2	předpis
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Podle	podle	k7c2	podle
nynějšího	nynější	k2eAgInSc2d1	nynější
plánu	plán	k1gInSc2	plán
by	by	kYmCp3nS	by
Cikánka	cikánka	k1gFnSc1	cikánka
II	II	kA	II
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
bezzásahová	bezzásahová	k1gFnSc1	bezzásahová
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkami	výjimka	k1gFnPc7	výjimka
občasného	občasný	k2eAgInSc2d1	občasný
výřezu	výřez	k1gInSc2	výřez
dřevin	dřevina	k1gFnPc2	dřevina
u	u	k7c2	u
otevřených	otevřený	k2eAgInPc2d1	otevřený
profilů	profil	k1gInPc2	profil
lomů	lom	k1gInPc2	lom
<g/>
,	,	kIx,	,
odstranění	odstranění	k1gNnSc1	odstranění
případných	případný	k2eAgFnPc2d1	případná
sutí	suť	k1gFnPc2	suť
u	u	k7c2	u
spodních	spodní	k2eAgFnPc2d1	spodní
vrstev	vrstva	k1gFnPc2	vrstva
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
naučné	naučný	k2eAgFnSc2d1	naučná
stezky	stezka	k1gFnSc2	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
návrh	návrh	k1gInSc1	návrh
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
administrativně-správní	administrativněprávní	k2eAgNnPc4d1	administrativně-správní
opatření	opatření	k1gNnPc4	opatření
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
Cikánka	cikánka	k1gFnSc1	cikánka
II	II	kA	II
se	s	k7c7	s
správnou	správný	k2eAgFnSc7d1	správná
lokalizací	lokalizace	k1gFnSc7	lokalizace
předmětů	předmět	k1gInPc2	předmět
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
po	po	k7c6	po
červené	červený	k2eAgFnSc6d1	červená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
od	od	k7c2	od
autobusové	autobusový	k2eAgFnSc2d1	autobusová
zastávky	zastávka	k1gFnSc2	zastávka
Lomy	lom	k1gInPc1	lom
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
asfaltové	asfaltový	k2eAgFnSc6d1	asfaltová
cestě	cesta	k1gFnSc6	cesta
ukončena	ukončit	k5eAaPmNgFnS	ukončit
značkou	značka	k1gFnSc7	značka
zákazu	zákaz	k1gInSc2	zákaz
vstupu	vstup	k1gInSc2	vstup
a	a	k8xC	a
závorami	závora	k1gFnPc7	závora
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
přístupová	přístupový	k2eAgFnSc1d1	přístupová
cesta	cesta	k1gFnSc1	cesta
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
území	území	k1gNnSc4	území
Cikánky	cikánka	k1gFnSc2	cikánka
I.	I.	kA	I.
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
borové	borový	k2eAgFnSc2d1	Borová
špičky	špička	k1gFnSc2	špička
je	být	k5eAaImIp3nS	být
vysekaná	vysekaný	k2eAgFnSc1d1	vysekaná
trasa	trasa	k1gFnSc1	trasa
vedoucí	vedoucí	k1gFnSc2	vedoucí
houštím	houští	k1gNnSc7	houští
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
až	až	k8xS	až
k	k	k7c3	k
údolí	údolí	k1gNnPc1	údolí
do	do	k7c2	do
lomů	lom	k1gInPc2	lom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
naučná	naučný	k2eAgFnSc1d1	naučná
cedule	cedule	k1gFnSc1	cedule
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
znak	znak	k1gInSc1	znak
Přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotografie	fotografia	k1gFnSc2	fotografia
Cikánky	cikánka	k1gFnSc2	cikánka
II	II	kA	II
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cikánka	Cikánek	k1gMnSc2	Cikánek
II	II	kA	II
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
