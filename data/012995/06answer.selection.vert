<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Cikánka	cikánka	k1gFnSc1	cikánka
II	II	kA	II
je	být	k5eAaImIp3nS	být
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
území	území	k1gNnSc4	území
hl.	hl.	k?	hl.
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
