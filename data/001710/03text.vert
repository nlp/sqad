<s>
Diskografie	diskografie	k1gFnSc1	diskografie
americké	americký	k2eAgFnSc2d1	americká
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
devět	devět	k4xCc1	devět
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
dvacet	dvacet	k4xCc4	dvacet
živých	živý	k2eAgFnPc2d1	živá
nahrávek	nahrávka	k1gFnPc2	nahrávka
a	a	k8xC	a
kompilací	kompilace	k1gFnPc2	kompilace
a	a	k8xC	a
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
deset	deset	k4xCc4	deset
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
1967	[number]	k4	1967
-	-	kIx~	-
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
1967	[number]	k4	1967
-	-	kIx~	-
Strange	Strange	k1gFnSc1	Strange
Days	Days	k1gInSc1	Days
1968	[number]	k4	1968
-	-	kIx~	-
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
the	the	k?	the
Sun	Sun	kA	Sun
1969	[number]	k4	1969
-	-	kIx~	-
The	The	k1gFnSc1	The
Soft	Soft	k?	Soft
Parade	Parad	k1gInSc5	Parad
1970	[number]	k4	1970
-	-	kIx~	-
Morrison	Morrison	k1gInSc1	Morrison
Hotel	hotel	k1gInSc1	hotel
1971	[number]	k4	1971
-	-	kIx~	-
L.A.	L.A.	k1gFnSc1	L.A.
<g />
.	.	kIx.	.
</s>
<s>
Woman	Woman	k1gInSc4	Woman
Další	další	k2eAgNnPc4d1	další
alba	album	k1gNnPc4	album
vydaná	vydaný	k2eAgNnPc4d1	vydané
Doors	Doors	k1gInSc4	Doors
byla	být	k5eAaImAgFnS	být
vydaná	vydaný	k2eAgFnSc1d1	vydaná
bez	bez	k7c2	bez
Jima	Jimus	k1gMnSc2	Jimus
Morrisona	Morrison	k1gMnSc2	Morrison
<g/>
:	:	kIx,	:
1971	[number]	k4	1971
-	-	kIx~	-
Other	Other	k1gInSc1	Other
Voices	Voices	k1gInSc1	Voices
1972	[number]	k4	1972
-	-	kIx~	-
Full	Full	k1gInSc1	Full
Circle	Circle	k1gNnSc1	Circle
1978	[number]	k4	1978
-	-	kIx~	-
An	An	k1gMnSc1	An
American	American	k1gMnSc1	American
Prayer	Prayer	k1gMnSc1	Prayer
1970	[number]	k4	1970
-	-	kIx~	-
Absolutely	Absolutela	k1gFnSc2	Absolutela
Live	Liv	k1gInSc2	Liv
1983	[number]	k4	1983
-	-	kIx~	-
Alive	Aliev	k1gFnSc2	Aliev
<g/>
,	,	kIx,	,
She	She	k1gFnSc1	She
Cried	Cried	k1gInSc1	Cried
1985	[number]	k4	1985
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
At	At	k1gFnSc2	At
Hollywood	Hollywood	k1gInSc1	Hollywood
Bowl	Bowl	k1gInSc1	Bowl
1991	[number]	k4	1991
-	-	kIx~	-
In	In	k1gFnSc1	In
Concert	Concert	k1gInSc1	Concert
2001	[number]	k4	2001
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
In	In	k1gFnSc2	In
<g />
.	.	kIx.	.
</s>
<s>
Detroit	Detroit	k1gInSc1	Detroit
2001	[number]	k4	2001
-	-	kIx~	-
Bright	Bright	k2eAgInSc1d1	Bright
Midnight	Midnight	k1gInSc1	Midnight
<g/>
:	:	kIx,	:
Live	Live	k1gNnSc1	Live
in	in	k?	in
America	Americ	k1gInSc2	Americ
2002	[number]	k4	2002
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Hollywood	Hollywood	k1gInSc1	Hollywood
2005	[number]	k4	2005
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
'	'	kIx"	'
<g/>
70	[number]	k4	70
2007	[number]	k4	2007
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Boston	Boston	k1gInSc1	Boston
2008	[number]	k4	2008
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
2009	[number]	k4	2009
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
2010	[number]	k4	2010
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Vancouver	Vancouver	k1gInSc1	Vancouver
1970	[number]	k4	1970
-	-	kIx~	-
13	[number]	k4	13
1972	[number]	k4	1972
-	-	kIx~	-
Weird	Weird	k1gInSc1	Weird
<g />
.	.	kIx.	.
</s>
<s>
Scenes	Scenes	k1gMnSc1	Scenes
Inside	Insid	k1gInSc5	Insid
the	the	k?	the
Gold	Gold	k1gInSc1	Gold
Mine	minout	k5eAaImIp3nS	minout
1973	[number]	k4	1973
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
1980	[number]	k4	1980
-	-	kIx~	-
The	The	k1gFnSc6	The
Doors	Doorsa	k1gFnPc2	Doorsa
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
1985	[number]	k4	1985
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
1991	[number]	k4	1991
-	-	kIx~	-
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
<g/>
:	:	kIx,	:
Original	Original	k1gFnSc1	Original
Soundtrack	soundtrack	k1gInSc4	soundtrack
Recording	Recording	k1gInSc1	Recording
1996	[number]	k4	1996
-	-	kIx~	-
The	The	k1gFnSc6	The
Doors	Doorsa	k1gFnPc2	Doorsa
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
1999	[number]	k4	1999
-	-	kIx~	-
Essential	Essential	k1gInSc1	Essential
Rarities	Rarities	k1gInSc1	Rarities
2000	[number]	k4	2000
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
the	the	k?	the
<g />
.	.	kIx.	.
</s>
<s>
Doors	Doors	k1gInSc1	Doors
2001	[number]	k4	2001
-	-	kIx~	-
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Best	Best	k1gMnSc1	Best
of	of	k?	of
the	the	k?	the
Doors	Doors	k1gInSc1	Doors
2003	[number]	k4	2003
-	-	kIx~	-
Legacy	Legaca	k1gFnSc2	Legaca
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Absolute	Absolut	k1gMnSc5	Absolut
Best	Bestum	k1gNnPc2	Bestum
2007	[number]	k4	2007
-	-	kIx~	-
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Best	Best	k1gMnSc1	Best
of	of	k?	of
the	the	k?	the
Doors	Doors	k1gInSc1	Doors
2008	[number]	k4	2008
-	-	kIx~	-
The	The	k1gMnSc5	The
Future	Futur	k1gMnSc5	Futur
Starts	Starts	k1gInSc1	Starts
Here	Her	k1gInPc1	Her
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Essential	Essential	k1gMnSc1	Essential
Doors	Doors	k1gInSc1	Doors
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
2008	[number]	k4	2008
-	-	kIx~	-
The	The	k1gFnSc1	The
Platinum	Platinum	k1gInSc4	Platinum
Collection	Collection	k1gInSc1	Collection
2010	[number]	k4	2010
-	-	kIx~	-
When	When	k1gInSc1	When
<g />
.	.	kIx.	.
</s>
<s>
You	You	k?	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Strange	Strange	k1gFnSc1	Strange
<g/>
:	:	kIx,	:
Music	Music	k1gMnSc1	Music
from	from	k1gMnSc1	from
the	the	k?	the
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
1997	[number]	k4	1997
-	-	kIx~	-
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
<g/>
:	:	kIx,	:
Box	box	k1gInSc1	box
Set	set	k1gInSc1	set
1999	[number]	k4	1999
-	-	kIx~	-
The	The	k1gFnSc1	The
Complete	Comple	k1gNnSc2	Comple
Studio	studio	k1gNnSc1	studio
Recordings	Recordings	k1gInSc1	Recordings
2003	[number]	k4	2003
-	-	kIx~	-
Boot	Boot	k1gMnSc1	Boot
Yer	Yer	k1gMnSc1	Yer
Butt	Butt	k1gMnSc1	Butt
2006	[number]	k4	2006
-	-	kIx~	-
Perception	Perception	k1gInSc1	Perception
2008	[number]	k4	2008
-	-	kIx~	-
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
<g/>
:	:	kIx,	:
Vinyl	vinyl	k1gInSc1	vinyl
Box	box	k1gInSc1	box
Set	set	k1gInSc1	set
2011	[number]	k4	2011
-	-	kIx~	-
A	a	k8xC	a
Collection	Collection	k1gInSc1	Collection
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
discography	discographa	k1gFnSc2	discographa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
