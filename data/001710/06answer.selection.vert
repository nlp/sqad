<s>
Diskografie	diskografie	k1gFnSc1	diskografie
americké	americký	k2eAgFnSc2d1	americká
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
devět	devět	k4xCc1	devět
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
dvacet	dvacet	k4xCc4	dvacet
živých	živý	k2eAgFnPc2d1	živá
nahrávek	nahrávka	k1gFnPc2	nahrávka
a	a	k8xC	a
kompilací	kompilace	k1gFnPc2	kompilace
a	a	k8xC	a
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
deset	deset	k4xCc4	deset
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
