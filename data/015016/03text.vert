<s>
Islám	islám	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
monoteistickém	monoteistický	k2eAgNnSc6d1
abrahámovském	abrahámovský	k2eAgNnSc6d1
náboženství	náboženství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Islam	Islam	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ka	Ka	k?
<g/>
'	'	kIx"
<g/>
ba	ba	k9
v	v	k7c6
mešitě	mešita	k1gFnSc6
Al-Masdžid	Al-Masdžida	k1gFnPc2
al-Harám	al-Hara	k1gFnPc3
v	v	k7c6
Mekce	Mekka	k1gFnSc6
určuje	určovat	k5eAaImIp3nS
směr	směr	k1gInSc4
modlitby	modlitba	k1gFnSc2
a	a	k8xC
cíl	cíl	k1gInSc1
muslimských	muslimský	k2eAgMnPc2d1
poutníků	poutník	k1gMnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
</s>
<s>
Prorok	prorok	k1gMnSc1
Mohamed	Mohamed	k1gMnSc1
během	během	k7c2
hadždže	hadždž	k1gFnSc2
do	do	k7c2
Mekky	Mekka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
632	#num#	k4
</s>
<s>
Islám	islám	k1gInSc1
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
<g/>
:	:	kIx,
ا	ا	k?
<g/>
;	;	kIx,
al-	al-	k?
<g/>
'	'	kIx"
<g/>
islā	islā	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
monoteistické	monoteistický	k2eAgNnSc1d1
abrahámovské	abrahámovský	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
založené	založený	k2eAgNnSc1d1
na	na	k7c6
učení	učení	k1gNnSc6
proroka	prorok	k1gMnSc2
Mohameda	Mohamed	k1gMnSc2
<g/>
,	,	kIx,
náboženského	náboženský	k2eAgMnSc2d1
a	a	k8xC
politického	politický	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
působícího	působící	k2eAgMnSc2d1
v	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
islám	islám	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
podrobení	podrobení	k1gNnSc1
se	s	k7c7
<g/>
“	“	k?
či	či	k8xC
odevzdání	odevzdání	k1gNnSc2
se	se	k3xPyFc4
Bohu	bůh	k1gMnSc3
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
Alláh	Alláh	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Stoupenec	stoupenec	k1gMnSc1
islámu	islám	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
muslim	muslim	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
podřizuje	podřizovat	k5eAaImIp3nS
[	[	kIx(
<g/>
Bohu	bůh	k1gMnSc3
<g/>
]	]	kIx)
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
muslimů	muslim	k1gMnPc2
je	být	k5eAaImIp3nS
asi	asi	k9
1,8	1,8	k4
až	až	k9
2	#num#	k4
miliardy	miliarda	k4xCgFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
z	z	k7c2
islámu	islám	k1gInSc2
činí	činit	k5eAaImIp3nS
po	po	k7c6
křesťanství	křesťanství	k1gNnSc6
druhé	druhý	k4xOgNnSc1
nejpočetnější	početní	k2eAgNnSc1d3
náboženství	náboženství	k1gNnSc1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
islámu	islám	k1gInSc2
byl	být	k5eAaImAgMnS
na	na	k7c6
začátku	začátek	k1gInSc6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
arabský	arabský	k2eAgMnSc1d1
obchodník	obchodník	k1gMnSc1
Mohamed	Mohamed	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
muslimové	muslim	k1gMnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c2
posledního	poslední	k2eAgMnSc2d1
proroka	prorok	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslimové	muslim	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Bůh	bůh	k1gMnSc1
Mohamedovi	Mohamed	k1gMnSc3
zjevil	zjevit	k5eAaPmAgMnS
Korán	korán	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
společně	společně	k6eAd1
se	s	k7c7
Sunnou	sunna	k1gFnSc7
(	(	kIx(
<g/>
Mohamedovy	Mohamedův	k2eAgInPc1d1
činy	čin	k1gInPc1
a	a	k8xC
slova	slovo	k1gNnPc1
<g/>
)	)	kIx)
považují	považovat	k5eAaImIp3nP
za	za	k7c4
základní	základní	k2eAgInPc4d1
prameny	pramen	k1gInPc4
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Muslimové	muslim	k1gMnPc1
jsou	být	k5eAaImIp3nP
povinni	povinen	k2eAgMnPc1d1
dodržovat	dodržovat	k5eAaImF
pět	pět	k4xCc1
pilířů	pilíř	k1gInPc2
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
pět	pět	k4xCc4
povinností	povinnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
muslimy	muslim	k1gMnPc4
spojují	spojovat	k5eAaImIp3nP
ve	v	k7c4
společenství	společenství	k1gNnSc4
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgNnPc1d1
jak	jak	k8xC,k8xS
pravidla	pravidlo	k1gNnPc1
uctívání	uctívání	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
islámské	islámský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
(	(	kIx(
<g/>
šaría	šaría	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
vyvinulo	vyvinout	k5eAaPmAgNnS
tradici	tradice	k1gFnSc4
různorodých	různorodý	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
dotýkají	dotýkat	k5eAaImIp3nP
prakticky	prakticky	k6eAd1
všech	všecek	k3xTgInPc2
aspektů	aspekt	k1gInPc2
života	život	k1gInSc2
muslima	muslim	k1gMnSc2
i	i	k8xC
celé	celý	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
zahrnuje	zahrnovat	k5eAaImIp3nS
vše	všechen	k3xTgNnSc1
od	od	k7c2
bankovnictví	bankovnictví	k1gNnSc2
<g/>
,	,	kIx,
společenské	společenský	k2eAgFnSc2d1
solidarity	solidarita	k1gFnSc2
<g/>
,	,	kIx,
pravidel	pravidlo	k1gNnPc2
boje	boj	k1gInSc2
(	(	kIx(
<g/>
džihádu	džihád	k1gInSc2
<g/>
)	)	kIx)
či	či	k8xC
postojů	postoj	k1gInPc2
k	k	k7c3
životnímu	životní	k2eAgNnSc3d1
prostředí	prostředí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islám	islám	k1gInSc1
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
kritizován	kritizován	k2eAgInSc1d1
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
násilnou	násilný	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
nevěřícím	nevěřící	k1gMnPc3
a	a	k8xC
omezování	omezování	k1gNnSc4
práv	právo	k1gNnPc2
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Téměř	téměř	k6eAd1
každý	každý	k3xTgMnSc1
muslim	muslim	k1gMnSc1
je	být	k5eAaImIp3nS
příslušník	příslušník	k1gMnSc1
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
dvou	dva	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
islámských	islámský	k2eAgFnPc2d1
větví	větev	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
sunny	sunna	k1gFnPc1
(	(	kIx(
<g/>
75	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
nebo	nebo	k8xC
šíity	šíita	k1gMnSc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdělení	rozdělení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
tomuto	tento	k3xDgNnSc3
rozštěpení	rozštěpení	k1gNnSc4
muslimské	muslimský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
nastalo	nastat	k5eAaPmAgNnS
v	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
příčinou	příčina	k1gFnSc7
byla	být	k5eAaImAgFnS
otázka	otázka	k1gFnSc1
nástupnictví	nástupnictví	k1gNnSc4
ve	v	k7c6
vedení	vedení	k1gNnSc6
muslimské	muslimský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
po	po	k7c6
smrti	smrt	k1gFnSc6
Mohameda	Mohamed	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islám	islám	k1gInSc1
je	být	k5eAaImIp3nS
převládající	převládající	k2eAgNnSc4d1
náboženství	náboženství	k1gNnSc4
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
a	a	k8xC
Středním	střední	k2eAgInSc6d1
východě	východ	k1gInSc6
a	a	k8xC
některých	některý	k3yIgFnPc6
částech	část	k1gFnPc6
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
13	#num#	k4
%	%	kIx~
muslimů	muslim	k1gMnPc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
Indonésii	Indonésie	k1gFnSc6
<g/>
,	,	kIx,
zemi	zem	k1gFnSc6
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
muslimskou	muslimský	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
vůbec	vůbec	k9
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
25	#num#	k4
%	%	kIx~
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
20	#num#	k4
%	%	kIx~
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
,	,	kIx,
resp.	resp.	kA
arabských	arabský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Početné	početný	k2eAgFnPc1d1
muslimské	muslimský	k2eAgFnPc1d1
komunity	komunita	k1gFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
Východní	východní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
muslimů	muslim	k1gMnPc2
také	také	k9
tvoří	tvořit	k5eAaImIp3nP
přistěhovalci	přistěhovalec	k1gMnPc1
v	v	k7c6
různých	různý	k2eAgFnPc6d1
částech	část	k1gFnPc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
islámu	islám	k1gInSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Počátek	počátek	k1gInSc1
a	a	k8xC
rozmach	rozmach	k1gInSc1
</s>
<s>
Expanze	expanze	k1gFnSc1
islámského	islámský	k2eAgInSc2d1
chalífátu	chalífát	k1gInSc2
v	v	k7c6
letech	let	k1gInPc6
622	#num#	k4
<g/>
–	–	k?
<g/>
750	#num#	k4
</s>
<s>
Počátky	počátek	k1gInPc1
islámu	islám	k1gInSc2
jsou	být	k5eAaImIp3nP
spojeny	spojen	k2eAgInPc1d1
s	s	k7c7
působením	působení	k1gNnSc7
arabského	arabský	k2eAgMnSc2d1
proroka	prorok	k1gMnSc2
Mohameda	Mohamed	k1gMnSc2
(	(	kIx(
<g/>
†	†	k?
632	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
Arabském	arabský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčová	klíčový	k2eAgFnSc1d1
událost	událost	k1gFnSc1
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
hidžra	hidžra	k1gFnSc1
roku	rok	k1gInSc2
622	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Mohamed	Mohamed	k1gMnSc1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
přívrženci	přívrženec	k1gMnPc7
přesídlil	přesídlit	k5eAaPmAgMnS
z	z	k7c2
Mekky	Mekka	k1gFnSc2
do	do	k7c2
Medíny	Medína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islám	islám	k1gInSc1
se	se	k3xPyFc4
do	do	k7c2
počátku	počátek	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
rychle	rychle	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
do	do	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
Středomoří	středomoří	k1gNnSc2
díky	díky	k7c3
islámské	islámský	k2eAgFnSc3d1
expanzi	expanze	k1gFnSc3
<g/>
,	,	kIx,
vojenských	vojenský	k2eAgInPc2d1
výbojů	výboj	k1gInPc2
arabských	arabský	k2eAgMnPc2d1
vyznavačů	vyznavač	k1gMnPc2
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
ummy	umma	k1gFnSc2
<g/>
,	,	kIx,
obce	obec	k1gFnSc2
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
po	po	k7c6
smrti	smrt	k1gFnSc6
Mohameda	Mohamed	k1gMnSc2
stanuli	stanout	k5eAaPmAgMnP
chalífové	chalífa	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
zprvu	zprvu	k6eAd1
voleni	volit	k5eAaImNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
zde	zde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozkolu	rozkol	k1gInSc3
islámu	islám	k1gInSc2
na	na	k7c4
přívržence	přívrženec	k1gMnPc4
chalífů	chalífa	k1gMnPc2
pokrevně	pokrevně	k6eAd1
spřízněných	spřízněný	k2eAgMnPc2d1
s	s	k7c7
prorokem	prorok	k1gMnSc7
(	(	kIx(
<g/>
šíité	šíita	k1gMnPc5
<g/>
)	)	kIx)
a	a	k8xC
většinové	většinový	k2eAgMnPc4d1
sunnity	sunnita	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
v	v	k7c6
čele	čelo	k1gNnSc6
chalífátu	chalífát	k1gInSc2
stanuli	stanout	k5eAaPmAgMnP
členové	člen	k1gMnPc1
rodu	rod	k1gInSc2
Umajjovců	Umajjovec	k1gMnPc2
(	(	kIx(
<g/>
661	#num#	k4
<g/>
–	–	k?
<g/>
750	#num#	k4
<g/>
)	)	kIx)
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Damašku	Damašek	k1gInSc2
a	a	k8xC
Abbásovců	Abbásovec	k1gInPc2
(	(	kIx(
<g/>
750	#num#	k4
<g/>
–	–	k?
<g/>
1258	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
přesídlili	přesídlit	k5eAaPmAgMnP
do	do	k7c2
Bagdádu	Bagdád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstředivé	odstředivý	k2eAgFnPc4d1
tendence	tendence	k1gFnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
islámské	islámský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
se	se	k3xPyFc4
také	také	k9
projevily	projevit	k5eAaPmAgInP
například	například	k6eAd1
vyhlášením	vyhlášení	k1gNnSc7
samostatného	samostatný	k2eAgInSc2d1
córdobského	córdobský	k2eAgInSc2d1
chalífátu	chalífát	k1gInSc2
na	na	k7c6
Pyrenejském	pyrenejský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
nebo	nebo	k8xC
chalífátu	chalífát	k1gInSc2
fátimovského	fátimovský	k2eAgInSc2d1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
(	(	kIx(
<g/>
Maghreb	Maghrba	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
uspořádání	uspořádání	k1gNnSc2
muslimského	muslimský	k2eAgInSc2d1
světa	svět	k1gInSc2
dále	daleko	k6eAd2
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
zasáhl	zasáhnout	k5eAaPmAgMnS
turkický	turkický	k2eAgInSc4d1
rod	rod	k1gInSc4
Seldžuků	Seldžuk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
prosadili	prosadit	k5eAaPmAgMnP
nejvyššího	vysoký	k2eAgMnSc4d3
světského	světský	k2eAgMnSc4d1
panovníka	panovník	k1gMnSc4
<g/>
,	,	kIx,
sultána	sultána	k1gFnSc1
(	(	kIx(
<g/>
obdobu	obdoba	k1gFnSc4
křesťanského	křesťanský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pravomoci	pravomoc	k1gFnPc1
chalífy	chalífa	k1gMnSc2
byly	být	k5eAaImAgFnP
omezeny	omezit	k5eAaPmNgFnP
na	na	k7c4
oblast	oblast	k1gFnSc4
duchovní	duchovní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Rozkol	rozkol	k1gInSc1
</s>
<s>
V	v	k7c6
původně	původně	k6eAd1
jednotné	jednotný	k2eAgFnSc6d1
obci	obec	k1gFnSc6
věřících	věřící	k1gMnPc2
však	však	k9
již	již	k6eAd1
v	v	k7c6
polovině	polovina	k1gFnSc6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nastal	nastat	k5eAaPmAgInS
rozkol	rozkol	k1gInSc1
v	v	k7c6
otázce	otázka	k1gFnSc6
nástupnictví	nástupnictví	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
pojetí	pojetí	k1gNnSc6
nejvyšší	vysoký	k2eAgFnSc2d3
nábožensko-politické	nábožensko-politický	k2eAgFnSc2d1
hodnosti	hodnost	k1gFnSc2
imáma	imám	k1gMnSc2
a	a	k8xC
chalífy	chalífa	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
nazvaná	nazvaný	k2eAgFnSc1d1
sunnité	sunnita	k1gMnPc5
(	(	kIx(
<g/>
sunna	sunna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uznává	uznávat	k5eAaImIp3nS
právní	právní	k2eAgInSc1d1
moci	moc	k1gFnSc3
volby	volba	k1gFnSc2
prvních	první	k4xOgInPc2
čtyř	čtyři	k4xCgInPc2
chalífů	chalífa	k1gMnPc2
(	(	kIx(
<g/>
Abú	abú	k1gMnSc2
Bakra	Bakr	k1gMnSc2
<g/>
,	,	kIx,
Umara	Umar	k1gMnSc2
<g/>
,	,	kIx,
Uthmána	Uthmán	k1gMnSc2
a	a	k8xC
Alího	Alí	k1gMnSc2
<g/>
)	)	kIx)
i	i	k9
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
za	za	k7c2
Umajjovců	Umajjovec	k1gInPc2
a	a	k8xC
Abbásovců	Abbásovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
nich	on	k3xPp3gMnPc2
má	mít	k5eAaImIp3nS
v	v	k7c6
čele	čelo	k1gNnSc6
obce	obec	k1gFnSc2
stát	stát	k5eAaPmF,k5eAaImF
chalífa	chalífa	k1gMnSc1
volený	volený	k2eAgMnSc1d1
předními	přední	k2eAgFnPc7d1
muži	muž	k1gMnSc3
z	z	k7c2
řad	řada	k1gFnPc2
mekkánského	mekkánský	k2eAgInSc2d1
vládnoucího	vládnoucí	k2eAgInSc2d1
rodu	rod	k1gInSc2
Kurajšá	Kurajšá	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
nim	on	k3xPp3gFnPc3
se	se	k3xPyFc4
postavili	postavit	k5eAaPmAgMnP
stoupenci	stoupenec	k1gMnPc7
Alího	Alíha	k1gFnSc5
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
nazývaný	nazývaný	k2eAgInSc1d1
ší	ší	k?
<g/>
'	'	kIx"
<g/>
at	at	k?
Alí	Alí	k1gFnSc1
-	-	kIx~
strana	strana	k1gFnSc1
Alího	Alí	k1gMnSc2
(	(	kIx(
<g/>
ší	ší	k?
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
kterých	který	k3yIgFnPc2,k3yRgFnPc2,k3yQgFnPc2
funkce	funkce	k1gFnSc1
imáma	imám	k1gMnSc2
přísluší	příslušet	k5eAaImIp3nS
pouze	pouze	k6eAd1
prorokově	prorokův	k2eAgFnSc3d1
rodině	rodina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc4
frakci	frakce	k1gFnSc4
stanovila	stanovit	k5eAaPmAgFnS
cháridža	cháridža	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
imámem	imám	k1gMnSc7
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
zvolen	zvolit	k5eAaPmNgMnS
vždy	vždy	k6eAd1
nejzbožnější	zbožní	k2eAgMnSc1d3
muslim	muslim	k1gMnSc1
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
původ	původ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
sunnité	sunnita	k1gMnPc1
asi	asi	k9
90	#num#	k4
<g/>
%	%	kIx~
všech	všecek	k3xTgMnPc2
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
ší	ší	k?
<g/>
'	'	kIx"
<g/>
iti	iti	k?
(	(	kIx(
<g/>
včetně	včetně	k7c2
různých	různý	k2eAgFnPc2d1
odnoží	odnož	k1gFnPc2
<g/>
)	)	kIx)
asi	asi	k9
10	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
cháridžovci	cháridžovec	k1gMnPc1
tvoří	tvořit	k5eAaImIp3nP
pouze	pouze	k6eAd1
několik	několik	k4yIc4
menších	malý	k2eAgFnPc2d2
enkláv	enkláva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sunna	sunna	k1gFnSc1
</s>
<s>
Sahíh	Sahíh	k1gMnSc1
al-Bucharí	al-Bucharí	k1gMnSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
šesti	šest	k4xCc2
sunnitských	sunnitský	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
(	(	kIx(
<g/>
Hadísů	Hadís	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
základním	základní	k2eAgInSc6d1
schizmatickém	schizmatický	k2eAgInSc6d1
rozkolu	rozkol	k1gInSc6
obce	obec	k1gFnSc2
věřících	věřící	k1gMnPc2
v	v	k7c6
otázce	otázka	k1gFnSc6
nástupnictví	nástupnictví	k1gNnSc2
se	se	k3xPyFc4
za	za	k7c7
sunnity	sunnita	k1gMnPc7
považovali	považovat	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nesdíleli	sdílet	k5eNaImAgMnP
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
vedení	vedení	k1gNnSc1
obce	obec	k1gFnSc2
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
zůstat	zůstat	k5eAaPmF
v	v	k7c6
rukou	ruka	k1gFnPc6
přímých	přímý	k2eAgMnPc2d1
potomků	potomek	k1gMnPc2
Mohameda	Mohamed	k1gMnSc2
v	v	k7c6
rodové	rodový	k2eAgFnSc6d1
linii	linie	k1gFnSc6
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
zetě	zeť	k1gMnSc2
Alího	Alí	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
manželky	manželka	k1gFnSc2
Fatimy	Fatima	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgFnPc6
podle	podle	k7c2
ší	ší	k?
<g/>
'	'	kIx"
<g/>
y	y	k?
prochází	procházet	k5eAaImIp3nS
Prorokovo	prorokův	k2eAgNnSc1d1
božské	božský	k2eAgNnSc1d1
posvěcení	posvěcení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sunnité	sunnita	k1gMnPc1
z	z	k7c2
principu	princip	k1gInSc2
obhajovali	obhajovat	k5eAaImAgMnP
zachování	zachování	k1gNnSc3
nástupnictví	nástupnictví	k1gNnSc2
v	v	k7c6
rodě	rod	k1gInSc6
Kurajšá	Kurajšá	k1gFnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
661	#num#	k4
přijali	přijmout	k5eAaPmAgMnP
jako	jako	k8xS,k8xC
fakt	fakt	k1gInSc4
uchvácení	uchvácení	k1gNnSc2
politické	politický	k2eAgFnSc2d1
moci	moc	k1gFnSc2
rodem	rod	k1gInSc7
Umajjovců	Umajjovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sunna	sunna	k1gFnSc1
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
v	v	k7c4
teologicko-ideologický	teologicko-ideologický	k2eAgInSc4d1
model	model	k1gInSc4
v	v	k7c6
odborné	odborný	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
<g/>
,	,	kIx,
nazývaný	nazývaný	k2eAgInSc1d1
také	také	k9
islámská	islámský	k2eAgFnSc1d1
ortodoxie	ortodoxie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sunna	sunna	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
konformistický	konformistický	k2eAgInSc1d1
státotvorný	státotvorný	k2eAgInSc1d1
a	a	k8xC
zákonodárný	zákonodárný	k2eAgInSc1d1
proud	proud	k1gInSc1
v	v	k7c6
islámu	islám	k1gInSc6
<g/>
,	,	kIx,
převažující	převažující	k2eAgFnSc6d1
na	na	k7c6
většině	většina	k1gFnSc6
území	území	k1gNnSc2
Dár	Dár	k1gFnSc2
al-islám	al-islat	k5eAaImIp1nS,k5eAaPmIp1nS
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
opozičních	opoziční	k2eAgInPc2d1
disidentských	disidentský	k2eAgInPc2d1
proudů	proud	k1gInPc2
a	a	k8xC
nežádoucích	žádoucí	k2eNgInPc2d1
jevů	jev	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ší	Ší	k?
<g/>
'	'	kIx"
<g/>
a	a	k8xC
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
druhý	druhý	k4xOgInSc4
nejvýznamnější	významný	k2eAgInSc4d3
směr	směr	k1gInSc4
v	v	k7c6
islámu	islám	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
během	během	k7c2
vlády	vláda	k1gFnSc2
třetího	třetí	k4xOgNnSc2
chalífy	chalífa	k1gMnSc2
Uthmána	Uthmán	k1gMnSc2
(	(	kIx(
<g/>
644	#num#	k4
<g/>
-	-	kIx~
<g/>
656	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
jako	jako	k9
reakce	reakce	k1gFnPc1
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
slabost	slabost	k1gFnSc4
a	a	k8xC
neúměrný	úměrný	k2eNgInSc4d1
vzestup	vzestup	k1gInSc4
členů	člen	k1gMnPc2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
Umajjovského	umajjovský	k2eAgInSc2d1
rodu	rod	k1gInSc2
ozývaly	ozývat	k5eAaImAgInP
hlasy	hlas	k1gInPc1
volající	volající	k2eAgInPc1d1
po	po	k7c6
nápravě	náprava	k1gFnSc6
a	a	k8xC
uplatnění	uplatnění	k1gNnSc4
práv	právo	k1gNnPc2
členy	člen	k1gMnPc7
Prorokovy	prorokův	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vražda	vražda	k1gFnSc1
Uthmána	Uthmán	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
prvním	první	k4xOgInSc7
velkým	velký	k2eAgInSc7d1
otřesem	otřes	k1gInSc7
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
spory	spor	k1gInPc4
dále	daleko	k6eAd2
vyhrotil	vyhrotit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alí	Alí	k1gMnSc1
nedokázal	dokázat	k5eNaPmAgMnS
využít	využít	k5eAaPmF
příznivé	příznivý	k2eAgFnPc4d1
situace	situace	k1gFnPc4
a	a	k8xC
podlehl	podlehnout	k5eAaPmAgMnS
rozhodnosti	rozhodnost	k1gFnSc2
pozdějšího	pozdní	k2eAgMnSc2d2
Umajjovského	umajjovský	k2eAgMnSc2d1
chalífy	chalífa	k1gMnSc2
Muawíje	Muawíj	k1gMnSc2
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
rukou	ruka	k1gFnSc7
stoupence	stoupenec	k1gMnSc2
cháridža	cháridžus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
jeho	jeho	k3xOp3gNnSc4
synové	syn	k1gMnPc1
Hasan	Hasan	k1gMnSc1
a	a	k8xC
Husajn	Husajn	k1gMnSc1
byli	být	k5eAaImAgMnP
odstraněni	odstraněn	k2eAgMnPc1d1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
úplatky	úplatek	k1gInPc7
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
vraždou	vražda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavení	postavení	k1gNnSc1
ší	ší	k?
<g/>
'	'	kIx"
<g/>
y	y	k?
však	však	k9
obecně	obecně	k6eAd1
zůstalo	zůstat	k5eAaPmAgNnS
poznamenáno	poznamenat	k5eAaPmNgNnS
okolnostmi	okolnost	k1gFnPc7
počátku	počátek	k1gInSc2
hnutí	hnutí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mocenskou	mocenský	k2eAgFnSc7d1
i	i	k8xC
početní	početní	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
získala	získat	k5eAaPmAgFnS
sunna	sunna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ší	Ší	k1gFnSc1
<g/>
'	'	kIx"
<g/>
a	a	k8xC
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
rozvíjela	rozvíjet	k5eAaImAgFnS
bez	bez	k7c2
opory	opora	k1gFnSc2
státní	státní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
svých	svůj	k3xOyFgFnPc6
odnožích	odnož	k1gFnPc6
považována	považován	k2eAgFnSc1d1
za	za	k7c4
schizmatickou	schizmatický	k2eAgFnSc4d1
<g/>
,	,	kIx,
heretickou	heretický	k2eAgFnSc4d1
a	a	k8xC
nežádoucí	žádoucí	k2eNgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnSc1
vyznavači	vyznavač	k1gMnPc1
byli	být	k5eAaImAgMnP
pronásledováni	pronásledovat	k5eAaImNgMnP
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ještě	ještě	k6eAd1
více	hodně	k6eAd2
upevňuje	upevňovat	k5eAaImIp3nS
víru	víra	k1gFnSc4
ve	v	k7c4
vlastní	vlastní	k2eAgFnSc4d1
pravdu	pravda	k1gFnSc4
a	a	k8xC
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
pěstování	pěstování	k1gNnSc3
odolnosti	odolnost	k1gFnSc2
vůči	vůči	k7c3
všem	všecek	k3xTgNnPc3
protivenstvím	protivenství	k1gNnPc3
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
ke	k	k7c3
specifickým	specifický	k2eAgFnPc3d1
formám	forma	k1gFnPc3
boje	boj	k1gInSc2
<g/>
,	,	kIx,
jindy	jindy	k6eAd1
k	k	k7c3
pasivitě	pasivita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
má	mít	k5eAaImIp3nS
několik	několik	k4yIc1
odnoží	odnož	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
utvářely	utvářet	k5eAaImAgFnP
po	po	k7c6
odklonu	odklon	k1gInSc6
od	od	k7c2
většinového	většinový	k2eAgInSc2d1
směru	směr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cháridža	Cháridža	k6eAd1
</s>
<s>
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
opustili	opustit	k5eAaPmAgMnP
Alího	Alí	k1gMnSc4
tábor	tábor	k1gInSc4
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Siffínu	Siffín	k1gInSc2
(	(	kIx(
<g/>
657	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
dali	dát	k5eAaPmAgMnP
tak	tak	k6eAd1
podnět	podnět	k1gInSc4
k	k	k7c3
označení	označení	k1gNnSc3
svého	svůj	k3xOyFgNnSc2
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjadřovali	vyjadřovat	k5eAaImAgMnP
nespokojenost	nespokojenost	k1gFnSc4
s	s	k7c7
očividně	očividně	k6eAd1
světskou	světský	k2eAgFnSc7d1
povahou	povaha	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
nabyl	nabýt	k5eAaPmAgInS
boj	boj	k1gInSc1
o	o	k7c4
moc	moc	k1gFnSc4
v	v	k7c6
muslimské	muslimský	k2eAgFnSc6d1
obci	obec	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůdce	vůdce	k1gMnSc1
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
imám	imám	k1gMnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
podle	podle	k7c2
jejich	jejich	k3xOp3gInSc2
názoru	názor	k1gInSc2
být	být	k5eAaImF
volen	volit	k5eAaImNgMnS
výhradně	výhradně	k6eAd1
podle	podle	k7c2
náboženských	náboženský	k2eAgFnPc2d1
a	a	k8xC
morálních	morální	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
původ	původ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
si	se	k3xPyFc3
neplní	plnit	k5eNaImIp3nS
své	svůj	k3xOyFgFnPc4
povinnosti	povinnost	k1gFnPc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
sesazen	sesadit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
hnutí	hnutí	k1gNnSc1
přitažlivé	přitažlivý	k2eAgNnSc1d1
pro	pro	k7c4
utlačované	utlačovaný	k1gMnPc4
společenské	společenský	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
se	se	k3xPyFc4
sice	sice	k8xC
početně	početně	k6eAd1
rozšířilo	rozšířit	k5eAaPmAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
prvních	první	k4xOgNnPc6
stoletích	století	k1gNnPc6
prošlo	projít	k5eAaPmAgNnS
řadou	řada	k1gFnSc7
krvavých	krvavý	k2eAgFnPc2d1
srážek	srážka	k1gFnPc2
a	a	k8xC
rozštěpilo	rozštěpit	k5eAaPmAgNnS
se	se	k3xPyFc4
do	do	k7c2
několika	několik	k4yIc2
sekt	sekta	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
pak	pak	k6eAd1
v	v	k7c6
dalších	další	k2eAgNnPc6d1
stoletích	století	k1gNnPc6
hrály	hrát	k5eAaImAgFnP
spíše	spíše	k9
okrajovou	okrajový	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Středověk	středověk	k1gInSc1
</s>
<s>
Tamerlán	Tamerlán	k1gInSc1
vítězí	vítězit	k5eAaImIp3nS
v	v	k7c6
bitvě	bitva	k1gFnSc6
o	o	k7c6
Dillí	Dillí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
jeho	jeho	k3xOp3gInPc6
výbojích	výboj	k1gInPc6
a	a	k8xC
válkách	válka	k1gFnPc6
proti	proti	k7c3
nevěřícím	nevěřící	k1gFnPc3
zahynuly	zahynout	k5eAaPmAgFnP
miliony	milion	k4xCgInPc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Maurští	maurský	k2eAgMnPc1d1
poddaní	poddaný	k1gMnPc1
krále	král	k1gMnSc2
Jakuba	Jakub	k1gMnSc2
I.	I.	kA
Aragonského	aragonský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potomci	potomek	k1gMnPc1
Maurů	Maur	k1gMnPc2
Moriskové	Moriskový	k2eAgFnPc1d1
byli	být	k5eAaImAgMnP
vyhnáni	vyhnat	k5eAaPmNgMnP
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
se	se	k3xPyFc4
islámský	islámský	k2eAgInSc4d1
svět	svět	k1gInSc4
již	již	k6eAd1
nevyznačoval	vyznačovat	k5eNaImAgMnS
expanzivní	expanzivní	k2eAgFnSc7d1
dynamikou	dynamika	k1gFnSc7
a	a	k8xC
formovaly	formovat	k5eAaImAgFnP
jej	on	k3xPp3gMnSc4
rozhodujícím	rozhodující	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
nearabské	arabský	k2eNgInPc4d1
vlivy	vliv	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západním	západní	k2eAgNnSc6d1
Středomoří	středomoří	k1gNnSc6
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Córdobského	Córdobský	k2eAgInSc2d1
chalífátu	chalífát	k1gInSc2
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
postupné	postupný	k2eAgNnSc1d1
dobývání	dobývání	k1gNnSc1
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
křesťanskými	křesťanský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
(	(	kIx(
<g/>
jimi	on	k3xPp3gMnPc7
nazývaná	nazývaný	k2eAgFnSc1d1
reconquista	reconquist	k1gMnSc2
<g/>
,	,	kIx,
znovudobývání	znovudobývání	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
nezabránilo	zabránit	k5eNaPmAgNnS
ani	ani	k8xC
sjednocení	sjednocení	k1gNnSc1
oblasti	oblast	k1gFnSc2
pod	pod	k7c7
vládou	vláda	k1gFnSc7
berberských	berberský	k2eAgInPc2d1
Almorávidů	Almorávid	k1gInPc2
a	a	k8xC
Almohadů	Almohad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Egyptě	Egypt	k1gInSc6
<g/>
,	,	kIx,
Sýrii	Sýrie	k1gFnSc6
a	a	k8xC
přilehlých	přilehlý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
po	po	k7c6
Fátimovcích	Fátimovec	k1gInPc6
převzal	převzít	k5eAaPmAgMnS
vládu	vláda	k1gFnSc4
kurdský	kurdský	k2eAgMnSc1d1
Saladin	Saladin	k2eAgMnSc1d1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
dynastie	dynastie	k1gFnSc2
Ajjúbovců	Ajjúbovec	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
v	v	k7c6
polovině	polovina	k1gFnSc6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vystřídal	vystřídat	k5eAaPmAgMnS
Mamlúcký	mamlúcký	k2eAgInSc4d1
sultanát	sultanát	k1gInSc4
s	s	k7c7
vládci	vládce	k1gMnPc7
z	z	k7c2
kasty	kasta	k1gFnSc2
elitních	elitní	k2eAgMnPc2d1
otroků	otrok	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
vývoje	vývoj	k1gInSc2
východního	východní	k2eAgNnSc2d1
Středomoří	středomoří	k1gNnSc2
také	také	k9
zasáhly	zasáhnout	k5eAaPmAgFnP
křížové	křížový	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
<g/>
,	,	kIx,
dvě	dva	k4xCgNnPc4
století	století	k1gNnPc2
trvající	trvající	k2eAgFnSc1d1
snaha	snaha	k1gFnSc1
křesťanských	křesťanský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
o	o	k7c4
získání	získání	k1gNnSc4
území	území	k1gNnSc2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
islámského	islámský	k2eAgInSc2d1
světa	svět	k1gInSc2
hráli	hrát	k5eAaImAgMnP
nejvýznamnější	významný	k2eAgFnSc4d3
roli	role	k1gFnSc4
turkičtí	turkický	k2eAgMnPc1d1
vládci	vládce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
konce	konec	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
Seldžucká	Seldžucký	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
expandovala	expandovat	k5eAaImAgFnS
na	na	k7c4
úkor	úkor	k1gInSc4
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
dočasně	dočasně	k6eAd1
také	také	k9
například	například	k6eAd1
východněji	východně	k6eAd2
položená	položený	k2eAgFnSc1d1
Chórezmská	Chórezmský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
konce	konec	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
začali	začít	k5eAaPmAgMnP
prosazovat	prosazovat	k5eAaImF
Osmané	Osman	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
expandovali	expandovat	k5eAaImAgMnP
až	až	k6eAd1
na	na	k7c4
Balkán	Balkán	k1gInSc4
a	a	k8xC
roku	rok	k1gInSc2
1453	#num#	k4
dobyli	dobýt	k5eAaPmAgMnP
Konstantinopol	Konstantinopol	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
nejen	nejen	k6eAd1
zanikla	zaniknout	k5eAaPmAgFnS
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
se	se	k3xPyFc4
islám	islám	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
na	na	k7c4
práh	práh	k1gInSc4
Evropy	Evropa	k1gFnSc2
a	a	k8xC
západokřesťanského	západokřesťanský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
této	tento	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
také	také	k9
dočasně	dočasně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
velmi	velmi	k6eAd1
výrazně	výrazně	k6eAd1
zasáhly	zasáhnout	k5eAaPmAgInP
výboje	výboj	k1gInPc1
mongolských	mongolský	k2eAgMnPc2d1
dobyvatelů	dobyvatel	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
Čingischán	Čingischán	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vyvrátil	vyvrátit	k5eAaPmAgMnS
Chórezmskou	Chórezmský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
a	a	k8xC
islám	islám	k1gInSc4
vyznávající	vyznávající	k2eAgInSc1d1
Tamerlán	Tamerlán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
na	na	k7c6
začátku	začátek	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
dočasně	dočasně	k6eAd1
oslabil	oslabit	k5eAaPmAgInS
Osmanskou	osmanský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Novověk	novověk	k1gInSc1
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
rozsáhlé	rozsáhlý	k2eAgFnPc1d1
islámské	islámský	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
dostaly	dostat	k5eAaPmAgFnP
pod	pod	k7c4
správu	správa	k1gFnSc4
velkých	velký	k2eAgFnPc2d1
říší	říš	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
východního	východní	k2eAgNnSc2d1
Středomoří	středomoří	k1gNnSc2
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
Persie	Persie	k1gFnSc2
říše	říš	k1gFnSc2
Safíovských	Safíovský	k2eAgMnPc2d1
šáhů	šáh	k1gMnPc2
a	a	k8xC
na	na	k7c6
indickém	indický	k2eAgInSc6d1
subkontinentu	subkontinent	k1gInSc6
Mughalská	Mughalský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
některé	některý	k3yIgFnPc1
oblasti	oblast	k1gFnPc1
dostávaly	dostávat	k5eAaImAgFnP
pod	pod	k7c4
správu	správa	k1gFnSc4
koloniálních	koloniální	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
včetně	včetně	k7c2
oblasti	oblast	k1gFnSc2
Blízkého	blízký	k2eAgInSc2d1
východu	východ	k1gInSc2
po	po	k7c6
rozpadu	rozpad	k1gInSc6
osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Věřící	věřící	k1gFnSc1
při	při	k7c6
modlitbě	modlitba	k1gFnSc6
v	v	k7c6
Somálsku	Somálsko	k1gNnSc6
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
</s>
<s>
Ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
počet	počet	k1gInSc4
muslimů	muslim	k1gMnPc2
stoupl	stoupnout	k5eAaPmAgMnS
z	z	k7c2
200	#num#	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
na	na	k7c4
551	#num#	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
počátku	počátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
populace	populace	k1gFnSc2
muslimů	muslim	k1gMnPc2
dosáhla	dosáhnout	k5eAaPmAgFnS
přibližně	přibližně	k6eAd1
1,6	1,6	k4
miliardy	miliarda	k4xCgFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demografická	demografický	k2eAgFnSc1d1
studie	studie	k1gFnSc1
agentury	agentura	k1gFnSc2
Pew	Pew	k1gMnPc1
Research	Researcha	k1gFnPc2
Center	centrum	k1gNnPc2
předpovídá	předpovídat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2050	#num#	k4
vzroste	vzrůst	k5eAaPmIp3nS
počet	počet	k1gInSc1
muslimů	muslim	k1gMnPc2
na	na	k7c4
2,8	2,8	k4
miliardy	miliarda	k4xCgFnSc2
a	a	k8xC
budou	být	k5eAaImBp3nP
tvořit	tvořit	k5eAaImF
přibližně	přibližně	k6eAd1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
světové	světový	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Islám	islám	k1gInSc1
je	být	k5eAaImIp3nS
nejrychleji	rychle	k6eAd3
rostoucím	rostoucí	k2eAgNnSc7d1
náboženstvím	náboženství	k1gNnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
muslimové	muslim	k1gMnPc1
tvoří	tvořit	k5eAaImIp3nP
okolo	okolo	k7c2
25	#num#	k4
<g/>
%	%	kIx~
obyvatel	obyvatel	k1gMnSc1
v	v	k7c4
Marseilles	Marseilles	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
Rotterdamu	Rotterdam	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Věrouka	věrouka	k1gFnSc1
</s>
<s>
Mohamed	Mohamed	k1gMnSc1
vede	vést	k5eAaImIp3nS
Abraháma	Abrahám	k1gMnSc2
<g/>
,	,	kIx,
Mojžíše	Mojžíš	k1gMnSc2
a	a	k8xC
Ježíše	Ježíš	k1gMnSc2
</s>
<s>
Muslimové	muslim	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
súry	súra	k1gFnPc4
Koránu	korán	k1gInSc2
diktoval	diktovat	k5eAaImAgMnS
Mohamedovi	Mohamed	k1gMnSc3
archanděl	archanděl	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
islámská	islámský	k2eAgFnSc1d1
věrouka	věrouka	k1gFnSc1
klade	klást	k5eAaImIp3nS
důraz	důraz	k1gInSc4
na	na	k7c4
pevný	pevný	k2eAgInSc4d1
životní	životní	k2eAgInSc4d1
řád	řád	k1gInSc4
<g/>
,	,	kIx,
vyjádřený	vyjádřený	k2eAgInSc4d1
přesně	přesně	k6eAd1
formulovanými	formulovaný	k2eAgFnPc7d1
představami	představa	k1gFnPc7
o	o	k7c6
světě	svět	k1gInSc6
a	a	k8xC
člověku	člověk	k1gMnSc3
a	a	k8xC
závazným	závazný	k2eAgInSc7d1
systémem	systém	k1gInSc7
právních	právní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
k	k	k7c3
Bohu	bůh	k1gMnSc3
a	a	k8xC
norem	norma	k1gFnPc2
interakce	interakce	k1gFnSc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
(	(	kIx(
<g/>
šaría	šaría	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propůjčuje	propůjčovat	k5eAaImIp3nS
relativně	relativně	k6eAd1
shodné	shodný	k2eAgInPc4d1
kulturní	kulturní	k2eAgInPc4d1
rysy	rys	k1gInPc4
většinově	většinově	k6eAd1
náboženským	náboženský	k2eAgFnPc3d1
společnostem	společnost	k1gFnPc3
i	i	k8xC
menšinovým	menšinový	k2eAgFnPc3d1
komunitám	komunita	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Islámská	islámský	k2eAgFnSc1d1
věrouka	věrouka	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
pěti	pět	k4xCc6
základních	základní	k2eAgInPc6d1
pilířích	pilíř	k1gInPc6
<g/>
:	:	kIx,
nauka	nauka	k1gFnSc1
o	o	k7c6
jedinosti	jedinost	k1gFnSc6
boží	božit	k5eAaImIp3nS
(	(	kIx(
<g/>
tauhíd	tauhíd	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nauka	nauka	k1gFnSc1
o	o	k7c6
prorocích	prorok	k1gMnPc6
(	(	kIx(
<g/>
anbijá	anbijé	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
nauka	nauka	k1gFnSc1
o	o	k7c6
andělech	anděl	k1gMnPc6
(	(	kIx(
<g/>
malá	malý	k2eAgFnSc1d1
<g/>
'	'	kIx"
<g/>
ika	ika	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nauka	nauka	k1gFnSc1
o	o	k7c6
soudném	soudný	k2eAgInSc6d1
dni	den	k1gInSc6
(	(	kIx(
<g/>
Jaume	Jaum	k1gInSc5
al-kijáma	al-kijám	k1gMnSc4
<g/>
)	)	kIx)
a	a	k8xC
trestu	trest	k1gInSc2
za	za	k7c4
hříchy	hřích	k1gInPc4
a	a	k8xC
nauka	nauka	k1gFnSc1
o	o	k7c6
predestinaci	predestinace	k1gFnSc6
(	(	kIx(
<g/>
al-káď	al-kádit	k5eAaPmRp2nS
wa	wa	k?
<g/>
'	'	kIx"
<g/>
l-kadar	l-kadar	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Islámská	islámský	k2eAgFnSc1d1
kosmologie	kosmologie	k1gFnSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
učení	učení	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Bůh	bůh	k1gMnSc1
stvořil	stvořit	k5eAaPmAgMnS
svět	svět	k1gInSc4
a	a	k8xC
člověka	člověk	k1gMnSc2
a	a	k8xC
zvěstoval	zvěstovat	k5eAaImAgMnS
lidem	člověk	k1gMnPc3
svou	svůj	k3xOyFgFnSc4
vůli	vůle	k1gFnSc4
prostřednictvím	prostřednictvím	k7c2
proroků	prorok	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
těm	ten	k3xDgInPc3
počítá	počítat	k5eAaImIp3nS
velké	velký	k2eAgFnSc2d1
postavy	postava	k1gFnSc2
Starého	Starého	k2eAgInSc2d1
i	i	k8xC
Nového	Nového	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
:	:	kIx,
významný	významný	k2eAgMnSc1d1
jsou	být	k5eAaImIp3nP
Núh	Núh	k1gMnPc1
(	(	kIx(
<g/>
Noe	Noe	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ibrahím	Ibrahí	k1gMnSc7
(	(	kIx(
<g/>
Abrahám	Abrahám	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnPc1
synové	syn	k1gMnPc1
Ismá	Ismé	k1gNnPc4
<g/>
'	'	kIx"
<g/>
íl	íl	k?
(	(	kIx(
<g/>
Izmael	Izmael	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Ishák	Ishák	k1gMnSc1
(	(	kIx(
<g/>
Izák	Izák	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Músá	Músá	k1gFnSc1
(	(	kIx(
<g/>
Mojžíš	Mojžíš	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Dawud	Dawud	k1gMnSc1
(	(	kIx(
<g/>
David	David	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sulajmán	Sulajmán	k2eAgMnSc1d1
(	(	kIx(
<g/>
Šalomoun	Šalomoun	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jahjá	Jahjá	k1gFnSc1
(	(	kIx(
<g/>
Jan	Jan	k1gMnSc1
Křtitel	křtitel	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Ísý	Ísý	k1gMnSc1
(	(	kIx(
<g/>
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Písmo	písmo	k1gNnSc1
islámu	islám	k1gInSc2
Korán	korán	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
také	také	k9
tří	tři	k4xCgInPc2
proroky	prorok	k1gMnPc4
staroarabské	staroarabský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
Húda	Húda	k1gMnSc1
<g/>
,	,	kIx,
Sáliha	Sáliha	k1gMnSc1
a	a	k8xC
Šu	Šu	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ajba	ajba	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
ale	ale	k8xC
učení	učení	k1gNnSc1
proroků	prorok	k1gMnPc2
a	a	k8xC
poslů	posel	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
přinášeli	přinášet	k5eAaImAgMnP
zjevenou	zjevený	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
(	(	kIx(
<g/>
Bible	bible	k1gFnSc1
<g/>
,	,	kIx,
Tóra	tóra	k1gFnSc1
<g/>
)	)	kIx)
odmítali	odmítat	k5eAaImAgMnP
nebo	nebo	k8xC
pokřivili	pokřivit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byl	být	k5eAaImAgMnS
Mohamed	Mohamed	k1gMnSc1
vybrán	vybrat	k5eAaPmNgMnS
za	za	k7c4
"	"	kIx"
<g/>
pečeť	pečeť	k1gFnSc1
proroků	prorok	k1gMnPc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chata	chata	k1gFnSc1
al-anbijá	al-anbijá	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zvěstoval	zvěstovat	k5eAaImAgMnS
konečné	konečný	k2eAgNnSc4d1
zjevení	zjevení	k1gNnSc4
-	-	kIx~
Korán	korán	k1gInSc1
(	(	kIx(
<g/>
arab	arab	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al	ala	k1gFnPc2
Kur	kur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
án	án	k1gMnSc1
al-Karím	al-Karit	k5eAaPmIp1nS,k5eAaImIp1nS,k5eAaBmIp1nS
<g/>
,	,	kIx,
dosl.	dosl.	k?
Vznešené	vznešený	k2eAgNnSc4d1
čtení	čtení	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
již	již	k6eAd1
nebude	být	k5eNaImBp3nS
pokřivené	pokřivený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabové	Arab	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yRgMnPc3,k3yQgMnPc3
byl	být	k5eAaImAgInS
korán	korán	k1gInSc4
zjeven	zjeven	k2eAgMnSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
příště	příště	k6eAd1
mají	mít	k5eAaImIp3nP
ubírat	ubírat	k5eAaImF
po	po	k7c6
přímé	přímý	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
víry	víra	k1gFnSc2
v	v	k7c4
jediného	jediný	k2eAgMnSc4d1
Boha	bůh	k1gMnSc4
a	a	k8xC
její	její	k3xOp3gFnSc2
odpovídající	odpovídající	k2eAgFnSc2d1
etiky	etika	k1gFnSc2
a	a	k8xC
mají	mít	k5eAaImIp3nP
pro	pro	k7c4
islám	islám	k1gInSc4
získat	získat	k5eAaPmF
celé	celý	k2eAgNnSc4d1
lidstvo	lidstvo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teologie	teologie	k1gFnSc1
</s>
<s>
Islámská	islámský	k2eAgFnSc1d1
teologie	teologie	k1gFnSc1
a	a	k8xC
právní	právní	k2eAgInSc1d1
systém	systém	k1gInSc1
byly	být	k5eAaImAgFnP
přepracovány	přepracovat	k5eAaPmNgInP
hlavně	hlavně	k9
v	v	k7c4
8	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústředním	ústřední	k2eAgInSc7d1
teologickým	teologický	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
je	být	k5eAaImIp3nS
jednota	jednota	k1gFnSc1
boží	boží	k2eAgFnSc1d1
(	(	kIx(
<g/>
tauhíd	tauhíd	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejtěžším	těžký	k2eAgInSc7d3
hříchem	hřích	k1gInSc7
a	a	k8xC
herezí	hereze	k1gFnSc7
je	být	k5eAaImIp3nS
přidružovat	přidružovat	k5eAaImF
k	k	k7c3
Bohu	bůh	k1gMnSc3
jiná	jiný	k2eAgNnPc4d1
božstva	božstvo	k1gNnPc4
nebo	nebo	k8xC
objekty	objekt	k1gInPc4
náboženské	náboženský	k2eAgFnSc2d1
úcty	úcta	k1gFnSc2
(	(	kIx(
<g/>
širk	širk	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odpadlictví	odpadlictví	k1gNnPc1
od	od	k7c2
víry	víra	k1gFnSc2
(	(	kIx(
<g/>
irtidád	irtidáda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
úmyslné	úmyslný	k2eAgNnSc1d1
rozvracení	rozvracení	k1gNnSc1
obce	obec	k1gFnSc2
a	a	k8xC
státu	stát	k1gInSc2
(	(	kIx(
<g/>
Fitna	Fitna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
cizoložství	cizoložství	k1gNnSc1
(	(	kIx(
<g/>
Zina	Zina	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
krádež	krádež	k1gFnSc4
nad	nad	k7c4
stanovené	stanovený	k2eAgNnSc4d1
minimum	minimum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
otázkách	otázka	k1gFnPc6
jedinosti	jedinost	k1gFnSc2
Boha	bůh	k1gMnSc4
je	být	k5eAaImIp3nS
normativní	normativní	k2eAgInSc1d1
klasický	klasický	k2eAgInSc1d1
islám	islám	k1gInSc1
striktní	striktní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítá	odmítat	k5eAaImIp3nS
dualismus	dualismus	k1gInSc4
nebo	nebo	k8xC
prvky	prvek	k1gInPc4
gnóze	gnóze	k1gFnSc2
<g/>
,	,	kIx,
křesťanské	křesťanský	k2eAgNnSc1d1
učení	učení	k1gNnSc1
o	o	k7c6
Trojici	trojice	k1gFnSc6
i	i	k8xC
Ježíšově	Ježíšův	k2eAgInSc6d1
synovstí	synovstět	k5eAaPmIp3nS,k5eAaImIp3nS
Božím	božit	k5eAaImIp1nS
<g/>
;	;	kIx,
uctívá	uctívat	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
pouze	pouze	k6eAd1
jako	jako	k9
významného	významný	k2eAgMnSc2d1
proroka	prorok	k1gMnSc2
syna	syn	k1gMnSc2
panenské	panenský	k2eAgFnSc2d1
matky	matka	k1gFnSc2
Marjam	Marjam	k1gInSc4
a	a	k8xC
tvůrce	tvůrce	k1gMnPc4
zázraků	zázrak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
ani	ani	k8xC
instituci	instituce	k1gFnSc4
církve	církev	k1gFnSc2
a	a	k8xC
kněžstva	kněžstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
organizační	organizační	k2eAgFnSc4d1
formu	forma	k1gFnSc4
pokládá	pokládat	k5eAaImIp3nS
stát	stát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovenstvo	duchovenstvo	k1gNnSc1
představují	představovat	k5eAaImIp3nP
náboženští	náboženský	k2eAgMnPc1d1
vzdělanci	vzdělanec	k1gMnPc1
(	(	kIx(
<g/>
Ulam	ulámat	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hlavně	hlavně	k9
vykladači	vykladač	k1gMnPc1
a	a	k8xC
tvůrci	tvůrce	k1gMnPc1
práva	právo	k1gNnSc2
(	(	kIx(
<g/>
fukahá	fukahat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
věroučné	věroučný	k2eAgFnPc1d1
spory	spora	k1gFnPc1
vyvolal	vyvolat	k5eAaPmAgInS
v	v	k7c6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
racionalistický	racionalistický	k2eAgInSc1d1
teologický	teologický	k2eAgInSc1d1
proud	proud	k1gInSc1
mu	on	k3xPp3gMnSc3
<g/>
'	'	kIx"
<g/>
tazila	tazil	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ortodoxní	ortodoxní	k2eAgInSc4d1
řešení	řešení	k1gNnSc1
prosadil	prosadit	k5eAaPmAgMnS
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
al-Aš	al-Aš	k1gInSc1
<g/>
'	'	kIx"
<g/>
arí	arí	k?
<g/>
,	,	kIx,
"	"	kIx"
<g/>
středovou	středový	k2eAgFnSc4d1
<g/>
"	"	kIx"
polohu	poloha	k1gFnSc4
sunnitského	sunnitský	k2eAgInSc2d1
islámu	islám	k1gInSc2
teoreticky	teoreticky	k6eAd1
dotvořil	dotvořit	k5eAaPmAgInS
Al-Ghazálí	Al-Ghazálý	k2eAgMnPc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
smířil	smířit	k5eAaPmAgInS
nepružné	pružný	k2eNgFnPc4d1
a	a	k8xC
rigidní	rigidní	k2eAgFnPc4d1
myšlenky	myšlenka	k1gFnPc4
ortodoxie	ortodoxie	k1gFnSc2
s	s	k7c7
racionalitou	racionalita	k1gFnSc7
a	a	k8xC
umírněnými	umírněný	k2eAgInPc7d1
postuláty	postulát	k1gInPc7
mystiky	mystika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oživení	oživení	k1gNnSc1
islámské	islámský	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
a	a	k8xC
právního	právní	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
přinesl	přinést	k5eAaPmAgInS
od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
islámský	islámský	k2eAgInSc4d1
reformismus	reformismus	k1gInSc4
(	(	kIx(
<g/>
al-Afghan	al-Afghan	k1gMnSc1
<g/>
,	,	kIx,
Abduh	Abduh	k1gMnSc1
<g/>
,	,	kIx,
Rida	Rida	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
navazují	navazovat	k5eAaImIp3nP
různé	různý	k2eAgInPc1d1
proudy	proud	k1gInPc1
islámského	islámský	k2eAgInSc2d1
modernismu	modernismus	k1gInSc2
a	a	k8xC
fundamentalismu	fundamentalismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Právo	právo	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Šaría	Šarí	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Šaría	Šaría	k6eAd1
v	v	k7c6
různých	různý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
<s>
Země	zem	k1gFnPc1
s	s	k7c7
muslimskou	muslimský	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
však	však	k9
šaría	šaría	k6eAd1
nemá	mít	k5eNaImIp3nS
vliv	vliv	k1gInSc4
na	na	k7c4
soudnictví	soudnictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Právní	právní	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
sekulární	sekulární	k2eAgInSc1d1
<g/>
,	,	kIx,
šaría	šaría	k6eAd1
se	se	k3xPyFc4
však	však	k9
používá	používat	k5eAaImIp3nS
ve	v	k7c6
věcech	věc	k1gFnPc6
osobního	osobní	k2eAgInSc2d1
statusu	status	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Plně	plně	k6eAd1
se	se	k3xPyFc4
uplatňuje	uplatňovat	k5eAaImIp3nS
šaría	šaría	k6eAd1
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
ve	v	k7c6
věcech	věc	k1gFnPc6
osobního	osobní	k2eAgInSc2d1
statusu	status	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
trestním	trestní	k2eAgNnSc6d1
řízení	řízení	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Různě	různě	k6eAd1
v	v	k7c6
různých	různý	k2eAgFnPc6d1
částech	část	k1gFnPc6
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Osobní	osobní	k2eAgInSc4d1
status	status	k1gInSc4
zahrnuje	zahrnovat	k5eAaImIp3nS
manželství	manželství	k1gNnSc1
<g/>
,	,	kIx,
rozvod	rozvod	k1gInSc1
<g/>
,	,	kIx,
dědictví	dědictví	k1gNnSc4
<g/>
,	,	kIx,
péči	péče	k1gFnSc4
o	o	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
pod	pod	k7c4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Náboženské	náboženský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
-	-	kIx~
Šaría	Šaría	k1gFnSc1
-	-	kIx~
ztělesňuje	ztělesňovat	k5eAaImIp3nS
ve	v	k7c6
struktuře	struktura	k1gFnSc6
islámu	islám	k1gInSc2
souhrn	souhrn	k1gInSc1
Božského	božský	k2eAgInSc2d1
pořádku	pořádek	k1gInSc2
přikázaného	přikázaný	k2eAgInSc2d1
lidstvu	lidstvo	k1gNnSc3
<g/>
,	,	kIx,
neproměnný	proměnný	k2eNgInSc1d1
morální	morální	k2eAgInSc1d1
Zákon	zákon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakterizuje	charakterizovat	k5eAaBmIp3nS
islám	islám	k1gInSc1
ještě	ještě	k6eAd1
výrazněji	výrazně	k6eAd2
než	než	k8xS
soustava	soustava	k1gFnSc1
dogmatické	dogmatický	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslimští	muslimský	k2eAgMnPc1d1
učenci	učenec	k1gMnPc1
označují	označovat	k5eAaImIp3nP
někdy	někdy	k6eAd1
své	svůj	k3xOyFgNnSc4
náboženství	náboženství	k1gNnSc4
za	za	k7c4
jednotu	jednota	k1gFnSc4
dogmata	dogma	k1gNnPc4
(	(	kIx(
<g/>
aqída	aqída	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
práva	právo	k1gNnPc1
(	(	kIx(
<g/>
šaría	šaría	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
kultury	kultura	k1gFnSc2
(	(	kIx(
<g/>
Hadar	Hadar	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právní	právní	k2eAgInSc1d1
systém	systém	k1gInSc1
přitom	přitom	k6eAd1
fakticky	fakticky	k6eAd1
zahrnuje	zahrnovat	k5eAaImIp3nS
i	i	k9
celou	celý	k2eAgFnSc4d1
dogmatiku	dogmatika	k1gFnSc4
do	do	k7c2
zákonné	zákonný	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
víry	víra	k1gFnSc2
<g/>
,	,	kIx,
prvního	první	k4xOgMnSc4
z	z	k7c2
pěti	pět	k4xCc2
pilířů	pilíř	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
vztahů	vztah	k1gInPc2
člověka	člověk	k1gMnSc2
k	k	k7c3
Bohu	bůh	k1gMnSc3
(	(	kIx(
<g/>
ibádát	ibádát	k1gMnSc1
<g/>
)	)	kIx)
i	i	k8xC
k	k	k7c3
druhým	druhý	k4xOgMnPc3
lidem	člověk	k1gMnPc3
(	(	kIx(
<g/>
mu	on	k3xPp3gMnSc3
<g/>
'	'	kIx"
<g/>
ámalát	ámalát	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
prostupuje	prostupovat	k5eAaImIp3nS
celý	celý	k2eAgInSc4d1
způsob	způsob	k1gInSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podrobných	podrobný	k2eAgFnPc6d1
rozpracovaných	rozpracovaný	k2eAgFnPc6d1
právníkách	právníkách	k?
se	se	k3xPyFc4
šaría	šaría	k6eAd1
zároveň	zároveň	k6eAd1
stává	stávat	k5eAaImIp3nS
vyčerpávající	vyčerpávající	k2eAgFnSc7d1
soustavou	soustava	k1gFnSc7
zákonů	zákon	k1gInPc2
a	a	k8xC
kvalifikací	kvalifikace	k1gFnPc2
vztahů	vztah	k1gInPc2
a	a	k8xC
činů	čin	k1gInPc2
i	i	k8xC
východiskem	východisko	k1gNnSc7
pro	pro	k7c4
morální	morální	k2eAgFnSc4d1
filozofii	filozofie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vědní	vědní	k2eAgInSc1d1
obor	obor	k1gInSc1
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
(	(	kIx(
<g/>
usúl	usúlit	k5eAaPmRp2nS
al-fiqh	al-fiqh	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
prameny	pramen	k1gInPc1
práva	právo	k1gNnSc2
a	a	k8xC
metodami	metoda	k1gFnPc7
<g/>
,	,	kIx,
kterým	který	k3yIgFnPc3,k3yQgFnPc3,k3yRgFnPc3
se	se	k3xPyFc4
z	z	k7c2
těchto	tento	k3xDgInPc2
pramenů	pramen	k1gInPc2
odvozují	odvozovat	k5eAaImIp3nP
právní	právní	k2eAgFnPc1d1
normy	norma	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmem	pojem	k1gInSc7
fiqh	fiqha	k1gFnPc2
označujeme	označovat	k5eAaImIp1nP
znalost	znalost	k1gFnSc4
konkrétních	konkrétní	k2eAgFnPc2d1
norem	norma	k1gFnPc2
<g/>
,	,	kIx,
resp.	resp.	kA
masu	maso	k1gNnSc3
práva	právo	k1gNnSc2
získanou	získaný	k2eAgFnSc4d1
z	z	k7c2
pramenů	pramen	k1gInPc2
na	na	k7c6
základě	základ	k1gInSc6
metod	metoda	k1gFnPc2
nalézání	nalézání	k1gNnSc4
a	a	k8xC
odvozování	odvozování	k1gNnSc4
norem	norma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fiqh	Fiqh	k1gInSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
produktem	produkt	k1gInSc7
usúl	usúla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
pramenům	pramen	k1gInPc3
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
je	být	k5eAaImIp3nS
řazen	řadit	k5eAaImNgInS
Korán	korán	k1gInSc1
<g/>
,	,	kIx,
hadísy	hadís	k1gInPc1
(	(	kIx(
<g/>
resp.	resp.	kA
Sunna	sunna	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
idžmá	idžmat	k5eAaPmIp3nS
<g/>
,	,	kIx,
podle	podle	k7c2
školy	škola	k1gFnSc2
či	či	k8xC
autora	autor	k1gMnSc2
pak	pak	k6eAd1
další	další	k2eAgInPc1d1
prameny	pramen	k1gInPc1
jako	jako	k8xC,k8xS
předchozí	předchozí	k2eAgNnSc4d1
zjevené	zjevený	k2eAgNnSc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
názory	názor	k1gInPc1
resp.	resp.	kA
Fatwy	Fatwa	k1gFnPc4
druhů	druh	k1gInPc2
Prorokových	prorokův	k2eAgInPc2d1
<g/>
,	,	kIx,
veřejný	veřejný	k2eAgInSc1d1
zájem	zájem	k1gInSc1
(	(	kIx(
<g/>
maslaha	maslaha	k1gFnSc1
)	)	kIx)
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
tvorbě	tvorba	k1gFnSc3
právních	právní	k2eAgFnPc2d1
norem	norma	k1gFnPc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
spekulativní	spekulativní	k2eAgInPc1d1
metodické	metodický	k2eAgInPc1d1
postupy	postup	k1gInPc1
(	(	kIx(
<g/>
analogie	analogie	k1gFnPc1
<g/>
,	,	kIx,
konsensus	konsensus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřily	patřit	k5eAaImAgInP
sem	sem	k6eAd1
i	i	k9
další	další	k2eAgInPc1d1
postupy	postup	k1gInPc1
jako	jako	k8xC,k8xS
dočasné	dočasný	k2eAgNnSc1d1
využití	využití	k1gNnSc1
staroarabského	staroarabský	k2eAgNnSc2d1
zvykového	zvykový	k2eAgNnSc2d1
práva	právo	k1gNnSc2
(	(	kIx(
<g/>
adát	adát	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
právní	právní	k2eAgFnPc1d1
normy	norma	k1gFnPc1
Byzance	Byzanc	k1gFnSc2
či	či	k8xC
Persie	Persie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Méně	málo	k6eAd2
známou	známý	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
byla	být	k5eAaImAgFnS
tzv.	tzv.	kA
amelioracie	amelioracie	k1gFnSc1
(	(	kIx(
<g/>
sitihsán	sitihsán	k2eAgInSc1d1
<g/>
,	,	kIx,
istisláh	istisláh	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
reforma	reforma	k1gFnSc1
či	či	k8xC
vylepšení	vylepšení	k1gNnSc1
předchozí	předchozí	k2eAgFnSc2d1
právní	právní	k2eAgFnSc2d1
normy	norma	k1gFnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
zajistit	zajistit	k5eAaPmF
princip	princip	k1gInSc4
obecného	obecný	k2eAgNnSc2d1
blaha	blaho	k1gNnSc2
(	(	kIx(
<g/>
al-maslaha	al-maslaha	k1gFnSc1
al-	al-	k?
<g/>
'	'	kIx"
<g/>
ámma	ámma	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecním	obecnět	k5eAaImIp1nS
metodologickým	metodologický	k2eAgInSc7d1
postupem	postup	k1gInSc7
zahrnujícím	zahrnující	k2eAgInSc7d1
všechny	všechen	k3xTgInPc4
výše	vysoce	k6eAd2
zmíněné	zmíněný	k2eAgInPc4d1
postupy	postup	k1gInPc4
byl	být	k5eAaImAgInS
princip	princip	k1gInSc1
idžtihád	idžtiháda	k1gFnPc2
-	-	kIx~
právo	právo	k1gNnSc1
samostatného	samostatný	k2eAgInSc2d1
úsudku	úsudek	k1gInSc2
a	a	k8xC
volné	volný	k2eAgFnSc2d1
interpretace	interpretace	k1gFnSc2
jevů	jev	k1gInPc2
s	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
základním	základní	k2eAgInPc3d1
textům	text	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Islámské	islámský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
je	být	k5eAaImIp3nS
souborem	soubor	k1gInSc7
Bohem	bůh	k1gMnSc7
zjevených	zjevený	k2eAgFnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
musí	muset	k5eAaImIp3nS
zbožný	zbožný	k2eAgMnSc1d1
muslim	muslim	k1gMnSc1
dodržovat	dodržovat	k5eAaImF
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
<g/>
-li	-li	k?
řádně	řádně	k6eAd1
plnit	plnit	k5eAaImF
své	svůj	k3xOyFgFnPc4
náboženské	náboženský	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
platnost	platnost	k1gFnSc4
a	a	k8xC
legitimitu	legitimita	k1gFnSc4
šaría	šarí	k1gInSc2
odvozuje	odvozovat	k5eAaImIp3nS
od	od	k7c2
svého	svůj	k3xOyFgInSc2
božského	božský	k2eAgInSc2d1
původu	původ	k1gInSc2
-	-	kIx~
je	být	k5eAaImIp3nS
manifestací	manifestace	k1gFnSc7
vůle	vůle	k1gFnSc2
Všemohoucího	Všemohoucí	k1gMnSc2
<g/>
,	,	kIx,
nezávisí	záviset	k5eNaImIp3nS
na	na	k7c6
autoritě	autorita	k1gFnSc6
jakéhokoli	jakýkoli	k3yIgMnSc2
pozemského	pozemský	k2eAgMnSc2d1
zákonodárce	zákonodárce	k1gMnSc2
nebo	nebo	k8xC
jednotlivce	jednotlivec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámské	islámský	k2eAgNnSc4d1
právo	právo	k1gNnSc4
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
Korán	korán	k1gInSc4
nestvořené	stvořený	k2eNgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k8xC,k8xS
zvláštní	zvláštní	k2eAgInSc1d1
druh	druh	k1gInSc1
se	se	k3xPyFc4
vyděluje	vydělovat	k5eAaImIp3nS
trestní	trestní	k2eAgNnSc1d1
právo	právo	k1gNnSc1
(	(	kIx(
<g/>
uqúbát	uqúbát	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
vypočítává	vypočítávat	k5eAaImIp3nS
zakázané	zakázaný	k2eAgNnSc4d1
resp.	resp.	kA
nevhodné	vhodný	k2eNgInPc4d1
činy	čin	k1gInPc4
a	a	k8xC
za	za	k7c4
překračování	překračování	k1gNnSc4
zákazů	zákaz	k1gInPc2
stanoví	stanovit	k5eAaPmIp3nS
systematicky	systematicky	k6eAd1
rozpracovaný	rozpracovaný	k2eAgInSc1d1
soubor	soubor	k1gInSc1
trestů	trest	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidské	lidský	k2eAgInPc1d1
skutky	skutek	k1gInPc1
se	se	k3xPyFc4
v	v	k7c6
šaríi	šarí	k1gFnSc6
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
pět	pět	k4xCc4
kategorií	kategorie	k1gFnPc2
<g/>
:	:	kIx,
povinnosti	povinnost	k1gFnPc1
(	(	kIx(
<g/>
fard	fard	k1gInSc1
<g/>
,	,	kIx,
farid	farid	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
činy	čin	k1gInPc1
bohulibé	bohulibý	k2eAgFnSc2d1
(	(	kIx(
<g/>
mandúb	mandúba	k1gFnPc2
<g/>
,	,	kIx,
mustahabb	mustahabba	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
činy	čin	k1gInPc1
indeferentní	indeferentní	k2eAgFnSc2d1
(	(	kIx(
<g/>
mubáh	mubáha	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
činy	čin	k1gInPc1
Bohu	bůh	k1gMnSc3
odporné	odporný	k2eAgInPc1d1
(	(	kIx(
<g/>
makruh	makruh	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
zakázané	zakázaný	k2eAgNnSc4d1
(	(	kIx(
<g/>
harám	harat	k5eAaImIp1nS,k5eAaBmIp1nS,k5eAaPmIp1nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Porovnání	porovnání	k1gNnSc1
se	s	k7c7
západním	západní	k2eAgNnSc7d1
právem	právo	k1gNnSc7
</s>
<s>
Mešita	mešita	k1gFnSc1
al-Azhar	al-Azhara	k1gFnPc2
v	v	k7c6
Káhiře	Káhira	k1gFnSc6
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc7d3
teologickou	teologický	k2eAgFnSc7d1
autoritou	autorita	k1gFnSc7
v	v	k7c6
Egyptě	Egypt	k1gInSc6
</s>
<s>
V	v	k7c6
západním	západní	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
je	být	k5eAaImIp3nS
právo	právo	k1gNnSc1
projevem	projev	k1gInSc7
vůle	vůle	k1gFnSc1
zákonodárce	zákonodárce	k1gMnSc1
(	(	kIx(
<g/>
panovník	panovník	k1gMnSc1
<g/>
,	,	kIx,
lid	lid	k1gInSc1
<g/>
,	,	kIx,
volení	volený	k2eAgMnPc1d1
zástupci	zástupce	k1gMnPc1
lidu	lid	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
historickém	historický	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
<g/>
,	,	kIx,
měnících	měnící	k2eAgFnPc6d1
se	se	k3xPyFc4
potřebách	potřeba	k1gFnPc6
a	a	k8xC
přáních	přání	k1gNnPc6
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
morální	morální	k2eAgFnPc4d1
a	a	k8xC
etické	etický	k2eAgFnPc4d1
normy	norma	k1gFnPc4
umísťované	umísťovaný	k2eAgFnPc4d1
do	do	k7c2
právního	právní	k2eAgInSc2d1
řádu	řád	k1gInSc2
podléhají	podléhat	k5eAaImIp3nP
změnám	změna	k1gFnPc3
etického	etický	k2eAgNnSc2d1
a	a	k8xC
morálního	morální	k2eAgNnSc2d1
povědomí	povědomí	k1gNnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
přizpůsobovat	přizpůsobovat	k5eAaImF
a	a	k8xC
měnit	měnit	k5eAaImF
podle	podle	k7c2
vůle	vůle	k1gFnSc2
zákonodárce	zákonodárce	k1gMnSc1
a	a	k8xC
rozhodující	rozhodující	k2eAgMnSc1d1
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
nebo	nebo	k8xC
formulování	formulování	k1gNnSc1
norem	norma	k1gFnPc2
není	být	k5eNaImIp3nS
absolutní	absolutní	k2eAgFnSc4d1
„	„	k?
<g/>
správnost	správnost	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
ale	ale	k8xC
dobové	dobový	k2eAgFnSc2d1
představy	představa	k1gFnSc2
zákonodárce	zákonodárce	k1gMnSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
správné	správný	k2eAgNnSc1d1
a	a	k8xC
hlavně	hlavně	k9
vhodné	vhodný	k2eAgNnSc1d1
a	a	k8xC
výhodné	výhodný	k2eAgNnSc1d1
pro	pro	k7c4
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
islámské	islámský	k2eAgNnSc4d1
Boží	boží	k2eAgNnSc4d1
právo	právo	k1gNnSc4
podle	podle	k7c2
islámské	islámský	k2eAgFnSc2d1
věrouky	věrouka	k1gFnSc2
dané	daný	k2eAgFnSc2d1
člověku	člověk	k1gMnSc3
jednou	jeden	k4xCgFnSc7
provždy	provždy	k6eAd1
-	-	kIx~
společnost	společnost	k1gFnSc1
se	se	k3xPyFc4
sama	sám	k3xTgMnSc4
musí	muset	k5eAaImIp3nS
přizpůsobit	přizpůsobit	k5eAaPmF
právu	právo	k1gNnSc3
<g/>
,	,	kIx,
spíše	spíše	k9
než	než	k8xS
aby	aby	kYmCp3nS
vytvářela	vytvářet	k5eAaImAgFnS
své	svůj	k3xOyFgInPc4
vlastní	vlastní	k2eAgInPc4d1
zákony	zákon	k1gInPc4
jako	jako	k8xS,k8xC
odpověď	odpověď	k1gFnSc4
na	na	k7c6
stále	stále	k6eAd1
se	se	k3xPyFc4
měnící	měnící	k2eAgInPc1d1
problémy	problém	k1gInPc1
běžného	běžný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámská	islámský	k2eAgFnSc1d1
právní	právní	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
uznává	uznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Boží	boží	k2eAgNnSc1d1
zjevení	zjevení	k1gNnSc1
nebylo	být	k5eNaImAgNnS
sesláno	seslat	k5eAaPmNgNnS
v	v	k7c6
zcela	zcela	k6eAd1
přístupné	přístupný	k2eAgFnSc6d1
<g/>
,	,	kIx,
srozumitelné	srozumitelný	k2eAgFnSc6d1
a	a	k8xC
systematizované	systematizovaný	k2eAgFnSc6d1
formě	forma	k1gFnSc6
a	a	k8xC
bylo	být	k5eAaImAgNnS
třeba	třeba	k6eAd1
vyvinout	vyvinout	k5eAaPmF
úsilí	úsilí	k1gNnSc3
islámských	islámský	k2eAgMnPc2d1
právníků	právník	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
získán	získat	k5eAaPmNgInS
a	a	k8xC
formulován	formulovat	k5eAaImNgInS
plný	plný	k2eAgInSc1d1
rozsah	rozsah	k1gInSc1
práva	právo	k1gNnSc2
na	na	k7c6
základě	základ	k1gInSc6
jím	jíst	k5eAaImIp1nS
uznaných	uznaný	k2eAgMnPc2d1
pramenů	pramen	k1gInPc2
a	a	k8xC
aby	aby	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
toto	tento	k3xDgNnSc4
právo	právo	k1gNnSc4
v	v	k7c6
praxi	praxe	k1gFnSc6
použito	použit	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
úsilí	úsilí	k1gNnSc1
však	však	k9
nesměřovalo	směřovat	k5eNaImAgNnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
nových	nový	k2eAgFnPc2d1
právních	právní	k2eAgFnPc2d1
norem	norma	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
jakoby	jakoby	k8xS
k	k	k7c3
objevení	objevení	k1gNnSc3
<g/>
,	,	kIx,
pochopení	pochopení	k1gNnSc3
a	a	k8xC
formulaci	formulace	k1gFnSc3
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
již	již	k9
existuje	existovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pojem	pojem	k1gInSc1
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
je	být	k5eAaImIp3nS
abstrakce	abstrakce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistuje	existovat	k5eNaImIp3nS
obecné	obecný	k2eAgNnSc1d1
islámské	islámský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pojem	pojem	k1gInSc1
se	se	k3xPyFc4
inherentně	inherentně	k6eAd1
může	moct	k5eAaImIp3nS
dost	dost	k6eAd1
výrazně	výrazně	k6eAd1
lišit	lišit	k5eAaImF
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
právo	právo	k1gNnSc4
sunnitské	sunnitský	k2eAgFnSc2d1
nebo	nebo	k8xC
šíitské	šíitský	k2eAgFnSc2d1
<g/>
,	,	kIx,
a	a	k8xC
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
převládající	převládající	k2eAgNnSc4d1
právo	právo	k1gNnSc4
sunnitské	sunnitský	k2eAgNnSc4d1
<g/>
,	,	kIx,
najdeme	najít	k5eAaPmIp1nP
rozdílné	rozdílný	k2eAgInPc4d1
názory	názor	k1gInPc4
nejen	nejen	k6eAd1
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
právními	právní	k2eAgFnPc7d1
školami	škola	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
rozdílné	rozdílný	k2eAgInPc1d1
názory	názor	k1gInPc1
na	na	k7c4
řešení	řešení	k1gNnSc4
některých	některý	k3yIgFnPc2
otázek	otázka	k1gFnPc2
existují	existovat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
rámci	rámec	k1gInSc6
těchto	tento	k3xDgFnPc2
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Muslimové	muslim	k1gMnPc1
usilují	usilovat	k5eAaImIp3nP
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
islámské	islámský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
upravovalo	upravovat	k5eAaImAgNnS
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
všemi	všecek	k3xTgMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nP
k	k	k7c3
islámu	islám	k1gInSc3
<g/>
,	,	kIx,
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
na	na	k7c4
území	území	k1gNnSc4
jakého	jaký	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
státu	stát	k1gInSc2
žijí	žít	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámské	islámský	k2eAgNnSc1d1
právo	právo	k1gNnSc4
jako	jako	k8xS,k8xC
právo	právo	k1gNnSc4
určité	určitý	k2eAgFnSc2d1
náboženské	náboženský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
tedy	tedy	k9
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
principu	princip	k1gInSc2
personality	personalita	k1gFnSc2
<g/>
,	,	kIx,
vztahuje	vztahovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
všechny	všechen	k3xTgMnPc4
muslimy	muslim	k1gMnPc4
a	a	k8xC
v	v	k7c6
celém	celý	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
pouze	pouze	k6eAd1
na	na	k7c4
ně	on	k3xPp3gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
islámské	islámský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
podřízení	podřízený	k1gMnPc1
i	i	k8xC
nemuslimové	nemuslim	k1gMnPc1
<g/>
,	,	kIx,
hlavně	hlavně	k9
křesťané	křesťan	k1gMnPc1
a	a	k8xC
židé	žid	k1gMnPc1
<g/>
,	,	kIx,
vztahují	vztahovat	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c4
ně	on	k3xPp3gFnPc4
buď	buď	k8xC
zvláštní	zvláštní	k2eAgFnPc4d1
normy	norma	k1gFnPc4
<g/>
,	,	kIx,
např.	např.	kA
předpisy	předpis	k1gInPc4
upravující	upravující	k2eAgNnSc4d1
placení	placení	k1gNnSc4
daně	daň	k1gFnSc2
z	z	k7c2
hlavy	hlava	k1gFnSc2
(	(	kIx(
<g/>
džizja	džizja	k6eAd1
<g/>
)	)	kIx)
nebo	nebo	k8xC
daně	daň	k1gFnPc1
z	z	k7c2
půdy	půda	k1gFnSc2
(	(	kIx(
<g/>
charadž	charadž	k6eAd1
<g/>
)	)	kIx)
namísto	namísto	k7c2
náboženské	náboženský	k2eAgFnSc2d1
daně	daň	k1gFnSc2
(	(	kIx(
<g/>
zakát	zakát	k1gInSc1
<g/>
)	)	kIx)
závazné	závazný	k2eAgNnSc1d1
pro	pro	k7c4
muslimy	muslim	k1gMnPc4
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
podřízeni	podřízen	k2eAgMnPc1d1
jurisdikci	jurisdikce	k1gFnSc4
vlastních	vlastní	k2eAgInPc6d1
<g/>
,	,	kIx,
tj.	tj.	kA
rabínských	rabínský	k2eAgInPc2d1
nebo	nebo	k8xC
církevních	církevní	k2eAgInPc2d1
tribunálů	tribunál	k1gInPc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
soukromého	soukromý	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úsilí	úsilí	k1gNnSc1
dnešních	dnešní	k2eAgInPc2d1
islámských	islámský	k2eAgInPc2d1
radikálů	radikál	k1gInPc2
plošně	plošně	k6eAd1
zavádět	zavádět	k5eAaImF
islámské	islámský	k2eAgNnSc4d1
právo	právo	k1gNnSc4
na	na	k7c6
územním	územní	k2eAgInSc6d1
principu	princip	k1gInSc6
s	s	k7c7
platností	platnost	k1gFnSc7
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
obyvatele	obyvatel	k1gMnPc4
tam	tam	k6eAd1
žijících	žijící	k2eAgFnPc2d1
<g/>
,	,	kIx,
na	na	k7c4
místo	místo	k1gNnSc4
obecně	obecně	k6eAd1
platného	platný	k2eAgNnSc2d1
práva	právo	k1gNnSc2
je	být	k5eAaImIp3nS
inovace	inovace	k1gFnSc1
ovlivněná	ovlivněný	k2eAgFnSc1d1
-	-	kIx~
i	i	k9
když	když	k8xS
to	ten	k3xDgNnSc1
zní	znět	k5eAaImIp3nS
paradoxně	paradoxně	k6eAd1
-	-	kIx~
právními	právní	k2eAgInPc7d1
systémy	systém	k1gInPc7
moderního	moderní	k2eAgNnSc2d1
<g/>
,	,	kIx,
územně	územně	k6eAd1
vymezeného	vymezený	k2eAgInSc2d1
státu	stát	k1gInSc2
s	s	k7c7
jednotnou	jednotný	k2eAgFnSc7d1
legislativou	legislativa	k1gFnSc7
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
občany	občan	k1gMnPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
islámu	islám	k1gInSc6
koncepcí	koncepce	k1gFnPc2
nepůvodní	původní	k2eNgFnSc1d1
<g/>
,	,	kIx,
nehistorickou	historický	k2eNgFnSc4d1
a	a	k8xC
klasickému	klasický	k2eAgInSc3d1
islámskému	islámský	k2eAgInSc3d1
právu	právo	k1gNnSc6
cizí	cizí	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Právní	právní	k2eAgFnPc1d1
školy	škola	k1gFnPc1
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
islamské	islamský	k2eAgFnPc1d1
právní	právní	k2eAgFnPc1d1
školy	škola	k1gFnPc1
</s>
<s>
V	v	k7c6
sunnitském	sunnitský	k2eAgInSc6d1
islámu	islám	k1gInSc6
se	se	k3xPyFc4
do	do	k7c2
současnosti	současnost	k1gFnSc2
uchovaly	uchovat	k5eAaPmAgInP
čtyři	čtyři	k4xCgInPc1
právní	právní	k2eAgInPc1d1
směry	směr	k1gInPc1
(	(	kIx(
<g/>
mazhaby	mazhaba	k1gFnPc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
hanífovský	hanífovský	k1gMnSc1
<g/>
,	,	kIx,
málikovský	málikovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
šáfi	šáfi	k1gNnSc1
<g/>
'	'	kIx"
<g/>
ovský	ovský	k2eAgMnSc1d1
a	a	k8xC
hanbalovský	hanbalovský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ší	Ší	k1gFnSc1
<g/>
'	'	kIx"
<g/>
iti	iti	k?
mají	mít	k5eAaImIp3nP
vlastní	vlastní	k2eAgInSc4d1
právní	právní	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
postaven	postavit	k5eAaPmNgInS
na	na	k7c6
vlastních	vlastní	k2eAgInPc6d1
textech	text	k1gInPc6
prorocké	prorocký	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
(	(	kIx(
<g/>
achbár	achbár	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
podle	podle	k7c2
6	#num#	k4
<g/>
.	.	kIx.
imáma	imám	k1gMnSc4
jako	jako	k8xS,k8xC
dža	dža	k?
<g/>
'	'	kIx"
<g/>
farovský	farovský	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
sunnitském	sunnitský	k2eAgInSc6d1
islámu	islám	k1gInSc6
představují	představovat	k5eAaImIp3nP
čtyři	čtyři	k4xCgFnPc4
zmíněné	zmíněný	k2eAgFnPc4d1
mazhaby	mazhaba	k1gFnPc4
již	již	k9
několik	několik	k4yIc4
století	století	k1gNnSc2
obecným	obecný	k2eAgInSc7d1
konsensem	konsens	k1gInSc7
přijímanou	přijímaný	k2eAgFnSc4d1
pluralitu	pluralita	k1gFnSc4
pravd	pravda	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
jediné	jediný	k2eAgFnSc2d1
ortodoxie	ortodoxie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíly	rozdíl	k1gInPc4
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
školami	škola	k1gFnPc7
a	a	k8xC
nejednotnost	nejednotnost	k1gFnSc1
názorů	názor	k1gInPc2
uvnitř	uvnitř	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
škol	škola	k1gFnPc2
mají	mít	k5eAaImIp3nP
původ	původ	k1gInSc4
jednak	jednak	k8xC
v	v	k7c6
uplatňování	uplatňování	k1gNnSc6
svobodného	svobodný	k2eAgInSc2d1
úsudku	úsudek	k1gInSc2
jednotlivých	jednotlivý	k2eAgMnPc2d1
učenců	učenec	k1gMnPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
v	v	k7c6
místním	místní	k2eAgNnSc6d1
zvykovém	zvykový	k2eAgNnSc6d1
právu	právo	k1gNnSc6
a	a	k8xC
lokálních	lokální	k2eAgFnPc6d1
právních	právní	k2eAgFnPc6d1
tradicích	tradice	k1gFnPc6
a	a	k8xC
doktrínách	doktrína	k1gFnPc6
<g/>
,	,	kIx,
významné	významný	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
rozdílné	rozdílný	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
práce	práce	k1gFnSc2
s	s	k7c7
právními	právní	k2eAgInPc7d1
prameny	pramen	k1gInPc7
resp.	resp.	kA
názory	názor	k1gInPc1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k9
pramenem	pramen	k1gInSc7
práva	právo	k1gNnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
není	být	k5eNaImIp3nS
a	a	k8xC
jaká	jaký	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
hierarchie	hierarchie	k1gFnSc1
těchto	tento	k3xDgInPc2
pramenů	pramen	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hanífovský	Hanífovský	k2eAgInSc1d1
mazhab	mazhab	k1gInSc1
převládal	převládat	k5eAaImAgInS
v	v	k7c6
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
součástí	součást	k1gFnSc7
Osmanské	osmanský	k2eAgFnPc1d1
říše	říš	k1gFnPc1
a	a	k8xC
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
málikovská	málikovský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
dominantní	dominantní	k2eAgFnSc1d1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
a	a	k8xC
saharské	saharský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
Střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
a	a	k8xC
na	na	k7c6
Kavkaze	Kavkaz	k1gInSc6
<g/>
,	,	kIx,
šáfi	šáfi	k6eAd1
<g/>
'	'	kIx"
<g/>
ovský	ovský	k2eAgInSc1d1
mazhab	mazhab	k1gInSc1
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgInS
hlavně	hlavně	k9
v	v	k7c6
Egyptě	Egypt	k1gInSc6
a	a	k8xC
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
Arabského	arabský	k2eAgInSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
poloostrova	poloostrov	k1gInSc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
však	však	k9
v	v	k7c6
Malajsii	Malajsie	k1gFnSc6
<g/>
,	,	kIx,
Indonésii	Indonésie	k1gFnSc6
a	a	k8xC
na	na	k7c6
Filipínách	Filipíny	k1gFnPc6
<g/>
,	,	kIx,
hanbalovský	hanbalovský	k2eAgInSc1d1
(	(	kIx(
<g/>
nejpřísnější	přísný	k2eAgInSc1d3
<g/>
)	)	kIx)
mazhab	mazhab	k1gInSc1
se	se	k3xPyFc4
ve	v	k7c6
středověku	středověk	k1gInSc6
neprosadil	prosadit	k5eNaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
reinterpretován	reinterpretován	k2eAgInSc4d1
a	a	k8xC
oživen	oživen	k2eAgInSc4d1
Ibn	Ibn	k1gMnSc7
Tajmíjem	Tajmíj	k1gMnSc7
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
inspirací	inspirace	k1gFnPc2
pro	pro	k7c4
legislativu	legislativa	k1gFnSc4
wahhábistů	wahhábista	k1gMnPc2
na	na	k7c6
arabském	arabský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nové	Nové	k2eAgInPc1d1
přístupy	přístup	k1gInPc1
k	k	k7c3
právu	právo	k1gNnSc3
přinesl	přinést	k5eAaPmAgInS
pod	pod	k7c7
tlakem	tlak	k1gInSc7
evropského	evropský	k2eAgNnSc2d1
osvícenství	osvícenství	k1gNnSc2
a	a	k8xC
kolonialismu	kolonialismus	k1gInSc2
až	až	k9
reformismus	reformismus	k1gInSc1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
rozvinuly	rozvinout	k5eAaPmAgFnP
rozhořčené	rozhořčený	k2eAgFnPc1d1
diskuse	diskuse	k1gFnPc1
o	o	k7c6
povaze	povaha	k1gFnSc6
šaríi	šaríi	k1gNnPc4
<g/>
,	,	kIx,
její	její	k3xOp3gNnPc1
místa	místo	k1gNnPc1
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
a	a	k8xC
míře	míra	k1gFnSc6
její	její	k3xOp3gFnSc2
reinterpretace	reinterpretace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
těchto	tento	k3xDgFnPc2
diskusí	diskuse	k1gFnPc2
se	se	k3xPyFc4
zapojovaly	zapojovat	k5eAaImAgInP
jednak	jednak	k8xC
konzervativnější	konzervativní	k2eAgInPc1d2
kruhy	kruh	k1gInPc1
Ulam	ulámat	k5eAaPmRp2nS
<g/>
,	,	kIx,
jednak	jednak	k8xC
kazatelé	kazatel	k1gMnPc1
a	a	k8xC
aktivisté	aktivista	k1gMnPc1
fundamentalistických	fundamentalistický	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
zpravidla	zpravidla	k6eAd1
požadují	požadovat	k5eAaImIp3nP
zavedení	zavedení	k1gNnSc4
"	"	kIx"
<g/>
původní	původní	k2eAgFnSc2d1
šaríi	šarí	k1gFnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
aš-šaría	aš-šaría	k1gFnSc1
al-Asilů	al-Asil	k1gInPc2
<g/>
)	)	kIx)
o	o	k7c4
to	ten	k3xDgNnSc4
urputněji	urputně	k6eAd2
<g/>
,	,	kIx,
oč	oč	k6eAd1
méně	málo	k6eAd2
jsou	být	k5eAaImIp3nP
kompetentní	kompetentní	k2eAgNnSc1d1
ji	on	k3xPp3gFnSc4
rozvíjet	rozvíjet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
fundamentalisty	fundamentalista	k1gMnPc4
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
základní	základní	k2eAgInSc4d1
požadavek	požadavek	k1gInSc4
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
odlišnosti	odlišnost	k1gFnPc4
vyplývající	vyplývající	k2eAgFnPc4d1
z	z	k7c2
teritoriálních	teritoriální	k2eAgFnPc2d1
nebo	nebo	k8xC
kulturních	kulturní	k2eAgFnPc2d1
specifik	specifika	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pět	pět	k4xCc1
pilířů	pilíř	k1gInPc2
islámu	islám	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Pět	pět	k4xCc1
pilířů	pilíř	k1gInPc2
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
muslimové	muslim	k1gMnPc1
v	v	k7c6
Bahrajnu	Bahrajno	k1gNnSc6
</s>
<s>
Zásadní	zásadní	k2eAgInSc4d1
význam	význam	k1gInSc4
má	mít	k5eAaImIp3nS
pět	pět	k4xCc4
povinností	povinnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
muslimské	muslimský	k2eAgNnSc1d1
učení	učení	k1gNnSc1
souhrnně	souhrnně	k6eAd1
označuje	označovat	k5eAaImIp3nS
za	za	k7c4
pět	pět	k4xCc4
sloupů	sloup	k1gInPc2
nebo	nebo	k8xC
pilířů	pilíř	k1gInPc2
náboženství	náboženství	k1gNnSc2
(	(	kIx(
<g/>
arkán	arkán	k1gInSc1
ad-dín	ad-dín	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
tradicí	tradice	k1gFnSc7
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
pokládají	pokládat	k5eAaImIp3nP
za	za	k7c4
nejpodstatnější	podstatný	k2eAgInPc4d3
znaky	znak	k1gInPc4
příslušnosti	příslušnost	k1gFnSc2
k	k	k7c3
islámu	islám	k1gInSc3
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
vyznání	vyznání	k1gNnSc1
víry	víra	k1gFnSc2
(	(	kIx(
<g/>
šaháda	šaháda	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
víra	víra	k1gFnSc1
v	v	k7c4
jedinost	jedinost	k1gFnSc4
Boží	božit	k5eAaImIp3nS
a	a	k8xC
božské	božský	k2eAgNnSc1d1
poslání	poslání	k1gNnSc1
Mohameda	Mohamed	k1gMnSc2
</s>
<s>
modlitba	modlitba	k1gFnSc1
(	(	kIx(
<g/>
salát	salát	k1gInSc1
<g/>
,	,	kIx,
per	pero	k1gNnPc2
<g/>
.	.	kIx.
namáz	namáza	k1gFnPc2
<g/>
)	)	kIx)
–	–	k?
pětkrát	pětkrát	k6eAd1
denně	denně	k6eAd1
s	s	k7c7
směrem	směr	k1gInSc7
k	k	k7c3
Mekce	Mekka	k1gFnSc3
</s>
<s>
předepsaná	předepsaný	k2eAgFnSc1d1
almužna	almužna	k1gFnSc1
(	(	kIx(
<g/>
zakát	zakát	k1gMnSc1
<g/>
,	,	kIx,
sadaka	sadak	k1gMnSc2
<g/>
)	)	kIx)
–	–	k?
2,5	2,5	k4
%	%	kIx~
čistých	čistý	k2eAgInPc2d1
ročních	roční	k2eAgInPc2d1
zisků	zisk	k1gInPc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
nižších	nízký	k2eAgFnPc2d2
sociálních	sociální	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
<g/>
;	;	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
z	z	k7c2
něho	on	k3xPp3gNnSc2
stala	stát	k5eAaPmAgFnS
náboženská	náboženský	k2eAgFnSc1d1
daň	daň	k1gFnSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
tyto	tento	k3xDgInPc4
prostředky	prostředek	k1gInPc4
bylo	být	k5eAaImAgNnS
možno	možno	k9
použít	použít	k5eAaPmF
nejen	nejen	k6eAd1
charitativně	charitativně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
k	k	k7c3
zajištění	zajištění	k1gNnSc3
chodu	chod	k1gInSc2
muslimské	muslimský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
dokonce	dokonce	k9
i	i	k9
k	k	k7c3
financování	financování	k1gNnSc3
válek	válka	k1gFnPc2
</s>
<s>
půst	půst	k1gInSc4
v	v	k7c6
měsíci	měsíc	k1gInSc6
ramadán	ramadán	k1gInSc1
(	(	kIx(
<g/>
saum	saum	k1gMnSc1
<g/>
,	,	kIx,
sawm	sawm	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
v	v	k7c6
měsíci	měsíc	k1gInSc6
Ramadánu	ramadán	k1gInSc2
od	od	k7c2
úsvitu	úsvit	k1gInSc2
do	do	k7c2
soumraku	soumrak	k1gInSc2
</s>
<s>
pouť	pouť	k1gFnSc1
do	do	k7c2
Mekky	Mekka	k1gFnSc2
(	(	kIx(
<g/>
hadždž	hadždž	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
jednou	jednou	k6eAd1
za	za	k7c4
život	život	k1gInSc4
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
<g/>
-li	-li	k?
to	ten	k3xDgNnSc1
finanční	finanční	k2eAgFnSc1d1
situace	situace	k1gFnSc1
a	a	k8xC
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
muslima	muslim	k1gMnSc2
</s>
<s>
V	v	k7c6
právnických	právnický	k2eAgFnPc6d1
knihách	kniha	k1gFnPc6
se	se	k3xPyFc4
celý	celý	k2eAgInSc4d1
okruh	okruh	k1gInSc4
ibádát	ibádát	k1gMnSc1
zpravidla	zpravidla	k6eAd1
přebírá	přebírat	k5eAaImIp3nS
hned	hned	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc2
a	a	k8xC
jednotlivým	jednotlivý	k2eAgInPc3d1
"	"	kIx"
<g/>
pilířovým	pilířový	k2eAgFnPc3d1
<g/>
"	"	kIx"
povinnostem	povinnost	k1gFnPc3
jsou	být	k5eAaImIp3nP
věnovány	věnovat	k5eAaPmNgFnP,k5eAaImNgFnP
samostatné	samostatný	k2eAgFnPc1d1
rozsáhlé	rozsáhlý	k2eAgFnPc1d1
kapitoly	kapitola	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namísto	namísto	k7c2
obsahu	obsah	k1gInSc2
víry	víra	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
předmětem	předmět	k1gInSc7
teologie	teologie	k1gFnSc2
<g/>
,	,	kIx,
právní	právní	k2eAgFnSc2d1
kompedie	kompedie	k1gFnSc2
však	však	k9
rozebírají	rozebírat	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
nejdříve	dříve	k6eAd3
pojem	pojem	k1gInSc1
rituální	rituální	k2eAgFnSc2d1
čistoty	čistota	k1gFnSc2
Tahar	Tahara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
následují	následovat	k5eAaImIp3nP
salát	salát	k1gInSc4
<g/>
,	,	kIx,
zakát	zakát	k1gInSc4
<g/>
,	,	kIx,
saum	saum	k6eAd1
a	a	k8xC
hadždž	hadždž	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
tohoto	tento	k3xDgInSc2
souboru	soubor	k1gInSc2
často	často	k6eAd1
připojuje	připojovat	k5eAaImIp3nS
ještě	ještě	k9
svatý	svatý	k2eAgInSc1d1
boj	boj	k1gInSc1
(	(	kIx(
<g/>
džihád	džihád	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyznání	vyznání	k1gNnSc1
víry	víra	k1gFnSc2
(	(	kIx(
<g/>
šaháda	šaháda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Termín	termín	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
svědectví	svědectví	k1gNnSc4
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
stanovení	stanovení	k1gNnSc1
předmětu	předmět	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
arabštině	arabština	k1gFnSc6
také	také	k9
„	„	k?
<g/>
vysvědčení	vysvědčení	k1gNnSc6
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
certifikát	certifikát	k1gInSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
ve	v	k7c6
specifickém	specifický	k2eAgInSc6d1
náboženském	náboženský	k2eAgInSc6d1
významu	význam	k1gInSc6
poté	poté	k6eAd1
„	„	k?
<g/>
vyznání	vyznání	k1gNnSc4
víry	víra	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámská	islámský	k2eAgFnSc1d1
šaháda	šaháda	k1gFnSc1
zní	znět	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Ašhadu	Ašhad	k1gInSc2
an	an	k?
lá	lá	k0
ilaha	ilaha	k1gMnSc1
illa-l-Láhu	illa-l-Láh	k1gMnSc3
wa	wa	k?
Muhammadan	Muhammadan	k1gMnSc1
Rasúlu-l-Láhi	Rasúlu-l-Láh	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
arab	arab	k1gMnSc1
<g/>
.	.	kIx.
ل	ل	k?
إ	إ	k?
إ	إ	k?
ا	ا	k?
و	و	k?
ر	ر	k?
ا	ا	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
česky	česky	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Není	být	k5eNaImIp3nS
boha	bůh	k1gMnSc4
kromě	kromě	k7c2
Boha	bůh	k1gMnSc2
a	a	k8xC
Mohamed	Mohamed	k1gMnSc1
je	být	k5eAaImIp3nS
Posel	posel	k1gMnSc1
Boží	boží	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
Toto	tento	k3xDgNnSc1
krátké	krátké	k1gNnSc1
vyznání	vyznání	k1gNnSc2
svou	svůj	k3xOyFgFnSc7
první	první	k4xOgFnSc7
částí	část	k1gFnSc7
řadí	řadit	k5eAaImIp3nS
islám	islám	k1gInSc1
k	k	k7c3
monoteistickému	monoteistický	k2eAgNnSc3d1
náboženství	náboženství	k1gNnSc3
a	a	k8xC
dále	daleko	k6eAd2
poukazuje	poukazovat	k5eAaImIp3nS
na	na	k7c4
Mohamedovo	Mohamedův	k2eAgNnSc4d1
prorocké	prorocký	k2eAgNnSc4d1
poslání	poslání	k1gNnSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
zahrnuje	zahrnovat	k5eAaImIp3nS
víru	víra	k1gFnSc4
ve	v	k7c4
zjevený	zjevený	k2eAgInSc4d1
Korán	korán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
chce	chtít	k5eAaImIp3nS
nevěřící	věřící	k2eNgMnSc1d1
stát	stát	k5eAaImF,k5eAaPmF
muslimem	muslim	k1gMnSc7
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
pronést	pronést	k5eAaPmF
šahádu	šaháda	k1gFnSc4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
zpravidla	zpravidla	k6eAd1
před	před	k7c7
významným	významný	k2eAgMnSc7d1
islámským	islámský	k2eAgMnSc7d1
činitelem	činitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stačí	stačit	k5eAaBmIp3nS
ji	on	k3xPp3gFnSc4
však	však	k9
vyslovit	vyslovit	k5eAaPmF
v	v	k7c6
přítomnosti	přítomnost	k1gFnSc6
dvou	dva	k4xCgMnPc2
svědků	svědek	k1gMnPc2
a	a	k8xC
pořídit	pořídit	k5eAaPmF
úřední	úřední	k2eAgInSc4d1
záznam	záznam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tom	ten	k3xDgNnSc6
je	být	k5eAaImIp3nS
samozřejmě	samozřejmě	k6eAd1
důležitá	důležitý	k2eAgFnSc1d1
upřímnost	upřímnost	k1gFnSc1
a	a	k8xC
poctivost	poctivost	k1gFnSc1
záměru	záměr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Modlitba	modlitba	k1gFnSc1
(	(	kIx(
<g/>
salát	salát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Věřící	věřící	k1gFnSc1
v	v	k7c6
mešitě	mešita	k1gFnSc6
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
v	v	k7c6
Turecku	Turecko	k1gNnSc6
</s>
<s>
Představuje	představovat	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc4d1
<g/>
,	,	kIx,
každodenní	každodenní	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
muslima	muslim	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
pokorný	pokorný	k2eAgInSc4d1
<g/>
,	,	kIx,
čistě	čistě	k6eAd1
bohabojný	bohabojný	k2eAgInSc1d1
projev	projev	k1gInSc1
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
spojována	spojovat	k5eAaImNgFnS
se	s	k7c7
snahou	snaha	k1gFnSc7
vyprosit	vyprosit	k5eAaPmF
si	se	k3xPyFc3
od	od	k7c2
Boha	bůh	k1gMnSc2
cokoliv	cokoliv	k3yInSc1
jiného	jiný	k2eAgNnSc2d1
než	než	k8xS
správné	správný	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosebnou	prosebný	k2eAgFnSc4d1
modlitbu	modlitba	k1gFnSc4
<g/>
,	,	kIx,
bližší	blízký	k2eAgNnSc1d2
křesťanskému	křesťanský	k2eAgNnSc3d1
pojetí	pojetí	k1gNnSc3
<g/>
,	,	kIx,
islám	islám	k1gInSc1
označuje	označovat	k5eAaImIp3nS
termínem	termín	k1gInSc7
du	du	k?
<g/>
'	'	kIx"
<g/>
á.	á.	k?
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
projevem	projev	k1gInSc7
čistě	čistě	k6eAd1
soukromým	soukromý	k2eAgMnSc7d1
a	a	k8xC
není	být	k5eNaImIp3nS
vázána	vázat	k5eAaImNgFnS
žádnými	žádný	k3yNgInPc7
předpisy	předpis	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závaznou	závazný	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
vůči	vůči	k7c3
Bohu	bůh	k1gMnSc3
představuje	představovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
modlitba	modlitba	k1gFnSc1
salát	salát	k1gInSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
provádět	provádět	k5eAaImF
podle	podle	k7c2
tradice	tradice	k1gFnSc2
a	a	k8xC
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
místo	místo	k1gNnSc4
a	a	k8xC
národnost	národnost	k1gFnSc4
věřících	věřící	k1gMnPc2
vždy	vždy	k6eAd1
pouze	pouze	k6eAd1
v	v	k7c6
arabštině	arabština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modlitba	modlitba	k1gFnSc1
je	být	k5eAaImIp3nS
platná	platný	k2eAgFnSc1d1
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
splněny	splnit	k5eAaPmNgFnP
stanovené	stanovený	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
<g/>
:	:	kIx,
čistota	čistota	k1gFnSc1
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
čistota	čistota	k1gFnSc1
místa	místo	k1gNnSc2
a	a	k8xC
oděvu	oděv	k1gInSc2
<g/>
,	,	kIx,
náležité	náležitý	k2eAgNnSc1d1
oblečení	oblečení	k1gNnSc1
<g/>
,	,	kIx,
zaujetí	zaujetí	k1gNnSc1
správného	správný	k2eAgInSc2d1
modlitebního	modlitební	k2eAgInSc2d1
směru	směr	k1gInSc2
<g/>
,	,	kIx,
znalost	znalost	k1gFnSc1
modlitebních	modlitební	k2eAgInPc2d1
časů	čas	k1gInPc2
a	a	k8xC
zbožné	zbožný	k2eAgNnSc4d1
předsevzetí	předsevzetí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Modlitba	modlitba	k1gFnSc1
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
relativně	relativně	k6eAd1
přesnou	přesný	k2eAgFnSc7d1
právní	právní	k2eAgFnSc7d1
normou	norma	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
detailech	detail	k1gInPc6
se	se	k3xPyFc4
modlitba	modlitba	k1gFnSc1
liší	lišit	k5eAaImIp3nS
podle	podle	k7c2
právního	právní	k2eAgInSc2d1
mazhabu	mazhab	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předepsaná	předepsaný	k2eAgFnSc1d1
almužna	almužna	k1gFnSc1
(	(	kIx(
<g/>
zakát	zakát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Instituce	instituce	k1gFnSc1
povinné	povinný	k2eAgFnSc2d1
almužny	almužna	k1gFnSc2
či	či	k8xC
náboženské	náboženský	k2eAgFnSc2d1
daně	daň	k1gFnSc2
určené	určený	k2eAgFnPc1d1
na	na	k7c4
obecně	obecně	k6eAd1
prospěšné	prospěšný	k2eAgInPc4d1
a	a	k8xC
dobročinné	dobročinný	k2eAgInPc4d1
účely	účel	k1gInPc4
se	se	k3xPyFc4
v	v	k7c6
medínské	medínský	k2eAgFnSc6d1
obci	obec	k1gFnSc6
vyvíjely	vyvíjet	k5eAaImAgInP
postupně	postupně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důraz	důraz	k1gInSc1
na	na	k7c4
záslužnost	záslužnost	k1gFnSc4
dobročinnosti	dobročinnost	k1gFnSc2
provázel	provázet	k5eAaImAgMnS
islámské	islámský	k2eAgNnSc1d1
učení	učení	k1gNnSc1
od	od	k7c2
samotného	samotný	k2eAgInSc2d1
počátku	počátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Almužna	almužna	k1gFnSc1
se	se	k3xPyFc4
proměnila	proměnit	k5eAaPmAgFnS
na	na	k7c4
daň	daň	k1gFnSc4
hned	hned	k6eAd1
po	po	k7c6
Mohamedově	Mohamedův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
spolu	spolu	k6eAd1
s	s	k7c7
rozmachem	rozmach	k1gInSc7
centralizované	centralizovaný	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
chalífátu	chalífát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradice	tradice	k1gFnSc1
připisuje	připisovat	k5eAaImIp3nS
pravověrným	pravověrný	k2eAgMnPc3d1
Chalífům	chalífa	k1gMnPc3
podrobné	podrobný	k2eAgNnSc4d1
fiskální	fiskální	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
právní	právní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
s	s	k7c7
drobnými	drobný	k2eAgInPc7d1
rozdíly	rozdíl	k1gInPc7
podle	podle	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
právních	právní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
kodifikovala	kodifikovat	k5eAaBmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
klasického	klasický	k2eAgNnSc2d1
práva	právo	k1gNnSc2
se	se	k3xPyFc4
zakát	zakát	k1gInSc1
odvádí	odvádět	k5eAaImIp3nS
každoročně	každoročně	k6eAd1
z	z	k7c2
určených	určený	k2eAgInPc2d1
druhů	druh	k1gInPc2
majetku	majetek	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
překračují	překračovat	k5eAaImIp3nP
stanovenou	stanovený	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
zakát	zakát	k1gInSc1
splynul	splynout	k5eAaPmAgInS
se	s	k7c7
státními	státní	k2eAgFnPc7d1
daněmi	daň	k1gFnPc7
a	a	k8xC
ztratil	ztratit	k5eAaPmAgMnS
bezprostřední	bezprostřední	k2eAgInSc4d1
charitativní	charitativní	k2eAgInSc4d1
smysl	smysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k6eAd1
si	se	k3xPyFc3
zakát	zakát	k5eAaPmF
ve	v	k7c6
vědomí	vědomí	k1gNnSc6
širokých	široký	k2eAgFnPc2d1
vrstev	vrstva	k1gFnPc2
věřících	věřící	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
uchoval	uchovat	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
tvář	tvář	k1gFnSc4
jednoho	jeden	k4xCgInSc2
z	z	k7c2
pilířů	pilíř	k1gInPc2
náboženství	náboženství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Půst	půst	k1gInSc1
v	v	k7c6
měsíci	měsíc	k1gInSc6
ramadán	ramadán	k1gInSc1
(	(	kIx(
<g/>
sawm	sawm	k1gInSc1
<g/>
,	,	kIx,
sijám	sijat	k5eAaPmIp1nS,k5eAaImIp1nS,k5eAaBmIp1nS
<g/>
)	)	kIx)
</s>
<s>
Svátek	svátek	k1gInSc4
přerušení	přerušení	k1gNnSc2
půstu	půst	k1gInSc2
v	v	k7c6
Maroku	Maroko	k1gNnSc6
</s>
<s>
Třicetidenním	třicetidenní	k2eAgInSc7d1
půst	půst	k1gInSc4
v	v	k7c6
měsíci	měsíc	k1gInSc6
ramadán	ramadán	k1gInSc1
zavedl	zavést	k5eAaPmAgMnS
Mohamed	Mohamed	k1gMnSc1
v	v	k7c6
Medíně	Medína	k1gFnSc6
na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
624	#num#	k4
namísto	namísto	k7c2
jednodenního	jednodenní	k2eAgInSc2d1
půstu	půst	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
připomínal	připomínat	k5eAaImAgInS
starší	starší	k1gMnPc4
<g/>
,	,	kIx,
židovský	židovský	k2eAgInSc4d1
vzor	vzor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půst	půst	k1gInSc1
od	od	k7c2
jídla	jídlo	k1gNnSc2
a	a	k8xC
pití	pití	k1gNnSc2
platí	platit	k5eAaImIp3nS
pouze	pouze	k6eAd1
pro	pro	k7c4
denní	denní	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povinnosti	povinnost	k1gFnSc2
jsou	být	k5eAaImIp3nP
zbaveni	zbaven	k2eAgMnPc1d1
staří	starý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
nemocní	nemocný	k1gMnPc1
<g/>
,	,	kIx,
těhotné	těhotný	k2eAgFnPc1d1
a	a	k8xC
kojící	kojící	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
konají	konat	k5eAaImIp3nP
těžkou	těžký	k2eAgFnSc4d1
fyzickou	fyzický	k2eAgFnSc4d1
práci	práce	k1gFnSc4
a	a	k8xC
lidé	člověk	k1gMnPc1
na	na	k7c6
cestách	cesta	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
půstu	půst	k1gInSc2
se	se	k3xPyFc4
mají	mít	k5eAaImIp3nP
věřící	věřící	k2eAgMnPc1d1
také	také	k6eAd1
vyvarovat	vyvarovat	k5eAaPmF
neshod	neshoda	k1gFnPc2
<g/>
,	,	kIx,
násilí	násilí	k1gNnSc2
<g/>
,	,	kIx,
neslušných	slušný	k2eNgFnPc2d1
řečí	řeč	k1gFnPc2
a	a	k8xC
pohlavního	pohlavní	k2eAgInSc2d1
styku	styk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
během	během	k7c2
dne	den	k1gInSc2
chovat	chovat	k5eAaImF
bohabojně	bohabojně	k6eAd1
a	a	k8xC
má	mít	k5eAaImIp3nS
konat	konat	k5eAaImF
dobro	dobro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
ramadánu	ramadán	k1gInSc2
jsou	být	k5eAaImIp3nP
noční	noční	k2eAgFnPc1d1
rodinné	rodinný	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současných	současný	k2eAgInPc6d1
poměrech	poměr	k1gInPc6
je	být	k5eAaImIp3nS
dodržování	dodržování	k1gNnSc1
půstu	půst	k1gInSc2
všemi	všecek	k3xTgMnPc7
obyvateli	obyvatel	k1gMnPc7
nemožné	nemožná	k1gFnSc2
vzhledem	vzhled	k1gInSc7
k	k	k7c3
chodu	chod	k1gInSc3
hospodářství	hospodářství	k1gNnSc2
a	a	k8xC
bezpečnosti	bezpečnost	k1gFnSc2
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pouť	pouť	k1gFnSc1
do	do	k7c2
Mekky	Mekka	k1gFnSc2
(	(	kIx(
<g/>
hadždž	hadždž	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Posvátné	posvátný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
všech	všecek	k3xTgMnPc2
muslimů	muslim	k1gMnPc2
–	–	k?
mešita	mešita	k1gFnSc1
al-Masdžid	al-Masdžid	k1gInSc1
al-Harám	al-Hara	k1gFnPc3
v	v	k7c6
Mekce	Mekka	k1gFnSc6
a	a	k8xC
svatyně	svatyně	k1gFnSc1
Kaaba	Kaaba	k1gFnSc1
</s>
<s>
Povinnost	povinnost	k1gFnSc1
vykonat	vykonat	k5eAaPmF
alespoň	alespoň	k9
jednou	jeden	k4xCgFnSc7
za	za	k7c4
život	život	k1gInSc4
pouť	pouť	k1gFnSc4
do	do	k7c2
Mekky	Mekka	k1gFnSc2
ukládá	ukládat	k5eAaImIp3nS
všem	všecek	k3xTgMnPc3
dospělým	dospělí	k1gMnPc3
muslimům	muslim	k1gMnPc3
<g/>
,	,	kIx,
mužům	muž	k1gMnPc3
i	i	k8xC
ženám	žena	k1gFnPc3
<g/>
,	,	kIx,
koránský	koránský	k2eAgInSc1d1
příkaz	příkaz	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
97	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povinnost	povinnost	k1gFnSc1
platí	platit	k5eAaImIp3nS
pro	pro	k7c4
každého	každý	k3xTgMnSc4
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
tělesně	tělesně	k6eAd1
a	a	k8xC
duševně	duševně	k6eAd1
zdravý	zdravý	k2eAgMnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
prostředky	prostředek	k1gInPc4
nutné	nutný	k2eAgInPc4d1
pro	pro	k7c4
cestu	cesta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
pouť	pouť	k1gFnSc4
má	mít	k5eAaImIp3nS
nejdříve	dříve	k6eAd3
doma	doma	k6eAd1
všestranně	všestranně	k6eAd1
uspořádat	uspořádat	k5eAaPmF
materiální	materiální	k2eAgInPc4d1
poměry	poměr	k1gInPc4
<g/>
,	,	kIx,
zaplatit	zaplatit	k5eAaPmF
případné	případný	k2eAgInPc4d1
dluhy	dluh	k1gInPc4
a	a	k8xC
zabezpečit	zabezpečit	k5eAaPmF
rodinu	rodina	k1gFnSc4
po	po	k7c4
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
nepřítomnosti	nepřítomnost	k1gFnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
odčinit	odčinit	k5eAaPmF
různé	různý	k2eAgNnSc4d1
minulé	minulý	k2eAgNnSc4d1
provinění	provinění	k1gNnSc4
a	a	k8xC
učinit	učinit	k5eAaPmF,k5eAaImF
upřímné	upřímný	k2eAgNnSc4d1
předsevzetí	předsevzetí	k1gNnSc4
žít	žít	k5eAaImF
nadále	nadále	k6eAd1
ctnostněji	ctnostně	k6eAd2
vůči	vůči	k7c3
Bohu	bůh	k1gMnSc3
i	i	k8xC
lidem	člověk	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Rituál	rituál	k1gInSc4
poutě	pouť	k1gFnSc2
určil	určit	k5eAaPmAgMnS
Mohamed	Mohamed	k1gMnSc1
<g/>
,	,	kIx,
ovlivněný	ovlivněný	k2eAgInSc1d1
arabskými	arabský	k2eAgInPc7d1
kulty	kult	k1gInPc7
i	i	k8xC
židovským	židovský	k2eAgInSc7d1
a	a	k8xC
křesťanským	křesťanský	k2eAgInSc7d1
monoteismem	monoteismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dal	dát	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
však	však	k8xC
nový	nový	k2eAgInSc1d1
ideový	ideový	k2eAgInSc1d1
obsah	obsah	k1gInSc1
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
novým	nový	k2eAgNnSc7d1
učením	učení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohamed	Mohamed	k1gMnSc1
nakonec	nakonec	k9
udělal	udělat	k5eAaPmAgMnS
z	z	k7c2
Káby	kába	k1gFnSc2
střed	střed	k1gInSc1
Dár	Dár	k1gFnSc4
al-islám	al-islat	k5eAaImIp1nS,k5eAaPmIp1nS
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
původně	původně	k6eAd1
uvažoval	uvažovat	k5eAaImAgMnS
o	o	k7c6
místě	místo	k1gNnSc6
v	v	k7c6
Medíně	Medína	k1gFnSc6
nebo	nebo	k8xC
Jeruzalémě	Jeruzalém	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uctívání	uctívání	k1gNnSc6
Káby	kába	k1gFnSc2
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
tradicí	tradice	k1gFnSc7
Ibrahimovou	Ibrahimová	k1gFnSc7
(	(	kIx(
<g/>
Abrahám	Abrahám	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
naklonil	naklonit	k5eAaPmAgMnS
hidžázské	hidžázský	k2eAgMnPc4d1
židy	žid	k1gMnPc4
a	a	k8xC
křesťany	křesťan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouť	pouť	k1gFnSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
mezi	mezi	k7c7
7	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
.	.	kIx.
dnem	dnem	k7c2
měsíce	měsíc	k1gInSc2
dhú	dhú	k?
<g/>
'	'	kIx"
<g/>
l-hidždža	l-hidždža	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
Mekce	Mekka	k1gFnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
provádí	provádět	k5eAaImIp3nS
složitý	složitý	k2eAgInSc1d1
kanonizovaný	kanonizovaný	k2eAgInSc1d1
rituál	rituál	k1gInSc1
za	za	k7c2
účasti	účast	k1gFnSc2
až	až	k9
dvou	dva	k4xCgInPc2
milionů	milion	k4xCgInPc2
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mystika	mystika	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Súfismus	súfismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tančící	tančící	k2eAgMnPc1d1
dervišové	derviš	k1gMnPc1
ve	v	k7c6
městě	město	k1gNnSc6
Konya	Kony	k1gInSc2
v	v	k7c6
Turecku	Turecko	k1gNnSc6
u	u	k7c2
Rúmího	Rúmí	k1gMnSc2
hrobky	hrobka	k1gFnSc2
</s>
<s>
Jako	jako	k8xC,k8xS
duchovní	duchovní	k2eAgInSc1d1
protipól	protipól	k1gInSc1
k	k	k7c3
striktním	striktní	k2eAgInSc7d1
příkazem	příkaz	k1gInSc7
šaríi	šarí	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c6
půdě	půda	k1gFnSc6
islámu	islám	k1gInSc2
rozvinula	rozvinout	k5eAaPmAgFnS
mystika	mystika	k1gFnSc1
(	(	kIx(
<g/>
súfismus	súfismus	k1gInSc1
<g/>
,	,	kIx,
arab	arab	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tasawwuf	Tasawwuf	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projevovala	projevovat	k5eAaImAgFnS
se	se	k3xPyFc4
větší	veliký	k2eAgFnSc7d2
zbožností	zbožnost	k1gFnSc7
než	než	k8xS
požaduje	požadovat	k5eAaImIp3nS
šaría	šaría	k6eAd1
a	a	k8xC
askezí	askeze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
súfisté	súfista	k1gMnPc1
vytvořili	vytvořit	k5eAaPmAgMnP
systémy	systém	k1gInPc4
duchovních	duchovní	k2eAgFnPc2d1
<g/>
,	,	kIx,
teoretických	teoretický	k2eAgFnPc2d1
i	i	k8xC
praktických	praktický	k2eAgFnPc2d1
nauk	nauka	k1gFnPc2
(	(	kIx(
<g/>
Batin	Batin	k1gMnSc1
<g/>
,	,	kIx,
Záhir	Záhir	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vedle	vedle	k7c2
rozumového	rozumový	k2eAgNnSc2d1
poznání	poznání	k1gNnSc2
postavili	postavit	k5eAaPmAgMnP
intuici	intuice	k1gFnSc4
a	a	k8xC
poznatky	poznatek	k1gInPc1
získávané	získávaný	k2eAgInPc1d1
srdcem	srdce	k1gNnSc7
<g/>
,	,	kIx,
své	svůj	k3xOyFgNnSc4
učení	učení	k1gNnSc4
vykládali	vykládat	k5eAaImAgMnP
také	také	k9
pomocí	pomocí	k7c2
poetických	poetický	k2eAgInPc2d1
obrazů	obraz	k1gInPc2
a	a	k8xC
do	do	k7c2
bohoslužby	bohoslužba	k1gFnSc2
či	či	k8xC
modlitby	modlitba	k1gFnSc2
(	(	kIx(
<g/>
wird	wird	k6eAd1
<g/>
)	)	kIx)
vkládali	vkládat	k5eAaImAgMnP
hudbu	hudba	k1gFnSc4
a	a	k8xC
tanec	tanec	k1gInSc4
(	(	kIx(
<g/>
samá	samý	k3xTgFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc4
mystika	mystika	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
představou	představa	k1gFnSc7
o	o	k7c6
jedinosti	jedinost	k1gFnSc6
Boha	bůh	k1gMnSc2
jako	jako	k8xC,k8xS
jednotě	jednota	k1gFnSc3
všeho	všecek	k3xTgNnSc2
bytí	bytí	k1gNnSc2
(	(	kIx(
<g/>
Ibn	Ibn	k1gFnSc1
Arabí	Arabí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
súfisté	súfista	k1gMnPc1
sdružovali	sdružovat	k5eAaImAgMnP
do	do	k7c2
zřetelně	zřetelně	k6eAd1
vymezených	vymezený	k2eAgNnPc2d1
mystických	mystický	k2eAgNnPc2d1
bratrstev	bratrstvo	k1gNnPc2
a	a	k8xC
řádů	řád	k1gInPc2
(	(	kIx(
<g/>
Tarik	Tarik	k1gInSc1
<g/>
)	)	kIx)
pod	pod	k7c7
vedením	vedení	k1gNnSc7
charismatických	charismatický	k2eAgMnPc2d1
šejků	šejk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Súfijské	Súfijský	k2eAgInPc1d1
Tariky	Tarik	k1gInPc1
tvořily	tvořit	k5eAaImAgInP
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
hlavní	hlavní	k2eAgFnSc4d1
formu	forma	k1gFnSc4
islámské	islámský	k2eAgFnSc2d1
spirituality	spiritualita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Orientalistika	orientalistika	k1gFnSc1
na	na	k7c6
tomto	tento	k3xDgNnSc6
širokém	široký	k2eAgNnSc6d1
poli	pole	k1gNnSc6
životních	životní	k2eAgInPc2d1
postojů	postoj	k1gInPc2
<g/>
,	,	kIx,
idejí	idea	k1gFnPc2
a	a	k8xC
duševních	duševní	k2eAgInPc2d1
i	i	k8xC
tělesných	tělesný	k2eAgInPc2d1
cviků	cvik	k1gInPc2
rozpoznává	rozpoznávat	k5eAaImIp3nS
vlivy	vliv	k1gInPc1
křesťanství	křesťanství	k1gNnSc2
(	(	kIx(
<g/>
naznačeno	naznačit	k5eAaPmNgNnS
již	již	k6eAd1
v	v	k7c6
Koránu	korán	k1gInSc6
57	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
gnóze	gnóze	k1gFnSc2
<g/>
,	,	kIx,
novoplatónské	novoplatónský	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
(	(	kIx(
<g/>
rozpracované	rozpracovaný	k2eAgNnSc1d1
arabskou	arabský	k2eAgFnSc7d1
falsafou	falsafa	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
židovské	židovský	k2eAgFnSc2d1
kabaly	kabala	k1gFnSc2
<g/>
,	,	kIx,
indické	indický	k2eAgFnSc2d1
védanty	védanta	k1gFnSc2
a	a	k8xC
jógy	jóga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
různých	různý	k2eAgFnPc6d1
podobách	podoba	k1gFnPc6
zdrženlivého	zdrženlivý	k2eAgMnSc2d1
<g/>
,	,	kIx,
osamoceného	osamocený	k2eAgInSc2d1
života	život	k1gInSc2
svědčí	svědčit	k5eAaImIp3nS
i	i	k9
množství	množství	k1gNnSc1
jmen	jméno	k1gNnPc2
dávaných	dávaný	k2eAgFnPc2d1
askety	asketa	k1gMnSc2
<g/>
:	:	kIx,
záhid	záhid	k1gInSc1
<g/>
,	,	kIx,
násik	násik	k1gInSc1
<g/>
;	;	kIx,
bakká	bakká	k1gFnSc1
<g/>
´	´	k?
"	"	kIx"
<g/>
plačlivec	plačlivec	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
faqír	faqír	k1gMnSc1
"	"	kIx"
<g/>
chudák	chudák	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
qári	qári	k1gNnSc1
<g/>
´	´	k?
horlivý	horlivý	k2eAgInSc1d1
"	"	kIx"
<g/>
čitatel	čitatel	k1gInSc1
<g/>
"	"	kIx"
Koránu	korán	k1gInSc2
<g/>
,	,	kIx,
qáss	qáss	k6eAd1
"	"	kIx"
potulný	potulný	k2eAgMnSc1d1
kazateľ	kazateľ	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
persky	persky	k6eAd1
devíš	devit	k5eAaBmIp2nS,k5eAaImIp2nS,k5eAaPmIp2nS
"	"	kIx"
žebrák	žebrák	k1gMnSc1
<g/>
"	"	kIx"
atď	atď	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
v	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
někteří	některý	k3yIgMnPc1
začali	začít	k5eAaPmAgMnP
sdružovat	sdružovat	k5eAaImF
do	do	k7c2
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nich	on	k3xPp3gInPc2
nejvýznamnější	významný	k2eAgFnPc1d3
byly	být	k5eAaImAgFnP
zřízeny	zřízen	k2eAgFnPc1d1
-	-	kIx~
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
většiny	většina	k1gFnSc2
islámských	islámský	k2eAgInPc2d1
vědních	vědní	k2eAgInPc2d1
oborů	obor	k1gInPc2
-	-	kIx~
v	v	k7c6
jihoiráckých	jihoirácký	k2eAgNnPc6d1
městech	město	k1gNnPc6
Basra	Basro	k1gNnSc2
a	a	k8xC
Kúfa	Kúfum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevil	objevit	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
výraz	výraz	k1gInSc1
súfí	súfí	k2eAgInSc1d1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
mystik	mystik	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Súfijští	Súfijský	k2eAgMnPc1d1
dervišové	derviš	k1gMnPc1
v	v	k7c6
Súdánu	Súdán	k1gInSc6
</s>
<s>
Konformní	konformní	k2eAgInSc1d1
islám	islám	k1gInSc1
byl	být	k5eAaImAgInS
kolektivní	kolektivní	k2eAgFnSc7d1
vírou	víra	k1gFnSc7
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
islámská	islámský	k2eAgFnSc1d1
mystika	mystika	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vyjádřením	vyjádření	k1gNnSc7
víry	víra	k1gFnSc2
jednotlivce	jednotlivec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otevřela	otevřít	k5eAaPmAgFnS
prostor	prostor	k1gInSc4
k	k	k7c3
individuálnímu	individuální	k2eAgNnSc3d1
hledání	hledání	k1gNnSc3
přímé	přímý	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
k	k	k7c3
Bohu	bůh	k1gMnSc3
bez	bez	k7c2
prostředníka	prostředník	k1gMnSc2
a	a	k8xC
poskytla	poskytnout	k5eAaPmAgFnS
člověku	člověk	k1gMnSc3
jinde	jinde	k6eAd1
nevídanou	vídaný	k2eNgFnSc4d1
tvůrčí	tvůrčí	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
vysvětlit	vysvětlit	k5eAaPmF
zdánlivá	zdánlivý	k2eAgFnSc1d1
roztříštěnost	roztříštěnost	k1gFnSc1
mystických	mystický	k2eAgNnPc2d1
společenství	společenství	k1gNnPc2
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
však	však	k9
o	o	k7c4
odraz	odraz	k1gInSc4
individuálních	individuální	k2eAgInPc2d1
a	a	k8xC
autentických	autentický	k2eAgInPc2d1
přístupů	přístup	k1gInPc2
ke	k	k7c3
společnému	společný	k2eAgInSc3d1
cíli	cíl	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc4
počátky	počátek	k1gInPc1
byly	být	k5eAaImAgInP
projevem	projev	k1gInSc7
nesouhlasu	nesouhlas	k1gInSc2
se	s	k7c7
způsobem	způsob	k1gInSc7
provádění	provádění	k1gNnSc2
norem	norma	k1gFnPc2
náboženského	náboženský	k2eAgInSc2d1
a	a	k8xC
společenského	společenský	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mystika	mystika	k1gFnSc1
v	v	k7c6
mnohém	mnohé	k1gNnSc6
obcházela	obcházet	k5eAaImAgFnS
omezující	omezující	k2eAgFnPc4d1
zásady	zásada	k1gFnPc4
kodifikovaného	kodifikovaný	k2eAgInSc2d1
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
bývala	bývat	k5eAaImAgFnS
zavrhovaná	zavrhovaný	k2eAgFnSc1d1
a	a	k8xC
pronásledována	pronásledován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
tohoto	tento	k3xDgNnSc2
členění	členění	k1gNnSc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
etablovaly	etablovat	k5eAaBmAgFnP
další	další	k2eAgInPc4d1
mystické	mystický	k2eAgInPc4d1
směry	směr	k1gInPc4
a	a	k8xC
na	na	k7c4
ně	on	k3xPp3gNnPc4
navazující	navazující	k2eAgNnPc4d1
bratrstva	bratrstvo	k1gNnPc4
(	(	kIx(
<g/>
čištíja	čištíja	k1gFnSc1
<g/>
,	,	kIx,
qádiríja	qádiríjus	k1gMnSc4
<g/>
,	,	kIx,
maulávíja	maulávíjus	k1gMnSc4
<g/>
,	,	kIx,
naqšbandíja	naqšbandíjus	k1gMnSc4
<g/>
,	,	kIx,
muhammadíja	muhammadíjus	k1gMnSc4
<g/>
,	,	kIx,
rifáíja	rifáíjus	k1gMnSc4
<g/>
,	,	kIx,
šadhilíja	šadhilíjus	k1gMnSc4
<g/>
,	,	kIx,
tidžáníja	tidžáníj	k2eAgMnSc4d1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
postupnému	postupný	k2eAgInSc3d1
úpadku	úpadek	k1gInSc3
vlivu	vliv	k1gInSc2
mystických	mystický	k2eAgNnPc2d1
bratrstev	bratrstvo	k1gNnPc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
někde	někde	k6eAd1
ještě	ještě	k6eAd1
hrály	hrát	k5eAaImAgInP
výraznou	výrazný	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
rozvoji	rozvoj	k1gInSc6
nápravných	nápravný	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
(	(	kIx(
<g/>
mahdistické	mahdistický	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
v	v	k7c6
Súdánu	Súdán	k1gInSc6
<g/>
,	,	kIx,
sanúsíja	sanúsíja	k6eAd1
v	v	k7c6
Libyi	Libye	k1gFnSc6
<g/>
)	)	kIx)
nebo	nebo	k8xC
odporu	odpor	k1gInSc2
muslimských	muslimský	k2eAgNnPc2d1
etnik	etnikum	k1gNnPc2
vůči	vůči	k7c3
evropskému	evropský	k2eAgInSc3d1
kolonialismu	kolonialismus	k1gInSc3
(	(	kIx(
<g/>
šajch	šajch	k1gMnSc1
Abd	Abd	k1gMnSc1
al-Kádir	al-Kádir	k1gMnSc1
v	v	k7c6
Alžírsku	Alžírsko	k1gNnSc6
<g/>
,	,	kIx,
Imám	imám	k1gMnSc1
Šamil	Šamil	k1gMnSc1
na	na	k7c6
Kavkaze	Kavkaz	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohromou	pohroma	k1gFnSc7
pro	pro	k7c4
existenci	existence	k1gFnSc4
bratrstev	bratrstvo	k1gNnPc2
byly	být	k5eAaImAgFnP
kemalistické	kemalistický	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
v	v	k7c6
Turecku	Turecko	k1gNnSc6
a	a	k8xC
převzetí	převzetí	k1gNnSc6
moci	moc	k1gFnSc2
na	na	k7c6
islámskými	islámský	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
bolševiky	bolševik	k1gMnPc7
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
menším	malý	k2eAgInSc6d2
rozsahu	rozsah	k1gInSc6
nástup	nástup	k1gInSc4
sekulárních	sekulární	k2eAgInPc2d1
režimů	režim	k1gInPc2
v	v	k7c6
mnoha	mnoho	k4c6
islámských	islámský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oživení	oživení	k1gNnSc1
mystických	mystický	k2eAgNnPc2d1
bratrstev	bratrstvo	k1gNnPc2
přinesly	přinést	k5eAaPmAgFnP
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
globální	globální	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
sekularismu	sekularismus	k1gInSc2
a	a	k8xC
liberalizací	liberalizace	k1gFnSc7
poměrů	poměr	k1gInPc2
v	v	k7c6
bývalých	bývalý	k2eAgFnPc6d1
komunistických	komunistický	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
ze	z	k7c2
sebou	se	k3xPyFc7
přinesly	přinést	k5eAaPmAgFnP
islamizaci	islamizace	k1gFnSc3
veřejného	veřejný	k2eAgInSc2d1
života	život	k1gInSc2
(	(	kIx(
<g/>
země	země	k1gFnSc1
Balkánu	Balkán	k1gInSc2
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
republikánské	republikánský	k2eAgInPc4d1
režimy	režim	k1gInPc4
"	"	kIx"
<g/>
arabského	arabský	k2eAgInSc2d1
socialismu	socialismus	k1gInSc2
<g/>
"	"	kIx"
byly	být	k5eAaImAgFnP
mystické	mystický	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
vždy	vždy	k6eAd1
nežádoucí	žádoucí	k2eNgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nepředstavovaly	představovat	k5eNaImAgFnP
reálné	reálný	k2eAgNnSc4d1
vnitropolitické	vnitropolitický	k2eAgNnSc4d1
nebezpečí	nebezpečí	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
vždy	vždy	k6eAd1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
byly	být	k5eAaImAgFnP
sice	sice	k8xC
nekonformní	konformní	k2eNgFnPc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
nepolitické	politický	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
fundamentalisty	fundamentalista	k1gMnPc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
k	k	k7c3
mystickým	mystický	k2eAgFnPc3d1
praktikám	praktika	k1gFnPc3
části	část	k1gFnSc2
společnosti	společnost	k1gFnSc2
vůbec	vůbec	k9
vyjadřují	vyjadřovat	k5eAaImIp3nP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
sféra	sféra	k1gFnSc1
islámské	islámský	k2eAgFnSc2d1
religiozity	religiozita	k1gFnSc2
jak	jak	k8xS,k8xC
jinak	jinak	k6eAd1
"	"	kIx"
<g/>
společenským	společenský	k2eAgNnSc7d1
bahnem	bahno	k1gNnSc7
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Maudúdí	Maudúdí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
"	"	kIx"
<g/>
rakovinným	rakovinný	k2eAgInSc7d1
vředem	vřed	k1gInSc7
na	na	k7c6
těle	tělo	k1gNnSc6
ummy	umma	k1gFnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Farag	Farag	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
vyoperovat	vyoperovat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sufismus	Sufismus	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
i	i	k8xC
vzory	vzor	k1gInPc4
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
etiku	etika	k1gFnSc4
<g/>
,	,	kIx,
estetiku	estetika	k1gFnSc4
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgFnPc6
formách	forma	k1gFnPc6
společenství	společenství	k1gNnSc2
zasahuje	zasahovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
hospodářského	hospodářský	k2eAgInSc2d1
<g/>
,	,	kIx,
politického	politický	k2eAgInSc2d1
a	a	k8xC
někdy	někdy	k6eAd1
vojenského	vojenský	k2eAgNnSc2d1
úsilí	úsilí	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
za	za	k7c4
hranice	hranice	k1gFnPc4
běžného	běžný	k2eAgNnSc2d1
chápání	chápání	k1gNnSc2
mystiky	mystika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sufismus	Sufismus	k1gInSc1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
označován	označovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
duchovní	duchovní	k2eAgInSc1d1
rozměr	rozměr	k1gInSc1
nebo	nebo	k8xC
spiritualita	spiritualita	k1gFnSc1
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
termín	termín	k1gInSc4
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
používaný	používaný	k2eAgInSc1d1
<g/>
,	,	kIx,
svým	svůj	k3xOyFgInSc7
polysémantickým	polysémantický	k2eAgInSc7d1
dosahem	dosah	k1gInSc7
meze	mez	k1gFnSc2
súfismu	súfismus	k1gInSc2
překračuje	překračovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Islámské	islámský	k2eAgNnSc1d1
učení	učení	k1gNnSc1
</s>
<s>
Korán	korán	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Korán	korán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Studium	studium	k1gNnSc1
Koránu	korán	k1gInSc2
v	v	k7c6
Senegalu	Senegal	k1gInSc6
</s>
<s>
Základní	základní	k2eAgFnSc7d1
knihou	kniha	k1gFnSc7
islámu	islám	k1gInSc2
je	být	k5eAaImIp3nS
Korán	korán	k1gInSc1
<g/>
,	,	kIx,
veršovaná	veršovaný	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
božích	boží	k2eAgNnPc2d1
sdělení	sdělení	k1gNnPc2
věřícím	věřící	k1gMnPc3
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
podle	podle	k7c2
muslimů	muslim	k1gMnPc2
Mohamedovi	Mohamed	k1gMnSc3
nadiktoval	nadiktovat	k5eAaPmAgInS
prostřednictvím	prostřednictvím	k7c2
archanděla	archanděl	k1gMnSc2
Gabriela	Gabriel	k1gMnSc2
sám	sám	k3xTgMnSc1
Bůh	bůh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
ze	z	k7c2
114	#num#	k4
súr	súra	k1gFnPc2
(	(	kIx(
<g/>
skladeb	skladba	k1gFnPc2
–	–	k?
ty	ten	k3xDgMnPc4
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
áje	áje	k?
<g/>
,	,	kIx,
verše	verš	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
čtyř	čtyři	k4xCgNnPc2
období	období	k1gNnPc2
<g/>
:	:	kIx,
První	první	k4xOgNnSc1
mekkánské	mekkánský	k2eAgNnSc1d1
období	období	k1gNnSc1
<g/>
,	,	kIx,
Druhé	druhý	k4xOgNnSc1
mekkánské	mekkánský	k2eAgNnSc1d1
období	období	k1gNnSc1
<g/>
,	,	kIx,
Třetí	třetí	k4xOgNnSc1
mekkánské	mekkánský	k2eAgNnSc1d1
období	období	k1gNnSc1
a	a	k8xC
Medínské	medínský	k2eAgNnSc1d1
období	období	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
muslimů	muslim	k1gMnPc2
kniha	kniha	k1gFnSc1
Korán	korán	k1gInSc4
představuje	představovat	k5eAaImIp3nS
uzavření	uzavření	k1gNnSc4
a	a	k8xC
zpřesnění	zpřesnění	k1gNnSc4
poselství	poselství	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
lidem	lid	k1gInSc7
dáno	dán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřívější	dřívější	k2eAgInPc1d1
poselství	poselství	k1gNnSc4
v	v	k7c6
Bibli	bible	k1gFnSc6
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
dle	dle	k7c2
muslimské	muslimský	k2eAgFnSc2d1
víry	víra	k1gFnSc2
<g/>
,	,	kIx,
lidmi	člověk	k1gMnPc7
pokřiveno	pokřiven	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Muslimové	muslim	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Korán	korán	k1gInSc1
byl	být	k5eAaImAgInS
dán	dát	k5eAaPmNgInS
muslimům	muslim	k1gMnPc3
jako	jako	k9
dokonalý	dokonalý	k2eAgMnSc1d1
a	a	k8xC
navždy	navždy	k6eAd1
neměnný	neměnný	k2eAgInSc1d1,k2eNgInSc1d1
text	text	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
pak	pak	k6eAd1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
interpretován	interpretovat	k5eAaBmNgInS
v	v	k7c6
původním	původní	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
např.	např.	kA
od	od	k7c2
Bible	bible	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
vykládána	vykládat	k5eAaImNgFnS
dle	dle	k7c2
kontextu	kontext	k1gInSc2
a	a	k8xC
kritického	kritický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomoc	k1gFnPc2
při	při	k7c6
interpretaci	interpretace	k1gFnSc6
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
komentáře	komentář	k1gInPc1
islámských	islámský	k2eAgFnPc2d1
autorit	autorita	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
„	„	k?
<g/>
neotřesitelnost	neotřesitelnost	k1gFnSc1
<g/>
“	“	k?
Koránu	korán	k1gInSc2
zaručuje	zaručovat	k5eAaImIp3nS
islámu	islám	k1gInSc2
pevné	pevný	k2eAgInPc4d1
základy	základ	k1gInPc4
napříč	napříč	k7c7
dějinnými	dějinný	k2eAgFnPc7d1
událostmi	událost	k1gFnPc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
je	být	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
prakticky	prakticky	k6eAd1
potlačena	potlačen	k2eAgFnSc1d1
jakákoliv	jakýkoliv	k3yIgFnSc1
možnost	možnost	k1gFnSc1
spekulace	spekulace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určité	určitý	k2eAgInPc1d1
náznaky	náznak	k1gInPc1
„	„	k?
<g/>
chápání	chápání	k1gNnSc2
v	v	k7c6
kontextu	kontext	k1gInSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
lze	lze	k6eAd1
pozorovat	pozorovat	k5eAaImF
jen	jen	k9
u	u	k7c2
Šíitů	šíita	k1gMnPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
prostor	prostor	k1gInSc1
pro	pro	k7c4
pochybování	pochybování	k1gNnSc4
je	být	k5eAaImIp3nS
i	i	k9
zde	zde	k6eAd1
naprosto	naprosto	k6eAd1
minimální	minimální	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
vysvětluje	vysvětlovat	k5eAaImIp3nS
neúspěch	neúspěch	k1gInSc4
uniformní	uniformní	k2eAgFnSc2d1
arabské	arabský	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
(	(	kIx(
<g/>
zaniká	zanikat	k5eAaImIp3nS
již	již	k6eAd1
koncem	koncem	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
proč	proč	k6eAd1
jsou	být	k5eAaImIp3nP
muslimové	muslim	k1gMnPc1
schopní	schopný	k2eAgMnPc1d1
i	i	k8xC
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
odolávat	odolávat	k5eAaImF
kulturní	kulturní	k2eAgFnSc4d1
relativizaci	relativizace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jinak	jinak	k6eAd1
postihla	postihnout	k5eAaPmAgFnS
velkou	velký	k2eAgFnSc4d1
část	část	k1gFnSc4
především	především	k9
křesťanského	křesťanský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
ale	ale	k9
také	také	k9
nastoluje	nastolovat	k5eAaImIp3nS
znepokojivé	znepokojivý	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
<g/>
,	,	kIx,
protože	protože	k8xS
za	za	k7c4
neměnné	neměnný	k2eAgNnSc4d1,k2eNgNnSc4d1
a	a	k8xC
navždy	navždy	k6eAd1
pravdivé	pravdivý	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
brány	brána	k1gFnPc1
i	i	k9
velmi	velmi	k6eAd1
kontroverzní	kontroverzní	k2eAgFnPc4d1
pasáže	pasáž	k1gFnPc4
Koránu	korán	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
9	#num#	k4
<g/>
.	.	kIx.
súra	súra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Není	být	k5eNaImIp3nS
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
byl	být	k5eAaImAgMnS
Mohamed	Mohamed	k1gMnSc1
gramotný	gramotný	k2eAgMnSc1d1
a	a	k8xC
jakého	jaký	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dostalo	dostat	k5eAaPmAgNnS
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k8xC,k8xS
obchodník	obchodník	k1gMnSc1
pravděpodobně	pravděpodobně	k6eAd1
ovládal	ovládat	k5eAaImAgMnS
alespoň	alespoň	k9
základy	základ	k1gInPc4
čtení	čtení	k1gNnSc2
a	a	k8xC
psaní	psaní	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
počátku	počátek	k1gInSc6
islámu	islám	k1gInSc2
se	se	k3xPyFc4
Korán	korán	k1gInSc1
tradoval	tradovat	k5eAaImAgInS
především	především	k9
ústně	ústně	k6eAd1
a	a	k8xC
měl	mít	k5eAaImAgMnS
tak	tak	k6eAd1
i	i	k9
větší	veliký	k2eAgFnSc4d2
hodnotu	hodnota	k1gFnSc4
než	než	k8xS
písemné	písemný	k2eAgFnPc4d1
verze	verze	k1gFnPc4
(	(	kIx(
<g/>
také	také	k9
kvůli	kvůli	k7c3
nedokonalosti	nedokonalost	k1gFnSc3
arabského	arabský	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neboť	neboť	k8xC
Korán	korán	k1gInSc1
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
primárně	primárně	k6eAd1
k	k	k7c3
recitaci	recitace	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Oficiální	oficiální	k2eAgFnSc1d1
písemná	písemný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
všeobecně	všeobecně	k6eAd1
uznávaná	uznávaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
vznikla	vzniknout	k5eAaPmAgFnS
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
656	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
po	po	k7c6
smrti	smrt	k1gFnSc6
Proroka	prorok	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
tzv.	tzv.	kA
Uthmánskou	Uthmánský	k2eAgFnSc7d1
redakcí	redakce	k1gFnSc7
<g/>
,	,	kIx,
zásluhou	zásluhou	k7c2
prvních	první	k4xOgMnPc2
chalífů	chalífa	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
komisi	komise	k1gFnSc4
stáli	stát	k5eAaImAgMnP
např.	např.	kA
<g/>
:	:	kIx,
Zajdn	Zajdn	k1gMnSc1
ibn	ibn	k?
Thábit	Thábit	k1gMnSc1
(	(	kIx(
<g/>
jehož	jehož	k3xOyRp3gFnSc1
verze	verze	k1gFnSc1
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
především	především	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Saíd	Saíd	k1gInSc1
ibn	ibn	k?
al-Ás	al-Ás	k1gInSc1
<g/>
,	,	kIx,
Abdarrahmán	Abdarrahmán	k2eAgInSc1d1
ibn	ibn	k?
al-Hárith	al-Hárith	k1gInSc1
a	a	k8xC
Abdalláh	Abdalláh	k1gInSc1
ibn	ibn	k?
az-Zubajr	az-Zubajr	k1gInSc1
(	(	kIx(
<g/>
Kurajšovci	Kurajšovec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
plně	plně	k6eAd1
autentickou	autentický	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
Koránu	korán	k1gInSc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
výlučně	výlučně	k6eAd1
ta	ten	k3xDgFnSc1
arabská	arabský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlady	překlad	k1gInPc4
však	však	k8xC
nejsou	být	k5eNaImIp3nP
zakázány	zakázat	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
existovaly	existovat	k5eAaImAgFnP
a	a	k8xC
dochovaly	dochovat	k5eAaPmAgFnP
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc1
sbírky-verze	sbírky-verze	k1gFnPc1
Koránu	korán	k1gInSc2
(	(	kIx(
<g/>
kodexy	kodex	k1gInPc4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Ubajje	Ubajje	k1gFnSc1
ibn	ibn	k?
Kaba	Kaba	k1gFnSc1
(	(	kIx(
<g/>
Sýrie	Sýrie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Abdalláha	Abdalláha	k1gMnSc1
ibn	ibn	k?
Masúda	Masúda	k1gMnSc1
(	(	kIx(
<g/>
Kúfa	Kúfa	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Abú	abú	k1gMnSc1
Músy	Músa	k1gFnSc2
al-	al-	k?
Ašarího	Ašarí	k2eAgInSc2d1
(	(	kIx(
<g/>
Basra	Basr	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
Miqdada	Miqdada	k1gFnSc1
ibn	ibn	k?
Amra	Amra	k1gFnSc1
(	(	kIx(
<g/>
Hims	Hims	k1gInSc1
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hadísy	Hadísa	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Hadís	Hadísa	k1gFnPc2
a	a	k8xC
Sunna	sunna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Výklad	výklad	k1gInSc1
40	#num#	k4
hadísů	hadís	k1gMnPc2
Nawawi	Nawaw	k1gFnSc2
v	v	k7c6
mešitě	mešita	k1gFnSc6
sultána	sultán	k1gMnSc2
Hassana	Hassan	k1gMnSc2
v	v	k7c6
Káhiře	Káhira	k1gFnSc6
</s>
<s>
Hadísy	Hadís	k1gInPc1
obsahují	obsahovat	k5eAaImIp3nP
soubor	soubor	k1gInSc1
učení	učení	k1gNnSc2
<g/>
,	,	kIx,
výroků	výrok	k1gInPc2
a	a	k8xC
činů	čin	k1gInPc2
Mohameda	Mohamed	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
sesbírány	sesbírán	k2eAgInPc1d1
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
vždy	vždy	k6eAd1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
takto	takto	k6eAd1
svědčil	svědčit	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
existuje	existovat	k5eAaImIp3nS
<g/>
-li	-li	k?
více	hodně	k6eAd2
verzí	verze	k1gFnSc7
jedné	jeden	k4xCgFnSc2
události	událost	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
uvedeny	uveden	k2eAgFnPc1d1
všechny	všechen	k3xTgFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hadísy	Hadís	k1gMnPc7
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
pomocný	pomocný	k2eAgInSc1d1
instrument	instrument	k1gInSc1
k	k	k7c3
objasňování	objasňování	k1gNnSc3
nejasných	jasný	k2eNgFnPc2d1
pasáží	pasáž	k1gFnPc2
Koránu	korán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Články	článek	k1gInPc1
víry	víra	k1gFnSc2
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
jediného	jediný	k2eAgMnSc4d1
Boha	bůh	k1gMnSc4
(	(	kIx(
<g/>
ímánu	ímána	k1gFnSc4
bi	bi	k?
lláhi	lláh	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
anděly	anděl	k1gMnPc4
(	(	kIx(
<g/>
malaky	malak	k1gMnPc7
<g/>
)	)	kIx)
i	i	k8xC
džiny	džin	k1gInPc1
(	(	kIx(
<g/>
ímánu	ímán	k1gInSc2
bi	bi	k?
l-malá	l-malý	k2eAgFnSc1d1
<g/>
'	'	kIx"
<g/>
iká	iká	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Víra	víra	k1gFnSc1
ve	v	k7c4
všechny	všechen	k3xTgFnPc4
seslané	seslaný	k2eAgFnPc4d1
Knihy	kniha	k1gFnPc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
starozákonních	starozákonní	k2eAgInPc2d1
spisů	spis	k1gInPc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
ímánu	ímánout	k5eAaImIp1nS,k5eAaPmIp1nS
bi	bi	k?
l-kutub	l-kutub	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
posly	posel	k1gMnPc4
Boží	božit	k5eAaImIp3nS
(	(	kIx(
<g/>
včetně	včetně	k7c2
biblických	biblický	k2eAgFnPc2d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
ímánu	ímánout	k5eAaImIp1nS,k5eAaPmIp1nS
bi	bi	k?
r-rusul	r-rusul	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
soudný	soudný	k2eAgInSc4d1
den	den	k1gInSc4
(	(	kIx(
<g/>
ímánu	ímánout	k5eAaImIp1nS,k5eAaPmIp1nS
bil-jawmi	bil-jaw	k1gFnPc7
l-áchir	l-áchir	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Víra	víra	k1gFnSc1
v	v	k7c4
Osud	osud	k1gInSc4
<g/>
,	,	kIx,
dobrý	dobrý	k2eAgInSc4d1
či	či	k8xC
špatný	špatný	k2eAgInSc4d1
<g/>
,	,	kIx,
že	že	k8xS
všechno	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
s	s	k7c7
vůlí	vůle	k1gFnSc7
Boha	bůh	k1gMnSc2
(	(	kIx(
<g/>
ímánu	ímánout	k5eAaPmIp1nS,k5eAaImIp1nS
bi	bi	k?
l-qadar	l-qadara	k1gFnPc2
<g/>
,	,	kIx,
chajrihi	chajrihi	k1gNnPc2
wa	wa	k?
šarrihi	šarrih	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Islám	islám	k1gInSc1
a	a	k8xC
stát	stát	k1gInSc1
</s>
<s>
Tradiční	tradiční	k2eAgNnSc1d1
islámské	islámský	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
náboženství	náboženství	k1gNnSc2
a	a	k8xC
státu	stát	k1gInSc2
jako	jako	k8xC,k8xS
nedělitelného	dělitelný	k2eNgInSc2d1
rámce	rámec	k1gInSc2
pro	pro	k7c4
obec	obec	k1gFnSc4
věřících	věřící	k1gMnPc2
(	(	kIx(
<g/>
umma	umma	k1gMnSc1
<g/>
)	)	kIx)
působí	působit	k5eAaImIp3nS
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
někdy	někdy	k6eAd1
dost	dost	k6eAd1
napjatě	napjatě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
islámu	islám	k1gInSc6
totiž	totiž	k9
neproběhlo	proběhnout	k5eNaPmAgNnS
oddělení	oddělení	k1gNnSc3
náboženství	náboženství	k1gNnSc2
od	od	k7c2
státu	stát	k1gInSc2
(	(	kIx(
<g/>
sekularismus	sekularismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
islám	islám	k1gInSc1
vstupuje	vstupovat	k5eAaImIp3nS
do	do	k7c2
společenských	společenský	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
a	a	k8xC
reálné	reálný	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostoru	prostor	k1gInSc6
současného	současný	k2eAgInSc2d1
islámského	islámský	k2eAgInSc2d1
světa	svět	k1gInSc2
od	od	k7c2
Maroka	Maroko	k1gNnSc2
a	a	k8xC
západní	západní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
až	až	k9
po	po	k7c6
Indonésii	Indonésie	k1gFnSc6
a	a	k8xC
jižní	jižní	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
Filipín	Filipíny	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
přesahy	přesah	k1gInPc7
novodobé	novodobý	k2eAgFnSc2d1
islámské	islámský	k2eAgFnSc2d1
diaspory	diaspora	k1gFnSc2
na	na	k7c4
všechny	všechen	k3xTgInPc4
kontinenty	kontinent	k1gInPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
ale	ale	k9
projevuje	projevovat	k5eAaImIp3nS
i	i	k9
silná	silný	k2eAgFnSc1d1
kulturní	kulturní	k2eAgFnSc1d1
rozmanitost	rozmanitost	k1gFnSc1
domácích	domácí	k2eAgFnPc2d1
tradic	tradice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Relativní	relativní	k2eAgFnSc4d1
jednotu	jednota	k1gFnSc4
islámu	islám	k1gInSc2
vytvářejí	vytvářet	k5eAaImIp3nP
společná	společný	k2eAgFnSc1d1
víra	víra	k1gFnSc1
<g/>
,	,	kIx,
korán	korán	k1gInSc1
<g/>
,	,	kIx,
šaría	šaría	k1gFnSc1
a	a	k8xC
množství	množství	k1gNnSc1
kulturních	kulturní	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
vágní	vágní	k2eAgNnSc1d1
přítomné	přítomný	k2eAgNnSc1d1
pouto	pouto	k1gNnSc1
politické	politický	k2eAgFnSc2d1
solidarity	solidarita	k1gFnSc2
-	-	kIx~
např.	např.	kA
Organizace	organizace	k1gFnSc2
islámské	islámský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
a	a	k8xC
mnoho	mnoho	k4c4
islámských	islámský	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Islámský	islámský	k2eAgInSc1d1
reformismus	reformismus	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
přineslo	přinést	k5eAaPmAgNnS
postupnou	postupný	k2eAgFnSc4d1
konfrontaci	konfrontace	k1gFnSc4
světa	svět	k1gInSc2
islámu	islám	k1gInSc2
s	s	k7c7
dynamickým	dynamický	k2eAgInSc7d1
evropským	evropský	k2eAgInSc7d1
kapitalismem	kapitalismus	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
původním	původní	k2eAgInSc7d1
rysem	rys	k1gInSc7
-	-	kIx~
kolonialismem	kolonialismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámští	islámský	k2eAgMnPc1d1
politici	politik	k1gMnPc1
a	a	k8xC
myslitelé	myslitel	k1gMnPc1
se	se	k3xPyFc4
během	během	k7c2
evropských	evropský	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
intervencí	intervence	k1gFnPc2
poprvé	poprvé	k6eAd1
setkali	setkat	k5eAaPmAgMnP
nejen	nejen	k6eAd1
s	s	k7c7
technicky	technicky	k6eAd1
vyspělými	vyspělý	k2eAgFnPc7d1
a	a	k8xC
triumfujícími	triumfující	k2eAgFnPc7d1
armádami	armáda	k1gFnPc7
"	"	kIx"
<g/>
nevěřícího	věřící	k2eNgMnSc2d1
<g/>
"	"	kIx"
nepřítele	nepřítel	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
také	také	k9
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
demokracie	demokracie	k1gFnSc2
<g/>
,	,	kIx,
hospodářskými	hospodářský	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
liberálního	liberální	k2eAgInSc2d1
kapitalismu	kapitalismus	k1gInSc2
a	a	k8xC
tomu	ten	k3xDgNnSc3
odpovídajícími	odpovídající	k2eAgFnPc7d1
hodnotami	hodnota	k1gFnPc7
politického	politický	k2eAgInSc2d1
mechanismu	mechanismus	k1gInSc2
-	-	kIx~
tedy	tedy	k8xC
hodnotami	hodnota	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
Západ	západ	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
vštípit	vštípit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámský	islámský	k2eAgInSc1d1
svět	svět	k1gInSc1
na	na	k7c4
evropský	evropský	k2eAgInSc4d1
koloniální	koloniální	k2eAgInSc4d1
impakt	impakt	k1gInSc4
na	na	k7c4
tento	tento	k3xDgInSc4
pokus	pokus	k1gInSc4
o	o	k7c4
"	"	kIx"
<g/>
kulturní	kulturní	k2eAgFnSc4d1
globalizaci	globalizace	k1gFnSc4
<g/>
"	"	kIx"
zareagoval	zareagovat	k5eAaPmAgMnS
různými	různý	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
a	a	k8xC
s	s	k7c7
intenzitou	intenzita	k1gFnSc7
odpovídající	odpovídající	k2eAgFnSc2d1
tradicím	tradice	k1gFnPc3
a	a	k8xC
aktuálním	aktuální	k2eAgInPc3d1
poměrům	poměr	k1gInPc3
v	v	k7c6
té	ten	k3xDgFnSc6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
jeho	jeho	k3xOp3gFnSc6
části	část	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
rysem	rys	k1gInSc7
byla	být	k5eAaImAgFnS
reforma	reforma	k1gFnSc1
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
však	však	k9
nesnažila	snažit	k5eNaImAgFnS
o	o	k7c4
"	"	kIx"
<g/>
pokrok	pokrok	k1gInSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tedy	tedy	k9
uplatnění	uplatnění	k1gNnSc4
nové	nový	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
retrospektivě	retrospektiva	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
idealizaci	idealizace	k1gFnSc6
původního	původní	k2eAgInSc2d1
pravzoru	pravzor	k1gInSc2
a	a	k8xC
pokusu	pokus	k1gInSc2
o	o	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
návrat	návrat	k1gInSc4
s	s	k7c7
využitím	využití	k1gNnSc7
některých	některý	k3yIgFnPc2
hodnot	hodnota	k1gFnPc2
a	a	k8xC
dovedností	dovednost	k1gFnPc2
Západu	západ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámský	islámský	k2eAgInSc1d1
reformismus	reformismus	k1gInSc1
je	být	k5eAaImIp3nS
čistě	čistě	k6eAd1
západní	západní	k2eAgInSc4d1
akademický	akademický	k2eAgInSc4d1
název	název	k1gInSc4
pro	pro	k7c4
široký	široký	k2eAgInSc4d1
proud	proud	k1gInSc4
snažení	snažení	k1gNnSc2
o	o	k7c4
nápravu	náprava	k1gFnSc4
islámu	islám	k1gInSc2
návratem	návrat	k1gInSc7
k	k	k7c3
ideálu	ideál	k1gInSc3
původní	původní	k2eAgFnSc2d1
obce	obec	k1gFnSc2
věřících	věřící	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
badatelé	badatel	k1gMnPc1
do	do	k7c2
ní	on	k3xPp3gFnSc2
zahrnují	zahrnovat	k5eAaImIp3nP
i	i	k9
jednotlivé	jednotlivý	k2eAgNnSc4d1
reformační	reformační	k2eAgNnSc4d1
bojové	bojový	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
18	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
fulbské	fulbský	k2eAgInPc1d1
džihády	džihád	k1gInPc1
na	na	k7c6
Sahaře	Sahara	k1gFnSc6
<g/>
,	,	kIx,
Mahdího	Mahdí	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
v	v	k7c6
Súdánu	Súdán	k1gInSc6
<g/>
,	,	kIx,
wahhábovské	wahhábovská	k1gFnSc6
hnutí	hnutí	k1gNnSc2
na	na	k7c6
Arabském	arabský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
,	,	kIx,
džihád	džihád	k1gInSc1
a	a	k8xC
hidžry	hidžra	k1gFnPc1
v	v	k7c6
muslimských	muslimský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
indického	indický	k2eAgInSc2d1
subkontinentu	subkontinent	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
užším	úzký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
se	se	k3xPyFc4
pod	pod	k7c7
reformismem	reformismus	k1gInSc7
rozumí	rozumět	k5eAaImIp3nS
heterogenní	heterogenní	k2eAgNnSc1d1
intelektuální	intelektuální	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
vznikající	vznikající	k2eAgNnSc1d1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k8xC,k8xS
reakce	reakce	k1gFnSc2
na	na	k7c6
prudce	prudko	k6eAd1
se	se	k3xPyFc4
rozvíjející	rozvíjející	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
a	a	k8xC
myšlenkové	myšlenkový	k2eAgInPc1d1
kontakty	kontakt	k1gInPc1
s	s	k7c7
Evropou	Evropa	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bojovná	bojovný	k2eAgNnPc1d1
reformační	reformační	k2eAgNnPc1d1
hnutí	hnutí	k1gNnPc1
</s>
<s>
Mahdího	Mahdí	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
<g/>
,	,	kIx,
1881	#num#	k4
<g/>
-	-	kIx~
<g/>
1899	#num#	k4
<g/>
,	,	kIx,
nábožensky	nábožensky	k6eAd1
motivované	motivovaný	k2eAgNnSc1d1
protibritské	protibritský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
v	v	k7c6
Súdánu	Súdán	k1gInSc6
</s>
<s>
Jejich	jejich	k3xOp3gInSc7
smyslem	smysl	k1gInSc7
bylo	být	k5eAaImAgNnS
vesměs	vesměs	k6eAd1
vytvořit	vytvořit	k5eAaPmF
islámskou	islámský	k2eAgFnSc4d1
komunitu	komunita	k1gFnSc4
(	(	kIx(
<g/>
umma	umma	k6eAd1
<g/>
)	)	kIx)
nové	nový	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
<g/>
,	,	kIx,
vtisknout	vtisknout	k5eAaPmF
její	její	k3xOp3gInSc4
etický	etický	k2eAgInSc4d1
a	a	k8xC
organizační	organizační	k2eAgInSc4d1
ráz	ráz	k1gInSc4
podobný	podobný	k2eAgInSc1d1
Mohamedově	Mohamedův	k2eAgFnSc3d1
ummě	umma	k1gFnSc3
v	v	k7c6
Medíně	Medína	k1gFnSc6
v	v	k7c6
letech	léto	k1gNnPc6
622-630	622-630	k4
a	a	k8xC
muslimské	muslimský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
prvních	první	k4xOgInPc2
čtyřech	čtyři	k4xCgInPc2
"	"	kIx"
<g/>
správně	správně	k6eAd1
vedených	vedený	k2eAgInPc2d1
<g/>
"	"	kIx"
chalífů	chalífa	k1gMnPc2
(	(	kIx(
<g/>
632	#num#	k4
<g/>
–	–	k?
<g/>
661	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
obrodu	obroda	k1gFnSc4
prvotní	prvotní	k2eAgFnSc2d1
obce	obec	k1gFnSc2
a	a	k8xC
nastolení	nastolení	k1gNnSc3
autentického	autentický	k2eAgInSc2d1
islámského	islámský	k2eAgInSc2d1
řádu	řád	k1gInSc2
byly	být	k5eAaImAgFnP
dotyčné	dotyčný	k2eAgNnSc4d1
reformační	reformační	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
připraveny	připraven	k2eAgInPc1d1
vést	vést	k5eAaImF
ozbrojenou	ozbrojený	k2eAgFnSc4d1
formu	forma	k1gFnSc4
džihádu	džihád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
ideově	ideově	k6eAd1
nejvyspělejší	vyspělý	k2eAgInPc4d3
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
považuje	považovat	k5eAaImIp3nS
učení	učení	k1gNnSc1
a	a	k8xC
politická	politický	k2eAgFnSc1d1
praxe	praxe	k1gFnSc1
wahhábovského	wahhábovský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
(	(	kIx(
<g/>
vzniklo	vzniknout	k5eAaPmAgNnS
r.	r.	kA
1745	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
Arabském	arabský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
ideologickým	ideologický	k2eAgMnSc7d1
dědicem	dědic	k1gMnSc7
je	být	k5eAaImIp3nS
současný	současný	k2eAgInSc1d1
totalitní	totalitní	k2eAgInSc1d1
královský	královský	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Intelektuální	intelektuální	k2eAgInSc1d1
reformismus	reformismus	k1gInSc1
(	(	kIx(
<g/>
přelom	přelom	k1gInSc1
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
amorfní	amorfní	k2eAgInSc1d1
proud	proud	k1gInSc1
islámského	islámský	k2eAgNnSc2d1
moderního	moderní	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
koncepci	koncepce	k1gFnSc4
muslimských	muslimský	k2eAgMnPc2d1
učenců	učenec	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
od	od	k7c2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přemýšleli	přemýšlet	k5eAaImAgMnP
nad	nad	k7c7
možnostmi	možnost	k1gFnPc7
reformy	reforma	k1gFnSc2
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
a	a	k8xC
kvalitativní	kvalitativní	k2eAgMnSc1d1
posunu	posunout	k5eAaPmIp1nS
v	v	k7c6
koránské	koránský	k2eAgFnSc6d1
exegezi	exegeze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
různými	různý	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
zpružnit	zpružnit	k5eAaPmF
a	a	k8xC
obohatit	obohatit	k5eAaPmF
strnulou	strnulý	k2eAgFnSc4d1
islámskou	islámský	k2eAgFnSc4d1
doktrínu	doktrína	k1gFnSc4
a	a	k8xC
přizpůsobit	přizpůsobit	k5eAaPmF
právní	právní	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
potřebám	potřeba	k1gFnPc3
a	a	k8xC
hodnotám	hodnota	k1gFnPc3
moderního	moderní	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
do	do	k7c2
organismu	organismus	k1gInSc2
"	"	kIx"
<g/>
islámské	islámský	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
"	"	kIx"
postupně	postupně	k6eAd1
implantovány	implantován	k2eAgInPc4d1
pod	pod	k7c7
vlivem	vliv	k1gInSc7
koloniální	koloniální	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
evropských	evropský	k2eAgFnPc2d1
mocností	mocnost	k1gFnPc2
<g/>
..	..	k?
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Islámský	islámský	k2eAgInSc1d1
fundamentalismus	fundamentalismus	k1gInSc1
</s>
<s>
Zhruba	zhruba	k6eAd1
od	od	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
prohlubovat	prohlubovat	k5eAaImF
proces	proces	k1gInSc1
vytváření	vytváření	k1gNnSc2
hnutí	hnutí	k1gNnSc2
a	a	k8xC
aktivistických	aktivistický	k2eAgFnPc2d1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
pomíjivých	pomíjivý	k2eAgFnPc2d1
<g/>
)	)	kIx)
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vesměs	vesměs	k6eAd1
ideologicky	ideologicky	k6eAd1
navazovaly	navazovat	k5eAaImAgFnP
na	na	k7c4
tradicí	tradice	k1gFnSc7
muslimského	muslimský	k2eAgNnSc2d1
bratrstva	bratrstvo	k1gNnSc2
v	v	k7c6
Egyptě	Egypt	k1gInSc6
(	(	kIx(
<g/>
založená	založený	k2eAgFnSc1d1
1929	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
učeních	učení	k1gNnPc6
dvou	dva	k4xCgMnPc6
jejich	jejich	k3xOp3gMnPc2
teoretiků	teoretik	k1gMnPc2
–	–	k?
Hasana	Hasana	k1gFnSc1
al-Banny	al-Banna	k1gFnSc2
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
hlavně	hlavně	k9
Sajjida	Sajjida	k1gFnSc1
Qutba	Qutba	k1gFnSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetím	třetí	k4xOgMnSc7
významným	významný	k2eAgMnSc7d1
teoretikem	teoretik	k1gMnSc7
islámu	islám	k1gInSc2
byl	být	k5eAaImAgMnS
Abdu	Abda	k1gFnSc4
<g/>
'	'	kIx"
<g/>
l-a	l-a	k?
<g/>
'	'	kIx"
<g/>
la	la	k1gNnSc1
Maudúdí	Maudúdí	k1gNnPc2
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
a	a	k8xC
ideolog	ideolog	k1gMnSc1
indické	indický	k2eAgFnSc2d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
pákistánské	pákistánský	k2eAgFnSc2d1
<g/>
,	,	kIx,
strany	strana	k1gFnSc2
Ďzamá	Ďzamý	k2eAgFnSc1d1
<g/>
'	'	kIx"
<g/>
ate	ate	k?
Islámí	Islámí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trend	trend	k1gInSc1
aktivistického	aktivistický	k2eAgInSc2d1
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
obecním	obecní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
svrhnout	svrhnout	k5eAaPmF
domácí	domácí	k2eAgInSc4d1
"	"	kIx"
<g/>
neislámský	islámský	k2eNgInSc4d1
<g/>
"	"	kIx"
režim	režim	k1gInSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
odrazit	odrazit	k5eAaPmF
evropský	evropský	k2eAgInSc4d1
kolonialismus	kolonialismus	k1gInSc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
jeho	jeho	k3xOp3gFnPc6
podobách	podoba	k1gFnPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
prosadil	prosadit	k5eAaPmAgMnS
v	v	k7c6
řadě	řada	k1gFnSc6
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
základním	základní	k2eAgInSc7d1
inspirativním	inspirativní	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
tamních	tamní	k2eAgNnPc2d1
opozičních	opoziční	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
vystupujících	vystupující	k2eAgNnPc2d1
z	z	k7c2
pozic	pozice	k1gFnPc2
radikálního	radikální	k2eAgInSc2d1
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
směr	směr	k1gInSc4
se	se	k3xPyFc4
v	v	k7c6
evropské	evropský	k2eAgFnSc6d1
odborné	odborný	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
nejdříve	dříve	k6eAd3
vžil	vžít	k5eAaPmAgInS
termín	termín	k1gInSc1
islámský	islámský	k2eAgInSc4d1
fundamentalismus	fundamentalismus	k1gInSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
jeho	jeho	k3xOp3gFnPc1
jiné	jiný	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
jako	jako	k8xS,k8xC
politický	politický	k2eAgInSc1d1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
islámský	islámský	k2eAgInSc1d1
radikalismus	radikalismus	k1gInSc1
<g/>
,	,	kIx,
islamismus	islamismus	k1gInSc1
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Islámský	islámský	k2eAgInSc1d1
svět	svět	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Islámský	islámský	k2eAgInSc1d1
svět	svět	k1gInSc1
a	a	k8xC
Umma	Umma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Muslimská	muslimský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Termín	termín	k1gInSc1
islámský	islámský	k2eAgInSc4d1
svět	svět	k1gInSc4
má	mít	k5eAaImIp3nS
několik	několik	k4yIc4
významů	význam	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
kultury	kultura	k1gFnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
celosvětovou	celosvětový	k2eAgFnSc4d1
komunitu	komunita	k1gFnSc4
stoupenců	stoupenec	k1gMnPc2
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pojímá	pojímat	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
1,6	1,6	k4
miliardy	miliarda	k4xCgFnSc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
asi	asi	k9
jednu	jeden	k4xCgFnSc4
čtvrtinu	čtvrtina	k1gFnSc4
světové	světový	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Součástí	součást	k1gFnSc7
této	tento	k3xDgFnSc2
komunity	komunita	k1gFnSc2
jsou	být	k5eAaImIp3nP
členové	člen	k1gMnPc1
nejrůznějších	různý	k2eAgFnPc2d3
kultur	kultura	k1gFnPc2
<g/>
,	,	kIx,
národů	národ	k1gInPc2
a	a	k8xC
etnických	etnický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
jediným	jediný	k2eAgNnSc7d1
společným	společný	k2eAgNnSc7d1
pojítkem	pojítko	k1gNnSc7
je	být	k5eAaImIp3nS
náboženství	náboženství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
historickém	historický	k2eAgInSc6d1
či	či	k8xC
geopolitickém	geopolitický	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
tento	tento	k3xDgInSc4
termín	termín	k1gInSc4
znamená	znamenat	k5eAaImIp3nS
státy	stát	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
dominuje	dominovat	k5eAaImIp3nS
obyvatelstvo	obyvatelstvo	k1gNnSc4
s	s	k7c7
muslimskou	muslimský	k2eAgFnSc7d1
vírou	víra	k1gFnSc7
<g/>
,	,	kIx,
či	či	k8xC
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
islám	islám	k1gInSc1
hlavní	hlavní	k2eAgInSc1d1
vliv	vliv	k1gInSc4
v	v	k7c6
politické	politický	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Celosvětová	celosvětový	k2eAgFnSc1d1
muslimská	muslimský	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
se	se	k3xPyFc4
také	také	k9
souhrnně	souhrnně	k6eAd1
nazývá	nazývat	k5eAaImIp3nS
umma	umma	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islám	islám	k1gInSc1
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
především	především	k9
jednotu	jednota	k1gFnSc4
a	a	k8xC
ochranu	ochrana	k1gFnSc4
ostatních	ostatní	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
odvětví	odvětví	k1gNnPc2
islámské	islámský	k2eAgFnSc2d1
víry	víra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakter	charakter	k1gInSc1
islámského	islámský	k2eAgInSc2d1
světa	svět	k1gInSc2
ovlivnily	ovlivnit	k5eAaPmAgInP
historicky	historicky	k6eAd1
zejména	zejména	k9
dva	dva	k4xCgInPc1
proudy	proud	k1gInPc1
<g/>
:	:	kIx,
panislamismus	panislamismus	k1gInSc1
a	a	k8xC
nacionalismus	nacionalismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
žije	žít	k5eAaImIp3nS
1	#num#	k4
miliarda	miliarda	k4xCgFnSc1
muslimů	muslim	k1gMnPc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
322	#num#	k4
milionů	milion	k4xCgInPc2
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
a	a	k8xC
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
243	#num#	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
subsaharské	subsaharský	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
a	a	k8xC
44	#num#	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indonésie	Indonésie	k1gFnSc1
je	být	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
s	s	k7c7
největším	veliký	k2eAgInSc7d3
počtem	počet	k1gInSc7
muslimů	muslim	k1gMnPc2
<g/>
;	;	kIx,
asi	asi	k9
205	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následují	následovat	k5eAaImIp3nP
Pákistán	Pákistán	k1gInSc4
se	s	k7c7
178	#num#	k4
miliony	milion	k4xCgInPc7
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
se	s	k7c7
177	#num#	k4
miliony	milion	k4xCgInPc7
<g/>
,	,	kIx,
Bangladéš	Bangladéš	k1gInSc1
se	s	k7c7
148	#num#	k4
miliony	milion	k4xCgInPc7
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
s	s	k7c7
80	#num#	k4
miliony	milion	k4xCgInPc7
<g/>
,	,	kIx,
a	a	k8xC
Írán	Írán	k1gInSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
a	a	k8xC
Nigérie	Nigérie	k1gFnSc1
s	s	k7c7
muslimskou	muslimský	k2eAgFnSc7d1
populací	populace	k1gFnSc7
75	#num#	k4
milionů	milion	k4xCgInPc2
v	v	k7c6
každé	každý	k3xTgFnSc6
z	z	k7c2
těchto	tento	k3xDgFnPc2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Muslimská	muslimský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
novodobých	novodobý	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
do	do	k7c2
západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
ve	v	k7c6
Francii	Francie	k1gFnSc6
tvoří	tvořit	k5eAaImIp3nS
7,5	7,5	k4
<g/>
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
pak	pak	k6eAd1
ze	z	k7c2
starousedlých	starousedlý	k2eAgFnPc2d1
komunit	komunita	k1gFnPc2
na	na	k7c6
Balkáně	Balkán	k1gInSc6
z	z	k7c2
dob	doba	k1gFnPc2
turecké	turecký	k2eAgFnSc2d1
expanze	expanze	k1gFnSc2
(	(	kIx(
<g/>
slovanští	slovanštit	k5eAaImIp3nS
Bosňáci	Bosňáci	k?
a	a	k8xC
Pomaci	Pomak	k1gMnPc1
<g/>
,	,	kIx,
Albánci	Albánec	k1gMnPc1
<g/>
,	,	kIx,
Turci	Turek	k1gMnPc1
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
například	například	k6eAd1
70	#num#	k4
<g/>
%	%	kIx~
obyvatel	obyvatel	k1gMnSc1
Albánie	Albánie	k1gFnSc2
a	a	k8xC
30	#num#	k4
<g/>
%	%	kIx~
populace	populace	k1gFnSc2
v	v	k7c6
Makedonii	Makedonie	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
Rusku	Rusko	k1gNnSc6
(	(	kIx(
<g/>
Tataři	Tatar	k1gMnPc1
v	v	k7c6
Povolží	Povolží	k1gNnSc6
nebo	nebo	k8xC
Čečenci	Čečence	k1gFnSc6
na	na	k7c6
Kavkaze	Kavkaz	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starousedlá	starousedlý	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
Tatarů	Tatar	k1gMnPc2
žije	žít	k5eAaImIp3nS
i	i	k9
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Páteční	páteční	k2eAgFnSc1d1
modlitba	modlitba	k1gFnSc1
v	v	k7c6
Dháce	Dháka	k1gFnSc6
v	v	k7c6
Bangladéši	Bangladéš	k1gInSc6
</s>
<s>
Mešita	mešita	k1gFnSc1
Istiqlal	Istiqlal	k1gFnSc2
v	v	k7c6
indonéské	indonéský	k2eAgFnSc6d1
Jakartě	Jakarta	k1gFnSc6
</s>
<s>
Kazašská	kazašský	k2eAgFnSc1d1
svatba	svatba	k1gFnSc1
v	v	k7c6
mešitě	mešita	k1gFnSc6
</s>
<s>
Věřící	věřící	k1gFnSc1
v	v	k7c6
mešitě	mešita	k1gFnSc6
v	v	k7c4
Kosovu	Kosův	k2eAgFnSc4d1
</s>
<s>
Islám	islám	k1gInSc1
a	a	k8xC
české	český	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Islám	islám	k1gInSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Vídně	Vídeň	k1gFnSc2
roku	rok	k1gInSc2
1683	#num#	k4
ukončila	ukončit	k5eAaPmAgFnS
tureckou	turecký	k2eAgFnSc4d1
expanzi	expanze	k1gFnSc4
do	do	k7c2
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
</s>
<s>
Český	český	k2eAgMnSc1d1
fotograf	fotograf	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Bruner-Dvořák	Bruner-Dvořák	k1gMnSc1
zdokumentoval	zdokumentovat	k5eAaPmAgMnS
muslimský	muslimský	k2eAgInSc4d1
svět	svět	k1gInSc4
Bosny	Bosna	k1gFnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
S	s	k7c7
islámem	islám	k1gInSc7
přicházejí	přicházet	k5eAaImIp3nP
české	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
častěji	často	k6eAd2
do	do	k7c2
kontaktu	kontakt	k1gInSc2
od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
době	doba	k1gFnSc6
expanze	expanze	k1gFnSc2
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
na	na	k7c4
Balkán	Balkán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
osmanští	osmanský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
ovládli	ovládnout	k5eAaPmAgMnP
dvě	dva	k4xCgNnPc4
nejposvátnější	posvátný	k2eAgNnPc4d3
místa	místo	k1gNnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
Mekku	Mekka	k1gFnSc4
a	a	k8xC
Medínu	Medína	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
osmanští	osmanský	k2eAgMnPc1d1
sultáni	sultán	k1gMnPc1
se	se	k3xPyFc4
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
chalífy	chalífa	k1gMnPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
vůdce	vůdce	k1gMnSc1
celého	celý	k2eAgInSc2d1
muslimského	muslimský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mají	mít	k5eAaImIp3nP
povinnost	povinnost	k1gFnSc4
šířit	šířit	k5eAaImF
islám	islám	k1gInSc4
všemi	všecek	k3xTgInPc7
možnými	možný	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
včetně	včetně	k7c2
džihádu	džihád	k1gInSc2
mečem	meč	k1gInSc7
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Válek	válek	k1gInSc1
s	s	k7c7
Turky	Turek	k1gMnPc4
se	se	k3xPyFc4
ve	v	k7c6
službách	služba	k1gFnPc6
uherských	uherský	k2eAgMnPc2d1
a	a	k8xC
habsburských	habsburský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
zúčastnili	zúčastnit	k5eAaPmAgMnP
např.	např.	kA
husitský	husitský	k2eAgMnSc1d1
válečník	válečník	k1gMnSc1
Jan	Jan	k1gMnSc1
Jiskra	jiskra	k1gFnSc1
z	z	k7c2
Brandýsa	Brandýs	k1gInSc2
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
českých	český	k2eAgInPc2d1
stavů	stav	k1gInPc2
Vilém	Viléma	k1gFnPc2
z	z	k7c2
Rožmberka	Rožmberk	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
švagr	švagr	k1gMnSc1
Mikuláš	Mikuláš	k1gMnSc1
Zrinský	Zrinský	k1gMnSc1
zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
obraně	obrana	k1gFnSc6
pevnosti	pevnost	k1gFnSc2
Szigetvár	Szigetvár	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
62	#num#	k4
<g/>
]	]	kIx)
mladý	mladý	k2eAgMnSc1d1
Albrecht	Albrecht	k1gMnSc1
z	z	k7c2
Valdštejna	Valdštejn	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
český	český	k2eAgMnSc1d1
šlechtic	šlechtic	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Kaplíř	Kaplíř	k1gMnSc1
ze	z	k7c2
Sulevic	Sulevice	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
velel	velet	k5eAaImAgInS
obraně	obraně	k6eAd1
Vídně	Vídeň	k1gFnSc2
před	před	k7c7
Turky	Turek	k1gMnPc7
roku	rok	k1gInSc2
1683	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
císařský	císařský	k2eAgMnSc1d1
polní	polní	k2eAgMnSc1d1
maršálek	maršálek	k1gMnSc1
Adolf	Adolf	k1gMnSc1
Schwarzenberg	Schwarzenberg	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
za	za	k7c4
vítězství	vítězství	k1gNnSc4
nad	nad	k7c4
Turky	Turek	k1gMnPc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Rábu	Ráb	k1gInSc2
získal	získat	k5eAaPmAgMnS
do	do	k7c2
rodového	rodový	k2eAgInSc2d1
erbu	erb	k1gInSc2
Schwarzenbergů	Schwarzenberg	k1gInPc2
pole	pole	k1gNnSc4
s	s	k7c7
hlavou	hlava	k1gFnSc7
Turka	Turek	k1gMnSc2
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
vyklovává	vyklovávat	k5eAaImIp3nS
oči	oko	k1gNnPc4
krkavec	krkavec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
českých	český	k2eAgInPc2d1
cestopisů	cestopis	k1gInPc2
pozdního	pozdní	k2eAgInSc2d1
středověku	středověk	k1gInSc2
a	a	k8xC
raného	raný	k2eAgInSc2d1
novověku	novověk	k1gInSc2
lze	lze	k6eAd1
zmínit	zmínit	k5eAaPmF
například	například	k6eAd1
cestopis	cestopis	k1gInSc4
Cronica	Cronicum	k1gNnSc2
Boemorum	Boemorum	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
Giovanni	Giovaneň	k1gFnSc6
Marignoli	Marignole	k1gFnSc6
líčí	líčit	k5eAaImIp3nS
svoji	svůj	k3xOyFgFnSc4
cestu	cesta	k1gFnSc4
do	do	k7c2
Číny	Čína	k1gFnSc2
a	a	k8xC
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
prostor	prostor	k1gInSc4
i	i	k9
muslimským	muslimský	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
cestopise	cestopis	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1505	#num#	k4
Putování	putování	k1gNnPc2
ke	k	k7c3
Svatému	svatý	k2eAgInSc3d1
hrobu	hrob	k1gInSc3
zprostředkoval	zprostředkovat	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Hasištejnský	Hasištejnský	k1gMnSc1
z	z	k7c2
Lobkovic	Lobkovice	k1gInPc2
českému	český	k2eAgMnSc3d1
čtenáři	čtenář	k1gMnPc7
řadu	řada	k1gFnSc4
informací	informace	k1gFnPc2
o	o	k7c6
islámu	islám	k1gInSc6
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
historii	historie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Budovec	Budovec	k1gMnSc1
z	z	k7c2
Budova	budova	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1577	#num#	k4
členem	člen	k1gMnSc7
císařského	císařský	k2eAgNnSc2d1
poselstva	poselstvo	k1gNnSc2
k	k	k7c3
osmanskému	osmanský	k2eAgMnSc3d1
sultánu	sultán	k1gMnSc3
<g/>
,	,	kIx,
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
polemický	polemický	k2eAgInSc4d1
spis	spis	k1gInSc4
o	o	k7c6
islámu	islám	k1gInSc6
zvaný	zvaný	k2eAgInSc1d1
Antialkorán	Antialkorán	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Obšírné	obšírný	k2eAgFnSc2d1
informace	informace	k1gFnSc2
o	o	k7c6
islámu	islám	k1gInSc6
podává	podávat	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
cestopise	cestopis	k1gInSc6
Kryštof	Kryštof	k1gMnSc1
Harant	Harant	k?
z	z	k7c2
Polžic	Polžice	k1gFnPc2
a	a	k8xC
Bezdružic	Bezdružice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islám	islám	k1gInSc4
a	a	k8xC
život	život	k1gInSc4
v	v	k7c6
Osmanské	osmanský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
popisuje	popisovat	k5eAaImIp3nS
také	také	k9
Václav	Václav	k1gMnSc1
Vratislav	Vratislav	k1gMnSc1
z	z	k7c2
Mitrovic	Mitrovice	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
strávil	strávit	k5eAaPmAgInS
několik	několik	k4yIc4
let	léto	k1gNnPc2
v	v	k7c6
tureckém	turecký	k2eAgNnSc6d1
zajetí	zajetí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
moravské	moravský	k2eAgNnSc4d1
území	území	k1gNnSc4
vpadli	vpadnout	k5eAaPmAgMnP
Turci	Turek	k1gMnPc1
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1599	#num#	k4
v	v	k7c6
průběhu	průběh	k1gInSc6
tzv.	tzv.	kA
dlouhé	dlouhý	k2eAgFnSc2d1
turecké	turecký	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1663	#num#	k4
podnikli	podniknout	k5eAaPmAgMnP
Tataři	Tatar	k1gMnPc1
z	z	k7c2
Krymského	krymský	k2eAgInSc2d1
chanátu	chanát	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	stoletý	k2eAgMnPc1d1
vazalové	vazal	k1gMnPc1
osmanského	osmanský	k2eAgMnSc2d1
sultána	sultán	k1gMnSc2
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
Turky	Turek	k1gMnPc7
během	během	k7c2
osmansko-habsburské	osmansko-habsburský	k2eAgFnSc2d1
války	válka	k1gFnSc2
několik	několik	k4yIc4
vpádů	vpád	k1gInPc2
na	na	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
Moravu	Morava	k1gFnSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
strategická	strategický	k2eAgNnPc1d1
pevnost	pevnost	k1gFnSc4
Nové	Nové	k2eAgInPc1d1
Zámky	zámek	k1gInPc1
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
odvlečeno	odvléct	k5eAaPmNgNnS
do	do	k7c2
otroctví	otroctví	k1gNnSc2
na	na	k7c4
12	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
„	„	k?
<g/>
Čoskoro	Čoskora	k1gFnSc5
sa	sa	k?
začalo	začít	k5eAaPmAgNnS
ohavné	ohavný	k2eAgFnPc4d1
neslýchané	slýchaný	k2eNgFnPc4d1
prznenie	prznenie	k1gFnPc4
a	a	k8xC
znásilňovanie	znásilňovanie	k1gFnPc4
žien	žieno	k1gNnPc2
a	a	k8xC
panien	panina	k1gFnPc2
<g/>
,	,	kIx,
<g/>
“	“	k?
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
očitý	očitý	k2eAgMnSc1d1
svědek	svědek	k1gMnSc1
evangelický	evangelický	k2eAgMnSc1d1
farář	farář	k1gMnSc1
Štefan	Štefan	k1gMnSc1
Pilárik	Pilárik	k1gMnSc1
o	o	k7c6
ženách	žena	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
odvlekli	odvléct	k5eAaPmAgMnP
Tataři	Tatar	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
unesených	unesený	k2eAgMnPc2d1
chlapců	chlapec	k1gMnPc2
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
stát	stát	k5eAaPmF,k5eAaImF
janičáři	janičár	k1gMnPc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byli	být	k5eAaImAgMnP
elitní	elitní	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
a	a	k8xC
nejobávanější	obávaný	k2eAgFnSc1d3
část	část	k1gFnSc1
osmanského	osmanský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
dětství	dětství	k1gNnSc6
odebíráni	odebírat	k5eAaImNgMnP
z	z	k7c2
křesťanských	křesťanský	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
na	na	k7c6
základě	základ	k1gInSc6
tzv.	tzv.	kA
„	„	k?
<g/>
daně	daň	k1gFnPc4
z	z	k7c2
krve	krev	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
vychováni	vychován	k2eAgMnPc1d1
v	v	k7c6
muslimské	muslimský	k2eAgFnSc6d1
víře	víra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
pochází	pocházet	k5eAaImIp3nS
české	český	k2eAgNnSc4d1
přísloví	přísloví	k1gNnSc4
poturčenec	poturčenec	k1gMnSc1
horší	zlý	k2eAgMnSc1d2
Turka	turek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
vytlačování	vytlačování	k1gNnSc6
islámu	islám	k1gInSc2
z	z	k7c2
Evropy	Evropa	k1gFnSc2
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgInS
s	s	k7c7
českými	český	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
spjatý	spjatý	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Laudon	Laudon	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
během	během	k7c2
osmé	osmý	k4xOgFnSc2
rakousko-turecké	rakousko-turecký	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
rakouské	rakouský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
dobyl	dobýt	k5eAaPmAgInS
roku	rok	k1gInSc2
1789	#num#	k4
na	na	k7c6
Turcích	turek	k1gInPc6
Bělehrad	Bělehrad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgInPc6
bojích	boj	k1gInPc6
se	se	k3xPyFc4
vyznamenal	vyznamenat	k5eAaPmAgMnS
Laudonův	Laudonův	k2eAgMnSc1d1
pobočník	pobočník	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
slavný	slavný	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
Jan	Jan	k1gMnSc1
Radecký	Radecký	k1gMnSc1
z	z	k7c2
Radče	Radč	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Češi	česat	k5eAaImIp1nS
sloužící	sloužící	k2eAgMnSc1d1
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
potlačovali	potlačovat	k5eAaImAgMnP
povstání	povstání	k1gNnSc3
bosenských	bosenský	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
proti	proti	k7c3
„	„	k?
<g/>
nevěřícím	nevěřící	k1gMnSc7
<g/>
“	“	k?
okupantům	okupant	k1gMnPc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
vypuklo	vypuknout	k5eAaPmAgNnS
poté	poté	k6eAd1
co	co	k9
se	se	k3xPyFc4
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
stala	stát	k5eAaPmAgFnS
roku	rok	k1gInSc2
1878	#num#	k4
protektorátem	protektorát	k1gInSc7
Rakousko-Uherska	Rakousko-Uhersko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přepadení	přepadení	k1gNnSc1
rakouských	rakouský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
místními	místní	k2eAgMnPc7d1
muslimy	muslim	k1gMnPc7
u	u	k7c2
města	město	k1gNnSc2
Maglaj	Maglaj	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
změnilo	změnit	k5eAaPmAgNnS
v	v	k7c4
krveprolití	krveprolití	k1gNnPc4
<g/>
,	,	kIx,
dalo	dát	k5eAaPmAgNnS
hovorové	hovorový	k2eAgNnSc1d1
češtině	čeština	k1gFnSc6
slovo	slovo	k1gNnSc1
„	„	k?
<g/>
maglajz	maglajz	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Místní	místní	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
nebrali	brát	k5eNaImAgMnP
ohledy	ohled	k1gInPc4
na	na	k7c4
běžné	běžný	k2eAgFnPc4d1
válečné	válečný	k2eAgFnPc4d1
zvyklosti	zvyklost	k1gFnPc4
<g/>
,	,	kIx,
nebrali	brát	k5eNaImAgMnP
zajatce	zajatec	k1gMnSc4
a	a	k8xC
vojákům	voják	k1gMnPc3
uřezávali	uřezávat	k5eAaImAgMnP
v	v	k7c4
duchu	duch	k1gMnSc6
domácích	domácí	k2eAgFnPc2d1
muslimských	muslimský	k2eAgFnPc2d1
zvyklostí	zvyklost	k1gFnPc2
hlavy	hlava	k1gFnSc2
<g/>
,	,	kIx,
nosy	nos	k1gInPc1
<g/>
,	,	kIx,
uši	ucho	k1gNnPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
části	část	k1gFnPc1
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
romantismu	romantismus	k1gInSc2
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
zájem	zájem	k1gInSc1
o	o	k7c4
orientální	orientální	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
a	a	k8xC
také	také	k9
o	o	k7c4
islám	islám	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
století	století	k1gNnSc2
po	po	k7c6
islámském	islámský	k2eAgInSc6d1
světě	svět	k1gInSc6
cestovali	cestovat	k5eAaImAgMnP
čeští	český	k2eAgMnPc1d1
orientalisté	orientalista	k1gMnPc1
jako	jako	k8xS,k8xC
Alois	Alois	k1gMnSc1
Musil	Musil	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
působil	působit	k5eAaImAgMnS
mezi	mezi	k7c7
beduínskými	beduínský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
v	v	k7c6
Arábii	Arábie	k1gFnSc6
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
protivníkem	protivník	k1gMnSc7
svého	svůj	k3xOyFgMnSc2
slavnějšího	slavný	k2eAgMnSc2d2
kolegy	kolega	k1gMnSc2
Lawrence	Lawrenec	k1gMnSc2
z	z	k7c2
Arábie	Arábie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1912	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
islám	islám	k1gInSc1
oficiálně	oficiálně	k6eAd1
uznaným	uznaný	k2eAgNnSc7d1
náboženstvím	náboženství	k1gNnSc7
na	na	k7c6
území	území	k1gNnSc6
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
,	,	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
vstřícný	vstřícný	k2eAgInSc4d1
krok	krok	k1gInSc4
vůči	vůči	k7c3
muslimům	muslim	k1gMnPc3
v	v	k7c6
Bosně	Bosna	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
tehdy	tehdy	k6eAd1
byla	být	k5eAaImAgFnS
monarchií	monarchie	k1gFnPc2
anektována	anektovat	k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústředí	ústředí	k1gNnPc1
muslimských	muslimský	k2eAgFnPc2d1
náboženských	náboženský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
funguje	fungovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
první	první	k4xOgFnSc1
mešita	mešita	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
i	i	k9
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
při	při	k7c6
sčítání	sčítání	k1gNnSc6
lidu	lid	k1gInSc2
k	k	k7c3
islámu	islám	k1gInSc3
přihlásilo	přihlásit	k5eAaPmAgNnS
3085	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
Česku	Česko	k1gNnSc6
odhadem	odhad	k1gInSc7
22	#num#	k4
000	#num#	k4
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Kritika	kritika	k1gFnSc1
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ženy	žena	k1gFnPc1
v	v	k7c6
nikábu	nikáb	k1gInSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1
radikálních	radikální	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
v	v	k7c6
Sydney	Sydney	k1gNnSc6
<g/>
,	,	kIx,
„	„	k?
<g/>
Připravte	připravit	k5eAaPmRp2nP
o	o	k7c4
hlavu	hlava	k1gFnSc4
každého	každý	k3xTgMnSc4
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
uráží	urážet	k5eAaPmIp3nS,k5eAaImIp3nS
proroka	prorok	k1gMnSc4
<g/>
“	“	k?
</s>
<s>
Mezi	mezi	k7c7
objekty	objekt	k1gInPc7
kritiky	kritika	k1gFnSc2
patří	patřit	k5eAaImIp3nS
mj.	mj.	kA
morální	morální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
proroka	prorok	k1gMnSc2
Mohameda	Mohamed	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k8xS,k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
veřejném	veřejný	k2eAgInSc6d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
osobním	osobní	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Otázky	otázka	k1gFnPc1
týkající	týkající	k2eAgFnPc1d1
se	se	k3xPyFc4
pravosti	pravost	k1gFnSc2
a	a	k8xC
morálky	morálka	k1gFnSc2
Koránu	korán	k1gInSc2
<g/>
,	,	kIx,
islámské	islámský	k2eAgFnSc2d1
svaté	svatý	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
kritiky	kritika	k1gFnPc1
také	také	k9
diskutovány	diskutován	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Jiné	jiný	k2eAgFnPc1d1
kritiky	kritika	k1gFnPc1
se	se	k3xPyFc4
zaměřují	zaměřovat	k5eAaImIp3nP
na	na	k7c4
otázku	otázka	k1gFnSc4
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
v	v	k7c6
dnešních	dnešní	k2eAgInPc6d1
islámských	islámský	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
postavení	postavení	k1gNnSc1
žen	žena	k1gFnPc2
v	v	k7c6
islámském	islámský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
a	a	k8xC
praxi	praxe	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
nedávný	dávný	k2eNgInSc4d1
trend	trend	k1gInSc4
multikulturalismu	multikulturalismus	k1gInSc2
byl	být	k5eAaImAgInS
kritizován	kritizován	k2eAgInSc1d1
vliv	vliv	k1gInSc1
islámu	islám	k1gInSc2
na	na	k7c4
schopnost	schopnost	k1gFnSc4
<g/>
,	,	kIx,
resp.	resp.	kA
neschopnost	neschopnost	k1gFnSc1
asimilace	asimilace	k1gFnSc2
muslimských	muslimský	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
na	na	k7c4
Západ	západ	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mladý	mladý	k2eAgInSc1d1
muslimský	muslimský	k2eAgInSc1d1
pár	pár	k1gInSc1
v	v	k7c6
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
čelí	čelit	k5eAaImIp3nS
kritice	kritika	k1gFnSc3
za	za	k7c2
šíření	šíření	k1gNnSc2
wahhábismu	wahhábismus	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
sunnitská	sunnitský	k2eAgFnSc1d1
fundamentální	fundamentální	k2eAgFnSc1d1
forma	forma	k1gFnSc1
islámu	islám	k1gInSc2
a	a	k8xC
státní	státní	k2eAgFnSc2d1
ideologie	ideologie	k1gFnSc2
Saúdské	saúdský	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
Striktní	striktní	k2eAgFnSc1d1
interpretací	interpretace	k1gFnSc7
wahhábismu	wahhábismus	k1gInSc2
se	se	k3xPyFc4
inspirovala	inspirovat	k5eAaBmAgNnP
mnohá	mnohý	k2eAgNnPc1d1
islamistická	islamistický	k2eAgNnPc1d1
hnutí	hnutí	k1gNnPc1
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Islámský	islámský	k2eAgInSc1d1
stát	stát	k1gInSc1
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
a	a	k8xC
Iráku	Irák	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Kavkazský	kavkazský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
syrští	syrský	k2eAgMnPc1d1
povstalci	povstalec	k1gMnPc1
z	z	k7c2
Fronty	fronta	k1gFnSc2
an-Nusrá	an-Nusrat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
teroristická	teroristický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Al-Káida	Al-Káida	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
Tálibán	Tálibán	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wahhábismus	Wahhábismus	k1gInSc1
začal	začít	k5eAaPmAgInS
expandovat	expandovat	k5eAaImF
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
když	když	k8xS
Saúdové	Saúda	k1gMnPc1
využili	využít	k5eAaPmAgMnP
svých	svůj	k3xOyFgMnPc2
příjmů	příjem	k1gInPc2
z	z	k7c2
prodeje	prodej	k1gInSc2
ropy	ropa	k1gFnSc2
na	na	k7c4
sponzorování	sponzorování	k1gNnSc4
výstavby	výstavba	k1gFnSc2
mešit	mešita	k1gFnPc2
a	a	k8xC
madras	madras	k1gInSc1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
na	na	k7c6
šíření	šíření	k1gNnSc6
své	svůj	k3xOyFgFnSc2
radikální	radikální	k2eAgFnSc2d1
a	a	k8xC
netolerantní	tolerantní	k2eNgFnSc2d1
interpretace	interpretace	k1gFnSc2
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
ke	k	k7c3
křesťanství	křesťanství	k1gNnSc3
a	a	k8xC
judaismu	judaismus	k1gInSc3
</s>
<s>
Islám	islám	k1gInSc1
<g/>
,	,	kIx,
judaismus	judaismus	k1gInSc1
a	a	k8xC
křesťanství	křesťanství	k1gNnPc1
jsou	být	k5eAaImIp3nP
monoteistická	monoteistický	k2eAgNnPc1d1
a	a	k8xC
prorocká	prorocký	k2eAgNnPc1d1
náboženství	náboženství	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabské	arabský	k2eAgFnPc4d1
slovo	slovo	k1gNnSc1
Alláh	Alláh	k1gMnSc1
je	být	k5eAaImIp3nS
příbuzné	příbuzná	k1gFnPc4
hebrejskému	hebrejský	k2eAgInSc3d1
Eloah	Eloaha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
ve	v	k7c6
Starém	starý	k2eAgInSc6d1
zákoně	zákon	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Muslimové	muslim	k1gMnPc1
nepovažují	považovat	k5eNaImIp3nP
Mohameda	Mohamed	k1gMnSc4
za	za	k7c2
zakladatele	zakladatel	k1gMnSc2
nového	nový	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c4
obnovitele	obnovitel	k1gMnPc4
původní	původní	k2eAgFnSc2d1
monoteistické	monoteistický	k2eAgFnSc2d1
víry	víra	k1gFnSc2
Adama	Adam	k1gMnSc2
<g/>
,	,	kIx,
Noeho	Noe	k1gMnSc2
<g/>
,	,	kIx,
Abraháma	Abrahám	k1gMnSc2
<g/>
,	,	kIx,
Mojžíše	Mojžíš	k1gMnSc2
<g/>
,	,	kIx,
Ježíše	Ježíš	k1gMnSc2
i	i	k8xC
dalších	další	k2eAgMnPc2d1
proroků	prorok	k1gMnPc2
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
islámské	islámský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
židé	žid	k1gMnPc1
a	a	k8xC
křesťané	křesťan	k1gMnPc1
zkreslili	zkreslit	k5eAaPmAgMnP
zjevení	zjevení	k1gNnSc4
daná	daný	k2eAgFnSc1d1
Bohem	bůh	k1gMnSc7
skrze	skrze	k?
proroky	prorok	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
buď	buď	k8xC
změnou	změna	k1gFnSc7
textu	text	k1gInSc2
<g/>
,	,	kIx,
nesprávnou	správný	k2eNgFnSc7d1
interpretací	interpretace	k1gFnSc7
nebo	nebo	k8xC
obojím	obojí	k4xRgMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
je	být	k5eAaImIp3nS
islám	islám	k1gInSc1
od	od	k7c2
křesťanství	křesťanství	k1gNnSc2
i	i	k8xC
judaismu	judaismus	k1gInSc2
zásadně	zásadně	k6eAd1
odlišný	odlišný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
Mohamedův	Mohamedův	k2eAgMnSc1d1
následovník	následovník	k1gMnSc1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
v	v	k7c6
první	první	k4xOgFnSc6
řadě	řada	k1gFnSc6
podvolit	podvolit	k5eAaPmF
Bohu	bůh	k1gMnSc3
a	a	k8xC
jeho	jeho	k3xOp3gMnPc3
vůli	vůle	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
hlásá	hlásat	k5eAaImIp3nS
naprostý	naprostý	k2eAgInSc4d1
fatalismus	fatalismus	k1gInSc4
<g/>
,	,	kIx,
takže	takže	k8xS
každý	každý	k3xTgInSc1
krok	krok	k1gInSc1
lidské	lidský	k2eAgFnSc2d1
bytosti	bytost	k1gFnSc2
je	být	k5eAaImIp3nS
předurčen	předurčit	k5eAaPmNgInS
Bohem	bůh	k1gMnSc7
a	a	k8xC
vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc7
dílem	dílo	k1gNnSc7
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
má	mít	k5eAaImIp3nS
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
lidskou	lidský	k2eAgFnSc4d1
motivaci	motivace	k1gFnSc4
a	a	k8xC
na	na	k7c4
absenci	absence	k1gFnSc4
individuality	individualita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Ze	z	k7c2
stejného	stejný	k2eAgInSc2d1
důvodu	důvod	k1gInSc2
islám	islám	k1gInSc1
nepracuje	pracovat	k5eNaImIp3nS
ani	ani	k8xC
s	s	k7c7
konceptem	koncept	k1gInSc7
svobodné	svobodný	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc1d1
text	text	k1gInSc1
islámu	islám	k1gInSc2
–	–	k?
Korán	korán	k1gInSc1
–	–	k?
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
absolutní	absolutní	k2eAgFnSc4d1
neměnnou	měnný	k2eNgFnSc4d1,k2eAgFnSc4d1
pravdu	pravda	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
předchozím	předchozí	k2eAgInSc6d1
není	být	k5eNaImIp3nS
v	v	k7c6
islámu	islám	k1gInSc6
místo	místo	k6eAd1
nejen	nejen	k6eAd1
pro	pro	k7c4
zpochybňování	zpochybňování	k1gNnSc4
zjevení	zjevení	k1gNnPc2
uvedených	uvedený	k2eAgNnPc2d1
v	v	k7c6
Koránu	korán	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
pro	pro	k7c4
kritické	kritický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
jako	jako	k8xC,k8xS
takové	takový	k3xDgNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
Zjevené	zjevený	k2eAgInPc1d1
texty	text	k1gInPc1
však	však	k9
podléhají	podléhat	k5eAaImIp3nP
určité	určitý	k2eAgFnSc3d1
interpretaci	interpretace	k1gFnSc3
(	(	kIx(
<g/>
výrazněji	výrazně	k6eAd2
u	u	k7c2
šíitů	šíita	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
různé	různý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
jsou	být	k5eAaImIp3nP
též	též	k9
v	v	k7c6
interakci	interakce	k1gFnSc6
s	s	k7c7
kulturními	kulturní	k2eAgInPc7d1
vzorci	vzorec	k1gInPc7
kultur	kultura	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
se	se	k3xPyFc4
šíří	šířit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	ten	k3xDgNnSc3
odpovídá	odpovídat	k5eAaImIp3nS
nejen	nejen	k6eAd1
existence	existence	k1gFnSc1
různých	různý	k2eAgInPc2d1
směrů	směr	k1gInPc2
a	a	k8xC
škol	škola	k1gFnPc2
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
praxe	praxe	k1gFnSc1
v	v	k7c6
různých	různý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Někteří	některý	k3yIgMnPc1
křesťanští	křesťanský	k2eAgMnPc1d1
teologové	teolog	k1gMnPc1
příslušnost	příslušnost	k1gFnSc4
islámu	islám	k1gInSc2
k	k	k7c3
abrahámovským	abrahámovský	k2eAgNnPc3d1
náboženstvím	náboženství	k1gNnPc3
interpretují	interpretovat	k5eAaBmIp3nP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
muslimové	muslim	k1gMnPc1
věří	věřit	k5eAaImIp3nP
ve	v	k7c4
stejného	stejný	k2eAgMnSc4d1
Boha	bůh	k1gMnSc4
jako	jako	k9
obě	dva	k4xCgNnPc1
starší	starý	k2eAgNnPc1d2
náboženství	náboženství	k1gNnPc1
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
to	ten	k3xDgNnSc1
ovšem	ovšem	k9
zpochybňují	zpochybňovat	k5eAaImIp3nP
či	či	k8xC
ostře	ostro	k6eAd1
odmítají	odmítat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Islám	islám	k1gInSc1
v	v	k7c6
západních	západní	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
</s>
<s>
Imám	imám	k1gMnSc1
Velke	Velk	k1gFnSc2
mešity	mešita	k1gFnSc2
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
pro	pro	k7c4
belgické	belgický	k2eAgMnPc4d1
muslimy	muslim	k1gMnPc4
přestavěna	přestavět	k5eAaPmNgFnS
v	v	k7c4
mešitu	mešita	k1gFnSc4
na	na	k7c4
náklady	náklad	k1gInPc4
Saúdské	saúdský	k2eAgFnSc2d1
ArábieStřet	ArábieStřet	k1gFnSc2
islámu	islám	k1gInSc2
a	a	k8xC
ostatních	ostatní	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
si	se	k3xPyFc3
však	však	k9
uvědomuji	uvědomovat	k5eAaImIp1nS
i	i	k9
reformní	reformní	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
snahy	snaha	k1gFnPc1
islám	islám	k1gInSc4
reformovat	reformovat	k5eAaBmF
směrem	směr	k1gInSc7
k	k	k7c3
sekularismu	sekularismus	k1gInSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
tedy	tedy	k9
„	„	k?
<g/>
odstranit	odstranit	k5eAaPmF
<g/>
“	“	k?
politické	politický	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajně	krajně	k6eAd1
liberální	liberální	k2eAgMnPc1d1
reformisté	reformista	k1gMnPc1
islámu	islám	k1gInSc2
dokonce	dokonce	k9
problematický	problematický	k2eAgInSc1d1
–	–	k?
„	„	k?
<g/>
medínský	medínský	k2eAgInSc4d1
<g/>
“	“	k?
islám	islám	k1gInSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
normativy	normativ	k1gInPc4
určené	určený	k2eAgFnSc2d1
tehdejší	tehdejší	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reformu	reforma	k1gFnSc4
islámu	islám	k1gInSc2
zakládají	zakládat	k5eAaImIp3nP
především	především	k9
na	na	k7c6
islámu	islám	k1gInSc6
„	„	k?
<g/>
mekánském	mekánský	k2eAgMnSc6d1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radikální	radikální	k2eAgMnPc1d1
islamističtí	islamistický	k2eAgMnPc1d1
salafisté	salafista	k1gMnPc1
mají	mít	k5eAaImIp3nP
stále	stále	k6eAd1
větší	veliký	k2eAgInSc4d2
vliv	vliv	k1gInSc4
na	na	k7c4
muslimskou	muslimský	k2eAgFnSc4d1
komunitu	komunita	k1gFnSc4
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
analýzy	analýza	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
zavedení	zavedení	k1gNnSc1
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
šaría	šarí	k1gInSc2
podporuje	podporovat	k5eAaImIp3nS
72	#num#	k4
<g/>
%	%	kIx~
francouzských	francouzský	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
a	a	k8xC
35	#num#	k4
<g/>
%	%	kIx~
muslimů	muslim	k1gMnPc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
ospravedlnitelné	ospravedlnitelný	k2eAgNnSc4d1
použít	použít	k5eAaPmF
násilí	násilí	k1gNnSc4
na	na	k7c4
ochranu	ochrana	k1gFnSc4
víry	víra	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
Německu	Německo	k1gNnSc6
či	či	k8xC
Švédsku	Švédsko	k1gNnSc6
roste	růst	k5eAaImIp3nS
počet	počet	k1gInSc1
protižidovských	protižidovský	k2eAgInPc2d1
útoků	útok	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Tomáše	Tomáš	k1gMnSc2
Pojara	Pojar	k1gMnSc2
„	„	k?
<g/>
V	v	k7c6
prostředí	prostředí	k1gNnSc6
muslimských	muslimský	k2eAgFnPc2d1
přistěhovaleckých	přistěhovalecký	k2eAgFnPc2d1
komunit	komunita	k1gFnPc2
je	být	k5eAaImIp3nS
antisemitismus	antisemitismus	k1gInSc1
velmi	velmi	k6eAd1
silně	silně	k6eAd1
rozšířený	rozšířený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc1
komunity	komunita	k1gFnPc1
početnější	početní	k2eAgFnPc1d2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
zhoršuje	zhoršovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
jejich	jejich	k3xOp3gFnSc2
integrace	integrace	k1gFnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
narůstá	narůstat	k5eAaImIp3nS
počet	počet	k1gInSc1
projevů	projev	k1gInPc2
antisemitismu	antisemitismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
chápat	chápat	k5eAaImF
v	v	k7c6
tom	ten	k3xDgInSc6
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgNnPc1
náboženství	náboženství	k1gNnPc1
vyznávají	vyznávat	k5eAaImIp3nP
jediného	jediný	k2eAgMnSc4d1
Boha	bůh	k1gMnSc4
a	a	k8xC
svou	svůj	k3xOyFgFnSc4
víru	víra	k1gFnSc4
odvozují	odvozovat	k5eAaImIp3nP
od	od	k7c2
víry	víra	k1gFnSc2
Abrahámovy	Abrahámův	k2eAgFnSc2d1
(	(	kIx(
<g/>
→	→	k?
Lumen	lumen	k1gMnSc1
gentium	gentium	k1gNnSc1
<g/>
,	,	kIx,
odstavec	odstavec	k1gInSc1
16	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islám	islám	k1gInSc4
však	však	k9
neuznává	uznávat	k5eNaImIp3nS
křesťanské	křesťanský	k2eAgNnSc4d1
učení	učení	k1gNnSc4
o	o	k7c6
božství	božství	k1gNnSc6
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
a	a	k8xC
o	o	k7c6
Nejsvětější	nejsvětější	k2eAgFnSc6d1
Trojici	trojice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
súře	súra	k1gFnSc6
„	„	k?
<g/>
Prostřený	prostřený	k2eAgInSc1d1
stůl	stůl	k1gInSc1
<g/>
"	"	kIx"
se	se	k3xPyFc4
praví	pravit	k5eAaBmIp3nS,k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
A	a	k8xC
věru	věra	k1gFnSc4
jsou	být	k5eAaImIp3nP
nevěřící	nevěřící	k1gFnSc1
ti	ty	k3xPp2nSc3
<g/>
,	,	kIx,
kdož	kdož	k3yRnSc1
říkají	říkat	k5eAaImIp3nP
<g/>
:	:	kIx,
»	»	k?
<g/>
Mesiáš	Mesiáš	k1gMnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Mariin	Mariin	k2eAgMnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Bůh	bůh	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
«	«	k?
<g/>
“	“	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Islam	Islam	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Ten	ten	k3xDgInSc4
Misconceptions	Misconceptions	k1gInSc4
About	About	k2eAgInSc1d1
Islam	Islam	k1gInSc1
<g/>
:	:	kIx,
Misconception	Misconception	k1gInSc1
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
Southern	Southern	k1gMnSc1
California	Californium	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GARDET	GARDET	kA
<g/>
,	,	kIx,
L.	L.	kA
<g/>
;	;	kIx,
JOMIER	JOMIER	kA
<g/>
,	,	kIx,
J.	J.	kA
Islam	Islam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnPc2
<g/>
:	:	kIx,
Encyclopaedia	Encyclopaedium	k1gNnSc2
of	of	k?
Islam	Islam	k1gInSc1
Online	Onlin	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Lane	Lane	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
lexicon	lexicona	k1gFnPc2
[	[	kIx(
<g/>
formát	formát	k1gInSc4
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Major	major	k1gMnSc1
Religions	Religionsa	k1gFnPc2
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
—	—	k?
<g/>
Ranked	Ranked	k1gInSc4
by	by	kYmCp3nS
Number	Number	k1gMnSc1
of	of	k?
Adherents	Adherents	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
adherents	adherents	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Esposito	Esposita	k1gFnSc5
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
41	#num#	k4
<g/>
;	;	kIx,
Ghamidi	Ghamid	k1gMnPc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Sources	Sources	k1gInSc1
of	of	k?
Islam	Islam	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
14	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
islám	islám	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInSc4d1
náboženství	náboženství	k1gNnSc1
přehledně	přehledně	k6eAd1
-	-	kIx~
Info	Info	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
video	video	k1gNnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Muslimové	muslim	k1gMnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
formát	formát	k1gInSc1
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
Varianty	varianta	k1gFnSc2
<g/>
,	,	kIx,
Člověk	člověk	k1gMnSc1
v	v	k7c6
Tísni	tíseň	k1gFnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
564	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
92	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
93	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
178	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WHALING	WHALING	kA
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Religion	religion	k1gInSc1
in	in	k?
today	todaa	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
world	world	k6eAd1
<g/>
:	:	kIx,
the	the	k?
religious	religious	k1gInSc1
situation	situation	k1gInSc1
of	of	k?
the	the	k?
world	world	k1gInSc1
from	from	k6eAd1
1945	#num#	k4
to	ten	k3xDgNnSc1
the	the	k?
present	present	k1gInSc1
day	day	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
T	T	kA
&	&	k?
T	T	kA
Clark	Clark	k1gInSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9452	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
38	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gMnSc5
Future	Futur	k1gMnSc5
of	of	k?
World	Worldo	k1gNnPc2
Religions	Religions	k1gInSc1
<g/>
:	:	kIx,
Population	Population	k1gInSc1
Growth	Growtha	k1gFnPc2
Projections	Projectionsa	k1gFnPc2
<g/>
,	,	kIx,
2010-2050	2010-2050	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pew	Pew	k1gMnSc1
Research	Research	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Každý	každý	k3xTgMnSc1
pátý	pátý	k4xOgMnSc1
občan	občan	k1gMnSc1
EU	EU	kA
bude	být	k5eAaImBp3nS
za	za	k7c4
čtyřicet	čtyřicet	k4xCc4
let	léto	k1gNnPc2
muslim	muslim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Přiznání	přiznání	k1gNnSc2
starosty	starosta	k1gMnSc2
Bruselu	Brusel	k1gInSc2
<g/>
,	,	kIx,
sídla	sídlo	k1gNnSc2
EU	EU	kA
<g/>
:	:	kIx,
Radikální	radikální	k2eAgMnPc1d1
salafisté	salafista	k1gMnPc1
ovládli	ovládnout	k5eAaPmAgMnP
všechny	všechen	k3xTgFnPc4
mešity	mešita	k1gFnPc4
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
718	#num#	k4
<g/>
-	-	kIx~
<g/>
2165	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
564	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
564	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
564	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
564	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Ľuboš	Ľuboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
117	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
POTMĚŠIL	potměšil	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarī	Šarī	k1gNnPc1
<g/>
:	:	kIx,
úvod	úvod	k1gInSc1
do	do	k7c2
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
57	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
,	,	kIx,
str	str	kA
<g/>
.197	.197	k4
<g/>
↑	↑	k?
POTMĚŠIL	potměšil	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarī	Šarī	k1gNnPc1
<g/>
:	:	kIx,
úvod	úvod	k1gInSc1
do	do	k7c2
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc4
šarí	šarit	k5eAaImIp3nP,k5eAaPmIp3nP,k5eAaBmIp3nP
<g/>
´	´	k?
<g/>
a	a	k8xC
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
632	#num#	k4
<g/>
-	-	kIx~
<g/>
633	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
POTMĚŠIL	potměšil	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarī	Šarī	k1gNnPc1
<g/>
:	:	kIx,
úvod	úvod	k1gInSc1
do	do	k7c2
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
POTMĚŠIL	potměšil	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarī	Šarī	k1gNnPc1
<g/>
:	:	kIx,
úvod	úvod	k1gInSc1
do	do	k7c2
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
POTMĚŠIL	potměšil	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarī	Šarī	k1gNnPc1
<g/>
:	:	kIx,
úvod	úvod	k1gInSc1
do	do	k7c2
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
POTMĚŠIL	potměšil	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarī	Šarī	k1gNnPc1
<g/>
:	:	kIx,
úvod	úvod	k1gInSc1
do	do	k7c2
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
22	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
564	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
POTMĚŠIL	potměšil	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šarī	Šarī	k1gNnPc1
<g/>
:	:	kIx,
úvod	úvod	k1gInSc1
do	do	k7c2
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
128	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
.	.	kIx.
str	str	kA
<g/>
.	.	kIx.
196	#num#	k4
<g/>
-	-	kIx~
<g/>
199	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Ľuboš	Ľuboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
91	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Ľuboš	Ľuboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
91	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Ľuboš	Ľuboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
92	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Ľuboš	Ľuboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
96	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc4
modlitba	modlitba	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
596	#num#	k4
<g/>
-	-	kIx~
<g/>
597	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
104	#num#	k4
<g/>
-	-	kIx~
<g/>
106	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
107	#num#	k4
<g/>
-	-	kIx~
<g/>
108	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc1
půst	půst	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
615	#num#	k4
<g/>
-	-	kIx~
<g/>
616	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
110	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc4
pouť	pouť	k1gFnSc1
do	do	k7c2
Mekky	Mekka	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
610	#num#	k4
<g/>
-	-	kIx~
<g/>
611	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
564	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
155	#num#	k4
<g/>
-	-	kIx~
<g/>
156	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
130	#num#	k4
<g/>
-	-	kIx~
<g/>
131	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Súfismus	súfismus	k1gInSc4
<g/>
:	:	kIx,
dějiny	dějiny	k1gFnPc4
islámské	islámský	k2eAgFnSc2d1
mystiky	mystika	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gMnSc1
Qur	Qur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
an	an	k?
and	and	k?
Qur	Qur	k1gFnSc1
<g/>
'	'	kIx"
<g/>
anic	anic	k1gInSc1
Interpretation	Interpretation	k1gInSc1
(	(	kIx(
<g/>
tafsir	tafsir	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
islam	islam	k1gInSc1
<g/>
.	.	kIx.
<g/>
uga	uga	k?
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://blog.aktualne.centrum.cz/blogy/zdenek-muller.php?itemid=15718	http://blog.aktualne.centrum.cz/blogy/zdenek-muller.php?itemid=15718	k4
<g/>
↑	↑	k?
RÁDL	RÁDL	kA
<g/>
,	,	kIx,
Emanuel	Emanuel	k1gMnSc1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
filosofie	filosofie	k1gFnSc2
I.	I.	kA
<g/>
,	,	kIx,
s.	s.	k?
401	#num#	k4
<g/>
–	–	k?
<g/>
406	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Předmluva	předmluva	k1gFnSc1
Koránu	korán	k1gInSc2
od	od	k7c2
I.	I.	kA
Hrbka	Hrbek	k1gMnSc4
Archivováno	archivován	k2eAgNnSc4d1
13	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
neplatný	platný	k2eNgInSc1d1
odkaz	odkaz	k1gInSc1
!	!	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
V	v	k7c6
samotném	samotný	k2eAgInSc6d1
Koránku	Koránek	k1gInSc6
např.	např.	kA
Súra	súra	k1gFnSc1
Al-Nahl	Al-Nahl	k1gFnSc1
<g/>
,	,	kIx,
verš	verš	k1gInSc1
<g/>
:	:	kIx,
98	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
Súra	súra	k1gFnSc1
Al-Muzzamil	Al-Muzzamil	k1gFnSc1
<g/>
,	,	kIx,
verš	verš	k1gInSc1
<g/>
:	:	kIx,
4	#num#	k4
<g/>
↑	↑	k?
HORYNA	Horyna	k1gMnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
Helena	Helena	k1gFnSc1
PAVLINCOVÁ	PAVLINCOVÁ	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judaismus	judaismus	k1gInSc1
<g/>
,	,	kIx,
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
podstatně	podstatně	k6eAd1
přeprac	přeprac	k6eAd1
<g/>
.	.	kIx.
a	a	k8xC
rozš	rozš	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Heslo	heslo	k1gNnSc1
islám	islám	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
564	#num#	k4
<g/>
-	-	kIx~
<g/>
567	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
45	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
48	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
48	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
puškou	puška	k1gFnSc7
a	a	k8xC
Koránem	korán	k1gInSc7
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
49	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Why	Why	k1gFnSc2
Muslims	Muslimsa	k1gFnPc2
are	ar	k1gInSc5
the	the	k?
world	world	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
fastest-growing	fastest-growing	k1gInSc1
religious	religious	k1gMnSc1
group	group	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pew	Pew	k1gMnSc1
Research	Research	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc5
Future	Futur	k1gMnSc5
of	of	k?
the	the	k?
Global	globat	k5eAaImAgMnS
Muslim	muslim	k1gMnSc1
Population	Population	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pew	Pew	k1gMnSc1
Research	Research	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Tatarští	tatarský	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
v	v	k7c6
polsko-litevském	polsko-litevský	k2eAgInSc6d1
státě	stát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navýchod	Navýchod	k1gInSc1
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Turci	Turek	k1gMnPc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Sűlejman	Sűlejman	k1gMnSc1
"	"	kIx"
<g/>
Nádherný	nádherný	k2eAgMnSc1d1
<g/>
"	"	kIx"
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
výboje	výboj	k1gInPc1
do	do	k7c2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
1533	#num#	k4
<g/>
–	–	k?
<g/>
1566	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Znojmo	Znojmo	k1gNnSc1
v	v	k7c6
tureckém	turecký	k2eAgNnSc6d1
ohrožení	ohrožení	k1gNnSc6
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
7	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemský	znojemský	k2eAgInSc1d1
týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Ráb	Ráb	k1gInSc1
oko	oko	k1gNnSc1
klovající	klovající	k2eAgInSc4d1
aneb	aneb	k?
Sifon	sifon	k1gInSc4
v	v	k7c6
Györu	Györ	k1gInSc6
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
OSTŘANSKÝ	OSTŘANSKÝ	kA
<g/>
,	,	kIx,
Bronislav	Bronislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odraz	odraz	k1gInSc1
islámu	islám	k1gInSc2
v	v	k7c6
českém	český	k2eAgNnSc6d1
písemnictví	písemnictví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
DINGIR	DINGIR	kA
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
-	-	kIx~
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
1371	#num#	k4
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Český	český	k2eAgMnSc1d1
diplomat	diplomat	k1gMnSc1
na	na	k7c6
dvoře	dvůr	k1gInSc6
sultánově	sultánův	k2eAgInSc6d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
a	a	k8xC
země	zem	k1gFnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Příhody	příhoda	k1gFnSc2
Václava	Václav	k1gMnSc4
Vratislava	Vratislav	k1gMnSc4
<g/>
,	,	kIx,
svobodného	svobodný	k2eAgMnSc4d1
pána	pán	k1gMnSc4
z	z	k7c2
Mitrovic	Mitrovice	k1gFnPc2
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Hrozí	hrozit	k5eAaImIp3nS
opakování	opakování	k1gNnSc4
historie	historie	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Mohamedáni	mohamedán	k1gMnPc1
kdysi	kdysi	k6eAd1
Moravu	Morava	k1gFnSc4
plenili	plenit	k5eAaImAgMnP
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Turecké	turecký	k2eAgInPc1d1
vojenské	vojenský	k2eAgInPc1d1
výboje	výboj	k1gInPc1
na	na	k7c6
území	území	k1gNnSc6
dnešného	dnešný	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Dvě	dva	k4xCgFnPc1
stě	sto	k4xCgFnPc1
Valachů	valach	k1gInPc2
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
350	#num#	k4
lety	léto	k1gNnPc7
se	se	k3xPyFc4
Turek	Turek	k1gMnSc1
jal	jmout	k5eAaPmAgMnS
plenit	plenit	k5eAaImF
Moravu	Morava	k1gFnSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Znojmo	Znojmo	k1gNnSc1
v	v	k7c6
tureckém	turecký	k2eAgNnSc6d1
ohrožení	ohrožení	k1gNnSc6
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
7	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemský	znojemský	k2eAgInSc1d1
týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lánové	lánový	k2eAgInPc1d1
rejstříky	rejstřík	k1gInPc1
(	(	kIx(
<g/>
1656	#num#	k4
<g/>
–	–	k?
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
ilcik	ilcik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
29	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
29	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Štefan	Štefan	k1gMnSc1
Pilárik	Pilárik	k1gMnSc1
<g/>
:	:	kIx,
Turcico-Tartarica	Turcico-Tartarica	k1gMnSc1
crudelitas	crudelitas	k1gMnSc1
(	(	kIx(
<g/>
Turecko-tatárska	Turecko-tatárska	k1gFnSc1
ukrutnosť	ukrutnostit	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Poturčenec	poturčenec	k1gMnSc1
horší	zlý	k2eAgMnSc1d2
Turka	turek	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
6	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navýchod	Navýchod	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Jan	Jan	k1gMnSc1
Josef	Josef	k1gMnSc1
Václav	Václav	k1gMnSc1
Radecký	Radecký	k1gMnSc1
z	z	k7c2
Radče	Radč	k1gFnSc2
<g/>
:	:	kIx,
Život	život	k1gInSc1
na	na	k7c6
bitevním	bitevní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
Archivováno	archivovat	k5eAaBmNgNnS
7	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
první	první	k4xOgMnSc1
„	„	k?
<g/>
maglajz	maglajz	k?
<g/>
“	“	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
a	a	k8xC
země	zem	k1gFnPc1
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://dejinyasoucasnost.cz/archiv/2005/10/musela-ji-vybojovat-infanterie-/	http://dejinyasoucasnost.cz/archiv/2005/10/musela-ji-vybojovat-infanterie-/	k4
<g/>
↑	↑	k?
NW	NW	kA
<g/>
,	,	kIx,
1615	#num#	k4
L.	L.	kA
St	St	kA
<g/>
;	;	kIx,
WASHINGTON	Washington	k1gInSc1
<g/>
,	,	kIx,
Suite	Suit	k1gInSc5
800	#num#	k4
<g/>
;	;	kIx,
INQUIRIES	INQUIRIES	kA
<g/>
,	,	kIx,
DC	DC	kA
20036	#num#	k4
USA202-419-4300	USA202-419-4300	k1gFnPc1
|	|	kIx~
Main	Main	k1gInSc1
<g/>
202	#num#	k4
<g/>
-	-	kIx~
<g/>
419	#num#	k4
<g/>
-	-	kIx~
<g/>
4349	#num#	k4
|	|	kIx~
Fax	fax	k1gInSc1
<g/>
202	#num#	k4
<g/>
-	-	kIx~
<g/>
419	#num#	k4
<g/>
-	-	kIx~
<g/>
4372	#num#	k4
|	|	kIx~
Media	medium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslim	muslim	k1gMnSc1
Population	Population	k1gInSc1
Growth	Growth	k1gMnSc1
in	in	k?
Europe	Europ	k1gMnSc5
|	|	kIx~
Pew	Pew	k1gMnSc6
Research	Resear	k1gFnPc6
Center	centrum	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-11-29	2017-11-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ibn	Ibn	k1gMnSc1
Warraq	Warraq	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Quest	Quest	k1gMnSc1
for	forum	k1gNnPc2
Historical	Historical	k1gMnSc1
Muhammad	Muhammad	k1gInSc1
(	(	kIx(
<g/>
Amherst	Amherst	k1gInSc1
<g/>
,	,	kIx,
Mass	Mass	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
<g/>
Prometheus	Prometheus	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
103	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bible	bible	k1gFnSc2
in	in	k?
Mohammedian	Mohammedian	k1gMnSc1
Literature	Literatur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
,	,	kIx,
by	by	kYmCp3nS
Kaufmann	Kaufmann	k1gMnSc1
Kohler	Kohler	k1gMnSc1
Duncan	Duncan	k1gMnSc1
B.	B.	kA
McDonald	McDonald	k1gMnSc1
<g/>
,	,	kIx,
Jewish	Jewish	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gMnSc1
April	April	k1gMnSc1
22	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Robert	Robert	k1gMnSc1
Spencer	Spencer	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Islam	Islam	k1gInSc1
Unveiled	Unveiled	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
22	#num#	k4
<g/>
,	,	kIx,
63	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
Encounter	Encounter	k1gInSc1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
893554	#num#	k4
<g/>
-	-	kIx~
<g/>
77	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
↑	↑	k?
http://www.freedomhouse.org/template.cfm?page=22&	http://www.freedomhouse.org/template.cfm?page=22&	k?
See	See	k1gMnPc2
also	also	k1gMnSc1
Timothy	Timotha	k1gFnSc2
Garton	Garton	k1gInSc1
Ash	Ash	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islam	Islam	k1gInSc1
in	in	k?
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
www.nybooks.com	www.nybooks.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Review	Review	k1gFnSc1
of	of	k?
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Timothy	Timotha	k1gFnSc2
Garton	Garton	k1gInSc4
Ash	Ash	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islam	Islam	k1gInSc1
in	in	k?
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
www.nybooks.com	www.nybooks.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Review	Review	k1gFnSc1
of	of	k?
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tariq	Tariq	k1gMnSc4
Modood	Modood	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Multiculturalism	Multiculturalism	k1gInSc1
<g/>
,	,	kIx,
Muslims	Muslims	k1gInSc1
and	and	k?
Citizenship	Citizenship	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
European	European	k1gMnSc1
Approach	Approach	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
st	st	kA
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
415	#num#	k4
<g/>
-	-	kIx~
<g/>
35515	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
29	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
<g/>
:	:	kIx,
Skutečnou	skutečný	k2eAgFnSc7d1
hrozbou	hrozba	k1gFnSc7
pro	pro	k7c4
Západ	západ	k1gInSc4
jsou	být	k5eAaImIp3nP
Saúdové	Saúdové	k2eAgNnPc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
:	:	kIx,
Spojenec	spojenec	k1gMnSc1
Západu	západ	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
doma	doma	k6eAd1
vládne	vládnout	k5eAaImIp3nS
tvrdou	tvrdý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Přesunou	přesunout	k5eAaPmIp3nP
se	se	k3xPyFc4
čečenští	čečenský	k2eAgMnPc1d1
rebelové	rebel	k1gMnPc1
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
na	na	k7c4
olympiádu	olympiáda	k1gFnSc4
v	v	k7c6
Soči	Soči	k1gNnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Sýrie	Sýrie	k1gFnSc1
ukrývá	ukrývat	k5eAaImIp3nS
památky	památka	k1gFnPc4
na	na	k7c6
utajených	utajený	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
je	on	k3xPp3gInPc4
nezničili	zničit	k5eNaPmAgMnP
extremisté	extremista	k1gMnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Hamas	Hamasa	k1gFnPc2
<g/>
,	,	kIx,
Al	ala	k1gFnPc2
Kajda	kajda	k1gFnSc1
a	a	k8xC
Gaza	Gaza	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Analyses	Analyses	k1gMnSc1
–	–	k?
Wahhabism	Wahhabism	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Public	publicum	k1gNnPc2
Broadcasting	Broadcasting	k1gInSc1
Service	Service	k1gFnSc1
(	(	kIx(
<g/>
PBS	PBS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠTAMPACH	ŠTAMPACH	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
,	,	kIx,
O.	O.	kA
Islám	islám	k1gInSc1
a	a	k8xC
evropská	evropský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
DINGIR	DINGIR	kA
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
1371	#num#	k4
<g/>
↑	↑	k?
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammad	Muhammad	k1gInSc4
a	a	k8xC
Korán	korán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Korán	korán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
794	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
444	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
32	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
a	a	k8xC
PETRÁČEK	Petráček	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammad	Muhammad	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
225	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
61	#num#	k4
a	a	k8xC
96	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
TAUER	TAUER	kA
<g/>
,	,	kIx,
F.	F.	kA
<g/>
,	,	kIx,
Svět	svět	k1gInSc1
Islámu	islám	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
59	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
např.	např.	kA
TAUER	TAUER	kA
<g/>
,	,	kIx,
F.	F.	kA
<g/>
,	,	kIx,
Svět	svět	k1gInSc1
Islámu	islám	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
90	#num#	k4
<g/>
-	-	kIx~
<g/>
91	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MÜLLER	MÜLLER	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islám	islám	k1gInSc4
a	a	k8xC
islamisus	islamisus	k1gInSc4
<g/>
:	:	kIx,
dilema	dilema	k1gNnSc1
náboženství	náboženství	k1gNnSc2
a	a	k8xC
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
256	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1818	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammad	Muhammad	k1gInSc4
a	a	k8xC
Korán	korán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Korán	korán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
794	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
444	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
str	str	kA
<g/>
.	.	kIx.
96	#num#	k4
<g/>
;	;	kIx,
citovaný	citovaný	k2eAgInSc1d1
verš	verš	k1gInSc1
koránu	korán	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
608	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
http://www.katyd.cz/clanky/nazory/mame-s-muslimy-stejneho-boha.html	http://www.katyd.cz/clanky/nazory/mame-s-muslimy-stejneho-boha.html	k1gInSc1
<g/>
↑	↑	k?
http://www.rkc-stetsko.tode.cz/mame-s-muslimy-stejneho-boha-odpoved-prof-halikovi/	http://www.rkc-stetsko.tode.cz/mame-s-muslimy-stejneho-boha-odpoved-prof-halikovi/	k?
<g/>
↑	↑	k?
http://www.duseahvezdy.cz/2016/09/13/kardinal-burke-tvrdit-ze-islam-cti-krestanskeho-boha-a-je-tedy-mirumilovny-je-vysoce-pochybne/	http://www.duseahvezdy.cz/2016/09/13/kardinal-burke-tvrdit-ze-islam-cti-krestanskeho-boha-a-je-tedy-mirumilovny-je-vysoce-pochybne/	k4
<g/>
↑	↑	k?
Umírnění	umírněný	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
sepsali	sepsat	k5eAaPmAgMnP
deklaraci	deklarace	k1gFnSc4
<g/>
,	,	kIx,
vyzývají	vyzývat	k5eAaImIp3nP
k	k	k7c3
humanismu	humanismus	k1gInSc3
a	a	k8xC
potírají	potírat	k5eAaImIp3nP
teokracii	teokracie	k1gFnSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POTMĚŠIL	potměšil	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šaría	Šaría	k1gFnSc1
–	–	k?
úvod	úvod	k1gInSc4
do	do	k7c2
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
224	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
4379	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Radikalizace	radikalizace	k1gFnSc1
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francii	Franci	k1gMnPc7
straší	strašit	k5eAaImIp3nS
sílící	sílící	k2eAgInSc1d1
vliv	vliv	k1gInSc1
salafistů	salafist	k1gInPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Zavedení	zavedení	k1gNnSc1
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
šaría	šarí	k1gInSc2
podporuje	podporovat	k5eAaImIp3nS
72	#num#	k4
procent	procento	k1gNnPc2
muslimů	muslim	k1gMnPc2
ve	v	k7c4
Francii	Francie	k1gFnSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Security	Securit	k1gInPc4
magazín	magazín	k1gInSc1
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016.1	2016.1	k4
2	#num#	k4
Z	z	k7c2
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
radikální	radikální	k2eAgInSc4d1
islám	islám	k1gInSc4
<g/>
,	,	kIx,
z	z	k7c2
druhé	druhý	k4xOgFnSc2
neonacisti	neonacisti	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židům	Žid	k1gMnPc3
se	se	k3xPyFc4
žije	žít	k5eAaImIp3nS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
dál	daleko	k6eAd2
hůř	zle	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
„	„	k?
<g/>
Arabské	arabský	k2eAgFnSc2d1
ulice	ulice	k1gFnSc2
<g/>
“	“	k?
ve	v	k7c6
švédských	švédský	k2eAgNnPc6d1
městech	město	k1gNnPc6
žijí	žít	k5eAaImIp3nP
větší	veliký	k2eAgInPc1d2
svobodou	svoboda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
'	'	kIx"
<g/>
ABD	ABD	kA
AL-	AL-	k1gMnSc1
<g/>
'	'	kIx"
<g/>
AZÍM	AZÍM	kA
<g/>
,	,	kIx,
Šaríf	Šaríf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženy	žena	k1gFnSc2
v	v	k7c6
islámu	islám	k1gInSc6
a	a	k8xC
ženy	žena	k1gFnPc1
v	v	k7c6
židovsko-křesťanské	židovsko-křesťanský	k2eAgFnSc6d1
tradici	tradice	k1gFnSc6
<g/>
:	:	kIx,
mýtus	mýtus	k1gInSc1
a	a	k8xC
realita	realita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
Muslimská	muslimský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
79	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904373	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BAHBOUH	BAHBOUH	kA
<g/>
,	,	kIx,
Charif	Charif	k1gMnSc1
<g/>
,	,	kIx,
FLEISSIG	FLEISSIG	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
a	a	k8xC
RACZYŃSKI	RACZYŃSKI	kA
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brandýs	Brandýs	k1gInSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
:	:	kIx,
Dar	dar	k1gInSc1
Ibn	Ibn	k1gMnSc1
Rushd	Rushd	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
366	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86149	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ELIADE	ELIADE	kA
<g/>
,	,	kIx,
Mircea	Mircea	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
náboženského	náboženský	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
:	:	kIx,
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Muhammada	Muhammada	k1gFnSc1
po	po	k7c4
dobu	doba	k1gFnSc4
křesťanských	křesťanský	k2eAgFnPc2d1
reforem	reforma	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
344	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86005	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Islám	islám	k1gInSc1
<g/>
:	:	kIx,
náboženství	náboženství	k1gNnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
budoucnost	budoucnost	k1gFnSc1
=	=	kIx~
[	[	kIx(
<g/>
Orig	Orig	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
The	The	k1gMnSc1
Islamic	Islamic	k1gMnSc1
world	world	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Richard	Richard	k1gMnSc1
Janda	Janda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
379	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7217	#num#	k4
<g/>
-	-	kIx~
<g/>
628	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Islámská	islámský	k2eAgFnSc1d1
čítanka	čítanka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studijní	studijní	k2eAgFnSc1d1
antologie	antologie	k1gFnSc1
arabského	arabský	k2eAgNnSc2d1
islámského	islámský	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Editoři	editor	k1gMnPc1
<g/>
:	:	kIx,
Ondřej	Ondřej	k1gMnSc1
Beránek	Beránek	k1gMnSc1
<g/>
,	,	kIx,
Bronislav	Bronislav	k1gMnSc1
Ostřanský	Ostřanský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Ťupek	ťupka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
,	,	kIx,
327	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978-80-7308-982-5	978-80-7308-982-5	k4
(	(	kIx(
<g/>
print	print	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-7308-983-2	978-80-7308-983-2	k4
(	(	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
pdf	pdf	k?
<g/>
)	)	kIx)
</s>
<s>
Lidská	lidský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
v	v	k7c6
Islámu	islám	k1gInSc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Islámská	islámský	k2eAgFnSc1d1
nadace	nadace	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c4
nakl	nakl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NÚR	NÚR	kA
-	-	kIx~
Fethi	Feth	k1gInSc6
Ben	Ben	k1gInSc4
Hassine	Hassin	k1gInSc5
Mnasria	Mnasrium	k1gNnPc4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
47	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903196	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
QARADĀ	QARADĀ	k?
<g/>
,	,	kIx,
Jū	Jū	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povolené	povolený	k2eAgInPc1d1
a	a	k8xC
zakázané	zakázaný	k2eAgInPc4d1
v	v	k7c6
Islámu	islám	k1gInSc6
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Al-Halál	Al-Halál	k1gInSc1
wal	wal	k?
Harám	Hara	k1gFnPc3
fil	fil	k?
Islám	islám	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Robert	Robert	k1gMnSc1
Hýsek	Hýsek	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Islámská	islámský	k2eAgFnSc1d1
nadace	nadace	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2004	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
252	#num#	k4
s.	s.	k?
</s>
<s>
TAUER	TAUER	kA
<g/>
,	,	kIx,
Felix	Felix	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
702	#num#	k4
<g/>
-	-	kIx~
<g/>
1828	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
821	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
444	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
OSTŘANSKÝ	OSTŘANSKÝ	kA
<g/>
,	,	kIx,
Bronislav	Bronislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
islámu	islám	k1gInSc2
a	a	k8xC
muslimské	muslimský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
404	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WERNER	Werner	k1gMnSc1
<g/>
,	,	kIx,
Helmut	Helmut	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámská	islámský	k2eAgFnSc1d1
Kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
<g/>
:	:	kIx,
představy	představa	k1gFnSc2
islámu	islám	k1gInSc2
o	o	k7c6
onom	onen	k3xDgInSc6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Pragma	Pragma	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2003	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
237	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7205	#num#	k4
<g/>
-	-	kIx~
<g/>
948	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
:	:	kIx,
illustrovaná	illustrovaný	k2eAgFnSc1d1
encyklopaedie	encyklopaedie	k1gFnSc1
obecných	obecný	k2eAgFnPc2d1
vědomostí	vědomost	k1gFnPc2
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1897	#num#	k4
<g/>
.	.	kIx.
1072	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
hesla	heslo	k1gNnPc4
„	„	k?
<g/>
Islám	islám	k1gInSc1
<g/>
"	"	kIx"
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
776	#num#	k4
<g/>
–	–	k?
<g/>
785	#num#	k4
a	a	k8xC
„	„	k?
<g/>
Islámská	islámský	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
"	"	kIx"
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
785	#num#	k4
<g/>
–	–	k?
<g/>
788	#num#	k4
<g/>
;	;	kIx,
autorem	autor	k1gMnSc7
obou	dva	k4xCgInPc2
článků	článek	k1gInPc2
je	být	k5eAaImIp3nS
Rudolf	Rudolf	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
RÁDL	RÁDL	kA
<g/>
,	,	kIx,
Emanuel	Emanuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
filosofie	filosofie	k1gFnSc2
I.	I.	kA
<g/>
:	:	kIx,
starověk	starověk	k1gInSc1
a	a	k8xC
středověk	středověk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Votobia	Votobia	k1gFnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7220	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
63	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Pět	pět	k4xCc1
pilířů	pilíř	k1gInPc2
islámu	islám	k1gInSc2
</s>
<s>
Islám	islám	k1gInSc1
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegovině	Hercegovina	k1gFnSc6
</s>
<s>
Islám	islám	k1gInSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Islám	islám	k1gInSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
</s>
<s>
Islámská	islámský	k2eAgFnSc1d1
expanze	expanze	k1gFnSc1
</s>
<s>
Islámská	islámský	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
</s>
<s>
Mohamed	Mohamed	k1gMnSc1
</s>
<s>
Hidžáb	Hidžáb	k1gMnSc1
</s>
<s>
Džihád	džihád	k1gInSc1
</s>
<s>
Islámský	islámský	k2eAgInSc1d1
terorismus	terorismus	k1gInSc1
</s>
<s>
Ženy	žena	k1gFnPc1
v	v	k7c6
islámu	islám	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
islám	islám	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
islám	islám	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Islám	islám	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Kategorie	kategorie	k1gFnSc1
Islám	islám	k1gInSc1
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
IslamWeb	IslamWba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
islámské	islámský	k2eAgFnSc2d1
nadace	nadace	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
Brně	Brno	k1gNnSc6
</s>
<s>
Infomuslim	Infomuslim	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
dění	dění	k1gNnSc2
v	v	k7c6
muslimské	muslimský	k2eAgFnSc6d1
komunitě	komunita	k1gFnSc6
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
Islámského	islámský	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
koránu	korán	k1gInSc2
s	s	k7c7
vyhledáváním	vyhledávání	k1gNnSc7
</s>
<s>
Islám	islám	k1gInSc1
(	(	kIx(
<g/>
Cyklus	cyklus	k1gInSc1
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Náboženství	náboženství	k1gNnSc1
světa	svět	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
video	video	k1gNnSc1
on-line	on-lin	k1gInSc5
v	v	k7c6
archivu	archiv	k1gInSc6
ČT	ČT	kA
</s>
<s>
Články	článek	k1gInPc1
v	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
-	-	kIx~
wikiislam	wikiislam	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
-	-	kIx~
informace	informace	k1gFnPc1
o	o	k7c6
islámu	islám	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Islám	islám	k1gInSc1
Víra	víra	k1gFnSc1
a	a	k8xC
praktiky	praktika	k1gFnPc1
</s>
<s>
Jedinost	jedinost	k1gFnSc1
Boha	bůh	k1gMnSc2
•	•	k?
Vyznání	vyznání	k1gNnSc1
víry	víra	k1gFnSc2
•	•	k?
Modlitba	modlitba	k1gFnSc1
•	•	k?
Půst	půst	k1gInSc1
•	•	k?
Pouť	pouť	k1gFnSc1
•	•	k?
Almužna	almužna	k1gFnSc1
•	•	k?
Mešita	mešita	k1gFnSc1
Osobnosti	osobnost	k1gFnSc2
</s>
<s>
Mohamed	Mohamed	k1gMnSc1
•	•	k?
Abú	abú	k1gMnSc1
Bakr	Bakr	k1gMnSc1
•	•	k?
Umar	Umar	k1gMnSc1
ibn	ibn	k?
al-Chattáb	al-Chattáb	k1gInSc1
•	•	k?
Uthmán	Uthmán	k2eAgInSc1d1
ibn	ibn	k?
Affán	Affán	k1gInSc1
•	•	k?
Alí	Alí	k1gFnSc2
ibn	ibn	k?
Abí	Abí	k1gMnSc1
Tálib	Tálib	k1gMnSc1
•	•	k?
Mohamedovi	Mohamedův	k2eAgMnPc1d1
současníci	současník	k1gMnPc1
•	•	k?
Členové	člen	k1gMnPc1
Mohamedovy	Mohamedův	k2eAgFnSc2d1
domácnosti	domácnost	k1gFnSc2
•	•	k?
Proroci	prorok	k1gMnPc1
islámu	islám	k1gInSc2
Náboženské	náboženský	k2eAgInPc4d1
texty	text	k1gInPc4
</s>
<s>
Korán	korán	k1gInSc1
•	•	k?
Sunna	sunna	k1gFnSc1
•	•	k?
Hadís	Hadís	k1gInSc4
Větve	větev	k1gFnSc2
islámu	islám	k1gInSc2
</s>
<s>
Sunnité	sunnita	k1gMnPc1
(	(	kIx(
<g/>
Hanífovský	Hanífovský	k2eAgMnSc1d1
•	•	k?
Málikovský	Málikovský	k2eAgInSc1d1
•	•	k?
Šáfiovský	Šáfiovský	k2eAgInSc1d1
•	•	k?
Hanbalovský	Hanbalovský	k2eAgInSc1d1
•	•	k?
Záhirovský	Záhirovský	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Šíité	šíita	k1gMnPc1
(	(	kIx(
<g/>
Ismá	Ismá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ílíja	ílíja	k1gFnSc1
(	(	kIx(
<g/>
Nizáríja	Nizáríja	k1gFnSc1
(	(	kIx(
<g/>
Asasíni	Asasín	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
Mustálíja	Mustálíja	k1gFnSc1
•	•	k?
Drúzové	drúzový	k2eAgFnPc4d1
<g/>
)	)	kIx)
•	•	k?
Isná	Isná	k1gFnSc1
ašaríja	ašaríja	k1gMnSc1
(	(	kIx(
<g/>
Dža	Dža	k1gMnSc1
<g/>
'	'	kIx"
<g/>
farovský	farovský	k1gMnSc1
•	•	k?
Alavité	Alavitý	k2eAgNnSc1d1
•	•	k?
Alevité	Alevitý	k2eAgFnPc1d1
<g/>
)	)	kIx)
•	•	k?
Zajdíja	Zajdíja	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Súfismus	súfismus	k1gInSc1
(	(	kIx(
<g/>
Bektašíja	Bektašíj	k2eAgFnSc1d1
•	•	k?
Čištíja	Čištíj	k2eAgFnSc1d1
•	•	k?
Mauláwíja	Mauláwíja	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Cháridža	Cháridža	k1gFnSc1
(	(	kIx(
<g/>
Ibádíja	Ibádíja	k1gFnSc1
<g/>
)	)	kIx)
Sociopolitické	Sociopolitický	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
</s>
<s>
Islámský	islámský	k2eAgInSc1d1
svět	svět	k1gInSc1
•	•	k?
Umění	umění	k1gNnSc1
•	•	k?
Architektura	architektura	k1gFnSc1
•	•	k?
Města	město	k1gNnSc2
•	•	k?
Kalendář	kalendář	k1gInSc1
•	•	k?
Věda	věda	k1gFnSc1
•	•	k?
Filosofie	filosofie	k1gFnSc1
•	•	k?
Náboženští	náboženský	k2eAgMnPc1d1
vůdcové	vůdce	k1gMnPc1
•	•	k?
Ženy	žena	k1gFnPc1
v	v	k7c6
islámu	islám	k1gInSc6
•	•	k?
Politický	politický	k2eAgInSc1d1
islám	islám	k1gInSc1
•	•	k?
Liberální	liberální	k2eAgInSc1d1
islám	islám	k1gInSc1
•	•	k?
Džihád	džihád	k1gInSc1
•	•	k?
Salafismus	Salafismus	k1gInSc1
(	(	kIx(
<g/>
Wahhábismus	Wahhábismus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Koránští	Koránský	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
•	•	k?
Ahmadíja	Ahmadíj	k1gInSc2
•	•	k?
Islamismus	islamismus	k1gInSc1
•	•	k?
Islámský	islámský	k2eAgInSc1d1
terorismus	terorismus	k1gInSc1
Související	související	k2eAgInSc1d1
články	článek	k1gInPc4
</s>
<s>
Káfir	Káfir	k1gInSc1
•	•	k?
Kritika	kritika	k1gFnSc1
islámu	islám	k1gInSc2
•	•	k?
LGBT	LGBT	kA
v	v	k7c6
islámu	islám	k1gInSc6
•	•	k?
Šaría	Šarí	k2eAgFnSc1d1
Kategorie	kategorie	k1gFnSc1
článků	článek	k1gInPc2
o	o	k7c6
islámu	islám	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Náboženství	náboženství	k1gNnSc1
|	|	kIx~
Islám	islám	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
114798	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4027743-4	4027743-4	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
7934	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85068390	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85068390	#num#	k4
</s>
