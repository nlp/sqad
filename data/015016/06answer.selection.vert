<s desamb="1">
Muslimové	muslim	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Bůh	bůh	k1gMnSc1
Mohamedovi	Mohamed	k1gMnSc3
zjevil	zjevit	k5eAaPmAgMnS
Korán	korán	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
společně	společně	k6eAd1
se	s	k7c7
Sunnou	sunna	k1gFnSc7
(	(	kIx(
<g/>
Mohamedovy	Mohamedův	k2eAgInPc1d1
činy	čin	k1gInPc1
a	a	k8xC
slova	slovo	k1gNnPc1
<g/>
)	)	kIx)
považují	považovat	k5eAaImIp3nP
za	za	k7c4
základní	základní	k2eAgInPc4d1
prameny	pramen	k1gInPc4
islámu	islám	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Muslimové	muslim	k1gMnPc1
jsou	být	k5eAaImIp3nP
povinni	povinen	k2eAgMnPc1d1
dodržovat	dodržovat	k5eAaImF
pět	pět	k4xCc1
pilířů	pilíř	k1gInPc2
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
pět	pět	k4xCc4
povinností	povinnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
muslimy	muslim	k1gMnPc4
spojují	spojovat	k5eAaImIp3nP
ve	v	k7c4
společenství	společenství	k1gNnSc4
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgNnPc1d1
jak	jak	k8xC,k8xS
pravidla	pravidlo	k1gNnPc1
uctívání	uctívání	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
islámské	islámský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
(	(	kIx(
<g/>
šaría	šaría	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>