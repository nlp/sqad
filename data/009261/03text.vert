<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
svislé	svislý	k2eAgInPc4d1	svislý
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnSc2d1	žlutá
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
je	být	k5eAaImIp3nS	být
umístěný	umístěný	k2eAgInSc1d1	umístěný
moldavský	moldavský	k2eAgInSc1d1	moldavský
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
(	(	kIx(	(
<g/>
orel	orel	k1gMnSc1	orel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
až	až	k9	až
2010	[number]	k4	2010
umístěn	umístit	k5eAaPmNgInS	umístit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
(	(	kIx(	(
<g/>
lícové	lícový	k2eAgFnSc6d1	lícová
<g/>
)	)	kIx)	)
straně	strana	k1gFnSc6	strana
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnSc4d1	zadní
(	(	kIx(	(
<g/>
rubovou	rubový	k2eAgFnSc4d1	rubová
<g/>
)	)	kIx)	)
stranu	strana	k1gFnSc4	strana
tvořila	tvořit	k5eAaImAgFnS	tvořit
trikolóra	trikolóra	k1gFnSc1	trikolóra
bez	bez	k7c2	bez
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
národních	národní	k2eAgFnPc2d1	národní
vlajek	vlajka	k1gFnPc2	vlajka
s	s	k7c7	s
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Odlišné	odlišný	k2eAgFnPc4d1	odlišná
strany	strana	k1gFnPc4	strana
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
vlajka	vlajka	k1gFnSc1	vlajka
Paraguaye	Paraguay	k1gFnSc2	Paraguay
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
zrcadlově	zrcadlově	k6eAd1	zrcadlově
obrácený	obrácený	k2eAgInSc1d1	obrácený
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
moldavské	moldavský	k2eAgFnSc2d1	Moldavská
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
vztyčovalo	vztyčovat	k5eAaImAgNnS	vztyčovat
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1918	[number]	k4	1918
modro-červenou	modro-červený	k2eAgFnSc4d1	modro-červená
bikolóru	bikolóra	k1gFnSc4	bikolóra
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
historické	historický	k2eAgFnPc4d1	historická
moldavské	moldavský	k2eAgFnPc4d1	Moldavská
barvy	barva	k1gFnPc4	barva
zastoupené	zastoupený	k2eAgNnSc1d1	zastoupené
i	i	k9	i
na	na	k7c6	na
rumunské	rumunský	k2eAgFnSc6d1	rumunská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
zavedená	zavedený	k2eAgFnSc1d1	zavedená
s	s	k7c7	s
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
Moldavské	moldavský	k2eAgFnSc2d1	Moldavská
SSR	SSR	kA	SSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
vlajky	vlajka	k1gFnPc1	vlajka
ostatních	ostatní	k2eAgFnPc2d1	ostatní
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
byla	být	k5eAaImAgFnS	být
i	i	k9	i
vlajka	vlajka	k1gFnSc1	vlajka
MSSR	MSSR	kA	MSSR
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
změněná	změněný	k2eAgFnSc1d1	změněná
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
změny	změna	k1gFnPc1	změna
se	se	k3xPyFc4	se
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
vlajka	vlajka	k1gFnSc1	vlajka
dočkala	dočkat	k5eAaPmAgFnS	dočkat
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
rubovou	rubový	k2eAgFnSc4d1	rubová
stranu	strana	k1gFnSc4	strana
<g/>
)	)	kIx)	)
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
až	až	k9	až
do	do	k7c2	do
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
autonomních	autonomní	k2eAgInPc2d1	autonomní
regionů	region	k1gInPc2	region
==	==	k?	==
</s>
</p>
<p>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dva	dva	k4xCgInPc1	dva
částečně	částečně	k6eAd1	částečně
autonomní	autonomní	k2eAgInPc1d1	autonomní
regiony	region	k1gInPc1	region
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Gagauzsko	Gagauzsko	k6eAd1	Gagauzsko
–	–	k?	–
autonomní	autonomní	k2eAgFnSc4d1	autonomní
oblast	oblast	k1gFnSc4	oblast
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
</s>
</p>
<p>
<s>
Podněstří	Podněstřit	k5eAaPmIp3nS	Podněstřit
–	–	k?	–
de	de	k?	de
facto	facto	k1gNnSc4	facto
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuznaný	uznaný	k2eNgInSc1d1	neuznaný
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
odtrhl	odtrhnout	k5eAaPmAgInS	odtrhnout
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Moldavska	Moldavsko	k1gNnPc4	Moldavsko
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
</s>
</p>
<p>
<s>
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
