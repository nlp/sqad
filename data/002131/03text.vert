<s>
Hamburk	Hamburk	k1gInSc1	Hamburk
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Hamburg	Hamburg	k1gInSc1	Hamburg
<g/>
,	,	kIx,	,
dolnoněmecky	dolnoněmecky	k6eAd1	dolnoněmecky
Hamborg	Hamborg	k1gInSc1	Hamborg
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
největší	veliký	k2eAgInSc1d3	veliký
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
městských	městský	k2eAgFnPc2d1	městská
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
1,74	[number]	k4	1,74
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
755,22	[number]	k4	755,22
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc4d1	současná
hranice	hranice	k1gFnPc4	hranice
získal	získat	k5eAaPmAgInS	získat
Hamburk	Hamburk	k1gInSc1	Hamburk
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1937	[number]	k4	1937
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
"	"	kIx"	"
<g/>
o	o	k7c6	o
Velkém	velký	k2eAgInSc6d1	velký
Hamburku	Hamburk	k1gInSc6	Hamburk
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
územních	územní	k2eAgFnPc6d1	územní
úpravách	úprava	k1gFnPc6	úprava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
některé	některý	k3yIgFnPc1	některý
sousední	sousední	k2eAgFnPc1d1	sousední
obce	obec	k1gFnPc1	obec
a	a	k8xC	a
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Altona	Alton	k1gMnSc2	Alton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ztratil	ztratit	k5eAaPmAgMnS	ztratit
svoje	svůj	k3xOyFgMnPc4	svůj
exklávy	exkláv	k1gMnPc4	exkláv
Geesthacht	Geesthacht	k1gInSc1	Geesthacht
<g/>
,	,	kIx,	,
Großhansdorf	Großhansdorf	k1gInSc1	Großhansdorf
a	a	k8xC	a
Ritzebüttel	Ritzebüttel	k1gInSc1	Ritzebüttel
s	s	k7c7	s
městem	město	k1gNnSc7	město
Cuxhavenem	Cuxhaven	k1gMnSc7	Cuxhaven
<g/>
.	.	kIx.	.
</s>
<s>
Hamburk	Hamburk	k1gInSc1	Hamburk
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Německa	Německo	k1gNnSc2	Německo
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Labe	Labe	k1gNnSc2	Labe
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
2428	[number]	k4	2428
mostů	most	k1gInPc2	most
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
počtem	počet	k1gInSc7	počet
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
spolku	spolek	k1gInSc2	spolek
Hanza	hanza	k1gFnSc1	hanza
<g/>
,	,	kIx,	,
sdružujícího	sdružující	k2eAgInSc2d1	sdružující
významné	významný	k2eAgInPc4d1	významný
přístavy	přístav	k1gInPc4	přístav
a	a	k8xC	a
obchodní	obchodní	k2eAgNnPc4d1	obchodní
města	město	k1gNnPc4	město
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
;	;	kIx,	;
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
německých	německý	k2eAgNnPc2d1	německé
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
hanzovní	hanzovní	k2eAgNnSc4d1	hanzovní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
dopravě	doprava	k1gFnSc6	doprava
a	a	k8xC	a
obchodě	obchod	k1gInSc6	obchod
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
Tor	Tor	k1gMnSc1	Tor
zur	zur	k?	zur
Welt	Welt	k1gMnSc1	Welt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Brána	brána	k1gFnSc1	brána
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
německých	německý	k2eAgFnPc2d1	německá
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
významné	významný	k2eAgInPc1d1	významný
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
(	(	kIx(	(
<g/>
loděnice	loděnice	k1gFnSc1	loděnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Michaela	Michael	k1gMnSc2	Michael
<g/>
,	,	kIx,	,
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
čtvrť	čtvrť	k1gFnSc1	čtvrť
červených	červený	k2eAgFnPc2d1	červená
luceren	lucerna	k1gFnPc2	lucerna
Sankt	Sankt	k1gInSc4	Sankt
Pauli	Paule	k1gFnSc4	Paule
s	s	k7c7	s
proslavenou	proslavený	k2eAgFnSc7d1	proslavená
ulicí	ulice	k1gFnSc7	ulice
Reeperbahn	Reeperbahna	k1gFnPc2	Reeperbahna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
všemožné	všemožný	k2eAgInPc4d1	všemožný
druhy	druh	k1gInPc4	druh
zábavy	zábava	k1gFnSc2	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
známá	známý	k2eAgFnSc1d1	známá
pouť	pouť	k1gFnSc1	pouť
Hamburger	hamburger	k1gInSc1	hamburger
Dom	Dom	k?	Dom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Altstad	Altstad	k1gInSc4	Altstad
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
provozováno	provozován	k2eAgNnSc1d1	provozováno
jachtaření	jachtaření	k1gNnSc1	jachtaření
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
Televizní	televizní	k2eAgFnSc1d1	televizní
věž	věž	k1gFnSc1	věž
Heinricha	Heinrich	k1gMnSc2	Heinrich
Hertze	hertz	k1gInSc5	hertz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
oblíbená	oblíbený	k2eAgNnPc4d1	oblíbené
místa	místo	k1gNnPc4	místo
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
Dungeon	Dungeon	k1gInSc1	Dungeon
-	-	kIx~	-
skladiště	skladiště	k1gNnSc1	skladiště
přestavěné	přestavěný	k2eAgNnSc1d1	přestavěné
na	na	k7c4	na
Dům	dům	k1gInSc4	dům
hrůzy	hrůza	k1gFnSc2	hrůza
<g/>
,	,	kIx,	,
Kontorhaus	Kontorhaus	k1gInSc4	Kontorhaus
postaven	postaven	k2eAgInSc4d1	postaven
r.	r.	kA	r.
1922	[number]	k4	1922
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
lodě	loď	k1gFnSc2	loď
či	či	k8xC	či
jezero	jezero	k1gNnSc1	jezero
Alster	Alstra	k1gFnPc2	Alstra
-	-	kIx~	-
největší	veliký	k2eAgFnSc1d3	veliký
ze	z	k7c2	z
3	[number]	k4	3
jezer	jezero	k1gNnPc2	jezero
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
více	hodně	k6eAd2	hodně
než	než	k8xS	než
160	[number]	k4	160
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
ZOO	zoo	k1gFnSc1	zoo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
téměř	téměř	k6eAd1	téměř
3000	[number]	k4	3000
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
hamburského	hamburský	k2eAgInSc2d1	hamburský
přístavu	přístav	k1gInSc2	přístav
Moldauhafen	Moldauhafna	k1gFnPc2	Moldauhafna
má	mít	k5eAaImIp3nS	mít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
pronajatou	pronajatý	k2eAgFnSc4d1	pronajatá
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2028	[number]	k4	2028
<g/>
.	.	kIx.	.
</s>
<s>
Pronájem	pronájem	k1gInSc1	pronájem
části	část	k1gFnSc2	část
Hamburku	Hamburk	k1gInSc2	Hamburk
od	od	k7c2	od
Německa	Německo	k1gNnSc2	Německo
je	být	k5eAaImIp3nS	být
důsledek	důsledek	k1gInSc1	důsledek
postavení	postavení	k1gNnSc2	postavení
Československa	Československo	k1gNnSc2	Československo
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vítězů	vítěz	k1gMnPc2	vítěz
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
právního	právní	k2eAgNnSc2d1	právní
hlediska	hledisko	k1gNnSc2	hledisko
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
od	od	k7c2	od
28.10	[number]	k4	28.10
<g/>
.1918	.1918	k4	.1918
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
)	)	kIx)	)
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
kapitulace	kapitulace	k1gFnSc2	kapitulace
11.11	[number]	k4	11.11
<g/>
.1918	.1918	k4	.1918
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
bojům	boj	k1gInPc3	boj
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
s	s	k7c7	s
československými	československý	k2eAgFnPc7d1	Československá
legiemi	legie	k1gFnPc7	legie
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
u	u	k7c2	u
Terronu	Terron	k1gInSc2	Terron
a	a	k8xC	a
Vouziers	Vouziersa	k1gFnPc2	Vouziersa
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Versailles	Versailles	k1gFnSc2	Versailles
(	(	kIx(	(
<g/>
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uznalo	uznat	k5eAaPmAgNnS	uznat
Československo	Československo	k1gNnSc1	Československo
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
historických	historický	k2eAgFnPc6d1	historická
hranicích	hranice	k1gFnPc6	hranice
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
včetně	včetně	k7c2	včetně
německých	německý	k2eAgInPc2d1	německý
separatistických	separatistický	k2eAgInPc2d1	separatistický
pohraničních	pohraniční	k2eAgInPc2d1	pohraniční
útvarů	útvar	k1gInPc2	útvar
Německé	německý	k2eAgFnPc1d1	německá
Čechy	Čechy	k1gFnPc1	Čechy
<g/>
,	,	kIx,	,
Šumavská	šumavský	k2eAgFnSc1d1	Šumavská
župa	župa	k1gFnSc1	župa
<g/>
,	,	kIx,	,
Sudetsko	Sudetsko	k1gNnSc1	Sudetsko
a	a	k8xC	a
Německá	německý	k2eAgFnSc1d1	německá
jižní	jižní	k2eAgFnSc1d1	jižní
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
enkláv	enkláva	k1gFnPc2	enkláva
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
odstoupilo	odstoupit	k5eAaPmAgNnS	odstoupit
Československu	Československo	k1gNnSc3	Československo
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
Hlučínsko	Hlučínsko	k1gNnSc4	Hlučínsko
a	a	k8xC	a
pronajalo	pronajmout	k5eAaPmAgNnS	pronajmout
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgMnPc2	svůj
přístavů	přístav	k1gInPc2	přístav
Štětín	Štětín	k1gInSc1	Štětín
a	a	k8xC	a
Hamburk	Hamburk	k1gInSc1	Hamburk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
Československu	Československo	k1gNnSc6	Československo
na	na	k7c4	na
99	[number]	k4	99
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
téže	tenže	k3xDgFnSc2	tenže
smlouvy	smlouva	k1gFnSc2	smlouva
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
internacionalizovány	internacionalizován	k2eAgFnPc1d1	internacionalizována
vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
přístavům	přístav	k1gInPc3	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
nesmělo	smět	k5eNaImAgNnS	smět
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
plavidel	plavidlo	k1gNnPc2	plavidlo
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
svých	svůj	k3xOyFgNnPc2	svůj
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každých	každý	k3xTgInPc2	každý
10	[number]	k4	10
let	léto	k1gNnPc2	léto
komise	komise	k1gFnSc1	komise
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
budou	být	k5eAaImBp3nP	být
prostory	prostor	k1gInPc1	prostor
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
pronájem	pronájem	k1gInSc4	pronájem
Československu	Československo	k1gNnSc3	Československo
vytyčovat	vytyčovat	k5eAaImF	vytyčovat
a	a	k8xC	a
že	že	k8xS	že
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c4	v
den	den	k1gInSc4	den
podpisu	podpis	k1gInSc2	podpis
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
dopředu	dopředu	k6eAd1	dopředu
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
se	s	k7c7	s
všemi	všecek	k3xTgNnPc7	všecek
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
a	a	k8xC	a
závěry	závěr	k1gInPc7	závěr
této	tento	k3xDgFnSc2	tento
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
město	město	k1gNnSc1	město
Hamburk	Hamburk	k1gInSc1	Hamburk
pronajalo	pronajmout	k5eAaPmAgNnS	pronajmout
vhodné	vhodný	k2eAgFnPc4d1	vhodná
prostory	prostora	k1gFnPc4	prostora
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
připadla	připadnout	k5eAaPmAgFnS	připadnout
celá	celý	k2eAgFnSc1d1	celá
československá	československý	k2eAgFnSc1d1	Československá
část	část	k1gFnSc1	část
Hamburku	Hamburk	k1gInSc2	Hamburk
České	český	k2eAgFnSc3d1	Česká
republice	republika	k1gFnSc3	republika
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
část	část	k1gFnSc1	část
Hamburku	Hamburk	k1gInSc2	Hamburk
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
a	a	k8xC	a
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
molem	mol	k1gInSc7	mol
s	s	k7c7	s
přilehlým	přilehlý	k2eAgInSc7d1	přilehlý
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
administrativními	administrativní	k2eAgFnPc7d1	administrativní
a	a	k8xC	a
skladovými	skladový	k2eAgFnPc7d1	skladová
budovami	budova	k1gFnPc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
českou	český	k2eAgFnSc7d1	Česká
částí	část	k1gFnSc7	část
Hamburku	Hamburk	k1gInSc2	Hamburk
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
svrchovanou	svrchovaný	k2eAgFnSc4d1	svrchovaná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
kterémkoliv	kterýkoliv	k3yIgNnSc6	kterýkoliv
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Hamburk	Hamburk	k1gInSc1	Hamburk
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
města	město	k1gNnPc4	město
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
rozvinutým	rozvinutý	k2eAgNnSc7d1	rozvinuté
námořním	námořní	k2eAgNnSc7d1	námořní
odvětvím	odvětví	k1gNnSc7	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
2	[number]	k4	2
<g/>
.	.	kIx.	.
největší	veliký	k2eAgInSc1d3	veliký
kontejnerový	kontejnerový	k2eAgInSc1d1	kontejnerový
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
obstaráváno	obstarávat	k5eAaImNgNnS	obstarávat
zejména	zejména	k6eAd1	zejména
lodní	lodní	k2eAgFnSc7d1	lodní
a	a	k8xC	a
silniční	silniční	k2eAgFnSc7d1	silniční
dopravou	doprava	k1gFnSc7	doprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
nezůstává	zůstávat	k5eNaImIp3nS	zůstávat
ani	ani	k9	ani
přeprava	přeprava	k1gFnSc1	přeprava
letecká	letecký	k2eAgFnSc1d1	letecká
či	či	k8xC	či
železniční	železniční	k2eAgFnSc1d1	železniční
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
8	[number]	k4	8
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Letiště	letiště	k1gNnSc1	letiště
Hamburk	Hamburk	k1gInSc1	Hamburk
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
kód	kód	k1gInSc4	kód
<g/>
:	:	kIx,	:
HAM	ham	k0	ham
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
spojení	spojení	k1gNnSc2	spojení
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Hamburg-Fuhlsbüttel	Hamburg-Fuhlsbüttela	k1gFnPc2	Hamburg-Fuhlsbüttela
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsluhují	obsluhovat	k5eAaImIp3nP	obsluhovat
125	[number]	k4	125
cílových	cílový	k2eAgNnPc2d1	cílové
letišť	letiště	k1gNnPc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
letiště	letiště	k1gNnSc2	letiště
je	být	k5eAaImIp3nS	být
Hamburg	Hamburg	k1gInSc1	Hamburg
Airport	Airport	k1gInSc1	Airport
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc2	letiště
spojeno	spojit	k5eAaPmNgNnS	spojit
pomocí	pomocí	k7c2	pomocí
metra	metro	k1gNnSc2	metro
linkou	linka	k1gFnSc7	linka
S-Bahn	S-Bahn	k1gMnSc1	S-Bahn
S1	S1	k1gMnSc1	S1
se	se	k3xPyFc4	se
středem	střed	k1gInSc7	střed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
nezůstává	zůstávat	k5eNaImIp3nS	zůstávat
pozadu	pozadu	k6eAd1	pozadu
<g/>
,	,	kIx,	,
největším	veliký	k2eAgNnSc7d3	veliký
nádražím	nádraží	k1gNnSc7	nádraží
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
Hamburg	Hamburg	k1gInSc1	Hamburg
Hauptbahnhof	Hauptbahnhof	k1gInSc1	Hauptbahnhof
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
odbaví	odbavit	k5eAaPmIp3nP	odbavit
denně	denně	k6eAd1	denně
kolem	kolem	k7c2	kolem
450	[number]	k4	450
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
hlavové	hlavový	k2eAgNnSc1d1	hlavové
Hamburg	Hamburg	k1gInSc4	Hamburg
Altona	Alton	k1gMnSc2	Alton
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
odbaví	odbavit	k5eAaPmIp3nP	odbavit
denně	denně	k6eAd1	denně
přes	přes	k7c4	přes
150	[number]	k4	150
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
které	který	k3yIgNnSc4	který
je	být	k5eAaImIp3nS	být
výchozí	výchozí	k2eAgMnSc1d1	výchozí
a	a	k8xC	a
cílovou	cílový	k2eAgFnSc7d1	cílová
stanicí	stanice	k1gFnSc7	stanice
většiny	většina	k1gFnSc2	většina
linií	linie	k1gFnPc2	linie
vlaků	vlak	k1gInPc2	vlak
ICE	ICE	kA	ICE
<g/>
,	,	kIx,	,
EuroCity	EuroCita	k1gFnPc1	EuroCita
<g/>
,	,	kIx,	,
InterCity	InterCita	k1gFnPc1	InterCita
a	a	k8xC	a
EuroNight	EuroNight	k2eAgInSc1d1	EuroNight
spojující	spojující	k2eAgInSc1d1	spojující
Hamburk	Hamburk	k1gInSc1	Hamburk
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
německými	německý	k2eAgNnPc7d1	německé
a	a	k8xC	a
evropskými	evropský	k2eAgNnPc7d1	Evropské
městy	město	k1gNnPc7	město
včetně	včetně	k7c2	včetně
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
je	být	k5eAaImIp3nS	být
také	také	k9	také
systém	systém	k1gInSc1	systém
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
U-Bahn	U-Bahn	k1gInSc1	U-Bahn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
koncentrační	koncentrační	k2eAgMnSc1d1	koncentrační
tábor	tábor	k1gMnSc1	tábor
Neuengamme	Neuengamme	k1gMnSc1	Neuengamme
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
například	například	k6eAd1	například
pobýval	pobývat	k5eAaImAgMnS	pobývat
i	i	k9	i
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Emil	Emil	k1gMnSc1	Emil
František	František	k1gMnSc1	František
Burián	Burián	k1gMnSc1	Burián
<g/>
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
von	von	k1gInSc4	von
Guericke	Guerick	k1gInSc2	Guerick
(	(	kIx(	(
<g/>
1602	[number]	k4	1602
<g/>
-	-	kIx~	-
<g/>
1686	[number]	k4	1686
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
vývěvy	vývěva	k1gFnSc2	vývěva
a	a	k8xC	a
vakua	vakuum	k1gNnSc2	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Fleming	Fleming	k1gInSc1	Fleming
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
-	-	kIx~	-
<g/>
1640	[number]	k4	1640
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
barokní	barokní	k2eAgMnSc1d1	barokní
lyrický	lyrický	k2eAgMnSc1d1	lyrický
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
Georg	Georg	k1gMnSc1	Georg
Philipp	Philipp	k1gMnSc1	Philipp
Telemann	Telemann	k1gMnSc1	Telemann
(	(	kIx(	(
<g/>
1681	[number]	k4	1681
<g/>
-	-	kIx~	-
<g/>
1767	[number]	k4	1767
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
varhaník	varhaník	k1gMnSc1	varhaník
Carl	Carl	k1gMnSc1	Carl
Philipp	Philipp	k1gMnSc1	Philipp
Emanuel	Emanuel	k1gMnSc1	Emanuel
Bach	Bach	k1gMnSc1	Bach
(	(	kIx(	(
<g/>
1714	[number]	k4	1714
<g/>
-	-	kIx~	-
<g/>
1788	[number]	k4	1788
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
cembalista	cembalista	k1gMnSc1	cembalista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Johanna	Johanno	k1gNnSc2	Johanno
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
Johann	Johann	k1gMnSc1	Johann
Bernhard	Bernhard	k1gMnSc1	Bernhard
Basedow	Basedow	k1gMnSc1	Basedow
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
-	-	kIx~	-
<g/>
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
vychovatel	vychovatel	k1gMnSc1	vychovatel
<g/>
,	,	kIx,	,
reformní	reformní	k2eAgMnSc1d1	reformní
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
filantrop	filantrop	k1gMnSc1	filantrop
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Klopstock	Klopstock	k1gMnSc1	Klopstock
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
-	-	kIx~	-
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Gotthold	Gotthold	k1gMnSc1	Gotthold
Ephraim	Ephraim	k1gMnSc1	Ephraim
Lessing	Lessing	k1gInSc1	Lessing
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
<g/>
-	-	kIx~	-
<g/>
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Heinrich	Heinrich	k1gMnSc1	Heinrich
Wilhelm	Wilhelma	k1gFnPc2	Wilhelma
von	von	k1gInSc1	von
Gerstenberg	Gerstenberg	k1gMnSc1	Gerstenberg
(	(	kIx(	(
<g/>
1737	[number]	k4	1737
<g/>
-	-	kIx~	-
<g/>
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
Johann	Johann	k1gMnSc1	Johann
Elert	Elert	k1gInSc4	Elert
Bode	bůst	k5eAaImIp3nS	bůst
(	(	kIx(	(
<g/>
1747	[number]	k4	1747
<g/>
-	-	kIx~	-
<g/>
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Johann	Johanna	k1gFnPc2	Johanna
Franz	Franz	k1gMnSc1	Franz
Encke	Enck	k1gFnSc2	Enck
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
-	-	kIx~	-
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Johann	Johann	k1gMnSc1	Johann
Georg	Georg	k1gMnSc1	Georg
Christian	Christian	k1gMnSc1	Christian
Lehmann	Lehmann	k1gMnSc1	Lehmann
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgInSc1d1	německý
<g />
.	.	kIx.	.
</s>
<s>
botanik	botanik	k1gMnSc1	botanik
Matthias	Matthias	k1gMnSc1	Matthias
Jacob	Jacoba	k1gFnPc2	Jacoba
Schleiden	Schleidna	k1gFnPc2	Schleidna
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
-	-	kIx~	-
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
botanik	botanik	k1gMnSc1	botanik
Felix	Felix	k1gMnSc1	Felix
Mendelssohn-Bartholdy	Mendelssohn-Bartholda	k1gFnSc2	Mendelssohn-Bartholda
(	(	kIx(	(
<g/>
1809	[number]	k4	1809
<g/>
-	-	kIx~	-
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
romantický	romantický	k2eAgMnSc1d1	romantický
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Cornelius	Cornelius	k1gMnSc1	Cornelius
Gurlitt	Gurlitt	k1gMnSc1	Gurlitt
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g />
.	.	kIx.	.
</s>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Brahms	Brahms	k1gMnSc1	Brahms
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
Heinrich	Heinrich	k1gMnSc1	Heinrich
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hertz	hertz	k1gInSc1	hertz
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
-	-	kIx~	-
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
mechanik	mechanik	k1gMnSc1	mechanik
Oskar	Oskar	k1gMnSc1	Oskar
Troplowitz	Troplowitz	k1gMnSc1	Troplowitz
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
lékárník	lékárník	k1gMnSc1	lékárník
<g/>
,	,	kIx,	,
spoluvynálezce	spoluvynálezce	k1gMnSc1	spoluvynálezce
krému	krém	k1gInSc2	krém
Nivea	Nivea	k1gMnSc1	Nivea
Friedrich	Friedrich	k1gMnSc1	Friedrich
Rittelmeyer	Rittelmeyer	k1gMnSc1	Rittelmeyer
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
evangelický	evangelický	k2eAgMnSc1d1	evangelický
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
Obce	obec	k1gFnSc2	obec
křesťanů	křesťan	k1gMnPc2	křesťan
Otto	Otto	k1gMnSc1	Otto
Diels	Diels	k1gInSc1	Diels
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
James	James	k1gMnSc1	James
Franck	Franck	k1gMnSc1	Franck
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Karen	Karna	k1gFnPc2	Karna
Horneyová	Horneyová	k1gFnSc1	Horneyová
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
psycholožka	psycholožka	k1gFnSc1	psycholožka
a	a	k8xC	a
psychiatrička	psychiatrička	k1gFnSc1	psychiatrička
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
Gustav	Gustav	k1gMnSc1	Gustav
Ludwig	Ludwig	k1gMnSc1	Ludwig
Hertz	hertz	k1gInSc1	hertz
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Carl	Carl	k1gMnSc1	Carl
von	von	k1gInSc1	von
Ossietzky	Ossietzek	k1gInPc1	Ossietzek
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
žurnalista	žurnalista	k1gMnSc1	žurnalista
a	a	k8xC	a
pacifista	pacifista	k1gMnSc1	pacifista
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
Hans	Hans	k1gMnSc1	Hans
Reichenbach	Reichenbach	k1gMnSc1	Reichenbach
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německo-americký	německomerický	k2eAgInSc1d1	německo-americký
<g />
.	.	kIx.	.
</s>
<s>
filosof	filosof	k1gMnSc1	filosof
vědy	věda	k1gFnSc2	věda
Karl	Karl	k1gMnSc1	Karl
Dönitz	Dönitz	k1gMnSc1	Dönitz
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
námořní	námořní	k2eAgMnSc1d1	námořní
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
budovatel	budovatel	k1gMnSc1	budovatel
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
německého	německý	k2eAgNnSc2d1	německé
ponorkového	ponorkový	k2eAgNnSc2d1	ponorkové
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
německého	německý	k2eAgNnSc2d1	německé
válečného	válečný	k2eAgNnSc2d1	válečné
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
i	i	k9	i
říšský	říšský	k2eAgMnSc1d1	říšský
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
po	po	k7c6	po
Hitlerově	Hitlerův	k2eAgFnSc6d1	Hitlerova
sebevraždě	sebevražda	k1gFnSc6	sebevražda
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Lamač	lamač	k1gMnSc1	lamač
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1897	[number]	k4	1897
<g/>
-	-	kIx~	-
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
filmař	filmař	k1gMnSc1	filmař
Arnold	Arnold	k1gMnSc1	Arnold
Gehlen	Gehlen	k2eAgMnSc1d1	Gehlen
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Bill	Bill	k1gMnSc1	Bill
Brandt	Brandt	k1gMnSc1	Brandt
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
novinářský	novinářský	k2eAgMnSc1d1	novinářský
fotograf	fotograf	k1gMnSc1	fotograf
Manfred	Manfred	k1gMnSc1	Manfred
von	von	k1gInSc4	von
Ardenne	Ardenn	k1gInSc5	Ardenn
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
Johannes	Johannes	k1gMnSc1	Johannes
Hans	Hans	k1gMnSc1	Hans
Daniel	Daniel	k1gMnSc1	Daniel
Jensen	Jensen	k2eAgMnSc1d1	Jensen
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
jaderný	jaderný	k2eAgMnSc1d1	jaderný
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Hildesheimer	Hildesheimer	k1gMnSc1	Hildesheimer
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
<g />
.	.	kIx.	.
</s>
<s>
Helmut	Helmut	k1gMnSc1	Helmut
Schmidt	Schmidt	k1gMnSc1	Schmidt
(	(	kIx(	(
<g/>
*	*	kIx~	*
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
SPD	SPD	kA	SPD
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
západoněmecký	západoněmecký	k2eAgMnSc1d1	západoněmecký
kancléř	kancléř	k1gMnSc1	kancléř
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
Gabriel	Gabriel	k1gMnSc1	Gabriel
Laub	Laub	k1gMnSc1	Laub
(	(	kIx(	(
<g/>
*	*	kIx~	*
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česko-polský	českoolský	k2eAgMnSc1d1	česko-polský
esejista	esejista	k1gMnSc1	esejista
a	a	k8xC	a
aforista	aforista	k1gMnSc1	aforista
Luboš	Luboš	k1gMnSc1	Luboš
Kohoutek	Kohoutek	k1gMnSc1	Kohoutek
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
astronom	astronom	k1gMnSc1	astronom
Juraj	Juraj	k1gMnSc1	Juraj
Kukura	Kukura	k1gFnSc1	Kukura
(	(	kIx(	(
<g/>
*	*	kIx~	*
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
herec	herec	k1gMnSc1	herec
Václav	Václav	k1gMnSc1	Václav
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Angela	Angela	k1gFnSc1	Angela
Merkelová	Merkelová	k1gFnSc1	Merkelová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1	Křesťanskodemokratická
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
současná	současný	k2eAgFnSc1d1	současná
kancléřka	kancléřka	k1gFnSc1	kancléřka
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
Andreas	Andreas	k1gInSc1	Andreas
Brehme	Brehm	k1gMnSc5	Brehm
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
obránce	obránce	k1gMnSc1	obránce
Eva	Eva	k1gFnSc1	Eva
Habermann	Habermann	k1gMnSc1	Habermann
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
herečka	herečka	k1gFnSc1	herečka
Tommy	Tomma	k1gFnSc2	Tomma
Haas	Haasa	k1gFnPc2	Haasa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
tenista	tenista	k1gMnSc1	tenista
Karim	Karima	k1gFnPc2	Karima
Guédé	Guédý	k2eAgFnPc1d1	Guédý
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tožský	tožský	k2eAgMnSc1d1	tožský
fotbalista	fotbalista	k1gMnSc1	fotbalista
Auckland	Aucklanda	k1gFnPc2	Aucklanda
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Drážďany	Drážďany	k1gInPc1	Drážďany
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
León	Leóna	k1gFnPc2	Leóna
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
Ósaka	Ósaka	k1gFnSc1	Ósaka
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
Sã	Sã	k1gFnPc2	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
