<s>
Jabloň	jabloň	k1gFnSc1	jabloň
(	(	kIx(	(
<g/>
Malus	Malus	k1gInSc1	Malus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
opadavých	opadavý	k2eAgInPc2d1	opadavý
listnatých	listnatý	k2eAgInPc2d1	listnatý
stromů	strom	k1gInPc2	strom
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
růžovitých	růžovitý	k2eAgMnPc2d1	růžovitý
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jádroviny	jádrovina	k1gFnPc4	jádrovina
<g/>
.	.	kIx.	.
</s>
<s>
Jabloně	jabloň	k1gFnPc1	jabloň
široce	široko	k6eAd1	široko
pěstované	pěstovaný	k2eAgInPc4d1	pěstovaný
pro	pro	k7c4	pro
plody	plod	k1gInPc4	plod
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
taxonomický	taxonomický	k2eAgInSc1d1	taxonomický
druh	druh	k1gInSc1	druh
jabloň	jabloň	k1gFnSc4	jabloň
domácí	domácí	k2eAgFnSc1d1	domácí
<g/>
,	,	kIx,	,
na	na	k7c4	na
křížení	křížení	k1gNnSc4	křížení
současných	současný	k2eAgFnPc2d1	současná
odrůd	odrůda	k1gFnPc2	odrůda
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
podílelo	podílet	k5eAaImAgNnS	podílet
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
jabloň	jabloň	k1gFnSc4	jabloň
<g/>
.	.	kIx.	.
</s>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
je	být	k5eAaImIp3nS	být
cizosprašný	cizosprašný	k2eAgInSc4d1	cizosprašný
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
diploidní	diploidní	k2eAgFnSc1d1	diploidní
<g/>
,	,	kIx,	,
triploidní	triploidní	k2eAgFnSc1d1	triploidní
<g/>
.	.	kIx.	.
</s>
<s>
Jabloně	jabloň	k1gFnPc1	jabloň
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
dlouhověké	dlouhověký	k2eAgInPc4d1	dlouhověký
stromy	strom	k1gInPc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
60-80	[number]	k4	60-80
let	léto	k1gNnPc2	léto
i	i	k9	i
nad	nad	k7c4	nad
100	[number]	k4	100
let	léto	k1gNnPc2	léto
a	a	k8xC	a
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
výšky	výška	k1gFnPc1	výška
až	až	k9	až
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
<g/>
,	,	kIx,	,
odrůdě	odrůda	k1gFnSc6	odrůda
a	a	k8xC	a
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ovocnářské	ovocnářský	k2eAgFnSc6d1	Ovocnářská
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
uplatnily	uplatnit	k5eAaPmAgFnP	uplatnit
pupenové	pupenový	k2eAgFnPc1d1	pupenová
mutace	mutace	k1gFnPc1	mutace
(	(	kIx(	(
<g/>
trvalá	trvalý	k2eAgFnSc1d1	trvalá
změna	změna	k1gFnSc1	změna
genotypu	genotyp	k1gInSc2	genotyp
<g/>
)	)	kIx)	)
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
části	část	k1gFnSc6	část
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
spur-typy	spuryp	k1gInPc4	spur-typ
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
některé	některý	k3yIgFnPc1	některý
odrůdy	odrůda	k1gFnPc1	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jabloně	jabloň	k1gFnSc2	jabloň
jsou	být	k5eAaImIp3nP	být
narůžovělé	narůžovělý	k2eAgFnPc1d1	narůžovělá
<g/>
,	,	kIx,	,
pětičetné	pětičetný	k2eAgFnPc1d1	pětičetná
<g/>
,	,	kIx,	,
s	s	k7c7	s
kalichem	kalich	k1gInSc7	kalich
a	a	k8xC	a
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
oboupohlavné	oboupohlavný	k2eAgInPc1d1	oboupohlavný
<g/>
.	.	kIx.	.
</s>
<s>
Květní	květní	k2eAgInPc1d1	květní
lístky	lístek	k1gInPc1	lístek
srůstají	srůstat	k5eAaImIp3nP	srůstat
v	v	k7c4	v
pohárek	pohárek	k1gInSc4	pohárek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
dužina	dužina	k1gFnSc1	dužina
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
jablko	jablko	k1gNnSc1	jablko
–	–	k?	–
malvice	malvice	k1gFnSc1	malvice
s	s	k7c7	s
asi	asi	k9	asi
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
semeny	semeno	k1gNnPc7	semeno
v	v	k7c6	v
jádřinci	jádřinec	k1gInSc6	jádřinec
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
plod	plod	k1gInSc4	plod
dužnatý	dužnatý	k2eAgInSc4d1	dužnatý
<g/>
,	,	kIx,	,
dozrávající	dozrávající	k2eAgInSc4d1	dozrávající
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
podmínkách	podmínka	k1gFnPc6	podmínka
v	v	k7c6	v
období	období	k1gNnSc6	období
srpna	srpen	k1gInSc2	srpen
<g/>
–	–	k?	–
<g/>
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Malus	Malus	k1gInSc1	Malus
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jabloň	jabloň	k1gFnSc1	jabloň
lesní	lesní	k2eAgFnSc1d1	lesní
<g/>
.	.	kIx.	.
</s>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Malus	Malus	k1gMnSc1	Malus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
kriticky	kriticky	k6eAd1	kriticky
ohrožena	ohrozit	k5eAaPmNgFnS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Nebývá	bývat	k5eNaImIp3nS	bývat
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Hustě	hustě	k6eAd1	hustě
rozvětvená	rozvětvený	k2eAgFnSc1d1	rozvětvená
koruna	koruna	k1gFnSc1	koruna
začíná	začínat	k5eAaImIp3nS	začínat
již	již	k6eAd1	již
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
větvičky	větvička	k1gFnPc1	větvička
bývají	bývat	k5eAaImIp3nP	bývat
kůlovitě	kůlovitě	k6eAd1	kůlovitě
zakončené	zakončený	k2eAgInPc4d1	zakončený
<g/>
.	.	kIx.	.
</s>
<s>
Borka	borka	k1gFnSc1	borka
šedohnědá	šedohnědý	k2eAgFnSc1d1	šedohnědá
šupinatá	šupinatý	k2eAgFnSc1d1	šupinatá
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jabloň	jabloň	k1gFnSc1	jabloň
domácí	domácí	k1gFnSc1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
domácí	domácí	k2eAgFnSc1d1	domácí
(	(	kIx(	(
<g/>
Malus	Malus	k1gMnSc1	Malus
×	×	k?	×
domestica	domestica	k1gMnSc1	domestica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
druhový	druhový	k2eAgMnSc1d1	druhový
kříženec	kříženec	k1gMnSc1	kříženec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dal	dát	k5eAaPmAgMnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
naším	náš	k3xOp1gMnSc7	náš
kulturním	kulturní	k2eAgFnPc3d1	kulturní
odrůdám	odrůda	k1gFnPc3	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kříženec	kříženec	k1gMnSc1	kříženec
Malus	Malus	k1gMnSc1	Malus
pumila	pumila	k1gFnSc1	pumila
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtitelskou	šlechtitelský	k2eAgFnSc7d1	šlechtitelská
činností	činnost	k1gFnSc7	činnost
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
vypěstována	vypěstován	k2eAgFnSc1d1	vypěstována
pestrá	pestrý	k2eAgFnSc1d1	pestrá
škála	škála	k1gFnSc1	škála
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
,	,	kIx,	,
známé	známá	k1gFnPc1	známá
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Jonathan	Jonathan	k1gMnSc1	Jonathan
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
,	,	kIx,	,
Golden	Goldna	k1gFnPc2	Goldna
Delicious	Delicious	k1gInSc1	Delicious
<g/>
,	,	kIx,	,
Spartan	Spartan	k?	Spartan
<g/>
.	.	kIx.	.
</s>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nejpěstovanější	pěstovaný	k2eAgFnSc7d3	nejpěstovanější
ovocnou	ovocný	k2eAgFnSc7d1	ovocná
dřevinou	dřevina	k1gFnSc7	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jabloň	jabloň	k1gFnSc1	jabloň
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
nízká	nízký	k2eAgFnSc1d1	nízká
(	(	kIx(	(
<g/>
Malus	Malus	k1gMnSc1	Malus
pumila	pumil	k1gMnSc2	pumil
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
Zakavkazsku	Zakavkazsko	k1gNnSc6	Zakavkazsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc4d1	významný
druh	druh	k1gInSc4	druh
pro	pro	k7c4	pro
šlechtění	šlechtění	k1gNnSc4	šlechtění
podnoží	podnoží	k1gNnSc2	podnoží
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
2	[number]	k4	2
typy	typ	k1gInPc4	typ
podnoží	podnožit	k5eAaPmIp3nS	podnožit
<g/>
:	:	kIx,	:
janče-	janče-	k?	janče-
<g/>
>	>	kIx)	>
Malus	Malus	k1gMnSc1	Malus
pumila	pumil	k1gMnSc2	pumil
var.	var.	k?	var.
paradisica	paradisicus	k1gMnSc2	paradisicus
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jančata	janče	k1gNnPc1	janče
mají	mít	k5eAaImIp3nP	mít
křehký	křehký	k2eAgInSc4d1	křehký
kořen	kořen	k1gInSc4	kořen
<g/>
.	.	kIx.	.
duzén-	duzén-	k?	duzén-
<g/>
>	>	kIx)	>
Malus	Malus	k1gMnSc1	Malus
pumila	pumil	k1gMnSc2	pumil
var.	var.	k?	var.
milis	milis	k1gFnSc2	milis
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jabloň	jabloň	k1gFnSc1	jabloň
Sieversova	Sieversův	k2eAgFnSc1d1	Sieversův
<g/>
.	.	kIx.	.
</s>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
sieversova	sieversův	k2eAgFnSc1d1	sieversův
(	(	kIx(	(
<g/>
Malus	Malus	k1gMnSc1	Malus
sieversii	sieversie	k1gFnSc4	sieversie
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
šlechtění	šlechtění	k1gNnSc3	šlechtění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
opadavý	opadavý	k2eAgInSc1d1	opadavý
strom	strom	k1gInSc1	strom
dorůstající	dorůstající	k2eAgInSc1d1	dorůstající
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
5-12	[number]	k4	5-12
m	m	kA	m
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgMnSc1d1	podobný
kulturním	kulturní	k2eAgFnPc3d1	kulturní
jabloním	jabloň	k1gFnPc3	jabloň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc6d3	veliký
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
Malus	Malus	k1gInSc4	Malus
<g/>
,	,	kIx,	,
až	až	k9	až
7	[number]	k4	7
cm	cm	kA	cm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
velikostí	velikost	k1gFnSc7	velikost
vyrovná	vyrovnat	k5eAaBmIp3nS	vyrovnat
mnoha	mnoho	k4c3	mnoho
soudobým	soudobý	k2eAgFnPc3d1	soudobá
odrůdám	odrůda	k1gFnPc3	odrůda
jablek	jablko	k1gNnPc2	jablko
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
nedávno	nedávno	k6eAd1	nedávno
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
americká	americký	k2eAgFnSc1d1	americká
služba	služba	k1gFnSc1	služba
pro	pro	k7c4	pro
zemědělský	zemědělský	k2eAgInSc4d1	zemědělský
výzkum	výzkum	k1gInSc4	výzkum
(	(	kIx(	(
<g/>
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Agricultural	Agricultural	k1gMnSc1	Agricultural
Research	Research	k1gMnSc1	Research
Service	Service	k1gFnSc1	Service
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
naleznou	naleznout	k5eAaPmIp3nP	naleznout
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
šlechtění	šlechtění	k1gNnSc4	šlechtění
moderních	moderní	k2eAgFnPc2d1	moderní
jabloní	jabloň	k1gFnPc2	jabloň
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
všechny	všechen	k3xTgInPc4	všechen
<g/>
,	,	kIx,	,
z	z	k7c2	z
výsledných	výsledný	k2eAgInPc2d1	výsledný
stromů	strom	k1gInPc2	strom
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
chorobám	choroba	k1gFnPc3	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Rozptyl	rozptyl	k1gInSc1	rozptyl
jejich	jejich	k3xOp3gFnSc2	jejich
individuální	individuální	k2eAgFnSc2d1	individuální
odezvy	odezva	k1gFnSc2	odezva
na	na	k7c4	na
onemocnění	onemocnění	k1gNnPc4	onemocnění
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
znamením	znamení	k1gNnSc7	znamení
toho	ten	k3xDgNnSc2	ten
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
geneticky	geneticky	k6eAd1	geneticky
rozmanitější	rozmanitý	k2eAgFnPc1d2	rozmanitější
než	než	k8xS	než
dnešní	dnešní	k2eAgFnPc1d1	dnešní
kulturní	kulturní	k2eAgFnPc1d1	kulturní
jabloně	jabloň	k1gFnPc1	jabloň
<g/>
.	.	kIx.	.
</s>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
drobnoplodá	drobnoplodý	k2eAgFnSc1d1	drobnoplodá
(	(	kIx(	(
<g/>
Malus	Malus	k1gMnSc1	Malus
baccata	baccata	k1gFnSc1	baccata
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
šlechtění	šlechtění	k1gNnSc3	šlechtění
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
jabloně	jabloň	k1gFnSc2	jabloň
–	–	k?	–
jablko	jablko	k1gNnSc1	jablko
–	–	k?	–
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
člověka	člověk	k1gMnSc4	člověk
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
utržením	utržení	k1gNnSc7	utržení
jablka	jablko	k1gNnSc2	jablko
ze	z	k7c2	z
zakázaného	zakázaný	k2eAgInSc2d1	zakázaný
stromu	strom	k1gInSc2	strom
v	v	k7c6	v
Rajské	rajský	k2eAgFnSc6d1	rajská
zahradě	zahrada	k1gFnSc6	zahrada
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
chybně	chybně	k6eAd1	chybně
spojován	spojován	k2eAgInSc4d1	spojován
prvotní	prvotní	k2eAgInSc4d1	prvotní
hřích	hřích	k1gInSc4	hřích
lidského	lidský	k2eAgNnSc2d1	lidské
pokolení	pokolení	k1gNnSc2	pokolení
<g/>
;	;	kIx,	;
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
nepíše	psát	k5eNaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c4	o
plod	plod	k1gInSc4	plod
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
nespecifikovaného	specifikovaný	k2eNgInSc2d1	nespecifikovaný
<g/>
)	)	kIx)	)
stromu	strom	k1gInSc2	strom
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Napodobenina	napodobenina	k1gFnSc1	napodobenina
jablka	jablko	k1gNnSc2	jablko
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
symbolů	symbol	k1gInPc2	symbol
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
jablko	jablko	k1gNnSc1	jablko
je	být	k5eAaImIp3nS	být
také	také	k9	také
předmětem	předmět	k1gInSc7	předmět
známého	známý	k2eAgInSc2d1	známý
sváru	svár	k1gInSc2	svár
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
odrůd	odrůda	k1gFnPc2	odrůda
jablek	jablko	k1gNnPc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Ovocné	ovocný	k2eAgFnPc4d1	ovocná
odrůdy	odrůda	k1gFnPc4	odrůda
jabloní	jabloň	k1gFnSc7	jabloň
lze	lze	k6eAd1	lze
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
více	hodně	k6eAd2	hodně
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
podle	podle	k7c2	podle
použití	použití	k1gNnSc2	použití
<g/>
,	,	kIx,	,
způsobu	způsoba	k1gFnSc4	způsoba
růstu	růst	k1gInSc2	růst
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
pomologických	pomologický	k2eAgInPc2d1	pomologický
znaků	znak	k1gInPc2	znak
či	či	k8xC	či
podle	podle	k7c2	podle
doby	doba	k1gFnSc2	doba
zrání	zrání	k1gNnSc2	zrání
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pěstování	pěstování	k1gNnSc6	pěstování
jabloní	jabloň	k1gFnPc2	jabloň
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaPmNgInP	využívat
různé	různý	k2eAgInPc1d1	různý
tvary	tvar	k1gInPc1	tvar
ovocných	ovocný	k2eAgFnPc2d1	ovocná
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
vedením	vedení	k1gNnSc7	vedení
výhonů	výhon	k1gInPc2	výhon
<g/>
,	,	kIx,	,
výškou	výška	k1gFnSc7	výška
štěpování	štěpování	k1gNnSc2	štěpování
nebo	nebo	k8xC	nebo
založení	založení	k1gNnSc2	založení
korunky	korunka	k1gFnSc2	korunka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
odrůdy	odrůda	k1gFnPc1	odrůda
jabloní	jabloň	k1gFnPc2	jabloň
jsou	být	k5eAaImIp3nP	být
různě	různě	k6eAd1	různě
tvarovány	tvarován	k2eAgInPc1d1	tvarován
a	a	k8xC	a
udržovány	udržován	k2eAgInPc1d1	udržován
i	i	k9	i
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
tvoření	tvoření	k1gNnSc2	tvoření
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Spur-typy	Spuryp	k1gInPc1	Spur-typ
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
méně	málo	k6eAd2	málo
bujným	bujný	k2eAgInSc7d1	bujný
vzrůstem	vzrůst	k1gInSc7	vzrůst
a	a	k8xC	a
krátkými	krátký	k2eAgNnPc7d1	krátké
internodii	internodium	k1gNnPc7	internodium
<g/>
.	.	kIx.	.
</s>
<s>
Spur-typy	Spuryp	k1gInPc1	Spur-typ
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívají	využívat	k5eAaImIp3nP	využívat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
nižších	nízký	k2eAgFnPc2d2	nižší
tvarech	tvar	k1gInPc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
vyšlechtěny	vyšlechtěn	k2eAgFnPc1d1	vyšlechtěna
sloupovité	sloupovitý	k2eAgFnPc1d1	sloupovitá
jabloně	jabloň	k1gFnPc1	jabloň
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
svislý	svislý	k2eAgInSc4d1	svislý
kordon	kordon	k1gInSc4	kordon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sloupovité	sloupovitý	k2eAgFnPc1d1	sloupovitá
jabloně	jabloň	k1gFnPc1	jabloň
nazýváme	nazývat	k5eAaImIp1nP	nazývat
též	též	k9	též
-	-	kIx~	-
baleríny	balerína	k1gFnSc2	balerína
<g/>
,	,	kIx,	,
kolumnární	kolumnární	k2eAgFnSc2d1	kolumnární
<g/>
,	,	kIx,	,
sloupovité	sloupovitý	k2eAgFnSc2d1	sloupovitá
nebo	nebo	k8xC	nebo
sloupcovité	sloupcovitý	k2eAgFnSc2d1	sloupcovitá
jabloně	jabloň	k1gFnSc2	jabloň
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tvary	tvar	k1gInPc1	tvar
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
pro	pro	k7c4	pro
silně	silně	k6eAd1	silně
zahuštěné	zahuštěný	k2eAgFnPc4d1	zahuštěná
krátkověké	krátkověký	k2eAgFnPc4d1	krátkověká
výsadby	výsadba	k1gFnPc4	výsadba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
vysoký	vysoký	k2eAgInSc4d1	vysoký
výnos	výnos	k1gInSc4	výnos
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
intenzivně	intenzivně	k6eAd1	intenzivně
ošetřované	ošetřovaný	k2eAgFnSc2d1	ošetřovaná
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
palmet	palmeta	k1gFnPc2	palmeta
(	(	kIx(	(
<g/>
kordón	kordón	k1gInSc1	kordón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Řez	řez	k1gInSc1	řez
jabloní	jabloň	k1gFnPc2	jabloň
<g/>
.	.	kIx.	.
</s>
<s>
Prořezávání	prořezávání	k1gNnSc1	prořezávání
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
odstranění	odstranění	k1gNnSc6	odstranění
nadbytečných	nadbytečný	k2eAgFnPc2d1	nadbytečná
nebo	nebo	k8xC	nebo
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
větví	větev	k1gFnPc2	větev
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
význam	význam	k1gInSc4	význam
prořezávání	prořezávání	k1gNnSc2	prořezávání
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
mezi	mezi	k7c7	mezi
ovocnáři	ovocnář	k1gMnPc7	ovocnář
na	na	k7c4	na
řez	řez	k1gInSc4	řez
jabloní	jabloň	k1gFnPc2	jabloň
jako	jako	k8xC	jako
takový	takový	k3xDgInSc4	takový
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
připustit	připustit	k5eAaPmF	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obecně	obecně	k6eAd1	obecně
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Prořezávání	prořezávání	k1gNnSc1	prořezávání
snižuje	snižovat	k5eAaImIp3nS	snižovat
asimilační	asimilační	k2eAgFnSc4d1	asimilační
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
výsadbě	výsadba	k1gFnSc6	výsadba
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
snížit	snížit	k5eAaPmF	snížit
asimilační	asimilační	k2eAgFnSc4d1	asimilační
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kořeny	kořen	k1gInPc1	kořen
byly	být	k5eAaImAgInP	být
omezeny	omezit	k5eAaPmNgInP	omezit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
schopnost	schopnost	k1gFnSc1	schopnost
stromu	strom	k1gInSc2	strom
přijímat	přijímat	k5eAaImF	přijímat
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
plná	plný	k2eAgFnSc1d1	plná
asimilační	asimilační	k2eAgFnSc1d1	asimilační
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpařování	odpařování	k1gNnSc1	odpařování
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
oslabí	oslabit	k5eAaPmIp3nS	oslabit
podstatně	podstatně	k6eAd1	podstatně
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
způsobí	způsobit	k5eAaPmIp3nS	způsobit
jeho	jeho	k3xOp3gInSc1	jeho
úhyn	úhyn	k1gInSc1	úhyn
během	během	k7c2	během
suchého	suchý	k2eAgNnSc2d1	suché
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prořezávání	prořezávání	k1gNnSc4	prořezávání
snižující	snižující	k2eAgFnSc4d1	snižující
asimilační	asimilační	k2eAgFnSc4d1	asimilační
plochu	plocha	k1gFnSc4	plocha
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
nutné	nutný	k2eAgNnSc1d1	nutné
případě	případ	k1gInSc6	případ
starých	starý	k2eAgInPc2d1	starý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
koruny	koruna	k1gFnPc1	koruna
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
velmi	velmi	k6eAd1	velmi
velkými	velká	k1gFnPc7	velká
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
slabých	slabý	k2eAgInPc2d1	slabý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
stromů	strom	k1gInPc2	strom
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kořeny	kořen	k1gInPc1	kořen
jabloní	jabloň	k1gFnPc2	jabloň
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
proniknout	proniknout	k5eAaPmF	proniknout
hluboko	hluboko	k6eAd1	hluboko
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
dřevo	dřevo	k1gNnSc1	dřevo
bylo	být	k5eAaImAgNnS	být
silně	silně	k6eAd1	silně
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Sklizeň	sklizeň	k1gFnSc1	sklizeň
probíhá	probíhat	k5eAaImIp3nS	probíhat
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
odrůdy	odrůda	k1gFnPc1	odrůda
mají	mít	k5eAaImIp3nP	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
skladovatelnosti	skladovatelnost	k1gFnSc2	skladovatelnost
<g/>
.	.	kIx.	.
</s>
<s>
Průsvitné	průsvitný	k2eAgInPc1d1	průsvitný
letní	letní	k2eAgFnSc4d1	letní
Sklizeň	sklizeň	k1gFnSc4	sklizeň
probíhá	probíhat	k5eAaImIp3nS	probíhat
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Skladovatelnost	skladovatelnost	k1gFnSc1	skladovatelnost
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
2-8	[number]	k4	2-8
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Coxova	Coxův	k2eAgFnSc1d1	Coxova
reneta	reneta	k1gFnSc1	reneta
Sklizeň	sklizeň	k1gFnSc1	sklizeň
probíhá	probíhat	k5eAaImIp3nS	probíhat
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
až	až	k6eAd1	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Skladovatelnost	skladovatelnost	k1gFnSc1	skladovatelnost
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
8-12	[number]	k4	8-12
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Malinové	Malinové	k2eAgInPc1d1	Malinové
hornokrajské	hornokrajský	k2eAgInPc1d1	hornokrajský
Selena	selen	k2eAgFnSc1d1	Selena
Sklizeň	sklizeň	k1gFnSc1	sklizeň
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Skladovatelnost	skladovatelnost	k1gFnSc1	skladovatelnost
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
12-24	[number]	k4	12-24
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Golden	Goldna	k1gFnPc2	Goldna
Delicious	Delicious	k1gMnSc1	Delicious
Jeptiška	jeptiška	k1gFnSc1	jeptiška
(	(	kIx(	(
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Železné	železný	k2eAgNnSc1d1	železné
jablko	jablko	k1gNnSc1	jablko
<g/>
)	)	kIx)	)
Vegetační	vegetační	k2eAgInSc1d1	vegetační
klid	klid	k1gInSc1	klid
jabloní	jabloň	k1gFnPc2	jabloň
nastává	nastávat	k5eAaImIp3nS	nastávat
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
pod	pod	k7c4	pod
6	[number]	k4	6
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
nad	nad	k7c7	nad
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
jabloně	jabloň	k1gFnPc1	jabloň
přerušují	přerušovat	k5eAaImIp3nP	přerušovat
vegetační	vegetační	k2eAgInSc4d1	vegetační
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Kritická	kritický	k2eAgFnSc1d1	kritická
teplota	teplota	k1gFnSc1	teplota
pro	pro	k7c4	pro
květ	květ	k1gInSc4	květ
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
-2	-2	k4	-2
°	°	k?	°
<g/>
C.	C.	kA	C.
Pro	pro	k7c4	pro
poupě	poupě	k1gNnSc4	poupě
přibližně	přibližně	k6eAd1	přibližně
-1,5	-1,5	k4	-1,5
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
-4	-4	k4	-4
°	°	k?	°
<g/>
C.	C.	kA	C.
Dřevo	dřevo	k1gNnSc1	dřevo
zmrzne	zmrznout	k5eAaPmIp3nS	zmrznout
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
-35	-35	k4	-35
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kritická	kritický	k2eAgFnSc1d1	kritická
teplota	teplota	k1gFnSc1	teplota
-10	-10	k4	-10
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
-20	-20	k4	-20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnPc4d1	základní
podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
zamrznutí	zamrznutí	k1gNnSc4	zamrznutí
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
teplotách	teplota	k1gFnPc6	teplota
je	být	k5eAaImIp3nS	být
rychlá	rychlý	k2eAgFnSc1d1	rychlá
změna	změna	k1gFnSc1	změna
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrazně	výrazně	k6eAd1	výrazně
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
po	po	k7c6	po
období	období	k1gNnSc6	období
teplot	teplota	k1gFnPc2	teplota
kolem	kolem	k7c2	kolem
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
mrazem	mráz	k1gInSc7	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jasném	jasný	k2eAgNnSc6d1	jasné
počasí	počasí	k1gNnSc6	počasí
se	se	k3xPyFc4	se
osluněná	osluněný	k2eAgFnSc1d1	osluněná
část	část	k1gFnSc1	část
stromu	strom	k1gInSc2	strom
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
při	při	k7c6	při
následném	následný	k2eAgNnSc6d1	následné
ochlazení	ochlazení	k1gNnSc6	ochlazení
během	během	k7c2	během
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
-15	-15	k4	-15
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
poškození	poškození	k1gNnSc1	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
mrazové	mrazový	k2eAgFnPc1d1	mrazová
trhliny	trhlina	k1gFnPc1	trhlina
<g/>
,	,	kIx,	,
mrazové	mrazový	k2eAgFnPc1d1	mrazová
desky	deska	k1gFnPc1	deska
na	na	k7c6	na
kmeni	kmen	k1gInSc6	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
jsou	být	k5eAaImIp3nP	být
poškození	poškození	k1gNnSc3	poškození
květů	květ	k1gInPc2	květ
vlivem	vliv	k1gInSc7	vliv
chladného	chladný	k2eAgNnSc2d1	chladné
počasí	počasí	k1gNnSc2	počasí
během	během	k7c2	během
kvetení	kvetení	k1gNnSc2	kvetení
a	a	k8xC	a
mladých	mladý	k2eAgInPc2d1	mladý
plůdků	plůdek	k1gInPc2	plůdek
<g/>
.	.	kIx.	.
</s>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
jabloní	jabloň	k1gFnPc2	jabloň
začalo	začít	k5eAaPmAgNnS	začít
300	[number]	k4	300
př.n.l.	př.n.l.	k?	př.n.l.
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
byla	být	k5eAaImAgNnP	být
jablka	jablko	k1gNnPc1	jablko
u	u	k7c2	u
Římanů	Říman	k1gMnPc2	Říman
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
jabloně	jabloň	k1gFnSc2	jabloň
uměli	umět	k5eAaImAgMnP	umět
i	i	k9	i
roubovat	roubovat	k5eAaImF	roubovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
s	s	k7c7	s
určitostí	určitost	k1gFnSc7	určitost
začaly	začít	k5eAaPmAgFnP	začít
jabloně	jabloň	k1gFnPc4	jabloň
pěstovat	pěstovat	k5eAaImF	pěstovat
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rozkvět	rozkvět	k1gInSc1	rozkvět
pěstování	pěstování	k1gNnSc1	pěstování
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
byl	být	k5eAaImAgInS	být
dalším	další	k2eAgInSc7d1	další
mezníkem	mezník	k1gInSc7	mezník
rozvoje	rozvoj	k1gInSc2	rozvoj
pěstování	pěstování	k1gNnSc2	pěstování
začátek	začátek	k1gInSc4	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
také	také	k9	také
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pomologická	pomologický	k2eAgFnSc1d1	pomologická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
registrovala	registrovat	k5eAaBmAgFnS	registrovat
odrůdy	odrůda	k1gFnPc4	odrůda
<g/>
.	.	kIx.	.
</s>
