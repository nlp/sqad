<s>
Cementování	cementování	k1gNnSc1	cementování
nebo	nebo	k8xC	nebo
cementace	cementace	k1gFnSc1	cementace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
proces	proces	k1gInSc4	proces
povrchového	povrchový	k2eAgNnSc2d1	povrchové
zušlechťování	zušlechťování	k1gNnSc2	zušlechťování
oceli	ocel	k1gFnSc2	ocel
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
povrchové	povrchový	k2eAgFnSc6d1	povrchová
vrstvě	vrstva	k1gFnSc6	vrstva
výrobku	výrobek	k1gInSc2	výrobek
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
obsah	obsah	k1gInSc1	obsah
uhlíku	uhlík	k1gInSc2	uhlík
(	(	kIx(	(
<g/>
do	do	k7c2	do
0,25	[number]	k4	0,25
%	%	kIx~	%
<g/>
)	)	kIx)	)
resp.	resp.	kA	resp.
martenzitu	martenzit	k1gInSc2	martenzit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pak	pak	k6eAd1	pak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
povrchové	povrchový	k2eAgNnSc1d1	povrchové
kalení	kalení	k1gNnSc1	kalení
<g/>
.	.	kIx.	.
</s>
