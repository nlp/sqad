<p>
<s>
Cementování	cementování	k1gNnSc1	cementování
nebo	nebo	k8xC	nebo
cementace	cementace	k1gFnSc1	cementace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
proces	proces	k1gInSc4	proces
povrchového	povrchový	k2eAgNnSc2d1	povrchové
zušlechťování	zušlechťování	k1gNnSc2	zušlechťování
oceli	ocel	k1gFnSc2	ocel
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
povrchové	povrchový	k2eAgFnSc6d1	povrchová
vrstvě	vrstva	k1gFnSc6	vrstva
výrobku	výrobek	k1gInSc2	výrobek
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
obsah	obsah	k1gInSc1	obsah
uhlíku	uhlík	k1gInSc2	uhlík
(	(	kIx(	(
<g/>
do	do	k7c2	do
0,25	[number]	k4	0,25
%	%	kIx~	%
<g/>
)	)	kIx)	)
resp.	resp.	kA	resp.
martenzitu	martenzit	k1gInSc2	martenzit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pak	pak	k6eAd1	pak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
povrchové	povrchový	k2eAgNnSc1d1	povrchové
kalení	kalení	k1gNnSc1	kalení
<g/>
.	.	kIx.	.
</s>
<s>
Nauhličená	nauhličený	k2eAgFnSc1d1	nauhličená
vrstva	vrstva	k1gFnSc1	vrstva
bývá	bývat	k5eAaImIp3nS	bývat
cca	cca	kA	cca
0,8	[number]	k4	0,8
mm	mm	kA	mm
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
hotový	hotový	k2eAgInSc1d1	hotový
výrobek	výrobek	k1gInSc1	výrobek
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
uvnitř	uvnitř	k6eAd1	uvnitř
houževnatý	houževnatý	k2eAgMnSc1d1	houževnatý
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
<g/>
.	.	kIx.	.
</s>
<s>
Cementování	cementování	k1gNnSc1	cementování
už	už	k6eAd1	už
běžně	běžně	k6eAd1	běžně
používali	používat	k5eAaImAgMnP	používat
staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
je	být	k5eAaImIp3nS	být
však	však	k9	však
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
technologie	technologie	k1gFnSc1	technologie
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
už	už	k6eAd1	už
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Oceli	ocel	k1gFnSc2	ocel
k	k	k7c3	k
cementování	cementování	k1gNnSc3	cementování
<g/>
:	:	kIx,	:
uhlíkové	uhlíkový	k2eAgFnSc2d1	uhlíková
<g/>
,	,	kIx,	,
slitinové	slitinový	k2eAgFnSc2d1	slitinová
s	s	k7c7	s
přísadou	přísada	k1gFnSc7	přísada
Cr	cr	k0	cr
<g/>
,	,	kIx,	,
Mn	Mn	k1gFnSc4	Mn
<g/>
,	,	kIx,	,
Ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
,	,	kIx,	,
nízko	nízko	k6eAd1	nízko
a	a	k8xC	a
středně	středně	k6eAd1	středně
legované	legovaný	k2eAgFnPc1d1	legovaná
třídy	třída	k1gFnPc1	třída
14	[number]	k4	14
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technologie	technologie	k1gFnSc2	technologie
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
dodávání	dodávání	k1gNnSc3	dodávání
uhlíku	uhlík	k1gInSc2	uhlík
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
cementační	cementační	k2eAgInSc1d1	cementační
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
součástka	součástka	k1gFnSc1	součástka
žíhá	žíhat	k5eAaImIp3nS	žíhat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
930	[number]	k4	930
°	°	k?	°
<g/>
C	C	kA	C
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
uhlíkaté	uhlíkatý	k2eAgFnPc1d1	uhlíkatá
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
kyanidy	kyanid	k1gInPc1	kyanid
<g/>
;	;	kIx,	;
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
jedovaté	jedovatý	k2eAgInPc1d1	jedovatý
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
uhlíkaté	uhlíkatý	k2eAgInPc1d1	uhlíkatý
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
propan	propan	k1gInSc1	propan
<g/>
,	,	kIx,	,
butan	butan	k1gInSc1	butan
nebo	nebo	k8xC	nebo
acetylen	acetylen	k1gInSc1	acetylen
<g/>
,	,	kIx,	,
smíšený	smíšený	k2eAgInSc1d1	smíšený
s	s	k7c7	s
neutrální	neutrální	k2eAgFnSc7d1	neutrální
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nevybuchl	vybuchnout	k5eNaPmAgInS	vybuchnout
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejvíce	hodně	k6eAd3	hodně
používána	používán	k2eAgFnSc1d1	používána
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
účinnost	účinnost	k1gFnSc1	účinnost
plynné	plynný	k2eAgFnSc2d1	plynná
cementace	cementace	k1gFnSc2	cementace
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
ve	v	k7c6	v
vakuové	vakuový	k2eAgFnSc6d1	vakuová
peci	pec	k1gFnSc6	pec
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízkém	nízký	k2eAgInSc6d1	nízký
tlaku	tlak	k1gInSc6	tlak
uhlíkatých	uhlíkatý	k2eAgInPc2d1	uhlíkatý
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nitrocementování	Nitrocementování	k1gNnSc1	Nitrocementování
</s>
</p>
<p>
<s>
Nitridování	nitridování	k1gNnSc1	nitridování
</s>
</p>
<p>
<s>
Kalení	kalení	k1gNnSc1	kalení
</s>
</p>
<p>
<s>
Martenzit	martenzit	k1gInSc1	martenzit
</s>
</p>
<p>
<s>
Ocel	ocel	k1gFnSc1	ocel
</s>
</p>
<p>
<s>
Třídy	třída	k1gFnPc1	třída
oceli	ocel	k1gFnSc2	ocel
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cementování	cementování	k1gNnSc2	cementování
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Cementování	cementování	k1gNnSc2	cementování
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Cementování	cementování	k1gNnSc1	cementování
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Korecký	Korecký	k2eAgMnSc1d1	Korecký
<g/>
,	,	kIx,	,
Cementování	cementování	k1gNnSc2	cementování
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
<g/>
'	'	kIx"	'
nové	nový	k2eAgFnPc1d1	nová
doby	doba	k1gFnPc1	doba
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Cementování	cementování	k1gNnSc1	cementování
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
871	[number]	k4	871
</s>
</p>
