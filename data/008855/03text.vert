<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
kaprinová	kaprinová	k1gFnSc1	kaprinová
(	(	kIx(	(
<g/>
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
kyselina	kyselina	k1gFnSc1	kyselina
dekanová	dekanová	k1gFnSc1	dekanová
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
karboxylová	karboxylový	k2eAgFnSc1d1	karboxylová
kyselina	kyselina	k1gFnSc1	kyselina
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
nasycené	nasycený	k2eAgNnSc4d1	nasycené
mastné	mastné	k1gNnSc4	mastné
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
soli	sůl	k1gFnPc1	sůl
a	a	k8xC	a
estery	ester	k1gInPc1	ester
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
dekanoáty	dekanoáta	k1gFnPc1	dekanoáta
nebo	nebo	k8xC	nebo
kaprináty	kaprinát	k1gInPc1	kaprinát
<g/>
.	.	kIx.	.
</s>
<s>
Triviální	triviální	k2eAgInSc1d1	triviální
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
kozím	kozí	k2eAgNnSc6d1	kozí
mléku	mléko	k1gNnSc6	mléko
(	(	kIx(	(
<g/>
capra	capra	k6eAd1	capra
je	být	k5eAaImIp3nS	být
latinsky	latinsky	k6eAd1	latinsky
koza	koza	k1gFnSc1	koza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
kaprinová	kaprinová	k1gFnSc1	kaprinová
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
kokosovém	kokosový	k2eAgInSc6d1	kokosový
oleji	olej	k1gInSc6	olej
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
palmojádrovém	palmojádrový	k2eAgInSc6d1	palmojádrový
oleji	olej	k1gInSc6	olej
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
olejích	olej	k1gInPc6	olej
ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
mléku	mléko	k1gNnSc6	mléko
různých	různý	k2eAgMnPc2d1	různý
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
živočišných	živočišný	k2eAgInPc6d1	živočišný
tucích	tuk	k1gInPc6	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
dvěma	dva	k4xCgMnPc7	dva
kyselinami	kyselina	k1gFnPc7	kyselina
pojmenovanými	pojmenovaný	k2eAgMnPc7d1	pojmenovaný
podle	podle	k7c2	podle
výskytu	výskyt	k1gInSc2	výskyt
v	v	k7c6	v
kozím	kozí	k2eAgNnSc6d1	kozí
mléku	mléko	k1gNnSc6	mléko
<g/>
,	,	kIx,	,
kapronovou	kapronový	k2eAgFnSc7d1	kapronová
a	a	k8xC	a
kyselinou	kyselina	k1gFnSc7	kyselina
kaprylovou	kaprylový	k2eAgFnSc7d1	kaprylová
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
15	[number]	k4	15
%	%	kIx~	%
tuku	tuk	k1gInSc3	tuk
kozího	kozí	k2eAgNnSc2d1	kozí
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
reakce	reakce	k1gFnSc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
kyselinu	kyselina	k1gFnSc4	kyselina
lze	lze	k6eAd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
oxidací	oxidace	k1gFnSc7	oxidace
primárního	primární	k2eAgInSc2d1	primární
alkoholu	alkohol	k1gInSc2	alkohol
dekanolu	dekanol	k1gInSc2	dekanol
oxidem	oxid	k1gInSc7	oxid
chromovým	chromový	k2eAgInSc7d1	chromový
v	v	k7c6	v
kyselém	kyselý	k2eAgNnSc6d1	kyselé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
<g/>
neutralizací	neutralizace	k1gFnSc7	neutralizace
kyseliny	kyselina	k1gFnSc2	kyselina
kaprinové	kaprinová	k1gFnSc2	kaprinová
nebo	nebo	k8xC	nebo
zmýdelněním	zmýdelnění	k1gNnSc7	zmýdelnění
jejich	jejich	k3xOp3gInPc2	jejich
esterů	ester	k1gInPc2	ester
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
triacylglycerolů	triacylglycerol	k1gInPc2	triacylglycerol
<g/>
)	)	kIx)	)
hydroxidy	hydroxid	k1gInPc1	hydroxid
vznikají	vznikat	k5eAaImIp3nP	vznikat
příslušné	příslušný	k2eAgFnPc1d1	příslušná
soli	sůl	k1gFnPc1	sůl
<g/>
,	,	kIx,	,
sodné	sodný	k2eAgFnPc1d1	sodná
a	a	k8xC	a
draselné	draselný	k2eAgFnPc1d1	draselná
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
mýdel	mýdlo	k1gNnPc2	mýdlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
kaprinová	kaprinová	k1gFnSc1	kaprinová
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
výrobů	výrob	k1gInPc2	výrob
esterů	ester	k1gInPc2	ester
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
umělá	umělý	k2eAgNnPc1d1	umělé
aromata	aroma	k1gNnPc1	aroma
a	a	k8xC	a
parfémy	parfém	k1gInPc1	parfém
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
některá	některý	k3yIgNnPc4	některý
léčiva	léčivo	k1gNnPc4	léčivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Decanoic	Decanoic	k1gMnSc1	Decanoic
acid	acid	k1gMnSc1	acid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
