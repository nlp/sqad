<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Norska	Norsko	k1gNnSc2	Norsko
je	být	k5eAaImIp3nS	být
červená	červená	k1gFnSc1	červená
s	s	k7c7	s
tmavě	tmavě	k6eAd1	tmavě
modrým	modrý	k2eAgInSc7d1	modrý
heroldským	heroldský	k2eAgInSc7d1	heroldský
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
bíle	bíle	k6eAd1	bíle
orámován	orámován	k2eAgInSc1d1	orámován
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kříž	kříž	k1gInSc1	kříž
i	i	k8xC	i
orámování	orámování	k1gNnSc1	orámování
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
k	k	k7c3	k
okrajům	okraj	k1gInPc3	okraj
<g/>
.	.	kIx.	.
</s>
