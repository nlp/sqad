<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Norska	Norsko	k1gNnSc2	Norsko
je	být	k5eAaImIp3nS	být
červená	červená	k1gFnSc1	červená
s	s	k7c7	s
tmavě	tmavě	k6eAd1	tmavě
modrým	modrý	k2eAgInSc7d1	modrý
heroldským	heroldský	k2eAgInSc7d1	heroldský
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
bíle	bíle	k6eAd1	bíle
orámován	orámován	k2eAgInSc1d1	orámován
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kříž	kříž	k1gInSc1	kříž
i	i	k8xC	i
orámování	orámování	k1gNnSc1	orámování
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
k	k	k7c3	k
okrajům	okraj	k1gInPc3	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgNnSc4d2	kratší
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
kříže	kříž	k1gInSc2	kříž
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
posunuto	posunout	k5eAaPmNgNnS	posunout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
žerďovému	žerďový	k2eAgInSc3d1	žerďový
lemu	lem	k1gInSc3	lem
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Norská	norský	k2eAgFnSc1d1	norská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
Dannebrogu	Dannebrog	k1gInSc6	Dannebrog
<g/>
,	,	kIx,	,
dánské	dánský	k2eAgFnSc3d1	dánská
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
obdélníkovým	obdélníkový	k2eAgInSc7d1	obdélníkový
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
barevné	barevný	k2eAgInPc1d1	barevný
elementy	element	k1gInPc1	element
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
v	v	k7c6	v
poměrech	poměr	k1gInPc6	poměr
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
červená	červený	k2eAgNnPc4d1	červené
<g/>
:	:	kIx,	:
<g/>
bílá	bílý	k2eAgNnPc4d1	bílé
<g/>
:	:	kIx,	:
<g/>
modrá	modrý	k2eAgNnPc4d1	modré
<g/>
:	:	kIx,	:
<g/>
bílá	bílý	k2eAgNnPc4d1	bílé
<g/>
:	:	kIx,	:
<g/>
červená	červený	k2eAgNnPc4d1	červené
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
(	(	kIx(	(
<g/>
červená	červený	k2eAgNnPc4d1	červené
<g/>
:	:	kIx,	:
<g/>
bílá	bílý	k2eAgNnPc4d1	bílé
<g/>
:	:	kIx,	:
<g/>
modrá	modrý	k2eAgNnPc4d1	modré
<g/>
:	:	kIx,	:
<g/>
bílá	bílý	k2eAgNnPc4d1	bílé
<g/>
:	:	kIx,	:
<g/>
červená	červené	k1gNnPc4	červené
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
===	===	k?	===
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
státní	státní	k2eAgFnSc1d1	státní
a	a	k8xC	a
válečná	válečný	k2eAgFnSc1d1	válečná
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
obdélníku	obdélník	k1gInSc2	obdélník
zakončeného	zakončený	k2eAgInSc2d1	zakončený
třemi	tři	k4xCgInPc7	tři
plameny	plamen	k1gInPc7	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
27	[number]	k4	27
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
barevné	barevný	k2eAgInPc1d1	barevný
elementy	element	k1gInPc1	element
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
v	v	k7c6	v
poměrech	poměr	k1gInPc6	poměr
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
:	:	kIx,	:
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
:	:	kIx,	:
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
:	:	kIx,	:
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
(	(	kIx(	(
<g/>
červená	červený	k2eAgNnPc4d1	červené
<g/>
:	:	kIx,	:
<g/>
bílá	bílý	k2eAgNnPc4d1	bílé
<g/>
:	:	kIx,	:
<g/>
modrá	modrý	k2eAgNnPc4d1	modré
<g/>
:	:	kIx,	:
<g/>
bílá	bílý	k2eAgNnPc4d1	bílé
<g/>
:	:	kIx,	:
<g/>
červená	červené	k1gNnPc4	červené
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Barvy	barva	k1gFnSc2	barva
===	===	k?	===
</s>
</p>
<p>
<s>
Červená	červený	k2eAgFnSc1d1	červená
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
PMS	PMS	kA	PMS
032	[number]	k4	032
U	U	kA	U
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
PMS	PMS	kA	PMS
281	[number]	k4	281
U	U	kA	U
v	v	k7c6	v
barevném	barevný	k2eAgInSc6d1	barevný
systému	systém	k1gInSc6	systém
Pantone	Panton	k1gInSc5	Panton
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
přibližně	přibližně	k6eAd1	přibližně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
RGB	RGB	kA	RGB
systému	systém	k1gInSc2	systém
barev	barva	k1gFnPc2	barva
hodnotám	hodnota	k1gFnPc3	hodnota
#	#	kIx~	#
<g/>
EF	EF	kA	EF
<g/>
2	[number]	k4	2
<g/>
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
D	D	kA	D
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
2868	[number]	k4	2868
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
)	)	kIx)	)
a	a	k8xC	a
#	#	kIx~	#
<g/>
FFFFFF	FFFFFF	kA	FFFFFF
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
Norsko	Norsko	k1gNnSc1	Norsko
používalo	používat	k5eAaImAgNnS	používat
dánskou	dánský	k2eAgFnSc4d1	dánská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
během	během	k7c2	během
krátkého	krátký	k2eAgNnSc2d1	krátké
osamostatnění	osamostatnění	k1gNnSc2	osamostatnění
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
krátkého	krátký	k2eAgNnSc2d1	krátké
panování	panování	k1gNnSc2	panování
Kristiána	Kristián	k1gMnSc2	Kristián
Friderika	Friderik	k1gMnSc2	Friderik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
na	na	k7c4	na
Dannebrogu	Dannebroga	k1gFnSc4	Dannebroga
založená	založený	k2eAgFnSc1d1	založená
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
levém	levý	k2eAgNnSc6d1	levé
horním	horní	k2eAgNnSc6d1	horní
poli	pole	k1gNnSc6	pole
norského	norský	k2eAgInSc2d1	norský
lva	lev	k1gInSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
bylo	být	k5eAaImAgNnS	být
Norsko	Norsko	k1gNnSc1	Norsko
připojeno	připojen	k2eAgNnSc1d1	připojeno
ke	k	k7c3	k
Švédsku	Švédsko	k1gNnSc3	Švédsko
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
pro	pro	k7c4	pro
Norsko	Norsko	k1gNnSc4	Norsko
žlutomodrá	žlutomodrý	k2eAgFnSc1d1	žlutomodrá
švédská	švédský	k2eAgFnSc1d1	švédská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
levém	levý	k2eAgNnSc6d1	levé
horním	horní	k2eAgNnSc6d1	horní
poli	pole	k1gNnSc6	pole
byl	být	k5eAaImAgInS	být
bílý	bílý	k2eAgInSc1d1	bílý
ondřejský	ondřejský	k2eAgInSc1d1	ondřejský
kříž	kříž	k1gInSc1	kříž
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
jako	jako	k8xS	jako
vládní	vládní	k2eAgFnSc1d1	vládní
vlajka	vlajka	k1gFnSc1	vlajka
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgFnPc4d1	obchodní
lodi	loď	k1gFnPc4	loď
plující	plující	k2eAgFnPc4d1	plující
za	za	k7c4	za
mys	mys	k1gInSc4	mys
Finisterre	Finisterr	k1gInSc5	Finisterr
<g/>
,	,	kIx,	,
ta	ten	k3xDgNnPc1	ten
byla	být	k5eAaImAgNnP	být
používána	používat	k5eAaImNgNnP	používat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Norsko	Norsko	k1gNnSc1	Norsko
nemělo	mít	k5eNaImAgNnS	mít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
piráty	pirát	k1gMnPc7	pirát
ze	z	k7c2	z
Severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobitá	osobitý	k2eAgFnSc1d1	osobitá
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
známá	známý	k2eAgFnSc1d1	známá
norská	norský	k2eAgFnSc1d1	norská
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
Fredrikem	Fredrik	k1gMnSc7	Fredrik
Meltzerem	Meltzer	k1gMnSc7	Meltzer
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
norského	norský	k2eAgInSc2d1	norský
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
Storting	Storting	k1gInSc1	Storting
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
byly	být	k5eAaImAgFnP	být
zvoleny	zvolit	k5eAaPmNgFnP	zvolit
povzoru	povzor	k1gInSc3	povzor
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluční	revoluční	k2eAgFnSc2d1	revoluční
trikolóry	trikolóra	k1gFnSc2	trikolóra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
levého	levý	k2eAgNnSc2d1	levé
horního	horní	k2eAgNnSc2d1	horní
pole	pole	k1gNnSc2	pole
norské	norský	k2eAgFnSc2d1	norská
vlajky	vlajka	k1gFnSc2	vlajka
umístěn	umístěn	k2eAgInSc1d1	umístěn
symbol	symbol	k1gInSc1	symbol
norsko-švédské	norsko-švédský	k2eAgFnSc2d1	norsko-švédská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Symbolu	symbol	k1gInSc3	symbol
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
Sildesalaten	Sildesalaten	k2eAgMnSc1d1	Sildesalaten
–	–	k?	–
"	"	kIx"	"
<g/>
sleďový	sleďový	k2eAgInSc1d1	sleďový
salát	salát	k1gInSc1	salát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
salát	salát	k1gInSc1	salát
připomínal	připomínat	k5eAaImAgInS	připomínat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
znak	znak	k1gInSc1	znak
norsko-švédské	norsko-švédský	k2eAgFnSc2d1	norsko-švédská
unie	unie	k1gFnSc2	unie
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
rovnost	rovnost	k1gFnSc4	rovnost
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
svazek	svazek	k1gInSc1	svazek
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
neoblíbený	oblíbený	k2eNgInSc1d1	neoblíbený
a	a	k8xC	a
norský	norský	k2eAgInSc1d1	norský
parlament	parlament	k1gInSc1	parlament
zrušil	zrušit	k5eAaPmAgInS	zrušit
unijní	unijní	k2eAgInSc1d1	unijní
znak	znak	k1gInSc1	znak
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
i	i	k8xC	i
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgInS	být
znak	znak	k1gInSc1	znak
norsko-švédské	norsko-švédský	k2eAgFnSc2d1	norsko-švédská
unie	unie	k1gFnSc2	unie
zrušen	zrušit	k5eAaPmNgInS	zrušit
i	i	k9	i
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
Švédsko	Švédsko	k1gNnSc1	Švédsko
podrželo	podržet	k5eAaPmAgNnS	podržet
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
vlajkách	vlajka	k1gFnPc6	vlajka
také	také	k9	také
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
norských	norský	k2eAgInPc2d1	norský
krajů	kraj	k1gInPc2	kraj
==	==	k?	==
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
18	[number]	k4	18
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
norsky	norsky	k6eAd1	norsky
<g/>
:	:	kIx,	:
fylke	fylke	k1gFnSc1	fylke
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
fylker	fylker	k1gInSc1	fylker
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
známých	známý	k2eAgNnPc2d1	známé
jako	jako	k8xC	jako
amt	amt	k?	amt
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
amter	amter	k1gInSc1	amter
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Oslo	Oslo	k1gNnSc2	Oslo
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
kraj	kraj	k1gInSc4	kraj
i	i	k9	i
za	za	k7c4	za
samosprávné	samosprávný	k2eAgNnSc4d1	samosprávné
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Poměry	poměr	k1gInPc1	poměr
stran	strana	k1gFnPc2	strana
zobrazených	zobrazený	k2eAgFnPc2d1	zobrazená
vlajek	vlajka	k1gFnPc2	vlajka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
poměrů	poměr	k1gInPc2	poměr
uvedených	uvedený	k2eAgInPc2d1	uvedený
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Uvedeny	uveden	k2eAgFnPc1d1	uvedena
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc4	ten
ze	z	k7c2	z
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byly	být	k5eAaImAgFnP	být
sloučeny	sloučit	k5eAaPmNgInP	sloučit
kraje	kraj	k1gInPc1	kraj
Nord-Trø	Nord-Trø	k1gFnPc2	Nord-Trø
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc2d1	severní
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sø	Sø	k1gFnSc2	Sø
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc2d1	jižní
<g/>
)	)	kIx)	)
do	do	k7c2	do
kraje	kraj	k1gInSc2	kraj
Trø	Trø	k1gFnSc2	Trø
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
ponechána	ponechat	k5eAaPmNgFnS	ponechat
ze	z	k7c2	z
severního	severní	k2eAgInSc2d1	severní
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
dvě	dva	k4xCgNnPc1	dva
zámořská	zámořský	k2eAgNnPc1d1	zámořské
teritoria	teritorium	k1gNnPc1	teritorium
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
Jan	Jana	k1gFnPc2	Jana
Mayen	Mayna	k1gFnPc2	Mayna
a	a	k8xC	a
Špicberky	Špicberky	k1gFnPc4	Špicberky
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
antarktická	antarktický	k2eAgNnPc4d1	antarktické
a	a	k8xC	a
subantarktická	subantarktický	k2eAgNnPc4d1	subantarktický
závislá	závislý	k2eAgNnPc4d1	závislé
území	území	k1gNnPc4	území
<g/>
:	:	kIx,	:
Bouvetův	Bouvetův	k2eAgInSc1d1	Bouvetův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
Petra	Petra	k1gFnSc1	Petra
I.	I.	kA	I.
a	a	k8xC	a
Zemi	zem	k1gFnSc4	zem
královny	královna	k1gFnSc2	královna
Maud	Mauda	k1gFnPc2	Mauda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
územích	území	k1gNnPc6	území
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
norská	norský	k2eAgFnSc1d1	norská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Hroch	Hroch	k1gMnSc1	Hroch
M.	M.	kA	M.
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnPc1d1	lidová
Noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
340	[number]	k4	340
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-7106-407-6	[number]	k4	80-7106-407-6
</s>
</p>
<p>
<s>
Frajdl	Frajdnout	k5eAaPmAgMnS	Frajdnout
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Zálabský	zálabský	k2eAgMnSc1d1	zálabský
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Vexilologie	Vexilologie	k1gFnSc1	Vexilologie
<g/>
.	.	kIx.	.
</s>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
politická	politický	k2eAgFnSc1d1	politická
škola	škola	k1gFnSc1	škola
SSM	SSM	kA	SSM
Seč	seč	k1gFnSc1	seč
<g/>
,	,	kIx,	,
Seč	seč	k1gFnSc1	seč
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
127	[number]	k4	127
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Norska	Norsko	k1gNnSc2	Norsko
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Norska	Norsko	k1gNnSc2	Norsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
norská	norský	k2eAgFnSc1d1	norská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Norské	norský	k2eAgFnPc1d1	norská
vlajky	vlajka	k1gFnPc1	vlajka
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
norsky	norsky	k6eAd1	norsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
norské	norský	k2eAgFnSc2d1	norská
vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c4	na
Stortinget	stortinget	k1gInSc4	stortinget
<g/>
.	.	kIx.	.
<g/>
no	no	k9	no
(	(	kIx(	(
<g/>
norsky	norsky	k6eAd1	norsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
vlajek	vlajka	k1gFnPc2	vlajka
Norska	Norsko	k1gNnSc2	Norsko
na	na	k7c4	na
Riksarkivet	Riksarkivet	k1gInSc4	Riksarkivet
<g/>
.	.	kIx.	.
<g/>
no	no	k9	no
(	(	kIx(	(
<g/>
norsky	norsky	k6eAd1	norsky
<g/>
)	)	kIx)	)
</s>
</p>
