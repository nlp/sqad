<s>
Eilenburg	Eilenburg	k1gInSc1	Eilenburg
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
okresní	okresní	k2eAgNnSc4d1	okresní
město	město	k1gNnSc4	město
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Muldě	mulda	k1gFnSc6	mulda
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Severní	severní	k2eAgNnSc4d1	severní
Sasko	Sasko	k1gNnSc4	Sasko
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
od	od	k7c2	od
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
17	[number]	k4	17
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
největším	veliký	k2eAgMnSc7d3	veliký
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Nordsachsen	Nordsachsen	k1gInSc1	Nordsachsen
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
městě	město	k1gNnSc6	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
961	[number]	k4	961
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
Oty	Ota	k1gMnSc2	Ota
I.	I.	kA	I.
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
jmenováno	jmenován	k2eAgNnSc1d1	jmenováno
coby	coby	k?	coby
civitas	civitas	k1gMnSc1	civitas
Ilburg	Ilburg	k1gMnSc1	Ilburg
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
předpona	předpona	k1gFnSc1	předpona
il	il	k?	il
je	být	k5eAaImIp3nS	být
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
příbuzná	příbuzná	k1gFnSc1	příbuzná
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
jíl	jíl	k1gInSc1	jíl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
původní	původní	k2eAgNnSc1d1	původní
osídlení	osídlení	k1gNnSc1	osídlení
bylo	být	k5eAaImAgNnS	být
slovanské	slovanský	k2eAgNnSc1d1	slovanské
<g/>
.	.	kIx.	.
</s>
<s>
Oskar	Oskar	k1gMnSc1	Oskar
Höcker	Höcker	k1gMnSc1	Höcker
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
-	-	kIx~	-
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Butzbach	Butzbach	k1gMnSc1	Butzbach
<g/>
,	,	kIx,	,
Hessensko	Hessensko	k1gNnSc1	Hessensko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Rawicz	Rawicza	k1gFnPc2	Rawicza
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
Tiraspol	Tiraspola	k1gFnPc2	Tiraspola
<g/>
,	,	kIx,	,
Podněstří	Podněstří	k1gFnSc1	Podněstří
(	(	kIx(	(
<g/>
de	de	k?	de
iure	iure	k1gInSc1	iure
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
</s>
