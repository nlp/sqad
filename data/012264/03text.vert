<p>
<s>
Rybák	Rybák	k1gMnSc1	Rybák
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Chlidonias	Chlidonias	k1gMnSc1	Chlidonias
niger	niger	k1gMnSc1	niger
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malým	malý	k2eAgInSc7d1	malý
druhem	druh	k1gInSc7	druh
rybáka	rybák	k1gMnSc2	rybák
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Chlidonias	Chlidoniasa	k1gFnPc2	Chlidoniasa
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
svatebním	svatební	k2eAgInSc6d1	svatební
šatu	šat	k1gInSc6	šat
černou	černý	k2eAgFnSc4d1	černá
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
šedá	šedý	k2eAgNnPc1d1	šedé
křídla	křídlo	k1gNnPc1	křídlo
<g/>
,	,	kIx,	,
kostřec	kostřec	k1gInSc1	kostřec
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
a	a	k8xC	a
bílé	bílý	k2eAgFnPc1d1	bílá
podocasní	podocasní	k2eAgFnPc1d1	podocasní
krovky	krovka	k1gFnPc1	krovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostém	prostý	k2eAgInSc6d1	prostý
šatu	šat	k1gInSc6	šat
a	a	k8xC	a
šatu	šat	k1gInSc6	šat
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
ostatním	ostatní	k2eAgInPc3d1	ostatní
druhům	druh	k1gInPc3	druh
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc2d1	západní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tažný	tažný	k2eAgMnSc1d1	tažný
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Chlidonias	Chlidonias	k1gMnSc1	Chlidonias
niger	niger	k1gMnSc1	niger
niger	niger	k1gMnSc1	niger
(	(	kIx(	(
<g/>
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
-	-	kIx~	-
rybák	rybák	k1gMnSc1	rybák
černý	černý	k1gMnSc1	černý
eurasijský	eurasijský	k2eAgMnSc1d1	eurasijský
</s>
</p>
<p>
<s>
Chlidonias	Chlidonias	k1gInSc1	Chlidonias
niger	niger	k1gInSc1	niger
surinamensis	surinamensis	k1gFnSc1	surinamensis
(	(	kIx(	(
<g/>
Gmelin	Gmelin	k1gInSc1	Gmelin
<g/>
,	,	kIx,	,
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
-	-	kIx~	-
rybák	rybák	k1gMnSc1	rybák
černý	černý	k1gMnSc1	černý
americký	americký	k2eAgMnSc1d1	americký
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
25	[number]	k4	25
cm	cm	kA	cm
dlouzí	dlouhý	k2eAgMnPc1d1	dlouhý
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc3	jejich
rozpětí	rozpětí	k1gNnSc3	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
61	[number]	k4	61
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
rybáků	rybák	k1gMnPc2	rybák
černých	černá	k1gFnPc2	černá
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
;	;	kIx,	;
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
62	[number]	k4	62
g.	g.	k?	g.
Mají	mít	k5eAaImIp3nP	mít
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgFnPc1d1	tmavá
nohy	noha	k1gFnPc1	noha
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
tmavě	tmavě	k6eAd1	tmavě
šedá	šedý	k2eAgNnPc4d1	šedé
záda	záda	k1gNnPc4	záda
s	s	k7c7	s
dlouhými	dlouhý	k2eAgNnPc7d1	dlouhé
křídly	křídlo	k1gNnPc7	křídlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
vepředu	vepředu	k6eAd1	vepředu
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Černá	Černá	k1gFnSc1	Černá
převládá	převládat	k5eAaImIp3nS	převládat
všude	všude	k6eAd1	všude
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
na	na	k7c6	na
krku	krk	k1gInSc6	krk
(	(	kIx(	(
<g/>
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
šedým	šedý	k2eAgInSc7d1	šedý
krkem	krk	k1gInSc7	krk
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Ocasní	ocasní	k2eAgNnSc1d1	ocasní
peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
šedé	šedý	k2eAgNnSc1d1	šedé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozeznat	rozeznat	k5eAaPmF	rozeznat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
poddruhy	poddruh	k1gInPc4	poddruh
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
eurasijský	eurasijský	k2eAgInSc1d1	eurasijský
poddruh	poddruh	k1gInSc1	poddruh
je	být	k5eAaImIp3nS	být
drobnější	drobný	k2eAgNnSc4d2	drobnější
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
světlejší	světlý	k2eAgFnPc4d2	světlejší
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
těžší	těžký	k2eAgMnSc1d2	těžší
je	být	k5eAaImIp3nS	být
rozeznat	rozeznat	k5eAaPmF	rozeznat
pohlaví	pohlaví	k1gNnSc4	pohlaví
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
minimální	minimální	k2eAgMnSc1d1	minimální
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
žádný	žádný	k3yNgInSc1	žádný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kříženci	kříženec	k1gMnPc5	kříženec
===	===	k?	===
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
kříženci	kříženec	k1gMnPc1	kříženec
rybáků	rybák	k1gMnPc2	rybák
černých	černý	k1gMnPc2	černý
a	a	k8xC	a
rybáků	rybák	k1gMnPc2	rybák
bělokřídlých	bělokřídlý	k2eAgMnPc2d1	bělokřídlý
(	(	kIx(	(
<g/>
Chlidonias	Chlidonias	k1gMnSc1	Chlidonias
leucopterus	leucopterus	k1gMnSc1	leucopterus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takoví	takový	k3xDgMnPc1	takový
kříženci	kříženec	k1gMnPc1	kříženec
nesou	nést	k5eAaImIp3nP	nést
znaky	znak	k1gInPc4	znak
obou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
břicho	břicho	k1gNnSc4	břicho
bílé	bílý	k2eAgNnSc4d1	bílé
s	s	k7c7	s
tmavými	tmavý	k2eAgFnPc7d1	tmavá
skvrnkami	skvrnka	k1gFnPc7	skvrnka
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
těla	tělo	k1gNnSc2	tělo
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
křížení	křížení	k1gNnSc1	křížení
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
především	především	k9	především
na	na	k7c6	na
území	území	k1gNnSc6	území
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
hnízdění	hnízdění	k1gNnSc3	hnízdění
využívá	využívat	k5eAaPmIp3nS	využívat
rybák	rybák	k1gMnSc1	rybák
černý	černý	k2eAgMnSc1d1	černý
zarostlé	zarostlý	k2eAgInPc4d1	zarostlý
okraje	okraj	k1gInPc4	okraj
sladkovodních	sladkovodní	k2eAgInPc2d1	sladkovodní
mokřadů	mokřad	k1gInPc2	mokřad
<g/>
.	.	kIx.	.
</s>
<s>
Sdružují	sdružovat	k5eAaImIp3nP	sdružovat
se	se	k3xPyFc4	se
do	do	k7c2	do
menších	malý	k2eAgFnPc2d2	menší
kolonií	kolonie	k1gFnPc2	kolonie
okolo	okolo	k7c2	okolo
20	[number]	k4	20
párů	pár	k1gInPc2	pár
(	(	kIx(	(
<g/>
zřídka	zřídka	k6eAd1	zřídka
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
i	i	k8xC	i
párů	pár	k1gInPc2	pár
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
jen	jen	k9	jen
v	v	k7c6	v
párech	pár	k1gInPc6	pár
<g/>
.	.	kIx.	.
</s>
<s>
Hnízda	hnízdo	k1gNnPc1	hnízdo
si	se	k3xPyFc3	se
staví	stavit	k5eAaBmIp3nP	stavit
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
vegetaci	vegetace	k1gFnSc6	vegetace
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
vajíčka	vajíčko	k1gNnPc4	vajíčko
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
to	ten	k3xDgNnSc1	ten
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
vejce	vejce	k1gNnSc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Inkubaci	inkubace	k1gFnSc4	inkubace
provádí	provádět	k5eAaImIp3nS	provádět
střídavě	střídavě	k6eAd1	střídavě
oba	dva	k4xCgMnPc4	dva
rodiče	rodič	k1gMnPc4	rodič
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
za	za	k7c4	za
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
až	až	k6eAd1	až
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
opeří	opeřit	k5eAaPmIp3nP	opeřit
a	a	k8xC	a
vyletí	vyletět	k5eAaPmIp3nP	vyletět
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počty	počet	k1gInPc1	počet
těchto	tento	k3xDgMnPc2	tento
ptáků	pták	k1gMnPc2	pták
pomalu	pomalu	k6eAd1	pomalu
klesají	klesat	k5eAaImIp3nP	klesat
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
kvůli	kvůli	k7c3	kvůli
přirozenému	přirozený	k2eAgInSc3d1	přirozený
úbytku	úbytek	k1gInSc3	úbytek
nebo	nebo	k8xC	nebo
kvůli	kvůli	k7c3	kvůli
lidské	lidský	k2eAgFnSc3d1	lidská
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
snižování	snižování	k1gNnSc4	snižování
jejich	jejich	k3xOp3gInPc2	jejich
počtů	počet	k1gInPc2	počet
může	moct	k5eAaImIp3nS	moct
především	především	k9	především
znečišťování	znečišťování	k1gNnSc1	znečišťování
a	a	k8xC	a
zalidňování	zalidňování	k1gNnSc1	zalidňování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
způsobené	způsobený	k2eAgNnSc4d1	způsobené
i	i	k9	i
rozmnožením	rozmnožení	k1gNnSc7	rozmnožení
jejich	jejich	k3xOp3gMnPc2	jejich
přirozených	přirozený	k2eAgMnPc2d1	přirozený
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
potravu	potrava	k1gFnSc4	potrava
vejce	vejce	k1gNnSc2	vejce
rybáků	rybák	k1gMnPc2	rybák
černých	černý	k2eAgMnPc2d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgMnPc4	tento
ptáky	pták	k1gMnPc4	pták
platí	platit	k5eAaImIp3nS	platit
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
africko-eurasijských	africkourasijský	k2eAgMnPc2d1	africko-eurasijský
stěhovavých	stěhovavý	k2eAgMnPc2d1	stěhovavý
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
zařazeni	zařadit	k5eAaPmNgMnP	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
kriticky	kriticky	k6eAd1	kriticky
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
málo	málo	k6eAd1	málo
dotčené	dotčený	k2eAgNnSc1d1	dotčené
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rybák	Rybák	k1gMnSc1	Rybák
černý	černý	k1gMnSc1	černý
americký	americký	k2eAgMnSc1d1	americký
běžně	běžně	k6eAd1	běžně
migruje	migrovat	k5eAaImIp3nS	migrovat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Rybák	Rybák	k1gMnSc1	Rybák
černý	černý	k1gMnSc1	černý
eurasijský	eurasijský	k2eAgMnSc1d1	eurasijský
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potravu	potrava	k1gFnSc4	potrava
shánějí	shánět	k5eAaImIp3nP	shánět
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
vodní	vodní	k2eAgInSc4d1	vodní
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc4d1	malá
rybky	rybka	k1gFnPc4	rybka
<g/>
,	,	kIx,	,
plži	plž	k1gMnPc1	plž
<g/>
,	,	kIx,	,
pulci	pulec	k1gMnPc1	pulec
a	a	k8xC	a
žáby	žába	k1gFnPc1	žába
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Black	Black	k1gMnSc1	Black
Tern	Tern	k1gMnSc1	Tern
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rybák	rybák	k1gMnSc1	rybák
černý	černý	k2eAgMnSc1d1	černý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Chlidonias	Chlidonias	k1gInSc1	Chlidonias
niger	niger	k1gInSc1	niger
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
