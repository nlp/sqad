<p>
<s>
Bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Castor	Castor	k1gMnSc1	Castor
fiber	fiber	k1gMnSc1	fiber
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
až	až	k9	až
metr	metr	k1gInSc1	metr
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
zavalitý	zavalitý	k2eAgMnSc1d1	zavalitý
hlodavec	hlodavec	k1gMnSc1	hlodavec
s	s	k7c7	s
hustou	hustý	k2eAgFnSc7d1	hustá
srstí	srst	k1gFnSc7	srst
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
plochým	plochý	k2eAgInSc7d1	plochý
ocasem	ocas	k1gInSc7	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
přizpůsoben	přizpůsoben	k2eAgInSc1d1	přizpůsoben
životu	život	k1gInSc3	život
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Buduje	budovat	k5eAaImIp3nS	budovat
soustavy	soustava	k1gFnPc4	soustava
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
hrází	hráz	k1gFnPc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
kromě	kromě	k7c2	kromě
bylin	bylina	k1gFnPc2	bylina
i	i	k8xC	i
větvičky	větvička	k1gFnSc2	větvička
a	a	k8xC	a
lýko	lýko	k1gNnSc4	lýko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecné	obecný	k2eAgFnPc4d1	obecná
informace	informace	k1gFnPc4	informace
==	==	k?	==
</s>
</p>
<p>
<s>
Bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
až	až	k9	až
30	[number]	k4	30
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jihoamerické	jihoamerický	k2eAgFnSc6d1	jihoamerická
kapybaře	kapybara	k1gFnSc6	kapybara
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
hlodavcem	hlodavec	k1gMnSc7	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
porostlé	porostlý	k2eAgNnSc1d1	porostlé
velmi	velmi	k6eAd1	velmi
hustou	hustý	k2eAgFnSc7d1	hustá
černohnědou	černohnědý	k2eAgFnSc7d1	černohnědá
srstí	srst	k1gFnSc7	srst
o	o	k7c6	o
hustotě	hustota	k1gFnSc6	hustota
až	až	k9	až
300	[number]	k4	300
chlupů	chlup	k1gInPc2	chlup
na	na	k7c4	na
mm	mm	kA	mm
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dokonale	dokonale	k6eAd1	dokonale
přizpůsoben	přizpůsoben	k2eAgInSc1d1	přizpůsoben
životu	život	k1gInSc3	život
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
díky	díky	k7c3	díky
plovacím	plovací	k2eAgFnPc3d1	plovací
blanám	blána	k1gFnPc3	blána
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
končetinách	končetina	k1gFnPc6	končetina
<g/>
,	,	kIx,	,
a	a	k8xC	a
uzavíratelným	uzavíratelný	k2eAgFnPc3d1	uzavíratelná
nozdrám	nozdra	k1gFnPc3	nozdra
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřít	uzavřít	k5eAaPmF	uzavřít
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
tlamu	tlama	k1gFnSc4	tlama
ihned	ihned	k6eAd1	ihned
za	za	k7c4	za
řezáky	řezák	k1gInPc4	řezák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
využití	využití	k1gNnSc1	využití
zubů	zub	k1gInPc2	zub
jako	jako	k8xS	jako
nástrojů	nástroj	k1gInPc2	nástroj
i	i	k9	i
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
před	před	k7c7	před
vodou	voda	k1gFnSc7	voda
chráněny	chránit	k5eAaImNgFnP	chránit
průhledným	průhledný	k2eAgMnSc7d1	průhledný
víčkem-mžurkou	víčkemžurka	k1gFnSc7	víčkem-mžurka
<g/>
.	.	kIx.	.
</s>
<s>
Výměšky	výměšek	k1gInPc1	výměšek
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
žlázy	žláza	k1gFnSc2	žláza
impregnují	impregnovat	k5eAaBmIp3nP	impregnovat
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
vydrží	vydržet	k5eAaPmIp3nS	vydržet
až	až	k9	až
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Chápavé	chápavý	k2eAgFnPc4d1	chápavá
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
díky	díky	k7c3	díky
pátému	pátý	k4xOgNnSc3	pátý
částečně	částečně	k6eAd1	částečně
protistojnému	protistojný	k2eAgInSc3d1	protistojný
prstu	prst	k1gInSc3	prst
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
především	především	k9	především
stavbu	stavba	k1gFnSc4	stavba
obydlí	obydlí	k1gNnSc2	obydlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
je	být	k5eAaImIp3nS	být
chráněn	chráněn	k2eAgMnSc1d1	chráněn
jak	jak	k8xC	jak
českou	český	k2eAgFnSc7d1	Česká
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
evropskou	evropský	k2eAgFnSc4d1	Evropská
(	(	kIx(	(
<g/>
Směrnice	směrnice	k1gFnSc1	směrnice
92	[number]	k4	92
<g/>
/	/	kIx~	/
<g/>
43	[number]	k4	43
<g/>
/	/	kIx~	/
<g/>
EEC	EEC	kA	EEC
o	o	k7c6	o
stanovištích	stanoviště	k1gNnPc6	stanoviště
<g/>
,	,	kIx,	,
příloha	příloha	k1gFnSc1	příloha
II	II	kA	II
a	a	k8xC	a
IV	IV	kA	IV
<g/>
)	)	kIx)	)
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
legislativou	legislativa	k1gFnSc7	legislativa
(	(	kIx(	(
<g/>
IUCN	IUCN	kA	IUCN
Red	Red	k1gFnSc1	Red
List	list	k1gInSc1	list
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc1d1	dotčený
druh	druh	k1gInSc1	druh
-	-	kIx~	-
LC	LC	kA	LC
-	-	kIx~	-
least	least	k1gMnSc1	least
concern	concern	k1gMnSc1	concern
<g/>
;	;	kIx,	;
Bernská	bernský	k2eAgFnSc1d1	Bernská
úmluva	úmluva	k1gFnSc1	úmluva
<g/>
,	,	kIx,	,
příloha	příloha	k1gFnSc1	příloha
III	III	kA	III
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
je	být	k5eAaImIp3nS	být
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
na	na	k7c6	na
říčních	říční	k2eAgFnPc6d1	říční
a	a	k8xC	a
lužních	lužní	k2eAgFnPc6d1	lužní
nivách	niva	k1gFnPc6	niva
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
přechodném	přechodný	k2eAgNnSc6d1	přechodné
vyhubení	vyhubení	k1gNnSc6	vyhubení
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začal	začít	k5eAaPmAgMnS	začít
vracet	vracet	k5eAaImF	vracet
migrací	migrace	k1gFnSc7	migrace
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
Odry	Odra	k1gFnSc2	Odra
<g/>
,	,	kIx,	,
Dyje	Dyje	k1gFnSc2	Dyje
a	a	k8xC	a
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
Kateřinském	kateřinský	k2eAgInSc6d1	kateřinský
potoku	potok	k1gInSc6	potok
<g/>
,	,	kIx,	,
Mži	Mže	k1gFnSc6	Mže
<g/>
,	,	kIx,	,
Radbuze	Radbuza	k1gFnSc6	Radbuza
<g/>
,	,	kIx,	,
Křemelné	křemelný	k2eAgFnSc3d1	Křemelná
a	a	k8xC	a
také	také	k9	také
Řezné	řezný	k2eAgInPc1d1	řezný
v	v	k7c6	v
Plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
na	na	k7c6	na
Úhlavě	Úhlava	k1gFnSc6	Úhlava
a	a	k8xC	a
Desné	Desná	k1gFnSc6	Desná
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Chebu	Cheb	k1gInSc2	Cheb
na	na	k7c6	na
přítocích	přítok	k1gInPc6	přítok
řeky	řeka	k1gFnSc2	řeka
Ohře	Ohře	k1gFnSc2	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
se	se	k3xPyFc4	se
bobři	bobr	k1gMnPc1	bobr
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Olšavě	Olšava	k1gFnSc6	Olšava
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Uherského	uherský	k2eAgInSc2d1	uherský
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
ČR	ČR	kA	ČR
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
přes	přes	k7c4	přes
1000	[number]	k4	1000
bobrů	bobr	k1gMnPc2	bobr
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
luzích	luh	k1gInPc6	luh
Litovelského	litovelský	k2eAgNnSc2d1	Litovelské
Pomoraví	Pomoraví	k1gNnSc2	Pomoraví
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
Července	červenka	k1gFnSc6	červenka
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
)	)	kIx)	)
asi	asi	k9	asi
300	[number]	k4	300
ks	ks	kA	ks
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
lužních	lužní	k2eAgInPc2d1	lužní
lesů	les	k1gInPc2	les
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
od	od	k7c2	od
Lednicko-valtického	lednickoaltický	k2eAgInSc2d1	lednicko-valtický
areálu	areál	k1gInSc2	areál
po	po	k7c6	po
oboru	obor	k1gInSc6	obor
Soutok	soutok	k1gInSc1	soutok
<g/>
,	,	kIx,	,
asi	asi	k9	asi
500	[number]	k4	500
ks	ks	kA	ks
<g/>
.	.	kIx.	.
</s>
<s>
Početnost	početnost	k1gFnSc1	početnost
bobrů	bobr	k1gMnPc2	bobr
kvůli	kvůli	k7c3	kvůli
absenci	absence	k1gFnSc3	absence
přirozených	přirozený	k2eAgMnPc2d1	přirozený
nepřátel	nepřítel	k1gMnPc2	nepřítel
a	a	k8xC	a
legislativní	legislativní	k2eAgFnSc3d1	legislativní
ochraně	ochrana	k1gFnSc3	ochrana
stále	stále	k6eAd1	stále
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
aktivita	aktivita	k1gFnSc1	aktivita
bobra	bobr	k1gMnSc2	bobr
způsobí	způsobit	k5eAaPmIp3nS	způsobit
škodu	škoda	k1gFnSc4	škoda
na	na	k7c6	na
soukromém	soukromý	k2eAgInSc6d1	soukromý
majetku	majetek	k1gInSc6	majetek
<g/>
,	,	kIx,	,
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
zákonné	zákonný	k2eAgFnPc4d1	zákonná
normy	norma	k1gFnPc4	norma
ČR	ČR	kA	ČR
zažádat	zažádat	k5eAaPmF	zažádat
o	o	k7c4	o
náhradu	náhrada	k1gFnSc4	náhrada
škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bobři	bobr	k1gMnPc1	bobr
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Bobr	bobr	k1gMnSc1	bobr
patřil	patřit	k5eAaImAgMnS	patřit
odedávna	odedávna	k6eAd1	odedávna
mezi	mezi	k7c4	mezi
lovnou	lovný	k2eAgFnSc4d1	lovná
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kožešina	kožešina	k1gFnSc1	kožešina
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
ceněna	cenit	k5eAaImNgFnS	cenit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
čepic	čepice	k1gFnPc2	čepice
a	a	k8xC	a
límců	límec	k1gInPc2	límec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
užívalo	užívat	k5eAaImAgNnS	užívat
se	se	k3xPyFc4	se
i	i	k9	i
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
pokládáno	pokládán	k2eAgNnSc1d1	pokládáno
za	za	k7c4	za
postní	postní	k2eAgNnSc4d1	postní
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bobr	bobr	k1gMnSc1	bobr
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
pochoutku	pochoutka	k1gFnSc4	pochoutka
byl	být	k5eAaImAgInS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
bobří	bobří	k2eAgInSc1d1	bobří
ocas	ocas	k1gInSc1	ocas
a	a	k8xC	a
zadní	zadní	k2eAgFnPc1d1	zadní
nohy	noha	k1gFnPc1	noha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzských	francouzský	k2eAgInPc6d1	francouzský
klášterech	klášter	k1gInPc6	klášter
se	se	k3xPyFc4	se
dělaly	dělat	k5eAaImAgFnP	dělat
i	i	k9	i
bobří	bobří	k2eAgFnPc1d1	bobří
klobásy	klobása	k1gFnPc1	klobása
<g/>
.	.	kIx.	.
</s>
<s>
Bobří	bobří	k2eAgFnPc1d1	bobří
pachové	pachový	k2eAgFnPc1d1	pachová
žlázy	žláza	k1gFnPc1	žláza
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgNnSc1d1	zvané
castoreum	castoreum	k1gNnSc1	castoreum
nebo	nebo	k8xC	nebo
bobří	bobří	k2eAgInSc1d1	bobří
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
užívány	užívat	k5eAaImNgInP	užívat
v	v	k7c6	v
renesanční	renesanční	k2eAgFnSc6d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgFnSc6d1	barokní
medicíně	medicína	k1gFnSc6	medicína
proti	proti	k7c3	proti
nachlazení	nachlazení	k1gNnSc3	nachlazení
a	a	k8xC	a
zánětům	zánět	k1gInPc3	zánět
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
výměšek	výměšek	k1gInSc1	výměšek
má	mít	k5eAaImIp3nS	mít
bohatý	bohatý	k2eAgInSc4d1	bohatý
obsah	obsah	k1gInSc4	obsah
kyseliny	kyselina	k1gFnSc2	kyselina
salicylové	salicylový	k2eAgFnSc2d1	salicylová
<g/>
.	.	kIx.	.
</s>
<s>
Bobři	bobr	k1gMnPc1	bobr
se	se	k3xPyFc4	se
chytali	chytat	k5eAaImAgMnP	chytat
většinou	většina	k1gFnSc7	většina
do	do	k7c2	do
pastí	past	k1gFnPc2	past
nebo	nebo	k8xC	nebo
norováním	norování	k1gNnSc7	norování
se	s	k7c7	s
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byli	být	k5eAaImAgMnP	být
bobři	bobr	k1gMnPc1	bobr
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Schwarzenbergové	Schwarzenbergový	k2eAgFnSc2d1	Schwarzenbergová
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
bobry	bobr	k1gMnPc4	bobr
chovat	chovat	k5eAaImF	chovat
na	na	k7c6	na
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
farmě	farma	k1gFnSc6	farma
v	v	k7c6	v
Červeném	červený	k2eAgInSc6d1	červený
dvoře	dvůr	k1gInSc6	dvůr
na	na	k7c6	na
Třeboňsku	Třeboňsko	k1gNnSc6	Třeboňsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
bobra	bobr	k1gMnSc2	bobr
uctívaly	uctívat	k5eAaImAgFnP	uctívat
jako	jako	k9	jako
posvátné	posvátný	k2eAgNnSc4d1	posvátné
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Škody	škoda	k1gFnSc2	škoda
působené	působený	k2eAgMnPc4d1	působený
bobry	bobr	k1gMnPc4	bobr
===	===	k?	===
</s>
</p>
<p>
<s>
Návrat	návrat	k1gInSc1	návrat
bobrů	bobr	k1gMnPc2	bobr
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
moravské	moravský	k2eAgFnSc2d1	Moravská
kulturní	kulturní	k2eAgFnSc2d1	kulturní
krajiny	krajina	k1gFnSc2	krajina
se	se	k3xPyFc4	se
neobešel	obešet	k5eNaImAgInS	obešet
bez	bez	k7c2	bez
těžkostí	těžkost	k1gFnPc2	těžkost
<g/>
.	.	kIx.	.
</s>
<s>
Bobři	bobr	k1gMnPc1	bobr
svou	svůj	k3xOyFgFnSc7	svůj
činností	činnost	k1gFnSc7	činnost
zejména	zejména	k9	zejména
narušují	narušovat	k5eAaImIp3nP	narušovat
protipovodňové	protipovodňový	k2eAgFnPc1d1	protipovodňová
nebo	nebo	k8xC	nebo
rybniční	rybniční	k2eAgFnPc1d1	rybniční
hráze	hráz	k1gFnPc1	hráz
<g/>
,	,	kIx,	,
ucpávají	ucpávat	k5eAaImIp3nP	ucpávat
koryta	koryto	k1gNnPc1	koryto
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
a	a	k8xC	a
likvidují	likvidovat	k5eAaBmIp3nP	likvidovat
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
plodiny	plodina	k1gFnPc1	plodina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
a	a	k8xC	a
medializované	medializovaný	k2eAgFnPc1d1	medializovaná
jsou	být	k5eAaImIp3nP	být
milionové	milionový	k2eAgFnPc1d1	milionová
škody	škoda	k1gFnPc1	škoda
v	v	k7c4	v
Lednicko	Lednicko	k1gNnSc4	Lednicko
-	-	kIx~	-
Valtickém	valtický	k2eAgInSc6d1	valtický
areálu	areál	k1gInSc6	areál
i	i	k9	i
na	na	k7c4	na
Dyji	Dyje	k1gFnSc4	Dyje
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Břeclavi	Břeclav	k1gFnSc2	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Oplocování	oplocování	k1gNnSc1	oplocování
nejcennějších	cenný	k2eAgInPc2d3	nejcennější
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
pozemků	pozemek	k1gInPc2	pozemek
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k9	jako
málo	málo	k6eAd1	málo
účinné	účinný	k2eAgNnSc1d1	účinné
(	(	kIx(	(
<g/>
plotem	plot	k1gInSc7	plot
nelze	lze	k6eNd1	lze
přehrazovat	přehrazovat	k5eAaImF	přehrazovat
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
)	)	kIx)	)
a	a	k8xC	a
individuální	individuální	k2eAgFnSc1d1	individuální
ochrana	ochrana	k1gFnSc1	ochrana
dřevin	dřevina	k1gFnPc2	dřevina
je	být	k5eAaImIp3nS	být
esteticky	esteticky	k6eAd1	esteticky
problematická	problematický	k2eAgFnSc1d1	problematická
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
nákladná	nákladný	k2eAgNnPc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
<s>
Hráze	hráz	k1gFnPc4	hráz
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
obkládat	obkládat	k5eAaImF	obkládat
kamennou	kamenný	k2eAgFnSc7d1	kamenná
rovnaninou	rovnanina	k1gFnSc7	rovnanina
nebo	nebo	k8xC	nebo
chránit	chránit	k5eAaImF	chránit
pevnými	pevný	k2eAgFnPc7d1	pevná
stěnami	stěna	k1gFnPc7	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnPc1d1	roční
škody	škoda	k1gFnPc1	škoda
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
stouply	stoupnout	k5eAaPmAgFnP	stoupnout
ze	z	k7c2	z
600	[number]	k4	600
000	[number]	k4	000
na	na	k7c4	na
pět	pět	k4xCc4	pět
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
citelnými	citelný	k2eAgFnPc7d1	citelná
škodami	škoda	k1gFnPc7	škoda
již	již	k6eAd1	již
žádají	žádat	k5eAaImIp3nP	žádat
majitelé	majitel	k1gMnPc1	majitel
a	a	k8xC	a
správci	správce	k1gMnPc1	správce
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
,	,	kIx,	,
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
odstřelu	odstřel	k1gInSc2	odstřel
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
účinné	účinný	k2eAgFnSc2d1	účinná
formy	forma	k1gFnSc2	forma
regulace	regulace	k1gFnSc2	regulace
těchto	tento	k3xDgInPc2	tento
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířících	šířící	k2eAgFnPc2d1	šířící
zákonem	zákon	k1gInSc7	zákon
chráněných	chráněný	k2eAgMnPc2d1	chráněný
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
zimy	zima	k1gFnSc2	zima
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
se	se	k3xPyFc4	se
bobr	bobr	k1gMnSc1	bobr
objevil	objevit	k5eAaPmAgMnS	objevit
také	také	k9	také
u	u	k7c2	u
Vltavy	Vltava	k1gFnSc2	Vltava
v	v	k7c6	v
Lenoře	Lenora	k1gFnSc6	Lenora
na	na	k7c6	na
Prachaticku	Prachaticko	k1gNnSc6	Prachaticko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
115	[number]	k4	115
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c4	o
poskytování	poskytování	k1gNnSc4	poskytování
náhrad	náhrada	k1gFnPc2	náhrada
škod	škoda	k1gFnPc2	škoda
způsobených	způsobený	k2eAgFnPc2d1	způsobená
vybranými	vybraná	k1gFnPc7	vybraná
zvláště	zvláště	k6eAd1	zvláště
chráněnými	chráněný	k2eAgMnPc7d1	chráněný
živočichy	živočich	k1gMnPc7	živočich
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
bobra	bobr	k1gMnSc4	bobr
evropského	evropský	k2eAgMnSc4d1	evropský
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
vybraných	vybraný	k2eAgInPc2d1	vybraný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uplatněna	uplatněn	k2eAgFnSc1d1	uplatněna
náhrada	náhrada	k1gFnSc1	náhrada
škody	škoda	k1gFnSc2	škoda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
poškození	poškození	k1gNnSc2	poškození
lesního	lesní	k2eAgNnSc2d1	lesní
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
trvalého	trvalý	k2eAgNnSc2d1	trvalé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
porostu	porůst	k5eAaPmIp1nS	porůst
nebo	nebo	k8xC	nebo
škodě	škoda	k1gFnSc3	škoda
na	na	k7c6	na
nesklizených	sklizený	k2eNgFnPc6d1	nesklizená
polních	polní	k2eAgFnPc6d1	polní
plodinách	plodina	k1gFnPc6	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Náhradu	náhrada	k1gFnSc4	náhrada
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
škod	škoda	k1gFnPc2	škoda
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
115	[number]	k4	115
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Náhradu	náhrada	k1gFnSc4	náhrada
(	(	kIx(	(
<g/>
újmu	újma	k1gFnSc4	újma
<g/>
)	)	kIx)	)
za	za	k7c4	za
ztížení	ztížení	k1gNnSc4	ztížení
zemědělského	zemědělský	k2eAgNnSc2d1	zemědělské
nebo	nebo	k8xC	nebo
lesnického	lesnický	k2eAgNnSc2d1	lesnické
hospodaření	hospodaření	k1gNnSc2	hospodaření
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
§	§	k?	§
58	[number]	k4	58
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
nelze	lze	k6eNd1	lze
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
škodu	škoda	k1gFnSc4	škoda
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
zvláště	zvláště	k6eAd1	zvláště
chráněným	chráněný	k2eAgMnSc7d1	chráněný
živočichem	živočich	k1gMnSc7	živočich
(	(	kIx(	(
<g/>
škoda	škoda	k6eAd1	škoda
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
aktivním	aktivní	k2eAgNnSc7d1	aktivní
působením	působení	k1gNnSc7	působení
živočicha	živočich	k1gMnSc2	živočich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náhradu	náhrada	k1gFnSc4	náhrada
újmy	újma	k1gFnSc2	újma
lze	lze	k6eAd1	lze
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vlastník	vlastník	k1gMnSc1	vlastník
či	či	k8xC	či
nájemce	nájemce	k1gMnSc1	nájemce
pozemku	pozemka	k1gFnSc4	pozemka
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c6	na
zemědělském	zemědělský	k2eAgInSc6d1	zemědělský
či	či	k8xC	či
lesnickém	lesnický	k2eAgInSc6d1	lesnický
hospodaření	hospodaření	k1gNnSc2	hospodaření
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
respektuje	respektovat	k5eAaImIp3nS	respektovat
ustanovení	ustanovení	k1gNnSc1	ustanovení
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
zákazy	zákaz	k1gInPc1	zákaz
v	v	k7c6	v
§	§	k?	§
50	[number]	k4	50
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
114	[number]	k4	114
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
či	či	k8xC	či
prováděcího	prováděcí	k2eAgInSc2d1	prováděcí
právního	právní	k2eAgInSc2d1	právní
předpisu	předpis	k1gInSc2	předpis
nebo	nebo	k8xC	nebo
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
vydaného	vydaný	k2eAgInSc2d1	vydaný
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
především	především	k9	především
lýkem	lýko	k1gNnSc7	lýko
a	a	k8xC	a
větvičkami	větvička	k1gFnPc7	větvička
vrby	vrba	k1gFnSc2	vrba
<g/>
,	,	kIx,	,
topolu	topol	k1gInSc2	topol
případně	případně	k6eAd1	případně
dalších	další	k2eAgFnPc2d1	další
dřevin	dřevina	k1gFnPc2	dřevina
(	(	kIx(	(
<g/>
jasan	jasan	k1gInSc1	jasan
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
<g/>
,	,	kIx,	,
jilm	jilm	k1gInSc1	jilm
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
a	a	k8xC	a
některými	některý	k3yIgFnPc7	některý
bylinami	bylina	k1gFnPc7	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Bobr	bobr	k1gMnSc1	bobr
většinou	většinou	k6eAd1	většinou
kácí	kácet	k5eAaImIp3nS	kácet
dřeviny	dřevina	k1gFnPc4	dřevina
asi	asi	k9	asi
50	[number]	k4	50
m	m	kA	m
od	od	k7c2	od
břehu	břeh	k1gInSc2	břeh
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Nepohrdne	pohrdnout	k5eNaPmIp3nS	pohrdnout
ani	ani	k8xC	ani
úrodou	úroda	k1gFnSc7	úroda
kukuřičného	kukuřičný	k2eAgNnSc2d1	kukuřičné
nebo	nebo	k8xC	nebo
řepného	řepný	k2eAgNnSc2d1	řepné
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc1	přednost
bylinnému	bylinný	k2eAgNnSc3d1	bylinné
patru	patro	k1gNnSc3	patro
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
také	také	k9	také
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
nahromadění	nahromadění	k1gNnSc4	nahromadění
potravy	potrava	k1gFnSc2	potrava
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdé	Tvrdé	k2eAgNnSc1d1	Tvrdé
dřevo	dřevo	k1gNnSc1	dřevo
dubu	dub	k1gInSc2	dub
či	či	k8xC	či
jasanu	jasan	k1gInSc2	jasan
používá	používat	k5eAaImIp3nS	používat
spíše	spíše	k9	spíše
jako	jako	k9	jako
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
na	na	k7c4	na
hráze	hráz	k1gFnPc4	hráz
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řek	řek	k1gMnSc1	řek
tyto	tento	k3xDgFnPc4	tento
dřeviny	dřevina	k1gFnPc4	dřevina
aktivně	aktivně	k6eAd1	aktivně
ničí	ničit	k5eAaImIp3nP	ničit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
přednostně	přednostně	k6eAd1	přednostně
konzumované	konzumovaný	k2eAgFnPc4d1	konzumovaná
vrby	vrba	k1gFnPc4	vrba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
105	[number]	k4	105
až	až	k9	až
107	[number]	k4	107
denní	denní	k2eAgFnSc2d1	denní
březosti	březost	k1gFnSc2	březost
se	se	k3xPyFc4	se
samici	samice	k1gFnSc3	samice
rodí	rodit	k5eAaImIp3nS	rodit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
nebo	nebo	k8xC	nebo
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ve	v	k7c6	v
dvougenerační	dvougenerační	k2eAgFnSc6d1	dvougenerační
mateřské	mateřský	k2eAgFnSc6d1	mateřská
kolonii	kolonie	k1gFnSc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
zakládají	zakládat	k5eAaImIp3nP	zakládat
novou	nový	k2eAgFnSc4d1	nová
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Bobři	bobr	k1gMnPc1	bobr
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgFnPc1d1	aktivní
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
bez	bez	k7c2	bez
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
kácejí	kácet	k5eAaImIp3nP	kácet
kmeny	kmen	k1gInPc1	kmen
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poradí	poradit	k5eAaPmIp3nS	poradit
si	se	k3xPyFc3	se
i	i	k9	i
s	s	k7c7	s
kmenem	kmen	k1gInSc7	kmen
70	[number]	k4	70
cm	cm	kA	cm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgFnSc1d1	noční
aktivita	aktivita	k1gFnSc1	aktivita
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
přímé	přímý	k2eAgNnSc4d1	přímé
pozorování	pozorování	k1gNnSc4	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
bobrů	bobr	k1gMnPc2	bobr
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
lokalitě	lokalita	k1gFnSc6	lokalita
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
podle	podle	k7c2	podle
ohlodaných	ohlodaný	k2eAgInPc2d1	ohlodaný
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
stop	stopa	k1gFnPc2	stopa
<g/>
,	,	kIx,	,
kupovitých	kupovitý	k2eAgFnPc2d1	kupovitá
staveb	stavba	k1gFnPc2	stavba
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
(	(	kIx(	(
<g/>
bobřích	bobří	k2eAgInPc2d1	bobří
hradů	hrad	k1gInPc2	hrad
<g/>
)	)	kIx)	)
a	a	k8xC	a
podle	podle	k7c2	podle
hrází	hráz	k1gFnPc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
místních	místní	k2eAgFnPc2d1	místní
podmínek	podmínka	k1gFnPc2	podmínka
bobři	bobr	k1gMnPc1	bobr
nemusejí	muset	k5eNaImIp3nP	muset
vždy	vždy	k6eAd1	vždy
hrad	hrad	k1gInSc4	hrad
postavit	postavit	k5eAaPmF	postavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
obydlí	obydlí	k1gNnSc2	obydlí
==	==	k?	==
</s>
</p>
<p>
<s>
Bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
neužívá	užívat	k5eNaImIp3nS	užívat
stromů	strom	k1gInPc2	strom
jen	jen	k6eAd1	jen
k	k	k7c3	k
potravě	potrava	k1gFnSc3	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Nemůže	moct	k5eNaImIp3nS	moct
<g/>
-li	i	k?	-li
budovat	budovat	k5eAaImF	budovat
noru	nora	k1gFnSc4	nora
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
,	,	kIx,	,
pokácí	pokácet	k5eAaPmIp3nS	pokácet
strom	strom	k1gInSc1	strom
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
kmen	kmen	k1gInSc1	kmen
použije	použít	k5eAaPmIp3nS	použít
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
svého	svůj	k3xOyFgInSc2	svůj
hradu	hrad	k1gInSc2	hrad
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
m	m	kA	m
vysokého	vysoký	k2eAgNnSc2d1	vysoké
a	a	k8xC	a
3	[number]	k4	3
m	m	kA	m
širokého	široký	k2eAgInSc2d1	široký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zaplavení	zaplavení	k1gNnSc4	zaplavení
hradu	hrad	k1gInSc2	hrad
vybuduje	vybudovat	k5eAaPmIp3nS	vybudovat
na	na	k7c6	na
vodním	vodní	k2eAgInSc6d1	vodní
toku	tok	k1gInSc6	tok
hráz	hráz	k1gFnSc4	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
hradu	hrad	k1gInSc2	hrad
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
hned	hned	k6eAd1	hned
po	po	k7c6	po
setmění	setmění	k1gNnSc6	setmění
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
až	až	k9	až
pozdě	pozdě	k6eAd1	pozdě
k	k	k7c3	k
ránu	ráno	k1gNnSc3	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stavební	stavební	k2eAgFnSc1d1	stavební
činnost	činnost	k1gFnSc1	činnost
kladně	kladně	k6eAd1	kladně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
druhovou	druhový	k2eAgFnSc4d1	druhová
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
dané	daný	k2eAgFnSc2d1	daná
lokality	lokalita	k1gFnSc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pokácení	pokácení	k1gNnSc2	pokácení
některých	některý	k3yIgInPc2	některý
stromů	strom	k1gInPc2	strom
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
prosvětlení	prosvětlení	k1gNnSc3	prosvětlení
území	území	k1gNnSc2	území
a	a	k8xC	a
přilákání	přilákání	k1gNnSc2	přilákání
dalších	další	k2eAgInPc2d1	další
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
–	–	k?	–
především	především	k9	především
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledávaným	vyhledávaný	k2eAgNnSc7d1	vyhledávané
prostředím	prostředí	k1gNnSc7	prostředí
jsou	být	k5eAaImIp3nP	být
místa	místo	k1gNnPc4	místo
s	s	k7c7	s
mírně	mírně	k6eAd1	mírně
tekoucí	tekoucí	k2eAgFnSc7d1	tekoucí
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
břehy	břeh	k1gInPc1	břeh
porostlé	porostlý	k2eAgInPc1d1	porostlý
měkkými	měkký	k2eAgFnPc7d1	měkká
dřevinami	dřevina	k1gFnPc7	dřevina
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
vrba	vrba	k1gFnSc1	vrba
<g/>
,	,	kIx,	,
olše	olše	k1gFnSc1	olše
<g/>
,	,	kIx,	,
osika	osika	k1gFnSc1	osika
<g/>
,	,	kIx,	,
bříza	bříza	k1gFnSc1	bříza
nebo	nebo	k8xC	nebo
topol	topol	k1gInSc1	topol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bobr	bobr	k1gMnSc1	bobr
kanadský	kanadský	k2eAgMnSc1d1	kanadský
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Castor	Castor	k1gInSc1	Castor
fiber	fiber	k1gInSc1	fiber
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Na	na	k7c6	na
Jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
chtějí	chtít	k5eAaImIp3nP	chtít
omezit	omezit	k5eAaPmF	omezit
výskyt	výskyt	k1gInSc4	výskyt
přemnožených	přemnožený	k2eAgMnPc2d1	přemnožený
bobrů	bobr	k1gMnPc2	bobr
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
KAČÍRKOVÁ	Kačírková	k1gFnSc1	Kačírková
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
<g/>
.	.	kIx.	.
video	video	k1gNnSc1	video
Bobr	bobr	k1gMnSc1	bobr
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Castor	Castor	k1gMnSc1	Castor
fiber	fiber	k1gMnSc1	fiber
<g/>
)	)	kIx)	)
Přerovsko	Přerovsko	k1gNnSc1	Přerovsko
<g/>
,	,	kIx,	,
14.5	[number]	k4	14.5
<g/>
.2019	.2019	k4	.2019
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kačírková	Kačírková	k1gFnSc1	Kačírková
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bobr	bobr	k1gMnSc1	bobr
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Agentura	agentura	k1gFnSc1	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
ČR	ČR	kA	ČR
<g/>
:	:	kIx,	:
http://www.biomonitoring.cz/druhy.php?druhID=45	[url]	k4	http://www.biomonitoring.cz/druhy.php?druhID=45
ochrana	ochrana	k1gFnSc1	ochrana
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
ekologie	ekologie	k1gFnSc1	ekologie
<g/>
,	,	kIx,	,
monitoring	monitoring	k1gInSc1	monitoring
<g/>
,	,	kIx,	,
mapa	mapa	k1gFnSc1	mapa
rozšíření	rozšíření	k1gNnSc2	rozšíření
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
IUCN	IUCN	kA	IUCN
<g/>
:	:	kIx,	:
https://web.archive.org/web/20110921042057/http://www.iucnredlist.org/apps/redlist/details/4007/0	[url]	k4	https://web.archive.org/web/20110921042057/http://www.iucnredlist.org/apps/redlist/details/4007/0
ochrana	ochrana	k1gFnSc1	ochrana
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
<g/>
,	,	kIx,	,
ekologie	ekologie	k1gFnSc2	ekologie
<g/>
:	:	kIx,	:
IUCN	IUCN	kA	IUCN
Red	Red	k1gFnSc1	Red
List	list	k1gInSc1	list
of	of	k?	of
Threatened	Threatened	k1gInSc4	Threatened
Species	species	k1gFnSc2	species
<g/>
.	.	kIx.	.
</s>
<s>
Version	Version	k1gInSc1	Version
2011.1	[number]	k4	2011.1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právní	právní	k2eAgFnPc1d1	právní
normy	norma	k1gFnPc1	norma
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
platné	platný	k2eAgFnSc2d1	platná
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
:	:	kIx,	:
https://web.archive.org/web/20110617114213/http://www.cizp.cz/Pravni-normy/Ochrana-prirody	[url]	k1gInPc1	https://web.archive.org/web/20110617114213/http://www.cizp.cz/Pravni-normy/Ochrana-prirody
</s>
</p>
