<s>
Vysokopecký	Vysokopecký	k2eAgInSc1d1
tunel	tunel	k1gInSc1
</s>
<s>
Vysokopecký	Vysokopecký	k2eAgInSc1d1
tunel	tunel	k1gInSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Provozní	provozní	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
68	#num#	k4
m	m	kA
Výstavba	výstavba	k1gFnSc1
Lokalizace	lokalizace	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
23,81	23,81	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
′	′	k?
<g/>
51,52	51,52	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Vysokopecký	Vysokopecký	k2eAgInSc1d1
tunel	tunel	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vysokopecký	Vysokopecký	k2eAgInSc1d1
tunel	tunel	k1gInSc1
je	být	k5eAaImIp3nS
železniční	železniční	k2eAgInSc1d1
tunel	tunel	k1gInSc1
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Nejdek	Nejdek	k1gInSc1
na	na	k7c6
úseku	úsek	k1gInSc6
železniční	železniční	k2eAgFnSc2d1
trati	trať	k1gFnSc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
–	–	k?
Johanngeorgenstadt	Johanngeorgenstadt	k1gInSc1
mezi	mezi	k7c7
zastávkami	zastávka	k1gFnPc7
Nejdek	Nejdek	k1gInSc4
zastávka	zastávka	k1gFnSc1
a	a	k8xC
Vysoká	vysoký	k2eAgFnSc1d1
Pec	Pec	k1gFnSc1
v	v	k7c6
km	km	kA
22,849	22,849	k4
<g/>
–	–	k?
<g/>
22,917	22,917	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Koncese	koncese	k1gFnSc1
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
místní	místní	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
byla	být	k5eAaImAgFnS
udělena	udělen	k2eAgFnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1895	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc1
výstavba	výstavba	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
první	první	k4xOgInSc1
byl	být	k5eAaImAgInS
budován	budovat	k5eAaImNgInS
úsek	úsek	k1gInSc1
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
dolní	dolní	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
–	–	k?
Stará	starat	k5eAaImIp3nS
Role	role	k1gFnSc1
–	–	k?
Nová	nový	k2eAgFnSc1d1
Role	role	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
navazoval	navazovat	k5eAaImAgInS
na	na	k7c4
místní	místní	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1881	#num#	k4
z	z	k7c2
Chodova	Chodov	k1gInSc2
přes	přes	k7c4
Novou	nový	k2eAgFnSc4d1
roli	role	k1gFnSc4
do	do	k7c2
Nejdku	Nejdek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
něj	on	k3xPp3gMnSc4
navazovalo	navazovat	k5eAaImAgNnS
prodloužení	prodloužení	k1gNnSc1
z	z	k7c2
Nejdku	Nejdek	k1gInSc2
do	do	k7c2
Horní	horní	k2eAgFnSc2d1
Blatné	blatný	k2eAgFnSc2d1
(	(	kIx(
<g/>
dokončen	dokončit	k5eAaPmNgInS
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1898	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
Horní	horní	k2eAgFnSc2d1
Blatné	blatný	k2eAgFnSc2d1
do	do	k7c2
Johanngeorgenstadtu	Johanngeorgenstadt	k1gInSc2
(	(	kIx(
<g/>
dokončen	dokončen	k2eAgMnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1899	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
města	město	k1gNnSc2
ležícího	ležící	k2eAgMnSc4d1
již	již	k9
v	v	k7c4
Sasku	Saska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provoz	provoz	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
trati	trať	k1gFnSc6
oficiálně	oficiálně	k6eAd1
zahájen	zahájit	k5eAaPmNgInS
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1899	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
trati	trať	k1gFnSc6
dlouhé	dlouhý	k2eAgFnSc6d1
47,2	47,2	k4
km	km	kA
je	být	k5eAaImIp3nS
sedmnáct	sedmnáct	k4xCc4
železničních	železniční	k2eAgFnPc2d1
stanic	stanice	k1gFnPc2
a	a	k8xC
zastávek	zastávka	k1gFnPc2
a	a	k8xC
čtyři	čtyři	k4xCgInPc1
tunely	tunel	k1gInPc1
<g/>
:	:	kIx,
Karlovarský	karlovarský	k2eAgInSc1d1
<g/>
,	,	kIx,
Nejdecký	nejdecký	k2eAgInSc1d1
<g/>
,	,	kIx,
Vysokopecký	Vysokopecký	k2eAgInSc1d1
a	a	k8xC
Novohamerský	Novohamerský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
mosty	most	k1gInPc4
vyniká	vynikat	k5eAaImIp3nS
Novohamerský	Novohamerský	k2eAgInSc1d1
a	a	k8xC
Perninský	Perninský	k2eAgInSc1d1
viadukt	viadukt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
rekonstruován	rekonstruovat	k5eAaBmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byla	být	k5eAaImAgFnS
trať	trať	k1gFnSc1
rekonstruována	rekonstruován	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jednokolejný	jednokolejný	k2eAgInSc1d1
tunel	tunel	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
pro	pro	k7c4
železniční	železniční	k2eAgFnSc4d1
trať	trať	k1gFnSc4
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
–	–	k?
Johanngeorgenstadt	Johanngeorgenstadt	k1gInSc1
mezi	mezi	k7c7
zastávkami	zastávka	k1gFnPc7
Nejdek	Nejdek	k1gInSc4
zastávka	zastávka	k1gFnSc1
a	a	k8xC
Vysoká	vysoký	k2eAgFnSc1d1
Pec	Pec	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1898	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
druhé	druhý	k4xOgFnSc6
etapě	etapa	k1gFnSc6
výstavby	výstavba	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
byl	být	k5eAaImAgInS
dán	dát	k5eAaPmNgInS
do	do	k7c2
provozu	provoz	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
postaven	postavit	k5eAaPmNgMnS
přes	přes	k7c4
výběžek	výběžek	k1gInSc4
hory	hora	k1gFnSc2
Jedlovec	Jedlovec	k1gMnSc1
(	(	kIx(
<g/>
853	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
s	s	k7c7
výjezdem	výjezd	k1gInSc7
ve	v	k7c6
směrovém	směrový	k2eAgInSc6d1
levotočivém	levotočivý	k2eAgInSc6d1
oblouku	oblouk	k1gInSc6
do	do	k7c2
zastávky	zastávka	k1gFnSc2
Vysoká	vysoký	k2eAgFnSc1d1
Pec	Pec	k1gFnSc1
vzdálené	vzdálený	k2eAgNnSc4d1
od	od	k7c2
severního	severní	k2eAgInSc2d1
portálu	portál	k1gInSc2
asi	asi	k9
150	#num#	k4
m.	m.	k?
</s>
<s>
Tunel	tunel	k1gInSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
645	#num#	k4
m	m	kA
a	a	k8xC
je	být	k5eAaImIp3nS
dlouhý	dlouhý	k2eAgInSc1d1
68	#num#	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Vysokopecký	Vysokopecký	k2eAgInSc4d1
(	(	kIx(
<g/>
tunel	tunel	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.atlasdrah.net	www.atlasdrah.net	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MICHÁLEK	Michálek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
110	#num#	k4
let	léto	k1gNnPc2
železniční	železniční	k2eAgFnSc2d1
trati	trať	k1gFnSc2
"	"	kIx"
<g/>
Krušnohorský	krušnohorský	k2eAgInSc1d1
Semmering	Semmering	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdecké	nejdecký	k2eAgInPc1d1
listy	list	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
město	město	k1gNnSc1
Nejdek	Nejdek	k1gInSc4
<g/>
,	,	kIx,
září	zářit	k5eAaImIp3nS
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
XI	XI	kA
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
města	město	k1gNnSc2
Nejdek	Nejdek	k1gInSc1
-	-	kIx~
Krušnohorský	krušnohorský	k2eAgInSc1d1
semmering	semmering	k1gInSc1
<g/>
.	.	kIx.
turista	turista	k1gMnSc1
<g/>
.	.	kIx.
<g/>
nejdek	nejdek	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
V	v	k7c6
Krušných	krušný	k2eAgFnPc6d1
horách	hora	k1gFnPc6
začne	začít	k5eAaPmIp3nS
odložená	odložený	k2eAgFnSc1d1
oprava	oprava	k1gFnSc1
tratě	trať	k1gFnSc2
z	z	k7c2
Nejdku	Nejdek	k1gInSc2
do	do	k7c2
Potůčků	potůček	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-05-12	2008-05-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SŮRA	SŮRA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půlmiliardová	půlmiliardový	k2eAgFnSc1d1
oprava	oprava	k1gFnSc1
krušnohorské	krušnohorský	k2eAgFnSc2d1
trati	trať	k1gFnSc2
začíná	začínat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdržel	zdržet	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
spor	spor	k1gInSc1
o	o	k7c4
trubku	trubka	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-04-25	2017-04-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOZOHORSKÝ	KOZOHORSKÝ	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železnici	železnice	k1gFnSc4
z	z	k7c2
Varů	Vary	k1gInPc2
do	do	k7c2
Potůčku	potůček	k1gInSc2
čeká	čekat	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
oprava	oprava	k1gFnSc1
<g/>
,	,	kIx,
vlaky	vlak	k1gInPc1
pak	pak	k6eAd1
pojedou	pojet	k5eAaPmIp3nP,k5eAaImIp3nP
rychleji	rychle	k6eAd2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-02	2017-05-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vysokopecký	Vysokopecký	k2eAgInSc4d1
tunel	tunel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vysokopecký	Vysokopecký	k2eAgInSc4d1
tunel	tunel	k1gInSc4
<g/>
.	.	kIx.
spravnym	spravnym	k1gInSc4
<g/>
.	.	kIx.
<g/>
smerem	smerem	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Železniční	železniční	k2eAgFnSc4d1
trať	trať	k1gFnSc4
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
–	–	k?
Johanngeorgenstadt	Johanngeorgenstadt	k1gInSc1
</s>
<s>
Seznam	seznam	k1gInSc1
železničních	železniční	k2eAgInPc2d1
tunelů	tunel	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Trať	trať	k1gFnSc1
142	#num#	k4
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
–	–	k?
Potůčky	Potůček	k1gMnPc7
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
–	–	k?
Johanngeorgenstadt	Johanngeorgenstadt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohledzvlaku	Pohledzvlak	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Železnice	železnice	k1gFnSc1
</s>
