<p>
<s>
Herbertové	Herbert	k1gMnPc1	Herbert
jsou	být	k5eAaImIp3nP	být
starý	starý	k2eAgInSc4d1	starý
anglický	anglický	k2eAgInSc4d1	anglický
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
rod	rod	k1gInSc4	rod
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
příbuzenskému	příbuzenský	k2eAgInSc3d1	příbuzenský
vztahu	vztah	k1gInSc3	vztah
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dostali	dostat	k5eAaPmAgMnP	dostat
mezi	mezi	k7c4	mezi
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
šlechtu	šlechta	k1gFnSc4	šlechta
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
řada	řada	k1gFnSc1	řada
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
zastávala	zastávat	k5eAaImAgFnS	zastávat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
funkce	funkce	k1gFnPc4	funkce
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
,	,	kIx,	,
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
významných	významný	k2eAgMnPc2d1	významný
státníků	státník	k1gMnPc2	státník
a	a	k8xC	a
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
George	George	k1gInSc1	George
Herbert	Herbert	k1gInSc1	Herbert
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
jako	jako	k8xS	jako
nadšený	nadšený	k2eAgMnSc1d1	nadšený
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
objevitel	objevitel	k1gMnSc1	objevitel
Tutanchamonovy	Tutanchamonův	k2eAgFnSc2d1	Tutanchamonova
hrobky	hrobka	k1gFnSc2	hrobka
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
rodová	rodový	k2eAgNnPc1d1	rodové
sídla	sídlo	k1gNnPc1	sídlo
Wilton	Wilton	k1gInSc1	Wilton
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
Wiltshire	Wiltshir	k1gMnSc5	Wiltshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Highclere	Highcler	k1gInSc5	Highcler
Castle	Castle	k1gFnPc1	Castle
(	(	kIx(	(
<g/>
Berkshire	Berkshir	k1gInSc5	Berkshir
<g/>
)	)	kIx)	)
a	a	k8xC	a
Powis	Powis	k1gFnSc1	Powis
Castle	Castle	k1gFnSc1	Castle
(	(	kIx(	(
<g/>
Wales	Wales	k1gInSc1	Wales
<g/>
)	)	kIx)	)
s	s	k7c7	s
bohatými	bohatý	k2eAgFnPc7d1	bohatá
uměleckými	umělecký	k2eAgFnPc7d1	umělecká
sbírkami	sbírka	k1gFnPc7	sbírka
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
zpřístupněna	zpřístupněn	k2eAgNnPc1d1	zpřístupněno
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
rod	rod	k1gInSc1	rod
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
William	William	k1gInSc4	William
Herbert	Herbert	k1gInSc1	Herbert
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
a	a	k8xC	a
John	John	k1gMnSc1	John
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
rodu	rod	k1gInSc2	rod
==	==	k?	==
</s>
</p>
<p>
<s>
Herbertové	Herbert	k1gMnPc1	Herbert
se	se	k3xPyFc4	se
připomínají	připomínat	k5eAaImIp3nP	připomínat
již	již	k6eAd1	již
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
vzestupu	vzestup	k1gInSc2	vzestup
rodu	rod	k1gInSc2	rod
byl	být	k5eAaImAgMnS	být
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc1	William
(	(	kIx(	(
<g/>
†	†	k?	†
1445	[number]	k4	1445
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Jindřicha	Jindřich	k1gMnSc2	Jindřich
V.	V.	kA	V.
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1426	[number]	k4	1426
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
William	William	k1gInSc4	William
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
založili	založit	k5eAaPmAgMnP	založit
dvě	dva	k4xCgFnPc4	dva
rodové	rodový	k2eAgFnPc4d1	rodová
větve	větev	k1gFnPc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Williamova	Williamův	k2eAgNnSc2d1	Williamovo
potomstva	potomstvo	k1gNnSc2	potomstvo
pochází	pocházet	k5eAaImIp3nS	pocházet
hrabata	hrabě	k1gNnPc4	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
i	i	k9	i
další	další	k2eAgFnSc2d1	další
rodové	rodový	k2eAgFnSc2d1	rodová
linie	linie	k1gFnSc2	linie
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
a	a	k8xC	a
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
pětkrát	pětkrát	k6eAd1	pětkrát
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
baronů	baron	k1gMnPc2	baron
z	z	k7c2	z
Chirbury	Chirbura	k1gFnSc2	Chirbura
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
baronát	baronát	k1gInSc1	baronát
Chirbury	Chirbura	k1gFnSc2	Chirbura
součástí	součást	k1gFnSc7	součást
titulatury	titulatura	k1gFnSc2	titulatura
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hrabata	hrabě	k1gNnPc4	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
===	===	k?	===
</s>
</p>
<p>
<s>
Titul	titul	k1gInSc1	titul
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gInSc2	Pembrok
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1551	[number]	k4	1551
William	William	k1gInSc1	William
Herbert	Herbert	k1gInSc4	Herbert
(	(	kIx(	(
<g/>
1501	[number]	k4	1501
<g/>
–	–	k?	–
<g/>
1570	[number]	k4	1570
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
švagrem	švagr	k1gMnSc7	švagr
Jindřicha	Jindřich	k1gMnSc4	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jindřichových	Jindřichův	k2eAgMnPc2d1	Jindřichův
potomků	potomek	k1gMnPc2	potomek
Eduarda	Eduard	k1gMnSc2	Eduard
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Tudorovny	Tudorovna	k1gFnSc2	Tudorovna
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
osobnostem	osobnost	k1gFnPc3	osobnost
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
rodové	rodový	k2eAgInPc4d1	rodový
statky	statek	k1gInPc4	statek
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
a	a	k8xC	a
díky	díky	k7c3	díky
přízni	přízeň	k1gFnSc3	přízeň
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
získal	získat	k5eAaPmAgMnS	získat
majetky	majetek	k1gInPc4	majetek
zrušených	zrušený	k2eAgInPc2d1	zrušený
klášterů	klášter	k1gInPc2	klášter
ve	v	k7c6	v
Wiltshire	Wiltshir	k1gInSc5	Wiltshir
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
sídlem	sídlo	k1gNnSc7	sídlo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zámek	zámek	k1gInSc4	zámek
Wilton	Wilton	k1gInSc1	Wilton
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
Wiltshire	Wiltshir	k1gMnSc5	Wiltshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
následující	následující	k2eAgFnPc1d1	následující
generace	generace	k1gFnPc1	generace
Herbertů	Herbert	k1gMnPc2	Herbert
soustředily	soustředit	k5eAaPmAgFnP	soustředit
bohaté	bohatý	k2eAgFnPc1d1	bohatá
umělecké	umělecký	k2eAgFnPc1d1	umělecká
sbírky	sbírka	k1gFnPc1	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Philip	Philip	k1gMnSc1	Philip
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
–	–	k?	–
<g/>
1650	[number]	k4	1650
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
3	[number]	k4	3
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Pembroke	Pembroke	k1gNnSc2	Pembroke
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
oblíbencem	oblíbenec	k1gMnSc7	oblíbenec
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1605	[number]	k4	1605
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Montgomery	Montgomera	k1gFnSc2	Montgomera
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bezdětném	bezdětný	k2eAgMnSc6d1	bezdětný
bratrovi	bratr	k1gMnSc6	bratr
zdědil	zdědit	k5eAaPmAgInS	zdědit
i	i	k9	i
titul	titul	k1gInSc1	titul
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
(	(	kIx(	(
<g/>
1630	[number]	k4	1630
<g/>
)	)	kIx)	)
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
hraběcí	hraběcí	k2eAgInPc1d1	hraběcí
tituly	titul	k1gInPc1	titul
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
sloučeny	sloučen	k2eAgInPc1d1	sloučen
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
(	(	kIx(	(
<g/>
1656	[number]	k4	1656
<g/>
–	–	k?	–
<g/>
1733	[number]	k4	1733
<g/>
)	)	kIx)	)
zastával	zastávat	k5eAaImAgMnS	zastávat
vysoké	vysoký	k2eAgInPc4d1	vysoký
státní	státní	k2eAgInPc4d1	státní
úřady	úřad	k1gInPc4	úřad
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Margaret	Margareta	k1gFnPc2	Margareta
Sawyer	Sawyra	k1gFnPc2	Sawyra
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
korunního	korunní	k2eAgMnSc4d1	korunní
právního	právní	k2eAgMnSc4d1	právní
zástupce	zástupce	k1gMnSc4	zástupce
Roberta	Robert	k1gMnSc4	Robert
Sawyera	Sawyer	k1gMnSc4	Sawyer
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
zámek	zámek	k1gInSc1	zámek
Highclere	Highcler	k1gInSc5	Highcler
Castle	Castle	k1gFnPc1	Castle
(	(	kIx(	(
<g/>
Berkshire	Berkshir	k1gMnSc5	Berkshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zdědil	zdědit	k5eAaPmAgInS	zdědit
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mladších	mladý	k2eAgMnPc2d2	mladší
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
William	William	k1gInSc1	William
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1696	[number]	k4	1696
<g/>
–	–	k?	–
<g/>
1757	[number]	k4	1757
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
pozdější	pozdní	k2eAgFnSc2d2	pozdější
linie	linie	k1gFnSc2	linie
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
linii	linie	k1gFnSc6	linie
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
zastávali	zastávat	k5eAaImAgMnP	zastávat
Herbertové	Herbert	k1gMnPc1	Herbert
během	během	k7c2	během
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvěstě	dvěsta	k1gFnSc3	dvěsta
let	léto	k1gNnPc2	léto
úřad	úřad	k1gInSc1	úřad
lorda	lord	k1gMnSc2	lord
<g/>
–	–	k?	–
<g/>
místodržitele	místodržitel	k1gMnSc2	místodržitel
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Wiltshire	Wiltshir	k1gInSc5	Wiltshir
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gInSc2	Pembrok
získalo	získat	k5eAaPmAgNnS	získat
Podvazkový	podvazkový	k2eAgInSc4d1	podvazkový
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hrabata	hrabě	k1gNnPc1	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
===	===	k?	===
</s>
</p>
<p>
<s>
Carnarvon	Carnarvon	k1gInSc1	Carnarvon
(	(	kIx(	(
<g/>
též	též	k9	též
Caernarfon	Caernarfon	k1gInSc1	Caernarfon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hrabství	hrabství	k1gNnSc1	hrabství
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
a	a	k8xC	a
hraběcí	hraběcí	k2eAgInSc1d1	hraběcí
titul	titul	k1gInSc1	titul
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
získal	získat	k5eAaPmAgMnS	získat
poprvé	poprvé	k6eAd1	poprvé
Robert	Robert	k1gMnSc1	Robert
Dormer	Dormer	k1gMnSc1	Dormer
(	(	kIx(	(
<g/>
1610	[number]	k4	1610
<g/>
–	–	k?	–
<g/>
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
anglické	anglický	k2eAgFnSc2d1	anglická
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Anna	Anna	k1gFnSc1	Anna
Sophia	Sophia	k1gFnSc1	Sophia
Herbert	Herbert	k1gInSc1	Herbert
(	(	kIx(	(
<g/>
1610	[number]	k4	1610
<g/>
–	–	k?	–
<g/>
1695	[number]	k4	1695
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
titul	titul	k1gInSc1	titul
hrabat	hrabat	k5eAaImF	hrabat
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
udělen	udělen	k2eAgInSc1d1	udělen
rodu	rod	k1gInSc3	rod
Herbertů	Herbert	k1gInPc2	Herbert
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
linie	linie	k1gFnSc2	linie
vynikl	vyniknout	k5eAaPmAgMnS	vyniknout
státník	státník	k1gMnSc1	státník
Henry	Henry	k1gMnSc1	Henry
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ministrem	ministr	k1gMnSc7	ministr
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgInS	dokončit
novogotickou	novogotický	k2eAgFnSc4d1	novogotická
přestavbu	přestavba	k1gFnSc4	přestavba
rodového	rodový	k2eAgNnSc2d1	rodové
sídla	sídlo	k1gNnSc2	sídlo
Highclere	Highcler	k1gInSc5	Highcler
Castle	Castle	k1gFnPc1	Castle
v	v	k7c6	v
Berkshire	Berkshir	k1gInSc5	Berkshir
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
jsou	být	k5eAaImIp3nP	být
soustředěny	soustředěn	k2eAgFnPc1d1	soustředěna
sbírky	sbírka	k1gFnPc1	sbírka
dokládající	dokládající	k2eAgInPc4d1	dokládající
archeologické	archeologický	k2eAgInPc4d1	archeologický
zájmy	zájem	k1gInPc4	zájem
jeho	jeho	k3xOp3gFnSc2	jeho
syna	syn	k1gMnSc2	syn
5	[number]	k4	5
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
financoval	financovat	k5eAaBmAgInS	financovat
výzkumy	výzkum	k1gInPc4	výzkum
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
objevil	objevit	k5eAaPmAgInS	objevit
Tutanchamonovu	Tutanchamonův	k2eAgFnSc4d1	Tutanchamonova
hrobku	hrobka	k1gFnSc4	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Highclere	Highcler	k1gInSc5	Highcler
Castle	Castle	k1gFnSc2	Castle
byl	být	k5eAaImAgInS	být
živým	živý	k2eAgInSc7d1	živý
společenským	společenský	k2eAgInSc7d1	společenský
centrem	centr	k1gInSc7	centr
i	i	k9	i
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Henry	Henry	k1gMnSc1	Henry
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
štolbou	štolba	k1gMnSc7	štolba
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
představitelem	představitel	k1gMnSc7	představitel
této	tento	k3xDgFnSc2	tento
linie	linie	k1gFnSc2	linie
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
George	George	k1gFnPc2	George
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hrabata	hrabě	k1gNnPc1	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
===	===	k?	===
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Powis	Powis	k1gFnSc2	Powis
Castle	Castle	k1gFnSc2	Castle
je	být	k5eAaImIp3nS	být
starobylým	starobylý	k2eAgNnSc7d1	starobylé
šlechtickým	šlechtický	k2eAgNnSc7d1	šlechtické
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
a	a	k8xC	a
baronský	baronský	k2eAgInSc4d1	baronský
titul	titul	k1gInSc4	titul
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
William	William	k1gInSc1	William
Herbert	Herbert	k1gInSc4	Herbert
(	(	kIx(	(
<g/>
1573	[number]	k4	1573
<g/>
–	–	k?	–
<g/>
1656	[number]	k4	1656
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bratranec	bratranec	k1gMnSc1	bratranec
3	[number]	k4	3
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
William	William	k1gInSc1	William
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1626	[number]	k4	1626
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
přední	přední	k2eAgMnSc1d1	přední
činitel	činitel	k1gMnSc1	činitel
katolické	katolický	k2eAgFnSc2d1	katolická
politiky	politika	k1gFnSc2	politika
Stuartovců	Stuartovec	k1gMnPc2	Stuartovec
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
hraběte	hrabě	k1gMnSc4	hrabě
(	(	kIx(	(
<g/>
1674	[number]	k4	1674
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c6	na
markýze	markýza	k1gFnSc6	markýza
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
(	(	kIx(	(
<g/>
1687	[number]	k4	1687
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Markýzát	Markýzát	k1gInSc1	Markýzát
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
generaci	generace	k1gFnSc6	generace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1748	[number]	k4	1748
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
linii	linie	k1gFnSc6	linie
baronů	baron	k1gMnPc2	baron
Herbertů	Herbert	k1gMnPc2	Herbert
z	z	k7c2	z
Cherbury	Cherbura	k1gFnSc2	Cherbura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1748	[number]	k4	1748
se	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
stal	stát	k5eAaPmAgMnS	stát
generál	generál	k1gMnSc1	generál
Henry	Henry	k1gMnSc1	Henry
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1703	[number]	k4	1703
<g/>
–	–	k?	–
<g/>
1772	[number]	k4	1772
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
George	George	k1gFnPc2	George
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
(	(	kIx(	(
<g/>
1755	[number]	k4	1755
<g/>
–	–	k?	–
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soustředil	soustředit	k5eAaPmAgInS	soustředit
na	na	k7c4	na
Powis	Powis	k1gFnSc4	Powis
Castle	Castle	k1gFnSc2	Castle
bohaté	bohatý	k2eAgFnSc2d1	bohatá
umělecké	umělecký	k2eAgFnSc2d1	umělecká
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bez	bez	k7c2	bez
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
získal	získat	k5eAaPmAgMnS	získat
jeho	jeho	k3xOp3gMnSc1	jeho
švagr	švagr	k1gMnSc1	švagr
Edward	Edward	k1gMnSc1	Edward
Clive	Cliev	k1gFnSc2	Cliev
(	(	kIx(	(
<g/>
1754	[number]	k4	1754
<g/>
–	–	k?	–
<g/>
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
Roberta	Robert	k1gMnSc2	Robert
Clive	Cliev	k1gFnSc2	Cliev
<g/>
.	.	kIx.	.
</s>
<s>
Clive	Clivat	k5eAaPmIp3nS	Clivat
zároveň	zároveň	k6eAd1	zároveň
přijal	přijmout	k5eAaPmAgMnS	přijmout
příjmení	příjmení	k1gNnSc4	příjmení
Herbert	Herbert	k1gMnSc1	Herbert
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
koupil	koupit	k5eAaPmAgMnS	koupit
zříceniny	zřícenina	k1gFnPc4	zřícenina
hradu	hrad	k1gInSc2	hrad
Ludlow	Ludlow	k1gFnSc2	Ludlow
Castle	Castle	k1gFnSc2	Castle
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
sídlili	sídlit	k5eAaImAgMnP	sídlit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Walcott	Walcott	k2eAgInSc4d1	Walcott
Hall	Hall	k1gInSc4	Hall
(	(	kIx(	(
<g/>
Shropshire	Shropshir	k1gMnSc5	Shropshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
Robert	Robert	k1gMnSc1	Robert
Clive	Cliev	k1gFnSc2	Cliev
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k6eAd1	George
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
lordem	lord	k1gMnSc7	lord
<g/>
–	–	k?	–
<g/>
místodržitelem	místodržitel	k1gMnSc7	místodržitel
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Shropshire	Shropshir	k1gInSc5	Shropshir
a	a	k8xC	a
podnikl	podniknout	k5eAaPmAgMnS	podniknout
poslední	poslední	k2eAgFnPc4d1	poslední
stavební	stavební	k2eAgFnPc4d1	stavební
úpravy	úprava	k1gFnPc4	úprava
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Powis	Powis	k1gFnSc2	Powis
Castle	Castle	k1gFnSc2	Castle
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
padli	padnout	k5eAaImAgMnP	padnout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
Powis	Powis	k1gFnSc3	Powis
Castle	Castle	k1gFnSc2	Castle
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
organizace	organizace	k1gFnSc2	organizace
National	National	k1gFnSc2	National
Trust	trust	k1gInSc1	trust
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
uvedených	uvedený	k2eAgFnPc2d1	uvedená
hlavních	hlavní	k2eAgFnPc2d1	hlavní
větví	větev	k1gFnPc2	větev
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
linií	linie	k1gFnPc2	linie
Herbertů	Herbert	k1gMnPc2	Herbert
<g/>
.	.	kIx.	.
</s>
<s>
Henry	Henry	k1gMnSc1	Henry
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1756	[number]	k4	1756
<g/>
–	–	k?	–
<g/>
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
irského	irský	k2eAgInSc2d1	irský
a	a	k8xC	a
anglického	anglický	k2eAgInSc2d1	anglický
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
po	po	k7c6	po
rodině	rodina	k1gFnSc6	rodina
MacCarthy	MacCartha	k1gFnSc2	MacCartha
zdědil	zdědit	k5eAaPmAgMnS	zdědit
statky	statek	k1gInPc7	statek
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
zde	zde	k6eAd1	zde
zbohatl	zbohatnout	k5eAaPmAgInS	zbohatnout
důlním	důlní	k2eAgNnSc7d1	důlní
podnikáním	podnikání	k1gNnSc7	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Henry	Henry	k1gMnSc1	Henry
Arthur	Arthur	k1gMnSc1	Arthur
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1806	[number]	k4	1806
<g/>
–	–	k?	–
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
ministrem	ministr	k1gMnSc7	ministr
pro	pro	k7c4	pro
Irsko	Irsko	k1gNnSc4	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Rodové	rodový	k2eAgNnSc1d1	rodové
sídlo	sídlo	k1gNnSc1	sídlo
Muckross	Muckrossa	k1gFnPc2	Muckrossa
House	house	k1gNnSc1	house
prošlo	projít	k5eAaPmAgNnS	projít
nákladnou	nákladný	k2eAgFnSc7d1	nákladná
přestavbou	přestavba	k1gFnSc7	přestavba
před	před	k7c7	před
návštěvou	návštěva	k1gFnSc7	návštěva
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
(	(	kIx(	(
<g/>
1501	[number]	k4	1501
<g/>
–	–	k?	–
<g/>
1570	[number]	k4	1570
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
státník	státník	k1gMnSc1	státník
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
štolba	štolba	k1gMnSc1	štolba
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
Podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
Henry	Henry	k1gMnSc1	Henry
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
(	(	kIx(	(
<g/>
1534	[number]	k4	1534
<g/>
–	–	k?	–
<g/>
1601	[number]	k4	1601
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
Podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
Edward	Edward	k1gMnSc1	Edward
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Herbert	Herbert	k1gMnSc1	Herbert
z	z	k7c2	z
Cherbury	Cherbura	k1gFnSc2	Cherbura
(	(	kIx(	(
<g/>
1583	[number]	k4	1583
<g/>
–	–	k?	–
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
</s>
</p>
<p>
<s>
Philip	Philip	k1gMnSc1	Philip
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
(	(	kIx(	(
<g/>
1584	[number]	k4	1584
<g/>
–	–	k?	–
<g/>
1650	[number]	k4	1650
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
komoří	komoří	k1gMnSc1	komoří
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
Podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc4	Georg
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1593	[number]	k4	1593
<g/>
–	–	k?	–
<g/>
1633	[number]	k4	1633
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
markýz	markýza	k1gFnPc2	markýza
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
(	(	kIx(	(
<g/>
1626	[number]	k4	1626
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
<g/>
,	,	kIx,	,
vychovatel	vychovatel	k1gMnSc1	vychovatel
Jakuba	Jakub	k1gMnSc2	Jakub
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
Podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Torringtonu	Torrington	k1gInSc2	Torrington
(	(	kIx(	(
<g/>
1647	[number]	k4	1647
<g/>
–	–	k?	–
<g/>
1716	[number]	k4	1716
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
admirál	admirál	k1gMnSc1	admirál
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
(	(	kIx(	(
<g/>
1656	[number]	k4	1656
<g/>
–	–	k?	–
<g/>
1733	[number]	k4	1733
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místokrál	místokrál	k1gMnSc1	místokrál
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
lord	lord	k1gMnSc1	lord
admirality	admiralita	k1gFnSc2	admiralita
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
Podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
Henry	Henry	k1gMnSc1	Henry
Arthur	Arthur	k1gMnSc1	Arthur
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
(	(	kIx(	(
<g/>
1703	[number]	k4	1703
<g/>
–	–	k?	–
<g/>
1772	[number]	k4	1772
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
</s>
</p>
<p>
<s>
George	George	k6eAd1	George
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Pembroke	Pembrok	k1gFnSc2	Pembrok
(	(	kIx(	(
<g/>
1759	[number]	k4	1759
<g/>
–	–	k?	–
<g/>
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
dvořan	dvořan	k1gMnSc1	dvořan
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
Podvazkového	podvazkový	k2eAgInSc2d1	podvazkový
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
Sidney	Sidnea	k1gMnSc2	Sidnea
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
–	–	k?	–
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
a	a	k8xC	a
kolonií	kolonie	k1gFnPc2	kolonie
</s>
</p>
<p>
<s>
Henry	Henry	k1gMnSc1	Henry
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
místokrál	místokrál	k1gMnSc1	místokrál
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
</s>
</p>
<p>
<s>
Sir	sir	k1gMnSc1	sir
Michael	Michael	k1gMnSc1	Michael
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
USA	USA	kA	USA
</s>
</p>
<p>
<s>
George	George	k6eAd1	George
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Carnarvonu	Carnarvon	k1gInSc2	Carnarvon
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
Tutanchamonovy	Tutanchamonův	k2eAgFnSc2d1	Tutanchamonova
hrobky	hrobka	k1gFnSc2	hrobka
</s>
</p>
<p>
<s>
Auberon	Auberon	k1gMnSc1	Auberon
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
válečný	válečný	k2eAgInSc1d1	válečný
pilot	pilot	k1gInSc1	pilot
</s>
</p>
<p>
<s>
Sir	sir	k1gMnSc1	sir
John	John	k1gMnSc1	John
Arthur	Arthur	k1gMnSc1	Arthur
Herbert	Herbert	k1gMnSc1	Herbert
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
v	v	k7c6	v
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Powis	Powis	k1gInSc1	Powis
CastleHrad	CastleHrada	k1gFnPc2	CastleHrada
Ludlow	Ludlow	k1gFnSc2	Ludlow
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
(	(	kIx(	(
<g/>
reprint	reprint	k1gInSc1	reprint
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
439	[number]	k4	439
<g/>
–	–	k?	–
<g/>
440	[number]	k4	440
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
7203	[number]	k4	7203
<g/>
–	–	k?	–
<g/>
249	[number]	k4	249
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
Historic	Historice	k1gInPc2	Historice
houses	housesa	k1gFnPc2	housesa
and	and	k?	and
gardens	gardensa	k1gFnPc2	gardensa
<g/>
,	,	kIx,	,
Banbury	Banbura	k1gFnSc2	Banbura
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
9531426	[number]	k4	9531426
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Rodokmen	rodokmen	k1gInSc4	rodokmen
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
PembrokeRodokmen	PembrokeRodokmen	k1gInSc4	PembrokeRodokmen
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
CarnarvonuRodokmen	CarnarvonuRodokmen	k1gInSc4	CarnarvonuRodokmen
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Powisu	Powis	k1gInSc2	Powis
</s>
</p>
