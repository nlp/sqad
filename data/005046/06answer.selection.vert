<s>
První	první	k4xOgInSc4	první
byla	být	k5eAaImAgFnS	být
osídlena	osídlen	k2eAgFnSc1d1	osídlena
severní	severní	k2eAgFnSc1d1	severní
Mezopotámie	Mezopotámie	k1gFnSc1	Mezopotámie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
bránit	bránit	k5eAaImF	bránit
záplavám	záplava	k1gFnPc3	záplava
Eufratu	Eufrat	k1gInSc2	Eufrat
a	a	k8xC	a
Tigridu	Tigris	k1gInSc2	Tigris
<g/>
.	.	kIx.	.
</s>
