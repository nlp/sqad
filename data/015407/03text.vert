<s>
Oblomov	Oblomov	k1gMnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oblomov	Oblomov	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Н	Н	k?
д	д	k?
и	и	k?
ж	ж	k?
И	И	k?
И	И	k?
О	О	k?
Země	zem	k1gFnSc2
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
ruština	ruština	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
140	#num#	k4
minut	minuta	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
filmové	filmový	k2eAgNnSc1d1
dramakomediální	dramakomediální	k2eAgNnSc1d1
drama	drama	k1gNnSc1
Předloha	předloha	k1gFnSc1
</s>
<s>
Oblomov	Oblomov	k1gMnSc1
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
<g/>
Ivan	Ivan	k1gMnSc1
Alexandrovič	Alexandrovič	k1gMnSc1
Gončarov	Gončarov	k1gInSc4
Scénář	scénář	k1gInSc1
</s>
<s>
Nikita	Nikita	k1gFnSc1
MichalkovAlexandr	MichalkovAlexandr	k1gInSc1
Adabašjan	Adabašjan	k1gMnSc1
Režie	režie	k1gFnSc1
</s>
<s>
Nikita	Nikita	k1gMnSc1
Michalkov	Michalkov	k1gInSc4
Obsazení	obsazení	k1gNnSc2
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Oleg	Oleg	k1gMnSc1
TabakovJelena	TabakovJelen	k2eAgFnSc1d1
SolovějováJurij	SolovějováJurij	k1gFnSc1
Bogatyrjov	Bogatyrjov	k1gInSc1
Produkce	produkce	k1gFnSc1
</s>
<s>
Willi	Will	k1gMnPc1
Geller	Geller	k1gMnSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
Artěmjev	Artěmjev	k1gFnSc2
Kamera	kamera	k1gFnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Lebešev	Lebešev	k1gMnSc1
Střih	střih	k1gInSc4
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Praxinová	Praxinová	k1gFnSc1
Oblomov	Oblomov	k1gMnSc1
na	na	k7c4
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oblomov	Oblomov	k1gMnSc1
(	(	kIx(
<g/>
v	v	k7c6
ruském	ruský	k2eAgInSc6d1
originále	originál	k1gInSc6
<g/>
:	:	kIx,
Н	Н	k?
д	д	k?
и	и	k?
ж	ж	k?
И	И	k?
И	И	k?
О	О	k?
<g/>
,	,	kIx,
Několik	několik	k4yIc4
dní	den	k1gInPc2
z	z	k7c2
života	život	k1gInSc2
I.	I.	kA
I.	I.	kA
Oblomova	Oblomov	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
romantické	romantický	k2eAgNnSc1d1
filmové	filmový	k2eAgNnSc1d1
drama	drama	k1gNnSc1
z	z	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
<g/>
,	,	kIx,
natočené	natočený	k2eAgNnSc1d1
ruským	ruský	k2eAgMnSc7d1
režisérem	režisér	k1gMnSc7
Nikitou	Nikita	k1gMnSc7
Michalkovem	Michalkov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předlohou	předloha	k1gFnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
román	román	k1gInSc1
Oblomov	Oblomov	k1gMnSc1
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
napsaný	napsaný	k2eAgInSc4d1
Ivanem	Ivan	k1gMnSc7
Alexandrovičem	Alexandrovič	k1gMnSc7
Gončarovem	Gončarov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
vypráví	vyprávět	k5eAaImIp3nS
příběh	příběh	k1gInSc1
hlavního	hlavní	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
středních	střední	k2eAgFnPc2d1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
žijícího	žijící	k2eAgInSc2d1
pasivním	pasivní	k2eAgInSc7d1
stylem	styl	k1gInSc7
života	život	k1gInSc2
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
setkání	setkání	k1gNnSc2
s	s	k7c7
okouzlující	okouzlující	k2eAgFnSc7d1
Olgou	Olga	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
v	v	k7c6
něm	on	k3xPp3gInSc6
zažehne	zažehnout	k5eAaPmIp3nS
city	city	k1gNnSc1
a	a	k8xC
úsilí	úsilí	k1gNnSc1
po	po	k7c6
změně	změna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titulní	titulní	k2eAgFnSc4d1
roli	role	k1gFnSc4
Oblomova	Oblomov	k1gMnSc4
ztvárnil	ztvárnit	k5eAaPmAgMnS
Oleg	Oleg	k1gMnSc1
Tabakov	Tabakov	k1gInSc4
<g/>
,	,	kIx,
Olgu	Olga	k1gFnSc4
představovala	představovat	k5eAaImAgFnS
Jelena	Jelena	k1gFnSc1
Solovějová	Solovějový	k2eAgFnSc1d1
a	a	k8xC
jejich	jejich	k3xOp3gMnSc4
společného	společný	k2eAgMnSc4d1
přítele	přítel	k1gMnSc4
Štolce	Štolec	k1gMnSc4
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgMnS
Jurij	Jurij	k1gMnSc1
Bogatyrjov	Bogatyrjov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Hudbu	hudba	k1gFnSc4
zkomponoval	zkomponovat	k5eAaPmAgMnS
Eduard	Eduard	k1gMnSc1
Artěmjev	Artěmjev	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc4
doprovázely	doprovázet	k5eAaImAgInP
také	také	k9
části	část	k1gFnSc2
díla	dílo	k1gNnSc2
italského	italský	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
Vincenza	Vincenz	k1gMnSc2
Belliniho	Bellini	k1gMnSc2
a	a	k8xC
ruského	ruský	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
Sergeje	Sergej	k1gMnSc2
Rachmaninova	Rachmaninův	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
lokalit	lokalita	k1gFnPc2
natáčení	natáčení	k1gNnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vesnice	vesnice	k1gFnSc1
Seňkino	Seňkino	k1gNnSc4
v	v	k7c6
moskevské	moskevský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
snímek	snímek	k1gInSc1
obdržel	obdržet	k5eAaPmAgInS
Cenu	cena	k1gFnSc4
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
amerických	americký	k2eAgMnPc2d1
filmových	filmový	k2eAgMnPc2d1
kritiků	kritik	k1gMnPc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Nejlepší	dobrý	k2eAgInSc4d3
cizojazyčný	cizojazyčný	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ilja	Ilja	k1gMnSc1
Iljič	Iljič	k1gMnSc1
Oblomov	Oblomov	k1gMnSc1
(	(	kIx(
<g/>
Oleg	Oleg	k1gMnSc1
Tabakov	Tabakov	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
petrohradský	petrohradský	k2eAgMnSc1d1
příslušník	příslušník	k1gMnSc1
vyšší	vysoký	k2eAgFnSc2d2
střední	střední	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
<g/>
,	,	kIx,
vlastník	vlastník	k1gMnSc1
půdy	půda	k1gFnSc2
ve	v	k7c6
vsi	ves	k1gFnSc6
Oblomovka	Oblomovka	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
jejíchž	jejíž	k3xOyRp3gInPc2
výnosů	výnos	k1gInPc2
žije	žít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdy	nikdy	k6eAd1
nepracoval	pracovat	k5eNaImAgMnS
<g/>
,	,	kIx,
většinu	většina	k1gFnSc4
času	čas	k1gInSc2
tráví	trávit	k5eAaImIp3nP
na	na	k7c6
divanu	divan	k1gInSc6
a	a	k8xC
bydlí	bydlet	k5eAaImIp3nS
pouze	pouze	k6eAd1
s	s	k7c7
obhroublým	obhroublý	k2eAgMnSc7d1
sluhou	sluha	k1gMnSc7
Zacharem	Zachar	k1gMnSc7
(	(	kIx(
<g/>
Andrej	Andrej	k1gMnSc1
Popov	Popov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
svého	svůj	k3xOyFgMnSc4
pána	pán	k1gMnSc4
uctívá	uctívat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horší	zlý	k2eAgInPc1d2
výnosy	výnos	k1gInPc1
znamenají	znamenat	k5eAaImIp3nP
existenční	existenční	k2eAgInPc1d1
problémy	problém	k1gInPc1
a	a	k8xC
nutnost	nutnost	k1gFnSc1
stěhování	stěhování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediného	jediné	k1gNnSc2
<g/>
,	,	kIx,
koho	kdo	k3yQnSc4,k3yRnSc4,k3yInSc4
má	mít	k5eAaImIp3nS
Ilja	Ilja	k1gMnSc1
upřímně	upřímně	k6eAd1
rád	rád	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
přítel	přítel	k1gMnSc1
z	z	k7c2
mládí	mládí	k1gNnSc2
Andrej	Andrej	k1gMnSc1
Štolc	Štolc	k1gInSc1
(	(	kIx(
<g/>
Jurij	Jurij	k1gFnSc1
Bogatyrjov	Bogatyrjov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
jej	on	k3xPp3gNnSc4
před	před	k7c7
odjezdem	odjezd	k1gInSc7
do	do	k7c2
západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
seznamuje	seznamovat	k5eAaImIp3nS
a	a	k8xC
svěřuje	svěřovat	k5eAaImIp3nS
do	do	k7c2
péče	péče	k1gFnSc2
Olgy	Olga	k1gFnSc2
Sergejevny	Sergejevna	k1gFnSc2
(	(	kIx(
<g/>
Jelena	Jelena	k1gFnSc1
Solovějová	Solovějový	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mladé	mladý	k2eAgFnPc1d1
dívky	dívka	k1gFnPc1
plné	plný	k2eAgFnPc1d1
života	život	k1gInSc2
s	s	k7c7
krásným	krásný	k2eAgInSc7d1
operním	operní	k2eAgInSc7d1
hlasem	hlas	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblomov	Oblomov	k1gMnSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
za	za	k7c7
ním	on	k3xPp3gMnSc7
dorazit	dorazit	k5eAaPmF
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
cestu	cesta	k1gFnSc4
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nevydá	vydat	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
čtyř	čtyři	k4xCgInPc2
měsíců	měsíc	k1gInPc2
dochází	docházet	k5eAaImIp3nS
mezi	mezi	k7c7
ním	on	k3xPp3gMnSc7
a	a	k8xC
Olgou	Olga	k1gFnSc7
k	k	k7c3
oboustrannému	oboustranný	k2eAgNnSc3d1
vzplanutí	vzplanutí	k1gNnSc3
citů	cit	k1gInPc2
a	a	k8xC
zamilování	zamilování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
náboj	náboj	k1gInSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
proměně	proměna	k1gFnSc3
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
dosavadního	dosavadní	k2eAgInSc2d1
bezcílného	bezcílný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
více	hodně	k6eAd2
číst	číst	k5eAaImF
a	a	k8xC
zajímat	zajímat	k5eAaImF
se	se	k3xPyFc4
o	o	k7c4
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
si	se	k3xPyFc3
však	však	k9
uvědomuje	uvědomovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Olžina	Olžin	k2eAgFnSc1d1
náklonnost	náklonnost	k1gFnSc1
je	být	k5eAaImIp3nS
způsobena	způsobit	k5eAaPmNgFnS
nedostatkem	nedostatek	k1gInSc7
lepších	dobrý	k2eAgMnPc2d2
mužů	muž	k1gMnPc2
v	v	k7c6
jejím	její	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upřímně	upřímně	k6eAd1
se	se	k3xPyFc4
tak	tak	k6eAd1
vyznává	vyznávat	k5eAaImIp3nS
v	v	k7c6
dopise	dopis	k1gInSc6
na	na	k7c4
rozloučenou	rozloučená	k1gFnSc4
<g/>
,	,	kIx,
rozhodnut	rozhodnut	k2eAgInSc4d1
nestát	stát	k5eNaImF,k5eNaPmF
v	v	k7c6
cestě	cesta	k1gFnSc6
jejímu	její	k3xOp3gNnSc3
budoucímu	budoucí	k2eAgNnSc3d1
štěstí	štěstí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
nedokáže	dokázat	k5eNaPmIp3nS
odjet	odjet	k5eAaPmF
a	a	k8xC
při	při	k7c6
Olžině	Olžin	k2eAgNnSc6d1
čtení	čtení	k1gNnSc6
dopisu	dopis	k1gInSc2
v	v	k7c6
aleji	alej	k1gFnSc6
<g/>
,	,	kIx,
rozechvělý	rozechvělý	k2eAgMnSc1d1
sleduje	sledovat	k5eAaImIp3nS
její	její	k3xOp3gFnSc4
plačtivou	plačtivý	k2eAgFnSc4d1
reakci	reakce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štolc	Štolc	k1gInSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
z	z	k7c2
cest	cesta	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
dlouholeté	dlouholetý	k2eAgNnSc1d1
okouzlení	okouzlení	k1gNnSc1
Olgou	Olga	k1gFnSc7
stále	stále	k6eAd1
nevyprchalo	vyprchat	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Vypravěč	vypravěč	k1gMnSc1
nastínil	nastínit	k5eAaPmAgMnS
další	další	k2eAgInPc4d1
osudy	osud	k1gInPc4
postav	postava	k1gFnPc2
<g/>
:	:	kIx,
Po	po	k7c6
roce	rok	k1gInSc6
od	od	k7c2
návratu	návrat	k1gInSc2
pojal	pojmout	k5eAaPmAgInS
Štolc	Štolc	k1gInSc1
za	za	k7c4
manželku	manželka	k1gFnSc4
Olgu	Olga	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Oblomovem	Oblomov	k1gMnSc7
se	se	k3xPyFc4
vídali	vídat	k5eAaImAgMnP
čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
dál	daleko	k6eAd2
méně	málo	k6eAd2
<g/>
,	,	kIx,
až	až	k9
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInPc1
kontakty	kontakt	k1gInPc1
přerušily	přerušit	k5eAaPmAgInP
zcela	zcela	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilja	Ilja	k1gMnSc1
se	se	k3xPyFc4
přestěhoval	přestěhovat	k5eAaPmAgMnS
do	do	k7c2
viborské	viborský	k2eAgFnSc2d1
části	část	k1gFnSc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
žil	žít	k5eAaImAgMnS
v	v	k7c6
domě	dům	k1gInSc6
vdovy	vdova	k1gFnSc2
Agafije	Agafije	k1gFnSc2
Matjejevny	Matjejevna	k1gFnSc2
Pšenicinové	Pšenicinová	k1gFnSc2
a	a	k8xC
romanticky	romanticky	k6eAd1
snil	snít	k5eAaImAgMnS
o	o	k7c6
cestě	cesta	k1gFnSc6
na	na	k7c6
vesnici	vesnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
se	se	k3xPyFc4
sblížili	sblížit	k5eAaPmAgMnP
a	a	k8xC
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
manželství	manželství	k1gNnSc2
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
vzešel	vzejít	k5eAaPmAgMnS
syn	syn	k1gMnSc1
Andrej	Andrej	k1gMnSc1
<g/>
,	,	kIx,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
na	na	k7c4
počest	počest	k1gFnSc4
přítele	přítel	k1gMnSc2
Štolce	Štolec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblomov	Oblomov	k1gMnSc1
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
zhodnotil	zhodnotit	k5eAaPmAgMnS
kladně	kladně	k6eAd1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jej	on	k3xPp3gMnSc4
neprovázelo	provázet	k5eNaImAgNnS
mnoho	mnoho	k4c1
vzletných	vzletný	k2eAgFnPc2d1
chvil	chvíle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dalších	další	k2eAgNnPc6d1
sedmi	sedm	k4xCc6
letech	léto	k1gNnPc6
skonal	skonat	k5eAaPmAgInS
na	na	k7c4
mrtvici	mrtvice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agafja	Agafj	k1gInSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
apatickou	apatický	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malého	Malý	k1gMnSc2
Andreje	Andrej	k1gMnSc2
přijali	přijmout	k5eAaPmAgMnP
jako	jako	k9
za	za	k7c2
vlastního	vlastní	k2eAgMnSc2d1
Štolc	Štolc	k1gInSc4
s	s	k7c7
Olgou	Olga	k1gFnSc7
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
vyrůstal	vyrůstat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Oleg	Oleg	k1gMnSc1
Tabakov	Tabakov	k1gInSc4
</s>
<s>
Ilja	Ilja	k1gMnSc1
Iljič	Iljič	k1gMnSc1
Oblomov	Oblomov	k1gMnSc1
</s>
<s>
Jelena	Jelena	k1gFnSc1
Solovějová	Solovějový	k2eAgFnSc1d1
</s>
<s>
Olga	Olga	k1gFnSc1
Sergejevna	Sergejevna	k1gFnSc1
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Bogatyrjov	Bogatyrjov	k1gInSc1
</s>
<s>
Andrej	Andrej	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
Štolc	Štolc	k1gFnSc4
</s>
<s>
Andrej	Andrej	k1gMnSc1
Popov	Popov	k1gInSc4
</s>
<s>
sluha	sluha	k1gMnSc1
Zachar	Zachar	k1gMnSc1
</s>
<s>
Avangard	Avangard	k1gMnSc1
Leontěv	Leontěv	k1gMnSc1
</s>
<s>
Alexejev	Alexejet	k5eAaPmDgInS
</s>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS
Steblov	Steblov	k1gInSc1
</s>
<s>
otec	otec	k1gMnSc1
Oblomova	Oblomov	k1gMnSc2
</s>
<s>
Jevgenija	Jevgenij	k2eAgFnSc1d1
Glušenková	Glušenkový	k2eAgFnSc1d1
</s>
<s>
matka	matka	k1gFnSc1
Oblomova	Oblomov	k1gMnSc2
</s>
<s>
Nikolaj	Nikolaj	k1gMnSc1
Pastuchov	Pastuchov	k1gInSc4
</s>
<s>
otec	otec	k1gMnSc1
Stolce	stolec	k1gInSc2
</s>
<s>
Jelena	Jelena	k1gFnSc1
Klevščeská	Klevščeský	k2eAgFnSc1d1
</s>
<s>
Káťa	Káťa	k1gFnSc1
</s>
<s>
Halina	Halina	k1gFnSc1
Šostková	Šostková	k1gFnSc1
</s>
<s>
teta	teta	k1gFnSc1
Olgy	Olga	k1gFnSc2
</s>
<s>
Gleb	Gleb	k1gInSc1
Striženov	Striženov	k1gInSc1
</s>
<s>
baron	baron	k1gMnSc1
von	von	k1gInSc4
Langwagen	Langwagen	k1gInSc1
</s>
<s>
Andrej	Andrej	k1gMnSc1
Razumovskij	Razumovskij	k1gMnSc1
</s>
<s>
malý	malý	k2eAgMnSc1d1
Ilja	Ilja	k1gMnSc1
Iljič	Iljič	k1gMnSc1
Oblomov	Oblomov	k1gMnSc1
</s>
<s>
Oleg	Oleg	k1gMnSc1
Kozlov	Kozlov	k1gInSc4
</s>
<s>
malý	malý	k2eAgMnSc1d1
Andrej	Andrej	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
Štolc	Štolc	k1gFnSc4
</s>
<s>
Fjodor	Fjodor	k1gInSc1
Stukov	Stukov	k1gInSc1
</s>
<s>
Andrjuša	Andrjuša	k1gMnSc1
Oblomov	Oblomov	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Kadočnikov	Kadočnikov	k1gInSc4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Petrovič	Petrovič	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oblomov	Oblomov	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Oblomov	Oblomov	k1gMnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
