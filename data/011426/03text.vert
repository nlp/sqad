<p>
<s>
Tatracentrum	Tatracentrum	k1gNnSc1	Tatracentrum
je	být	k5eAaImIp3nS	být
polyfunkční	polyfunkční	k2eAgInSc4d1	polyfunkční
objekt	objekt	k1gInSc4	objekt
na	na	k7c6	na
Hodžově	Hodžův	k2eAgNnSc6d1	Hodžovo
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
kolektiv	kolektiv	k1gInSc4	kolektiv
architektů	architekt	k1gMnPc2	architekt
Radomil	Radomil	k1gMnSc1	Radomil
Kachlík	Kachlík	k1gMnSc1	Kachlík
<g/>
,	,	kIx,	,
Matej	Matej	k1gInSc1	Matej
Siebert	Siebert	k1gInSc1	Siebert
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Vavrica	Vavrica	k1gMnSc1	Vavrica
a	a	k8xC	a
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Závodný	Závodný	k2eAgMnSc1d1	Závodný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
až	až	k9	až
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Urbanisticko-architektonické	urbanistickorchitektonický	k2eAgNnSc4d1	urbanisticko-architektonický
řešení	řešení	k1gNnSc4	řešení
==	==	k?	==
</s>
</p>
<p>
<s>
Hmotově-prostorové	Hmotověrostorový	k2eAgNnSc1d1	Hmotově-prostorový
řešení	řešení	k1gNnSc1	řešení
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
ze	z	k7c2	z
stanovení	stanovení	k1gNnSc2	stanovení
zásad	zásada	k1gFnPc2	zásada
na	na	k7c4	na
výrazové	výrazový	k2eAgNnSc4d1	výrazové
řešení	řešení	k1gNnSc4	řešení
polyfunkčního	polyfunkční	k2eAgInSc2d1	polyfunkční
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
respektují	respektovat	k5eAaImIp3nP	respektovat
kontextuální	kontextuální	k2eAgInSc4d1	kontextuální
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
stávajícím	stávající	k2eAgFnPc3d1	stávající
zachovávání	zachovávání	k1gNnPc1	zachovávání
okolním	okolní	k2eAgInSc7d1	okolní
architektonickým	architektonický	k2eAgInSc7d1	architektonický
objektem	objekt	k1gInSc7	objekt
a	a	k8xC	a
respektují	respektovat	k5eAaImIp3nP	respektovat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
konfiguraci	konfigurace	k1gFnSc4	konfigurace
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
"	"	kIx"	"
<g/>
uzávěr	uzávěr	k1gInSc1	uzávěr
'	'	kIx"	'
<g/>
Hodžova	Hodžův	k2eAgNnSc2d1	Hodžovo
náměstí	náměstí	k1gNnSc2	náměstí
liniovou	liniový	k2eAgFnSc7d1	liniová
hmotou	hmota	k1gFnSc7	hmota
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
objektu	objekt	k1gInSc2	objekt
členit	členit	k5eAaImF	členit
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
urbanistických	urbanistický	k2eAgNnPc2d1	Urbanistické
kritérií	kritérion	k1gNnPc2	kritérion
–	–	k?	–
zasklená	zasklený	k2eAgFnSc1d1	zasklená
a	a	k8xC	a
transparentní	transparentní	k2eAgFnSc1d1	transparentní
hlava	hlava	k1gFnSc1	hlava
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
vysunutá	vysunutý	k2eAgFnSc1d1	vysunutá
část	část	k1gFnSc1	část
do	do	k7c2	do
náměstí	náměstí	k1gNnSc2	náměstí
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
Prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
zapuštěná	zapuštěný	k2eAgFnSc1d1	zapuštěná
<g/>
"	"	kIx"	"
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
'	'	kIx"	'
část	část	k1gFnSc1	část
fasády	fasáda	k1gFnSc2	fasáda
a	a	k8xC	a
kontextuální	kontextuální	k2eAgInSc1d1	kontextuální
závěr	závěr	k1gInSc1	závěr
objektu	objekt	k1gInSc2	objekt
–	–	k?	–
výškově	výškově	k6eAd1	výškově
zónování	zónování	k1gNnSc2	zónování
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgNnSc7d1	důležité
kritériem	kritérion	k1gNnSc7	kritérion
bylo	být	k5eAaImAgNnS	být
zachování	zachování	k1gNnSc1	zachování
urbanistických	urbanistický	k2eAgInPc2d1	urbanistický
průhledů	průhled	k1gInPc2	průhled
z	z	k7c2	z
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
ulice	ulice	k1gFnSc2	ulice
na	na	k7c4	na
Hrad	hrad	k1gInSc4	hrad
ale	ale	k8xC	ale
i	i	k9	i
průhledu	průhled	k1gInSc2	průhled
z	z	k7c2	z
Poštové	Poštové	k?	Poštové
ulice	ulice	k1gFnSc2	ulice
na	na	k7c4	na
Grassalkovičův	Grassalkovičův	k2eAgInSc4d1	Grassalkovičův
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
průhlednosti	průhlednost	k1gFnSc6	průhlednost
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
při	při	k7c6	při
pohledech	pohled	k1gInPc6	pohled
přes	přes	k7c4	přes
budovu	budova	k1gFnSc4	budova
dát	dát	k5eAaPmF	dát
vytušit	vytušit	k5eAaPmF	vytušit
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
za	za	k7c7	za
terénní	terénní	k2eAgFnSc7d1	terénní
hranou	hrana	k1gFnSc7	hrana
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
důležitým	důležitý	k2eAgInSc7d1	důležitý
byla	být	k5eAaImAgFnS	být
možnost	možnost	k1gFnSc4	možnost
dálkových	dálkový	k2eAgInPc2d1	dálkový
pohledů	pohled	k1gInPc2	pohled
z	z	k7c2	z
objektu	objekt	k1gInSc2	objekt
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
Hradní	hradní	k2eAgInSc4d1	hradní
kopec	kopec	k1gInSc4	kopec
a	a	k8xC	a
zajištění	zajištění	k1gNnSc4	zajištění
vizuálního	vizuální	k2eAgInSc2d1	vizuální
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Transparentnost	transparentnost	k1gFnSc1	transparentnost
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
vlastností	vlastnost	k1gFnPc2	vlastnost
tohoto	tento	k3xDgInSc2	tento
polyfunkčního	polyfunkční	k2eAgInSc2d1	polyfunkční
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Parter	parter	k1gInSc1	parter
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
řešen	řešit	k5eAaImNgInS	řešit
atraktivně	atraktivně	k6eAd1	atraktivně
–	–	k?	–
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
obchodní	obchodní	k2eAgInPc4d1	obchodní
provozy	provoz	k1gInPc4	provoz
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
podlažích	podlaží	k1gNnPc6	podlaží
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
pasáží	pasáž	k1gFnSc7	pasáž
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
bankovní	bankovní	k2eAgFnSc1d1	bankovní
expozitura	expozitura	k1gFnSc1	expozitura
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
obchodů	obchod	k1gInPc2	obchod
však	však	k9	však
netvoří	tvořit	k5eNaImIp3nS	tvořit
žádný	žádný	k3yNgInSc4	žádný
orientální	orientální	k2eAgInSc4d1	orientální
bazar	bazar	k1gInSc4	bazar
naplněný	naplněný	k2eAgInSc4d1	naplněný
butiky	butik	k1gInPc4	butik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
důstojně	důstojně	k6eAd1	důstojně
jasný	jasný	k2eAgInSc1d1	jasný
a	a	k8xC	a
přehledný	přehledný	k2eAgInSc1d1	přehledný
blok	blok	k1gInSc1	blok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
budovy	budova	k1gFnSc2	budova
jsou	být	k5eAaImIp3nP	být
vytvořeny	vytvořen	k2eAgFnPc4d1	vytvořena
parkovací	parkovací	k2eAgNnPc4d1	parkovací
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
administrativu	administrativa	k1gFnSc4	administrativa
centrály	centrála	k1gFnSc2	centrála
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
obchodních	obchodní	k2eAgInPc2d1	obchodní
provozů	provoz	k1gInPc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
řešen	řešit	k5eAaImNgInS	řešit
s	s	k7c7	s
dvoupodlažním	dvoupodlažní	k2eAgInSc7d1	dvoupodlažní
parterem	parter	k1gInSc7	parter
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
pilíři	pilíř	k1gInPc7	pilíř
s	s	k7c7	s
kamenným	kamenný	k2eAgInSc7d1	kamenný
obkladem	obklad	k1gInSc7	obklad
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
žula	žula	k1gFnSc1	žula
–	–	k?	–
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ji	on	k3xPp3gFnSc4	on
použil	použít	k5eAaPmAgMnS	použít
Emil	Emil	k1gMnSc1	Emil
Belluš	Belluš	k1gMnSc1	Belluš
na	na	k7c6	na
Družstevních	družstevní	k2eAgInPc6d1	družstevní
domech	dům	k1gInPc6	dům
<g/>
)	)	kIx)	)
v	v	k7c6	v
příčném	příčný	k2eAgInSc6d1	příčný
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Hodžova	Hodžův	k2eAgNnSc2d1	Hodžovo
náměstí	náměstí	k1gNnSc2	náměstí
si	se	k3xPyFc3	se
parter	parter	k2eAgNnSc4d1	parter
přímo	přímo	k6eAd1	přímo
přál	přát	k5eAaImAgMnS	přát
být	být	k5eAaImF	být
dvoupodlažní	dvoupodlažní	k2eAgMnSc1d1	dvoupodlažní
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
jednopodlažní	jednopodlažní	k2eAgNnSc1d1	jednopodlažní
měřítko	měřítko	k1gNnSc1	měřítko
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
v	v	k7c6	v
dopravním	dopravní	k2eAgInSc6d1	dopravní
podchodu	podchod	k1gInSc6	podchod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
masivní	masivní	k2eAgFnPc4d1	masivní
betonové	betonový	k2eAgFnPc4d1	betonová
zábrany	zábrana	k1gFnPc4	zábrana
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vizuálně	vizuálně	k6eAd1	vizuálně
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
utopen	utopit	k5eAaPmNgInS	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hlavní	hlavní	k2eAgFnSc6d1	hlavní
části	část	k1gFnSc6	část
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
parter	parter	k1gInSc4	parter
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
příčné	příčný	k2eAgInPc1d1	příčný
sloupy	sloup	k1gInPc1	sloup
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
barevné	barevný	k2eAgInPc1d1	barevný
i	i	k9	i
materiálově	materiálově	k6eAd1	materiálově
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
svojí	svůj	k3xOyFgFnSc7	svůj
příčnou	příčný	k2eAgFnSc7d1	příčná
polohou	poloha	k1gFnSc7	poloha
otevírají	otevírat	k5eAaImIp3nP	otevírat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
pasáž	pasáž	k1gFnSc4	pasáž
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
plocha	plocha	k1gFnSc1	plocha
chodníku	chodník	k1gInSc2	chodník
a	a	k8xC	a
náměstí	náměstí	k1gNnSc1	náměstí
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Zasklené	zasklený	k2eAgFnPc1d1	zasklená
stěny	stěna	k1gFnPc1	stěna
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
přírodního	přírodní	k2eAgInSc2d1	přírodní
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Zasklená	zasklený	k2eAgFnSc1d1	zasklená
stěna	stěna	k1gFnSc1	stěna
parteru	parter	k1gInSc2	parter
je	být	k5eAaImIp3nS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
až	až	k9	až
po	po	k7c4	po
původní	původní	k2eAgInSc4d1	původní
terén	terén	k1gInSc4	terén
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
průhledu	průhled	k1gInSc2	průhled
do	do	k7c2	do
pasáže	pasáž	k1gFnSc2	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
nadzemních	nadzemní	k2eAgNnPc2d1	nadzemní
podlaží	podlaží	k1gNnPc2	podlaží
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
konzolově	konzolově	k6eAd1	konzolově
vyložených	vyložený	k2eAgMnPc2d1	vyložený
do	do	k7c2	do
Hodžovo	Hodžův	k2eAgNnSc4d1	Hodžovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
optického	optický	k2eAgNnSc2d1	optické
propojení	propojení	k1gNnSc2	propojení
s	s	k7c7	s
náměstím	náměstí	k1gNnSc7	náměstí
je	být	k5eAaImIp3nS	být
navržen	navržen	k2eAgInSc1d1	navržen
nízký	nízký	k2eAgInSc1d1	nízký
parapet	parapet	k1gInSc1	parapet
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
kancelářských	kancelářský	k2eAgFnPc6d1	kancelářská
prostorách	prostora	k1gFnPc6	prostora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
"	"	kIx"	"
<g/>
hlavě	hlava	k1gFnSc6	hlava
<g/>
"	"	kIx"	"
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
zasklený	zasklený	k2eAgInSc1d1	zasklený
hliníkový	hliníkový	k2eAgInSc1d1	hliníkový
plášť	plášť	k1gInSc1	plášť
bez	bez	k7c2	bez
parapetu	parapet	k1gInSc2	parapet
–	–	k?	–
až	až	k9	až
po	po	k7c4	po
podlahu	podlaha	k1gFnSc4	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
objektu	objekt	k1gInSc2	objekt
má	mít	k5eAaImIp3nS	mít
výrazné	výrazný	k2eAgNnSc4d1	výrazné
hliníkové	hliníkový	k2eAgNnSc4d1	hliníkové
lamelové	lamelový	k2eAgNnSc4d1	lamelové
ukončení	ukončení	k1gNnSc4	ukončení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
fasády	fasáda	k1gFnSc2	fasáda
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
identická	identický	k2eAgFnSc1d1	identická
jako	jako	k8xC	jako
severní	severní	k2eAgFnSc1d1	severní
-	-	kIx~	-
přední	přední	k2eAgFnSc1d1	přední
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
soustavu	soustava	k1gFnSc4	soustava
horizontálních	horizontální	k2eAgFnPc2d1	horizontální
skleněných	skleněný	k2eAgFnPc2d1	skleněná
lamel	lamela	k1gFnPc2	lamela
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
krajku	krajka	k1gFnSc4	krajka
<g/>
"	"	kIx"	"
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
kvalitu	kvalita	k1gFnSc4	kvalita
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
neutrální	neutrální	k2eAgFnPc1d1	neutrální
<g/>
,	,	kIx,	,
až	až	k8xS	až
ztrácející	ztrácející	k2eAgMnSc1d1	ztrácející
se	se	k3xPyFc4	se
výraz	výraz	k1gInSc1	výraz
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
osmé	osmý	k4xOgNnSc1	osmý
nadzemní	nadzemní	k2eAgNnSc1d1	nadzemní
podlaží	podlaží	k1gNnSc1	podlaží
je	být	k5eAaImIp3nS	být
ustoupené	ustoupený	k2eAgNnSc1d1	ustoupené
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
tímto	tento	k3xDgNnSc7	tento
podlažím	podlaží	k1gNnSc7	podlaží
je	být	k5eAaImIp3nS	být
vyložena	vyložen	k2eAgFnSc1d1	vyložena
střecha	střecha	k1gFnSc1	střecha
s	s	k7c7	s
hliníkovým	hliníkový	k2eAgInSc7d1	hliníkový
obkladem	obklad	k1gInSc7	obklad
<g/>
.	.	kIx.	.
</s>
<s>
Nepřehlédnutelným	přehlédnutelný	k2eNgMnSc7d1	nepřehlédnutelný
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
šilt	šilt	k1gInSc1	šilt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
překlenul	překlenout	k5eAaPmAgInS	překlenout
schody	schod	k1gInPc4	schod
mezi	mezi	k7c7	mezi
Hodžova	Hodžův	k2eAgFnSc1d1	Hodžova
náměstím	náměstí	k1gNnSc7	náměstí
a	a	k8xC	a
Poštovní	poštovní	k2eAgFnSc1d1	poštovní
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Má	mít	k5eAaImIp3nS	mít
snahu	snaha	k1gFnSc4	snaha
protáhnout	protáhnout	k5eAaPmF	protáhnout
dům	dům	k1gInSc4	dům
v	v	k7c6	v
uliční	uliční	k2eAgFnSc6d1	uliční
frontě	fronta	k1gFnSc6	fronta
před	před	k7c4	před
hotel	hotel	k1gInSc4	hotel
Forum	forum	k1gNnSc1	forum
(	(	kIx(	(
<g/>
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
vlastně	vlastně	k9	vlastně
hotel	hotel	k1gInSc4	hotel
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
na	na	k7c4	na
uliční	uliční	k2eAgFnSc4d1	uliční
čáru	čára	k1gFnSc4	čára
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
i	i	k9	i
z	z	k7c2	z
urbanistického	urbanistický	k2eAgNnSc2d1	Urbanistické
hlediska	hledisko	k1gNnSc2	hledisko
patří	patřit	k5eAaImIp3nS	patřit
<g/>
)	)	kIx)	)
a	a	k8xC	a
napojit	napojit	k5eAaPmF	napojit
se	se	k3xPyFc4	se
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
tam	tam	k6eAd1	tam
bude	být	k5eAaImBp3nS	být
vznikat	vznikat	k5eAaImF	vznikat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
ulice	ulice	k1gFnPc1	ulice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
na	na	k7c4	na
Markův	Markův	k2eAgInSc4d1	Markův
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
takové	takový	k3xDgNnSc1	takový
pomyslné	pomyslný	k2eAgNnSc1d1	pomyslné
propojení	propojení	k1gNnSc1	propojení
<g/>
.	.	kIx.	.
</s>
<s>
Kšilt	kšilt	k1gInSc1	kšilt
mluví	mluvit	k5eAaImIp3nS	mluvit
také	také	k9	také
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Překrývá	překrývat	k5eAaImIp3nS	překrývat
hlavu	hlava	k1gFnSc4	hlava
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
za	za	k7c7	za
hradem	hrad	k1gInSc7	hrad
a	a	k8xC	a
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
jsou	být	k5eAaImIp3nP	být
vynikající	vynikající	k2eAgInPc1d1	vynikající
výhledy	výhled	k1gInPc1	výhled
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
sedí	sedit	k5eAaImIp3nS	sedit
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
vedoucích	vedoucí	k2eAgFnPc6d1	vedoucí
pozicích	pozice	k1gFnPc6	pozice
Tatrabanky	Tatrabanka	k1gFnSc2	Tatrabanka
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
část	část	k1gFnSc1	část
'	'	kIx"	'
<g/>
objektu	objekt	k1gInSc2	objekt
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
ulice	ulice	k1gFnSc2	ulice
má	mít	k5eAaImIp3nS	mít
samostatné	samostatný	k2eAgInPc4d1	samostatný
okenní	okenní	k2eAgInPc4d1	okenní
otvory	otvor	k1gInPc4	otvor
s	s	k7c7	s
okenním	okenní	k2eAgInSc7d1	okenní
pásem	pás	k1gInSc7	pás
a	a	k8xC	a
nadpražím	nadpraží	k1gNnSc7	nadpraží
v	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
nadzemním	nadzemní	k2eAgNnSc6d1	nadzemní
podlaží	podlaží	k1gNnSc6	podlaží
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
optického	optický	k2eAgNnSc2d1	optické
vylehčení	vylehčení	k1gNnSc2	vylehčení
a	a	k8xC	a
snížení	snížení	k1gNnSc2	snížení
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
navržena	navržen	k2eAgFnSc1d1	navržena
omítková	omítkový	k2eAgFnSc1d1	omítková
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
nejkompromisnejší	jkompromisnejší	k2eNgFnSc1d1	jkompromisnejší
tvář	tvář	k1gFnSc1	tvář
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Třípodlažní	třípodlažní	k2eAgFnSc1d1	třípodlažní
hmota	hmota	k1gFnSc1	hmota
do	do	k7c2	do
Náměstí	náměstí	k1gNnPc2	náměstí
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
cituje	citovat	k5eAaBmIp3nS	citovat
tektoniku	tektonika	k1gFnSc4	tektonika
otvorů	otvor	k1gInPc2	otvor
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
obložena	obložit	k5eAaPmNgFnS	obložit
hliníkovými	hliníkový	k2eAgInPc7d1	hliníkový
plechy	plech	k1gInPc7	plech
z	z	k7c2	z
ALUCOBONDu	ALUCOBONDus	k1gInSc2	ALUCOBONDus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dispoziční-provozní	Dispozičnírovozní	k2eAgNnPc4d1	Dispoziční-provozní
řešení	řešení	k1gNnPc4	řešení
==	==	k?	==
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
dispoziční	dispoziční	k2eAgFnSc1d1	dispoziční
koncepce	koncepce	k1gFnSc1	koncepce
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
řešena	řešit	k5eAaImNgFnS	řešit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
dvou	dva	k4xCgInPc2	dva
základních	základní	k2eAgInPc2d1	základní
provozů	provoz	k1gInPc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgInPc1d1	obchodní
provozy	provoz	k1gInPc1	provoz
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
parterových	parterový	k2eAgNnPc6d1	parterový
podlažích	podlaží	k1gNnPc6	podlaží
-	-	kIx~	-
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
NP	NP	kA	NP
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
NP	NP	kA	NP
s	s	k7c7	s
přímými	přímý	k2eAgInPc7d1	přímý
vstupy	vstup	k1gInPc7	vstup
z	z	k7c2	z
Hodžovo	Hodžův	k2eAgNnSc1d1	Hodžovo
náměstí	náměstí	k1gNnSc4	náměstí
a	a	k8xC	a
z	z	k7c2	z
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
úrovně	úroveň	k1gFnPc1	úroveň
jsou	být	k5eAaImIp3nP	být
propojeny	propojit	k5eAaPmNgFnP	propojit
pohyblivými	pohyblivý	k2eAgNnPc7d1	pohyblivé
schodišti	schodiště	k1gNnPc7	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
pasáž	pasáž	k1gFnSc1	pasáž
je	být	k5eAaImIp3nS	být
napojena	napojit	k5eAaPmNgFnS	napojit
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
vertikálními	vertikální	k2eAgFnPc7d1	vertikální
komunikacemi	komunikace	k1gFnPc7	komunikace
na	na	k7c4	na
parkovací	parkovací	k2eAgNnPc4d1	parkovací
místa	místo	k1gNnPc4	místo
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
podzemních	podzemní	k2eAgNnPc6d1	podzemní
podlažích	podlaží	k1gNnPc6	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obchodní	obchodní	k2eAgFnSc6d1	obchodní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
supermarket	supermarket	k1gInSc1	supermarket
s	s	k7c7	s
přímým	přímý	k2eAgNnSc7d1	přímé
napojením	napojení	k1gNnSc7	napojení
na	na	k7c6	na
zásobování	zásobování	k1gNnSc6	zásobování
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
provozy	provoz	k1gInPc1	provoz
<g/>
,	,	kIx,	,
nenáročné	náročný	k2eNgNnSc1d1	nenáročné
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
zboží	zboží	k1gNnSc2	zboží
jsou	být	k5eAaImIp3nP	být
zásobovány	zásobovat	k5eAaImNgFnP	zásobovat
z	z	k7c2	z
pasáže	pasáž	k1gFnSc2	pasáž
v	v	k7c6	v
přesném	přesný	k2eAgInSc6d1	přesný
provozním	provozní	k2eAgInSc6d1	provozní
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
pasáže	pasáž	k1gFnSc2	pasáž
je	být	k5eAaImIp3nS	být
i	i	k9	i
malá	malý	k2eAgFnSc1d1	malá
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
parterech	parter	k1gInPc6	parter
čela	čelo	k1gNnSc2	čelo
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
malá	malý	k2eAgFnSc1d1	malá
expozitura	expozitura	k1gFnSc1	expozitura
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
přístupná	přístupný	k2eAgFnSc1d1	přístupná
i	i	k9	i
z	z	k7c2	z
pasáže	pasáž	k1gFnSc2	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
administrativní	administrativní	k2eAgFnSc2d1	administrativní
části	část	k1gFnSc2	část
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Hodžova	Hodžův	k2eAgNnSc2d1	Hodžovo
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
vstupní	vstupní	k2eAgFnSc1d1	vstupní
hala	hala	k1gFnSc1	hala
přímo	přímo	k6eAd1	přímo
navazující	navazující	k2eAgFnSc1d1	navazující
na	na	k7c4	na
rychlovýtahy	rychlovýtah	k1gInPc4	rychlovýtah
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
výtahů	výtah	k1gInPc2	výtah
je	být	k5eAaImIp3nS	být
propojen	propojit	k5eAaPmNgInS	propojit
do	do	k7c2	do
suterénních	suterénní	k2eAgNnPc2d1	suterénní
parkovacích	parkovací	k2eAgNnPc2d1	parkovací
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
,	,	kIx,	,
poskytujících	poskytující	k2eAgInPc2d1	poskytující
vysoký	vysoký	k2eAgInSc4d1	vysoký
stupeň	stupeň	k1gInSc4	stupeň
pohodlí	pohodlí	k1gNnSc2	pohodlí
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Parkoviště	parkoviště	k1gNnSc1	parkoviště
je	být	k5eAaImIp3nS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
nejmodernějším	moderní	k2eAgNnSc7d3	nejmodernější
technickým	technický	k2eAgNnSc7d1	technické
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
mycím	mycí	k2eAgInSc7d1	mycí
boxem	box	k1gInSc7	box
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nepřetržitě	přetržitě	k6eNd1	přetržitě
je	být	k5eAaImIp3nS	být
monitorováno	monitorovat	k5eAaImNgNnS	monitorovat
bezpečnostním	bezpečnostní	k2eAgInSc7d1	bezpečnostní
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
ulice	ulice	k1gFnSc2	ulice
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
restaurace	restaurace	k1gFnSc1	restaurace
a	a	k8xC	a
jídelna	jídelna	k1gFnSc1	jídelna
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
typických	typický	k2eAgNnPc6d1	typické
podlažích	podlaží	k1gNnPc6	podlaží
je	být	k5eAaImIp3nS	být
navržen	navržen	k2eAgInSc4d1	navržen
dispoziční	dispoziční	k2eAgInSc4d1	dispoziční
pětkrát	pětkrát	k6eAd1	pětkrát
administrativy	administrativa	k1gFnSc2	administrativa
nejstarší	starý	k2eAgFnSc2d3	nejstarší
domácí	domácí	k2eAgInSc4d1	domácí
-	-	kIx~	-
slovenské	slovenský	k2eAgFnPc1d1	slovenská
banky	banka	k1gFnPc1	banka
s	s	k7c7	s
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
obslužnými	obslužný	k2eAgInPc7d1	obslužný
prostory	prostor	k1gInPc7	prostor
-	-	kIx~	-
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
,	,	kIx,	,
kuchyňky	kuchyňka	k1gFnPc1	kuchyňka
<g/>
,	,	kIx,	,
sklady	sklad	k1gInPc1	sklad
<g/>
,	,	kIx,	,
archivy	archiv	k1gInPc1	archiv
a	a	k8xC	a
malé	malý	k2eAgInPc1d1	malý
jednací	jednací	k2eAgInPc1d1	jednací
boxy	box	k1gInPc1	box
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
pracovní	pracovní	k2eAgNnPc4d1	pracovní
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
750	[number]	k4	750
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
objektu	objekt	k1gInSc2	objekt
(	(	kIx(	(
<g/>
v	v	k7c6	v
"	"	kIx"	"
<g/>
hlavě	hlava	k1gFnSc6	hlava
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgFnPc4d1	umístěna
kanceláře	kancelář	k1gFnPc4	kancelář
vedení	vedení	k1gNnSc2	vedení
se	s	k7c7	s
sekretariátem	sekretariát	k1gInSc7	sekretariát
a	a	k8xC	a
zasedací	zasedací	k2eAgFnPc4d1	zasedací
místnosti	místnost	k1gFnPc4	místnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
stavby	stavba	k1gFnSc2	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
stavby	stavba	k1gFnSc2	stavba
<g/>
:	:	kIx,	:
TATRACENTRUM	TATRACENTRUM	kA	TATRACENTRUM
<g/>
,	,	kIx,	,
Polyfunkční	polyfunkční	k2eAgInSc1d1	polyfunkční
objekt	objekt	k1gInSc1	objekt
</s>
</p>
<p>
<s>
Místo	místo	k7c2	místo
stavby	stavba	k1gFnSc2	stavba
<g/>
:	:	kIx,	:
Hodžovo	Hodžův	k2eAgNnSc1d1	Hodžovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
</s>
</p>
<p>
<s>
Autoři	autor	k1gMnPc1	autor
stavby	stavba	k1gFnSc2	stavba
<g/>
:	:	kIx,	:
Radomil	Radomil	k1gMnSc1	Radomil
Kachlík	Kachlík	k1gMnSc1	Kachlík
<g/>
,	,	kIx,	,
Matej	Matej	k1gInSc1	Matej
Siebert	Siebert	k1gInSc1	Siebert
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Vavrica	Vavrica	k1gMnSc1	Vavrica
<g/>
,	,	kIx,	,
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Závodný	Závodný	k2eAgMnSc1d1	Závodný
</s>
</p>
<p>
<s>
Začátek	začátek	k1gInSc1	začátek
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
:	:	kIx,	:
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Dokončení	dokončení	k1gNnSc1	dokončení
stavby	stavba	k1gFnSc2	stavba
<g/>
:	:	kIx,	:
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
plocha	plocha	k1gFnSc1	plocha
objektem	objekt	k1gInSc7	objekt
celkem	celkem	k6eAd1	celkem
<g/>
:	:	kIx,	:
3	[number]	k4	3
647,0	[number]	k4	647,0
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Užitná	užitný	k2eAgFnSc1d1	užitná
plocha	plocha	k1gFnSc1	plocha
celkem	celkem	k6eAd1	celkem
<g/>
:	:	kIx,	:
29	[number]	k4	29
925,8	[number]	k4	925,8
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Užitná	užitný	k2eAgFnSc1d1	užitná
plocha	plocha	k1gFnSc1	plocha
obchodní	obchodní	k2eAgFnSc1d1	obchodní
provozu	provoz	k1gInSc2	provoz
<g/>
:	:	kIx,	:
4	[number]	k4	4
499,8	[number]	k4	499,8
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Užitná	užitný	k2eAgFnSc1d1	užitná
plocha	plocha	k1gFnSc1	plocha
administrativa	administrativa	k1gFnSc1	administrativa
<g/>
:	:	kIx,	:
15	[number]	k4	15
653,5	[number]	k4	653,5
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Užitná	užitný	k2eAgFnSc1d1	užitná
plocha	plocha	k1gFnSc1	plocha
expozitura	expozitura	k1gFnSc1	expozitura
banky	banka	k1gFnSc2	banka
<g/>
:	:	kIx,	:
395,5	[number]	k4	395,5
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Užitná	užitný	k2eAgFnSc1d1	užitná
plocha	plocha	k1gFnSc1	plocha
parkoviště	parkoviště	k1gNnSc2	parkoviště
<g/>
:	:	kIx,	:
8	[number]	k4	8
0	[number]	k4	0
<g/>
12,9	[number]	k4	12,9
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Užitná	užitný	k2eAgFnSc1d1	užitná
plocha	plocha	k1gFnSc1	plocha
–	–	k?	–
technické	technický	k2eAgNnSc4d1	technické
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
:	:	kIx,	:
1	[number]	k4	1
364	[number]	k4	364
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
pracovníků	pracovník	k1gMnPc2	pracovník
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
:	:	kIx,	:
950	[number]	k4	950
pracovníků	pracovník	k1gMnPc2	pracovník
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
parkovacích	parkovací	k2eAgNnPc2d1	parkovací
míst	místo	k1gNnPc2	místo
<g/>
:	:	kIx,	:
celkem	celkem	k6eAd1	celkem
343	[number]	k4	343
parkovacích	parkovací	k2eAgNnPc2d1	parkovací
míst	místo	k1gNnPc2	místo
</s>
</p>
<p>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
administrativa	administrativa	k1gFnSc1	administrativa
<g/>
:	:	kIx,	:
127	[number]	k4	127
</s>
</p>
<p>
<s>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
<g/>
:	:	kIx,	:
216	[number]	k4	216
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
nadzemních	nadzemní	k2eAgNnPc2d1	nadzemní
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
:	:	kIx,	:
8	[number]	k4	8
podlaží	podlaží	k1gNnPc2	podlaží
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Tatracentrum	Tatracentrum	k1gNnSc4	Tatracentrum
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
Ľubomíra	Ľubomír	k1gMnSc2	Ľubomír
Závodního	závodní	k1gMnSc2	závodní
</s>
</p>
