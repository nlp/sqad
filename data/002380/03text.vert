<s>
Druhý	druhý	k4xOgInSc4	druhý
vatikánský	vatikánský	k2eAgInSc4d1	vatikánský
koncil	koncil	k1gInSc4	koncil
bylo	být	k5eAaImAgNnS	být
shromáždění	shromáždění	k1gNnSc1	shromáždění
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2500	[number]	k4	2500
katolických	katolický	k2eAgMnPc2d1	katolický
biskupů	biskup	k1gMnPc2	biskup
na	na	k7c6	na
dosud	dosud	k6eAd1	dosud
posledním	poslední	k2eAgInSc6d1	poslední
ekumenickém	ekumenický	k2eAgInSc6d1	ekumenický
koncilu	koncil	k1gInSc6	koncil
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
svolal	svolat	k5eAaPmAgMnS	svolat
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
a	a	k8xC	a
jež	jenž	k3xRgNnSc1	jenž
zasedalo	zasedat	k5eAaImAgNnS	zasedat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1962	[number]	k4	1962
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
koncilu	koncil	k1gInSc2	koncil
bylo	být	k5eAaImAgNnS	být
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
potřeby	potřeba	k1gFnPc4	potřeba
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Explicitně	explicitně	k6eAd1	explicitně
nedefinoval	definovat	k5eNaBmAgInS	definovat
žádnou	žádný	k3yNgFnSc4	žádný
novou	nový	k2eAgFnSc4d1	nová
nauku	nauka	k1gFnSc4	nauka
či	či	k8xC	či
dogmata	dogma	k1gNnPc4	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýraznější	výrazný	k2eAgNnPc4d3	nejvýraznější
teologická	teologický	k2eAgNnPc4d1	teologické
vyjádření	vyjádření	k1gNnPc4	vyjádření
koncilu	koncil	k1gInSc2	koncil
patří	patřit	k5eAaImIp3nS	patřit
potvrzení	potvrzení	k1gNnSc4	potvrzení
svátostné	svátostný	k2eAgFnSc2d1	svátostná
povahy	povaha	k1gFnSc2	povaha
biskupského	biskupský	k2eAgNnSc2d1	Biskupské
svěcení	svěcení	k1gNnSc2	svěcení
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
měl	mít	k5eAaImAgInS	mít
výrazný	výrazný	k2eAgInSc1d1	výrazný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
proměnu	proměna	k1gFnSc4	proměna
liturgie	liturgie	k1gFnSc2	liturgie
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tímto	tento	k3xDgInSc7	tento
koncilem	koncil	k1gInSc7	koncil
započala	započnout	k5eAaPmAgFnS	započnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
výrazný	výrazný	k2eAgInSc1d1	výrazný
posun	posun	k1gInSc1	posun
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
sama	sám	k3xTgMnSc4	sám
chápe	chápat	k5eAaImIp3nS	chápat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
ekleziologie	ekleziologie	k1gFnSc2	ekleziologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
římskokatolické	římskokatolický	k2eAgNnSc1d1	římskokatolické
a	a	k8xC	a
biblické	biblický	k2eAgNnSc1d1	biblické
učení	učení	k1gNnSc1	učení
začalo	začít	k5eAaPmAgNnS	začít
odchylovat	odchylovat	k5eAaImF	odchylovat
od	od	k7c2	od
neo-scholasticismu	neocholasticismus	k1gInSc2	neo-scholasticismus
a	a	k8xC	a
biblického	biblický	k2eAgInSc2d1	biblický
literalismu	literalismus	k1gInSc2	literalismus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
lpění	lpění	k1gNnSc1	lpění
na	na	k7c6	na
starých	starý	k2eAgNnPc6d1	staré
dogmatech	dogma	k1gNnPc6	dogma
a	a	k8xC	a
doslovném	doslovný	k2eAgNnSc6d1	doslovné
znění	znění	k1gNnSc6	znění
textu	text	k1gInSc2	text
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
konzervativní	konzervativní	k2eAgInPc4d1	konzervativní
přístupy	přístup	k1gInPc4	přístup
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
vyprovokovány	vyprovokován	k2eAgFnPc1d1	vyprovokována
modernismem	modernismus	k1gInSc7	modernismus
a	a	k8xC	a
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
se	se	k3xPyFc4	se
prosazovaly	prosazovat	k5eAaImAgFnP	prosazovat
od	od	k7c2	od
Prvního	první	k4xOgInSc2	první
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
liberálněji	liberálně	k6eAd2	liberálně
orientovaných	orientovaný	k2eAgMnPc2d1	orientovaný
teologů	teolog	k1gMnPc2	teolog
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Yves	Yves	k1gInSc1	Yves
Congar	Congar	k1gInSc1	Congar
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Rahner	Rahner	k1gMnSc1	Rahner
a	a	k8xC	a
John	John	k1gMnSc1	John
Courtney	Courtnea	k1gFnSc2	Courtnea
Murray	Murraa	k1gFnSc2	Murraa
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
směřovali	směřovat	k5eAaImAgMnP	směřovat
k	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
moderních	moderní	k2eAgInPc2d1	moderní
lidských	lidský	k2eAgInPc2d1	lidský
prožitků	prožitek	k1gInPc2	prožitek
s	s	k7c7	s
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
dogmatem	dogma	k1gNnSc7	dogma
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Josepha	Joseph	k1gMnSc2	Joseph
Ratzingera	Ratzinger	k1gMnSc2	Ratzinger
(	(	kIx(	(
<g/>
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Henriho	Henri	k1gMnSc2	Henri
de	de	k?	de
Lubac	Lubac	k1gInSc1	Lubac
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zase	zase	k9	zase
snažili	snažit	k5eAaImAgMnP	snažit
o	o	k7c4	o
obrodu	obroda	k1gFnSc4	obroda
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
lepšího	dobrý	k2eAgNnSc2d2	lepší
pochopení	pochopení	k1gNnSc2	pochopení
Písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
posvátných	posvátný	k2eAgInPc2d1	posvátný
textů	text	k1gInPc2	text
<g/>
)	)	kIx)	)
a	a	k8xC	a
raných	raný	k2eAgMnPc2d1	raný
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
církevních	církevní	k2eAgMnPc2d1	církevní
otců	otec	k1gMnPc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
biskupové	biskup	k1gMnPc1	biskup
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
čelili	čelit	k5eAaImAgMnP	čelit
velkým	velký	k2eAgFnPc3d1	velká
výzvám	výzva	k1gFnPc3	výzva
zapříčiněným	zapříčiněný	k2eAgFnPc3d1	zapříčiněná
politickými	politický	k2eAgFnPc7d1	politická
<g/>
,	,	kIx,	,
sociálními	sociální	k2eAgInPc7d1	sociální
<g/>
,	,	kIx,	,
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
a	a	k8xC	a
technickými	technický	k2eAgFnPc7d1	technická
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
úbytku	úbytek	k1gInSc3	úbytek
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
odmítání	odmítání	k1gNnSc3	odmítání
víry	víra	k1gFnSc2	víra
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
materialistických	materialistický	k2eAgFnPc2d1	materialistická
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
viděli	vidět	k5eAaImAgMnP	vidět
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
výzvy	výzva	k1gFnPc4	výzva
ve	v	k7c6	v
změnách	změna	k1gFnPc6	změna
struktury	struktura	k1gFnSc2	struktura
zvyklostí	zvyklost	k1gFnPc2	zvyklost
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejlépe	dobře	k6eAd3	dobře
organizované	organizovaný	k2eAgNnSc4d1	organizované
patřila	patřit	k5eAaImAgFnS	patřit
skupina	skupina	k1gFnSc1	skupina
nizozemských	nizozemský	k2eAgInPc2d1	nizozemský
a	a	k8xC	a
německých	německý	k2eAgInPc2d1	německý
biskupů	biskup	k1gInPc2	biskup
známá	známá	k1gFnSc1	známá
jako	jako	k8xC	jako
Rýnští	rýnský	k2eAgMnPc1d1	rýnský
biskupové	biskup	k1gMnPc1	biskup
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
téměř	téměř	k6eAd1	téměř
před	před	k7c7	před
stoletím	století	k1gNnSc7	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Prusko-francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
jen	jen	k9	jen
rozprava	rozprava	k1gFnSc1	rozprava
o	o	k7c4	o
roli	role	k1gFnSc4	role
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zkoumání	zkoumání	k1gNnSc3	zkoumání
pastoračních	pastorační	k2eAgFnPc2d1	pastorační
a	a	k8xC	a
dogmatických	dogmatický	k2eAgFnPc2d1	dogmatická
otázek	otázka	k1gFnPc2	otázka
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
celé	celý	k2eAgFnSc2d1	celá
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
dokončit	dokončit	k5eAaPmF	dokončit
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
pokročilém	pokročilý	k2eAgInSc6d1	pokročilý
věku	věk	k1gInSc6	věk
a	a	k8xC	a
špatném	špatný	k2eAgInSc6d1	špatný
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
vnímán	vnímat	k5eAaImNgInS	vnímat
spíše	spíše	k9	spíše
jako	jako	k8xC	jako
papež	papež	k1gMnSc1	papež
na	na	k7c4	na
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
necelé	celý	k2eNgInPc4d1	necelý
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
oznámil	oznámit	k5eAaPmAgInS	oznámit
úmysl	úmysl	k1gInSc1	úmysl
svolat	svolat	k5eAaPmF	svolat
další	další	k2eAgInSc4d1	další
koncil	koncil	k1gInSc4	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
příštích	příští	k2eAgNnPc2d1	příští
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgNnPc6d1	další
prohlášeních	prohlášení	k1gNnPc6	prohlášení
své	svůj	k3xOyFgInPc4	svůj
záměry	záměr	k1gInPc4	záměr
dále	daleko	k6eAd2	daleko
konkretizoval	konkretizovat	k5eAaBmAgInS	konkretizovat
<g/>
,	,	kIx,	,
až	až	k9	až
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
koncil	koncil	k1gInSc1	koncil
skutečně	skutečně	k6eAd1	skutečně
zahájen	zahájit	k5eAaPmNgInS	zahájit
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
příhod	příhoda	k1gFnPc2	příhoda
tradovaná	tradovaný	k2eAgFnSc1d1	tradovaná
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
úsilím	úsilí	k1gNnSc7	úsilí
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc4	jeho
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemu	co	k3yQnSc3	co
bude	být	k5eAaImBp3nS	být
další	další	k2eAgInSc1d1	další
koncil	koncil	k1gInSc1	koncil
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
okázale	okázale	k6eAd1	okázale
otevřel	otevřít	k5eAaPmAgMnS	otevřít
okno	okno	k1gNnSc4	okno
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
chci	chtít	k5eAaImIp1nS	chtít
nechat	nechat	k5eAaPmF	nechat
otevřená	otevřený	k2eAgNnPc4d1	otevřené
okna	okno	k1gNnPc4	okno
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
viděli	vidět	k5eAaImAgMnP	vidět
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
venku	venku	k6eAd1	venku
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
se	se	k3xPyFc4	se
zase	zase	k9	zase
mohli	moct	k5eAaImAgMnP	moct
dívat	dívat	k5eAaImF	dívat
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyzval	vyzvat	k5eAaPmAgInS	vyzvat
také	také	k9	také
další	další	k2eAgFnSc2d1	další
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
koncil	koncil	k1gInSc4	koncil
vyslaly	vyslat	k5eAaPmAgInP	vyslat
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
přistoupily	přistoupit	k5eAaPmAgFnP	přistoupit
protestantské	protestantský	k2eAgFnPc1d1	protestantská
i	i	k8xC	i
pravoslavné	pravoslavný	k2eAgFnPc1d1	pravoslavná
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
ruská	ruský	k2eAgFnSc1d1	ruská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
před	před	k7c7	před
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
vládou	vláda	k1gFnSc7	vláda
přijala	přijmout	k5eAaPmAgFnS	přijmout
pozvání	pozvání	k1gNnSc4	pozvání
až	až	k6eAd1	až
po	po	k7c4	po
ujištění	ujištění	k1gNnSc4	ujištění
o	o	k7c6	o
nepolitické	politický	k2eNgFnSc6d1	nepolitická
podstatě	podstata	k1gFnSc6	podstata
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
koncilu	koncil	k1gInSc3	koncil
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
Josef	Josef	k1gMnSc1	Josef
kardinál	kardinál	k1gMnSc1	kardinál
Beran	Beran	k1gMnSc1	Beran
(	(	kIx(	(
<g/>
jen	jen	k9	jen
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
zasedání	zasedání	k1gNnSc1	zasedání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Róbert	Róbert	k1gInSc1	Róbert
Pobožný	pobožný	k2eAgInSc1d1	pobožný
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
prvního	první	k4xOgNnSc2	první
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Nécsey	Nécsea	k1gFnSc2	Nécsea
<g/>
,	,	kIx,	,
Ambróz	Ambróza	k1gFnPc2	Ambróza
Lazík	Lazík	k1gMnSc1	Lazík
a	a	k8xC	a
František	František	k1gMnSc1	František
Tomášek	Tomášek	k1gMnSc1	Tomášek
(	(	kIx(	(
<g/>
všechna	všechen	k3xTgNnPc4	všechen
čtyři	čtyři	k4xCgNnPc4	čtyři
zasedání	zasedání	k1gNnPc4	zasedání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Merell	Merell	k1gMnSc1	Merell
a	a	k8xC	a
doprovod	doprovod	k1gInSc1	doprovod
složený	složený	k2eAgInSc1d1	složený
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
ze	z	k7c2	z
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
exponentů	exponent	k1gMnPc2	exponent
prorežimního	prorežimní	k2eAgInSc2d1	prorežimní
Celostátního	celostátní	k2eAgInSc2d1	celostátní
mírového	mírový	k2eAgInSc2d1	mírový
výboru	výbor	k1gInSc2	výbor
katolického	katolický	k2eAgNnSc2d1	katolické
duchovenstva	duchovenstvo	k1gNnSc2	duchovenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenkou	myšlenka	k1gFnSc7	myšlenka
dokončení	dokončení	k1gNnSc2	dokončení
I.	I.	kA	I.
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
již	již	k6eAd1	již
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
předpokládaným	předpokládaný	k2eAgFnPc3d1	předpokládaná
těžkostem	těžkost	k1gFnPc3	těžkost
se	se	k3xPyFc4	se
však	však	k9	však
této	tento	k3xDgFnSc2	tento
myšlenky	myšlenka	k1gFnSc2	myšlenka
brzy	brzy	k6eAd1	brzy
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
Pius	Pius	k1gMnSc1	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
však	však	k9	však
měl	mít	k5eAaImAgInS	mít
touhu	touha	k1gFnSc4	touha
koncil	koncil	k1gInSc1	koncil
svolat	svolat	k5eAaPmF	svolat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
uložil	uložit	k5eAaPmAgInS	uložit
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
přípravnými	přípravný	k2eAgFnPc7d1	přípravná
pracemi	práce	k1gFnPc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
encykliku	encyklika	k1gFnSc4	encyklika
Humani	Humaň	k1gFnSc3	Humaň
generis	generis	k1gFnSc3	generis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
vypořádal	vypořádat	k5eAaPmAgMnS	vypořádat
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
teologickými	teologický	k2eAgInPc7d1	teologický
směry	směr	k1gInPc7	směr
uplynulých	uplynulý	k2eAgNnPc2d1	uplynulé
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
vložil	vložit	k5eAaPmAgMnS	vložit
i	i	k9	i
paragraf	paragraf	k1gInSc4	paragraf
o	o	k7c4	o
závaznosti	závaznost	k1gFnPc4	závaznost
stanovisek	stanovisko	k1gNnPc2	stanovisko
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
v	v	k7c6	v
encyklikách	encyklika	k1gFnPc6	encyklika
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
i	i	k9	i
dogma	dogma	k1gNnSc4	dogma
o	o	k7c4	o
Nanebevzetí	nanebevzetí	k1gNnSc4	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
chystal	chystat	k5eAaImAgMnS	chystat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
měl	mít	k5eAaImAgInS	mít
kodifikovat	kodifikovat	k5eAaBmF	kodifikovat
myšlenky	myšlenka	k1gFnPc4	myšlenka
Humani	Humaň	k1gFnSc3	Humaň
generis	generis	k1gFnSc2	generis
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
by	by	kYmCp3nP	by
skončil	skončit	k5eAaPmAgInS	skončit
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
mariánského	mariánský	k2eAgNnSc2d1	Mariánské
dogmatu	dogma	k1gNnSc2	dogma
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Pius	Pius	k1gMnSc1	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
očekávaných	očekávaný	k2eAgFnPc2d1	očekávaná
těžkostí	těžkost	k1gFnPc2	těžkost
přípravy	příprava	k1gFnSc2	příprava
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
údajně	údajně	k6eAd1	údajně
ještě	ještě	k6eAd1	ještě
naznačil	naznačit	k5eAaPmAgInS	naznačit
jezuitskému	jezuitský	k2eAgMnSc3d1	jezuitský
kazateli	kazatel	k1gMnSc3	kazatel
Riccardu	Riccard	k1gMnSc3	Riccard
Lombardimu	Lombardi	k1gMnSc3	Lombardi
<g/>
,	,	kIx,	,
že	že	k8xS	že
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncil	koncil	k1gInSc1	koncil
svolá	svolat	k5eAaPmIp3nS	svolat
jeho	jeho	k3xOp3gMnSc2	jeho
nástupce	nástupce	k1gMnSc2	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
zaznělo	zaznít	k5eAaPmAgNnS	zaznít
slovo	slovo	k1gNnSc1	slovo
koncil	koncil	k1gInSc1	koncil
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
dni	den	k1gInSc6	den
konkláve	konkláve	k1gNnSc2	konkláve
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
papeže	papež	k1gMnSc4	papež
Jana	Jan	k1gMnSc4	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
kardinála	kardinál	k1gMnSc2	kardinál
Alfreda	Alfred	k1gMnSc2	Alfred
Ottaviana	Ottavian	k1gMnSc2	Ottavian
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
se	s	k7c7	s
s	s	k7c7	s
přípravnými	přípravný	k2eAgInPc7d1	přípravný
dokumenty	dokument	k1gInPc7	dokument
sice	sice	k8xC	sice
seznámil	seznámit	k5eAaPmAgMnS	seznámit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
takovýto	takovýto	k3xDgInSc1	takovýto
koncil	koncil	k1gInSc1	koncil
by	by	kYmCp3nS	by
on	on	k3xPp3gMnSc1	on
nesvolával	svolávat	k5eNaImAgMnS	svolávat
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
proslovu	proslov	k1gInSc6	proslov
k	k	k7c3	k
benátským	benátský	k2eAgMnPc3d1	benátský
poutníkům	poutník	k1gMnPc3	poutník
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1962	[number]	k4	1962
a	a	k8xC	a
v	v	k7c6	v
deníkovém	deníkový	k2eAgInSc6d1	deníkový
záznamu	záznam	k1gInSc6	záznam
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
uváděl	uvádět	k5eAaImAgMnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c4	na
svolání	svolání	k1gNnSc4	svolání
koncilu	koncil	k1gInSc2	koncil
jej	on	k3xPp3gInSc4	on
napadla	napadnout	k5eAaPmAgFnS	napadnout
zcela	zcela	k6eAd1	zcela
spontánně	spontánně	k6eAd1	spontánně
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
necelé	celý	k2eNgInPc4d1	necelý
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
zvolení	zvolení	k1gNnSc6	zvolení
<g/>
)	)	kIx)	)
při	při	k7c6	při
ranním	ranní	k2eAgInSc6d1	ranní
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
kardinálem	kardinál	k1gMnSc7	kardinál
státním	státní	k2eAgMnSc7d1	státní
tajemníkem	tajemník	k1gMnSc7	tajemník
Tardinim	Tardinima	k1gFnPc2	Tardinima
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
papežova	papežův	k2eAgNnSc2d1	papežovo
líčení	líčení	k1gNnSc2	líčení
probírali	probírat	k5eAaImAgMnP	probírat
tehdy	tehdy	k6eAd1	tehdy
situaci	situace	k1gFnSc4	situace
církve	církev	k1gFnSc2	církev
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
uznávanou	uznávaný	k2eAgFnSc4d1	uznávaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neoblíbenou	oblíbený	k2eNgFnSc7d1	neoblíbená
a	a	k8xC	a
nechápanou	chápaný	k2eNgFnSc7d1	nechápaná
institucí	instituce	k1gFnSc7	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
třetina	třetina	k1gFnSc1	třetina
lidstva	lidstvo	k1gNnSc2	lidstvo
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
ateistických	ateistický	k2eAgInPc6d1	ateistický
režimech	režim	k1gInPc6	režim
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
tradičně	tradičně	k6eAd1	tradičně
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
zemích	zem	k1gFnPc6	zem
klesal	klesat	k5eAaImAgInS	klesat
počet	počet	k1gInSc1	počet
praktikujících	praktikující	k2eAgMnPc2d1	praktikující
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Náhle	náhle	k6eAd1	náhle
v	v	k7c4	v
Nás	my	k3xPp1nPc4	my
vytrysklo	vytrysknout	k5eAaPmAgNnS	vytrysknout
vnuknutí	vnuknutí	k1gNnSc3	vnuknutí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
když	když	k8xS	když
se	se	k3xPyFc4	se
květ	květ	k1gInSc1	květ
rozevře	rozevřít	k5eAaPmIp3nS	rozevřít
v	v	k7c6	v
nečekaném	čekaný	k2eNgNnSc6d1	nečekané
jaru	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
duše	duše	k1gFnSc1	duše
byla	být	k5eAaImAgFnS	být
osvícena	osvítit	k5eAaPmNgFnS	osvítit
velkou	velký	k2eAgFnSc7d1	velká
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
...	...	k?	...
Jedno	jeden	k4xCgNnSc1	jeden
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
a	a	k8xC	a
zavazující	zavazující	k2eAgFnSc1d1	zavazující
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
na	na	k7c6	na
našich	náš	k3xOp1gInPc6	náš
rtech	ret	k1gInPc6	ret
<g/>
.	.	kIx.	.
</s>
<s>
Náš	náš	k3xOp1gInSc1	náš
hlas	hlas	k1gInSc1	hlas
to	ten	k3xDgNnSc4	ten
poprvé	poprvé	k6eAd1	poprvé
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
-	-	kIx~	-
koncil	koncil	k1gInSc1	koncil
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Dále	daleko	k6eAd2	daleko
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
očekával	očekávat	k5eAaImAgMnS	očekávat
u	u	k7c2	u
Tardiniho	Tardini	k1gMnSc2	Tardini
zděšení	zděšení	k1gNnSc2	zděšení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
tváři	tvář	k1gFnSc6	tvář
ukázal	ukázat	k5eAaPmAgInS	ukázat
radostný	radostný	k2eAgInSc1d1	radostný
souhlas	souhlas	k1gInSc1	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historiků	historik	k1gMnPc2	historik
svědčí	svědčit	k5eAaImIp3nS	svědčit
toto	tento	k3xDgNnSc1	tento
líčení	líčení	k1gNnSc1	líčení
vyslovené	vyslovený	k2eAgNnSc1d1	vyslovené
rok	rok	k1gInSc4	rok
po	po	k7c4	po
Tardiniho	Tardini	k1gMnSc4	Tardini
úmrtí	úmrtí	k1gNnSc2	úmrtí
o	o	k7c6	o
papežově	papežův	k2eAgInSc6d1	papežův
taktu	takt	k1gInSc6	takt
a	a	k8xC	a
velkomyslnosti	velkomyslnost	k1gFnSc6	velkomyslnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Tardini	Tardin	k2eAgMnPc1d1	Tardin
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
snažil	snažit	k5eAaImAgInS	snažit
koncil	koncil	k1gInSc1	koncil
oddálit	oddálit	k5eAaPmF	oddálit
doufaje	doufat	k5eAaImSgInS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
papežova	papežův	k2eAgFnSc1d1	papežova
smrt	smrt	k1gFnSc1	smrt
vše	všechen	k3xTgNnSc4	všechen
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Veřejně	veřejně	k6eAd1	veřejně
byl	být	k5eAaImAgInS	být
koncil	koncil	k1gInSc1	koncil
prvně	prvně	k?	prvně
oznámen	oznámen	k2eAgInSc1d1	oznámen
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
zcela	zcela	k6eAd1	zcela
nečekaně	nečekaně	k6eAd1	nečekaně
přednesl	přednést	k5eAaPmAgMnS	přednést
svůj	svůj	k3xOyFgInSc4	svůj
úmysl	úmysl	k1gInSc4	úmysl
před	před	k7c7	před
kolegiem	kolegium	k1gNnSc7	kolegium
17	[number]	k4	17
kardinálů	kardinál	k1gMnPc2	kardinál
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
za	za	k7c7	za
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
koncil	koncil	k1gInSc4	koncil
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
-	-	kIx~	-
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
obnova	obnova	k1gFnSc1	obnova
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
srozumitelnost	srozumitelnost	k1gFnSc1	srozumitelnost
poselství	poselství	k1gNnSc2	poselství
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
posílení	posílení	k1gNnSc3	posílení
svazků	svazek	k1gInPc2	svazek
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgInS	mít
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
koncil	koncil	k1gInSc4	koncil
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
změní	změnit	k5eAaPmIp3nS	změnit
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
znovu	znovu	k6eAd1	znovu
měla	mít	k5eAaImAgFnS	mít
co	co	k3yRnSc4	co
říci	říct	k5eAaPmF	říct
současnému	současný	k2eAgInSc3d1	současný
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
církve	církev	k1gFnSc2	církev
označil	označit	k5eAaPmAgMnS	označit
Jan	Jan	k1gMnSc1	Jan
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
Aggiornamento	Aggiornamento	k1gNnSc1	Aggiornamento
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
italský	italský	k2eAgInSc1d1	italský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
uvedení	uvedení	k1gNnSc4	uvedení
účetních	účetní	k2eAgFnPc2d1	účetní
knih	kniha	k1gFnPc2	kniha
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
odpovídajícího	odpovídající	k2eAgInSc2d1	odpovídající
aktuálnímu	aktuální	k2eAgNnSc3d1	aktuální
datu	datum	k1gNnSc3	datum
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
svolání	svolání	k1gNnSc6	svolání
koncilu	koncil	k1gInSc2	koncil
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
přijata	přijmout	k5eAaPmNgFnS	přijmout
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pozorností	pozornost	k1gFnSc7	pozornost
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
menším	malý	k2eAgNnSc7d2	menší
nadšením	nadšení	k1gNnSc7	nadšení
ji	on	k3xPp3gFnSc4	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
kuriální	kuriální	k2eAgMnPc1d1	kuriální
kardinálové	kardinál	k1gMnPc1	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nepodporovali	podporovat	k5eNaImAgMnP	podporovat
ani	ani	k8xC	ani
některé	některý	k3yIgFnPc1	některý
modernizační	modernizační	k2eAgFnPc1d1	modernizační
tendence	tendence	k1gFnPc1	tendence
za	za	k7c2	za
papežů	papež	k1gMnPc2	papež
Pia	Pius	k1gMnSc2	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
a	a	k8xC	a
Pia	Pius	k1gMnSc4	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
vážně	vážně	k6eAd1	vážně
obávali	obávat	k5eAaImAgMnP	obávat
o	o	k7c4	o
oslabení	oslabení	k1gNnSc4	oslabení
nejen	nejen	k6eAd1	nejen
své	svůj	k3xOyFgFnPc4	svůj
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
církevního	církevní	k2eAgNnSc2d1	církevní
učení	učení	k1gNnSc2	učení
jeho	jeho	k3xOp3gNnSc7	jeho
přizpůsobením	přizpůsobení	k1gNnSc7	přizpůsobení
duchu	duch	k1gMnSc3	duch
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
o	o	k7c4	o
maximální	maximální	k2eAgInSc4d1	maximální
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
plánování	plánování	k1gNnSc4	plánování
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc4	obsah
i	i	k8xC	i
dokumenty	dokument	k1gInPc4	dokument
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Janu	Jana	k1gFnSc4	Jana
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
koncilu	koncil	k1gInSc2	koncil
77	[number]	k4	77
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
především	především	k6eAd1	především
o	o	k7c4	o
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
apoštolskou	apoštolský	k2eAgFnSc7d1	apoštolská
konstitucí	konstituce	k1gFnSc7	konstituce
Humanae	Humana	k1gFnSc2	Humana
salutis	salutis	k1gFnSc2	salutis
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1961	[number]	k4	1961
oficiálně	oficiálně	k6eAd1	oficiálně
svolat	svolat	k5eAaPmF	svolat
koncil	koncil	k1gInSc4	koncil
na	na	k7c4	na
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1959	[number]	k4	1959
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
sešla	sejít	k5eAaPmAgFnS	sejít
Komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
zahájení	zahájení	k1gNnSc4	zahájení
příprav	příprava	k1gFnPc2	příprava
<g/>
,	,	kIx,	,
Commissio	Commissio	k6eAd1	Commissio
antepraeparatoria	antepraeparatorium	k1gNnSc2	antepraeparatorium
<g/>
.	.	kIx.	.
</s>
<s>
Komisi	komise	k1gFnSc3	komise
předsedal	předsedat	k5eAaImAgMnS	předsedat
kardinál	kardinál	k1gMnSc1	kardinál
státní	státní	k2eAgMnSc1d1	státní
sekretář	sekretář	k1gMnSc1	sekretář
Domenico	Domenico	k6eAd1	Domenico
Tardini	Tardin	k2eAgMnPc1d1	Tardin
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
3500	[number]	k4	3500
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgMnPc4d1	církevní
představené	představená	k1gFnSc2	představená
a	a	k8xC	a
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
předložili	předložit	k5eAaPmAgMnP	předložit
návrhy	návrh	k1gInPc4	návrh
programu	program	k1gInSc2	program
jednání	jednání	k1gNnPc2	jednání
přípravných	přípravný	k2eAgFnPc2d1	přípravná
komisí	komise	k1gFnPc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
obdržela	obdržet	k5eAaPmAgFnS	obdržet
2812	[number]	k4	2812
postulátů	postulát	k1gInPc2	postulát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
přečteny	přečíst	k5eAaPmNgInP	přečíst
a	a	k8xC	a
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
začala	začít	k5eAaPmAgFnS	začít
vlastní	vlastní	k2eAgFnSc1d1	vlastní
přípravná	přípravný	k2eAgFnSc1d1	přípravná
fáze	fáze	k1gFnSc1	fáze
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
zahájení	zahájení	k1gNnSc3	zahájení
dal	dát	k5eAaPmAgMnS	dát
pokyn	pokyn	k1gInSc4	pokyn
papež	papež	k1gMnSc1	papež
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1960	[number]	k4	1960
svým	svůj	k3xOyFgInSc7	svůj
motu	moto	k1gNnSc3	moto
proprio	propria	k1gFnSc5	propria
Superno	Superna	k1gFnSc5	Superna
Dei	Dei	k1gFnSc6	Dei
nutu	nutus	k1gInSc6	nutus
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
deset	deset	k4xCc1	deset
přípravných	přípravný	k2eAgFnPc2d1	přípravná
komisí	komise	k1gFnPc2	komise
(	(	kIx(	(
<g/>
Commissiones	Commissiones	k1gInSc1	Commissiones
praeparatoriae	praeparatoria	k1gInSc2	praeparatoria
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgInPc2d1	předchozí
koncilů	koncil	k1gInPc2	koncil
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
přípravné	přípravný	k2eAgFnPc1d1	přípravná
komise	komise	k1gFnPc1	komise
tvořeny	tvořit	k5eAaImNgFnP	tvořit
převážně	převážně	k6eAd1	převážně
teology	teolog	k1gMnPc4	teolog
a	a	k8xC	a
kanonisty	kanonista	k1gMnPc4	kanonista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
neměli	mít	k5eNaImAgMnP	mít
žádné	žádný	k3yNgNnSc4	žádný
hlasovací	hlasovací	k2eAgNnSc4d1	hlasovací
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
2	[number]	k4	2
<g/>
.	.	kIx.	.
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
tvořili	tvořit	k5eAaImAgMnP	tvořit
komise	komise	k1gFnPc4	komise
asi	asi	k9	asi
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
biskupové	biskup	k1gMnPc1	biskup
a	a	k8xC	a
představení	představení	k1gNnSc1	představení
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
lidi	člověk	k1gMnPc4	člověk
blízké	blízký	k2eAgFnPc4d1	blízká
kurii	kurie	k1gFnSc4	kurie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
získat	získat	k5eAaPmF	získat
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
průběh	průběh	k1gInSc4	průběh
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
tak	tak	k6eAd1	tak
při	při	k7c6	při
prosazování	prosazování	k1gNnSc3	prosazování
svých	svůj	k3xOyFgNnPc2	svůj
témat	téma	k1gNnPc2	téma
pro	pro	k7c4	pro
jednání	jednání	k1gNnSc4	jednání
koncilu	koncil	k1gInSc2	koncil
narážel	narážet	k5eAaPmAgInS	narážet
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
u	u	k7c2	u
ekumenismu	ekumenismus	k1gInSc2	ekumenismus
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
ale	ale	k9	ale
již	již	k6eAd1	již
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
rozproudil	rozproudit	k5eAaPmAgMnS	rozproudit
dialog	dialog	k1gInSc4	dialog
o	o	k7c6	o
náplni	náplň	k1gFnSc6	náplň
chystaného	chystaný	k2eAgInSc2d1	chystaný
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
teolog	teolog	k1gMnSc1	teolog
Hans	Hans	k1gMnSc1	Hans
Küng	Küng	k1gMnSc1	Küng
vyučující	vyučující	k2eAgMnSc1d1	vyučující
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Tübingenu	Tübingen	k1gInSc6	Tübingen
probíral	probírat	k5eAaImAgMnS	probírat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
Konzil	Konzil	k1gFnSc1	Konzil
und	und	k?	und
Wiedervereinigung	Wiedervereinigung	k1gInSc1	Wiedervereinigung
(	(	kIx(	(
<g/>
Koncil	koncil	k1gInSc1	koncil
a	a	k8xC	a
znovusjednocení	znovusjednocení	k1gNnSc1	znovusjednocení
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
reálné	reálný	k2eAgFnPc4d1	reálná
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
ekumenismus	ekumenismus	k1gInSc4	ekumenismus
<g/>
,	,	kIx,	,
reformu	reforma	k1gFnSc4	reforma
kurie	kurie	k1gFnSc2	kurie
<g/>
,	,	kIx,	,
mezináboženský	mezináboženský	k2eAgInSc4d1	mezináboženský
dialog	dialog	k1gInSc4	dialog
<g/>
,	,	kIx,	,
a	a	k8xC	a
zrušení	zrušení	k1gNnSc1	zrušení
Index	index	k1gInSc1	index
librorum	librorum	k1gNnSc1	librorum
prohibitorum	prohibitorum	k1gInSc1	prohibitorum
<g/>
.	.	kIx.	.
</s>
<s>
Shrnul	shrnout	k5eAaPmAgMnS	shrnout
tak	tak	k9	tak
cíle	cíl	k1gInPc1	cíl
mnoha	mnoho	k4c2	mnoho
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
teologů	teolog	k1gMnPc2	teolog
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
podporovány	podporovat	k5eAaImNgFnP	podporovat
i	i	k9	i
mnoha	mnoho	k4c7	mnoho
laiky	laik	k1gMnPc7	laik
a	a	k8xC	a
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
nakonec	nakonec	k6eAd1	nakonec
i	i	k8xC	i
částečně	částečně	k6eAd1	částečně
dosaženy	dosažen	k2eAgFnPc1d1	dosažena
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vyvážení	vyvážení	k1gNnSc4	vyvážení
vlivu	vliv	k1gInSc2	vliv
kurie	kurie	k1gFnSc2	kurie
v	v	k7c6	v
přípravách	příprava	k1gFnPc6	příprava
koncilu	koncil	k1gInSc2	koncil
papež	papež	k1gMnSc1	papež
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
sekretariát	sekretariát	k1gInSc1	sekretariát
pro	pro	k7c4	pro
jednotu	jednota	k1gFnSc4	jednota
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stanul	stanout	k5eAaPmAgMnS	stanout
německý	německý	k2eAgMnSc1d1	německý
kardinál	kardinál	k1gMnSc1	kardinál
Augustin	Augustin	k1gMnSc1	Augustin
Bea	Bea	k1gMnSc1	Bea
SJ	SJ	kA	SJ
<g/>
,	,	kIx,	,
jmenován	jmenován	k2eAgMnSc1d1	jmenován
bývalý	bývalý	k2eAgMnSc1d1	bývalý
zpovědník	zpovědník	k1gMnSc1	zpovědník
Pia	Pius	k1gMnSc2	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sekretariát	sekretariát	k1gInSc1	sekretariát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyrovnával	vyrovnávat	k5eAaImAgInS	vyrovnávat
vliv	vliv	k1gInSc4	vliv
komisí	komise	k1gFnPc2	komise
ovlivněných	ovlivněný	k2eAgFnPc2d1	ovlivněná
kurií	kurie	k1gFnPc2	kurie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nadále	nadále	k6eAd1	nadále
odpovědný	odpovědný	k2eAgInSc1d1	odpovědný
za	za	k7c4	za
otázky	otázka	k1gFnPc4	otázka
ekumenismu	ekumenismus	k1gInSc2	ekumenismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představitelé	představitel	k1gMnPc1	představitel
ostatních	ostatní	k2eAgFnPc2d1	ostatní
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
církví	církev	k1gFnPc2	církev
uvítali	uvítat	k5eAaPmAgMnP	uvítat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
přístup	přístup	k1gInSc1	přístup
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
kurie	kurie	k1gFnSc2	kurie
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
již	již	k6eAd1	již
poněkud	poněkud	k6eAd1	poněkud
znelíbil	znelíbit	k5eAaPmAgMnS	znelíbit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1961	[number]	k4	1961
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
přípravy	příprava	k1gFnPc4	příprava
konečné	konečný	k2eAgFnSc3d1	konečná
fázi	fáze	k1gFnSc3	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
ještě	ještě	k6eAd1	ještě
vyřešit	vyřešit	k5eAaPmF	vyřešit
organizační	organizační	k2eAgFnPc4d1	organizační
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
jak	jak	k6eAd1	jak
velký	velký	k2eAgInSc4d1	velký
okruh	okruh	k1gInSc4	okruh
účastníků	účastník	k1gMnPc2	účastník
přizvat	přizvat	k5eAaPmF	přizvat
<g/>
.	.	kIx.	.
</s>
<s>
Pevně	pevně	k6eAd1	pevně
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedinou	jediný	k2eAgFnSc7d1	jediná
jednací	jednací	k2eAgFnSc7d1	jednací
řečí	řeč	k1gFnSc7	řeč
bude	být	k5eAaImBp3nS	být
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
znalosti	znalost	k1gFnPc1	znalost
této	tento	k3xDgFnSc2	tento
řeči	řeč	k1gFnSc2	řeč
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
episkopátu	episkopát	k1gInSc2	episkopát
"	"	kIx"	"
<g/>
poněkud	poněkud	k6eAd1	poněkud
slabší	slabý	k2eAgInPc1d2	slabší
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1962	[number]	k4	1962
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
69	[number]	k4	69
předloh	předloha	k1gFnPc2	předloha
zahrnujících	zahrnující	k2eAgFnPc2d1	zahrnující
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
byly	být	k5eAaImAgFnP	být
napsány	napsat	k5eAaPmNgFnP	napsat
velmi	velmi	k6eAd1	velmi
rozvláčným	rozvláčný	k2eAgInSc7d1	rozvláčný
stylem	styl	k1gInSc7	styl
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
encyklik	encyklika	k1gFnPc2	encyklika
předchozích	předchozí	k2eAgMnPc2d1	předchozí
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
liturgická	liturgický	k2eAgFnSc1d1	liturgická
komise	komise	k1gFnSc1	komise
předložila	předložit	k5eAaPmAgFnS	předložit
použitelný	použitelný	k2eAgInSc4d1	použitelný
koncept	koncept	k1gInSc4	koncept
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
základem	základ	k1gInSc7	základ
dalších	další	k2eAgNnPc2d1	další
jednání	jednání	k1gNnPc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
komise	komise	k1gFnPc1	komise
čekaly	čekat	k5eAaImAgFnP	čekat
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncil	koncil	k1gInSc1	koncil
bude	být	k5eAaImBp3nS	být
spíše	spíše	k9	spíše
zavazovat	zavazovat	k5eAaImF	zavazovat
a	a	k8xC	a
schraňovat	schraňovat	k5eAaImF	schraňovat
<g/>
,	,	kIx,	,
než	než	k8xS	než
obnovovat	obnovovat	k5eAaImF	obnovovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
ale	ale	k9	ale
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
především	především	k9	především
zástupci	zástupce	k1gMnPc1	zástupce
místních	místní	k2eAgFnPc2d1	místní
církví	církev	k1gFnPc2	církev
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ukázala	ukázat	k5eAaPmAgFnS	ukázat
ztráta	ztráta	k1gFnSc1	ztráta
vlivu	vliv	k1gInSc2	vliv
kurie	kurie	k1gFnSc2	kurie
vůči	vůči	k7c3	vůči
vlivu	vliv	k1gInSc3	vliv
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
zdržel	zdržet	k5eAaPmAgInS	zdržet
připomínek	připomínka	k1gFnPc2	připomínka
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
69	[number]	k4	69
navrženým	navržený	k2eAgFnPc3d1	navržená
předlohám	předloha	k1gFnPc3	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
on	on	k3xPp3gMnSc1	on
neměl	mít	k5eNaImAgMnS	mít
jasnou	jasný	k2eAgFnSc4d1	jasná
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
na	na	k7c4	na
co	co	k3yInSc4	co
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
koncil	koncil	k1gInSc1	koncil
zaměřit	zaměřit	k5eAaPmF	zaměřit
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
dát	dát	k5eAaPmF	dát
prostor	prostor	k1gInSc4	prostor
volným	volný	k2eAgInSc7d1	volný
a	a	k8xC	a
samostatným	samostatný	k2eAgNnSc7d1	samostatné
jednáním	jednání	k1gNnSc7	jednání
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
představách	představa	k1gFnPc6	představa
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
koncil	koncil	k1gInSc4	koncil
rychlý	rychlý	k2eAgInSc4d1	rychlý
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
vlastní	vlastní	k2eAgFnSc1d1	vlastní
dynamika	dynamika	k1gFnSc1	dynamika
koncilu	koncil	k1gInSc2	koncil
dostala	dostat	k5eAaPmAgFnS	dostat
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nakonec	nakonec	k6eAd1	nakonec
splnil	splnit	k5eAaPmAgMnS	splnit
jeho	jeho	k3xOp3gFnSc4	jeho
představu	představa	k1gFnSc4	představa
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
novými	nový	k2eAgFnPc7d1	nová
letnicemi	letnice	k1gFnPc7	letnice
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mohutném	mohutný	k2eAgNnSc6d1	mohutné
procesí	procesí	k1gNnSc6	procesí
vešlo	vejít	k5eAaPmAgNnS	vejít
2498	[number]	k4	2498
koncilních	koncilní	k2eAgMnPc2d1	koncilní
otců	otec	k1gMnPc2	otec
do	do	k7c2	do
svatopetrské	svatopetrský	k2eAgFnSc2d1	Svatopetrská
baziliky	bazilika	k1gFnSc2	bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
přitom	přitom	k6eAd1	přitom
neměl	mít	k5eNaImAgMnS	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
tiáru	tiára	k1gFnSc4	tiára
coby	coby	k?	coby
symbol	symbol	k1gInSc4	symbol
papežské	papežský	k2eAgFnSc2d1	Papežská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
mitru	mitra	k1gFnSc4	mitra
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zde	zde	k6eAd1	zde
nevystupoval	vystupovat	k5eNaImAgMnS	vystupovat
jako	jako	k8xC	jako
vládce	vládce	k1gMnPc4	vládce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
pastýř	pastýř	k1gMnSc1	pastýř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
nést	nést	k5eAaImF	nést
pouze	pouze	k6eAd1	pouze
přes	přes	k7c4	přes
Svatopetrské	svatopetrský	k2eAgNnSc4d1	Svatopetrské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ho	on	k3xPp3gNnSc4	on
lidé	člověk	k1gMnPc1	člověk
mohli	moct	k5eAaImAgMnP	moct
lépe	dobře	k6eAd2	dobře
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
z	z	k7c2	z
nosítek	nosítka	k1gNnPc2	nosítka
sestoupil	sestoupit	k5eAaPmAgMnS	sestoupit
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
byli	být	k5eAaImAgMnP	být
přítomní	přítomný	k2eAgMnPc1d1	přítomný
biskupové	biskup	k1gMnPc1	biskup
ze	z	k7c2	z
133	[number]	k4	133
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
baziliky	bazilika	k1gFnSc2	bazilika
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
na	na	k7c4	na
mohutnou	mohutný	k2eAgFnSc4d1	mohutná
koncilní	koncilní	k2eAgFnSc4d1	koncilní
aulu	aula	k1gFnSc4	aula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostřední	prostřední	k2eAgFnSc6d1	prostřední
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
nacházely	nacházet	k5eAaImAgFnP	nacházet
90	[number]	k4	90
m	m	kA	m
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
stupňovité	stupňovitý	k2eAgFnSc2d1	stupňovitá
tribuny	tribuna	k1gFnSc2	tribuna
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
seděli	sedět	k5eAaImAgMnP	sedět
účastníci	účastník	k1gMnPc1	účastník
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
zasedáních	zasedání	k1gNnPc6	zasedání
(	(	kIx(	(
<g/>
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
všeobecné	všeobecný	k2eAgFnPc4d1	všeobecná
kongregace	kongregace	k1gFnSc2	kongregace
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
na	na	k7c4	na
modernisty	modernista	k1gMnPc4	modernista
a	a	k8xC	a
konzervativce	konzervativec	k1gMnPc4	konzervativec
<g/>
.	.	kIx.	.
</s>
<s>
Kurie	kurie	k1gFnSc1	kurie
chtěla	chtít	k5eAaImAgFnS	chtít
koncil	koncil	k1gInSc4	koncil
řádně	řádně	k6eAd1	řádně
řídit	řídit	k5eAaImF	řídit
a	a	k8xC	a
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
se	se	k3xPyFc4	se
mít	mít	k5eAaImF	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
program	program	k1gInSc4	program
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
obsazení	obsazení	k1gNnSc2	obsazení
rozhodujících	rozhodující	k2eAgFnPc2d1	rozhodující
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
zkouškou	zkouška	k1gFnSc7	zkouška
bylo	být	k5eAaImAgNnS	být
zasedání	zasedání	k1gNnSc2	zasedání
deseti	deset	k4xCc2	deset
koncilových	koncilův	k2eAgFnPc2d1	koncilův
komisí	komise	k1gFnPc2	komise
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc1	deset
komisí	komise	k1gFnPc2	komise
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
svým	svůj	k3xOyFgInSc7	svůj
počtem	počet	k1gInSc7	počet
deseti	deset	k4xCc3	deset
přípravným	přípravný	k2eAgFnPc3d1	přípravná
komisím	komise	k1gFnPc3	komise
a	a	k8xC	a
deseti	deset	k4xCc3	deset
tematickým	tematický	k2eAgInPc3d1	tematický
okruhům	okruh	k1gInPc3	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
zapracovat	zapracovat	k5eAaPmF	zapracovat
výsledky	výsledek	k1gInPc4	výsledek
diskusí	diskuse	k1gFnPc2	diskuse
generálního	generální	k2eAgInSc2d1	generální
shromáždění	shromáždění	k1gNnSc1	shromáždění
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgNnPc3d1	jednotlivé
schématům	schéma	k1gNnPc3	schéma
a	a	k8xC	a
přepracovaná	přepracovaný	k2eAgNnPc1d1	přepracované
schémata	schéma	k1gNnPc1	schéma
potom	potom	k6eAd1	potom
znovu	znovu	k6eAd1	znovu
předložit	předložit	k5eAaPmF	předložit
generálnímu	generální	k2eAgNnSc3d1	generální
shromáždění	shromáždění	k1gNnSc3	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnPc1	komise
měly	mít	k5eAaImAgFnP	mít
mít	mít	k5eAaImF	mít
24	[number]	k4	24
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
16	[number]	k4	16
koncilních	koncilní	k2eAgMnPc2d1	koncilní
otců	otec	k1gMnPc2	otec
a	a	k8xC	a
zbylých	zbylý	k2eAgMnPc2d1	zbylý
osm	osm	k4xCc1	osm
měl	mít	k5eAaImAgMnS	mít
jmenovat	jmenovat	k5eAaImF	jmenovat
papež	papež	k1gMnSc1	papež
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
zasedání	zasedání	k1gNnSc6	zasedání
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
členů	člen	k1gMnPc2	člen
komisí	komise	k1gFnPc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
biskupové	biskup	k1gMnPc1	biskup
příliš	příliš	k6eAd1	příliš
neznali	neznat	k5eAaImAgMnP	neznat
<g/>
,	,	kIx,	,
členové	člen	k1gMnPc1	člen
kurie	kurie	k1gFnSc2	kurie
rozdali	rozdat	k5eAaPmAgMnP	rozdat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
hlasovacími	hlasovací	k2eAgInPc7d1	hlasovací
lístky	lístek	k1gInPc7	lístek
seznamy	seznam	k1gInPc1	seznam
členů	člen	k1gMnPc2	člen
přípravných	přípravný	k2eAgFnPc2d1	přípravná
komisí	komise	k1gFnPc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Kardinálové	kardinál	k1gMnPc1	kardinál
Liénart	Liénarta	k1gFnPc2	Liénarta
(	(	kIx(	(
<g/>
z	z	k7c2	z
Lille	Lille	k1gFnSc2	Lille
<g/>
)	)	kIx)	)
a	a	k8xC	a
Frings	Frings	k1gInSc4	Frings
(	(	kIx(	(
<g/>
z	z	k7c2	z
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
)	)	kIx)	)
podotkli	podotknout	k5eAaPmAgMnP	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
otcové	otec	k1gMnPc1	otec
měli	mít	k5eAaImAgMnP	mít
nejdříve	dříve	k6eAd3	dříve
poznat	poznat	k5eAaPmF	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
potlesk	potlesk	k1gInSc1	potlesk
a	a	k8xC	a
první	první	k4xOgNnSc1	první
zasedání	zasedání	k1gNnSc1	zasedání
tím	ten	k3xDgNnSc7	ten
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
minutách	minuta	k1gFnPc6	minuta
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
sešly	sejít	k5eAaPmAgInP	sejít
národní	národní	k2eAgInPc1d1	národní
a	a	k8xC	a
regionální	regionální	k2eAgFnSc2d1	regionální
biskupské	biskupský	k2eAgFnSc2d1	biskupská
konference	konference	k1gFnSc2	konference
a	a	k8xC	a
sestavily	sestavit	k5eAaPmAgFnP	sestavit
nové	nový	k2eAgFnPc4d1	nová
kandidátní	kandidátní	k2eAgFnPc4d1	kandidátní
listiny	listina	k1gFnPc4	listina
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgInSc7d3	nejlepší
dojmem	dojem	k1gInSc7	dojem
působila	působit	k5eAaImAgFnS	působit
kandidátka	kandidátka	k1gFnSc1	kandidátka
předložená	předložený	k2eAgFnSc1d1	předložená
středoevropskými	středoevropský	k2eAgInPc7d1	středoevropský
a	a	k8xC	a
francouzskými	francouzský	k2eAgInPc7d1	francouzský
biskupy	biskup	k1gInPc7	biskup
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
odborníky	odborník	k1gMnPc4	odborník
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
období	období	k1gNnSc2	období
zasedala	zasedat	k5eAaImAgFnS	zasedat
36	[number]	k4	36
<g/>
×	×	k?	×
generální	generální	k2eAgFnSc2d1	generální
kongregace	kongregace	k1gFnSc2	kongregace
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
předneseno	přednést	k5eAaPmNgNnS	přednést
640	[number]	k4	640
projevů	projev	k1gInPc2	projev
a	a	k8xC	a
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
33	[number]	k4	33
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
se	se	k3xPyFc4	se
o	o	k7c6	o
předlohách	předloha	k1gFnPc6	předloha
o	o	k7c6	o
liturgii	liturgie	k1gFnSc6	liturgie
<g/>
,	,	kIx,	,
zjevení	zjevení	k1gNnSc6	zjevení
<g/>
,	,	kIx,	,
o	o	k7c6	o
sdělovacích	sdělovací	k2eAgInPc6d1	sdělovací
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
,	,	kIx,	,
o	o	k7c6	o
východních	východní	k2eAgFnPc6d1	východní
církvích	církev	k1gFnPc6	církev
a	a	k8xC	a
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
dokumentů	dokument	k1gInPc2	dokument
však	však	k9	však
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
nebyl	být	k5eNaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
smysluplně	smysluplně	k6eAd1	smysluplně
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
koordinační	koordinační	k2eAgFnSc1d1	koordinační
komise	komise	k1gFnSc1	komise
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
koncil	koncil	k1gInSc1	koncil
sešel	sejít	k5eAaPmAgInS	sejít
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
naposledy	naposledy	k6eAd1	naposledy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1962	[number]	k4	1962
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
od	od	k7c2	od
lékařů	lékař	k1gMnPc2	lékař
dověděl	dovědět	k5eAaPmAgMnS	dovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
zbývá	zbývat	k5eAaImIp3nS	zbývat
již	již	k9	již
jen	jen	k9	jen
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
pozemského	pozemský	k2eAgInSc2d1	pozemský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pověřil	pověřit	k5eAaPmAgMnS	pověřit
proto	proto	k8xC	proto
kardinály	kardinál	k1gMnPc4	kardinál
Suenense	Suenens	k1gMnSc4	Suenens
a	a	k8xC	a
Montiniho	Montini	k1gMnSc4	Montini
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
před	před	k7c7	před
koncilem	koncil	k1gInSc7	koncil
přednesli	přednést	k5eAaPmAgMnP	přednést
své	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
o	o	k7c6	o
dalším	další	k2eAgNnSc6d1	další
směrování	směrování	k1gNnSc6	směrování
koncilu	koncil	k1gInSc2	koncil
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
přednesli	přednést	k5eAaPmAgMnP	přednést
soukromě	soukromě	k6eAd1	soukromě
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Pokračování	pokračování	k1gNnSc1	pokračování
koncilu	koncil	k1gInSc2	koncil
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
necelé	celý	k2eNgInPc4d1	necelý
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
po	po	k7c6	po
dvoudenním	dvoudenní	k2eAgNnSc6d1	dvoudenní
konkláve	konkláve	k1gNnSc6	konkláve
zvolen	zvolit	k5eAaPmNgMnS	zvolit
novým	nový	k2eAgMnSc7d1	nový
papežem	papež	k1gMnSc7	papež
kardinál	kardinál	k1gMnSc1	kardinál
Montini	Montin	k2eAgMnPc1d1	Montin
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c4	v
den	den	k1gInSc4	den
své	svůj	k3xOyFgFnSc2	svůj
volby	volba	k1gFnSc2	volba
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncil	koncil	k1gInSc1	koncil
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
prvního	první	k4xOgNnSc2	první
zasedání	zasedání	k1gNnSc2	zasedání
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
na	na	k7c4	na
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
projevu	projev	k1gInSc6	projev
papež	papež	k1gMnSc1	papež
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžištěm	těžiště	k1gNnSc7	těžiště
jednání	jednání	k1gNnSc2	jednání
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
konstituce	konstituce	k1gFnSc1	konstituce
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
-	-	kIx~	-
obnova	obnova	k1gFnSc1	obnova
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
jednota	jednota	k1gFnSc1	jednota
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
měsíci	měsíc	k1gInSc6	měsíc
byla	být	k5eAaImAgFnS	být
předloha	předloha	k1gFnSc1	předloha
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
důkladně	důkladně	k6eAd1	důkladně
přepracována	přepracován	k2eAgFnSc1d1	přepracována
a	a	k8xC	a
přijata	přijat	k2eAgFnSc1d1	přijata
jako	jako	k8xS	jako
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
programu	program	k1gInSc6	program
dekrety	dekret	k1gInPc4	dekret
o	o	k7c6	o
biskupech	biskup	k1gInPc6	biskup
a	a	k8xC	a
o	o	k7c6	o
ekumenismu	ekumenismus	k1gInSc6	ekumenismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dekretu	dekret	k1gInSc6	dekret
o	o	k7c6	o
ekumenismu	ekumenismus	k1gInSc6	ekumenismus
narazily	narazit	k5eAaPmAgInP	narazit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
pasáže	pasáž	k1gFnSc2	pasáž
o	o	k7c6	o
Židech	Žid	k1gMnPc6	Žid
a	a	k8xC	a
o	o	k7c6	o
náboženské	náboženský	k2eAgFnSc6d1	náboženská
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
dekret	dekret	k1gInSc1	dekret
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
a	a	k8xC	a
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
prvních	první	k4xOgFnPc6	první
třech	tři	k4xCgFnPc6	tři
kapitolách	kapitola	k1gFnPc6	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
dokumentech	dokument	k1gInPc6	dokument
o	o	k7c6	o
liturgii	liturgie	k1gFnSc6	liturgie
a	a	k8xC	a
o	o	k7c6	o
sdělovacích	sdělovací	k2eAgInPc6d1	sdělovací
prostředcích	prostředek	k1gInPc6	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Konstituce	konstituce	k1gFnSc1	konstituce
o	o	k7c4	o
liturgii	liturgie	k1gFnSc4	liturgie
a	a	k8xC	a
dekret	dekret	k1gInSc4	dekret
o	o	k7c6	o
prostředcích	prostředek	k1gInPc6	prostředek
masové	masový	k2eAgFnSc2d1	masová
komunikace	komunikace	k1gFnSc2	komunikace
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
zasedání	zasedání	k1gNnSc6	zasedání
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
schváleny	schválit	k5eAaPmNgInP	schválit
koncilem	koncil	k1gInSc7	koncil
a	a	k8xC	a
vyhlášeny	vyhlášen	k2eAgFnPc4d1	vyhlášena
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
přitom	přitom	k6eAd1	přitom
prvně	prvně	k?	prvně
použil	použít	k5eAaPmAgMnS	použít
novou	nový	k2eAgFnSc4d1	nová
formuli	formule	k1gFnSc4	formule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyzdvihovala	vyzdvihovat	k5eAaImAgFnS	vyzdvihovat
kolegialitu	kolegialita	k1gFnSc4	kolegialita
biskupů	biskup	k1gInPc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
jednací	jednací	k2eAgNnSc1d1	jednací
období	období	k1gNnSc1	období
probíhalo	probíhat	k5eAaImAgNnS	probíhat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
až	až	k6eAd1	až
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
probíhala	probíhat	k5eAaImAgFnS	probíhat
jednání	jednání	k1gNnSc4	jednání
i	i	k8xC	i
předběžná	předběžný	k2eAgNnPc4d1	předběžné
hlasování	hlasování	k1gNnSc4	hlasování
<g/>
,	,	kIx,	,
přijímaly	přijímat	k5eAaImAgInP	přijímat
se	se	k3xPyFc4	se
pozměňovací	pozměňovací	k2eAgInPc1d1	pozměňovací
návrhy	návrh	k1gInPc1	návrh
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zpracovávala	zpracovávat	k5eAaImAgNnP	zpracovávat
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
schémata	schéma	k1gNnPc1	schéma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
zasedání	zasedání	k1gNnSc6	zasedání
byly	být	k5eAaImAgFnP	být
odhlasovány	odhlasován	k2eAgFnPc1d1	odhlasována
a	a	k8xC	a
slavnostně	slavnostně	k6eAd1	slavnostně
vyhlášeny	vyhlásit	k5eAaPmNgFnP	vyhlásit
konstituce	konstituce	k1gFnPc1	konstituce
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
dekrety	dekret	k1gInPc4	dekret
o	o	k7c6	o
ekumenismu	ekumenismus	k1gInSc6	ekumenismus
a	a	k8xC	a
o	o	k7c6	o
východních	východní	k2eAgFnPc6d1	východní
církvích	církev	k1gFnPc6	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
čtvrtému	čtvrtý	k4xOgInSc3	čtvrtý
jednacímu	jednací	k2eAgInSc3d1	jednací
období	období	k1gNnPc4	období
se	se	k3xPyFc4	se
otcové	otec	k1gMnPc1	otec
sešli	sejít	k5eAaPmAgMnP	sejít
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
16	[number]	k4	16
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
dokumentů	dokument	k1gInPc2	dokument
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
pouze	pouze	k6eAd1	pouze
pět	pět	k4xCc1	pět
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
dalších	další	k2eAgInPc2d1	další
dokumentů	dokument	k1gInPc2	dokument
nebylo	být	k5eNaImAgNnS	být
tentokrát	tentokrát	k6eAd1	tentokrát
necháno	nechat	k5eAaPmNgNnS	nechat
až	až	k9	až
na	na	k7c4	na
poslední	poslední	k2eAgNnSc4d1	poslední
zasedání	zasedání	k1gNnSc4	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zasedání	zasedání	k1gNnSc6	zasedání
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byly	být	k5eAaImAgFnP	být
schváleny	schválit	k5eAaPmNgInP	schválit
dekrety	dekret	k1gInPc1	dekret
o	o	k7c6	o
pastýřské	pastýřský	k2eAgFnSc6d1	pastýřská
službě	služba	k1gFnSc6	služba
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
o	o	k7c6	o
výchově	výchova	k1gFnSc6	výchova
ke	k	k7c3	k
kněžství	kněžství	k1gNnSc3	kněžství
<g/>
,	,	kIx,	,
o	o	k7c6	o
obnově	obnova	k1gFnSc6	obnova
řeholního	řeholní	k2eAgInSc2d1	řeholní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
o	o	k7c6	o
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
výchově	výchova	k1gFnSc6	výchova
a	a	k8xC	a
deklarace	deklarace	k1gFnSc2	deklarace
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
nekřesťanským	křesťanský	k2eNgNnPc3d1	nekřesťanské
náboženstvím	náboženství	k1gNnPc3	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
zasedání	zasedání	k1gNnSc6	zasedání
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
konstituce	konstituce	k1gFnSc1	konstituce
O	o	k7c6	o
Božím	boží	k2eAgNnSc6d1	boží
zjevení	zjevení	k1gNnSc6	zjevení
a	a	k8xC	a
dekret	dekret	k1gInSc1	dekret
o	o	k7c6	o
laickém	laický	k2eAgInSc6d1	laický
apoštolátu	apoštolát	k1gInSc6	apoštolát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
třetím	třetí	k4xOgNnSc6	třetí
zasedání	zasedání	k1gNnSc6	zasedání
byl	být	k5eAaImAgInS	být
schválen	schválen	k2eAgInSc1d1	schválen
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
a	a	k8xC	a
nejdiskutovanější	diskutovaný	k2eAgInSc1d3	nejdiskutovanější
dokument	dokument	k1gInSc1	dokument
-	-	kIx~	-
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgInP	být
vyhlášeny	vyhlášen	k2eAgInPc1d1	vyhlášen
dekrety	dekret	k1gInPc1	dekret
o	o	k7c6	o
službě	služba	k1gFnSc6	služba
a	a	k8xC	a
životě	život	k1gInSc6	život
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
o	o	k7c6	o
misijní	misijní	k2eAgFnSc6d1	misijní
činnosti	činnost	k1gFnSc6	činnost
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
deklarace	deklarace	k1gFnSc1	deklarace
o	o	k7c6	o
náboženské	náboženský	k2eAgFnSc6d1	náboženská
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
tohoto	tento	k3xDgNnSc2	tento
zasedání	zasedání	k1gNnSc2	zasedání
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k6eAd1	ještě
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
významné	významný	k2eAgFnSc3d1	významná
události	událost	k1gFnSc3	událost
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
a	a	k8xC	a
přítomný	přítomný	k2eAgMnSc1d1	přítomný
zástupce	zástupce	k1gMnSc1	zástupce
ekumenického	ekumenický	k2eAgMnSc2d1	ekumenický
patriarchy	patriarcha	k1gMnSc2	patriarcha
Athenagora	Athenagor	k1gMnSc2	Athenagor
zrušili	zrušit	k5eAaPmAgMnP	zrušit
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
exkomunikaci	exkomunikace	k1gFnSc4	exkomunikace
mezi	mezi	k7c7	mezi
východní	východní	k2eAgFnSc7d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc7d1	západní
církví	církev	k1gFnSc7	církev
vyhlášenou	vyhlášený	k2eAgFnSc7d1	vyhlášená
roku	rok	k1gInSc2	rok
1054	[number]	k4	1054
<g/>
.	.	kIx.	.
</s>
<s>
Deváté	devátý	k4xOgNnSc4	devátý
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
zasedání	zasedání	k1gNnSc4	zasedání
koncilu	koncil	k1gInSc2	koncil
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
bylo	být	k5eAaImAgNnS	být
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
bohoslužbou	bohoslužba	k1gFnSc7	bohoslužba
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
období	období	k1gNnSc6	období
<g/>
:	:	kIx,	:
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1962	[number]	k4	1962
2	[number]	k4	2
<g/>
.	.	kIx.	.
období	období	k1gNnSc6	období
<g/>
:	:	kIx,	:
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
3	[number]	k4	3
<g/>
.	.	kIx.	.
období	období	k1gNnSc4	období
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1964	[number]	k4	1964
4	[number]	k4	4
<g/>
.	.	kIx.	.
období	období	k1gNnSc6	období
<g/>
:	:	kIx,	:
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
Koncil	koncil	k1gInSc1	koncil
vydal	vydat	k5eAaPmAgInS	vydat
16	[number]	k4	16
dokumentů	dokument	k1gInPc2	dokument
<g/>
:	:	kIx,	:
Koncilové	Koncilové	k2eAgInPc1d1	Koncilové
dokumenty	dokument	k1gInPc1	dokument
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgInP	zabývat
především	především	k6eAd1	především
těmito	tento	k3xDgFnPc7	tento
oblastmi	oblast	k1gFnPc7	oblast
<g/>
:	:	kIx,	:
Ritus	ritus	k1gInSc1	ritus
<g/>
:	:	kIx,	:
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
konstituce	konstituce	k1gFnSc2	konstituce
o	o	k7c6	o
posvátné	posvátný	k2eAgFnSc6d1	posvátná
liturgii	liturgie	k1gFnSc6	liturgie
Sacrosanctum	Sacrosanctum	k1gNnSc1	Sacrosanctum
Concilium	Concilium	k1gNnSc1	Concilium
nastaly	nastat	k5eAaPmAgFnP	nastat
dvě	dva	k4xCgFnPc1	dva
liturgické	liturgický	k2eAgFnPc1d1	liturgická
reformy	reforma	k1gFnPc1	reforma
<g/>
:	:	kIx,	:
První	první	k4xOgInPc1	první
reformní	reformní	k2eAgInPc1d1	reformní
kroky	krok	k1gInPc1	krok
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgInP	projevit
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
vydání	vydání	k1gNnSc6	vydání
římského	římský	k2eAgInSc2d1	římský
misálu	misál	k1gInSc2	misál
Missale	Missala	k1gFnSc3	Missala
Romanum	Romanum	k?	Romanum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ale	ale	k9	ale
ještě	ještě	k6eAd1	ještě
nezohledňovalo	zohledňovat	k5eNaImAgNnS	zohledňovat
požadavek	požadavek	k1gInSc4	požadavek
konstituce	konstituce	k1gFnSc2	konstituce
nabídnout	nabídnout	k5eAaPmF	nabídnout
věřícím	věřící	k2eAgInSc7d1	věřící
během	běh	k1gInSc7	běh
liturgie	liturgie	k1gFnSc2	liturgie
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
biblických	biblický	k2eAgInPc2d1	biblický
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
další	další	k2eAgNnSc1d1	další
vydání	vydání	k1gNnSc1	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
zavedlo	zavést	k5eAaPmAgNnS	zavést
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
řád	řád	k1gInSc4	řád
čtení	čtení	k1gNnSc2	čtení
(	(	kIx(	(
<g/>
lekcionář	lekcionář	k1gInSc1	lekcionář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
liturgickou	liturgický	k2eAgFnSc7d1	liturgická
reformou	reforma	k1gFnSc7	reforma
byla	být	k5eAaImAgFnS	být
latina	latina	k1gFnSc1	latina
jako	jako	k8xC	jako
liturgický	liturgický	k2eAgInSc1d1	liturgický
jazyk	jazyk	k1gInSc1	jazyk
široce	široko	k6eAd1	široko
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
národními	národní	k2eAgInPc7d1	národní
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
původně	původně	k6eAd1	původně
ani	ani	k8xC	ani
konstituce	konstituce	k1gFnSc1	konstituce
nepředpokládala	předpokládat	k5eNaImAgFnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
postupné	postupný	k2eAgFnSc2d1	postupná
liturgické	liturgický	k2eAgFnSc2d1	liturgická
reformy	reforma	k1gFnSc2	reforma
tak	tak	k9	tak
selhal	selhat	k5eAaPmAgInS	selhat
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
duchovenstvem	duchovenstvo	k1gNnSc7	duchovenstvo
i	i	k8xC	i
lidem	lid	k1gInSc7	lid
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
"	"	kIx"	"
<g/>
zdola	zdola	k6eAd1	zdola
<g/>
"	"	kIx"	"
zaváděna	zaváděn	k2eAgFnSc1d1	zaváděna
"	"	kIx"	"
<g/>
nová	nový	k2eAgFnSc1d1	nová
liturgie	liturgie	k1gFnSc1	liturgie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
o	o	k7c4	o
"	"	kIx"	"
<g/>
starou	starý	k2eAgFnSc4d1	stará
liturgii	liturgie	k1gFnSc4	liturgie
<g/>
"	"	kIx"	"
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
se	se	k3xPyFc4	se
Římský	římský	k2eAgInSc1d1	římský
misál	misál	k1gInSc1	misál
Pavla	Pavel	k1gMnSc2	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
mohl	moct	k5eAaImAgInS	moct
zavádět	zavádět	k5eAaImF	zavádět
jen	jen	k9	jen
postupně	postupně	k6eAd1	postupně
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
téměř	téměř	k6eAd1	téměř
nikde	nikde	k6eAd1	nikde
zcela	zcela	k6eAd1	zcela
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgNnPc3d1	ostatní
vyznáním	vyznání	k1gNnPc3	vyznání
<g/>
:	:	kIx,	:
Vývoj	vývoj	k1gInSc1	vývoj
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
vatikánském	vatikánský	k2eAgInSc6d1	vatikánský
koncilu	koncil	k1gInSc6	koncil
postihnul	postihnout	k5eAaPmAgInS	postihnout
také	také	k9	také
oblast	oblast	k1gFnSc4	oblast
vztahů	vztah	k1gInPc2	vztah
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
k	k	k7c3	k
ostatním	ostatní	k2eAgNnPc3d1	ostatní
vyznáním	vyznání	k1gNnPc3	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
až	až	k9	až
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
se	se	k3xPyFc4	se
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
pokládala	pokládat	k5eAaImAgFnS	pokládat
za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
pravou	pravý	k2eAgFnSc4d1	pravá
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
během	během	k7c2	během
koncilu	koncil	k1gInSc2	koncil
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
opatrně	opatrně	k6eAd1	opatrně
relativizován	relativizovat	k5eAaImNgInS	relativizovat
<g/>
.	.	kIx.	.
</s>
<s>
Oporou	opora	k1gFnSc7	opora
tohoto	tento	k3xDgInSc2	tento
vývoje	vývoj	k1gInSc2	vývoj
jsou	být	k5eAaImIp3nP	být
dokumenty	dokument	k1gInPc1	dokument
Unitatis	Unitatis	k1gFnSc2	Unitatis
redintegratio	redintegratio	k6eAd1	redintegratio
(	(	kIx(	(
<g/>
jiná	jiný	k2eAgFnSc1d1	jiná
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nostra	Nostra	k1gFnSc1	Nostra
aetate	aetat	k1gInSc5	aetat
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgNnPc1d1	ostatní
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dekretu	dekret	k1gInSc6	dekret
o	o	k7c6	o
náboženské	náboženský	k2eAgFnSc6d1	náboženská
svobodě	svoboda	k1gFnSc6	svoboda
(	(	kIx(	(
<g/>
Dignitatis	Dignitatis	k1gInSc1	Dignitatis
humanae	humana	k1gInSc2	humana
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
...	...	k?	...
sám	sám	k3xTgMnSc1	sám
Bůh	bůh	k1gMnSc1	bůh
dal	dát	k5eAaPmAgMnS	dát
lidstvu	lidstvo	k1gNnSc3	lidstvo
poznat	poznat	k5eAaPmF	poznat
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
slouží	sloužit	k5eAaImIp3nP	sloužit
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
v	v	k7c4	v
Kristu	Krista	k1gFnSc4	Krista
spásy	spása	k1gFnSc2	spása
a	a	k8xC	a
blaženosti	blaženost	k1gFnSc2	blaženost
<g/>
.	.	kIx.	.
</s>
<s>
Věříme	věřit	k5eAaImIp1nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
jediné	jediný	k2eAgNnSc1d1	jediné
pravé	pravý	k2eAgNnSc1d1	pravé
náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
uskutečněno	uskutečnit	k5eAaPmNgNnS	uskutečnit
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
a	a	k8xC	a
apoštolské	apoštolský	k2eAgFnSc6d1	apoštolská
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
Pán	pán	k1gMnSc1	pán
Ježíš	Ježíš	k1gMnSc1	Ježíš
svěřil	svěřit	k5eAaPmAgMnS	svěřit
úkol	úkol	k1gInSc4	úkol
přinášet	přinášet	k5eAaImF	přinášet
je	on	k3xPp3gInPc4	on
ke	k	k7c3	k
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
DH	DH	kA	DH
1	[number]	k4	1
<g/>
)	)	kIx)	)
Vztah	vztah	k1gInSc1	vztah
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
:	:	kIx,	:
Koncil	koncil	k1gInSc1	koncil
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
právo	právo	k1gNnSc1	právo
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
vyznání	vyznání	k1gNnSc4	vyznání
důrazněji	důrazně	k6eAd2	důrazně
než	než	k8xS	než
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
vírou	víra	k1gFnSc7	víra
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Právo	právo	k1gNnSc1	právo
na	na	k7c4	na
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
svobodu	svoboda	k1gFnSc4	svoboda
tedy	tedy	k9	tedy
nevyplývá	vyplývat	k5eNaImIp3nS	vyplývat
ze	z	k7c2	z
subjektivní	subjektivní	k2eAgFnSc2d1	subjektivní
dispozice	dispozice	k1gFnSc2	dispozice
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
ze	z	k7c2	z
samé	samý	k3xTgFnSc2	samý
její	její	k3xOp3gFnSc2	její
přirozenosti	přirozenost	k1gFnSc2	přirozenost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
svobodu	svoboda	k1gFnSc4	svoboda
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
neplní	plnit	k5eNaImIp3nS	plnit
povinnost	povinnost	k1gFnSc1	povinnost
hledat	hledat	k5eAaImF	hledat
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
přidržet	přidržet	k5eAaPmF	přidržet
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
DH	DH	kA	DH
2	[number]	k4	2
<g/>
)	)	kIx)	)
Vztah	vztah	k1gInSc1	vztah
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
státu	stát	k1gInSc2	stát
<g/>
:	:	kIx,	:
Koncil	koncil	k1gInSc1	koncil
se	se	k3xPyFc4	se
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
požadavku	požadavek	k1gInSc2	požadavek
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
od	od	k7c2	od
reformace	reformace	k1gFnSc2	reformace
neprosaditelného	prosaditelný	k2eNgInSc2d1	neprosaditelný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
veřejnost	veřejnost	k1gFnSc1	veřejnost
i	i	k8xC	i
státní	státní	k2eAgFnPc1d1	státní
struktury	struktura	k1gFnPc1	struktura
jednaly	jednat	k5eAaImAgFnP	jednat
podle	podle	k7c2	podle
katolických	katolický	k2eAgInPc2d1	katolický
principů	princip	k1gInPc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
absolutnost	absolutnost	k1gFnSc1	absolutnost
katolického	katolický	k2eAgNnSc2d1	katolické
náboženství	náboženství	k1gNnSc2	náboženství
omezena	omezit	k5eAaPmNgFnS	omezit
čistě	čistě	k6eAd1	čistě
na	na	k7c4	na
duchovní	duchovní	k2eAgFnSc4d1	duchovní
rovinu	rovina	k1gFnSc4	rovina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zaměňována	zaměňován	k2eAgFnSc1d1	zaměňována
s	s	k7c7	s
totalitními	totalitní	k2eAgFnPc7d1	totalitní
ideologiemi	ideologie	k1gFnPc7	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
výraznou	výrazný	k2eAgFnSc4d1	výrazná
většinu	většina	k1gFnSc4	většina
souhlasných	souhlasný	k2eAgInPc2d1	souhlasný
hlasů	hlas	k1gInPc2	hlas
při	při	k7c6	při
hlasováních	hlasování	k1gNnPc6	hlasování
o	o	k7c6	o
formulacích	formulace	k1gFnPc6	formulace
koncilních	koncilní	k2eAgInPc2d1	koncilní
dokumentů	dokument	k1gInPc2	dokument
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
zohledňován	zohledňován	k2eAgInSc1d1	zohledňován
názor	názor	k1gInSc1	názor
menšiny	menšina	k1gFnSc2	menšina
-	-	kIx~	-
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
kolem	kolem	k7c2	kolem
300	[number]	k4	300
otců	otec	k1gMnPc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nakonec	nakonec	k6eAd1	nakonec
některé	některý	k3yIgFnPc1	některý
formulace	formulace	k1gFnPc1	formulace
nejsou	být	k5eNaImIp3nP	být
dost	dost	k6eAd1	dost
jednoznačné	jednoznačný	k2eAgInPc4d1	jednoznačný
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
rozpor	rozpor	k1gInSc4	rozpor
chápání	chápání	k1gNnSc2	chápání
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
jako	jako	k9	jako
Božího	boží	k2eAgInSc2d1	boží
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
jako	jako	k9	jako
hierarchie	hierarchie	k1gFnSc1	hierarchie
<g/>
,	,	kIx,	,
rozpor	rozpor	k1gInSc1	rozpor
kolegiality	kolegialita	k1gFnSc2	kolegialita
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
nadřazenosti	nadřazenost	k1gFnSc6	nadřazenost
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
rozpor	rozpor	k1gInSc1	rozpor
smýšlení	smýšlení	k1gNnSc2	smýšlení
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
učitelského	učitelský	k2eAgInSc2d1	učitelský
úřadu	úřad	k1gInSc2	úřad
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Liturgická	liturgický	k2eAgFnSc1d1	liturgická
reforma	reforma	k1gFnSc1	reforma
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejviditelnějším	viditelný	k2eAgMnSc7d3	nejviditelnější
a	a	k8xC	a
nejtrvalejším	trvalý	k2eAgNnSc7d3	nejtrvalejší
dílem	dílo	k1gNnSc7	dílo
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
reformy	reforma	k1gFnSc2	reforma
Pia	Pius	k1gMnSc2	Pius
V.	V.	kA	V.
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
až	až	k9	až
do	do	k7c2	do
počátků	počátek	k1gInPc2	počátek
liturgického	liturgický	k2eAgNnSc2d1	liturgické
hnutí	hnutí	k1gNnSc2	hnutí
měla	mít	k5eAaImAgFnS	mít
liturgie	liturgie	k1gFnSc1	liturgie
přesně	přesně	k6eAd1	přesně
daná	daný	k2eAgNnPc4d1	dané
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
ministrant	ministrant	k1gMnSc1	ministrant
<g/>
,	,	kIx,	,
lektor	lektor	k1gMnSc1	lektor
<g/>
,	,	kIx,	,
kantor	kantor	k1gMnSc1	kantor
<g/>
,	,	kIx,	,
jáhen	jáhen	k1gMnSc1	jáhen
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
měli	mít	k5eAaImAgMnP	mít
přesně	přesně	k6eAd1	přesně
určené	určený	k2eAgFnPc4d1	určená
role	role	k1gFnPc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Bohoslužebným	bohoslužebný	k2eAgInSc7d1	bohoslužebný
jazykem	jazyk	k1gInSc7	jazyk
byla	být	k5eAaImAgFnS	být
latina	latina	k1gFnSc1	latina
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
proměňování	proměňování	k1gNnSc2	proměňování
směl	smět	k5eAaImAgMnS	smět
kněz	kněz	k1gMnSc1	kněz
dokonce	dokonce	k9	dokonce
pronášet	pronášet	k5eAaImF	pronášet
jen	jen	k9	jen
polohlasně	polohlasně	k6eAd1	polohlasně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnPc4	on
slyšeli	slyšet	k5eAaImAgMnP	slyšet
jen	jen	k6eAd1	jen
jemu	on	k3xPp3gNnSc3	on
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
při	při	k7c6	při
mši	mše	k1gFnSc6	mše
buď	buď	k8xC	buď
zpívali	zpívat	k5eAaImAgMnP	zpívat
mešní	mešní	k2eAgFnPc4d1	mešní
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
v	v	k7c6	v
příslušné	příslušný	k2eAgFnSc6d1	příslušná
zemi	zem	k1gFnSc6	zem
existovaly	existovat	k5eAaImAgFnP	existovat
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
tiše	tiš	k1gFnSc2	tiš
modlili	modlit	k5eAaImAgMnP	modlit
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
liturgické	liturgický	k2eAgNnSc1d1	liturgické
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
i	i	k9	i
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
X.	X.	kA	X.
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
dvojjazyčné	dvojjazyčný	k2eAgInPc1d1	dvojjazyčný
lidové	lidový	k2eAgInPc1d1	lidový
misály	misál	k1gInPc1	misál
<g/>
.	.	kIx.	.
</s>
<s>
Liturgická	liturgický	k2eAgFnSc1d1	liturgická
obnova	obnova	k1gFnSc1	obnova
pomalu	pomalu	k6eAd1	pomalu
postupovala	postupovat	k5eAaImAgFnS	postupovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
koncilem	koncil	k1gInSc7	koncil
se	se	k3xPyFc4	se
již	již	k9	již
lid	lid	k1gInSc1	lid
zapojoval	zapojovat	k5eAaImAgInS	zapojovat
do	do	k7c2	do
mešního	mešní	k2eAgNnSc2d1	mešní
dění	dění	k1gNnSc2	dění
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpíval	zpívat	k5eAaImAgMnS	zpívat
alespoň	alespoň	k9	alespoň
krátké	krátký	k2eAgFnPc4d1	krátká
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
kněžské	kněžský	k2eAgFnPc4d1	kněžská
modlitby	modlitba	k1gFnPc4	modlitba
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
ojediněle	ojediněle	k6eAd1	ojediněle
chrámový	chrámový	k2eAgInSc1d1	chrámový
sbor	sbor	k1gInSc1	sbor
volil	volit	k5eAaImAgInS	volit
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
Credo	Credo	k1gNnSc4	Credo
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
zpěvu	zpěv	k1gInSc6	zpěv
střídal	střídat	k5eAaImAgMnS	střídat
s	s	k7c7	s
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Leckde	leckde	k6eAd1	leckde
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
před	před	k7c7	před
latinskými	latinský	k2eAgInPc7d1	latinský
texty	text	k1gInPc7	text
předčítat	předčítat	k5eAaImF	předčítat
jejich	jejich	k3xOp3gInPc4	jejich
překlady	překlad	k1gInPc4	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Koncilová	Koncilový	k2eAgFnSc1d1	Koncilový
reforma	reforma	k1gFnSc1	reforma
liturgie	liturgie	k1gFnSc1	liturgie
přinesla	přinést	k5eAaPmAgFnS	přinést
mnoho	mnoho	k4c4	mnoho
zásadních	zásadní	k2eAgFnPc2d1	zásadní
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Předně	předně	k6eAd1	předně
dovolila	dovolit	k5eAaPmAgFnS	dovolit
široké	široký	k2eAgNnSc4d1	široké
použití	použití	k1gNnSc4	použití
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
liturgii	liturgie	k1gFnSc6	liturgie
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
mešního	mešní	k2eAgInSc2d1	mešní
kánonu	kánon	k1gInSc2	kánon
<g/>
)	)	kIx)	)
i	i	k9	i
při	při	k7c6	při
udělování	udělování	k1gNnSc6	udělování
ostatních	ostatní	k2eAgFnPc2d1	ostatní
svátostí	svátost	k1gFnPc2	svátost
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
při	při	k7c6	při
modlitbách	modlitba	k1gFnPc6	modlitba
řeholníků	řeholník	k1gMnPc2	řeholník
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
římský	římský	k2eAgInSc1d1	římský
misál	misál	k1gInSc1	misál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
však	však	k9	však
šel	jít	k5eAaImAgMnS	jít
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
kromě	kromě	k7c2	kromě
tří	tři	k4xCgFnPc2	tři
nových	nový	k2eAgFnPc2d1	nová
eucharistických	eucharistický	k2eAgFnPc2d1	eucharistická
modliteb	modlitba	k1gFnPc2	modlitba
(	(	kIx(	(
<g/>
vycházejících	vycházející	k2eAgMnPc2d1	vycházející
ze	z	k7c2	z
starokřesťanských	starokřesťanský	k2eAgInPc2d1	starokřesťanský
formulářů	formulář	k1gInPc2	formulář
<g/>
)	)	kIx)	)
povolil	povolit	k5eAaPmAgInS	povolit
i	i	k9	i
mešní	mešní	k2eAgInSc1d1	mešní
kánon	kánon	k1gInSc1	kánon
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
změnou	změna	k1gFnSc7	změna
bylo	být	k5eAaImAgNnS	být
obrácení	obrácení	k1gNnSc1	obrácení
kněze	kněz	k1gMnSc2	kněz
čelem	čelo	k1gNnSc7	čelo
k	k	k7c3	k
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
ovšem	ovšem	k9	ovšem
přišla	přijít	k5eAaPmAgFnS	přijít
až	až	k6eAd1	až
po	po	k7c6	po
koncilu	koncil	k1gInSc6	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc4	koncil
nic	nic	k3yNnSc1	nic
takového	takový	k3xDgMnSc4	takový
nenařídil	nařídit	k5eNaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
koncilních	koncilní	k2eAgInPc6d1	koncilní
dokumentech	dokument	k1gInPc6	dokument
se	se	k3xPyFc4	se
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
změně	změna	k1gFnSc6	změna
nepíše	psát	k5eNaImIp3nS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
odpověď	odpověď	k1gFnSc4	odpověď
dala	dát	k5eAaPmAgFnS	dát
Kongregace	kongregace	k1gFnSc1	kongregace
pro	pro	k7c4	pro
božský	božský	k2eAgInSc4d1	božský
kult	kult	k1gInSc4	kult
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloužit	sloužit	k5eAaImF	sloužit
čelem	čelo	k1gNnSc7	čelo
k	k	k7c3	k
lidu	lid	k1gInSc3	lid
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
povinné	povinný	k2eAgNnSc1d1	povinné
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byla	být	k5eAaImAgFnS	být
umožněna	umožněn	k2eAgFnSc1d1	umožněna
koncelebrace	koncelebrace	k1gFnSc1	koncelebrace
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
společné	společný	k2eAgNnSc1d1	společné
slavení	slavení	k1gNnSc1	slavení
mše	mše	k1gFnSc2	mše
svaté	svatý	k2eAgFnSc2d1	svatá
více	hodně	k6eAd2	hodně
kněžími	kněz	k1gMnPc7	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Liturgie	liturgie	k1gFnSc1	liturgie
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
věřícím	věřící	k2eAgInSc7d1	věřící
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc1d2	veliký
rozsah	rozsah	k1gInSc1	rozsah
biblických	biblický	k2eAgNnPc2d1	biblické
čtení	čtení	k1gNnPc2	čtení
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
opakují	opakovat	k5eAaImIp3nP	opakovat
v	v	k7c6	v
tříletých	tříletý	k2eAgInPc6d1	tříletý
cyklech	cyklus	k1gInPc6	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Lektor	lektor	k1gMnSc1	lektor
se	se	k3xPyFc4	se
přemístil	přemístit	k5eAaPmAgMnS	přemístit
k	k	k7c3	k
nově	nově	k6eAd1	nově
zřízenému	zřízený	k2eAgInSc3d1	zřízený
ambonu	ambon	k1gInSc3	ambon
(	(	kIx(	(
<g/>
čtecí	čtecí	k2eAgInSc1d1	čtecí
pultík	pultík	k1gInSc1	pultík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
kazatelna	kazatelna	k1gFnSc1	kazatelna
stala	stát	k5eAaPmAgFnS	stát
nadbytečnou	nadbytečný	k2eAgFnSc7d1	nadbytečná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
ji	on	k3xPp3gFnSc4	on
nadále	nadále	k6eAd1	nadále
využívat	využívat	k5eAaPmF	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
liturgie	liturgie	k1gFnSc2	liturgie
byly	být	k5eAaImAgInP	být
odstraněny	odstranit	k5eAaPmNgInP	odstranit
mnohé	mnohý	k2eAgInPc1d1	mnohý
historické	historický	k2eAgInPc1d1	historický
nánosy	nános	k1gInPc1	nános
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
většina	většina	k1gFnSc1	většina
soukromých	soukromý	k2eAgFnPc2d1	soukromá
tichých	tichý	k2eAgFnPc2d1	tichá
modliteb	modlitba	k1gFnPc2	modlitba
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
umenšila	umenšit	k5eAaPmAgFnS	umenšit
se	se	k3xPyFc4	se
obřadnost	obřadnost	k1gFnSc1	obřadnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
pestrost	pestrost	k1gFnSc4	pestrost
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
upravilo	upravit	k5eAaPmAgNnS	upravit
nově	nově	k6eAd1	nově
užití	užití	k1gNnSc1	užití
chrámové	chrámový	k2eAgFnSc2d1	chrámová
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
mše	mše	k1gFnSc1	mše
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
koncertem	koncert	k1gInSc7	koncert
chrámového	chrámový	k2eAgInSc2d1	chrámový
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
provedení	provedení	k1gNnSc4	provedení
posvátné	posvátný	k2eAgFnSc2d1	posvátná
polyfonie	polyfonie	k1gFnSc2	polyfonie
vykládat	vykládat	k5eAaImF	vykládat
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
věřících	věřící	k2eAgMnPc2d1	věřící
byla	být	k5eAaImAgFnS	být
zapojena	zapojit	k5eAaPmNgFnS	zapojit
do	do	k7c2	do
společného	společný	k2eAgInSc2d1	společný
zpěvu	zpěv	k1gInSc2	zpěv
modliteb	modlitba	k1gFnPc2	modlitba
a	a	k8xC	a
mešních	mešní	k2eAgFnPc2d1	mešní
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
důsledkem	důsledek	k1gInSc7	důsledek
koncilu	koncil	k1gInSc2	koncil
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
sebepochopení	sebepochopení	k1gNnSc2	sebepochopení
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
sice	sice	k8xC	sice
ustalo	ustat	k5eAaPmAgNnS	ustat
pronásledování	pronásledování	k1gNnSc1	pronásledování
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
institucionalizovala	institucionalizovat	k5eAaImAgFnS	institucionalizovat
podle	podle	k7c2	podle
modelu	model	k1gInSc2	model
římského	římský	k2eAgNnSc2d1	římské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
hierarchie	hierarchie	k1gFnSc1	hierarchie
vzdalovala	vzdalovat	k5eAaImAgFnS	vzdalovat
laikům	laik	k1gMnPc3	laik
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Lutherovy	Lutherův	k2eAgInPc1d1	Lutherův
požadavky	požadavek	k1gInPc1	požadavek
reforem	reforma	k1gFnPc2	reforma
však	však	k9	však
způsobily	způsobit	k5eAaPmAgInP	způsobit
upevnění	upevnění	k1gNnSc4	upevnění
hierarchického	hierarchický	k2eAgNnSc2d1	hierarchické
pojetí	pojetí	k1gNnSc2	pojetí
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
církev	církev	k1gFnSc1	církev
chápala	chápat	k5eAaImAgFnS	chápat
sebe	sebe	k3xPyFc4	sebe
jako	jako	k9	jako
mocenský	mocenský	k2eAgInSc1d1	mocenský
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
nároky	nárok	k1gInPc4	nárok
a	a	k8xC	a
podle	podle	k7c2	podle
svých	svůj	k3xOyFgFnPc2	svůj
sil	síla	k1gFnPc2	síla
je	on	k3xPp3gNnSc4	on
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgMnSc6	tento
duchu	duch	k1gMnSc6	duch
také	také	k6eAd1	také
přípravná	přípravný	k2eAgFnSc1d1	přípravná
komise	komise	k1gFnSc1	komise
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
koncept	koncept	k1gInSc4	koncept
konstituce	konstituce	k1gFnSc2	konstituce
o	o	k7c6	o
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začínal	začínat	k5eAaImAgInS	začínat
bodem	bod	k1gInSc7	bod
Podstata	podstata	k1gFnSc1	podstata
církve	církev	k1gFnSc2	církev
bojující	bojující	k2eAgFnSc1d1	bojující
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
začala	začít	k5eAaPmAgFnS	začít
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Boží	boží	k2eAgInSc1d1	boží
lid	lid	k1gInSc1	lid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
vyzdvižena	vyzdvižen	k2eAgFnSc1d1	vyzdvižena
role	role	k1gFnSc1	role
"	"	kIx"	"
<g/>
laiků	laik	k1gMnPc2	laik
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přestalo	přestat	k5eAaPmAgNnS	přestat
rozlišování	rozlišování	k1gNnSc1	rozlišování
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
"	"	kIx"	"
<g/>
učící	učící	k2eAgNnPc4d1	učící
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
slyšící	slyšící	k2eAgMnSc1d1	slyšící
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Laikům	laik	k1gMnPc3	laik
je	být	k5eAaImIp3nS	být
přisouzen	přisouzen	k2eAgInSc1d1	přisouzen
úkol	úkol	k1gInSc1	úkol
apoštolátu	apoštolát	k1gInSc2	apoštolát
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
křtu	křest	k1gInSc2	křest
a	a	k8xC	a
biřmování	biřmování	k1gNnSc2	biřmování
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
až	až	k9	až
z	z	k7c2	z
pověření	pověření	k1gNnSc2	pověření
hierarchie	hierarchie	k1gFnSc2	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Laici	laik	k1gMnPc1	laik
mají	mít	k5eAaImIp3nP	mít
hledat	hledat	k5eAaImF	hledat
Boha	bůh	k1gMnSc4	bůh
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
povolání	povolání	k1gNnSc6	povolání
a	a	k8xC	a
posvěcovat	posvěcovat	k5eAaImF	posvěcovat
svět	svět	k1gInSc4	svět
svědectvím	svědectví	k1gNnSc7	svědectví
svého	svůj	k3xOyFgNnSc2	svůj
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
přestala	přestat	k5eAaPmAgFnS	přestat
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
spolek	spolek	k1gInSc1	spolek
vyvolených	vyvolená	k1gFnPc2	vyvolená
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Bůh	bůh	k1gMnSc1	bůh
zachraňuje	zachraňovat	k5eAaImIp3nS	zachraňovat
ze	z	k7c2	z
zkaženého	zkažený	k2eAgInSc2d1	zkažený
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
je	být	k5eAaImIp3nS	být
znamením	znamení	k1gNnSc7	znamení
jednoty	jednota	k1gFnSc2	jednota
lidstva	lidstvo	k1gNnSc2	lidstvo
s	s	k7c7	s
Bohem	bůh	k1gMnSc7	bůh
a	a	k8xC	a
všeho	všecek	k3xTgNnSc2	všecek
lidstva	lidstvo	k1gNnSc2	lidstvo
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
<s>
Uznala	uznat	k5eAaPmAgFnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
v	v	k7c6	v
nekřesťanských	křesťanský	k2eNgNnPc6d1	nekřesťanské
náboženstvích	náboženství	k1gNnPc6	náboženství
existuje	existovat	k5eAaImIp3nS	existovat
hledání	hledání	k1gNnSc1	hledání
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
že	že	k8xS	že
i	i	k9	i
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
Boží	boží	k2eAgFnSc4d1	boží
milost	milost	k1gFnSc4	milost
<g/>
,	,	kIx,	,
že	že	k8xS	že
trvalým	trvalý	k2eAgInSc7d1	trvalý
základem	základ	k1gInSc7	základ
křesťanství	křesťanství	k1gNnSc2	křesťanství
je	být	k5eAaImIp3nS	být
židovská	židovský	k2eAgFnSc1d1	židovská
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
nekatolické	katolický	k2eNgFnPc1d1	nekatolická
církve	církev	k1gFnPc1	církev
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
církve	církev	k1gFnSc2	církev
univerzální	univerzální	k2eAgFnSc2d1	univerzální
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Karla	Karel	k1gMnSc2	Karel
Rahnera	Rahner	k1gMnSc2	Rahner
stala	stát	k5eAaPmAgFnS	stát
poprvé	poprvé	k6eAd1	poprvé
církví	církev	k1gFnSc7	církev
světovou	světový	k2eAgFnSc7d1	světová
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
etapy	etapa	k1gFnSc2	etapa
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
(	(	kIx(	(
<g/>
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnSc6d1	krátká
etapě	etapa	k1gFnSc6	etapa
církve	církev	k1gFnSc2	církev
židovského	židovský	k2eAgNnSc2d1	Židovské
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
etapě	etapa	k1gFnSc6	etapa
pohanského	pohanský	k2eAgNnSc2d1	pohanské
křesťanství	křesťanství	k1gNnSc2	křesťanství
Západu	západ	k1gInSc2	západ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
byla	být	k5eAaImAgFnS	být
území	území	k1gNnSc4	území
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
misijní	misijní	k2eAgMnPc4d1	misijní
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
biskupové	biskup	k1gMnPc1	biskup
podléhali	podléhat	k5eAaImAgMnP	podléhat
Kongregaci	kongregace	k1gFnSc4	kongregace
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
podle	podle	k7c2	podle
Kodexu	kodex	k1gInSc2	kodex
církevního	církevní	k2eAgNnSc2d1	církevní
práva	právo	k1gNnSc2	právo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
jsou	být	k5eAaImIp3nP	být
misijní	misijní	k2eAgNnSc4d1	misijní
biskupství	biskupství	k1gNnSc4	biskupství
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
<g/>
,	,	kIx,	,
rovnoprávnými	rovnoprávný	k2eAgFnPc7d1	rovnoprávná
církvemi	církev	k1gFnPc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
církev	církev	k1gFnSc1	církev
nejvíce	hodně	k6eAd3	hodně
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
a	a	k8xC	a
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
myšlenky	myšlenka	k1gFnPc4	myšlenka
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
chudých	chudý	k2eAgInPc2d1	chudý
<g/>
,	,	kIx,	,
pronásledovaných	pronásledovaný	k2eAgInPc2d1	pronásledovaný
a	a	k8xC	a
bezbranných	bezbranný	k2eAgInPc2d1	bezbranný
<g/>
.	.	kIx.	.
</s>
<s>
Odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
pohrdání	pohrdání	k1gNnSc4	pohrdání
lidskou	lidský	k2eAgFnSc7d1	lidská
důstojností	důstojnost	k1gFnSc7	důstojnost
<g/>
,	,	kIx,	,
zabíjení	zabíjení	k1gNnSc1	zabíjení
v	v	k7c6	v
ničivých	ničivý	k2eAgFnPc6d1	ničivá
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
umělé	umělý	k2eAgInPc1d1	umělý
potraty	potrat	k1gInPc1	potrat
i	i	k8xC	i
nespravedlnosti	nespravedlnost	k1gFnPc1	nespravedlnost
politických	politický	k2eAgInPc2d1	politický
a	a	k8xC	a
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Závěry	závěr	k1gInPc1	závěr
koncilu	koncil	k1gInSc2	koncil
nebyly	být	k5eNaImAgInP	být
všude	všude	k6eAd1	všude
a	a	k8xC	a
všemi	všecek	k3xTgFnPc7	všecek
přijaty	přijmout	k5eAaPmNgInP	přijmout
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
odmítají	odmítat	k5eAaImIp3nP	odmítat
přijmout	přijmout	k5eAaPmF	přijmout
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
koncil	koncil	k1gInSc1	koncil
přinesl	přinést	k5eAaPmAgInS	přinést
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
označováni	označován	k2eAgMnPc1d1	označován
jako	jako	k9	jako
tradicionalisté	tradicionalista	k1gMnPc1	tradicionalista
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
dokonce	dokonce	k9	dokonce
označují	označovat	k5eAaImIp3nP	označovat
učení	učení	k1gNnSc4	učení
koncilu	koncil	k1gInSc2	koncil
jako	jako	k8xS	jako
heretické	heretický	k2eAgFnPc1d1	heretická
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
tradicionalisté	tradicionalista	k1gMnPc1	tradicionalista
zdůvodňují	zdůvodňovat	k5eAaImIp3nP	zdůvodňovat
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Otcové	otec	k1gMnPc1	otec
koncilu	koncil	k1gInSc2	koncil
se	se	k3xPyFc4	se
do	do	k7c2	do
dění	dění	k1gNnSc2	dění
zapojovali	zapojovat	k5eAaImAgMnP	zapojovat
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
různým	různý	k2eAgNnSc7d1	různé
nasazením	nasazení	k1gNnSc7	nasazení
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgMnPc1d1	aktivní
byli	být	k5eAaImAgMnP	být
především	především	k9	především
biskupové	biskup	k1gMnPc1	biskup
tzv.	tzv.	kA	tzv.
Rýnské	rýnský	k2eAgFnSc2d1	Rýnská
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
názory	názor	k1gInPc4	názor
liberálních	liberální	k2eAgFnPc2d1	liberální
<g/>
,	,	kIx,	,
především	především	k9	především
německých	německý	k2eAgMnPc2d1	německý
teologů	teolog	k1gMnPc2	teolog
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
i	i	k9	i
vliv	vliv	k1gInSc4	vliv
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
jinověrců	jinověrec	k1gMnPc2	jinověrec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
předchozích	předchozí	k2eAgInPc2d1	předchozí
koncilů	koncil	k1gInPc2	koncil
nemyslitelné	myslitelný	k2eNgNnSc1d1	nemyslitelné
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
koncilu	koncil	k1gInSc2	koncil
chyběli	chybět	k5eAaImAgMnP	chybět
světci	světec	k1gMnPc1	světec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
za	za	k7c4	za
průběh	průběh	k1gInSc4	průběh
koncilu	koncil	k1gInSc2	koncil
modlili	modlit	k5eAaImAgMnP	modlit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
po	po	k7c6	po
koncilu	koncil	k1gInSc6	koncil
nastal	nastat	k5eAaPmAgInS	nastat
masivní	masivní	k2eAgInSc1d1	masivní
odpad	odpad	k1gInSc1	odpad
kněží	kněz	k1gMnPc2	kněz
i	i	k8xC	i
řeholníků	řeholník	k1gMnPc2	řeholník
<g/>
.	.	kIx.	.
</s>
<s>
Koncilní	koncilní	k2eAgInPc1d1	koncilní
dokumenty	dokument	k1gInPc1	dokument
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
jako	jako	k9	jako
mnohomluvné	mnohomluvný	k2eAgFnPc1d1	mnohomluvná
<g/>
,	,	kIx,	,
neurčité	určitý	k2eNgFnPc1d1	neurčitá
<g/>
,	,	kIx,	,
nehomogenní	homogenní	k2eNgFnPc1d1	nehomogenní
<g/>
,	,	kIx,	,
nepřesné	přesný	k2eNgFnPc1d1	nepřesná
a	a	k8xC	a
právně	právně	k6eAd1	právně
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
nekřesťanským	křesťanský	k2eNgFnPc3d1	nekřesťanská
náboženstvím	náboženství	k1gNnSc7	náboženství
církev	církev	k1gFnSc1	církev
vyklízí	vyklízet	k5eAaImIp3nS	vyklízet
pozice	pozice	k1gFnPc4	pozice
a	a	k8xC	a
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
náhrady	náhrada	k1gFnSc2	náhrada
výsadního	výsadní	k2eAgNnSc2d1	výsadní
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
ke	k	k7c3	k
státu	stát	k1gInSc3	stát
v	v	k7c6	v
tradičně	tradičně	k6eAd1	tradičně
katolických	katolický	k2eAgFnPc6d1	katolická
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dekret	dekret	k1gInSc1	dekret
o	o	k7c6	o
ekumenismu	ekumenismus	k1gInSc6	ekumenismus
mylně	mylně	k6eAd1	mylně
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
nekatolické	katolický	k2eNgFnPc1d1	nekatolická
církve	církev	k1gFnPc1	církev
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nástrojem	nástroj	k1gInSc7	nástroj
spásy	spása	k1gFnSc2	spása
<g/>
.	.	kIx.	.
</s>
<s>
Věroučná	věroučný	k2eAgFnSc1d1	věroučná
tradice	tradice	k1gFnSc1	tradice
je	být	k5eAaImIp3nS	být
zatlačena	zatlačit	k5eAaPmNgFnS	zatlačit
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
a	a	k8xC	a
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
Písmo	písmo	k1gNnSc1	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Proklamovaná	proklamovaný	k2eAgFnSc1d1	proklamovaná
kolegialita	kolegialita	k1gFnSc1	kolegialita
biskupů	biskup	k1gMnPc2	biskup
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
podle	podle	k7c2	podle
tradicionalistů	tradicionalista	k1gMnPc2	tradicionalista
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
téměř	téměř	k6eAd1	téměř
žádnou	žádný	k3yNgFnSc4	žádný
pozornost	pozornost	k1gFnSc4	pozornost
obhajobě	obhajoba	k1gFnSc3	obhajoba
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
nerozpoznal	rozpoznat	k5eNaPmAgMnS	rozpoznat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
náboženského	náboženský	k2eAgInSc2d1	náboženský
relativismu	relativismus	k1gInSc2	relativismus
<g/>
.	.	kIx.	.
</s>
<s>
Nedal	dát	k5eNaPmAgMnS	dát
katolíkům	katolík	k1gMnPc3	katolík
jasné	jasný	k2eAgFnPc4d1	jasná
mravní	mravní	k2eAgFnPc4d1	mravní
normy	norma	k1gFnPc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
otevřel	otevřít	k5eAaPmAgInS	otevřít
cestu	cesta	k1gFnSc4	cesta
ke	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
liturgie	liturgie	k1gFnSc2	liturgie
a	a	k8xC	a
jejímu	její	k3xOp3gNnSc3	její
ochuzení	ochuzení	k1gNnSc1	ochuzení
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
podstatných	podstatný	k2eAgInPc2d1	podstatný
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
kritiků	kritik	k1gMnPc2	kritik
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
prohlubující	prohlubující	k2eAgFnSc4d1	prohlubující
se	se	k3xPyFc4	se
krizi	krize	k1gFnSc4	krize
církve	církev	k1gFnPc1	církev
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
koncil	koncil	k1gInSc1	koncil
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
závěry	závěr	k1gInPc1	závěr
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnSc7	její
příčinou	příčina	k1gFnSc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
profesor	profesor	k1gMnSc1	profesor
Roberto	Roberta	k1gFnSc5	Roberta
de	de	k?	de
Mattei	Matte	k1gInSc6	Matte
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
pastoračního	pastorační	k2eAgNnSc2d1	pastorační
hlediska	hledisko	k1gNnSc2	hledisko
skončily	skončit	k5eAaPmAgFnP	skončit
metody	metoda	k1gFnPc1	metoda
koncilu	koncil	k1gInSc2	koncil
naprostým	naprostý	k2eAgNnSc7d1	naprosté
fiaskem	fiasko	k1gNnSc7	fiasko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
upustit	upustit	k5eAaPmF	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
koncilu	koncil	k1gInSc2	koncil
ovšem	ovšem	k9	ovšem
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
reforma	reforma	k1gFnSc1	reforma
byla	být	k5eAaImAgFnS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
<g/>
.	.	kIx.	.
</s>
<s>
Ku	k	k7c3	k
příkladu	příklad	k1gInSc3	příklad
jezuita	jezuita	k1gMnSc1	jezuita
Petr	Petr	k1gMnSc1	Petr
Kolář	Kolář	k1gMnSc1	Kolář
o	o	k7c6	o
tradicionalistech	tradicionalista	k1gMnPc6	tradicionalista
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
svým	svůj	k3xOyFgNnSc7	svůj
odmítáním	odmítání	k1gNnSc7	odmítání
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
jen	jen	k9	jen
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
velkou	velký	k2eAgFnSc4d1	velká
reformu	reforma	k1gFnSc4	reforma
pozadu	pozadu	k6eAd1	pozadu
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nejviditelnějším	viditelný	k2eAgInSc7d3	nejviditelnější
projevem	projev	k1gInSc7	projev
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c7	mezi
modernistickým	modernistický	k2eAgInSc7d1	modernistický
a	a	k8xC	a
tradicionalistickým	tradicionalistický	k2eAgInSc7d1	tradicionalistický
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc1	založení
Kněžského	kněžský	k2eAgNnSc2d1	kněžské
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
svatého	svatý	k2eAgMnSc2d1	svatý
Pia	Pius	k1gMnSc2	Pius
X.	X.	kA	X.
dakarským	dakarský	k2eAgMnSc7d1	dakarský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
monsignorem	monsignore	k1gMnSc7	monsignore
Lefebvrem	Lefebvr	k1gMnSc7	Lefebvr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozkolu	rozkol	k1gInSc3	rozkol
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
stárnoucí	stárnoucí	k2eAgInSc4d1	stárnoucí
Mons	Mons	k1gInSc4	Mons
<g/>
.	.	kIx.	.
</s>
<s>
Lefebvre	Lefebvr	k1gMnSc5	Lefebvr
po	po	k7c6	po
marných	marný	k2eAgNnPc6d1	marné
jednáních	jednání	k1gNnPc6	jednání
s	s	k7c7	s
Římem	Řím	k1gInSc7	Řím
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
čtyři	čtyři	k4xCgMnPc4	čtyři
biskupy	biskup	k1gMnPc4	biskup
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
poté	poté	k6eAd1	poté
světící	světící	k2eAgInPc4d1	světící
i	i	k8xC	i
vysvěcené	vysvěcený	k2eAgInPc4d1	vysvěcený
biskupy	biskup	k1gInPc4	biskup
exkomunikoval	exkomunikovat	k5eAaBmAgMnS	exkomunikovat
<g/>
,	,	kIx,	,
exkomunikaci	exkomunikace	k1gFnSc4	exkomunikace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Benedikt	benedikt	k1gInSc1	benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2010	[number]	k4	2010
zaslal	zaslat	k5eAaPmAgMnS	zaslat
monsignor	monsignor	k1gMnSc1	monsignor
Gherardini	Gherardin	k2eAgMnPc1d1	Gherardin
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
profesor	profesor	k1gMnSc1	profesor
eklesiologie	eklesiologie	k1gFnSc2	eklesiologie
na	na	k7c6	na
Lateránské	lateránský	k2eAgFnSc6d1	Lateránská
univerzitě	univerzita	k1gFnSc6	univerzita
papeži	papež	k1gMnSc3	papež
petici	petice	k1gFnSc4	petice
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
některými	některý	k3yIgMnPc7	některý
katolickými	katolický	k2eAgMnPc7d1	katolický
teology	teolog	k1gMnPc7	teolog
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
osobnostmi	osobnost	k1gFnPc7	osobnost
činnými	činný	k2eAgFnPc7d1	činná
v	v	k7c6	v
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
petice	petice	k1gFnSc1	petice
požaduje	požadovat	k5eAaImIp3nS	požadovat
provést	provést	k5eAaPmF	provést
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
analýzu	analýza	k1gFnSc4	analýza
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Požaduje	požadovat	k5eAaImIp3nS	požadovat
například	například	k6eAd1	například
prověření	prověření	k1gNnSc1	prověření
dogmatičnosti	dogmatičnost	k1gFnSc2	dogmatičnost
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
definice	definice	k1gFnSc2	definice
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
pojetí	pojetí	k1gNnSc1	pojetí
kolegiality	kolegialita	k1gFnSc2	kolegialita
biskupů	biskup	k1gInPc2	biskup
<g/>
,	,	kIx,	,
kněžství	kněžství	k1gNnSc2	kněžství
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
nesmělo	smět	k5eNaImAgNnS	smět
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
diskutovat	diskutovat	k5eAaImF	diskutovat
a	a	k8xC	a
které	který	k3yIgInPc1	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
přenechány	přenechat	k5eAaPmNgInP	přenechat
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
papeži	papež	k1gMnSc3	papež
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
rozčarování	rozčarování	k1gNnSc3	rozčarování
zejména	zejména	k9	zejména
z	z	k7c2	z
encykliky	encyklika	k1gFnSc2	encyklika
Humanae	Humana	k1gFnSc2	Humana
vitae	vita	k1gFnPc4	vita
vydané	vydaný	k2eAgFnPc4d1	vydaná
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
encyklika	encyklika	k1gFnSc1	encyklika
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
říká	říkat	k5eAaImIp3nS	říkat
mnoho	mnoho	k6eAd1	mnoho
hodnotného	hodnotný	k2eAgNnSc2d1	hodnotné
a	a	k8xC	a
dobrého	dobré	k1gNnSc2	dobré
o	o	k7c4	o
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
přinesla	přinést	k5eAaPmAgFnS	přinést
mnoha	mnoho	k4c3	mnoho
katolíkům	katolík	k1gMnPc3	katolík
zklamání	zklamání	k1gNnSc2	zklamání
naděje	naděje	k1gFnSc2	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
plánování	plánování	k1gNnSc2	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgInPc4d1	spojený
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
mohla	moct	k5eAaImAgFnS	moct
zaujmout	zaujmout	k5eAaPmF	zaujmout
diferencovanější	diferencovaný	k2eAgInSc4d2	diferencovanější
postoj	postoj	k1gInSc4	postoj
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
použitelných	použitelný	k2eAgFnPc2d1	použitelná
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
zde	zde	k6eAd1	zde
tak	tak	k6eAd1	tak
rozpor	rozpor	k1gInSc1	rozpor
mezi	mezi	k7c7	mezi
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
příslušníků	příslušník	k1gMnPc2	příslušník
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
reprezentanty	reprezentant	k1gMnPc7	reprezentant
učitelského	učitelský	k2eAgInSc2d1	učitelský
úřadu	úřad	k1gInSc2	úřad
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
se	se	k3xPyFc4	se
nedomnívají	domnívat	k5eNaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
vůbec	vůbec	k9	vůbec
příslušelo	příslušet	k5eAaImAgNnS	příslušet
učitelskému	učitelský	k2eAgInSc3d1	učitelský
úřadu	úřad	k1gInSc3	úřad
o	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
věcech	věc	k1gFnPc6	věc
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
a	a	k8xC	a
ani	ani	k8xC	ani
je	on	k3xPp3gInPc4	on
nenapadne	napadnout	k5eNaPmIp3nS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
rozpor	rozpor	k1gInSc4	rozpor
s	s	k7c7	s
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
učením	učení	k1gNnSc7	učení
neměli	mít	k5eNaImAgMnP	mít
ani	ani	k8xC	ani
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
příslušníky	příslušník	k1gMnPc4	příslušník
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Obránci	obránce	k1gMnPc1	obránce
encykliky	encyklika	k1gFnSc2	encyklika
pak	pak	k6eAd1	pak
berou	brát	k5eAaImIp3nP	brát
tento	tento	k3xDgInSc4	tento
postoj	postoj	k1gInSc4	postoj
jako	jako	k8xC	jako
útok	útok	k1gInSc4	útok
na	na	k7c4	na
ústřední	ústřední	k2eAgInSc4d1	ústřední
článek	článek	k1gInSc4	článek
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
na	na	k7c4	na
samotného	samotný	k2eAgMnSc4d1	samotný
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
otázku	otázka	k1gFnSc4	otázka
věroučnou	věroučný	k2eAgFnSc4d1	věroučná
<g/>
,	,	kIx,	,
nenastal	nastat	k5eNaPmAgInS	nastat
prakticky	prakticky	k6eAd1	prakticky
žádný	žádný	k3yNgInSc1	žádný
posun	posun	k1gInSc1	posun
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
i	i	k8xC	i
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
vyvazovali	vyvazovat	k5eAaImAgMnP	vyvazovat
kněze	kněz	k1gMnPc4	kněz
z	z	k7c2	z
povinnosti	povinnost	k1gFnSc2	povinnost
celibátu	celibát	k1gInSc2	celibát
a	a	k8xC	a
umožňovali	umožňovat	k5eAaImAgMnP	umožňovat
jim	on	k3xPp3gMnPc3	on
církevní	církevní	k2eAgInSc4d1	církevní
sňatek	sňatek	k1gInSc4	sňatek
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byli	být	k5eAaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
složit	složit	k5eAaPmF	složit
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
praxi	praxe	k1gFnSc4	praxe
zastavil	zastavit	k5eAaPmAgMnS	zastavit
a	a	k8xC	a
umožňoval	umožňovat	k5eAaImAgMnS	umožňovat
převedení	převedení	k1gNnSc4	převedení
do	do	k7c2	do
laického	laický	k2eAgInSc2d1	laický
stavu	stav	k1gInSc2	stav
pouze	pouze	k6eAd1	pouze
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
přijetí	přijetí	k1gNnSc1	přijetí
svěcení	svěcení	k1gNnSc2	svěcení
z	z	k7c2	z
donucení	donucení	k1gNnSc4	donucení
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
církevní	církevní	k2eAgNnSc1d1	církevní
právo	právo	k1gNnSc1	právo
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
uznává	uznávat	k5eAaImIp3nS	uznávat
i	i	k9	i
nátlak	nátlak	k1gInSc1	nátlak
psychický	psychický	k2eAgInSc1d1	psychický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
koncil	koncil	k1gInSc1	koncil
přinesl	přinést	k5eAaPmAgInS	přinést
převratné	převratný	k2eAgFnPc4d1	převratná
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
liturgii	liturgie	k1gFnSc6	liturgie
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
vývojové	vývojový	k2eAgFnPc1d1	vývojová
tendence	tendence	k1gFnPc1	tendence
byly	být	k5eAaImAgFnP	být
zcela	zcela	k6eAd1	zcela
zastaveny	zastaven	k2eAgFnPc1d1	zastavena
-	-	kIx~	-
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
společná	společný	k2eAgFnSc1d1	společná
zpověď	zpověď	k1gFnSc1	zpověď
<g/>
,	,	kIx,	,
společné	společný	k2eAgNnSc4d1	společné
slavení	slavení	k1gNnSc4	slavení
Večeře	večeře	k1gFnSc1	večeře
Páně	páně	k2eAgFnSc1d1	páně
s	s	k7c7	s
reformačními	reformační	k2eAgFnPc7d1	reformační
církvemi	církev	k1gFnPc7	církev
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ekumenické	ekumenický	k2eAgInPc4d1	ekumenický
<g/>
"	"	kIx"	"
sňatky	sňatek	k1gInPc4	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Liturgickou	liturgický	k2eAgFnSc4d1	liturgická
reformu	reforma	k1gFnSc4	reforma
považuje	považovat	k5eAaImIp3nS	považovat
Řím	Řím	k1gInSc1	Řím
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
nového	nový	k2eAgInSc2d1	nový
misálu	misál	k1gInSc2	misál
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
za	za	k7c4	za
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
odchylek	odchylka	k1gFnPc2	odchylka
zakázána	zakázán	k2eAgFnSc1d1	zakázána
tanzanijská	tanzanijský	k2eAgFnSc1d1	tanzanijská
liturgie	liturgie	k1gFnSc1	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
nebyla	být	k5eNaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
i	i	k9	i
s	s	k7c7	s
výhradami	výhrada	k1gFnPc7	výhrada
<g/>
,	,	kIx,	,
upřesněními	upřesnění	k1gNnPc7	upřesnění
a	a	k8xC	a
úpravami	úprava	k1gFnPc7	úprava
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
limská	limský	k2eAgFnSc1d1	Limská
liturgie	liturgie	k1gFnSc1	liturgie
<g/>
"	"	kIx"	"
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
Komisí	komise	k1gFnSc7	komise
pro	pro	k7c4	pro
víru	víra	k1gFnSc4	víra
a	a	k8xC	a
řád	řád	k1gInSc4	řád
Světové	světový	k2eAgFnSc2d1	světová
rady	rada	k1gFnSc2	rada
církví	církev	k1gFnPc2	církev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zvlášť	zvlášť	k6eAd1	zvlášť
slavnostní	slavnostní	k2eAgFnSc7d1	slavnostní
formou	forma	k1gFnSc7	forma
liturgie	liturgie	k1gFnSc2	liturgie
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
hudební	hudební	k2eAgInSc4d1	hudební
a	a	k8xC	a
textové	textový	k2eAgInPc4d1	textový
prvky	prvek	k1gInPc4	prvek
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
pokoncilní	pokoncilní	k2eAgFnSc4d1	pokoncilní
liturgii	liturgie	k1gFnSc4	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
mladý	mladý	k2eAgMnSc1d1	mladý
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
peritus	peritus	k1gMnSc1	peritus
na	na	k7c6	na
koncilu	koncil	k1gInSc6	koncil
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
apoštolský	apoštolský	k2eAgInSc1d1	apoštolský
list	list	k1gInSc1	list
-	-	kIx~	-
motu	moto	k1gNnSc3	moto
proprio	proprio	k1gNnSc1	proprio
<g/>
:	:	kIx,	:
Summorum	Summorum	k1gInSc1	Summorum
pontificum	pontificum	k1gInSc1	pontificum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
slavení	slavení	k1gNnSc2	slavení
starší	starý	k2eAgFnPc4d2	starší
liturgické	liturgický	k2eAgFnPc4d1	liturgická
formy	forma	k1gFnPc4	forma
podle	podle	k7c2	podle
misálu	misál	k1gInSc2	misál
Jana	Jan	k1gMnSc2	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tridentská	tridentský	k2eAgFnSc1d1	tridentská
mše	mše	k1gFnSc1	mše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ustanovuje	ustanovovat	k5eAaImIp3nS	ustanovovat
jako	jako	k9	jako
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
formu	forma	k1gFnSc4	forma
římského	římský	k2eAgInSc2d1	římský
ritu	rit	k1gInSc2	rit
(	(	kIx(	(
<g/>
forma	forma	k1gFnSc1	forma
extraordinaria	extraordinarium	k1gNnSc2	extraordinarium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
možnost	možnost	k1gFnSc4	možnost
slavení	slavení	k1gNnSc2	slavení
podle	podle	k7c2	podle
staršího	starý	k2eAgInSc2d2	starší
způsobu	způsob	k1gInSc2	způsob
oproti	oproti	k7c3	oproti
dřívějšku	dřívějšek	k1gInSc3	dřívějšek
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
koncil	koncil	k1gInSc1	koncil
říkal	říkat	k5eAaImAgInS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
hlubšího	hluboký	k2eAgInSc2d2	hlubší
vzhledu	vzhled	k1gInSc2	vzhled
do	do	k7c2	do
zjevení	zjevení	k1gNnSc2	zjevení
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
vždy	vždy	k6eAd1	vždy
znovu	znovu	k6eAd1	znovu
interpretovat	interpretovat	k5eAaBmF	interpretovat
<g/>
,	,	kIx,	,
Kongregace	kongregace	k1gFnSc1	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
vydala	vydat	k5eAaPmAgFnS	vydat
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
instrukci	instrukce	k1gFnSc3	instrukce
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
musí	muset	k5eAaImIp3nS	muset
všichni	všechen	k3xTgMnPc1	všechen
budoucí	budoucí	k2eAgMnPc1d1	budoucí
nositelé	nositel	k1gMnPc1	nositel
vyšších	vysoký	k2eAgInPc2d2	vyšší
církevních	církevní	k2eAgInPc2d1	církevní
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
učí	učit	k5eAaImIp3nS	učit
z	z	k7c2	z
církevního	církevní	k2eAgNnSc2d1	církevní
pověření	pověření	k1gNnSc2	pověření
<g/>
,	,	kIx,	,
skládat	skládat	k5eAaImF	skládat
přísahu	přísaha	k1gFnSc4	přísaha
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
teolog	teolog	k1gMnSc1	teolog
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
intelektuální	intelektuální	k2eAgFnSc7d1	intelektuální
integritou	integrita	k1gFnSc7	integrita
<g/>
"	"	kIx"	"
a	a	k8xC	a
oficiální	oficiální	k2eAgFnSc7d1	oficiální
naukou	nauka	k1gFnSc7	nauka
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
jej	on	k3xPp3gMnSc4	on
vynést	vynést	k5eAaPmF	vynést
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
do	do	k7c2	do
hromadných	hromadný	k2eAgInPc2d1	hromadný
sdělovacích	sdělovací	k2eAgInPc2d1	sdělovací
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
nejen	nejen	k6eAd1	nejen
dogmat	dogma	k1gNnPc2	dogma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
"	"	kIx"	"
<g/>
autentické	autentický	k2eAgFnSc2d1	autentická
<g/>
"	"	kIx"	"
nauky	nauka	k1gFnSc2	nauka
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
definitivně	definitivně	k6eAd1	definitivně
neomylně	omylně	k6eNd1	omylně
závaznou	závazný	k2eAgFnSc4d1	závazná
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
vlastně	vlastně	k9	vlastně
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
cokoliv	cokoliv	k3yInSc1	cokoliv
měnit	měnit	k5eAaImF	měnit
na	na	k7c6	na
současné	současný	k2eAgFnSc6d1	současná
církevní	církevní	k2eAgFnSc6d1	církevní
nauce	nauka	k1gFnSc6	nauka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
změna	změna	k1gFnSc1	změna
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
větší	veliký	k2eAgFnSc4d2	veliký
decentralizaci	decentralizace	k1gFnSc4	decentralizace
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
větší	veliký	k2eAgInSc4d2	veliký
ohledy	ohled	k1gInPc4	ohled
na	na	k7c4	na
specifika	specifikon	k1gNnPc4	specifikon
místních	místní	k2eAgFnPc2d1	místní
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Praxe	praxe	k1gFnSc1	praxe
biskupských	biskupský	k2eAgInPc2d1	biskupský
synodů	synod	k1gInPc2	synod
ale	ale	k8xC	ale
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
papež	papež	k1gMnSc1	papež
pouze	pouze	k6eAd1	pouze
pasivně	pasivně	k6eAd1	pasivně
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
a	a	k8xC	a
poté	poté	k6eAd1	poté
sám	sám	k3xTgMnSc1	sám
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
církevní	církevní	k2eAgNnSc1d1	církevní
právo	právo	k1gNnSc1	právo
posílilo	posílit	k5eAaPmAgNnS	posílit
pravomoci	pravomoc	k1gFnSc6	pravomoc
papeže	papež	k1gMnSc4	papež
-	-	kIx~	-
ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
koncil	koncil	k1gInSc1	koncil
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
vykonávání	vykonávání	k1gNnSc2	vykonávání
"	"	kIx"	"
<g/>
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
řídící	řídící	k2eAgFnSc2d1	řídící
moci	moc	k1gFnSc2	moc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
papež	papež	k1gMnSc1	papež
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
se	se	k3xPyFc4	se
stejnou	stejný	k2eAgFnSc7d1	stejná
oprávněností	oprávněnost	k1gFnSc7	oprávněnost
jako	jako	k9	jako
koncil	koncil	k1gInSc1	koncil
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
není	být	k5eNaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
volit	volit	k5eAaImF	volit
složitější	složitý	k2eAgFnSc4d2	složitější
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
cesta	cesta	k1gFnSc1	cesta
mnohem	mnohem	k6eAd1	mnohem
snadnější	snadný	k2eAgFnSc1d2	snazší
<g/>
.	.	kIx.	.
</s>
<s>
Ekumenický	ekumenický	k2eAgInSc1d1	ekumenický
dialog	dialog	k1gInSc1	dialog
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
některých	některý	k3yIgInPc2	některý
formálních	formální	k2eAgInPc2d1	formální
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
impulsů	impuls	k1gInPc2	impuls
byla	být	k5eAaImAgFnS	být
nová	nový	k2eAgFnSc1d1	nová
instrukce	instrukce	k1gFnSc1	instrukce
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
vydaná	vydaný	k2eAgNnPc4d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
450	[number]	k4	450
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
Lutherových	Lutherův	k2eAgFnPc2d1	Lutherova
tezí	teze	k1gFnPc2	teze
o	o	k7c6	o
odpustcích	odpustek	k1gInPc6	odpustek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	léto	k1gNnPc6	léto
nezájmu	nezájem	k1gInSc2	nezájem
z	z	k7c2	z
katolické	katolický	k2eAgFnSc2d1	katolická
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
reformační	reformační	k2eAgFnSc2d1	reformační
církve	církev	k1gFnSc2	církev
vracet	vracet	k5eAaImF	vracet
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
praxím	praxe	k1gFnPc3	praxe
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dočasně	dočasně	k6eAd1	dočasně
po	po	k7c6	po
koncilu	koncil	k1gInSc6	koncil
zastavily	zastavit	k5eAaPmAgFnP	zastavit
-	-	kIx~	-
svěcení	svěcení	k1gNnPc1	svěcení
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
anglikánské	anglikánský	k2eAgFnSc6d1	anglikánská
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
volby	volba	k1gFnPc4	volba
biskupek	biskupka	k1gFnPc2	biskupka
v	v	k7c6	v
luteránských	luteránský	k2eAgFnPc6d1	luteránská
církvích	církev	k1gFnPc6	církev
<g/>
...	...	k?	...
Karl	Karl	k1gMnSc1	Karl
Rahner	Rahner	k1gMnSc1	Rahner
formuloval	formulovat	k5eAaImAgMnS	formulovat
následující	následující	k2eAgInPc4d1	následující
úkoly	úkol	k1gInPc4	úkol
vyplývající	vyplývající	k2eAgInPc4d1	vyplývající
z	z	k7c2	z
koncilu	koncil	k1gInSc2	koncil
<g/>
:	:	kIx,	:
Odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
ateismus	ateismus	k1gInSc4	ateismus
<g/>
:	:	kIx,	:
Chybí	chybit	k5eAaPmIp3nS	chybit
zde	zde	k6eAd1	zde
stále	stále	k6eAd1	stále
teologická	teologický	k2eAgFnSc1d1	teologická
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
význam	význam	k1gInSc4	význam
ateismu	ateismus	k1gInSc2	ateismus
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
koncil	koncil	k1gInSc1	koncil
ateismus	ateismus	k1gInSc1	ateismus
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
<g/>
,	,	kIx,	,
nedává	dávat	k5eNaImIp3nS	dávat
žádnou	žádný	k3yNgFnSc4	žádný
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
i	i	k9	i
ateista	ateista	k1gMnSc1	ateista
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
spásy	spása	k1gFnPc4	spása
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Boží	boží	k2eAgFnSc2d1	boží
milosti	milost	k1gFnSc2	milost
Inkulturace	inkulturace	k1gFnSc2	inkulturace
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
:	:	kIx,	:
Nelze	lze	k6eNd1	lze
se	se	k3xPyFc4	se
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stačí	stačit	k5eAaBmIp3nS	stačit
přeložit	přeložit	k5eAaPmF	přeložit
liturgické	liturgický	k2eAgInPc4d1	liturgický
texty	text	k1gInPc4	text
do	do	k7c2	do
příslušného	příslušný	k2eAgInSc2d1	příslušný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
působí	působit	k5eAaImIp3nS	působit
dojmem	dojem	k1gInSc7	dojem
importu	import	k1gInSc2	import
západní	západní	k2eAgFnSc2d1	západní
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Kolegialita	kolegialita	k1gFnSc1	kolegialita
biskupů	biskup	k1gMnPc2	biskup
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyřešit	vyřešit	k5eAaPmF	vyřešit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zajistit	zajistit	k5eAaPmF	zajistit
společnou	společný	k2eAgFnSc4d1	společná
existenci	existence	k1gFnSc4	existence
dvou	dva	k4xCgInPc2	dva
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
subjektů	subjekt	k1gInPc2	subjekt
moci	moct	k5eAaImF	moct
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
-	-	kIx~	-
kolegia	kolegium	k1gNnPc1	kolegium
biskupů	biskup	k1gInPc2	biskup
a	a	k8xC	a
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
papež	papež	k1gMnSc1	papež
jednal	jednat	k5eAaImAgMnS	jednat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
biskupy	biskup	k1gMnPc7	biskup
jako	jako	k8xS	jako
hlava	hlava	k1gFnSc1	hlava
jejich	jejich	k3xOp3gNnSc2	jejich
kolegia	kolegium	k1gNnSc2	kolegium
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
<g/>
?	?	kIx.	?
</s>
<s>
Revidovatelnost	Revidovatelnost	k1gFnSc1	Revidovatelnost
a	a	k8xC	a
diferencovaná	diferencovaný	k2eAgFnSc1d1	diferencovaná
závaznost	závaznost	k1gFnSc1	závaznost
výroků	výrok	k1gInPc2	výrok
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
diferencovaná	diferencovaný	k2eAgFnSc1d1	diferencovaná
závaznost	závaznost	k1gFnSc1	závaznost
výroků	výrok	k1gInPc2	výrok
učitelského	učitelský	k2eAgInSc2d1	učitelský
úřadu	úřad	k1gInSc2	úřad
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Revidovatelná	Revidovatelný	k2eAgFnSc1d1	Revidovatelný
forma	forma	k1gFnSc1	forma
výroků	výrok	k1gInPc2	výrok
k	k	k7c3	k
aktuálním	aktuální	k2eAgInPc3d1	aktuální
problémům	problém	k1gInPc3	problém
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgNnP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
závaznost	závaznost	k1gFnSc4	závaznost
pro	pro	k7c4	pro
katolické	katolický	k2eAgMnPc4d1	katolický
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
.	.	kIx.	.
</s>
