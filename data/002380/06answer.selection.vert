<s>
Druhý	druhý	k4xOgInSc4	druhý
vatikánský	vatikánský	k2eAgInSc4d1	vatikánský
koncil	koncil	k1gInSc4	koncil
bylo	být	k5eAaImAgNnS	být
shromáždění	shromáždění	k1gNnSc1	shromáždění
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2500	[number]	k4	2500
katolických	katolický	k2eAgMnPc2d1	katolický
biskupů	biskup	k1gMnPc2	biskup
na	na	k7c6	na
dosud	dosud	k6eAd1	dosud
posledním	poslední	k2eAgInSc6d1	poslední
ekumenickém	ekumenický	k2eAgInSc6d1	ekumenický
koncilu	koncil	k1gInSc6	koncil
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
svolal	svolat	k5eAaPmAgMnS	svolat
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
a	a	k8xC	a
jež	jenž	k3xRgNnSc1	jenž
zasedalo	zasedat	k5eAaImAgNnS	zasedat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1962	[number]	k4	1962
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
