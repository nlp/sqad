<p>
<s>
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
je	být	k5eAaImIp3nS	být
plemeno	plemeno	k1gNnSc4	plemeno
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
zkřížením	zkřížení	k1gNnSc7	zkřížení
německého	německý	k2eAgMnSc2d1	německý
ovčáka	ovčák	k1gMnSc2	ovčák
a	a	k8xC	a
karpatského	karpatský	k2eAgMnSc2d1	karpatský
vlka	vlk	k1gMnSc2	vlk
(	(	kIx(	(
<g/>
Canis	Canis	k1gFnSc1	Canis
lupus	lupus	k1gInSc1	lupus
lupus	lupus	k1gInSc1	lupus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
pokus	pokus	k1gInSc1	pokus
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
stráže	stráž	k1gFnSc2	stráž
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
plukovníka	plukovník	k1gMnSc2	plukovník
Ing.	ing.	kA	ing.
Karla	Karel	k1gMnSc2	Karel
Hartla	Hartl	k1gMnSc2	Hartl
vyústil	vyústit	k5eAaPmAgMnS	vyústit
ve	v	k7c4	v
vznik	vznik	k1gInSc4	vznik
československého	československý	k2eAgNnSc2d1	Československé
národního	národní	k2eAgNnSc2d1	národní
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
rozdělení	rozdělení	k1gNnSc4	rozdělení
československé	československý	k2eAgFnSc2d1	Československá
federace	federace	k1gFnSc2	federace
<g/>
)	)	kIx)	)
převzalo	převzít	k5eAaPmAgNnS	převzít
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
patronát	patronát	k1gInSc4	patronát
plemene	plemeno	k1gNnSc2	plemeno
(	(	kIx(	(
<g/>
vůči	vůči	k7c3	vůči
Mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
kynologické	kynologický	k2eAgFnSc3d1	kynologická
federaci	federace	k1gFnSc3	federace
FCI	FCI	kA	FCI
<g/>
)	)	kIx)	)
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Počátek	počátek	k1gInSc1	počátek
historie	historie	k1gFnSc2	historie
tohoto	tento	k3xDgNnSc2	tento
plemene	plemeno	k1gNnSc2	plemeno
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Ing.	ing.	kA	ing.
Karel	Karel	k1gMnSc1	Karel
Hartl	Hartl	k1gMnSc1	Hartl
začal	začít	k5eAaPmAgMnS	začít
zabývat	zabývat	k5eAaImF	zabývat
křížením	křížení	k1gNnSc7	křížení
karpatského	karpatský	k2eAgMnSc2d1	karpatský
vlka	vlk	k1gMnSc2	vlk
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
ovčákem	ovčák	k1gMnSc7	ovčák
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
bylo	být	k5eAaImAgNnS	být
křížení	křížení	k1gNnSc1	křížení
prováděno	provádět	k5eAaImNgNnS	provádět
jako	jako	k8xS	jako
vědecký	vědecký	k2eAgInSc1d1	vědecký
experiment	experiment	k1gInSc1	experiment
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
zrodila	zrodit	k5eAaPmAgFnS	zrodit
myšlenka	myšlenka	k1gFnSc1	myšlenka
vyšlechtit	vyšlechtit	k5eAaPmF	vyšlechtit
nové	nový	k2eAgNnSc4d1	nové
plemeno	plemeno	k1gNnSc4	plemeno
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
chovu	chov	k1gInSc2	chov
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zachovat	zachovat	k5eAaPmF	zachovat
vlčí	vlčí	k2eAgInSc4d1	vlčí
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
zdraví	zdraví	k1gNnSc4	zdraví
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
upevnit	upevnit	k5eAaPmF	upevnit
povahové	povahový	k2eAgFnPc1d1	povahová
vlastnosti	vlastnost	k1gFnPc1	vlastnost
pracovního	pracovní	k2eAgMnSc2d1	pracovní
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgMnPc1	první
kříženci	kříženec	k1gMnPc1	kříženec
vlčice	vlčice	k1gFnSc2	vlčice
Brity	Brit	k1gMnPc4	Brit
a	a	k8xC	a
německého	německý	k2eAgMnSc4d1	německý
ovčáka	ovčák	k1gMnSc4	ovčák
Cézara	Cézar	k1gMnSc4	Cézar
z	z	k7c2	z
Březového	březový	k2eAgInSc2d1	březový
háje	háj	k1gInSc2	háj
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1958	[number]	k4	1958
v	v	k7c6	v
Libějovicích	Libějovice	k1gFnPc6	Libějovice
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
velkoryse	velkoryse	k6eAd1	velkoryse
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
<g/>
.	.	kIx.	.
</s>
<s>
Štěňata	štěně	k1gNnPc1	štěně
byla	být	k5eAaImAgNnP	být
pečlivě	pečlivě	k6eAd1	pečlivě
sledována	sledovat	k5eAaImNgFnS	sledovat
<g/>
,	,	kIx,	,
vychovávána	vychovávat	k5eAaImNgFnS	vychovávat
v	v	k7c6	v
různém	různý	k2eAgNnSc6d1	různé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zkoumány	zkoumat	k5eAaImNgInP	zkoumat
jejich	jejich	k3xOp3gInPc1	jejich
předpoklady	předpoklad	k1gInPc1	předpoklad
k	k	k7c3	k
výcviku	výcvik	k1gInSc3	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
nepříbuznými	příbuzný	k2eNgMnPc7d1	nepříbuzný
německými	německý	k2eAgMnPc7d1	německý
ovčáky	ovčák	k1gMnPc7	ovčák
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
s	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
generací	generace	k1gFnSc7	generace
klesal	klesat	k5eAaImAgInS	klesat
podíl	podíl	k1gInSc1	podíl
"	"	kIx"	"
<g/>
vlčí	vlčí	k2eAgFnSc2d1	vlčí
krve	krev	k1gFnSc2	krev
<g/>
"	"	kIx"	"
až	až	k9	až
na	na	k7c4	na
6,25	[number]	k4	6,25
%	%	kIx~	%
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
filiální	filiální	k2eAgFnSc6d1	filiální
generaci	generace	k1gFnSc6	generace
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
porovnávány	porovnáván	k2eAgInPc1d1	porovnáván
anatomické	anatomický	k2eAgInPc1d1	anatomický
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
fyziologické	fyziologický	k2eAgFnPc1d1	fyziologická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
i	i	k8xC	i
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
prvky	prvek	k1gInPc1	prvek
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
zvířat	zvíře	k1gNnPc2	zvíře
nejen	nejen	k6eAd1	nejen
mezi	mezi	k7c7	mezi
vlkem	vlk	k1gMnSc7	vlk
<g/>
,	,	kIx,	,
psem	pes	k1gMnSc7	pes
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
kříženci	kříženec	k1gMnPc1	kříženec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mezi	mezi	k7c7	mezi
jedinci	jedinec	k1gMnPc7	jedinec
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
úrovních	úroveň	k1gFnPc6	úroveň
křížení	křížení	k1gNnSc2	křížení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Štěňata	štěně	k1gNnPc1	štěně
první	první	k4xOgFnSc2	první
generace	generace	k1gFnSc2	generace
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
i	i	k8xC	i
chováním	chování	k1gNnSc7	chování
podobala	podobat	k5eAaImAgFnS	podobat
vlku	vlk	k1gMnSc3	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
výchova	výchova	k1gFnSc1	výchova
byla	být	k5eAaImAgFnS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
výcvik	výcvik	k1gInSc1	výcvik
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
možný	možný	k2eAgInSc1d1	možný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výsledky	výsledek	k1gInPc1	výsledek
stěží	stěží	k6eAd1	stěží
odpovídaly	odpovídat	k5eAaImAgInP	odpovídat
vynaloženému	vynaložený	k2eAgNnSc3d1	vynaložené
úsilí	úsilí	k1gNnSc3	úsilí
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
druhé	druhý	k4xOgFnPc4	druhý
generace	generace	k1gFnPc4	generace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byli	být	k5eAaImAgMnP	být
včas	včas	k6eAd1	včas
odebráni	odebrat	k5eAaPmNgMnP	odebrat
z	z	k7c2	z
vrhu	vrh	k1gInSc2	vrh
a	a	k8xC	a
individuálně	individuálně	k6eAd1	individuálně
vychováváni	vychováván	k2eAgMnPc1d1	vychováván
<g/>
,	,	kIx,	,
již	jenž	k3xRgMnPc1	jenž
byli	být	k5eAaImAgMnP	být
plně	plně	k6eAd1	plně
cvičitelní	cvičitelný	k2eAgMnPc1d1	cvičitelný
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
však	však	k9	však
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
hraví	hravý	k2eAgMnPc1d1	hravý
<g/>
,	,	kIx,	,
výcvik	výcvik	k1gInSc1	výcvik
trval	trvat	k5eAaImAgInS	trvat
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Vynikali	vynikat	k5eAaImAgMnP	vynikat
v	v	k7c6	v
pachových	pachový	k2eAgFnPc6d1	pachová
pracích	práce	k1gFnPc6	práce
(	(	kIx(	(
<g/>
sledování	sledování	k1gNnSc6	sledování
stopy	stopa	k1gFnSc2	stopa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jedinců	jedinec	k1gMnPc2	jedinec
třetí	třetí	k4xOgFnSc2	třetí
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgFnSc2	čtvrtý
filiální	filiální	k2eAgFnSc2d1	filiální
generace	generace	k1gFnSc2	generace
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
absolvovat	absolvovat	k5eAaPmF	absolvovat
běžný	běžný	k2eAgInSc4d1	běžný
výcvikový	výcvikový	k2eAgInSc4d1	výcvikový
kurs	kurs	k1gInSc4	kurs
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
zařazováni	zařazovat	k5eAaImNgMnP	zařazovat
do	do	k7c2	do
výkonu	výkon	k1gInSc2	výkon
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
psům	pes	k1gMnPc3	pes
měli	mít	k5eAaImAgMnP	mít
lepší	dobrý	k2eAgFnPc4d2	lepší
orientační	orientační	k2eAgFnPc4d1	orientační
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
dokonalejší	dokonalý	k2eAgNnSc4d2	dokonalejší
vidění	vidění	k1gNnSc4	vidění
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgInSc4d2	lepší
sluch	sluch	k1gInSc4	sluch
i	i	k8xC	i
čich	čich	k1gInSc4	čich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
testech	test	k1gInPc6	test
vytrvalosti	vytrvalost	k1gFnSc2	vytrvalost
uběhli	uběhnout	k5eAaPmAgMnP	uběhnout
kříženci	kříženec	k1gMnPc1	kříženec
celou	celá	k1gFnSc4	celá
100	[number]	k4	100
km	km	kA	km
trať	trať	k1gFnSc4	trať
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
vyčerpáni	vyčerpat	k5eAaPmNgMnP	vyčerpat
<g/>
,	,	kIx,	,
opotřebování	opotřebování	k1gNnSc6	opotřebování
polštářků	polštářek	k1gInPc2	polštářek
tlap	tlapa	k1gFnPc2	tlapa
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něm.	něm.	k?	něm.
ovčáků	ovčák	k1gMnPc2	ovčák
rovnoměrné	rovnoměrný	k2eAgNnSc1d1	rovnoměrné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výkonu	výkon	k1gInSc6	výkon
odpočívali	odpočívat	k5eAaImAgMnP	odpočívat
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
akademií	akademie	k1gFnSc7	akademie
věd	věda	k1gFnPc2	věda
byla	být	k5eAaImAgFnS	být
sledována	sledovat	k5eAaImNgFnS	sledovat
endogenní	endogenní	k2eAgFnSc1d1	endogenní
aktivita	aktivita	k1gFnSc1	aktivita
vlků	vlk	k1gMnPc2	vlk
<g/>
,	,	kIx,	,
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
kříženců	kříženec	k1gMnPc2	kříženec
<g/>
.	.	kIx.	.
</s>
<s>
Křivka	křivka	k1gFnSc1	křivka
aktivity	aktivita	k1gFnSc2	aktivita
během	během	k7c2	během
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
byla	být	k5eAaImAgFnS	být
podobná	podobný	k2eAgFnSc1d1	podobná
–	–	k?	–
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
maximy	maxim	k1gInPc7	maxim
(	(	kIx(	(
<g/>
ráno	ráno	k6eAd1	ráno
a	a	k8xC	a
večer	večer	k6eAd1	večer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
intenzita	intenzita	k1gFnSc1	intenzita
aktivity	aktivita	k1gFnSc2	aktivita
vlků	vlk	k1gMnPc2	vlk
a	a	k8xC	a
kříženců	kříženec	k1gMnPc2	kříženec
vysoko	vysoko	k6eAd1	vysoko
převyšovala	převyšovat	k5eAaImAgFnS	převyšovat
hodnoty	hodnota	k1gFnPc4	hodnota
zaznamenané	zaznamenaný	k2eAgFnPc4d1	zaznamenaná
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
experimentu	experiment	k1gInSc2	experiment
byly	být	k5eAaImAgInP	být
vyhodnoceny	vyhodnotit	k5eAaPmNgInP	vyhodnotit
a	a	k8xC	a
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1965	[number]	k4	1965
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
Světová	světový	k2eAgFnSc1d1	světová
výstava	výstava	k1gFnSc1	výstava
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
výroční	výroční	k2eAgNnSc1d1	výroční
zasedání	zasedání	k1gNnSc1	zasedání
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
kynologické	kynologický	k2eAgFnSc2d1	kynologická
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
FCI	FCI	kA	FCI
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
kynologický	kynologický	k2eAgInSc1d1	kynologický
kongres	kongres	k1gInSc1	kongres
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
přednáška	přednáška	k1gFnSc1	přednáška
Ing.	ing.	kA	ing.
Karla	Karel	k1gMnSc2	Karel
Hartla	Hartl	k1gMnSc2	Hartl
"	"	kIx"	"
<g/>
Výsledky	výsledek	k1gInPc1	výsledek
křížení	křížení	k1gNnSc3	křížení
vlků	vlk	k1gMnPc2	vlk
se	s	k7c7	s
psy	pes	k1gMnPc7	pes
<g/>
"	"	kIx"	"
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
Ing.	ing.	kA	ing.
Hartlem	Hartl	k1gMnSc7	Hartl
sestaven	sestavna	k1gFnPc2	sestavna
návrh	návrh	k1gInSc1	návrh
standardu	standard	k1gInSc2	standard
nového	nový	k2eAgNnSc2d1	nové
plemene	plemeno	k1gNnSc2	plemeno
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
českého	český	k2eAgMnSc4d1	český
vlčáka	vlčák	k1gMnSc4	vlčák
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
potomci	potomek	k1gMnPc1	potomek
křížení	křížení	k1gNnSc1	křížení
vlků	vlk	k1gMnPc2	vlk
se	s	k7c7	s
psy	pes	k1gMnPc7	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlčice	vlčice	k1gFnSc1	vlčice
Brita	Brit	k1gMnSc2	Brit
pak	pak	k6eAd1	pak
dala	dát	k5eAaPmAgFnS	dát
ještě	ještě	k9	ještě
základ	základ	k1gInSc1	základ
druhé	druhý	k4xOgFnSc2	druhý
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
německým	německý	k2eAgMnSc7d1	německý
ovčákem	ovčák	k1gMnSc7	ovčák
Kurtem	Kurt	k1gMnSc7	Kurt
z	z	k7c2	z
Václavky	václavka	k1gFnSc2	václavka
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
linie	linie	k1gFnSc1	linie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
vlka	vlk	k1gMnSc2	vlk
Arga	Argos	k1gMnSc2	Argos
a	a	k8xC	a
feny	fena	k1gFnSc2	fena
německého	německý	k2eAgMnSc2d1	německý
ovčáka	ovčák	k1gMnSc2	ovčák
Asty	Asta	k1gMnSc2	Asta
z	z	k7c2	z
SNB	SNB	kA	SNB
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
byla	být	k5eAaImAgFnS	být
kříženka	kříženka	k1gFnSc1	kříženka
3	[number]	k4	3
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
Xela	Xel	k2eAgFnSc1d1	Xel
z	z	k7c2	z
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
stráže	stráž	k1gFnSc2	stráž
kryta	kryt	k2eAgFnSc1d1	kryta
vlkem	vlk	k1gMnSc7	vlk
Šarikem	Šarik	k1gMnSc7	Šarik
<g/>
,	,	kIx,	,
Šarik	Šarik	k1gMnSc1	Šarik
pak	pak	k6eAd1	pak
nakryl	nakryl	k1gInSc4	nakryl
ještě	ještě	k9	ještě
fenu	fena	k1gFnSc4	fena
Urtu	Urtus	k1gInSc2	Urtus
z	z	k7c2	z
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
stráže	stráž	k1gFnSc2	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
přilití	přilití	k1gNnSc1	přilití
krve	krev	k1gFnSc2	krev
vlka	vlk	k1gMnSc2	vlk
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Vlčice	vlčice	k1gFnSc1	vlčice
Lejdy	Lejda	k1gFnSc2	Lejda
ze	z	k7c2	z
ZOO	zoo	k1gFnSc2	zoo
Hluboká	Hluboká	k1gFnSc1	Hluboká
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
poslední	poslední	k2eAgFnSc4d1	poslední
linii	linie	k1gFnSc4	linie
nově	nova	k1gFnSc3	nova
šlechtěného	šlechtěný	k2eAgNnSc2d1	šlechtěné
plemene	plemeno	k1gNnSc2	plemeno
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
štěňat	štěně	k1gNnPc2	štěně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
německý	německý	k2eAgMnSc1d1	německý
ovčák	ovčák	k1gMnSc1	ovčák
Bojar	bojar	k1gMnSc1	bojar
von	von	k1gInSc4	von
Shottenhof	Shottenhof	k1gInSc1	Shottenhof
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
již	již	k6eAd1	již
prováděla	provádět	k5eAaImAgFnS	provádět
plemenitba	plemenitba	k1gFnSc1	plemenitba
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
uzavřené	uzavřený	k2eAgFnSc6d1	uzavřená
populaci	populace	k1gFnSc6	populace
a	a	k8xC	a
kříženci	kříženec	k1gMnPc1	kříženec
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
označováni	označovat	k5eAaImNgMnP	označovat
jako	jako	k9	jako
český	český	k2eAgMnSc1d1	český
vlčák	vlčák	k1gMnSc1	vlčák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
uznán	uznat	k5eAaPmNgInS	uznat
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
Federálním	federální	k2eAgInSc7d1	federální
výborem	výbor	k1gInSc7	výbor
chovatelských	chovatelský	k2eAgInPc2d1	chovatelský
svazů	svaz	k1gInPc2	svaz
ČSSR	ČSSR	kA	ČSSR
jako	jako	k8xS	jako
národní	národní	k2eAgNnSc1d1	národní
plemeno	plemeno	k1gNnSc1	plemeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podklady	podklad	k1gInPc1	podklad
k	k	k7c3	k
žádosti	žádost	k1gFnSc3	žádost
o	o	k7c6	o
uznání	uznání	k1gNnSc6	uznání
plemene	plemeno	k1gNnSc2	plemeno
byly	být	k5eAaImAgInP	být
připravovány	připravován	k2eAgInPc1d1	připravován
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
odbornou	odborný	k2eAgFnSc7d1	odborná
komisí	komise	k1gFnSc7	komise
chovatelů	chovatel	k1gMnPc2	chovatel
psů	pes	k1gMnPc2	pes
ČSCH	ČSCH	kA	ČSCH
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
nové	nový	k2eAgNnSc4d1	nové
plemeno	plemeno	k1gNnSc4	plemeno
vytrvale	vytrvale	k6eAd1	vytrvale
podporoval	podporovat	k5eAaImAgMnS	podporovat
zejména	zejména	k9	zejména
pan	pan	k1gMnSc1	pan
Ing.	ing.	kA	ing.
Jan	Jan	k1gMnSc1	Jan
Findejs	Findejs	k1gInSc1	Findejs
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
komise	komise	k1gFnSc1	komise
FCI	FCI	kA	FCI
doporučila	doporučit	k5eAaPmAgFnS	doporučit
standard	standard	k1gInSc4	standard
československého	československý	k2eAgMnSc2d1	československý
vlčáka	vlčák	k1gMnSc2	vlčák
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zapracování	zapracování	k1gNnSc6	zapracování
připomínek	připomínka	k1gFnPc2	připomínka
a	a	k8xC	a
nutných	nutný	k2eAgFnPc2d1	nutná
změn	změna	k1gFnPc2	změna
do	do	k7c2	do
standardu	standard	k1gInSc2	standard
bylo	být	k5eAaImAgNnS	být
plemeno	plemeno	k1gNnSc1	plemeno
uznáno	uznat	k5eAaPmNgNnS	uznat
hlasováním	hlasování	k1gNnSc7	hlasování
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Generálním	generální	k2eAgNnSc6d1	generální
shromáždění	shromáždění	k1gNnSc6	shromáždění
FCI	FCI	kA	FCI
(	(	kIx(	(
<g/>
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
uznání	uznání	k1gNnSc4	uznání
na	na	k7c4	na
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
prosadil	prosadit	k5eAaPmAgMnS	prosadit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
československý	československý	k2eAgMnSc1d1	československý
zástupce	zástupce	k1gMnSc1	zástupce
v	v	k7c6	v
FCI	FCI	kA	FCI
<g/>
,	,	kIx,	,
RNDr.	RNDr.	kA	RNDr.
Petr	Petr	k1gMnSc1	Petr
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Standard	standard	k1gInSc1	standard
československého	československý	k2eAgMnSc2d1	československý
vlčáka	vlčák	k1gMnSc2	vlčák
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
a	a	k8xC	a
vydán	vydat	k5eAaPmNgInS	vydat
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
332	[number]	k4	332
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
tituly	titul	k1gInPc1	titul
"	"	kIx"	"
<g/>
světový	světový	k2eAgMnSc1d1	světový
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
"	"	kIx"	"
tak	tak	k6eAd1	tak
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
československým	československý	k2eAgMnPc3d1	československý
vlčákům	vlčák	k1gMnPc3	vlčák
zadány	zadat	k5eAaPmNgInP	zadat
na	na	k7c6	na
Světové	světový	k2eAgFnSc3d1	světová
výstavě	výstava	k1gFnSc3	výstava
psů	pes	k1gMnPc2	pes
všech	všecek	k3xTgNnPc2	všecek
plemen	plemeno	k1gNnPc2	plemeno
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Výstavu	výstava	k1gFnSc4	výstava
posuzoval	posuzovat	k5eAaImAgMnS	posuzovat
mezinárodní	mezinárodní	k2eAgMnSc1d1	mezinárodní
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
plemene	plemeno	k1gNnSc2	plemeno
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Ing.	ing.	kA	ing.
Karel	Karel	k1gMnSc1	Karel
Hartl	Hartl	k1gMnSc1	Hartl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
uznání	uznání	k1gNnSc2	uznání
standardu	standard	k1gInSc2	standard
bylo	být	k5eAaImAgNnS	být
plemeno	plemeno	k1gNnSc1	plemeno
znovu	znovu	k6eAd1	znovu
předmětem	předmět	k1gInSc7	předmět
jednání	jednání	k1gNnSc2	jednání
FCI	FCI	kA	FCI
–	–	k?	–
muselo	muset	k5eAaImAgNnS	muset
se	se	k3xPyFc4	se
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plemeno	plemeno	k1gNnSc1	plemeno
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
životaschopné	životaschopný	k2eAgNnSc1d1	životaschopné
a	a	k8xC	a
trvale	trvale	k6eAd1	trvale
splňuje	splňovat	k5eAaImIp3nS	splňovat
všechna	všechen	k3xTgNnPc4	všechen
stanovená	stanovený	k2eAgNnPc4d1	stanovené
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
obhajobu	obhajoba	k1gFnSc4	obhajoba
našeho	náš	k3xOp1gNnSc2	náš
plemene	plemeno	k1gNnSc2	plemeno
vzala	vzít	k5eAaPmAgFnS	vzít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
Československa	Československo	k1gNnSc2	Československo
totiž	totiž	k9	totiž
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
Českomoravskou	českomoravský	k2eAgFnSc7d1	Českomoravská
kynologickou	kynologický	k2eAgFnSc7d1	kynologická
unií	unie	k1gFnSc7	unie
a	a	k8xC	a
Slovenskou	slovenský	k2eAgFnSc7d1	slovenská
kynologickou	kynologický	k2eAgFnSc7d1	kynologická
jednotou	jednota	k1gFnSc7	jednota
ze	z	k7c2	z
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
přebírá	přebírat	k5eAaImIp3nS	přebírat
patronát	patronát	k1gInSc1	patronát
nad	nad	k7c7	nad
plemenem	plemeno	k1gNnSc7	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
země	země	k1gFnSc1	země
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
uváděno	uvádět	k5eAaImNgNnS	uvádět
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
bývalé	bývalý	k2eAgNnSc1d1	bývalé
Československo	Československo	k1gNnSc1	Československo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
podkladů	podklad	k1gInPc2	podklad
pro	pro	k7c4	pro
FCI	FCI	kA	FCI
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
obě	dva	k4xCgFnPc4	dva
nástupnické	nástupnický	k2eAgFnPc4d1	nástupnická
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
podepsané	podepsaný	k2eAgNnSc1d1	podepsané
20	[number]	k4	20
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
kynologické	kynologický	k2eAgFnSc2d1	kynologická
federace	federace	k1gFnSc2	federace
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
uznání	uznání	k1gNnSc1	uznání
plemene	plemeno	k1gNnSc2	plemeno
československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
definitivně	definitivně	k6eAd1	definitivně
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
může	moct	k5eAaImIp3nS	moct
FCI	FCI	kA	FCI
potvrzovat	potvrzovat	k5eAaImF	potvrzovat
i	i	k9	i
udělené	udělený	k2eAgInPc4d1	udělený
návrhy	návrh	k1gInPc4	návrh
na	na	k7c6	na
CACIB	CACIB	kA	CACIB
a	a	k8xC	a
CACIT	CACIT	kA	CACIT
a	a	k8xC	a
československým	československý	k2eAgMnPc3d1	československý
vlčákům	vlčák	k1gMnPc3	vlčák
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
otevřena	otevřen	k2eAgFnSc1d1	otevřena
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
titulům	titul	k1gInPc3	titul
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
šampiónů	šampión	k1gMnPc2	šampión
krásy	krása	k1gFnSc2	krása
a	a	k8xC	a
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
předložil	předložit	k5eAaPmAgMnS	předložit
pan	pan	k1gMnSc1	pan
Štefan	Štefan	k1gMnSc1	Štefan
Štefík	Štefík	k1gMnSc1	Štefík
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
kynologické	kynologický	k2eAgFnSc2d1	kynologická
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
publikována	publikovat	k5eAaBmNgFnS	publikovat
dosud	dosud	k6eAd1	dosud
poslední	poslední	k2eAgFnSc1d1	poslední
úprava	úprava	k1gFnSc1	úprava
standardu	standard	k1gInSc2	standard
plemene	plemeno	k1gNnSc2	plemeno
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
neschválila	schválit	k5eNaPmAgFnS	schválit
česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
standard	standard	k1gInSc1	standard
bez	bez	k7c2	bez
jejího	její	k3xOp3gInSc2	její
souhlasu	souhlas	k1gInSc2	souhlas
svévolně	svévolně	k6eAd1	svévolně
změněn	změnit	k5eAaPmNgInS	změnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
šlo	jít	k5eAaImAgNnS	jít
nalézt	nalézt	k5eAaBmF	nalézt
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
zapsaných	zapsaný	k2eAgNnPc2d1	zapsané
štěňat	štěně	k1gNnPc2	štěně
ročně	ročně	k6eAd1	ročně
československých	československý	k2eAgMnPc2d1	československý
vlčáků	vlčák	k1gMnPc2	vlčák
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
až	až	k9	až
200	[number]	k4	200
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chov	chov	k1gInSc1	chov
tohoto	tento	k3xDgNnSc2	tento
plemene	plemeno	k1gNnSc2	plemeno
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
stagnuje	stagnovat	k5eAaImIp3nS	stagnovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kolem	kolem	k7c2	kolem
50	[number]	k4	50
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
větší	veliký	k2eAgMnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
a	a	k8xC	a
pohybem	pohyb	k1gInSc7	pohyb
připomíná	připomínat	k5eAaImIp3nS	připomínat
vlka	vlk	k1gMnSc4	vlk
<g/>
.	.	kIx.	.
</s>
<s>
Feny	fena	k1gFnPc1	fena
měří	měřit	k5eAaImIp3nP	měřit
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
minimálně	minimálně	k6eAd1	minimálně
60	[number]	k4	60
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
minimálně	minimálně	k6eAd1	minimálně
65	[number]	k4	65
cm	cm	kA	cm
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
však	však	k9	však
feny	fena	k1gFnPc1	fena
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
65	[number]	k4	65
i	i	k9	i
více	hodně	k6eAd2	hodně
cm	cm	kA	cm
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
až	až	k9	až
75	[number]	k4	75
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
plemenným	plemenný	k2eAgInSc7d1	plemenný
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
vlčí	vlčí	k2eAgFnSc1d1	vlčí
maska	maska	k1gFnSc1	maska
okolo	okolo	k7c2	okolo
tlamy	tlama	k1gFnSc2	tlama
<g/>
,	,	kIx,	,
na	na	k7c6	na
bradě	brada	k1gFnSc6	brada
a	a	k8xC	a
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
úzké	úzký	k2eAgFnSc3d1	úzká
<g/>
,	,	kIx,	,
šikmo	šikmo	k6eAd1	šikmo
uložené	uložený	k2eAgNnSc1d1	uložené
světle	světle	k6eAd1	světle
jantarové	jantarový	k2eAgNnSc1d1	jantarové
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
vlkošedá	vlkošedý	k2eAgFnSc1d1	vlkošedá
barva	barva	k1gFnSc1	barva
srsti	srst	k1gFnSc2	srst
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
odstínech	odstín	k1gInPc6	odstín
od	od	k7c2	od
stříbrošedé	stříbrošedý	k2eAgFnSc2d1	stříbrošedá
až	až	k9	až
po	po	k7c4	po
tmavošedou	tmavošedý	k2eAgFnSc4d1	tmavošedá
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
hustá	hustý	k2eAgFnSc1d1	hustá
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgNnSc1d1	krátké
ucho	ucho	k1gNnSc1	ucho
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
dosahující	dosahující	k2eAgInSc1d1	dosahující
k	k	k7c3	k
hleznu	hlezno	k1gNnSc3	hlezno
a	a	k8xC	a
úzký	úzký	k2eAgInSc4d1	úzký
postoj	postoj	k1gInSc4	postoj
předních	přední	k2eAgFnPc2d1	přední
končetin	končetina	k1gFnPc2	končetina
s	s	k7c7	s
vybočenými	vybočený	k2eAgFnPc7d1	vybočená
tlapami	tlapa	k1gFnPc7	tlapa
<g/>
.	.	kIx.	.
</s>
<s>
Linie	linie	k1gFnSc1	linie
hřbetu	hřbet	k1gInSc2	hřbet
a	a	k8xC	a
zádě	záď	k1gFnSc2	záď
je	být	k5eAaImIp3nS	být
rovná	rovný	k2eAgFnSc1d1	rovná
<g/>
,	,	kIx,	,
končetiny	končetina	k1gFnPc1	končetina
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
s	s	k7c7	s
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
běhovými	běhový	k2eAgFnPc7d1	běhová
kostmi	kost	k1gFnPc7	kost
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
pohybem	pohyb	k1gInSc7	pohyb
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
prostorný	prostorný	k2eAgInSc1d1	prostorný
klus	klus	k1gInSc1	klus
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
je	být	k5eAaImIp3nS	být
vytrvalý	vytrvalý	k2eAgMnSc1d1	vytrvalý
klusák	klusák	k1gMnSc1	klusák
<g/>
,	,	kIx,	,
trénovaní	trénovaný	k2eAgMnPc1d1	trénovaný
jedinci	jedinec	k1gMnPc1	jedinec
uběhnou	uběhnout	k5eAaPmIp3nP	uběhnout
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
100	[number]	k4	100
km	km	kA	km
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
rychlostí	rychlost	k1gFnSc7	rychlost
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgMnSc1d1	aktivní
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
k	k	k7c3	k
cizím	cizí	k2eAgFnPc3d1	cizí
nedůvěřivý	důvěřivý	k2eNgInSc1d1	nedůvěřivý
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
ČSV	ČSV	kA	ČSV
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
k	k	k7c3	k
naprosto	naprosto	k6eAd1	naprosto
cizím	cizí	k2eAgMnPc3d1	cizí
lidem	člověk	k1gMnPc3	člověk
přátelsky	přátelsky	k6eAd1	přátelsky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hodící	hodící	k2eAgFnSc4d1	hodící
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výstavy	výstava	k1gFnPc4	výstava
i	i	k8xC	i
služební	služební	k2eAgInSc4d1	služební
výcvik	výcvik	k1gInSc4	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Výcvik	výcvik	k1gInSc1	výcvik
je	být	k5eAaImIp3nS	být
však	však	k9	však
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
německého	německý	k2eAgMnSc2d1	německý
ovčáka	ovčák	k1gMnSc2	ovčák
a	a	k8xC	a
ortodoxní	ortodoxní	k2eAgMnPc1d1	ortodoxní
výcvikáři	výcvikář	k1gMnPc1	výcvikář
si	se	k3xPyFc3	se
toto	tento	k3xDgNnSc4	tento
plemeno	plemeno	k1gNnSc4	plemeno
pořizují	pořizovat	k5eAaImIp3nP	pořizovat
málokdy	málokdy	k6eAd1	málokdy
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
"	"	kIx"	"
<g/>
čé-es-véčka	čéséčka	k1gFnSc1	čé-es-véčka
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
československému	československý	k2eAgMnSc3d1	československý
vlčáku	vlčák	k1gMnSc3	vlčák
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
takzvané	takzvaný	k2eAgNnSc1d1	takzvané
"	"	kIx"	"
<g/>
účelové	účelový	k2eAgNnSc1d1	účelové
chování	chování	k1gNnSc1	chování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bezduché	bezduchý	k2eAgNnSc1d1	bezduché
opakování	opakování	k1gNnSc1	opakování
cviku	cvik	k1gInSc2	cvik
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
budete	být	k5eAaImBp2nP	být
požadovat	požadovat	k5eAaImF	požadovat
marně	marně	k6eAd1	marně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyniká	vynikat	k5eAaImIp3nS	vynikat
však	však	k9	však
velikou	veliký	k2eAgFnSc7d1	veliká
vytrvalostí	vytrvalost	k1gFnSc7	vytrvalost
<g/>
,	,	kIx,	,
odolností	odolnost	k1gFnSc7	odolnost
vůči	vůči	k7c3	vůči
nepříznivému	příznivý	k2eNgNnSc3d1	nepříznivé
počasí	počasí	k1gNnSc3	počasí
a	a	k8xC	a
schopností	schopnost	k1gFnSc7	schopnost
samostatně	samostatně	k6eAd1	samostatně
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Věrnost	věrnost	k1gFnSc1	věrnost
pánovi	pán	k1gMnSc6	pán
a	a	k8xC	a
smečce	smečka	k1gFnSc6	smečka
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
příslovečná	příslovečný	k2eAgFnSc1d1	příslovečná
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
také	také	k9	také
výběr	výběr	k1gInSc1	výběr
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nebudete	být	k5eNaImBp2nP	být
moci	moct	k5eAaImF	moct
psovi	psův	k2eAgMnPc1d1	psův
plně	plně	k6eAd1	plně
věnovat	věnovat	k5eAaImF	věnovat
a	a	k8xC	a
nepředpokládáte	předpokládat	k5eNaImIp2nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
budete	být	k5eAaImBp2nP	být
moci	moct	k5eAaImF	moct
pečovat	pečovat	k5eAaImF	pečovat
po	po	k7c4	po
jeho	on	k3xPp3gInSc4	on
celý	celý	k2eAgInSc4d1	celý
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
nepořizujte	pořizovat	k5eNaImRp2nP	pořizovat
si	se	k3xPyFc3	se
toto	tento	k3xDgNnSc4	tento
plemeno	plemeno	k1gNnSc4	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
si	se	k3xPyFc3	se
špatně	špatně	k6eAd1	špatně
zvykají	zvykat	k5eAaImIp3nP	zvykat
na	na	k7c4	na
nového	nový	k2eAgMnSc4d1	nový
majitele	majitel	k1gMnSc4	majitel
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
velmi	velmi	k6eAd1	velmi
trpí	trpět	k5eAaImIp3nS	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
je	být	k5eAaImIp3nS	být
i	i	k9	i
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
psem	pes	k1gMnSc7	pes
výstavním	výstavní	k2eAgNnSc6d1	výstavní
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československým	československý	k2eAgMnPc3d1	československý
vlčákům	vlčák	k1gMnPc3	vlčák
zbyl	zbýt	k5eAaPmAgInS	zbýt
po	po	k7c6	po
vlcích	vlk	k1gMnPc6	vlk
silný	silný	k2eAgInSc1d1	silný
smečkový	smečkový	k2eAgInSc1d1	smečkový
pud	pud	k1gInSc1	pud
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
žijí	žít	k5eAaImIp3nP	žít
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
smečku	smečka	k1gFnSc4	smečka
a	a	k8xC	a
smečkové	smečkový	k2eAgNnSc4d1	smečkový
chování	chování	k1gNnSc4	chování
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
převládá	převládat	k5eAaImIp3nS	převládat
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
s	s	k7c7	s
fenou	fena	k1gFnSc7	fena
zpravidla	zpravidla	k6eAd1	zpravidla
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
pevné	pevný	k2eAgNnSc4d1	pevné
partnerství	partnerství	k1gNnSc4	partnerství
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Samci	samec	k1gInSc3	samec
tohoto	tento	k3xDgNnSc2	tento
plemene	plemeno	k1gNnSc2	plemeno
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
k	k	k7c3	k
fenám	fena	k1gFnPc3	fena
velmi	velmi	k6eAd1	velmi
"	"	kIx"	"
<g/>
galantně	galantně	k6eAd1	galantně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
pes	pes	k1gMnSc1	pes
žijící	žijící	k2eAgMnSc1d1	žijící
trvale	trvale	k6eAd1	trvale
v	v	k7c6	v
páru	pár	k1gInSc6	pár
s	s	k7c7	s
fenou	fena	k1gFnSc7	fena
odmítá	odmítat	k5eAaImIp3nS	odmítat
jiné	jiný	k2eAgFnPc4d1	jiná
feny	fena	k1gFnPc4	fena
krýt	krýt	k5eAaImF	krýt
<g/>
.	.	kIx.	.
</s>
<s>
Opuštěný	opuštěný	k2eAgMnSc1d1	opuštěný
československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
umí	umět	k5eAaImIp3nS	umět
dát	dát	k5eAaPmF	dát
velmi	velmi	k6eAd1	velmi
hlasitě	hlasitě	k6eAd1	hlasitě
najevo	najevo	k6eAd1	najevo
svou	svůj	k3xOyFgFnSc4	svůj
samotu	samota	k1gFnSc4	samota
a	a	k8xC	a
volat	volat	k5eAaImF	volat
svého	svůj	k3xOyFgMnSc4	svůj
pána	pán	k1gMnSc4	pán
či	či	k8xC	či
svého	svůj	k3xOyFgMnSc2	svůj
nepřítomného	přítomný	k2eNgMnSc2d1	nepřítomný
psího	psí	k2eAgMnSc2d1	psí
partnera	partner	k1gMnSc2	partner
daleko	daleko	k6eAd1	daleko
se	se	k3xPyFc4	se
nesoucím	nesoucí	k2eAgNnSc7d1	nesoucí
vytím	vytí	k1gNnSc7	vytí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HARTL	Hartl	k1gMnSc1	Hartl
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JEDLIČKA	Jedlička	k1gMnSc1	Jedlička
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Loba	Loba	k1gFnSc1	Loba
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Klubem	klub	k1gInSc7	klub
chovatelů	chovatel	k1gMnPc2	chovatel
československého	československý	k2eAgMnSc2d1	československý
vlčáka	vlčák	k1gMnSc2	vlčák
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
75	[number]	k4	75
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
1107	[number]	k4	1107
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc2	galerie
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
-	-	kIx~	-
Psí	psí	k2eAgFnSc2d1	psí
rasy	rasa	k1gFnSc2	rasa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Československý	československý	k2eAgMnSc1d1	československý
vlčák	vlčák	k1gMnSc1	vlčák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
chovatelů	chovatel	k1gMnPc2	chovatel
československého	československý	k2eAgMnSc2d1	československý
vlčáka	vlčák	k1gMnSc2	vlčák
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
chovatelů	chovatel	k1gMnPc2	chovatel
československého	československý	k2eAgMnSc2d1	československý
vlčáka	vlčák	k1gMnSc2	vlčák
v	v	k7c6	v
SR	SR	kA	SR
</s>
</p>
<p>
<s>
Psi	pes	k1gMnPc1	pes
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
hranici	hranice	k1gFnSc6	hranice
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
český	český	k2eAgInSc1d1	český
web	web	k1gInSc1	web
o	o	k7c6	o
plemeni	plemeno	k1gNnSc6	plemeno
čsv	čsv	k?	čsv
</s>
</p>
<p>
<s>
největší	veliký	k2eAgInSc1d3	veliký
slovenský	slovenský	k2eAgInSc1d1	slovenský
web	web	k1gInSc1	web
o	o	k7c6	o
plemeni	plemeno	k1gNnSc6	plemeno
čsv	čsv	k?	čsv
</s>
</p>
