<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kynologická	kynologický	k2eAgFnSc1d1	kynologická
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Fédération	Fédération	k1gInSc1	Fédération
Cynologique	Cynologiqu	k1gFnSc2	Cynologiqu
Internationale	Internationale	k1gFnSc2	Internationale
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
zkratka	zkratka	k1gFnSc1	zkratka
FCI	FCI	kA	FCI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
plemeny	plemeno	k1gNnPc7	plemeno
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
kynologií	kynologie	k1gFnPc2	kynologie
jako	jako	k8xC	jako
takovou	takový	k3xDgFnSc4	takový
<g/>
.	.	kIx.	.
</s>
