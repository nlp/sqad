<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kynologická	kynologický	k2eAgFnSc1d1	kynologická
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Fédération	Fédération	k1gInSc1	Fédération
Cynologique	Cynologiqu	k1gFnSc2	Cynologiqu
Internationale	Internationale	k1gFnSc2	Internationale
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
zkratka	zkratka	k1gFnSc1	zkratka
FCI	FCI	kA	FCI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
plemeny	plemeno	k1gNnPc7	plemeno
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
kynologií	kynologie	k1gFnPc2	kynologie
jako	jako	k8xS	jako
takovou	takový	k3xDgFnSc4	takový
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
v	v	k7c6	v
městě	město	k1gNnSc6	město
Thuin	Thuina	k1gFnPc2	Thuina
<g/>
.	.	kIx.	.
</s>
<s>
FCI	FCI	kA	FCI
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Rakousko-Uherskem	Rakousko-Uhersko	k1gNnSc7	Rakousko-Uhersko
<g/>
,	,	kIx,	,
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
Société	Sociétý	k2eAgFnSc2d1	Sociétý
Centrale	Central	k1gMnSc5	Central
Canine	Canin	k1gInSc5	Canin
de	de	k?	de
France	Franc	k1gMnSc2	Franc
a	a	k8xC	a
Société	Sociétý	k2eAgFnSc3d1	Sociétý
Royale	Royala	k1gFnSc3	Royala
Saint-Hubert	Saint-Hubert	k1gInSc4	Saint-Hubert
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
znovu	znovu	k6eAd1	znovu
obnovily	obnovit	k5eAaPmAgFnP	obnovit
FCI	FCI	kA	FCI
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
když	když	k8xS	když
během	během	k7c2	během
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
FCI	FCI	kA	FCI
80	[number]	k4	80
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
468	[number]	k4	468
plemen	plemeno	k1gNnPc2	plemeno
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
FCI	FCI	kA	FCI
evidována	evidovat	k5eAaImNgFnS	evidovat
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
každým	každý	k3xTgNnSc7	každý
jednotlivým	jednotlivý	k2eAgNnSc7d1	jednotlivé
plemenem	plemeno	k1gNnSc7	plemeno
má	mít	k5eAaImIp3nS	mít
patronát	patronát	k1gInSc1	patronát
určitý	určitý	k2eAgInSc1d1	určitý
členský	členský	k2eAgInSc1d1	členský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sepisuje	sepisovat	k5eAaImIp3nS	sepisovat
standard	standard	k1gInSc4	standard
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
FCI	FCI	kA	FCI
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
překlad	překlad	k1gInSc4	překlad
a	a	k8xC	a
doplňování	doplňování	k1gNnSc1	doplňování
<g/>
.	.	kIx.	.
</s>
<s>
Standardy	standard	k1gInPc1	standard
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
regule	regule	k1gFnPc1	regule
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posudky	posudek	k1gInPc4	posudek
plemen	plemeno	k1gNnPc2	plemeno
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
standardech	standard	k1gInPc6	standard
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
plemena	plemeno	k1gNnPc1	plemeno
psů	pes	k1gMnPc2	pes
jsou	být	k5eAaImIp3nP	být
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
deseti	deset	k4xCc2	deset
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
znacích	znak	k1gInPc6	znak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
vzhled	vzhled	k1gInSc4	vzhled
nebo	nebo	k8xC	nebo
využití	využití	k1gNnSc4	využití
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Plemena	plemeno	k1gNnPc1	plemeno
ovčácká	ovčácký	k2eAgNnPc1d1	ovčácké
<g/>
,	,	kIx,	,
pastevecká	pastevecký	k2eAgNnPc1d1	pastevecké
a	a	k8xC	a
honácká	honácký	k2eAgNnPc1d1	honácké
Pinčové	pinč	k1gMnPc5	pinč
<g/>
,	,	kIx,	,
knírači	knírač	k1gMnPc5	knírač
<g/>
,	,	kIx,	,
plemena	plemeno	k1gNnPc4	plemeno
molossoidní	molossoidní	k2eAgNnPc4d1	molossoidní
a	a	k8xC	a
švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
salašničtí	salašnický	k2eAgMnPc1d1	salašnický
psi	pes	k1gMnPc1	pes
Teriéři	teriér	k1gMnPc1	teriér
Jezevčíci	jezevčík	k1gMnPc1	jezevčík
Špicové	špic	k1gMnPc1	špic
a	a	k8xC	a
psi	pes	k1gMnPc1	pes
původního	původní	k2eAgInSc2d1	původní
typu	typ	k1gInSc2	typ
Honiči	honič	k1gMnSc3	honič
a	a	k8xC	a
barváři	barvář	k1gMnSc3	barvář
Ohaři	ohař	k1gMnSc3	ohař
Slídiči	slídič	k1gMnSc3	slídič
<g/>
,	,	kIx,	,
retrívři	retrívr	k1gMnPc1	retrívr
a	a	k8xC	a
vodní	vodní	k2eAgMnPc1d1	vodní
psi	pes	k1gMnPc1	pes
Společenská	společenský	k2eAgNnPc4d1	společenské
plemena	plemeno	k1gNnPc4	plemeno
Chrti	chrt	k1gMnPc1	chrt
FCI	FCI	kA	FCI
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
přehlídkách	přehlídka	k1gFnPc6	přehlídka
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
členské	členský	k2eAgFnSc6d1	členská
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
jsou	být	k5eAaImIp3nP	být
posílány	posílat	k5eAaImNgInP	posílat
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kynologická	kynologický	k2eAgFnSc1d1	kynologická
federace	federace	k1gFnSc1	federace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Fédération	Fédération	k1gInSc4	Fédération
Cynologique	Cynologique	k1gFnSc4	Cynologique
Internationale	Internationale	k1gFnSc2	Internationale
mapa	mapa	k1gFnSc1	mapa
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
vyznačeni	vyznačen	k2eAgMnPc1d1	vyznačen
členové	člen	k1gMnPc1	člen
FCI	FCI	kA	FCI
ČeskoMoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
kynologická	kynologický	k2eAgFnSc1d1	kynologická
unie	unie	k1gFnSc1	unie
7	[number]	k4	7
nejprestižnějších	prestižní	k2eAgFnPc2d3	nejprestižnější
psích	psí	k2eAgFnPc2d1	psí
výstav	výstava	k1gFnPc2	výstava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
</s>
