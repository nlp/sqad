<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
majetkem	majetek	k1gInSc7	majetek
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
zakladateli	zakladatel	k1gMnSc6	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
genetiky	genetika	k1gFnSc2	genetika
<g/>
,	,	kIx,	,
meteorologovi	meteorolog	k1gMnSc6	meteorolog
Johannu	Johann	k1gMnSc6	Johann
Gregoru	Gregor	k1gMnSc6	Gregor
Mendelovi	Mendel	k1gMnSc6	Mendel
<g/>
.	.	kIx.	.
</s>
