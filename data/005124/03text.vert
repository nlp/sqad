<s>
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
polární	polární	k2eAgFnSc1d1	polární
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
Jamese	Jamese	k1gFnSc2	Jamese
Rosse	Rosse	k1gFnSc2	Rosse
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgMnS	založit
ji	on	k3xPp3gFnSc4	on
polárník	polárník	k1gMnSc1	polárník
Pavel	Pavel	k1gMnSc1	Pavel
Prošek	Prošek	k1gMnSc1	Prošek
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostně	slavnostně	k6eAd1	slavnostně
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
Česko	Česko	k1gNnSc1	Česko
stalo	stát	k5eAaPmAgNnS	stát
26	[number]	k4	26
<g/>
.	.	kIx.	.
zemí	zem	k1gFnSc7	zem
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
základnou	základna	k1gFnSc7	základna
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
majetkem	majetek	k1gInSc7	majetek
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
zakladateli	zakladatel	k1gMnSc6	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
genetiky	genetika	k1gFnSc2	genetika
<g/>
,	,	kIx,	,
meteorologovi	meteorolog	k1gMnSc6	meteorolog
Johannu	Johann	k1gMnSc6	Johann
Gregoru	Gregor	k1gMnSc6	Gregor
Mendelovi	Mendel	k1gMnSc6	Mendel
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
výzkumné	výzkumný	k2eAgFnSc3d1	výzkumná
činnosti	činnost	k1gFnSc3	činnost
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
stanici	stanice	k1gFnSc6	stanice
patří	patřit	k5eAaImIp3nS	patřit
Česko	Česko	k1gNnSc1	Česko
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
s	s	k7c7	s
hlasovacím	hlasovací	k2eAgNnSc7d1	hlasovací
právem	právo	k1gNnSc7	právo
v	v	k7c6	v
Antarktickém	antarktický	k2eAgInSc6d1	antarktický
smluvním	smluvní	k2eAgInSc6d1	smluvní
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
české	český	k2eAgFnSc2d1	Česká
polární	polární	k2eAgFnSc2d1	polární
stanice	stanice	k1gFnSc2	stanice
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zčásti	zčásti	k6eAd1	zčásti
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
politickými	politický	k2eAgInPc7d1	politický
zájmy	zájem	k1gInPc7	zájem
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
převažovaly	převažovat	k5eAaImAgFnP	převažovat
zejména	zejména	k9	zejména
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
možné	možný	k2eAgNnSc4d1	možné
rozšíření	rozšíření	k1gNnSc4	rozšíření
československého	československý	k2eAgInSc2d1	československý
biologického	biologický	k2eAgInSc2d1	biologický
<g/>
,	,	kIx,	,
glaciologického	glaciologický	k2eAgInSc2d1	glaciologický
<g/>
,	,	kIx,	,
klimatologického	klimatologický	k2eAgInSc2d1	klimatologický
a	a	k8xC	a
geologického	geologický	k2eAgInSc2d1	geologický
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
peněz	peníze	k1gInPc2	peníze
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
rovněž	rovněž	k9	rovněž
zájem	zájem	k1gInSc1	zájem
investovat	investovat	k5eAaBmF	investovat
do	do	k7c2	do
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
tak	tak	k6eAd1	tak
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
a	a	k8xC	a
specifické	specifický	k2eAgFnSc6d1	specifická
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
zájmu	zájem	k1gInSc2	zájem
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Českého	český	k2eAgInSc2d1	český
geologického	geologický	k2eAgInSc2d1	geologický
ústavu	ústav	k1gInSc2	ústav
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Česká	český	k2eAgFnSc1d1	Česká
geologická	geologický	k2eAgFnSc1d1	geologická
služba	služba	k1gFnSc1	služba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
vybrána	vybrat	k5eAaPmNgFnS	vybrat
i	i	k9	i
lokalita	lokalita	k1gFnSc1	lokalita
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Antarktidy	Antarktida	k1gFnSc2	Antarktida
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Prydz	Prydz	k1gMnSc1	Prydz
Bay	Bay	k1gMnSc1	Bay
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
později	pozdě	k6eAd2	pozdě
své	svůj	k3xOyFgFnPc4	svůj
stanice	stanice	k1gFnPc4	stanice
postavili	postavit	k5eAaPmAgMnP	postavit
Australané	Australan	k1gMnPc1	Australan
<g/>
,	,	kIx,	,
Rusové	Rus	k1gMnPc1	Rus
a	a	k8xC	a
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
dostatek	dostatek	k1gInSc4	dostatek
finančních	finanční	k2eAgInPc2d1	finanční
zdrojů	zdroj	k1gInPc2	zdroj
ovšem	ovšem	k9	ovšem
nebyl	být	k5eNaImAgInS	být
projekt	projekt	k1gInSc1	projekt
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
problém	problém	k1gInSc4	problém
nutnosti	nutnost	k1gFnSc2	nutnost
zaštítění	zaštítění	k1gNnSc2	zaštítění
některou	některý	k3yIgFnSc7	některý
z	z	k7c2	z
federálních	federální	k2eAgFnPc2d1	federální
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
plán	plán	k1gInSc1	plán
následně	následně	k6eAd1	následně
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
federálnímu	federální	k2eAgNnSc3d1	federální
shromáždění	shromáždění	k1gNnSc3	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
instituce	instituce	k1gFnSc1	instituce
se	se	k3xPyFc4	se
však	však	k9	však
nenašla	najít	k5eNaPmAgFnS	najít
a	a	k8xC	a
i	i	k9	i
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
vydala	vydat	k5eAaPmAgFnS	vydat
zamítavé	zamítavý	k2eAgNnSc4d1	zamítavé
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
česká	český	k2eAgFnSc1d1	Česká
vědecká	vědecký	k2eAgFnSc1d1	vědecká
výprava	výprava	k1gFnSc1	výprava
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Českého	český	k2eAgInSc2d1	český
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
prováděla	provádět	k5eAaImAgFnS	provádět
výzkumy	výzkum	k1gInPc4	výzkum
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
stanice	stanice	k1gFnSc2	stanice
polské	polský	k2eAgFnSc2d1	polská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
dalšími	další	k2eAgFnPc7d1	další
stanicemi	stanice	k1gFnPc7	stanice
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Hostování	hostování	k1gNnSc1	hostování
na	na	k7c6	na
cizí	cizí	k2eAgFnSc6d1	cizí
stanici	stanice	k1gFnSc6	stanice
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
konfliktům	konflikt	k1gInPc3	konflikt
mezi	mezi	k7c7	mezi
výzkumnými	výzkumný	k2eAgInPc7d1	výzkumný
týmy	tým	k1gInPc7	tým
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vyvstal	vyvstat	k5eAaPmAgInS	vyvstat
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
společné	společný	k2eAgFnSc2d1	společná
stanice	stanice	k1gFnSc2	stanice
pro	pro	k7c4	pro
země	zem	k1gFnPc4	zem
visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
čtyřky	čtyřka	k1gFnSc2	čtyřka
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
rozdělením	rozdělení	k1gNnSc7	rozdělení
financování	financování	k1gNnSc2	financování
nakonec	nakonec	k6eAd1	nakonec
zamítnut	zamítnut	k2eAgMnSc1d1	zamítnut
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
česká	český	k2eAgFnSc1d1	Česká
vědecká	vědecký	k2eAgFnSc1d1	vědecká
činnost	činnost	k1gFnSc1	činnost
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
se	se	k3xPyFc4	se
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
až	až	k9	až
2004	[number]	k4	2004
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
absenci	absence	k1gFnSc3	absence
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
přípravy	příprava	k1gFnSc2	příprava
však	však	k9	však
již	již	k6eAd1	již
probíhaly	probíhat	k5eAaImAgFnP	probíhat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
prováděna	provádět	k5eAaImNgFnS	provádět
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
britské	britský	k2eAgFnSc6d1	britská
a	a	k8xC	a
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
polohu	poloha	k1gFnSc4	poloha
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
české	český	k2eAgFnSc2d1	Česká
stanice	stanice	k1gFnSc2	stanice
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
zvolen	zvolit	k5eAaPmNgInS	zvolit
Turret	Turret	k1gInSc1	Turret
Point	pointa	k1gFnPc2	pointa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
u	u	k7c2	u
zátoky	zátoka	k1gFnSc2	zátoka
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Krále	Král	k1gMnSc2	Král
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
dnešní	dnešní	k2eAgFnSc2d1	dnešní
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Lokace	lokace	k1gFnSc1	lokace
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
předložena	předložit	k5eAaPmNgFnS	předložit
Výboru	výbor	k1gInSc3	výbor
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
antarktického	antarktický	k2eAgNnSc2d1	antarktické
prostředí	prostředí	k1gNnSc2	prostředí
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ten	ten	k3xDgMnSc1	ten
ji	on	k3xPp3gFnSc4	on
zamítl	zamítnout	k5eAaPmAgMnS	zamítnout
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
množství	množství	k1gNnSc2	množství
stanic	stanice	k1gFnPc2	stanice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
návrhu	návrh	k1gInSc3	návrh
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
o	o	k7c4	o
prohlášení	prohlášení	k1gNnSc4	prohlášení
území	území	k1gNnSc2	území
za	za	k7c4	za
chráněné	chráněný	k2eAgFnPc4d1	chráněná
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
další	další	k2eAgFnSc4d1	další
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
využití	využití	k1gNnSc3	využití
některé	některý	k3yIgFnSc2	některý
ze	z	k7c2	z
zakonzervovaných	zakonzervovaný	k2eAgFnPc2d1	zakonzervovaná
britských	britský	k2eAgFnPc2d1	britská
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
české	český	k2eAgFnSc3d1	Česká
vědecké	vědecký	k2eAgFnSc3d1	vědecká
výpravě	výprava	k1gFnSc3	výprava
nevyhovovala	vyhovovat	k5eNaImAgFnS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Zvolilo	zvolit	k5eAaPmAgNnS	zvolit
se	se	k3xPyFc4	se
tak	tak	k9	tak
nové	nový	k2eAgNnSc1d1	nové
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
představeno	představit	k5eAaPmNgNnS	představit
na	na	k7c6	na
následující	následující	k2eAgFnSc6d1	následující
konferenci	konference	k1gFnSc6	konference
Výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
<s>
Schválení	schválení	k1gNnSc1	schválení
nové	nový	k2eAgFnSc2d1	nová
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
polohy	poloha	k1gFnSc2	poloha
stanice	stanice	k1gFnSc2	stanice
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Lokalita	lokalita	k1gFnSc1	lokalita
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
cípu	cíp	k1gInSc6	cíp
ostrova	ostrov	k1gInSc2	ostrov
Jamese	Jamese	k1gFnSc2	Jamese
Rosse	Rosse	k1gFnSc2	Rosse
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
základna	základna	k1gFnSc1	základna
opravdu	opravdu	k6eAd1	opravdu
později	pozdě	k6eAd2	pozdě
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
mluvilo	mluvit	k5eAaImAgNnS	mluvit
zejména	zejména	k9	zejména
nezaledněné	zaledněný	k2eNgNnSc1d1	zaledněný
okolí	okolí	k1gNnSc1	okolí
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
stanic	stanice	k1gFnPc2	stanice
–	–	k?	–
nejblíže	blízce	k6eAd3	blízce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
argentinská	argentinský	k2eAgFnSc1d1	Argentinská
stanice	stanice	k1gFnSc1	stanice
Marambio	Marambio	k6eAd1	Marambio
ležící	ležící	k2eAgFnSc1d1	ležící
přes	přes	k7c4	přes
sedmdesát	sedmdesát	k4xCc4	sedmdesát
kilometrů	kilometr	k1gInPc2	kilometr
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
kongresu	kongres	k1gInSc6	kongres
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
antarktického	antarktický	k2eAgNnSc2d1	antarktické
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
dopracována	dopracován	k2eAgFnSc1d1	dopracována
projektová	projektový	k2eAgFnSc1d1	projektová
dokumentace	dokumentace	k1gFnSc1	dokumentace
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ten	ten	k3xDgMnSc1	ten
ji	on	k3xPp3gFnSc4	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
kvůli	kvůli	k7c3	kvůli
úpravám	úprava	k1gFnPc3	úprava
provedeným	provedený	k2eAgFnPc3d1	provedená
po	po	k7c6	po
termínu	termín	k1gInSc6	termín
<g/>
.	.	kIx.	.
</s>
<s>
Definitivního	definitivní	k2eAgNnSc2d1	definitivní
schválení	schválení	k1gNnSc2	schválení
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
dočkala	dočkat	k5eAaPmAgFnS	dočkat
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
nezměněném	změněný	k2eNgInSc6d1	nezměněný
rozsahu	rozsah	k1gInSc6	rozsah
až	až	k9	až
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
transport	transport	k1gInSc1	transport
materiálu	materiál	k1gInSc2	materiál
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
budoucí	budoucí	k2eAgFnSc2d1	budoucí
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
stanice	stanice	k1gFnSc2	stanice
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dbát	dbát	k5eAaImF	dbát
o	o	k7c6	o
zachování	zachování	k1gNnSc6	zachování
minimální	minimální	k2eAgFnSc2d1	minimální
energetické	energetický	k2eAgFnSc2d1	energetická
náročnosti	náročnost	k1gFnSc2	náročnost
a	a	k8xC	a
také	také	k9	také
minimální	minimální	k2eAgFnSc2d1	minimální
ekologické	ekologický	k2eAgFnSc2d1	ekologická
zátěže	zátěž	k1gFnSc2	zátěž
pro	pro	k7c4	pro
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
hlavní	hlavní	k2eAgFnPc1d1	hlavní
zásady	zásada	k1gFnPc1	zásada
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
stanice	stanice	k1gFnSc2	stanice
bylo	být	k5eAaImAgNnS	být
<g/>
:	:	kIx,	:
vybudování	vybudování	k1gNnSc1	vybudování
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
zázemí	zázemí	k1gNnSc2	zázemí
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
relaxaci	relaxace	k1gFnSc4	relaxace
<g/>
,	,	kIx,	,
hygienu	hygiena	k1gFnSc4	hygiena
i	i	k8xC	i
stravování	stravování	k1gNnSc4	stravování
<g/>
,	,	kIx,	,
minimalizace	minimalizace	k1gFnSc1	minimalizace
rizik	riziko	k1gNnPc2	riziko
požáru	požár	k1gInSc2	požár
či	či	k8xC	či
havárie	havárie	k1gFnSc2	havárie
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgInPc1d1	vhodný
stavební	stavební	k2eAgInPc1d1	stavební
materiály	materiál	k1gInPc1	materiál
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
tepelné	tepelný	k2eAgFnSc2d1	tepelná
izolace	izolace	k1gFnSc2	izolace
a	a	k8xC	a
odolnosti	odolnost	k1gFnSc2	odolnost
vůči	vůči	k7c3	vůči
korozi	koroze	k1gFnSc3	koroze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekologické	ekologický	k2eAgNnSc1d1	ekologické
získávání	získávání	k1gNnSc1	získávání
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
větrem	vítr	k1gInSc7	vítr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přívod	přívod	k1gInSc4	přívod
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
ledovcového	ledovcový	k2eAgInSc2d1	ledovcový
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgNnSc1d1	vhodné
a	a	k8xC	a
dostatečně	dostatečně	k6eAd1	dostatečně
ekologické	ekologický	k2eAgNnSc4d1	ekologické
nakládání	nakládání	k1gNnSc4	nakládání
s	s	k7c7	s
odpady	odpad	k1gInPc7	odpad
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ostrov	ostrov	k1gInSc1	ostrov
Jamese	Jamese	k1gFnSc2	Jamese
Rosse	Rosse	k1gFnSc2	Rosse
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
63	[number]	k4	63
<g/>
°	°	k?	°
<g/>
48	[number]	k4	48
<g/>
'	'	kIx"	'
<g/>
0	[number]	k4	0
<g/>
2,3	[number]	k4	2,3
<g/>
''	''	k?	''
j.	j.	k?	j.
š.	š.	k?	š.
<g/>
,	,	kIx,	,
57	[number]	k4	57
<g/>
°	°	k?	°
<g/>
52	[number]	k4	52
<g/>
'	'	kIx"	'
<g/>
59,9	[number]	k4	59,9
<g/>
''	''	k?	''
z.	z.	k?	z.
d.	d.	k?	d.
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Jamese	Jamese	k1gFnSc2	Jamese
Rosse	Rosse	k1gFnSc2	Rosse
nedaleko	nedaleko	k7c2	nedaleko
pobřeží	pobřeží	k1gNnSc2	pobřeží
Antarktického	antarktický	k2eAgInSc2d1	antarktický
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
poloostrov	poloostrov	k1gInSc1	poloostrov
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
z	z	k7c2	z
pevninské	pevninský	k2eAgFnSc2d1	pevninská
Antarktidy	Antarktida	k1gFnSc2	Antarktida
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
k	k	k7c3	k
jižnímu	jižní	k2eAgInSc3d1	jižní
cípu	cíp	k1gInSc3	cíp
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Jamese	Jamese	k1gFnSc2	Jamese
Rosse	Rosse	k1gFnSc2	Rosse
zřejmě	zřejmě	k6eAd1	zřejmě
poprvé	poprvé	k6eAd1	poprvé
doplul	doplout	k5eAaPmAgMnS	doplout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
James	James	k1gInSc4	James
Clark	Clark	k1gInSc4	Clark
Ross	Rossa	k1gFnPc2	Rossa
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
ho	on	k3xPp3gNnSc4	on
jako	jako	k9	jako
Haddingtonovu	Haddingtonův	k2eAgFnSc4d1	Haddingtonův
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1902	[number]	k4	1902
<g/>
/	/	kIx~	/
<g/>
1903	[number]	k4	1903
však	však	k8xC	však
výprava	výprava	k1gMnSc1	výprava
Otto	Otto	k1gMnSc1	Otto
Nordenskjölda	Nordenskjölda	k1gFnSc1	Nordenskjölda
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Haddingtonova	Haddingtonův	k2eAgFnSc1d1	Haddingtonův
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ostrovem	ostrov	k1gInSc7	ostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
svého	svůj	k3xOyFgMnSc2	svůj
objevitele	objevitel	k1gMnSc2	objevitel
ostrovem	ostrov	k1gInSc7	ostrov
Jamese	Jamese	k1gFnPc1	Jamese
Rosse	Rosse	k1gFnSc2	Rosse
<g/>
.	.	kIx.	.
</s>
<s>
Průliv	průliv	k1gInSc1	průliv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
nazvala	nazvat	k5eAaPmAgFnS	nazvat
průlivem	průliv	k1gInSc7	průliv
prince	princ	k1gMnSc2	princ
Gustava	Gustav	k1gMnSc2	Gustav
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
hustotou	hustota	k1gFnSc7	hustota
polárních	polární	k2eAgFnPc2d1	polární
stanic	stanice	k1gFnPc2	stanice
–	–	k?	–
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
je	být	k5eAaImIp3nS	být
argentinská	argentinský	k2eAgFnSc1d1	Argentinská
základna	základna	k1gFnSc1	základna
Marambio	Marambio	k6eAd1	Marambio
na	na	k7c6	na
Seymourově	Seymourův	k2eAgInSc6d1	Seymourův
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
polární	polární	k2eAgFnSc1d1	polární
stanice	stanice	k1gFnSc1	stanice
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
Jamese	Jamese	k1gFnSc2	Jamese
Rosse	Rosse	k1gFnSc2	Rosse
(	(	kIx(	(
<g/>
na	na	k7c6	na
mírně	mírně	k6eAd1	mírně
kamenité	kamenitý	k2eAgFnSc6d1	kamenitá
pláži	pláž	k1gFnSc3	pláž
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
m	m	kA	m
od	od	k7c2	od
mořského	mořský	k2eAgNnSc2d1	mořské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
9	[number]	k4	9
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mezi	mezi	k7c4	mezi
mysy	mys	k1gInPc4	mys
Bibby	Bibba	k1gFnSc2	Bibba
Point	pointa	k1gFnPc2	pointa
a	a	k8xC	a
Cape	capat	k5eAaImIp3nS	capat
Lachman	Lachman	k1gMnSc1	Lachman
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
směřována	směřován	k2eAgFnSc1d1	směřována
do	do	k7c2	do
cca	cca	kA	cca
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
km	km	kA	km
širokého	široký	k2eAgInSc2d1	široký
průlivu	průliv	k1gInSc2	průliv
prince	princ	k1gMnSc2	princ
Gustava	Gustav	k1gMnSc2	Gustav
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
vrstvou	vrstva	k1gFnSc7	vrstva
věčného	věčný	k2eAgInSc2d1	věčný
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
poprvé	poprvé	k6eAd1	poprvé
rozmrzl	rozmrznout	k5eAaPmAgInS	rozmrznout
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
plný	plný	k2eAgInSc4d1	plný
ledových	ledový	k2eAgFnPc2d1	ledová
ker	kra	k1gFnPc2	kra
a	a	k8xC	a
kusů	kus	k1gInPc2	kus
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
stále	stále	k6eAd1	stále
(	(	kIx(	(
<g/>
k	k	k7c3	k
r.	r.	kA	r.
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
Ulu	Ulu	k1gFnSc1	Ulu
Peninsula	Peninsula	k1gFnSc1	Peninsula
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
odledněných	odledněný	k2eAgFnPc2d1	odledněná
částí	část	k1gFnPc2	část
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Odledněno	odledněn	k2eAgNnSc1d1	odledněn
je	být	k5eAaImIp3nS	být
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
díky	díky	k7c3	díky
srážkovému	srážkový	k2eAgInSc3d1	srážkový
stínu	stín	k1gInSc3	stín
Antarktického	antarktický	k2eAgInSc2d1	antarktický
poloostrova	poloostrov	k1gInSc2	poloostrov
má	mít	k5eAaImIp3nS	mít
nízký	nízký	k2eAgInSc1d1	nízký
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
cca	cca	kA	cca
300	[number]	k4	300
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
tající	tající	k2eAgInSc1d1	tající
sníh	sníh	k1gInSc1	sníh
z	z	k7c2	z
občasných	občasný	k2eAgFnPc2d1	občasná
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
rychle	rychle	k6eAd1	rychle
vsakuje	vsakovat	k5eAaImIp3nS	vsakovat
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
totiž	totiž	k9	totiž
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
mořské	mořský	k2eAgFnSc6d1	mořská
terase	terasa	k1gFnSc6	terasa
tvořené	tvořený	k2eAgNnSc1d1	tvořené
kompaktním	kompaktní	k2eAgInSc7d1	kompaktní
jemným	jemný	k2eAgInSc7d1	jemný
pískem	písek	k1gInSc7	písek
<g/>
,	,	kIx,	,
regolitem	regolit	k1gInSc7	regolit
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
chod	chod	k1gInSc1	chod
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
minima	minimum	k1gNnPc1	minimum
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
maxima	maximum	k1gNnSc2	maximum
12,5	[number]	k4	12,5
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
Ø	Ø	k?	Ø
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlhkost	vlhkost	k1gFnSc1	vlhkost
minima	minimum	k1gNnSc2	minimum
71,2	[number]	k4	71,2
%	%	kIx~	%
a	a	k8xC	a
maxima	maximum	k1gNnSc2	maximum
88,1	[number]	k4	88,1
%	%	kIx~	%
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
81	[number]	k4	81
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
umístění	umístění	k1gNnSc1	umístění
stanice	stanice	k1gFnSc2	stanice
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
hlavní	hlavní	k2eAgNnPc4d1	hlavní
kritéria	kritérion	k1gNnPc4	kritérion
<g/>
:	:	kIx,	:
přístupnost	přístupnost	k1gFnSc1	přístupnost
lodí	loď	k1gFnPc2	loď
či	či	k8xC	či
helikoptérou	helikoptéra	k1gFnSc7	helikoptéra
z	z	k7c2	z
nejbližších	blízký	k2eAgFnPc2d3	nejbližší
polárních	polární	k2eAgFnPc2d1	polární
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
vhodnost	vhodnost	k1gFnSc1	vhodnost
přistávacích	přistávací	k2eAgNnPc2d1	přistávací
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
logistické	logistický	k2eAgFnPc4d1	logistická
operace	operace	k1gFnPc4	operace
<g/>
,	,	kIx,	,
pestrost	pestrost	k1gFnSc4	pestrost
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc4d1	umožňující
co	co	k8xS	co
nejširší	široký	k2eAgNnSc4d3	nejširší
spektrum	spektrum	k1gNnSc4	spektrum
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukční	konstrukční	k2eAgInPc1d1	konstrukční
prvky	prvek	k1gInPc1	prvek
stanice	stanice	k1gFnSc2	stanice
i	i	k8xC	i
části	část	k1gFnSc2	část
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
příprav	příprava	k1gFnPc2	příprava
některé	některý	k3yIgFnPc1	některý
její	její	k3xOp3gFnPc1	její
části	část	k1gFnPc1	část
i	i	k9	i
složeny	složit	k5eAaPmNgInP	složit
a	a	k8xC	a
vyzkoušeny	vyzkoušet	k5eAaPmNgInP	vyzkoušet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
minimalizoval	minimalizovat	k5eAaBmAgInS	minimalizovat
čas	čas	k1gInSc1	čas
stavby	stavba	k1gFnSc2	stavba
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
případné	případný	k2eAgInPc4d1	případný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
přeprava	přeprava	k1gFnSc1	přeprava
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
materiál	materiál	k1gInSc1	materiál
putoval	putovat	k5eAaImAgInS	putovat
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Hamburku	Hamburk	k1gInSc2	Hamburk
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
chilského	chilský	k2eAgInSc2d1	chilský
přístavu	přístav	k1gInSc2	přístav
Punta	punto	k1gNnSc2	punto
Arenas	Arenasa	k1gFnPc2	Arenasa
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vše	všechen	k3xTgNnSc4	všechen
převezeno	převezen	k2eAgNnSc4d1	převezeno
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Jamese	Jamese	k1gFnSc2	Jamese
Rosse	Rosse	k1gFnSc2	Rosse
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
při	při	k7c6	při
transportu	transport	k1gInSc6	transport
nastaly	nastat	k5eAaPmAgFnP	nastat
potíže	potíž	k1gFnPc1	potíž
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
Anatarctic	Anatarctice	k1gFnPc2	Anatarctice
Dream	Dream	k1gInSc1	Dream
<g/>
,	,	kIx,	,
nevyplula	vyplout	k5eNaPmAgFnS	vyplout
kvůli	kvůli	k7c3	kvůli
špatnému	špatný	k2eAgInSc3d1	špatný
technickému	technický	k2eAgInSc3d1	technický
stavu	stav	k1gInSc3	stav
vůbec	vůbec	k9	vůbec
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
Porvenir	Porvenir	k1gInSc4	Porvenir
I.	I.	kA	I.
<g/>
,	,	kIx,	,
havarovala	havarovat	k5eAaPmAgFnS	havarovat
nedaleko	nedaleko	k7c2	nedaleko
přístavu	přístav	k1gInSc2	přístav
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
nakládku	nakládka	k1gFnSc4	nakládka
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
až	až	k9	až
třetí	třetí	k4xOgFnSc1	třetí
přeprava	přeprava	k1gFnSc1	přeprava
chilským	chilský	k2eAgMnSc7d1	chilský
vojenským	vojenský	k2eAgMnSc7d1	vojenský
ledoborcem	ledoborec	k1gMnSc7	ledoborec
Oscar	Oscara	k1gFnPc2	Oscara
Almirante	Almirant	k1gMnSc5	Almirant
Viel	Viela	k1gFnPc2	Viela
<g/>
.	.	kIx.	.
</s>
<s>
Ledoborec	ledoborec	k1gMnSc1	ledoborec
se	se	k3xPyFc4	se
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
budoucí	budoucí	k2eAgFnSc2d1	budoucí
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
čekala	čekat	k5eAaImAgFnS	čekat
přípravná	přípravný	k2eAgFnSc1d1	přípravná
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
v	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
a	a	k8xC	a
během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
dvou	dva	k4xCgInPc2	dva
dnů	den	k1gInPc2	den
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
lodi	loď	k1gFnSc2	loď
vyloženo	vyložit	k5eAaPmNgNnS	vyložit
osm	osm	k4xCc1	osm
kontejnerů	kontejner	k1gInPc2	kontejner
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
hmotnosti	hmotnost	k1gFnSc6	hmotnost
130	[number]	k4	130
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
následovaly	následovat	k5eAaImAgFnP	následovat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
vyloďovacích	vyloďovací	k2eAgFnPc2d1	vyloďovací
činností	činnost	k1gFnPc2	činnost
a	a	k8xC	a
z	z	k7c2	z
první	první	k4xOgFnSc2	první
dodávky	dodávka	k1gFnSc2	dodávka
materiálu	materiál	k1gInSc2	materiál
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
antarktického	antarktický	k2eAgNnSc2d1	antarktické
léta	léto	k1gNnSc2	léto
postavena	postaven	k2eAgFnSc1d1	postavena
během	během	k7c2	během
sedmi	sedm	k4xCc2	sedm
dnů	den	k1gInPc2	den
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
uskladněn	uskladněn	k2eAgInSc4d1	uskladněn
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc1	první
dodávka	dodávka	k1gFnSc1	dodávka
neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
veškeré	veškerý	k3xTgInPc4	veškerý
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
systémy	systém	k1gInPc4	systém
nutné	nutný	k2eAgInPc4d1	nutný
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
i	i	k8xC	i
přilehlé	přilehlý	k2eAgInPc1d1	přilehlý
kontejnery	kontejner	k1gInPc1	kontejner
zazimovány	zazimován	k2eAgInPc1d1	zazimován
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
další	další	k2eAgInSc4d1	další
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Povětrnostní	povětrnostní	k2eAgFnPc1d1	povětrnostní
podmínky	podmínka	k1gFnPc1	podmínka
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
na	na	k7c4	na
antarktické	antarktický	k2eAgInPc4d1	antarktický
poměry	poměr	k1gInPc4	poměr
velmi	velmi	k6eAd1	velmi
příznivé	příznivý	k2eAgInPc4d1	příznivý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
-8	-8	k4	-8
do	do	k7c2	do
+6	+6	k4	+6
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
vítr	vítr	k1gInSc1	vítr
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
až	až	k9	až
120	[number]	k4	120
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
někdy	někdy	k6eAd1	někdy
neustával	ustávat	k5eNaImAgInS	ustávat
po	po	k7c4	po
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
veškerých	veškerý	k3xTgFnPc6	veškerý
přípravách	příprava	k1gFnPc6	příprava
byla	být	k5eAaImAgFnS	být
stanice	stanice	k1gFnSc1	stanice
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
a	a	k8xC	a
pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
<g/>
.	.	kIx.	.
</s>
<s>
Slavnosti	slavnost	k1gFnSc2	slavnost
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
členové	člen	k1gMnPc1	člen
vědeckého	vědecký	k2eAgInSc2d1	vědecký
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
českého	český	k2eAgNnSc2d1	české
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
a	a	k8xC	a
děkan	děkan	k1gMnSc1	děkan
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
výstavbu	výstavba	k1gFnSc4	výstavba
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
nositelem	nositel	k1gMnSc7	nositel
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
zhotovitelem	zhotovitel	k1gMnSc7	zhotovitel
celého	celý	k2eAgInSc2d1	celý
komplexu	komplex	k1gInSc2	komplex
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
stala	stát	k5eAaPmAgFnS	stát
firma	firma	k1gFnSc1	firma
PSG	PSG	kA	PSG
-International	-Internationat	k5eAaImAgInS	-Internationat
ze	z	k7c2	z
Zlína	Zlín	k1gInSc2	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
stanice	stanice	k1gFnSc2	stanice
činily	činit	k5eAaImAgFnP	činit
asi	asi	k9	asi
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
tradičními	tradiční	k2eAgFnPc7d1	tradiční
antarktickými	antarktický	k2eAgFnPc7d1	antarktická
základnami	základna	k1gFnPc7	základna
spíše	spíše	k9	spíše
menší	malý	k2eAgFnSc3d2	menší
velikosti	velikost	k1gFnSc3	velikost
a	a	k8xC	a
vymyká	vymykat	k5eAaImIp3nS	vymykat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
typickému	typický	k2eAgInSc3d1	typický
vzhledu	vzhled	k1gInSc2	vzhled
předimenzovaných	předimenzovaný	k2eAgFnPc2d1	předimenzovaná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
postaveny	postavit	k5eAaPmNgInP	postavit
jinými	jiný	k2eAgFnPc7d1	jiná
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
konstrukce	konstrukce	k1gFnSc1	konstrukce
je	být	k5eAaImIp3nS	být
podřízena	podřídit	k5eAaPmNgFnS	podřídit
extrémním	extrémní	k2eAgFnPc3d1	extrémní
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
panují	panovat	k5eAaImIp3nP	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
především	především	k9	především
jednopatrovou	jednopatrový	k2eAgFnSc7d1	jednopatrová
hlavní	hlavní	k2eAgFnSc7d1	hlavní
budovou	budova	k1gFnSc7	budova
(	(	kIx(	(
<g/>
dřevostavba	dřevostavba	k1gFnSc1	dřevostavba
26,5	[number]	k4	26,5
m	m	kA	m
×	×	k?	×
11,5	[number]	k4	11,5
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
2,8	[number]	k4	2,8
<g/>
–	–	k?	–
<g/>
3,6	[number]	k4	3,6
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
využívaná	využívaný	k2eAgFnSc1d1	využívaná
k	k	k7c3	k
ubytování	ubytování	k1gNnSc3	ubytování
<g/>
,	,	kIx,	,
stravování	stravování	k1gNnSc3	stravování
<g/>
,	,	kIx,	,
relaxaci	relaxace	k1gFnSc3	relaxace
i	i	k8xC	i
k	k	k7c3	k
laboratorní	laboratorní	k2eAgFnSc3d1	laboratorní
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvanáct	dvanáct	k4xCc4	dvanáct
jedno-	jedno-	k?	jedno-
až	až	k6eAd1	až
dvoulůžkových	dvoulůžkový	k2eAgFnPc2d1	dvoulůžková
ložnic	ložnice	k1gFnPc2	ložnice
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
společné	společný	k2eAgFnPc4d1	společná
pracovny	pracovna	k1gFnPc4	pracovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jídelna	jídelna	k1gFnSc1	jídelna
<g/>
,	,	kIx,	,
kuchyňka	kuchyňka	k1gFnSc1	kuchyňka
<g/>
,	,	kIx,	,
hygienické	hygienický	k2eAgNnSc1d1	hygienické
zařízení	zařízení	k1gNnSc1	zařízení
a	a	k8xC	a
sušárna	sušárna	k1gFnSc1	sušárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základech	základ	k1gInPc6	základ
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
budova	budova	k1gFnSc1	budova
mříž	mříž	k1gFnSc1	mříž
z	z	k7c2	z
dubových	dubový	k2eAgInPc2d1	dubový
pražců	pražec	k1gInPc2	pražec
zapuštěných	zapuštěný	k2eAgInPc2d1	zapuštěný
do	do	k7c2	do
mělkých	mělký	k2eAgInPc2d1	mělký
základů	základ	k1gInPc2	základ
a	a	k8xC	a
tvořících	tvořící	k2eAgInPc2d1	tvořící
konstrukci	konstrukce	k1gFnSc4	konstrukce
stojící	stojící	k2eAgFnSc4d1	stojící
nejméně	málo	k6eAd3	málo
40	[number]	k4	40
cm	cm	kA	cm
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
omezily	omezit	k5eAaPmAgFnP	omezit
ztráty	ztráta	k1gFnPc1	ztráta
tepla	teplo	k1gNnSc2	teplo
do	do	k7c2	do
chladné	chladný	k2eAgFnSc2d1	chladná
antarktické	antarktický	k2eAgFnSc2d1	antarktická
zeminy	zemina	k1gFnSc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
(	(	kIx(	(
<g/>
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
265	[number]	k4	265
mm	mm	kA	mm
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
systémem	systém	k1gInSc7	systém
K-Kontrol	K-Kontrola	k1gFnPc2	K-Kontrola
<g/>
,	,	kIx,	,
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
OSB	OSB	kA	OSB
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
umístěné	umístěný	k2eAgFnPc1d1	umístěná
izolační	izolační	k2eAgFnPc1d1	izolační
výplně	výplň	k1gFnPc1	výplň
<g/>
.	.	kIx.	.
</s>
<s>
Zvnějšku	zvnějšku	k6eAd1	zvnějšku
obvodových	obvodový	k2eAgFnPc2d1	obvodová
stěn	stěna	k1gFnPc2	stěna
je	být	k5eAaImIp3nS	být
překližka	překližka	k1gFnSc1	překližka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
konstrukci	konstrukce	k1gFnSc4	konstrukce
před	před	k7c7	před
nepříznivými	příznivý	k2eNgInPc7d1	nepříznivý
vlivy	vliv	k1gInPc7	vliv
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
slaný	slaný	k2eAgInSc1d1	slaný
aerosol	aerosol	k1gInSc1	aerosol
<g/>
,	,	kIx,	,
občasné	občasný	k2eAgFnPc1d1	občasná
písečné	písečný	k2eAgFnPc4d1	písečná
bouře	bouř	k1gFnPc4	bouř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podlaha	podlaha	k1gFnSc1	podlaha
a	a	k8xC	a
stropy	strop	k1gInPc1	strop
jsou	být	k5eAaImIp3nP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
tloušťku	tloušťka	k1gFnSc4	tloušťka
(	(	kIx(	(
<g/>
320	[number]	k4	320
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Střecha	střecha	k1gFnSc1	střecha
je	být	k5eAaImIp3nS	být
zkosena	zkosen	k2eAgFnSc1d1	zkosena
5	[number]	k4	5
%	%	kIx~	%
k	k	k7c3	k
jižní	jižní	k2eAgFnSc3d1	jižní
straně	strana	k1gFnSc3	strana
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
polyvinylchloridovou	polyvinylchloridový	k2eAgFnSc7d1	polyvinylchloridový
deskou	deska	k1gFnSc7	deska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
objekt	objekt	k1gInSc4	objekt
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
účinky	účinek	k1gInPc7	účinek
UV	UV	kA	UV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dalších	další	k2eAgInPc2d1	další
devět	devět	k4xCc1	devět
technických	technický	k2eAgInPc2d1	technický
kontejnerů	kontejner	k1gInPc2	kontejner
(	(	kIx(	(
<g/>
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
6	[number]	k4	6
×	×	k?	×
2,5	[number]	k4	2,5
×	×	k?	×
2,6	[number]	k4	2,6
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
sklady	sklad	k1gInPc1	sklad
dieselových	dieselový	k2eAgInPc2d1	dieselový
agregátů	agregát	k1gInPc2	agregát
a	a	k8xC	a
náhradních	náhradní	k2eAgInPc2d1	náhradní
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
garáže	garáž	k1gFnPc1	garáž
<g/>
,	,	kIx,	,
spalovna	spalovna	k1gFnSc1	spalovna
odpadů	odpad	k1gInPc2	odpad
či	či	k8xC	či
elektrocentrála	elektrocentrála	k1gFnSc1	elektrocentrála
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kontejnerech	kontejner	k1gInPc6	kontejner
jsou	být	k5eAaImIp3nP	být
připevněny	připevněn	k2eAgFnPc1d1	připevněna
větrné	větrný	k2eAgFnPc1d1	větrná
elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
při	při	k7c6	při
silném	silný	k2eAgInSc6d1	silný
větru	vítr	k1gInSc6	vítr
sklopit	sklopit	k5eAaPmF	sklopit
<g/>
.	.	kIx.	.
</s>
<s>
Plošné	plošný	k2eAgNnSc1d1	plošné
rozptýlení	rozptýlení	k1gNnSc1	rozptýlení
objektů	objekt	k1gInPc2	objekt
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc1	riziko
požáru	požár	k1gInSc2	požár
nebo	nebo	k8xC	nebo
ekologické	ekologický	k2eAgFnSc2d1	ekologická
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
sezónní	sezónní	k2eAgInSc4d1	sezónní
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
během	během	k7c2	během
letního	letní	k2eAgNnSc2d1	letní
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
pracuje	pracovat	k5eAaImIp3nS	pracovat
asi	asi	k9	asi
patnáct	patnáct	k4xCc4	patnáct
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
letní	letní	k2eAgFnSc4d1	letní
sezónu	sezóna	k1gFnSc4	sezóna
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
problém	problém	k1gInSc1	problém
s	s	k7c7	s
vytápěním	vytápění	k1gNnSc7	vytápění
a	a	k8xC	a
zajištěním	zajištění	k1gNnSc7	zajištění
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
získávat	získávat	k5eAaImF	získávat
roztápěním	roztápění	k1gNnSc7	roztápění
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
zimní	zimní	k2eAgInSc1d1	zimní
pobyt	pobyt	k1gInSc1	pobyt
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
spotřeboval	spotřebovat	k5eAaPmAgMnS	spotřebovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
nafty	nafta	k1gFnSc2	nafta
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
vybavena	vybavit	k5eAaPmNgFnS	vybavit
promyšleným	promyšlený	k2eAgInSc7d1	promyšlený
systémem	systém	k1gInSc7	systém
alternativních	alternativní	k2eAgInPc2d1	alternativní
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
spotřebu	spotřeba	k1gFnSc4	spotřeba
nafty	nafta	k1gFnSc2	nafta
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
budovy	budova	k1gFnSc2	budova
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
dopadá	dopadat	k5eAaImIp3nS	dopadat
nejvíce	hodně	k6eAd3	hodně
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgInPc1d1	umístěn
kolektory	kolektor	k1gInPc1	kolektor
pokrývající	pokrývající	k2eAgFnSc4d1	pokrývající
plochu	plocha	k1gFnSc4	plocha
36	[number]	k4	36
m	m	kA	m
<g/>
2	[number]	k4	2
zdi	zeď	k1gFnPc1	zeď
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ohřívají	ohřívat	k5eAaImIp3nP	ohřívat
vzduch	vzduch	k1gInSc4	vzduch
až	až	k9	až
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
55	[number]	k4	55
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
vhání	vhánět	k5eAaImIp3nS	vhánět
do	do	k7c2	do
místností	místnost	k1gFnPc2	místnost
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
teplotu	teplota	k1gFnSc4	teplota
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
°	°	k?	°
<g/>
C.	C.	kA	C.
Dále	daleko	k6eAd2	daleko
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
původně	původně	k6eAd1	původně
byly	být	k5eAaImAgInP	být
upevněny	upevnit	k5eAaPmNgInP	upevnit
také	také	k9	také
ploché	plochý	k2eAgInPc1d1	plochý
panely	panel	k1gInPc1	panel
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
teplých	teplý	k2eAgInPc6d1	teplý
dnech	den	k1gInPc6	den
ohřívala	ohřívat	k5eAaImAgFnS	ohřívat
voda	voda	k1gFnSc1	voda
pro	pro	k7c4	pro
kuchyň	kuchyň	k1gFnSc4	kuchyň
a	a	k8xC	a
koupelnu	koupelna	k1gFnSc4	koupelna
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedostatečné	dostatečný	k2eNgFnSc3d1	nedostatečná
kapacitě	kapacita	k1gFnSc3	kapacita
těchto	tento	k3xDgInPc2	tento
panelů	panel	k1gInPc2	panel
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
výměně	výměna	k1gFnSc3	výměna
za	za	k7c4	za
fotovoltaické	fotovoltaický	k2eAgInPc4d1	fotovoltaický
články	článek	k1gInPc4	článek
vyrábějící	vyrábějící	k2eAgInSc4d1	vyrábějící
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Elektřinu	elektřina	k1gFnSc4	elektřina
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
také	také	k9	také
osm	osm	k4xCc1	osm
větrných	větrný	k2eAgFnPc2d1	větrná
turbín	turbína	k1gFnPc2	turbína
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
1,5	[number]	k4	1,5
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
energii	energie	k1gFnSc4	energie
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ukládat	ukládat	k5eAaImF	ukládat
do	do	k7c2	do
NiCd	NiCda	k1gFnPc2	NiCda
baterií	baterie	k1gFnPc2	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
elektřiny	elektřina	k1gFnSc2	elektřina
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dieselové	dieselový	k2eAgInPc4d1	dieselový
generátory	generátor	k1gInPc4	generátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
počasí	počasí	k1gNnSc6	počasí
se	se	k3xPyFc4	se
obnovitelné	obnovitelný	k2eAgInPc1d1	obnovitelný
zdroje	zdroj	k1gInPc1	zdroj
energie	energie	k1gFnSc2	energie
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
zásobování	zásobování	k1gNnSc6	zásobování
stanice	stanice	k1gFnSc2	stanice
elektřinou	elektřina	k1gFnSc7	elektřina
mezi	mezi	k7c7	mezi
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
používá	používat	k5eAaImIp3nS	používat
argentinské	argentinský	k2eAgNnSc4d1	argentinské
časové	časový	k2eAgNnSc4d1	časové
pásmo	pásmo	k1gNnSc4	pásmo
UTC-	UTC-	k1gFnSc2	UTC-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
z	z	k7c2	z
nedalekého	daleký	k2eNgInSc2d1	nedaleký
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
až	až	k8xS	až
březnu	březno	k1gNnSc6	březno
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zásoba	zásoba	k1gFnSc1	zásoba
vody	voda	k1gFnSc2	voda
ukládána	ukládat	k5eAaImNgFnS	ukládat
ještě	ještě	k6eAd1	ještě
do	do	k7c2	do
speciálních	speciální	k2eAgInPc2d1	speciální
kontejnerů	kontejner	k1gInPc2	kontejner
uvnitř	uvnitř	k7c2	uvnitř
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
se	s	k7c7	s
světem	svět	k1gInSc7	svět
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
díky	díky	k7c3	díky
satelitní	satelitní	k2eAgFnSc3d1	satelitní
technologii	technologie	k1gFnSc3	technologie
Bender	Bendra	k1gFnPc2	Bendra
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
Inmarsat	Inmarsat	k1gFnSc2	Inmarsat
<g/>
,	,	kIx,	,
vyvinuté	vyvinutý	k2eAgFnSc2d1	vyvinutá
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Fakultou	fakulta	k1gFnSc7	fakulta
elektrotechnickou	elektrotechnický	k2eAgFnSc7d1	elektrotechnická
ČVUT	ČVUT	kA	ČVUT
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
výměnu	výměna	k1gFnSc4	výměna
dat	datum	k1gNnPc2	datum
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
492	[number]	k4	492
kbit	kbita	k1gFnPc2	kbita
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Součástí	součást	k1gFnSc7	součást
vybavení	vybavení	k1gNnSc2	vybavení
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
spouštěcí	spouštěcí	k2eAgFnSc1d1	spouštěcí
rampa	rampa	k1gFnSc1	rampa
a	a	k8xC	a
odvod	odvod	k1gInSc1	odvod
upravené	upravený	k2eAgFnSc2d1	upravená
splaškové	splaškový	k2eAgFnSc2d1	splašková
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
projektování	projektování	k1gNnSc6	projektování
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
všech	všecek	k3xTgInPc2	všecek
jejích	její	k3xOp3gInPc2	její
systémů	systém	k1gInPc2	systém
byly	být	k5eAaImAgInP	být
plně	plně	k6eAd1	plně
respektovány	respektovat	k5eAaImNgInP	respektovat
veškeré	veškerý	k3xTgInPc1	veškerý
ekologické	ekologický	k2eAgInPc1d1	ekologický
požadavky	požadavek	k1gInPc1	požadavek
a	a	k8xC	a
předpisy	předpis	k1gInPc1	předpis
obsažené	obsažený	k2eAgInPc1d1	obsažený
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
Antarktické	antarktický	k2eAgFnSc2d1	antarktická
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
Protocol	Protocol	k1gInSc1	Protocol
of	of	k?	of
Environmental	Environmental	k1gMnSc1	Environmental
Protection	Protection	k1gInSc1	Protection
in	in	k?	in
Antarctica	Antarctic	k1gInSc2	Antarctic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInPc1d1	roční
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
jsou	být	k5eAaImIp3nP	být
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
z	z	k7c2	z
menší	malý	k2eAgFnSc1d2	menší
pak	pak	k6eAd1	pak
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgInPc1d1	případný
výpadky	výpadek	k1gInPc1	výpadek
<g/>
,	,	kIx,	,
či	či	k8xC	či
snížení	snížení	k1gNnSc1	snížení
financování	financování	k1gNnSc2	financování
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
odletělo	odletět	k5eAaPmAgNnS	odletět
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc1	šest
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
pokrývat	pokrývat	k5eAaImF	pokrývat
Český	český	k2eAgInSc1d1	český
antarktický	antarktický	k2eAgInSc1d1	antarktický
nadační	nadační	k2eAgInSc1d1	nadační
fond	fond	k1gInSc1	fond
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
založen	založit	k5eAaPmNgInS	založit
a	a	k8xC	a
jehož	jehož	k3xOyRp3gMnSc7	jehož
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
zakladatel	zakladatel	k1gMnSc1	zakladatel
české	český	k2eAgFnSc2d1	Česká
stanice	stanice	k1gFnSc2	stanice
Pavel	Pavel	k1gMnSc1	Pavel
Prošek	Prošek	k1gMnSc1	Prošek
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
vybudováním	vybudování	k1gNnSc7	vybudování
vědecky	vědecky	k6eAd1	vědecky
neprobádané	probádaný	k2eNgInPc1d1	neprobádaný
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
český	český	k2eAgInSc1d1	český
výzkum	výzkum	k1gInSc1	výzkum
Antarktidy	Antarktida	k1gFnSc2	Antarktida
soustředí	soustředit	k5eAaPmIp3nS	soustředit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geologického	geologický	k2eAgNnSc2d1	geologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
oblast	oblast	k1gFnSc1	oblast
křídových	křídový	k2eAgInPc2d1	křídový
sedimentů	sediment	k1gInPc2	sediment
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c6	na
zkameněliny	zkamenělina	k1gFnSc2	zkamenělina
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
zespoda	zespoda	k7c2	zespoda
místy	místo	k1gNnPc7	místo
pronikaly	pronikat	k5eAaImAgFnP	pronikat
vulkanické	vulkanický	k2eAgFnPc1d1	vulkanická
horniny	hornina	k1gFnPc1	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
aktivita	aktivita	k1gFnSc1	aktivita
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
pod	pod	k7c7	pod
ledovci	ledovec	k1gInPc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
ústupem	ústup	k1gInSc7	ústup
ledovců	ledovec	k1gInPc2	ledovec
a	a	k8xC	a
kolonizací	kolonizace	k1gFnPc2	kolonizace
obnaženého	obnažený	k2eAgInSc2d1	obnažený
povrchu	povrch	k1gInSc2	povrch
nižšími	nízký	k2eAgFnPc7d2	nižší
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
organismů	organismus	k1gInPc2	organismus
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
zcela	zcela	k6eAd1	zcela
nových	nový	k2eAgFnPc2d1	nová
a	a	k8xC	a
vysoké	vysoký	k2eAgNnSc1d1	vysoké
je	být	k5eAaImIp3nS	být
i	i	k9	i
zastoupení	zastoupení	k1gNnSc1	zastoupení
endemitů	endemit	k1gInPc2	endemit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sladkovodních	sladkovodní	k2eAgNnPc6d1	sladkovodní
jezerech	jezero	k1gNnPc6	jezero
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
a	a	k8xC	a
neprostudovaná	prostudovaný	k2eNgNnPc1d1	prostudovaný
společenství	společenství	k1gNnPc1	společenství
řas	řasa	k1gFnPc2	řasa
<g/>
,	,	kIx,	,
sinic	sinice	k1gFnPc2	sinice
a	a	k8xC	a
jednoduchých	jednoduchý	k2eAgMnPc2d1	jednoduchý
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumné	výzkumný	k2eAgInPc1d1	výzkumný
projekty	projekt	k1gInPc1	projekt
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
započaly	započnout	k5eAaPmAgFnP	započnout
již	již	k6eAd1	již
před	před	k7c7	před
stavbou	stavba	k1gFnSc7	stavba
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
geologický	geologický	k2eAgInSc4d1	geologický
výzkum	výzkum	k1gInSc4	výzkum
vedený	vedený	k2eAgInSc1d1	vedený
Českou	český	k2eAgFnSc7d1	Česká
geologickou	geologický	k2eAgFnSc7d1	geologická
službou	služba	k1gFnSc7	služba
a	a	k8xC	a
klimatologicko-geomorfologický	klimatologickoeomorfologický	k2eAgInSc1d1	klimatologicko-geomorfologický
výzkum	výzkum	k1gInSc1	výzkum
vedený	vedený	k2eAgInSc1d1	vedený
Přírodovědeckou	přírodovědecký	k2eAgFnSc7d1	Přírodovědecká
fakultou	fakulta	k1gFnSc7	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dokončovala	dokončovat	k5eAaImAgFnS	dokončovat
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozběhl	rozběhnout	k5eAaPmAgInS	rozběhnout
botanický	botanický	k2eAgInSc1d1	botanický
průzkum	průzkum	k1gInSc1	průzkum
sinic	sinice	k1gFnPc2	sinice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Jihočeské	jihočeský	k2eAgFnSc2d1	Jihočeská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Botanického	botanický	k2eAgInSc2d1	botanický
ústavu	ústav	k1gInSc2	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
MU	MU	kA	MU
také	také	k9	také
připravila	připravit	k5eAaPmAgFnS	připravit
projekt	projekt	k1gInSc4	projekt
výzkumu	výzkum	k1gInSc2	výzkum
UV	UV	kA	UV
záření	záření	k1gNnSc4	záření
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc1	projekt
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
výzkum	výzkum	k1gInSc4	výzkum
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
a	a	k8xC	a
ozonové	ozonový	k2eAgFnPc1d1	ozonová
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Geologické	geologický	k2eAgNnSc1d1	geologické
mapování	mapování	k1gNnSc1	mapování
okolí	okolí	k1gNnSc2	okolí
stanice	stanice	k1gFnSc2	stanice
realizovala	realizovat	k5eAaBmAgFnS	realizovat
také	také	k9	také
Česká	český	k2eAgFnSc1d1	Česká
geologická	geologický	k2eAgFnSc1d1	geologická
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Probíhal	probíhat	k5eAaImAgMnS	probíhat
též	též	k9	též
základní	základní	k2eAgInSc4d1	základní
ornitologický	ornitologický	k2eAgInSc4d1	ornitologický
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
většina	většina	k1gFnSc1	většina
ptáků	pták	k1gMnPc2	pták
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
již	již	k6eAd1	již
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
ornitologové	ornitolog	k1gMnPc1	ornitolog
dostanou	dostat	k5eAaPmIp3nP	dostat
až	až	k9	až
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Ichtyologický	ichtyologický	k2eAgInSc1d1	ichtyologický
výzkum	výzkum	k1gInSc1	výzkum
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
rybám	ryba	k1gFnPc3	ryba
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
parazitům	parazit	k1gMnPc3	parazit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
výzkum	výzkum	k1gInSc1	výzkum
vnímání	vnímání	k1gNnSc2	vnímání
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
živočichy	živočich	k1gMnPc4	živočich
byl	být	k5eAaImAgMnS	být
prováděn	prováděn	k2eAgMnSc1d1	prováděn
na	na	k7c6	na
blešivcích	blešivec	k1gMnPc6	blešivec
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriologický	bakteriologický	k2eAgInSc1d1	bakteriologický
výzkum	výzkum	k1gInSc1	výzkum
popsal	popsat	k5eAaPmAgInS	popsat
nové	nový	k2eAgInPc4d1	nový
druhy	druh	k1gInPc4	druh
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
projektů	projekt	k1gInPc2	projekt
hodnotil	hodnotit	k5eAaImAgInS	hodnotit
stres	stres	k1gInSc1	stres
polárních	polární	k2eAgMnPc2d1	polární
výzkumníků	výzkumník	k1gMnPc2	výzkumník
během	během	k7c2	během
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
také	také	k9	také
míru	míra	k1gFnSc4	míra
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
těla	tělo	k1gNnSc2	tělo
arktickým	arktický	k2eAgFnPc3d1	arktická
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
jiný	jiný	k1gMnSc1	jiný
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
stárnutí	stárnutí	k1gNnSc4	stárnutí
plastů	plast	k1gInPc2	plast
v	v	k7c6	v
extrémních	extrémní	k2eAgFnPc6d1	extrémní
podmínkách	podmínka	k1gFnPc6	podmínka
antarktického	antarktický	k2eAgNnSc2d1	antarktické
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výzkumném	výzkumný	k2eAgNnSc6d1	výzkumné
úsilí	úsilí	k1gNnSc6	úsilí
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
i	i	k9	i
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
instituce	instituce	k1gFnPc4	instituce
včetně	včetně	k7c2	včetně
Instituto	Institut	k2eAgNnSc1d1	Instituto
Antártico	Antártica	k1gFnSc5	Antártica
Argentino	Argentina	k1gFnSc5	Argentina
<g/>
,	,	kIx,	,
Instituto	Institut	k2eAgNnSc1d1	Instituto
Nacional	Nacional	k1gFnSc7	Nacional
Antártico	Antártico	k6eAd1	Antártico
Chileno	Chilen	k2eAgNnSc1d1	Chileno
<g/>
,	,	kIx,	,
British	British	k1gInSc1	British
Antarctic	Antarctice	k1gFnPc2	Antarctice
Survey	Survea	k1gFnSc2	Survea
<g/>
,	,	kIx,	,
Servicio	Servicio	k6eAd1	Servicio
Meteorológico	Meteorológico	k6eAd1	Meteorológico
Nacional	Nacional	k1gFnSc1	Nacional
Argentina	Argentina	k1gFnSc1	Argentina
a	a	k8xC	a
Nederlands	Nederlands	k1gInSc1	Nederlands
Instituut	Instituut	k2eAgInSc4d1	Instituut
voor	voor	k1gInSc4	voor
Ecologie	Ecologie	k1gFnSc2	Ecologie
<g/>
.	.	kIx.	.
</s>
<s>
Volná	volný	k2eAgNnPc1d1	volné
místa	místo	k1gNnPc1	místo
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
jsou	být	k5eAaImIp3nP	být
nabízena	nabízen	k2eAgNnPc1d1	nabízeno
vědcům	vědec	k1gMnPc3	vědec
a	a	k8xC	a
studentům	student	k1gMnPc3	student
českých	český	k2eAgFnPc2d1	Česká
i	i	k8xC	i
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
pracovišť	pracoviště	k1gNnPc2	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Nelsonově	Nelsonův	k2eAgInSc6d1	Nelsonův
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
Jižní	jižní	k2eAgFnSc2d1	jižní
Shetlandy	Shetlanda	k1gFnSc2	Shetlanda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
polární	polární	k2eAgFnSc2d1	polární
stanice	stanice	k1gFnSc2	stanice
Eco	Eco	k1gMnSc1	Eco
Nelson	Nelson	k1gMnSc1	Nelson
založená	založený	k2eAgNnPc1d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
polárníkem	polárník	k1gMnSc7	polárník
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Pavlíčkem	Pavlíček	k1gMnSc7	Pavlíček
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stanice	stanice	k1gFnSc1	stanice
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
charakter	charakter	k1gInSc4	charakter
a	a	k8xC	a
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
českou	český	k2eAgFnSc4d1	Česká
stanici	stanice	k1gFnSc4	stanice
<g/>
.	.	kIx.	.
</s>
