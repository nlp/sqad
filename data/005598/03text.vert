<p>
<s>
Minonoska	minonoska	k1gFnSc1	minonoska
je	být	k5eAaImIp3nS	být
válečná	válečný	k2eAgFnSc1d1	válečná
loď	loď	k1gFnSc1	loď
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
na	na	k7c6	na
pokládání	pokládání	k1gNnSc6	pokládání
námořních	námořní	k2eAgFnPc2d1	námořní
min	min	k1gFnPc2	min
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnPc1	charakteristikon
minonosek	minonoska	k1gFnPc2	minonoska
==	==	k?	==
</s>
</p>
<p>
<s>
Minonosky	minonoska	k1gFnPc1	minonoska
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
navzájem	navzájem	k6eAd1	navzájem
značně	značně	k6eAd1	značně
lišit	lišit	k5eAaImF	lišit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
velikosti	velikost	k1gFnSc6	velikost
a	a	k8xC	a
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
plavidla	plavidlo	k1gNnPc1	plavidlo
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
výtlak	výtlak	k1gInSc4	výtlak
od	od	k7c2	od
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
tun	tuna	k1gFnPc2	tuna
až	až	k9	až
po	po	k7c4	po
několik	několik	k4yIc4	několik
tisíců	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Specifickou	specifický	k2eAgFnSc7d1	specifická
výbavou	výbava	k1gFnSc7	výbava
minonosek	minonoska	k1gFnPc2	minonoska
je	být	k5eAaImIp3nS	být
nákladní	nákladní	k2eAgInSc4d1	nákladní
prostor	prostor	k1gInSc4	prostor
k	k	k7c3	k
uložení	uložení	k1gNnSc3	uložení
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
min	mina	k1gFnPc2	mina
a	a	k8xC	a
speciální	speciální	k2eAgFnSc2d1	speciální
kolejnice	kolejnice	k1gFnSc2	kolejnice
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yIgInPc2	který
jsou	být	k5eAaImIp3nP	být
miny	mina	k1gFnPc1	mina
dopravovány	dopravovat	k5eAaImNgFnP	dopravovat
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Kolejnice	kolejnice	k1gFnPc1	kolejnice
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
otvory	otvor	k1gInPc7	otvor
v	v	k7c6	v
zádi	záď	k1gFnSc6	záď
plavidla	plavidlo	k1gNnSc2	plavidlo
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
přes	přes	k7c4	přes
palubu	paluba	k1gFnSc4	paluba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
plavidlo	plavidlo	k1gNnSc4	plavidlo
i	i	k8xC	i
osádku	osádka	k1gFnSc4	osádka
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
pokládání	pokládání	k1gNnSc1	pokládání
kabelem	kabel	k1gInSc7	kabel
dálkově	dálkově	k6eAd1	dálkově
řízených	řízený	k2eAgFnPc2d1	řízená
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
aktivovat	aktivovat	k5eAaBmF	aktivovat
či	či	k8xC	či
deaktivovat	deaktivovat	k5eAaImF	deaktivovat
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Vedle	vedle	k7c2	vedle
min	mina	k1gFnPc2	mina
samotných	samotný	k2eAgMnPc2d1	samotný
musí	muset	k5eAaImIp3nS	muset
loď	loď	k1gFnSc4	loď
nést	nést	k5eAaImF	nést
i	i	k9	i
kabeláž	kabeláž	k1gFnSc1	kabeláž
a	a	k8xC	a
nutná	nutný	k2eAgFnSc1d1	nutná
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
specialistů	specialista	k1gMnPc2	specialista
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
minonosek	minonoska	k1gFnPc2	minonoska
a	a	k8xC	a
alternativní	alternativní	k2eAgInPc4d1	alternativní
způsoby	způsob	k1gInPc4	způsob
pokládání	pokládání	k1gNnSc2	pokládání
námořních	námořní	k2eAgFnPc2d1	námořní
min	mina	k1gFnPc2	mina
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
příslušném	příslušný	k2eAgNnSc6d1	příslušné
uzpůsobení	uzpůsobení	k1gNnSc6	uzpůsobení
lze	lze	k6eAd1	lze
k	k	k7c3	k
účelu	účel	k1gInSc3	účel
pokládání	pokládání	k1gNnSc2	pokládání
min	mina	k1gFnPc2	mina
využít	využít	k5eAaPmF	využít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
každou	každý	k3xTgFnSc4	každý
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
válečnou	válečná	k1gFnSc4	válečná
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
obchodní	obchodní	k2eAgFnSc1d1	obchodní
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Speciální	speciální	k2eAgFnPc4d1	speciální
miny	mina	k1gFnPc4	mina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
lze	lze	k6eAd1	lze
vypouštět	vypouštět	k5eAaImF	vypouštět
pomocí	pomocí	k7c2	pomocí
torpédometů	torpédomet	k1gInPc2	torpédomet
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zastávat	zastávat	k5eAaImF	zastávat
tuto	tento	k3xDgFnSc4	tento
úlohu	úloha	k1gFnSc4	úloha
i	i	k9	i
ponorkám	ponorka	k1gFnPc3	ponorka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
námořních	námořní	k2eAgFnPc2d1	námořní
sil	síla	k1gFnPc2	síla
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
od	od	k7c2	od
použití	použití	k1gNnSc2	použití
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
minonosek	minonoska	k1gFnPc2	minonoska
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
a	a	k8xC	a
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
pokládání	pokládání	k1gNnSc1	pokládání
námořních	námořní	k2eAgFnPc2d1	námořní
min	mina	k1gFnPc2	mina
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1	některý
ze	z	k7c2	z
států	stát	k1gInPc2	stát
s	s	k7c7	s
dlouhými	dlouhý	k2eAgNnPc7d1	dlouhé
a	a	k8xC	a
mělkými	mělký	k2eAgNnPc7d1	mělké
pobřežími	pobřeží	k1gNnPc7	pobřeží
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
lze	lze	k6eAd1	lze
jen	jen	k6eAd1	jen
obtížně	obtížně	k6eAd1	obtížně
bránit	bránit	k5eAaImF	bránit
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
však	však	k9	však
minonosky	minonoska	k1gFnPc1	minonoska
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
ponechaly	ponechat	k5eAaPmAgFnP	ponechat
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
V	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
službě	služba	k1gFnSc6	služba
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
např.	např.	kA	např.
minonosky	minonoska	k1gFnPc4	minonoska
jihokorejské	jihokorejský	k2eAgFnPc4d1	jihokorejská
nebo	nebo	k8xC	nebo
finské	finský	k2eAgFnPc4d1	finská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Minenleger	Minenlegero	k1gNnPc2	Minenlegero
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
Minelayer	Minelayer	k1gInSc1	Minelayer
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Minelæ	Minelæ	k1gInSc4	Minelæ
na	na	k7c6	na
dánské	dánský	k2eAgFnSc6d1	dánská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Minolovka	minolovka	k1gFnSc1	minolovka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
minonoska	minonoska	k1gFnSc1	minonoska
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
