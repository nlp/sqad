<p>
<s>
Miluj	milovat	k5eAaImRp2nS	milovat
bližního	bližní	k1gMnSc2	bližní
svého	své	k1gNnSc2	své
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
německých	německý	k2eAgMnPc2d1	německý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Ericha	Erich	k1gMnSc4	Erich
Marii	Maria	k1gFnSc4	Maria
Remarquea	Remarquea	k1gFnSc1	Remarquea
<g/>
.	.	kIx.	.
</s>
<s>
Knižně	knižně	k6eAd1	knižně
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydán	vydán	k2eAgInSc1d1	vydán
roku	rok	k1gInSc3	rok
1941	[number]	k4	1941
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
Remarqueovým	Remarqueův	k2eAgInPc3d1	Remarqueův
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
sedmým	sedmý	k4xOgInSc7	sedmý
románem	román	k1gInSc7	román
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
romány	román	k1gInPc7	román
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc4	klid
nebo	nebo	k8xC	nebo
Tři	tři	k4xCgMnPc1	tři
kamarádi	kamarád	k1gMnPc1	kamarád
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
nejčtenějších	čtený	k2eAgNnPc2d3	nejčtenější
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Román	Román	k1gMnSc1	Román
sleduje	sledovat	k5eAaImIp3nS	sledovat
životy	život	k1gInPc4	život
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
,	,	kIx,	,
uprchlíku	uprchlík	k1gMnSc3	uprchlík
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
vyhoštěných	vyhoštěný	k2eAgMnPc2d1	vyhoštěný
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
sílícího	sílící	k2eAgInSc2d1	sílící
nacionálního	nacionální	k2eAgInSc2d1	nacionální
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
samotnému	samotný	k2eAgMnSc3d1	samotný
autorovi	autor	k1gMnSc3	autor
důvěrně	důvěrně	k6eAd1	důvěrně
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
jejich	jejich	k3xOp3gFnSc4	jejich
cestu	cesta	k1gFnSc4	cesta
napříč	napříč	k7c7	napříč
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
marnou	marný	k2eAgFnSc4d1	marná
snahu	snaha	k1gFnSc4	snaha
získat	získat	k5eAaPmF	získat
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
dlouhodobému	dlouhodobý	k2eAgInSc3d1	dlouhodobý
pobytu	pobyt	k1gInSc3	pobyt
a	a	k8xC	a
prchání	prchání	k1gNnSc4	prchání
před	před	k7c7	před
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Román	Román	k1gMnSc1	Román
popisuje	popisovat	k5eAaImIp3nS	popisovat
osudy	osud	k1gInPc4	osud
tří	tři	k4xCgInPc2	tři
německých	německý	k2eAgMnPc2d1	německý
utečenců	utečenec	k1gMnPc2	utečenec
<g/>
,	,	kIx,	,
jednadvacetiletého	jednadvacetiletý	k2eAgMnSc4d1	jednadvacetiletý
studenta	student	k1gMnSc4	student
medicíny	medicína	k1gFnSc2	medicína
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Kerna	Kern	k1gMnSc4	Kern
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
krámek	krámek	k1gInSc4	krámek
s	s	k7c7	s
drogerií	drogerie	k1gFnSc7	drogerie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
sám	sám	k3xTgMnSc1	sám
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Falešně	falešně	k6eAd1	falešně
obviněnému	obviněný	k1gMnSc3	obviněný
otci	otec	k1gMnSc3	otec
je	být	k5eAaImIp3nS	být
krámek	krámek	k1gInSc4	krámek
zabaven	zabaven	k2eAgInSc4d1	zabaven
a	a	k8xC	a
odcházejí	odcházet	k5eAaImIp3nP	odcházet
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
k	k	k7c3	k
matčiným	matčin	k2eAgMnPc3d1	matčin
příbuzným	příbuzný	k1gMnPc3	příbuzný
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Ludvíkův	Ludvíkův	k2eAgMnSc1d1	Ludvíkův
otec	otec	k1gMnSc1	otec
je	být	k5eAaImIp3nS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
dostává	dostávat	k5eAaImIp3nS	dostávat
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
maďarskénu	maďarskén	k1gInSc3	maďarskén
původu	původ	k1gInSc2	původ
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
hrdiny	hrdina	k1gMnPc7	hrdina
knihy	kniha	k1gFnSc2	kniha
jsou	být	k5eAaImIp3nP	být
válečný	válečný	k2eAgMnSc1d1	válečný
veterán	veterán	k1gMnSc1	veterán
Josef	Josef	k1gMnSc1	Josef
Steiner	Steiner	k1gMnSc1	Steiner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
režimem	režim	k1gInSc7	režim
a	a	k8xC	a
po	po	k7c6	po
útěku	útěk	k1gInSc6	útěk
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
je	být	k5eAaImIp3nS	být
nucen	nutit	k5eAaImNgMnS	nutit
opustit	opustit	k5eAaPmF	opustit
nemocnou	nemocný	k2eAgFnSc4d1	nemocná
manželku	manželka	k1gFnSc4	manželka
a	a	k8xC	a
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
mladá	mladý	k2eAgFnSc1d1	mladá
dívka	dívka	k1gFnSc1	dívka
Ruth	Ruth	k1gFnSc1	Ruth
Hollandová	Hollandový	k2eAgFnSc1d1	Hollandová
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
knihy	kniha	k1gFnSc2	kniha
začíná	začínat	k5eAaImIp3nS	začínat
během	během	k7c2	během
policejní	policejní	k2eAgFnSc2d1	policejní
razie	razie	k1gFnSc2	razie
v	v	k7c6	v
emigranském	emigranský	k2eAgInSc6d1	emigranský
hotelu	hotel	k1gInSc6	hotel
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Kern	Kern	k1gMnSc1	Kern
zatčen	zatčen	k2eAgMnSc1d1	zatčen
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
Josefem	Josef	k1gMnSc7	Josef
Steinerem	Steiner	k1gMnSc7	Steiner
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
zadrženi	zadržet	k5eAaPmNgMnP	zadržet
a	a	k8xC	a
převezeni	převézt	k5eAaPmNgMnP	převézt
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
se	se	k3xPyFc4	se
se	s	k7c7	s
zkušeným	zkušený	k2eAgMnSc7d1	zkušený
Steinerem	Steiner	k1gMnSc7	Steiner
postupně	postupně	k6eAd1	postupně
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
<g/>
,	,	kIx,	,
Steiner	Steiner	k1gMnSc1	Steiner
naučí	naučit	k5eAaPmIp3nS	naučit
mladičkého	mladičký	k2eAgMnSc4d1	mladičký
Ludvíka	Ludvík	k1gMnSc4	Ludvík
hrát	hrát	k5eAaImF	hrát
karty	karta	k1gFnPc1	karta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
dát	dát	k5eAaPmF	dát
na	na	k7c4	na
Steinerovu	Steinerův	k2eAgFnSc4d1	Steinerova
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
vyhostit	vyhostit	k5eAaPmF	vyhostit
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
dostane	dostat	k5eAaPmIp3nS	dostat
desetidenní	desetidenní	k2eAgNnSc4d1	desetidenní
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
organizace	organizace	k1gFnSc2	organizace
Pomoc	pomoc	k1gFnSc1	pomoc
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
získává	získávat	k5eAaImIp3nS	získávat
nocleh	nocleh	k1gInSc1	nocleh
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Bristol	Bristol	k1gInSc1	Bristol
a	a	k8xC	a
lístky	lístek	k1gInPc1	lístek
do	do	k7c2	do
menzy	menza	k1gFnSc2	menza
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
potkává	potkávat	k5eAaImIp3nS	potkávat
židovskou	židovský	k2eAgFnSc4d1	židovská
dívku	dívka	k1gFnSc4	dívka
Ruth	Ruth	k1gFnSc2	Ruth
Hollandovou	Hollandový	k2eAgFnSc4d1	Hollandová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
padnou	padnout	k5eAaImIp3nP	padnout
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
pražských	pražský	k2eAgFnPc2d1	Pražská
drogerií	drogerie	k1gFnPc2	drogerie
nedaleko	nedaleko	k7c2	nedaleko
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
Ludvík	Ludvík	k1gMnSc1	Ludvík
objevuje	objevovat	k5eAaImIp3nS	objevovat
parfémy	parfém	k1gInPc4	parfém
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
jeho	jeho	k3xOp3gMnPc7	jeho
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
prodavače	prodavač	k1gMnSc2	prodavač
přichází	přicházet	k5eAaImIp3nS	přicházet
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
na	na	k7c6	na
stopu	stop	k1gInSc6	stop
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
ho	on	k3xPp3gMnSc4	on
otec	otec	k1gMnSc1	otec
navštíví	navštívit	k5eAaPmIp3nS	navštívit
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
hotelu	hotel	k1gInSc6	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
spatří	spatřit	k5eAaPmIp3nS	spatřit
otce	otec	k1gMnSc4	otec
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
špatném	špatný	k2eAgInSc6d1	špatný
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
Při	při	k7c6	při
loučení	loučení	k1gNnSc6	loučení
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
naposled	naposled	k6eAd1	naposled
<g/>
,	,	kIx,	,
co	co	k9	co
otce	otec	k1gMnSc4	otec
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
dnů	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
Ruth	Ruth	k1gFnSc1	Ruth
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
kamarádkou	kamarádka	k1gFnSc7	kamarádka
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
dostane	dostat	k5eAaPmIp3nS	dostat
ještě	ještě	k6eAd1	ještě
pětidenní	pětidenní	k2eAgInSc1d1	pětidenní
prodloužení	prodloužení	k1gNnSc4	prodloužení
pobytu	pobyt	k1gInSc2	pobyt
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
také	také	k9	také
vyhoštěn	vyhoštěn	k2eAgMnSc1d1	vyhoštěn
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
volnou	volný	k2eAgFnSc4d1	volná
jízdenku	jízdenka	k1gFnSc4	jízdenka
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
Steiner	Steiner	k1gMnSc1	Steiner
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
zařídí	zařídit	k5eAaPmIp3nS	zařídit
práci	práce	k1gFnSc4	práce
u	u	k7c2	u
cirkusu	cirkus	k1gInSc2	cirkus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sám	sám	k3xTgMnSc1	sám
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
opět	opět	k6eAd1	opět
potkává	potkávat	k5eAaImIp3nS	potkávat
Ruth	Ruth	k1gFnSc1	Ruth
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	se	k3xPyFc2	se
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
strávené	strávený	k2eAgFnSc6d1	strávená
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
zatčen	zatknout	k5eAaPmNgMnS	zatknout
na	na	k7c6	na
protižidovské	protižidovský	k2eAgFnSc6d1	protižidovská
demonstraci	demonstrace	k1gFnSc6	demonstrace
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
Ruth	Ruth	k1gFnSc2	Ruth
nuceni	nucen	k2eAgMnPc1d1	nucen
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
ale	ale	k8xC	ale
Ruth	Ruth	k1gFnSc6	Ruth
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
zůstat	zůstat	k5eAaPmF	zůstat
u	u	k7c2	u
sedláka	sedlák	k1gMnSc2	sedlák
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jim	on	k3xPp3gMnPc3	on
umožní	umožnit	k5eAaPmIp3nS	umožnit
přespávat	přespávat	k5eAaImF	přespávat
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podomním	podomní	k2eAgInSc6d1	podomní
prodeji	prodej	k1gInSc6	prodej
narazí	narazit	k5eAaPmIp3nS	narazit
Ludvík	Ludvík	k1gMnSc1	Ludvík
náhodou	náhoda	k1gFnSc7	náhoda
na	na	k7c4	na
německého	německý	k2eAgMnSc4d1	německý
konfidenta	konfident	k1gMnSc4	konfident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gNnSc4	on
hned	hned	k6eAd1	hned
udává	udávat	k5eAaImIp3nS	udávat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
policista	policista	k1gMnSc1	policista
umožní	umožnit	k5eAaPmIp3nS	umožnit
Ludvíkovi	Ludvík	k1gMnSc3	Ludvík
během	během	k7c2	během
eskorty	eskorta	k1gFnSc2	eskorta
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Ruth	Ruth	k1gFnSc1	Ruth
dostává	dostávat	k5eAaImIp3nS	dostávat
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
večer	večer	k6eAd1	večer
Ludvík	Ludvík	k1gMnSc1	Ludvík
přechází	přecházet	k5eAaImIp3nS	přecházet
před	před	k7c4	před
nemocnici	nemocnice	k1gFnSc4	nemocnice
a	a	k8xC	a
mává	mávat	k5eAaImIp3nS	mávat
do	do	k7c2	do
Ruthina	Ruthin	k2eAgNnSc2d1	Ruthin
okna	okno	k1gNnSc2	okno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
chycen	chytit	k5eAaPmNgMnS	chytit
<g/>
,	,	kIx,	,
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
vyhoštěn	vyhostit	k5eAaPmNgInS	vyhostit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
ubytují	ubytovat	k5eAaPmIp3nP	ubytovat
v	v	k7c6	v
normálním	normální	k2eAgInSc6d1	normální
hotelu	hotel	k1gInSc6	hotel
a	a	k8xC	a
zažádají	zažádat	k5eAaPmIp3nP	zažádat
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Ruth	Ruth	k1gFnSc1	Ruth
jej	on	k3xPp3gMnSc4	on
dostává	dostávat	k5eAaImIp3nS	dostávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ludvík	Ludvík	k1gMnSc1	Ludvík
ne	ne	k9	ne
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mu	on	k3xPp3gMnSc3	on
nezbývá	zbývat	k5eNaImIp3nS	zbývat
<g/>
,	,	kIx,	,
než	než	k8xS	než
žít	žít	k5eAaImF	žít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
nelegálně	legálně	k6eNd1	legálně
<g/>
.	.	kIx.	.
</s>
<s>
Najdou	najít	k5eAaPmIp3nP	najít
si	se	k3xPyFc3	se
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ludvík	Ludvík	k1gMnSc1	Ludvík
dělá	dělat	k5eAaImIp3nS	dělat
nelegálně	legálně	k6eNd1	legálně
a	a	k8xC	a
Steiner	Steiner	k1gMnSc1	Steiner
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
padělanému	padělaný	k2eAgInSc3d1	padělaný
pasu	pas	k1gInSc3	pas
<g/>
,	,	kIx,	,
legálně	legálně	k6eAd1	legálně
<g/>
.	.	kIx.	.
</s>
<s>
Steiner	Steiner	k1gMnSc1	Steiner
se	se	k3xPyFc4	se
však	však	k9	však
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
naléhání	naléhání	k1gNnSc4	naléhání
Ludvíka	Ludvík	k1gMnSc2	Ludvík
s	s	k7c7	s
Ruth	Ruth	k1gFnSc7	Ruth
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
minulosti	minulost	k1gFnSc3	minulost
hrozí	hrozit	k5eAaImIp3nS	hrozit
vězení	vězení	k1gNnSc1	vězení
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
návštěvě	návštěva	k1gFnSc6	návštěva
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
ho	on	k3xPp3gNnSc4	on
na	na	k7c4	na
udání	udání	k1gNnSc4	udání
sestry	sestra	k1gFnSc2	sestra
zatkne	zatknout	k5eAaPmIp3nS	zatknout
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Steiner	Steiner	k1gMnSc1	Steiner
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
navštívit	navštívit	k5eAaPmF	navštívit
umírající	umírající	k2eAgFnSc4d1	umírající
maželku	maželka	k1gFnSc4	maželka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
vrhne	vrhnout	k5eAaImIp3nS	vrhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
policistů	policista	k1gMnPc2	policista
a	a	k8xC	a
skočí	skočit	k5eAaPmIp3nS	skočit
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc4	dva
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
situace	situace	k1gFnSc1	situace
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
Paříž	Paříž	k1gFnSc1	Paříž
začne	začít	k5eAaPmIp3nS	začít
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
kontrolována	kontrolován	k2eAgFnSc1d1	kontrolována
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
vyhoštěn	vyhostit	k5eAaPmNgMnS	vyhostit
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
ale	ale	k9	ale
stačí	stačit	k5eAaBmIp3nS	stačit
vrátit	vrátit	k5eAaPmF	vrátit
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
přítel	přítel	k1gMnSc1	přítel
Marill	Marill	k1gMnSc1	Marill
jim	on	k3xPp3gMnPc3	on
za	za	k7c4	za
část	část	k1gFnSc4	část
peněz	peníze	k1gInPc2	peníze
zanechaných	zanechaný	k2eAgInPc2d1	zanechaný
Steinerem	Steiner	k1gMnSc7	Steiner
zajistí	zajistit	k5eAaPmIp3nP	zajistit
lístky	lístek	k1gInPc4	lístek
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
právě	právě	k6eAd1	právě
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
utečence	utečenec	k1gMnPc4	utečenec
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
knihy	kniha	k1gFnSc2	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
Remarque	Remarque	k1gInSc1	Remarque
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
románu	román	k1gInSc6	román
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1938	[number]	k4	1938
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
exilu	exil	k1gInSc6	exil
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Juttou	Jutta	k1gFnSc7	Jutta
Ilse	Ilse	k1gFnSc7	Ilse
Zambonou	Zambona	k1gFnSc7	Zambona
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1939	[number]	k4	1939
dokončil	dokončit	k5eAaPmAgMnS	dokončit
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
Miluj	milovat	k5eAaImRp2nS	milovat
bližního	bližní	k1gMnSc2	bližní
svého	svůj	k3xOyFgInSc2	svůj
začal	začít	k5eAaPmAgInS	začít
vycházet	vycházet	k5eAaImF	vycházet
nejdříve	dříve	k6eAd3	dříve
časopisecky	časopisecky	k6eAd1	časopisecky
pod	pod	k7c7	pod
anglickým	anglický	k2eAgInSc7d1	anglický
titulem	titul	k1gInSc7	titul
Flotsam	Flotsam	k1gInSc1	Flotsam
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
září	září	k1gNnSc2	září
1939	[number]	k4	1939
jako	jako	k8xC	jako
příloha	příloha	k1gFnSc1	příloha
amerického	americký	k2eAgInSc2d1	americký
týdeníku	týdeník	k1gInSc2	týdeník
Collier	Collira	k1gFnPc2	Collira
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Weekly	Weekl	k1gInPc7	Weekl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
revidovaného	revidovaný	k2eAgInSc2d1	revidovaný
textu	text	k1gInSc2	text
následovalo	následovat	k5eAaImAgNnS	následovat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1941	[number]	k4	1941
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Little	Little	k1gFnSc2	Little
<g/>
,	,	kIx,	,
Brown	Brown	k1gMnSc1	Brown
and	and	k?	and
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
německá	německý	k2eAgFnSc1d1	německá
verze	verze	k1gFnSc1	verze
románu	román	k1gInSc2	román
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1941	[number]	k4	1941
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Liebe	Lieb	k1gInSc5	Lieb
deinen	deinen	k1gInSc1	deinen
Nächsten	Nächsten	k2eAgMnSc1d1	Nächsten
v	v	k7c6	v
exilovém	exilový	k2eAgNnSc6d1	exilové
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Bermann	Bermann	k1gMnSc1	Bermann
Fischer	Fischer	k1gMnSc1	Fischer
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
románu	román	k1gInSc2	román
natočen	natočit	k5eAaBmNgInS	natočit
americký	americký	k2eAgInSc1d1	americký
film	film	k1gInSc1	film
So	So	kA	So
Ends	Ends	k1gInSc4	Ends
Our	Our	k1gFnPc2	Our
Night	Nighta	k1gFnPc2	Nighta
režiséra	režisér	k1gMnSc2	režisér
Johna	John	k1gMnSc2	John
Cromwella	Cromwell	k1gMnSc2	Cromwell
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byl	být	k5eAaImAgInS	být
román	román	k1gInSc1	román
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česky	česky	k6eAd1	česky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
v	v	k7c6	v
osmi	osm	k4xCc6	osm
vydáních	vydání	k1gNnPc6	vydání
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
ve	v	k7c6	v
Svobodném	svobodný	k2eAgNnSc6d1	svobodné
slovu	slovo	k1gNnSc6	slovo
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Ikar	Ikara	k1gFnPc2	Ikara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Erich	Erich	k1gMnSc1	Erich
Maria	Mario	k1gMnSc2	Mario
Remarque	Remarqu	k1gFnSc2	Remarqu
<g/>
,	,	kIx,	,
Miluj	milovat	k5eAaImRp2nS	milovat
bližního	bližní	k1gMnSc2	bližní
svého	své	k1gNnSc2	své
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Houba	houba	k1gFnSc1	houba
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Liebe	Lieb	k1gInSc5	Lieb
deinen	deinen	k1gInSc1	deinen
Nächsten	Nächsten	k2eAgMnSc1d1	Nächsten
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
So	So	kA	So
Ends	Endsa	k1gFnPc2	Endsa
Our	Our	k1gMnSc1	Our
Night	Night	k1gMnSc1	Night
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Miluj	milovat	k5eAaImRp2nS	milovat
bližního	bližní	k1gMnSc2	bližní
svého	svůj	k3xOyFgMnSc2	svůj
</s>
</p>
