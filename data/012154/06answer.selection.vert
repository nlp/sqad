<s>
Důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c6	o
kulatosti	kulatost	k1gFnSc6	kulatost
Země	zem	k1gFnSc2	zem
přinesla	přinést	k5eAaPmAgFnS	přinést
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
Magalhã	Magalhã	k1gMnSc1	Magalhã
vedl	vést	k5eAaImAgMnS	vést
a	a	k8xC	a
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
V.	V.	kA	V.
poprvé	poprvé	k6eAd1	poprvé
obeplula	obeplout	k5eAaPmAgFnS	obeplout
celou	celý	k2eAgFnSc4d1	celá
zeměkouli	zeměkoule	k1gFnSc4	zeměkoule
<g/>
.	.	kIx.	.
</s>
