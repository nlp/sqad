<s>
Sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
(	(	kIx(	(
<g/>
Cetartiodactyla	Cetartiodactyla	k1gMnPc1	Cetartiodactyla
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Artiodactyla	Artiodactyla	k1gFnSc1	Artiodactyla
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
řádem	řád	k1gInSc7	řád
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
vývojové	vývojový	k2eAgFnSc2d1	vývojová
linie	linie	k1gFnSc2	linie
úprava	úprava	k1gFnSc1	úprava
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
funkčního	funkční	k2eAgNnSc2d1	funkční
uplatnění	uplatnění	k1gNnSc2	uplatnění
dvou	dva	k4xCgInPc2	dva
prstů	prst	k1gInPc2	prst
–	–	k?	–
třetího	třetí	k4xOgMnSc2	třetí
a	a	k8xC	a
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
prochází	procházet	k5eAaImIp3nS	procházet
osa	osa	k1gFnSc1	osa
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prsty	prst	k1gInPc1	prst
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
rohovitá	rohovitý	k2eAgNnPc1d1	rohovité
kopyta	kopyto	k1gNnPc1	kopyto
a	a	k8xC	a
nesou	nést	k5eAaImIp3nP	nést
celou	celý	k2eAgFnSc4d1	celá
hmotnost	hmotnost	k1gFnSc4	hmotnost
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hrochů	hroch	k1gMnPc2	hroch
a	a	k8xC	a
velbloudovitých	velbloudovitý	k2eAgInPc2d1	velbloudovitý
jsou	být	k5eAaImIp3nP	být
nehtovitého	htovitý	k2eNgInSc2d1	htovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
jsou	být	k5eAaImIp3nP	být
prstochodci	prstochodec	k1gMnPc1	prstochodec
<g/>
,	,	kIx,	,
jen	jen	k9	jen
velbloudi	velbloud	k1gMnPc1	velbloud
a	a	k8xC	a
lamy	lama	k1gFnPc1	lama
našlapují	našlapovat	k5eAaImIp3nP	našlapovat
na	na	k7c4	na
poslední	poslední	k2eAgInPc4d1	poslední
tři	tři	k4xCgInPc4	tři
prstní	prstní	k2eAgInPc4d1	prstní
články	článek	k1gInPc4	článek
kryté	krytý	k2eAgInPc4d1	krytý
silným	silný	k2eAgInSc7d1	silný
<g/>
,	,	kIx,	,
pružným	pružný	k2eAgInSc7d1	pružný
mozolem	mozol	k1gInSc7	mozol
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
jméno	jméno	k1gNnSc1	jméno
Artiodactyla	Artiodactyla	k1gFnSc2	Artiodactyla
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckých	řecký	k2eAgNnPc2d1	řecké
slov	slovo	k1gNnPc2	slovo
artios	artiosa	k1gFnPc2	artiosa
<g/>
,	,	kIx,	,
sudý	sudý	k2eAgInSc1d1	sudý
<g/>
,	,	kIx,	,
a	a	k8xC	a
daktylos	daktylos	k1gInSc1	daktylos
<g/>
,	,	kIx,	,
prst	prst	k1gInSc1	prst
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
10	[number]	k4	10
čeledí	čeleď	k1gFnPc2	čeleď
a	a	k8xC	a
asi	asi	k9	asi
300	[number]	k4	300
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
i	i	k9	i
s	s	k7c7	s
kytovci	kytovec	k1gMnPc7	kytovec
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČR	ČR	kA	ČR
3	[number]	k4	3
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
10	[number]	k4	10
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
řádem	řád	k1gInSc7	řád
velkých	velký	k2eAgMnPc2d1	velký
býložravců	býložravec	k1gMnPc2	býložravec
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
člověku	člověk	k1gMnSc6	člověk
<g/>
)	)	kIx)	)
a	a	k8xC	a
obývají	obývat	k5eAaImIp3nP	obývat
různé	různý	k2eAgInPc4d1	různý
ekosystémy	ekosystém	k1gInPc4	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
velká	velký	k2eAgFnSc1d1	velká
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
jsou	být	k5eAaImIp3nP	být
býložravci	býložravec	k1gMnPc1	býložravec
<g/>
,	,	kIx,	,
jen	jen	k9	jen
někteří	některý	k3yIgMnPc1	některý
jsou	být	k5eAaImIp3nP	být
všežraví	všežravý	k2eAgMnPc1d1	všežravý
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
větší	veliký	k2eAgMnPc1d2	veliký
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
:	:	kIx,	:
nejmenší	malý	k2eAgMnSc1d3	nejmenší
známý	známý	k2eAgMnSc1d1	známý
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
<g/>
,	,	kIx,	,
kančil	kančit	k5eAaImAgMnS	kančit
menší	malý	k2eAgMnSc1d2	menší
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
sotva	sotva	k6eAd1	sotva
jeden	jeden	k4xCgInSc4	jeden
kilogram	kilogram	k1gInSc4	kilogram
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hroch	hroch	k1gMnSc1	hroch
obojživelný	obojživelný	k2eAgMnSc1d1	obojživelný
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
až	až	k9	až
4,5	[number]	k4	4,5
tuny	tuna	k1gFnPc4	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
sudokopytníky	sudokopytník	k1gMnPc4	sudokopytník
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
žijící	žijící	k2eAgMnSc1d1	žijící
savec	savec	k1gMnSc1	savec
<g/>
,	,	kIx,	,
žirafa	žirafa	k1gFnSc1	žirafa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
moderní	moderní	k2eAgFnSc2d1	moderní
taxonomie	taxonomie	k1gFnSc2	taxonomie
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
plejtvák	plejtvák	k1gMnSc1	plejtvák
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
.	.	kIx.	.
</s>
<s>
Nejnápadnějším	nápadní	k2eAgInSc7d3	nápadní
společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
prst	prst	k1gInSc4	prst
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nesou	nést	k5eAaImIp3nP	nést
váhu	váha	k1gFnSc4	váha
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Osa	osa	k1gFnSc1	osa
končetin	končetina	k1gFnPc2	končetina
prochází	procházet	k5eAaImIp3nS	procházet
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
pátý	pátý	k4xOgInSc4	pátý
prst	prst	k1gInSc4	prst
bývají	bývat	k5eAaImIp3nP	bývat
vždy	vždy	k6eAd1	vždy
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
nedotýkají	dotýkat	k5eNaImIp3nP	dotýkat
se	se	k3xPyFc4	se
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
nemají	mít	k5eNaImIp3nP	mít
kostní	kostní	k2eAgInSc4d1	kostní
podklad	podklad	k1gInSc4	podklad
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Palec	palec	k1gInSc1	palec
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
první	první	k4xOgInSc4	první
prst	prst	k1gInSc4	prst
<g/>
,	,	kIx,	,
chybí	chybit	k5eAaPmIp3nS	chybit
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
i	i	k8xC	i
pánevních	pánevní	k2eAgFnPc6d1	pánevní
končetinách	končetina	k1gFnPc6	končetina
sudý	sudý	k2eAgInSc1d1	sudý
počet	počet	k1gInSc1	počet
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
pekariovití	pekariovití	k1gMnPc1	pekariovití
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
končetinách	končetina	k1gFnPc6	končetina
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc4	tři
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Klíční	klíční	k2eAgFnSc1d1	klíční
kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
dalších	další	k2eAgMnPc2d1	další
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Obličejová	obličejový	k2eAgFnSc1d1	obličejová
část	část	k1gFnSc1	část
lebky	lebka	k1gFnSc2	lebka
bývá	bývat	k5eAaImIp3nS	bývat
protáhlá	protáhlý	k2eAgFnSc1d1	protáhlá
<g/>
,	,	kIx,	,
mozkovna	mozkovna	k1gFnSc1	mozkovna
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
užší	úzký	k2eAgNnSc1d2	užší
<g/>
,	,	kIx,	,
očnice	očnice	k1gFnPc1	očnice
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
oddělené	oddělený	k2eAgInPc1d1	oddělený
od	od	k7c2	od
spánkové	spánkový	k2eAgFnSc2d1	spánková
jámy	jáma	k1gFnSc2	jáma
<g/>
.	.	kIx.	.
</s>
<s>
Sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
mívají	mívat	k5eAaImIp3nP	mívat
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
44	[number]	k4	44
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
horní	horní	k2eAgInPc1d1	horní
řežáky	řežák	k1gInPc1	řežák
a	a	k8xC	a
špíčáky	špíčák	k1gInPc1	špíčák
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
často	často	k6eAd1	často
úplně	úplně	k6eAd1	úplně
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgInPc1d1	dolní
řezáky	řezák	k1gInPc1	řezák
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
dolní	dolní	k2eAgInPc1d1	dolní
špičáky	špičák	k1gInPc1	špičák
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
tvarově	tvarově	k6eAd1	tvarově
podobají	podobat	k5eAaImIp3nP	podobat
řezákům	řezák	k1gInPc3	řezák
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
přeměněny	přeměněn	k2eAgInPc4d1	přeměněn
v	v	k7c4	v
kly	kel	k1gInPc4	kel
<g/>
.	.	kIx.	.
</s>
<s>
Třenové	třenový	k2eAgInPc1d1	třenový
zuby	zub	k1gInPc1	zub
nejsou	být	k5eNaImIp3nP	být
molarizované	molarizovaný	k2eAgMnPc4d1	molarizovaný
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
vypadající	vypadající	k2eAgFnPc1d1	vypadající
jako	jako	k8xC	jako
stoličky	stolička	k1gFnPc1	stolička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
lichokopytníků	lichokopytník	k1gMnPc2	lichokopytník
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
špičákům	špičák	k1gMnPc3	špičák
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
rohy	roh	k1gInPc1	roh
nebo	nebo	k8xC	nebo
parohy	paroh	k1gInPc1	paroh
<g/>
.	.	kIx.	.
</s>
<s>
Žaludek	žaludek	k1gInSc1	žaludek
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
,	,	kIx,	,
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
vícekomorový	vícekomorový	k2eAgInSc1d1	vícekomorový
<g/>
.	.	kIx.	.
</s>
<s>
Předžaludky	předžaludek	k1gInPc1	předžaludek
jsou	být	k5eAaImIp3nP	být
osídlené	osídlený	k2eAgInPc1d1	osídlený
symbiotickými	symbiotický	k2eAgFnPc7d1	symbiotická
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
prvoky	prvok	k1gMnPc7	prvok
a	a	k8xC	a
houbami	houba	k1gFnPc7	houba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
fermentují	fermentovat	k5eAaBmIp3nP	fermentovat
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
štěpí	štěpit	k5eAaImIp3nS	štěpit
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
produkty	produkt	k1gInPc4	produkt
(	(	kIx(	(
<g/>
těkavé	těkavý	k2eAgNnSc4d1	těkavé
mastné	mastné	k1gNnSc4	mastné
kyseliny	kyselina	k1gFnSc2	kyselina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
strávenými	strávený	k2eAgNnPc7d1	strávené
těly	tělo	k1gNnPc7	tělo
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
zdrojem	zdroj	k1gInSc7	zdroj
živin	živina	k1gFnPc2	živina
<g/>
.	.	kIx.	.
</s>
<s>
Přežvýkaví	přežvýkavý	k2eAgMnPc1d1	přežvýkavý
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
dokážou	dokázat	k5eAaPmIp3nP	dokázat
zužitkovat	zužitkovat	k5eAaPmF	zužitkovat
těžko	těžko	k6eAd1	těžko
stravitelnou	stravitelný	k2eAgFnSc4d1	stravitelná
potravu	potrava	k1gFnSc4	potrava
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
lichokopytníci	lichokopytník	k1gMnPc1	lichokopytník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
příčinou	příčina	k1gFnSc7	příčina
jejich	jejich	k3xOp3gInSc2	jejich
evolučního	evoluční	k2eAgInSc2d1	evoluční
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Slepé	slepý	k2eAgNnSc1d1	slepé
střevo	střevo	k1gNnSc1	střevo
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
tračník	tračník	k1gInSc1	tračník
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
zavinutý	zavinutý	k2eAgMnSc1d1	zavinutý
do	do	k7c2	do
spirály	spirála	k1gFnSc2	spirála
<g/>
.	.	kIx.	.
</s>
<s>
Děloha	děloha	k1gFnSc1	děloha
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
je	být	k5eAaImIp3nS	být
dvourohá	dvourohý	k2eAgFnSc1d1	dvourohá
<g/>
,	,	kIx,	,
placenta	placenta	k1gFnSc1	placenta
je	být	k5eAaImIp3nS	být
rozptýlená	rozptýlený	k2eAgFnSc1d1	rozptýlená
nebo	nebo	k8xC	nebo
klkovitá	klkovitý	k2eAgFnSc1d1	klkovitý
<g/>
,	,	kIx,	,
nepravá	pravý	k2eNgFnSc1d1	nepravá
(	(	kIx(	(
<g/>
adeciduata	adeciduata	k1gFnSc1	adeciduata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
491	[number]	k4	491
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
rodí	rodit	k5eAaImIp3nS	rodit
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
taxonomie	taxonomie	k1gFnSc1	taxonomie
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
sudokopytníky	sudokopytník	k1gMnPc4	sudokopytník
i	i	k8xC	i
kytovce	kytovec	k1gMnPc4	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
křídy	křída	k1gFnSc2	křída
se	se	k3xPyFc4	se
od	od	k7c2	od
předků	předek	k1gInPc2	předek
dnešních	dnešní	k2eAgMnPc2d1	dnešní
hmyzožravců	hmyzožravec	k1gMnPc2	hmyzožravec
oddělila	oddělit	k5eAaPmAgFnS	oddělit
skupina	skupina	k1gFnSc1	skupina
prakopytníků	prakopytník	k1gMnPc2	prakopytník
(	(	kIx(	(
<g/>
Condylarthra	Condylarthra	k1gFnSc1	Condylarthra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společní	společní	k2eAgMnPc1d1	společní
předkové	předek	k1gMnPc1	předek
prašelem	prašel	k1gInSc7	prašel
(	(	kIx(	(
<g/>
Creodonta	Creodonta	k1gFnSc1	Creodonta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několika	několik	k4yIc2	několik
vymřelých	vymřelý	k2eAgInPc2d1	vymřelý
řádů	řád	k1gInPc2	řád
kopytníků	kopytník	k1gMnPc2	kopytník
(	(	kIx(	(
<g/>
Litopterna	Litopterna	k1gFnSc1	Litopterna
<g/>
,	,	kIx,	,
Notoungulata	Notoungule	k1gNnPc1	Notoungule
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
i	i	k8xC	i
dnešních	dnešní	k2eAgMnPc2d1	dnešní
lichokopytníků	lichokopytník	k1gMnPc2	lichokopytník
a	a	k8xC	a
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnPc1d3	nejstarší
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
už	už	k6eAd1	už
v	v	k7c6	v
eocénu	eocén	k1gInSc6	eocén
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
55	[number]	k4	55
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
lichokopytníků	lichokopytník	k1gMnPc2	lichokopytník
<g/>
,	,	kIx,	,
během	během	k7c2	během
miocénu	miocén	k1gInSc2	miocén
ale	ale	k8xC	ale
začali	začít	k5eAaPmAgMnP	začít
lichokopytníci	lichokopytník	k1gMnPc1	lichokopytník
vymírat	vymírat	k5eAaImF	vymírat
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
přežvýkaví	přežvýkavý	k2eAgMnPc1d1	přežvýkavý
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
ústup	ústup	k1gInSc1	ústup
lichokopytníků	lichokopytník	k1gMnPc2	lichokopytník
umožnil	umožnit	k5eAaPmAgInS	umožnit
rozšíření	rozšíření	k1gNnSc4	rozšíření
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jestli	jestli	k8xS	jestli
vymírání	vymírání	k1gNnSc4	vymírání
lichokopytníků	lichokopytník	k1gMnPc2	lichokopytník
způsobila	způsobit	k5eAaPmAgFnS	způsobit
naopak	naopak	k6eAd1	naopak
konkurence	konkurence	k1gFnSc1	konkurence
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
lesů	les	k1gInPc2	les
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
travnatých	travnatý	k2eAgFnPc2d1	travnatá
stepí	step	k1gFnPc2	step
a	a	k8xC	a
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
mohli	moct	k5eAaImAgMnP	moct
novou	nový	k2eAgFnSc4d1	nová
potravu	potrava	k1gFnSc4	potrava
trávit	trávit	k5eAaImF	trávit
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
lichokopytníci	lichokopytník	k1gMnPc1	lichokopytník
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
velkých	velký	k2eAgFnPc2d1	velká
šelem	šelma	k1gFnPc2	šelma
<g/>
,	,	kIx,	,
a	a	k8xC	a
přežvýkaví	přežvýkavý	k2eAgMnPc1d1	přežvýkavý
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
měli	mít	k5eAaImAgMnP	mít
výhodu	výhoda	k1gFnSc4	výhoda
ve	v	k7c6	v
schopnosti	schopnost	k1gFnSc6	schopnost
rychle	rychle	k6eAd1	rychle
zkonzumovat	zkonzumovat	k5eAaPmF	zkonzumovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
tu	tu	k6eAd1	tu
pak	pak	k6eAd1	pak
zpracovat	zpracovat	k5eAaPmF	zpracovat
někde	někde	k6eAd1	někde
v	v	k7c6	v
ústraní	ústraní	k1gNnSc6	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Přežvykování	přežvykování	k1gNnSc1	přežvykování
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dvojí	dvojí	k4xRgNnSc4	dvojí
zpracování	zpracování	k1gNnSc4	zpracování
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
fermentována	fermentovat	k5eAaBmNgFnS	fermentovat
v	v	k7c6	v
předžaludcích	předžaludek	k1gInPc6	předžaludek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
sudokopytníků	sudokopytník	k1gMnPc2	sudokopytník
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Velbloudi	velbloud	k1gMnPc1	velbloud
mají	mít	k5eAaImIp3nP	mít
tříkomorový	tříkomorový	k2eAgInSc4d1	tříkomorový
žaludek	žaludek	k1gInSc4	žaludek
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
kančilovití	kančilovitý	k2eAgMnPc1d1	kančilovitý
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
přežvýkaví	přežvýkavý	k2eAgMnPc1d1	přežvýkavý
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
mají	mít	k5eAaImIp3nP	mít
dokonalejší	dokonalý	k2eAgInSc4d2	dokonalejší
čtyřkomorový	čtyřkomorový	k2eAgInSc4d1	čtyřkomorový
žaludek	žaludek	k1gInSc4	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kytovci	kytovec	k1gMnPc1	kytovec
jsou	být	k5eAaImIp3nP	být
blízkými	blízký	k2eAgMnPc7d1	blízký
příbuznými	příbuzný	k1gMnPc7	příbuzný
hrochů	hroch	k1gMnPc2	hroch
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
kytovci	kytovec	k1gMnPc1	kytovec
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sudokopytníky	sudokopytník	k1gMnPc7	sudokopytník
(	(	kIx(	(
<g/>
Artiodactyla	Artiodactyla	k1gFnPc7	Artiodactyla
<g/>
)	)	kIx)	)
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
řádu	řád	k1gInSc2	řád
Cetartiodactyla	Cetartiodactyla	k1gFnSc2	Cetartiodactyla
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nepřežvýkavé	přežvýkavý	k2eNgFnPc4d1	přežvýkavý
patří	patřit	k5eAaImIp3nP	patřit
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
s	s	k7c7	s
jednokomorovým	jednokomorový	k2eAgInSc7d1	jednokomorový
žaludkem	žaludek	k1gInSc7	žaludek
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
hrochů	hroch	k1gMnPc2	hroch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepřežvykují	přežvykovat	k5eNaImIp3nP	přežvykovat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
všežravci	všežravec	k1gMnPc1	všežravec
<g/>
,	,	kIx,	,
sklovina	sklovina	k1gFnSc1	sklovina
stoliček	stolička	k1gFnPc2	stolička
tvoří	tvořit	k5eAaImIp3nP	tvořit
hrboly	hrbol	k1gInPc1	hrbol
<g/>
,	,	kIx,	,
zuby	zub	k1gInPc1	zub
mají	mít	k5eAaImIp3nP	mít
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
dřeňovou	dřeňový	k2eAgFnSc4d1	dřeňová
dutinu	dutina	k1gFnSc4	dutina
a	a	k8xC	a
nedorůstají	dorůstat	k5eNaImIp3nP	dorůstat
<g/>
.	.	kIx.	.
</s>
<s>
Špičáky	špičák	k1gInPc1	špičák
bývají	bývat	k5eAaImIp3nP	bývat
přeměněné	přeměněný	k2eAgInPc1d1	přeměněný
v	v	k7c4	v
kly	kel	k1gInPc4	kel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
mají	mít	k5eAaImIp3nP	mít
čtyři	čtyři	k4xCgInPc1	čtyři
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
prsty	prst	k1gInPc1	prst
(	(	kIx(	(
<g/>
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
pekariovití	pekariovití	k1gMnPc1	pekariovití
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
prst	prst	k1gInSc4	prst
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
spárky	spárka	k1gFnSc2	spárka
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
pátý	pátý	k4xOgInSc4	pátý
paspárky	paspárek	k1gInPc4	paspárek
<g/>
.	.	kIx.	.
infrařád	infrařáda	k1gFnPc2	infrařáda
<g/>
:	:	kIx,	:
Cetancodonta	Cetancodonta	k1gFnSc1	Cetancodonta
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Caenotheriidae	Caenotheriidae	k1gFnSc1	Caenotheriidae
†	†	k?	†
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Anoplotheriidae	Anoplotheriidae	k1gFnSc1	Anoplotheriidae
†	†	k?	†
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Anthracotheriidae	Anthracotheriidae	k1gFnSc1	Anthracotheriidae
†	†	k?	†
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Hippopotamidae	Hippopotamidae	k1gFnSc1	Hippopotamidae
–	–	k?	–
hrochovití	hrochovitý	k2eAgMnPc1d1	hrochovitý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Cetacea	cetaceum	k1gNnSc2	cetaceum
–	–	k?	–
kytovci	kytovec	k1gMnSc3	kytovec
nadčeleď	nadčeleď	k1gFnSc4	nadčeleď
<g/>
:	:	kIx,	:
štětináči	štětináč	k1gMnPc1	štětináč
(	(	kIx(	(
<g/>
Suoidea	Suoidea	k1gMnSc1	Suoidea
<g/>
)	)	kIx)	)
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Suidae	Suidae	k1gFnSc1	Suidae
–	–	k?	–
prasatovití	prasatovitý	k2eAgMnPc1d1	prasatovitý
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
:	:	kIx,	:
Tayassuidae	Tayassuidae	k1gNnSc4	Tayassuidae
–	–	k?	–
pekariovití	pekariovitý	k2eAgMnPc1d1	pekariovitý
nadčeleď	nadčeleď	k1gFnSc1	nadčeleď
<g/>
:	:	kIx,	:
Oreodontoidea	Oreodontoidea	k1gFnSc1	Oreodontoidea
†	†	k?	†
Mozolnatci	Mozolnatec	k1gMnPc1	Mozolnatec
(	(	kIx(	(
<g/>
velbloudi	velbloud	k1gMnPc1	velbloud
<g/>
,	,	kIx,	,
lamy	lama	k1gFnPc1	lama
<g/>
,	,	kIx,	,
vikuně	vikuně	k6eAd1	vikuně
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
přežvýkaví	přežvýkavý	k2eAgMnPc1d1	přežvýkavý
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc4	jejich
žaludek	žaludek	k1gInSc1	žaludek
je	být	k5eAaImIp3nS	být
tříkomorový	tříkomorový	k2eAgInSc1d1	tříkomorový
<g/>
.	.	kIx.	.
</s>
<s>
Našlapují	našlapovat	k5eAaImIp3nP	našlapovat
na	na	k7c4	na
prstní	prstní	k2eAgInPc4d1	prstní
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
podložené	podložený	k2eAgInPc1d1	podložený
pružným	pružný	k2eAgInSc7d1	pružný
mozolem	mozol	k1gInSc7	mozol
<g/>
.	.	kIx.	.
</s>
<s>
Kopýtka	kopýtko	k1gNnPc1	kopýtko
jsou	být	k5eAaImIp3nP	být
nehtovitého	htovitý	k2eNgInSc2d1	htovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
prst	prst	k1gInSc4	prst
je	být	k5eAaImIp3nS	být
zakrnělý	zakrnělý	k2eAgMnSc1d1	zakrnělý
<g/>
,	,	kIx,	,
záprstní	záprstní	k2eAgFnPc1d1	záprstní
a	a	k8xC	a
nártní	nártní	k2eAgFnPc1d1	nártní
kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
srostlé	srostlý	k2eAgFnPc1d1	srostlá
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k6eAd1	tak
jedinou	jediný	k2eAgFnSc4d1	jediná
kost	kost	k1gFnSc4	kost
tvaru	tvar	k1gInSc2	tvar
obráceného	obrácený	k2eAgInSc2d1	obrácený
Y.	Y.	kA	Y.
Loketní	loketní	k2eAgFnPc1d1	loketní
a	a	k8xC	a
lýtková	lýtkový	k2eAgFnSc1d1	lýtková
kost	kost	k1gFnSc1	kost
jsou	být	k5eAaImIp3nP	být
redukované	redukovaný	k2eAgInPc1d1	redukovaný
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
mozolnatců	mozolnatec	k1gMnPc2	mozolnatec
jsou	být	k5eAaImIp3nP	být
kulaté	kulatý	k2eAgFnPc1d1	kulatá
červené	červený	k2eAgFnPc1d1	červená
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
.	.	kIx.	.
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Camelidae	Camelidae	k1gFnSc1	Camelidae
–	–	k?	–
velbloudovití	velbloudovitý	k2eAgMnPc1d1	velbloudovitý
Přežvýkaví	přežvýkavý	k2eAgMnPc1d1	přežvýkavý
mají	mít	k5eAaImIp3nP	mít
vícekomorové	vícekomorový	k2eAgInPc4d1	vícekomorový
žaludky	žaludek	k1gInPc4	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
prst	prst	k1gInSc4	prst
jsou	být	k5eAaImIp3nP	být
jediné	jediný	k2eAgInPc1d1	jediný
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgInPc1d1	funkční
prsty	prst	k1gInPc1	prst
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInPc1d1	poslední
články	článek	k1gInPc1	článek
jsou	být	k5eAaImIp3nP	být
kryté	krytý	k2eAgFnPc1d1	krytá
rohovinou	rohovina	k1gFnSc7	rohovina
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
paznehty	pazneht	k1gInPc1	pazneht
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
okraj	okraj	k1gInSc4	okraj
zvířata	zvíře	k1gNnPc4	zvíře
našlapují	našlapovat	k5eAaImIp3nP	našlapovat
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
pátý	pátý	k4xOgInSc4	pátý
prst	prst	k1gInSc4	prst
nemají	mít	k5eNaImIp3nP	mít
kostní	kostní	k2eAgInSc4d1	kostní
podklad	podklad	k1gInSc4	podklad
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
rudimentární	rudimentární	k2eAgFnPc4d1	rudimentární
paznehtky	paznehtka	k1gFnPc4	paznehtka
<g/>
.	.	kIx.	.
</s>
<s>
Záprstní	záprstní	k2eAgFnPc1d1	záprstní
a	a	k8xC	a
nártní	nártní	k2eAgFnPc1d1	nártní
kosti	kost	k1gFnPc1	kost
jsou	být	k5eAaImIp3nP	být
srostlé	srostlý	k2eAgFnPc1d1	srostlá
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
jedinou	jediný	k2eAgFnSc4d1	jediná
kost	kost	k1gFnSc4	kost
válcovitého	válcovitý	k2eAgInSc2d1	válcovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Loketní	loketní	k2eAgFnSc1d1	loketní
a	a	k8xC	a
lýtková	lýtkový	k2eAgFnSc1d1	lýtková
kost	kost	k1gFnSc1	kost
jsou	být	k5eAaImIp3nP	být
redukované	redukovaný	k2eAgInPc1d1	redukovaný
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInPc1d1	horní
řezáky	řezák	k1gInPc1	řezák
chybí	chybět	k5eAaImIp3nP	chybět
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgInPc1d1	spodní
špičáky	špičák	k1gInPc1	špičák
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
řezákům	řezák	k1gInPc3	řezák
<g/>
.	.	kIx.	.
</s>
<s>
Stoličky	stolička	k1gFnPc1	stolička
mají	mít	k5eAaImIp3nP	mít
zřasenou	zřasený	k2eAgFnSc4d1	zřasená
sklovinu	sklovina	k1gFnSc4	sklovina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
semihypselodontní	semihypselodontní	k2eAgFnPc1d1	semihypselodontní
<g/>
,	,	kIx,	,
s	s	k7c7	s
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
dobou	doba	k1gFnSc7	doba
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
nejprimitivnější	primitivní	k2eAgMnPc1d3	nejprimitivnější
přežvýkaví	přežvýkavý	k2eAgMnPc1d1	přežvýkavý
<g/>
,	,	kIx,	,
kančilovití	kančilovitý	k2eAgMnPc1d1	kančilovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
malá	malý	k2eAgNnPc4d1	malé
zvířata	zvíře	k1gNnPc4	zvíře
s	s	k7c7	s
třídílným	třídílný	k2eAgInSc7d1	třídílný
žaludkem	žaludek	k1gInSc7	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInPc1d1	horní
řezáky	řezák	k1gInPc1	řezák
samců	samec	k1gMnPc2	samec
jsou	být	k5eAaImIp3nP	být
zvětšené	zvětšený	k2eAgInPc1d1	zvětšený
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
dýkovitý	dýkovitý	k2eAgInSc4d1	dýkovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
:	:	kIx,	:
Tragulidae	Tragulidae	k1gNnSc4	Tragulidae
–	–	k?	–
kančilovití	kančilovitý	k2eAgMnPc1d1	kančilovitý
Přežvýkavci	přežvýkavec	k1gMnPc1	přežvýkavec
se	s	k7c7	s
čtyřdílným	čtyřdílný	k2eAgInSc7d1	čtyřdílný
žaludkem	žaludek	k1gInSc7	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Primitivní	primitivní	k2eAgMnPc1d1	primitivní
zástupci	zástupce	k1gMnPc1	zástupce
mají	mít	k5eAaImIp3nP	mít
zvětšené	zvětšený	k2eAgInPc4d1	zvětšený
horní	horní	k2eAgInPc4d1	horní
špičáky	špičák	k1gInPc4	špičák
(	(	kIx(	(
<g/>
kabarovití	kabarovitý	k2eAgMnPc1d1	kabarovitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
rohy	roh	k1gInPc1	roh
nebo	nebo	k8xC	nebo
parohy	paroh	k1gInPc1	paroh
<g/>
.	.	kIx.	.
nadčeleď	nadčeleď	k1gFnSc1	nadčeleď
<g/>
:	:	kIx,	:
jelenovci	jelenovec	k1gMnPc1	jelenovec
(	(	kIx(	(
<g/>
Cervoidea	Cervoidea	k1gMnSc1	Cervoidea
<g/>
)	)	kIx)	)
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Moschidae	Moschidae	k1gFnSc1	Moschidae
–	–	k?	–
kabarovití	kabarovitý	k2eAgMnPc1d1	kabarovitý
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
:	:	kIx,	:
Cervidae	Cervidae	k1gNnSc4	Cervidae
–	–	k?	–
jelenovití	jelenovitý	k2eAgMnPc1d1	jelenovitý
nadčeleď	nadčeleď	k1gFnSc4	nadčeleď
<g/>
:	:	kIx,	:
žirafovci	žirafovec	k1gInPc7	žirafovec
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Giraffoidea	Giraffoidea	k1gFnSc1	Giraffoidea
<g/>
)	)	kIx)	)
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Giraffidae	Giraffidae	k1gFnSc1	Giraffidae
–	–	k?	–
žirafovití	žirafovitý	k2eAgMnPc1d1	žirafovitý
nadčeleď	nadčeleď	k1gFnSc4	nadčeleď
<g/>
:	:	kIx,	:
vidlorohové	vidloroh	k1gMnPc1	vidloroh
(	(	kIx(	(
<g/>
Antilocaproidea	Antilocaproidea	k1gMnSc1	Antilocaproidea
<g/>
)	)	kIx)	)
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Antilocapridae	Antilocapridae	k1gFnSc1	Antilocapridae
–	–	k?	–
vidlorohovití	vidlorohovitý	k2eAgMnPc1d1	vidlorohovitý
nadčeleď	nadčeleď	k1gFnSc4	nadčeleď
<g/>
:	:	kIx,	:
turovci	turovec	k1gMnPc1	turovec
(	(	kIx(	(
<g/>
Bovoidea	Bovoidea	k1gMnSc1	Bovoidea
<g/>
)	)	kIx)	)
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
:	:	kIx,	:
Bovidae	Bovidae	k1gFnPc2	Bovidae
–	–	k?	–
turovití	turovití	k1gMnPc1	turovití
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sudokopytníci	sudokopytník	k1gMnPc1	sudokopytník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sudokopytník	sudokopytník	k1gMnSc1	sudokopytník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Artiodactyla	Artiodactyla	k1gFnSc2	Artiodactyla
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Evoluce	evoluce	k1gFnSc1	evoluce
savců	savec	k1gMnPc2	savec
3	[number]	k4	3
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Order	Order	k1gInSc1	Order
Cetartiodactyla	Cetartiodactyla	k1gFnSc2	Cetartiodactyla
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
ADW	ADW	kA	ADW
<g/>
:	:	kIx,	:
Artiodactyla	Artiodactyla	k1gMnSc1	Artiodactyla
</s>
