<s>
Termín	termín	k1gInSc1	termín
surrealismus	surrealismus	k1gInSc1	surrealismus
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
divadelní	divadelní	k2eAgFnSc7d1	divadelní
hrou	hra	k1gFnSc7	hra
-	-	kIx~	-
Prsy	prs	k1gInPc1	prs
Thirésiovy	Thirésiův	k2eAgInPc1d1	Thirésiův
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
absolutní	absolutní	k2eAgFnSc4d1	absolutní
realitu	realita	k1gFnSc4	realita
-	-	kIx~	-
surrealitu	surrealita	k1gFnSc4	surrealita
(	(	kIx(	(
<g/>
francouzská	francouzský	k2eAgFnSc1d1	francouzská
předpona	předpona	k1gFnSc1	předpona
sur	sur	k?	sur
-	-	kIx~	-
nad	nad	k7c7	nad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
