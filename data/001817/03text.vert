<s>
Surrealismus	surrealismus	k1gInSc1	surrealismus
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
syrealismus	syrealismus	k1gInSc1	syrealismus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
evropský	evropský	k2eAgInSc4d1	evropský
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
osvobození	osvobození	k1gNnSc4	osvobození
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
podvědomí	podvědomí	k1gNnSc1	podvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
o	o	k7c6	o
zachycení	zachycení	k1gNnSc6	zachycení
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
pocitů	pocit	k1gInPc2	pocit
a	a	k8xC	a
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
impulsy	impuls	k1gInPc4	impuls
surrealismus	surrealismus	k1gInSc1	surrealismus
dostal	dostat	k5eAaPmAgInS	dostat
od	od	k7c2	od
André	André	k1gMnSc2	André
Bretona	Breton	k1gMnSc2	Breton
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
umožnil	umožnit	k5eAaPmAgInS	umožnit
Salvador	Salvador	k1gInSc4	Salvador
Dalí	Dalí	k1gFnSc2	Dalí
<g/>
.	.	kIx.	.
</s>
<s>
Surrealismus	surrealismus	k1gInSc1	surrealismus
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgInSc4d1	aktivní
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
od	od	k7c2	od
původních	původní	k2eAgFnPc2d1	původní
myšlenek	myšlenka	k1gFnPc2	myšlenka
značně	značně	k6eAd1	značně
vzdálil	vzdálit	k5eAaPmAgMnS	vzdálit
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
surrealismus	surrealismus	k1gInSc1	surrealismus
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
divadelní	divadelní	k2eAgFnSc7d1	divadelní
hrou	hra	k1gFnSc7	hra
-	-	kIx~	-
Prsy	prs	k1gInPc1	prs
Thirésiovy	Thirésiův	k2eAgInPc1d1	Thirésiův
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
absolutní	absolutní	k2eAgFnSc4d1	absolutní
realitu	realita	k1gFnSc4	realita
-	-	kIx~	-
surrealitu	surrealita	k1gFnSc4	surrealita
(	(	kIx(	(
<g/>
francouzská	francouzský	k2eAgFnSc1d1	francouzská
předpona	předpona	k1gFnSc1	předpona
sur	sur	k?	sur
-	-	kIx~	-
nad	nad	k7c7	nad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
surrealismu	surrealismus	k1gInSc2	surrealismus
navazovaly	navazovat	k5eAaImAgInP	navazovat
na	na	k7c4	na
dadaismus	dadaismus	k1gInSc4	dadaismus
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
přišlo	přijít	k5eAaPmAgNnS	přijít
mnoho	mnoho	k4c1	mnoho
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
surrealismus	surrealismus	k1gInSc4	surrealismus
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
širší	široký	k2eAgInSc4d2	širší
rozsah	rozsah	k1gInSc4	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Dada	dada	k1gNnSc4	dada
byl	být	k5eAaImAgMnS	být
negativní	negativní	k2eAgFnSc7d1	negativní
odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
první	první	k4xOgFnSc4	první
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
surrealismus	surrealismus	k1gInSc4	surrealismus
měl	mít	k5eAaImAgInS	mít
pozitivnější	pozitivní	k2eAgInSc1d2	pozitivnější
názor	názor	k1gInSc1	názor
-	-	kIx~	-
tj.	tj.	kA	tj.
svět	svět	k1gInSc1	svět
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
změněn	změnit	k5eAaPmNgInS	změnit
a	a	k8xC	a
transformován	transformovat	k5eAaBmNgInS	transformovat
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Surrealismus	surrealismus	k1gInSc1	surrealismus
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dadaismu	dadaismus	k1gInSc2	dadaismus
<g/>
)	)	kIx)	)
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
společenské	společenský	k2eAgFnPc4d1	společenská
<g/>
,	,	kIx,	,
filosofické	filosofický	k2eAgFnPc4d1	filosofická
<g/>
,	,	kIx,	,
umělecké	umělecký	k2eAgFnPc4d1	umělecká
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
otázky	otázka	k1gFnPc4	otázka
a	a	k8xC	a
problémy	problém	k1gInPc1	problém
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
vydal	vydat	k5eAaPmAgMnS	vydat
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
Surrealistický	surrealistický	k2eAgInSc4d1	surrealistický
Manifest	manifest	k1gInSc4	manifest
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgInS	začít
vycházet	vycházet	k5eAaImF	vycházet
časopis	časopis	k1gInSc1	časopis
La	la	k1gNnSc6	la
Révolution	Révolution	k1gInSc1	Révolution
Surréaliste	Surréalist	k1gMnSc5	Surréalist
(	(	kIx(	(
<g/>
Surrealistická	surrealistický	k2eAgFnSc1d1	surrealistická
Revoluce	revoluce	k1gFnSc1	revoluce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dostávali	dostávat	k5eAaImAgMnP	dostávat
prostor	prostor	k1gInSc4	prostor
především	především	k9	především
surrealisté	surrealista	k1gMnPc1	surrealista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Surrealistickém	surrealistický	k2eAgInSc6d1	surrealistický
manifestu	manifest	k1gInSc6	manifest
Breton	Breton	k1gMnSc1	Breton
definoval	definovat	k5eAaBmAgMnS	definovat
surrealismus	surrealismus	k1gInSc4	surrealismus
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
čirý	čirý	k2eAgInSc1d1	čirý
psychický	psychický	k2eAgInSc1d1	psychický
automatismus	automatismus	k1gInSc1	automatismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
posílit	posílit	k5eAaPmF	posílit
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
kresbou	kresba	k1gFnSc7	kresba
a	a	k8xC	a
výrazovými	výrazový	k2eAgInPc7d1	výrazový
prostředky	prostředek	k1gInPc7	prostředek
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
funkci	funkce	k1gFnSc4	funkce
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
rozumem	rozum	k1gInSc7	rozum
nekontrolovatelný	kontrolovatelný	k2eNgInSc1d1	nekontrolovatelný
proud	proud	k1gInSc1	proud
asociací	asociace	k1gFnPc2	asociace
-	-	kIx~	-
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
umělec	umělec	k1gMnSc1	umělec
ponořen	ponořen	k2eAgMnSc1d1	ponořen
sám	sám	k3xTgMnSc1	sám
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
rozumové	rozumový	k2eAgFnSc2d1	rozumová
kontroly	kontrola	k1gFnSc2	kontrola
<g/>
,	,	kIx,	,
či	či	k8xC	či
záměru	záměra	k1gFnSc4	záměra
zaznamenávat	zaznamenávat	k5eAaImF	zaznamenávat
stavy	stav	k1gInPc4	stav
své	svůj	k3xOyFgFnSc2	svůj
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
své	svůj	k3xOyFgNnSc4	svůj
nitro	nitro	k1gNnSc4	nitro
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
sny	sen	k1gInPc4	sen
samozřejmě	samozřejmě	k6eAd1	samozřejmě
také	také	k9	také
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
uměle	uměle	k6eAd1	uměle
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
navozovali	navozovat	k5eAaImAgMnP	navozovat
(	(	kIx(	(
<g/>
či	či	k8xC	či
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
<g/>
)	)	kIx)	)
stavy	stav	k1gInPc1	stav
porušení	porušení	k1gNnSc2	porušení
psychické	psychický	k2eAgFnSc2d1	psychická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
(	(	kIx(	(
<g/>
extatické	extatický	k2eAgFnSc2d1	extatická
<g/>
,	,	kIx,	,
hypnotické	hypnotický	k2eAgFnSc2d1	hypnotická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
psychopatologické	psychopatologický	k2eAgNnSc1d1	psychopatologické
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
byla	být	k5eAaImAgFnS	být
následným	následný	k2eAgInSc7d1	následný
vývojem	vývoj	k1gInSc7	vývoj
surrealismu	surrealismus	k1gInSc2	surrealismus
pojímána	pojímán	k2eAgFnSc1d1	pojímána
velmi	velmi	k6eAd1	velmi
volně	volně	k6eAd1	volně
a	a	k8xC	a
ukázala	ukázat	k5eAaPmAgFnS	ukázat
se	se	k3xPyFc4	se
jako	jako	k9	jako
velice	velice	k6eAd1	velice
životaschopná	životaschopný	k2eAgFnSc1d1	životaschopná
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
dalšího	další	k2eAgInSc2d1	další
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
cíl	cíl	k1gInSc1	cíl
jim	on	k3xPp3gMnPc3	on
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
teoretický	teoretický	k2eAgMnSc1d1	teoretický
a	a	k8xC	a
filosofický	filosofický	k2eAgInSc1d1	filosofický
základ	základ	k1gInSc1	základ
především	především	k9	především
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Surrealismus	surrealismus	k1gInSc4	surrealismus
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
základní	základní	k2eAgNnPc4d1	základní
období	období	k1gNnPc4	období
<g/>
:	:	kIx,	:
Intuitivní	intuitivní	k2eAgNnSc4d1	intuitivní
období	období	k1gNnSc4	období
-	-	kIx~	-
věří	věřit	k5eAaImIp3nS	věřit
ve	v	k7c4	v
všemohoucnost	všemohoucnost	k1gFnSc4	všemohoucnost
myšlenky	myšlenka	k1gFnSc2	myšlenka
postavené	postavený	k2eAgFnSc2d1	postavená
nad	nad	k7c4	nad
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
surrealisté	surrealista	k1gMnPc1	surrealista
opírali	opírat	k5eAaImAgMnP	opírat
o	o	k7c4	o
filosofický	filosofický	k2eAgInSc4d1	filosofický
idealismus	idealismus	k1gInSc4	idealismus
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc4d1	sociální
aktivitu	aktivita	k1gFnSc4	aktivita
nevyhledávali	vyhledávat	k5eNaImAgMnP	vyhledávat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
program	program	k1gInSc1	program
vyložil	vyložit	k5eAaPmAgInS	vyložit
v	v	k7c6	v
Manifestu	manifest	k1gInSc6	manifest
surrealismu	surrealismus	k1gInSc2	surrealismus
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgNnSc1d1	politické
období	období	k1gNnSc1	období
-	-	kIx~	-
Surrealisté	surrealista	k1gMnPc1	surrealista
si	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
vázanost	vázanost	k1gFnSc4	vázanost
svého	svůj	k3xOyFgInSc2	svůj
experimentu	experiment	k1gInSc2	experiment
a	a	k8xC	a
pociťovali	pociťovat	k5eAaImAgMnP	pociťovat
nutnost	nutnost	k1gFnSc4	nutnost
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
se	se	k3xPyFc4	se
k	k	k7c3	k
tehdejšímu	tehdejší	k2eAgInSc3d1	tehdejší
politickému	politický	k2eAgInSc3d1	politický
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
Učinili	učinit	k5eAaImAgMnP	učinit
to	ten	k3xDgNnSc4	ten
veřejným	veřejný	k2eAgInSc7d1	veřejný
protestem	protest	k1gInSc7	protest
proti	proti	k7c3	proti
kolonialismu	kolonialismus	k1gInSc3	kolonialismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
nastává	nastávat	k5eAaImIp3nS	nastávat
jejich	jejich	k3xOp3gNnPc2	jejich
přibližování	přibližování	k1gNnPc2	přibližování
levici	levice	k1gFnSc4	levice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gNnSc4	on
zavedla	zavést	k5eAaPmAgFnS	zavést
až	až	k9	až
k	k	k7c3	k
obdivu	obdiv	k1gInSc3	obdiv
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stávali	stávat	k5eAaImAgMnP	stávat
velmi	velmi	k6eAd1	velmi
ideologickými	ideologický	k2eAgMnPc7d1	ideologický
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
Druhém	druhý	k4xOgInSc6	druhý
manifestu	manifest	k1gInSc6	manifest
surrealismu	surrealismus	k1gInSc2	surrealismus
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
změnou	změna	k1gFnSc7	změna
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
i	i	k9	i
název	název	k1gInSc1	název
časopisu	časopis	k1gInSc2	časopis
(	(	kIx(	(
<g/>
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
celý	celý	k2eAgInSc4d1	celý
problém	problém	k1gInSc4	problém
<g/>
)	)	kIx)	)
ze	z	k7c2	z
Surrealistické	surrealistický	k2eAgFnSc2d1	surrealistická
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Surrealismus	surrealismus	k1gInSc1	surrealismus
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
a	a	k8xC	a
Philippe	Philipp	k1gInSc5	Philipp
Soupault	Soupault	k1gMnSc1	Soupault
napsali	napsat	k5eAaPmAgMnP	napsat
první	první	k4xOgFnSc4	první
automatickou	automatický	k2eAgFnSc4d1	automatická
knihu	kniha	k1gFnSc4	kniha
-	-	kIx~	-
Les	les	k1gInSc4	les
Champs	Champsa	k1gFnPc2	Champsa
Magnetiques	Magnetiquesa	k1gFnPc2	Magnetiquesa
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
André	André	k1gMnSc1	André
Masson	Masson	k1gMnSc1	Masson
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
automatické	automatický	k2eAgNnSc4d1	automatické
kreslení	kreslení	k1gNnSc4	kreslení
a	a	k8xC	a
obraz	obraz	k1gInSc4	obraz
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiné	jiný	k2eAgFnPc4d1	jiná
automatické	automatický	k2eAgFnPc4d1	automatická
metody	metoda	k1gFnPc4	metoda
stalo	stát	k5eAaPmAgNnS	stát
významnou	významný	k2eAgFnSc7d1	významná
částí	část	k1gFnSc7	část
surrealistické	surrealistický	k2eAgFnSc2d1	surrealistická
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Automacie	Automacie	k1gFnSc1	Automacie
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
přizpůsobena	přizpůsobit	k5eAaPmNgFnS	přizpůsobit
počítači	počítač	k1gInPc7	počítač
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
)	)	kIx)	)
Mnoho	mnoho	k4c1	mnoho
populárních	populární	k2eAgMnPc2d1	populární
umělců	umělec	k1gMnPc2	umělec
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
období	období	k1gNnSc2	období
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
)	)	kIx)	)
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
k	k	k7c3	k
surrealismu	surrealismus	k1gInSc2	surrealismus
hlásili	hlásit	k5eAaImAgMnP	hlásit
například	například	k6eAd1	například
René	René	k1gMnSc5	René
Magritte	Magritt	k1gMnSc5	Magritt
<g/>
,	,	kIx,	,
Joan	Joan	k1gMnSc1	Joan
Miró	Miró	k1gMnSc1	Miró
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
,	,	kIx,	,
Alberto	Alberta	k1gFnSc5	Alberta
Giacometti	Giacomett	k1gMnPc1	Giacomett
<g/>
,	,	kIx,	,
Valentine	Valentin	k1gMnSc5	Valentin
Hugo	Hugo	k1gMnSc5	Hugo
<g/>
,	,	kIx,	,
Meret	Meret	k1gMnSc1	Meret
Oppenheim	Oppenheim	k1gMnSc1	Oppenheim
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
Man	Man	k1gMnSc1	Man
Ray	Ray	k1gMnSc1	Ray
a	a	k8xC	a
Yves	Yves	k1gInSc1	Yves
Tanguy	Tangua	k1gFnSc2	Tangua
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
dominanci	dominance	k1gFnSc3	dominance
Francie	Francie	k1gFnSc2	Francie
bývá	bývat	k5eAaImIp3nS	bývat
tento	tento	k3xDgInSc4	tento
směr	směr	k1gInSc4	směr
někdy	někdy	k6eAd1	někdy
mylně	mylně	k6eAd1	mylně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
čistě	čistě	k6eAd1	čistě
francouzský	francouzský	k2eAgMnSc1d1	francouzský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
surrealismus	surrealismus	k1gInSc1	surrealismus
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
již	již	k9	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
této	tento	k3xDgFnSc2	tento
francouzské	francouzský	k2eAgFnSc2d1	francouzská
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
např.	např.	kA	např.
i	i	k9	i
Česká	český	k2eAgFnSc1d1	Česká
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
mluvčími	mluvčí	k1gFnPc7	mluvčí
byli	být	k5eAaImAgMnP	být
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gFnSc2	Teig
<g/>
;	;	kIx,	;
Česká	český	k2eAgFnSc1d1	Česká
skupina	skupina	k1gFnSc1	skupina
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
nepřetržitě	přetržitě	k6eNd1	přetržitě
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
surrealistických	surrealistický	k2eAgMnPc2d1	surrealistický
teoretiků	teoretik	k1gMnPc2	teoretik
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
radikálů	radikál	k1gMnPc2	radikál
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
než	než	k8xS	než
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Rumun	Rumun	k1gMnSc1	Rumun
Gherasim	Gherasim	k1gMnSc1	Gherasim
Luca	Luca	k1gMnSc1	Luca
<g/>
.	.	kIx.	.
</s>
<s>
Ohlas	ohlas	k1gInSc1	ohlas
surrealismu	surrealismus	k1gInSc2	surrealismus
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
fašismu	fašismus	k1gInSc2	fašismus
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
oslaben	oslabit	k5eAaPmNgInS	oslabit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
toto	tento	k3xDgNnSc1	tento
hnutí	hnutí	k1gNnSc1	hnutí
opustilo	opustit	k5eAaPmAgNnS	opustit
mnoho	mnoho	k4c1	mnoho
umělců	umělec	k1gMnPc2	umělec
(	(	kIx(	(
<g/>
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
Tristan	Tristan	k1gInSc4	Tristan
Tzara	Tzar	k1gInSc2	Tzar
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Eluard	Eluard	k1gMnSc1	Eluard
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
surrealismus	surrealismus	k1gInSc1	surrealismus
často	často	k6eAd1	často
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
obrazy	obraz	k1gInPc7	obraz
Salvadora	Salvador	k1gMnSc2	Salvador
Dalího	Dalí	k1gMnSc2	Dalí
<g/>
.	.	kIx.	.
</s>
<s>
Dalí	Dalí	k1gMnSc1	Dalí
byl	být	k5eAaImAgMnS	být
aktivním	aktivní	k2eAgMnSc7d1	aktivní
surrealistou	surrealista	k1gMnSc7	surrealista
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
-	-	kIx~	-
1936	[number]	k4	1936
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
hnutí	hnutí	k1gNnSc4	hnutí
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
sám	sám	k3xTgMnSc1	sám
nazval	nazvat	k5eAaBmAgMnS	nazvat
Paranoicko-Kritickou	paranoickoritický	k2eAgFnSc7d1	paranoicko-kritická
metodou	metoda	k1gFnSc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
hnutí	hnutí	k1gNnSc2	hnutí
začala	začít	k5eAaPmAgFnS	začít
surrealismu	surrealismus	k1gInSc2	surrealismus
vzdalovat	vzdalovat	k5eAaImF	vzdalovat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
začaly	začít	k5eAaPmAgInP	začít
vznikat	vznikat	k5eAaImF	vznikat
surrealistické	surrealistický	k2eAgFnSc2d1	surrealistická
skupiny	skupina	k1gFnSc2	skupina
i	i	k9	i
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
surrealisté	surrealista	k1gMnPc1	surrealista
nikdy	nikdy	k6eAd1	nikdy
nepůsobili	působit	k5eNaImAgMnP	působit
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Surrealistická	surrealistický	k2eAgFnSc1d1	surrealistická
literatura	literatura	k1gFnSc1	literatura
se	se	k3xPyFc4	se
také	také	k9	také
snažila	snažit	k5eAaImAgFnS	snažit
být	být	k5eAaImF	být
jen	jen	k9	jen
přepisem	přepis	k1gInSc7	přepis
podvědomí	podvědomí	k1gNnSc2	podvědomí
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nutí	nutit	k5eAaImIp3nS	nutit
pozdějšího	pozdní	k2eAgMnSc4d2	pozdější
čtenáře	čtenář	k1gMnSc4	čtenář
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
stav	stav	k1gInSc4	stav
v	v	k7c6	v
jakém	jaký	k3yRgNnSc6	jaký
byl	být	k5eAaImAgMnS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc1	dílo
nesrozumitelné	srozumitelný	k2eNgNnSc1d1	nesrozumitelné
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
básně	báseň	k1gFnPc1	báseň
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případech	případ	k1gInPc6	případ
nerýmují	rýmovat	k5eNaImIp3nP	rýmovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
umělci	umělec	k1gMnPc7	umělec
stali	stát	k5eAaPmAgMnP	stát
<g/>
:	:	kIx,	:
Ačkoli	ačkoli	k8xS	ačkoli
Breton	breton	k1gInSc1	breton
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
k	k	k7c3	k
možnosti	možnost	k1gFnSc3	možnost
surrealistické	surrealistický	k2eAgFnSc2d1	surrealistická
hudby	hudba	k1gFnSc2	hudba
stavěl	stavět	k5eAaImAgMnS	stavět
spíše	spíše	k9	spíše
záporně	záporně	k6eAd1	záporně
(	(	kIx(	(
<g/>
esejem	esej	k1gInSc7	esej
"	"	kIx"	"
<g/>
Ticho	ticho	k1gNnSc1	ticho
je	být	k5eAaImIp3nS	být
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdnější	pozdný	k2eAgMnPc1d2	pozdný
surrealisté	surrealista	k1gMnPc1	surrealista
našli	najít	k5eAaPmAgMnP	najít
paralely	paralela	k1gFnPc4	paralela
k	k	k7c3	k
surrealismu	surrealismus	k1gInSc3	surrealismus
v	v	k7c6	v
jazzu	jazz	k1gInSc3	jazz
<g/>
,	,	kIx,	,
a	a	k8xC	a
blues	blues	k1gNnSc4	blues
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
surrealismu	surrealismus	k1gInSc2	surrealismus
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaImF	vysledovat
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
rockových	rockový	k2eAgFnPc6d1	rocková
a	a	k8xC	a
rapových	rapový	k2eAgFnPc6d1	rapová
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Surrealistic	Surrealistice	k1gFnPc2	Surrealistice
Pillow	Pillow	k1gFnPc2	Pillow
(	(	kIx(	(
<g/>
Surrealistický	surrealistický	k2eAgInSc1d1	surrealistický
polštář	polštář	k1gInSc1	polštář
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
druhým	druhý	k4xOgNnSc7	druhý
albem	album	k1gNnSc7	album
americké	americký	k2eAgFnSc2d1	americká
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Jefferson	Jefferson	k1gMnSc1	Jefferson
Airplane	Airplan	k1gMnSc5	Airplan
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
surrealismu	surrealismus	k1gInSc2	surrealismus
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
různých	různý	k2eAgInPc2d1	různý
směrů	směr	k1gInPc2	směr
současné	současný	k2eAgFnSc2d1	současná
avantgardní	avantgardní	k2eAgFnSc2d1	avantgardní
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ambient	ambient	k1gInSc1	ambient
<g/>
,	,	kIx,	,
noise	noise	k1gFnSc1	noise
<g/>
,	,	kIx,	,
či	či	k8xC	či
různá	různý	k2eAgFnSc1d1	různá
elektronická	elektronický	k2eAgFnSc1d1	elektronická
taneční	taneční	k2eAgFnSc1d1	taneční
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výstavou	výstava	k1gFnSc7	výstava
Andyho	Andy	k1gMnSc2	Andy
Lasse	Lass	k1gMnSc2	Lass
(	(	kIx(	(
<g/>
vernisáž	vernisáž	k1gFnSc1	vernisáž
25	[number]	k4	25
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
201	[number]	k4	201
<g/>
)	)	kIx)	)
Rozjímání	rozjímání	k1gNnSc6	rozjímání
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
byla	být	k5eAaImAgFnS	být
realizována	realizován	k2eAgFnSc1d1	realizována
hudba	hudba	k1gFnSc1	hudba
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
grafické	grafický	k2eAgFnSc2d1	grafická
partitury	partitura	k1gFnSc2	partitura
Miroslava	Miroslav	k1gMnSc2	Miroslav
Veselého	Veselý	k1gMnSc2	Veselý
využívající	využívající	k2eAgInSc4d1	využívající
improvizačních	improvizační	k2eAgFnPc2d1	improvizační
možností	možnost	k1gFnSc7	možnost
postupů	postup	k1gInPc2	postup
freejazzu	freejazz	k1gInSc2	freejazz
<g/>
,	,	kIx,	,
středověké	středověký	k2eAgFnSc2d1	středověká
a	a	k8xC	a
japonské	japonský	k2eAgFnSc2d1	japonská
hudby	hudba	k1gFnSc2	hudba
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Jan	Jan	k1gMnSc1	Jan
Grunt	Grunt	k1gMnSc1	Grunt
a	a	k8xC	a
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Chadima	Chadima	k1gMnSc1	Chadima
(	(	kIx(	(
<g/>
saxofony	saxofon	k1gInPc1	saxofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Matoušek	Matoušek	k1gMnSc1	Matoušek
(	(	kIx(	(
<g/>
šakuhači	šakuhač	k1gMnPc1	šakuhač
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kateřina	Kateřina	k1gFnSc1	Kateřina
Vožická	vožický	k2eAgFnSc1d1	Vožická
(	(	kIx(	(
<g/>
niněra	niněra	k1gFnSc1	niněra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Šváb	Šváb	k1gMnSc1	Šváb
-	-	kIx~	-
Pražský	pražský	k2eAgInSc1d1	pražský
dixieland	dixieland	k1gInSc1	dixieland
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
několik	několik	k4yIc4	několik
pokusů	pokus	k1gInPc2	pokus
vytvořit	vytvořit	k5eAaPmF	vytvořit
čistě	čistě	k6eAd1	čistě
surrealistické	surrealistický	k2eAgInPc4d1	surrealistický
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
nejznámější	známý	k2eAgInPc4d3	nejznámější
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
filmy	film	k1gInPc1	film
od	od	k7c2	od
Luise	Luisa	k1gFnSc3	Luisa
Bunuela	Bunuel	k1gMnSc4	Bunuel
Andaluský	andaluský	k2eAgMnSc1d1	andaluský
pes	pes	k1gMnSc1	pes
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
surrealistickým	surrealistický	k2eAgMnSc7d1	surrealistický
tvůrcem	tvůrce	k1gMnSc7	tvůrce
filmů	film	k1gInPc2	film
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
za	za	k7c4	za
surrealistického	surrealistický	k2eAgMnSc4d1	surrealistický
režiséra	režisér	k1gMnSc4	režisér
považován	považován	k2eAgMnSc1d1	považován
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nikdy	nikdy	k6eAd1	nikdy
nezúčastnil	zúčastnit	k5eNaPmAgInS	zúčastnit
surrealistického	surrealistický	k2eAgNnSc2d1	surrealistické
hnutí	hnutí	k1gNnSc2	hnutí
ani	ani	k8xC	ani
nepodnikl	podniknout	k5eNaPmAgMnS	podniknout
žádné	žádný	k3yNgFnPc4	žádný
surrealistické	surrealistický	k2eAgFnPc4d1	surrealistická
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
filmech	film	k1gInPc6	film
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
surrealistické	surrealistický	k2eAgInPc4d1	surrealistický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
Zeď	zeď	k1gFnSc4	zeď
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některé	některý	k3yIgInPc4	některý
surrealistické	surrealistický	k2eAgInPc4d1	surrealistický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Šváb	Šváb	k1gMnSc1	Šváb
-	-	kIx~	-
krátké	krátký	k2eAgInPc4d1	krátký
filmy	film	k1gInPc4	film
(	(	kIx(	(
<g/>
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
Martina	Martina	k1gFnSc1	Martina
Kudlacek	Kudlacka	k1gFnPc2	Kudlacka
<g/>
:	:	kIx,	:
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Amour	Amour	k1gMnSc1	Amour
fou	fou	k?	fou
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Švankmajer	Švankmajer	k1gMnSc1	Švankmajer
-	-	kIx~	-
Přežít	přežít	k5eAaPmF	přežít
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
Šílení	šílení	k1gNnSc4	šílení
<g/>
,	,	kIx,	,
Spiklenci	spiklenec	k1gMnPc1	spiklenec
slasti	slast	k1gFnSc2	slast
<g/>
,	,	kIx,	,
Otesánek	Otesánek	k1gMnSc1	Otesánek
<g/>
,	,	kIx,	,
Lekce	lekce	k1gFnSc1	lekce
Faust	Faust	k1gFnSc1	Faust
<g/>
,	,	kIx,	,
...	...	k?	...
David	David	k1gMnSc1	David
Jařab	Jařab	k1gMnSc1	Jařab
-	-	kIx~	-
Hlava	hlava	k1gFnSc1	hlava
ruce	ruka	k1gFnPc4	ruka
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
Vaterland	vaterland	k1gInSc4	vaterland
<g/>
,	,	kIx,	,
...	...	k?	...
Jan	Jan	k1gMnSc1	Jan
Daňhel	Daňhel	k1gMnSc1	Daňhel
-	-	kIx~	-
Světlopisná	Světlopisný	k2eAgFnSc1d1	Světlopisný
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
Kadmon	Kadmon	k1gMnSc1	Kadmon
<g/>
,	,	kIx,	,
...	...	k?	...
Pavel	Pavel	k1gMnSc1	Pavel
Marek	Marek	k1gMnSc1	Marek
-	-	kIx~	-
Vychovatel	vychovatel	k1gMnSc1	vychovatel
ke	k	k7c3	k
strachu	strach	k1gInSc3	strach
<g/>
,	,	kIx,	,
Mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
Narozeniny	narozeniny	k1gFnPc1	narozeniny
v	v	k7c6	v
parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
Sklizeň	sklizeň	k1gFnSc1	sklizeň
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
