<s>
Krychle	krychle	k1gFnSc1	krychle
(	(	kIx(	(
<g/>
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
šestistěn	šestistěn	k2eAgInSc1d1	šestistěn
nebo	nebo	k8xC	nebo
také	také	k9	také
hexaedr	hexaedr	k1gInSc1	hexaedr
<g/>
)	)	kIx)	)
lidově	lidově	k6eAd1	lidově
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
kostka	kostka	k1gFnSc1	kostka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
trojrozměrné	trojrozměrný	k2eAgNnSc1d1	trojrozměrné
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
stěny	stěna	k1gFnPc1	stěna
tvoří	tvořit	k5eAaImIp3nP	tvořit
šest	šest	k4xCc4	šest
stejných	stejný	k2eAgInPc2d1	stejný
čtverců	čtverec	k1gInPc2	čtverec
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
osm	osm	k4xCc1	osm
rohů	roh	k1gInPc2	roh
a	a	k8xC	a
dvanáct	dvanáct	k4xCc1	dvanáct
hran	hrana	k1gFnPc2	hrana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
a	a	k8xC	a
povrch	povrch	k1gInSc4	povrch
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
krychle	krychle	k1gFnPc4	krychle
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
z	z	k7c2	z
délky	délka	k1gFnSc2	délka
její	její	k3xOp3gFnSc2	její
hrany	hrana	k1gFnSc2	hrana
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
jako	jako	k9	jako
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	V	kA	V
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
6	[number]	k4	6
⋅	⋅	k?	⋅
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Délka	délka	k1gFnSc1	délka
stěnové	stěnový	k2eAgFnSc2d1	stěnová
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
délkou	délka	k1gFnSc7	délka
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
čtverce	čtverec	k1gInSc2	čtverec
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
ke	k	k7c3	k
straně	strana	k1gFnSc3	strana
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
a	a	k8xC	a
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u_	u_	k?	u_
<g/>
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgMnSc1d1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Délku	délka	k1gFnSc4	délka
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
krychle	krychle	k1gFnSc2	krychle
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
dvou	dva	k4xCgInPc2	dva
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
neleží	ležet	k5eNaImIp3nP	ležet
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
stěně	stěna	k1gFnSc6	stěna
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
z	z	k7c2	z
Pythagorovy	Pythagorův	k2eAgFnSc2d1	Pythagorova
věty	věta	k1gFnSc2	věta
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
=	=	kIx~	=
a	a	k8xC	a
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgMnSc1d1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Krychle	krychle	k1gFnSc1	krychle
má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
shodných	shodný	k2eAgFnPc2d1	shodná
stěn	stěna	k1gFnPc2	stěna
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
osm	osm	k4xCc4	osm
vrcholů	vrchol	k1gInPc2	vrchol
a	a	k8xC	a
dvanáct	dvanáct	k4xCc1	dvanáct
hran	hrana	k1gFnPc2	hrana
stejné	stejný	k2eAgFnSc2d1	stejná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Krychle	krychle	k1gFnSc1	krychle
je	být	k5eAaImIp3nS	být
středově	středově	k6eAd1	středově
souměrná	souměrný	k2eAgFnSc1d1	souměrná
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
průsečíku	průsečík	k1gInSc2	průsečík
tělesových	tělesový	k2eAgFnPc2d1	tělesová
úhlopříček	úhlopříčka	k1gFnPc2	úhlopříčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krychle	krychle	k1gFnSc1	krychle
je	být	k5eAaImIp3nS	být
osově	osově	k6eAd1	osově
souměrná	souměrný	k2eAgFnSc1d1	souměrná
podle	podle	k7c2	podle
9	[number]	k4	9
os	osa	k1gFnPc2	osa
<g/>
:	:	kIx,	:
tří	tři	k4xCgFnPc2	tři
spojnic	spojnice	k1gFnPc2	spojnice
středů	střed	k1gInPc2	střed
protilehlých	protilehlý	k2eAgInPc2d1	protilehlý
stěn	stěna	k1gFnPc2	stěna
šesti	šest	k4xCc2	šest
spojnic	spojnice	k1gFnPc2	spojnice
středů	střed	k1gInPc2	střed
protilehlých	protilehlý	k2eAgFnPc2d1	protilehlá
hran	hrana	k1gFnPc2	hrana
Krychle	krychle	k1gFnSc2	krychle
je	být	k5eAaImIp3nS	být
rovinově	rovinově	k6eAd1	rovinově
souměrná	souměrný	k2eAgFnSc1d1	souměrná
podle	podle	k7c2	podle
devíti	devět	k4xCc2	devět
rovin	rovina	k1gFnPc2	rovina
<g/>
:	:	kIx,	:
tří	tři	k4xCgFnPc2	tři
rovin	rovina	k1gFnPc2	rovina
rovnoběžných	rovnoběžný	k2eAgFnPc2d1	rovnoběžná
se	s	k7c7	s
stěnami	stěna	k1gFnPc7	stěna
a	a	k8xC	a
procházejících	procházející	k2eAgFnPc2d1	procházející
středem	středem	k7c2	středem
krychle	krychle	k1gFnSc2	krychle
šesti	šest	k4xCc2	šest
rovin	rovina	k1gFnPc2	rovina
určených	určený	k2eAgInPc2d1	určený
dvojicí	dvojice	k1gFnSc7	dvojice
protilehlých	protilehlý	k2eAgFnPc2d1	protilehlá
hran	hrana	k1gFnPc2	hrana
Krychle	krychle	k1gFnSc2	krychle
je	být	k5eAaImIp3nS	být
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
kvádru	kvádr	k1gInSc2	kvádr
-	-	kIx~	-
patří	patřit	k5eAaImIp3nS	patřit
tedy	tedy	k9	tedy
mezi	mezi	k7c7	mezi
mnohostěny	mnohostěn	k1gInPc7	mnohostěn
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
shodnosti	shodnost	k1gFnSc3	shodnost
všech	všecek	k3xTgFnPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
stěn	stěna	k1gFnPc2	stěna
i	i	k8xC	i
hran	hrana	k1gFnPc2	hrana
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
takzvaná	takzvaný	k2eAgNnPc4d1	takzvané
platónská	platónský	k2eAgNnPc4d1	platónské
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgFnPc4	každý
dvě	dva	k4xCgFnPc4	dva
stěny	stěna	k1gFnPc4	stěna
krychle	krychle	k1gFnSc2	krychle
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
nebo	nebo	k8xC	nebo
kolmé	kolmý	k2eAgFnPc1d1	kolmá
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgMnSc1d1	zajímavý
na	na	k7c6	na
objemu	objem	k1gInSc6	objem
krychle	krychle	k1gFnSc2	krychle
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
následující	následující	k2eAgInSc4d1	následující
problém	problém	k1gInSc4	problém
<g/>
:	:	kIx,	:
Existuje	existovat	k5eAaImIp3nS	existovat
krychle	krychle	k1gFnSc1	krychle
s	s	k7c7	s
celočíselnou	celočíselný	k2eAgFnSc7d1	celočíselná
délkou	délka	k1gFnSc7	délka
hrany	hrana	k1gFnSc2	hrana
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
objem	objem	k1gInSc4	objem
rovný	rovný	k2eAgInSc4d1	rovný
součtu	součet	k1gInSc2	součet
objemů	objem	k1gInPc2	objem
dvou	dva	k4xCgFnPc6	dva
menších	malý	k2eAgFnPc2d2	menší
krychliček	krychlička	k1gFnPc2	krychlička
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
celočíselnými	celočíselný	k2eAgFnPc7d1	celočíselná
délkami	délka	k1gFnPc7	délka
hran	hrana	k1gFnPc2	hrana
<g/>
?	?	kIx.	?
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
obecnější	obecní	k2eAgFnSc2d2	obecní
Velké	velký	k2eAgFnSc2d1	velká
Fermatovy	Fermatův	k2eAgFnSc2d1	Fermatova
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Nemožnost	nemožnost	k1gFnSc4	nemožnost
existence	existence	k1gFnSc2	existence
takové	takový	k3xDgFnSc2	takový
krychle	krychle	k1gFnSc2	krychle
dokázal	dokázat	k5eAaPmAgMnS	dokázat
již	již	k9	již
Euler	Euler	k1gMnSc1	Euler
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krychle	krychle	k1gFnSc2	krychle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hexaedr	hexaedr	k1gInSc1	hexaedr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
krychle	krychle	k1gFnSc2	krychle
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Hrací	hrací	k2eAgFnSc1d1	hrací
kostka	kostka	k1gFnSc1	kostka
Nadkrychle	Nadkrychle	k1gFnSc1	Nadkrychle
<g/>
:	:	kIx,	:
teserakt	teserakt	k1gInSc1	teserakt
<g/>
,	,	kIx,	,
penterakt	penterakt	k1gInSc1	penterakt
<g/>
,	,	kIx,	,
...	...	k?	...
Kvádr	kvádr	k1gInSc1	kvádr
Mnohostěn	mnohostěn	k1gInSc1	mnohostěn
</s>
