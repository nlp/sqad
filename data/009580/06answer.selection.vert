<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1969	[number]	k4	1969
se	se	k3xPyFc4	se
Armstrong	Armstrong	k1gMnSc1	Armstrong
a	a	k8xC	a
Aldrin	aldrin	k1gInSc1	aldrin
v	v	k7c6	v
lunárním	lunární	k2eAgInSc6d1	lunární
modulu	modul	k1gInSc6	modul
spustili	spustit	k5eAaPmAgMnP	spustit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
ve	v	k7c6	v
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
UTC	UTC	kA	UTC
přistáli	přistát	k5eAaImAgMnP	přistát
v	v	k7c6	v
Moři	moře	k1gNnSc6	moře
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
Mare	Mare	k1gFnSc1	Mare
Tranquillitatis	Tranquillitatis	k1gFnSc2	Tranquillitatis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
