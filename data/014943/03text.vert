<s>
Pí	pí	k1gNnSc1
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
K	k	k7c3
označení	označení	k1gNnSc3
konstanty	konstanta	k1gFnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
malé	malý	k2eAgNnSc1d1
π	π	k?
</s>
<s>
Ludolfovo	Ludolfův	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
značené	značený	k2eAgNnSc1d1
π	π	k?
(	(	kIx(
<g/>
čteme	číst	k5eAaImIp1nP
pí	pí	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
matematická	matematický	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
udává	udávat	k5eAaImIp3nS
poměr	poměr	k1gInSc4
obvodu	obvod	k1gInSc2
jakéhokoli	jakýkoli	k3yIgInSc2
kruhu	kruh	k1gInSc2
v	v	k7c6
eukleidovské	eukleidovský	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
průměru	průměr	k1gInSc3
<g/>
;	;	kIx,
také	také	k9
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
hodnota	hodnota	k1gFnSc1
poměru	poměr	k1gInSc2
obsahu	obsah	k1gInSc2
kruhu	kruh	k1gInSc2
ke	k	k7c3
čtverci	čtverec	k1gInSc3
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
poloměru	poloměr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
hodnota	hodnota	k1gFnSc1
v	v	k7c6
desítkové	desítkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
3,141	3,141	k4
<g/>
59265359	#num#	k4
(	(	kIx(
<g/>
lze	lze	k6eAd1
použít	použít	k5eAaPmF
praktické	praktický	k2eAgFnPc4d1
racionální	racionální	k2eAgFnPc4d1
aproximace	aproximace	k1gFnPc4
22	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
pro	pro	k7c4
orientační	orientační	k2eAgInPc4d1
výpočty	výpočet	k1gInPc4
vyžadující	vyžadující	k2eAgFnSc4d1
přesnost	přesnost	k1gFnSc4
hodnoty	hodnota	k1gFnSc2
pouze	pouze	k6eAd1
na	na	k7c4
setiny	setina	k1gFnPc4
resp.	resp.	kA
355	#num#	k4
<g/>
/	/	kIx~
<g/>
113	#num#	k4
pro	pro	k7c4
přesnost	přesnost	k1gFnSc4
pouze	pouze	k6eAd1
na	na	k7c4
miliontiny	miliontina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
matematických	matematický	k2eAgFnPc2d1
<g/>
,	,	kIx,
vědeckých	vědecký	k2eAgFnPc2d1
a	a	k8xC
inženýrských	inženýrský	k2eAgFnPc2d1
rovnic	rovnice	k1gFnPc2
obsahuje	obsahovat	k5eAaImIp3nS
pí	pí	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
z	z	k7c2
něj	on	k3xPp3gMnSc2
dělá	dělat	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejdůležitějších	důležitý	k2eAgFnPc2d3
matematických	matematický	k2eAgFnPc2d1
konstant	konstanta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
je	být	k5eAaImIp3nS
iracionální	iracionální	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
vyjádřeno	vyjádřit	k5eAaPmNgNnS
zlomkem	zlomek	k1gInSc7
m	m	kA
<g/>
/	/	kIx~
<g/>
n	n	k0
<g/>
,	,	kIx,
kde	kde	k6eAd1
m	m	kA
je	být	k5eAaImIp3nS
celé	celý	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
a	a	k8xC
n	n	k0
je	být	k5eAaImIp3nS
přirozené	přirozený	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
také	také	k6eAd1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
nelze	lze	k6eNd1
vyjádřit	vyjádřit	k5eAaPmF
konečným	konečný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
v	v	k7c6
desítkové	desítkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ani	ani	k8xC
pomocí	pomocí	k7c2
periody	perioda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
π	π	k?
dokonce	dokonce	k9
transcendentní	transcendentní	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gInSc4
nelze	lze	k6eNd1
vyjádřit	vyjádřit	k5eAaPmF
konečně	konečně	k6eAd1
dlouhou	dlouhý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
algebraických	algebraický	k2eAgFnPc2d1
operací	operace	k1gFnPc2
s	s	k7c7
celými	celý	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
<g/>
;	;	kIx,
důkaz	důkaz	k1gInSc1
tohoto	tento	k3xDgNnSc2
tvrzení	tvrzení	k1gNnSc2
byl	být	k5eAaImAgInS
výsledkem	výsledek	k1gInSc7
německé	německý	k2eAgFnSc2d1
matematiky	matematika	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dějinách	dějiny	k1gFnPc6
matematiky	matematika	k1gFnSc2
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
snahy	snaha	k1gFnPc1
o	o	k7c6
čím	čí	k3xOyQgNnSc6,k3xOyRgNnSc6
dál	daleko	k6eAd2
přesnější	přesný	k2eAgNnPc4d2
vyjádření	vyjádření	k1gNnPc4
π	π	k?
a	a	k8xC
pochopení	pochopení	k1gNnSc1
jeho	jeho	k3xOp3gFnSc2
povahy	povaha	k1gFnSc2
<g/>
;	;	kIx,
fascinace	fascinace	k1gFnSc1
tímto	tento	k3xDgNnSc7
číslem	číslo	k1gNnSc7
se	se	k3xPyFc4
promítla	promítnout	k5eAaPmAgFnS
i	i	k9
mimo	mimo	k7c4
sféru	sféra	k1gFnSc4
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejspíše	nejspíše	k9
pro	pro	k7c4
jednoduchost	jednoduchost	k1gFnSc4
své	svůj	k3xOyFgFnSc2
definice	definice	k1gFnSc2
se	s	k7c7
π	π	k?
promítlo	promítnout	k5eAaPmAgNnS
do	do	k7c2
populární	populární	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
jiné	jiný	k2eAgInPc1d1
matematické	matematický	k2eAgInPc1d1
konstrukty	konstrukt	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
nejspíše	nejspíše	k9
nejběžnějším	běžný	k2eAgNnSc7d3
společným	společný	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
mezi	mezi	k7c4
matematiky	matematik	k1gMnPc4
a	a	k8xC
nematematiky	nematematik	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zprávy	zpráva	k1gFnPc1
o	o	k7c6
nejnovějším	nový	k2eAgMnSc6d3
<g/>
,	,	kIx,
nejpřesnějším	přesný	k2eAgInSc6d3
odhadu	odhad	k1gInSc6
π	π	k?
se	se	k3xPyFc4
běžně	běžně	k6eAd1
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
tisku	tisk	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2019	#num#	k4
byl	být	k5eAaImAgInS
např.	např.	kA
publikován	publikován	k2eAgInSc4d1
rekord	rekord	k1gInSc4
v	v	k7c6
nejpřesnějším	přesný	k2eAgInSc6d3
odhadu	odhad	k1gInSc6
π	π	k?
v	v	k7c6
desítkové	desítkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
31,4	31,4	k4
bilionu	bilion	k4xCgInSc2
číslic	číslice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Spočítala	spočítat	k5eAaPmAgFnS
jej	on	k3xPp3gNnSc4
japonská	japonský	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
specialistka	specialistka	k1gFnSc1
Emma	Emma	k1gFnSc1
Haruka	Haruk	k1gMnSc2
Iwao	Iwao	k6eAd1
s	s	k7c7
využitím	využití	k1gNnSc7
výpočetní	výpočetní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
cloudu	clouda	k1gMnSc4
Google	Googl	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konstantě	konstanta	k1gFnSc3
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
Ludolfovo	Ludolfův	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
po	po	k7c6
Ludolphovi	Ludolph	k1gMnSc6
van	van	k1gInSc4
Ceulenovi	Ceulenovi	k1gRnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spíše	spíše	k9
historické	historický	k2eAgFnSc6d1
(	(	kIx(
<g/>
ale	ale	k8xC
např.	např.	kA
v	v	k7c6
angličtině	angličtina	k1gFnSc6
používané	používaný	k2eAgNnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc3
Archimédova	Archimédův	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
po	po	k7c6
Archimédovi	Archimédes	k1gMnSc6
ze	z	k7c2
Syrakus	Syrakusy	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základy	základ	k1gInPc1
</s>
<s>
Malé	Malé	k2eAgMnSc1d1
π	π	k?
označující	označující	k2eAgFnSc4d1
konstantu	konstanta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mozaika	mozaika	k1gFnSc1
před	před	k7c7
budovou	budova	k1gFnSc7
matematiky	matematika	k1gFnSc2
na	na	k7c6
Technické	technický	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Berlíně	Berlín	k1gInSc6
</s>
<s>
Písmeno	písmeno	k1gNnSc1
π	π	k?
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Pí	pí	k1gNnSc1
(	(	kIx(
<g/>
písmeno	písmeno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1
písmeno	písmeno	k1gNnSc1
π	π	k?
(	(	kIx(
<g/>
pí	pí	kA
<g/>
)	)	kIx)
pro	pro	k7c4
označení	označení	k1gNnSc4
tohoto	tento	k3xDgNnSc2
čísla	číslo	k1gNnSc2
použil	použít	k5eAaPmAgMnS
poprvé	poprvé	k6eAd1
velšský	velšský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
William	William	k1gInSc1
Jones	Jones	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1706	#num#	k4
jako	jako	k8xS,k8xC
zkratku	zkratka	k1gFnSc4
řeckého	řecký	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
pro	pro	k7c4
obvod	obvod	k1gInSc4
<g/>
,	,	kIx,
řecky	řecky	k6eAd1
<g/>
:	:	kIx,
π	π	k?
(	(	kIx(
<g/>
perimetros	perimetrosa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc4
označení	označení	k1gNnSc4
zpopularizoval	zpopularizovat	k5eAaPmAgMnS
Leonhard	Leonhard	k1gMnSc1
Euler	Euler	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1737	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geometrická	geometrický	k2eAgFnSc1d1
definice	definice	k1gFnSc1
</s>
<s>
V	v	k7c6
eukleidovské	eukleidovský	k2eAgFnSc6d1
geometrii	geometrie	k1gFnSc6
je	být	k5eAaImIp3nS
π	π	k?
definováno	definován	k2eAgNnSc4d1
jako	jako	k8xC,k8xS
poměr	poměr	k1gInSc4
délky	délka	k1gFnSc2
o	o	k7c4
kružnice	kružnice	k1gFnPc4
k	k	k7c3
jejímu	její	k3xOp3gInSc3
průměru	průměr	k1gInSc3
d	d	k?
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
o	o	k7c6
</s>
<s>
d	d	k?
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
o	o	k7c4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
d	d	k?
<g/>
}}	}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Poměr	poměr	k1gInSc1
o	o	k7c6
<g/>
/	/	kIx~
<g/>
d	d	k?
je	být	k5eAaImIp3nS
konstantní	konstantní	k2eAgFnSc1d1
<g/>
,	,	kIx,
nezávisí	záviset	k5eNaImIp3nS
na	na	k7c6
obvodu	obvod	k1gInSc6
kružnice	kružnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
má	mít	k5eAaImIp3nS
například	například	k6eAd1
kružnice	kružnice	k1gFnSc2
dvakrát	dvakrát	k6eAd1
větší	veliký	k2eAgInSc1d2
průměr	průměr	k1gInSc1
než	než	k8xS
druhá	druhý	k4xOgFnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
také	také	k9
dvakrát	dvakrát	k6eAd1
větší	veliký	k2eAgInSc1d2
obvod	obvod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
π	π	k?
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
také	také	k9
definováno	definovat	k5eAaBmNgNnS
jako	jako	k8xS,k8xC
poměr	poměr	k1gInSc1
obsahu	obsah	k1gInSc2
S	s	k7c7
kruhu	kruh	k1gInSc3
ke	k	k7c3
čtverci	čtverec	k1gInSc3
poloměru	poloměr	k1gInSc2
r	r	kA
kružnice	kružnice	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
S	s	k7c7
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
S	s	k7c7
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Tyto	tento	k3xDgFnPc1
definice	definice	k1gFnPc1
závisí	záviset	k5eAaImIp3nP
na	na	k7c6
důsledcích	důsledek	k1gInPc6
eukleidovské	eukleidovský	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
že	že	k8xS
všechny	všechen	k3xTgFnPc4
kružnice	kružnice	k1gFnPc4
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
podobné	podobný	k2eAgFnPc1d1
a	a	k8xC
že	že	k8xS
pravé	pravý	k2eAgFnPc4d1
strany	strana	k1gFnPc4
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
rovnic	rovnice	k1gFnPc2
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
rovné	rovný	k2eAgFnSc2d1
(	(	kIx(
<g/>
resp.	resp.	kA
že	že	k8xS
<g/>
:	:	kIx,
</s>
<s>
S	s	k7c7
</s>
<s>
=	=	kIx~
</s>
<s>
o	o	k7c6
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S	s	k7c7
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
or	or	k?
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
</s>
<s>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
dvě	dva	k4xCgFnPc4
geometrické	geometrický	k2eAgFnPc4d1
definice	definice	k1gFnPc4
mohou	moct	k5eAaImIp3nP
narazit	narazit	k5eAaPmF
na	na	k7c4
problémy	problém	k1gInPc4
v	v	k7c6
oblastech	oblast	k1gFnPc6
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
jindy	jindy	k6eAd1
geometrii	geometrie	k1gFnSc3
nepoužívá	používat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
matematici	matematik	k1gMnPc1
často	často	k6eAd1
dávají	dávat	k5eAaImIp3nP
přednost	přednost	k1gFnSc4
definici	definice	k1gFnSc3
π	π	k?
bez	bez	k7c2
geometrie	geometrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
k	k	k7c3
tomu	ten	k3xDgMnSc3
matematickou	matematický	k2eAgFnSc4d1
analýzu	analýza	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	s	k7c7
π	π	k?
definuje	definovat	k5eAaBmIp3nS
jako	jako	k9
dvojnásobek	dvojnásobek	k1gInSc1
nejmenší	malý	k2eAgFnSc2d3
kladné	kladný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
x	x	k?
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
je	být	k5eAaImIp3nS
goniometrická	goniometrický	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
cos	cos	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
rovna	roven	k2eAgFnSc1d1
nule	nula	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obvod	obvod	k1gInSc1
=	=	kIx~
π	π	k?
×	×	k?
průměrObsah	průměrObsah	k1gInSc1
kruhu	kruh	k1gInSc2
je	být	k5eAaImIp3nS
π	π	k?
×	×	k?
obsah	obsah	k1gInSc1
šedého	šedý	k2eAgInSc2d1
čtverceProtože	čtverceProtožit	k5eAaImSgInS
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
transcendentní	transcendentní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
nelze	lze	k6eNd1
přes	přes	k7c4
eukleidovskou	eukleidovský	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
provést	provést	k5eAaPmF
kvadraturu	kvadratura	k1gFnSc4
kruhu	kruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Iracionálnost	iracionálnost	k1gFnSc1
a	a	k8xC
transcendentnost	transcendentnost	k1gFnSc1
</s>
<s>
π	π	k?
je	být	k5eAaImIp3nS
iracionální	iracionální	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gInSc4
nelze	lze	k6eNd1
vyjádřit	vyjádřit	k5eAaPmF
podílem	podíl	k1gInSc7
dvou	dva	k4xCgNnPc2
celých	celý	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
také	také	k9
transcendentní	transcendentní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
neexistuje	existovat	k5eNaImIp3nS
polynom	polynom	k1gInSc4
s	s	k7c7
racionálními	racionální	k2eAgInPc7d1
koeficienty	koeficient	k1gInPc7
<g/>
,	,	kIx,
pro	pro	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
by	by	kYmCp3nP
π	π	k?
bylo	být	k5eAaImAgNnS
kořenem	kořen	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Jedním	jeden	k4xCgInSc7
z	z	k7c2
důsledků	důsledek	k1gInPc2
transcendentnosti	transcendentnost	k1gFnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
π	π	k?
nelze	lze	k6eNd1
zkonstruovat	zkonstruovat	k5eAaPmF
kružítkem	kružítko	k1gNnSc7
a	a	k8xC
pravítkem	pravítko	k1gNnSc7
(	(	kIx(
<g/>
euklidovsky	euklidovsky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
souřadnice	souřadnice	k1gFnSc1
všech	všecek	k3xTgInPc2
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
konstruovány	konstruován	k2eAgInPc1d1
eukleidovskou	eukleidovský	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
konstruovatelná	konstruovatelný	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
nelze	lze	k6eNd1
např.	např.	kA
provést	provést	k5eAaPmF
kvadraturu	kvadratura	k1gFnSc4
kruhu	kruh	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pouze	pouze	k6eAd1
pomocí	pomocí	k7c2
kružítka	kružítko	k1gNnSc2
a	a	k8xC
pravítka	pravítko	k1gNnSc2
nelze	lze	k6eNd1
zkonstruovat	zkonstruovat	k5eAaPmF
čtverec	čtverec	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
obsah	obsah	k1gInSc1
je	být	k5eAaImIp3nS
stejný	stejný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
obsah	obsah	k1gInSc1
daného	daný	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
kvadratura	kvadratura	k1gFnSc1
kruhu	kruh	k1gInSc2
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
tří	tři	k4xCgInPc2
geometrických	geometrický	k2eAgInPc2d1
problémů	problém	k1gInPc2
pocházejících	pocházející	k2eAgInPc2d1
už	už	k6eAd1
z	z	k7c2
antického	antický	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyjádření	vyjádření	k1gNnSc1
v	v	k7c6
desítkové	desítkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
</s>
<s>
Prvních	první	k4xOgFnPc2
50	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
π	π	k?
v	v	k7c6
desítkové	desítkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
=	=	kIx~
3,141	3,141	k4
<g/>
59	#num#	k4
26535	#num#	k4
89793	#num#	k4
23846	#num#	k4
26433	#num#	k4
83279	#num#	k4
50288	#num#	k4
41971	#num#	k4
69399	#num#	k4
37510	#num#	k4
<g/>
…	…	k?
</s>
<s>
Na	na	k7c6
různých	různý	k2eAgFnPc6d1
internetových	internetový	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
číslic	číslice	k1gFnPc2
čísla	číslo	k1gNnSc2
π	π	k?
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
Emma	Emma	k1gFnSc1
Harukaová	Harukaová	k1gFnSc1
Iwaová	Iwaová	k1gFnSc1
představila	představit	k5eAaPmAgFnS
hodnotu	hodnota	k1gFnSc4
π	π	k?
na	na	k7c4
31,416	31,416	k4
bilionů	bilion	k4xCgInPc2
desetinných	desetinný	k2eAgFnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpočet	výpočet	k1gInSc1
trval	trvat	k5eAaImAgInS
121	#num#	k4
dní	den	k1gInPc2
na	na	k7c6
25	#num#	k4
počítačích	počítač	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
Tento	tento	k3xDgInSc1
počin	počin	k1gInSc1
je	být	k5eAaImIp3nS
zapsán	zapsat	k5eAaPmNgInS
v	v	k7c6
Guinnessově	Guinnessův	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
rekordů	rekord	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
když	když	k8xS
bylo	být	k5eAaImAgNnS
π	π	k?
spočítáno	spočítán	k2eAgNnSc4d1
na	na	k7c6
více	hodně	k6eAd2
než	než	k8xS
bilion	bilion	k4xCgInSc4
(	(	kIx(
<g/>
1012	#num#	k4
<g/>
)	)	kIx)
číslic	číslice	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
aplikované	aplikovaný	k2eAgFnSc6d1
matematice	matematika	k1gFnSc6
se	se	k3xPyFc4
většinou	většinou	k6eAd1
používá	používat	k5eAaImIp3nS
zaokrouhlení	zaokrouhlení	k1gNnSc4
pouze	pouze	k6eAd1
na	na	k7c4
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
11	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
π	π	k?
například	například	k6eAd1
stačí	stačit	k5eAaBmIp3nS
na	na	k7c4
odhad	odhad	k1gInSc4
délky	délka	k1gFnSc2
kružnice	kružnice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Země	země	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
chybou	chyba	k1gFnSc7
menší	malý	k2eAgFnSc7d2
než	než	k8xS
jeden	jeden	k4xCgInSc4
milimetr	milimetr	k1gInSc4
a	a	k8xC
39	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
stačí	stačit	k5eAaBmIp3nS
na	na	k7c4
jakoukoli	jakýkoli	k3yIgFnSc4
představitelnou	představitelný	k2eAgFnSc4d1
aplikaci	aplikace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
NASA	NASA	kA
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
výpočtech	výpočet	k1gInPc6
používá	používat	k5eAaImIp3nS
15	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Protože	protože	k8xS
π	π	k?
je	být	k5eAaImIp3nS
iracionální	iracionální	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
číslice	číslice	k1gFnSc1
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
desetinném	desetinný	k2eAgInSc6d1
rozvoji	rozvoj	k1gInSc6
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nezačnou	začít	k5eNaPmIp3nP
opakovat	opakovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sled	sled	k1gInSc1
těchto	tento	k3xDgFnPc2
číslic	číslice	k1gFnPc2
fascinuje	fascinovat	k5eAaBmIp3nS
matematiky	matematik	k1gMnPc4
i	i	k8xC
laiky	laik	k1gMnPc4
a	a	k8xC
během	během	k7c2
posledních	poslední	k2eAgNnPc2d1
pár	pár	k4xCyI
století	století	k1gNnPc2
se	se	k3xPyFc4
vkládají	vkládat	k5eAaImIp3nP
snahy	snaha	k1gFnPc1
do	do	k7c2
vypočítání	vypočítání	k1gNnSc2
více	hodně	k6eAd2
číslic	číslice	k1gFnPc2
π	π	k?
a	a	k8xC
zkoumání	zkoumání	k1gNnSc3
jeho	jeho	k3xOp3gFnPc2
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Zatím	zatím	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
nepodařilo	podařit	k5eNaPmAgNnS
najít	najít	k5eAaPmF
žádný	žádný	k3yNgInSc4
vzor	vzor	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yQgNnSc2,k3yIgNnSc2,k3yRgNnSc2
by	by	kYmCp3nP
se	se	k3xPyFc4
číslice	číslice	k1gFnPc1
opakovaly	opakovat	k5eAaImAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odhad	odhad	k1gInSc1
π	π	k?
</s>
<s>
Číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Aproximace	aproximace	k1gFnSc1
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
Dvojková	dvojkový	k2eAgFnSc1d1
</s>
<s>
11,001	11,001	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
10000	#num#	k4
11111	#num#	k4
10110	#num#	k4
<g/>
…	…	k?
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osmičková	osmičkový	k2eAgFnSc1d1
</s>
<s>
3,110	3,110	k4
<g/>
37	#num#	k4
55242	#num#	k4
10264	#num#	k4
30215	#num#	k4
<g/>
…	…	k?
</s>
<s>
Desítková	desítkový	k2eAgFnSc1d1
</s>
<s>
3,141	3,141	k4
<g/>
59	#num#	k4
26535	#num#	k4
89793	#num#	k4
23846	#num#	k4
26433	#num#	k4
83279	#num#	k4
50288	#num#	k4
<g/>
…	…	k?
</s>
<s>
Šestnáctková	šestnáctkový	k2eAgFnSc1d1
</s>
<s>
3,243	3,243	k4
<g/>
F	F	kA
<g/>
6	#num#	k4
A8885	A8885	k1gFnSc1
A308D	A308D	k1gFnSc1
31319	#num#	k4
<g/>
…	…	k?
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Racionální	racionální	k2eAgFnSc1d1
aproximace	aproximace	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
⁄	⁄	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
333	#num#	k4
<g/>
⁄	⁄	k?
<g/>
106	#num#	k4
<g/>
,	,	kIx,
355	#num#	k4
<g/>
⁄	⁄	k?
<g/>
113	#num#	k4
<g/>
,	,	kIx,
103993	#num#	k4
<g/>
⁄	⁄	k?
<g/>
33102	#num#	k4
<g/>
,	,	kIx,
…	…	k?
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
seřazeno	seřadit	k5eAaPmNgNnS
od	od	k7c2
méně	málo	k6eAd2
přesné	přesný	k2eAgFnSc2d1
k	k	k7c3
přesnější	přesný	k2eAgFnSc3d2
aproximaci	aproximace	k1gFnSc3
<g/>
)	)	kIx)
</s>
<s>
Řetězový	řetězový	k2eAgInSc1d1
zlomek	zlomek	k1gInSc1
</s>
<s>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
;	;	kIx,
<g/>
7,15	7,15	k4
<g/>
,1	,1	k4
<g/>
,292	,292	k4
<g/>
,1	,1	k4
<g/>
,1	,1	k4
<g/>
,1	,1	k4
<g/>
,2	,2	k4
<g/>
,1	,1	k4
<g/>
,3	,3	k4
<g/>
,1	,1	k4
<g/>
,14	,14	k4
<g/>
,2	,2	k4
<g/>
,1	,1	k4
<g/>
,1	,1	k4
<g/>
…	…	k?
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
Tento	tento	k3xDgInSc1
zlomek	zlomek	k1gInSc1
není	být	k5eNaImIp3nS
periodický	periodický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zobrazeno	zobrazit	k5eAaPmNgNnS
v	v	k7c6
lineárním	lineární	k2eAgInSc6d1
zápisu	zápis	k1gInSc6
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
1415926535	#num#	k4
8979323846	#num#	k4
2643383279	#num#	k4
5028841971	#num#	k4
6939937510	#num#	k4
5820974944	#num#	k4
5923078164	#num#	k4
0628620899	#num#	k4
8628034825	#num#	k4
3421170679	#num#	k4
8214808651	#num#	k4
3282306647	#num#	k4
0938446095	#num#	k4
5058223172	#num#	k4
5359408128	#num#	k4
4811174502	#num#	k4
8410270193	#num#	k4
8521105559	#num#	k4
6446229489	#num#	k4
5493038196	#num#	k4
4428810975	#num#	k4
6659334461	#num#	k4
2847564823	#num#	k4
3786783165	#num#	k4
2712019091	#num#	k4
4564856692	#num#	k4
3460348610	#num#	k4
4543266482	#num#	k4
1339360726	#num#	k4
0249141273	#num#	k4
7245870066	#num#	k4
0631558817	#num#	k4
4881520920	#num#	k4
9628292540	#num#	k4
9171536436	#num#	k4
7892590360	#num#	k4
0113305305	#num#	k4
4882046652	#num#	k4
1384146951	#num#	k4
9415116094	#num#	k4
3305727036	#num#	k4
5759591953	#num#	k4
0921861173	#num#	k4
8193261179	#num#	k4
3105118548	#num#	k4
0744623799	#num#	k4
6274956735	#num#	k4
1885752724	#num#	k4
8912279381	#num#	k4
8301194912	#num#	k4
9833673362	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
4406566430	#num#	k4
8602139494	#num#	k4
6395224737	#num#	k4
1907021798	#num#	k4
6094370277	#num#	k4
0539217176	#num#	k4
2931767523	#num#	k4
8467481846	#num#	k4
7669405132	#num#	k4
0005681271	#num#	k4
4526356082	#num#	k4
7785771342	#num#	k4
7577896091	#num#	k4
7363717872	#num#	k4
1468440901	#num#	k4
2249534301	#num#	k4
4654958537	#num#	k4
1050792279	#num#	k4
6892589235	#num#	k4
4201995611	#num#	k4
2129021960	#num#	k4
8640344181	#num#	k4
5981362977	#num#	k4
4771309960	#num#	k4
5187072113	#num#	k4
4999999837	#num#	k4
2978049951	#num#	k4
0597317328	#num#	k4
1609631859	#num#	k4
5024459455	#num#	k4
3469083026	#num#	k4
4252230825	#num#	k4
3344685035	#num#	k4
2619311881	#num#	k4
7101000313	#num#	k4
7838752886	#num#	k4
5875332083	#num#	k4
8142061717	#num#	k4
7669147303	#num#	k4
5982534904	#num#	k4
2875546873	#num#	k4
1159562863	#num#	k4
8823537875	#num#	k4
9375195778	#num#	k4
1857780532	#num#	k4
1712268066	#num#	k4
1300192787	#num#	k4
6611195909	#num#	k4
2164201989	#num#	k4
3809525720	#num#	k4
1065485863	#num#	k4
2788659361	#num#	k4
5338182796	#num#	k4
8230301952	#num#	k4
0353018529	#num#	k4
6899577362	#num#	k4
2599413891	#num#	k4
2497217752	#num#	k4
8347913151	#num#	k4
5574857242	#num#	k4
4541506959	#num#	k4
</s>
<s>
Prvních	první	k4xOgFnPc2
1120	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
čísla	číslo	k1gNnSc2
π	π	k?
správně	správně	k6eAd1
určili	určit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
John	John	k1gMnSc1
Wrench	Wrench	k1gMnSc1
a	a	k8xC
Levi	Levi	k1gNnSc1
Smith	Smitha	k1gFnPc2
pomocí	pomocí	k7c2
kalkulačky	kalkulačka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
nejpřesnější	přesný	k2eAgInSc1d3
odhad	odhad	k1gInSc1
π	π	k?
před	před	k7c7
příchodem	příchod	k1gInSc7
počítačů	počítač	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejhrubější	hrubý	k2eAgInSc1d3
odhad	odhad	k1gInSc1
π	π	k?
je	být	k5eAaImIp3nS
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
hodnota	hodnota	k1gFnSc1
může	moct	k5eAaImIp3nS
stačit	stačit	k5eAaBmF
u	u	k7c2
případů	případ	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
není	být	k5eNaImIp3nS
potřeba	potřeba	k1gFnSc1
velké	velký	k2eAgFnSc2d1
přesnosti	přesnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
poměr	poměr	k1gInSc1
obvodu	obvod	k1gInSc2
vepsaného	vepsaný	k2eAgInSc2d1
pravidelného	pravidelný	k2eAgInSc2d1
šestiúhelníku	šestiúhelník	k1gInSc2
k	k	k7c3
průměru	průměr	k1gInSc3
kružnice	kružnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
π	π	k?
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
odhadnout	odhadnout	k5eAaPmF
narýsováním	narýsování	k1gNnSc7
kružnice	kružnice	k1gFnSc2
<g/>
,	,	kIx,
změřením	změření	k1gNnSc7
jejího	její	k3xOp3gInSc2
průměru	průměr	k1gInSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
délky	délka	k1gFnSc2
a	a	k8xC
následným	následný	k2eAgNnSc7d1
vydělením	vydělení	k1gNnSc7
délky	délka	k1gFnSc2
průměrem	průměr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
navrhl	navrhnout	k5eAaPmAgInS
Archimédés	Archimédés	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
spočítat	spočítat	k5eAaPmF
obvod	obvod	k1gInSc1
on	on	k3xPp3gMnSc1
pravidelného	pravidelný	k2eAgInSc2d1
mnohoúhelníku	mnohoúhelník	k1gInSc2
s	s	k7c7
n	n	k0
stranami	strana	k1gFnPc7
s	s	k7c7
vepsanou	vepsaný	k2eAgFnSc7d1
kružnicí	kružnice	k1gFnSc7
o	o	k7c6
průměru	průměr	k1gInSc6
d.	d.	k?
Potom	potom	k8xC
lze	lze	k6eAd1
vytvořit	vytvořit	k5eAaPmF
limitu	limita	k1gFnSc4
posloupnosti	posloupnost	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
n	n	k0
přibližuje	přibližovat	k5eAaImIp3nS
nekonečnu	nekonečno	k1gNnSc6
<g/>
:	:	kIx,
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
lim	limo	k1gNnPc2
</s>
<s>
n	n	k0
</s>
<s>
→	→	k?
</s>
<s>
∞	∞	k?
</s>
<s>
o	o	k7c6
</s>
<s>
n	n	k0
</s>
<s>
d	d	k?
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
\	\	kIx~
<g/>
lim	limo	k1gNnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
infty	inft	k1gInPc4
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
o_	o_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
d	d	k?
<g/>
}}	}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
více	hodně	k6eAd2
má	mít	k5eAaImIp3nS
mnohoúhelník	mnohoúhelník	k1gInSc1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
menší	malý	k2eAgFnSc1d2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
největší	veliký	k2eAgFnSc1d3
vzdálenost	vzdálenost	k1gFnSc1
od	od	k7c2
kružnice	kružnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archimédés	Archimédés	k1gInSc1
určil	určit	k5eAaPmAgInS
přesnost	přesnost	k1gFnSc4
tohoto	tento	k3xDgInSc2
způsobu	způsob	k1gInSc2
porovnáním	porovnání	k1gNnSc7
obvodu	obvod	k1gInSc2
mnohoúhelníku	mnohoúhelník	k1gInSc2
opsaného	opsaný	k2eAgInSc2d1
kružnici	kružnice	k1gFnSc3
s	s	k7c7
obvodem	obvod	k1gInSc7
mnohoúhelníku	mnohoúhelník	k1gInSc2
se	s	k7c7
stejným	stejný	k2eAgInSc7d1
počtem	počet	k1gInSc7
stran	strana	k1gFnPc2
vepsaného	vepsaný	k2eAgInSc2d1
kružnici	kružnice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
použitím	použití	k1gNnSc7
mnohoúhelníku	mnohoúhelník	k1gInSc2
s	s	k7c7
96	#num#	k4
stranami	strana	k1gFnPc7
spočítal	spočítat	k5eAaPmAgInS
rozsah	rozsah	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
π	π	k?
leží	ležet	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
10	#num#	k4
</s>
<s>
71	#num#	k4
</s>
<s>
<	<	kIx(
</s>
<s>
π	π	k?
</s>
<s>
<	<	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
10	#num#	k4
</s>
<s>
70	#num#	k4
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
3	#num#	k4
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
tfrac	tfrac	k1gInSc1
{	{	kIx(
<g/>
10	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
71	#num#	k4
<g/>
}}	}}	k?
<g/>
<	<	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
<	<	kIx(
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
tfrac	tfrac	k1gInSc1
{	{	kIx(
<g/>
10	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
70	#num#	k4
<g/>
}}	}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
lze	lze	k6eAd1
také	také	k9
spočítat	spočítat	k5eAaPmF
čistě	čistě	k6eAd1
matematickými	matematický	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
je	být	k5eAaImIp3nS
transcendentní	transcendentní	k2eAgNnSc1d1
<g/>
,	,	kIx,
nelze	lze	k6eNd1
ho	on	k3xPp3gMnSc4
vyjádřit	vyjádřit	k5eAaPmF
pomocí	pomoc	k1gFnSc7
algebraické	algebraický	k2eAgFnSc2d1
rovnice	rovnice	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
vyskytovaly	vyskytovat	k5eAaImAgInP
jen	jen	k9
racionální	racionální	k2eAgInPc1d1
koeficienty	koeficient	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Vyjádření	vyjádření	k1gNnSc1
pomocí	pomocí	k7c2
elementární	elementární	k2eAgFnSc2d1
aritmetiky	aritmetika	k1gFnSc2
často	často	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
řady	řada	k1gFnPc1
nebo	nebo	k8xC
sumační	sumační	k2eAgNnSc1d1
značení	značení	k1gNnSc1
(	(	kIx(
<g/>
např.	např.	kA
„	„	k?
<g/>
…	…	k?
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vzorec	vzorec	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
vzorcem	vzorec	k1gInSc7
pro	pro	k7c4
nekonečnou	konečný	k2eNgFnSc4d1
řadu	řada	k1gFnSc4
aproximací	aproximace	k1gFnPc2
π	π	k?
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
více	hodně	k6eAd2
prvků	prvek	k1gInPc2
sumace	sumace	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
přesnější	přesný	k2eAgFnSc1d2
bude	být	k5eAaImBp3nS
odhad	odhad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vzorce	vzorec	k1gInPc1
k	k	k7c3
vypočítání	vypočítání	k1gNnSc3
π	π	k?
mají	mít	k5eAaImIp3nP
požadované	požadovaný	k2eAgFnPc4d1
matematické	matematický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
těžko	těžko	k6eAd1
pochopitelné	pochopitelný	k2eAgInPc1d1
bez	bez	k7c2
znalosti	znalost	k1gFnSc2
trigonometrie	trigonometrie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgNnSc1
jsou	být	k5eAaImIp3nP
ale	ale	k9
jednodušší	jednoduchý	k2eAgInPc1d2
<g/>
,	,	kIx,
například	například	k6eAd1
Leibnizova	Leibnizův	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
k	k	k7c3
</s>
<s>
2	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
⋯	⋯	k?
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
4	#num#	k4
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
^	^	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
9	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
cdots	cdots	k6eAd1
.	.	kIx.
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
I	i	k9
když	když	k8xS
je	být	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
řadu	řada	k1gFnSc4
jednoduché	jednoduchý	k2eAgNnSc1d1
napsat	napsat	k5eAaPmF,k5eAaBmF
a	a	k8xC
spočítat	spočítat	k5eAaPmF
<g/>
,	,	kIx,
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
zpočátku	zpočátku	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
proč	proč	k6eAd1
je	být	k5eAaImIp3nS
její	její	k3xOp3gInSc1
výsledek	výsledek	k1gInSc1
π	π	k?
Navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
zdlouhavá	zdlouhavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
téměř	téměř	k6eAd1
300	#num#	k4
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyšla	vyjít	k5eAaPmAgFnS
správně	správně	k6eAd1
první	první	k4xOgNnPc1
dvě	dva	k4xCgNnPc1
desetinná	desetinný	k2eAgNnPc1d1
místa	místo	k1gNnPc1
π	π	k?
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
se	se	k3xPyFc4
ale	ale	k9
řada	řada	k1gFnSc1
upraví	upravit	k5eAaPmIp3nS
<g/>
,	,	kIx,
lze	lze	k6eAd1
π	π	k?
počítat	počítat	k5eAaImF
mnohem	mnohem	k6eAd1
rychleji	rychle	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vezměme	vzít	k5eAaPmRp1nP
si	se	k3xPyFc3
posloupnost	posloupnost	k1gFnSc4
</s>
<s>
π	π	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
π	π	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
π	π	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
3	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
π	π	k?
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
4	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
⋯	⋯	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0,1	0,1	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
,	,	kIx,
<g/>
\	\	kIx~
\	\	kIx~
<g/>
pi	pi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0,2	0,2	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
,	,	kIx,
<g/>
\	\	kIx~
\	\	kIx~
<g/>
pi	pi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0,3	0,3	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
,	,	kIx,
<g/>
\	\	kIx~
\	\	kIx~
<g/>
pi	pi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0,4	0,4	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
}}	}}	k?
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
cdots	cdots	k6eAd1
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
poté	poté	k6eAd1
definujme	definovat	k5eAaBmRp1nP
</s>
<s>
π	π	k?
</s>
<s>
i	i	k9
</s>
<s>
,	,	kIx,
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
π	π	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
j	j	k?
</s>
<s>
+	+	kIx~
</s>
<s>
π	π	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
j	j	k?
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
pro	pro	k7c4
</s>
<s>
i	i	k9
</s>
<s>
,	,	kIx,
</s>
<s>
j	j	k?
</s>
<s>
≥	≥	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
,	,	kIx,
<g/>
j	j	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
i-	i-	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
<g/>
j	j	k?
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
pi	pi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
i-	i-	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
<g/>
j	j	k?
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
text	text	k1gInSc1
<g/>
{	{	kIx(
pro	pro	k7c4
}}	}}	k?
<g/>
i	i	k9
<g/>
,	,	kIx,
<g/>
j	j	k?
<g/>
\	\	kIx~
<g/>
geq	geq	k?
1	#num#	k4
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Poté	poté	k6eAd1
výpočet	výpočet	k1gInSc1
</s>
<s>
π	π	k?
</s>
<s>
10	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
10	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
10,10	10,10	k4
<g/>
}}	}}	k?
</s>
<s>
zabere	zabrat	k5eAaPmIp3nS
stejně	stejně	k6eAd1
času	čas	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
výpočet	výpočet	k1gInSc1
150	#num#	k4
prvků	prvek	k1gInPc2
původní	původní	k2eAgFnSc2d1
řady	řada	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
</s>
<s>
π	π	k?
</s>
<s>
10	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
10	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
3.141	3.141	k4
<g/>
592653	#num#	k4
</s>
<s>
…	…	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
10,10	10,10	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
3.141	3.141	k4
<g/>
592653	#num#	k4
<g/>
\	\	kIx~
<g/>
ldots	ldots	k6eAd1
}	}	kIx)
</s>
<s>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
správnost	správnost	k1gFnSc4
na	na	k7c4
9	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
úprava	úprava	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
van	van	k1gInSc1
Wijngaardenova	Wijngaardenův	k2eAgFnSc1d1
transformace	transformace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aproximace	aproximace	k1gFnSc1
355	#num#	k4
<g/>
/	/	kIx~
<g/>
113	#num#	k4
(	(	kIx(
<g/>
3,141	3,141	k4
<g/>
5929	#num#	k4
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
dává	dávat	k5eAaImIp3nS
správně	správně	k6eAd1
prvních	první	k4xOgNnPc6
7	#num#	k4
číslic	číslice	k1gFnPc2
π	π	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
získat	získat	k5eAaPmF
ze	z	k7c2
řetězového	řetězový	k2eAgInSc2d1
zlomku	zlomek	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejlepší	dobrý	k2eAgFnSc1d3
aproximace	aproximace	k1gFnSc1
π	π	k?
vyjádřená	vyjádřený	k2eAgFnSc1d1
zlomkem	zlomek	k1gInSc7
s	s	k7c7
maximálně	maximálně	k6eAd1
čtyřcifernými	čtyřciferný	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
<g/>
;	;	kIx,
další	další	k2eAgNnSc1d1
lepší	lepší	k1gNnSc1
aproximace	aproximace	k1gFnSc2
je	být	k5eAaImIp3nS
až	až	k9
52163	#num#	k4
<g/>
/	/	kIx~
<g/>
16604	#num#	k4
(	(	kIx(
<g/>
3,141	3,141	k4
<g/>
592387	#num#	k4
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Starověk	starověk	k1gInSc1
</s>
<s>
O	o	k7c6
znalosti	znalost	k1gFnSc6
π	π	k?
se	se	k3xPyFc4
spekuluje	spekulovat	k5eAaImIp3nS
již	již	k9
u	u	k7c2
starověkých	starověký	k2eAgMnPc2d1
Egypťanů	Egypťan	k1gMnPc2
v	v	k7c6
období	období	k1gNnSc6
Staré	Staré	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
základě	základ	k1gInSc6
rozměrů	rozměr	k1gInPc2
pyramid	pyramid	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgFnPc6,k3yQgFnPc6,k3yRgFnPc6
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
zakódováno	zakódovat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
pyramida	pyramida	k1gFnSc1
v	v	k7c6
Gíze	Gíze	k1gFnSc6
zkonstruovaná	zkonstruovaný	k2eAgFnSc1d1
někdy	někdy	k6eAd1
mezi	mezi	k7c7
lety	let	k1gInPc7
2589	#num#	k4
<g/>
–	–	k?
<g/>
2566	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
s	s	k7c7
obvodem	obvod	k1gInSc7
1760	#num#	k4
loktů	loket	k1gInPc2
a	a	k8xC
s	s	k7c7
výškou	výška	k1gFnSc7
280	#num#	k4
loktů	loket	k1gInPc2
<g/>
;	;	kIx,
poměr	poměr	k1gInSc1
1760	#num#	k4
<g/>
/	/	kIx~
<g/>
280	#num#	k4
=	=	kIx~
44	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
≈	≈	k?
2	#num#	k4
<g/>
π	π	k?
Stejné	stejný	k2eAgFnPc1d1
proporce	proporce	k1gFnPc1
byly	být	k5eAaImAgFnP
zvoleny	zvolit	k5eAaPmNgFnP
při	při	k7c6
dřívější	dřívější	k2eAgFnSc6d1
stavbě	stavba	k1gFnSc6
pyramidy	pyramida	k1gFnSc2
Meidum	Meidum	k1gInSc1
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
2613	#num#	k4
<g/>
–	–	k?
<g/>
2589	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
egyptologové	egyptolog	k1gMnPc1
to	ten	k3xDgNnSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
záměr	záměr	k1gInSc4
architektů	architekt	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miroslav	Miroslav	k1gMnSc1
Verner	Verner	k1gMnSc1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Můžeme	moct	k5eAaImIp1nP
usoudit	usoudit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
když	když	k8xS
Egypťané	Egypťan	k1gMnPc1
neuměli	umět	k5eNaImAgMnP
přesně	přesně	k6eAd1
určit	určit	k5eAaPmF
hodnotu	hodnota	k1gFnSc4
π	π	k?
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
ho	on	k3xPp3gInSc4
používali	používat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Podobný	podobný	k2eAgInSc1d1
názor	názor	k1gInSc1
zastával	zastávat	k5eAaImAgInS
i	i	k9
William	William	k1gInSc1
Flinders	Flindersa	k1gFnPc2
Petrie	Petrie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Jiní	jiný	k1gMnPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
starověcí	starověký	k2eAgMnPc1d1
Egypťané	Egypťan	k1gMnPc1
o	o	k7c6
pí	pí	k1gNnSc6
nevěděli	vědět	k5eNaImAgMnP
a	a	k8xC
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
staveb	stavba	k1gFnPc2
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
tudíž	tudíž	k8xC
nesnažili	snažit	k5eNaImAgMnP
promítnout	promítnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pyramidy	pyramida	k1gFnPc4
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
stavěny	stavit	k5eAaImNgFnP
jednoduše	jednoduše	k6eAd1
podle	podle	k7c2
poměrů	poměr	k1gInPc2
stran	strana	k1gFnPc2
pravoúhlých	pravoúhlý	k2eAgInPc2d1
trojúhelníků	trojúhelník	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
vysvětlení	vysvětlení	k1gNnSc1
výskytu	výskyt	k1gInSc2
čísla	číslo	k1gNnSc2
pí	pí	k1gNnSc2
jako	jako	k8xC,k8xS
důsledku	důsledek	k1gInSc2
použité	použitý	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
měření	měření	k1gNnSc2
(	(	kIx(
<g/>
vytyčování	vytyčování	k1gNnSc1
vzdáleností	vzdálenost	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délkové	délkový	k2eAgFnPc4d1
míry	míra	k1gFnPc4
se	se	k3xPyFc4
měřily	měřit	k5eAaImAgFnP
odvalením	odvalení	k1gNnSc7
kola	kolo	k1gNnSc2
<g/>
,	,	kIx,
výškové	výškový	k2eAgFnSc2d1
pak	pak	k6eAd1
jeho	jeho	k3xOp3gNnSc7
přikládáním	přikládání	k1gNnSc7
na	na	k7c4
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
dojde	dojít	k5eAaPmIp3nS
automaticky	automaticky	k6eAd1
k	k	k7c3
promítnutí	promítnutí	k1gNnSc3
čísla	číslo	k1gNnSc2
pí	pí	k1gNnSc7
do	do	k7c2
poměru	poměr	k1gInSc2
výšky	výška	k1gFnSc2
a	a	k8xC
strany	strana	k1gFnSc2
pyramidy	pyramida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pyramida	pyramida	k1gFnSc1
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
stavěna	stavěn	k2eAgFnSc1d1
s	s	k7c7
jednoduchými	jednoduchý	k2eAgFnPc7d1
mírami	míra	k1gFnPc7,k1gFnPc7wR
<g/>
:	:	kIx,
280	#num#	k4
„	„	k?
<g/>
výškových	výškový	k2eAgInPc2d1
loktů	loket	k1gInPc2
<g/>
“	“	k?
a	a	k8xC
280	#num#	k4
„	„	k?
<g/>
délkových	délkový	k2eAgInPc2d1
loktů	loket	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3
písemně	písemně	k6eAd1
doložené	doložený	k2eAgInPc4d1
odhady	odhad	k1gInPc4
π	π	k?
se	se	k3xPyFc4
datují	datovat	k5eAaImIp3nP
do	do	k7c2
doby	doba	k1gFnSc2
okolo	okolo	k7c2
1900	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
;	;	kIx,
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
256	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
(	(	kIx(
<g/>
Egypt	Egypt	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
25	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
(	(	kIx(
<g/>
Babylon	Babylon	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
méně	málo	k6eAd2
než	než	k8xS
1	#num#	k4
%	%	kIx~
vzdálené	vzdálený	k2eAgInPc1d1
od	od	k7c2
skutečné	skutečný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Indický	indický	k2eAgInSc1d1
text	text	k1gInSc1
Šatapatha	Šatapath	k1gMnSc2
Brahmana	Brahman	k1gMnSc2
dává	dávat	k5eAaImIp3nS
odhad	odhad	k1gInSc1
339	#num#	k4
<g/>
/	/	kIx~
<g/>
108	#num#	k4
≈	≈	k?
3,139	3,139	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pasáže	pasáž	k1gFnSc2
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
knize	kniha	k1gFnSc6
královské	královský	k2eAgNnSc1d1
7	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
knize	kniha	k1gFnSc6
kronik	kronika	k1gFnPc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
obřadním	obřadní	k2eAgInSc6d1
bazénu	bazén	k1gInSc6
v	v	k7c6
paláci	palác	k1gInSc6
krále	král	k1gMnSc2
Šalomouna	Šalomoun	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
má	mít	k5eAaImIp3nS
průměr	průměr	k1gInSc4
deset	deset	k4xCc4
loktů	loket	k1gInPc2
a	a	k8xC
obvod	obvod	k1gInSc1
třicet	třicet	k4xCc1
loktů	loket	k1gInPc2
<g/>
;	;	kIx,
někteří	některý	k3yIgMnPc1
z	z	k7c2
toho	ten	k3xDgNnSc2
usuzují	usuzovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
autoři	autor	k1gMnPc1
přisuzovali	přisuzovat	k5eAaImAgMnP
pí	pí	k1gNnSc4
hodnotu	hodnota	k1gFnSc4
okolo	okolo	k7c2
tří	tři	k4xCgNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
jiní	jiný	k2eAgMnPc1d1
se	se	k3xPyFc4
to	ten	k3xDgNnSc4
snaží	snažit	k5eAaImIp3nS
vysvětlit	vysvětlit	k5eAaPmF
šestiúhelníkovým	šestiúhelníkový	k2eAgInSc7d1
bazénem	bazén	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Archimédés	Archimédés	k1gInSc1
používal	používat	k5eAaImAgInS
k	k	k7c3
odhadu	odhad	k1gInSc3
π	π	k?
metodu	metoda	k1gFnSc4
vyčerpání	vyčerpání	k1gNnSc2
</s>
<s>
Archimédés	Archimédés	k1gInSc1
(	(	kIx(
<g/>
287	#num#	k4
<g/>
–	–	k?
<g/>
212	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
první	první	k4xOgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
odhadl	odhadnout	k5eAaPmAgInS
π	π	k?
důsledně	důsledně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvědomil	uvědomit	k5eAaPmAgInS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
hodnota	hodnota	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ohraničena	ohraničit	k5eAaPmNgFnS
shora	shora	k6eAd1
i	i	k8xC
zespoda	zespoda	k7c2
vepsáním	vepsání	k1gNnSc7
a	a	k8xC
opsáním	opsání	k1gNnSc7
pravidelných	pravidelný	k2eAgInPc2d1
mnohoúhelníků	mnohoúhelník	k1gInPc2
do	do	k7c2
kružnice	kružnice	k1gFnSc2
a	a	k8xC
vypočtením	vypočtení	k1gNnSc7
jejich	jejich	k3xOp3gInPc2
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použitím	použití	k1gNnSc7
96	#num#	k4
<g/>
úhelníků	úhelník	k1gInPc2
dokázal	dokázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
223	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
<	<	kIx(
π	π	k?
<	<	kIx(
220	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměr	průměr	k1gInSc1
těchto	tento	k3xDgFnPc2
hodnot	hodnota	k1gFnPc2
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
3,141	3,141	k4
<g/>
85	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ptolemaios	Ptolemaios	k1gMnSc1
udává	udávat	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
Almagestu	Almagest	k1gInSc6
hodnotu	hodnota	k1gFnSc4
3,141	3,141	k4
<g/>
67	#num#	k4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
možná	možná	k9
získal	získat	k5eAaPmAgMnS
od	od	k7c2
Apollónia	Apollónium	k1gNnSc2
z	z	k7c2
Pergy	Perga	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Okolo	okolo	k7c2
roku	rok	k1gInSc2
265	#num#	k4
poskytl	poskytnout	k5eAaPmAgMnS
Liou	Lio	k1gMnSc3
Chuej	Chuej	k1gInSc4
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
z	z	k7c2
říše	říš	k1gFnSc2
Cchao	Cchao	k6eAd1
Wej	Wej	k1gMnPc2
<g/>
,	,	kIx,
jednoduchý	jednoduchý	k2eAgInSc4d1
a	a	k8xC
důsledný	důsledný	k2eAgInSc4d1
opakující	opakující	k2eAgInSc4d1
se	se	k3xPyFc4
algoritmus	algoritmus	k1gInSc4
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
π	π	k?
s	s	k7c7
libovolnou	libovolný	k2eAgFnSc7d1
přesností	přesnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
vypočítal	vypočítat	k5eAaPmAgInS
hodnotu	hodnota	k1gFnSc4
pro	pro	k7c4
3072	#num#	k4
<g/>
úhelník	úhelník	k1gInSc4
a	a	k8xC
získal	získat	k5eAaPmAgMnS
hodnotu	hodnota	k1gFnSc4
3,141	3,141	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
vynalezl	vynaleznout	k5eAaPmAgMnS
rychlejší	rychlý	k2eAgFnSc4d2
metodu	metoda	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
získal	získat	k5eAaPmAgMnS
hodnotu	hodnota	k1gFnSc4
3,14	3,14	k4
s	s	k7c7
použitím	použití	k1gNnSc7
96	#num#	k4
<g/>
úhelníku	úhelník	k1gInSc2
<g/>
.	.	kIx.
<g/>
>	>	kIx)
</s>
<s>
Okolo	okolo	k7c2
roku	rok	k1gInSc2
480	#num#	k4
čínský	čínský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
Cu	Cu	k1gFnSc2
Čchung-č	Čchung-č	k1gMnSc1
<g/>
’	’	k?
pomocí	pomocí	k7c2
metody	metoda	k1gFnSc2
Liou	Lioa	k1gFnSc4
Chueje	Chuej	k1gMnSc2
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
π	π	k?
≈	≈	k?
355	#num#	k4
<g/>
/	/	kIx~
<g/>
113	#num#	k4
a	a	k8xC
3,141	3,141	k4
<g/>
5926	#num#	k4
<	<	kIx(
π	π	k?
<	<	kIx(
3,141	3,141	k4
<g/>
5927	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použil	použít	k5eAaPmAgMnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
12288	#num#	k4
<g/>
úhelník	úhelník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
hodnota	hodnota	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
nejpřesnější	přesný	k2eAgFnSc1d3
dlouhých	dlouhý	k2eAgInPc2d1
900	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhé	druhý	k4xOgNnSc1
tisíciletí	tisíciletí	k1gNnSc1
</s>
<s>
Ruční	ruční	k2eAgInPc1d1
výpočty	výpočet	k1gInPc1
</s>
<s>
Odhad	odhad	k1gInSc1
π	π	k?
pomocí	pomoc	k1gFnPc2
vepsaných	vepsaný	k2eAgFnPc2d1
mnohoúhelníkůOdhad	mnohoúhelníkůOdhad	k6eAd1
π	π	k?
pomocí	pomocí	k7c2
vepsaných	vepsaný	k2eAgInPc2d1
a	a	k8xC
opsaných	opsaný	k2eAgInPc2d1
mnohoúhelníků	mnohoúhelník	k1gInPc2
(	(	kIx(
<g/>
Archimédova	Archimédův	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
prohlásil	prohlásit	k5eAaPmAgMnS
Maimonides	Maimonides	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
π	π	k?
je	být	k5eAaImIp3nS
iracionální	iracionální	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
dokázáno	dokázat	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1761	#num#	k4
Johannem	Johann	k1gMnSc7
Heinrichem	Heinrich	k1gMnSc7
Lambertem	Lambert	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byly	být	k5eAaImAgInP
nalezeny	nalezen	k2eAgInPc1d1
důkazy	důkaz	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
vyžadují	vyžadovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
znalosti	znalost	k1gFnPc4
integrálu	integrál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
proslavil	proslavit	k5eAaPmAgMnS
Ivan	Ivan	k1gMnSc1
Niven	Niven	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Podobný	podobný	k2eAgInSc4d1
důkaz	důkaz	k1gInSc4
ukázala	ukázat	k5eAaPmAgFnS
o	o	k7c4
něco	něco	k3yInSc4
dříve	dříve	k6eAd2
Mary	Mary	k1gFnSc1
Cartwright	Cartwrighta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc1
bylo	být	k5eAaImAgNnS
π	π	k?
spočítáno	spočítán	k2eAgNnSc4d1
na	na	k7c6
méně	málo	k6eAd2
než	než	k8xS
10	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
výrazný	výrazný	k2eAgInSc1d1
pokrok	pokrok	k1gInSc1
v	v	k7c6
odhadování	odhadování	k1gNnSc6
jeho	jeho	k3xOp3gFnSc2
hodnoty	hodnota	k1gFnSc2
přišel	přijít	k5eAaPmAgMnS
s	s	k7c7
vývojem	vývoj	k1gInSc7
nekonečných	konečný	k2eNgFnPc2d1
řad	řada	k1gFnPc2
a	a	k8xC
diferenciálního	diferenciální	k2eAgInSc2d1
a	a	k8xC
integrálního	integrální	k2eAgInSc2d1
počtu	počet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgFnSc3d1
π	π	k?
spočítat	spočítat	k5eAaPmF
s	s	k7c7
jakoukoli	jakýkoli	k3yIgFnSc7
přesností	přesnost	k1gFnSc7
pomocí	pomocí	k7c2
řad	řada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
roku	rok	k1gInSc2
1400	#num#	k4
vymyslel	vymyslet	k5eAaPmAgMnS
Madhava	Madhava	k1gFnSc1
ze	z	k7c2
Sangamagramy	Sangamagram	k1gInPc1
první	první	k4xOgFnSc6
nám	my	k3xPp1nPc3
známou	známý	k2eAgFnSc4d1
takovou	takový	k3xDgFnSc4
řadu	řada	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
k	k	k7c3
</s>
<s>
2	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋯	⋯	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
<g/>
=	=	kIx~
<g/>
4	#num#	k4
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
9	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
cdots	cdots	k1gInSc1
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Tato	tento	k3xDgFnSc1
řada	řada	k1gFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Madhavova-Leibnizova	Madhavova-Leibnizův	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
Gregoryho-Leibnizova	Gregoryho-Leibnizův	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
ji	on	k3xPp3gFnSc4
znovuobjevili	znovuobjevit	k5eAaPmAgMnP
James	James	k1gMnSc1
Gregory	Gregor	k1gMnPc7
a	a	k8xC
Gottfried	Gottfried	k1gMnSc1
Leibniz	Leibniz	k1gMnSc1
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postup	postup	k1gInSc1
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
tak	tak	k6eAd1
pomalý	pomalý	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
spočítat	spočítat	k5eAaPmF
4000	#num#	k4
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
výsledek	výsledek	k1gInSc1
přesnější	přesný	k2eAgInSc1d2
než	než	k8xS
ten	ten	k3xDgInSc1
Archimédův	Archimédův	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
po	po	k7c6
úpravě	úprava	k1gFnSc6
řady	řada	k1gFnSc2
na	na	k7c4
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
12	#num#	k4
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
3	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
−	−	k?
</s>
<s>
k	k	k7c3
</s>
<s>
2	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
12	#num#	k4
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
k	k	k7c3
</s>
<s>
2	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
12	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋯	⋯	k?
</s>
<s>
)	)	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gMnSc1
<g/>
{	{	kIx(
<g/>
aligned	aligned	k1gMnSc1
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
pi	pi	k0
&	&	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
12	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-k	-k	k?
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
12	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
\\	\\	k?
<g/>
&	&	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
12	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
left	left	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
\	\	kIx~
<g/>
over	over	k1gInSc1
3	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
3	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
\	\	kIx~
<g/>
over	over	k1gInSc1
5	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
3	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
\	\	kIx~
<g/>
over	over	k1gInSc1
7	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
3	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
cdots	cdots	k1gInSc1
\	\	kIx~
<g/>
right	right	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
aligned	aligned	k1gMnSc1
<g/>
}}}	}}}	k?
</s>
<s>
byl	být	k5eAaImAgInS
Madhava	Madhava	k1gFnSc1
schopen	schopen	k2eAgInSc4d1
π	π	k?
odhadnout	odhadnout	k5eAaPmF
na	na	k7c4
3,141	3,141	k4
<g/>
59265359	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
správně	správně	k6eAd1
na	na	k7c4
11	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rekord	rekord	k1gInSc4
pokořil	pokořit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1424	#num#	k4
perský	perský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
Al-Káší	Al-Káší	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ho	on	k3xPp3gMnSc4
odhadl	odhadnout	k5eAaPmAgInS
na	na	k7c4
16	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
po	po	k7c6
Archimédovi	Archimédes	k1gMnSc6
udělal	udělat	k5eAaPmAgInS
další	další	k2eAgInSc4d1
velký	velký	k2eAgInSc4d1
pokrok	pokrok	k1gInSc4
německý	německý	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
Ludolph	Ludolph	k1gMnSc1
van	vana	k1gFnPc2
Ceulen	Ceulna	k1gFnPc2
(	(	kIx(
<g/>
1540	#num#	k4
<g/>
–	–	k?
<g/>
1610	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
použil	použít	k5eAaPmAgInS
geometrickou	geometrický	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
<g/>
,	,	kIx,
díky	díky	k7c3
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
odhadl	odhadnout	k5eAaPmAgInS
π	π	k?
správně	správně	k6eAd1
na	na	k7c4
35	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
svůj	svůj	k3xOyFgInSc4
výpočet	výpočet	k1gInSc4
byl	být	k5eAaImAgMnS
tak	tak	k9
hrdý	hrdý	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
tuto	tento	k3xDgFnSc4
hodnotu	hodnota	k1gFnSc4
nechal	nechat	k5eAaPmAgMnS
vytesat	vytesat	k5eAaPmF
na	na	k7c4
hrob	hrob	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pí	pí	k1gNnSc7
se	se	k3xPyFc4
proto	proto	k8xC
někdy	někdy	k6eAd1
nazývá	nazývat	k5eAaImIp3nS
Ludolfovým	Ludolfův	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Někdy	někdy	k6eAd1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
začala	začít	k5eAaPmAgFnS
vyvíjet	vyvíjet	k5eAaImF
metoda	metoda	k1gFnSc1
nekonečných	konečný	k2eNgFnPc2d1
řad	řada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
takové	takový	k3xDgNnSc4
vyjádření	vyjádření	k1gNnSc4
nabízel	nabízet	k5eAaImAgInS
nekonečný	konečný	k2eNgInSc1d1
součin	součin	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1593	#num#	k4
vymyslel	vymyslet	k5eAaPmAgMnS
François	François	k1gFnSc4
Viè	Viè	k1gInSc5
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
⋯	⋯	k?
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}}}	}}}}}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
\	\	kIx~
<g/>
cdots	cdots	k1gInSc1
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Dalším	další	k2eAgNnSc7d1
známým	známý	k2eAgNnSc7d1
vyjádřením	vyjádření	k1gNnSc7
je	být	k5eAaImIp3nS
Wallisův	Wallisův	k2eAgInSc1d1
součin	součin	k1gInSc1
od	od	k7c2
Johna	John	k1gMnSc2
Wallise	Wallise	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1655	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
π	π	k?
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
∏	∏	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
6	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
8	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
⋯	⋯	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
16	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
36	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
64	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
⋯	⋯	k?
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
prod	prod	k6eAd1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
3	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
6	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
6	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
8	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
8	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
9	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdots	cdots	k6eAd1
\	\	kIx~
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
16	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
15	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
36	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
35	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
64	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
63	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdots	cdots	k1gInSc1
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Isaac	Isaac	k6eAd1
Newton	Newton	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1665	#num#	k4
<g/>
–	–	k?
<g/>
66	#num#	k4
odvodil	odvodit	k5eAaPmAgInS
vzorec	vzorec	k1gInSc1
pomocí	pomocí	k7c2
řady	řada	k1gFnSc2
s	s	k7c7
arkem	arkus	k1gInSc7
sinem	sinus	k1gInSc7
a	a	k8xC
vypočítal	vypočítat	k5eAaPmAgMnS
π	π	k?
na	na	k7c6
15	#num#	k4
číslic	číslice	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
6	#num#	k4
</s>
<s>
arcsin	arcsin	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
6	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
5	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
3	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
4	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
6	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
7	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋯	⋯	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
16	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
9	#num#	k4
</s>
<s>
640	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
15	#num#	k4
</s>
<s>
7168	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
35	#num#	k4
</s>
<s>
98304	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
189	#num#	k4
</s>
<s>
2883584	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
693	#num#	k4
</s>
<s>
54525952	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
429	#num#	k4
</s>
<s>
167772160	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋯	⋯	k?
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gMnSc1
<g/>
{	{	kIx(
<g/>
aligned	aligned	k1gMnSc1
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
pi	pi	k0
&	&	k?
<g/>
=	=	kIx~
<g/>
6	#num#	k4
<g/>
\	\	kIx~
<g/>
arcsin	arcsin	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\\	\\	k?
<g/>
&	&	k?
<g/>
=	=	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
6	#num#	k4
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
1	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
3	#num#	k4
<g/>
}}	}}	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
+	+	kIx~
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
5	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
3	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
5	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
4	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
6	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
7	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}}	}}	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
cdots	cdots	k6eAd1
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
\\	\\	k?
<g/>
&	&	k?
<g/>
=	=	kIx~
<g/>
3	#num#	k4
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
\	\	kIx~
<g/>
binom	binom	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
16	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}}	}}	k?
<g/>
\\	\\	k?
<g/>
&	&	k?
<g/>
=	=	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
8	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
9	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
640	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
15	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
7168	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
35	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
98304	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
189	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2883584	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
693	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
54525952	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
429	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
167772160	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
cdots	cdots	k6eAd1
.	.	kIx.
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
aligned	aligned	k1gMnSc1
<g/>
}}}	}}}	k?
</s>
<s>
Později	pozdě	k6eAd2
ale	ale	k9
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Stydím	stydět	k5eAaImIp1nS
se	se	k3xPyFc4
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
na	na	k7c4
kolik	kolik	k4yRc4,k4yIc4,k4yQc4
číslic	číslice	k1gFnPc2
jsem	být	k5eAaImIp1nS
tyto	tento	k3xDgInPc4
výpočty	výpočet	k1gInPc4
prováděl	provádět	k5eAaImAgInS
<g/>
,	,	kIx,
když	když	k8xS
jsem	být	k5eAaImIp1nS
zrovna	zrovna	k6eAd1
neměl	mít	k5eNaImAgMnS
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
jiného	jiný	k2eAgNnSc2d1
na	na	k7c6
práci	práce	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
výpočet	výpočet	k1gInSc1
se	se	k3xPyFc4
k	k	k7c3
π	π	k?
přibližuje	přibližovat	k5eAaImIp3nS
lineárně	lineárně	k6eAd1
s	s	k7c7
rychlostí	rychlost	k1gFnSc7
μ	μ	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
alespoň	alespoň	k9
tři	tři	k4xCgNnPc1
desetinná	desetinný	k2eAgNnPc1d1
místa	místo	k1gNnPc1
na	na	k7c4
každých	každý	k3xTgInPc2
pět	pět	k4xCc4
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	s	k7c7
n	n	k0
blíží	blížit	k5eAaImIp3nS
k	k	k7c3
nekonečnu	nekonečno	k1gNnSc3
<g/>
,	,	kIx,
μ	μ	k?
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
k	k	k7c3
1	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
/	/	kIx~
<g/>
μ	μ	k?
ke	k	k7c3
4	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
μ	μ	k?
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
;	;	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
μ	μ	k?
</s>
<s>
=	=	kIx~
</s>
<s>
8	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mu	on	k3xPp3gMnSc3
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
n-	n-	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
8	#num#	k4
<g/>
n	n	k0
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
}}	}}	k?
<g/>
;	;	kIx,
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mu	on	k3xPp3gMnSc3
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
8	#num#	k4
<g/>
n	n	k0
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
n-	n-	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1706	#num#	k4
se	se	k3xPyFc4
povedlo	povést	k5eAaPmAgNnS
Johnu	John	k1gMnSc3
Machinovi	Machin	k1gMnSc3
jako	jako	k8xC,k8xS
prvnímu	první	k4xOgNnSc3
spočítat	spočítat	k5eAaPmF
π	π	k?
na	na	k7c4
100	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Použil	použít	k5eAaPmAgMnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
řadu	řada	k1gFnSc4
s	s	k7c7
arkem	arkus	k1gInSc7
tangens	tangens	k1gInSc2
</s>
<s>
arctan	arctan	k1gInSc1
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
k	k	k7c3
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
x	x	k?
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
x	x	k?
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
7	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋯	⋯	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
arctan	arctan	k1gInSc4
\	\	kIx~
<g/>
,	,	kIx,
<g/>
x	x	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
}	}	kIx)
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
x-	x-	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
cdots	cdots	k1gInSc1
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
vymyslel	vymyslet	k5eAaPmAgMnS
vzorec	vzorec	k1gInSc4
</s>
<s>
π	π	k?
</s>
<s>
4	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
arctan	arctan	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
arctan	arctan	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
239	#num#	k4
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
4	#num#	k4
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
arctan	arctan	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
arctan	arctan	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
239	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Vzorce	vzorec	k1gInPc1
podobného	podobný	k2eAgInSc2d1
typu	typ	k1gInSc2
se	se	k3xPyFc4
až	až	k9
do	do	k7c2
příchodu	příchod	k1gInSc2
počítačů	počítač	k1gMnPc2
staly	stát	k5eAaPmAgFnP
nejlepší	dobrý	k2eAgFnSc7d3
možnou	možný	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
výpočtu	výpočet	k1gInSc2
π	π	k?
Johann	Johann	k1gInSc1
Dase	Dase	k1gFnPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
z	z	k7c2
takzvaných	takzvaný	k2eAgInPc2d1
lidských	lidský	k2eAgInPc2d1
kalkulátorů	kalkulátor	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1844	#num#	k4
pomocí	pomocí	k7c2
podobné	podobný	k2eAgFnSc2d1
rovnice	rovnice	k1gFnSc2
v	v	k7c6
hlavě	hlava	k1gFnSc6
vypočítal	vypočítat	k5eAaPmAgInS
200	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
π	π	k?
Na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
držel	držet	k5eAaImAgInS
rekord	rekord	k1gInSc1
William	William	k1gInSc1
Shanks	Shanks	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
za	za	k7c2
15	#num#	k4
let	léto	k1gNnPc2
vypočítal	vypočítat	k5eAaPmAgMnS
707	#num#	k4
číslic	číslice	k1gFnPc2
π	π	k?
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
kvůli	kvůli	k7c3
chybě	chyba	k1gFnSc3
bylo	být	k5eAaImAgNnS
jen	jen	k9
527	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
správně	správně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Novější	nový	k2eAgInPc1d2
rekordy	rekord	k1gInPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
počítány	počítat	k5eAaImNgFnP
dvakrát	dvakrát	k6eAd1
dvěma	dva	k4xCgFnPc7
různými	různý	k2eAgFnPc7d1
rovnicemi	rovnice	k1gFnPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zabránilo	zabránit	k5eAaPmAgNnS
takovýmto	takovýto	k3xDgFnPc3
chybám	chyba	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
výsledky	výsledek	k1gInPc1
shodují	shodovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
nejspíše	nejspíše	k9
správné	správný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Teoretický	teoretický	k2eAgInSc1d1
pokrok	pokrok	k1gInSc1
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vedl	vést	k5eAaImAgMnS
k	k	k7c3
několika	několik	k4yIc3
objevům	objev	k1gInPc3
o	o	k7c6
povaze	povaha	k1gFnSc6
π	π	k?
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
neproveditelné	proveditelný	k2eNgFnPc1d1
pouze	pouze	k6eAd1
pomocí	pomocí	k7c2
číselných	číselný	k2eAgInPc2d1
výpočtů	výpočet	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1761	#num#	k4
dokázal	dokázat	k5eAaPmAgMnS
Johann	Johann	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Lambert	Lambert	k1gMnSc1
iracionalitu	iracionalita	k1gFnSc4
π	π	k?
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1794	#num#	k4
dokázal	dokázat	k5eAaPmAgInS
Adrien-Marie	Adrien-Marie	k1gFnSc2
Legendre	Legendr	k1gInSc5
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
π	π	k?
je	být	k5eAaImIp3nS
iracionální	iracionální	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Leonhard	Leonhard	k1gMnSc1
Euler	Euler	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1735	#num#	k4
vyřešil	vyřešit	k5eAaPmAgInS
známý	známý	k2eAgInSc1d1
Basilejský	basilejský	k2eAgInSc1d1
problém	problém	k1gInSc1
a	a	k8xC
zjistil	zjistit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
hodnota	hodnota	k1gFnSc1
Riemannovy	Riemannův	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
zeta	zet	k1gMnSc2
pro	pro	k7c4
2	#num#	k4
je	být	k5eAaImIp3nS
</s>
<s>
ζ	ζ	k?
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
1	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋯	⋯	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
zeta	zet	k1gMnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
k	k	k7c3
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
2	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
cdots	cdots	k1gInSc1
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
neboli	neboli	k8xC
π	π	k?
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
prokázal	prokázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
π	π	k?
má	mít	k5eAaImIp3nS
spojitost	spojitost	k1gFnSc1
s	s	k7c7
prvočísly	prvočíslo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legendre	Legendr	k1gInSc5
i	i	k8xC
Euler	Eulra	k1gFnPc2
navrhli	navrhnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
π	π	k?
by	by	kYmCp3nP
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
transcendentní	transcendentní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
roce	rok	k1gInSc6
1882	#num#	k4
dokázal	dokázat	k5eAaPmAgMnS
Ferdinand	Ferdinand	k1gMnSc1
von	von	k1gInSc4
Lindemann	Lindemann	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výpočet	výpočet	k1gInSc1
v	v	k7c6
počítačové	počítačový	k2eAgFnSc6d1
éře	éra	k1gFnSc6
</s>
<s>
Počítačová	počítačový	k2eAgFnSc1d1
animace	animace	k1gFnSc1
π	π	k?
</s>
<s>
Příchod	příchod	k1gInSc1
počítačů	počítač	k1gMnPc2
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vedl	vést	k5eAaImAgInS
k	k	k7c3
novým	nový	k2eAgInPc3d1
a	a	k8xC
novým	nový	k2eAgInPc3d1
rekordům	rekord	k1gInPc3
ve	v	k7c6
výpočtu	výpočet	k1gInSc6
π	π	k?
John	John	k1gMnSc1
von	von	k1gInSc1
Neumann	Neumann	k1gMnSc1
se	se	k3xPyFc4
svým	svůj	k3xOyFgInSc7
týmem	tým	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
použil	použít	k5eAaPmAgInS
ENIAC	ENIAC	kA
k	k	k7c3
výpočtu	výpočet	k1gInSc3
prvních	první	k4xOgFnPc2
2037	#num#	k4
číslic	číslice	k1gFnPc2
π	π	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
počítači	počítač	k1gInSc6
zabralo	zabrat	k5eAaPmAgNnS
70	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
příštích	příští	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
byly	být	k5eAaImAgInP
přidány	přidat	k5eAaPmNgInP
tisíce	tisíc	k4xCgInPc1
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
a	a	k8xC
hranice	hranice	k1gFnPc4
milionu	milion	k4xCgInSc2
číslic	číslice	k1gFnPc2
byla	být	k5eAaImAgFnS
překonána	překonat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokrok	pokrok	k1gInSc1
nebyl	být	k5eNaImAgInS
rychlejší	rychlý	k2eAgMnSc1d2
než	než	k8xS
předtím	předtím	k6eAd1
jen	jen	k9
kvůli	kvůli	k7c3
rychlejšímu	rychlý	k2eAgNnSc3d2
hardware	hardware	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
díky	díky	k7c3
novým	nový	k2eAgInPc3d1
algoritmům	algoritmus	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
největších	veliký	k2eAgInPc2d3
pokroků	pokrok	k1gInPc2
byl	být	k5eAaImAgInS
objev	objev	k1gInSc1
rychlé	rychlý	k2eAgFnSc2d1
Fourierovy	Fourierův	k2eAgFnSc2d1
transformace	transformace	k1gFnSc2
(	(	kIx(
<g/>
FFT	fft	k0
<g/>
)	)	kIx)
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
umožňuje	umožňovat	k5eAaImIp3nS
počítačům	počítač	k1gInPc3
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
provádět	provádět	k5eAaImF
výpočty	výpočet	k1gInPc4
s	s	k7c7
velkými	velký	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vymyslel	vymyslet	k5eAaPmAgMnS
indický	indický	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
Šrínivása	Šrínivás	k1gMnSc2
Rámanudžan	Rámanudžan	k1gMnSc1
několik	několik	k4yIc4
nových	nový	k2eAgInPc2d1
vzorců	vzorec	k1gInPc2
pro	pro	k7c4
π	π	k?
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
některé	některý	k3yIgFnPc1
jsou	být	k5eAaImIp3nP
pozoruhodné	pozoruhodný	k2eAgFnPc1d1
svou	svůj	k3xOyFgFnSc7
elegancí	elegance	k1gFnSc7
<g/>
,	,	kIx,
hloubkou	hloubka	k1gFnSc7
a	a	k8xC
rychlostí	rychlost	k1gFnSc7
konvergence	konvergence	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Jeden	jeden	k4xCgInSc1
z	z	k7c2
jeho	jeho	k3xOp3gInPc2
vzorců	vzorec	k1gInPc2
je	být	k5eAaImIp3nS
řada	řada	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
9801	#num#	k4
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
(	(	kIx(
</s>
<s>
1103	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
26390	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
k	k	k7c3
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
4	#num#	k4
</s>
<s>
396	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
9801	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
(	(	kIx(
<g/>
1103	#num#	k4
<g/>
+	+	kIx~
<g/>
26390	#num#	k4
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
k	k	k7c3
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
396	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
k	k	k7c3
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
k	k	k7c3
<g/>
!	!	kIx.
</s>
<s desamb="1">
je	být	k5eAaImIp3nS
faktoriál	faktoriál	k1gInSc4
k.	k.	k?
</s>
<s>
Několik	několik	k4yIc1
dalších	další	k2eAgMnPc2d1
je	být	k5eAaImIp3nS
ukázáno	ukázat	k5eAaPmNgNnS
v	v	k7c6
následující	následující	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
42	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
6	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
((	((	k?
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
42	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
16	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
(	(	kIx(
</s>
<s>
21460	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1123	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
4	#num#	k4
</s>
<s>
441	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
(	(	kIx(
<g/>
21460	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1123	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
441	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
10	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
6	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
{{	{{	k?
<g/>
4	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
32	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
32	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
8	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
42	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
5	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
30	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
3	#num#	k4
</s>
<s>
64	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{{	{{	k?
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
5	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
8	#num#	k4
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
42	#num#	k4
<g/>
n	n	k0
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
+	+	kIx~
<g/>
30	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
5	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
{{	{{	k?
<g/>
64	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
27	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
27	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
Z	Z	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
15	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
27	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
15	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
right	right	k1gInSc1
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
15	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
15	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
Z	Z	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
125	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
33	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
125	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
33	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
right	right	k1gInSc1
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
85	#num#	k4
</s>
<s>
85	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
85	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
85	#num#	k4
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
18	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
85	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
133	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
8	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
85	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
133	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
right	right	k1gInSc1
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
125	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
11	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
125	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
11	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
right	right	k1gInSc1
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
8	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
9	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
9	#num#	k4
<g/>
Z	Z	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
40	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
49	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
40	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
49	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
11	#num#	k4
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
11	#num#	k4
<g/>
Z	Z	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
280	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
19	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
99	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
280	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
99	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
Z	Z	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
10	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
10	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
9	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
Z	Z	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
644	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
41	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
72	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
644	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
41	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
5	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
72	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
Z	Z	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
28	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
28	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
20	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
20	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
72	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
72	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
(	(	kIx(
</s>
<s>
260	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
23	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
18	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
(	(	kIx(
<g/>
260	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
23	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
4	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
18	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
3528	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3528	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
Z	z	k7c2
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
(	(	kIx(
</s>
<s>
21460	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1123	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
882	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
(	(	kIx(
<g/>
21460	#num#	k4
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1123	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
4	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
882	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
n	n	k0
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
podobného	podobný	k2eAgInSc2d1
vzorce	vzorec	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
vymysleli	vymyslet	k5eAaPmAgMnP
bratři	bratr	k1gMnPc1
Čudnovští	Čudnovský	k2eAgMnPc1d1
<g/>
,	,	kIx,
</s>
<s>
426880	#num#	k4
</s>
<s>
10005	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
6	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
(	(	kIx(
</s>
<s>
13591409	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
545140134	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
(	(	kIx(
</s>
<s>
k	k	k7c3
</s>
<s>
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
−	−	k?
</s>
<s>
640320	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
426880	#num#	k4
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
10005	#num#	k4
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}}	}}	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
(	(	kIx(
<g/>
13591409	#num#	k4
<g/>
+	+	kIx~
<g/>
545140134	#num#	k4
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
(	(	kIx(
<g/>
k	k	k7c3
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
-	-	kIx~
<g/>
640320	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
k	k	k7c3
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
lze	lze	k6eAd1
získat	získat	k5eAaPmF
14	#num#	k4
číslic	číslice	k1gFnPc2
na	na	k7c4
jeden	jeden	k4xCgInSc4
prvek	prvek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Pomocí	pomocí	k7c2
něho	on	k3xPp3gMnSc2
bratři	bratr	k1gMnPc1
ke	k	k7c3
konci	konec	k1gInSc3
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
vytvořili	vytvořit	k5eAaPmAgMnP
několik	několik	k4yIc4
rekordů	rekord	k1gInPc2
ve	v	k7c6
výpočtu	výpočet	k1gInSc6
π	π	k?
včetně	včetně	k7c2
pokoření	pokoření	k1gNnSc2
hranice	hranice	k1gFnSc2
jedné	jeden	k4xCgFnSc2
miliardy	miliarda	k4xCgFnSc2
číslic	číslice	k1gFnPc2
(	(	kIx(
<g/>
konkrétně	konkrétně	k6eAd1
1	#num#	k4
011	#num#	k4
196	#num#	k4
691	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
na	na	k7c4
výpočet	výpočet	k1gInSc4
π	π	k?
v	v	k7c6
programech	program	k1gInPc6
na	na	k7c6
dnešních	dnešní	k2eAgInPc6d1
osobních	osobní	k2eAgInPc6d1
počítačích	počítač	k1gInPc6
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
PhysOrg	PhysOrg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
že	že	k8xS
počítačoví	počítačový	k2eAgMnPc1d1
experti	expert	k1gMnPc1
Shigeru	Shiger	k1gInSc2
Kondo	Kondo	k1gNnSc1
a	a	k8xC
Alexander	Alexandra	k1gFnPc2
Yee	Yee	k1gMnPc1
vypočítali	vypočítat	k5eAaPmAgMnP
π	π	k?
na	na	k7c4
pět	pět	k4xCc4
bilionů	bilion	k4xCgInPc2
desetinných	desetinný	k2eAgFnPc2d1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
dvojnásobně	dvojnásobně	k6eAd1
překonali	překonat	k5eAaPmAgMnP
předchozí	předchozí	k2eAgInSc4d1
rekord	rekord	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
rekord	rekord	k1gInSc4
navýšili	navýšit	k5eAaPmAgMnP
na	na	k7c4
deset	deset	k4xCc4
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
dvanáct	dvanáct	k4xCc4
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
bilionů	bilion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
řad	řada	k1gFnPc2
zvyšuje	zvyšovat	k5eAaImIp3nS
přesnost	přesnost	k1gFnSc1
výpočtu	výpočet	k1gInSc2
s	s	k7c7
pevně	pevně	k6eAd1
danou	daný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
existují	existovat	k5eAaImIp3nP
algoritmy	algoritmus	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
násobí	násobit	k5eAaImIp3nP
počet	počet	k1gInSc4
správných	správný	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
s	s	k7c7
každým	každý	k3xTgInSc7
krokem	krok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
matematici	matematik	k1gMnPc1
Richard	Richarda	k1gFnPc2
Brent	Brent	k?
a	a	k8xC
Eugene	Eugen	k1gInSc5
Salamin	Salamin	k2eAgInSc1d1
objevili	objevit	k5eAaPmAgMnP
Brentův-Salaminův	Brentův-Salaminův	k2eAgInSc4d1
algoritmus	algoritmus	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
s	s	k7c7
každým	každý	k3xTgInSc7
dalším	další	k2eAgInSc7d1
krokem	krok	k1gInSc7
počet	počet	k1gInSc1
správných	správný	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
zdvojnásobí	zdvojnásobit	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Nejdříve	dříve	k6eAd3
se	se	k3xPyFc4
definují	definovat	k5eAaBmIp3nP
proměnné	proměnná	k1gFnPc1
</s>
<s>
a	a	k8xC
</s>
<s>
0	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
0	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
t	t	k?
</s>
<s>
0	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
p	p	k?
</s>
<s>
0	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a_	a_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
b_	b_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
t_	t_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
p_	p_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
poté	poté	k6eAd1
se	se	k3xPyFc4
opakuje	opakovat	k5eAaImIp3nS
</s>
<s>
a	a	k8xC
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
n	n	k0
</s>
<s>
2	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
n	n	k0
</s>
<s>
b	b	k?
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a_	a_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
b_	b_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
b_	b_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
b_	b_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
t	t	k?
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
t	t	k?
</s>
<s>
n	n	k0
</s>
<s>
−	−	k?
</s>
<s>
p	p	k?
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
a	a	k8xC
</s>
<s>
n	n	k0
</s>
<s>
−	−	k?
</s>
<s>
a	a	k8xC
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
p	p	k?
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
p	p	k?
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
t_	t_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
t_	t_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
-p_	-p_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
-a_	-a_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
p_	p_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
p_	p_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
dokud	dokud	k6eAd1
an	an	k?
a	a	k8xC
bn	bn	k?
nemají	mít	k5eNaImIp3nP
podobnou	podobný	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
lze	lze	k6eAd1
π	π	k?
odhadnout	odhadnout	k5eAaPmF
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
π	π	k?
</s>
<s>
≈	≈	k?
</s>
<s>
(	(	kIx(
</s>
<s>
a	a	k8xC
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
n	n	k0
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
t	t	k?
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
\	\	kIx~
<g/>
approx	approx	k1gInSc4
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
(	(	kIx(
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
b_	b_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
t_	t_	k?
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}}	}}}	k?
<g/>
.	.	kIx.
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
25	#num#	k4
opakování	opakování	k1gNnSc1
tohoto	tento	k3xDgInSc2
algoritmu	algoritmus	k1gInSc2
vede	vést	k5eAaImIp3nS
ke	k	k7c3
45	#num#	k4
milionům	milion	k4xCgInPc3
správných	správný	k2eAgNnPc2d1
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc1d1
algoritmus	algoritmus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
každým	každý	k3xTgMnPc3
krokem	krokem	k6eAd1
zečtyřnásobuje	zečtyřnásobovat	k5eAaImIp3nS
přesnost	přesnost	k1gFnSc4
<g/>
,	,	kIx,
objevili	objevit	k5eAaPmAgMnP
Jonathan	Jonathan	k1gMnSc1
a	a	k8xC
Peter	Peter	k1gMnSc1
Borweinovi	Borweinův	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc4
metody	metoda	k1gFnPc4
použil	použít	k5eAaPmAgMnS
Jasumasa	Jasumas	k1gMnSc2
Kanada	Kanada	k1gFnSc1
se	s	k7c7
svým	svůj	k3xOyFgInSc7
týmem	tým	k1gInSc7
k	k	k7c3
dosažení	dosažení	k1gNnSc3
většiny	většina	k1gFnSc2
rekordů	rekord	k1gInPc2
v	v	k7c6
odhadu	odhad	k1gInSc6
π	π	k?
od	od	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpřesnějšího	přesný	k2eAgInSc2d3
výsledku	výsledek	k1gInSc2
do	do	k7c2
konce	konec	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc6
dosáhl	dosáhnout	k5eAaPmAgMnS
roku	rok	k1gInSc2
1999	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
π	π	k?
vypočítal	vypočítat	k5eAaPmAgInS
na	na	k7c4
206	#num#	k4
158	#num#	k4
430	#num#	k4
000	#num#	k4
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
Simon	Simon	k1gMnSc1
Plouffe	Plouff	k1gInSc5
vyvinul	vyvinout	k5eAaPmAgInS
Baileyho-Borweinův-Plouffeův	Baileyho-Borweinův-Plouffeův	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
(	(	kIx(
<g/>
BBP	BBP	kA
vzorec	vzorec	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
∞	∞	k?
</s>
<s>
1	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
2	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
k	k	k7c3
</s>
<s>
+	+	kIx~
</s>
<s>
6	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
16	#num#	k4
<g/>
^	^	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
k	k	k7c3
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
8	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
8	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
8	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
5	#num#	k4
<g/>
}}	}}	k?
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
8	#num#	k4
<g/>
k	k	k7c3
<g/>
+	+	kIx~
<g/>
6	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
vzorec	vzorec	k1gInSc1
je	být	k5eAaImIp3nS
zajímavý	zajímavý	k2eAgInSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
dokáže	dokázat	k5eAaPmIp3nS
určit	určit	k5eAaPmF
n-tou	n-tý	k2eAgFnSc4d1
binární	binární	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
π	π	k?
bez	bez	k7c2
počítání	počítání	k1gNnSc2
těch	ten	k3xDgMnPc2
předchozích	předchozí	k2eAgMnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
zjistilo	zjistit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
biliardtý	biliardtý	k2eAgInSc1d1
bit	bit	k1gInSc1
π	π	k?
má	mít	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pí	pí	k1gNnSc1
a	a	k8xC
řetězový	řetězový	k2eAgInSc1d1
zlomek	zlomek	k1gInSc1
</s>
<s>
Čísla	číslo	k1gNnPc1
v	v	k7c6
tomto	tento	k3xDgNnSc6
vyjádření	vyjádření	k1gNnSc6
π	π	k?
v	v	k7c6
nekonečném	konečný	k2eNgInSc6d1
řetězovém	řetězový	k2eAgInSc6d1
zlomku	zlomek	k1gInSc6
nevykazují	vykazovat	k5eNaImIp3nP
žádnou	žádný	k3yNgFnSc4
pravidelnost	pravidelnost	k1gFnSc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
[	[	kIx(
</s>
<s>
3	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
7	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
15	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
292	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
3	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
14	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
84	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
⋯	⋯	k?
</s>
<s>
]	]	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
;	;	kIx,
<g/>
7,15	7,15	k4
<g/>
,1	,1	k4
<g/>
,292	,292	k4
<g/>
,1	,1	k4
<g/>
,1	,1	k4
<g/>
,1	,1	k4
<g/>
,2	,2	k4
<g/>
,1	,1	k4
<g/>
,3	,3	k4
<g/>
,1	,1	k4
<g/>
,14	,14	k4
<g/>
,2	,2	k4
<g/>
,1	,1	k4
<g/>
,1	,1	k4
<g/>
,2	,2	k4
<g/>
,2	,2	k4
<g/>
,2	,2	k4
<g/>
,2	,2	k4
<g/>
,1	,1	k4
<g/>
,84	,84	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
cdots	cdots	k6eAd1
]	]	kIx)
<g/>
}	}	kIx)
</s>
<s>
neboli	neboli	k8xC
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
292	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋱	⋱	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
15	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
292	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
ddots	ddots	k6eAd1
}}}}}}}}}}}}}}}	}}}}}}}}}}}}}}}	k?
</s>
<s>
Existují	existovat	k5eAaImIp3nP
ale	ale	k9
zobecněné	zobecněný	k2eAgInPc4d1
řetězové	řetězový	k2eAgInPc4d1
zlomky	zlomek	k1gInPc4
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
π	π	k?
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
mají	mít	k5eAaImIp3nP
pravidelnou	pravidelný	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
tento	tento	k3xDgMnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
7	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
9	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋱	⋱	k?
</s>
<s>
=	=	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
7	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
9	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋱	⋱	k?
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
⋱	⋱	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
=	=	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
7	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
9	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
ddots	ddots	k6eAd1
}}}}}}}}}}}}	}}}}}}}}}}}}	k?
<g/>
=	=	kIx~
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k6eAd1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
5	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
7	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
9	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
<g/>
ddots	ddots	k6eAd1
}}}}}}}}}}	}}}}}}}}}}	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
5	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
7	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
textstyle	textstyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
cfrac	cfrac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
9	#num#	k4
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
ddots	ddots	k6eAd1
}}}}}}}}}}}	}}}}}}}}}}}	k?
</s>
<s>
Otevřené	otevřený	k2eAgFnPc1d1
otázky	otázka	k1gFnPc1
</s>
<s>
Jedna	jeden	k4xCgFnSc1
otevřená	otevřený	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
o	o	k7c6
</s>
<s>
π	π	k?
je	být	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
<g/>
,	,	kIx,
jestli	jestli	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
normální	normální	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
–	–	k?
jestli	jestli	k8xS
se	se	k3xPyFc4
jakákoli	jakýkoli	k3yIgFnSc1
řada	řada	k1gFnSc1
čísel	číslo	k1gNnPc2
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
rozvoji	rozvoj	k1gInSc6
objevuje	objevovat	k5eAaImIp3nS
tak	tak	k6eAd1
často	často	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
statisticky	statisticky	k6eAd1
předpokládá	předpokládat	k5eAaImIp3nS
u	u	k7c2
„	„	k?
<g/>
náhodné	náhodný	k2eAgFnSc2d1
<g/>
“	“	k?
řady	řada	k1gFnSc2
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnSc7d1
otázkou	otázka	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
jestli	jestli	k8xS
je	být	k5eAaImIp3nS
množina	množina	k1gFnSc1
{	{	kIx(
<g/>
π	π	k?
<g/>
,	,	kIx,
e	e	k0
<g/>
}	}	kIx)
algebraicky	algebraicky	k6eAd1
nezávislá	závislý	k2eNgFnSc1d1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
Jurij	Jurij	k1gMnSc1
Nesterenko	Nesterenka	k1gFnSc5
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
dokázal	dokázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
množina	množina	k1gFnSc1
{	{	kIx(
<g/>
π	π	k?
<g/>
,	,	kIx,
eπ	eπ	k?
<g/>
,	,	kIx,
Γ	Γ	k?
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
}	}	kIx)
algebraicky	algebraicky	k6eAd1
nezávislá	závislý	k2eNgFnSc1d1
je	být	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použití	použití	k1gNnSc1
v	v	k7c6
matematice	matematika	k1gFnSc6
a	a	k8xC
ve	v	k7c6
vědě	věda	k1gFnSc6
</s>
<s>
π	π	k?
se	se	k3xPyFc4
hojně	hojně	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
rovnicích	rovnice	k1gFnPc6
v	v	k7c6
matematice	matematika	k1gFnSc6
<g/>
,	,	kIx,
vědě	věda	k1gFnSc6
a	a	k8xC
inženýrství	inženýrství	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nevyskytuje	vyskytovat	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
zřetelná	zřetelný	k2eAgFnSc1d1
spojitost	spojitost	k1gFnSc1
s	s	k7c7
kruhy	kruh	k1gInPc7
eukleidovské	eukleidovský	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geometrie	geometrie	k1gFnSc1
a	a	k8xC
goniometrie	goniometrie	k1gFnSc1
</s>
<s>
Pro	pro	k7c4
každý	každý	k3xTgInSc4
kruh	kruh	k1gInSc4
s	s	k7c7
poloměrem	poloměr	k1gInSc7
r	r	kA
a	a	k8xC
průměrem	průměr	k1gInSc7
d	d	k?
=	=	kIx~
2	#num#	k4
<g/>
r	r	kA
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInSc1
obvod	obvod	k1gInSc1
je	být	k5eAaImIp3nS
π	π	k1gInSc4
a	a	k8xC
obsah	obsah	k1gInSc4
π	π	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	s	k7c7
π	π	k?
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
rovnicích	rovnice	k1gFnPc6
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
obsahů	obsah	k1gInPc2
a	a	k8xC
objemů	objem	k1gInPc2
pro	pro	k7c4
mnoho	mnoho	k4c4
geometrických	geometrický	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
tvary	tvar	k1gInPc1
jsou	být	k5eAaImIp3nP
založené	založený	k2eAgFnPc1d1
na	na	k7c6
kružnicích	kružnice	k1gFnPc6
<g/>
,	,	kIx,
například	například	k6eAd1
elipsy	elipsa	k1gFnSc2
<g/>
,	,	kIx,
koule	koule	k1gFnSc2
<g/>
,	,	kIx,
kužely	kužel	k1gInPc4
a	a	k8xC
tory	torus	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Proto	proto	k8xC
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
určitých	určitý	k2eAgInPc6d1
integrálech	integrál	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
popisují	popisovat	k5eAaImIp3nP
obvod	obvod	k1gInSc1
<g/>
,	,	kIx,
obsah	obsah	k1gInSc1
nebo	nebo	k8xC
objem	objem	k1gInSc1
útvarů	útvar	k1gInPc2
vytvářených	vytvářený	k2eAgInPc2d1
kruhy	kruh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
polovina	polovina	k1gFnSc1
plochy	plocha	k1gFnSc2
jednotkového	jednotkový	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
je	být	k5eAaImIp3nS
vyjádřena	vyjádřit	k5eAaPmNgFnS
integrálem	integrál	k1gInSc7
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
∫	∫	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
π	π	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
int	int	k?
_	_	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
-x	-x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
∫	∫	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
int	int	k?
_	_	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
-x	-x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
vyjadřuje	vyjadřovat	k5eAaImIp3nS
polovinu	polovina	k1gFnSc4
délky	délka	k1gFnSc2
jednotkové	jednotkový	k2eAgFnSc2d1
kružnice	kružnice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Lze	lze	k6eAd1
integrovat	integrovat	k5eAaBmF
i	i	k9
složitější	složitý	k2eAgNnPc4d2
tělesa	těleso	k1gNnPc4
<g/>
,	,	kIx,
např.	např.	kA
rotační	rotační	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
definice	definice	k1gFnSc2
goniometrických	goniometrický	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
pomocí	pomocí	k7c2
jednotkové	jednotkový	k2eAgFnSc2d1
kružnice	kružnice	k1gFnSc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
sinus	sinus	k1gInSc1
a	a	k8xC
kosinus	kosinus	k1gInSc1
mají	mít	k5eAaImIp3nP
periodu	perioda	k1gFnSc4
2	#num#	k4
<g/>
π	π	k?
To	to	k9
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
každé	každý	k3xTgInPc4
x	x	k?
a	a	k8xC
celé	celý	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
n	n	k0
platí	platit	k5eAaImIp3nS
sin	sin	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
=	=	kIx~
sin	sin	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
+	+	kIx~
2	#num#	k4
<g/>
π	π	k1gNnPc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
cos	cos	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
=	=	kIx~
cos	cos	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
+	+	kIx~
2	#num#	k4
<g/>
π	π	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
sin	sin	kA
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
=	=	kIx~
0	#num#	k4
<g/>
,	,	kIx,
sin	sin	kA
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
π	π	k1gFnPc2
<g/>
)	)	kIx)
=	=	kIx~
0	#num#	k4
pro	pro	k7c4
všechna	všechen	k3xTgNnPc4
celá	celý	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
n.	n.	k?
Z	z	k7c2
definice	definice	k1gFnSc2
také	také	k9
plyne	plynout	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
180	#num#	k4
<g/>
°	°	k?
je	být	k5eAaImIp3nS
rovno	roven	k2eAgNnSc1d1
π	π	k?
radiánům	radián	k1gInPc3
<g/>
,	,	kIx,
neboli	neboli	k8xC
1	#num#	k4
<g/>
°	°	k?
=	=	kIx~
(	(	kIx(
<g/>
π	π	k?
<g/>
/	/	kIx~
<g/>
180	#num#	k4
<g/>
)	)	kIx)
radiánů	radián	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
moderní	moderní	k2eAgFnSc6d1
matematice	matematika	k1gFnSc6
je	být	k5eAaImIp3nS
π	π	k?
často	často	k6eAd1
definováno	definován	k2eAgNnSc1d1
pomocí	pomocí	k7c2
goniometrických	goniometrický	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
jako	jako	k9
nejmenší	malý	k2eAgFnPc4d3
kladné	kladný	k2eAgFnPc4d1
x	x	k?
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
platí	platit	k5eAaImIp3nS
sin	sin	kA
x	x	k?
=	=	kIx~
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělá	dělat	k5eAaImIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
odstranění	odstranění	k1gNnSc3
závislosti	závislost	k1gFnSc2
na	na	k7c6
eukleidovské	eukleidovský	k2eAgFnSc6d1
geometrii	geometrie	k1gFnSc6
<g/>
.	.	kIx.
π	π	k?
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
také	také	k9
definováno	definovat	k5eAaBmNgNnS
pomocí	pomocí	k7c2
cyklometrických	cyklometrický	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
π	π	k?
=	=	kIx~
2	#num#	k4
arccos	arccosa	k1gFnPc2
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
π	π	k?
=	=	kIx~
4	#num#	k4
arctan	arctan	k1gInSc1
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšíření	rozšíření	k1gNnSc1
cyklometrických	cyklometrický	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
jako	jako	k8xC,k8xS
mocninných	mocninný	k2eAgFnPc2d1
řad	řada	k1gFnPc2
je	být	k5eAaImIp3nS
nejjednodušší	jednoduchý	k2eAgInSc4d3
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
odvodit	odvodit	k5eAaPmF
nekonečné	konečný	k2eNgFnSc2d1
řady	řada	k1gFnSc2
pro	pro	k7c4
π	π	k?
</s>
<s>
Komplexní	komplexní	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
Eulerův	Eulerův	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
komplexní	komplexní	k2eAgFnSc1d1
rovina	rovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvýšení	zvýšení	k1gNnSc1
úhlu	úhel	k1gInSc2
φ	φ	k?
na	na	k7c4
π	π	k?
radiánů	radián	k1gInPc2
(	(	kIx(
<g/>
180	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
vede	vést	k5eAaImIp3nS
k	k	k7c3
Eulerově	Eulerův	k2eAgFnSc3d1
rovnosti	rovnost	k1gFnSc3
</s>
<s>
Komplexní	komplexní	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
</s>
<s>
z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
z	z	k7c2
<g/>
}	}	kIx)
</s>
<s>
lze	lze	k6eAd1
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
polární	polární	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
souřadnic	souřadnice	k1gFnPc2
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
r	r	kA
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
φ	φ	k?
</s>
<s>
+	+	kIx~
</s>
<s>
i	i	k9
</s>
<s>
sin	sin	kA
</s>
<s>
φ	φ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
z	z	k7c2
<g/>
=	=	kIx~
<g/>
r	r	kA
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
(	(	kIx(
<g/>
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
+	+	kIx~
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
Častý	častý	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
π	π	k?
v	v	k7c6
komplexní	komplexní	k2eAgFnSc6d1
analýze	analýza	k1gFnSc6
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
spojitost	spojitost	k1gFnSc4
s	s	k7c7
chováním	chování	k1gNnSc7
exponenciální	exponenciální	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
komplexní	komplexní	k2eAgFnSc2d1
proměnné	proměnná	k1gFnSc2
popsané	popsaný	k2eAgNnSc1d1
Eulerovým	Eulerův	k2eAgInSc7d1
vzorcem	vzorec	k1gInSc7
</s>
<s>
e	e	k0
</s>
<s>
i	i	k9
</s>
<s>
φ	φ	k?
</s>
<s>
=	=	kIx~
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
φ	φ	k?
</s>
<s>
+	+	kIx~
</s>
<s>
i	i	k9
</s>
<s>
sin	sin	kA
</s>
<s>
φ	φ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
e	e	k0
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
+	+	kIx~
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
varphi	varphi	k6eAd1
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
i	i	k9
je	být	k5eAaImIp3nS
imaginární	imaginární	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
platí	platit	k5eAaImIp3nP
i²	i²	k?
=	=	kIx~
−	−	k?
<g/>
,	,	kIx,
a	a	k8xC
e	e	k0
≈	≈	k?
2,718	2,718	k4
<g/>
28	#num#	k4
je	být	k5eAaImIp3nS
Eulerovo	Eulerův	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
vzorec	vzorec	k1gInSc4
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
imaginární	imaginární	k2eAgFnPc1d1
mocniny	mocnina	k1gFnPc1
e	e	k0
popisují	popisovat	k5eAaImIp3nP
rotaci	rotace	k1gFnSc4
na	na	k7c6
jednotkové	jednotkový	k2eAgFnSc6d1
kružnici	kružnice	k1gFnSc6
v	v	k7c6
komplexní	komplexní	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
<g/>
;	;	kIx,
tyto	tento	k3xDgFnPc1
rotace	rotace	k1gFnPc1
mají	mít	k5eAaImIp3nP
periodu	perioda	k1gFnSc4
360	#num#	k4
<g/>
°	°	k?
=	=	kIx~
2	#num#	k4
<g/>
π	π	k?
Když	když	k8xS
se	se	k3xPyFc4
úhel	úhel	k1gInSc1
φ	φ	k?
rovná	rovnat	k5eAaImIp3nS
π	π	k?
<g/>
,	,	kIx,
vznikne	vzniknout	k5eAaPmIp3nS
Eulerova	Eulerův	k2eAgFnSc1d1
rovnost	rovnost	k1gFnSc1
</s>
<s>
e	e	k0
</s>
<s>
i	i	k9
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
e	e	k0
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
<g/>
=	=	kIx~
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
e	e	k0
</s>
<s>
i	i	k9
</s>
<s>
π	π	k?
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
e	e	k0
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
\	\	kIx~
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
}	}	kIx)
</s>
<s>
Eulerova	Eulerův	k2eAgFnSc1d1
rovnost	rovnost	k1gFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
díky	díky	k7c3
spojení	spojení	k1gNnSc4
několika	několik	k4yIc2
základních	základní	k2eAgInPc2d1
matematických	matematický	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
do	do	k7c2
jednoho	jeden	k4xCgInSc2
krátkého	krátký	k2eAgInSc2d1
elegantního	elegantní	k2eAgInSc2d1
vzorce	vzorec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
n	n	k0
různých	různý	k2eAgFnPc2d1
n-tých	n-tých	k2eAgFnPc2d1
odmocnin	odmocnina	k1gFnPc2
jedničky	jednička	k1gFnSc2
</s>
<s>
e	e	k0
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
/	/	kIx~
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
…	…	k?
</s>
<s>
,	,	kIx,
</s>
<s>
n	n	k0
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
e	e	k0
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
i	i	k8xC
<g/>
}	}	kIx)
k	k	k7c3
<g/>
/	/	kIx~
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
qquad	qquad	k6eAd1
(	(	kIx(
<g/>
k	k	k7c3
<g/>
=	=	kIx~
<g/>
0,1	0,1	k4
<g/>
,2	,2	k4
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
dots	dots	k1gInSc1
,	,	kIx,
<g/>
n-	n-	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1
a	a	k8xC
statistika	statistika	k1gFnSc1
</s>
<s>
V	v	k7c6
pravděpodobnosti	pravděpodobnost	k1gFnSc6
a	a	k8xC
statistice	statistika	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
několik	několik	k4yIc1
vzorců	vzorec	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
obsahují	obsahovat	k5eAaImIp3nP
π	π	k?
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
2	#num#	k4
náhodně	náhodně	k6eAd1
zvolená	zvolený	k2eAgNnPc1d1
celá	celý	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
jsou	být	k5eAaImIp3nP
nesoudělná	soudělný	k2eNgNnPc1d1
<g/>
,	,	kIx,
je	on	k3xPp3gMnPc4
</s>
<s>
6	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
6	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
</s>
<s>
Funkce	funkce	k1gFnSc1
hustoty	hustota	k1gFnSc2
pravděpodobnosti	pravděpodobnost	k1gFnSc2
pro	pro	k7c4
normální	normální	k2eAgNnSc4d1
rozdělení	rozdělení	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
μ	μ	k?
je	být	k5eAaImIp3nS
střední	střední	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
a	a	k8xC
σ	σ	k?
je	být	k5eAaImIp3nS
směrodatná	směrodatný	k2eAgFnSc1d1
odchylka	odchylka	k1gFnSc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
σ	σ	k?
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
e	e	k0
</s>
<s>
−	−	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
−	−	k?
</s>
<s>
μ	μ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
/	/	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
σ	σ	k?
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
1	#num#	k4
\	\	kIx~
<g/>
over	over	k1gInSc1
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
e	e	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
x-	x-	k?
<g/>
\	\	kIx~
<g/>
mu	on	k3xPp3gMnSc3
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
sigma	sigma	k1gNnSc2
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
}}	}}	k?
</s>
<s>
</s>
<s>
Funkce	funkce	k1gFnSc1
hustoty	hustota	k1gFnSc2
pravděpodobnosti	pravděpodobnost	k1gFnSc2
pro	pro	k7c4
(	(	kIx(
<g/>
standardní	standardní	k2eAgInSc4d1
<g/>
)	)	kIx)
Cauchyho	Cauchy	k1gMnSc2
rozdělení	rozdělení	k1gNnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
(	(	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
}}	}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Se	s	k7c7
znalostí	znalost	k1gFnSc7
faktu	fakt	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
libovolnou	libovolný	k2eAgFnSc4d1
hustotu	hustota	k1gFnSc4
pravděpodobnosti	pravděpodobnost	k1gFnSc2
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
∫	∫	k?
</s>
<s>
−	−	k?
</s>
<s>
∞	∞	k?
</s>
<s>
∞	∞	k?
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
int	int	k?
_	_	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
dx	dx	k?
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
lze	lze	k6eAd1
z	z	k7c2
předchozích	předchozí	k2eAgInPc2d1
vzorců	vzorec	k1gInPc2
odvodit	odvodit	k5eAaPmF
další	další	k2eAgInPc4d1
vzorce	vzorec	k1gInPc4
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
čísla	číslo	k1gNnSc2
π	π	k?
</s>
<s>
Buffonova	Buffonův	k2eAgFnSc1d1
jehla	jehla	k1gFnSc1
je	být	k5eAaImIp3nS
úloha	úloha	k1gFnSc1
<g/>
,	,	kIx,
díky	díky	k7c3
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
empiricky	empiricky	k6eAd1
odhadnout	odhadnout	k5eAaPmF
hodnotu	hodnota	k1gFnSc4
π	π	k?
Představme	představit	k5eAaPmRp1nP
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
máme	mít	k5eAaImIp1nP
jehlu	jehla	k1gFnSc4
o	o	k7c6
délce	délka	k1gFnSc6
L	L	kA
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
opakovaně	opakovaně	k6eAd1
házíme	házet	k5eAaImIp1nP
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
rovnoběžnými	rovnoběžný	k2eAgFnPc7d1
linkami	linka	k1gFnPc7
od	od	k7c2
sebe	sebe	k3xPyFc4
vzdálenými	vzdálený	k2eAgFnPc7d1
S	s	k7c7
jednotek	jednotka	k1gFnPc2
(	(	kIx(
<g/>
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
S	s	k7c7
>	>	kIx)
L	L	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
jehla	jehla	k1gFnSc1
hozena	hozen	k2eAgFnSc1d1
n	n	k0
<g/>
–	–	k?
<g/>
krát	krát	k6eAd1
a	a	k8xC
x	x	k?
<g/>
–	–	k?
<g/>
krát	krát	k6eAd1
z	z	k7c2
toho	ten	k3xDgMnSc2
překříží	překřížit	k5eAaPmIp3nS
linku	linka	k1gFnSc4
(	(	kIx(
<g/>
x	x	k?
>	>	kIx)
0	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
potom	potom	k6eAd1
lze	lze	k6eAd1
π	π	k?
přibližně	přibližně	k6eAd1
odhadnout	odhadnout	k5eAaPmF
pomocí	pomocí	k7c2
metody	metoda	k1gFnSc2
Monte	Mont	k1gInSc5
Carlo	Carlo	k1gNnSc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
π	π	k?
</s>
<s>
≈	≈	k?
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
L	L	kA
</s>
<s>
x	x	k?
</s>
<s>
S	s	k7c7
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
\	\	kIx~
<g/>
approx	approx	k1gInSc4
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
nL	nL	k?
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
xS	xS	k?
<g/>
}}	}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
I	i	k9
když	když	k8xS
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
metoda	metoda	k1gFnSc1
matematicky	matematicky	k6eAd1
dokonalá	dokonalý	k2eAgFnSc1d1
<g/>
,	,	kIx,
nelze	lze	k6eNd1
s	s	k7c7
ní	on	k3xPp3gFnSc7
určit	určit	k5eAaPmF
hodnotu	hodnota	k1gFnSc4
π	π	k?
pomocí	pomocí	k7c2
experimentu	experiment	k1gInSc2
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
pár	pár	k4xCyI
číslic	číslice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správné	správný	k2eAgNnSc1d1
určení	určení	k1gNnSc1
pouze	pouze	k6eAd1
tří	tři	k4xCgInPc2
prvních	první	k4xOgInPc2
číslic	číslice	k1gFnPc2
(	(	kIx(
<g/>
3,14	3,14	k4
<g/>
)	)	kIx)
vyžaduje	vyžadovat	k5eAaImIp3nS
miliony	milion	k4xCgInPc4
hodů	hod	k1gInPc2
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
počet	počet	k1gInSc1
hodů	hod	k1gInPc2
roste	růst	k5eAaImIp3nS
exponenciálně	exponenciálně	k6eAd1
s	s	k7c7
počtem	počet	k1gInSc7
požadovaných	požadovaný	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
důležité	důležitý	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
z	z	k7c2
dalších	další	k2eAgInPc2d1
oborů	obor	k1gInPc2
matematiky	matematika	k1gFnSc2
</s>
<s>
Gaussův	Gaussův	k2eAgInSc1d1
integrál	integrál	k1gInSc1
</s>
<s>
∫	∫	k?
</s>
<s>
−	−	k?
</s>
<s>
∞	∞	k?
</s>
<s>
∞	∞	k?
</s>
<s>
e	e	k0
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
π	π	k?
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
int	int	k?
_	_	kIx~
<g/>
{	{	kIx(
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
infty	inft	k1gInPc1
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
e	e	k0
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
-x	-x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}}	}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Důsledkem	důsledek	k1gInSc7
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
gama	gama	k1gNnSc1
funkce	funkce	k1gFnSc2
poloviny	polovina	k1gFnSc2
celého	celý	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
je	být	k5eAaImIp3nS
racionálním	racionální	k2eAgInSc7d1
násobkem	násobek	k1gInSc7
√	√	k?
<g/>
π	π	k?
<g/>
,	,	kIx,
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
Γ	Γ	k?
</s>
<s>
(	(	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
Gamma	Gammum	k1gNnPc4
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
{	{	kIx(
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}}}	}}}	k?
</s>
<s>
Stirlingův	Stirlingův	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
n	n	k0
</s>
<s>
</s>
<s>
∼	∼	k?
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
n	n	k0
</s>
<s>
e	e	k0
</s>
<s>
)	)	kIx)
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
\	\	kIx~
<g/>
sim	sima	k1gFnPc2
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
n	n	k0
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
e	e	k0
<g/>
}	}	kIx)
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}	}}	k?
</s>
<s>
Cauchyův	Cauchyův	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
z	z	k7c2
</s>
<s>
0	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
i	i	k9
</s>
<s>
∮	∮	k?
</s>
<s>
c	c	k0
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
z	z	k7c2
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
z	z	k7c2
</s>
<s>
z	z	k7c2
</s>
<s>
−	−	k?
</s>
<s>
z	z	k7c2
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
(	(	kIx(
<g/>
z_	z_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
}}	}}	k?
<g/>
\	\	kIx~
<g/>
oint	oint	k5eAaPmF
_	_	kIx~
<g/>
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
z	z	k7c2
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
z	z	k7c2
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
z-z_	z-z_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
Fyzika	fyzika	k1gFnSc1
</s>
<s>
I	i	k9
když	když	k8xS
π	π	k?
není	být	k5eNaImIp3nS
fyzikální	fyzikální	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
<g/>
,	,	kIx,
objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
ve	v	k7c6
fyzikálních	fyzikální	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
koeficienty	koeficient	k1gInPc7
obsahující	obsahující	k2eAgInSc4d1
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
vyjadřují	vyjadřovat	k5eAaImIp3nP
jistou	jistý	k2eAgFnSc4d1
geometrickou	geometrický	k2eAgFnSc4d1
symetrii	symetrie	k1gFnSc4
(	(	kIx(
<g/>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
pro	pro	k7c4
kruhovou	kruhový	k2eAgFnSc4d1
či	či	k8xC
válcovou	válcový	k2eAgFnSc4d1
symetrii	symetrie	k1gFnSc4
–	–	k?
plný	plný	k2eAgInSc4d1
rovinný	rovinný	k2eAgInSc4d1
úhel	úhel	k1gInSc4
v	v	k7c6
obloukové	obloukový	k2eAgFnSc6d1
míře	míra	k1gFnSc6
<g/>
,	,	kIx,
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
pro	pro	k7c4
kulovou	kulový	k2eAgFnSc4d1
symetrii	symetrie	k1gFnSc4
–	–	k?
plný	plný	k2eAgInSc4d1
prostorový	prostorový	k2eAgInSc4d1
úhel	úhel	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volbou	volba	k1gFnSc7
jednotek	jednotka	k1gFnPc2
koeficient	koeficient	k1gInSc1
sice	sice	k8xC
může	moct	k5eAaImIp3nS
zmizet	zmizet	k5eAaPmF
u	u	k7c2
jednoho	jeden	k4xCgInSc2
vztahu	vztah	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
objeví	objevit	k5eAaPmIp3nS
se	se	k3xPyFc4
u	u	k7c2
jiného	jiný	k2eAgNnSc2d1
<g/>
,	,	kIx,
takže	takže	k8xS
plně	plně	k6eAd1
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
zbavit	zbavit	k5eAaPmF
nelze	lze	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
soustavy	soustava	k1gFnSc2
jednotek	jednotka	k1gFnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
soustava	soustava	k1gFnSc1
SI	si	k1gNnSc2
pro	pro	k7c4
elektromagnetické	elektromagnetický	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
)	)	kIx)
proto	proto	k8xC
respektují	respektovat	k5eAaImIp3nP
tzv.	tzv.	kA
racionalizaci	racionalizace	k1gFnSc4
<g/>
,	,	kIx,
tzn.	tzn.	kA
že	že	k8xS
koeficienty	koeficient	k1gInPc1
obsahující	obsahující	k2eAgInPc1d1
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
v	v	k7c6
těch	ten	k3xDgInPc6
vztazích	vztah	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
geometricky	geometricky	k6eAd1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
oprávněné	oprávněný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Níže	nízce	k6eAd2
uvedený	uvedený	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
pro	pro	k7c4
Coulombovu	Coulombův	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
(	(	kIx(
<g/>
všesměrové	všesměrový	k2eAgNnSc4d1
působení	působení	k1gNnSc4
do	do	k7c2
plného	plný	k2eAgInSc2d1
prostorového	prostorový	k2eAgInSc2d1
úhlu	úhel	k1gInSc2
<g/>
)	)	kIx)
proto	proto	k8xC
koeficient	koeficient	k1gInSc1
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
obsahuje	obsahovat	k5eAaImIp3nS
„	„	k?
<g/>
oprávněně	oprávněně	k6eAd1
<g/>
“	“	k?
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
eliminace	eliminace	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
Gaussově	Gaussův	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
jednotek	jednotka	k1gFnPc2
CGS	CGS	kA
<g/>
)	)	kIx)
pak	pak	k6eAd1
vede	vést	k5eAaImIp3nS
k	k	k7c3
„	„	k?
<g/>
podivným	podivný	k2eAgInPc3d1
<g/>
“	“	k?
koeficientům	koeficient	k1gInPc3
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
ve	v	k7c6
vztazích	vztah	k1gInPc6
pro	pro	k7c4
elektrickou	elektrický	k2eAgFnSc4d1
indukci	indukce	k1gFnSc4
a	a	k8xC
v	v	k7c6
Maxwellových	Maxwellův	k2eAgFnPc6d1
rovnicích	rovnice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
stejného	stejný	k2eAgInSc2d1
důvodu	důvod	k1gInSc2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
faktor	faktor	k1gInSc1
2	#num#	k4
<g/>
·	·	k?
<g/>
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
v	v	k7c6
níže	nízce	k6eAd2
uvedené	uvedený	k2eAgFnSc6d1
Einsteinově	Einsteinův	k2eAgFnSc6d1
rovnici	rovnice	k1gFnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
vztah	vztah	k1gInSc1
pro	pro	k7c4
Newtonův	Newtonův	k2eAgInSc4d1
gravitační	gravitační	k2eAgInSc4d1
zákon	zákon	k1gInSc4
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
neracionalizovaný	racionalizovaný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Koeficienty	koeficient	k1gInPc1
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
i	i	k9
u	u	k7c2
některých	některý	k3yIgFnPc2
rovnic	rovnice	k1gFnPc2
periodických	periodický	k2eAgInPc2d1
dějů	děj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
fáze	fáze	k1gFnSc1
jedné	jeden	k4xCgFnSc2
periody	perioda	k1gFnSc2
se	se	k3xPyFc4
tradičně	tradičně	k6eAd1
pokládá	pokládat	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
,	,	kIx,
tedy	tedy	k8xC
plnému	plný	k2eAgInSc3d1
úhlu	úhel	k1gInSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
zajištěna	zajištěn	k2eAgFnSc1d1
korespondence	korespondence	k1gFnSc1
průmětu	průmět	k1gInSc2
rovnoměrného	rovnoměrný	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
po	po	k7c6
kružnici	kružnice	k1gFnSc6
do	do	k7c2
jedné	jeden	k4xCgFnSc2
osy	osa	k1gFnSc2
souřadné	souřadný	k2eAgFnSc2d1
s	s	k7c7
harmonickým	harmonický	k2eAgInSc7d1
kmitavým	kmitavý	k2eAgInSc7d1
pohybem	pohyb	k1gInSc7
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
i	i	k9
původ	původ	k1gInSc4
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
v	v	k7c6
níže	nízce	k6eAd2
uvedené	uvedený	k2eAgFnSc6d1
relaci	relace	k1gFnSc6
neurčitosti	neurčitost	k1gFnSc2
<g/>
,	,	kIx,
použije	použít	k5eAaPmIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
namísto	namísto	k7c2
„	„	k?
<g/>
přirozené	přirozený	k2eAgNnSc1d1
<g/>
“	“	k?
modifikované	modifikovaný	k2eAgFnPc4d1
Planckovy	Planckův	k2eAgFnPc4d1
konstanty	konstanta	k1gFnPc4
konstanta	konstanta	k1gFnSc1
nemodifikovaná	modifikovaný	k2eNgFnSc1d1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Hodnotu	hodnota	k1gFnSc4
lze	lze	k6eAd1
získat	získat	k5eAaPmF
i	i	k9
z	z	k7c2
různých	různý	k2eAgInPc2d1
experimentů	experiment	k1gInPc2
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
pozorování	pozorování	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příklady	příklad	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Heisenbergův	Heisenbergův	k2eAgInSc1d1
princip	princip	k1gInSc1
neurčitosti	neurčitost	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nepřesnosti	nepřesnost	k1gFnSc3
měření	měření	k1gNnSc2
polohy	poloha	k1gFnSc2
částice	částice	k1gFnSc2
(	(	kIx(
<g/>
Δ	Δ	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
hybnosti	hybnost	k1gFnSc2
(	(	kIx(
<g/>
Δ	Δ	k1gInSc1
<g/>
)	)	kIx)
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
zároveň	zároveň	k6eAd1
libovolně	libovolně	k6eAd1
malé	malý	k2eAgNnSc1d1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Δ	Δ	k?
</s>
<s>
x	x	k?
</s>
<s>
Δ	Δ	k?
</s>
<s>
p	p	k?
</s>
<s>
≥	≥	k?
</s>
<s>
h	h	k?
</s>
<s>
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
=	=	kIx~
</s>
<s>
ℏ	ℏ	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
Delta	delta	k1gFnSc1
x	x	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
\	\	kIx~
<g/>
Delta	delta	k1gFnSc1
p	p	k?
<g/>
\	\	kIx~
<g/>
geq	geq	k?
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
h	h	k?
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
hbar	hbar	k1gInSc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
</s>
<s>
Einsteinovy	Einsteinův	k2eAgFnPc1d1
rovnice	rovnice	k1gFnPc1
gravitačního	gravitační	k2eAgNnSc2d1
pole	pole	k1gNnSc2
v	v	k7c6
obecné	obecný	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
relativity	relativita	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
R	R	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
−	−	k?
</s>
<s>
g	g	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
Λ	Λ	k?
</s>
<s>
g	g	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
=	=	kIx~
</s>
<s>
8	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
G	G	kA
</s>
<s>
c	c	k0
</s>
<s>
4	#num#	k4
</s>
<s>
T	T	kA
</s>
<s>
i	i	k9
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
R_	R_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}	}	kIx)
<g/>
-	-	kIx~
<g/>
{	{	kIx(
<g/>
g_	g_	k?
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}	}	kIx)
<g/>
R	R	kA
\	\	kIx~
<g/>
over	over	k1gInSc1
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
Lambda	lambda	k1gNnSc1
g_	g_	k?
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
8	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
G	G	kA
\	\	kIx~
<g/>
over	over	k1gMnSc1
c	c	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
}}	}}	k?
<g/>
T_	T_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
ik	ik	k?
<g/>
}}	}}	k?
</s>
<s>
Kosmologická	kosmologický	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
Λ	Λ	k?
z	z	k7c2
Einsteinovy	Einsteinův	k2eAgFnSc2d1
rovnice	rovnice	k1gFnSc2
gravitačního	gravitační	k2eAgNnSc2d1
pole	pole	k1gNnSc2
je	být	k5eAaImIp3nS
ve	v	k7c6
vztahu	vztah	k1gInSc6
s	s	k7c7
hustotou	hustota	k1gFnSc7
energie	energie	k1gFnSc2
vakua	vakuum	k1gNnSc2
ρ	ρ	k6eAd1
následovně	následovně	k6eAd1
(	(	kIx(
<g/>
G	G	kA
je	být	k5eAaImIp3nS
gravitační	gravitační	k2eAgFnSc1d1
konstanta	konstanta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Λ	Λ	k?
</s>
<s>
=	=	kIx~
</s>
<s>
8	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
G	G	kA
</s>
<s>
ρ	ρ	k?
</s>
<s>
vac	vac	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
Lambda	lambda	k1gNnPc6
=	=	kIx~
<g/>
8	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
G	G	kA
<g/>
\	\	kIx~
\	\	kIx~
<g/>
rho	rho	k?
_	_	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
text	text	k1gInSc1
<g/>
{	{	kIx(
<g/>
vac	vac	k?
<g/>
}}}	}}}	k?
</s>
<s>
Coulombův	Coulombův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
elektrické	elektrický	k2eAgFnSc2d1
síly	síla	k1gFnSc2
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
elektrickými	elektrický	k2eAgInPc7d1
náboji	náboj	k1gInPc7
(	(	kIx(
<g/>
q	q	k?
<g/>
1	#num#	k4
a	a	k8xC
q	q	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
od	od	k7c2
sebe	sebe	k3xPyFc4
vzdálených	vzdálený	k2eAgInPc2d1
délkou	délka	k1gFnSc7
r	r	kA
(	(	kIx(
<g/>
ε	ε	k?
<g/>
0	#num#	k4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
permitivitu	permitivita	k1gFnSc4
vakua	vakuum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
q	q	k?
</s>
<s>
1	#num#	k4
</s>
<s>
q	q	k?
</s>
<s>
2	#num#	k4
</s>
<s>
|	|	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
ε	ε	k?
</s>
<s>
0	#num#	k4
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
left	left	k1gInSc1
<g/>
|	|	kIx~
<g/>
q_	q_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
q_	q_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
|	|	kIx~
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
4	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
\	\	kIx~
<g/>
varepsilon	varepsilon	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
Ampérův	Ampérův	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
pro	pro	k7c4
magnetickou	magnetický	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
působící	působící	k2eAgFnSc7d1
na	na	k7c6
délce	délka	k1gFnSc6
</s>
<s>
l	l	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
l	l	kA
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
rovnoběžnými	rovnoběžný	k2eAgInPc7d1
vodiči	vodič	k1gInPc7
vzdálenými	vzdálený	k2eAgInPc7d1
od	od	k7c2
sebe	sebe	k3xPyFc4
o	o	k7c4
kolmou	kolmý	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
</s>
<s>
d	d	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
d	d	k?
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
protékanými	protékaný	k2eAgInPc7d1
proudy	proud	k1gInPc7
</s>
<s>
I	i	k9
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I_	I_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
I	i	k9
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I_	I_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
}	}	kIx)
</s>
<s>
:	:	kIx,
</s>
<s>
F	F	kA
</s>
<s>
l	l	kA
</s>
<s>
=	=	kIx~
</s>
<s>
μ	μ	k?
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
I	i	k9
</s>
<s>
1	#num#	k4
</s>
<s>
I	i	k9
</s>
<s>
2	#num#	k4
</s>
<s>
d	d	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
F	F	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
l	l	kA
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mu	on	k3xPp3gMnSc3
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}}	}}	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
I_	I_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
I_	I_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
d	d	k?
<g/>
}}}	}}}	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerův	Keplerův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
doby	doba	k1gFnSc2
oběhu	oběh	k1gInSc2
(	(	kIx(
<g/>
T	T	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velké	velký	k2eAgFnSc2d1
poloosy	poloosa	k1gFnSc2
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
a	a	k8xC
hmotností	hmotnost	k1gFnSc7
(	(	kIx(
<g/>
M	M	kA
a	a	k8xC
m	m	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
(	(	kIx(
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
T	T	kA
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
3	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
ω	ω	k?
</s>
<s>
2	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
3	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
G	G	kA
</s>
<s>
(	(	kIx(
</s>
<s>
M	M	kA
</s>
<s>
+	+	kIx~
</s>
<s>
m	m	kA
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
<g/>
{	{	kIx(
<g/>
T	T	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
,	,	kIx,
<g/>
a	a	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
a	a	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
G	G	kA
<g/>
(	(	kIx(
<g/>
M	M	kA
<g/>
+	+	kIx~
<g/>
m	m	kA
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
Memorování	memorování	k1gNnSc1
číslic	číslice	k1gFnPc2
</s>
<s>
Nynější	nynější	k2eAgInSc1d1
světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
v	v	k7c6
Guinnessově	Guinnessův	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
rekordů	rekord	k1gInPc2
drží	držet	k5eAaImIp3nS
Rajveer	Rajveer	k1gInSc1
Meena	Meeno	k1gNnSc2
z	z	k7c2
Indie	Indie	k1gFnSc2
se	se	k3xPyFc4
70	#num#	k4
000	#num#	k4
zapamatovanými	zapamatovaný	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvalo	trvat	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
9	#num#	k4
hodin	hodina	k1gFnPc2
27	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahradil	nahradit	k5eAaPmAgMnS
tak	tak	k9
Číňana	Číňan	k1gMnSc4
Lu	Lu	k1gMnSc1
Chao	Chao	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
67	#num#	k4
890	#num#	k4
zapamatovaných	zapamatovaný	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Recitování	recitování	k1gNnSc2
mu	on	k3xPp3gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
trvalo	trvat	k5eAaImAgNnS
24	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
4	#num#	k4
minuty	minuta	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Rekord	rekord	k1gInSc1
japonského	japonský	k2eAgMnSc2d1
inženýra	inženýr	k1gMnSc2
Akira	Akir	k1gMnSc2
Haragučiho	Haraguči	k1gMnSc2
100	#num#	k4
000	#num#	k4
číslic	číslice	k1gFnPc2
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
nebyl	být	k5eNaImAgInS
zatím	zatím	k6eAd1
Guinnessovou	Guinnessův	k2eAgFnSc7d1
knihou	kniha	k1gFnSc7
uznán	uznat	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
způsobů	způsob	k1gInPc2
zapamatování	zapamatování	k1gNnSc2
si	se	k3xPyFc3
co	co	k9
nejvíce	hodně	k6eAd3,k6eAd1
číslic	číslice	k1gFnPc2
desetinného	desetinný	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
π	π	k?
<g/>
,	,	kIx,
například	například	k6eAd1
tzv.	tzv.	kA
piemy	piem	k1gInPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jsou	být	k5eAaImIp3nP
básně	báseň	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
délka	délka	k1gFnSc1
každého	každý	k3xTgNnSc2
slova	slovo	k1gNnSc2
reprezentuje	reprezentovat	k5eAaImIp3nS
číslici	číslice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Báseň	báseň	k1gFnSc1
Cadaeic	Cadaeice	k1gFnPc2
Cadenza	Cadenza	k1gFnSc1
takto	takto	k6eAd1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
prvních	první	k4xOgNnPc6
3834	#num#	k4
číslic	číslice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Existují	existovat	k5eAaImIp3nP
i	i	k9
různé	různý	k2eAgFnPc1d1
mnemotechnické	mnemotechnický	k2eAgFnPc1d1
pomůcky	pomůcka	k1gFnPc1
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yIgFnPc3,k3yQgFnPc3,k3yRgFnPc3
si	se	k3xPyFc3
lze	lze	k6eAd1
několik	několik	k4yIc4
číslic	číslice	k1gFnPc2
π	π	k?
zapamatovat	zapamatovat	k5eAaPmF
<g/>
:	:	kIx,
</s>
<s>
Sám	sám	k3xTgInSc4
u	u	k7c2
sebe	se	k3xPyFc2
v	v	k7c6
hlavě	hlava	k1gFnSc6
magického	magický	k2eAgNnSc2d1
pí	pí	k1gNnSc2
číslic	číslice	k1gFnPc2
deset	deset	k4xCc4
mám	mít	k5eAaImIp1nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
9	#num#	k4
za	za	k7c7
desetinnou	desetinný	k2eAgFnSc7d1
čárkou	čárka	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Lín	lín	k1gMnSc1
a	a	k8xC
kapr	kapr	k1gMnSc1
u	u	k7c2
hráze	hráz	k1gFnSc2
prohlídli	prohlídnout	k5eAaPmAgMnP
si	se	k3xPyFc3
rybáře	rybář	k1gMnSc4
<g/>
,	,	kIx,
udici	udice	k1gFnSc4
měl	mít	k5eAaImAgMnS
novou	nový	k2eAgFnSc4d1
<g/>
,	,	kIx,
jikrnáči	jikrnáč	k1gMnPc1
neuplovou	uplovat	k5eNaPmIp3nP
<g/>
.	.	kIx.
(	(	kIx(
<g/>
12	#num#	k4
za	za	k7c7
desetinnou	desetinný	k2eAgFnSc7d1
čárkou	čárka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Jiná	jiný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Rak	rak	k1gMnSc1
a	a	k8xC
kapr	kapr	k1gMnSc1
u	u	k7c2
hráze	hráz	k1gFnSc2
pohleděli	pohledět	k5eAaPmAgMnP
na	na	k7c4
rybáře	rybář	k1gMnSc4
<g/>
,	,	kIx,
udici	udice	k1gFnSc4
měl	mít	k5eAaImAgMnS
novou	nový	k2eAgFnSc4d1
<g/>
,	,	kIx,
šupináči	šupináč	k1gMnPc1
neuplavou	uplavat	k5eNaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Dej	dát	k5eAaPmRp2nS
<g/>
,	,	kIx,
ó	ó	k0
Bože	bůh	k1gMnSc5
<g/>
,	,	kIx,
a	a	k8xC
číslo	číslo	k1gNnSc1
Ludolfovo	Ludolfův	k2eAgNnSc1d1
já	já	k3xPp1nSc1
navždy	navždy	k6eAd1
pomnu	pomnout	k5eAaPmIp1nS
<g/>
,	,	kIx,
pro	pro	k7c4
větší	veliký	k2eAgNnSc4d2
naplnění	naplnění	k1gNnSc4
moudrosti	moudrost	k1gFnSc2
početní	početní	k2eAgInPc1d1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
13	#num#	k4
číslic	číslice	k1gFnPc2
za	za	k7c7
desetinnou	desetinný	k2eAgFnSc7d1
čárkou	čárka	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Mám	mít	k5eAaImIp1nS
ó	ó	k0
bože	bůh	k1gMnSc5
ó	ó	k0
velký	velký	k2eAgMnSc1d1
pamatovat	pamatovat	k5eAaImF
si	se	k3xPyFc3
takový	takový	k3xDgInSc4
cifer	cifra	k1gFnPc2
řád	řád	k1gInSc1
<g/>
,	,	kIx,
velký	velký	k2eAgMnSc1d1
slovutný	slovutný	k2eAgMnSc1d1
Archimedes	Archimedes	k1gMnSc1
<g/>
,	,	kIx,
pomáhej	pomáhat	k5eAaImRp2nS
trápenému	trápený	k2eAgInSc3d1
<g/>
,	,	kIx,
dej	dát	k5eAaPmRp2nS
mu	on	k3xPp3gMnSc3
moc	moc	k6eAd1
<g/>
,	,	kIx,
nazpaměť	nazpaměť	k6eAd1
nechť	nechť	k9
odříká	odříkat	k5eAaImIp3nS,k5eAaPmIp3nS
ty	ten	k3xDgFnPc4
slavné	slavný	k2eAgFnPc4d1
sice	sice	k8xC
<g/>
,	,	kIx,
ale	ale	k8xC
tak	tak	k6eAd1
protivné	protivný	k2eAgNnSc1d1
nám	my	k3xPp1nPc3
<g/>
,	,	kIx,
ach	ach	k0
<g/>
,	,	kIx,
číslice	číslice	k1gFnPc1
Ludolfovy	Ludolfův	k2eAgFnPc1d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
30	#num#	k4
číslic	číslice	k1gFnPc2
za	za	k7c7
desetinnou	desetinný	k2eAgFnSc7d1
čárkou	čárka	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Pi	pi	k0
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
PICKOVER	PICKOVER	kA
<g/>
,	,	kIx,
Clifford	Clifford	k1gInSc1
A.	A.	kA
A	a	k9
passion	passion	k1gInSc4
for	forum	k1gNnPc2
mathematics	mathematics	k6eAd1
<g/>
:	:	kIx,
numbers	numbers	k1gInSc1
<g/>
,	,	kIx,
puzzles	puzzles	k1gInSc1
<g/>
,	,	kIx,
madness	madness	k1gInSc1
<g/>
,	,	kIx,
religion	religion	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
the	the	k?
quest	quest	k1gFnSc1
for	forum	k1gNnPc2
reality	realita	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
John	John	k1gMnSc1
Wiley	Wilea	k1gFnSc2
and	and	k?
Sons	Sons	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
471	#num#	k4
<g/>
-	-	kIx~
<g/>
69098	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
52	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Extract	Extract	k1gInSc1
of	of	k?
page	pagat	k5eAaPmIp3nS
52	#num#	k4
</s>
<s>
↑	↑	k?
BERGGREN	BERGGREN	kA
<g/>
,	,	kIx,
Lennart	Lennart	k1gInSc1
<g/>
;	;	kIx,
BORWEIN	BORWEIN	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
;	;	kIx,
BORWEIN	BORWEIN	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pi	pi	k0
<g/>
:	:	kIx,
A	a	k9
Source	Source	k1gMnSc1
Book	Book	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
387	#num#	k4
<g/>
-	-	kIx~
<g/>
98946	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
POSAMENTIER	POSAMENTIER	kA
<g/>
,	,	kIx,
Alfred	Alfred	k1gMnSc1
<g/>
;	;	kIx,
LEHMANN	LEHMANN	kA
<g/>
,	,	kIx,
Ingmar	Ingmar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pi	pi	k0
<g/>
:	:	kIx,
A	a	k9
Biography	Biographa	k1gMnSc2
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Most	most	k1gInSc1
Mysterious	Mysterious	k1gMnSc1
Number	Number	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Pormetheus	Pormetheus	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59102	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
“	“	k?
<g/>
Man	Man	k1gMnSc1
recites	recites	k1gMnSc1
pi	pi	k0
from	from	k6eAd1
memory	memor	k1gInPc4
to	ten	k3xDgNnSc1
83,431	83,431	k4
places	places	k1gMnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
MSNBC	MSNBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SCHUDEL	SCHUDEL	kA
<g/>
,	,	kIx,
Matt	Matt	k1gMnSc1
<g/>
.	.	kIx.
“	“	k?
<g/>
John	John	k1gMnSc1
W.	W.	kA
Wrench	Wrench	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Mathematician	Mathematician	k1gMnSc1
Had	had	k1gMnSc1
a	a	k8xC
Taste	tasit	k5eAaPmRp2nP
for	forum	k1gNnPc2
Pi	pi	k0
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
“	“	k?
<g/>
The	The	k1gFnSc2
Big	Big	k1gMnSc1
Question	Question	k1gInSc1
<g/>
:	:	kIx,
How	How	k1gFnSc1
close	close	k1gFnSc1
have	have	k1gFnSc1
we	we	k?
come	comat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
knowing	knowing	k1gInSc1
the	the	k?
precise	precise	k1gFnSc1
value	value	k1gFnSc1
of	of	k?
pi	pi	k0
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Pi	pi	k0
<g/>
,	,	kIx,
a	a	k8xC
mathematical	mathematicat	k5eAaPmAgMnS
story	story	k1gFnSc4
that	that	k2eAgMnSc1d1
would	would	k1gMnSc1
take	tak	k1gInSc2
49,000	49,000	k4
years	years	k6eAd1
to	ten	k3xDgNnSc4
tell	tell	k1gMnSc1
<g/>
.	.	kIx.
www.timesonline.co.uk	www.timesonline.co.uk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnota	hodnota	k1gFnSc1
pí	pí	k1gNnSc2
má	mít	k5eAaImIp3nS
novou	nový	k2eAgFnSc4d1
rekordní	rekordní	k2eAgFnSc4d1
délku	délka	k1gFnSc4
<g/>
,	,	kIx,
31	#num#	k4
bilionů	bilion	k4xCgInPc2
číslic	číslice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČTK	ČTK	kA
<g/>
,	,	kIx,
2019-03-15	2019-03-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MIHULKA	mihulka	k1gFnSc1
<g/>
,	,	kIx,
Stanislav	Stanislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonská	japonský	k2eAgFnSc1d1
matematička	matematička	k1gFnSc1
spočítala	spočítat	k5eAaPmAgFnS
rekordně	rekordně	k6eAd1
dlouhé	dlouhý	k2eAgNnSc4d1
pí	pí	k1gNnSc4
<g/>
:	:	kIx,
Má	mít	k5eAaImIp3nS
31	#num#	k4
bilionů	bilion	k4xCgInPc2
číslic	číslice	k1gFnPc2
<g/>
.	.	kIx.
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
zahraniční	zahraniční	k2eAgFnSc4d1
zajímavost	zajímavost	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-19	2019-03-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
MOLINA	molino	k1gNnSc2
<g/>
,	,	kIx,
Brett	Brett	k1gMnSc1
<g/>
;	;	kIx,
Usa	Usa	k1gMnSc1
Today	Todaa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pi	pi	k0
has	hasit	k5eAaImRp2nS
been	been	k1gMnSc1
calculated	calculated	k1gMnSc1
out	out	k?
to	ten	k3xDgNnSc1
31.4	31.4	k4
trillion	trillion	k1gInSc1
decimals	decimals	k1gInSc4
<g/>
,	,	kIx,
Google	Google	k1gInSc4
announces	announcesa	k1gFnPc2
on	on	k3xPp3gMnSc1
Pi	pi	k0
Day	Day	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
TechXplore	TechXplor	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-15	2019-03-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CUYT	CUYT	kA
<g/>
,	,	kIx,
Annie	Annie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Handbook	handbook	k1gInSc1
of	of	k?
continued	continued	k1gInSc1
fractions	fractionsa	k1gFnPc2
for	forum	k1gNnPc2
special	special	k1gMnSc1
functions	functions	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Ask	Ask	k1gFnPc2
Dr	dr	kA
<g/>
.	.	kIx.
Math	Math	k1gMnSc1
FAQ	FAQ	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Beckmann	Beckmann	k1gNnSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
121	#num#	k4
<g/>
↑	↑	k?
RICHMOND	RICHMOND	kA
<g/>
,	,	kIx,
Bettina	Bettina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Area	area	k1gFnSc1
of	of	k?
a	a	k8xC
Circle	Circle	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Western	western	k1gInSc1
Kentucky	Kentucka	k1gFnSc2
University	universita	k1gFnSc2
<g/>
,	,	kIx,
1999-01-12	1999-01-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RUDIN	RUDIN	kA
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Principles	Principles	k1gMnSc1
of	of	k?
Mathematical	Mathematical	k1gFnSc1
Analysis	Analysis	k1gFnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
e.	e.	k?
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
McGraw-Hill	McGraw-Hill	k1gMnSc1
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
54235	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
183	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
MAYER	Mayer	k1gMnSc1
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Transcendence	transcendence	k1gFnSc1
of	of	k?
π	π	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Squaring	Squaring	k1gInSc1
the	the	k?
Circle	Circle	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
cut-the-knot	cut-the-knota	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Beckmann	Beckmann	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
34	#num#	k4
<g/>
↑	↑	k?
Schlager	Schlager	k1gMnSc1
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
<g/>
;	;	kIx,
Lauer	Lauer	k1gMnSc1
<g/>
,	,	kIx,
Josh	Josh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
and	and	k?
Its	Its	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
:	:	kIx,
Understanding	Understanding	k1gInSc1
the	the	k?
Social	Social	k1gInSc1
Significance	Significance	k1gFnSc2
of	of	k?
Scientific	Scientific	k1gMnSc1
Discovery	Discovera	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Gale	Gale	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Science	Science	k1gFnSc1
and	and	k?
Its	Its	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
787639338	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
185	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
A	a	k9
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
796	#num#	k4
<g/>
:	:	kIx,
Decimal	Decimal	k1gInSc1
expansion	expansion	k1gInSc1
of	of	k?
Pi	pi	k0
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
On-Line	On-Lin	k1gInSc5
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Integer	Integer	k1gMnSc1
Sequences	Sequences	k1gMnSc1
<g/>
↑	↑	k?
Pí	pí	kA
na	na	k7c4
milion	milion	k4xCgInSc1
desetinných	desetinný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
↑	↑	k?
“	“	k?
<g/>
Pi	pi	k0
to	ten	k3xDgNnSc1
More	mor	k1gInSc5
Decimal	Decimal	k1gMnSc1
Places	Placesa	k1gFnPc2
Than	Than	k1gMnSc1
You	You	k1gMnSc1
Will	Will	k1gMnSc1
Ever	Ever	k1gMnSc1
Need	Need	k1gMnSc1
<g/>
”	”	k?
<g/>
,	,	kIx,
University	universita	k1gFnPc1
of	of	k?
Exeter	Exeter	k1gInSc1
<g/>
,	,	kIx,
School	School	k1gInSc1
of	of	k?
Physics	Physics	k1gInSc1
<g/>
,	,	kIx,
Quantum	Quantum	k1gNnSc1
Physics	Physics	k1gInSc1
and	and	k?
Nanomaterials	Nanomaterials	k1gInSc1
Group	Group	k1gInSc1
(	(	kIx(
<g/>
poskytuje	poskytovat	k5eAaImIp3nS
pí	pí	k1gNnPc4
na	na	k7c4
milion	milion	k4xCgInSc4
číslic	číslice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NEAGLE	NEAGLE	kA
<g/>
,	,	kIx,
Mia	Mia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
recipe	recipe	k1gNnSc4
for	forum	k1gNnPc2
beating	beating	k1gInSc1
the	the	k?
record	record	k1gInSc1
of	of	k?
most-calculated	most-calculated	k1gInSc1
digits	digits	k1gInSc1
of	of	k?
pi	pi	k0
<g/>
.	.	kIx.
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
google	google	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
PORTER	porter	k1gInSc1
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
employee	employee	k1gFnPc2
calculates	calculatesa	k1gFnPc2
pi	pi	k0
to	ten	k3xDgNnSc1
record	record	k1gMnSc1
31	#num#	k4
trillion	trillion	k1gInSc1
digits	digits	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Verge	Verge	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Guinness	Guinness	k1gInSc1
world	world	k1gMnSc1
records	records	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Current	Current	k1gMnSc1
publicized	publicized	k1gMnSc1
world	world	k1gMnSc1
record	record	k1gMnSc1
of	of	k?
pi	pi	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
YOUNG	YOUNG	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
M.	M.	kA
Excursions	Excursions	k1gInSc1
in	in	k?
Calculus	Calculus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
:	:	kIx,
Mathematical	Mathematical	k1gFnSc1
Association	Association	k1gInSc1
of	of	k?
America	America	k1gFnSc1
(	(	kIx(
<g/>
MAA	MAA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
883853175	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
417	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Pí	pí	k1gNnSc1
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c6
encyklopedii	encyklopedie	k1gFnSc6
MathWorld	MathWorlda	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
BOUTIN	BOUTIN	kA
<g/>
,	,	kIx,
Chad	Chad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pi	pi	k0
seems	seems	k6eAd1
a	a	k8xC
good	good	k6eAd1
random	random	k1gInSc1
number	numbra	k1gFnPc2
generator	generator	k1gInSc4
–	–	k?
but	but	k?
not	nota	k1gFnPc2
always	always	k6eAd1
the	the	k?
best	best	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
uns	uns	k?
<g/>
.	.	kIx.
<g/>
purdue	purdue	k1gFnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Purdue	Purdue	k1gNnSc7
University	universita	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexander	Alexandra	k1gFnPc2
D.	D.	kA
Poularikas	Poularikas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
handbook	handbook	k1gInSc4
of	of	k?
formulas	formulas	k1gInSc1
and	and	k?
tables	tables	k1gMnSc1
for	forum	k1gNnPc2
signal	signat	k5eAaPmAgMnS,k5eAaImAgMnS
processing	processing	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
CRC	CRC	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780849385797	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9.8	9.8	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sample	Sample	k1gFnSc2
digits	digits	k6eAd1
for	forum	k1gNnPc2
hexa	hexa	k6eAd1
decimal	decimat	k5eAaBmAgInS,k5eAaPmAgInS,k5eAaImAgInS
digits	digits	k1gInSc1
of	of	k?
pi	pi	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
December	December	k1gInSc1
6	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
GOURDON	GOURDON	kA
<g/>
,	,	kIx,
Xavier	Xavier	k1gMnSc1
<g/>
,	,	kIx,
Pascal	Pascal	k1gMnSc1
Sebah	Sebah	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Collection	Collection	k1gInSc1
of	of	k?
approximations	approximations	k6eAd1
for	forum	k1gNnPc2
π	π	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Numbers	Numbers	k1gInSc1
<g/>
,	,	kIx,
constants	constants	k1gInSc1
and	and	k?
computation	computation	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
A	a	k9
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1203	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Continued	Continued	k1gInSc4
fraction	fraction	k1gInSc4
for	forum	k1gNnPc2
Pi	pi	k0
<g/>
,	,	kIx,
On-Line	On-Lin	k1gInSc5
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Integer	Integer	k1gMnSc1
Sequences	Sequences	k1gMnSc1
<g/>
↑	↑	k?
WRENCH	WRENCH	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Mathematics	Mathematicsa	k1gFnPc2
Teacher	Teachra	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
evolution	evolution	k1gInSc1
of	of	k?
extended	extended	k1gInSc1
decimal	decimat	k5eAaBmAgInS,k5eAaPmAgInS,k5eAaImAgInS
approximations	approximations	k6eAd1
to	ten	k3xDgNnSc4
π	π	k?
<g/>
,	,	kIx,
s.	s.	k?
644	#num#	k4
<g/>
–	–	k?
<g/>
650	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GROLEAU	GROLEAU	kA
<g/>
,	,	kIx,
Rick	Rick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infinite	Infinit	k1gInSc5
Secrets	Secrets	k1gInSc1
<g/>
:	:	kIx,
Approximating	Approximating	k1gInSc1
Pi	pi	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NOVA	nova	k1gFnSc1
<g/>
,	,	kIx,
09-2003	09-2003	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Beckmann	Beckmann	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
54	#num#	k4
<g/>
↑	↑	k?
WEISSTEIN	WEISSTEIN	kA
<g/>
,	,	kIx,
Eric	Eric	k1gInSc1
W.	W.	kA
Pi	pi	k0
Formulas	Formulas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MathWorld	MathWorlda	k1gFnPc2
<g/>
,	,	kIx,
2007-09-27	2007-09-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
EYMARD	EYMARD	kA
<g/>
,	,	kIx,
Pierre	Pierr	k1gMnSc5
<g/>
,	,	kIx,
Jean-Pierre	Jean-Pierr	k1gMnSc5
Lafon	Lafon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Number	Numbra	k1gFnPc2
π	π	k?
Stephen	Stephna	k1gFnPc2
S.	S.	kA
Wilson	Wilson	k1gMnSc1
(	(	kIx(
<g/>
translator	translator	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
American	American	k1gInSc1
Mathematical	Mathematical	k1gFnSc2
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
821832468	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
2.6	2.6	k4
<g/>
,	,	kIx,
s.	s.	k?
53	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
LAMPRET	LAMPRET	kA
<g/>
,	,	kIx,
SPANISH	SPANISH	kA
<g/>
,	,	kIx,
Vito	vit	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Even	Even	k1gMnSc1
from	from	k1gMnSc1
Gregory-Leibniz	Gregory-Leibniz	k1gMnSc1
series	series	k1gMnSc1
π	π	k?
could	could	k1gMnSc1
be	be	k?
computed	computed	k1gMnSc1
<g/>
:	:	kIx,
an	an	k?
example	example	k6eAd1
of	of	k?
how	how	k?
convergence	convergence	k1gFnSc2
of	of	k?
series	series	k1gMnSc1
can	can	k?
be	be	k?
accelerated	accelerated	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lecturas	Lecturas	k1gMnSc1
Mathematicas	Mathematicas	k1gMnSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
21	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.scm.org.co	www.scm.org.co	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
A.	A.	kA
van	van	k1gInSc4
Wijngaarden	Wijngaardna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cursus	Cursus	k1gInSc1
<g/>
:	:	kIx,
Wetenschappelijk	Wetenschappelijk	k1gInSc1
Rekenen	Rekenen	k1gInSc1
B	B	kA
<g/>
,	,	kIx,
Process	Process	k1gInSc1
Analyse	analysa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stichting	Stichting	k1gInSc1
Mathematisch	Mathematisch	k1gInSc4
Centrum	centrum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1965	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
51	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Verner	Verner	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
str	str	kA
<g/>
.	.	kIx.
70	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Petrie	Petrie	k1gFnSc1
Wisdom	Wisdom	k1gInSc1
of	of	k?
the	the	k?
Egyptians	Egyptians	k1gInSc1
1940	#num#	k4
<g/>
:	:	kIx,
30	#num#	k4
<g/>
↑	↑	k?
ROSSI	ROSSI	kA
<g/>
,	,	kIx,
Corinna	Corinno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architecture	Architectur	k1gMnSc5
and	and	k?
Mathematics	Mathematics	k1gInSc1
in	in	k?
Ancient	Ancient	k1gInSc1
Egypt	Egypt	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
521690539	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Beckmann	Beckmann	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
12	#num#	k4
<g/>
↑	↑	k?
Beckmann	Beckmann	k1gNnSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
65	#num#	k4
<g/>
↑	↑	k?
Beckmann	Beckmann	k1gNnSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
54	#num#	k4
a	a	k8xC
56	#num#	k4
<g/>
↑	↑	k?
Beckmann	Beckmann	k1gNnSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
63	#num#	k4
<g/>
–	–	k?
<g/>
641	#num#	k4
2	#num#	k4
Beckmann	Beckmanna	k1gFnPc2
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
24	#num#	k4
<g/>
↑	↑	k?
Komentář	komentář	k1gInSc4
k	k	k7c3
Mišne	Mišn	k1gInSc5
Tora	Tor	k1gMnSc2
<g/>
,	,	kIx,
začátek	začátek	k1gInSc4
Eruvin	Eruvina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Beckmann	Beckmann	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
83	#num#	k4
<g/>
↑	↑	k?
NIVEN	NIVEN	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
simple	simple	k6eAd1
proof	proof	k1gMnSc1
that	that	k1gMnSc1
π	π	k?
is	is	k?
irrational	irrationat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Mathematical	Mathematical	k1gMnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
1947	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
509	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9904	#num#	k4
<g/>
-	-	kIx~
<g/>
1947	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8821	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
RICHTER	Richter	k1gMnSc1
<g/>
,	,	kIx,
Helmut	Helmut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pi	pi	k0
Is	Is	k1gFnSc3
Irrational	Irrational	k1gFnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leibniz	Leibniz	k1gMnSc1
Rechenzentrum	Rechenzentrum	k1gNnSc1
<g/>
,	,	kIx,
1999-07-28	1999-07-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
JEFFREYS	JEFFREYS	kA
<g/>
,	,	kIx,
Harold	Harold	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scientific	Scientifice	k1gInPc2
Inference	inference	k1gFnSc2
<g/>
.	.	kIx.
3	#num#	k4
<g/>
rd	rd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
521084466	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GEORGE	GEORGE	kA
E.	E.	kA
ANDREWS	ANDREWS	kA
<g/>
,	,	kIx,
RICHARD	Richard	k1gMnSc1
ASKEY	ASKEY	kA
<g/>
,	,	kIx,
Ranjan	Ranjan	k1gMnSc1
Roy	Roy	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Special	Special	k1gInSc1
Functions	Functions	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
521789885	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
58	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GUPTA	GUPTA	kA
<g/>
,	,	kIx,
R.	R.	kA
C.	C.	kA
On	on	k3xPp3gInSc1
the	the	k?
remainder	remainder	k1gInSc1
term	term	k1gInSc1
in	in	k?
the	the	k?
Madhava-Leibniz	Madhava-Leibniz	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
series	seriesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ganita	Ganita	k1gFnSc1
Bharati	Bharat	k1gMnPc1
<g/>
.	.	kIx.
1992	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
68	#num#	k4
<g/>
–	–	k?
<g/>
71	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Beckmenn	Beckmenna	k1gFnPc2
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
85	#num#	k4
<g/>
–	–	k?
<g/>
86	#num#	k4
<g/>
↑	↑	k?
Beckmann	Beckmann	k1gNnSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
79	#num#	k4
<g/>
↑	↑	k?
GLEICK	GLEICK	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Even	Even	k1gNnSc1
Mathematicians	Mathematiciansa	k1gFnPc2
Can	Can	k1gFnSc2
Get	Get	k1gFnPc1
Carried	Carried	k1gInSc4
Away	Awaa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
1987-03-08	1987-03-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Beckmann	Beckmann	k1gInSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
141	#num#	k4
<g/>
–	–	k?
<g/>
142	#num#	k4
<g/>
↑	↑	k?
Beckmann	Beckmann	k1gNnSc1
1998	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1521	#num#	k4
2	#num#	k4
The	The	k1gMnSc1
constant	constant	k1gMnSc1
π	π	k?
<g/>
:	:	kIx,
Ramanujan	Ramanujan	k1gMnSc1
type	typ	k1gInSc5
formulas	formulasit	k5eAaPmRp2nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc1
world	world	k1gMnSc1
of	of	k?
Pi	pi	k0
–	–	k?
Simon	Simon	k1gMnSc1
Plouffe	Plouff	k1gInSc5
/	/	kIx~
David	David	k1gMnSc1
Bailey	Bailea	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pi	pi	k0
<g/>
314	#num#	k4
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Collection	Collection	k1gInSc1
of	of	k?
series	series	k1gInSc1
for	forum	k1gNnPc2
$	$	kIx~
<g/>
\	\	kIx~
<g/>
pi	pi	k0
$	$	kIx~
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Numbers	Numbers	k1gInSc1
<g/>
.	.	kIx.
<g/>
computation	computation	k1gInSc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
5	#num#	k4
Trillion	Trillion	k1gInSc1
Digits	Digits	k1gInSc4
of	of	k?
Pi	pi	k0
–	–	k?
New	New	k1gMnSc1
World	World	k1gMnSc1
Record	Record	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Round	round	k1gInSc1
2	#num#	k4
<g/>
...	...	k?
10	#num#	k4
Trillion	Trillion	k1gInSc1
Digits	Digits	k1gInSc4
of	of	k?
Pi	pi	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
12.1	12.1	k4
Trillion	Trillion	k1gInSc1
Digits	Digits	k1gInSc4
of	of	k?
Pi	pi	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BRENT	BRENT	k?
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Multiple-precision	Multiple-precision	k1gInSc1
zero-finding	zero-finding	k1gInSc4
methods	methods	k1gInSc1
and	and	k?
the	the	k?
complexity	complexita	k1gFnSc2
of	of	k?
elementary	elementara	k1gFnSc2
function	function	k1gInSc1
evaluation	evaluation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Traub	Trauba	k1gFnPc2
J	J	kA
F.	F.	kA
Analytic	Analytice	k1gFnPc2
Computational	Computational	k1gFnPc2
Complexity	Complexita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academic	Academice	k1gInPc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
151	#num#	k4
<g/>
–	–	k?
<g/>
176	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BORWEIN	BORWEIN	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
M	M	kA
<g/>
,	,	kIx,
Borwein	Borwein	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
;	;	kIx,
Berggren	Berggrna	k1gFnPc2
<g/>
,	,	kIx,
Lennart	Lennarta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pi	pi	k0
<g/>
:	:	kIx,
A	a	k9
Source	Source	k1gMnSc1
Book	Book	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Springer	Springer	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
387205713	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Bailey	Bailey	k1gInPc7
<g/>
,	,	kIx,
David	David	k1gMnSc1
H.	H.	kA
<g/>
;	;	kIx,
Borwein	Borwein	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
B.	B.	kA
<g/>
;	;	kIx,
and	and	k?
Plouffe	Plouff	k1gMnSc5
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gInSc1
the	the	k?
Rapid	rapid	k1gInSc1
Computation	Computation	k1gInSc1
of	of	k?
Various	Various	k1gInSc1
Polylogarithmic	Polylogarithmic	k1gMnSc1
Constants	Constants	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mathematics	Mathematics	k1gInSc1
of	of	k?
Computation	Computation	k1gInSc1
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
903	#num#	k4
<g/>
–	–	k?
<g/>
913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
25	#num#	k4
<g/>
-	-	kIx~
<g/>
5718	#num#	k4
<g/>
-	-	kIx~
<g/>
97	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
856	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
</s>
<s>
BELLARD	BELLARD	kA
<g/>
,	,	kIx,
Fabrice	fabrika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
new	new	k?
formula	formula	k1gFnSc1
to	ten	k3xDgNnSc4
compute	comput	k1gInSc5
the	the	k?
nth	nth	k?
binary	binara	k1gFnSc2
digit	digit	k1gMnSc1
of	of	k?
pi	pi	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
LANGE	LANGE	kA
<g/>
,	,	kIx,
L.	L.	kA
J.	J.	kA
An	An	k1gMnSc1
Elegant	elegant	k1gMnSc1
Continued	Continued	k1gMnSc1
Fraction	Fraction	k1gInSc4
for	forum	k1gNnPc2
π	π	k?
The	The	k1gMnSc1
American	American	k1gMnSc1
Mathematical	Mathematical	k1gMnSc1
Monthly	Monthly	k1gMnSc1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
456	#num#	k4
<g/>
–	–	k?
<g/>
458	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2589152	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WEISSTEIN	WEISSTEIN	kA
<g/>
,	,	kIx,
Eric	Eric	k1gInSc1
W.	W.	kA
Normal	Normal	k1gInSc1
Number	Number	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MathWorld	MathWorlda	k1gFnPc2
<g/>
,	,	kIx,
2005-12-22	2005-12-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PREUSS	PREUSS	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Are	ar	k1gInSc5
The	The	k1gFnPc3
Digits	Digits	k1gInSc1
of	of	k?
Pi	pi	k0
Random	Random	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Lab	Lab	k1gFnSc1
Researcher	Researchra	k1gFnPc2
May	May	k1gMnSc1
Hold	hold	k1gInSc4
The	The	k1gFnSc2
Key	Key	k1gFnSc1
<g/>
.	.	kIx.
www.lbl.gov	www.lbl.gov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lawrence	Lawrence	k1gFnSc1
Berkeley	Berkelea	k1gFnSc2
National	National	k1gFnSc2
Laboratory	Laborator	k1gInPc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nesterenko	Nesterenka	k1gFnSc5
<g/>
,	,	kIx,
Yuri	Yur	k1gMnSc3
V.	V.	kA
Modular	Modular	k1gInSc1
Functions	Functions	k1gInSc1
and	and	k?
Transcendence	transcendence	k1gFnSc2
Problems	Problemsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comptes	Comptes	k1gMnSc1
rendus	rendus	k1gMnSc1
de	de	k?
l	l	kA
<g/>
’	’	k?
<g/>
Académie	Académie	k1gFnSc2
des	des	k1gNnSc2
sciences	sciences	k1gInSc1
Série	série	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
909	#num#	k4
<g/>
–	–	k?
<g/>
914	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Howard	Howard	k1gInSc1
Whitley	Whitlea	k1gFnSc2
Eves	Evesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gFnPc2
Introduction	Introduction	k1gInSc4
to	ten	k3xDgNnSc1
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Mathematics	Mathematics	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Holt	Holt	k?
<g/>
,	,	kIx,
Rinehart	Rinehart	k1gInSc1
&	&	k?
Winston	Winston	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Japanese	Japanese	k1gFnSc1
breaks	breaks	k6eAd1
pi	pi	k0
memory	memora	k1gFnPc4
record	record	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Area	area	k1gFnSc1
and	and	k?
Circumference	Circumference	k1gFnSc2
of	of	k?
a	a	k8xC
Circle	Circle	k1gInSc4
by	by	kYmCp3nS
Archimedes	Archimedes	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Penn	Penn	k1gNnSc1
State	status	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WEISSTEIN	WEISSTEIN	kA
<g/>
,	,	kIx,
Eric	Eric	k1gInSc1
W.	W.	kA
Unit	Unit	k2eAgInSc1d1
Disk	disk	k1gInSc1
Integral	Integral	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MathWorld	MathWorlda	k1gFnPc2
<g/>
,	,	kIx,
2006-01-28	2006-01-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WEISSTEIN	WEISSTEIN	kA
<g/>
,	,	kIx,
Eric	Eric	k1gInSc1
W.	W.	kA
Solid	solid	k1gInSc1
of	of	k?
Revolution	Revolution	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MathWorld	MathWorlda	k1gFnPc2
<g/>
,	,	kIx,
2006-05-04	2006-05-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WEISSTEIN	WEISSTEIN	kA
<g/>
,	,	kIx,
Eric	Eric	k1gInSc1
W.	W.	kA
Gaussian	Gaussian	k1gInSc1
Integral	Integral	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MathWorld	MathWorlda	k1gFnPc2
<g/>
,	,	kIx,
2004-10-07	2004-10-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WEISSTEIN	WEISSTEIN	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
W.	W.	kA
Cauchy	Caucha	k1gFnPc1
Distribution	Distribution	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MathWorld	MathWorlda	k1gFnPc2
<g/>
,	,	kIx,
2005-10-11	2005-10-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
WEISSTEIN	WEISSTEIN	kA
<g/>
,	,	kIx,
Eric	Eric	k1gInSc1
W.	W.	kA
Buffon	Buffon	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Needle	Needle	k1gMnSc7
Problem	Probl	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MathWorld	MathWorlda	k1gFnPc2
<g/>
,	,	kIx,
2005-12-12	2005-12-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BOGOMOLNY	BOGOMOLNY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
cut-the-knot	cut-the-knota	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2001-08	2001-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
RAMALEY	RAMALEY	kA
<g/>
,	,	kIx,
J.	J.	kA
F.	F.	kA
Buffon	Buffon	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Noodle	Noodle	k1gMnSc7
Problem	Probl	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
American	American	k1gMnSc1
Mathematical	Mathematical	k1gMnSc1
Monthly	Monthly	k1gMnSc1
<g/>
.	.	kIx.
1969	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
916	#num#	k4
<g/>
–	–	k?
<g/>
918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2317945	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
datastructures	datastructures	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-01-09	2007-01-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://is.muni.cz/th/345337/pedf_b_b1/	http://is.muni.cz/th/345337/pedf_b_b1/	k4
-	-	kIx~
Číslo	číslo	k1gNnSc4
pí	pí	k1gNnSc2
v	v	k7c6
učivu	učivo	k1gNnSc6
matematiky	matematika	k1gFnSc2
na	na	k7c6
Základní	základní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
↑	↑	k?
http://www.scienceworld.cz/neziva-priroda/pi-na-nebesich/	http://www.scienceworld.cz/neziva-priroda/pi-na-nebesich/	k?
-	-	kIx~
Pí	pí	k1gNnSc1
na	na	k7c6
nebesích	nebesa	k1gNnPc6
<g/>
↑	↑	k?
IMAMURA	IMAMURA	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
M.	M.	kA
Heisenberg	Heisenberg	k1gMnSc1
Uncertainty	Uncertainta	k1gFnSc2
Principle	Principle	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
Oregon	Oregon	k1gMnSc1
<g/>
,	,	kIx,
2005-08-17	2005-08-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
EINSTEIN	Einstein	k1gMnSc1
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Foundation	Foundation	k1gInSc4
of	of	k?
the	the	k?
General	General	k1gMnSc1
Theory	Theora	k1gFnSc2
of	of	k?
Relativity	relativita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annalen	Annalen	k2eAgInSc1d1
der	drát	k5eAaImRp2nS
Physik	Physik	k1gMnSc1
<g/>
.	.	kIx.
1916	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.alberteinstein.info	www.alberteinstein.info	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PERKINS	PERKINS	kA
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
H.	H.	kA
Particle	Particle	k1gInSc1
astrophysics	astrophysics	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
nd	nd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Oxford	Oxford	k1gInSc1
master	master	k1gMnSc1
series	series	k1gMnSc1
in	in	k?
particle	particle	k1gInSc1
physics	physics	k1gInSc1
<g/>
,	,	kIx,
astrophysics	astrophysics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
cosmology	cosmolog	k1gMnPc7
<g/>
;	;	kIx,
sv.	sv.	kA
10	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
199545456	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
184	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NAVE	NAVE	kA
<g/>
,	,	kIx,
C.	C.	kA
Rod	rod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HyperPhysics	HyperPhysics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Georgia	Georgia	k1gFnSc1
State	status	k1gInSc5
University	universita	k1gFnSc2
<g/>
,	,	kIx,
2005-06-28	2005-06-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Pi	pi	k0
World	World	k1gInSc4
Ranking	Ranking	k1gInSc1
List	list	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chinese	Chinese	k1gFnSc2
student	student	k1gMnSc1
breaks	breaksa	k1gFnPc2
Guiness	Guiness	k1gMnSc1
record	record	k1gMnSc1
by	by	k9
reciting	reciting	k1gInSc4
67,890	67,890	k4
digits	digits	k1gInSc1
of	of	k?
pi	pi	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
News	News	k1gInSc1
Guangdong	Guangdong	k1gInSc4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
OTAKE	OTAKE	kA
<g/>
,	,	kIx,
Tomoko	Tomoko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
can	can	k?
anyone	anyon	k1gInSc5
remember	remembra	k1gFnPc2
100,000	100,000	k4
numbers	numbersa	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Japan	japan	k1gInSc4
Times	Times	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KEITH	KEITH	kA
<g/>
,	,	kIx,
Mike	Mike	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cadaeic	Cadaeice	k1gInPc2
Cadenza	Cadenza	k1gFnSc1
Notes	notes	k1gInSc1
&	&	k?
Commentary	Commentara	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BECKMANN	BECKMANN	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
čísla	číslo	k1gNnSc2
π	π	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
655	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
pí	pí	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
pí	pí	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
http://numbers.computation.free.fr/Constants/Pi/pi.html	http://numbers.computation.free.fr/Constants/Pi/pi.htmnout	k5eAaPmAgMnS
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Digits	Digits	k1gInSc1
of	of	k?
Pi	pi	k0
na	na	k7c4
Open	Open	k1gNnSc4
Directory	Director	k1gInPc4
Project	Project	k2eAgInSc4d1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Formulas	Formulasa	k1gFnPc2
for	forum	k1gNnPc2
π	π	k?
na	na	k7c4
MathWorld	MathWorld	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Representations	Representations	k1gInSc1
of	of	k?
Pi	pi	k0
na	na	k7c4
Wolfram	wolfram	k1gInSc4
Alpha	Alph	k1gMnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Pi	pi	k0
na	na	k7c4
PlanetMath	PlanetMath	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Determination	Determination	k1gInSc1
of	of	k?
π	π	k?
na	na	k7c6
Cut-the-knot	Cut-the-knota	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Statistical	Statistical	k1gFnSc1
Distribution	Distribution	k1gInSc1
Information	Information	k1gInSc1
on	on	k3xPp3gMnSc1
PI	pi	k0
based	based	k1gInSc1
on	on	k3xPp3gInSc1
1.2	1.2	k4
trillion	trillion	k1gInSc1
digits	digits	k1gInSc4
of	of	k?
PI	pi	k0
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Pi	pi	k0
Search	Search	k1gMnSc1
Engine	Engin	k1gInSc5
(	(	kIx(
<g/>
2	#num#	k4
billion	billion	k1gInSc1
digits	digits	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Pi	pi	k0
is	is	k?
Wrong	Wronga	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
The	The	k1gFnSc1
Tau	tau	k1gNnSc2
Manifesto	Manifesta	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
A	a	k9
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
796	#num#	k4
v	v	k7c6
OEIS	OEIS	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4174646-6	4174646-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85101712	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85101712	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
