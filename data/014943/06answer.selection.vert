<s desamb="1">
Její	její	k3xOp3gFnSc1
hodnota	hodnota	k1gFnSc1
v	v	k7c6
desítkové	desítkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
3,141	3,141	k4
<g/>
59265359	#num#	k4
(	(	kIx(
<g/>
lze	lze	k6eAd1
použít	použít	k5eAaPmF
praktické	praktický	k2eAgFnPc4d1
racionální	racionální	k2eAgFnPc4d1
aproximace	aproximace	k1gFnPc4
22	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
pro	pro	k7c4
orientační	orientační	k2eAgInPc4d1
výpočty	výpočet	k1gInPc4
vyžadující	vyžadující	k2eAgFnSc4d1
přesnost	přesnost	k1gFnSc4
hodnoty	hodnota	k1gFnSc2
pouze	pouze	k6eAd1
na	na	k7c4
setiny	setina	k1gFnPc4
resp.	resp.	kA
355	#num#	k4
<g/>
/	/	kIx~
<g/>
113	#num#	k4
pro	pro	k7c4
přesnost	přesnost	k1gFnSc4
pouze	pouze	k6eAd1
na	na	k7c4
miliontiny	miliontina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>