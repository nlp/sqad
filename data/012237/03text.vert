<p>
<s>
Dřevnice	Dřevnice	k1gFnSc1	Dřevnice
je	být	k5eAaImIp3nS	být
levostranný	levostranný	k2eAgInSc4d1	levostranný
přítok	přítok	k1gInSc4	přítok
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
je	být	k5eAaImIp3nS	být
42,3	[number]	k4	42,3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
436,5	[number]	k4	436,5
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Řeka	řeka	k1gFnSc1	řeka
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Hostýnských	hostýnský	k2eAgInPc2d1	hostýnský
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Držková	držkový	k2eAgNnPc1d1	držkový
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
vlastní	vlastní	k2eAgFnSc2d1	vlastní
obce	obec	k1gFnSc2	obec
Držková	držkový	k2eAgFnSc1d1	Držková
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
551	[number]	k4	551
m.	m.	k?	m.
Protéká	protékat	k5eAaImIp3nS	protékat
Slušovicemi	Slušovice	k1gFnPc7	Slušovice
<g/>
,	,	kIx,	,
Zlínem	Zlín	k1gInSc7	Zlín
a	a	k8xC	a
Otrokovicemi	Otrokovice	k1gFnPc7	Otrokovice
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
zleva	zleva	k6eAd1	zleva
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
182	[number]	k4	182
m.	m.	k?	m.
Podél	podél	k7c2	podél
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
vedou	vést	k5eAaImIp3nP	vést
trasy	trasa	k1gFnPc1	trasa
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
a	a	k8xC	a
cykloturisty	cykloturista	k1gMnPc4	cykloturista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Větší	veliký	k2eAgInPc1d2	veliký
přítoky	přítok	k1gInPc1	přítok
===	===	k?	===
</s>
</p>
<p>
<s>
levé	levá	k1gFnPc1	levá
–	–	k?	–
Trnávka	Trnávka	k1gFnSc1	Trnávka
<g/>
,	,	kIx,	,
Všeminka	Všeminka	k1gFnSc1	Všeminka
<g/>
,	,	kIx,	,
Lutoninka	Lutoninka	k1gFnSc1	Lutoninka
<g/>
,	,	kIx,	,
Obůrek	obůrek	k1gInSc1	obůrek
</s>
</p>
<p>
<s>
pravé	pravý	k2eAgInPc1d1	pravý
–	–	k?	–
Ostratky	Ostratek	k1gInPc1	Ostratek
<g/>
,	,	kIx,	,
Fryštácký	Fryštácký	k2eAgInSc1d1	Fryštácký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Racková	Racková	k1gFnSc1	Racková
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
3,15	[number]	k4	3,15
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Hlásný	hlásný	k2eAgInSc1d1	hlásný
profil	profil	k1gInSc1	profil
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Splavnění	splavnění	k1gNnSc2	splavnění
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
splavnění	splavnění	k1gNnSc1	splavnění
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
cca	cca	kA	cca
1	[number]	k4	1
km	km	kA	km
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vybudováním	vybudování	k1gNnSc7	vybudování
Baťova	Baťův	k2eAgInSc2d1	Baťův
kanálu	kanál	k1gInSc2	kanál
<g/>
,	,	kIx,	,
sloužícím	sloužící	k1gMnSc7	sloužící
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
lignitu	lignit	k1gInSc2	lignit
z	z	k7c2	z
Ratíškovických	Ratíškovický	k2eAgInPc2d1	Ratíškovický
dolů	dol	k1gInPc2	dol
u	u	k7c2	u
Hodonína	Hodonín	k1gInSc2	Hodonín
do	do	k7c2	do
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Baťových	Baťových	k2eAgInPc6d1	Baťových
závodech	závod	k1gInPc6	závod
v	v	k7c6	v
Baťově	Baťův	k2eAgFnSc6d1	Baťova
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Otrokovice	Otrokovice	k1gFnPc4	Otrokovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Splavný	splavný	k2eAgInSc1d1	splavný
byl	být	k5eAaImAgInS	být
úsek	úsek	k1gInSc1	úsek
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
Dřevnice	Dřevnice	k1gFnSc2	Dřevnice
do	do	k7c2	do
Moravy	Morava	k1gFnSc2	Morava
k	k	k7c3	k
továrně	továrna	k1gFnSc3	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
splavněný	splavněný	k2eAgInSc4d1	splavněný
úsek	úsek	k1gInSc4	úsek
Dřevnice	Dřevnice	k1gFnSc2	Dřevnice
navazoval	navazovat	k5eAaImAgMnS	navazovat
760	[number]	k4	760
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
kanál	kanál	k1gInSc4	kanál
s	s	k7c7	s
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vedl	vést	k5eAaImAgInS	vést
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
továrny	továrna	k1gFnSc2	továrna
kolem	kolem	k7c2	kolem
koželužen	koželužna	k1gFnPc2	koželužna
k	k	k7c3	k
elektrárně	elektrárna	k1gFnSc3	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
byl	být	k5eAaImAgInS	být
100	[number]	k4	100
m	m	kA	m
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
18	[number]	k4	18
m	m	kA	m
široký	široký	k2eAgMnSc1d1	široký
a	a	k8xC	a
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
kotvení	kotvení	k1gNnSc3	kotvení
až	až	k9	až
8	[number]	k4	8
nákladních	nákladní	k2eAgInPc2d1	nákladní
38	[number]	k4	38
m	m	kA	m
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
překonání	překonání	k1gNnSc4	překonání
výškového	výškový	k2eAgInSc2d1	výškový
rozdílu	rozdíl	k1gInSc2	rozdíl
mezi	mezi	k7c7	mezi
hladinou	hladina	k1gFnSc7	hladina
Dřevnice	Dřevnice	k1gFnSc2	Dřevnice
a	a	k8xC	a
kanálem	kanál	k1gInSc7	kanál
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
plavební	plavební	k2eAgFnSc1d1	plavební
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
patrná	patrný	k2eAgFnSc1d1	patrná
v	v	k7c6	v
Přístavní	přístavní	k2eAgFnSc6d1	přístavní
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
a	a	k8xC	a
přístav	přístav	k1gInSc1	přístav
byly	být	k5eAaImAgFnP	být
zasypány	zasypat	k5eAaPmNgInP	zasypat
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rybolov	rybolov	k1gInSc4	rybolov
==	==	k?	==
</s>
</p>
<p>
<s>
Dřevnice	Dřevnice	k1gFnSc1	Dřevnice
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
až	až	k9	až
po	po	k7c4	po
most	most	k1gInSc4	most
u	u	k7c2	u
vlakové	vlakový	k2eAgFnSc2d1	vlaková
zastávky	zastávka	k1gFnSc2	zastávka
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Příluky	Příluk	k1gMnPc4	Příluk
voda	voda	k1gFnSc1	voda
pstruhová	pstruhový	k2eAgFnSc1d1	pstruhová
<g/>
,	,	kIx,	,
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
až	až	k9	až
po	po	k7c6	po
ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
Moravy	Morava	k1gFnSc2	Morava
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
voda	voda	k1gFnSc1	voda
mimopstruhová	mimopstruhový	k2eAgFnSc1d1	mimopstruhový
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
hojných	hojný	k2eAgInPc6d1	hojný
počtech	počet	k1gInPc6	počet
parmy	parma	k1gFnSc2	parma
<g/>
,	,	kIx,	,
hrouzci	hrouzek	k1gMnPc1	hrouzek
a	a	k8xC	a
jelci	jelec	k1gMnPc1	jelec
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
v	v	k7c6	v
dolních	dolní	k2eAgFnPc6d1	dolní
částech	část	k1gFnPc6	část
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
ostroretka	ostroretka	k1gFnSc1	ostroretka
stěhovavá	stěhovavý	k2eAgFnSc1d1	stěhovavá
či	či	k8xC	či
kapr	kapr	k1gMnSc1	kapr
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
proudnějších	proudný	k2eAgFnPc6d2	proudný
částech	část	k1gFnPc6	část
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
pstruzi	pstruh	k1gMnPc1	pstruh
potoční	potoční	k2eAgMnPc1d1	potoční
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
rychlejšího	rychlý	k2eAgInSc2d2	rychlejší
a	a	k8xC	a
menšího	malý	k2eAgInSc2d2	menší
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ideální	ideální	k2eAgFnPc4d1	ideální
rybolovné	rybolovný	k2eAgFnPc4d1	rybolovná
techniky	technika	k1gFnPc4	technika
patří	patřit	k5eAaImIp3nS	patřit
přívlač	přívlač	k1gFnSc1	přívlač
<g/>
,	,	kIx,	,
plavaná	plavaná	k1gFnSc1	plavaná
<g/>
,	,	kIx,	,
muškaření	muškaření	k1gNnSc1	muškaření
a	a	k8xC	a
feeder	feeder	k1gInSc1	feeder
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dřevnice	Dřevnice	k1gFnSc2	Dřevnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zlín	Zlín	k1gInSc1	Zlín
–	–	k?	–
aktuální	aktuální	k2eAgInSc1d1	aktuální
vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Povodí	povodí	k1gNnSc2	povodí
Moravy	Morava	k1gFnSc2	Morava
</s>
</p>
<p>
<s>
Pramen	pramen	k1gInSc1	pramen
Dřevnice	Dřevnice	k1gFnSc2	Dřevnice
</s>
</p>
