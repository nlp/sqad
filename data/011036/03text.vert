<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Liška	Liška	k1gMnSc1	Liška
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1972	[number]	k4	1972
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
divadelní	divadelní	k2eAgMnSc1d1	divadelní
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
JAMU	jam	k1gInSc2	jam
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
brněnském	brněnský	k2eAgNnSc6d1	brněnské
HaDivadle	HaDivadlo	k1gNnSc6	HaDivadlo
i	i	k8xC	i
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
Romea	Romeo	k1gMnSc2	Romeo
v	v	k7c6	v
Shakespearově	Shakespearův	k2eAgFnSc6d1	Shakespearova
hře	hra	k1gFnSc6	hra
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
filmovou	filmový	k2eAgFnSc7d1	filmová
rolí	role	k1gFnSc7	role
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
v	v	k7c6	v
Gedeonově	Gedeonův	k2eAgInSc6d1	Gedeonův
Návratu	návrat	k1gInSc6	návrat
idiota	idiot	k1gMnSc2	idiot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
manželkou	manželka	k1gFnSc7	manželka
dcera	dcera	k1gFnSc1	dcera
politického	politický	k2eAgMnSc2d1	politický
aktivisty	aktivista	k1gMnSc2	aktivista
Johna	John	k1gMnSc2	John
Boka	boka	k1gFnSc1	boka
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnSc1	tanečnice
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
Kristýna	Kristýna	k1gFnSc1	Kristýna
Liška	liška	k1gFnSc1	liška
Boková	bokový	k2eAgFnSc1d1	Boková
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Šimon	Šimon	k1gMnSc1	Šimon
Samuel	Samuel	k1gMnSc1	Samuel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
partnerkou	partnerka	k1gFnSc7	partnerka
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Barbora	Barbora	k1gFnSc1	Barbora
Poláková	Poláková	k1gFnSc1	Poláková
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
Návrat	návrat	k1gInSc1	návrat
idiota	idiot	k1gMnSc2	idiot
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyhnání	vyhnání	k1gNnSc1	vyhnání
z	z	k7c2	z
ráje	ráj	k1gInSc2	ráj
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Divoké	divoký	k2eAgFnPc1d1	divoká
včely	včela	k1gFnPc1	včela
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pupendo	Pupendo	k1gNnSc1	Pupendo
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nuda	nuda	k1gFnSc1	nuda
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mazaný	mazaný	k2eAgMnSc1d1	mazaný
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čert	čert	k1gMnSc1	čert
ví	vědět	k5eAaImIp3nS	vědět
proč	proč	k6eAd1	proč
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Horem	horem	k6eAd1	horem
pádem	pád	k1gInSc7	pád
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Štěstí	štěstí	k1gNnSc1	štěstí
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Šílení	šílení	k1gNnSc1	šílení
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Účastníci	účastník	k1gMnPc1	účastník
zájezdu	zájezd	k1gInSc2	zájezd
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ábelov	Ábelov	k1gInSc1	Ábelov
čierny	čierna	k1gFnSc2	čierna
pes	pes	k1gMnSc1	pes
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Venkovský	venkovský	k2eAgMnSc1d1	venkovský
učitel	učitel	k1gMnSc1	učitel
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nestyda	nestyda	k1gFnSc1	nestyda
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
princů	princ	k1gMnPc2	princ
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polski	Polski	k6eAd1	Polski
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marta	Marta	k1gFnSc1	Marta
&	&	k?	&
Věra	Věra	k1gFnSc1	Věra
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
–	–	k?	–
Brouček	Brouček	k1gMnSc1	Brouček
</s>
</p>
<p>
<s>
Tři	tři	k4xCgMnPc1	tři
bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
-	-	kIx~	-
hlas	hlas	k1gInSc1	hlas
holuba	holub	k1gMnSc2	holub
</s>
</p>
<p>
<s>
Svatojánský	svatojánský	k2eAgInSc1d1	svatojánský
věneček	věneček	k1gInSc1	věneček
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
–	–	k?	–
Markýz	markýz	k1gMnSc1	markýz
</s>
</p>
<p>
<s>
Zloději	zloděj	k1gMnPc1	zloděj
zelených	zelený	k2eAgMnPc2d1	zelený
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Anděl	Anděl	k1gMnSc1	Anděl
Páně	páně	k2eAgMnSc1d1	páně
2	[number]	k4	2
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Špunti	Špunti	k?	Špunti
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pavel	Pavla	k1gFnPc2	Pavla
Liška	liška	k1gFnSc1	liška
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Liška	Liška	k1gMnSc1	Liška
na	na	k7c4	na
PORT	port	k1gInSc4	port
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Liška	Liška	k1gMnSc1	Liška
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Liška	Liška	k1gMnSc1	Liška
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
