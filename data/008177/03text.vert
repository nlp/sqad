<p>
<s>
<g/>
cz	cz	k?	cz
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc1d1	národní
doména	doména	k1gFnSc1	doména
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
řádu	řád	k1gInSc2	řád
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Doménu	doména	k1gFnSc4	doména
spravuje	spravovat	k5eAaImIp3nS	spravovat
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
<g/>
NIC	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Registrace	registrace	k1gFnSc1	registrace
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
pouze	pouze	k6eAd1	pouze
přes	přes	k7c4	přes
akreditované	akreditovaný	k2eAgMnPc4d1	akreditovaný
registrátory	registrátor	k1gMnPc4	registrátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
rozdělením	rozdělení	k1gNnSc7	rozdělení
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
doména	doména	k1gFnSc1	doména
.	.	kIx.	.
<g/>
cs	cs	k?	cs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Registrace	registrace	k1gFnSc1	registrace
domény	doména	k1gFnSc2	doména
==	==	k?	==
</s>
</p>
<p>
<s>
Zájemce	zájemce	k1gMnSc1	zájemce
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
domény	doména	k1gFnSc2	doména
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
nejprve	nejprve	k6eAd1	nejprve
vybrat	vybrat	k5eAaPmF	vybrat
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
akreditovaných	akreditovaný	k2eAgMnPc2d1	akreditovaný
registrátorů	registrátor	k1gMnPc2	registrátor
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
něhož	jenž	k3xRgInSc2	jenž
bude	být	k5eAaImBp3nS	být
veškeré	veškerý	k3xTgNnSc1	veškerý
administrativní	administrativní	k2eAgNnSc1d1	administrativní
i	i	k9	i
finanční	finanční	k2eAgFnPc4d1	finanční
záležitosti	záležitost	k1gFnPc4	záležitost
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Majitelem	majitel	k1gMnSc7	majitel
domény	doména	k1gFnSc2	doména
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
fyzická	fyzický	k2eAgFnSc1d1	fyzická
i	i	k8xC	i
právnická	právnický	k2eAgFnSc1d1	právnická
osoba	osoba	k1gFnSc1	osoba
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
uvést	uvést	k5eAaPmF	uvést
svůj	svůj	k3xOyFgInSc4	svůj
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
identifikační	identifikační	k2eAgInSc4d1	identifikační
údaj	údaj	k1gInSc4	údaj
(	(	kIx(	(
<g/>
datum	datum	k1gNnSc4	datum
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
průkazu	průkaz	k1gInSc2	průkaz
totožnosti	totožnost	k1gFnSc2	totožnost
<g/>
,	,	kIx,	,
IČO	IČO	kA	IČO
<g/>
)	)	kIx)	)
a	a	k8xC	a
souhlasit	souhlasit	k5eAaImF	souhlasit
s	s	k7c7	s
pravidly	pravidlo	k1gNnPc7	pravidlo
registrace	registrace	k1gFnSc2	registrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
provoz	provoz	k1gInSc4	provoz
domény	doména	k1gFnSc2	doména
.	.	kIx.	.
<g/>
cz	cz	k?	cz
se	se	k3xPyFc4	se
standardně	standardně	k6eAd1	standardně
platí	platit	k5eAaImIp3nS	platit
roční	roční	k2eAgInSc1d1	roční
poplatek	poplatek	k1gInSc1	poplatek
<g/>
,	,	kIx,	,
samotné	samotný	k2eAgNnSc1d1	samotné
zřízení	zřízení	k1gNnSc1	zřízení
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Velkoobchodní	velkoobchodní	k2eAgFnSc1d1	velkoobchodní
cena	cena	k1gFnSc1	cena
pro	pro	k7c4	pro
registrátory	registrátor	k1gMnPc4	registrátor
je	být	k5eAaImIp3nS	být
145	[number]	k4	145
Kč	Kč	kA	Kč
bez	bez	k7c2	bez
DPH	DPH	kA	DPH
/	/	kIx~	/
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Doménu	doména	k1gFnSc4	doména
lze	lze	k6eAd1	lze
registrovat	registrovat	k5eAaBmF	registrovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
let	léto	k1gNnPc2	léto
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
kdykoliv	kdykoliv	k6eAd1	kdykoliv
prodloužit	prodloužit	k5eAaPmF	prodloužit
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgFnPc1d1	uvedená
podmínky	podmínka	k1gFnPc1	podmínka
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
registrátorů	registrátor	k1gMnPc2	registrátor
mírně	mírně	k6eAd1	mírně
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Platnost	platnost	k1gFnSc1	platnost
domény	doména	k1gFnSc2	doména
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nemůže	moct	k5eNaImIp3nS	moct
překročit	překročit	k5eAaPmF	překročit
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
únoru	únor	k1gInSc3	únor
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
.	.	kIx.	.
<g/>
cz	cz	k?	cz
registrováno	registrovat	k5eAaBmNgNnS	registrovat
přes	přes	k7c4	přes
380	[number]	k4	380
000	[number]	k4	000
domén	doména	k1gFnPc2	doména
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
překonal	překonat	k5eAaPmAgInS	překonat
počet	počet	k1gInSc1	počet
registrovaných	registrovaný	k2eAgFnPc2d1	registrovaná
domén	doména	k1gFnPc2	doména
tři	tři	k4xCgMnPc1	tři
čtvrtě	čtvrt	k1xP	čtvrt
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
překročen	překročit	k5eAaPmNgInS	překročit
rovný	rovný	k2eAgMnSc1d1	rovný
1	[number]	k4	1
milion	milion	k4xCgInSc1	milion
domén	doména	k1gFnPc2	doména
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
přes	přes	k7c4	přes
1	[number]	k4	1
300	[number]	k4	300
000	[number]	k4	000
domén	doména	k1gFnPc2	doména
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
doméně	doména	k1gFnSc6	doména
.	.	kIx.	.
<g/>
cz	cz	k?	cz
zatím	zatím	k6eAd1	zatím
nelze	lze	k6eNd1	lze
používat	používat	k5eAaImF	používat
diakritiku	diakritika	k1gFnSc4	diakritika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Autorizace	autorizace	k1gFnSc1	autorizace
změn	změna	k1gFnPc2	změna
u	u	k7c2	u
domény	doména	k1gFnSc2	doména
==	==	k?	==
</s>
</p>
<p>
<s>
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
<g/>
NIC	nic	k3yNnSc1	nic
nestanovuje	stanovovat	k5eNaImIp3nS	stanovovat
pevné	pevný	k2eAgInPc4d1	pevný
autorizační	autorizační	k2eAgInPc4d1	autorizační
mechanismy	mechanismus	k1gInPc4	mechanismus
pro	pro	k7c4	pro
potvrzování	potvrzování	k1gNnSc4	potvrzování
změn	změna	k1gFnPc2	změna
u	u	k7c2	u
domén	doména	k1gFnPc2	doména
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
registrátor	registrátor	k1gMnSc1	registrátor
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
poskytnout	poskytnout	k5eAaPmF	poskytnout
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
papírový	papírový	k2eAgInSc4d1	papírový
způsob	způsob	k1gInSc4	způsob
(	(	kIx(	(
<g/>
např.	např.	kA	např.
podpis	podpis	k1gInSc1	podpis
písemné	písemný	k2eAgFnSc2d1	písemná
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
ověřený	ověřený	k2eAgInSc1d1	ověřený
notářem	notář	k1gMnSc7	notář
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
elektronický	elektronický	k2eAgInSc1d1	elektronický
způsob	způsob	k1gInSc1	způsob
(	(	kIx(	(
<g/>
potvrzený	potvrzený	k2eAgInSc1d1	potvrzený
heslem	heslo	k1gNnSc7	heslo
<g/>
,	,	kIx,	,
elektronickým	elektronický	k2eAgInSc7d1	elektronický
podpisem	podpis	k1gInSc7	podpis
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
každé	každý	k3xTgFnSc2	každý
změny	změna	k1gFnSc2	změna
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
registrátor	registrátor	k1gMnSc1	registrátor
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
provedl	provést	k5eAaPmAgInS	provést
na	na	k7c6	na
základě	základ	k1gInSc6	základ
žádosti	žádost	k1gFnSc2	žádost
od	od	k7c2	od
oprávněné	oprávněný	k2eAgFnSc2d1	oprávněná
osoby	osoba	k1gFnSc2	osoba
majitele	majitel	k1gMnSc2	majitel
domény	doména	k1gFnSc2	doména
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
volitelná	volitelný	k2eAgFnSc1d1	volitelná
pojistka	pojistka	k1gFnSc1	pojistka
proti	proti	k7c3	proti
neoprávněným	oprávněný	k2eNgFnPc3d1	neoprávněná
či	či	k8xC	či
chybným	chybný	k2eAgFnPc3d1	chybná
změnám	změna	k1gFnPc3	změna
u	u	k7c2	u
domény	doména	k1gFnSc2	doména
slouží	sloužit	k5eAaImIp3nS	sloužit
tzv.	tzv.	kA	tzv.
konfirmace	konfirmace	k1gFnSc2	konfirmace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přímo	přímo	k6eAd1	přímo
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
<g/>
NIC	nic	k3yNnSc1	nic
ještě	ještě	k9	ještě
dodatečně	dodatečně	k6eAd1	dodatečně
vyzve	vyzvat	k5eAaPmIp3nS	vyzvat
majitele	majitel	k1gMnPc4	majitel
domény	doména	k1gFnSc2	doména
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
požadované	požadovaný	k2eAgFnPc4d1	požadovaná
změny	změna	k1gFnPc4	změna
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Akreditovaní	akreditovaný	k2eAgMnPc1d1	akreditovaný
registrátoři	registrátor	k1gMnPc1	registrátor
==	==	k?	==
</s>
</p>
<p>
<s>
Správce	správce	k1gMnSc1	správce
domény	doména	k1gFnSc2	doména
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
<g/>
NIC	nic	k6eAd1	nic
<g/>
)	)	kIx)	)
nenabízí	nabízet	k5eNaImIp3nS	nabízet
registraci	registrace	k1gFnSc4	registrace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
domén	doména	k1gFnPc2	doména
přímo	přímo	k6eAd1	přímo
koncovým	koncový	k2eAgMnPc3d1	koncový
zájemcům	zájemce	k1gMnPc3	zájemce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
domény	doména	k1gFnPc1	doména
registrují	registrovat	k5eAaBmIp3nP	registrovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
akreditovaných	akreditovaný	k2eAgMnPc2d1	akreditovaný
registrátorů	registrátor	k1gMnPc2	registrátor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
komerční	komerční	k2eAgInPc1d1	komerční
subjekty	subjekt	k1gInPc1	subjekt
nabízející	nabízející	k2eAgFnSc2d1	nabízející
služby	služba	k1gFnSc2	služba
registrace	registrace	k1gFnSc2	registrace
a	a	k8xC	a
správy	správa	k1gFnSc2	správa
domén	doména	k1gFnPc2	doména
svým	svůj	k3xOyFgMnPc3	svůj
zákazníkům	zákazník	k1gMnPc3	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
přináší	přinášet	k5eAaImIp3nS	přinášet
konkurenční	konkurenční	k2eAgNnSc4d1	konkurenční
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
variabilitu	variabilita	k1gFnSc4	variabilita
nabízených	nabízený	k2eAgFnPc2d1	nabízená
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
přidaná	přidaný	k2eAgFnSc1d1	přidaná
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
dotování	dotování	k1gNnSc1	dotování
cen	cena	k1gFnPc2	cena
domén	doména	k1gFnPc2	doména
při	při	k7c6	při
využívání	využívání	k1gNnSc6	využívání
jiných	jiný	k2eAgFnPc2d1	jiná
služeb	služba	k1gFnPc2	služba
registrátora	registrátor	k1gMnSc2	registrátor
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Registrátorem	registrátor	k1gMnSc7	registrátor
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
právnická	právnický	k2eAgFnSc1d1	právnická
osoba	osoba	k1gFnSc1	osoba
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
či	či	k8xC	či
akreditovaný	akreditovaný	k2eAgMnSc1d1	akreditovaný
registrátor	registrátor	k1gMnSc1	registrátor
ICANN	ICANN	kA	ICANN
<g/>
.	.	kIx.	.
</s>
<s>
Registrátorství	Registrátorství	k1gNnSc1	Registrátorství
se	se	k3xPyFc4	se
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
některým	některý	k3yIgInPc3	některý
poplatkům	poplatek	k1gInPc3	poplatek
a	a	k8xC	a
nákladům	náklad	k1gInPc3	náklad
na	na	k7c4	na
technický	technický	k2eAgInSc4d1	technický
provoz	provoz	k1gInSc4	provoz
vyplatí	vyplatit	k5eAaPmIp3nS	vyplatit
až	až	k9	až
od	od	k7c2	od
určitého	určitý	k2eAgInSc2d1	určitý
počtu	počet	k1gInSc2	počet
domén	doména	k1gFnPc2	doména
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
registrátorů	registrátor	k1gMnPc2	registrátor
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
není	být	k5eNaImIp3nS	být
(	(	kIx(	(
<g/>
k	k	k7c3	k
červenci	červenec	k1gInSc3	červenec
2009	[number]	k4	2009
jich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
37	[number]	k4	37
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	září	k1gNnSc2	září
2011	[number]	k4	2011
představil	představit	k5eAaPmAgMnS	představit
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
<g/>
NIC	nic	k6eAd1	nic
projekt	projekt	k1gInSc1	projekt
s	s	k7c7	s
názvem	název	k1gInSc7	název
Certifikace	certifikace	k1gFnSc2	certifikace
registrátorů	registrátor	k1gMnPc2	registrátor
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
poskytnout	poskytnout	k5eAaPmF	poskytnout
zájemcům	zájemce	k1gMnPc3	zájemce
o	o	k7c4	o
domény	doména	k1gFnPc4	doména
základní	základní	k2eAgInSc1d1	základní
přehled	přehled	k1gInSc1	přehled
o	o	k7c6	o
úrovni	úroveň	k1gFnSc6	úroveň
služeb	služba	k1gFnPc2	služba
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
registrátorů	registrátor	k1gMnPc2	registrátor
a	a	k8xC	a
definovat	definovat	k5eAaBmF	definovat
ideální	ideální	k2eAgFnPc4d1	ideální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
funkce	funkce	k1gFnPc4	funkce
registračního	registrační	k2eAgInSc2d1	registrační
systému	systém	k1gInSc2	systém
a	a	k8xC	a
souvisejících	související	k2eAgFnPc2d1	související
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
registrátorů	registrátor	k1gMnPc2	registrátor
tak	tak	k9	tak
teď	teď	k6eAd1	teď
mohou	moct	k5eAaImIp3nP	moct
zájemci	zájemce	k1gMnPc1	zájemce
najít	najít	k5eAaPmF	najít
vedle	vedle	k7c2	vedle
návodů	návod	k1gInPc2	návod
jak	jak	k6eAd1	jak
registrovat	registrovat	k5eAaBmF	registrovat
a	a	k8xC	a
symbolů	symbol	k1gInPc2	symbol
mojeID	mojeID	k?	mojeID
<g/>
,	,	kIx,	,
IPv	IPv	k1gFnSc1	IPv
<g/>
6	[number]	k4	6
a	a	k8xC	a
DNSSEC	DNSSEC	kA	DNSSEC
také	také	k9	také
hvězdičky	hvězdička	k1gFnPc1	hvězdička
ukazující	ukazující	k2eAgFnPc1d1	ukazující
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
který	který	k3yQgMnSc1	který
registrátor	registrátor	k1gMnSc1	registrátor
v	v	k7c4	v
certifikaci	certifikace	k1gFnSc4	certifikace
uspěl	uspět	k5eAaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Expirace	Expirace	k1gFnSc2	Expirace
domény	doména	k1gFnSc2	doména
==	==	k?	==
</s>
</p>
<p>
<s>
Jestliže	jestliže	k8xS	jestliže
doména	doména	k1gFnSc1	doména
expiruje	expirovat	k5eAaPmIp3nS	expirovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
není	být	k5eNaImIp3nS	být
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
platnost	platnost	k1gFnSc1	platnost
vyprší	vypršet	k5eAaPmIp3nS	vypršet
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
hned	hned	k6eAd1	hned
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
deaktivována	deaktivován	k2eAgFnSc1d1	deaktivována
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
dalších	další	k2eAgInPc2d1	další
třicet	třicet	k4xCc4	třicet
dní	den	k1gInPc2	den
dále	daleko	k6eAd2	daleko
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
deaktivována	deaktivovat	k5eAaImNgFnS	deaktivovat
(	(	kIx(	(
<g/>
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
z	z	k7c2	z
DNS	DNS	kA	DNS
serverů	server	k1gInPc2	server
domény	doména	k1gFnSc2	doména
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
patnácti	patnáct	k4xCc6	patnáct
dnech	den	k1gInPc6	den
bez	bez	k7c2	bez
prodloužení	prodloužení	k1gNnSc2	prodloužení
je	být	k5eAaImIp3nS	být
definitivně	definitivně	k6eAd1	definitivně
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
majitel	majitel	k1gMnSc1	majitel
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
po	po	k7c6	po
expiraci	expirace	k1gFnSc6	expirace
domény	doména	k1gFnSc2	doména
provést	provést	k5eAaPmF	provést
změnu	změna	k1gFnSc4	změna
registrátora	registrátor	k1gMnSc2	registrátor
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
současně	současně	k6eAd1	současně
provést	provést	k5eAaPmF	provést
i	i	k9	i
její	její	k3xOp3gNnSc4	její
prodloužení	prodloužení	k1gNnSc4	prodloužení
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
změny	změna	k1gFnPc1	změna
u	u	k7c2	u
domény	doména	k1gFnSc2	doména
nejsou	být	k5eNaImIp3nP	být
omezeny	omezit	k5eAaPmNgFnP	omezit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Autoritativní	autoritativní	k2eAgInPc4d1	autoritativní
DNS	DNS	kA	DNS
servery	server	k1gInPc4	server
pro	pro	k7c4	pro
domény	doména	k1gFnPc4	doména
.	.	kIx.	.
<g/>
cz	cz	k?	cz
==	==	k?	==
</s>
</p>
<p>
<s>
a.	a.	k?	a.
<g/>
ns	ns	k?	ns
<g/>
.	.	kIx.	.
<g/>
nic	nic	k6eAd1	nic
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
b.	b.	k?	b.
<g/>
ns	ns	k?	ns
<g/>
.	.	kIx.	.
<g/>
nic	nic	k6eAd1	nic
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
c.	c.	k?	c.
<g/>
ns	ns	k?	ns
<g/>
.	.	kIx.	.
<g/>
nic	nic	k6eAd1	nic
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
d.	d.	k?	d.
<g/>
ns	ns	k?	ns
<g/>
.	.	kIx.	.
<g/>
nic	nic	k6eAd1	nic
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
f.	f.	k?	f.
<g/>
ns	ns	k?	ns
<g/>
.	.	kIx.	.
<g/>
nic	nic	k6eAd1	nic
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Whois	Whois	k1gFnSc1	Whois
informace	informace	k1gFnSc2	informace
IANA	Ianus	k1gMnSc2	Ianus
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
akreditovaných	akreditovaný	k2eAgMnPc2d1	akreditovaný
registrátorů	registrátor	k1gMnPc2	registrátor
</s>
</p>
<p>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
registrace	registrace	k1gFnSc2	registrace
doménových	doménový	k2eAgNnPc2d1	doménové
jmen	jméno	k1gNnPc2	jméno
v	v	k7c6	v
doméně	doména	k1gFnSc6	doména
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
</s>
</p>
