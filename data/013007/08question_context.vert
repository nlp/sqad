<s>
Marianský	Marianský	k2eAgInSc1d1
příkop	příkop	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
češtině	čeština	k1gFnSc6
obvykle	obvykle	k6eAd1
psán	psán	k2eAgInSc1d1
Mariánský	mariánský	k2eAgInSc1d1
příkop	příkop	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
asi	asi	k9
2550	[number]	k4
km	km	kA
dlouhé	dlouhý	k2eAgNnSc1d1
a	a	k8xC
průměrně	průměrně	k6eAd1
69	[number]	k4
km	km	kA
široké	široký	k2eAgNnSc4d1
podmořské	podmořský	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
tektonického	tektonický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
východně	východně	k6eAd1
od	od	k7c2
souostroví	souostroví	k1gNnSc2
Mariany	Mariana	k1gFnSc2
(	(	kIx(
<g/>
Mariánské	mariánský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poblíž	poblíž	k7c2
ostrova	ostrov	k1gInSc2
Guam	Guama	k1gFnPc2
<g/>
.	.	kIx.
</s>