<s>
Jeho	jeho	k3xOp3gFnSc7
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
i	i	k9
vůči	vůči	k7c3
hladině	hladina	k1gFnSc3
moře	moře	k1gNnSc2
nejhlubší	hluboký	k2eAgNnSc4d3
místo	místo	k1gNnSc4
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
<g/>
,	,	kIx,
hloubka	hloubka	k1gFnSc1
podle	podle	k7c2
posledního	poslední	k2eAgNnSc2d1
měření	měření	k1gNnSc2
činí	činit	k5eAaImIp3nS
10	[number]	k4
994	[number]	k4
m	m	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
uváděno	uvádět	k5eAaImNgNnS
10	[number]	k4
911	[number]	k4
m	m	kA
až	až	k9
11	[number]	k4
034	[number]	k4
m	m	kA
<g/>
)	)	kIx)
pod	pod	k7c7
hladinou	hladina	k1gFnSc7
severního	severní	k2eAgInSc2d1
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>