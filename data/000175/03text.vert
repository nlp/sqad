<s>
Hana	Hana	k1gFnSc1	Hana
Holišová	Holišová	k1gFnSc1	Holišová
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
držitelka	držitelka	k1gFnSc1	držitelka
Ceny	cena	k1gFnSc2	cena
Thálie	Thálie	k1gFnSc2	Thálie
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
za	za	k7c4	za
roli	role	k1gFnSc4	role
papežky	papežka	k1gFnSc2	papežka
Jany	Jana	k1gFnSc2	Jana
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Papežka	papežka	k1gFnSc1	papežka
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Evžen	Evžen	k1gMnSc1	Evžen
Holiš	Holiš	k1gMnSc1	Holiš
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
korepetitor	korepetitor	k1gMnSc1	korepetitor
ND	ND	kA	ND
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Horká	Horká	k1gFnSc1	Horká
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
sólistkou	sólistka	k1gFnSc7	sólistka
zpěvohry	zpěvohra	k1gFnSc2	zpěvohra
ND	ND	kA	ND
a	a	k8xC	a
MdB	MdB	k1gFnSc2	MdB
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
polorodou	polorodý	k2eAgFnSc7d1	polorodá
sestrou	sestra	k1gFnSc7	sestra
je	být	k5eAaImIp3nS	být
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Michaela	Michaela	k1gFnSc1	Michaela
Horká	Horká	k1gFnSc1	Horká
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
Střední	střední	k2eAgFnSc4d1	střední
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
činoherní	činoherní	k2eAgNnSc1d1	činoherní
herectví	herectví	k1gNnSc1	herectví
na	na	k7c6	na
JAMU	jam	k1gInSc6	jam
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc4d1	divadelní
kariéru	kariéra	k1gFnSc4	kariéra
začala	začít	k5eAaPmAgFnS	začít
hostováním	hostování	k1gNnSc7	hostování
v	v	k7c6	v
brněnském	brněnský	k2eAgNnSc6d1	brněnské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
inscenacích	inscenace	k1gFnPc6	inscenace
Zpívání	zpívání	k1gNnSc2	zpívání
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
<g/>
,	,	kIx,	,
Kristián	Kristián	k1gMnSc1	Kristián
a	a	k8xC	a
Divotvorný	divotvorný	k2eAgInSc1d1	divotvorný
hrnec	hrnec	k1gInSc1	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
krátké	krátký	k2eAgNnSc4d1	krátké
hostování	hostování	k1gNnSc4	hostování
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Nová	nový	k2eAgFnSc1d1	nová
Scéna	scéna	k1gFnSc1	scéna
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
diváci	divák	k1gMnPc1	divák
mohli	moct	k5eAaImAgMnP	moct
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
roli	role	k1gFnSc6	role
Jeanie	Jeanie	k1gFnSc2	Jeanie
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Hair	Hair	k1gInSc1	Hair
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Hudebním	hudební	k2eAgNnSc6d1	hudební
divadle	divadlo	k1gNnSc6	divadlo
Karlín	Karlín	k1gInSc1	Karlín
nastudovala	nastudovat	k5eAaBmAgFnS	nastudovat
roli	role	k1gFnSc4	role
Aleny	Alena	k1gFnSc2	Alena
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterou	který	k3yQgFnSc7	který
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Thálie	Thálie	k1gFnSc2	Thálie
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Inscenace	inscenace	k1gFnSc1	inscenace
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
stálého	stálý	k2eAgNnSc2d1	stálé
angažmá	angažmá	k1gNnSc2	angažmá
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
množství	množství	k1gNnSc4	množství
rolí	role	k1gFnPc2	role
v	v	k7c6	v
muzikálech	muzikál	k1gInPc6	muzikál
jako	jako	k8xS	jako
např.	např.	kA	např.
Jesus	Jesus	k1gMnSc1	Jesus
Christ	Christ	k1gMnSc1	Christ
Superstar	superstar	k1gFnSc1	superstar
<g/>
,	,	kIx,	,
Čarodějky	čarodějka	k1gFnPc1	čarodějka
z	z	k7c2	z
Eastwicku	Eastwick	k1gInSc2	Eastwick
<g/>
,	,	kIx,	,
Evita	Evit	k1gInSc2	Evit
<g/>
,	,	kIx,	,
Pokrevní	pokrevní	k2eAgMnPc1d1	pokrevní
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
Bídníci	bídník	k1gMnPc1	bídník
<g/>
,	,	kIx,	,
Papežka	papežka	k1gFnSc1	papežka
<g/>
,	,	kIx,	,
Kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
Funny	Funen	k2eAgFnPc1d1	Funen
girl	girl	k1gFnSc1	girl
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
činoherní	činoherní	k2eAgFnSc6d1	činoherní
scéně	scéna	k1gFnSc6	scéna
MdB	MdB	k1gMnPc1	MdB
např.	např.	kA	např.
inscenace	inscenace	k1gFnSc1	inscenace
Equus	Equus	k1gMnSc1	Equus
<g/>
,	,	kIx,	,
Becket	Becket	k1gMnSc1	Becket
aneb	aneb	k?	aneb
čest	čest	k1gFnSc1	čest
Boží	boží	k2eAgFnSc1d1	boží
<g/>
,	,	kIx,	,
Dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
Škola	škola	k1gFnSc1	škola
základ	základ	k1gInSc1	základ
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Podivný	podivný	k2eAgInSc1d1	podivný
případ	případ	k1gInSc1	případ
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Brněnským	brněnský	k2eAgNnSc7d1	brněnské
angažmá	angažmá	k1gNnSc7	angažmá
návštěvníci	návštěvník	k1gMnPc1	návštěvník
pražských	pražský	k2eAgFnPc2d1	Pražská
divadelních	divadelní	k2eAgFnPc2d1	divadelní
scén	scéna	k1gFnPc2	scéna
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
působení	působení	k1gNnSc4	působení
nepřišli	přijít	k5eNaPmAgMnP	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Hostovala	hostovat	k5eAaImAgFnS	hostovat
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Státní	státní	k2eAgFnSc6d1	státní
opeře	opera	k1gFnSc6	opera
v	v	k7c6	v
lyrikálu	lyrikál	k1gInSc6	lyrikál
M.	M.	kA	M.
Horáčka	Horáček	k1gMnSc2	Horáček
a	a	k8xC	a
P.	P.	kA	P.
Hapky	hapek	k1gInPc1	hapek
Kudykam	Kudykam	k1gInSc4	Kudykam
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
ženské	ženský	k2eAgFnSc6d1	ženská
roli	role	k1gFnSc6	role
Martiny	Martina	k1gFnSc2	Martina
a	a	k8xC	a
aktuálně	aktuálně	k6eAd1	aktuálně
hraje	hrát	k5eAaImIp3nS	hrát
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
muzikálu	muzikál	k1gInSc2	muzikál
Mamma	Mamma	k1gFnSc1	Mamma
Mia	Mia	k1gFnSc1	Mia
<g/>
!	!	kIx.	!
</s>
<s>
uváděném	uváděný	k2eAgNnSc6d1	uváděné
v	v	k7c6	v
Kongresovém	kongresový	k2eAgNnSc6d1	Kongresové
centru	centrum	k1gNnSc6	centrum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
širšího	široký	k2eAgNnSc2d2	širší
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
díky	díky	k7c3	díky
roli	role	k1gFnSc3	role
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
sestřičky	sestřička	k1gFnSc2	sestřička
Anny	Anna	k1gFnSc2	Anna
Peškové	Pešková	k1gFnSc2	Pešková
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Velmi	velmi	k6eAd1	velmi
křehké	křehký	k2eAgInPc4d1	křehký
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Zahrála	zahrát	k5eAaPmAgFnS	zahrát
si	se	k3xPyFc3	se
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
českém	český	k2eAgInSc6d1	český
filmu	film	k1gInSc6	film
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Drhy	drh	k1gInPc7	drh
Anglické	anglický	k2eAgFnSc2d1	anglická
jahody	jahoda	k1gFnSc2	jahoda
<g/>
,	,	kIx,	,
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
také	také	k9	také
v	v	k7c6	v
TV	TV	kA	TV
seriálu	seriál	k1gInSc2	seriál
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
s	s	k7c7	s
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Donutilem	Donutil	k1gMnSc7	Donutil
<g/>
,	,	kIx,	,
v	v	k7c6	v
TV	TV	kA	TV
filmu	film	k1gInSc6	film
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Michálka	Michálek	k1gMnSc2	Michálek
-	-	kIx~	-
Milenka	Milenka	k1gFnSc1	Milenka
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
cyklu	cyklus	k1gInSc2	cyklus
Okno	okno	k1gNnSc4	okno
do	do	k7c2	do
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
série	série	k1gFnSc2	série
Soukromých	soukromý	k2eAgFnPc2d1	soukromá
pastí	past	k1gFnPc2	past
(	(	kIx(	(
<g/>
Nezabiješ	zabít	k5eNaPmIp2nS	zabít
<g/>
,	,	kIx,	,
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Jiřího	Jiří	k1gMnSc2	Jiří
Věrčáka	Věrčák	k1gMnSc2	Věrčák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgMnPc1d1	televizní
diváci	divák	k1gMnPc1	divák
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
mohli	moct	k5eAaImAgMnP	moct
vidět	vidět	k5eAaImF	vidět
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Užij	užít	k5eAaPmRp2nS	užít
si	se	k3xPyFc3	se
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Filipa	Filip	k1gMnSc2	Filip
Renče	Renče	k?	Renče
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
TV	TV	kA	TV
Nova	novum	k1gNnSc2	novum
-	-	kIx~	-
Ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
roli	role	k1gFnSc4	role
Veroniky	Veronika	k1gFnSc2	Veronika
Maléřové	Maléřové	k?	Maléřové
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
první	první	k4xOgFnPc4	první
řady	řada	k1gFnPc4	řada
hudebně	hudebně	k6eAd1	hudebně
-	-	kIx~	-
zábavné	zábavný	k2eAgFnSc2d1	zábavná
show	show	k1gFnSc2	show
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
Tvoje	tvůj	k3xOp2gFnSc1	tvůj
tvář	tvář	k1gFnSc1	tvář
má	mít	k5eAaImIp3nS	mít
známý	známý	k2eAgInSc4d1	známý
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deseti	deset	k4xCc6	deset
kolech	kolo	k1gNnPc6	kolo
postupně	postupně	k6eAd1	postupně
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
tyto	tento	k3xDgMnPc4	tento
zpěváky	zpěvák	k1gMnPc4	zpěvák
/	/	kIx~	/
herce	herec	k1gMnSc4	herec
-	-	kIx~	-
Édith	Édith	k1gMnSc1	Édith
Piaf	Piaf	k1gFnSc1	Piaf
<g/>
,	,	kIx,	,
Beyoncé	Beyoncé	k1gNnSc1	Beyoncé
Knowles	Knowlesa	k1gFnPc2	Knowlesa
<g/>
,	,	kIx,	,
Katy	Kata	k1gFnSc2	Kata
Perry	Perra	k1gFnSc2	Perra
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Zagorová	Zagorová	k1gFnSc1	Zagorová
<g/>
,	,	kIx,	,
MC	MC	kA	MC
Hammer	Hammer	k1gInSc1	Hammer
<g/>
,	,	kIx,	,
Shania	Shanium	k1gNnPc1	Shanium
Twain	Twain	k1gMnSc1	Twain
<g/>
,	,	kIx,	,
Aretha	Aretha	k1gMnSc1	Aretha
Franklin	Franklin	k1gInSc1	Franklin
<g/>
,	,	kIx,	,
Catherin	Catherin	k1gInSc1	Catherin
Zeta-Jones	Zeta-Jones	k1gMnSc1	Zeta-Jones
a	a	k8xC	a
John	John	k1gMnSc1	John
Travolta	Travolta	k1gMnSc1	Travolta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
její	její	k3xOp3gFnSc7	její
celkovou	celkový	k2eAgFnSc7d1	celková
vítězkou	vítězka	k1gFnSc7	vítězka
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
získala	získat	k5eAaPmAgFnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
"	"	kIx"	"
<g/>
nejoblíbenější	oblíbený	k2eAgFnSc1d3	nejoblíbenější
herečka	herečka	k1gFnSc1	herečka
<g/>
"	"	kIx"	"
v	v	k7c6	v
divácké	divácký	k2eAgFnSc6d1	divácká
anketě	anketa	k1gFnSc6	anketa
Křídla	křídlo	k1gNnSc2	křídlo
(	(	kIx(	(
<g/>
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
její	její	k3xOp3gInSc4	její
zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
ale	ale	k8xC	ale
považovat	považovat	k5eAaImF	považovat
získání	získání	k1gNnSc4	získání
Ceny	cena	k1gFnSc2	cena
Thálie	Thálie	k1gFnSc2	Thálie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
každoročně	každoročně	k6eAd1	každoročně
uděluje	udělovat	k5eAaImIp3nS	udělovat
Herecké	herecký	k2eAgFnPc4d1	herecká
asociace	asociace	k1gFnPc4	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
za	za	k7c4	za
roli	role	k1gFnSc4	role
Papežky	papežka	k1gFnSc2	papežka
Jany	Jana	k1gFnSc2	Jana
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Papežka	papežka	k1gFnSc1	papežka
<g/>
,	,	kIx,	,
uváděném	uváděný	k2eAgNnSc6d1	uváděné
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
její	její	k3xOp3gNnSc1	její
pěvecké	pěvecký	k2eAgNnSc1d1	pěvecké
účinkování	účinkování	k1gNnSc1	účinkování
s	s	k7c7	s
orchestry	orchestr	k1gInPc7	orchestr
Cool	Coola	k1gFnPc2	Coola
Time	Time	k1gFnSc7	Time
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
Jazz	jazz	k1gInSc1	jazz
side	sid	k1gFnSc2	sid
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
B-side	Bid	k1gInSc5	B-sid
Band	banda	k1gFnPc2	banda
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
Orchestrem	orchestr	k1gInSc7	orchestr
Gustava	Gustav	k1gMnSc2	Gustav
Broma	Brom	k1gMnSc2	Brom
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc4d1	Městské
divadlo	divadlo	k1gNnSc4	divadlo
Brno	Brno	k1gNnSc1	Brno
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Zpívání	zpívání	k1gNnSc6	zpívání
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Kathy	Kath	k1gInPc1	Kath
Seldenová	Seldenová	k1gFnSc1	Seldenová
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Cabaret	Cabaret	k1gInSc1	Cabaret
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Frenchie	Frenchie	k1gFnSc1	Frenchie
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Hair	Hair	k1gInSc1	Hair
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Diane	Dian	k1gInSc5	Dian
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Kdyby	kdyby	k9	kdyby
tisíc	tisíc	k4xCgInPc2	tisíc
klarinetů	klarinet	k1gInPc2	klarinet
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Brigita	Brigita	k1gFnSc1	Brigita
<g/>
,	,	kIx,	,
<g/>
Tereza	Tereza	k1gFnSc1	Tereza
<g/>
,	,	kIx,	,
redaktorka	redaktorka	k1gFnSc1	redaktorka
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Oliver	Olivra	k1gFnPc2	Olivra
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
role	role	k1gFnPc4	role
<g/>
:	:	kIx,	:
Off	Off	k1gFnSc4	Off
-	-	kIx~	-
stage	stagus	k1gMnSc5	stagus
<g/>
,	,	kIx,	,
Charlotte	Charlott	k1gMnSc5	Charlott
Od	od	k7c2	od
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Jesus	Jesus	k1gMnSc1	Jesus
Christ	Christ	k1gMnSc1	Christ
Superstar	superstar	k1gFnSc1	superstar
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Marie	Marie	k1gFnSc1	Marie
Magdalena	Magdalena	k1gFnSc1	Magdalena
<g/>
/	/	kIx~	/
<g/>
Soulgirl	Soulgirl	k1gInSc1	Soulgirl
II	II	kA	II
Od	od	k7c2	od
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Jozef	Jozef	k1gMnSc1	Jozef
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
úžasný	úžasný	k2eAgInSc1d1	úžasný
pestrobarevný	pestrobarevný	k2eAgInSc1d1	pestrobarevný
plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Vypravěčka	vypravěčka	k1gFnSc1	vypravěčka
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Magická	magický	k2eAgFnSc1d1	magická
<g />
.	.	kIx.	.
</s>
<s>
flétna	flétna	k1gFnSc1	flétna
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Papagena	Papagena	k1gFnSc1	Papagena
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Odysseia	Odysseia	k1gFnSc1	Odysseia
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Athéna	Athéna	k1gFnSc1	Athéna
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Markéta	Markéta	k1gFnSc1	Markéta
Lazarová	Lazarová	k1gFnSc1	Lazarová
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Alexandra	Alexandra	k1gFnSc1	Alexandra
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Kozlíkova	kozlíkův	k2eAgFnSc1d1	Kozlíkova
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Červený	červený	k2eAgInSc1d1	červený
a	a	k8xC	a
Černý	černý	k2eAgInSc1d1	černý
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
Od	od	k7c2	od
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Čarodějky	čarodějka	k1gFnPc1	čarodějka
z	z	k7c2	z
Eastwicku	Eastwick	k1gInSc2	Eastwick
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Žena	žena	k1gFnSc1	žena
v	v	k7c6	v
Eastwicku	Eastwicko	k1gNnSc6	Eastwicko
<g/>
,	,	kIx,	,
Sukie	Sukie	k1gFnSc1	Sukie
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Jánošík	Jánošík	k1gMnSc1	Jánošík
aneb	aneb	k?	aneb
Na	na	k7c6	na
skle	sklo	k1gNnSc6	sklo
malované	malovaný	k2eAgInPc1d1	malovaný
<g/>
,	,	kIx,	,
role	role	k1gFnPc1	role
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc1	první
děvče	děvče	k1gNnSc4	děvče
Od	od	k7c2	od
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Peklo	peklo	k1gNnSc1	peklo
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Lucie	Lucie	k1gFnSc1	Lucie
Paulová	Paulová	k1gFnSc1	Paulová
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Evita	Evita	k1gFnSc1	Evita
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
María	Marí	k2eAgFnSc1d1	María
Eva	Eva	k1gFnSc1	Eva
Duarte	Duart	k1gInSc5	Duart
de	de	k?	de
Perón	perón	k1gInSc1	perón
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Konstance	Konstance	k1gFnSc1	Konstance
Weber	weber	k1gInSc1	weber
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Bídníci	bídník	k1gMnPc1	bídník
<g/>
,	,	kIx,	,
role	role	k1gFnPc1	role
<g/>
:	:	kIx,	:
Eponine	Eponin	k1gInSc5	Eponin
Od	od	k7c2	od
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Probuzení	probuzení	k1gNnSc1	probuzení
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Elsa	Elsa	k1gFnSc1	Elsa
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Nahá	nahý	k2eAgFnSc1d1	nahá
múza	múza	k1gFnSc1	múza
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
-	-	kIx~	-
dívka	dívka	k1gFnSc1	dívka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Kristýna	Kristýna	k1gFnSc1	Kristýna
Od	od	k7c2	od
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Matylda	Matylda	k1gFnSc1	Matylda
Od	od	k7c2	od
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Liz	liz	k1gInSc1	liz
<g/>
,	,	kIx,	,
vězeňkyně	vězeňkyně	k1gFnSc1	vězeňkyně
<g/>
,	,	kIx,	,
Sprostá	sprostá	k1gFnSc1	sprostá
Kitty	Kitta	k1gFnSc2	Kitta
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Kvítek	kvítek	k1gInSc1	kvítek
z	z	k7c2	z
horrroru	horrror	k1gInSc2	horrror
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Crystal	Crystal	k1gFnSc1	Crystal
<g />
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Jekyll	Jekyll	k1gMnSc1	Jekyll
a	a	k8xC	a
Hyde	Hyde	k1gFnSc1	Hyde
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Emma	Emma	k1gFnSc1	Emma
Carewová	Carewová	k1gFnSc1	Carewová
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Papežka	papežka	k1gFnSc1	papežka
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Jana	Jana	k1gFnSc1	Jana
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Pokrevní	pokrevní	k2eAgMnPc1d1	pokrevní
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
role	role	k1gFnPc1	role
<g/>
:	:	kIx,	:
paní	paní	k1gFnSc1	paní
Johnstonová	Johnstonový	k2eAgFnSc1d1	Johnstonová
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Funny	Funna	k1gFnSc2	Funna
Girl	girl	k1gFnSc2	girl
<g/>
,	,	kIx,	,
role	role	k1gFnPc1	role
<g/>
:	:	kIx,	:
Fanny	Fanna	k1gFnPc1	Fanna
Briceová	Briceový	k2eAgFnSc1d1	Briceový
Od	od	k7c2	od
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Donaha	donaha	k6eAd1	donaha
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Georgie	Georgie	k1gFnSc1	Georgie
Bukatinská	Bukatinský	k2eAgFnSc1d1	Bukatinský
Od	od	k7c2	od
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Očistec	očistec	k1gInSc1	očistec
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
,	,	kIx,	,
fanynka	fanynka	k1gFnSc1	fanynka
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Grácie	Grácie	k1gFnSc1	Grácie
<g/>
,	,	kIx,	,
řádová	řádový	k2eAgFnSc1d1	řádová
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
šachová	šachový	k2eAgFnSc1d1	šachová
figurka	figurka	k1gFnSc1	figurka
Od	od	k7c2	od
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Cats	Cats	k1gInSc1	Cats
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Grizabella	Grizabella	k1gFnSc1	Grizabella
Od	od	k7c2	od
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Flashdance	Flashdance	k1gFnPc1	Flashdance
<g/>
,	,	kIx,	,
role	role	k1gFnPc1	role
<g/>
:	:	kIx,	:
Kiki	Kik	k1gFnPc1	Kik
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnPc1	tanečnice
Od	od	k7c2	od
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Sliby	slib	k1gInPc1	slib
chyby	chyba	k1gFnSc2	chyba
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Marge	Marge	k1gFnSc1	Marge
MacDougallová	MacDougallový	k2eAgFnSc1d1	MacDougallový
Od	od	k7c2	od
2014	[number]	k4	2014
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Eliška	Eliška	k1gFnSc1	Eliška
Od	od	k7c2	od
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Johnny	Johnen	k2eAgInPc1d1	Johnen
Blue	Blu	k1gInPc1	Blu
<g/>
,	,	kIx,	,
role	role	k1gFnPc1	role
<g/>
:	:	kIx,	:
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
Johnnyho	Johnny	k1gMnSc2	Johnny
láska	láska	k1gFnSc1	láska
Od	od	k7c2	od
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Ostrov	ostrov	k1gInSc1	ostrov
pokladů	poklad	k1gInPc2	poklad
<g/>
,	,	kIx,	,
role	role	k1gFnSc2	role
<g/>
:	:	kIx,	:
Fanny	Fanna	k1gMnSc2	Fanna
Osbourneová	Osbourneová	k1gFnSc1	Osbourneová
<g/>
,	,	kIx,	,
Paní	paní	k1gFnSc1	paní
Hawkinsová	Hawkinsový	k2eAgFnSc1d1	Hawkinsový
Národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
Brno	Brno	k1gNnSc1	Brno
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Zpívání	zpívání	k1gNnPc4	zpívání
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Kathy	Kath	k1gInPc1	Kath
Seldenová	Seldenová	k1gFnSc1	Seldenová
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Kristián	Kristián	k1gMnSc1	Kristián
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
vypravěčka	vypravěčka	k1gFnSc1	vypravěčka
příběhu	příběh	k1gInSc2	příběh
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Divotvorný	divotvorný	k2eAgInSc1d1	divotvorný
hrnec	hrnec	k1gInSc1	hrnec
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Káča	Káča	k1gFnSc1	Káča
Hudební	hudební	k2eAgFnSc1d1	hudební
divadlo	divadlo	k1gNnSc4	divadlo
Karlín	Karlín	k1gInSc1	Karlín
Od	od	k7c2	od
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Alena	Alena	k1gFnSc1	Alena
Divadlo	divadlo	k1gNnSc1	divadlo
pod	pod	k7c7	pod
Palmovkou	Palmovka	k1gFnSc7	Palmovka
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Sliby	slib	k1gInPc1	slib
chyby	chyba	k1gFnSc2	chyba
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Fran	Fran	k1gInSc1	Fran
Kubeliková	Kubelikový	k2eAgFnSc1d1	Kubeliková
Státní	státní	k2eAgFnSc1d1	státní
opera	opera	k1gFnSc1	opera
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Kudykam	Kudykam	k1gInSc1	Kudykam
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Martina	Martina	k1gFnSc1	Martina
Kongresové	kongresový	k2eAgNnSc1d1	Kongresové
centrum	centrum	k1gNnSc4	centrum
Praha	Praha	k1gFnSc1	Praha
Od	od	k7c2	od
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Mamma	Mamma	k1gFnSc1	Mamma
Mia	Mia	k1gFnSc1	Mia
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Sophie	Sophie	k1gFnSc1	Sophie
Od	od	k7c2	od
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Ať	ať	k8xS	ať
žijí	žít	k5eAaImIp3nP	žít
duchové	duch	k1gMnPc1	duch
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Leona	Leona	k1gFnSc1	Leona
(	(	kIx(	(
<g/>
Leontýnka	Leontýnka	k1gFnSc1	Leontýnka
z	z	k7c2	z
Brtníku	brtník	k1gMnSc3	brtník
<g/>
)	)	kIx)	)
Státní	státní	k2eAgInSc1d1	státní
hrad	hrad	k1gInSc1	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
Od	od	k7c2	od
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Eliška	Eliška	k1gFnSc1	Eliška
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc4	divadlo
Brno	Brno	k1gNnSc1	Brno
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Equus	Equus	k1gInSc1	Equus
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Jill	Jill	k1gInSc1	Jill
Masonová	Masonová	k1gFnSc1	Masonová
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Večer	večer	k6eAd1	večer
tříkrálový	tříkrálový	k2eAgInSc1d1	tříkrálový
aneb	aneb	k?	aneb
Cokoli	cokoli	k3yInSc4	cokoli
chcete	chtít	k5eAaImIp2nP	chtít
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Viola	Viola	k1gFnSc1	Viola
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Becket	Becket	k1gInSc1	Becket
aneb	aneb	k?	aneb
Čest	čest	k1gFnSc1	čest
Boží	boží	k2eAgFnSc1d1	boží
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Gwendolina	Gwendolina	k1gFnSc1	Gwendolina
Od	od	k7c2	od
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Judy	judo	k1gNnPc7	judo
Od	od	k7c2	od
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Škola	škola	k1gFnSc1	škola
základ	základ	k1gInSc1	základ
<g />
.	.	kIx.	.
</s>
<s>
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Anda	Anda	k1gFnSc1	Anda
Pařízková	Pařízková	k1gFnSc1	Pařízková
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Tři	tři	k4xCgMnPc1	tři
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Lucinda	Lucinda	k1gFnSc1	Lucinda
Od	od	k7c2	od
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Podivný	podivný	k2eAgInSc1d1	podivný
případ	případ	k1gInSc1	případ
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Siobhan	Siobhan	k1gInSc1	Siobhan
Od	od	k7c2	od
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Vrabčák	vrabčák	k1gMnSc1	vrabčák
a	a	k8xC	a
anděl	anděl	k1gMnSc1	anděl
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Edith	Edith	k1gInSc1	Edith
Piaf	Piaf	k1gFnSc2	Piaf
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Brak	brak	k1gInSc1	brak
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Rodinná	rodinný	k2eAgNnPc4d1	rodinné
pouta	pouto	k1gNnPc4	pouto
/	/	kIx~	/
Velmi	velmi	k6eAd1	velmi
křehé	křehý	k2eAgInPc1d1	křehý
vztahy	vztah	k1gInPc1	vztah
-	-	kIx~	-
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Anna	Anna	k1gFnSc1	Anna
Pešková	Pešková	k1gFnSc1	Pešková
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Anglické	anglický	k2eAgFnPc1d1	anglická
jahody	jahoda	k1gFnPc1	jahoda
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
3	[number]	k4	3
plus	plus	k1gInSc1	plus
1	[number]	k4	1
s	s	k7c7	s
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Donutilem	Donutil	k1gInSc7	Donutil
-	-	kIx~	-
povídkový	povídkový	k2eAgMnSc1d1	povídkový
TV	TV	kA	TV
pořad	pořad	k1gInSc4	pořad
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Okno	okno	k1gNnSc1	okno
do	do	k7c2	do
hřbitova	hřbitov	k1gInSc2	hřbitov
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Vetřelci	vetřelec	k1gMnPc1	vetřelec
a	a	k8xC	a
lovci	lovec	k1gMnPc1	lovec
<g/>
:	:	kIx,	:
Užij	užít	k5eAaPmRp2nS	užít
si	se	k3xPyFc3	se
se	s	k7c7	s
psem	pes	k1gMnSc7	pes
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Nickyho	Nicky	k1gMnSc2	Nicky
rodina	rodina	k1gFnSc1	rodina
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Ulice	ulice	k1gFnSc1	ulice
-	-	kIx~	-
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
Veronika	Veronika	k1gFnSc1	Veronika
Maléřová	Maléřová	k1gFnSc1	Maléřová
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Policie	policie	k1gFnSc1	policie
Modrava	Modrava	k1gFnSc1	Modrava
-	-	kIx~	-
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
chatařka	chatařka	k1gFnSc1	chatařka
Olinka	Olinka	k1gFnSc1	Olinka
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Labyrint	labyrint	k1gInSc1	labyrint
-	-	kIx~	-
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
doktorka	doktorka	k1gFnSc1	doktorka
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Tvoje	tvůj	k3xOp2gFnSc1	tvůj
tvář	tvář	k1gFnSc1	tvář
má	mít	k5eAaImIp3nS	mít
známý	známý	k2eAgInSc4d1	známý
hlas	hlas	k1gInSc4	hlas
-	-	kIx~	-
pěvecká	pěvecký	k2eAgFnSc1d1	pěvecká
TV	TV	kA	TV
show	show	k1gFnSc1	show
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Četníci	četník	k1gMnPc1	četník
z	z	k7c2	z
Luhačovic	Luhačovice	k1gFnPc2	Luhačovice
-	-	kIx~	-
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
role	role	k1gFnSc1	role
<g/>
:	:	kIx,	:
společnice	společnice	k1gFnSc1	společnice
Katka	Katka	k1gFnSc1	Katka
</s>
