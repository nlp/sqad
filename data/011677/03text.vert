<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Országh	Országh	k1gMnSc1	Országh
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1977	[number]	k4	1977
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slovenský	slovenský	k2eAgMnSc1d1	slovenský
hokejový	hokejový	k2eAgMnSc1d1	hokejový
útočník	útočník	k1gMnSc1	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
trenér	trenér	k1gMnSc1	trenér
HC	HC	kA	HC
Slovan	Slovan	k1gMnSc1	Slovan
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
profesionálním	profesionální	k2eAgInSc7d1	profesionální
hokejem	hokej	k1gInSc7	hokej
začínal	začínat	k5eAaImAgInS	začínat
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
debutoval	debutovat	k5eAaBmAgMnS	debutovat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
slovenské	slovenský	k2eAgFnSc6d1	slovenská
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
lize	liga	k1gFnSc6	liga
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
95	[number]	k4	95
kde	kde	k6eAd1	kde
odehrál	odehrát	k5eAaPmAgInS	odehrát
38	[number]	k4	38
zápasů	zápas	k1gInPc2	zápas
ve	v	k7c6	v
kterých	který	k3yIgInPc2	který
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
30	[number]	k4	30
bodů	bod	k1gInPc2	bod
pomohl	pomoct	k5eAaPmAgInS	pomoct
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
do	do	k7c2	do
slovenské	slovenský	k2eAgFnSc2d1	slovenská
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ligy	liga	k1gFnSc2	liga
kde	kde	k6eAd1	kde
také	také	k9	také
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
draftován	draftovat	k5eAaImNgInS	draftovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
v	v	k7c4	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
106	[number]	k4	106
<g/>
.	.	kIx.	.
týmem	tým	k1gInSc7	tým
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Islanders	Islanders	k1gInSc1	Islanders
<g/>
.	.	kIx.	.
</s>
<s>
Sezóny	sezóna	k1gFnPc1	sezóna
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
a	a	k8xC	a
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
98	[number]	k4	98
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Utah	Utah	k1gInSc1	Utah
Grizzlies	Grizzlies	k1gInSc1	Grizzlies
v	v	k7c6	v
IHL	IHL	kA	IHL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
týmu	tým	k1gInSc6	tým
odehrál	odehrát	k5eAaPmAgMnS	odehrát
130	[number]	k4	130
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc2	který
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
50	[number]	k4	50
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
98	[number]	k4	98
debutoval	debutovat	k5eAaBmAgInS	debutovat
v	v	k7c6	v
NHL	NHL	kA	NHL
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Islanders	Islanders	k1gInSc1	Islanders
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
Islanders	Islandersa	k1gFnPc2	Islandersa
odehrál	odehrát	k5eAaPmAgMnS	odehrát
tři	tři	k4xCgFnPc4	tři
sezóny	sezóna	k1gFnPc4	sezóna
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
ve	v	k7c6	v
kterých	který	k3yRgInPc2	který
odehrál	odehrát	k5eAaPmAgMnS	odehrát
34	[number]	k4	34
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
Islanders	Islandersa	k1gFnPc2	Islandersa
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Lowell	Lowell	k1gMnSc1	Lowell
Lock	Lock	k1gMnSc1	Lock
Monsters	Monsters	k1gInSc4	Monsters
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
odehrál	odehrát	k5eAaPmAgMnS	odehrát
tři	tři	k4xCgFnPc4	tři
sezóny	sezóna	k1gFnPc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
volným	volný	k2eAgMnSc7d1	volný
hráčem	hráč	k1gMnSc7	hráč
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Djurgå	Djurgå	k1gMnSc2	Djurgå
IF	IF	kA	IF
Hockey	Hockea	k1gMnSc2	Hockea
který	který	k3yRgInSc4	který
hrál	hrát	k5eAaImAgInS	hrát
Elitserien	Elitserien	k1gInSc1	Elitserien
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
pomohl	pomoct	k5eAaPmAgMnS	pomoct
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
švédské	švédský	k2eAgFnSc2d1	švédská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Nashville	Nashville	k1gInSc1	Nashville
Predators	Predatorsa	k1gFnPc2	Predatorsa
<g/>
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2002	[number]	k4	2002
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Nashville	Nashville	k1gNnSc2	Nashville
Predators	Predatorsa	k1gFnPc2	Predatorsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nashville	Nashvill	k1gInSc6	Nashvill
odehrál	odehrát	k5eAaPmAgMnS	odehrát
tři	tři	k4xCgFnPc4	tři
sezóny	sezóna	k1gFnPc4	sezóna
ve	v	k7c6	v
kterých	který	k3yIgInPc2	který
odehrál	odehrát	k5eAaPmAgMnS	odehrát
239	[number]	k4	239
zápasů	zápas	k1gInPc2	zápas
ve	v	k7c6	v
kterých	který	k3yRgInPc2	který
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
105	[number]	k4	105
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
Slovákem	Slovák	k1gMnSc7	Slovák
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Predators	Predators	k1gInSc1	Predators
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
výluky	výluka	k1gFnSc2	výluka
v	v	k7c6	v
NHL	NHL	kA	NHL
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Slovenska	Slovensko	k1gNnSc2	Slovensko
nejprv	nejprv	k6eAd1	nejprv
do	do	k7c2	do
mateřského	mateřský	k2eAgInSc2d1	mateřský
týmu	tým	k1gInSc2	tým
HC	HC	kA	HC
05	[number]	k4	05
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
dostal	dostat	k5eAaPmAgInS	dostat
nabídku	nabídka	k1gFnSc4	nabídka
od	od	k7c2	od
týmu	tým	k1gInSc2	tým
HKm	HKm	k1gMnSc2	HKm
Zvolen	Zvolen	k1gInSc1	Zvolen
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
postoupil	postoupit	k5eAaPmAgMnS	postoupit
do	do	k7c2	do
play-off	playff	k1gMnSc1	play-off
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
kde	kde	k6eAd1	kde
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
týmu	tým	k1gInSc2	tým
HC	HC	kA	HC
Slovan	Slovan	k1gInSc1	Slovan
Bratislava	Bratislava	k1gFnSc1	Bratislava
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
dostával	dostávat	k5eAaImAgMnS	dostávat
nabídky	nabídka	k1gFnPc4	nabídka
od	od	k7c2	od
různých	různý	k2eAgInPc2d1	různý
klubů	klub	k1gInPc2	klub
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
týmu	tým	k1gInSc2	tým
Luleå	Luleå	k1gFnSc2	Luleå
HF	HF	kA	HF
kde	kde	k6eAd1	kde
hrával	hrávat	k5eAaImAgMnS	hrávat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Phoenix	Phoenix	k1gInSc1	Phoenix
Coyotes	Coyotesa	k1gFnPc2	Coyotesa
ale	ale	k8xC	ale
smlouvu	smlouva	k1gFnSc4	smlouva
kterou	který	k3yRgFnSc4	který
podepsal	podepsat	k5eAaPmAgMnS	podepsat
byla	být	k5eAaImAgNnP	být
neplatná	platný	k2eNgFnSc1d1	neplatná
protože	protože	k8xS	protože
týmy	tým	k1gInPc1	tým
s	s	k7c7	s
NHL	NHL	kA	NHL
si	se	k3xPyFc3	se
vybírat	vybírat	k5eAaImF	vybírat
hráče	hráč	k1gMnSc4	hráč
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
novém	nový	k2eAgInSc6d1	nový
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vrátit	vrátit	k5eAaPmF	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
St.	st.	kA	st.
Louis	louis	k1gInSc1	louis
Blues	blues	k1gNnSc1	blues
z	z	k7c2	z
waiver	waivra	k1gFnPc2	waivra
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
St.	st.	kA	st.
Louis	louis	k1gInSc4	louis
Blues	blues	k1gNnSc2	blues
dohrál	dohrát	k5eAaPmAgMnS	dohrát
sezónu	sezóna	k1gFnSc4	sezóna
z	z	k7c2	z
16	[number]	k4	16
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
vynechal	vynechat	k5eAaPmAgMnS	vynechat
sezónu	sezóna	k1gFnSc4	sezóna
kvůli	kvůli	k7c3	kvůli
operaci	operace	k1gFnSc3	operace
kolene	kolen	k1gInSc5	kolen
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
20	[number]	k4	20
měsících	měsíc	k1gInPc6	měsíc
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
led	led	k1gInSc4	led
do	do	k7c2	do
druhé	druhý	k4xOgFnSc6	druhý
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odehrál	odehrát	k5eAaPmAgInS	odehrát
jeden	jeden	k4xCgInSc1	jeden
zápas	zápas	k1gInSc1	zápas
za	za	k7c4	za
mateřský	mateřský	k2eAgInSc4d1	mateřský
klub	klub	k1gInSc4	klub
HC	HC	kA	HC
05	[number]	k4	05
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
proti	proti	k7c3	proti
týmu	tým	k1gInSc3	tým
MHK	MHK	kA	MHK
Dolný	Dolný	k1gMnSc1	Dolný
Kubín	Kubín	k1gMnSc1	Kubín
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
odehrál	odehrát	k5eAaPmAgMnS	odehrát
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
připsal	připsat	k5eAaPmAgMnS	připsat
si	se	k3xPyFc3	se
jednu	jeden	k4xCgFnSc4	jeden
asistenci	asistence	k1gFnSc4	asistence
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
zranění	zranění	k1gNnPc4	zranění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
sezónu	sezóna	k1gFnSc4	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
lize	liga	k1gFnSc6	liga
odehrál	odehrát	k5eAaPmAgInS	odehrát
14	[number]	k4	14
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
oznámil	oznámit	k5eAaPmAgMnS	oznámit
definitivní	definitivní	k2eAgInSc4d1	definitivní
konec	konec	k1gInSc4	konec
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Trenérská	trenérský	k2eAgFnSc1d1	trenérská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zůstat	zůstat	k5eAaPmF	zůstat
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
založit	založit	k5eAaPmF	založit
hráčskou	hráčský	k2eAgFnSc4d1	hráčská
asociaci	asociace	k1gFnSc4	asociace
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
asistentem	asistent	k1gMnSc7	asistent
trenéra	trenér	k1gMnSc2	trenér
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
HKm	HKm	k1gFnSc1	HKm
Zvolen	Zvolen	k1gInSc1	Zvolen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
HKm	HKm	k1gFnSc1	HKm
Zvolen	Zvolen	k1gInSc1	Zvolen
-	-	kIx~	-
Asistent	asistent	k1gMnSc1	asistent
trenéra	trenér	k1gMnSc2	trenér
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
MS	MS	kA	MS
2005	[number]	k4	2005
proti	proti	k7c3	proti
Švýcarsku	Švýcarsko	k1gNnSc3	Švýcarsko
si	se	k3xPyFc3	se
poranil	poranit	k5eAaPmAgMnS	poranit
v	v	k7c6	v
levém	levý	k2eAgNnSc6d1	levé
kolenu	koleno	k1gNnSc6	koleno
chrupavku	chrupavka	k1gFnSc4	chrupavka
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
musel	muset	k5eAaImAgMnS	muset
později	pozdě	k6eAd2	pozdě
ukončit	ukončit	k5eAaPmF	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
MS	MS	kA	MS
2005	[number]	k4	2005
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Marcel	Marcel	k1gMnSc1	Marcel
Hossa	Hoss	k1gMnSc2	Hoss
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
zápas	zápas	k1gInSc1	zápas
v	v	k7c6	v
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
proti	proti	k7c3	proti
týmu	tým	k1gInSc3	tým
Calgary	Calgary	k1gNnSc2	Calgary
Flames	Flamesa	k1gFnPc2	Flamesa
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
odehrál	odehrát	k5eAaPmAgMnS	odehrát
16	[number]	k4	16
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnPc1	ocenění
a	a	k8xC	a
úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
SEL	sít	k5eAaImAgMnS	sít
-	-	kIx~	-
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
střelec	střelec	k1gMnSc1	střelec
v	v	k7c6	v
přesilových	přesilový	k2eAgFnPc6d1	přesilová
hrách	hra	k1gFnPc6	hra
</s>
</p>
<p>
<s>
==	==	k?	==
Prvenství	prvenství	k1gNnSc1	prvenství
==	==	k?	==
</s>
</p>
<p>
<s>
Debut	debut	k1gInSc1	debut
v	v	k7c6	v
NHL	NHL	kA	NHL
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
Buffalo	Buffalo	k1gFnSc1	Buffalo
Sabres	Sabres	k1gInSc1	Sabres
proti	proti	k7c3	proti
New	New	k1gFnSc3	New
York	York	k1gInSc1	York
Islanders	Islanders	k1gInSc4	Islanders
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
v	v	k7c6	v
NHL	NHL	kA	NHL
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
Devils	Devilsa	k1gFnPc2	Devilsa
proti	proti	k7c3	proti
New	New	k1gMnPc3	New
York	York	k1gInSc4	York
Islanders	Islandersa	k1gFnPc2	Islandersa
brankáři	brankář	k1gMnSc3	brankář
Martin	Martin	k1gMnSc1	Martin
Brodeur	Brodeur	k1gMnSc1	Brodeur
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
asistence	asistence	k1gFnSc1	asistence
v	v	k7c6	v
NHL	NHL	kA	NHL
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
Carolina	Carolina	k1gFnSc1	Carolina
Hurricanes	Hurricanes	k1gInSc1	Hurricanes
proti	proti	k7c3	proti
New	New	k1gFnSc3	New
York	York	k1gInSc1	York
Islanders	Islanders	k1gInSc4	Islanders
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
hattrick	hattrick	k1gInSc4	hattrick
v	v	k7c6	v
NHL	NHL	kA	NHL
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
Nashville	Nashville	k1gNnSc1	Nashville
Predators	Predatorsa	k1gFnPc2	Predatorsa
proti	proti	k7c3	proti
Buffalo	Buffalo	k1gFnPc3	Buffalo
Sabres	Sabres	k1gInSc4	Sabres
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Klubová	klubový	k2eAgFnSc1d1	klubová
statistika	statistika	k1gFnSc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnPc1d1	reprezentační
statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Országh	Országh	k1gMnSc1	Országh
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Hockeydb	Hockeydb	k1gInSc4	Hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Országh	Országh	k1gMnSc1	Országh
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Eliteprospects	Eliteprospects	k1gInSc4	Eliteprospects
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Országh	Országh	k1gMnSc1	Országh
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Országh	Országh	k1gMnSc1	Országh
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Legendsofhockey	Legendsofhockey	k1gInPc4	Legendsofhockey
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Országh	Országh	k1gMnSc1	Országh
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
