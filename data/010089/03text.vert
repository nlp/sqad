<p>
<s>
Platon	Platon	k1gMnSc1	Platon
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Lebeděv	Lebeděv	k1gMnSc1	Lebeděv
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
П	П	k?	П
<g/>
́	́	k?	́
<g/>
н	н	k?	н
Л	Л	k?	Л
<g/>
́	́	k?	́
<g/>
д	д	k?	д
Л	Л	k?	Л
<g/>
́	́	k?	́
<g/>
б	б	k?	б
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
výkonný	výkonný	k2eAgMnSc1d1	výkonný
ředitel	ředitel	k1gMnSc1	ředitel
skupiny	skupina	k1gFnSc2	skupina
Menatep	Menatep	k1gMnSc1	Menatep
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
blízký	blízký	k2eAgMnSc1d1	blízký
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Michaila	Michail	k1gMnSc2	Michail
Chodorkovského	Chodorkovský	k2eAgMnSc2d1	Chodorkovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
vězněn	věznit	k5eAaImNgMnS	věznit
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
jej	on	k3xPp3gInSc4	on
organizace	organizace	k1gFnSc2	organizace
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc4	International
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Chodorkovského	Chodorkovský	k2eAgInSc2d1	Chodorkovský
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
vězně	vězeň	k1gMnPc4	vězeň
svědomí	svědomí	k1gNnSc2	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
Propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
byl	být	k5eAaImAgMnS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Lebeděv	Lebedět	k5eAaPmDgInS	Lebedět
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
krácení	krácení	k1gNnSc4	krácení
daní	daň	k1gFnPc2	daň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
devíti	devět	k4xCc3	devět
letům	léto	k1gNnPc3	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
údajné	údajný	k2eAgFnSc2d1	údajná
krádeže	krádež	k1gFnSc2	krádež
218	[number]	k4	218
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
praní	praní	k1gNnSc2	praní
špinavých	špinavý	k2eAgInPc2d1	špinavý
peněz	peníze	k1gInPc2	peníze
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
446	[number]	k4	446
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
skutky	skutek	k1gInPc4	skutek
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Chodorkovským	Chodorkovský	k2eAgInSc7d1	Chodorkovský
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
celkově	celkově	k6eAd1	celkově
na	na	k7c4	na
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
jim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc4	trest
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
snížen	snížit	k5eAaPmNgInS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
však	však	k9	však
Lebeděv	Lebeděv	k1gFnSc4	Lebeděv
popírá	popírat	k5eAaImIp3nS	popírat
a	a	k8xC	a
trest	trest	k1gInSc1	trest
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
politickou	politický	k2eAgFnSc4d1	politická
pomstu	pomsta	k1gFnSc4	pomsta
Kremlu	Kreml	k1gInSc2	Kreml
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
oběma	dva	k4xCgMnPc7	dva
mužům	muž	k1gMnPc3	muž
13	[number]	k4	13
<g/>
letý	letý	k2eAgInSc4d1	letý
trest	trest	k1gInSc4	trest
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
snížen	snížen	k2eAgInSc1d1	snížen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úpravách	úprava	k1gFnPc6	úprava
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
totiž	totiž	k9	totiž
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
byly	být	k5eAaImAgFnP	být
sníženy	snížen	k2eAgFnPc1d1	snížena
horní	horní	k2eAgFnPc1d1	horní
sazby	sazba	k1gFnPc1	sazba
za	za	k7c4	za
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgInPc4	jenž
byli	být	k5eAaImAgMnP	být
podnikatelé	podnikatel	k1gMnPc1	podnikatel
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Lebeděv	Lebedět	k5eAaPmDgInS	Lebedět
se	se	k3xPyFc4	se
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Chodorkovskij	Chodorkovskít	k5eAaPmRp2nS	Chodorkovskít
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
dostal	dostat	k5eAaPmAgMnS	dostat
milost	milost	k1gFnSc4	milost
od	od	k7c2	od
ruského	ruský	k2eAgMnSc2d1	ruský
prezidenta	prezident	k1gMnSc2	prezident
Vladimira	Vladimir	k1gMnSc2	Vladimir
Putina	putin	k2eAgMnSc2d1	putin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
