<p>
<s>
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
ve	v	k7c6	v
skocích	skok	k1gInPc6	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
závodů	závod	k1gInPc2	závod
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
úrovně	úroveň	k1gFnSc2	úroveň
ve	v	k7c6	v
skocích	skok	k1gInPc6	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
Kuusamu	Kuusam	k1gInSc6	Kuusam
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgInSc7d1	poslední
závodem	závod	k1gInSc7	závod
byl	být	k5eAaImAgInS	být
závod	závod	k1gInSc4	závod
ve	v	k7c6	v
slovinské	slovinský	k2eAgFnSc6d1	slovinská
Planici	Planice	k1gFnSc6	Planice
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
poháru	pohár	k1gInSc2	pohár
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Rakušan	Rakušan	k1gMnSc1	Rakušan
Thomas	Thomas	k1gMnSc1	Thomas
Morgenstern	Morgenstern	k1gMnSc1	Morgenstern
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Individuální	individuální	k2eAgInPc1d1	individuální
závody	závod	k1gInPc1	závod
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Závody	závod	k1gInPc1	závod
družstev	družstvo	k1gNnPc2	družstvo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgNnSc1d1	Konečné
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Celkově	celkově	k6eAd1	celkově
===	===	k?	===
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
pořadí	pořadí	k1gNnSc1	pořadí
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
FIS	fis	k1gNnSc2	fis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lety	let	k1gInPc7	let
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
===	===	k?	===
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
pořadí	pořadí	k1gNnSc1	pořadí
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
FIS	fis	k1gNnSc2	fis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pohár	pohár	k1gInSc4	pohár
národů	národ	k1gInPc2	národ
===	===	k?	===
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
pořadí	pořadí	k1gNnSc1	pořadí
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
FIS	fis	k1gNnSc2	fis
<g/>
.	.	kIx.	.
</s>
</p>
