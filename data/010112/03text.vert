<p>
<s>
Heptoda	Heptoda	k1gFnSc1	Heptoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
zapojeních	zapojení	k1gNnPc6	zapojení
též	též	k6eAd1	též
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
pentagrid	pentagrid	k1gInSc4	pentagrid
je	být	k5eAaImIp3nS	být
elektronka	elektronka	k1gFnSc1	elektronka
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
elektrodami	elektroda	k1gFnPc7	elektroda
–	–	k?	–
katodou	katoda	k1gFnSc7	katoda
<g/>
,	,	kIx,	,
anodou	anoda	k1gFnSc7	anoda
a	a	k8xC	a
5	[number]	k4	5
mřížkami	mřížka	k1gFnPc7	mřížka
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
obvykle	obvykle	k6eAd1	obvykle
ke	k	k7c3	k
směšování	směšování	k1gNnSc3	směšování
signálu	signál	k1gInSc2	signál
v	v	k7c6	v
heterodynech	heterodyn	k1gInPc6	heterodyn
a	a	k8xC	a
superheterodynech	superheterodyn	k1gInPc6	superheterodyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
mřížek	mřížka	k1gFnPc2	mřížka
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
řídící	řídící	k2eAgMnPc4d1	řídící
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc4d1	ostatní
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
stínění	stínění	k1gNnSc3	stínění
či	či	k8xC	či
brzdění	brzdění	k1gNnSc4	brzdění
toku	tok	k1gInSc2	tok
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
a	a	k8xC	a
význam	význam	k1gInSc1	význam
mřížek	mřížka	k1gFnPc2	mřížka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
elektronky	elektronka	k1gFnSc2	elektronka
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
číslovky	číslovka	k1gFnSc2	číslovka
sedm	sedm	k4xCc4	sedm
(	(	kIx(	(
<g/>
hepta	hepta	k1gFnSc1	hepta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pět	pět	k4xCc1	pět
(	(	kIx(	(
<g/>
penta-grid	pentarid	k1gInSc1	penta-grid
=	=	kIx~	=
pět-mřížek	pětřížek	k1gInSc1	pět-mřížek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
především	především	k9	především
dvě	dva	k4xCgNnPc1	dva
zapojení	zapojení	k1gNnPc1	zapojení
heptody	heptoda	k1gFnSc2	heptoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
katoda	katoda	k1gFnSc1	katoda
a	a	k8xC	a
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
mřížky	mřížka	k1gFnPc1	mřížka
pracovaly	pracovat	k5eAaImAgFnP	pracovat
prakticky	prakticky	k6eAd1	prakticky
jako	jako	k8xS	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
trioda	trioda	k1gFnSc1	trioda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
druhá	druhý	k4xOgFnSc1	druhý
mřížka	mřížka	k1gFnSc1	mřížka
měla	mít	k5eAaImAgFnS	mít
funkci	funkce	k1gFnSc4	funkce
pomocné	pomocný	k2eAgFnSc2d1	pomocná
anody	anoda	k1gFnSc2	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
mřížka	mřížka	k1gFnSc1	mřížka
řídila	řídit	k5eAaImAgFnS	řídit
proud	proud	k1gInSc4	proud
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
pomocné	pomocný	k2eAgFnSc2d1	pomocná
triody	trioda	k1gFnSc2	trioda
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
a	a	k8xC	a
pátá	pátý	k4xOgFnSc1	pátý
mřížka	mřížka	k1gFnSc1	mřížka
byly	být	k5eAaImAgFnP	být
pak	pak	k6eAd1	pak
stínící	stínící	k2eAgFnPc1d1	stínící
<g/>
.	.	kIx.	.
</s>
<s>
Stínící	stínící	k2eAgFnPc1d1	stínící
mřížky	mřížka	k1gFnPc1	mřížka
byly	být	k5eAaImAgFnP	být
obvykle	obvykle	k6eAd1	obvykle
spojeny	spojit	k5eAaPmNgFnP	spojit
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
baňce	baňka	k1gFnSc6	baňka
a	a	k8xC	a
na	na	k7c6	na
patici	patice	k1gFnSc6	patice
vedl	vést	k5eAaImAgMnS	vést
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
společný	společný	k2eAgInSc4d1	společný
vývod	vývod	k1gInSc4	vývod
<g/>
.	.	kIx.	.
</s>
<s>
Vylepšením	vylepšení	k1gNnSc7	vylepšení
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
o	o	k7c4	o
brzdící	brzdící	k2eAgFnSc4d1	brzdící
mřížku	mřížka	k1gFnSc4	mřížka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
oktoda	oktoda	k1gFnSc1	oktoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
první	první	k4xOgFnSc1	první
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
mřížka	mřížka	k1gFnSc1	mřížka
byly	být	k5eAaImAgFnP	být
řídící	řídící	k2eAgFnPc1d1	řídící
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
a	a	k8xC	a
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
stínící	stínící	k2eAgFnSc1d1	stínící
<g/>
,	,	kIx,	,
pátá	pátý	k4xOgFnSc1	pátý
pak	pak	k6eAd1	pak
brzdící	brzdící	k2eAgFnSc1d1	brzdící
<g/>
.	.	kIx.	.
</s>
<s>
Elektronka	elektronka	k1gFnSc1	elektronka
prováděla	provádět	k5eAaImAgFnS	provádět
pouze	pouze	k6eAd1	pouze
směšování	směšování	k1gNnSc4	směšování
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
zdroj	zdroj	k1gInSc1	zdroj
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
osazen	osadit	k5eAaPmNgInS	osadit
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
elektronkou	elektronka	k1gFnSc7	elektronka
<g/>
.	.	kIx.	.
</s>
</p>
