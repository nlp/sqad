<s>
Heptoda	Heptoda	k1gFnSc1	Heptoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
zapojeních	zapojení	k1gNnPc6	zapojení
též	též	k6eAd1	též
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
pentagrid	pentagrid	k1gInSc4	pentagrid
je	být	k5eAaImIp3nS	být
elektronka	elektronka	k1gFnSc1	elektronka
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
elektrodami	elektroda	k1gFnPc7	elektroda
–	–	k?	–
katodou	katoda	k1gFnSc7	katoda
<g/>
,	,	kIx,	,
anodou	anoda	k1gFnSc7	anoda
a	a	k8xC	a
5	[number]	k4	5
mřížkami	mřížka	k1gFnPc7	mřížka
<g/>
.	.	kIx.	.
</s>
