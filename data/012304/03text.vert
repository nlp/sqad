<p>
<s>
Tenkozobec	tenkozobec	k1gMnSc1	tenkozobec
opačný	opačný	k2eAgMnSc1d1	opačný
(	(	kIx(	(
<g/>
Recurvirostra	Recurvirostra	k1gFnSc1	Recurvirostra
avosetta	avosetta	k1gFnSc1	avosetta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
dlouhokřídlý	dlouhokřídlý	k2eAgMnSc1d1	dlouhokřídlý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
tenkozobcovitých	tenkozobcovitý	k2eAgFnPc2d1	tenkozobcovitý
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
migruje	migrovat	k5eAaImIp3nS	migrovat
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
nebo	nebo	k8xC	nebo
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
s	s	k7c7	s
příznivějším	příznivý	k2eAgNnSc7d2	příznivější
klimatem	klima	k1gNnSc7	klima
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
celoročně	celoročně	k6eAd1	celoročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
pobývá	pobývat	k5eAaImIp3nS	pobývat
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
jej	on	k3xPp3gNnSc2	on
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
v	v	k7c6	v
logu	logo	k1gNnSc6	logo
britské	britský	k2eAgFnSc2d1	britská
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
43-48	[number]	k4	43-48
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
73	[number]	k4	73
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
290-400	[number]	k4	290-400
g	g	kA	g
</s>
<s>
Tenkozobec	Tenkozobec	k1gInSc1	Tenkozobec
opačný	opačný	k2eAgInSc1d1	opačný
je	být	k5eAaImIp3nS	být
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
pták	pták	k1gMnSc1	pták
dorůstající	dorůstající	k2eAgFnSc2d1	dorůstající
velikosti	velikost	k1gFnSc2	velikost
vrány	vrána	k1gFnSc2	vrána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgInSc6d1	zimní
šatě	šat	k1gInSc6	šat
má	mít	k5eAaImIp3nS	mít
převážně	převážně	k6eAd1	převážně
čistě	čistě	k6eAd1	čistě
bílé	bílý	k2eAgNnSc1d1	bílé
opeření	opeření	k1gNnSc1	opeření
s	s	k7c7	s
černým	černý	k2eAgMnSc7d1	černý
hřbetem	hřbet	k1gMnSc7	hřbet
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgFnSc7d1	vrchní
stranou	strana	k1gFnSc7	strana
hrdla	hrdlo	k1gNnSc2	hrdlo
a	a	k8xC	a
temenem	temeno	k1gNnSc7	temeno
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
,	,	kIx,	,
tenký	tenký	k2eAgInSc4d1	tenký
<g/>
,	,	kIx,	,
nahoru	nahoru	k6eAd1	nahoru
zahnutý	zahnutý	k2eAgMnSc1d1	zahnutý
a	a	k8xC	a
černě	černě	k6eAd1	černě
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
zobák	zobák	k1gInSc4	zobák
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
modravé	modravý	k2eAgFnPc4d1	modravá
až	až	k8xS	až
černé	černý	k2eAgFnPc4d1	černá
končetiny	končetina	k1gFnPc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
mládí	mládí	k1gNnSc6	mládí
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
pískovitě	pískovitě	k6eAd1	pískovitě
zbarvené	zbarvený	k2eAgNnSc4d1	zbarvené
opeření	opeření	k1gNnSc4	opeření
s	s	k7c7	s
šedými	šedý	k2eAgFnPc7d1	šedá
kresbami	kresba	k1gFnPc7	kresba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Tenkozobec	tenkozobec	k1gMnSc1	tenkozobec
opačný	opačný	k2eAgMnSc1d1	opačný
je	být	k5eAaImIp3nS	být
obyvatelem	obyvatel	k1gMnSc7	obyvatel
mělkých	mělký	k2eAgFnPc2d1	mělká
stojatých	stojatý	k2eAgFnPc2d1	stojatá
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
slaných	slaný	k2eAgNnPc2d1	slané
jezer	jezero	k1gNnPc2	jezero
nebo	nebo	k8xC	nebo
mokřin	mokřina	k1gFnPc2	mokřina
s	s	k7c7	s
řídkým	řídký	k2eAgInSc7d1	řídký
vegetačním	vegetační	k2eAgInSc7d1	vegetační
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
potravy	potrava	k1gFnSc2	potrava
upřeně	upřeně	k6eAd1	upřeně
hledí	hledět	k5eAaImIp3nS	hledět
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
při	při	k7c6	při
spatření	spatření	k1gNnSc6	spatření
kořisti	kořist	k1gFnSc2	kořist
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
svým	svůj	k3xOyFgInSc7	svůj
zahnutým	zahnutý	k2eAgInSc7d1	zahnutý
zobákem	zobák	k1gInSc7	zobák
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
potravou	potrava	k1gFnSc7	potrava
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
drobní	drobný	k2eAgMnPc1d1	drobný
vodní	vodní	k2eAgMnPc1d1	vodní
korýši	korýš	k1gMnPc1	korýš
nebo	nebo	k8xC	nebo
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
loví	lovit	k5eAaImIp3nP	lovit
i	i	k9	i
vodní	vodní	k2eAgInSc4d1	vodní
hmyz	hmyz	k1gInSc4	hmyz
nebo	nebo	k8xC	nebo
rybí	rybí	k2eAgInSc4d1	rybí
potěr	potěr	k1gInSc4	potěr
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
jasným	jasný	k2eAgInSc7d1	jasný
melodickým	melodický	k2eAgInSc7d1	melodický
"	"	kIx"	"
<g/>
pluit	pluit	k1gInSc1	pluit
pluit	pluit	k1gInSc1	pluit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
mimo	mimo	k7c4	mimo
období	období	k1gNnSc4	období
hnízdění	hnízděný	k2eAgMnPc1d1	hnízděný
jej	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
často	často	k6eAd1	často
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
v	v	k7c6	v
hejnech	hejno	k1gNnPc6	hejno
jiných	jiný	k2eAgMnPc2d1	jiný
pobřežních	pobřežní	k2eAgMnPc2d1	pobřežní
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
jde	jít	k5eAaImIp3nS	jít
nejčastěji	často	k6eAd3	často
o	o	k7c4	o
hejna	hejno	k1gNnPc4	hejno
racků	racek	k1gMnPc2	racek
chechtavých	chechtavý	k2eAgMnPc2d1	chechtavý
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vysoce	vysoce	k6eAd1	vysoce
složitý	složitý	k2eAgInSc4d1	složitý
svatební	svatební	k2eAgInSc4d1	svatební
ceremoniál	ceremoniál	k1gInSc4	ceremoniál
odehrávající	odehrávající	k2eAgInSc4d1	odehrávající
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
mělčinách	mělčina	k1gFnPc6	mělčina
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
června	červen	k1gInSc2	červen
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
klade	klást	k5eAaImIp3nS	klást
3-4	[number]	k4	3-4
vejce	vejce	k1gNnSc2	vejce
do	do	k7c2	do
mělkého	mělký	k2eAgInSc2d1	mělký
důlku	důlek	k1gInSc2	důlek
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
vystlaném	vystlaný	k2eAgInSc6d1	vystlaný
krátkou	krátký	k2eAgFnSc7d1	krátká
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nP	sedit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
23-35	[number]	k4	23-35
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc1	mládě
samostatná	samostatný	k2eAgNnPc1d1	samostatné
a	a	k8xC	a
vyrážejí	vyrážet	k5eAaImIp3nP	vyrážet
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
výpravu	výprava	k1gFnSc4	výprava
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
bere	brát	k5eAaImIp3nS	brát
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
učí	učit	k5eAaImIp3nS	učit
plavat	plavat	k5eAaImF	plavat
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nP	naučit
létat	létat	k5eAaImF	létat
již	již	k6eAd1	již
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
35-42	[number]	k4	35-42
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
stav	stav	k1gInSc1	stav
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
tenkozobců	tenkozobec	k1gMnPc2	tenkozobec
opačných	opačný	k2eAgInPc6d1	opačný
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pravidelně	pravidelně	k6eAd1	pravidelně
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
Českobudějovické	českobudějovický	k2eAgFnSc6d1	českobudějovická
pánvi	pánev	k1gFnSc6	pánev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zahnízdily	zahnízdit	k5eAaPmAgInP	zahnízdit
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
na	na	k7c6	na
částečně	částečně	k6eAd1	částečně
letněném	letněný	k2eAgInSc6d1	letněný
rybníce	rybník	k1gInSc6	rybník
Nesyt	nesyta	k1gFnPc2	nesyta
v	v	k7c6	v
NPR	NPR	kA	NPR
Lednické	lednický	k2eAgInPc4d1	lednický
rybníky	rybník	k1gInPc4	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
lokalitě	lokalita	k1gFnSc6	lokalita
zahnízdilo	zahnízdit	k5eAaPmAgNnS	zahnízdit
při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
letnění	letnění	k1gNnSc6	letnění
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
devět	devět	k4xCc1	devět
až	až	k9	až
jedenáct	jedenáct	k4xCc1	jedenáct
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
devět	devět	k4xCc4	devět
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
na	na	k7c6	na
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
vytvořeném	vytvořený	k2eAgInSc6d1	vytvořený
snížením	snížení	k1gNnSc7	snížení
hladiny	hladina	k1gFnSc2	hladina
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
pět	pět	k4xCc4	pět
hnízdění	hnízdění	k1gNnPc2	hnízdění
přitom	přitom	k6eAd1	přitom
bylo	být	k5eAaImAgNnS	být
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
a	a	k8xC	a
přísně	přísně	k6eAd1	přísně
chráněný	chráněný	k2eAgInSc4d1	chráněný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
africko-euroasijských	africkouroasijský	k2eAgMnPc2d1	africko-euroasijský
stěhovavých	stěhovavý	k2eAgMnPc2d1	stěhovavý
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
AEWA	AEWA	kA	AEWA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInPc1	zdroj
a	a	k8xC	a
reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Pied	Pied	k1gInSc4	Pied
Avocet	Avocet	k1gMnPc2	Avocet
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Szablodziób	Szablodziób	k1gInSc4	Szablodziób
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biolib	Biolib	k1gMnSc1	Biolib
</s>
</p>
