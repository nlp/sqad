<s>
Frekvence	frekvence	k1gFnSc1	frekvence
(	(	kIx(	(
<g/>
též	též	k9	též
kmitočet	kmitočet	k1gInSc1	kmitočet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
počet	počet	k1gInSc4	počet
opakování	opakování	k1gNnSc2	opakování
periodického	periodický	k2eAgInSc2d1	periodický
děje	děj	k1gInSc2	děj
za	za	k7c4	za
daný	daný	k2eAgInSc4d1	daný
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
<g/>
.	.	kIx.	.
</s>
