<p>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
(	(	kIx(	(
<g/>
též	též	k9	též
kmitočet	kmitočet	k1gInSc1	kmitočet
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
počet	počet	k1gInSc4	počet
opakování	opakování	k1gNnSc2	opakování
periodického	periodický	k2eAgInSc2d1	periodický
děje	děj	k1gInSc2	děj
za	za	k7c4	za
daný	daný	k2eAgInSc4d1	daný
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
takto	takto	k6eAd1	takto
označujeme	označovat	k5eAaImIp1nP	označovat
počet	počet	k1gInSc4	počet
kmitů	kmit	k1gInPc2	kmit
napětí	napětí	k1gNnSc2	napětí
či	či	k8xC	či
proudu	proud	k1gInSc2	proud
za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Frekvence	frekvence	k1gFnSc1	frekvence
==	==	k?	==
</s>
</p>
<p>
<s>
Symbol	symbol	k1gInSc1	symbol
veličiny	veličina	k1gFnSc2	veličina
<g/>
:	:	kIx,	:
f	f	k?	f
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
řecké	řecký	k2eAgNnSc1d1	řecké
písmeno	písmeno	k1gNnSc1	písmeno
ný	ný	k?	ný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
hertz	hertz	k1gInSc1	hertz
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
rozměr	rozměr	k1gInSc1	rozměr
1	[number]	k4	1
Hz	Hz	kA	Hz
=	=	kIx~	=
1	[number]	k4	1
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
používané	používaný	k2eAgInPc1d1	používaný
násobky	násobek	k1gInPc1	násobek
<g/>
:	:	kIx,	:
kilohertz	kilohertz	k1gInSc1	kilohertz
(	(	kIx(	(
<g/>
kHz	khz	kA	khz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
megahertz	megahertz	k1gInSc1	megahertz
(	(	kIx(	(
<g/>
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
a	a	k8xC	a
gigahertz	gigahertz	k1gMnSc1	gigahertz
(	(	kIx(	(
<g/>
GHz	GHz	k1gMnSc1	GHz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
otáčky	otáčka	k1gFnPc1	otáčka
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
(	(	kIx(	(
<g/>
ot	ot	k1gMnSc1	ot
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
veličinám	veličina	k1gFnPc3	veličina
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
frekvencí	frekvence	k1gFnSc7	frekvence
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
časovou	časový	k2eAgFnSc7d1	časová
periodou	perioda	k1gFnSc7	perioda
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
platí	platit	k5eAaImIp3nS	platit
vztah	vztah	k1gInSc4	vztah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
Při	při	k7c6	při
popisu	popis	k1gInSc6	popis
kmitání	kmitání	k1gNnSc2	kmitání
a	a	k8xC	a
vlnění	vlnění	k1gNnSc2	vlnění
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
úhlová	úhlový	k2eAgFnSc1d1	úhlová
frekvence	frekvence	k1gFnSc1	frekvence
(	(	kIx(	(
<g/>
úhlový	úhlový	k2eAgInSc1d1	úhlový
kmitočet	kmitočet	k1gInSc1	kmitočet
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
T	T	kA	T
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnPc6	omega
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
f	f	k?	f
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Frekvence	frekvence	k1gFnSc1	frekvence
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
vlny	vlna	k1gFnSc2	vlna
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
periodického	periodický	k2eAgNnSc2d1	periodické
vlnění	vlnění	k1gNnSc2	vlnění
se	se	k3xPyFc4	se
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
vlny	vlna	k1gFnSc2	vlna
<g/>
)	)	kIx)	)
určí	určit	k5eAaPmIp3nS	určit
podle	podle	k7c2	podle
vzorce	vzorec	k1gInSc2	vzorec
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
ω	ω	k?	ω
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
jednotky	jednotka	k1gFnPc1	jednotka
[	[	kIx(	[
<g/>
m	m	kA	m
<g/>
;	;	kIx,	;
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
Hz	Hz	kA	Hz
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
šíření	šíření	k1gNnSc2	šíření
vlnění	vlnění	k1gNnSc2	vlnění
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
</s>
</p>
<p>
<s>
délka	délka	k1gFnSc1	délka
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
vlnění	vlnění	k1gNnSc4	vlnění
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
zvukové	zvukový	k2eAgFnPc4d1	zvuková
vlny	vlna	k1gFnPc4	vlna
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rychlost	rychlost	k1gFnSc4	rychlost
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dřívější	dřívější	k2eAgNnSc4d1	dřívější
užití	užití	k1gNnSc4	užití
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgNnSc6d1	rozhlasové
vysílání	vysílání	k1gNnSc6	vysílání
==	==	k?	==
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
frekvence	frekvence	k1gFnSc2	frekvence
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
kmitočtu	kmitočet	k1gInSc2	kmitočet
<g/>
)	)	kIx)	)
např.	např.	kA	např.
nosné	nosný	k2eAgFnPc4d1	nosná
vlny	vlna	k1gFnPc4	vlna
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
udávala	udávat	k5eAaImAgFnS	udávat
délka	délka	k1gFnSc1	délka
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Údaj	údaj	k1gInSc4	údaj
vysíláme	vysílat	k5eAaImIp1nP	vysílat
na	na	k7c6	na
vlně	vlna	k1gFnSc6	vlna
183	[number]	k4	183
m	m	kA	m
tedy	tedy	k9	tedy
znamenal	znamenat	k5eAaImAgInS	znamenat
vysílání	vysílání	k1gNnSc4	vysílání
s	s	k7c7	s
nosnou	nosný	k2eAgFnSc7d1	nosná
frekvencí	frekvence	k1gFnSc7	frekvence
1640	[number]	k4	1640
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Udávání	udávání	k1gNnSc1	udávání
frekvence	frekvence	k1gFnSc2	frekvence
pomocí	pomocí	k7c2	pomocí
délky	délka	k1gFnSc2	délka
vlny	vlna	k1gFnSc2	vlna
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
<g/>
.	.	kIx.	.
</s>
<s>
Užitečná	užitečný	k2eAgFnSc1d1	užitečná
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
její	její	k3xOp3gFnSc1	její
znalost	znalost	k1gFnSc1	znalost
u	u	k7c2	u
nižších	nízký	k2eAgFnPc2d2	nižší
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
velikost	velikost	k1gFnSc4	velikost
antén	anténa	k1gFnPc2	anténa
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
průniku	průnik	k1gInSc2	průnik
překážkami	překážka	k1gFnPc7	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
používáme	používat	k5eAaImIp1nP	používat
označení	označení	k1gNnSc6	označení
jednotku	jednotka	k1gFnSc4	jednotka
Hz	Hz	kA	Hz
a	a	k8xC	a
její	její	k3xOp3gInPc4	její
násobky	násobek	k1gInPc4	násobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
užíváním	užívání	k1gNnSc7	užívání
jednotky	jednotka	k1gFnSc2	jednotka
Hz	Hz	kA	Hz
se	se	k3xPyFc4	se
také	také	k9	také
používala	používat	k5eAaImAgFnS	používat
zkratka	zkratka	k1gFnSc1	zkratka
cykly	cyklus	k1gInPc4	cyklus
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
100	[number]	k4	100
MHz	Mhz	kA	Mhz
se	se	k3xPyFc4	se
označovalo	označovat	k5eAaImAgNnS	označovat
jako	jako	k9	jako
100	[number]	k4	100
megacyklů	megacykl	k1gInPc2	megacykl
(	(	kIx(	(
<g/>
100	[number]	k4	100
Mcyc	Mcyc	k1gInSc1	Mcyc
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
cykly	cyklus	k1gInPc4	cyklus
za	za	k7c4	za
1	[number]	k4	1
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
megacykly	megacyknout	k5eAaPmAgInP	megacyknout
a	a	k8xC	a
kilocykly	kilocykl	k1gInPc1	kilocykl
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
jednotku	jednotka	k1gFnSc4	jednotka
SI	si	k1gNnSc2	si
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
u	u	k7c2	u
vzorkování	vzorkování	k1gNnSc2	vzorkování
jako	jako	k8xS	jako
vzorkovací	vzorkovací	k2eAgFnSc1d1	vzorkovací
frekvence	frekvence	k1gFnSc1	frekvence
jednotky	jednotka	k1gFnSc2	jednotka
vzorků	vzorek	k1gInPc2	vzorek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
100	[number]	k4	100
MS	MS	kA	MS
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
vzorky	vzorek	k1gInPc4	vzorek
za	za	k7c4	za
1	[number]	k4	1
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kmitání	kmitání	k1gNnSc1	kmitání
</s>
</p>
<p>
<s>
Vlnění	vlnění	k1gNnSc1	vlnění
</s>
</p>
<p>
<s>
Úhlová	úhlový	k2eAgFnSc1d1	úhlová
frekvence	frekvence	k1gFnSc1	frekvence
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
frekvence	frekvence	k1gFnSc2	frekvence
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
frekvence	frekvence	k1gFnSc1	frekvence
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
