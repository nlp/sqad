<s>
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
zastánci	zastánce	k1gMnPc1	zastánce
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c4	po
reformaci	reformace	k1gFnSc4	reformace
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kalvinismem	kalvinismus	k1gInSc7	kalvinismus
a	a	k8xC	a
luteránstvím	luteránství	k1gNnSc7	luteránství
</s>
