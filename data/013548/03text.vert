<s>
Peugeot	peugeot	k1gInSc1
1007	#num#	k4
</s>
<s>
Peugeot	peugeot	k1gInSc4
1007	#num#	k4
Peugeot	peugeot	k1gInSc1
1007	#num#	k4
<g/>
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Peugeot	peugeot	k1gInSc1
Koncern	koncern	k1gInSc1
</s>
<s>
PSA	pes	k1gMnSc4
Peugeot-Citroën	Peugeot-Citroëna	k1gFnPc2
Roky	rok	k1gInPc1
produkce	produkce	k1gFnSc2
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
Nástupce	nástupce	k1gMnSc2
</s>
<s>
Peugeot	peugeot	k1gInSc1
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
Příbuzné	příbuzný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
</s>
<s>
Peugeot	peugeot	k1gInSc1
206	#num#	k4
<g/>
,	,	kIx,
<g/>
Citroen	citroen	k1gInSc4
C3	C3	k1gFnSc2
Konkurence	konkurence	k1gFnSc1
</s>
<s>
Opel	opel	k1gInSc1
Agila	Agila	k1gFnSc1
<g/>
,	,	kIx,
Suzuki	suzuki	k1gNnSc1
Splash	Splash	k1gMnSc1
Designér	designér	k1gMnSc1
</s>
<s>
Pininfarina	Pininfarina	k1gMnSc1
Technické	technický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Délka	délka	k1gFnSc1
</s>
<s>
3730	#num#	k4
mm	mm	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
1820	#num#	k4
mm	mm	kA
Výška	výška	k1gFnSc1
</s>
<s>
1620	#num#	k4
mm	mm	kA
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
Šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Zastaralý	zastaralý	k2eAgInSc1d1
parametr	parametr	k1gInSc1
infoboxu	infobox	k1gInSc2
<g/>
}}	}}	k?
prohlašuje	prohlašovat	k5eAaImIp3nS
parametr	parametr	k1gInSc1
"	"	kIx"
<g/>
hmotnost	hmotnost	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
"	"	kIx"
<g/>
1140	#num#	k4
–	–	k?
1216	#num#	k4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
šablony	šablona	k1gFnPc1
"	"	kIx"
<g/>
Infobox	Infobox	k1gInSc1
-	-	kIx~
automobil	automobil	k1gInSc1
<g/>
"	"	kIx"
za	za	k7c4
zastaralý	zastaralý	k2eAgInSc4d1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
1140	#num#	k4
–	–	k?
1216	#num#	k4
(	(	kIx(
<g/>
parametr	parametr	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
upřesnit	upřesnit	k5eAaPmF
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Peugeot	Peugeot	k1gInSc1
1007	#num#	k4
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc1d1
třídveřový	třídveřový	k2eAgInSc1d1
MPV	MPV	kA
</s>
<s>
automobil	automobil	k1gInSc1
vyráběný	vyráběný	k2eAgInSc1d1
francouzskou	francouzský	k2eAgFnSc7d1
automobilkou	automobilka	k1gFnSc7
Peugeot	peugeot	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
z	z	k7c2
konceptu	koncept	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
představen	představit	k5eAaPmNgInS
na	na	k7c6
autosalonu	autosalon	k1gInSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
automobil	automobil	k1gInSc1
Peugeot	Peugeot	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
název	název	k1gInSc1
s	s	k7c7
čtyřciferným	čtyřciferný	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůz	vůz	k1gInSc1
je	být	k5eAaImIp3nS
charakteristický	charakteristický	k2eAgInSc1d1
svými	svůj	k3xOyFgFnPc7
posuvnými	posuvný	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Motory	motor	k1gInPc1
</s>
<s>
ObjemTypVýkon	ObjemTypVýkon	k1gMnSc1
</s>
<s>
1,4	1,4	k4
L	L	kA
(	(	kIx(
<g/>
1360	#num#	k4
cm³	cm³	k?
<g/>
)	)	kIx)
<g/>
TU	tu	k9
<g/>
355	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
74	#num#	k4
koní	kůň	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
1,4	1,4	k4
L	L	kA
HDi	HDi	k1gFnSc1
diesel	diesel	k1gInSc1
(	(	kIx(
<g/>
1398	#num#	k4
cm³	cm³	k?
<g/>
)	)	kIx)
<g/>
DV	DV	kA
<g/>
450	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
68	#num#	k4
koní	kůň	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
1,6	1,6	k4
L	L	kA
(	(	kIx(
<g/>
1587	#num#	k4
cm³	cm³	k?
<g/>
)	)	kIx)
<g/>
TU	tu	k9
<g/>
581	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
108	#num#	k4
koní	kůň	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Peugeot	peugeot	k1gInSc1
1007	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
</s>
<s>
•	•	k?
kz	kz	k?
•	•	k?
d	d	k?
•	•	k?
e	e	k0
Automobily	automobil	k1gInPc4
značky	značka	k1gFnSc2
Peugeot	peugeot	k1gInSc1
1980	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
1980	#num#	k4
</s>
<s>
199020002010	#num#	k4
</s>
<s>
0123456789012345678901234567890123456789	#num#	k4
</s>
<s>
Mini	mini	k2eAgFnSc1d1
</s>
<s>
104	#num#	k4
</s>
<s>
106	#num#	k4
</s>
<s>
107	#num#	k4
</s>
<s>
108	#num#	k4
</s>
<s>
Malé	Malé	k2eAgInPc1d1
</s>
<s>
205	#num#	k4
</s>
<s>
206	#num#	k4
</s>
<s>
207	#num#	k4
</s>
<s>
208	#num#	k4
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
střední	střední	k2eAgFnSc1d1
</s>
<s>
301	#num#	k4
</s>
<s>
305	#num#	k4
</s>
<s>
309	#num#	k4
</s>
<s>
306	#num#	k4
</s>
<s>
308	#num#	k4
</s>
<s>
308	#num#	k4
II	II	kA
</s>
<s>
307	#num#	k4
</s>
<s>
408	#num#	k4
</s>
<s>
408	#num#	k4
II	II	kA
</s>
<s>
Střední	střední	k2eAgFnSc1d1
</s>
<s>
405	#num#	k4
</s>
<s>
406	#num#	k4
</s>
<s>
407	#num#	k4
</s>
<s>
508	#num#	k4
</s>
<s>
508	#num#	k4
II	II	kA
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
střední	střední	k2eAgFnSc1d1
</s>
<s>
505	#num#	k4
</s>
<s>
506	#num#	k4
</s>
<s>
604	#num#	k4
</s>
<s>
605	#num#	k4
</s>
<s>
607	#num#	k4
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1
</s>
<s>
RCZ	RCZ	kA
</s>
<s>
MPV	MPV	kA
</s>
<s>
1007	#num#	k4
</s>
<s>
3008	#num#	k4
</s>
<s>
5008	#num#	k4
</s>
<s>
806	#num#	k4
</s>
<s>
807	#num#	k4
</s>
<s>
SUV	SUV	kA
<g/>
/	/	kIx~
<g/>
crossover	crossover	k1gMnSc1
</s>
<s>
4007	#num#	k4
</s>
<s>
5008	#num#	k4
</s>
<s>
3008	#num#	k4
</s>
<s>
3008	#num#	k4
II	II	kA
</s>
<s>
4008	#num#	k4
</s>
<s>
2008	#num#	k4
</s>
<s>
2008	#num#	k4
II	II	kA
</s>
<s>
Užitkové	užitkový	k2eAgNnSc1d1
</s>
<s>
Bipper	Bipper	k1gMnSc1
</s>
<s>
Partner	partner	k1gMnSc1
I	i	k9
</s>
<s>
Partner	partner	k1gMnSc1
II	II	kA
</s>
<s>
Rifter	Rifter	k1gMnSc1
</s>
<s>
J9	J9	k4
</s>
<s>
Expert	expert	k1gMnSc1
I	i	k9
</s>
<s>
Expert	expert	k1gMnSc1
II	II	kA
</s>
<s>
Expert	expert	k1gMnSc1
III	III	kA
</s>
<s>
J5	J5	k4
</s>
<s>
Boxer	boxer	k1gMnSc1
I	i	k9
</s>
<s>
Boxer	boxer	k1gMnSc1
II	II	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
</s>
