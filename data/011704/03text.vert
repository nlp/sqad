<p>
<s>
Cement	cement	k1gInSc1	cement
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
pojivo	pojivo	k1gNnSc1	pojivo
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
tuhnout	tuhnout	k5eAaImF	tuhnout
a	a	k8xC	a
vázat	vázat	k5eAaImF	vázat
další	další	k2eAgInPc4d1	další
materiály	materiál	k1gInPc4	materiál
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Nejobvyklejší	obvyklý	k2eAgInSc1d3	nejobvyklejší
portlandský	portlandský	k2eAgInSc1d1	portlandský
cement	cement	k1gInSc1	cement
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zejména	zejména	k9	zejména
oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
a	a	k8xC	a
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgInSc1d1	výrobní
proces	proces	k1gInSc1	proces
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pálení	pálení	k1gNnSc4	pálení
vápence	vápenec	k1gInSc2	vápenec
s	s	k7c7	s
jílem	jíl	k1gInSc7	jíl
či	či	k8xC	či
pískem	písek	k1gInSc7	písek
a	a	k8xC	a
případnými	případný	k2eAgFnPc7d1	případná
příměsemi	příměse	k1gFnPc7	příměse
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
betonu	beton	k1gInSc2	beton
či	či	k8xC	či
malty	malta	k1gFnSc2	malta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
Kořeny	kořen	k1gInPc1	kořen
slova	slovo	k1gNnSc2	slovo
cement	cement	k1gInSc1	cement
sahají	sahat	k5eAaImIp3nP	sahat
k	k	k7c3	k
starověkým	starověký	k2eAgMnPc3d1	starověký
Římanům	Říman	k1gMnPc3	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
výraz	výraz	k1gInSc4	výraz
"	"	kIx"	"
<g/>
opus	opus	k1gInSc1	opus
caementicium	caementicium	k1gNnSc1	caementicium
<g/>
"	"	kIx"	"
používali	používat	k5eAaImAgMnP	používat
pro	pro	k7c4	pro
zdivo	zdivo	k1gNnSc4	zdivo
podobné	podobný	k2eAgNnSc4d1	podobné
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
obsahující	obsahující	k2eAgNnSc4d1	obsahující
jako	jako	k8xC	jako
pojivo	pojivo	k1gNnSc4	pojivo
pálené	pálený	k2eAgNnSc4d1	pálené
vápno	vápno	k1gNnSc4	vápno
<g/>
.	.	kIx.	.
</s>
<s>
Sopečný	sopečný	k2eAgInSc1d1	sopečný
popel	popel	k1gInSc1	popel
pucolán	pucolána	k1gFnPc2	pucolána
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
páleným	pálený	k2eAgNnSc7d1	pálené
vápnem	vápno	k1gNnSc7	vápno
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
hydraulické	hydraulický	k2eAgNnSc1d1	hydraulické
pojivo	pojivo	k1gNnSc1	pojivo
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
Římany	Říman	k1gMnPc7	Říman
cementum	cementum	k1gNnSc1	cementum
<g/>
,	,	kIx,	,
cimentum	cimentum	k1gNnSc1	cimentum
<g/>
,	,	kIx,	,
cäment	cäment	k1gInSc1	cäment
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
cement	cement	k1gInSc4	cement
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
slovo	slovo	k1gNnSc1	slovo
cement	cement	k1gInSc1	cement
označuje	označovat	k5eAaImIp3nS	označovat
práškové	práškový	k2eAgNnSc4d1	práškové
pojivo	pojivo	k1gNnSc4	pojivo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
schopnosti	schopnost	k1gFnPc1	schopnost
pojit	pojit	k5eAaImF	pojit
jiné	jiný	k2eAgFnPc4d1	jiná
sypké	sypký	k2eAgFnPc4d1	sypká
látky	látka	k1gFnPc4	látka
v	v	k7c4	v
pevnou	pevný	k2eAgFnSc4d1	pevná
hmotu	hmota	k1gFnSc4	hmota
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
betonových	betonový	k2eAgFnPc2d1	betonová
nebo	nebo	k8xC	nebo
maltových	maltový	k2eAgFnPc2d1	maltová
směsí	směs	k1gFnPc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
hydraulický	hydraulický	k2eAgInSc4d1	hydraulický
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
po	po	k7c6	po
smíchání	smíchání	k1gNnSc6	smíchání
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
a	a	k8xC	a
tvrdne	tvrdnout	k5eAaImIp3nS	tvrdnout
(	(	kIx(	(
<g/>
např.	např.	kA	např.
portlandský	portlandský	k2eAgInSc1d1	portlandský
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pro	pro	k7c4	pro
tvrdnutí	tvrdnutí	k1gNnPc4	tvrdnutí
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
přítomnost	přítomnost	k1gFnSc1	přítomnost
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
(	(	kIx(	(
<g/>
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc1	starověk
a	a	k8xC	a
středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc4	první
použití	použití	k1gNnSc4	použití
pojiv	pojivo	k1gNnPc2	pojivo
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
přírodního	přírodní	k2eAgInSc2d1	přírodní
nebo	nebo	k8xC	nebo
vyráběného	vyráběný	k2eAgInSc2d1	vyráběný
cementu	cement	k1gInSc2	cement
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
republikánského	republikánský	k2eAgNnSc2d1	republikánské
období	období	k1gNnSc2	období
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
materiál	materiál	k1gInSc1	materiál
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
pojiva	pojivo	k1gNnSc2	pojivo
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
sopečný	sopečný	k2eAgInSc1d1	sopečný
produkt	produkt	k1gInSc1	produkt
pucolán	pucolán	k1gInSc1	pucolán
–	–	k?	–
přírodní	přírodní	k2eAgInSc1d1	přírodní
hydraulický	hydraulický	k2eAgInSc1d1	hydraulický
cement	cement	k1gInSc1	cement
s	s	k7c7	s
vynikajícími	vynikající	k2eAgFnPc7d1	vynikající
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
pojiv	pojivo	k1gNnPc2	pojivo
umožnil	umožnit	k5eAaPmAgInS	umožnit
vybudování	vybudování	k1gNnSc4	vybudování
významných	významný	k2eAgFnPc2d1	významná
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
přístavních	přístavní	k2eAgFnPc2d1	přístavní
hrází	hráz	k1gFnPc2	hráz
<g/>
,	,	kIx,	,
akvaduktů	akvadukt	k1gInPc2	akvadukt
a	a	k8xC	a
mostů	most	k1gInPc2	most
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vynikajících	vynikající	k2eAgInPc2d1	vynikající
příkladů	příklad	k1gInPc2	příklad
těchto	tento	k3xDgFnPc2	tento
staveb	stavba	k1gFnPc2	stavba
ještě	ještě	k6eAd1	ještě
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Technologickým	technologický	k2eAgInSc7d1	technologický
zázrakem	zázrak	k1gInSc7	zázrak
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
obrovská	obrovský	k2eAgFnSc1d1	obrovská
monolitická	monolitický	k2eAgFnSc1d1	monolitická
kopule	kopule	k1gFnSc1	kopule
na	na	k7c6	na
Pantheonu	Pantheon	k1gInSc6	Pantheon
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
43,3	[number]	k4	43,3
m	m	kA	m
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
technologií	technologie	k1gFnSc7	technologie
litého	litý	k2eAgInSc2d1	litý
betonu	beton	k1gInSc2	beton
za	za	k7c4	za
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
118	[number]	k4	118
<g/>
–	–	k?	–
<g/>
125	[number]	k4	125
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kopule	kopule	k1gFnSc1	kopule
srovnatelné	srovnatelný	k2eAgFnSc2d1	srovnatelná
velikosti	velikost	k1gFnSc2	velikost
byly	být	k5eAaImAgFnP	být
ještě	ještě	k9	ještě
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
stavěny	stavit	k5eAaImNgInP	stavit
technologií	technologie	k1gFnSc7	technologie
kamenné	kamenný	k2eAgFnSc2d1	kamenná
klenby	klenba	k1gFnSc2	klenba
po	po	k7c4	po
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
všeobecně	všeobecně	k6eAd1	všeobecně
přijímaného	přijímaný	k2eAgInSc2d1	přijímaný
názoru	názor	k1gInSc2	názor
byla	být	k5eAaImAgFnS	být
znalost	znalost	k1gFnSc4	znalost
používání	používání	k1gNnSc2	používání
hydraulických	hydraulický	k2eAgNnPc2d1	hydraulické
pojiv	pojivo	k1gNnPc2	pojivo
ztracena	ztraceno	k1gNnSc2	ztraceno
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
znovuobjevena	znovuobjeven	k2eAgFnSc1d1	znovuobjeven
až	až	k9	až
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
novověkými	novověký	k2eAgInPc7d1	novověký
pokusy	pokus	k1gInPc7	pokus
Johna	John	k1gMnSc2	John
Smeatona	Smeaton	k1gMnSc2	Smeaton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
překvapivé	překvapivý	k2eAgFnPc4d1	překvapivá
analýzy	analýza	k1gFnPc4	analýza
původního	původní	k2eAgNnSc2d1	původní
zdiva	zdivo	k1gNnSc2	zdivo
Karlova	Karlův	k2eAgInSc2d1	Karlův
mostu	most	k1gInSc2	most
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
unikátní	unikátní	k2eAgInSc4d1	unikátní
příklad	příklad	k1gInSc4	příklad
pokračování	pokračování	k1gNnSc2	pokračování
antické	antický	k2eAgFnSc2d1	antická
tradice	tradice	k1gFnSc2	tradice
použití	použití	k1gNnSc2	použití
vysoce	vysoce	k6eAd1	vysoce
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
malt	malta	k1gFnPc2	malta
<g/>
/	/	kIx~	/
<g/>
betonů	beton	k1gInPc2	beton
s	s	k7c7	s
hydraulickým	hydraulický	k2eAgNnSc7d1	hydraulické
pojivem	pojivo	k1gNnSc7	pojivo
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
středověké	středověký	k2eAgFnSc6d1	středověká
stavbě	stavba	k1gFnSc6	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novodobá	novodobý	k2eAgFnSc1d1	novodobá
historie	historie	k1gFnSc1	historie
výroby	výroba	k1gFnSc2	výroba
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
obdržel	obdržet	k5eAaPmAgMnS	obdržet
patent	patent	k1gInSc4	patent
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
cementu	cement	k1gInSc2	cement
John	John	k1gMnSc1	John
Aspdin	Aspdina	k1gFnPc2	Aspdina
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
minimální	minimální	k2eAgFnPc4d1	minimální
vědecké	vědecký	k2eAgFnPc4d1	vědecká
znalosti	znalost	k1gFnPc4	znalost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
William	William	k1gInSc4	William
Aspdin	Aspdin	k2eAgInSc4d1	Aspdin
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
cementu	cement	k1gInSc2	cement
v	v	k7c4	v
North	North	k1gInSc4	North
West	West	k2eAgInSc4d1	West
Kent	Kent	k1gInSc4	Kent
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgInS	působit
i	i	k9	i
Joseph	Joseph	k1gInSc1	Joseph
Aspdin	Aspdin	k2eAgInSc1d1	Aspdin
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
u	u	k7c2	u
Boulogne	Boulogn	k1gInSc5	Boulogn
sur	sur	k?	sur
Meer	Meera	k1gFnPc2	Meera
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výrobna	výrobna	k1gFnSc1	výrobna
cementu	cement	k1gInSc2	cement
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
firma	firma	k1gFnSc1	firma
Brunkhorst	Brunkhorst	k1gInSc1	Brunkhorst
&	&	k?	&
Westfalen	Westfalen	k2eAgInSc1d1	Westfalen
v	v	k7c6	v
Buxtehude	Buxtehud	k1gInSc5	Buxtehud
u	u	k7c2	u
Hamburku	Hamburk	k1gInSc6	Hamburk
první	první	k4xOgMnSc1	první
portlandský	portlandský	k2eAgInSc1d1	portlandský
cement	cement	k1gInSc1	cement
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
několik	několik	k4yIc4	několik
německých	německý	k2eAgMnPc2d1	německý
šlechticů	šlechtic	k1gMnPc2	šlechtic
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
založilo	založit	k5eAaPmAgNnS	založit
výrobu	výroba	k1gFnSc4	výroba
cementu	cement	k1gInSc2	cement
v	v	k7c6	v
Bohosudově	Bohosudův	k2eAgInSc6d1	Bohosudův
u	u	k7c2	u
Teplic	Teplice	k1gFnPc2	Teplice
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
kapitálu	kapitál	k1gInSc2	kapitál
založil	založit	k5eAaPmAgMnS	založit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Barta	Barta	k1gMnSc1	Barta
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
cementárnu	cementárna	k1gFnSc4	cementárna
v	v	k7c6	v
Radotíně	Radotín	k1gInSc6	Radotín
a	a	k8xC	a
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
cementárnu	cementárna	k1gFnSc4	cementárna
v	v	k7c6	v
Podolí	Podolí	k1gNnSc6	Podolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
začala	začít	k5eAaPmAgFnS	začít
výroba	výroba	k1gFnSc1	výroba
portlandského	portlandský	k2eAgInSc2d1	portlandský
cementu	cement	k1gInSc2	cement
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výroba	výroba	k1gFnSc1	výroba
cementu	cement	k1gInSc2	cement
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ochrany	ochrana	k1gFnSc2	ochrana
zdraví	zdraví	k1gNnSc2	zdraví
a	a	k8xC	a
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Cement	cement	k1gInSc1	cement
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
nepříznivě	příznivě	k6eNd1	příznivě
těmito	tento	k3xDgInPc7	tento
účinky	účinek	k1gInPc7	účinek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
drážděním	dráždění	k1gNnSc7	dráždění
pokožky	pokožka	k1gFnSc2	pokožka
(	(	kIx(	(
<g/>
iritační	iritační	k2eAgFnSc1d1	iritační
dermatitida	dermatitida	k1gFnSc1	dermatitida
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vlivem	vliv	k1gInSc7	vliv
přítomných	přítomný	k2eAgFnPc2d1	přítomná
sloučenin	sloučenina	k1gFnPc2	sloučenina
šestimocného	šestimocný	k2eAgInSc2d1	šestimocný
chromu	chrom	k1gInSc2	chrom
na	na	k7c4	na
pokožku	pokožka	k1gFnSc4	pokožka
(	(	kIx(	(
<g/>
alergická	alergický	k2eAgFnSc1d1	alergická
dermatitida	dermatitida	k1gFnSc1	dermatitida
–	–	k?	–
citlivost	citlivost	k1gFnSc1	citlivost
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
u	u	k7c2	u
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
stavebních	stavební	k2eAgMnPc2d1	stavební
dělníků	dělník	k1gMnPc2	dělník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
popálením	popálení	k1gNnSc7	popálení
–	–	k?	–
směs	směs	k1gFnSc1	směs
cementu	cement	k1gInSc2	cement
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
zásaditá	zásaditý	k2eAgFnSc1d1	zásaditá
–	–	k?	–
zejména	zejména	k9	zejména
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
zabránit	zabránit	k5eAaPmF	zabránit
vniknutí	vniknutí	k1gNnSc4	vniknutí
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
</s>
</p>
<p>
<s>
vdechnutím	vdechnutí	k1gNnSc7	vdechnutí
prachuK	prachuK	k?	prachuK
omezení	omezení	k1gNnSc2	omezení
účinků	účinek	k1gInPc2	účinek
šestimocného	šestimocný	k2eAgInSc2d1	šestimocný
chromu	chrom	k1gInSc2	chrom
se	se	k3xPyFc4	se
cement	cement	k1gInSc1	cement
v	v	k7c6	v
baleních	balení	k1gNnPc6	balení
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
ruční	ruční	k2eAgNnSc4d1	ruční
zpracování	zpracování	k1gNnSc4	zpracování
(	(	kIx(	(
<g/>
pytlovaný	pytlovaný	k2eAgMnSc1d1	pytlovaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mísí	mísit	k5eAaImIp3nP	mísit
se	s	k7c7	s
specifickými	specifický	k2eAgNnPc7d1	specifické
redukčními	redukční	k2eAgNnPc7d1	redukční
činidly	činidlo	k1gNnPc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Volně	volně	k6eAd1	volně
ložený	ložený	k2eAgInSc1d1	ložený
cement	cement	k1gInSc1	cement
(	(	kIx(	(
<g/>
vagónový	vagónový	k2eAgInSc1d1	vagónový
<g/>
,	,	kIx,	,
z	z	k7c2	z
automobilových	automobilový	k2eAgInPc2d1	automobilový
přepravníků	přepravník	k1gInPc2	přepravník
<g/>
)	)	kIx)	)
určený	určený	k2eAgMnSc1d1	určený
pro	pro	k7c4	pro
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nepřijde	přijít	k5eNaPmIp3nS	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
,	,	kIx,	,
limity	limit	k1gInPc7	limit
obsahu	obsah	k1gInSc2	obsah
chromu	chrom	k1gInSc2	chrom
nemusí	muset	k5eNaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
a	a	k8xC	a
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
jej	on	k3xPp3gMnSc4	on
ručně	ručně	k6eAd1	ručně
je	být	k5eAaImIp3nS	být
nežádoucí	žádoucí	k2eNgFnSc1d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nebezpečnost	nebezpečnost	k1gFnSc1	nebezpečnost
hotového	hotový	k2eAgInSc2d1	hotový
cementu	cement	k1gInSc2	cement
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
se	se	k3xPyFc4	se
neočekává	očekávat	k5eNaImIp3nS	očekávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
hmoty	hmota	k1gFnPc1	hmota
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
elektronická	elektronický	k2eAgFnSc1d1	elektronická
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
260	[number]	k4	260
<g/>
-	-	kIx~	-
<g/>
4972	[number]	k4	4972
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
950	[number]	k4	950
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cement	cement	k1gInSc1	cement
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cement	cement	k1gInSc1	cement
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
