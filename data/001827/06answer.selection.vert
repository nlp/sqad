<s>
Avantgarda	avantgarda	k1gFnSc1	avantgarda
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
francouzského	francouzský	k2eAgInSc2d1	francouzský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
avantgarde	avantgard	k1gMnSc5	avantgard
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
avant	avant	k1gInSc1	avant
=	=	kIx~	=
před	před	k7c7	před
<g/>
,	,	kIx,	,
la	la	k1gNnSc4	la
garde	garde	k1gNnSc1	garde
=	=	kIx~	=
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
,	,	kIx,	,
stráž	stráž	k1gFnSc1	stráž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
znamenající	znamenající	k2eAgInSc1d1	znamenající
vojenský	vojenský	k2eAgInSc1d1	vojenský
předvoj	předvoj	k1gInSc1	předvoj
<g/>
.	.	kIx.	.
</s>
