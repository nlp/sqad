<s>
Avantgarda	avantgarda	k1gFnSc1	avantgarda
bylo	být	k5eAaImAgNnS	být
kulturní	kulturní	k2eAgInSc4d1	kulturní
a	a	k8xC	a
umělecké	umělecký	k2eAgNnSc4d1	umělecké
hnutí	hnutí	k1gNnSc4	hnutí
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Avantgardní	avantgardní	k2eAgNnSc1d1	avantgardní
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
etapa	etapa	k1gFnSc1	etapa
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc7d1	poslední
vývojovou	vývojový	k2eAgFnSc7d1	vývojová
fází	fáze	k1gFnSc7	fáze
novodobého	novodobý	k2eAgNnSc2d1	novodobé
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Předcházející	předcházející	k2eAgFnSc7d1	předcházející
etapou	etapa	k1gFnSc7	etapa
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
umění	umění	k1gNnSc2	umění
byla	být	k5eAaImAgFnS	být
moderna	moderna	k1gFnSc1	moderna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
přechodným	přechodný	k2eAgNnSc7d1	přechodné
vývojovým	vývojový	k2eAgNnSc7d1	vývojové
stadiem	stadion	k1gNnSc7	stadion
mezi	mezi	k7c7	mezi
tradičním	tradiční	k2eAgNnSc7d1	tradiční
uměním	umění	k1gNnSc7	umění
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
modernou	moderna	k1gFnSc7	moderna
je	být	k5eAaImIp3nS	být
avantgarda	avantgarda	k1gFnSc1	avantgarda
spojena	spojit	k5eAaPmNgFnS	spojit
antitradicionalismem	antitradicionalismus	k1gInSc7	antitradicionalismus
a	a	k8xC	a
společenskou	společenský	k2eAgFnSc7d1	společenská
revoltou	revolta	k1gFnSc7	revolta
<g/>
.	.	kIx.	.
</s>
<s>
Etapou	etapa	k1gFnSc7	etapa
následující	následující	k2eAgFnSc7d1	následující
je	být	k5eAaImIp3nS	být
postmoderní	postmoderní	k2eAgNnSc4d1	postmoderní
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Výchozími	výchozí	k2eAgFnPc7d1	výchozí
ideovými	ideový	k2eAgFnPc7d1	ideová
ohnisky	ohnisko	k1gNnPc7	ohnisko
bylo	být	k5eAaImAgNnS	být
poválečnému	poválečný	k2eAgInSc3d1	poválečný
avantgardnímu	avantgardní	k2eAgInSc3d1	avantgardní
hnutí	hnutí	k1gNnPc2	hnutí
curyšské	curyšský	k2eAgNnSc1d1	Curyšské
dada	dada	k1gNnSc1	dada
a	a	k8xC	a
sovětské	sovětský	k2eAgNnSc1d1	sovětské
revoluční	revoluční	k2eAgNnSc1d1	revoluční
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
znaky	znak	k1gInPc1	znak
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
:	:	kIx,	:
odmítání	odmítání	k1gNnSc3	odmítání
sociální	sociální	k2eAgFnSc2d1	sociální
nespravedlnosti	nespravedlnost	k1gFnSc2	nespravedlnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plynoucí	plynoucí	k2eAgInSc1d1	plynoucí
příklon	příklon	k1gInSc4	příklon
k	k	k7c3	k
radikálním	radikální	k2eAgInPc3d1	radikální
a	a	k8xC	a
extremistickým	extremistický	k2eAgNnSc7d1	extremistické
politickým	politický	k2eAgNnSc7d1	politické
hnutím	hnutí	k1gNnSc7	hnutí
(	(	kIx(	(
<g/>
komunismus	komunismus	k1gInSc1	komunismus
<g/>
,	,	kIx,	,
anarchismus	anarchismus	k1gInSc1	anarchismus
<g/>
,	,	kIx,	,
fašismus	fašismus	k1gInSc1	fašismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
například	například	k6eAd1	například
pro	pro	k7c4	pro
italský	italský	k2eAgInSc4d1	italský
futurismus	futurismus	k1gInSc4	futurismus
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
kolektivismus	kolektivismus	k1gInSc1	kolektivismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
projevoval	projevovat	k5eAaImAgInS	projevovat
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
sdružování	sdružování	k1gNnSc4	sdružování
se	se	k3xPyFc4	se
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
spolků	spolek	k1gInPc2	spolek
<g/>
,	,	kIx,	,
svazů	svaz	k1gInPc2	svaz
a	a	k8xC	a
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
Bauhaus	Bauhaus	k1gMnSc1	Bauhaus
<g/>
,	,	kIx,	,
De	De	k?	De
Stijl	Stijl	k1gInSc1	Stijl
<g/>
,	,	kIx,	,
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podstatným	podstatný	k2eAgInSc7d1	podstatný
rysem	rys	k1gInSc7	rys
je	být	k5eAaImIp3nS	být
experimentování	experimentování	k1gNnSc1	experimentování
a	a	k8xC	a
hledání	hledání	k1gNnSc1	hledání
nových	nový	k2eAgInPc2d1	nový
obsahů	obsah	k1gInPc2	obsah
a	a	k8xC	a
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
vzniku	vznik	k1gInSc2	vznik
avantgardy	avantgarda	k1gFnSc2	avantgarda
je	být	k5eAaImIp3nS	být
obdobím	období	k1gNnSc7	období
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
krachu	krach	k1gInSc2	krach
evropských	evropský	k2eAgInPc2d1	evropský
humanitních	humanitní	k2eAgInPc2d1	humanitní
ideálů	ideál	k1gInPc2	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
však	však	k9	však
také	také	k9	také
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
idea	idea	k1gFnSc1	idea
nové	nový	k2eAgFnSc2d1	nová
a	a	k8xC	a
spravedlivější	spravedlivý	k2eAgFnSc2d2	spravedlivější
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
přicházející	přicházející	k2eAgNnSc1d1	přicházející
s	s	k7c7	s
Říjnovou	říjnový	k2eAgFnSc7d1	říjnová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
událostmi	událost	k1gFnPc7	událost
probíhá	probíhat	k5eAaImIp3nS	probíhat
bouřlivý	bouřlivý	k2eAgInSc4d1	bouřlivý
rozvoj	rozvoj	k1gInSc4	rozvoj
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
víře	víra	k1gFnSc6	víra
v	v	k7c4	v
utopistickou	utopistický	k2eAgFnSc4d1	utopistická
vizi	vize	k1gFnSc4	vize
nového	nový	k2eAgInSc2d1	nový
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ovládaného	ovládaný	k2eAgInSc2d1	ovládaný
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Vystřízlivění	vystřízlivění	k1gNnSc1	vystřízlivění
z	z	k7c2	z
utopických	utopický	k2eAgInPc2d1	utopický
snů	sen	k1gInPc2	sen
přinesla	přinést	k5eAaPmAgFnS	přinést
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
otřásla	otřást	k5eAaPmAgFnS	otřást
avantgardní	avantgardní	k2eAgFnSc7d1	avantgardní
vírou	víra	k1gFnSc7	víra
v	v	k7c4	v
zázračnou	zázračný	k2eAgFnSc4d1	zázračná
moc	moc	k1gFnSc4	moc
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
ve	v	k7c4	v
zrod	zrod	k1gInSc4	zrod
nového	nový	k2eAgMnSc2d1	nový
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgInPc4d1	nový
vynálezy	vynález	k1gInPc4	vynález
a	a	k8xC	a
společenské	společenský	k2eAgInPc4d1	společenský
vztahy	vztah	k1gInPc4	vztah
od	od	k7c2	od
základu	základ	k1gInSc2	základ
změní	změnit	k5eAaPmIp3nS	změnit
samu	sám	k3xTgFnSc4	sám
biologickou	biologický	k2eAgFnSc4d1	biologická
podstatu	podstata	k1gFnSc4	podstata
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
promění	proměnit	k5eAaPmIp3nS	proměnit
jeho	jeho	k3xOp3gMnSc1	jeho
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
utvářené	utvářený	k2eAgFnPc1d1	utvářená
fyziologické	fyziologický	k2eAgFnPc1d1	fyziologická
a	a	k8xC	a
psychické	psychický	k2eAgInPc1d1	psychický
mechanismy	mechanismus	k1gInPc1	mechanismus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
už	už	k6eAd1	už
tehdy	tehdy	k6eAd1	tehdy
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k8xC	jako
zcela	zcela	k6eAd1	zcela
nereálná	reálný	k2eNgFnSc1d1	nereálná
<g/>
.	.	kIx.	.
</s>
<s>
Avantgardní	avantgardní	k2eAgNnSc1d1	avantgardní
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
různorodou	různorodý	k2eAgFnSc4d1	různorodá
<g/>
,	,	kIx,	,
členitou	členitý	k2eAgFnSc4d1	členitá
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
rozporuplnou	rozporuplný	k2eAgFnSc7d1	rozporuplná
periodou	perioda	k1gFnSc7	perioda
vývoje	vývoj	k1gInSc2	vývoj
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
novými	nový	k2eAgInPc7d1	nový
vědeckými	vědecký	k2eAgInPc7d1	vědecký
objevy	objev	k1gInPc7	objev
<g/>
,	,	kIx,	,
technickými	technický	k2eAgInPc7d1	technický
vynálezy	vynález	k1gInPc7	vynález
a	a	k8xC	a
materiály	materiál	k1gInPc7	materiál
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
euforickou	euforický	k2eAgFnSc7d1	euforická
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
životním	životní	k2eAgInSc7d1	životní
rytmem	rytmus	k1gInSc7	rytmus
a	a	k8xC	a
novými	nový	k2eAgFnPc7d1	nová
formami	forma	k1gFnPc7	forma
velkoměstského	velkoměstský	k2eAgInSc2d1	velkoměstský
života	život	k1gInSc2	život
<g/>
:	:	kIx,	:
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
jazzem	jazz	k1gInSc7	jazz
<g/>
,	,	kIx,	,
sportem	sport	k1gInSc7	sport
<g/>
,	,	kIx,	,
moderními	moderní	k2eAgInPc7d1	moderní
dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
automobily	automobil	k1gInPc7	automobil
<g/>
,	,	kIx,	,
letadly	letadlo	k1gNnPc7	letadlo
<g/>
,	,	kIx,	,
transatlantiky	transatlantik	k1gInPc7	transatlantik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
exotikou	exotika	k1gFnSc7	exotika
dalekých	daleký	k2eAgInPc2d1	daleký
krajů	kraj	k1gInPc2	kraj
a	a	k8xC	a
zámořských	zámořský	k2eAgFnPc2d1	zámořská
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
většinu	většina	k1gFnSc4	většina
těchto	tento	k3xDgInPc2	tento
podnětů	podnět	k1gInPc2	podnět
přijímalo	přijímat	k5eAaImAgNnS	přijímat
v	v	k7c6	v
radikální	radikální	k2eAgFnSc6d1	radikální
utopické	utopický	k2eAgFnSc6d1	utopická
perspektivě	perspektiva	k1gFnSc6	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Avantgarda	avantgarda	k1gFnSc1	avantgarda
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
francouzského	francouzský	k2eAgInSc2d1	francouzský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
avantgarde	avantgard	k1gMnSc5	avantgard
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
avant	avant	k1gInSc1	avant
=	=	kIx~	=
před	před	k7c7	před
<g/>
,	,	kIx,	,
la	la	k1gNnSc4	la
garde	garde	k1gNnSc1	garde
=	=	kIx~	=
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
,	,	kIx,	,
stráž	stráž	k1gFnSc1	stráž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
znamenající	znamenající	k2eAgInSc1d1	znamenající
vojenský	vojenský	k2eAgInSc1d1	vojenský
předvoj	předvoj	k1gInSc1	předvoj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
terminologii	terminologie	k1gFnSc6	terminologie
umělecké	umělecký	k2eAgFnSc6d1	umělecká
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
představuje	představovat	k5eAaImIp3nS	představovat
umělce	umělec	k1gMnSc4	umělec
či	či	k8xC	či
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
dobu	doba	k1gFnSc4	doba
průkopnická	průkopnický	k2eAgFnSc1d1	průkopnická
či	či	k8xC	či
objevná	objevný	k2eAgFnSc1d1	objevná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
uměleckých	umělecký	k2eAgNnPc6d1	umělecké
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ohromné	ohromný	k2eAgNnSc1d1	ohromné
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgInPc2d1	různý
avantgardních	avantgardní	k2eAgInPc2d1	avantgardní
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
proudů	proud	k1gInPc2	proud
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
oblast	oblast	k1gFnSc4	oblast
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
se	se	k3xPyFc4	se
odrazily	odrazit	k5eAaPmAgInP	odrazit
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Abstraktivismus	abstraktivismus	k1gInSc4	abstraktivismus
-	-	kIx~	-
nefigurativní	figurativní	k2eNgNnSc4d1	nefigurativní
<g/>
,	,	kIx,	,
abstraktní	abstraktní	k2eAgNnSc4d1	abstraktní
malířství	malířství	k1gNnSc4	malířství
a	a	k8xC	a
sochařství	sochařství	k1gNnSc4	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
počátky	počátek	k1gInPc1	počátek
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Vasilij	Vasilij	k1gMnSc1	Vasilij
Kandinskij	Kandinskij	k1gMnSc1	Kandinskij
namaloval	namalovat	k5eAaPmAgMnS	namalovat
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
abstraktní	abstraktní	k2eAgInSc4d1	abstraktní
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Vyhraněnou	vyhraněný	k2eAgFnSc7d1	vyhraněná
podobou	podoba	k1gFnSc7	podoba
se	se	k3xPyFc4	se
abstraktivismus	abstraktivismus	k1gInSc1	abstraktivismus
projevil	projevit	k5eAaPmAgInS	projevit
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
skrze	skrze	k?	skrze
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
konstruktivismus	konstruktivismus	k1gInSc1	konstruktivismus
<g/>
.	.	kIx.	.
</s>
<s>
Dadaismus	dadaismus	k1gInSc1	dadaismus
-	-	kIx~	-
zrodil	zrodit	k5eAaPmAgInS	zrodit
se	se	k3xPyFc4	se
uprostřed	uprostřed	k7c2	uprostřed
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
jako	jako	k9	jako
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
nesmyslnému	smyslný	k2eNgNnSc3d1	nesmyslné
vraždění	vraždění	k1gNnSc3	vraždění
na	na	k7c6	na
válečných	válečný	k2eAgFnPc6d1	válečná
frontách	fronta	k1gFnPc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
principem	princip	k1gInSc7	princip
dadaismu	dadaismus	k1gInSc2	dadaismus
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc4	využití
náhody	náhoda	k1gFnSc2	náhoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přináší	přinášet	k5eAaImIp3nS	přinášet
nesmyslné	smyslný	k2eNgFnPc4d1	nesmyslná
souvislosti	souvislost	k1gFnPc4	souvislost
<g/>
,	,	kIx,	,
vymykající	vymykající	k2eAgFnPc4d1	vymykající
se	se	k3xPyFc4	se
racionálnímu	racionální	k2eAgNnSc3d1	racionální
chápání	chápání	k1gNnSc3	chápání
<g/>
.	.	kIx.	.
</s>
<s>
Dadaisté	dadaista	k1gMnPc1	dadaista
objevili	objevit	k5eAaPmAgMnP	objevit
nové	nový	k2eAgFnPc4d1	nová
tvůrčí	tvůrčí	k2eAgFnPc4d1	tvůrčí
metody	metoda	k1gFnPc4	metoda
(	(	kIx(	(
<g/>
koláž	koláž	k1gFnSc4	koláž
<g/>
,	,	kIx,	,
ready-made	readyást	k5eAaPmIp3nS	ready-mást
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Expresionismus	expresionismus	k1gInSc1	expresionismus
-	-	kIx~	-
směr	směr	k1gInSc1	směr
projevující	projevující	k2eAgInSc1d1	projevující
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
dramatické	dramatický	k2eAgFnSc3d1	dramatická
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Odrážel	odrážet	k5eAaImAgInS	odrážet
společenskou	společenský	k2eAgFnSc4d1	společenská
dobovou	dobový	k2eAgFnSc4d1	dobová
krizi	krize	k1gFnSc4	krize
plnou	plný	k2eAgFnSc4d1	plná
sociálních	sociální	k2eAgMnPc2d1	sociální
rozporů	rozpor	k1gInPc2	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
používanými	používaný	k2eAgFnPc7d1	používaná
technikami	technika	k1gFnPc7	technika
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
technika	technik	k1gMnSc2	technik
dřevořezu	dřevořez	k1gInSc2	dřevořez
a	a	k8xC	a
akvarel	akvarel	k1gInSc4	akvarel
<g/>
.	.	kIx.	.
</s>
<s>
Expresionismus	expresionismus	k1gInSc1	expresionismus
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
projevil	projevit	k5eAaPmAgInS	projevit
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
skupin	skupina	k1gFnPc2	skupina
Die	Die	k1gMnSc2	Die
Brűcke	Brűck	k1gMnSc2	Brűck
a	a	k8xC	a
Der	drát	k5eAaImRp2nS	drát
blaue	blaue	k1gNnSc4	blaue
Reiter	Reiter	k1gInSc1	Reiter
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
skupinu	skupina	k1gFnSc4	skupina
tvořili	tvořit	k5eAaImAgMnP	tvořit
výtvarníci	výtvarník	k1gMnPc1	výtvarník
Vasilij	Vasilij	k1gMnSc1	Vasilij
Kandinskij	Kandinskij	k1gMnSc1	Kandinskij
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Klee	Kle	k1gFnSc2	Kle
<g/>
.	.	kIx.	.
</s>
<s>
Působili	působit	k5eAaImAgMnP	působit
na	na	k7c6	na
Bauhausu	Bauhaus	k1gInSc6	Bauhaus
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
ovlivňovali	ovlivňovat	k5eAaImAgMnP	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
význam	význam	k1gInSc4	význam
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
geometrické	geometrický	k2eAgFnPc4d1	geometrická
formy	forma	k1gFnPc4	forma
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
souvislosti	souvislost	k1gFnPc4	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
představitele	představitel	k1gMnSc4	představitel
expresionismu	expresionismus	k1gInSc2	expresionismus
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
norský	norský	k2eAgMnSc1d1	norský
malíř	malíř	k1gMnSc1	malíř
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
<g/>
.	.	kIx.	.
</s>
<s>
Futurismus	futurismus	k1gInSc1	futurismus
-	-	kIx~	-
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
stojící	stojící	k2eAgFnSc4d1	stojící
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Projevoval	projevovat	k5eAaImAgMnS	projevovat
obdiv	obdiv	k1gInSc4	obdiv
k	k	k7c3	k
technické	technický	k2eAgFnSc3d1	technická
civilizaci	civilizace	k1gFnSc3	civilizace
a	a	k8xC	a
radikální	radikální	k2eAgInSc4d1	radikální
odpor	odpor	k1gInSc4	odpor
ke	k	k7c3	k
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
předindustriální	předindustriální	k2eAgFnSc2d1	předindustriální
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
k	k	k7c3	k
mocenskému	mocenský	k2eAgNnSc3d1	mocenské
postavení	postavení	k1gNnSc3	postavení
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Ústředními	ústřední	k2eAgNnPc7d1	ústřední
tématy	téma	k1gNnPc7	téma
futurismu	futurismus	k1gInSc2	futurismus
byl	být	k5eAaImAgInS	být
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
destrukce	destrukce	k1gFnSc1	destrukce
<g/>
,	,	kIx,	,
futuristické	futuristický	k2eAgNnSc1d1	futuristické
okouzlení	okouzlení	k1gNnSc1	okouzlení
technikou	technika	k1gFnSc7	technika
přejal	přejmout	k5eAaPmAgInS	přejmout
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaný	uznávaný	k2eAgInSc4d1	uznávaný
směr	směr	k1gInSc4	směr
avantgardy	avantgarda	k1gFnSc2	avantgarda
poetismus	poetismus	k1gInSc1	poetismus
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
však	však	k9	však
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
odnož	odnož	k1gFnSc4	odnož
kubofuturismu	kubofuturismus	k1gInSc2	kubofuturismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kubismus	kubismus	k1gInSc1	kubismus
-	-	kIx~	-
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
cube	cube	k1gNnSc1	cube
-	-	kIx~	-
krychle	krychle	k1gFnPc1	krychle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malířský	malířský	k2eAgInSc1d1	malířský
a	a	k8xC	a
sochařský	sochařský	k2eAgInSc1d1	sochařský
směr	směr	k1gInSc1	směr
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
projevil	projevit	k5eAaPmAgInS	projevit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
-	-	kIx~	-
některé	některý	k3yIgFnPc4	některý
básně	báseň	k1gFnPc4	báseň
Jeana	Jean	k1gMnSc2	Jean
Cocteaua	Cocteauus	k1gMnSc2	Cocteauus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
odlišil	odlišit	k5eAaPmAgInS	odlišit
od	od	k7c2	od
dosavadního	dosavadní	k2eAgNnSc2d1	dosavadní
pojetí	pojetí	k1gNnSc2	pojetí
obrazové	obrazový	k2eAgFnSc2d1	obrazová
reality	realita	k1gFnSc2	realita
<g/>
.	.	kIx.	.
</s>
<s>
Podnětem	podnět	k1gInSc7	podnět
vzniku	vznik	k1gInSc2	vznik
bylo	být	k5eAaImAgNnS	být
poznání	poznání	k1gNnSc1	poznání
oceánského	oceánský	k2eAgNnSc2d1	oceánské
a	a	k8xC	a
negerského	negerský	k2eAgNnSc2d1	negerský
sochařství	sochařství	k1gNnSc2	sochařství
<g/>
.	.	kIx.	.
</s>
<s>
Představitel	představitel	k1gMnSc1	představitel
kubismu	kubismus	k1gInSc2	kubismus
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Maluji	malovat	k5eAaImIp1nS	malovat
předměty	předmět	k1gInPc4	předmět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	on	k3xPp3gMnPc4	on
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	on	k3xPp3gNnSc4	on
vidím	vidět	k5eAaImIp1nS	vidět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kubismus	kubismus	k1gInSc1	kubismus
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
také	také	k9	také
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
architektuře	architektura	k1gFnSc6	architektura
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
nemá	mít	k5eNaImIp3nS	mít
obdobu	obdoba	k1gFnSc4	obdoba
nikde	nikde	k6eAd1	nikde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kubofuturismus	Kubofuturismus	k1gInSc1	Kubofuturismus
-	-	kIx~	-
jako	jako	k8xS	jako
kubofuturismus	kubofuturismus	k1gInSc1	kubofuturismus
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
spojení	spojení	k1gNnSc3	spojení
některých	některý	k3yIgInPc2	některý
kubistických	kubistický	k2eAgInPc2d1	kubistický
prvků	prvek	k1gInPc2	prvek
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
futurismu	futurismus	k1gInSc2	futurismus
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
za	za	k7c4	za
představitele	představitel	k1gMnSc4	představitel
kubofuturismu	kubofuturismus	k1gInSc2	kubofuturismus
považuje	považovat	k5eAaImIp3nS	považovat
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
<g/>
.	.	kIx.	.
</s>
<s>
Funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
-	-	kIx~	-
architektonický	architektonický	k2eAgInSc4d1	architektonický
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
směr	směr	k1gInSc4	směr
evropské	evropský	k2eAgFnSc2d1	Evropská
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
základním	základní	k2eAgNnSc7d1	základní
heslem	heslo	k1gNnSc7	heslo
bylo	být	k5eAaImAgNnS	být
<g/>
:	:	kIx,	:
forma	forma	k1gFnSc1	forma
následuje	následovat	k5eAaImIp3nS	následovat
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
organizaci	organizace	k1gFnSc4	organizace
životního	životní	k2eAgNnSc2d1	životní
dění	dění	k1gNnSc2	dění
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
biologickém	biologický	k2eAgInSc6d1	biologický
<g/>
,	,	kIx,	,
sociálním	sociální	k2eAgNnSc6d1	sociální
<g/>
,	,	kIx,	,
ekonomickém	ekonomický	k2eAgNnSc6d1	ekonomické
<g/>
,	,	kIx,	,
psychickém	psychický	k2eAgNnSc6d1	psychické
a	a	k8xC	a
technickém	technický	k2eAgNnSc6d1	technické
<g/>
.	.	kIx.	.
</s>
<s>
Futurismus	futurismus	k1gInSc1	futurismus
přerostl	přerůst	k5eAaPmAgInS	přerůst
meze	mez	k1gFnPc4	mez
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
novým	nový	k2eAgInSc7d1	nový
způsobem	způsob	k1gInSc7	způsob
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Podstatu	podstata	k1gFnSc4	podstata
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
objasnil	objasnit	k5eAaPmAgMnS	objasnit
architekt	architekt	k1gMnSc1	architekt
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
a	a	k8xC	a
formuloval	formulovat	k5eAaImAgMnS	formulovat
jeho	jeho	k3xOp3gInSc4	jeho
manifest	manifest	k1gInSc4	manifest
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
poslední	poslední	k2eAgInSc4d1	poslední
společný	společný	k2eAgInSc4d1	společný
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
sloh	sloh	k1gInSc4	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Surrealismus	surrealismus	k1gInSc1	surrealismus
-	-	kIx~	-
zrodil	zrodit	k5eAaPmAgInS	zrodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
dadaistické	dadaistický	k2eAgFnSc6d1	dadaistická
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
surrealistického	surrealistický	k2eAgInSc2d1	surrealistický
hnuti	hnut	k2eAgMnPc1d1	hnut
byl	být	k5eAaImAgInS	být
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
surrealismus	surrealismus	k1gInSc4	surrealismus
definoval	definovat	k5eAaBmAgInS	definovat
jako	jako	k8xC	jako
čistý	čistý	k2eAgInSc1d1	čistý
psychický	psychický	k2eAgInSc1d1	psychický
automatismus	automatismus	k1gInSc1	automatismus
<g/>
.	.	kIx.	.
</s>
<s>
Diktát	diktát	k1gInSc1	diktát
myšlení	myšlení	k1gNnSc2	myšlení
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
rozumového	rozumový	k2eAgInSc2d1	rozumový
dozoru	dozor	k1gInSc2	dozor
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jakékoliv	jakýkoliv	k3yIgInPc4	jakýkoliv
estetické	estetický	k2eAgInPc4d1	estetický
nebo	nebo	k8xC	nebo
mravní	mravní	k2eAgInPc4d1	mravní
zřetele	zřetel	k1gInPc4	zřetel
<g/>
.	.	kIx.	.
</s>
<s>
Inspirován	inspirovat	k5eAaBmNgMnS	inspirovat
byl	být	k5eAaImAgMnS	být
především	především	k9	především
psychoanalýzou	psychoanalýza	k1gFnSc7	psychoanalýza
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Nadrealismus	nadrealismus	k1gInSc1	nadrealismus
-	-	kIx~	-
modifikace	modifikace	k1gFnSc1	modifikace
surrealismu	surrealismus	k1gInSc2	surrealismus
ve	v	k7c6	v
slovenské	slovenský	k2eAgFnSc6d1	slovenská
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nadrealismus	nadrealismus	k1gInSc1	nadrealismus
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
českým	český	k2eAgMnSc7d1	český
a	a	k8xC	a
francouzským	francouzský	k2eAgInSc7d1	francouzský
surrealismem	surrealismus	k1gInSc7	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
Zappa	Zapp	k1gMnSc2	Zapp
Captain	Captain	k2eAgInSc4d1	Captain
Beefheart	Beefheart	k1gInSc4	Beefheart
Ornette	Ornett	k1gInSc5	Ornett
Coleman	Coleman	k1gMnSc1	Coleman
John	John	k1gMnSc1	John
Cage	Cage	k1gFnPc2	Cage
Derek	Derek	k1gMnSc1	Derek
Baily	Baila	k1gFnSc2	Baila
The	The	k1gMnSc1	The
Residents	Residentsa	k1gFnPc2	Residentsa
San	San	k1gMnSc1	San
Ra	ra	k0	ra
Alexandro	Alexandra	k1gFnSc5	Alexandra
Jodorowsky	Jodorowsko	k1gNnPc7	Jodorowsko
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
Pro	pro	k7c4	pro
avantgardní	avantgardní	k2eAgFnSc4d1	avantgardní
fotografii	fotografia	k1gFnSc4	fotografia
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
pohled	pohled	k1gInSc4	pohled
z	z	k7c2	z
ptačí	ptačí	k2eAgFnSc2d1	ptačí
(	(	kIx(	(
<g/>
nadhled	nadhled	k1gInSc4	nadhled
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
žabí	žabí	k2eAgFnPc1d1	žabí
perspektivy	perspektiva	k1gFnPc1	perspektiva
,	,	kIx,	,
diagonální	diagonální	k2eAgFnPc1d1	diagonální
kompozice	kompozice	k1gFnPc1	kompozice
<g/>
,	,	kIx,	,
odvážné	odvážný	k2eAgInPc1d1	odvážný
výřezy	výřez	k1gInPc1	výřez
<g/>
,	,	kIx,	,
objevování	objevování	k1gNnSc1	objevování
krásy	krása	k1gFnSc2	krása
zdánlivě	zdánlivě	k6eAd1	zdánlivě
neestetických	estetický	k2eNgInPc2d1	neestetický
a	a	k8xC	a
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
oslava	oslava	k1gFnSc1	oslava
moderního	moderní	k2eAgInSc2d1	moderní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
moderních	moderní	k2eAgInPc2d1	moderní
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
také	také	k9	také
s	s	k7c7	s
pojmenováním	pojmenování	k1gNnSc7	pojmenování
téhož	týž	k3xTgNnSc2	týž
období	období	k1gNnSc2	období
jako	jako	k9	jako
modernismus	modernismus	k1gInSc4	modernismus
nebo	nebo	k8xC	nebo
moderní	moderní	k2eAgFnSc2d1	moderní
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
období	období	k1gNnSc6	období
1918	[number]	k4	1918
-	-	kIx~	-
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
kolektivním	kolektivní	k2eAgNnSc7d1	kolektivní
vystoupením	vystoupení	k1gNnSc7	vystoupení
fotografické	fotografický	k2eAgFnSc2d1	fotografická
avantgardy	avantgarda	k1gFnSc2	avantgarda
je	být	k5eAaImIp3nS	být
výstava	výstava	k1gFnSc1	výstava
Nová	nový	k2eAgFnSc1d1	nová
fotografie	fotografie	k1gFnSc1	fotografie
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
pozval	pozvat	k5eAaPmAgMnS	pozvat
Alexander	Alexandra	k1gFnPc2	Alexandra
Hackenschmied	Hackenschmied	k1gInSc1	Hackenschmied
Josefa	Josef	k1gMnSc2	Josef
Sudka	sudka	k1gFnSc1	sudka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
představitele	představitel	k1gMnPc4	představitel
československé	československý	k2eAgFnSc2d1	Československá
avantgardy	avantgarda	k1gFnSc2	avantgarda
jsou	být	k5eAaImIp3nP	být
umělecká	umělecký	k2eAgFnSc1d1	umělecká
skupina	skupina	k1gFnSc1	skupina
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
,	,	kIx,	,
Ra	ra	k0	ra
<g/>
,	,	kIx,	,
Fotoskupina	Fotoskupina	k1gFnSc1	Fotoskupina
pěti	pět	k4xCc3	pět
<g/>
,	,	kIx,	,
Fotolinie	Fotolinie	k1gFnPc4	Fotolinie
a	a	k8xC	a
DAV	Dav	k1gInSc4	Dav
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jednotlivé	jednotlivý	k2eAgMnPc4d1	jednotlivý
umělce	umělec	k1gMnPc4	umělec
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
František	František	k1gMnSc1	František
Drtikol	Drtikol	k1gInSc1	Drtikol
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Funke	Funk	k1gFnSc2	Funk
nebo	nebo	k8xC	nebo
Josef	Josef	k1gMnSc1	Josef
Sudek	sudka	k1gFnPc2	sudka
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rössler	Rössler	k1gMnSc1	Rössler
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
Adolf	Adolf	k1gMnSc1	Adolf
Hoffmeister	Hoffmeister	k1gMnSc1	Hoffmeister
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gFnSc2	Teig
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
Evžen	Evžen	k1gMnSc1	Evžen
Markalous	Markalous	k1gMnSc1	Markalous
-	-	kIx~	-
člen	člen	k1gMnSc1	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
Jindřich	Jindřich	k1gMnSc1	Jindřich
Štyrský	Štyrský	k2eAgMnSc1d1	Štyrský
-	-	kIx~	-
člen	člen	k1gInSc1	člen
Devětsilu	Devětsil	k1gInSc2	Devětsil
<g/>
,	,	kIx,	,
surrealista	surrealista	k1gMnSc1	surrealista
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
erotické	erotický	k2eAgFnSc2d1	erotická
koláže	koláž	k1gFnSc2	koláž
Emilie	Emilie	k1gFnSc2	Emilie
přichází	přicházet	k5eAaImIp3nS	přicházet
ke	k	k7c3	k
mně	já	k3xPp1nSc3	já
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
<g/>
,	,	kIx,	,
obálky	obálka	k1gFnSc2	obálka
sešitového	sešitový	k2eAgNnSc2d1	sešitové
vydání	vydání	k1gNnSc2	vydání
Fantomase	Fantomas	k1gInSc6	Fantomas
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
Irena	Irena	k1gFnSc1	Irena
Blühová	Blühová	k1gFnSc1	Blühová
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Foltýn	Foltýn	k1gMnSc1	Foltýn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
německou	německý	k2eAgFnSc4d1	německá
školu	škola	k1gFnSc4	škola
Bauhaus	Bauhaus	k1gMnSc1	Bauhaus
Karel	Karel	k1gMnSc1	Karel
Kašpařík	Kašpařík	k1gMnSc1	Kašpařík
-	-	kIx~	-
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
surrealistický	surrealistický	k2eAgMnSc1d1	surrealistický
fotograf	fotograf	k1gMnSc1	fotograf
známý	známý	k1gMnSc1	známý
svými	svůj	k3xOyFgInPc7	svůj
konstruktivistickými	konstruktivistický	k2eAgInPc7d1	konstruktivistický
portréty	portrét	k1gInPc7	portrét
Miloš	Miloš	k1gMnSc1	Miloš
Dohnány	dohnán	k2eAgFnPc1d1	dohnána
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
Funkeho	Funke	k1gMnSc2	Funke
Josef	Josef	k1gMnSc1	Josef
Ehm	ehm	k0	ehm
-	-	kIx~	-
pohled	pohled	k1gInSc4	pohled
z	z	k7c2	z
ptačí	ptačí	k2eAgFnSc2d1	ptačí
nebo	nebo	k8xC	nebo
žabí	žabí	k2eAgFnSc2d1	žabí
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
,	,	kIx,	,
diagonální	diagonální	k2eAgFnSc2d1	diagonální
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
,	,	kIx,	,
odvážné	odvážný	k2eAgInPc1d1	odvážný
výřezy	výřez	k1gInPc1	výřez
<g/>
,	,	kIx,	,
objevování	objevování	k1gNnSc1	objevování
krásy	krása	k1gFnSc2	krása
zdánlivě	zdánlivě	k6eAd1	zdánlivě
neestetických	estetický	k2eNgInPc2d1	neestetický
a	a	k8xC	a
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
oslava	oslava	k1gFnSc1	oslava
moderního	moderní	k2eAgInSc2d1	moderní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
moderních	moderní	k2eAgInPc2d1	moderní
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Vobecký	Vobecký	k2eAgMnSc1d1	Vobecký
surrealista	surrealista	k1gMnSc1	surrealista
Josef	Josef	k1gMnSc1	Josef
Bartuška	Bartuška	k1gMnSc1	Bartuška
surrealista	surrealista	k1gMnSc1	surrealista
Václav	Václav	k1gMnSc1	Václav
Zykmund	Zykmund	k1gMnSc1	Zykmund
surrealista	surrealista	k1gMnSc1	surrealista
Ada	Ada	kA	Ada
Novák	Novák	k1gMnSc1	Novák
surrealista	surrealista	k1gMnSc1	surrealista
Bohumil	Bohumil	k1gMnSc1	Bohumil
Němec	Němec	k1gMnSc1	Němec
surrealista	surrealista	k1gMnSc1	surrealista
František	František	k1gMnSc1	František
Povolný	Povolný	k1gMnSc1	Povolný
surrealista	surrealista	k1gMnSc1	surrealista
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hák	Hák	k1gMnSc1	Hák
surrealista	surrealista	k1gMnSc1	surrealista
František	František	k1gMnSc1	František
Čermák	Čermák	k1gMnSc1	Čermák
(	(	kIx(	(
<g/>
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
)	)	kIx)	)
-	-	kIx~	-
uskutečňoval	uskutečňovat	k5eAaImAgInS	uskutečňovat
principy	princip	k1gInPc4	princip
avantgardního	avantgardní	k2eAgInSc2d1	avantgardní
konstruktivismu	konstruktivismus	k1gInSc2	konstruktivismus
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
věcnosti	věcnost	k1gFnSc2	věcnost
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hnízdo	hnízdo	k1gNnSc4	hnízdo
-	-	kIx~	-
uskutečňoval	uskutečňovat	k5eAaImAgInS	uskutečňovat
principy	princip	k1gInPc4	princip
avantgardního	avantgardní	k2eAgInSc2d1	avantgardní
konstruktivismu	konstruktivismus	k1gInSc2	konstruktivismus
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
věcnosti	věcnost	k1gFnSc2	věcnost
Jiří	Jiří	k1gMnSc1	Jiří
Kroha	Kroha	k1gMnSc1	Kroha
-	-	kIx~	-
koláže	koláž	k1gFnPc1	koláž
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
politickou	politický	k2eAgFnSc4d1	politická
propagaci	propagace	k1gFnSc4	propagace
Karol	Karol	k1gInSc1	Karol
Plicka	Plicko	k1gNnSc2	Plicko
-	-	kIx~	-
idealizované	idealizovaný	k2eAgFnSc2d1	idealizovaná
etnografické	etnografický	k2eAgFnSc2d1	etnografická
fotografie	fotografia	k1gFnSc2	fotografia
Eugen	Eugen	k1gInSc1	Eugen
Wiškovský	Wiškovský	k2eAgInSc1d1	Wiškovský
-	-	kIx~	-
obrazová	obrazový	k2eAgFnSc1d1	obrazová
metafora	metafora	k1gFnSc1	metafora
Ze	z	k7c2	z
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
fotografů	fotograf	k1gMnPc2	fotograf
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
maďarští	maďarský	k2eAgMnPc1d1	maďarský
emigranti	emigrant	k1gMnPc1	emigrant
André	André	k1gMnSc1	André
Kertész	Kertész	k1gMnSc1	Kertész
<g/>
,	,	kIx,	,
László	László	k1gMnSc1	László
Moholy-Nagy	Moholy-Naga	k1gFnSc2	Moholy-Naga
a	a	k8xC	a
Brassaï	Brassaï	k1gFnSc2	Brassaï
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
Ernö	Ernö	k1gFnSc1	Ernö
Berda	berdo	k1gNnSc2	berdo
Německo	Německo	k1gNnSc1	Německo
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Heartfield	Heartfield	k1gMnSc1	Heartfield
proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
dadaistickými	dadaistický	k2eAgFnPc7d1	dadaistická
kolážemi	koláž	k1gFnPc7	koláž
proti	proti	k7c3	proti
nacistickému	nacistický	k2eAgInSc3d1	nacistický
režimu	režim	k1gInSc3	režim
Trude	trud	k1gInSc5	trud
Fleischmann	Fleischmanna	k1gFnPc2	Fleischmanna
Helmut	Helmut	k1gMnSc1	Helmut
Newton	Newton	k1gMnSc1	Newton
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruská	ruský	k2eAgFnSc1d1	ruská
avantgarda	avantgarda	k1gFnSc1	avantgarda
<g/>
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
<g/>
:	:	kIx,	:
Alexandr	Alexandr	k1gMnSc1	Alexandr
Rodčenko	Rodčenka	k1gFnSc5	Rodčenka
-	-	kIx~	-
proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
kolážemi	koláž	k1gFnPc7	koláž
Robert	Robert	k1gMnSc1	Robert
Maplesoap	Maplesoap	k1gMnSc1	Maplesoap
Man	mana	k1gFnPc2	mana
Ray	Ray	k1gFnSc2	Ray
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
:	:	kIx,	:
Trčka	Trčka	k1gMnSc1	Trčka
-	-	kIx~	-
expresivní	expresivní	k2eAgInPc1d1	expresivní
portréty	portrét	k1gInPc1	portrét
Koppitz	Koppitza	k1gFnPc2	Koppitza
-	-	kIx~	-
moderní	moderní	k2eAgInSc1d1	moderní
tanec	tanec	k1gInSc1	tanec
Max	Max	k1gMnSc1	Max
Ernst	Ernst	k1gMnSc1	Ernst
-	-	kIx~	-
dadaistické	dadaistický	k2eAgFnSc2d1	dadaistická
koláže	koláž	k1gFnSc2	koláž
Bill	Bill	k1gMnSc1	Bill
Brant	Brant	k?	Brant
Z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Mieczyslav	Mieczyslav	k1gMnSc1	Mieczyslav
Szczuka	Szczuk	k1gMnSc2	Szczuk
-	-	kIx~	-
fotomontáže	fotomontáž	k1gFnPc1	fotomontáž
Teresa	Teresa	k1gFnSc1	Teresa
Zarnowerová	Zarnowerová	k1gFnSc1	Zarnowerová
Janusz	Janusz	k1gInSc1	Janusz
M.	M.	kA	M.
Brzeský	Brzeský	k2eAgInSc1d1	Brzeský
-	-	kIx~	-
koláže	koláž	k1gFnPc1	koláž
Herbert	Herbert	k1gMnSc1	Herbert
Bayer	Bayer	k1gMnSc1	Bayer
Wladyslaw	Wladyslaw	k1gMnSc1	Wladyslaw
Bednarczuk	Bednarczuk	k1gMnSc1	Bednarczuk
-	-	kIx~	-
uskutečňoval	uskutečňovat	k5eAaImAgInS	uskutečňovat
principy	princip	k1gInPc4	princip
avantgardního	avantgardní	k2eAgInSc2d1	avantgardní
konstruktivismu	konstruktivismus	k1gInSc2	konstruktivismus
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
věcnosti	věcnost	k1gFnSc2	věcnost
Hannah	Hannah	k1gMnSc1	Hannah
Höch	Höch	k1gMnSc1	Höch
-	-	kIx~	-
dadaistické	dadaistický	k2eAgFnSc2d1	dadaistická
koláže	koláž	k1gFnSc2	koláž
Paul	Paul	k1gMnSc1	Paul
Citroen	citroen	k1gInSc1	citroen
-	-	kIx~	-
absolvent	absolvent	k1gMnSc1	absolvent
Bauhausu	Bauhaus	k1gInSc2	Bauhaus
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
koláže	koláž	k1gFnSc2	koláž
Metropolis	Metropolis	k1gFnSc1	Metropolis
Sergej	Sergej	k1gMnSc1	Sergej
Protopopov	Protopopov	k1gInSc1	Protopopov
-	-	kIx~	-
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kata	kata	k9	kata
Kálmán	Kálmán	k2eAgMnSc1d1	Kálmán
-	-	kIx~	-
sociální	sociální	k2eAgFnPc1d1	sociální
fotografie	fotografia	k1gFnPc1	fotografia
oslavující	oslavující	k2eAgFnSc4d1	oslavující
práci	práce	k1gFnSc4	práce
Helmar	Helmar	k1gMnSc1	Helmar
Lerský	Lerský	k1gMnSc1	Lerský
-	-	kIx~	-
uskutečňoval	uskutečňovat	k5eAaImAgInS	uskutečňovat
principy	princip	k1gInPc4	princip
avantgardního	avantgardní	k2eAgInSc2d1	avantgardní
konstruktivismu	konstruktivismus	k1gInSc2	konstruktivismus
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
věcnosti	věcnost	k1gFnSc2	věcnost
Károly	Károla	k1gFnSc2	Károla
Escher	Eschra	k1gFnPc2	Eschra
-	-	kIx~	-
uskutečňoval	uskutečňovat	k5eAaImAgInS	uskutečňovat
principy	princip	k1gInPc4	princip
avantgardního	avantgardní	k2eAgInSc2d1	avantgardní
konstruktivismu	konstruktivismus	k1gInSc2	konstruktivismus
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
věcnosti	věcnost	k1gFnSc2	věcnost
Bauhaus	Bauhaus	k1gMnSc1	Bauhaus
Škola	škola	k1gFnSc1	škola
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
Bratislava	Bratislava	k1gFnSc1	Bratislava
Státní	státní	k2eAgFnSc1d1	státní
grafická	grafický	k2eAgFnSc1d1	grafická
škola	škola	k1gFnSc1	škola
Praha	Praha	k1gFnSc1	Praha
</s>
