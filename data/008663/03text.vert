<p>
<s>
Barklya	Barkly	k2eAgFnSc1d1	Barkly
syringifolia	syringifolia	k1gFnSc1	syringifolia
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
bobovité	bobovitý	k2eAgFnPc1d1	bobovitá
a	a	k8xC	a
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
rodu	rod	k1gInSc2	rod
Barklya	Barklyum	k1gNnSc2	Barklyum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stálezelený	stálezelený	k2eAgInSc1d1	stálezelený
strom	strom	k1gInSc1	strom
se	s	k7c7	s
sytě	sytě	k6eAd1	sytě
zelenými	zelený	k2eAgInPc7d1	zelený
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
listy	list	k1gInPc7	list
a	a	k8xC	a
žlutooranžovými	žlutooranžový	k2eAgInPc7d1	žlutooranžový
květy	květ	k1gInPc7	květ
v	v	k7c6	v
bohatých	bohatý	k2eAgNnPc6d1	bohaté
květenstvích	květenství	k1gNnPc6	květenství
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
východní	východní	k2eAgFnSc2d1	východní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
pěstován	pěstovat	k5eAaImNgInS	pěstovat
jako	jako	k8xC	jako
okrasná	okrasný	k2eAgFnSc1d1	okrasná
dřevina	dřevina	k1gFnSc1	dřevina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Barklya	Barkly	k2eAgFnSc1d1	Barkly
syringifolia	syringifolia	k1gFnSc1	syringifolia
je	být	k5eAaImIp3nS	být
stálezelený	stálezelený	k2eAgInSc4d1	stálezelený
strom	strom	k1gInSc4	strom
dorůstající	dorůstající	k2eAgFnSc2d1	dorůstající
výšek	výška	k1gFnPc2	výška
7	[number]	k4	7
až	až	k9	až
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
(	(	kIx(	(
<g/>
jednolisté	jednolistý	k2eAgFnPc1d1	jednolistá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srdčité	srdčitý	k2eAgFnPc1d1	srdčitá
<g/>
,	,	kIx,	,
dlouze	dlouho	k6eAd1	dlouho
řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
řapíku	řapík	k1gInSc2	řapík
je	být	k5eAaImIp3nS	být
pulvinus	pulvinus	k1gInSc1	pulvinus
<g/>
.	.	kIx.	.
</s>
<s>
Palisty	palist	k1gInPc1	palist
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgInPc1d1	drobný
<g/>
,	,	kIx,	,
oválné	oválný	k2eAgInPc1d1	oválný
<g/>
,	,	kIx,	,
opadavé	opadavý	k2eAgInPc1d1	opadavý
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
zatuchle	zatuchle	k6eAd1	zatuchle
voní	vonět	k5eAaImIp3nP	vonět
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgFnPc1d1	drobná
<g/>
,	,	kIx,	,
žlutooranžové	žlutooranžový	k2eAgFnPc1d1	žlutooranžová
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
stopkaté	stopkatý	k2eAgFnPc1d1	stopkatá
<g/>
,	,	kIx,	,
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
v	v	k7c6	v
hustých	hustý	k2eAgInPc6d1	hustý
vrcholových	vrcholový	k2eAgInPc6d1	vrcholový
hroznech	hrozen	k1gInPc6	hrozen
skládajících	skládající	k2eAgInPc6d1	skládající
až	až	k8xS	až
20	[number]	k4	20
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
laty	lata	k1gFnSc2	lata
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
úžlabní	úžlabní	k2eAgFnSc1d1	úžlabní
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
zvonkovitý	zvonkovitý	k2eAgInSc1d1	zvonkovitý
<g/>
,	,	kIx,	,
zakončený	zakončený	k2eAgInSc1d1	zakončený
krátkými	krátký	k2eAgInPc7d1	krátký
tupými	tupý	k2eAgInPc7d1	tupý
laloky	lalok	k1gInPc7	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Korunní	korunní	k2eAgInPc1d1	korunní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
volné	volný	k2eAgFnPc1d1	volná
<g/>
,	,	kIx,	,
podobného	podobný	k2eAgInSc2d1	podobný
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
dlouze	dlouho	k6eAd1	dlouho
nehetnaté	hetnatý	k2eNgNnSc1d1	hetnatý
<g/>
.	.	kIx.	.
</s>
<s>
Pavéza	pavéza	k1gFnSc1	pavéza
je	být	k5eAaImIp3nS	být
širší	široký	k2eAgInPc4d2	širší
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc4d1	ostatní
korunní	korunní	k2eAgInPc4d1	korunní
lístky	lístek	k1gInPc4	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
volné	volný	k2eAgInPc1d1	volný
a	a	k8xC	a
delší	dlouhý	k2eAgInPc1d2	delší
než	než	k8xS	než
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
stopkatý	stopkatý	k2eAgMnSc1d1	stopkatý
<g/>
,	,	kIx,	,
s	s	k7c7	s
několika	několik	k4yIc7	několik
vajíčky	vajíčko	k1gNnPc7	vajíčko
a	a	k8xC	a
krátkou	krátký	k2eAgFnSc7d1	krátká
čnělkou	čnělka	k1gFnSc7	čnělka
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
drobnou	drobný	k2eAgFnSc4d1	drobná
vrcholovou	vrcholový	k2eAgFnSc4d1	vrcholová
bliznu	blizna	k1gFnSc4	blizna
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
podlouhle	podlouhle	k6eAd1	podlouhle
kopinaté	kopinatý	k2eAgFnPc1d1	kopinatá
<g/>
,	,	kIx,	,
stopkaté	stopkatý	k2eAgFnPc1d1	stopkatá
<g/>
,	,	kIx,	,
ploché	plochý	k2eAgFnPc1d1	plochá
lusky	luska	k1gFnPc1	luska
<g/>
,	,	kIx,	,
nepukavé	pukavý	k2eNgFnPc1d1	nepukavá
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
slabě	slabě	k6eAd1	slabě
pukající	pukající	k2eAgFnSc1d1	pukající
tenkostěnnými	tenkostěnný	k2eAgFnPc7d1	tenkostěnná
chlopněmi	chlopeň	k1gFnPc7	chlopeň
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
plochá	plochý	k2eAgNnPc4d1	ploché
semena	semeno	k1gNnPc4	semeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
a	a	k8xC	a
subtropických	subtropický	k2eAgFnPc6d1	subtropická
oblastech	oblast	k1gFnPc6	oblast
při	při	k7c6	při
východním	východní	k2eAgNnSc6d1	východní
a	a	k8xC	a
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Queenslandu	Queensland	k1gInSc2	Queensland
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
suššího	suchý	k2eAgInSc2d2	sušší
deštného	deštný	k2eAgInSc2d1	deštný
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
keřovité	keřovitý	k2eAgFnSc2d1	keřovitá
vegetace	vegetace	k1gFnSc2	vegetace
a	a	k8xC	a
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
na	na	k7c6	na
pískovcových	pískovcový	k2eAgInPc6d1	pískovcový
<g/>
,	,	kIx,	,
grandioritových	grandioritový	k2eAgInPc6d1	grandioritový
nebo	nebo	k8xC	nebo
čedičových	čedičový	k2eAgInPc6d1	čedičový
podkladech	podklad	k1gInPc6	podklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
Barklya	Barkly	k1gInSc2	Barkly
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
taxonomii	taxonomie	k1gFnSc6	taxonomie
bobovitých	bobovitý	k2eAgFnPc2d1	bobovitá
řazen	řadit	k5eAaImNgMnS	řadit
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
Cercidoideae	Cercidoidea	k1gFnSc2	Cercidoidea
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
příbuznými	příbuzný	k2eAgFnPc7d1	příbuzná
skupinami	skupina	k1gFnPc7	skupina
jsou	být	k5eAaImIp3nP	být
dle	dle	k7c2	dle
výsledků	výsledek	k1gInPc2	výsledek
molekulárních	molekulární	k2eAgFnPc2d1	molekulární
analýz	analýza	k1gFnPc2	analýza
rod	rod	k1gInSc1	rod
Schnella	Schnella	k1gMnSc1	Schnella
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
rody	rod	k1gInPc1	rod
oddělené	oddělený	k2eAgInPc1d1	oddělený
od	od	k7c2	od
rodu	rod	k1gInSc2	rod
Bauhinia	Bauhinium	k1gNnSc2	Bauhinium
(	(	kIx(	(
<g/>
Gigasiphon	Gigasiphon	k1gNnSc1	Gigasiphon
<g/>
,	,	kIx,	,
Lysiphyllum	Lysiphyllum	k1gNnSc1	Lysiphyllum
<g/>
,	,	kIx,	,
Phanera	Phaner	k1gMnSc2	Phaner
a	a	k8xC	a
Tylosema	Tylosem	k1gMnSc2	Tylosem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Barklya	Barkly	k2eAgFnSc1d1	Barkly
syringifolia	syringifolia	k1gFnSc1	syringifolia
je	být	k5eAaImIp3nS	být
krásně	krásně	k6eAd1	krásně
kvetoucí	kvetoucí	k2eAgInSc1d1	kvetoucí
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
pěstovaný	pěstovaný	k2eAgInSc1d1	pěstovaný
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
i	i	k9	i
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
či	či	k8xC	či
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
okrasná	okrasný	k2eAgFnSc1d1	okrasná
dřevina	dřevina	k1gFnSc1	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
má	mít	k5eAaImIp3nS	mít
spíše	spíše	k9	spíše
keřovitý	keřovitý	k2eAgInSc1d1	keřovitý
vzrůst	vzrůst	k1gInSc1	vzrůst
a	a	k8xC	a
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
do	do	k7c2	do
výšek	výška	k1gFnPc2	výška
okolo	okolo	k7c2	okolo
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Barklya	Barklyus	k1gMnSc2	Barklyus
syringifolia	syringifolius	k1gMnSc2	syringifolius
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
