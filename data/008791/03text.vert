<p>
<s>
Loď	loď	k1gFnSc1	loď
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Ship	Ship	k1gMnSc1	Ship
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
USS	USS	kA	USS
nebo	nebo	k8xC	nebo
též	též	k9	též
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
<g/>
S.	S.	kA	S.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prefix	prefix	k1gInSc4	prefix
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
plavidlo	plavidlo	k1gNnSc1	plavidlo
náležící	náležící	k2eAgNnSc1d1	náležící
Námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
se	se	k3xPyFc4	se
před	před	k7c4	před
samotné	samotný	k2eAgNnSc4d1	samotné
jméno	jméno	k1gNnSc4	jméno
vojenského	vojenský	k2eAgNnSc2d1	vojenské
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
např.	např.	kA	např.
USS	USS	kA	USS
Enterprise	Enterprise	k1gFnSc1	Enterprise
(	(	kIx(	(
<g/>
CV-	CV-	k1gFnSc1	CV-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
uvádí	uvádět	k5eAaImIp3nS	uvádět
číslo	číslo	k1gNnSc1	číslo
trupu	trup	k1gInSc2	trup
lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
a	a	k8xC	a
lze	lze	k6eAd1	lze
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
rozlišit	rozlišit	k5eAaPmF	rozlišit
dvě	dva	k4xCgNnPc4	dva
různá	různý	k2eAgNnPc4d1	různé
plavidla	plavidlo	k1gNnPc4	plavidlo
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
USS	USS	kA	USS
Hornet	Hornet	k1gMnSc1	Hornet
(	(	kIx(	(
<g/>
CV-	CV-	k1gFnSc1	CV-
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
a	a	k8xC	a
USS	USS	kA	USS
Hornet	Hornet	k1gInSc1	Hornet
(	(	kIx(	(
<g/>
CV-	CV-	k1gFnSc1	CV-
<g/>
12	[number]	k4	12
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Prefix	prefix	k1gInSc1	prefix
USS	USS	kA	USS
loď	loď	k1gFnSc1	loď
nese	nést	k5eAaImIp3nS	nést
pouze	pouze	k6eAd1	pouze
během	během	k7c2	během
aktivní	aktivní	k2eAgFnSc2d1	aktivní
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
prefixu	prefix	k1gInSc2	prefix
PCU	PCU	kA	PCU
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pre-commissioning	Preommissioning	k1gInSc1	Pre-commissioning
Unit	Unita	k1gFnPc2	Unita
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
z	z	k7c2	z
aktivní	aktivní	k2eAgFnSc2d1	aktivní
služby	služba	k1gFnSc2	služba
nenese	nést	k5eNaImIp3nS	nést
loď	loď	k1gFnSc1	loď
prefix	prefix	k1gInSc4	prefix
žádný	žádný	k3yNgInSc4	žádný
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
již	již	k9	již
jen	jen	k9	jen
samotným	samotný	k2eAgNnSc7d1	samotné
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
DEPARTMENT	department	k1gInSc1	department
OF	OF	kA	OF
THE	THE	kA	THE
NAVY	NAVY	k?	NAVY
-	-	kIx~	-
Ship	Ship	k1gInSc1	Ship
Naming	Naming	k1gInSc1	Naming
in	in	k?	in
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Navy	Navy	k?	Navy
</s>
</p>
