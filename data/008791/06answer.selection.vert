<s>
Loď	loď	k1gFnSc1	loď
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
Ship	Ship	k1gMnSc1	Ship
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
USS	USS	kA	USS
nebo	nebo	k8xC	nebo
též	též	k9	též
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
<g/>
S.	S.	kA	S.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
prefix	prefix	k1gInSc4	prefix
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
plavidlo	plavidlo	k1gNnSc1	plavidlo
náležící	náležící	k2eAgNnSc1d1	náležící
Námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
