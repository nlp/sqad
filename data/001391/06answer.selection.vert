<s>
Kobalt	kobalt	k1gInSc1	kobalt
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Co	co	k9	co
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Cobaltum	Cobaltum	k1gNnSc1	Cobaltum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
namodralý	namodralý	k2eAgMnSc1d1	namodralý
<g/>
,	,	kIx,	,
feromagnetický	feromagnetický	k2eAgInSc1d1	feromagnetický
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
