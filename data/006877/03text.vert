<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
2012	[number]	k4	2012
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Nice	Nice	k1gFnPc1	Nice
ve	v	k7c6	v
víceúčelové	víceúčelový	k2eAgFnSc6d1	víceúčelová
konferenční	konferenční	k2eAgFnSc6d1	konferenční
budově	budova	k1gFnSc6	budova
Palais	Palais	k1gFnSc1	Palais
des	des	k1gNnSc4	des
Congrè	Congrè	k1gMnSc2	Congrè
Acropolis	Acropolis	k1gInSc4	Acropolis
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
26	[number]	k4	26
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnem	duben	k1gInSc7	duben
2012	[number]	k4	2012
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
soutěžních	soutěžní	k2eAgFnPc6d1	soutěžní
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
:	:	kIx,	:
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgInPc1d1	taneční
páry	pár	k1gInPc1	pár
a	a	k8xC	a
sportovní	sportovní	k2eAgFnSc1d1	sportovní
dvojice	dvojice	k1gFnSc1	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Šampionát	šampionát	k1gInSc1	šampionát
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
muži	muž	k1gMnPc1	muž
Michal	Michal	k1gMnSc1	Michal
Březina	Březina	k1gMnSc1	Březina
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Verner	Verner	k1gMnSc1	Verner
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
Eliška	Eliška	k1gFnSc1	Eliška
Březinová	Březinová	k1gFnSc1	Březinová
a	a	k8xC	a
taneční	taneční	k2eAgInSc1d1	taneční
pár	pár	k1gInSc1	pár
Gabriela	Gabriela	k1gFnSc1	Gabriela
Kubová	Kubová	k1gFnSc1	Kubová
–	–	k?	–
Dmitrij	Dmitrij	k1gFnSc1	Dmitrij
Kiseljov	Kiseljov	k1gInSc1	Kiseljov
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc2	mistrovství
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
závodníci	závodník	k1gMnPc1	závodník
z	z	k7c2	z
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
bruslařské	bruslařský	k2eAgFnSc2d1	bruslařská
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
minimální	minimální	k2eAgFnPc4d1	minimální
věkové	věkový	k2eAgFnPc4d1	věková
hranice	hranice	k1gFnPc4	hranice
15	[number]	k4	15
let	léto	k1gNnPc2	léto
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
šampionát	šampionát	k1gInSc4	šampionát
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
kvalifikovat	kvalifikovat	k5eAaBmF	kvalifikovat
pouze	pouze	k6eAd1	pouze
krasobruslaři	krasobruslař	k1gMnPc1	krasobruslař
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
překročili	překročit	k5eAaPmAgMnP	překročit
minimální	minimální	k2eAgFnSc4d1	minimální
hodnotu	hodnota	k1gFnSc4	hodnota
v	v	k7c6	v
technických	technický	k2eAgInPc6d1	technický
elementech	element	k1gInPc6	element
(	(	kIx(	(
<g/>
technical	technicat	k5eAaPmAgInS	technicat
elements	elementsit	k5eAaPmRp2nS	elementsit
score	scor	k1gMnSc5	scor
<g/>
;	;	kIx,	;
TES	tes	k1gInSc4	tes
<g/>
)	)	kIx)	)
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
soutěži	soutěž	k1gFnSc6	soutěž
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
aktuální	aktuální	k2eAgFnSc2d1	aktuální
či	či	k8xC	či
předchozí	předchozí	k2eAgFnSc2d1	předchozí
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
hranice	hranice	k1gFnSc1	hranice
technických	technický	k2eAgInPc2d1	technický
elementů	element	k1gInPc2	element
(	(	kIx(	(
<g/>
TES	tes	k1gInSc1	tes
<g/>
)	)	kIx)	)
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
disciplínách	disciplína	k1gFnPc6	disciplína
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
:	:	kIx,	:
Na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
či	či	k8xC	či
tři	tři	k4xCgMnPc4	tři
závodníky	závodník	k1gMnPc4	závodník
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
kategorii	kategorie	k1gFnSc6	kategorie
získala	získat	k5eAaPmAgFnS	získat
země	země	k1gFnSc1	země
nárok	nárok	k1gInSc4	nárok
podle	podle	k7c2	podle
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vzešly	vzejít	k5eAaPmAgFnP	vzejít
z	z	k7c2	z
umístění	umístění	k1gNnSc2	umístění
na	na	k7c6	na
minulém	minulý	k2eAgNnSc6d1	Minulé
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
výpravy	výprava	k1gFnPc1	výprava
mohou	moct	k5eAaImIp3nP	moct
nasadit	nasadit	k5eAaPmF	nasadit
dva	dva	k4xCgInPc4	dva
či	či	k8xC	či
tři	tři	k4xCgMnPc4	tři
závodníky	závodník	k1gMnPc4	závodník
v	v	k7c6	v
příslušných	příslušný	k2eAgFnPc6d1	příslušná
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
:	:	kIx,	:
Nice	Nice	k1gFnSc1	Nice
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Pondělí	pondělí	k1gNnSc1	pondělí
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
–	–	k?	–
taneční	taneční	k2eAgInPc1d1	taneční
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
–	–	k?	–
sportovní	sportovní	k2eAgFnSc1d1	sportovní
dvojice	dvojice	k1gFnSc1	dvojice
<g/>
,	,	kIx,	,
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
Úterý	úterý	k1gNnSc1	úterý
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
<g/>
0	[number]	k4	0
–	–	k?	–
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
–	–	k?	–
muži	muž	k1gMnSc3	muž
<g/>
,	,	kIx,	,
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
Středa	středa	k1gFnSc1	středa
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
sportovní	sportovní	k2eAgFnSc1d1	sportovní
dvojice	dvojice	k1gFnSc1	dvojice
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc1d1	krátký
program	program	k1gInSc1	program
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
–	–	k?	–
taneční	taneční	k2eAgInPc1d1	taneční
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
krátké	krátká	k1gFnPc1	krátká
tance	tanec	k1gInSc2	tanec
Čtvrtek	čtvrtek	k1gInSc1	čtvrtek
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
–	–	k?	–
ženy	žena	k1gFnSc2	žena
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
program	program	k1gInSc4	program
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
–	–	k?	–
taneční	taneční	k2eAgFnSc2d1	taneční
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
volné	volný	k2eAgInPc4d1	volný
tance	tanec	k1gInPc4	tanec
Pátek	Pátek	k1gMnSc1	Pátek
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
–	–	k?	–
muži	muž	k1gMnSc3	muž
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
<g />
.	.	kIx.	.
</s>
<s>
program	program	k1gInSc1	program
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
–	–	k?	–
sportovní	sportovní	k2eAgFnSc2d1	sportovní
dvojice	dvojice	k1gFnSc2	dvojice
<g/>
,	,	kIx,	,
volné	volný	k2eAgFnSc2d1	volná
jízdy	jízda	k1gFnSc2	jízda
Sobota	sobota	k1gFnSc1	sobota
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
–	–	k?	–
muži	muž	k1gMnSc6	muž
<g/>
,	,	kIx,	,
volné	volný	k2eAgFnSc2d1	volná
jítdy	jítda	k1gFnSc2	jítda
18	[number]	k4	18
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
–	–	k?	–
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
volné	volný	k2eAgFnSc2d1	volná
jízdy	jízda	k1gFnSc2	jízda
Neděle	neděle	k1gFnSc1	neděle
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
–	–	k?	–
exhibice	exhibice	k1gFnSc2	exhibice
Mediální	mediální	k2eAgNnSc1d1	mediální
pokrytí	pokrytí	k1gNnSc1	pokrytí
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
vysílal	vysílat	k5eAaImAgInS	vysílat
veřejnoprávní	veřejnoprávní	k2eAgInSc1d1	veřejnoprávní
program	program	k1gInSc1	program
ČT	ČT	kA	ČT
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Přenosy	přenos	k1gInPc1	přenos
komentoval	komentovat	k5eAaBmAgMnS	komentovat
Miroslav	Miroslav	k1gMnSc1	Miroslav
Langer	Langer	k1gMnSc1	Langer
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
CH	Ch	kA	Ch
–	–	k?	–
Celkové	celkový	k2eAgNnSc1d1	celkové
hodnocení	hodnocení	k1gNnSc1	hodnocení
OD	od	k7c2	od
–	–	k?	–
Odstoupení	odstoupení	k1gNnSc1	odstoupení
Dvacet	dvacet	k4xCc1	dvacet
sedm	sedm	k4xCc1	sedm
závodníků	závodník	k1gMnPc2	závodník
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
do	do	k7c2	do
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
si	se	k3xPyFc3	se
dvanáct	dvanáct	k4xCc4	dvanáct
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc4	titul
obhájil	obhájit	k5eAaPmAgMnS	obhájit
Kanaďan	Kanaďan	k1gMnSc1	Kanaďan
Patrick	Patrick	k1gMnSc1	Patrick
Chan	Chan	k1gMnSc1	Chan
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
krasobruslařem	krasobruslař	k1gMnSc7	krasobruslař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
dvě	dva	k4xCgNnPc4	dva
mistrovství	mistrovství	k1gNnPc4	mistrovství
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
od	od	k7c2	od
dvou	dva	k4xCgInPc2	dva
triumfů	triumf	k1gInPc2	triumf
Švýcara	Švýcar	k1gMnSc4	Švýcar
Stéphana	Stéphan	k1gMnSc4	Stéphan
Lambiela	Lambiel	k1gMnSc4	Lambiel
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
stupních	stupeň	k1gInPc6	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
dva	dva	k4xCgMnPc1	dva
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
.	.	kIx.	.
</s>
<s>
Třicet	třicet	k4xCc1	třicet
tři	tři	k4xCgFnPc4	tři
krasobruslařek	krasobruslařka	k1gFnPc2	krasobruslařka
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
do	do	k7c2	do
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
si	se	k3xPyFc3	se
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
postup	postup	k1gInSc4	postup
do	do	k7c2	do
krátkého	krátký	k2eAgInSc2d1	krátký
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Premiérový	premiérový	k2eAgInSc1d1	premiérový
titul	titul	k1gInSc1	titul
světové	světový	k2eAgFnSc2d1	světová
šampiónky	šampiónka	k1gFnSc2	šampiónka
získala	získat	k5eAaPmAgFnS	získat
Italka	Italka	k1gFnSc1	Italka
Carolina	Carolina	k1gFnSc1	Carolina
Kostnerová	Kostnerová	k1gFnSc1	Kostnerová
<g/>
.	.	kIx.	.
</s>
<s>
Úřadující	úřadující	k2eAgFnSc1d1	úřadující
vicemistryně	vicemistryně	k1gFnSc1	vicemistryně
Evropy	Evropa	k1gFnSc2	Evropa
Kiira	Kiira	k1gFnSc1	Kiira
Korpiová	Korpiový	k2eAgFnSc1d1	Korpiová
byla	být	k5eAaImAgFnS	být
finskou	finský	k2eAgFnSc7d1	finská
výpravou	výprava	k1gFnSc7	výprava
odhlášena	odhlášen	k2eAgFnSc1d1	odhlášena
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
pro	pro	k7c4	pro
vleklé	vleklý	k2eAgNnSc4d1	vleklé
zranění	zranění	k1gNnSc4	zranění
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
kyčle	kyčel	k1gFnSc2	kyčel
<g/>
.	.	kIx.	.
</s>
<s>
Jedenáct	jedenáct	k4xCc1	jedenáct
párů	pár	k1gInPc2	pár
startovalo	startovat	k5eAaBmAgNnS	startovat
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
a	a	k8xC	a
pět	pět	k4xCc1	pět
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
si	se	k3xPyFc3	se
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Dvacet	dvacet	k4xCc4	dvacet
tři	tři	k4xCgFnPc4	tři
párů	pár	k1gInPc2	pár
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
do	do	k7c2	do
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
a	a	k8xC	a
prvních	první	k4xOgFnPc2	první
deset	deset	k4xCc4	deset
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
do	do	k7c2	do
krátkého	krátký	k2eAgInSc2d1	krátký
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
