<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
2012	[number]	k4	2012
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Nice	Nice	k1gFnPc1	Nice
ve	v	k7c6	v
víceúčelové	víceúčelový	k2eAgFnSc6d1	víceúčelová
konferenční	konferenční	k2eAgFnSc6d1	konferenční
budově	budova	k1gFnSc6	budova
Palais	Palais	k1gFnSc1	Palais
des	des	k1gNnSc4	des
Congrè	Congrè	k1gMnSc2	Congrè
Acropolis	Acropolis	k1gInSc4	Acropolis
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
26	[number]	k4	26
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnem	duben	k1gInSc7	duben
2012	[number]	k4	2012
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
soutěžních	soutěžní	k2eAgFnPc6d1	soutěžní
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
:	:	kIx,	:
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgInPc1d1	taneční
páry	pár	k1gInPc1	pár
a	a	k8xC	a
sportovní	sportovní	k2eAgFnSc1d1	sportovní
dvojice	dvojice	k1gFnSc1	dvojice
<g/>
.	.	kIx.	.
</s>
