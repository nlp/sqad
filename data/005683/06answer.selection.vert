<s>
Amfora	amfora	k1gFnSc1	amfora
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
starověké	starověký	k2eAgFnSc2d1	starověká
keramické	keramický	k2eAgFnSc2d1	keramická
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
úchyty	úchyt	k1gInPc7	úchyt
a	a	k8xC	a
podlouhlým	podlouhlý	k2eAgNnSc7d1	podlouhlé
hrdlem	hrdlo	k1gNnSc7	hrdlo
<g/>
,	,	kIx,	,
užším	úzký	k2eAgNnSc7d2	užší
než	než	k8xS	než
zbytek	zbytek	k1gInSc4	zbytek
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
.	.	kIx.	.
</s>
