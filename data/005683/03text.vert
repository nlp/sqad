<s>
Amfora	amfora	k1gFnSc1	amfora
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
starověké	starověký	k2eAgFnSc2d1	starověká
keramické	keramický	k2eAgFnSc2d1	keramická
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
úchyty	úchyt	k1gInPc7	úchyt
a	a	k8xC	a
podlouhlým	podlouhlý	k2eAgNnSc7d1	podlouhlé
hrdlem	hrdlo	k1gNnSc7	hrdlo
<g/>
,	,	kIx,	,
užším	úzký	k2eAgNnSc7d2	užší
než	než	k8xS	než
zbytek	zbytek	k1gInSc4	zbytek
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
výrazu	výraz	k1gInSc2	výraz
amphora	amphora	k1gFnSc1	amphora
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckých	řecký	k2eAgInPc2d1	řecký
výrazů	výraz	k1gInPc2	výraz
amforeas	amforeasa	k1gFnPc2	amforeasa
(	(	kIx(	(
<g/>
Α	Α	k?	Α
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
amfiforeas	amfiforeasa	k1gFnPc2	amfiforeasa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
amfi-	amfi-	k?	amfi-
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
kolem	kolem	k6eAd1	kolem
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
foreas	foreas	k1gMnSc1	foreas
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nosič	nosič	k1gInSc1	nosič
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slovesa	sloveso	k1gNnSc2	sloveso
ferein	fereina	k1gFnPc2	fereina
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nést	nést	k5eAaImF	nést
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amfory	amfora	k1gFnPc1	amfora
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
libanonsko-syrském	libanonskoyrský	k2eAgNnSc6d1	libanonsko-syrský
pobřeží	pobřeží	k1gNnSc6	pobřeží
okolo	okolo	k7c2	okolo
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
antickém	antický	k2eAgInSc6d1	antický
světě	svět	k1gInSc6	svět
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
starověkými	starověký	k2eAgFnPc7d1	starověká
Řeky	Řek	k1gMnPc4	Řek
a	a	k8xC	a
Římany	Říman	k1gMnPc4	Říman
jako	jako	k8xS	jako
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
pomůcka	pomůcka	k1gFnSc1	pomůcka
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
a	a	k8xC	a
přenášení	přenášení	k1gNnSc4	přenášení
hroznového	hroznový	k2eAgNnSc2d1	hroznové
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
olivového	olivový	k2eAgInSc2d1	olivový
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
zrní	zrní	k1gNnSc2	zrní
<g/>
,	,	kIx,	,
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
dalšího	další	k2eAgNnSc2d1	další
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
starého	starý	k2eAgNnSc2d1	staré
Řecka	Řecko	k1gNnSc2	Řecko
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgNnSc6d1	průmyslové
měřítku	měřítko	k1gNnSc6	měřítko
a	a	k8xC	a
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
až	až	k9	až
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
začaly	začít	k5eAaPmAgInP	začít
nejspíše	nejspíše	k9	nejspíše
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
a	a	k8xC	a
kožené	kožený	k2eAgFnPc1d1	kožená
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
užitečné	užitečný	k2eAgFnPc1d1	užitečná
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
podmořské	podmořský	k2eAgMnPc4d1	podmořský
archeology	archeolog	k1gMnPc4	archeolog
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
díky	díky	k7c3	díky
amforám	amfora	k1gFnPc3	amfora
nalezeným	nalezený	k2eAgFnPc3d1	nalezená
ve	v	k7c6	v
vraku	vrak	k1gInSc6	vrak
lodi	loď	k1gFnSc2	loď
dovedou	dovést	k5eAaPmIp3nP	dovést
stanovit	stanovit	k5eAaPmF	stanovit
stáří	stáří	k1gNnSc4	stáří
vraku	vrak	k1gInSc2	vrak
a	a	k8xC	a
zeměpisný	zeměpisný	k2eAgInSc4d1	zeměpisný
původ	původ	k1gInSc4	původ
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
dochovají	dochovat	k5eAaPmIp3nP	dochovat
v	v	k7c6	v
tak	tak	k6eAd1	tak
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
stále	stále	k6eAd1	stále
přítomen	přítomen	k2eAgInSc1d1	přítomen
původní	původní	k2eAgInSc1d1	původní
obsah	obsah	k1gInSc1	obsah
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
neocenitelným	ocenitelný	k2eNgInSc7d1	neocenitelný
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
stravovacích	stravovací	k2eAgInPc6d1	stravovací
návycích	návyk	k1gInPc6	návyk
a	a	k8xC	a
obchodu	obchod	k1gInSc6	obchod
středomořských	středomořský	k2eAgInPc2d1	středomořský
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgInP	existovat
dva	dva	k4xCgInPc1	dva
základní	základní	k2eAgInPc1d1	základní
druhy	druh	k1gInPc1	druh
amfor	amfora	k1gFnPc2	amfora
<g/>
:	:	kIx,	:
kulovitá	kulovitý	k2eAgFnSc1d1	kulovitá
amfora	amfora	k1gFnSc1	amfora
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
hrdlo	hrdlo	k1gNnSc4	hrdlo
svírá	svírat	k5eAaImIp3nS	svírat
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
amfory	amfora	k1gFnSc2	amfora
ostrý	ostrý	k2eAgInSc4d1	ostrý
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
a	a	k8xC	a
celistvá	celistvý	k2eAgFnSc1d1	celistvá
amfora	amfora	k1gFnSc1	amfora
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
hrdlem	hrdlo	k1gNnSc7	hrdlo
a	a	k8xC	a
tělem	tělo	k1gNnSc7	tělo
tvořen	tvořit	k5eAaImNgInS	tvořit
spojitou	spojitý	k2eAgFnSc7d1	spojitá
křivkou	křivka	k1gFnSc7	křivka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
dobách	doba	k1gFnPc6	doba
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
byly	být	k5eAaImAgFnP	být
běžně	běžně	k6eAd1	běžně
používány	používat	k5eAaImNgFnP	používat
kulovité	kulovitý	k2eAgFnPc1d1	kulovitá
amfory	amfora	k1gFnPc1	amfora
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
asi	asi	k9	asi
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
nahradily	nahradit	k5eAaPmAgFnP	nahradit
amfory	amfora	k1gFnPc1	amfora
celistvé	celistvý	k2eAgFnPc1d1	celistvá
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
byly	být	k5eAaImAgFnP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
se	s	k7c7	s
špičatou	špičatý	k2eAgFnSc7d1	špičatá
patou	pata	k1gFnSc7	pata
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
uchovávány	uchovávat	k5eAaImNgInP	uchovávat
ve	v	k7c6	v
svislé	svislý	k2eAgFnSc6d1	svislá
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
zabořené	zabořený	k2eAgInPc1d1	zabořený
do	do	k7c2	do
písku	písek	k1gInSc2	písek
nebo	nebo	k8xC	nebo
měkké	měkký	k2eAgFnSc2d1	měkká
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
amfory	amfora	k1gFnSc2	amfora
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
různila	různit	k5eAaImAgFnS	různit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
amfory	amfora	k1gFnPc1	amfora
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
až	až	k9	až
1,5	[number]	k4	1,5
m	m	kA	m
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgFnPc1d1	jiná
měřily	měřit	k5eAaImAgFnP	měřit
sotva	sotva	k6eAd1	sotva
30	[number]	k4	30
cm	cm	kA	cm
–	–	k?	–
nejmenší	malý	k2eAgFnPc1d3	nejmenší
amfory	amfora	k1gFnPc1	amfora
se	se	k3xPyFc4	se
nazývaly	nazývat	k5eAaImAgFnP	nazývat
amphoriskoi	amphorisko	k1gFnPc1	amphorisko
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
malé	malý	k2eAgFnPc4d1	malá
amfory	amfora	k1gFnPc4	amfora
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
amfor	amfora	k1gFnPc2	amfora
měřila	měřit	k5eAaImAgFnS	měřit
kolem	kolem	k7c2	kolem
45	[number]	k4	45
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
variant	varianta	k1gFnPc2	varianta
amfor	amfora	k1gFnPc2	amfora
došlo	dojít	k5eAaPmAgNnS	dojít
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
ke	k	k7c3	k
standardizaci	standardizace	k1gFnSc3	standardizace
–	–	k?	–
například	například	k6eAd1	například
vinná	vinný	k2eAgFnSc1d1	vinná
amfora	amfora	k1gFnSc1	amfora
pojala	pojmout	k5eAaPmAgFnS	pojmout
obvykle	obvykle	k6eAd1	obvykle
39	[number]	k4	39
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
bylo	být	k5eAaImAgNnS	být
rozpoznáno	rozpoznat	k5eAaPmNgNnS	rozpoznat
okolo	okolo	k7c2	okolo
66	[number]	k4	66
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
amfor	amfora	k1gFnPc2	amfora
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
amfory	amfora	k1gFnPc1	amfora
také	také	k9	také
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k8xS	jako
dutá	dutat	k5eAaImIp3nS	dutat
míra	míra	k1gFnSc1	míra
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
26,2	[number]	k4	26,2
litru	litr	k1gInSc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
společenské	společenský	k2eAgInPc4d1	společenský
a	a	k8xC	a
obřadní	obřadní	k2eAgInPc4d1	obřadní
účely	účel	k1gInPc4	účel
byly	být	k5eAaImAgFnP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
malované	malovaný	k2eAgFnPc1d1	malovaná
amfory	amfora	k1gFnPc1	amfora
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
praktičtějších	praktický	k2eAgFnPc2d2	praktičtější
amfor	amfora	k1gFnPc2	amfora
<g/>
:	:	kIx,	:
pyšní	pyšnit	k5eAaImIp3nS	pyšnit
se	se	k3xPyFc4	se
velkým	velký	k2eAgInSc7d1	velký
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
prstencovým	prstencový	k2eAgInSc7d1	prstencový
spodkem	spodek	k1gInSc7	spodek
<g/>
,	,	kIx,	,
leštěným	leštěný	k2eAgInSc7d1	leštěný
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
ozdobami	ozdoba	k1gFnPc7	ozdoba
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
lidských	lidský	k2eAgFnPc2d1	lidská
postav	postava	k1gFnPc2	postava
nebo	nebo	k8xC	nebo
geometrických	geometrický	k2eAgInPc2d1	geometrický
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
takové	takový	k3xDgFnPc1	takový
amfory	amfora	k1gFnPc1	amfora
byly	být	k5eAaImAgFnP	být
používány	používán	k2eAgInPc1d1	používán
při	při	k7c6	při
pohřbívání	pohřbívání	k1gNnSc6	pohřbívání
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
ceny	cena	k1gFnPc4	cena
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnPc4	vítěz
při	při	k7c6	při
různých	různý	k2eAgNnPc6d1	různé
kláních	klání	k1gNnPc6	klání
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
panathénajské	panathénajský	k2eAgFnSc2d1	panathénajský
amfory	amfora	k1gFnSc2	amfora
udělované	udělovaný	k2eAgFnSc2d1	udělovaná
vítězům	vítěz	k1gMnPc3	vítěz
panathénajských	panathénajský	k2eAgFnPc2d1	panathénajský
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
konaných	konaný	k2eAgFnPc2d1	konaná
každoročně	každoročně	k6eAd1	každoročně
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
bohyně	bohyně	k1gFnSc2	bohyně
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
patronky	patronka	k1gFnPc4	patronka
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
nádoby	nádoba	k1gFnPc1	nádoba
byl	být	k5eAaImAgInS	být
naplňovány	naplňovat	k5eAaImNgFnP	naplňovat
olejem	olej	k1gInSc7	olej
lisovaným	lisovaný	k2eAgInSc7d1	lisovaný
z	z	k7c2	z
posvátných	posvátný	k2eAgFnPc2d1	posvátná
oliv	oliva	k1gFnPc2	oliva
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
černofigurovém	černofigurový	k2eAgInSc6d1	černofigurový
stylu	styl	k1gInSc6	styl
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
pozadím	pozadí	k1gNnSc7	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
nesly	nést	k5eAaImAgInP	nést
postavu	postava	k1gFnSc4	postava
bohyně	bohyně	k1gFnSc2	bohyně
Athény	Athéna	k1gFnSc2	Athéna
po	po	k7c6	po
jejíchž	jejíž	k3xOyRp3gFnPc6	jejíž
stranách	strana	k1gFnPc6	strana
stály	stát	k5eAaImAgInP	stát
dva	dva	k4xCgInPc1	dva
sloupy	sloup	k1gInPc1	sloup
s	s	k7c7	s
kohoutem	kohout	k1gInSc7	kohout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Z	z	k7c2	z
athénských	athénský	k2eAgInPc2d1	athénský
závodů	závod	k1gInPc2	závod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
scéně	scéna	k1gFnSc6	scéna
byla	být	k5eAaImAgFnS	být
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
scéna	scéna	k1gFnSc1	scéna
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
podarovaný	podarovaný	k2eAgMnSc1d1	podarovaný
závodník	závodník	k1gMnSc1	závodník
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
doplněná	doplněná	k1gFnSc1	doplněná
datem	datum	k1gNnSc7	datum
konání	konání	k1gNnSc2	konání
<g/>
.	.	kIx.	.
</s>
