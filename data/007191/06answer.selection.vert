<s>
Šrámkova	Šrámkův	k2eAgFnSc1d1	Šrámkova
Sobotka	Sobotka	k1gFnSc1	Sobotka
je	být	k5eAaImIp3nS	být
festival	festival	k1gInSc4	festival
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
konaný	konaný	k2eAgMnSc1d1	konaný
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
významného	významný	k2eAgMnSc2d1	významný
soboteckého	sobotecký	k2eAgMnSc2d1	sobotecký
rodáka	rodák	k1gMnSc2	rodák
básníka	básník	k1gMnSc2	básník
Fráni	Fráňa	k1gMnSc2	Fráňa
Šrámka	Šrámek	k1gMnSc2	Šrámek
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
