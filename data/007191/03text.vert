<s>
Šrámkova	Šrámkův	k2eAgFnSc1d1	Šrámkova
Sobotka	Sobotka	k1gFnSc1	Sobotka
je	být	k5eAaImIp3nS	být
festival	festival	k1gInSc4	festival
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
konaný	konaný	k2eAgMnSc1d1	konaný
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
významného	významný	k2eAgMnSc2d1	významný
soboteckého	sobotecký	k2eAgMnSc2d1	sobotecký
rodáka	rodák	k1gMnSc2	rodák
básníka	básník	k1gMnSc2	básník
Fráni	Fráňa	k1gMnSc2	Fráňa
Šrámka	Šrámek	k1gMnSc2	Šrámek
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
festivalu	festival	k1gInSc2	festival
jsou	být	k5eAaImIp3nP	být
odborné	odborný	k2eAgFnPc1d1	odborná
přednášky	přednáška	k1gFnPc1	přednáška
<g/>
,	,	kIx,	,
autorská	autorský	k2eAgNnPc1d1	autorské
čtení	čtení	k1gNnPc1	čtení
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnSc1d1	divadelní
představení	představení	k1gNnSc1	představení
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
také	také	k6eAd1	také
dílny	dílna	k1gFnSc2	dílna
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
českými	český	k2eAgMnPc7d1	český
umělci	umělec	k1gMnPc7	umělec
a	a	k8xC	a
odborníky	odborník	k1gMnPc7	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
konat	konat	k5eAaImF	konat
již	již	k6eAd1	již
61	[number]	k4	61
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
tématem	téma	k1gNnSc7	téma
bude	být	k5eAaImBp3nS	být
Kritika	kritika	k1gFnSc1	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Výpověď	výpověď	k1gFnSc1	výpověď
umění	umění	k1gNnSc2	umění
<g/>
?	?	kIx.	?
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
myšlenky	myšlenka	k1gFnSc2	myšlenka
festivalu	festival	k1gInSc2	festival
jsou	být	k5eAaImIp3nP	být
spjaty	spjat	k2eAgInPc1d1	spjat
jednak	jednak	k8xC	jednak
s	s	k7c7	s
předválečnými	předválečný	k2eAgFnPc7d1	předválečná
letními	letní	k2eAgFnPc7d1	letní
divadelními	divadelní	k2eAgFnPc7d1	divadelní
slavnostmi	slavnost	k1gFnPc7	slavnost
v	v	k7c6	v
Sobotce	Sobotka	k1gFnSc6	Sobotka
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
Šrámek	šrámek	k1gInSc1	šrámek
účastnil	účastnit	k5eAaImAgInS	účastnit
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednak	jednak	k8xC	jednak
s	s	k7c7	s
letní	letní	k2eAgFnSc7d1	letní
přehlídkou	přehlídka	k1gFnSc7	přehlídka
amatérských	amatérský	k2eAgMnPc2d1	amatérský
divadelních	divadelní	k2eAgMnPc2d1	divadelní
souborů	soubor	k1gInPc2	soubor
pořádanou	pořádaný	k2eAgFnSc7d1	pořádaná
místním	místní	k2eAgMnSc7d1	místní
patriotem	patriot	k1gMnSc7	patriot
MUDr.	MUDr.	kA	MUDr.
Aloisem	Alois	k1gMnSc7	Alois
Kafkou	Kafka	k1gMnSc7	Kafka
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Šrámkových	Šrámkových	k2eAgInPc2d1	Šrámkových
nedožitých	dožitý	k2eNgInPc2d1	dožitý
80	[number]	k4	80
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
první	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
uskutečnění	uskutečnění	k1gNnSc6	uskutečnění
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
především	především	k9	především
Milka	Milka	k1gFnSc1	Milka
Hrdličková-Šrámková	Hrdličková-Šrámkový	k2eAgFnSc1d1	Hrdličková-Šrámkový
(	(	kIx(	(
<g/>
Šrámkova	Šrámkův	k2eAgFnSc1d1	Šrámkova
družka	družka	k1gFnSc1	družka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Hejnovi	Hejnův	k2eAgMnPc1d1	Hejnův
(	(	kIx(	(
<g/>
učitelé	učitel	k1gMnPc1	učitel
a	a	k8xC	a
správci	správce	k1gMnPc1	správce
archívu	archív	k1gInSc2	archív
Fráni	Fráňa	k1gFnSc2	Fráňa
Šrámka	Šrámek	k1gMnSc2	Šrámek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Bílková-Kyzivátová	Bílková-Kyzivátový	k2eAgFnSc1d1	Bílková-Kyzivátový
(	(	kIx(	(
<g/>
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
MNV	MNV	kA	MNV
<g/>
)	)	kIx)	)
a	a	k8xC	a
již	již	k6eAd1	již
zmiňovaný	zmiňovaný	k2eAgMnSc1d1	zmiňovaný
Alois	Alois	k1gMnSc1	Alois
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Šrámkově	Šrámkův	k2eAgFnSc6d1	Šrámkova
Sobotce	Sobotka	k1gFnSc6	Sobotka
založen	založit	k5eAaPmNgInS	založit
časopis	časopis	k1gInSc1	časopis
Splav	splav	k1gInSc1	splav
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
redaktoři	redaktor	k1gMnPc1	redaktor
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
aktivně	aktivně	k6eAd1	aktivně
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
festivalu	festival	k1gInSc2	festival
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
místním	místní	k2eAgNnSc7d1	místní
kulturním	kulturní	k2eAgNnSc7d1	kulturní
střediskem	středisko	k1gNnSc7	středisko
a	a	k8xC	a
soboteckými	sobotecký	k2eAgMnPc7d1	sobotecký
patrioty	patriot	k1gMnPc7	patriot
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
prázdninovém	prázdninový	k2eAgInSc6d1	prázdninový
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
otevírá	otevírat	k5eAaImIp3nS	otevírat
tak	tak	k9	tak
léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
školní	školní	k2eAgInSc4d1	školní
rok	rok	k1gInSc4	rok
i	i	k8xC	i
divadelní	divadelní	k2eAgFnSc4d1	divadelní
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
ročník	ročník	k1gInSc1	ročník
má	mít	k5eAaImIp3nS	mít
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
nosné	nosný	k2eAgNnSc1d1	nosné
téma	téma	k1gNnSc1	téma
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
se	se	k3xPyFc4	se
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
náplň	náplň	k1gFnSc1	náplň
festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníky	návštěvník	k1gMnPc7	návštěvník
festivalu	festival	k1gInSc2	festival
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
studenti	student	k1gMnPc1	student
humanitních	humanitní	k2eAgInPc2d1	humanitní
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
učitelé	učitel	k1gMnPc1	učitel
<g/>
,	,	kIx,	,
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
knihoven	knihovna	k1gFnPc2	knihovna
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
kulturní	kulturní	k2eAgMnPc1d1	kulturní
pracovníci	pracovník	k1gMnPc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Festivalové	festivalový	k2eAgNnSc1d1	festivalové
publikum	publikum	k1gNnSc1	publikum
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pestré	pestrý	k2eAgNnSc1d1	pestré
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
jej	on	k3xPp3gNnSc4	on
lidé	člověk	k1gMnPc1	člověk
všech	všecek	k3xTgFnPc2	všecek
generací	generace	k1gFnPc2	generace
-	-	kIx~	-
i	i	k9	i
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
programové	programový	k2eAgFnSc3d1	programová
skladbě	skladba	k1gFnSc3	skladba
se	se	k3xPyFc4	se
na	na	k7c4	na
festival	festival	k1gInSc4	festival
stále	stále	k6eAd1	stále
vracejí	vracet	k5eAaImIp3nP	vracet
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c4	na
festival	festival	k1gInSc4	festival
jezdili	jezdit	k5eAaImAgMnP	jezdit
jako	jako	k9	jako
studenti	student	k1gMnPc1	student
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
vrací	vracet	k5eAaImIp3nS	vracet
i	i	k9	i
s	s	k7c7	s
vlastními	vlastní	k2eAgFnPc7d1	vlastní
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
partnery	partner	k1gMnPc7	partner
<g/>
,	,	kIx,	,
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
publika	publikum	k1gNnSc2	publikum
tvoří	tvořit	k5eAaImIp3nP	tvořit
také	také	k9	také
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
seniorském	seniorský	k2eAgInSc6d1	seniorský
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Učitelé	učitel	k1gMnPc1	učitel
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
mohou	moct	k5eAaImIp3nP	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
festivalu	festival	k1gInSc2	festival
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
základny	základna	k1gFnSc2	základna
EXOD	EXOD	kA	EXOD
<g/>
,	,	kIx,	,
projektu	projekt	k1gInSc2	projekt
odborového	odborový	k2eAgInSc2d1	odborový
svazu	svaz	k1gInSc2	svaz
pracovníků	pracovník	k1gMnPc2	pracovník
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
ČMKOS	ČMKOS	kA	ČMKOS
<g/>
.	.	kIx.	.
</s>
<s>
Festivalových	festivalový	k2eAgFnPc2d1	festivalová
dílen	dílna	k1gFnPc2	dílna
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
zúčastní	zúčastnit	k5eAaPmIp3nP	zúčastnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc1	sto
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
týden	týden	k1gInSc1	týden
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
prožije	prožít	k5eAaPmIp3nS	prožít
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
stě	sto	k4xCgFnPc1	sto
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
však	však	k9	však
přijíždějí	přijíždět	k5eAaImIp3nP	přijíždět
jen	jen	k9	jen
na	na	k7c4	na
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
pestrosti	pestrost	k1gFnSc3	pestrost
festivalového	festivalový	k2eAgNnSc2d1	festivalové
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Programové	programový	k2eAgNnSc1d1	programové
schéma	schéma	k1gNnSc1	schéma
festivalu	festival	k1gInSc2	festival
se	se	k3xPyFc4	se
dynamicky	dynamicky	k6eAd1	dynamicky
proměňuje	proměňovat	k5eAaImIp3nS	proměňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
následovalo	následovat	k5eAaImAgNnS	následovat
poetické	poetický	k2eAgNnSc1d1	poetické
odpoledne	odpoledne	k1gNnSc4	odpoledne
spojené	spojený	k2eAgNnSc4d1	spojené
s	s	k7c7	s
procházkou	procházka	k1gFnSc7	procházka
po	po	k7c6	po
městě	město	k1gNnSc6	město
a	a	k8xC	a
recitací	recitace	k1gFnPc2	recitace
<g/>
,	,	kIx,	,
následující	následující	k2eAgInPc1d1	následující
dny	den	k1gInPc1	den
pak	pak	k6eAd1	pak
začínaly	začínat	k5eAaImAgInP	začínat
odbornými	odborný	k2eAgFnPc7d1	odborná
přednáškami	přednáška	k1gFnPc7	přednáška
a	a	k8xC	a
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
odpoledními	odpolední	k2eAgFnPc7d1	odpolední
vzdělávacími	vzdělávací	k2eAgFnPc7d1	vzdělávací
a	a	k8xC	a
kulturními	kulturní	k2eAgFnPc7d1	kulturní
akcemi	akce	k1gFnPc7	akce
<g/>
;	;	kIx,	;
ty	ten	k3xDgFnPc1	ten
pak	pak	k6eAd1	pak
tvořily	tvořit	k5eAaImAgFnP	tvořit
také	také	k9	také
program	program	k1gInSc4	program
večerní	večerní	k2eAgInSc4d1	večerní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
festival	festival	k1gInSc1	festival
zahajován	zahajovat	k5eAaImNgInS	zahajovat
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
večer	večer	k6eAd1	večer
koncertem	koncert	k1gInSc7	koncert
<g/>
,	,	kIx,	,
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
dopoledne	dopoledne	k6eAd1	dopoledne
probíhá	probíhat	k5eAaImIp3nS	probíhat
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
zahájení	zahájení	k1gNnSc4	zahájení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
týdne	týden	k1gInSc2	týden
začíná	začínat	k5eAaImIp3nS	začínat
den	den	k1gInSc4	den
odbornou	odborný	k2eAgFnSc7d1	odborná
přednáškou	přednáška	k1gFnSc7	přednáška
<g/>
,	,	kIx,	,
po	po	k7c6	po
poledni	poledne	k1gNnSc6	poledne
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
vybrat	vybrat	k5eAaPmF	vybrat
z	z	k7c2	z
poslechových	poslechový	k2eAgInPc2d1	poslechový
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
bloků	blok	k1gInPc2	blok
autorských	autorský	k2eAgNnPc2d1	autorské
čtení	čtení	k1gNnPc2	čtení
<g/>
;	;	kIx,	;
večery	večer	k1gInPc1	večer
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
věnovány	věnovat	k5eAaImNgFnP	věnovat
divadelním	divadelní	k2eAgFnPc3d1	divadelní
inscenacím	inscenace	k1gFnPc3	inscenace
<g/>
,	,	kIx,	,
koncertům	koncert	k1gInPc3	koncert
nebo	nebo	k8xC	nebo
filmovým	filmový	k2eAgFnPc3d1	filmová
projekcím	projekce	k1gFnPc3	projekce
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
navštívit	navštívit	k5eAaPmF	navštívit
také	také	k9	také
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
výstav	výstava	k1gFnPc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
konal	konat	k5eAaImAgInS	konat
běžecký	běžecký	k2eAgInSc1d1	běžecký
závod	závod	k1gInSc1	závod
Šrámkova	Šrámkův	k2eAgFnSc1d1	Šrámkova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nahradil	nahradit	k5eAaPmAgInS	nahradit
dříve	dříve	k6eAd2	dříve
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
běh	běh	k1gInSc1	běh
k	k	k7c3	k
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
neexistující	existující	k2eNgFnSc6d1	neexistující
Semtinské	Semtinský	k2eAgFnSc6d1	Semtinská
lípě	lípa	k1gFnSc6	lípa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
pak	pak	k6eAd1	pak
probíhá	probíhat	k5eAaImIp3nS	probíhat
jednodenní	jednodenní	k2eAgInSc1d1	jednodenní
BarCamp	BarCamp	k1gInSc1	BarCamp
<g/>
,	,	kIx,	,
interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
konference	konference	k1gFnSc1	konference
určená	určený	k2eAgFnSc1d1	určená
především	především	k9	především
učitelům	učitel	k1gMnPc3	učitel
a	a	k8xC	a
pracovníkům	pracovník	k1gMnPc3	pracovník
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
zastřešuje	zastřešovat	k5eAaImIp3nS	zastřešovat
a	a	k8xC	a
organizuje	organizovat	k5eAaBmIp3nS	organizovat
Josef	Josef	k1gMnSc1	Josef
Šlerka	Šlerka	k1gMnSc1	Šlerka
<g/>
.	.	kIx.	.
</s>
<s>
Paralelně	paralelně	k6eAd1	paralelně
probíhají	probíhat	k5eAaImIp3nP	probíhat
také	také	k9	také
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
dílny	dílna	k1gFnSc2	dílna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
výstupy	výstup	k1gInPc1	výstup
účastníci	účastník	k1gMnPc1	účastník
prezentují	prezentovat	k5eAaBmIp3nP	prezentovat
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
jarmarku	jarmarku	k?	jarmarku
dílen	dílna	k1gFnPc2	dílna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
byly	být	k5eAaImAgFnP	být
dílny	dílna	k1gFnPc1	dílna
věnovány	věnovat	k5eAaImNgFnP	věnovat
výhradně	výhradně	k6eAd1	výhradně
uměleckému	umělecký	k2eAgInSc3d1	umělecký
přednesu	přednes	k1gInSc3	přednes
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
určeny	určit	k5eAaPmNgInP	určit
pouze	pouze	k6eAd1	pouze
studentům	student	k1gMnPc3	student
pedagogických	pedagogický	k2eAgFnPc2d1	pedagogická
a	a	k8xC	a
filozofických	filozofický	k2eAgFnPc2d1	filozofická
fakult	fakulta	k1gFnPc2	fakulta
(	(	kIx(	(
<g/>
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byly	být	k5eAaImAgFnP	být
koncipovány	koncipovat	k5eAaBmNgFnP	koncipovat
včetně	včetně	k7c2	včetně
soutěžní	soutěžní	k2eAgFnSc2d1	soutěžní
přehlídky	přehlídka	k1gFnSc2	přehlídka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
měnící	měnící	k2eAgFnSc7d1	měnící
se	se	k3xPyFc4	se
dobou	doba	k1gFnSc7	doba
a	a	k8xC	a
vývojem	vývoj	k1gInSc7	vývoj
zájmů	zájem	k1gInPc2	zájem
účastníků	účastník	k1gMnPc2	účastník
byla	být	k5eAaImAgFnS	být
nabídka	nabídka	k1gFnSc1	nabídka
dílen	dílna	k1gFnPc2	dílna
postupně	postupně	k6eAd1	postupně
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tradičně	tradičně	k6eAd1	tradičně
jsou	být	k5eAaImIp3nP	být
pořádány	pořádán	k2eAgFnPc1d1	pořádána
dílny	dílna	k1gFnPc1	dílna
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
překladu	překlad	k1gInSc2	překlad
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
tvůrčímu	tvůrčí	k2eAgNnSc3d1	tvůrčí
psaní	psaní	k1gNnSc3	psaní
<g/>
,	,	kIx,	,
rozhlasovému	rozhlasový	k2eAgInSc3d1	rozhlasový
dokumentu	dokument	k1gInSc3	dokument
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc3d1	divadelní
a	a	k8xC	a
literární	literární	k2eAgFnSc3d1	literární
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
recitaci	recitace	k1gFnSc3	recitace
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc3d1	divadelní
tvorbě	tvorba	k1gFnSc3	tvorba
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
lektory	lektor	k1gMnPc4	lektor
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
objevili	objevit	k5eAaPmAgMnP	objevit
např.	např.	kA	např.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Dimter	Dimter	k1gMnSc1	Dimter
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Šindelka	Šindelka	k1gMnSc1	Šindelka
<g/>
,	,	kIx,	,
Jonáš	Jonáš	k1gMnSc1	Jonáš
Hájek	Hájek	k1gMnSc1	Hájek
či	či	k8xC	či
Olga	Olga	k1gFnSc1	Olga
Walló	Walló	k1gFnSc2	Walló
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
dětské	dětský	k2eAgFnSc2d1	dětská
dílny	dílna	k1gFnSc2	dílna
-	-	kIx~	-
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
se	se	k3xPyFc4	se
tvůrčí	tvůrčí	k2eAgFnSc3d1	tvůrčí
práci	práce	k1gFnSc3	práce
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
věnují	věnovat	k5eAaImIp3nP	věnovat
zkušení	zkušený	k2eAgMnPc1d1	zkušený
pedagogové	pedagog	k1gMnPc1	pedagog
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
dílen	dílna	k1gFnPc2	dílna
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
mohou	moct	k5eAaImIp3nP	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
i	i	k9	i
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přijedou	přijet	k5eAaPmIp3nP	přijet
s	s	k7c7	s
vlastními	vlastní	k2eAgFnPc7d1	vlastní
ratolestmi	ratolest	k1gFnPc7	ratolest
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
ostatně	ostatně	k6eAd1	ostatně
snaží	snažit	k5eAaImIp3nP	snažit
vycházet	vycházet	k5eAaImF	vycházet
vstříc	vstříc	k6eAd1	vstříc
mladým	mladý	k2eAgFnPc3d1	mladá
rodinám	rodina	k1gFnPc3	rodina
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
festivalové	festivalový	k2eAgFnSc2d1	festivalová
zahrady	zahrada	k1gFnSc2	zahrada
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
hračky	hračka	k1gFnSc2	hračka
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
těšit	těšit	k5eAaImF	těšit
na	na	k7c4	na
několik	několik	k4yIc4	několik
divadelních	divadelní	k2eAgNnPc2d1	divadelní
představení	představení	k1gNnPc2	představení
určených	určený	k2eAgNnPc2d1	určené
právě	právě	k6eAd1	právě
mladším	mladý	k2eAgInPc3d2	mladší
ročníkům	ročník	k1gInPc3	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
vlastní	vlastní	k2eAgInSc1d1	vlastní
časopis	časopis	k1gInSc1	časopis
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
podle	podle	k7c2	podle
Šrámkovy	Šrámkův	k2eAgFnSc2d1	Šrámkova
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
:	:	kIx,	:
Splav	splav	k1gInSc1	splav
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Redakci	redakce	k1gFnSc3	redakce
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinou	většinou	k6eAd1	většinou
studenti	student	k1gMnPc1	student
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
kreativní	kreativní	k2eAgFnSc4d1	kreativní
reflexi	reflexe	k1gFnSc4	reflexe
festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
o	o	k7c6	o
redakci	redakce	k1gFnSc6	redakce
časopisu	časopis	k1gInSc2	časopis
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
další	další	k2eAgFnSc6d1	další
sobotecké	sobotecký	k2eAgFnSc6d1	sobotecká
dílně	dílna	k1gFnSc6	dílna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
redakce	redakce	k1gFnSc1	redakce
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
zletislost	zletislost	k1gFnSc4	zletislost
časopisu	časopis	k1gInSc2	časopis
mj.	mj.	kA	mj.
jeho	jeho	k3xOp3gInPc7	jeho
přejmenováním	přejmenování	k1gNnSc7	přejmenování
na	na	k7c4	na
Raport	raport	k1gInSc4	raport
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
název	název	k1gInSc1	název
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
Šrámkových	Šrámkových	k2eAgNnPc2d1	Šrámkových
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
–	–	k?	–
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
–	–	k?	–
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
festivalem	festival	k1gInSc7	festival
spjata	spjat	k2eAgFnSc1d1	spjata
také	také	k6eAd1	také
literární	literární	k2eAgFnSc4d1	literární
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
kategoriích	kategorie	k1gFnPc6	kategorie
(	(	kIx(	(
<g/>
poezie	poezie	k1gFnSc1	poezie
pro	pro	k7c4	pro
autory	autor	k1gMnPc4	autor
do	do	k7c2	do
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
próza	próza	k1gFnSc1	próza
pro	pro	k7c4	pro
autory	autor	k1gMnPc4	autor
do	do	k7c2	do
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vlastivědné	vlastivědný	k2eAgFnPc1d1	vlastivědná
práce	práce	k1gFnPc1	práce
o	o	k7c6	o
regionu	region	k1gInSc6	region
Českého	český	k2eAgInSc2d1	český
ráje	ráj	k1gInSc2	ráj
a	a	k8xC	a
Jičínska	Jičínsko	k1gNnSc2	Jičínsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
úspěšnými	úspěšný	k2eAgMnPc7d1	úspěšný
autory	autor	k1gMnPc7	autor
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
některé	některý	k3yIgInPc4	některý
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
oceňované	oceňovaný	k2eAgMnPc4d1	oceňovaný
prozaiky	prozaik	k1gMnPc4	prozaik
nebo	nebo	k8xC	nebo
básníky	básník	k1gMnPc4	básník
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Rudčenková	Rudčenkový	k2eAgFnSc1d1	Rudčenková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
je	být	k5eAaImIp3nS	být
pořádán	pořádán	k2eAgInSc1d1	pořádán
soboteckým	sobotecký	k2eAgNnSc7d1	sobotecké
Městským	městský	k2eAgNnSc7d1	Městské
kulturním	kulturní	k2eAgNnSc7d1	kulturní
střediskem	středisko	k1gNnSc7	středisko
za	za	k7c2	za
aktivní	aktivní	k2eAgFnSc2d1	aktivní
účasti	účast	k1gFnSc2	účast
dobrovolné	dobrovolný	k2eAgFnPc1d1	dobrovolná
skupiny	skupina	k1gFnPc1	skupina
nadšenců	nadšenec	k1gMnPc2	nadšenec
různého	různý	k2eAgNnSc2d1	různé
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
není	být	k5eNaImIp3nS	být
plánován	plánovat	k5eAaImNgInS	plánovat
jako	jako	k8xS	jako
komerční	komerční	k2eAgFnPc1d1	komerční
akce	akce	k1gFnPc1	akce
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zisku	zisk	k1gInSc2	zisk
z	z	k7c2	z
některého	některý	k3yIgNnSc2	některý
večerního	večerní	k2eAgNnSc2d1	večerní
představení	představení	k1gNnSc2	představení
díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
návštěvnosti	návštěvnost	k1gFnSc3	návštěvnost
místních	místní	k2eAgMnPc2d1	místní
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
bilance	bilance	k1gFnSc1	bilance
je	být	k5eAaImIp3nS	být
ztrátová	ztrátový	k2eAgFnSc1d1	ztrátová
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Skladba	skladba	k1gFnSc1	skladba
peněžních	peněžní	k2eAgInPc2d1	peněžní
zdrojů	zdroj	k1gInPc2	zdroj
je	být	k5eAaImIp3nS	být
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
a	a	k8xC	a
nejist	nejisto	k1gNnPc2	nejisto
<g/>
,	,	kIx,	,
festival	festival	k1gInSc4	festival
finančně	finančně	k6eAd1	finančně
podporují	podporovat	k5eAaImIp3nP	podporovat
především	především	k9	především
město	město	k1gNnSc4	město
Sobotka	Sobotka	k1gMnSc1	Sobotka
<g/>
,	,	kIx,	,
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
ČR	ČR	kA	ČR
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
festivalu	festival	k1gInSc2	festival
Šrámkova	Šrámkův	k2eAgFnSc1d1	Šrámkova
Sobotka	Sobotka	k1gFnSc1	Sobotka
Šrámkova	Šrámkův	k2eAgFnSc1d1	Šrámkova
Sobotka	Sobotka	k1gFnSc1	Sobotka
na	na	k7c6	na
soboteckém	sobotecký	k2eAgInSc6d1	sobotecký
webu	web	k1gInSc6	web
Stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
festivalu	festival	k1gInSc6	festival
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
časopisu	časopis	k1gInSc2	časopis
SPLAV	splav	k1gInSc4	splav
Písemné	písemný	k2eAgInPc1d1	písemný
výstupy	výstup	k1gInPc1	výstup
účastníků	účastník	k1gMnPc2	účastník
dílen	dílna	k1gFnPc2	dílna
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
Facebookové	Facebookový	k2eAgFnPc4d1	Facebooková
stránky	stránka	k1gFnPc4	stránka
festivalu	festival	k1gInSc2	festival
Informace	informace	k1gFnSc2	informace
o	o	k7c4	o
BarCampu	BarCampa	k1gFnSc4	BarCampa
Wolkrův	Wolkrův	k2eAgInSc1d1	Wolkrův
Prostějov	Prostějov	k1gInSc1	Prostějov
Šrámkův	Šrámkův	k2eAgInSc1d1	Šrámkův
Písek	Písek	k1gInSc1	Písek
Jiráskův	Jiráskův	k2eAgInSc4d1	Jiráskův
Hronov	Hronov	k1gInSc4	Hronov
Neumannovy	Neumannův	k2eAgInPc4d1	Neumannův
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
Ortenova	Ortenův	k2eAgFnSc1d1	Ortenova
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
</s>
