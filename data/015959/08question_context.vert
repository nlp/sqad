<s>
Chilli	chilli	k2
paprička	paprička	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
chili	chile	k1gFnSc4
paprička	paprička	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
čili	čili	k8xC
paprička	paprička	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
kajenský	kajenský	k2eAgInSc1d1
pepř	pepř	k1gInSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
cayenský	cayenský	k2eAgInSc1d1
pepř	pepř	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
plod	plod	k1gInSc4
především	především	k6eAd1
exotických	exotický	k2eAgInPc2d1
druhů	druh	k1gInPc2
křovitých	křovitý	k2eAgFnPc2d1
paprik	paprika	k1gFnPc2
<g/>
,	,	kIx,
rostoucích	rostoucí	k2eAgInPc2d1
v	v	k7c6
tropech	trop	k1gInPc6
a	a	k8xC
subtropech	subtropy	k1gInPc6
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
Vietnamu	Vietnam	k1gInSc2
<g/>
,	,	kIx,
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc2d1
a	a	k8xC
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
Mexika	Mexiko	k1gNnSc2
<g/>
.	.	kIx.
</s>