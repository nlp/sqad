<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
o	o	k7c6
rozloze	rozloha	k1gFnSc6
asi	asi	k9
606	#num#	k4
km²	km²	k?
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
krajiny	krajina	k1gFnSc2
<g/>
,	,	kIx,
geologických	geologický	k2eAgFnPc2d1
a	a	k8xC
botanických	botanický	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>