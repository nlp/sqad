<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
geomorfologické	geomorfologický	k2eAgFnSc6d1
jednotce	jednotka	k1gFnSc6
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgNnPc1d1
oblast	oblast	k1gFnSc4
Slavkovský	slavkovský	k2eAgInSc1d1
lesIUCN	lesIUCN	k?
kategorie	kategorie	k1gFnSc1
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Pohled	pohled	k1gInSc1
na	na	k7c4
lesZákladní	lesZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1974	#num#	k4
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
374	#num#	k4
<g/>
–	–	k?
<g/>
983	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
606	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInPc1
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
a	a	k8xC
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc1
</s>
<s>
www.slavkovskyles.ochranaprirody.cz	www.slavkovskyles.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
poloha	poloha	k1gFnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
je	být	k5eAaImIp3nS
hornaté	hornatý	k2eAgNnSc4d1
území	území	k1gNnSc4
parovinného	parovinný	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
mezi	mezi	k7c7
Tachovskou	tachovský	k2eAgFnSc7d1
brázdou	brázda	k1gFnSc7
<g/>
,	,	kIx,
Chebskou	chebský	k2eAgFnSc7d1
a	a	k8xC
Sokolovskou	sokolovský	k2eAgFnSc7d1
pánví	pánev	k1gFnSc7
<g/>
;	;	kIx,
má	mít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
tvar	tvar	k1gInSc4
trojúhelníku	trojúhelník	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
jeho	jeho	k3xOp3gInPc2
vrcholů	vrchol	k1gInPc2
leží	ležet	k5eAaImIp3nS
lázeňská	lázeňská	k1gFnSc1
města	město	k1gNnSc2
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
,	,	kIx,
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
a	a	k8xC
Františkovy	Františkův	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
o	o	k7c6
rozloze	rozloha	k1gFnSc6
asi	asi	k9
606	#num#	k4
km²	km²	k?
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
krajiny	krajina	k1gFnSc2
<g/>
,	,	kIx,
geologických	geologický	k2eAgFnPc2d1
a	a	k8xC
botanických	botanický	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
znaku	znak	k1gInSc6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
vzácná	vzácný	k2eAgFnSc1d1
arnika	arnika	k1gFnSc1
horská	horský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Arnica	Arnic	k2eAgFnSc1d1
montana	montana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
typická	typický	k2eAgFnSc1d1
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
oblast	oblast	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
pozadí	pozadí	k1gNnSc6
vrchů	vrch	k1gInPc2
Lesný	lesný	k2eAgMnSc1d1
<g/>
,	,	kIx,
Lysina	lysina	k1gFnSc1
a	a	k8xC
Kružný	Kružný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
CHKO	CHKO	kA
vede	vést	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
po	po	k7c6
spojnici	spojnice	k1gFnSc6
měst	město	k1gNnPc2
Karlovy	Karlův	k2eAgFnSc2d1
Vary	Vary	k1gInPc1
–	–	k?
Loket	loket	k1gInSc1
–	–	k?
Kynšperk	Kynšperk	k1gInSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
–	–	k?
Mariánské	mariánský	k2eAgFnSc2d1
Lázně	lázeň	k1gFnSc2
–	–	k?
Teplá	teplý	k2eAgFnSc1d1
–	–	k?
Andělská	andělský	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
–	–	k?
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsevernější	severní	k2eAgInSc1d3
bod	bod	k1gInSc1
je	být	k5eAaImIp3nS
nedaleko	nedaleko	k7c2
obce	obec	k1gFnSc2
Sedlečko	Sedlečka	k1gFnSc5
<g/>
,	,	kIx,
nejjižnější	jižní	k2eAgFnSc6d3
v	v	k7c6
obci	obec	k1gFnSc6
Výškov	Výškov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýchodnější	východní	k2eAgInSc1d3
bod	bod	k1gInSc1
je	být	k5eAaImIp3nS
pod	pod	k7c7
přírodní	přírodní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
Šemnická	Šemnický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
a	a	k8xC
nejzápadnější	západní	k2eAgFnSc1d3
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Manského	manský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
u	u	k7c2
Dolního	dolní	k2eAgInSc2d1
Žandova	Žandův	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejcennějším	cenný	k2eAgFnPc3d3
partiím	partie	k1gFnPc3
patří	patřit	k5eAaImIp3nS
Mnichovské	mnichovský	k2eAgInPc4d1
hadce	hadec	k1gInPc4
a	a	k8xC
Kladské	kladský	k2eAgFnPc4d1
rašeliny	rašelina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Příroda	příroda	k1gFnSc1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
pestrá	pestrý	k2eAgFnSc1d1
<g/>
:	:	kIx,
vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
např.	např.	kA
mokřadní	mokřadní	k2eAgFnPc4d1
louky	louka	k1gFnPc4
<g/>
,	,	kIx,
zachovalé	zachovalý	k2eAgFnPc1d1
bučiny	bučina	k1gFnPc1
a	a	k8xC
kaňony	kaňon	k1gInPc1
se	s	k7c7
skalními	skalní	k2eAgInPc7d1
útvary	útvar	k1gInPc7
v	v	k7c6
údolích	údolí	k1gNnPc6
řek	řeka	k1gFnPc2
Teplá	teplý	k2eAgFnSc1d1
a	a	k8xC
Ohře	Ohře	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejcennějším	cenný	k2eAgFnPc3d3
partiím	partie	k1gFnPc3
patří	patřit	k5eAaImIp3nS
Mnichovské	mnichovský	k2eAgInPc4d1
hadce	hadec	k1gInPc4
a	a	k8xC
Kladské	kladský	k2eAgFnPc4d1
rašeliny	rašelina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
s	s	k7c7
mírně	mírně	k6eAd1
chladným	chladný	k2eAgMnSc7d1
a	a	k8xC
mírně	mírně	k6eAd1
suchým	suchý	k2eAgNnSc7d1
létem	léto	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Delší	dlouhý	k2eAgInSc4d2
léto	léto	k1gNnSc1
a	a	k8xC
teplejší	teplý	k2eAgFnSc1d2
zima	zima	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
údolí	údolí	k1gNnSc6
Ohře	Ohře	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnPc4d3
polohy	poloha	k1gFnPc4
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
chladné	chladný	k2eAgFnSc6d1
klimatické	klimatický	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
s	s	k7c7
relativně	relativně	k6eAd1
delší	dlouhý	k2eAgFnSc7d2
zimou	zima	k1gFnSc7
a	a	k8xC
vyšší	vysoký	k2eAgFnSc7d2
sněhovou	sněhový	k2eAgFnSc7d1
pokrývkou	pokrývka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejchladnější	chladný	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
Lysiny	lysina	k1gFnSc2
(	(	kIx(
<g/>
982,5	982,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
Lesného	lesný	k2eAgInSc2d1
(	(	kIx(
<g/>
983,5	983,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
zároveň	zároveň	k6eAd1
nejvyšší	vysoký	k2eAgInSc1d3
dlouhodobý	dlouhodobý	k2eAgInSc1d1
průměrný	průměrný	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
úhrn	úhrn	k1gInSc1
srážek	srážka	k1gFnPc2
(	(	kIx(
<g/>
nad	nad	k7c4
900	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Měření	měření	k1gNnSc1
průměrných	průměrný	k2eAgFnPc2d1
srážek	srážka	k1gFnPc2
na	na	k7c6
Lysině	lysina	k1gFnSc6
za	za	k7c4
roky	rok	k1gInPc4
1990	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
vykazuje	vykazovat	k5eAaImIp3nS
téměř	téměř	k6eAd1
1000	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geologie	geologie	k1gFnSc1
a	a	k8xC
geomorfologie	geomorfologie	k1gFnSc1
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
část	část	k1gFnSc1
území	území	k1gNnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
geomorfologickém	geomorfologický	k2eAgInSc6d1
celku	celek	k1gInSc6
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
jihovýchodu	jihovýchod	k1gInSc2
v	v	k7c6
Tepelské	tepelský	k2eAgFnSc6d1
vrchovině	vrchovina	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
sem	sem	k6eAd1
zasahují	zasahovat	k5eAaImIp3nP
okrajové	okrajový	k2eAgFnSc3d1
části	část	k1gFnSc3
Doupovských	Doupovský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
sem	sem	k6eAd1
svým	svůj	k3xOyFgInSc7
okrajem	okraj	k1gInSc7
zasahuje	zasahovat	k5eAaImIp3nS
Sokolovská	sokolovský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
severozápadní	severozápadní	k2eAgFnSc1d1
Chebská	chebský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
je	být	k5eAaImIp3nS
sevřen	sevřít	k5eAaPmNgInS
částí	část	k1gFnSc7
Krušných	krušný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
Smrčinami	Smrčiny	k1gFnPc7
a	a	k8xC
Českým	český	k2eAgInSc7d1
lesem	les	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
a	a	k8xC
jihozápadě	jihozápad	k1gInSc6
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
strmě	strmě	k6eAd1
k	k	k7c3
východnímu	východní	k2eAgInSc3d1
okraji	okraj	k1gInSc3
neogenní	neogenní	k2eAgFnSc2d1
chebské	chebský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
převážně	převážně	k6eAd1
žulovými	žulový	k2eAgFnPc7d1
horninami	hornina	k1gFnPc7
<g/>
,	,	kIx,
tzv.	tzv.	kA
horského	horský	k2eAgInSc2d1
a	a	k8xC
krušnohorského	krušnohorský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
části	část	k1gFnSc6
jsou	být	k5eAaImIp3nP
metamorfované	metamorfovaný	k2eAgNnSc1d1
greiseny	greisen	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plášť	plášť	k1gInSc1
žuly	žula	k1gFnSc2
zde	zde	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
slabě	slabě	k6eAd1
granitizované	granitizovaný	k2eAgFnPc1d1
slavkovské	slavkovský	k2eAgFnPc1d1
pararuly	pararula	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
významným	významný	k2eAgMnPc3d1
patří	patřit	k5eAaImIp3nS
ještě	ještě	k6eAd1
horniny	hornina	k1gFnSc2
mariánskolázeňského	mariánskolázeňský	k2eAgInSc2d1
metabasitového	metabasitový	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
amfibolity	amfibolit	k1gInPc1
a	a	k8xC
serpentinity	serpentinit	k1gInPc1
(	(	kIx(
<g/>
hadce	hadec	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pruh	pruh	k1gInSc1
hadcových	hadcový	k2eAgFnPc2d1
hornin	hornina	k1gFnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
mezi	mezi	k7c7
Mariánskými	mariánský	k2eAgFnPc7d1
Lázněmi	lázeň	k1gFnPc7
<g/>
,	,	kIx,
Prameny	pramen	k1gInPc7
a	a	k8xC
Novou	Nová	k1gFnSc7
Vsí	ves	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
je	být	k5eAaImIp3nS
charakteristická	charakteristický	k2eAgFnSc1d1
značná	značný	k2eAgFnSc1d1
různorodost	různorodost	k1gFnSc1
hornin	hornina	k1gFnPc2
a	a	k8xC
přítomnost	přítomnost	k1gFnSc1
některých	některý	k3yIgInPc2
atraktivních	atraktivní	k2eAgInPc2d1
geologických	geologický	k2eAgInPc2d1
jevů	jev	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
řada	řada	k1gFnSc1
geologicky	geologicky	k6eAd1
podmíněných	podmíněný	k2eAgNnPc2d1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Svatošské	Svatošský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
<g/>
,	,	kIx,
hadcové	hadcový	k2eAgFnSc2d1
skály	skála	k1gFnSc2
Vlček	Vlček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neovulkanity	Neovulkanita	k1gFnPc1
jsou	být	k5eAaImIp3nP
roztroušeny	roztroušet	k5eAaImNgFnP,k5eAaPmNgFnP
po	po	k7c6
oblasti	oblast	k1gFnSc6
v	v	k7c6
několika	několik	k4yIc6
izolovaných	izolovaný	k2eAgInPc6d1
výchozech	výchoz	k1gInPc6
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
okolí	okolí	k1gNnSc6
Sedlečka	Sedlečka	k1gFnSc1
a	a	k8xC
Šemnic	Šemnice	k1gFnPc2
(	(	kIx(
<g/>
neovulkanity	neovulkanita	k1gFnPc1
Doupovských	Doupovský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
chráněny	chránit	k5eAaImNgFnP
v	v	k7c6
přírodní	přírodní	k2eAgFnSc6d1
památce	památka	k1gFnSc6
Šemnická	Šemnický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgInPc3d1
výrazným	výrazný	k2eAgInPc3d1
neovulkanitům	neovulkanit	k1gInPc3
patří	patřit	k5eAaImIp3nS
PP	PP	kA
Homolka	homolka	k1gFnSc1
(	(	kIx(
<g/>
698	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
Podhorní	podhorní	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
847,2	847,2	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tvořené	tvořený	k2eAgNnSc1d1
olivinickým	olivinický	k2eAgInSc7d1
nefelinitem	nefelinit	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čedič	čedič	k1gInSc1
se	s	k7c7
sloupcovitou	sloupcovitý	k2eAgFnSc7d1
odlučností	odlučnost	k1gFnSc7
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
na	na	k7c6
území	území	k1gNnSc6
PP	PP	kA
Čedičové	čedičový	k2eAgInPc1d1
varhany	varhany	k1gInPc4
u	u	k7c2
Hlinek	hlinka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
oblast	oblast	k1gFnSc4
jsou	být	k5eAaImIp3nP
charakteristické	charakteristický	k2eAgInPc1d1
výrony	výron	k1gInPc1
plynů	plyn	k1gInPc2
například	například	k6eAd1
PR	pr	k0
Smraďoch	smraďoch	k1gMnSc1
<g/>
,	,	kIx,
PP	PP	kA
Sirňák	Sirňák	k1gInSc1
<g/>
,	,	kIx,
PP	PP	kA
Milhostovské	Milhostovský	k2eAgFnPc4d1
mofety	mofeta	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
jsou	být	k5eAaImIp3nP
pozůstatkem	pozůstatek	k1gInSc7
třetihorního	třetihorní	k2eAgInSc2d1
vulkanismu	vulkanismus	k1gInSc2
a	a	k8xC
výskyt	výskyt	k1gInSc1
termálních	termální	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
minerálek	minerálka	k1gFnPc2
a	a	k8xC
kyselek	kyselka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
chráněném	chráněný	k2eAgNnSc6d1
území	území	k1gNnSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
1000	#num#	k4
ložisek	ložisko	k1gNnPc2
rašeliny	rašelina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hubský	Hubský	k2eAgInSc1d1
peň	peň	k1gInSc1
Krásno	krásno	k1gNnSc1
</s>
<s>
Oblast	oblast	k1gFnSc1
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
předmětem	předmět	k1gInSc7
dobývání	dobývání	k1gNnSc2
rud	ruda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
nepaměti	nepaměť	k1gFnSc2
byl	být	k5eAaImAgInS
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
spojován	spojovat	k5eAaImNgInS
s	s	k7c7
dobýváním	dobývání	k1gNnSc7
kvalitní	kvalitní	k2eAgFnSc2d1
cínové	cínový	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámější	známý	k2eAgFnSc1d3
je	on	k3xPp3gNnSc4
dobývání	dobývání	k1gNnSc4
bohatých	bohatý	k2eAgInPc2d1
cínových	cínový	k2eAgInPc2d1
a	a	k8xC
wolframových	wolframový	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
na	na	k7c6
greisenových	greisenův	k2eAgInPc6d1
pních	peň	k1gInPc6
v	v	k7c6
okolí	okolí	k1gNnSc6
Horního	horní	k2eAgInSc2d1
Slavkova	Slavkov	k1gInSc2
a	a	k8xC
Krásna	krásno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k9
krystaly	krystal	k1gInPc1
kassiteritu	kassiterit	k1gInSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgInSc2d1
minerálu	minerál	k1gInSc2
cínu	cín	k1gInSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
ve	v	k7c6
všech	všecek	k3xTgFnPc6
významných	významný	k2eAgFnPc6d1
mineralogických	mineralogický	k2eAgFnPc6d1
sbírkách	sbírka	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
v	v	k7c6
muzeích	muzeum	k1gNnPc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
i	i	k9
soukromých	soukromý	k2eAgFnPc6d1
sbírkách	sbírka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
povrchovým	povrchový	k2eAgInSc7d1
pozůstatkem	pozůstatek	k1gInSc7
po	po	k7c4
dobývání	dobývání	k1gNnSc4
rud	ruda	k1gFnPc2
cínu	cín	k1gInSc2
a	a	k8xC
wolframu	wolfram	k1gInSc2
je	být	k5eAaImIp3nS
propadlina	propadlina	k1gFnSc1
na	na	k7c6
tzv.	tzv.	kA
Hubském	Hubské	k1gNnSc6
pni	peň	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
menších	malý	k2eAgNnPc2d2
důlních	důlní	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
se	se	k3xPyFc4
tu	tu	k6eAd1
při	při	k7c6
velké	velký	k2eAgFnSc3d1
katastrofě	katastrofa	k1gFnSc3
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
důsledku	důsledek	k1gInSc6
značného	značný	k2eAgNnSc2d1
poddolování	poddolování	k1gNnSc2
celého	celý	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
hromadně	hromadně	k6eAd1
propadlo	propadnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
je	být	k5eAaImIp3nS
významnou	významný	k2eAgFnSc7d1
hornickou	hornický	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
dobývaní	dobývaný	k2eAgMnPc1d1
cínové	cínový	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
staré	starý	k2eAgNnSc4d1
důlní	důlní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
Jeroným	Jeroným	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stěnách	stěna	k1gFnPc6
chodeb	chodba	k1gFnPc2
s	s	k7c7
povlaky	povlak	k1gInPc7
sazí	saze	k1gFnPc2
po	po	k7c6
rozrušování	rozrušování	k1gNnSc6
horniny	hornina	k1gFnSc2
ohněm	oheň	k1gInSc7
(	(	kIx(
<g/>
tzv.	tzv.	kA
sázení	sázený	k2eAgMnPc1d1
ohněm	oheň	k1gInSc7
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
v	v	k7c6
žule	žula	k1gFnSc6
patrné	patrný	k2eAgFnSc2d1
stopy	stopa	k1gFnSc2
po	po	k7c6
želízku	želízko	k1gNnSc6
a	a	k8xC
mlátku	mlátek	k1gInSc6
<g/>
,	,	kIx,
nástrojích	nástroj	k1gInPc6
dávných	dávný	k2eAgMnPc2d1
horníků	horník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okolí	okolí	k1gNnSc6
Horního	horní	k2eAgInSc2d1
Slavkova	Slavkov	k1gInSc2
byl	být	k5eAaImAgInS
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
dobýván	dobýván	k2eAgInSc4d1
uran	uran	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
význačných	význačný	k2eAgInPc2d1
mineralogických	mineralogický	k2eAgInPc2d1
výskytů	výskyt	k1gInPc2
<g/>
,	,	kIx,
za	za	k7c4
zmínku	zmínka	k1gFnSc4
již	již	k6eAd1
vytěžené	vytěžený	k2eAgNnSc1d1
pegmatitové	pegmatitový	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
u	u	k7c2
Lázní	lázeň	k1gFnPc2
Kynžvartu	Kynžvart	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vyskytovalo	vyskytovat	k5eAaImAgNnS
mnoha	mnoho	k4c2
zajímavých	zajímavý	k2eAgInPc2d1
minerálů	minerál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
významným	významný	k2eAgInSc7d1
důlním	důlní	k2eAgInSc7d1
revírem	revír	k1gInSc7
byly	být	k5eAaImAgFnP
v	v	k7c6
minulosti	minulost	k1gFnSc6
Michalovy	Michalův	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
nejjižnějším	jižní	k2eAgInSc6d3
cípu	cíp	k1gInSc6
CHKO	CHKO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudní	rudní	k2eAgFnPc4d1
žíly	žíla	k1gFnPc4
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
jsou	být	k5eAaImIp3nP
geologicky	geologicky	k6eAd1
i	i	k9
parageneticky	parageneticky	k6eAd1
zajímavé	zajímavý	k2eAgNnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
dlouhá	dlouhý	k2eAgFnSc1d1
doba	doba	k1gFnSc1
od	od	k7c2
ukončení	ukončení	k1gNnSc2
důlní	důlní	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
způsobila	způsobit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc1
dříve	dříve	k6eAd2
významné	významný	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
dolování	dolování	k1gNnSc2
téměř	téměř	k6eAd1
upadlo	upadnout	k5eAaPmAgNnS
v	v	k7c4
zapomnění	zapomnění	k1gNnSc4
a	a	k8xC
pěkné	pěkný	k2eAgFnPc4d1
ukázky	ukázka	k1gFnPc4
rud	ruda	k1gFnPc2
niklu	nikl	k1gInSc2
<g/>
,	,	kIx,
kobaltu	kobalt	k1gInSc2
<g/>
,	,	kIx,
olova	olovo	k1gNnSc2
<g/>
,	,	kIx,
zinku	zinek	k1gInSc2
<g/>
,	,	kIx,
stříbra	stříbro	k1gNnSc2
a	a	k8xC
mědi	měď	k1gFnSc2
odtud	odtud	k6eAd1
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
vzácné	vzácný	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hydrologie	hydrologie	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
potok	potok	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
náleží	náležet	k5eAaImIp3nS
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
do	do	k7c2
povodí	povodí	k1gNnSc2
Ohře	Ohře	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
menší	malý	k2eAgFnSc2d2
části	část	k1gFnSc2
do	do	k7c2
povodí	povodí	k1gNnSc2
Mže	Mže	k1gFnSc2
a	a	k8xC
Střely	střela	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
Ohře	Ohře	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
převládající	převládající	k2eAgFnSc1d1
část	část	k1gFnSc1
území	území	k1gNnSc2
chráněné	chráněný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohře	Ohře	k1gFnSc1
vtéká	vtékat	k5eAaImIp3nS
do	do	k7c2
území	území	k1gNnSc2
CHKO	CHKO	kA
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
krátkým	krátký	k2eAgInSc7d1
úsekem	úsek	k1gInSc7
mezi	mezi	k7c7
Loktem	loket	k1gInSc7
a	a	k8xC
Doubím	doubí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
odvodňovacím	odvodňovací	k2eAgInSc7d1
tokem	tok	k1gInSc7
území	území	k1gNnSc2
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Teplá	teplý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pramení	pramenit	k5eAaImIp3nS
v	v	k7c6
mokřadech	mokřad	k1gInPc6
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
obce	obec	k1gFnPc1
Zádub	Záduba	k1gFnPc2
<g/>
–	–	k?
<g/>
Závišín	Závišína	k1gFnPc2
a	a	k8xC
ústí	ústit	k5eAaImIp3nS
do	do	k7c2
řeky	řeka	k1gFnSc2
Ohře	Ohře	k1gFnSc2
v	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
své	svůj	k3xOyFgFnSc6
cestě	cesta	k1gFnSc6
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
hluboké	hluboký	k2eAgNnSc4d1
kaňonovité	kaňonovitý	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnými	významný	k2eAgInPc7d1
přítoky	přítok	k1gInPc7
jsou	být	k5eAaImIp3nP
Pramenský	Pramenský	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
,	,	kIx,
Bečovský	Bečovský	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
,	,	kIx,
Otročínský	Otročínský	k2eAgInSc4d1
potok	potok	k1gInSc4
a	a	k8xC
Lomnický	lomnický	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
Teplé	Teplá	k1gFnSc2
byla	být	k5eAaImAgFnS
u	u	k7c2
Březové	březový	k2eAgFnSc2d1
vybudována	vybudován	k2eAgFnSc1d1
ve	v	k7c6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
přehradní	přehradní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
Březová	březový	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
dosud	dosud	k6eAd1
sloužící	sloužící	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
retenční	retenční	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
Karlových	Karlův	k2eAgInPc2d1
Varů	Vary	k1gInPc2
před	před	k7c7
povodněmi	povodeň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
umělým	umělý	k2eAgInSc7d1
vodním	vodní	k2eAgInSc7d1
dílem	díl	k1gInSc7
je	být	k5eAaImIp3nS
Dlouhá	dlouhý	k2eAgFnSc1d1
stoka	stoka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
systém	systém	k1gInSc4
vodních	vodní	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
vybudovaný	vybudovaný	k2eAgInSc4d1
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
především	především	k9
pro	pro	k7c4
pohon	pohon	k1gInSc4
důlních	důlní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
v	v	k7c4
Krásně	krásně	k6eAd1
a	a	k8xC
Horním	horní	k2eAgInSc6d1
Slavkově	Slavkov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Ohře	Ohře	k1gFnSc2
se	se	k3xPyFc4
vlévá	vlévat	k5eAaImIp3nS
v	v	k7c6
Lokti	loket	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
a	a	k8xC
Velká	velký	k2eAgFnSc1d1
Libava	Libava	k1gFnSc1
odvodňují	odvodňovat	k5eAaImIp3nP
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pramení	pramenit	k5eAaImIp3nS
u	u	k7c2
Lazů	Lazů	k?
a	a	k8xC
po	po	k7c6
vzájemném	vzájemný	k2eAgInSc6d1
soutoku	soutok	k1gInSc6
ústí	ústit	k5eAaImIp3nS
od	od	k7c2
Ohře	Ohře	k1gFnSc2
v	v	k7c6
Libavském	Libavský	k2eAgNnSc6d1
Údolí	údolí	k1gNnSc6
nedaleko	nedaleko	k7c2
Kynšperku	Kynšperk	k1gInSc2
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Libavě	Libava	k1gFnSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vybudována	vybudován	k2eAgFnSc1d1
pod	pod	k7c7
obcí	obec	k1gFnSc7
Rovná	rovnat	k5eAaImIp3nS
u	u	k7c2
zaniklé	zaniklý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Krásná	krásný	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
menší	malý	k2eAgFnSc1d2
vodárenská	vodárenský	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lipoltovský	Lipoltovský	k2eAgInSc1d1
potok	potok	k1gInSc1
protéká	protékat	k5eAaImIp3nS
na	na	k7c6
úpatí	úpatí	k1gNnSc6
výrazného	výrazný	k2eAgInSc2d1
Mariánskolázeňského	mariánskolázeňský	k2eAgInSc2d1
zlomu	zlom	k1gInSc2
a	a	k8xC
ústí	ústit	k5eAaImIp3nS
do	do	k7c2
řeky	řeka	k1gFnSc2
Odravy	Odrava	k1gFnSc2
u	u	k7c2
vsi	ves	k1gFnSc2
Obilná	obilný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejjižnější	jižní	k2eAgFnPc4d3
části	část	k1gFnPc4
oblasti	oblast	k1gFnSc2
odvodňuje	odvodňovat	k5eAaImIp3nS
povodí	povodí	k1gNnSc1
Mže	Mže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepatrnou	patrný	k2eNgFnSc4d1,k2eAgFnSc4d1
část	část	k1gFnSc4
oblasti	oblast	k1gFnSc2
odvodňuje	odvodňovat	k5eAaImIp3nS
Kosí	kosí	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
,	,	kIx,
vlévající	vlévající	k2eAgInPc4d1
se	se	k3xPyFc4
do	do	k7c2
Mže	Mže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
Střely	střela	k1gFnPc1
odvodňuje	odvodňovat	k5eAaImIp3nS
nepatrné	patrný	k2eNgNnSc1d1,k2eAgNnSc1d1
území	území	k1gNnSc1
při	při	k7c6
východní	východní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
chráněného	chráněný	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
fenoménu	fenomén	k1gInSc3
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
patří	patřit	k5eAaImIp3nS
vývěry	vývěr	k1gInPc4
léčivých	léčivý	k2eAgFnPc2d1
a	a	k8xC
minerálních	minerální	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
jsou	být	k5eAaImIp3nP
kromě	kromě	k7c2
jímaných	jímaný	k2eAgFnPc2d1
a	a	k8xC
jinak	jinak	k6eAd1
využívaných	využívaný	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
také	také	k9
přírodní	přírodní	k2eAgInPc4d1
vývěry	vývěr	k1gInPc4
minerálních	minerální	k2eAgFnPc2d1
vod	voda	k1gFnPc2
zvané	zvaný	k2eAgFnSc2d1
divoké	divoký	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
registrováno	registrovat	k5eAaBmNgNnS
více	hodně	k6eAd2
než	než	k8xS
1000	#num#	k4
výronů	výron	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Flóra	Flóra	k1gFnSc1
a	a	k8xC
fauna	fauna	k1gFnSc1
</s>
<s>
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
je	být	k5eAaImIp3nS
z	z	k7c2
botanického	botanický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
zajímavým	zajímavý	k2eAgNnSc7d1
a	a	k8xC
pestrým	pestrý	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvozdík	hvozdík	k1gInSc1
pyšný	pyšný	k2eAgInSc1d1
v	v	k7c6
PR	pr	k0
Mokřady	mokřad	k1gInPc4
pod	pod	k7c7
Vlčkem	vlček	k1gInSc7
Prha	prha	k1gFnSc1
arnika	arnika	k1gFnSc1
v	v	k7c6
CHKO	CHKO	kA
v	v	k7c6
červnu	červen	k1gInSc6
2014	#num#	k4
Kromě	kromě	k7c2
charakteristických	charakteristický	k2eAgNnPc2d1
skalních	skalní	k2eAgNnPc2d1
společenstev	společenstvo	k1gNnPc2
a	a	k8xC
rozsáhlého	rozsáhlý	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
rašelinišť	rašeliniště	k1gNnPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
různých	různý	k2eAgInPc2d1
typů	typ	k1gInPc2
pramenišť	prameniště	k1gNnPc2
<g/>
,	,	kIx,
lučních	luční	k2eAgInPc2d1
porostů	porost	k1gInPc2
a	a	k8xC
pastvin	pastvina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
dobře	dobře	k6eAd1
zachovalá	zachovalý	k2eAgNnPc4d1
společenstva	společenstvo	k1gNnPc4
vřesovišť	vřesoviště	k1gNnPc2
<g/>
,	,	kIx,
vodní	vodní	k2eAgFnSc2d1
a	a	k8xC
bažinné	bažinný	k2eAgFnSc2d1
vegetace	vegetace	k1gFnSc2
a	a	k8xC
také	také	k9
společenstva	společenstvo	k1gNnPc4
původních	původní	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lužních	lužní	k2eAgInPc6d1
lesích	les	k1gInPc6
můžeme	moct	k5eAaImIp1nP
z	z	k7c2
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
najít	najít	k5eAaPmF
oměj	oměj	k1gInSc1
pestrý	pestrý	k2eAgInSc1d1
(	(	kIx(
<g/>
Aconitum	Aconitum	k1gNnSc1
variegatum	variegatum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
oměj	oměj	k1gInSc4
vlčí	vlčí	k2eAgInSc1d1
mor	mor	k1gInSc1
(	(	kIx(
<g/>
Aconitum	Aconitum	k1gNnSc1
lycoctonum	lycoctonum	k1gInSc1
ssp	ssp	k?
<g/>
.	.	kIx.
lycoctonum	lycoctonum	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
doubravách	doubrava	k1gFnPc6
např.	např.	kA
lilii	lilie	k1gFnSc4
zlatohlavou	zlatohlavý	k2eAgFnSc7d1
(	(	kIx(
<g/>
Lilium	lilium	k1gNnSc1
martagon	martagona	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
hadcových	hadcový	k2eAgFnPc6d1
skalkách	skalka	k1gFnPc6
řadu	řad	k1gInSc2
specifických	specifický	k2eAgInPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
nepříliš	příliš	k6eNd1
častých	častý	k2eAgInPc2d1
druhů	druh	k1gInPc2
jako	jako	k8xS,k8xC
např.	např.	kA
vřesovec	vřesovec	k1gInSc1
pleťový	pleťový	k2eAgInSc1d1
(	(	kIx(
<g/>
Erica	Eric	k2eAgFnSc1d1
carnea	carnea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zimostrázek	zimostrázek	k1gInSc4
alpský	alpský	k2eAgInSc4d1
(	(	kIx(
<g/>
Polygala	Polygala	k1gFnSc1
chamaebuxus	chamaebuxus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
vzácných	vzácný	k2eAgInPc2d1
endemitů	endemit	k1gInPc2
rožce	rožec	k1gInSc2
kuřičkolistého	kuřičkolistý	k2eAgInSc2d1
(	(	kIx(
<g/>
Cerastium	Cerastium	k1gNnSc1
alsinifolium	alsinifolium	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
chrastavce	chrastavka	k1gFnSc3
rolního	rolní	k2eAgNnSc2d1
hadcového	hadcový	k2eAgNnSc2d1
(	(	kIx(
<g/>
Knautia	Knautium	k1gNnSc2
arvensis	arvensis	k1gFnSc2
ssp	ssp	k?
<g/>
.	.	kIx.
serpentinicola	serpentinicola	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
vzácné	vzácný	k2eAgFnPc4d1
hadcové	hadcový	k2eAgFnPc4d1
kapradinky	kapradinka	k1gFnPc4
sleziník	sleziník	k1gInSc1
nepravý	pravý	k2eNgInSc1d1
(	(	kIx(
<g/>
Asplenium	Asplenium	k1gNnSc1
adulterinum	adulterinum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
sleziník	sleziník	k1gInSc1
hadcový	hadcový	k2eAgInSc1d1
(	(	kIx(
<g/>
Asplenium	Asplenium	k1gNnSc1
cuneifolium	cuneifolium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
rašeliništích	rašeliniště	k1gNnPc6
suchopýr	suchopýr	k1gInSc1
pochvatý	pochvatý	k2eAgInSc1d1
(	(	kIx(
<g/>
Eriophorum	Eriophorum	k1gInSc1
vaginatum	vaginatum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
loukách	louka	k1gFnPc6
roste	růst	k5eAaImIp3nS
kosatec	kosatec	k1gInSc1
sibiřský	sibiřský	k2eAgInSc1d1
(	(	kIx(
<g/>
Iris	iris	k1gInSc1
sibirica	sibiric	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hořeček	hořeček	k1gInSc4
drsný	drsný	k2eAgInSc4d1
(	(	kIx(
<g/>
Gentianella	Gentianella	k1gFnSc1
obtusifolia	obtusifolia	k1gFnSc1
Sturmiana	Sturmiana	k1gFnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
hořec	hořec	k1gInSc1
hořepník	hořepník	k1gInSc1
(	(	kIx(
<g/>
Gentiana	Gentiana	k1gFnSc1
pneumonanthe	pneumonanth	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
upolín	upolín	k1gInSc1
evropský	evropský	k2eAgInSc1d1
(	(	kIx(
<g/>
Trollius	Trollius	k1gInSc1
altissimus	altissimus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prstnatec	prstnatec	k1gMnSc1
májový	májový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Dactylorhiza	Dactylorhiza	k1gFnSc1
majalis	majalis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vemeník	vemeník	k1gInSc4
dvoulistý	dvoulistý	k2eAgInSc4d1
(	(	kIx(
<g/>
Platanthera	Platanthera	k1gFnSc1
bifolia	bifolia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
rašeliništích	rašeliniště	k1gNnPc6
můžeme	moct	k5eAaImIp1nP
ze	z	k7c2
zajímavých	zajímavý	k2eAgInPc2d1
druhů	druh	k1gInPc2
zde	zde	k6eAd1
spatřit	spatřit	k5eAaPmF
všivec	všivec	k1gInSc4
bahenní	bahenní	k2eAgInSc4d1
(	(	kIx(
<g/>
Pedicularis	Pedicularis	k1gInSc4
palustris	palustris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vachtu	vachta	k1gFnSc4
trojlistou	trojlistý	k2eAgFnSc4d1
(	(	kIx(
<g/>
Menyanthes	Menyanthes	k1gMnSc1
trifoliata	trifoliat	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kruštík	kruštík	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
(	(	kIx(
<g/>
Epipactis	Epipactis	k1gInSc1
palustris	palustris	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
vzácný	vzácný	k2eAgInSc4d1
ostřic	ostřice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc1d1
žije	žít	k5eAaImIp3nS
řada	řada	k1gFnSc1
vzácných	vzácný	k2eAgInPc2d1
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ptáků	pták	k1gMnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
pravidelný	pravidelný	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
čápa	čáp	k1gMnSc2
černého	černý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Ciconia	Ciconium	k1gNnSc2
nigra	nigr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
datla	datel	k1gMnSc4
černého	černý	k1gMnSc4
(	(	kIx(
<g/>
Dryocopus	Dryocopus	k1gMnSc1
martius	martius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jestřába	jestřáb	k1gMnSc2
lesního	lesní	k2eAgMnSc2d1
(	(	kIx(
<g/>
Accipiter	Accipiter	k1gInSc1
gentilis	gentilis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
krahujce	krahujec	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
(	(	kIx(
<g/>
Accipiter	Accipiter	k1gInSc1
nisus	nisus	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Vzácné	vzácný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
několikeré	několikerý	k4xRyIgNnSc1
pozorování	pozorování	k1gNnSc1
dudka	dudek	k1gMnSc2
chocholatého	chocholatý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Upupa	Upupa	k1gFnSc1
epops	epopsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přežívá	přežívat	k5eAaImIp3nS
zde	zde	k6eAd1
nepočetná	početný	k2eNgFnSc1d1
populace	populace	k1gFnSc1
tetřívka	tetřívek	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
(	(	kIx(
<g/>
Tetrao	Tetrao	k1gNnSc1
tetrix	tetrix	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mokřady	mokřad	k1gInPc4
obývá	obývat	k5eAaImIp3nS
chřástal	chřástal	k1gMnSc1
polní	polní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Crex	Crex	k1gInSc1
crex	crex	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
skalní	skalní	k2eAgFnSc4d1
údolí	údolí	k1gNnSc3
výr	výr	k1gMnSc1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Bubo	Bubo	k1gMnSc1
bubo	bubo	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staré	Staré	k2eAgInPc1d1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
nepřístupné	přístupný	k2eNgFnSc2d1
štoly	štola	k1gFnSc2
jsou	být	k5eAaImIp3nP
významnými	významný	k2eAgNnPc7d1
zimovišti	zimoviště	k1gNnPc7
pro	pro	k7c4
řadu	řada	k1gFnSc4
druhů	druh	k1gInPc2
netopýrů	netopýr	k1gMnPc2
<g/>
,	,	kIx,
především	především	k9
netopýra	netopýr	k1gMnSc2
velkého	velký	k2eAgMnSc2d1
(	(	kIx(
<g/>
Myotis	Myotis	k1gInSc1
myotis	myotis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netopýra	netopýr	k1gMnSc2
ušatého	ušatý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Plecotus	Plecotus	k1gInSc1
auritus	auritus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
netopýra	netopýr	k1gMnSc2
vodního	vodní	k2eAgMnSc2d1
(	(	kIx(
<g/>
Myotis	Myotis	k1gInSc1
daubentonii	daubentonie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netopýra	netopýr	k1gMnSc2
řasnatého	řasnatý	k2eAgMnSc2d1
(	(	kIx(
<g/>
Myotis	Myotis	k1gInSc1
nattereri	natterer	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netopýra	netopýr	k1gMnSc2
Brandtova	Brandtův	k2eAgMnSc2d1
(	(	kIx(
<g/>
Myotis	Myotis	k1gInSc1
brandtii	brandtie	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
netopýra	netopýr	k1gMnSc2
černého	černý	k1gMnSc2
(	(	kIx(
<g/>
Barbastella	Barbastella	k1gMnSc1
barbastellus	barbastellus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
dalších	další	k2eAgInPc2d1
živočišných	živočišný	k2eAgInPc2d1
druhů	druh	k1gInPc2
je	být	k5eAaImIp3nS
chráněn	chráněn	k2eAgMnSc1d1
např.	např.	kA
hnědásek	hnědásek	k1gMnSc1
chrastavcový	chrastavcový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Euphydryas	Euphydryas	k1gInSc1
aurinia	aurinium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žluťásek	žluťásek	k1gInSc4
borůvkový	borůvkový	k2eAgInSc4d1
(	(	kIx(
<g/>
Colias	Colias	k1gInSc4
palaeno	palaen	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vážka	vážka	k1gFnSc1
běloústá	běloústý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Leucorrhinia	Leucorrhinium	k1gNnPc1
albifrons	albifronsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vranka	vranka	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Cottus	Cottus	k1gMnSc1
gobio	gobio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čáp	čáp	k1gMnSc1
černý	černý	k1gMnSc1
(	(	kIx(
<g/>
Ciconia	Ciconium	k1gNnSc2
nigra	nigr	k1gMnSc2
<g/>
)	)	kIx)
či	či	k8xC
sysel	sysel	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Spermophilus	Spermophilus	k1gMnSc1
cittelus	cittelus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Lilie	lilie	k1gFnSc1
zlatohlavá	zlatohlavý	k2eAgFnSc1d1
v	v	k7c6
červenci	červenec	k1gInSc6
2009	#num#	k4
v	v	k7c6
PP	PP	kA
Homolka	Homolka	k1gMnSc1
</s>
<s>
Kosatec	kosatec	k1gInSc1
sibiřský	sibiřský	k2eAgInSc1d1
v	v	k7c6
NPP	NPP	kA
Upolínová	Upolínový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
pod	pod	k7c7
Křížky	křížek	k1gInPc7
v	v	k7c6
červnu	červen	k1gInSc6
2013	#num#	k4
</s>
<s>
Hořeček	Hořeček	k1gInSc1
drsný	drsný	k2eAgInSc1d1
Sturmův	Sturmův	k2eAgInSc1d1
v	v	k7c6
PP	PP	kA
Hořečková	Hořečkový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
na	na	k7c6
Pile	pila	k1gFnSc6
v	v	k7c6
srpnu	srpen	k1gInSc6
2014	#num#	k4
</s>
<s>
Chlupáček	chlupáček	k1gInSc1
oranžový	oranžový	k2eAgInSc1d1
pod	pod	k7c7
Vlčkem	vlček	k1gInSc7
nad	nad	k7c7
obcí	obec	k1gFnSc7
Prameny	pramen	k1gInPc7
v	v	k7c6
srpnu	srpen	k1gInSc6
2014	#num#	k4
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
(	(	kIx(
<g/>
NPR	NPR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tři	tři	k4xCgFnPc1
národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
(	(	kIx(
<g/>
NPP	NPP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
(	(	kIx(
<g/>
PR	pr	k0
<g/>
)	)	kIx)
a	a	k8xC
přírodních	přírodní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
(	(	kIx(
<g/>
PP	PP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgNnSc1d1
území	území	k1gNnPc1
se	se	k3xPyFc4
připravují	připravovat	k5eAaImIp3nP
k	k	k7c3
vyhlášení	vyhlášení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
</s>
<s>
NPR	NPR	kA
Kladské	kladský	k2eAgFnSc2d1
rašeliny	rašelina	k1gFnSc2
</s>
<s>
NPR	NPR	kA
Pluhův	Pluhův	k2eAgInSc4d1
bor	bor	k1gInSc4
</s>
<s>
Národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
NPP	NPP	kA
Křížky	křížek	k1gInPc1
</s>
<s>
NPP	NPP	kA
Upolínová	Upolínový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
NPP	NPP	kA
Svatošské	Svatošský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
PR	pr	k0
Holina	holina	k1gFnSc1
</s>
<s>
PR	pr	k0
Rašeliniště	rašeliniště	k1gNnPc4
u	u	k7c2
myslivny	myslivna	k1gFnSc2
</s>
<s>
PR	pr	k0
Údolí	údolí	k1gNnSc6
Teplé	Teplé	k2eAgInPc1d1
</s>
<s>
PR	pr	k0
Smraďoch	smraďoch	k1gMnSc1
</s>
<s>
PR	pr	k0
Vlček	Vlček	k1gMnSc1
</s>
<s>
PR	pr	k0
Lazurový	lazurový	k2eAgInSc4d1
vrch	vrch	k1gInSc4
</s>
<s>
PR	pr	k0
Podhorní	podhorní	k2eAgFnSc1d1
vrch	vrch	k1gInSc4
</s>
<s>
PR	pr	k0
Mokřady	mokřad	k1gInPc4
pod	pod	k7c7
Vlčkem	vlček	k1gInSc7
</s>
<s>
PR	pr	k0
Planý	planý	k2eAgMnSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
PR	pr	k0
Prameniště	prameniště	k1gNnSc2
Teplé	Teplé	k2eAgInPc1d1
</s>
<s>
PR	pr	k0
Žižkův	Žižkův	k2eAgMnSc1d1
vrch	vrch	k1gInSc4
</s>
<s>
PR	pr	k0
Hloubek	hloubek	k1gInSc1
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
PP	PP	kA
Čedičové	čedičový	k2eAgFnPc4d1
varhany	varhany	k1gFnPc4
u	u	k7c2
Hlinek	hlinka	k1gFnPc2
</s>
<s>
PP	PP	kA
Čertkus	čertkus	k1gInSc1
nová	nový	k2eAgNnPc4d1
PP	PP	kA
<g/>
,	,	kIx,
vyhlášená	vyhlášený	k2eAgFnSc1d1
v	v	k7c6
lednu	leden	k1gInSc6
2014	#num#	k4
</s>
<s>
PP	PP	kA
Čiperka	čiperka	k1gFnSc1
</s>
<s>
PP	PP	kA
Dominova	Dominův	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
</s>
<s>
PP	PP	kA
Homolka	Homolka	k1gMnSc1
</s>
<s>
PP	PP	kA
Hořečková	Hořečková	k1gFnSc1
louka	louka	k1gFnSc1
na	na	k7c6
Pile	pila	k1gFnSc6
</s>
<s>
PP	PP	kA
Koňský	koňský	k2eAgInSc4d1
pramen	pramen	k1gInSc4
</s>
<s>
PP	PP	kA
Kounické	Kounický	k2eAgFnPc4d1
louky	louka	k1gFnPc4
</s>
<s>
PP	PP	kA
Kynžvartský	kynžvartský	k2eAgInSc4d1
kámen	kámen	k1gInSc4
</s>
<s>
PP	PP	kA
Milhostovské	Milhostovský	k2eAgFnSc2d1
mofety	mofeta	k1gFnSc2
</s>
<s>
PP	PP	kA
Moučné	moučný	k2eAgInPc1d1
pytle	pytel	k1gInPc1
</s>
<s>
PP	PP	kA
Pístovská	Pístovský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
PP	PP	kA
Podhorní	podhorní	k2eAgFnSc6d1
slatě	slata	k1gFnSc6
</s>
<s>
PP	PP	kA
Sirňák	Sirňák	k1gMnSc1
</s>
<s>
PP	PP	kA
Šemnická	Šemnický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
PP	PP	kA
Těšovské	Těšovský	k2eAgFnPc4d1
pastviny	pastvina	k1gFnPc4
</s>
<s>
PP	PP	kA
Velikonoční	velikonoční	k2eAgInSc4d1
rybník	rybník	k1gInSc4
</s>
<s>
PP	PP	kA
Na	na	k7c6
Vážkách	vážka	k1gFnPc6
</s>
<s>
V	v	k7c6
působnosti	působnost	k1gFnSc6
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
jsou	být	k5eAaImIp3nP
dále	daleko	k6eAd2
NPR	NPR	kA
Soos	Soosa	k1gFnPc2
<g/>
,	,	kIx,
NPR	NPR	kA
Rolavská	Rolavský	k2eAgNnPc1d1
vrchoviště	vrchoviště	k1gNnPc1
(	(	kIx(
<g/>
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
jsou	být	k5eAaImIp3nP
dřívější	dřívější	k2eAgInSc4d1
NPR	NPR	kA
Velké	velká	k1gFnSc2
jeřábí	jeřábí	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
a	a	k8xC
Velký	velký	k2eAgInSc1d1
močál	močál	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
NPP	NPP	kA
Komorní	komorní	k2eAgFnSc1d1
hůrka	hůrka	k1gFnSc1
<g/>
,	,	kIx,
NPP	NPP	kA
Železná	železný	k2eAgFnSc1d1
hůrka	hůrka	k1gFnSc1
a	a	k8xC
NPP	NPP	kA
Lužní	lužní	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
však	však	k9
nacházejí	nacházet	k5eAaImIp3nP
mimo	mimo	k7c4
CHKO	CHKO	kA
<g/>
.	.	kIx.
</s>
<s>
Turismus	turismus	k1gInSc1
</s>
<s>
Území	území	k1gNnSc1
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
je	být	k5eAaImIp3nS
výletním	výletní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
mnoha	mnoho	k4c2
mariánskolázeňských	mariánskolázeňský	k2eAgMnPc2d1
hostů	host	k1gMnPc2
a	a	k8xC
turistů	turist	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
navštěvována	navštěvován	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Kladské	kladský	k2eAgFnSc2d1
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Smraďoch	smraďoch	k1gMnSc1
<g/>
,	,	kIx,
národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Upolínová	Upolínový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
pod	pod	k7c7
Křížky	křížek	k1gInPc7
a	a	k8xC
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Geologický	geologický	k2eAgInSc1d1
park	park	k1gInSc1
v	v	k7c6
Mariánských	mariánský	k2eAgFnPc6d1
Lázních	lázeň	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
karlovarských	karlovarský	k2eAgFnPc2d1
lázeňských	lázeňská	k1gFnPc2
hostů	host	k1gMnPc2
a	a	k8xC
turistů	turist	k1gMnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
především	především	k6eAd1
národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Svatoššské	Svatoššský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
lokalitám	lokalita	k1gFnPc3
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
lázeňských	lázeňský	k2eAgNnPc2d1
měst	město	k1gNnPc2
vedou	vést	k5eAaImIp3nP
značené	značený	k2eAgFnPc4d1
turistické	turistický	k2eAgFnPc4d1
trasy	trasa	k1gFnPc4
i	i	k8xC
naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
příhraniční	příhraniční	k2eAgFnSc3d1
poloze	poloha	k1gFnSc3
Slavkovského	slavkovský	k2eAgInSc2d1
lesa	les	k1gInSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
umístění	umístění	k1gNnSc4
uvnitř	uvnitř	k7c2
západočeského	západočeský	k2eAgInSc2d1
lázeňského	lázeňský	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
značný	značný	k2eAgInSc1d1
příliv	příliv	k1gInSc1
zahraničních	zahraniční	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
z	z	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmto	tento	k3xDgFnPc3
okolnostem	okolnost	k1gFnPc3
jsou	být	k5eAaImIp3nP
i	i	k9
přizpůsobeny	přizpůsoben	k2eAgFnPc4d1
informační	informační	k2eAgFnPc4d1
tabule	tabule	k1gFnPc4
s	s	k7c7
překlady	překlad	k1gInPc7
do	do	k7c2
německého	německý	k2eAgMnSc2d1
<g/>
,	,	kIx,
někde	někde	k6eAd1
i	i	k9
anglického	anglický	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
</s>
<s>
Bečovská	Bečovský	k2eAgFnSc1d1
botanická	botanický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
</s>
<s>
Bečovská	Bečovský	k2eAgFnSc1d1
panoramatická	panoramatický	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Dolování	dolování	k1gNnSc1
v	v	k7c6
okolí	okolí	k1gNnSc6
Michalových	Michalových	k2eAgFnPc2d1
hor	hora	k1gFnPc2
</s>
<s>
Geologický	geologický	k2eAgInSc1d1
park	park	k1gInSc1
v	v	k7c6
Mariánských	mariánský	k2eAgFnPc6d1
Lázních	lázeň	k1gFnPc6
</s>
<s>
Lázeňskými	lázeňský	k2eAgInPc7d1
lesy	les	k1gInPc7
okolo	okolo	k7c2
Mariánských	mariánský	k2eAgFnPc2d1
Lázní	lázeň	k1gFnPc2
</s>
<s>
Kladské	kladský	k2eAgFnPc1d1
rašeliny	rašelina	k1gFnPc1
</s>
<s>
Kynžvartské	kynžvartský	k2eAgFnPc1d1
kyselky	kyselka	k1gFnPc1
</s>
<s>
Milhostovské	Milhostovský	k2eAgFnPc1d1
mofety	mofeta	k1gFnPc1
</s>
<s>
Mnichovské	mnichovský	k2eAgInPc1d1
hadce	hadec	k1gInPc1
</s>
<s>
Smraďoch	smraďoch	k1gMnSc1
</s>
<s>
Svatošské	Svatošský	k2eAgFnPc1d1
skály	skála	k1gFnPc1
</s>
<s>
Šibeniční	šibeniční	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Zaniklé	zaniklý	k2eAgFnPc1d1
obce	obec	k1gFnPc1
na	na	k7c6
Březovsku	Březovsko	k1gNnSc6
</s>
<s>
Siardův	Siardův	k2eAgInSc4d1
pramen	pramen	k1gInSc4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeňsko	Plzeňsko	k1gNnSc1
a	a	k8xC
Karlovarsko	Karlovarsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc3
Jiří	Jiří	k1gMnSc1
Zahradnický	zahradnický	k2eAgMnSc1d1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Mackovčin	Mackovčin	k2eAgMnSc1d1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
XI	XI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86064	#num#	k4
<g/>
-	-	kIx~
<g/>
68	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
498	#num#	k4
<g/>
–	–	k?
<g/>
514	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KRÁM	krám	k1gInSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hydrologická	hydrologický	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
dlouhodobě	dlouhodobě	k6eAd1
monitorovaného	monitorovaný	k2eAgNnSc2d1
povodí	povodí	k1gNnSc2
Lysina	lysina	k1gFnSc1
–	–	k?
abstrakt	abstrakt	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
261	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
geologie	geologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POŠMOURNÝ	pošmourný	k2eAgInSc1d1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geologie	geologie	k1gFnSc1
chráněných	chráněný	k2eAgFnPc2d1
krajinných	krajinný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
území	území	k1gNnSc2
ČR	ČR	kA
–	–	k?
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7075	#num#	k4
<g/>
-	-	kIx~
<g/>
584	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
vodopis	vodopis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOLÁŘOVÁ	Kolářová	k1gFnSc1
<g/>
,	,	kIx,
Margita	Margita	k1gFnSc1
<g/>
;	;	kIx,
MYSLIL	myslit	k5eAaImAgMnS
<g/>
,	,	kIx,
Vlastimil	Vlastimil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minerální	minerální	k2eAgFnPc4d1
vody	voda	k1gFnPc4
Západočeského	západočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústřední	ústřední	k2eAgInSc1d1
ústav	ústav	k1gInSc1
geologický	geologický	k2eAgInSc1d1
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Koncepce	koncepce	k1gFnSc1
terénního	terénní	k2eAgNnSc2d1
informačního	informační	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mariánské	mariánský	k2eAgFnPc1d1
Lázně	lázeň	k1gFnPc1
<g/>
:	:	kIx,
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
v	v	k7c6
okrese	okres	k1gInSc6
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc4d1
les	les	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Na	na	k7c6
webu	web	k1gInSc6
Správy	správa	k1gFnSc2
CHKO	CHKO	kA
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
Na	na	k7c6
webu	web	k1gInSc6
Základní	základní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Českého	český	k2eAgInSc2d1
svazu	svaz	k1gInSc2
ochránců	ochránce	k1gMnPc2
přírody	příroda	k1gFnSc2
Kladská	kladský	k2eAgNnPc4d1
</s>
<s>
Naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Cheb	Cheb	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kladské	kladský	k2eAgFnPc1d1
rašeliny	rašelina	k1gFnPc1
•	•	k?
Soos	Soos	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bublák	bublák	k1gMnSc1
a	a	k8xC
niva	niva	k1gFnSc1
Plesné	plesný	k2eAgFnSc2d1
•	•	k?
Bystřina	bystřina	k1gFnSc1
–	–	k?
Lužní	lužní	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Komorní	komorní	k2eAgFnSc1d1
hůrka	hůrka	k1gFnSc1
•	•	k?
Křížky	křížek	k1gInPc1
•	•	k?
Upolínová	Upolínový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
pod	pod	k7c7
Křížky	křížek	k1gInPc7
•	•	k?
Železná	železný	k2eAgFnSc1d1
hůrka	hůrka	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Amerika	Amerika	k1gFnSc1
•	•	k?
Děvín	Děvín	k1gInSc1
•	•	k?
Hamrnický	Hamrnický	k2eAgInSc1d1
mokřad	mokřad	k1gInSc1
•	•	k?
Holina	holina	k1gFnSc1
•	•	k?
Kosatcová	kosatcový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Lipovka	Lipovka	k1gFnSc1
•	•	k?
Mechové	mechový	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Mokřady	mokřad	k1gInPc1
pod	pod	k7c7
Vlčkem	vlček	k1gInSc7
•	•	k?
Planý	planý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Podhorní	podhorní	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Pomezní	pomezní	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Prameniště	prameniště	k1gNnSc1
Teplé	Teplá	k1gFnSc2
•	•	k?
Rathsam	Rathsam	k1gInSc1
•	•	k?
Smraďoch	smraďoch	k1gMnSc1
•	•	k?
Stráň	stráň	k1gFnSc1
u	u	k7c2
Dubiny	dubina	k1gFnSc2
•	•	k?
Studna	studna	k1gFnSc1
u	u	k7c2
Lužné	Lužná	k1gFnSc2
•	•	k?
U	u	k7c2
Sedmi	sedm	k4xCc2
rybníků	rybník	k1gInPc2
•	•	k?
Údolí	údolí	k1gNnSc4
Teplé	Teplá	k1gFnSc2
•	•	k?
Vlček	vlček	k1gInSc1
•	•	k?
Ztracený	ztracený	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Žižkův	Žižkův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Čertkus	čertkus	k1gInSc1
•	•	k?
Goethova	Goethova	k1gFnSc1
skalka	skalka	k1gFnSc1
•	•	k?
Koňský	koňský	k2eAgInSc1d1
pramen	pramen	k1gInSc1
•	•	k?
Kynžvartský	kynžvartský	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Milhostovské	Milhostovský	k2eAgFnSc2d1
mofety	mofeta	k1gFnSc2
•	•	k?
Milhostovské	Milhostovský	k2eAgFnSc2d1
mofety	mofeta	k1gFnSc2
•	•	k?
Sirňák	Sirňák	k1gMnSc1
•	•	k?
Těšovské	Těšovský	k2eAgFnPc1d1
pastviny	pastvina	k1gFnPc1
•	•	k?
U	u	k7c2
cihelny	cihelna	k1gFnSc2
•	•	k?
Vernéřovské	Vernéřovský	k2eAgInPc4d1
doly	dol	k1gInPc4
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Horní	horní	k2eAgFnSc1d1
Střela	střela	k1gFnSc1
•	•	k?
Stráž	stráž	k1gFnSc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
•	•	k?
Zlatý	zlatý	k2eAgInSc1d1
kopec	kopec	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Božídarské	božídarský	k2eAgNnSc1d1
rašeliniště	rašeliniště	k1gNnSc1
•	•	k?
Nebesa	nebesa	k1gNnPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Skalky	skalka	k1gFnPc1
skřítků	skřítek	k1gMnPc2
•	•	k?
Svatošské	Svatošský	k2eAgFnPc1d1
skály	skála	k1gFnPc1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Hloubek	hloubek	k1gInSc1
•	•	k?
Chlum	chlum	k1gInSc1
•	•	k?
Malé	Malá	k1gFnSc2
jeřábí	jeřábí	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
•	•	k?
Oceán	oceán	k1gInSc4
•	•	k?
Ostrovské	ostrovský	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
•	•	k?
Ryžovna	ryžovna	k1gFnSc1
•	•	k?
Údolí	údolí	k1gNnSc1
Teplé	Teplá	k1gFnSc2
•	•	k?
Vladař	vladař	k1gMnSc1
Přírodní	přírodní	k2eAgMnSc1d1
památky	památka	k1gFnPc4
</s>
<s>
Blažejský	Blažejský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Čedičová	čedičový	k2eAgFnSc1d1
žíla	žíla	k1gFnSc1
Boč	bočit	k5eAaImRp2nS
•	•	k?
Čedičové	čedičový	k2eAgFnPc4d1
varhany	varhany	k1gFnPc4
u	u	k7c2
Hlinek	hlinka	k1gFnPc2
•	•	k?
Dubohabřina	Dubohabřina	k1gFnSc1
ve	v	k7c6
Vojkovicích	Vojkovice	k1gFnPc6
•	•	k?
Homolka	Homolka	k1gMnSc1
•	•	k?
Hornohradský	Hornohradský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Hořečková	Hořečková	k1gFnSc1
louka	louka	k1gFnSc1
na	na	k7c6
Pile	pila	k1gFnSc6
•	•	k?
Jezerský	Jezerský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Koňský	koňský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Kounické	Kounický	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Lomnický	lomnický	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Louky	louka	k1gFnSc2
u	u	k7c2
Dlouhé	Dlouhé	k2eAgFnSc2d1
Lomnice	Lomnice	k1gFnSc2
•	•	k?
Malý	malý	k2eAgInSc1d1
Stolec	stolec	k1gInSc1
•	•	k?
Pastviny	pastvina	k1gFnSc2
u	u	k7c2
Srní	srní	k1gNnSc2
•	•	k?
Pernink	Pernink	k1gInSc4
•	•	k?
Prachomety	Prachomet	k1gInPc4
•	•	k?
<g/>
Rudenská	Rudenský	k2eAgFnSc1d1
luční	luční	k2eAgNnSc4d1
prameniště	prameniště	k1gNnSc4
•	•	k?
Šemnická	Šemnický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Špičák	špičák	k1gInSc1
u	u	k7c2
Vojkovic	Vojkovice	k1gFnPc2
•	•	k?
Toto-Karo	Toto-Kara	k1gFnSc5
•	•	k?
Týniště	týniště	k1gNnSc4
•	•	k?
Valeč	Valeč	k1gInSc1
•	•	k?
Valečské	Valečský	k2eAgInPc1d1
lemy	lem	k1gInPc1
•	•	k?
Valečské	Valečský	k2eAgInPc1d1
sklepy	sklep	k1gInPc1
•	•	k?
Velikonoční	velikonoční	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Viklan	viklan	k1gInSc1
•	•	k?
Vlčí	vlčet	k5eAaImIp3nS
jámy	jáma	k1gFnPc4
•	•	k?
Za	za	k7c4
Údrčí	Údrčí	k2eAgFnSc4d1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Sokolov	Sokolovo	k1gNnPc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kladské	kladský	k2eAgFnSc2d1
rašeliny	rašelina	k1gFnSc2
•	•	k?
Pluhův	Pluhův	k2eAgInSc1d1
bor	bor	k1gInSc1
•	•	k?
Rolavská	Rolavský	k2eAgNnPc4d1
vrchoviště	vrchoviště	k1gNnPc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Pískovna	pískovna	k1gFnSc1
Erika	Erik	k1gMnSc2
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Rašeliniště	rašeliniště	k1gNnSc1
u	u	k7c2
myslivny	myslivna	k1gFnSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Teplé	Teplá	k1gFnSc2
•	•	k?
V	v	k7c6
rašelinách	rašelina	k1gFnPc6
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Dominova	Dominův	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
•	•	k?
Kamenný	kamenný	k2eAgInSc1d1
hřib	hřib	k1gInSc1
•	•	k?
Moučné	moučný	k2eAgInPc1d1
pytle	pytel	k1gInPc1
•	•	k?
Na	na	k7c6
Vážkách	vážka	k1gFnPc6
•	•	k?
Přebuzské	Přebuzský	k2eAgNnSc1d1
vřesoviště	vřesoviště	k1gNnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Haar	Haar	k1gMnSc1
•	•	k?
Rotava	Rotava	k1gFnSc1
•	•	k?
Studenec	Studenec	k1gInSc1
•	•	k?
Tisovec	tisovec	k1gInSc1
•	•	k?
Údolí	údolí	k1gNnSc1
Ohře	Ohře	k1gFnSc2
•	•	k?
Vysoký	vysoký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Tachov	Tachov	k1gInSc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Hadovka	hadovka	k1gFnSc1
•	•	k?
Sedmihoří	Sedmihoř	k1gFnPc2
•	•	k?
Úterský	Úterský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Valcha	valcha	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Na	na	k7c6
požárech	požár	k1gInPc6
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Broumovská	broumovský	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
•	•	k?
Bučina	bučina	k1gFnSc1
u	u	k7c2
Žďáru	Žďár	k1gInSc2
•	•	k?
Diana	Diana	k1gFnSc1
•	•	k?
Farské	farský	k2eAgFnSc2d1
bažiny	bažina	k1gFnSc2
•	•	k?
Hradištský	Hradištský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Jezírka	jezírko	k1gNnSc2
u	u	k7c2
Rozvadova	Rozvadův	k2eAgInSc2d1
•	•	k?
Křížový	křížový	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Lazurový	lazurový	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Mělký	mělký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Ostrůvek	ostrůvek	k1gInSc1
•	•	k?
Pavlova	Pavlův	k2eAgFnSc1d1
Huť	huť	k1gFnSc1
•	•	k?
Pavlovická	pavlovický	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Volfštejnem	Volfštejn	k1gInSc7
•	•	k?
Podkovák	Podkovák	k?
•	•	k?
Přimda	Přimda	k1gMnSc1
•	•	k?
Tisovské	tisovský	k2eAgInPc4d1
rybníky	rybník	k1gInPc4
•	•	k?
Tišina	tišina	k1gFnSc1
•	•	k?
U	u	k7c2
rybníčků	rybníček	k1gInPc2
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bonětice	Bonětice	k1gFnSc1
•	•	k?
Černošínský	Černošínský	k2eAgInSc1d1
bor	bor	k1gInSc1
•	•	k?
Čiperka	čiperka	k1gFnSc1
•	•	k?
Kolowratův	Kolowratův	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Krasíkov	Krasíkov	k1gInSc1
•	•	k?
Louky	louka	k1gFnSc2
u	u	k7c2
Prostředního	prostřední	k2eAgInSc2d1
Žďáru	Žďár	k1gInSc2
•	•	k?
Maršovy	Maršovy	k?
Chody	chod	k1gInPc1
•	•	k?
Milov	Milov	k1gInSc1
•	•	k?
Na	na	k7c4
Kolmu	Kolma	k1gFnSc4
•	•	k?
Niva	niva	k1gFnSc1
Bílého	bílý	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Petrské	petrský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Pístovská	Pístovský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Pod	pod	k7c7
Šipínem	Šipín	k1gInSc7
•	•	k?
Prameniště	prameniště	k1gNnSc4
Kateřinského	kateřinský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Racovské	Racovský	k2eAgInPc1d1
rybníčky	rybníček	k1gInPc1
•	•	k?
Šelmberk	Šelmberk	k1gInSc1
•	•	k?
Valcha	valcha	k1gFnSc1
•	•	k?
Žďár	Žďár	k1gInSc1
u	u	k7c2
Chodského	chodský	k2eAgNnSc2d1
Újezda	Újezdo	k1gNnSc2
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
