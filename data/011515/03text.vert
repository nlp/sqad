<p>
<s>
Obec	obec	k1gFnSc1	obec
Čistá	čistý	k2eAgFnSc1d1	čistá
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Středočeský	středočeský	k2eAgInSc1d1	středočeský
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
asi	asi	k9	asi
devět	devět	k4xCc1	devět
kilometrů	kilometr	k1gInPc2	kilometr
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Mladé	mladá	k1gFnSc2	mladá
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
783	[number]	k4	783
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Územněsprávní	územněsprávní	k2eAgNnSc4d1	územněsprávní
začlenění	začlenění	k1gNnSc4	začlenění
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1850	[number]	k4	1850
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Bělá	bělat	k5eAaImIp3nS	bělat
</s>
</p>
<p>
<s>
1855	[number]	k4	1855
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Bělá	bělat	k5eAaImIp3nS	bělat
</s>
</p>
<p>
<s>
1868	[number]	k4	1868
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
okres	okres	k1gInSc4	okres
Mnichovo	mnichův	k2eAgNnSc1d1	Mnichovo
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Bělá	bělat	k5eAaImIp3nS	bělat
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
okres	okres	k1gInSc4	okres
Mnichovo	mnichův	k2eAgNnSc1d1	Mnichovo
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Bělá	bělat	k5eAaImIp3nS	bělat
pod	pod	k7c7	pod
Bezdězem	Bezděz	k1gInSc7	Bezděz
</s>
</p>
<p>
<s>
1942	[number]	k4	1942
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Bělá	bělat	k5eAaImIp3nS	bělat
pod	pod	k7c7	pod
Bezdězem	Bezděz	k1gInSc7	Bezděz
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
správní	správní	k2eAgInSc4d1	správní
okres	okres	k1gInSc4	okres
Mnichovo	mnichův	k2eAgNnSc1d1	Mnichovo
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Bělá	bělat	k5eAaImIp3nS	bělat
pod	pod	k7c7	pod
Bezdězem	Bezděz	k1gInSc7	Bezděz
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
Liberecký	liberecký	k2eAgInSc1d1	liberecký
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Doksy	Doksy	k1gInPc1	Doksy
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
</s>
</p>
<p>
<s>
===	===	k?	===
Rok	rok	k1gInSc1	rok
1932	[number]	k4	1932
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Čistá	čistá	k1gFnSc1	čistá
s	s	k7c7	s
1028	[number]	k4	1028
obyvateli	obyvatel	k1gMnPc7	obyvatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
byly	být	k5eAaImAgInP	být
evidovány	evidován	k2eAgInPc1d1	evidován
tyto	tento	k3xDgInPc1	tento
úřady	úřad	k1gInPc1	úřad
<g/>
,	,	kIx,	,
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
,	,	kIx,	,
cihelna	cihelna	k1gFnSc1	cihelna
<g/>
,	,	kIx,	,
cukrář	cukrář	k1gMnSc1	cukrář
<g/>
,	,	kIx,	,
2	[number]	k4	2
holiči	holič	k1gMnPc7	holič
<g/>
,	,	kIx,	,
4	[number]	k4	4
hostince	hostinec	k1gInPc1	hostinec
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
železných	železný	k2eAgNnPc2d1	železné
kamen	kamna	k1gNnPc2	kamna
<g/>
,	,	kIx,	,
3	[number]	k4	3
koláři	kolář	k1gMnPc7	kolář
<g/>
,	,	kIx,	,
konsum	konsum	k1gInSc1	konsum
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
kováři	kovář	k1gMnPc1	kovář
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
2	[number]	k4	2
pekaři	pekař	k1gMnPc7	pekař
<g/>
,	,	kIx,	,
porodní	porodní	k2eAgFnSc1d1	porodní
asistentka	asistentka	k1gFnSc1	asistentka
<g/>
,	,	kIx,	,
18	[number]	k4	18
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
2	[number]	k4	2
řezníci	řezník	k1gMnPc1	řezník
<g/>
,	,	kIx,	,
6	[number]	k4	6
obchodů	obchod	k1gInPc2	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
spořitelní	spořitelní	k2eAgFnPc1d1	spořitelní
a	a	k8xC	a
záložní	záložní	k2eAgInSc1d1	záložní
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
strojírna	strojírna	k1gFnSc1	strojírna
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
se	s	k7c7	s
střižním	střižní	k2eAgNnSc7d1	střižní
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
3	[number]	k4	3
trafiky	trafika	k1gFnSc2	trafika
<g/>
,	,	kIx,	,
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
,	,	kIx,	,
zednický	zednický	k2eAgMnSc1d1	zednický
mistr	mistr	k1gMnSc1	mistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Václav	Václav	k1gMnSc1	Václav
Holeček	Holeček	k1gMnSc1	Holeček
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Čisté	čistá	k1gFnSc2	čistá
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
sklářský	sklářský	k2eAgMnSc1d1	sklářský
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
VŠUP	VŠUP	kA	VŠUP
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
návrhů	návrh	k1gInPc2	návrh
a	a	k8xC	a
realizátor	realizátor	k1gMnSc1	realizátor
vitrážových	vitrážový	k2eAgNnPc2d1	vitrážové
oken	okno	k1gNnPc2	okno
s	s	k7c7	s
náměty	námět	k1gInPc7	námět
českých	český	k2eAgInPc2d1	český
patronů	patron	k1gInPc2	patron
pro	pro	k7c4	pro
Československou	československý	k2eAgFnSc4d1	Československá
smírčí	smírčí	k2eAgFnSc4d1	smírčí
kapli	kaple	k1gFnSc4	kaple
na	na	k7c6	na
Olivetské	olivetský	k2eAgFnSc6d1	Olivetská
hoře	hora	k1gFnSc6	hora
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
<s>
Středem	středem	k7c2	středem
obce	obec	k1gFnSc2	obec
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
obce	obec	k1gFnSc2	obec
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
Nymburk	Nymburk	k1gInSc1	Nymburk
-	-	kIx~	-
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
-	-	kIx~	-
Bělá	bělat	k5eAaImIp3nS	bělat
pod	pod	k7c7	pod
Bezdězem	Bezděz	k1gInSc7	Bezděz
-	-	kIx~	-
Doksy	Doksy	k1gInPc1	Doksy
-	-	kIx~	-
Jestřebí	Jestřebí	k1gNnSc1	Jestřebí
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
ani	ani	k8xC	ani
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
obci	obec	k1gFnSc3	obec
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Bělá	bělat	k5eAaImIp3nS	bělat
pod	pod	k7c7	pod
Bezdězem	Bezděz	k1gInSc7	Bezděz
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
2	[number]	k4	2
km	km	kA	km
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
080	[number]	k4	080
z	z	k7c2	z
Bakova	Bakov	k1gInSc2	Bakov
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
Lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
</s>
</p>
<p>
<s>
Obcí	obec	k1gFnSc7	obec
projížděly	projíždět	k5eAaImAgInP	projíždět
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
<g/>
:	:	kIx,	:
Bělá	bělat	k5eAaImIp3nS	bělat
pod	pod	k7c7	pod
Bezdězem	Bezděz	k1gInSc7	Bezděz
<g/>
,	,	kIx,	,
Bezno	Bezno	k1gNnSc4	Bezno
<g/>
,	,	kIx,	,
Doksy	Doksy	k1gInPc1	Doksy
<g/>
,	,	kIx,	,
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
dopravce	dopravce	k1gMnSc1	dopravce
TRANSCENTRUM	TRANSCENTRUM	kA	TRANSCENTRUM
bus	bus	k1gInSc1	bus
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Čistá	čistá	k1gFnSc1	čistá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Čistá	čistá	k1gFnSc1	čistá
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
