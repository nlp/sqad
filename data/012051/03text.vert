<p>
<s>
Wolfova	Wolfův	k2eAgFnSc1d1	Wolfova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
matematiku	matematika	k1gFnSc4	matematika
je	být	k5eAaImIp3nS	být
izraelské	izraelský	k2eAgNnSc1d1	izraelské
vědecké	vědecký	k2eAgNnSc1d1	vědecké
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
,	,	kIx,	,
udělované	udělovaný	k2eAgNnSc1d1	udělované
každoročně	každoročně	k6eAd1	každoročně
Wolfovou	Wolfův	k2eAgFnSc7d1	Wolfova
nadací	nadace	k1gFnSc7	nadace
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
kategoriemi	kategorie	k1gFnPc7	kategorie
ocenění	ocenění	k1gNnSc2	ocenění
jsou	být	k5eAaImIp3nP	být
ceny	cena	k1gFnPc1	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
nositelů	nositel	k1gMnPc2	nositel
==	==	k?	==
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Israel	Israel	k1gInSc1	Israel
Gelfand	Gelfand	k1gInSc4	Gelfand
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Ludwig	Ludwig	k1gMnSc1	Ludwig
Siegel	Siegel	k1gMnSc1	Siegel
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
Jean	Jean	k1gMnSc1	Jean
Leray	Leraa	k1gFnPc4	Leraa
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
Weil	Weil	k1gMnSc1	Weil
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Henri	Henri	k1gNnSc1	Henri
Cartan	Cartan	k1gInSc1	Cartan
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Kolmogorov	Kolmogorov	k1gInSc1	Kolmogorov
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Lars	Lars	k1gInSc1	Lars
Ahlfors	Ahlfors	k1gInSc4	Ahlfors
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gInSc4	Oscar
Zariski	Zarisk	k1gFnSc2	Zarisk
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Hassler	Hassler	k1gInSc1	Hassler
Whitney	Whitnea	k1gFnSc2	Whitnea
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Grigoryevich	Grigoryevich	k1gMnSc1	Grigoryevich
Krein	Krein	k1gMnSc1	Krein
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Shiing-Shen	Shiing-Shno	k1gNnPc2	Shiing-Shno
Chern	Cherna	k1gFnPc2	Cherna
<g/>
,	,	kIx,	,
Pál	Pál	k1gFnPc2	Pál
Erdős	Erdősa	k1gFnPc2	Erdősa
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
Kunihiko	Kunihika	k1gFnSc5	Kunihika
Kodaira	Kodaira	k1gMnSc1	Kodaira
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
Lewy	Lewa	k1gFnSc2	Lewa
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
Samuel	Samuel	k1gMnSc1	Samuel
Eilenberg	Eilenberg	k1gMnSc1	Eilenberg
<g/>
,	,	kIx,	,
Atle	Atle	k1gFnSc1	Atle
Selberg	Selberg	k1gMnSc1	Selberg
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Kiyoshi	Kiyoshi	k1gNnSc7	Kiyoshi
Itō	Itō	k1gFnSc2	Itō
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Lax	Lax	k1gMnSc1	Lax
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
Friedrich	Friedrich	k1gMnSc1	Friedrich
Hirzebruch	Hirzebruch	k1gMnSc1	Hirzebruch
<g/>
,	,	kIx,	,
Lars	Lars	k1gInSc1	Lars
Hörmander	Hörmandra	k1gFnPc2	Hörmandra
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
Alberto	Alberta	k1gFnSc5	Alberta
Calderón	Calderón	k1gMnSc1	Calderón
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Milnor	Milnor	k1gMnSc1	Milnor
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Ennio	Ennio	k1gMnSc1	Ennio
de	de	k?	de
Giorgi	Giorg	k1gFnSc2	Giorg
<g/>
,	,	kIx,	,
Ilja	Ilja	k1gFnSc1	Ilja
Pjatěckij-Šapiro	Pjatěckij-Šapiro	k1gNnSc1	Pjatěckij-Šapiro
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
bez	bez	k7c2	bez
ocenění	ocenění	k1gNnSc2	ocenění
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Lennart	Lennarta	k1gFnPc2	Lennarta
Carleson	Carleson	k1gMnSc1	Carleson
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Griggs	Griggsa	k1gFnPc2	Griggsa
Thompson	Thompson	k1gMnSc1	Thompson
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
Michail	Michail	k1gInSc1	Michail
Gromov	Gromov	k1gInSc4	Gromov
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
Tits	Titsa	k1gFnPc2	Titsa
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
Jurgen	Jurgen	k1gInSc1	Jurgen
Moser	Moser	k1gInSc4	Moser
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
Robert	Roberta	k1gFnPc2	Roberta
Langlands	Langlandsa	k1gFnPc2	Langlandsa
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
Wiles	Wiles	k1gMnSc1	Wiles
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
Joseph	Josepha	k1gFnPc2	Josepha
B.	B.	kA	B.
Keller	Keller	k1gMnSc1	Keller
<g/>
,	,	kIx,	,
Jakov	Jakov	k1gInSc1	Jakov
Sinaj	Sinaj	k1gInSc1	Sinaj
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
bez	bez	k7c2	bez
ocenění	ocenění	k1gNnSc2	ocenění
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
László	László	k1gMnSc1	László
Lovász	Lovász	k1gMnSc1	Lovász
<g/>
,	,	kIx,	,
Elias	Elias	k1gMnSc1	Elias
Menachem	Menach	k1gMnSc7	Menach
Stein	Stein	k1gMnSc1	Stein
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Raoul	Raoul	k1gInSc1	Raoul
Bott	Bott	k2eAgInSc1d1	Bott
<g/>
,	,	kIx,	,
Jean-Pierre	Jean-Pierr	k1gMnSc5	Jean-Pierr
Serre	Serr	k1gMnSc5	Serr
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
Vladimir	Vladimir	k1gMnSc1	Vladimir
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
,	,	kIx,	,
Saharon	Saharon	k1gMnSc1	Saharon
Šelach	Šelach	k1gMnSc1	Šelach
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
Mikio	Mikio	k1gMnSc1	Mikio
Sató	Sató	k1gMnSc1	Sató
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Tate	Tat	k1gFnSc2	Tat
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
bez	bez	k7c2	bez
ocenění	ocenění	k1gNnSc2	ocenění
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Gregory	Gregor	k1gMnPc7	Gregor
Margulis	Margulis	k1gFnSc2	Margulis
<g/>
,	,	kIx,	,
Sergei	Serge	k1gFnSc2	Serge
Petrovich	Petrovicha	k1gFnPc2	Petrovicha
Novikov	Novikov	k1gInSc1	Novikov
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
Stephen	Stephno	k1gNnPc2	Stephno
Smale	Smale	k1gNnSc2	Smale
<g/>
,	,	kIx,	,
Hilel	Hilel	k1gMnSc1	Hilel
Fürstenberg	Fürstenberg	k1gMnSc1	Fürstenberg
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Pierre	Pierr	k1gMnSc5	Pierr
Deligne	Delign	k1gMnSc5	Delign
<g/>
,	,	kIx,	,
Phillip	Phillip	k1gInSc4	Phillip
Griffits	Griffitsa	k1gFnPc2	Griffitsa
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Mumford	Mumford	k1gMnSc1	Mumford
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
bez	bez	k7c2	bez
ocenění	ocenění	k1gNnSc2	ocenění
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Shing	Shing	k1gMnSc1	Shing
-	-	kIx~	-
Tung	Tung	k1gMnSc1	Tung
Yau	Yau	k1gMnSc1	Yau
<g/>
,	,	kIx,	,
Dennis	Dennis	k1gFnSc1	Dennis
Sullivan	Sullivan	k1gMnSc1	Sullivan
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
bez	bez	k7c2	bez
ocenění	ocenění	k1gNnSc2	ocenění
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Michael	Michaela	k1gFnPc2	Michaela
Aschbacher	Aschbachra	k1gFnPc2	Aschbachra
<g/>
,	,	kIx,	,
Luís	Luísa	k1gFnPc2	Luísa
Angel	Angela	k1gFnPc2	Angela
Caffarelli	Caffarell	k1gMnPc1	Caffarell
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
Michael	Michael	k1gMnSc1	Michael
Artin	Artin	k1gMnSc1	Artin
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Peter	Peter	k1gMnSc1	Peter
Sarnak	Sarnak	k1gMnSc1	Sarnak
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
James	James	k1gMnSc1	James
G.	G.	kA	G.
Arthur	Arthur	k1gMnSc1	Arthur
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
bez	bez	k7c2	bez
ocenění	ocenění	k1gNnSc2	ocenění
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
Richard	Richard	k1gMnSc1	Richard
Schoen	Schoen	k1gInSc1	Schoen
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Fefferman	Fefferman	k1gMnSc1	Fefferman
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wolfova	Wolfův	k2eAgFnSc1d1	Wolfova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
matematiku	matematika	k1gFnSc4	matematika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
