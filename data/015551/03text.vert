<s>
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolekNorddeutscher	spolekNorddeutschra	k1gFnPc2
Bund	bunda	k1gFnPc2
</s>
<s>
←	←	k?
←	←	k?
←	←	k?
←	←	k?
</s>
<s>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1871	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
je	být	k5eAaImIp3nS
vyznačený	vyznačený	k2eAgInSc1d1
zeleně	zeleně	k6eAd1
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Berlín	Berlín	k1gInSc1
</s>
<s>
nejdelší	dlouhý	k2eAgFnSc1d3
řeka	řeka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Rýn	Rýn	k1gInSc1
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
30	#num#	k4
milionů	milion	k4xCgInPc2
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
němčina	němčina	k1gFnSc1
<g/>
,	,	kIx,
dánština	dánština	k1gFnSc1
<g/>
,	,	kIx,
dolnoněmčina	dolnoněmčina	k1gFnSc1
<g/>
,	,	kIx,
sater-fríština	sater-fríština	k1gFnSc1
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc1d1
fríština	fríština	k1gFnSc1
<g/>
,	,	kIx,
polština	polština	k1gFnSc1
<g/>
,	,	kIx,
litevština	litevština	k1gFnSc1
<g/>
,	,	kIx,
lužická	lužický	k2eAgFnSc1d1
srbština	srbština	k1gFnSc1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
protestantské	protestantský	k2eAgNnSc1d1
(	(	kIx(
<g/>
luteránské	luteránský	k2eAgInPc1d1
<g/>
,	,	kIx,
kalvínské	kalvínský	k2eAgInPc1d1
<g/>
,	,	kIx,
spojené	spojený	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
menšinově	menšinově	k6eAd1
římskokatolické	římskokatolický	k2eAgFnSc3d1
<g/>
,	,	kIx,
judaistické	judaistický	k2eAgFnSc3d1
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
konfederace	konfederace	k1gFnSc1
</s>
<s>
vznik	vznik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1866	#num#	k4
–	–	k?
výsledek	výsledek	k1gInSc1
prusko-rakouské	prusko-rakouský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
zánik	zánik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1871	#num#	k4
–	–	k?
výsledek	výsledek	k1gInSc1
prusko-francouzské	prusko-francouzský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Německý	německý	k2eAgInSc1d1
spolek	spolek	k1gInSc1
</s>
<s>
Šlesvicko	Šlesvicko	k6eAd1
</s>
<s>
Pruské	pruský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Poznaňsko	Poznaňsko	k6eAd1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
nebo	nebo	k8xC
Severoněmecká	severoněmecký	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Norddeutscher	Norddeutscher	k1gFnPc2
Bund	bund	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
konfederací	konfederace	k1gFnSc7
(	(	kIx(
<g/>
spolkem	spolek	k1gInSc7
<g/>
)	)	kIx)
severoněmeckých	severoněmecký	k2eAgInPc2d1
států	stát	k1gInPc2
nacházejících	nacházející	k2eAgInPc2d1
se	se	k3xPyFc4
severně	severně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Mohan	Mohan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
existoval	existovat	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1867	#num#	k4
<g/>
–	–	k?
<g/>
1871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
po	po	k7c6
prusko-rakouské	prusko-rakouský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
byl	být	k5eAaImAgMnS
nástupcem	nástupce	k1gMnSc7
Německého	německý	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
zahrnoval	zahrnovat	k5eAaImAgInS
však	však	k9
jen	jen	k9
severněji	severně	k6eAd2
položené	položený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolek	spolek	k1gInSc1
tvořil	tvořit	k5eAaImAgInS
základ	základ	k1gInSc4
pro	pro	k7c4
budoucí	budoucí	k2eAgNnSc4d1
Německé	německý	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc4
byl	být	k5eAaImAgMnS
přímým	přímý	k2eAgMnSc7d1
předchůdcem	předchůdce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
ve	v	k7c6
spolku	spolek	k1gInSc6
mělo	mít	k5eAaImAgNnS
samozřejmě	samozřejmě	k6eAd1
Prusko	Prusko	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
bylo	být	k5eAaImAgNnS
také	také	k6eAd1
majoritní	majoritní	k2eAgFnSc7d1
silou	síla	k1gFnSc7
jak	jak	k8xS,k8xC
vojensky	vojensky	k6eAd1
<g/>
,	,	kIx,
tak	tak	k9
rozlohou	rozloha	k1gFnSc7
a	a	k8xC
ve	v	k7c6
spolkovém	spolkový	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
mělo	mít	k5eAaImAgNnS
17	#num#	k4
ze	z	k7c2
43	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1866	#num#	k4
se	se	k3xPyFc4
politicky	politicky	k6eAd1
postupně	postupně	k6eAd1
ze	z	k7c2
spolku	spolek	k1gInSc2
(	(	kIx(
<g/>
konfederace	konfederace	k1gFnSc2
<g/>
)	)	kIx)
stala	stát	k5eAaPmAgFnS
federace	federace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Součástí	součást	k1gFnSc7
spolku	spolek	k1gInSc2
byly	být	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
německé	německý	k2eAgInPc1d1
státy	stát	k1gInPc1
respektive	respektive	k9
státy	stát	k1gInPc1
jež	jenž	k3xRgInPc1
byly	být	k5eAaImAgInP
předtím	předtím	k6eAd1
součástí	součást	k1gFnSc7
Německého	německý	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
Rakouska	Rakousko	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bavorska	Bavorsko	k1gNnSc2
<g/>
,	,	kIx,
Württemberska	Württembersko	k1gNnSc2
<g/>
,	,	kIx,
Bádenska	Bádensko	k1gNnSc2
a	a	k8xC
jižních	jižní	k2eAgFnPc2d1
částí	část	k1gFnPc2
Hesenska	Hesensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hesensko	Hesensko	k1gNnSc1
(	(	kIx(
<g/>
Hesenské	hesenský	k2eAgNnSc1d1
velkovévodství	velkovévodství	k1gNnSc1
<g/>
)	)	kIx)
respektive	respektive	k9
Hesensko-Darmstadtsko	Hesensko-Darmstadtsko	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
severně	severně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Mohan	Mohan	k1gInSc4
ležící	ležící	k2eAgFnSc1d1
polovička	polovička	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
stala	stát	k5eAaPmAgFnS
také	také	k9
součástí	součást	k1gFnSc7
Severoněmeckého	severoněmecký	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
z	z	k7c2
poloviny	polovina	k1gFnSc2
nezávislým	závislý	k2eNgInSc7d1
státem	stát	k1gInSc7
a	a	k8xC
z	z	k7c2
poloviny	polovina	k1gFnSc2
členem	člen	k1gMnSc7
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
takže	takže	k8xS
jeho	jeho	k3xOp3gFnSc1
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
zůstávala	zůstávat	k5eAaImAgFnS
mimo	mimo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgNnSc7d1
územím	území	k1gNnSc7
jižně	jižně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Mohan	Mohan	k1gInSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
bylo	být	k5eAaImAgNnS
pevnou	pevný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
Pruska	Prusko	k1gNnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Hohenzollernsko	Hohenzollernsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
parlament	parlament	k1gInSc1
<g/>
)	)	kIx)
zasedal	zasedat	k5eAaImAgInS
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
spolku	spolek	k1gInSc2
byl	být	k5eAaImAgMnS
pruský	pruský	k2eAgMnSc1d1
král	král	k1gMnSc1
Vilém	Vilém	k1gMnSc1
I.	I.	kA
a	a	k8xC
kancléřem	kancléř	k1gMnSc7
byl	být	k5eAaImAgMnS
Otto	Otto	k1gMnSc1
von	von	k1gInSc4
Bismarck	Bismarck	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
sjednocením	sjednocení	k1gNnSc7
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
bylo	být	k5eAaImAgNnS
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1871	#num#	k4
v	v	k7c6
zrcadlovém	zrcadlový	k2eAgInSc6d1
sále	sál	k1gInSc6
ve	v	k7c6
Versailles	Versailles	k1gFnSc6
po	po	k7c6
vítězství	vítězství	k1gNnSc6
Severoněmeckého	severoněmecký	k2eAgInSc2d1
spolku	spolek	k1gInSc2
a	a	k8xC
spojeneckých	spojenecký	k2eAgInPc2d1
jihoněmeckých	jihoněmecký	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
prusko-francouzské	prusko-francouzský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vyhlášeno	vyhlášen	k2eAgNnSc1d1
Německé	německý	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Pražský	pražský	k2eAgInSc1d1
mír	mír	k1gInSc1
(	(	kIx(
<g/>
1866	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Severoněmecký	severoněmecký	k2eAgInSc4d1
spolek	spolek	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
Severoněmeckého	severoněmecký	k2eAgInSc2d1
spolku	spolek	k1gInSc2
Království	království	k1gNnSc1
</s>
<s>
Pruské	pruský	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
Lauenbursko	Lauenbursko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Saské	saský	k2eAgNnSc4d1
království	království	k1gNnSc4
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
1866-1871	1866-1871	k4
Velkovévodství	velkovévodství	k1gNnPc2
</s>
<s>
Hesenské	hesenský	k2eAgNnSc1d1
velkovévodství	velkovévodství	k1gNnSc1
(	(	kIx(
<g/>
severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sasko-výmarsko-eisenašsko	Sasko-výmarsko-eisenašsko	k1gNnSc1
<g/>
,	,	kIx,
Lucembursko	Lucembursko	k1gNnSc1
<g/>
,	,	kIx,
<g/>
Meklenbursko-Střelicko	Meklenbursko-Střelicko	k1gNnSc1
<g/>
,	,	kIx,
Meklenbursko-Zvěřínsko	Meklenbursko-Zvěřínsko	k1gNnSc1
<g/>
,	,	kIx,
Oldenburské	oldenburský	k2eAgNnSc1d1
velkovévodství	velkovévodství	k1gNnSc1
Vévodství	vévodství	k1gNnSc2
</s>
<s>
Anhaltsko	Anhaltsko	k1gNnSc1
<g/>
,	,	kIx,
Brunšvické	brunšvický	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
,	,	kIx,
Sasko-Altenbursko	Sasko-Altenbursko	k1gNnSc1
<g/>
,	,	kIx,
Sasko-Meiningensko	Sasko-Meiningensko	k1gNnSc1
<g/>
,	,	kIx,
Sasko-Kobursko-Gothajsko	Sasko-Kobursko-Gothajsko	k1gNnSc1
Knížectví	knížectví	k1gNnSc2
</s>
<s>
knížectví	knížectví	k1gNnSc1
Lippe	Lipp	k1gInSc5
<g/>
,	,	kIx,
knížectví	knížectví	k1gNnSc2
Reuss	Reussa	k1gFnPc2
<g/>
,	,	kIx,
Schaumburg-Lippe	Schaumburg-Lipp	k1gMnSc5
<g/>
,	,	kIx,
Schwarzbursko-Rudolstadt	Schwarzbursko-Rudolstadt	k1gInSc1
<g/>
,	,	kIx,
Schwarzbursko-Sondershausen	Schwarzbursko-Sondershausen	k1gInSc1
<g/>
,	,	kIx,
Waldeck	Waldeck	k1gInSc1
<g/>
,	,	kIx,
Hohenzollernsko	Hohenzollernsko	k1gNnSc1
Svobodná	svobodný	k2eAgFnSc1d1
města	město	k1gNnSc2
</s>
<s>
Brémy	Brémy	k1gFnPc1
<g/>
,	,	kIx,
Frankfurt	Frankfurt	k1gInSc1
<g/>
,	,	kIx,
Hamburk	Hamburk	k1gInSc1
<g/>
,	,	kIx,
Lübeck	Lübeck	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
2033898-3	2033898-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1884	#num#	k4
8032	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81097801	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
131360783	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81097801	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
</s>
