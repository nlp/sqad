<s>
Severoněmecký	severoněmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
nebo	nebo	k8xC
Severoněmecká	severoněmecký	k2eAgFnSc1d1
konfederace	konfederace	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Norddeutscher	Norddeutscher	k1gFnPc2
Bund	bund	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
konfederací	konfederace	k1gFnSc7
(	(	kIx(
<g/>
spolkem	spolek	k1gInSc7
<g/>
)	)	kIx)
severoněmeckých	severoněmecký	k2eAgInPc2d1
států	stát	k1gInPc2
nacházejících	nacházející	k2eAgInPc2d1
se	se	k3xPyFc4
severně	severně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Mohan	Mohan	k1gInSc1
<g/>
.	.	kIx.
</s>