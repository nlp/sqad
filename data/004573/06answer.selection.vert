<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
českým	český	k2eAgInSc7d1	český
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
Sněžka	Sněžka	k1gFnSc1	Sněžka
s	s	k7c7	s
1603	[number]	k4	1603
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejnižším	nízký	k2eAgInSc7d3	nejnižší
pak	pak	k6eAd1	pak
Labe	Labe	k1gNnPc4	Labe
na	na	k7c6	na
odtoku	odtok	k1gInSc6	odtok
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
u	u	k7c2	u
Hřenska	Hřensko	k1gNnSc2	Hřensko
se	s	k7c7	s
115	[number]	k4	115
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
