<p>
<s>
Okres	okres	k1gInSc1	okres
Le-jie	Leie	k1gFnSc2	Le-jie
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Le-jie	Leie	k1gFnSc2	Le-jie
sian	siana	k1gFnPc2	siana
<g/>
,	,	kIx,	,
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
Lè	Lè	k1gMnSc2	Lè
Xià	Xià	k1gMnSc2	Xià
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc7	znak
乐	乐	k?	乐
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
okres	okres	k1gInSc1	okres
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
pod	pod	k7c4	pod
městskou	městský	k2eAgFnSc4d1	městská
prefekturu	prefektura	k1gFnSc4	prefektura
Paj-se	Paje	k1gFnSc2	Paj-se
na	na	k7c6	na
západě	západ	k1gInSc6	západ
čuangské	čuangský	k2eAgFnSc2d1	čuangský
autonomní	autonomní	k2eAgFnSc2d1	autonomní
oblasti	oblast	k1gFnSc2	oblast
Kuang-si	Kuange	k1gFnSc3	Kuang-se
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
okresu	okres	k1gInSc2	okres
je	být	k5eAaImIp3nS	být
2617	[number]	k4	2617
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
měl	mít	k5eAaImAgInS	mít
157	[number]	k4	157
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
je	být	k5eAaImIp3nS	být
městys	městys	k1gInSc1	městys
Tchung-le	Tchunge	k1gFnSc2	Tchung-le
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konalo	konat	k5eAaImAgNnS	konat
mistrovství	mistrovství	k1gNnSc1	mistrovství
Asie	Asie	k1gFnSc2	Asie
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Leye	Ley	k1gFnSc2	Ley
(	(	kIx(	(
<g/>
Bose	Bose	kA	Bose
<g/>
)	)	kIx)	)
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
