<s>
Finsko	Finsko	k1gNnSc1	Finsko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
eurozóny	eurozóna	k1gFnSc2	eurozóna
(	(	kIx(	(
<g/>
od	od	k7c2	od
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
sousední	sousední	k2eAgNnSc4d1	sousední
Švédsko	Švédsko	k1gNnSc4	Švédsko
<g/>
.	.	kIx.	.
</s>
