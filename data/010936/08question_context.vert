<s>
Titan	titan	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
50	[number]	k4
%	%	kIx~
větší	veliký	k2eAgFnSc2d2
a	a	k8xC
80	[number]	k4
%	%	kIx~
hmotnější	hmotný	k2eAgInSc1d2
než	než	k8xS
zemský	zemský	k2eAgInSc1d1
Měsíc	měsíc	k1gInSc1
<g/>
,	,	kIx,
po	po	k7c6
Ganymedu	Ganymed	k1gMnSc6
druhý	druhý	k4xOgInSc4
největší	veliký	k2eAgInSc4d3
měsíc	měsíc	k1gInSc4
v	v	k7c6
celé	celý	k2eAgFnSc6d1
planetární	planetární	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Titan	titan	k1gInSc1
(	(	kIx(
<g/>
Saturn	Saturn	k1gInSc1
VI	VI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
z	z	k7c2
62	[number]	k4
do	do	k7c2
roku	rok	k1gInSc2
2017	[number]	k4
objevených	objevený	k2eAgInPc2d1
měsíců	měsíc	k1gInPc2
planety	planeta	k1gFnSc2
Saturn	Saturn	k1gMnSc1
<g/>
.	.	kIx.
</s>