<s>
Je	být	k5eAaImIp3nS
o	o	k7c4
něco	něco	k3yInSc4
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
nejmenší	malý	k2eAgFnSc1d3
planeta	planeta	k1gFnSc1
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
Merkur	Merkur	k1gInSc1
<g/>
,	,	kIx,
dosahuje	dosahovat	k5eAaImIp3nS
však	však	k9
jen	jen	k9
40	[number]	k4
%	%	kIx~
Merkurovy	Merkurův	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
<g/>
.	.	kIx.
</s>