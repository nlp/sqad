<p>
<s>
Den	den	k1gInSc1	den
obnovy	obnova	k1gFnSc2	obnova
samostatného	samostatný	k2eAgInSc2d1	samostatný
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
Deň	Deň	k1gFnSc1	Deň
vzniku	vznik	k1gInSc2	vznik
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Den	den	k1gInSc1	den
vzniku	vznik	k1gInSc2	vznik
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
státním	státní	k2eAgInSc7d1	státní
svátkem	svátek	k1gInSc7	svátek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
připadající	připadající	k2eAgFnSc2d1	připadající
na	na	k7c6	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
tedy	tedy	k9	tedy
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
vyhlášený	vyhlášený	k2eAgInSc1d1	vyhlášený
na	na	k7c6	na
základě	základ	k1gInSc6	základ
novely	novela	k1gFnSc2	novela
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
přijaté	přijatý	k2eAgFnSc2d1	přijatá
v	v	k7c6	v
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
jako	jako	k8xS	jako
Deň	Deň	k1gMnSc1	Deň
vzniku	vznik	k1gInSc2	vznik
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
řádový	řádový	k2eAgInSc4d1	řádový
den	den	k1gInSc4	den
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
tedy	tedy	k9	tedy
o	o	k7c4	o
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
udělovat	udělovat	k5eAaImF	udělovat
Státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
existovala	existovat	k5eAaImAgFnS	existovat
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
ČSFR	ČSFR	kA	ČSFR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
republiky	republika	k1gFnPc1	republika
měly	mít	k5eAaImAgFnP	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
národní	národní	k2eAgFnSc4d1	národní
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Centrálními	centrální	k2eAgInPc7d1	centrální
federálními	federální	k2eAgInPc7d1	federální
orgány	orgán	k1gInPc7	orgán
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
a	a	k8xC	a
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
platilo	platit	k5eAaImAgNnS	platit
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
právně	právně	k6eAd1	právně
i	i	k8xC	i
fakticky	fakticky	k6eAd1	fakticky
zaniká	zanikat	k5eAaImIp3nS	zanikat
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Společný	společný	k2eAgInSc1d1	společný
stát	stát	k1gInSc1	stát
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
existoval	existovat	k5eAaImAgInS	existovat
74	[number]	k4	74
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
dva	dva	k4xCgInPc4	dva
nové	nový	k2eAgInPc4d1	nový
samostatné	samostatný	k2eAgInPc4d1	samostatný
státy	stát	k1gInPc4	stát
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
představitelé	představitel	k1gMnPc1	představitel
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
vyšlých	vyšlý	k2eAgFnPc2d1	vyšlá
vítězně	vítězně	k6eAd1	vítězně
z	z	k7c2	z
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
české	český	k2eAgFnPc4d1	Česká
ODS	ODS	kA	ODS
a	a	k8xC	a
slovenské	slovenský	k2eAgFnSc2d1	slovenská
HZDS	HZDS	kA	HZDS
<g/>
,	,	kIx,	,
když	když	k8xS	když
nenašli	najít	k5eNaPmAgMnP	najít
oboustranně	oboustranně	k6eAd1	oboustranně
přijatelnou	přijatelný	k2eAgFnSc4d1	přijatelná
formu	forma	k1gFnSc4	forma
soužití	soužití	k1gNnSc4	soužití
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
skutečnosti	skutečnost	k1gFnSc3	skutečnost
předcházel	předcházet	k5eAaImAgInS	předcházet
maraton	maraton	k1gInSc1	maraton
česko-slovenských	českolovenský	k2eAgNnPc2d1	česko-slovenské
jednání	jednání	k1gNnPc2	jednání
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
o	o	k7c6	o
kompetencích	kompetence	k1gFnPc6	kompetence
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
o	o	k7c6	o
státoprávním	státoprávní	k2eAgNnSc6d1	státoprávní
uspořádání	uspořádání	k1gNnSc6	uspořádání
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
současné	současný	k2eAgFnPc1d1	současná
politické	politický	k2eAgFnPc1d1	politická
a	a	k8xC	a
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
faktory	faktor	k1gInPc1	faktor
nahrávaly	nahrávat	k5eAaImAgInP	nahrávat
naopak	naopak	k6eAd1	naopak
integraci	integrace	k1gFnSc4	integrace
<g/>
,	,	kIx,	,
spojování	spojování	k1gNnSc4	spojování
do	do	k7c2	do
většího	veliký	k2eAgInSc2d2	veliký
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byly	být	k5eAaImAgFnP	být
projednány	projednat	k5eAaPmNgInP	projednat
základní	základní	k2eAgInPc1d1	základní
dokumenty	dokument	k1gInPc1	dokument
o	o	k7c6	o
budoucí	budoucí	k2eAgFnSc6d1	budoucí
spolupráci	spolupráce	k1gFnSc6	spolupráce
samostatných	samostatný	k2eAgFnPc2d1	samostatná
republik	republika	k1gFnPc2	republika
<g/>
:	:	kIx,	:
vytvoření	vytvoření	k1gNnSc1	vytvoření
celní	celní	k2eAgFnSc2d1	celní
unie	unie	k1gFnSc2	unie
mezi	mezi	k7c7	mezi
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
<g/>
,	,	kIx,	,
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
zaměstnávání	zaměstnávání	k1gNnSc6	zaměstnávání
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
o	o	k7c6	o
sociálním	sociální	k2eAgNnSc6d1	sociální
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
<g/>
,	,	kIx,	,
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
dobrém	dobrý	k2eAgNnSc6d1	dobré
sousedství	sousedství	k1gNnSc6	sousedství
<g/>
,	,	kIx,	,
přátelských	přátelský	k2eAgInPc6d1	přátelský
vztazích	vztah	k1gInPc6	vztah
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
majetku	majetek	k1gInSc2	majetek
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
převodu	převod	k1gInSc2	převod
na	na	k7c4	na
republiky	republika	k1gFnPc4	republika
<g/>
,	,	kIx,	,
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
podpoře	podpora	k1gFnSc6	podpora
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
ochraně	ochrana	k1gFnSc3	ochrana
investic	investice	k1gFnPc2	investice
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
společné	společný	k2eAgFnSc2d1	společná
schůzi	schůze	k1gFnSc4	schůze
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSFR	ČSFR	kA	ČSFR
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
ČSFR	ČSFR	kA	ČSFR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývalá	bývalý	k2eAgFnSc1d1	bývalá
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
v	v	k7c4	v
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
byla	být	k5eAaImAgFnS	být
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
přijata	přijmout	k5eAaPmNgFnS	přijmout
za	za	k7c4	za
člena	člen	k1gMnSc4	člen
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
provedena	provést	k5eAaPmNgFnS	provést
měnová	měnový	k2eAgFnSc1d1	měnová
odluka	odluka	k1gFnSc1	odluka
měn	měna	k1gFnPc2	měna
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
platit	platit	k5eAaImF	platit
vlastní	vlastní	k2eAgFnSc1d1	vlastní
měna	měna	k1gFnSc1	měna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
okolkováním	okolkování	k1gNnSc7	okolkování
federálních	federální	k2eAgNnPc2d1	federální
platidel	platidlo	k1gNnPc2	platidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozpadem	rozpad	k1gInSc7	rozpad
Československa	Československo	k1gNnSc2	Československo
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
destabilizaci	destabilizace	k1gFnSc3	destabilizace
poměrů	poměr	k1gInPc2	poměr
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k6eAd1	jen
se	se	k3xPyFc4	se
nefungující	fungující	k2eNgFnSc1d1	nefungující
federace	federace	k1gFnSc1	federace
změnila	změnit	k5eAaPmAgFnS	změnit
ústavní	ústavní	k2eAgFnSc7d1	ústavní
cestou	cesta	k1gFnSc7	cesta
ve	v	k7c4	v
dva	dva	k4xCgInPc4	dva
samostatné	samostatný	k2eAgInPc4d1	samostatný
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
vztahy	vztah	k1gInPc1	vztah
jsou	být	k5eAaImIp3nP	být
korektní	korektní	k2eAgInPc1d1	korektní
a	a	k8xC	a
přátelské	přátelský	k2eAgInPc1d1	přátelský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
Československa	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
svátky	svátek	k1gInPc1	svátek
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
Státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
ostatních	ostatní	k2eAgInPc6d1	ostatní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
významných	významný	k2eAgInPc6d1	významný
dnech	den	k1gInPc6	den
a	a	k8xC	a
o	o	k7c6	o
dnech	den	k1gInPc6	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
101	[number]	k4	101
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KRUŠINA	krušina	k1gFnSc1	krušina
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
MALOVECZKÁ	MALOVECZKÁ	kA	MALOVECZKÁ
<g/>
,	,	kIx,	,
Andrea	Andrea	k1gFnSc1	Andrea
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
–	–	k?	–
Slavné	slavný	k2eAgInPc4d1	slavný
dny	den	k1gInPc4	den
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2012-01-01	[number]	k4	2012-01-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
