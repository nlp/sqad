<s>
Den	den	k1gInSc1	den
obnovy	obnova	k1gFnSc2	obnova
samostatného	samostatný	k2eAgInSc2d1	samostatný
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
Deň	Deň	k1gFnSc1	Deň
vzniku	vznik	k1gInSc2	vznik
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Den	den	k1gInSc1	den
vzniku	vznik	k1gInSc2	vznik
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
státním	státní	k2eAgInSc7d1	státní
svátkem	svátek	k1gInSc7	svátek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
připadající	připadající	k2eAgFnSc2d1	připadající
na	na	k7c6	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
tedy	tedy	k9	tedy
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
