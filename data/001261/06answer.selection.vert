<s>
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1858	[number]	k4	1858
New	New	k1gMnSc2	New
York	York	k1gInSc4	York
City	city	k1gNnSc2	city
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
Oystar	Oystar	k1gMnSc1	Oystar
Bay	Bay	k1gMnSc1	Bay
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
také	také	k9	také
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
T.	T.	kA	T.
<g/>
R.	R.	kA	R.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
proslul	proslout	k5eAaPmAgMnS	proslout
svojí	svůj	k3xOyFgFnSc7	svůj
energickou	energický	k2eAgFnSc7d1	energická
povahou	povaha	k1gFnSc7	povaha
a	a	k8xC	a
šíří	šíř	k1gFnSc7	šíř
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
