<s>
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Kropotkin	Kropotkina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
ruském	ruský	k2eAgInSc6d1
městu	město	k1gNnSc3
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Kropotkin	Kropotkin	k1gInSc1
(	(	kIx(
<g/>
město	město	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
<g/>
,	,	kIx,
fotografováno	fotografován	k2eAgNnSc1d1
NadaremCelé	NadaremCelý	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
Kropotkin	Kropotkin	k2eAgInSc4d1
Region	region	k1gInSc4
</s>
<s>
ruská	ruský	k2eAgFnSc1d1
filosofiezápadní	filosofiezápadní	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
Období	období	k1gNnSc1
</s>
<s>
filosofie	filosofie	k1gFnSc1
19	#num#	k4
<g/>
.	.	kIx.
stoletífilosofie	stoletífilosofie	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Narození	narození	k1gNnPc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
prosincegreg	prosincegreg	k1gInSc1
<g/>
.	.	kIx.
/	/	kIx~
27	#num#	k4
<g/>
.	.	kIx.
listopadujul	listopadujula	k1gFnPc2
<g/>
.	.	kIx.
1842	#num#	k4
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1921	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
78	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Dmitrov	Dmitrov	k1gInSc1
<g/>
,	,	kIx,
Ruská	ruský	k2eAgFnSc1d1
sovětská	sovětský	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Škola	škola	k1gFnSc1
<g/>
/	/	kIx~
<g/>
tradice	tradice	k1gFnSc1
</s>
<s>
anarchokomunismus	anarchokomunismus	k1gInSc1
Oblasti	oblast	k1gFnSc2
zájmu	zájem	k1gInSc2
</s>
<s>
autorita	autorita	k1gFnSc1
<g/>
,	,	kIx,
spolupráce	spolupráce	k1gFnSc1
<g/>
,	,	kIx,
politika	politika	k1gFnSc1
<g/>
,	,	kIx,
revoluce	revoluce	k1gFnSc1
<g/>
,	,	kIx,
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
ekonomie	ekonomie	k1gFnSc1
<g/>
,	,	kIx,
zemědělství	zemědělství	k1gNnSc1
<g/>
,	,	kIx,
evoluce	evoluce	k1gFnSc1
<g/>
,	,	kIx,
geografie	geografie	k1gFnSc1
<g/>
,	,	kIx,
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
věda	věda	k1gFnSc1
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc1
<g/>
,	,	kIx,
etika	etika	k1gFnSc1
Význačné	význačný	k2eAgFnSc2d1
ideje	idea	k1gFnSc2
</s>
<s>
zakladatel	zakladatel	k1gMnSc1
anarchokomunismu	anarchokomunismus	k1gInSc2
Významná	významný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Mutual	Mutual	k1gInSc1
Aid	Aida	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
Factor	Factor	k1gInSc1
of	of	k?
Evolution	Evolution	k1gInSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
Conquest	Conquest	k1gFnSc1
of	of	k?
Bread	Bread	k1gInSc1
<g/>
,	,	kIx,
Fields	Fields	k1gInSc1
<g/>
,	,	kIx,
Factories	Factories	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Workshops	Workshops	k1gInSc1
<g/>
,	,	kIx,
The	The	k1gFnSc1
Great	Great	k1gInSc1
French	French	k1gMnSc1
Revolution	Revolution	k1gInSc1
<g/>
:	:	kIx,
1789	#num#	k4
<g/>
–	–	k?
<g/>
1793	#num#	k4
a	a	k8xC
Memoirs	Memoirs	k1gInSc1
of	of	k?
a	a	k8xC
Revolutionist	Revolutionist	k1gInSc4
Vlivy	vliv	k1gInPc1
</s>
<s>
Marx	Marx	k1gMnSc1
<g/>
,	,	kIx,
Engels	Engels	k1gMnSc1
<g/>
,	,	kIx,
Proudhon	Proudhon	k1gMnSc1
<g/>
,	,	kIx,
Bakunin	Bakunin	k2eAgMnSc1d1
<g/>
,	,	kIx,
Darwin	Darwin	k1gMnSc1
Vliv	vliv	k1gInSc1
na	na	k7c6
</s>
<s>
Berkman	Berkman	k1gMnSc1
<g/>
,	,	kIx,
Malatesta	Malatesta	k1gMnSc1
<g/>
,	,	kIx,
Wilde	Wild	k1gMnSc5
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tolstoj	Tolstoj	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Kafka	Kafka	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Machno	Machno	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Rocker	rocker	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Kō	Kō	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Brown	Brown	k1gMnSc1
<g/>
,	,	kIx,
Ward	Ward	k1gMnSc1
<g/>
,	,	kIx,
Galleani	Gallean	k1gMnPc1
<g/>
,	,	kIx,
Goldman	Goldman	k1gMnSc1
<g/>
,	,	kIx,
Woodcock	Woodcock	k1gMnSc1
<g/>
,	,	kIx,
Bookchin	Bookchin	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Black	Black	k1gMnSc1
<g/>
,	,	kIx,
Chomsky	Chomsky	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Gould	Gould	k1gInSc1
<g/>
¨	¨	k?
</s>
<s>
Ursula	Ursula	k1gFnSc1
K.	K.	kA
le	le	k?
Guin	Guin	k1gInSc1
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Kníže	kníže	k1gMnSc1
Petr	Petr	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
П	П	k?
А	А	k?
К	К	k?
<g/>
;	;	kIx,
27	#num#	k4
<g/>
.	.	kIx.
listopadujul	listopadujout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
/	/	kIx~
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1842	#num#	k4
<g/>
greg	grega	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Moskva	Moskva	k1gFnSc1
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1921	#num#	k4
<g/>
,	,	kIx,
Dmitrov	Dmitrov	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
revolucionář	revolucionář	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
<g/>
,	,	kIx,
geograf	geograf	k1gMnSc1
a	a	k8xC
geolog	geolog	k1gMnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
jeden	jeden	k4xCgMnSc1
z	z	k7c2
předních	přední	k2eAgMnPc2d1
ruských	ruský	k2eAgMnPc2d1
anarchistů	anarchista	k1gMnPc2
a	a	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
prvních	první	k4xOgMnPc2
zastánců	zastánce	k1gMnPc2
a	a	k8xC
teoretiků	teoretik	k1gMnPc2
anarchokomunismu	anarchokomunismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
knížecímu	knížecí	k2eAgInSc3d1
titulu	titul	k1gInSc3
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
důležitosti	důležitost	k1gFnSc2
jako	jako	k8xS,k8xC
anarchisty	anarchista	k1gMnSc2
byl	být	k5eAaImAgMnS
koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
začátkem	začátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nazýván	nazýván	k2eAgInSc4d1
„	„	k?
<g/>
Anarchistický	anarchistický	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oscar	Oscar	k1gInSc1
Wilde	Wild	k1gInSc5
ho	on	k3xPp3gInSc4
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
„	„	k?
<g/>
novým	nový	k2eAgMnSc7d1
Kristem	Kristus	k1gMnSc7
přicházejícím	přicházející	k2eAgMnSc7d1
z	z	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílel	podílet	k5eAaImAgMnS
se	se	k3xPyFc4
také	také	k9
jako	jako	k9
přispěvatel	přispěvatel	k1gMnSc1
do	do	k7c2
11	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
Encyclopæ	Encyclopæ	k1gFnSc2
Britannica	Britannic	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1911	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Kropotkinova	Kropotkinův	k2eAgFnSc1d1
kresba	kresba	k1gFnSc1
z	z	k7c2
Finska	Finsko	k1gNnSc2
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
rodině	rodina	k1gFnSc6
starobylého	starobylý	k2eAgInSc2d1
knížecího	knížecí	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
staršího	starý	k2eAgMnSc4d2
bratra	bratr	k1gMnSc4
Alexandra	Alexandr	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
bratranec	bratranec	k1gMnSc1
Dmitrij	Dmitrij	k1gMnSc1
byl	být	k5eAaImAgMnS
později	pozdě	k6eAd2
zavražděn	zavraždit	k5eAaPmNgMnS
revolucionářem	revolucionář	k1gMnSc7
Grigorijem	Grigorij	k1gMnSc7
Goldenbergem	Goldenberg	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
15	#num#	k4
letech	léto	k1gNnPc6
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
studia	studio	k1gNnPc4
v	v	k7c6
prestižní	prestižní	k2eAgFnSc6d1
aristokratické	aristokratický	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
<g/>
,	,	kIx,
do	do	k7c2
Pážecího	pážecí	k2eAgInSc2d1
sboru	sbor	k1gInSc2
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc3
<g/>
,	,	kIx,
po	po	k7c6
jejichž	jejichž	k3xOyRp3gNnSc6
dokončení	dokončení	k1gNnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
komorníkem	komorník	k1gMnSc7
cara	car	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1868	#num#	k4
získal	získat	k5eAaPmAgMnS
důstojnickou	důstojnický	k2eAgFnSc4d1
hodnost	hodnost	k1gFnSc4
a	a	k8xC
odešel	odejít	k5eAaPmAgMnS
sloužit	sloužit	k5eAaImF
k	k	k7c3
Amurskému	amurský	k2eAgNnSc3d1
kozáckému	kozácký	k2eAgNnSc3d1
vojsku	vojsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1862	#num#	k4
odjel	odjet	k5eAaPmAgMnS
na	na	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
na	na	k7c4
Sibiř	Sibiř	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgMnS
vědeckou	vědecký	k2eAgFnSc7d1
a	a	k8xC
společenskou	společenský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
(	(	kIx(
<g/>
účastnil	účastnit	k5eAaImAgMnS
se	se	k3xPyFc4
geografických	geografický	k2eAgFnPc2d1
expedicí	expedice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určil	určit	k5eAaPmAgMnS
směr	směr	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
se	se	k3xPyFc4
táhnou	táhnout	k5eAaImIp3nP
základní	základní	k2eAgInPc1d1
horské	horský	k2eAgInPc1d1
masivy	masiv	k1gInPc1
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
od	od	k7c2
jihozápadu	jihozápad	k1gInSc2
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
vystoupil	vystoupit	k5eAaPmAgMnS
s	s	k7c7
hypotézou	hypotéza	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
základem	základ	k1gInSc7
byla	být	k5eAaImAgFnS
myšlenka	myšlenka	k1gFnSc1
o	o	k7c6
dávném	dávný	k2eAgNnSc6d1
zalednění	zalednění	k1gNnSc6
celé	celý	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1867	#num#	k4
odjel	odjet	k5eAaPmAgMnS
do	do	k7c2
Petrohradu	Petrohrad	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
zaměstnání	zaměstnání	k1gNnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
členem	člen	k1gMnSc7
Ruské	ruský	k2eAgFnSc2d1
geografické	geografický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1872	#num#	k4
se	se	k3xPyFc4
po	po	k7c6
cestě	cesta	k1gFnSc6
do	do	k7c2
Švýcarska	Švýcarsko	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
Jurské	jurský	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
následovníky	následovník	k1gMnPc7
svého	svůj	k1gMnSc2
krajana	krajan	k1gMnSc2
Michaila	Michail	k1gMnSc2
Bakunina	Bakunin	k2eAgMnSc2d1
a	a	k8xC
přešel	přejít	k5eAaPmAgMnS
na	na	k7c4
pozice	pozice	k1gFnPc4
anarchismu	anarchismus	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
Michailem	Michail	k1gMnSc7
Bakuninem	Bakunin	k1gInSc7
se	se	k3xPyFc4
však	však	k9
nikdy	nikdy	k6eAd1
nesetkal	setkat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Ruska	Rusko	k1gNnSc2
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
anarchistické	anarchistický	k2eAgFnSc3d1
propagandě	propaganda	k1gFnSc3
<g/>
,	,	kIx,
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
kroužku	kroužek	k1gInSc2
„	„	k?
<g/>
čajkovců	čajkovec	k1gInPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
po	po	k7c6
jehož	jehož	k3xOyRp3gInSc6
zničení	zničení	k1gNnSc6
byl	být	k5eAaImAgMnS
zatčen	zatknout	k5eAaPmNgMnS
a	a	k8xC
uvězněn	uvěznit	k5eAaPmNgMnS
v	v	k7c6
Petropavlovské	petropavlovský	k2eAgFnSc6d1
pevnosti	pevnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
uskutečnil	uskutečnit	k5eAaPmAgInS
útěk	útěk	k1gInSc4
z	z	k7c2
vězeňské	vězeňský	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
a	a	k8xC
prchl	prchnout	k5eAaPmAgMnS
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
40	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
exilu	exil	k1gInSc6
se	se	k3xPyFc4
z	z	k7c2
něho	on	k3xPp3gMnSc2
stal	stát	k5eAaPmAgMnS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
činitelů	činitel	k1gMnPc2
anarchistického	anarchistický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
–	–	k?
vystupoval	vystupovat	k5eAaImAgInS
na	na	k7c6
schůzích	schůze	k1gFnPc6
<g/>
,	,	kIx,
účastnil	účastnit	k5eAaImAgMnS
se	se	k3xPyFc4
kongresů	kongres	k1gInPc2
anarchistické	anarchistický	k2eAgFnSc2d1
Internacionály	Internacionála	k1gFnSc2
a	a	k8xC
veřejných	veřejný	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
psal	psát	k5eAaImAgInS
propagační	propagační	k2eAgFnPc4d1
publikace	publikace	k1gFnPc4
a	a	k8xC
založil	založit	k5eAaPmAgMnS
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
První	první	k4xOgFnSc2
internacionály	internacionála	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1872	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1881	#num#	k4
byl	být	k5eAaImAgInS
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
aktivity	aktivita	k1gFnPc4
vyhoštěn	vyhoštěn	k2eAgMnSc1d1
ze	z	k7c2
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
na	na	k7c4
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
<g/>
,	,	kIx,
ač	ač	k8xS
nevinen	vinen	k2eNgMnSc1d1
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
odsouzen	odsouzen	k2eAgMnSc1d1
k	k	k7c3
pěti	pět	k4xCc3
letům	let	k1gInPc3
vězení	vězení	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
byl	být	k5eAaImAgInS
pod	pod	k7c7
tlakem	tlak	k1gInSc7
veřejnosti	veřejnost	k1gFnSc2
předčasně	předčasně	k6eAd1
propuštěn	propuštěn	k2eAgMnSc1d1
a	a	k8xC
odjel	odjet	k5eAaPmAgMnS
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
vyšla	vyjít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gNnPc4
nejznámější	známý	k2eAgNnPc4d3
díla	dílo	k1gNnPc4
jak	jak	k6eAd1
z	z	k7c2
oblasti	oblast	k1gFnSc2
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
především	především	k9
geografie	geografie	k1gFnSc1
<g/>
)	)	kIx)
tak	tak	k9
i	i	k9
anarchistické	anarchistický	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1892	#num#	k4
přispíval	přispívat	k5eAaImAgInS
po	po	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
do	do	k7c2
anglického	anglický	k2eAgNnSc2d1
periodika	periodikum	k1gNnSc2
Nineteenth	Nineteentha	k1gFnPc2
Century	Centura	k1gFnSc2
do	do	k7c2
rubriky	rubrika	k1gFnSc2
Recent	Recent	k1gMnSc1
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
před	před	k7c7
ním	on	k3xPp3gMnSc7
vedl	vést	k5eAaImAgMnS
Thomas	Thomas	k1gMnSc1
Henry	henry	k1gInSc2
Huxley	Huxlea	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
únorové	únorový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
1917	#num#	k4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
jeho	jeho	k3xOp3gNnSc4
působení	působení	k1gNnSc4
na	na	k7c4
ruský	ruský	k2eAgInSc4d1
lid	lid	k1gInSc4
bylo	být	k5eAaImAgNnS
brzy	brzy	k6eAd1
omezeno	omezit	k5eAaPmNgNnS
bolševickou	bolševický	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
–	–	k?
byl	být	k5eAaImAgInS
vystěhován	vystěhovat	k5eAaPmNgInS
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
do	do	k7c2
Dmitrova	Dmitrův	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgInPc6
dopisech	dopis	k1gInPc6
se	se	k3xPyFc4
ještě	ještě	k6eAd1
snažil	snažit	k5eAaImAgMnS
varovat	varovat	k5eAaImF
dělníky	dělník	k1gMnPc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
před	před	k7c7
bolševickou	bolševický	k2eAgFnSc7d1
diktaturou	diktatura	k1gFnSc7
–	–	k?
ale	ale	k8xC
marně	marně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhy	záhy	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
pohřeb	pohřeb	k1gInSc4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
s	s	k7c7
Leninovým	Leninův	k2eAgNnSc7d1
povolením	povolení	k1gNnSc7
posledním	poslední	k2eAgNnSc7d1
hromadným	hromadný	k2eAgNnSc7d1
shromážděním	shromáždění	k1gNnSc7
anarchistů	anarchista	k1gMnPc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
až	až	k9
do	do	k7c2
rozpadu	rozpad	k1gInSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Myšlení	myšlení	k1gNnSc1
</s>
<s>
Kropotkin	Kropotkin	k1gMnSc1
s	s	k7c7
ministrem	ministr	k1gMnSc7
války	válka	k1gFnSc2
Pavlem	Pavel	k1gMnSc7
Miljukovem	Miljukov	k1gInSc7
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
roku	rok	k1gInSc2
1917	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
byl	být	k5eAaImAgMnS
významným	významný	k2eAgMnSc7d1
kritikem	kritik	k1gMnSc7
sociálního	sociální	k2eAgInSc2d1
darwinismu	darwinismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kropotkin	Kropotkin	k1gMnSc1
nepřijal	přijmout	k5eNaPmAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
biologie	biologie	k1gFnSc1
podněcuje	podněcovat	k5eAaImIp3nS
boj	boj	k1gInSc4
o	o	k7c4
život	život	k1gInSc4
a	a	k8xC
soutěž	soutěž	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgNnSc2
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
úspěšné	úspěšný	k2eAgNnSc1d1
byly	být	k5eAaImAgInP
takové	takový	k3xDgInPc1
druhy	druh	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
využily	využít	k5eAaPmAgInP
kolektivní	kolektivní	k2eAgFnSc4d1
energii	energie	k1gFnSc4
a	a	k8xC
byly	být	k5eAaImAgFnP
schopné	schopný	k2eAgFnPc1d1
vzájemné	vzájemný	k2eAgFnPc1d1
pomoci	pomoc	k1gFnPc1
neboli	neboli	k8xC
spolupráce	spolupráce	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Jeden	jeden	k4xCgInSc1
odvážný	odvážný	k2eAgInSc1d1
čin	čin	k1gInSc1
vykoná	vykonat	k5eAaPmIp3nS
více	hodně	k6eAd2
práce	práce	k1gFnSc2
nežli	nežli	k8xS
tisíce	tisíc	k4xCgInSc2
brožurek	brožurka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Etika	etika	k1gFnSc1
anarchismu	anarchismus	k1gInSc2
</s>
<s>
Buřičovy	Buřičův	k2eAgFnPc1d1
řeči	řeč	k1gFnPc1
<g/>
,	,	kIx,
přístupná	přístupný	k2eAgFnSc1d1
kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Pařížská	pařížský	k2eAgFnSc1d1
komuna	komuna	k1gFnSc1
<g/>
“	“	k?
na	na	k7c4
Anarchy	Anarch	k1gMnPc4
Wiki	Wik	k1gFnSc2
a	a	k8xC
kapitola	kapitola	k1gFnSc1
„	„	k?
<g/>
Revoluční	revoluční	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
“	“	k?
na	na	k7c6
AP	ap	kA
Čítárně	čítárna	k1gFnSc6
</s>
<s>
Dobývání	dobývání	k1gNnSc1
chleba	chléb	k1gInSc2
</s>
<s>
Historická	historický	k2eAgFnSc1d1
úloha	úloha	k1gFnSc1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
přístupné	přístupný	k2eAgNnSc1d1
ve	v	k7c6
slovenštině	slovenština	k1gFnSc6
na	na	k7c6
Anarchy	Anarcha	k1gMnSc2
Wiki	Wik	k1gFnSc6
</s>
<s>
Komunismus	komunismus	k1gInSc1
a	a	k8xC
anarchie	anarchie	k1gFnPc1
<g/>
,	,	kIx,
přístupné	přístupný	k2eAgInPc1d1
na	na	k7c6
AP	ap	kA
Čítárně	čítárna	k1gFnSc6
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
anarchie	anarchie	k1gFnSc1
</s>
<s>
Paměti	paměť	k1gFnPc1
Revolucionáře	revolucionář	k1gMnSc2
</s>
<s>
Pole	pole	k1gFnPc1
<g/>
,	,	kIx,
továrny	továrna	k1gFnPc1
a	a	k8xC
dílny	dílna	k1gFnPc1
</s>
<s>
Pospolitost	pospolitost	k1gFnSc1
</s>
<s>
Současná	současný	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
anarchie	anarchie	k1gFnSc1
</s>
<s>
Vydání	vydání	k1gNnSc1
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
Mládeži	mládež	k1gFnSc3
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc4
<g/>
,	,	kIx,
Budoucnost	budoucnost	k1gFnSc1
<g/>
,	,	kIx,
1885	#num#	k4
<g/>
,	,	kIx,
40	#num#	k4
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
Epištoly	epištola	k1gFnSc2
revoluce	revoluce	k1gFnSc2
č.	č.	k?
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1885	#num#	k4
</s>
<s>
Revoluční	revoluční	k2eAgFnPc1d1
vlády	vláda	k1gFnPc1
<g/>
,	,	kIx,
New	New	k1gFnPc1
York	York	k1gInSc1
<g/>
,	,	kIx,
časopis	časopis	k1gInSc1
Volné	volný	k2eAgFnSc2d1
listy	lista	k1gFnSc2
<g/>
,	,	kIx,
1890	#num#	k4
</s>
<s>
Zákon	zákon	k1gInSc1
a	a	k8xC
autorita	autorita	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Skupina	skupina	k1gFnSc1
Bezvládí	bezvládí	k1gNnSc2
<g/>
,	,	kIx,
1893	#num#	k4
</s>
<s>
Revoluční	revoluční	k2eAgMnSc1d1
duch	duch	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Skupina	skupina	k1gFnSc1
Bezvládí	bezvládí	k1gNnSc2
<g/>
,	,	kIx,
1894	#num#	k4
</s>
<s>
Mravouka	mravouka	k1gFnSc1
anarchie	anarchie	k1gFnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
M.	M.	kA
Hodek	Hodek	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Dělnické	dělnický	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
,	,	kIx,
1896	#num#	k4
</s>
<s>
Slova	slovo	k1gNnSc2
revolucionáře	revolucionář	k1gMnSc2
:	:	kIx,
revoluční	revoluční	k2eAgFnPc1d1
menšiny	menšina	k1gFnPc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
J.	J.	kA
A.	A.	kA
P.	P.	kA
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Dělnické	dělnický	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
,	,	kIx,
1897	#num#	k4
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1
komuna	komuna	k1gFnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
J.	J.	kA
Nekvasil	kvasit	k5eNaImAgInS
<g/>
,	,	kIx,
New	New	k1gFnPc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Dělnické	dělnický	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
,	,	kIx,
1898	#num#	k4
</s>
<s>
Kollektivism	Kollektivism	k1gInSc1
a	a	k8xC
komunism	komunism	k1gInSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Vilém	Vilém	k1gMnSc1
Körber	Körber	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
nákladem	náklad	k1gInSc7
redakce	redakce	k1gFnSc2
Omladiny	Omladina	k1gFnSc2
(	(	kIx(
<g/>
Viléma	Vilém	k1gMnSc2
Körbera	Körber	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1899	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
stran	strana	k1gFnPc2
</s>
<s>
Slova	slovo	k1gNnPc1
vzpůrcova	vzpůrcův	k2eAgNnPc1d1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Skupina	skupina	k1gFnSc1
Bezvládí	bezvládí	k1gNnSc2
<g/>
,	,	kIx,
1898	#num#	k4
</s>
<s>
Slova	slovo	k1gNnPc1
vzpůrcova	vzpůrcův	k2eAgNnPc1d1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Skupina	skupina	k1gFnSc1
Bezvládí	bezvládí	k1gNnSc2
<g/>
,	,	kIx,
1899	#num#	k4
</s>
<s>
Slova	slovo	k1gNnPc1
vzpůrcova	vzpůrcův	k2eAgNnPc1d1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
Pařížská	pařížský	k2eAgFnSc1d1
komuna	komuna	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Skupina	skupina	k1gFnSc1
Bezvládí	bezvládí	k1gNnSc2
<g/>
,	,	kIx,
1900	#num#	k4
</s>
<s>
Kommunismus	Kommunismus	k1gInSc1
a	a	k8xC
anarchie	anarchie	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
1901	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
kult	kult	k1gInSc1
<g/>
,	,	kIx,
1903	#num#	k4
</s>
<s>
Paměti	paměť	k1gFnPc1
revolucionářovy	revolucionářův	k2eAgFnPc1d1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Emanuel	Emanuel	k1gMnSc1
Volný	volný	k2eAgMnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Samostatnost	samostatnost	k1gFnSc1
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
–	–	k?
<g/>
1907	#num#	k4
</s>
<s>
Anarchistická	anarchistický	k2eAgFnSc1d1
morálka	morálka	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Volné	volný	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
,	,	kIx,
1907	#num#	k4
<g/>
,	,	kIx,
40	#num#	k4
stran	strana	k1gFnPc2
</s>
<s>
Buřičovy	Buřičův	k2eAgFnPc1d1
řeči	řeč	k1gFnPc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Petr	Petr	k1gMnSc1
Pohan	pohan	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Čas	čas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komuna	komuna	k1gFnSc1
(	(	kIx(
<g/>
Karel	Karel	k1gMnSc1
Vohryzek	Vohryzek	k1gInSc1
a	a	k8xC
Ladislav	Ladislav	k1gMnSc1
Knotek	Knotek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1907	#num#	k4
<g/>
,	,	kIx,
213	#num#	k4
stran	strana	k1gFnPc2
</s>
<s>
Kollektivistické	Kollektivistický	k2eAgNnSc1d1
námezdnictví	námezdnictví	k1gNnSc1
<g/>
,	,	kIx,
výňatek	výňatek	k1gInSc1
z	z	k7c2
La	la	k1gNnSc2
conquê	conquê	k5eAaPmIp2nP
du	du	k?
pain	pain	k1gInSc4
<g/>
,	,	kIx,
překlad	překlad	k1gInSc4
V.	V.	kA
Icha	Ich	k1gInSc2
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
Sdružení	sdružení	k1gNnSc1
českých	český	k2eAgMnPc2d1
neodvislých	odvislý	k2eNgMnPc2d1
socialistů	socialist	k1gMnPc2
<g/>
,	,	kIx,
1914	#num#	k4
</s>
<s>
Anarchistická	anarchistický	k2eAgFnSc1d1
morálka	morálka	k1gFnSc1
:	:	kIx,
Komunism	Komunism	k1gInSc1
a	a	k8xC
anarchie	anarchie	k1gFnSc1
:	:	kIx,
Velká	velký	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Fr.	Fr.	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1919	#num#	k4
<g/>
,	,	kIx,
137	#num#	k4
stran	strana	k1gFnPc2
</s>
<s>
Mladým	mladý	k2eAgMnPc3d1
lidem	člověk	k1gMnPc3
:	:	kIx,
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
čekání	čekání	k1gNnSc2
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Junius	Junius	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Fr.	Fr.	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
:	:	kIx,
Průlom	průlom	k1gInSc1
<g/>
,	,	kIx,
1920	#num#	k4
</s>
<s>
Pole	pole	k1gFnPc1
<g/>
,	,	kIx,
továrny	továrna	k1gFnPc1
a	a	k8xC
dílny	dílna	k1gFnPc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
Vlasta	Vlasta	k1gMnSc1
Borek	Borek	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Mladé	mladý	k2eAgInPc1d1
proudy	proud	k1gInPc1
<g/>
,	,	kIx,
1920	#num#	k4
</s>
<s>
Pospolitost	pospolitost	k1gFnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
z	z	k7c2
angl.	angl.	k?
A.	A.	kA
A.	A.	kA
Hoch	hoch	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Družstvo	družstvo	k1gNnSc1
Kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
1922	#num#	k4
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
anarchie	anarchie	k1gFnSc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
V.	V.	kA
Borek	borek	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
B.	B.	kA
Vrbenský	Vrbenský	k2eAgMnSc1d1
a	a	k8xC
V.	V.	kA
Borek	borek	k1gInSc1
<g/>
,	,	kIx,
1922	#num#	k4
</s>
<s>
Herbert	Herbert	k1gMnSc1
Spencer	Spencer	k1gMnSc1
–	–	k?
jeho	jeho	k3xOp3gFnPc1
filosofie	filosofie	k1gFnPc1
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
V.	V.	kA
Borek	borek	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
B.	B.	kA
Vrbenský	Vrbenský	k2eAgMnSc1d1
a	a	k8xC
V.	V.	kA
Borek	borek	k1gInSc1
<g/>
,	,	kIx,
1923	#num#	k4
</s>
<s>
Historická	historický	k2eAgFnSc1d1
úloha	úloha	k1gFnSc1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
V.	V.	kA
Borek	borek	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
B.	B.	kA
Vrbenský	Vrbenský	k2eAgMnSc1d1
a	a	k8xC
V.	V.	kA
Borek	borek	k1gInSc1
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
</s>
<s>
Anarchistická	anarchistický	k2eAgFnSc1d1
morálka	morálka	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Doutnák	doutnák	k1gInSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
bez	bez	k7c2
ISBN	ISBN	kA
<g/>
,	,	kIx,
28	#num#	k4
stran	strana	k1gFnPc2
</s>
<s>
Anarchistická	anarchistický	k2eAgFnSc1d1
etika	etika	k1gFnSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
(	(	kIx(
<g/>
překlad	překlad	k1gInSc1
Alan	Alan	k1gMnSc1
Černohous	Černohous	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Votobia	Votobia	k1gFnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
2000	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7198	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
50	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
128	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
překlady	překlad	k1gInPc4
děl	dělo	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
Etické	etický	k2eAgInPc1d1
principy	princip	k1gInPc1
anarchismu	anarchismus	k1gInSc2
(	(	kIx(
<g/>
původní	původní	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
pod	pod	k7c7
názvem	název	k1gInSc7
Morále	Morál	k1gMnSc5
Anarchiste	Anarchist	k1gMnSc5
<g/>
,	,	kIx,
noviny	novina	k1gFnPc1
La	la	k1gNnSc2
Révolte	Révolte	k1gFnPc2
<g/>
,	,	kIx,
1890	#num#	k4
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
vycházelo	vycházet	k5eAaImAgNnS
též	též	k9
pod	pod	k7c7
názvy	název	k1gInPc7
Etika	etik	k1gMnSc2
anarchismu	anarchismus	k1gInSc2
či	či	k8xC
Chléb	chléb	k1gInSc1
a	a	k8xC
svoboda	svoboda	k1gFnSc1
–	–	k?
např.	např.	kA
Londýn	Londýn	k1gInSc4
1907	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spravedlnost	spravedlnost	k1gFnSc1
a	a	k8xC
mravnost	mravnost	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
původní	původní	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
přednáška	přednáška	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1888	#num#	k4
v	v	k7c6
posluchárně	posluchárna	k1gFnSc6
Ancotského	Ancotský	k2eAgNnSc2d1
bratrstva	bratrstvo	k1gNnSc2
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
<g/>
,	,	kIx,
tištěná	tištěný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
rusky	rusky	k6eAd1
1920	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Morální	morální	k2eAgFnSc1d1
volba	volba	k1gFnSc1
L.	L.	kA
N.	N.	kA
Tolstého	Tolstý	k2eAgNnSc2d1
(	(	kIx(
<g/>
původně	původně	k6eAd1
série	série	k1gFnSc1
8	#num#	k4
přednášek	přednáška	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
na	na	k7c6
Lowellsově	Lowellsův	k2eAgInSc6d1
institutu	institut	k1gInSc6
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
Bostonu	Boston	k1gInSc6
<g/>
,	,	kIx,
knižně	knižně	k6eAd1
vyšlo	vyjít	k5eAaPmAgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
Kropotkinovy	Kropotkinův	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
Ideály	ideál	k1gInPc1
a	a	k8xC
realita	realita	k1gFnSc1
v	v	k7c6
ruské	ruský	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
smyslu	smysl	k1gInSc6
odplaty	odplata	k1gFnSc2
(	(	kIx(
<g/>
vyňatá	vyňatý	k2eAgFnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
kapitola	kapitola	k1gFnSc1
z	z	k7c2
knihy	kniha	k1gFnSc2
O	o	k7c6
francouzských	francouzský	k2eAgFnPc6d1
a	a	k8xC
ruských	ruský	k2eAgFnPc6d1
věznicích	věznice	k1gFnPc6
<g/>
,	,	kIx,
Petrohrad	Petrohrad	k1gInSc1
1906	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
jiný	jiný	k2eAgInSc1d1
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
Etických	etický	k2eAgInPc2d1
principů	princip	k1gInPc2
anarchismu	anarchismus	k1gInSc2
<g/>
:	:	kIx,
Anarchistická	anarchistický	k2eAgFnSc1d1
morálka	morálka	k1gFnSc1
<g/>
,	,	kIx,
Kalendář	kalendář	k1gInSc1
revolucionářů	revolucionář	k1gMnPc2
na	na	k7c4
rok	rok	k1gInSc4
1904	#num#	k4
<g/>
,	,	kIx,
Anarchy	Anarcha	k1gFnPc1
Wiki	Wik	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MARSHALL	MARSHALL	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demanding	Demanding	k1gInSc1
the	the	k?
Impossible	Impossible	k1gFnSc2
<g/>
:	:	kIx,
A	a	k8xC
History	Histor	k1gInPc1
of	of	k?
Anarchism	Anarchism	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
PM	PM	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781604862706	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
177	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TOLSTOJ	Tolstoj	k1gMnSc1
<g/>
,	,	kIx,
Lev	Lev	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
<g/>
;	;	kIx,
MobileReference	MobileReference	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Works	Works	kA
of	of	k?
Leo	Leo	k1gMnSc1
Tolstoy	Tolstoa	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
MobileReference	MobileReference	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781605011561	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GRAY	GRAY	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
T.	T.	kA
A	A	kA
Franz	Franz	k1gMnSc1
Kafka	Kafka	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
Publishing	Publishing	k1gInSc1
Group	Group	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780313303753	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
170	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SCHARLAU	SCHARLAU	kA
<g/>
,	,	kIx,
Winfried	Winfried	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Who	Who	k1gFnSc1
is	is	k?
Alexander	Alexandra	k1gFnPc2
Grothendieck	Grothendieck	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Part	part	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
Anarchy	Anarcha	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Books	Books	k1gInSc1
on	on	k3xPp3gInSc1
Demand	Demand	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9783842340923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GRAUR	GRAUR	kA
<g/>
,	,	kIx,
Mina	mina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gMnSc1
Anarchist	Anarchist	k1gMnSc1
Rabbi	Rabb	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc1
Life	Lif	k1gFnSc2
and	and	k?
Teachings	Teachings	k1gInSc1
of	of	k?
Rudolf	Rudolf	k1gMnSc1
Rocker	rocker	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
St.	st.	kA
Martin	Martin	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
312	#num#	k4
<g/>
-	-	kIx~
<g/>
17273	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
22	#num#	k4
<g/>
–	–	k?
<g/>
36	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PEREZ	PEREZ	kA
<g/>
,	,	kIx,
Louis	Louis	k1gMnSc1
G.	G.	kA
Japan	japan	k1gInSc1
at	at	k?
War	War	k1gFnSc1
<g/>
:	:	kIx,
An	An	k1gFnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
ABC-CLIO	ABC-CLIO	k1gFnSc1
ISBN	ISBN	kA
9781598847420	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kō	Kō	k1gInSc2
Shū	Shū	k1gFnSc2
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
–	–	k?
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
190	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BOOKCHIN	BOOKCHIN	kA
<g/>
,	,	kIx,
Murray	Murray	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Ecology	Ecologa	k1gFnSc2
of	of	k?
Freedom	Freedom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oakland	Oakland	k1gInSc1
<g/>
:	:	kIx,
AK	AK	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Noam	Noam	k1gMnSc1
Chomsky	Chomsky	k1gFnSc2
Reading	Reading	k1gInSc1
List	list	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Left	Left	k1gInSc1
Reference	reference	k1gFnSc2
Guide	Guid	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HUDEČEK	Hudeček	k1gMnSc1
<g/>
,	,	kIx,
Theodor	Theodor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peter	Peter	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
:	:	kIx,
Život	život	k1gInSc1
a	a	k8xC
dielo	dielo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Slovenské	slovenský	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelstvo	nakladatelstvo	k1gNnSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KROPOTKIN	KROPOTKIN	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
anarchie	anarchie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Socialistická	socialistický	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
SOLIDARITA	solidarita	k1gFnSc1
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politické	politický	k2eAgFnPc4d1
ideologie	ideologie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Victoria	Victorium	k1gNnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85865	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kolektivistický	kolektivistický	k2eAgInSc1d1
anarchismus	anarchismus	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
194	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TOMEK	Tomek	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
;	;	kIx,
SLAČÁLEK	SLAČÁLEK	kA
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anarchismus	anarchismus	k1gInSc1
:	:	kIx,
svoboda	svoboda	k1gFnSc1
proti	proti	k7c3
moci	moc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
781	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buřičova	Buřičův	k2eAgFnSc1d1
morálka	morálka	k1gFnSc1
a	a	k8xC
pospolitost	pospolitost	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
200	#num#	k4
<g/>
-	-	kIx~
<g/>
236	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HUDEČEK	Hudeček	k1gMnSc1
<g/>
,	,	kIx,
Theodor	Theodor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peter	Peter	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
:	:	kIx,
Život	život	k1gInSc1
a	a	k8xC
dielo	dielo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Slovenské	slovenský	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelstvo	nakladatelstvo	k1gNnSc1
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
72	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
PURCHASE	PURCHASE	kA
<g/>
,	,	kIx,
Graham	Graham	k1gMnSc1
(	(	kIx(
<g/>
přeložil	přeložit	k5eAaPmAgMnS
WOHLMUTH	WOHLMUTH	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evoluce	evoluce	k1gFnSc2
a	a	k8xC
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
života	život	k1gInSc2
a	a	k8xC
myšlenek	myšlenka	k1gFnPc2
P.	P.	kA
A.	A.	kA
Kropotkina	Kropotkina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Neklid	neklid	k1gInSc1
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
256	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
907562	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Jurská	jurský	k2eAgFnSc1d1
federace	federace	k1gFnSc1
</s>
<s>
Anarchokomunismus	anarchokomunismus	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autor	autor	k1gMnSc1
Peter	Peter	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Osoba	osoba	k1gFnSc1
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
</s>
<s>
Plné	plný	k2eAgInPc4d1
texty	text	k1gInPc4
děl	dít	k5eAaBmAgMnS,k5eAaImAgMnS
autora	autor	k1gMnSc2
Peter	Peter	k1gMnSc1
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
na	na	k7c6
projektu	projekt	k1gInSc6
Gutenberg	Gutenberg	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Stepňak	Stepňak	k1gMnSc1
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Kropotkin	Kropotkin	k1gMnSc1
–	–	k?
Československá	československý	k2eAgFnSc1d1
anarchistická	anarchistický	k2eAgFnSc1d1
federace	federace	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Anarchy	Anarch	k1gInPc1
Wiki	Wik	k1gFnSc2
–	–	k?
Československá	československý	k2eAgFnSc1d1
anarchistická	anarchistický	k2eAgFnSc1d1
federace	federace	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Archiv	archiv	k1gInSc1
Petra	Petr	k1gMnSc2
Kropotkina	Kropotkin	k1gMnSc2
–	–	k?
Anarchy	Anarcha	k1gFnSc2
Archives	Archivesa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Kropotkinův	Kropotkinův	k2eAgInSc1d1
příspěvek	příspěvek	k1gInSc1
do	do	k7c2
Encyclopæ	Encyclopæ	k1gNnSc2
Britannica	Britannic	k1gInSc2
-	-	kIx~
Anarchism	Anarchism	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Nadarovy	Nadarův	k2eAgInPc1d1
portréty	portrét	k1gInPc1
Petra	Petr	k1gMnSc2
Kropotkina	Kropotkin	k1gMnSc2
v	v	k7c6
archivu	archiv	k1gInSc6
La	la	k1gNnSc2
Médiathè	Médiathè	k1gFnSc2
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
architecture	architectur	k1gMnSc5
et	et	k?
du	du	k?
patrimoine	patrimoinout	k5eAaPmIp3nS
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Shrnující	shrnující	k2eAgFnSc1d1
odborná	odborný	k2eAgFnSc1d1
studie	studie	k1gFnSc1
"	"	kIx"
<g/>
Evoluce	evoluce	k1gFnSc1
a	a	k8xC
revoluce	revoluce	k1gFnSc1
-	-	kIx~
úvod	úvod	k1gInSc1
do	do	k7c2
života	život	k1gInSc2
a	a	k8xC
díla	dílo	k1gNnSc2
P.	P.	kA
A.	A.	kA
Kropotkina	Kropotkina	k1gFnSc1
<g/>
"	"	kIx"
od	od	k7c2
australského	australský	k2eAgMnSc2d1
anarchisty	anarchista	k1gMnSc2
Grahama	Graham	k1gMnSc2
Purchase	Purchas	k1gInSc6
-	-	kIx~
nakladatelství	nakladatelství	k1gNnSc6
Anarchistická	anarchistický	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
(	(	kIx(
<g/>
jaime	jaimat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19992015180	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118567055	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2281	#num#	k4
103X	103X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79033037	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
66469467	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79033037	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
