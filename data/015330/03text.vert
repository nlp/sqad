<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Železné	železný	k2eAgFnSc2d1
horyIUCN	horyIUCN	k?
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
NPR	NPR	kA
Lichnice	Lichnice	k1gFnPc1
–	–	k?
Kaňkovy	Kaňkův	k2eAgFnPc1d1
hory	hora	k1gFnPc1
z	z	k7c2
vyhlídky	vyhlídka	k1gFnSc2
Dívčí	dívčí	k2eAgFnSc2d1
kámenZákladní	kámenZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
268	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
284	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInPc5
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
a	a	k8xC
Vysočina	vysočina	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
2,74	2,74	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
56,57	56,57	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
65	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.zeleznehory.ochranaprirody.cz	www.zeleznehory.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
byla	být	k5eAaImAgFnS
ustanovena	ustanovit	k5eAaPmNgFnS
vyhláškou	vyhláška	k1gFnSc7
Ministerstva	ministerstvo	k1gNnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
ČR	ČR	kA
č.	č.	k?
<g/>
156	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
(	(	kIx(
<g/>
účinnost	účinnost	k1gFnSc1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1991	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
rozloze	rozloha	k1gFnSc6
284	#num#	k4
km²	km²	k?
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
zhruba	zhruba	k6eAd1
mezi	mezi	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
městy	město	k1gNnPc7
Slatiňany	Slatiňany	k1gInPc4
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
Chotěboř	Chotěboř	k1gFnSc1
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
Třemošnice	Třemošnice	k1gFnSc1
na	na	k7c6
západě	západ	k1gInSc6
a	a	k8xC
Trhová	trhový	k2eAgFnSc1d1
Kamenice	Kamenice	k1gFnSc1
a	a	k8xC
Nasavrky	Nasavrky	k1gFnPc1
na	na	k7c6
východě	východ	k1gInSc6
(	(	kIx(
<g/>
49	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
'	'	kIx"
<g/>
-	-	kIx~
49	#num#	k4
<g/>
°	°	k?
<g/>
42	#num#	k4
<g/>
'	'	kIx"
N	N	kA
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
'	'	kIx"
–	–	k?
15	#num#	k4
<g/>
°	°	k?
<g/>
53	#num#	k4
<g/>
'	'	kIx"
<g/>
E	E	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
kolísá	kolísat	k5eAaImIp3nS
od	od	k7c2
268	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
u	u	k7c2
Slatiňan	Slatiňany	k1gInPc2
a	a	k8xC
nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
Vestcem	Vestec	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
měří	měřit	k5eAaImIp3nS
668	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Nasavrkách	Nasavrka	k1gFnPc6
(	(	kIx(
<g/>
Náměstí	náměstí	k1gNnSc1
317	#num#	k4
<g/>
,	,	kIx,
538	#num#	k4
25	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHKO	CHKO	kA
má	mít	k5eAaImIp3nS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
území	území	k1gNnSc6
24	#num#	k4
maloplošných	maloplošný	k2eAgFnPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgFnPc2d1
území	území	k1gNnPc2
přírody	příroda	k1gFnSc2
–	–	k?
1	#num#	k4
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
(	(	kIx(
<g/>
NPR	NPR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
12	#num#	k4
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
(	(	kIx(
<g/>
PR	pr	k0
<g/>
)	)	kIx)
a	a	k8xC
11	#num#	k4
přírodních	přírodní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
–	–	k?
14	#num#	k4
památných	památné	k1gNnPc2
stromů	strom	k1gInPc2
a	a	k8xC
6	#num#	k4
naučných	naučný	k2eAgFnPc2d1
stezek	stezka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
oblasti	oblast	k1gFnSc2
</s>
<s>
Reliéf	reliéf	k1gInSc1
krajiny	krajina	k1gFnSc2
</s>
<s>
CHKO	CHKO	kA
Železné	železný	k2eAgFnPc4d1
hory	hora	k1gFnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
</s>
<s>
CHKO	CHKO	kA
zaujímá	zaujímat	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc1d1
část	část	k1gFnSc1
Železných	železný	k2eAgInPc2d1
hor.	hor.	k?
Nejvyššími	vysoký	k2eAgInPc7d3
vrcholy	vrchol	k1gInPc7
jsou	být	k5eAaImIp3nP
Vestec	Vestec	k1gInSc4
s	s	k7c7
668	#num#	k4
metry	metr	k1gInPc4
nad	nad	k7c7
mořem	moře	k1gNnSc7
a	a	k8xC
Spálava	Spálava	k1gFnSc1
s	s	k7c7
663	#num#	k4
metry	metro	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnižší	nízký	k2eAgNnSc1d3
místa	místo	k1gNnPc1
jsou	být	k5eAaImIp3nP
u	u	k7c2
Podhořan	podhořan	k1gMnSc1
a	a	k8xC
Slatiňan	Slatiňany	k1gInPc2
se	se	k3xPyFc4
shodnou	shodný	k2eAgFnSc7d1
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
268	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nápadným	nápadný	k2eAgInSc7d1
útvarem	útvar	k1gInSc7
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
od	od	k7c2
Ždírce	Ždírec	k1gInSc2
nad	nad	k7c7
Doubravou	Doubrava	k1gFnSc7
do	do	k7c2
Podhořan	podhořan	k1gMnSc1
a	a	k8xC
dále	daleko	k6eAd2
k	k	k7c3
Týnci	Týnec	k1gInSc3
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
hřebenu	hřeben	k1gInSc6
jsou	být	k5eAaImIp3nP
vedle	vedle	k7c2
četných	četný	k2eAgInPc2d1
vrcholů	vrchol	k1gInPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Vestce	Vestec	k1gInSc2
a	a	k8xC
Spálavy	Spálava	k1gFnSc2
<g/>
)	)	kIx)
nápadné	nápadný	k2eAgNnSc1d1
i	i	k8xC
rokle	rokle	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInPc1d3
a	a	k8xC
nejkrásnější	krásný	k2eAgInPc1d3
jsou	být	k5eAaImIp3nP
Lovětínská	Lovětínský	k2eAgFnSc1d1
a	a	k8xC
Hedvíkovská	Hedvíkovský	k2eAgFnSc1d1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
prorážejí	prorážet	k5eAaImIp3nP
hlavní	hlavní	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
u	u	k7c2
Třemošnice	Třemošnice	k1gFnSc2
a	a	k8xC
Závratce	Závratec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
nejnápadnější	nápadní	k2eAgFnSc1d3
Bučina	bučina	k1gFnSc1
<g/>
,	,	kIx,
kopec	kopec	k1gInSc1
mezi	mezi	k7c7
Kraskovem	Kraskov	k1gInSc7
a	a	k8xC
Prachovicemi	Prachovice	k1gFnPc7
<g/>
,	,	kIx,
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
606	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Slatiňan	Slatiňany	k1gInPc2
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
391	#num#	k4
metrů	metr	k1gInPc2
je	být	k5eAaImIp3nS
kopec	kopec	k1gInSc1
Hůra	hůra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
jediná	jediný	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
vytvořená	vytvořený	k2eAgFnSc1d1
řekou	řeka	k1gFnSc7
Chrudimkou	Chrudimka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
kaňon	kaňon	k1gInSc1
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
v	v	k7c6
rámci	rámec	k1gInSc6
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
Krkanka	Krkanka	k1gFnSc1
a	a	k8xC
Strádovské	Strádovský	k2eAgNnSc1d1
peklo	peklo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Jižně	jižně	k6eAd1
od	od	k7c2
hlavního	hlavní	k2eAgInSc2d1
hřebene	hřeben	k1gInSc2
je	být	k5eAaImIp3nS
nižší	nízký	k2eAgFnSc1d2
část	část	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
Dlouhá	dlouhý	k2eAgFnSc1d1
mez	mez	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ní	on	k3xPp3gFnSc2
je	být	k5eAaImIp3nS
nápadný	nápadný	k2eAgInSc1d1
u	u	k7c2
Libice	Libice	k1gFnPc1
nad	nad	k7c7
Doubravou	Doubrava	k1gFnSc7
kopec	kopec	k1gInSc4
Hradiště	Hradiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
k	k	k7c3
jihu	jih	k1gInSc3
mezi	mezi	k7c7
Bílkem	bílek	k1gInSc7
a	a	k8xC
Chotěboří	Chotěboř	k1gFnSc7
je	být	k5eAaImIp3nS
malebné	malebný	k2eAgNnSc1d1
kaňonovité	kaňonovitý	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
řeky	řeka	k1gFnSc2
Doubravy	Doubrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Ždírcem	Ždírec	k1gInSc7
nad	nad	k7c7
Doubravou	Doubrava	k1gFnSc7
a	a	k8xC
Studencem	Studenec	k1gInSc7
leží	ležet	k5eAaImIp3nP
táhlý	táhlý	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
Cerhovy	Cerhova	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Flóra	Flóra	k1gFnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
registrováno	registrovat	k5eAaBmNgNnS
přes	přes	k7c4
1	#num#	k4
200	#num#	k4
druhů	druh	k1gInPc2
vyšších	vysoký	k2eAgFnPc2d2
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
asi	asi	k9
1	#num#	k4
000	#num#	k4
druhů	druh	k1gInPc2
domácích	domácí	k2eAgInPc2d1
tj.	tj.	kA
druhů	druh	k1gInPc2
přirozeně	přirozeně	k6eAd1
se	se	k3xPyFc4
vyskytujících	vyskytující	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
žije	žít	k5eAaImIp3nS
přes	přes	k7c4
75	#num#	k4
druhů	druh	k1gInPc2
měkkýšů	měkkýš	k1gMnPc2
<g/>
,	,	kIx,
významní	významný	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
motýli	motýl	k1gMnPc1
zvláště	zvláště	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Dlouhé	Dlouhé	k2eAgFnSc2d1
meze	mez	k1gFnSc2
(	(	kIx(
<g/>
celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
druhů	druh	k1gInPc2
motýlí	motýlí	k2eAgFnSc2d1
fauny	fauna	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
je	být	k5eAaImIp3nS
1262	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
obratlovců	obratlovec	k1gMnPc2
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenat	k5eAaPmNgNnS
230	#num#	k4
druhů	druh	k1gInPc2
(	(	kIx(
<g/>
24	#num#	k4
ryb	ryba	k1gFnPc2
<g/>
,	,	kIx,
12	#num#	k4
obojživelníků	obojživelník	k1gMnPc2
<g/>
,	,	kIx,
7	#num#	k4
plazů	plaz	k1gMnPc2
<g/>
,	,	kIx,
141	#num#	k4
ptáků	pták	k1gMnPc2
a	a	k8xC
46	#num#	k4
savců	savec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Lyžování	lyžování	k1gNnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
a	a	k8xC
v	v	k7c6
jejím	její	k3xOp3gNnSc6
těsném	těsný	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
6	#num#	k4
sjezdovek	sjezdovka	k1gFnPc2
s	s	k7c7
lyžařskými	lyžařský	k2eAgInPc7d1
vleky	vlek	k1gInPc7
<g/>
:	:	kIx,
Trhová	trhový	k2eAgFnSc1d1
Kamenice	Kamenice	k1gFnSc1
<g/>
,	,	kIx,
Hluboká	Hluboká	k1gFnSc1
<g/>
,	,	kIx,
Chotěboř	Chotěboř	k1gFnSc1
<g/>
,	,	kIx,
Vápenný	vápenný	k2eAgInSc1d1
Podol	Podol	k1gInSc1
<g/>
,	,	kIx,
Horní	horní	k2eAgNnSc1d1
Bradlo	bradlo	k1gNnSc1
a	a	k8xC
Nasavrky	Nasavrky	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
NPR	NPR	kA
Lichnice	Lichnice	k1gFnPc1
-	-	kIx~
Kaňkovy	Kaňkův	k2eAgFnPc1d1
hory	hora	k1gFnPc1
</s>
<s>
PR	pr	k0
</s>
<s>
Hubský	Hubský	k1gMnSc1
</s>
<s>
Krkanka	Krkanka	k1gFnSc1
</s>
<s>
Maršálka	maršálka	k1gFnSc1
</s>
<s>
Mokřadlo	mokřadlo	k1gNnSc1
</s>
<s>
Oheb	Oheb	k1gMnSc1
</s>
<s>
Polom	polom	k1gInSc1
</s>
<s>
Spálava	Spálava	k1gFnSc1
</s>
<s>
Strádovka	Strádovka	k1gFnSc1
</s>
<s>
Strádovské	Strádovský	k2eAgNnSc1d1
Peklo	peklo	k1gNnSc1
</s>
<s>
Svatomariánské	Svatomariánský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
</s>
<s>
Údolí	údolí	k1gNnSc1
Doubravy	Doubrava	k1gFnSc2
</s>
<s>
Vápenice	vápenice	k1gFnSc1
</s>
<s>
Vršovská	Vršovský	k2eAgFnSc1d1
olšina	olšina	k1gFnSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
louka	louka	k1gFnSc1
</s>
<s>
Zubří	zubří	k2eAgFnSc1d1
</s>
<s>
PP	PP	kA
</s>
<s>
Boušovka	Boušovka	k1gFnSc1
</s>
<s>
Buchtovka	Buchtovka	k1gFnSc1
</s>
<s>
Chuchelská	chuchelský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
</s>
<s>
Kaštanka	kaštanka	k1gFnSc1
</s>
<s>
Na	na	k7c6
Obůrce	obůrka	k1gFnSc6
</s>
<s>
Na	na	k7c6
skalách	skála	k1gFnPc6
</s>
<s>
Písník	písník	k1gInSc1
u	u	k7c2
Sokolovce	sokolovka	k1gFnSc6
</s>
<s>
Polánka	Polánka	k1gFnSc1
</s>
<s>
Upolíny	upolín	k1gInPc1
u	u	k7c2
Kamenice	Kamenice	k1gFnSc2
</s>
<s>
V	v	k7c6
Koutech	kout	k1gInPc6
</s>
<s>
Památné	památný	k2eAgInPc1d1
stromy	strom	k1gInPc1
</s>
<s>
Běstvina	Běstvina	k1gFnSc1
–	–	k?
duby	dub	k1gInPc1
letní	letní	k2eAgInPc1d1
(	(	kIx(
<g/>
mohutné	mohutný	k2eAgInPc1d1
stromy	strom	k1gInPc1
na	na	k7c6
okraji	okraj	k1gInSc6
obce	obec	k1gFnSc2
o	o	k7c6
stáří	stáří	k1gNnSc6
asi	asi	k9
500	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Běstvina	Běstvina	k1gFnSc1
–	–	k?
platan	platan	k1gInSc4
javorolistý	javorolistý	k2eAgInSc4d1
(	(	kIx(
<g/>
velkolepý	velkolepý	k2eAgMnSc1d1
jedinec	jedinec	k1gMnSc1
u	u	k7c2
zámku	zámek	k1gInSc2
o	o	k7c6
stáří	stáří	k1gNnSc6
asi	asi	k9
200	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Horní	horní	k2eAgInSc1d1
Vestec	Vestec	k1gInSc1
–	–	k?
kaštanovník	kaštanovník	k1gInSc1
setý	setý	k2eAgInSc1d1
(	(	kIx(
<g/>
mohutný	mohutný	k2eAgInSc1d1
strom	strom	k1gInSc1
o	o	k7c6
stáří	stáří	k1gNnSc6
asi	asi	k9
190	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Chotěboř	Chotěboř	k1gFnSc1
–	–	k?
duby	dub	k1gInPc1
letní	letní	k2eAgInPc1d1
(	(	kIx(
<g/>
roztroušeně	roztroušeně	k6eAd1
rostoucí	rostoucí	k2eAgInPc1d1
stromy	strom	k1gInPc1
u	u	k7c2
bývalých	bývalý	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
–	–	k?
stáří	stář	k1gFnPc2
asi	asi	k9
500	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Kameničky	Kameničky	k1gFnPc1
–	–	k?
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
strom	strom	k1gInSc1
na	na	k7c6
okraji	okraj	k1gInSc6
obce	obec	k1gFnSc2
o	o	k7c6
stáří	stáří	k1gNnSc6
460	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Klokočov	Klokočov	k1gInSc1
–	–	k?
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
strom	strom	k1gInSc1
s	s	k7c7
mohutným	mohutný	k2eAgInSc7d1
dutým	dutý	k2eAgInSc7d1
kmenem	kmen	k1gInSc7
a	a	k8xC
stářím	stáří	k1gNnSc7
asi	asi	k9
1000	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Lány	lán	k1gInPc1
–	–	k?
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
velký	velký	k2eAgInSc1d1
strom	strom	k1gInSc1
na	na	k7c6
okraji	okraj	k1gInSc6
obce	obec	k1gFnSc2
o	o	k7c6
stáří	stáří	k1gNnSc6
asi	asi	k9
500	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Lipka	lipka	k1gFnSc1
–	–	k?
lípa	lípa	k1gFnSc1
srdčitá	srdčitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
dvojkmenný	dvojkmenný	k2eAgMnSc1d1
jedinec	jedinec	k1gMnSc1
o	o	k7c6
stáří	stáří	k1gNnSc6
asi	asi	k9
600	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Podhradí	Podhradí	k1gNnSc4
–	–	k?
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Žižkův	Žižkův	k2eAgInSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
velký	velký	k2eAgInSc1d1
strom	strom	k1gInSc1
u	u	k7c2
bývalé	bývalý	k2eAgFnSc2d1
hradní	hradní	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
o	o	k7c6
stáří	stáří	k1gNnSc6
700	#num#	k4
až	až	k9
800	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Přemilov	Přemilov	k1gInSc1
–	–	k?
jilm	jilm	k1gInSc1
habrolistý	habrolistý	k2eAgInSc1d1
(	(	kIx(
<g/>
mohutný	mohutný	k2eAgMnSc1d1
jedinec	jedinec	k1gMnSc1
o	o	k7c6
stáří	stáří	k1gNnSc6
asi	asi	k9
230	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Slavice	slavice	k1gFnSc1
–	–	k?
smrk	smrk	k1gInSc4
ztepilý	ztepilý	k2eAgInSc4d1
(	(	kIx(
<g/>
vícekmenný	vícekmenný	k2eAgInSc4d1
strom	strom	k1gInSc4
<g/>
,	,	kIx,
poškozen	poškodit	k5eAaPmNgInS
větrem	vítr	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc2
asi	asi	k9
160	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Spálava	Spálava	k1gFnSc1
–	–	k?
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
velký	velký	k2eAgInSc1d1
strom	strom	k1gInSc1
na	na	k7c4
místní	místní	k2eAgFnPc4d1
návsi	náves	k1gFnPc4
o	o	k7c6
stáří	stáří	k1gNnSc6
asi	asi	k9
500	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Svidnice	Svidnice	k1gFnSc1
–	–	k?
jalovec	jalovec	k1gInSc4
obecný	obecný	k2eAgInSc4d1
(	(	kIx(
<g/>
mohutný	mohutný	k2eAgMnSc1d1
jedinec	jedinec	k1gMnSc1
o	o	k7c6
stáří	stáří	k1gNnSc6
asi	asi	k9
100	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Štikov	Štikov	k1gInSc1
–	–	k?
lípa	lípa	k1gFnSc1
velkolistá	velkolistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
velkolepý	velkolepý	k2eAgInSc4d1
strom	strom	k1gInSc4
na	na	k7c6
bývalé	bývalý	k2eAgFnSc6d1
návsi	náves	k1gFnSc6
o	o	k7c6
stáří	stáří	k1gNnSc6
asi	asi	k9
600	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
</s>
<s>
MAGMA	magma	k1gNnSc1
(	(	kIx(
<g/>
naučná	naučný	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
–	–	k?
MAlebný	malebný	k2eAgMnSc1d1
Geologický	geologický	k2eAgMnSc1d1
MAglajz	MAglajz	k?
<g/>
)	)	kIx)
–	–	k?
56	#num#	k4
km	km	kA
z	z	k7c2
Hlinska	Hlinsko	k1gNnSc2
do	do	k7c2
Chrudimi	Chrudim	k1gFnSc2
<g/>
,	,	kIx,
počet	počet	k1gInSc1
zastávek	zastávka	k1gFnPc2
<g/>
:	:	kIx,
8	#num#	k4
</s>
<s>
Ke	k	k7c3
Kočičímu	kočičí	k2eAgInSc3d1
hrádku	hrádek	k1gInSc3
–	–	k?
3	#num#	k4
km	km	kA
v	v	k7c6
Zámeckém	zámecký	k2eAgInSc6d1
parku	park	k1gInSc6
ve	v	k7c6
Slatiňanech	Slatiňany	k1gInPc6
<g/>
,	,	kIx,
počet	počet	k1gInSc1
zastávek	zastávka	k1gFnPc2
<g/>
:	:	kIx,
<g/>
7	#num#	k4
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
–	–	k?
3,2	3,2	k4
km	km	kA
kolem	kolem	k7c2
Třemošnice	Třemošnice	k1gFnSc2
<g/>
,	,	kIx,
počet	počet	k1gInSc1
zastávek	zastávka	k1gFnPc2
<g/>
:	:	kIx,
7	#num#	k4
</s>
<s>
Údolím	údolí	k1gNnSc7
Doubravy	Doubrava	k1gFnSc2
–	–	k?
4,5	4,5	k4
km	km	kA
z	z	k7c2
Chotěboře	Chotěboř	k1gFnSc2
do	do	k7c2
Bílku	bílek	k1gInSc2
<g/>
,	,	kIx,
počet	počet	k1gInSc1
zastávek	zastávka	k1gFnPc2
<g/>
:	:	kIx,
11	#num#	k4
</s>
<s>
Krajem	kraj	k1gInSc7
Chrudimky	Chrudimka	k1gFnSc2
(	(	kIx(
<g/>
vlastivědná	vlastivědný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
82	#num#	k4
km	km	kA
od	od	k7c2
pramene	pramen	k1gInSc2
Chrudimky	Chrudimka	k1gFnSc2
u	u	k7c2
Filipova	Filipův	k2eAgMnSc2d1
do	do	k7c2
Chrudimi	Chrudim	k1gFnSc2
<g/>
,	,	kIx,
počet	počet	k1gInSc1
zastávek	zastávka	k1gFnPc2
<g/>
:	:	kIx,
37	#num#	k4
</s>
<s>
Krajem	kraj	k1gInSc7
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
(	(	kIx(
<g/>
vlastivědná	vlastivědný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
23,5	23,5	k4
km	km	kA
ze	z	k7c2
Seče	seč	k1gFnSc2
do	do	k7c2
Ronova	Ronův	k2eAgNnSc2d1
nad	nad	k7c7
Doubravou	Doubrava	k1gFnSc7
<g/>
,	,	kIx,
počet	počet	k1gInSc1
zastávek	zastávka	k1gFnPc2
<g/>
:	:	kIx,
15	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ŠUMPICH	ŠUMPICH	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
:	:	kIx,
Motýli	motýl	k1gMnPc1
Železných	železný	k2eAgInPc2d1
hor.	hor.	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
prací	prací	k2eAgInSc1d1
č.	č.	k?
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
přátel	přítel	k1gMnPc2
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
Nasavrky	Nasavrka	k1gFnSc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
:	:	kIx,
265	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-902400-2-X	80-902400-2-X	k4
:	:	kIx,
s.	s.	k?
243	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Železné	železný	k2eAgFnPc4d1
hory	hora	k1gFnPc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Zřizovací	zřizovací	k2eAgFnSc1d1
vyhláška	vyhláška	k1gFnSc1
</s>
<s>
Společnost	společnost	k1gFnSc1
přátel	přítel	k1gMnPc2
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
Organizace	organizace	k1gFnSc2
podporující	podporující	k2eAgFnSc4d1
popularizaci	popularizace	k1gFnSc4
a	a	k8xC
turistický	turistický	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
regionu	region	k1gInSc2
</s>
<s>
Geopark	Geopark	k1gInSc1
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Havlíčkův	Havlíčkův	k2eAgInSc1d1
Brod	Brod	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Ransko	Ransko	k6eAd1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Havranka	Havranka	k1gFnSc1
•	•	k?
Hroznětínská	Hroznětínský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
a	a	k8xC
olšina	olšina	k1gFnSc1
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
trouba	trouba	k1gFnSc1
•	•	k?
Maršálka	maršálka	k1gFnSc1
•	•	k?
Mokřadlo	mokřadlo	k1gNnSc1
•	•	k?
Niva	niva	k1gFnSc1
Doubravy	Doubrava	k1gFnSc2
•	•	k?
Ranská	Ranská	k1gFnSc1
jezírka	jezírko	k1gNnSc2
•	•	k?
Řeka	řeka	k1gFnSc1
•	•	k?
Spálava	Spálava	k1gFnSc1
•	•	k?
Stvořidla	Stvořidlo	k1gNnSc2
•	•	k?
Svatomariánské	Svatomariánský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
•	•	k?
Štíří	štířit	k5eAaImIp3nS
důl	důl	k1gInSc4
•	•	k?
Údolí	údolí	k1gNnSc2
Doubravy	Doubrava	k1gFnSc2
•	•	k?
Velká	velký	k2eAgFnSc1d1
a	a	k8xC
Malá	malý	k2eAgFnSc1d1
olšina	olšina	k1gFnSc1
•	•	k?
Zlatá	zlatý	k2eAgFnSc1d1
louka	louka	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Borecká	borecký	k2eAgFnSc1d1
skalka	skalka	k1gFnSc1
•	•	k?
Čertův	čertův	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Chuchelská	chuchelský	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Kamenický	kamenický	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Písník	písník	k1gInSc1
u	u	k7c2
Sokolovce	sokolovka	k1gFnSc3
•	•	k?
Pod	pod	k7c7
Kazbalem	Kazbal	k1gInSc7
•	•	k?
Rybníček	rybníček	k1gInSc1
u	u	k7c2
Dolního	dolní	k2eAgInSc2d1
Dvora	Dvůr	k1gInSc2
•	•	k?
Sochorov	Sochorov	k1gInSc1
•	•	k?
Šlapanka	Šlapanka	k1gFnSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
