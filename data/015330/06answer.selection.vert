<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
byla	být	k5eAaImAgFnS
ustanovena	ustanovit	k5eAaPmNgFnS
vyhláškou	vyhláška	k1gFnSc7
Ministerstva	ministerstvo	k1gNnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
ČR	ČR	kA
č.	č.	k?
<g/>
156	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
(	(	kIx(
<g/>
účinnost	účinnost	k1gFnSc1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1991	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
rozloze	rozloha	k1gFnSc6
284	#num#	k4
km²	km²	k?
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
zhruba	zhruba	k6eAd1
mezi	mezi	k7c7
<g />
.	.	kIx.
</s>