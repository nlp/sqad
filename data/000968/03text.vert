<s>
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Ungarisch	Ungarisch	k1gMnSc1	Ungarisch
Hradisch	Hradisch	k1gMnSc1	Hradisch
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Magyarhradis	Magyarhradis	k1gFnSc1	Magyarhradis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
23	[number]	k4	23
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Zlína	Zlín	k1gInSc2	Zlín
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Starým	Starý	k1gMnSc7	Starý
Městem	město	k1gNnSc7	město
a	a	k8xC	a
Kunovicemi	Kunovice	k1gFnPc7	Kunovice
tvoří	tvořit	k5eAaImIp3nS	tvořit
městskou	městský	k2eAgFnSc4d1	městská
aglomeraci	aglomerace	k1gFnSc4	aglomerace
s	s	k7c7	s
38	[number]	k4	38
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1257	[number]	k4	1257
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
je	být	k5eAaImIp3nS	být
odedávna	odedávna	k6eAd1	odedávna
přirozeným	přirozený	k2eAgInSc7d1	přirozený
středem	střed	k1gInSc7	střed
Slovácka	Slovácko	k1gNnSc2	Slovácko
-	-	kIx~	-
regionu	region	k1gInSc2	region
proslulého	proslulý	k2eAgInSc2d1	proslulý
svébytným	svébytný	k2eAgInSc7d1	svébytný
folklórem	folklór	k1gInSc7	folklór
<g/>
,	,	kIx,	,
cimbálovou	cimbálový	k2eAgFnSc7d1	cimbálová
muzikou	muzika	k1gFnSc7	muzika
<g/>
,	,	kIx,	,
kvalitním	kvalitní	k2eAgNnSc7d1	kvalitní
vínem	víno	k1gNnSc7	víno
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
zdobenými	zdobený	k2eAgInPc7d1	zdobený
kroji	kroj	k1gInPc7	kroj
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
zachovaných	zachovaný	k2eAgFnPc2d1	zachovaná
lidových	lidový	k2eAgFnPc2d1	lidová
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Uherskohradišťské	uherskohradišťský	k2eAgNnSc1d1	uherskohradišťské
gymnázium	gymnázium	k1gNnSc1	gymnázium
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
jako	jako	k8xC	jako
nejstarší	starý	k2eAgFnSc1d3	nejstarší
česká	český	k2eAgFnSc1d1	Česká
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
morfologie	morfologie	k1gFnSc2	morfologie
město	město	k1gNnSc1	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
územní	územní	k2eAgFnSc4d1	územní
rovinu	rovina	k1gFnSc4	rovina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyrovnávaly	vyrovnávat	k5eAaImAgInP	vyrovnávat
náplavy	náplav	k1gInPc4	náplav
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
178	[number]	k4	178
<g/>
-	-	kIx~	-
<g/>
180	[number]	k4	180
m.	m.	k?	m.
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
terén	terén	k1gInSc1	terén
úměrně	úměrně	k6eAd1	úměrně
zvedá	zvedat	k5eAaImIp3nS	zvedat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
205	[number]	k4	205
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
východně	východně	k6eAd1	východně
od	od	k7c2	od
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Mařatice	Mařatice	k1gFnSc2	Mařatice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
údolí	údolí	k1gNnSc4	údolí
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
kótou	kóta	k1gFnSc7	kóta
230	[number]	k4	230
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Plocha	plocha	k1gFnSc1	plocha
uherskohradišťského	uherskohradišťský	k2eAgInSc2d1	uherskohradišťský
katastru	katastr	k1gInSc2	katastr
činí	činit	k5eAaImIp3nS	činit
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
126	[number]	k4	126
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
jsou	být	k5eAaImIp3nP	být
souhrnné	souhrnný	k2eAgFnPc1d1	souhrnná
za	za	k7c4	za
všechny	všechen	k3xTgFnPc4	všechen
městské	městský	k2eAgFnPc4d1	městská
čtvrtě	čtvrt	k1gFnPc4	čtvrt
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
Jarošov	Jarošov	k1gInSc1	Jarošov
<g/>
,	,	kIx,	,
Mařatice	Mařatice	k1gFnSc1	Mařatice
<g/>
,	,	kIx,	,
Míkovice	Míkovice	k1gFnSc1	Míkovice
<g/>
,	,	kIx,	,
Rybárny	rybárna	k1gFnPc1	rybárna
<g/>
,	,	kIx,	,
Sady	sad	k1gInPc1	sad
a	a	k8xC	a
Vésky	Vésk	k1gInPc1	Vésk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
navíc	navíc	k6eAd1	navíc
také	také	k9	také
dnešní	dnešní	k2eAgNnPc1d1	dnešní
města	město	k1gNnPc1	město
Kunovice	Kunovice	k1gFnPc1	Kunovice
a	a	k8xC	a
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
osamostatnění	osamostatnění	k1gNnSc1	osamostatnění
bylo	být	k5eAaImAgNnS	být
příčinou	příčina	k1gFnSc7	příčina
desetitisícového	desetitisícový	k2eAgInSc2d1	desetitisícový
propadu	propad	k1gInSc2	propad
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
začátkem	začátkem	k7c2	začátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vymezená	vymezený	k2eAgFnSc1d1	vymezená
aglomerace	aglomerace	k1gFnSc1	aglomerace
čítala	čítat	k5eAaImAgFnS	čítat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
celkem	celkem	k6eAd1	celkem
38	[number]	k4	38
007	[number]	k4	007
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Veligrad	Veligrad	k1gInSc1	Veligrad
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
už	už	k9	už
ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
sídelním	sídelní	k2eAgFnPc3d1	sídelní
oblastem	oblast	k1gFnPc3	oblast
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
místy	místy	k6eAd1	místy
vedla	vést	k5eAaImAgFnS	vést
ve	v	k7c6	v
vertikále	vertikála	k1gFnSc6	vertikála
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
moravská	moravský	k2eAgFnSc1d1	Moravská
část	část	k1gFnSc1	část
jantarové	jantarový	k2eAgFnSc2d1	jantarová
cesty	cesta	k1gFnSc2	cesta
Krakov	Krakov	k1gInSc1	Krakov
<g/>
-	-	kIx~	-
<g/>
Morava	Morava	k1gFnSc1	Morava
<g/>
-	-	kIx~	-
<g/>
Vídeň	Vídeň	k1gFnSc1	Vídeň
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
znovuobjevena	znovuobjeven	k2eAgFnSc1d1	znovuobjeven
jako	jako	k8xC	jako
lákadlo	lákadlo	k1gNnSc1	lákadlo
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
<g/>
)	)	kIx)	)
a	a	k8xC	a
uherská	uherský	k2eAgFnSc1d1	uherská
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
horizontále	horizontála	k1gFnSc6	horizontála
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
křižovatce	křižovatka	k1gFnSc6	křižovatka
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
pevnostní	pevnostní	k2eAgInSc1d1	pevnostní
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
neobydlené	obydlený	k2eNgInPc1d1	neobydlený
ostrovy	ostrov	k1gInPc1	ostrov
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
osídleny	osídlen	k2eAgInPc4d1	osídlen
Slovany	Slovan	k1gInPc4	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gMnSc7	centr
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
Svatojiřský	svatojiřský	k2eAgInSc1d1	svatojiřský
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
kaple	kaple	k1gFnSc2	kaple
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
patronem	patron	k1gInSc7	patron
byl	být	k5eAaImAgMnS	být
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Velkomoravská	velkomoravský	k2eAgFnSc1d1	Velkomoravská
aglomerace	aglomerace	k1gFnSc1	aglomerace
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
a	a	k8xC	a
Sadů	sad	k1gInPc2	sad
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
důležitým	důležitý	k2eAgNnPc3d1	důležité
střediskům	středisko	k1gNnPc3	středisko
Velké	velká	k1gFnSc2	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1	archeologický
průzkum	průzkum	k1gInSc1	průzkum
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
lokality	lokalita	k1gFnSc2	lokalita
Mikulčice-Valy	Mikulčice-Vala	k1gFnSc2	Mikulčice-Vala
však	však	k9	však
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
současná	současný	k2eAgFnSc1d1	současná
zástavba	zástavba	k1gFnSc1	zástavba
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
Kyjově	Kyjov	k1gInSc6	Kyjov
či	či	k8xC	či
Hodoníně	Hodonín	k1gInSc6	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
současného	současný	k2eAgNnSc2d1	současné
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc2	jeho
části	část	k1gFnSc2	část
Sady	sada	k1gFnSc2	sada
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
aglomerace	aglomerace	k1gFnPc4	aglomerace
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
centrum	centrum	k1gNnSc1	centrum
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
byla	být	k5eAaImAgFnS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
mocenská	mocenský	k2eAgFnSc1d1	mocenská
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc1d1	náboženský
objekty	objekt	k1gInPc1	objekt
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
výhodám	výhoda	k1gFnPc3	výhoda
říčního	říční	k2eAgInSc2d1	říční
terénu	terén	k1gInSc2	terén
(	(	kIx(	(
<g/>
umístění	umístění	k1gNnSc4	umístění
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
)	)	kIx)	)
nebyly	být	k5eNaImAgInP	být
donedávna	donedávna	k6eAd1	donedávna
nalezeny	nalezen	k2eAgInPc1d1	nalezen
stopy	stop	k1gInPc1	stop
žádného	žádný	k1gMnSc2	žádný
specifického	specifický	k2eAgNnSc2d1	specifické
mohutného	mohutný	k2eAgNnSc2d1	mohutné
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
potvrzovalo	potvrzovat	k5eAaImAgNnS	potvrzovat
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
centrum	centrum	k1gNnSc4	centrum
velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
Veligrad	Veligrada	k1gFnPc2	Veligrada
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
nastala	nastat	k5eAaPmAgFnS	nastat
až	až	k9	až
objevením	objevení	k1gNnSc7	objevení
zatím	zatím	k6eAd1	zatím
nejmohutnějšího	mohutný	k2eAgNnSc2d3	nejmohutnější
velkomoravského	velkomoravský	k2eAgNnSc2d1	velkomoravské
opevnění	opevnění	k1gNnSc2	opevnění
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Rybárny	rybárna	k1gFnSc2	rybárna
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
brodu	brod	k1gInSc3	brod
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
vedla	vést	k5eAaImAgFnS	vést
přes	přes	k7c4	přes
ostrov	ostrov	k1gInSc4	ostrov
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgMnS	založit
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
královské	královský	k2eAgNnSc4d1	královské
město	město	k1gNnSc4	město
Nový	nový	k2eAgInSc1d1	nový
Veligrad	Veligrad	k1gInSc1	Veligrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
stávala	stávat	k5eAaImAgFnS	stávat
podle	podle	k7c2	podle
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
zpráv	zpráva	k1gFnPc2	zpráva
kamenná	kamenný	k2eAgFnSc1d1	kamenná
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
trosky	troska	k1gFnPc1	troska
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
pod	pod	k7c7	pod
základovým	základový	k2eAgNnSc7d1	základové
zdivem	zdivo	k1gNnSc7	zdivo
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zrušeného	zrušený	k2eAgInSc2d1	zrušený
a	a	k8xC	a
zbořeného	zbořený	k2eAgInSc2d1	zbořený
gotického	gotický	k2eAgInSc2d1	gotický
svatojiřského	svatojiřský	k2eAgInSc2d1	svatojiřský
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nahradil	nahradit	k5eAaPmAgInS	nahradit
kapli	kaple	k1gFnSc4	kaple
někdy	někdy	k6eAd1	někdy
počátkem	počátkem	k7c2	počátkem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stával	stávat	k5eAaImAgMnS	stávat
v	v	k7c6	v
úhlopříčce	úhlopříčka	k1gFnSc6	úhlopříčka
hlavního	hlavní	k2eAgNnSc2d1	hlavní
náměstí	náměstí	k1gNnSc2	náměstí
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
zástavba	zástavba	k1gFnSc1	zástavba
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
posoudit	posoudit	k5eAaPmF	posoudit
lidnatost	lidnatost	k1gFnSc4	lidnatost
velkomoravského	velkomoravský	k2eAgNnSc2d1	velkomoravské
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
nálezy	nález	k1gInPc1	nález
však	však	k9	však
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
osídlení	osídlení	k1gNnSc4	osídlení
již	již	k6eAd1	již
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dobu	doba	k1gFnSc4	doba
velkomoravskou	velkomoravský	k2eAgFnSc4d1	Velkomoravská
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaPmF	vysledovat
souvislý	souvislý	k2eAgInSc4d1	souvislý
pás	pás	k1gInSc4	pás
osídlení	osídlení	k1gNnSc2	osídlení
od	od	k7c2	od
brodu	brod	k1gInSc2	brod
u	u	k7c2	u
Bumbalova	bumbalův	k2eAgInSc2d1	bumbalův
až	až	k9	až
po	po	k7c4	po
svahy	svah	k1gInPc4	svah
mařatického	mařatický	k2eAgInSc2d1	mařatický
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
sídelních	sídelní	k2eAgInPc2d1	sídelní
objektů	objekt	k1gInPc2	objekt
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
velkomoravské	velkomoravský	k2eAgInPc1d1	velkomoravský
hroby	hrob	k1gInPc1	hrob
<g/>
,	,	kIx,	,
třmeny	třmen	k1gInPc1	třmen
<g/>
,	,	kIx,	,
bojové	bojový	k2eAgFnPc1d1	bojová
sekery	sekera	k1gFnPc1	sekera
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
u	u	k7c2	u
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
výstroj	výstroj	k1gFnSc1	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
opevnění	opevnění	k1gNnPc4	opevnění
toho	ten	k3xDgNnSc2	ten
mnoho	mnoho	k6eAd1	mnoho
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
středověkou	středověký	k2eAgFnSc7d1	středověká
hradební	hradební	k2eAgFnSc7d1	hradební
zdí	zeď	k1gFnSc7	zeď
se	se	k3xPyFc4	se
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zbytky	zbytek	k1gInPc1	zbytek
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
hliněného	hliněný	k2eAgInSc2d1	hliněný
valu	val	k1gInSc2	val
neznámého	známý	k2eNgInSc2d1	neznámý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
jiný	jiný	k2eAgInSc1d1	jiný
průběh	průběh	k1gInSc1	průběh
než	než	k8xS	než
středověké	středověký	k2eAgFnPc1d1	středověká
hradby	hradba	k1gFnPc1	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Pozemky	pozemek	k1gInPc1	pozemek
okolo	okolo	k7c2	okolo
valu	val	k1gInSc2	val
nesou	nést	k5eAaImIp3nP	nést
název	název	k1gInSc4	název
Stará	Stará	k1gFnSc1	Stará
Tenice	Tenice	k1gFnSc1	Tenice
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
místo	místo	k1gNnSc4	místo
starého	starý	k2eAgNnSc2d1	staré
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
konce	konec	k1gInSc2	konec
valu	val	k1gInSc2	val
za	za	k7c7	za
Bumbalovem	Bumbalov	k1gInSc7	Bumbalov
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
i	i	k9	i
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
břehu	břeh	k1gInSc6	břeh
regulované	regulovaný	k2eAgFnSc2d1	regulovaná
Moravy	Morava	k1gFnSc2	Morava
jakási	jakýsi	k3yIgFnSc1	jakýsi
vyvýšená	vyvýšený	k2eAgFnSc1d1	vyvýšená
linie	linie	k1gFnSc1	linie
podél	podél	k7c2	podél
starého	starý	k2eAgInSc2d1	starý
meandru	meandr	k1gInSc2	meandr
Moravy	Morava	k1gFnSc2	Morava
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
mařatickému	mařatický	k2eAgInSc3d1	mařatický
kopci	kopec	k1gInSc3	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
zástavbou	zástavba	k1gFnSc7	zástavba
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
a	a	k8xC	a
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
kopce	kopec	k1gInSc2	kopec
objevuje	objevovat	k5eAaImIp3nS	objevovat
možné	možný	k2eAgNnSc1d1	možné
její	její	k3xOp3gNnSc4	její
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
archeologického	archeologický	k2eAgInSc2d1	archeologický
ověření	ověření	k1gNnSc3	ověření
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
nelze	lze	k6eNd1	lze
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Pokračování	pokračování	k1gNnSc1	pokračování
bubalovského	bubalovský	k2eAgNnSc2d1	bubalovský
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Christinova	Christinův	k2eAgInSc2d1	Christinův
valu	val	k1gInSc2	val
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
též	též	k9	též
zlechovského	zlechovský	k2eAgInSc2d1	zlechovský
valu	val	k1gInSc2	val
<g/>
)	)	kIx)	)
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
skvělou	skvělý	k2eAgFnSc4d1	skvělá
obrannou	obranný	k2eAgFnSc4d1	obranná
linii	linie	k1gFnSc4	linie
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
velkomoravskou	velkomoravský	k2eAgFnSc4d1	Velkomoravská
aglomeraci	aglomerace	k1gFnSc4	aglomerace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
uzavřít	uzavřít	k5eAaPmF	uzavřít
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
od	od	k7c2	od
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Obdobný	obdobný	k2eAgInSc4d1	obdobný
charakter	charakter	k1gInSc4	charakter
opevnění	opevnění	k1gNnSc2	opevnění
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
toku	tok	k1gInSc2	tok
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
lokalit	lokalita	k1gFnPc2	lokalita
(	(	kIx(	(
<g/>
Novgorod	Novgorod	k1gInSc4	Novgorod
<g/>
)	)	kIx)	)
a	a	k8xC	a
předsunuté	předsunutý	k2eAgInPc4d1	předsunutý
valy	val	k1gInPc4	val
např.	např.	kA	např.
ze	z	k7c2	z
Starých	Starých	k2eAgInPc2d1	Starých
zámků	zámek	k1gInPc2	zámek
u	u	k7c2	u
Líšně	Líšeň	k1gFnSc2	Líšeň
nebo	nebo	k8xC	nebo
i	i	k9	i
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgNnSc1d1	centrální
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
evidenční	evidenční	k2eAgFnSc1d1	evidenční
část	část	k1gFnSc1	část
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
naproti	naproti	k7c3	naproti
Starému	starý	k2eAgNnSc3d1	staré
Městu	město	k1gNnSc3	město
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
též	též	k6eAd1	též
ZSJ	ZSJ	kA	ZSJ
Na	na	k7c6	na
Rybníku	rybník	k1gInSc6	rybník
z	z	k7c2	z
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Mařatice	Mařatice	k1gFnSc2	Mařatice
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
přesahující	přesahující	k2eAgNnSc1d1	přesahující
východně	východně	k6eAd1	východně
od	od	k7c2	od
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
na	na	k7c4	na
sever	sever	k1gInSc4	sever
na	na	k7c4	na
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
tvoří	tvořit	k5eAaImIp3nS	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
evidenční	evidenční	k2eAgFnSc4d1	evidenční
část	část	k1gFnSc4	část
Rybárny	rybárna	k1gFnSc2	rybárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
východní	východní	k2eAgFnSc1d1	východní
Morava	Morava	k1gFnSc1	Morava
-	-	kIx~	-
tehdy	tehdy	k6eAd1	tehdy
stále	stále	k6eAd1	stále
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
vlivu	vliv	k1gInSc2	vliv
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
a	a	k8xC	a
Uher	Uhry	k1gFnPc2	Uhry
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
Lucká	lucký	k2eAgFnSc1d1	Lucká
provincie	provincie	k1gFnSc1	provincie
-	-	kIx~	-
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
pod	pod	k7c7	pod
nepřátelskými	přátelský	k2eNgInPc7d1	nepřátelský
nájezdy	nájezd	k1gInPc7	nájezd
<g/>
,	,	kIx,	,
kterými	který	k3yIgMnPc7	který
trpělo	trpět	k5eAaImAgNnS	trpět
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
i	i	k8xC	i
nedaleký	daleký	k2eNgInSc1d1	nedaleký
klášter	klášter	k1gInSc1	klášter
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
opat	opat	k1gMnSc1	opat
Hartlib	Hartlib	k1gMnSc1	Hartlib
z	z	k7c2	z
velehradského	velehradský	k2eAgInSc2d1	velehradský
kláštera	klášter	k1gInSc2	klášter
(	(	kIx(	(
<g/>
založeného	založený	k2eAgInSc2d1	založený
ve	v	k7c4	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
cisterciáky	cisterciák	k1gMnPc7	cisterciák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
obrátil	obrátit	k5eAaPmAgMnS	obrátit
na	na	k7c4	na
panovníka	panovník	k1gMnSc4	panovník
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Prostředníkem	prostředník	k1gInSc7	prostředník
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Bruno	Bruno	k1gMnSc1	Bruno
ze	z	k7c2	z
Schauenburku	Schauenburk	k1gInSc2	Schauenburk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
královskými	královský	k2eAgMnPc7d1	královský
rádci	rádce	k1gMnPc7	rádce
zvolil	zvolit	k5eAaPmAgMnS	zvolit
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
pevnosti	pevnost	k1gFnSc2	pevnost
ostrov	ostrov	k1gInSc1	ostrov
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Moravě	Morava	k1gFnSc6	Morava
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgMnSc6	který
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
několik	několik	k4yIc1	několik
rybářských	rybářský	k2eAgFnPc2d1	rybářská
chatrčí	chatrč	k1gFnPc2	chatrč
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
poté	poté	k6eAd1	poté
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1257	[number]	k4	1257
dosvědčil	dosvědčit	k5eAaPmAgInS	dosvědčit
listinou	listina	k1gFnSc7	listina
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
velehradského	velehradský	k2eAgInSc2d1	velehradský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
listiny	listina	k1gFnSc2	listina
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
ochraňovat	ochraňovat	k5eAaImF	ochraňovat
konvent	konvent	k1gInSc4	konvent
i	i	k8xC	i
zemskou	zemský	k2eAgFnSc4d1	zemská
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
namísto	namísto	k7c2	namísto
původního	původní	k2eAgInSc2d1	původní
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc4	město
začal	začít	k5eAaPmAgInS	začít
hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1258	[number]	k4	1258
používat	používat	k5eAaImF	používat
název	název	k1gInSc4	název
Nový	nový	k2eAgInSc4d1	nový
Velehrad	Velehrad	k1gInSc4	Velehrad
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sousedního	sousední	k2eAgInSc2d1	sousední
Veligradu	Veligrad	k1gInSc2	Veligrad
(	(	kIx(	(
<g/>
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
a	a	k8xC	a
blízkých	blízký	k2eAgFnPc2d1	blízká
Kunovic	Kunovice	k1gFnPc2	Kunovice
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
také	také	k9	také
přenesena	přenesen	k2eAgNnPc4d1	přeneseno
tržní	tržní	k2eAgNnPc4d1	tržní
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
od	od	k7c2	od
pojmenování	pojmenování	k1gNnSc2	pojmenování
Nový	nový	k2eAgInSc4d1	nový
Velehrad	Velehrad	k1gInSc4	Velehrad
upouštěno	upouštěn	k2eAgNnSc1d1	upouštěno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1294	[number]	k4	1294
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
již	již	k6eAd1	již
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Hradištěm	Hradiště	k1gNnSc7	Hradiště
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgInS	ustálit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
Velehrad	Velehrad	k1gInSc1	Velehrad
vžilo	vžít	k5eAaPmAgNnS	vžít
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
obec	obec	k1gFnSc4	obec
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
okolo	okolo	k7c2	okolo
velehradského	velehradský	k2eAgInSc2d1	velehradský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
rozšiřovaný	rozšiřovaný	k2eAgMnSc1d1	rozšiřovaný
na	na	k7c4	na
"	"	kIx"	"
<g/>
Hradiště	Hradiště	k1gNnSc4	Hradiště
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
často	často	k6eAd1	často
v	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
pramenech	pramen	k1gInPc6	pramen
pozměněn	pozměněn	k2eAgMnSc1d1	pozměněn
<g/>
,	,	kIx,	,
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
uvádí	uvádět	k5eAaImIp3nS	uvádět
např.	např.	kA	např.
jako	jako	k8xC	jako
Hradisst	Hradisst	k1gFnSc1	Hradisst
či	či	k8xC	či
Hradisscze	Hradisscze	k1gFnSc1	Hradisscze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
názvu	název	k1gInSc2	název
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
latinské	latinský	k2eAgNnSc1d1	latinské
či	či	k8xC	či
německé	německý	k2eAgFnSc2d1	německá
Radisch	Radisch	k1gInSc4	Radisch
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1587	[number]	k4	1587
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
stejným	stejný	k2eAgInSc7d1	stejný
přívlastkem	přívlastek	k1gInSc7	přívlastek
v	v	k7c6	v
názvu	název	k1gInSc6	název
Uherského	uherský	k2eAgInSc2d1	uherský
Brodu	Brod	k1gInSc2	Brod
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
"	"	kIx"	"
<g/>
Brod	Brod	k1gInSc1	Brod
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
uherských	uherský	k2eAgMnPc2d1	uherský
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
přívlastek	přívlastek	k1gInSc1	přívlastek
začal	začít	k5eAaPmAgInS	začít
objevovat	objevovat	k5eAaImF	objevovat
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
přejmenování	přejmenování	k1gNnSc4	přejmenování
na	na	k7c4	na
"	"	kIx"	"
<g/>
Slovácké	slovácký	k2eAgNnSc4d1	Slovácké
Hradiště	Hradiště	k1gNnSc4	Hradiště
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přívlastek	přívlastek	k1gInSc4	přívlastek
"	"	kIx"	"
<g/>
uherské	uherský	k2eAgNnSc4d1	Uherské
<g/>
"	"	kIx"	"
zněl	znět	k5eAaImAgMnS	znět
pro	pro	k7c4	pro
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dobu	doba	k1gFnSc4	doba
již	již	k9	již
poněkud	poněkud	k6eAd1	poněkud
zastarale	zastarale	k6eAd1	zastarale
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
nového	nový	k2eAgInSc2d1	nový
názvu	název	k1gInSc2	název
však	však	k9	však
byl	být	k5eAaImAgInS	být
veřejností	veřejnost	k1gFnSc7	veřejnost
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
700	[number]	k4	700
let	léto	k1gNnPc2	léto
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
zamítnut	zamítnout	k5eAaPmNgMnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
Zlína	Zlín	k1gInSc2	Zlín
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
nesl	nést	k5eAaImAgMnS	nést
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Gottwaldov	Gottwaldov	k1gInSc1	Gottwaldov
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Vnorovy	Vnorův	k2eAgFnSc2d1	Vnorův
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
používal	používat	k5eAaImAgInS	používat
i	i	k9	i
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Znorovy	Znorov	k1gInPc1	Znorov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uzákoněn	uzákoněn	k2eAgInSc1d1	uzákoněn
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
termín	termín	k1gInSc1	termín
písemně	písemně	k6eAd1	písemně
starší	starý	k2eAgMnSc1d2	starší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Splávek	splávek	k1gInSc1	splávek
<g/>
:	:	kIx,	:
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
dodnes	dodnes	k6eAd1	dodnes
dvě	dva	k4xCgNnPc1	dva
náměstí	náměstí	k1gNnPc1	náměstí
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
oddělených	oddělený	k2eAgInPc6d1	oddělený
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Dělil	dělit	k5eAaImAgMnS	dělit
je	být	k5eAaImIp3nS	být
úzký	úzký	k2eAgInSc4d1	úzký
potok	potok	k1gInSc4	potok
či	či	k8xC	či
strouha	strouha	k1gFnSc1	strouha
-	-	kIx~	-
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
široký	široký	k2eAgInSc1d1	široký
kanál	kanál	k1gInSc1	kanál
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
"	"	kIx"	"
<g/>
Splávek	splávek	k1gInSc1	splávek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tekl	téct	k5eAaImAgMnS	téct
dnešní	dnešní	k2eAgFnSc7d1	dnešní
ulicí	ulice	k1gFnSc7	ulice
Na	na	k7c6	na
Splávku	splávek	k1gInSc6	splávek
přes	přes	k7c4	přes
Zelný	zelný	k2eAgInSc4d1	zelný
trh	trh	k1gInSc4	trh
<g/>
,	,	kIx,	,
Prostřední	prostřednět	k5eAaImIp3nS	prostřednět
ulicí	ulice	k1gFnSc7	ulice
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
dvorem	dvůr	k1gInSc7	dvůr
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
zahradou	zahrada	k1gFnSc7	zahrada
tehdy	tehdy	k6eAd1	tehdy
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zasvěceného	zasvěcený	k2eAgInSc2d1	zasvěcený
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
(	(	kIx(	(
<g/>
současný	současný	k2eAgInSc4d1	současný
kostel	kostel	k1gInSc4	kostel
Zvěstování	zvěstování	k1gNnSc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc4d1	původní
byl	být	k5eAaImAgMnS	být
poničen	poničen	k2eAgMnSc1d1	poničen
na	na	k7c6	na
konci	konec	k1gInSc6	konec
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
opouštěl	opouštět	k5eAaImAgMnS	opouštět
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Znázornění	znázornění	k1gNnSc1	znázornění
Splávku	splávek	k1gInSc2	splávek
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
viditelné	viditelný	k2eAgNnSc1d1	viditelné
na	na	k7c6	na
starých	starý	k2eAgFnPc6d1	stará
rytinách	rytina	k1gFnPc6	rytina
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
náměstí	náměstí	k1gNnPc4	náměstí
<g/>
:	:	kIx,	:
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
založení	založení	k1gNnSc6	založení
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
osídleno	osídlen	k2eAgNnSc1d1	osídleno
obyvateli	obyvatel	k1gMnPc7	obyvatel
dvou	dva	k4xCgFnPc6	dva
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
trhových	trhový	k2eAgFnPc2d1	trhová
obcí	obec	k1gFnPc2	obec
-	-	kIx~	-
Kunovic	Kunovice	k1gFnPc2	Kunovice
a	a	k8xC	a
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
si	se	k3xPyFc3	se
postavily	postavit	k5eAaPmAgFnP	postavit
své	své	k1gNnSc4	své
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
:	:	kIx,	:
Kunovjané	Kunovjan	k1gMnPc1	Kunovjan
měli	mít	k5eAaImAgMnP	mít
náměstí	náměstí	k1gNnSc4	náměstí
zvané	zvaný	k2eAgNnSc4d1	zvané
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
náměstí	náměstí	k1gNnSc3	náměstí
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Gottwaldovo	Gottwaldův	k2eAgNnSc1d1	Gottwaldovo
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
;	;	kIx,	;
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
základy	základ	k1gInPc1	základ
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
skryty	skryt	k1gInPc1	skryt
pod	pod	k7c7	pod
dlažbou	dlažba	k1gFnSc7	dlažba
<g/>
.	.	kIx.	.
</s>
<s>
Staroměšťané	Staroměšťan	k1gMnPc1	Staroměšťan
měli	mít	k5eAaImAgMnP	mít
Mariánské	mariánský	k2eAgNnSc4d1	Mariánské
náměstí	náměstí	k1gNnSc4	náměstí
s	s	k7c7	s
mariánským	mariánský	k2eAgInSc7d1	mariánský
morovým	morový	k2eAgInSc7d1	morový
sloupem	sloup	k1gInSc7	sloup
<g/>
,	,	kIx,	,
za	za	k7c2	za
komunismu	komunismus	k1gInSc2	komunismus
zvané	zvaný	k2eAgNnSc1d1	zvané
náměstí	náměstí	k1gNnSc1	náměstí
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
náměstí	náměstí	k1gNnPc4	náměstí
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
spojena	spojit	k5eAaPmNgFnS	spojit
Prostřední	prostřední	k2eAgFnSc7d1	prostřední
ulicí	ulice	k1gFnSc7	ulice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
úzká	úzký	k2eAgFnSc1d1	úzká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
snadno	snadno	k6eAd1	snadno
přehradit	přehradit	k5eAaPmF	přehradit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgNnPc7	dva
etniky	etnikum	k1gNnPc7	etnikum
vznikaly	vznikat	k5eAaImAgFnP	vznikat
často	často	k6eAd1	často
šarvátky	šarvátka	k1gFnPc1	šarvátka
a	a	k8xC	a
boje	boj	k1gInPc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
<g/>
:	:	kIx,	:
Uprostřed	uprostřed	k7c2	uprostřed
Prostřední	prostřední	k2eAgFnSc2d1	prostřední
ulice	ulice	k1gFnSc2	ulice
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1296	[number]	k4	1296
<g/>
,	,	kIx,	,
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
původních	původní	k2eAgInPc2d1	původní
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
postavena	postaven	k2eAgFnSc1d1	postavena
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejzachovalejší	zachovalý	k2eAgFnSc7d3	nejzachovalejší
ukázkou	ukázka	k1gFnSc7	ukázka
středověké	středověký	k2eAgFnSc2d1	středověká
architektury	architektura	k1gFnSc2	architektura
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
dvou	dva	k4xCgInPc2	dva
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
starší	starý	k2eAgInSc4d2	starší
levý	levý	k2eAgInSc4d1	levý
dům	dům	k1gInSc4	dům
má	mít	k5eAaImIp3nS	mít
gotickou	gotický	k2eAgFnSc4d1	gotická
dispozici	dispozice	k1gFnSc4	dispozice
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgFnSc4d1	renesanční
část	část	k1gFnSc4	část
je	být	k5eAaImIp3nS	být
střed	střed	k1gInSc1	střed
průjezdu	průjezd	k1gInSc2	průjezd
a	a	k8xC	a
horní	horní	k2eAgFnSc1d1	horní
síň	síň	k1gFnSc1	síň
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgInSc1d2	mladší
dům	dům	k1gInSc1	dům
má	mít	k5eAaImIp3nS	mít
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgMnSc1d1	barokní
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
domy	dům	k1gInPc1	dům
jsou	být	k5eAaImIp3nP	být
propojeny	propojit	k5eAaPmNgInP	propojit
barokním	barokní	k2eAgInSc7d1	barokní
portálem	portál	k1gInSc7	portál
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
průjezdu	průjezd	k1gInSc2	průjezd
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
úpravy	úprava	k1gFnPc1	úprava
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Radnice	radnice	k1gFnSc1	radnice
sloužila	sloužit	k5eAaImAgFnS	sloužit
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
až	až	k6eAd1	až
do	do	k7c2	do
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
Palackého	Palackého	k2eAgNnSc6d1	Palackého
náměstí	náměstí	k1gNnSc6	náměstí
nová	nový	k2eAgFnSc1d1	nová
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
radnice	radnice	k1gFnSc1	radnice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
staré	starý	k2eAgFnSc2d1	stará
radnice	radnice	k1gFnSc2	radnice
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
o	o	k7c4	o
několik	několik	k4yIc4	několik
stupňů	stupeň	k1gInPc2	stupeň
do	do	k7c2	do
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
"	"	kIx"	"
<g/>
slovácká	slovácký	k2eAgFnSc1d1	Slovácká
Pisa	Pisa	k1gFnSc1	Pisa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
ironické	ironický	k2eAgNnSc1d1	ironické
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
technika	technika	k1gFnSc1	technika
staveb	stavba	k1gFnPc2	stavba
na	na	k7c6	na
pískových	pískový	k2eAgInPc6d1	pískový
sedimentech	sediment	k1gInPc6	sediment
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
převzata	převzat	k2eAgFnSc1d1	převzata
z	z	k7c2	z
laguny	laguna	k1gFnSc2	laguna
italských	italský	k2eAgFnPc2d1	italská
Benátek	Benátky	k1gFnPc2	Benátky
a	a	k8xC	a
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
zavrtání	zavrtání	k1gNnSc6	zavrtání
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
kmenů	kmen	k1gInPc2	kmen
dubu	dub	k1gInSc2	dub
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc2	borovice
<g/>
,	,	kIx,	,
smrku	smrk	k1gInSc2	smrk
či	či	k8xC	či
sosny	sosna	k1gFnSc2	sosna
do	do	k7c2	do
pískového	pískový	k2eAgNnSc2d1	pískové
podloží	podloží	k1gNnSc2	podloží
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
hojně	hojně	k6eAd1	hojně
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
a	a	k8xC	a
tuto	tento	k3xDgFnSc4	tento
technologii	technologie	k1gFnSc4	technologie
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
realizovali	realizovat	k5eAaBmAgMnP	realizovat
často	často	k6eAd1	často
řemeslníci	řemeslník	k1gMnPc1	řemeslník
vyrábějící	vyrábějící	k2eAgFnSc2d1	vyrábějící
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Uherská	uherský	k2eAgFnSc1d1	uherská
cesta	cesta	k1gFnSc1	cesta
<g/>
:	:	kIx,	:
Během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
byla	být	k5eAaImAgFnS	být
stará	starý	k2eAgFnSc1d1	stará
obchodní	obchodní	k2eAgFnSc1d1	obchodní
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Uher	uher	k1gInSc4	uher
přeložena	přeložit	k5eAaPmNgFnS	přeložit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
procházela	procházet	k5eAaImAgFnS	procházet
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
mostem	most	k1gInSc7	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Moravu	Morava	k1gFnSc4	Morava
procházela	procházet	k5eAaImAgFnS	procházet
Staroměstskou	staroměstský	k2eAgFnSc7d1	Staroměstská
branou	brána	k1gFnSc7	brána
na	na	k7c4	na
Mariánské	mariánský	k2eAgNnSc4d1	Mariánské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
na	na	k7c4	na
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stáčela	stáčet	k5eAaImAgFnS	stáčet
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Havlíčkovy	Havlíčkův	k2eAgFnSc2d1	Havlíčkova
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
ulice	ulice	k1gFnSc1	ulice
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
náměstí	náměstí	k1gNnSc4	náměstí
také	také	k9	také
pod	pod	k7c7	pod
neobvyklým	obvyklý	k2eNgInSc7d1	neobvyklý
úhlem	úhel	k1gInSc7	úhel
45	[number]	k4	45
stupňů	stupeň	k1gInPc2	stupeň
místo	místo	k7c2	místo
pravoúhlého	pravoúhlý	k2eAgNnSc2d1	pravoúhlé
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
Kunovickou	kunovický	k2eAgFnSc7d1	kunovická
branou	brána	k1gFnSc7	brána
opouštěla	opouštět	k5eAaImAgFnS	opouštět
město	město	k1gNnSc4	město
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Vlárskému	vlárský	k2eAgInSc3d1	vlárský
průsmyku	průsmyk	k1gInSc3	průsmyk
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
<g/>
:	:	kIx,	:
Opevnění	opevnění	k1gNnSc1	opevnění
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
dřevěné	dřevěný	k2eAgNnSc1d1	dřevěné
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
více	hodně	k6eAd2	hodně
fortifikační	fortifikační	k2eAgNnSc1d1	fortifikační
<g/>
:	:	kIx,	:
asi	asi	k9	asi
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
započato	započnout	k5eAaPmNgNnS	započnout
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
kamenných	kamenný	k2eAgFnPc2d1	kamenná
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
česko-uherských	českoherský	k2eAgFnPc2d1	česko-uherská
válek	válka	k1gFnPc2	válka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
pevnost	pevnost	k1gFnSc1	pevnost
přebudována	přebudovat	k5eAaPmNgFnS	přebudovat
<g/>
.	.	kIx.	.
</s>
<s>
Existovalo	existovat	k5eAaImAgNnS	existovat
několik	několik	k4yIc1	několik
bran	brána	k1gFnPc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Dochovala	dochovat	k5eAaPmAgFnS	dochovat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
Shořelá	shořelý	k2eAgFnSc1d1	shořelá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Matyášova	Matyášův	k2eAgFnSc1d1	Matyášova
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
každý	každý	k3xTgMnSc1	každý
musel	muset	k5eAaImAgMnS	muset
sklonit	sklonit	k5eAaPmF	sklonit
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
musel	muset	k5eAaImAgMnS	muset
sklonit	sklonit	k5eAaPmF	sklonit
před	před	k7c7	před
městem	město	k1gNnSc7	město
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
darovat	darovat	k5eAaPmF	darovat
rytíře	rytíř	k1gMnSc4	rytíř
do	do	k7c2	do
znaku	znak	k1gInSc2	znak
za	za	k7c4	za
udatnost	udatnost	k1gFnSc4	udatnost
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
pevnosti	pevnost	k1gFnSc2	pevnost
během	během	k7c2	během
česko-uherských	českoherský	k2eAgFnPc2d1	česko-uherská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
bylo	být	k5eAaImAgNnS	být
dobyto	dobýt	k5eAaPmNgNnS	dobýt
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
historii	historie	k1gFnSc4	historie
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Prusy	Prus	k1gMnPc7	Prus
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
:	:	kIx,	:
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
stavbou	stavba	k1gFnSc7	stavba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
odsvěcena	odsvětit	k5eAaPmNgFnS	odsvětit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
také	také	k9	také
jako	jako	k9	jako
hostinec	hostinec	k1gInSc4	hostinec
či	či	k8xC	či
sklad	sklad	k1gInSc4	sklad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
opravě	oprava	k1gFnSc6	oprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
lékárna	lékárna	k1gFnSc1	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
<g/>
:	:	kIx,	:
Jediná	jediný	k2eAgFnSc1d1	jediná
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
renesanční	renesanční	k2eAgFnSc1d1	renesanční
stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1578	[number]	k4	1578
s	s	k7c7	s
renesančním	renesanční	k2eAgNnSc7d1	renesanční
podloubím	podloubí	k1gNnSc7	podloubí
vysokým	vysoký	k2eAgNnSc7d1	vysoké
5,5	[number]	k4	5,5
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
byl	být	k5eAaImAgInS	být
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
stavební	stavební	k2eAgInSc1d1	stavební
předpis	předpis	k1gInSc1	předpis
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jím	on	k3xPp3gNnSc7	on
mohl	moct	k5eAaImAgInS	moct
projet	projet	k5eAaPmF	projet
plně	plně	k6eAd1	plně
naložený	naložený	k2eAgInSc4d1	naložený
vůz	vůz	k1gInSc4	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
skladiště	skladiště	k1gNnSc4	skladiště
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Sousední	sousední	k2eAgNnSc1d1	sousední
podloubí	podloubí	k1gNnSc1	podloubí
u	u	k7c2	u
lékárny	lékárna	k1gFnSc2	lékárna
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
estetická	estetický	k2eAgFnSc1d1	estetická
úprava	úprava	k1gFnSc1	úprava
provedena	provést	k5eAaPmNgFnS	provést
za	za	k7c2	za
přestavby	přestavba	k1gFnSc2	přestavba
domu	dům	k1gInSc2	dům
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
řády	řád	k1gInPc1	řád
-	-	kIx~	-
františkáni	františkán	k1gMnPc1	františkán
a	a	k8xC	a
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
Janem	Jan	k1gMnSc7	Jan
Filipcem	Filipec	k1gMnSc7	Filipec
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
hodnotu	hodnota	k1gFnSc4	hodnota
má	mít	k5eAaImIp3nS	mít
jeho	jeho	k3xOp3gInSc4	jeho
refektář	refektář	k1gInSc4	refektář
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
sídlí	sídlet	k5eAaImIp3nS	sídlet
okresní	okresní	k2eAgInSc1d1	okresní
archiv	archiv	k1gInSc1	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Jezuité	jezuita	k1gMnPc1	jezuita
<g/>
:	:	kIx,	:
Jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
budovy	budova	k1gFnSc2	budova
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
dnes	dnes	k6eAd1	dnes
farního	farní	k2eAgInSc2d1	farní
a	a	k8xC	a
děkanátního	děkanátní	k2eAgInSc2d1	děkanátní
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
Xaverského	xaverský	k2eAgMnSc2d1	xaverský
<g/>
,	,	kIx,	,
jezuitské	jezuitský	k2eAgFnPc1d1	jezuitská
koleje	kolej	k1gFnPc1	kolej
a	a	k8xC	a
gymnázia	gymnázium	k1gNnPc1	gymnázium
s	s	k7c7	s
divadelním	divadelní	k2eAgInSc7d1	divadelní
sálem	sál	k1gInSc7	sál
tzv.	tzv.	kA	tzv.
Reduta	reduta	k1gFnSc1	reduta
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgMnS	postavit
jej	on	k3xPp3gMnSc4	on
Jan	Jan	k1gMnSc1	Jan
Jeroným	Jeroným	k1gMnSc1	Jeroným
Canevallo	Canevallo	k1gNnSc4	Canevallo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1670	[number]	k4	1670
<g/>
-	-	kIx~	-
<g/>
1685	[number]	k4	1685
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc4	projekt
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Jan	Jan	k1gMnSc1	Jan
Dominik	Dominik	k1gMnSc1	Dominik
Orsi	Orsi	k1gNnPc2	Orsi
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltářní	oltářní	k2eAgInSc1d1	oltářní
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
Křest	křest	k1gInSc1	křest
indického	indický	k2eAgMnSc2d1	indický
prince	princ	k1gMnSc2	princ
sv.	sv.	kA	sv.
Františkem	František	k1gMnSc7	František
Xaverským	xaverský	k2eAgFnPc3d1	xaverský
autor	autor	k1gMnSc1	autor
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Heinsch	Heinsch	k1gMnSc1	Heinsch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1689	[number]	k4	1689
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bočních	boční	k2eAgFnPc6d1	boční
kaplích	kaple	k1gFnPc6	kaple
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejcennějším	cenný	k2eAgFnPc3d3	nejcennější
barokní	barokní	k2eAgInPc1d1	barokní
obrazy	obraz	k1gInPc7	obraz
(	(	kIx(	(
<g/>
autorem	autor	k1gMnSc7	autor
Ignác	Ignác	k1gMnSc1	Ignác
Raab	Raab	k1gMnSc1	Raab
<g/>
)	)	kIx)	)
či	či	k8xC	či
plastiky	plastika	k1gFnSc2	plastika
(	(	kIx(	(
<g/>
sochař	sochař	k1gMnSc1	sochař
Ondřej	Ondřej	k1gMnSc1	Ondřej
Schweigl	Schweigl	k1gMnSc1	Schweigl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
benátské	benátský	k2eAgInPc1d1	benátský
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
dřevěném	dřevěný	k2eAgInSc6d1	dřevěný
roštu	rošt	k1gInSc6	rošt
z	z	k7c2	z
borovice	borovice	k1gFnSc2	borovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
pilířům	pilíř	k1gInPc3	pilíř
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
sonda	sonda	k1gFnSc1	sonda
a	a	k8xC	a
technologický	technologický	k2eAgInSc1d1	technologický
průzkum	průzkum	k1gInSc1	průzkum
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dřevo	dřevo	k1gNnSc1	dřevo
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
bezvadném	bezvadný	k2eAgInSc6d1	bezvadný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Palackého	Palackého	k2eAgNnSc6d1	Palackého
náměstí	náměstí	k1gNnSc6	náměstí
stojí	stát	k5eAaImIp3nS	stát
barokní	barokní	k2eAgFnSc1d1	barokní
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Šebestiána	Šebestián	k1gMnSc2	Šebestián
postavená	postavený	k2eAgFnSc1d1	postavená
roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
městskou	městský	k2eAgFnSc7d1	městská
posádkou	posádka	k1gFnSc7	posádka
jako	jako	k8xC	jako
výraz	výraz	k1gInSc1	výraz
díků	dík	k1gInPc2	dík
za	za	k7c4	za
odvrácení	odvrácení	k1gNnSc4	odvrácení
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Lékárna	lékárna	k1gFnSc1	lékárna
U	u	k7c2	u
zlaté	zlatý	k2eAgFnSc2d1	zlatá
koruny	koruna	k1gFnSc2	koruna
<g/>
:	:	kIx,	:
Barokní	barokní	k2eAgFnSc1d1	barokní
Lékárna	lékárna	k1gFnSc1	lékárna
U	u	k7c2	u
zlaté	zlatý	k2eAgFnSc2d1	zlatá
koruny	koruna	k1gFnSc2	koruna
sloužila	sloužit	k5eAaImAgFnS	sloužit
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
původní	původní	k2eAgInPc1d1	původní
měšťanské	měšťanský	k2eAgInPc1d1	měšťanský
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgInPc1d1	stojící
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
barokně	barokně	k6eAd1	barokně
přestavěny	přestavěn	k2eAgFnPc1d1	přestavěna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
pseudorenesanční	pseudorenesanční	k2eAgFnSc7d1	pseudorenesanční
fasádou	fasáda	k1gFnSc7	fasáda
se	s	k7c7	s
sgrafitovou	sgrafitový	k2eAgFnSc7d1	sgrafitová
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Schaniak	Schaniak	k1gMnSc1	Schaniak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
původní	původní	k2eAgFnSc1d1	původní
nástropní	nástropní	k2eAgFnSc1d1	nástropní
freska	freska	k1gFnSc1	freska
(	(	kIx(	(
<g/>
Joseph	Joseph	k1gMnSc1	Joseph
Ignatz	Ignatz	k1gMnSc1	Ignatz
Sadler	Sadler	k1gMnSc1	Sadler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgFnSc1d1	doplněná
štukaturou	štukatura	k1gFnSc7	štukatura
pocházející	pocházející	k2eAgFnSc7d1	pocházející
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Sovy	sova	k1gFnSc2	sova
<g/>
:	:	kIx,	:
Mariánské	mariánský	k2eAgNnSc1d1	Mariánské
náměstí	náměstí	k1gNnSc1	náměstí
č.	č.	k?	č.
46	[number]	k4	46
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
starý	starý	k2eAgInSc4d1	starý
barokní	barokní	k2eAgInSc4d1	barokní
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
U	u	k7c2	u
Sovy	sova	k1gFnSc2	sova
není	být	k5eNaImIp3nS	být
původní	původní	k2eAgMnSc1d1	původní
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k6eAd1	až
z	z	k7c2	z
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
štítech	štít	k1gInPc6	štít
byla	být	k5eAaImAgNnP	být
často	často	k6eAd1	často
jen	jen	k6eAd1	jen
datace	datace	k1gFnSc1	datace
ukrytá	ukrytý	k2eAgFnSc1d1	ukrytá
do	do	k7c2	do
latinského	latinský	k2eAgInSc2d1	latinský
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Bývalo	bývat	k5eAaImAgNnS	bývat
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
biblický	biblický	k2eAgInSc1d1	biblický
<g/>
,	,	kIx,	,
skrýval	skrývat	k5eAaImAgInS	skrývat
rok	rok	k1gInSc1	rok
vyhotovení	vyhotovení	k1gNnSc2	vyhotovení
památky	památka	k1gFnSc2	památka
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
nad	nad	k7c7	nad
vchodem	vchod	k1gInSc7	vchod
do	do	k7c2	do
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1348	[number]	k4	1348
a	a	k8xC	a
1860	[number]	k4	1860
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
sídlem	sídlo	k1gNnSc7	sídlo
Hradišťského	hradišťský	k2eAgInSc2d1	hradišťský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
obyvateli	obyvatel	k1gMnSc3	obyvatel
města	město	k1gNnSc2	město
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
založení	založení	k1gNnSc6	založení
byli	být	k5eAaImAgMnP	být
osadníci	osadník	k1gMnPc1	osadník
z	z	k7c2	z
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
vesnic	vesnice	k1gFnPc2	vesnice
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
a	a	k8xC	a
Kunovice	Kunovice	k1gFnPc4	Kunovice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obce	obec	k1gFnPc1	obec
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1960	[number]	k4	1960
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
součástí	součást	k1gFnPc2	součást
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
Staré	Staré	k2eAgNnSc4d1	Staré
Město	město	k1gNnSc4	město
a	a	k8xC	a
Kunovice	Kunovice	k1gFnPc4	Kunovice
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
obcemi	obec	k1gFnPc7	obec
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
povýšenými	povýšený	k2eAgFnPc7d1	povýšená
na	na	k7c4	na
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
pevnosti	pevnost	k1gFnSc2	pevnost
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k6eAd1	až
od	od	k7c2	od
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
expandovala	expandovat	k5eAaImAgFnS	expandovat
městská	městský	k2eAgFnSc1d1	městská
zástavba	zástavba	k1gFnSc1	zástavba
mimo	mimo	k7c4	mimo
staré	starý	k2eAgFnPc4d1	stará
hradby	hradba	k1gFnPc4	hradba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1842	[number]	k4	1842
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
tzv.	tzv.	kA	tzv.
Severní	severní	k2eAgFnSc1d1	severní
dráha	dráha	k1gFnSc1	dráha
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
Přerova	Přerov	k1gInSc2	Přerov
a	a	k8xC	a
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Radní	radní	k1gMnPc1	radní
města	město	k1gNnSc2	město
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
vedení	vedení	k1gNnSc4	vedení
trati	trať	k1gFnSc6	trať
krajským	krajský	k2eAgNnSc7d1	krajské
městem	město	k1gNnSc7	město
a	a	k8xC	a
trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
obce	obec	k1gFnSc2	obec
Staré	Stará	k1gFnSc2	Stará
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
regionu	region	k1gInSc6	region
vládl	vládnout	k5eAaImAgMnS	vládnout
duch	duch	k1gMnSc1	duch
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1848	[number]	k4	1848
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
základní	základní	k2eAgInPc1d1	základní
kameny	kámen	k1gInPc1	kámen
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
celkem	celek	k1gInSc7	celek
20	[number]	k4	20
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
i	i	k9	i
pískovcový	pískovcový	k2eAgInSc1d1	pískovcový
kámen	kámen	k1gInSc1	kámen
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
Buchlova	Buchlův	k2eAgInSc2d1	Buchlův
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgFnPc2d1	významná
budov	budova	k1gFnPc2	budova
<g/>
:	:	kIx,	:
krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
s	s	k7c7	s
věznicí	věznice	k1gFnSc7	věznice
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staré	starý	k2eAgFnSc2d1	stará
pošty	pošta	k1gFnSc2	pošta
a	a	k8xC	a
zbrojnice	zbrojnice	k1gFnSc1	zbrojnice
nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
na	na	k7c6	na
Palackého	Palackého	k2eAgNnSc6d1	Palackého
náměstí	náměstí	k1gNnSc6	náměstí
budova	budova	k1gFnSc1	budova
českého	český	k2eAgNnSc2d1	české
gymnázia	gymnázium	k1gNnSc2	gymnázium
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
podobu	podoba	k1gFnSc4	podoba
zlaté	zlatý	k2eAgFnSc2d1	zlatá
kapličky	kaplička	k1gFnSc2	kaplička
se	s	k7c7	s
stříškou	stříška	k1gFnSc7	stříška
inspirovanou	inspirovaný	k2eAgFnSc4d1	inspirovaná
Národním	národní	k2eAgNnSc7d1	národní
divadlem	divadlo	k1gNnSc7	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g />
.	.	kIx.	.
</s>
<s>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
zřizovací	zřizovací	k2eAgFnSc6d1	zřizovací
komisi	komise	k1gFnSc6	komise
stále	stále	k6eAd1	stále
vládl	vládnout	k5eAaImAgMnS	vládnout
obrozenecký	obrozenecký	k2eAgMnSc1d1	obrozenecký
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1892	[number]	k4	1892
až	až	k9	až
1897	[number]	k4	1897
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
firmou	firma	k1gFnSc7	firma
Nekvasil	kvasit	k5eNaImAgInS	kvasit
Justiční	justiční	k2eAgInSc1d1	justiční
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Střední	střední	k2eAgFnSc1d1	střední
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1	uměleckoprůmyslová
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1883	[number]	k4	1883
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
tzv.	tzv.	kA	tzv.
Vlárská	vlárský	k2eAgFnSc1d1	Vlárská
dráha	dráha	k1gFnSc1	dráha
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
až	až	k9	až
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
vytvořením	vytvoření	k1gNnSc7	vytvoření
spojky	spojka	k1gFnSc2	spojka
mezi	mezi	k7c7	mezi
Kunovicemi	Kunovice	k1gFnPc7	Kunovice
a	a	k8xC	a
Starým	starý	k2eAgNnSc7d1	staré
Městem	město	k1gNnSc7	město
železnice	železnice	k1gFnSc2	železnice
i	i	k9	i
do	do	k7c2	do
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
firmy	firma	k1gFnSc2	firma
Baťa	Baťa	k1gMnSc1	Baťa
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
z	z	k7c2	z
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
malého	malý	k2eAgNnSc2d1	malé
městečka	městečko	k1gNnSc2	městečko
Zlína	Zlín	k1gInSc2	Zlín
přistěhoval	přistěhovat	k5eAaPmAgMnS	přistěhovat
švec	švec	k1gMnSc1	švec
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
starší	starší	k1gMnSc1	starší
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
si	se	k3xPyFc3	se
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
části	část	k1gFnSc6	část
Rybárny	rybárna	k1gFnSc2	rybárna
obuvnickou	obuvnický	k2eAgFnSc4d1	obuvnická
živnost	živnost	k1gFnSc4	živnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
syny	syn	k1gMnPc7	syn
Antonínem	Antonín	k1gMnSc7	Antonín
a	a	k8xC	a
Tomášem	Tomáš	k1gMnSc7	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Dílna	dílna	k1gFnSc1	dílna
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
několika	několik	k4yIc2	několik
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
než	než	k8xS	než
její	její	k3xOp3gFnSc4	její
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
zastavil	zastavit	k5eAaPmAgInS	zastavit
požár	požár	k1gInSc1	požár
města	město	k1gNnSc2	město
a	a	k8xC	a
bankrot	bankrot	k1gInSc1	bankrot
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
dlužníka	dlužník	k1gMnSc2	dlužník
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
si	se	k3xPyFc3	se
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
sestra	sestra	k1gFnSc1	sestra
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
novou	nový	k2eAgFnSc4d1	nová
firmu	firma	k1gFnSc4	firma
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
celosvětovému	celosvětový	k2eAgInSc3d1	celosvětový
koncernu	koncern	k1gInSc3	koncern
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c4	na
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
společnost	společnost	k1gFnSc4	společnost
T	T	kA	T
<g/>
&	&	k?	&
<g/>
A.	A.	kA	A.
<g/>
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
ml.	ml.	kA	ml.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Anna	Anna	k1gFnSc1	Anna
odešla	odejít	k5eAaPmAgFnS	odejít
z	z	k7c2	z
vedení	vedení	k1gNnSc2	vedení
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Majitelem	majitel	k1gMnSc7	majitel
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
BAŤA	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tragicky	tragicky	k6eAd1	tragicky
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Uherskohradišťskou	uherskohradišťský	k2eAgFnSc4d1	Uherskohradišťská
dílnu	dílna	k1gFnSc4	dílna
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
manžela	manžel	k1gMnSc2	manžel
Antonína	Antonín	k1gMnSc2	Antonín
Bati	Baťa	k1gMnSc2	Baťa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
definitivně	definitivně	k6eAd1	definitivně
zrušila	zrušit	k5eAaPmAgFnS	zrušit
Baťova	Baťův	k2eAgFnSc1d1	Baťova
vdova	vdova	k1gFnSc1	vdova
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
také	také	k9	také
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svých	svůj	k3xOyFgMnPc2	svůj
sourozenců	sourozenec	k1gMnPc2	sourozenec
vedení	vedení	k1gNnSc1	vedení
zlínského	zlínský	k2eAgInSc2d1	zlínský
podniku	podnik	k1gInSc2	podnik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
převzal	převzít	k5eAaPmAgMnS	převzít
<g/>
.	.	kIx.	.
</s>
<s>
Tomášův	Tomášův	k2eAgMnSc1d1	Tomášův
syn	syn	k1gMnSc1	syn
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
ml.	ml.	kA	ml.
zase	zase	k9	zase
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
uherskohradišťské	uherskohradišťský	k2eAgFnSc6d1	Uherskohradišťská
obchodní	obchodní	k2eAgFnSc6d1	obchodní
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
má	mít	k5eAaImIp3nS	mít
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
obuví	obuv	k1gFnSc7	obuv
v	v	k7c6	v
Prostřední	prostřední	k2eAgFnSc6d1	prostřední
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
firma	firma	k1gFnSc1	firma
Baťa	Baťa	k1gMnSc1	Baťa
prodejnu	prodejna	k1gFnSc4	prodejna
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
Rybáren	rybárna	k1gFnPc2	rybárna
dnes	dnes	k6eAd1	dnes
začíná	začínat	k5eAaImIp3nS	začínat
třetí	třetí	k4xOgInSc4	třetí
úsek	úsek	k1gInSc4	úsek
Baťova	Baťův	k2eAgInSc2d1	Baťův
kanálu	kanál	k1gInSc2	kanál
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnPc1d1	vodní
cesty	cesta	k1gFnPc1	cesta
vybudované	vybudovaný	k2eAgFnPc1d1	vybudovaná
společností	společnost	k1gFnSc7	společnost
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
také	také	k9	také
zlínská	zlínský	k2eAgFnSc1d1	zlínská
Univerzita	univerzita	k1gFnSc1	univerzita
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
ml.	ml.	kA	ml.
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
tehdy	tehdy	k6eAd1	tehdy
mj.	mj.	kA	mj.
usedl	usednout	k5eAaPmAgInS	usednout
do	do	k7c2	do
lavice	lavice	k1gFnSc2	lavice
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
Zemská	zemský	k2eAgFnSc1d1	zemská
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
nákladem	náklad	k1gInSc7	náklad
340	[number]	k4	340
000	[number]	k4	000
K	K	kA	K
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
zemského	zemský	k2eAgMnSc2d1	zemský
architekta	architekt	k1gMnSc2	architekt
J.	J.	kA	J.
Karáska	Karásek	k1gMnSc2	Karásek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stavbu	stavba	k1gFnSc4	stavba
zdržel	zdržet	k5eAaPmAgInS	zdržet
odpor	odpor	k1gInSc1	odpor
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
dané	daný	k2eAgFnSc2d1	daná
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
první	první	k4xOgInSc1	první
špitál	špitál	k1gInSc1	špitál
sv.	sv.	kA	sv.
Alžběty	Alžběta	k1gFnSc2	Alžběta
roku	rok	k1gInSc2	rok
1362	[number]	k4	1362
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
financován	financovat	k5eAaBmNgInS	financovat
z	z	k7c2	z
milodarů	milodar	k1gInPc2	milodar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1727	[number]	k4	1727
bylo	být	k5eAaImAgNnS	být
zvýšeno	zvýšen	k2eAgNnSc1d1	zvýšeno
počet	počet	k1gInSc4	počet
lůžek	lůžko	k1gNnPc2	lůžko
z	z	k7c2	z
osmi	osm	k4xCc2	osm
na	na	k7c4	na
dvanáct	dvanáct	k4xCc4	dvanáct
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
krajský	krajský	k2eAgMnSc1d1	krajský
hejtman	hejtman	k1gMnSc1	hejtman
Ignác	Ignác	k1gMnSc1	Ignác
z	z	k7c2	z
Bevieru	Bevier	k1gInSc2	Bevier
zřídil	zřídit	k5eAaPmAgMnS	zřídit
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
postele	postel	k1gFnPc4	postel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
špitál	špitál	k1gInSc1	špitál
začal	začít	k5eAaPmAgInS	začít
měnit	měnit	k5eAaImF	měnit
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
nemocnici	nemocnice	k1gFnSc4	nemocnice
zřídil	zřídit	k5eAaPmAgMnS	zřídit
majitel	majitel	k1gMnSc1	majitel
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
Berchtold	Berchtold	k1gMnSc1	Berchtold
v	v	k7c6	v
lisovně	lisovna	k1gFnSc6	lisovna
oleje	olej	k1gInSc2	olej
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
buchlovském	buchlovský	k2eAgNnSc6d1	buchlovské
panství	panství	k1gNnSc6	panství
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
12	[number]	k4	12
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
otevřel	otevřít	k5eAaPmAgInS	otevřít
velkou	velký	k2eAgFnSc4d1	velká
nemocnici	nemocnice	k1gFnSc4	nemocnice
o	o	k7c6	o
52	[number]	k4	52
lůžkách	lůžko	k1gNnPc6	lůžko
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Buchlovicích	Buchlovice	k1gFnPc6	Buchlovice
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
jednoho	jeden	k4xCgMnSc4	jeden
lékaře	lékař	k1gMnSc4	lékař
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
duchovního	duchovní	k1gMnSc4	duchovní
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc4	dva
ošetřovatele	ošetřovatel	k1gMnPc4	ošetřovatel
a	a	k8xC	a
18	[number]	k4	18
lůžek	lůžko	k1gNnPc2	lůžko
pro	pro	k7c4	pro
přestárlé	přestárlý	k1gMnPc4	přestárlý
<g/>
.	.	kIx.	.
</s>
<s>
Přijímal	přijímat	k5eAaImAgMnS	přijímat
nejen	nejen	k6eAd1	nejen
místní	místní	k2eAgMnSc1d1	místní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pocestné	pocestná	k1gFnSc2	pocestná
<g/>
;	;	kIx,	;
omezení	omezení	k1gNnSc6	omezení
měli	mít	k5eAaImAgMnP	mít
duševně	duševně	k6eAd1	duševně
nemocní	nemocný	k1gMnPc1	nemocný
a	a	k8xC	a
epileptici	epileptik	k1gMnPc1	epileptik
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
pohlavně	pohlavně	k6eAd1	pohlavně
nemocní	nemocný	k1gMnPc1	nemocný
-	-	kIx~	-
ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
přijati	přijmout	k5eAaPmNgMnP	přijmout
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
první	první	k4xOgFnSc6	první
nákaze	nákaza	k1gFnSc6	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1899	[number]	k4	1899
usilovala	usilovat	k5eAaImAgFnS	usilovat
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
zemské	zemský	k2eAgFnSc2d1	zemská
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Buchlovicích	Buchlovice	k1gFnPc6	Buchlovice
se	s	k7c7	s
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1908	[number]	k4	1908
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
schůzka	schůzka	k1gFnSc1	schůzka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dále	daleko	k6eAd2	daleko
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
tzv.	tzv.	kA	tzv.
Buchlovickou	buchlovický	k2eAgFnSc4d1	Buchlovická
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Sešli	sejít	k5eAaPmAgMnP	sejít
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
k	k	k7c3	k
jednání	jednání	k1gNnSc2	jednání
rakousko-uherský	rakouskoherský	k2eAgMnSc1d1	rakousko-uherský
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Aloise	Alois	k1gMnSc2	Alois
Lexa	Lexa	k1gMnSc1	Lexa
z	z	k7c2	z
Aehrenthalu	Aehrenthal	k1gInSc2	Aehrenthal
a	a	k8xC	a
ruský	ruský	k2eAgMnSc1d1	ruský
ministr	ministr	k1gMnSc1	ministr
Alexandr	Alexandr	k1gMnSc1	Alexandr
Petrovič	Petrovič	k1gMnSc1	Petrovič
Izvolskij	Izvolskij	k1gMnSc1	Izvolskij
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
jednání	jednání	k1gNnSc2	jednání
bylo	být	k5eAaImAgNnS	být
napětí	napětí	k1gNnSc1	napětí
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
Aehrenthal	Aehrenthal	k1gMnSc1	Aehrenthal
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
souhlas	souhlas	k1gInSc4	souhlas
Ruska	Rusko	k1gNnSc2	Rusko
k	k	k7c3	k
anexi	anexe	k1gFnSc3	anexe
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
obsazení	obsazení	k1gNnSc1	obsazení
Bosny	Bosna	k1gFnSc2	Bosna
Rakouskem	Rakousko	k1gNnSc7	Rakousko
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Hostitelem	hostitel	k1gMnSc7	hostitel
jednajících	jednající	k2eAgMnPc2d1	jednající
ministrů	ministr	k1gMnPc2	ministr
byl	být	k5eAaImAgMnS	být
majitel	majitel	k1gMnSc1	majitel
buchlovického	buchlovický	k2eAgInSc2d1	buchlovický
zámku	zámek	k1gInSc2	zámek
Leopold	Leopolda	k1gFnPc2	Leopolda
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Berchtold	Berchtold	k1gMnSc1	Berchtold
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
později	pozdě	k6eAd2	pozdě
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Aehrenthala	Aehrenthal	k1gMnSc4	Aehrenthal
ve	v	k7c4	v
funkci	funkce	k1gFnSc4	funkce
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
(	(	kIx(	(
<g/>
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1912	[number]	k4	1912
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podal	podat	k5eAaPmAgMnS	podat
demisi	demise	k1gFnSc4	demise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
podpis	podpis	k1gInSc1	podpis
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
ultimátem	ultimátum	k1gNnSc7	ultimátum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc3	rok
1914	[number]	k4	1914
zasláno	zaslat	k5eAaPmNgNnS	zaslat
Srbsku	Srbsko	k1gNnSc3	Srbsko
a	a	k8xC	a
jehož	jehož	k3xOyRp3gNnSc7	jehož
odmítnutím	odmítnutí	k1gNnSc7	odmítnutí
poté	poté	k6eAd1	poté
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgMnS	moct
dlouho	dlouho	k6eAd1	dlouho
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
panství	panství	k1gNnSc4	panství
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gNnSc3	on
kladena	kladen	k2eAgNnPc4d1	kladeno
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
protislovanská	protislovanský	k2eAgFnSc1d1	protislovanská
politika	politika	k1gFnSc1	politika
a	a	k8xC	a
rozpoutání	rozpoutání	k1gNnSc1	rozpoutání
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hájil	hájit	k5eAaImAgInS	hájit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ultimátum	ultimátum	k1gNnSc4	ultimátum
podepsal	podepsat	k5eAaPmAgMnS	podepsat
jako	jako	k9	jako
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgMnS	mít
úředně	úředně	k6eAd1	úředně
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
trvale	trvale	k6eAd1	trvale
pobývat	pobývat	k5eAaImF	pobývat
na	na	k7c6	na
území	území	k1gNnSc6	území
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
majetku	majetek	k1gInSc3	majetek
mu	on	k3xPp3gNnSc3	on
byl	být	k5eAaImAgInS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
až	až	k9	až
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gMnSc3	on
nadiktovala	nadiktovat	k5eAaPmAgFnS	nadiktovat
místní	místní	k2eAgFnSc1d1	místní
samospráva	samospráva	k1gFnSc1	samospráva
<g/>
:	:	kIx,	:
vyplatit	vyplatit	k5eAaPmF	vyplatit
odškodné	odškodné	k1gNnSc4	odškodné
válečným	válečný	k2eAgFnPc3d1	válečná
vdovám	vdova	k1gFnPc3	vdova
a	a	k8xC	a
sirotkům	sirotek	k1gMnPc3	sirotek
<g/>
,	,	kIx,	,
spolufinancovat	spolufinancovat	k5eAaImF	spolufinancovat
železniční	železniční	k2eAgFnPc4d1	železniční
trati	trať	k1gFnPc4	trať
do	do	k7c2	do
Morkovic	Morkovice	k1gFnPc2	Morkovice
atd.	atd.	kA	atd.
Československým	československý	k2eAgMnSc7d1	československý
občanem	občan	k1gMnSc7	občan
se	se	k3xPyFc4	se
však	však	k9	však
stát	stát	k1gInSc1	stát
nesměl	smět	k5eNaImAgInS	smět
<g/>
,	,	kIx,	,
panství	panství	k1gNnSc2	panství
spravoval	spravovat	k5eAaImAgMnS	spravovat
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Alois	Alois	k1gMnSc1	Alois
Berchtold	Berchtold	k1gMnSc1	Berchtold
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Peresznye	Pereszny	k1gFnSc2	Pereszny
u	u	k7c2	u
Šoproně	Šoproň	k1gFnSc2	Šoproň
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
zdědil	zdědit	k5eAaPmAgMnS	zdědit
panství	panství	k1gNnSc4	panství
Alois	Alois	k1gMnSc1	Alois
Berchtold	Berchtold	k1gMnSc1	Berchtold
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
zestátnění	zestátnění	k1gNnSc4	zestátnění
panství	panství	k1gNnSc2	panství
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Buchlovice	Buchlovice	k1gFnPc4	Buchlovice
navštívil	navštívit	k5eAaPmAgInS	navštívit
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pozitivně	pozitivně	k6eAd1	pozitivně
překvapen	překvapit	k5eAaPmNgMnS	překvapit
údržbou	údržba	k1gFnSc7	údržba
a	a	k8xC	a
stavem	stav	k1gInSc7	stav
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
založil	založit	k5eAaPmAgMnS	založit
inženýr	inženýr	k1gMnSc1	inženýr
Ludvík	Ludvík	k1gMnSc1	Ludvík
Kirschner	Kirschner	k1gMnSc1	Kirschner
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
mýdlo	mýdlo	k1gNnSc4	mýdlo
<g/>
,	,	kIx,	,
mazivo	mazivo	k1gNnSc4	mazivo
a	a	k8xC	a
laky	laka	k1gFnPc4	laka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
znárodněna	znárodněn	k2eAgFnSc1d1	znárodněna
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
Barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
laky	laka	k1gFnSc2	laka
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Colorlak	Colorlak	k1gMnSc1	Colorlak
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
Hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgNnSc4d1	kulturní
i	i	k8xC	i
politické	politický	k2eAgNnSc4d1	politické
centrum	centrum	k1gNnSc4	centrum
kraje	kraj	k1gInSc2	kraj
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přesouvalo	přesouvat	k5eAaImAgNnS	přesouvat
do	do	k7c2	do
Zlína	Zlín	k1gInSc2	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
navštívil	navštívit	k5eAaPmAgMnS	navštívit
starosta	starosta	k1gMnSc1	starosta
Chicaga	Chicago	k1gNnSc2	Chicago
Antonín	Antonín	k1gMnSc1	Antonín
Čermák	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
zahájen	zahájit	k5eAaPmNgInS	zahájit
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
Zlínský	zlínský	k2eAgInSc1d1	zlínský
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
pořádaly	pořádat	k5eAaImAgInP	pořádat
se	se	k3xPyFc4	se
automobilové	automobilový	k2eAgInPc1d1	automobilový
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
se	se	k3xPyFc4	se
Baťův	Baťův	k2eAgInSc1d1	Baťův
kanál	kanál	k1gInSc1	kanál
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
činností	činnost	k1gFnSc7	činnost
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
rodiny	rodina	k1gFnSc2	rodina
Baťů	Baťa	k1gMnPc2	Baťa
odsunuly	odsunout	k5eAaPmAgInP	odsunout
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
úřednického	úřednický	k2eAgNnSc2d1	úřednické
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
města	město	k1gNnSc2	město
dařilo	dařit	k5eAaImAgNnS	dařit
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
umělecké	umělecký	k2eAgInPc4d1	umělecký
trendy	trend	k1gInPc4	trend
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
rondokubismus	rondokubismus	k1gInSc1	rondokubismus
a	a	k8xC	a
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
protipól	protipól	k1gInSc4	protipól
vkusu	vkus	k1gInSc2	vkus
eklekticismu	eklekticismus	k1gInSc2	eklekticismus
a	a	k8xC	a
neostylům	neostyl	k1gInPc3	neostyl
vilek	vilka	k1gFnPc2	vilka
např.	např.	kA	např.
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
vily	vila	k1gFnSc2	vila
stavitele	stavitel	k1gMnSc2	stavitel
Šupky	Šupky	k?	Šupky
atd.	atd.	kA	atd.
Své	svůj	k3xOyFgInPc4	svůj
návrhy	návrh	k1gInPc4	návrh
ve	v	k7c6	v
městě	město	k1gNnSc6	město
realizovali	realizovat	k5eAaBmAgMnP	realizovat
architekti	architekt	k1gMnPc1	architekt
jako	jako	k8xS	jako
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
nebo	nebo	k8xC	nebo
Vladimír	Vladimír	k1gMnSc1	Vladimír
Zákrejs	Zákrejsa	k1gFnPc2	Zákrejsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
poté	poté	k6eAd1	poté
Adolf	Adolf	k1gMnSc1	Adolf
Liebscher	Liebschra	k1gFnPc2	Liebschra
realizoval	realizovat	k5eAaBmAgMnS	realizovat
největší	veliký	k2eAgFnSc4d3	veliký
funkcionalistickou	funkcionalistický	k2eAgFnSc4d1	funkcionalistická
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
Infekční	infekční	k2eAgInSc4d1	infekční
pavilón	pavilón	k1gInSc4	pavilón
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
dynamická	dynamický	k2eAgFnSc1d1	dynamická
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
cukrárna	cukrárna	k1gFnSc1	cukrárna
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
Kunovicích	Kunovice	k1gFnPc6	Kunovice
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
městské	městský	k2eAgNnSc1d1	Městské
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
strategických	strategický	k2eAgInPc2d1	strategický
důvodů	důvod	k1gInPc2	důvod
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
přemístit	přemístit	k5eAaPmF	přemístit
část	část	k1gFnSc4	část
výroby	výroba	k1gFnSc2	výroba
letadel	letadlo	k1gNnPc2	letadlo
ze	z	k7c2	z
západních	západní	k2eAgFnPc2d1	západní
a	a	k8xC	a
středních	střední	k2eAgFnPc2d1	střední
Čech	Čechy	k1gFnPc2	Čechy
do	do	k7c2	do
obce	obec	k1gFnSc2	obec
Kunovice	Kunovice	k1gFnPc1	Kunovice
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
zde	zde	k6eAd1	zde
tak	tak	k6eAd1	tak
nová	nový	k2eAgFnSc1d1	nová
tradice	tradice	k1gFnSc1	tradice
leteckého	letecký	k2eAgInSc2d1	letecký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
komplex	komplex	k1gInSc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Kunovicích	Kunovice	k1gFnPc6	Kunovice
pobočný	pobočný	k2eAgInSc4d1	pobočný
závod	závod	k1gInSc4	závod
pražské	pražský	k2eAgFnSc2d1	Pražská
letecké	letecký	k2eAgFnSc2d1	letecká
továrny	továrna	k1gFnSc2	továrna
Avia	Avia	k1gFnSc1	Avia
<g/>
.	.	kIx.	.
</s>
<s>
Záborem	zábor	k1gInSc7	zábor
Rakouska	Rakousko	k1gNnSc2	Rakousko
Hitlerem	Hitler	k1gMnSc7	Hitler
se	se	k3xPyFc4	se
však	však	k9	však
tento	tento	k3xDgInSc1	tento
strategický	strategický	k2eAgInSc1d1	strategický
plán	plán	k1gInSc1	plán
minul	minout	k5eAaImAgInS	minout
účinkem	účinek	k1gInSc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Výstava	výstava	k1gFnSc1	výstava
Slovácka	Slovácko	k1gNnSc2	Slovácko
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
až	až	k9	až
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1937	[number]	k4	1937
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
národopisná	národopisný	k2eAgFnSc1d1	národopisná
a	a	k8xC	a
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
Výstava	výstava	k1gFnSc1	výstava
Slovácka	Slovácko	k1gNnSc2	Slovácko
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podpořila	podpořit	k5eAaPmAgFnS	podpořit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
Slovácko	Slovácko	k1gNnSc4	Slovácko
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
národopis	národopis	k1gInSc4	národopis
mezi	mezi	k7c7	mezi
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
i	i	k8xC	i
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenku	myšlenka	k1gFnSc4	myšlenka
uspořádání	uspořádání	k1gNnSc2	uspořádání
výstavy	výstava	k1gFnSc2	výstava
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
tehdy	tehdy	k6eAd1	tehdy
oživilo	oživit	k5eAaPmAgNnS	oživit
Slovácké	slovácký	k2eAgNnSc1d1	Slovácké
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
podpořil	podpořit	k5eAaPmAgMnS	podpořit
je	on	k3xPp3gNnPc4	on
starosta	starosta	k1gMnSc1	starosta
Metoděj	Metoděj	k1gMnSc1	Metoděj
Garlík	Garlík	k1gMnSc1	Garlík
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
výstavního	výstavní	k2eAgInSc2d1	výstavní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Brněnský	brněnský	k2eAgMnSc1d1	brněnský
výtvarník	výtvarník	k1gMnSc1	výtvarník
Adolf	Adolf	k1gMnSc1	Adolf
Roštlapil	Roštlapil	k1gMnSc1	Roštlapil
vyprojektoval	vyprojektovat	k5eAaPmAgMnS	vyprojektovat
areál	areál	k1gInSc4	areál
výstaviště	výstaviště	k1gNnSc2	výstaviště
o	o	k7c6	o
33	[number]	k4	33
pavilonech	pavilon	k1gInPc6	pavilon
<g/>
,	,	kIx,	,
umístěného	umístěný	k2eAgMnSc2d1	umístěný
do	do	k7c2	do
Smetanových	Smetanových	k2eAgInPc2d1	Smetanových
sadů	sad	k1gInPc2	sad
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
náměstí	náměstí	k1gNnSc2	náměstí
Míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Přístavbou	přístavba	k1gFnSc7	přístavba
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Fuchse	Fuchs	k1gMnSc2	Fuchs
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
Slováckého	slovácký	k2eAgNnSc2d1	Slovácké
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
nedaleko	daleko	k6eNd1	daleko
v	v	k7c6	v
parku	park	k1gInSc6	park
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
i	i	k8xC	i
dodnes	dodnes	k6eAd1	dodnes
stojící	stojící	k2eAgFnSc1d1	stojící
Slovácká	slovácký	k2eAgFnSc1d1	Slovácká
búda	búda	k1gFnSc1	búda
<g/>
.	.	kIx.	.
</s>
<s>
Vystavovalo	vystavovat	k5eAaImAgNnS	vystavovat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
budovách	budova	k1gFnPc6	budova
hradišťského	hradišťský	k2eAgNnSc2d1	Hradišťské
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
škol	škola	k1gFnPc2	škola
(	(	kIx(	(
<g/>
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
měšťanské	měšťanský	k2eAgFnPc1d1	měšťanská
školy	škola	k1gFnPc1	škola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dětského	dětský	k2eAgInSc2d1	dětský
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
spořitelny	spořitelna	k1gFnSc2	spořitelna
a	a	k8xC	a
Jarošovy	Jarošův	k2eAgFnSc2d1	Jarošova
továrny	továrna	k1gFnSc2	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
kulturně	kulturně	k6eAd1	kulturně
výchovného	výchovné	k1gNnSc2	výchovné
(	(	kIx(	(
<g/>
národopisná	národopisný	k2eAgFnSc1d1	národopisná
výstava	výstava	k1gFnSc1	výstava
prezentující	prezentující	k2eAgNnSc1d1	prezentující
dílo	dílo	k1gNnSc1	dílo
Joži	Joža	k1gFnSc2	Joža
Uprky	Uprka	k1gFnSc2	Uprka
<g/>
,	,	kIx,	,
celostátní	celostátní	k2eAgFnSc1d1	celostátní
výstava	výstava	k1gFnSc1	výstava
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
lázeňství	lázeňství	k1gNnSc2	lázeňství
a	a	k8xC	a
rekreace	rekreace	k1gFnSc2	rekreace
Poznej	poznat	k5eAaPmRp2nS	poznat
svou	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
(	(	kIx(	(
<g/>
prezentace	prezentace	k1gFnSc1	prezentace
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sjezdy	sjezd	k1gInPc1	sjezd
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
aktivity	aktivita	k1gFnPc1	aktivita
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
pořádaly	pořádat	k5eAaImAgInP	pořádat
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
Výstavy	výstava	k1gFnSc2	výstava
Slovácka	Slovácko	k1gNnSc2	Slovácko
i	i	k8xC	i
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
spolky	spolek	k1gInPc4	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Záštitu	záštita	k1gFnSc4	záštita
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
převzal	převzít	k5eAaPmAgMnS	převzít
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
osobně	osobně	k6eAd1	osobně
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
s	s	k7c7	s
chotí	choť	k1gFnSc7	choť
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc4d1	vlastní
ulici	ulice	k1gFnSc4	ulice
mají	mít	k5eAaImIp3nP	mít
hradišťští	hradišťský	k2eAgMnPc1d1	hradišťský
Židé	Žid	k1gMnPc1	Žid
zmíněnu	zmíněn	k2eAgFnSc4d1	zmíněna
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1342	[number]	k4	1342
<g/>
,	,	kIx,	,
dřívější	dřívější	k2eAgFnSc1d1	dřívější
Židovská	židovská	k1gFnSc1	židovská
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Františkánská	františkánský	k2eAgFnSc1d1	Františkánská
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
byl	být	k5eAaImAgInS	být
zřízen	zřízen	k2eAgInSc1d1	zřízen
vlastní	vlastní	k2eAgInSc1d1	vlastní
hřbitov	hřbitov	k1gInSc1	hřbitov
za	za	k7c7	za
městskými	městský	k2eAgFnPc7d1	městská
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
vpravo	vpravo	k6eAd1	vpravo
za	za	k7c7	za
Staroměstskou	staroměstský	k2eAgFnSc7d1	Staroměstská
bránou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Vyhnáni	vyhnán	k2eAgMnPc1d1	vyhnán
byli	být	k5eAaImAgMnP	být
Židé	Žid	k1gMnPc1	Žid
z	z	k7c2	z
města	město	k1gNnSc2	město
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1514	[number]	k4	1514
a	a	k8xC	a
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Uherský	uherský	k2eAgInSc1d1	uherský
Ostroh	Ostroh	k1gInSc1	Ostroh
a	a	k8xC	a
Bzenec	Bzenec	k1gInSc1	Bzenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
jim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgInS	být
povolen	povolit	k5eAaPmNgInS	povolit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jarmarky	jarmarky	k?	jarmarky
za	za	k7c4	za
7	[number]	k4	7
krejcarů	krejcar	k1gInPc2	krejcar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
Židům	Žid	k1gMnPc3	Žid
zakázán	zakázán	k2eAgInSc4d1	zakázán
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
města	město	k1gNnSc2	město
jen	jen	k9	jen
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
a	a	k8xC	a
o	o	k7c6	o
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
mohli	moct	k5eAaImAgMnP	moct
do	do	k7c2	do
města	město	k1gNnSc2	město
vstoupit	vstoupit	k5eAaPmF	vstoupit
za	za	k7c4	za
poplatek	poplatek	k1gInSc4	poplatek
17	[number]	k4	17
krejcarů	krejcar	k1gInPc2	krejcar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
směli	smět	k5eAaImAgMnP	smět
Židé	Žid	k1gMnPc1	Žid
opět	opět	k6eAd1	opět
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
17	[number]	k4	17
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
měly	mít	k5eAaImAgFnP	mít
67	[number]	k4	67
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
dokonce	dokonce	k9	dokonce
veřejná	veřejný	k2eAgFnSc1d1	veřejná
košer	košer	k2eAgFnSc1d1	košer
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
povolena	povolit	k5eAaPmNgFnS	povolit
i	i	k9	i
spolková	spolkový	k2eAgFnSc1d1	spolková
činnost	činnost	k1gFnSc1	činnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
organizace	organizace	k1gFnSc1	organizace
Spolek	spolek	k1gInSc1	spolek
občanských	občanský	k2eAgFnPc2d1	občanská
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
se	se	k3xPyFc4	se
Spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
občanské	občanský	k2eAgFnPc4d1	občanská
záležitosti	záležitost	k1gFnPc4	záležitost
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
na	na	k7c4	na
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
obec	obec	k1gFnSc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
znovuobnoven	znovuobnoven	k2eAgInSc4d1	znovuobnoven
starý	starý	k2eAgInSc4d1	starý
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Derfli	Derfli	k1gFnSc6	Derfli
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Sady	sada	k1gFnPc1	sada
<g/>
,	,	kIx,	,
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byli	být	k5eAaImAgMnP	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
první	první	k4xOgMnPc1	první
židovští	židovský	k2eAgMnPc1d1	židovský
zástupci	zástupce	k1gMnPc1	zástupce
do	do	k7c2	do
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
synagoga	synagoga	k1gFnSc1	synagoga
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
za	za	k7c4	za
18	[number]	k4	18
tisíc	tisíc	k4xCgInSc4	tisíc
zlatých	zlatá	k1gFnPc2	zlatá
v	v	k7c6	v
pseudomaurském	pseudomaurský	k2eAgInSc6d1	pseudomaurský
slohu	sloh	k1gInSc6	sloh
s	s	k7c7	s
rozetovým	rozetový	k2eAgNnSc7d1	rozetové
oknem	okno	k1gNnSc7	okno
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byla	být	k5eAaImAgFnS	být
vysvěcena	vysvěcen	k2eAgFnSc1d1	vysvěcena
rabínem	rabín	k1gMnSc7	rabín
Mühmsamem	Mühmsam	k1gInSc7	Mühmsam
z	z	k7c2	z
bzenecké	bzenecký	k2eAgFnSc2d1	Bzenecká
diaspory	diaspora	k1gFnSc2	diaspora
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
zástupcem	zástupce	k1gMnSc7	zástupce
neevropské	evropský	k2eNgFnSc2d1	neevropská
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
kopule	kopule	k1gFnSc1	kopule
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
směly	smět	k5eAaImAgFnP	smět
židovské	židovský	k2eAgFnPc1d1	židovská
děti	dítě	k1gFnPc1	dítě
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
značný	značný	k2eAgInSc4d1	značný
rozvoj	rozvoj	k1gInSc4	rozvoj
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
nikdy	nikdy	k6eAd1	nikdy
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
vlastní	vlastní	k2eAgFnSc2d1	vlastní
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
vyhotoven	vyhotovit	k5eAaPmNgMnS	vyhotovit
nacisty	nacista	k1gMnPc7	nacista
seznam	seznam	k1gInSc4	seznam
325	[number]	k4	325
Židů	Žid	k1gMnPc2	Žid
žijících	žijící	k2eAgMnPc2d1	žijící
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
byli	být	k5eAaImAgMnP	být
odvezeni	odvézt	k5eAaPmNgMnP	odvézt
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
transportu	transport	k1gInSc6	transport
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
i	i	k8xC	i
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
transportérem	transportér	k1gInSc7	transportér
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Zachránilo	zachránit	k5eAaPmAgNnS	zachránit
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
náhrobků	náhrobek	k1gInPc2	náhrobek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
památníku	památník	k1gInSc2	památník
v	v	k7c6	v
Sadech	sad	k1gInPc6	sad
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Protinacistický	protinacistický	k2eAgInSc4d1	protinacistický
odboj	odboj	k1gInSc4	odboj
na	na	k7c6	na
Uherskohradišťsku	Uherskohradišťsko	k1gNnSc6	Uherskohradišťsko
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
se	se	k3xPyFc4	se
během	během	k7c2	během
války	válka	k1gFnSc2	válka
zapojovali	zapojovat	k5eAaImAgMnP	zapojovat
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
československého	československý	k2eAgInSc2d1	československý
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nacistické	nacistický	k2eAgFnSc6d1	nacistická
okupaci	okupace	k1gFnSc6	okupace
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1939	[number]	k4	1939
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
městě	město	k1gNnSc6	město
na	na	k7c4	na
popud	popud	k1gInSc4	popud
bývalého	bývalý	k2eAgMnSc2d1	bývalý
legionáře	legionář	k1gMnSc2	legionář
Františka	František	k1gMnSc2	František
Šlerky	Šlerka	k1gMnSc2	Šlerka
nejprve	nejprve	k6eAd1	nejprve
oblastní	oblastní	k2eAgFnSc1d1	oblastní
a	a	k8xC	a
poté	poté	k6eAd1	poté
krajské	krajský	k2eAgNnSc4d1	krajské
vojenské	vojenský	k2eAgNnSc4d1	vojenské
velitelství	velitelství	k1gNnSc4	velitelství
ilegální	ilegální	k2eAgFnSc1d1	ilegální
organizace	organizace	k1gFnSc1	organizace
Obrana	obrana	k1gFnSc1	obrana
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Buňka	buňka	k1gFnSc1	buňka
byla	být	k5eAaImAgFnS	být
zlikvidována	zlikvidovat	k5eAaPmNgFnS	zlikvidovat
gestapem	gestapo	k1gNnSc7	gestapo
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
vlnách	vlna	k1gFnPc6	vlna
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1940	[number]	k4	1940
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
činnost	činnost	k1gFnSc4	činnost
navázala	navázat	k5eAaPmAgFnS	navázat
skupina	skupina	k1gFnSc1	skupina
iniciovaná	iniciovaný	k2eAgFnSc1d1	iniciovaná
západním	západní	k2eAgInSc7d1	západní
výsadkem	výsadek	k1gInSc7	výsadek
z	z	k7c2	z
operace	operace	k1gFnSc2	operace
Carbon	Carbona	k1gFnPc2	Carbona
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnichovském	mnichovský	k2eAgNnSc6d1	mnichovské
obsazení	obsazení	k1gNnSc6	obsazení
pohraničí	pohraničí	k1gNnSc2	pohraničí
a	a	k8xC	a
poté	poté	k6eAd1	poté
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
druhé	druhý	k4xOgFnSc2	druhý
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
několik	několik	k4yIc4	několik
občanů	občan	k1gMnPc2	občan
z	z	k7c2	z
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
nacismu	nacismus	k1gInSc3	nacismus
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Dostali	dostat	k5eAaPmAgMnP	dostat
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
Polsko	Polsko	k1gNnSc4	Polsko
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
kapitulaci	kapitulace	k1gFnSc6	kapitulace
byli	být	k5eAaImAgMnP	být
evakuováni	evakuován	k2eAgMnPc1d1	evakuován
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
Le	Le	k1gFnSc2	Le
Havre	Havr	k1gInSc5	Havr
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
zapojili	zapojit	k5eAaPmAgMnP	zapojit
do	do	k7c2	do
činností	činnost	k1gFnPc2	činnost
československých	československý	k2eAgFnPc2d1	Československá
stíhacích	stíhací	k2eAgFnPc2d1	stíhací
perutí	peruť	k1gFnPc2	peruť
310	[number]	k4	310
a	a	k8xC	a
312	[number]	k4	312
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
později	pozdě	k6eAd2	pozdě
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
bombardovací	bombardovací	k2eAgFnPc1d1	bombardovací
perutě	peruť	k1gFnPc1	peruť
311	[number]	k4	311
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
odcházeli	odcházet	k5eAaImAgMnP	odcházet
také	také	k9	také
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
tzv.	tzv.	kA	tzv.
jižní	jižní	k2eAgFnSc7d1	jižní
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
do	do	k7c2	do
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
poté	poté	k6eAd1	poté
lodí	loď	k1gFnPc2	loď
z	z	k7c2	z
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
na	na	k7c4	na
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
britské	britský	k2eAgFnSc2d1	britská
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
či	či	k8xC	či
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1944	[number]	k4	1944
seskočila	seskočit	k5eAaPmAgFnS	seskočit
v	v	k7c6	v
lese	les	k1gInSc6	les
Rudníčku	Rudníček	k1gInSc2	Rudníček
u	u	k7c2	u
Ratíškovic	Ratíškovice	k1gFnPc2	Ratíškovice
skupina	skupina	k1gFnSc1	skupina
čtyř	čtyři	k4xCgMnPc2	čtyři
parašutistů	parašutista	k1gMnPc2	parašutista
<g/>
,	,	kIx,	,
účastníků	účastník	k1gMnPc2	účastník
operace	operace	k1gFnSc2	operace
Carbon	Carbon	k1gNnSc1	Carbon
iniciované	iniciovaný	k2eAgNnSc1d1	iniciované
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Velitelem	velitel	k1gMnSc7	velitel
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Bogataj	Bogataj	k1gMnSc1	Bogataj
z	z	k7c2	z
Uherského	uherský	k2eAgInSc2d1	uherský
Ostrohu	Ostroh	k1gInSc2	Ostroh
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
členy	člen	k1gInPc7	člen
Josef	Josef	k1gMnSc1	Josef
Vanc	Vanc	k1gFnSc1	Vanc
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kobzík	Kobzík	k1gMnSc1	Kobzík
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šperl	Šperl	k1gMnSc1	Šperl
(	(	kIx(	(
<g/>
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
v	v	k7c6	v
Kunovicích	Kunovice	k1gFnPc6	Kunovice
zformovala	zformovat	k5eAaPmAgFnS	zformovat
skupina	skupina	k1gFnSc1	skupina
Ovčáček	ovčáček	k1gMnSc1	ovčáček
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
dle	dle	k7c2	dle
svého	svůj	k3xOyFgMnSc2	svůj
zakladatele	zakladatel	k1gMnSc2	zakladatel
Františka	František	k1gMnSc2	František
Ovčáčka	ovčáček	k1gMnSc2	ovčáček
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centrum	k1gNnSc7	centrum
jejích	její	k3xOp3gFnPc2	její
odbojových	odbojový	k2eAgFnPc2d1	odbojová
aktivit	aktivita	k1gFnPc2	aktivita
byla	být	k5eAaImAgFnS	být
kunovická	kunovický	k2eAgFnSc1d1	kunovická
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Velké	velký	k2eAgNnSc1d1	velké
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
až	až	k9	až
1941	[number]	k4	1941
probíhalo	probíhat	k5eAaImAgNnS	probíhat
několik	několik	k4yIc4	několik
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
Slovácka	Slovácko	k1gNnSc2	Slovácko
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
leží	ležet	k5eAaImIp3nS	ležet
<g/>
,	,	kIx,	,
ke	k	k7c3	k
Slovensku	Slovensko	k1gNnSc3	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
Slovácka	Slovácko	k1gNnSc2	Slovácko
se	se	k3xPyFc4	se
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
době	doba	k1gFnSc6	doba
po	po	k7c6	po
Mnichovu	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
protějším	protější	k2eAgInSc6d1	protější
břehu	břeh	k1gInSc6	břeh
Moravy	Morava	k1gFnSc2	Morava
se	se	k3xPyFc4	se
jimi	on	k3xPp3gFnPc7	on
vážně	vážně	k6eAd1	vážně
zaobíral	zaobírat	k5eAaImAgMnS	zaobírat
pozdější	pozdní	k2eAgMnSc1d2	pozdější
slovenský	slovenský	k2eAgMnSc1d1	slovenský
premiér	premiér	k1gMnSc1	premiér
Vojtech	Vojta	k1gMnPc6	Vojta
Tuka	Tuka	k1gMnSc1	Tuka
(	(	kIx(	(
<g/>
prezident	prezident	k1gMnSc1	prezident
Jozef	Jozef	k1gMnSc1	Jozef
Tiso	Tisa	k1gFnSc5	Tisa
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
Moravanům	Moravan	k1gMnPc3	Moravan
nedůvěřivý	důvěřivý	k2eNgInSc1d1	nedůvěřivý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
propagandy	propaganda	k1gFnSc2	propaganda
Šaňo	Šaňo	k1gMnSc1	Šaňo
Mach	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Čatloš	Čatloš	k1gMnSc1	Čatloš
a	a	k8xC	a
zprvopočátku	zprvopočátku	k6eAd1	zprvopočátku
zejména	zejména	k9	zejména
vlivný	vlivný	k2eAgMnSc1d1	vlivný
"	"	kIx"	"
<g/>
ľuďák	ľuďák	k1gInSc1	ľuďák
<g/>
"	"	kIx"	"
Karol	Karol	k1gInSc1	Karol
Sidor	Sidora	k1gFnPc2	Sidora
<g/>
.	.	kIx.	.
</s>
<s>
Propaganda	propaganda	k1gFnSc1	propaganda
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
území	území	k1gNnSc4	území
Slovácka	Slovácko	k1gNnSc2	Slovácko
za	za	k7c4	za
oblast	oblast	k1gFnSc4	oblast
obývanou	obývaný	k2eAgFnSc7d1	obývaná
zahraničními	zahraniční	k2eAgInPc7d1	zahraniční
Slováky	Slováky	k1gInPc7	Slováky
a	a	k8xC	a
argumentace	argumentace	k1gFnSc1	argumentace
sahala	sahat	k5eAaImAgFnS	sahat
až	až	k9	až
k	k	k7c3	k
Velkomoravské	velkomoravský	k2eAgFnSc3d1	Velkomoravská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
prvnímu	první	k4xOgMnSc3	první
slovenskému	slovenský	k2eAgInSc3d1	slovenský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Veľké	Veľký	k2eAgNnSc4d1	Veľké
Slovensko	Slovensko	k1gNnSc4	Slovensko
byla	být	k5eAaImAgFnS	být
politická	politický	k2eAgFnSc1d1	politická
koncepce	koncepce	k1gFnSc1	koncepce
připojení	připojení	k1gNnSc2	připojení
území	území	k1gNnSc2	území
Moravského	moravský	k2eAgNnSc2d1	Moravské
Slovácka	Slovácko	k1gNnSc2	Slovácko
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
dalšího	další	k2eAgNnSc2d1	další
území	území	k1gNnSc2	území
východní	východní	k2eAgFnSc2d1	východní
Moravy	Morava	k1gFnSc2	Morava
ke	k	k7c3	k
Slovenskému	slovenský	k2eAgInSc3d1	slovenský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Rozkazem	rozkaz	k1gInSc7	rozkaz
protektora	protektor	k1gMnSc2	protektor
K.	K.	kA	K.
H.	H.	kA	H.
Franka	Frank	k1gMnSc2	Frank
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
se	se	k3xPyFc4	se
okresy	okres	k1gInPc1	okres
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Litovel	Litovel	k1gFnSc1	Litovel
a	a	k8xC	a
Prostějov	Prostějov	k1gInSc1	Prostějov
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
bojového	bojový	k2eAgNnSc2d1	bojové
pásma	pásmo	k1gNnSc2	pásmo
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
Mitte	Mitt	k1gMnSc5	Mitt
a	a	k8xC	a
tedy	tedy	k9	tedy
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
vojenských	vojenský	k2eAgMnPc2d1	vojenský
velitelů	velitel	k1gMnPc2	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozkaz	rozkaz	k1gInSc1	rozkaz
zároveň	zároveň	k6eAd1	zároveň
umožnil	umožnit	k5eAaPmAgInS	umožnit
daleko	daleko	k6eAd1	daleko
ostřejší	ostrý	k2eAgInPc4d2	ostřejší
zásahy	zásah	k1gInPc4	zásah
jak	jak	k8xC	jak
proti	proti	k7c3	proti
partyzánům	partyzán	k1gMnPc3	partyzán
tak	tak	k8xC	tak
proti	proti	k7c3	proti
českému	český	k2eAgInSc3d1	český
odboji	odboj	k1gInSc3	odboj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vypálení	vypálení	k1gNnSc3	vypálení
vsi	ves	k1gFnSc2	ves
Javoříčko	Javoříčko	k1gNnSc1	Javoříčko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
a	a	k8xC	a
již	již	k6eAd1	již
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
řeky	řeka	k1gFnPc4	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
kroky	krok	k1gInPc4	krok
na	na	k7c6	na
moravské	moravský	k2eAgFnSc6d1	Moravská
půdě	půda	k1gFnSc6	půda
byly	být	k5eAaImAgInP	být
provázeny	provázen	k2eAgInPc1d1	provázen
urputnými	urputný	k2eAgInPc7d1	urputný
boji	boj	k1gInPc7	boj
a	a	k8xC	a
značnými	značný	k2eAgFnPc7d1	značná
lidskými	lidský	k2eAgFnPc7d1	lidská
ztrátami	ztráta	k1gFnPc7	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vojska	vojsko	k1gNnSc2	vojsko
přechod	přechod	k1gInSc4	přechod
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Nejúpornější	úporný	k2eAgInPc1d3	nejúpornější
boje	boj	k1gInPc1	boj
byly	být	k5eAaImAgInP	být
svedeny	svést	k5eAaPmNgInP	svést
o	o	k7c4	o
vstupní	vstupní	k2eAgFnSc4d1	vstupní
bránu	brána	k1gFnSc4	brána
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
o	o	k7c4	o
ves	ves	k1gFnSc4	ves
Lanžhot	Lanžhota	k1gFnPc2	Lanžhota
a	a	k8xC	a
město	město	k1gNnSc1	město
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
Moravu	Morava	k1gFnSc4	Morava
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
jednotkami	jednotka	k1gFnPc7	jednotka
2	[number]	k4	2
<g/>
.	.	kIx.	.
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
frontu	front	k1gInSc2	front
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vedl	vést	k5eAaImAgInS	vést
maršál	maršál	k1gMnSc1	maršál
R.	R.	kA	R.
J.	J.	kA	J.
Malinovskij	Malinovskij	k1gMnSc1	Malinovskij
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
operací	operace	k1gFnSc7	operace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
směru	směr	k1gInSc2	směr
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
frontu	fronta	k1gFnSc4	fronta
předposlední	předposlední	k2eAgFnSc3d1	předposlední
operaci	operace	k1gFnSc3	operace
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
byla	být	k5eAaImAgFnS	být
operace	operace	k1gFnSc1	operace
bratislavsko-brněnská	bratislavskorněnský	k2eAgFnSc1d1	bratislavsko-brněnský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
operace	operace	k1gFnSc2	operace
bylo	být	k5eAaImAgNnS	být
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
osvobozena	osvobozen	k2eAgFnSc1d1	osvobozena
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Německým	německý	k2eAgNnPc3d1	německé
vojskům	vojsko	k1gNnPc3	vojsko
hrozilo	hrozit	k5eAaImAgNnS	hrozit
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Moravě	Morava	k1gFnSc6	Morava
obklíčení	obklíčení	k1gNnSc2	obklíčení
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
přijata	přijat	k2eAgNnPc4d1	přijato
opatření	opatření	k1gNnPc4	opatření
ARLZ	ARLZ	kA	ARLZ
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
politika	politika	k1gFnSc1	politika
spálené	spálený	k2eAgFnSc2d1	spálená
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
ARLZ	ARLZ	kA	ARLZ
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
:	:	kIx,	:
A	a	k9	a
-	-	kIx~	-
Auflockerung	Auflockerung	k1gInSc1	Auflockerung
=	=	kIx~	=
uvolnění	uvolnění	k1gNnSc1	uvolnění
<g/>
;	;	kIx,	;
R	R	kA	R
-	-	kIx~	-
Räumung	Räumung	k1gMnSc1	Räumung
=	=	kIx~	=
vyklizení	vyklizení	k1gNnSc1	vyklizení
<g/>
;	;	kIx,	;
L	L	kA	L
-	-	kIx~	-
Lähmung	Lähmung	k1gInSc1	Lähmung
=	=	kIx~	=
ochromení	ochromení	k1gNnSc2	ochromení
<g/>
;	;	kIx,	;
Z	z	k7c2	z
-	-	kIx~	-
Zerstörung	Zerstörung	k1gInSc1	Zerstörung
=	=	kIx~	=
zničení	zničení	k1gNnSc1	zničení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
linie	linie	k1gFnSc2	linie
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
-	-	kIx~	-
Valašské	valašský	k2eAgInPc1d1	valašský
Klobouky	Klobouky	k1gInPc1	Klobouky
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
tratě	trať	k1gFnSc2	trať
Trenčianska	Trenčiansko	k1gNnSc2	Trenčiansko
Teplá	Teplá	k1gFnSc1	Teplá
-	-	kIx~	-
Valašské	valašský	k2eAgInPc1d1	valašský
Klobouky	Klobouky	k1gInPc1	Klobouky
<g/>
,	,	kIx,	,
tratě	trať	k1gFnPc1	trať
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
-	-	kIx~	-
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
a	a	k8xC	a
Zlín	Zlín	k1gInSc1	Zlín
-	-	kIx~	-
Vizovice	Vizovice	k1gFnPc1	Vizovice
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
destrukci	destrukce	k1gFnSc4	destrukce
prováděly	provádět	k5eAaImAgFnP	provádět
jednotky	jednotka	k1gFnPc1	jednotka
armád	armáda	k1gFnPc2	armáda
"	"	kIx"	"
<g/>
Mitte	Mitt	k1gMnSc5	Mitt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
disponovaly	disponovat	k5eAaBmAgInP	disponovat
speciálními	speciální	k2eAgInPc7d1	speciální
železničními	železniční	k2eAgInPc7d1	železniční
vlaky	vlak	k1gInPc7	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc1	Brod
-	-	kIx~	-
Bojkovice	Bojkovice	k1gFnPc1	Bojkovice
-	-	kIx~	-
Valašské	valašský	k2eAgInPc1d1	valašský
Klobouky	Klobouky	k1gInPc1	Klobouky
-	-	kIx~	-
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zničen	zničen	k2eAgMnSc1d1	zničen
byl	být	k5eAaImAgInS	být
také	také	k9	také
vodní	vodní	k2eAgInSc1d1	vodní
jez	jez	k1gInSc1	jez
ve	v	k7c6	v
Spytihněvi	Spytihněev	k1gFnSc6	Spytihněev
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mohl	moct	k5eAaImAgMnS	moct
sloužit	sloužit	k5eAaImF	sloužit
sovětským	sovětský	k2eAgFnPc3d1	sovětská
jednotkám	jednotka	k1gFnPc3	jednotka
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
zničen	zničen	k2eAgInSc4d1	zničen
most	most	k1gInSc4	most
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
-	-	kIx~	-
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Bojů	boj	k1gInPc2	boj
4	[number]	k4	4
<g/>
.	.	kIx.	.
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
frontu	front	k1gInSc2	front
na	na	k7c6	na
ostravsko-moravském	ostravskooravský	k2eAgInSc6d1	ostravsko-moravský
směru	směr	k1gInSc6	směr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vedl	vést	k5eAaImAgMnS	vést
maršál	maršál	k1gMnSc1	maršál
A.	A.	kA	A.
I.	I.	kA	I.
Jeremenko	Jeremenka	k1gFnSc5	Jeremenka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
tankisté	tankista	k1gMnPc1	tankista
a	a	k8xC	a
letci	letec	k1gMnSc3	letec
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Rudou	ruda	k1gFnSc7	ruda
a	a	k8xC	a
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Podolí	Podolí	k1gNnSc1	Podolí
bylo	být	k5eAaImAgNnS	být
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
již	již	k9	již
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Rumuny	Rumun	k1gMnPc7	Rumun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
válečný	válečný	k2eAgInSc1d1	válečný
památník	památník	k1gInSc1	památník
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
šohaje	šohaj	k1gMnSc2	šohaj
ve	v	k7c6	v
slováckém	slovácký	k2eAgInSc6d1	slovácký
kroji	kroj	k1gInSc6	kroj
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Jan	Jan	k1gMnSc1	Jan
Habarta	Habarta	k1gMnSc1	Habarta
starší	starší	k1gMnSc1	starší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
a	a	k8xC	a
nesoucí	nesoucí	k2eAgFnSc3d1	nesoucí
informaci	informace	k1gFnSc3	informace
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
padlých	padlý	k2eAgMnPc2d1	padlý
Rudoarmějců	rudoarmějec	k1gMnPc2	rudoarmějec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byly	být	k5eAaImAgFnP	být
přidány	přidán	k2eAgFnPc1d1	přidána
pamětní	pamětní	k2eAgFnPc1d1	pamětní
desky	deska	k1gFnPc1	deska
také	také	k9	také
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
místních	místní	k2eAgFnPc2d1	místní
obětí	oběť	k1gFnPc2	oběť
nacismu	nacismus	k1gInSc2	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Zeleném	zelený	k2eAgNnSc6d1	zelené
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kasáren	kasárny	k1gFnPc2	kasárny
odhalen	odhalit	k5eAaPmNgInS	odhalit
nový	nový	k2eAgInSc1d1	nový
památník	památník	k1gInSc1	památník
příslušníkům	příslušník	k1gMnPc3	příslušník
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
Obrana	obrana	k1gFnSc1	obrana
národa	národ	k1gInSc2	národ
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
vojenské	vojenský	k2eAgFnSc2d1	vojenská
přilby	přilba	k1gFnSc2	přilba
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ho	on	k3xPp3gNnSc4	on
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Tománek	Tománek	k1gMnSc1	Tománek
a	a	k8xC	a
občané	občan	k1gMnPc1	občan
uctívají	uctívat	k5eAaImIp3nP	uctívat
památku	památka	k1gFnSc4	památka
odbojářů	odbojář	k1gMnPc2	odbojář
jeho	jeho	k3xOp3gNnSc7	jeho
obcházením	obcházení	k1gNnSc7	obcházení
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
pamětního	pamětní	k2eAgInSc2d1	pamětní
nápisu	nápis	k1gInSc2	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obnově	obnova	k1gFnSc6	obnova
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Benešových	Benešových	k2eAgInPc2d1	Benešových
dekretů	dekret	k1gInPc2	dekret
ke	k	k7c3	k
konfiskaci	konfiskace	k1gFnSc3	konfiskace
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
na	na	k7c6	na
Uherskohradišťsku	Uherskohradišťsko	k1gNnSc6	Uherskohradišťsko
převážně	převážně	k6eAd1	převážně
rodu	rod	k1gInSc3	rod
Logothetti	Logothetť	k1gFnSc2	Logothetť
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Bílovicích	Bílovice	k1gInPc6	Bílovice
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Berchtoldů	Berchtold	k1gMnPc2	Berchtold
z	z	k7c2	z
buchlovického	buchlovický	k2eAgNnSc2d1	Buchlovické
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
majetku	majetek	k1gInSc2	majetek
zabaveného	zabavený	k2eAgInSc2d1	zabavený
v	v	k7c6	v
Bílovicích	Bílovice	k1gInPc6	Bílovice
byly	být	k5eAaImAgInP	být
i	i	k9	i
obrazy	obraz	k1gInPc1	obraz
Josefa	Josef	k1gMnSc2	Josef
Mánesa	Mánes	k1gMnSc2	Mánes
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
pražské	pražský	k2eAgFnSc2d1	Pražská
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
část	část	k1gFnSc4	část
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
MUDr.	MUDr.	kA	MUDr.
R.	R.	kA	R.
Tománek	Tománek	k1gMnSc1	Tománek
z	z	k7c2	z
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
daroval	darovat	k5eAaPmAgMnS	darovat
Slováckému	slovácký	k2eAgNnSc3d1	Slovácké
muzeu	muzeum	k1gNnSc3	muzeum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
Veruny	Veruna	k1gFnSc2	Veruna
Čudové	Čud	k1gMnPc1	Čud
v	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
oblečení	oblečení	k1gNnSc6	oblečení
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gMnSc4	jeho
syn	syn	k1gMnSc1	syn
údajně	údajně	k6eAd1	údajně
vzal	vzít	k5eAaPmAgMnS	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
také	také	k9	také
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazit	k5eAaPmNgFnS	vyobrazit
ve	v	k7c6	v
slováckém	slovácký	k2eAgInSc6d1	slovácký
kroji	kroj	k1gInSc6	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
založeno	založit	k5eAaPmNgNnS	založit
Slovácké	slovácký	k2eAgNnSc1d1	Slovácké
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
sezónu	sezóna	k1gFnSc4	sezóna
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
hrou	hra	k1gFnSc7	hra
Gabriely	Gabriela	k1gFnSc2	Gabriela
Preissové	Preissová	k1gFnSc2	Preissová
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
58	[number]	k4	58
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
působila	působit	k5eAaImAgFnS	působit
Jana	Jana	k1gFnSc1	Jana
Werichová	Werichová	k1gFnSc1	Werichová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
otevřením	otevření	k1gNnSc7	otevření
nového	nový	k2eAgNnSc2d1	nové
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Gottwaldově	Gottwaldov	k1gInSc6	Gottwaldov
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
Slovácké	slovácký	k2eAgNnSc1d1	Slovácké
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
stalo	stát	k5eAaPmAgNnS	stát
nejmenší	malý	k2eAgFnSc7d3	nejmenší
scénou	scéna	k1gFnSc7	scéna
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
působilo	působit	k5eAaImAgNnS	působit
jen	jen	k9	jen
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hradišťská	hradišťský	k2eAgFnSc1d1	Hradišťská
věznice	věznice	k1gFnSc1	věznice
<g/>
,	,	kIx,	,
za	za	k7c2	za
války	válka	k1gFnSc2	válka
využívaná	využívaný	k2eAgFnSc1d1	využívaná
gestapem	gestapo	k1gNnSc7	gestapo
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
místem	místo	k1gNnSc7	místo
mučení	mučení	k1gNnSc2	mučení
politické	politický	k2eAgFnSc2d1	politická
opozice	opozice	k1gFnSc2	opozice
i	i	k8xC	i
lidí	člověk	k1gMnPc2	člověk
zcela	zcela	k6eAd1	zcela
nevinných	vinný	k2eNgMnPc2d1	nevinný
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Alois	Alois	k1gMnSc1	Alois
Grebeníček	Grebeníčko	k1gNnPc2	Grebeníčko
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Višinka	Višinka	k1gFnSc1	Višinka
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Holub	Holub	k1gMnSc1	Holub
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Bláha	Bláha	k1gMnSc1	Bláha
<g/>
,	,	kIx,	,
proslula	proslout	k5eAaPmAgFnS	proslout
svoji	svůj	k3xOyFgFnSc4	svůj
krutostí	krutost	k1gFnPc2	krutost
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Uherskohradišťska	Uherskohradišťsko	k1gNnSc2	Uherskohradišťsko
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
o	o	k7c4	o
nekvalifikované	kvalifikovaný	k2eNgFnPc4d1	nekvalifikovaná
osoby	osoba	k1gFnPc4	osoba
s	s	k7c7	s
nízkým	nízký	k2eAgNnSc7d1	nízké
vzděláním	vzdělání	k1gNnSc7	vzdělání
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
uplatnění	uplatnění	k1gNnSc4	uplatnění
zde	zde	k6eAd1	zde
nalezli	nalézt	k5eAaBmAgMnP	nalézt
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
vyškolení	vyškolený	k2eAgMnPc1d1	vyškolený
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
u	u	k7c2	u
NKVD	NKVD	kA	NKVD
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
byl	být	k5eAaImAgMnS	být
dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1993	[number]	k4	1993
vedle	vedle	k7c2	vedle
bývalé	bývalý	k2eAgFnSc2d1	bývalá
věznice	věznice	k1gFnSc2	věznice
na	na	k7c6	na
Palackého	Palackého	k2eAgNnSc6d1	Palackého
náměstí	náměstí	k1gNnSc6	náměstí
odhalen	odhalen	k2eAgInSc4d1	odhalen
pomník	pomník	k1gInSc4	pomník
jeho	jeho	k3xOp3gFnPc3	jeho
obětem	oběť	k1gFnPc3	oběť
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
hlavním	hlavní	k2eAgMnSc7d1	hlavní
iniciátorem	iniciátor	k1gMnSc7	iniciátor
byl	být	k5eAaImAgMnS	být
předseda	předseda	k1gMnSc1	předseda
uherskobrodské	uherskobrodský	k2eAgFnSc2d1	Uherskobrodská
pobočky	pobočka	k1gFnSc2	pobočka
Konfederace	konfederace	k1gFnSc2	konfederace
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
Miroslav	Miroslav	k1gMnSc1	Miroslav
Minks	Minks	k1gInSc1	Minks
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
snahám	snaha	k1gFnPc3	snaha
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
názvu	název	k1gInSc2	název
na	na	k7c4	na
Slovácké	slovácký	k2eAgNnSc4d1	Slovácké
Hradiště	Hradiště	k1gNnSc4	Hradiště
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
veřejností	veřejnost	k1gFnSc7	veřejnost
zamítnuto	zamítnout	k5eAaPmNgNnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
pořádal	pořádat	k5eAaImAgMnS	pořádat
ve	v	k7c6	v
Slováckém	slovácký	k2eAgNnSc6d1	Slovácké
muzeu	muzeum	k1gNnSc6	muzeum
výstavu	výstav	k1gInSc2	výstav
ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
vydání	vydání	k1gNnSc2	vydání
Kytice	kytice	k1gFnSc2	kytice
Bedřich	Bedřich	k1gMnSc1	Bedřich
Beneš	Beneš	k1gMnSc1	Beneš
Buchlovan	Buchlovan	k1gMnSc1	Buchlovan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
dorazil	dorazit	k5eAaPmAgMnS	dorazit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
Strážnickými	strážnický	k2eAgFnPc7d1	Strážnická
slavnostmi	slavnost	k1gFnPc7	slavnost
na	na	k7c4	na
neoficiální	oficiální	k2eNgFnSc4d1	neoficiální
návštěvu	návštěva	k1gFnSc4	návštěva
města	město	k1gNnSc2	město
prezident	prezident	k1gMnSc1	prezident
Antonín	Antonín	k1gMnSc1	Antonín
Zápotocký	Zápotocký	k1gMnSc1	Zápotocký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
oslavy	oslava	k1gFnPc1	oslava
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
výročí	výročí	k1gNnSc2	výročí
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
zbyl	zbýt	k5eAaPmAgMnS	zbýt
na	na	k7c6	na
dlažbě	dlažba	k1gFnSc6	dlažba
u	u	k7c2	u
knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
Portál	portál	k1gInSc1	portál
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
nápis	nápis	k1gInSc1	nápis
1257	[number]	k4	1257
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
až	až	k9	až
1960	[number]	k4	1960
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
i	i	k9	i
obce	obec	k1gFnSc2	obec
Kunovice	Kunovice	k1gFnPc1	Kunovice
a	a	k8xC	a
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mojmír	Mojmír	k1gMnSc1	Mojmír
I	i	k9	i
<g/>
:	:	kIx,	:
první	první	k4xOgNnSc4	první
socialistické	socialistický	k2eAgNnSc4d1	socialistické
sídliště	sídliště	k1gNnSc4	sídliště
podle	podle	k7c2	podle
moskevského	moskevský	k2eAgInSc2d1	moskevský
vzoru	vzor	k1gInSc2	vzor
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Mojmír	Mojmír	k1gMnSc1	Mojmír
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnPc1	ulice
nesou	nést	k5eAaImIp3nP	nést
jména	jméno	k1gNnPc1	jméno
slavných	slavný	k2eAgMnPc2d1	slavný
husitů	husita	k1gMnPc2	husita
<g/>
:	:	kIx,	:
Prokop	Prokop	k1gMnSc1	Prokop
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Chelčický	Chelčický	k2eAgMnSc1d1	Chelčický
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
architekturu	architektura	k1gFnSc4	architektura
s	s	k7c7	s
typickými	typický	k2eAgInPc7d1	typický
znaky	znak	k1gInPc7	znak
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
-	-	kIx~	-
sorely	sorela	k1gFnSc2	sorela
<g/>
,	,	kIx,	,
např.	např.	kA	např.
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgNnPc4d1	malé
okna	okno	k1gNnPc4	okno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
využijí	využít	k5eAaPmIp3nP	využít
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
UH	uh	k0	uh
nemají	mít	k5eNaImIp3nP	mít
opodstatnění	opodstatnění	k1gNnSc4	opodstatnění
(	(	kIx(	(
<g/>
zimní	zimní	k2eAgFnSc1d1	zimní
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
:	:	kIx,	:
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
<g/>
:	:	kIx,	:
+	+	kIx~	+
4,50	[number]	k4	4,50
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
:	:	kIx,	:
+	+	kIx~	+
8	[number]	k4	8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anachronismy	anachronismus	k1gInPc1	anachronismus
<g/>
:	:	kIx,	:
sedlová	sedlový	k2eAgFnSc1d1	sedlová
stodolová	stodolový	k2eAgFnSc1d1	Stodolová
střecha	střecha	k1gFnSc1	střecha
<g/>
,	,	kIx,	,
dekorativní	dekorativní	k2eAgInPc4d1	dekorativní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Mojmír	Mojmír	k1gMnSc1	Mojmír
I	i	k8xC	i
zachraňuje	zachraňovat	k5eAaImIp3nS	zachraňovat
jeho	jeho	k3xOp3gFnSc1	jeho
urbanistická	urbanistický	k2eAgFnSc1d1	urbanistická
skladba	skladba	k1gFnSc1	skladba
navržená	navržený	k2eAgFnSc1d1	navržená
netypicky	typicky	k6eNd1	typicky
do	do	k7c2	do
půlkruhu	půlkruh	k1gInSc2	půlkruh
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Kasárna	kasárna	k1gNnPc1	kasárna
československé	československý	k2eAgFnSc2d1	Československá
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zrušena	zrušit	k5eAaPmNgNnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
fasáda	fasáda	k1gFnSc1	fasáda
spojující	spojující	k2eAgMnPc4d1	spojující
lidové	lidový	k2eAgMnPc4d1	lidový
motivy	motiv	k1gInPc4	motiv
a	a	k8xC	a
válečnou	válečný	k2eAgFnSc4d1	válečná
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
Bytové	bytový	k2eAgInPc1d1	bytový
domy	dům	k1gInPc1	dům
na	na	k7c6	na
Velehradské	velehradský	k2eAgFnSc6d1	Velehradská
třídě	třída	k1gFnSc6	třída
<g/>
.	.	kIx.	.
</s>
<s>
Učiliště	učiliště	k1gNnSc1	učiliště
podniku	podnik	k1gInSc2	podnik
MESIT	MESIT	kA	MESIT
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
"	"	kIx"	"
<g/>
Za	za	k7c2	za
Alejí	alej	k1gFnPc2	alej
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
symetrie	symetrie	k1gFnSc1	symetrie
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Neofunkcionalistické	Neofunkcionalistický	k2eAgNnSc4d1	Neofunkcionalistický
kino	kino	k1gNnSc4	kino
Hvězda	hvězda	k1gFnSc1	hvězda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
(	(	kIx(	(
<g/>
architekt	architekt	k1gMnSc1	architekt
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Michal	Michal	k1gMnSc1	Michal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
typizovanou	typizovaný	k2eAgFnSc4d1	typizovaná
stavbu	stavba	k1gFnSc4	stavba
<g/>
;	;	kIx,	;
podobná	podobný	k2eAgFnSc1d1	podobná
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
(	(	kIx(	(
<g/>
kino	kino	k1gNnSc1	kino
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
(	(	kIx(	(
<g/>
kino	kino	k1gNnSc1	kino
Metro	metro	k1gNnSc1	metro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navrženo	navržen	k2eAgNnSc1d1	navrženo
současně	současně	k6eAd1	současně
jako	jako	k8xC	jako
protiatomový	protiatomový	k2eAgInSc1d1	protiatomový
kryt	kryt	k1gInSc1	kryt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
okupaci	okupace	k1gFnSc3	okupace
země	zem	k1gFnSc2	zem
armádami	armáda	k1gFnPc7	armáda
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
zahynul	zahynout	k5eAaPmAgMnS	zahynout
jeden	jeden	k4xCgMnSc1	jeden
sovětský	sovětský	k2eAgMnSc1d1	sovětský
voják	voják	k1gMnSc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
města	město	k1gNnSc2	město
Zlína	Zlín	k1gInSc2	Zlín
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Gottwaldova	Gottwaldův	k2eAgFnSc1d1	Gottwaldova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
spotřebním	spotřební	k2eAgInSc6d1	spotřební
průmyslu	průmysl	k1gInSc6	průmysl
-	-	kIx~	-
Svit	svit	k1gInSc1	svit
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
rozvoji	rozvoj	k1gInSc3	rozvoj
strojírenství	strojírenství	k1gNnSc2	strojírenství
(	(	kIx(	(
<g/>
ZPS	ZPS	kA	ZPS
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc3	rozvoj
gumárenského	gumárenský	k2eAgInSc2d1	gumárenský
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
<g/>
)	)	kIx)	)
-	-	kIx~	-
Barum	barum	k1gInSc1	barum
RŘ	RŘ	kA	RŘ
n.	n.	k?	n.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Continental	Continental	k1gMnSc1	Continental
<g/>
,	,	kIx,	,
také	také	k9	také
i	i	k9	i
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
Pozemních	pozemní	k2eAgFnPc2d1	pozemní
staveb	stavba	k1gFnPc2	stavba
-	-	kIx~	-
PSG	PSG	kA	PSG
a	a	k8xC	a
Průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
staveb	stavba	k1gFnPc2	stavba
-	-	kIx~	-
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
zrušen	zrušit	k5eAaPmNgInS	zrušit
bez	bez	k7c2	bez
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
;	;	kIx,	;
majetek	majetek	k1gInSc1	majetek
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c4	mezi
nástupnické	nástupnický	k2eAgFnPc4d1	nástupnická
firmy	firma	k1gFnPc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
doslalo	doslat	k5eAaPmAgNnS	doslat
město	město	k1gNnSc1	město
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
novou	nový	k2eAgFnSc4d1	nová
perspektivu	perspektiva	k1gFnSc4	perspektiva
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
centrem	centrum	k1gNnSc7	centrum
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1971	[number]	k4	1971
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
spojeno	spojit	k5eAaPmNgNnS	spojit
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
s	s	k7c7	s
obcemi	obec	k1gFnPc7	obec
Kunovice	Kunovice	k1gFnPc4	Kunovice
a	a	k8xC	a
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
plán	plán	k1gInSc1	plán
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
centra	centrum	k1gNnSc2	centrum
kraje	kraj	k1gInSc2	kraj
s	s	k7c7	s
aglomerací	aglomerace	k1gFnSc7	aglomerace
pro	pro	k7c4	pro
70	[number]	k4	70
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
textilního	textilní	k2eAgInSc2d1	textilní
<g/>
,	,	kIx,	,
leteckého	letecký	k2eAgInSc2d1	letecký
a	a	k8xC	a
konzervárenského	konzervárenský	k2eAgInSc2d1	konzervárenský
<g/>
.	.	kIx.	.
</s>
<s>
Plánovalo	plánovat	k5eAaImAgNnS	plánovat
se	se	k3xPyFc4	se
zavedení	zavedení	k1gNnSc1	zavedení
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Vybudování	vybudování	k1gNnSc1	vybudování
unifikovaných	unifikovaný	k2eAgNnPc2d1	unifikované
sídlišť	sídliště	k1gNnPc2	sídliště
Mojmír	Mojmír	k1gMnSc1	Mojmír
II	II	kA	II
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
sídliště	sídliště	k1gNnSc1	sídliště
Východ	východ	k1gInSc1	východ
<g/>
.	.	kIx.	.
</s>
<s>
Projekty	projekt	k1gInPc1	projekt
pocházely	pocházet	k5eAaImAgInP	pocházet
z	z	k7c2	z
anonymních	anonymní	k2eAgFnPc2d1	anonymní
stavebních	stavební	k2eAgFnPc2d1	stavební
kanceláří	kancelář	k1gFnPc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Východ	východ	k1gInSc1	východ
z	z	k7c2	z
urbanistického	urbanistický	k2eAgNnSc2d1	Urbanistické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
nevhodně	vhodně	k6eNd1	vhodně
umístěno	umístit	k5eAaPmNgNnS	umístit
na	na	k7c6	na
větrném	větrný	k2eAgInSc6d1	větrný
kopci	kopec	k1gInSc6	kopec
<g/>
,	,	kIx,	,
v	v	k7c6	v
jinak	jinak	k6eAd1	jinak
rovinatém	rovinatý	k2eAgInSc6d1	rovinatý
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
odkudkoliv	odkudkoliv	k6eAd1	odkudkoliv
patrný	patrný	k2eAgInSc1d1	patrný
jeho	jeho	k3xOp3gInSc1	jeho
deficit	deficit	k1gInSc1	deficit
v	v	k7c6	v
požadavcích	požadavek	k1gInPc6	požadavek
na	na	k7c4	na
pohledovost	pohledovost	k1gFnSc4	pohledovost
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jeho	jeho	k3xOp3gFnPc1	jeho
stavby	stavba	k1gFnPc1	stavba
stojí	stát	k5eAaImIp3nP	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
proudění	proudění	k1gNnSc2	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
snad	snad	k9	snad
jako	jako	k8xC	jako
ironie	ironie	k1gFnSc1	ironie
stavitelů	stavitel	k1gMnPc2	stavitel
nese	nést	k5eAaImIp3nS	nést
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
ulic	ulice	k1gFnPc2	ulice
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Větrná	větrný	k2eAgFnSc1d1	větrná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Socialistické	socialistický	k2eAgFnPc1d1	socialistická
školy	škola	k1gFnPc1	škola
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
nadstandardně	nadstandardně	k6eAd1	nadstandardně
vybaveny	vybaven	k2eAgInPc1d1	vybaven
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
ZŠ	ZŠ	kA	ZŠ
dnes	dnes	k6eAd1	dnes
škola	škola	k1gFnSc1	škola
"	"	kIx"	"
<g/>
Za	za	k7c7	za
alejí	alej	k1gFnSc7	alej
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
např.	např.	kA	např.
vybavena	vybavit	k5eAaPmNgFnS	vybavit
vlastním	vlastní	k2eAgInSc7d1	vlastní
bazénem	bazén	k1gInSc7	bazén
a	a	k8xC	a
jídelnou	jídelna	k1gFnSc7	jídelna
budovanou	budovaný	k2eAgFnSc7d1	budovaná
podle	podle	k7c2	podle
měřítek	měřítko	k1gNnPc2	měřítko
socialistické	socialistický	k2eAgFnSc2d1	socialistická
restaurace	restaurace	k1gFnSc2	restaurace
II	II	kA	II
<g/>
.	.	kIx.	.
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
škola	škola	k1gFnSc1	škola
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
atd.	atd.	kA	atd.
Městu	město	k1gNnSc3	město
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
realizovat	realizovat	k5eAaBmF	realizovat
novou	nový	k2eAgFnSc4d1	nová
budovu	budova	k1gFnSc4	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
realizace	realizace	k1gFnSc1	realizace
veřejných	veřejný	k2eAgFnPc2d1	veřejná
staveb	stavba	k1gFnPc2	stavba
<g/>
:	:	kIx,	:
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
realizoval	realizovat	k5eAaBmAgInS	realizovat
plavecký	plavecký	k2eAgInSc1d1	plavecký
a	a	k8xC	a
zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc1d1	nové
autobusové	autobusový	k2eAgNnSc1d1	autobusové
nádraží	nádraží	k1gNnSc1	nádraží
a	a	k8xC	a
divácká	divácký	k2eAgFnSc1d1	divácká
tribuna	tribuna	k1gFnSc1	tribuna
týmu	tým	k1gInSc2	tým
FC	FC	kA	FC
Slovácká	slovácký	k2eAgFnSc1d1	Slovácká
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
.	.	kIx.	.
</s>
<s>
Nezrealizovala	zrealizovat	k5eNaPmAgFnS	zrealizovat
se	se	k3xPyFc4	se
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
ani	ani	k8xC	ani
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
obchvat	obchvat	k1gInSc1	obchvat
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
však	však	k9	však
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
kontroverzních	kontroverzní	k2eAgFnPc2d1	kontroverzní
staveb	stavba	k1gFnPc2	stavba
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
neekonomický	ekonomický	k2eNgInSc1d1	neekonomický
a	a	k8xC	a
plastový	plastový	k2eAgInSc1d1	plastový
"	"	kIx"	"
<g/>
Klub	klub	k1gInSc1	klub
pracujících	pracující	k1gMnPc2	pracující
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Klub	klub	k1gInSc1	klub
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
a	a	k8xC	a
nevzhledná	vzhledný	k2eNgFnSc1d1	nevzhledná
"	"	kIx"	"
<g/>
Výšková	výškový	k2eAgFnSc1d1	výšková
budova	budova	k1gFnSc1	budova
policie	policie	k1gFnSc2	policie
<g/>
"	"	kIx"	"
na	na	k7c6	na
Velehradské	velehradský	k2eAgFnSc6d1	Velehradská
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
po	po	k7c6	po
oproti	oproti	k7c3	oproti
svému	svůj	k3xOyFgInSc3	svůj
plánu	plán	k1gInSc3	plán
snížena	snížit	k5eAaPmNgFnS	snížit
rozdělením	rozdělení	k1gNnSc7	rozdělení
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
návrh	návrh	k1gInSc4	návrh
vypracovalo	vypracovat	k5eAaPmAgNnS	vypracovat
projekční	projekční	k2eAgNnSc1d1	projekční
oddělení	oddělení	k1gNnSc1	oddělení
vojenských	vojenský	k2eAgFnPc2d1	vojenská
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
1984	[number]	k4	1984
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
tragédie	tragédie	k1gFnSc1	tragédie
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
podniku	podnik	k1gInSc6	podnik
MESIT	MESIT	kA	MESIT
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zřícení	zřícení	k1gNnSc1	zřícení
části	část	k1gFnSc2	část
výrobní	výrobní	k2eAgFnSc2d1	výrobní
haly	hala	k1gFnSc2	hala
a	a	k8xC	a
třípatrové	třípatrový	k2eAgFnSc2d1	třípatrová
budovy	budova	k1gFnSc2	budova
uvěznilo	uvěznit	k5eAaPmAgNnS	uvěznit
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
38	[number]	k4	38
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zavinilo	zavinit	k5eAaPmAgNnS	zavinit
smrt	smrt	k1gFnSc4	smrt
18	[number]	k4	18
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
zranění	zranění	k1gNnSc1	zranění
čtyř	čtyři	k4xCgFnPc2	čtyři
desítek	desítka	k1gFnPc2	desítka
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
ČSSR	ČSSR	kA	ČSSR
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
vůdce	vůdce	k1gMnSc1	vůdce
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Michail	Michaila	k1gFnPc2	Michaila
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
a	a	k8xC	a
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
si	se	k3xPyFc3	se
letecký	letecký	k2eAgInSc4d1	letecký
průmysl	průmysl	k1gInSc4	průmysl
v	v	k7c6	v
Kunovicích	Kunovice	k1gFnPc6	Kunovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pobočka	pobočka	k1gFnSc1	pobočka
Občanského	občanský	k2eAgNnSc2d1	občanské
fóra	fórum	k1gNnSc2	fórum
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
od	od	k7c2	od
12	[number]	k4	12
do	do	k7c2	do
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
zapojili	zapojit	k5eAaPmAgMnP	zapojit
do	do	k7c2	do
generální	generální	k2eAgFnSc2d1	generální
stávky	stávka	k1gFnSc2	stávka
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
Konec	konec	k1gInSc1	konec
vlády	vláda	k1gFnSc2	vláda
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
následujících	následující	k2eAgFnPc6d1	následující
politických	politický	k2eAgFnPc6d1	politická
změnách	změna	k1gFnPc6	změna
byla	být	k5eAaImAgFnS	být
zrušena	zrušen	k2eAgFnSc1d1	zrušena
či	či	k8xC	či
přeměněna	přeměněn	k2eAgFnSc1d1	přeměněna
řada	řada	k1gFnSc1	řada
institucí	instituce	k1gFnPc2	instituce
-	-	kIx~	-
asi	asi	k9	asi
nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
nastěhování	nastěhování	k1gNnSc1	nastěhování
Střední	střední	k2eAgFnSc2d1	střední
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
školy	škola	k1gFnSc2	škola
do	do	k7c2	do
bývalé	bývalý	k2eAgFnSc2d1	bývalá
budovy	budova	k1gFnSc2	budova
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
město	město	k1gNnSc1	město
navštívil	navštívit	k5eAaPmAgMnS	navštívit
československý	československý	k2eAgMnSc1d1	československý
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
někdejší	někdejší	k2eAgMnSc1d1	někdejší
disident	disident	k1gMnSc1	disident
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přestavbě	přestavba	k1gFnSc3	přestavba
budovy	budova	k1gFnSc2	budova
pošty	pošta	k1gFnSc2	pošta
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
estetické	estetický	k2eAgFnSc3d1	estetická
úrovni	úroveň	k1gFnSc3	úroveň
dřívějších	dřívější	k2eAgInPc2d1	dřívější
časů	čas	k1gInPc2	čas
byl	být	k5eAaImAgInS	být
však	však	k9	však
z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
hlediska	hledisko	k1gNnSc2	hledisko
prakticky	prakticky	k6eAd1	prakticky
nemožný	možný	k2eNgMnSc1d1	nemožný
<g/>
.	.	kIx.	.
</s>
<s>
Estetický	estetický	k2eAgInSc1d1	estetický
úpadek	úpadek	k1gInSc1	úpadek
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
byl	být	k5eAaImAgInS	být
patrný	patrný	k2eAgInSc1d1	patrný
na	na	k7c6	na
stavbách	stavba	k1gFnPc6	stavba
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Klub	klub	k1gInSc1	klub
pracujících	pracující	k1gMnPc2	pracující
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Výšková	výškový	k2eAgFnSc1d1	výšková
budova	budova	k1gFnSc1	budova
policie	policie	k1gFnSc2	policie
<g/>
"	"	kIx"	"
na	na	k7c6	na
Velehradské	velehradský	k2eAgFnSc6d1	Velehradská
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
likvidace	likvidace	k1gFnSc1	likvidace
interiéru	interiér	k1gInSc2	interiér
budovy	budova	k1gFnSc2	budova
spořitelny	spořitelna	k1gFnSc2	spořitelna
od	od	k7c2	od
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Fuchse	Fuchs	k1gMnSc2	Fuchs
či	či	k8xC	či
chátráním	chátrání	k1gNnSc7	chátrání
městských	městský	k2eAgFnPc2d1	městská
lázní	lázeň	k1gFnPc2	lázeň
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
téhož	týž	k3xTgMnSc2	týž
architekta	architekt	k1gMnSc2	architekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
město	město	k1gNnSc4	město
velká	velký	k2eAgFnSc1d1	velká
povodeň	povodeň	k1gFnSc1	povodeň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
značné	značný	k2eAgFnSc2d1	značná
škody	škoda	k1gFnSc2	škoda
a	a	k8xC	a
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejpostiženějších	postižený	k2eAgNnPc2d3	nejpostiženější
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
popovodňové	popovodňový	k2eAgFnSc2d1	popovodňový
obnovy	obnova	k1gFnSc2	obnova
se	se	k3xPyFc4	se
ale	ale	k9	ale
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
(	(	kIx(	(
<g/>
Liebschnerův	Liebschnerův	k2eAgInSc1d1	Liebschnerův
pavilon	pavilon	k1gInSc1	pavilon
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
vrátila	vrátit	k5eAaPmAgFnS	vrátit
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
architektura	architektura	k1gFnSc1	architektura
-	-	kIx~	-
bytový	bytový	k2eAgInSc1d1	bytový
dům	dům	k1gInSc1	dům
s	s	k7c7	s
atriem	atrium	k1gNnSc7	atrium
v	v	k7c6	v
Luční	luční	k2eAgFnSc6d1	luční
čtvrti	čtvrt	k1gFnSc6	čtvrt
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
brněnských	brněnský	k2eAgMnPc2d1	brněnský
architektů	architekt	k1gMnPc2	architekt
Gustava	Gustav	k1gMnSc2	Gustav
Křivinky	Křivinka	k1gFnSc2	Křivinka
a	a	k8xC	a
Aleše	Aleš	k1gMnSc2	Aleš
Buriana	Burian	k1gMnSc2	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Snahu	snaha	k1gFnSc4	snaha
některých	některý	k3yIgMnPc2	některý
obyvatel	obyvatel	k1gMnPc2	obyvatel
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Jarošov	Jarošov	k1gInSc4	Jarošov
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
se	se	k3xPyFc4	se
od	od	k7c2	od
města	město	k1gNnSc2	město
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
v	v	k7c6	v
místním	místní	k2eAgNnSc6d1	místní
referendu	referendum	k1gNnSc6	referendum
konaném	konaný	k2eAgNnSc6d1	konané
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2012	[number]	k4	2012
pouze	pouze	k6eAd1	pouze
23	[number]	k4	23
%	%	kIx~	%
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
dorazil	dorazit	k5eAaPmAgInS	dorazit
do	do	k7c2	do
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1841	[number]	k4	1841
od	od	k7c2	od
Břeclavi	Břeclav	k1gFnSc2	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
"	"	kIx"	"
<g/>
Ungarisch	Ungarisch	k1gMnSc1	Ungarisch
Hradisch	Hradisch	k1gMnSc1	Hradisch
<g/>
"	"	kIx"	"
Severní	severní	k2eAgFnSc2d1	severní
dráhy	dráha	k1gFnSc2	dráha
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
parostrojní	parostrojní	k2eAgFnSc1d1	parostrojní
železnice	železnice	k1gFnSc1	železnice
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc2d1	dnešní
stanice	stanice	k1gFnSc2	stanice
"	"	kIx"	"
<g/>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
železnice	železnice	k1gFnSc1	železnice
nemusela	muset	k5eNaImAgFnS	muset
procházet	procházet	k5eAaImF	procházet
historickým	historický	k2eAgNnSc7d1	historické
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
východní	východní	k2eAgFnSc2d1	východní
větve	větev	k1gFnSc2	větev
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
transverzální	transverzální	k2eAgFnSc2d1	transverzální
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Vlárské	vlárský	k2eAgFnPc4d1	Vlárská
dráhy	dráha	k1gFnPc4	dráha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1883	[number]	k4	1883
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
lokální	lokální	k2eAgFnSc6d1	lokální
dráze	dráha	k1gFnSc6	dráha
ze	z	k7c2	z
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
přes	přes	k7c4	přes
centrum	centrum	k1gNnSc4	centrum
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
do	do	k7c2	do
Uherského	uherský	k2eAgInSc2d1	uherský
Brodu	Brod	k1gInSc2	Brod
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
v	v	k7c6	v
Kunovicích	Kunovice	k1gFnPc6	Kunovice
napojené	napojený	k2eAgFnPc1d1	napojená
na	na	k7c4	na
Kyjov	Kyjov	k1gInSc4	Kyjov
a	a	k8xC	a
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
provizorní	provizorní	k2eAgFnSc6d1	provizorní
spojce	spojka	k1gFnSc6	spojka
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Kunovice	Kunovice	k1gFnPc4	Kunovice
a	a	k8xC	a
Staré	Staré	k2eAgNnSc4d1	Staré
Město	město	k1gNnSc4	město
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
reorganizace	reorganizace	k1gFnSc2	reorganizace
tratí	trať	k1gFnPc2	trať
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
je	být	k5eAaImIp3nS	být
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
koncovou	koncový	k2eAgFnSc7d1	koncová
stanicí	stanice	k1gFnSc7	stanice
trati	trať	k1gFnSc2	trať
340	[number]	k4	340
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
)	)	kIx)	)
a	a	k8xC	a
prochází	procházet	k5eAaImIp3nS	procházet
jím	on	k3xPp3gInSc7	on
trať	trať	k1gFnSc1	trať
341	[number]	k4	341
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
-	-	kIx~	-
Vlárský	vlárský	k2eAgInSc1d1	vlárský
průsmyk	průsmyk	k1gInSc1	průsmyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
od	od	k7c2	od
architekta	architekt	k1gMnSc4	architekt
Karla	Karel	k1gMnSc2	Karel
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
,	,	kIx,	,
vyzdobená	vyzdobený	k2eAgFnSc1d1	vyzdobená
typickými	typický	k2eAgInPc7d1	typický
slováckými	slovácký	k2eAgInPc7d1	slovácký
motivy	motiv	k1gInPc7	motiv
od	od	k7c2	od
Rozky	Rozka	k1gFnSc2	Rozka
Falešníkové	Falešníkové	k?	Falešníkové
z	z	k7c2	z
Tasova	Tasov	k1gInSc2	Tasov
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
sdružení	sdružení	k1gNnSc2	sdružení
Asociace	asociace	k1gFnSc2	asociace
Entente	entente	k1gFnSc2	entente
Florale	Floral	k1gMnSc5	Floral
CZ	CZ	kA	CZ
-	-	kIx~	-
Souznění	souznění	k1gNnSc1	souznění
zvolena	zvolit	k5eAaPmNgFnS	zvolit
nejkrásnějším	krásný	k2eAgNnSc7d3	nejkrásnější
nádražím	nádraží	k1gNnSc7	nádraží
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Předcházela	předcházet	k5eAaImAgFnS	předcházet
tomu	ten	k3xDgNnSc3	ten
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
budovy	budova	k1gFnSc2	budova
provedená	provedený	k2eAgFnSc1d1	provedená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
město	město	k1gNnSc1	město
ocenilo	ocenit	k5eAaPmAgNnS	ocenit
titulem	titul	k1gInSc7	titul
Stavba	stavba	k1gFnSc1	stavba
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
stanice	stanice	k1gFnSc2	stanice
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
se	se	k3xPyFc4	se
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k6eAd1	ještě
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Vésky	Véska	k1gFnSc2	Véska
a	a	k8xC	a
bezprostředně	bezprostředně	k6eAd1	bezprostředně
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
katastru	katastr	k1gInSc2	katastr
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
Sady	sada	k1gFnSc2	sada
leží	ležet	k5eAaImIp3nP	ležet
železniční	železniční	k2eAgFnPc4d1	železniční
stanice	stanice	k1gFnPc4	stanice
Kunovice	Kunovice	k1gFnPc4	Kunovice
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
EuroCity	EuroCita	k1gFnSc2	EuroCita
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
má	mít	k5eAaImIp3nS	mít
7	[number]	k4	7
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
a	a	k8xC	a
Kunovice	Kunovice	k1gFnPc4	Kunovice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
města	město	k1gNnSc2	město
první	první	k4xOgInPc4	první
ekologické	ekologický	k2eAgInPc4d1	ekologický
autobusy	autobus	k1gInPc4	autobus
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
na	na	k7c4	na
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
vysoké	vysoký	k2eAgFnSc3d1	vysoká
ceně	cena	k1gFnSc3	cena
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc3d1	vysoká
pořizovací	pořizovací	k2eAgFnSc3d1	pořizovací
ceně	cena	k1gFnSc3	cena
nových	nový	k2eAgInPc2d1	nový
plynových	plynový	k2eAgInPc2d1	plynový
autobusů	autobus	k1gInPc2	autobus
a	a	k8xC	a
také	také	k9	také
stáří	stáří	k1gNnSc1	stáří
autobusů	autobus	k1gInPc2	autobus
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
ekologické	ekologický	k2eAgInPc1d1	ekologický
autobusy	autobus	k1gInPc1	autobus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vyřazeny	vyřazen	k2eAgInPc1d1	vyřazen
a	a	k8xC	a
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
novými	nový	k2eAgInPc7d1	nový
naftovými	naftový	k2eAgInPc7d1	naftový
autobusy	autobus	k1gInPc7	autobus
plnícími	plnící	k2eAgInPc7d1	plnící
normu	norma	k1gFnSc4	norma
Euro	euro	k1gNnSc1	euro
3	[number]	k4	3
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
ekologičtější	ekologický	k2eAgFnPc1d2	ekologičtější
než	než	k8xS	než
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
staré	starý	k2eAgInPc4d1	starý
autobusy	autobus	k1gInPc7	autobus
na	na	k7c6	na
CNG	CNG	kA	CNG
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
jsou	být	k5eAaImIp3nP	být
pořizovány	pořizován	k2eAgInPc1d1	pořizován
pouze	pouze	k6eAd1	pouze
nízkopodlažní	nízkopodlažní	k2eAgInPc1d1	nízkopodlažní
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
nízkopodlažní	nízkopodlažní	k2eAgInPc1d1	nízkopodlažní
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
MHD	MHD	kA	MHD
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
,	,	kIx,	,
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
a	a	k8xC	a
Kunovicích	Kunovice	k1gFnPc6	Kunovice
13	[number]	k4	13
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
nízkopodlažních	nízkopodlažní	k2eAgInPc2d1	nízkopodlažní
nebo	nebo	k8xC	nebo
vybavena	vybavit	k5eAaPmNgFnS	vybavit
zvedací	zvedací	k2eAgFnSc7d1	zvedací
plošinou	plošina	k1gFnSc7	plošina
pro	pro	k7c4	pro
invalidní	invalidní	k2eAgInSc4d1	invalidní
vozík	vozík	k1gInSc4	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
je	být	k5eAaImIp3nS	být
ČSAD	ČSAD	kA	ČSAD
BUS	bus	k1gInSc4	bus
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
a.	a.	k?	a.
s.	s.	k?	s.
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
stavebních	stavební	k2eAgFnPc6d1	stavební
pracích	práce	k1gFnPc6	práce
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
otevřen	otevřen	k2eAgInSc1d1	otevřen
kompletní	kompletní	k2eAgInSc1d1	kompletní
obchvat	obchvat	k1gInSc1	obchvat
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
6,851	[number]	k4	6,851
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
přeložka	přeložka	k1gFnSc1	přeložka
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
spojující	spojující	k2eAgFnSc7d1	spojující
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
Trenčín	Trenčín	k1gInSc4	Trenčín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
jižně	jižně	k6eAd1	jižně
pod	pod	k7c7	pod
obcí	obec	k1gFnSc7	obec
Zlechov	Zlechovo	k1gNnPc2	Zlechovo
<g/>
,	,	kIx,	,
podchází	podcházet	k5eAaImIp3nS	podcházet
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Přerov	Přerov	k1gInSc1	Přerov
<g/>
-	-	kIx~	-
<g/>
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
mostem	most	k1gInSc7	most
délky	délka	k1gFnSc2	délka
138	[number]	k4	138
m	m	kA	m
překračuje	překračovat	k5eAaImIp3nS	překračovat
řeku	řeka	k1gFnSc4	řeka
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
jižně	jižně	k6eAd1	jižně
obchází	obcházet	k5eAaImIp3nP	obcházet
lokalitu	lokalita	k1gFnSc4	lokalita
Kunovského	Kunovský	k2eAgInSc2d1	Kunovský
lesa	les	k1gInSc2	les
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
dále	daleko	k6eAd2	daleko
ke	k	k7c3	k
Kunovicím	Kunovice	k1gFnPc3	Kunovice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
unikátní	unikátní	k2eAgFnSc7d1	unikátní
estakádou	estakáda	k1gFnSc7	estakáda
(	(	kIx(	(
<g/>
svou	svůj	k3xOyFgFnSc7	svůj
délkou	délka	k1gFnSc7	délka
<g />
.	.	kIx.	.
</s>
<s>
1012	[number]	k4	1012
m	m	kA	m
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
otevření	otevření	k1gNnSc2	otevření
stala	stát	k5eAaPmAgFnS	stát
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
mostním	mostní	k2eAgInSc7d1	mostní
objektem	objekt	k1gInSc7	objekt
na	na	k7c6	na
silniční	silniční	k2eAgFnSc6d1	silniční
síti	síť	k1gFnSc6	síť
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
překračuje	překračovat	k5eAaImIp3nS	překračovat
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Brno	Brno	k1gNnSc4	Brno
-	-	kIx~	-
Trenčianská	trenčianský	k2eAgFnSc1d1	Trenčianská
Teplá	Teplá	k1gFnSc1	Teplá
a	a	k8xC	a
původní	původní	k2eAgFnSc3d1	původní
silnici	silnice	k1gFnSc3	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
již	již	k6eAd1	již
vybudovaný	vybudovaný	k2eAgInSc4d1	vybudovaný
pokračující	pokračující	k2eAgInSc4d1	pokračující
úsek	úsek	k1gInSc4	úsek
Kunovice	Kunovice	k1gFnPc4	Kunovice
<g/>
-	-	kIx~	-
<g/>
Vésky	Véska	k1gFnPc4	Véska
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obchvat	obchvat	k1gInSc1	obchvat
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
hejtmana	hejtman	k1gMnSc2	hejtman
a	a	k8xC	a
čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Stavba	stavba	k1gFnSc1	stavba
roku	rok	k1gInSc2	rok
Zlínského	zlínský	k2eAgInSc2d1	zlínský
kraje	kraj	k1gInSc2	kraj
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
estakáda	estakáda	k1gFnSc1	estakáda
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
od	od	k7c2	od
ministra	ministr	k1gMnSc2	ministr
dopravy	doprava	k1gFnSc2	doprava
ocenění	ocenění	k1gNnSc2	ocenění
Mostní	mostní	k2eAgNnSc4d1	mostní
dílo	dílo	k1gNnSc4	dílo
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
provozuje	provozovat	k5eAaImIp3nS	provozovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Klubu	klub	k1gInSc2	klub
kultury	kultura	k1gFnSc2	kultura
sedm	sedm	k4xCc1	sedm
kulturních	kulturní	k2eAgInPc2d1	kulturní
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
Klub	klub	k1gInSc1	klub
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
Reduta	reduta	k1gFnSc1	reduta
<g/>
,	,	kIx,	,
Slovácká	slovácký	k2eAgFnSc1d1	Slovácká
búda	búda	k1gFnSc1	búda
<g/>
,	,	kIx,	,
MKZ	MKZ	kA	MKZ
Mařatice	Mařatice	k1gFnPc1	Mařatice
<g/>
,	,	kIx,	,
MKZ	MKZ	kA	MKZ
Sady	sada	k1gFnPc1	sada
<g/>
,	,	kIx,	,
MKZ	MKZ	kA	MKZ
Vésky	Véska	k1gFnPc1	Véska
<g/>
,	,	kIx,	,
MKZ	MKZ	kA	MKZ
Míkovice	Míkovice	k1gFnSc1	Míkovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
celé	celý	k2eAgFnSc2d1	celá
republiky	republika	k1gFnSc2	republika
nebývalý	bývalý	k2eNgInSc4d1	bývalý
počet	počet	k1gInSc4	počet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
kino	kino	k1gNnSc4	kino
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
tři	tři	k4xCgNnPc4	tři
divadla	divadlo	k1gNnPc4	divadlo
<g/>
:	:	kIx,	:
plně	plně	k6eAd1	plně
profesionální	profesionální	k2eAgNnSc4d1	profesionální
Slovácké	slovácký	k2eAgNnSc4d1	Slovácké
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
poloprofesionální	poloprofesionální	k2eAgNnSc4d1	poloprofesionální
Hoffmanovo	Hoffmanův	k2eAgNnSc4d1	Hoffmanovo
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
amatérské	amatérský	k2eAgFnSc2d1	amatérská
SKI	ski	k1gFnSc2	ski
Vésky	Véska	k1gFnSc2	Véska
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
navštívení	navštívení	k1gNnSc2	navštívení
stálé	stálý	k2eAgFnSc2d1	stálá
i	i	k8xC	i
dočasných	dočasný	k2eAgFnPc2d1	dočasná
výstav	výstava	k1gFnPc2	výstava
o	o	k7c6	o
městě	město	k1gNnSc6	město
a	a	k8xC	a
regionu	region	k1gInSc6	region
nabízí	nabízet	k5eAaImIp3nS	nabízet
Slovácké	slovácký	k2eAgNnSc4d1	Slovácké
muzeum	muzeum	k1gNnSc4	muzeum
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
svou	svůj	k3xOyFgFnSc4	svůj
pobočku	pobočka	k1gFnSc4	pobočka
-	-	kIx~	-
Centrum	centrum	k1gNnSc1	centrum
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
archeologie	archeologie	k1gFnSc2	archeologie
-	-	kIx~	-
i	i	k9	i
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
koná	konat	k5eAaImIp3nS	konat
celostátně	celostátně	k6eAd1	celostátně
proslulá	proslulý	k2eAgFnSc1d1	proslulá
přehlídka	přehlídka	k1gFnSc1	přehlídka
filmů	film	k1gInPc2	film
Letní	letní	k2eAgFnSc1d1	letní
filmová	filmový	k2eAgFnSc1d1	filmová
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
hostí	hostit	k5eAaImIp3nS	hostit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
folklorních	folklorní	k2eAgFnPc2d1	folklorní
akcí	akce	k1gFnPc2	akce
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
Slovácké	slovácký	k2eAgFnPc1d1	Slovácká
slavnosti	slavnost	k1gFnPc1	slavnost
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
otevřených	otevřený	k2eAgFnPc2d1	otevřená
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nP	sídlet
zde	zde	k6eAd1	zde
regionální	regionální	k2eAgInPc1d1	regionální
deníky	deník	k1gInPc1	deník
Slovácké	slovácký	k2eAgFnSc2d1	Slovácká
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Dobrý	dobrý	k2eAgInSc1d1	dobrý
den	den	k1gInSc1	den
s	s	k7c7	s
Kurýrem	kurýr	k1gMnSc7	kurýr
atd.	atd.	kA	atd.
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
přes	přes	k7c4	přes
35	[number]	k4	35
kulturně	kulturně	k6eAd1	kulturně
orientovaných	orientovaný	k2eAgInPc2d1	orientovaný
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nP	patřit
folklorní	folklorní	k2eAgInPc4d1	folklorní
soubory	soubor	k1gInPc4	soubor
Hradišťan	Hradišťan	k1gMnSc1	Hradišťan
<g/>
,	,	kIx,	,
Kunovjan	Kunovjan	k1gMnSc1	Kunovjan
<g/>
,	,	kIx,	,
CM	cm	kA	cm
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Čecha	Čech	k1gMnSc2	Čech
<g/>
,	,	kIx,	,
Burčáci	Burčák	k1gMnPc1	Burčák
<g/>
,	,	kIx,	,
Hradišťánek	Hradišťánek	k1gMnSc1	Hradišťánek
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
působením	působení	k1gNnSc7	působení
pozměnili	pozměnit	k5eAaPmAgMnP	pozměnit
tvář	tvář	k1gFnSc4	tvář
města	město	k1gNnPc1	město
architekti	architekt	k1gMnPc1	architekt
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Zákrejs	Zákrejsa	k1gFnPc2	Zákrejsa
-	-	kIx~	-
rondokubismus	rondokubismus	k1gInSc1	rondokubismus
<g/>
.	.	kIx.	.
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
-	-	kIx~	-
stavby	stavba	k1gFnPc1	stavba
v	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
:	:	kIx,	:
Městské	městský	k2eAgFnPc1d1	městská
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
spořitelny	spořitelna	k1gFnSc2	spořitelna
původně	původně	k6eAd1	původně
salon	salon	k1gInSc4	salon
firmy	firma	k1gFnSc2	firma
Auto	auto	k1gNnSc4	auto
Praga	Prag	k1gMnSc2	Prag
<g/>
,	,	kIx,	,
Slovácké	slovácký	k2eAgNnSc1d1	Slovácké
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Liebschner	Liebschner	k1gMnSc1	Liebschner
-	-	kIx~	-
přední	přední	k2eAgMnSc1d1	přední
funkcionalistický	funkcionalistický	k2eAgMnSc1d1	funkcionalistický
architekt	architekt	k1gMnSc1	architekt
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
postavil	postavit	k5eAaPmAgInS	postavit
infekční	infekční	k2eAgInSc1d1	infekční
pavilon	pavilon	k1gInSc1	pavilon
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
tohoto	tento	k3xDgInSc2	tento
izolačního	izolační	k2eAgInSc2d1	izolační
pavilonu	pavilon	k1gInSc2	pavilon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
léčila	léčit	k5eAaImAgFnS	léčit
např.	např.	kA	např.
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
až	až	k9	až
po	po	k7c6	po
vyjmutí	vyjmutí	k1gNnSc6	vyjmutí
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
nevratných	vratný	k2eNgFnPc2d1	nevratná
přestaveb	přestavba	k1gFnPc2	přestavba
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zrušení	zrušení	k1gNnSc1	zrušení
horizontálního	horizontální	k2eAgInSc2d1	horizontální
balkonu	balkon	k1gInSc2	balkon
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
budovy	budova	k1gFnSc2	budova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přebudování	přebudování	k1gNnSc3	přebudování
celého	celý	k2eAgInSc2d1	celý
interiéru	interiér	k1gInSc2	interiér
podle	podle	k7c2	podle
dnešních	dnešní	k2eAgFnPc2d1	dnešní
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Schaniak	Schaniak	k1gMnSc1	Schaniak
-	-	kIx~	-
uherskohradišťský	uherskohradišťský	k2eAgMnSc1d1	uherskohradišťský
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
staré	starý	k2eAgFnSc2d1	stará
budovy	budova	k1gFnSc2	budova
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Schaniakových	Schaniakový	k2eAgInPc2d1	Schaniakový
domů	dům	k1gInPc2	dům
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
ulici	ulice	k1gFnSc6	ulice
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Michal	Michal	k1gMnSc1	Michal
-	-	kIx~	-
realizace	realizace	k1gFnSc1	realizace
typizovaného	typizovaný	k2eAgInSc2d1	typizovaný
projektu	projekt	k1gInSc2	projekt
(	(	kIx(	(
<g/>
kino	kino	k1gNnSc1	kino
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Křivinka	Křivinka	k1gFnSc1	Křivinka
a	a	k8xC	a
Aleš	Aleš	k1gMnSc1	Aleš
Burian	Burian	k1gMnSc1	Burian
-	-	kIx~	-
brněnští	brněnský	k2eAgMnPc1d1	brněnský
architekti	architekt	k1gMnPc1	architekt
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
povodňový	povodňový	k2eAgInSc4d1	povodňový
bytový	bytový	k2eAgInSc4d1	bytový
dům	dům	k1gInSc4	dům
v	v	k7c6	v
Luční	luční	k2eAgFnSc6d1	luční
čtvrti	čtvrt	k1gFnSc6	čtvrt
s	s	k7c7	s
atriem	atrium	k1gNnSc7	atrium
<g/>
,	,	kIx,	,
oceněný	oceněný	k2eAgInSc4d1	oceněný
řadou	řada	k1gFnSc7	řada
architektonických	architektonický	k2eAgFnPc2d1	architektonická
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Augustin	Augustin	k1gMnSc1	Augustin
Sedlář	Sedlář	k1gMnSc1	Sedlář
-	-	kIx~	-
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
stavba	stavba	k1gFnSc1	stavba
veslařského	veslařský	k2eAgInSc2d1	veslařský
klubu	klub	k1gInSc2	klub
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Sady	sad	k1gInPc1	sad
"	"	kIx"	"
<g/>
Špitálky	špitálek	k1gInPc1	špitálek
<g/>
"	"	kIx"	"
-	-	kIx~	-
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
,	,	kIx,	,
velkomoravská	velkomoravský	k2eAgFnSc1d1	Velkomoravská
akropole	akropole	k1gFnSc1	akropole
<g/>
,	,	kIx,	,
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
významného	významný	k2eAgInSc2d1	významný
církevního	církevní	k2eAgInSc2d1	církevní
komplexu	komplex	k1gInSc2	komplex
z	z	k7c2	z
období	období	k1gNnSc2	období
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
možného	možný	k2eAgNnSc2d1	možné
místa	místo	k1gNnSc2	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
knížete	kníže	k1gMnSc2	kníže
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
nebo	nebo	k8xC	nebo
biskupa	biskup	k1gMnSc2	biskup
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
Xaverského	xaverský	k2eAgMnSc2d1	xaverský
<g/>
,	,	kIx,	,
jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
kolej	kolej	k1gFnSc1	kolej
<g/>
,	,	kIx,	,
Reduta	reduta	k1gFnSc1	reduta
Kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc6	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Sadech	sad	k1gInPc6	sad
Kostel	kostel	k1gInSc1	kostel
Zvěstování	zvěstování	k1gNnSc6	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Barokní	barokní	k2eAgFnSc1d1	barokní
kašna	kašna	k1gFnSc1	kašna
na	na	k7c6	na
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
Stanclova	Stanclův	k2eAgFnSc1d1	Stanclův
Lékárna	lékárna	k1gFnSc1	lékárna
U	u	k7c2	u
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
koruny	koruna	k1gFnSc2	koruna
-	-	kIx~	-
vzácné	vzácný	k2eAgNnSc1d1	vzácné
sgrafito	sgrafito	k1gNnSc1	sgrafito
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc1d1	barokní
freska	freska	k1gFnSc1	freska
Matyášova	Matyášův	k2eAgFnSc1d1	Matyášova
brána	brána	k1gFnSc1	brána
-	-	kIx~	-
spíše	spíše	k9	spíše
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
původní	původní	k2eAgFnSc2d1	původní
středověké	středověký	k2eAgFnSc2d1	středověká
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc1d1	původní
brána	brána	k1gFnSc1	brána
<g />
.	.	kIx.	.
</s>
<s>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
zadní	zadní	k2eAgNnPc4d1	zadní
<g/>
"	"	kIx"	"
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
"	"	kIx"	"
<g/>
shořelá	shořelý	k2eAgFnSc1d1	shořelá
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zbourána	zbourán	k2eAgFnSc1d1	zbourána
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
<g/>
)	)	kIx)	)
Zbytky	zbytek	k1gInPc1	zbytek
středověkých	středověký	k2eAgFnPc2d1	středověká
hradeb	hradba	k1gFnPc2	hradba
ve	v	k7c6	v
Vodní	vodní	k2eAgFnSc6d1	vodní
ulici	ulice	k1gFnSc6	ulice
-	-	kIx~	-
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
hradební	hradební	k2eAgFnSc1d1	hradební
bašta	bašta	k1gFnSc1	bašta
na	na	k7c4	na
obytný	obytný	k2eAgInSc4d1	obytný
dům	dům	k1gInSc4	dům
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
Justiční	justiční	k2eAgFnSc1d1	justiční
palác	palác	k1gInSc1	palác
-	-	kIx~	-
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
stavba	stavba	k1gFnSc1	stavba
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
byla	být	k5eAaImAgFnS	být
sídlem	sídlo	k1gNnSc7	sídlo
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
nachází	nacházet	k5eAaImIp3nS	nacházet
Střední	střední	k2eAgFnSc1d1	střední
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1	uměleckoprůmyslová
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
na	na	k7c6	na
Mariánském	mariánský	k2eAgNnSc6d1	Mariánské
náměstí	náměstí	k1gNnSc6	náměstí
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Alžběty	Alžběta	k1gFnSc2	Alžběta
-	-	kIx~	-
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
raně	raně	k6eAd1	raně
gotické	gotický	k2eAgInPc1d1	gotický
prvky	prvek	k1gInPc1	prvek
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Šebestiána	Šebestián	k1gMnSc2	Šebestián
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Palackého	Palacký	k1gMnSc2	Palacký
-	-	kIx~	-
postavena	postaven	k2eAgFnSc1d1	postavena
roku	rok	k1gInSc2	rok
1715	[number]	k4	1715
městskou	městský	k2eAgFnSc7d1	městská
posádkou	posádka	k1gFnSc7	posádka
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
po	po	k7c6	po
kolejích	kolej	k1gFnPc6	kolej
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
místa	místo	k1gNnSc2	místo
u	u	k7c2	u
hotelu	hotel	k1gInSc2	hotel
o	o	k7c4	o
32	[number]	k4	32
metrů	metr	k1gInPc2	metr
na	na	k7c4	na
nově	nově	k6eAd1	nově
vybudované	vybudovaný	k2eAgInPc4d1	vybudovaný
základy	základ	k1gInPc4	základ
-	-	kIx~	-
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zkoušku	zkouška	k1gFnSc4	zkouška
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
použita	použít	k5eAaPmNgFnS	použít
při	při	k7c6	při
přesunu	přesun	k1gInSc6	přesun
děkanského	děkanský	k2eAgInSc2d1	děkanský
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Mostě	most	k1gInSc6	most
<g/>
.	.	kIx.	.
</s>
<s>
Synagoga	synagoga	k1gFnSc1	synagoga
-	-	kIx~	-
dnešní	dnešní	k2eAgFnSc1d1	dnešní
knihovna	knihovna	k1gFnSc1	knihovna
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Beneše	Beneš	k1gMnSc2	Beneš
Buchlovana	Buchlovan	k1gMnSc2	Buchlovan
<g/>
,	,	kIx,	,
obnovena	obnoven	k2eAgFnSc1d1	obnovena
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
v	v	k7c6	v
barevnosti	barevnost	k1gFnSc6	barevnost
fasády	fasáda	k1gFnSc2	fasáda
<g/>
,	,	kIx,	,
opatřena	opatřen	k2eAgFnSc1d1	opatřena
pamětní	pamětní	k2eAgFnSc7d1	pamětní
deskou	deska	k1gFnSc7	deska
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
a	a	k8xC	a
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
,	,	kIx,	,
dobudována	dobudován	k2eAgFnSc1d1	dobudována
věž	věž	k1gFnSc1	věž
podle	podle	k7c2	podle
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
fotografií	fotografia	k1gFnPc2	fotografia
v	v	k7c6	v
pseudomaurském	pseudomaurský	k2eAgInSc6d1	pseudomaurský
slohu	sloh	k1gInSc6	sloh
Zbytky	zbytek	k1gInPc1	zbytek
židovského	židovský	k2eAgInSc2d1	židovský
hřbitova	hřbitov	k1gInSc2	hřbitov
v	v	k7c6	v
Sadech	sad	k1gInPc6	sad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
zničeného	zničený	k2eAgInSc2d1	zničený
wehrmachtem	wehrmacht	k1gInSc7	wehrmacht
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
Hotel	hotel	k1gInSc1	hotel
Slunce	slunce	k1gNnSc2	slunce
-	-	kIx~	-
původní	původní	k2eAgNnSc4d1	původní
středověké	středověký	k2eAgNnSc4d1	středověké
podloubí	podloubí	k1gNnSc4	podloubí
<g/>
,	,	kIx,	,
u	u	k7c2	u
sousední	sousední	k2eAgFnSc2d1	sousední
lékárny	lékárna	k1gFnSc2	lékárna
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
úpravu	úprava	k1gFnSc4	úprava
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Slovácká	slovácký	k2eAgFnSc1d1	Slovácká
búda	búda	k1gFnSc1	búda
ve	v	k7c6	v
Smetanových	Smetanových	k2eAgInPc6d1	Smetanových
sadech	sad	k1gInPc6	sad
Vinný	vinný	k2eAgInSc4d1	vinný
sklep	sklep	k1gInSc4	sklep
U	u	k7c2	u
Lisu	lis	k1gInSc2	lis
-	-	kIx~	-
starý	starý	k2eAgInSc1d1	starý
vinný	vinný	k2eAgInSc1d1	vinný
sklep	sklep	k1gInSc1	sklep
Budova	budova	k1gFnSc1	budova
gymnázia	gymnázium	k1gNnPc4	gymnázium
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Schaniaka	Schaniak	k1gMnSc2	Schaniak
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
stavba	stavba	k1gFnSc1	stavba
Lázně	lázeň	k1gFnSc2	lázeň
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
spořitelny	spořitelna	k1gFnSc2	spořitelna
na	na	k7c6	na
Palackého	Palackého	k2eAgNnSc6d1	Palackého
náměstí	náměstí	k1gNnSc6	náměstí
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Fuchse	Fuchs	k1gMnSc2	Fuchs
Galerie	galerie	k1gFnSc1	galerie
Slováckého	slovácký	k2eAgNnSc2d1	Slovácké
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Otakarově	Otakarův	k2eAgFnSc6d1	Otakarova
ulici	ulice	k1gFnSc6	ulice
hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
Slováckého	slovácký	k2eAgNnSc2d1	Slovácké
muzea	muzeum	k1gNnSc2	muzeum
ve	v	k7c6	v
Smetanových	Smetanových	k2eAgInPc6d1	Smetanových
sadech	sad	k1gInPc6	sad
Roku	rok	k1gInSc2	rok
1749	[number]	k4	1749
založil	založit	k5eAaPmAgMnS	založit
děkan	děkan	k1gMnSc1	děkan
P.	P.	kA	P.
František	František	k1gMnSc1	František
Schuppler	Schuppler	k1gMnSc1	Schuppler
u	u	k7c2	u
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
Loretánskou	loretánský	k2eAgFnSc4d1	Loretánská
kapli	kaple	k1gFnSc4	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
gotickou	gotický	k2eAgFnSc7d1	gotická
trojlodní	trojlodní	k2eAgFnSc7d1	trojlodní
stavbou	stavba	k1gFnSc7	stavba
s	s	k7c7	s
nižšími	nízký	k2eAgFnPc7d2	nižší
bočními	boční	k2eAgFnPc7d1	boční
loděmi	loď	k1gFnPc7	loď
a	a	k8xC	a
s	s	k7c7	s
polygonálním	polygonální	k2eAgInSc7d1	polygonální
závěrem	závěr	k1gInSc7	závěr
presbytáře	presbytář	k1gInSc2	presbytář
a	a	k8xC	a
stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
nynějším	nynější	k2eAgNnSc6d1	nynější
Masarykově	Masarykův	k2eAgNnSc6d1	Masarykovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
byla	být	k5eAaImAgFnS	být
Santa	Santa	k1gFnSc1	Santa
Casa	Cas	k1gInSc2	Cas
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
zbořena	zbořit	k5eAaPmNgFnS	zbořit
<g/>
.	.	kIx.	.
</s>
<s>
Oltářní	oltářní	k2eAgFnSc1d1	oltářní
stěna	stěna	k1gFnSc1	stěna
z	z	k7c2	z
Loretánské	loretánský	k2eAgFnSc2d1	Loretánská
kaple	kaple	k1gFnSc2	kaple
byla	být	k5eAaImAgFnS	být
přenesena	přenést	k5eAaPmNgFnS	přenést
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
Xaverského	xaverský	k2eAgMnSc2d1	xaverský
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Slovácko	Slovácko	k1gNnSc1	Slovácko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Městském	městský	k2eAgInSc6d1	městský
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
stadionu	stadion	k1gInSc6	stadion
Miroslava	Miroslav	k1gMnSc2	Miroslav
Valenty	Valenta	k1gMnSc2	Valenta
hraje	hrát	k5eAaImIp3nS	hrát
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
klub	klub	k1gInSc4	klub
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Slovácko	Slovácko	k1gNnSc1	Slovácko
1	[number]	k4	1
<g/>
.	.	kIx.	.
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
ženský	ženský	k2eAgInSc4d1	ženský
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
kopané	kopaná	k1gFnSc2	kopaná
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
začíná	začínat	k5eAaImIp3nS	začínat
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
přivezen	přivezen	k2eAgInSc4d1	přivezen
první	první	k4xOgInSc4	první
kopací	kopací	k2eAgInSc4d1	kopací
míč	míč	k1gInSc4	míč
vysokoškolákem	vysokoškolák	k1gMnSc7	vysokoškolák
Viktorem	Viktor	k1gMnSc7	Viktor
Rumlerem	Rumler	k1gMnSc7	Rumler
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
Slavie	slavie	k1gFnSc2	slavie
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
několik	několik	k4yIc4	několik
fotbalových	fotbalový	k2eAgMnPc2d1	fotbalový
nadšenců	nadšenec	k1gMnPc2	nadšenec
a	a	k8xC	a
hoteliér	hoteliér	k1gMnSc1	hoteliér
Antonín	Antonín	k1gMnSc1	Antonín
Čáp	Čáp	k1gMnSc1	Čáp
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Grand	grand	k1gMnSc1	grand
založili	založit	k5eAaPmAgMnP	založit
klub	klub	k1gInSc4	klub
AC	AC	kA	AC
Slovácká	slovácký	k2eAgFnSc1d1	Slovácká
Slavia	Slavia	k1gFnSc1	Slavia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
96	[number]	k4	96
postoupil	postoupit	k5eAaPmAgInS	postoupit
městský	městský	k2eAgInSc1d1	městský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
tým	tým	k1gInSc1	tým
FC	FC	kA	FC
Slovácká	slovácký	k2eAgFnSc1d1	Slovácká
Slavia	Slavia	k1gFnSc1	Slavia
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
druhý	druhý	k4xOgInSc4	druhý
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
sehrál	sehrát	k5eAaPmAgMnS	sehrát
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1995	[number]	k4	1995
se	s	k7c7	s
Spartou	Sparta	k1gFnSc7	Sparta
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
např.	např.	kA	např.
Jan	Jan	k1gMnSc1	Jan
Koller	Koller	k1gMnSc1	Koller
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
Lokvenc	Lokvenc	k1gInSc1	Lokvenc
či	či	k8xC	či
brankář	brankář	k1gMnSc1	brankář
Petr	Petr	k1gMnSc1	Petr
Kouba	Kouba	k1gMnSc1	Kouba
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
vicemistrem	vicemistr	k1gMnSc7	vicemistr
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc4	zápas
skončil	skončit	k5eAaPmAgMnS	skončit
stavem	stav	k1gInSc7	stav
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ještě	ještě	k9	ještě
nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
vedlo	vést	k5eAaImAgNnS	vést
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
11	[number]	k4	11
000	[number]	k4	000
platících	platící	k2eAgMnPc2d1	platící
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
15	[number]	k4	15
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
regionu	region	k1gInSc2	region
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
klubu	klub	k1gInSc2	klub
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
a	a	k8xC	a
překonala	překonat	k5eAaPmAgFnS	překonat
rekord	rekord	k1gInSc4	rekord
10	[number]	k4	10
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
soutěže	soutěž	k1gFnSc2	soutěž
Český	český	k2eAgInSc4d1	český
pohár	pohár	k1gInSc4	pohár
utkalo	utkat	k5eAaPmAgNnS	utkat
se	s	k7c7	s
Slavií	slavie	k1gFnSc7	slavie
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
gól	gól	k1gInSc1	gól
za	za	k7c4	za
FC	FC	kA	FC
Slováckou	slovácký	k2eAgFnSc4d1	Slovácká
Slavii	slavie	k1gFnSc4	slavie
umístil	umístit	k5eAaPmAgMnS	umístit
záložník	záložník	k1gMnSc1	záložník
Kasper	Kasper	k1gMnSc1	Kasper
Så	Så	k1gMnSc1	Så
<g/>
,	,	kIx,	,
posila	posila	k1gFnSc1	posila
z	z	k7c2	z
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
hlavou	hlava	k1gFnSc7	hlava
již	již	k9	již
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
prvního	první	k4xOgInSc2	první
poločasu	poločas	k1gInSc2	poločas
<g/>
,	,	kIx,	,
po	po	k7c6	po
pravém	pravý	k2eAgInSc6d1	pravý
rohovém	rohový	k2eAgInSc6d1	rohový
kopu	kop	k1gInSc6	kop
Petra	Petr	k1gMnSc2	Petr
Podaného	podaný	k2eAgMnSc2d1	podaný
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
HC	HC	kA	HC
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
je	být	k5eAaImIp3nS	být
zimní	zimní	k2eAgInSc1d1	zimní
stadion	stadion	k1gInSc1	stadion
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
1500	[number]	k4	1500
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
HC	HC	kA	HC
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
2	[number]	k4	2
<g/>
.	.	kIx.	.
českou	český	k2eAgFnSc4d1	Česká
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
AC	AC	kA	AC
Slovácká	slovácký	k2eAgFnSc1d1	Slovácká
Slávia	Slávia	k1gFnSc1	Slávia
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
velkou	velká	k1gFnSc4	velká
a	a	k8xC	a
bohatou	bohatý	k2eAgFnSc4d1	bohatá
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
atletika	atletika	k1gFnSc1	atletika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
kořeny	kořen	k1gInPc1	kořen
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
několik	několik	k4yIc1	několik
atletů	atlet	k1gMnPc2	atlet
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
úrovně	úroveň	k1gFnSc2	úroveň
jako	jako	k8xS	jako
např.	např.	kA	např.
Dana	Dana	k1gFnSc1	Dana
Zátopková	Zátopková	k1gFnSc1	Zátopková
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tělocvičná	tělocvičný	k2eAgFnSc1d1	Tělocvičná
jednota	jednota	k1gFnSc1	jednota
Sokol	Sokol	k1gMnSc1	Sokol
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
T.	T.	kA	T.
J.	J.	kA	J.
Sokol	Sokol	k1gMnSc1	Sokol
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
500	[number]	k4	500
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mohou	moct	k5eAaImIp3nP	moct
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
různá	různý	k2eAgNnPc4d1	různé
cvičení	cvičení	k1gNnSc4	cvičení
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
jak	jak	k8xS	jak
na	na	k7c4	na
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
průpravu	průprava	k1gFnSc4	průprava
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
sporty	sport	k1gInPc4	sport
(	(	kIx(	(
<g/>
volejbal	volejbal	k1gInSc4	volejbal
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgInSc4d1	stolní
tenis	tenis	k1gInSc4	tenis
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc4	basketbal
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
rozmanitou	rozmanitý	k2eAgFnSc7d1	rozmanitá
krajinou	krajina	k1gFnSc7	krajina
a	a	k8xC	a
přírodními	přírodní	k2eAgFnPc7d1	přírodní
rezervacemi	rezervace	k1gFnPc7	rezervace
<g/>
,	,	kIx,	,
řekou	řeka	k1gFnSc7	řeka
Moravou	Morava	k1gFnSc7	Morava
s	s	k7c7	s
rozsáhlými	rozsáhlý	k2eAgInPc7d1	rozsáhlý
lužními	lužní	k2eAgInPc7d1	lužní
lesy	les	k1gInPc7	les
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
flórou	flóra	k1gFnSc7	flóra
a	a	k8xC	a
faunou	fauna	k1gFnSc7	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
je	být	k5eAaImIp3nS	být
technická	technický	k2eAgFnSc1d1	technická
stavba	stavba	k1gFnSc1	stavba
Baťova	Baťův	k2eAgInSc2d1	Baťův
kanálu	kanál	k1gInSc2	kanál
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
podmanivá	podmanivý	k2eAgNnPc1d1	podmanivé
pohoří	pohoří	k1gNnPc1	pohoří
<g/>
,	,	kIx,	,
Chřiby	Chřiby	k1gInPc1	Chřiby
a	a	k8xC	a
Bílé	bílý	k2eAgInPc1d1	bílý
Karpaty	Karpaty	k1gInPc1	Karpaty
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
biotopem	biotop	k1gInSc7	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Nejzápadnějším	západní	k2eAgInSc7d3	nejzápadnější
výběžkem	výběžek	k1gInSc7	výběžek
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jejich	jejich	k3xOp3gFnSc2	jejich
části	část	k1gFnSc2	část
-	-	kIx~	-
Vizovických	vizovický	k2eAgInPc2d1	vizovický
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
návrší	návrš	k1gFnSc7	návrš
Černá	černý	k2eAgFnSc1d1	černá
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
302,2	[number]	k4	302,2
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnSc3d1	známá
spíše	spíše	k9	spíše
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Rochus	Rochus	k1gInSc1	Rochus
podle	podle	k7c2	podle
kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Rocha	Roch	k1gMnSc2	Roch
nad	nad	k7c7	nad
Mařaticemi	Mařatice	k1gFnPc7	Mařatice
na	na	k7c6	na
východě	východ	k1gInSc6	východ
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
románská	románský	k2eAgFnSc1d1	románská
bazilika	bazilika	k1gFnSc1	bazilika
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
a	a	k8xC	a
středověké	středověký	k2eAgNnSc1d1	středověké
lapidárium	lapidárium	k1gNnSc1	lapidárium
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Buchlovice	Buchlovice	k1gFnPc1	Buchlovice
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
Buchlov	Buchlov	k1gInSc1	Buchlov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrabě	hrabě	k1gMnSc1	hrabě
Bedřich	Bedřich	k1gMnSc1	Bedřich
Berchtold	Berchtold	k1gMnSc1	Berchtold
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Leopold	Leopold	k1gMnSc1	Leopold
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
přírodovědnou	přírodovědný	k2eAgFnSc7d1	přírodovědná
sbírkou	sbírka	k1gFnSc7	sbírka
<g/>
,	,	kIx,	,
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
mumii	mumie	k1gFnSc4	mumie
atd.	atd.	kA	atd.
Bridgwater	Bridgwater	k1gInSc1	Bridgwater
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Krosno	krosna	k1gFnSc5	krosna
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Mayen	Mayna	k1gFnPc2	Mayna
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
Priverno	Priverna	k1gFnSc5	Priverna
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Skalica	Skalica	k1gFnSc1	Skalica
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Balaštík	Balaštík	k1gMnSc1	Balaštík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Daniel	Daniel	k1gMnSc1	Daniel
Bambas	Bambas	k1gMnSc1	Bambas
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Bařinka	Bařinka	k1gFnSc1	Bařinka
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Moravských	moravský	k2eAgInPc2d1	moravský
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
PSP	PSP	kA	PSP
ČR	ČR	kA	ČR
Jan	Jan	k1gMnSc1	Jan
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
<g/>
,	,	kIx,	,
dědic	dědic	k1gMnSc1	dědic
a	a	k8xC	a
pokračovatel	pokračovatel	k1gMnSc1	pokračovatel
obuvnické	obuvnický	k2eAgFnSc2d1	obuvnická
firmy	firma	k1gFnSc2	firma
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
mecenáš	mecenáš	k1gMnSc1	mecenáš
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
měst	město	k1gNnPc2	město
a	a	k8xC	a
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kandidát	kandidát	k1gMnSc1	kandidát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
Bedřich	Bedřich	k1gMnSc1	Bedřich
Beneš	Beneš	k1gMnSc1	Beneš
Buchlovan	Buchlovan	k1gMnSc1	Buchlovan
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
polštiny	polština	k1gFnSc2	polština
<g/>
,	,	kIx,	,
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
knihovník	knihovník	k1gMnSc1	knihovník
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Beneše	Beneš	k1gMnSc2	Beneš
Buchlovana	Buchlovan	k1gMnSc2	Buchlovan
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Východ	východ	k1gInSc4	východ
<g/>
)	)	kIx)	)
Radim	Radim	k1gMnSc1	Radim
Bičánek	Bičánek	k1gMnSc1	Bičánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
Robert	Robert	k1gMnSc1	Robert
Býček	býček	k1gMnSc1	býček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
kickboxu	kickbox	k1gInSc6	kickbox
Slavomil	Slavomil	k1gMnSc1	Slavomil
Ctibor	Ctibor	k1gMnSc1	Ctibor
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
evangelický	evangelický	k2eAgMnSc1d1	evangelický
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
biblista	biblista	k1gMnSc1	biblista
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
Hana	Hana	k1gFnSc1	Hana
Doupovcová	Doupovcový	k2eAgFnSc1d1	Doupovcová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
senátorka	senátorka	k1gFnSc1	senátorka
Ludvík	Ludvík	k1gMnSc1	Ludvík
Hovorka	Hovorka	k1gMnSc1	Hovorka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Štěpán	Štěpán	k1gMnSc1	Štěpán
Hulík	Hulík	k1gMnSc1	Hulík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Chalabala	Chalabal	k1gMnSc2	Chalabal
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
houslista	houslista	k1gMnSc1	houslista
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
operní	operní	k2eAgMnSc1d1	operní
dirigent	dirigent	k1gMnSc1	dirigent
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kadlec	Kadlec	k1gMnSc1	Kadlec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
vicemistrů	vicemistr	k1gMnPc2	vicemistr
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Petr	Petr	k1gMnSc1	Petr
Karlík	Karlík	k1gMnSc1	Karlík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bohemista	bohemista	k1gMnSc1	bohemista
<g/>
,	,	kIx,	,
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
syntax	syntax	k1gFnSc4	syntax
Josef	Josef	k1gMnSc1	Josef
Krofta	Krofta	k1gMnSc1	Krofta
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1943	[number]	k4	1943
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelník	divadelník	k1gMnSc1	divadelník
Ruda	Ruda	k1gMnSc1	Ruda
Kubíček	Kubíček	k1gMnSc1	Kubíček
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
pracovník	pracovník	k1gMnSc1	pracovník
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
esperantista	esperantista	k1gMnSc1	esperantista
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Rudy	ruda	k1gFnSc2	ruda
Kubíčka	kubíčko	k1gNnSc2	kubíčko
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Východ	východ	k1gInSc4	východ
<g/>
)	)	kIx)	)
Otakar	Otakar	k1gMnSc1	Otakar
Levý	levý	k2eAgMnSc1d1	levý
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
romanista	romanista	k1gMnSc1	romanista
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
František	František	k1gMnSc1	František
Nábělek	Nábělek	k1gMnSc1	Nábělek
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
Miroslav	Miroslav	k1gMnSc1	Miroslav
Náplava	náplava	k1gFnSc1	náplava
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
osobnostmi	osobnost	k1gFnPc7	osobnost
jako	jako	k9	jako
Thor	Thor	k1gMnSc1	Thor
Heyerdahl	Heyerdahl	k1gMnSc1	Heyerdahl
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
von	von	k1gInSc1	von
Däniken	Däniken	k2eAgMnSc1d1	Däniken
a	a	k8xC	a
Arthur	Arthur	k1gMnSc1	Arthur
C.	C.	kA	C.
Clarke	Clark	k1gFnPc1	Clark
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Jan	Jan	k1gMnSc1	Jan
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
kosmolog	kosmolog	k1gMnSc1	kosmolog
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
Erik	Erik	k1gMnSc1	Erik
Pardus	pardus	k1gInSc1	pardus
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g />
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Pavlica	Pavlica	k1gMnSc1	Pavlica
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
interpret	interpret	k1gMnSc1	interpret
moravského	moravský	k2eAgInSc2d1	moravský
folkloru	folklor	k1gInSc2	folklor
<g/>
,	,	kIx,	,
primáš	primáš	k1gMnSc1	primáš
souboru	soubor	k1gInSc2	soubor
Hradišťan	Hradišťan	k1gMnSc1	Hradišťan
Iva	Iva	k1gFnSc1	Iva
Pazderková	Pazderková	k1gFnSc1	Pazderková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
bavička	bavička	k1gFnSc1	bavička
<g/>
,	,	kIx,	,
moderátorka	moderátorka	k1gFnSc1	moderátorka
a	a	k8xC	a
modelka	modelka	k1gFnSc1	modelka
Antonín	Antonín	k1gMnSc1	Antonín
Pecen	pecen	k1gInSc1	pecen
(	(	kIx(	(
<g/>
Tony	Tony	k1gFnSc1	Tony
Peckham	Peckham	k1gInSc1	Peckham
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
2	[number]	k4	2
<g/>
.	.	kIx.	.
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
R.	R.	kA	R.
<g/>
A.	A.	kA	A.
<g/>
F.	F.	kA	F.
<g/>
,	,	kIx,	,
Air	Air	k1gFnSc1	Air
Gunner	Gunner	k1gInSc1	Gunner
311	[number]	k4	311
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
bombardovací	bombardovací	k2eAgFnSc1d1	bombardovací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Popelka	Popelka	k1gMnSc1	Popelka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
<g/>
,	,	kIx,	,
mediálně	mediálně	k6eAd1	mediálně
známý	známý	k2eAgMnSc1d1	známý
představitel	představitel	k1gMnSc1	představitel
tzv.	tzv.	kA	tzv.
Holešovské	holešovský	k2eAgFnSc2d1	Holešovská
výzvy	výzva	k1gFnSc2	výzva
Adolf	Adolf	k1gMnSc1	Adolf
Promber	Promber	k1gMnSc1	Promber
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
českoněmecký	českoněmecký	k2eAgMnSc1d1	českoněmecký
advokát	advokát	k1gMnSc1	advokát
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
Moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
Alois	Alois	k1gMnSc1	Alois
Pražák	Pražák	k1gMnSc1	Pražák
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
baron	baron	k1gMnSc1	baron
<g/>
,	,	kIx,	,
JUDr.	JUDr.	kA	JUDr.
<g/>
,	,	kIx,	,
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
moravský	moravský	k2eAgMnSc1d1	moravský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
roboty	robota	k1gFnSc2	robota
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
Jindřich	Jindřich	k1gMnSc1	Jindřich
Prucha	Prucha	k1gMnSc1	Prucha
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
impresionista	impresionista	k1gMnSc1	impresionista
Břetislav	Břetislav	k1gMnSc1	Břetislav
Rychlík	Rychlík	k1gMnSc1	Rychlík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Josef	Josef	k1gMnSc1	Josef
Schaniak	Schaniak	k1gMnSc1	Schaniak
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
-	-	kIx~	-
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
barevné	barevný	k2eAgFnSc2d1	barevná
fasády	fasáda	k1gFnSc2	fasáda
a	a	k8xC	a
sgrafita	sgrafito	k1gNnSc2	sgrafito
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Václav	Václav	k1gMnSc1	Václav
Staněk	Staněk	k1gMnSc1	Staněk
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1975	[number]	k4	1975
první	první	k4xOgInSc4	první
<g />
.	.	kIx.	.
</s>
<s>
primáš	primáš	k1gMnSc1	primáš
původního	původní	k2eAgInSc2d1	původní
souboru	soubor	k1gInSc2	soubor
Hradišťan	Hradišťan	k1gMnSc1	Hradišťan
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Staňka	Staněk	k1gMnSc2	Staněk
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
Věra	Věra	k1gFnSc1	Věra
Suková	Suková	k1gFnSc1	Suková
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
Stanislav	Stanislav	k1gMnSc1	Stanislav
Šácha	Šácha	k?	Šácha
<g/>
,	,	kIx,	,
Wireless	Wireless	k1gInSc1	Wireless
Operator	Operator	k1gInSc1	Operator
311	[number]	k4	311
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
bombardovací	bombardovací	k2eAgFnSc1d1	bombardovací
peruť	peruť	k1gFnSc1	peruť
RAF	raf	k0	raf
Štěpán	Štěpán	k1gMnSc1	Štěpán
Urban	Urban	k1gMnSc1	Urban
(	(	kIx(	(
<g/>
*	*	kIx~	*
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
pěvecké	pěvecký	k2eAgFnSc6d1	pěvecká
soutěži	soutěž	k1gFnSc6	soutěž
Česko-Slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
SuperStar	superstar	k1gFnSc1	superstar
2015	[number]	k4	2015
Vladislav	Vladislava	k1gFnPc2	Vladislava
Vaculka	Vaculka	k1gFnSc1	Vaculka
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
keramik	keramik	k1gMnSc1	keramik
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Vladislava	Vladislav	k1gMnSc2	Vladislav
Vaculky	Vaculka	k1gFnSc2	Vaculka
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
Ida	Ida	k1gFnSc1	Ida
Vaculková	Vaculková	k1gFnSc1	Vaculková
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
<g/>
,	,	kIx,	,
sochařka	sochařka	k1gFnSc1	sochařka
a	a	k8xC	a
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
malíře	malíř	k1gMnSc2	malíř
Vladislava	Vladislav	k1gMnSc2	Vladislav
Vaculky	Vaculka	k1gFnSc2	Vaculka
Miki	Mik	k1gFnSc2	Mik
Volek	Volek	k1gMnSc1	Volek
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
rockový	rockový	k2eAgMnSc1d1	rockový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
muzikant	muzikant	k1gMnSc1	muzikant
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Vybíral	Vybíral	k1gMnSc1	Vybíral
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
profesor	profesor	k1gMnSc1	profesor
psychologie	psychologie	k1gFnSc2	psychologie
Antonín	Antonín	k1gMnSc1	Antonín
Zelnitius	Zelnitius	k1gMnSc1	Zelnitius
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
staroměstský	staroměstský	k2eAgMnSc1d1	staroměstský
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
kronikář	kronikář	k1gMnSc1	kronikář
a	a	k8xC	a
varhaník	varhaník	k1gMnSc1	varhaník
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
archeolog	archeolog	k1gMnSc1	archeolog
a	a	k8xC	a
světoběžník	světoběžník	k1gMnSc1	světoběžník
<g/>
,	,	kIx,	,
významně	významně	k6eAd1	významně
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
archeologický	archeologický	k2eAgInSc4d1	archeologický
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
záchranu	záchrana	k1gFnSc4	záchrana
velkomoravských	velkomoravský	k2eAgFnPc2d1	Velkomoravská
památek	památka	k1gFnPc2	památka
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
Antonín	Antonín	k1gMnSc1	Antonín
Baťa	Baťa	k1gMnSc1	Baťa
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
žil	žít	k5eAaImAgInS	žít
a	a	k8xC	a
podnikal	podnikat	k5eAaImAgInS	podnikat
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
firmu	firma	k1gFnSc4	firma
Tomáš	Tomáš	k1gMnSc1	Tomáš
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
-	-	kIx~	-
Rybárny	rybárna	k1gFnSc2	rybárna
žil	žít	k5eAaImAgMnS	žít
zde	zde	k6eAd1	zde
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1886	[number]	k4	1886
až	až	k9	až
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
10	[number]	k4	10
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g />
.	.	kIx.	.
</s>
<s>
Baťa	Baťa	k1gMnSc1	Baťa
ml.	ml.	kA	ml.
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
akademii	akademie	k1gFnSc6	akademie
Milan	Milan	k1gMnSc1	Milan
Blahynka	Blahynka	k1gFnSc1	Blahynka
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
Otakar	Otakar	k1gMnSc1	Otakar
Borůvka	Borůvka	k1gMnSc1	Borůvka
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
Bruno	Bruno	k1gMnSc1	Bruno
ze	z	k7c2	z
Schauenburku	Schauenburk	k1gInSc2	Schauenburk
<g/>
,	,	kIx,	,
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
diplomat	diplomat	k1gMnSc1	diplomat
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
respektu	respekt	k1gInSc2	respekt
a	a	k8xC	a
věhlasu	věhlas	k1gInSc2	věhlas
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
několik	několik	k4yIc4	několik
staveb	stavba	k1gFnPc2	stavba
Felix	Felix	k1gMnSc1	Felix
Kadlinský	Kadlinský	k2eAgMnSc1d1	Kadlinský
<g/>
,	,	kIx,	,
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
mnich	mnich	k1gMnSc1	mnich
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
<g/>
,	,	kIx,	,
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
feudál	feudál	k1gMnSc1	feudál
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
věnoval	věnovat	k5eAaPmAgInS	věnovat
postavu	postava	k1gFnSc4	postava
rytíře	rytíř	k1gMnSc2	rytíř
František	František	k1gMnSc1	František
Kožík	Kožík	k1gMnSc1	Kožík
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
esperantista	esperantista	k1gMnSc1	esperantista
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
František	František	k1gMnSc1	František
Kretz	Kretz	k1gMnSc1	Kretz
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
,	,	kIx,	,
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
Národopisné	národopisný	k2eAgNnSc4d1	Národopisné
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaPmAgMnS	věnovat
mu	on	k3xPp3gMnSc3	on
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nakoupil	nakoupit	k5eAaPmAgInS	nakoupit
z	z	k7c2	z
vlastních	vlastní	k2eAgInPc2d1	vlastní
prostředků	prostředek	k1gInPc2	prostředek
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
vesnicích	vesnice	k1gFnPc6	vesnice
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
rytíř	rytíř	k1gMnSc1	rytíř
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
velký	velký	k2eAgMnSc1d1	velký
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
císaře	císař	k1gMnPc4	císař
Říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
I.	I.	kA	I.
<g/>
,	,	kIx,	,
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
feudál	feudál	k1gMnSc1	feudál
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
Veligrad	Veligrada	k1gFnPc2	Veligrada
někdy	někdy	k6eAd1	někdy
ztotožňováno	ztotožňován	k2eAgNnSc1d1	ztotožňováno
s	s	k7c7	s
městem	město	k1gNnSc7	město
sv.	sv.	kA	sv.
Konstantin	Konstantin	k1gMnSc1	Konstantin
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
písma	písmo	k1gNnSc2	písmo
hlaholice	hlaholice	k1gFnSc2	hlaholice
Svatý	svatý	k2eAgMnSc1d1	svatý
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
autorství	autorství	k1gNnSc3	autorství
cyrilice	cyrilice	k1gFnSc2	cyrilice
<g/>
,	,	kIx,	,
působiště	působiště	k1gNnSc1	působiště
na	na	k7c6	na
Špitálkách	Špitálkách	k?	Špitálkách
Sadech	sad	k1gInPc6	sad
Antonín	Antonín	k1gMnSc1	Antonín
Šuránek	Šuránek	k1gMnSc1	Šuránek
<g/>
,	,	kIx,	,
spirituál	spirituál	k1gMnSc1	spirituál
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kněžského	kněžský	k2eAgInSc2d1	kněžský
semináře	seminář	k1gInSc2	seminář
<g/>
,	,	kIx,	,
Služebník	služebník	k1gMnSc1	služebník
Boží	boží	k2eAgFnSc2d1	boží
<g/>
,	,	kIx,	,
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
studoval	studovat	k5eAaImAgMnS	studovat
a	a	k8xC	a
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
Tas	tasit	k5eAaPmRp2nS	tasit
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Jana	Jana	k1gFnSc1	Jana
Werichová	Werichová	k1gFnSc1	Werichová
<g/>
,	,	kIx,	,
začínala	začínat	k5eAaImAgFnS	začínat
ve	v	k7c6	v
Slováckém	slovácký	k2eAgNnSc6d1	Slovácké
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1958	[number]	k4	1958
</s>
