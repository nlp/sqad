<s>
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Ungarisch	Ungarisch	k1gMnSc1	Ungarisch
Hradisch	Hradisch	k1gMnSc1	Hradisch
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Magyarhradis	Magyarhradis	k1gFnSc1	Magyarhradis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
23	[number]	k4	23
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Zlína	Zlín	k1gInSc2	Zlín
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
