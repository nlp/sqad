<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
má	mít	k5eAaImIp3nS	mít
965	[number]	k4	965
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Alžírskem	Alžírsko	k1gNnSc7	Alžírsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
459	[number]	k4	459
km	km	kA	km
s	s	k7c7	s
Libyí	Libye	k1gFnSc7	Libye
a	a	k8xC	a
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
a	a	k8xC	a
severu	sever	k1gInSc2	sever
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc3	jeho
pobřeží	pobřeží	k1gNnSc3	pobřeží
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
148	[number]	k4	148
km	km	kA	km
omýváno	omýván	k2eAgNnSc1d1	omýváno
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
