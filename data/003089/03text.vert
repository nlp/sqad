<s>
Tunisko	Tunisko	k1gNnSc1	Tunisko
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
Tuniská	tuniský	k2eAgFnSc1d1	Tuniská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
pojmenování	pojmenování	k1gNnSc2	pojmenování
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Tunisu	Tunis	k1gInSc2	Tunis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
163	[number]	k4	163
610	[number]	k4	610
km2	km2	k4	km2
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
přes	přes	k7c4	přes
10,8	[number]	k4	10,8
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tunisko	Tunisko	k1gNnSc1	Tunisko
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgInPc2d1	pouhý
150	[number]	k4	150
km	km	kA	km
od	od	k7c2	od
Sicílie	Sicílie	k1gFnSc2	Sicílie
a	a	k8xC	a
tedy	tedy	k9	tedy
od	od	k7c2	od
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
má	mít	k5eAaImIp3nS	mít
965	[number]	k4	965
km	km	kA	km
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Alžírskem	Alžírsko	k1gNnSc7	Alžírsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
459	[number]	k4	459
km	km	kA	km
s	s	k7c7	s
Libyí	Libye	k1gFnSc7	Libye
a	a	k8xC	a
ze	z	k7c2	z
severovýchodu	severovýchod	k1gInSc2	severovýchod
a	a	k8xC	a
severu	sever	k1gInSc2	sever
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc3	jeho
pobřeží	pobřeží	k1gNnSc3	pobřeží
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
148	[number]	k4	148
km	km	kA	km
omýváno	omýván	k2eAgNnSc1d1	omýváno
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
pohořím	pohoří	k1gNnSc7	pohoří
je	být	k5eAaImIp3nS	být
masiv	masiv	k1gInSc1	masiv
Dorsale	Dorsala	k1gFnSc3	Dorsala
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
pokračováním	pokračování	k1gNnSc7	pokračování
pohoří	pohoří	k1gNnSc2	pohoří
Atlas	Atlas	k1gInSc1	Atlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
poblíž	poblíž	k7c2	poblíž
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Alžírskem	Alžírsko	k1gNnSc7	Alžírsko
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
tuniská	tuniský	k2eAgFnSc1d1	Tuniská
hora	hora	k1gFnSc1	hora
Džabal	Džabal	k1gInSc1	Džabal
aš-Šánabí	aš-Šánabit	k5eAaPmIp3nS	aš-Šánabit
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
1	[number]	k4	1
544	[number]	k4	544
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
Severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
země	zem	k1gFnSc2	zem
vévodí	vévodit	k5eAaImIp3nP	vévodit
náhorní	náhorní	k2eAgFnPc4d1	náhorní
pláně	pláň	k1gFnPc4	pláň
a	a	k8xC	a
úrodné	úrodný	k2eAgNnSc4d1	úrodné
údolí	údolí	k1gNnSc4	údolí
řeky	řeka	k1gFnSc2	řeka
Medžerda	Medžerda	k1gMnSc1	Medžerda
(	(	kIx(	(
<g/>
Medjerda	Medjerda	k1gMnSc1	Medjerda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Medžerda	Medžerda	k1gFnSc1	Medžerda
pramení	pramenit	k5eAaImIp3nS	pramenit
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
stálou	stálý	k2eAgFnSc7d1	stálá
řekou	řeka	k1gFnSc7	řeka
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
(	(	kIx(	(
<g/>
ostatní	ostatní	k2eAgFnPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
sezónní	sezónní	k2eAgFnPc1d1	sezónní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
horského	horský	k2eAgInSc2d1	horský
masivu	masiv	k1gInSc2	masiv
Dorsale	Dorsala	k1gFnSc6	Dorsala
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
vyprahlé	vyprahlý	k2eAgFnPc1d1	vyprahlá
<g/>
,	,	kIx,	,
neúrodné	úrodný	k2eNgFnPc1d1	neúrodná
stepi	step	k1gFnPc1	step
<g/>
,	,	kIx,	,
přecházející	přecházející	k2eAgFnPc1d1	přecházející
v	v	k7c4	v
šotty	šotta	k1gFnPc4	šotta
(	(	kIx(	(
<g/>
solná	solný	k2eAgNnPc4d1	solné
jezera	jezero	k1gNnPc4	jezero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižněji	jižně	k6eAd2	jižně
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
poušť	poušť	k1gFnSc1	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Tunisko	Tunisko	k1gNnSc1	Tunisko
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
subtropickém	subtropický	k2eAgInSc6d1	subtropický
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Horké	Horké	k2eAgNnSc1d1	Horké
suché	suchý	k2eAgNnSc1d1	suché
léto	léto	k1gNnSc1	léto
zde	zde	k6eAd1	zde
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgNnSc1d1	zimní
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
,	,	kIx,	,
chladné	chladný	k2eAgInPc1d1	chladný
proudy	proud	k1gInPc1	proud
z	z	k7c2	z
Atlantiku	Atlantik	k1gInSc2	Atlantik
přináší	přinášet	k5eAaImIp3nS	přinášet
vítr	vítr	k1gInSc1	vítr
a	a	k8xC	a
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
i	i	k9	i
jaro	jaro	k1gNnSc4	jaro
a	a	k8xC	a
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
mnohem	mnohem	k6eAd1	mnohem
kratší	krátký	k2eAgInPc1d2	kratší
než	než	k8xS	než
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
vane	vanout	k5eAaImIp3nS	vanout
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
suchý	suchý	k2eAgInSc1d1	suchý
<g/>
,	,	kIx,	,
horký	horký	k2eAgInSc1d1	horký
vítr	vítr	k1gInSc1	vítr
scirocco	scirocco	k1gNnSc1	scirocco
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
5-6	[number]	k4	5-6
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
33	[number]	k4	33
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
je	být	k5eAaImIp3nS	být
horké	horký	k2eAgNnSc1d1	horké
<g/>
,	,	kIx,	,
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
až	až	k9	až
50	[number]	k4	50
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
měsíce	měsíc	k1gInPc1	měsíc
pro	pro	k7c4	pro
návštěvu	návštěva	k1gFnSc4	návštěva
jsou	být	k5eAaImIp3nP	být
konec	konec	k1gInSc4	konec
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
přelom	přelom	k1gInSc1	přelom
září	září	k1gNnSc2	září
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Tunisko	Tunisko	k1gNnSc1	Tunisko
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
Féničany	Féničan	k1gMnPc4	Féničan
ve	v	k7c4	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
velký	velký	k2eAgInSc1d1	velký
městský	městský	k2eAgInSc1d1	městský
stát	stát	k1gInSc1	stát
Kartágo	Kartágo	k1gNnSc1	Kartágo
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
fénického	fénický	k2eAgInSc2d1	fénický
názvu	název	k1gInSc2	název
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgNnSc4d1	Nové
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
ovládal	ovládat	k5eAaImAgInS	ovládat
většinu	většina	k1gFnSc4	většina
západního	západní	k2eAgNnSc2d1	západní
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
punské	punský	k2eAgFnPc1d1	punská
války	válka	k1gFnPc1	válka
mezi	mezi	k7c7	mezi
Římem	Řím	k1gInSc7	Řím
a	a	k8xC	a
Kartágem	Kartágo	k1gNnSc7	Kartágo
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
byla	být	k5eAaImAgFnS	být
nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
římským	římský	k2eAgMnSc7d1	římský
vojevůdcem	vojevůdce	k1gMnSc7	vojevůdce
Scipionem	Scipion	k1gInSc7	Scipion
Africanem	African	k1gInSc7	African
proti	proti	k7c3	proti
Hannibalovi	Hannibalův	k2eAgMnPc1d1	Hannibalův
<g/>
)	)	kIx)	)
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
kompletnímu	kompletní	k2eAgNnSc3d1	kompletní
zničení	zničení	k1gNnSc3	zničení
Kartága	Kartágo	k1gNnSc2	Kartágo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
146	[number]	k4	146
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
439	[number]	k4	439
<g/>
-	-	kIx~	-
<g/>
533	[number]	k4	533
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
území	území	k1gNnSc4	území
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Vandalové	Vandal	k1gMnPc1	Vandal
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Kartágo	Kartágo	k1gNnSc4	Kartágo
částí	část	k1gFnPc2	část
římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
než	než	k8xS	než
ho	on	k3xPp3gMnSc4	on
zabrali	zabrat	k5eAaPmAgMnP	zabrat
Arabové	Arab	k1gMnPc1	Arab
(	(	kIx(	(
<g/>
648	[number]	k4	648
<g/>
-	-	kIx~	-
<g/>
669	[number]	k4	669
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vládly	vládnout	k5eAaImAgInP	vládnout
zde	zde	k6eAd1	zde
různé	různý	k2eAgInPc4d1	různý
arabské	arabský	k2eAgInPc4d1	arabský
a	a	k8xC	a
berberské	berberský	k2eAgFnPc4d1	berberská
dynastie	dynastie	k1gFnPc4	dynastie
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgMnPc4d1	následovaný
Turky	Turek	k1gMnPc4	Turek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zabrali	zabrat	k5eAaPmAgMnP	zabrat
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1570	[number]	k4	1570
<g/>
-	-	kIx~	-
<g/>
1574	[number]	k4	1574
a	a	k8xC	a
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
učinili	učinit	k5eAaImAgMnP	učinit
součást	součást	k1gFnSc4	součást
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
základny	základna	k1gFnPc1	základna
pirátů	pirát	k1gMnPc2	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
vojáci	voják	k1gMnPc1	voják
obsadili	obsadit	k5eAaPmAgMnP	obsadit
zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
a	a	k8xC	a
tuniský	tuniský	k2eAgMnSc1d1	tuniský
bej	bej	k1gMnSc1	bej
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
ustanovující	ustanovující	k2eAgInSc4d1	ustanovující
francouzský	francouzský	k2eAgInSc4d1	francouzský
protektorát	protektorát	k1gInSc4	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1942	[number]	k4	1942
a	a	k8xC	a
1943	[number]	k4	1943
probíhaly	probíhat	k5eAaImAgInP	probíhat
těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
Spojenci	spojenec	k1gMnPc7	spojenec
a	a	k8xC	a
mocnostmi	mocnost	k1gFnPc7	mocnost
Osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
skončily	skončit	k5eAaPmAgFnP	skončit
drtivou	drtivý	k2eAgFnSc7d1	drtivá
porážkou	porážka	k1gFnSc7	porážka
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Nacionalistický	nacionalistický	k2eAgInSc1d1	nacionalistický
neklid	neklid	k1gInSc1	neklid
přinutil	přinutit	k5eAaPmAgInS	přinutit
Francii	Francie	k1gFnSc4	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
uznat	uznat	k5eAaPmF	uznat
Tunisko	Tunisko	k1gNnSc1	Tunisko
jako	jako	k8xC	jako
nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ustavující	ustavující	k2eAgNnSc1d1	ustavující
shromáždění	shromáždění	k1gNnSc1	shromáždění
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1957	[number]	k4	1957
sesadilo	sesadit	k5eAaPmAgNnS	sesadit
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
sultána	sultán	k1gMnSc2	sultán
Muhammada	Muhammada	k1gFnSc1	Muhammada
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
deklarovalo	deklarovat	k5eAaBmAgNnS	deklarovat
Tunisko	Tunisko	k1gNnSc4	Tunisko
jako	jako	k8xS	jako
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
Habíba	Habíb	k1gMnSc4	Habíb
Burgibu	Burgiba	k1gMnSc4	Burgiba
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Burgiba	Burgiba	k1gFnSc1	Burgiba
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
západních	západní	k2eAgInPc2d1	západní
států	stát	k1gInPc2	stát
zaváděl	zavádět	k5eAaImAgInS	zavádět
radikální	radikální	k2eAgFnPc4d1	radikální
sociální	sociální	k2eAgFnPc4d1	sociální
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zmodernizovat	zmodernizovat	k5eAaPmF	zmodernizovat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
i	i	k8xC	i
společnost	společnost	k1gFnSc4	společnost
samotnou	samotný	k2eAgFnSc4d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Zbavil	zbavit	k5eAaPmAgMnS	zbavit
náboženské	náboženský	k2eAgMnPc4d1	náboženský
vůdce	vůdce	k1gMnPc4	vůdce
jejich	jejich	k3xOp3gInSc2	jejich
společenského	společenský	k2eAgInSc2d1	společenský
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
považoval	považovat	k5eAaImAgInS	považovat
islám	islám	k1gInSc1	islám
za	za	k7c4	za
brzdící	brzdící	k2eAgFnSc4d1	brzdící
sílu	síla	k1gFnSc4	síla
rozvoje	rozvoj	k1gInSc2	rozvoj
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
ho	on	k3xPp3gMnSc4	on
zvolilo	zvolit	k5eAaPmAgNnS	zvolit
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
doživotně	doživotně	k6eAd1	doživotně
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dramatickému	dramatický	k2eAgInSc3d1	dramatický
nárůstu	nárůst	k1gInSc3	nárůst
islámské	islámský	k2eAgFnSc2d1	islámská
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
strachu	strach	k1gInSc6	strach
z	z	k7c2	z
lidového	lidový	k2eAgNnSc2d1	lidové
povstání	povstání	k1gNnSc2	povstání
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Zín	Zín	k1gFnSc2	Zín
Abidín	Abidín	k1gInSc1	Abidín
bin	bin	k?	bin
Alí	Alí	k1gMnSc2	Alí
pokojný	pokojný	k2eAgInSc4d1	pokojný
palácový	palácový	k2eAgInSc4d1	palácový
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
prohlásit	prohlásit	k5eAaPmF	prohlásit
Burgibu	Burgiba	k1gFnSc4	Burgiba
za	za	k7c4	za
fyzicky	fyzicky	k6eAd1	fyzicky
i	i	k8xC	i
mentálně	mentálně	k6eAd1	mentálně
neschopného	schopný	k2eNgMnSc4d1	neschopný
vykonávat	vykonávat	k5eAaImF	vykonávat
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Bin	Bin	k?	Bin
Alí	Alí	k1gMnSc1	Alí
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
jak	jak	k8xS	jak
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
(	(	kIx(	(
<g/>
sekularismus	sekularismus	k1gInSc1	sekularismus
a	a	k8xC	a
politická	politický	k2eAgFnSc1d1	politická
represe	represe	k1gFnSc1	represe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
mezinárodním	mezinárodnět	k5eAaPmIp1nS	mezinárodnět
(	(	kIx(	(
<g/>
umírněná	umírněný	k2eAgFnSc1d1	umírněná
a	a	k8xC	a
prozápadní	prozápadní	k2eAgFnSc1d1	prozápadní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
potlačovala	potlačovat	k5eAaImAgFnS	potlačovat
ostatní	ostatní	k2eAgFnPc4d1	ostatní
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
cenzurovala	cenzurovat	k5eAaImAgFnS	cenzurovat
tisk	tisk	k1gInSc4	tisk
i	i	k8xC	i
internet	internet	k1gInSc4	internet
<g/>
,	,	kIx,	,
omezovala	omezovat	k5eAaImAgFnS	omezovat
náboženské	náboženský	k2eAgFnPc4d1	náboženská
svobody	svoboda	k1gFnPc4	svoboda
a	a	k8xC	a
pronásledovala	pronásledovat	k5eAaImAgFnS	pronásledovat
intelektuály	intelektuál	k1gMnPc4	intelektuál
<g/>
,	,	kIx,	,
opoziční	opoziční	k2eAgMnPc4d1	opoziční
aktivisty	aktivista	k1gMnPc4	aktivista
i	i	k8xC	i
novináře	novinář	k1gMnPc4	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
bin	bin	k?	bin
Alímu	Alím	k1gInSc2	Alím
podařilo	podařit	k5eAaPmAgNnS	podařit
počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
potlačit	potlačit	k5eAaPmF	potlačit
islámskou	islámský	k2eAgFnSc4d1	islámská
opozici	opozice	k1gFnSc4	opozice
<g/>
,	,	kIx,	,
úspěšně	úspěšně	k6eAd1	úspěšně
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
získat	získat	k5eAaPmF	získat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
<g/>
.	.	kIx.	.
</s>
<s>
Povolil	povolit	k5eAaPmAgMnS	povolit
politický	politický	k2eAgInSc4d1	politický
systém	systém	k1gInSc4	systém
více	hodně	k6eAd2	hodně
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
omezil	omezit	k5eAaPmAgInS	omezit
policejní	policejní	k2eAgFnPc4d1	policejní
represe	represe	k1gFnPc4	represe
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
politické	politický	k2eAgMnPc4d1	politický
emigranty	emigrant	k1gMnPc4	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
Tuniska	Tunisko	k1gNnSc2	Tunisko
již	již	k6eAd1	již
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
umírněnost	umírněnost	k1gFnSc1	umírněnost
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
dialog	dialog	k1gInSc4	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Bin	Bin	k?	Bin
Alí	Alí	k1gFnSc1	Alí
několikrát	několikrát	k6eAd1	několikrát
za	za	k7c7	za
sebou	se	k3xPyFc7	se
znovu	znovu	k6eAd1	znovu
legitimizoval	legitimizovat	k5eAaBmAgMnS	legitimizovat
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
a	a	k8xC	a
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
probíhaly	probíhat	k5eAaImAgFnP	probíhat
nebo	nebo	k8xC	nebo
probíhají	probíhat	k5eAaImIp3nP	probíhat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
protesty	protest	k1gInPc1	protest
<g/>
,	,	kIx,	,
nepokoje	nepokoj	k1gInPc1	nepokoj
<g/>
,	,	kIx,	,
povstání	povstání	k1gNnPc1	povstání
a	a	k8xC	a
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nazývány	nazývat	k5eAaImNgFnP	nazývat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
arabské	arabský	k2eAgNnSc1d1	arabské
jaro	jaro	k1gNnSc1	jaro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
nepokoje	nepokoj	k1gInPc1	nepokoj
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
a	a	k8xC	a
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
prezident	prezident	k1gMnSc1	prezident
bin	bin	k?	bin
Alí	Alí	k1gMnSc1	Alí
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
prchl	prchnout	k5eAaPmAgMnS	prchnout
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgMnS	chtít
utéci	utéct	k5eAaPmF	utéct
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
mu	on	k3xPp3gMnSc3	on
nepovolil	povolit	k5eNaPmAgMnS	povolit
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nakrátko	nakrátko	k6eAd1	nakrátko
premiér	premiér	k1gMnSc1	premiér
Muhammad	Muhammad	k1gInSc4	Muhammad
Ghannúší	Ghannúší	k1gNnSc2	Ghannúší
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Fuád	Fuád	k1gMnSc1	Fuád
Mebazá	Mebazá	k1gMnSc1	Mebazá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
uklidnila	uklidnit	k5eAaPmAgFnS	uklidnit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
dobré	dobrý	k2eAgFnSc6d1	dobrá
úrovni	úroveň	k1gFnSc6	úroveň
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
a	a	k8xC	a
jižních	jižní	k2eAgFnPc6d1	jižní
částech	část	k1gFnPc6	část
Tuniska	Tunisko	k1gNnSc2	Tunisko
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
teroristické	teroristický	k2eAgFnPc1d1	teroristická
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
teroristickému	teroristický	k2eAgInSc3d1	teroristický
útoku	útok	k1gInSc3	útok
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
Bardo	Bardo	k1gNnSc4	Bardo
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Tunisu	Tunis	k1gInSc2	Tunis
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
v	v	k7c6	v
Sousse	Soussa	k1gFnSc6	Soussa
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
činila	činit	k5eAaImAgFnS	činit
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
zhruba	zhruba	k6eAd1	zhruba
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
mělo	mít	k5eAaImAgNnS	mít
Tunisko	Tunisko	k1gNnSc1	Tunisko
4,2	[number]	k4	4,2
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
etnickou	etnický	k2eAgFnSc7d1	etnická
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
asi	asi	k9	asi
98	[number]	k4	98
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
arabsko-berberští	arabskoerberský	k2eAgMnPc1d1	arabsko-berberský
míšenci	míšenec	k1gMnPc1	míšenec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
čistě	čistě	k6eAd1	čistě
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
populace	populace	k1gFnSc2	populace
tvoří	tvořit	k5eAaImIp3nP	tvořit
Berbeři	Berber	k1gMnPc1	Berber
<g/>
,	,	kIx,	,
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
oblastmi	oblast	k1gFnPc7	oblast
berberského	berberský	k2eAgInSc2d1	berberský
osídlení	osídlení	k1gNnSc3	osídlení
jsou	být	k5eAaImIp3nP	být
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
Džerba	Džerba	k1gFnSc1	Džerba
<g/>
,	,	kIx,	,
horské	horský	k2eAgFnSc2d1	horská
osady	osada	k1gFnSc2	osada
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Tunisku	Tunisko	k1gNnSc6	Tunisko
a	a	k8xC	a
několik	několik	k4yIc1	několik
starých	starý	k2eAgFnPc2d1	stará
berberských	berberský	k2eAgFnPc2d1	berberská
vesnic	vesnice	k1gFnPc2	vesnice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
ksáry	ksára	k1gFnSc2	ksára
<g/>
)	)	kIx)	)
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
jihotuniského	jihotuniský	k2eAgNnSc2d1	jihotuniský
města	město	k1gNnSc2	město
Tatáwín	Tatáwína	k1gFnPc2	Tatáwína
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
věkové	věkový	k2eAgFnSc2d1	věková
struktury	struktura	k1gFnSc2	struktura
je	být	k5eAaImIp3nS	být
22,7	[number]	k4	22,7
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
70,1	[number]	k4	70,1
%	%	kIx~	%
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
a	a	k8xC	a
7,2	[number]	k4	7,2
%	%	kIx~	%
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
nad	nad	k7c4	nad
65	[number]	k4	65
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
obyvatel	obyvatel	k1gMnPc2	obyvatel
Tuniska	Tunisko	k1gNnSc2	Tunisko
je	být	k5eAaImIp3nS	být
75,78	[number]	k4	75,78
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
pak	pak	k6eAd1	pak
73,98	[number]	k4	73,98
let	léto	k1gNnPc2	léto
a	a	k8xC	a
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
77,7	[number]	k4	77,7
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
onemocněných	onemocněný	k2eAgMnPc2d1	onemocněný
HIV	HIV	kA	HIV
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
0,1	[number]	k4	0,1
%	%	kIx~	%
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
74,3	[number]	k4	74,3
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
gramotných	gramotný	k2eAgFnPc2d1	gramotná
a	a	k8xC	a
urbanizace	urbanizace	k1gFnSc1	urbanizace
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
67	[number]	k4	67
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tunisko	Tunisko	k1gNnSc1	Tunisko
je	být	k5eAaImIp3nS	být
arabská	arabský	k2eAgFnSc1d1	arabská
země	země	k1gFnSc1	země
s	s	k7c7	s
islámskou	islámský	k2eAgFnSc7d1	islámská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
98	[number]	k4	98
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Tuniska	Tunisko	k1gNnSc2	Tunisko
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
islámu	islám	k1gInSc3	islám
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
státním	státní	k2eAgNnSc7d1	státní
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
,	,	kIx,	,
zbylá	zbylý	k2eAgFnSc1d1	zbylá
2	[number]	k4	2
%	%	kIx~	%
představují	představovat	k5eAaImIp3nP	představovat
židé	žid	k1gMnPc1	žid
a	a	k8xC	a
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
95	[number]	k4	95
%	%	kIx~	%
Tunisanů	Tunisan	k1gMnPc2	Tunisan
je	být	k5eAaImIp3nS	být
ortodoxního	ortodoxní	k2eAgNnSc2d1	ortodoxní
sunnitského	sunnitský	k2eAgNnSc2d1	sunnitské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žije	žít	k5eAaImIp3nS	žít
několik	několik	k4yIc1	několik
malých	malý	k2eAgFnPc2d1	malá
komunit	komunita	k1gFnPc2	komunita
hlásících	hlásící	k2eAgFnPc2d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
muslimské	muslimský	k2eAgFnSc3d1	muslimská
sektě	sekta	k1gFnSc3	sekta
cháridža	cháridž	k1gInSc2	cháridž
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
státní	státní	k2eAgFnSc2d1	státní
náboženství	náboženství	k1gNnSc3	náboženství
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
pouze	pouze	k6eAd1	pouze
muslim	muslim	k1gMnSc1	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
také	také	k9	také
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
asi	asi	k9	asi
25	[number]	k4	25
000	[number]	k4	000
křesťanů	křesťan	k1gMnPc2	křesťan
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
okolo	okolo	k6eAd1	okolo
22	[number]	k4	22
000	[number]	k4	000
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
protestanti	protestant	k1gMnPc1	protestant
tvoří	tvořit	k5eAaImIp3nP	tvořit
menšinu	menšina	k1gFnSc4	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
1	[number]	k4	1
500	[number]	k4	500
židů	žid	k1gMnPc2	žid
žije	žít	k5eAaImIp3nS	žít
převážně	převážně	k6eAd1	převážně
kolem	kolem	k7c2	kolem
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Džerba	Džerba	k1gFnSc1	Džerba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
39	[number]	k4	39
synagog	synagoga	k1gFnPc2	synagoga
a	a	k8xC	a
komunita	komunita	k1gFnSc1	komunita
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
historii	historie	k1gFnSc4	historie
dlouho	dlouho	k6eAd1	dlouho
2500	[number]	k4	2500
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
synagog	synagoga	k1gFnPc2	synagoga
na	na	k7c6	na
světě	svět	k1gInSc6	svět
sem	sem	k6eAd1	sem
každoročně	každoročně	k6eAd1	každoročně
přichází	přicházet	k5eAaImIp3nS	přicházet
mnoho	mnoho	k4c1	mnoho
poutníků	poutník	k1gMnPc2	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
získáním	získání	k1gNnSc7	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
255	[number]	k4	255
000	[number]	k4	000
Evropanů	Evropan	k1gMnPc2	Evropan
(	(	kIx(	(
<g/>
Francouzi	Francouz	k1gMnPc1	Francouz
a	a	k8xC	a
Italové	Ital	k1gMnPc1	Ital
<g/>
)	)	kIx)	)
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
evropským	evropský	k2eAgInSc7d1	evropský
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
v	v	k7c6	v
turistických	turistický	k2eAgNnPc6d1	turistické
letoviscích	letovisko	k1gNnPc6	letovisko
rovněž	rovněž	k6eAd1	rovněž
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
italština	italština	k1gFnSc1	italština
<g/>
.	.	kIx.	.
</s>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
je	být	k5eAaImIp3nS	být
jazykem	jazyk	k1gInSc7	jazyk
obchodních	obchodní	k2eAgNnPc2d1	obchodní
jednání	jednání	k1gNnPc2	jednání
<g/>
,	,	kIx,	,
intelektuální	intelektuální	k2eAgFnPc4d1	intelektuální
komunikace	komunikace	k1gFnPc4	komunikace
a	a	k8xC	a
vyučujícím	vyučující	k2eAgInSc7d1	vyučující
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
žák	žák	k1gMnSc1	žák
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
učit	učit	k5eAaImF	učit
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnPc4	třída
arabsky	arabsky	k6eAd1	arabsky
a	a	k8xC	a
od	od	k7c2	od
třetí	třetí	k4xOgFnSc2	třetí
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
Angličtinu	angličtina	k1gFnSc4	angličtina
se	se	k3xPyFc4	se
Tunisané	Tunisan	k1gMnPc1	Tunisan
učí	učit	k5eAaImIp3nP	učit
až	až	k9	až
od	od	k7c2	od
páté	pátý	k4xOgFnSc2	pátý
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tunisané	Tunisan	k1gMnPc1	Tunisan
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
styku	styk	k1gInSc6	styk
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
mluví	mluvit	k5eAaImIp3nS	mluvit
místním	místní	k2eAgInSc7d1	místní
dialektem	dialekt	k1gInSc7	dialekt
nesprávně	správně	k6eNd1	správně
řečeno	říct	k5eAaPmNgNnS	říct
hovorové	hovorový	k2eAgFnPc4d1	hovorová
arabštiny	arabština	k1gFnSc2	arabština
"	"	kIx"	"
<g/>
tunisštinou	tunisština	k1gFnSc7	tunisština
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
spisovné	spisovný	k2eAgFnSc2d1	spisovná
arabštiny	arabština	k1gFnSc2	arabština
hodně	hodně	k6eAd1	hodně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Mluvená	mluvený	k2eAgFnSc1d1	mluvená
"	"	kIx"	"
<g/>
tuniština	tuniština	k1gFnSc1	tuniština
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
typickým	typický	k2eAgInSc7d1	typický
středomořským	středomořský	k2eAgInSc7d1	středomořský
jazykem	jazyk	k1gInSc7	jazyk
(	(	kIx(	(
<g/>
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
maltština	maltština	k1gFnSc1	maltština
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
více	hodně	k6eAd2	hodně
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
<g/>
:	:	kIx,	:
arabštiny	arabština	k1gFnPc1	arabština
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnPc1	francouzština
<g/>
,	,	kIx,	,
španělštiny	španělština	k1gFnPc1	španělština
<g/>
,	,	kIx,	,
hebrejštiny	hebrejština	k1gFnPc1	hebrejština
<g/>
,	,	kIx,	,
italštiny	italština	k1gFnPc1	italština
<g/>
,	,	kIx,	,
berberštiny	berberština	k1gFnPc1	berberština
<g/>
,	,	kIx,	,
maltštiny	maltština	k1gFnPc1	maltština
aj.	aj.	kA	aj.
Tuniská	tuniský	k2eAgFnSc1d1	Tuniská
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
diverzifikovaná	diverzifikovaný	k2eAgFnSc1d1	diverzifikovaná
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
velkých	velký	k2eAgNnPc2d1	velké
důležitých	důležitý	k2eAgNnPc2d1	důležité
odvětví	odvětví	k1gNnPc2	odvětví
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
tedy	tedy	k9	tedy
není	být	k5eNaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
sektoru	sektor	k1gInSc6	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
problémy	problém	k1gInPc7	problém
jsou	být	k5eAaImIp3nP	být
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc4	nedostatek
kvalifikovaných	kvalifikovaný	k2eAgFnPc2d1	kvalifikovaná
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
privatizace	privatizace	k1gFnSc1	privatizace
a	a	k8xC	a
liberalizace	liberalizace	k1gFnSc1	liberalizace
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
sektoru	sektor	k1gInSc2	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
vysoké	vysoký	k2eAgFnSc3d1	vysoká
nezaměstnanosti	nezaměstnanost	k1gFnSc3	nezaměstnanost
patří	patřit	k5eAaImIp3nS	patřit
tuniská	tuniský	k2eAgFnSc1d1	Tuniská
ekonomika	ekonomika	k1gFnSc1	ekonomika
mezi	mezi	k7c7	mezi
rozvojovými	rozvojový	k2eAgFnPc7d1	rozvojová
zeměmi	zem	k1gFnPc7	zem
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
k	k	k7c3	k
těm	ten	k3xDgFnPc3	ten
nejúspěšnějším	úspěšný	k2eAgFnPc3d3	nejúspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
Tunisko	Tunisko	k1gNnSc1	Tunisko
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgInSc7	první
arabským	arabský	k2eAgInSc7d1	arabský
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
připuštěn	připustit	k5eAaPmNgInS	připustit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Export	export	k1gInSc1	export
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
představoval	představovat	k5eAaImAgMnS	představovat
14,43	[number]	k4	14,43
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
vyváželo	vyvážet	k5eAaImAgNnS	vyvážet
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
,	,	kIx,	,
polozpracované	polozpracovaný	k2eAgNnSc1d1	polozpracovaný
zboží	zboží	k1gNnSc1	zboží
a	a	k8xC	a
textil	textil	k1gInSc1	textil
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
strojní	strojní	k2eAgInPc1d1	strojní
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
fosfáty	fosfát	k1gInPc1	fosfát
a	a	k8xC	a
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
a	a	k8xC	a
elektrická	elektrický	k2eAgNnPc1d1	elektrické
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
odběrateli	odběratel	k1gMnPc7	odběratel
byli	být	k5eAaImAgMnP	být
Francie	Francie	k1gFnSc1	Francie
29,6	[number]	k4	29,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
21	[number]	k4	21
%	%	kIx~	%
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
8,8	[number]	k4	8,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
5,8	[number]	k4	5,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
5	[number]	k4	5
%	%	kIx~	%
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
4,8	[number]	k4	4,8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
dovezlo	dovézt	k5eAaPmAgNnS	dovézt
do	do	k7c2	do
Tuniska	Tunisko	k1gNnSc2	Tunisko
zboží	zboží	k1gNnSc2	zboží
za	za	k7c4	za
14,43	[number]	k4	14,43
mld.	mld.	k?	mld.
USD	USD	kA	USD
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
textil	textil	k1gInSc1	textil
<g/>
,	,	kIx,	,
strojní	strojní	k2eAgNnSc1d1	strojní
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
potraviny	potravina	k1gFnPc1	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zboží	zboží	k1gNnSc2	zboží
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
20,1	[number]	k4	20,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
16,4	[number]	k4	16,4
%	%	kIx~	%
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
8,8	[number]	k4	8,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
Číny	Čína	k1gFnSc2	Čína
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
4,5	[number]	k4	4,5
%	%	kIx~	%
a	a	k8xC	a
USA	USA	kA	USA
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
údaje	údaj	k1gInPc4	údaj
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
HDP	HDP	kA	HDP
činí	činit	k5eAaImIp3nS	činit
83,21	[number]	k4	83,21
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
,	,	kIx,	,
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
8000	[number]	k4	8000
USD	USD	kA	USD
představuje	představovat	k5eAaImIp3nS	představovat
HDP	HDP	kA	HDP
<g/>
/	/	kIx~	/
<g/>
obyv	obyv	k1gMnSc1	obyv
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
PPP	PPP	kA	PPP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
35	[number]	k4	35
%	%	kIx~	%
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
z	z	k7c2	z
10,9	[number]	k4	10,9
%	%	kIx~	%
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
z	z	k7c2	z
54,1	[number]	k4	54,1
%	%	kIx~	%
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
rozložena	rozložit	k5eAaPmNgFnS	rozložit
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
průmysl	průmysl	k1gInSc1	průmysl
35	[number]	k4	35
%	%	kIx~	%
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
10,9	[number]	k4	10,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
služby	služba	k1gFnSc2	služba
54,1	[number]	k4	54,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
3,7	[number]	k4	3,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
14,7	[number]	k4	14,7
%	%	kIx~	%
a	a	k8xC	a
3,8	[number]	k4	3,8
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc1	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těžební	těžební	k2eAgInSc1d1	těžební
průmysl	průmysl	k1gInSc1	průmysl
<g/>
:	:	kIx,	:
fosfáty	fosfát	k1gInPc1	fosfát
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
těžbě	těžba	k1gFnSc6	těžba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
(	(	kIx(	(
<g/>
25	[number]	k4	25
%	%	kIx~	%
příjmů	příjem	k1gInPc2	příjem
země	zem	k1gFnSc2	zem
jde	jít	k5eAaImIp3nS	jít
z	z	k7c2	z
exportu	export	k1gInSc2	export
ropy	ropa	k1gFnSc2	ropa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
(	(	kIx(	(
<g/>
fosfáty	fosfát	k1gInPc1	fosfát
a	a	k8xC	a
hnojiva	hnojivo	k1gNnPc1	hnojivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc1	zpracování
kůží	kůže	k1gFnPc2	kůže
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInPc4d1	textilní
<g/>
,	,	kIx,	,
obuvnický	obuvnický	k2eAgInSc4d1	obuvnický
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
(	(	kIx(	(
<g/>
mléčné	mléčný	k2eAgInPc4d1	mléčný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
nápoje	nápoj	k1gInPc4	nápoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
produkty	produkt	k1gInPc1	produkt
tvoří	tvořit	k5eAaImIp3nP	tvořit
olivy	oliva	k1gFnPc4	oliva
<g/>
,	,	kIx,	,
olivový	olivový	k2eAgInSc4d1	olivový
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
citrusové	citrusový	k2eAgInPc4d1	citrusový
plody	plod	k1gInPc4	plod
<g/>
,	,	kIx,	,
datle	datle	k1gFnPc4	datle
<g/>
,	,	kIx,	,
mandle	mandle	k1gFnPc4	mandle
<g/>
,	,	kIx,	,
rajčata	rajče	k1gNnPc4	rajče
<g/>
,	,	kIx,	,
obilniny	obilnina	k1gFnPc1	obilnina
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
rybolov	rybolov	k1gInSc1	rybolov
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
potravin	potravina	k1gFnPc2	potravina
musí	muset	k5eAaImIp3nS	muset
Tunisko	Tunisko	k1gNnSc1	Tunisko
dovážet	dovážet	k5eAaImF	dovážet
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
rozvoj	rozvoj	k1gInSc1	rozvoj
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
státem	stát	k1gInSc7	stát
podporované	podporovaný	k2eAgNnSc1d1	podporované
<g/>
.	.	kIx.	.
</s>
<s>
Krásné	krásný	k2eAgFnPc4d1	krásná
pískové	pískový	k2eAgFnPc4d1	písková
pláže	pláž	k1gFnPc4	pláž
<g/>
,	,	kIx,	,
teplé	teplý	k2eAgNnSc4d1	teplé
a	a	k8xC	a
čisté	čistý	k2eAgNnSc4d1	čisté
moře	moře	k1gNnSc4	moře
dělají	dělat	k5eAaImIp3nP	dělat
z	z	k7c2	z
Tuniska	Tunisko	k1gNnSc2	Tunisko
významnou	významný	k2eAgFnSc4d1	významná
turistickou	turistický	k2eAgFnSc4d1	turistická
destinaci	destinace	k1gFnSc4	destinace
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
turistů	turist	k1gMnPc2	turist
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
sem	sem	k6eAd1	sem
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
relativně	relativně	k6eAd1	relativně
nízkým	nízký	k2eAgFnPc3d1	nízká
cenám	cena	k1gFnPc3	cena
a	a	k8xC	a
četným	četný	k2eAgFnPc3d1	četná
památkám	památka	k1gFnPc3	památka
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
jsou	být	k5eAaImIp3nP	být
pracovníci	pracovník	k1gMnPc1	pracovník
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
státy	stát	k1gInPc1	stát
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velmi	velmi	k6eAd1	velmi
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
veřejné	veřejný	k2eAgFnSc2d1	veřejná
meziměstské	meziměstský	k2eAgFnSc2d1	meziměstská
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
nájemné	nájemný	k2eAgInPc1d1	nájemný
taxíky	taxík	k1gInPc1	taxík
tzv.	tzv.	kA	tzv.
louages	louagesa	k1gFnPc2	louagesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ceny	cena	k1gFnPc1	cena
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
a	a	k8xC	a
frekvence	frekvence	k1gFnSc1	frekvence
spojů	spoj	k1gInPc2	spoj
překvapivě	překvapivě	k6eAd1	překvapivě
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
14	[number]	k4	14
se	s	k7c7	s
zpevněným	zpevněný	k2eAgInSc7d1	zpevněný
povrchem	povrch	k1gInSc7	povrch
ranvejí	ranvej	k1gFnPc2	ranvej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
osm	osm	k4xCc1	osm
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
letiště	letiště	k1gNnPc1	letiště
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Tunisu	Tunis	k1gInSc6	Tunis
<g/>
,	,	kIx,	,
Djerbě	Djerba	k1gFnSc6	Djerba
<g/>
,	,	kIx,	,
Monastiru	Monastiro	k1gNnSc6	Monastiro
a	a	k8xC	a
Tabarce	Tabarka	k1gFnSc6	Tabarka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
výstavba	výstavba	k1gFnSc1	výstavba
nového	nový	k2eAgNnSc2d1	nové
letiště	letiště	k1gNnSc2	letiště
Enfidha	Enfidh	k1gMnSc2	Enfidh
<g/>
,	,	kIx,	,
doplňujícího	doplňující	k2eAgMnSc2d1	doplňující
letecké	letecký	k2eAgFnPc1d1	letecká
spojení	spojení	k1gNnSc4	spojení
východotuniské	východotuniská	k1gFnSc2	východotuniská
rekreační	rekreační	k2eAgFnSc2d1	rekreační
zóny	zóna	k1gFnSc2	zóna
pro	pro	k7c4	pro
charterové	charterový	k2eAgInPc4d1	charterový
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
kapacita	kapacita	k1gFnSc1	kapacita
letišť	letiště	k1gNnPc2	letiště
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
19	[number]	k4	19
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
112	[number]	k4	112
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
z	z	k7c2	z
28	[number]	k4	28
zemí	zem	k1gFnPc2	zem
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
1400	[number]	k4	1400
letů	let	k1gInPc2	let
týdně	týdně	k6eAd1	týdně
do	do	k7c2	do
a	a	k8xC	a
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tuniskými	tuniský	k2eAgFnPc7d1	Tuniská
leteckými	letecký	k2eAgFnPc7d1	letecká
společnostmi	společnost	k1gFnPc7	společnost
jsou	být	k5eAaImIp3nP	být
Tunisair	Tunisair	k1gInSc4	Tunisair
<g/>
,	,	kIx,	,
Nouvelair	Nouvelair	k1gInSc4	Nouvelair
<g/>
,	,	kIx,	,
Karthago	Karthago	k6eAd1	Karthago
Airlines	Airlines	k1gInSc1	Airlines
a	a	k8xC	a
Tunis	Tunis	k1gInSc1	Tunis
Avia	Avia	k1gFnSc1	Avia
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
železniční	železniční	k2eAgFnSc2d1	železniční
sítě	síť	k1gFnSc2	síť
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
za	za	k7c2	za
francouzského	francouzský	k2eAgInSc2d1	francouzský
protektorátu	protektorát	k1gInSc2	protektorát
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pozdější	pozdní	k2eAgFnSc1d2	pozdější
tuniská	tuniský	k2eAgFnSc1d1	Tuniská
vláda	vláda	k1gFnSc1	vláda
dále	daleko	k6eAd2	daleko
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
na	na	k7c6	na
území	území	k1gNnSc6	území
Tuniska	Tunisko	k1gNnSc2	Tunisko
však	však	k9	však
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ještě	ještě	k9	ještě
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Tunisem	Tunis	k1gInSc7	Tunis
a	a	k8xC	a
městem	město	k1gNnSc7	město
La	la	k1gNnSc2	la
Marsa	Mars	k1gMnSc2	Mars
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
doprava	doprava	k1gFnSc1	doprava
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1872	[number]	k4	1872
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
19	[number]	k4	19
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
dnes	dnes	k6eAd1	dnes
provozuje	provozovat	k5eAaImIp3nS	provozovat
státní	státní	k2eAgFnSc1d1	státní
společnost	společnost	k1gFnSc1	společnost
Société	Sociétý	k2eAgFnSc2d1	Sociétý
Nationale	Nationale	k1gFnSc2	Nationale
de	de	k?	de
Chemins	Chemins	k1gInSc1	Chemins
de	de	k?	de
Fer	Fer	k1gFnSc2	Fer
Tunisiens	Tunisiensa	k1gFnPc2	Tunisiensa
(	(	kIx(	(
<g/>
SNCFT	SNCFT	kA	SNCFT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1956	[number]	k4	1956
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
okolo	okolo	k7c2	okolo
6	[number]	k4	6
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
společnost	společnost	k1gFnSc1	společnost
přepravila	přepravit	k5eAaPmAgFnS	přepravit
35,7	[number]	k4	35,7
milionu	milion	k4xCgInSc2	milion
cestujících	cestující	k1gFnPc2	cestující
a	a	k8xC	a
11,6	[number]	k4	11,6
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Vagony	vagon	k1gInPc1	vagon
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
přepravu	přeprava	k1gFnSc4	přeprava
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
kvalitativních	kvalitativní	k2eAgFnPc2d1	kvalitativní
tříd	třída	k1gFnPc2	třída
-	-	kIx~	-
Grand	grand	k1gMnSc1	grand
Confort	Confort	k1gInSc4	Confort
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
2	[number]	k4	2
152	[number]	k4	152
km	km	kA	km
železnic	železnice	k1gFnPc2	železnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
468	[number]	k4	468
km	km	kA	km
má	mít	k5eAaImIp3nS	mít
standardní	standardní	k2eAgInSc4d1	standardní
rozchod	rozchod	k1gInSc4	rozchod
1	[number]	k4	1
435	[number]	k4	435
mm	mm	kA	mm
a	a	k8xC	a
1	[number]	k4	1
674	[number]	k4	674
má	mít	k5eAaImIp3nS	mít
rozchod	rozchod	k1gInSc4	rozchod
1	[number]	k4	1
000	[number]	k4	000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Elektrifikovaných	elektrifikovaný	k2eAgInPc2d1	elektrifikovaný
je	být	k5eAaImIp3nS	být
65	[number]	k4	65
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
koridor	koridor	k1gInSc1	koridor
(	(	kIx(	(
<g/>
La	la	k0	la
Ligne	Lign	k1gMnSc5	Lign
de	de	k?	de
la	la	k1gNnPc2	la
Côte	Côte	k1gInSc1	Côte
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Tunis	Tunis	k1gInSc1	Tunis
-	-	kIx~	-
Sousse	Sousse	k1gFnSc1	Sousse
-	-	kIx~	-
Sfax	Sfax	k1gInSc1	Sfax
-	-	kIx~	-
Gabè	Gabè	k1gFnSc1	Gabè
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Tuniska	Tunisko	k1gNnSc2	Tunisko
je	být	k5eAaImIp3nS	být
úzkorozchodná	úzkorozchodný	k2eAgFnSc1d1	úzkorozchodná
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
přepravují	přepravovat	k5eAaImIp3nP	přepravovat
zejména	zejména	k9	zejména
fosfáty	fosfát	k1gInPc1	fosfát
a	a	k8xC	a
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
z	z	k7c2	z
vnitrozemského	vnitrozemský	k2eAgNnSc2d1	vnitrozemské
města	město	k1gNnSc2	město
Gafsa	Gafs	k1gMnSc2	Gafs
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
Sfax	Sfax	k1gInSc1	Sfax
<g/>
.	.	kIx.	.
</s>
<s>
Tuniská	tuniský	k2eAgFnSc1d1	Tuniská
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
propojena	propojit	k5eAaPmNgFnS	propojit
se	s	k7c7	s
sítí	síť	k1gFnSc7	síť
alžírskou	alžírský	k2eAgFnSc7d1	alžírská
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tudy	tudy	k6eAd1	tudy
vlaky	vlak	k1gInPc4	vlak
nejezdí	jezdit	k5eNaImIp3nP	jezdit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
buduje	budovat	k5eAaImIp3nS	budovat
železnice	železnice	k1gFnSc1	železnice
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
Gabè	Gabè	k1gFnSc2	Gabè
k	k	k7c3	k
libyjským	libyjský	k2eAgFnPc3d1	Libyjská
hranicím	hranice	k1gFnPc3	hranice
do	do	k7c2	do
města	město	k1gNnSc2	město
Ras	ras	k1gMnSc1	ras
Ajdir	Ajdir	k1gMnSc1	Ajdir
<g/>
.	.	kIx.	.
</s>
<s>
Tuniská	tuniský	k2eAgFnSc1d1	Tuniská
a	a	k8xC	a
libyjská	libyjský	k2eAgFnSc1d1	Libyjská
síť	síť	k1gFnSc1	síť
zatím	zatím	k6eAd1	zatím
propojena	propojit	k5eAaPmNgFnS	propojit
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnPc1	silnice
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
19	[number]	k4	19
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
je	být	k5eAaImIp3nS	být
vyasfaltováno	vyasfaltován	k2eAgNnSc4d1	vyasfaltováno
13	[number]	k4	13
000	[number]	k4	000
km	km	kA	km
a	a	k8xC	a
dálnice	dálnice	k1gFnSc1	dálnice
z	z	k7c2	z
Tunisu	Tunis	k1gInSc2	Tunis
do	do	k7c2	do
Sfaxu	Sfax	k1gInSc2	Sfax
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
165	[number]	k4	165
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
Tunisu	Tunis	k1gInSc2	Tunis
na	na	k7c4	na
sever	sever	k1gInSc4	sever
zatím	zatím	k6eAd1	zatím
100	[number]	k4	100
km	km	kA	km
a	a	k8xC	a
z	z	k7c2	z
Tunisu	Tunis	k1gInSc2	Tunis
do	do	k7c2	do
Bizerty	Bizert	k1gInPc7	Bizert
60	[number]	k4	60
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
1203	[number]	k4	1203
km	km	kA	km
ropovodů	ropovod	k1gInPc2	ropovod
a	a	k8xC	a
3059	[number]	k4	3059
km	km	kA	km
plynovodů	plynovod	k1gInPc2	plynovod
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
přístavy	přístav	k1gInPc7	přístav
jsou	být	k5eAaImIp3nP	být
Sfax	Sfax	k1gInSc4	Sfax
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
Goulette	Goulett	k1gInSc5	Goulett
-	-	kIx~	-
Tunis	Tunis	k1gInSc1	Tunis
(	(	kIx(	(
<g/>
Halk	Halk	k1gInSc1	Halk
al-Wádí	al-Wádí	k1gNnSc2	al-Wádí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sousse	Souss	k1gMnSc4	Souss
<g/>
,	,	kIx,	,
Bizerta	Bizert	k1gMnSc4	Bizert
<g/>
,	,	kIx,	,
Schira	Schir	k1gMnSc4	Schir
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
24	[number]	k4	24
guvernorátů	guvernorát	k1gInPc2	guvernorát
(	(	kIx(	(
<g/>
vilájetů	vilájet	k1gInPc2	vilájet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Arjána	Arján	k2eAgFnSc1d1	Arján
(	(	kIx(	(
<g/>
Ariana	Ariana	k1gFnSc1	Ariana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bádža	Bádža	k1gFnSc1	Bádža
(	(	kIx(	(
<g/>
Beja	Beja	k?	Beja
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bin	Bin	k1gFnSc1	Bin
'	'	kIx"	'
<g/>
Arús	Arús	k1gInSc1	Arús
(	(	kIx(	(
<g/>
Ben	Ben	k1gInSc1	Ben
Arous	Arous	k1gInSc1	Arous
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Banzart	Banzart	k1gInSc1	Banzart
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Bizerta	Bizerta	k1gFnSc1	Bizerta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kábis	Kábis	k1gFnSc1	Kábis
(	(	kIx(	(
<g/>
Gabes	Gabes	k1gInSc1	Gabes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Káfsa	Káfsa	k1gFnSc1	Káfsa
(	(	kIx(	(
<g/>
Gafsa	Gafsa	k1gFnSc1	Gafsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Džunduba	Džunduba	k1gMnSc1	Džunduba
(	(	kIx(	(
<g/>
Jendouba	Jendouba	k1gMnSc1	Jendouba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
al-Kajruwán	al-Kajruwán	k1gMnSc1	al-Kajruwán
(	(	kIx(	(
<g/>
Kairouan	Kairouan	k1gMnSc1	Kairouan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
al-Kasrajn	al-Kasrajn	k1gMnSc1	al-Kasrajn
(	(	kIx(	(
<g/>
Kasserine	Kasserin	k1gInSc5	Kasserin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kibilí	Kibilý	k2eAgMnPc1d1	Kibilý
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Kebili	Kebít	k5eAaPmAgMnP	Kebít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
al-Káf	al-Káf	k1gMnSc1	al-Káf
(	(	kIx(	(
<g/>
Kef	Kef	k1gMnSc1	Kef
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
al-Mahdíja	al-Mahdíj	k2eAgFnSc1d1	al-Mahdíj
(	(	kIx(	(
<g/>
Mahdia	Mahdium	k1gNnPc1	Mahdium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Manúba	Manúba	k1gMnSc1	Manúba
(	(	kIx(	(
<g/>
Manouba	Manouba	k1gMnSc1	Manouba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Madanín	Madanín	k1gMnSc1	Madanín
(	(	kIx(	(
<g/>
Medenine	Medenin	k1gInSc5	Medenin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
al-Monastir	al-Monastir	k1gMnSc1	al-Monastir
(	(	kIx(	(
<g/>
Monastir	Monastir	k1gMnSc1	Monastir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nábul	Nábul	k1gInSc1	Nábul
(	(	kIx(	(
<g/>
Nabeul	Nabeul	k1gInSc1	Nabeul
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Safákis	Safákis	k1gFnSc1	Safákis
(	(	kIx(	(
<g/>
Sfax	Sfax	k1gInSc1	Sfax
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sídí	Sídí	k1gMnSc1	Sídí
Bú	bú	k0	bú
Zajd	Zajd	k1gInSc4	Zajd
(	(	kIx(	(
<g/>
Sidi	Side	k1gFnSc4	Side
Bou	boa	k1gFnSc4	boa
said	saida	k1gFnPc2	saida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Siljána	Siljána	k1gFnSc1	Siljána
(	(	kIx(	(
<g/>
Siliana	Siliana	k1gFnSc1	Siliana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sús	Sús	k1gFnSc1	Sús
(	(	kIx(	(
<g/>
Sousse	Sousse	k1gFnSc1	Sousse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tatáwín	Tatáwín	k1gInSc4	Tatáwín
(	(	kIx(	(
<g/>
Tataouine	Tataouin	k1gMnSc5	Tataouin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tawzár	Tawzár	k1gMnSc1	Tawzár
(	(	kIx(	(
<g/>
Tozeur	Tozeur	k1gMnSc1	Tozeur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tunis	Tunis	k1gInSc1	Tunis
<g/>
,	,	kIx,	,
Zangwán	Zangwán	k1gInSc1	Zangwán
(	(	kIx(	(
<g/>
Zaghouan	Zaghouan	k1gInSc1	Zaghouan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Tunis	Tunis	k1gInSc1	Tunis
s	s	k7c7	s
aglomerací	aglomerace	k1gFnSc7	aglomerace
čítající	čítající	k2eAgFnSc7d1	čítající
2,3	[number]	k4	2,3
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc1d2	veliký
města	město	k1gNnPc1	město
leží	ležet	k5eAaImIp3nP	ležet
zpravidla	zpravidla	k6eAd1	zpravidla
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
většinou	většina	k1gFnSc7	většina
o	o	k7c4	o
významné	významný	k2eAgFnPc4d1	významná
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
turistická	turistický	k2eAgNnPc1d1	turistické
letoviska	letovisko	k1gNnPc1	letovisko
<g/>
.	.	kIx.	.
</s>
<s>
Tunisko	Tunisko	k1gNnSc1	Tunisko
je	být	k5eAaImIp3nS	být
prezidentskou	prezidentský	k2eAgFnSc7d1	prezidentská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgInSc1d1	právní
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
právním	právní	k2eAgInSc6d1	právní
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
před	před	k7c7	před
revolucí	revoluce	k1gFnSc7	revoluce
neexistovala	existovat	k5eNaImAgFnS	existovat
demokracie	demokracie	k1gFnSc1	demokracie
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
policejní	policejní	k2eAgInSc4d1	policejní
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
probíhá	probíhat	k5eAaImIp3nS	probíhat
proces	proces	k1gInSc4	proces
demokratizace	demokratizace	k1gFnSc2	demokratizace
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
maximálně	maximálně	k6eAd1	maximálně
třikrát	třikrát	k6eAd1	třikrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
(	(	kIx(	(
<g/>
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgMnS	být
však	však	k9	však
Zín	Zín	k1gMnSc1	Zín
Abidín	Abidín	k1gMnSc1	Abidín
bin	bin	k?	bin
Alí	Alí	k1gMnSc1	Alí
zvolen	zvolen	k2eAgMnSc1d1	zvolen
popáté	popáté	k4xO	popáté
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
mimo	mimo	k7c4	mimo
prezidenta	prezident	k1gMnSc4	prezident
i	i	k9	i
premiér	premiér	k1gMnSc1	premiér
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
dvoukomorové	dvoukomorový	k2eAgNnSc1d1	dvoukomorové
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
tvořené	tvořený	k2eAgNnSc1d1	tvořené
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
(	(	kIx(	(
<g/>
Madžlis	Madžlis	k1gFnSc1	Madžlis
an-Nuwáb	an-Nuwáb	k1gMnSc1	an-Nuwáb
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
214	[number]	k4	214
poslanců	poslanec	k1gMnPc2	poslanec
volených	volený	k2eAgMnPc2d1	volený
na	na	k7c4	na
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sněmovnou	sněmovna	k1gFnSc7	sněmovna
poradců	poradce	k1gMnPc2	poradce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
voleno	volen	k2eAgNnSc1d1	voleno
126	[number]	k4	126
poradců	poradce	k1gMnPc2	poradce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
85	[number]	k4	85
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
126	[number]	k4	126
poradců	poradce	k1gMnPc2	poradce
je	být	k5eAaImIp3nS	být
voleno	volen	k2eAgNnSc1d1	voleno
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
obecních	obecní	k2eAgNnPc2d1	obecní
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
a	a	k8xC	a
obchodních	obchodní	k2eAgFnPc2d1	obchodní
komor	komora	k1gFnPc2	komora
<g/>
;	;	kIx,	;
zbylých	zbylý	k2eAgNnPc2d1	zbylé
41	[number]	k4	41
jmenováno	jmenován	k2eAgNnSc1d1	jmenováno
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
stranou	strana	k1gFnSc7	strana
Strana	strana	k1gFnSc1	strana
obnovy	obnova	k1gFnSc2	obnova
<g/>
,	,	kIx,	,
islamistická	islamistický	k2eAgFnSc1d1	islamistická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
až	až	k9	až
do	do	k7c2	do
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
do	do	k7c2	do
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
oficiální	oficiální	k2eAgFnSc1d1	oficiální
vládní	vládní	k2eAgFnSc1d1	vládní
stranou	strana	k1gFnSc7	strana
RCD	RCD	kA	RCD
(	(	kIx(	(
<g/>
Demokratické	demokratický	k2eAgNnSc1d1	demokratické
ústavní	ústavní	k2eAgNnSc1d1	ústavní
sdružení	sdružení	k1gNnSc1	sdružení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgFnSc1d1	oficiální
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
republikánská	republikánský	k2eAgFnSc1d1	republikánská
strana	strana	k1gFnSc1	strana
levého	levý	k2eAgInSc2d1	levý
středu	střed	k1gInSc2	střed
<g/>
,	,	kIx,	,
držící	držící	k2eAgFnSc4d1	držící
fakticky	fakticky	k6eAd1	fakticky
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
vládce	vládce	k1gMnSc1	vládce
země	zem	k1gFnSc2	zem
Zín	Zín	k1gMnSc1	Zín
Abidín	Abidín	k1gMnSc1	Abidín
bin	bin	k?	bin
Alí	Alí	k1gMnSc1	Alí
a	a	k8xC	a
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
režimu	režim	k1gInSc2	režim
byla	být	k5eAaImAgFnS	být
strana	strana	k1gFnSc1	strana
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
revolucí	revoluce	k1gFnSc7	revoluce
kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
stály	stát	k5eAaImAgFnP	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
PUP	pupa	k1gFnPc2	pupa
(	(	kIx(	(
<g/>
Strana	strana	k1gFnSc1	strana
lidové	lidový	k2eAgFnSc2d1	lidová
jednoty	jednota	k1gFnSc2	jednota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
radikálně	radikálně	k6eAd1	radikálně
reformní	reformní	k2eAgFnSc1d1	reformní
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Muhammad	Muhammad	k1gInSc1	Muhammad
Búšiha	Búšiha	k1gFnSc1	Búšiha
<g/>
;	;	kIx,	;
MDS	MDS	kA	MDS
(	(	kIx(	(
<g/>
Strana	strana	k1gFnSc1	strana
socialistických	socialistický	k2eAgMnPc2d1	socialistický
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
levice	levice	k1gFnSc1	levice
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
opoziční	opoziční	k2eAgFnSc1d1	opoziční
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Ismáil	Ismáil	k1gMnSc1	Ismáil
Bulahja	Bulahja	k1gMnSc1	Bulahja
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
UDU	UDU	kA	UDU
(	(	kIx(	(
<g/>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
unionistická	unionistický	k2eAgFnSc1d1	unionistická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opoziční	opoziční	k2eAgFnSc1d1	opoziční
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Ahmad	Ahmad	k1gInSc1	Ahmad
Inúblí	Inúblý	k2eAgMnPc1d1	Inúblý
<g/>
;	;	kIx,	;
Hnutí	hnutí	k1gNnSc2	hnutí
at-Tadždíd	at-Tadždído	k1gNnPc2	at-Tadždído
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Ahmad	Ahmad	k1gInSc1	Ahmad
Ibráhím	Ibráhí	k2eAgNnSc7d1	Ibráhí
<g/>
;	;	kIx,	;
FDTL	FDTL	kA	FDTL
(	(	kIx(	(
<g/>
Demokratické	demokratický	k2eAgNnSc1d1	demokratické
hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Mustafa	Mustaf	k1gMnSc2	Mustaf
bin	bin	k?	bin
Džafar	Džafar	k1gMnSc1	Džafar
<g/>
;	;	kIx,	;
PVP	PVP	kA	PVP
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
strana	strana	k1gFnSc1	strana
pokroku	pokrok	k1gInSc2	pokrok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Mongí	Mongí	k1gMnSc1	Mongí
Chamasí	Chamasý	k2eAgMnPc1d1	Chamasý
<g/>
;	;	kIx,	;
PSL	PSL	kA	PSL
(	(	kIx(	(
<g/>
Liberálně	liberálně	k6eAd1	liberálně
sociální	sociální	k2eAgFnSc1d1	sociální
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Mondhir	Mondhir	k1gMnSc1	Mondhir
Thabet	Thabet	k1gMnSc1	Thabet
<g/>
;	;	kIx,	;
ABEDA	ABEDA	kA	ABEDA
<g/>
,	,	kIx,	,
AfDB	AfDB	k1gFnSc1	AfDB
<g/>
,	,	kIx,	,
AFESD	AFESD	kA	AFESD
<g/>
,	,	kIx,	,
AL	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
AMF	AMF	kA	AMF
<g/>
,	,	kIx,	,
AMU	AMU	kA	AMU
<g/>
,	,	kIx,	,
APM	APM	kA	APM
<g/>
,	,	kIx,	,
AU	au	k0	au
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
BSEC	BSEC	kA	BSEC
(	(	kIx(	(
<g/>
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
BTWC	BTWC	kA	BTWC
<g/>
,	,	kIx,	,
CEN-SAD	CEN-SAD	k1gFnSc1	CEN-SAD
<g/>
,	,	kIx,	,
CTBT	CTBT	kA	CTBT
<g/>
,	,	kIx,	,
ENMOD	ENMOD	kA	ENMOD
<g/>
,	,	kIx,	,
FAO	FAO	kA	FAO
<g/>
,	,	kIx,	,
G-	G-	k1gFnSc1	G-
<g/>
77	[number]	k4	77
<g/>
,	,	kIx,	,
GAFTA	GAFTA	kA	GAFTA
<g/>
,	,	kIx,	,
IAEA	IAEA	kA	IAEA
<g/>
,	,	kIx,	,
IBRD	IBRD	kA	IBRD
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
,	,	kIx,	,
ICC	ICC	kA	ICC
<g/>
,	,	kIx,	,
ICRM	ICRM	kA	ICRM
<g/>
,	,	kIx,	,
ICSID	ICSID	kA	ICSID
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
IDA	Ida	k1gFnSc1	Ida
<g/>
,	,	kIx,	,
IDB	IDB	kA	IDB
<g/>
,	,	kIx,	,
IFAD	IFAD	kA	IFAD
<g/>
,	,	kIx,	,
IFC	IFC	kA	IFC
<g/>
,	,	kIx,	,
IFRCS	IFRCS	kA	IFRCS
<g/>
,	,	kIx,	,
IHO	IHO	kA	IHO
<g/>
,	,	kIx,	,
ILO	ILO	kA	ILO
<g/>
,	,	kIx,	,
IMF	IMF	kA	IMF
<g/>
,	,	kIx,	,
IMO	IMO	kA	IMO
<g/>
,	,	kIx,	,
IMSO	IMSO	kA	IMSO
<g/>
,	,	kIx,	,
Interpol	interpol	k1gInSc1	interpol
<g/>
,	,	kIx,	,
IOC	IOC	kA	IOC
<g/>
,	,	kIx,	,
IOM	IOM	kA	IOM
<g/>
,	,	kIx,	,
IPU	IPU	kA	IPU
<g/>
,	,	kIx,	,
ISA	ISA	kA	ISA
<g/>
,	,	kIx,	,
ISESCO	ISESCO	kA	ISESCO
<g/>
,	,	kIx,	,
ISO	ISO	kA	ISO
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ITSO	ITSO	kA	ITSO
<g/>
,	,	kIx,	,
ITU	ITU	kA	ITU
<g/>
,	,	kIx,	,
ITUC	ITUC	kA	ITUC
<g/>
,	,	kIx,	,
KP	KP	kA	KP
<g/>
,	,	kIx,	,
MIGA	MIGA	kA	MIGA
<g/>
,	,	kIx,	,
NAM	NAM	kA	NAM
<g/>
,	,	kIx,	,
NPT	NPT	kA	NPT
<g/>
,	,	kIx,	,
NTBT	NTBT	kA	NTBT
<g/>
,	,	kIx,	,
OAPEC	OAPEC	kA	OAPEC
(	(	kIx(	(
<g/>
suspendován	suspendován	k2eAgMnSc1d1	suspendován
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
OAS	oasa	k1gFnPc2	oasa
(	(	kIx(	(
<g/>
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
OIC	OIC	kA	OIC
<g/>
,	,	kIx,	,
OIF	OIF	kA	OIF
<g/>
,	,	kIx,	,
OPCW	OPCW	kA	OPCW
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
OSCE	OSCE	kA	OSCE
(	(	kIx(	(
<g/>
partner	partner	k1gMnSc1	partner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
UN	UN	kA	UN
<g/>
,	,	kIx,	,
UNCLOS	UNCLOS	kA	UNCLOS
<g/>
,	,	kIx,	,
UNCTAD	UNCTAD	kA	UNCTAD
<g/>
,	,	kIx,	,
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
UNFCC	UNFCC	kA	UNFCC
<g/>
,	,	kIx,	,
UNHCR	UNHCR	kA	UNHCR
<g/>
,	,	kIx,	,
UNIDO	UNIDO	kA	UNIDO
<g/>
,	,	kIx,	,
UNWTO	UNWTO	kA	UNWTO
<g/>
,	,	kIx,	,
UPU	UPU	kA	UPU
<g/>
,	,	kIx,	,
WCO	WCO	kA	WCO
<g/>
,	,	kIx,	,
WFTU	WFTU	kA	WFTU
<g/>
,	,	kIx,	,
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
WIPO	WIPO	kA	WIPO
<g/>
,	,	kIx,	,
WMO	WMO	kA	WMO
<g/>
,	,	kIx,	,
WTO	WTO	kA	WTO
<g/>
.	.	kIx.	.
</s>
