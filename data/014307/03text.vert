<s>
Tom	Tom	k1gMnSc1
Gehrels	Gehrelsa	k1gFnPc2
</s>
<s>
Tom	Tom	k1gMnSc1
Gehrels	Gehrels	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1925	#num#	k4
<g/>
Haarlemmermeer	Haarlemmermeero	k1gNnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
86	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Tucson	Tucson	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
LeidenuChicagská	LeidenuChicagský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
astronom	astronom	k1gMnSc1
a	a	k8xC
odbojář	odbojář	k1gMnSc1
Zaměstnavatel	zaměstnavatel	k1gMnSc1
</s>
<s>
University	universita	k1gFnPc1
of	of	k?
Arizona	Arizona	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tom	Tom	k1gMnSc1
Gehrels	Gehrelsa	k1gFnPc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1925	#num#	k4
<g/>
,	,	kIx,
Haarlemmermeer	Haarlemmermeer	k1gInSc1
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2011	#num#	k4
<g/>
,	,	kIx,
Tucson	Tucson	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
nizozemsko-americký	nizozemsko-americký	k2eAgMnSc1d1
astronom	astronom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslavil	proslavit	k5eAaPmAgMnS
se	se	k3xPyFc4
fotometrií	fotometrie	k1gFnSc7
asteroidů	asteroid	k1gInPc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
rozvinul	rozvinout	k5eAaPmAgInS
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
zkoumal	zkoumat	k5eAaImAgInS
vztah	vztah	k1gInSc1
vlnové	vlnový	k2eAgFnSc2d1
délky	délka	k1gFnSc2
a	a	k8xC
polarizace	polarizace	k1gFnSc2
hvězd	hvězda	k1gFnPc2
a	a	k8xC
planet	planeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevil	objevit	k5eAaPmAgMnS
přes	přes	k7c4
4000	#num#	k4
asteroidů	asteroid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
působení	působení	k1gNnSc1
</s>
<s>
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
nacistické	nacistický	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
Holandska	Holandsko	k1gNnSc2
ještě	ještě	k9
jako	jako	k8xC,k8xS
mladistvý	mladistvý	k1gMnSc1
uprchl	uprchnout	k5eAaPmAgMnS
do	do	k7c2
Anglie	Anglie	k1gFnSc2
a	a	k8xC
jako	jako	k9
parašutista	parašutista	k1gMnSc1
byl	být	k5eAaImAgMnS
shozen	shozen	k2eAgMnSc1d1
na	na	k7c4
nizozemské	nizozemský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
připravil	připravit	k5eAaPmAgMnS
půdu	půda	k1gFnSc4
pro	pro	k7c4
britské	britský	k2eAgFnPc4d1
speciální	speciální	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1951	#num#	k4
vystudoval	vystudovat	k5eAaPmAgMnS
fyziku	fyzika	k1gFnSc4
a	a	k8xC
astronomii	astronomie	k1gFnSc4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Leidenu	Leiden	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Titul	titul	k1gInSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
získal	získat	k5eAaPmAgMnS
již	již	k6eAd1
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
na	na	k7c6
Chicagské	chicagský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
roku	rok	k1gInSc2
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
začal	začít	k5eAaPmAgMnS
pracovat	pracovat	k5eAaImF
na	na	k7c6
Arizonské	arizonský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Tucsonu	Tucson	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
založil	založit	k5eAaPmAgMnS
projekt	projekt	k1gInSc4
Spacewatch	Spacewatch	k1gMnSc1
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	s	k7c7
profesorem	profesor	k1gMnSc7
a	a	k8xC
kde	kde	k6eAd1
působil	působit	k5eAaImAgMnS
až	až	k9
do	do	k7c2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
jeho	jeho	k3xOp3gMnPc3
blízkým	blízký	k2eAgMnPc3d1
spolupracovníkům	spolupracovník	k1gMnPc3
a	a	k8xC
přátelům	přítel	k1gMnPc3
patřili	patřit	k5eAaImAgMnP
Gerard	Gerard	k1gInSc1
Kuiper	Kuiper	k1gInSc1
<g/>
,	,	kIx,
Cornelis	Cornelis	k1gInSc1
Johannes	Johannesa	k1gFnPc2
van	vana	k1gFnPc2
Houten	Houten	k2eAgInSc1d1
a	a	k8xC
Ingrid	Ingrid	k1gFnSc1
van	van	k1gInSc1
Houten-Groeneveldová	Houten-Groeneveldová	k1gFnSc1
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
tři	tři	k4xCgMnPc1
nizozemského	nizozemský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.dutchinamerica.com	www.dutchinamerica.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Spacewatch	Spacewatch	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
stk	stk	k?
<g/>
2009477126	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118907476	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1032	#num#	k4
1981	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79097311	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
108488997	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79097311	#num#	k4
</s>
