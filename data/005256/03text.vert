<s>
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Windows	Windows	kA	Windows
Internet	Internet	k1gInSc4	Internet
Explorer	Explorra	k1gFnPc2	Explorra
nebo	nebo	k8xC	nebo
také	také	k9	také
Microsoft	Microsoft	kA	Microsoft
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
<g/>
,	,	kIx,	,
zkracovaný	zkracovaný	k2eAgInSc1d1	zkracovaný
jako	jako	k8xS	jako
IE	IE	kA	IE
<g/>
,	,	kIx,	,
MSIE	MSIE	kA	MSIE
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proprietární	proprietární	k2eAgMnSc1d1	proprietární
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
rodiny	rodina	k1gFnSc2	rodina
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
Internet	Internet	k1gInSc1	Internet
Exploreru	Explorero	k1gNnSc3	Explorero
je	být	k5eAaImIp3nS	být
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
NCIS	NCIS	kA	NCIS
Mosaic	Mosaic	k1gMnSc1	Mosaic
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
pro	pro	k7c4	pro
Netscape	Netscap	k1gInSc5	Netscap
Navigator	Navigator	k1gMnSc1	Navigator
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
zdrojové	zdrojový	k2eAgInPc1d1	zdrojový
kódy	kód	k1gInPc1	kód
byly	být	k5eAaImAgInP	být
zčásti	zčásti	k6eAd1	zčásti
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
prohlížeči	prohlížeč	k1gInSc6	prohlížeč
Spyglass	Spyglassa	k1gFnPc2	Spyglassa
Mosaic	Mosaice	k1gInPc2	Mosaice
licencovanému	licencovaný	k2eAgInSc3d1	licencovaný
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Spyglass	Spyglassa	k1gFnPc2	Spyglassa
firmě	firma	k1gFnSc3	firma
Microsoft	Microsoft	kA	Microsoft
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
firma	firma	k1gFnSc1	firma
Spyglass	Spyglassa	k1gFnPc2	Spyglassa
obdrží	obdržet	k5eAaPmIp3nP	obdržet
určitá	určitý	k2eAgNnPc1d1	určité
procenta	procento	k1gNnPc1	procento
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
verze	verze	k1gFnSc1	verze
3	[number]	k4	3
již	již	k9	již
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
bez	bez	k7c2	bez
licencovaných	licencovaný	k2eAgInPc2d1	licencovaný
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
vyvíjen	vyvíjen	k2eAgInSc1d1	vyvíjen
také	také	k9	také
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
:	:	kIx,	:
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
Internet	Internet	k1gInSc1	Internet
Explorer	Explorra	k1gFnPc2	Explorra
pro	pro	k7c4	pro
Mac	Mac	kA	Mac
a	a	k8xC	a
vznikal	vznikat	k5eAaImAgInS	vznikat
Internet	Internet	k1gInSc1	Internet
Explorer	Explorero	k1gNnPc2	Explorero
pro	pro	k7c4	pro
UNIX	UNIX	kA	UNIX
(	(	kIx(	(
<g/>
pracující	pracující	k1gMnSc1	pracující
přes	přes	k7c4	přes
X	X	kA	X
Window	Window	k1gMnSc7	Window
System	Syst	k1gMnSc7	Syst
na	na	k7c6	na
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
Solaris	Solaris	k1gFnSc2	Solaris
a	a	k8xC	a
HP-UX	HP-UX	k1gFnSc2	HP-UX
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
všech	všecek	k3xTgFnPc2	všecek
uvedených	uvedený	k2eAgFnPc2d1	uvedená
verzí	verze	k1gFnPc2	verze
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
verzích	verze	k1gFnPc6	verze
Windows	Windows	kA	Windows
7	[number]	k4	7
se	se	k3xPyFc4	se
uživatel	uživatel	k1gMnSc1	uživatel
může	moct	k5eAaImIp3nS	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
chce	chtít	k5eAaImIp3nS	chtít
IE	IE	kA	IE
používat	používat	k5eAaImF	používat
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gNnSc4	on
odinstalovat	odinstalovat	k5eAaPmF	odinstalovat
a	a	k8xC	a
systém	systém	k1gInSc4	systém
uživateli	uživatel	k1gMnPc7	uživatel
dá	dát	k5eAaPmIp3nS	dát
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
alternativních	alternativní	k2eAgInPc2d1	alternativní
prohlížečů	prohlížeč	k1gInPc2	prohlížeč
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Mozilla	Mozilla	k1gFnSc1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
<g/>
,	,	kIx,	,
Google	Google	k1gInSc1	Google
Chrome	chromat	k5eAaImIp3nS	chromat
<g/>
,	,	kIx,	,
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
Safari	safari	k1gNnSc1	safari
<g/>
,	,	kIx,	,
Maxthon	Maxthon	k1gInSc1	Maxthon
atd.	atd.	kA	atd.
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
opatření	opatření	k1gNnSc4	opatření
kvůli	kvůli	k7c3	kvůli
Evropské	evropský	k2eAgFnSc3d1	Evropská
komisi	komise	k1gFnSc3	komise
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnPc4	který
firma	firma	k1gFnSc1	firma
Opera	opera	k1gFnSc1	opera
Software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
tvůrce	tvůrce	k1gMnSc1	tvůrce
webového	webový	k2eAgInSc2d1	webový
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
Opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
podala	podat	k5eAaPmAgFnS	podat
stížnost	stížnost	k1gFnSc1	stížnost
na	na	k7c6	na
zneužívání	zneužívání	k1gNnSc6	zneužívání
tržního	tržní	k2eAgInSc2d1	tržní
podílu	podíl	k1gInSc2	podíl
firmou	firma	k1gFnSc7	firma
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
upravená	upravený	k2eAgFnSc1d1	upravená
verze	verze	k1gFnSc1	verze
Spyglass	Spyglassa	k1gFnPc2	Spyglassa
Mosaic	Mosaice	k1gFnPc2	Mosaice
<g/>
.	.	kIx.	.
</s>
<s>
Přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
Microsoft	Microsoft	kA	Microsoft
plus	plus	k1gInSc1	plus
<g/>
!	!	kIx.	!
</s>
<s>
pro	pro	k7c4	pro
OEM	OEM	kA	OEM
vydání	vydání	k1gNnSc2	vydání
Windows	Windows	kA	Windows
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
1.5	[number]	k4	1.5
byla	být	k5eAaImAgFnS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
o	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
NT	NT	kA	NT
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
pro	pro	k7c4	pro
základní	základní	k2eAgInSc4d1	základní
tabulkový	tabulkový	k2eAgInSc4d1	tabulkový
překlad	překlad	k1gInSc4	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
2.0	[number]	k4	2.0
byla	být	k5eAaImAgFnS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
95	[number]	k4	95
a	a	k8xC	a
Windows	Windows	kA	Windows
NT	NT	kA	NT
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1995	[number]	k4	1995
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
SSL	SSL	kA	SSL
<g/>
,	,	kIx,	,
cookies	cookies	k1gMnSc1	cookies
<g/>
,	,	kIx,	,
VRML	VRML	kA	VRML
a	a	k8xC	a
Internetových	internetový	k2eAgFnPc2d1	internetová
diskuzních	diskuzní	k2eAgFnPc2d1	diskuzní
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
první	první	k4xOgFnSc1	první
široce	široko	k6eAd1	široko
používanou	používaný	k2eAgFnSc7d1	používaná
verzí	verze	k1gFnSc7	verze
Internet	Internet	k1gInSc1	Internet
Exploreru	Explorer	k1gInSc6	Explorer
<g/>
.	.	kIx.	.
</s>
<s>
Neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
již	již	k9	již
licencované	licencovaný	k2eAgInPc4d1	licencovaný
zdrojové	zdrojový	k2eAgInPc4d1	zdrojový
kódy	kód	k1gInPc4	kód
firmy	firma	k1gFnSc2	firma
Spyglass	Spyglassa	k1gFnPc2	Spyglassa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
používána	používán	k2eAgFnSc1d1	používána
"	"	kIx"	"
<g/>
technologie	technologie	k1gFnSc1	technologie
<g/>
"	"	kIx"	"
Spyglass	Spyglass	k1gInSc1	Spyglass
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
licenční	licenční	k2eAgFnPc4d1	licenční
informace	informace	k1gFnPc4	informace
v	v	k7c6	v
programové	programový	k2eAgFnSc6d1	programová
dokumentaci	dokumentace	k1gFnSc6	dokumentace
uvedeny	uveden	k2eAgInPc1d1	uveden
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
3	[number]	k4	3
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
významnější	významný	k2eAgInSc4d2	významnější
prohlížeč	prohlížeč	k1gInSc4	prohlížeč
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
kaskádových	kaskádový	k2eAgInPc2d1	kaskádový
stylů	styl	k1gInPc2	styl
(	(	kIx(	(
<g/>
CSS	CSS	kA	CSS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
částečná	částečný	k2eAgFnSc1d1	částečná
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
ActiveX	ActiveX	k1gFnPc4	ActiveX
<g/>
,	,	kIx,	,
applety	appleta	k1gFnPc4	appleta
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Javum	k1gNnSc2	Javum
<g/>
,	,	kIx,	,
multimédia	multimédium	k1gNnSc2	multimédium
a	a	k8xC	a
PISC	PISC	kA	PISC
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
začlenění	začlenění	k1gNnSc4	začlenění
metadat	metadat	k5eAaImF	metadat
<g/>
.	.	kIx.	.
</s>
<s>
Obsahoval	obsahovat	k5eAaImAgInS	obsahovat
tak	tak	k9	tak
proti	proti	k7c3	proti
konkurenčnímu	konkurenční	k2eAgInSc3d1	konkurenční
prohlížeči	prohlížeč	k1gInSc3	prohlížeč
Netscape	Netscap	k1gInSc5	Netscap
Navigator	Navigator	k1gInSc1	Navigator
mnoho	mnoho	k4c1	mnoho
novinek	novinka	k1gFnPc2	novinka
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
3	[number]	k4	3
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
klienta	klient	k1gMnSc4	klient
Microsoft	Microsoft	kA	Microsoft
Internet	Internet	k1gInSc1	Internet
Mail	mail	k1gInSc1	mail
and	and	k?	and
News	News	k1gInSc1	News
pro	pro	k7c4	pro
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
poštu	pošta	k1gFnSc4	pošta
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc1	předchůdce
Outlook	Outlook	k1gInSc4	Outlook
Expressu	express	k1gInSc2	express
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
NetMeeting	NetMeeting	k1gInSc4	NetMeeting
a	a	k8xC	a
ranou	raný	k2eAgFnSc4d1	raná
verzi	verze	k1gFnSc4	verze
Windows	Windows	kA	Windows
Address	Address	k1gInSc1	Address
Book	Book	k1gInSc1	Book
(	(	kIx(	(
<g/>
adresář	adresář	k1gInSc1	adresář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
3	[number]	k4	3
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Windows	Windows	kA	Windows
95	[number]	k4	95
OSR	OSR	kA	OSR
2	[number]	k4	2
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
první	první	k4xOgFnSc7	první
populární	populární	k2eAgFnSc7d1	populární
verzí	verze	k1gFnSc7	verze
Internet	Internet	k1gInSc1	Internet
Exploreru	Explorer	k1gInSc6	Explorer
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
září	září	k1gNnSc6	září
1997	[number]	k4	1997
a	a	k8xC	a
prohlubovala	prohlubovat	k5eAaImAgFnS	prohlubovat
úroveň	úroveň	k1gFnSc4	úroveň
integrace	integrace	k1gFnSc2	integrace
mezi	mezi	k7c7	mezi
internetovým	internetový	k2eAgInSc7d1	internetový
prohlížečem	prohlížeč	k1gInSc7	prohlížeč
a	a	k8xC	a
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Instalace	instalace	k1gFnSc1	instalace
verze	verze	k1gFnSc1	verze
4	[number]	k4	4
do	do	k7c2	do
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
95	[number]	k4	95
nebo	nebo	k8xC	nebo
Windows	Windows	kA	Windows
NT	NT	kA	NT
4	[number]	k4	4
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
"	"	kIx"	"
<g/>
Windows	Windows	kA	Windows
desktop	desktop	k1gInSc1	desktop
update	update	k1gInSc1	update
<g/>
"	"	kIx"	"
přinesla	přinést	k5eAaPmAgFnS	přinést
možnost	možnost	k1gFnSc1	možnost
zobrazení	zobrazení	k1gNnSc2	zobrazení
webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
desktopu	desktop	k1gInSc2	desktop
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
Internet	Internet	k1gInSc4	Internet
Exploreru	Explorer	k1gInSc2	Explorer
i	i	k9	i
pro	pro	k7c4	pro
zobrazování	zobrazování	k1gNnSc4	zobrazování
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
adresářů	adresář	k1gInPc2	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
volba	volba	k1gFnSc1	volba
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
instalátorech	instalátor	k1gMnPc6	instalátor
pozdějších	pozdní	k2eAgInPc2d2	pozdější
verzí	verze	k1gFnPc2	verze
Internet	Internet	k1gInSc1	Internet
Exploreru	Explorer	k1gInSc2	Explorer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
ze	z	k7c2	z
systému	systém	k1gInSc2	systém
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
nainstalování	nainstalování	k1gNnSc6	nainstalování
již	již	k6eAd1	již
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
4	[number]	k4	4
přinesl	přinést	k5eAaPmAgInS	přinést
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
Group	Group	k1gInSc4	Group
Policy	Polica	k1gFnSc2	Polica
<g/>
,	,	kIx,	,
dovolující	dovolující	k2eAgFnSc1d1	dovolující
konfigurovat	konfigurovat	k5eAaBmF	konfigurovat
a	a	k8xC	a
nastavit	nastavit	k5eAaPmF	nastavit
konfiguraci	konfigurace	k1gFnSc4	konfigurace
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
počítačů	počítač	k1gInPc2	počítač
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
Internet	Internet	k1gInSc1	Internet
Mail	mail	k1gInSc1	mail
a	a	k8xC	a
News	News	k1gInSc1	News
byl	být	k5eAaImAgInS	být
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
Outlook	Outlook	k1gInSc1	Outlook
Expressem	express	k1gInSc7	express
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
i	i	k9	i
Microsoft	Microsoft	kA	Microsoft
Chat	chata	k1gFnPc2	chata
a	a	k8xC	a
vylepšený	vylepšený	k2eAgInSc4d1	vylepšený
NetMeeting	NetMeeting	k1gInSc4	NetMeeting
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
součástí	součást	k1gFnSc7	součást
Windows	Windows	kA	Windows
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
<s>
Integrace	integrace	k1gFnSc1	integrace
verze	verze	k1gFnSc1	verze
4	[number]	k4	4
s	s	k7c7	s
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
soudních	soudní	k2eAgFnPc2d1	soudní
žalob	žaloba	k1gFnPc2	žaloba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vinily	vinit	k5eAaImAgFnP	vinit
společnost	společnost	k1gFnSc4	společnost
Microsoft	Microsoft	kA	Microsoft
ze	z	k7c2	z
zneužití	zneužití	k1gNnSc2	zneužití
dominantního	dominantní	k2eAgNnSc2d1	dominantní
postavení	postavení	k1gNnSc2	postavení
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Pátá	pátý	k4xOgFnSc1	pátý
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1999	[number]	k4	1999
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
integrována	integrovat	k5eAaBmNgFnS	integrovat
ve	v	k7c6	v
Windows	Windows	kA	Windows
98	[number]	k4	98
SE	s	k7c7	s
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
2000	[number]	k4	2000
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
Office	Office	kA	Office
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
podpora	podpora	k1gFnSc1	podpora
obousměrného	obousměrný	k2eAgInSc2d1	obousměrný
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
ruby	rub	k1gInPc4	rub
znaků	znak	k1gInPc2	znak
pro	pro	k7c4	pro
čínštinu	čínština	k1gFnSc4	čínština
<g/>
,	,	kIx,	,
XML	XML	kA	XML
<g/>
,	,	kIx,	,
XSL	XSL	kA	XSL
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
chránit	chránit	k5eAaImF	chránit
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
v	v	k7c6	v
MHTML	MHTML	kA	MHTML
formátu	formát	k1gInSc6	formát
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
2000	[number]	k4	2000
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
Internet	Internet	k1gInSc4	Internet
Explorer	Explorer	k1gInSc4	Explorer
5.01	[number]	k4	5.01
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
XMLHttpRequest	XMLHttpRequest	k1gFnSc1	XMLHttpRequest
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
5.5	[number]	k4	5.5
vydaná	vydaný	k2eAgNnPc1d1	vydané
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2000	[number]	k4	2000
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
tiskový	tiskový	k2eAgInSc4d1	tiskový
náhled	náhled	k1gInSc4	náhled
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
CSS	CSS	kA	CSS
a	a	k8xC	a
HTML	HTML	kA	HTML
standardů	standard	k1gInPc2	standard
<g/>
,	,	kIx,	,
vývojářských	vývojářský	k2eAgInPc2d1	vývojářský
API	API	kA	API
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
Windows	Windows	kA	Windows
Me	Me	k1gFnSc7	Me
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
5.5	[number]	k4	5.5
také	také	k9	také
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
128	[number]	k4	128
<g/>
-bitové	itové	k2eAgNnPc2d1	-bitové
šifrování	šifrování	k1gNnPc2	šifrování
<g/>
.	.	kIx.	.
</s>
<s>
Šestá	šestý	k4xOgFnSc1	šestý
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2001	[number]	k4	2001
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
Windows	Windows	kA	Windows
XP	XP	kA	XP
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
byla	být	k5eAaImAgFnS	být
integrována	integrován	k2eAgFnSc1d1	integrována
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
též	též	k9	též
dostupná	dostupný	k2eAgFnSc1d1	dostupná
jako	jako	k8xC	jako
instalační	instalační	k2eAgInSc1d1	instalační
balíček	balíček	k1gInSc1	balíček
pro	pro	k7c4	pro
starší	starý	k2eAgInPc4d2	starší
systémy	systém	k1gInPc4	systém
až	až	k9	až
po	po	k7c6	po
Windows	Windows	kA	Windows
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
nově	nově	k6eAd1	nově
DHTML	DHTML	kA	DHTML
<g/>
,	,	kIx,	,
částečnou	částečný	k2eAgFnSc4d1	částečná
podporu	podpora	k1gFnSc4	podpora
CSS	CSS	kA	CSS
úrovně	úroveň	k1gFnSc2	úroveň
1	[number]	k4	1
<g/>
,	,	kIx,	,
DOM	DOM	k?	DOM
úroveň	úroveň	k1gFnSc4	úroveň
1	[number]	k4	1
a	a	k8xC	a
SMIL	smil	k1gInSc1	smil
2.0	[number]	k4	2.0
<g/>
.	.	kIx.	.
</s>
<s>
MSXML	MSXML	kA	MSXML
engine	enginout	k5eAaPmIp3nS	enginout
byl	být	k5eAaImAgInS	být
aktualizován	aktualizovat	k5eAaBmNgMnS	aktualizovat
na	na	k7c6	na
verzi	verze	k1gFnSc6	verze
3.0	[number]	k4	3.0
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
prvky	prvek	k1gInPc1	prvek
byly	být	k5eAaImAgInP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
verzi	verze	k1gFnSc6	verze
Internet	Internet	k1gInSc4	Internet
Explorer	Explorer	k1gInSc1	Explorer
Administration	Administration	k1gInSc1	Administration
Kit	kit	k1gInSc1	kit
(	(	kIx(	(
<g/>
IEAK	IEAK	kA	IEAK
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Media	medium	k1gNnSc2	medium
bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Messenger	Messenger	k1gInSc4	Messenger
<g/>
,	,	kIx,	,
sběr	sběr	k1gInSc4	sběr
poruch	porucha	k1gFnPc2	porucha
<g/>
,	,	kIx,	,
přenastavitelný	přenastavitelný	k2eAgInSc4d1	přenastavitelný
automatický	automatický	k2eAgInSc4d1	automatický
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
P3P	P3P	k1gFnSc4	P3P
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
vzhled	vzhled	k1gInSc4	vzhled
použitý	použitý	k2eAgInSc4d1	použitý
ve	v	k7c4	v
Windows	Windows	kA	Windows
XP	XP	kA	XP
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
poslední	poslední	k2eAgFnSc4d1	poslední
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
rok	rok	k1gInSc4	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
následující	následující	k2eAgFnSc2d1	následující
verze	verze	k1gFnSc2	verze
7	[number]	k4	7
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Verzi	verze	k1gFnSc4	verze
7	[number]	k4	7
oznámil	oznámit	k5eAaPmAgInS	oznámit
již	již	k6eAd1	již
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
Bill	Bill	k1gMnSc1	Bill
Gates	Gates	k1gMnSc1	Gates
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
na	na	k7c6	na
RSA	RSA	kA	RSA
konferenci	konference	k1gFnSc6	konference
2005	[number]	k4	2005
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
betaverze	betaverze	k1gFnSc1	betaverze
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
byla	být	k5eAaImAgFnS	být
uvolněna	uvolněn	k2eAgFnSc1d1	uvolněna
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
jako	jako	k8xC	jako
technické	technický	k2eAgNnSc4d1	technické
zkušební	zkušební	k2eAgNnSc4d1	zkušební
vydání	vydání	k1gNnSc4	vydání
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
byla	být	k5eAaImAgFnS	být
betaverze	betaverze	k1gFnSc1	betaverze
uvolněna	uvolněn	k2eAgFnSc1d1	uvolněna
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Finální	finální	k2eAgFnSc1d1	finální
veřejná	veřejný	k2eAgFnSc1d1	veřejná
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jen	jen	k6eAd1	jen
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
XP	XP	kA	XP
SP2	SP2	k1gMnSc1	SP2
a	a	k8xC	a
novější	nový	k2eAgMnSc1d2	novější
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2003	[number]	k4	2003
SP1	SP1	k1gMnPc2	SP1
a	a	k8xC	a
Windows	Windows	kA	Windows
Vista	vista	k2eAgFnPc4d1	vista
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
7	[number]	k4	7
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
název	název	k1gInSc1	název
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
na	na	k7c4	na
Windows	Windows	kA	Windows
Internet	Internet	k1gInSc4	Internet
Explorer	Explorra	k1gFnPc2	Explorra
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
7	[number]	k4	7
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
ochranu	ochrana	k1gFnSc4	ochrana
uživatele	uživatel	k1gMnSc2	uživatel
před	před	k7c7	před
phishingem	phishing	k1gInSc7	phishing
a	a	k8xC	a
záludným	záludný	k2eAgInSc7d1	záludný
softwarem	software	k1gInSc7	software
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
plnou	plný	k2eAgFnSc7d1	plná
uživatelskou	uživatelský	k2eAgFnSc7d1	Uživatelská
kontrolou	kontrola	k1gFnSc7	kontrola
nad	nad	k7c7	nad
ActiveX	ActiveX	k1gFnSc7	ActiveX
a	a	k8xC	a
lepším	dobrý	k2eAgInSc7d2	lepší
bezpečnostním	bezpečnostní	k2eAgInSc7d1	bezpečnostní
rámcem	rámec	k1gInSc7	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozí	předchozí	k2eAgFnSc2d1	předchozí
verze	verze	k1gFnSc2	verze
neběží	běžet	k5eNaImIp3nS	běžet
ActiveX	ActiveX	k1gFnSc1	ActiveX
control	controla	k1gFnPc2	controla
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
Windows	Windows	kA	Windows
Exploreru	Explorera	k1gFnSc4	Explorera
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běží	běžet	k5eAaImIp3nS	běžet
ve	v	k7c6	v
vlastním	vlastní	k2eAgInSc6d1	vlastní
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
7	[number]	k4	7
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
webové	webový	k2eAgInPc4d1	webový
standardy	standard	k1gInPc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
Microsoft	Microsoft	kA	Microsoft
odstranil	odstranit	k5eAaPmAgMnS	odstranit
softwarové	softwarový	k2eAgNnSc4d1	softwarové
ověření	ověření	k1gNnSc4	ověření
pravosti	pravost	k1gFnSc2	pravost
Windows	Windows	kA	Windows
(	(	kIx(	(
<g/>
Windows	Windows	kA	Windows
Genuine	Genuin	k1gMnSc5	Genuin
Advantage	Advantagus	k1gMnSc5	Advantagus
<g/>
)	)	kIx)	)
před	před	k7c7	před
instalací	instalace	k1gFnSc7	instalace
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
8	[number]	k4	8
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
verze	verze	k1gFnSc1	verze
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
XP	XP	kA	XP
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
beta	beta	k1gNnSc1	beta
verze	verze	k1gFnSc2	verze
byla	být	k5eAaImAgFnS	být
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgNnPc1	druhý
beta	beta	k1gNnPc1	beta
verze	verze	k1gFnSc2	verze
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Finální	finální	k2eAgFnSc1d1	finální
verze	verze	k1gFnSc1	verze
vyšla	vyjít	k5eAaPmAgFnS	vyjít
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Novinkou	novinka	k1gFnSc7	novinka
jsou	být	k5eAaImIp3nP	být
Accelerators	Accelerators	k1gInSc4	Accelerators
a	a	k8xC	a
WebSlices	WebSlices	k1gInSc4	WebSlices
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
Microsoftu	Microsoft	k1gInSc2	Microsoft
jsou	být	k5eAaImIp3nP	být
prioritami	priorita	k1gFnPc7	priorita
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
použitelnost	použitelnost	k1gFnSc4	použitelnost
<g/>
,	,	kIx,	,
zlepšení	zlepšení	k1gNnSc4	zlepšení
podpory	podpora	k1gFnSc2	podpora
technologií	technologie	k1gFnPc2	technologie
RSS	RSS	kA	RSS
<g/>
,	,	kIx,	,
CSS	CSS	kA	CSS
a	a	k8xC	a
AJAX	AJAX	kA	AJAX
<g/>
.	.	kIx.	.
</s>
<s>
Webové	webový	k2eAgInPc1d1	webový
standardy	standard	k1gInPc1	standard
jsou	být	k5eAaImIp3nP	být
podporovány	podporovat	k5eAaImNgInP	podporovat
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finální	finální	k2eAgFnSc6d1	finální
verzi	verze	k1gFnSc6	verze
je	být	k5eAaImIp3nS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
podpora	podpora	k1gFnSc1	podpora
CSS	CSS	kA	CSS
2.1	[number]	k4	2.1
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
projít	projít	k5eAaPmF	projít
testem	test	k1gInSc7	test
Acid	Acid	k1gInSc4	Acid
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zpětné	zpětný	k2eAgFnSc3d1	zpětná
kompatibilitě	kompatibilita	k1gFnSc3	kompatibilita
však	však	k9	však
IE8	IE8	k1gFnSc1	IE8
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
renderovací	renderovací	k2eAgNnSc1d1	renderovací
jádro	jádro	k1gNnSc1	jádro
IE	IE	kA	IE
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
vylepšeným	vylepšený	k2eAgNnSc7d1	vylepšené
jádrem	jádro	k1gNnSc7	jádro
IE	IE	kA	IE
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
můžou	můžou	k?	můžou
díky	díky	k7c3	díky
speciálnímu	speciální	k2eAgInSc3d1	speciální
meta	meta	k1gFnSc1	meta
elementu	element	k1gInSc2	element
vyvolat	vyvolat	k5eAaPmF	vyvolat
renderování	renderování	k1gNnSc2	renderování
IE	IE	kA	IE
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Mnohých	mnohý	k2eAgNnPc2d1	mnohé
vylepšení	vylepšení	k1gNnPc2	vylepšení
doznala	doznat	k5eAaPmAgFnS	doznat
také	také	k9	také
podpora	podpora	k1gFnSc1	podpora
JavaScriptu	JavaScript	k1gInSc2	JavaScript
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Finální	finální	k2eAgFnSc1d1	finální
verze	verze	k1gFnSc1	verze
vyšla	vyjít	k5eAaPmAgFnS	vyjít
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Novinkou	novinka	k1gFnSc7	novinka
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
vypnutí	vypnutí	k1gNnSc4	vypnutí
částí	část	k1gFnPc2	část
prohlížené	prohlížený	k2eAgFnSc2d1	prohlížená
stránky	stránka	k1gFnSc2	stránka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
internetových	internetový	k2eAgFnPc2d1	internetová
domén	doména	k1gFnPc2	doména
a	a	k8xC	a
serverů	server	k1gInPc2	server
<g/>
,	,	kIx,	,
laicky	laicky	k6eAd1	laicky
tedy	tedy	k9	tedy
vypnutí	vypnutí	k1gNnSc4	vypnutí
zobrazování	zobrazování	k1gNnSc2	zobrazování
reklamy	reklama	k1gFnSc2	reklama
nebo	nebo	k8xC	nebo
počítadel	počítadlo	k1gNnPc2	počítadlo
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
prioritami	priorita	k1gFnPc7	priorita
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
jsou	být	k5eAaImIp3nP	být
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
HW	HW	kA	HW
akcelerace	akcelerace	k1gFnSc1	akcelerace
<g/>
,	,	kIx,	,
kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
a	a	k8xC	a
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zlepšena	zlepšen	k2eAgFnSc1d1	zlepšena
podpora	podpora	k1gFnSc1	podpora
nových	nový	k2eAgInPc2d1	nový
webových	webový	k2eAgInPc2d1	webový
standardů	standard	k1gInPc2	standard
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
podpora	podpora	k1gFnSc1	podpora
HTML5	HTML5	k1gFnSc1	HTML5
a	a	k8xC	a
CSS	CSS	kA	CSS
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zpětné	zpětný	k2eAgFnSc3d1	zpětná
kompatibilitě	kompatibilita	k1gFnSc3	kompatibilita
však	však	k9	však
IE9	IE9	k1gFnSc1	IE9
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
renderovací	renderovací	k2eAgNnSc1d1	renderovací
jádro	jádro	k1gNnSc1	jádro
IE7	IE7	k1gFnSc2	IE7
a	a	k8xC	a
IE	IE	kA	IE
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
vylepšeným	vylepšený	k2eAgNnSc7d1	vylepšené
jádrem	jádro	k1gNnSc7	jádro
IE	IE	kA	IE
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
díky	díky	k7c3	díky
speciálnímu	speciální	k2eAgInSc3d1	speciální
meta	meta	k1gFnSc1	meta
elementu	element	k1gInSc2	element
vyvolat	vyvolat	k5eAaPmF	vyvolat
renderování	renderování	k1gNnSc4	renderování
IE7	IE7	k1gFnSc2	IE7
nebo	nebo	k8xC	nebo
IE	IE	kA	IE
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
vydal	vydat	k5eAaPmAgInS	vydat
první	první	k4xOgFnSc4	první
testovací	testovací	k2eAgFnSc4d1	testovací
verzi	verze	k1gFnSc4	verze
s	s	k7c7	s
názvem	název	k1gInSc7	název
Platform	Platform	k1gInSc1	Platform
Preview	Preview	k1gFnPc2	Preview
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Finální	finální	k2eAgFnSc1d1	finální
verze	verze	k1gFnSc1	verze
Internet	Internet	k1gInSc4	Internet
Explorer	Explorer	k1gInSc1	Explorer
10	[number]	k4	10
vyšla	vyjít	k5eAaPmAgFnS	vyjít
společně	společně	k6eAd1	společně
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
Windows	Windows	kA	Windows
8	[number]	k4	8
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
i	i	k8xC	i
předběžná	předběžný	k2eAgFnSc1d1	předběžná
verze	verze	k1gFnSc1	verze
(	(	kIx(	(
<g/>
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgFnPc1d1	funkční
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
finální	finální	k2eAgNnSc1d1	finální
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
systém	systém	k1gInSc4	systém
Windows	Windows	kA	Windows
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nejnovější	nový	k2eAgFnSc1d3	nejnovější
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Windows	Windows	kA	Windows
8.1	[number]	k4	8.1
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2013	[number]	k4	2013
<g/>
..	..	k?	..
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
verzi	verze	k1gFnSc6	verze
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
3D	[number]	k4	3D
grafika	grafikon	k1gNnSc2	grafikon
například	například	k6eAd1	například
u	u	k7c2	u
Bing	bingo	k1gNnPc2	bingo
Map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
implementována	implementován	k2eAgFnSc1d1	implementována
podpora	podpora	k1gFnSc1	podpora
protokolu	protokol	k1gInSc2	protokol
SPDY	SPDY	kA	SPDY
<g/>
.	.	kIx.	.
</s>
<s>
Webový	webový	k2eAgInSc1d1	webový
prohlížeč	prohlížeč	k1gInSc1	prohlížeč
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc4	Explorer
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
verze	verze	k1gFnPc1	verze
6	[number]	k4	6
či	či	k8xC	či
starší	starý	k2eAgMnSc1d2	starší
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zhusta	zhusta	k6eAd1	zhusta
kritizován	kritizován	k2eAgInSc1d1	kritizován
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ta	ten	k3xDgFnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
šíření	šíření	k1gNnSc4	šíření
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
spyware	spywar	k1gMnSc5	spywar
či	či	k8xC	či
adware	adwar	k1gMnSc5	adwar
<g/>
.	.	kIx.	.
</s>
<s>
Chyby	chyba	k1gFnPc1	chyba
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
prohlížeči	prohlížeč	k1gInSc6	prohlížeč
bývají	bývat	k5eAaImIp3nP	bývat
mnohdy	mnohdy	k6eAd1	mnohdy
zneužívány	zneužívat	k5eAaImNgInP	zneužívat
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
systémem	systém	k1gInSc7	systém
či	či	k8xC	či
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
citlivých	citlivý	k2eAgInPc2d1	citlivý
údajů	údaj	k1gInPc2	údaj
o	o	k7c6	o
uživatelích	uživatel	k1gMnPc6	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
tvůrcům	tvůrce	k1gMnPc3	tvůrce
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
též	též	k9	též
nejednou	jednou	k6eNd1	jednou
vyčítala	vyčítat	k5eAaImAgFnS	vyčítat
malá	malý	k2eAgFnSc1d1	malá
podpora	podpora	k1gFnSc1	podpora
webových	webový	k2eAgInPc2d1	webový
standardů	standard	k1gInPc2	standard
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
častá	častý	k2eAgFnSc1d1	častá
chybná	chybný	k2eAgFnSc1d1	chybná
implementace	implementace	k1gFnSc1	implementace
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
vývojářů	vývojář	k1gMnPc2	vývojář
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
společnost	společnost	k1gFnSc1	společnost
Microsoft	Microsoft	kA	Microsoft
brzdí	brzdit	k5eAaImIp3nS	brzdit
rozvoj	rozvoj	k1gInSc4	rozvoj
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
webových	webový	k2eAgFnPc2d1	webová
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
verze	verze	k1gFnPc1	verze
však	však	k9	však
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
přinášejí	přinášet	k5eAaImIp3nP	přinášet
mnohá	mnohý	k2eAgNnPc1d1	mnohé
zlepšení	zlepšení	k1gNnPc1	zlepšení
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
podpory	podpora	k1gFnSc2	podpora
HTML	HTML	kA	HTML
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
kritiky	kritika	k1gFnSc2	kritika
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gMnSc1	Explorer
souvisí	souviset	k5eAaImIp3nP	souviset
se	s	k7c7	s
zájmy	zájem	k1gInPc7	zájem
o	o	k7c4	o
větší	veliký	k2eAgFnSc4d2	veliký
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
kvůli	kvůli	k7c3	kvůli
spywaru	spywar	k1gInSc3	spywar
<g/>
,	,	kIx,	,
adwaru	adwar	k1gInSc3	adwar
a	a	k8xC	a
počítačovým	počítačový	k2eAgFnPc3d1	počítačová
virům	vir	k1gInPc3	vir
šířícím	šířící	k2eAgFnPc3d1	šířící
se	se	k3xPyFc4	se
napříč	napříč	k7c7	napříč
Internetem	Internet	k1gInSc7	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
využívaly	využívat	k5eAaImAgFnP	využívat
chyby	chyba	k1gFnPc1	chyba
a	a	k8xC	a
kazy	kaz	k1gInPc1	kaz
v	v	k7c6	v
bezpečnostní	bezpečnostní	k2eAgFnSc6d1	bezpečnostní
architektuře	architektura	k1gFnSc6	architektura
Internet	Internet	k1gInSc1	Internet
Exploreru	Explorer	k1gInSc6	Explorer
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
stačí	stačit	k5eAaBmIp3nS	stačit
jenom	jenom	k9	jenom
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
podvodnou	podvodný	k2eAgFnSc4d1	podvodná
webovou	webový	k2eAgFnSc4d1	webová
stránku	stránka	k1gFnSc4	stránka
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
spyware	spywar	k1gMnSc5	spywar
mohl	moct	k5eAaImAgInS	moct
nainstalovat	nainstalovat	k5eAaPmF	nainstalovat
na	na	k7c4	na
váš	váš	k3xOp2gInSc4	váš
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
drive-by	drivea	k1gFnPc4	drive-ba
download	download	k6eAd1	download
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
pokus	pokus	k1gInSc4	pokus
oklamat	oklamat	k5eAaPmF	oklamat
uživatele	uživatel	k1gMnPc4	uživatel
a	a	k8xC	a
instalací	instalace	k1gFnSc7	instalace
podvodného	podvodný	k2eAgInSc2d1	podvodný
softwaru	software	k1gInSc2	software
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Internet	Internet	k1gInSc1	Internet
Explorer	Explorra	k1gFnPc2	Explorra
není	být	k5eNaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
všudypřítomnost	všudypřítomnost	k1gFnSc1	všudypřítomnost
má	mít	k5eAaImIp3nS	mít
následky	následek	k1gInPc4	následek
ve	v	k7c6	v
spoustě	spousta	k1gFnSc6	spousta
napadených	napadený	k2eAgMnPc2d1	napadený
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
nereagoval	reagovat	k5eNaBmAgInS	reagovat
tak	tak	k9	tak
rychle	rychle	k6eAd1	rychle
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
konkurence	konkurence	k1gFnSc1	konkurence
v	v	k7c6	v
opravování	opravování	k1gNnSc6	opravování
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
děr	děra	k1gFnPc2	děra
a	a	k8xC	a
vytváření	vytváření	k1gNnSc6	vytváření
dostupných	dostupný	k2eAgFnPc2d1	dostupná
záplat	záplata	k1gFnPc2	záplata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
tím	ten	k3xDgNnSc7	ten
dal	dát	k5eAaPmAgInS	dát
podvodným	podvodný	k2eAgFnPc3d1	podvodná
webovým	webový	k2eAgFnPc3d1	webová
stránkám	stránka	k1gFnPc3	stránka
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gMnPc4	on
zneužívaly	zneužívat	k5eAaImAgFnP	zneužívat
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
nové	nový	k2eAgFnSc2d1	nová
záplaty	záplata	k1gFnSc2	záplata
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k8xC	však
nové	nový	k2eAgFnPc4d1	nová
verze	verze	k1gFnPc4	verze
Internet	Internet	k1gInSc4	Internet
Exploreru	Explorer	k1gInSc2	Explorer
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
8	[number]	k4	8
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
)	)	kIx)	)
udělaly	udělat	k5eAaPmAgFnP	udělat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
velký	velký	k2eAgInSc4d1	velký
pokrok	pokrok	k1gInSc4	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
buďtež	buďtež	k6eAd1	buďtež
nezávislé	závislý	k2eNgInPc4d1	nezávislý
testy	test	k1gInPc4	test
NSS	NSS	kA	NSS
Labs	Labs	k1gInSc4	Labs
provedené	provedený	k2eAgNnSc1d1	provedené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yRgMnPc6	který
byla	být	k5eAaImAgFnS	být
testována	testovat	k5eAaImNgFnS	testovat
schopnost	schopnost	k1gFnSc1	schopnost
prohlížeče	prohlížeč	k1gInSc2	prohlížeč
zabránit	zabránit	k5eAaPmF	zabránit
uživatelům	uživatel	k1gMnPc3	uživatel
ve	v	k7c6	v
stažení	stažení	k1gNnSc6	stažení
malwaru	malwar	k1gInSc2	malwar
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
9	[number]	k4	9
dokázal	dokázat	k5eAaPmAgInS	dokázat
zachytit	zachytit	k5eAaPmF	zachytit
98,2	[number]	k4	98,2
%	%	kIx~	%
malwaru	malwar	k1gInSc2	malwar
<g/>
,	,	kIx,	,
konkurenční	konkurenční	k2eAgInPc1d1	konkurenční
prohlížeče	prohlížeč	k1gInPc1	prohlížeč
se	se	k3xPyFc4	se
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
testy	test	k1gInPc1	test
nicméně	nicméně	k8xC	nicméně
netestovaly	testovat	k5eNaImAgInP	testovat
celkovou	celkový	k2eAgFnSc4d1	celková
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc4	jeho
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
malwarem	malwar	k1gInSc7	malwar
šířeným	šířený	k2eAgInSc7d1	šířený
pomocí	pomocí	k7c2	pomocí
technik	technika	k1gFnPc2	technika
sociálního	sociální	k2eAgNnSc2d1	sociální
inženýrství	inženýrství	k1gNnSc2	inženýrství
(	(	kIx(	(
<g/>
Socially-engineered	Sociallyngineered	k1gInSc1	Socially-engineered
malware	malwar	k1gMnSc5	malwar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mozilla	Mozilla	k6eAd1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
Safari	safari	k1gNnSc2	safari
Opera	opera	k1gFnSc1	opera
Google	Google	k1gFnSc1	Google
Chrome	chromat	k5eAaImIp3nS	chromat
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Internet	Internet	k1gInSc1	Internet
Explorer	Explorero	k1gNnPc2	Explorero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
Internet	Internet	k1gInSc1	Internet
Exploreru	Explorera	k1gFnSc4	Explorera
na	na	k7c6	na
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
Internet	Internet	k1gInSc1	Internet
Exploreru	Explorera	k1gFnSc4	Explorera
na	na	k7c6	na
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Blog	Blog	k1gInSc1	Blog
produktového	produktový	k2eAgInSc2d1	produktový
týmu	tým	k1gInSc2	tým
Windows	Windows	kA	Windows
Internet	Internet	k1gInSc1	Internet
Explorer	Explorer	k1gInSc1	Explorer
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
aktuální	aktuální	k2eAgFnSc6d1	aktuální
verzi	verze	k1gFnSc6	verze
Internet	Internet	k1gInSc4	Internet
Explorer	Explorer	k1gInSc1	Explorer
9	[number]	k4	9
na	na	k7c6	na
Živě	Živa	k1gFnSc6	Živa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
