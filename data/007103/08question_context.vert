<s>
Světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
část	část	k1gFnSc1	část
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
frekvence	frekvence	k1gFnSc1	frekvence
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
3,9	[number]	k4	3,9
<g/>
×	×	k?	×
<g/>
1014	[number]	k4	1014
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
hertz	hertz	k1gInSc1	hertz
<g/>
)	)	kIx)	)
do	do	k7c2	do
7,9	[number]	k4	7,9
<g/>
×	×	k?	×
<g/>
1014	[number]	k4	1014
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
z	z	k7c2	z
intervalu	interval	k1gInSc2	interval
390	[number]	k4	390
<g/>
–	–	k?	–
<g/>
760	[number]	k4	760
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
