<s>
Coventry	Coventr	k1gMnPc4	Coventr
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
a	a	k8xC	a
metropolitní	metropolitní	k2eAgInSc1d1	metropolitní
distrikt	distrikt	k1gInSc1	distrikt
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
hrabství	hrabství	k1gNnSc6	hrabství
West	West	k2eAgInSc4d1	West
Midlands	Midlands	k1gInSc4	Midlands
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
mělo	mít	k5eAaImAgNnS	mít
Coventry	Coventr	k1gInPc4	Coventr
329	[number]	k4	329
810	[number]	k4	810
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
desátým	desátý	k4xOgNnSc7	desátý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
třináctým	třináctý	k4xOgNnSc7	třináctý
městem	město	k1gNnSc7	město
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
po	po	k7c6	po
Birminghamu	Birmingham	k1gInSc6	Birmingham
druhé	druhý	k4xOgFnSc2	druhý
v	v	k7c4	v
Midlands	Midlands	k1gInSc4	Midlands
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
153	[number]	k4	153
km	km	kA	km
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Birminghamu	Birmingham	k1gInSc2	Birmingham
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejvzdálenějším	vzdálený	k2eAgNnSc7d3	nejvzdálenější
městem	město	k1gNnSc7	město
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
svou	svůj	k3xOyFgFnSc7	svůj
moderní	moderní	k2eAgFnSc7d1	moderní
katedrálou	katedrála	k1gFnSc7	katedrála
postavenou	postavený	k2eAgFnSc7d1	postavená
po	po	k7c6	po
bombardování	bombardování	k1gNnSc6	bombardování
původní	původní	k2eAgFnSc2d1	původní
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
příspěvkem	příspěvek	k1gInSc7	příspěvek
místních	místní	k2eAgInPc2d1	místní
společností	společnost	k1gFnSc7	společnost
automobilovému	automobilový	k2eAgInSc3d1	automobilový
průmyslu	průmysl	k1gInSc3	průmysl
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc1	sídlo
dvou	dva	k4xCgFnPc2	dva
univerzit	univerzita	k1gFnPc2	univerzita
-	-	kIx~	-
Coventry	Coventr	k1gInPc1	Coventr
University	universita	k1gFnSc2	universita
a	a	k8xC	a
University	universita	k1gFnSc2	universita
of	of	k?	of
Warwick	Warwick	k1gMnSc1	Warwick
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Coventry	Coventr	k1gInPc4	Coventr
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1043	[number]	k4	1043
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
Leofric	Leofric	k1gMnSc1	Leofric
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Mercie	Mercie	k1gFnSc2	Mercie
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
Godiva	Godivo	k1gNnSc2	Godivo
<g/>
,	,	kIx,	,
nechali	nechat	k5eAaPmAgMnP	nechat
postavit	postavit	k5eAaPmF	postavit
benediktýnské	benediktýnský	k2eAgNnSc4d1	benediktýnské
opatství	opatství	k1gNnSc4	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
výzkumy	výzkum	k1gInPc1	výzkum
ale	ale	k8xC	ale
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
existenci	existence	k1gFnSc4	existence
tohoto	tento	k3xDgNnSc2	tento
opatství	opatství	k1gNnSc2	opatství
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1022	[number]	k4	1022
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
král	král	k1gMnSc1	král
Leofric	Leofric	k1gMnSc1	Leofric
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
již	již	k6eAd1	již
postavené	postavený	k2eAgNnSc4d1	postavené
opatství	opatství	k1gNnSc4	opatství
pouze	pouze	k6eAd1	pouze
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
městský	městský	k2eAgInSc1d1	městský
trh	trh	k1gInSc1	trh
u	u	k7c2	u
brán	brán	k2eAgInSc1d1	brán
opatství	opatství	k1gNnSc2	opatství
a	a	k8xC	a
osada	osada	k1gFnSc1	osada
se	se	k3xPyFc4	se
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
centrem	centr	k1gInSc7	centr
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
plátnem	plátno	k1gNnSc7	plátno
a	a	k8xC	a
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
a	a	k8xC	a
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
měst	město	k1gNnPc2	město
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc4	status
města	město	k1gNnSc2	město
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
roku	rok	k1gInSc2	rok
1345	[number]	k4	1345
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
samostatným	samostatný	k2eAgNnSc7d1	samostatné
hrabstvím	hrabství	k1gNnSc7	hrabství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Coventry	Coventr	k1gMnPc7	Coventr
stalo	stát	k5eAaPmAgNnS	stát
centrem	centrum	k1gNnSc7	centrum
výroby	výroba	k1gFnSc2	výroba
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	dík	k1gInPc1	dík
společnosti	společnost	k1gFnSc2	společnost
Rover	rover	k1gMnSc1	rover
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
odvětví	odvětví	k1gNnSc2	odvětví
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
výroba	výroba	k1gFnSc1	výroba
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
střediskem	středisko	k1gNnSc7	středisko
motorového	motorový	k2eAgInSc2d1	motorový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Coventry	Coventr	k1gInPc7	Coventr
stalo	stát	k5eAaPmAgNnS	stát
terčem	terč	k1gInSc7	terč
masivních	masivní	k2eAgInPc2d1	masivní
náletů	nálet	k1gInPc2	nálet
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zničily	zničit	k5eAaPmAgFnP	zničit
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
původní	původní	k2eAgFnSc4d1	původní
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
Plymouthem	Plymouth	k1gInSc7	Plymouth
bylo	být	k5eAaImAgNnS	být
Coventry	Coventr	k1gInPc7	Coventr
nejvíce	nejvíce	k6eAd1	nejvíce
poškozenými	poškozený	k2eAgInPc7d1	poškozený
anglickými	anglický	k2eAgInPc7d1	anglický
městy	město	k1gNnPc7	město
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
napadeno	napadnout	k5eAaPmNgNnS	napadnout
především	především	k6eAd1	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
centrem	centrum	k1gNnSc7	centrum
výroby	výroba	k1gFnSc2	výroba
munice	munice	k1gFnSc2	munice
a	a	k8xC	a
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
náletech	nálet	k1gInPc6	nálet
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
historických	historický	k2eAgFnPc2d1	historická
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
obnova	obnova	k1gFnSc1	obnova
nebyla	být	k5eNaImAgFnS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gMnSc1	Churchill
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1940	[number]	k4	1940
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nevarovat	varovat	k5eNaImF	varovat
obyvatele	obyvatel	k1gMnSc4	obyvatel
města	město	k1gNnSc2	město
před	před	k7c7	před
hrozícím	hrozící	k2eAgInSc7d1	hrozící
náletem	nálet	k1gInSc7	nálet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Němci	Němec	k1gMnPc1	Němec
nepojali	pojmout	k5eNaPmAgMnP	pojmout
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
Britové	Brit	k1gMnPc1	Brit
dokážou	dokázat	k5eAaPmIp3nP	dokázat
prolomit	prolomit	k5eAaPmF	prolomit
kódování	kódování	k1gNnSc4	kódování
zpráv	zpráva	k1gFnPc2	zpráva
pomocí	pomocí	k7c2	pomocí
přístroje	přístroj	k1gInSc2	přístroj
Enigma	enigma	k1gFnSc1	enigma
se	se	k3xPyFc4	se
nezakládá	zakládat	k5eNaImIp3nS	zakládat
na	na	k7c6	na
pravdě	pravda	k1gFnSc6	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Coventry	Coventr	k1gInPc1	Coventr
a	a	k8xC	a
Lidice	Lidice	k1gInPc1	Lidice
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sesterskými	sesterský	k2eAgNnPc7d1	sesterské
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
podle	podle	k7c2	podle
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
Gibsonova	Gibsonův	k2eAgInSc2d1	Gibsonův
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
orientované	orientovaný	k2eAgNnSc4d1	orientované
jako	jako	k8xC	jako
pěší	pěší	k2eAgFnSc1d1	pěší
zóna	zóna	k1gFnSc1	zóna
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
tak	tak	k6eAd1	tak
velkém	velký	k2eAgInSc6d1	velký
rozsahu	rozsah	k1gInSc6	rozsah
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
i	i	k8xC	i
nová	nový	k2eAgFnSc1d1	nová
katedrála	katedrála	k1gFnSc1	katedrála
Svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
(	(	kIx(	(
<g/>
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
tapisérií	tapisérie	k1gFnSc7	tapisérie
na	na	k7c6	na
světě	svět	k1gInSc6	svět
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
této	tento	k3xDgFnSc2	tento
obnovy	obnova	k1gFnSc2	obnova
dostalo	dostat	k5eAaPmAgNnS	dostat
město	město	k1gNnSc1	město
Coventry	Coventr	k1gMnPc4	Coventr
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
jako	jako	k8xS	jako
šítonoše	šítonoš	k1gMnSc2	šítonoš
fénixe	fénix	k1gMnSc2	fénix
který	který	k3yIgMnSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
znovuzrození	znovuzrození	k1gNnSc1	znovuzrození
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
obory	obor	k1gInPc7	obor
v	v	k7c4	v
Coventry	Coventr	k1gMnPc4	Coventr
jsou	být	k5eAaImIp3nP	být
výroba	výroba	k1gFnSc1	výroba
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
elektronických	elektronický	k2eAgFnPc2d1	elektronická
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
umělých	umělý	k2eAgNnPc2d1	umělé
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
součástek	součástka	k1gFnPc2	součástka
pro	pro	k7c4	pro
letecký	letecký	k2eAgInSc4d1	letecký
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
telekomunikace	telekomunikace	k1gFnPc4	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
sledovat	sledovat	k5eAaImF	sledovat
odklon	odklon	k1gInSc4	odklon
od	od	k7c2	od
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
finančním	finanční	k2eAgFnPc3d1	finanční
službám	služba	k1gFnPc3	služba
<g/>
,	,	kIx,	,
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
designu	design	k1gInSc2	design
zásobování	zásobování	k1gNnSc2	zásobování
a	a	k8xC	a
turistice	turistika	k1gFnSc6	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Coventry	Coventrum	k1gNnPc7	Coventrum
bylo	být	k5eAaImAgNnS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
centrem	centrum	k1gNnSc7	centrum
výroby	výroba	k1gFnSc2	výroba
jízdních	jízdní	k2eAgNnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
společnosti	společnost	k1gFnPc4	společnost
působící	působící	k2eAgFnPc4d1	působící
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
v	v	k7c4	v
Coventry	Coventr	k1gMnPc4	Coventr
patří	patřit	k5eAaImIp3nP	patřit
méně	málo	k6eAd2	málo
známé	známý	k2eAgInPc1d1	známý
Coventry	Coventr	k1gInPc1	Coventr
Motette	Motett	k1gInSc5	Motett
<g/>
,	,	kIx,	,
Great	Great	k2eAgInSc4d1	Great
Horseless	Horseless	k1gInSc4	Horseless
Carriage	Carriage	k1gNnSc2	Carriage
Co	co	k9	co
<g/>
,	,	kIx,	,
Swift	Swift	k2eAgInSc1d1	Swift
nebo	nebo	k8xC	nebo
známější	známý	k2eAgInSc1d2	známější
Humber	Humber	k1gInSc1	Humber
<g/>
,	,	kIx,	,
Riley	Riley	k1gInPc1	Riley
<g/>
,	,	kIx,	,
Daimler	Daimler	k1gInSc1	Daimler
a	a	k8xC	a
Triumph	Triumph	k1gInSc1	Triumph
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zažíval	zažívat	k5eAaImAgMnS	zažívat
motorový	motorový	k2eAgInSc4d1	motorový
průmysl	průmysl	k1gInSc4	průmysl
v	v	k7c4	v
Coventry	Coventr	k1gMnPc4	Coventr
výrazný	výrazný	k2eAgInSc1d1	výrazný
růst	růst	k1gInSc1	růst
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
následován	následovat	k5eAaImNgInS	následovat
znatelným	znatelný	k2eAgInSc7d1	znatelný
poklesem	pokles	k1gInSc7	pokles
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
což	což	k3yRnSc1	což
silně	silně	k6eAd1	silně
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
mělo	mít	k5eAaImAgNnS	mít
Coventry	Coventr	k1gMnPc4	Coventr
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
procent	procento	k1gNnPc2	procento
nezaměstnaných	zaměstnaný	k2eNgMnPc2d1	nezaměstnaný
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
vlivem	vlivem	k7c2	vlivem
rozvoje	rozvoj	k1gInSc2	rozvoj
jiných	jiný	k2eAgNnPc2d1	jiné
odvětví	odvětví	k1gNnPc2	odvětví
pomalu	pomalu	k6eAd1	pomalu
zotavuje	zotavovat	k5eAaImIp3nS	zotavovat
ale	ale	k8xC	ale
význam	význam	k1gInSc4	význam
motorového	motorový	k2eAgInSc2d1	motorový
průmyslu	průmysl	k1gInSc2	průmysl
stále	stále	k6eAd1	stále
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
pokles	pokles	k1gInSc4	pokles
významu	význam	k1gInSc2	význam
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
v	v	k7c4	v
Coventry	Coventr	k1gInPc4	Coventr
vývojové	vývojový	k2eAgNnSc4d1	vývojové
centrum	centrum	k1gNnSc4	centrum
Jaguar	Jaguara	k1gFnPc2	Jaguara
(	(	kIx(	(
<g/>
v	v	k7c4	v
Allesey	Allesey	k1gInPc4	Allesey
a	a	k8xC	a
Whitley	Whitley	k1gInPc4	Whitley
<g/>
)	)	kIx)	)
a	a	k8xC	a
Peugeot	peugeot	k1gInSc1	peugeot
zde	zde	k6eAd1	zde
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
některé	některý	k3yIgFnPc4	některý
součásti	součást	k1gFnPc4	součást
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgInPc1d1	známý
černé	černý	k2eAgInPc1d1	černý
vozy	vůz	k1gInPc1	vůz
(	(	kIx(	(
<g/>
black	black	k6eAd1	black
cab	cabit	k5eAaImRp2nS	cabit
<g/>
)	)	kIx)	)
londýnské	londýnský	k2eAgFnSc2d1	londýnská
taxislužby	taxislužba	k1gFnSc2	taxislužba
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
společnost	společnost	k1gFnSc1	společnost
LTI	LTI	kA	LTI
v	v	k7c4	v
Coventry	Coventr	k1gInPc4	Coventr
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgInPc1d1	jediný
typy	typ	k1gInPc1	typ
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
celé	celý	k2eAgNnSc1d1	celé
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Coventry	Coventr	k1gInPc1	Coventr
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
400	[number]	k4	400
let	léto	k1gNnPc2	léto
samostatným	samostatný	k2eAgNnSc7d1	samostatné
hrabstvím	hrabství	k1gNnSc7	hrabství
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
součástí	součást	k1gFnSc7	součást
hrabství	hrabství	k1gNnSc2	hrabství
Warwickshire	Warwickshir	k1gInSc5	Warwickshir
<g/>
.	.	kIx.	.
</s>
<s>
Distriktem	distrikt	k1gInSc7	distrikt
<g/>
,	,	kIx,	,
nezávislým	závislý	k2eNgNnSc7d1	nezávislé
na	na	k7c4	na
hrabství	hrabství	k1gNnSc4	hrabství
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
správní	správní	k2eAgFnSc6d1	správní
reformě	reforma	k1gFnSc6	reforma
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
metropolitním	metropolitní	k2eAgInSc7d1	metropolitní
distriktem	distrikt	k1gInSc7	distrikt
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hrabství	hrabství	k1gNnSc2	hrabství
West	Westa	k1gFnPc2	Westa
Midlands	Midlandsa	k1gFnPc2	Midlandsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
byla	být	k5eAaImAgFnS	být
rada	rada	k1gFnSc1	rada
hrabství	hrabství	k1gNnSc2	hrabství
West	Westa	k1gFnPc2	Westa
Midlands	Midlandsa	k1gFnPc2	Midlandsa
zrušena	zrušen	k2eAgFnSc1d1	zrušena
a	a	k8xC	a
Coventry	Coventr	k1gMnPc7	Coventr
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
správní	správní	k2eAgFnSc7d1	správní
jednotkou	jednotka	k1gFnSc7	jednotka
(	(	kIx(	(
<g/>
unitary	unitara	k1gFnSc2	unitara
authority	authorita	k1gFnSc2	authorita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Coventry	Coventr	k1gInPc4	Coventr
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
silně	silně	k6eAd1	silně
spojeno	spojen	k2eAgNnSc1d1	spojeno
s	s	k7c7	s
tradičním	tradiční	k2eAgNnSc7d1	tradiční
hrabstvím	hrabství	k1gNnSc7	hrabství
Warwickshire	Warwickshir	k1gMnSc5	Warwickshir
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
jeho	jeho	k3xOp3gFnSc7	jeho
geografickou	geografický	k2eAgFnSc7d1	geografická
polohou	poloha	k1gFnSc7	poloha
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
hrabství	hrabství	k1gNnSc3	hrabství
<g/>
.	.	kIx.	.
</s>
<s>
Coventry	Coventr	k1gMnPc4	Coventr
je	být	k5eAaImIp3nS	být
řízeno	řízen	k2eAgNnSc1d1	řízeno
radou	rada	k1gMnSc7	rada
města	město	k1gNnSc2	město
Coventry	Coventr	k1gMnPc7	Coventr
(	(	kIx(	(
<g/>
Coventry	Coventr	k1gMnPc7	Coventr
City	City	k1gFnSc2	City
Council	Council	k1gInSc1	Council
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
18	[number]	k4	18
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
do	do	k7c2	do
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
třemi	tři	k4xCgInPc7	tři
radními	radní	k1gMnPc7	radní
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
místní	místní	k2eAgFnPc1d1	místní
služby	služba	k1gFnPc1	služba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
policejní	policejní	k2eAgFnPc4d1	policejní
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
požární	požární	k2eAgFnSc4d1	požární
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
orgány	orgán	k1gInPc1	orgán
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
West	West	k2eAgInSc4d1	West
Midlands	Midlands	k1gInSc4	Midlands
<g/>
.	.	kIx.	.
</s>
<s>
Coventry	Coventr	k1gInPc4	Coventr
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
volební	volební	k2eAgInPc4d1	volební
obvody	obvod	k1gInPc4	obvod
-	-	kIx~	-
Coventry	Coventr	k1gInPc4	Coventr
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gInPc1	Coventr
jih	jih	k1gInSc1	jih
a	a	k8xC	a
Coventry	Coventr	k1gMnPc4	Coventr
severozápad	severozápad	k1gInSc4	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Coventry	Coventr	k1gInPc1	Coventr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
dálnic	dálnice	k1gFnPc2	dálnice
M	M	kA	M
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
M69	M69	k1gMnPc2	M69
a	a	k8xC	a
M40	M40	k1gMnPc2	M40
a	a	k8xC	a
rychlostních	rychlostní	k2eAgFnPc2d1	rychlostní
komunikací	komunikace	k1gFnPc2	komunikace
A45	A45	k1gFnSc2	A45
a	a	k8xC	a
A	a	k9	a
<g/>
46	[number]	k4	46
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
sevřeno	sevřít	k5eAaPmNgNnS	sevřít
dopravním	dopravní	k2eAgInSc7d1	dopravní
okruhem	okruh	k1gInSc7	okruh
dálničního	dálniční	k2eAgInSc2d1	dálniční
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc1d1	doplněný
nedávno	nedávno	k6eAd1	nedávno
vybudovaným	vybudovaný	k2eAgInSc7d1	vybudovaný
severo	severo	k1gNnSc1	severo
jižním	jižní	k2eAgInSc7d1	jižní
průtahem	průtah	k1gInSc7	průtah
Phoenix	Phoenix	k1gInSc1	Phoenix
Way	Way	k1gFnPc2	Way
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
v	v	k7c4	v
Coventry	Coventr	k1gInPc4	Coventr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
West	West	k1gMnSc1	West
Coast	Coast	k1gMnSc1	Coast
Main	Main	k1gMnSc1	Main
Line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
spojení	spojení	k1gNnSc1	spojení
zastávkovými	zastávkový	k2eAgInPc7d1	zastávkový
vlaky	vlak	k1gInPc7	vlak
dopravce	dopravce	k1gMnSc1	dopravce
Central	Central	k1gMnSc1	Central
trains	trains	k6eAd1	trains
s	s	k7c7	s
Northamptonem	Northampton	k1gInSc7	Northampton
<g/>
,	,	kIx,	,
Birminghamem	Birmingham	k1gInSc7	Birmingham
a	a	k8xC	a
Nuneatonem	Nuneaton	k1gInSc7	Nuneaton
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Bedworth	Bedworth	k1gInSc4	Bedworth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dálkovou	dálkový	k2eAgFnSc4d1	dálková
dopravu	doprava	k1gFnSc4	doprava
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
vlaky	vlak	k1gInPc1	vlak
Virgin	Virgina	k1gFnPc2	Virgina
trainsu	trains	k1gInSc2	trains
relací	relace	k1gFnPc2	relace
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
Wolverhampton	Wolverhampton	k1gInSc1	Wolverhampton
a	a	k8xC	a
Skotsko	Skotsko	k1gNnSc1	Skotsko
-	-	kIx~	-
Bournemouth	Bournemouth	k1gInSc1	Bournemouth
<g/>
/	/	kIx~	/
<g/>
Brighton	Brighton	k1gInSc1	Brighton
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgNnSc1d1	jižní
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Coventry	Coventr	k1gMnPc4	Coventr
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
dvě	dva	k4xCgFnPc4	dva
příměstské	příměstský	k2eAgFnPc4d1	příměstská
železniční	železniční	k2eAgFnPc4d1	železniční
stanice	stanice	k1gFnPc4	stanice
-	-	kIx~	-
Canley	Canlea	k1gFnPc4	Canlea
a	a	k8xC	a
Tile	til	k1gInSc5	til
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c4	v
Coventry	Coventr	k1gMnPc4	Coventr
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dopravci	dopravce	k1gMnPc1	dopravce
Travel	Travel	k1gInSc4	Travel
Coventry	Coventr	k1gInPc7	Coventr
<g/>
,	,	kIx,	,
Travel	Travel	k1gMnSc1	Travel
De	De	k?	De
Courcey	Courcea	k1gFnSc2	Courcea
a	a	k8xC	a
Stagecoach	Stagecoacha	k1gFnPc2	Stagecoacha
Warwickshire	Warwickshir	k1gInSc5	Warwickshir
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližšími	blízký	k2eAgNnPc7d3	nejbližší
letišti	letiště	k1gNnPc7	letiště
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
Coventry	Coventr	k1gInPc4	Coventr
jsou	být	k5eAaImIp3nP	být
Letiště	letiště	k1gNnPc1	letiště
Coventry	Coventrum	k1gNnPc7	Coventrum
v	v	k7c6	v
Bagintonu	Baginton	k1gInSc6	Baginton
asi	asi	k9	asi
8	[number]	k4	8
km	km	kA	km
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
města	město	k1gNnSc2	město
odkud	odkud	k6eAd1	odkud
společnost	společnost	k1gFnSc1	společnost
Thomsonfly	Thomsonfly	k1gMnPc2	Thomsonfly
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
obchodní	obchodní	k2eAgInPc4d1	obchodní
lety	let	k1gInPc4	let
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
měst	město	k1gNnPc2	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nadregionálním	nadregionální	k2eAgInSc7d1	nadregionální
leteckým	letecký	k2eAgInSc7d1	letecký
terminálem	terminál	k1gInSc7	terminál
je	být	k5eAaImIp3nS	být
Letiště	letiště	k1gNnSc1	letiště
Birmingham	Birmingham	k1gInSc1	Birmingham
International	International	k1gFnSc1	International
asi	asi	k9	asi
17	[number]	k4	17
km	km	kA	km
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Coventry	Coventr	k1gInPc4	Coventr
Canal	Canal	k1gInSc1	Canal
končí	končit	k5eAaImIp3nS	končit
nedaleko	nedaleko	k7c2	nedaleko
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
v	v	k7c4	v
Coventry	Coventr	k1gInPc4	Coventr
Canal	Canal	k1gMnSc1	Canal
Basin	Basin	k1gMnSc1	Basin
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
splavný	splavný	k2eAgMnSc1d1	splavný
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
61	[number]	k4	61
km	km	kA	km
až	až	k6eAd1	až
do	do	k7c2	do
Fradley	Fradlea	k1gFnSc2	Fradlea
Junction	Junction	k1gInSc1	Junction
v	v	k7c6	v
Staffordshire	Staffordshir	k1gMnSc5	Staffordshir
<g/>
.	.	kIx.	.
</s>
<s>
Bělehradské	bělehradský	k2eAgNnSc1d1	Bělehradské
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c4	v
Coventry	Coventr	k1gMnPc4	Coventr
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
prvním	první	k4xOgMnSc6	první
účelově	účelově	k6eAd1	účelově
postaveným	postavený	k2eAgNnSc7d1	postavené
občanským	občanský	k2eAgNnSc7d1	občanské
městským	městský	k2eAgNnSc7d1	Městské
divadlem	divadlo	k1gNnSc7	divadlo
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
rozšíření	rozšíření	k1gNnSc4	rozšíření
funkcí	funkce	k1gFnPc2	funkce
divadla	divadlo	k1gNnSc2	divadlo
jako	jako	k8xC	jako
vzdělávací	vzdělávací	k2eAgFnSc2d1	vzdělávací
instituce	instituce	k1gFnSc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
organizace	organizace	k1gFnSc1	organizace
však	však	k9	však
byla	být	k5eAaImAgFnS	být
městskou	městský	k2eAgFnSc7d1	městská
radou	rada	k1gFnSc7	rada
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
divadlem	divadlo	k1gNnSc7	divadlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
College	Collegus	k1gMnSc5	Collegus
Theatre	Theatr	k1gMnSc5	Theatr
<g/>
,	,	kIx,	,
s	s	k7c7	s
866	[number]	k4	866
sedadly	sedadlo	k1gNnPc7	sedadlo
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
divadel	divadlo	k1gNnPc2	divadlo
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc4d1	současný
Coventry	Coventr	k1gInPc4	Coventr
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
pořádaných	pořádaný	k2eAgFnPc2d1	pořádaná
hudebních	hudební	k2eAgFnPc2d1	hudební
akcí	akce	k1gFnPc2	akce
včetně	včetně	k7c2	včetně
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
jazzového	jazzový	k2eAgInSc2d1	jazzový
festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Godiva	Godivo	k1gNnSc2	Godivo
Festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Warwick	Warwick	k6eAd1	Warwick
Arts	Arts	k1gInSc1	Arts
Centre	centr	k1gInSc5	centr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
University	universita	k1gFnSc2	universita
of	of	k?	of
Warwick	Warwick	k1gMnSc1	Warwick
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
galerii	galerie	k1gFnSc4	galerie
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgInSc4d1	koncertní
sál	sál	k1gInSc4	sál
a	a	k8xC	a
kino	kino	k1gNnSc4	kino
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Barbican	Barbican	k1gMnSc1	Barbican
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
5,5	[number]	k4	5,5
km	km	kA	km
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Ricoh	Ricoh	k1gInSc1	Ricoh
Arena	Areno	k1gNnSc2	Areno
pro	pro	k7c4	pro
32	[number]	k4	32
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
stadión	stadión	k1gInSc4	stadión
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
hraje	hrát	k5eAaImIp3nS	hrát
svá	svůj	k3xOyFgNnPc4	svůj
utkání	utkání	k1gNnPc4	utkání
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
klub	klub	k1gInSc4	klub
Coventry	Coventrum	k1gNnPc7	Coventrum
City	City	k1gFnSc2	City
FC	FC	kA	FC
a	a	k8xC	a
pořádají	pořádat	k5eAaImIp3nP	pořádat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
velké	velký	k2eAgInPc1d1	velký
rockové	rockový	k2eAgInPc1d1	rockový
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zde	zde	k6eAd1	zde
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
Red	Red	k1gMnPc1	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc4	Chile
Peppers	Peppers	k1gInSc4	Peppers
nebo	nebo	k8xC	nebo
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hala	hala	k1gFnSc1	hala
pro	pro	k7c4	pro
6	[number]	k4	6
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
-	-	kIx~	-
Ricoh	Ricoh	k1gInSc1	Ricoh
Exhibition	Exhibition	k1gInSc1	Exhibition
Hall	Hall	k1gInSc1	Hall
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Coventry	Coventr	k1gInPc4	Coventr
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
univerzity	univerzita	k1gFnPc1	univerzita
-	-	kIx~	-
Coventry	Coventr	k1gInPc1	Coventr
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
studentském	studentský	k2eAgInSc6d1	studentský
areálu	areál	k1gInSc6	areál
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
University	universita	k1gFnSc2	universita
of	of	k?	of
Warwick	Warwick	k1gInSc1	Warwick
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
asi	asi	k9	asi
6	[number]	k4	6
km	km	kA	km
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Warwickshire	Warwickshir	k1gInSc5	Warwickshir
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
univerzita	univerzita	k1gFnSc1	univerzita
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
umístily	umístit	k5eAaPmAgFnP	umístit
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
desítce	desítka	k1gFnSc6	desítka
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kvality	kvalita	k1gFnSc2	kvalita
výuky	výuka	k1gFnSc2	výuka
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2007	[number]	k4	2007
získal	získat	k5eAaPmAgMnS	získat
prestižní	prestižní	k2eAgNnSc1d1	prestižní
ocenění	ocenění	k1gNnSc1	ocenění
BBC	BBC	kA	BBC
University	universita	k1gFnSc2	universita
Challenge	Challeng	k1gFnSc2	Challeng
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
také	také	k9	také
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc4	dva
velké	velká	k1gFnPc4	velká
vysokoškolské	vysokoškolský	k2eAgFnSc2d1	vysokoškolská
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
City	City	k1gFnSc1	City
College	College	k1gFnSc1	College
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
Coventry	Coventr	k1gInPc4	Coventr
Technical	Technical	k1gFnPc7	Technical
College	College	k1gFnPc2	College
a	a	k8xC	a
Tile	til	k1gInSc5	til
Hill	Hill	k1gMnSc1	Hill
College	Colleg	k1gMnSc2	Colleg
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
Henley	Henle	k2eAgMnPc4d1	Henle
College	Colleg	k1gMnPc4	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
střední	střední	k2eAgFnPc1d1	střední
školy	škola	k1gFnPc1	škola
v	v	k7c4	v
Coventry	Coventr	k1gInPc4	Coventr
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
jsou	být	k5eAaImIp3nP	být
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Finham	Finham	k1gInSc1	Finham
Park	park	k1gInSc1	park
School	School	k1gInSc1	School
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gMnPc4	Coventr
Blue	Blu	k1gInSc2	Blu
Coat	Coat	k1gMnSc1	Coat
Church	Church	k1gMnSc1	Church
of	of	k?	of
England	England	k1gInSc1	England
School	Schoola	k1gFnPc2	Schoola
se	se	k3xPyFc4	se
nedávno	nedávno	k6eAd1	nedávno
přeorientovala	přeorientovat	k5eAaPmAgFnS	přeorientovat
na	na	k7c4	na
hudební	hudební	k2eAgFnSc4d1	hudební
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
takto	takto	k6eAd1	takto
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
institucí	instituce	k1gFnPc2	instituce
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bishop	Bishop	k1gInSc1	Bishop
Ullathorne	Ullathorn	k1gInSc5	Ullathorn
RC	RC	kA	RC
School	School	k1gInSc1	School
se	se	k3xPyFc4	se
orientuje	orientovat	k5eAaBmIp3nS	orientovat
na	na	k7c4	na
humanitní	humanitní	k2eAgFnPc4d1	humanitní
obory	obora	k1gFnPc4	obora
<g/>
,	,	kIx,	,
Woodlands	Woodlands	k1gInSc1	Woodlands
School	Schoola	k1gFnPc2	Schoola
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
sportovní	sportovní	k2eAgFnPc4d1	sportovní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Exhall	Exhall	k1gMnSc1	Exhall
Grange	Grang	k1gFnSc2	Grang
School	School	k1gInSc1	School
a	a	k8xC	a
Science	Science	k1gFnSc1	Science
College	College	k1gFnSc1	College
<g/>
.	.	kIx.	.
</s>
<s>
Ernesford	Ernesford	k1gInSc1	Ernesford
Grange	Grange	k1gNnSc2	Grange
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
příštích	příští	k2eAgInPc2d1	příští
vědeckých	vědecký	k2eAgInPc2d1	vědecký
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Pattison	Pattison	k1gNnSc1	Pattison
College	Colleg	k1gInSc2	Colleg
<g/>
,	,	kIx,	,
soukromá	soukromý	k2eAgFnSc1d1	soukromá
škola	škola	k1gFnSc1	škola
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
dramatických	dramatický	k2eAgNnPc2d1	dramatické
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
sportovní	sportovní	k2eAgInPc1d1	sportovní
kluby	klub	k1gInPc1	klub
v	v	k7c4	v
Coventry	Coventr	k1gMnPc4	Coventr
-	-	kIx~	-
Coventry	Coventr	k1gMnPc7	Coventr
City	City	k1gFnSc2	City
F.	F.	kA	F.
<g/>
C.	C.	kA	C.
<g/>
,	,	kIx,	,
Coventry	Coventrum	k1gNnPc7	Coventrum
Sphinx	Sphinx	k1gInSc1	Sphinx
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gInPc1	Coventr
Copsewood	Copsewood	k1gInSc1	Copsewood
(	(	kIx(	(
<g/>
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gInPc4	Coventr
Rugby	rugby	k1gNnPc2	rugby
Club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
ragby	ragby	k1gNnSc1	ragby
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
City	City	k1gFnSc1	City
Of	Of	k1gFnPc1	Of
Coventry	Coventr	k1gInPc1	Coventr
Swimming	Swimming	k1gInSc4	Swimming
Club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
plavání	plavání	k1gNnSc1	plavání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gInPc1	Coventr
Blaze	blaze	k6eAd1	blaze
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gInPc1	Coventr
Bears	Bears	k1gInSc1	Bears
(	(	kIx(	(
<g/>
ragby	ragby	k1gNnSc1	ragby
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gInPc1	Coventr
Godiva	Godiva	k1gFnSc1	Godiva
Harriers	Harriers	k1gInSc1	Harriers
(	(	kIx(	(
<g/>
atletika	atletika	k1gFnSc1	atletika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gInPc1	Coventr
Bees	Bees	k1gInSc1	Bees
<g/>
(	(	kIx(	(
<g/>
plochá	plochý	k2eAgFnSc1d1	plochá
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gInPc1	Coventr
Crusaders	Crusaders	k1gInSc1	Crusaders
(	(	kIx(	(
<g/>
basketbal	basketbal	k1gInSc1	basketbal
<g/>
)	)	kIx)	)
a	a	k8xC	a
Coventry	Coventr	k1gInPc1	Coventr
Cassidy	Cassida	k1gFnSc2	Cassida
Jets	Jetsa	k1gFnPc2	Jetsa
(	(	kIx(	(
<g/>
americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgFnSc7d3	nejznámější
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
Coventry	Coventr	k1gInPc1	Coventr
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
katedrála	katedrála	k1gFnSc1	katedrála
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
zničena	zničit	k5eAaPmNgNnP	zničit
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
zachovala	zachovat	k5eAaPmAgFnS	zachovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc1	část
vnějších	vnější	k2eAgFnPc2d1	vnější
zdí	zeď	k1gFnPc2	zeď
a	a	k8xC	a
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
katedrála	katedrála	k1gFnSc1	katedrála
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
poblíž	poblíž	k7c2	poblíž
trosek	troska	k1gFnPc2	troska
původní	původní	k2eAgMnSc1d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
Basilem	Basil	k1gMnSc7	Basil
Spencem	Spenec	k1gMnSc7	Spenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tapisérie	tapisérie	k1gFnSc1	tapisérie
Svatořečení	svatořečení	k1gNnSc1	svatořečení
Krista	Kristus	k1gMnSc2	Kristus
od	od	k7c2	od
Grahama	Graham	k1gMnSc2	Graham
Sutherlanda	Sutherlando	k1gNnSc2	Sutherlando
<g/>
.	.	kIx.	.
</s>
<s>
Bronzová	bronzový	k2eAgFnSc1d1	bronzová
socha	socha	k1gFnSc1	socha
Vítězství	vítězství	k1gNnSc2	vítězství
svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
nad	nad	k7c7	nad
ďáblem	ďábel	k1gMnSc7	ďábel
od	od	k7c2	od
Jakuba	Jakub	k1gMnSc2	Jakub
Epsteina	Epstein	k1gMnSc2	Epstein
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
exteriéru	exteriér	k1gInSc6	exteriér
katedrály	katedrála	k1gFnSc2	katedrála
poblíž	poblíž	k7c2	poblíž
hlavního	hlavní	k2eAgInSc2d1	hlavní
vchodu	vchod	k1gInSc2	vchod
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgNnSc1d1	válečné
rekviem	rekviem	k1gNnSc1	rekviem
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
Benjamina	Benjamin	k1gMnSc2	Benjamin
Brittena	Britten	k2eAgFnSc1d1	Brittena
<g/>
,	,	kIx,	,
mnohými	mnohý	k2eAgMnPc7d1	mnohý
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
mistrovské	mistrovský	k2eAgNnSc4d1	mistrovské
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
otevření	otevření	k1gNnSc2	otevření
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
<g/>
Věž	věž	k1gFnSc1	věž
původní	původní	k2eAgFnSc2d1	původní
katedrály	katedrála	k1gFnSc2	katedrála
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
trojice	trojice	k1gFnSc2	trojice
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
dominanty	dominanta	k1gFnSc2	dominanta
města	město	k1gNnSc2	město
již	již	k6eAd1	již
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
jsou	být	k5eAaImIp3nP	být
věž	věž	k1gFnSc1	věž
Christ	Christ	k1gInSc4	Christ
Church	Church	k1gInSc1	Church
(	(	kIx(	(
<g/>
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
pouze	pouze	k6eAd1	pouze
věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kostela	kostel	k1gInSc2	kostel
Svaté	svatý	k2eAgFnSc2d1	svatá
trojice	trojice	k1gFnSc2	trojice
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
turistickou	turistický	k2eAgFnSc7d1	turistická
zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
Muzeum	muzeum	k1gNnSc1	muzeum
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c4	v
Coventry	Coventr	k1gInPc4	Coventr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3	nejrozsáhlejší
sbírku	sbírka	k1gFnSc4	sbírka
silničních	silniční	k2eAgNnPc2d1	silniční
vozidel	vozidlo	k1gNnPc2	vozidlo
vyrobených	vyrobený	k2eAgNnPc2d1	vyrobené
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc1	vstup
bez	bez	k7c2	bez
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Nejatraktivnějšími	atraktivní	k2eAgInPc7d3	nejatraktivnější
exponáty	exponát	k1gInPc7	exponát
jsou	být	k5eAaImIp3nP	být
auta	auto	k1gNnPc1	auto
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pokořila	pokořit	k5eAaPmAgFnS	pokořit
rychlostní	rychlostní	k2eAgInPc4d1	rychlostní
rekordy	rekord	k1gInPc4	rekord
-	-	kIx~	-
Thrust	Thrust	k1gInSc4	Thrust
<g/>
2	[number]	k4	2
a	a	k8xC	a
ThrustSSC	ThrustSSC	k1gMnSc1	ThrustSSC
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
prošlo	projít	k5eAaPmAgNnS	projít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
výraznou	výrazný	k2eAgFnSc7d1	výrazná
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
Art	Art	k1gFnSc2	Art
Gallery	Galler	k1gInPc4	Galler
and	and	k?	and
Museum	museum	k1gNnSc1	museum
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
umělecká	umělecký	k2eAgFnSc1d1	umělecká
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
6	[number]	k4	6
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
hranicích	hranice	k1gFnPc6	hranice
v	v	k7c6	v
Bagintonu	Baginton	k1gInSc6	Baginton
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rekonstruovaná	rekonstruovaný	k2eAgFnSc1d1	rekonstruovaná
pevnost	pevnost	k1gFnSc1	pevnost
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Římanů	Říman	k1gMnPc2	Říman
-	-	kIx~	-
Lunt	lunt	k1gMnSc1	lunt
Fort	Fort	k?	Fort
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
letecké	letecký	k2eAgFnSc2d1	letecká
techniky	technika	k1gFnSc2	technika
Midland	Midlanda	k1gFnPc2	Midlanda
Air	Air	k1gFnSc2	Air
Museum	museum	k1gNnSc1	museum
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
poblíž	poblíž	k7c2	poblíž
letiště	letiště	k1gNnSc2	letiště
Coventry	Coventr	k1gInPc1	Coventr
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
policejní	policejní	k2eAgFnSc1d1	policejní
stanice	stanice	k1gFnSc1	stanice
na	na	k7c4	na
Little	Little	k1gFnPc4	Little
Park	park	k1gInSc4	park
Street	Street	k1gInSc1	Street
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
muzeum	muzeum	k1gNnSc4	muzeum
policie	policie	k1gFnSc2	policie
Coventry	Coventr	k1gMnPc7	Coventr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
části	část	k1gFnSc6	část
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sbírky	sbírka	k1gFnPc4	sbírka
zachycující	zachycující	k2eAgFnSc4d1	zachycující
historii	historie	k1gFnSc4	historie
policie	policie	k1gFnSc2	policie
v	v	k7c4	v
Coventry	Coventr	k1gInPc4	Coventr
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
podrobnosti	podrobnost	k1gFnPc1	podrobnost
o	o	k7c6	o
některých	některý	k3yIgInPc6	některý
zvláštních	zvláštní	k2eAgInPc6d1	zvláštní
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
je	být	k5eAaImIp3nS	být
podporováno	podporovat	k5eAaImNgNnS	podporovat
charitativními	charitativní	k2eAgInPc7d1	charitativní
fondy	fond	k1gInPc7	fond
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
návštěva	návštěva	k1gFnSc1	návštěva
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
předchozí	předchozí	k2eAgNnSc4d1	předchozí
objednání	objednání	k1gNnSc4	objednání
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Marston	Marston	k1gInSc1	Marston
(	(	kIx(	(
<g/>
1576	[number]	k4	1576
-	-	kIx~	-
1634	[number]	k4	1634
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
pozdní	pozdní	k2eAgFnSc2d1	pozdní
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
alžbětinského	alžbětinský	k2eAgNnSc2d1	alžbětinské
divadla	divadlo	k1gNnSc2	divadlo
Philip	Philip	k1gMnSc1	Philip
Larkin	Larkin	k1gMnSc1	Larkin
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
-	-	kIx~	-
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
Nigel	Nigel	k1gMnSc1	Nigel
Hawthorne	Hawthorn	k1gInSc5	Hawthorn
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
-	-	kIx~	-
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Kevin	Kevin	k1gMnSc1	Kevin
Warwick	Warwick	k1gMnSc1	Warwick
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
kybernetiky	kybernetika	k1gFnSc2	kybernetika
Clint	Clint	k1gMnSc1	Clint
Mansell	Mansell	k1gMnSc1	Mansell
(	(	kIx(	(
<g/>
*	*	kIx~	*
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Clive	Cliev	k1gFnSc2	Cliev
Owen	Owen	k1gMnSc1	Owen
(	(	kIx(	(
<g/>
*	*	kIx~	*
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Dominic	Dominice	k1gFnPc2	Dominice
Dale	Dale	k1gFnPc2	Dale
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
snookeru	snooker	k1gInSc2	snooker
Marlon	Marlon	k1gInSc1	Marlon
Devonish	Devonish	k1gInSc1	Devonish
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
sprinter	sprinter	k1gMnSc1	sprinter
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
<g/>
,	,	kIx,	,
halový	halový	k2eAgMnSc1d1	halový
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
na	na	k7c4	na
200	[number]	k4	200
m	m	kA	m
</s>
