<s>
V	v	k7c4
Coventry	Coventr	k1gInPc4
existují	existovat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
univerzity	univerzita	k1gFnPc1
-	-	kIx~
Coventry	Coventr	k1gInPc1
University	universita	k1gFnSc2
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
moderním	moderní	k2eAgInSc6d1
studentském	studentský	k2eAgInSc6d1
areálu	areál	k1gInSc6
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
a	a	k8xC
University	universita	k1gFnSc2
of	of	k?
Warwick	Warwick	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
je	být	k5eAaImIp3nS
umístěna	umístěn	k2eAgFnSc1d1
asi	asi	k9
6	[number]	k4
km	km	kA
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
na	na	k7c6
hranicích	hranice	k1gFnPc6
s	s	k7c7
Warwickshire	Warwickshir	k1gInSc5
<g/>
.	.	kIx.
</s>