<p>
<s>
Západní	západní	k2eAgFnPc1d1	západní
polokoule	polokoule	k1gFnPc1	polokoule
nebo	nebo	k8xC	nebo
západní	západní	k2eAgFnSc1d1	západní
hemisféra	hemisféra	k1gFnSc1	hemisféra
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
označující	označující	k2eAgInSc1d1	označující
polovinu	polovina	k1gFnSc4	polovina
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
západně	západně	k6eAd1	západně
od	od	k7c2	od
nultého	nultý	k4xOgInSc2	nultý
poledníku	poledník	k1gInSc2	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
východní	východní	k2eAgFnSc1d1	východní
polokoule	polokoule	k1gFnSc1	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnPc4	označení
pouze	pouze	k6eAd1	pouze
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
některé	některý	k3yIgMnPc4	některý
evropské	evropský	k2eAgMnPc4d1	evropský
<g/>
,	,	kIx,	,
africké	africký	k2eAgFnPc1d1	africká
či	či	k8xC	či
oceánské	oceánský	k2eAgFnPc1d1	oceánská
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
zas	zas	k6eAd1	zas
jako	jako	k9	jako
západní	západní	k2eAgFnSc1d1	západní
polokoule	polokoule	k1gFnSc1	polokoule
světa	svět	k1gInSc2	svět
označuje	označovat	k5eAaImIp3nS	označovat
celý	celý	k2eAgInSc1d1	celý
Nový	nový	k2eAgInSc1d1	nový
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
včetně	včetně	k7c2	včetně
kontinentu	kontinent	k1gInSc2	kontinent
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Země	zem	k1gFnSc2	zem
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
polokoulích	polokoule	k1gFnPc6	polokoule
==	==	k?	==
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
územím	území	k1gNnSc7	území
prochází	procházet	k5eAaImIp3nS	procházet
základní	základní	k2eAgInSc1d1	základní
poledník	poledník	k1gInSc1	poledník
<g/>
,	,	kIx,	,
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
</s>
</p>
<p>
<s>
Mali	Mali	k1gNnSc1	Mali
</s>
</p>
<p>
<s>
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
</s>
</p>
<p>
<s>
Togo	Togo	k1gNnSc1	Togo
</s>
</p>
<p>
<s>
GhanaZemě	GhanaZemě	k6eAd1	GhanaZemě
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
územím	území	k1gNnSc7	území
prochází	procházet	k5eAaImIp3nS	procházet
180	[number]	k4	180
<g/>
.	.	kIx.	.
poledník	poledník	k1gInSc1	poledník
<g/>
,	,	kIx,	,
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Kiribati	Kiribat	k5eAaImF	Kiribat
</s>
</p>
<p>
<s>
Tuvalu	Tuvala	k1gFnSc4	Tuvala
</s>
</p>
<p>
<s>
Wallis	Wallis	k1gFnSc1	Wallis
a	a	k8xC	a
Futuna	Futuna	k1gFnSc1	Futuna
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fidži	Fidzat	k5eAaPmIp1nS	Fidzat
</s>
</p>
<p>
<s>
==	==	k?	==
Země	zem	k1gFnSc2	zem
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
mimo	mimo	k7c4	mimo
americký	americký	k2eAgInSc4d1	americký
kontinent	kontinent	k1gInSc4	kontinent
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnPc1d1	následující
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
úplně	úplně	k6eAd1	úplně
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
Samoa	Samoa	k1gFnSc1	Samoa
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
</s>
</p>
<p>
<s>
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fidži	Fidzat	k5eAaPmIp1nS	Fidzat
</s>
</p>
<p>
<s>
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Polynésie	Polynésie	k1gFnSc1	Polynésie
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gambie	Gambie	k1gFnSc1	Gambie
</s>
</p>
<p>
<s>
Ghana	Ghana	k1gFnSc1	Ghana
</s>
</p>
<p>
<s>
Guinea	Guinea	k1gFnSc1	Guinea
</s>
</p>
<p>
<s>
Guinea-Bissau	Guinea-Bissau	k6eAd1	Guinea-Bissau
</s>
</p>
<p>
<s>
Irsko	Irsko	k1gNnSc1	Irsko
</s>
</p>
<p>
<s>
Island	Island	k1gInSc1	Island
</s>
</p>
<p>
<s>
Kapverdy	Kapverdy	k6eAd1	Kapverdy
</s>
</p>
<p>
<s>
Kiribati	Kiribat	k5eAaImF	Kiribat
</s>
</p>
<p>
<s>
Libérie	Libérie	k1gFnSc1	Libérie
</s>
</p>
<p>
<s>
Mali	Mali	k1gNnSc1	Mali
</s>
</p>
<p>
<s>
Mauritánie	Mauritánie	k1gFnSc1	Mauritánie
</s>
</p>
<p>
<s>
Maroko	Maroko	k1gNnSc1	Maroko
</s>
</p>
<p>
<s>
Niue	Niue	k1gNnSc1	Niue
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pitcairnovy	Pitcairnův	k2eAgInPc1d1	Pitcairnův
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
</s>
</p>
<p>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
Samoa	Samoa	k1gFnSc1	Samoa
</s>
</p>
<p>
<s>
Senegal	Senegal	k1gInSc1	Senegal
</s>
</p>
<p>
<s>
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
Togo	Togo	k1gNnSc1	Togo
</s>
</p>
<p>
<s>
Tokelau	Tokelau	k5eAaPmIp1nS	Tokelau
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tonga	Tonga	k1gFnSc1	Tonga
</s>
</p>
<p>
<s>
Tuvalu	Tuvala	k1gFnSc4	Tuvala
</s>
</p>
<p>
<s>
Wallis	Wallis	k1gFnSc1	Wallis
a	a	k8xC	a
Futuna	Futuna	k1gFnSc1	Futuna
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Západní	západní	k2eAgFnSc1d1	západní
Sahara	Sahara	k1gFnSc1	Sahara
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Western	Western	kA	Western
Hemisphere	Hemispher	k1gInSc5	Hemispher
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
