<p>
<s>
Karla	Karel	k1gMnSc2	Karel
Olivares	Olivares	k1gMnSc1	Olivares
Souza	Souz	k1gMnSc2	Souz
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k1gNnSc1	México
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mexická	mexický	k2eAgFnSc1d1	mexická
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
rolí	role	k1gFnSc7	role
Laurel	Laurela	k1gFnPc2	Laurela
Castillo	Castillo	k1gNnSc4	Castillo
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
stanice	stanice	k1gFnSc2	stanice
ABC	ABC	kA	ABC
Vražedná	vražedný	k2eAgNnPc4d1	vražedné
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c4	v
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
México	México	k6eAd1	México
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
8	[number]	k4	8
let	léto	k1gNnPc2	léto
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Aspenu	Aspen	k1gInSc6	Aspen
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
<g/>
.	.	kIx.	.
</s>
<s>
Herectví	herectví	k1gNnSc4	herectví
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c4	na
Centro	Centro	k1gNnSc4	Centro
de	de	k?	de
Educación	Educación	k1gMnSc1	Educación
Artística	Artística	k1gMnSc1	Artística
<g/>
,	,	kIx,	,
slavná	slavný	k2eAgFnSc1d1	slavná
herecká	herecký	k2eAgFnSc1d1	herecká
škola	škola	k1gFnSc1	škola
v	v	k7c4	v
Mexico	Mexico	k1gNnSc4	Mexico
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
hereckou	herecký	k2eAgFnSc4d1	herecká
školu	škola	k1gFnSc4	škola
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
kurz	kurz	k1gInSc4	kurz
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
na	na	k7c4	na
Central	Central	k1gFnSc4	Central
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Speech	speech	k1gInSc1	speech
and	and	k?	and
Drama	drama	k1gNnSc1	drama
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
22	[number]	k4	22
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Mexico	Mexico	k6eAd1	Mexico
City	city	k1gNnSc2	city
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
přišla	přijít	k5eAaPmAgFnS	přijít
její	její	k3xOp3gFnSc1	její
první	první	k4xOgFnSc1	první
role	role	k1gFnSc1	role
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
mexické	mexický	k2eAgFnSc6d1	mexická
telenovele	telenovela	k1gFnSc6	telenovela
Verano	Verana	k1gFnSc5	Verana
de	de	k?	de
amor	amor	k1gMnSc1	amor
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
mexických	mexický	k2eAgInPc6d1	mexický
sit-comech	sitom	k1gInPc6	sit-com
Los	los	k1gInSc4	los
Héroes	Héroesa	k1gFnPc2	Héroesa
del	del	k?	del
Norte	Nort	k1gInSc5	Nort
a	a	k8xC	a
La	la	k1gNnSc3	la
Clinica	Clinicum	k1gNnSc2	Clinicum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnPc4	její
filmové	filmový	k2eAgFnPc4d1	filmová
role	role	k1gFnPc4	role
patří	patřit	k5eAaImIp3nP	patřit
Dámičky	dámička	k1gFnPc1	dámička
v	v	k7c6	v
sekáči	sekáč	k1gInSc6	sekáč
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Smetánka	smetánka	k1gFnSc1	smetánka
v	v	k7c6	v
bryndě	brynda	k1gFnSc6	brynda
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bez	bez	k7c2	bez
návodu	návod	k1gInSc2	návod
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
studentky	studentka	k1gFnSc2	studentka
práva	právo	k1gNnSc2	právo
Laurel	Laurela	k1gFnPc2	Laurela
Castillo	Castillo	k1gNnSc4	Castillo
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Shondy	Shonda	k1gFnSc2	Shonda
Rhimes	Rhimes	k1gInSc4	Rhimes
Vražedná	vražedný	k2eAgNnPc4d1	vražedné
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
stanice	stanice	k1gFnPc4	stanice
ABC	ABC	kA	ABC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
plynule	plynule	k6eAd1	plynule
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	V	kA	V
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zasnoubila	zasnoubit	k5eAaPmAgFnS	zasnoubit
s	s	k7c7	s
Marshall	Marshall	k1gInSc1	Marshall
Trenkmannem	Trenkmann	k1gInSc7	Trenkmann
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k1gInSc1	pár
se	se	k3xPyFc4	se
vzal	vzít	k5eAaPmAgInS	vzít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Gianna	Gianna	k1gFnSc1	Gianna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
a	a	k8xC	a
nominace	nominace	k1gFnSc1	nominace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Karla	Karel	k1gMnSc2	Karel
Souza	Souz	k1gMnSc2	Souz
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
