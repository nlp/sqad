<s>
Saint	Saint	k1gMnSc1	Saint
Paul	Paul	k1gMnSc1	Paul
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
St.	st.	kA	st.
Paul	Paula	k1gFnPc2	Paula
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Minnesota	Minnesota	k1gFnSc1	Minnesota
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Minneapolisem	Minneapolis	k1gInSc7	Minneapolis
<g/>
,	,	kIx,	,
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
dvě	dva	k4xCgNnPc1	dva
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
oblast	oblast	k1gFnSc1	oblast
Minneapolis-Saint	Minneapolis-Saint	k1gMnSc1	Minneapolis-Saint
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
16	[number]	k4	16
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc4d3	veliký
velkoměstskou	velkoměstský	k2eAgFnSc4d1	velkoměstská
aglomeraci	aglomerace	k1gFnSc4	aglomerace
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Saint	Saint	k1gMnSc1	Saint
Paul	Paul	k1gMnSc1	Paul
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
okresní	okresní	k2eAgNnSc1d1	okresní
město	město	k1gNnSc1	město
Ramsey	Ramsea	k1gFnSc2	Ramsea
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
nejmenšímu	malý	k2eAgNnSc3d3	nejmenší
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
obydlenému	obydlený	k2eAgInSc3d1	obydlený
okresu	okres	k1gInSc3	okres
v	v	k7c6	v
Minnesotě	Minnesota	k1gFnSc6	Minnesota
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
285	[number]	k4	285
068	[number]	k4	068
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
sídlilo	sídlit	k5eAaImAgNnS	sídlit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
ve	v	k7c6	v
městě	město	k1gNnSc6	město
287	[number]	k4	287
151	[number]	k4	151
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
112	[number]	k4	112
109	[number]	k4	109
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
60	[number]	k4	60
987	[number]	k4	987
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
2100,6	[number]	k4	2100,6
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
60,1	[number]	k4	60,1
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
15,7	[number]	k4	15,7
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
1,1	[number]	k4	1,1
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
15,0	[number]	k4	15,0
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
3,9	[number]	k4	3,9
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,2	[number]	k4	4,2
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
9,6	[number]	k4	9,6
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Francis	Francis	k1gInSc1	Francis
Scott	Scott	k1gMnSc1	Scott
Fitzgerald	Fitzgerald	k1gMnSc1	Fitzgerald
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
-	-	kIx~	-
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Warren	Warrna	k1gFnPc2	Warrna
E.	E.	kA	E.
Burger	Burger	k1gMnSc1	Burger
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
-	-	kIx~	-
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
USA	USA	kA	USA
Melvin	Melvina	k1gFnPc2	Melvina
Calvin	Calvina	k1gFnPc2	Calvina
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
-	-	kIx~	-
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelové	Nobelové	k2eAgFnSc2d1	Nobelové
ceny	cena	k1gFnSc2	cena
Herb	Herb	k1gMnSc1	Herb
Brooks	Brooks	k1gInSc1	Brooks
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1937	[number]	k4	1937
-	-	kIx~	-
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
a	a	k8xC	a
trenér	trenér	k1gMnSc1	trenér
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
NHL	NHL	kA	NHL
Jorgos	Jorgos	k1gInSc4	Jorgos
Papandreu	Papandreus	k1gInSc2	Papandreus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgMnSc1d1	řecký
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Řecka	Řecko	k1gNnSc2	Řecko
Duane	Duan	k1gInSc5	Duan
Gene	gen	k1gInSc5	gen
Carey	Careum	k1gNnPc7	Careum
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
letec	letec	k1gMnSc1	letec
a	a	k8xC	a
astronaut	astronaut	k1gMnSc1	astronaut
NASA	NASA	kA	NASA
<g />
.	.	kIx.	.
</s>
<s>
Tim	Tim	k?	Tim
Pawlenty	Pawlenta	k1gFnPc1	Pawlenta
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
republikánský	republikánský	k2eAgMnSc1d1	republikánský
guvernér	guvernér	k1gMnSc1	guvernér
Minnesoty	Minnesota	k1gFnSc2	Minnesota
<g/>
,	,	kIx,	,
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c6	na
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
Chad	Chad	k1gMnSc1	Chad
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Red	Red	k1gMnSc1	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc4	Chile
Peppers	Peppers	k1gInSc1	Peppers
Josh	Josh	k1gMnSc1	Josh
Hartnett	Hartnett	k1gMnSc1	Hartnett
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
producent	producent	k1gMnSc1	producent
Lindsey	Lindsea	k1gFnSc2	Lindsea
Vonn	Vonn	k1gMnSc1	Vonn
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyžařka	lyžařka	k1gFnSc1	lyžařka
<g/>
,	,	kIx,	,
vítězka	vítězka	k1gFnSc1	vítězka
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
Campo	Campa	k1gFnSc5	Campa
Grande	grand	k1gMnSc5	grand
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnPc4	Brazílie
Ciudad	Ciudad	k1gInSc4	Ciudad
Romero	Romero	k1gNnSc1	Romero
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gInSc1	Salvador
Culiacán	Culiacán	k2eAgInSc1d1	Culiacán
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
George	Georg	k1gInSc2	Georg
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
Hadera	Hadero	k1gNnSc2	Hadero
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc4	Izrael
Manzanillo	Manzanillo	k1gNnSc4	Manzanillo
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc4	Mexiko
Nagasaki	Nagasaki	k1gNnSc1	Nagasaki
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Neuss	Neussa	k1gFnPc2	Neussa
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Tiberias	Tiberias	k1gInSc1	Tiberias
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
</s>
