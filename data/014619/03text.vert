<s>
Systém	systém	k1gInSc1
řízení	řízení	k1gNnSc2
báze	báze	k1gFnSc2
dat	datum	k1gNnPc2
</s>
<s>
Systém	systém	k1gInSc1
řízení	řízení	k1gNnSc2
báze	báze	k1gFnSc2
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
zkr.	zkr.	kA
DBMS	DBMS	kA
[	[	kIx(
<g/>
en	en	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Database	Databasa	k1gFnSc6
Management	management	k1gInSc1
System	Syst	k1gInSc7
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
softwarové	softwarový	k2eAgNnSc4d1
vybavení	vybavení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
práci	práce	k1gFnSc4
s	s	k7c7
databází	databáze	k1gFnSc7
<g/>
,	,	kIx,
tzn.	tzn.	kA
tvoří	tvořit	k5eAaImIp3nP
rozhraní	rozhraní	k1gNnSc4
mezi	mezi	k7c7
aplikačními	aplikační	k2eAgInPc7d1
programy	program	k1gInPc7
a	a	k8xC
uloženými	uložený	k2eAgNnPc7d1
daty	datum	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občas	občas	k6eAd1
se	se	k3xPyFc4
pojem	pojem	k1gInSc1
zaměňuje	zaměňovat	k5eAaImIp3nS
s	s	k7c7
pojmem	pojem	k1gInSc7
databázový	databázový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
Databázový	databázový	k2eAgInSc1d1
systém	systém	k1gInSc1
jako	jako	k8xC,k8xS
celek	celek	k1gInSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
DBMS	DBMS	kA
dohromady	dohromady	k6eAd1
s	s	k7c7
bází	báze	k1gFnSc7
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Schopnosti	schopnost	k1gFnPc1
</s>
<s>
Aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
nějaký	nějaký	k3yIgInSc4
programový	programový	k2eAgInSc4d1
systém	systém	k1gInSc4
označený	označený	k2eAgInSc4d1
za	za	k7c4
DBMS	DBMS	kA
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
jednak	jednak	k8xC
schopen	schopen	k2eAgMnSc1d1
efektivně	efektivně	k6eAd1
pracovat	pracovat	k5eAaImF
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
schopný	schopný	k2eAgMnSc1d1
řídit	řídit	k5eAaImF
(	(	kIx(
<g/>
vkládat	vkládat	k5eAaImF
<g/>
,	,	kIx,
modifikovat	modifikovat	k5eAaBmF
<g/>
,	,	kIx,
mazat	mazat	k5eAaImF
<g/>
)	)	kIx)
a	a	k8xC
definovat	definovat	k5eAaBmF
strukturu	struktura	k1gFnSc4
těchto	tento	k3xDgNnPc2
perzistentních	perzistentní	k2eAgNnPc2d1
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
prostého	prostý	k2eAgInSc2d1
souborového	souborový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
používané	používaný	k2eAgInPc4d1
databázové	databázový	k2eAgInPc4d1
systémy	systém	k1gInPc4
mají	mít	k5eAaImIp3nP
i	i	k9
mnoho	mnoho	k4c1
dalších	další	k2eAgFnPc2d1
charakteristických	charakteristický	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
podporu	podpora	k1gFnSc4
pro	pro	k7c4
definici	definice	k1gFnSc4
datových	datový	k2eAgInPc2d1
modelů	model	k1gInPc2
(	(	kIx(
<g/>
například	například	k6eAd1
relační	relační	k2eAgInSc1d1
<g/>
,	,	kIx,
logický	logický	k2eAgInSc1d1
<g/>
,	,	kIx,
objektový	objektový	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
správa	správa	k1gFnSc1
klíčů	klíč	k1gInPc2
<g/>
:	:	kIx,
vlastní	vlastní	k2eAgInPc4d1
(	(	kIx(
<g/>
interně	interně	k6eAd1
implementované	implementovaný	k2eAgNnSc1d1
<g/>
)	)	kIx)
indexování	indexování	k1gNnSc1
<g/>
,	,	kIx,
dodržování	dodržování	k1gNnSc1
unikátnosti	unikátnost	k1gFnSc2
hodnot	hodnota	k1gFnPc2
ve	v	k7c6
sloupcích	sloupec	k1gInPc6
<g/>
,	,	kIx,
nad	nad	k7c7
kterými	který	k3yQgInPc7,k3yRgInPc7,k3yIgInPc7
je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
unikátní	unikátní	k2eAgMnSc1d1
nebo	nebo	k8xC
primární	primární	k2eAgInSc1d1
klíč	klíč	k1gInSc1
<g/>
;	;	kIx,
implementace	implementace	k1gFnSc2
fulltextového	fulltextový	k2eAgNnSc2d1
vyhledávání	vyhledávání	k1gNnSc2
pro	pro	k7c4
fulltextové	fulltextový	k2eAgInPc4d1
klíče	klíč	k1gInPc4
<g/>
;	;	kIx,
implementace	implementace	k1gFnPc4
cizích	cizí	k2eAgInPc2d1
klíčů	klíč	k1gInPc2
</s>
<s>
využití	využití	k1gNnSc1
některého	některý	k3yIgInSc2
jazyka	jazyk	k1gInSc2
vyšší	vysoký	k2eAgFnSc2d2
úrovně	úroveň	k1gFnSc2
pro	pro	k7c4
manipulaci	manipulace	k1gFnSc4
a	a	k8xC
definici	definice	k1gFnSc4
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
SQL	SQL	kA
<g/>
,	,	kIx,
QBE	QBE	kA
<g/>
,	,	kIx,
datalog	datalog	k1gMnSc1
<g/>
,	,	kIx,
Common	Common	k1gMnSc1
English	English	k1gMnSc1
Query	Quera	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
vyřešení	vyřešení	k1gNnSc1
komunikačního	komunikační	k2eAgInSc2d1
kanálu	kanál	k1gInSc2
mezi	mezi	k7c7
uživatelem	uživatel	k1gMnSc7
či	či	k8xC
skriptem	skriptum	k1gNnSc7
a	a	k8xC
DBMS	DBMS	kA
v	v	k7c6
tomto	tento	k3xDgInSc6
jazyku	jazyk	k1gInSc6
<g/>
,	,	kIx,
</s>
<s>
autentizaci	autentizace	k1gFnSc4
uživatelů	uživatel	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
autorizaci	autorizace	k1gFnSc4
k	k	k7c3
operacím	operace	k1gFnPc3
nad	nad	k7c7
daty	datum	k1gNnPc7
(	(	kIx(
<g/>
u	u	k7c2
každého	každý	k3xTgMnSc4
uživatele	uživatel	k1gMnSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
definováno	definovat	k5eAaBmNgNnS
<g/>
,	,	kIx,
jaký	jaký	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
typ	typ	k1gInSc1
příkazů	příkaz	k1gInPc2
je	být	k5eAaImIp3nS
oprávněn	oprávnit	k5eAaPmNgInS
spouštět	spouštět	k5eAaImF
<g/>
)	)	kIx)
</s>
<s>
správu	správa	k1gFnSc4
transakcí	transakce	k1gFnPc2
<g/>
,	,	kIx,
atomicitu	atomicita	k1gFnSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
příkazů	příkaz	k1gInPc2
</s>
<s>
robustnost	robustnost	k1gFnSc1
a	a	k8xC
zotavitelnost	zotavitelnost	k1gFnSc1
po	po	k7c6
chybách	chyba	k1gFnPc6
bez	bez	k7c2
ztráty	ztráta	k1gFnSc2
dat	datum	k1gNnPc2
</s>
<s>
uložené	uložený	k2eAgFnPc1d1
procedury	procedura	k1gFnPc1
</s>
<s>
triggery	triggera	k1gFnPc1
</s>
<s>
integritu	integrita	k1gFnSc4
dat	datum	k1gNnPc2
<g/>
;	;	kIx,
například	například	k6eAd1
nepovolením	nepovolení	k1gNnSc7
vložení	vložení	k1gNnSc2
duplicitního	duplicitní	k2eAgInSc2d1
řádku	řádek	k1gInSc2
s	s	k7c7
unikátním	unikátní	k2eAgInSc7d1
klíčem	klíč	k1gInSc7
nebo	nebo	k8xC
řádku	řádek	k1gInSc2
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
NULL	NULL	kA
u	u	k7c2
sloupců	sloupec	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
NULL	NULL	kA
být	být	k5eAaImF
nesmějí	smát	k5eNaImIp3nP
</s>
<s>
kanály	kanál	k1gInPc1
pro	pro	k7c4
hlášení	hlášení	k1gNnSc4
zpráv	zpráva	k1gFnPc2
po	po	k7c6
úspěšně	úspěšně	k6eAd1
vykonaných	vykonaný	k2eAgInPc6d1
dotazech	dotaz	k1gInPc6
<g/>
,	,	kIx,
chybových	chybový	k2eAgFnPc2d1
hlášek	hláška	k1gFnPc2
<g/>
,	,	kIx,
varování	varování	k1gNnSc1
</s>
<s>
pokročilé	pokročilý	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
jako	jako	k8xS,k8xC
např.	např.	kA
Common	Common	k1gInSc1
Table	tablo	k1gNnSc6
Expressions	Expressions	k1gInSc1
<g/>
,	,	kIx,
zpožděné	zpožděný	k2eAgInPc1d1
zápisy	zápis	k1gInPc1
<g/>
,	,	kIx,
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
</s>
<s>
profilování	profilování	k1gNnSc4
<g/>
,	,	kIx,
statistické	statistický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
běhu	běh	k1gInSc6
dotazů	dotaz	k1gInPc2
<g/>
,	,	kIx,
procesů	proces	k1gInPc2
<g/>
,	,	kIx,
přístupu	přístup	k1gInSc2
uživatelů	uživatel	k1gMnPc2
atd.	atd.	kA
</s>
<s>
Seznam	seznam	k1gInSc4
systémů	systém	k1gInPc2
řízení	řízení	k1gNnSc4
báze	báze	k1gFnSc2
dat	datum	k1gNnPc2
</s>
<s>
Následující	následující	k2eAgInSc1d1
seznam	seznam	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
příklady	příklad	k1gInPc4
některých	některý	k3yIgInPc2
systémů	systém	k1gInPc2
řízení	řízení	k1gNnSc4
báze	báze	k1gFnSc2
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
CSQL	CSQL	kA
</s>
<s>
DB2	DB2	k4
</s>
<s>
FileMaker	FileMaker	k1gMnSc1
</s>
<s>
Firebird	Firebird	k6eAd1
</s>
<s>
Ingres	Ingres	k1gMnSc1
</s>
<s>
Informix	Informix	kA
</s>
<s>
MariaDB	MariaDB	k?
</s>
<s>
Microsoft	Microsoft	kA
Access	Access	k1gInSc1
</s>
<s>
Microsoft	Microsoft	kA
SQL	SQL	kA
Server	server	k1gInSc1
</s>
<s>
Microsoft	Microsoft	kA
Visual	Visual	k1gInSc1
FoxPro	FoxPro	k1gNnSc1
</s>
<s>
MySQL	MySQL	k?
</s>
<s>
OpenLink	OpenLink	k1gInSc1
Virtuoso	Virtuosa	k1gFnSc5
</s>
<s>
Oracle	Oracle	k6eAd1
</s>
<s>
PostgreSQL	PostgreSQL	k?
</s>
<s>
Progress	Progress	k6eAd1
</s>
<s>
SQLite	SQLit	k1gMnSc5
</s>
<s>
Sybase	Sybase	k6eAd1
Adaptive	Adaptiv	k1gInSc5
Server	server	k1gInSc1
Enterprise	Enterpris	k1gInSc5
</s>
<s>
Teradata	Teradata	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
12539	#num#	k4
</s>
