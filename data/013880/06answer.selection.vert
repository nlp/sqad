<s>
Vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
klienti	klient	k1gMnPc1
sami	sám	k3xTgMnPc1
utvářejí	utvářet	k5eAaImIp3nP
příběhy	příběh	k1gInPc4
svého	svůj	k3xOyFgNnSc2
dětství	dětství	k1gNnSc2
a	a	k8xC
minulosti	minulost	k1gFnSc2
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
které	který	k3yRgInPc1
poté	poté	k6eAd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
život	život	k1gInSc4
a	a	k8xC
sebepojetí	sebepojetí	k1gNnSc4
<g/>
.	.	kIx.
</s>