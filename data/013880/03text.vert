<s>
Narativní	narativní	k2eAgFnPc1d1
terapie	terapie	k1gFnPc1
</s>
<s>
Narativní	narativní	k2eAgFnSc1d1
terapie	terapie	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
to	ten	k3xDgNnSc1
narrate	narrat	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
ze	z	k7c2
směrů	směr	k1gInPc2
konstruktivistické	konstruktivistický	k2eAgFnSc2d1
terapie	terapie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
klienti	klient	k1gMnPc1
sami	sám	k3xTgMnPc1
utvářejí	utvářet	k5eAaImIp3nP
příběhy	příběh	k1gInPc4
svého	svůj	k3xOyFgNnSc2
dětství	dětství	k1gNnSc2
a	a	k8xC
minulosti	minulost	k1gFnSc2
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
které	který	k3yRgInPc1
poté	poté	k6eAd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
život	život	k1gInSc4
a	a	k8xC
sebepojetí	sebepojetí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
terapie	terapie	k1gFnSc2
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
náhled	náhled	k1gInSc1
na	na	k7c4
dosavadní	dosavadní	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
člověk	člověk	k1gMnSc1
může	moct	k5eAaImIp3nS
vnímat	vnímat	k5eAaImF
jako	jako	k9
nepříjemnou	příjemný	k2eNgFnSc4d1
a	a	k8xC
zatěžující	zatěžující	k2eAgFnSc4d1
vzpomínku	vzpomínka	k1gFnSc4
<g/>
,	,	kIx,
lze	lze	k6eAd1
vyložit	vyložit	k5eAaPmF
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
v	v	k7c6
jiném	jiný	k2eAgInSc6d1
kontextu	kontext	k1gInSc6
a	a	k8xC
najít	najít	k5eAaPmF
alternativní	alternativní	k2eAgInSc4d1
výklad	výklad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Strnad	Strnad	k1gMnSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
;	;	kIx,
Nejedlá	Nejedlá	k1gFnSc1
<g/>
,	,	kIx,
Alžběta	Alžběta	k1gFnSc1
<g/>
:	:	kIx,
Základy	základ	k1gInPc1
narativní	narativní	k2eAgFnSc2d1
terapie	terapie	k1gFnSc2
a	a	k8xC
narativního	narativní	k2eAgInSc2d1
koučinku	koučinka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-262-0729-0	978-80-262-0729-0	k4
</s>
<s>
Freedman	Freedman	k1gMnSc1
,	,	kIx,
Jill	Jill	k1gMnSc1
<g/>
;	;	kIx,
Combs	Combs	k1gInSc1
<g/>
,	,	kIx,
Gene	gen	k1gInSc5
<g/>
:	:	kIx,
Narativní	narativní	k2eAgFnSc1d1
psychoterapie	psychoterapie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-7367-549-3	978-80-7367-549-3	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
|	|	kIx~
Psychologie	psychologie	k1gFnSc1
</s>
