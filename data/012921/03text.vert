<p>
<s>
Ohnivé	ohnivý	k2eAgFnPc1d1	ohnivá
ženy	žena	k1gFnPc1	žena
mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
je	být	k5eAaImIp3nS	být
československá	československý	k2eAgFnSc1d1	Československá
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
režírovaná	režírovaný	k2eAgFnSc1d1	režírovaná
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Podskalským	podskalský	k2eAgMnSc7d1	podskalský
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
je	být	k5eAaImIp3nS	být
volným	volný	k2eAgNnSc7d1	volné
pokračováním	pokračování	k1gNnSc7	pokračování
komedií	komedie	k1gFnSc7	komedie
Ohnivé	ohnivý	k2eAgFnSc2d1	ohnivá
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
Ohnivé	ohnivý	k2eAgFnPc1d1	ohnivá
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
oslavy	oslava	k1gFnPc1	oslava
700	[number]	k4	700
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
Milotína	Milotín	k1gInSc2	Milotín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
je	být	k5eAaImIp3nS	být
pouť	pouť	k1gFnSc1	pouť
<g/>
,	,	kIx,	,
pouťový	pouťový	k2eAgMnSc1d1	pouťový
zpěvák	zpěvák	k1gMnSc1	zpěvák
zpívá	zpívat	k5eAaImIp3nS	zpívat
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
Martě	Marta	k1gFnSc6	Marta
<g/>
,	,	kIx,	,
Magdaléně	Magdaléna	k1gFnSc3	Magdaléna
a	a	k8xC	a
Jankovi	Janek	k1gMnSc3	Janek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
Milotína	Milotín	k1gInSc2	Milotín
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
televizní	televizní	k2eAgInSc1d1	televizní
štáb	štáb	k1gInSc1	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Režisérka	režisérka	k1gFnSc1	režisérka
je	být	k5eAaImIp3nS	být
nervózní	nervózní	k2eAgFnSc1d1	nervózní
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikde	nikde	k6eAd1	nikde
nikdo	nikdo	k3yNnSc1	nikdo
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
od	od	k7c2	od
produkčního	produkční	k2eAgNnSc2d1	produkční
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Korn	Korn	k1gMnSc1	Korn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
oslavy	oslava	k1gFnPc4	oslava
výročí	výročí	k1gNnSc2	výročí
moderovat	moderovat	k5eAaBmF	moderovat
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nS	sedit
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
kvůli	kvůli	k7c3	kvůli
mlze	mlha	k1gFnSc3	mlha
krouží	kroužit	k5eAaImIp3nS	kroužit
nad	nad	k7c7	nad
Bratislavou	Bratislava	k1gFnSc7	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Dramaturg	dramaturg	k1gMnSc1	dramaturg
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
najít	najít	k5eAaPmF	najít
nějakého	nějaký	k3yIgMnSc4	nějaký
místního	místní	k2eAgMnSc4d1	místní
umělce	umělec	k1gMnSc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
místní	místní	k2eAgFnSc2d1	místní
knihovnice	knihovnice	k1gFnSc2	knihovnice
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
archivářem	archivář	k1gMnSc7	archivář
Holoubkem	Holoubek	k1gMnSc7	Holoubek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Martě	Marta	k1gFnSc6	Marta
a	a	k8xC	a
Magdaléně	Magdaléna	k1gFnSc6	Magdaléna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
pan	pan	k1gMnSc1	pan
Holoubek	Holoubek	k1gMnSc1	Holoubek
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
dramaturgovi	dramaturg	k1gMnSc3	dramaturg
portréty	portrét	k1gInPc1	portrét
Marty	Marta	k1gFnSc2	Marta
a	a	k8xC	a
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
a	a	k8xC	a
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
paní	paní	k1gFnSc1	paní
režisérka	režisérka	k1gFnSc1	režisérka
je	být	k5eAaImIp3nS	být
Martě	Marta	k1gFnSc3	Marta
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Knihovnice	knihovnice	k1gFnSc1	knihovnice
panu	pan	k1gMnSc3	pan
Holoubkovi	Holoubek	k1gMnSc3	Holoubek
nese	nést	k5eAaImIp3nS	nést
ohřátou	ohřátý	k2eAgFnSc4d1	ohřátá
večeři	večeře	k1gFnSc4	večeře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
stejně	stejně	k6eAd1	stejně
vychladne	vychladnout	k5eAaPmIp3nS	vychladnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Režisérka	režisérka	k1gFnSc1	režisérka
se	se	k3xPyFc4	se
jde	jít	k5eAaImIp3nS	jít
podívat	podívat	k5eAaImF	podívat
na	na	k7c4	na
místní	místní	k2eAgFnSc4d1	místní
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
říká	říkat	k5eAaImIp3nS	říkat
Devils	Devils	k1gInSc4	Devils
of	of	k?	of
Milotín	Milotín	k1gInSc1	Milotín
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
režisérce	režisérka	k1gFnSc3	režisérka
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
Marta	Marta	k1gFnSc1	Marta
a	a	k8xC	a
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
duchy	duch	k1gMnPc4	duch
dokáží	dokázat	k5eAaPmIp3nP	dokázat
zachytit	zachytit	k5eAaPmF	zachytit
televizní	televizní	k2eAgFnPc1d1	televizní
kamery	kamera	k1gFnPc1	kamera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
štáb	štáb	k1gInSc1	štáb
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
k	k	k7c3	k
produkčnímu	produkční	k2eAgNnSc3d1	produkční
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
dramaturg	dramaturg	k1gMnSc1	dramaturg
přivede	přivést	k5eAaPmIp3nS	přivést
archiváře	archivář	k1gMnSc4	archivář
Holoubka	Holoubek	k1gMnSc4	Holoubek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
režisérce	režisérka	k1gFnSc3	režisérka
o	o	k7c6	o
Martě	Marta	k1gFnSc6	Marta
a	a	k8xC	a
Magdaléně	Magdaléna	k1gFnSc6	Magdaléna
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
jejich	jejich	k3xOp3gInSc4	jejich
příběh	příběh	k1gInSc4	příběh
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
by	by	kYmCp3nS	by
Martu	Marta	k1gFnSc4	Marta
hrát	hrát	k5eAaImF	hrát
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jí	on	k3xPp3gFnSc3	on
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
režisérka	režisérka	k1gFnSc1	režisérka
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
události	událost	k1gFnSc6	událost
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
Holoubek	Holoubek	k1gMnSc1	Holoubek
jí	on	k3xPp3gFnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
událost	událost	k1gFnSc1	událost
stala	stát	k5eAaPmAgFnS	stát
před	před	k7c7	před
300	[number]	k4	300
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
režisérka	režisérka	k1gFnSc1	režisérka
naštvaně	naštvaně	k6eAd1	naštvaně
odchází	odcházet	k5eAaImIp3nS	odcházet
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
venku	venku	k6eAd1	venku
ale	ale	k9	ale
zakopne	zakopnout	k5eAaPmIp3nS	zakopnout
a	a	k8xC	a
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Marta	Marta	k1gFnSc1	Marta
a	a	k8xC	a
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
chtějí	chtít	k5eAaImIp3nP	chtít
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
režisérky	režisérka	k1gFnSc2	režisérka
<g/>
,	,	kIx,	,
Marta	Marta	k1gFnSc1	Marta
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
režisérka	režisérka	k1gFnSc1	režisérka
s	s	k7c7	s
duší	duše	k1gFnSc7	duše
Marty	Marta	k1gFnSc2	Marta
probere	probrat	k5eAaPmIp3nS	probrat
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
Holoubek	Holoubek	k1gMnSc1	Holoubek
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
omlouvá	omlouvat	k5eAaImIp3nS	omlouvat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nemyslel	myslet	k5eNaImAgMnS	myslet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
režisérka	režisérka	k1gFnSc1	režisérka
tak	tak	k6eAd1	tak
stará	starý	k2eAgFnSc1d1	stará
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Probíhají	probíhat	k5eAaImIp3nP	probíhat
další	další	k2eAgFnPc4d1	další
zkoušky	zkouška	k1gFnPc4	zkouška
vystoupení	vystoupení	k1gNnSc2	vystoupení
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Asistent	asistent	k1gMnSc1	asistent
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
Martě	Marta	k1gFnSc3	Marta
<g/>
,	,	kIx,	,
že	že	k8xS	že
režisérka	režisérka	k1gFnSc1	režisérka
může	moct	k5eAaImIp3nS	moct
vše	všechen	k3xTgNnSc4	všechen
řídit	řídit	k5eAaImF	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Marta	Marta	k1gFnSc1	Marta
tak	tak	k9	tak
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
zastaví	zastavit	k5eAaPmIp3nS	zastavit
veškeré	veškerý	k3xTgNnSc4	veškerý
dění	dění	k1gNnSc4	dění
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
zničí	zničit	k5eAaPmIp3nS	zničit
vystoupení	vystoupení	k1gNnSc1	vystoupení
skupiny	skupina	k1gFnSc2	skupina
Devils	Devilsa	k1gFnPc2	Devilsa
of	of	k?	of
Milotín	Milotína	k1gFnPc2	Milotína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
ruší	rušit	k5eAaImIp3nS	rušit
záběry	záběr	k1gInPc4	záběr
jako	jako	k8xS	jako
duch	duch	k1gMnSc1	duch
a	a	k8xC	a
Marta	Marta	k1gFnSc1	Marta
se	se	k3xPyFc4	se
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
ducha	duch	k1gMnSc4	duch
vyžene	vyhnat	k5eAaPmIp3nS	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Opustí	opustit	k5eAaPmIp3nP	opustit
tělo	tělo	k1gNnSc4	tělo
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
prát	prát	k5eAaImF	prát
s	s	k7c7	s
Magdalénou	Magdaléna	k1gFnSc7	Magdaléna
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
přenosového	přenosový	k2eAgInSc2d1	přenosový
vozu	vůz	k1gInSc2	vůz
tak	tak	k6eAd1	tak
vidí	vidět	k5eAaImIp3nS	vidět
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
duchy	duch	k1gMnPc4	duch
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
povaha	povaha	k1gFnSc1	povaha
režisérky	režisérka	k1gFnSc2	režisérka
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
Holoubek	Holoubek	k1gMnSc1	Holoubek
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
pozve	pozvat	k5eAaPmIp3nS	pozvat
psychiatra	psychiatr	k1gMnSc4	psychiatr
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
uzná	uznat	k5eAaPmIp3nS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
režisérka	režisérka	k1gFnSc1	režisérka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezdá	zdát	k5eNaImIp3nS	zdát
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
vedoucí	vedoucí	k1gMnSc1	vedoucí
přenosového	přenosový	k2eAgInSc2d1	přenosový
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
odveze	odvézt	k5eAaPmIp3nS	odvézt
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
ordinace	ordinace	k1gFnSc2	ordinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Milotína	Milotín	k1gInSc2	Milotín
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
konferenciérka	konferenciérka	k1gFnSc1	konferenciérka
ze	z	k7c2	z
Žiliny	Žilina	k1gFnSc2	Žilina
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
ale	ale	k8xC	ale
zakopne	zakopnout	k5eAaPmIp3nS	zakopnout
a	a	k8xC	a
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Těla	tělo	k1gNnPc1	tělo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dočká	dočkat	k5eAaPmIp3nS	dočkat
i	i	k9	i
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
Marty	Marta	k1gFnSc2	Marta
a	a	k8xC	a
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
Marta	Marta	k1gFnSc1	Marta
Magdaléně	Magdaléna	k1gFnSc3	Magdaléna
uzdraví	uzdravit	k5eAaPmIp3nS	uzdravit
tělo	tělo	k1gNnSc1	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblíbenost	oblíbenost	k1gFnSc1	oblíbenost
archiváře	archivář	k1gMnSc2	archivář
Holoubka	Holoubek	k1gMnSc2	Holoubek
u	u	k7c2	u
režisérky	režisérka	k1gFnSc2	režisérka
se	se	k3xPyFc4	se
nelíbí	líbit	k5eNaImIp3nS	líbit
knihovnici	knihovnice	k1gFnSc3	knihovnice
a	a	k8xC	a
paní	paní	k1gFnSc3	paní
Čiperné	čiperný	k2eAgFnPc1d1	čiperná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
domluví	domluvit	k5eAaPmIp3nS	domluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
na	na	k7c4	na
oslavy	oslava	k1gFnPc4	oslava
měl	mít	k5eAaImAgMnS	mít
dohlédnout	dohlédnout	k5eAaPmF	dohlédnout
kulturní	kulturní	k2eAgMnSc1d1	kulturní
referent	referent	k1gMnSc1	referent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
Dramaturg	dramaturg	k1gMnSc1	dramaturg
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
přijde	přijít	k5eAaPmIp3nS	přijít
zkontrolovat	zkontrolovat	k5eAaPmF	zkontrolovat
referent	referent	k1gMnSc1	referent
a	a	k8xC	a
tak	tak	k6eAd1	tak
mu	on	k3xPp3gMnSc3	on
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
prohodil	prohodit	k5eAaPmAgMnS	prohodit
také	také	k9	také
několik	několik	k4yIc4	několik
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
kontrola	kontrola	k1gFnSc1	kontrola
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nekoná	konat	k5eNaImIp3nS	konat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marta	Marta	k1gFnSc1	Marta
<g/>
,	,	kIx,	,
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
i	i	k8xC	i
Janek	Janek	k1gMnSc1	Janek
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
tělech	tělo	k1gNnPc6	tělo
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
oslavy	oslava	k1gFnPc1	oslava
výročí	výročí	k1gNnSc2	výročí
založení	založení	k1gNnSc2	založení
Milotína	Milotín	k1gInSc2	Milotín
začínají	začínat	k5eAaImIp3nP	začínat
vystoupením	vystoupení	k1gNnSc7	vystoupení
dívčí	dívčí	k2eAgFnSc2d1	dívčí
skupiny	skupina	k1gFnSc2	skupina
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Janka	Janka	k1gFnSc1	Janka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nyní	nyní	k6eAd1	nyní
místo	místo	k1gNnSc1	místo
mladými	mladý	k2eAgFnPc7d1	mladá
dívkami	dívka	k1gFnPc7	dívka
tvořena	tvořit	k5eAaImNgFnS	tvořit
staršími	starý	k2eAgFnPc7d2	starší
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Ohnivé	ohnivý	k2eAgFnPc1d1	ohnivá
ženy	žena	k1gFnPc1	žena
mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
na	na	k7c4	na
stránkách	stránka	k1gFnPc6	stránka
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
</s>
</p>
