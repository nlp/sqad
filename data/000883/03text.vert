<s>
Kiss	Kiss	k6eAd1	Kiss
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
bizarně	bizarně	k6eAd1	bizarně
líčenými	líčený	k2eAgFnPc7d1	líčená
tvářemi	tvář	k1gFnPc7	tvář
svých	svůj	k3xOyFgMnPc2	svůj
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc4	kostým
a	a	k8xC	a
vizuálním	vizuální	k2eAgNnSc7d1	vizuální
ztvárněním	ztvárnění	k1gNnSc7	ztvárnění
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
plivali	plivat	k5eAaImAgMnP	plivat
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
kouřilo	kouřit	k5eAaImAgNnS	kouřit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
z	z	k7c2	z
kytar	kytara	k1gFnPc2	kytara
a	a	k8xC	a
používali	používat	k5eAaImAgMnP	používat
pyrotechniku	pyrotechnika	k1gFnSc4	pyrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
oblečení	oblečení	k1gNnSc1	oblečení
a	a	k8xC	a
make-up	makep	k1gInSc1	make-up
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
strašidelně	strašidelně	k6eAd1	strašidelně
laděných	laděný	k2eAgFnPc2d1	laděná
komiksových	komiksový	k2eAgFnPc2d1	komiksová
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
atraktivní	atraktivní	k2eAgFnPc1d1	atraktivní
prezentace	prezentace	k1gFnPc1	prezentace
přitvrzené	přitvrzený	k2eAgFnPc1d1	přitvrzená
glam	gla	k1gNnSc7	gla
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
zapůsobila	zapůsobit	k5eAaPmAgFnS	zapůsobit
na	na	k7c4	na
široké	široký	k2eAgFnPc4d1	široká
masy	masa	k1gFnPc4	masa
publika	publikum	k1gNnSc2	publikum
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
celosvětově	celosvětově	k6eAd1	celosvětově
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
a	a	k8xC	a
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
amerických	americký	k2eAgFnPc2d1	americká
rockových	rockový	k2eAgFnPc2d1	rocková
skupin	skupina	k1gFnPc2	skupina
hlavně	hlavně	k9	hlavně
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kiss	Kiss	k6eAd1	Kiss
doposud	doposud	k6eAd1	doposud
získali	získat	k5eAaPmAgMnP	získat
od	od	k7c2	od
RIAA	RIAA	kA	RIAA
22	[number]	k4	22
zlatých	zlatý	k2eAgFnPc2d1	zlatá
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
potvrzený	potvrzený	k2eAgInSc4d1	potvrzený
prodej	prodej	k1gInSc4	prodej
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
19	[number]	k4	19
milionů	milion	k4xCgInPc2	milion
nahrávek	nahrávka	k1gFnPc2	nahrávka
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Kiss	Kiss	k6eAd1	Kiss
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Wicked	Wicked	k1gMnSc1	Wicked
Lester	Lester	k1gMnSc1	Lester
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skupinu	skupina	k1gFnSc4	skupina
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
vedli	vést	k5eAaImAgMnP	vést
Gene	gen	k1gInSc5	gen
Simmons	Simmonsa	k1gFnPc2	Simmonsa
a	a	k8xC	a
Paul	Paula	k1gFnPc2	Paula
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
.	.	kIx.	.
</s>
<s>
Wicked	Wicked	k1gMnSc1	Wicked
Lester	Lester	k1gMnSc1	Lester
svou	svůj	k3xOyFgFnSc7	svůj
tvorbou	tvorba	k1gFnSc7	tvorba
ale	ale	k8xC	ale
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
výraznějšího	výrazný	k2eAgInSc2d2	výraznější
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
mezi	mezi	k7c7	mezi
sebe	sebe	k3xPyFc4	sebe
Simmons	Simmons	k1gInSc1	Simmons
se	s	k7c7	s
Stanleyem	Stanley	k1gMnSc7	Stanley
přijali	přijmout	k5eAaPmAgMnP	přijmout
veterána	veterán	k1gMnSc4	veterán
hudební	hudební	k2eAgFnSc2d1	hudební
klubové	klubový	k2eAgFnSc2d1	klubová
scény	scéna	k1gFnSc2	scéna
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
Petera	Peter	k1gMnSc2	Peter
Crisse	Criss	k1gMnSc2	Criss
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
zkouškách	zkouška	k1gFnPc6	zkouška
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgFnPc1	první
písničky	písnička	k1gFnPc1	písnička
jako	jako	k8xS	jako
Strutter	Strutter	k1gMnSc1	Strutter
či	či	k8xC	či
Deuce	Deuce	k1gMnSc1	Deuce
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
začátkům	začátek	k1gInPc3	začátek
Wicked	Wicked	k1gInSc1	Wicked
Lester	Lester	k1gMnSc1	Lester
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
muziku	muzika	k1gFnSc4	muzika
přitvrzovat	přitvrzovat	k5eAaImF	přitvrzovat
a	a	k8xC	a
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
kapely	kapela	k1gFnSc2	kapela
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Dolls	Dollsa	k1gFnPc2	Dollsa
zkoušela	zkoušet	k5eAaImAgFnS	zkoušet
experimentovat	experimentovat	k5eAaImF	experimentovat
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
make-up	makep	k1gInSc4	make-up
<g/>
,	,	kIx,	,
vlasy	vlas	k1gInPc4	vlas
měla	mít	k5eAaImAgNnP	mít
upravené	upravený	k2eAgFnPc1d1	upravená
trvalou	trvalá	k1gFnSc7	trvalá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1973	[number]	k4	1973
se	se	k3xPyFc4	se
k	k	k7c3	k
trojici	trojice	k1gFnSc3	trojice
kapely	kapela	k1gFnSc2	kapela
připojil	připojit	k5eAaPmAgMnS	připojit
sólový	sólový	k2eAgMnSc1d1	sólový
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
excentrik	excentrik	k1gMnSc1	excentrik
Paul	Paul	k1gMnSc1	Paul
"	"	kIx"	"
<g/>
Ace	Ace	k1gMnSc1	Ace
<g/>
"	"	kIx"	"
Frehley	Frehlea	k1gMnSc2	Frehlea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
Gene	gen	k1gInSc5	gen
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
nově	nově	k6eAd1	nově
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
kapelu	kapela	k1gFnSc4	kapela
KISS	KISS	kA	KISS
<g/>
.	.	kIx.	.
</s>
<s>
Ace	Ace	k?	Ace
pak	pak	k6eAd1	pak
dostal	dostat	k5eAaPmAgInS	dostat
nápad	nápad	k1gInSc1	nápad
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	k9	by
písmena	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
SS	SS	kA	SS
<g/>
"	"	kIx"	"
na	na	k7c6	na
konci	konec	k1gInSc6	konec
názvu	název	k1gInSc2	název
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
blesků	blesk	k1gInPc2	blesk
<g/>
:	:	kIx,	:
KI	KI	kA	KI
<g/>
⚡	⚡	k?	⚡
(	(	kIx(	(
<g/>
německé	německý	k2eAgFnSc2d1	německá
edice	edice	k1gFnSc2	edice
alb	alba	k1gFnPc2	alba
skupiny	skupina	k1gFnSc2	skupina
však	však	k9	však
musely	muset	k5eAaImAgFnP	muset
mít	mít	k5eAaImF	mít
tento	tento	k3xDgInSc1	tento
text	text	k1gInSc1	text
upravený	upravený	k2eAgInSc1d1	upravený
jako	jako	k8xC	jako
dvě	dva	k4xCgNnPc4	dva
obrácená	obrácený	k2eAgNnPc4d1	obrácené
"	"	kIx"	"
<g/>
ZZ	ZZ	kA	ZZ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
blesky	blesk	k1gInPc1	blesk
silně	silně	k6eAd1	silně
připomínaly	připomínat	k5eAaImAgInP	připomínat
nacistický	nacistický	k2eAgInSc4d1	nacistický
znak	znak	k1gInSc4	znak
SS	SS	kA	SS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1973	[number]	k4	1973
v	v	k7c4	v
Bell	bell	k1gInSc4	bell
Sound	Sound	k1gInSc1	Sound
Studios	Studios	k?	Studios
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
oficiální	oficiální	k2eAgNnSc1d1	oficiální
vystoupení	vystoupení	k1gNnSc1	vystoupení
skupiny	skupina	k1gFnSc2	skupina
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
Academy	Academa	k1gFnPc4	Academa
of	of	k?	of
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
NY	NY	kA	NY
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předskakovali	předskakovat	k5eAaImAgMnP	předskakovat
kapele	kapela	k1gFnSc3	kapela
Blue	Blue	k1gFnSc1	Blue
Öyster	Öyster	k1gMnSc1	Öyster
Cult	Cult	k1gMnSc1	Cult
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
koncertu	koncert	k1gInSc6	koncert
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
plivání	plivání	k1gNnSc6	plivání
plamenů	plamen	k1gInPc2	plamen
vzplanuly	vzplanout	k5eAaPmAgInP	vzplanout
nalakované	nalakovaný	k2eAgInPc1d1	nalakovaný
vlasy	vlas	k1gInPc1	vlas
Geneho	Gene	k1gMnSc2	Gene
Simmonse	Simmons	k1gMnSc2	Simmons
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1974	[number]	k4	1974
v	v	k7c6	v
kanadském	kanadský	k2eAgInSc6d1	kanadský
Edmontonu	Edmonton	k1gInSc6	Edmonton
na	na	k7c4	na
Northern	Northern	k1gInSc4	Northern
Alberta	Albert	k1gMnSc2	Albert
Jubilee	Jubile	k1gInSc2	Jubile
Auditorium	auditorium	k1gNnSc4	auditorium
skupina	skupina	k1gFnSc1	skupina
nastartovala	nastartovat	k5eAaPmAgFnS	nastartovat
své	svůj	k3xOyFgNnSc4	svůj
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
s	s	k7c7	s
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
názvem	název	k1gInSc7	název
Kiss	Kissa	k1gFnPc2	Kissa
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
ABC	ABC	kA	ABC
v	v	k7c6	v
programu	program	k1gInSc6	program
Dicka	Dicka	k1gMnSc1	Dicka
Clarka	Clarka	k1gMnSc1	Clarka
In	In	k1gMnSc1	In
Concert	Concert	k1gMnSc1	Concert
odvysíláno	odvysílán	k2eAgNnSc4d1	odvysíláno
první	první	k4xOgNnSc4	první
televizní	televizní	k2eAgNnSc4d1	televizní
vystoupení	vystoupení	k1gNnSc4	vystoupení
skupiny	skupina	k1gFnSc2	skupina
se	s	k7c7	s
skladbami	skladba	k1gFnPc7	skladba
Nothin	Nothina	k1gFnPc2	Nothina
<g/>
'	'	kIx"	'
to	ten	k3xDgNnSc1	ten
Lose	los	k1gInSc6	los
<g/>
,	,	kIx,	,
Firehouse	Firehous	k1gInSc6	Firehous
a	a	k8xC	a
Black	Black	k1gMnSc1	Black
Diamond	Diamond	k1gMnSc1	Diamond
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
skupina	skupina	k1gFnSc1	skupina
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
programu	program	k1gInSc6	program
The	The	k1gFnSc2	The
Mike	Mik	k1gFnSc2	Mik
Douglas	Douglasa	k1gFnPc2	Douglasa
Show	show	k1gNnSc1	show
se	s	k7c7	s
skladbou	skladba	k1gFnSc7	skladba
Firehouse	Firehouse	k1gFnSc2	Firehouse
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
vystoupení	vystoupení	k1gNnSc3	vystoupení
předcházelo	předcházet	k5eAaImAgNnS	předcházet
Douglasovo	Douglasův	k2eAgNnSc1d1	Douglasovo
interview	interview	k1gNnSc1	interview
se	s	k7c7	s
Simmonsem	Simmons	k1gInSc7	Simmons
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
namaskovaný	namaskovaný	k2eAgInSc1d1	namaskovaný
Gene	gen	k1gInSc5	gen
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zosobněním	zosobnění	k1gNnSc7	zosobnění
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
<g/>
,	,	kIx,	,
pobavil	pobavit	k5eAaPmAgMnS	pobavit
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
atmosféru	atmosféra	k1gFnSc4	atmosféra
ve	v	k7c6	v
zmateném	zmatený	k2eAgNnSc6d1	zmatené
hledišti	hlediště	k1gNnSc6	hlediště
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
přes	přes	k7c4	přes
publicitu	publicita	k1gFnSc4	publicita
a	a	k8xC	a
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
jen	jen	k9	jen
75	[number]	k4	75
000	[number]	k4	000
nosičů	nosič	k1gInPc2	nosič
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Casablanca	Casablanca	k1gFnSc1	Casablanca
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
album	album	k1gNnSc1	album
produkovalo	produkovat	k5eAaImAgNnS	produkovat
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
své	svůj	k3xOyFgFnPc4	svůj
investice	investice	k1gFnPc4	investice
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zkusit	zkusit	k5eAaPmF	zkusit
nahrát	nahrát	k5eAaBmF	nahrát
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
odletěli	odletět	k5eAaPmAgMnP	odletět
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
do	do	k7c2	do
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pokusili	pokusit	k5eAaPmAgMnP	pokusit
odčinit	odčinit	k5eAaPmF	odčinit
komerční	komerční	k2eAgInSc4d1	komerční
neúspěch	neúspěch	k1gInSc4	neúspěch
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Hotter	Hottra	k1gFnPc2	Hottra
Than	Thano	k1gNnPc2	Thano
Hell	Hellum	k1gNnPc2	Hellum
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
jen	jen	k9	jen
na	na	k7c4	na
stou	stý	k4xOgFnSc4	stý
příčku	příčka	k1gFnSc4	příčka
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
jen	jen	k9	jen
skladba	skladba	k1gFnSc1	skladba
Let	léto	k1gNnPc2	léto
Me	Me	k1gFnSc2	Me
Go	Go	k1gFnSc1	Go
<g/>
,	,	kIx,	,
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Rollum	k1gNnPc2	Rollum
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
vypadlo	vypadnout	k5eAaPmAgNnS	vypadnout
z	z	k7c2	z
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
zrušení	zrušení	k1gNnSc4	zrušení
probíhajícího	probíhající	k2eAgNnSc2d1	probíhající
koncertního	koncertní	k2eAgNnSc2d1	koncertní
turné	turné	k1gNnSc2	turné
a	a	k8xC	a
načatého	načatý	k2eAgNnSc2d1	načaté
nahrávaní	nahrávaný	k2eAgMnPc1d1	nahrávaný
třetího	třetí	k4xOgNnSc2	třetí
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
produkce	produkce	k1gFnSc2	produkce
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
vzal	vzít	k5eAaPmAgMnS	vzít
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
rukou	ruka	k1gFnPc2	ruka
sám	sám	k3xTgMnSc1	sám
ředitel	ředitel	k1gMnSc1	ředitel
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Casablanca	Casablanca	k1gFnSc1	Casablanca
Neil	Neil	k1gMnSc1	Neil
Bogart	Bogart	k1gInSc1	Bogart
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
zkresleného	zkreslený	k2eAgInSc2d1	zkreslený
zvuku	zvuk	k1gInSc2	zvuk
alba	album	k1gNnSc2	album
Hotter	Hotter	k1gMnSc1	Hotter
Than	Than	k1gMnSc1	Than
Hell	Hell	k1gMnSc1	Hell
na	na	k7c4	na
čistší	čistý	k2eAgInSc4d2	čistší
<g/>
,	,	kIx,	,
trochu	trochu	k6eAd1	trochu
víc	hodně	k6eAd2	hodně
popovější	popový	k2eAgFnSc4d2	popovější
verzi	verze	k1gFnSc4	verze
nových	nový	k2eAgFnPc2d1	nová
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
album	album	k1gNnSc1	album
Dressed	Dressed	k1gInSc1	Dressed
to	ten	k3xDgNnSc1	ten
Kill	Kill	k1gInSc1	Kill
mělo	mít	k5eAaImAgNnS	mít
mírně	mírně	k6eAd1	mírně
lepší	dobrý	k2eAgInSc4d2	lepší
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
albu	album	k1gNnSc6	album
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
klasických	klasický	k2eAgFnPc2d1	klasická
skladeb	skladba	k1gFnPc2	skladba
Kiss	Kissa	k1gFnPc2	Kissa
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gMnSc1	Roll
All	All	k1gMnSc1	All
Nite	nit	k1gInSc5	nit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
alba	alba	k1gFnSc1	alba
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
nedosahovala	dosahovat	k5eNaImAgFnS	dosahovat
očekávaného	očekávaný	k2eAgInSc2d1	očekávaný
prodeje	prodej	k1gInSc2	prodej
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
originální	originální	k2eAgFnSc3d1	originální
show	show	k1gFnSc3	show
na	na	k7c6	na
živých	živý	k2eAgNnPc6d1	živé
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Děly	dít	k5eAaImAgFnP	dít
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
takové	takový	k3xDgFnPc1	takový
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
jako	jako	k9	jako
bylo	být	k5eAaImAgNnS	být
Geneho	Gene	k1gMnSc4	Gene
plivání	plivání	k1gNnSc2	plivání
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
jogurtu	jogurt	k1gInSc2	jogurt
a	a	k8xC	a
potravinářské	potravinářský	k2eAgFnSc2d1	potravinářská
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plivání	plivání	k1gNnSc1	plivání
plamenů	plamen	k1gInPc2	plamen
<g/>
,	,	kIx,	,
Frehleyova	Frehleyův	k2eAgFnSc1d1	Frehleyův
kytara	kytara	k1gFnSc1	kytara
se	se	k3xPyFc4	se
během	během	k7c2	během
sóla	sólo	k1gNnSc2	sólo
vznítila	vznítit	k5eAaPmAgFnS	vznítit
(	(	kIx(	(
<g/>
efekt	efekt	k1gInSc4	efekt
světel	světlo	k1gNnPc2	světlo
a	a	k8xC	a
dýmovnic	dýmovnice	k1gFnPc2	dýmovnice
<g/>
,	,	kIx,	,
skrytých	skrytý	k2eAgFnPc2d1	skrytá
v	v	k7c6	v
kytaře	kytara	k1gFnSc6	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
Crissova	Crissův	k2eAgInSc2d1	Crissův
bubnovaní	bubnovaný	k2eAgMnPc1d1	bubnovaný
létaly	létat	k5eAaImAgFnP	létat
jiskry	jiskra	k1gFnPc1	jiskra
a	a	k8xC	a
rytmus	rytmus	k1gInSc1	rytmus
Stanleyovy	Stanleyův	k2eAgFnSc2d1	Stanleyova
kytary	kytara	k1gFnSc2	kytara
byl	být	k5eAaImAgInS	být
zvýrazněn	zvýraznit	k5eAaPmNgInS	zvýraznit
pyrotechnikou	pyrotechnika	k1gFnSc7	pyrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
bylo	být	k5eAaImAgNnS	být
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Casablanca	Casablanca	k1gFnSc1	Casablanca
blízko	blízko	k7c2	blízko
krachu	krach	k1gInSc2	krach
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
propadnutí	propadnutí	k1gNnSc2	propadnutí
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
mohl	moct	k5eAaImAgInS	moct
zachránit	zachránit	k5eAaPmF	zachránit
jen	jen	k9	jen
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Překvapením	překvapení	k1gNnSc7	překvapení
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
komerční	komerční	k2eAgInSc1d1	komerční
úspěch	úspěch	k1gInSc1	úspěch
přišel	přijít	k5eAaPmAgInS	přijít
po	po	k7c6	po
vydaní	vydaný	k2eAgMnPc1d1	vydaný
dvojalba	dvojalbum	k1gNnSc2	dvojalbum
Alive	Aliev	k1gFnPc1	Aliev
<g/>
!	!	kIx.	!
</s>
<s>
ze	z	k7c2	z
živých	živý	k2eAgNnPc2d1	živé
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
jejich	jejich	k3xOp3gInSc6	jejich
prvním	první	k4xOgNnSc6	první
živém	živý	k2eAgNnSc6d1	živé
albu	album	k1gNnSc6	album
mínila	mínit	k5eAaImAgFnS	mínit
skupina	skupina	k1gFnSc1	skupina
předvést	předvést	k5eAaPmF	předvést
to	ten	k3xDgNnSc4	ten
vzrušení	vzrušení	k1gNnSc4	vzrušení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
provázelo	provázet	k5eAaImAgNnS	provázet
jejich	jejich	k3xOp3gInPc4	jejich
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
chtěla	chtít	k5eAaImAgFnS	chtít
vydat	vydat	k5eAaPmF	vydat
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
nahrát	nahrát	k5eAaPmF	nahrát
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
získalo	získat	k5eAaPmAgNnS	získat
čtyřnásobnou	čtyřnásobný	k2eAgFnSc4d1	čtyřnásobná
platinu	platina	k1gFnSc4	platina
a	a	k8xC	a
živá	živý	k2eAgFnSc1d1	živá
verze	verze	k1gFnSc1	verze
singlu	singl	k1gInSc2	singl
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
All	All	k1gMnSc1	All
Night	Night	k1gMnSc1	Night
s	s	k7c7	s
kytarovým	kytarový	k2eAgNnSc7d1	kytarové
sólem	sólo	k1gNnSc7	sólo
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
zachránila	zachránit	k5eAaPmAgFnS	zachránit
před	před	k7c7	před
finančním	finanční	k2eAgInSc7d1	finanční
krachem	krach	k1gInSc7	krach
skupinu	skupina	k1gFnSc4	skupina
i	i	k8xC	i
studio	studio	k1gNnSc4	studio
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Bobem	Bob	k1gMnSc7	Bob
Ezrinem	Ezrin	k1gMnSc7	Ezrin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
předtím	předtím	k6eAd1	předtím
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
hvězdou	hvězda	k1gFnSc7	hvězda
glam	glam	k6eAd1	glam
rocku	rock	k1gInSc3	rock
Alice	Alice	k1gFnSc2	Alice
Cooperem	Cooper	k1gInSc7	Cooper
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
této	tento	k3xDgFnSc2	tento
spolupráce	spolupráce	k1gFnSc2	spolupráce
bylo	být	k5eAaImAgNnS	být
zvukově	zvukově	k6eAd1	zvukově
i	i	k8xC	i
hudebně	hudebně	k6eAd1	hudebně
nejlépe	dobře	k6eAd3	dobře
zpracované	zpracovaný	k2eAgNnSc1d1	zpracované
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
Destroyer	Destroyer	k1gInSc1	Destroyer
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
produkci	produkce	k1gFnSc6	produkce
alba	album	k1gNnSc2	album
byly	být	k5eAaImAgFnP	být
využity	využít	k5eAaPmNgFnP	využít
orchestrální	orchestrální	k2eAgFnPc1d1	orchestrální
party	parta	k1gFnPc1	parta
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
páskových	páskový	k2eAgInPc2d1	páskový
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
Beth	Beth	k1gInSc1	Beth
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
žebříčků	žebříček	k1gInPc2	žebříček
a	a	k8xC	a
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
platinovým	platinový	k2eAgInSc7d1	platinový
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
deskou	deska	k1gFnSc7	deska
nahranou	nahraný	k2eAgFnSc7d1	nahraná
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
KISS	KISS	kA	KISS
definitivně	definitivně	k6eAd1	definitivně
zařadili	zařadit	k5eAaPmAgMnP	zařadit
mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgFnPc4d3	nejpopulárnější
kapely	kapela	k1gFnPc4	kapela
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gMnSc1	Roll
Over	Over	k1gMnSc1	Over
bylo	být	k5eAaImAgNnS	být
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
"	"	kIx"	"
<g/>
živého	živý	k2eAgMnSc2d1	živý
<g/>
"	"	kIx"	"
zvuku	zvuk	k1gInSc2	zvuk
na	na	k7c6	na
studiové	studiový	k2eAgFnSc6d1	studiová
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
proto	proto	k8xC	proto
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
Newyorských	newyorský	k2eAgNnPc2d1	newyorské
divadel	divadlo	k1gNnPc2	divadlo
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
naživo	naživo	k1gNnSc1	naživo
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
taktéž	taktéž	k?	taktéž
platinovou	platinový	k2eAgFnSc4d1	platinová
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Love	lov	k1gInSc5	lov
Gun	Gun	k1gFnPc4	Gun
vydané	vydaný	k2eAgFnPc4d1	vydaná
roku	rok	k1gInSc3	rok
1977	[number]	k4	1977
bylo	být	k5eAaImAgNnS	být
zvukově	zvukově	k6eAd1	zvukově
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnSc6d1	podobná
předchozí	předchozí	k2eAgFnSc6d1	předchozí
desce	deska	k1gFnSc6	deska
a	a	k8xC	a
jen	jen	k9	jen
podtrhovalo	podtrhovat	k5eAaImAgNnS	podtrhovat
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
dominanci	dominance	k1gFnSc4	dominance
KISS	KISS	kA	KISS
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
hudebním	hudební	k2eAgInSc6d1	hudební
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
písní	píseň	k1gFnSc7	píseň
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hit	hit	k1gInSc1	hit
Love	lov	k1gInSc5	lov
gun	gun	k?	gun
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
další	další	k2eAgInPc4d1	další
hity	hit	k1gInPc4	hit
z	z	k7c2	z
alb	alba	k1gFnPc2	alba
Destroyer	Destroyer	k1gInSc1	Destroyer
<g/>
,	,	kIx,	,
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gMnSc1	Roll
Over	Over	k1gMnSc1	Over
a	a	k8xC	a
Love	lov	k1gInSc5	lov
Gun	Gun	k1gMnSc6	Gun
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
živém	živý	k2eAgInSc6d1	živý
záznamu	záznam	k1gInSc6	záznam
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
dvojalbu	dvojalbum	k1gNnSc6	dvojalbum
Alive	Aliev	k1gFnSc2	Aliev
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
desku	deska	k1gFnSc4	deska
KISS	KISS	kA	KISS
nahráli	nahrát	k5eAaBmAgMnP	nahrát
několik	několik	k4yIc1	několik
úplně	úplně	k6eAd1	úplně
nových	nový	k2eAgFnPc2d1	nová
písniček	písnička	k1gFnPc2	písnička
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
prý	prý	k9	prý
nechtěli	chtít	k5eNaImAgMnP	chtít
na	na	k7c6	na
koncertních	koncertní	k2eAgFnPc6d1	koncertní
šňůrách	šňůra	k1gFnPc6	šňůra
opakovat	opakovat	k5eAaImF	opakovat
stále	stále	k6eAd1	stále
stejný	stejný	k2eAgInSc4d1	stejný
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1976	[number]	k4	1976
až	až	k8xS	až
1978	[number]	k4	1978
skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
vydělala	vydělat	k5eAaPmAgFnS	vydělat
na	na	k7c6	na
nahrávkách	nahrávka	k1gFnPc6	nahrávka
a	a	k8xC	a
živém	živý	k2eAgNnSc6d1	živé
vystupování	vystupování	k1gNnSc6	vystupování
17,7	[number]	k4	17,7
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Marvel	Marvel	k1gInSc1	Marvel
Comics	comics	k1gInSc1	comics
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
komiks	komiks	k1gInSc1	komiks
s	s	k7c7	s
postavami	postava	k1gFnPc7	postava
super	super	k2eAgMnPc2d1	super
hrdinů	hrdina	k1gMnPc2	hrdina
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Gallupův	Gallupův	k2eAgInSc1d1	Gallupův
ústav	ústav	k1gInSc1	ústav
výzkumu	výzkum	k1gInSc2	výzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
je	být	k5eAaImIp3nS	být
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
skupina	skupina	k1gFnSc1	skupina
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
japonské	japonský	k2eAgFnPc1d1	japonská
Budokan	Budokan	k1gInSc4	Budokan
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Tokia	Tokio	k1gNnSc2	Tokio
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
skupina	skupina	k1gFnSc1	skupina
Kiss	Kiss	k1gInSc1	Kiss
5	[number]	k4	5
vyprodaných	vyprodaný	k2eAgInPc2d1	vyprodaný
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
překonala	překonat	k5eAaPmAgFnS	překonat
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
rekord	rekord	k1gInSc4	rekord
skupiny	skupina	k1gFnSc2	skupina
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS	vyjít
remixovaná	remixovaný	k2eAgFnSc1d1	remixovaná
kompilace	kompilace	k1gFnSc1	kompilace
jejich	jejich	k3xOp3gInPc2	jejich
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
dvojalbum	dvojalbum	k1gNnSc1	dvojalbum
Double	double	k2eAgNnSc1d1	double
Platinum	Platinum	k1gNnSc1	Platinum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c4	na
trh	trh	k1gInSc4	trh
dostal	dostat	k5eAaPmAgMnS	dostat
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
neměl	mít	k5eNaImAgInS	mít
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
komiksu	komiks	k1gInSc2	komiks
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
inkoust	inkoust	k1gInSc1	inkoust
s	s	k7c7	s
"	"	kIx"	"
<g/>
příměsí	příměs	k1gFnSc7	příměs
pravé	pravá	k1gFnSc2	pravá
krve	krev	k1gFnSc2	krev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
skupiny	skupina	k1gFnSc2	skupina
graficky	graficky	k6eAd1	graficky
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
pinball	pinball	k1gInSc1	pinball
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Kiss	Kiss	k1gInSc1	Kiss
Your	Your	k1gInSc1	Your
Face	Face	k1gInSc1	Face
Makeup	makeup	k1gInSc4	makeup
<g/>
"	"	kIx"	"
-	-	kIx~	-
šminkové	šminkový	k2eAgInPc4d1	šminkový
sety	set	k1gInPc4	set
<g/>
,	,	kIx,	,
kissácké	kissácký	k2eAgFnPc4d1	kissácký
masky	maska	k1gFnPc4	maska
na	na	k7c4	na
Halloween	Halloween	k1gInSc4	Halloween
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgFnPc4d1	stolní
hry	hra	k1gFnPc4	hra
s	s	k7c7	s
postavičkami	postavička	k1gFnPc7	postavička
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
"	"	kIx"	"
<g/>
pozoruhodných	pozoruhodný	k2eAgInPc2d1	pozoruhodný
suvenýrů	suvenýr	k1gInPc2	suvenýr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1977	[number]	k4	1977
až	až	k9	až
1979	[number]	k4	1979
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
materiálu	materiál	k1gInSc6	materiál
prodávaném	prodávaný	k2eAgInSc6d1	prodávaný
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
a	a	k8xC	a
během	během	k7c2	během
koncertů	koncert	k1gInPc2	koncert
podařilo	podařit	k5eAaPmAgNnS	podařit
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
vydělat	vydělat	k5eAaPmF	vydělat
asi	asi	k9	asi
100	[number]	k4	100
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
dostávala	dostávat	k5eAaImAgFnS	dostávat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
vrchol	vrchol	k1gInSc4	vrchol
komerční	komerční	k2eAgFnSc2d1	komerční
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
měla	mít	k5eAaImAgFnS	mít
čtyři	čtyři	k4xCgNnPc4	čtyři
platinová	platinový	k2eAgNnPc4d1	platinové
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
koncertů	koncert	k1gInPc2	koncert
byla	být	k5eAaImAgFnS	být
13	[number]	k4	13
350	[number]	k4	350
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
hrubý	hrubý	k2eAgInSc4d1	hrubý
příjem	příjem	k1gInSc4	příjem
skupiny	skupina	k1gFnSc2	skupina
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1977	[number]	k4	1977
byl	být	k5eAaImAgMnS	být
10,2	[number]	k4	10,2
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kreativním	kreativní	k2eAgMnSc7d1	kreativní
manažérem	manažér	k1gMnSc7	manažér
Billem	Bill	k1gMnSc7	Bill
Aucoinem	Aucoin	k1gMnSc7	Aucoin
hledali	hledat	k5eAaImAgMnP	hledat
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
úrovně	úroveň	k1gFnSc2	úroveň
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1978	[number]	k4	1978
Bill	Bill	k1gMnSc1	Bill
určil	určit	k5eAaPmAgMnS	určit
dva	dva	k4xCgInPc4	dva
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
situaci	situace	k1gFnSc4	situace
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
byl	být	k5eAaImAgInS	být
nápad	nápad	k1gInSc1	nápad
vydat	vydat	k5eAaPmF	vydat
čtyři	čtyři	k4xCgNnPc4	čtyři
sólová	sólový	k2eAgNnPc4d1	sólové
alba	album	k1gNnPc4	album
členů	člen	k1gMnPc2	člen
skupiny	skupina	k1gFnSc2	skupina
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
těchto	tento	k3xDgNnPc2	tento
alb	album	k1gNnPc2	album
navzájem	navzájem	k6eAd1	navzájem
nespolupracovali	spolupracovat	k5eNaImAgMnP	spolupracovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xS	jako
alba	alba	k1gFnSc1	alba
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
designem	design	k1gInSc7	design
obalů	obal	k1gInPc2	obal
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
vyšla	vyjít	k5eAaPmAgFnS	vyjít
naráz	naráz	k6eAd1	naráz
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
šance	šance	k1gFnSc1	šance
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
skupiny	skupina	k1gFnSc2	skupina
prezentovat	prezentovat	k5eAaBmF	prezentovat
své	svůj	k3xOyFgFnPc4	svůj
individuální	individuální	k2eAgFnPc4d1	individuální
hudební	hudební	k2eAgFnPc4d1	hudební
představy	představa	k1gFnPc4	představa
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Geneho	Gene	k1gMnSc4	Gene
albu	alba	k1gFnSc4	alba
hraje	hrát	k5eAaImIp3nS	hrát
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Aerosmith	Aerosmith	k1gMnSc1	Aerosmith
Joe	Joe	k1gFnSc1	Joe
Perry	Perra	k1gFnSc2	Perra
<g/>
,	,	kIx,	,
zpívá	zpívat	k5eAaImIp3nS	zpívat
disko	disko	k2eAgFnSc1d1	disko
hvězda	hvězda	k1gFnSc1	hvězda
Donna	donna	k1gFnSc1	donna
Summer	Summer	k1gInSc1	Summer
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Cher	Chera	k1gFnPc2	Chera
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
nahráli	nahrát	k5eAaBmAgMnP	nahrát
Stanley	Stanley	k1gInPc4	Stanley
a	a	k8xC	a
Frehley	Frehley	k1gInPc4	Frehley
jsou	být	k5eAaImIp3nP	být
víc	hodně	k6eAd2	hodně
hardrocková	hardrockový	k2eAgFnSc1d1	hardrocková
a	a	k8xC	a
Crissovo	Crissův	k2eAgNnSc1d1	Crissův
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
laděno	ladit	k5eAaImNgNnS	ladit
víc	hodně	k6eAd2	hodně
baladicky	baladicky	k6eAd1	baladicky
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B.	B.	kA	B.
Na	na	k7c4	na
trh	trh	k1gInSc4	trh
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
5	[number]	k4	5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
nosičů	nosič	k1gInPc2	nosič
<g/>
,	,	kIx,	,
všechna	všechen	k3xTgNnPc4	všechen
čtyři	čtyři	k4xCgNnPc4	čtyři
alba	album	k1gNnPc4	album
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
padesátky	padesátka	k1gFnSc2	padesátka
žebříčku	žebříček	k1gInSc2	žebříček
Billboardu	billboard	k1gInSc2	billboard
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
cílem	cíl	k1gInSc7	cíl
Aucoinsova	Aucoinsův	k2eAgInSc2d1	Aucoinsův
plánu	plán	k1gInSc2	plán
bylo	být	k5eAaImAgNnS	být
dostat	dostat	k5eAaPmF	dostat
členy	člen	k1gInPc4	člen
skupiny	skupina	k1gFnSc2	skupina
na	na	k7c4	na
filmová	filmový	k2eAgNnPc4d1	filmové
plátna	plátno	k1gNnPc4	plátno
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nP	aby
jejich	jejich	k3xOp3gFnPc1	jejich
postavy	postava	k1gFnPc1	postava
upevnily	upevnit	k5eAaPmAgFnP	upevnit
mínění	mínění	k1gNnSc4	mínění
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
hrdinství	hrdinství	k1gNnSc4	hrdinství
<g/>
.	.	kIx.	.
</s>
<s>
Filmovaní	filmovaný	k2eAgMnPc1d1	filmovaný
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
měl	mít	k5eAaImAgInS	mít
projekt	projekt	k1gInSc4	projekt
ambice	ambice	k1gFnSc2	ambice
stvořit	stvořit	k5eAaPmF	stvořit
něco	něco	k3yInSc4	něco
mezi	mezi	k7c4	mezi
Hard	Hard	k1gInSc4	Hard
Day	Day	k1gFnSc2	Day
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Beatles	Beatles	k1gFnSc2	Beatles
a	a	k8xC	a
Hvězdnými	hvězdný	k2eAgFnPc7d1	hvězdná
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc4	výsledek
nebyl	být	k5eNaImAgMnS	být
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
ambicemi	ambice	k1gFnPc7	ambice
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
se	se	k3xPyFc4	se
vícekrát	vícekrát	k6eAd1	vícekrát
přepisoval	přepisovat	k5eAaImAgInS	přepisovat
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Criss	Criss	k1gInSc4	Criss
a	a	k8xC	a
Frehley	Frehley	k1gInPc4	Frehley
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
znechuceni	znechutit	k5eAaPmNgMnP	znechutit
průběhem	průběh	k1gInSc7	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Criss	Criss	k6eAd1	Criss
se	se	k3xPyFc4	se
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
post-produkce	postrodukce	k1gFnPc4	post-produkce
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
dialogy	dialog	k1gInPc1	dialog
nadabovány	nadabován	k2eAgMnPc4d1	nadabován
jiným	jiný	k2eAgInSc7d1	jiný
hercem	herc	k1gInSc7	herc
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
Kiss	Kissa	k1gFnPc2	Kissa
Meets	Meets	k1gInSc4	Meets
the	the	k?	the
Phantom	Phantom	k1gInSc1	Phantom
of	of	k?	of
the	the	k?	the
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
produkovaný	produkovaný	k2eAgInSc1d1	produkovaný
společností	společnost	k1gFnSc7	společnost
Hanna-Barbera	Hanna-Barbera	k1gFnSc1	Hanna-Barbera
byl	být	k5eAaImAgInS	být
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
televizí	televize	k1gFnSc7	televize
NBC	NBC	kA	NBC
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
pohrdavé	pohrdavý	k2eAgFnPc4d1	pohrdavá
odezvy	odezva	k1gFnPc4	odezva
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
televizní	televizní	k2eAgInSc1d1	televizní
program	program	k1gInSc1	program
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
ratingů	rating	k1gInPc2	rating
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
hrdinů	hrdina	k1gMnPc2	hrdina
ale	ale	k8xC	ale
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
předvedl	předvést	k5eAaPmAgMnS	předvést
členy	člen	k1gMnPc4	člen
skupiny	skupina	k1gFnSc2	skupina
skoro	skoro	k6eAd1	skoro
jako	jako	k8xC	jako
šaškovské	šaškovský	k2eAgFnPc1d1	šaškovská
<g/>
,	,	kIx,	,
komické	komický	k2eAgFnPc1d1	komická
postavy	postava	k1gFnPc1	postava
<g/>
.	.	kIx.	.
</s>
<s>
Umělecký	umělecký	k2eAgInSc1d1	umělecký
propad	propad	k1gInSc1	propad
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
podnítil	podnítit	k5eAaPmAgMnS	podnítit
skupinu	skupina	k1gFnSc4	skupina
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
aby	aby	kYmCp3nS	aby
rozvázala	rozvázat	k5eAaPmAgFnS	rozvázat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Aucoinem	Aucoin	k1gMnSc7	Aucoin
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1979	[number]	k4	1979
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
další	další	k2eAgNnSc1d1	další
společné	společný	k2eAgNnSc1d1	společné
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
nazvané	nazvaný	k2eAgMnPc4d1	nazvaný
Dynasty	dynasta	k1gMnPc4	dynasta
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
platinovým	platinový	k2eAgMnSc7d1	platinový
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
světový	světový	k2eAgInSc1d1	světový
hit	hit	k1gInSc1	hit
historie	historie	k1gFnSc2	historie
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kiss	k1gInSc1	Kiss
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
stylů	styl	k1gInPc2	styl
Hard	Hard	k1gInSc4	Hard
rock	rock	k1gInSc1	rock
a	a	k8xC	a
Disko	disko	k2eAgFnSc1d1	disko
<g/>
:	:	kIx,	:
I	i	k8xC	i
Was	Was	k1gFnSc3	Was
Made	Made	k1gFnSc3	Made
For	forum	k1gNnPc2	forum
Lovin	Lovin	k1gInSc1	Lovin
<g/>
'	'	kIx"	'
You	You	k1gFnSc1	You
<g/>
.	.	kIx.	.
</s>
<s>
Bicí	bicí	k2eAgMnSc1d1	bicí
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
albu	album	k1gNnSc6	album
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
Anton	Anton	k1gMnSc1	Anton
Fig	Fig	k1gMnSc1	Fig
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
producent	producent	k1gMnSc1	producent
alba	album	k1gNnSc2	album
Vini	Vini	k1gNnSc2	Vini
Poncia	Poncium	k1gNnSc2	Poncium
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Crissova	Crissův	k2eAgFnSc1d1	Crissova
technická	technický	k2eAgFnSc1d1	technická
úroveň	úroveň	k1gFnSc1	úroveň
není	být	k5eNaImIp3nS	být
adekvátní	adekvátní	k2eAgFnSc3d1	adekvátní
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
si	se	k3xPyFc3	se
skladby	skladba	k1gFnPc1	skladba
alba	album	k1gNnSc2	album
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
Crissova	Crissův	k2eAgFnSc1d1	Crissova
skladba	skladba	k1gFnSc1	skladba
(	(	kIx(	(
<g/>
kterou	který	k3yIgFnSc7	který
i	i	k9	i
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
<g/>
)	)	kIx)	)
na	na	k7c6	na
albu	album	k1gNnSc6	album
je	on	k3xPp3gInPc4	on
Dirty	Dirt	k1gInPc4	Dirt
Livin	Livina	k1gFnPc2	Livina
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
koncertní	koncertní	k2eAgFnSc1d1	koncertní
šňůra	šňůra	k1gFnSc1	šňůra
k	k	k7c3	k
albu	album	k1gNnSc3	album
měla	mít	k5eAaImAgFnS	mít
název	název	k1gInSc4	název
The	The	k1gFnSc2	The
Return	Return	k1gInSc1	Return
of	of	k?	of
Kiss	Kiss	k1gInSc1	Kiss
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
šňůra	šňůra	k1gFnSc1	šňůra
byla	být	k5eAaImAgFnS	být
poznamenaná	poznamenaný	k2eAgFnSc1d1	poznamenaná
poklesem	pokles	k1gInSc7	pokles
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Crissova	Crissův	k2eAgFnSc1d1	Crissova
technická	technický	k2eAgFnSc1d1	technická
úroveň	úroveň	k1gFnSc1	úroveň
byla	být	k5eAaImAgFnS	být
opravdu	opravdu	k6eAd1	opravdu
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
zpomalení	zpomalení	k1gNnSc3	zpomalení
rytmu	rytmus	k1gInSc2	rytmus
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
koncert	koncert	k1gInSc1	koncert
této	tento	k3xDgFnSc2	tento
šňůry	šňůra	k1gFnSc2	šňůra
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1979	[number]	k4	1979
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
posledním	poslední	k2eAgNnSc7d1	poslední
Crissovým	Crissův	k2eAgNnSc7d1	Crissův
vystoupením	vystoupení	k1gNnSc7	vystoupení
s	s	k7c7	s
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
album	album	k1gNnSc1	album
Unmasked	Unmasked	k1gInSc1	Unmasked
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaBmNgNnS	nahrát
se	s	k7c7	s
studiovým	studiový	k2eAgMnSc7d1	studiový
bubeníkem	bubeník	k1gMnSc7	bubeník
Antonem	Anton	k1gMnSc7	Anton
Figem	Fig	k1gMnSc7	Fig
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
má	mít	k5eAaImIp3nS	mít
víc	hodně	k6eAd2	hodně
popový	popový	k2eAgInSc1d1	popový
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgInSc7	první
od	od	k7c2	od
alba	album	k1gNnSc2	album
Dressed	Dressed	k1gInSc1	Dressed
to	ten	k3xDgNnSc1	ten
Kill	Kill	k1gInSc1	Kill
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
na	na	k7c4	na
platinový	platinový	k2eAgInSc4d1	platinový
prodej	prodej	k1gInSc4	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Crisse	Crisse	k6eAd1	Crisse
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Eric	Eric	k1gFnSc4	Eric
Carr	Carr	k1gMnSc1	Carr
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1980	[number]	k4	1980
v	v	k7c4	v
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
neexistujícím	existující	k2eNgInSc7d1	neexistující
Palladium	palladium	k1gNnSc1	palladium
Theatre	Theatr	k1gInSc5	Theatr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncert	koncert	k1gInSc1	koncert
k	k	k7c3	k
albu	album	k1gNnSc3	album
Unmasked	Unmasked	k1gInSc1	Unmasked
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgNnSc7d1	jediné
vystoupením	vystoupení	k1gNnSc7	vystoupení
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
potom	potom	k6eAd1	potom
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
šňůru	šňůra	k1gFnSc4	šňůra
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
turné	turné	k1gNnSc1	turné
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
médií	médium	k1gNnPc2	médium
bylo	být	k5eAaImAgNnS	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nahrávání	nahrávání	k1gNnSc4	nahrávání
dalšího	další	k2eAgNnSc2d1	další
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
přizvat	přizvat	k5eAaPmF	přizvat
producenta	producent	k1gMnSc4	producent
Boba	Bob	k1gMnSc4	Bob
Ezrina	Ezrin	k1gMnSc4	Ezrin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
osvědčil	osvědčit	k5eAaPmAgInS	osvědčit
při	při	k7c6	při
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
albu	album	k1gNnSc6	album
Destroyer	Destroyra	k1gFnPc2	Destroyra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
tiskové	tiskový	k2eAgFnPc1d1	tisková
zprávy	zpráva	k1gFnPc1	zpráva
hovořily	hovořit	k5eAaImAgFnP	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
hardrockovému	hardrockový	k2eAgInSc3d1	hardrockový
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
album	album	k1gNnSc1	album
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc4	název
Music	Music	k1gMnSc1	Music
from	from	k1gMnSc1	from
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Elder	Elder	k1gMnSc1	Elder
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
koncepční	koncepční	k2eAgNnSc1d1	koncepční
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
hrají	hrát	k5eAaImIp3nP	hrát
i	i	k9	i
středověké	středověký	k2eAgInPc1d1	středověký
dechové	dechový	k2eAgInPc1d1	dechový
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
smyčce	smyčec	k1gInPc1	smyčec
<g/>
,	,	kIx,	,
harfy	harfa	k1gFnPc1	harfa
a	a	k8xC	a
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
prezentováno	prezentovat	k5eAaBmNgNnS	prezentovat
jako	jako	k8xC	jako
soundtrack	soundtrack	k1gInSc1	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
natočen	natočit	k5eAaBmNgInS	natočit
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skutečnosti	skutečnost	k1gFnSc3	skutečnost
není	být	k5eNaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
pochopit	pochopit	k5eAaPmF	pochopit
koncepci	koncepce	k1gFnSc4	koncepce
jeho	jeho	k3xOp3gFnSc2	jeho
dějové	dějový	k2eAgFnSc2d1	dějová
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Komerčně	komerčně	k6eAd1	komerčně
tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
nebyl	být	k5eNaImAgInS	být
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vydání	vydání	k1gNnSc6	vydání
ani	ani	k9	ani
nenásledovalo	následovat	k5eNaImAgNnS	následovat
větší	veliký	k2eAgNnSc4d2	veliký
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
situací	situace	k1gFnSc7	situace
frustrovaný	frustrovaný	k2eAgMnSc1d1	frustrovaný
i	i	k8xC	i
Ace	Ace	k1gMnSc1	Ace
Frehley	Frehlea	k1gFnSc2	Frehlea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
už	už	k6eAd1	už
na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
nespolupracoval	spolupracovat	k5eNaImAgMnS	spolupracovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Frehley	Frehlea	k1gFnSc2	Frehlea
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
na	na	k7c6	na
obalech	obal	k1gInPc6	obal
následujících	následující	k2eAgFnPc2d1	následující
alb	alba	k1gFnPc2	alba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
Killers	Killersa	k1gFnPc2	Killersa
a	a	k8xC	a
Creatures	Creaturesa	k1gFnPc2	Creaturesa
of	of	k?	of
the	the	k?	the
Night	Night	k1gInSc1	Night
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Creatures	Creaturesa	k1gFnPc2	Creaturesa
of	of	k?	of
the	the	k?	the
Night	Nighta	k1gFnPc2	Nighta
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
45	[number]	k4	45
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčků	žebříček	k1gInPc2	žebříček
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Kytarové	kytarový	k2eAgFnPc4d1	kytarová
party	parta	k1gFnPc4	parta
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
albu	album	k1gNnSc6	album
nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
vícero	vícero	k1gNnSc1	vícero
muzikantů	muzikant	k1gMnPc2	muzikant
včetně	včetně	k7c2	včetně
Vinnie	Vinnie	k1gFnSc2	Vinnie
Vincenta	Vincent	k1gMnSc2	Vincent
<g/>
.	.	kIx.	.
</s>
<s>
Vincent	Vincent	k1gMnSc1	Vincent
oficiálně	oficiálně	k6eAd1	oficiálně
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Frehleye	Frehleye	k1gInSc4	Frehleye
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1982	[number]	k4	1982
během	během	k7c2	během
koncertního	koncertní	k2eAgNnSc2d1	koncertní
turné	turné	k1gNnSc2	turné
při	při	k7c6	při
10	[number]	k4	10
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vincentova	Vincentův	k2eAgFnSc1d1	Vincentova
identita	identita	k1gFnSc1	identita
<g/>
/	/	kIx~	/
<g/>
maska	maska	k1gFnSc1	maska
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
egyptského	egyptský	k2eAgMnSc4d1	egyptský
bojovníka	bojovník	k1gMnSc4	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
turné	turné	k1gNnSc1	turné
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skupina	skupina	k1gFnSc1	skupina
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
koncerty	koncert	k1gInPc7	koncert
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgNnPc6	který
bylo	být	k5eAaImAgNnS	být
nejvíc	nejvíc	k6eAd1	nejvíc
diváků	divák	k1gMnPc2	divák
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
137	[number]	k4	137
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
v	v	k7c6	v
Rio	Rio	k1gFnSc6	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1983	[number]	k4	1983
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
Sã	Sã	k1gFnSc6	Sã
Paulo	Paula	k1gFnSc5	Paula
poslední	poslední	k2eAgInPc4d1	poslední
koncert	koncert	k1gInSc4	koncert
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
skupina	skupina	k1gFnSc1	skupina
Kiss	Kiss	k1gInSc1	Kiss
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
maskách	maska	k1gFnPc6	maska
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Lick	Lick	k1gMnSc1	Lick
It	It	k1gMnSc1	It
Up	Up	k1gMnSc1	Up
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
znovu	znovu	k6eAd1	znovu
oživit	oživit	k5eAaPmF	oživit
kariéru	kariéra	k1gFnSc4	kariéra
skupiny	skupina	k1gFnSc2	skupina
bylo	být	k5eAaImAgNnS	být
zároveň	zároveň	k6eAd1	zároveň
jejich	jejich	k3xOp3gFnSc7	jejich
prvním	první	k4xOgMnSc6	první
po	po	k7c6	po
demaskovaní	demaskovaný	k2eAgMnPc1d1	demaskovaný
<g/>
.	.	kIx.	.
</s>
<s>
Ukončení	ukončení	k1gNnSc1	ukončení
používání	používání	k1gNnSc2	používání
masek	maska	k1gFnPc2	maska
a	a	k8xC	a
kostýmů	kostým	k1gInPc2	kostým
mělo	mít	k5eAaImAgNnS	mít
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
změnu	změna	k1gFnSc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Odmaskování	odmaskování	k1gNnSc1	odmaskování
KISS	KISS	kA	KISS
je	být	k5eAaImIp3nS	být
však	však	k9	však
dnes	dnes	k6eAd1	dnes
bráno	brát	k5eAaImNgNnS	brát
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejhorších	zlý	k2eAgNnPc2d3	nejhorší
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
ztratila	ztratit	k5eAaPmAgFnS	ztratit
démonickou	démonický	k2eAgFnSc4d1	démonická
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
předtím	předtím	k6eAd1	předtím
oplývaly	oplývat	k5eAaImAgInP	oplývat
jejich	jejich	k3xOp3gInPc1	jejich
koncerty	koncert	k1gInPc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
bez	bez	k7c2	bez
masek	maska	k1gFnPc2	maska
objevili	objevit	k5eAaPmAgMnP	objevit
při	při	k7c6	při
prezentaci	prezentace	k1gFnSc6	prezentace
alb	album	k1gNnPc2	album
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1983	[number]	k4	1983
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
MTV	MTV	kA	MTV
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Lick	Licka	k1gFnPc2	Licka
It	It	k1gFnPc2	It
Up	Up	k1gMnPc2	Up
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
hned	hned	k6eAd1	hned
získalo	získat	k5eAaPmAgNnS	získat
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
byla	být	k5eAaImAgFnS	být
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
při	při	k7c6	při
předešlém	předešlý	k2eAgInSc6d1	předešlý
projektu	projekt	k1gInSc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
koncertního	koncertní	k2eAgNnSc2d1	koncertní
turné	turné	k1gNnSc2	turné
vznikaly	vznikat	k5eAaImAgInP	vznikat
tlaky	tlak	k1gInPc1	tlak
mezi	mezi	k7c7	mezi
Vincentem	Vincent	k1gMnSc7	Vincent
a	a	k8xC	a
zbytkem	zbytek	k1gInSc7	zbytek
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
skončily	skončit	k5eAaPmAgInP	skončit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1984	[number]	k4	1984
Vincentovým	Vincentův	k2eAgInSc7d1	Vincentův
odchodem	odchod	k1gInSc7	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gNnSc4	on
bývalý	bývalý	k2eAgMnSc1d1	bývalý
studiový	studiový	k2eAgMnSc1d1	studiový
hráč	hráč	k1gMnSc1	hráč
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
Mark	Mark	k1gMnSc1	Mark
St.	st.	kA	st.
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
kytaristou	kytarista	k1gMnSc7	kytarista
skupina	skupina	k1gFnSc1	skupina
nahrála	nahrát	k5eAaBmAgFnS	nahrát
v	v	k7c6	v
září	září	k1gNnSc6	září
1984	[number]	k4	1984
nejlépe	dobře	k6eAd3	dobře
prodávané	prodávaný	k2eAgNnSc4d1	prodávané
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Animalize	Animalize	k1gFnSc2	Animalize
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
úspěch	úspěch	k1gInSc1	úspěch
skupiny	skupina	k1gFnSc2	skupina
nebyl	být	k5eNaImAgInS	být
porovnatelný	porovnatelný	k2eAgInSc1d1	porovnatelný
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
zlatou	zlatý	k2eAgFnSc7d1	zlatá
érou	éra	k1gFnSc7	éra
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zlepšení	zlepšení	k1gNnSc3	zlepšení
jejich	jejich	k3xOp3gNnSc2	jejich
postavení	postavení	k1gNnSc2	postavení
na	na	k7c6	na
hudebním	hudební	k2eAgInSc6d1	hudební
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
St.	st.	kA	st.
John	John	k1gMnSc1	John
měl	mít	k5eAaImAgMnS	mít
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
reaktivní	reaktivní	k2eAgFnSc7d1	reaktivní
artritidou	artritida	k1gFnSc7	artritida
<g/>
.	.	kIx.	.
</s>
<s>
Choroba	choroba	k1gFnSc1	choroba
ho	on	k3xPp3gMnSc4	on
omezovala	omezovat	k5eAaImAgFnS	omezovat
na	na	k7c6	na
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
výměně	výměna	k1gFnSc6	výměna
během	během	k7c2	během
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
sólového	sólový	k2eAgMnSc2d1	sólový
kytaristy	kytarista	k1gMnSc2	kytarista
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1984	[number]	k4	1984
Bruce	Bruce	k1gMnSc1	Bruce
Kullick	Kullick	k1gMnSc1	Kullick
<g/>
.	.	kIx.	.
</s>
<s>
Sestava	sestava	k1gFnSc1	sestava
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
,	,	kIx,	,
Simmons	Simmonsa	k1gFnPc2	Simmonsa
<g/>
,	,	kIx,	,
Carr	Carra	k1gFnPc2	Carra
a	a	k8xC	a
Kullick	Kullicka	k1gFnPc2	Kullicka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejstabilnější	stabilní	k2eAgFnSc1d3	nejstabilnější
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
originální	originální	k2eAgFnSc2d1	originální
sestavy	sestava	k1gFnSc2	sestava
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
vydali	vydat	k5eAaPmAgMnP	vydat
sérii	série	k1gFnSc4	série
platinových	platinový	k2eAgNnPc2d1	platinové
alb	album	k1gNnPc2	album
<g/>
:	:	kIx,	:
Asylum	Asylum	k1gInSc1	Asylum
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Crazy	Craz	k1gInPc1	Craz
Nights	Nightsa	k1gFnPc2	Nightsa
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
a	a	k8xC	a
kompilaci	kompilace	k1gFnSc4	kompilace
jejich	jejich	k3xOp3gInPc2	jejich
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
Smashes	Smashesa	k1gFnPc2	Smashesa
<g/>
,	,	kIx,	,
Trashes	Trashesa	k1gFnPc2	Trashesa
&	&	k?	&
Hits	Hitsa	k1gFnPc2	Hitsa
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Crazy	Craza	k1gFnSc2	Craza
Nights	Nightsa	k1gFnPc2	Nightsa
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgNnPc2d3	nejúspěšnější
alb	album	k1gNnPc2	album
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
kdy	kdy	k6eAd1	kdy
skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
nahrála	nahrát	k5eAaBmAgFnS	nahrát
<g/>
.	.	kIx.	.
</s>
<s>
Single	singl	k1gInSc5	singl
Crazy	Craz	k1gInPc4	Craz
<g/>
,	,	kIx,	,
Crazy	Craz	k1gInPc4	Craz
Nights	Nightsa	k1gFnPc2	Nightsa
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
příčky	příčka	k1gFnPc4	příčka
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
album	album	k1gNnSc1	album
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Hot	hot	k0	hot
in	in	k?	in
the	the	k?	the
Shade	Shad	k1gInSc5	Shad
sice	sice	k8xC	sice
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
platiny	platina	k1gFnSc2	platina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
singl	singl	k1gInSc1	singl
Forever	Forevra	k1gFnPc2	Forevra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zkomponován	zkomponovat	k5eAaPmNgInS	zkomponovat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Michaelem	Michael	k1gMnSc7	Michael
Boltonem	Bolton	k1gInSc7	Bolton
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
80	[number]	k4	80
<g/>
.	.	kIx.	.
příčky	příčka	k1gFnSc2	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
umístění	umístění	k1gNnSc1	umístění
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
singlu	singl	k1gInSc2	singl
I	i	k8xC	i
Was	Was	k1gFnSc3	Was
Made	Made	k1gFnSc3	Made
for	forum	k1gNnPc2	forum
Lovin	Lovin	k1gInSc1	Lovin
<g/>
'	'	kIx"	'
You	You	k1gFnSc1	You
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
singl	singl	k1gInSc1	singl
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
singlem	singl	k1gInSc7	singl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
žebříčcích	žebříček	k1gInPc6	žebříček
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odmaskovaní	odmaskovaný	k2eAgMnPc1d1	odmaskovaný
skupiny	skupina	k1gFnSc2	skupina
ztratil	ztratit	k5eAaPmAgInS	ztratit
Kiss	Kiss	k1gInSc1	Kiss
svůj	svůj	k3xOyFgInSc4	svůj
démonický	démonický	k2eAgInSc4d1	démonický
image	image	k1gInSc4	image
<g/>
.	.	kIx.	.
</s>
<s>
Simmons	Simmons	k1gInSc1	Simmons
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vedoucí	vedoucí	k1gMnSc1	vedoucí
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
atraktivitě	atraktivita	k1gFnSc6	atraktivita
pro	pro	k7c4	pro
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
prominentem	prominent	k1gMnSc7	prominent
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
.	.	kIx.	.
</s>
<s>
Produkcí	produkce	k1gFnSc7	produkce
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
pověřen	pověřen	k2eAgMnSc1d1	pověřen
Bob	Bob	k1gMnSc1	Bob
Ezrin	Ezrin	k1gMnSc1	Ezrin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
stihla	stihnout	k5eAaPmAgFnS	stihnout
pustit	pustit	k5eAaPmF	pustit
do	do	k7c2	do
nahrávaní	nahrávaný	k2eAgMnPc1d1	nahrávaný
<g/>
,	,	kIx,	,
zjistili	zjistit	k5eAaPmAgMnP	zjistit
lékaři	lékař	k1gMnPc1	lékař
u	u	k7c2	u
Erica	Ericus	k1gMnSc2	Ericus
Carra	Carra	k1gFnSc1	Carra
nádor	nádor	k1gInSc1	nádor
na	na	k7c6	na
srdci	srdce	k1gNnSc6	srdce
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
úspěšně	úspěšně	k6eAd1	úspěšně
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
objevily	objevit	k5eAaPmAgInP	objevit
nádory	nádor	k1gInPc1	nádor
na	na	k7c6	na
plicích	plíce	k1gFnPc6	plíce
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterým	který	k3yIgFnPc3	který
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
chemoterapii	chemoterapie	k1gFnSc3	chemoterapie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
léčba	léčba	k1gFnSc1	léčba
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
Eric	Eric	k1gFnSc1	Eric
nad	nad	k7c7	nad
rakovinou	rakovina	k1gFnSc7	rakovina
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
s	s	k7c7	s
prvním	první	k4xOgMnSc7	první
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
krvácení	krvácení	k1gNnPc2	krvácení
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
41	[number]	k4	41
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c4	v
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Queen	Queen	k1gInSc1	Queen
<g/>
,	,	kIx,	,
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
.	.	kIx.	.
</s>
<s>
Tragédií	tragédie	k1gFnSc7	tragédie
zničení	zničení	k1gNnSc2	zničení
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
hráčem	hráč	k1gMnSc7	hráč
na	na	k7c4	na
bicí	bicí	k2eAgInSc4d1	bicí
Ericem	Erice	k1gMnSc7	Erice
Singerem	Singer	k1gMnSc7	Singer
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k6eAd1	Eric
Singer	Singer	k1gMnSc1	Singer
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předtím	předtím	k6eAd1	předtím
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
,	,	kIx,	,
Garym	Garym	k1gInSc1	Garym
Moorem	Moorma	k1gFnPc2	Moorma
<g/>
,	,	kIx,	,
Litou	litý	k2eAgFnSc7d1	litá
Ford	ford	k1gInSc4	ford
a	a	k8xC	a
Alice	Alice	k1gFnSc2	Alice
Cooperem	Coopero	k1gNnSc7	Coopero
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
již	již	k6eAd1	již
známý	známý	k2eAgMnSc1d1	známý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c6	na
klubových	klubový	k2eAgFnPc6d1	klubová
akcích	akce	k1gFnPc6	akce
Paula	Paul	k1gMnSc2	Paul
Stanleyeho	Stanleye	k1gMnSc2	Stanleye
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
album	album	k1gNnSc4	album
Revenge	Reveng	k1gInSc2	Reveng
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
laděno	ladit	k5eAaImNgNnS	ladit
v	v	k7c6	v
hardrockovém	hardrockový	k2eAgInSc6d1	hardrockový
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
singl	singl	k1gInSc1	singl
Unholy	Unhola	k1gFnSc2	Unhola
<g/>
.	.	kIx.	.
</s>
<s>
Překvapující	překvapující	k2eAgFnSc1d1	překvapující
byla	být	k5eAaImAgFnS	být
podpora	podpora	k1gFnSc1	podpora
z	z	k7c2	z
tantiém	tantiéma	k1gFnPc2	tantiéma
na	na	k7c4	na
skladby	skladba	k1gFnPc4	skladba
pro	pro	k7c4	pro
bývalého	bývalý	k2eAgMnSc4d1	bývalý
(	(	kIx(	(
<g/>
vyhozeného	vyhozený	k2eAgMnSc2d1	vyhozený
<g/>
)	)	kIx)	)
člena	člen	k1gMnSc2	člen
skupiny	skupina	k1gFnSc2	skupina
Vinnieho	Vinnie	k1gMnSc2	Vinnie
Vincenta	Vincent	k1gMnSc2	Vincent
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
žebříčku	žebříček	k1gInSc2	žebříček
hitparád	hitparáda	k1gFnPc2	hitparáda
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
zlatým	zlatý	k1gInSc7	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
Erica	Ericus	k1gMnSc2	Ericus
Carra	Carr	k1gMnSc2	Carr
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
albu	album	k1gNnSc6	album
jako	jako	k8xS	jako
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
skladba	skladba	k1gFnSc1	skladba
zařazená	zařazený	k2eAgFnSc1d1	zařazená
starší	starý	k2eAgFnSc1d2	starší
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
nahrávka	nahrávka	k1gFnSc1	nahrávka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Carr	Carr	k1gInSc1	Carr
Jam	jáma	k1gFnPc2	jáma
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
stěžejní	stěžejní	k2eAgFnSc7d1	stěžejní
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
Carrovo	Carrův	k2eAgNnSc4d1	Carrovo
sólo	sólo	k1gNnSc4	sólo
na	na	k7c4	na
bicí	bicí	k2eAgFnPc4d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1	nahrávka
z	z	k7c2	z
koncertního	koncertní	k2eAgNnSc2d1	koncertní
turné	turné	k1gNnSc2	turné
Revenge	Reveng	k1gFnSc2	Reveng
Tour	Toura	k1gFnPc2	Toura
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
na	na	k7c6	na
živém	živý	k2eAgNnSc6d1	živé
albu	album	k1gNnSc6	album
Alive	Aliev	k1gFnSc2	Aliev
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vydáno	vydat	k5eAaPmNgNnS	vydat
album	album	k1gNnSc4	album
Kiss	Kissa	k1gFnPc2	Kissa
My	my	k3xPp1nPc1	my
Ass	Ass	k1gMnPc1	Ass
<g/>
:	:	kIx,	:
Classic	Classic	k1gMnSc1	Classic
Kiss	Kissa	k1gFnPc2	Kissa
Regrooved	Regrooved	k1gMnSc1	Regrooved
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
kompilací	kompilace	k1gFnSc7	kompilace
jejich	jejich	k3xOp3gInPc2	jejich
známých	známý	k2eAgInPc2d1	známý
hitů	hit	k1gInPc2	hit
v	v	k7c6	v
interpretaci	interpretace	k1gFnSc6	interpretace
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
hvězd	hvězda	k1gFnPc2	hvězda
jako	jako	k8xS	jako
například	například	k6eAd1	například
Lenny	Lenny	k?	Lenny
Kravitz	Kravitz	k1gInSc1	Kravitz
<g/>
,	,	kIx,	,
Stevie	Stevie	k1gFnSc1	Stevie
Wonder	Wonder	k1gMnSc1	Wonder
<g/>
,	,	kIx,	,
Garth	Garth	k1gMnSc1	Garth
Brooks	Brooks	k1gInSc1	Brooks
(	(	kIx(	(
<g/>
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anthrax	Anthrax	k1gInSc1	Anthrax
<g/>
,	,	kIx,	,
Gin	gin	k1gInSc1	gin
Blossoms	Blossoms	k1gInSc1	Blossoms
<g/>
,	,	kIx,	,
Toad	Toad	k1gMnSc1	Toad
the	the	k?	the
Wet	Wet	k1gMnSc1	Wet
Sprocket	Sprocket	k1gMnSc1	Sprocket
<g/>
,	,	kIx,	,
Shandi	Shand	k1gMnPc1	Shand
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Addiction	Addiction	k1gInSc1	Addiction
<g/>
,	,	kIx,	,
Dinosaur	Dinosaur	k1gMnSc1	Dinosaur
Jr	Jr	k1gMnSc1	Jr
<g/>
,	,	kIx,	,
Extreme	Extrem	k1gInSc5	Extrem
<g/>
,	,	kIx,	,
The	The	k1gFnPc3	The
Lemonheads	Lemonheadsa	k1gFnPc2	Lemonheadsa
<g/>
,	,	kIx,	,
The	The	k1gFnSc2	The
Mighty	Mighta	k1gFnSc2	Mighta
Mighty	Mighta	k1gFnSc2	Mighta
Bosstones	Bosstonesa	k1gFnPc2	Bosstonesa
<g/>
,	,	kIx,	,
Yoshiki	Yoshiki	k1gNnPc2	Yoshiki
a	a	k8xC	a
Die	Die	k1gFnPc2	Die
Ärzte	Ärzt	k1gMnSc5	Ärzt
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
19	[number]	k4	19
<g/>
.	.	kIx.	.
příčky	příčka	k1gFnSc2	příčka
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboardu	billboard	k1gInSc2	billboard
a	a	k8xC	a
od	od	k7c2	od
RIAA	RIAA	kA	RIAA
získalo	získat	k5eAaPmAgNnS	získat
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
knihu	kniha	k1gFnSc4	kniha
Kisstory	Kisstor	k1gMnPc4	Kisstor
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
byla	být	k5eAaImAgFnS	být
detailní	detailní	k2eAgFnSc1d1	detailní
historie	historie	k1gFnSc1	historie
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c4	na
unikátní	unikátní	k2eAgFnSc4d1	unikátní
a	a	k8xC	a
pozitivně	pozitivně	k6eAd1	pozitivně
vnímané	vnímaný	k2eAgInPc1d1	vnímaný
celosvětové	celosvětový	k2eAgInPc1d1	celosvětový
Worldwide	Worldwid	k1gInSc5	Worldwid
Kiss	Kiss	k1gInSc4	Kiss
Convention	Convention	k1gInSc4	Convention
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
své	svůj	k3xOyFgInPc4	svůj
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
z	z	k7c2	z
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
,	,	kIx,	,
dělala	dělat	k5eAaImAgFnS	dělat
i	i	k9	i
besedy	beseda	k1gFnPc1	beseda
s	s	k7c7	s
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
,	,	kIx,	,
podpisové	podpisový	k2eAgFnPc4d1	podpisová
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
hrála	hrát	k5eAaImAgFnS	hrát
dvouhodinové	dvouhodinový	k2eAgInPc4d1	dvouhodinový
akustické	akustický	k2eAgInPc4d1	akustický
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgMnPc6	který
hrála	hrát	k5eAaImAgFnS	hrát
skladby	skladba	k1gFnPc4	skladba
podle	podle	k7c2	podle
spontánních	spontánní	k2eAgInPc2d1	spontánní
požadavků	požadavek	k1gInPc2	požadavek
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
koncertu	koncert	k1gInSc6	koncert
v	v	k7c6	v
USA	USA	kA	USA
zpíval	zpívat	k5eAaImAgMnS	zpívat
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
16	[number]	k4	16
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
s	s	k7c7	s
Kiss	Kissa	k1gFnPc2	Kissa
skladby	skladba	k1gFnSc2	skladba
Hard	Hard	k1gMnSc1	Hard
Luck	Luck	k1gMnSc1	Luck
Woman	Woman	k1gMnSc1	Woman
a	a	k8xC	a
Nothin	Nothin	k1gMnSc1	Nothin
<g/>
'	'	kIx"	'
to	ten	k3xDgNnSc1	ten
Lose	los	k1gInSc6	los
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Peter	Peter	k1gMnSc1	Peter
Criss	Criss	k1gInSc1	Criss
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
se	se	k3xPyFc4	se
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
znovu	znovu	k6eAd1	znovu
spojili	spojit	k5eAaPmAgMnP	spojit
Criss	Criss	k1gInSc4	Criss
s	s	k7c7	s
Frehleyem	Frehley	k1gInSc7	Frehley
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
spolu	spolu	k6eAd1	spolu
zahráli	zahrát	k5eAaPmAgMnP	zahrát
v	v	k7c6	v
programu	program	k1gInSc6	program
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gInSc1	Unplugged
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
spustila	spustit	k5eAaPmAgFnS	spustit
vlnu	vlna	k1gFnSc4	vlna
spekulací	spekulace	k1gFnPc2	spekulace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
spojí	spojit	k5eAaPmIp3nS	spojit
původní	původní	k2eAgFnSc1d1	původní
sestava	sestava	k1gFnSc1	sestava
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
týdnů	týden	k1gInPc2	týden
po	po	k7c4	po
Unplugged	Unplugged	k1gInSc4	Unplugged
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
Simmons	Simmons	k1gInSc1	Simmons
<g/>
,	,	kIx,	,
Stanley	Stanley	k1gInPc1	Stanley
<g/>
,	,	kIx,	,
Kullick	Kullick	k1gInSc1	Kullick
a	a	k8xC	a
Singer	Singer	k1gInSc1	Singer
<g/>
)	)	kIx)	)
střetla	střetnout	k5eAaPmAgFnS	střetnout
v	v	k7c6	v
nahrávacím	nahrávací	k2eAgNnSc6d1	nahrávací
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahrála	nahrát	k5eAaPmAgFnS	nahrát
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
album	album	k1gNnSc1	album
Carnival	Carnival	k1gMnSc7	Carnival
of	of	k?	of
Souls	Souls	k1gInSc1	Souls
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Final	Final	k1gMnSc1	Final
Sessions	Sessions	k1gInSc1	Sessions
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
hotovo	hotov	k2eAgNnSc1d1	hotovo
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g />
.	.	kIx.	.
</s>
<s>
jeho	jeho	k3xOp3gNnSc1	jeho
vydání	vydání	k1gNnSc1	vydání
se	se	k3xPyFc4	se
opozdilo	opozdit	k5eAaPmAgNnS	opozdit
skoro	skoro	k6eAd1	skoro
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1996	[number]	k4	1996
během	během	k7c2	během
36	[number]	k4	36
<g/>
.	.	kIx.	.
udělování	udělování	k1gNnSc1	udělování
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
se	se	k3xPyFc4	se
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
zbroji	zbroj	k1gFnSc6	zbroj
s	s	k7c7	s
maskami	maska	k1gFnPc7	maska
a	a	k8xC	a
v	v	k7c6	v
kostýmech	kostým	k1gInPc6	kostým
z	z	k7c2	z
éry	éra	k1gFnSc2	éra
Love	lov	k1gInSc5	lov
Gun	Gun	k1gMnPc4	Gun
za	za	k7c2	za
obrovských	obrovský	k2eAgFnPc2d1	obrovská
ovací	ovace	k1gFnPc2	ovace
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
originální	originální	k2eAgFnSc1d1	originální
sestava	sestava	k1gFnSc1	sestava
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
na	na	k7c6	na
tiskovce	tiskovka	k1gFnSc6	tiskovka
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
KISS	KISS	kA	KISS
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
původní	původní	k2eAgFnSc6d1	původní
sestavě	sestava	k1gFnSc6	sestava
rozeběhnout	rozeběhnout	k5eAaPmF	rozeběhnout
koncertní	koncertní	k2eAgFnSc4d1	koncertní
šňůru	šňůra	k1gFnSc4	šňůra
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
svého	svůj	k3xOyFgMnSc2	svůj
nového	nový	k2eAgMnSc2d1	nový
manažera	manažer	k1gMnSc2	manažer
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Doc	doc	kA	doc
McGhee	McGhe	k1gInPc1	McGhe
<g/>
.	.	kIx.	.
</s>
<s>
Konference	konference	k1gFnSc1	konference
moderovaná	moderovaný	k2eAgFnSc1d1	moderovaná
Conanem	Conan	k1gMnSc7	Conan
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Brienem	Brieno	k1gNnSc7	Brieno
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
do	do	k7c2	do
58	[number]	k4	58
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
40	[number]	k4	40
000	[number]	k4	000
lístků	lístek	k1gInPc2	lístek
na	na	k7c4	na
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
vyprodány	vyprodat	k5eAaPmNgFnP	vyprodat
za	za	k7c4	za
47	[number]	k4	47
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
veřejným	veřejný	k2eAgMnSc7d1	veřejný
vystoupením	vystoupení	k1gNnPc3	vystoupení
byla	být	k5eAaImAgNnP	být
asi	asi	k9	asi
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
show	show	k1gFnSc1	show
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
pro	pro	k7c4	pro
výroční	výroční	k2eAgNnSc4d1	výroční
KROQ	KROQ	kA	KROQ
Weenie	Weenie	k1gFnSc1	Weenie
Roast	Roast	k1gFnSc1	Roast
v	v	k7c6	v
Irvine	Irvin	k1gInSc5	Irvin
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc5	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
Alive	Aliev	k1gFnSc2	Aliev
<g/>
/	/	kIx~	/
<g/>
Worldwide	Worldwid	k1gInSc5	Worldwid
Tour	Tour	k1gInSc1	Tour
začalo	začít	k5eAaPmAgNnS	začít
vystoupením	vystoupení	k1gNnSc7	vystoupení
před	před	k7c4	před
39	[number]	k4	39
867	[number]	k4	867
diváky	divák	k1gMnPc7	divák
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
na	na	k7c4	na
Tiger	Tiger	k1gInSc4	Tiger
Stadium	stadium	k1gNnSc1	stadium
v	v	k7c6	v
Detroitu	Detroit	k1gInSc6	Detroit
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
po	po	k7c6	po
192	[number]	k4	192
koncertech	koncert	k1gInPc6	koncert
během	během	k7c2	během
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
vydělaly	vydělat	k5eAaPmAgInP	vydělat
43,6	[number]	k4	43,6
mil	míle	k1gFnPc2	míle
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
účast	účast	k1gFnSc1	účast
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
koncert	koncert	k1gInSc4	koncert
byla	být	k5eAaImAgFnS	být
13	[number]	k4	13
737	[number]	k4	737
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
září	září	k1gNnSc6	září
1998	[number]	k4	1998
společné	společný	k2eAgFnSc6d1	společná
album	album	k1gNnSc4	album
Psycho	psycha	k1gFnSc5	psycha
Circus	Circus	k1gInSc1	Circus
<g/>
.	.	kIx.	.
</s>
<s>
Hráčsky	hráčsky	k6eAd1	hráčsky
se	se	k3xPyFc4	se
Frehley	Frehlea	k1gFnPc1	Frehlea
a	a	k8xC	a
Criss	Criss	k1gInSc1	Criss
na	na	k7c4	na
nahrávaní	nahrávaný	k2eAgMnPc1d1	nahrávaný
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
podíleli	podílet	k5eAaImAgMnP	podílet
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Sólové	sólový	k2eAgFnPc4d1	sólová
kytary	kytara	k1gFnPc4	kytara
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
Bruce	Bruce	k1gMnSc1	Bruce
Kullick	Kullick	k1gMnSc1	Kullick
a	a	k8xC	a
budoucí	budoucí	k2eAgMnSc1d1	budoucí
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Tommy	Tomma	k1gFnSc2	Tomma
Thayer	Thayra	k1gFnPc2	Thayra
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
bicích	bicí	k2eAgFnPc2d1	bicí
sekcí	sekce	k1gFnPc2	sekce
nahrál	nahrát	k5eAaBmAgMnS	nahrát
studiový	studiový	k2eAgMnSc1d1	studiový
hudebník	hudebník	k1gMnSc1	hudebník
Kevin	Kevin	k1gMnSc1	Kevin
Valentine	Valentin	k1gMnSc5	Valentin
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
rozpory	rozpor	k1gInPc4	rozpor
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
album	album	k1gNnSc4	album
v	v	k7c6	v
žebříčích	žebříč	k1gFnPc6	žebříč
třetí	třetí	k4xOgNnSc4	třetí
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
alb	alba	k1gFnPc2	alba
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
skladba	skladba	k1gFnSc1	skladba
alba	album	k1gNnSc2	album
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hardrockovou	hardrockový	k2eAgFnSc4d1	hardrocková
skladbu	skladba	k1gFnSc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
Psycho	psycha	k1gFnSc5	psycha
Circus	Circus	k1gInSc1	Circus
Tour	Tour	k1gInSc1	Tour
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c4	na
Dodger	Dodger	k1gInSc4	Dodger
Stadium	stadium	k1gNnSc4	stadium
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
svátek	svátek	k1gInSc4	svátek
Halloween	Hallowena	k1gFnPc2	Hallowena
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
přes	přes	k7c4	přes
FM	FM	kA	FM
rádia	rádius	k1gInSc2	rádius
v	v	k7c6	v
celých	celá	k1gFnPc6	celá
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1999	[number]	k4	1999
má	mít	k5eAaImIp3nS	mít
skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
svou	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c4	na
Hollywood	Hollywood	k1gInSc4	Hollywood
Walk	Walk	k1gMnSc1	Walk
of	of	k?	of
Fame	Fam	k1gFnSc2	Fam
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
hudebního	hudební	k2eAgInSc2d1	hudební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byla	být	k5eAaImAgFnS	být
premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
název	název	k1gInSc1	název
Detroit	Detroit	k1gInSc1	Detroit
Rock	rock	k1gInSc1	rock
City	City	k1gFnSc2	City
o	o	k7c6	o
čtyřech	čtyři	k4xCgMnPc6	čtyři
teenagerech	teenager	k1gMnPc6	teenager
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
schopní	schopný	k2eAgMnPc1d1	schopný
udělat	udělat	k5eAaPmF	udělat
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kissa	k1gFnPc2	Kissa
v	v	k7c6	v
Detroitu	Detroit	k1gInSc6	Detroit
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
měsíc	měsíc	k1gInSc4	měsíc
skupina	skupina	k1gFnSc1	skupina
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	s	k7c7	s
zápasníkem	zápasník	k1gMnSc7	zápasník
World	World	k1gMnSc1	World
Championship	Championship	k1gMnSc1	Championship
Wrestling	Wrestling	k1gInSc4	Wrestling
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
The	The	k1gMnSc1	The
Kiss	Kissa	k1gFnPc2	Kissa
Demon	Demon	k1gMnSc1	Demon
<g/>
.	.	kIx.	.
</s>
<s>
Tvář	tvář	k1gFnSc4	tvář
měl	mít	k5eAaImAgInS	mít
namaskovanou	namaskovaný	k2eAgFnSc4d1	namaskovaná
jako	jako	k8xC	jako
Gene	gen	k1gInSc5	gen
Simmons	Simmons	k1gInSc1	Simmons
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
uvedení	uvedení	k1gNnSc4	uvedení
během	během	k7c2	během
WCW	WCW	kA	WCW
Thursday	Thursdaa	k1gFnPc1	Thursdaa
Night	Night	k1gMnSc1	Night
Thunder	Thunder	k1gMnSc1	Thunder
skupina	skupina	k1gFnSc1	skupina
naživo	naživo	k1gNnSc1	naživo
zahrála	zahrát	k5eAaPmAgFnS	zahrát
skladbu	skladba	k1gFnSc4	skladba
God	God	k1gFnSc2	God
of	of	k?	of
Thunder	Thunder	k1gMnSc1	Thunder
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zápasnická	zápasnický	k2eAgFnSc1d1	zápasnická
postava	postava	k1gFnSc1	postava
neměla	mít	k5eNaImAgFnS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
životnost	životnost	k1gFnSc4	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
vazby	vazba	k1gFnPc1	vazba
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
<g/>
,	,	kIx,	,
když	když	k8xS	když
WCW	WCW	kA	WCW
už	už	k6eAd1	už
v	v	k7c4	v
září	září	k1gNnSc4	září
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
rozvázalo	rozvázat	k5eAaPmAgNnS	rozvázat
pracovní	pracovní	k2eAgInSc4d1	pracovní
poměr	poměr	k1gInSc4	poměr
se	s	k7c7	s
šéfem	šéf	k1gMnSc7	šéf
Ericem	Erice	k1gMnSc7	Erice
Bishofem	Bishof	k1gMnSc7	Bishof
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2000	[number]	k4	2000
Kiss	Kissa	k1gFnPc2	Kissa
oznámili	oznámit	k5eAaPmAgMnP	oznámit
Turné	turné	k1gNnSc4	turné
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
australské	australský	k2eAgFnSc6d1	australská
a	a	k8xC	a
japonské	japonský	k2eAgFnSc6d1	japonská
části	část	k1gFnSc6	část
turné	turné	k1gNnSc2	turné
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Petera	Petera	k1gMnSc1	Petera
Crisse	Crisse	k1gFnSc2	Crisse
(	(	kIx(	(
<g/>
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
masce	maska	k1gFnSc6	maska
catmana	catman	k1gMnSc4	catman
<g/>
)	)	kIx)	)
předešlý	předešlý	k2eAgMnSc1d1	předešlý
bubeník	bubeník	k1gMnSc1	bubeník
Eric	Eric	k1gFnSc4	Eric
Singer	Singra	k1gFnPc2	Singra
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
ohledně	ohledně	k7c2	ohledně
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
neukončit	ukončit	k5eNaPmF	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
bubeníka	bubeník	k1gMnSc2	bubeník
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Peter	Peter	k1gMnSc1	Peter
Criss	Crissa	k1gFnPc2	Crissa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
opustil	opustit	k5eAaPmAgMnS	opustit
Kiss	Kiss	k1gInSc4	Kiss
Ace	Ace	k1gMnPc2	Ace
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
Tommy	Tomma	k1gFnSc2	Tomma
Thayer	Thayer	k1gInSc1	Thayer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
už	už	k6eAd1	už
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
s	s	k7c7	s
Kiss	Kiss	k1gInSc4	Kiss
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sestavě	sestava	k1gFnSc6	sestava
skupina	skupina	k1gFnSc1	skupina
zrealizovala	zrealizovat	k5eAaPmAgFnS	zrealizovat
svůj	svůj	k3xOyFgInSc4	svůj
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
nahráli	nahrát	k5eAaBmAgMnP	nahrát
koncertní	koncertní	k2eAgNnSc4d1	koncertní
album	album	k1gNnSc4	album
s	s	k7c7	s
melbournským	melbournský	k2eAgInSc7d1	melbournský
symfonickým	symfonický	k2eAgInSc7d1	symfonický
orchestrem	orchestr	k1gInSc7	orchestr
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
ho	on	k3xPp3gMnSc4	on
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Kiss	Kissa	k1gFnPc2	Kissa
Symphony	Symphona	k1gFnSc2	Symphona
<g/>
:	:	kIx,	:
Alive	Aliev	k1gFnSc2	Aliev
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
skupinu	skupina	k1gFnSc4	skupina
znovu	znovu	k6eAd1	znovu
opustil	opustit	k5eAaPmAgMnS	opustit
Peter	Peter	k1gMnSc1	Peter
Criss	Crissa	k1gFnPc2	Crissa
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
opět	opět	k6eAd1	opět
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Eric	Eric	k1gFnSc4	Eric
Singer	Singer	k1gMnSc1	Singer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sestavě	sestava	k1gFnSc6	sestava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vydržela	vydržet	k5eAaPmAgFnS	vydržet
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
objeli	objet	k5eAaPmAgMnP	objet
Kiss	Kiss	k1gInSc4	Kiss
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
planetu	planeta	k1gFnSc4	planeta
s	s	k7c7	s
Rock	rock	k1gInSc1	rock
the	the	k?	the
nation	nation	k1gInSc1	nation
world	world	k1gMnSc1	world
tour	tour	k1gMnSc1	tour
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
turné	turné	k1gNnSc6	turné
skupina	skupina	k1gFnSc1	skupina
natočila	natočit	k5eAaBmAgFnS	natočit
koncerty	koncert	k1gInPc4	koncert
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
a	a	k8xC	a
Virginia	Virginium	k1gNnPc1	Virginium
Beach	Beacha	k1gFnPc2	Beacha
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
je	on	k3xPp3gMnPc4	on
na	na	k7c6	na
DVD	DVD	kA	DVD
Rock	rock	k1gInSc1	rock
the	the	k?	the
Nation	Nation	k1gInSc1	Nation
Live	Live	k1gInSc1	Live
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
Kiss	Kissa	k1gFnPc2	Kissa
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
turné	turné	k1gNnSc2	turné
Alive	Aliev	k1gFnSc2	Aliev
35	[number]	k4	35
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
výročí	výročí	k1gNnSc2	výročí
třiceti	třicet	k4xCc7	třicet
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
turné	turné	k1gNnSc7	turné
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
Kiss	Kissa	k1gFnPc2	Kissa
vydali	vydat	k5eAaPmAgMnP	vydat
po	po	k7c6	po
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Sonic	Sonic	k1gMnSc1	Sonic
Boom	boom	k1gInSc1	boom
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
žebříčků	žebříček	k1gInPc2	žebříček
popularity	popularita	k1gFnSc2	popularita
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
turné	turné	k1gNnSc6	turné
Alive	Aliev	k1gFnSc2	Aliev
35	[number]	k4	35
po	po	k7c6	po
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
navštívila	navštívit	k5eAaPmAgFnS	navštívit
skupina	skupina	k1gFnSc1	skupina
Evropu	Evropa	k1gFnSc4	Evropa
při	při	k7c6	při
turné	turné	k1gNnSc6	turné
Sonic	Sonice	k1gFnPc2	Sonice
Boom	boom	k1gInSc1	boom
over	over	k1gMnSc1	over
Europe	Europ	k1gInSc5	Europ
<g/>
:	:	kIx,	:
From	From	k1gInSc1	From
the	the	k?	the
begining	begining	k1gInSc1	begining
to	ten	k3xDgNnSc1	ten
the	the	k?	the
boom	boom	k1gInSc1	boom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
Monster	monstrum	k1gNnPc2	monstrum
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
ukázali	ukázat	k5eAaPmAgMnP	ukázat
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
areně	areeň	k1gFnSc2	areeň
<g/>
.	.	kIx.	.
</s>
<s>
Show	show	k1gFnSc1	show
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
ale	ale	k8xC	ale
i	i	k9	i
starými	starý	k2eAgInPc7d1	starý
hity	hit	k1gInPc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
začal	začít	k5eAaPmAgInS	začít
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
Psycho	psycha	k1gFnSc5	psycha
Circus	Circus	k1gInSc1	Circus
<g/>
"	"	kIx"	"
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
i	i	k9	i
klasické	klasický	k2eAgInPc4d1	klasický
prvky	prvek	k1gInPc4	prvek
KISS	KISS	kA	KISS
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
například	například	k6eAd1	například
plivání	plivání	k1gNnSc1	plivání
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
střílení	střílení	k1gNnSc2	střílení
z	z	k7c2	z
kytar	kytara	k1gFnPc2	kytara
a	a	k8xC	a
z	z	k7c2	z
bazuky	bazuka	k1gFnSc2	bazuka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
zazněly	zaznít	k5eAaPmAgFnP	zaznít
skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Hell	Hell	k1gMnSc1	Hell
Or	Or	k1gMnSc1	Or
Hallelujah	Hallelujah	k1gMnSc1	Hallelujah
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Outta	Outta	k1gFnSc1	Outta
This	This	k1gInSc1	This
World	World	k1gInSc1	World
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zazněly	zaznít	k5eAaPmAgInP	zaznít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
klasické	klasický	k2eAgInPc4d1	klasický
hity	hit	k1gInPc4	hit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
I	i	k9	i
Was	Was	k1gFnSc4	Was
Made	Mad	k1gFnSc2	Mad
For	forum	k1gNnPc2	forum
Lovin	Lovin	k1gMnSc1	Lovin
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Detroit	Detroit	k1gInSc1	Detroit
Rock	rock	k1gInSc1	rock
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Gun	Gun	k1gFnSc5	Gun
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Black	Black	k1gMnSc1	Black
Diamond	Diamond	k1gMnSc1	Diamond
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Shout	Shout	k1gInSc1	Shout
it	it	k?	it
out	out	k?	out
Loud	louda	k1gFnPc2	louda
<g/>
"	"	kIx"	"
atd.	atd.	kA	atd.
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
byli	být	k5eAaImAgMnP	být
Kiss	Kiss	k1gInSc4	Kiss
uvedeni	uveden	k2eAgMnPc1d1	uveden
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
And	Anda	k1gFnPc2	Anda
Rollový	Rollový	k2eAgMnSc1d1	Rollový
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
opět	opět	k6eAd1	opět
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
předvedla	předvést	k5eAaPmAgFnS	předvést
skvělou	skvělý	k2eAgFnSc4d1	skvělá
show	show	k1gFnSc4	show
k	k	k7c3	k
turné	turné	k1gNnSc3	turné
"	"	kIx"	"
<g/>
40	[number]	k4	40
Anniversary	Anniversara	k1gFnSc2	Anniversara
tour	toura	k1gFnPc2	toura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
že	že	k8xS	že
vyjede	vyjet	k5eAaPmIp3nS	vyjet
na	na	k7c4	na
letní	letní	k2eAgNnSc4d1	letní
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
USA	USA	kA	USA
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Freedom	Freedom	k1gInSc1	Freedom
To	to	k9	to
Rock	rock	k1gInSc1	rock
Tour	Toura	k1gFnPc2	Toura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
kapela	kapela	k1gFnSc1	kapela
má	mít	k5eAaImIp3nS	mít
25.5	[number]	k4	25.5
<g/>
.16	.16	k4	.16
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
uvést	uvést	k5eAaPmF	uvést
koncert	koncert	k1gInSc4	koncert
a	a	k8xC	a
dokument	dokument	k1gInSc4	dokument
o	o	k7c6	o
kapele	kapela	k1gFnSc6	kapela
Kiss	Kissa	k1gFnPc2	Kissa
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
.	.	kIx.	.
</s>
<s>
Gene	gen	k1gInSc5	gen
Simmons	Simmons	k1gInSc1	Simmons
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
-	-	kIx~	-
maska	maska	k1gFnSc1	maska
démona	démon	k1gMnSc2	démon
Paul	Paul	k1gMnSc1	Paul
Stanley	Stanlea	k1gMnSc2	Stanlea
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
-	-	kIx~	-
maska	maska	k1gFnSc1	maska
hvězdného	hvězdný	k2eAgNnSc2d1	Hvězdné
dítěte	dítě	k1gNnSc2	dítě
Ace	Ace	k1gMnSc2	Ace
Frehley	Frehlea	k1gMnSc2	Frehlea
-	-	kIx~	-
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
-	-	kIx~	-
maska	maska	k1gFnSc1	maska
Space	Space	k1gFnSc2	Space
Ace	Ace	k1gFnSc1	Ace
Peter	Peter	k1gMnSc1	Peter
Criss	Criss	k1gInSc1	Criss
-	-	kIx~	-
bicí	bicí	k2eAgInSc1d1	bicí
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
-	-	kIx~	-
maska	maska	k1gFnSc1	maska
kočičího	kočičí	k2eAgMnSc2d1	kočičí
muže	muž	k1gMnSc2	muž
Gene	gen	k1gInSc5	gen
Simmons	Simmons	k1gInSc1	Simmons
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Paul	Paul	k1gMnSc1	Paul
Stanley	Stanlea	k1gMnSc2	Stanlea
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Eric	Eric	k1gFnSc1	Eric
Singer	Singer	k1gMnSc1	Singer
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
od	od	k7c2	od
r.	r.	kA	r.
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Tommy	Tomma	k1gFnSc2	Tomma
Thayer	Thayer	k1gInSc1	Thayer
-	-	kIx~	-
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Peter	Peter	k1gMnSc1	Peter
Criss	Crissa	k1gFnPc2	Crissa
-	-	kIx~	-
bicí	bicí	k2eAgFnSc2d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Ace	Ace	k1gFnSc2	Ace
Frehley	Frehlea	k1gFnSc2	Frehlea
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Eric	Eric	k1gInSc1	Eric
Carr	Carr	k1gInSc1	Carr
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
/	/	kIx~	/
maska	maska	k1gFnSc1	maska
Lišáka	lišák	k1gMnSc2	lišák
<g/>
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
)	)	kIx)	)
Vinnie	Vinnie	k1gFnSc1	Vinnie
Vincent	Vincent	k1gMnSc1	Vincent
-	-	kIx~	-
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
/	/	kIx~	/
maska	maska	k1gFnSc1	maska
egyptského	egyptský	k2eAgMnSc2d1	egyptský
válečníka	válečník	k1gMnSc2	válečník
Ankh-Warrior	Ankh-Warrior	k1gMnSc1	Ankh-Warrior
Mark	Mark	k1gMnSc1	Mark
St.	st.	kA	st.
John	John	k1gMnSc1	John
-	-	kIx~	-
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
)	)	kIx)	)
Bruce	Bruce	k1gFnSc1	Bruce
Kulick	Kulick	k1gMnSc1	Kulick
-	-	kIx~	-
sólová	sólový	k2eAgFnSc1d1	sólová
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Videografie	Videografie	k1gFnSc2	Videografie
Kiss	Kissa	k1gFnPc2	Kissa
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
KISS	KISS	kA	KISS
(	(	kIx(	(
<g/>
band	band	k1gInSc1	band
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Kiss	Kiss	k1gInSc1	Kiss
Peter	Peter	k1gMnSc1	Peter
Criss	Crissa	k1gFnPc2	Crissa
Bruce	Bruce	k1gMnSc1	Bruce
Kulick	Kulick	k1gMnSc1	Kulick
Gene	gen	k1gInSc5	gen
Simmons	Simmons	k1gInSc4	Simmons
Paul	Paul	k1gMnSc1	Paul
Stanley	Stanlea	k1gFnSc2	Stanlea
Eric	Eric	k1gInSc1	Eric
Singer	Singer	k1gInSc1	Singer
Tommy	Tomma	k1gFnSc2	Tomma
Thayer	Thayra	k1gFnPc2	Thayra
</s>
