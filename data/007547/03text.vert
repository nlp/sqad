<s>
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc4	Brno
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
firma	firma	k1gFnSc1	firma
a	a	k8xC	a
výrobce	výrobce	k1gMnSc1	výrobce
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zbraní	zbraň	k1gFnPc2	zbraň
však	však	k9	však
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
také	také	k9	také
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
jako	jako	k8xC	jako
státní	státní	k2eAgInSc1d1	státní
podnik	podnik	k1gInSc1	podnik
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Československé	československý	k2eAgInPc4d1	československý
závody	závod	k1gInPc4	závod
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
opravovala	opravovat	k5eAaImAgFnS	opravovat
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
pušky	puška	k1gFnPc4	puška
<g/>
,	,	kIx,	,
telefonní	telefonní	k2eAgNnSc4d1	telefonní
a	a	k8xC	a
železniční	železniční	k2eAgNnSc4d1	železniční
zařízení	zařízení	k1gNnSc4	zařízení
a	a	k8xC	a
nářadí	nářadí	k1gNnSc4	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
montovala	montovat	k5eAaImAgFnS	montovat
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
rakouské	rakouský	k2eAgFnSc2d1	rakouská
pušky	puška	k1gFnSc2	puška
Mauser	Mausra	k1gFnPc2	Mausra
a	a	k8xC	a
Mannlicher	Mannlichra	k1gFnPc2	Mannlichra
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
svoje	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
pušek	puška	k1gFnPc2	puška
a	a	k8xC	a
kulometů	kulomet	k1gInPc2	kulomet
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
i	i	k9	i
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
motory	motor	k1gInPc1	motor
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
také	také	k9	také
licenční	licenční	k2eAgInPc1d1	licenční
psací	psací	k2eAgInPc1d1	psací
stroje	stroj	k1gInPc1	stroj
Remington	remington	k1gInSc1	remington
a	a	k8xC	a
traktory	traktor	k1gInPc1	traktor
Škoda	škoda	k6eAd1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
zbraně	zbraň	k1gFnPc4	zbraň
pro	pro	k7c4	pro
Wehrmacht	wehrmacht	k1gFnPc4	wehrmacht
a	a	k8xC	a
Waffen-SS	Waffen-SS	k1gFnPc4	Waffen-SS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
Brna	Brno	k1gNnSc2	Brno
továrna	továrna	k1gFnSc1	továrna
vážně	vážně	k6eAd1	vážně
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
znovu	znovu	k6eAd1	znovu
fungovala	fungovat	k5eAaImAgFnS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
motory	motor	k1gInPc4	motor
<g/>
,	,	kIx,	,
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
traktory	traktor	k1gInPc4	traktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
prototyp	prototyp	k1gInSc1	prototyp
traktoru	traktor	k1gInSc2	traktor
Zetor	zetor	k1gInSc1	zetor
Z-	Z-	k1gFnSc1	Z-
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
pojmenování	pojmenování	k1gNnSc1	pojmenování
Zetor	zetor	k1gInSc1	zetor
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
udrželo	udržet	k5eAaPmAgNnS	udržet
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
Zet	zet	k5eAaImF	zet
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vyhláskované	vyhláskovaný	k2eAgNnSc4d1	vyhláskovaný
první	první	k4xOgNnSc4	první
písmeno	písmeno	k1gNnSc4	písmeno
zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
or	or	k?	or
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
slova	slovo	k1gNnSc2	slovo
traktor	traktor	k1gInSc1	traktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
přeorientovává	přeorientovávat	k5eAaImIp3nS	přeorientovávat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
komunikační	komunikační	k2eAgFnPc4d1	komunikační
a	a	k8xC	a
výpočetní	výpočetní	k2eAgFnPc4d1	výpočetní
technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
opravy	oprava	k1gFnSc2	oprava
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začíná	začínat	k5eAaImIp3nS	začínat
klesat	klesat	k5eAaImF	klesat
počet	počet	k1gInSc4	počet
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
začala	začít	k5eAaPmAgFnS	začít
firma	firma	k1gFnSc1	firma
krachovat	krachovat	k5eAaBmF	krachovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
zbraní	zbraň	k1gFnPc2	zbraň
ukončena	ukončen	k2eAgFnSc1d1	ukončena
a	a	k8xC	a
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
byli	být	k5eAaImAgMnP	být
propuštěni	propuštěn	k2eAgMnPc1d1	propuštěn
poslední	poslední	k2eAgMnPc1d1	poslední
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
září	září	k1gNnSc2	září
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
BRNO	Brno	k1gNnSc1	Brno
RIFLES	RIFLES	kA	RIFLES
na	na	k7c6	na
veřejné	veřejný	k2eAgFnSc6d1	veřejná
dražbě	dražba	k1gFnSc6	dražba
vydražila	vydražit	k5eAaPmAgNnP	vydražit
obráběcí	obráběcí	k2eAgNnPc1d1	obráběcí
centra	centrum	k1gNnPc1	centrum
a	a	k8xC	a
část	část	k1gFnSc1	část
dalšího	další	k2eAgNnSc2d1	další
strojového	strojový	k2eAgNnSc2d1	strojové
vybavení	vybavení	k1gNnSc2	vybavení
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dražitelé	dražitel	k1gMnPc1	dražitel
nemínili	mínit	k5eNaImAgMnP	mínit
zbytečně	zbytečně	k6eAd1	zbytečně
mrhat	mrhat	k5eAaImF	mrhat
financemi	finance	k1gFnPc7	finance
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
neměli	mít	k5eNaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
stroje	stroj	k1gInPc4	stroj
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
hlavní	hlavní	k2eAgFnSc4d1	hlavní
a	a	k8xC	a
pažeb	pažba	k1gFnPc2	pažba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
vlastní	vlastní	k2eAgInPc1d1	vlastní
CZUB	CZUB	kA	CZUB
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
hlavně	hlavně	k6eAd1	hlavně
a	a	k8xC	a
pažby	pažba	k1gFnPc4	pažba
nakupovat	nakupovat	k5eAaBmF	nakupovat
z	z	k7c2	z
Uherského	uherský	k2eAgInSc2d1	uherský
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
úsilí	úsilí	k1gNnSc1	úsilí
pak	pak	k6eAd1	pak
směřovalo	směřovat	k5eAaImAgNnS	směřovat
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
nákupu	nákup	k1gInSc2	nákup
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
práv	právo	k1gNnPc2	právo
nejperspektivnějších	perspektivní	k2eAgFnPc2d3	nejperspektivnější
zbraní	zbraň	k1gFnPc2	zbraň
portfolia	portfolio	k1gNnSc2	portfolio
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
kozlicím	kozlice	k1gFnPc3	kozlice
BO	BO	k?	BO
řady	řada	k1gFnSc2	řada
800	[number]	k4	800
a	a	k8xC	a
kulovnicím	kulovnice	k1gFnPc3	kulovnice
Brno	Brno	k1gNnSc1	Brno
Effect	Effect	k1gInSc1	Effect
<g/>
.	.	kIx.	.
</s>
<s>
Snadné	snadný	k2eAgNnSc1d1	snadné
rovněž	rovněž	k9	rovněž
nebylo	být	k5eNaImAgNnS	být
najít	najít	k5eAaPmF	najít
původní	původní	k2eAgMnPc4d1	původní
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
kterých	který	k3yIgInPc6	který
společnost	společnost	k1gFnSc1	společnost
BRNO	Brno	k1gNnSc1	Brno
RIFLES	RIFLES	kA	RIFLES
přijala	přijmout	k5eAaPmAgFnS	přijmout
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
dvacet	dvacet	k4xCc4	dvacet
devět	devět	k4xCc4	devět
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgFnSc1	všechen
námaha	námaha	k1gFnSc1	námaha
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
šťastnému	šťastný	k2eAgInSc3d1	šťastný
konci	konec	k1gInSc3	konec
a	a	k8xC	a
už	už	k6eAd1	už
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
vyexpedovala	vyexpedovat	k5eAaPmAgFnS	vyexpedovat
BRNO	Brno	k1gNnSc4	Brno
RIFLES	RIFLES	kA	RIFLES
první	první	k4xOgFnSc1	první
kozlice	kozlice	k1gFnSc1	kozlice
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
podle	podle	k7c2	podle
smluvních	smluvní	k2eAgMnPc2d1	smluvní
vztahů	vztah	k1gInPc2	vztah
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Česká	český	k2eAgFnSc1d1	Česká
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Uherský	uherský	k2eAgInSc1d1	uherský
Brod	Brod	k1gInSc4	Brod
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
pro	pro	k7c4	pro
BRNO	Brno	k1gNnSc4	Brno
RIFLES	RIFLES	kA	RIFLES
vše	všechen	k3xTgNnSc1	všechen
související	související	k2eAgNnSc1d1	související
s	s	k7c7	s
obchodem	obchod	k1gInSc7	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
22,5	[number]	k4	22,5
ha	ha	kA	ha
byl	být	k5eAaImAgInS	být
koncem	koncem	k7c2	koncem
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
vydražen	vydražit	k5eAaPmNgMnS	vydražit
za	za	k7c4	za
částku	částka	k1gFnSc4	částka
707	[number]	k4	707
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
jej	on	k3xPp3gNnSc4	on
slovenská	slovenský	k2eAgFnSc1d1	slovenská
holdingová	holdingový	k2eAgFnSc1d1	holdingová
společnost	společnost	k1gFnSc1	společnost
J	J	kA	J
<g/>
&	&	k?	&
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
zbraní	zbraň	k1gFnSc7	zbraň
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
areál	areál	k1gInSc1	areál
využívá	využívat	k5eAaImIp3nS	využívat
několik	několik	k4yIc4	několik
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
estetice	estetika	k1gFnSc3	estetika
areálu	areál	k1gInSc2	areál
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
k	k	k7c3	k
fotografické	fotografický	k2eAgFnSc3d1	fotografická
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
také	také	k9	také
hudební	hudební	k2eAgFnPc1d1	hudební
zkušebny	zkušebna	k1gFnPc1	zkušebna
<g/>
.	.	kIx.	.
</s>
<s>
Nájemní	nájemní	k2eAgFnSc1d1	nájemní
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
Brněnskou	brněnský	k2eAgFnSc7d1	brněnská
zbrojovkou	zbrojovka	k1gFnSc7	zbrojovka
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
majitelem	majitel	k1gMnSc7	majitel
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokračováno	pokračován	k2eAgNnSc1d1	pokračováno
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
v	v	k7c6	v
pronajatých	pronajatý	k2eAgFnPc6d1	pronajatá
prostorách	prostora	k1gFnPc6	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Puška	puška	k1gFnSc1	puška
vz.	vz.	k?	vz.
24	[number]	k4	24
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
Lehký	lehký	k2eAgInSc1d1	lehký
kulomet	kulomet	k1gInSc1	kulomet
vz.	vz.	k?	vz.
26	[number]	k4	26
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
Těžký	těžký	k2eAgInSc1d1	těžký
kulomet	kulomet	k1gInSc1	kulomet
vz.	vz.	k?	vz.
37	[number]	k4	37
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
vývoj	vývoj	k1gInSc1	vývoj
30	[number]	k4	30
mm	mm	kA	mm
kanónu	kanón	k1gInSc2	kanón
s	s	k7c7	s
německým	německý	k2eAgNnSc7d1	německé
označením	označení	k1gNnSc7	označení
Br	br	k0	br
303	[number]	k4	303
(	(	kIx(	(
<g/>
vývojové	vývojový	k2eAgInPc1d1	vývojový
názvy	název	k1gInPc1	název
<g/>
:	:	kIx,	:
ZK	ZK	kA	ZK
414	[number]	k4	414
<g/>
,	,	kIx,	,
ZB	ZB	kA	ZB
303	[number]	k4	303
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
F.	F.	kA	F.
Koutský	Koutský	k1gMnSc1	Koutský
<g/>
)	)	kIx)	)
1949	[number]	k4	1949
-	-	kIx~	-
květen	květen	k1gInSc4	květen
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
1950	[number]	k4	1950
-	-	kIx~	-
1954	[number]	k4	1954
<g/>
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
30	[number]	k4	30
mm	mm	kA	mm
kanón	kanón	k1gInSc4	kanón
ZK	ZK	kA	ZK
453	[number]	k4	453
30	[number]	k4	30
mm	mm	kA	mm
PLdvK	PLdvK	k1gFnPc2	PLdvK
vz.	vz.	k?	vz.
<g/>
53	[number]	k4	53
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
státní	státní	k2eAgFnSc1d1	státní
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
výrobního	výrobní	k2eAgInSc2d1	výrobní
programu	program	k1gInSc2	program
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1922	[number]	k4	1922
konstruktér	konstruktér	k1gMnSc1	konstruktér
Ing.	ing.	kA	ing.
Břetislav	Břetislav	k1gMnSc1	Břetislav
Novotný	Novotný	k1gMnSc1	Novotný
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
vedení	vedení	k1gNnSc1	vedení
zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
svůj	svůj	k3xOyFgInSc4	svůj
vůz	vůz	k1gInSc4	vůz
OMEGA	omega	k1gNnSc2	omega
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
neortodoxním	ortodoxní	k2eNgInSc7d1	neortodoxní
třecím	třecí	k2eAgInSc7d1	třecí
převodem	převod	k1gInSc7	převod
místo	místo	k7c2	místo
klasické	klasický	k2eAgFnSc2d1	klasická
převodovky	převodovka	k1gFnSc2	převodovka
a	a	k8xC	a
pohonem	pohon	k1gInSc7	pohon
dvoutaktním	dvoutaktní	k2eAgInSc7d1	dvoutaktní
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
vůz	vůz	k1gInSc1	vůz
Disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
sériové	sériový	k2eAgFnSc2d1	sériová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Novotného	Novotného	k2eAgFnSc1d1	Novotného
konstrukce	konstrukce	k1gFnSc1	konstrukce
se	se	k3xPyFc4	se
však	však	k9	však
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
nevyzrálá	vyzrálý	k2eNgFnSc1d1	nevyzrálá
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Novotný	Novotný	k1gMnSc1	Novotný
byl	být	k5eAaImAgMnS	být
donucen	donutit	k5eAaPmNgMnS	donutit
automobilku	automobilka	k1gFnSc4	automobilka
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
následné	následný	k2eAgFnPc4d1	následná
úpravy	úprava	k1gFnPc4	úprava
byly	být	k5eAaImAgFnP	být
prodeje	prodej	k1gInSc2	prodej
fiaskem	fiasko	k1gNnSc7	fiasko
<g/>
,	,	kIx,	,
nespolehlivé	spolehlivý	k2eNgInPc1d1	nespolehlivý
vozy	vůz	k1gInPc1	vůz
musel	muset	k5eAaImAgMnS	muset
výrobce	výrobce	k1gMnSc1	výrobce
od	od	k7c2	od
zákazníků	zákazník	k1gMnPc2	zákazník
odkoupit	odkoupit	k5eAaPmF	odkoupit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
vozu	vůz	k1gInSc2	vůz
Disk	disk	k1gInSc1	disk
vedení	vedení	k1gNnSc2	vedení
zbrojovky	zbrojovka	k1gFnSc2	zbrojovka
zvažovalo	zvažovat	k5eAaImAgNnS	zvažovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
pokračovat	pokračovat	k5eAaImF	pokračovat
s	s	k7c7	s
pokusy	pokus	k1gInPc7	pokus
o	o	k7c4	o
výrobu	výroba	k1gFnSc4	výroba
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
vývoj	vývoj	k1gInSc1	vývoj
nového	nový	k2eAgInSc2d1	nový
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktér	konstruktér	k1gMnSc1	konstruktér
ing.	ing.	kA	ing.
František	František	k1gMnSc1	František
Mackerle	Mackerle	k1gFnSc2	Mackerle
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1925	[number]	k4	1925
připravil	připravit	k5eAaPmAgInS	připravit
návrh	návrh	k1gInSc4	návrh
poměrně	poměrně	k6eAd1	poměrně
konvenčního	konvenční	k2eAgInSc2d1	konvenční
vozu	vůz	k1gInSc2	vůz
Z	z	k7c2	z
18	[number]	k4	18
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
opět	opět	k6eAd1	opět
poháněl	pohánět	k5eAaImAgInS	pohánět
dvoutaktní	dvoutaktní	k2eAgInSc1d1	dvoutaktní
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
prvkem	prvek	k1gInSc7	prvek
všech	všecek	k3xTgInPc2	všecek
následujících	následující	k2eAgInPc2d1	následující
vozů	vůz	k1gInPc2	vůz
značky	značka	k1gFnSc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Důkladně	důkladně	k6eAd1	důkladně
odzkoušený	odzkoušený	k2eAgInSc1d1	odzkoušený
automobil	automobil	k1gInSc1	automobil
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
prodejně	prodejně	k6eAd1	prodejně
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přes	přes	k7c4	přes
2500	[number]	k4	2500
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
větší	veliký	k2eAgMnSc1d2	veliký
a	a	k8xC	a
dražší	drahý	k2eAgInSc1d2	dražší
typ	typ	k1gInSc1	typ
Z	z	k7c2	z
9	[number]	k4	9
již	již	k9	již
takový	takový	k3xDgInSc4	takový
úspěch	úspěch	k1gInSc4	úspěch
neměl	mít	k5eNaImAgMnS	mít
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
bylo	být	k5eAaImAgNnS	být
zhotoveno	zhotovit	k5eAaPmNgNnS	zhotovit
pouze	pouze	k6eAd1	pouze
850	[number]	k4	850
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
byla	být	k5eAaImAgFnS	být
zvažována	zvažován	k2eAgFnSc1d1	zvažována
perspektiva	perspektiva	k1gFnSc1	perspektiva
budoucí	budoucí	k2eAgFnSc2d1	budoucí
výroby	výroba	k1gFnSc2	výroba
automobilů	automobil	k1gInPc2	automobil
ve	v	k7c6	v
Zbrojovce	zbrojovka	k1gFnSc6	zbrojovka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvořila	tvořit	k5eAaImAgFnS	tvořit
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
6	[number]	k4	6
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Východiskem	východisko	k1gNnSc7	východisko
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
automobil	automobil	k1gInSc4	automobil
moderní	moderní	k2eAgFnSc2d1	moderní
koncepce	koncepce	k1gFnSc2	koncepce
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
předních	přední	k2eAgNnPc2d1	přední
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c4	na
licenci	licence	k1gFnSc4	licence
DKW	DKW	kA	DKW
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
nákup	nákup	k1gInSc1	nákup
se	se	k3xPyFc4	se
však	však	k9	však
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
nového	nový	k2eAgMnSc2d1	nový
šéfa	šéf	k1gMnSc2	šéf
výroby	výroba	k1gFnSc2	výroba
Ing.	ing.	kA	ing.
Antonína	Antonín	k1gMnSc2	Antonín
Voženílka	Voženílek	k1gMnSc2	Voženílek
a	a	k8xC	a
nového	nový	k2eAgMnSc2d1	nový
šéfkonstruktéra	šéfkonstruktér	k1gMnSc2	šéfkonstruktér
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
Odstrčila	Odstrčil	k1gMnSc2	Odstrčil
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
nový	nový	k2eAgInSc1d1	nový
vůz	vůz	k1gInSc1	vůz
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
předních	přední	k2eAgNnPc2d1	přední
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
patentových	patentový	k2eAgNnPc2d1	patentové
řešení	řešení	k1gNnPc2	řešení
firmy	firma	k1gFnSc2	firma
DKW	DKW	kA	DKW
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
označení	označení	k1gNnSc4	označení
Z	z	k7c2	z
4	[number]	k4	4
(	(	kIx(	(
<g/>
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
produkční	produkční	k2eAgInSc1d1	produkční
typ	typ	k1gInSc1	typ
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
úspěšným	úspěšný	k2eAgInPc3d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
rekordních	rekordní	k2eAgInPc2d1	rekordní
953	[number]	k4	953
vozů	vůz	k1gInPc2	vůz
značky	značka	k1gFnSc2	značka
Z	z	k7c2	z
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
automobilka	automobilka	k1gFnSc1	automobilka
poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
za	za	k7c4	za
tradiční	tradiční	k2eAgFnSc4d1	tradiční
silnou	silný	k2eAgFnSc4d1	silná
trojici	trojice	k1gFnSc4	trojice
Tatra	Tatra	k1gFnSc1	Tatra
<g/>
,	,	kIx,	,
Praga	Praga	k1gFnSc1	Praga
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
následoval	následovat	k5eAaImAgInS	následovat
lidový	lidový	k2eAgInSc1d1	lidový
vůz	vůz	k1gInSc1	vůz
Z	z	k7c2	z
6	[number]	k4	6
Hurvínek	Hurvínek	k1gMnSc1	Hurvínek
a	a	k8xC	a
střední	střední	k2eAgMnPc1d1	střední
Z	z	k7c2	z
5	[number]	k4	5
Expres	expres	k2eAgFnPc2d1	expres
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
zhoršující	zhoršující	k2eAgFnSc3d1	zhoršující
se	se	k3xPyFc4	se
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
situaci	situace	k1gFnSc3	situace
však	však	k9	však
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
automobilů	automobil	k1gInPc2	automobil
ve	v	k7c6	v
Zbrojovce	zbrojovka	k1gFnSc6	zbrojovka
Brno	Brno	k1gNnSc1	Brno
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
MNO	MNO	kA	MNO
ukončena	ukončen	k2eAgFnSc1d1	ukončena
k	k	k7c3	k
15	[number]	k4	15
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
vozy	vůz	k1gInPc1	vůz
Z	z	k7c2	z
5	[number]	k4	5
a	a	k8xC	a
Z	z	k7c2	z
6	[number]	k4	6
byly	být	k5eAaImAgFnP	být
smontovány	smontovat	k5eAaPmNgFnP	smontovat
ze	z	k7c2	z
zbylých	zbylý	k2eAgInPc2d1	zbylý
dílů	díl	k1gInPc2	díl
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ještě	ještě	k6eAd1	ještě
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
prototypy	prototyp	k1gInPc1	prototyp
vojenských	vojenský	k2eAgMnPc2d1	vojenský
terénních	terénní	k2eAgMnPc2d1	terénní
vozů	vůz	k1gInPc2	vůz
T2	T2	k1gFnSc2	T2
a	a	k8xC	a
ZV	ZV	kA	ZV
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
nedostaly	dostat	k5eNaPmAgInP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
SKŘIVAN	Skřivan	k1gMnSc1	Skřivan
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
</s>
<s>
Export	export	k1gInSc1	export
of	of	k?	of
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
Czechoslovak	Czechoslovak	k1gInSc1	Czechoslovak
Arms	Arms	k1gInSc1	Arms
Factory	Factor	k1gInPc1	Factor
of	of	k?	of
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
to	ten	k3xDgNnSc4	ten
China	China	k1gFnSc1	China
in	in	k?	in
the	the	k?	the
Interwar	Interwar	k1gInSc1	Interwar
Period	perioda	k1gFnPc2	perioda
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
,	,	kIx,	,
s.	s.	k?	s.
111	[number]	k4	111
<g/>
-	-	kIx~	-
<g/>
134	[number]	k4	134
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
118	[number]	k4	118
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
název	název	k1gInSc1	název
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
ČZ	ČZ	kA	ČZ
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zetor	zetor	k1gInSc1	zetor
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgInPc2d1	český
automobilů	automobil	k1gInPc2	automobil
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc4	Brno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
