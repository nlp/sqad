<s>
V	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
3D	[number]	k4	3D
grafice	grafika	k1gFnSc6	grafika
je	on	k3xPp3gNnSc4	on
anizotropní	anizotropní	k2eAgNnSc4d1	anizotropní
filtrování	filtrování	k1gNnSc4	filtrování
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
kvality	kvalita	k1gFnSc2	kvalita
textur	textura	k1gFnPc2	textura
s	s	k7c7	s
kosými	kosý	k2eAgInPc7d1	kosý
zornými	zorný	k2eAgInPc7d1	zorný
úhly	úhel	k1gInPc7	úhel
a	a	k8xC	a
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
úhel	úhel	k1gInSc4	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
projekce	projekce	k1gFnSc1	projekce
textury	textura	k1gFnSc2	textura
působí	působit	k5eAaImIp3nS	působit
nekolmým	kolmý	k2eNgInSc7d1	kolmý
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
