<s>
V	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
3D	[number]	k4	3D
grafice	grafika	k1gFnSc6	grafika
je	on	k3xPp3gNnSc4	on
anizotropní	anizotropní	k2eAgNnSc4d1	anizotropní
filtrování	filtrování	k1gNnSc4	filtrování
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
kvality	kvalita	k1gFnSc2	kvalita
textur	textura	k1gFnPc2	textura
s	s	k7c7	s
kosými	kosý	k2eAgInPc7d1	kosý
zornými	zorný	k2eAgInPc7d1	zorný
úhly	úhel	k1gInPc7	úhel
a	a	k8xC	a
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
úhel	úhel	k1gInSc4	úhel
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
projekce	projekce	k1gFnSc1	projekce
textury	textura	k1gFnSc2	textura
působí	působit	k5eAaImIp3nS	působit
nekolmým	kolmý	k2eNgInSc7d1	kolmý
dojmem	dojem	k1gInSc7	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
bilineární	bilineární	k2eAgNnSc4d1	bilineární
a	a	k8xC	a
trilineární	trilineární	k2eAgNnSc4d1	trilineární
filtrování	filtrování	k1gNnSc4	filtrování
<g/>
,	,	kIx,	,
i	i	k9	i
anizotropní	anizotropní	k2eAgNnSc4d1	anizotropní
filtrování	filtrování	k1gNnSc4	filtrování
eliminuje	eliminovat	k5eAaBmIp3nS	eliminovat
tzv.	tzv.	kA	tzv.
aliasing	aliasing	k1gInSc4	aliasing
efekty	efekt	k1gInPc7	efekt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
nim	on	k3xPp3gMnPc3	on
vylepšen	vylepšen	k2eAgMnSc1d1	vylepšen
díky	díky	k7c3	díky
redukci	redukce	k1gFnSc3	redukce
rozostření	rozostření	k1gNnSc2	rozostření
a	a	k8xC	a
zachování	zachování	k1gNnSc2	zachování
vysokého	vysoký	k2eAgInSc2d1	vysoký
detailu	detail	k1gInSc2	detail
i	i	k9	i
v	v	k7c6	v
extrémních	extrémní	k2eAgInPc6d1	extrémní
zorných	zorný	k2eAgInPc6d1	zorný
úhlech	úhel	k1gInPc6	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Anizotropická	Anizotropický	k2eAgFnSc1d1	Anizotropický
komprese	komprese	k1gFnSc1	komprese
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
intenzívní	intenzívní	k2eAgNnSc1d1	intenzívní
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
standardní	standardní	k2eAgFnSc1d1	standardní
funkce	funkce	k1gFnSc1	funkce
současných	současný	k2eAgFnPc2d1	současná
grafických	grafický	k2eAgFnPc2d1	grafická
karet	kareta	k1gFnPc2	kareta
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Anizotropní	Anizotropní	k2eAgNnSc1d1	Anizotropní
filtrování	filtrování	k1gNnSc1	filtrování
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
běžné	běžný	k2eAgNnSc1d1	běžné
v	v	k7c6	v
moderních	moderní	k2eAgNnPc6d1	moderní
grafických	grafický	k2eAgNnPc6d1	grafické
zařízeních	zařízení	k1gNnPc6	zařízení
včetně	včetně	k7c2	včetně
video	video	k1gNnSc4	video
softwaru	software	k1gInSc2	software
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uživatelem	uživatel	k1gMnSc7	uživatel
aktivováno	aktivovat	k5eAaBmNgNnS	aktivovat
skrze	skrze	k?	skrze
nastavení	nastavení	k1gNnSc1	nastavení
grafické	grafický	k2eAgInPc1d1	grafický
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
ovladače	ovladač	k1gInPc1	ovladač
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
videohrách	videohra	k1gFnPc6	videohra
přes	přes	k7c4	přes
programové	programový	k2eAgNnSc4d1	programové
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtenář	čtenář	k1gMnSc1	čtenář
je	být	k5eAaImIp3nS	být
obeznámen	obeznámen	k2eAgInSc1d1	obeznámen
s	s	k7c7	s
MIP	MIP	kA	MIP
mappingem	mapping	k1gInSc7	mapping
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bližším	blízký	k2eAgInSc6d2	bližší
průzkumu	průzkum	k1gInSc6	průzkum
anizotropního	anizotropní	k2eAgInSc2d1	anizotropní
algoritmu	algoritmus	k1gInSc2	algoritmus
a	a	k8xC	a
RIP	RIP	kA	RIP
mappingu	mapping	k1gInSc2	mapping
(	(	kIx(	(
<g/>
rozšíření	rozšíření	k1gNnSc1	rozšíření
MIP	MIP	kA	MIP
mappingu	mapping	k1gInSc2	mapping
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
pochopit	pochopit	k5eAaPmF	pochopit
jak	jak	k8xC	jak
anizotropní	anizotropní	k2eAgNnSc4d1	anizotropní
filtrování	filtrování	k1gNnSc4	filtrování
získává	získávat	k5eAaImIp3nS	získávat
takovou	takový	k3xDgFnSc4	takový
kvalitu	kvalita	k1gFnSc4	kvalita
texturového	texturový	k2eAgInSc2d1	texturový
mappingu	mapping	k1gInSc2	mapping
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
texturu	textura	k1gFnSc4	textura
horizontální	horizontální	k2eAgFnSc2d1	horizontální
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kosém	kosý	k2eAgInSc6d1	kosý
úhlu	úhel	k1gInSc6	úhel
ke	k	k7c3	k
kameře	kamera	k1gFnSc3	kamera
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgNnSc1d1	tradiční
MIP	MIP	kA	MIP
mappingové	mappingový	k2eAgNnSc1d1	mappingový
zmenšení	zmenšení	k1gNnSc1	zmenšení
by	by	kYmCp3nS	by
nám	my	k3xPp1nPc3	my
neposkytlo	poskytnout	k5eNaPmAgNnS	poskytnout
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
horizontální	horizontální	k2eAgNnSc4d1	horizontální
rozlišení	rozlišení	k1gNnSc4	rozlišení
kvůli	kvůli	k7c3	kvůli
redukci	redukce	k1gFnSc3	redukce
obrazové	obrazový	k2eAgFnSc2d1	obrazová
frekvence	frekvence	k1gFnSc2	frekvence
na	na	k7c6	na
vertikální	vertikální	k2eAgFnSc6d1	vertikální
ose	osa	k1gFnSc6	osa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
MIP	MIP	kA	MIP
mappingu	mapping	k1gInSc2	mapping
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
MIP	MIP	kA	MIP
úroveň	úroveň	k1gFnSc1	úroveň
izotropická	izotropický	k2eAgFnSc1d1	izotropický
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
256	[number]	k4	256
<g/>
×	×	k?	×
<g/>
256	[number]	k4	256
textura	textura	k1gFnSc1	textura
je	být	k5eAaImIp3nS	být
zmenšena	zmenšit	k5eAaPmNgFnS	zmenšit
na	na	k7c4	na
128	[number]	k4	128
<g/>
×	×	k?	×
<g/>
128	[number]	k4	128
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
na	na	k7c4	na
64	[number]	k4	64
<g/>
×	×	k?	×
<g/>
64	[number]	k4	64
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
rozlišení	rozlišení	k1gNnSc1	rozlišení
půlí	půlit	k5eAaImIp3nS	půlit
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
ose	osa	k1gFnSc6	osa
souběžně	souběžně	k6eAd1	souběžně
a	a	k8xC	a
obraz	obraz	k1gInSc1	obraz
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
frekvenci	frekvence	k1gFnSc4	frekvence
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
osách	osa	k1gFnPc6	osa
<g/>
.	.	kIx.	.
</s>
<s>
Takhle	takhle	k6eAd1	takhle
se	se	k3xPyFc4	se
při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
aliasingu	aliasing	k1gInSc3	aliasing
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
frekvencí	frekvence	k1gFnSc7	frekvence
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostatním	ostatní	k2eAgFnPc3d1	ostatní
osám	osa	k1gFnPc3	osa
bude	být	k5eAaImBp3nS	být
rovněž	rovněž	k9	rovněž
snížena	snížen	k2eAgFnSc1d1	snížena
vzorkovací	vzorkovací	k2eAgFnSc1d1	vzorkovací
frekvence	frekvence	k1gFnSc1	frekvence
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
budou	být	k5eAaImBp3nP	být
potenciálně	potenciálně	k6eAd1	potenciálně
rozostřené	rozostřený	k2eAgFnPc1d1	rozostřená
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
RIP	RIP	kA	RIP
mappingovým	mappingový	k2eAgNnPc3d1	mappingový
anizotropním	anizotropní	k2eAgNnPc3d1	anizotropní
filtrováním	filtrování	k1gNnPc3	filtrování
lze	lze	k6eAd1	lze
obrazům	obraz	k1gInPc3	obraz
snížit	snížit	k5eAaPmF	snížit
vzorkovací	vzorkovací	k2eAgFnSc4d1	vzorkovací
frekvenci	frekvence	k1gFnSc4	frekvence
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
128	[number]	k4	128
<g/>
×	×	k?	×
<g/>
128	[number]	k4	128
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
změnit	změnit	k5eAaPmF	změnit
i	i	k9	i
na	na	k7c4	na
256	[number]	k4	256
<g/>
×	×	k?	×
<g/>
128	[number]	k4	128
<g/>
,	,	kIx,	,
32	[number]	k4	32
<g/>
×	×	k?	×
<g/>
128	[number]	k4	128
apod.	apod.	kA	apod.
Tyto	tento	k3xDgInPc4	tento
obrazy	obraz	k1gInPc4	obraz
s	s	k7c7	s
anizotropně	anizotropně	k6eAd1	anizotropně
sníženou	snížený	k2eAgFnSc7d1	snížená
vzorkovací	vzorkovací	k2eAgFnSc7d1	vzorkovací
frekvencí	frekvence	k1gFnSc7	frekvence
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
sondovány	sondovat	k5eAaImNgInP	sondovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
frekvence	frekvence	k1gFnSc1	frekvence
texturně	texturně	k6eAd1	texturně
zmapovaného	zmapovaný	k2eAgInSc2d1	zmapovaný
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
jiná	jiný	k2eAgFnSc1d1	jiná
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
texturní	texturní	k2eAgFnSc4d1	texturní
osu	osa	k1gFnSc4	osa
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jedna	jeden	k4xCgFnSc1	jeden
osa	osa	k1gFnSc1	osa
nemusí	muset	k5eNaImIp3nS	muset
rozmazávat	rozmazávat	k5eAaImF	rozmazávat
kvůli	kvůli	k7c3	kvůli
frekvenci	frekvence	k1gFnSc3	frekvence
jiné	jiný	k2eAgFnSc2d1	jiná
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
k	k	k7c3	k
aliasingu	aliasing	k1gInSc3	aliasing
přesto	přesto	k8xC	přesto
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
Laicky	laicky	k6eAd1	laicky
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
anizotropní	anizotropní	k2eAgNnSc1d1	anizotropní
filtrování	filtrování	k1gNnSc1	filtrování
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
ostrost	ostrost	k1gFnSc4	ostrost
textury	textura	k1gFnSc2	textura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
během	během	k7c2	během
MIP	MIP	kA	MIP
mappingu	mapping	k1gInSc2	mapping
textur	textura	k1gFnPc2	textura
při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
aliasingu	aliasing	k1gInSc3	aliasing
<g/>
.	.	kIx.	.
</s>
<s>
Anizotropní	Anizotropní	k2eAgNnSc1d1	Anizotropní
filtrování	filtrování	k1gNnSc1	filtrování
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
udržet	udržet	k5eAaPmF	udržet
vysokou	vysoký	k2eAgFnSc4d1	vysoká
kvalitu	kvalita	k1gFnSc4	kvalita
textury	textura	k1gFnSc2	textura
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
zorných	zorný	k2eAgInPc6d1	zorný
úhlech	úhel	k1gInPc6	úhel
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
rychlé	rychlý	k2eAgNnSc4d1	rychlé
anti-aliasingové	antiliasingový	k2eAgNnSc4d1	anti-aliasingový
filtrování	filtrování	k1gNnSc4	filtrování
textur	textura	k1gFnPc2	textura
<g/>
.	.	kIx.	.
</s>
<s>
Anizotropické	Anizotropický	k2eAgNnSc1d1	Anizotropický
filtrování	filtrování	k1gNnSc1	filtrování
sonduje	sondovat	k5eAaImIp3nS	sondovat
texturu	textura	k1gFnSc4	textura
průběžně	průběžně	k6eAd1	průběžně
pixel	pixel	k1gInSc1	pixel
po	po	k7c6	po
pixelu	pixel	k1gInSc6	pixel
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
anizotropická	anizotropický	k2eAgFnSc1d1	anizotropický
filtrovací	filtrovací	k2eAgFnSc1d1	filtrovací
sonda	sonda	k1gFnSc1	sonda
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
sama	sám	k3xTgFnSc1	sám
filtrovaným	filtrovaný	k2eAgInSc7d1	filtrovaný
Mipmapovým	Mipmapův	k2eAgInSc7d1	Mipmapův
vzorkem	vzorek	k1gInSc7	vzorek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přináší	přinášet	k5eAaImIp3nS	přinášet
více	hodně	k6eAd2	hodně
vzorkování	vzorkování	k1gNnSc1	vzorkování
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Šestnáct	šestnáct	k4xCc1	šestnáct
trilineárních	trilineární	k2eAgInPc2d1	trilineární
anizotropních	anizotropní	k2eAgInPc2d1	anizotropní
vzorků	vzorek	k1gInPc2	vzorek
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
128	[number]	k4	128
vzorků	vzorek	k1gInPc2	vzorek
z	z	k7c2	z
uložené	uložený	k2eAgFnSc2d1	uložená
textury	textura	k1gFnSc2	textura
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
trilineární	trilineárnit	k5eAaPmIp3nS	trilineárnit
Mipmapové	Mipmapový	k2eAgNnSc1d1	Mipmapový
filtrování	filtrování	k1gNnSc1	filtrování
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
4	[number]	k4	4
vzorky	vzorek	k1gInPc7	vzorek
krát	krát	k6eAd1	krát
2	[number]	k4	2
MIP	MIP	kA	MIP
úrovně	úroveň	k1gFnSc2	úroveň
a	a	k8xC	a
anizotropické	anizotropický	k2eAgNnSc1d1	anizotropické
vzorkování	vzorkování	k1gNnSc1	vzorkování
poté	poté	k6eAd1	poté
vezme	vzít	k5eAaPmIp3nS	vzít
šestnáct	šestnáct	k4xCc1	šestnáct
filtrovaných	filtrovaný	k2eAgFnPc2d1	filtrovaná
trilineárních	trilineární	k2eAgFnPc2d1	trilineární
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
takhle	takhle	k6eAd1	takhle
složité	složitý	k2eAgNnSc1d1	složité
filtrování	filtrování	k1gNnSc1	filtrování
není	být	k5eNaImIp3nS	být
vyžadováno	vyžadovat	k5eAaImNgNnS	vyžadovat
neustále	neustále	k6eAd1	neustále
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
snižují	snižovat	k5eAaImIp3nP	snižovat
množství	množství	k1gNnSc4	množství
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
musí	muset	k5eAaImIp3nS	muset
hardware	hardware	k1gInSc1	hardware
při	při	k7c6	při
renderování	renderování	k1gNnSc6	renderování
vykonat	vykonat	k5eAaPmF	vykonat
<g/>
.	.	kIx.	.
</s>
<s>
Požadovaný	požadovaný	k2eAgInSc1d1	požadovaný
počet	počet	k1gInSc1	počet
vzorků	vzorek	k1gInPc2	vzorek
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
anizotropické	anizotropický	k2eAgNnSc4d1	anizotropické
filtrování	filtrování	k1gNnSc4	filtrování
extrémně	extrémně	k6eAd1	extrémně
zatíží	zatížit	k5eAaPmIp3nP	zatížit
šířku	šířka	k1gFnSc4	šířka
kmitočtového	kmitočtový	k2eAgNnSc2d1	kmitočtové
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Mnohonásobné	mnohonásobný	k2eAgFnPc1d1	mnohonásobná
textury	textura	k1gFnPc1	textura
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgInPc1d1	běžný
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
texturový	texturový	k2eAgInSc1d1	texturový
vzorek	vzorek	k1gInSc1	vzorek
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
4	[number]	k4	4
byty	byt	k1gInPc4	byt
nebo	nebo	k8xC	nebo
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
každý	každý	k3xTgInSc1	každý
anizotropní	anizotropní	k2eAgInSc1d1	anizotropní
pixel	pixel	k1gInSc1	pixel
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
512	[number]	k4	512
bytů	byt	k1gInPc2	byt
z	z	k7c2	z
texturové	texturový	k2eAgFnSc2d1	texturová
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
běžně	běžně	k6eAd1	běžně
používaná	používaný	k2eAgFnSc1d1	používaná
komprese	komprese	k1gFnSc1	komprese
textur	textura	k1gFnPc2	textura
toto	tento	k3xDgNnSc4	tento
množství	množství	k1gNnSc4	množství
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
monitor	monitor	k1gInSc1	monitor
dokáže	dokázat	k5eAaPmIp3nS	dokázat
obsáhnout	obsáhnout	k5eAaPmF	obsáhnout
okolo	okolo	k7c2	okolo
dvou	dva	k4xCgInPc2	dva
milionů	milion	k4xCgInPc2	milion
pixelů	pixel	k1gInPc2	pixel
a	a	k8xC	a
snímkovou	snímkový	k2eAgFnSc4d1	snímková
frekvenci	frekvence	k1gFnSc4	frekvence
30-60	[number]	k4	30-60
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
víc	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
kmitočtového	kmitočtový	k2eAgNnSc2d1	kmitočtové
pásma	pásmo	k1gNnSc2	pásmo
texturní	texturní	k2eAgFnSc2d1	texturní
paměti	paměť	k1gFnSc2	paměť
rychle	rychle	k6eAd1	rychle
může	moct	k5eAaImIp3nS	moct
vyšplhat	vyšplhat	k5eAaPmF	vyšplhat
velmi	velmi	k6eAd1	velmi
vysoko	vysoko	k6eAd1	vysoko
<g/>
.	.	kIx.	.
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
až	až	k9	až
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
gigabytů	gigabyt	k1gInPc2	gigabyt
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
anizotropní	anizotropní	k2eAgNnSc1d1	anizotropní
filtrování	filtrování	k1gNnSc1	filtrování
zapnuté	zapnutý	k2eAgNnSc1d1	zapnuté
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Naked	Naked	k1gMnSc1	Naked
Truth	Truth	k1gMnSc1	Truth
About	About	k1gMnSc1	About
Anisotropic	Anisotropic	k1gMnSc1	Anisotropic
Filtering	Filtering	k1gInSc4	Filtering
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Anisotropic	Anisotropice	k1gInPc2	Anisotropice
filtering	filtering	k1gInSc4	filtering
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
