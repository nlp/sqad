<s>
Sir	sir	k1gMnSc1	sir
James	James	k1gMnSc1	James
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
MBE	MBE	kA	MBE
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
rodičům	rodič	k1gMnPc3	rodič
s	s	k7c7	s
irskými	irský	k2eAgInPc7d1	irský
předky	předek	k1gInPc7	předek
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
McCartney	McCartnea	k1gFnSc2	McCartnea
a	a	k8xC	a
Mary	Mary	k1gFnSc1	Mary
Patricia	Patricius	k1gMnSc2	Patricius
Mohinová	Mohinová	k1gFnSc1	Mohinová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Michaelem	Michael	k1gMnSc7	Michael
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Mike	Mike	k1gNnSc2	Mike
McGear	McGear	k1gInSc1	McGear
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
v	v	k7c6	v
komediální	komediální	k2eAgFnSc6d1	komediální
hudební	hudební	k2eAgFnSc6d1	hudební
skupině	skupina	k1gFnSc6	skupina
The	The	k1gMnSc1	The
Scaffold	Scaffold	k1gMnSc1	Scaffold
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
vychováváni	vychováván	k2eAgMnPc1d1	vychováván
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
víře	víra	k1gFnSc6	víra
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
prsu	prs	k1gInSc2	prs
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Paulovi	Paulův	k2eAgMnPc1d1	Paulův
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
mu	on	k3xPp3gMnSc3	on
otec	otec	k1gMnSc1	otec
koupil	koupit	k5eAaPmAgMnS	koupit
první	první	k4xOgFnSc4	první
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
dříve	dříve	k6eAd2	dříve
sám	sám	k3xTgMnSc1	sám
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
trubku	trubka	k1gFnSc4	trubka
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
v	v	k7c6	v
jazzové	jazzový	k2eAgFnSc6d1	jazzová
kapele	kapela	k1gFnSc6	kapela
<g/>
,	,	kIx,	,
dával	dávat	k5eAaImAgInS	dávat
Paulovi	Paul	k1gMnSc3	Paul
první	první	k4xOgFnSc2	první
lekce	lekce	k1gFnSc2	lekce
hudby	hudba	k1gFnSc2	hudba
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
Liverpool	Liverpool	k1gInSc1	Liverpool
Institute	institut	k1gInSc5	institut
poznal	poznat	k5eAaPmAgMnS	poznat
George	Georg	k1gMnSc4	Georg
Harrisona	Harrison	k1gMnSc4	Harrison
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
učit	učit	k5eAaImF	učit
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
-	-	kIx~	-
Paul	Paul	k1gMnSc1	Paul
je	být	k5eAaImIp3nS	být
levák	levák	k1gMnSc1	levák
a	a	k8xC	a
kytaru	kytara	k1gFnSc4	kytara
se	s	k7c7	s
zrcadlově	zrcadlově	k6eAd1	zrcadlově
nataženými	natažený	k2eAgFnPc7d1	natažená
strunami	struna	k1gFnPc7	struna
drží	držet	k5eAaImIp3nP	držet
krkem	krk	k1gInSc7	krk
na	na	k7c4	na
opačnou	opačný	k2eAgFnSc4d1	opačná
stranu	strana	k1gFnSc4	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnSc1	Beatles
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
pouti	pouť	k1gFnSc6	pouť
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Wooltonu	Woolton	k1gInSc6	Woolton
Paul	Paul	k1gMnSc1	Paul
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Lennonem	Lennon	k1gMnSc7	Lennon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgMnS	přivést
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Quarry	Quarra	k1gMnSc2	Quarra
Men	Men	k1gMnSc2	Men
jako	jako	k8xS	jako
kytaristu	kytarista	k1gMnSc4	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
okamžikem	okamžik	k1gInSc7	okamžik
vzniká	vznikat	k5eAaImIp3nS	vznikat
nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
hudební	hudební	k2eAgNnSc4d1	hudební
spojení	spojení	k1gNnSc4	spojení
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
-	-	kIx~	-
Lennon	Lennon	k1gInSc1	Lennon
<g/>
/	/	kIx~	/
<g/>
McCartney	McCartnea	k1gFnPc1	McCartnea
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
a	a	k8xC	a
McCartney	McCartnea	k1gFnPc1	McCartnea
pak	pak	k6eAd1	pak
začínají	začínat	k5eAaImIp3nP	začínat
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
za	za	k7c4	za
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
spolupráce	spolupráce	k1gFnSc2	spolupráce
jich	on	k3xPp3gFnPc2	on
napsali	napsat	k5eAaPmAgMnP	napsat
kolem	kolem	k6eAd1	kolem
jednoho	jeden	k4xCgMnSc4	jeden
sta	sto	k4xCgNnPc4	sto
<g/>
.	.	kIx.	.
</s>
<s>
Veškerý	veškerý	k3xTgInSc4	veškerý
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
trávili	trávit	k5eAaImAgMnP	trávit
spolu	spolu	k6eAd1	spolu
a	a	k8xC	a
hráli	hrát	k5eAaImAgMnP	hrát
na	na	k7c4	na
kytary	kytara	k1gFnPc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
prospíval	prospívat	k5eAaImAgMnS	prospívat
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
německy	německy	k6eAd1	německy
i	i	k9	i
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
muzice	muzika	k1gFnSc3	muzika
a	a	k8xC	a
tak	tak	k6eAd1	tak
místo	místo	k7c2	místo
učení	učení	k1gNnSc2	učení
začal	začít	k5eAaPmAgMnS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
po	po	k7c6	po
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
místo	místo	k7c2	místo
dalšího	další	k2eAgNnSc2d1	další
studia	studio	k1gNnSc2	studio
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
pomocník	pomocník	k1gMnSc1	pomocník
strojvedoucího	strojvedoucí	k1gMnSc2	strojvedoucí
na	na	k7c6	na
lokomotivě	lokomotiva	k1gFnSc6	lokomotiva
a	a	k8xC	a
stáčet	stáčet	k5eAaImF	stáčet
drát	drát	k1gInSc4	drát
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
Quarry	Quarra	k1gFnSc2	Quarra
Men	Men	k1gFnSc1	Men
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Johnny	Johnn	k1gInPc4	Johnn
and	and	k?	and
The	The	k1gFnSc2	The
Moondogs	Moondogsa	k1gFnPc2	Moondogsa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
opět	opět	k6eAd1	opět
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
The	The	k1gFnSc6	The
Silver	Silver	k1gMnSc1	Silver
Beetles	Beetles	k1gMnSc1	Beetles
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
naposledy	naposledy	k6eAd1	naposledy
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
The	The	k1gFnSc4	The
Beatles	Beatles	k1gFnPc2	Beatles
(	(	kIx(	(
<g/>
slovní	slovní	k2eAgFnSc1d1	slovní
hříčka	hříčka	k1gFnSc1	hříčka
-	-	kIx~	-
beetle	beetle	k6eAd1	beetle
znamená	znamenat	k5eAaImIp3nS	znamenat
brouk	brouk	k1gMnSc1	brouk
<g/>
,	,	kIx,	,
beat	beat	k1gInSc1	beat
znamená	znamenat	k5eAaImIp3nS	znamenat
tlouci	tlouct	k5eAaImF	tlouct
<g/>
,	,	kIx,	,
bít	bít	k5eAaImF	bít
<g/>
,	,	kIx,	,
tep	tep	k1gInSc1	tep
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
angažmá	angažmá	k1gNnPc4	angažmá
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
odchodu	odchod	k1gInSc3	odchod
původního	původní	k2eAgMnSc2d1	původní
basového	basový	k2eAgMnSc2d1	basový
kytaristy	kytarista	k1gMnSc2	kytarista
Stuarta	Stuart	k1gMnSc2	Stuart
Sutcliffa	Sutcliff	k1gMnSc2	Sutcliff
jeho	jeho	k3xOp3gNnSc1	jeho
nástroj	nástroj	k1gInSc4	nástroj
přebírá	přebírat	k5eAaImIp3nS	přebírat
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
The	The	k1gFnPc2	The
Beatles	Beatles	k1gFnPc2	Beatles
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
rozpadu	rozpad	k1gInSc2	rozpad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
manažera	manažer	k1gMnSc2	manažer
skupiny	skupina	k1gFnSc2	skupina
Briana	Brian	k1gMnSc2	Brian
Epsteina	Epstein	k1gMnSc2	Epstein
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nepsaným	nepsaný	k2eAgMnSc7d1	nepsaný
vůdcem	vůdce	k1gMnSc7	vůdce
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
především	především	k9	především
jejím	její	k3xOp3gMnSc7	její
nejaktivnějším	aktivní	k2eAgMnSc7d3	nejaktivnější
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
a	a	k8xC	a
nejznámější	známý	k2eAgFnPc4d3	nejznámější
písně	píseň	k1gFnPc4	píseň
éry	éra	k1gFnSc2	éra
Beatles	Beatles	k1gFnPc2	Beatles
patří	patřit	k5eAaImIp3nS	patřit
Eleanor	Eleanor	k1gInSc1	Eleanor
Rigby	Rigba	k1gFnSc2	Rigba
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Buy	Buy	k1gMnSc1	Buy
Me	Me	k1gMnSc1	Me
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Let	let	k1gInSc1	let
It	It	k1gFnSc2	It
Be	Be	k1gFnSc2	Be
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
Hey	Hey	k1gFnSc1	Hey
Jude	Jude	k1gFnSc1	Jude
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
a	a	k8xC	a
Yesterday	Yesterday	k1gInPc1	Yesterday
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
začal	začít	k5eAaPmAgMnS	začít
Paul	Paul	k1gMnSc1	Paul
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
)	)	kIx)	)
užívat	užívat	k5eAaImF	užívat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
drogy	droga	k1gFnPc4	droga
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
vydává	vydávat	k5eAaPmIp3nS	vydávat
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
meditovala	meditovat	k5eAaImAgFnS	meditovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
drog	droga	k1gFnPc2	droga
Paul	Paul	k1gMnSc1	Paul
experimentuje	experimentovat	k5eAaImIp3nS	experimentovat
jako	jako	k9	jako
první	první	k4xOgInSc1	první
člen	člen	k1gInSc1	člen
Beatles	Beatles	k1gFnSc2	Beatles
s	s	k7c7	s
elektronickou	elektronický	k2eAgFnSc7d1	elektronická
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
natáčí	natáčet	k5eAaImIp3nP	natáčet
soukromé	soukromý	k2eAgInPc4d1	soukromý
undergroundové	undergroundový	k2eAgInPc4d1	undergroundový
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Paulovým	Paulův	k2eAgNnSc7d1	Paulovo
vedením	vedení	k1gNnSc7	vedení
také	také	k9	také
vzniká	vznikat	k5eAaImIp3nS	vznikat
kritikou	kritika	k1gFnSc7	kritika
odsouzený	odsouzený	k2eAgMnSc1d1	odsouzený
a	a	k8xC	a
veřejností	veřejnost	k1gFnSc7	veřejnost
nepochopený	pochopený	k2eNgInSc4d1	nepochopený
(	(	kIx(	(
<g/>
avšak	avšak	k8xC	avšak
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
pozitivně	pozitivně	k6eAd1	pozitivně
vnímaný	vnímaný	k2eAgInSc1d1	vnímaný
<g/>
)	)	kIx)	)
film	film	k1gInSc1	film
Beatles	Beatles	k1gFnSc2	Beatles
Magical	Magical	k1gFnSc2	Magical
Mystery	Myster	k1gInPc1	Myster
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc1d1	plný
psychedelických	psychedelický	k2eAgMnPc2d1	psychedelický
obrazců	obrazec	k1gInPc2	obrazec
a	a	k8xC	a
výjevů	výjev	k1gInPc2	výjev
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
žil	žít	k5eAaImAgMnS	žít
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Jane	Jan	k1gMnSc5	Jan
Asherovou	Asherův	k2eAgFnSc7d1	Asherova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Lindou	Linda	k1gFnSc7	Linda
Eastmanovou	Eastmanův	k2eAgFnSc7d1	Eastmanův
<g/>
,	,	kIx,	,
americkou	americký	k2eAgFnSc7d1	americká
fotografkou	fotografka	k1gFnSc7	fotografka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
,	,	kIx,	,
o	o	k7c4	o
osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
tak	tak	k8xS	tak
předběhli	předběhnout	k5eAaPmAgMnP	předběhnout
Johna	John	k1gMnSc4	John
Lennona	Lennon	k1gMnSc4	Lennon
s	s	k7c7	s
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
Lindinu	Lindin	k2eAgFnSc4d1	Lindina
dceru	dcera	k1gFnSc4	dcera
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
Heather	Heathra	k1gFnPc2	Heathra
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Lindou	Linda	k1gFnSc7	Linda
měli	mít	k5eAaImAgMnP	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
-	-	kIx~	-
Mary	Mary	k1gFnSc1	Mary
Annu	Anna	k1gFnSc4	Anna
<g/>
,	,	kIx,	,
Stellu	Stella	k1gFnSc4	Stella
Ninu	Nina	k1gFnSc4	Nina
a	a	k8xC	a
Jamese	Jamese	k1gFnPc4	Jamese
Louise	Louis	k1gMnSc2	Louis
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
manželé	manžel	k1gMnPc1	manžel
vždy	vždy	k6eAd1	vždy
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
devíti	devět	k4xCc2	devět
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
McCartney	McCartnea	k1gFnSc2	McCartnea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
vězení	vězení	k1gNnSc6	vězení
(	(	kIx(	(
<g/>
za	za	k7c2	za
držení	držení	k1gNnSc2	držení
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1970	[number]	k4	1970
Paul	Paula	k1gFnPc2	Paula
McCartney	McCartnea	k1gFnSc2	McCartnea
veřejně	veřejně	k6eAd1	veřejně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupinu	skupina	k1gFnSc4	skupina
Beatles	Beatles	k1gFnPc2	Beatles
opouští	opouštět	k5eAaImIp3nS	opouštět
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
kapela	kapela	k1gFnSc1	kapela
de	de	k?	de
facto	facto	k1gNnSc1	facto
neexistovala	existovat	k5eNaImAgFnS	existovat
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc4d1	ostatní
jej	on	k3xPp3gMnSc4	on
následovali	následovat	k5eAaImAgMnP	následovat
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaPmIp3nP	věnovat
vlastním	vlastní	k2eAgInPc3d1	vlastní
projektům	projekt	k1gInPc3	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
naučil	naučit	k5eAaPmAgMnS	naučit
manželku	manželka	k1gFnSc4	manželka
Lindu	Linda	k1gFnSc4	Linda
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
a	a	k8xC	a
angažoval	angažovat	k5eAaBmAgInS	angažovat
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nové	nový	k2eAgFnSc6d1	nová
skupině	skupina	k1gFnSc6	skupina
Wings	Wingsa	k1gFnPc2	Wingsa
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
společnou	společný	k2eAgFnSc7d1	společná
prací	práce	k1gFnSc7	práce
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc4	píseň
Another	Anothra	k1gFnPc2	Anothra
Day	Day	k1gMnPc3	Day
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
hudební	hudební	k2eAgMnPc1d1	hudební
kritici	kritik	k1gMnPc1	kritik
Lindu	Linda	k1gFnSc4	Linda
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
za	za	k7c4	za
její	její	k3xOp3gInSc4	její
zpěv	zpěv	k1gInSc4	zpěv
i	i	k8xC	i
hraní	hraní	k1gNnSc4	hraní
<g/>
,	,	kIx,	,
Wings	Wings	k1gInSc4	Wings
získali	získat	k5eAaPmAgMnP	získat
mnoho	mnoho	k4c4	mnoho
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
skupina	skupina	k1gFnSc1	skupina
na	na	k7c4	na
první	první	k4xOgNnSc4	první
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
vydávají	vydávat	k5eAaPmIp3nP	vydávat
The	The	k1gFnPc1	The
Wings	Wingsa	k1gFnPc2	Wingsa
patrně	patrně	k6eAd1	patrně
své	svůj	k3xOyFgNnSc4	svůj
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
Band	band	k1gInSc4	band
On	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Run	Runa	k1gFnPc2	Runa
<g/>
.	.	kIx.	.
</s>
<s>
McCartney	McCartne	k1gMnPc4	McCartne
vydal	vydat	k5eAaPmAgInS	vydat
nespočet	nespočet	k1gInSc1	nespočet
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Wings	Wingsa	k1gFnPc2	Wingsa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInSc4d3	nejznámější
vedle	vedle	k7c2	vedle
již	již	k6eAd1	již
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
patří	patřit	k5eAaImIp3nS	patřit
Ram	Ram	k1gMnSc1	Ram
<g/>
,	,	kIx,	,
Venus	Venus	k1gMnSc1	Venus
and	and	k?	and
Mars	Mars	k1gMnSc1	Mars
či	či	k8xC	či
Tug	Tug	k1gMnSc1	Tug
of	of	k?	of
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
se	s	k7c7	s
Steviem	Stevius	k1gMnSc7	Stevius
Wonderem	Wonder	k1gMnSc7	Wonder
nebo	nebo	k8xC	nebo
Michaelem	Michael	k1gMnSc7	Michael
Jacksonem	Jackson	k1gMnSc7	Jackson
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
vegetariány	vegetarián	k1gMnPc4	vegetarián
a	a	k8xC	a
bojovníky	bojovník	k1gMnPc4	bojovník
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
předává	předávat	k5eAaImIp3nS	předávat
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
zbylým	zbylý	k2eAgMnPc3d1	zbylý
členům	člen	k1gMnPc3	člen
Beatles	Beatles	k1gFnSc4	Beatles
Lennonovy	Lennonův	k2eAgFnSc2d1	Lennonova
demonahrávky	demonahrávka	k1gFnSc2	demonahrávka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
pak	pak	k6eAd1	pak
nahrají	nahrát	k5eAaPmIp3nP	nahrát
hudební	hudební	k2eAgInSc4d1	hudební
doprovod	doprovod	k1gInSc4	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
skladby	skladba	k1gFnPc1	skladba
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Real	Real	k1gInSc4	Real
Love	lov	k1gInSc5	lov
a	a	k8xC	a
Free	Free	k1gNnPc1	Free
as	as	k1gNnPc2	as
a	a	k8xC	a
Bird	Birda	k1gFnPc2	Birda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vydány	vydat	k5eAaPmNgInP	vydat
na	na	k7c6	na
albech	album	k1gNnPc6	album
Anthology	Antholog	k1gMnPc4	Antholog
1	[number]	k4	1
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
Anthology	Antholog	k1gMnPc4	Antholog
2	[number]	k4	2
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
Now	Now	k1gFnSc2	Now
And	Anda	k1gFnPc2	Anda
Then	Then	k1gInSc1	Then
<g/>
,	,	kIx,	,
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
pro	pro	k7c4	pro
Anthology	Antholog	k1gMnPc4	Antholog
3	[number]	k4	3
(	(	kIx(	(
<g/>
Apple	Apple	kA	Apple
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgInS	být
McCartney	McCartnea	k1gFnSc2	McCartnea
pasován	pasován	k2eAgInSc1d1	pasován
na	na	k7c4	na
rytíře	rytíř	k1gMnSc4	rytíř
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
neuvěřitelným	uvěřitelný	k2eNgInSc7d1	neuvěřitelný
počtem	počet	k1gInSc7	počet
prodaných	prodaný	k2eAgFnPc2d1	prodaná
desek	deska	k1gFnPc2	deska
je	být	k5eAaImIp3nS	být
právem	právem	k6eAd1	právem
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
hudebníků	hudebník	k1gMnPc2	hudebník
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Paulova	Paulův	k2eAgFnSc1d1	Paulova
žena	žena	k1gFnSc1	žena
Linda	Linda	k1gFnSc1	Linda
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
McCartney	McCartnea	k1gFnPc1	McCartnea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
podruhé	podruhé	k6eAd1	podruhé
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
modelkou	modelka	k1gFnSc7	modelka
Heather	Heathra	k1gFnPc2	Heathra
Millsovou	Millsův	k2eAgFnSc7d1	Millsova
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Beatrice	Beatrice	k1gFnSc2	Beatrice
Milly	Milla	k1gFnSc2	Milla
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
s	s	k7c7	s
Heather	Heathra	k1gFnPc2	Heathra
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
i	i	k9	i
vážné	vážný	k2eAgFnSc3d1	vážná
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
složil	složit	k5eAaPmAgMnS	složit
několik	několik	k4yIc4	několik
klasických	klasický	k2eAgNnPc2d1	klasické
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Liverpoolské	liverpoolský	k2eAgNnSc1d1	Liverpoolské
oratorium	oratorium	k1gNnSc1	oratorium
nebo	nebo	k8xC	nebo
Standing	Standing	k1gInSc1	Standing
Stone	ston	k1gInSc5	ston
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
byl	být	k5eAaImAgMnS	být
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
koncert	koncert	k1gInSc4	koncert
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
Lindě	Linda	k1gFnSc6	Linda
a	a	k8xC	a
také	také	k9	také
první	první	k4xOgFnSc4	první
výstavu	výstava	k1gFnSc4	výstava
svých	svůj	k3xOyFgInPc2	svůj
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Beatles	Beatles	k1gFnSc2	Beatles
<g/>
,	,	kIx,	,
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
převzal	převzít	k5eAaPmAgMnS	převzít
ocenění	ocenění	k1gNnSc4	ocenění
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
hudební	hudební	k2eAgInSc4d1	hudební
přínos	přínos	k1gInSc4	přínos
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
BRIT	Brit	k1gMnSc1	Brit
Awards	Awards	k1gInSc1	Awards
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyšel	vyjít	k5eAaPmAgInS	vyjít
1	[number]	k4	1
DVD	DVD	kA	DVD
+	+	kIx~	+
2	[number]	k4	2
CD	CD	kA	CD
live	livat	k5eAaPmIp3nS	livat
koncert	koncert	k1gInSc1	koncert
s	s	k7c7	s
názvem	název	k1gInSc7	název
Good	Good	k1gInSc1	Good
Evening	Evening	k1gInSc4	Evening
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
sestříhán	sestříhat	k5eAaPmNgInS	sestříhat
z	z	k7c2	z
několika	několik	k4yIc2	několik
nocí	noc	k1gFnPc2	noc
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jak	jak	k6eAd1	jak
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
tvorby	tvorba	k1gFnSc2	tvorba
Paula	Paul	k1gMnSc2	Paul
+	+	kIx~	+
Wings	Wings	k1gInSc1	Wings
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
hity	hit	k1gInPc1	hit
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
potřetí	potřetí	k4xO	potřetí
oženil	oženit	k5eAaPmAgInS	oženit
s	s	k7c7	s
Nancy	Nancy	k1gFnSc7	Nancy
Shevellovou	Shevellová	k1gFnSc7	Shevellová
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
hudebním	hudební	k2eAgInSc7d1	hudební
počinem	počin	k1gInSc7	počin
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
deska	deska	k1gFnSc1	deska
Kisses	Kisses	k1gInSc4	Kisses
On	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Bottom	Bottom	k1gInSc1	Bottom
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
vzdal	vzdát	k5eAaPmAgMnS	vzdát
hold	hold	k1gInSc4	hold
svým	svůj	k3xOyFgFnPc3	svůj
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
melodiím	melodie	k1gFnPc3	melodie
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
nadělil	nadělit	k5eAaPmAgInS	nadělit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
70	[number]	k4	70
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
benefičním	benefiční	k2eAgInSc6d1	benefiční
koncertu	koncert	k1gInSc6	koncert
121212	[number]	k4	121212
<g/>
,	,	kIx,	,
věnovaném	věnovaný	k2eAgInSc6d1	věnovaný
obětem	oběť	k1gFnPc3	oběť
hurikánu	hurikán	k1gInSc3	hurikán
Sandy	Sand	k1gInPc4	Sand
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bývalými	bývalý	k2eAgMnPc7d1	bývalý
členy	člen	k1gMnPc7	člen
skupiny	skupina	k1gFnSc2	skupina
Nirvana	Nirvan	k1gMnSc2	Nirvan
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
rozpadu	rozpad	k1gInSc2	rozpad
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahráli	zahrát	k5eAaPmAgMnP	zahrát
společnou	společný	k2eAgFnSc4d1	společná
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Cut	Cut	k1gFnSc4	Cut
Me	Me	k1gMnSc2	Me
Some	Som	k1gMnSc2	Som
Slack	Slack	k1gMnSc1	Slack
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
Sound	Sounda	k1gFnPc2	Sounda
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
režíruje	režírovat	k5eAaImIp3nS	režírovat
ex	ex	k6eAd1	ex
bubeník	bubeník	k1gMnSc1	bubeník
Nirvany	Nirvan	k1gMnPc4	Nirvan
Dave	Dav	k1gInSc5	Dav
Grohl	Grohl	k1gFnSc4	Grohl
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
vydává	vydávat	k5eAaImIp3nS	vydávat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
New	New	k1gFnSc2	New
se	s	k7c7	s
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
titulní	titulní	k2eAgFnSc7d1	titulní
písní	píseň	k1gFnSc7	píseň
<g/>
.	.	kIx.	.
1961	[number]	k4	1961
<g/>
:	:	kIx,	:
Hofner	Hofner	k1gInSc4	Hofner
500	[number]	k4	500
Bass	Bass	k1gMnSc1	Bass
-	-	kIx~	-
Model	model	k1gInSc1	model
měl	mít	k5eAaImAgInS	mít
hnědo-žlutou	hnědo-žlutý	k2eAgFnSc4d1	hnědo-žlutá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
ho	on	k3xPp3gMnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
a	a	k8xC	a
používal	používat	k5eAaImAgInS	používat
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dostal	dostat	k5eAaPmAgInS	dostat
druhý	druhý	k4xOgInSc4	druhý
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc4d1	podobný
model	model	k1gInSc4	model
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
záložní	záložní	k2eAgInSc1d1	záložní
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968-69	[number]	k4	1968-69
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
oba	dva	k4xCgInPc4	dva
modely	model	k1gInPc4	model
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
poté	poté	k6eAd1	poté
kytaru	kytara	k1gFnSc4	kytara
někdo	někdo	k3yInSc1	někdo
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
<g/>
.	.	kIx.	.
1963	[number]	k4	1963
<g/>
:	:	kIx,	:
Hofner	Hofner	k1gInSc1	Hofner
500	[number]	k4	500
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
Bass	Bass	k1gMnSc1	Bass
-	-	kIx~	-
Model	model	k1gInSc1	model
měl	mít	k5eAaImAgInS	mít
hnědo-žlutou	hnědo-žlutý	k2eAgFnSc4d1	hnědo-žlutá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
ho	on	k3xPp3gMnSc4	on
darem	dar	k1gInSc7	dar
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Hofner	Hofner	k1gMnSc1	Hofner
používal	používat	k5eAaImAgMnS	používat
ho	on	k3xPp3gInSc2	on
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
starým	starý	k2eAgInSc7d1	starý
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
Hofnerem	Hofner	k1gMnSc7	Hofner
<g/>
:	:	kIx,	:
Starý	Starý	k1gMnSc1	Starý
Měl	mít	k5eAaImAgMnS	mít
Tmavší	tmavý	k2eAgFnSc4d2	tmavší
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
napsané	napsaný	k2eAgNnSc4d1	napsané
logo	logo	k1gNnSc4	logo
<g/>
,	,	kIx,	,
snímače	snímač	k1gInPc4	snímač
hned	hned	k6eAd1	hned
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
maličkosti	maličkost	k1gFnPc1	maličkost
<g/>
.	.	kIx.	.
1964	[number]	k4	1964
<g/>
:	:	kIx,	:
Epiphone	Epiphon	k1gMnSc5	Epiphon
FT-79	FT-79	k1gMnSc5	FT-79
Texan	Texan	k1gInSc4	Texan
Model	model	k1gInSc1	model
měl	mít	k5eAaImAgInS	mít
přírodní	přírodní	k2eAgFnSc4d1	přírodní
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
modelem	model	k1gInSc7	model
hrál	hrát	k5eAaImAgMnS	hrát
Yesterday	Yesterdaa	k1gFnPc4	Yesterdaa
<g/>
.	.	kIx.	.
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
(	(	kIx(	(
Ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
)	)	kIx)	)
All	All	k1gFnSc1	All
My	my	k3xPp1nPc1	my
Loving	Loving	k1gInSc1	Loving
Can	Can	k1gFnSc1	Can
<g/>
́	́	k?	́
<g/>
t	t	k?	t
Buy	Buy	k1gMnSc1	Buy
Me	Me	k1gMnSc1	Me
Love	lov	k1gInSc5	lov
Yesterday	Yesterday	k1gInPc1	Yesterday
Drive	drive	k1gInSc1	drive
My	my	k3xPp1nPc1	my
Car	car	k1gMnSc1	car
Eleanor	Eleanor	k1gInSc4	Eleanor
Rigby	Rigba	k1gFnSc2	Rigba
Penny	penny	k1gFnSc2	penny
Lane	Lan	k1gFnSc2	Lan
Hey	Hey	k1gFnSc2	Hey
Jude	Jud	k1gFnSc2	Jud
Let	léto	k1gNnPc2	léto
It	It	k1gMnSc7	It
Be	Be	k1gMnSc7	Be
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
(	(	kIx(	(
Ve	v	k7c6	v
Skupině	skupina	k1gFnSc6	skupina
Wings	Wingsa	k1gFnPc2	Wingsa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
C	C	kA	C
Moon	Moon	k1gMnSc1	Moon
My	my	k3xPp1nPc1	my
Love	lov	k1gInSc5	lov
Live	Live	k1gNnSc3	Live
And	Anda	k1gFnPc2	Anda
Let	léto	k1gNnPc2	léto
Die	Die	k1gMnSc1	Die
Jet	jet	k2eAgInSc4d1	jet
Band	band	k1gInSc4	band
On	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Run	Runa	k1gFnPc2	Runa
Let	let	k1gInSc4	let
́	́	k?	́
<g/>
Em	Ema	k1gFnPc2	Ema
In	In	k1gMnSc1	In
Mull	Mull	k1gMnSc1	Mull
Of	Of	k1gMnSc1	Of
Kintyre	Kintyr	k1gInSc5	Kintyr
70	[number]	k4	70
<g/>
.	.	kIx.	.
+	+	kIx~	+
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
Maybe	Mayb	k1gInSc5	Mayb
I	i	k9	i
<g/>
́	́	k?	́
<g/>
m	m	kA	m
Amazed	Amazed	k1gInSc4	Amazed
Ebony	Ebona	k1gFnSc2	Ebona
And	Anda	k1gFnPc2	Anda
Ivory	Ivora	k1gFnSc2	Ivora
(	(	kIx(	(
<g/>
+	+	kIx~	+
Stevie	Stevie	k1gFnSc1	Stevie
Wonder	Wonder	k1gMnSc1	Wonder
<g/>
)	)	kIx)	)
Say	Say	k1gMnSc1	Say
Say	Say	k1gMnSc1	Say
<g />
.	.	kIx.	.
</s>
<s>
Say	Say	k?	Say
(	(	kIx(	(
<g/>
+	+	kIx~	+
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
)	)	kIx)	)
No	no	k9	no
More	mor	k1gInSc5	mor
Lonely	Lonel	k1gInPc4	Lonel
Nights	Nights	k1gInSc1	Nights
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
a	a	k8xC	a
2000	[number]	k4	2000
-	-	kIx~	-
2012	[number]	k4	2012
Hope	Hope	k1gNnPc2	Hope
Of	Of	k1gMnPc2	Of
Deliverence	Deliverence	k1gFnSc2	Deliverence
Young	Young	k1gMnSc1	Young
Boy	boy	k1gMnSc1	boy
Fine	Fin	k1gMnSc5	Fin
Line	linout	k5eAaImIp3nS	linout
McCartney	McCartneum	k1gNnPc7	McCartneum
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
Ram	Ram	k1gFnPc2	Ram
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
UK	UK	kA	UK
<g />
.	.	kIx.	.
</s>
<s>
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
2	[number]	k4	2
McCartney	McCartnea	k1gFnSc2	McCartnea
II	II	kA	II
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
Tug	Tug	k1gMnSc1	Tug
of	of	k?	of
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
UK	UK	kA	UK
<g/>
#	#	kIx~	#
1	[number]	k4	1
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
Pipes	Pipes	k1gMnSc1	Pipes
of	of	k?	of
Peace	Peace	k1gMnSc1	Peace
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
4	[number]	k4	4
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
15	[number]	k4	15
Press	Pressa	k1gFnPc2	Pressa
To	ten	k3xDgNnSc1	ten
Play	play	k0	play
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
8	[number]	k4	8
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
30	[number]	k4	30
С	С	k?	С
в	в	k?	в
С	С	k?	С
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
63	[number]	k4	63
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
109	[number]	k4	109
Flowers	Flowers	k1gInSc1	Flowers
in	in	k?	in
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Dirt	Dirt	k1gInSc1	Dirt
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
24	[number]	k4	24
Off	Off	k1gMnSc1	Off
The	The	k1gMnSc1	The
Ground	Ground	k1gMnSc1	Ground
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
5	[number]	k4	5
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
17	[number]	k4	17
Off	Off	k1gMnSc1	Off
The	The	k1gMnSc1	The
Ground	Ground	k1gMnSc1	Ground
-	-	kIx~	-
The	The	k1gMnSc1	The
Complete	Comple	k1gNnSc2	Comple
Works	Works	kA	Works
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Flaming	Flaming	k1gInSc1	Flaming
Pie	Pius	k1gMnPc4	Pius
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
2	[number]	k4	2
Run	Runa	k1gFnPc2	Runa
Devil	Devila	k1gFnPc2	Devila
Run	run	k1gInSc1	run
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
12	[number]	k4	12
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
27	[number]	k4	27
Driving	Driving	k1gInSc1	Driving
Rain	Rain	k1gInSc4	Rain
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
26	[number]	k4	26
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
26	[number]	k4	26
Chaos	chaos	k1gInSc1	chaos
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
Creation	Creation	k1gInSc1	Creation
in	in	k?	in
the	the	k?	the
Backyard	Backyard	k1gInSc1	Backyard
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
10	[number]	k4	10
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
6	[number]	k4	6
Memory	Memora	k1gFnSc2	Memora
Almost	Almost	k1gFnSc1	Almost
Full	Full	k1gMnSc1	Full
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
5	[number]	k4	5
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
3	[number]	k4	3
Kisses	Kissesa	k1gFnPc2	Kissesa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Bottom	Bottom	k1gInSc1	Bottom
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
New	New	k1gFnSc2	New
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Tripping	Tripping	k1gInSc1	Tripping
the	the	k?	the
Live	Liv	k1gInSc2	Liv
Fantastic	Fantastice	k1gFnPc2	Fantastice
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
16	[number]	k4	16
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
26	[number]	k4	26
Tripping	Tripping	k1gInSc1	Tripping
the	the	k?	the
Live	Liv	k1gInSc2	Liv
Fantastic	Fantastice	k1gFnPc2	Fantastice
<g/>
:	:	kIx,	:
Highlights	Highlightsa	k1gFnPc2	Highlightsa
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
141	[number]	k4	141
Unplugged	Unplugged	k1gMnSc1	Unplugged
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Official	Official	k1gMnSc1	Official
Bootleg	Bootleg	k1gMnSc1	Bootleg
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
7	[number]	k4	7
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
14	[number]	k4	14
Paul	Paul	k1gMnSc1	Paul
Is	Is	k1gFnSc4	Is
Live	Liv	k1gFnSc2	Liv
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
34	[number]	k4	34
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
78	[number]	k4	78
Back	Back	k1gMnSc1	Back
in	in	k?	in
the	the	k?	the
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
8	[number]	k4	8
Back	Back	k1gMnSc1	Back
in	in	k?	in
the	the	k?	the
World	World	k1gInSc1	World
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
5	[number]	k4	5
Good	Good	k1gInSc1	Good
Evening	Evening	k1gInSc4	Evening
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
City	City	k1gFnSc2	City
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
All	All	k1gMnSc1	All
the	the	k?	the
Best	Best	k1gMnSc1	Best
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
62	[number]	k4	62
The	The	k1gMnSc1	The
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Never	Nevra	k1gFnPc2	Nevra
Stop	stop	k1gInSc1	stop
Doing	Doing	k1gMnSc1	Doing
What	What	k1gMnSc1	What
You	You	k1gMnSc1	You
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Pure	Pur	k1gInSc2	Pur
McCartney	McCartnea	k1gFnSc2	McCartnea
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gMnSc2	McCartnea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Liverpool	Liverpool	k1gInSc1	Liverpool
Oratorio	Oratorio	k1gMnSc1	Oratorio
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
177	[number]	k4	177
Paul	Paula	k1gFnPc2	Paula
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Standing	Standing	k1gInSc1	Standing
Stone	ston	k1gInSc5	ston
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
US	US	kA	US
#	#	kIx~	#
<g/>
194	[number]	k4	194
Paul	Paula	k1gFnPc2	Paula
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Working	Working	k1gInSc1	Working
Classical	Classical	k1gFnSc1	Classical
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Ecce	Ecc	k1gInSc2	Ecc
Cor	Cor	k1gMnSc1	Cor
Meum	Meum	k1gMnSc1	Meum
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Ocean	Oceana	k1gFnPc2	Oceana
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
s	s	k7c7	s
Kingdom	Kingdom	k1gInSc1	Kingdom
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Thrillington	Thrillington	k1gInSc1	Thrillington
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Strawberries	Strawberries	k1gInSc1	Strawberries
Oceans	Oceansa	k1gFnPc2	Oceansa
Ships	Ships	k1gInSc1	Ships
Forest	Forest	k1gMnSc1	Forest
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Rushes	Rushesa	k1gFnPc2	Rushesa
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Liverpool	Liverpool	k1gInSc1	Liverpool
Sound	Sounda	k1gFnPc2	Sounda
Collage	Collage	k1gFnPc2	Collage
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Twin	Twin	k1gNnSc1	Twin
Freaks	Freaksa	k1gFnPc2	Freaksa
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Electric	Electrice	k1gFnPc2	Electrice
Arguments	Arguments	k1gInSc1	Arguments
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
The	The	k1gFnPc6	The
Family	Famila	k1gFnSc2	Famila
Way	Way	k1gFnSc2	Way
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Give	Giv	k1gInSc2	Giv
My	my	k3xPp1nPc1	my
Regards	Regards	k1gInSc1	Regards
to	ten	k3xDgNnSc1	ten
Broad	Broad	k1gInSc4	Broad
Street	Street	k1gInSc1	Street
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
21	[number]	k4	21
Rupert	Rupert	k1gMnSc1	Rupert
and	and	k?	and
the	the	k?	the
Frog	Frog	k1gInSc1	Frog
Song	song	k1gInSc1	song
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Wings	Wingsa	k1gFnPc2	Wingsa
Wild	Wild	k1gInSc1	Wild
Life	Lif	k1gInSc2	Lif
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g />
.	.	kIx.	.
</s>
<s>
<g/>
11	[number]	k4	11
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
10	[number]	k4	10
Red	Red	k1gMnSc2	Red
Rose	Ros	k1gMnSc2	Ros
Speedway	Speedwaa	k1gMnSc2	Speedwaa
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
5	[number]	k4	5
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
Band	banda	k1gFnPc2	banda
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Run	run	k1gInSc1	run
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
Venus	Venus	k1gInSc1	Venus
and	and	k?	and
Mars	Mars	k1gInSc1	Mars
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
Wings	Wings	k1gInSc1	Wings
at	at	k?	at
the	the	k?	the
Speed	Speed	k1gMnSc1	Speed
of	of	k?	of
Sound	Sound	k1gMnSc1	Sound
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
London	London	k1gMnSc1	London
Town	Town	k1gMnSc1	Town
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
4	[number]	k4	4
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
Back	Back	k1gInSc1	Back
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Egg	Egg	k1gMnPc7	Egg
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
6	[number]	k4	6
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
8	[number]	k4	8
Band	banda	k1gFnPc2	banda
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Run	run	k1gInSc1	run
<g/>
:	:	kIx,	:
25	[number]	k4	25
<g/>
th	th	k?	th
Anniversary	Anniversar	k1gInPc1	Anniversar
Edition	Edition	k1gInSc1	Edition
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
69	[number]	k4	69
Wings	Wingsa	k1gFnPc2	Wingsa
over	over	k1gMnSc1	over
America	America	k1gMnSc1	America
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
8	[number]	k4	8
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
1	[number]	k4	1
Wings	Wings	k1gInSc1	Wings
Greatest	Greatest	k1gInSc4	Greatest
(	(	kIx(	(
<g/>
1	[number]	k4	1
December	December	k1gInSc1	December
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
5	[number]	k4	5
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
29	[number]	k4	29
Wingspan	Wingspan	k1gInSc1	Wingspan
<g/>
:	:	kIx,	:
Hits	Hits	k1gInSc1	Hits
and	and	k?	and
History	Histor	k1gMnPc7	Histor
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
UK	UK	kA	UK
#	#	kIx~	#
<g/>
5	[number]	k4	5
<g/>
;	;	kIx,	;
US	US	kA	US
#	#	kIx~	#
<g/>
2	[number]	k4	2
</s>
