<s>
Jistící	jistící	k2eAgInSc1d1	jistící
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
pevné	pevný	k2eAgNnSc4d1	pevné
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
nebo	nebo	k8xC	nebo
umělé	umělý	k2eAgFnSc3d1	umělá
lezecké	lezecký	k2eAgFnSc3d1	lezecká
stěně	stěna	k1gFnSc3	stěna
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
je	být	k5eAaImIp3nS	být
užíváno	užíván	k2eAgNnSc1d1	užíváno
v	v	k7c6	v
horolezectví	horolezectví	k1gNnSc6	horolezectví
<g/>
.	.	kIx.	.
</s>
