<s>
Jistící	jistící	k2eAgInSc1d1	jistící
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
pevné	pevný	k2eAgNnSc4d1	pevné
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
nebo	nebo	k8xC	nebo
umělé	umělý	k2eAgFnSc3d1	umělá
lezecké	lezecký	k2eAgFnSc3d1	lezecká
stěně	stěna	k1gFnSc3	stěna
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
je	být	k5eAaImIp3nS	být
užíváno	užíván	k2eAgNnSc1d1	užíváno
v	v	k7c6	v
horolezectví	horolezectví	k1gNnSc6	horolezectví
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
jistícího	jistící	k2eAgInSc2d1	jistící
bodu	bod	k1gInSc2	bod
jsou	být	k5eAaImIp3nP	být
erární	erární	k2eAgFnPc1d1	erární
a	a	k8xC	a
provizorní	provizorní	k2eAgFnPc1d1	provizorní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pevných	pevný	k2eAgFnPc6d1	pevná
horninách	hornina	k1gFnPc6	hornina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
bazalt	bazalt	k1gInSc4	bazalt
nebo	nebo	k8xC	nebo
znělec	znělec	k1gInSc4	znělec
a	a	k8xC	a
vápenec	vápenec	k1gInSc4	vápenec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
erární	erární	k2eAgInSc1d1	erární
bod	bod	k1gInSc1	bod
expanzivní	expanzivní	k2eAgInSc4d1	expanzivní
nýt	nýt	k1gInSc4	nýt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lepený	lepený	k2eAgInSc1d1	lepený
borhák	borhák	k1gInSc1	borhák
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
nýty	nýt	k1gInPc1	nýt
a	a	k8xC	a
borháky	borhák	k1gInPc1	borhák
jsou	být	k5eAaImIp3nP	být
certifikovány	certifikovat	k5eAaImNgInP	certifikovat
na	na	k7c6	na
22	[number]	k4	22
kN	kN	k?	kN
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
při	při	k7c6	při
naší	náš	k3xOp1gFnSc6	náš
gravitaci	gravitace	k1gFnSc6	gravitace
2200	[number]	k4	2200
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
měkkých	měkký	k2eAgFnPc6d1	měkká
horninách	hornina	k1gFnPc6	hornina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
pískovec	pískovec	k1gInSc1	pískovec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
jistící	jistící	k2eAgInSc1d1	jistící
bod	bod	k1gInSc1	bod
pískovcový	pískovcový	k2eAgInSc4d1	pískovcový
kruh	kruh	k1gInSc4	kruh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
unese	unést	k5eAaPmIp3nS	unést
až	až	k9	až
40	[number]	k4	40
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
každé	každý	k3xTgFnSc2	každý
jednodélkové	jednodélkový	k2eAgFnSc2d1	jednodélkový
cesty	cesta	k1gFnSc2	cesta
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
navíc	navíc	k6eAd1	navíc
slaňovací	slaňovací	k2eAgInSc4d1	slaňovací
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
návrat	návrat	k1gInSc4	návrat
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
dodržení	dodržení	k1gNnSc2	dodržení
jistých	jistý	k2eAgFnPc2d1	jistá
zásad	zásada	k1gFnPc2	zásada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
pískovcové	pískovcový	k2eAgFnSc2d1	pískovcová
skály	skála	k1gFnSc2	skála
se	se	k3xPyFc4	se
slaňovací	slaňovací	k2eAgInSc1d1	slaňovací
bod	bod	k1gInSc1	bod
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
spojením	spojení	k1gNnSc7	spojení
dvou	dva	k4xCgInPc2	dva
nýtů	nýt	k1gInPc2	nýt
(	(	kIx(	(
<g/>
borháků	borhák	k1gMnPc2	borhák
<g/>
)	)	kIx)	)
řetězem	řetěz	k1gInSc7	řetěz
a	a	k8xC	a
maillonami	maillona	k1gFnPc7	maillona
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
zde	zde	k6eAd1	zde
být	být	k5eAaImF	být
také	také	k9	také
erární	erární	k2eAgFnSc1d1	erární
karabina	karabina	k1gFnSc1	karabina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
provazování	provazování	k1gNnSc4	provazování
lana	lano	k1gNnSc2	lano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pískovcových	pískovcový	k2eAgFnPc6d1	pískovcová
skalách	skála	k1gFnPc6	skála
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
slaňovací	slaňovací	k2eAgInSc1d1	slaňovací
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zapuštěn	zapustit	k5eAaPmNgInS	zapustit
mnohem	mnohem	k6eAd1	mnohem
hlouběji	hluboko	k6eAd2	hluboko
než	než	k8xS	než
nýt	nýt	k1gInSc4	nýt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pevných	pevný	k2eAgFnPc6d1	pevná
horninách	hornina	k1gFnPc6	hornina
je	být	k5eAaImIp3nS	být
výběr	výběr	k1gInSc1	výběr
pestřejší	pestrý	k2eAgInSc1d2	pestřejší
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
osadit	osadit	k5eAaPmF	osadit
například	například	k6eAd1	například
skobu	skoba	k1gFnSc4	skoba
<g/>
,	,	kIx,	,
friend	friend	k1gMnSc1	friend
<g/>
,	,	kIx,	,
vklíněnec	vklíněnec	k1gMnSc1	vklíněnec
nebo	nebo	k8xC	nebo
hexcentrik	hexcentrik	k1gMnSc1	hexcentrik
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
erárních	erární	k2eAgInPc2d1	erární
bodů	bod	k1gInPc2	bod
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
lezení	lezení	k1gNnSc6	lezení
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
tyto	tento	k3xDgInPc1	tento
body	bod	k1gInPc1	bod
zruší	zrušit	k5eAaPmIp3nP	zrušit
a	a	k8xC	a
snesou	snést	k5eAaPmIp3nP	snést
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
výjimka	výjimka	k1gFnSc1	výjimka
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
skoba	skoba	k1gFnSc1	skoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pískovcích	pískovec	k1gInPc6	pískovec
ovšem	ovšem	k9	ovšem
takový	takový	k3xDgInSc1	takový
výběr	výběr	k1gInSc1	výběr
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
použít	použít	k5eAaPmF	použít
pouze	pouze	k6eAd1	pouze
smyčky	smyčka	k1gFnPc4	smyčka
založené	založený	k2eAgFnPc4d1	založená
např.	např.	kA	např.
jako	jako	k8xC	jako
vklíněný	vklíněný	k2eAgInSc4d1	vklíněný
uzel	uzel	k1gInSc4	uzel
<g/>
.	.	kIx.	.
</s>
