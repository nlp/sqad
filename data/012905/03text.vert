<p>
<s>
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
106	[number]	k4	106
km	km	kA	km
a	a	k8xC	a
plochou	plocha	k1gFnSc7	plocha
povodí	povodit	k5eAaPmIp3nS	povodit
867	[number]	k4	867
km2	km2	k4	km2
<g/>
,	,	kIx,	,
pramení	pramenit	k5eAaImIp3nS	pramenit
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Kameničky	kamenička	k1gFnSc2	kamenička
v	v	k7c6	v
krajinné	krajinný	k2eAgFnSc6d1	krajinná
oblasti	oblast	k1gFnSc6	oblast
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
v	v	k7c6	v
levostranném	levostranný	k2eAgInSc6d1	levostranný
přítoku	přítok	k1gInSc6	přítok
do	do	k7c2	do
evropské	evropský	k2eAgFnSc2d1	Evropská
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
ve	v	k7c6	v
Východolabské	Východolabský	k2eAgFnSc6d1	Východolabská
tabuli	tabule	k1gFnSc6	tabule
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
administrativně	administrativně	k6eAd1	administrativně
správním	správní	k2eAgMnSc7d1	správní
protéká	protékat	k5eAaImIp3nS	protékat
okresy	okres	k1gInPc4	okres
Chrudim	Chrudim	k1gFnSc1	Chrudim
a	a	k8xC	a
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
náležejícími	náležející	k2eAgInPc7d1	náležející
do	do	k7c2	do
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
Rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
prameniště	prameniště	k1gNnSc1	prameniště
s	s	k7c7	s
několika	několik	k4yIc7	několik
vodními	vodní	k2eAgInPc7d1	vodní
prameny	pramen	k1gInPc7	pramen
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
okrsku	okrsek	k1gInSc6	okrsek
Kameničská	Kameničský	k2eAgFnSc1d1	Kameničský
vrchovina	vrchovina	k1gFnSc1	vrchovina
s	s	k7c7	s
rozvodnicí	rozvodnice	k1gFnSc7	rozvodnice
na	na	k7c6	na
vyvýšeninách	vyvýšenina	k1gFnPc6	vyvýšenina
železnohorského	železnohorský	k2eAgInSc2d1	železnohorský
hřbetu	hřbet	k1gInSc2	hřbet
s	s	k7c7	s
vrcholy	vrchol	k1gInPc4	vrchol
Pešava	Pešava	k1gFnSc1	Pešava
(	(	kIx(	(
<g/>
697	[number]	k4	697
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dědovský	dědovský	k2eAgInSc1d1	dědovský
kopec	kopec	k1gInSc1	kopec
(	(	kIx(	(
<g/>
676	[number]	k4	676
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
oběšeného	oběšený	k2eAgInSc2d1	oběšený
(	(	kIx(	(
<g/>
737	[number]	k4	737
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
v	v	k7c6	v
Železných	železný	k2eAgFnPc6d1	železná
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
prameniště	prameniště	k1gNnSc2	prameniště
v	v	k7c6	v
dílčím	dílčí	k2eAgNnSc6d1	dílčí
povodí	povodí	k1gNnSc6	povodí
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
leží	ležet	k5eAaImIp3nS	ležet
hlavní	hlavní	k2eAgFnSc1d1	hlavní
pramenná	pramenný	k2eAgFnSc1d1	pramenná
zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
zhruba	zhruba	k6eAd1	zhruba
705	[number]	k4	705
m	m	kA	m
její	její	k3xOp3gInSc4	její
nejvýše	nejvýše	k6eAd1	nejvýše
položený	položený	k2eAgInSc4d1	položený
Filipovský	Filipovský	k2eAgInSc4d1	Filipovský
pramen	pramen	k1gInSc4	pramen
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
lesní	lesní	k2eAgFnSc2d1	lesní
lokality	lokalita	k1gFnSc2	lokalita
Humperky	Humperka	k1gFnSc2	Humperka
nad	nad	k7c7	nad
vesničkou	vesnička	k1gFnSc7	vesnička
Filipov	Filipovo	k1gNnPc2	Filipovo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
byl	být	k5eAaImAgInS	být
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
<g/>
.	.	kIx.	.
</s>
<s>
Filipovský	Filipovský	k2eAgInSc1d1	Filipovský
pramen	pramen	k1gInSc1	pramen
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Krouna	Krouna	k1gFnSc1	Krouna
<g/>
.	.	kIx.	.
</s>
<s>
Prameniště	prameniště	k1gNnSc1	prameniště
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
chráněného	chráněný	k2eAgNnSc2d1	chráněné
území	území	k1gNnSc2	území
přirozené	přirozený	k2eAgFnSc2d1	přirozená
akumulace	akumulace	k1gFnSc2	akumulace
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
Chráněné	chráněný	k2eAgFnSc6d1	chráněná
krajinné	krajinný	k2eAgFnSc6d1	krajinná
oblasti	oblast	k1gFnSc6	oblast
Žďárské	Žďárské	k2eAgInPc4d1	Žďárské
vrchy	vrch	k1gInPc4	vrch
<g/>
,	,	kIx,	,
protékané	protékaný	k2eAgInPc4d1	protékaný
řekou	řeka	k1gFnSc7	řeka
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
od	od	k7c2	od
Flipovského	Flipovský	k2eAgInSc2d1	Flipovský
pramene	pramen	k1gInSc2	pramen
do	do	k7c2	do
Trhové	trhový	k2eAgFnSc2d1	trhová
Kamenice	Kamenice	k1gFnSc2	Kamenice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
<g/>
,	,	kIx,	,
od	od	k7c2	od
Trhové	trhový	k2eAgFnSc2d1	trhová
Kamenice	Kamenice	k1gFnSc2	Kamenice
až	až	k9	až
po	po	k7c4	po
přehradní	přehradní	k2eAgFnSc4d1	přehradní
hráz	hráz	k1gFnSc4	hráz
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Práčov	Práčovo	k1gNnPc2	Práčovo
<g/>
,	,	kIx,	,
teče	téct	k5eAaImIp3nS	téct
Chráněnou	chráněný	k2eAgFnSc7d1	chráněná
krajinnou	krajinný	k2eAgFnSc7d1	krajinná
oblastí	oblast	k1gFnSc7	oblast
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
až	až	k9	až
do	do	k7c2	do
Chrudimi	Chrudim	k1gFnSc2	Chrudim
na	na	k7c6	na
území	území	k1gNnSc6	území
Národního	národní	k2eAgInSc2d1	národní
geoparku	geopark	k1gInSc2	geopark
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
s	s	k7c7	s
mnoha	mnoho	k4c2	mnoho
významnými	významný	k2eAgFnPc7d1	významná
geologickými	geologický	k2eAgFnPc7d1	geologická
lokalitami	lokalita	k1gFnPc7	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
tvoří	tvořit	k5eAaImIp3nS	tvořit
hydrogeografickou	hydrogeografický	k2eAgFnSc4d1	hydrogeografický
osu	osa	k1gFnSc4	osa
Sečské	sečský	k2eAgFnSc2d1	Sečská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gInSc7	její
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
vodním	vodní	k2eAgInSc7d1	vodní
tokem	tok	k1gInSc7	tok
s	s	k7c7	s
několika	několik	k4yIc7	několik
vodními	vodní	k2eAgInPc7d1	vodní
díly	díl	k1gInPc7	díl
<g/>
,	,	kIx,	,
vodohospodářským	vodohospodářský	k2eAgNnSc7d1	Vodohospodářské
a	a	k8xC	a
energetickým	energetický	k2eAgNnSc7d1	energetické
využitím	využití	k1gNnSc7	využití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
kamenitého	kamenitý	k2eAgNnSc2d1	kamenité
koryta	koryto	k1gNnSc2	koryto
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
byla	být	k5eAaImAgFnS	být
řeka	řeka	k1gFnSc1	řeka
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Kamenice	Kamenice	k1gFnSc1	Kamenice
a	a	k8xC	a
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
prudkým	prudký	k2eAgInSc7d1	prudký
ohybem	ohyb	k1gInSc7	ohyb
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Seč	seč	k6eAd1	seč
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
také	také	k9	také
Ohebka	Ohebka	k1gFnSc1	Ohebka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
pojmenování	pojmenování	k1gNnSc1	pojmenování
řeky	řeka	k1gFnSc2	řeka
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Chrudim	Chrudim	k1gFnSc4	Chrudim
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
protéká	protékat	k5eAaImIp3nS	protékat
v	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Chrudimské	chrudimský	k2eAgFnSc2d1	Chrudimská
tabule	tabule	k1gFnSc2	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
soutokem	soutok	k1gInSc7	soutok
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Novohradkou	Novohradka	k1gFnSc7	Novohradka
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
Pardubické	pardubický	k2eAgFnSc2d1	pardubická
kotliny	kotlina	k1gFnSc2	kotlina
a	a	k8xC	a
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Pardubice	Pardubice	k1gInPc4	Pardubice
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
216	[number]	k4	216
m	m	kA	m
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hydrogeografie	Hydrogeografie	k1gFnSc2	Hydrogeografie
==	==	k?	==
</s>
</p>
<p>
<s>
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
číslem	číslo	k1gNnSc7	číslo
hydrologického	hydrologický	k2eAgNnSc2d1	hydrologické
pořadí	pořadí	k1gNnSc2	pořadí
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
a	a	k8xC	a
malou	malý	k2eAgFnSc4d1	malá
oblast	oblast	k1gFnSc4	oblast
Východočeské	východočeský	k2eAgFnSc2d1	Východočeská
tabule	tabule	k1gFnSc2	tabule
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
činí	činit	k5eAaImIp3nS	činit
867,12	[number]	k4	867,12
km2	km2	k4	km2
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
celku	celek	k1gInSc6	celek
Železné	železný	k2eAgFnPc1d1	železná
hory	hora	k1gFnPc1	hora
tvoří	tvořit	k5eAaImIp3nP	tvořit
11,47	[number]	k4	11,47
km2	km2	k4	km2
plochu	plocha	k1gFnSc4	plocha
<g />
.	.	kIx.	.
</s>
<s>
dílčího	dílčí	k2eAgNnSc2d1	dílčí
povodí	povodí	k1gNnSc2	povodí
s	s	k7c7	s
prameništěm	prameniště	k1gNnSc7	prameniště
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
614	[number]	k4	614
<g/>
–	–	k?	–
<g/>
737	[number]	k4	737
m	m	kA	m
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
618	[number]	k4	618
<g/>
–	–	k?	–
<g/>
705	[number]	k4	705
m.	m.	k?	m.
<g/>
V	v	k7c6	v
nejvyšších	vysoký	k2eAgFnPc6d3	nejvyšší
polohách	poloha	k1gFnPc6	poloha
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
prameny	pramen	k1gInPc1	pramen
více	hodně	k6eAd2	hodně
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
v	v	k7c6	v
lesní	lesní	k2eAgFnSc6d1	lesní
lokalitě	lokalita	k1gFnSc6	lokalita
s	s	k7c7	s
názvem	název	k1gInSc7	název
Humperky	Humperka	k1gFnSc2	Humperka
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Krouna	Krouna	k1gFnSc1	Krouna
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
níže	nízce	k6eAd2	nízce
od	od	k7c2	od
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
vlhkých	vlhký	k2eAgFnPc2d1	vlhká
luk	louka	k1gFnPc2	louka
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
úbočí	úbočí	k1gNnSc6	úbočí
vrcholu	vrchol	k1gInSc2	vrchol
U	u	k7c2	u
oběšeného	oběšený	k2eAgInSc2d1	oběšený
(	(	kIx(	(
<g/>
737,4	[number]	k4	737,4
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
750	[number]	k4	750
m	m	kA	m
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
(	(	kIx(	(
<g/>
312	[number]	k4	312
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
704,68	[number]	k4	704,68
m	m	kA	m
u	u	k7c2	u
lesní	lesní	k2eAgFnSc2d1	lesní
cesty	cesta	k1gFnSc2	cesta
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
lesa	les	k1gInSc2	les
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
voda	voda	k1gFnSc1	voda
pramenné	pramenný	k2eAgFnSc2d1	pramenná
zdrojnice	zdrojnice	k1gFnSc2	zdrojnice
<g/>
,	,	kIx,	,
situačně	situačně	k6eAd1	situačně
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
nad	nad	k7c7	nad
sídelní	sídelní	k2eAgFnSc7d1	sídelní
lokalitou	lokalita	k1gFnSc7	lokalita
Filipov	Filipovo	k1gNnPc2	Filipovo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
byl	být	k5eAaImAgInS	být
vývěr	vývěr	k1gInSc1	vývěr
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
Filipovským	Filipovský	k2eAgInSc7d1	Filipovský
pramenem	pramen	k1gInSc7	pramen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
prameniště	prameniště	k1gNnSc4	prameniště
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
lze	lze	k6eAd1	lze
vymezit	vymezit	k5eAaPmF	vymezit
obcemi	obec	k1gFnPc7	obec
(	(	kIx(	(
<g/>
od	od	k7c2	od
severu	sever	k1gInSc2	sever
<g/>
)	)	kIx)	)
Dědová	Dědová	k1gFnSc1	Dědová
<g/>
,	,	kIx,	,
Svratouch	Svratouch	k1gInSc1	Svratouch
<g/>
,	,	kIx,	,
Chlumětín	Chlumětín	k1gInSc1	Chlumětín
<g/>
,	,	kIx,	,
Kameničky	Kameničky	k1gFnPc1	Kameničky
a	a	k8xC	a
Jeníkov	Jeníkov	k1gInSc1	Jeníkov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
melioracemi	meliorace	k1gFnPc7	meliorace
značně	značně	k6eAd1	značně
odvodněno	odvodnit	k5eAaPmNgNnS	odvodnit
<g/>
,	,	kIx,	,
toky	tok	k1gInPc1	tok
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
napřímeny	napřímen	k2eAgMnPc4d1	napřímen
a	a	k8xC	a
upraveny	upravit	k5eAaPmNgFnP	upravit
do	do	k7c2	do
lichoběžníkových	lichoběžníkový	k2eAgNnPc2d1	lichoběžníkové
koryt	koryto	k1gNnPc2	koryto
až	až	k9	až
po	po	k7c6	po
Chrudimkou	Chrudimka	k1gFnSc7	Chrudimka
průtočný	průtočný	k2eAgInSc1d1	průtočný
rybník	rybník	k1gInSc1	rybník
Groš	groš	k1gInSc1	groš
v	v	k7c6	v
Kameničkách	Kameničky	k1gFnPc6	Kameničky
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýše	nejvýše	k6eAd1	nejvýše
položené	položený	k2eAgFnPc1d1	položená
zdrojnice	zdrojnice	k1gFnPc1	zdrojnice
řeky	řeka	k1gFnSc2	řeka
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
vyvýšenin	vyvýšenina	k1gFnPc2	vyvýšenina
nad	nad	k7c7	nad
Kameničkami	Kameničky	k1gFnPc7	Kameničky
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
s	s	k7c7	s
vrcholy	vrchol	k1gInPc4	vrchol
Pešava	Pešava	k1gFnSc1	Pešava
(	(	kIx(	(
<g/>
697	[number]	k4	697
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dědovský	dědovský	k2eAgInSc1d1	dědovský
kopec	kopec	k1gInSc1	kopec
(	(	kIx(	(
<g/>
676	[number]	k4	676
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
U	u	k7c2	u
oběšeného	oběšený	k2eAgInSc2d1	oběšený
(	(	kIx(	(
<g/>
737	[number]	k4	737
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vydatnější	vydatný	k2eAgFnPc1d2	vydatnější
zdrojnice	zdrojnice	k1gFnPc1	zdrojnice
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Dědová	Dědová	k1gFnSc1	Dědová
v	v	k7c6	v
pramenné	pramenný	k2eAgFnSc6d1	pramenná
sníženině	sníženina	k1gFnSc6	sníženina
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
Bahna	bahno	k1gNnSc2	bahno
pod	pod	k7c7	pod
vyvýšeninou	vyvýšenina	k1gFnSc7	vyvýšenina
Na	na	k7c6	na
Bahnech	bahno	k1gNnPc6	bahno
(	(	kIx(	(
<g/>
663	[number]	k4	663
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc4d2	menší
vydatnost	vydatnost	k1gFnSc4	vydatnost
mají	mít	k5eAaImIp3nP	mít
zdrojnice	zdrojnice	k1gFnPc1	zdrojnice
pod	pod	k7c7	pod
vyvýšeninou	vyvýšenina	k1gFnSc7	vyvýšenina
Na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
(	(	kIx(	(
<g/>
681	[number]	k4	681
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
svodnice	svodnice	k1gFnSc1	svodnice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
vlhkými	vlhký	k2eAgFnPc7d1	vlhká
loukami	louka	k1gFnPc7	louka
vedena	vést	k5eAaImNgFnS	vést
meliorační	meliorační	k2eAgFnSc7d1	meliorační
trubkou	trubka	k1gFnSc7	trubka
<g/>
.	.	kIx.	.
</s>
<s>
Otevřené	otevřený	k2eAgNnSc1d1	otevřené
koryto	koryto	k1gNnSc1	koryto
s	s	k7c7	s
vodotečí	vodoteč	k1gFnSc7	vodoteč
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
až	až	k9	až
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
Dědová	Dědová	k1gFnSc1	Dědová
–	–	k?	–
Kameničky	kamenička	k1gFnSc2	kamenička
a	a	k8xC	a
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
pod	pod	k7c7	pod
Dědovou	dědův	k2eAgFnSc7d1	dědova
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
zaústěny	zaústěn	k2eAgFnPc1d1	zaústěn
další	další	k2eAgFnPc1d1	další
strouhy	strouha	k1gFnPc1	strouha
odvádějící	odvádějící	k2eAgFnSc4d1	odvádějící
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
vlhkých	vlhký	k2eAgFnPc2d1	vlhká
luk	louka	k1gFnPc2	louka
a	a	k8xC	a
lesního	lesní	k2eAgInSc2d1	lesní
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Napřímená	napřímený	k2eAgNnPc1d1	napřímené
koryta	koryto	k1gNnPc1	koryto
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
jsou	být	k5eAaImIp3nP	být
vedena	vést	k5eAaImNgNnP	vést
svahem	svah	k1gInSc7	svah
k	k	k7c3	k
Filipovu	Filipův	k2eAgInSc3d1	Filipův
<g/>
,	,	kIx,	,
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
s	s	k7c7	s
bystřinou	bystřina	k1gFnSc7	bystřina
přivádějící	přivádějící	k2eAgFnSc4d1	přivádějící
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
lesní	lesní	k2eAgFnSc2d1	lesní
lokality	lokalita	k1gFnSc2	lokalita
Humperky	Humperka	k1gFnSc2	Humperka
s	s	k7c7	s
pramennou	pramenný	k2eAgFnSc7d1	pramenná
zdrojnicí	zdrojnice	k1gFnSc7	zdrojnice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pramennou	pramenný	k2eAgFnSc4d1	pramenná
zdrojnici	zdrojnice	k1gFnSc4	zdrojnice
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považována	považován	k2eAgFnSc1d1	považována
zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
nejvodnatější	vodnatý	k2eAgFnSc1d3	nejvodnatější
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
a	a	k8xC	a
s	s	k7c7	s
pramenem	pramen	k1gInSc7	pramen
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
soutokem	soutok	k1gInSc7	soutok
přibírá	přibírat	k5eAaImIp3nS	přibírat
pramenná	pramenný	k2eAgFnSc1d1	pramenná
zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
pod	pod	k7c7	pod
lesní	lesní	k2eAgFnSc7d1	lesní
lokalitou	lokalita	k1gFnSc7	lokalita
Humperky	Humperka	k1gFnSc2	Humperka
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
levostranného	levostranný	k2eAgInSc2d1	levostranný
přítoku	přítok	k1gInSc2	přítok
méně	málo	k6eAd2	málo
vydatné	vydatný	k2eAgFnPc1d1	vydatná
vodoteče	vodoteč	k1gFnPc1	vodoteč
s	s	k7c7	s
pramenem	pramen	k1gInSc7	pramen
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
Paseky	paseka	k1gFnSc2	paseka
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Chlumětín	Chlumětína	k1gFnPc2	Chlumětína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pramen	pramen	k1gInSc1	pramen
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Na	na	k7c6	na
Pasekách	paseka	k1gFnPc6	paseka
je	být	k5eAaImIp3nS	být
sveden	svést	k5eAaPmNgInS	svést
trubkou	trubka	k1gFnSc7	trubka
do	do	k7c2	do
malého	malý	k2eAgInSc2d1	malý
rybníčku	rybníček	k1gInSc2	rybníček
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
704	[number]	k4	704
m	m	kA	m
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
540	[number]	k4	540
m	m	kA	m
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
od	od	k7c2	od
Filipovského	Filipovský	k2eAgInSc2d1	Filipovský
pramene	pramen	k1gInSc2	pramen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soutokem	soutok	k1gInSc7	soutok
bystřin	bystřina	k1gFnPc2	bystřina
mezi	mezi	k7c7	mezi
Filipovem	Filipovo	k1gNnSc7	Filipovo
a	a	k8xC	a
Kameničkami	Kameničky	k1gFnPc7	Kameničky
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
500	[number]	k4	500
m	m	kA	m
nad	nad	k7c7	nad
monitorovací	monitorovací	k2eAgFnSc7d1	monitorovací
stanicí	stanice	k1gFnSc7	stanice
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vod	voda	k1gFnPc2	voda
(	(	kIx(	(
<g/>
PLA	PLA	kA	PLA
338	[number]	k4	338
<g/>
)	)	kIx)	)
Českého	český	k2eAgInSc2d1	český
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
104,2	[number]	k4	104,2
v	v	k7c6	v
profilu	profil	k1gInSc6	profil
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
průtokem	průtok	k1gInSc7	průtok
vydatnější	vydatný	k2eAgInSc4d2	vydatnější
potok	potok	k1gInSc4	potok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
Kameničkami	Kameničky	k1gFnPc7	Kameničky
přibírá	přibírat	k5eAaImIp3nS	přibírat
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pravostranného	pravostranný	k2eAgInSc2d1	pravostranný
přítoku	přítok	k1gInSc2	přítok
–	–	k?	–
Jeníkovského	Jeníkovský	k2eAgInSc2d1	Jeníkovský
potoku	potok	k1gInSc6	potok
s	s	k7c7	s
pramenem	pramen	k1gInSc7	pramen
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
Pešavy	Pešava	k1gFnSc2	Pešava
(	(	kIx(	(
<g/>
697	[number]	k4	697
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
levostranného	levostranný	k2eAgInSc2d1	levostranný
přítoku	přítok	k1gInSc2	přítok
bezejmenné	bezejmenný	k2eAgFnSc2d1	bezejmenná
vodoteče	vodoteč	k1gFnSc2	vodoteč
<g/>
,	,	kIx,	,
odvádějící	odvádějící	k2eAgFnSc4d1	odvádějící
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
mokřadů	mokřad	k1gInPc2	mokřad
a	a	k8xC	a
vlhkých	vlhký	k2eAgFnPc2d1	vlhká
luk	louka	k1gFnPc2	louka
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Volákův	Volákův	k2eAgInSc1d1	Volákův
kopec	kopec	k1gInSc1	kopec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Kameniček	Kameničky	k1gFnPc2	Kameničky
na	na	k7c6	na
Chrudimce	Chrudimka	k1gFnSc6	Chrudimka
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
rybník	rybník	k1gInSc1	rybník
Groš	groš	k1gInSc1	groš
<g/>
,	,	kIx,	,
výtok	výtok	k1gInSc1	výtok
řeky	řeka	k1gFnSc2	řeka
na	na	k7c6	na
přelivu	přeliv	k1gInSc6	přeliv
rybníku	rybník	k1gInSc6	rybník
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
vrstevnicové	vrstevnicový	k2eAgFnSc2d1	vrstevnicová
mapy	mapa	k1gFnSc2	mapa
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
618	[number]	k4	618
m	m	kA	m
nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
orografické	orografický	k2eAgFnSc2d1	orografická
rozvodnice	rozvodnice	k1gFnSc2	rozvodnice
<g/>
,	,	kIx,	,
vztaženým	vztažený	k2eAgInSc7d1	vztažený
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
dílčím	dílčí	k2eAgNnSc6d1	dílčí
povodí	povodí	k1gNnSc6	povodí
s	s	k7c7	s
prameništěm	prameniště	k1gNnSc7	prameniště
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
prameniště	prameniště	k1gNnSc2	prameniště
a	a	k8xC	a
orografické	orografický	k2eAgFnSc2d1	orografická
rozvodnice	rozvodnice	k1gFnSc2	rozvodnice
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc4	vrchol
U	u	k7c2	u
oběšeného	oběšený	k2eAgInSc2d1	oběšený
(	(	kIx(	(
<g/>
737,4	[number]	k4	737,4
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
Svratouch	Svratoucha	k1gFnPc2	Svratoucha
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
rozhraní	rozhraní	k1gNnSc2	rozhraní
Sečské	sečský	k2eAgFnSc2d1	Sečská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
a	a	k8xC	a
Žďárských	Žďárských	k2eAgInPc2d1	Žďárských
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrcholovou	vrcholový	k2eAgFnSc7d1	vrcholová
částí	část	k1gFnSc7	část
U	u	k7c2	u
oběšeného	oběšený	k2eAgNnSc2d1	oběšené
prochází	procházet	k5eAaImIp3nS	procházet
hlavní	hlavní	k2eAgFnSc1d1	hlavní
rozvodnice	rozvodnice	k1gFnSc1	rozvodnice
mezi	mezi	k7c7	mezi
evropskými	evropský	k2eAgFnPc7d1	Evropská
řekami	řeka	k1gFnPc7	řeka
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
tvoří	tvořit	k5eAaImIp3nS	tvořit
vrchol	vrchol	k1gInSc4	vrchol
rozhraní	rozhraní	k1gNnSc2	rozhraní
mezi	mezi	k7c7	mezi
orografickými	orografický	k2eAgFnPc7d1	orografická
rozvodnicemi	rozvodnice	k1gFnPc7	rozvodnice
dílčího	dílčí	k2eAgInSc2d1	dílčí
povodí	povodí	k1gNnPc1	povodí
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnSc2	řeka
Krounky	Krounka	k1gFnSc2	Krounka
a	a	k8xC	a
Chlumětínského	Chlumětínský	k2eAgInSc2d1	Chlumětínský
potoku	potok	k1gInSc6	potok
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
evropské	evropský	k2eAgFnSc2d1	Evropská
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
a	a	k8xC	a
potoku	potok	k1gInSc6	potok
Brodek	brodek	k1gInSc1	brodek
v	v	k7c6	v
dílčím	dílčí	k2eAgNnSc6d1	dílčí
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
a	a	k8xC	a
evropské	evropský	k2eAgFnSc2d1	Evropská
řeky	řeka	k1gFnSc2	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dílčí	dílčí	k2eAgNnPc1d1	dílčí
povodí	povodí	k1gNnPc1	povodí
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
U	u	k7c2	u
oběšeného	oběšený	k2eAgInSc2d1	oběšený
po	po	k7c6	po
ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
750	[number]	k4	750
m	m	kA	m
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
,	,	kIx,	,
než	než	k8xS	než
vlastní	vlastní	k2eAgFnSc1d1	vlastní
délka	délka	k1gFnSc1	délka
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
od	od	k7c2	od
Filipovského	Filipovský	k2eAgInSc2d1	Filipovský
pramene	pramen	k1gInSc2	pramen
(	(	kIx(	(
<g/>
105,97	[number]	k4	105,97
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
je	být	k5eAaImIp3nS	být
prameniště	prameniště	k1gNnSc4	prameniště
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Kameniček	Kameničky	k1gFnPc2	Kameničky
součástí	součást	k1gFnPc2	součást
chráněné	chráněný	k2eAgFnSc3d1	chráněná
oblasti	oblast	k1gFnSc3	oblast
přirozené	přirozený	k2eAgFnSc2d1	přirozená
akumulace	akumulace	k1gFnSc2	akumulace
vod	voda	k1gFnPc2	voda
(	(	kIx(	(
<g/>
CHOPAV	CHOPAV	kA	CHOPAV
<g/>
)	)	kIx)	)
s	s	k7c7	s
názvem	název	k1gInSc7	název
Žďárské	Žďárské	k2eAgInPc4d1	Žďárské
vrchy	vrch	k1gInPc4	vrch
<g/>
,	,	kIx,	,
s	s	k7c7	s
více	hodně	k6eAd2	hodně
zdrojnicemi	zdrojnice	k1gFnPc7	zdrojnice
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
prameny	pramen	k1gInPc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
CHOPAV	CHOPAV	kA	CHOPAV
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
nařízení	nařízení	k1gNnSc2	nařízení
vlády	vláda	k1gFnSc2	vláda
ČSR	ČSR	kA	ČSR
č.	č.	k?	č.
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
1978	[number]	k4	1978
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
vymezeno	vymezit	k5eAaPmNgNnS	vymezit
shodně	shodně	k6eAd1	shodně
s	s	k7c7	s
Chráněnou	chráněný	k2eAgFnSc7d1	chráněná
krajinnou	krajinný	k2eAgFnSc7d1	krajinná
oblastí	oblast	k1gFnSc7	oblast
Žďárské	Žďárské	k2eAgInPc1d1	Žďárské
vrchy	vrch	k1gInPc1	vrch
<g/>
.	.	kIx.	.
<g/>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
dílčího	dílčí	k2eAgNnSc2d1	dílčí
povodí	povodí	k1gNnSc2	povodí
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
je	být	k5eAaImIp3nS	být
situovaný	situovaný	k2eAgInSc1d1	situovaný
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgNnSc4d1	hlavní
koryto	koryto	k1gNnSc4	koryto
řeky	řeka	k1gFnSc2	řeka
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
s	s	k7c7	s
povodím	povodí	k1gNnSc7	povodí
Chlumětínského	Chlumětínský	k2eAgInSc2d1	Chlumětínský
potoku	potok	k1gInSc3	potok
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
V	v	k7c6	v
ohradách	ohrada	k1gFnPc6	ohrada
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
614	[number]	k4	614
m	m	kA	m
dle	dle	k7c2	dle
vrstevnicové	vrstevnicový	k2eAgFnSc2d1	vrstevnicová
mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
V	v	k7c6	v
ohradách	ohrada	k1gFnPc6	ohrada
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
vodoteče	vodoteč	k1gFnSc2	vodoteč
z	z	k7c2	z
odtoku	odtok	k1gInSc2	odtok
pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
rybníku	rybník	k1gInSc3	rybník
Groš	groš	k1gInSc1	groš
zaústěna	zaústěn	k2eAgFnSc1d1	zaústěn
levostranná	levostranný	k2eAgFnSc1d1	levostranná
svodnice	svodnice	k1gFnSc1	svodnice
odvádějící	odvádějící	k2eAgFnSc4d1	odvádějící
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
mokřadů	mokřad	k1gInPc2	mokřad
<g/>
.	.	kIx.	.
<g/>
Chlumětínský	Chlumětínský	k2eAgInSc1d1	Chlumětínský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
uváděný	uváděný	k2eAgInSc1d1	uváděný
jako	jako	k8xC	jako
zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
700	[number]	k4	700
m	m	kA	m
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
U	u	k7c2	u
oběšeného	oběšený	k2eAgNnSc2d1	oběšené
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
okraji	okraj	k1gInSc6	okraj
Kameniček	Kameničky	k1gFnPc2	Kameničky
je	být	k5eAaImIp3nS	být
zaústěn	zaústit	k5eAaPmNgInS	zaústit
strouhou	strouha	k1gFnSc7	strouha
do	do	k7c2	do
rybníku	rybník	k1gInSc6	rybník
Groš	groš	k1gInSc1	groš
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
základní	základní	k2eAgFnSc2d1	základní
mapy	mapa	k1gFnSc2	mapa
Česka	Česko	k1gNnSc2	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průtok	průtok	k1gInSc1	průtok
k	k	k7c3	k
rybníku	rybník	k1gInSc3	rybník
Groš	groš	k1gInSc1	groš
je	být	k5eAaImIp3nS	být
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
výškou	výška	k1gFnSc7	výška
hladiny	hladina	k1gFnSc2	hladina
nedalekého	daleký	k2eNgInSc2d1	nedaleký
rybníku	rybník	k1gInSc6	rybník
Krejcar	krejcar	k1gInSc4	krejcar
<g/>
,	,	kIx,	,
napájeného	napájený	k2eAgNnSc2d1	napájené
vodou	voda	k1gFnSc7	voda
Chlumětínského	Chlumětínský	k2eAgMnSc2d1	Chlumětínský
potoku	potok	k1gInSc3	potok
<g/>
.	.	kIx.	.
</s>
<s>
Vydatnější	vydatný	k2eAgInSc1d2	vydatnější
je	být	k5eAaImIp3nS	být
průtok	průtok	k1gInSc1	průtok
potoku	potok	k1gInSc3	potok
do	do	k7c2	do
rybníku	rybník	k1gInSc3	rybník
Krejcar	krejcar	k1gInSc4	krejcar
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
obtokového	obtokový	k2eAgNnSc2d1	obtokové
koryta	koryto	k1gNnSc2	koryto
vedeného	vedený	k2eAgInSc2d1	vedený
lesní	lesní	k2eAgMnSc1d1	lesní
lokalitou	lokalita	k1gFnSc7	lokalita
Ohrady	ohrada	k1gFnSc2	ohrada
se	s	k7c7	s
zaústěním	zaústění	k1gNnSc7	zaústění
do	do	k7c2	do
Krejcarského	Krejcarský	k2eAgInSc2d1	Krejcarský
potoku	potok	k1gInSc6	potok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
Krejcarským	Krejcarský	k2eAgInSc7d1	Krejcarský
potokem	potok	k1gInSc7	potok
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
evidence	evidence	k1gFnSc2	evidence
rozvodnic	rozvodnice	k1gFnPc2	rozvodnice
Českého	český	k2eAgInSc2d1	český
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
uváděno	uváděn	k2eAgNnSc1d1	uváděno
dílčí	dílčí	k2eAgNnSc1d1	dílčí
povodí	povodí	k1gNnSc1	povodí
Chlumětínského	Chlumětínský	k2eAgInSc2d1	Chlumětínský
potoku	potok	k1gInSc3	potok
<g/>
.	.	kIx.	.
</s>
<s>
Recipient	recipient	k1gMnSc1	recipient
neboli	neboli	k8xC	neboli
příjemce	příjemce	k1gMnSc1	příjemce
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dílčím	dílčí	k2eAgNnSc6d1	dílčí
povodí	povodí	k1gNnSc6	povodí
Chlumětínský	Chlumětínský	k2eAgInSc4d1	Chlumětínský
potok	potok	k1gInSc4	potok
s	s	k7c7	s
vydatnějším	vydatný	k2eAgInSc7d2	vydatnější
průtokem	průtok	k1gInSc7	průtok
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
prvním	první	k4xOgInSc7	první
větším	veliký	k2eAgInSc7d2	veliký
levostranným	levostranný	k2eAgInSc7d1	levostranný
přítokem	přítok	k1gInSc7	přítok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
luční	luční	k2eAgFnSc2d1	luční
nivy	niva	k1gFnSc2	niva
s	s	k7c7	s
mokřady	mokřad	k1gInPc7	mokřad
pod	pod	k7c7	pod
Kameničkami	Kameničky	k1gFnPc7	Kameničky
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
viz	vidět	k5eAaImRp2nS	vidět
Větší	veliký	k2eAgInPc1d2	veliký
přítoky	přítok	k1gInPc7	přítok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
Filipovského	Filipovský	k2eAgInSc2d1	Filipovský
pramene	pramen	k1gInSc2	pramen
je	být	k5eAaImIp3nS	být
ústí	ústí	k1gNnSc4	ústí
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
přibližně	přibližně	k6eAd1	přibližně
38	[number]	k4	38
km	km	kA	km
severo-severozápadním	severoeverozápadní	k2eAgInSc7d1	severo-severozápadní
směrem	směr	k1gInSc7	směr
(	(	kIx(	(
<g/>
334	[number]	k4	334
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hydrogeografická	Hydrogeografický	k2eAgFnSc1d1	Hydrogeografický
osa	osa	k1gFnSc1	osa
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
toku	tok	k1gInSc2	tok
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
směrech	směr	k1gInPc6	směr
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
vlastností	vlastnost	k1gFnPc2	vlastnost
hornin	hornina	k1gFnPc2	hornina
tvořících	tvořící	k2eAgInPc2d1	tvořící
geologické	geologický	k2eAgNnSc4d1	geologické
podloží	podloží	k1gNnSc4	podloží
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
hydrologie	hydrologie	k1gFnSc2	hydrologie
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tři	tři	k4xCgNnPc1	tři
územně	územně	k6eAd1	územně
vymezené	vymezený	k2eAgFnSc2d1	vymezená
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
hydrogeologické	hydrogeologický	k2eAgInPc1d1	hydrogeologický
rajóny	rajón	k1gInPc1	rajón
s	s	k7c7	s
názvy	název	k1gInPc7	název
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Krystalinikum	krystalinikum	k1gNnSc1	krystalinikum
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Filipovský	Filipovský	k2eAgInSc4d1	Filipovský
pramen	pramen	k1gInSc4	pramen
až	až	k6eAd1	až
Škrovád	Škrováda	k1gFnPc2	Škrováda
(	(	kIx(	(
<g/>
horní	horní	k2eAgInSc4d1	horní
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
tok	tok	k1gInSc4	tok
v	v	k7c6	v
Sečské	sečský	k2eAgFnSc6d1	Sečská
vrchovině	vrchovina	k1gFnSc6	vrchovina
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chrudimská	chrudimský	k2eAgFnSc1d1	Chrudimská
křída	křída	k1gFnSc1	křída
mezi	mezi	k7c7	mezi
Slatiňany	Slatiňany	k1gInPc7	Slatiňany
a	a	k8xC	a
Tuněchody	Tuněchod	k1gInPc7	Tuněchod
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
v	v	k7c6	v
Chrudimské	chrudimský	k2eAgFnSc6d1	Chrudimská
tabuli	tabule	k1gFnSc6	tabule
Svitavské	svitavský	k2eAgFnSc2d1	Svitavská
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvartér	kvartér	k1gInSc1	kvartér
Loučné	Loučný	k2eAgInPc1d1	Loučný
a	a	k8xC	a
Chrudimky	Chrudimek	k1gInPc1	Chrudimek
od	od	k7c2	od
Tuněchod	Tuněchod	k1gInSc1	Tuněchod
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
(	(	kIx(	(
<g/>
část	část	k1gFnSc4	část
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Pardubic	Pardubice	k1gInPc2	Pardubice
v	v	k7c6	v
Pardubické	pardubický	k2eAgFnSc6d1	pardubická
kotlině	kotlina	k1gFnSc6	kotlina
Východolabské	Východolabský	k2eAgFnSc2d1	Východolabská
tabule	tabule	k1gFnSc2	tabule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
105,97	[number]	k4	105,97
km	km	kA	km
nemá	mít	k5eNaImIp3nS	mít
spádovou	spádový	k2eAgFnSc4d1	spádová
křivku	křivka	k1gFnSc4	křivka
ideálně	ideálně	k6eAd1	ideálně
vyrovnanou	vyrovnaný	k2eAgFnSc4d1	vyrovnaná
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
toku	tok	k1gInSc2	tok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
obecně	obecně	k6eAd1	obecně
úseky	úsek	k1gInPc1	úsek
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
rychlostí	rychlost	k1gFnSc7	rychlost
proudění	proudění	k1gNnSc2	proudění
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
intenzitou	intenzita	k1gFnSc7	intenzita
erozní	erozní	k2eAgFnSc2d1	erozní
schopnosti	schopnost	k1gFnSc2	schopnost
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
geologickým	geologický	k2eAgNnSc7d1	geologické
podložím	podloží	k1gNnSc7	podloží
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
složitým	složitý	k2eAgInSc7d1	složitý
v	v	k7c6	v
krajinné	krajinný	k2eAgFnSc6d1	krajinná
oblasti	oblast	k1gFnSc6	oblast
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
horního	horní	k2eAgInSc2d1	horní
a	a	k8xC	a
středního	střední	k2eAgInSc2d1	střední
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průtoku	průtok	k1gInSc2	průtok
Sečskou	sečský	k2eAgFnSc7d1	Sečská
vrchovinou	vrchovina	k1gFnSc7	vrchovina
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
od	od	k7c2	od
Filipovského	Filipovský	k2eAgInSc2d1	Filipovský
pramene	pramen	k1gInSc2	pramen
po	po	k7c4	po
vodní	vodní	k2eAgFnSc4d1	vodní
nádrž	nádrž	k1gFnSc4	nádrž
Seč	seč	k6eAd1	seč
u	u	k7c2	u
Horní	horní	k2eAgFnSc2d1	horní
Vsi	ves	k1gFnSc2	ves
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
města	město	k1gNnSc2	město
Seč	seč	k1gFnSc1	seč
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hydrogeografická	hydrogeografický	k2eAgFnSc1d1	hydrogeografický
osa	osa	k1gFnSc1	osa
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
orientována	orientován	k2eAgFnSc1d1	orientována
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
–	–	k?	–
severozápad	severozápad	k1gInSc1	severozápad
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
malých	malý	k2eAgFnPc2d1	malá
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
meandrech	meandr	k1gInPc6	meandr
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
výrazných	výrazný	k2eAgInPc2d1	výrazný
oblouků	oblouk	k1gInPc2	oblouk
s	s	k7c7	s
korytem	koryto	k1gNnSc7	koryto
řeky	řeka	k1gFnSc2	řeka
vyhloubeném	vyhloubený	k2eAgInSc6d1	vyhloubený
kolem	kolem	k7c2	kolem
vyvýšenin	vyvýšenina	k1gFnPc2	vyvýšenina
mezi	mezi	k7c7	mezi
Kameničkami	Kameničky	k1gFnPc7	Kameničky
<g/>
,	,	kIx,	,
Hamry	Hamry	k1gInPc7	Hamry
a	a	k8xC	a
Vítanovem	Vítanovo	k1gNnSc7	Vítanovo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Trhové	trhový	k2eAgFnSc2d1	trhová
Kamenice	Kamenice	k1gFnSc2	Kamenice
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Seč	seč	k6eAd1	seč
se	se	k3xPyFc4	se
koryto	koryto	k1gNnSc1	koryto
řeky	řeka	k1gFnSc2	řeka
mírně	mírně	k6eAd1	mírně
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
k	k	k7c3	k
jihozápadnímu	jihozápadní	k2eAgInSc3d1	jihozápadní
hřbetu	hřbet	k1gInSc3	hřbet
Železných	Železný	k1gMnPc2	Železný
hor.	hor.	k?	hor.
</s>
</p>
<p>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zdvižením	zdvižení	k1gNnSc7	zdvižení
zemské	zemský	k2eAgFnSc2d1	zemská
klínové	klínový	k2eAgFnSc2d1	klínová
kry	kra	k1gFnSc2	kra
geomorfologického	geomorfologický	k2eAgInSc2d1	geomorfologický
celku	celek	k1gInSc2	celek
v	v	k7c6	v
období	období	k1gNnSc6	období
kenozoika	kenozoikum	k1gNnSc2	kenozoikum
nad	nad	k7c7	nad
železnohorským	železnohorský	k2eAgInSc7d1	železnohorský
tektonickým	tektonický	k2eAgInSc7d1	tektonický
zlomem	zlom	k1gInSc7	zlom
(	(	kIx(	(
<g/>
Ždírec	Ždírec	k1gInSc1	Ždírec
nad	nad	k7c7	nad
Doubravou	Doubrava	k1gMnSc7	Doubrava
–	–	k?	–
Týnec	Týnec	k1gInSc1	Týnec
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
úklonu	úklon	k1gInSc2	úklon
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
jihozápad	jihozápad	k1gInSc1	jihozápad
–	–	k?	–
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
vodní	vodní	k2eAgInSc1d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
Seč	seč	k1gFnSc1	seč
prudce	prudko	k6eAd1	prudko
směr	směr	k1gInSc4	směr
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
s	s	k7c7	s
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
Oheb	Oheba	k1gFnPc2	Oheba
a	a	k8xC	a
skalním	skalní	k2eAgInSc7d1	skalní
ostrohem	ostroh	k1gInSc7	ostroh
se	s	k7c7	s
zříceninou	zřícenina	k1gFnSc7	zřícenina
hradu	hrad	k1gInSc2	hrad
Oheb	Oheb	k1gMnSc1	Oheb
nad	nad	k7c7	nad
Chrudimkou	Chrudimka	k1gFnSc7	Chrudimka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
od	od	k7c2	od
Sečské	sečský	k2eAgFnSc2d1	Sečská
přehrady	přehrada	k1gFnSc2	přehrada
proráží	prorážet	k5eAaImIp3nS	prorážet
horniny	hornina	k1gFnSc2	hornina
železnohorského	železnohorský	k2eAgInSc2d1	železnohorský
plutonu	pluton	k1gInSc2	pluton
v	v	k7c6	v
hlubokých	hluboký	k2eAgNnPc6d1	hluboké
říčních	říční	k2eAgNnPc6d1	říční
údolích	údolí	k1gNnPc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	Řek	k1gMnSc4	Řek
výraznou	výrazný	k2eAgFnSc7d1	výrazná
měrou	míra	k1gFnSc7wR	míra
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
reliéf	reliéf	k1gInSc4	reliéf
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
úsecích	úsek	k1gInPc6	úsek
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
příčný	příčný	k2eAgInSc1d1	příčný
profil	profil	k1gInSc1	profil
typu	typ	k1gInSc2	typ
soutěsky	soutěska	k1gFnSc2	soutěska
(	(	kIx(	(
<g/>
využit	využít	k5eAaPmNgInS	využít
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
přehradní	přehradní	k2eAgFnSc2d1	přehradní
hráze	hráz	k1gFnSc2	hráz
Seč	seč	k6eAd1	seč
<g/>
,	,	kIx,	,
pohledově	pohledově	k6eAd1	pohledově
výrazný	výrazný	k2eAgInSc1d1	výrazný
například	například	k6eAd1	například
v	v	k7c6	v
přírodních	přírodní	k2eAgFnPc6d1	přírodní
rezervacích	rezervace	k1gFnPc6	rezervace
Krkanka	Krkanka	k1gFnSc1	Krkanka
a	a	k8xC	a
Strádovské	Strádovský	k2eAgNnSc1d1	Strádovské
Peklo	peklo	k1gNnSc1	peklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlubokém	hluboký	k2eAgNnSc6d1	hluboké
údolí	údolí	k1gNnSc6	údolí
pod	pod	k7c7	pod
Nasavrky	Nasavrek	k1gMnPc7	Nasavrek
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
řeka	řeka	k1gFnSc1	řeka
k	k	k7c3	k
severu	sever	k1gInSc3	sever
a	a	k8xC	a
u	u	k7c2	u
Slatiňan	Slatiňany	k1gInPc2	Slatiňany
opouští	opouštět	k5eAaImIp3nS	opouštět
krajinnou	krajinný	k2eAgFnSc4d1	krajinná
oblast	oblast	k1gFnSc4	oblast
Sečské	sečský	k2eAgFnSc2d1	Sečská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
(	(	kIx(	(
<g/>
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Chrudimskou	chrudimský	k2eAgFnSc7d1	Chrudimská
tabulí	tabule	k1gFnSc7	tabule
(	(	kIx(	(
<g/>
Svitavskou	svitavský	k2eAgFnSc7d1	Svitavská
pahorkatinou	pahorkatina	k1gFnSc7	pahorkatina
<g/>
)	)	kIx)	)
od	od	k7c2	od
Slatiňan	Slatiňany	k1gInPc2	Slatiňany
teče	téct	k5eAaImIp3nS	téct
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
od	od	k7c2	od
Chrudimi	Chrudim	k1gFnSc2	Chrudim
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
<g/>
,	,	kIx,	,
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
s	s	k7c7	s
Novohradkou	Novohradka	k1gFnSc7	Novohradka
pod	pod	k7c7	pod
Úhřetickou	Úhřetický	k2eAgFnSc7d1	Úhřetická
Lhotou	Lhota	k1gFnSc7	Lhota
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Chrudimka	Chrudimek	k1gMnSc2	Chrudimek
<g/>
)	)	kIx)	)
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Chrudimské	chrudimský	k2eAgFnSc2d1	Chrudimská
tabule	tabule	k1gFnSc2	tabule
tzv.	tzv.	kA	tzv.
načepován	načepovat	k5eAaPmNgMnS	načepovat
jiným	jiný	k2eAgMnSc7d1	jiný
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Novohradka	Novohradek	k1gMnSc2	Novohradek
<g/>
)	)	kIx)	)
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
erozivní	erozivní	k2eAgFnSc7d1	erozivní
činností	činnost	k1gFnSc7	činnost
(	(	kIx(	(
<g/>
úkaz	úkaz	k1gInSc1	úkaz
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
pirátství	pirátství	k1gNnSc4	pirátství
říčního	říční	k2eAgInSc2d1	říční
toku	tok	k1gInSc2	tok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Novohradka	Novohradka	k1gFnSc1	Novohradka
pravostranným	pravostranný	k2eAgMnSc7d1	pravostranný
a	a	k8xC	a
největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
toku	tok	k1gInSc2	tok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
Novohradkou	Novohradka	k1gFnSc7	Novohradka
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
severozápadu	severozápad	k1gInSc3	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Pardubickou	pardubický	k2eAgFnSc7d1	pardubická
kotlinou	kotlina	k1gFnSc7	kotlina
protéká	protékat	k5eAaImIp3nS	protékat
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
úseku	úsek	k1gInSc6	úsek
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
v	v	k7c6	v
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
okrsku	okrsek	k1gInSc6	okrsek
Východolabská	Východolabský	k2eAgFnSc1d1	Východolabská
niva	niva	k1gFnSc1	niva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
protéká	protékat	k5eAaImIp3nS	protékat
městy	město	k1gNnPc7	město
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
<g/>
,	,	kIx,	,
Slatiňany	Slatiňany	k1gInPc1	Slatiňany
<g/>
,	,	kIx,	,
Chrudim	Chrudim	k1gFnSc1	Chrudim
a	a	k8xC	a
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
administrativně	administrativně	k6eAd1	administrativně
správního	správní	k2eAgMnSc2d1	správní
tvoří	tvořit	k5eAaImIp3nS	tvořit
osa	osa	k1gFnSc1	osa
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
úseku	úsek	k1gInSc2	úsek
od	od	k7c2	od
říčního	říční	k2eAgMnSc2d1	říční
km	km	kA	km
61,3	[number]	k4	61,3
pod	pod	k7c7	pod
lokalitou	lokalita	k1gFnSc7	lokalita
Paseky	paseka	k1gFnSc2	paseka
po	po	k7c4	po
chatovou	chatový	k2eAgFnSc4d1	chatová
osadu	osada	k1gFnSc4	osada
Rackovice	Rackovice	k1gFnSc2	Rackovice
nad	nad	k7c7	nad
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
Seč	seč	k6eAd1	seč
<g/>
,	,	kIx,	,
rozhraní	rozhraní	k1gNnSc1	rozhraní
mezi	mezi	k7c7	mezi
Pardubickým	pardubický	k2eAgInSc7d1	pardubický
krajem	kraj	k1gInSc7	kraj
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
(	(	kIx(	(
<g/>
sídelní	sídelní	k2eAgFnPc1d1	sídelní
lokality	lokalita	k1gFnPc1	lokalita
města	město	k1gNnSc2	město
Seč	seč	k6eAd1	seč
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
)	)	kIx)	)
a	a	k8xC	a
Krajem	krajem	k6eAd1	krajem
Vysočina	vysočina	k1gFnSc1	vysočina
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
(	(	kIx(	(
<g/>
obce	obec	k1gFnSc2	obec
Rušinov	Rušinov	k1gInSc1	Rušinov
<g/>
,	,	kIx,	,
Klokočov	Klokočov	k1gInSc1	Klokočov
a	a	k8xC	a
Jeřišno	Jeřišno	k1gNnSc1	Jeřišno
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnPc1d1	významná
lokality	lokalita	k1gFnPc1	lokalita
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vodním	vodní	k2eAgInSc6d1	vodní
toku	tok	k1gInSc6	tok
a	a	k8xC	a
podél	podél	k7c2	podél
jeho	jeho	k3xOp3gNnSc2	jeho
koryta	koryto	k1gNnSc2	koryto
byla	být	k5eAaImAgNnP	být
vyhlášena	vyhlášen	k2eAgNnPc1d1	vyhlášeno
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
soustavy	soustava	k1gFnSc2	soustava
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
na	na	k7c4	na
celkem	celkem	k6eAd1	celkem
čtyřech	čtyři	k4xCgMnPc6	čtyři
evropsky	evropsky	k6eAd1	evropsky
významných	významný	k2eAgFnPc6d1	významná
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Agentury	agentura	k1gFnSc2	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
názvy	název	k1gInPc7	název
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Údolí	údolí	k1gNnSc1	údolí
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
6,38	[number]	k4	6,38
ha	ha	kA	ha
a	a	k8xC	a
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
poloh	poloha	k1gFnPc2	poloha
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
klesající	klesající	k2eAgFnSc7d1	klesající
od	od	k7c2	od
665	[number]	k4	665
do	do	k7c2	do
606	[number]	k4	606
m	m	kA	m
v	v	k7c6	v
Sečské	sečský	k2eAgFnSc6d1	Sečská
vrchovině	vrchovina	k1gFnSc6	vrchovina
na	na	k7c6	na
území	území	k1gNnSc6	území
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Žďárské	Žďárské	k2eAgInPc4d1	Žďárské
vrchy	vrch	k1gInPc4	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Situačně	situačně	k6eAd1	situačně
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
geomorfologického	geomorfologický	k2eAgInSc2d1	geomorfologický
okrsku	okrsek	k1gInSc2	okrsek
Kameničská	Kameničský	k2eAgFnSc1d1	Kameničský
vrchovina	vrchovina	k1gFnSc1	vrchovina
od	od	k7c2	od
vodního	vodní	k2eAgInSc2d1	vodní
pramene	pramen	k1gInSc2	pramen
s	s	k7c7	s
názvem	název	k1gInSc7	název
Chrudimka	Chrudimek	k1gMnSc2	Chrudimek
pod	pod	k7c7	pod
obcí	obec	k1gFnSc7	obec
Dědová	Dědová	k1gFnSc1	Dědová
<g/>
,	,	kIx,	,
v	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
zdrojnice	zdrojnice	k1gFnSc2	zdrojnice
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
korytě	koryto	k1gNnSc6	koryto
začínajícím	začínající	k2eAgFnPc3d1	začínající
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3439	[number]	k4	3439
(	(	kIx(	(
<g/>
úsek	úsek	k1gInSc1	úsek
Dědová	Dědová	k1gFnSc1	Dědová
–	–	k?	–
Kameničky	kamenička	k1gFnSc2	kamenička
<g/>
)	)	kIx)	)
po	po	k7c4	po
soutok	soutok	k1gInSc4	soutok
s	s	k7c7	s
pramennou	pramenný	k2eAgFnSc7d1	pramenná
zdrojnicí	zdrojnice	k1gFnSc7	zdrojnice
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
pod	pod	k7c7	pod
Filipovem	Filipov	k1gInSc7	Filipov
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
obcí	obec	k1gFnPc2	obec
Kameničky	kamenička	k1gFnSc2	kamenička
a	a	k8xC	a
údolní	údolní	k2eAgFnSc7d1	údolní
nivou	niva	k1gFnSc7	niva
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Lány	lán	k1gInPc4	lán
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
mostu	most	k1gInSc2	most
silnice	silnice	k1gFnSc1	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
3438	[number]	k4	3438
(	(	kIx(	(
<g/>
úsek	úsek	k1gInSc1	úsek
Hamry	Hamry	k1gInPc1	Hamry
–	–	k?	–
Vortová	Vortová	k1gFnSc1	Vortová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
vzdutí	vzdutí	k1gNnSc2	vzdutí
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Hamry	Hamry	k1gInPc1	Hamry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
výskyt	výskyt	k1gInSc1	výskyt
mihule	mihule	k1gFnSc2	mihule
potoční	potoční	k2eAgFnSc2d1	potoční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
230	[number]	k4	230
ha	ha	kA	ha
a	a	k8xC	a
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
poloh	poloha	k1gFnPc2	poloha
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
klesající	klesající	k2eAgFnSc7d1	klesající
od	od	k7c2	od
525	[number]	k4	525
do	do	k7c2	do
407	[number]	k4	407
m	m	kA	m
v	v	k7c6	v
Sečské	sečský	k2eAgFnSc6d1	Sečská
vrchovině	vrchovina	k1gFnSc6	vrchovina
na	na	k7c6	na
území	území	k1gNnSc6	území
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Situačně	situačně	k6eAd1	situačně
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
v	v	k7c6	v
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
okrsku	okrsek	k1gInSc6	okrsek
Kameničská	Kameničský	k2eAgFnSc1d1	Kameničský
vrchovina	vrchovina	k1gFnSc1	vrchovina
od	od	k7c2	od
Trhové	trhový	k2eAgFnSc2d1	trhová
Kamenice	Kamenice	k1gFnSc2	Kamenice
se	s	k7c7	s
silničním	silniční	k2eAgInSc7d1	silniční
mostem	most	k1gInSc7	most
v	v	k7c4	v
říční	říční	k2eAgNnSc4d1	říční
km	km	kA	km
69,9	[number]	k4	69,9
(	(	kIx(	(
<g/>
pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
Havlíčkova	Havlíčkův	k2eAgFnSc1d1	Havlíčkova
ulice	ulice	k1gFnPc1	ulice
<g/>
)	)	kIx)	)
v	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
řeky	řeka	k1gFnSc2	řeka
údolím	údolí	k1gNnSc7	údolí
Kameničské	Kameničský	k2eAgFnSc2d1	Kameničský
vrchoviny	vrchovina	k1gFnSc2	vrchovina
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Horní	horní	k2eAgNnSc1d1	horní
Bradlo	bradlo	k1gNnSc1	bradlo
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Seč	seč	k1gFnSc1	seč
<g/>
,	,	kIx,	,
Bojanov	Bojanov	k1gInSc1	Bojanov
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
mostu	most	k1gInSc3	most
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
40,4	[number]	k4	40,4
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
33760	[number]	k4	33760
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Křižanovice	Křižanovice	k1gFnPc4	Křižanovice
<g/>
,	,	kIx,	,
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
vzdutí	vzdutí	k1gNnSc2	vzdutí
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Křižanovice	Křižanovice	k1gFnSc2	Křižanovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
výskyt	výskyt	k1gInSc1	výskyt
vydry	vydra	k1gFnSc2	vydra
říční	říční	k2eAgFnSc2d1	říční
a	a	k8xC	a
vegetace	vegetace	k1gFnSc2	vegetace
svazů	svaz	k1gInPc2	svaz
Ranunculion	Ranunculion	k1gInSc1	Ranunculion
fluitantis	fluitantis	k1gFnPc1	fluitantis
a	a	k8xC	a
Callitricho-Batrachion	Callitricho-Batrachion	k1gInSc1	Callitricho-Batrachion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krkanka	Krkanka	k1gFnSc1	Krkanka
–	–	k?	–
Strádovské	Strádovský	k2eAgNnSc4d1	Strádovské
peklo	peklo	k1gNnSc4	peklo
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
277,5	[number]	k4	277,5
ha	ha	kA	ha
a	a	k8xC	a
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
poloh	poloha	k1gFnPc2	poloha
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
klesající	klesající	k2eAgFnSc7d1	klesající
od	od	k7c2	od
542	[number]	k4	542
do	do	k7c2	do
310	[number]	k4	310
m	m	kA	m
v	v	k7c6	v
Sečské	sečský	k2eAgFnSc6d1	Sečská
vrchovině	vrchovina	k1gFnSc6	vrchovina
na	na	k7c6	na
území	území	k1gNnSc6	území
Chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
lokalit	lokalita	k1gFnPc2	lokalita
podél	podél	k7c2	podél
pravostranných	pravostranný	k2eAgInPc2d1	pravostranný
přítoků	přítok	k1gInPc2	přítok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
potoky	potok	k1gInPc1	potok
Lhotický	Lhotický	k2eAgMnSc1d1	Lhotický
a	a	k8xC	a
Debrný	Debrný	k2eAgMnSc1d1	Debrný
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnSc2d1	významná
lokality	lokalita	k1gFnSc2	lokalita
na	na	k7c6	na
průtoku	průtok	k1gInSc6	průtok
řeky	řeka	k1gFnSc2	řeka
pod	pod	k7c7	pod
Křižanovickou	Křižanovický	k2eAgFnSc7d1	Křižanovická
přehradou	přehrada	k1gFnSc7	přehrada
(	(	kIx(	(
<g/>
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
37,5	[number]	k4	37,5
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
ostrém	ostrý	k2eAgInSc6d1	ostrý
ohybu	ohyb	k1gInSc6	ohyb
pod	pod	k7c7	pod
sídelní	sídelní	k2eAgFnSc7d1	sídelní
lokalitou	lokalita	k1gFnSc7	lokalita
Slavice	slavice	k1gFnSc2	slavice
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Licibořice	Licibořice	k1gFnSc2	Licibořice
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
přírodními	přírodní	k2eAgFnPc7d1	přírodní
rezervacemi	rezervace	k1gFnPc7	rezervace
Krkanka	Krkanka	k1gFnSc1	Krkanka
v	v	k7c6	v
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
okrsku	okrsek	k1gInSc6	okrsek
Kameničská	Kameničský	k2eAgFnSc1d1	Kameničský
vrchovina	vrchovina	k1gFnSc1	vrchovina
a	a	k8xC	a
Strádovské	Strádovský	k2eAgNnSc1d1	Strádovské
peklo	peklo	k1gNnSc1	peklo
ve	v	k7c6	v
Skutečské	skutečský	k2eAgFnSc6d1	Skutečská
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
vzdutí	vzdutí	k1gNnSc2	vzdutí
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Práčov	Práčov	k1gInSc1	Práčov
(	(	kIx(	(
<g/>
za	za	k7c7	za
mostem	most	k1gInSc7	most
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
v	v	k7c6	v
říčním	říční	k2eAgNnSc6d1	říční
km	km	kA	km
31,3	[number]	k4	31,3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
výskyt	výskyt	k1gInSc4	výskyt
vranky	vranka	k1gFnPc1	vranka
obecné	obecná	k1gFnSc2	obecná
a	a	k8xC	a
podél	podél	k7c2	podél
břehů	břeh	k1gInPc2	břeh
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
hlubokého	hluboký	k2eAgNnSc2d1	hluboké
říčního	říční	k2eAgNnSc2d1	říční
údolí	údolí	k1gNnSc2	údolí
typu	typ	k1gInSc2	typ
soutěsky	soutěska	k1gFnSc2	soutěska
lesy	les	k1gInPc1	les
svazu	svaz	k1gInSc2	svaz
Tilio	Tilio	k6eAd1	Tilio
Acerion	Acerion	k1gInSc4	Acerion
a	a	k8xC	a
na	na	k7c6	na
silikátových	silikátový	k2eAgInPc6d1	silikátový
skalních	skalní	k2eAgInPc6d1	skalní
výstupech	výstup	k1gInPc6	výstup
chasmofytická	chasmofytický	k2eAgFnSc1d1	chasmofytický
vegetace	vegetace	k1gFnSc1	vegetace
<g/>
,	,	kIx,	,
také	také	k9	také
bučiny	bučina	k1gFnSc2	bučina
asociace	asociace	k1gFnSc2	asociace
Luzulo-Fagetum	Luzulo-Fagetum	k1gNnSc1	Luzulo-Fagetum
a	a	k8xC	a
Asperulo-Fagetum	Asperulo-Fagetum	k1gNnSc1	Asperulo-Fagetum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
<g/>
,	,	kIx,	,
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
65,58	[number]	k4	65,58
ha	ha	kA	ha
a	a	k8xC	a
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
poloh	poloha	k1gFnPc2	poloha
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
klesající	klesající	k2eAgFnSc7d1	klesající
od	od	k7c2	od
239	[number]	k4	239
do	do	k7c2	do
221	[number]	k4	221
m	m	kA	m
v	v	k7c6	v
Chrudimské	chrudimský	k2eAgFnSc6d1	Chrudimská
tabuli	tabule	k1gFnSc6	tabule
a	a	k8xC	a
v	v	k7c6	v
Pardubické	pardubický	k2eAgFnSc6d1	pardubická
kotlině	kotlina	k1gFnSc6	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terénu	terén	k1gInSc6	terén
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
okrsku	okrsek	k1gInSc6	okrsek
Heřmanoměstecká	heřmanoměstecký	k2eAgFnSc1d1	Heřmanoměstecká
tabule	tabule	k1gFnSc1	tabule
od	od	k7c2	od
jezu	jez	k1gInSc2	jez
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
16,4	[number]	k4	16,4
(	(	kIx(	(
<g/>
Vestec	Vestec	k1gInSc1	Vestec
<g/>
)	)	kIx)	)
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Chrudim	Chrudim	k1gFnSc1	Chrudim
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
s	s	k7c7	s
Novohradkou	Novohradka	k1gFnSc7	Novohradka
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
okrsku	okrsek	k1gInSc6	okrsek
Dřenická	Dřenický	k2eAgFnSc1d1	Dřenický
kotlina	kotlina	k1gFnSc1	kotlina
(	(	kIx(	(
<g/>
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
úsekem	úsek	k1gInSc7	úsek
od	od	k7c2	od
říčního	říční	k2eAgMnSc2d1	říční
km	km	kA	km
9	[number]	k4	9
do	do	k7c2	do
7,2	[number]	k4	7,2
v	v	k7c6	v
Holické	holický	k2eAgFnSc6d1	Holická
tabuli	tabule	k1gFnSc6	tabule
<g/>
)	)	kIx)	)
až	až	k9	až
za	za	k7c4	za
most	most	k1gInSc4	most
pro	pro	k7c4	pro
pěší	pěší	k1gMnPc4	pěší
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
2,9	[number]	k4	2,9
na	na	k7c6	na
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
výskyt	výskyt	k1gInSc1	výskyt
klínatky	klínatka	k1gFnSc2	klínatka
rohaté	rohatý	k2eAgFnSc2d1	rohatá
a	a	k8xC	a
vegetace	vegetace	k1gFnSc2	vegetace
svazů	svaz	k1gInPc2	svaz
Ranunculion	Ranunculion	k1gInSc1	Ranunculion
fluitantis	fluitantis	k1gFnPc1	fluitantis
a	a	k8xC	a
Callitricho-Batrachion	Callitricho-Batrachion	k1gInSc1	Callitricho-Batrachion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pramen	pramen	k1gInSc1	pramen
===	===	k?	===
</s>
</p>
<p>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vrchovině	vrchovina	k1gFnSc6	vrchovina
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
U	u	k7c2	u
oběšeného	oběšený	k2eAgInSc2d1	oběšený
(	(	kIx(	(
<g/>
737,4	[number]	k4	737,4
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
plochou	plochý	k2eAgFnSc7d1	plochá
vrcholovou	vrcholový	k2eAgFnSc7d1	vrcholová
částí	část	k1gFnSc7	část
prochází	procházet	k5eAaImIp3nS	procházet
hlavní	hlavní	k2eAgFnSc1d1	hlavní
evropská	evropský	k2eAgFnSc1d1	Evropská
rozvodnice	rozvodnice	k1gFnSc1	rozvodnice
<g/>
,	,	kIx,	,
dělící	dělící	k2eAgNnSc1d1	dělící
úmoří	úmoří	k1gNnSc1	úmoří
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
<g/>
)	)	kIx)	)
a	a	k8xC	a
úmoří	úmoří	k1gNnSc1	úmoří
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Svratka	Svratka	k1gFnSc1	Svratka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
úbočí	úbočí	k1gNnSc6	úbočí
kopce	kopec	k1gInSc2	kopec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvýše	nejvýše	k6eAd1	nejvýše
položené	položený	k2eAgNnSc4d1	položené
prameniště	prameniště	k1gNnSc4	prameniště
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
<g/>
,	,	kIx,	,
z	z	k7c2	z
několika	několik	k4yIc2	několik
pramenů	pramen	k1gInPc2	pramen
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
v	v	k7c6	v
turistické	turistický	k2eAgFnSc6d1	turistická
mapě	mapa	k1gFnSc6	mapa
a	a	k8xC	a
na	na	k7c6	na
informační	informační	k2eAgFnSc6d1	informační
tabuli	tabule	k1gFnSc6	tabule
vlastivědné	vlastivědný	k2eAgFnSc2d1	vlastivědná
stezky	stezka	k1gFnSc2	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
uváděná	uváděný	k2eAgFnSc1d1	uváděná
lesní	lesní	k2eAgFnSc1d1	lesní
studánka	studánka	k1gFnSc1	studánka
v	v	k7c6	v
polesí	polesí	k1gNnSc6	polesí
Stará	starý	k2eAgFnSc1d1	stará
obora	obora	k1gFnSc1	obora
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
však	však	k9	však
Filipovský	Filipovský	k2eAgInSc4d1	Filipovský
pramen	pramen	k1gInSc4	pramen
<g/>
,	,	kIx,	,
situovaný	situovaný	k2eAgInSc4d1	situovaný
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
705	[number]	k4	705
m	m	kA	m
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
lesní	lesní	k2eAgFnSc2d1	lesní
lokality	lokalita	k1gFnSc2	lokalita
Humperky	Humperka	k1gFnSc2	Humperka
nad	nad	k7c7	nad
<g />
.	.	kIx.	.
</s>
<s>
Filipovem	Filipov	k1gInSc7	Filipov
(	(	kIx(	(
<g/>
od	od	k7c2	od
něho	on	k3xPp3gInSc2	on
název	název	k1gInSc4	název
pramene	pramen	k1gInSc2	pramen
<g/>
,	,	kIx,	,
výchozí	výchozí	k2eAgFnSc4d1	výchozí
místo	místo	k7c2	místo
vlastivědné	vlastivědný	k2eAgFnSc2d1	vlastivědná
stezky	stezka	k1gFnSc2	stezka
Krajem	krajem	k6eAd1	krajem
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
členitém	členitý	k2eAgInSc6d1	členitý
terénu	terén	k1gInSc6	terén
vrchoviny	vrchovina	k1gFnSc2	vrchovina
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
několik	několik	k4yIc1	několik
pramenišť	prameniště	k1gNnPc2	prameniště
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
lesní	lesní	k2eAgFnSc2d1	lesní
lokality	lokalita	k1gFnSc2	lokalita
Humperky	Humperka	k1gFnSc2	Humperka
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vlhké	vlhký	k2eAgFnPc1d1	vlhká
louky	louka	k1gFnPc1	louka
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
pod	pod	k7c7	pod
vyvýšeninou	vyvýšenina	k1gFnSc7	vyvýšenina
Na	na	k7c6	na
Kopci	kopec	k1gInSc6	kopec
(	(	kIx(	(
<g/>
681	[number]	k4	681
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
Dědová	Dědová	k1gFnSc1	Dědová
a	a	k8xC	a
také	také	k9	také
lokality	lokalita	k1gFnPc4	lokalita
přírodní	přírodní	k2eAgFnPc4d1	přírodní
památky	památka	k1gFnPc4	památka
Bahna	bahno	k1gNnSc2	bahno
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
bystřin	bystřina	k1gFnPc2	bystřina
odvádějí	odvádět	k5eAaImIp3nP	odvádět
vodu	voda	k1gFnSc4	voda
od	od	k7c2	od
pramenů	pramen	k1gInPc2	pramen
do	do	k7c2	do
širokého	široký	k2eAgNnSc2d1	široké
a	a	k8xC	a
mělkého	mělký	k2eAgNnSc2d1	mělké
údolí	údolí	k1gNnSc2	údolí
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
sídelní	sídelní	k2eAgFnSc2d1	sídelní
lokality	lokalita	k1gFnSc2	lokalita
Filipov	Filipovo	k1gNnPc2	Filipovo
<g/>
,	,	kIx,	,
s	s	k7c7	s
roztroušenou	roztroušený	k2eAgFnSc7d1	roztroušená
zástavbou	zástavba	k1gFnSc7	zástavba
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
několika	několik	k4yIc2	několik
vyvýšenin	vyvýšenina	k1gFnPc2	vyvýšenina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
úžlabí	úžlabí	k1gNnSc6	úžlabí
tvořeném	tvořený	k2eAgNnSc6d1	tvořené
úbočím	úbočí	k1gNnSc7	úbočí
vrcholu	vrchol	k1gInSc2	vrchol
U	u	k7c2	u
oběšeného	oběšený	k2eAgInSc2d1	oběšený
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
a	a	k8xC	a
protilehlými	protilehlý	k2eAgInPc7d1	protilehlý
svahy	svah	k1gInPc7	svah
vyvýšeniny	vyvýšenina	k1gFnSc2	vyvýšenina
Na	na	k7c6	na
Kopci	kopec	k1gInSc6	kopec
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
uzavřeném	uzavřený	k2eAgNnSc6d1	uzavřené
svahy	svah	k1gInPc1	svah
Dědovského	dědovský	k2eAgInSc2d1	dědovský
kopce	kopec	k1gInSc2	kopec
(	(	kIx(	(
<g/>
676	[number]	k4	676
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
svahové	svahový	k2eAgFnPc1d1	Svahová
kupy	kupa	k1gFnPc1	kupa
Na	na	k7c6	na
Bahnech	bahno	k1gNnPc6	bahno
(	(	kIx(	(
<g/>
663	[number]	k4	663
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Širší	široký	k2eAgNnSc4d2	širší
prameniště	prameniště	k1gNnSc4	prameniště
s	s	k7c7	s
několika	několik	k4yIc2	několik
dalšími	další	k2eAgFnPc7d1	další
zdrojnicemi	zdrojnice	k1gFnPc7	zdrojnice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dílčího	dílčí	k2eAgNnSc2d1	dílčí
povodí	povodí	k1gNnSc2	povodí
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
Kameniček	Kameničky	k1gFnPc2	Kameničky
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
od	od	k7c2	od
Filipovského	Filipovský	k2eAgInSc2d1	Filipovský
pramene	pramen	k1gInSc2	pramen
protéká	protékat	k5eAaImIp3nS	protékat
Chráněnou	chráněný	k2eAgFnSc7d1	chráněná
krajinnou	krajinný	k2eAgFnSc7d1	krajinná
oblastí	oblast	k1gFnSc7	oblast
Žďárské	Žďárské	k2eAgInPc7d1	Žďárské
vrchy	vrch	k1gInPc7	vrch
<g/>
,	,	kIx,	,
mělkým	mělký	k2eAgNnSc7d1	mělké
a	a	k8xC	a
širokým	široký	k2eAgNnSc7d1	široké
úvalovým	úvalový	k2eAgNnSc7d1	úvalový
údolím	údolí	k1gNnSc7	údolí
se	s	k7c7	s
zamokřenými	zamokřený	k2eAgFnPc7d1	zamokřená
loukami	louka	k1gFnPc7	louka
náhorní	náhorní	k2eAgFnSc2d1	náhorní
plošiny	plošina	k1gFnSc2	plošina
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
zdrojnic	zdrojnice	k1gFnPc2	zdrojnice
pod	pod	k7c7	pod
Filipovem	Filipovo	k1gNnSc7	Filipovo
v	v	k7c6	v
podmáčených	podmáčený	k2eAgFnPc6d1	podmáčená
loukách	louka	k1gFnPc6	louka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
bystřiny	bystřina	k1gFnSc2	bystřina
nad	nad	k7c7	nad
monitorovací	monitorovací	k2eAgFnSc7d1	monitorovací
stanicí	stanice	k1gFnSc7	stanice
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vod	voda	k1gFnPc2	voda
Českého	český	k2eAgInSc2d1	český
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
(	(	kIx(	(
<g/>
v	v	k7c4	v
říční	říční	k2eAgNnSc4d1	říční
km	km	kA	km
104,2	[number]	k4	104,2
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jako	jako	k9	jako
potok	potok	k1gInSc1	potok
Kameničkami	Kameničky	k1gFnPc7	Kameničky
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
osada	osada	k1gFnSc1	osada
Kamenice	Kamenice	k1gFnSc1	Kamenice
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
názvu	název	k1gInSc2	název
řeky	řeka	k1gFnSc2	řeka
pojmenované	pojmenovaný	k2eAgFnSc6d1	pojmenovaná
podle	podle	k7c2	podle
kamenitého	kamenitý	k2eAgNnSc2d1	kamenité
koryta	koryto	k1gNnSc2	koryto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kameničky	Kameničky	k1gFnPc1	Kameničky
nesou	nést	k5eAaImIp3nP	nést
symbol	symbol	k1gInSc4	symbol
pramene	pramen	k1gInSc2	pramen
řeky	řeka	k1gFnSc2	řeka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
modrého	modrý	k2eAgInSc2d1	modrý
hrotu	hrot	k1gInSc2	hrot
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
a	a	k8xC	a
modrého	modrý	k2eAgInSc2d1	modrý
pásu	pás	k1gInSc2	pás
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
komunálních	komunální	k2eAgInPc6d1	komunální
symbolech	symbol	k1gInPc6	symbol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
Kameničkami	Kameničky	k1gFnPc7	Kameničky
přibírá	přibírat	k5eAaImIp3nS	přibírat
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
Jeníkovského	Jeníkovský	k2eAgInSc2d1	Jeníkovský
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
svodnice	svodnice	k1gFnSc2	svodnice
z	z	k7c2	z
mokřadů	mokřad	k1gInPc2	mokřad
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Volákův	Volákův	k2eAgInSc1d1	Volákův
kopec	kopec	k1gInSc1	kopec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgInSc6d1	jihozápadní
okraji	okraj	k1gInSc6	okraj
Kameniček	Kameničky	k1gFnPc2	Kameničky
rybník	rybník	k1gInSc4	rybník
Groš	groš	k1gInSc4	groš
<g/>
,	,	kIx,	,
průtokem	průtok	k1gInSc7	průtok
rybníku	rybník	k1gInSc3	rybník
prodělává	prodělávat	k5eAaImIp3nS	prodělávat
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
výraznou	výrazný	k2eAgFnSc4d1	výrazná
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
je	být	k5eAaImIp3nS	být
vydatnější	vydatný	k2eAgNnSc4d2	vydatnější
a	a	k8xC	a
koryto	koryto	k1gNnSc4	koryto
širší	široký	k2eAgNnSc4d2	širší
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
rybníkem	rybník	k1gInSc7	rybník
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
lesního	lesní	k2eAgInSc2d1	lesní
prostoru	prostor	k1gInSc2	prostor
přerušovaného	přerušovaný	k2eAgInSc2d1	přerušovaný
loukami	louka	k1gFnPc7	louka
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
meandry	meandr	k1gInPc7	meandr
v	v	k7c6	v
údolní	údolní	k2eAgFnSc6d1	údolní
nivě	niva	k1gFnSc6	niva
s	s	k7c7	s
místní	místní	k2eAgFnSc7d1	místní
lokalitou	lokalita	k1gFnSc7	lokalita
Lány	lán	k1gInPc4	lán
(	(	kIx(	(
<g/>
na	na	k7c6	na
vlastivědné	vlastivědný	k2eAgFnSc6d1	vlastivědná
stezce	stezka	k1gFnSc6	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
stanoviště	stanoviště	k1gNnSc2	stanoviště
č.	č.	k?	č.
4	[number]	k4	4
s	s	k7c7	s
názvem	název	k1gInSc7	název
Luční	luční	k2eAgFnSc1d1	luční
niva	niva	k1gFnSc1	niva
Lány	lán	k1gInPc1	lán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
se	se	k3xPyFc4	se
smrky	smrk	k1gInPc7	smrk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
ekotyp	ekotyp	k1gInSc1	ekotyp
borovice	borovice	k1gFnSc2	borovice
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
lánská	lánský	k2eAgFnSc1d1	lánská
(	(	kIx(	(
<g/>
lesním	lesní	k2eAgInSc7d1	lesní
prostorem	prostor	k1gInSc7	prostor
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
vedena	vést	k5eAaImNgFnS	vést
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Borovice	borovice	k1gFnSc1	borovice
lánská	lánský	k2eAgFnSc1d1	lánská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
tzv.	tzv.	kA	tzv.
lánským	lánský	k2eAgInSc7d1	lánský
luhem	luh	k1gInSc7	luh
přibírá	přibírat	k5eAaImIp3nS	přibírat
vody	voda	k1gFnPc4	voda
z	z	k7c2	z
mokřadů	mokřad	k1gInPc2	mokřad
<g/>
,	,	kIx,	,
Chlumětínského	Chlumětínský	k2eAgNnSc2d1	Chlumětínský
potoku	potok	k1gInSc3	potok
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
levostranného	levostranný	k2eAgInSc2d1	levostranný
přítoku	přítok	k1gInSc2	přítok
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
ČHMÚ	ČHMÚ	kA	ČHMÚ
<g/>
)	)	kIx)	)
také	také	k9	také
Krejcarského	Krejcarský	k2eAgMnSc2d1	Krejcarský
potoku	potok	k1gInSc3	potok
<g/>
,	,	kIx,	,
s	s	k7c7	s
prameništěm	prameniště	k1gNnSc7	prameniště
pod	pod	k7c7	pod
Vortovským	Vortovský	k2eAgInSc7d1	Vortovský
vrchem	vrch	k1gInSc7	vrch
(	(	kIx(	(
<g/>
704	[number]	k4	704
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
lesní	lesní	k2eAgFnSc2d1	lesní
lokality	lokalita	k1gFnSc2	lokalita
podtéká	podtékat	k5eAaImIp3nS	podtékat
most	most	k1gInSc1	most
silnice	silnice	k1gFnSc2	silnice
III	III	kA	III
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
3438	[number]	k4	3438
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Hamry	Hamry	k1gInPc4	Hamry
–	–	k?	–
Vortová	Vortová	k1gFnSc1	Vortová
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vzdutí	vzdutí	k1gNnSc2	vzdutí
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Hamry	Hamry	k1gInPc1	Hamry
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
její	její	k3xOp3gInSc4	její
další	další	k2eAgInSc4d1	další
levostranný	levostranný	k2eAgInSc4d1	levostranný
přítok	přítok	k1gInSc4	přítok
Vortovský	Vortovský	k2eAgInSc4d1	Vortovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
s	s	k7c7	s
prameništěm	prameniště	k1gNnSc7	prameniště
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
mezi	mezi	k7c7	mezi
vrchy	vrch	k1gInPc4	vrch
Šindelný	Šindelný	k2eAgMnSc1d1	Šindelný
(	(	kIx(	(
<g/>
806	[number]	k4	806
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Otrok	otrok	k1gMnSc1	otrok
(	(	kIx(	(
<g/>
717	[number]	k4	717
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cca	cca	kA	cca
1	[number]	k4	1
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Mariánské	mariánský	k2eAgFnSc2d1	Mariánská
Hutě	huť	k1gFnSc2	huť
<g/>
,	,	kIx,	,
lokality	lokalita	k1gFnSc2	lokalita
obce	obec	k1gFnSc2	obec
Herálec	Herálec	k1gInSc1	Herálec
<g/>
.	.	kIx.	.
</s>
<s>
Vortovský	Vortovský	k2eAgInSc1d1	Vortovský
potok	potok	k1gInSc1	potok
(	(	kIx(	(
<g/>
i	i	k8xC	i
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
Krejcarský	Krejcarský	k2eAgInSc1d1	Krejcarský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
Devítiskalské	Devítiskalský	k2eAgFnSc6d1	Devítiskalský
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
,	,	kIx,	,
geomorfologickém	geomorfologický	k2eAgInSc6d1	geomorfologický
okrsku	okrsek	k1gInSc6	okrsek
Žďárských	Žďárských	k2eAgInPc2d1	Žďárských
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Prameniště	prameniště	k1gNnSc1	prameniště
Vortovského	Vortovský	k2eAgInSc2d1	Vortovský
potoka	potok	k1gInSc2	potok
je	být	k5eAaImIp3nS	být
nejvýše	nejvýše	k6eAd1	nejvýše
položeným	položený	k2eAgNnSc7d1	položené
v	v	k7c4	v
povodí	povodí	k1gNnSc4	povodí
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
a	a	k8xC	a
Šindelný	Šindelný	k2eAgInSc1d1	Šindelný
vrch	vrch	k1gInSc1	vrch
jeho	jeho	k3xOp3gInSc7	jeho
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hamerská	hamerský	k2eAgFnSc1d1	Hamerská
přehrada	přehrada	k1gFnSc1	přehrada
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
93,2	[number]	k4	93,2
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
se	s	k7c7	s
sypanou	sypaný	k2eAgFnSc7d1	sypaná
hrází	hráz	k1gFnSc7	hráz
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
realizovaná	realizovaný	k2eAgFnSc1d1	realizovaná
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Hamry	Hamry	k1gInPc1	Hamry
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgInSc7	první
vodním	vodní	k2eAgInSc7d1	vodní
dílem	díl	k1gInSc7	díl
provedeným	provedený	k2eAgInSc7d1	provedený
na	na	k7c6	na
Chrudimce	Chrudimka	k1gFnSc6	Chrudimka
v	v	k7c6	v
letech	let	k1gInPc6	let
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
ochrany	ochrana	k1gFnSc2	ochrana
před	před	k7c7	před
povodněmi	povodeň	k1gFnPc7	povodeň
<g/>
,	,	kIx,	,
způsobujícími	způsobující	k2eAgFnPc7d1	způsobující
v	v	k7c4	v
povodí	povodí	k1gNnSc4	povodí
řeky	řeka	k1gFnSc2	řeka
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
škody	škoda	k1gFnSc2	škoda
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
i	i	k8xC	i
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
nádrži	nádrž	k1gFnSc6	nádrž
přibírá	přibírat	k5eAaImIp3nS	přibírat
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
vody	voda	k1gFnSc2	voda
levostranného	levostranný	k2eAgInSc2d1	levostranný
přítoku	přítok	k1gInSc2	přítok
<g/>
,	,	kIx,	,
potoku	potok	k1gInSc6	potok
Valčice	Valčice	k1gFnSc2	Valčice
<g/>
.	.	kIx.	.
</s>
<s>
Přehradu	přehrada	k1gFnSc4	přehrada
opouští	opouštět	k5eAaImIp3nS	opouštět
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
menší	malý	k2eAgFnSc2d2	menší
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
protéká	protékat	k5eAaImIp3nS	protékat
obcí	obec	k1gFnSc7	obec
Hamry	Hamry	k1gInPc1	Hamry
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
osada	osada	k1gFnSc1	osada
s	s	k7c7	s
názvem	název	k1gInSc7	název
Přerostlé	přerostlý	k2eAgNnSc1d1	přerostlé
<g/>
,	,	kIx,	,
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
vodním	vodní	k2eAgInSc7d1	vodní
hamrem	hamr	k1gInSc7	hamr
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
Stupník	stupník	k1gInSc4	stupník
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
vodní	vodní	k2eAgFnSc1d1	vodní
stoupa	stoupa	k1gFnSc1	stoupa
na	na	k7c4	na
drcení	drcení	k1gNnSc4	drcení
rud	ruda	k1gFnPc2	ruda
a	a	k8xC	a
strusek	struska	k1gFnPc2	struska
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
dále	daleko	k6eAd2	daleko
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
samotě	samota	k1gFnSc3	samota
Hamřík	Hamřík	k1gMnSc1	Hamřík
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomocí	pomocí	k7c2	pomocí
vodního	vodní	k2eAgNnSc2d1	vodní
kola	kolo	k1gNnSc2	kolo
poháněl	pohánět	k5eAaImAgInS	pohánět
tzv.	tzv.	kA	tzv.
kosařský	kosařský	k2eAgInSc1d1	kosařský
hamr	hamr	k1gInSc1	hamr
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
kos	kosa	k1gFnPc2	kosa
pro	pro	k7c4	pro
sečení	sečení	k1gNnSc4	sečení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Hamříku	Hamřík	k1gMnSc3	Hamřík
směřuje	směřovat	k5eAaImIp3nS	směřovat
řeka	řeka	k1gFnSc1	řeka
k	k	k7c3	k
městu	město	k1gNnSc3	město
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
části	část	k1gFnSc6	část
Blatno	Blaten	k2eAgNnSc1d1	Blaten
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
Hlinsko	Hlinsko	k1gNnSc1	Hlinsko
<g/>
)	)	kIx)	)
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Blatenský	blatenský	k2eAgInSc4d1	blatenský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
pravostranný	pravostranný	k2eAgInSc4d1	pravostranný
přítok	přítok	k1gInSc4	přítok
s	s	k7c7	s
prameništěm	prameniště	k1gNnSc7	prameniště
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
mezi	mezi	k7c7	mezi
vrchy	vrch	k1gInPc4	vrch
Pešava	Pešava	k1gFnSc1	Pešava
(	(	kIx(	(
<g/>
697	[number]	k4	697
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dědovský	dědovský	k2eAgInSc1d1	dědovský
kopec	kopec	k1gInSc1	kopec
(	(	kIx(	(
<g/>
676	[number]	k4	676
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
Hlinskem	Hlinsko	k1gNnSc7	Hlinsko
je	být	k5eAaImIp3nS	být
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
značně	značně	k6eAd1	značně
regulován	regulovat	k5eAaImNgInS	regulovat
jezy	jez	k1gInPc7	jez
a	a	k8xC	a
břehy	břeh	k1gInPc7	břeh
koryta	koryto	k1gNnSc2	koryto
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
vyzděné	vyzděný	k2eAgNnSc4d1	vyzděné
(	(	kIx(	(
<g/>
Dvořákovo	Dvořákův	k2eAgNnSc4d1	Dvořákovo
a	a	k8xC	a
Budovcovo	Budovcův	k2eAgNnSc4d1	Budovcův
nábřeží	nábřeží	k1gNnSc4	nábřeží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
tok	tok	k1gInSc1	tok
řeky	řeka	k1gFnSc2	řeka
s	s	k7c7	s
četnými	četný	k2eAgInPc7d1	četný
meandry	meandr	k1gInPc7	meandr
začíná	začínat	k5eAaImIp3nS	začínat
opět	opět	k6eAd1	opět
až	až	k9	až
za	za	k7c2	za
obcí	obec	k1gFnPc2	obec
Vítanov	Vítanovo	k1gNnPc2	Vítanovo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
levostranného	levostranný	k2eAgInSc2d1	levostranný
přítoku	přítok	k1gInSc2	přítok
Slubice	Slubice	k1gFnSc2	Slubice
směřuje	směřovat	k5eAaImIp3nS	směřovat
hydrogeografická	hydrogeografický	k2eAgFnSc1d1	hydrogeografický
osa	osa	k1gFnSc1	osa
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
k	k	k7c3	k
severozápadu	severozápad	k1gInSc3	severozápad
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
jihozápadního	jihozápadní	k2eAgInSc2d1	jihozápadní
hřbetu	hřbet	k1gInSc2	hřbet
Železných	Železný	k1gMnPc2	Železný
hor.	hor.	k?	hor.
Koryto	koryto	k1gNnSc4	koryto
řeky	řeka	k1gFnSc2	řeka
od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
s	s	k7c7	s
potokem	potok	k1gInSc7	potok
Slubice	Slubice	k1gFnSc2	Slubice
až	až	k9	až
k	k	k7c3	k
Trhové	trhový	k2eAgFnSc3d1	trhová
Kamenici	Kamenice	k1gFnSc3	Kamenice
tvoří	tvořit	k5eAaImIp3nS	tvořit
rozhraní	rozhraní	k1gNnSc1	rozhraní
mezi	mezi	k7c7	mezi
geomorfologickými	geomorfologický	k2eAgInPc7d1	geomorfologický
okrsky	okrsek	k1gInPc7	okrsek
Sečské	sečský	k2eAgFnSc2d1	Sečská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
levobřežní	levobřežní	k2eAgFnSc1d1	levobřežní
část	část	k1gFnSc1	část
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
Stružinecké	Stružinecký	k2eAgFnSc2d1	Stružinecký
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
,	,	kIx,	,
pravobřežní	pravobřežní	k2eAgFnSc2d1	pravobřežní
do	do	k7c2	do
Kameničské	Kameničský	k2eAgFnSc2d1	Kameničský
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
<g/>
Stružinecká	Stružinecký	k2eAgFnSc1d1	Stružinecký
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgNnPc4d1	významné
Dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
potokem	potok	k1gInSc7	potok
<g/>
,	,	kIx,	,
levostranným	levostranný	k2eAgInSc7d1	levostranný
přítokem	přítok	k1gInSc7	přítok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
pod	pod	k7c7	pod
Veselým	veselý	k2eAgInSc7d1	veselý
Kopcem	kopec	k1gInSc7	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
potok	potok	k1gInSc1	potok
i	i	k8xC	i
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
stavby	stavba	k1gFnPc4	stavba
se	s	k7c7	s
zařízením	zařízení	k1gNnSc7	zařízení
na	na	k7c4	na
vodní	vodní	k2eAgInSc4d1	vodní
pohon	pohon	k1gInSc4	pohon
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
historická	historický	k2eAgFnSc1d1	historická
památka	památka	k1gFnSc1	památka
Králova	Králův	k2eAgFnSc1d1	Králova
pila	pila	k1gFnSc1	pila
(	(	kIx(	(
<g/>
na	na	k7c6	na
vlastivědné	vlastivědný	k2eAgFnSc6d1	vlastivědná
stezce	stezka	k1gFnSc6	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
stanoviště	stanoviště	k1gNnSc2	stanoviště
č.	č.	k?	č.
8	[number]	k4	8
<g/>
)	)	kIx)	)
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
vesničky	vesnička	k1gFnSc2	vesnička
Milesimov	Milesimov	k1gInSc1	Milesimov
<g/>
,	,	kIx,	,
s	s	k7c7	s
historicky	historicky	k6eAd1	historicky
významnou	významný	k2eAgFnSc7d1	významná
vodní	vodní	k2eAgFnSc7d1	vodní
pilou	pila	k1gFnSc7	pila
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
jednuškou	jednuška	k1gFnSc7	jednuška
a	a	k8xC	a
obilním	obilní	k2eAgInSc7d1	obilní
mlýnem	mlýn	k1gInSc7	mlýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
objekty	objekt	k1gInPc1	objekt
lidového	lidový	k2eAgNnSc2d1	lidové
stavitelství	stavitelství	k1gNnSc2	stavitelství
na	na	k7c4	na
vodní	vodní	k2eAgInSc4d1	vodní
pohon	pohon	k1gInSc4	pohon
byly	být	k5eAaImAgFnP	být
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
na	na	k7c6	na
Veselém	veselý	k2eAgInSc6d1	veselý
Kopci	kopec	k1gInSc6	kopec
(	(	kIx(	(
<g/>
na	na	k7c6	na
vlastivědné	vlastivědný	k2eAgFnSc6d1	vlastivědná
stezce	stezka	k1gFnSc6	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
stanoviště	stanoviště	k1gNnSc2	stanoviště
č.	č.	k?	č.
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
Vysočina	vysočina	k1gFnSc1	vysočina
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Soubor	soubor	k1gInSc1	soubor
lidových	lidový	k2eAgFnPc2d1	lidová
staveb	stavba	k1gFnPc2	stavba
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
venkovní	venkovní	k2eAgInSc1d1	venkovní
areál	areál	k1gInSc1	areál
tzv.	tzv.	kA	tzv.
skanzenu	skanzen	k1gInSc2	skanzen
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
nad	nad	k7c7	nad
soutokem	soutok	k1gInSc7	soutok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
a	a	k8xC	a
Dlouhého	Dlouhého	k2eAgInSc2d1	Dlouhého
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
přitékajícího	přitékající	k2eAgInSc2d1	přitékající
ze	z	k7c2	z
Stružinecké	Stružinecký	k2eAgFnSc2d1	Stružinecký
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
venkovní	venkovní	k2eAgFnSc3d1	venkovní
expozici	expozice	k1gFnSc3	expozice
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
železářský	železářský	k2eAgInSc1d1	železářský
hamr	hamr	k1gInSc1	hamr
s	s	k7c7	s
náhonem	náhon	k1gInSc7	náhon
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
v	v	k7c6	v
sídelní	sídelní	k2eAgFnSc6d1	sídelní
lokalitě	lokalita	k1gFnSc6	lokalita
Svobodné	svobodný	k2eAgInPc1d1	svobodný
Hamry	Hamry	k1gInPc1	Hamry
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hamr	hamr	k1gInSc4	hamr
nad	nad	k7c7	nad
Kamenicí	Kamenice	k1gFnSc7	Kamenice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Veselým	veselý	k2eAgInSc7d1	veselý
Kopcem	kopec	k1gInSc7	kopec
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
průtoku	průtok	k1gInSc2	průtok
Svobodnými	svobodný	k2eAgInPc7d1	svobodný
Hamry	Hamry	k1gInPc7	Hamry
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
významné	významný	k2eAgInPc4d1	významný
rybníky	rybník	k1gInPc4	rybník
(	(	kIx(	(
<g/>
na	na	k7c6	na
vlastivědné	vlastivědný	k2eAgFnSc6d1	vlastivědná
stezce	stezka	k1gFnSc6	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
stanoviště	stanoviště	k1gNnSc2	stanoviště
č.	č.	k?	č.
12	[number]	k4	12
s	s	k7c7	s
názvem	název	k1gInSc7	název
Trhovokamenické	trhovokamenický	k2eAgInPc4d1	trhovokamenický
rybníky	rybník	k1gInPc4	rybník
<g/>
)	)	kIx)	)
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
Mlýnský	mlýnský	k2eAgInSc4d1	mlýnský
rybník	rybník	k1gInSc4	rybník
a	a	k8xC	a
rybník	rybník	k1gInSc4	rybník
Rohlík	rohlík	k1gInSc1	rohlík
a	a	k8xC	a
Zadní	zadní	k2eAgInSc1d1	zadní
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
rybník	rybník	k1gInSc1	rybník
s	s	k7c7	s
názvem	název	k1gInSc7	název
Velká	velký	k2eAgFnSc1d1	velká
Kamenice	Kamenice	k1gFnSc1	Kamenice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Trhové	trhový	k2eAgFnSc6d1	trhová
Kamenici	Kamenice	k1gFnSc6	Kamenice
<g/>
,	,	kIx,	,
městysu	městys	k1gInSc6	městys
založeném	založený	k2eAgInSc6d1	založený
původně	původně	k6eAd1	původně
jako	jako	k8xS	jako
osada	osada	k1gFnSc1	osada
na	na	k7c6	na
brodu	brod	k1gInSc6	brod
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
s	s	k7c7	s
dřívějším	dřívější	k2eAgInSc7d1	dřívější
názvem	název	k1gInSc7	název
Kamenice	Kamenice	k1gFnSc2	Kamenice
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
část	část	k1gFnSc1	část
názvu	název	k1gInSc2	název
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
právy	právo	k1gNnPc7	právo
trhu	trh	k1gInSc2	trh
(	(	kIx(	(
<g/>
Trhová	trhový	k2eAgFnSc1d1	trhová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
nejdříve	dříve	k6eAd3	dříve
v	v	k7c4	v
říční	říční	k2eAgNnSc4d1	říční
km	km	kA	km
72,9	[number]	k4	72,9
pod	pod	k7c7	pod
železobetonovým	železobetonový	k2eAgInSc7d1	železobetonový
mostem	most	k1gInSc7	most
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
37	[number]	k4	37
(	(	kIx(	(
<g/>
úsek	úsek	k1gInSc1	úsek
Trhová	trhový	k2eAgFnSc1d1	trhová
Kamenice	Kamenice	k1gFnSc1	Kamenice
–	–	k?	–
Údavy	Údava	k1gFnSc2	Údava
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
69,9	[number]	k4	69,9
pod	pod	k7c7	pod
původním	původní	k2eAgInSc7d1	původní
mostem	most	k1gInSc7	most
přes	přes	k7c4	přes
Chrudimku	Chrudimka	k1gFnSc4	Chrudimka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Havlíčkovy	Havlíčkův	k2eAgFnSc2d1	Havlíčkova
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opouští	opouštět	k5eAaImIp3nS	opouštět
území	území	k1gNnSc4	území
CHKO	CHKO	kA	CHKO
Žďárské	Žďárské	k2eAgInPc4d1	Žďárské
vrchy	vrch	k1gInPc4	vrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příroda	příroda	k1gFnSc1	příroda
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Žďárských	Žďárských	k2eAgInPc2d1	Žďárských
vrchů	vrch	k1gInPc2	vrch
a	a	k8xC	a
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
mnohé	mnohé	k1gNnSc4	mnohé
malíře	malíř	k1gMnSc2	malíř
–	–	k?	–
krajináře	krajinář	k1gMnSc2	krajinář
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
letech	let	k1gInPc6	let
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
pobýval	pobývat	k5eAaImAgMnS	pobývat
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
–	–	k?	–
impresionista	impresionista	k1gMnSc1	impresionista
Antonín	Antonín	k1gMnSc1	Antonín
Slavíček	Slavíček	k1gMnSc1	Slavíček
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
v	v	k7c6	v
Kameničkách	Kameničky	k1gFnPc6	Kameničky
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
busta	busta	k1gFnSc1	busta
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
fary	fara	k1gFnSc2	fara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořil	tvořit	k5eAaImAgInS	tvořit
zde	zde	k6eAd1	zde
také	také	k9	také
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
Herbert	Herbert	k1gMnSc1	Herbert
Masaryk	Masaryk	k1gMnSc1	Masaryk
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
prvního	první	k4xOgMnSc4	první
československého	československý	k2eAgMnSc4d1	československý
prezidenta	prezident	k1gMnSc4	prezident
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
Charlotty	Charlotta	k1gFnSc2	Charlotta
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masarykové	Masarykové	k2eAgMnPc1d1	Masarykové
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
.	.	kIx.	.
<g/>
Hlinsko	Hlinsko	k1gNnSc4	Hlinsko
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
tvorby	tvorba	k1gFnSc2	tvorba
například	například	k6eAd1	například
František	František	k1gMnSc1	František
Kaván	Kaván	k1gMnSc1	Kaván
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pobýval	pobývat	k5eAaImAgMnS	pobývat
ve	v	k7c6	v
Svobodných	svobodný	k2eAgInPc6d1	svobodný
Hamrech	Hamry	k1gInPc6	Hamry
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Chrudimkou	Chrudimka	k1gFnSc7	Chrudimka
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
Františka	František	k1gMnSc2	František
Kavána	Kaván	k1gMnSc2	Kaván
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
části	část	k1gFnSc2	část
turistické	turistický	k2eAgFnSc2d1	turistická
trasy	trasa	k1gFnSc2	trasa
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
vedené	vedený	k2eAgFnSc2d1	vedená
podél	podél	k7c2	podél
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
z	z	k7c2	z
Hlinska	Hlinsko	k1gNnSc2	Hlinsko
do	do	k7c2	do
Trhové	trhový	k2eAgFnSc2d1	trhová
Kamenice	Kamenice	k1gFnSc2	Kamenice
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
vlastivědné	vlastivědný	k2eAgFnSc2d1	vlastivědná
stezky	stezka	k1gFnSc2	stezka
Krajem	krajem	k6eAd1	krajem
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Střední	střední	k2eAgInSc1d1	střední
tok	tok	k1gInSc1	tok
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
protéká	protékat	k5eAaImIp3nS	protékat
Národním	národní	k2eAgInSc7d1	národní
geoparkem	geoparek	k1gInSc7	geoparek
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Trhová	trhový	k2eAgFnSc1d1	trhová
Kamenice	Kamenice	k1gFnSc1	Kamenice
až	až	k9	až
přehradní	přehradní	k2eAgFnSc1d1	přehradní
hráz	hráz	k1gFnSc1	hráz
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Práčov	Práčovo	k1gNnPc2	Práčovo
také	také	k6eAd1	také
Chráněnou	chráněný	k2eAgFnSc7d1	chráněná
krajinnou	krajinný	k2eAgFnSc7d1	krajinná
oblastí	oblast	k1gFnSc7	oblast
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Trhovou	trhový	k2eAgFnSc7d1	trhová
Kamenicí	Kamenice	k1gFnSc7	Kamenice
a	a	k8xC	a
Horním	horní	k2eAgNnSc7d1	horní
Bradlem	bradlo	k1gNnSc7	bradlo
<g/>
,	,	kIx,	,
obcí	obec	k1gFnSc7	obec
známou	známý	k2eAgFnSc7d1	známá
sklářskou	sklářský	k2eAgFnSc7d1	sklářská
hutí	huť	k1gFnSc7	huť
rodiny	rodina	k1gFnSc2	rodina
Rücklů	Rückl	k1gInPc2	Rückl
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc2d2	pozdější
výrobou	výroba	k1gFnSc7	výroba
vánočních	vánoční	k2eAgFnPc2d1	vánoční
ozdob	ozdoba	k1gFnPc2	ozdoba
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
v	v	k7c6	v
několika	několik	k4yIc6	několik
meandrech	meandr	k1gInPc6	meandr
s	s	k7c7	s
většími	veliký	k2eAgInPc7d2	veliký
oblouky	oblouk	k1gInPc7	oblouk
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
údolím	údolí	k1gNnSc7	údolí
Kameničské	Kameničský	k2eAgFnSc2d1	Kameničský
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
geomorfologického	geomorfologický	k2eAgInSc2d1	geomorfologický
okrsku	okrsek	k1gInSc2	okrsek
s	s	k7c7	s
názvem	název	k1gInSc7	název
odvozeným	odvozený	k2eAgInSc7d1	odvozený
od	od	k7c2	od
malé	malý	k2eAgFnSc2d1	malá
vesničky	vesnička	k1gFnSc2	vesnička
Kameničky	kamenička	k1gFnSc2	kamenička
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc2d1	ležící
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nad	nad	k7c7	nad
levým	levý	k2eAgInSc7d1	levý
břehem	břeh	k1gInSc7	břeh
řeky	řeka	k1gFnSc2	řeka
strmé	strmý	k2eAgInPc1d1	strmý
svahy	svah	k1gInPc1	svah
lesní	lesní	k2eAgFnSc2d1	lesní
lokality	lokalita	k1gFnSc2	lokalita
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
Polom	polom	k1gInSc1	polom
(	(	kIx(	(
<g/>
649	[number]	k4	649
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Silvestrovským	silvestrovský	k2eAgInSc7d1	silvestrovský
výnosem	výnos	k1gInSc7	výnos
o	o	k7c6	o
prvních	první	k4xOgNnPc6	první
chráněných	chráněný	k2eAgNnPc6d1	chráněné
územích	území	k1gNnPc6	území
v	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Polom	polom	k1gInSc1	polom
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
jedlobukového	jedlobukový	k2eAgInSc2d1	jedlobukový
pralesa	prales	k1gInSc2	prales
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
lesním	lesní	k2eAgInSc7d1	lesní
porostem	porost	k1gInSc7	porost
(	(	kIx(	(
<g/>
na	na	k7c6	na
vlastivědné	vlastivědný	k2eAgFnSc6d1	vlastivědná
stezce	stezka	k1gFnSc6	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
stanoviště	stanoviště	k1gNnSc2	stanoviště
č.	č.	k?	č.
15	[number]	k4	15
s	s	k7c7	s
názvem	název	k1gInSc7	název
Polom	polom	k1gInSc1	polom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
lesní	lesní	k2eAgFnSc1d1	lesní
lokalita	lokalita	k1gFnSc1	lokalita
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgNnPc3d3	nejvýznamnější
v	v	k7c6	v
CHKO	CHKO	kA	CHKO
Železné	železný	k2eAgFnPc4d1	železná
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
prvonálezy	prvonález	k1gInPc4	prvonález
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mykoflóry	mykoflóra	k1gFnSc2	mykoflóra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
Horního	horní	k2eAgNnSc2d1	horní
Bradla	bradlo	k1gNnSc2	bradlo
k	k	k7c3	k
Přemilovu	Přemilův	k2eAgNnSc3d1	Přemilův
protéká	protékat	k5eAaImIp3nS	protékat
více	hodně	k6eAd2	hodně
sevřenějším	sevřený	k2eAgNnSc7d2	sevřenější
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
údolím	údolí	k1gNnSc7	údolí
podél	podél	k7c2	podél
hlavního	hlavní	k2eAgInSc2d1	hlavní
hřebenu	hřeben	k1gInSc2	hřeben
Železných	Železný	k1gMnPc2	Železný
hor.	hor.	k?	hor.
Nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
modře	modro	k6eAd1	modro
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
trasa	trasa	k1gFnSc1	trasa
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
vlastivědné	vlastivědný	k2eAgFnSc2d1	vlastivědná
stezky	stezka	k1gFnSc2	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
tok	toka	k1gFnPc2	toka
řeky	řeka	k1gFnSc2	řeka
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
až	až	k9	až
k	k	k7c3	k
vodní	vodní	k2eAgFnSc3d1	vodní
nádrži	nádrž	k1gFnSc3	nádrž
Seč	seč	k1gFnSc4	seč
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vtoku	vtok	k1gInSc6	vtok
do	do	k7c2	do
přehradní	přehradní	k2eAgFnSc2d1	přehradní
nádrže	nádrž	k1gFnSc2	nádrž
nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
skalní	skalní	k2eAgFnPc4d1	skalní
stěny	stěna	k1gFnPc4	stěna
a	a	k8xC	a
rekreační	rekreační	k2eAgFnSc4d1	rekreační
oblast	oblast	k1gFnSc4	oblast
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Ústupky	ústupek	k1gInPc4	ústupek
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
průtoku	průtok	k1gInSc6	průtok
vodní	vodní	k2eAgMnSc1d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
stáčí	stáčet	k5eAaImIp3nS	stáčet
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
georeliéfu	georeliéf	k1gInSc6	georeliéf
v	v	k7c6	v
období	období	k1gNnSc6	období
kenozoika	kenozoikum	k1gNnSc2	kenozoikum
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
<g/>
,	,	kIx,	,
tekl	téct	k5eAaImAgMnS	téct
po	po	k7c6	po
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
téměř	téměř	k6eAd1	téměř
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Doubravou	Doubrava	k1gFnSc7	Doubrava
<g/>
,	,	kIx,	,
oddělený	oddělený	k2eAgMnSc1d1	oddělený
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
koryta	koryto	k1gNnSc2	koryto
hlavním	hlavní	k2eAgInSc7d1	hlavní
hřebenem	hřeben	k1gInSc7	hřeben
Železných	železný	k2eAgInPc2d1	železný
hor.	hor.	k?	hor.
Podle	podle	k7c2	podle
nálezu	nález	k1gInSc2	nález
sedimentů	sediment	k1gInPc2	sediment
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Počáteckého	Počátecký	k2eAgInSc2d1	Počátecký
a	a	k8xC	a
Zlatého	zlatý	k2eAgInSc2d1	zlatý
potoka	potok	k1gInSc2	potok
v	v	k7c6	v
Podhradské	podhradský	k2eAgFnSc6d1	Podhradská
kotlině	kotlina	k1gFnSc6	kotlina
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
Horní	horní	k2eAgFnSc2d1	horní
Vsi	ves	k1gFnSc2	ves
(	(	kIx(	(
<g/>
sídelní	sídelní	k2eAgFnSc1d1	sídelní
lokalita	lokalita	k1gFnSc1	lokalita
součástí	součást	k1gFnPc2	součást
města	město	k1gNnSc2	město
Seč	seč	k6eAd1	seč
<g/>
)	)	kIx)	)
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
ose	osa	k1gFnSc6	osa
Počáteckého	Počátecký	k2eAgInSc2d1	Počátecký
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
pravostranným	pravostranný	k2eAgInSc7d1	pravostranný
přítokem	přítok	k1gInSc7	přítok
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Doubravy	Doubrava	k1gFnSc2	Doubrava
pod	pod	k7c7	pod
Třemošnicí	Třemošnice	k1gFnSc7	Třemošnice
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
změn	změna	k1gFnPc2	změna
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
byly	být	k5eAaImAgInP	být
tektonické	tektonický	k2eAgInPc1d1	tektonický
pohyby	pohyb	k1gInPc1	pohyb
zemských	zemský	k2eAgFnPc2d1	zemská
desek	deska	k1gFnPc2	deska
podél	podél	k7c2	podél
dnešního	dnešní	k2eAgInSc2d1	dnešní
železnohorského	železnohorský	k2eAgInSc2d1	železnohorský
zlomu	zlom	k1gInSc2	zlom
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mohutná	mohutný	k2eAgFnSc1d1	mohutná
zemská	zemský	k2eAgFnSc1d1	zemská
kra	kra	k1gFnSc1	kra
byla	být	k5eAaImAgFnS	být
vyzdvižena	vyzdvihnout	k5eAaPmNgFnS	vyzdvihnout
a	a	k8xC	a
ukloněna	uklonit	k5eAaPmNgFnS	uklonit
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jihozápad	jihozápad	k1gInSc1	jihozápad
–	–	k?	–
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
dnešního	dnešní	k2eAgInSc2d1	dnešní
toku	tok	k1gInSc2	tok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
od	od	k7c2	od
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Seč	seč	k6eAd1	seč
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
čtvrtohor	čtvrtohory	k1gFnPc2	čtvrtohory
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
linii	linie	k1gFnSc6	linie
Seč	seč	k6eAd1	seč
–	–	k?	–
Chrudim	Chrudim	k1gFnSc1	Chrudim
hluboká	hluboký	k2eAgFnSc1d1	hluboká
údolí	údolí	k1gNnPc1	údolí
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
profil	profil	k1gInSc1	profil
byl	být	k5eAaImAgInS	být
výhodně	výhodně	k6eAd1	výhodně
využit	využít	k5eAaPmNgInS	využít
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
několika	několik	k4yIc2	několik
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
na	na	k7c6	na
Chrudimce	Chrudimka	k1gFnSc6	Chrudimka
vybudováno	vybudován	k2eAgNnSc1d1	vybudováno
vodní	vodní	k2eAgNnSc1d1	vodní
dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Seče	seč	k1gFnSc2	seč
<g/>
,	,	kIx,	,
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
50,7	[number]	k4	50,7
vyzděná	vyzděný	k2eAgFnSc1d1	vyzděná
kameny	kámen	k1gInPc7	kámen
42	[number]	k4	42
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
přehradní	přehradní	k2eAgFnSc1d1	přehradní
hráz	hráz	k1gFnSc1	hráz
<g/>
,	,	kIx,	,
na	na	k7c6	na
odtoku	odtok	k1gInSc6	odtok
výpusti	výpust	k1gFnSc2	výpust
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Seč	seč	k6eAd1	seč
má	mít	k5eAaImIp3nS	mít
stálé	stálý	k2eAgNnSc1d1	stálé
nadržení	nadržení	k1gNnSc1	nadržení
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
1,303	[number]	k4	1,303
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
m3	m3	k4	m3
vody	voda	k1gFnPc1	voda
se	s	k7c7	s
zatopenou	zatopený	k2eAgFnSc7d1	zatopená
plochou	plocha	k1gFnSc7	plocha
27,584	[number]	k4	27,584
ha	ha	kA	ha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
vodárenské	vodárenský	k2eAgNnSc4d1	vodárenské
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
úpravnách	úpravna	k1gFnPc6	úpravna
vody	voda	k1gFnSc2	voda
Seč	seč	k1gFnSc4	seč
a	a	k8xC	a
Monako	Monako	k1gNnSc4	Monako
u	u	k7c2	u
Slatiňan	Slatiňany	k1gInPc2	Slatiňany
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
strmé	strmý	k2eAgInPc1d1	strmý
svahy	svah	k1gInPc1	svah
a	a	k8xC	a
skaliska	skalisko	k1gNnSc2	skalisko
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
prudkým	prudký	k2eAgInSc7d1	prudký
ohybem	ohyb	k1gInSc7	ohyb
řeky	řeka	k1gFnSc2	řeka
se	s	k7c7	s
zátokou	zátoka	k1gFnSc7	zátoka
zvláště	zvláště	k6eAd1	zvláště
chráněné	chráněný	k2eAgNnSc1d1	chráněné
území	území	k1gNnSc1	území
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Oheb	Oheba	k1gFnPc2	Oheba
s	s	k7c7	s
teplomilnými	teplomilný	k2eAgFnPc7d1	teplomilná
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
reliktním	reliktní	k2eAgInSc7d1	reliktní
borem	bor	k1gInSc7	bor
a	a	k8xC	a
také	také	k9	také
suťovými	suťový	k2eAgInPc7d1	suťový
lesy	les	k1gInPc7	les
s	s	k7c7	s
bučinou	bučina	k1gFnSc7	bučina
a	a	k8xC	a
javory	javor	k1gInPc7	javor
<g/>
,	,	kIx,	,
na	na	k7c6	na
strmém	strmý	k2eAgInSc6d1	strmý
skalním	skalní	k2eAgInSc6d1	skalní
ostrohu	ostroh	k1gInSc6	ostroh
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Oheb	Oheba	k1gFnPc2	Oheba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
Sečské	sečský	k2eAgFnSc2d1	Sečská
přehrady	přehrada	k1gFnSc2	přehrada
protéká	protékat	k5eAaImIp3nS	protékat
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
v	v	k7c6	v
hlubokém	hluboký	k2eAgInSc6d1	hluboký
zářezu	zářez	k1gInSc6	zářez
rulami	rula	k1gFnPc7	rula
Sečské	sečský	k2eAgFnSc2d1	Sečská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
skalní	skalní	k2eAgInPc4d1	skalní
útvary	útvar	k1gInPc4	útvar
<g/>
,	,	kIx,	,
též	též	k9	též
Markova	Markův	k2eAgFnSc1d1	Markova
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
po	po	k7c6	po
směru	směr	k1gInSc6	směr
toku	tok	k1gInSc2	tok
špičková	špičkový	k2eAgFnSc1d1	špičková
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
(	(	kIx(	(
<g/>
Francisova	Francisův	k2eAgFnSc1d1	Francisova
turbína	turbína	k1gFnSc1	turbína
<g/>
)	)	kIx)	)
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Na	na	k7c6	na
Bělidle	bělidlo	k1gNnSc6	bělidlo
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyrovnávací	vyrovnávací	k2eAgFnSc7d1	vyrovnávací
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
Padrty	Padrty	k?	Padrty
(	(	kIx(	(
<g/>
též	též	k9	též
Seč	seč	k1gFnSc1	seč
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
47,9	[number]	k4	47,9
<g />
.	.	kIx.	.
</s>
<s>
přehradní	přehradní	k2eAgFnSc1d1	přehradní
hráz	hráz	k1gFnSc1	hráz
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc3	on
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
toku	tok	k1gInSc2	tok
Padrťský	Padrťský	k2eAgInSc1d1	Padrťský
Mlýn	mlýn	k1gInSc1	mlýn
(	(	kIx(	(
<g/>
od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
vedená	vedený	k2eAgFnSc1d1	vedená
vlastivědná	vlastivědný	k2eAgFnSc1d1	vlastivědná
stezka	stezka	k1gFnSc1	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
a	a	k8xC	a
část	část	k1gFnSc4	část
naučné	naučný	k2eAgFnSc2d1	naučná
stezky	stezka	k1gFnSc2	stezka
Historie	historie	k1gFnSc2	historie
vápenictví	vápenictví	k1gNnSc2	vápenictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
obcí	obec	k1gFnSc7	obec
Bojanov	Bojanovo	k1gNnPc2	Bojanovo
při	při	k7c6	při
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
rybník	rybník	k1gInSc4	rybník
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
,	,	kIx,	,
za	za	k7c7	za
Bojanovem	Bojanovo	k1gNnSc7	Bojanovo
meandruje	meandrovat	k5eAaImIp3nS	meandrovat
širokou	široký	k2eAgFnSc7d1	široká
údolní	údolní	k2eAgFnSc7d1	údolní
nivou	niva	k1gFnSc7	niva
s	s	k7c7	s
rybníky	rybník	k1gInPc7	rybník
Bojanovský	Bojanovský	k1gMnSc1	Bojanovský
a	a	k8xC	a
Novomlýnský	novomlýnský	k2eAgMnSc1d1	novomlýnský
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
strmými	strmý	k2eAgInPc7d1	strmý
svahy	svah	k1gInPc7	svah
kopců	kopec	k1gInPc2	kopec
Kameničské	Kameničský	k2eAgFnSc2d1	Kameničský
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Vápenice	vápenice	k1gFnSc2	vápenice
s	s	k7c7	s
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
důlních	důlní	k2eAgFnPc2d1	důlní
štol	štola	k1gFnPc2	štola
<g/>
,	,	kIx,	,
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
ohrožené	ohrožený	k2eAgFnSc2d1	ohrožená
populace	populace	k1gFnSc2	populace
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
41,7	[number]	k4	41,7
most	most	k1gInSc1	most
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
337	[number]	k4	337
(	(	kIx(	(
<g/>
po	po	k7c6	po
mostě	most	k1gInSc6	most
vedena	vést	k5eAaImNgFnS	vést
vlastivědná	vlastivědný	k2eAgFnSc1d1	vlastivědná
stezka	stezka	k1gFnSc1	stezka
Krajem	krajem	k6eAd1	krajem
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
40,8	[number]	k4	40,8
vybudován	vybudovat	k5eAaPmNgMnS	vybudovat
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
jez	jez	k1gInSc1	jez
u	u	k7c2	u
chatovou	chatový	k2eAgFnSc4d1	chatová
osady	osada	k1gFnSc2	osada
Mezisvětí	Mezisvětí	k1gNnSc1	Mezisvětí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
40,4	[number]	k4	40,4
most	most	k1gInSc1	most
s	s	k7c7	s
komunikací	komunikace	k1gFnSc7	komunikace
do	do	k7c2	do
Křižanovic	Křižanovice	k1gFnPc2	Křižanovice
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vzdutí	vzdutí	k1gNnSc2	vzdutí
údolní	údolní	k2eAgFnSc2d1	údolní
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Křižanovice	Křižanovice	k1gFnSc2	Křižanovice
(	(	kIx(	(
<g/>
též	též	k9	též
Křižanovice	Křižanovice	k1gFnSc1	Křižanovice
I	I	kA	I
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
významná	významný	k2eAgFnSc1d1	významná
lokalita	lokalita	k1gFnSc1	lokalita
s	s	k7c7	s
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
keltského	keltský	k2eAgNnSc2d1	keltské
oppida	oppidum	k1gNnSc2	oppidum
(	(	kIx(	(
<g/>
bývalé	bývalý	k2eAgNnSc1d1	bývalé
hradiště	hradiště	k1gNnSc1	hradiště
s	s	k7c7	s
archeologickým	archeologický	k2eAgNnSc7d1	Archeologické
nalezištěm	naleziště	k1gNnSc7	naleziště
<g/>
,	,	kIx,	,
Oppidum	oppidum	k1gNnSc4	oppidum
České	český	k2eAgFnSc2d1	Česká
Lhotice	Lhotice	k1gFnSc2	Lhotice
<g/>
,	,	kIx,	,
též	též	k9	též
Keltská	keltský	k2eAgFnSc1d1	keltská
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
Křižanovice	Křižanovice	k1gFnSc2	Křižanovice
koryto	koryto	k1gNnSc1	koryto
řeky	řeka	k1gFnSc2	řeka
plné	plný	k2eAgFnSc3d1	plná
balvanů	balvan	k1gInPc2	balvan
a	a	k8xC	a
peřejí	peřej	k1gFnPc2	peřej
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
se	se	k3xPyFc4	se
ostře	ostro	k6eAd1	ostro
zařezává	zařezávat	k5eAaImIp3nS	zařezávat
do	do	k7c2	do
nasavrckého	nasavrcký	k2eAgInSc2d1	nasavrcký
žulového	žulový	k2eAgInSc2d1	žulový
masivu	masiv	k1gInSc2	masiv
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
hluboká	hluboký	k2eAgNnPc4d1	hluboké
říční	říční	k2eAgNnPc4d1	říční
údolí	údolí	k1gNnPc4	údolí
typu	typ	k1gInSc2	typ
soutěsky	soutěska	k1gFnSc2	soutěska
se	s	k7c7	s
skálami	skála	k1gFnPc7	skála
a	a	k8xC	a
suťovými	suťový	k2eAgNnPc7d1	suťové
poli	pole	k1gNnPc7	pole
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
Krkanka	Krkanka	k1gFnSc1	Krkanka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pravostranném	pravostranný	k2eAgInSc6d1	pravostranný
bezejmenném	bezejmenný	k2eAgInSc6d1	bezejmenný
přítoku	přítok	k1gInSc6	přítok
(	(	kIx(	(
<g/>
ústí	ústí	k1gNnSc6	ústí
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
365	[number]	k4	365
m	m	kA	m
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
Hradišťský	hradišťský	k2eAgInSc1d1	hradišťský
vodopád	vodopád	k1gInSc1	vodopád
(	(	kIx(	(
<g/>
cca	cca	kA	cca
3	[number]	k4	3
m	m	kA	m
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
135	[number]	k4	135
m	m	kA	m
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
strmým	strmý	k2eAgInSc7d1	strmý
pravým	pravý	k2eAgInSc7d1	pravý
břehem	břeh	k1gInSc7	břeh
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
kolem	kolem	k7c2	kolem
500	[number]	k4	500
m	m	kA	m
leží	ležet	k5eAaImIp3nS	ležet
město	město	k1gNnSc1	město
Nasavrky	Nasavrka	k1gFnSc2	Nasavrka
(	(	kIx(	(
<g/>
pracoviště	pracoviště	k1gNnSc1	pracoviště
správy	správa	k1gFnSc2	správa
CHKO	CHKO	kA	CHKO
Železné	železný	k2eAgFnPc4d1	železná
hory	hora	k1gFnPc4	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
Krkanka	Krkanka	k1gFnSc1	Krkanka
opouští	opouštět	k5eAaImIp3nS	opouštět
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
oblast	oblast	k1gFnSc1	oblast
Kameničské	Kameničský	k2eAgFnSc2d1	Kameničský
vrchoviny	vrchovina	k1gFnSc2	vrchovina
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
Skutečské	skutečský	k2eAgFnSc6d1	Skutečská
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
kaňonovitým	kaňonovitý	k2eAgNnSc7d1	kaňonovité
údolím	údolí	k1gNnSc7	údolí
s	s	k7c7	s
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
Strádovské	Strádovský	k2eAgNnSc4d1	Strádovské
Peklo	peklo	k1gNnSc4	peklo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
31,8	[number]	k4	31,8
most	most	k1gInSc1	most
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
vpravo	vpravo	k6eAd1	vpravo
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Strádov	Strádovo	k1gNnPc2	Strádovo
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
ostrohu	ostroh	k1gInSc6	ostroh
<g/>
,	,	kIx,	,
ohraničeném	ohraničený	k2eAgInSc6d1	ohraničený
údolím	údolí	k1gNnSc7	údolí
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
roklí	rokle	k1gFnPc2	rokle
Libáňského	Libáňský	k2eAgInSc2d1	Libáňský
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
31,3	[number]	k4	31,3
před	před	k7c7	před
mostem	most	k1gInSc7	most
pro	pro	k7c4	pro
pěší	pěší	k2eAgInSc4d1	pěší
začátek	začátek	k1gInSc4	začátek
vzdutí	vzdutí	k1gNnSc2	vzdutí
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Práčov	Práčov	k1gInSc1	Práčov
(	(	kIx(	(
<g/>
též	též	k9	též
Křižanovice	Křižanovice	k1gFnSc1	Křižanovice
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
vesnice	vesnice	k1gFnPc4	vesnice
Práčov	Práčov	k1gInSc1	Práčov
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Svídnice	Svídnice	k1gFnSc2	Svídnice
<g/>
)	)	kIx)	)
s	s	k7c7	s
dominantou	dominanta	k1gFnSc7	dominanta
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
vodárenskou	vodárenský	k2eAgFnSc7d1	vodárenská
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
sypanou	sypaný	k2eAgFnSc7d1	sypaná
hrází	hráz	k1gFnSc7	hráz
přehrady	přehrada	k1gFnSc2	přehrada
se	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
přelivem	přeliv	k1gInSc7	přeliv
opouští	opouštět	k5eAaImIp3nS	opouštět
řeka	řeka	k1gFnSc1	řeka
CHKO	CHKO	kA	CHKO
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
meandruje	meandrovat	k5eAaImIp3nS	meandrovat
údolím	údolí	k1gNnSc7	údolí
kolem	kolem	k7c2	kolem
obce	obec	k1gFnSc2	obec
Svídnice	Svídnice	k1gFnSc2	Svídnice
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
samotou	samota	k1gFnSc7	samota
Brusy	brus	k1gInPc1	brus
se	se	k3xPyFc4	se
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
nachází	nacházet	k5eAaImIp3nS	nacházet
technická	technický	k2eAgFnSc1d1	technická
památka	památka	k1gFnSc1	památka
Lukavická	Lukavická	k1gFnSc1	Lukavická
štola	štola	k1gFnSc1	štola
(	(	kIx(	(
<g/>
na	na	k7c6	na
vlastivědné	vlastivědný	k2eAgFnSc6d1	vlastivědná
stezce	stezka	k1gFnSc6	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
stanoviště	stanoviště	k1gNnSc2	stanoviště
č.	č.	k?	č.
28	[number]	k4	28
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
Škrovádem	Škrovád	k1gInSc7	Škrovád
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
Slatiňan	Slatiňany	k1gInPc2	Slatiňany
<g/>
)	)	kIx)	)
protéká	protékat	k5eAaImIp3nS	protékat
chatovou	chatový	k2eAgFnSc7d1	chatová
oblastí	oblast	k1gFnSc7	oblast
Borek	borek	k1gInSc4	borek
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
bývalými	bývalý	k2eAgInPc7d1	bývalý
pískovcovými	pískovcový	k2eAgInPc7d1	pískovcový
lomy	lom	k1gInPc7	lom
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
vodním	vodní	k2eAgInSc7d1	vodní
mlýnem	mlýn	k1gInSc7	mlýn
Skály	skála	k1gFnSc2	skála
se	s	k7c7	s
stejnojmenným	stejnojmenný	k2eAgInSc7d1	stejnojmenný
rybníkem	rybník	k1gInSc7	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
mostem	most	k1gInSc7	most
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
v	v	k7c6	v
říčním	říční	k2eAgNnSc6d1	říční
km	km	kA	km
26,9	[number]	k4	26,9
km	km	kA	km
opouští	opouštět	k5eAaImIp3nS	opouštět
řeka	řeka	k1gFnSc1	řeka
Skutečskou	skutečský	k2eAgFnSc4d1	Skutečská
pahorkatinu	pahorkatina	k1gFnSc4	pahorkatina
a	a	k8xC	a
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
úseku	úsek	k1gInSc6	úsek
se	se	k3xPyFc4	se
navrací	navracet	k5eAaImIp3nS	navracet
do	do	k7c2	do
Kameničské	Kameničský	k2eAgFnSc2d1	Kameničský
vrchoviny	vrchovina	k1gFnSc2	vrchovina
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
části	část	k1gFnSc2	část
pod	pod	k7c7	pod
lokalitou	lokalita	k1gFnSc7	lokalita
Na	na	k7c6	na
Blehově	Blehův	k2eAgFnSc6d1	Blehův
stráni	stráň	k1gFnSc6	stráň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
24,4	[number]	k4	24,4
ve	v	k7c6	v
Slatiňanech	Slatiňany	k1gInPc6	Slatiňany
řeka	řeka	k1gFnSc1	řeka
opouští	opouštět	k5eAaImIp3nS	opouštět
oblast	oblast	k1gFnSc4	oblast
Sečské	sečský	k2eAgFnSc2d1	Sečská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
(	(	kIx(	(
<g/>
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
nížinné	nížinný	k2eAgFnSc2d1	nížinná
krajiny	krajina	k1gFnSc2	krajina
Chrudimské	chrudimský	k2eAgFnSc2d1	Chrudimská
tabule	tabule	k1gFnSc2	tabule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
===	===	k?	===
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
v	v	k7c6	v
Chrudimské	chrudimský	k2eAgFnSc6d1	Chrudimská
tabuli	tabule	k1gFnSc6	tabule
a	a	k8xC	a
v	v	k7c6	v
Pardubické	pardubický	k2eAgFnSc6d1	pardubická
kotlině	kotlina	k1gFnSc6	kotlina
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
urbanizaci	urbanizace	k1gFnSc3	urbanizace
krajiny	krajina	k1gFnSc2	krajina
značně	značně	k6eAd1	značně
regulován	regulovat	k5eAaImNgInS	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jezem	jez	k1gInSc7	jez
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
23,6	[number]	k4	23,6
vtéká	vtékat	k5eAaImIp3nS	vtékat
na	na	k7c4	na
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
města	město	k1gNnSc2	město
Chrudim	Chrudim	k1gFnSc4	Chrudim
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgInSc2	který
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
zeměpisné	zeměpisný	k2eAgNnSc1d1	zeměpisné
jméno	jméno	k1gNnSc1	jméno
(	(	kIx(	(
<g/>
hydronymum	hydronymum	k1gNnSc1	hydronymum
<g/>
)	)	kIx)	)
řeky	řeka	k1gFnPc1	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
průtok	průtok	k1gInSc4	průtok
městem	město	k1gNnSc7	město
Chrudimí	Chrudim	k1gFnPc2	Chrudim
je	být	k5eAaImIp3nS	být
navázána	navázán	k2eAgFnSc1d1	navázána
soustava	soustava	k1gFnSc1	soustava
bývalých	bývalý	k2eAgInPc2d1	bývalý
mlýnských	mlýnský	k2eAgInPc2d1	mlýnský
náhonů	náhon	k1gInPc2	náhon
<g/>
,	,	kIx,	,
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
20	[number]	k4	20
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
jez	jez	k1gInSc1	jez
<g/>
,	,	kIx,	,
od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
dvě	dva	k4xCgFnPc1	dva
koryta	koryto	k1gNnPc1	koryto
vytvářející	vytvářející	k2eAgInPc4d1	vytvářející
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
při	při	k7c6	při
pravém	pravý	k2eAgNnSc6d1	pravé
rameni	rameno	k1gNnSc6	rameno
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
toku	tok	k1gInSc2	tok
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Ptačí	ptačí	k2eAgInPc1d1	ptačí
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
významné	významný	k2eAgNnSc1d1	významné
hnízdiště	hnízdiště	k1gNnSc1	hnízdiště
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kolonie	kolonie	k1gFnPc1	kolonie
havranů	havran	k1gMnPc2	havran
<g/>
,	,	kIx,	,
též	též	k9	též
park	park	k1gInSc1	park
s	s	k7c7	s
názvem	název	k1gInSc7	název
Střelnice	střelnice	k1gFnSc2	střelnice
<g/>
,	,	kIx,	,
vybudován	vybudován	k2eAgInSc4d1	vybudován
původně	původně	k6eAd1	původně
jako	jako	k9	jako
neveřejný	veřejný	k2eNgMnSc1d1	neveřejný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c7	za
Chrudimí	Chrudim	k1gFnSc7	Chrudim
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
a	a	k8xC	a
mělkém	mělký	k2eAgNnSc6d1	mělké
údolí	údolí	k1gNnSc6	údolí
mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Tuněchody	Tuněchod	k1gInPc4	Tuněchod
a	a	k8xC	a
Úhřetická	Úhřetický	k2eAgFnSc1d1	Úhřetická
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Úhřetickou	Úhřetický	k2eAgFnSc7d1	Úhřetická
Lhotou	Lhota	k1gFnSc7	Lhota
se	se	k3xPyFc4	se
do	do	k7c2	do
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
vlévá	vlévat	k5eAaImIp3nS	vlévat
řeka	řeka	k1gFnSc1	řeka
Novohradka	Novohradka	k1gFnSc1	Novohradka
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
toku	tok	k1gInSc2	tok
značně	značně	k6eAd1	značně
meandruje	meandrovat	k5eAaImIp3nS	meandrovat
<g/>
.	.	kIx.	.
</s>
<s>
Lokalita	lokalita	k1gFnSc1	lokalita
je	být	k5eAaImIp3nS	být
příkladem	příklad	k1gInSc7	příklad
tzv.	tzv.	kA	tzv.
říčního	říční	k2eAgNnSc2d1	říční
pirátství	pirátství	k1gNnSc2	pirátství
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
;	;	kIx,	;
původně	původně	k6eAd1	původně
řeka	řeka	k1gFnSc1	řeka
Novohradka	Novohradka	k1gFnSc1	Novohradka
protékala	protékat	k5eAaImAgFnS	protékat
oblastí	oblast	k1gFnSc7	oblast
jako	jako	k8xS	jako
silně	silně	k6eAd1	silně
erozivní	erozivní	k2eAgInSc4d1	erozivní
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
řeka	řeka	k1gFnSc1	řeka
přitékající	přitékající	k2eAgFnSc1d1	přitékající
od	od	k7c2	od
jihozápadu	jihozápad	k1gInSc2	jihozápad
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Chrudimka	Chrudimek	k1gMnSc2	Chrudimek
<g/>
)	)	kIx)	)
prorazila	prorazit	k5eAaPmAgFnS	prorazit
koryto	koryto	k1gNnSc4	koryto
v	v	k7c6	v
křídové	křídový	k2eAgFnSc6d1	křídová
tabuli	tabule	k1gFnSc6	tabule
<g/>
.	.	kIx.	.
</s>
<s>
Novohradka	Novohradka	k1gFnSc1	Novohradka
následně	následně	k6eAd1	následně
tzv.	tzv.	kA	tzv.
načepovala	načepovat	k5eAaPmAgFnS	načepovat
vodu	voda	k1gFnSc4	voda
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
a	a	k8xC	a
odvedla	odvést	k5eAaPmAgFnS	odvést
ji	on	k3xPp3gFnSc4	on
svým	svůj	k3xOyFgNnSc7	svůj
korytem	koryto	k1gNnSc7	koryto
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
Novohradka	Novohradka	k1gFnSc1	Novohradka
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
pravostranný	pravostranný	k2eAgInSc4d1	pravostranný
přítok	přítok	k1gInSc4	přítok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c7	za
soutokem	soutok	k1gInSc7	soutok
s	s	k7c7	s
Novohradkou	Novohradka	k1gFnSc7	Novohradka
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
Pardubické	pardubický	k2eAgFnSc2d1	pardubická
kotliny	kotlina	k1gFnSc2	kotlina
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
koryto	koryto	k1gNnSc4	koryto
řeky	řeka	k1gFnSc2	řeka
propojené	propojený	k2eAgInPc1d1	propojený
úzkou	úzký	k2eAgFnSc7d1	úzká
strouhou	strouha	k1gFnSc7	strouha
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Ohrádeckou	Ohrádecký	k2eAgFnSc7d1	Ohrádecký
svodnicí	svodnice	k1gFnSc7	svodnice
se	s	k7c7	s
Zminkou	Zminka	k1gFnSc7	Zminka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
velkém	velký	k2eAgInSc6d1	velký
průtoku	průtok	k1gInSc6	průtok
odváděna	odváděn	k2eAgFnSc1d1	odváděna
voda	voda	k1gFnSc1	voda
do	do	k7c2	do
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zaústění	zaústění	k1gNnSc2	zaústění
strouhy	strouha	k1gFnSc2	strouha
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
katastrálním	katastrální	k2eAgNnSc7d1	katastrální
územím	území	k1gNnSc7	území
města	město	k1gNnSc2	město
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
Štětín	štětín	k1gMnSc1	štětín
do	do	k7c2	do
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
ústí	ústit	k5eAaImIp3nS	ústit
levostranný	levostranný	k2eAgInSc1d1	levostranný
přítok	přítok	k1gInSc1	přítok
zvaný	zvaný	k2eAgInSc1d1	zvaný
Náhon	náhon	k1gInSc4	náhon
od	od	k7c2	od
Tuněchod	Tuněchod	k1gInSc4	Tuněchod
a	a	k8xC	a
u	u	k7c2	u
Mnětic	Mnětice	k1gFnPc2	Mnětice
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
7,2	[number]	k4	7,2
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
jez	jez	k1gInSc1	jez
s	s	k7c7	s
náhonem	náhon	k1gInSc7	náhon
pro	pro	k7c4	pro
malou	malý	k2eAgFnSc4d1	malá
vodní	vodní	k2eAgFnSc4d1	vodní
elektrárnu	elektrárna	k1gFnSc4	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Mněticemi	Mnětice	k1gFnPc7	Mnětice
tok	tok	k1gInSc1	tok
Chrudimky	Chrudimek	k1gInPc4	Chrudimek
napřímen	napřímen	k2eAgMnSc1d1	napřímen
<g/>
;	;	kIx,	;
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
je	být	k5eAaImIp3nS	být
značný	značný	k2eAgInSc1d1	značný
počet	počet	k1gInSc1	počet
slepých	slepý	k2eAgNnPc2d1	slepé
ramen	rameno	k1gNnPc2	rameno
pod	pod	k7c7	pod
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
Nemošická	Nemošický	k2eAgFnSc1d1	Nemošická
stráň	stráň	k1gFnSc1	stráň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Matiční	matiční	k2eAgNnSc1d1	matiční
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
<g/>
Zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
m	m	kA	m
před	před	k7c7	před
silničním	silniční	k2eAgInSc7d1	silniční
mostem	most	k1gInSc7	most
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
0,6	[number]	k4	0,6
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
pravostranný	pravostranný	k2eAgInSc4d1	pravostranný
přítok	přítok	k1gInSc4	přítok
z	z	k7c2	z
uměle	uměle	k6eAd1	uměle
vybudovaného	vybudovaný	k2eAgInSc2d1	vybudovaný
kanálu	kanál	k1gInSc2	kanál
Halda	halda	k1gFnSc1	halda
<g/>
,	,	kIx,	,
přivádějící	přivádějící	k2eAgFnSc4d1	přivádějící
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
Loučné	Loučný	k2eAgFnSc2d1	Loučná
do	do	k7c2	do
zaniklé	zaniklý	k2eAgFnSc2d1	zaniklá
pardubické	pardubický	k2eAgFnSc2d1	pardubická
rybniční	rybniční	k2eAgFnSc2d1	rybniční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
turistické	turistický	k2eAgFnSc6d1	turistická
mapě	mapa	k1gFnSc6	mapa
uváděné	uváděný	k2eAgFnSc2d1	uváděná
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
Čičák	Čičák	k1gInSc1	Čičák
a	a	k8xC	a
Velké	velký	k2eAgNnSc1d1	velké
jezero	jezero	k1gNnSc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Odtoky	odtok	k1gInPc1	odtok
z	z	k7c2	z
rybníků	rybník	k1gInPc2	rybník
jsou	být	k5eAaImIp3nP	být
pomocí	pomocí	k7c2	pomocí
vodního	vodní	k2eAgInSc2d1	vodní
kanálu	kanál	k1gInSc2	kanál
svedeny	svést	k5eAaPmNgInP	svést
do	do	k7c2	do
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
toku	tok	k1gInSc2	tok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
v	v	k7c6	v
Pardubické	pardubický	k2eAgFnSc6d1	pardubická
kotlině	kotlina	k1gFnSc6	kotlina
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
malým	malý	k2eAgInSc7d1	malý
spádem	spád	k1gInSc7	spád
i	i	k8xC	i
četnými	četný	k2eAgInPc7d1	četný
jezy	jez	k1gInPc7	jez
a	a	k8xC	a
také	také	k9	také
vzdutím	vzdutí	k1gNnSc7	vzdutí
hladiny	hladina	k1gFnSc2	hladina
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
215,51	[number]	k4	215,51
m	m	kA	m
jako	jako	k8xC	jako
levostranný	levostranný	k2eAgInSc1d1	levostranný
přítok	přítok	k1gInSc1	přítok
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
966,66	[number]	k4	966,66
nad	nad	k7c7	nad
pardubickým	pardubický	k2eAgNnSc7d1	pardubické
zdymadlem	zdymadlo	k1gNnSc7	zdymadlo
s	s	k7c7	s
plavební	plavební	k2eAgFnSc7d1	plavební
komorou	komora	k1gFnSc7	komora
a	a	k8xC	a
malou	malý	k2eAgFnSc7d1	malá
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Větší	veliký	k2eAgInPc1d2	veliký
přítoky	přítok	k1gInPc1	přítok
==	==	k?	==
</s>
</p>
<p>
<s>
Celkově	celkově	k6eAd1	celkově
největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Novohradka	Novohradka	k1gFnSc1	Novohradka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
49,2	[number]	k4	49,2
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
její	její	k3xOp3gInPc1	její
přítoky	přítok	k1gInPc1	přítok
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
říční	říční	k2eAgFnSc2d1	říční
sítě	síť	k1gFnSc2	síť
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
činí	činit	k5eAaImIp3nS	činit
1,25	[number]	k4	1,25
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
povodí	povodí	k1gNnSc6	povodí
nachází	nacházet	k5eAaImIp3nS	nacházet
999	[number]	k4	999
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
kilometru	kilometr	k1gInSc2	kilometr
a	a	k8xC	a
271	[number]	k4	271
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Potoky	potok	k1gInPc1	potok
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
km	km	kA	km
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
tři	tři	k4xCgFnPc1	tři
vodoteče	vodoteč	k1gFnPc1	vodoteč
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
mezi	mezi	k7c7	mezi
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
délce	délka	k1gFnSc6	délka
40	[number]	k4	40
až	až	k9	až
60	[number]	k4	60
km	km	kA	km
se	se	k3xPyFc4	se
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
nalézá	nalézat	k5eAaImIp3nS	nalézat
jeden	jeden	k4xCgInSc4	jeden
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlumětínský	Chlumětínský	k2eAgInSc1d1	Chlumětínský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
100,2	[number]	k4	100,2
</s>
</p>
<p>
<s>
Vortovský	Vortovský	k2eAgInSc1d1	Vortovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
97,1	[number]	k4	97,1
</s>
</p>
<p>
<s>
Valčice	Valčice	k1gFnSc1	Valčice
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
95,9	[number]	k4	95,9
</s>
</p>
<p>
<s>
Blatenský	blatenský	k2eAgInSc1d1	blatenský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
90,0	[number]	k4	90,0
</s>
</p>
<p>
<s>
Drachtinka	Drachtinka	k1gFnSc1	Drachtinka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
88,4	[number]	k4	88,4
</s>
</p>
<p>
<s>
Vítanec	Vítanec	k1gInSc1	Vítanec
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
84,8	[number]	k4	84,8
</s>
</p>
<p>
<s>
Slubice	Slubice	k1gFnSc1	Slubice
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
82,7	[number]	k4	82,7
</s>
</p>
<p>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
79,6	[number]	k4	79,6
</s>
</p>
<p>
<s>
Chobotovský	Chobotovský	k2eAgInSc1d1	Chobotovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
74,0	[number]	k4	74,0
</s>
</p>
<p>
<s>
Rohozenský	Rohozenský	k2eAgInSc1d1	Rohozenský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
73,2	[number]	k4	73,2
</s>
</p>
<p>
<s>
Javorský	Javorský	k1gMnSc1	Javorský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
68,6	[number]	k4	68,6
</s>
</p>
<p>
<s>
Javorenský	Javorenský	k2eAgInSc1d1	Javorenský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
66,6	[number]	k4	66,6
</s>
</p>
<p>
<s>
Vršovský	Vršovský	k2eAgInSc1d1	Vršovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
63,8	[number]	k4	63,8
</s>
</p>
<p>
<s>
Luční	luční	k2eAgInSc1d1	luční
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
63,3	[number]	k4	63,3
</s>
</p>
<p>
<s>
Zlatník	zlatník	k1gInSc1	zlatník
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
44,7	[number]	k4	44,7
</s>
</p>
<p>
<s>
Dehetník	Dehetník	k1gInSc1	Dehetník
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
44,5	[number]	k4	44,5
</s>
</p>
<p>
<s>
Mecký	Mecký	k2eAgInSc1d1	Mecký
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
42,3	[number]	k4	42,3
</s>
</p>
<p>
<s>
Jezerní	jezerní	k2eAgInSc1d1	jezerní
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
<g/>
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
41,0	[number]	k4	41,0
</s>
</p>
<p>
<s>
Debrný	Debrný	k2eAgInSc1d1	Debrný
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
35,6	[number]	k4	35,6
</s>
</p>
<p>
<s>
Libáňský	Libáňský	k2eAgInSc1d1	Libáňský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
32,8	[number]	k4	32,8
</s>
</p>
<p>
<s>
Okrouhlický	okrouhlický	k2eAgInSc1d1	okrouhlický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
29,8	[number]	k4	29,8
</s>
</p>
<p>
<s>
Podhůra	Podhůra	k6eAd1	Podhůra
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
23,8	[number]	k4	23,8
</s>
</p>
<p>
<s>
Novohradka	Novohradka	k1gFnSc1	Novohradka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
13,8	[number]	k4	13,8
</s>
</p>
<p>
<s>
Mnětický	Mnětický	k2eAgInSc1d1	Mnětický
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
4,5	[number]	k4	4,5
</s>
</p>
<p>
<s>
==	==	k?	==
Geologická	geologický	k2eAgFnSc1d1	geologická
zajímavost	zajímavost	k1gFnSc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Chrudimka	Chrudimka	k1gFnSc1	Chrudimka
je	být	k5eAaImIp3nS	být
příkladem	příklad	k1gInSc7	příklad
pirátství	pirátství	k1gNnSc2	pirátství
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeden	jeden	k4xCgMnSc1	jeden
silně	silně	k6eAd1	silně
erodující	erodující	k2eAgInSc1d1	erodující
tok	tok	k1gInSc1	tok
načepuje	načepovat	k5eAaPmIp3nS	načepovat
tok	tok	k1gInSc1	tok
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
počátkem	počátek	k1gInSc7	počátek
čtvrtohor	čtvrtohory	k1gFnPc2	čtvrtohory
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
pod	pod	k7c7	pod
nynější	nynější	k2eAgFnSc7d1	nynější
Sečí	seč	k1gFnSc7	seč
<g/>
.	.	kIx.	.
</s>
<s>
Úklon	úklon	k1gInSc1	úklon
Železných	železný	k2eAgFnPc2d1	železná
hor	hora	k1gFnPc2	hora
k	k	k7c3	k
severovýchodu	severovýchod	k1gInSc3	severovýchod
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgNnSc1d1	probíhající
od	od	k7c2	od
třetihor	třetihory	k1gFnPc2	třetihory
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
nejen	nejen	k6eAd1	nejen
výrazný	výrazný	k2eAgInSc4d1	výrazný
zlomový	zlomový	k2eAgInSc4d1	zlomový
svah	svah	k1gInSc4	svah
nad	nad	k7c7	nad
Čáslavskou	čáslavský	k2eAgFnSc7d1	čáslavská
kotlinou	kotlina	k1gFnSc7	kotlina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožnil	umožnit	k5eAaPmAgMnS	umožnit
potoku	potok	k1gInSc3	potok
tekoucímu	tekoucí	k2eAgInSc3d1	tekoucí
údolím	údolí	k1gNnSc7	údolí
ke	k	k7c3	k
Slatiňanům	Slatiňany	k1gInPc3	Slatiňany
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
načepovat	načepovat	k5eAaPmF	načepovat
původní	původní	k2eAgInSc4d1	původní
tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
tekla	téct	k5eAaImAgFnS	téct
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
údolím	údolí	k1gNnSc7	údolí
nynějšího	nynější	k2eAgInSc2d1	nynější
Počáteckého	Počátecký	k2eAgInSc2d1	Počátecký
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Kraskovským	Kraskovský	k2eAgInSc7d1	Kraskovský
permokarbonem	permokarbon	k1gInSc7	permokarbon
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Peklem	peklo	k1gNnSc7	peklo
a	a	k8xC	a
Hedvičiným	Hedvičin	k2eAgNnSc7d1	Hedvičino
údolím	údolí	k1gNnSc7	údolí
jako	jako	k9	jako
Zlatý	zlatý	k2eAgInSc1d1	zlatý
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc1d1	současný
pravostranný	pravostranný	k2eAgInSc1d1	pravostranný
přítok	přítok	k1gInSc1	přítok
Doubravy	Doubrava	k1gFnSc2	Doubrava
a	a	k8xC	a
vlévala	vlévat	k5eAaImAgFnS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
u	u	k7c2	u
Záboří	Záboří	k1gNnSc2	Záboří
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
náčepního	náčepní	k2eAgInSc2d1	náčepní
lokte	loket	k1gInSc2	loket
tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ohyb	ohyb	k1gInSc1	ohyb
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
i	i	k9	i
hrad	hrad	k1gInSc1	hrad
Oheb	Oheb	k1gInSc1	Oheb
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vystavěn	vystavět	k5eAaPmNgInS	vystavět
na	na	k7c6	na
vysokém	vysoký	k2eAgInSc6d1	vysoký
ostrohu	ostroh	k1gInSc6	ostroh
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
vybudována	vybudován	k2eAgFnSc1d1	vybudována
klenutá	klenutý	k2eAgFnSc1d1	klenutá
zděná	zděný	k2eAgFnSc1d1	zděná
hráz	hráz	k1gFnSc1	hráz
Sečské	sečský	k2eAgFnSc2d1	Sečská
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
koruně	koruna	k1gFnSc6	koruna
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
343	[number]	k4	343
ze	z	k7c2	z
Seče	seč	k1gFnSc2	seč
do	do	k7c2	do
Horního	horní	k2eAgNnSc2d1	horní
Bradla	bradlo	k1gNnSc2	bradlo
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
tunelem	tunel	k1gInSc7	tunel
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
silnice	silnice	k1gFnSc1	silnice
ústí	ústit	k5eAaImIp3nS	ústit
na	na	k7c4	na
hráz	hráz	k1gFnSc4	hráz
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Vildštejn	Vildštejno	k1gNnPc2	Vildštejno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
6,02	[number]	k4	6,02
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Průměrné	průměrný	k2eAgInPc1d1	průměrný
dlouhodobé	dlouhodobý	k2eAgInPc1d1	dlouhodobý
měsíční	měsíční	k2eAgInPc1d1	měsíční
průtoky	průtok	k1gInPc1	průtok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
(	(	kIx(	(
<g/>
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Nemošice	Nemošice	k1gFnSc2	Nemošice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Průměrné	průměrný	k2eAgInPc1d1	průměrný
měsíční	měsíční	k2eAgInPc1d1	měsíční
průtoky	průtok	k1gInPc1	průtok
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
(	(	kIx(	(
<g/>
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Nemošice	Nemošice	k1gFnSc2	Nemošice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgInPc4d1	vybraný
hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgNnPc1d1	vodní
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Přehrady	přehrada	k1gFnPc1	přehrada
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
===	===	k?	===
</s>
</p>
<p>
<s>
Hamry	Hamry	k1gInPc1	Hamry
<g/>
,	,	kIx,	,
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
93,1	[number]	k4	93,1
budována	budovat	k5eAaImNgFnS	budovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
hráz	hráz	k1gFnSc1	hráz
sypaná	sypaný	k2eAgFnSc1d1	sypaná
17,7	[number]	k4	17,7
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
;	;	kIx,	;
funkce	funkce	k1gFnSc1	funkce
vodohospodářská	vodohospodářský	k2eAgFnSc1d1	vodohospodářská
(	(	kIx(	(
<g/>
zajištění	zajištění	k1gNnPc1	zajištění
minimálního	minimální	k2eAgInSc2d1	minimální
průtoku	průtok	k1gInSc2	průtok
v	v	k7c6	v
Chrudimce	Chrudimka	k1gFnSc6	Chrudimka
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
území	území	k1gNnSc2	území
před	před	k7c7	před
povodní	povodeň	k1gFnSc7	povodeň
<g/>
,	,	kIx,	,
akumulace	akumulace	k1gFnSc1	akumulace
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
vodárenské	vodárenský	k2eAgInPc4d1	vodárenský
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
a	a	k8xC	a
účelové	účelový	k2eAgNnSc1d1	účelové
rybí	rybí	k2eAgNnSc1d1	rybí
hospodářství	hospodářství	k1gNnSc1	hospodářství
se	s	k7c7	s
zákazem	zákaz	k1gInSc7	zákaz
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seč	seč	k1gFnSc1	seč
(	(	kIx(	(
<g/>
též	též	k9	též
Seč	seč	k6eAd1	seč
I	i	k9	i
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
50,7	[number]	k4	50,7
budována	budovat	k5eAaImNgFnS	budovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
hráz	hráz	k1gFnSc1	hráz
zděná	zděný	k2eAgFnSc1d1	zděná
42	[number]	k4	42
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
na	na	k7c6	na
výpusti	výpust	k1gFnSc6	výpust
<g/>
;	;	kIx,	;
funkce	funkce	k1gFnSc1	funkce
vodohospodářská	vodohospodářský	k2eAgFnSc1d1	vodohospodářská
(	(	kIx(	(
<g/>
zajištění	zajištění	k1gNnPc1	zajištění
minimálního	minimální	k2eAgInSc2d1	minimální
průtoku	průtok	k1gInSc2	průtok
v	v	k7c6	v
Chrudimce	Chrudimka	k1gFnSc6	Chrudimka
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
území	území	k1gNnSc2	území
před	před	k7c7	před
povodní	povodeň	k1gFnSc7	povodeň
<g/>
,	,	kIx,	,
akumulace	akumulace	k1gFnSc1	akumulace
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
vodárenské	vodárenský	k2eAgInPc4d1	vodárenský
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
energetická	energetický	k2eAgFnSc1d1	energetická
(	(	kIx(	(
<g/>
špičková	špičkový	k2eAgFnSc1d1	špičková
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Seč	seč	k1gFnSc1	seč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
účelové	účelový	k2eAgNnSc1d1	účelové
rybí	rybí	k2eAgNnSc1d1	rybí
hospodářství	hospodářství	k1gNnSc1	hospodářství
(	(	kIx(	(
<g/>
sportovní	sportovní	k2eAgInSc1d1	sportovní
rybolov	rybolov	k1gInSc1	rybolov
<g/>
)	)	kIx)	)
a	a	k8xC	a
rekreační	rekreační	k2eAgInPc4d1	rekreační
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
koupání	koupání	k1gNnSc1	koupání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Padrty	Padrty	k?	Padrty
(	(	kIx(	(
<g/>
též	též	k9	též
Seč	seč	k1gFnSc1	seč
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
47,92	[number]	k4	47,92
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
hráz	hráz	k1gFnSc1	hráz
zemní	zemnit	k5eAaImIp3nS	zemnit
8	[number]	k4	8
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
;	;	kIx,	;
funkce	funkce	k1gFnSc1	funkce
vyrovnávání	vyrovnávání	k1gNnSc2	vyrovnávání
odtoku	odtok	k1gInSc2	odtok
vody	voda	k1gFnSc2	voda
ze	z	k7c2	z
špičkové	špičkový	k2eAgFnSc2d1	špičková
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
Seč	seč	k1gFnSc1	seč
(	(	kIx(	(
<g/>
Francisova	Francisův	k2eAgFnSc1d1	Francisova
turbína	turbína	k1gFnSc1	turbína
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
energetické	energetický	k2eAgNnSc4d1	energetické
využití	využití	k1gNnSc4	využití
malou	malý	k2eAgFnSc7d1	malá
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
přehradní	přehradní	k2eAgFnSc2d1	přehradní
hráze	hráz	k1gFnSc2	hráz
(	(	kIx(	(
<g/>
dvě	dva	k4xCgFnPc1	dva
turbíny	turbína	k1gFnPc1	turbína
Banki	Banki	k1gNnSc1	Banki
60	[number]	k4	60
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křižanovice	Křižanovice	k1gFnSc1	Křižanovice
(	(	kIx(	(
<g/>
též	též	k9	též
Křižanovice	Křižanovice	k1gFnSc1	Křižanovice
I	I	kA	I
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
37,15	[number]	k4	37,15
budována	budovat	k5eAaImNgFnS	budovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
hráz	hráz	k1gFnSc1	hráz
zděná	zděný	k2eAgFnSc1d1	zděná
31,7	[number]	k4	31,7
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
na	na	k7c6	na
výpusti	výpust	k1gFnSc6	výpust
<g/>
;	;	kIx,	;
funkce	funkce	k1gFnSc1	funkce
vodohospodářská	vodohospodářský	k2eAgFnSc1d1	vodohospodářská
<g/>
,	,	kIx,	,
energetická	energetický	k2eAgFnSc1d1	energetická
(	(	kIx(	(
<g/>
špičková	špičkový	k2eAgFnSc1d1	špičková
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Práčov	Práčovo	k1gNnPc2	Práčovo
<g/>
,	,	kIx,	,
též	též	k9	též
Práčov	Práčov	k1gInSc1	Práčov
I	i	k9	i
<g/>
)	)	kIx)	)
a	a	k8xC	a
účelové	účelový	k2eAgNnSc1d1	účelové
rybí	rybí	k2eAgNnSc1d1	rybí
hospodářství	hospodářství	k1gNnSc1	hospodářství
se	s	k7c7	s
zákazem	zákaz	k1gInSc7	zákaz
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Práčov	Práčov	k1gInSc1	Práčov
(	(	kIx(	(
<g/>
též	též	k9	též
Křižanovice	Křižanovice	k1gFnSc1	Křižanovice
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
říčním	říční	k2eAgInSc6d1	říční
km	km	kA	km
30,4	[number]	k4	30,4
budována	budovat	k5eAaImNgFnS	budovat
současně	současně	k6eAd1	současně
s	s	k7c7	s
vodním	vodní	k2eAgInSc7d1	vodní
dílem	díl	k1gInSc7	díl
Křižanovice	Křižanovice	k1gFnSc2	Křižanovice
v	v	k7c6	v
letech	let	k1gInPc6	let
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
hráz	hráz	k1gFnSc1	hráz
zemní	zemnit	k5eAaImIp3nS	zemnit
11	[number]	k4	11
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
;	;	kIx,	;
funkce	funkce	k1gFnSc1	funkce
vyrovnávání	vyrovnávání	k1gNnSc2	vyrovnávání
odtoku	odtok	k1gInSc2	odtok
vody	voda	k1gFnSc2	voda
ze	z	k7c2	z
špičkové	špičkový	k2eAgFnSc2d1	špičková
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
Práčov	Práčov	k1gInSc1	Práčov
I	i	k8xC	i
(	(	kIx(	(
<g/>
Francisova	Francisův	k2eAgFnSc1d1	Francisova
turbína	turbína	k1gFnSc1	turbína
<g/>
)	)	kIx)	)
a	a	k8xC	a
energetické	energetický	k2eAgNnSc4d1	energetické
využití	využití	k1gNnSc4	využití
malou	malý	k2eAgFnSc7d1	malá
vodní	vodní	k2eAgFnSc7d1	vodní
elektrárnou	elektrárna	k1gFnSc7	elektrárna
Práčov	Práčov	k1gInSc1	Práčov
II	II	kA	II
(	(	kIx(	(
<g/>
Kaplanova	Kaplanův	k2eAgFnSc1d1	Kaplanova
turbína	turbína	k1gFnSc1	turbína
<g/>
)	)	kIx)	)
na	na	k7c6	na
výpusti	výpust	k1gFnSc6	výpust
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
přehradní	přehradní	k2eAgFnSc2d1	přehradní
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastivědná	vlastivědný	k2eAgFnSc1d1	vlastivědná
stezka	stezka	k1gFnSc1	stezka
Krajem	kraj	k1gInSc7	kraj
Chrudimky	Chrudimka	k1gFnSc2	Chrudimka
<g/>
,	,	kIx,	,
výchozí	výchozí	k2eAgMnSc1d1	výchozí
místo	místo	k6eAd1	místo
Filipovský	Filipovský	k2eAgInSc4d1	Filipovský
pramen	pramen	k1gInSc4	pramen
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
město	město	k1gNnSc1	město
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
,	,	kIx,	,
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
82	[number]	k4	82
km	km	kA	km
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
překonává	překonávat	k5eAaImIp3nS	překonávat
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
390	[number]	k4	390
m	m	kA	m
a	a	k8xC	a
na	na	k7c6	na
jednatřiceti	jednatřicet	k4xCc6	jednatřicet
přírodovědně	přírodovědně	k6eAd1	přírodovědně
a	a	k8xC	a
kulturně	kulturně	k6eAd1	kulturně
zajímavých	zajímavý	k2eAgNnPc6d1	zajímavé
místech	místo	k1gNnPc6	místo
vybavena	vybavit	k5eAaPmNgFnS	vybavit
informačními	informační	k2eAgFnPc7d1	informační
tabulemi	tabule	k1gFnPc7	tabule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Filipovský	Filipovský	k2eAgInSc4d1	Filipovský
pramen	pramen	k1gInSc4	pramen
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chrudimka	Chrudimek	k1gMnSc2	Chrudimek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Hamry	Hamry	k1gInPc1	Hamry
–	–	k?	–
aktuální	aktuální	k2eAgInSc4d1	aktuální
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
</s>
</p>
<p>
<s>
Přemilov	Přemilov	k1gInSc1	Přemilov
–	–	k?	–
aktuální	aktuální	k2eAgInSc1d1	aktuální
vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
</s>
</p>
<p>
<s>
Padrty	Padrty	k?	Padrty
–	–	k?	–
aktuální	aktuální	k2eAgInSc4d1	aktuální
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
</s>
</p>
<p>
<s>
Svídnice	Svídnice	k1gFnSc1	Svídnice
–	–	k?	–
aktuální	aktuální	k2eAgInSc4d1	aktuální
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
</s>
</p>
<p>
<s>
Nemošice	Nemošice	k1gFnSc1	Nemošice
–	–	k?	–
aktuální	aktuální	k2eAgInSc4d1	aktuální
vodní	vodní	k2eAgInSc4d1	vodní
stav	stav	k1gInSc4	stav
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c6	na
Chrudimce	Chrudimka	k1gFnSc6	Chrudimka
</s>
</p>
