<s>
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Pürglitz	Pürglitz	k1gInSc1	Pürglitz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
a	a	k8xC	a
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
středověkých	středověký	k2eAgInPc2d1	středověký
hradů	hrad	k1gInPc2	hrad
českých	český	k2eAgMnPc2d1	český
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
285	[number]	k4	285
<g/>
-	-	kIx~	-
<g/>
290	[number]	k4	290
metrů	metr	k1gInPc2	metr
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
ostrohu	ostroh	k1gInSc6	ostroh
nad	nad	k7c7	nad
Rakovnickým	rakovnický	k2eAgInSc7d1	rakovnický
potokem	potok	k1gInSc7	potok
<g/>
,	,	kIx,	,
přítokem	přítok	k1gInSc7	přítok
řeky	řeka	k1gFnSc2	řeka
Berounky	Berounka	k1gFnSc2	Berounka
na	na	k7c6	na
území	území	k1gNnSc6	území
městyse	městys	k1gInSc2	městys
Křivoklátu	Křivoklát	k1gInSc2	Křivoklát
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rakovník	Rakovník	k1gInSc1	Rakovník
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zpřístupněný	zpřístupněný	k2eAgInSc1d1	zpřístupněný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xS	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
ČR	ČR	kA	ČR
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
národních	národní	k2eAgFnPc2d1	národní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4	jeho
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
zpřístupnění	zpřístupnění	k1gNnSc1	zpřístupnění
veřejnosti	veřejnost	k1gFnSc2	veřejnost
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
nejprve	nejprve	k6eAd1	nejprve
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
lovecký	lovecký	k2eAgInSc1d1	lovecký
hrad	hrad	k1gInSc1	hrad
knížat	kníže	k1gMnPc2wR	kníže
a	a	k8xC	a
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Kosmas	Kosmas	k1gMnSc1	Kosmas
se	se	k3xPyFc4	se
o	o	k7c6	o
Křivoklátu	Křivoklát	k1gInSc2	Křivoklát
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
lety	let	k1gInPc7	let
1110	[number]	k4	1110
<g/>
-	-	kIx~	-
<g/>
1113	[number]	k4	1113
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
měl	mít	k5eAaImAgMnS	mít
kníže	kníže	k1gMnSc1	kníže
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
věznit	věznit	k5eAaImF	věznit
svého	svůj	k3xOyFgMnSc4	svůj
bratrance	bratranec	k1gMnSc4	bratranec
Otu	Ota	k1gMnSc4	Ota
Olomouckého	olomoucký	k2eAgMnSc4d1	olomoucký
<g/>
.	.	kIx.	.
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1	archeologický
průzkum	průzkum	k1gInSc1	průzkum
však	však	k9	však
žádné	žádný	k3yNgNnSc1	žádný
osídlení	osídlení	k1gNnSc1	osídlení
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
neprokázal	prokázat	k5eNaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Kosmova	Kosmův	k2eAgFnSc1d1	Kosmova
zmínka	zmínka	k1gFnSc1	zmínka
týká	týkat	k5eAaImIp3nS	týkat
jiného	jiný	k2eAgNnSc2d1	jiné
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průzkumu	průzkum	k1gInSc6	průzkum
starého	starý	k2eAgInSc2d1	starý
valu	val	k1gInSc2	val
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
parkánu	parkán	k1gInSc6	parkán
byly	být	k5eAaImAgInP	být
nalezeny	nalezen	k2eAgInPc1d1	nalezen
střepy	střep	k1gInPc1	střep
datované	datovaný	k2eAgInPc1d1	datovaný
do	do	k7c2	do
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrad	hrad	k1gInSc1	hrad
zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
roku	rok	k1gInSc2	rok
1110	[number]	k4	1110
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
současného	současný	k2eAgInSc2d1	současný
<g/>
,	,	kIx,	,
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Ostrožna	ostrožna	k1gFnSc1	ostrožna
byla	být	k5eAaImAgFnS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
již	již	k6eAd1	již
v	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
<g/>
.	.	kIx.	.
</s>
<s>
Archeologickým	archeologický	k2eAgInSc7d1	archeologický
výzkumem	výzkum	k1gInSc7	výzkum
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
parkánu	parkán	k1gInSc2	parkán
nalezena	nalézt	k5eAaBmNgFnS	nalézt
keramika	keramika	k1gFnSc1	keramika
štítarské	štítarský	k2eAgFnSc2d1	štítarská
kultury	kultura	k1gFnSc2	kultura
z	z	k7c2	z
pozdní	pozdní	k2eAgFnSc2d1	pozdní
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnPc1d1	bronzová
<g/>
,	,	kIx,	,
zbytky	zbytek	k1gInPc1	zbytek
roštové	roštový	k2eAgFnSc2d1	roštová
konstrukce	konstrukce	k1gFnSc2	konstrukce
a	a	k8xC	a
část	část	k1gFnSc4	část
chaty	chata	k1gFnSc2	chata
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
obranný	obranný	k2eAgInSc1d1	obranný
příkop	příkop	k1gInSc1	příkop
ze	z	k7c2	z
stejného	stejný	k2eAgNnSc2d1	stejné
období	období	k1gNnSc2	období
zachycený	zachycený	k2eAgInSc1d1	zachycený
ve	v	k7c6	v
výkopu	výkop	k1gInSc6	výkop
v	v	k7c6	v
bráně	brána	k1gFnSc6	brána
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozmístění	rozmístění	k1gNnSc2	rozmístění
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
nálezů	nález	k1gInPc2	nález
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
štítarské	štítarský	k2eAgNnSc1d1	štítarský
hradiště	hradiště	k1gNnSc1	hradiště
bylo	být	k5eAaImAgNnS	být
rozsáhlejší	rozsáhlý	k2eAgInSc4d2	rozsáhlejší
než	než	k8xS	než
středověký	středověký	k2eAgInSc4d1	středověký
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nádvoří	nádvoří	k1gNnSc2	nádvoří
dále	daleko	k6eAd2	daleko
pocházejí	pocházet	k5eAaImIp3nP	pocházet
nálezy	nález	k1gInPc1	nález
keramiky	keramika	k1gFnSc2	keramika
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
halštatské	halštatský	k2eAgFnSc2d1	halštatská
a	a	k8xC	a
laténské	laténský	k2eAgFnSc2d1	laténská
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc4	hrad
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
Stavební	stavební	k2eAgFnSc1d1	stavební
práce	práce	k1gFnSc1	práce
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
i	i	k9	i
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
sídelních	sídelní	k2eAgInPc2d1	sídelní
hradů	hrad	k1gInPc2	hrad
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
spravován	spravovat	k5eAaImNgMnS	spravovat
purkrabím	purkrabí	k1gMnSc7	purkrabí
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zde	zde	k6eAd1	zde
pobýval	pobývat	k5eAaImAgMnS	pobývat
Václav	Václav	k1gMnSc1	Václav
I.	I.	kA	I.
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
dokončil	dokončit	k5eAaPmAgMnS	dokončit
přestavbu	přestavba	k1gFnSc4	přestavba
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
zbudovat	zbudovat	k5eAaPmF	zbudovat
hradní	hradní	k2eAgFnSc4d1	hradní
kapli	kaple	k1gFnSc4	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1316	[number]	k4	1316
se	se	k3xPyFc4	se
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
na	na	k7c4	na
Křivoklát	Křivoklát	k1gInSc4	Křivoklát
uchýlila	uchýlit	k5eAaPmAgFnS	uchýlit
královna	královna	k1gFnSc1	královna
Eliška	Eliška	k1gFnSc1	Eliška
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roztržce	roztržka	k1gFnSc6	roztržka
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
s	s	k7c7	s
Eliškou	Eliška	k1gFnSc7	Eliška
Přemyslovnou	Přemyslovna	k1gFnSc7	Přemyslovna
a	a	k8xC	a
dvouměsíčním	dvouměsíční	k2eAgNnSc6d1	dvouměsíční
uvěznění	uvěznění	k1gNnSc6	uvěznění
na	na	k7c6	na
Lokti	loket	k1gInSc6	loket
zde	zde	k6eAd1	zde
v	v	k7c6	v
odloučení	odloučení	k1gNnSc6	odloučení
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
prožil	prožít	k5eAaPmAgMnS	prožít
dětství	dětství	k1gNnSc4	dětství
(	(	kIx(	(
<g/>
1319	[number]	k4	1319
<g/>
-	-	kIx~	-
<g/>
1323	[number]	k4	1323
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1316	[number]	k4	1316
<g/>
-	-	kIx~	-
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
jako	jako	k9	jako
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1323	[number]	k4	1323
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
o	o	k7c4	o
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
vrátil	vrátit	k5eAaPmAgInS	vrátit
i	i	k9	i
s	s	k7c7	s
těhotnou	těhotná	k1gFnSc7	těhotná
manželkou	manželka	k1gFnSc7	manželka
Blankou	Blanka	k1gFnSc7	Blanka
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Křivoklátě	Křivoklát	k1gInSc6	Křivoklát
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
jejich	jejich	k3xOp3gFnSc1	jejich
dcera	dcera	k1gFnSc1	dcera
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgFnSc1d1	budoucí
uherská	uherský	k2eAgFnSc1d1	uherská
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
však	však	k9	však
Karel	Karel	k1gMnSc1	Karel
dával	dávat	k5eAaImAgMnS	dávat
přednost	přednost	k1gFnSc4	přednost
jím	on	k3xPp3gNnSc7	on
založenému	založený	k2eAgInSc3d1	založený
hradu	hrad	k1gInSc2	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zde	zde	k6eAd1	zde
pobýval	pobývat	k5eAaImAgMnS	pobývat
před	před	k7c7	před
postavením	postavení	k1gNnSc7	postavení
hradu	hrad	k1gInSc2	hrad
Točník	Točník	k1gInSc4	Točník
i	i	k9	i
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
mnohokrát	mnohokrát	k6eAd1	mnohokrát
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
těžce	těžce	k6eAd1	těžce
poškozovaly	poškozovat	k5eAaImAgInP	poškozovat
požáry	požár	k1gInPc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Hořelo	hořet	k5eAaImAgNnS	hořet
zde	zde	k6eAd1	zde
každých	každý	k3xTgNnPc2	každý
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1422	[number]	k4	1422
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
poškozen	poškodit	k5eAaPmNgInS	poškodit
požárem	požár	k1gInSc7	požár
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
požáru	požár	k1gInSc6	požár
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Manský	manský	k2eAgInSc1d1	manský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
relikty	relikt	k1gInPc1	relikt
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
archeologicky	archeologicky	k6eAd1	archeologicky
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
Tomáš	Tomáš	k1gMnSc1	Tomáš
Durdík	Durdík	k1gMnSc1	Durdík
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
tohoto	tento	k3xDgInSc2	tento
výzkumu	výzkum	k1gInSc2	výzkum
nejen	nejen	k6eAd1	nejen
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
zcela	zcela	k6eAd1	zcela
unikátní	unikátní	k2eAgInSc4d1	unikátní
soubor	soubor	k1gInSc4	soubor
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
luxusních	luxusní	k2eAgInPc2d1	luxusní
předmětů	předmět	k1gInPc2	předmět
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
zevrubné	zevrubný	k2eAgNnSc1d1	zevrubné
poznání	poznání	k1gNnSc1	poznání
zcela	zcela	k6eAd1	zcela
ojediněle	ojediněle	k6eAd1	ojediněle
dochované	dochovaný	k2eAgFnSc2d1	dochovaná
archeologické	archeologický	k2eAgFnSc2d1	archeologická
lokality	lokalita	k1gFnSc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Manský	manský	k2eAgInSc1d1	manský
dům	dům	k1gInSc1	dům
totiž	totiž	k9	totiž
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
formě	forma	k1gFnSc6	forma
obnoven	obnoven	k2eAgMnSc1d1	obnoven
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
zbytky	zbytek	k1gInPc1	zbytek
byly	být	k5eAaImAgInP	být
ponechány	ponechat	k5eAaPmNgInP	ponechat
zcela	zcela	k6eAd1	zcela
bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
zarostly	zarůst	k5eAaPmAgInP	zarůst
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1471	[number]	k4	1471
nechal	nechat	k5eAaPmAgInS	nechat
hrad	hrad	k1gInSc1	hrad
nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgInSc1d1	jagellonský
vykoupit	vykoupit	k5eAaPmF	vykoupit
a	a	k8xC	a
pozdněgoticky	pozdněgoticky	k6eAd1	pozdněgoticky
přestavět	přestavět	k5eAaPmF	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
hrad	hrad	k1gInSc1	hrad
získal	získat	k5eAaPmAgInS	získat
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
rysech	rys	k1gInPc6	rys
dochoval	dochovat	k5eAaPmAgInS	dochovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
hrad	hrad	k1gInSc1	hrad
stal	stát	k5eAaPmAgInS	stát
majetkem	majetek	k1gInSc7	majetek
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
zřídili	zřídit	k5eAaPmAgMnP	zřídit
státní	státní	k2eAgFnSc4d1	státní
věznici	věznice	k1gFnSc4	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
Edward	Edward	k1gMnSc1	Edward
Kelley	Kellea	k1gFnSc2	Kellea
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
požár	požár	k1gInSc4	požár
hrad	hrad	k1gInSc4	hrad
téměř	téměř	k6eAd1	téměř
zničil	zničit	k5eAaPmAgMnS	zničit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1597	[number]	k4	1597
<g/>
.	.	kIx.	.
</s>
<s>
Hořelo	hořet	k5eAaImAgNnS	hořet
zde	zde	k6eAd1	zde
i	i	k9	i
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1643	[number]	k4	1643
zde	zde	k6eAd1	zde
požár	požár	k1gInSc1	požár
poškodil	poškodit	k5eAaPmAgInS	poškodit
38	[number]	k4	38
místností	místnost	k1gFnPc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Valdštejnů	Valdštejn	k1gMnPc2	Valdštejn
a	a	k8xC	a
Fürstenberků	Fürstenberka	k1gMnPc2	Fürstenberka
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
postihl	postihnout	k5eAaPmAgInS	postihnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1826	[number]	k4	1826
<g/>
,	,	kIx,	,
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
vrchního	vrchní	k2eAgMnSc2d1	vrchní
správce	správce	k1gMnSc2	správce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
prošel	projít	k5eAaPmAgInS	projít
hrad	hrad	k1gInSc4	hrad
výraznou	výrazný	k2eAgFnSc7d1	výrazná
puristickou	puristický	k2eAgFnSc7d1	puristická
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
stáli	stát	k5eAaImAgMnP	stát
architekti	architekt	k1gMnPc1	architekt
dostavby	dostavba	k1gFnSc2	dostavba
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
Josef	Josef	k1gMnSc1	Josef
Mocker	Mocker	k1gMnSc1	Mocker
a	a	k8xC	a
Kamil	Kamil	k1gMnSc1	Kamil
Hilbert	Hilbert	k1gMnSc1	Hilbert
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
zásahem	zásah	k1gInSc7	zásah
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Hilbertova	Hilbertův	k2eAgFnSc1d1	Hilbertova
znovuvýstavba	znovuvýstavba	k1gFnSc1	znovuvýstavba
Královnina	královnin	k2eAgNnSc2d1	královnino
(	(	kIx(	(
<g/>
severního	severní	k2eAgNnSc2d1	severní
<g/>
)	)	kIx)	)
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
zřítilo	zřítit	k5eAaPmAgNnS	zřítit
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
byl	být	k5eAaImAgInS	být
pečlivě	pečlivě	k6eAd1	pečlivě
rozebrán	rozebrán	k2eAgInSc1d1	rozebrán
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
architektonických	architektonický	k2eAgInPc2d1	architektonický
detailů	detail	k1gInPc2	detail
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jen	jen	k9	jen
přízemí	přízemí	k1gNnSc1	přízemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
prodal	prodat	k5eAaPmAgMnS	prodat
Max	Max	k1gMnSc1	Max
Egon	Egon	k1gMnSc1	Egon
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Fürstenberg	Fürstenberg	k1gInSc1	Fürstenberg
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
Československému	československý	k2eAgInSc3d1	československý
státu	stát	k1gInSc3	stát
za	za	k7c4	za
118,5	[number]	k4	118,5
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hrad	hrad	k1gInSc1	hrad
spravuje	spravovat	k5eAaImIp3nS	spravovat
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
sloužil	sloužit	k5eAaImAgInS	sloužit
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Habsburků	Habsburk	k1gMnPc2	Habsburk
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
jako	jako	k8xS	jako
obávané	obávaný	k2eAgNnSc4d1	obávané
státní	státní	k2eAgNnSc4d1	státní
vězení	vězení	k1gNnSc4	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vězení	vězení	k1gNnSc4	vězení
sloužil	sloužit	k5eAaImAgMnS	sloužit
ovšem	ovšem	k9	ovšem
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Slavnými	slavný	k2eAgMnPc7d1	slavný
vězni	vězeň	k1gMnPc7	vězeň
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
Ota	Ota	k1gMnSc1	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
(	(	kIx(	(
<g/>
1085	[number]	k4	1085
<g/>
-	-	kIx~	-
<g/>
1126	[number]	k4	1126
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1110	[number]	k4	1110
ho	on	k3xPp3gInSc4	on
zde	zde	k6eAd1	zde
uvěznil	uvěznit	k5eAaPmAgMnS	uvěznit
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
Jindřich	Jindřich	k1gMnSc1	Jindřich
ze	z	k7c2	z
Šumburku	Šumburk	k1gInSc2	Šumburk
<g/>
,	,	kIx,	,
vyšehradský	vyšehradský	k2eAgMnSc1d1	vyšehradský
kanovník	kanovník	k1gMnSc1	kanovník
<g/>
,	,	kIx,	,
uvězněn	uvězněn	k2eAgInSc1d1	uvězněn
roku	rok	k1gInSc2	rok
1318	[number]	k4	1318
Jindřich	Jindřich	k1gMnSc1	Jindřich
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
rakouský	rakouský	k2eAgMnSc1d1	rakouský
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1322	[number]	k4	1322
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1316	[number]	k4	1316
<g/>
-	-	kIx~	-
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
dětství	dětství	k1gNnSc6	dětství
čtyři	čtyři	k4xCgMnPc1	čtyři
předáci	předák	k1gMnPc1	předák
povstání	povstání	k1gNnSc2	povstání
kutnohorských	kutnohorský	k2eAgMnPc2d1	kutnohorský
havířů	havíř	k1gMnPc2	havíř
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1496	[number]	k4	1496
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
popraveni	popraven	k2eAgMnPc1d1	popraven
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
omráčil	omráčit	k5eAaPmAgMnS	omráčit
kata	kat	k1gMnSc4	kat
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgMnPc2d1	další
deset	deset	k4xCc1	deset
vůdců	vůdce	k1gMnPc2	vůdce
povstání	povstání	k1gNnSc2	povstání
bylo	být	k5eAaImAgNnS	být
popraveno	popravit	k5eAaPmNgNnS	popravit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
kostela	kostel	k1gInSc2	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
Poděbradech	Poděbrady	k1gInPc6	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Augusta	Augusta	k1gMnSc1	Augusta
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
<g/>
-	-	kIx~	-
<g/>
1572	[number]	k4	1572
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
Jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
<g/>
,	,	kIx,	,
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
byl	být	k5eAaImAgMnS	být
vězněn	věznit	k5eAaImNgMnS	věznit
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
šestnáct	šestnáct	k4xCc1	šestnáct
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1547	[number]	k4	1547
<g/>
-	-	kIx~	-
<g/>
1563	[number]	k4	1563
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlídalo	hlídat	k5eAaImAgNnS	hlídat
ho	on	k3xPp3gMnSc4	on
dvacet	dvacet	k4xCc4	dvacet
strážných	strážný	k1gMnPc2	strážný
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
nemohl	moct	k5eNaImAgMnS	moct
domlouvat	domlouvat	k5eAaImF	domlouvat
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
přistižen	přistižen	k2eAgMnSc1d1	přistižen
a	a	k8xC	a
mučen	mučen	k2eAgMnSc1d1	mučen
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Bílek	Bílek	k1gMnSc1	Bílek
(	(	kIx(	(
<g/>
1516	[number]	k4	1516
<g/>
-	-	kIx~	-
<g/>
1581	[number]	k4	1581
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bratrský	bratrský	k2eAgMnSc1d1	bratrský
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
sekretář	sekretář	k1gMnSc1	sekretář
Jana	Jan	k1gMnSc2	Jan
Augusty	Augusta	k1gMnSc2	Augusta
<g/>
,	,	kIx,	,
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
Augustovy	Augustův	k2eAgFnPc4d1	Augustova
útrapy	útrapa	k1gFnPc4	útrapa
<g/>
,	,	kIx,	,
vězněn	věznit	k5eAaImNgMnS	věznit
třináct	třináct	k4xCc4	třináct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
Kelley	Kellea	k1gFnSc2	Kellea
(	(	kIx(	(
<g/>
1555	[number]	k4	1555
<g/>
-	-	kIx~	-
<g/>
1597	[number]	k4	1597
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
alchymista	alchymista	k1gMnSc1	alchymista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
císaři	císař	k1gMnSc3	císař
Rudolfu	Rudolf	k1gMnSc3	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
sliboval	slibovat	k5eAaImAgMnS	slibovat
elixír	elixír	k1gInSc4	elixír
života	život	k1gInSc2	život
a	a	k8xC	a
proměnu	proměna	k1gFnSc4	proměna
obyčejného	obyčejný	k2eAgInSc2d1	obyčejný
kovu	kov	k1gInSc2	kov
ve	v	k7c4	v
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zabil	zabít	k5eAaPmAgMnS	zabít
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
dvořanů	dvořan	k1gMnPc2	dvořan
<g/>
,	,	kIx,	,
došla	dojít	k5eAaPmAgFnS	dojít
Rudolfovi	Rudolf	k1gMnSc3	Rudolf
trpělivost	trpělivost	k1gFnSc4	trpělivost
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
uvězněn	uvěznit	k5eAaPmNgInS	uvěznit
<g/>
.	.	kIx.	.
1109-1110	[number]	k4	1109-1110
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
lovecké	lovecký	k2eAgFnSc6d1	lovecká
stanici	stanice	k1gFnSc6	stanice
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
1319	[number]	k4	1319
<g/>
-	-	kIx~	-
<g/>
1323	[number]	k4	1323
nucený	nucený	k2eAgInSc1d1	nucený
pobyt	pobyt	k1gInSc1	pobyt
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
1333	[number]	k4	1333
Blanka	Blanka	k1gFnSc1	Blanka
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
zde	zde	k6eAd1	zde
porodila	porodit	k5eAaPmAgFnS	porodit
dceru	dcera	k1gFnSc4	dcera
Markétu	Markéta	k1gFnSc4	Markéta
1380	[number]	k4	1380
Václav	Václava	k1gFnPc2	Václava
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
přijal	přijmout	k5eAaPmAgMnS	přijmout
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
delegaci	delegace	k1gFnSc4	delegace
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Richarda	Richard	k1gMnSc2	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
1422	[number]	k4	1422
požár	požár	k1gInSc1	požár
1597	[number]	k4	1597
požár	požár	k1gInSc1	požár
1643	[number]	k4	1643
požár	požár	k1gInSc1	požár
1655	[number]	k4	1655
(	(	kIx(	(
<g/>
1658	[number]	k4	1658
<g/>
)	)	kIx)	)
zastaven	zastavit	k5eAaPmNgInS	zastavit
Schwarzenbergům	Schwarzenberg	k1gInPc3	Schwarzenberg
1685	[number]	k4	1685
prodán	prodat	k5eAaPmNgInS	prodat
Valdštejnům	Valdštejno	k1gNnPc3	Valdštejno
1733	[number]	k4	1733
sňatkem	sňatek	k1gInSc7	sňatek
přešel	přejít	k5eAaPmAgMnS	přejít
na	na	k7c4	na
Fürstenbergy	Fürstenberg	k1gInPc4	Fürstenberg
1826	[number]	k4	1826
požár	požár	k1gInSc1	požár
1929	[number]	k4	1929
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Egon	Egon	k1gMnSc1	Egon
II	II	kA	II
<g/>
.	.	kIx.	.
prodal	prodat	k5eAaPmAgInS	prodat
hrad	hrad	k1gInSc4	hrad
státu	stát	k1gInSc2	stát
Hrad	hrad	k1gInSc1	hrad
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
nepravé	pravý	k2eNgFnSc6d1	nepravá
ostrožně	ostrožna	k1gFnSc6	ostrožna
tvořené	tvořený	k2eAgInPc1d1	tvořený
starohorními	starohorní	k2eAgFnPc7d1	starohorní
drobami	droba	k1gFnPc7	droba
a	a	k8xC	a
jílovitými	jílovitý	k2eAgFnPc7d1	jílovitá
břidlicemi	břidlice	k1gFnPc7	břidlice
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
jako	jako	k8xC	jako
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Strmé	strmý	k2eAgInPc1d1	strmý
svahy	svah	k1gInPc1	svah
ostrožny	ostrožna	k1gFnSc2	ostrožna
obtéká	obtékat	k5eAaImIp3nS	obtékat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
Rakovnický	rakovnický	k2eAgInSc1d1	rakovnický
potok	potok	k1gInSc1	potok
a	a	k8xC	a
snadný	snadný	k2eAgInSc1d1	snadný
přístup	přístup	k1gInSc1	přístup
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Staveniště	staveniště	k1gNnSc1	staveniště
hradu	hrad	k1gInSc2	hrad
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
půdorys	půdorys	k1gInSc4	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
chráněno	chránit	k5eAaImNgNnS	chránit
obvodovou	obvodový	k2eAgFnSc7d1	obvodová
hradbou	hradba	k1gFnSc7	hradba
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
příčná	příčný	k2eAgFnSc1d1	příčná
hradba	hradba	k1gFnSc1	hradba
ho	on	k3xPp3gMnSc4	on
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c4	na
větší	veliký	k2eAgInSc4d2	veliký
dolní	dolní	k2eAgInSc4d1	dolní
a	a	k8xC	a
menší	malý	k2eAgInSc4d2	menší
horní	horní	k2eAgInSc4d1	horní
hrad	hrad	k1gInSc4	hrad
(	(	kIx(	(
<g/>
hradní	hradní	k2eAgNnSc4d1	hradní
jádro	jádro	k1gNnSc4	jádro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
pod	pod	k7c7	pod
čtverhrannou	čtverhranný	k2eAgFnSc7d1	čtverhranná
věží	věž	k1gFnSc7	věž
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
nároží	nároží	k1gNnSc6	nároží
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
západní	západní	k2eAgFnSc2d1	západní
hradby	hradba	k1gFnSc2	hradba
stála	stát	k5eAaImAgFnS	stát
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k8xC	jako
purkrabský	purkrabský	k2eAgInSc1d1	purkrabský
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
příčnou	příčný	k2eAgFnSc7d1	příčná
hradbou	hradba	k1gFnSc7	hradba
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
obdélný	obdélný	k2eAgInSc1d1	obdélný
sálový	sálový	k2eAgInSc1d1	sálový
palác	palác	k1gInSc1	palác
a	a	k8xC	a
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
cípu	cíp	k1gInSc6	cíp
velká	velký	k2eAgFnSc1d1	velká
okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
obytná	obytný	k2eAgFnSc1d1	obytná
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
donjon	donjon	k1gInSc1	donjon
<g/>
)	)	kIx)	)
zapojená	zapojený	k2eAgFnSc1d1	zapojená
do	do	k7c2	do
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Palác	palác	k1gInSc1	palác
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
mnohokrát	mnohokrát	k6eAd1	mnohokrát
přestavován	přestavován	k2eAgMnSc1d1	přestavován
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
nejstarší	starý	k2eAgFnSc2d3	nejstarší
fáze	fáze	k1gFnSc2	fáze
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgNnP	dochovat
románská	románský	k2eAgNnPc1d1	románské
okénka	okénko	k1gNnPc1	okénko
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
.	.	kIx.	.
</s>
<s>
Hradby	hradba	k1gFnPc1	hradba
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
patě	pata	k1gFnSc6	pata
donjonu	donjon	k1gInSc2	donjon
jsou	být	k5eAaImIp3nP	být
zesíleny	zesílen	k2eAgFnPc1d1	zesílena
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
celý	celý	k2eAgInSc4d1	celý
prostor	prostor	k1gInSc4	prostor
nároží	nároží	k1gNnSc2	nároží
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
tak	tak	k6eAd1	tak
mohutný	mohutný	k2eAgInSc1d1	mohutný
břit	břit	k1gInSc1	břit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
třináctého	třináctý	k4xOgNnSc2	třináctý
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
dále	daleko	k6eAd2	daleko
rozšiřovány	rozšiřován	k2eAgFnPc4d1	rozšiřována
obytné	obytný	k2eAgFnPc4d1	obytná
prostory	prostora	k1gFnPc4	prostora
a	a	k8xC	a
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jižní	jižní	k2eAgFnSc3d1	jižní
hradbě	hradba	k1gFnSc3	hradba
byla	být	k5eAaImAgFnS	být
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
dvouprostorová	dvouprostorový	k2eAgFnSc1d1	dvouprostorová
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
dolního	dolní	k2eAgNnSc2d1	dolní
nádvoří	nádvoří	k1gNnSc2	nádvoří
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
příčné	příčný	k2eAgFnPc1d1	příčná
hradby	hradba	k1gFnPc1	hradba
na	na	k7c4	na
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
nároží	nároží	k1gNnSc6	nároží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
studna	studna	k1gFnSc1	studna
<g/>
,	,	kIx,	,
stály	stát	k5eAaImAgFnP	stát
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgMnPc4	který
patřila	patřit	k5eAaImAgFnS	patřit
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
zemnice	zemnice	k1gFnSc1	zemnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
paláci	palác	k1gInSc6	palác
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
průjezd	průjezd	k1gInSc1	průjezd
nové	nový	k2eAgFnSc2d1	nová
brány	brána	k1gFnSc2	brána
do	do	k7c2	do
horního	horní	k2eAgInSc2d1	horní
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vybavený	vybavený	k2eAgInSc1d1	vybavený
sedilii	sedile	k1gNnPc7	sedile
a	a	k8xC	a
zaklenutý	zaklenutý	k2eAgInSc1d1	zaklenutý
dvěma	dva	k4xCgFnPc7	dva
poli	pole	k1gNnSc6	pole
křížové	křížový	k2eAgFnSc2d1	křížová
klenby	klenba	k1gFnSc2	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
průjezd	průjezd	k1gInSc1	průjezd
byl	být	k5eAaImAgInS	být
nutný	nutný	k2eAgInSc1d1	nutný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zbylé	zbylý	k2eAgFnPc1d1	zbylá
strany	strana	k1gFnPc1	strana
nádvoří	nádvoří	k1gNnSc2	nádvoří
horního	horní	k2eAgInSc2d1	horní
hradu	hrad	k1gInSc2	hrad
začaly	začít	k5eAaPmAgFnP	začít
zaplňovat	zaplňovat	k5eAaImF	zaplňovat
stavby	stavba	k1gFnPc1	stavba
nových	nový	k2eAgInPc2d1	nový
paláců	palác	k1gInPc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgFnSc4d2	starší
plochostropé	plochostropý	k2eAgNnSc4d1	plochostropé
jižní	jižní	k2eAgNnSc4d1	jižní
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
přízemí	přízemí	k1gNnSc6	přízemí
byla	být	k5eAaImAgNnP	být
ještě	ještě	k6eAd1	ještě
použita	použit	k2eAgNnPc1d1	použito
románská	románský	k2eAgNnPc1d1	románské
okénka	okénko	k1gNnPc1	okénko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
úpravy	úprava	k1gFnPc4	úprava
plynule	plynule	k6eAd1	plynule
navázalo	navázat	k5eAaPmAgNnS	navázat
další	další	k2eAgNnSc1d1	další
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
reprezentačních	reprezentační	k2eAgFnPc2d1	reprezentační
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolním	dolní	k2eAgNnSc6d1	dolní
nádvoří	nádvoří	k1gNnSc6	nádvoří
začal	začít	k5eAaPmAgInS	začít
při	při	k7c6	při
severní	severní	k2eAgFnSc6d1	severní
hradbě	hradba	k1gFnSc6	hradba
vznikat	vznikat	k5eAaImF	vznikat
druhý	druhý	k4xOgInSc4	druhý
palácový	palácový	k2eAgInSc4d1	palácový
okrsek	okrsek	k1gInSc4	okrsek
spojovaný	spojovaný	k2eAgInSc4d1	spojovaný
s	s	k7c7	s
mocenskými	mocenský	k2eAgFnPc7d1	mocenská
ambicemi	ambice	k1gFnPc7	ambice
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
ubytovávány	ubytováván	k2eAgFnPc4d1	ubytováván
významné	významný	k2eAgFnPc4d1	významná
návštěvy	návštěva	k1gFnPc4	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Tvořila	tvořit	k5eAaImAgFnS	tvořit
ho	on	k3xPp3gInSc4	on
dvě	dva	k4xCgNnPc4	dva
protilehlá	protilehlý	k2eAgNnPc4d1	protilehlé
palácová	palácový	k2eAgNnPc4d1	palácové
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
svírala	svírat	k5eAaImAgFnS	svírat
úzké	úzký	k2eAgNnSc4d1	úzké
nádvoří	nádvoří	k1gNnSc4	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc4d1	jižní
stranu	strana	k1gFnSc4	strana
uzavírala	uzavírat	k5eAaImAgFnS	uzavírat
arkáda	arkáda	k1gFnSc1	arkáda
se	s	k7c7	s
vstupem	vstup	k1gInSc7	vstup
z	z	k7c2	z
dolního	dolní	k2eAgNnSc2d1	dolní
nádvoří	nádvoří	k1gNnSc2	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
křídle	křídlo	k1gNnSc6	křídlo
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
a	a	k8xC	a
roubená	roubený	k2eAgFnSc1d1	roubená
komora	komora	k1gFnSc1	komora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hradním	hradní	k2eAgNnSc6d1	hradní
jádře	jádro	k1gNnSc6	jádro
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
severní	severní	k2eAgNnSc1d1	severní
křídlo	křídlo	k1gNnSc1	křídlo
se	s	k7c7	s
záchodovou	záchodový	k2eAgFnSc7d1	záchodová
věží	věž	k1gFnSc7	věž
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
stranu	strana	k1gFnSc4	strana
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
velkou	velký	k2eAgFnSc7d1	velká
věží	věž	k1gFnSc7	věž
tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
malý	malý	k2eAgInSc1d1	malý
dvorek	dvorek	k1gInSc1	dvorek
a	a	k8xC	a
lichoběžníkové	lichoběžníkový	k2eAgNnSc1d1	lichoběžníkové
nádvoří	nádvoří	k1gNnSc1	nádvoří
svírané	svíraný	k2eAgFnSc2d1	svíraná
palácovými	palácový	k2eAgNnPc7d1	palácové
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
stran	stran	k7c2	stran
ho	on	k3xPp3gMnSc2	on
obklopoval	obklopovat	k5eAaImAgInS	obklopovat
jednopatrový	jednopatrový	k2eAgInSc1d1	jednopatrový
arkádový	arkádový	k2eAgInSc1d1	arkádový
ochoz	ochoz	k1gInSc1	ochoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
patře	patro	k1gNnSc6	patro
západního	západní	k2eAgInSc2d1	západní
paláce	palác	k1gInSc2	palác
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
zaklenutý	zaklenutý	k2eAgInSc1d1	zaklenutý
čtyřmi	čtyři	k4xCgNnPc7	čtyři
poli	pole	k1gNnSc6	pole
šestidílných	šestidílný	k2eAgFnPc2d1	šestidílná
kleneb	klenba	k1gFnPc2	klenba
a	a	k8xC	a
osvětlovaný	osvětlovaný	k2eAgInSc1d1	osvětlovaný
osmi	osm	k4xCc2	osm
kružbovými	kružbový	k2eAgFnPc7d1	kružbová
hrotitými	hrotitý	k2eAgFnPc7d1	hrotitá
okny	okno	k1gNnPc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
patře	patro	k1gNnSc6	patro
jižního	jižní	k2eAgInSc2d1	jižní
paláce	palác	k1gInSc2	palác
byl	být	k5eAaImAgInS	být
menší	malý	k2eAgInSc1d2	menší
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
místností	místnost	k1gFnPc2	místnost
s	s	k7c7	s
valenými	valený	k2eAgFnPc7d1	valená
a	a	k8xC	a
křížovými	křížový	k2eAgFnPc7d1	křížová
klenbami	klenba	k1gFnPc7	klenba
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
křídlo	křídlo	k1gNnSc4	křídlo
zakončovala	zakončovat	k5eAaImAgFnS	zakončovat
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
nároží	nároží	k1gNnSc1	nároží
dolního	dolní	k2eAgInSc2d1	dolní
hradu	hrad	k1gInSc2	hrad
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
čtverhranná	čtverhranný	k2eAgFnSc1d1	čtverhranná
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
čtrnáctého	čtrnáctý	k4xOgNnSc2	čtrnáctý
století	století	k1gNnSc2	století
hrad	hrad	k1gInSc4	hrad
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgInSc1d2	mladší
palácový	palácový	k2eAgInSc1d1	palácový
okrsek	okrsek	k1gInSc1	okrsek
v	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
hradu	hrad	k1gInSc6	hrad
rychle	rychle	k6eAd1	rychle
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
udržovány	udržovat	k5eAaImNgFnP	udržovat
v	v	k7c6	v
provozuschopném	provozuschopný	k2eAgInSc6d1	provozuschopný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
přestavbu	přestavba	k1gFnSc4	přestavba
zahájil	zahájit	k5eAaPmAgMnS	zahájit
až	až	k9	až
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
oblíbil	oblíbit	k5eAaPmAgInS	oblíbit
zejména	zejména	k9	zejména
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1390	[number]	k4	1390
začal	začít	k5eAaPmAgMnS	začít
dávat	dávat	k5eAaImF	dávat
přednost	přednost	k1gFnSc4	přednost
jiným	jiný	k2eAgInPc3d1	jiný
hradům	hrad	k1gInPc3	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Úpravy	úprava	k1gFnPc1	úprava
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
dotkly	dotknout	k5eAaPmAgInP	dotknout
celého	celý	k2eAgInSc2d1	celý
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
délky	délka	k1gFnSc2	délka
jižní	jižní	k2eAgFnSc2d1	jižní
hradby	hradba	k1gFnSc2	hradba
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
Prochoditá	prochoditý	k2eAgFnSc1d1	Prochoditá
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
bránou	brána	k1gFnSc7	brána
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
snížen	snížit	k5eAaPmNgInS	snížit
terén	terén	k1gInSc1	terén
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
dolního	dolní	k2eAgNnSc2d1	dolní
nádvoří	nádvoří	k1gNnSc2	nádvoří
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
jeho	jeho	k3xOp3gNnSc1	jeho
členění	členění	k1gNnSc1	členění
příčnými	příčný	k2eAgFnPc7d1	příčná
hradbami	hradba	k1gFnPc7	hradba
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
oddělovala	oddělovat	k5eAaImAgFnS	oddělovat
studniční	studniční	k2eAgNnSc4d1	studniční
nádvoří	nádvoří	k1gNnSc4	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
obehnán	obehnat	k5eAaPmNgInS	obehnat
novou	nový	k2eAgFnSc7d1	nová
parkánovou	parkánový	k2eAgFnSc7d1	Parkánová
hradbou	hradba	k1gFnSc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
nároží	nároží	k1gNnSc6	nároží
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
napojoval	napojovat	k5eAaImAgInS	napojovat
úzký	úzký	k2eAgInSc1d1	úzký
areál	areál	k1gInSc1	areál
tzv.	tzv.	kA	tzv.
manského	manský	k2eAgInSc2d1	manský
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
tvořilo	tvořit	k5eAaImAgNnS	tvořit
podlouhlé	podlouhlý	k2eAgNnSc4d1	podlouhlé
nádvoří	nádvoří	k1gNnSc4	nádvoří
a	a	k8xC	a
kvalitně	kvalitně	k6eAd1	kvalitně
provedený	provedený	k2eAgInSc1d1	provedený
palác	palác	k1gInSc1	palác
zakončený	zakončený	k2eAgInSc1d1	zakončený
čtverhrannou	čtverhranný	k2eAgFnSc7d1	čtverhranná
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
strany	strana	k1gFnSc2	strana
paláce	palác	k1gInSc2	palác
do	do	k7c2	do
hradu	hrad	k1gInSc2	hrad
vedla	vést	k5eAaImAgFnS	vést
nová	nový	k2eAgFnSc1d1	nová
přístupová	přístupový	k2eAgFnSc1d1	přístupová
cesta	cesta	k1gFnSc1	cesta
chráněná	chráněný	k2eAgFnSc1d1	chráněná
dvojicí	dvojice	k1gFnSc7	dvojice
bran	brána	k1gFnPc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Obdélná	obdélný	k2eAgFnSc1d1	obdélná
budova	budova	k1gFnSc1	budova
zakončená	zakončený	k2eAgFnSc1d1	zakončená
snad	snad	k9	snad
také	také	k9	také
věží	věžit	k5eAaImIp3nS	věžit
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
nároží	nároží	k1gNnSc6	nároží
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
západní	západní	k2eAgFnSc2d1	západní
hradby	hradba	k1gFnSc2	hradba
dolního	dolní	k2eAgNnSc2d1	dolní
nádvoří	nádvoří	k1gNnSc2	nádvoří
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
budova	budova	k1gFnSc1	budova
purkrabství	purkrabství	k1gNnSc2	purkrabství
vybavená	vybavený	k2eAgFnSc1d1	vybavená
velkým	velký	k2eAgInSc7d1	velký
sálem	sál	k1gInSc7	sál
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kvalitně	kvalitně	k6eAd1	kvalitně
provedených	provedený	k2eAgInPc2d1	provedený
architektonických	architektonický	k2eAgInPc2d1	architektonický
detailů	detail	k1gInPc2	detail
ji	on	k3xPp3gFnSc4	on
postavila	postavit	k5eAaPmAgFnS	postavit
dvorská	dvorský	k2eAgFnSc1d1	dvorská
huť	huť	k1gFnSc1	huť
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
terénu	terén	k1gInSc2	terén
dolního	dolní	k2eAgNnSc2d1	dolní
nádvoří	nádvoří	k1gNnSc2	nádvoří
si	se	k3xPyFc3	se
vynutilo	vynutit	k5eAaPmAgNnS	vynutit
úpravu	úprava	k1gFnSc4	úprava
brány	brána	k1gFnSc2	brána
do	do	k7c2	do
horního	horní	k2eAgInSc2d1	horní
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
úroveň	úroveň	k1gFnSc1	úroveň
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
výrazně	výrazně	k6eAd1	výrazně
snížena	snížit	k5eAaPmNgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
přestavěno	přestavěn	k2eAgNnSc1d1	přestavěno
také	také	k9	také
severní	severní	k2eAgNnSc1d1	severní
křídlo	křídlo	k1gNnSc1	křídlo
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
těžce	těžce	k6eAd1	těžce
poškozen	poškodit	k5eAaPmNgInS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
purkrabství	purkrabství	k1gNnSc2	purkrabství
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
stavu	stav	k1gInSc3	stav
sloužila	sloužit	k5eAaImAgFnS	sloužit
jen	jen	k9	jen
jako	jako	k9	jako
sýpka	sýpka	k1gFnSc1	sýpka
a	a	k8xC	a
vyhořelý	vyhořelý	k2eAgInSc1d1	vyhořelý
areál	areál	k1gInSc1	areál
manského	manský	k2eAgInSc2d1	manský
domu	dům	k1gInSc2	dům
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Opravy	oprava	k1gFnPc1	oprava
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Vysunutá	vysunutý	k2eAgFnSc1d1	vysunutá
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
nároží	nároží	k1gNnSc6	nároží
byla	být	k5eAaImAgFnS	být
zkrácena	zkrátit	k5eAaPmNgFnS	zkrátit
a	a	k8xC	a
podél	podél	k7c2	podél
severní	severní	k2eAgFnSc2d1	severní
hradby	hradba	k1gFnSc2	hradba
dolního	dolní	k2eAgInSc2d1	dolní
hradu	hrad	k1gInSc2	hrad
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
trojprostorová	trojprostorový	k2eAgFnSc1d1	trojprostorová
budova	budova	k1gFnSc1	budova
nového	nový	k2eAgNnSc2d1	nové
purkrabství	purkrabství	k1gNnSc2	purkrabství
<g/>
.	.	kIx.	.
</s>
<s>
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgInSc7d1	jediný
královským	královský	k2eAgInSc7d1	královský
hradem	hrad	k1gInSc7	hrad
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
disponoval	disponovat	k5eAaBmAgMnS	disponovat
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgMnS	věnovat
proto	proto	k8xC	proto
jeho	jeho	k3xOp3gFnSc3	jeho
přestavbě	přestavba	k1gFnSc3	přestavba
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
provedené	provedený	k2eAgFnPc4d1	provedená
úpravy	úprava	k1gFnPc4	úprava
obytné	obytný	k2eAgFnSc2d1	obytná
části	část	k1gFnSc2	část
hradu	hrad	k1gInSc2	hrad
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
učinily	učinit	k5eAaImAgInP	učinit
rezidenci	rezidence	k1gFnSc4	rezidence
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
nejkvalitnější	kvalitní	k2eAgNnPc4d3	nejkvalitnější
sídla	sídlo	k1gNnPc4	sídlo
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
zmodernizován	zmodernizován	k2eAgInSc1d1	zmodernizován
obranný	obranný	k2eAgInSc1d1	obranný
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
byla	být	k5eAaImAgFnS	být
využita	využit	k2eAgFnSc1d1	využita
řada	řada	k1gFnSc1	řada
moderních	moderní	k2eAgInPc2d1	moderní
prvků	prvek	k1gInPc2	prvek
aktivní	aktivní	k2eAgFnSc2d1	aktivní
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Přestavbu	přestavba	k1gFnSc4	přestavba
řídili	řídit	k5eAaImAgMnP	řídit
mistři	mistr	k1gMnPc1	mistr
Benedikt	Benedikt	k1gMnSc1	Benedikt
Rejt	Rejt	k1gMnSc1	Rejt
a	a	k8xC	a
Hans	Hans	k1gMnSc1	Hans
Spiess	Spiessa	k1gFnPc2	Spiessa
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
přestavěno	přestavěn	k2eAgNnSc4d1	přestavěno
bylo	být	k5eAaImAgNnS	být
hradní	hradní	k2eAgNnSc1d1	hradní
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Arkády	arkáda	k1gFnPc1	arkáda
a	a	k8xC	a
dělící	dělící	k2eAgFnSc1d1	dělící
zeď	zeď	k1gFnSc1	zeď
mezi	mezi	k7c7	mezi
nádvořím	nádvoří	k1gNnSc7	nádvoří
a	a	k8xC	a
dvorkem	dvorek	k1gInSc7	dvorek
pod	pod	k7c7	pod
velkou	velký	k2eAgFnSc7d1	velká
věží	věž	k1gFnSc7	věž
byly	být	k5eAaImAgFnP	být
strženy	stržen	k2eAgFnPc1d1	stržena
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
patra	patro	k1gNnPc4	patro
paláců	palác	k1gInPc2	palác
spojilo	spojit	k5eAaPmAgNnS	spojit
nové	nový	k2eAgNnSc1d1	nové
čtyřramenné	čtyřramenný	k2eAgNnSc1d1	čtyřramenné
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Zbořeno	zbořen	k2eAgNnSc1d1	zbořeno
a	a	k8xC	a
nově	nově	k6eAd1	nově
postaveno	postaven	k2eAgNnSc4d1	postaveno
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
přízemí	přízemí	k1gNnSc2	přízemí
také	také	k9	také
severní	severní	k2eAgNnSc1d1	severní
palácové	palácový	k2eAgNnSc1d1	palácové
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
paláci	palác	k1gInSc6	palác
získal	získat	k5eAaPmAgMnS	získat
novou	nový	k2eAgFnSc4d1	nová
klenbu	klenba	k1gFnSc4	klenba
a	a	k8xC	a
arkýř	arkýř	k1gInSc4	arkýř
nad	nad	k7c7	nad
průjezdem	průjezd	k1gInSc7	průjezd
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
průčelí	průčelí	k1gNnPc4	průčelí
zdobí	zdobit	k5eAaImIp3nP	zdobit
reliéfy	reliéf	k1gInPc1	reliéf
Vladislava	Vladislav	k1gMnSc2	Vladislav
Jagellonského	jagellonský	k2eAgMnSc2d1	jagellonský
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Ozdobné	ozdobný	k2eAgInPc4d1	ozdobný
architektonické	architektonický	k2eAgInPc4d1	architektonický
detaily	detail	k1gInPc4	detail
včetně	včetně	k7c2	včetně
tzv.	tzv.	kA	tzv.
manské	manský	k2eAgFnSc2d1	manská
lodžie	lodžie	k1gFnSc2	lodžie
u	u	k7c2	u
brány	brána	k1gFnSc2	brána
a	a	k8xC	a
oltáře	oltář	k1gInSc2	oltář
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
mistra	mistr	k1gMnSc2	mistr
Hanuše	Hanuš	k1gMnSc2	Hanuš
Špisse	Špiss	k1gMnSc2	Špiss
<g/>
.	.	kIx.	.
</s>
<s>
Upravena	upraven	k2eAgFnSc1d1	upravena
byla	být	k5eAaImAgFnS	být
také	také	k9	také
velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
nový	nový	k2eAgInSc4d1	nový
vstup	vstup	k1gInSc4	vstup
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
ze	z	k7c2	z
severního	severní	k2eAgNnSc2d1	severní
palácového	palácový	k2eAgNnSc2d1	palácové
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
patra	patro	k1gNnSc2	patro
spojovalo	spojovat	k5eAaImAgNnS	spojovat
schodiště	schodiště	k1gNnSc1	schodiště
v	v	k7c6	v
síle	síla	k1gFnSc6	síla
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Paláce	palác	k1gInPc1	palác
i	i	k8xC	i
věž	věž	k1gFnSc1	věž
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
hrázděným	hrázděný	k2eAgNnSc7d1	hrázděné
nebo	nebo	k8xC	nebo
roubeným	roubený	k2eAgNnSc7d1	roubené
obranným	obranný	k2eAgNnSc7d1	obranné
polopatrem	polopatro	k1gNnSc7	polopatro
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
arkýřových	arkýřový	k2eAgFnPc2d1	arkýřová
vížek	vížka	k1gFnPc2	vížka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
hradu	hrad	k1gInSc6	hrad
byla	být	k5eAaImAgFnS	být
zbořena	zbořen	k2eAgFnSc1d1	zbořena
zástavba	zástavba	k1gFnSc1	zástavba
podél	podél	k7c2	podél
severní	severní	k2eAgFnSc2d1	severní
hradby	hradba	k1gFnSc2	hradba
včetně	včetně	k7c2	včetně
hradby	hradba	k1gFnSc2	hradba
samotné	samotný	k2eAgFnSc2d1	samotná
<g/>
.	.	kIx.	.
</s>
<s>
Starou	starý	k2eAgFnSc4d1	stará
věž	věž	k1gFnSc4	věž
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
nároží	nároží	k1gNnSc6	nároží
nahradila	nahradit	k5eAaPmAgFnS	nahradit
hranolová	hranolový	k2eAgFnSc1d1	hranolová
věž	věž	k1gFnSc1	věž
Huderka	Huderka	k1gFnSc1	Huderka
se	s	k7c7	s
zděnou	zděný	k2eAgFnSc7d1	zděná
helmicí	helmice	k1gFnSc7	helmice
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
původní	původní	k2eAgFnSc2d1	původní
hradby	hradba	k1gFnSc2	hradba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
manských	manský	k2eAgInPc2d1	manský
domků	domek	k1gInPc2	domek
s	s	k7c7	s
kuchyní	kuchyně	k1gFnSc7	kuchyně
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
hradním	hradní	k2eAgNnSc7d1	hradní
jádrem	jádro	k1gNnSc7	jádro
objekt	objekt	k1gInSc4	objekt
druhé	druhý	k4xOgFnSc2	druhý
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
částečně	částečně	k6eAd1	částečně
vysunutý	vysunutý	k2eAgInSc1d1	vysunutý
do	do	k7c2	do
parkánu	parkán	k1gInSc2	parkán
<g/>
.	.	kIx.	.
</s>
<s>
Průchod	průchod	k1gInSc1	průchod
parkánem	parkán	k1gInSc7	parkán
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
klenutá	klenutý	k2eAgFnSc1d1	klenutá
chodbička	chodbička	k1gFnSc1	chodbička
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
se	se	k3xPyFc4	se
také	také	k9	také
zástavba	zástavba	k1gFnSc1	zástavba
podél	podél	k7c2	podél
západní	západní	k2eAgFnSc2d1	západní
hradby	hradba	k1gFnSc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
Prochodité	prochoditý	k2eAgFnSc2d1	Prochoditá
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
hradní	hradní	k2eAgInSc1d1	hradní
pivovar	pivovar	k1gInSc1	pivovar
a	a	k8xC	a
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
se	se	k3xPyFc4	se
napojil	napojit	k5eAaPmAgInS	napojit
nově	nově	k6eAd1	nově
postavený	postavený	k2eAgInSc1d1	postavený
dvoutraktový	dvoutraktový	k2eAgInSc1d1	dvoutraktový
Hejtmanský	hejtmanský	k2eAgInSc1d1	hejtmanský
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
obranou	obrana	k1gFnSc7	obrana
linií	linie	k1gFnPc2	linie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
parkánová	parkánový	k2eAgFnSc1d1	Parkánová
hradba	hradba	k1gFnSc1	hradba
s	s	k7c7	s
krytými	krytý	k2eAgInPc7d1	krytý
střeleckými	střelecký	k2eAgInPc7d1	střelecký
ochozy	ochoz	k1gInPc7	ochoz
a	a	k8xC	a
arkýřovými	arkýřový	k2eAgFnPc7d1	arkýřová
vížkami	vížka	k1gFnPc7	vížka
<g/>
.	.	kIx.	.
</s>
<s>
Chránila	chránit	k5eAaImAgFnS	chránit
ji	on	k3xPp3gFnSc4	on
řada	řada	k1gFnSc1	řada
progresívních	progresívní	k2eAgInPc2d1	progresívní
prvků	prvek	k1gInPc2	prvek
aktivní	aktivní	k2eAgFnSc2d1	aktivní
dělostřelecké	dělostřelecký	k2eAgFnSc2d1	dělostřelecká
obrany	obrana	k1gFnSc2	obrana
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
bateriová	bateriový	k2eAgFnSc1d1	bateriová
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
bollwerk	bollwerk	k1gInSc1	bollwerk
nebo	nebo	k8xC	nebo
okrouhlá	okrouhlý	k2eAgFnSc1d1	okrouhlá
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
bašta	bašta	k1gFnSc1	bašta
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
nároží	nároží	k1gNnSc6	nároží
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgNnSc7d1	nové
předbraním	předbraní	k1gNnSc7	předbraní
s	s	k7c7	s
kasematami	kasemata	k1gFnPc7	kasemata
byla	být	k5eAaImAgFnS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
brána	brána	k1gFnSc1	brána
v	v	k7c6	v
Prochodité	prochoditý	k2eAgFnSc6d1	Prochoditá
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Přístupovou	přístupový	k2eAgFnSc4d1	přístupová
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
chránil	chránit	k5eAaImAgMnS	chránit
příkop	příkop	k1gInSc4	příkop
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
branskou	branský	k2eAgFnSc7d1	branská
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
kulisovou	kulisový	k2eAgFnSc7d1	kulisová
bránou	brána	k1gFnSc7	brána
v	v	k7c6	v
příčné	příčný	k2eAgFnSc6d1	příčná
zdi	zeď	k1gFnSc6	zeď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zbořena	zbořit	k5eAaPmNgFnS	zbořit
až	až	k9	až
v	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
renesance	renesance	k1gFnSc2	renesance
se	se	k3xPyFc4	se
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
téměř	téměř	k6eAd1	téměř
nestavělo	stavět	k5eNaImAgNnS	stavět
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
nechal	nechat	k5eAaPmAgMnS	nechat
provést	provést	k5eAaPmF	provést
drobné	drobný	k2eAgFnPc4d1	drobná
opravy	oprava	k1gFnPc4	oprava
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
krbů	krb	k1gInPc2	krb
a	a	k8xC	a
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
dalo	dát	k5eAaPmAgNnS	dát
bydlet	bydlet	k5eAaImF	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
nechal	nechat	k5eAaPmAgInS	nechat
některé	některý	k3yIgFnPc4	některý
budovy	budova	k1gFnPc4	budova
vybavit	vybavit	k5eAaPmF	vybavit
sgrafitovými	sgrafitový	k2eAgFnPc7d1	sgrafitová
omítkami	omítka	k1gFnPc7	omítka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
hradu	hrad	k1gInSc2	hrad
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1597	[number]	k4	1597
nechal	nechat	k5eAaPmAgMnS	nechat
provést	provést	k5eAaPmF	provést
jen	jen	k9	jen
nejnutnější	nutný	k2eAgFnPc4d3	nejnutnější
opravy	oprava	k1gFnPc4	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
opravu	oprava	k1gFnSc4	oprava
byl	být	k5eAaImAgInS	být
vypracován	vypracovat	k5eAaPmNgInS	vypracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1638	[number]	k4	1638
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zničil	zničit	k5eAaPmAgInS	zničit
38	[number]	k4	38
místností	místnost	k1gFnPc2	místnost
<g/>
,	,	kIx,	,
přiměl	přimět	k5eAaPmAgMnS	přimět
krále	král	k1gMnSc4	král
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
k	k	k7c3	k
opravě	oprava	k1gFnSc3	oprava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1644	[number]	k4	1644
<g/>
.	.	kIx.	.
</s>
<s>
Neopravena	opraven	k2eNgFnSc1d1	neopravena
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jen	jen	k9	jen
velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgNnSc1d1	jižní
palácové	palácový	k2eAgNnSc1d1	palácové
křídlo	křídlo	k1gNnSc1	křídlo
bylo	být	k5eAaImAgNnS	být
znovu	znovu	k6eAd1	znovu
opraveno	opravit	k5eAaPmNgNnS	opravit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1685	[number]	k4	1685
<g/>
.	.	kIx.	.
</s>
<s>
Fürstenbergové	Fürstenberg	k1gMnPc1	Fürstenberg
nechali	nechat	k5eAaPmAgMnP	nechat
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1756	[number]	k4	1756
velký	velký	k2eAgInSc1d1	velký
královský	královský	k2eAgInSc1d1	královský
sál	sál	k1gInSc1	sál
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
patra	patro	k1gNnPc4	patro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodním	spodní	k2eAgNnSc6d1	spodní
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
místností	místnost	k1gFnPc2	místnost
a	a	k8xC	a
horní	horní	k2eAgNnSc1d1	horní
začalo	začít	k5eAaPmAgNnS	začít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
skladiště	skladiště	k1gNnSc4	skladiště
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
ze	z	k7c2	z
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1826	[number]	k4	1826
zničil	zničit	k5eAaPmAgInS	zničit
prakticky	prakticky	k6eAd1	prakticky
celý	celý	k2eAgInSc4d1	celý
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Opravy	oprava	k1gFnPc1	oprava
započaly	započnout	k5eAaPmAgFnP	započnout
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
a	a	k8xC	a
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
přestávkami	přestávka	k1gFnPc7	přestávka
se	se	k3xPyFc4	se
protáhly	protáhnout	k5eAaPmAgInP	protáhnout
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přerušení	přerušení	k1gNnSc1	přerušení
způsobené	způsobený	k2eAgFnSc2d1	způsobená
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kaple	kaple	k1gFnSc2	kaple
Korunování	korunování	k1gNnSc2	korunování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozdně	pozdně	k6eAd1	pozdně
gotická	gotický	k2eAgFnSc1d1	gotická
kaple	kaple	k1gFnSc1	kaple
Korunování	korunování	k1gNnSc2	korunování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
krále	král	k1gMnSc2	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonského	jagellonský	k2eAgInSc2d1	jagellonský
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
křídle	křídlo	k1gNnSc6	křídlo
hradního	hradní	k2eAgNnSc2d1	hradní
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Nahradila	nahradit	k5eAaPmAgFnS	nahradit
starší	starý	k2eAgFnSc4d2	starší
kapli	kaple	k1gFnSc4	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
ze	z	k7c2	z
třináctého	třináctý	k4xOgNnSc2	třináctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
původního	původní	k2eAgNnSc2d1	původní
rovného	rovný	k2eAgNnSc2d1	rovné
zakončení	zakončení	k1gNnSc2	zakončení
získala	získat	k5eAaPmAgFnS	získat
polygonální	polygonální	k2eAgInSc4d1	polygonální
uzávěr	uzávěr	k1gInSc4	uzávěr
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zaklenuta	zaklenout	k5eAaPmNgFnS	zaklenout
síťovou	síťový	k2eAgFnSc7d1	síťová
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bočních	boční	k2eAgFnPc6d1	boční
stěnách	stěna	k1gFnPc6	stěna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řady	řada	k1gFnSc2	řada
sedilií	sedile	k1gNnPc2	sedile
oddělených	oddělený	k2eAgFnPc2d1	oddělená
šroubovitým	šroubovitý	k2eAgInSc7d1	šroubovitý
středním	střední	k2eAgInSc7d1	střední
sloupkem	sloupek	k1gInSc7	sloupek
a	a	k8xC	a
zdobených	zdobený	k2eAgFnPc2d1	zdobená
mitrovitými	mitrovitý	k2eAgInPc7d1	mitrovitý
oblouky	oblouk	k1gInPc7	oblouk
s	s	k7c7	s
kraby	krab	k1gMnPc7	krab
<g/>
.	.	kIx.	.
</s>
<s>
Portál	portál	k1gInSc1	portál
do	do	k7c2	do
sakristie	sakristie	k1gFnSc2	sakristie
je	být	k5eAaImIp3nS	být
zdobený	zdobený	k2eAgMnSc1d1	zdobený
reliéfním	reliéfní	k2eAgInSc7d1	reliéfní
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
ornamentem	ornament	k1gInSc7	ornament
<g/>
.	.	kIx.	.
</s>
<s>
Oltář	Oltář	k1gInSc1	Oltář
je	být	k5eAaImIp3nS	být
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
prací	práce	k1gFnSc7	práce
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1490	[number]	k4	1490
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
neznámý	známý	k2eNgMnSc1d1	neznámý
mistr	mistr	k1gMnSc1	mistr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
Hanuše	Hanuš	k1gMnSc2	Hanuš
Spiesse	Spiess	k1gMnSc2	Spiess
z	z	k7c2	z
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
křídlové	křídlový	k2eAgFnSc2d1	křídlová
archy	archa	k1gFnSc2	archa
s	s	k7c7	s
dřevořezbou	dřevořezba	k1gFnSc7	dřevořezba
Korunování	korunování	k1gNnSc2	korunování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
malovanými	malovaný	k2eAgInPc7d1	malovaný
výjevy	výjev	k1gInPc7	výjev
ze	z	k7c2	z
života	život	k1gInSc2	život
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
maleb	malba	k1gFnPc2	malba
je	být	k5eAaImIp3nS	být
neznámý	známý	k2eNgMnSc1d1	neznámý
malíř	malíř	k1gMnSc1	malíř
označovaný	označovaný	k2eAgMnSc1d1	označovaný
jako	jako	k8xC	jako
Mistr	mistr	k1gMnSc1	mistr
křivoklátského	křivoklátský	k2eAgInSc2d1	křivoklátský
oltáře	oltář	k1gInSc2	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Pohled	pohled	k1gInSc1	pohled
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
směřuje	směřovat	k5eAaImIp3nS	směřovat
do	do	k7c2	do
velké	velká	k1gFnSc2	velká
kamenné	kamenný	k2eAgNnSc1d1	kamenné
sedile	sedile	k1gNnSc1	sedile
vpravo	vpravo	k6eAd1	vpravo
v	v	k7c6	v
presbytáři	presbytář	k1gInSc6	presbytář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývalo	bývat	k5eAaImAgNnS	bývat
místo	místo	k1gNnSc1	místo
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
panovníka	panovník	k1gMnSc4	panovník
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
sedilií	sedile	k1gNnPc2	sedile
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
nacházely	nacházet	k5eAaImAgInP	nacházet
dva	dva	k4xCgInPc1	dva
znaky	znak	k1gInPc1	znak
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
obnovy	obnova	k1gFnSc2	obnova
kaple	kaple	k1gFnSc2	kaple
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
sejmuty	sejmut	k2eAgInPc1d1	sejmut
a	a	k8xC	a
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
fürstenberskými	fürstenberský	k2eAgInPc7d1	fürstenberský
rodovými	rodový	k2eAgInPc7d1	rodový
erby	erb	k1gInPc7	erb
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Velká	velký	k2eAgFnSc1d1	velká
věž	věž	k1gFnSc1	věž
v	v	k7c6	v
hradním	hradní	k2eAgNnSc6d1	hradní
jádře	jádro	k1gNnSc6	jádro
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
méně	málo	k6eAd2	málo
obvyklé	obvyklý	k2eAgInPc4d1	obvyklý
donjony	donjon	k1gInPc4	donjon
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obytné	obytný	k2eAgFnPc1d1	obytná
věže	věž	k1gFnPc1	věž
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
bývaly	bývat	k5eAaImAgFnP	bývat
obvykle	obvykle	k6eAd1	obvykle
čtverhranné	čtverhranný	k2eAgFnPc1d1	čtverhranná
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
první	první	k4xOgFnSc2	první
stavební	stavební	k2eAgFnSc2d1	stavební
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
42	[number]	k4	42
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
měří	měřit	k5eAaImIp3nS	měřit
dvanáct	dvanáct	k4xCc1	dvanáct
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
do	do	k7c2	do
věže	věž	k1gFnSc2	věž
vcházelo	vcházet	k5eAaImAgNnS	vcházet
portálem	portál	k1gInSc7	portál
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
druhého	druhý	k4xOgNnSc2	druhý
patra	patro	k1gNnSc2	patro
z	z	k7c2	z
jižního	jižní	k2eAgNnSc2d1	jižní
palácového	palácový	k2eAgNnSc2d1	palácové
křídla	křídlo	k1gNnSc2	křídlo
po	po	k7c6	po
padacím	padací	k2eAgInSc6d1	padací
můstku	můstek	k1gInSc6	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
můstek	můstek	k1gInSc1	můstek
navazoval	navazovat	k5eAaImAgInS	navazovat
<g/>
,	,	kIx,	,
shořela	shořet	k5eAaPmAgFnS	shořet
při	při	k7c6	při
posledním	poslední	k2eAgInSc6d1	poslední
velkém	velký	k2eAgInSc6d1	velký
požáru	požár	k1gInSc6	požár
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
už	už	k6eAd1	už
nebyla	být	k5eNaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Druhotně	druhotně	k6eAd1	druhotně
byl	být	k5eAaImAgInS	být
proražen	proražen	k2eAgInSc4d1	proražen
nový	nový	k2eAgInSc4d1	nový
portál	portál	k1gInSc4	portál
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
vcházelo	vcházet	k5eAaImAgNnS	vcházet
ze	z	k7c2	z
severního	severní	k2eAgNnSc2d1	severní
palácového	palácový	k2eAgNnSc2d1	palácové
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Vchod	vchod	k1gInSc1	vchod
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
hladomorny	hladomorna	k1gFnSc2	hladomorna
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
proražení	proražení	k1gNnSc6	proražení
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
našlo	najít	k5eAaPmAgNnS	najít
šest	šest	k4xCc1	šest
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
<g/>
.	.	kIx.	.
</s>
<s>
Obrannou	obranný	k2eAgFnSc4d1	obranná
funkci	funkce	k1gFnSc4	funkce
věže	věž	k1gFnSc2	věž
dokládá	dokládat	k5eAaImIp3nS	dokládat
i	i	k9	i
síla	síla	k1gFnSc1	síla
stěn	stěna	k1gFnPc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nádvorní	nádvorní	k2eAgFnSc6d1	nádvorní
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
nehrozilo	hrozit	k5eNaImAgNnS	hrozit
reálné	reálný	k2eAgNnSc1d1	reálné
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
2,5	[number]	k4	2,5
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
věž	věž	k1gFnSc1	věž
propojená	propojený	k2eAgFnSc1d1	propojená
s	s	k7c7	s
hradbou	hradba	k1gFnSc7	hradba
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
šířka	šířka	k1gFnSc1	šířka
zdi	zeď	k1gFnSc3	zeď
až	až	k6eAd1	až
deset	deset	k4xCc4	deset
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hrad	hrad	k1gInSc1	hrad
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
cenných	cenný	k2eAgInPc2d1	cenný
<g/>
,	,	kIx,	,
pozdně	pozdně	k6eAd1	pozdně
goticky	goticky	k6eAd1	goticky
upravených	upravený	k2eAgFnPc2d1	upravená
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
především	především	k9	především
kaple	kaple	k1gFnSc1	kaple
s	s	k7c7	s
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
pozdněgotickým	pozdněgotický	k2eAgInSc7d1	pozdněgotický
malovaným	malovaný	k2eAgInSc7d1	malovaný
oltářem	oltář	k1gInSc7	oltář
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
gotickým	gotický	k2eAgMnPc3d1	gotický
prostorům	prostor	k1gInPc3	prostor
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
místě	místo	k1gNnSc6	místo
hradní	hradní	k2eAgFnSc2d1	hradní
dispozice	dispozice	k1gFnSc2	dispozice
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
42	[number]	k4	42
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
bergfrit	bergfrit	k1gInSc4	bergfrit
<g/>
,	,	kIx,	,
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
relativně	relativně	k6eAd1	relativně
těsné	těsný	k2eAgNnSc1d1	těsné
jádro	jádro	k1gNnSc1	jádro
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
trojicí	trojice	k1gFnSc7	trojice
křídel	křídlo	k1gNnPc2	křídlo
obklopujících	obklopující	k2eAgInPc2d1	obklopující
trojúhelné	trojúhelný	k2eAgNnSc4d1	trojúhelné
nádvoří	nádvoří	k1gNnSc4	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgNnSc1d1	jižní
křídlo	křídlo	k1gNnSc1	křídlo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rozměrnou	rozměrný	k2eAgFnSc4d1	rozměrná
hradní	hradní	k2eAgFnSc4d1	hradní
kapli	kaple	k1gFnSc4	kaple
Korunování	korunování	k1gNnSc2	korunování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
křídlo	křídlo	k1gNnSc1	křídlo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
severním	severní	k2eAgInSc7d1	severní
(	(	kIx(	(
<g/>
královniným	královnin	k2eAgMnSc7d1	královnin
<g/>
)	)	kIx)	)
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
menší	malý	k2eAgFnPc1d2	menší
soukromé	soukromý	k2eAgFnPc1d1	soukromá
prostory	prostora	k1gFnPc1	prostora
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
hradní	hradní	k2eAgInSc4d1	hradní
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Přízemí	přízemí	k1gNnSc1	přízemí
<g/>
:	:	kIx,	:
Vězení	vězení	k1gNnSc1	vězení
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
kaplí	kaple	k1gFnSc7	kaple
a	a	k8xC	a
Malý	Malý	k1gMnSc1	Malý
rytířským	rytířský	k2eAgInSc7d1	rytířský
sálem	sál	k1gInSc7	sál
<g/>
)	)	kIx)	)
-	-	kIx~	-
novodobé	novodobý	k2eAgFnSc2d1	novodobá
repliky	replika	k1gFnSc2	replika
mučících	mučící	k2eAgInPc2d1	mučící
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Augustovo	Augustův	k2eAgNnSc1d1	Augustovo
vězení	vězení	k1gNnSc1	vězení
Cela	cela	k1gFnSc1	cela
s	s	k7c7	s
honebním	honební	k2eAgInSc7d1	honební
kočárem	kočár	k1gInSc7	kočár
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Prostřední	prostřední	k2eAgFnSc1d1	prostřední
cela	cela	k1gFnSc1	cela
Třetí	třetí	k4xOgFnSc1	třetí
cela	cela	k1gFnSc1	cela
Stříbrnice	Stříbrnice	k1gFnPc1	Stříbrnice
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
Královským	královský	k2eAgInSc7d1	královský
sálem	sál	k1gInSc7	sál
<g/>
)	)	kIx)	)
-	-	kIx~	-
pokladnice	pokladnice	k1gFnSc1	pokladnice
<g/>
,	,	kIx,	,
za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
sem	sem	k6eAd1	sem
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
nechal	nechat	k5eAaPmAgMnS	nechat
převézt	převézt	k5eAaPmF	převézt
české	český	k2eAgInPc4d1	český
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
skladovaly	skladovat	k5eAaImAgFnP	skladovat
zbraně	zbraň	k1gFnPc1	zbraň
hradní	hradní	k2eAgFnSc2d1	hradní
posádky	posádka	k1gFnSc2	posádka
nebo	nebo	k8xC	nebo
ulovená	ulovený	k2eAgFnSc1d1	ulovená
zvěř	zvěř	k1gFnSc1	zvěř
z	z	k7c2	z
okolních	okolní	k2eAgInPc2d1	okolní
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
umístěny	umístit	k5eAaPmNgInP	umístit
tři	tři	k4xCgInPc1	tři
modely	model	k1gInPc1	model
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
visí	viset	k5eAaImIp3nS	viset
ohořelé	ohořelý	k2eAgFnPc4d1	ohořelá
původní	původní	k2eAgFnPc4d1	původní
dubové	dubový	k2eAgFnPc4d1	dubová
dveře	dveře	k1gFnPc4	dveře
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1490	[number]	k4	1490
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
klepadlo	klepadlo	k1gNnSc1	klepadlo
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
starší	starý	k2eAgMnSc1d2	starší
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
rozměr	rozměr	k1gInSc4	rozměr
190	[number]	k4	190
×	×	k?	×
113	[number]	k4	113
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
dveří	dveře	k1gFnPc2	dveře
na	na	k7c6	na
kovové	kovový	k2eAgFnSc6d1	kovová
aplikaci	aplikace	k1gFnSc6	aplikace
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
zachoval	zachovat	k5eAaPmAgInS	zachovat
původní	původní	k2eAgInSc1d1	původní
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
Letha	Letha	k1gFnSc1	Letha
boziho	bozi	k1gMnSc2	bozi
tisiczeho	tisicze	k1gMnSc2	tisicze
cztirsteho	cztirste	k1gMnSc2	cztirste
sedmdesateho	sedmdesateze	k6eAd1	sedmdesateze
devateho	devateze	k6eAd1	devateze
ty	ten	k3xDgInPc1	ten
su	su	k?	su
dwerze	dwerze	k1gFnPc4	dwerze
dielany	dielana	k1gFnSc2	dielana
za	za	k7c4	za
Hrozska	Hrozsko	k1gNnPc4	Hrozsko
z	z	k7c2	z
Prossowicz	Prossowicza	k1gFnPc2	Prossowicza
hitmana	hitman	k1gMnSc2	hitman
na	na	k7c4	na
hradku	hradka	k1gFnSc4	hradka
Krivoklatu	Krivoklat	k1gInSc2	Krivoklat
<g/>
.	.	kIx.	.
</s>
<s>
Dveře	dveře	k1gFnPc1	dveře
byly	být	k5eAaImAgFnP	být
poškozeny	poškozen	k2eAgInPc1d1	poškozen
požárem	požár	k1gInSc7	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
Fürstenbergové	Fürstenberg	k1gMnPc1	Fürstenberg
nechali	nechat	k5eAaPmAgMnP	nechat
do	do	k7c2	do
vchodu	vchod	k1gInSc2	vchod
vsadit	vsadit	k5eAaPmF	vsadit
jejich	jejich	k3xOp3gFnSc7	jejich
věrnou	věrný	k2eAgFnSc7d1	věrná
kopii	kopie	k1gFnSc4	kopie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
<g/>
:	:	kIx,	:
Malý	malý	k2eAgInSc1d1	malý
rytířský	rytířský	k2eAgInSc1d1	rytířský
sál	sál	k1gInSc1	sál
-	-	kIx~	-
výstava	výstava	k1gFnSc1	výstava
gotického	gotický	k2eAgNnSc2d1	gotické
umění	umění	k1gNnSc2	umění
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
nebo	nebo	k8xC	nebo
také	také	k9	také
Velký	velký	k2eAgInSc1d1	velký
rytířský	rytířský	k2eAgInSc1d1	rytířský
sál	sál	k1gInSc1	sál
-	-	kIx~	-
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
gotický	gotický	k2eAgInSc1d1	gotický
sál	sál	k1gInSc1	sál
bez	bez	k7c2	bez
podpěrných	podpěrný	k2eAgInPc2d1	podpěrný
sloupů	sloup	k1gInPc2	sloup
uprostřed	uprostřed	k6eAd1	uprostřed
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
po	po	k7c6	po
Vladislavském	vladislavský	k2eAgInSc6d1	vladislavský
sále	sál	k1gInSc6	sál
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Sál	sál	k1gInSc1	sál
měří	měřit	k5eAaImIp3nS	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
28	[number]	k4	28
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
8	[number]	k4	8
m	m	kA	m
a	a	k8xC	a
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
8,5	[number]	k4	8,5
m.	m.	k?	m.
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
uvádíme	uvádět	k5eAaImIp1nP	uvádět
rozměry	rozměr	k1gInPc4	rozměr
Vladislavského	vladislavský	k2eAgInSc2d1	vladislavský
sálu	sál	k1gInSc2	sál
<g/>
:	:	kIx,	:
délka	délka	k1gFnSc1	délka
62	[number]	k4	62
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
16	[number]	k4	16
m	m	kA	m
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
13	[number]	k4	13
m.	m.	k?	m.
Oproti	oproti	k7c3	oproti
pražskému	pražský	k2eAgInSc3d1	pražský
sálu	sál	k1gInSc3	sál
je	být	k5eAaImIp3nS	být
mladší	mladý	k2eAgFnSc1d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
rozdílem	rozdíl	k1gInSc7	rozdíl
je	být	k5eAaImIp3nS	být
zaklenutí	zaklenutí	k1gNnSc1	zaklenutí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
hvězdicová	hvězdicový	k2eAgFnSc1d1	hvězdicová
klenba	klenba	k1gFnSc1	klenba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vladislavském	vladislavský	k2eAgInSc6d1	vladislavský
sále	sál	k1gInSc6	sál
je	být	k5eAaImIp3nS	být
kroužená	kroužený	k2eAgFnSc1d1	kroužená
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
-	-	kIx~	-
čítá	čítat	k5eAaImIp3nS	čítat
na	na	k7c4	na
53	[number]	k4	53
tisíc	tisíc	k4xCgInPc2	tisíc
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
ji	on	k3xPp3gFnSc4	on
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
jediný	jediný	k2eAgMnSc1d1	jediný
člověk	člověk	k1gMnSc1	člověk
-	-	kIx~	-
lantkrabě	lantkrabě	k1gMnSc1	lantkrabě
Karel	Karel	k1gMnSc1	Karel
Egon	Egon	k1gMnSc1	Egon
I.	I.	kA	I.
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
okolo	okolo	k7c2	okolo
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
knihou	kniha	k1gFnSc7	kniha
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
je	být	k5eAaImIp3nS	být
hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
didaktika	didaktika	k1gFnSc1	didaktika
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
váží	vážit	k5eAaImIp3nS	vážit
přes	přes	k7c4	přes
11	[number]	k4	11
kilogramů	kilogram	k1gInPc2	kilogram
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
na	na	k7c4	na
2500	[number]	k4	2500
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
velké	velký	k2eAgFnPc1d1	velká
knihovny	knihovna	k1gFnPc1	knihovna
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
rostly	růst	k5eAaImAgFnP	růst
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
i	i	k8xC	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
sídlech	sídlo	k1gNnPc6	sídlo
rodu	rod	k1gInSc2	rod
-	-	kIx~	-
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Donaueschingenu	Donaueschingen	k1gInSc6	Donaueschingen
a	a	k8xC	a
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
Vitorazi	Vitoraz	k1gFnSc6	Vitoraz
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
<g/>
:	:	kIx,	:
Obrazárna	obrazárna	k1gFnSc1	obrazárna
-	-	kIx~	-
fürstenberské	fürstenberský	k2eAgFnSc2d1	fürstenberská
reprezentativní	reprezentativní	k2eAgFnSc2d1	reprezentativní
rodové	rodový	k2eAgFnSc2d1	rodová
podobizny	podobizna	k1gFnSc2	podobizna
z	z	k7c2	z
období	období	k1gNnSc2	období
16	[number]	k4	16
<g/>
.	.	kIx.	.
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
<g />
.	.	kIx.	.
</s>
<s>
vynikají	vynikat	k5eAaImIp3nP	vynikat
práce	práce	k1gFnPc1	práce
významných	významný	k2eAgMnPc2d1	významný
evropských	evropský	k2eAgMnPc2d1	evropský
portrétistů	portrétista	k1gMnPc2	portrétista
Martina	Martin	k1gMnSc2	Martin
van	vana	k1gFnPc2	vana
Meytense	Meytens	k1gMnSc2	Meytens
(	(	kIx(	(
<g/>
1695	[number]	k4	1695
<g/>
-	-	kIx~	-
<g/>
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc4	Josef
Weisse	Weiss	k1gMnSc4	Weiss
(	(	kIx(	(
<g/>
1699	[number]	k4	1699
<g/>
-	-	kIx~	-
<g/>
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
Grassiho	Grassi	k1gMnSc2	Grassi
(	(	kIx(	(
<g/>
1758	[number]	k4	1758
<g/>
-	-	kIx~	-
<g/>
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karla	Karel	k1gMnSc2	Karel
Škréty	Škréta	k1gMnSc2	Škréta
(	(	kIx(	(
<g/>
1610	[number]	k4	1610
<g/>
-	-	kIx~	-
<g/>
1674	[number]	k4	1674
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hradní	hradní	k2eAgNnSc1d1	hradní
muzeum	muzeum	k1gNnSc1	muzeum
-	-	kIx~	-
střední	střední	k2eAgFnSc1d1	střední
místnost	místnost	k1gFnSc1	místnost
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
Filipině	Filipin	k2eAgFnSc3d1	Filipina
Welserové	Welserová	k1gFnSc3	Welserová
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
především	především	k9	především
olejomalba	olejomalba	k1gFnSc1	olejomalba
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
Filipina	Filipin	k2eAgInSc2d1	Filipin
návštěvu	návštěva	k1gFnSc4	návštěva
Jana	Jan	k1gMnSc4	Jan
Augustu	Augusta	k1gMnSc4	Augusta
v	v	k7c6	v
křivoklátském	křivoklátský	k2eAgInSc6d1	křivoklátský
hradním	hradní	k2eAgInSc6d1	hradní
žaláři	žalář	k1gInSc6	žalář
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vrcholným	vrcholný	k2eAgNnSc7d1	vrcholné
životním	životní	k2eAgNnSc7d1	životní
dílem	dílo	k1gNnSc7	dílo
malíře	malíř	k1gMnSc2	malíř
romantického	romantický	k2eAgInSc2d1	romantický
historického	historický	k2eAgInSc2d1	historický
žánru	žánr	k1gInSc2	žánr
Alfréda	Alfréd	k1gMnSc2	Alfréd
Seiferta	Seifert	k1gMnSc2	Seifert
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hradní	hradní	k2eAgNnSc1d1	hradní
muzeum	muzeum	k1gNnSc1	muzeum
-	-	kIx~	-
Velký	velký	k2eAgInSc1d1	velký
sál	sál	k1gInSc1	sál
královnina	královnin	k2eAgNnSc2d1	královnino
křídla	křídlo	k1gNnSc2	křídlo
zpřístupňuje	zpřístupňovat	k5eAaImIp3nS	zpřístupňovat
sbírku	sbírka	k1gFnSc4	sbírka
historických	historický	k2eAgFnPc2d1	historická
saní	saně	k1gFnPc2	saně
z	z	k7c2	z
období	období	k1gNnSc2	období
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
rokoka	rokoko	k1gNnSc2	rokoko
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zdobí	zdobit	k5eAaImIp3nS	zdobit
řezba	řezba	k1gFnSc1	řezba
i	i	k8xC	i
malba	malba	k1gFnSc1	malba
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
Prodavačka	prodavačka	k1gFnSc1	prodavačka
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
malíř	malíř	k1gMnSc1	malíř
Joachim	Joachim	k1gMnSc1	Joachim
Beuckelaer	Beuckelaer	k1gMnSc1	Beuckelaer
(	(	kIx(	(
<g/>
1530	[number]	k4	1530
<g/>
-	-	kIx~	-
<g/>
1574	[number]	k4	1574
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
učitelem	učitel	k1gMnSc7	učitel
byl	být	k5eAaImAgMnS	být
Peter	Peter	k1gMnSc1	Peter
Paul	Paul	k1gMnSc1	Paul
Rubens	Rubens	k1gInSc4	Rubens
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hradním	hradní	k2eAgNnSc7d1	hradní
jádrem	jádro	k1gNnSc7	jádro
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
předhradí	předhradí	k1gNnSc1	předhradí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
kompletně	kompletně	k6eAd1	kompletně
obestavěno	obestavět	k5eAaPmNgNnS	obestavět
budovami	budova	k1gFnPc7	budova
a	a	k8xC	a
flankováno	flankován	k2eAgNnSc4d1	flankován
dvojicí	dvojice	k1gFnSc7	dvojice
pozdněgotických	pozdněgotický	k2eAgFnPc2d1	pozdněgotická
bašt	bašta	k1gFnPc2	bašta
<g/>
,	,	kIx,	,
situovaných	situovaný	k2eAgInPc2d1	situovaný
na	na	k7c6	na
spodních	spodní	k2eAgNnPc6d1	spodní
nárožích	nároží	k1gNnPc6	nároží
hradního	hradní	k2eAgInSc2d1	hradní
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
hradního	hradní	k2eAgNnSc2d1	hradní
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
situována	situován	k2eAgFnSc1d1	situována
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
brána	brána	k1gFnSc1	brána
-	-	kIx~	-
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
tesaný	tesaný	k2eAgInSc4d1	tesaný
český	český	k2eAgInSc4d1	český
lev	lev	k1gInSc4	lev
<g/>
.	.	kIx.	.
</s>
<s>
Pokladna	pokladna	k1gFnSc1	pokladna
a	a	k8xC	a
suvenýry	suvenýr	k1gInPc1	suvenýr
-	-	kIx~	-
za	za	k7c7	za
branou	brána	k1gFnSc7	brána
vlevo	vlevo	k6eAd1	vlevo
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ulička	ulička	k1gFnSc1	ulička
(	(	kIx(	(
<g/>
za	za	k7c7	za
branou	brána	k1gFnSc7	brána
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
prampouchy	prampouch	k1gInPc1	prampouch
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
Malou	malý	k2eAgFnSc4d1	malá
parkánovou	parkánový	k2eAgFnSc4d1	Parkánová
zahrádku	zahrádka	k1gFnSc4	zahrádka
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
průvodcovské	průvodcovský	k2eAgFnSc2d1	průvodcovská
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
byt	byt	k1gInSc1	byt
kastelána	kastelán	k1gMnSc2	kastelán
<g/>
.	.	kIx.	.
</s>
<s>
Prochoditá	prochoditý	k2eAgFnSc1d1	Prochoditá
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
hodinovým	hodinový	k2eAgInSc7d1	hodinový
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
pivovar	pivovar	k1gInSc1	pivovar
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgNnSc1d1	jižní
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
správa	správa	k1gFnSc1	správa
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Hejtmanství	hejtmanství	k1gNnSc1	hejtmanství
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgNnSc1d1	jižní
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
pivovarský	pivovarský	k2eAgInSc1d1	pivovarský
provoz	provoz	k1gInSc1	provoz
<g/>
,	,	kIx,	,
hradní	hradní	k2eAgInSc1d1	hradní
pivovar	pivovar	k1gInSc1	pivovar
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Místnosti	místnost	k1gFnPc1	místnost
suterénu	suterén	k1gInSc2	suterén
jsou	být	k5eAaImIp3nP	být
propojeny	propojen	k2eAgFnPc1d1	propojena
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
galerií	galerie	k1gFnPc2	galerie
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
budovou	budova	k1gFnSc7	budova
dnešní	dnešní	k2eAgFnSc2d1	dnešní
správy	správa	k1gFnSc2	správa
hradu	hrad	k1gInSc2	hrad
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc1d1	starý
pivovar	pivovar	k1gInSc1	pivovar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
u	u	k7c2	u
kamen	kamna	k1gNnPc2	kamna
Galerie	galerie	k1gFnSc2	galerie
Spilka	spilka	k1gFnSc1	spilka
-	-	kIx~	-
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
výstava	výstava	k1gFnSc1	výstava
o	o	k7c6	o
československých	československý	k2eAgMnPc6d1	československý
legionářích	legionář	k1gMnPc6	legionář
Galerie	galerie	k1gFnSc2	galerie
Lednice	Lednice	k1gFnSc2	Lednice
Purkrabství	purkrabství	k1gNnSc1	purkrabství
(	(	kIx(	(
<g/>
západní	západní	k2eAgNnSc1d1	západní
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
)	)	kIx)	)
Manské	manský	k2eAgInPc1d1	manský
domy	dům	k1gInPc1	dům
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
kuchyně	kuchyně	k1gFnSc1	kuchyně
(	(	kIx(	(
<g/>
severní	severní	k2eAgNnSc1d1	severní
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
)	)	kIx)	)
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
byty	byt	k1gInPc4	byt
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
ubytování	ubytování	k1gNnSc4	ubytování
pro	pro	k7c4	pro
průvodce	průvodce	k1gMnSc4	průvodce
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
jsou	být	k5eAaImIp3nP	být
sluneční	sluneční	k2eAgFnPc1d1	sluneční
hodiny	hodina	k1gFnPc1	hodina
s	s	k7c7	s
latinským	latinský	k2eAgInSc7d1	latinský
nápisem	nápis	k1gInSc7	nápis
Horas	Horas	k1gInSc1	Horas
non	non	k?	non
numero	numero	k?	numero
<g/>
,	,	kIx,	,
nisi	nisit	k5eAaPmRp2nS	nisit
serenas	serenas	k1gInSc1	serenas
(	(	kIx(	(
<g/>
Počítám	počítat	k5eAaImIp1nS	počítat
jen	jen	k9	jen
ty	ten	k3xDgFnPc4	ten
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
jasné	jasný	k2eAgFnPc1d1	jasná
<g/>
)	)	kIx)	)
a	a	k8xC	a
vročením	vročení	k1gNnSc7	vročení
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
Huderka	Huderka	k1gFnSc1	Huderka
-	-	kIx~	-
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věži	věž	k1gFnSc6	věž
byl	být	k5eAaImAgMnS	být
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
anglický	anglický	k2eAgMnSc1d1	anglický
alchymista	alchymista	k1gMnSc1	alchymista
<g/>
,	,	kIx,	,
šarlatán	šarlatán	k1gMnSc1	šarlatán
a	a	k8xC	a
podvodník	podvodník	k1gMnSc1	podvodník
Edward	Edward	k1gMnSc1	Edward
Kelley	Kellea	k1gFnSc2	Kellea
<g/>
.	.	kIx.	.
</s>
<s>
Studna	studna	k1gFnSc1	studna
má	mít	k5eAaImIp3nS	mít
hloubku	hloubka	k1gFnSc4	hloubka
42	[number]	k4	42
m	m	kA	m
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
tak	tak	k6eAd1	tak
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
věž	věž	k1gFnSc1	věž
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Nefunkční	funkční	k2eNgFnSc1d1	nefunkční
kovová	kovový	k2eAgFnSc1d1	kovová
kašna	kašna	k1gFnSc1	kašna
ozdobená	ozdobený	k2eAgFnSc1d1	ozdobená
fürstenberskými	fürstenberský	k2eAgInPc7d1	fürstenberský
erby	erb	k1gInPc7	erb
a	a	k8xC	a
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
fiála	fiála	k1gFnSc1	fiála
s	s	k7c7	s
vročením	vročení	k1gNnSc7	vročení
1858	[number]	k4	1858
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
×	×	k?	×
jírovec	jírovec	k1gInSc1	jírovec
(	(	kIx(	(
<g/>
Aesculus	Aesculus	k1gInSc1	Aesculus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
×	×	k?	×
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
<g/>
)	)	kIx)	)
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
mladší	mladý	k2eAgFnSc1d2	mladší
líska	líska	k1gFnSc1	líska
turecká	turecký	k2eAgFnSc1d1	turecká
(	(	kIx(	(
<g/>
Corylus	Corylus	k1gInSc1	Corylus
colurna	colurno	k1gNnSc2	colurno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejužším	úzký	k2eAgNnSc6d3	nejužší
místě	místo	k1gNnSc6	místo
ostrožny	ostrožna	k1gFnSc2	ostrožna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
objekt	objekt	k1gInSc1	objekt
pivovarských	pivovarský	k2eAgInPc2d1	pivovarský
sklepů	sklep	k1gInPc2	sklep
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
hradní	hradní	k2eAgInSc4d1	hradní
pivovar	pivovar	k1gInSc4	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Pivo	pivo	k1gNnSc1	pivo
se	se	k3xPyFc4	se
vařilo	vařit	k5eAaImAgNnS	vařit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Hejtmanství	hejtmanství	k1gNnSc2	hejtmanství
na	na	k7c6	na
dolním	dolní	k2eAgNnSc6d1	dolní
nádvoří	nádvoří	k1gNnSc6	nádvoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ležácké	ležácký	k2eAgInPc4d1	ležácký
sklepy	sklep	k1gInPc4	sklep
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
více	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Přízemí	přízemí	k1gNnSc1	přízemí
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
bednárna	bednárna	k1gFnSc1	bednárna
<g/>
,	,	kIx,	,
lahvárna	lahvárna	k1gFnSc1	lahvárna
<g/>
,	,	kIx,	,
umývárna	umývárna	k1gFnSc1	umývárna
a	a	k8xC	a
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
stáčení	stáčení	k1gNnSc4	stáčení
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
prostorách	prostora	k1gFnPc6	prostora
počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
zprovozněním	zprovoznění	k1gNnSc7	zprovoznění
minipivovaru	minipivovar	k1gInSc2	minipivovar
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc2	restaurace
a	a	k8xC	a
ubytování	ubytování	k1gNnSc2	ubytování
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
sklepech	sklep	k1gInPc6	sklep
expozice	expozice	k1gFnSc2	expozice
Křivoklátské	křivoklátský	k2eAgInPc4d1	křivoklátský
přízraky	přízrak	k1gInPc4	přízrak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
budova	budova	k1gFnSc1	budova
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
ubytovna	ubytovna	k1gFnSc1	ubytovna
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
a	a	k8xC	a
technické	technický	k2eAgNnSc4d1	technické
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pivovarskými	pivovarský	k2eAgInPc7d1	pivovarský
sklepy	sklep	k1gInPc7	sklep
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c4	na
bývalým	bývalý	k2eAgMnSc7d1	bývalý
hradním	hradní	k2eAgInSc7d1	hradní
příkopem	příkop	k1gInSc7	příkop
kaplička	kaplička	k1gFnSc1	kaplička
svaté	svatý	k2eAgFnSc2d1	svatá
Trojice	trojice	k1gFnSc2	trojice
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
asi	asi	k9	asi
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
sedmnáctého	sedmnáctý	k4xOgMnSc4	sedmnáctý
a	a	k8xC	a
osmnáctého	osmnáctý	k4xOgMnSc4	osmnáctý
století	století	k1gNnSc6	století
a	a	k8xC	a
bývala	bývat	k5eAaImAgFnS	bývat
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
svatému	svatý	k1gMnSc3	svatý
Vendelínovi	Vendelín	k1gMnSc3	Vendelín
a	a	k8xC	a
Nejsvětější	nejsvětější	k2eAgFnSc3d1	nejsvětější
Trojici	trojice	k1gFnSc3	trojice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
vysvěcena	vysvěcen	k2eAgFnSc1d1	vysvěcena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bocích	bok	k1gInPc6	bok
kapličky	kaplička	k1gFnPc1	kaplička
jsou	být	k5eAaImIp3nP	být
obrazy	obraz	k1gInPc4	obraz
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Vendelína	Vendelín	k1gMnSc2	Vendelín
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Šefců	Šefce	k1gMnPc2	Šefce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
začátku	začátek	k1gInSc2	začátek
příjezdové	příjezdový	k2eAgFnSc2d1	příjezdová
cesty	cesta	k1gFnSc2	cesta
stojí	stát	k5eAaImIp3nS	stát
pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Nahoře	nahoře	k6eAd1	nahoře
je	být	k5eAaImIp3nS	být
medailon	medailon	k1gInSc1	medailon
s	s	k7c7	s
poprsím	poprsí	k1gNnSc7	poprsí
mistra	mistr	k1gMnSc2	mistr
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
rámuje	rámovat	k5eAaImIp3nS	rámovat
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
Také	také	k9	také
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
se	se	k3xPyFc4	se
milowali	milowat	k5eAaImAgMnP	milowat
we	we	k?	we
spolek	spolek	k1gInSc1	spolek
a	a	k8xC	a
prawdy	prawdy	k6eAd1	prawdy
každému	každý	k3xTgMnSc3	každý
přáli	přát	k5eAaImAgMnP	přát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
1415	[number]	k4	1415
<g/>
,	,	kIx,	,
S	s	k7c7	s
VAŠÍ	váš	k3xOp2gFnSc7	váš
SMRTÍ	smrtit	k5eAaImIp3nS	smrtit
NAŠE	náš	k3xOp1gNnSc1	náš
SVOBODA	svoboda	k1gFnSc1	svoboda
<g/>
"	"	kIx"	"
a	a	k8xC	a
výčet	výčet	k1gInSc4	výčet
padlých	padlý	k1gMnPc2	padlý
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
bráně	brána	k1gFnSc3	brána
stojí	stát	k5eAaImIp3nS	stát
osm	osm	k4xCc1	osm
zděných	zděný	k2eAgInPc2d1	zděný
podstavců	podstavec	k1gInPc2	podstavec
soch	socha	k1gFnPc2	socha
z	z	k7c2	z
období	období	k1gNnPc2	období
1731	[number]	k4	1731
<g/>
-	-	kIx~	-
<g/>
1733	[number]	k4	1733
<g/>
.	.	kIx.	.
</s>
<s>
Bývaly	bývat	k5eAaImAgFnP	bývat
zde	zde	k6eAd1	zde
sochy	socha	k1gFnPc1	socha
světců	světec	k1gMnPc2	světec
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
lidového	lidový	k2eAgNnSc2d1	lidové
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
sochou	socha	k1gFnSc7	socha
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
kopie	kopie	k1gFnSc1	kopie
sochy	socha	k1gFnSc2	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
originál	originál	k1gInSc1	originál
byl	být	k5eAaImAgInS	být
restaurován	restaurovat	k5eAaBmNgInS	restaurovat
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
akademického	akademický	k2eAgMnSc2d1	akademický
sochaře	sochař	k1gMnSc2	sochař
J.	J.	kA	J.
Novotného	Novotný	k1gMnSc4	Novotný
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
dochovanými	dochovaný	k2eAgFnPc7d1	dochovaná
sochami	socha	k1gFnPc7	socha
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
depozitáři	depozitář	k1gInSc6	depozitář
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
sochy	socha	k1gFnPc1	socha
představovaly	představovat	k5eAaImAgFnP	představovat
svatého	svatý	k2eAgMnSc2d1	svatý
Antonína	Antonín	k1gMnSc2	Antonín
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Evangelistu	evangelista	k1gMnSc4	evangelista
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc4d1	svatý
Floriána	Florián	k1gMnSc4	Florián
<g/>
,	,	kIx,	,
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
podstavci	podstavec	k1gInSc6	podstavec
stál	stát	k5eAaImAgInS	stát
kříž	kříž	k1gInSc1	kříž
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
socha	socha	k1gFnSc1	socha
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
svatého	svatý	k2eAgMnSc2d1	svatý
Josefa	Josef	k1gMnSc2	Josef
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
příjezdové	příjezdový	k2eAgFnSc2d1	příjezdová
cesty	cesta	k1gFnSc2	cesta
stojí	stát	k5eAaImIp3nS	stát
budova	budova	k1gFnSc1	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
četnické	četnický	k2eAgFnSc2d1	četnická
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
pracovali	pracovat	k5eAaImAgMnP	pracovat
tři	tři	k4xCgInPc4	tři
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
šest	šest	k4xCc1	šest
četníků	četník	k1gMnPc2	četník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jednu	jeden	k4xCgFnSc4	jeden
motorku	motorka	k1gFnSc4	motorka
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
v	v	k7c6	v
takových	takový	k3xDgFnPc6	takový
stanicích	stanice	k1gFnPc6	stanice
bývalo	bývat	k5eAaImAgNnS	bývat
skromné	skromný	k2eAgNnSc1d1	skromné
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
ho	on	k3xPp3gMnSc4	on
tvořil	tvořit	k5eAaImAgInS	tvořit
jen	jen	k9	jen
stůl	stůl	k1gInSc1	stůl
s	s	k7c7	s
telefonem	telefon	k1gInSc7	telefon
<g/>
,	,	kIx,	,
židle	židle	k1gFnSc1	židle
<g/>
,	,	kIx,	,
stojací	stojací	k2eAgInSc1d1	stojací
věšák	věšák	k1gInSc1	věšák
<g/>
,	,	kIx,	,
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
ohrádka	ohrádka	k1gFnSc1	ohrádka
oddělující	oddělující	k2eAgFnSc1d1	oddělující
prostor	prostor	k1gInSc4	prostor
vyhrazený	vyhrazený	k2eAgInSc1d1	vyhrazený
četníkům	četník	k1gMnPc3	četník
od	od	k7c2	od
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
a	a	k8xC	a
zázemí	zázemí	k1gNnSc4	zázemí
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
po	po	k7c6	po
zřízení	zřízení	k1gNnSc6	zřízení
Sboru	sbor	k1gInSc2	sbor
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pokladna	pokladna	k1gFnSc1	pokladna
a	a	k8xC	a
průvodcovská	průvodcovský	k2eAgFnSc1d1	průvodcovská
místnost	místnost	k1gFnSc1	místnost
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
komerčnímu	komerční	k2eAgNnSc3d1	komerční
ubytování	ubytování	k1gNnSc3	ubytování
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
v	v	k7c6	v
protějším	protější	k2eAgInSc6d1	protější
svahu	svah	k1gInSc6	svah
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
Nových	Nových	k2eAgFnPc6d1	Nových
cestách	cesta	k1gFnPc6	cesta
nachází	nacházet	k5eAaImIp3nS	nacházet
pomník	pomník	k1gInSc4	pomník
Karla	Karel	k1gMnSc2	Karel
Egona	Egon	k1gMnSc2	Egon
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Fürstenbergu	Fürstenberg	k1gInSc2	Fürstenberg
<g/>
.	.	kIx.	.
</s>
<s>
Novogotický	novogotický	k2eAgInSc1d1	novogotický
čtrnáct	čtrnáct	k4xCc4	čtrnáct
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
pískovcový	pískovcový	k2eAgInSc4d1	pískovcový
pomník	pomník	k1gInSc4	pomník
byl	být	k5eAaImAgInS	být
zhotoven	zhotovit	k5eAaPmNgInS	zhotovit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1858	[number]	k4	1858
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
Karlem	Karel	k1gMnSc7	Karel
J.	J.	kA	J.
Svobodou	Svoboda	k1gMnSc7	Svoboda
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Antonína	Antonín	k1gMnSc2	Antonín
Jiruše	Jiruše	k1gFnSc2	Jiruše
<g/>
.	.	kIx.	.
</s>
<s>
Portrétní	portrétní	k2eAgFnSc4d1	portrétní
bustu	busta	k1gFnSc4	busta
knížete	kníže	k1gMnSc2	kníže
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
mramoru	mramor	k1gInSc2	mramor
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sochař	sochař	k1gMnSc1	sochař
Emanuel	Emanuel	k1gMnSc1	Emanuel
Max	Max	k1gMnSc1	Max
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
-	-	kIx~	-
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Monument	monument	k1gInSc4	monument
věnovali	věnovat	k5eAaPmAgMnP	věnovat
čeští	český	k2eAgMnPc1d1	český
fürstenberští	fürstenberský	k2eAgMnPc1d1	fürstenberský
úředníci	úředník	k1gMnPc1	úředník
a	a	k8xC	a
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
křivoklátského	křivoklátský	k2eAgNnSc2d1	Křivoklátské
panství	panství	k1gNnSc2	panství
jako	jako	k8xS	jako
výraz	výraz	k1gInSc1	výraz
úcty	úcta	k1gFnSc2	úcta
a	a	k8xC	a
vděku	vděk	k1gInSc2	vděk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čtvercovém	čtvercový	k2eAgInSc6d1	čtvercový
podstavci	podstavec	k1gInSc6	podstavec
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgFnPc4	čtyři
mramorové	mramorový	k2eAgFnPc4d1	mramorová
desky	deska	k1gFnPc4	deska
s	s	k7c7	s
nápisy	nápis	k1gInPc7	nápis
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
němčině	němčina	k1gFnSc6	němčina
a	a	k8xC	a
latině	latina	k1gFnSc6	latina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
opěvují	opěvovat	k5eAaImIp3nP	opěvovat
knížete	kníže	k1gNnSc4wR	kníže
Karla	Karel	k1gMnSc4	Karel
Egona	Egon	k1gMnSc2	Egon
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1795	[number]	k4	1795
<g/>
-	-	kIx~	-
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Princeps	Princeps	k1gInSc1	Princeps
serenissimus	serenissimus	k1gMnSc1	serenissimus
Carolus	Carolus	k1gMnSc1	Carolus
Egon	Egon	k1gMnSc1	Egon
de	de	k?	de
Fürstenberg	Fürstenberg	k1gMnSc1	Fürstenberg
<g/>
,	,	kIx,	,
quum	quum	k1gMnSc1	quum
eruditione	erudition	k1gInSc5	erudition
per	pero	k1gNnPc2	pero
civitatem	civitato	k1gNnSc7	civitato
<g/>
,	,	kIx,	,
tum	tum	k?	tum
humanitate	humanitat	k1gMnSc5	humanitat
domi	dom	k1gMnSc5	dom
<g/>
,	,	kIx,	,
forisque	forisquat	k5eAaPmIp3nS	forisquat
praeclarus	praeclarus	k1gMnSc1	praeclarus
natus	natus	k1gMnSc1	natus
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Octob	Octoba	k1gFnPc2	Octoba
<g/>
.	.	kIx.	.
1796	[number]	k4	1796
<g/>
.	.	kIx.	.
obiit	obiit	k1gInSc1	obiit
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
<s>
Octob	Octoba	k1gFnPc2	Octoba
<g/>
.	.	kIx.	.
1854	[number]	k4	1854
<g/>
"	"	kIx"	"
-	-	kIx~	-
přední	přední	k2eAgFnSc1d1	přední
strana	strana	k1gFnSc1	strana
pomníku	pomník	k1gInSc2	pomník
"	"	kIx"	"
<g/>
Quod	Quod	k1gInSc1	Quod
ut	ut	k?	ut
venerationis	venerationis	k1gInSc1	venerationis
ita	ita	k?	ita
amoris	amoris	k1gFnSc2	amoris
monimentum	monimentum	k1gNnSc1	monimentum
<g/>
,	,	kIx,	,
qui	qui	k?	qui
Bohemicis	Bohemicis	k1gFnSc1	Bohemicis
ejus	ejus	k1gInSc1	ejus
possessionibus	possessionibus	k1gMnSc1	possessionibus
curandis	curandis	k1gFnPc2	curandis
praeerant	praeerant	k1gMnSc1	praeerant
<g/>
.	.	kIx.	.
</s>
<s>
Administri	Administri	k6eAd1	Administri
et	et	k?	et
Officiales	Officiales	k1gInSc1	Officiales
veluti	veluť	k1gFnSc2	veluť
patri	patr	k1gFnSc2	patr
posuerunt	posuerunt	k1gInSc1	posuerunt
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
zadní	zadní	k2eAgFnSc1d1	zadní
strana	strana	k1gFnSc1	strana
"	"	kIx"	"
<g/>
Postavený	postavený	k2eAgMnSc1d1	postavený
od	od	k7c2	od
úředníkův	úředníkův	k2eAgInSc4d1	úředníkův
a	a	k8xC	a
služebníkův	služebníkův	k2eAgInSc4d1	služebníkův
českých	český	k2eAgMnPc2d1	český
domu	dům	k1gInSc2	dům
knížat	kníže	k1gMnPc2wR	kníže
z	z	k7c2	z
Fürstenbergů	Fürstenberg	k1gInPc2	Fürstenberg
<g/>
.	.	kIx.	.
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
vlevo	vlevo	k6eAd1	vlevo
"	"	kIx"	"
<g/>
Errichtet	Errichtet	k1gInSc1	Errichtet
von	von	k1gInSc1	von
den	den	k1gInSc1	den
Fürstlich	Fürstlich	k1gInSc1	Fürstlich
Fürstenberg	Fürstenberg	k1gInSc4	Fürstenberg
<g/>
'	'	kIx"	'
<g/>
schen	schen	k1gInSc4	schen
Beamten	Beamten	k2eAgInSc4d1	Beamten
und	und	k?	und
Dienern	Dienern	k1gInSc4	Dienern
in	in	k?	in
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
.	.	kIx.	.
1860	[number]	k4	1860
<g/>
"	"	kIx"	"
-	-	kIx~	-
vpravo	vpravo	k6eAd1	vpravo
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
busta	busta	k1gFnSc1	busta
svržena	svrhnout	k5eAaPmNgFnS	svrhnout
ze	z	k7c2	z
stráně	stráň	k1gFnSc2	stráň
kladenskými	kladenský	k2eAgInPc7d1	kladenský
radikály	radikál	k1gInPc7	radikál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vedl	vést	k5eAaImAgMnS	vést
agitátor	agitátor	k1gMnSc1	agitátor
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
komunistický	komunistický	k2eAgMnSc1d1	komunistický
poslanec	poslanec	k1gMnSc1	poslanec
Alois	Alois	k1gMnSc1	Alois
Muna	muna	k1gFnSc1	muna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1928	[number]	k4	1928
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
umístěna	umístit	k5eAaPmNgFnS	umístit
pod	pod	k7c4	pod
baldachýn	baldachýn	k1gInSc4	baldachýn
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dvanáct	dvanáct	k4xCc1	dvanáct
křivoklátských	křivoklátský	k2eAgFnPc2d1	Křivoklátská
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
spolků	spolek	k1gInPc2	spolek
sepsalo	sepsat	k5eAaPmAgNnS	sepsat
petici	petice	k1gFnSc4	petice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zase	zase	k9	zase
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
městské	městský	k2eAgNnSc1d1	Městské
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
opětovně	opětovně	k6eAd1	opětovně
stržena	stržen	k2eAgFnSc1d1	stržena
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
však	však	k9	však
tajně	tajně	k6eAd1	tajně
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
široké	široký	k2eAgFnSc6d1	široká
osvětě	osvěta	k1gFnSc6	osvěta
a	a	k8xC	a
zásahu	zásah	k1gInSc6	zásah
Státního	státní	k2eAgInSc2d1	státní
památkového	památkový	k2eAgInSc2d1	památkový
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
umístěna	umístit	k5eAaPmNgFnS	umístit
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pomníku	pomník	k1gInSc2	pomník
je	být	k5eAaImIp3nS	být
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
se	se	k3xPyFc4	se
natáčely	natáčet	k5eAaImAgInP	natáčet
následující	následující	k2eAgInPc1d1	následující
filmy	film	k1gInPc1	film
a	a	k8xC	a
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
:	:	kIx,	:
Slasti	slast	k1gFnSc2	slast
Otce	otec	k1gMnSc2	otec
vlasti	vlast	k1gFnSc2	vlast
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Steklý	Steklý	k1gMnSc1	Steklý
<g/>
)	)	kIx)	)
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podskalský	podskalský	k2eAgMnSc1d1	podskalský
<g/>
)	)	kIx)	)
Honza	Honza	k1gMnSc1	Honza
málem	málem	k6eAd1	málem
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
budí	budit	k5eAaImIp3nP	budit
princezny	princezna	k1gFnPc1	princezna
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Vorlíček	Vorlíček	k1gMnSc1	Vorlíček
<g/>
)	)	kIx)	)
Nebojte	bát	k5eNaImRp2nP	bát
se	se	k3xPyFc4	se
na	na	k7c6	na
Česnečce	česnečka	k1gFnSc6	česnečka
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Libuše	Libuše	k1gFnSc1	Libuše
Koutná	Koutná	k1gFnSc1	Koutná
<g/>
)	)	kIx)	)
Třetí	třetí	k4xOgMnSc1	třetí
princ	princ	k1gMnSc1	princ
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Antonín	Antonín	k1gMnSc1	Antonín
Moskalyk	Moskalyk	k1gMnSc1	Moskalyk
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Tři	tři	k4xCgMnPc1	tři
veteráni	veterán	k1gMnPc1	veterán
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lipský	lipský	k2eAgMnSc1d1	lipský
<g/>
)	)	kIx)	)
Anděl	Anděl	k1gMnSc1	Anděl
Páně	páně	k2eAgMnSc1d1	páně
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Strach	strach	k1gInSc1	strach
<g/>
)	)	kIx)	)
Kletba	kletba	k1gFnSc1	kletba
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gInPc2	Grimm
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Wanted	Wanted	k1gInSc1	Wanted
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Křivoklad	Křivoklad	k1gInSc1	Křivoklad
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
<g/>
)	)	kIx)	)
Motiv	motiv	k1gInSc1	motiv
z	z	k7c2	z
Křivoklátu	Křivoklát	k1gInSc2	Křivoklát
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
Vězeň	vězeň	k1gMnSc1	vězeň
na	na	k7c6	na
Křivoklátě	Křivoklát	k1gInSc6	Křivoklát
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Svátek	Svátek	k1gMnSc1	Svátek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trojdílný	trojdílný	k2eAgInSc1d1	trojdílný
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
</s>
