<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
současné	současný	k2eAgNnSc1d1	současné
Rusko	Rusko	k1gNnSc1	Rusko
mnohonárodnostním	mnohonárodnostní	k2eAgInSc7d1	mnohonárodnostní
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kromě	kromě	k7c2	kromě
úřední	úřední	k2eAgFnSc2d1	úřední
ruštiny	ruština	k1gFnSc2	ruština
uznává	uznávat	k5eAaImIp3nS	uznávat
také	také	k9	také
regionální	regionální	k2eAgInSc4d1	regionální
jazyky	jazyk	k1gInPc4	jazyk
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
