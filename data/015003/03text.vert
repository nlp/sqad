<s>
Mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
</s>
<s>
Mentální	mentální	k2eAgFnSc1d1
retardaceKlasifikace	retardaceKlasifikace	k1gFnSc1
MKN-10	MKN-10	k1gFnSc2
</s>
<s>
F70	F70	k4
F71	F71	k1gMnSc1
F72	F72	k1gMnSc1
F73	F73	k1gMnSc1
F78	F78	k1gMnSc1
F79	F79	k1gMnSc1
MeSH	MeSH	k1gMnSc1
</s>
<s>
D008607	D008607	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
mentální	mentální	k2eAgNnSc1d1
postižení	postižení	k1gNnSc1
<g/>
,	,	kIx,
je	on	k3xPp3gNnSc4
trvalé	trvalý	k2eAgNnSc4d1
snížení	snížení	k1gNnSc4
inteligence	inteligence	k1gFnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
organického	organický	k2eAgNnSc2d1
poškození	poškození	k1gNnSc2
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retardaci	retardace	k1gFnSc6
tedy	tedy	k8xC
nelze	lze	k6eNd1
léčit	léčit	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
nejde	jít	k5eNaImIp3nS
o	o	k7c4
nemoc	nemoc	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
trvalý	trvalý	k2eAgInSc1d1
fyziologický	fyziologický	k2eAgInSc1d1
stav	stav	k1gInSc1
(	(	kIx(
<g/>
zaostalý	zaostalý	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
rozumových	rozumový	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
<g/>
,	,	kIx,
odlišný	odlišný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
některých	některý	k3yIgFnPc2
psychických	psychický	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
poruchy	porucha	k1gFnPc1
ve	v	k7c6
schopnosti	schopnost	k1gFnSc6
adaptace	adaptace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinci	jedinec	k1gMnPc7
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
k	k	k7c3
zaostávání	zaostávání	k1gNnSc3
rozumového	rozumový	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
z	z	k7c2
jiných	jiný	k2eAgMnPc2d1
důvodů	důvod	k1gInPc2
než	než	k8xS
kvůli	kvůli	k7c3
organickému	organický	k2eAgNnSc3d1
poškození	poškození	k1gNnSc3
mozku	mozek	k1gInSc2
(	(	kIx(
<g/>
například	například	k6eAd1
kvůli	kvůli	k7c3
vlivu	vliv	k1gInSc3
společenského	společenský	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
nemožnosti	nemožnost	k1gFnSc3
vzdělávání	vzdělávání	k1gNnSc2
apod.	apod.	kA
<g/>
)	)	kIx)
a	a	k8xC
vykazují	vykazovat	k5eAaImIp3nP
IQ	iq	kA
70	#num#	k4
a	a	k8xC
více	hodně	k6eAd2
<g/>
,	,	kIx,
se	se	k3xPyFc4
sice	sice	k8xC
považují	považovat	k5eAaImIp3nP
za	za	k7c4
mírně	mírně	k6eAd1
mentálně	mentálně	k6eAd1
retardované	retardovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nikoliv	nikoliv	k9
z	z	k7c2
klinických	klinický	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mentální	mentální	k2eAgFnSc4d1
retardaci	retardace	k1gFnSc4
klasifikujeme	klasifikovat	k5eAaImIp1nP
v	v	k7c6
šesti	šest	k4xCc6
základních	základní	k2eAgFnPc6d1
kategoriích	kategorie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
tradiční	tradiční	k2eAgFnPc4d1
označení	označení	k1gNnSc3
těchto	tento	k3xDgNnPc2
postižení	postižení	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
běžná	běžný	k2eAgFnSc1d1
zhruba	zhruba	k6eAd1
do	do	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
již	již	k6eAd1
v	v	k7c6
psychiatrické	psychiatrický	k2eAgFnSc6d1
terminologii	terminologie	k1gFnSc6
nepoužívají	používat	k5eNaImIp3nP
(	(	kIx(
<g/>
zejména	zejména	k9
výrazy	výraz	k1gInPc1
debilita	debilita	k1gFnSc1
<g/>
,	,	kIx,
idiocie	idiocie	k1gFnSc1
a	a	k8xC
imbecilita	imbecilita	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příčiny	příčina	k1gFnPc4
vzniku	vznik	k1gInSc2
mentální	mentální	k2eAgFnSc2d1
retardace	retardace	k1gFnSc2
</s>
<s>
Mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
může	moct	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
ve	v	k7c6
třech	tři	k4xCgNnPc6
obdobích	období	k1gNnPc6
<g/>
:	:	kIx,
</s>
<s>
prenatálním	prenatální	k2eAgMnPc3d1
(	(	kIx(
<g/>
před	před	k7c7
narozením	narození	k1gNnSc7
dítěte	dítě	k1gNnSc2
<g/>
)	)	kIx)
–	–	k?
kvůli	kvůli	k7c3
infekci	infekce	k1gFnSc3
matky	matka	k1gFnSc2
během	během	k7c2
těhotenství	těhotenství	k1gNnSc2
<g/>
,	,	kIx,
jejímu	její	k3xOp3gInSc3
špatnému	špatný	k2eAgInSc3d1
životnímu	životní	k2eAgInSc3d1
stylu	styl	k1gInSc3
<g/>
,	,	kIx,
úrazu	úraz	k1gInSc3
</s>
<s>
perinatálním	perinatální	k2eAgInSc7d1
(	(	kIx(
<g/>
během	běh	k1gInSc7
porodu	porod	k1gInSc2
nebo	nebo	k8xC
těsně	těsně	k6eAd1
po	po	k7c6
něm	on	k3xPp3gInSc6
<g/>
)	)	kIx)
–	–	k?
porod	porod	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
protrahovaný	protrahovaný	k2eAgMnSc1d1
(	(	kIx(
<g/>
dlouhotrvající	dlouhotrvající	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
hypoxii	hypoxie	k1gFnSc3
plodu	plod	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
k	k	k7c3
nedostatku	nedostatek	k1gInSc3
kyslíku	kyslík	k1gInSc2
atd.	atd.	kA
</s>
<s>
postnatálním	postnatální	k2eAgMnPc3d1
(	(	kIx(
<g/>
po	po	k7c6
porodu	porod	k1gInSc6
do	do	k7c2
2	#num#	k4
let	léto	k1gNnPc2
věku	věk	k1gInSc2
dítěte	dítě	k1gNnSc2
<g/>
)	)	kIx)
–	–	k?
infekce	infekce	k1gFnPc4
<g/>
,	,	kIx,
úrazy	úraz	k1gInPc4
<g/>
,	,	kIx,
záněty	zánět	k1gInPc4
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
těžká	těžký	k2eAgFnSc1d1
žloutenka	žloutenka	k1gFnSc1
<g/>
,	,	kIx,
špatná	špatný	k2eAgFnSc1d1
výživa	výživa	k1gFnSc1
</s>
<s>
konsagvinita	konsagvinita	k1gFnSc1
–	–	k?
blízké	blízký	k2eAgNnSc4d1
příbuzenství	příbuzenství	k1gNnSc4
mezi	mezi	k7c7
rodiči	rodič	k1gMnPc7
</s>
<s>
Svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
zde	zde	k6eAd1
hraje	hrát	k5eAaImIp3nS
i	i	k9
dědičnost	dědičnost	k1gFnSc4
a	a	k8xC
specifické	specifický	k2eAgFnPc4d1
genetické	genetický	k2eAgFnPc4d1
příčiny	příčina	k1gFnPc4
jako	jako	k8xC,k8xS
například	například	k6eAd1
chromozomální	chromozomální	k2eAgFnSc1d1
aberace	aberace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
způsobuje	způsobovat	k5eAaImIp3nS
Downův	Downův	k2eAgInSc4d1
syndrom	syndrom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kategorie	kategorie	k1gFnPc1
mentální	mentální	k2eAgFnSc2d1
retardace	retardace	k1gFnSc2
</s>
<s>
Lehká	lehký	k2eAgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
</s>
<s>
Synonyma	synonymum	k1gNnPc1
<g/>
:	:	kIx,
lehká	lehký	k2eAgFnSc1d1
slabomyslnost	slabomyslnost	k1gFnSc1
<g/>
,	,	kIx,
lehká	lehký	k2eAgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
subnormalita	subnormalita	k1gFnSc1
<g/>
,	,	kIx,
oligofrenie	oligofrenie	k1gFnSc1
<g/>
,	,	kIx,
debilita	debilita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
IQ	iq	kA
50	#num#	k4
<g/>
–	–	k?
<g/>
69	#num#	k4
</s>
<s>
zastoupení	zastoupení	k1gNnSc1
v	v	k7c6
populaci	populace	k1gFnSc6
2,6	2,6	k4
%	%	kIx~
</s>
<s>
výskyt	výskyt	k1gInSc1
v	v	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
jedinců	jedinec	k1gMnPc2
s	s	k7c7
mentální	mentální	k2eAgFnSc7d1
retardací	retardace	k1gFnSc7
80	#num#	k4
%	%	kIx~
</s>
<s>
Jedinci	jedinec	k1gMnPc1
jsou	být	k5eAaImIp3nP
většinou	většinou	k6eAd1
schopni	schopen	k2eAgMnPc1d1
užívat	užívat	k5eAaImF
řeč	řeč	k1gFnSc4
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
,	,	kIx,
dosáhnout	dosáhnout	k5eAaPmF
nezávislosti	nezávislost	k1gFnSc3
v	v	k7c6
osobní	osobní	k2eAgFnSc6d1
péči	péče	k1gFnSc6
(	(	kIx(
<g/>
jídlo	jídlo	k1gNnSc1
<g/>
,	,	kIx,
hygiena	hygiena	k1gFnSc1
<g/>
,	,	kIx,
oblékání	oblékání	k1gNnSc1
<g/>
…	…	k?
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
praktickém	praktický	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
emocionální	emocionální	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
afektivní	afektivní	k2eAgFnSc1d1
labilita	labilita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	K	kA
lehké	lehký	k2eAgFnSc6d1
mentální	mentální	k2eAgFnSc6d1
retardaci	retardace	k1gFnSc6
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
individuálně	individuálně	k6eAd1
přidružit	přidružit	k5eAaPmF
vývojové	vývojový	k2eAgFnPc4d1
poruchy	porucha	k1gFnPc4
<g/>
,	,	kIx,
autismus	autismus	k1gInSc4
<g/>
,	,	kIx,
tělesné	tělesný	k2eAgNnSc4d1
postižení	postižení	k1gNnSc4
<g/>
,	,	kIx,
epilepsie	epilepsie	k1gFnPc4
<g/>
,	,	kIx,
poruchy	porucha	k1gFnPc4
chování	chování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
vliv	vliv	k1gInSc1
má	mít	k5eAaImIp3nS
dědičnost	dědičnost	k1gFnSc4
i	i	k9
sociokulturně	sociokulturně	k6eAd1
znevýhodněné	znevýhodněný	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
<g/>
:	:	kIx,
do	do	k7c2
3	#num#	k4
let	léto	k1gNnPc2
je	být	k5eAaImIp3nS
dítě	dítě	k1gNnSc1
jen	jen	k9
lehce	lehko	k6eAd1
psychomotoricky	psychomotoricky	k6eAd1
opožděno	opožděn	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
mezi	mezi	k7c7
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
6	#num#	k4
<g/>
.	.	kIx.
rokem	rok	k1gInSc7
se	se	k3xPyFc4
už	už	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
opožděný	opožděný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
řeči	řeč	k1gFnSc2
<g/>
,	,	kIx,
malá	malý	k2eAgFnSc1d1
slovní	slovní	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
<g/>
,	,	kIx,
nízká	nízký	k2eAgFnSc1d1
zvídavost	zvídavost	k1gFnSc1
a	a	k8xC
vynalézavost	vynalézavost	k1gFnSc1
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
se	se	k3xPyFc4
pak	pak	k6eAd1
tato	tento	k3xDgFnSc1
retardace	retardace	k1gFnSc1
projevuje	projevovat	k5eAaImIp3nS
ve	v	k7c6
věku	věk	k1gInSc6
školní	školní	k2eAgFnSc2d1
docházky	docházka	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
omezené	omezený	k2eAgNnSc1d1
logické	logický	k2eAgNnSc1d1
<g/>
,	,	kIx,
abstraktní	abstraktní	k2eAgNnSc4d1
a	a	k8xC
mechanické	mechanický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
<g/>
,	,	kIx,
lehce	lehko	k6eAd1
opožděná	opožděný	k2eAgFnSc1d1
jemná	jemný	k2eAgFnSc1d1
i	i	k8xC
hrubá	hrubá	k1gFnSc1
motorika	motorik	k1gMnSc2
<g/>
,	,	kIx,
slabší	slabý	k2eAgFnSc1d2
paměť	paměť	k1gFnSc1
atd.	atd.	kA
Postižení	postižený	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
vzdělávají	vzdělávat	k5eAaImIp3nP
většinou	většina	k1gFnSc7
ve	v	k7c6
školách	škola	k1gFnPc6
praktických	praktický	k2eAgFnPc6d1
podle	podle	k7c2
odpovídajícího	odpovídající	k2eAgInSc2d1
vzdělávacího	vzdělávací	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
splnění	splnění	k1gNnSc6
stanovených	stanovený	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
je	být	k5eAaImIp3nS
však	však	k9
možná	možná	k9
i	i	k9
integrace	integrace	k1gFnSc1
do	do	k7c2
běžné	běžný	k2eAgFnSc2d1
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vzdělávání	vzdělávání	k1gNnSc6
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
rozvíjet	rozvíjet	k5eAaImF
jejich	jejich	k3xOp3gFnPc4
dovednosti	dovednost	k1gFnPc4
a	a	k8xC
kompenzovat	kompenzovat	k5eAaBmF
nedostatky	nedostatek	k1gInPc4
<g/>
,	,	kIx,
většinu	většina	k1gFnSc4
jedinců	jedinec	k1gMnPc2
(	(	kIx(
<g/>
z	z	k7c2
horní	horní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
LMR	LMR	kA
<g/>
)	)	kIx)
lze	lze	k6eAd1
poté	poté	k6eAd1
zaměstnat	zaměstnat	k5eAaPmF
v	v	k7c6
praktických	praktický	k2eAgFnPc6d1
profesích	profes	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Středně	středně	k6eAd1
těžká	těžký	k2eAgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
</s>
<s>
Synonyma	synonymum	k1gNnPc1
<g/>
:	:	kIx,
střední	střední	k2eAgFnSc1d1
slabomyslnost	slabomyslnost	k1gFnSc1
<g/>
,	,	kIx,
středně	středně	k6eAd1
těžká	těžký	k2eAgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
subnormalita	subnormalita	k1gFnSc1
<g/>
,	,	kIx,
těžká	těžký	k2eAgFnSc1d1
oligofrenie	oligofrenie	k1gFnSc1
<g/>
,	,	kIx,
imbecilita	imbecilita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
IQ	iq	kA
35	#num#	k4
<g/>
–	–	k?
<g/>
49	#num#	k4
</s>
<s>
zastoupení	zastoupení	k1gNnSc1
v	v	k7c6
populaci	populace	k1gFnSc6
0,4	0,4	k4
%	%	kIx~
</s>
<s>
výskyt	výskyt	k1gInSc1
v	v	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
jedinců	jedinec	k1gMnPc2
s	s	k7c7
mentální	mentální	k2eAgFnSc7d1
retardací	retardace	k1gFnSc7
12	#num#	k4
%	%	kIx~
</s>
<s>
Jedinci	jedinec	k1gMnPc1
mají	mít	k5eAaImIp3nP
omezenou	omezený	k2eAgFnSc4d1
zručnost	zručnost	k1gFnSc4
a	a	k8xC
schopnost	schopnost	k1gFnSc4
postarat	postarat	k5eAaPmF
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
o	o	k7c4
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
ale	ale	k8xC
bývají	bývat	k5eAaImIp3nP
fyzicky	fyzicky	k6eAd1
aktivní	aktivní	k2eAgMnPc1d1
a	a	k8xC
mobilní	mobilní	k2eAgMnPc1d1
se	se	k3xPyFc4
schopností	schopnost	k1gFnSc7
komunikovat	komunikovat	k5eAaImF
a	a	k8xC
navazovat	navazovat	k5eAaImF
kontakty	kontakt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
vykonávat	vykonávat	k5eAaImF
jednoduchou	jednoduchý	k2eAgFnSc4d1
manuální	manuální	k2eAgFnSc4d1
práci	práce	k1gFnSc4
pod	pod	k7c7
odborným	odborný	k2eAgInSc7d1
dohledem	dohled	k1gInSc7
nebo	nebo	k8xC
v	v	k7c6
chráněném	chráněný	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
(	(	kIx(
<g/>
chráněné	chráněný	k2eAgFnSc2d1
dílny	dílna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
touto	tento	k3xDgFnSc7
retardaci	retardace	k1gFnSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
spjatý	spjatý	k2eAgInSc4d1
dětský	dětský	k2eAgInSc4d1
autismus	autismus	k1gInSc4
<g/>
,	,	kIx,
mírná	mírný	k2eAgNnPc1d1
tělesná	tělesný	k2eAgNnPc1d1
postižení	postižení	k1gNnPc1
<g/>
,	,	kIx,
neurologická	neurologický	k2eAgFnSc1d1,k2eNgFnSc1d1
nemoc	nemoc	k1gFnSc1
(	(	kIx(
<g/>
epilepsie	epilepsie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
psychiatrická	psychiatrický	k2eAgFnSc1d1
nemoc	nemoc	k1gFnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
<g/>
:	:	kIx,
výrazně	výrazně	k6eAd1
opožděný	opožděný	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
chápání	chápání	k1gNnSc2
a	a	k8xC
užívání	užívání	k1gNnSc2
řeči	řeč	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
přetrvává	přetrvávat	k5eAaImIp3nS
do	do	k7c2
dospělosti	dospělost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
někteří	některý	k3yIgMnPc1
jedinci	jedinec	k1gMnPc1
si	se	k3xPyFc3
osvojí	osvojit	k5eAaPmIp3nP
triviální	triviální	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
(	(	kIx(
<g/>
čtení	čtení	k1gNnSc4
<g/>
,	,	kIx,
psaní	psaní	k1gNnSc4
a	a	k8xC
počítání	počítání	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
vzdělávají	vzdělávat	k5eAaImIp3nP
se	se	k3xPyFc4
proto	proto	k8xC
podle	podle	k7c2
odpovídajícího	odpovídající	k2eAgInSc2d1
vzdělávacího	vzdělávací	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
v	v	k7c6
základních	základní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
speciálních	speciální	k2eAgFnPc6d1
<g/>
,	,	kIx,
další	další	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
ve	v	k7c6
škole	škola	k1gFnSc6
praktické	praktický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nutný	nutný	k2eAgInSc4d1
individuální	individuální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
senzomotorických	senzomotorický	k2eAgFnPc2d1
dovedností	dovednost	k1gFnPc2
a	a	k8xC
verbálních	verbální	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
</s>
<s>
Synonyma	synonymum	k1gNnPc1
<g/>
:	:	kIx,
těžká	těžký	k2eAgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
subnormalita	subnormalita	k1gFnSc1
<g/>
,	,	kIx,
těžká	těžký	k2eAgFnSc1d1
slabomyslnost	slabomyslnost	k1gFnSc1
<g/>
,	,	kIx,
těžká	těžký	k2eAgFnSc1d1
oligofrenie	oligofrenie	k1gFnSc1
<g/>
,	,	kIx,
prostá	prostý	k2eAgFnSc1d1
idiocie	idiocie	k1gFnSc1
<g/>
,	,	kIx,
lehká	lehký	k2eAgFnSc1d1
idiocie	idiocie	k1gFnSc1
<g/>
,	,	kIx,
idioimbecilita	idioimbecilita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
IQ	iq	kA
21	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
</s>
<s>
zastoupení	zastoupení	k1gNnSc1
v	v	k7c6
populaci	populace	k1gFnSc6
0,2	0,2	k4
%	%	kIx~
</s>
<s>
výskyt	výskyt	k1gInSc1
v	v	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
jedinců	jedinec	k1gMnPc2
s	s	k7c7
mentální	mentální	k2eAgFnSc7d1
retardací	retardace	k1gFnSc7
7	#num#	k4
%	%	kIx~
</s>
<s>
Většina	většina	k1gFnSc1
jedinců	jedinec	k1gMnPc2
trpí	trpět	k5eAaImIp3nS
značným	značný	k2eAgInSc7d1
stupněm	stupeň	k1gInSc7
poruchy	porucha	k1gFnSc2
motoriky	motorik	k1gMnPc7
a	a	k8xC
jinými	jiný	k2eAgFnPc7d1
přidruženými	přidružený	k2eAgFnPc7d1
vadami	vada	k1gFnPc7
(	(	kIx(
<g/>
špatný	špatný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
nervového	nervový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeč	řeč	k1gFnSc1
je	být	k5eAaImIp3nS
jednoduchá	jednoduchý	k2eAgFnSc1d1
<g/>
,	,	kIx,
omezena	omezen	k2eAgFnSc1d1
na	na	k7c4
jednotlivá	jednotlivý	k2eAgNnPc4d1
slova	slovo	k1gNnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
nemusí	muset	k5eNaImIp3nS
vytvořit	vytvořit	k5eAaPmF
vůbec	vůbec	k9
–	–	k?
vzdělatelnost	vzdělatelnost	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
omezená	omezený	k2eAgFnSc1d1
<g/>
,	,	kIx,
probíhá	probíhat	k5eAaImIp3nS
podle	podle	k7c2
odpovídajícího	odpovídající	k2eAgInSc2d1
vzdělávacího	vzdělávací	k2eAgInSc2d1
programu	program	k1gInSc2
ve	v	k7c6
speciální	speciální	k2eAgFnSc6d1
základní	základní	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
včasná	včasný	k2eAgFnSc1d1
systematická	systematický	k2eAgFnSc1d1
a	a	k8xC
kvalifikovaná	kvalifikovaný	k2eAgFnSc1d1
rehabilitační	rehabilitační	k2eAgFnSc1d1
a	a	k8xC
vzdělávací	vzdělávací	k2eAgFnSc1d1
péče	péče	k1gFnSc1
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
motoriky	motorik	k1gMnPc4
<g/>
,	,	kIx,
komunikativnosti	komunikativnost	k1gFnSc2
<g/>
,	,	kIx,
rozumových	rozumový	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
a	a	k8xC
soběstačnosti	soběstačnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
touto	tento	k3xDgFnSc7
retardací	retardace	k1gFnSc7
už	už	k6eAd1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
spojené	spojený	k2eAgFnPc4d1
tělesné	tělesný	k2eAgFnPc4d1
vady	vada	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Hluboká	hluboký	k2eAgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
</s>
<s>
Synonyma	synonymum	k1gNnPc1
<g/>
:	:	kIx,
těžká	těžký	k2eAgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
subnormalita	subnormalita	k1gFnSc1
<g/>
,	,	kIx,
hluboká	hluboký	k2eAgFnSc1d1
slabomyslnost	slabomyslnost	k1gFnSc1
<g/>
,	,	kIx,
hluboká	hluboký	k2eAgFnSc1d1
oligofrenie	oligofrenie	k1gFnSc1
<g/>
,	,	kIx,
těžká	těžký	k2eAgFnSc1d1
idiocie	idiocie	k1gFnSc1
<g/>
,	,	kIx,
idiocie	idiocie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
IQ	iq	kA
pod	pod	k7c7
20	#num#	k4
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
nelze	lze	k6eNd1
změřit	změřit	k5eAaPmF
<g/>
,	,	kIx,
hodnota	hodnota	k1gFnSc1
je	být	k5eAaImIp3nS
jen	jen	k9
odhadovaná	odhadovaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
zastoupení	zastoupení	k1gNnSc1
v	v	k7c6
populaci	populace	k1gFnSc6
0,03	0,03	k4
%	%	kIx~
</s>
<s>
výskyt	výskyt	k1gInSc1
v	v	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
jedinců	jedinec	k1gMnPc2
s	s	k7c7
mentální	mentální	k2eAgFnSc7d1
retardací	retardace	k1gFnSc7
1	#num#	k4
%	%	kIx~
</s>
<s>
Většina	většina	k1gFnSc1
osob	osoba	k1gFnPc2
vykazuje	vykazovat	k5eAaImIp3nS
výrazné	výrazný	k2eAgInPc4d1
neurologické	urologický	k2eNgInPc4d1,k2eAgInPc4d1
a	a	k8xC
jiné	jiný	k2eAgInPc4d1
tělesné	tělesný	k2eAgInPc4d1
nedostatky	nedostatek	k1gInPc4
postihující	postihující	k2eAgFnSc1d1
hybnost	hybnost	k1gFnSc1
<g/>
:	:	kIx,
bývají	bývat	k5eAaImIp3nP
imobilní	imobilní	k2eAgFnPc1d1
nebo	nebo	k8xC
velmi	velmi	k6eAd1
omezení	omezení	k1gNnSc1
v	v	k7c6
pohybu	pohyb	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vykonávají	vykonávat	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
automatické	automatický	k2eAgInPc1d1
stereotypní	stereotypní	k2eAgInPc1d1
pohyby	pohyb	k1gInPc1
(	(	kIx(
<g/>
kývání	kývání	k1gNnSc1
těla	tělo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemají	mít	k5eNaImIp3nP
schopnost	schopnost	k1gFnSc4
se	se	k3xPyFc4
o	o	k7c4
sebe	sebe	k3xPyFc4
postarat	postarat	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
potřebují	potřebovat	k5eAaImIp3nP
stálou	stálý	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
častá	častý	k2eAgNnPc4d1
poškození	poškození	k1gNnSc4
zraku	zrak	k1gInSc2
a	a	k8xC
sluchu	sluch	k1gInSc2
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
jen	jen	k9
nejjednodušších	jednoduchý	k2eAgFnPc2d3
zrakově	zrakově	k6eAd1
prostorových	prostorový	k2eAgFnPc2d1
orientačních	orientační	k2eAgFnPc2d1
dovedností	dovednost	k1gFnPc2
(	(	kIx(
<g/>
nepoznávají	poznávat	k5eNaImIp3nP
okolí	okolí	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
pouze	pouze	k6eAd1
primitivního	primitivní	k2eAgNnSc2d1
neverbálního	verbální	k2eNgNnSc2d1
dorozumívání	dorozumívání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinci	jedinec	k1gMnPc7
obvykle	obvykle	k6eAd1
nedokáží	dokázat	k5eNaPmIp3nP
udržet	udržet	k5eAaPmF
moč	moč	k1gFnSc4
ani	ani	k8xC
stolici	stolice	k1gFnSc4
<g/>
,	,	kIx,
častá	častý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
epilepsie	epilepsie	k1gFnPc4
a	a	k8xC
sklony	sklon	k1gInPc4
k	k	k7c3
sebepoškozování	sebepoškozování	k1gNnSc3
(	(	kIx(
<g/>
mají	mít	k5eAaImIp3nP
snížený	snížený	k2eAgInSc4d1
práh	práh	k1gInSc4
citlivosti	citlivost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nespecifikovaná	specifikovaný	k2eNgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
</s>
<s>
Synonyma	synonymum	k1gNnPc1
<g/>
:	:	kIx,
Mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
NS	NS	kA
<g/>
,	,	kIx,
mentální	mentální	k2eAgFnSc1d1
subnormalita	subnormalita	k1gFnSc1
NS	NS	kA
<g/>
,	,	kIx,
oligofrenie	oligofrenie	k1gFnSc2
NS	NS	kA
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
této	tento	k3xDgFnSc2
kategorie	kategorie	k1gFnSc2
patří	patřit	k5eAaImIp3nP
jedinci	jedinec	k1gMnPc1
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yIgFnPc2,k3yRgFnPc2,k3yQgFnPc2
je	být	k5eAaImIp3nS
sice	sice	k8xC
mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
prokazatelná	prokazatelný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
nedostatek	nedostatek	k1gInSc4
informací	informace	k1gFnPc2
nelze	lze	k6eNd1
jedince	jedinko	k6eAd1
zařadit	zařadit	k5eAaPmF
do	do	k7c2
některé	některý	k3yIgFnSc2
u	u	k7c2
výše	výše	k1gFnSc1
uvedených	uvedený	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
</s>
<s>
Do	do	k7c2
této	tento	k3xDgFnSc2
kategorie	kategorie	k1gFnSc2
patří	patřit	k5eAaImIp3nP
jedinci	jedinec	k1gMnPc1
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgMnPc2
pro	pro	k7c4
hodnocení	hodnocení	k1gNnSc4
intelektové	intelektový	k2eAgFnSc2d1
retardace	retardace	k1gFnSc2
nelze	lze	k6eNd1
(	(	kIx(
<g/>
nebo	nebo	k8xC
jen	jen	k9
obtížně	obtížně	k6eAd1
<g/>
)	)	kIx)
použít	použít	k5eAaPmF
obvyklé	obvyklý	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
například	například	k6eAd1
o	o	k7c4
jedince	jedinec	k1gMnPc4
slepé	slepý	k2eAgMnPc4d1
<g/>
,	,	kIx,
hluché	hluchý	k2eAgFnPc1d1
<g/>
,	,	kIx,
němé	němý	k2eAgFnPc1d1
<g/>
,	,	kIx,
hluchoněmé	hluchoněmý	k2eAgFnPc1d1
či	či	k8xC
hluchoslepé	hluchoslepý	k2eAgFnPc1d1
<g/>
,	,	kIx,
jedince	jedinko	k6eAd1
s	s	k7c7
těžkými	těžký	k2eAgFnPc7d1
poruchami	porucha	k1gFnPc7
chování	chování	k1gNnSc2
<g/>
,	,	kIx,
autisty	autista	k1gMnSc2
atd.	atd.	kA
</s>
<s>
Vzdělávání	vzdělávání	k1gNnSc1
lidí	člověk	k1gMnPc2
s	s	k7c7
mentální	mentální	k2eAgFnSc7d1
retardací	retardace	k1gFnSc7
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Vzdělávání	vzdělávání	k1gNnSc4
osob	osoba	k1gFnPc2
se	s	k7c7
speciálními	speciální	k2eAgFnPc7d1
výchovnými	výchovný	k2eAgFnPc7d1
potřebami	potřeba	k1gFnPc7
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Výchova	výchova	k1gFnSc1
a	a	k8xC
vzdělávání	vzdělávání	k1gNnSc1
lidí	člověk	k1gMnPc2
s	s	k7c7
mentální	mentální	k2eAgFnSc7d1
retardací	retardace	k1gFnSc7
jsou	být	k5eAaImIp3nP
celoživotním	celoživotní	k2eAgInSc7d1
procesem	proces	k1gInSc7
<g/>
,	,	kIx,
pro	pro	k7c4
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
je	být	k5eAaImIp3nS
charakteristická	charakteristický	k2eAgFnSc1d1
nutnost	nutnost	k1gFnSc1
stálého	stálý	k2eAgNnSc2d1
opakování	opakování	k1gNnSc2
<g/>
,	,	kIx,
prohlubování	prohlubování	k1gNnSc2
znalostí	znalost	k1gFnPc2
a	a	k8xC
dovedností	dovednost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc1
se	se	k3xPyFc4
ovšem	ovšem	k9
dostavují	dostavovat	k5eAaImIp3nP
velmi	velmi	k6eAd1
zvolna	zvolna	k6eAd1
a	a	k8xC
pokrok	pokrok	k1gInSc4
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
nepozorovatelný	pozorovatelný	k2eNgInSc1d1
<g/>
,	,	kIx,
základ	základ	k1gInSc1
výchovy	výchova	k1gFnSc2
tedy	tedy	k8xC
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
rodině	rodina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdělávání	vzdělávání	k1gNnSc1
mentálně	mentálně	k6eAd1
retardovaných	retardovaný	k2eAgMnPc2d1
zajišťují	zajišťovat	k5eAaImIp3nP
speciální	speciální	k2eAgFnPc4d1
školy	škola	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
praktické	praktický	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
speciální	speciální	k2eAgNnPc1d1
učiliště	učiliště	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
připravují	připravovat	k5eAaImIp3nP
mentálně	mentálně	k6eAd1
postižené	postižený	k2eAgFnPc1d1
po	po	k7c6
stránce	stránka	k1gFnSc6
kvalifikace	kvalifikace	k1gFnSc2
a	a	k8xC
přípravy	příprava	k1gFnSc2
k	k	k7c3
povolání	povolání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odlišný	odlišný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
vzdělávání	vzdělávání	k1gNnSc2
a	a	k8xC
výchovy	výchova	k1gFnSc2
poskytují	poskytovat	k5eAaImIp3nP
některé	některý	k3yIgFnPc4
sociální	sociální	k2eAgFnPc4d1
instituce	instituce	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
denní	denní	k2eAgInPc1d1
stacionáře	stacionář	k1gInPc1
či	či	k8xC
domovy	domov	k1gInPc1
pro	pro	k7c4
osoby	osoba	k1gFnPc4
se	s	k7c7
zdravotním	zdravotní	k2eAgNnSc7d1
postižením	postižení	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
zaměřují	zaměřovat	k5eAaImIp3nP
na	na	k7c4
soběstačnost	soběstačnost	k1gFnSc4
postižených	postižený	k2eAgMnPc2d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Netradiční	tradiční	k2eNgFnPc1d1
formy	forma	k1gFnPc1
vzdělávání	vzdělávání	k1gNnSc2
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
usnadnění	usnadnění	k1gNnSc4
orientace	orientace	k1gFnSc2
v	v	k7c6
sociálním	sociální	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
a	a	k8xC
k	k	k7c3
dorozumívání	dorozumívání	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
speciálních	speciální	k2eAgFnPc6d1
a	a	k8xC
praktických	praktický	k2eAgFnPc6d1
školách	škola	k1gFnPc6
by	by	kYmCp3nS
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
spíše	spíše	k9
podpůrné	podpůrný	k2eAgInPc4d1
<g/>
,	,	kIx,
pomocné	pomocný	k2eAgInPc4d1
nebo	nebo	k8xC
motivační	motivační	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
dobře	dobře	k6eAd1
využitelné	využitelný	k2eAgInPc1d1
při	při	k7c6
individuálním	individuální	k2eAgNnSc6d1
vzdělávání	vzdělávání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Sociální	sociální	k2eAgNnSc1d1
čtení	čtení	k1gNnSc1
<g/>
:	:	kIx,
zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
zraková	zrakový	k2eAgNnPc4d1
znamení	znamení	k1gNnPc4
a	a	k8xC
symboly	symbol	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
nutné	nutný	k2eAgInPc1d1
pro	pro	k7c4
orientaci	orientace	k1gFnSc4
v	v	k7c6
okolním	okolní	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
lze	lze	k6eAd1
je	být	k5eAaImIp3nS
bezprostředně	bezprostředně	k6eAd1
využít	využít	k5eAaPmF
v	v	k7c6
praxi	praxe	k1gFnSc6
(	(	kIx(
<g/>
poznání	poznání	k1gNnSc1
<g/>
,	,	kIx,
interpretace	interpretace	k1gFnSc1
a	a	k8xC
reakce	reakce	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
vyvoláván	vyvolávat	k5eAaImNgInS
i	i	k9
motivační	motivační	k2eAgInSc1d1
efekt	efekt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
je	být	k5eAaImIp3nS
srozumitelnost	srozumitelnost	k1gFnSc1
pro	pro	k7c4
ostatní	ostatní	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kategorie	kategorie	k1gFnSc1
1	#num#	k4
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
soubory	soubor	k1gInPc4
obrázků	obrázek	k1gInPc2
<g/>
,	,	kIx,
kategorie	kategorie	k1gFnSc1
2	#num#	k4
na	na	k7c4
značky	značka	k1gFnPc4
a	a	k8xC
piktogramy	piktogram	k1gInPc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
dopravních	dopravní	k2eAgFnPc2d1
značek	značka	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
kategorie	kategorie	k1gFnSc1
3	#num#	k4
na	na	k7c4
slova	slovo	k1gNnPc4
a	a	k8xC
skupiny	skupina	k1gFnPc4
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vhodné	vhodný	k2eAgNnSc4d1
téma	téma	k1gNnSc4
pro	pro	k7c4
sociální	sociální	k2eAgNnSc4d1
čtení	čtení	k1gNnSc4
je	být	k5eAaImIp3nS
„	„	k?
<g/>
časová	časový	k2eAgFnSc1d1
orientace	orientace	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
opakuje	opakovat	k5eAaImIp3nS
se	se	k3xPyFc4
každý	každý	k3xTgInSc1
den	den	k1gInSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
hodně	hodně	k6eAd1
obrázků	obrázek	k1gInPc2
a	a	k8xC
piktogramů	piktogram	k1gInPc2
<g/>
,	,	kIx,
jmen	jméno	k1gNnPc2
<g/>
,	,	kIx,
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
symboly	symbol	k1gInPc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Instrumentální	instrumentální	k2eAgNnSc1d1
obohacování	obohacování	k1gNnSc1
–	–	k?
„	„	k?
<g/>
učíme	učit	k5eAaImIp1nP
se	se	k3xPyFc4
učit	učit	k5eAaImF
<g/>
“	“	k?
(	(	kIx(
<g/>
podle	podle	k7c2
Reuvena	Reuven	k2eAgFnSc1d1
Feuersteina	Feuersteina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Neverbální	verbální	k2eNgFnPc1d1
komunikační	komunikační	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
</s>
<s>
Dorozumívání	dorozumívání	k1gNnSc1
beze	beze	k7c2
slov	slovo	k1gNnPc2
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
těžce	těžce	k6eAd1
postižené	postižený	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nejsou	být	k5eNaImIp3nP
schopny	schopen	k2eAgFnPc1d1
verbálního	verbální	k2eAgNnSc2d1
vyjadřování	vyjadřování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladem	klad	k1gInSc7
takové	takový	k3xDgFnSc2
alternativní	alternativní	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
je	být	k5eAaImIp3nS
zvýšení	zvýšení	k1gNnSc1
aktivity	aktivita	k1gFnSc2
dětí	dítě	k1gFnPc2
(	(	kIx(
<g/>
resp.	resp.	kA
snížení	snížení	k1gNnSc4
tendence	tendence	k1gFnSc2
k	k	k7c3
pasivitě	pasivita	k1gFnSc3
<g/>
)	)	kIx)
a	a	k8xC
rozšíření	rozšíření	k1gNnSc1
možností	možnost	k1gFnPc2
dorozumívání	dorozumívání	k1gNnSc2
i	i	k9
pro	pro	k7c4
jedince	jedinec	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
mají	mít	k5eAaImIp3nP
velké	velký	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
při	při	k7c6
vyjadřování	vyjadřování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záporem	zápor	k1gInSc7
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
nízká	nízký	k2eAgFnSc1d1
společenská	společenský	k2eAgFnSc1d1
využitelnost	využitelnost	k1gFnSc1
<g/>
,	,	kIx,
faktické	faktický	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
těchto	tento	k3xDgMnPc2
jedinců	jedinec	k1gMnPc2
od	od	k7c2
většinové	většinový	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
tyto	tento	k3xDgInPc4
způsoby	způsob	k1gInPc4
neovládá	ovládat	k5eNaImIp3nS
<g/>
)	)	kIx)
a	a	k8xC
určitá	určitý	k2eAgFnSc1d1
pravděpodobnost	pravděpodobnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
dítě	dítě	k1gNnSc1
ztratí	ztratit	k5eAaPmIp3nS
potřebu	potřeba	k1gFnSc4
i	i	k9
jen	jen	k9
pokusit	pokusit	k5eAaPmF
se	se	k3xPyFc4
mluvit	mluvit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
komunikační	komunikační	k2eAgInSc1d1
systém	systém	k1gInSc1
BLISS	BLISS	kA
<g/>
:	:	kIx,
statický	statický	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
jsou	být	k5eAaImIp3nP
symboly	symbol	k1gInPc1
překládány	překládán	k2eAgInPc1d1
v	v	k7c6
určité	určitý	k2eAgFnSc6d1
dvojrozměrné	dvojrozměrný	k2eAgFnSc6d1
nebo	nebo	k8xC
trojrozměrné	trojrozměrný	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
komunikace	komunikace	k1gFnSc2
nemění	měnit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Symboly	symbol	k1gInPc7
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
založeny	založit	k5eAaPmNgInP
na	na	k7c6
stálém	stálý	k2eAgInSc6d1
zjevu	zjev	k1gInSc6
a	a	k8xC
významu	význam	k1gInSc6
<g/>
,	,	kIx,
nikoli	nikoli	k9
na	na	k7c6
zvucích	zvuk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokážou	dokázat	k5eAaPmIp3nP
mu	on	k3xPp3gMnSc3
porozumět	porozumět	k5eAaPmF
i	i	k9
ti	ten	k3xDgMnPc1
jedinci	jedinec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nedokáží	dokázat	k5eNaPmIp3nP
číst	číst	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
znaková	znakový	k2eAgFnSc1d1
řeč	řeč	k1gFnSc1
Makaton	Makaton	k1gInSc1
<g/>
:	:	kIx,
dynamický	dynamický	k2eAgInSc1d1
systém	systém	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
znakovém	znakový	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
neslyšících	slyšící	k2eNgMnPc2d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jednotlivá	jednotlivý	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
se	se	k3xPyFc4
vyjadřují	vyjadřovat	k5eAaImIp3nP
pohybem	pohyb	k1gInSc7
ruky	ruka	k1gFnSc2
<g/>
,	,	kIx,
hlavy	hlava	k1gFnSc2
apod.	apod.	kA
Znaky	znak	k1gInPc1
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
doprovázeny	doprovázet	k5eAaImNgFnP
normální	normální	k2eAgFnSc7d1
gramatickou	gramatický	k2eAgFnSc7d1
řečí	řeč	k1gFnSc7
učitele	učitel	k1gMnSc2
<g/>
,	,	kIx,
užívá	užívat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
mimický	mimický	k2eAgInSc1d1
výraz	výraz	k1gInSc1
obličeje	obličej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
snoezelen	snoezelen	k2eAgInSc4d1
<g/>
:	:	kIx,
multisenzoriální	multisenzoriální	k2eAgInSc4d1
<g/>
,	,	kIx,
pozitivně	pozitivně	k6eAd1
naladěné	naladěný	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
ABZ	ABZ	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Sborník	sborník	k1gInSc1
Hradeckých	Hradeckých	k2eAgInPc2d1
dnů	den	k1gInPc2
sociální	sociální	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Online	Onlin	k1gInSc5
revue	revue	k1gFnSc2
pro	pro	k7c4
speciální	speciální	k2eAgMnPc4d1
pedagogy	pedagog	k1gMnPc4
www.specialni-pedagogika.cz	www.specialni-pedagogika.cza	k1gFnPc2
<g/>
.	.	kIx.
www.specialni-pedagogika.cz	www.specialni-pedagogika.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Švarcová	Švarcová	k1gFnSc1
<g/>
,	,	kIx,
Iva	Iva	k1gFnSc1
<g/>
:	:	kIx,
Mentální	mentální	k2eAgFnSc1d1
retardace	retardace	k1gFnSc1
<g/>
:	:	kIx,
vzdělávání	vzdělávání	k1gNnSc1
<g/>
,	,	kIx,
výchova	výchova	k1gFnSc1
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc1d1
péče	péče	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tomická	Tomická	k1gFnSc1
<g/>
,	,	kIx,
Václava	Václava	k1gFnSc1
<g/>
:	:	kIx,
Vybrané	vybraný	k2eAgFnPc1d1
kapitoly	kapitola	k1gFnPc1
k	k	k7c3
integraci	integrace	k1gFnSc3
ve	v	k7c6
školství	školství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
TUL	Tula	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Středa	středa	k1gFnSc1
<g/>
,	,	kIx,
Leoš	Leoš	k1gMnSc1
<g/>
,	,	kIx,
Marádová	Marádový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
Zima	zima	k1gFnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
:	:	kIx,
Vybrané	vybraný	k2eAgFnPc1d1
kapitoly	kapitola	k1gFnPc1
o	o	k7c4
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
UK	UK	kA
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
|	|	kIx~
Psychologie	psychologie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
122758	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4019852-2	4019852-2	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12898	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85083658	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85083658	#num#	k4
</s>
