<s desamb="1">
Retardaci	retardace	k1gFnSc6
tedy	tedy	k8xC
nelze	lze	k6eNd1
léčit	léčit	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
nejde	jít	k5eNaImIp3nS
o	o	k7c4
nemoc	nemoc	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
trvalý	trvalý	k2eAgInSc1d1
fyziologický	fyziologický	k2eAgInSc1d1
stav	stav	k1gInSc1
(	(	kIx(
<g/>
zaostalý	zaostalý	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
rozumových	rozumový	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
<g/>
,	,	kIx,
odlišný	odlišný	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
některých	některý	k3yIgFnPc2
psychických	psychický	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
poruchy	porucha	k1gFnPc1
ve	v	k7c6
schopnosti	schopnost	k1gFnSc6
adaptace	adaptace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>