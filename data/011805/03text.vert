<p>
<s>
Prázdninová	prázdninový	k2eAgFnSc1d1	prázdninová
škola	škola	k1gFnSc1	škola
Lipnice	Lipnice	k1gFnSc2	Lipnice
je	být	k5eAaImIp3nS	být
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
pořádající	pořádající	k2eAgInPc1d1	pořádající
kurzy	kurz	k1gInPc1	kurz
rozvoje	rozvoj	k1gInSc2	rozvoj
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
užívající	užívající	k2eAgMnPc1d1	užívající
metod	metoda	k1gFnPc2	metoda
zážitkové	zážitkový	k2eAgFnSc2d1	zážitková
pedagogiky	pedagogika	k1gFnSc2	pedagogika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
občanské	občanský	k2eAgNnSc4d1	občanské
sdružení	sdružení	k1gNnSc4	sdružení
se	se	k3xPyFc4	se
ustavila	ustavit	k5eAaPmAgFnS	ustavit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
PŠL	PŠL	kA	PŠL
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
Outward	Outward	k1gMnSc1	Outward
Bound	Bound	k1gMnSc1	Bound
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
založila	založit	k5eAaPmAgFnS	založit
společnost	společnost	k1gFnSc1	společnost
Outward	Outward	k1gMnSc1	Outward
Bound	Bound	k1gMnSc1	Bound
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
cesta	cesta	k1gFnSc1	cesta
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
nabízející	nabízející	k2eAgInPc1d1	nabízející
zážitkové	zážitkový	k2eAgInPc1d1	zážitkový
kurzy	kurz	k1gInPc1	kurz
na	na	k7c6	na
komerčním	komerční	k2eAgInSc6d1	komerční
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
jako	jako	k8xC	jako
služby	služba	k1gFnSc2	služba
tzv.	tzv.	kA	tzv.
teambuildingu	teambuilding	k1gInSc2	teambuilding
pro	pro	k7c4	pro
firemní	firemní	k2eAgMnPc4d1	firemní
zákazníky	zákazník	k1gMnPc4	zákazník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PŠL	PŠL	kA	PŠL
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zážitkové	zážitkový	k2eAgFnSc2d1	zážitková
pedagogiky	pedagogika	k1gFnSc2	pedagogika
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
dosti	dosti	k6eAd1	dosti
průkopnické	průkopnický	k2eAgFnSc2d1	průkopnická
postavení	postavení	k1gNnSc3	postavení
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
metodika	metodika	k1gFnSc1	metodika
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgInPc2d1	další
podobných	podobný	k2eAgInPc2d1	podobný
kurzů	kurz	k1gInPc2	kurz
a	a	k8xC	a
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
práci	práce	k1gFnSc4	práce
dalších	další	k2eAgFnPc2d1	další
výchovných	výchovný	k2eAgFnPc2d1	výchovná
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Prvky	prvek	k1gInPc1	prvek
zážitkové	zážitkový	k2eAgFnSc2d1	zážitková
pedagogiky	pedagogika	k1gFnSc2	pedagogika
ovšem	ovšem	k9	ovšem
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
některých	některý	k3yIgFnPc2	některý
výchovných	výchovný	k2eAgFnPc2d1	výchovná
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
skautingu	skauting	k1gInSc2	skauting
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
zastoupené	zastoupený	k2eAgInPc1d1	zastoupený
již	již	k9	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
PŠL	PŠL	kA	PŠL
nabízí	nabízet	k5eAaImIp3nS	nabízet
několik	několik	k4yIc4	několik
stálých	stálý	k2eAgInPc2d1	stálý
projektů	projekt	k1gInPc2	projekt
(	(	kIx(	(
<g/>
Zdrsem	Zdrso	k1gNnSc7	Zdrso
-	-	kIx~	-
první	první	k4xOgFnSc4	první
pomoc	pomoc	k1gFnSc4	pomoc
zážitkem	zážitek	k1gInSc7	zážitek
<g/>
,	,	kIx,	,
Sborovna	sborovna	k1gFnSc1	sborovna
<g/>
,	,	kIx,	,
Gymnasion	gymnasion	k1gNnSc1	gymnasion
-	-	kIx~	-
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
zážitkovou	zážitkový	k2eAgFnSc4d1	zážitková
pedagogiku	pedagogika	k1gFnSc4	pedagogika
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
práce	práce	k1gFnSc2	práce
PŠL	PŠL	kA	PŠL
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
autorské	autorský	k2eAgInPc4d1	autorský
kurzy	kurz	k1gInPc4	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
těchto	tento	k3xDgInPc2	tento
kurzů	kurz	k1gInPc2	kurz
je	být	k5eAaImIp3nS	být
poskytnout	poskytnout	k5eAaPmF	poskytnout
účastníkům	účastník	k1gMnPc3	účastník
silný	silný	k2eAgInSc4d1	silný
impuls	impuls	k1gInSc4	impuls
vedoucí	vedoucí	k1gFnSc2	vedoucí
především	především	k9	především
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
a	a	k8xC	a
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
jak	jak	k6eAd1	jak
o	o	k7c4	o
kurzy	kurz	k1gInPc4	kurz
letní	letní	k2eAgInPc4d1	letní
(	(	kIx(	(
<g/>
Karibu	karibu	k1gMnPc4	karibu
<g/>
,	,	kIx,	,
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
divočiny	divočina	k1gFnSc2	divočina
<g/>
,	,	kIx,	,
Deja-vu	Deja	k1gInSc2	Deja-v
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
zimní	zimní	k2eAgInPc4d1	zimní
expediční	expediční	k2eAgInPc4d1	expediční
a	a	k8xC	a
pobytové	pobytový	k2eAgInPc4d1	pobytový
kurzy	kurz	k1gInPc4	kurz
(	(	kIx(	(
<g/>
Wintertouch	Wintertouch	k1gMnSc1	Wintertouch
<g/>
,	,	kIx,	,
Zafúkané	Zafúkaný	k2eAgFnSc3d1	Zafúkaný
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
gotickej	gotickej	k?	gotickej
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovějším	nový	k2eAgInSc7d3	nejnovější
trendem	trend	k1gInSc7	trend
je	být	k5eAaImIp3nS	být
podpora	podpora	k1gFnSc1	podpora
změny	změna	k1gFnSc2	změna
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
metod	metoda	k1gFnPc2	metoda
zážitkové	zážitkový	k2eAgFnSc2d1	zážitková
pedagogiky	pedagogika	k1gFnSc2	pedagogika
(	(	kIx(	(
<g/>
iCan	iCan	k1gInSc1	iCan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdrsem	Zdrso	k1gNnSc7	Zdrso
==	==	k?	==
</s>
</p>
<p>
<s>
ZDrSEM	ZDrSEM	k?	ZDrSEM
–	–	k?	–
první	první	k4xOgFnSc2	první
pomoc	pomoc	k1gFnSc4	pomoc
zážitkem	zážitek	k1gInSc7	zážitek
je	být	k5eAaImIp3nS	být
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
světovém	světový	k2eAgInSc6d1	světový
Červeném	červený	k2eAgInSc6d1	červený
kříži	kříž	k1gInSc6	kříž
mají	mít	k5eAaImIp3nP	mít
kurzy	kurz	k1gInPc1	kurz
ZDrSem	ZDrSem	k1gInSc1	ZDrSem
v	v	k7c6	v
ČR	ČR	kA	ČR
nejdelší	dlouhý	k2eAgFnSc6d3	nejdelší
tradici	tradice	k1gFnSc6	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
poslání	poslání	k1gNnSc2	poslání
PŠL	PŠL	kA	PŠL
využívat	využívat	k5eAaImF	využívat
a	a	k8xC	a
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
zážitkovou	zážitkový	k2eAgFnSc4d1	zážitková
pedagogiku	pedagogika	k1gFnSc4	pedagogika
jako	jako	k8xS	jako
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
osobnosti	osobnost	k1gFnSc2	osobnost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
ZDrSem	ZDrSem	k6eAd1	ZDrSem
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
být	být	k5eAaImF	být
metodickým	metodický	k2eAgNnSc7d1	metodické
centrem	centrum	k1gNnSc7	centrum
výuky	výuka	k1gFnSc2	výuka
první	první	k4xOgFnSc2	první
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
kromě	kromě	k7c2	kromě
pořádání	pořádání	k1gNnSc2	pořádání
množství	množství	k1gNnSc2	množství
vlastních	vlastní	k2eAgInPc2d1	vlastní
kurzů	kurz	k1gInPc2	kurz
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
cílové	cílový	k2eAgFnPc4d1	cílová
skupiny	skupina	k1gFnPc4	skupina
od	od	k7c2	od
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
sportovce	sportovec	k1gMnPc4	sportovec
<g/>
,	,	kIx,	,
firmy	firma	k1gFnSc2	firma
až	až	k9	až
k	k	k7c3	k
široké	široký	k2eAgFnSc3d1	široká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
)	)	kIx)	)
pořádá	pořádat	k5eAaImIp3nS	pořádat
i	i	k9	i
akreditovaný	akreditovaný	k2eAgInSc1d1	akreditovaný
výcvik	výcvik	k1gInSc1	výcvik
budoucích	budoucí	k2eAgMnPc2d1	budoucí
lektorů	lektor	k1gMnPc2	lektor
s	s	k7c7	s
názvem	název	k1gInSc7	název
Školitel	školitel	k1gMnSc1	školitel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnPc1	první
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zaměřovat	zaměřovat	k5eAaImF	zaměřovat
na	na	k7c4	na
důkladnější	důkladný	k2eAgFnSc4d2	důkladnější
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
simulovanými	simulovaný	k2eAgFnPc7d1	simulovaná
situacemi	situace	k1gFnPc7	situace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
reflektovány	reflektován	k2eAgMnPc4d1	reflektován
lektory	lektor	k1gMnPc4	lektor
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
účastník	účastník	k1gMnSc1	účastník
odnesl	odnést	k5eAaPmAgMnS	odnést
reálnou	reálný	k2eAgFnSc4d1	reálná
život	život	k1gInSc4	život
zachraňující	zachraňující	k2eAgFnSc4d1	zachraňující
dovednost	dovednost	k1gFnSc4	dovednost
a	a	k8xC	a
zejména	zejména	k9	zejména
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
pro	pro	k7c4	pro
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
první	první	k4xOgFnSc2	první
pomoci	pomoc	k1gFnSc2	pomoc
i	i	k9	i
v	v	k7c6	v
emočně	emočně	k6eAd1	emočně
vypjatých	vypjatý	k2eAgFnPc6d1	vypjatá
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
reálnými	reálný	k2eAgInPc7d1	reálný
prožitky	prožitek	k1gInPc7	prožitek
a	a	k8xC	a
stresem	stres	k1gInSc7	stres
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
první	první	k4xOgFnSc7	první
pomocí	pomoc	k1gFnSc7	pomoc
neoddělitelně	oddělitelně	k6eNd1	oddělitelně
spjatý	spjatý	k2eAgMnSc1d1	spjatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gInSc3	jeho
vzniku	vznik	k1gInSc3	vznik
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
postupně	postupně	k6eAd1	postupně
objevuje	objevovat	k5eAaImIp3nS	objevovat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
uznávají	uznávat	k5eAaImIp3nP	uznávat
výuku	výuka	k1gFnSc4	výuka
zážitkem	zážitek	k1gInSc7	zážitek
a	a	k8xC	a
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
se	s	k7c7	s
ZDrSemem	ZDrSem	k1gInSc7	ZDrSem
inspirují	inspirovat	k5eAaBmIp3nP	inspirovat
(	(	kIx(	(
<g/>
namátkou	namátkou	k6eAd1	namátkou
např.	např.	kA	např.
PrPom	PrPom	k1gInSc1	PrPom
<g/>
,	,	kIx,	,
Rescue	Rescue	k1gInSc1	Rescue
Training	Training	k1gInSc1	Training
<g/>
,	,	kIx,	,
Vitalus	Vitalus	k1gInSc1	Vitalus
či	či	k8xC	či
Jak	jak	k6eAd1	jak
resuscitovat	resuscitovat	k5eAaImF	resuscitovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
není	být	k5eNaImIp3nS	být
ZDrSEM	ZDrSEM	k1gFnSc1	ZDrSEM
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Prázdninové	prázdninový	k2eAgFnSc2d1	prázdninová
školy	škola	k1gFnSc2	škola
Lipnice	Lipnice	k1gFnSc2	Lipnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
subjekt	subjekt	k1gInSc4	subjekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
fond	fond	k1gInSc1	fond
her	hra	k1gFnPc2	hra
I.	I.	kA	I.
Hry	hra	k1gFnSc2	hra
a	a	k8xC	a
programy	program	k1gInPc4	program
připravené	připravený	k2eAgInPc4d1	připravený
pro	pro	k7c4	pro
kurzy	kurz	k1gInPc4	kurz
Prázdninové	prázdninový	k2eAgFnSc2d1	prázdninová
školy	škola	k1gFnSc2	škola
Lipnice	Lipnice	k1gFnSc2	Lipnice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7367-506-6	[number]	k4	978-80-7367-506-6
</s>
</p>
<p>
<s>
Hrkal	hrkat	k5eAaImAgMnS	hrkat
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
<g/>
:	:	kIx,	:
Zlatý	zlatý	k2eAgInSc1d1	zlatý
fond	fond	k1gInSc1	fond
her	hra	k1gFnPc2	hra
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc4	hra
a	a	k8xC	a
programy	program	k1gInPc4	program
připravené	připravený	k2eAgInPc4d1	připravený
pro	pro	k7c4	pro
kurzy	kurz	k1gInPc4	kurz
Prázdninové	prázdninový	k2eAgFnSc2d1	prázdninová
školy	škola	k1gFnSc2	škola
Lipnice	Lipnice	k1gFnSc2	Lipnice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7367-923-1	[number]	k4	978-80-7367-923-1
</s>
</p>
<p>
<s>
Zounková	Zounková	k1gFnSc1	Zounková
<g/>
,	,	kIx,	,
Daniela	Daniela	k1gFnSc1	Daniela
<g/>
:	:	kIx,	:
Zlatý	zlatý	k2eAgInSc1d1	zlatý
fond	fond	k1gInSc1	fond
her	hra	k1gFnPc2	hra
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc4	hra
a	a	k8xC	a
programy	program	k1gInPc4	program
připravené	připravený	k2eAgInPc4d1	připravený
pro	pro	k7c4	pro
kurzy	kurz	k1gInPc4	kurz
Prázdninové	prázdninový	k2eAgFnSc2d1	prázdninová
školy	škola	k1gFnSc2	škola
Lipnice	Lipnice	k1gFnSc2	Lipnice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-262-0394-0	[number]	k4	978-80-262-0394-0
</s>
</p>
<p>
<s>
Hilská	Hilská	k1gFnSc1	Hilská
<g/>
,	,	kIx,	,
Veronika	Veronika	k1gFnSc1	Veronika
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Zlatý	zlatý	k2eAgInSc1d1	zlatý
fond	fond	k1gInSc1	fond
her	hra	k1gFnPc2	hra
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc4	hra
a	a	k8xC	a
programy	program	k1gInPc4	program
připravené	připravený	k2eAgInPc4d1	připravený
pro	pro	k7c4	pro
kurzy	kurz	k1gInPc4	kurz
Prázdninové	prázdninový	k2eAgFnSc2d1	prázdninová
školy	škola	k1gFnSc2	škola
Lipnice	Lipnice	k1gFnSc2	Lipnice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-262-0336-0	[number]	k4	978-80-262-0336-0
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
