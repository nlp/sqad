<p>
<s>
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacapa	k1gFnSc1	Zacapa
Centario	Centario	k6eAd1	Centario
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejoblíbenější	oblíbený	k2eAgInPc4d3	nejoblíbenější
rumy	rum	k1gInPc4	rum
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
rumy	rum	k1gInPc4	rum
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
z	z	k7c2	z
panenského	panenský	k2eAgInSc2d1	panenský
sirupu	sirup	k1gInSc2	sirup
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
z	z	k7c2	z
melasy	melasa	k1gFnSc2	melasa
<g/>
)	)	kIx)	)
a	a	k8xC	a
zraje	zrát	k5eAaImIp3nS	zrát
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
dubu	dub	k1gInSc2	dub
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
Guatemale	Guatemala	k1gFnSc6	Guatemala
způsobem	způsob	k1gInSc7	způsob
blendování	blendování	k1gNnSc2	blendování
sistema	sistemum	k1gNnSc2	sistemum
solera	soler	k1gMnSc2	soler
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
2333	[number]	k4	2333
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
rumů	rum	k1gInPc2	rum
Zacapa	Zacapa	k1gFnSc1	Zacapa
Centario	Centario	k1gNnSc4	Centario
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
destilerie	destilerie	k1gFnSc1	destilerie
založena	založit	k5eAaPmNgFnS	založit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Zacapa	Zacap	k1gMnSc2	Zacap
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Guatemaly	Guatemala	k1gFnSc2	Guatemala
229	[number]	k4	229
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
rumů	rum	k1gInPc2	rum
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Plantáže	plantáž	k1gFnPc1	plantáž
rumů	rum	k1gInPc2	rum
Zacapa	Zacapa	k1gFnSc1	Zacapa
Centario	Centario	k6eAd1	Centario
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Guatemaly	Guatemala	k1gFnSc2	Guatemala
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
350	[number]	k4	350
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Destilerie	Destilerie	k1gFnSc1	Destilerie
se	se	k3xPyFc4	se
chlubí	chlubit	k5eAaImIp3nS	chlubit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnPc1	jejich
plantáže	plantáž	k1gFnPc1	plantáž
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
na	na	k7c6	na
nejlepším	dobrý	k2eAgNnSc6d3	nejlepší
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kombinaci	kombinace	k1gFnSc3	kombinace
mikroklima	mikroklima	k1gNnSc1	mikroklima
<g/>
,	,	kIx,	,
půdy	půda	k1gFnPc1	půda
a	a	k8xC	a
teploty	teplota	k1gFnPc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
výrobců	výrobce	k1gMnPc2	výrobce
rumů	rum	k1gInPc2	rum
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pro	pro	k7c4	pro
destilaci	destilace	k1gFnSc4	destilace
používají	používat	k5eAaImIp3nP	používat
melasu	melasa	k1gFnSc4	melasa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zbytkový	zbytkový	k2eAgInSc4d1	zbytkový
produkt	produkt	k1gInSc4	produkt
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
destilerie	destilerie	k1gFnSc1	destilerie
Zacapa	Zacapa	k1gFnSc1	Zacapa
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
Panenský	panenský	k2eAgInSc4d1	panenský
sirup	sirup	k1gInSc4	sirup
(	(	kIx(	(
<g/>
v	v	k7c4	v
orig	orig	k1gInSc4	orig
<g/>
.	.	kIx.	.
</s>
<s>
Virgin	Virgin	k2eAgInSc1d1	Virgin
sugar	sugar	k1gInSc1	sugar
cane	can	k1gMnSc2	can
honey	honea	k1gMnSc2	honea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
primární	primární	k2eAgInSc1d1	primární
výrobek	výrobek	k1gInSc1	výrobek
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přijde	přijít	k5eAaPmIp3nS	přijít
čas	čas	k1gInSc4	čas
na	na	k7c4	na
kvašení	kvašení	k1gNnSc4	kvašení
<g/>
,	,	kIx,	,
destilerie	destilerie	k1gFnPc4	destilerie
používá	používat	k5eAaImIp3nS	používat
vlastní	vlastní	k2eAgInSc1d1	vlastní
kmen	kmen	k1gInSc1	kmen
kvasinek	kvasinka	k1gFnPc2	kvasinka
získaných	získaný	k2eAgFnPc2d1	získaná
z	z	k7c2	z
ananasu	ananas	k1gInSc2	ananas
<g/>
:	:	kIx,	:
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
cerevisiae	cerevisia	k1gInSc2	cerevisia
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kvasinky	kvasinka	k1gFnPc1	kvasinka
produkují	produkovat	k5eAaImIp3nP	produkovat
specifické	specifický	k2eAgNnSc1d1	specifické
aroma	aroma	k1gNnSc1	aroma
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
rumy	rum	k1gInPc4	rum
Zacapa	Zacap	k1gMnSc2	Zacap
Centario	Centario	k1gNnSc4	Centario
<g/>
.	.	kIx.	.
</s>
<s>
Kvašení	kvašení	k1gNnSc1	kvašení
probíhá	probíhat	k5eAaImIp3nS	probíhat
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
kvasinky	kvasinka	k1gFnPc1	kvasinka
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
čas	čas	k1gInSc4	čas
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
ke	k	k7c3	k
kýženému	kýžený	k2eAgInSc3d1	kýžený
výsledku	výsledek	k1gInSc3	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
posuneme	posunout	k5eAaPmIp1nP	posunout
o	o	k7c4	o
2000	[number]	k4	2000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
do	do	k7c2	do
2333	[number]	k4	2333
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
"	"	kIx"	"
<g/>
The	The	k1gMnSc2	The
House	house	k1gNnSc4	house
Above	Aboev	k1gFnSc2	Aboev
the	the	k?	the
Clouds	Cloudsa	k1gFnPc2	Cloudsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
dům	dům	k1gInSc4	dům
nad	nad	k7c4	nad
mraky	mrak	k1gInPc4	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýše	nejvýše	k6eAd1	nejvýše
položených	položený	k2eAgNnPc2d1	položené
míst	místo	k1gNnPc2	místo
využívaných	využívaný	k2eAgNnPc2d1	využívané
ke	k	k7c3	k
zrání	zrání	k1gNnSc3	zrání
destilátů	destilát	k1gInPc2	destilát
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pomalým	pomalý	k2eAgNnSc7d1	pomalé
zráním	zrání	k1gNnSc7	zrání
získávají	získávat	k5eAaImIp3nP	získávat
rumy	rum	k1gInPc1	rum
Zacapa	Zacap	k1gMnSc2	Zacap
Centario	Centario	k6eAd1	Centario
svoje	svůj	k3xOyFgFnPc4	svůj
jedinečné	jedinečný	k2eAgFnPc4d1	jedinečná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
nižší	nízký	k2eAgFnSc1d2	nižší
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
16,66	[number]	k4	16,66
°	°	k?	°
<g/>
C	C	kA	C
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
zrání	zrání	k1gNnSc4	zrání
a	a	k8xC	a
řidší	řídký	k2eAgInSc4d2	řidší
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
nižší	nízký	k2eAgInSc4d2	nižší
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
tlak	tlak	k1gInSc4	tlak
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
zintenzivnit	zintenzivnit	k5eAaPmF	zintenzivnit
infuzi	infuze	k1gFnSc4	infuze
chutí	chuť	k1gFnPc2	chuť
z	z	k7c2	z
dubových	dubový	k2eAgInPc2d1	dubový
sudů	sud	k1gInPc2	sud
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
jsou	být	k5eAaImIp3nP	být
sudy	sud	k1gInPc1	sud
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
"	"	kIx"	"
<g/>
The	The	k1gFnSc6	The
House	house	k1gNnSc1	house
Above	Aboev	k1gFnSc2	Aboev
the	the	k?	the
Clouds	Clouds	k1gInSc1	Clouds
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
složitý	složitý	k2eAgInSc1d1	složitý
jemně	jemně	k6eAd1	jemně
vyvážený	vyvážený	k2eAgInSc1d1	vyvážený
proces	proces	k1gInSc1	proces
zrání	zrání	k1gNnSc2	zrání
<g/>
.	.	kIx.	.
</s>
<s>
Destilerie	Destilerie	k1gFnSc1	Destilerie
používá	používat	k5eAaImIp3nS	používat
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
způsob	způsob	k1gInSc1	způsob
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
Sistema	Sistema	k1gFnSc1	Sistema
Solera	Solera	k1gFnSc1	Solera
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
převzatý	převzatý	k2eAgMnSc1d1	převzatý
ze	z	k7c2	z
zrání	zrání	k1gNnSc2	zrání
Sherry	sherry	k1gNnSc2	sherry
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
destilerie	destilerie	k1gFnSc2	destilerie
Zacapa	Zacap	k1gMnSc2	Zacap
je	být	k5eAaImIp3nS	být
sistema	sistema	k1gFnSc1	sistema
solera	solera	k1gFnSc1	solera
používána	používán	k2eAgFnSc1d1	používána
pro	pro	k7c4	pro
míchání	míchání	k1gNnSc4	míchání
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
Blendování	Blendování	k1gNnSc1	Blendování
<g/>
)	)	kIx)	)
rumů	rum	k1gInPc2	rum
různého	různý	k2eAgNnSc2d1	různé
stáří	stáří	k1gNnSc2	stáří
v	v	k7c6	v
sudech	sud	k1gInPc6	sud
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dříve	dříve	k6eAd2	dříve
zrály	zrát	k5eAaImAgFnP	zrát
robustní	robustní	k2eAgFnPc1d1	robustní
americké	americký	k2eAgFnPc1d1	americká
whiskey	whiskea	k1gFnPc1	whiskea
<g/>
,	,	kIx,	,
jemné	jemný	k2eAgNnSc1d1	jemné
sherry	sherry	k1gNnSc1	sherry
a	a	k8xC	a
delikátní	delikátní	k2eAgInSc1d1	delikátní
vína	víno	k1gNnSc2	víno
Pedro	Pedro	k1gNnSc1	Pedro
Ximenez	Ximenez	k1gInSc1	Ximenez
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kapka	kapka	k1gFnSc1	kapka
rumu	rum	k1gInSc2	rum
zde	zde	k6eAd1	zde
prochází	procházet	k5eAaImIp3nS	procházet
každým	každý	k3xTgInSc7	každý
typem	typ	k1gInSc7	typ
sudu	suda	k1gFnSc4	suda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
podělí	podělit	k5eAaPmIp3nS	podělit
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
aroma	aroma	k1gNnSc4	aroma
a	a	k8xC	a
chuť	chuť	k1gFnSc4	chuť
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
rum	rum	k1gInSc4	rum
výjimečné	výjimečný	k2eAgFnSc2d1	výjimečná
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
Master	master	k1gMnSc1	master
Blender	Blender	k1gMnSc1	Blender
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
3	[number]	k4	3
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
potravinářském	potravinářský	k2eAgInSc6d1	potravinářský
průmyslu	průmysl	k1gInSc6	průmysl
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
Lorena	Loren	k2eAgFnSc1d1	Lorena
Vásquezová	Vásquezová	k1gFnSc1	Vásquezová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Produkty	produkt	k1gInPc1	produkt
==	==	k?	==
</s>
</p>
<p>
<s>
Zacapa	Zacapa	k1gFnSc1	Zacapa
15	[number]	k4	15
<g/>
yo	yo	k?	yo
–	–	k?	–
Prémiový	prémiový	k2eAgInSc4d1	prémiový
rum	rum	k1gInSc4	rum
zrající	zrající	k2eAgInSc4d1	zrající
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
5	[number]	k4	5
až	až	k9	až
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
%	%	kIx~	%
Alc	Alc	k1gFnSc1	Alc
</s>
</p>
<p>
<s>
Zacapa	Zacapa	k1gFnSc1	Zacapa
23	[number]	k4	23
<g/>
yo	yo	k?	yo
-	-	kIx~	-
Prémiový	prémiový	k2eAgInSc1d1	prémiový
rum	rum	k1gInSc1	rum
zrající	zrající	k2eAgInSc1d1	zrající
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
až	až	k9	až
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
%	%	kIx~	%
Alc	Alc	k1gFnSc1	Alc
</s>
</p>
<p>
<s>
Zacapa	Zacapa	k1gFnSc1	Zacapa
23	[number]	k4	23
<g/>
yo	yo	k?	yo
Etiqueta	Etiqueto	k1gNnSc2	Etiqueto
Negra	negr	k1gMnSc2	negr
–	–	k?	–
Prémiový	prémiový	k2eAgInSc4d1	prémiový
rum	rum	k1gInSc4	rum
zrající	zrající	k2eAgInSc4d1	zrající
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
až	až	k9	až
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
43	[number]	k4	43
<g/>
%	%	kIx~	%
Alc	Alc	k1gFnSc1	Alc
-	-	kIx~	-
Limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
</s>
</p>
<p>
<s>
Zacapa	Zacapa	k1gFnSc1	Zacapa
23	[number]	k4	23
<g/>
yo	yo	k?	yo
Straight	Straight	k2eAgMnSc1d1	Straight
from	from	k1gMnSc1	from
the	the	k?	the
Cask	Cask	k1gInSc1	Cask
–	–	k?	–
Prémiový	prémiový	k2eAgInSc1d1	prémiový
rum	rum	k1gInSc1	rum
zrající	zrající	k2eAgInSc1d1	zrající
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
až	až	k9	až
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
45	[number]	k4	45
<g/>
%	%	kIx~	%
Alc	Alc	k1gFnSc1	Alc
</s>
</p>
<p>
<s>
Zacapa	Zacapa	k1gFnSc1	Zacapa
Aniversario	Aniversario	k1gNnSc1	Aniversario
30	[number]	k4	30
-	-	kIx~	-
Exklusivní	exklusivní	k2eAgInSc1d1	exklusivní
rum	rum	k1gInSc1	rum
zrající	zrající	k2eAgInSc1d1	zrající
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
až	až	k9	až
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
%	%	kIx~	%
Alc	Alc	k1gFnSc1	Alc
-	-	kIx~	-
vydán	vydán	k2eAgInSc1d1	vydán
ku	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
30	[number]	k4	30
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
destilerie	destilerie	k1gFnSc2	destilerie
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centario	Centario	k1gMnSc1	Centario
-	-	kIx~	-
Limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
</s>
</p>
<p>
<s>
Zacapa	Zacapa	k1gFnSc1	Zacapa
XO	XO	kA	XO
-	-	kIx~	-
Exklusivní	exklusivní	k2eAgInSc4d1	exklusivní
rum	rum	k1gInSc4	rum
zrající	zrající	k2eAgInSc4d1	zrající
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
až	až	k9	až
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
%	%	kIx~	%
Alc	Alc	k1gFnSc1	Alc
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
<g/>
Caribbean	Caribbean	k1gMnSc1	Caribbean
Week	Week	k1gMnSc1	Week
Rum	rum	k1gInSc1	rum
Taste	tasit	k5eAaPmRp2nP	tasit
Festival	festival	k1gInSc4	festival
-	-	kIx~	-
Barbados	Barbados	k1gMnSc1	Barbados
-	-	kIx~	-
1	[number]	k4	1
<g/>
st	st	kA	st
place	plac	k1gInSc6	plac
-	-	kIx~	-
Premium	Premium	k1gNnSc1	Premium
category	categora	k1gFnSc2	categora
-	-	kIx~	-
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k1gMnSc1	Centenario
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
Caribbean	Caribbean	k1gMnSc1	Caribbean
Week	Week	k1gMnSc1	Week
Rum	rum	k1gInSc1	rum
Taste	tasit	k5eAaPmRp2nP	tasit
Festival	festival	k1gInSc4	festival
-	-	kIx~	-
Barbados	Barbados	k1gMnSc1	Barbados
-	-	kIx~	-
1	[number]	k4	1
<g/>
st	st	kA	st
place	plac	k1gInSc6	plac
-	-	kIx~	-
Premium	Premium	k1gNnSc1	Premium
category	categora	k1gFnSc2	categora
-	-	kIx~	-
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k1gMnSc1	Centenario
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
<g/>
Caribbean	Caribbean	k1gMnSc1	Caribbean
Week	Week	k1gMnSc1	Week
Rum	rum	k1gInSc1	rum
Taste	tasit	k5eAaPmRp2nP	tasit
Festival	festival	k1gInSc4	festival
-	-	kIx~	-
Barbados	Barbados	k1gMnSc1	Barbados
-	-	kIx~	-
Gold	Gold	k1gMnSc1	Gold
Award	Award	k1gMnSc1	Award
-	-	kIx~	-
Premium	Premium	k1gNnSc1	Premium
category	categora	k1gFnSc2	categora
-	-	kIx~	-
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k1gMnSc1	Centenario
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
<g/>
Caribbean	Caribbean	k1gMnSc1	Caribbean
Week	Week	k1gMnSc1	Week
Rum	rum	k1gInSc1	rum
Taste	tasit	k5eAaPmRp2nP	tasit
Festival	festival	k1gInSc4	festival
-	-	kIx~	-
Barbados	Barbados	k1gMnSc1	Barbados
-	-	kIx~	-
Gold	Gold	k1gMnSc1	Gold
Award	Award	k1gMnSc1	Award
-	-	kIx~	-
Premium	Premium	k1gNnSc1	Premium
category	categora	k1gFnSc2	categora
-	-	kIx~	-
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k1gMnSc1	Centenario
X.O.	X.O.	k1gMnSc1	X.O.
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
International	International	k1gFnSc1	International
Spirits	Spiritsa	k1gFnPc2	Spiritsa
Challenge	Challeng	k1gFnSc2	Challeng
-	-	kIx~	-
London	London	k1gMnSc1	London
<g/>
,	,	kIx,	,
UK	UK	kA	UK
-	-	kIx~	-
Gold	Gold	k1gMnSc1	Gold
Award	Award	k1gMnSc1	Award
-	-	kIx~	-
Super	super	k2eAgNnSc1d1	super
premium	premium	k1gNnSc1	premium
category	categora	k1gFnSc2	categora
-	-	kIx~	-
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k1gMnSc1	Centenario
X.O.	X.O.	k1gMnSc1	X.O.
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
Rum	rum	k1gInSc1	rum
Fest	fest	k6eAd1	fest
-	-	kIx~	-
Gold	Gold	k1gInSc1	Gold
Award	Award	k1gInSc1	Award
-	-	kIx~	-
Super	super	k2eAgNnSc1d1	super
premium	premium	k1gNnSc1	premium
category	categora	k1gFnSc2	categora
-	-	kIx~	-
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k1gMnSc1	Centenario
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
Rum	rum	k1gInSc1	rum
Fest	fest	k6eAd1	fest
-	-	kIx~	-
Gold	Gold	k1gInSc1	Gold
Award	Award	k1gInSc1	Award
-	-	kIx~	-
Super	super	k2eAgNnSc1d1	super
premium	premium	k1gNnSc1	premium
category	categora	k1gFnSc2	categora
-	-	kIx~	-
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k1gMnSc1	Centenario
X.O.	X.O.	k1gMnSc1	X.O.
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
Caribbean	Caribbean	k1gMnSc1	Caribbean
Week	Week	k1gMnSc1	Week
Rum	rum	k1gInSc1	rum
Taste	tasit	k5eAaPmRp2nP	tasit
Festival	festival	k1gInSc4	festival
-	-	kIx~	-
Barbados	Barbados	k1gInSc1	Barbados
-	-	kIx~	-
Super	super	k2eAgNnSc1d1	super
Premium	Premium	k1gNnSc1	Premium
Category	Categora	k1gFnSc2	Categora
-	-	kIx~	-
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k1gMnSc1	Centenario
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
Caribbean	Caribbean	k1gMnSc1	Caribbean
Week	Week	k1gMnSc1	Week
Rum	rum	k1gInSc1	rum
Taste	tasit	k5eAaPmRp2nP	tasit
Festival	festival	k1gInSc4	festival
-	-	kIx~	-
Barbados	Barbados	k1gInSc1	Barbados
-	-	kIx~	-
Super	super	k2eAgNnSc1d1	super
Premium	Premium	k1gNnSc1	Premium
Category	Categora	k1gFnSc2	Categora
-	-	kIx~	-
Ron	ron	k1gInSc1	ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k6eAd1	Centenario
XO	XO	kA	XO
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
Caribbean	Caribbean	k1gMnSc1	Caribbean
Week	Week	k1gMnSc1	Week
Rum	rum	k1gInSc1	rum
Taste	tasit	k5eAaPmRp2nP	tasit
Festival	festival	k1gInSc4	festival
-	-	kIx~	-
Barbados	Barbados	k1gInSc1	Barbados
-	-	kIx~	-
Super	super	k2eAgNnSc1d1	super
Premium	Premium	k1gNnSc1	Premium
Category	Categora	k1gFnSc2	Categora
-	-	kIx~	-
Ron	Ron	k1gMnSc1	Ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k1gMnSc1	Centenario
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
Caribbean	Caribbean	k1gMnSc1	Caribbean
Week	Week	k1gMnSc1	Week
Rum	rum	k1gInSc1	rum
Taste	tasit	k5eAaPmRp2nP	tasit
Festival	festival	k1gInSc4	festival
-	-	kIx~	-
Barbados	Barbados	k1gInSc1	Barbados
-	-	kIx~	-
Super	super	k2eAgNnSc1d1	super
Premium	Premium	k1gNnSc1	Premium
Category	Categora	k1gFnSc2	Categora
-	-	kIx~	-
Ron	ron	k1gInSc1	ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k6eAd1	Centenario
XO	XO	kA	XO
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
Caribbean	Caribbean	k1gMnSc1	Caribbean
Week	Week	k1gMnSc1	Week
Rum	rum	k1gInSc1	rum
Taste	tasit	k5eAaPmRp2nP	tasit
Festival	festival	k1gInSc4	festival
-	-	kIx~	-
Barbados	Barbados	k1gInSc1	Barbados
-	-	kIx~	-
Super	super	k2eAgNnSc1d1	super
Premium	Premium	k1gNnSc1	Premium
Category	Categora	k1gFnSc2	Categora
-	-	kIx~	-
Ron	ron	k1gInSc1	ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k6eAd1	Centenario
XO	XO	kA	XO
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
World	World	k1gMnSc1	World
Spirits	Spiritsa	k1gFnPc2	Spiritsa
Competition	Competition	k1gInSc1	Competition
-	-	kIx~	-
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
Bronze	bronz	k1gInSc5	bronz
-	-	kIx~	-
Ron	ron	k1gInSc1	ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k6eAd1	Centenario
23	[number]	k4	23
Years	Yearsa	k1gFnPc2	Yearsa
Old	Olda	k1gFnPc2	Olda
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
International	International	k1gFnSc1	International
Sugar	Sugara	k1gFnPc2	Sugara
Cane	Cane	k1gFnSc1	Cane
Spirits	Spirits	k1gInSc1	Spirits
Festival	festival	k1gInSc1	festival
and	and	k?	and
Tasting	Tasting	k1gInSc1	Tasting
Competition	Competition	k1gInSc1	Competition
-	-	kIx~	-
Tampa	Tampa	k1gFnSc1	Tampa
<g/>
,	,	kIx,	,
FL	FL	kA	FL
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
Gold	Gold	k1gMnSc1	Gold
-	-	kIx~	-
Premium	Premium	k1gNnSc1	Premium
Rum	rum	k1gInSc1	rum
-	-	kIx~	-
Ron	ron	k1gInSc1	ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k6eAd1	Centenario
15	[number]	k4	15
Year	Yeara	k1gFnPc2	Yeara
Old	Olda	k1gFnPc2	Olda
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
International	International	k1gFnSc1	International
Sugar	Sugara	k1gFnPc2	Sugara
Cane	Cane	k1gFnSc1	Cane
Spirits	Spirits	k1gInSc1	Spirits
Festival	festival	k1gInSc1	festival
and	and	k?	and
Tasting	Tasting	k1gInSc1	Tasting
Competition	Competition	k1gInSc1	Competition
-	-	kIx~	-
Tampa	Tampa	k1gFnSc1	Tampa
<g/>
,	,	kIx,	,
FL	FL	kA	FL
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
Gold	Gold	k1gMnSc1	Gold
-	-	kIx~	-
Premium	Premium	k1gNnSc1	Premium
Rum	rum	k1gInSc1	rum
-	-	kIx~	-
Ron	ron	k1gInSc1	ron
Zacapa	Zacap	k1gMnSc2	Zacap
Centenario	Centenario	k6eAd1	Centenario
23	[number]	k4	23
Year	Yeara	k1gFnPc2	Yeara
Old	Olda	k1gFnPc2	Olda
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
World	World	k1gMnSc1	World
Spirits	Spiritsa	k1gFnPc2	Spiritsa
Competition	Competition	k1gInSc1	Competition
-	-	kIx~	-
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
Silver	Silver	k1gMnSc1	Silver
Medal	Medal	k1gMnSc1	Medal
-	-	kIx~	-
Zacapa	Zacapa	k1gFnSc1	Zacapa
15	[number]	k4	15
Year	Yeara	k1gFnPc2	Yeara
Old	Olda	k1gFnPc2	Olda
Rum	rum	k1gInSc1	rum
Centenario	Centenario	k6eAd1	Centenario
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
World	World	k1gMnSc1	World
Spirits	Spiritsa	k1gFnPc2	Spiritsa
Competition	Competition	k1gInSc1	Competition
-	-	kIx~	-
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
Silver	Silver	k1gMnSc1	Silver
Medal	Medal	k1gMnSc1	Medal
-	-	kIx~	-
Zacapa	Zacapa	k1gFnSc1	Zacapa
25	[number]	k4	25
Year	Yeara	k1gFnPc2	Yeara
Old	Olda	k1gFnPc2	Olda
Rum	rum	k1gInSc1	rum
Centenario	Centenario	k6eAd1	Centenario
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ron	ron	k1gInSc1	ron
Zacapa	Zacap	k1gMnSc4	Zacap
Centenario	Centenario	k6eAd1	Centenario
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Ron	ron	k1gInSc1	ron
Zacapa	Zacapa	k1gFnSc1	Zacapa
Centario	Centario	k1gMnSc1	Centario
</s>
</p>
<p>
<s>
Petrovy	Petrův	k2eAgFnPc1d1	Petrova
rumové	rumový	k2eAgFnPc1d1	rumová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
