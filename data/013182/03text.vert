<p>
<s>
Vincenc	Vincenc	k1gMnSc1	Vincenc
Červinka	Červinka	k1gMnSc1	Červinka
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
I.	I.	kA	I.
odboje	odboj	k1gInPc4	odboj
proti	proti	k7c3	proti
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc3	Rakousku-Uhersko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Vincenc	Vincenc	k1gMnSc1	Vincenc
Červinka	Červinka	k1gMnSc1	Červinka
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Kolíně	Kolín	k1gInSc6	Kolín
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
tamního	tamní	k2eAgMnSc2d1	tamní
průmyslníka	průmyslník	k1gMnSc2	průmyslník
<g/>
,	,	kIx,	,
dlouholetého	dlouholetý	k2eAgMnSc2d1	dlouholetý
člena	člen	k1gMnSc2	člen
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
předáka	předák	k1gMnSc2	předák
strany	strana	k1gFnSc2	strana
Staročechů	Staročech	k1gMnPc2	Staročech
Josefa	Josef	k1gMnSc2	Josef
Červinky	Červinka	k1gMnSc2	Červinka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
na	na	k7c6	na
kolínském	kolínský	k2eAgNnSc6d1	kolínské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
medicínu	medicína	k1gFnSc4	medicína
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlo-Ferdinandově	Karlo-Ferdinandův	k2eAgFnSc6d1	Karlo-Ferdinandova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
medicíny	medicína	k1gFnSc2	medicína
však	však	k9	však
brzy	brzy	k6eAd1	brzy
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
filologii	filologie	k1gFnSc4	filologie
a	a	k8xC	a
novinářství	novinářství	k1gNnSc4	novinářství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc3	Petrohrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
se	se	k3xPyFc4	se
sblížil	sblížit	k5eAaPmAgInS	sblížit
s	s	k7c7	s
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Aloisem	Alois	k1gMnSc7	Alois
Rašínem	Rašín	k1gMnSc7	Rašín
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
skupinou	skupina	k1gFnSc7	skupina
kolem	kolem	k7c2	kolem
týdeníku	týdeník	k1gInSc2	týdeník
Slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
letech	let	k1gInPc6	let
1901	[number]	k4	1901
–	–	k?	–
1905	[number]	k4	1905
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
jako	jako	k9	jako
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
zhruba	zhruba	k6eAd1	zhruba
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
založil	založit	k5eAaPmAgMnS	založit
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Kramářem	kramář	k1gMnSc7	kramář
časopis	časopis	k1gInSc4	časopis
Nová	nový	k2eAgFnSc1d1	nová
česká	český	k2eAgFnSc1d1	Česká
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
na	na	k7c4	na
deník	deník	k1gInSc4	deník
Den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
redaktorem	redaktor	k1gMnSc7	redaktor
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
časopisem	časopis	k1gInSc7	časopis
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Praha	Praha	k1gFnSc1	Praha
vydavatele	vydavatel	k1gMnSc2	vydavatel
Jana	Jan	k1gMnSc2	Jan
Otta	Otta	k1gMnSc1	Otta
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gInSc7	člen
jeho	jeho	k3xOp3gFnSc2	jeho
redakce	redakce	k1gFnSc2	redakce
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
přispíval	přispívat	k5eAaImAgInS	přispívat
do	do	k7c2	do
časopisů	časopis	k1gInPc2	časopis
Osvěta	osvěta	k1gFnSc1	osvěta
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
revue	revue	k1gFnSc1	revue
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gInSc1	Lumír
a	a	k8xC	a
Zvon	zvon	k1gInSc1	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
redakce	redakce	k1gFnSc2	redakce
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
jejím	její	k3xOp3gMnSc7	její
tajemníkem	tajemník	k1gMnSc7	tajemník
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
odpovědným	odpovědný	k2eAgMnSc7d1	odpovědný
redaktorem	redaktor	k1gMnSc7	redaktor
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zástupcem	zástupce	k1gMnSc7	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
zde	zde	k6eAd1	zde
rubriku	rubrika	k1gFnSc4	rubrika
Ze	z	k7c2	z
světa	svět	k1gInSc2	svět
slovanského	slovanský	k2eAgMnSc2d1	slovanský
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
úvodníky	úvodník	k1gInPc7	úvodník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
také	také	k9	také
literárním	literární	k2eAgInPc3d1	literární
překladům	překlad	k1gInPc3	překlad
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
přeložil	přeložit	k5eAaPmAgMnS	přeložit
díla	dílo	k1gNnPc4	dílo
Leonida	Leonid	k1gMnSc2	Leonid
Andrejeva	Andrejev	k1gMnSc2	Andrejev
<g/>
,	,	kIx,	,
Arkadije	Arkadije	k1gMnSc2	Arkadije
Averčenka	Averčenka	k1gFnSc1	Averčenka
<g/>
,	,	kIx,	,
Ivana	Ivan	k1gMnSc2	Ivan
Bunina	Bunin	k1gMnSc2	Bunin
<g/>
,	,	kIx,	,
Denise	Denisa	k1gFnSc3	Denisa
Fonvizina	Fonvizino	k1gNnSc2	Fonvizino
<g/>
,	,	kIx,	,
Vasilije	Vasilije	k1gFnSc1	Vasilije
Němiroviče-Dančenka	Němiroviče-Dančenka	k1gFnSc1	Němiroviče-Dančenka
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
zásluhou	zásluha	k1gFnSc7	zásluha
byla	být	k5eAaImAgFnS	být
čeština	čeština	k1gFnSc1	čeština
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgInSc2	jenž
byly	být	k5eAaImAgFnP	být
přeloženy	přeložen	k2eAgFnPc1d1	přeložena
hry	hra	k1gFnPc1	hra
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
židovského	židovský	k2eAgMnSc2d1	židovský
dramatika	dramatik	k1gMnSc2	dramatik
Lea	Leo	k1gMnSc2	Leo
Birinského	Birinský	k2eAgMnSc2d1	Birinský
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
Mumraj	mumraj	k1gInSc1	mumraj
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
uveden	uveden	k2eAgInSc1d1	uveden
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
po	po	k7c6	po
německých	německý	k2eAgFnPc6d1	německá
premiérách	premiéra	k1gFnPc6	premiéra
<g/>
.	.	kIx.	.
</s>
<s>
Leo	Leo	k1gMnSc1	Leo
Birinski	Birinski	k1gNnSc2	Birinski
byl	být	k5eAaImAgMnS	být
i	i	k9	i
díky	díky	k7c3	díky
Červinkově	Červinkův	k2eAgFnSc3d1	Červinkova
překladatelskému	překladatelský	k2eAgNnSc3d1	překladatelské
zaměření	zaměření	k1gNnSc3	zaměření
na	na	k7c4	na
ruštinu	ruština	k1gFnSc4	ruština
mylně	mylně	k6eAd1	mylně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
spisovatele	spisovatel	k1gMnPc4	spisovatel
píšícího	píšící	k2eAgMnSc4d1	píšící
rusky	rusky	k6eAd1	rusky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Politicky	politicky	k6eAd1	politicky
měl	mít	k5eAaImAgMnS	mít
Červinka	Červinka	k1gMnSc1	Červinka
blízko	blízko	k6eAd1	blízko
ke	k	k7c3	k
Karlu	Karel	k1gMnSc3	Karel
Kramářovi	kramář	k1gMnSc3	kramář
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
stoupencem	stoupenec	k1gMnSc7	stoupenec
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
vzájemnosti	vzájemnost	k1gFnSc2	vzájemnost
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
vlivem	vliv	k1gInSc7	vliv
připojil	připojit	k5eAaPmAgInS	připojit
i	i	k9	i
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
novoslovanství	novoslovanství	k1gNnSc2	novoslovanství
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Kramářovy	kramářův	k2eAgFnSc2d1	Kramářova
národní	národní	k2eAgFnSc2d1	národní
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
po	po	k7c4	po
jistý	jistý	k2eAgInSc4d1	jistý
čas	čas	k1gInSc4	čas
i	i	k9	i
jeho	jeho	k3xOp3gMnSc7	jeho
osobním	osobní	k2eAgMnSc7d1	osobní
tajemníkem	tajemník	k1gMnSc7	tajemník
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
sympatizoval	sympatizovat	k5eAaImAgMnS	sympatizovat
se	s	k7c7	s
svobodným	svobodný	k2eAgNnSc7d1	svobodné
zednářstvím	zednářství	k1gNnSc7	zednářství
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
pražské	pražský	k2eAgFnSc2d1	Pražská
lóže	lóže	k1gFnSc2	lóže
Hiram	Hiram	k1gInSc1	Hiram
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
První	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
I.	I.	kA	I.
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Kramářem	kramář	k1gMnSc7	kramář
<g/>
,	,	kIx,	,
Aloisem	Alois	k1gMnSc7	Alois
Rašínem	Rašín	k1gMnSc7	Rašín
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Zamazalem	Zamazal	k1gMnSc7	Zamazal
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
po	po	k7c6	po
osmiměsíčním	osmiměsíční	k2eAgInSc6d1	osmiměsíční
procesu	proces	k1gInSc6	proces
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
a	a	k8xC	a
vyzvědačství	vyzvědačství	k1gNnSc4	vyzvědačství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
milostí	milost	k1gFnSc7	milost
nového	nový	k2eAgMnSc2d1	nový
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
trest	trest	k1gInSc1	trest
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c4	na
20	[number]	k4	20
let	léto	k1gNnPc2	léto
žaláře	žalář	k1gInSc2	žalář
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
amnestován	amnestovat	k5eAaBmNgInS	amnestovat
a	a	k8xC	a
propuštěn	propustit	k5eAaPmNgInS	propustit
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenosti	zkušenost	k1gFnPc1	zkušenost
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
později	pozdě	k6eAd2	pozdě
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Moje	můj	k3xOp1gInPc1	můj
rakouské	rakouský	k2eAgInPc1d1	rakouský
žaláře	žalář	k1gInPc1	žalář
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
žaláře	žalář	k1gInSc2	žalář
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
v	v	k7c6	v
Národních	národní	k2eAgInPc6d1	národní
listech	list	k1gInPc6	list
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
Syndikátu	syndikát	k1gInSc2	syndikát
denního	denní	k2eAgInSc2d1	denní
tisku	tisk	k1gInSc2	tisk
československého	československý	k2eAgInSc2d1	československý
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c6	na
pozvání	pozvání	k1gNnSc6	pozvání
krajanů	krajan	k1gMnPc2	krajan
na	na	k7c4	na
přednáškové	přednáškový	k2eAgNnSc4d1	přednáškové
turné	turné	k1gNnSc4	turné
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
k	k	k7c3	k
jednotkám	jednotka	k1gFnPc3	jednotka
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
přes	přes	k7c4	přes
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
a	a	k8xC	a
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vykonal	vykonat	k5eAaPmAgMnS	vykonat
cestu	cesta	k1gFnSc4	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
využil	využít	k5eAaPmAgInS	využít
k	k	k7c3	k
napsání	napsání	k1gNnSc2	napsání
několika	několik	k4yIc2	několik
knih	kniha	k1gFnPc2	kniha
s	s	k7c7	s
politickými	politický	k2eAgFnPc7d1	politická
a	a	k8xC	a
cestopisnými	cestopisný	k2eAgFnPc7d1	cestopisná
úvahami	úvaha	k1gFnPc7	úvaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
Československu	Československo	k1gNnSc6	Československo
dále	daleko	k6eAd2	daleko
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
zůstal	zůstat	k5eAaPmAgInS	zůstat
věrný	věrný	k2eAgMnSc1d1	věrný
až	až	k9	až
do	do	k7c2	do
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
výslužby	výslužba	k1gFnSc2	výslužba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
široký	široký	k2eAgInSc4d1	široký
novinářský	novinářský	k2eAgInSc4d1	novinářský
záběr	záběr	k1gInSc4	záběr
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
články	článek	k1gInPc4	článek
politické	politický	k2eAgInPc4d1	politický
<g/>
,	,	kIx,	,
komentáře	komentář	k1gInPc1	komentář
<g/>
,	,	kIx,	,
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
významných	významný	k2eAgFnPc6d1	významná
osobnostech	osobnost	k1gFnPc6	osobnost
různých	různý	k2eAgInPc2d1	různý
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc2d1	literární
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
novinářské	novinářský	k2eAgFnSc2d1	novinářská
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
–	–	k?	–
1933	[number]	k4	1933
v	v	k7c6	v
Ottově	Ottův	k2eAgNnSc6d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
řídil	řídit	k5eAaImAgMnS	řídit
a	a	k8xC	a
redigoval	redigovat	k5eAaImAgMnS	redigovat
edici	edice	k1gFnSc4	edice
Ruská	ruský	k2eAgFnSc1d1	ruská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
respektovanou	respektovaný	k2eAgFnSc4d1	respektovaná
pro	pro	k7c4	pro
ediční	ediční	k2eAgInSc4d1	ediční
výběr	výběr	k1gInSc4	výběr
i	i	k9	i
pro	pro	k7c4	pro
kvalitu	kvalita	k1gFnSc4	kvalita
překladů	překlad	k1gInPc2	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
i	i	k9	i
v	v	k7c6	v
překladatelské	překladatelský	k2eAgFnSc6d1	překladatelská
práci	práce	k1gFnSc6	práce
a	a	k8xC	a
české	český	k2eAgMnPc4d1	český
čtenáře	čtenář	k1gMnPc4	čtenář
seznamoval	seznamovat	k5eAaImAgInS	seznamovat
s	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
sovětských	sovětský	k2eAgMnPc2d1	sovětský
autorů	autor	k1gMnPc2	autor
i	i	k8xC	i
ruských	ruský	k2eAgMnPc2d1	ruský
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
překládal	překládat	k5eAaImAgMnS	překládat
díla	dílo	k1gNnPc4	dílo
Valerije	Valerije	k1gFnSc2	Valerije
Brjusova	Brjusův	k2eAgNnSc2d1	Brjusův
<g/>
,	,	kIx,	,
Michaila	Michail	k1gMnSc2	Michail
Bulgakova	Bulgakův	k2eAgMnSc2d1	Bulgakův
<g/>
,	,	kIx,	,
Valentina	Valentin	k1gMnSc2	Valentin
Katajeva	Katajev	k1gMnSc2	Katajev
<g/>
,	,	kIx,	,
Ivana	Ivan	k1gMnSc2	Ivan
Naživina	Naživina	k1gFnSc1	Naživina
nebo	nebo	k8xC	nebo
Michaila	Michaila	k1gFnSc1	Michaila
Zoščenka	Zoščenka	k1gFnSc1	Zoščenka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
zavedl	zavést	k5eAaPmAgInS	zavést
užívání	užívání	k1gNnSc4	užívání
původně	původně	k6eAd1	původně
ruských	ruský	k2eAgNnPc2d1	ruské
slov	slovo	k1gNnPc2	slovo
bezprizorný	bezprizorný	k2eAgInSc1d1	bezprizorný
<g/>
,	,	kIx,	,
kulak	kulak	k1gMnSc1	kulak
nebo	nebo	k8xC	nebo
kerenština	kerenština	k1gFnSc1	kerenština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
nově	nova	k1gFnSc3	nova
založené	založený	k2eAgFnSc2d1	založená
zednářské	zednářský	k2eAgFnSc2d1	zednářská
lóže	lóže	k1gFnSc2	lóže
Jan	Jan	k1gMnSc1	Jan
Ámos	Ámos	k1gMnSc1	Ámos
Komenský	Komenský	k1gMnSc1	Komenský
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jejím	její	k3xOp3gMnSc7	její
členem	člen	k1gMnSc7	člen
až	až	k8xS	až
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
všech	všecek	k3xTgFnPc2	všecek
československých	československý	k2eAgFnPc2d1	Československá
novinářských	novinářský	k2eAgFnPc2d1	novinářská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Syndikátu	syndikát	k1gInSc6	syndikát
československých	československý	k2eAgMnPc2d1	československý
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Slovanského	slovanský	k2eAgInSc2d1	slovanský
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
předsedou	předseda	k1gMnSc7	předseda
Společnosti	společnost	k1gFnSc3	společnost
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
vzájemnosti	vzájemnost	k1gFnSc2	vzájemnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Používané	používaný	k2eAgInPc1d1	používaný
pseudonymy	pseudonym	k1gInPc1	pseudonym
a	a	k8xC	a
akronymy	akronym	k1gInPc1	akronym
===	===	k?	===
</s>
</p>
<p>
<s>
E.	E.	kA	E.
Adámek	Adámek	k1gMnSc1	Adámek
<g/>
,	,	kIx,	,
Ervín	Ervín	k1gMnSc1	Ervín
Kačer	kačer	k1gMnSc1	kačer
<g/>
,	,	kIx,	,
Vikentij	Vikentij	k1gMnSc1	Vikentij
Osipovič	Osipovič	k1gMnSc1	Osipovič
Červinka	Červinka	k1gMnSc1	Červinka
<g/>
,	,	kIx,	,
Campanus	Campanus	k1gMnSc1	Campanus
<g/>
,	,	kIx,	,
Tantris	Tantris	k1gFnSc1	Tantris
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
Č.	Č.	kA	Č.
<g/>
,	,	kIx,	,
Č.	Č.	kA	Č.
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
Ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
E.A.	E.A.	k1gMnSc1	E.A.
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
Kol	Kola	k1gFnPc2	Kola
<g/>
,	,	kIx,	,
Viki	Viki	k1gNnSc4	Viki
<g/>
,	,	kIx,	,
Samo	sám	k3xTgNnSc4	sám
<g/>
,	,	kIx,	,
Bis	Bis	k1gFnPc1	Bis
<g/>
,	,	kIx,	,
A-	A-	k1gFnPc1	A-
<g/>
Z.	Z.	kA	Z.
Za	za	k7c4	za
okupace	okupace	k1gFnPc4	okupace
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
používal	používat	k5eAaImAgInS	používat
značky	značka	k1gFnSc2	značka
J.	J.	kA	J.
<g/>
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Leon	Leona	k1gFnPc2	Leona
B.	B.	kA	B.
<g/>
,	,	kIx,	,
L.B.	L.B.	k1gFnSc1	L.B.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
===	===	k?	===
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
velkoknížete	velkokníže	k1gNnSc2wR	velkokníže
Gediminase	Gediminasa	k1gFnSc3	Gediminasa
III	III	kA	III
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
Litevská	litevský	k2eAgFnSc1d1	Litevská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vlastní	vlastní	k2eAgFnPc4d1	vlastní
publikace	publikace	k1gFnPc4	publikace
===	===	k?	===
</s>
</p>
<p>
<s>
Novinářská	novinářský	k2eAgFnSc1d1	novinářská
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
organisace	organisace	k1gFnSc1	organisace
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
oceán	oceán	k1gInSc4	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bolševický	bolševický	k2eAgInSc1d1	bolševický
ráj	ráj	k1gInSc1	ráj
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
E.	E.	kA	E.
Adámek	Adámek	k1gMnSc1	Adámek
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Romanem	Roman	k1gMnSc7	Roman
Čechem	Čech	k1gMnSc7	Čech
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naši	náš	k3xOp1gMnPc1	náš
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sibiřské	sibiřský	k2eAgInPc1d1	sibiřský
děje	děj	k1gInPc1	děj
a	a	k8xC	a
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tragédie	tragédie	k1gFnSc1	tragédie
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
F.	F.	kA	F.
M.	M.	kA	M.
Dostojevskij	Dostojevskij	k1gFnPc2	Dostojevskij
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
<g/>
:	:	kIx,	:
Výběr	výběr	k1gInSc1	výběr
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střízlivá	střízlivý	k2eAgFnSc1d1	střízlivá
exotika	exotika	k1gFnSc1	exotika
<g/>
,	,	kIx,	,
cestopisné	cestopisný	k2eAgFnPc1d1	cestopisná
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dostojevskij	Dostojevskít	k5eAaPmRp2nS	Dostojevskít
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moje	můj	k3xOp1gInPc1	můj
rakouské	rakouský	k2eAgInPc1d1	rakouský
žaláře	žalář	k1gInPc1	žalář
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínková	vzpomínkový	k2eAgFnSc1d1	vzpomínková
kronika	kronika	k1gFnSc1	kronika
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1914	[number]	k4	1914
–	–	k?	–
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
a	a	k8xC	a
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Editace	editace	k1gFnSc1	editace
sborníků	sborník	k1gInPc2	sborník
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
persekucí	persekuce	k1gFnPc2	persekuce
a	a	k8xC	a
kriminálů	kriminál	k1gInPc2	kriminál
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
českých	český	k2eAgMnPc2d1	český
novinářů	novinář	k1gMnPc2	novinář
a	a	k8xC	a
literátů	literát	k1gMnPc2	literát
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Padesátka	padesátka	k1gFnSc1	padesátka
<g/>
.	.	kIx.	.
</s>
<s>
Veselé	Veselá	k1gFnPc4	Veselá
i	i	k8xC	i
tragické	tragický	k2eAgInPc4d1	tragický
obrazy	obraz	k1gInPc4	obraz
současného	současný	k2eAgNnSc2d1	současné
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antologie	antologie	k1gFnSc1	antologie
sovětského	sovětský	k2eAgInSc2d1	sovětský
humoru	humor	k1gInSc2	humor
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
L.	L.	kA	L.
<g/>
N.	N.	kA	N.
<g/>
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
současníků	současník	k1gMnPc2	současník
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
jidiš	jidiš	k1gNnSc2	jidiš
prózy	próza	k1gFnSc2	próza
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Překlady	překlad	k1gInPc4	překlad
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozsahu	rozsah	k1gInSc3	rozsah
Červinkova	Červinkův	k2eAgNnSc2d1	Červinkův
překladatelského	překladatelský	k2eAgNnSc2d1	překladatelské
díla	dílo	k1gNnSc2	dílo
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
uvedeni	uvést	k5eAaPmNgMnP	uvést
jen	jen	k9	jen
nejznámější	známý	k2eAgMnSc1d3	nejznámější
autoři	autor	k1gMnPc1	autor
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Birinského	Birinský	k2eAgInSc2d1	Birinský
her	hra	k1gFnPc2	hra
jde	jít	k5eAaImIp3nS	jít
vesměs	vesměs	k6eAd1	vesměs
o	o	k7c4	o
překlady	překlad	k1gInPc4	překlad
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leonid	Leonid	k1gInSc1	Leonid
Andrejev	Andrejev	k1gMnSc1	Andrejev
<g/>
:	:	kIx,	:
Tři	tři	k4xCgFnPc1	tři
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rozum	rozum	k1gInSc1	rozum
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
k	k	k7c3	k
bližnímu	bližní	k1gMnSc3	bližní
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
prósy	prós	k1gInPc1	prós
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zachraňte	zachránit	k5eAaPmRp2nP	zachránit
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
výboru	výbor	k1gInSc2	výbor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nezabiješ	zabít	k5eNaPmIp2nS	zabít
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
práce	práce	k1gFnPc1	práce
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jidáš	jidáš	k1gInSc1	jidáš
Iškariotský	iškariotský	k2eAgInSc1d1	iškariotský
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
práce	práce	k1gFnPc1	práce
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Boženou	Božena	k1gFnSc7	Božena
Popovou	popový	k2eAgFnSc7d1	popová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
sedmi	sedm	k4xCc6	sedm
oběšených	oběšený	k2eAgInPc6d1	oběšený
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Boženou	Božena	k1gFnSc7	Božena
Popovou	popový	k2eAgFnSc7d1	popová
a	a	k8xC	a
Milošem	Miloš	k1gMnSc7	Miloš
Novákem	Novák	k1gMnSc7	Novák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dni	den	k1gInSc3	den
našeho	náš	k3xOp1gInSc2	náš
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arkadij	Arkadít	k5eAaPmRp2nS	Arkadít
Averčenko	Averčenka	k1gFnSc5	Averčenka
<g/>
:	:	kIx,	:
Humoresky	humoreska	k1gFnPc1	humoreska
a	a	k8xC	a
satiry	satira	k1gFnPc1	satira
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
J.	J.	kA	J.
Havlínem	Havlín	k1gMnSc7	Havlín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kola	Kola	k1gFnSc1	Kola
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dvanáct	dvanáct	k4xCc4	dvanáct
portrétů	portrét	k1gInPc2	portrét
znamenitých	znamenitý	k2eAgMnPc2d1	znamenitý
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hra	hra	k1gFnSc1	hra
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
pětka	pětka	k1gFnSc1	pětka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dramata	drama	k1gNnPc4	drama
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Druhá	druhý	k4xOgFnSc1	druhý
pětka	pětka	k1gFnSc1	pětka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Dramata	drama	k1gNnPc4	drama
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Povídky	povídka	k1gFnPc1	povídka
pro	pro	k7c4	pro
rekonvalescenty	rekonvalescent	k1gMnPc4	rekonvalescent
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tucet	tucet	k1gInSc1	tucet
nožů	nůž	k1gInPc2	nůž
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leo	Leo	k1gMnSc1	Leo
Birinski	Birinsk	k1gFnSc2	Birinsk
<g/>
:	:	kIx,	:
Moloch	Moloch	k1gMnSc1	Moloch
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mumraj	mumraj	k1gInSc1	mumraj
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Christianovič	Christianovič	k1gMnSc1	Christianovič
Brunovskij	Brunovskij	k1gMnSc1	Brunovskij
<g/>
:	:	kIx,	:
V	v	k7c6	v
sovětských	sovětský	k2eAgInPc6d1	sovětský
žalářích	žalář	k1gInPc6	žalář
(	(	kIx(	(
<g/>
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
odsouzence	odsouzenec	k1gMnSc2	odsouzenec
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Michail	Michail	k1gMnSc1	Michail
Bulgakov	Bulgakov	k1gInSc1	Bulgakov
<g/>
:	:	kIx,	:
Bílá	bílý	k2eAgFnSc1d1	bílá
garda	garda	k1gFnSc1	garda
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Čechov	Čechov	k1gMnSc1	Čechov
<g/>
:	:	kIx,	:
Slzy	slza	k1gFnPc1	slza
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
svět	svět	k1gInSc1	svět
nevidí	vidět	k5eNaImIp3nS	vidět
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Smutné	Smutné	k2eAgFnPc1d1	Smutné
povídky	povídka	k1gFnPc1	povídka
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ivanov	Ivanov	k1gInSc1	Ivanov
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Racek	racek	k1gMnSc1	racek
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Višňový	višňový	k2eAgInSc1d1	višňový
sad	sad	k1gInSc1	sad
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strýček	strýček	k1gMnSc1	strýček
Váňa	Váňa	k1gMnSc1	Váňa
(	(	kIx(	(
<g/>
rukopis	rukopis	k1gInSc1	rukopis
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Děnikin	Děnikin	k1gMnSc1	Děnikin
<g/>
:	:	kIx,	:
Ruská	ruský	k2eAgFnSc1d1	ruská
otázka	otázka	k1gFnSc1	otázka
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fjodor	Fjodor	k1gMnSc1	Fjodor
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
<g/>
:	:	kIx,	:
Výběr	výběr	k1gInSc1	výběr
povídek	povídka	k1gFnPc2	povídka
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strýčinkův	strýčinkův	k2eAgInSc1d1	strýčinkův
sen	sen	k1gInSc1	sen
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
Fedin	Fedin	k1gMnSc1	Fedin
<g/>
:	:	kIx,	:
Narovčatská	Narovčatský	k2eAgFnSc1d1	Narovčatský
kronika	kronika	k1gFnSc1	kronika
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Denis	Denisa	k1gFnPc2	Denisa
Fonvizin	Fonvizin	k1gMnSc1	Fonvizin
<g/>
:	:	kIx,	:
Mazánek	Mazánek	k1gMnSc1	Mazánek
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Gončarov	Gončarov	k1gInSc1	Gončarov
<g/>
:	:	kIx,	:
Sluhové	sluhová	k1gFnSc2	sluhová
starého	starý	k2eAgInSc2d1	starý
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
J.	J.	kA	J.
Pelíškem	pelíšek	k1gInSc7	pelíšek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maxim	Maxim	k1gMnSc1	Maxim
Gorkij	Gorkij	k1gMnSc1	Gorkij
<g/>
:	:	kIx,	:
Žaloby	žaloba	k1gFnPc1	žaloba
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc1	pohádka
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přátelé	přítel	k1gMnPc1	přítel
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Jevrejnov	Jevrejnov	k1gInSc1	Jevrejnov
<g/>
:	:	kIx,	:
Co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
nejhlavnější	hlavní	k2eAgFnSc4d3	nejhlavnější
<g/>
,	,	kIx,	,
Koráb	koráb	k1gInSc4	koráb
spravedlivých	spravedlivý	k2eAgMnPc2d1	spravedlivý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Valentin	Valentin	k1gMnSc1	Valentin
Katajev	Katajev	k1gMnSc1	Katajev
<g/>
:	:	kIx,	:
Kvadratura	kvadratura	k1gFnSc1	kvadratura
kruhu	kruh	k1gInSc2	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Galaktionovič	Galaktionovič	k1gMnSc1	Galaktionovič
Korolenko	Korolenka	k1gFnSc5	Korolenka
<g/>
:	:	kIx,	:
Pád	Pád	k1gInSc1	Pád
carské	carský	k2eAgFnSc2d1	carská
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
výklad	výklad	k1gInSc1	výklad
prostým	prostý	k2eAgMnPc3d1	prostý
lidem	člověk	k1gMnPc3	člověk
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Kuprin	Kuprin	k1gInSc1	Kuprin
<g/>
:	:	kIx,	:
Souboj	souboj	k1gInSc1	souboj
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Naživin	Naživina	k1gFnPc2	Naživina
<g/>
:	:	kIx,	:
V	v	k7c6	v
mlhách	mlha	k1gFnPc6	mlha
budoucnosti	budoucnost	k1gFnSc2	budoucnost
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rasputin	Rasputin	k1gMnSc1	Rasputin
I.	I.	kA	I.
–	–	k?	–
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
–	–	k?	–
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Věčná	věčný	k2eAgFnSc1d1	věčná
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vasilij	Vasilít	k5eAaPmRp2nS	Vasilít
Němirovič-Dančenko	Němirovič-Dančenka	k1gFnSc5	Němirovič-Dančenka
<g/>
:	:	kIx,	:
Pestré	pestrý	k2eAgFnPc1d1	pestrá
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strašní	strašný	k2eAgMnPc1d1	strašný
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Ostrovskij	Ostrovskij	k1gMnSc1	Ostrovskij
<g/>
:	:	kIx,	:
Bouře	bouře	k1gFnSc1	bouře
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
použil	použít	k5eAaPmAgMnS	použít
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
pro	pro	k7c4	pro
libreto	libreto	k1gNnSc4	libreto
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
Káťa	Káťa	k1gFnSc1	Káťa
Kabanová	Kabanová	k1gFnSc1	Kabanová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bez	bez	k7c2	bez
věna	věno	k1gNnSc2	věno
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nevolnice	nevolnice	k1gFnSc1	nevolnice
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Horoucí	horoucí	k2eAgNnSc1d1	horoucí
srdce	srdce	k1gNnSc1	srdce
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
I	i	k8xC	i
<g />
.	.	kIx.	.
</s>
<s>
chytrák	chytrák	k1gMnSc1	chytrák
se	se	k3xPyFc4	se
spálí	spálit	k5eAaPmIp3nS	spálit
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Do	do	k7c2	do
cizích	cizí	k2eAgFnPc2d1	cizí
saní	saně	k1gFnPc2	saně
nesedej	sedat	k5eNaImRp2nS	sedat
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pravda	pravda	k9	pravda
dobrá	dobrý	k2eAgFnSc1d1	dobrá
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
štěstí	štěstí	k1gNnSc1	štěstí
lepší	dobrý	k2eAgNnSc1d2	lepší
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šaškové	Šaškové	k2eAgFnSc1d1	Šaškové
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vlci	vlk	k1gMnPc1	vlk
a	a	k8xC	a
ovce	ovce	k1gFnPc1	ovce
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alexej	Alexej	k1gMnSc1	Alexej
Pisemskij	Pisemskij	k1gMnSc1	Pisemskij
<g/>
:	:	kIx,	:
Hořký	hořký	k2eAgInSc1d1	hořký
osud	osud	k1gInSc1	osud
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čím	co	k3yQnSc7	co
se	se	k3xPyFc4	se
provinila	provinit	k5eAaPmAgFnS	provinit
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ve	v	k7c6	v
víru	vír	k1gInSc6	vír
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mr	Mr	k1gFnSc4	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Batmanov	Batmanov	k1gInSc1	Batmanov
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Setník	setník	k1gMnSc1	setník
Ruchněv	Ruchněv	k1gMnSc1	Ruchněv
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ruští	ruský	k2eAgMnPc1d1	ruský
lháři	lhář	k1gMnPc1	lhář
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
V.	V.	kA	V.
Frypésem	Frypés	k1gMnSc7	Frypés
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Měšťáci	měšťák	k1gMnPc1	měšťák
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alexej	Alexej	k1gMnSc1	Alexej
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
:	:	kIx,	:
Zlatíčko	zlatíčko	k1gNnSc1	zlatíčko
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kukaččiny	kukaččin	k2eAgFnPc1d1	kukaččin
slzy	slza	k1gFnPc1	slza
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nečistá	čistý	k2eNgFnSc1d1	nečistá
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
zlatá	zlatý	k2eAgFnSc1d1	zlatá
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lev	Lev	k1gMnSc1	Lev
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
:	:	kIx,	:
Sonata	Sonata	k1gFnSc1	Sonata
Kreutzerova	Kreutzerův	k2eAgFnSc1d1	Kreutzerova
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
První	první	k4xOgFnPc1	první
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
a	a	k8xC	a
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Povídky	povídka	k1gFnPc1	povídka
a	a	k8xC	a
hry	hra	k1gFnPc1	hra
kdysi	kdysi	k6eAd1	kdysi
potlačené	potlačený	k2eAgInPc4d1	potlačený
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Falešný	falešný	k2eAgInSc1d1	falešný
kupon	kupon	k1gInSc1	kupon
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
práce	práce	k1gFnPc1	práce
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
A	a	k8xC	a
světlo	světlo	k1gNnSc1	světlo
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
svítí	svítit	k5eAaImIp3nS	svítit
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nakažená	nakažený	k2eAgFnSc1d1	nakažená
rodina	rodina	k1gFnSc1	rodina
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Plody	plod	k1gInPc1	plod
osvěty	osvěta	k1gFnSc2	osvěta
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vláda	vláda	k1gFnSc1	vláda
tmy	tma	k1gFnSc2	tma
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Živá	živý	k2eAgFnSc1d1	živá
mrtvola	mrtvola	k1gFnSc1	mrtvola
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Turgeněv	Turgeněv	k1gMnSc1	Turgeněv
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc1	první
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Michail	Michail	k1gMnSc1	Michail
Zoščenko	Zoščenka	k1gFnSc5	Zoščenka
<g/>
:	:	kIx,	:
Ironické	ironický	k2eAgFnPc1d1	ironická
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zločin	zločin	k1gInSc1	zločin
a	a	k8xC	a
trest	trest	k1gInSc1	trest
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vážený	vážený	k2eAgMnSc1d1	vážený
soudruh	soudruh	k1gMnSc1	soudruh
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Pozůstalost	pozůstalost	k1gFnSc1	pozůstalost
po	po	k7c6	po
Vincenci	Vincenc	k1gMnSc6	Vincenc
Červinkovi	Červinka	k1gMnSc6	Červinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
–	–	k?	–
biografie	biografie	k1gFnSc1	biografie
1	[number]	k4	1
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vykoupil	vykoupit	k5eAaPmAgMnS	vykoupit
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
;	;	kIx,	;
Ecce	Ecce	k1gFnSc6	Ecce
Homo	Homo	k1gNnSc4	Homo
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc4d1	český
rozhlas	rozhlas	k1gInSc4	rozhlas
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jirsová	Jirsová	k1gFnSc1	Jirsová
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
<g/>
;	;	kIx,	;
Vincenc	Vincenc	k1gMnSc1	Vincenc
Červinka	Červinka	k1gMnSc1	Červinka
–	–	k?	–
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
;	;	kIx,	;
Vyžlovka	Vyžlovka	k1gFnSc1	Vyžlovka
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
212	[number]	k4	212
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
11	[number]	k4	11
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Čern	černo	k1gNnPc2	černo
<g/>
–	–	k?	–
<g/>
Čž	Čž	k1gFnSc2	Čž
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
104	[number]	k4	104
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
368	[number]	k4	368
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
68	[number]	k4	68
<g/>
–	–	k?	–
<g/>
69	[number]	k4	69
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vincenc	Vincenc	k1gMnSc1	Vincenc
Červinka	červinka	k1gFnSc1	červinka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Vincenc	Vincenc	k1gMnSc1	Vincenc
Červinka	Červinka	k1gMnSc1	Červinka
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vincenc	Vincenc	k1gMnSc1	Vincenc
Červinka	Červinka	k1gMnSc1	Červinka
</s>
</p>
<p>
<s>
Lóže	lóže	k1gFnSc1	lóže
Humanizmus	humanizmus	k1gInSc1	humanizmus
-	-	kIx~	-
Dějiny	dějiny	k1gFnPc1	dějiny
svobodného	svobodný	k2eAgNnSc2d1	svobodné
zednářství	zednářství	k1gNnSc2	zednářství
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Online	Onlin	k1gInSc5	Onlin
Computer	computer	k1gInSc1	computer
Library	Librar	k1gInPc4	Librar
Center	centrum	k1gNnPc2	centrum
-	-	kIx~	-
Vincenc	Vincenc	k1gMnSc1	Vincenc
Červinka	Červinka	k1gMnSc1	Červinka
v	v	k7c6	v
metakatalogu	metakatalog	k1gInSc6	metakatalog
WorldCat	WorldCat	k1gInSc1	WorldCat
</s>
</p>
