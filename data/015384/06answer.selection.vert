<s desamb="1">
Na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
je	být	k5eAaImIp3nS
ohraničeno	ohraničit	k5eAaPmNgNnS
plážemi	pláž	k1gFnPc7
Gibraltarské	gibraltarský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
plážemi	pláž	k1gFnPc7
Alboránského	Alboránský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
jinak	jinak	k6eAd1
svou	svůj	k3xOyFgFnSc7
severní	severní	k2eAgFnSc7d1
částí	část	k1gFnSc7
hraničí	hraničit	k5eAaImIp3nS
s	s	k7c7
městem	město	k1gNnSc7
San	San	k1gMnSc2
Rogue	Rogu	k1gMnSc2
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
bylo	být	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1870	#num#	k4
součástí	součást	k1gFnPc2
<g/>
.	.	kIx.
</s>