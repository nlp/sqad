<s>
La	la	k1gNnSc1
Línea	Líne	k1gInSc2
de	de	k?
la	la	k1gNnSc1
Concepción	Concepción	k1gMnSc1
</s>
<s>
La	la	k1gNnSc1
Línea	Líne	k1gInSc2
de	de	k?
la	la	k1gNnSc1
Concepción	Concepción	k1gMnSc1
Línea	Líneus	k1gMnSc2
de	de	k?
la	la	k1gNnSc1
Concepción	Concepción	k1gMnSc1
<g/>
,	,	kIx,
La	la	k1gNnSc1
Pohled	pohled	k1gInSc1
z	z	k7c2
Gibraltarské	gibraltarský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
přes	přes	k7c4
mezinárodní	mezinárodní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
Gibraltar	Gibraltar	k1gInSc4
na	na	k7c4
La	la	k1gNnSc4
Línea	Líne	k1gInSc2
de	de	k?
la	la	k1gNnSc1
Concepción	Concepción	k1gMnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
36	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
<g/>
40	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
55	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
5	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
SEČ	SEČ	kA
<g/>
/	/	kIx~
<g/>
SELČ	SELČ	kA
Stát	stát	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc4
Autonomní	autonomní	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
Provincie	provincie	k1gFnSc2
</s>
<s>
Cádiz	Cádiz	k1gInSc1
</s>
<s>
La	la	k1gNnSc1
Línea	Líne	k1gInSc2
de	de	k?
la	la	k1gNnSc1
Concepción	Concepción	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
19,27	19,27	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
63	#num#	k4
630	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
3	#num#	k4
302	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
José	José	k6eAd1
Juan	Juan	k1gMnSc1
Franco	Franco	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2015	#num#	k4
<g/>
)	)	kIx)
Vznik	vznik	k1gInSc1
</s>
<s>
1870	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.lalinea.es	www.lalinea.es	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
+	+	kIx~
<g/>
34	#num#	k4
<g/>
)	)	kIx)
956	#num#	k4
ó	ó	k0
856	#num#	k4
PSČ	PSČ	kA
</s>
<s>
11300	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
CA	ca	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
La	la	k1gNnSc1
Línea	Líne	k1gInSc2
de	de	k?
la	la	k1gNnSc1
Concepción	Concepción	k1gMnSc1
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
ve	v	k7c6
španělské	španělský	k2eAgFnSc6d1
autonomní	autonomní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Andalusie	Andalusie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
provincii	provincie	k1gFnSc6
Cádiz	Cádiz	k1gInSc1
na	na	k7c4
šíji	šíje	k1gFnSc4
spojující	spojující	k2eAgFnSc4d1
pevninu	pevnina	k1gFnSc4
s	s	k7c7
Gibraltarem	Gibraltar	k1gInSc7
<g/>
,	,	kIx,
s	s	k7c7
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
je	být	k5eAaImIp3nS
ekonomicky	ekonomicky	k6eAd1
i	i	k9
kulturně	kulturně	k6eAd1
provázáno	provázán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
je	být	k5eAaImIp3nS
ohraničeno	ohraničit	k5eAaPmNgNnS
plážemi	pláž	k1gFnPc7
Gibraltarské	gibraltarský	k2eAgFnSc2d1
zátoky	zátoka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
plážemi	pláž	k1gFnPc7
Alboránského	Alboránský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
jinak	jinak	k6eAd1
svou	svůj	k3xOyFgFnSc7
severní	severní	k2eAgFnSc7d1
částí	část	k1gFnSc7
hraničí	hraničit	k5eAaImIp3nS
s	s	k7c7
městem	město	k1gNnSc7
San	San	k1gMnSc2
Rogue	Rogu	k1gMnSc2
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
bylo	být	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1870	#num#	k4
součástí	součást	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
bezmála	bezmála	k6eAd1
dvacet	dvacet	k4xCc4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
skoro	skoro	k6eAd1
65	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Když	když	k8xS
v	v	k7c6
rámci	rámec	k1gInSc6
Války	válka	k1gFnSc2
o	o	k7c4
španělské	španělský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
dobyla	dobýt	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1704	#num#	k4
společná	společný	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
Anglie	Anglie	k1gFnSc2
a	a	k8xC
Holandska	Holandsko	k1gNnSc2
(	(	kIx(
<g/>
vracející	vracející	k2eAgFnSc2d1
se	se	k3xPyFc4
z	z	k7c2
neúspěšného	úspěšný	k2eNgNnSc2d1
dobývání	dobývání	k1gNnSc2
Barcelony	Barcelona	k1gFnSc2
do	do	k7c2
Lisabonu	Lisabon	k1gInSc2
<g/>
)	)	kIx)
Gibraltar	Gibraltar	k1gInSc1
<g/>
,	,	kIx,
odešlo	odejít	k5eAaPmAgNnS
původní	původní	k2eAgNnSc4d1
španělské	španělský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
do	do	k7c2
nedalekého	daleký	k2eNgNnSc2d1
města	město	k1gNnSc2
San	San	k1gMnSc2
Roque	Roqu	k1gMnSc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
s	s	k7c7
nadějí	naděje	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
brzy	brzy	k6eAd1
vrátit	vrátit	k5eAaPmF
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utrechtský	Utrechtský	k2eAgInSc1d1
mír	mír	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1713	#num#	k4
ovšem	ovšem	k9
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vládu	vláda	k1gFnSc4
nad	nad	k7c7
Gibraltarem	Gibraltar	k1gInSc7
mají	mít	k5eAaImIp3nP
nadále	nadále	k6eAd1
uplatňovat	uplatňovat	k5eAaImF
Angličané	Angličan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělé	Španěl	k1gMnPc1
se	se	k3xPyFc4
s	s	k7c7
takovým	takový	k3xDgInSc7
vývojem	vývoj	k1gInSc7
nechtěli	chtít	k5eNaImAgMnP
smířit	smířit	k5eAaPmF
a	a	k8xC
král	král	k1gMnSc1
Filip	Filip	k1gMnSc1
V.	V.	kA
nechal	nechat	k5eAaPmAgMnS
pevnost	pevnost	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1727	#num#	k4
oblehnout	oblehnout	k5eAaPmF
v	v	k7c6
rámci	rámec	k1gInSc6
marné	marný	k2eAgFnSc2d1
snahy	snaha	k1gFnSc2
o	o	k7c4
její	její	k3xOp3gNnSc4
dobytí	dobytí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
dobytí	dobytí	k1gNnSc1
nezdařilo	zdařit	k5eNaPmAgNnS
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
vybudovat	vybudovat	k5eAaPmF
trvalé	trvalý	k2eAgNnSc4d1
hraniční	hraniční	k2eAgNnSc4d1
opevnění	opevnění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
by	by	kYmCp3nS
oddělilo	oddělit	k5eAaPmAgNnS
Gibraltar	Gibraltar	k1gInSc4
od	od	k7c2
pevniny	pevnina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavba	výstavba	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1731	#num#	k4
až	až	k9
1735	#num#	k4
a	a	k8xC
právě	právě	k6eAd1
nedaleká	daleký	k2eNgNnPc1d1
obydlí	obydlí	k1gNnPc1
rodin	rodina	k1gFnPc2
pracovníků	pracovník	k1gMnPc2
a	a	k8xC
dodavatelů	dodavatel	k1gMnPc2
představovala	představovat	k5eAaImAgFnS
první	první	k4xOgInSc4
zárodek	zárodek	k1gInSc4
budoucího	budoucí	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgInPc1d1
opevnění	opevněný	k2eAgMnPc1d1
přitom	přitom	k6eAd1
rozbořili	rozbořit	k5eAaPmAgMnP
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1810	#num#	k4
Britové	Brit	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
v	v	k7c6
rámci	rámec	k1gInSc6
Španělské	španělský	k2eAgFnSc2d1
války	válka	k1gFnSc2
o	o	k7c4
nezávislost	nezávislost	k1gFnSc4
stáli	stát	k5eAaImAgMnP
na	na	k7c6
straně	strana	k1gFnSc6
Španělů	Španěl	k1gMnPc2
proti	proti	k7c3
Napoleonovi	Napoleon	k1gMnSc3
Bonapartemu	Bonapartem	k1gMnSc3
a	a	k8xC
báli	bát	k5eAaImAgMnP
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
opevnění	opevnění	k1gNnSc1
využije	využít	k5eAaPmIp3nS
blížící	blížící	k2eAgMnSc1d1
se	se	k3xPyFc4
francouzské	francouzský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
po	po	k7c6
rozboření	rozboření	k1gNnSc6
opevnění	opevnění	k1gNnSc2
ovšem	ovšem	k9
město	město	k1gNnSc1
dále	daleko	k6eAd2
prosperovalo	prosperovat	k5eAaImAgNnS
jakožto	jakožto	k8xS
hraniční	hraniční	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
Gibraltaru	Gibraltar	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
zásobovalo	zásobovat	k5eAaImAgNnS
potravinami	potravina	k1gFnPc7
a	a	k8xC
naopak	naopak	k6eAd1
nabízelo	nabízet	k5eAaImAgNnS
jeho	jeho	k3xOp3gFnSc4
obyvatelům	obyvatel	k1gMnPc3
různé	různý	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouho	dlouho	k6eAd1
bylo	být	k5eAaImAgNnS
přímou	přímý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
města	město	k1gNnSc2
San	San	k1gFnPc4
Rogue	Rogu	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
obyvatelé	obyvatel	k1gMnPc1
se	se	k3xPyFc4
časem	časem	k6eAd1
začali	začít	k5eAaPmAgMnP
dožadovat	dožadovat	k5eAaImF
správní	správní	k2eAgFnSc2d1
samostatnosti	samostatnost	k1gFnSc2
a	a	k8xC
tak	tak	k6eAd1
se	s	k7c7
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1870	#num#	k4
stalo	stát	k5eAaPmAgNnS
samostatným	samostatný	k2eAgInSc7d1
správním	správní	k2eAgInSc7d1
celkem	celek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
zvolili	zvolit	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
občanů	občan	k1gMnPc2
prvního	první	k4xOgMnSc4
starostu	starosta	k1gMnSc4
a	a	k8xC
první	první	k4xOgNnSc1
zasedání	zasedání	k1gNnSc1
obecní	obecní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
jednomyslně	jednomyslně	k6eAd1
schválilo	schválit	k5eAaPmAgNnS
název	název	k1gInSc4
La	la	k1gNnSc2
Línea	Líne	k1gInSc2
de	de	k?
la	la	k1gNnSc1
Concepción	Concepción	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
král	král	k1gMnSc1
Alfons	Alfons	k1gMnSc1
XIII	XIII	kA
<g/>
.	.	kIx.
povýšil	povýšit	k5eAaPmAgMnS
obec	obec	k1gFnSc4
na	na	k7c4
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
zůstávalo	zůstávat	k5eAaImAgNnS
město	město	k1gNnSc4
těsně	těsně	k6eAd1
provázané	provázaný	k2eAgFnSc2d1
s	s	k7c7
nedalekým	daleký	k2eNgInSc7d1
Gibraltarem	Gibraltar	k1gInSc7
a	a	k8xC
když	když	k8xS
Francisco	Francisco	k6eAd1
Franco	Franco	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1969	#num#	k4
jako	jako	k8xS,k8xC
odpověď	odpověď	k1gFnSc4
na	na	k7c4
novou	nový	k2eAgFnSc4d1
gibraltarskou	gibraltarský	k2eAgFnSc4d1
ústavu	ústava	k1gFnSc4
nařídil	nařídit	k5eAaPmAgMnS
uzavřít	uzavřít	k5eAaPmF
hranici	hranice	k1gFnSc4
<g/>
,	,	kIx,
tisíce	tisíc	k4xCgInPc1
místních	místní	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
tím	ten	k3xDgNnSc7
přišly	přijít	k5eAaPmAgFnP
o	o	k7c4
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
Pevnosti	pevnost	k1gFnPc1
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
opevnění	opevnění	k1gNnSc2
budovaného	budovaný	k2eAgNnSc2d1
v	v	k7c6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
zničeného	zničený	k2eAgNnSc2d1
na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byly	být	k5eAaImAgInP
dvě	dva	k4xCgFnPc4
velké	velký	k2eAgFnPc4d1
pevnosti	pevnost	k1gFnPc4
<g/>
,	,	kIx,
Santa	Santa	k1gFnSc1
Bárbara	Bárbara	k1gFnSc1
na	na	k7c6
východním	východní	k2eAgInSc6d1
konci	konec	k1gInSc6
a	a	k8xC
San	San	k1gFnSc6
Felipe	Felip	k1gInSc5
na	na	k7c6
západním	západní	k2eAgInSc6d1
konci	konec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
z	z	k7c2
pevnosti	pevnost	k1gFnSc2
San	San	k1gFnSc2
Felipe	Felip	k1gInSc5
mnoho	mnoho	k4c1
nezbylo	zbýt	k5eNaPmAgNnS
<g/>
,	,	kIx,
rozvaliny	rozvalina	k1gFnSc2
pevnosti	pevnost	k1gFnSc2
Santa	Sant	k1gMnSc2
Bárbara	Bárbar	k1gMnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
si	se	k3xPyFc3
prohlédnout	prohlédnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Muzeum	muzeum	k1gNnSc1
gibraltarské	gibraltarský	k2eAgFnSc2d1
šíje	šíj	k1gFnSc2
</s>
<s>
El	Ela	k1gFnPc2
Museo	Museo	k1gMnSc1
del	del	k?
Istmo	Istma	k1gFnSc5
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
nejstarší	starý	k2eAgFnSc6d3
zachovalé	zachovalý	k2eAgFnSc6d1
budově	budova	k1gFnSc6
města	město	k1gNnSc2
<g/>
:	:	kIx,
bývalém	bývalý	k2eAgNnSc6d1
vojenském	vojenský	k2eAgNnSc6d1
velitelství	velitelství	k1gNnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
nejstarší	starý	k2eAgFnPc1d3
části	část	k1gFnPc1
byly	být	k5eAaImAgFnP
postaveny	postavit	k5eAaPmNgFnP
v	v	k7c6
letech	léto	k1gNnPc6
1863	#num#	k4
<g/>
-	-	kIx~
<g/>
1865	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Býčí	býčí	k2eAgFnSc1d1
aréna	aréna	k1gFnSc1
</s>
<s>
Stadion	stadion	k1gInSc1
pro	pro	k7c4
býčí	býčí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
pro	pro	k7c4
6000	#num#	k4
diváků	divák	k1gMnPc2
byl	být	k5eAaImAgInS
postavený	postavený	k2eAgMnSc1d1
v	v	k7c6
letech	léto	k1gNnPc6
1880-1883	1880-1883	k4
a	a	k8xC
je	být	k5eAaImIp3nS
dobrou	dobrý	k2eAgFnSc7d1
ukázkou	ukázka	k1gFnSc7
místní	místní	k2eAgFnSc2d1
dobové	dobový	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Socha	Socha	k1gMnSc1
Camaróno	Camaróen	k2eAgNnSc1d1
de	de	k?
la	la	k1gNnSc1
Isla	Isl	k1gInSc2
</s>
<s>
Socha	Socha	k1gMnSc1
zpěváka	zpěvák	k1gMnSc2
Camaróno	Camaróno	k1gNnSc4
de	de	k?
la	la	k1gNnSc2
Isla	Islus	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zde	zde	k6eAd1
strávil	strávit	k5eAaPmAgInS
část	část	k1gFnSc4
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořil	vytvořit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
Nacho	Nac	k1gMnSc4
Falgueras	Falgueras	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
La	la	k1gNnSc2
Línea	Líne	k1gInSc2
de	de	k?
la	la	k1gNnSc1
Concepción	Concepción	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Národní	národní	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
:	:	kIx,
Padrón	Padrón	k1gInSc1
municipal	municipat	k5eAaImAgInS,k5eAaPmAgInS
de	de	k?
Españ	Españ	k1gInSc2
de	de	k?
2020	#num#	k4
<g/>
.	.	kIx.
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
La	la	k1gNnSc2
Línea	Líne	k1gInSc2
de	de	k?
la	la	k1gNnSc2
Concepción	Concepción	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obce	obec	k1gFnPc1
v	v	k7c6
provincii	provincie	k1gFnSc6
Cádiz	Cádiz	k1gInSc4
</s>
<s>
Alcalá	Alcalý	k2eAgFnSc1d1
de	de	k?
los	los	k1gInSc1
Gazules	Gazules	k1gInSc1
•	•	k?
Alcalá	Alcalý	k2eAgFnSc1d1
del	del	k?
Valle	Valle	k1gFnSc1
•	•	k?
Algar	Algar	k1gMnSc1
•	•	k?
Algeciras	Algeciras	k1gMnSc1
•	•	k?
Algodonales	Algodonales	k1gMnSc1
•	•	k?
Arcos	Arcos	k1gMnSc1
de	de	k?
la	la	k1gNnSc2
Frontera	Fronter	k1gMnSc2
•	•	k?
Barbate	Barbat	k1gInSc5
•	•	k?
Benalup-Casas	Benalup-Casas	k1gMnSc1
Viejas	Viejas	k1gMnSc1
•	•	k?
Benamahoma	Benamahoma	k1gFnSc1
•	•	k?
Benaocaz	Benaocaz	k1gInSc1
•	•	k?
Bornos	Bornos	k1gInSc1
•	•	k?
Cádiz	Cádiz	k1gInSc1
•	•	k?
Castellar	Castellar	k1gInSc1
de	de	k?
la	la	k1gNnSc2
Frontera	Fronter	k1gMnSc2
•	•	k?
Chiclana	Chiclan	k1gMnSc2
de	de	k?
la	la	k1gNnSc1
Frontera	Fronter	k1gMnSc2
•	•	k?
Chipiona	Chipion	k1gMnSc2
•	•	k?
Conil	Conil	k1gInSc1
de	de	k?
la	la	k1gNnSc2
Frontera	Fronter	k1gMnSc2
•	•	k?
El	Ela	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
Bosque	Bosque	k1gFnSc1
•	•	k?
El	Ela	k1gFnPc2
Gastor	Gastor	k1gMnSc1
•	•	k?
El	Ela	k1gFnPc2
Puerto	Puerta	k1gFnSc5
de	de	k?
Santa	Sant	k1gMnSc2
María	Maríus	k1gMnSc2
•	•	k?
Espera	Esper	k1gMnSc2
•	•	k?
Grazalema	Grazalem	k1gMnSc2
•	•	k?
Jerez	Jerez	k1gInSc1
de	de	k?
la	la	k1gNnSc2
Frontera	Fronter	k1gMnSc2
•	•	k?
Jimena	Jimen	k1gMnSc2
de	de	k?
la	la	k1gNnSc1
Frontera	Fronter	k1gMnSc2
•	•	k?
La	la	k1gNnSc1
Línea	Línea	k1gMnSc1
de	de	k?
la	la	k1gNnPc2
Concepción	Concepción	k1gInSc1
•	•	k?
Los	los	k1gInSc1
Barrios	Barrios	k1gInSc1
•	•	k?
Medina-Sidonia	Medina-Sidonium	k1gNnSc2
•	•	k?
Olvera	Olvero	k1gNnSc2
•	•	k?
Paterna	Paterna	k1gFnSc1
de	de	k?
Rivera	Rivera	k1gFnSc1
•	•	k?
Prado	Prado	k1gNnSc1
del	del	k?
Rey	Rea	k1gFnSc2
•	•	k?
Puerto	Puerta	k1gFnSc5
Real	Real	k1gInSc1
•	•	k?
Puerto	Puerta	k1gFnSc5
Serrano	Serrana	k1gFnSc5
•	•	k?
Rota	rota	k1gFnSc1
•	•	k?
San	San	k1gFnSc1
Fernando	Fernanda	k1gFnSc5
•	•	k?
San	San	k1gMnPc1
José	Josý	k2eAgFnSc2d1
del	del	k?
Valle	Valle	k1gFnSc2
•	•	k?
San	San	k1gMnSc1
Roque	Roqu	k1gFnSc2
•	•	k?
Sanlúcar	Sanlúcar	k1gMnSc1
de	de	k?
Barrameda	Barrameda	k1gMnSc1
•	•	k?
Setenil	Setenil	k1gMnSc1
de	de	k?
las	laso	k1gNnPc2
Bodegas	Bodegas	k1gInSc1
•	•	k?
Tarifa	tarifa	k1gFnSc1
•	•	k?
Torre	torr	k1gInSc5
Alháquime	Alháquim	k1gInSc5
•	•	k?
Trebujena	Trebujen	k2eAgFnSc1d1
•	•	k?
Ubrique	Ubrique	k1gFnSc1
•	•	k?
Vejer	Vejer	k1gInSc1
de	de	k?
la	la	k1gNnSc2
Frontera	Fronter	k1gMnSc2
•	•	k?
Villaluenga	Villalueng	k1gMnSc2
del	del	k?
Rosario	Rosario	k1gMnSc1
•	•	k?
Villamartín	Villamartín	k1gMnSc1
•	•	k?
Zahara	Zahara	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Sierra	Sierra	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1038544130	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
78073103	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
137750398	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
78073103	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
