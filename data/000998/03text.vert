<s>
Teplice	Teplice	k1gFnPc1	Teplice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Teplitz	Teplitz	k1gInSc1	Teplitz
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Teplitz-Schönau	Teplitz-Schönaa	k1gFnSc4	Teplitz-Schönaa
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
Teplice-Šanov	Teplice-Šanovo	k1gNnPc2	Teplice-Šanovo
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
lázeňské	lázeňská	k1gFnPc1	lázeňská
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
15	[number]	k4	15
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
v	v	k7c6	v
široké	široký	k2eAgFnSc6d1	široká
kotlině	kotlina	k1gFnSc6	kotlina
mezi	mezi	k7c7	mezi
Krušnými	krušný	k2eAgFnPc7d1	krušná
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
Českým	český	k2eAgNnSc7d1	české
středohořím	středohoří	k1gNnSc7	středohoří
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
zdaleka	zdaleka	k6eAd1	zdaleka
je	být	k5eAaImIp3nS	být
Doubravská	Doubravský	k2eAgFnSc1d1	Doubravská
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Teplice	Teplice	k1gFnPc1	Teplice
mají	mít	k5eAaImIp3nP	mít
okolo	okolo	k7c2	okolo
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgNnSc4d1	známé
jako	jako	k8xS	jako
lázeňské	lázeňský	k2eAgNnSc4d1	lázeňské
město	město	k1gNnSc4	město
i	i	k8xC	i
svým	svůj	k3xOyFgInSc7	svůj
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
klubem	klub	k1gInSc7	klub
FK	FK	kA	FK
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
městu	město	k1gNnSc3	město
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
vysokou	vysoký	k2eAgFnSc4d1	vysoká
kulturní	kulturní	k2eAgFnSc4d1	kulturní
úroveň	úroveň	k1gFnSc4	úroveň
i	i	k8xC	i
klasickou	klasický	k2eAgFnSc4d1	klasická
architekturu	architektura	k1gFnSc4	architektura
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
"	"	kIx"	"
<g/>
Malá	malý	k2eAgFnSc1d1	malá
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Archeologické	archeologický	k2eAgFnPc1d1	archeologická
vykopávky	vykopávka	k1gFnPc1	vykopávka
dosvědčují	dosvědčovat	k5eAaImIp3nP	dosvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
sídlili	sídlit	k5eAaImAgMnP	sídlit
pravěcí	pravěký	k2eAgMnPc1d1	pravěký
lovci	lovec	k1gMnPc1	lovec
před	před	k7c7	před
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
tisíci	tisíc	k4xCgInPc7	tisíc
roky	rok	k1gInPc7	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
sídlili	sídlit	k5eAaImAgMnP	sídlit
Keltové	Kelt	k1gMnPc1	Kelt
a	a	k8xC	a
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
Markomanů	Markoman	k1gMnPc2	Markoman
a	a	k8xC	a
Kvádů	Kvád	k1gMnPc2	Kvád
<g/>
.	.	kIx.	.
</s>
<s>
Termální	termální	k2eAgInPc1d1	termální
prameny	pramen	k1gInPc1	pramen
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
podle	podle	k7c2	podle
Hájkovy	Hájkův	k2eAgFnSc2d1	Hájkova
kroniky	kronika	k1gFnSc2	kronika
objeveny	objevit	k5eAaPmNgInP	objevit
roku	rok	k1gInSc2	rok
762	[number]	k4	762
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
památka	památka	k1gFnSc1	památka
Pravřídlo	Pravřídlo	k1gFnPc4	Pravřídlo
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
první	první	k4xOgFnSc1	první
důvěryhodná	důvěryhodný	k2eAgFnSc1d1	důvěryhodná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
Trnovany	Trnovan	k1gMnPc4	Trnovan
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1057	[number]	k4	1057
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
pak	pak	k6eAd1	pak
z	z	k7c2	z
konce	konec	k1gInSc2	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
starší	starý	k2eAgFnSc2d2	starší
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
vesnice	vesnice	k1gFnSc2	vesnice
založila	založit	k5eAaPmAgFnS	založit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1160	[number]	k4	1160
královna	královna	k1gFnSc1	královna
Judita	Judita	k1gFnSc1	Judita
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
klášter	klášter	k1gInSc4	klášter
benediktinek	benediktinka	k1gFnPc2	benediktinka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zhruba	zhruba	k6eAd1	zhruba
stovce	stovka	k1gFnSc6	stovka
let	léto	k1gNnPc2	léto
vzniká	vznikat	k5eAaImIp3nS	vznikat
opevněné	opevněný	k2eAgNnSc1d1	opevněné
gotické	gotický	k2eAgNnSc1d1	gotické
město	město	k1gNnSc1	město
na	na	k7c6	na
obdélném	obdélný	k2eAgInSc6d1	obdélný
půdoryse	půdorys	k1gInSc6	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Teplicemi	Teplice	k1gFnPc7	Teplice
procházela	procházet	k5eAaImAgFnS	procházet
významná	významný	k2eAgFnSc1d1	významná
obchodní	obchodní	k2eAgFnSc1d1	obchodní
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
nebo	nebo	k8xC	nebo
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
roku	rok	k1gInSc2	rok
1426	[number]	k4	1426
a	a	k8xC	a
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1436	[number]	k4	1436
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
klášterní	klášterní	k2eAgFnPc1d1	klášterní
budovy	budova	k1gFnPc1	budova
stávají	stávat	k5eAaImIp3nP	stávat
majetkem	majetek	k1gInSc7	majetek
Jakoubka	jakoubka	k1gFnSc1	jakoubka
z	z	k7c2	z
Vřesovic	Vřesovice	k1gFnPc2	Vřesovice
<g/>
.	.	kIx.	.
</s>
<s>
Vrchnost	vrchnost	k1gFnSc1	vrchnost
se	se	k3xPyFc4	se
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
často	často	k6eAd1	často
střídala	střídat	k5eAaImAgFnS	střídat
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kláštera	klášter	k1gInSc2	klášter
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
lázeňství	lázeňství	k1gNnSc1	lázeňství
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
český	český	k2eAgMnSc1d1	český
majitel	majitel	k1gMnSc1	majitel
Teplic	Teplice	k1gFnPc2	Teplice
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
Vchynský	Vchynský	k1gMnSc1	Vchynský
ze	z	k7c2	z
Vchynic	Vchynice	k1gFnPc2	Vchynice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Albrechtem	Albrecht	k1gMnSc7	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1634	[number]	k4	1634
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
bylo	být	k5eAaImAgNnS	být
zkonfiskováno	zkonfiskovat	k5eAaPmNgNnS	zkonfiskovat
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
věnováno	věnovat	k5eAaPmNgNnS	věnovat
Janovi	Jan	k1gMnSc6	Jan
z	z	k7c2	z
Aldringenu	Aldringen	k1gInSc2	Aldringen
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
umřel	umřít	k5eAaPmAgMnS	umřít
za	za	k7c2	za
záhadných	záhadný	k2eAgFnPc2d1	záhadná
okolností	okolnost	k1gFnPc2	okolnost
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
panství	panství	k1gNnSc4	panství
spatřil	spatřit	k5eAaPmAgMnS	spatřit
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc4	panství
zdědila	zdědit	k5eAaPmAgFnS	zdědit
Janova	Janův	k2eAgFnSc1d1	Janova
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
věnem	věno	k1gNnSc7	věno
přinesla	přinést	k5eAaPmAgFnS	přinést
Jeronýmu	Jeroným	k1gMnSc3	Jeroným
Clarymu	Clarym	k1gInSc6	Clarym
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
vybité	vybitý	k2eAgNnSc1d1	vybité
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Teplic	Teplice	k1gFnPc2	Teplice
doplňováno	doplňovat	k5eAaImNgNnS	doplňovat
přísunem	přísun	k1gInSc7	přísun
německých	německý	k2eAgMnPc2d1	německý
kolonistů	kolonista	k1gMnPc2	kolonista
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
potomci	potomek	k1gMnPc1	potomek
Teplice	teplice	k1gFnSc2	teplice
obývali	obývat	k5eAaImAgMnP	obývat
až	až	k9	až
do	do	k7c2	do
odsunu	odsun	k1gInSc2	odsun
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1680	[number]	k4	1680
pocházejí	pocházet	k5eAaImIp3nP	pocházet
první	první	k4xOgInPc1	první
seznamy	seznam	k1gInPc1	seznam
lázeňských	lázeňský	k2eAgMnPc2d1	lázeňský
hostů	host	k1gMnPc2	host
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Claryové	Claryus	k1gMnPc1	Claryus
přestavili	přestavit	k5eAaPmAgMnP	přestavit
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1718	[number]	k4	1718
také	také	k6eAd1	také
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
barokní	barokní	k2eAgInSc4d1	barokní
sloup	sloup	k1gInSc4	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
(	(	kIx(	(
<g/>
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
<g/>
)	)	kIx)	)
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Matyáše	Matyáš	k1gMnSc2	Matyáš
Bernarda	Bernard	k1gMnSc2	Bernard
Brauna	Braun	k1gMnSc2	Braun
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
Zámeckém	zámecký	k2eAgNnSc6d1	zámecké
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
průmysl	průmysl	k1gInSc1	průmysl
<g/>
:	:	kIx,	:
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1742	[number]	k4	1742
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Teplic	Teplice	k1gFnPc2	Teplice
první	první	k4xOgInPc4	první
hnědouhelné	hnědouhelný	k2eAgInPc4d1	hnědouhelný
doly	dol	k1gInPc4	dol
a	a	k8xC	a
vzkvétala	vzkvétat	k5eAaImAgFnS	vzkvétat
také	také	k9	také
výroba	výroba	k1gFnSc1	výroba
punčoch	punčocha	k1gFnPc2	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
Teplice	Teplice	k1gFnPc1	Teplice
podlehly	podlehnout	k5eAaPmAgFnP	podlehnout
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
požáru	požár	k1gInSc2	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
<g/>
,	,	kIx,	,
obnovovány	obnovován	k2eAgFnPc1d1	obnovována
byly	být	k5eAaImAgFnP	být
již	již	k9	již
v	v	k7c6	v
klasicistním	klasicistní	k2eAgInSc6d1	klasicistní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
se	se	k3xPyFc4	se
v	v	k7c6	v
Zámecké	zámecký	k2eAgFnSc6d1	zámecká
zahradě	zahrada	k1gFnSc6	zahrada
setkali	setkat	k5eAaPmAgMnP	setkat
Ludwig	Ludwig	k1gInSc4	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
s	s	k7c7	s
Johannem	Johann	k1gMnSc7	Johann
Wolfgangem	Wolfgang	k1gMnSc7	Wolfgang
Goethem	Goeth	k1gInSc7	Goeth
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
první	první	k4xOgNnSc1	první
setkání	setkání	k1gNnSc1	setkání
bylo	být	k5eAaImAgNnS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
jejich	jejich	k3xOp3gMnPc1	jejich
poslední	poslední	k2eAgMnPc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Teplice	Teplice	k1gFnPc1	Teplice
byly	být	k5eAaImAgFnP	být
místem	místem	k6eAd1	místem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1813	[number]	k4	1813
panovníci	panovník	k1gMnPc1	panovník
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Pruska	Prusko	k1gNnSc2	Prusko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
podepsali	podepsat	k5eAaPmAgMnP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
proti	proti	k7c3	proti
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byly	být	k5eAaImAgFnP	být
Teplice	Teplice	k1gFnPc1	Teplice
připojeny	připojit	k5eAaPmNgFnP	připojit
jakožto	jakožto	k8xS	jakožto
součást	součást	k1gFnSc1	součást
Sudet	Sudety	k1gFnPc2	Sudety
k	k	k7c3	k
Třetí	třetí	k4xOgFnSc3	třetí
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Teplice	Teplice	k1gFnPc1	Teplice
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
189	[number]	k4	189
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
jihovýchodní	jihovýchodní	k2eAgInSc4d1	jihovýchodní
okraj	okraj	k1gInSc4	okraj
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
až	až	k9	až
399	[number]	k4	399
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
vrchol	vrchol	k1gInSc1	vrchol
Doubravské	Doubravský	k2eAgFnSc2d1	Doubravská
hory	hora	k1gFnSc2	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
teplé	teplý	k2eAgFnSc6d1	teplá
klimatické	klimatický	k2eAgFnSc6d1	klimatická
oblasti	oblast	k1gFnSc6	oblast
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
lednová	lednový	k2eAgFnSc1d1	lednová
teplota	teplota	k1gFnSc1	teplota
za	za	k7c4	za
roky	rok	k1gInPc4	rok
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
činí	činit	k5eAaImIp3nS	činit
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
červencová	červencový	k2eAgFnSc1d1	červencová
pak	pak	k6eAd1	pak
18,2	[number]	k4	18,2
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
563	[number]	k4	563
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
teplotní	teplotní	k2eAgFnPc4d1	teplotní
inverze	inverze	k1gFnPc4	inverze
spojené	spojený	k2eAgFnPc4d1	spojená
se	s	k7c7	s
zhoršenou	zhoršený	k2eAgFnSc7d1	zhoršená
kvalitou	kvalita	k1gFnSc7	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
ovzduší	ovzduší	k1gNnSc2	ovzduší
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
však	však	k9	však
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
postupně	postupně	k6eAd1	postupně
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Podkrušnohorské	podkrušnohorský	k2eAgFnSc6d1	Podkrušnohorská
pánvi	pánev	k1gFnSc6	pánev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
žilo	žít	k5eAaImAgNnS	žít
2	[number]	k4	2
130	[number]	k4	130
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
již	již	k9	již
3	[number]	k4	3
584	[number]	k4	584
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
nárůst	nárůst	k1gInSc1	nárůst
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
díky	díky	k7c3	díky
rychlému	rychlý	k2eAgInSc3d1	rychlý
rozvoji	rozvoj	k1gInSc3	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
přistěhovalectví	přistěhovalectví	k1gNnSc2	přistěhovalectví
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byli	být	k5eAaImAgMnP	být
převažující	převažující	k2eAgMnPc1d1	převažující
Němci	Němec	k1gMnPc1	Němec
odsunuti	odsunut	k2eAgMnPc1d1	odsunut
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
Češi	česat	k5eAaImIp1nS	česat
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
tvořili	tvořit	k5eAaImAgMnP	tvořit
91,2	[number]	k4	91,2
%	%	kIx~	%
trvale	trvale	k6eAd1	trvale
bydlících	bydlící	k2eAgInPc2d1	bydlící
<g/>
,	,	kIx,	,
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
12,9	[number]	k4	12,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Počty	počet	k1gInPc4	počet
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
i	i	k8xC	i
poválečné	poválečný	k2eAgNnSc4d1	poválečné
administrativní	administrativní	k2eAgNnSc4d1	administrativní
slučování	slučování	k1gNnSc4	slučování
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
počtu	počet	k1gInSc2	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
způsoben	způsoben	k2eAgMnSc1d1	způsoben
i	i	k9	i
špatným	špatný	k2eAgNnSc7d1	špatné
životním	životní	k2eAgNnSc7d1	životní
prostředím	prostředí	k1gNnSc7	prostředí
(	(	kIx(	(
<g/>
exhalace	exhalace	k1gFnSc2	exhalace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
604	[number]	k4	604
domech	dům	k1gInPc6	dům
28	[number]	k4	28
892	[number]	k4	892
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
815	[number]	k4	815
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
4	[number]	k4	4
406	[number]	k4	406
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
22	[number]	k4	22
489	[number]	k4	489
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
396	[number]	k4	396
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
22	[number]	k4	22
561	[number]	k4	561
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
1	[number]	k4	1
909	[number]	k4	909
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
59	[number]	k4	59
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
3	[number]	k4	3
128	[number]	k4	128
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
900	[number]	k4	900
domech	dům	k1gInPc6	dům
30	[number]	k4	30
799	[number]	k4	799
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
5	[number]	k4	5
232	[number]	k4	232
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
23	[number]	k4	23
127	[number]	k4	127
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
22	[number]	k4	22
942	[number]	k4	942
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
2	[number]	k4	2
089	[number]	k4	089
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
414	[number]	k4	414
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
3	[number]	k4	3
213	[number]	k4	213
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
stoletích	století	k1gNnPc6	století
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žila	žít	k5eAaImAgFnS	žít
velmi	velmi	k6eAd1	velmi
početná	početný	k2eAgFnSc1d1	početná
a	a	k8xC	a
hospodářsky	hospodářsky	k6eAd1	hospodářsky
důležitá	důležitý	k2eAgFnSc1d1	důležitá
židovská	židovský	k2eAgFnSc1d1	židovská
menšina	menšina	k1gFnSc1	menšina
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1414	[number]	k4	1414
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
druhá	druhý	k4xOgFnSc1	druhý
nejstarší	starý	k2eAgFnSc1d3	nejstarší
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
hned	hned	k6eAd1	hned
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
jak	jak	k6eAd1	jak
svou	svůj	k3xOyFgFnSc4	svůj
vymezenou	vymezený	k2eAgFnSc4d1	vymezená
část	část	k1gFnSc4	část
-	-	kIx~	-
ghetto	ghetto	k1gNnSc4	ghetto
<g/>
,	,	kIx,	,
tak	tak	k9	tak
svou	svůj	k3xOyFgFnSc4	svůj
Starou	starý	k2eAgFnSc4d1	stará
synagogu	synagoga	k1gFnSc4	synagoga
<g/>
,	,	kIx,	,
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
rituální	rituální	k2eAgFnPc1d1	rituální
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
košer	košer	k6eAd1	košer
masné	masný	k2eAgInPc4d1	masný
krámy	krám	k1gInPc4	krám
<g/>
,	,	kIx,	,
sirotčinec	sirotčinec	k1gInSc4	sirotčinec
<g/>
,	,	kIx,	,
chudobinec	chudobinec	k1gInSc4	chudobinec
<g/>
,	,	kIx,	,
nemocnici	nemocnice	k1gFnSc4	nemocnice
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
charitativní	charitativní	k2eAgFnPc1d1	charitativní
organizace	organizace	k1gFnPc1	organizace
a	a	k8xC	a
spolky	spolek	k1gInPc1	spolek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
půlce	půlka	k1gFnSc6	půlka
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
tepličtí	teplický	k2eAgMnPc1d1	teplický
Židé	Žid	k1gMnPc1	Žid
také	také	k6eAd1	také
Novou	nový	k2eAgFnSc4d1	nová
synagogu	synagoga	k1gFnSc4	synagoga
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
staveb	stavba	k1gFnPc2	stavba
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
symbolicky	symbolicky	k6eAd1	symbolicky
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
důležitost	důležitost	k1gFnSc4	důležitost
této	tento	k3xDgFnSc2	tento
minority	minorita	k1gFnSc2	minorita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
záboru	zábor	k1gInSc6	zábor
Sudet	Sudety	k1gFnPc2	Sudety
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
však	však	k9	však
byli	být	k5eAaImAgMnP	být
takřka	takřka	k6eAd1	takřka
všichni	všechen	k3xTgMnPc1	všechen
tepličtí	teplický	k2eAgMnPc1d1	teplický
Židé	Žid	k1gMnPc1	Žid
nuceni	nutit	k5eAaImNgMnP	nutit
uprchnout	uprchnout	k5eAaPmF	uprchnout
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
asi	asi	k9	asi
šest	šest	k4xCc4	šest
tisíc	tisíc	k4xCgInSc4	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1939	[number]	k4	1939
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
Nová	nový	k2eAgFnSc1d1	nová
synagoga	synagoga	k1gFnSc1	synagoga
nacisty	nacista	k1gMnPc7	nacista
vypálena	vypálen	k2eAgFnSc1d1	vypálena
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
teplických	teplický	k2eAgMnPc2d1	teplický
Židů	Žid	k1gMnPc2	Žid
však	však	k9	však
během	během	k7c2	během
války	válka	k1gFnSc2	válka
stejně	stejně	k6eAd1	stejně
zkáze	zkáza	k1gFnSc3	zkáza
neunikla	uniknout	k5eNaPmAgFnS	uniknout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
zbytku	zbytek	k1gInSc2	zbytek
ČSR	ČSR	kA	ČSR
byli	být	k5eAaImAgMnP	být
nejprve	nejprve	k6eAd1	nejprve
odvezeni	odvézt	k5eAaPmNgMnP	odvézt
do	do	k7c2	do
ghetta	ghetto	k1gNnSc2	ghetto
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
vyvražděno	vyvraždit	k5eAaPmNgNnS	vyvraždit
ve	v	k7c6	v
vyhlazovacích	vyhlazovací	k2eAgInPc6d1	vyhlazovací
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Zkázu	zkáza	k1gFnSc4	zkáza
židovských	židovský	k2eAgFnPc2d1	židovská
Teplic	Teplice	k1gFnPc2	Teplice
dokonalo	dokonat	k5eAaPmAgNnS	dokonat
spojenecké	spojenecký	k2eAgNnSc1d1	spojenecké
bombardování	bombardování	k1gNnSc1	bombardování
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bomby	bomba	k1gFnPc1	bomba
dopadly	dopadnout	k5eAaPmAgFnP	dopadnout
do	do	k7c2	do
prostor	prostora	k1gFnPc2	prostora
bývalého	bývalý	k2eAgNnSc2d1	bývalé
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
kompletně	kompletně	k6eAd1	kompletně
asanováno	asanován	k2eAgNnSc1d1	asanováno
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
malá	malý	k2eAgFnSc1d1	malá
židovská	židovská	k1gFnSc1	židovská
obec	obec	k1gFnSc1	obec
(	(	kIx(	(
<g/>
obnovena	obnoven	k2eAgFnSc1d1	obnovena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nemnoha	nemnoh	k1gMnSc2	nemnoh
členů	člen	k1gInPc2	člen
tvoří	tvořit	k5eAaImIp3nS	tvořit
právě	právě	k9	právě
přeživší	přeživší	k2eAgInSc1d1	přeživší
holocaustu	holocaust	k1gInSc3	holocaust
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
kulturně	kulturně	k6eAd1	kulturně
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
činná	činný	k2eAgFnSc1d1	činná
i	i	k8xC	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
židovské	židovský	k2eAgFnPc4d1	židovská
památky	památka	k1gFnPc4	památka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patří	patřit	k5eAaImIp3nP	patřit
nový	nový	k2eAgInSc4d1	nový
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
a	a	k8xC	a
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Sobědruhách	Sobědruha	k1gFnPc6	Sobědruha
-	-	kIx~	-
oba	dva	k4xCgMnPc1	dva
chráněné	chráněný	k2eAgFnPc4d1	chráněná
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnPc4d1	kulturní
památky	památka	k1gFnPc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Teplice	Teplice	k1gFnPc1	Teplice
centrum	centrum	k1gNnSc1	centrum
Šanov	Šanovo	k1gNnPc2	Šanovo
I	I	kA	I
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc1d1	starý
Šanov	Šanov	k1gInSc1	Šanov
<g/>
,	,	kIx,	,
Šanov	Šanov	k1gInSc1	Šanov
<g/>
,	,	kIx,	,
Lázně	lázeň	k1gFnPc1	lázeň
Šanov	Šanov	k1gInSc1	Šanov
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
centra	centrum	k1gNnSc2	centrum
<g/>
)	)	kIx)	)
Šanov	Šanov	k1gInSc1	Šanov
II	II	kA	II
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Šanov	Šanov	k1gInSc1	Šanov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
Šanova	Šanovo	k1gNnSc2	Šanovo
<g/>
,	,	kIx,	,
sídliště	sídliště	k1gNnSc1	sídliště
postavené	postavený	k2eAgNnSc1d1	postavené
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
libereckého	liberecký	k2eAgInSc2d1	liberecký
SIALu	sial	k1gInSc2	sial
Letná	Letná	k1gFnSc1	Letná
<g/>
,	,	kIx,	,
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
Šanova	Šanov	k1gInSc2	Šanov
Bílá	bílý	k2eAgFnSc1d1	bílá
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
sídliště	sídliště	k1gNnSc1	sídliště
u	u	k7c2	u
Nové	Nová	k1gFnSc2	Nová
Vsi	ves	k1gFnSc3	ves
Prosetice	Prosetika	k1gFnSc3	Prosetika
(	(	kIx(	(
<g/>
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
)	)	kIx)	)
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
(	(	kIx(	(
<g/>
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
)	)	kIx)	)
Řetenice	Řetenice	k1gFnSc1	Řetenice
(	(	kIx(	(
<g/>
západně	západně	k6eAd1	západně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
)	)	kIx)	)
Hudcov	Hudcov	k1gInSc1	Hudcov
(	(	kIx(	(
<g/>
nejzápadnější	západní	k2eAgFnSc1d3	nejzápadnější
část	část	k1gFnSc1	část
Teplic	Teplice	k1gFnPc2	Teplice
<g/>
)	)	kIx)	)
Trnovany	Trnovan	k1gMnPc4	Trnovan
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
usadil	usadit	k5eAaPmAgMnS	usadit
mohutný	mohutný	k2eAgInSc4d1	mohutný
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
keramický	keramický	k2eAgInSc1d1	keramický
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
samostatným	samostatný	k2eAgNnSc7d1	samostatné
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
sloučeny	sloučen	k2eAgInPc1d1	sloučen
s	s	k7c7	s
Teplicemi	Teplice	k1gFnPc7	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
probíhaly	probíhat	k5eAaImAgFnP	probíhat
plošné	plošný	k2eAgFnPc1d1	plošná
demolice	demolice	k1gFnPc1	demolice
staré	starý	k2eAgFnSc2d1	stará
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
místo	místo	k6eAd1	místo
panelovým	panelový	k2eAgNnPc3d1	panelové
sídlištím	sídliště	k1gNnPc3	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Anger	Anger	k1gInSc1	Anger
(	(	kIx(	(
<g/>
Angrovy	Angrův	k2eAgFnPc1d1	Angrova
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
Angerteich	Angerteich	k1gInSc1	Angerteich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Trnovan	Trnovan	k1gMnSc1	Trnovan
Sobědruhy	Sobědruha	k1gFnPc4	Sobědruha
Mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
lázeňské	lázeňský	k2eAgFnSc2d1	lázeňská
péče	péče	k1gFnSc2	péče
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
síť	síť	k1gFnSc4	síť
ordinací	ordinace	k1gFnPc2	ordinace
praktických	praktický	k2eAgMnPc2d1	praktický
i	i	k8xC	i
odborných	odborný	k2eAgMnPc2d1	odborný
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
pět	pět	k4xCc1	pět
domů	dům	k1gInPc2	dům
s	s	k7c7	s
pečovatelskou	pečovatelský	k2eAgFnSc7d1	pečovatelská
službou	služba	k1gFnSc7	služba
<g/>
,	,	kIx,	,
krajskou	krajský	k2eAgFnSc4d1	krajská
nemocnici	nemocnice	k1gFnSc4	nemocnice
s	s	k7c7	s
poliklinikou	poliklinika	k1gFnSc7	poliklinika
na	na	k7c6	na
Duchcovské	duchcovský	k2eAgFnSc6d1	Duchcovská
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
jedenáct	jedenáct	k4xCc1	jedenáct
lékáren	lékárna	k1gFnPc2	lékárna
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc6	jeho
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
mají	mít	k5eAaImIp3nP	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
svými	svůj	k3xOyFgInPc7	svůj
obchodními	obchodní	k2eAgInPc7d1	obchodní
domy	dům	k1gInPc7	dům
všechny	všechen	k3xTgInPc1	všechen
velké	velký	k2eAgInPc1d1	velký
obchodní	obchodní	k2eAgInPc1d1	obchodní
řetězce	řetězec	k1gInPc1	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
obchodních	obchodní	k2eAgFnPc2d1	obchodní
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
postavena	postaven	k2eAgFnSc1d1	postavena
obchodní	obchodní	k2eAgFnSc1d1	obchodní
centra	centrum	k1gNnSc2	centrum
Fontána	fontána	k1gFnSc1	fontána
a	a	k8xC	a
Galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
výrazně	výrazně	k6eAd1	výrazně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
nabídku	nabídka	k1gFnSc4	nabídka
služeb	služba	k1gFnPc2	služba
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
i	i	k9	i
možnosti	možnost	k1gFnPc4	možnost
parkování	parkování	k1gNnSc3	parkování
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
centrum	centrum	k1gNnSc1	centrum
Teplice	teplice	k1gFnSc2	teplice
<g/>
,	,	kIx,	,
Srbice	Srbice	k1gFnSc1	Srbice
Fontána	fontána	k1gFnSc1	fontána
Teplice	teplice	k1gFnSc1	teplice
Galerie	galerie	k1gFnSc1	galerie
Teplice	teplice	k1gFnSc1	teplice
V	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
se	se	k3xPyFc4	se
léčí	léčit	k5eAaImIp3nP	léčit
v	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
řadě	řada	k1gFnSc6	řada
kardiovaskulární	kardiovaskulární	k2eAgFnSc2d1	kardiovaskulární
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
problémy	problém	k1gInPc1	problém
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
onkologickými	onkologický	k2eAgNnPc7d1	onkologické
onemocněními	onemocnění	k1gNnPc7	onemocnění
<g/>
,	,	kIx,	,
nemoci	moct	k5eNaImF	moct
z	z	k7c2	z
poruch	porucha	k1gFnPc2	porucha
výměny	výměna	k1gFnSc2	výměna
látkové	látkový	k2eAgFnSc2d1	látková
a	a	k8xC	a
žláz	žláza	k1gFnPc2	žláza
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
sekrecí	sekrece	k1gFnSc7	sekrece
(	(	kIx(	(
<g/>
cukrovka	cukrovka	k1gFnSc1	cukrovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnSc2	nemoc
pohybového	pohybový	k2eAgNnSc2d1	pohybové
ústrojí	ústrojí	k1gNnSc2	ústrojí
a	a	k8xC	a
nervová	nervový	k2eAgNnPc1d1	nervové
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Lázeňské	lázeňský	k2eAgInPc1d1	lázeňský
domy	dům	k1gInPc1	dům
<g/>
:	:	kIx,	:
Císařské	císařský	k2eAgFnPc1d1	císařská
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
po	po	k7c6	po
císaři	císař	k1gMnSc6	císař
Františku	František	k1gMnSc6	František
Josefovi	Josef	k1gMnSc6	Josef
I.	I.	kA	I.
Lázeňský	lázeňský	k2eAgInSc4d1	lázeňský
dům	dům	k1gInSc4	dům
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgFnPc4d1	spojená
a	a	k8xC	a
nové	nový	k2eAgFnPc4d1	nová
budovy	budova	k1gFnPc4	budova
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
Sadové	sadový	k2eAgFnPc1d1	Sadová
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
pod	pod	k7c4	pod
Lázeňský	lázeňský	k2eAgInSc4d1	lázeňský
dům	dům	k1gInSc4	dům
Beethoven	Beethoven	k1gMnSc1	Beethoven
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
depadence	depadence	k1gFnSc2	depadence
<g/>
)	)	kIx)	)
Kamenné	kamenný	k2eAgFnPc1d1	kamenná
lázně	lázeň	k1gFnPc1	lázeň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
<g />
.	.	kIx.	.
</s>
<s>
pod	pod	k7c4	pod
Kamenné	kamenný	k2eAgFnPc4d1	kamenná
lázně	lázeň	k1gFnPc4	lázeň
Nové	Nové	k2eAgFnSc2d1	Nové
lázně	lázeň	k1gFnSc2	lázeň
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
lázně	lázeň	k1gFnSc2	lázeň
(	(	kIx(	(
<g/>
nepatří	patřit	k5eNaImIp3nS	patřit
pod	pod	k7c4	pod
společnost	společnost	k1gFnSc4	společnost
Lázně	lázeň	k1gFnSc2	lázeň
Teplice	teplice	k1gFnSc2	teplice
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
Hadí	hadit	k5eAaImIp3nP	hadit
lázně	lázeň	k1gFnPc1	lázeň
V	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
zahušťovat	zahušťovat	k5eAaImF	zahušťovat
automobilová	automobilový	k2eAgFnSc1d1	automobilová
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
neproblematičtějším	problematický	k2eNgNnSc7d2	problematický
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
třída	třída	k1gFnSc1	třída
v	v	k7c6	v
Trnovanech	Trnovan	k1gMnPc6	Trnovan
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
prochází	procházet	k5eAaImIp3nS	procházet
jak	jak	k6eAd1	jak
lokální	lokální	k2eAgFnSc1d1	lokální
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
tranzitní	tranzitní	k2eAgFnSc1d1	tranzitní
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
chybí	chybět	k5eAaImIp3nS	chybět
obchvat	obchvat	k1gInSc1	obchvat
města	město	k1gNnSc2	město
a	a	k8xC	a
jakkákoli	jakkákoli	k6eAd1	jakkákoli
mimoúrovňová	mimoúrovňový	k2eAgNnPc1d1	mimoúrovňové
křížení	křížení	k1gNnPc1	křížení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
zahustila	zahustit	k5eAaPmAgFnS	zahustit
díky	díky	k7c3	díky
novým	nový	k2eAgInPc3d1	nový
obchodním	obchodní	k2eAgInPc3d1	obchodní
centrům	centr	k1gInPc3	centr
doprava	doprava	k6eAd1	doprava
i	i	k8xC	i
v	v	k7c6	v
Alejní	Alejní	k2eAgFnSc6d1	Alejní
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Výhodný	výhodný	k2eAgInSc4d1	výhodný
je	být	k5eAaImIp3nS	být
severojižní	severojižní	k2eAgInSc4d1	severojižní
průtah	průtah	k1gInSc4	průtah
městem	město	k1gNnSc7	město
s	s	k7c7	s
mimoúrovňovým	mimoúrovňový	k2eAgNnSc7d1	mimoúrovňové
křížením	křížení	k1gNnSc7	křížení
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
zámecké	zámecký	k2eAgFnSc2d1	zámecká
zahrady	zahrada	k1gFnSc2	zahrada
ale	ale	k8xC	ale
chybí	chybit	k5eAaPmIp3nS	chybit
sjezdy	sjezd	k1gInPc7	sjezd
a	a	k8xC	a
nájezdy	nájezd	k1gInPc7	nájezd
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
využitelnost	využitelnost	k1gFnSc1	využitelnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
města	město	k1gNnSc2	město
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
pro	pro	k7c4	pro
průjezd	průjezd	k1gInSc4	průjezd
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
zahušťuje	zahušťovat	k5eAaImIp3nS	zahušťovat
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Přibylo	přibýt	k5eAaPmAgNnS	přibýt
několik	několik	k4yIc1	několik
kruhových	kruhový	k2eAgInPc2d1	kruhový
objezdů	objezd	k1gInPc2	objezd
<g/>
,	,	kIx,	,
majoritní	majoritní	k2eAgMnSc1d1	majoritní
ovšem	ovšem	k9	ovšem
nadále	nadále	k6eAd1	nadále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
světelně	světelně	k6eAd1	světelně
řízené	řízený	k2eAgFnSc2d1	řízená
křižovatky	křižovatka	k1gFnSc2	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Přibývá	přibývat	k5eAaImIp3nS	přibývat
i	i	k9	i
ulic	ulice	k1gFnPc2	ulice
s	s	k7c7	s
jednosměrným	jednosměrný	k2eAgInSc7d1	jednosměrný
provozem	provoz	k1gInSc7	provoz
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
řešení	řešení	k1gNnSc2	řešení
parkování	parkování	k1gNnSc2	parkování
a	a	k8xC	a
zklidňování	zklidňování	k1gNnSc2	zklidňování
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
negativní	negativní	k2eAgInSc1d1	negativní
efekt	efekt	k1gInSc1	efekt
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
prodloužení	prodloužení	k1gNnSc2	prodloužení
průjezdů	průjezd	k1gInPc2	průjezd
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
První	první	k4xOgFnSc1	první
železnice	železnice	k1gFnSc1	železnice
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
Teplic	Teplice	k1gFnPc2	Teplice
z	z	k7c2	z
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
úsecích	úsek	k1gInPc6	úsek
prodlužována	prodlužován	k2eAgFnSc1d1	prodlužována
přes	přes	k7c4	přes
Duchcov	Duchcov	k1gInSc4	Duchcov
do	do	k7c2	do
Chomutova	Chomutov	k1gInSc2	Chomutov
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
<g/>
,	,	kIx,	,
s	s	k7c7	s
taktovou	taktový	k2eAgFnSc7d1	taktová
dopravou	doprava	k1gFnSc7	doprava
a	a	k8xC	a
v	v	k7c6	v
jízdním	jízdní	k2eAgInSc6d1	jízdní
řádu	řád	k1gInSc6	řád
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
130	[number]	k4	130
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
teplické	teplický	k2eAgNnSc1d1	Teplické
nádraží	nádraží	k1gNnSc1	nádraží
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Teplice	Teplice	k1gFnPc1	Teplice
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
rychlíková	rychlíkový	k2eAgFnSc1d1	rychlíková
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
-	-	kIx~	-
Chomutov	Chomutov	k1gInSc1	Chomutov
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
přestupu	přestup	k1gInSc2	přestup
na	na	k7c6	na
vlaky	vlak	k1gInPc1	vlak
dvou	dva	k4xCgFnPc2	dva
dalších	další	k2eAgFnPc2d1	další
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
však	však	k9	však
není	být	k5eNaImIp3nS	být
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
odbočujících	odbočující	k2eAgFnPc2d1	odbočující
tratí	trať	k1gFnPc2	trať
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
stanicích	stanice	k1gFnPc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
Teplice	teplice	k1gFnSc2	teplice
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
Řetenice	Řetenice	k1gFnSc2	Řetenice
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
uváděny	uvádět	k5eAaImNgInP	uvádět
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
0	[number]	k4	0
<g/>
97	[number]	k4	97
<g/>
,	,	kIx,	,
130	[number]	k4	130
i	i	k8xC	i
134	[number]	k4	134
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Teplice-Trnovany	Teplice-Trnovan	k1gMnPc4	Teplice-Trnovan
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
zastávka	zastávka	k1gFnSc1	zastávka
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
po	po	k7c6	po
sousední	sousední	k2eAgFnSc6d1	sousední
obci	obec	k1gFnSc6	obec
Proboštov	Proboštov	k1gInSc4	Proboštov
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
teplické	teplický	k2eAgFnPc1d1	Teplická
trati	trať	k1gFnPc1	trať
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Lovosice	Lovosice	k1gInPc1	Lovosice
-	-	kIx~	-
Teplice	Teplice	k1gFnPc1	Teplice
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
č.	č.	k?	č.
0	[number]	k4	0
<g/>
97	[number]	k4	97
<g/>
,	,	kIx,	,
Teplice	Teplice	k1gFnPc1	Teplice
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
-	-	kIx~	-
Řetenice	Řetenice	k1gFnSc1	Řetenice
-	-	kIx~	-
Úpořiny	Úpořin	k2eAgInPc1d1	Úpořin
-	-	kIx~	-
Lovosice	Lovosice	k1gInPc1	Lovosice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
leží	ležet	k5eAaImIp3nS	ležet
stanice	stanice	k1gFnSc1	stanice
Teplice	Teplice	k1gFnPc4	Teplice
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
Řetenice	Řetenice	k1gFnSc1	Řetenice
<g/>
,	,	kIx,	,
Teplice	teplice	k1gFnSc1	teplice
zámecká	zámecký	k2eAgFnSc1d1	zámecká
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
Prosetice	Prosetika	k1gFnSc3	Prosetika
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Teplice	teplice	k1gFnSc2	teplice
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
-	-	kIx~	-
Litvínov	Litvínov	k1gInSc4	Litvínov
(	(	kIx(	(
<g/>
č.	č.	k?	č.
134	[number]	k4	134
<g/>
,	,	kIx,	,
Teplice	Teplice	k1gFnPc1	Teplice
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
-	-	kIx~	-
Řetenice	Řetenice	k1gFnSc1	Řetenice
-	-	kIx~	-
Oldřichov	Oldřichov	k1gInSc1	Oldřichov
u	u	k7c2	u
Duchcova	Duchcův	k2eAgMnSc2d1	Duchcův
-	-	kIx~	-
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oddělující	oddělující	k2eAgFnSc2d1	oddělující
se	se	k3xPyFc4	se
od	od	k7c2	od
trati	trať	k1gFnSc2	trať
130	[number]	k4	130
v	v	k7c6	v
Oldřichově	Oldřichův	k2eAgFnSc6d1	Oldřichova
u	u	k7c2	u
Duchcova	Duchcův	k2eAgNnSc2d1	Duchcův
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Děčín	Děčín	k1gInSc1	Děčín
-	-	kIx~	-
Oldřichov	Oldřichov	k1gInSc1	Oldřichov
u	u	k7c2	u
Duchcova	Duchcův	k2eAgNnSc2d1	Duchcův
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
severní	severní	k2eAgFnSc7d1	severní
částí	část	k1gFnSc7	část
Teplic	Teplice	k1gFnPc2	Teplice
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
území	území	k1gNnSc4	území
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
tratěmi	trať	k1gFnPc7	trať
propojena	propojit	k5eAaPmNgFnS	propojit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
má	mít	k5eAaImIp3nS	mít
zastávku	zastávka	k1gFnSc4	zastávka
Teplice	teplice	k1gFnSc2	teplice
lesní	lesní	k2eAgFnSc1d1	lesní
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
veřejné	veřejný	k2eAgFnSc2d1	veřejná
osobní	osobní	k2eAgFnSc2d1	osobní
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Arriva	Arrivo	k1gNnSc2	Arrivo
Teplice	teplice	k1gFnSc2	teplice
<g/>
,	,	kIx,	,
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
,	,	kIx,	,
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
a	a	k8xC	a
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
MHD	MHD	kA	MHD
se	se	k3xPyFc4	se
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vyjely	vyjet	k5eAaPmAgInP	vyjet
do	do	k7c2	do
teplických	teplický	k2eAgFnPc2d1	Teplická
ulic	ulice	k1gFnPc2	ulice
poprvé	poprvé	k6eAd1	poprvé
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
provoz	provoz	k1gInSc4	provoz
elektrickými	elektrický	k2eAgFnPc7d1	elektrická
tramvajemi	tramvaj	k1gFnPc7	tramvaj
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
nepočítáme	počítat	k5eNaImIp1nP	počítat
zkušební	zkušební	k2eAgFnSc4d1	zkušební
a	a	k8xC	a
propagační	propagační	k2eAgFnSc4d1	propagační
Křižíkovu	Křižíkův	k2eAgFnSc4d1	Křižíkova
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
dráhu	dráha	k1gFnSc4	dráha
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	léto	k1gNnPc6	léto
rozvoje	rozvoj	k1gInSc2	rozvoj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
meziměstská	meziměstská	k1gFnSc1	meziměstská
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Dubí	dubí	k1gNnSc2	dubí
<g/>
)	)	kIx)	)
přišel	přijít	k5eAaPmAgMnS	přijít
úpadek	úpadek	k1gInSc4	úpadek
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
zrušena	zrušit	k5eAaPmNgNnP	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbusy	trolejbus	k1gInPc1	trolejbus
měly	mít	k5eAaImAgInP	mít
původně	původně	k6eAd1	původně
tramvaje	tramvaj	k1gFnPc4	tramvaj
doplňovat	doplňovat	k5eAaImF	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
ale	ale	k8xC	ale
tato	tento	k3xDgNnPc1	tento
vozidla	vozidlo	k1gNnPc1	vozidlo
tramvaje	tramvaj	k1gFnSc2	tramvaj
zcela	zcela	k6eAd1	zcela
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc4d2	veliký
rozmach	rozmach	k1gInSc4	rozmach
prodělaly	prodělat	k5eAaPmAgFnP	prodělat
trolejbusy	trolejbus	k1gInPc4	trolejbus
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
však	však	k9	však
nastala	nastat	k5eAaPmAgFnS	nastat
stagnace	stagnace	k1gFnSc1	stagnace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
moderní	moderní	k2eAgInPc1d1	moderní
trolejbusy	trolejbus	k1gInPc1	trolejbus
opět	opět	k6eAd1	opět
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
veřejností	veřejnost	k1gFnSc7	veřejnost
využívané	využívaný	k2eAgFnSc2d1	využívaná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
městské	městský	k2eAgInPc1d1	městský
autobusy	autobus	k1gInPc1	autobus
se	se	k3xPyFc4	se
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
objevily	objevit	k5eAaPmAgFnP	objevit
již	již	k6eAd1	již
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
však	však	k9	však
o	o	k7c4	o
linky	linka	k1gFnPc4	linka
soukromých	soukromý	k2eAgMnPc2d1	soukromý
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
převzaty	převzít	k5eAaPmNgFnP	převzít
městskou	městský	k2eAgFnSc7d1	městská
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provozovala	provozovat	k5eAaImAgFnS	provozovat
tramvaje	tramvaj	k1gFnPc4	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
tyto	tento	k3xDgFnPc4	tento
linky	linka	k1gFnPc4	linka
ale	ale	k8xC	ale
převzala	převzít	k5eAaPmAgFnS	převzít
ČSAD	ČSAD	kA	ČSAD
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
autobusy	autobus	k1gInPc1	autobus
dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
tak	tak	k6eAd1	tak
vyjely	vyjet	k5eAaPmAgFnP	vyjet
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgInS	následovat
prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
Teplice	teplice	k1gFnSc2	teplice
provozoval	provozovat	k5eAaImAgInS	provozovat
27	[number]	k4	27
linek	linka	k1gFnPc2	linka
autobusů	autobus	k1gInPc2	autobus
po	po	k7c6	po
městě	město	k1gNnSc6	město
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
deset	deset	k4xCc4	deset
linek	linka	k1gFnPc2	linka
trolejbusů	trolejbus	k1gInPc2	trolejbus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
podnik	podnik	k1gInSc1	podnik
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
převzala	převzít	k5eAaPmAgFnS	převzít
společnost	společnost	k1gFnSc1	společnost
Veolia	Veolia	k1gFnSc1	Veolia
transport	transport	k1gInSc4	transport
česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
francouzské	francouzský	k2eAgFnSc2d1	francouzská
firmy	firma	k1gFnSc2	firma
Veolia	Veolium	k1gNnSc2	Veolium
Transport	transport	k1gInSc1	transport
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c4	na
Veolia	Veolium	k1gNnPc4	Veolium
Transport	transporta	k1gFnPc2	transporta
Teplice	teplice	k1gFnSc1	teplice
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
byla	být	k5eAaImAgFnS	být
převzata	převzat	k2eAgFnSc1d1	převzata
i	i	k8xC	i
dopravní	dopravní	k2eAgFnSc1d1	dopravní
obslužnost	obslužnost	k1gFnSc1	obslužnost
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
název	název	k1gInSc1	název
DP	DP	kA	DP
Teplice	teplice	k1gFnSc2	teplice
na	na	k7c4	na
Veolia	Veolium	k1gNnPc4	Veolium
Transport	transporta	k1gFnPc2	transporta
Teplice	teplice	k1gFnSc1	teplice
a	a	k8xC	a
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
datem	datum	k1gNnSc7	datum
také	také	k9	také
navždy	navždy	k6eAd1	navždy
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
i	i	k8xC	i
dokumentů	dokument	k1gInPc2	dokument
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
jeho	jeho	k3xOp3gNnSc4	jeho
žlutomodré	žlutomodrý	k2eAgNnSc4d1	žlutomodré
logo	logo	k1gNnSc4	logo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2013	[number]	k4	2013
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
opětovné	opětovný	k2eAgNnSc1d1	opětovné
přejmenování	přejmenování	k1gNnSc1	přejmenování
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
se	se	k3xPyFc4	se
Veolia	Veolia	k1gFnSc1	Veolia
Transport	transporta	k1gFnPc2	transporta
Teplice	teplice	k1gFnSc1	teplice
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
Arriva	Arriv	k1gMnSc4	Arriv
Teplice	teplice	k1gFnSc2	teplice
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
nyní	nyní	k6eAd1	nyní
funguje	fungovat	k5eAaImIp3nS	fungovat
16	[number]	k4	16
základních	základní	k2eAgFnPc2d1	základní
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
příspěvkových	příspěvkový	k2eAgFnPc2d1	příspěvková
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
škol	škola	k1gFnPc2	škola
středních	střední	k2eAgFnPc2d1	střední
Střední	střední	k2eAgFnPc1d1	střední
škola	škola	k1gFnSc1	škola
AGC	AGC	kA	AGC
a.s.	a.s.	k?	a.s.
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
Teplice	teplice	k1gFnSc2	teplice
Hotelová	hotelový	k2eAgFnSc1d1	hotelová
škola	škola	k1gFnSc1	škola
Teplice	teplice	k1gFnSc2	teplice
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
textilní	textilní	k2eAgFnSc1d1	textilní
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
stavební	stavební	k2eAgFnSc2d1	stavební
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Teplice	teplice	k1gFnSc2	teplice
Konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
Teplice	teplice	k1gFnSc1	teplice
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc1d1	tradiční
akcí	akce	k1gFnSc7	akce
je	být	k5eAaImIp3nS	být
každoroční	každoroční	k2eAgNnSc4d1	každoroční
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
zahájení	zahájení	k1gNnSc4	zahájení
lázeňské	lázeňský	k2eAgFnSc2d1	lázeňská
sezóny	sezóna	k1gFnSc2	sezóna
vždy	vždy	k6eAd1	vždy
poslední	poslední	k2eAgInSc4d1	poslední
květnový	květnový	k2eAgInSc4d1	květnový
víkend	víkend	k1gInSc4	víkend
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc1	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
zahájení	zahájení	k1gNnSc1	zahájení
lázeňské	lázeňský	k2eAgFnSc2d1	lázeňská
sezóny	sezóna	k1gFnSc2	sezóna
kvůli	kvůli	k7c3	kvůli
volbám	volba	k1gFnPc3	volba
o	o	k7c4	o
týden	týden	k1gInSc4	týden
zpožděno	zpožděn	k2eAgNnSc1d1	zpožděno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
centry	centr	k1gMnPc7	centr
kultury	kultura	k1gFnSc2	kultura
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Krušnohorské	krušnohorský	k2eAgNnSc1d1	Krušnohorské
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
Dům	dům	k1gInSc4	dům
Kultury	kultura	k1gFnSc2	kultura
Regionální	regionální	k2eAgNnSc1d1	regionální
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgFnSc1d1	regionální
knihovna	knihovna	k1gFnSc1	knihovna
Botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
Šanov	Šanov	k1gInSc1	Šanov
Kino	kino	k1gNnSc1	kino
Květen	květen	k1gInSc1	květen
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jediné	jediný	k2eAgInPc1d1	jediný
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
šesti	šest	k4xCc2	šest
přemístěno	přemístěn	k2eAgNnSc4d1	přemístěno
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
Kulturního	kulturní	k2eAgInSc2d1	kulturní
domu	dům	k1gInSc2	dům
Galerie	galerie	k1gFnSc1	galerie
Bohemia	bohemia	k1gFnSc1	bohemia
Art	Art	k1gFnSc1	Art
<g/>
,	,	kIx,	,
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
výstavních	výstavní	k2eAgFnPc2d1	výstavní
síní	síň	k1gFnPc2	síň
Severočeská	severočeský	k2eAgFnSc1d1	Severočeská
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
a	a	k8xC	a
planetárium	planetárium	k1gNnSc1	planetárium
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
-	-	kIx~	-
hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
na	na	k7c6	na
Písečném	písečný	k2eAgInSc6d1	písečný
vrchu	vrch	k1gInSc6	vrch
a	a	k8xC	a
planetárium	planetárium	k1gNnSc1	planetárium
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
v	v	k7c6	v
Šanově	Šanov	k1gInSc6	Šanov
Severočeská	severočeský	k2eAgFnSc1d1	Severočeská
filharmonie	filharmonie	k1gFnSc1	filharmonie
Teplice	teplice	k1gFnSc2	teplice
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
tělovýchovných	tělovýchovný	k2eAgFnPc2d1	Tělovýchovná
jednot	jednota	k1gFnPc2	jednota
a	a	k8xC	a
klubů	klub	k1gInPc2	klub
sportovního	sportovní	k2eAgInSc2d1	sportovní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnější	populární	k2eAgNnSc1d3	nejpopulárnější
je	být	k5eAaImIp3nS	být
prvoligový	prvoligový	k2eAgInSc4d1	prvoligový
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
FK	FK	kA	FK
Teplice	teplice	k1gFnSc2	teplice
založený	založený	k2eAgInSc4d1	založený
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
umístění	umístění	k1gNnSc4	umístění
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
ze	z	k7c2	z
sezóny	sezóna	k1gFnSc2	sezóna
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
a	a	k8xC	a
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
ze	z	k7c2	z
sezóny	sezóna	k1gFnSc2	sezóna
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
futsalový	futsalový	k2eAgMnSc1d1	futsalový
FC	FC	kA	FC
Balticflora	Balticflor	k1gMnSc4	Balticflor
a	a	k8xC	a
Wolves	Wolves	k1gInSc4	Wolves
Řetenice	Řetenice	k1gFnSc2	Řetenice
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
TJ	tj	kA	tj
Hvězda	Hvězda	k1gMnSc1	Hvězda
Trnovany	Trnovan	k1gMnPc4	Trnovan
<g/>
,	,	kIx,	,
TJ	tj	kA	tj
Lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
Teplice	teplice	k1gFnSc1	teplice
<g/>
,	,	kIx,	,
TJ	tj	kA	tj
Bonex	Bonex	k1gInSc1	Bonex
<g/>
,	,	kIx,	,
TJ	tj	kA	tj
AGC	AGC	kA	AGC
nebo	nebo	k8xC	nebo
florbalový	florbalový	k2eAgInSc4d1	florbalový
tým	tým	k1gInSc4	tým
DDM	DDM	kA	DDM
Dynamo	dynamo	k1gNnSc1	dynamo
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
areál	areál	k1gInSc4	areál
Aquacentrum	Aquacentrum	k1gNnSc1	Aquacentrum
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgInS	zapsat
předválečný	předválečný	k2eAgInSc1d1	předválečný
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Teplitzer	Teplitzra	k1gFnPc2	Teplitzra
FK	FK	kA	FK
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
účastníkem	účastník	k1gMnSc7	účastník
československé	československý	k2eAgFnSc2d1	Československá
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
za	za	k7c4	za
který	který	k3yIgInSc4	který
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
řada	řada	k1gFnSc1	řada
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
reprezentantů	reprezentant	k1gMnPc2	reprezentant
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Karel	Karel	k1gMnSc1	Karel
Koželuh	koželuh	k1gMnSc1	koželuh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
probíhá	probíhat	k5eAaImIp3nS	probíhat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgMnPc2d3	nejsilnější
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
šachových	šachový	k2eAgInPc2d1	šachový
turnajů	turnaj	k1gInPc2	turnaj
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Open	Opena	k1gFnPc2	Opena
Teplice	teplice	k1gFnSc2	teplice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
Šachový	šachový	k2eAgInSc1d1	šachový
klub	klub	k1gInSc1	klub
Teplice	teplice	k1gFnSc2	teplice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
turnaj	turnaj	k1gInSc1	turnaj
evropského	evropský	k2eAgInSc2d1	evropský
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
také	také	k9	také
pravidelně	pravidelně	k6eAd1	pravidelně
dubnový	dubnový	k2eAgInSc1d1	dubnový
Evropský	evropský	k2eAgInSc1d1	evropský
pohár	pohár	k1gInSc1	pohár
v	v	k7c6	v
judu	judo	k1gNnSc6	judo
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
spolupořádaný	spolupořádaný	k2eAgInSc1d1	spolupořádaný
teplickým	teplický	k2eAgMnSc7d1	teplický
Pro	pro	k7c4	pro
Sport	sport	k1gInSc4	sport
Teplice	teplice	k1gFnSc2	teplice
Judo	judo	k1gNnSc4	judo
a	a	k8xC	a
Českým	český	k2eAgInSc7d1	český
svazem	svaz	k1gInSc7	svaz
juda	judo	k1gNnSc2	judo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
také	také	k9	také
ME	ME	kA	ME
judistů	judista	k1gMnPc2	judista
kategorie	kategorie	k1gFnSc2	kategorie
U	u	k7c2	u
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
Děkanský	děkanský	k2eAgInSc1d1	děkanský
kostel	kostel	k1gInSc1	kostel
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
Zámeckého	zámecký	k2eAgNnSc2d1	zámecké
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1585	[number]	k4	1585
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starší	starý	k2eAgFnSc2d2	starší
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
1594	[number]	k4	1594
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
byla	být	k5eAaImAgFnS	být
průčelní	průčelní	k2eAgFnSc1d1	průčelní
západní	západní	k2eAgFnSc1d1	západní
věž	věž	k1gFnSc1	věž
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
bleskem	blesk	k1gInSc7	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Požárem	požár	k1gInSc7	požár
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgInPc1	některý
zvony	zvon	k1gInPc1	zvon
zničeny	zničit	k5eAaPmNgInP	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1659	[number]	k4	1659
sem	sem	k6eAd1	sem
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
tři	tři	k4xCgInPc1	tři
nové	nový	k2eAgInPc1d1	nový
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
zvonař	zvonař	k1gMnSc1	zvonař
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Löw	Löw	k1gMnSc1	Löw
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
válečných	válečný	k2eAgFnPc2d1	válečná
rekvizic	rekvizice	k1gFnPc2	rekvizice
přesunut	přesunout	k5eAaPmNgInS	přesunout
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Karlíně	Karlín	k1gInSc6	Karlín
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
je	být	k5eAaImIp3nS	být
nezvěstný	zvěstný	k2eNgMnSc1d1	nezvěstný
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
nachází	nacházet	k5eAaImIp3nS	nacházet
zvon	zvon	k1gInSc1	zvon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
od	od	k7c2	od
Wolfa	Wolf	k1gMnSc2	Wolf
Hilgera	Hilger	k1gMnSc2	Hilger
a	a	k8xC	a
zvon	zvon	k1gInSc4	zvon
z	z	k7c2	z
r.	r.	kA	r.
1482	[number]	k4	1482
od	od	k7c2	od
Hanuše	Hanuš	k1gMnSc2	Hanuš
Konváře	konvář	k1gMnSc2	konvář
<g/>
.	.	kIx.	.
</s>
<s>
Sanktusník	sanktusník	k1gInSc1	sanktusník
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
luŽidé	luŽidý	k2eAgInPc1d1	luŽidý
ve	v	k7c6	v
městěcerna	městěcerna	k1gFnSc1	městěcerna
i	i	k9	i
postranní	postranní	k2eAgFnSc1d1	postranní
věžička	věžička	k1gFnSc1	věžička
jsou	být	k5eAaImIp3nP	být
prázdné	prázdný	k2eAgInPc1d1	prázdný
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
zvonovým	zvonový	k2eAgNnSc7d1	zvonové
patrem	patro	k1gNnSc7	patro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
byt	byt	k1gInSc1	byt
věžníka	věžník	k1gMnSc2	věžník
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
je	být	k5eAaImIp3nS	být
cenné	cenný	k2eAgNnSc1d1	cenné
barokní	barokní	k2eAgNnSc1d1	barokní
zařízení	zařízení	k1gNnSc1	zařízení
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
1730	[number]	k4	1730
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
a	a	k8xC	a
na	na	k7c6	na
bočních	boční	k2eAgInPc6d1	boční
oltářích	oltář	k1gInPc6	oltář
svatého	svatý	k1gMnSc4	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
a	a	k8xC	a
čtrnácti	čtrnáct	k4xCc2	čtrnáct
svatých	svatá	k1gFnPc2	svatá
pomocníků	pomocník	k1gMnPc2	pomocník
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
Petra	Petr	k1gMnSc2	Petr
Brandla	Brandla	k1gMnSc2	Brandla
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgInSc2d1	svatý
Kříže	kříž	k1gInSc2	kříž
na	na	k7c6	na
Zámeckém	zámecký	k2eAgNnSc6d1	zámecké
náměstí	náměstí	k1gNnSc6	náměstí
mezi	mezi	k7c7	mezi
kostelem	kostel	k1gInSc7	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
a	a	k8xC	a
zámkem	zámek	k1gInSc7	zámek
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
zámecká	zámecký	k2eAgFnSc1d1	zámecká
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1550	[number]	k4	1550
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1798-1806	[number]	k4	1798-1806
romanticky	romanticky	k6eAd1	romanticky
zgotizovaná	zgotizovaný	k2eAgFnSc1d1	zgotizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
pravoslavný	pravoslavný	k2eAgInSc4d1	pravoslavný
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boční	boční	k2eAgFnSc6d1	boční
severní	severní	k2eAgFnSc6d1	severní
věži	věž	k1gFnSc6	věž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zvon	zvon	k1gInSc1	zvon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
v	v	k7c6	v
gotické	gotický	k2eAgFnSc6d1	gotická
minuskuli	minuskule	k1gFnSc6	minuskule
a	a	k8xC	a
litinový	litinový	k2eAgInSc4d1	litinový
zvon	zvon	k1gInSc4	zvon
z	z	k7c2	z
bochumských	bochumský	k2eAgFnPc2d1	bochumský
oceláren	ocelárna	k1gFnPc2	ocelárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
okně	okno	k1gNnSc6	okno
věže	věž	k1gFnSc2	věž
v	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
patře	patro	k1gNnSc6	patro
je	být	k5eAaImIp3nS	být
zavěšen	zavěsit	k5eAaPmNgInS	zavěsit
také	také	k9	také
zvonovinový	zvonovinový	k2eAgInSc1d1	zvonovinový
hodinový	hodinový	k2eAgInSc1d1	hodinový
cymbál	cymbál	k1gInSc1	cymbál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
50	[number]	k4	50
cm	cm	kA	cm
a	a	k8xC	a
kolem	kolem	k7c2	kolem
jeho	jeho	k3xOp3gFnSc2	jeho
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
vine	vinout	k5eAaImIp3nS	vinout
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
ornamentální	ornamentální	k2eAgInSc1d1	ornamentální
pás	pás	k1gInSc1	pás
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
cymbál	cymbál	k1gInSc1	cymbál
nezdobený	zdobený	k2eNgInSc1d1	nezdobený
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
zaklenuta	zaklenout	k5eAaPmNgFnS	zaklenout
křížovou	křížový	k2eAgFnSc7d1	křížová
klenbou	klenba	k1gFnSc7	klenba
<g/>
,	,	kIx,	,
v	v	k7c6	v
boční	boční	k2eAgFnSc6d1	boční
kapli	kaple	k1gFnSc6	kaple
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
italské	italský	k2eAgInPc1d1	italský
mramorové	mramorový	k2eAgInPc1d1	mramorový
oltáře	oltář	k1gInPc1	oltář
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1420	[number]	k4	1420
a	a	k8xC	a
1440	[number]	k4	1440
<g/>
,	,	kIx,	,
použité	použitý	k2eAgInPc1d1	použitý
jako	jako	k8xC	jako
náhrobky	náhrobek	k1gInPc1	náhrobek
Kostel	kostel	k1gInSc1	kostel
Prokopa	Prokop	k1gMnSc2	Prokop
Holého	Holý	k1gMnSc2	Holý
na	na	k7c4	na
návrší	návrší	k1gNnPc4	návrší
<g/>
,	,	kIx,	,
Chelčického	Chelčický	k2eAgNnSc2d1	Chelčický
447	[number]	k4	447
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
Českobratrskou	českobratrský	k2eAgFnSc7d1	Českobratrská
<g/>
,	,	kIx,	,
s	s	k7c7	s
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
luteránský	luteránský	k2eAgInSc1d1	luteránský
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
1864	[number]	k4	1864
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
pruského	pruský	k2eAgMnSc2d1	pruský
architekta	architekt	k1gMnSc2	architekt
Friedricha	Friedrich	k1gMnSc2	Friedrich
Stülera	Stüler	k1gMnSc2	Stüler
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
<g/>
,	,	kIx,	,
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přeměněný	přeměněný	k2eAgInSc1d1	přeměněný
na	na	k7c6	na
restauraci	restaurace	k1gFnSc6	restaurace
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
dokončená	dokončený	k2eAgFnSc1d1	dokončená
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
zvonu	zvon	k1gInSc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnPc1	kaple
Povýšení	povýšení	k1gNnSc2	povýšení
svatého	svatý	k2eAgInSc2d1	svatý
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
hřbitovní	hřbitovní	k2eAgInPc1d1	hřbitovní
<g/>
,	,	kIx,	,
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1728	[number]	k4	1728
<g/>
-	-	kIx~	-
<g/>
1730	[number]	k4	1730
<g/>
.	.	kIx.	.
</s>
<s>
Drobná	drobný	k2eAgFnSc1d1	drobná
obdélná	obdélný	k2eAgFnSc1d1	obdélná
stavba	stavba	k1gFnSc1	stavba
bez	bez	k7c2	bez
věže	věž	k1gFnSc2	věž
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Havlíčkových	Havlíčkových	k2eAgInPc6d1	Havlíčkových
sadech	sad	k1gInPc6	sad
<g/>
.	.	kIx.	.
</s>
<s>
Sanktusník	sanktusník	k1gInSc1	sanktusník
bez	bez	k7c2	bez
zvonu	zvon	k1gInSc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
kaplí	kaple	k1gFnSc7	kaple
je	být	k5eAaImIp3nS	být
pomník	pomník	k1gInSc4	pomník
básníka	básník	k1gMnSc2	básník
J.	J.	kA	J.
G.	G.	kA	G.
Seume	Seum	k1gMnSc5	Seum
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Alžběty	Alžběta	k1gFnSc2	Alžběta
v	v	k7c6	v
Šanově	Šanov	k1gInSc6	Šanov
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
architekta	architekt	k1gMnSc2	architekt
Heinricha	Heinrich	k1gMnSc2	Heinrich
Ferstela	Ferstel	k1gMnSc2	Ferstel
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
zadní	zadní	k2eAgFnSc7d1	zadní
severní	severní	k2eAgFnSc7d1	severní
věží	věž	k1gFnSc7	věž
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Šanovských	Šanovský	k2eAgInPc6d1	Šanovský
sadech	sad	k1gInPc6	sad
na	na	k7c6	na
Zeyerově	Zeyerův	k2eAgNnSc6d1	Zeyerovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
se	se	k3xPyFc4	se
na	na	k7c6	na
ocelové	ocelový	k2eAgFnSc6d1	ocelová
konstrukci	konstrukce	k1gFnSc6	konstrukce
nachází	nacházet	k5eAaImIp3nS	nacházet
zvon	zvon	k1gInSc1	zvon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
od	od	k7c2	od
Petra	Petr	k1gMnSc2	Petr
Hilzera	Hilzer	k1gMnSc2	Hilzer
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Nejsvětějšího	nejsvětější	k2eAgNnSc2d1	nejsvětější
Srdce	srdce	k1gNnSc2	srdce
Páně	páně	k2eAgNnSc2d1	páně
v	v	k7c6	v
Trnovanech	Trnovan	k1gMnPc6	Trnovan
je	být	k5eAaImIp3nS	být
cihlová	cihlový	k2eAgFnSc1d1	cihlová
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
basilika	basilika	k1gFnSc1	basilika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
při	při	k7c6	při
silnici	silnice	k1gFnSc6	silnice
do	do	k7c2	do
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
jižní	jižní	k2eAgFnSc6d1	jižní
věži	věž	k1gFnSc6	věž
se	se	k3xPyFc4	se
na	na	k7c6	na
ocelové	ocelový	k2eAgFnSc6d1	ocelová
konstrukci	konstrukce	k1gFnSc6	konstrukce
nachází	nacházet	k5eAaImIp3nS	nacházet
zvon	zvon	k1gInSc1	zvon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
od	od	k7c2	od
Richarda	Richard	k1gMnSc2	Richard
Herolda	herold	k1gMnSc2	herold
a	a	k8xC	a
litinový	litinový	k2eAgInSc4d1	litinový
zvon	zvon	k1gInSc4	zvon
z	z	k7c2	z
bochumských	bochumský	k2eAgFnPc2d1	bochumský
oceláren	ocelárna	k1gFnPc2	ocelárna
<g/>
.	.	kIx.	.
</s>
<s>
Teplický	teplický	k2eAgInSc1d1	teplický
zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
rozlehlý	rozlehlý	k2eAgInSc1d1	rozlehlý
komplex	komplex	k1gInSc1	komplex
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
uspořádaný	uspořádaný	k2eAgInSc4d1	uspořádaný
do	do	k7c2	do
pěti	pět	k4xCc2	pět
křídel	křídlo	k1gNnPc2	křídlo
s	s	k7c7	s
nádvořím	nádvoří	k1gNnSc7	nádvoří
otevřeným	otevřený	k2eAgNnSc7d1	otevřené
do	do	k7c2	do
zámeckého	zámecký	k2eAgInSc2d1	zámecký
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
zámku	zámek	k1gInSc2	zámek
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
objeveny	objevit	k5eAaPmNgInP	objevit
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
zbytky	zbytek	k1gInPc1	zbytek
románského	románský	k2eAgInSc2d1	románský
kláštera	klášter	k1gInSc2	klášter
benediktinek	benediktinka	k1gFnPc2	benediktinka
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
půdorys	půdorys	k1gInSc1	půdorys
trojlodního	trojlodní	k2eAgInSc2d1	trojlodní
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
uzavíral	uzavírat	k5eAaPmAgInS	uzavírat
zhruba	zhruba	k6eAd1	zhruba
čtvercový	čtvercový	k2eAgInSc1d1	čtvercový
dvůr	dvůr	k1gInSc1	dvůr
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
přístupné	přístupný	k2eAgNnSc1d1	přístupné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
hlavní	hlavní	k2eAgInSc1d1	hlavní
trakt	trakt	k1gInSc1	trakt
(	(	kIx(	(
<g/>
do	do	k7c2	do
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
přestavován	přestavovat	k5eAaImNgInS	přestavovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
přistavěno	přistavěn	k2eAgNnSc4d1	přistavěno
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
traktem	trakt	k1gInSc7	trakt
V	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nS	sídlet
muzeum	muzeum	k1gNnSc1	muzeum
s	s	k7c7	s
pozoruhodnou	pozoruhodný	k2eAgFnSc7d1	pozoruhodná
sbírkou	sbírka	k1gFnSc7	sbírka
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Zahradní	zahradní	k2eAgInSc1d1	zahradní
a	a	k8xC	a
plesový	plesový	k2eAgInSc1d1	plesový
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgFnSc1d1	barokní
patrová	patrový	k2eAgFnSc1d1	patrová
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
roku	rok	k1gInSc2	rok
1732	[number]	k4	1732
postavil	postavit	k5eAaPmAgMnS	postavit
K.	K.	kA	K.
Lagler	Lagler	k1gMnSc1	Lagler
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
volně	volně	k6eAd1	volně
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
zámku	zámek	k1gInSc2	zámek
a	a	k8xC	a
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
na	na	k7c6	na
Doubravské	Doubravský	k2eAgFnSc6d1	Doubravská
hoře	hora	k1gFnSc6	hora
se	s	k7c7	s
šesti	šest	k4xCc7	šest
baštami	bašta	k1gFnPc7	bašta
a	a	k8xC	a
příkopem	příkop	k1gInSc7	příkop
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1478	[number]	k4	1478
<g/>
-	-	kIx~	-
<g/>
1486	[number]	k4	1486
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1644	[number]	k4	1644
pobořený	pobořený	k2eAgMnSc1d1	pobořený
a	a	k8xC	a
opravený	opravený	k2eAgMnSc1d1	opravený
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
s	s	k7c7	s
kinem	kino	k1gNnSc7	kino
a	a	k8xC	a
restaurací	restaurace	k1gFnSc7	restaurace
postavil	postavit	k5eAaPmAgMnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919-1924	[number]	k4	1919-1924
architekt	architekt	k1gMnSc1	architekt
R.	R.	kA	R.
Bitzan	Bitzan	k1gMnSc1	Bitzan
<g/>
.	.	kIx.	.
</s>
<s>
Concordia	Concordium	k1gNnSc2	Concordium
je	být	k5eAaImIp3nS	být
expresionistická	expresionistický	k2eAgFnSc1d1	expresionistická
budova	budova	k1gFnSc1	budova
postavená	postavený	k2eAgFnSc1d1	postavená
pro	pro	k7c4	pro
libereckou	liberecký	k2eAgFnSc4d1	liberecká
pojišťovnu	pojišťovna	k1gFnSc4	pojišťovna
Concordia	Concordium	k1gNnSc2	Concordium
(	(	kIx(	(
<g/>
dokončena	dokončen	k2eAgFnSc1d1	dokončena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architektů	architekt	k1gMnPc2	architekt
Maxe	Max	k1gMnSc2	Max
Loose	Loos	k1gMnSc2	Loos
a	a	k8xC	a
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Dorantha	Doranth	k1gMnSc2	Doranth
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Schaefterem-Hyrotsbergem	Schaefterem-Hyrotsberg	k1gMnSc7	Schaefterem-Hyrotsberg
<g/>
.	.	kIx.	.
</s>
<s>
Sloup	sloup	k1gInSc1	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
na	na	k7c6	na
Zámeckém	zámecký	k2eAgNnSc6d1	zámecké
náměstí	náměstí	k1gNnSc6	náměstí
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc1	dílo
Matyáše	Matyáš	k1gMnSc2	Matyáš
Bernarda	Bernard	k1gMnSc2	Bernard
Brauna	Braun	k1gMnSc2	Braun
a	a	k8xC	a
kameníka	kameník	k1gMnSc4	kameník
M.	M.	kA	M.
Blümela	Blümel	k1gMnSc4	Blümel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1719	[number]	k4	1719
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
barokních	barokní	k2eAgFnPc2d1	barokní
soch	socha	k1gFnPc2	socha
po	po	k7c6	po
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Jubilejní	jubilejní	k2eAgFnSc1d1	jubilejní
kašna	kašna	k1gFnSc1	kašna
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
"	"	kIx"	"
<g/>
u	u	k7c2	u
prasátek	prasátko	k1gNnPc2	prasátko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c6	v
parku	park	k1gInSc6	park
u	u	k7c2	u
divadla	divadlo	k1gNnSc2	divadlo
je	být	k5eAaImIp3nS	být
šestiboká	šestiboký	k2eAgFnSc1d1	šestiboká
kašna	kašna	k1gFnSc1	kašna
s	s	k7c7	s
novorománským	novorománský	k2eAgInSc7d1	novorománský
sloupem	sloup	k1gInSc7	sloup
uprostřed	uprostřed	k6eAd1	uprostřed
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
pověst	pověst	k1gFnSc4	pověst
o	o	k7c4	o
založení	založení	k1gNnSc4	založení
lázní	lázeň	k1gFnPc2	lázeň
roku	rok	k1gInSc2	rok
762	[number]	k4	762
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
chrliče	chrlič	k1gInPc1	chrlič
slouží	sloužit	k5eAaImIp3nP	sloužit
prasečí	prasečí	k2eAgFnPc4d1	prasečí
hlavy	hlava	k1gFnPc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Pravřídlo	Pravřídnout	k5eAaPmAgNnS	Pravřídnout
<g/>
,	,	kIx,	,
kamenný	kamenný	k2eAgInSc4d1	kamenný
výklenek	výklenek	k1gInSc4	výklenek
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
pramenem	pramen	k1gInSc7	pramen
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
pod	pod	k7c7	pod
kostelem	kostel	k1gInSc7	kostel
Pomník	pomník	k1gInSc4	pomník
německého	německý	k2eAgMnSc2d1	německý
básníka	básník	k1gMnSc2	básník
J.	J.	kA	J.
G.	G.	kA	G.
Seumeho	Seume	k1gMnSc2	Seume
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
,	,	kIx,	,
v	v	k7c6	v
Havlíčkových	Havlíčkových	k2eAgInPc6d1	Havlíčkových
sadech	sad	k1gInPc6	sad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
panském	panský	k2eAgInSc6d1	panský
špitálu	špitál	k1gInSc6	špitál
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1674	[number]	k4	1674
založena	založen	k2eAgFnSc1d1	založena
Loretánská	loretánský	k2eAgFnSc1d1	Loretánská
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Postavena	postaven	k2eAgFnSc1d1	postavena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
zaniklého	zaniklý	k2eAgInSc2d1	zaniklý
kostela	kostel	k1gInSc2	kostel
zasvěceného	zasvěcený	k2eAgInSc2d1	zasvěcený
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Marek	Marek	k1gMnSc1	Marek
Clary-Aldringen	Clary-Aldringen	k1gInSc4	Clary-Aldringen
<g/>
.	.	kIx.	.
</s>
<s>
Santa	Santa	k1gFnSc1	Santa
Casa	Casa	k1gFnSc1	Casa
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
zbořena	zbořit	k5eAaPmNgFnS	zbořit
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Meißner	Meißner	k1gMnSc1	Meißner
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
-	-	kIx~	-
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
August	August	k1gMnSc1	August
Stradal	Stradal	k1gMnSc1	Stradal
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
Hermann	Hermann	k1gMnSc1	Hermann
Hallwich	Hallwich	k1gMnSc1	Hallwich
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
politik	politik	k1gMnSc1	politik
Gustav	Gustav	k1gMnSc1	Gustav
Karl	Karl	k1gMnSc1	Karl
Laube	Laub	k1gInSc5	Laub
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
-	-	kIx~	-
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
geolog	geolog	k1gMnSc1	geolog
a	a	k8xC	a
paleontolog	paleontolog	k1gMnSc1	paleontolog
Julius	Julius	k1gMnSc1	Julius
von	von	k1gInSc4	von
Payer	Payer	k1gInSc1	Payer
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kartograf	kartograf	k1gMnSc1	kartograf
<g/>
,	,	kIx,	,
polární	polární	k2eAgMnSc1d1	polární
badatel	badatel	k1gMnSc1	badatel
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
Emanuel	Emanuel	k1gMnSc1	Emanuel
Kudela	Kudela	k1gFnSc1	Kudela
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklista	cyklista	k1gMnSc1	cyklista
Paul	Paul	k1gMnSc1	Paul
Kohner	Kohner	k1gMnSc1	Kohner
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
agent	agent	k1gMnSc1	agent
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Dušan	Dušan	k1gMnSc1	Dušan
Třeštík	Třeštík	k1gMnSc1	Třeštík
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgInSc1d1	narozen
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Sobědruhy	Sobědruha	k1gFnSc2	Sobědruha
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
</s>
