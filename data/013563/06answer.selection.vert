<s desamb="1">
Kulové	kulový	k2eAgFnPc4d1
hvězdokupy	hvězdokupa	k1gFnPc4
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
staré	starý	k2eAgFnPc1d1
<g/>
,	,	kIx,
nejméně	málo	k6eAd3
10	#num#	k4
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
galaxie	galaxie	k1gFnPc1
právě	právě	k9
vytvářely	vytvářet	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>