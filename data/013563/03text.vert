<s>
Kulová	kulový	k2eAgFnSc1d1
hvězdokupa	hvězdokupa	k1gFnSc1
</s>
<s>
Kulová	kulový	k2eAgFnSc1d1
hvězdokupa	hvězdokupa	k1gFnSc1
47	#num#	k4
Tucanae	Tucana	k1gInSc2
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Tukan	tukan	k1gMnSc1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
jsou	být	k5eAaImIp3nP
kulovité	kulovitý	k2eAgFnPc1d1
koncentrace	koncentrace	k1gFnPc1
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
průměr	průměr	k1gInSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
řádově	řádově	k6eAd1
v	v	k7c6
desítkách	desítka	k1gFnPc6
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
a	a	k8xC
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
obsahují	obsahovat	k5eAaImIp3nP
statisíce	statisíce	k1gInPc4
až	až	k8xS
miliony	milion	k4xCgInPc4
hvězd	hvězda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulové	kulový	k2eAgFnPc4d1
hvězdokupy	hvězdokupa	k1gFnPc4
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
staré	starý	k2eAgFnPc1d1
<g/>
,	,	kIx,
nejméně	málo	k6eAd3
10	#num#	k4
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
galaxie	galaxie	k1gFnPc1
právě	právě	k9
vytvářely	vytvářet	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
rozptýleny	rozptýlit	k5eAaPmNgFnP
ve	v	k7c6
sférickém	sférický	k2eAgNnSc6d1
halu	halo	k1gNnSc6
obklopujícím	obklopující	k2eAgNnSc6d1
naši	náš	k3xOp1gFnSc4
Galaxii	galaxie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
kolem	kolem	k7c2
středu	střed	k1gInSc2
Galaxie	galaxie	k1gFnSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
řádech	řád	k1gInPc6
stovek	stovka	k1gFnPc2
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
a	a	k8xC
souhvězdí	souhvězdí	k1gNnPc1
</s>
<s>
Rozložení	rozložení	k1gNnSc1
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
v	v	k7c6
naší	náš	k3xOp1gFnSc6
Galaxii	galaxie	k1gFnSc6
je	být	k5eAaImIp3nS
koncentrováno	koncentrovat	k5eAaBmNgNnS
okolo	okolo	k7c2
galaktického	galaktický	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
v	v	k7c6
oblastech	oblast	k1gFnPc6
souhvězdí	souhvězdí	k1gNnSc2
Střelce	Střelec	k1gMnSc2
(	(	kIx(
<g/>
Sagittarius	Sagittarius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Štíra	štír	k1gMnSc4
(	(	kIx(
<g/>
Scorpius	Scorpius	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
Hadonoše	hadonoš	k1gMnSc2
(	(	kIx(
<g/>
Ophiuchus	Ophiuchus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
138	#num#	k4
hvězdokup	hvězdokupa	k1gFnPc2
Galaxie	galaxie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
nalézají	nalézat	k5eAaImIp3nP
v	v	k7c6
katalogu	katalog	k1gInSc6
„	„	k?
<g/>
Sky	Sky	k1gMnSc1
Catalog	Catalog	k1gMnSc1
2000.0	2000.0	k4
<g/>
“	“	k?
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
souhvězdí	souhvězdí	k1gNnSc4
Střelec	Střelec	k1gMnSc1
29	#num#	k4
<g/>
,	,	kIx,
Štír	štír	k1gMnSc1
18	#num#	k4
a	a	k8xC
Hadonoš	hadonoš	k1gMnSc1
24	#num#	k4
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
71	#num#	k4
hvězdokup	hvězdokupa	k1gFnPc2
neboli	neboli	k8xC
51,4	51,4	k4
procent	procento	k1gNnPc2
(	(	kIx(
<g/>
ovšem	ovšem	k9
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
připustit	připustit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
29	#num#	k4
hvězdokup	hvězdokupa	k1gFnPc2
ze	z	k7c2
souhvězdí	souhvězdí	k1gNnSc2
Střelce	Střelec	k1gMnSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
pravděpodobně	pravděpodobně	k6eAd1
4	#num#	k4
členové	člen	k1gMnPc1
trpasličí	trpasličí	k2eAgFnSc2d1
eliptické	eliptický	k2eAgFnSc2d1
galaxie	galaxie	k1gFnSc2
Sagittarius	Sagittarius	k1gMnSc1
objevené	objevený	k2eAgFnSc2d1
roku	rok	k1gInSc2
1994	#num#	k4
a	a	k8xC
nejsou	být	k5eNaImIp3nP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
z	z	k7c2
Galaxie	galaxie	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
i	i	k9
M	M	kA
<g/>
54	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nedávné	dávný	k2eNgNnSc1d1
sčítání	sčítání	k1gNnSc1
(	(	kIx(
<g/>
červen	červen	k1gInSc1
2002	#num#	k4
<g/>
)	)	kIx)
tyto	tento	k3xDgInPc4
počty	počet	k1gInPc4
změnilo	změnit	k5eAaPmAgNnS
takto	takto	k6eAd1
<g/>
:	:	kIx,
33	#num#	k4
(	(	kIx(
<g/>
Střelec	Střelec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
19	#num#	k4
(	(	kIx(
<g/>
Štír	štír	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
25	#num#	k4
(	(	kIx(
<g/>
Hadonoš	hadonoš	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dohromady	dohromady	k6eAd1
77	#num#	k4
hvězdokup	hvězdokupa	k1gFnPc2
z	z	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
149	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
51,7	51,7	k4
procenta	procento	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
88	#num#	k4
souhvězdí	souhvězdí	k1gNnPc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
43	#num#	k4
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
polovina	polovina	k1gFnSc1
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
několik	několik	k4yIc4
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
;	;	kIx,
Orel	Orel	k1gMnSc1
(	(	kIx(
<g/>
Aquila	Aquila	k1gFnSc1
<g/>
)	)	kIx)
obsahuje	obsahovat	k5eAaImIp3nS
6	#num#	k4
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
,	,	kIx,
Had	had	k1gMnSc1
(	(	kIx(
<g/>
Serpens	Serpens	k1gInSc1
<g/>
)	)	kIx)
5	#num#	k4
a	a	k8xC
Herkules	Herkules	k1gMnSc1
(	(	kIx(
<g/>
Hercules	Hercules	k1gMnSc1
<g/>
)	)	kIx)
4	#num#	k4
hvězdokupy	hvězdokupa	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
žádné	žádný	k3yNgNnSc4
další	další	k2eAgNnSc4d1
souhvězdí	souhvězdí	k1gNnSc4
neobsahuje	obsahovat	k5eNaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
3	#num#	k4
hvězdokupy	hvězdokupa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
149	#num#	k4
známých	známý	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
,	,	kIx,
136	#num#	k4
(	(	kIx(
<g/>
91,3	91,3	k4
procenta	procento	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
koncentrováno	koncentrovat	k5eAaBmNgNnS
v	v	k7c6
polokouli	polokoule	k1gFnSc6
soustředěné	soustředěný	k2eAgFnSc6d1
okolo	okolo	k7c2
souhvězdí	souhvězdí	k1gNnPc2
Střelce	Střelec	k1gMnSc2
(	(	kIx(
<g/>
Sagittarius	Sagittarius	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pouze	pouze	k6eAd1
13	#num#	k4
hvězdokup	hvězdokupa	k1gFnPc2
(	(	kIx(
<g/>
8,7	8,7	k4
procenta	procento	k1gNnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
od	od	k7c2
nás	my	k3xPp1nPc2
(	(	kIx(
<g/>
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
M	M	kA
<g/>
79	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
zřetelná	zřetelný	k2eAgFnSc1d1
anizotropie	anizotropie	k1gFnSc1
v	v	k7c6
rozložení	rozložení	k1gNnSc6
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
měla	mít	k5eAaImAgFnS
historický	historický	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
z	z	k7c2
toho	ten	k3xDgMnSc2
Harlow	Harlow	k1gMnSc2
Shapley	Shaplea	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
odvodil	odvodit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
střed	střed	k1gInSc1
naší	náš	k3xOp1gFnSc2
galaxie	galaxie	k1gFnSc2
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
značné	značný	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
ve	v	k7c6
směru	směr	k1gInSc6
souhvězdí	souhvězdí	k1gNnSc2
Střelce	Střelec	k1gMnSc2
(	(	kIx(
<g/>
Sagittarius	Sagittarius	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
není	být	k5eNaImIp3nS
blízko	blízko	k7c2
našeho	náš	k3xOp1gInSc2
slunečního	sluneční	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
dříve	dříve	k6eAd2
domnívali	domnívat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
Měření	měření	k1gNnSc1
radiální	radiální	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
prozradilo	prozradit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
většina	většina	k1gFnSc1
hvězdokup	hvězdokupa	k1gFnPc2
pohybuje	pohybovat	k5eAaImIp3nS
ve	v	k7c6
vysoce	vysoce	k6eAd1
excentrických	excentrický	k2eAgFnPc6d1
eliptických	eliptický	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
zabírají	zabírat	k5eAaImIp3nP
vnější	vnější	k2eAgFnPc4d1
prostory	prostora	k1gFnPc4
Galaxie	galaxie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
halo	halo	k1gNnSc4
zhruba	zhruba	k6eAd1
kulového	kulový	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
koncentrováno	koncentrován	k2eAgNnSc1d1
ke	k	k7c3
galaktickému	galaktický	k2eAgInSc3d1
středu	střed	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
dosahuje	dosahovat	k5eAaImIp3nS
do	do	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
několika	několik	k4yIc2
set	sto	k4xCgNnPc2
tisíc	tisíc	k4xCgInSc1
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
rozměry	rozměr	k1gInPc4
galaktického	galaktický	k2eAgInSc2d1
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepodílejí	podílet	k5eNaImIp3nP
se	se	k3xPyFc4
na	na	k7c6
rotaci	rotace	k1gFnSc6
galaktického	galaktický	k2eAgInSc2d1
disku	disk	k1gInSc2
a	a	k8xC
proto	proto	k8xC
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
relativně	relativně	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
několika	několik	k4yIc2
set	set	k1gInSc4
km	km	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
vzhledem	vzhled	k1gInSc7
k	k	k7c3
naší	náš	k3xOp1gFnSc3
sluneční	sluneční	k2eAgFnSc3d1
soustavě	soustava	k1gFnSc3
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
projevilo	projevit	k5eAaPmAgNnS
při	při	k7c6
měření	měření	k1gNnSc6
radiální	radiální	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Galaktické	galaktický	k2eAgNnSc1d1
halo	halo	k1gNnSc1
a	a	k8xC
kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
</s>
<s>
Galaktické	galaktický	k2eAgNnSc1d1
halo	halo	k1gNnSc1
je	být	k5eAaImIp3nS
kulový	kulový	k2eAgInSc1d1
útvar	útvar	k1gInSc1
tvořený	tvořený	k2eAgInSc1d1
starými	starý	k2eAgFnPc7d1
hvězdami	hvězda	k1gFnPc7
a	a	k8xC
kulovými	kulový	k2eAgFnPc7d1
hvězdokupami	hvězdokupa	k1gFnPc7
<g/>
,	,	kIx,
obklopující	obklopující	k2eAgFnSc3d1
galaxii	galaxie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poloměr	poloměr	k1gInSc1
tohoto	tento	k3xDgInSc2
útvaru	útvar	k1gInSc2
je	být	k5eAaImIp3nS
asi	asi	k9
50	#num#	k4
000	#num#	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Složení	složení	k1gNnSc1
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
</s>
<s>
Spektroskopické	spektroskopický	k2eAgInPc1d1
výzkumy	výzkum	k1gInPc1
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
obsahují	obsahovat	k5eAaImIp3nP
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
těžkých	těžký	k2eAgInPc2d1
prvků	prvek	k1gInPc2
než	než	k8xS
hvězdy	hvězda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
vytvářely	vytvářet	k5eAaImAgFnP
v	v	k7c6
disku	disk	k1gInSc6
galaxie	galaxie	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
naše	náš	k3xOp1gNnSc1
Slunce	slunce	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
velmi	velmi	k6eAd1
staré	starý	k2eAgMnPc4d1
a	a	k8xC
skládají	skládat	k5eAaImIp3nP
se	se	k3xPyFc4
z	z	k7c2
počátečních	počáteční	k2eAgFnPc2d1
generací	generace	k1gFnPc2
hvězd	hvězda	k1gFnPc2
(	(	kIx(
<g/>
hvězdy	hvězda	k1gFnSc2
populace	populace	k1gFnSc2
II	II	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
vytvořily	vytvořit	k5eAaPmAgFnP
z	z	k7c2
prvotního	prvotní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
v	v	k7c6
mladé	mladý	k2eAgFnSc6d1
galaxii	galaxie	k1gFnSc6
právě	právě	k9
v	v	k7c6
době	doba	k1gFnSc6
(	(	kIx(
<g/>
nebo	nebo	k8xC
před	před	k7c7
<g/>
)	)	kIx)
jejich	jejich	k3xOp3gNnSc7
formováním	formování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdy	Hvězda	k1gMnPc7
v	v	k7c6
disku	disk	k1gInSc6
naopak	naopak	k6eAd1
procházejí	procházet	k5eAaImIp3nP
mnoha	mnoho	k4c2
cykly	cyklus	k1gInPc1
zrodu	zrod	k1gInSc2
hvězd	hvězda	k1gFnPc2
a	a	k8xC
supernov	supernova	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
koncentraci	koncentrace	k1gFnSc4
těžkých	těžký	k2eAgInPc2d1
prvků	prvek	k1gInPc2
v	v	k7c6
mračnech	mračno	k1gNnPc6
tvorby	tvorba	k1gFnSc2
hvězd	hvězda	k1gFnPc2
a	a	k8xC
možná	možná	k9
také	také	k9
spouští	spouštět	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc3
zhroucení	zhroucení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
často	často	k6eAd1
obsahují	obsahovat	k5eAaImIp3nP
množství	množství	k1gNnSc4
proměnných	proměnný	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
hvězdy	hvězda	k1gFnSc2
typu	typ	k1gInSc2
RR	RR	kA
Lyrae	Lyra	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
kdysi	kdysi	k6eAd1
nazývány	nazývat	k5eAaImNgFnP
„	„	k?
<g/>
proměnné	proměnný	k2eAgFnSc2d1
hvězdokupy	hvězdokupa	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
hojnosti	hojnost	k1gFnSc3
v	v	k7c6
hvězdokupách	hvězdokupa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc1
výskyt	výskyt	k1gInSc1
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
od	od	k7c2
hvězdokupy	hvězdokupa	k1gFnSc2
ke	k	k7c3
hvězdokupě	hvězdokupa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
několika	několik	k4yIc6
kulových	kulový	k2eAgFnPc6d1
hvězdokupách	hvězdokupa	k1gFnPc6
byly	být	k5eAaImAgFnP
zjištěny	zjistit	k5eAaPmNgFnP
planetární	planetární	k2eAgFnPc1d1
mlhoviny	mlhovina	k1gFnPc1
<g/>
;	;	kIx,
zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
výjimečný	výjimečný	k2eAgInSc4d1
jev	jev	k1gInSc4
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
životnost	životnost	k1gFnSc4
planetárních	planetární	k2eAgFnPc2d1
mlhovin	mlhovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kulové	kulový	k2eAgFnPc4d1
hvězdokupy	hvězdokupa	k1gFnPc4
obsahují	obsahovat	k5eAaImIp3nP
také	také	k9
značný	značný	k2eAgInSc4d1
počet	počet	k1gInSc4
bílých	bílý	k2eAgMnPc2d1
trpaslíků	trpaslík	k1gMnPc2
a	a	k8xC
menší	malý	k2eAgNnSc1d2
množství	množství	k1gNnSc1
neutronových	neutronový	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
některé	některý	k3yIgFnPc1
se	se	k3xPyFc4
projevují	projevovat	k5eAaImIp3nP
jako	jako	k9
pulsary	pulsar	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
HR	hr	k2eAgInSc1d1
diagram	diagram	k1gInSc1
(	(	kIx(
<g/>
Hertzsprungův-Russellův	Hertzsprungův-Russellův	k2eAgInSc1d1
diagram	diagram	k1gInSc1
<g/>
)	)	kIx)
pro	pro	k7c4
kulové	kulový	k2eAgFnPc4d1
hvězdokupy	hvězdokupa	k1gFnPc4
má	mít	k5eAaImIp3nS
typicky	typicky	k6eAd1
krátkou	krátký	k2eAgFnSc4d1
hlavní	hlavní	k2eAgFnSc4d1
posloupnost	posloupnost	k1gFnSc4
a	a	k8xC
nápadné	nápadný	k2eAgFnPc1d1
horizontální	horizontální	k2eAgFnPc1d1
větve	větev	k1gFnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
opět	opět	k6eAd1
představuje	představovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
staré	starý	k2eAgFnPc4d1
hvězdy	hvězda	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
vyvinuly	vyvinout	k5eAaPmAgFnP
do	do	k7c2
fáze	fáze	k1gFnSc2
obrů	obr	k1gMnPc2
a	a	k8xC
nadobrů	nadobr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Porovnání	porovnání	k1gNnSc1
naměřených	naměřený	k2eAgInPc2d1
HR	hr	k2eAgInPc2d1
diagramů	diagram	k1gInPc2
pro	pro	k7c4
každou	každý	k3xTgFnSc4
kulovou	kulový	k2eAgFnSc4d1
hvězdokupu	hvězdokupa	k1gFnSc4
s	s	k7c7
teoretickým	teoretický	k2eAgInSc7d1
modelem	model	k1gInSc7
HR	hr	k2eAgInSc2d1
diagramu	diagram	k1gInSc2
založeném	založený	k2eAgInSc6d1
na	na	k7c6
teorii	teorie	k1gFnSc6
hvězdného	hvězdný	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
poskytuje	poskytovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
odvodit	odvodit	k5eAaPmF
nebo	nebo	k8xC
odhadnout	odhadnout	k5eAaPmF
stáří	stáří	k1gNnSc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
překvapující	překvapující	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
všechny	všechen	k3xTgFnPc1
dosud	dosud	k6eAd1
pozorované	pozorovaný	k2eAgFnPc1d1
kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
jsou	být	k5eAaImIp3nP
přibližně	přibližně	k6eAd1
stejně	stejně	k6eAd1
staré	starý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdá	zdát	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
nějakou	nějaký	k3yIgFnSc4
fyzikální	fyzikální	k2eAgFnSc4d1
zákonitost	zákonitost	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
všechny	všechen	k3xTgFnPc1
vytvořily	vytvořit	k5eAaPmAgFnP
v	v	k7c6
relativně	relativně	k6eAd1
krátkém	krátký	k2eAgNnSc6d1
období	období	k1gNnSc6
historie	historie	k1gFnSc2
vesmíru	vesmír	k1gInSc2
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
galaxie	galaxie	k1gFnPc1
velmi	velmi	k6eAd1
mladé	mladý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedávné	dávný	k2eNgInPc1d1
odhady	odhad	k1gInPc1
stáří	stáří	k1gNnSc2
se	se	k3xPyFc4
přiklánějí	přiklánět	k5eAaImIp3nP
k	k	k7c3
intervalu	interval	k1gInSc3
od	od	k7c2
12	#num#	k4
do	do	k7c2
20	#num#	k4
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
lepší	dobrý	k2eAgFnSc1d2
hodnota	hodnota	k1gFnSc1
z	z	k7c2
pozorování	pozorování	k1gNnSc2
je	být	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
12	#num#	k4
až	až	k9
16	#num#	k4
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
horní	horní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
souvisí	souviset	k5eAaImIp3nS
se	s	k7c7
stářím	stáří	k1gNnSc7
našeho	náš	k3xOp1gInSc2
vesmíru	vesmír	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
vedou	vést	k5eAaImIp3nP
diskuse	diskuse	k1gFnPc1
<g/>
,	,	kIx,
kolik	kolik	k9
to	ten	k3xDgNnSc1
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
je	být	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1997	#num#	k4
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
komplexní	komplexní	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
délkových	délkový	k2eAgNnPc2d1
měřítek	měřítko	k1gNnPc2
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
opírala	opírat	k5eAaImAgFnS
o	o	k7c4
výsledky	výsledek	k1gInPc4
z	z	k7c2
astrometrického	astrometrický	k2eAgInSc2d1
satelitu	satelit	k1gInSc2
Hipparcos	Hipparcosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
výsledky	výsledek	k1gInPc1
naznačily	naznačit	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
galaxie	galaxie	k1gFnSc1
a	a	k8xC
mnoho	mnoho	k4c1
galaktických	galaktický	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
o	o	k7c4
deset	deset	k4xCc4
procent	procento	k1gNnPc2
dále	daleko	k6eAd2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
skutečná	skutečný	k2eAgFnSc1d1
jasnost	jasnost	k1gFnSc1
všech	všecek	k3xTgFnPc2
jejich	jejich	k3xOp3gFnPc2
hvězd	hvězda	k1gFnPc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
o	o	k7c4
20	#num#	k4
%	%	kIx~
vyšší	vysoký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
různé	různý	k2eAgFnPc4d1
závislosti	závislost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
významné	významný	k2eAgFnPc1d1
pro	pro	k7c4
pochopení	pochopení	k1gNnSc4
hvězdné	hvězdný	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
a	a	k8xC
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
kulové	kulový	k2eAgFnPc4d1
hvězdokupy	hvězdokupa	k1gFnPc4
zhruba	zhruba	k6eAd1
o	o	k7c4
15	#num#	k4
%	%	kIx~
mladší	mladý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
pohybují	pohybovat	k5eAaImIp3nP
zhruba	zhruba	k6eAd1
miliardu	miliarda	k4xCgFnSc4
let	léto	k1gNnPc2
po	po	k7c6
oběžné	oběžný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
okolo	okolo	k7c2
galaktického	galaktický	k2eAgInSc2d1
středu	střed	k1gInSc2
Galaxie	galaxie	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
náchylné	náchylný	k2eAgFnPc1d1
k	k	k7c3
různým	různý	k2eAgFnPc3d1
poruchám	porucha	k1gFnPc3
<g/>
:	:	kIx,
</s>
<s>
některé	některý	k3yIgFnPc1
jejich	jejich	k3xOp3gFnPc1
hvězdy	hvězda	k1gFnPc1
unikají	unikat	k5eAaImIp3nP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
dostávají	dostávat	k5eAaImIp3nP
náhodné	náhodný	k2eAgNnSc4d1
zrychlení	zrychlení	k1gNnSc4
při	při	k7c6
vzájemných	vzájemný	k2eAgInPc6d1
setkání	setkání	k1gNnSc6
<g/>
,	,	kIx,
</s>
<s>
slapová	slapový	k2eAgFnSc1d1
síla	síla	k1gFnSc1
z	z	k7c2
mateřské	mateřský	k2eAgFnSc2d1
galaxie	galaxie	k1gFnSc2
na	na	k7c4
ně	on	k3xPp3gMnPc4
působí	působit	k5eAaImIp3nP
<g/>
,	,	kIx,
zejména	zejména	k9
silné	silný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
působení	působení	k1gNnSc1
v	v	k7c6
místech	místo	k1gNnPc6
jejich	jejich	k3xOp3gFnSc2
dráhy	dráha	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
nejblíže	blízce	k6eAd3
galaktickému	galaktický	k2eAgInSc3d1
středu	střed	k1gInSc3
<g/>
,	,	kIx,
</s>
<s>
každý	každý	k3xTgInSc4
průchod	průchod	k1gInSc4
přes	přes	k7c4
galaktický	galaktický	k2eAgInSc4d1
rovník	rovník	k1gInSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
blízká	blízký	k2eAgFnSc1d1
srážka	srážka	k1gFnSc1
s	s	k7c7
větší	veliký	k2eAgFnSc7d2
hmotností	hmotnost	k1gFnSc7
podobné	podobný	k2eAgFnSc2d1
hvězdokupy	hvězdokupa	k1gFnSc2
nebo	nebo	k8xC
velkého	velký	k2eAgNnSc2d1
mlhovinového	mlhovinový	k2eAgNnSc2d1
mračna	mračno	k1gNnSc2
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
poruchám	porucha	k1gFnPc3
<g/>
,	,	kIx,
</s>
<s>
hvězdné	hvězdný	k2eAgNnSc1d1
evoluční	evoluční	k2eAgNnSc1d1
působení	působení	k1gNnSc1
a	a	k8xC
ztráta	ztráta	k1gFnSc1
plynu	plyn	k1gInSc2
také	také	k9
přispívá	přispívat	k5eAaImIp3nS
ke	k	k7c3
snižování	snižování	k1gNnSc3
hmotnosti	hmotnost	k1gFnSc2
hvězdokupy	hvězdokupa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgFnPc1
poruchy	porucha	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
porovnáním	porovnání	k1gNnSc7
s	s	k7c7
méně	málo	k6eAd2
zhuštěnými	zhuštěný	k2eAgFnPc7d1
a	a	k8xC
méně	málo	k6eAd2
početnějšími	početní	k2eAgFnPc7d2
otevřenými	otevřený	k2eAgFnPc7d1
hvězdokupami	hvězdokupa	k1gFnPc7
podstatně	podstatně	k6eAd1
pomalejší	pomalý	k2eAgFnSc1d2
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k8xC
směřují	směřovat	k5eAaImIp3nP
k	k	k7c3
rozpadu	rozpad	k1gInSc3
hvězdokupy	hvězdokupa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
existující	existující	k2eAgFnPc4d1
hvězdokupy	hvězdokupa	k1gFnPc4
jsou	být	k5eAaImIp3nP
pravděpodobně	pravděpodobně	k6eAd1
zbytkem	zbytek	k1gInSc7
podstatně	podstatně	k6eAd1
větší	veliký	k2eAgFnSc2d2
populace	populace	k1gFnSc2
<g/>
;	;	kIx,
zbytek	zbytek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
rozložil	rozložit	k5eAaPmAgMnS
a	a	k8xC
rozprostřel	rozprostřít	k5eAaPmAgMnS
jejich	jejich	k3xOp3gFnPc4
hvězdy	hvězda	k1gFnPc4
po	po	k7c6
galaktickém	galaktický	k2eAgNnSc6d1
halo	halo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc1
zkázy	zkáza	k1gFnSc2
stále	stále	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
polovina	polovina	k1gFnSc1
hvězdokup	hvězdokupa	k1gFnPc2
Galaxie	galaxie	k1gFnSc1
přestane	přestat	k5eAaPmIp3nS
existovat	existovat	k5eAaImF
během	během	k7c2
10	#num#	k4
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
mimo	mimo	k7c4
naší	náš	k3xOp1gFnSc2
Galaxii	galaxie	k1gFnSc6
</s>
<s>
Naše	náš	k3xOp1gFnSc1
Galaxie	galaxie	k1gFnSc1
má	mít	k5eAaImIp3nS
soustavu	soustava	k1gFnSc4
asi	asi	k9
přibližně	přibližně	k6eAd1
180	#num#	k4
až	až	k9
200	#num#	k4
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
28	#num#	k4
z	z	k7c2
29	#num#	k4
Messierových	Messierův	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
M	M	kA
<g/>
54	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
ostatních	ostatní	k2eAgFnPc2d1
galaxií	galaxie	k1gFnPc2
má	mít	k5eAaImIp3nS
také	také	k9
soustavu	soustava	k1gFnSc4
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
(	(	kIx(
<g/>
například	například	k6eAd1
M	M	kA
<g/>
87	#num#	k4
<g/>
)	)	kIx)
obsahující	obsahující	k2eAgFnSc2d1
několik	několik	k4yIc4
tisíc	tisíc	k4xCgInPc2
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
malý	malý	k2eAgInSc1d1
počet	počet	k1gInSc1
mimogalaktických	mimogalaktický	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
dosahu	dosah	k1gInSc6
větších	veliký	k2eAgInPc2d2
amatérských	amatérský	k2eAgInPc2d1
dalekohledů	dalekohled	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zatímco	zatímco	k8xS
všechny	všechen	k3xTgFnPc4
kulové	kulový	k2eAgFnPc4d1
hvězdokupy	hvězdokupa	k1gFnPc4
v	v	k7c6
naší	náš	k3xOp1gFnSc6
Galaxii	galaxie	k1gFnSc6
a	a	k8xC
v	v	k7c6
našem	náš	k3xOp1gInSc6
větším	většit	k5eAaImIp1nS
průvodci	průvodce	k1gMnPc1
<g/>
,	,	kIx,
galaxii	galaxie	k1gFnSc4
v	v	k7c6
Andromedě	Andromed	k1gInSc6
M	M	kA
<g/>
31	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
staré	starý	k2eAgFnPc1d1
<g/>
,	,	kIx,
další	další	k2eAgFnPc1d1
galaxie	galaxie	k1gFnPc1
Místní	místní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
-	-	kIx~
Velké	velký	k2eAgNnSc4d1
a	a	k8xC
Malé	Malé	k2eAgNnSc4d1
Magellanovo	Magellanův	k2eAgNnSc4d1
mračno	mračno	k1gNnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
galaxie	galaxie	k1gFnSc2
M33	M33	k1gFnSc2
v	v	k7c6
Trojúhelníku	trojúhelník	k1gInSc6
také	také	k9
obsahují	obsahovat	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
mladší	mladý	k2eAgFnPc4d2
kulové	kulový	k2eAgFnPc4d1
hvězdokupy	hvězdokupa	k1gFnPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
s	s	k7c7
jistotou	jistota	k1gFnSc7
usuzováno	usuzován	k2eAgNnSc1d1
ze	z	k7c2
spektroskopických	spektroskopický	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgFnPc1
galaxie	galaxie	k1gFnPc1
také	také	k9
obsahují	obsahovat	k5eAaImIp3nP
extrémně	extrémně	k6eAd1
rozsáhlé	rozsáhlý	k2eAgFnPc1d1
difúzní	difúzní	k2eAgFnPc1d1
mlhoviny	mlhovina	k1gFnPc1
s	s	k7c7
hmotností	hmotnost	k1gFnSc7
řádově	řádově	k6eAd1
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
,	,	kIx,
jasné	jasný	k2eAgFnPc4d1
kandidáty	kandidát	k1gMnPc7
pro	pro	k7c4
budoucí	budoucí	k2eAgMnPc4d1
mladé	mladý	k1gMnPc4
hvězdokupy	hvězdokupa	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
ve	v	k7c6
stádiu	stádium	k1gNnSc6
tvoření	tvoření	k1gNnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
mlhovina	mlhovina	k1gFnSc1
Tarantule	tarantule	k1gFnSc1
(	(	kIx(
<g/>
NGC	NGC	kA
2070	#num#	k4
<g/>
)	)	kIx)
ve	v	k7c6
Velkém	velký	k2eAgNnSc6d1
Magellanově	Magellanův	k2eAgNnSc6d1
mračnu	mračno	k1gNnSc6
a	a	k8xC
NGC	NGC	kA
604	#num#	k4
v	v	k7c6
M	M	kA
<g/>
33	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velká	k1gFnSc2
množství	množství	k1gNnSc2
<g/>
,	,	kIx,
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
mladých	mladý	k2eAgFnPc2d1
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
detekováno	detekovat	k5eAaImNgNnS
v	v	k7c6
M	M	kA
<g/>
82	#num#	k4
<g/>
,	,	kIx,
nepravidelné	pravidelný	k2eNgFnSc6d1
galaxii	galaxie	k1gFnSc6
za	za	k7c7
Místní	místní	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Nejjasnější	jasný	k2eAgFnPc1d3
kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
v	v	k7c6
osmi	osm	k4xCc6
blízkých	blízký	k2eAgFnPc6d1
galaxiích	galaxie	k1gFnPc6
</s>
<s>
GalaxieHvězdokupaRektascenzeDeklinaceMagnituda	GalaxieHvězdokupaRektascenzeDeklinaceMagnituda	k1gFnSc1
</s>
<s>
Fornax	Fornax	k1gInSc1
DwarfNGC	DwarfNGC	k1gFnSc1
104902	#num#	k4
<g/>
h	h	k?
39	#num#	k4
<g/>
m	m	kA
49	#num#	k4
<g/>
s	s	k7c7
<g/>
+	+	kIx~
<g/>
34	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
'	'	kIx"
29	#num#	k4
<g/>
"	"	kIx"
<g/>
12,6	12,6	k4
</s>
<s>
M	M	kA
31	#num#	k4
<g/>
Mayall	Mayall	k1gInSc4
II	II	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
h	h	k?
32	#num#	k4
<g/>
m	m	kA
47	#num#	k4
<g/>
s	s	k7c7
<g/>
+	+	kIx~
<g/>
39	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
'	'	kIx"
41	#num#	k4
<g/>
"	"	kIx"
<g/>
13,7	13,7	k4
</s>
<s>
M	M	kA
110	#num#	k4
<g/>
G	G	kA
<g/>
7300	#num#	k4
<g/>
h	h	k?
40	#num#	k4
<g/>
m	m	kA
55	#num#	k4
<g/>
s	s	k7c7
<g/>
+	+	kIx~
<g/>
41	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
'	'	kIx"
15	#num#	k4
<g/>
"	"	kIx"
<g/>
15,0	15,0	k4
</s>
<s>
M	M	kA
33	#num#	k4
<g/>
C	C	kA
<g/>
3901	#num#	k4
<g/>
h	h	k?
34	#num#	k4
<g/>
m	m	kA
50	#num#	k4
<g/>
s	s	k7c7
<g/>
+	+	kIx~
<g/>
30	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
'	'	kIx"
56	#num#	k4
<g/>
"	"	kIx"
<g/>
15,9	15,9	k4
</s>
<s>
WLMWLM	WLMWLM	kA
100	#num#	k4
<g/>
h	h	k?
0	#num#	k4
<g/>
1	#num#	k4
<g/>
m	m	kA
50	#num#	k4
<g/>
s-	s-	k?
<g/>
15	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
'	'	kIx"
31	#num#	k4
<g/>
"	"	kIx"
<g/>
16,1	16,1	k4
</s>
<s>
NGC	NGC	kA
185	#num#	k4
<g/>
Hodge	Hodg	k1gInSc2
500	#num#	k4
<g/>
h	h	k?
39	#num#	k4
<g/>
m	m	kA
14	#num#	k4
<g/>
s	s	k7c7
<g/>
+	+	kIx~
<g/>
48	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
'	'	kIx"
0	#num#	k4
<g/>
6	#num#	k4
<g/>
"	"	kIx"
<g/>
16,7	16,7	k4
</s>
<s>
NGC	NGC	kA
147	#num#	k4
<g/>
Hodge	Hodg	k1gInSc2
300	#num#	k4
<g/>
h	h	k?
33	#num#	k4
<g/>
m	m	kA
15	#num#	k4
<g/>
s	s	k7c7
<g/>
+	+	kIx~
<g/>
48	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
'	'	kIx"
23	#num#	k4
<g/>
"	"	kIx"
<g/>
17,0	17,0	k4
</s>
<s>
NGC	NGC	kA
2403	#num#	k4
<g/>
F	F	kA
<g/>
4607	#num#	k4
<g/>
h	h	k?
36	#num#	k4
<g/>
m	m	kA
29	#num#	k4
<g/>
s	s	k7c7
<g/>
+	+	kIx~
<g/>
65	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
'	'	kIx"
30	#num#	k4
<g/>
"	"	kIx"
<g/>
17,9	17,9	k4
</s>
<s>
Pohyb	pohyb	k1gInSc1
hvězdokupy	hvězdokupa	k1gFnSc2
</s>
<s>
Hvězdy	hvězda	k1gFnPc1
ve	v	k7c6
hvězdokupách	hvězdokupa	k1gFnPc6
vykonávají	vykonávat	k5eAaImIp3nP
řadu	řada	k1gFnSc4
pohybů	pohyb	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
hvězdy	hvězda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
tvoří	tvořit	k5eAaImIp3nP
kulové	kulový	k2eAgFnPc1d1
hvězdokupy	hvězdokupa	k1gFnPc1
ale	ale	k8xC
i	i	k9
v	v	k7c6
mnohých	mnohý	k2eAgFnPc6d1
otevřených	otevřený	k2eAgFnPc6d1
hvězdokupách	hvězdokupa	k1gFnPc6
<g/>
,	,	kIx,
obíhají	obíhat	k5eAaImIp3nP
kolem	kolem	k7c2
hmotného	hmotný	k2eAgInSc2d1
středu	střed	k1gInSc2
kup	kupa	k1gFnPc2
a	a	k8xC
příležitostně	příležitostně	k6eAd1
gravitačně	gravitačně	k6eAd1
vzájemně	vzájemně	k6eAd1
působí	působit	k5eAaImIp3nS
s	s	k7c7
hvězdami	hvězda	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
procházejí	procházet	k5eAaImIp3nP
velmi	velmi	k6eAd1
blízko	blízko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběžné	oběžný	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
hvězd	hvězda	k1gFnPc2
kolem	kolem	k7c2
kupy	kupa	k1gFnSc2
nejsou	být	k5eNaImIp3nP
většinou	většina	k1gFnSc7
kruhové	kruhový	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
oběžné	oběžný	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
planet	planeta	k1gFnPc2
v	v	k7c6
naší	náš	k3xOp1gFnSc6
sluneční	sluneční	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hvězdy	hvězda	k1gFnSc2
v	v	k7c6
hvězdokupách	hvězdokupa	k1gFnPc6
často	často	k6eAd1
padají	padat	k5eAaImIp3nP
přímo	přímo	k6eAd1
do	do	k7c2
středu	střed	k1gInSc2
a	a	k8xC
mnohokrát	mnohokrát	k6eAd1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
po	po	k7c6
neobvyklých	obvyklý	k2eNgFnPc6d1
a	a	k8xC
složitých	složitý	k2eAgFnPc6d1
smyčkách	smyčka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
rozsáhlému	rozsáhlý	k2eAgInSc3d1
prostoru	prostor	k1gInSc3
uvnitř	uvnitř	k7c2
kup	kupa	k1gFnPc2
ale	ale	k8xC
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
srážkám	srážka	k1gFnPc3
hvězd	hvězda	k1gFnPc2
jen	jen	k6eAd1
zřídka	zřídka	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
text	text	k1gInSc1
(	(	kIx(
<g/>
GFDL	GFDL	kA
<g/>
)	)	kIx)
ze	z	k7c2
stránky	stránka	k1gFnSc2
z	z	k7c2
webu	web	k1gInSc2
http://astro.pef.zcu.cz/.	http://astro.pef.zcu.cz/.	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
v	v	k7c6
Mléčné	mléčný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
</s>
<s>
Hvězdokupa	hvězdokupa	k1gFnSc1
</s>
<s>
Otevřená	otevřený	k2eAgFnSc1d1
hvězdokupa	hvězdokupa	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kulová	kulový	k2eAgFnSc1d1
hvězdokupa	hvězdokupa	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4298254-6	4298254-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85127432	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85127432	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
