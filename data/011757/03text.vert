<p>
<s>
Omsk	Omsk	k1gInSc1	Omsk
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
О	О	k?	О
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Ruské	ruský	k2eAgFnSc6d1	ruská
federaci	federace	k1gFnSc6	federace
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
1,178	[number]	k4	1,178
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sedmým	sedmý	k4xOgNnSc7	sedmý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
Omské	omský	k2eAgFnSc2d1	Omská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
též	též	k9	též
centrem	centrum	k1gNnSc7	centrum
sibiřských	sibiřský	k2eAgInPc2d1	sibiřský
kozáků	kozák	k1gInPc2	kozák
<g/>
,	,	kIx,	,
sídlem	sídlo	k1gNnSc7	sídlo
pravoslavného	pravoslavný	k2eAgInSc2d1	pravoslavný
biskupa	biskup	k1gInSc2	biskup
(	(	kIx(	(
<g/>
omská	omský	k2eAgFnSc1d1	Omská
eparchie	eparchie	k1gFnSc1	eparchie
<g/>
)	)	kIx)	)
a	a	k8xC	a
sibiřského	sibiřský	k2eAgMnSc2d1	sibiřský
imáma	imám	k1gMnSc2	imám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Omsk	Omsk	k1gInSc1	Omsk
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Transsibiřské	transsibiřský	k2eAgFnSc6d1	Transsibiřská
magistrále	magistrála	k1gFnSc6	magistrála
zhruba	zhruba	k6eAd1	zhruba
2700	[number]	k4	2700
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Moskvy	Moskva	k1gFnSc2	Moskva
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Irtyše	Irtyše	k1gFnSc2	Irtyše
<g/>
,	,	kIx,	,
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Om	Om	k1gFnSc2	Om
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Omsk	Omsk	k1gInSc1	Omsk
je	být	k5eAaImIp3nS	být
příhraniční	příhraniční	k2eAgNnSc4d1	příhraniční
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Kazachstánem	Kazachstán	k1gInSc7	Kazachstán
<g/>
,	,	kIx,	,
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Barabinské	Barabinský	k2eAgFnSc2d1	Barabinský
stepi	step	k1gFnSc2	step
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
<g/>
,	,	kIx,	,
suché	suchý	k2eAgInPc1d1	suchý
<g/>
,	,	kIx,	,
charakterizované	charakterizovaný	k2eAgInPc1d1	charakterizovaný
dramatickými	dramatický	k2eAgFnPc7d1	dramatická
změnami	změna	k1gFnPc7	změna
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
denní	denní	k2eAgFnSc1d1	denní
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
souhrn	souhrn	k1gInSc1	souhrn
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
pro	pro	k7c4	pro
červen	červen	k1gInSc4	červen
+20	+20	k4	+20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
leden	leden	k1gInSc4	leden
pak	pak	k6eAd1	pak
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
však	však	k9	však
počasí	počasí	k1gNnSc1	počasí
extrémní	extrémní	k2eAgFnSc2d1	extrémní
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
+40	+40	k4	+40
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
-40	-40	k4	-40
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Omsk	Omsk	k1gInSc4	Omsk
v	v	k7c6	v
době	doba	k1gFnSc6	doba
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1716	[number]	k4	1716
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Irtyše	Irtyše	k1gFnSc1	Irtyše
a	a	k8xC	a
Omu	Omu	k1gFnSc1	Omu
postavená	postavený	k2eAgFnSc1d1	postavená
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
bránit	bránit	k5eAaImF	bránit
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
ruské	ruský	k2eAgFnSc2d1	ruská
impérium	impérium	k1gNnSc4	impérium
před	před	k7c4	před
vpády	vpád	k1gInPc4	vpád
stepních	stepní	k2eAgMnPc2d1	stepní
nomádů	nomád	k1gMnPc2	nomád
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Kyrgyzů	Kyrgyz	k1gInPc2	Kyrgyz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
Omsk	Omsk	k1gInSc4	Omsk
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
1782	[number]	k4	1782
<g/>
)	)	kIx)	)
a	a	k8xC	a
opevnění	opevnění	k1gNnSc1	opevnění
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
zděným	zděný	k2eAgInSc7d1	zděný
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
dvě	dva	k4xCgFnPc1	dva
brány	brána	k1gFnPc1	brána
–	–	k?	–
Tobolská	Tobolský	k2eAgFnSc1d1	Tobolský
a	a	k8xC	a
Tarská	Tarský	k2eAgFnSc1d1	Tarský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Omsk	Omsk	k1gInSc1	Omsk
centrem	centr	k1gInSc7	centr
stepní	stepní	k2eAgFnSc2d1	stepní
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
)	)	kIx)	)
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházelo	nacházet	k5eAaImAgNnS	nacházet
několik	několik	k4yIc1	několik
kostelů	kostel	k1gInPc2	kostel
určených	určený	k2eAgInPc2d1	určený
různým	různý	k2eAgFnPc3d1	různá
církvím	církev	k1gFnPc3	církev
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
mešit	mešita	k1gFnPc2	mešita
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
úřad	úřad	k1gInSc1	úřad
gubernátora	gubernátor	k1gMnSc2	gubernátor
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
akademie	akademie	k1gFnSc1	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
hranice	hranice	k1gFnSc1	hranice
neustále	neustále	k6eAd1	neustále
posouvala	posouvat	k5eAaImAgFnS	posouvat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
význam	význam	k1gInSc1	význam
města	město	k1gNnSc2	město
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
upadat	upadat	k5eAaImF	upadat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
sem	sem	k6eAd1	sem
posíláni	posílán	k2eAgMnPc1d1	posílán
vyhnanci	vyhnanec	k1gMnPc1	vyhnanec
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Dostojevskij	Dostojevskij	k1gFnSc1	Dostojevskij
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
přišla	přijít	k5eAaPmAgFnS	přijít
spása	spása	k1gFnSc1	spása
–	–	k?	–
Transsibiřská	transsibiřský	k2eAgFnSc1d1	Transsibiřská
magistrála	magistrála	k1gFnSc1	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
nastal	nastat	k5eAaPmAgInS	nastat
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
nastěhovalo	nastěhovat	k5eAaPmAgNnS	nastěhovat
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
mnoho	mnoho	k4c4	mnoho
obchodních	obchodní	k2eAgFnPc2d1	obchodní
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
otevřeny	otevřen	k2eAgInPc1d1	otevřen
konzuláty	konzulát	k1gInPc1	konzulát
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
-	-	kIx~	-
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
zde	zde	k6eAd1	zde
zastupovaly	zastupovat	k5eAaImAgFnP	zastupovat
obchodní	obchodní	k2eAgInPc4d1	obchodní
zájmy	zájem	k1gInPc4	zájem
svých	svůj	k3xOyFgFnPc2	svůj
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
konzulát	konzulát	k1gInSc1	konzulát
i	i	k8xC	i
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Omsk	Omsk	k1gInSc1	Omsk
zviditelnil	zviditelnit	k5eAaPmAgInS	zviditelnit
uspořádáním	uspořádání	k1gNnSc7	uspořádání
Sibiřské	sibiřský	k2eAgFnSc2d1	sibiřská
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výstavy	výstava	k1gFnSc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
města	město	k1gNnSc2	město
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
nadchla	nadchnout	k5eAaPmAgFnS	nadchnout
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c6	o
Omsku	Omsk	k1gInSc6	Omsk
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
jako	jako	k9	jako
o	o	k7c6	o
Chicagu	Chicago	k1gNnSc6	Chicago
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
monarchisté	monarchista	k1gMnPc1	monarchista
a	a	k8xC	a
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
tu	tu	k6eAd1	tu
ustanovili	ustanovit	k5eAaPmAgMnP	ustanovit
nejprve	nejprve	k6eAd1	nejprve
pětičlennné	pětičlennný	k2eAgFnSc3d1	pětičlennný
demokratické	demokratický	k2eAgFnSc3d1	demokratická
"	"	kIx"	"
<g/>
Direktorium	direktorium	k1gNnSc1	direktorium
<g/>
"	"	kIx"	"
a	a	k8xC	a
poté	poté	k6eAd1	poté
prozatímní	prozatímní	k2eAgFnSc4d1	prozatímní
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Kolčakem	Kolčak	k1gInSc7	Kolčak
<g/>
;	;	kIx,	;
Omsk	Omsk	k1gInSc1	Omsk
byl	být	k5eAaImAgInS	být
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Českoslovenští	československý	k2eAgMnPc1d1	československý
legionáři	legionář	k1gMnPc1	legionář
během	během	k7c2	během
svojí	svůj	k3xOyFgFnSc2	svůj
pouti	pouť	k1gFnSc2	pouť
na	na	k7c4	na
východ	východ	k1gInSc4	východ
do	do	k7c2	do
Vladivostoku	Vladivostok	k1gInSc2	Vladivostok
tu	ten	k3xDgFnSc4	ten
také	také	k9	také
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
bílým	bílý	k1gMnPc3	bílý
udržet	udržet	k5eAaPmF	udržet
město	město	k1gNnSc4	město
a	a	k8xC	a
střežit	střežit	k5eAaImF	střežit
carský	carský	k2eAgInSc4d1	carský
zlatý	zlatý	k2eAgInSc4d1	zlatý
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
Kolčakova	Kolčakův	k2eAgFnSc1d1	Kolčakova
vláda	vláda	k1gFnSc1	vláda
musela	muset	k5eAaImAgFnS	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
do	do	k7c2	do
Irkutska	Irkutsk	k1gInSc2	Irkutsk
a	a	k8xC	a
Omsk	Omsk	k1gInSc4	Omsk
obsadili	obsadit	k5eAaPmAgMnP	obsadit
rudí	rudý	k2eAgMnPc1d1	rudý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Omsk	Omsk	k1gInSc4	Omsk
po	po	k7c6	po
VŘSR	VŘSR	kA	VŘSR
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
aktem	akt	k1gInSc7	akt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
bolševici	bolševik	k1gMnPc1	bolševik
provedli	provést	k5eAaPmAgMnP	provést
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přemístění	přemístění	k1gNnSc1	přemístění
všech	všecek	k3xTgInPc2	všecek
úřadů	úřad	k1gInPc2	úřad
správy	správa	k1gFnSc2	správa
západní	západní	k2eAgFnSc1d1	západní
Sibiře	Sibiř	k1gFnPc1	Sibiř
do	do	k7c2	do
Nikolajevska	Nikolajevsko	k1gNnSc2	Nikolajevsko
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Novosibirsk	Novosibirsk	k1gInSc4	Novosibirsk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
masivní	masivní	k2eAgNnSc4d1	masivní
stěhování	stěhování	k1gNnSc4	stěhování
veškerých	veškerý	k3xTgInPc2	veškerý
správních	správní	k2eAgInPc2d1	správní
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
také	také	k9	také
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
historie	historie	k1gFnSc1	historie
rivality	rivalita	k1gFnSc2	rivalita
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
Omsk	Omsk	k1gInSc1	Omsk
stal	stát	k5eAaPmAgInS	stát
správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
impulzem	impulz	k1gInSc7	impulz
byla	být	k5eAaImAgFnS	být
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
sem	sem	k6eAd1	sem
bylo	být	k5eAaImAgNnS	být
přemístěno	přemístit	k5eAaPmNgNnS	přemístit
mnoho	mnoho	k4c1	mnoho
závodů	závod	k1gInPc2	závod
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
;	;	kIx,	;
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
natrvalo	natrvalo	k6eAd1	natrvalo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
města	město	k1gNnSc2	město
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
komplex	komplex	k1gInSc1	komplex
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nP	patřit
společnosti	společnost	k1gFnPc1	společnost
Sibněft	Sibněftum	k1gNnPc2	Sibněftum
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Omsk	Omsk	k1gInSc1	Omsk
za	za	k7c2	za
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
si	se	k3xPyFc3	se
Omsk	Omsk	k1gInSc4	Omsk
jako	jako	k8xS	jako
celé	celý	k2eAgNnSc4d1	celé
Rusko	Rusko	k1gNnSc4	Rusko
hledá	hledat	k5eAaImIp3nS	hledat
svoje	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
různí	různý	k2eAgMnPc1d1	různý
obchodníci	obchodník	k1gMnPc1	obchodník
získat	získat	k5eAaPmF	získat
vliv	vliv	k1gInSc4	vliv
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
podnicích	podnik	k1gInPc6	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Sibněft	Sibněft	k1gMnSc1	Sibněft
získal	získat	k5eAaPmAgMnS	získat
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
i	i	k8xC	i
majetek	majetek	k1gInSc4	majetek
nad	nad	k7c7	nad
nejvýnosnějšími	výnosný	k2eAgInPc7d3	nejvýnosnější
podniky	podnik	k1gInPc7	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
většinu	většina	k1gFnSc4	většina
moci	moct	k5eAaImF	moct
ve	v	k7c6	v
městě	město	k1gNnSc6	město
ale	ale	k8xC	ale
regionální	regionální	k2eAgFnSc1d1	regionální
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
médii	médium	k1gNnPc7	médium
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
první	první	k4xOgInSc1	první
nový	nový	k2eAgInSc1d1	nový
konzulát	konzulát	k1gInSc1	konzulát
otevřený	otevřený	k2eAgInSc1d1	otevřený
zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Omsk	Omsk	k1gInSc1	Omsk
má	mít	k5eAaImIp3nS	mít
zachovalé	zachovalý	k2eAgNnSc4d1	zachovalé
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
prvních	první	k4xOgNnPc2	první
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
omských	omský	k2eAgFnPc2d1	Omská
památek	památka	k1gFnPc2	památka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
hlavní	hlavní	k2eAgFnSc2d1	hlavní
městské	městský	k2eAgFnSc2d1	městská
třídy	třída	k1gFnSc2	třída
–	–	k?	–
Ljubinského	Ljubinský	k2eAgInSc2d1	Ljubinský
prospektu	prospekt	k1gInSc2	prospekt
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
divadlo	divadlo	k1gNnSc1	divadlo
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
Při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
i	i	k9	i
domy	dům	k1gInPc1	dům
ze	z	k7c2	z
století	století	k1gNnPc2	století
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
byla	být	k5eAaImAgFnS	být
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
byla	být	k5eAaImAgFnS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
a	a	k8xC	a
zdemolovaná	zdemolovaný	k2eAgFnSc1d1	zdemolovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
obnovená	obnovený	k2eAgFnSc1d1	obnovená
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
i	i	k9	i
novoklasicistní	novoklasicistní	k2eAgFnSc1d1	novoklasicistní
katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
od	od	k7c2	od
Vasilije	Vasilije	k1gMnSc2	Vasilije
Stasova	Stasův	k2eAgFnSc1d1	Stasův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bude	být	k5eAaImBp3nS	být
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
350	[number]	k4	350
let	léto	k1gNnPc2	léto
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
dokončena	dokončen	k2eAgFnSc1d1	dokončena
výstavba	výstavba	k1gFnSc1	výstavba
kopie	kopie	k1gFnSc2	kopie
Vojenského	vojenský	k2eAgInSc2d1	vojenský
chrámu	chrám	k1gInSc2	chrám
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
sibiřského	sibiřský	k2eAgNnSc2d1	sibiřské
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
zbourán	zbourat	k5eAaPmNgInS	zbourat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tu	tu	k6eAd1	tu
jsou	být	k5eAaImIp3nP	být
památky	památka	k1gFnPc1	památka
na	na	k7c4	na
předříjnové	předříjnový	k2eAgMnPc4d1	předříjnový
obchodníky	obchodník	k1gMnPc4	obchodník
a	a	k8xC	a
sibiřské	sibiřský	k2eAgMnPc4d1	sibiřský
kozáky	kozák	k1gMnPc4	kozák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Omsku	Omsk	k1gInSc6	Omsk
sídlí	sídlet	k5eAaImIp3nP	sídlet
tyto	tento	k3xDgInPc4	tento
sportovní	sportovní	k2eAgInPc4d1	sportovní
týmy	tým	k1gInPc4	tým
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Avangard	Avangard	k1gInSc1	Avangard
Omsk	Omsk	k1gInSc1	Omsk
-	-	kIx~	-
Kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
Aréna	aréna	k1gFnSc1	aréna
Omsk	Omsk	k1gInSc1	Omsk
</s>
</p>
<p>
<s>
Omička	Omička	k6eAd1	Omička
Omsk	Omsk	k1gInSc1	Omsk
-	-	kIx~	-
Ženská	ženský	k2eAgFnSc1d1	ženská
Volejbalová	volejbalový	k2eAgFnSc1d1	volejbalová
Super	super	k2eAgFnSc1d1	super
Liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
Irtyš	Irtyš	k1gInSc1	Irtyš
Omsk	Omsk	k1gInSc1	Omsk
-	-	kIx~	-
Ruská	ruský	k2eAgFnSc1d1	ruská
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
Neftyanik	Neftyanik	k1gMnSc1	Neftyanik
Omsk	Omsk	k1gInSc1	Omsk
-	-	kIx~	-
Basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
Superliga	superliga	k1gFnSc1	superliga
B	B	kA	B
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
Transsibiřská	transsibiřský	k2eAgFnSc1d1	Transsibiřská
magistrála	magistrála	k1gFnSc1	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
pět	pět	k4xCc4	pět
kilometrů	kilometr	k1gInPc2	kilometr
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
Centrální	centrální	k2eAgNnSc1d1	centrální
letiště	letiště	k1gNnSc1	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
dopravu	doprava	k1gFnSc4	doprava
obstarávají	obstarávat	k5eAaImIp3nP	obstarávat
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
,	,	kIx,	,
tramvaje	tramvaj	k1gFnPc1	tramvaj
a	a	k8xC	a
maršrutky	maršrutka	k1gFnPc1	maršrutka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
linka	linka	k1gFnSc1	linka
Omského	omský	k2eAgNnSc2d1	omský
metra	metro	k1gNnSc2	metro
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnSc3	osobnost
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Innokentij	Innokentít	k5eAaPmRp2nS	Innokentít
Fjodorovič	Fjodorovič	k1gMnSc1	Fjodorovič
Anněnskij	Anněnskij	k1gMnSc1	Anněnskij
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
–	–	k?	–
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
symbolistický	symbolistický	k2eAgMnSc1d1	symbolistický
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
kritik	kritik	k1gMnSc1	kritik
</s>
</p>
<p>
<s>
Valerian	Valerian	k1gInSc1	Valerian
Kujbyšev	Kujbyšev	k1gInSc1	Kujbyšev
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
–	–	k?	–
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bolševický	bolševický	k2eAgMnSc1d1	bolševický
revolucionář	revolucionář	k1gMnSc1	revolucionář
a	a	k8xC	a
prominentní	prominentní	k2eAgMnSc1d1	prominentní
sovětský	sovětský	k2eAgMnSc1d1	sovětský
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Igor	Igor	k1gMnSc1	Igor
Letov	Letov	k1gInSc1	Letov
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
–	–	k?	–
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
punk	punk	k1gMnSc1	punk
rockový	rockový	k2eAgMnSc1d1	rockový
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
</s>
</p>
<p>
<s>
Lisa	Lisa	k1gFnSc1	Lisa
Ryzihová	Ryzihová	k1gFnSc1	Ryzihová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
tyčkařka	tyčkařka	k?	tyčkařka
</s>
</p>
<p>
<s>
Jevgenija	Jevgenij	k2eAgFnSc1d1	Jevgenija
Kanajevová	Kanajevová	k1gFnSc1	Kanajevová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ruská	ruský	k2eAgFnSc1d1	ruská
moderní	moderní	k2eAgFnSc1d1	moderní
gymnastka	gymnastka	k1gFnSc1	gymnastka
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Omsk	Omsk	k1gInSc1	Omsk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Portál	portál	k1gInSc1	portál
města	město	k1gNnSc2	město
</s>
</p>
