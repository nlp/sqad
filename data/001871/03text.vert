<s>
Nukulaelae	Nukulaelae	k1gFnSc1	Nukulaelae
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
ostrovů	ostrov	k1gInPc2	ostrov
tvořících	tvořící	k2eAgInPc2d1	tvořící
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
stát	stát	k1gInSc4	stát
Tuvalu	Tuval	k1gInSc2	Tuval
ležící	ležící	k2eAgFnPc1d1	ležící
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
měl	mít	k5eAaImAgInS	mít
393	[number]	k4	393
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
osídlem	osídlo	k1gNnSc7	osídlo
lidmi	člověk	k1gMnPc7	člověk
ze	z	k7c2	z
sousedního	sousední	k2eAgInSc2d1	sousední
ostrova	ostrov	k1gInSc2	ostrov
Vaitipu	Vaitip	k1gInSc2	Vaitip
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
představeno	představit	k5eAaPmNgNnS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
trosečníkem	trosečník	k1gMnSc7	trosečník
z	z	k7c2	z
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
Elekanou	Elekaný	k2eAgFnSc4d1	Elekaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
uneseny	unesen	k2eAgFnPc1d1	unesena
Peruánskými	peruánský	k2eAgMnPc7d1	peruánský
otrokáři	otrokář	k1gMnPc7	otrokář
do	do	k7c2	do
fosfátových	fosfátový	k2eAgFnPc2d1	fosfátová
dolů	dolů	k6eAd1	dolů
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Chincha	Chincha	k1gFnSc1	Chincha
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Peru	Peru	k1gNnSc2	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
kapitán	kapitán	k1gMnSc1	kapitán
Davis	Davis	k1gFnSc4	Davis
napočítal	napočítat	k5eAaPmAgMnS	napočítat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
pouhých	pouhý	k2eAgInPc2d1	pouhý
95	[number]	k4	95
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Nukulaelae	Nukulaelae	k1gFnSc1	Nukulaelae
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Tuvalu	Tuval	k1gInSc2	Tuval
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
atol	atol	k1gInSc1	atol
sestávající	sestávající	k2eAgFnSc2d1	sestávající
z	z	k7c2	z
devatenácti	devatenáct	k4xCc2	devatenáct
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
,	,	kIx,	,
s	s	k7c7	s
lagunou	laguna	k1gFnSc7	laguna
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
do	do	k7c2	do
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
říká	říkat	k5eAaImIp3nS	říkat
že	že	k8xS	že
ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
spatřen	spatřit	k5eAaPmNgInS	spatřit
bílým	bílý	k2eAgMnSc7d1	bílý
mužem	muž	k1gMnSc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neusadil	usadit	k5eNaPmAgMnS	usadit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
nebyly	být	k5eNaImAgInP	být
stromy	strom	k1gInPc1	strom
a	a	k8xC	a
zem	zem	k1gFnSc1	zem
byla	být	k5eAaImAgFnS	být
pustá	pustý	k2eAgFnSc1d1	pustá
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
tradování	tradování	k1gNnSc2	tradování
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
jiný	jiný	k2eAgMnSc1d1	jiný
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
Valoa	Valoa	k1gMnSc1	Valoa
z	z	k7c2	z
Vaitupu	Vaitup	k1gInSc2	Vaitup
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
objevil	objevit	k5eAaPmAgInS	objevit
Nukulaelae	Nukulaelae	k1gNnSc7	Nukulaelae
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
rybářské	rybářský	k2eAgFnSc6d1	rybářská
výpravě	výprava	k1gFnSc6	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Nezdržel	zdržet	k5eNaPmAgMnS	zdržet
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c6	na
Vaitupu	Vaitup	k1gInSc6	Vaitup
pro	pro	k7c4	pro
kokosové	kokosový	k2eAgInPc4d1	kokosový
ořechy	ořech	k1gInPc4	ořech
které	který	k3yIgNnSc1	který
poté	poté	k6eAd1	poté
na	na	k7c4	na
Nukulaelae	Nukulaelae	k1gFnSc4	Nukulaelae
zasadil	zasadit	k5eAaPmAgInS	zasadit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vrátil	vrátit	k5eAaPmAgInS	vrátit
ještě	ještě	k9	ještě
mnohokrát	mnohokrát	k6eAd1	mnohokrát
a	a	k8xC	a
pokaždé	pokaždé	k6eAd1	pokaždé
přivezl	přivézt	k5eAaPmAgMnS	přivézt
další	další	k2eAgInPc4d1	další
ořechy	ořech	k1gInPc4	ořech
k	k	k7c3	k
zasazení	zasazení	k1gNnSc3	zasazení
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
když	když	k8xS	když
stromy	strom	k1gInPc1	strom
začaly	začít	k5eAaPmAgInP	začít
plodit	plodit	k5eAaImF	plodit
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zeptal	zeptat	k5eAaPmAgInS	zeptat
náčelníka	náčelník	k1gMnSc2	náčelník
Vaitupu	Vaitup	k1gInSc2	Vaitup
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
na	na	k7c4	na
Nukulaelae	Nukulaelae	k1gFnSc4	Nukulaelae
nemohl	moct	k5eNaImAgMnS	moct
usadit	usadit	k5eAaPmF	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Valou	Vala	k1gMnSc7	Vala
doprovodili	doprovodit	k5eAaPmAgMnP	doprovodit
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
Moeva	Moevo	k1gNnSc2	Moevo
a	a	k8xC	a
Katuli	Katule	k1gFnSc4	Katule
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
Teaalo	Teaala	k1gFnSc5	Teaala
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
válečník	válečník	k1gMnSc1	válečník
Takauapa	Takauap	k1gMnSc2	Takauap
z	z	k7c2	z
Funafuti	Funafuť	k1gFnSc2	Funafuť
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
chlapce	chlapec	k1gMnSc4	chlapec
zabil	zabít	k5eAaPmAgMnS	zabít
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Teaalo	Teaala	k1gFnSc5	Teaala
Byla	být	k5eAaImAgNnP	být
ušetřena	ušetřen	k2eAgFnSc1d1	ušetřena
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
porodila	porodit	k5eAaPmAgFnS	porodit
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
Tuvalu	Tuval	k1gInSc2	Tuval
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zvýšit	zvýšit	k5eAaPmF	zvýšit
příjmy	příjem	k1gInPc4	příjem
vydávalo	vydávat	k5eAaPmAgNnS	vydávat
jak	jak	k8xS	jak
poštovní	poštovní	k2eAgFnPc1d1	poštovní
známky	známka	k1gFnPc1	známka
<g/>
,	,	kIx,	,
platící	platící	k2eAgFnPc1d1	platící
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
svých	svůj	k3xOyFgInPc6	svůj
atolech	atol	k1gInPc6	atol
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
i	i	k9	i
známky	známka	k1gFnPc1	známka
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
atol	atol	k1gInSc4	atol
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
existují	existovat	k5eAaImIp3nP	existovat
na	na	k7c6	na
filatelistickém	filatelistický	k2eAgInSc6d1	filatelistický
trhu	trh	k1gInSc6	trh
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
i	i	k8xC	i
známky	známka	k1gFnSc2	známka
s	s	k7c7	s
názvem	název	k1gInSc7	název
NUKULAELAE	NUKULAELAE	kA	NUKULAELAE
-	-	kIx~	-
TUVALU	TUVALU	kA	TUVALU
<g/>
.	.	kIx.	.
</s>
