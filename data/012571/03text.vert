<p>
<s>
Bossa	boss	k1gMnSc4	boss
nova	nova	k1gFnSc1	nova
(	(	kIx(	(
<g/>
též	též	k9	též
bossanova	bossanův	k2eAgFnSc1d1	bossanova
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc4	styl
brazilské	brazilský	k2eAgFnSc2d1	brazilská
hudby	hudba	k1gFnSc2	hudba
konce	konec	k1gInSc2	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
jej	on	k3xPp3gNnSc4	on
skupina	skupina	k1gFnSc1	skupina
studentů	student	k1gMnPc2	student
a	a	k8xC	a
muzikantů	muzikant	k1gMnPc2	muzikant
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
čtvrtích	čtvrt	k1gFnPc6	čtvrt
Copacabana	Copacaban	k1gMnSc2	Copacaban
a	a	k8xC	a
Ipanema	Ipanem	k1gMnSc2	Ipanem
města	město	k1gNnSc2	město
Rio	Rio	k1gMnSc2	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
vešel	vejít	k5eAaPmAgInS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
písní	píseň	k1gFnPc2	píseň
Chega	Chega	k1gFnSc1	Chega
de	de	k?	de
Saudade	Saudad	k1gInSc5	Saudad
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
složili	složit	k5eAaPmAgMnP	složit
Antonio	Antonio	k1gMnSc1	Antonio
Carlos	Carlos	k1gMnSc1	Carlos
Jobim	Jobim	k1gMnSc1	Jobim
a	a	k8xC	a
Vinicius	Vinicius	k1gMnSc1	Vinicius
de	de	k?	de
Moraes	Moraes	k1gMnSc1	Moraes
a	a	k8xC	a
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
Joã	Joã	k1gMnSc1	Joã
Gilberto	Gilberta	k1gFnSc5	Gilberta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světově	světově	k6eAd1	světově
nejznámější	známý	k2eAgFnSc7d3	nejznámější
písní	píseň	k1gFnSc7	píseň
žánru	žánr	k1gInSc2	žánr
je	být	k5eAaImIp3nS	být
The	The	k1gMnSc1	The
Girl	girl	k1gFnSc2	girl
from	from	k1gMnSc1	from
Ipanema	Ipanema	k1gFnSc1	Ipanema
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
nazpívané	nazpívaný	k2eAgFnSc2d1	nazpívaná
Astrud	Astrud	k1gInSc1	Astrud
Gilberto	Gilberta	k1gFnSc5	Gilberta
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
české	český	k2eAgMnPc4d1	český
bossanovy	bossanův	k2eAgMnPc4d1	bossanův
patří	patřit	k5eAaImIp3nS	patřit
Bossa	boss	k1gMnSc2	boss
nova	novum	k1gNnSc2	novum
z	z	k7c2	z
muzikálu	muzikál	k1gInSc2	muzikál
Starci	stařec	k1gMnPc1	stařec
na	na	k7c6	na
chmelu	chmel	k1gInSc6	chmel
či	či	k8xC	či
píseň	píseň	k1gFnSc1	píseň
Cukrářská	cukrářský	k2eAgFnSc1d1	cukrářská
Bossa	boss	k1gMnSc4	boss
nova	nova	k1gFnSc1	nova
(	(	kIx(	(
<g/>
Jarek	Jarek	k1gMnSc1	Jarek
Nohavica	Nohavica	k1gMnSc1	Nohavica
<g/>
,	,	kIx,	,
CD	CD	kA	CD
Koncert	koncert	k1gInSc1	koncert
-	-	kIx~	-
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Samba	samba	k1gFnSc1	samba
</s>
</p>
<p>
<s>
Cha-cha	Chaha	k1gFnSc1	Cha-cha
</s>
</p>
<p>
<s>
Twist	twist	k1gInSc1	twist
</s>
</p>
