<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
studium	studium	k1gNnSc4	studium
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
tzv.	tzv.	kA	tzv.
Appletonových	Appletonový	k2eAgFnPc2d1	Appletonová
vrstev	vrstva	k1gFnPc2	vrstva
v	v	k7c6	v
ionosféře	ionosféra	k1gFnSc6	ionosféra
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
za	za	k7c4	za
důkaz	důkaz	k1gInSc4	důkaz
existence	existence	k1gFnSc2	existence
ionosféry	ionosféra	k1gFnSc2	ionosféra
<g/>
.	.	kIx.	.
</s>
