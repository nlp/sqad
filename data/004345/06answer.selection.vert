<s>
Světáci	Světák	k1gMnPc1	Světák
je	on	k3xPp3gNnSc4	on
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
situační	situační	k2eAgFnSc4d1	situační
komedii	komedie	k1gFnSc4	komedie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
objevují	objevovat	k5eAaImIp3nP	objevovat
ostré	ostrý	k2eAgFnPc4d1	ostrá
narážky	narážka	k1gFnPc4	narážka
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
