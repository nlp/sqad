<p>
<s>
Hunor	Hunor	k1gMnSc1	Hunor
a	a	k8xC	a
Magor	magor	k1gMnSc1	magor
byli	být	k5eAaImAgMnP	být
mýtičtí	mýtický	k2eAgMnPc1d1	mýtický
bratři	bratr	k1gMnPc1	bratr
z	z	k7c2	z
maďarských	maďarský	k2eAgFnPc2d1	maďarská
předkřesťanských	předkřesťanský	k2eAgFnPc2d1	předkřesťanská
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Synové	syn	k1gMnPc1	syn
bohatýra	bohatýr	k1gMnSc2	bohatýr
Ménróta	Ménrót	k1gMnSc2	Ménrót
(	(	kIx(	(
<g/>
krále	král	k1gMnSc2	král
Nimróda	Nimród	k1gMnSc2	Nimród
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
ženy	žena	k1gFnPc4	žena
Eneth	Enetha	k1gFnPc2	Enetha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
Hunor	Hunor	k1gInSc1	Hunor
stal	stát	k5eAaPmAgInS	stát
praotcem	praotec	k1gMnSc7	praotec
Hunů	Hun	k1gMnPc2	Hun
a	a	k8xC	a
Magor	magor	k1gMnSc1	magor
praotcem	praotec	k1gMnSc7	praotec
Maďarů	Maďar	k1gMnPc2	Maďar
(	(	kIx(	(
<g/>
Magyarok	Magyarok	k1gInSc1	Magyarok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Hunorovi	Hunor	k1gMnSc6	Hunor
a	a	k8xC	a
Magorovi	magorův	k2eAgMnPc1d1	magorův
<g/>
:	:	kIx,	:
Bratři	bratr	k1gMnPc1	bratr
Hunor	Hunor	k1gMnSc1	Hunor
a	a	k8xC	a
Magor	magor	k1gMnSc1	magor
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
lov	lov	k1gInSc4	lov
do	do	k7c2	do
bažin	bažina	k1gFnPc2	bažina
meotiských	meotiský	k1gMnPc2	meotiský
<g/>
.	.	kIx.	.
</s>
<s>
Uviděli	uvidět	k5eAaPmAgMnP	uvidět
krásného	krásný	k2eAgMnSc4d1	krásný
jelena	jelen	k1gMnSc4	jelen
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
verzích	verze	k1gFnPc6	verze
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
laň	laň	k1gFnSc4	laň
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
o	o	k7c4	o
jelena	jelen	k1gMnSc4	jelen
zlatého	zlatý	k1gInSc2	zlatý
<g/>
)	)	kIx)	)
a	a	k8xC	a
jali	jmout	k5eAaPmAgMnP	jmout
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
přijeli	přijet	k5eAaPmAgMnP	přijet
na	na	k7c4	na
úžasné	úžasný	k2eAgNnSc4d1	úžasné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
ideální	ideální	k2eAgInSc4d1	ideální
pro	pro	k7c4	pro
usazení	usazení	k1gNnSc4	usazení
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Dostali	dostat	k5eAaPmAgMnP	dostat
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
povolení	povolení	k1gNnPc2	povolení
tak	tak	k6eAd1	tak
učinit	učinit	k5eAaPmF	učinit
a	a	k8xC	a
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
močálů	močál	k1gInPc2	močál
meotiských	meotiských	k2eAgFnSc1d1	meotiských
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yQnSc4	co
vyšli	vyjít	k5eAaPmAgMnP	vyjít
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
našli	najít	k5eAaPmAgMnP	najít
tábor	tábor	k1gInSc4	tábor
bez	bez	k7c2	bez
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
tam	tam	k6eAd1	tam
žily	žít	k5eAaImAgFnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byly	být	k5eAaImAgFnP	být
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
dcery	dcera	k1gFnPc1	dcera
mocného	mocný	k2eAgInSc2d1	mocný
vládce	vládce	k1gMnPc4	vládce
Alanů	Alan	k1gMnPc2	Alan
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
bratři	bratr	k1gMnPc1	bratr
pojali	pojmout	k5eAaPmAgMnP	pojmout
za	za	k7c4	za
manželky	manželka	k1gFnPc4	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
manželky	manželka	k1gFnPc4	manželka
jim	on	k3xPp3gMnPc3	on
daly	dát	k5eAaPmAgInP	dát
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
plémě	plémě	k1gNnSc1	plémě
hunsko-maďarské	hunskoaďarský	k2eAgNnSc1d1	hunsko-maďarský
<g/>
...	...	k?	...
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
verzi	verze	k1gFnSc6	verze
najdeme	najít	k5eAaPmIp1nP	najít
legendu	legenda	k1gFnSc4	legenda
například	například	k6eAd1	například
v	v	k7c6	v
kronice	kronika	k1gFnSc6	kronika
Gesta	gesto	k1gNnSc2	gesto
Hungarorum	Hungarorum	k1gInSc1	Hungarorum
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
legenda	legenda	k1gFnSc1	legenda
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hunové	Hun	k1gMnPc1	Hun
a	a	k8xC	a
Maďaři	Maďar	k1gMnPc1	Maďar
byli	být	k5eAaImAgMnP	být
příbuzné	příbuzný	k2eAgInPc4d1	příbuzný
kmeny	kmen	k1gInPc4	kmen
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
dobytím	dobytí	k1gNnSc7	dobytí
Panonie	Panonie	k1gFnSc2	Panonie
Hunové	Hun	k1gMnPc1	Hun
dali	dát	k5eAaPmAgMnP	dát
Maďarům	Maďar	k1gMnPc3	Maďar
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
"	"	kIx"	"
<g/>
opětovné	opětovný	k2eAgNnSc4d1	opětovné
dobytí	dobytí	k1gNnSc4	dobytí
<g/>
"	"	kIx"	"
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Uher	Uher	k1gMnSc1	Uher
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Legendy	legenda	k1gFnPc1	legenda
a	a	k8xC	a
kroniky	kronika	k1gFnPc1	kronika
koruny	koruna	k1gFnSc2	koruna
uherské	uherský	k2eAgNnSc1d1	Uherské
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Richard	Richard	k1gMnSc1	Richard
Pražák	Pražák	k1gMnSc1	Pražák
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Dagmar	Dagmar	k1gFnSc1	Dagmar
Bartoňková	Bartoňková	k1gFnSc1	Bartoňková
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Nechutová	Nechutový	k2eAgFnSc1d1	Nechutová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
389	[number]	k4	389
s.	s.	k?	s.
</s>
</p>
