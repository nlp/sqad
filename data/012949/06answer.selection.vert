<s>
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
též	též	k9	též
zimní	zimní	k2eAgFnSc1d1	zimní
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
ZOH	ZOH	kA	ZOH
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
zimních	zimní	k2eAgFnPc2d1	zimní
sportovních	sportovní	k2eAgFnPc2d1	sportovní
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
