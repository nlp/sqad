<s>
Součástí	součást	k1gFnSc7
areálu	areál	k1gInSc2
zooparku	zoopark	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
i	i	k8xC
replika	replika	k1gFnSc1
hanáckého	hanácký	k2eAgInSc2d1
statku	statek	k1gInSc2
z	z	k7c2
přelomu	přelom	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
s	s	k7c7
expozicí	expozice	k1gFnSc7
tradičního	tradiční	k2eAgInSc2d1
venkovského	venkovský	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
centrem	centrum	k1gNnSc7
environmentální	environmentální	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
jsou	být	k5eAaImIp3nP
interaktivní	interaktivní	k2eAgFnPc1d1
expozice	expozice	k1gFnPc1
s	s	k7c7
ekologickými	ekologický	k2eAgNnPc7d1
tématy	téma	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Součástí	součást	k1gFnSc7
zooparku	zoopark	k1gInSc2
je	být	k5eAaImIp3nS
i	i	k9
DinoPark	DinoPark	k1gInSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vyškovská	vyškovský	k2eAgFnSc1d1
hvězdárna	hvězdárna	k1gFnSc1
v	v	k7c6
přírodním	přírodní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
Marchanického	Marchanický	k2eAgInSc2d1
hájku	hájek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>